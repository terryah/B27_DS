

@echo off
setlocal enabledelayedexpansion

title CATAECTunnelArrayRing
@echo ===================================
@echo CATAECTunnelArrayRing
@echo Name:Create all data
@echo Auther:xu song
@echo Data:2021/5/24
@echo Vesion:V1
@echo input_parameter:catpart file path.
@echo input_parameter:enter the name of the geometry set where the condition is located.
@echo input_parameter:serial number of the line development plane line in the set.
@echo input_parameter:serial number of line profile spread line in set.
@echo input_parameter:serial number of the line flat curve in the set.
@echo input_parameter:number of points.
@echo input_parameter:spacing of points.
@echo ===================================


@REM 在这种写法可读性好，也能执行多语句，但兼容性不太好
set varA=%COMPUTERNAME%
if "%varA%"=="DESKTOP-2MG53GI" (
    echo %varA% -- "I coding At The Company"
    GOTO COMPANY
) else if "%varA%"=="DDESKTOP-9I4G9M6" (
echo %varA% -- "I coding At Home"
    GOTO HOME
) else (
    echo %varA% -- C
    echo CCC
)

:COMPANY
set CAT_CODE=D:\app\DS\B27_GT\GTR4
set CATPath1=D:\app\DS\B27_GT\GTR4
set CATPath2=D:\app\DS\B27_GT\GTR5
set CATPath3=
set CATPath4=
set CATPath5=
set CATPath6=
set CATPath7=
GOTO START
:HOME
set CAT_CODE=C:\Apps\DS\B27_GT\GTR4
set CATPath1=%CAT_CODE%
set CATPath2=C:\Apps\DS\B27_GT\GTR5
set CATPath3=C:\Apps\DS\B27_GT\GTR7
set CATPath4=%~dp0
set CATPath5=
set CATPath6=
set CATPath7=
GOTO START

:START

@echo ===================================
set CATEnvName=Env9_GT
Set CATEnvPath="%~dp0\CATEnv"
set ModuleName=CATAECTunnelArrayRing.exe
set p1="C:\Users\user\Downloads\MODEL\YONGZHOU1.CATPart"
set p2="RIGHT"
set p3=7
set p4=5
set p5=3
set p6=4400
set p7=2
@echo ===================================

@REM "%CAT_CODE%\win_b64\code\bin\CATSTART.exe" -run "%~dp0win_b64\code\bin\%ModuleName% -h" -env %CATEnvName% -direnv %CATEnvPath%

"%CAT_CODE%\win_b64\code\bin\CATSTART.exe" -run "%~dp0win_b64\code\bin\%ModuleName% %p1% %p2% %p3% %p4% %p5% %p6% %p7%" -env %CATEnvName% -direnv %CATEnvPath%

::cd %~dp0
::"C:\Program Files\Dassault Systemes\B27\win_b64\code\bin\CATSTART.exe"  -run "win_b64\code\bin\CATArcWallBatch.exe C:\XUS\200-CODE\Dassault_Systemes\3DEXPERIENCE_Gitee\DP_B27_Documents\ArcWall\wall_demo.CATPart" -env CATIA_P3.V5-6R2017.B27 -direnv "C:\Program Files\Dassault Systemes\B27\CATEnv"

timeout /t 1
pause

