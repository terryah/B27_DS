@echo off
setlocal enabledelayedexpansion
chcp 936
title CATCIVTunImportAlignmentBatch
@echo ===================================
@echo CATCIVTunImportAlignmentBatch
@echo Name:Create all data
@echo Auther:xu song
@echo Data:2021/5/24
@echo Vesion:V1
@echo input_parameter:cat_part_file_path
@echo ===================================
@REM IF EXIST "D:\apps\DASSAULT_SYSTEMES\DS_DP\GTR3" set CAT_CODE=D:\apps\DASSAULT_SYSTEMES\DS_DP\GTR3


@REM 在这种写法可读性好，也能执行多语句，但兼容性不太好
set varA=%COMPUTERNAME%
if "%varA%"=="DESKTOP-2MG53GI" (
    echo %varA% -- "I coding At The Company"
    GOTO COMPANY
) else if "%varA%"=="DESKTOP-9I4G9M6" (
echo %varA% -- "I coding At Home"
    GOTO HOME
) else (
    echo %varA% -- C
    echo CCC
)

:COMPANY
set CAT_CODE=D:\app\DS\B27_GT\GTR4
set CATPath1=D:\app\DS\B27_GT\GTR4
set CATPath2=D:\app\DS\B27_GT\GTR5
set CATPath3=
set CATPath4=
set CATPath5=
set CATPath6=
set CATPath7=
GOTO START
:HOME
set CAT_CODE=C:\Apps\DS\B27_GT\GTR4
set CATPath1=%CAT_CODE%
set CATPath2=C:\Apps\DS\B27_GT\GTR5
set CATPath3=C:\Apps\DS\B27_GT\GTR7
set CATPath4=%~dp0
set CATPath5=
set CATPath6=
set CATPath7=
GOTO START

:START

@echo ===================================
set CATEnvName=Env9
Set CATEnvPath="%~dp0\CATEnv"
set ModuleName=CATCIVTunImportAlignmentBatch.exe
set txt="C:\Users\user\Downloads\tst.txt"
set part="C:\Users\user\Downloads\Part1.CATPart"

@echo ===================================
"%CAT_CODE%\win_b64\code\bin\CATSTART.exe" -run "%~dp0win_b64\code\bin\%ModuleName% %txt% %part%" -env %CATEnvName% -direnv %CATEnvPath%

::cd %~dp0
::"C:\Program Files\Dassault Systemes\B27\win_b64\code\bin\CATSTART.exe"  -run "win_b64\code\bin\CATArcWallBatch.exe C:\XUS\200-CODE\Dassault_Systemes\3DEXPERIENCE_Gitee\DP_B27_Documents\ArcWall\wall_demo.CATPart" -env CATIA_P3.V5-6R2017.B27 -direnv "C:\Program Files\Dassault Systemes\B27\CATEnv"

timeout /t 1
pause
