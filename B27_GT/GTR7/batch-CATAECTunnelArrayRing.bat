
chcp 936
@echo off
setlocal enabledelayedexpansion

title CATAECTunnelArrayRing
@echo ===================================
@echo CATAECTunnelArrayRing
@echo Name:Create all data
@echo Auther:xu song
@echo Data:2021/5/24
@echo Vesion:V1
@echo input_parameter:enter the line file address
@echo input_parameter:enter template address
@echo input_parameter:enter the name of the graph set where the line is located
@echo input_parameter:ring quantity
@echo ===================================

@REM 在这种写法可读性好，也能执行多语句，但兼容性不太好
set varA=%COMPUTERNAME%
if "%varA%"=="DESKTOP-2MG53GI" (
    echo %varA% -- "I coding At The Company"
    GOTO COMPANY
) else if "%varA%"=="DESKTOP-9I4G9M6" (
echo %varA% -- "I coding At Home"
    GOTO HOME
) else (
    echo %varA% -- C
    echo CCC
)

:COMPANY
set CAT_CODE=D:\app\DS\B27_GT\GTR4
set CATPath1=D:\app\DS\B27_GT\GTR4
set CATPath2=D:\app\DS\B27_GT\GTR5
set CATPath3=
set CATPath4=
set CATPath5=
set CATPath6=
set CATPath7=
GOTO START
:HOME
set CAT_CODE=C:\Apps\DS\B27_GT\GTR4
set CATPath1=C:\Apps\DS\B27_GT\GTR4
set CATPath2=C:\Apps\DS\B27_GT\GTR5
set CATPath3=C:\Apps\DS\B27_GT\GTR7
set CATPath4=%~dp0
set CATPath5=
set CATPath6=
set CATPath7=
GOTO START

:START

@echo ===================================
set CATEnvName=Env9_GT
Set CATEnvPath="%~dp0\CATEnv"
set ModuleName=CATAECTunnelArrayRing.exe
set p1=%~dp0win_b64\startup\Tunnel\Part3.CATPart
set p2="%~dp0win_b64\startup\Tunnel\TUNNEL-RING-PW-NOREC.CATPart"
set p3="AA"
set p4=100
set p5=28
@echo ===================================

@REM "%CAT_CODE%\win_b64\code\bin\CATSTART.exe" -run "%~dp0win_b64\code\bin\%ModuleName% -h" -env %CATEnvName% -direnv %CATEnvPath%

"%CAT_CODE%\win_b64\code\bin\CATSTART.exe" -run "%~dp0win_b64\code\bin\%ModuleName% %p1% %p2% %p3% %p4% %p5%" -env %CATEnvName% -direnv %CATEnvPath%

::cd %~dp0
::"C:\Program Files\Dassault Systemes\B27\win_b64\code\bin\CATSTART.exe"  -run "win_b64\code\bin\CATArcWallBatch.exe C:\XUS\200-CODE\Dassault_Systemes\3DEXPERIENCE_Gitee\DP_B27_Documents\ArcWall\wall_demo.CATPart" -env CATIA_P3.V5-6R2017.B27 -direnv "C:\Program Files\Dassault Systemes\B27\CATEnv"

timeout /t 1
pause

@REM 角度算法
@REM #include <iostream>
@REM #include <vector>

@REM int main() {
@REM     double angle = 360.0000/14.0000;
@REM     double interval = (360.0000/28.0000)*3;
@REM     std::vector<double> angleList;
@REM     angleList.push_back(0.000);
@REM     while (angle <= 360.0000) {
@REM         //std::cout << angle << "度" << std::endl;
@REM         angleList.push_back(angle);
@REM         angle += interval;
@REM     }
@REM     for (const auto &item : angleList) {
@REM         std::cout << item << std::endl;
@REM     }
@REM     return 0;
@REM }

