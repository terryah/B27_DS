@echo off
SET DS_UNINSTALL_BATCH=YES
if exist "C:\Program Files\Dassault Systemes\B27_GT\EndBatch" del "C:\Program Files\Dassault Systemes\B27_GT\EndBatch"
if exist "C:\Program Files\Dassault Systemes\B27_GT\ErrorBatch" del "C:\Program Files\Dassault Systemes\B27_GT\ErrorBatch"
"C:\Program Files\Dassault Systemes\B27_GT\win_b64\code\bin\Uninstall.exe" "C:\Program Files\Dassault Systemes\B27_GT" "CODE" "GUI" "B27" "0"
:wait
if  exist "C:\Program Files\Dassault Systemes\B27_GT\ErrorBatch" goto error
if not exist "C:\Program Files\Dassault Systemes\B27_GT\EndBatch" goto wait
goto end
:error
@echo Uninstall failed : see  C:\Users\user\AppData\Local\Temp\cxinst.log
if exist "C:\Program Files\Dassault Systemes\B27_GT\ErrorBatch" del "C:\Program Files\Dassault Systemes\B27_GT\ErrorBatch"
goto exit
:end
del /Q  "C:\Program Files\Dassault Systemes\B27_GT\win_b64\code\bin\Uninstall.exe" 2>nul
rmdir /Q /S "C:\Program Files\Dassault Systemes\B27_GT" 2>nul
@echo End of uninstall
:exit
