/* global dscef */

(function() {
  'use strict';
  var deps = ['DS/WebRecordReplayDriver/InteractionInterpreterNative'];
  require(deps, function(Interpreter) {
    try {
      dscef.Record.interpreter = new Interpreter();
      dscef.Record.notifyInterpreterLoad();
      console.log('InteractionInterpreterNative instantiated.');
    } catch (e) {
      var err = 'Failed to load InteractionInterpreterNative.js.';
      dscef.showNotification('critical', err, 0);
      console.error(err, e);
    }
  });
})();
