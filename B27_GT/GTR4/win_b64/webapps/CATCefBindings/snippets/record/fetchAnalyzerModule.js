/* global dscef */

(function() {
  'use strict';
  // TODO: (d6k) Load WebRecordAnalyzer and save the import as a property in dscef.Record.
  var deps = ['DS/WebRecordClient/Record/WebRecordAnalyzer'];
  require(deps, function (analyzer) {
    try {
      dscef.Record.analyzer = analyzer;
    } catch (e) {
      var err = 'Failed to load WebRecordAnalyzer.js.';
      dscef.showNotification('critical', err, 0);
      console.error(err, e);
    }
  });
})();
