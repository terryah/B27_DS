/* global dscef */

(function() {
  'use strict';
  var deps = ['DS/WebRecordClient/Record/RecordManagerInterface'];
  require(deps, function(recordInterface) {
    try {
      dscef.Record.manager = recordInterface;
      dscef.Record.manager.startRecord();
    } catch (e) {
      var err = 'Failed to load RecordManager.';
      dscef.showNotification('critical', err, 0);
      console.error(err, e);
    }
  });
})();
