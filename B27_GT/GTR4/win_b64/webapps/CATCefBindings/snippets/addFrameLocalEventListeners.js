/* global dscef */

(function() {
  'use strict';

  // Trigger a native event on zoom changes.
  // We are using web event listeners because zoom should not happen when an
  // event listener has used Event.preventDefault().
  // In this case, handling zoom actions in the C++ wrapper is too early.
  document.addEventListener('mousewheel', function(e) {
    if (e.ctrlKey) {
      // Don't scroll if CTRL is held down (expected behavior for zoom in and zoom out).
      e.preventDefault();
      // On scroll down, deltaY is positive while wheelDeltaY is negative. Be careful.
      var delta = e.wheelDeltaY;
      dscef._internals.notifyZoomChange(
        delta - delta % dscef._internals.SCROLL_STEP
      );
    }
  });

  // Same thing for zoom triggered with a keyboard.
  document.addEventListener('keydown', function(e) {
    // On AZERTY layouts, ALTGR is interpreted as CTRL+ALT.
    if (e.ctrlKey && !e.altKey) {
      // With CTRL held down.
      var key = e.keyCode;
      if (key === 187 || key === 107) {
        // '= +' or 'numpad +' key.
        dscef._internals.notifyZoomChange(dscef._internals.SCROLL_STEP);
      } else if (key === 189 || key === 109) {
        // '- _' or 'numpad -' key.
        dscef._internals.notifyZoomChange(-dscef._internals.SCROLL_STEP);
      } else if (key === 48) {
        // '0' key.
        dscef._internals.notifyZoomReset();
      }
    }
  });
})();
