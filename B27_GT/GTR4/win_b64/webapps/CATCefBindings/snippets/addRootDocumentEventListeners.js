/* global dscef */
/* eslint no-console: off */

(function() {
  'use strict';

  // Notify the native client when the DOM is ready.
  var notifyDOMReady = dscef._internals.notifyDOMReady;
  if (document.readyState === 'loading') {
    document.addEventListener('DOMContentLoaded', notifyDOMReady, true);
  } else {
    notifyDOMReady();
  }

  // Notify the native client when the document is being unloaded.
  document.addEventListener('unload', dscef._internals.notifyUnload);
})();
