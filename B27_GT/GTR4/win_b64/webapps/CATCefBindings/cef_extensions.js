/*! Copyright Dassault Systemes 2015 */

/**
 * Global namespace for all bindings, registered as a v8 extension.
 * @example <caption>Use this to check for its existence in webapp contexts.</caption>
 * typeof(dscef) !== 'undefined'
 * @namespace dscef
 */
var dscef = {};

// Avoid references to the DOM or loading the extension will terminate the process.
(function() {
  'use strict';

  /* jshint ignore:start */
  // Public.
  native function __sendDocumentSize();
  native function __sendString();
  native function __loadHomepage();
  native function __openDevTools();
  native function __closeDevTools();
  native function __showNotification();
  native function __getEnv();
  native function __getMyAppsURL();
  native function __saveValue();
  native function __loadValue();

  // Private.
  native function __notifyDOMReady();
  native function __notifyLoad();
  native function __notifyUnload();
  native function __notifyZoomChange();
  native function __notifyZoomReset();
  native function __registerPromise();
  native function __resolvePromise();
  native function __rejectPromise();
  /* jshint ignore:end */

  /**
   * Sends the document width and height to the native client.
   * @function sendDocumentSize
   * @memberof dscef
   * @return {undefined}
   */
  dscef.sendDocumentSize = function() {
    __sendDocumentSize();
  };

  /**
   * Sends one or several string objects to the native client.
   * @function sendString
   * @memberof dscef
   * @param {...String} args
   * String data that will be sent to the native controller.
   * @param {Object} [options]
   * Optional parameters for the function.
   * @param {Boolean} [options.recordable=false]
   * If true, the data will be captured for Record ODTs.
   * Else, it is ignored.
   * @param {Boolean} [options.captureAll=true]
   * This option is taken into account only if options.recordable is true.
   * If true, all arguments will be captured for Record ODTs.
   * Else, only the first argument will be captured.
   * @return {undefined}
   */
  dscef.sendString = function() {
    log('sendString', arguments);

    // Final arguments for SendString().
    var origin = {
      url: '<anonymous>',
      line: 1,
      column: 1
    };
    var args = [];
    var options = {};

    var argCount = arguments.length;

    // Check the arguments.
    if (argCount < 1) {
      throw new Error('The sendString() function needs at least one argument.');
    }

    for (var i = 0; i < argCount; ++i) {
      var current = arguments[i];
      var last = (i === argCount - 1);

      // The last argument can be a string or the options object.
      if (last && typeof (current) === 'object') {
        options = current;
        continue;
      }
      checkType(i, current, 'string', 'sendString');
      args.push(current);
    }

    // Line 0: error creation in _trace().
    // Line 1: _trace() call in dscef function.
    // > Line 2: dscef function call.
    var location = trace(2);
    if (location) {
      origin.url = location.url;
      origin.line = Number.parseInt(location.line, 10);
      origin.column = Number.parseInt(location.column, 10);
    }

    __sendString(args, origin, options);
  };

  /**
   * Loads the homepage set by the native client.
   * @function loadHomepage
   * @memberof dscef
   * @return {undefined}
   */
  dscef.loadHomepage = function() {
    log('loadHomepage');
    __loadHomepage();
  };

  /**
   * Opens the developer tools in a popup window.
   * @function openDevTools
   * @memberof dscef
   * @return {undefined}
   */
  dscef.openDevTools = function() {
    log('openDevTools');
    __openDevTools();
  };

  /**
   * Closes the developer tools window associated with the current browser.
   * @function closeDevTools
   * @memberof dscef
   * @return {undefined}
   */
  dscef.closeDevTools = function() {
    log('closeDevTools');
    __closeDevTools();
  };

  /**
   * Sends a message that will be displayed as a visual notification
   * by the native client.
   * @function showNotification
   * @memberof dscef
   * @param {String} type
   * The type of notification. It can be: 'info', 'warning', 'error' or 'critical'.
   * @param {String} text
   * The text that will be displayed by the notification.
   * @param {Number} duration
   * The display duration in seconds.
   * @return {undefined}
   */
//   dscef.showNotification = function(type, text, duration) {
//     log('showNotification', arguments);
//     checkType(0, type, 'string', 'showNotification');
//     checkType(1, text, 'string', 'showNotification');
//     checkType(2, duration, 'number', 'showNotification');
//     __showNotification(type, text, duration);
//   };

  /**
   * Returns the value of an environment variable.
   * @function getEnv
   * @memberof dscef
   * @param {String} name
   * The name of the wanted variable.
   * @return {String}
   * The value of the wanted variable.
   */
  dscef.getEnv = function(name) {
    // Do not use _log() here to avoid triggering infinite recursive loops.
    // _log('getEnv', arguments);
    return __getEnv(name);
  };

  /**
   * Returns a root URL that is required to use web services on MyApps.
   * @return {Promise}
   * A Promise that can be resolved with a URL for MyApps.
   */
//   dscef.getMyAppsURL = function() {
//     return dscef._internals.promisify(__getMyAppsURL);
//   };

  /**
   * Saves any value on disk using a CATSysPreferenceRepository.
   * @param {String} name
   * A unique identifier for the value.
   * @param {*} value
   * A JS value of any type, except `undefined` and `function`.
   * @return {Promise<void>}
   * A promise that resolves if the value was saved
   * or rejects with an error describing the issue.
   */
  dscef.saveValue = function(name, value) {
    if (typeof name !== 'string' || name.length < 1) {
      return Promise.reject(
        new Error('1st argument is invalid: the name should be a non-empty string')
      );
    }

    var type = typeof value;
    if (type === 'undefined' || type === 'function' || type === 'symbol') {
      return Promise.reject(new Error('2nd argument is invalid: unsupported type (' + type + ')'));
    }

    return dscef._internals.promisify(__saveValue, {
      origin: encodeURIComponent(window.location.origin),
      name: name,
      value: JSON.stringify(value)
    });
  };

  /**
   * Loads the value identified by the `name` parameter using a CATSysPreferenceRepository.
   * @param {String} name
   * The unique identifier for the value.
   * @return {Promise<*>}
   * A promise that resolves with the targeted value
   * or rejects with an error describing the issue.
   */
  dscef.loadValue = function(name) {
    if (typeof name !== 'string' || name.length < 1) {
      return Promise.reject(
        new Error('1st argument is invalid: the name should be a non-empty string')
      );
    }

    return dscef._internals.promisify(__loadValue, {
      origin: encodeURIComponent(window.location.origin),
      name: name
    });
  };

  /**
   * Gathers all private attributes and functions.
   * @private
   */
  dscef._internals = {
    /**
     * Private callback to dispatch a native event on the DOMContentLoaded event.
     * We still have to expose it or addEventListener won't find it.
     * @private
     * @return {undefined}
     */
    notifyDOMReady: function() {
      __notifyDOMReady(); // jshint ignore:line
    },

    /**
     * [description]
     * @return {[type]} [description]
     */
    notifyLoad: function() {
      __notifyLoad(); // jshint ignore:line
    },

    /**
     * Private callback to dispatch a native event on the unload event.
     * @private
     * @return {undefined}
     */
    notifyUnload: function() {
      __notifyUnload(); // jshint ignore:line
    },

    /**
     * Private callback to dispatch a native event on zoom changes
     * triggered by a keyboard or a mouse.
     * @param {Number} delta
     * The increment on the current zoom value.
     * @private
     * @return {undefined}
     */
    notifyZoomChange: function(delta) {
      checkType(0, delta, 'number', 'notifyZoomChange');
      __notifyZoomChange(delta); // jshint ignore:line
    },

    /**
     * Private callback to dispatch a native event on zoom reset.
     * @private
     * @return {undefined}
     */
    notifyZoomReset: function() {
      __notifyZoomReset(); // jshint ignore:line
    },

    /**
     * Wraps a native function with a promise.
     * The target function should be implemented with as:
     *   function target(Number: promiseKey, Array: args)
     * @param {Function} func
     * The function that should resolve or reject the promise.
     * @param {Object} args
     * The arguments for the wrapped function, as named properties.
     * @return {Promise}
     * A promise that will resolve with the wrapped function's return value
     * or reject with an error.
     */
    promisify: function(func, args) {
      return new Promise(function(resolve, reject) {
        var promiseKey = registerPromise(new PromiseExecutor(resolve, reject));
        func(promiseKey, args);
      });
    },

    /**
     * Private helper for resolving Promise objects
     * that are stored in one of the C++ handlers.
     * @param {Number} key
     * Unique identifier for the Promise object.
     * @param {*} value
     * The result for the resolved Promise.
     * @private
     * @return {undefined}
     */
    resolvePromise: function(key, value) {
      __resolvePromise(key, value); // jshint ignore:line
    },

    /**
     * Private helper for rejecting Promise objects
     * that are stored in one of the C++ handlers.
     * @param {Number} key
     * Unique identifier for the Promise object.
     * @param {*} reason
     * The reason why the Promise has been rejected.
     * @private
     * @return {undefined}
     */
    rejectPromise: function(key, reason) {
      __rejectPromise(key, reason); // jshint ignore:line
    },

    /**
     * Fixed multiplier for CEF scroll steps.
     * @const {Number}
     * @private
     * @readonly
     */
    SCROLL_STEP: 50
  };

  // ------------------------------------------------------------------------
  // Helper functions.
  // ------------------------------------------------------------------------

  // Argument checks.
  function checkType(argIndex, arg, expectedType, functionName) {
    var argType = typeof (arg);
    if (argType !== expectedType) {
      throw new TypeError(
        'Argument ' + String(argIndex + 1) + ' for function ' +
        functionName + '() should be of type: ' + expectedType +
        '. Actual type is: ' + argType
      );
    }
  }

  // Wrapper class for promise executor functions that carry a resolver and a rejector
  // to defer their execution in a native V8 handler.
  var PromiseExecutor = function(resolve, reject) {
    this.resolve = resolve;
    this.reject = reject;
  };

  // Registers resolve() and reject() functions in a native handler
  // so they can be called asynchronously from C++ functions.
  // Returns a key to retrieve the item later.
  function registerPromise(promiseExecutor) {
    return __registerPromise(promiseExecutor);
  }

  // Location struct for describing call traces.
  var Location = function(url, line, column) {
    this.url = url;
    this.line = line;
    this.column = column;
  };

  // Formatting for console logs.
  Location.prototype.toString = function() {
    return this.url + ':' + this.line + ':' + this.column;
  };

  // Separates line and column numbers from a string of the form "uri:line:column".
  function extractLocation(location) {
    // Fail-fast but return locations like "(native)".
    if (location.indexOf(':') === -1) {
      return [location];
    }

    // Removes parens.
    var stripped = location.replace(/[\(\)]/g, '');

    // Captures URI, line number, and column without the colons.
    var partsRE = /(.+?)(?:\:(\d+))?(?:\:(\d+))?$/;
    var parts = partsRE.exec(stripped);

    return new Location(
      parts[1],
      parts[2] || 1,
      parts[3] || 1
    );
  }

  // Returns a parsed error stack.
  function getOriginFromError(error, target) {
    var stackLines = error.stack.split('\n');
    var filtered = stackLines.filter(function(line) {
      // RegExp for V8 stack traces.
      return !!line.match(/^\s*at .*(\S+\:\d+|\(native\))/m);
    });

    var parse = function(line) {
      if (line.indexOf('(eval ') > -1) {
        // Throw away eval info (not supported).
        line = line.replace(/eval code/g, 'eval')
          .replace(/(\(eval at [^\()]*)|(\)\,.*$)/g, '');
      }

      // Tokenize the stack frame.
      var tokens = line.replace(/^\s+/, '')
        .replace(/\(eval code/g, '(')
        .split(/\s+/)
        .slice(1);
      var location = extractLocation(tokens.pop());
      var fileName = ['eval', '<anonymous>'].indexOf(location.url) > -1 ? '<anonymous>' : location.url;

      return new Location(
        fileName,
        location.line || 1,
        location.column || 1
      );
    };

    return filtered.length > target ? parse(filtered[target]) : null;
  }

  // Returns a stack trace to help track a caller origin.
  function trace(target) {
    return getOriginFromError(new Error(), target);
  }

  // Conditional log for dscef functions.
  var _logList = [];

  function log(functionName, args) {
    if (_logList.length === 0) {
      // Extract the names of the functions to log.
      _logList = dscef.getEnv('CATCEF_BINDINGS_LOG')
        .replace(/\s/g, '')
        .split(',');
    }

    var location = trace(3);

    if (_logList.indexOf(functionName) !== -1 || _logList.indexOf('*') !== -1) {
      console.log('[DSCEF LOG] ' + functionName + ' called from ' + location.toString());
      if (args) {
        console.dir(args);
      }
    }
  }
}());

/**
 * @function CATCefSendString
 * @deprecated Use {@link dscef.sendString} instead.
 * @return {void}
 */
var CATCefSendString = function() { //eslint-disable-line no-unused-vars
  'use strict';
  console.warn('CATCefSendString() is deprecated. Use dscef.sendString() instead.');
  // Using func.apply() because no spread operator in ES5.
  dscef.sendString.apply(this, arguments);
};
