if (typeof (window.dsCATIAV5MakeHandlers) === 'function') {
  console.log("V5 Integration: dsCATIAV5MakeHandlers already defined");
}
else {
  window.dsCATIAV5MakeHandlers = function () {
    console.log("V5 Integration: dsCATIAV5MakeHandlers");

    'use strict';

    (function waitForMakeAPIReady() 
    {
      if (typeof (window._addin_subscribe) === 'function') 
      {
        window._addin_publish('READY');

        window._addin_subscribe('REQUEST_PART', function () {
            console.log('CV5MakeIntegration::REQUEST_PART')
            sendEventToWin('cv5make_request_part', {});
        });
  
        window._addin_subscribe('REMOVE_PART', function ( id ) {
            console.log('CV5MakeIntegration::REMOVE_PART : ' + id);
            sendEventToWin('cv5make_remove_part', { 'id': id });
        });
  
        window._addin_subscribe('REQUEST_DRIVE_DOCUMENTS', function ( obj ) {
            console.log('CV5MakeIntegration::REQUEST_DRIVE_DOCUMENTS : ' + obj.ids);
            sendEventToWin('cv5make_request_drive_documents', obj);
          });

        window._addin_subscribe('CLOSE', function () {
            console.log('CV5MakeIntegration::CLOSE');
            if (typeof (dscef.loadHomepage) === 'function') {
                dscef.loadHomepage();
            }
            sendEventToWin('cv5make_close', {});
        });

        window.addEventListener("beforeunload", function (event) {
          console.log('CV5MakeIntegration::beforeunload');
          sendEventToWin('cv5make_beforeunload', {});
        });

        sendEventToWin('cv5make_ready', {});
      }
      else
      {
        console.log("V5 Integration: dsCATIAV5MakeHandlers - wait for make API ready");
        window.setTimeout(waitForMakeAPIReady, 250);
      }
      
    })();


    function sendEventToWin(event, data) {
      if (typeof (dscef.sendString) === 'function') {
          dscef.sendString(JSON.stringify({ 'event': event, 'content': data }));
      }
    }

  };
  window.dsCATIAV5MakeHandlers();
}
