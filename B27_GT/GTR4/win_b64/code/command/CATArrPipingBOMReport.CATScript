'// COPYRIGHT DASSAULT SYSTEMES  2002
'//============================================================================
'//
'// CATArrBOMReport.CATScript
'//
'// Language="VBSCRIPT"
'// This CATScript allows the user to generate a report on bult count from a
'// Piping document.  
'// It assumes that the user has opened a piping doucment and the piping workbench
'// is activated.
'//  
'//
'//============================================================================
'// Major CATIA interface used:
'//
'// interface           VB Name         Remarks
'// -----------       ------------    --------------
'// CATIADocument       Document        access the current document
'// CATIAArrWorkbench   ArrWorkbench    access the arrangement workbench 
'//============================================================================
'//
'//============================================================================           
'//
'// Audit Trail:
'//----------------------------------------------------------------------------
'//   Author     : ASN
'//   Date       : 4/2002
'//   Chg id     :
'//   Chg nature :
'//----------------------------------------------------------------------------
'//============================================================================

Option Explicit

'******************************************************************************
'  GLOBAL variable declarations section
'******************************************************************************
'// ---------- Debug Traces
Const intG_TRACEON = 0

Dim  intGCancel                  As Integer

'// ---------- CATIAV5 application objects
Dim objCATIAV5Document As Document
Dim objCATIAV5ArrWorkbench As Workbench
Dim objCATIAV5ArrBOMReport As ArrBOMReport

Dim  strGReportOutputPath  As String

Dim  strGInputPanelTitle         As String
Dim  strGInputPanelPrompt As String

Const intNT   = 0
Const intUNIX = 1

Dim  intOS                  As Integer

Dim strDirectorySlashArray(2) As String
strDirectorySlashArray(intNT) = "\"
strDirectorySlashArray(intUNIX) = "/"

Dim strCATStartupPath As String
strCATStartupPath = CATIA.SystemService.Environ("CATStartupPath") 
Dim intSlashLocation As Integer
intSlashLocation  = Instr(3, strCATStartupPath, strDirectorySlashArray(intUNIX))  
       
'---------- Search for a slash 
If (intSlashLocation > 0) Then    
  '// ----------- Unix Operating System
  DbgTrace " ----------- Unix Operating System",0
  intOS = intUNIX
Else
  '// ----------- NT Operating System
  DbgTrace " ----------- NT Operating System",0
  intOS = intNT
End If


strGInputPanelTitle = "Report Generation"

strGInputPanelPrompt = "In order for this script to generate a bolt count report for a piping document, Piping workbench must be activated. Define report output file:"


'------------------------------------------------------------------------------
Sub DbgTrace (iStrMsgString, iIntYesError)
'------------------------------------------------------------------------------
If (intG_TRACEON = 1) Then
  CATIA.SystemService.Print iStrMsgString 
  If (iIntYesError = 1) Then 
    CATIA.SystemService.Print "Err Number = " & Err.Number
  End If
End If

End Sub '//////////////////////////////////////////////////////////// DbgTrace


'------------------------------------------------------------------------------
Sub StartCATIAV5 ()
'------------------------------------------------------------------------------
  On Error Resume Next
  Set objCATIAV5Document = CATIA.ActiveDocument
  DbgTrace "V5: Active Document",1

  '//---------- Get Schematic from current document
  Set objCATIAV5ArrWorkbench = objCATIAV5Document.GetWorkbench("ArrWorkbench") 
  DbgTrace "V5: GetWorkbench",1

  Set objCATIAV5ArrBOMReport = objCATIAV5ArrWorkbench.FindInterface ("CATIAArrBOMReport",objCATIAV5Document)
  DbgTrace "V5: FindInterface",1

  '[L33]::CATScript changed for IR-165963 
  Dim Ret
  Ret = objCATIAV5ArrBOMReport.GenerateBOMReport(objCATIAV5Document, strGReportOutputPath)
  
  'If (Not (objCATIAV5ArrWorkbench Is Nothing)) Then
  'objCATIAV5ArrBOMReport.GenerateBOMReport objCATIAV5Document, strGReportOutputPath
  'End If
      'CATIA.SystemService.Print "Err Number = " & Err.Number
      'CATIA.SystemService.Print "Err Source = " & Err.Source
      'CATIA.SystemService.Print "Err Description = " & Err.Description 
  'If Err.Number <> 0 Then   
     'Err.Clear
     'CATIA.SystemService.Print "Err Number (Complete) = " & Err.Number
     'nBtn = MsgBox("Report Generation Complete",0,strGInputPanelTitle) 
  'Else
	 'CATIA.SystemService.Print "Err Number (Failed) = " & Err.Number
     'nBtn = MsgBox("Report Generation Failed",0,strGInputPanelTitle) 
  'End If 

  '[L33]::CATScript changed for IR-165963
  'Message popup is now governed by the return value from GenerateBOMReport()
  Dim nBtn
  If ( Ret = 0 ) Then
     nBtn = MsgBox("Report Generation Complete",0,strGInputPanelTitle) 
  Else
     nBtn = MsgBox("Report Generation Failed",0,strGInputPanelTitle) 
  End If
  
End Sub '/////////////////////////////////////////////////////////// StartCATIAV5


'------------------------------------------------------------------------------
Sub GetReportFile ()
'------------------------------------------------------------------------------
  Dim strCATTempPath As String

  Dim ApplIfileSys      As Object
  Set ApplIfileSys = CATIA.FileSystem 

  '// Find a temp directory to place the output file
  strCATTempPath    = CATIA.SystemService.Environ("CATTemp") 
  DbgTrace "CATTemp path = " & strCATTempPath ,0

  strGReportOutputPath = strCATTempPath + strDirectorySlashArray(intOS) + "PipingBOMReport.html"

    '---------- InputBox returns the string when user press the Enter/OK key
	'---------- and returns null string when user press Cancel
    strGReportOutputPath = InputBox(strGInputPanelPrompt, strGInputPanelTitle, strGReportOutputPath)

    If ( Len (strGReportOutputPath) = 0) Then
      intGCancel = 1
    End If

  '---------- No need to display error message, the above loop will not
  '---------- be exited until a valid template is found or Cancel is selected
  If ( intGCancel = 0 And AppliFileSys.FileExists(strGReportOutputPath)) Then
     Dim strMessage
	 Dim nBtn

     strMessage  = "Report File: " + strGReportOutputPath + " exists, Overwrite?"
	 ' Yes/No button = 4, vBQuestion 32
     nBtn = MsgBox(strMessage,36,strGInputPanelTitle)
	 If ( nBtn = 7 ) Then
	   intGCancel = 1
	 End If
     Exit Sub
  End If


End Sub '/////////////////////////////////////////////// 


'====================================================================================
Sub CATMain ()
'====================================================================================
intGCancel = 0


GetReportFile

If ( intGCancel = 0 ) Then
   StartCATIAV5
End If

End Sub '/////////////////////////////////////////////////////////// CATMain
