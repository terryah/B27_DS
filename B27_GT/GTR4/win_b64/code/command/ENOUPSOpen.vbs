' This script triggers the 3DExperience Open panel in CATIA V5.
' The panel will be prefilled by the information of the 3DExperience object referred by the input physical ID.
'
' usage:
' wscript ENOUPSOpen.vbs /phid=...
' 
' This script assumes CATIA is started and connected to relevant 3DExperience server.

' Customization:
' This script should not be modified within CATIA V5 installation itself.
' It should only be modified inside customer specific environment.
'

' This subfunction displays specific error message
Sub DisplayError (iError)
Dim oError
oError = Replace(iError, "\n", vbCrLf)
MsgBox oError, vbOKOnly+vbCritical, "3DExperience"
End Sub

On Error Resume Next

' Get input physical ID
Dim iPhID
iPhID = WScript.Arguments.Named.Item("phid")

' Get running CATIA V5
' NB: CATIA V5 should be registered
' Care to potential conflict if CATIA V6 is running
Dim CATIA
Set CATIA = GetObject(,"CATIA.Application")
If (err Or CATIA Is Nothing) Then
    DisplayError "CATIA.Application : " & err.Description
    WScript.Quit
End If

' Get 3DExperience Integration Engine
Dim PLMXEngine
Set PLMXEngine = CATIA.GetItem("PLMXEngine")
If err Then
    DisplayError err.Description
    WScript.Quit
End If
    	
' Create Object to open
Dim Root1
Set Root1 = PLMXEngine.GetObjectFromPhysicalID(iPhID)
If err Then
    DisplayError err.Description
    WScript.Quit
End If

' Create Open Operation
Dim OpenOperation01
Set OpenOperation01 = PLMXEngine.CreateOpenOperation()
If err Then
    DisplayError err.Description
    WScript.Quit
End If

' Add object to open
OpenOperation01.AddRoot(Root1)
If err Then
    DisplayError err.Description
    WScript.Quit
End If

' Perform Open
OpenOperation01.Run()
If err Then
    DisplayError err.Description
    WScript.Quit
End If
