' This script triggers the 3DExperience Open panel in CATIA V5.
' The panel will be prefilled by the information of the 3DExperience object referred by the input physical ID.
'
' usage:
' wscript ENOCD5Open.vbs /phid=...
' 
' This script assumes CATIA is started and connected to relevant 3DExperience server.

' Customization:
' This script should not be modified within CATIA V5 installation itself.
' It should only be modified inside customer specific environment.
'

' This subfunction displays specific error message
Sub DisplayError (iError)
Dim oError
oError = Replace(iError, "\n", vbCrLf)
MsgBox oError, vbOKOnly+vbCritical, "3DExperience"
End Sub

On Error Resume Next

' Get input physical ID
Dim iPhID
iPhID = WScript.Arguments.Named.Item("phid")

' Get running CATIA V5
' NB: CATIA V5 should be registered
' Care to potential conflict if CATIA V6 is running
Dim CATIA
Set CATIA = GetObject(,"CATIA.Application")
If (err Or CATIA Is Nothing) Then
    DisplayError "CATIA.Application : " & err.Description
    WScript.Quit
End If

' Get 3DExperience Integration Engine
Dim CD5Engine
Set CD5Engine = CATIA.GetItem("CD5EngineV6R2015")
If err Then
    DisplayError err.Description
    WScript.Quit
End If
    	
' Get CD5ID from physical ID
Dim oID
Set oID = CD5Engine.GetIDFromPhysicalID(iPhID)
If err Then
    DisplayError err.Description
    WScript.Quit
End If

' Trigger Ineractive Open panel, prefilled with the input object information
CD5Engine.InteractiveOpen(oID)
If err Then
    DisplayError err.Description
    WScript.Quit
End If
