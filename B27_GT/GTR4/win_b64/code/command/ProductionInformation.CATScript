'// COPYRIGHT DASSAULT SYSTEMES  1999
'//============================================================================
'//
'// Language="VBSCRIPT"
'// Sample of macro to add and valuate attribute for data extraction
'//
'//============================================================================
'// It is advised that the user understand the VBScript and VBA concepts before
'// attempting to modify the code to suit their needs. Microsoft Excel 97
'// provides excellent documentation on the use of VBScript and VBA.
'// 
'// 
'//============================================================================

dim workbooks as AnyObject
dim workbook as AnyObject
dim strWB as Workbench
dim strServ as AnyObject

'//---------------------------------------------------------------------------
'// User customization of the attributes which will be added or valuated
'// tabAttribute is an array which defines the name of the attributes
'// typAttribute is an array which defines the type of the attributes
'// In this sample we provide 4 types but many others are available
'// cf Knowledgeware automation guide
'//---------------------------------------------------------------------------
dim nbAttributes as integer
nbAttributes = 4
dim tabAttributes(4)
dim typAttributes(4)

tabAttributes(1) = "DataString"
tabAttributes(2) = "DataReal"
tabAttributes(3) = "DataInteger"
tabAttributes(4) = "DataLength"

typAttributes(1) = "String"
typAttributes(2) = "Real"
typAttributes(3) = "Integer"
typAttributes(4) = "Length"

'//---------------------------------------------------------------------------
'// Create parameters value
'//---------------------------------------------------------------------------
Sub CreateParameters(iProduct)
   
   On Error Resume Next

   dim parameters as Parameters
   dim userParameters as Parameters
   dim param as Parameter
   dim paramName as string

   set parameters = iProduct.ReferenceProduct.Parameters
   set userParameters = iProduct.ReferenceProduct.UserRefProperties

   dim nbParameters As Integer
   nbParameters = RefParameters.Count

   dim sInstanceName As String
   dim sPartNumber As String
   sInstanceName = iProduct.Name
   sPartNumber = iProduct.ReferenceProduct.Name

   Set relations1 = iProduct.ReferenceProduct.Relations

   Err.Clear

   Dim member as StrMember
   Dim members as StrMembers
   Set members = iProduct.Parent.Parent.GetTechnologicalObject("StructureMembers")
   if (Not(members Is Nothing)) then
	set member = members.Item(sInstanceName)
	if (Err.Number <> 0) then
		set member = Nothing
      end if
   end if

   Err.Clear

   if (member Is Nothing) then
   	Dim plate As StrPlate 
   	Dim plates as StrPlates  	
   	Set plates = iProduct.Parent.Parent.GetTechnologicalObject("StructurePlates")
   	if (Not(plates Is Nothing)) then
		set plate = plates.Item(sInstanceName)
		if (Err.Number <> 0) then
			set plate = Nothing
		end if
      end if
   end if

   dim lfound as Boolean
   dim i As Integer
   if (nbAttributes > 0 AND (Not(member Is Nothing) OR Not(plate Is Nothing))) then
	  for i = 1 to nbAttributes

            Err.Clear

		paramName = sPartNumber & "\Properties\" & tabAttributes(i)

'// We find if the attribute already exist
		lfound = false
            for j = 1 To userParameters.Count
			if (userParameters.Item(j).Name = paramName) then
				set param = userParameters.Item(j)
		   		lfound = true
                  end if
            next 				 
				
   		if (lfound = true) then
'// Update of the attribute value if needed
'// In this sample we modify the value using param.Value
			if (typAttributes(i) = "String") then
			Elseif (typAttributes(i) = "Length") then
				param.Value = 200
			Elseif (typAttributes(i) = "Real") then
				param.Value = 400.0
			Elseif (typAttributes(i) = "Integer") then
				param.Value = 50
           		end if
   		else
'// Creation of the attributes according to its type
'// If you need to create other type of attribute
'// you can use the mode Record to record a macro interactively and edit it to see the
'// synthax to be used. 
			if (typAttributes(i) = "String") then
				set param = userParameters.CreateString(tabAttributes(i), "TEST")
				Dim formula1 As Formula
				if (Not(member Is Nothing)) then
					Set formula1 = relations1.CreateFormula("Formula.2", "", param, "MemberType")
					formula1.Rename "Formula.2"
				Elseif (Not(plate Is Nothing)) then
					Set formula1 = relations1.CreateFormula("Formula.2", "", param, "PlateType")
					formula1.Rename "Formula.2"
				end if
			Elseif (typAttributes(i) = "Length") then
				set param = userParameters.CreateDimension(tabAttributes(i), "LENGTH", 0.000000)
				param.Value = 100
			Elseif (typAttributes(i) = "Real") then
				set param= userParameters.CreateReal(tabAttributes(i), 0.000000)
				param.Value = 200.0
			Elseif (typAttributes(i) = "Integer") then
				set param = userParameters.CreateInteger(tabAttributes(i), 0)
				param.Value = 10
           		end if
		end if
	  Next
   end if
End Sub

'//---------------------------------------------------------------------------
'// Main
'//---------------------------------------------------------------------------
Sub CATMain()
   On Error Resume Next

   dim ActiveDoc As Document
   Set ActiveDoc = CATIA.ActiveDocument 

   If (InStr(oActiveDoc.Name,".CATProduct")) <> 0 Then
   	Dim ProductList As Products
	Set ProductList = ActiveDoc.Product.Products 

	msgbox "Active product: " & ActiveDoc.Product.Name

'// We call the sub routine CreateParameters defined above to define attributes on all
'// structural objects (members and plates) below the ActiveProduct.
	dim i as Integer	
	For i = 1 to ProductList.Count
		dim product as Product
            set product = ProductList.Item(i)
	      CreateParameters(product)
      Next
   end if

End Sub 
