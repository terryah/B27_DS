# COPYRIGHT DASSAULT SYSTEMES 2002
#==============================================================================
#
# CATTTRSModel.dic
#
#==============================================================================
# Usage notes:
#
#==============================================================================
# Jul. 2002  Creation                                                  S. Veron
#==============================================================================

#--------------------------------------------------------------------------------
# TTRSSet
#--------------------------------------------------------------------------------
CATEarlyDataFactoryRGEComponent       CATICreateInstance    libCATTTRSModel
CATEarlyDataFactoryRGEInteger         CATICreateInstance    libCATTTRSModel
CATEarlyDataFactoryRGESpecObject      CATICreateInstance    libCATTTRSModel
CATEarlyDataFactoryCATIAReleaseBloc   CATICreateInstance    libCATTTRSModel
CATEarlyDataFactoryRGEBlocMath        CATICreateInstance    libCATTTRSModel
CATEarlyDataFactoryRGEGSMCurve        CATICreateInstance    libCATTTRSModel
CATEarlyDataFactoryRGEGeom            CATICreateInstance    libCATTTRSModel
CATEarlyDataFactoryTTRSGeomCGMId      CATICreateInstance    libCATTTRSModel
CATEarlyDataFactoryTTRSGeomCGMIdPart  CATICreateInstance    libCATTTRSModel
CATEarlyDataFactoryTTRSLocalAxis      CATICreateInstance    libCATTTRSModel
CATEarlyDataFactoryTTRSRespec         CATICreateInstance    libCATTTRSModel

#--------------------------------------------------------------------------------
# CATMfTTRS
#--------------------------------------------------------------------------------
CATMfTTRS                   CATICreateInstance                 libCATTTRSModel
CATMfTTRS                   CATITTRS                           libCATTTRSModel
CATMfTTRS                   LifeCycleObject                    libCATTTRSModel
CATMfTTRS                   CATIUpgrade                        libCATTTRSModel
CATMfTTRS                   CATIBuild                          libCATTTRSModel
CATMfTTRS                   CATITTRSPriv                       libCATTTRSModel
CATMfTTRS                   CATITTRSLocalAxisSystem            libCATTTRSModel

#--------------------------------------------------------------------------------
# TTRS Classes
#--------------------------------------------------------------------------------
CATMfSphericalTTRS          CATICreateInstance                 libCATTTRSModel
CATMfSphericalTTRS          CATITTRS                           libCATTTRSModel
CATMfPlanarTTRS             CATICreateInstance                 libCATTTRSModel
CATMfPlanarTTRS             CATITTRS                           libCATTTRSModel
CATMfCylindricalTTRS        CATICreateInstance                 libCATTTRSModel
CATMfCylindricalTTRS        CATITTRS                           libCATTTRSModel
CATMfRevoluteTTRS           CATICreateInstance                 libCATTTRSModel
CATMfRevoluteTTRS           CATITTRS                           libCATTTRSModel
CATMfPrismaticTTRS          CATICreateInstance                 libCATTTRSModel
CATMfPrismaticTTRS          CATITTRS                           libCATTTRSModel
CATMfComplexTTRS            CATICreateInstance                 libCATTTRSModel
CATMfComplexTTRS            CATITTRS                           libCATTTRSModel

#--------------------------------------------------------------------------------
# CATMFRGE
#--------------------------------------------------------------------------------
CATMFRGE                    CATICreateInstance                 libCATTTRSModel
CATMFRGE                    CATIRGE                            libCATTTRSModel
CATMFRGE                    CATIRGETopology                    libCATTTRSModel
CATMFRGE                    CATIFTARGETopology                 libCATTTRSModel
CATMFRGE                    CATIBuild                          libCATTTRSModel
CATMFRGE                    LifeCycleObject                    libCATTTRSModel
CATMFRGE                    CATIUpgrade                        libCATTTRSModel
CATMmrRGE                   CATIFTAManageRGEInfo               libCATTTRSModel

#--------------------------------------------------------------------------------
# RGE Classes
#--------------------------------------------------------------------------------
CATMFComplexRGE             CATICreateInstance                 libCATTTRSModel
CATMFComplexRGE             CATIRGE                            libCATTTRSModel
CATMFComplexRGE             CATIRGETopology                    libCATTTRSModel
CATMFComplexRGE             CATIFTARGETopology                 libCATTTRSModel
CATMFCylindricalRGE         CATICreateInstance                 libCATTTRSModel
CATMFCylindricalRGE         CATIRGE                            libCATTTRSModel
CATMFCylindricalRGE         CATIRGETopology                    libCATTTRSModel
CATMFCylindricalRGE         CATIFTARGETopology                 libCATTTRSModel
CATMFPlanarRGE              CATICreateInstance                 libCATTTRSModel
CATMFPlanarRGE              CATIRGE                            libCATTTRSModel
CATMFPlanarRGE              CATIRGETopology                    libCATTTRSModel
CATMFPlanarRGE              CATIFTARGETopology                 libCATTTRSModel
CATMFPrismaticRGE           CATICreateInstance                 libCATTTRSModel
CATMFPrismaticRGE           CATIRGE                            libCATTTRSModel
CATMFPrismaticRGE           CATIRGETopology                    libCATTTRSModel
CATMFPrismaticRGE           CATIFTARGETopology                 libCATTTRSModel
CATMFRevoluteRGE            CATICreateInstance                 libCATTTRSModel
CATMFRevoluteRGE            CATIRGE                            libCATTTRSModel
CATMFRevoluteRGE            CATIRGETopology                    libCATTTRSModel
CATMFRevoluteRGE            CATIFTARGETopology                 libCATTTRSModel
CATMFSphericalRGE           CATICreateInstance                 libCATTTRSModel
CATMFSphericalRGE           CATIRGE                            libCATTTRSModel
CATMFSphericalRGE           CATIRGETopology                    libCATTTRSModel
CATMFSphericalRGE           CATIFTARGETopology                 libCATTTRSModel
CATMFRGEExtension           CATIRGE                            libCATTTRSModel
CATMFRGEExtension           CATIAlias                          libCATTTRSModel
CATMFRGEExtension           CATICstChanged                     libCATTTRSModel
CATMFRGEExtension           LifeCycleObject                    libCATTTRSModel
CATMFRGEExtension           CATITTRSSubscriber                 libCATTTRSModel
CATMFRGEExtension           CATIBuild                          libCATTTRSModel
CATMFRGEExtension           CATITTRSWorkModeStatus             libCATTTRSModel
CATMFRGEExtension           CATITTRSLocalAxisSystem            libCATTTRSModel
CATMFRGEExtension           CATIRGEBroken                      libCATTTRSModel

#--------------------------------------------------------------------------------
# CATMmrRGE
#--------------------------------------------------------------------------------
CATMmrRGE                   CATITTRSSubscriber                 libCATTTRSModel      CATNewRGE
CATMmrRGE                   CATICreateInstance                 libCATTTRSModel
CATMmrRGE                   CATIRGEFTAServices                 libCATTTRSModel
CATMmrRGE                   CATIRGE                            libCATTTRSModel
CATMmrRGE                   CATIRGETopology                    libCATTTRSModel
CATMmrRGE                   CATIFTARGETopology                 libCATTTRSModel
CATMmrRGE                   LifeCycleObject                    libCATTTRSModel
CATMmrRGE                   CATIBuild                          libCATTTRSModel
CATMmrRGE                   CATITTRSWorkModeStatus             libCATTTRSModel
CATMmrRGE                   CATICstChanged                     libCATTTRSModel      CATNewRGE
CATMmrRGE                   CATITTRSLocalAxisSystem            libCATTTRSModel
CATMmrRGE                   CATIReplace                        libCATTTRSModel
CATMmrRGE                   CATIReplacable                     libCATTTRSModel
CATMmrRGE                   CATIReplaceUI                      libCATTTRSModel
CATMmrRGE                   CATITechnoLinkableCtorToProd       libCATTTRSModel
# RHZ(07/06/12) FTA New Associativity (delegate Listener/Dispatcher patern):
CATMmrRGE                   CATIMmiDelegateListener            libCATTTRSModel
CATMmrRGE                   CATIRGEInternal                    libCATTTRSModel
# CVQ(08/01/24) RGE Upgrade to TTRSSet
CATMmrRGE                   CATIMigrateRGE                     libCATTTRSModel      CATTPSConditionMigrateRGE
CATMmrRGE                    CATIUpgrade                       libCATTTRSModel      CATTPSConditionUpgradeRGE

#--------------------------------------------------------------------------------
# CATMmrTTRS
#--------------------------------------------------------------------------------
CATMmrTTRS                  CATICreateInstance                 libCATTTRSModel
CATMmrTTRS                  CATITTRS                           libCATTTRSModel
CATMmrTTRS                  CATITTRSAdvanced                   libCATTTRSModel
CATMmrTTRS                  LifeCycleObject                    libCATTTRSModel
CATMmrTTRS                  CATIBuild                          libCATTTRSModel
CATMmrTTRS                  CATITTRSPriv                       libCATTTRSModel
CATMmrTTRS                  CATIReplace                        libCATTTRSModel
CATMmrTTRS                  CATITTRSCanonicityRespec           libCATTTRSModel
CATMmrTTRS                  CATITTRSWorkModeStatus             libCATTTRSModel
CATMmrTTRS                  CATITTRSLocalAxisSystem            libCATTTRSModel
CATMmrTTRS                  CATIFTATTRS                           libCATTTRSModel

#--------------------------------------------------------------------------------
# CATTTRSVersionningServices
#--------------------------------------------------------------------------------
CATTTRSVersionningServices  CATICreateInstance                 libCATTTRSModel
CATTTRSVersionningServices  CATITTRSVersionningServices        libCATTTRSModel

#--------------------------------------------------------------------------------
# CATTTRSSetManagementServices
#--------------------------------------------------------------------------------
CATTTRSSetManagementServices  CATICreateInstance                 libCATTTRSModel
CATTTRSSetManagementServices  CATITTRSSetManagementServices      libCATTTRSModel

#--------------------------------------------------------------------------------
# CATPrtCont
#--------------------------------------------------------------------------------
CATPrtCont                  CATIRGEFactory                     libCATTTRSModel
CATPrtCont                  CATITTRSFactory                    libCATTTRSModel
CATPrtCont                  CATITTRSSetFactory                 libCATTTRSModel

#--------------------------------------------------------------------------------
# CATTTRSCont
#--------------------------------------------------------------------------------
CATTTRSCont                 CATIRGEFactory                     libCATTTRSModel
CATTTRSCont                 CATITTRSFactory                    libCATTTRSModel
CATTTRSCont                 CATITTRSSetFactory                 libCATTTRSModel

#--------------------------------------------------------------------------------
# CATPart
#--------------------------------------------------------------------------------
CATPart                     CATITTRSDocument                   libCATTTRSModel

#--------------------------------------------------------------------------------
# CATProduct
#--------------------------------------------------------------------------------
CATProduct                  CATITPSTopologyV4                  libCATTTRSModel
CATProduct                  CATITTRSDocument                   libCATTTRSModel

#--------------------------------------------------------------------------------
# CATProcess
#--------------------------------------------------------------------------------
CATProcess                  CATITPSTopologyV4                  libCATTTRSModel
CATProcess                  CATITTRSDocument                   libCATTTRSModel

#--------------------------------------------------------------------------------
# MechanicalPart
#--------------------------------------------------------------------------------
MechanicalPart              CATITTRSSetManager                 libCATTTRSModel

#--------------------------------------------------------------------------------
# ASMPRODUCT
#--------------------------------------------------------------------------------
ASMPRODUCT                  CATITTRSSetManager                 libCATTTRSModel

#--------------------------------------------------------------------------------
# TPSEngIntent
#--------------------------------------------------------------------------------
TPSEngIntent                CATITechnoLinkableCtorToProd       libCATTTRSModel

#--------------------------------------------------------------------------------
# TTRSSet
#--------------------------------------------------------------------------------
TTRSSet                     CATITTRSSet						             libCATTTRSModel
TTRSSet                     CATISpecObject					           libCATTTRSModel
TTRSSet                     CATIRGEFactory					           libCATTTRSModel 
TTRSSet                     CATITTRSFactory					           libCATTTRSModel
TTRSSet                     CATIAlias                          libCATTTRSModel
TTRSSet                     LifeCycleObject                    libCATTTRSModel
TTRSSet                     CATIDescendants                    libCATTTRSModel

#--------------------------------------------------------------------------------
# CATRGEAssociativityServices
# RHZ(07/06/12) FTA New Associativity:
#--------------------------------------------------------------------------------
CATRGEAssociativityServices  CATICreateInstance                libCATTTRSModel
CATRGEAssociativityServices  CATIRGEAssociativityServices      libCATTTRSModel


