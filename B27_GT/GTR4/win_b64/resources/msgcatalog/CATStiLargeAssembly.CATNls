//================================================================================
// NLS for Large Assembly.
// ===============================================================================

CreateNodeTree.Title = "Create WIP"; 
CreateNodeTree.Text = "There is some problem while gathering information from SmarTeam, during Tree Creation";

InstanceInfo.Title = "Create WIP"; 
InstanceInfo.Text = "For one or more Document(s) in WIP Tree, Instance information is not present in SmarTeam \n/p1";

DocInSession.Title = "Create WIP"; 
DocInSession.Text = "Following Document(s) selected for WIP is/are already present in CATIA session.\n/p1 Please close these documents and run Create WIP command again";

DocWithWIPNameInSession.Title = "Create WIP"; 
DocWithWIPNameInSession.Text = "Document with WIP name /p1 \nis already present in CATIA session.Please close this document and run Create WIP command again";

RootProduct.Title = "Create WIP"; 
RootProduct.Text = "Problem during creating Root product in CATIA:\n/p1.";

ErrorDuringCopyFile.Title = "Create WIP"; 
ErrorDuringCopyFile.Text = "Problem during copying file from SmarTeam to Work for following document.\n/p1";

ErrorDuringInsertingDoc.Title = "Create WIP"; 
ErrorDuringInsertingDoc.Text = "Problem during inserting following document(s) under WIP. \n/p1 \nWIP creation failed";

ErrorDuringSettingProp.Title = "Create WIP"; 
ErrorDuringSettingProp.Text = "Problem during Setting following component's properties on WIP. \n/p1 \nWIP creation failed";

InvalidWIPName.Title = "Create WIP"; 
InvalidWIPName.Text = "The Name provided for WIP \n/p1 is not valid  \nWIP creation failed";

ErrorDuringWIPCreation.Title = "Create WIP"; 
ErrorDuringWIPCreation.Text = "Error during creating WIP in CATIA";

ErrorDuringCopyFile1.Title = "Create WIP"; 
ErrorDuringCopyFile1.Text = "Problem during copying representation from SmarTeam for following document\n/p1 Please check that SmarTeam is configured correctly to generate CGRs during SmarTeam Save";

ErrorDuringCopyFile2.Title = "Create WIP"; 
ErrorDuringCopyFile2.Text = "Problem during copying representation from SmarTeam for following document\n/p1";

//
// ---------------------------------------------------------------------------
// Messages for Progress Indicator Dialog
// ---------------------------------------------------------------------------
//

ProgressIndicatorDlg.ProgressIndicator.Title     = "Create WIP in progress";
ProgressIndicatorDlg.ProgressIndicator.ShortHelp = "Creates WIP in CATIA";
ProgressIndicatorDlg.ProgressIndicator.LongHelp  = "Creates WIP in CATIA";

ProgressIndicatorDlg.DefaultTitle = "TASK IN PROGRESS";
ProgressIndicatorDlg.Title_WIP_Creation = "WIP Creation";
ProgressIndicatorDlg.Title_Insert_component = "Insert component in WIP";
ProgressIndicatorDlg.Info_SmarTeam = "Retrieving information from SmarTeam";
ProgressIndicatorDlg.Doc_In_Session = "Checking documents in CATIA session";
ProgressIndicatorDlg.Copy_Documents = "Copying documents from SmarTeam";
ProgressIndicatorDlg.Root_Document = "Creating root WIP product";
ProgressIndicatorDlg.Get_Root_Document = " Getting root WIP product";
ProgressIndicatorDlg.Insert_Component = "Inserting components and setting properties";
ProgressIndicatorDlg.Title_Synchronize = "Synchronize";
ProgressIndicatorDlg.Instance_Delete = "Removing  ";
ProgressIndicatorDlg.Instance_Update = "Updating  ";
ProgressIndicatorDlg.Instance_Add = "Inserting instance of  ";
ProgressIndicatorDlg.UndefinedTask = "UNDEFINED TASK";

//
// ---------------------------------------------------------------------------
// Messages for WIP Management
// ---------------------------------------------------------------------------
//

SwitchToDesignFailed.Title = "WIP Management : Switch Representation"; 
SwitchToDesignFailed.Text = "Switch to Design representation failed";

SwitchToLightFailed.Title = "WIP Management : Switch Representation"; 
SwitchToLightFailed.Text = "Switch to Light representation failed";

RefreshRepresentationFailed.Title = "WIP Management : Refresh Representation"; 
RefreshRepresentationFailed.Text = "Refresh Representation Failed";

InsertCompInWIPFailed.Title = "WIP Management : Insert Component";
InsertCompInWIPFailed.Text = "Inserting component in WIP failed";

ProblemReadingOMBProperties.Title = "WIP Management";
ProblemReadingOMBProperties.Text = "There is problem found in current WIP Tree in CATIA for componenet \n/p1 \nOperation aborted.";

InconsistentWIP.Title = "WIP Management";
InconsistentWIP.Text = "WIP Product is inconsistent because of following component(s) \n/p1\nTo solve this issue, Please use CAI Global Refresh and Refresh Light Representations."; 

DocumentsIgnoredForRefresh.Title = "WIP Management : Refresh Representation";
DocumentsIgnoredForRefresh.Text = "Following selected instance(s) will be ignored.\n\n/p1 Do you want to continue?";	

DocumentsIgnoredErrorForRefresh.Title = "WIP Management : Refresh Representation";
DocumentsIgnoredErrorForRefresh.Text = "All selected instances will be ignored.\n\n/p1";	

DocumentsIgnoredForSwitch.Title = "WIP Management : Switch Representation";
DocumentsIgnoredForSwitch.Text = "Following selected instance(s) will be ignored.\n\n/p1 Do you want to continue?";	

DocumentsIgnoredErrorForSwitch.Title = "WIP Management : Switch Representation";
DocumentsIgnoredErrorForSwitch.Text = "All selected instances will be ignored.\n\n/p1";	

ReplaceAll.Title = "WIP Management : Switch Representation";
ReplaceAll.Text = "Do you want to switch all instances?";

ExistingWIPDocsNotUnloadable.Title = "WIP Management : Insert Component"; 
ExistingWIPDocsNotUnloadable.Text = "Following components selected for insert are already present in WIP and are not Unloadable. \n/p1\nInsert operation aborted.";

ErrorDuringReplace.Title = "WIP Management";
ErrorDuringReplace.Text = "There is some error while replacing following components \n/p1";

ErrorDuringCopyFileForWIPManagement.Title = "WIP Management"; 
ErrorDuringCopyFileForWIPManagement.Text = "Problem during copying file from SmarTeam to Work for following document.\n/p1 \nOperation aborted.";

ErrorDuringSettingPropForWIPManagement.Title = "WIP Management"; 
ErrorDuringSettingPropForWIPManagement.Text = "Problem during setting following component's properties on WIP \n/p1 \nOperation aborted.";

NOTInWIP = "Reason: Out of WIP";
InvalidRepresentation = "Reason: Not valid representation";
Inconsistent = "Reason: Inconsistent";
NotUnloadable = "Reason: Not unloadable";
CheckedOut = "Reason: Latest revision is in Checked-out state";
ImmediateChild = "Reason: Not direct child of WIP";

InfoRefreshNothing.Title = "WIP Management : Refresh Representation";
InfoRefreshNothing.Text  = "Light Representation are at their latest revision.\nThere is nothing to refresh";

InfoRefreshDone.Title = "WIP Management";
InfoRefreshDone.Text  = "Refresh successful";

NotValidLightRep.Title = "WIP Management : Refresh Representation";
NotValidLightRep.Text = "Selection does not contain any valid light representation";

NoChildToRefresh.Title = "WIP Management : Refresh Representation";
NoChildToRefresh.Text  = "Selected WIP doesn't contain any child for refresh";

SomeLightRepDirtyWarning.Title = "WIP Management";
SomeLightRepDirtyWarning.Text = "There are some unsaved or modified components under Embedded Component(s) \n\n/p1 Please Save these components on disk or to SmarTeam as they will be removed.\nDo you want to continue operation without saving?";	

AllLightRepDirtyWarning.Title = "WIP Management";
AllLightRepDirtyWarning.Text = "There are some unsaved or modified components under all selected Embedded Component(s) \n\n/p1\nPlease Save these components on disk or to SmarTeam as they will be removed.\nDo you want to continue operation without saving?";	

SwitchOnDocumentsInCOState.Title = "WIP Management : Switch Representation";
SwitchOnDocumentsInCOState.Text = "Documents of following light representations are in Checked-out state.\n/p1\nDo you want to continue the operation?";

RefreshOnDocumentsInCOState.Title = "WIP Management : Refresh Representation";
RefreshOnDocumentsInCOState.Text  = "Latest revision(s)of following document(s) are in Checked out state.\n/p1\nThose will be refreshed as per their latest public revision.\nDo you want to continue the operation?";


//
// ---------------------------------------------------------------------------
// Messages for Synchronize WIP 
// ---------------------------------------------------------------------------
//

SynchronizeFailed.Title = "WIP Management : Synchronize"; 
SynchronizeFailed.Text = "WIP Synchronize failed";

SynchronizeSucceeded.Title = "WIP Management : Synchronize"; 
SynchronizeSucceeded.Text = "WIP Synchronize succeeded";

SynchronizeWithLatestRevisions.Title = "WIP Management : Synchronize";
SynchronizeWithLatestRevisions.Text = "Do you want to refresh components to their revisions under latest \n/p1 ? \nClick 'No' to refresh them to their individual latest revision.";

IncorrectLatestState.Title = "WIP Management : Synchronize";
IncorrectLatestState.Text = "The latest revision of \n/p1\nis in Checked out state, Operation aborted.";

ErrorDuringDelete.Title = "WIP Management";
ErrorDuringDelete.Text = "There is some error while removing following components \n/p1";

ErrorDuringInsertingInstance.Title = "WIP Management : Synchronize"; 
ErrorDuringInsertingInstance.Text = "Problem during inserting following document(s) under WIP. \n/p1";

WIPWithOldVersion.Title = "WIP Management";
WIPWithOldVersion.Text = "Selected WIP - /p1 is not eligible for this command.\nBecause it is created with earlier version of CATIA.";

DocumentsIgnoredForSynchronize.Title = "WIP Management : Synchronize";
DocumentsIgnoredForSynchronize.Text = "Synchronize interrupted due to following document(s) \n/p1";	

CheckedOutDocumentsForSynchronize.Title = "WIP Management : Synchronize";
CheckedOutDocumentsForSynchronize.Text = "Latest revision(s)of following document(s) are in Checked out state.\n/p1Those will be synchronized as per their latest public revisions.\nDo you want to continue the operation?";	

IncorrectLatestState_Warning.Title = "WIP Management : Synchronize";
IncorrectLatestState_Warning.Text = "The latest revision of \n/p1\nis in Checked out state.\nDo you want to Synchronize WIP as per its latest public revision?";


















	


