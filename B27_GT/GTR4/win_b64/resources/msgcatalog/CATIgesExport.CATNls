//NLS-ENGLISH VERSION
//=======================================================================
// MODULE :  IgesDataExchange
//=======================================================================


// KVL 2002-06-04 : Creation de CATIgesExport.CATNls et refonte des anciens CATNls Iges.
// DFB 2002-03-07 : Ajout de CircleReparam 
// KVL 2002-04-15 : CATIgesCompositeCurve  
// KVL 2002-05-30 : CATIgesInit : info sur le choix des options BSpline ou Standard � l'export 
// KVL 2002-06-07 : CATIgesInit : info sur le choix de l'export des entit�s en NoShow et des options wireframe ou Surfacic
// KVL 2002-06-19 : CATIgesVirtualBody et CATIgesElement : Info sur chaque �l�ment cr�e (Type, Show, Layer, Name, Color)
// KVL 2002-07-03 : CATIgesCurveOnParametricSurface : �chec de cr�ation des PCurve.
// OPC 2006-06-23 : Reecriture des messages niveau Customer pour meilleure compr�hension

// Numeros utilises : (export Iges : de [6001] a [6999]
// [6001] a [6049] : CATIgesBSplineCurve
// [6050] a [6099] : CATIgesCompositeCurve
// [6100] a [6149] : CATIgesInit
// [6150] a [6199] : CATIgesVirtualBody
// [6200] a [6249] : CATIgesElement
// [6250] a [6299] : CATIgesCurveOnParametricSurface
// [6300] a [6349] : CATIgesTrimmedParametricSurface
// [6350] a [6370] : CATSaveAsIGS
// [6850] a [6860] : CATIgesInterface

//----------------------------------------------
//  Messages from CATIgesBSplineCurve
//---------------------------------------------

CATIgesBSplineCurve.Info.CircleReparam=
"   <I> [6001] [T=/p2] [/p1] The exported circle is reparametrised /p3";

CATIgesBSplineCurve.Info.ParamInverse=
"   <I> [6002] [T=/p2] [/p1] The BSpline parameters are inverted /p3";

CATIgesBSplineCurve.Info.BSplineFermee=
"   <I> [6003] [T=/p2] [/p1] The BSpline is closed /p3";

//-----------------------------------------------
//  Messages from CATIgesCompositeCurve
//-----------------------------------------------

CATIgesCompositeCurve.Info.hierar=
"   <I> [6050] [T=/p2] [#/p4] All entities depending on this entity will inherit its attributes /p3";
 
 

//-----------------------------------------------
//  Messages from CATIgesInit
//-----------------------------------------------

CATIgesInit.Info.optionBSpline=
"   <I> [6100] You have selected the BSpline export option /p3 \n"; 

CATIgesInit.Info.optionStandard=
"   <I> [6101] You have selected the Standard export option /p3 \n";

CATIgesInit.Info.optionNoShowRef=
"   <I> [6102] You have chosen not to export NoShow entities /p3 \n"; 

CATIgesInit.Info.optionNoShowAcc=
"   <I> [6103] You have chosen to export NoShow entities /p3 \n";

CATIgesInit.Info.optionWireframe=
"   <I> [6104] You have selected the Wireframe export option /p3 \n";

CATIgesInit.Info.optionSurfacic=
"   <I> [6105] You have selected the Surfacic export option /p3 \n";

CATIgesInit.Info.optionMSBO=
"   <I> [6106] You have selected the MSBO Export Option (for Solids, Shells and Faces) /p3 \n";


//-----------------------------------------------
//  Messages from CATIgesVirtualBody (messages utilises parfois dans CATIgesCldBody)
//-----------------------------------------------

CATIgesVirtualBody.Info.ProcessingBody=
"\n***PROCESSING BODY ";

CATIgesVirtualBody.Info.ProcessingElement=
"\n **PROCESSING ELEMENT [T=/p2] [#/p1]";

CATIgesVirtualBody.Info.Name=
"   <I> [6150] [T=/p2] [#/p1] The V5 entity name is /p3";

CATIgesVirtualBody.Info.NoName=
"   <I> [6151] [T=/p2] [#/p1] No name has been specified for this entity.";

CATIgesVirtualBody.Info.NbElem=
"   <I> [6152] The body is composed by /p2 /p1 /p3";

CATIgesVirtualBody.Info.Wire=
"   <I> [6153] [T=/p2] [#/p1] The wire is closed";

CATIgesVirtualBody.Error.ElementNotExported=
"!! <E> [6154] [T=/p2] [#/p1] Impossible to export this element (due to invalid input data). /p3";


//-----------------------------------------------
//  Messages from CATIgesElement
//-----------------------------------------------

CATIgesElement.Info.NoShow=
"   <I> [6200] [T=/p2] [#/p1] NoShow entity";

CATIgesElement.Info.Layer=
"   <I> [6201] [T=/p2] [#/p1] The layer is /p3";

CATIgesElement.Info.Color=
"   <I> [6202] [T=/p2] [#/p1] The entity color is R=/p3 G=/p4 B=/p5";

CATIgesElement.Warning.IgsControlName=
"   <W> [6203] The entity name is invalid : /p1 character(s) not allowed met on write access. A cleaning attempt is done";

//-----------------------------------------------
//  Messages from CATIgesCurveOnParametricSurface
//-----------------------------------------------
CATIgesCurveOnParametricSurface.Warning.PCurveNotCreated=
"   <W> [6250] [T=/p2] [#/p1] PCurve IGES not created /p3";


//-----------------------------------------------
//  Messages from CATIgesTrimmedParametricSurface
//-----------------------------------------------
CATIgesTrimmedParametricSurface.Warning.PCurveNotExported=
"   <W> [6300] [T=/p2] [#/p1] PCurve IGES not exported /p3";

//-----------------------------------------------
//  Messages from CATSaveAsIGS
//-----------------------------------------------
CATSaveAsIGS.TranscriptChild.NoChild=
"   <W> [6350] no child has been found ! ";
CATSaveAsIGS.Error.ConvertV4ToV5=
"   <E> [6351] Fails to convert V4 model to CATPart ! ";
CATSaveAsIGS.Info.ConvertV4ToV5=
"   <I> [6352] The V4 model named : /p1 is converted";

//--------------------------------------
//  Messages from CATIgesInterface
//--------------------------------------

CATDXFileServices.Error.InvalidName = 
"!! <E> [6859] The name of the IGES file to save is not valid (error PathERR_1021):
               The file name contains non-ASCII characters or is empty.
               Check the file name for any invalid character. Choose another name if necessary then try to save it again.";


//------------------------------------------
//  Messages for CATIReporter 
//------------------------------------------

PathERR_1021.Request="The name of the IGES file to save is not valid." ;
PathERR_1021.Diagnostic="The file name contains non-ASCII characters or is empty." ;
PathERR_1021.Advice="Check the file name for any invalid character. Choose another name if necessary then try to save it again." ;

