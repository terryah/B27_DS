// COPYRIGHT DASSAULT SYSTEMES 2002
//===========================================================================
//
// CATRmaShaderCarPaintFrm (English)
//
//===========================================================================

paintColorLabel.Title = "Paint Color ";
paintColorSpinner.Help = "Modify paint color";
paintColorSpinner.ShortHelp = "Paint Color";
paintColorSpinner.LongHelp =
"Modulates the paint color.";

particleIntensityLabel.Title = "Particle Brightness ";
particleIntensitySlider.Help = "Modify particle brightness";
particleIntensitySlider.ShortHelp = "Particle Brightness";
particleIntensitySlider.LongHelp =
"Modulates the particle brightness.";

densityLabel.Title = "Particle Density ";
densitySpinner.Help = "Modify particle density";
densitySpinner.ShortHelp = "Particle Density";
densitySpinner.LongHelp =
"Modulates the particle density.";

reflectivityLabel.Title = "Reflectivity ";
reflectivitySpinner.Help = "Modify paint reflectivity";
reflectivitySpinner.ShortHelp = "Paint Reflectivity";
reflectivitySpinner.LongHelp =
"Modulates the reflectivity of the material.";

shininessLabel.Title = "Roughness ";
shininessSpinner.Help = "Modify paint dullness";
shininessSpinner.ShortHelp = "Paint Roughness";
shininessSpinner.LongHelp =
"Modulates the bright area size of the paint material.";

DiffuseStrength.Title = "Diffuse Strength ";
DiffuseStrengthSpinner.Help = "Modify Diffuse Strength";
DiffuseStrengthSpinner.ShortHelp = "Diffuse Strength";
DiffuseStrengthSpinner.LongHelp =
"Modify the diffuse strength of the paint material.";

SpecularStrength.Title = "Specular Strength ";
SpecularStrengthSpinner.Help = "Modify Diffuse Strength";
SpecularStrengthSpinner.ShortHelp = "Specular Strength";
SpecularStrengthSpinner.LongHelp =
"Modify the specular strength of the paint material.";

SpecularExpon.Title = "Specular Expon ";
SpecularExponSpinner.Help = "Modify Specular Expon";
SpecularExponSpinner.ShortHelp = "Specular Expon";
SpecularExponSpinner.LongHelp =
"Modify the specular expon of the paint material.";

ReflectionStrength.Title = "Reflection Strength ";
ReflectionStrengthSpinner.Help = "Modify Reflection Strength";
ReflectionStrengthSpinner.ShortHelp = "Reflection Strength";
ReflectionStrengthSpinner.LongHelp =
"Modify the reflection strength of the paint material.";

FresnelReflectivityMax.Title = "Fresnel Reflectivity Max ";
FresnelReflectivityMaxSpinner.Help = "Modify Fresnel Reflectivity maximum limit";
FresnelReflectivityMaxSpinner.ShortHelp = "Fresnel Reflectivity maximum limit";
FresnelReflectivityMaxSpinner.LongHelp =
"Modify the Fresnel Reflectivity maximum limit of the paint material.";

FresnelReflectivityMin.Title = "Fresnel Reflectivity Min ";
FresnelReflectivityMinSpinner.Help = "Modify Fresnel Reflectivity minimum limit";
FresnelReflectivityMinSpinner.ShortHelp = "Fresnel Reflectivity minimum limit";
FresnelReflectivityMinSpinner.LongHelp =
"Modify the Fresnel Reflectivity minimum limit of the paint material.";

FresnelExpon.Title = "Fresnel Expon ";
FresnelExponSpinner.Help = "Modify Fresnel Exponent";
FresnelExponSpinner.ShortHelp = "Fresnel Exponent";
FresnelExponSpinner.LongHelp =
"Modify the fresnel exponent of the paint material.";

BumpHeight.Title = "Bump Height ";
BumpHeightSpinner.Help = "Modify Bump Height";
BumpHeightSpinner.ShortHelp = "Bump Height";
BumpHeightSpinner.LongHelp =
"Modify the bump height of the paint material.";

BumpRepeatU.Title = "Bump Repeat U ";
BumpRepeatUSpinner.Help = "Modify Bump Repeat U";
BumpRepeatUSpinner.ShortHelp = "Bump Repeat U";
BumpRepeatUSpinner.LongHelp =
"Modify the bump repeat U of the paint material.";

BumpRepeatV.Title = "Bump Repeat V ";
BumpRepeatVSpinner.Help = "Modify Bump Repeat V";
BumpRepeatVSpinner.ShortHelp = "Bump Repeat V";
BumpRepeatVSpinner.LongHelp =
"Modify the bump repeat V of the paint material.";

BrdfStrength.Title = "Brdf Strength ";
BrdfStrengthSpinner.Help = "Modify Brdf Strength";
BrdfStrengthSpinner.ShortHelp = "Brdf Strength";
BrdfStrengthSpinner.LongHelp =
"Modify the brdf strength of the paint material.";

BrdfExpon.Title = "Brdf Expon ";
BrdfExponSpinner.Help = "Modify Brdf Exponent";
BrdfExponSpinner.ShortHelp = "Brdf Exponent";
BrdfExponSpinner.LongHelp =
"Modify the brdf exponent of the paint material.";


EnvironmentContrast.Title = "Environment Contrast ";
EnvironmentContrastSpinner.Help = "Modify Environment Contrast";
EnvironmentContrastSpinner.ShortHelp = "Environment Contrast";
EnvironmentContrastSpinner.LongHelp =
"Modify the environment contrast of the paint material.";

EnvironmentSaturation.Title = "Environment Saturation ";
EnvironmentSaturationSpinner.Help = "Modify Environment Saturation";
EnvironmentSaturationSpinner.ShortHelp = "Environment Saturation";
EnvironmentSaturationSpinner.LongHelp =
"Modify the environment saturation of the paint material.";

AmbientColor.Title = "Ambient Color ";
AmbientColorSpinner.Help = "Modify ambient color";
AmbientColorSpinner.ShortHelp = "ambient Color";
AmbientColorSpinner.LongHelp =
"Modulates the ambient color.";

DiffuseColor.Title = "Diffuse Color ";
DiffuseColorSpinner.Help = "Modify diffuse color";
DiffuseColorSpinner.ShortHelp = "diffuse Color";
DiffuseColorSpinner.LongHelp =
"Modulates the diffuse color.";

SpecularColor.Title = "Specular Color ";
SpecularColorSpinner.Help = "Modify specular color";
SpecularColorSpinner.ShortHelp = "Specular Color";
SpecularColorSpinner.LongHelp =
"Modulates the specular color.";

BrdfColor.Title = "Brdf Color ";
BrdfColorSpinner.Help = "Modify brdf color";
BrdfColorSpinner.ShortHelp = "Brdf Color";
BrdfColorSpinner.LongHelp =
"Modulates the brdf color.";

PaintMap.Title = "Paint Map ";
ReflectionMap.Title = "Reflection Map ";
NormalMap.Title = "Normal Map ";

warningLabel.Title = "Warning: Parameters for real time rendering only";
