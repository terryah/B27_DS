//----------------------------------------------
// Resource file for CATGSMUIFillPanel class
// En_US
//----------------------------------------------
Title="Fill Surface Definition";
FraFill.LabListCrv.Title="Boundary:";

// number of the selected elements
NumberColumnTitle="No.";

CurvesColumn="Curves";
SupportsColumn="Supports";
ContinuityColumn="Continuity";

FraFill.LabContinuityType.Title="Continuity:";
PointType="Point";
TangentType="Tangent";
CurvatureType="Curvature";

FraFill.FraFillButton.AddAfterButton.Title="AddAfter";
FraFill.FraFillButton.RemoveButton.Title="Remove";
FraFill.FraFillButton.ReplaceButton.Title="Replace";
FraFill.FraFillButton.AddBeforeButton.Title="AddBefore";
FraFill.FraFillButton.RemoveSupportButton.Title="RemoveSupport";
FraFill.FraFillButton.ReplaceSupportButton.Title="ReplaceSupport";

FraConstraint.LabConstraint.Title="Passing element(s):";


FraFill.LabListCrv.LongHelp=
"Specifies the curves, surface edges and support surfaces 
used to form the closed boundary of the fill surface.";
FraFill.FraList.LongHelp=
"List the curves, surface edges and support surfaces 
used to form the closed boundary of the fill surface.";
FraFill.LabContinuityType.LongHelp=
"Lists the available continuity types between
the support surfaces and the fill surface.";
FraFill.CmbContinuityType.LongHelp=
"Specifies the continuity types between the support 
 surfaces and the fill surface.";

TabContBoundaries.TabOuterBoundary.Title="Outer Boundaries";
TabContBoundaries.TabOuterBoundary.LongHelp=
"Select this tab to specify the curves, surface edges and support surfaces 
used to form the 'Outer' closed boundary of the fill surface.";
TabContBoundaries.TabInnerBoundary.Title="Inner Boundaries";
TabContBoundaries.TabInnerBoundary.LongHelp=
"Select this tab to specify the curves, surface edges and support surfaces 
used to form the 'Inner' closed boundary(s) of the fill surface.";

TabContBoundaries.TabOuterBoundary.LabListCrv.Title=" ";
TabContBoundaries.TabOuterBoundary.LabListCrv.LongHelp=
"Specifies the curves, surface edges and support surfaces 
used to form the 'Outer' closed boundary of the fill surface.";
TabContBoundaries.TabOuterBoundary.FraList.LongHelp=
"List the curves, surface edges and support surfaces 
used to form the 'Outer' closed boundary of the fill surface.";

TabContBoundaries.TabInnerBoundary.LabListCrv.Title=" ";
TabContBoundaries.TabInnerBoundary.LabListCrv.LongHelp=
"Specifies the curves, surface edges and support surfaces 
used to form the 'Inner' closed boundary(s) of the fill surface.";
TabContBoundaries.TabInnerBoundary.FraList.LongHelp=
"List the curves, surface edges and support surfaces 
used to form the 'Inner' closed boundary(s) of the fill surface.";

TabContBoundaries.TabInnerBoundary.FraNavig.LabNavig.Title="Inner Boundary No: ";
TabContBoundaries.TabInnerBoundary.FraNavig.FraPrev.Title=" Previous ";
TabContBoundaries.TabInnerBoundary.FraNavig.FraPrev.LongHelp="Go to Previous Inner Boundary";
TabContBoundaries.TabInnerBoundary.FraNavig.FraNext.Title="   Next   ";
TabContBoundaries.TabInnerBoundary.FraNavig.FraNext.LongHelp="Go to Next Inner Boundary";
TabContBoundaries.TabInnerBoundary.FraNavig.FraSpinner.Title="Spinner";
TabContBoundaries.TabInnerBoundary.FraNavig.FraSpinner.LongHelp="Navigate through the Inner Boundaries";
TabContBoundaries.TabInnerBoundary.FraPush.FraAdd.Title =    "   Add New Boundary    ";
TabContBoundaries.TabInnerBoundary.FraPush.FraAdd.LongHelp =    "Adds a new inner boundary to the existing list.";
TabContBoundaries.TabInnerBoundary.FraPush.FraReomve.Title = "Remove Current Boundary";
TabContBoundaries.TabInnerBoundary.FraPush.FraReomve.LongHelp = "Removes current inner boundary.";

FraFill.FraFillButton.ReplaceButton.LongHelp="Allows you to replace the selected element by another element.";
FraFill.FraFillButton.RemoveButton.LongHelp="Allows you to remove the selected element from the list.";
FraFill.FraFillButton.AddAfterButton.LongHelp="Allows you to add a new element to the list after the selected element.";
FraFill.FraFillButton.ReplaceSupportButton.LongHelp="Allows you to replace the support of selected element by another support element.";
FraFill.FraFillButton.RemoveSupportButton.LongHelp="Allows you to remove the selected support element from the list.";
FraFill.FraFillButton.AddBeforeButton.LongHelp="Allows you to add a new element to the list before the selected element.";

FraConstraint.LabConstraint.LongHelp=
"Element used as a passing constraint for the fill surface.";
FraConstraint.Constraint.LongHelp=
"Select Element(s) used as passing constraint(s) for the fill surface.";
FraDir.CheckOption.Title="Planar Boundary Only";
FraDir.CheckOption.LongHelp="Only planar boundaries are taken into account.";

FraDev.CheckDev.Title = "Deviation: ";
FraDev.DevValueFr.EnglobingFrame.IntermediateFrame.Spinner.Title="Deviation";
FraDev.DevValueFr.EnglobingFrame.IntermediateFrame.Spinner.LongHelp="Specifies the Tolerance Deviation value.\nYou can also use the contextual menu to specify formula.";

FraConstraint.LabConstraint.Title="Passing element(s):";
FraConstraint.LabConstraint.LongHelp="Point(s)/Curve(s) used as a passing constraint for the fill surface.";

FraConstraint.Constraint.LongHelp=
"Select point(s)/ curve(s) used as a passing constraint(s) for the fill surface.";

PassingElem="Passing element(s)";
PassingElemLabel="Passing element";

FraCanonicalDetection.CheckCanonicalDetection.Title = "Canonical portion detection";

Curve="Curve";
Support="Support";

TolerantMode_None="None";
TolerantMode_Automatic="Automatic";
TolerantMode_Manual="Manual";
FraDev.TolerantModeLab.Title = "Deviation: ";



