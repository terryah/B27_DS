//--------------------------------------------
// Resource file for CATGuiextractPanel class
// En_FR
//--------------------------------------------

PanelTitre="Extract Definition";
LabIndustryType.Title="Recognition context: ";

LabIndustryType.LongHelp=
"Specifies the recognition context. 
If the selected cell is connected to another cell 
by a constant fillet (resp. chamfer) whose radius 
(resp. length) belongs to the selected context and 
if the cells pair form a depression (resp. protrusion) 
in depression (resp. protrusion) propagation, 
then the cell is added to the result.";

CmbIndustryType.LongHelp=
"Specifies the recognition context. 
If the selected cell is connected to another cell 
by a constant fillet (resp. chamfer) whose radius 
(resp. length) belongs to the selected context and 
if the cells pair form a depression (resp. protrusion) 
in depression (resp. protrusion) propagation, 
then the cell is added to the result.";

BodyTitre="To Extract :";
CATGuiExtractSolideReRoute="Brep Deleted. Please, replace the erroneous element";
CATGuiExtractSolideMauvaisePart="You cannot select an element from another part than the current one\n with this Propagation type. Change Propagation type to No propagation";
//CATGuiExtractSolideBesoinSupport="You need a support for this kind of propagation on this internal edge\n please select a support linked with this internal edge";
CATGuiExtractSolideBesoinSupport="You need a support for this kind of propagation on this internal edge, please select a support";

IndustryTypePowerTrain="Power Train (0mm - 15mm)";
IndustryTypeBiW="BiW (1mm - 100mm)";
IndustryTypeConsumerGoods="Consumer Goods (0mm - 50mm)";
IndustryTypeShipBuilding="Ship Building (0mm - infinity)";
IndustryTypeHighTech="High Tech (0mm - 1.5mm)";
IndustryTypeBuilding="Building (1mm - 100mm)";
IndustryTypeMachineDesign="Machine Design (see User Assistance)";

ExtractCombo0="Point continuity";
ExtractCombo1="Tangent continuity";
ExtractCombo2="Curvature continuity";
ExtractCombo3="No propagation";
ExtractComboD = "Depression propagation";
ExtractComboP = "Protrusion propagation";
ComboTitre="Propagation type:";

Type.LongHelp="Defines the propagation type.
You can create only the selected element (BRep Object),
or propagate the creation according to tangency conditions [Tangent continuity]
or point continuity (the extracted element will not present any hole).";

CmbList.LongHelp="Defines the propagation type.
You can create only the selected element (BRep Object),
or to propagate the creation according to tangency conditions [Tangent continuity]
or point continuity (the extracted element will not present any hole).";

FraBody.LongHelp="Specifies the element to be extracted.";

Body.LongHelp="Specifies the element to be extracted.";

SupportTitre="Support :";

FraSupport.LongHelp="Defines the support.
You must select a support in order to compute
the propagation of this edge (internal one) 
because there is ambiguity (more than one support for this edge).";

Support.LongHelp="Defines the support.
You must select a support in order to compute
the propagation of this edge (internal one) 
because there is ambiguity (more than one support for this edge).";

FraTolerance.ComplementaryMode.Title="Complementary mode";
FraTolerance.ComplementaryMode.LongHelp="This option selects the elements that were not previously
selected, while deselecting the elements that were explicitly selected.";

FraTolerance.Federation.Title="Federation";
FraTolerance.Federation.LongHelp="Check this button to generate groups of elements belonging
to the resulting extracted element that will be detected together
with the pointer when selecting one of its sub-elements.";

FraTolerance.PointThreshold.Title="Distance Threshold";
FraTolerance.FraTolerancePtCke.EnglobingFrame.IntermediateFrame.Spinner.Title="Distance Threshold";
FraTolerance.PointThreshold.LongHelp="Specifies the point discontinuity under which the extract is generated.";

FraTolerance.TangentThreshold.Title="Angular Threshold";
FraTolerance.FraToleranceTgtCke.EnglobingFrame.IntermediateFrame.Spinner.Title="Angular Threshold";
FraTolerance.TangentThreshold.LongHelp="Specifies the tangent discontinuity under which the extract is generated.";

FraTolerance.CurvatureThreshold.Title="Curvature Threshold";
FraTolerance.CurvatureThreshold.LongHelp="Specifies the curvature discontinuity above which the extract is generated.
r=1 corresponds to a continuous curvature.
A great discontinuity will require a low r to be taken into account.";
FraTolerance.FraToleranceCurCke.EnglobingFrame.IntermediateFrame.Spinner.Title="Curvature Threshold";

ElementsListTitle="Element(s) to extract";

MoreInfo="Show parameters >>";
LessInfo="Hide parameters <<";
FraTolerance.MoreInfoBt.ShortHelp="Hide or show extended information";

CATGuiExtractSolideDimension2="Incorrect dimension of the selected object. Expected dimension: 2   and selected: ";
CATGuiExtractSolideNomElement="Element";
CATGuiExtractSolideNomSupport="Support";
