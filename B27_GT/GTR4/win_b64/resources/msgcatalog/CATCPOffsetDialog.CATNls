//=============================================================================
//                                     CNEXT - CXR1
//                          COPYRIGHT DASSAULT SYSTEMES 1997 
//-----------------------------------------------------------------------------
// FILENAME    :    CATCPOffsetDialog
// LOCATION    :    DraftingGenUI/CNext/resources/msgcatalog/
// AUTHOR      :    mmr
// DATE        :    Nov. 26 1997
//------------------------------------------------------------------------------
// DESCRIPTION :    Resources File for NLS purpose related to Section View 
//                  Sketch command
//                  
//------------------------------------------------------------------------------
// COMMENTS    :
//------------------------------------------------------------------------------
//MODIFICATIONS     user  date      purpose
//   HISTORY        ----  ----      -------
//                  UVR April,20,2007 Fix for IR 0554011 
//------------------------------------------------------------------------------

CATCPOffsetDialog.Etat0.Message="Select the starting point or a circular edge or an axis";
CATCPOffsetDialog.Etat01.Message="Select an edge or click";
CATCPOffsetDialog.Etat11.Message="Define the first line orientation";
CATCPOffsetDialog.Etat111.Message="Select an edge, click or double-click to end the profile definition";
CATCPOffsetDialog.Etat112.Message="Select an edge, click or double-click to end the profile definition";
CATCPOffsetDialog.Etat113.Message="Select an edge, click or double-click to end the profile definition";



//CATCPOffsetDialog.SystemBarRessource.Bar.Title="Section Profile";


//******************************************************************************
// Parallel constraint
//******************************************************************************
CATCPOffsetDialog.SystemBarRessource.Parallel.Title = "Parallel";
CATCPOffsetDialog.SystemBarRessource.Parallel.ShortHelp="Parallel";
CATCPOffsetDialog.SystemBarRessource.Parallel.Help=
"Creates a profile parallel to the selected item";
CATCPOffsetDialog.SystemBarRessource.Parallel.LongHelp=
"Creates a profile parallel to the selected item";

//******************************************************************************
// Perpendicular constraint
//******************************************************************************
CATCPOffsetDialog.SystemBarRessource.Perpendicular.Title = "Perpendicular";
CATCPOffsetDialog.SystemBarRessource.Perpendicular.ShortHelp="Perpendicular";
CATCPOffsetDialog.SystemBarRessource.Perpendicular.Help=
"Creates a profile perpendicular to the selected item";
CATCPOffsetDialog.SystemBarRessource.Perpendicular.LongHelp=
"Creates a profile perpendicular to the selected item";

//******************************************************************************
// Angular constraint
//******************************************************************************
CATCPOffsetDialog.SystemBarRessource.Angle.Title = "Angle";
CATCPOffsetDialog.SystemBarRessource.Angle.ShortHelp="Angle";
CATCPOffsetDialog.SystemBarRessource.Angle.Help=
"Creates a profile with an angle to the selected item";
CATCPOffsetDialog.SystemBarRessource.Angle.LongHelp=
"Creates a profile with an angle to the selected item";

//******************************************************************************
// Angle
//******************************************************************************
CATCPOffsetDialog.Angle.Dimension.EnglobingFrame.IntermediateFrame.Label.Title = "Angle: ";
CATCPOffsetDialog.Angle.Dimension.EnglobingFrame.IntermediateFrame.Label.ShortHelp = "Enter an angle between +/-90 from the selected direction";

//******************************************************************************
// Offset constraint
//******************************************************************************
CATCPOffsetDialog.SystemBarRessource.Offset.Title = "Offset";
CATCPOffsetDialog.SystemBarRessource.Offset.ShortHelp="Offset";
CATCPOffsetDialog.SystemBarRessource.Offset.Help=
"Creates a profile with an Offset to the selected item";
CATCPOffsetDialog.SystemBarRessource.Offset.LongHelp=
"Creates a profile with an Offset to the selected item";

//******************************************************************************
// Offset
//******************************************************************************
CATCPOffsetDialog.Offset.Dimension.EnglobingFrame.IntermediateFrame.Label.Title = "Offset: ";
CATCPOffsetDialog.Offset.Dimension.EnglobingFrame.IntermediateFrame.Label.ShortHelp = "Enter a positive Offset value";

// Begin changes by UVR  
// for fixing IR 0554011 Function name displayed incorrectly in undo-redo history wondow
//******************************************************************************
//Undo Title
//******************************************************************************
CATCPOffsetDialog.CATCPOffsetDialog_Copilot.UndoTitle="Select a Point";
CATCPOffsetDialog.CATCPOffsetDialog_Selection.UndoTitle="Select an Edge";
CATCPOffsetDialog.UndoTitle="Define Profile";

//End changes by UVR 
