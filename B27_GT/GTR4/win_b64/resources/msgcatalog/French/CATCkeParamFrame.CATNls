// COPYRIGHT DASSAULT SYSTEMES  1997
//=============================================================================
//
// Catalog for CATCkeParamFrame class
//
//=============================================================================
// 10/22/97   Creation                                   A. Anquetil
//  7/11/00   Adding SetOfEquations:           D. Ouchakov, LEDAS Ltd. (vtn1)
//============================================================================

//Objet graphic
CATCkeParamFrame.ParamValue_.Help="Changer la valeur du param�tre : /P1 attendu(e).";
CATCkeParamFrame.ParamValue_.LongHelp="Changer la valeur du param�tre : /P1 requis.",
" Un menu contextuel est disponible.";

//Warning
CATCkeParamFrame.Warning_.Title="Avertissement";
CATCkeParamFrame.FormatWarning_.Text="Le type de ce param�tre est incorrect";
CATCkeParamFrame.FormulaWarning1_.Text="La formule pr�sente va �tre d�sactiv�e";
CATCkeParamFrame.FormulaWarning2_.Text="Le programme pr�sent va �tre d�sactiv�";

//Menu contextuel
CATCkeParamFrame.CreateFormulaMenu_.Title="Editer formule...";
CATCkeParamFrame.ModifyEquMenu_.Title="Formule";
//tdv debut
CATCkeParamFrame.ModifyDeactivatedEquMenu_.Title="Formule d�sactiv�e";
CATCkeParamFrame.ModifyAllDeactivatedEquMenu_.Title="Formules d�sactiv�es";
//tdv fin
CATCkeParamFrame.EditEquMenu_.Title="Editer...";
CATCkeParamFrame.DeleteEquMenu_.Title="Supprimer";
CATCkeParamFrame.DesactivateMenu_.Title="D�sactiver";
CATCkeParamFrame.ActivateMenu_.Title="Activer";
CATCkeParamFrame.RenameMenu_.Title="Editer...";
CATCkeParamFrame.MeasuringMenu_.Title="Mesurer entre...";
CATCkeParamFrame.MeasuringMenu2_.Title="Mesurer �l�ment...";
CATCkeParamFrame.CreateToleranceMenu_.Title="Ajouter une tol�rance...";
CATCkeParamFrame.ModifyToleranceMenu_.Title="Tol�rance";
CATCkeParamFrame.EditToleranceMenu_.Title="Editer...";
CATCkeParamFrame.SuppressToleranceMenu_.Title="Supprimer";

CATCkeParamFrame.ListOfValuesGeneralSubMenu_.Title="Valeurs Multiples";
CATCkeParamFrame.ListOfValuesUpdateMenu_.Title="Editer valeurs...";
CATCkeParamFrame.ListOfValuesSuppressMenu_.Title="Supprimer valeurs...";
CATCkeParamFrame.ListOfValuesAddMenu_.Title="Ajouter des valeurs multiples...";
CATCkeParamFrame.ListOfValuesQuestionSuppression.Text="Souhaitez-vous vraiment supprimer plusieurs valeurs ?";
CATCkeParamFrame.ListOfValuesQuestionSuppression.Title="Suppression des valeurs multiples";

CATCkeParamFrame.CreateRangeMenu_.Title="Ajouter bornes...";
CATCkeParamFrame.ModifyRangeMenu_.Title="Bornes";
CATCkeParamFrame.EditRangeMenu_.Title="Editer...";
CATCkeParamFrame.SuppressRangeMenu_.Title="Supprimer";
CATCkeParamFrame.CommentMenu_.Title="Editer commentaire...";
CATCkeParamFrame.HideMenu_.Title="Cacher";
CATCkeParamFrame.ShowMenu_.Title="Afficher";
CATCkeParamFrame.LockMenu_.Title="Verrouiller";
CATCkeParamFrame.UnlockMenu_.Title="D�verrouiller";
CATCkeParamFrame.UnSetMenu_.Title="Non valu�";
DissociateMenu="Enlever l'association avec la table";
DissociateQuestion="Etes-vous s�r de vouloir enlever cette association ?\nLe param�tre ne sera plus pilot� par la table de param�trage";
DissociateHeader="Enlever l'association ?";

CATCkeParamFrame.CommentDlg_.Title="Commentaire de /P1";

//ShortHelp
CATCkeParamFrame.widget1.ShortHelp="=/P1";
CATCkeParamFrame.widget2.ShortHelp="= Prog : /P1";
CATCkeParamFrame.widget3.ShortHelp="Entrer /P1";

//Dialog
CATCkeParamFrame.RenameDlg_.Title="Edition de param�tre";

RelButtonDTHelp="Ouvre une bo�te de dialogue permettant de changer la configuration de la table de param�trage pilotante";
RelButtonFormHelp="Ouvre une bo�te de dialogue qui permet de modifier la formule pilotante";
RelButtonRuleHelp="Ouvre une bo�te de dialogue qui permet de modifier la r�gle pilotante";
RelButtonCheckHelp="Ouvre une bo�te de dialogue qui permet de modifier la v�rification dont ce param�tre est le r�sultat";

CATCkeParamFrame.Warning.Title="Avertissement";
CATCkeParamFrame.Warning.Text="Vous ne pouvez pas modifier cette formule, car elle utilise des op�rateurs externes \n",
"et la licence Knowledge Advisor est indisponible";
MeasureResult="Voulez-vous copier le r�sultat de cette mesure dans le param�tre ?";
MeasureResult2="Voulez-vous cr�er une formule utilisant le param�tre de mesure cr�� (r�pondre Oui)\nou seulement copier la valeur (r�pondre Non)?";
MeasureResultHd="La commande de mesure s'est termin�e";

//FastForm="In this calculator fonctionnality, the dimensions without units are considered like dimensions expressed in MKS\n(meter for length magnitude for instance).\n\nIt is so strongly advised to type all units\n(the 10+10 expression won't necessary give the same result than if you directly type 20)";
//FastFormHd="Warning";

// Added by vtn1 on 7/11/2000:
RelButtonSetOfEquationsHelp="Ouvre une bo�te de dialogue qui permet de modifier l'ensemble d'�quations pilotantes";
Unset="Non valu�";

NoFormulaForVolatile="Vous ne pouvez pas ajouter de formule sur ce param�tre";

// Added by SOC on 22/03/02
IntegerOverflowWg="D�passement de la capacit� d'un entier";
MeasureHelp="Editer la mesure pilotante";

RemoveLinkWithMeasure="Supprime le lien avec la mesure";

// SGQ
CATCkeParamFrame.LCAProxiesWarning.Title="Avertissement";
CATCkeParamFrame.LCAProxiesWarning.Text="L'�dition de cet objet d'un module technologique n'est pas autoris�e car il utilise des param�tres ou des fonctions d�charg�s.";


//BUD: Bornes applicatives
CATCkeParamFrame.UnableToDeleteRange.Title="Impossible de supprimer la ou les plage(s)";
CATCkeParamFrame.UnableToDeleteRange.Message="
		Impossible de supprimer /P1 plage(s). Elles sont d�finies en lecture seule.";
CATCkeParamFrame.Inferior="Inf�rieure";
CATCkeParamFrame.Superior="Sup�rieure";
CATCkeParamFrame.InferiorAndSuperior="Inf�rieure et sup�rieure";
CATCkeParamFrame.CATCkeValueOutOfRangeGT="La valeur /P1 n'est pas comprise dans la plage autoris�e : elle doit �tre sup�rieure � /P2.\nLa valeur /P3 a �t� restaur�e.\nEssayez d'�diter la plage...";
CATCkeParamFrame.CATCkeValueOutOfRangeLW="La valeur /P1 n'est pas comprise dans la plage autoris�e : elle doit �tre inf�rieure � /P2.\nLa valeur /P3 a �t� restaur�e.\nEssayez d'�diter la plage...";
CATCkeParamFrame.CATCkeValueOutOfRangeGTInclusive="La valeur /P1 n'est pas comprise dans la plage autoris�e : elle doit �tre sup�rieure ou �gale � /P2.\nLa valeur /P3 a �t� restaur�e.\nEssayez d'�diter la plage...";
CATCkeParamFrame.CATCkeValueOutOfRangeLWInclusive="La valeur /P1 n'est pas comprise dans la plage autoris�e : elle doit �tre inf�rieure ou �gale � /P2.\nLa valeur /P3 a �t� restaur�e.\nEssayez d'�diter la plage...";
CATCkeParamFrame.CATCkeValueOutOfRangeGT_Old="La valeur /P1 n'est pas comprise dans la plage autoris�e : la valeur doit �tre sup�rieure � /P2. \nEssayez d'�diter la plage...";
CATCkeParamFrame.CATCkeValueOutOfRangeLW_Old="La valeur /P1 n'est pas comprise dans la plage autoris�e : la valeur doit �tre inf�rieure � /P2. \nEssayez d'�diter la plage...";


//BUD 30/11/2007: Passage sur le servive de Marc: CATCkeVRC = CATCkeValuateParameter(...)
CATCkeParamFrame.OverflowWg="D�passement de la capacit� d'un param�tre";
CATCkeParamFrame.NoChangeWg="Avertissement : la valeur du param�tre /P1 n'a pas �t� modifi�e.";
CATCkeParamFrame.UnderEpsilonWg="La valeur du param�tre est trop faible. La valeur 0 a �t� restaur�e.";


