//=============================================================================
//                          COPYRIGHT DASSAULT SYSTEMES PROVENCE 2007 
//-----------------------------------------------------------------------------
// FILENAME    :    CATStd_dxf.CATNls
// AUTHOR      :    prb
// DATE        :    07/2007
//------------------------------------------------------------------------------
// DESCRIPTION :    NLS File for names of nodes in standard xml files for dxf
//                  
//------------------------------------------------------------------------------
// COMMENTS    :
//------------------------------------------------------------------------------
//MODIFICATIONS     user  date      purpose
//   HISTORY        ----  ----      -------
// BMT : 02/09 : Authorize yes/No : REport correction phase2->phase1 : A0633407
//------------------------------------------------------------------------------

DxfImport.Title="Importer un fichier DXF";

DxfImport.LineTypeMapping.Title="Mappage de types de lignes";
DxfImport.LineTypeMapping.DxfLineTypeName.Title="Nom du type de ligne DXF";
DxfImport.LineTypeMapping.CatiaLineTypeNumber.Title="Num�ro du type de ligne CATIA";

DxfImport.ColorToThicknessMapping.Title="Mappage des �paisseurs � partir des couleurs";
DxfImport.ColorToThicknessMapping.AuthorizeMapping.Title="Activer le mappage";
DxfImport.ColorToThicknessMapping.AuthorizeMapping.Yes.Title="Oui";
DxfImport.ColorToThicknessMapping.AuthorizeMapping.No.Title="Non";
DxfImport.ColorToThicknessMapping.DefaultThickness.Title="Epaisseur par d�faut";
DxfImport.ColorToThicknessMapping.DxfColorNumber.Title="Num�ro de la couleur DXF";
DxfImport.ColorToThicknessMapping.CatiaThicknessNumber.Title="Num�ro de l'�paisseur CATIA";

DxfImport.TextFontMapping.Title="Mappage des polices de caract�res";
DxfImport.TextFontMapping.Default.Title="Par d�faut";
DxfImport.TextFontMapping.Default.CatiaFontName.Title="Nom de la police CATIA";
DxfImport.TextFontMapping.Default.Ratio.Title="Fraction";
DxfImport.TextFontMapping.DefaultDBCS.Title="Par d�faut pour DBCS";
DxfImport.TextFontMapping.DefaultDBCS.CatiaFontName.Title="Nom de la police CATIA";
DxfImport.TextFontMapping.DefaultDBCS.Ratio.Title="Fraction";
DxfImport.TextFontMapping.DxfFontName.Title="Nom de la police DXF";
DxfImport.TextFontMapping.CatiaFontName.Title="Nom de la police CATIA";
DxfImport.TextFontMapping.Ratio.Title="Fraction";

DxfImport.ThickPolylineMapping.Title="Mappage de polydroite �paisse";
DxfImport.ThickPolylineMapping.CreateAreaFill.Title="Activer la cr�ation de hachures";
DxfImport.ThickPolylineMapping.CreateAreaFill.Yes.Title="Oui";
DxfImport.ThickPolylineMapping.CreateAreaFill.No.Title="Non";
DxfImport.ThickPolylineMapping.MinThicknessForAreaFill.Title="Epaisseur minimale des hachures (en mm)";

DxfImport.ColorMapping.Title="Mappage de couleur";
DxfImport.ColorMapping.AdaptToBackground.Title="Adaptation � l'arri�re-plan";
DxfImport.ColorMapping.AdaptToBackground.Yes.Title="Oui";
DxfImport.ColorMapping.AdaptToBackground.No.Title="Non";
DxfImport.ColorMapping.DarkBackgroundLuminosity.Title="Luminosit� d'arri�re-plan sombre (0..255)";
DxfImport.ColorMapping.LightBackgroundLuminosity.Title="Luminosit� d'arri�re-plan claire (0..255)";
