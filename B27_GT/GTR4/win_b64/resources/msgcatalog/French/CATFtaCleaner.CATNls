// COPYRIGHT DASSAULT SYSTEMES 2003
//=============================================================================
//
// Resources File for NLS purpose related to FTA cleaner
//
//=============================================================================
// Usage notes:
//
//=============================================================================
// Apr. 2003  Creation                                               Z. Y. Gong
//=============================================================================

FTA_1.ShortMessage="Validit� des composants dans le conteneur TTRS.";
FTA_1.ErrorMessage="Certains composants du conteneur TTRS ne sont pas valides.";
FTA_1.CleanMessage="Les composants incorrects du conteneur TTRS sont supprim�s du document.";

FTA_2.ShortMessage="Utilisation de RGE pour l'annotation 3D.";
FTA_2.ErrorMessage="Le composant RGE n'est utilis� par aucune annotation 3D.";
FTA_2.CleanMessage="Le composant RGE non utilis� est supprim� du document.";

FTA_3.ShortMessage="Utilisation de TTRS par l'annotation 3D.";
FTA_3.ErrorMessage="Le composant TTRS n'est utilis� par aucune annotation 3D.";
FTA_3.CleanMessage="Le composant TTRS non utilis� est supprim� du document.";

FTA_4.ShortMessage="Liens entre l'annotation de dessin et l'annotation 3D dans le set.";
FTA_4.ErrorMessage="Une ou plusieurs annotations de dessin ne sont pas utilis�es par une annotation 3D.";
FTA_4.CleanMessage="Les annotations non utilis�es sont supprim�es du document.";

FTA_5.ShortMessage="Lien entre l'annotation 3D et l'entit� de tol�rancement.";
FTA_5.ErrorMessage="L'annotation 3D n'est utilis�e par aucune entit� de tol�rancement.";
FTA_5.CleanMessage="L'annotation 3D est supprim�e du document.";

FTA_6.ShortMessage="Repr�sentation des annotations incoh�rente";
FTA_6.ErrorMessage="Les composants d'annotation FTA ne sont pas compl�tement redirig�s sur la nouvelle g�om�trie.";
FTA_6.CleanMessage="L'annotation FTA est correctement reconnect�e � la g�om�trie.";

FTA_7.ShortMessage="G�om�trie redondante sous un TTRS.";
FTA_7.Error1Message="La surface de l'utilisateur comporte plusieurs composants pointant vers la m�me g�om�trie. => /p1 doit �tre reconnect� manuellement avec la g�om�trie.";
FTA_7.Error2Message="La surface de l'utilisateur comporte plusieurs composants pointant vers la m�me g�om�trie. => /p1 doit �tre reconnect� manuellement avec la g�om�trie.  /p2 est cr�� sur cette surface de l'utilisateur.";
FTA_7.Error3Message="La surface utilisateur a plusieurs composants qui pointent vers la m�me g�om�trie. => Les composants '/p3' et '/p4' de '/p1' pointent vers la m�me g�om�trie. '/p1' doit �tre reconnect� manuellement avec la g�om�trie. '/p2' est cr�� sur cette surface utilisateur.";

FTA_8.ShortMessage="Erreur de calcul du statut de l'unit� d'ex�cution CG.";
FTA_8.ErrorMessage="La red�finition de la repr�sentation TTRS de l'unit� d'ex�cution CG est incorrecte.";

FTA_9.ShortMessage="Affichage incorrect de la tol�rance g�om�trique.";
FTA_9.ErrorMessage="L'affichage 3D de la tol�rance g�om�trique n'est pas coh�rent avec l'attribut s�mantique associ�.
La zone de tol�rance est un cylindre d'apr�s les attributs s�mantiques, mais le symbole de diam�tre n'appara�t pas dans le trait de coupe.
Pour corriger cette erreur, modifiez la tol�rance g�om�trique (le symbole de diam�tre est alors ajout� automatiquement) et :
1. Cliquez sur le bouton OK si l'objectif de la conception est de d�finir une zone de tol�rance cylindrique ou
2. S�lectionnez une direction de zone de tol�rance si l'objectif de la conception est de d�finir une zone de tol�rance de 2 plans parall�les et cliquez sur OK.";

FTA_10.ShortMessage="Utilisation de TTRS incoh�rente par la vue FTA.";
FTA_10.ErrorMessage="La vue FTA cr��e sur le syst�me d'axes 3D utilise l'objet TTRS d'un document diff�rent.";
FTA_10.CleanMessage="Le TTRS utilis� par la vue est corrig�.";

FTA_11.ShortMessage="Lien URL incoh�rent.";
FTA_11.ErrorMessage="Au moins un lien URL du composant FlagNote ou NOA est vide.";
FTA_11.CleanMessage="Le lien URL vide a �t� supprim�.";

FTA_12.ShortMessage="Lien entre l'annotation 3D et une vue de l'annotation 3D.";
FTA_12.ErrorMessage="Aucune vue d'annotation 3D n'a �t� trouv�e pour l'annotation 3D.";
FTA_12.CleanMessage="Une vue d'annotation 3D a �t� cr��e et reconnect�e � l'annotation.";

FTA_13.ShortMessage="Zone de r�f�rence partielle incoh�rente.";
FTA_13.ErrorMessage="La zone de r�f�rence partielle s�mantique n'est pas associ�e � une r�f�rence s�mantique.";
FTA_13.CleanMessage="La zone de r�f�rence partielle incoh�rente sans r�f�rence s�mantique est supprim�e.";

FTA_14.ShortMessage="Liens incoh�rents dans le composant de dimension de visualisation.";
FTA_14.ErrorMessage="Le composant de dimension de visualisation est li� par plusieurs annotations.";
FTA_14.CleanMessage="Le lien non valide au composant de dimension de visualisation a �t� supprim�.";

FTA_15.ShortMessage="Liens externes de capture FTA.";
FTA_15.ErrorMessage="Au moins un lien externe de capture pointe vers un document non existant.";
FTA_15.CleanMessage="Les liens externes non existants ont �t� supprim�s.";

FTA_16.ShortMessage="Liens externes FTA NOA.";
FTA_16.ErrorMessage="Un lien externe de Noa pointe vers un document non existant.";
FTA_16.CleanMessage="Un lien externe non existant a �t� supprim�.";

FTA_17.ShortMessage="FTA Noa avec Ditto.";
FTA_17.ErrorMessage="Ditto de Noa n'est pas cr�� sur un calque de fond.";
FTA_17.CleanMessage="Ditto de Noa est redirig� vers un calque de fond.";

FTA_18.ShortMessage="Liens externes de texte FTA.";
FTA_18.ErrorMessage="Un lien externe de texte pointe vers un document inexistant.";
FTA_18.CleanMessage="Les liens externes inexistants ont �t� supprim�s.";

FTA_19.ShortMessage="Identifiant de composant r�p�titif incoh�rent.";
FTA_19.ErrorMessage="Soit l'identifiant du composant r�p�titif ne correspond pas au nombre d'�l�ments de motif auxquels la cote est li�e, soit il y a plusieurs identifiants de composant r�p�titif ou il n'y en a aucun, soit il n'existe pas d'espace (� �) entre l'identifiant du composant r�p�titif et le texte qui le pr�c�de ou le suit.";
FTA_19.CleanMessage="L'identifiant de composant r�p�titif incoh�rent d'une cote de motif a �t� corrig�.";
