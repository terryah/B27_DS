// COPYRIGHT DASSAULT SYSTEMES 2000
//===========================================================================
//
// CATSamMaterialFrame (English)
//
//===========================================================================
Analysis="Analyse";

INF_PoissonRatioModifed="Le coefficient de Poisson ne peut pas d�passer 0,5 : il a �t� d�fini sur 0,4999.";
INF_PoissonRatioExceed="Le coefficient de Poisson est sup�rieur � la valeur maximale autoris�e. Il a �t� d�fini � la valeur maximale autoris�e.";
INF_YoungsModulusRatio="Le coefficient du module de Young est sup�rieur � la valeur maximale autoris�e. Les valeurs du coefficient du module de Young et de Poisson sont d�finies � 0.";
INF_PoissonRatioOrthotropic3D="Les valeurs du coefficient de Poisson sont sup�rieures � la valeur maximale autoris�e. Elles ont �t� d�finies � la valeur maximale autoris�e";
INF_YoungsModulusRatioOrthotropic3D="Les valeurs du coefficient du module de Young sont sup�rieures � la valeur maximale autoris�e. Les valeurs du coefficient du module de Young et de Poisson sont d�finies � 0.";
INF_CombinedOrthotropic3DExceed="La combinaison des valeurs du coefficient de Poisson et du module de Young est sup�rieure � la valeur autoris�e de 1. Les valeurs des coefficients du module de Young et de Poisson sont d�finies � 0.";
MaterialInfoUITitle="Information";

analysisFrame.Title="Propri�t�s structurales";

SAMYoungModulus.EnglobingFrame.IntermediateFrame.Label.Title="Module de Young";
SAMYoungModulus.EnglobingFrame.IntermediateFrame.Label.Value.Help="Module de Young du mat�riau";
SAMYoungModulus.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="Module d'Young pour d�crire la rigidit� de la pi�ce";

SAMPoissonRatio.EnglobingFrame.IntermediateFrame.Label.Title="Coefficient de Poisson";
SAMPoissonRatio.EnglobingFrame.IntermediateFrame.Label.Value.Help="Coefficient de Poisson du mat�riau";
SAMPoissonRatio.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="Coefficient de Poisson du mat�riau 
(dilatation m�canique valeur entre 0 et 0.5)";

SAMDensity.EnglobingFrame.IntermediateFrame.Label.Title="Masse volumique";
SAMDensity.EnglobingFrame.IntermediateFrame.Label.Value.Help="Masse volumique du mat�riau";
SAMDensity.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="Masse volumique du mat�riau (utile pour calcul modal)";

SAMThermalExpansion.EnglobingFrame.IntermediateFrame.Label.Title="Coefficient d'expansion thermique";
SAMThermalExpansion.EnglobingFrame.IntermediateFrame.Label.Value.Help="Coefficient d'expansion thermique du mat�riau";
SAMThermalExpansion.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="Coefficient d'expansion thermique du mat�riau (effet de dilatation) thermique";

SAMShearModulus.EnglobingFrame.IntermediateFrame.Label.Title="Limite �lastique";
SAMShearModulus.EnglobingFrame.IntermediateFrame.Label.Value.Help="Limite �lastique du mat�riau";
SAMShearModulus.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="Limite �lastique du mat�riau.
Doit �tre compar�e avec la valeur maximale de VON MISES pour voir si la pi�ce casse";

// SAMOrthotropic
SAMYoungModulus_11.EnglobingFrame.IntermediateFrame.Label.Title="Module de Young longitudinal";
SAMYoungModulus_11.EnglobingFrame.IntermediateFrame.Label.Value.Help="Module de Young longitudinal";
SAMYoungModulus_11.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="Module de Young longitudinal";

SAMYoungModulus_22.EnglobingFrame.IntermediateFrame.Label.Title="Module de Young transversal";
SAMYoungModulus_22.EnglobingFrame.IntermediateFrame.Label.Value.Help="Module de Young transversal";
SAMYoungModulus_22.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="Module de Young transversal";

SAMPoissonRatio_12.EnglobingFrame.IntermediateFrame.Label.Title="Coefficient de Poisson dans le plan XY";
SAMPoissonRatio_12.EnglobingFrame.IntermediateFrame.Label.Value.Help="Coefficient de Poisson du mat�riau dans le plan XY";
SAMPoissonRatio_12.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="Coefficient de Poisson dans le plan XY";

SAMPoissonRatio_13.EnglobingFrame.IntermediateFrame.Label.Title="Coefficient de Poisson dans le plan XZ";
SAMPoissonRatio_13.EnglobingFrame.IntermediateFrame.Label.Value.Help="Coefficient de Poisson du mat�riau dans le plan XZ";
SAMPoissonRatio_13.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="Coefficient de Poisson dans le plan XZ";
SAMPoissonRatio_23.EnglobingFrame.IntermediateFrame.Label.Title="Coefficient de Poisson dans le plan YZ";
SAMPoissonRatio_23.EnglobingFrame.IntermediateFrame.Label.Value.Help="Coefficient de Poisson du mat�riau dans le plan YZ";
SAMPoissonRatio_23.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="Coefficient de Poisson dans le plan YZ";

SAMShearModulus_12.EnglobingFrame.IntermediateFrame.Label.Title="Module de cisaillement dans le plan XY";
SAMShearModulus_12.EnglobingFrame.IntermediateFrame.Label.Value.Help="Module de cisaillement dans le plan XY";
SAMShearModulus_12.EnglobingFrame.IntermediateFrame.Label.Value.longHelp="Module de cisaillement dans le plan XY";

SAMShearModulus_1Z.EnglobingFrame.IntermediateFrame.Label.Title="Module de cisaillement dans le plan XZ";
SAMShearModulus_1Z.EnglobingFrame.IntermediateFrame.Label.Value.Help="Module de cisaillement dans le plan XZ";
SAMShearModulus_1Z.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="Module de cisaillement dans le plan XZ";

SAMShearModulus_2Z.EnglobingFrame.IntermediateFrame.Label.Title="Module de cisaillement dans le plan YZ";
SAMShearModulus_2Z.EnglobingFrame.IntermediateFrame.Label.Value.Help="Module de cisaillement dans le plan YZ";
SAMShearModulus_2Z.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="Module de cisaillement dans le plan YZ";

SAMShearModulus_13.EnglobingFrame.IntermediateFrame.Label.Title="Module de cisaillement dans le plan XZ";
SAMShearModulus_13.EnglobingFrame.IntermediateFrame.Label.Value.Help="Module de cisaillement dans le plan XZ";
SAMShearModulus_13.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="Module de cisaillement dans le plan XZ";

SAMShearModulus_23.EnglobingFrame.IntermediateFrame.Label.Title="Module de cisaillement dans le plan YZ";
SAMShearModulus_23.EnglobingFrame.IntermediateFrame.Label.Value.Help="Module de cisaillement dans le plan YZ";
SAMShearModulus_23.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="Module de cisaillement dans le plan YZ";

SAMTensileStressLimit_X.EnglobingFrame.IntermediateFrame.Label.Title="Contrainte longitudinale limite de traction";
SAMTensileStressLimit_X.EnglobingFrame.IntermediateFrame.Label.Value.Help="Contrainte longitudinale limite de traction";
SAMTensileStressLimit_X.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="Contrainte longitudinale limite de traction";

SAMCompressiveStressLimit_X.EnglobingFrame.IntermediateFrame.Label.Title="Contrainte longitudinale limite de compression";
SAMCompressiveStressLimit_X.EnglobingFrame.IntermediateFrame.Label.Value.Help="Contrainte longitudinale limite de compression";
SAMCompressiveStressLimit_X.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="Contrainte longitudinale limite de compression";

SAMTensileStressLimit_Y.EnglobingFrame.IntermediateFrame.Label.Title="Contrainte transversale limite de traction";
SAMTensileStressLimit_Y.EnglobingFrame.IntermediateFrame.Label.Value.Help="Contrainte transversale limite de traction";
SAMTensileStressLimit_Y.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="Contrainte transversale limite de traction";

SAMCompressiveStressLimit_Y.EnglobingFrame.IntermediateFrame.Label.Title="Contrainte transversale limite de compression";
SAMCompressiveStressLimit_Y.EnglobingFrame.IntermediateFrame.Label.Value.Help="Contrainte transversale limite de compression";
SAMCompressiveStressLimit_Y.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="Contrainte transversale limite de compression";

SAMTensileStrainLimit_X.EnglobingFrame.IntermediateFrame.Label.Title="Effort longitudinal de traction";
SAMTensileStrainLimit_X.EnglobingFrame.IntermediateFrame.Label.Value.Help="Effort longitudinal de traction du mat�riau";
SAMTensileStrainLimit_X.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="Effort longitudinal de traction";

SAMCompressiveStrainLimit_X.EnglobingFrame.IntermediateFrame.Label.Title="Effort longitudinal de compression";
SAMCompressiveStrainLimit_X.EnglobingFrame.IntermediateFrame.Label.Value.Help="Effort longitudinal de compression du mat�riau";
SAMCompressiveStrainLimit_X.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="Effort longitudinal de compression";

SAMTensileStrainLimit_Y.EnglobingFrame.IntermediateFrame.Label.Title="Effort transversal de traction";
SAMTensileStrainLimit_Y.EnglobingFrame.IntermediateFrame.Label.Value.Help="Effort transversal de traction du mat�riau";
SAMTensileStrainLimit_Y.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="Effort transversal de traction";

SAMCompressiveStrainLimit_Y.EnglobingFrame.IntermediateFrame.Label.Title="Effort transversal de compression";
SAMCompressiveStrainLimit_Y.EnglobingFrame.IntermediateFrame.Label.Value.Help="Effort transversal de compression du mat�riau";
SAMCompressiveStrainLimit_Y.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="Effort transversal de compression";

SAMThermalExpansion_X.EnglobingFrame.IntermediateFrame.Label.Title="Coefficient d'expansion thermique longitudinal";
SAMThermalExpansion_X.EnglobingFrame.IntermediateFrame.Label.Value.Help="Coefficient d'expansion thermique longitudinal";
SAMThermalExpansion_X.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="Coefficient d'expansion thermique longitudinal";

SAMThermalExpansion_Y.EnglobingFrame.IntermediateFrame.Label.Title="Coefficient d'expansion thermique transversal";
SAMThermalExpansion_Y.EnglobingFrame.IntermediateFrame.Label.Value.Help="Coefficient d'expansion thermique transversal";
SAMThermalExpansion_Y.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="Coefficient d'expansion thermique transversal";

SAMThermalExpansion_33.EnglobingFrame.IntermediateFrame.Label.Title="Coefficient d'expansion thermique normal";
SAMThermalExpansion_33.EnglobingFrame.IntermediateFrame.Label.Value.Help="Coefficient d'expansion thermique normal du mat�riau";
SAMThermalExpansion_33.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="Coefficient d'expansion thermique normal";

analysisFrame.stressFrame.Title="Contraintes limites";
analysisFrame.strainFrame.Title="Contraintes limites";


// combo de selection de type de materaiux
MaterialType.Help="D�finir le type de mat�riau";
MaterialType.MaterialTypeLabel.Title="Mat�riau";
MaterialType.MaterialTypeCombo.SAMIsotropicMaterial.Title="Mat�riau isotropique";
MaterialType.MaterialTypeCombo.SAMOrthotropicMaterial.Title="Mat�riau orthotropique 2D";
MaterialType.MaterialTypeCombo.SAMFiberMaterial.Title="Mat�riau fibre";
MaterialType.MaterialTypeCombo.SAMHoneyCombMaterial.Title="Mat�riau nid d'abeille";
MaterialType.MaterialTypeCombo.SAMOrthotropic3DMaterial.Title="Mat�riau orthotropique 3D";
MaterialType.MaterialTypeCombo.SAMAnisotropicMaterial.Title="Mat�riau anisotrope";


// Material HONEYCOMB
SAMYoungModulus_33.EnglobingFrame.IntermediateFrame.Label.Title="Module de Young normal";
SAMYoungModulus_33.EnglobingFrame.IntermediateFrame.Label.Value.Help="Module de Young normal du mat�riau";
SAMYoungModulus_33.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="Module de Young normal";

SAMShearStressLimit_13.EnglobingFrame.IntermediateFrame.Label.Title="Contrainte limite de cisaillement dans le plan XZ";
SAMShearStressLimit_13.EnglobingFrame.IntermediateFrame.Label.Value.Help="Contrainte limite de cisaillement du mat�riau dans le plan XZ";
SAMShearStressLimit_13.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="Contrainte limite de cisaillement dans le plan XZ";

SAMShearStressLimit_23.EnglobingFrame.IntermediateFrame.Label.Title="Contrainte limite de cisaillement dans le plan YZ";
SAMShearStressLimit_23.EnglobingFrame.IntermediateFrame.Label.Value.Help="Contrainte limite de cisaillement du mat�riau dans le plan YZ";
SAMShearStressLimit_23.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="Contrainte limite de cisaillement dans le plan YZ";

// Material FIBER
SAMShearStressLimit_12.EnglobingFrame.IntermediateFrame.Label.Title="Contrainte limite de cisaillement dans le plan XY";
SAMShearStressLimit_12.EnglobingFrame.IntermediateFrame.Label.Value.Help="Contrainte limite de cisaillement du mat�riau dans le plan XY";
SAMShearStressLimit_12.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="Contrainte limite de cisaillement dans le plan XY";

// Material ANISOTROPE
SAMThermalExpansion_Z.EnglobingFrame.IntermediateFrame.Label.Title="Coefficient d'expansion thermique normal";
SAMThermalExpansion_Z.EnglobingFrame.IntermediateFrame.Label.Value.Help="Coefficient d'expansion thermique normal du mat�riau";
SAMThermalExpansion_Z.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="Coefficient d'expansion thermique normal";

SAMShearModulus_11.EnglobingFrame.IntermediateFrame.Label.Title="Module de cisaillement longitudinal";
SAMShearModulus_11.EnglobingFrame.IntermediateFrame.Label.Value.Help="Module de cisaillement longitudinal du mat�riau";
SAMShearModulus_11.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="Module de cisaillement longitudinal";

SAMShearModulus_22.EnglobingFrame.IntermediateFrame.Label.Title="Module de cisaillement transversal";
SAMShearModulus_22.EnglobingFrame.IntermediateFrame.Label.Value.Help="Module de cisaillement transversal du mat�riau";
SAMShearModulus_22.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="Module de cisaillement transversal";

SAMShearModulus_33.EnglobingFrame.IntermediateFrame.Label.Title="Module de cisaillement normal";
SAMShearModulus_33.EnglobingFrame.IntermediateFrame.Label.Value.Help="Module de cisaillement normal du mat�riau";
SAMShearModulus_33.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="Module de cisaillement normal";

SAMTensileStressLimit.EnglobingFrame.IntermediateFrame.Label.Title="Effort de traction";
SAMTensileStressLimit.EnglobingFrame.IntermediateFrame.Label.Value.Help="Effort de traction du mat�riau";
SAMTensileStressLimit.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="Effort de traction";

SAMCompressiveStressLimit.EnglobingFrame.IntermediateFrame.Label.Title="Effort de compression";
SAMCompressiveStressLimit.EnglobingFrame.IntermediateFrame.Label.Value.Help="Effort de compression du mat�riau";
SAMCompressiveStressLimit.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="Effort de compression";

SAMShearStressLimit.EnglobingFrame.IntermediateFrame.Label.Title="Effort de cisaillement";
SAMShearStressLimit.EnglobingFrame.IntermediateFrame.Label.Value.Help="Effort de cisaillement du mat�riau";
SAMShearStressLimit.EnglobingFrame.IntermediateFrame.Label.Value.LongHelp="Effort de cisaillement";

