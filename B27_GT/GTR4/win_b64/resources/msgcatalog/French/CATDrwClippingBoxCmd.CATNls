//=============================================================================
//                          COPYRIGHT Dassault Systemes 2005
//-----------------------------------------------------------------------------
// FILENAME    :    CATDrwClippingBoxCmd
// LOCATION    :    DraftingGenUI/CNext/resources/msgcatalog/
// AUTHOR      :    wsa
// DATE        :    May 2005
//------------------------------------------------------------------------------
// DESCRIPTION :    Resources File for NLS purpose related to CATDrwClippingBoxCmd
//                  
//------------------------------------------------------------------------------
// COMMENTS    :	 ENGLISH VERSION
//------------------------------------------------------------------------------
//MODIFICATIONS     user  date        purpose
//   HISTORY        ----  ----        -------
//   01.            apr3  19.04.2016  Revision
//------------------------------------------------------------------------------

CATDrwClippingBoxCmd.initialState.Message="Cliquer pour sortir";
CATDrwClippingBoxCmd.Title="Objet de coupure";
CATDrwClippingBoxCmd.ModeFrame="Mode Coupure";
CATDrwClippingBoxCmd.Mode1="Zone de coupure";
CATDrwClippingBoxCmd.Mode2="Coupure par tranche";
CATDrwClippingBoxCmd.Mode3="Plan de d�coupe arri�re";
CATDrwClippingBoxCmd.InputFrame="Entr�e utilisateur";
CATDrwClippingBoxCmd.PointFrame.Title="Centre";
CATDrwClippingBoxCmd.PointFrame.ShortHelp="Centre de l'objet de coupure";
CATDrwClippingBoxCmd.PointFrame.Help="Centre de l'objet de coupure";
CATDrwClippingBoxCmd.PointFrame.LongHelp="Centre de l'objet de coupure";
CATDrwClippingBoxCmd.DimensionFrame.Title="Dimension";
CATDrwClippingBoxCmd.DimensionFrame.ShortHelp="Dimension de l'objet de coupure. La dimension minimale autoris�e (par rapport � la tol�rance de dessin) est de 1 mm.";
CATDrwClippingBoxCmd.DimensionFrame.Help="Dimension de l'objet de coupure. La dimension minimale autoris�e (par rapport � la tol�rance de dessin) est de 1 mm.";
CATDrwClippingBoxCmd.DimensionFrame.LongHelp="Dimension de l'objet de coupure. La dimension minimale autoris�e (par rapport � la tol�rance de dessin) est de 1 mm.";
CATDrwClippingBoxCmd.Label1=" X ";
CATDrwClippingBoxCmd.Label2=" Y ";
CATDrwClippingBoxCmd.Label3=" Z ";
CATDrwClippingBoxCmd.Label4=" Longueur ";
CATDrwClippingBoxCmd.Label5=" Largeur ";
CATDrwClippingBoxCmd.Label6=" Hauteur ";
CATDrwClippingBoxCmd.CreateModifyPushButton.Title="      Cr�er      ";
CATDrwClippingBoxCmd.CreateModifyPushButton.ShortHelp="Cr�e un objet de coupure.";
CATDrwClippingBoxCmd.CreateModifyPushButton.Help="Cr�e un objet de coupure dans la vue.";
CATDrwClippingBoxCmd.CreateModifyPushButton.LongHelp="Cr�e un objet de coupure dans la vue et le met � jour. Permet en outre de fermer la fen�tre.";
CATDrwClippingBoxCmd.CommandMode2.Title="      Modifier      ";
CATDrwClippingBoxCmd.OKPushButton.CommandMode2.ShortHelp="Modifie un objet de coupure.";
CATDrwClippingBoxCmd.OKPushButton.CommandMode2.Help="Modifie un objet de coupure dans la vue.";
CATDrwClippingBoxCmd.OKPushButton.CommandMode2.LongHelp="Modifie un objet de coupure dans la vue et le met � jour. Permet en outre de fermer la fen�tre.";
CATDrwClippingBoxCmd.CancelPushButton.Title="      Annuler      ";
CATDrwClippingBoxCmd.CancelPushButton.ShortHelp="Annule la commande.";
CATDrwClippingBoxCmd.CancelPushButton.Help="Annule la commande et ferme la fen�tre.";
CATDrwClippingBoxCmd.CancelPushButton.LongHelp="Annule la commande et ferme la fen�tre. Ne permet ni de cr�er ni de modifier un objet de coupure.";
CATDrwClippingBoxCmd.ApplyPushButton.Title="      Aper�u      ";
CATDrwClippingBoxCmd.ApplyPushButton.ShortHelp="Applique l'objet de coupure";
CATDrwClippingBoxCmd.ApplyPushButton.Help="Applique l'objet de coupure et met � jour la vue.";
CATDrwClippingBoxCmd.ApplyPushButton.LongHelp="Applique l'objet de coupure et met � jour la vue. Ne permet pas de fermer la fen�tre.";
CATDrwClippingBoxCmd.ModeComboBox.ShortHelp="Mode de s�lection";
CATDrwClippingBoxCmd.ModeComboBox.Help="Permet de s�lectionner l'un des trois modes de coupure d'objet.";
CATDrwClippingBoxCmd.ModeComboBox.LongHelp="Permet de s�lectionner l'un des trois modes de coupure d'objet.";
CATDrwClippingBoxCmd.PointSpinner1.ShortHelp="Coordonn�e X du centre";
CATDrwClippingBoxCmd.PointSpinner1.Help="Coordonn�e X du centre de l'objet de coupure.";
CATDrwClippingBoxCmd.PointSpinner1.LongHelp="Coordonn�e X du centre de l'objet de coupure.";
CATDrwClippingBoxCmd.PointSpinner2.ShortHelp="Coordonn�e Y du centre";
CATDrwClippingBoxCmd.PointSpinner2.Help="Coordonn�e Y du centre de l'objet de coupure.";
CATDrwClippingBoxCmd.PointSpinner2.LongHelp="Coordonn�e Y du centre de l'objet de coupure.";
CATDrwClippingBoxCmd.PointSpinner3.ShortHelp="Coordonn�e Z du centre";
CATDrwClippingBoxCmd.PointSpinner3.Help="Coordonn�e Z du centre de l'objet de coupure.";
CATDrwClippingBoxCmd.PointSpinner3.LongHelp="Coordonn�e Z du centre de l'objet de coupure.";
CATDrwClippingBoxCmd.DimensionSpinner1.ShortHelp="Longueur de la coupure 3D. En mode de coupure par tranche, il s'agit de la distance entre les plans de coupure arri�re et avant.";
CATDrwClippingBoxCmd.DimensionSpinner1.Help="Longueur de la coupure 3D. En mode de coupure par tranche, il s'agit de la distance entre les plans de coupure arri�re et avant.";
CATDrwClippingBoxCmd.DimensionSpinner1.LongHelp="Longueur de la coupure 3D. En mode de coupure par tranche, il s'agit de la distance entre les plans de coupure arri�re et avant.";
CATDrwClippingBoxCmd.DimensionSpinner2.ShortHelp="Largeur de la coupure 3D";
CATDrwClippingBoxCmd.DimensionSpinner2.Help="Largeur de la coupure 3D.";
CATDrwClippingBoxCmd.DimensionSpinner2.LongHelp="Largeur de la coupure 3D.";
CATDrwClippingBoxCmd.DimensionSpinner3.ShortHelp="Hauteur de la coupure 3D";
CATDrwClippingBoxCmd.DimensionSpinner3.Help="Hauteur de la coupure 3D.";
CATDrwClippingBoxCmd.DimensionSpinner3.LongHelp="Hauteur de la coupure 3D.";
CATDrwClippingBoxCmd.3DViewer.ShortHelp="Pi�ce ou produit 3D avec l'objet de coupure";
CATDrwClippingBoxCmd.3DViewer.Help="Pi�ce ou produit 3D avec l'objet de coupure. D�finit l'objet de coupure dans la vue 3D.";
CATDrwClippingBoxCmd.3DViewer.LongHelp="Pi�ce ou produit 3D avec l'objet de coupure. D�finit l'objet de coupure dans le visualiseur 3D. La fl�che bleue indique la direction de vue.";
ProgressTaskUI.Title="Zone de coupure";
CATDrwClippingBoxCmd.InitBox.Message="Manipuler l'objet de coupure.";
CATDrwClippingBoxCmd.ModifiedClippingObject.Message="Manipuler l'objet de coupure.";
CATDrwClippingBoxCmd.ResetManipButton="Effacer le manipulateur en cours";
CATDrwClippingBoxCmd.UndoTitle="Ajouter ou modifier une d�coupe 3D";

