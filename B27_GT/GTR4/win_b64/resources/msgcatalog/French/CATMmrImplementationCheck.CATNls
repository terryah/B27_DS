//======================================================================
//   Errors messages relative to adhesion
//======================================================================

NoOverloadedAdhesion="Le d�marrage de \"/p1\" n'est pas �tendu � \"/p2\" (\"/p3\" par d�faut), bien qu'il s'agisse d'un �l�ment g�om�trique 3D ou qu'il est compatible avec CATI3DGeometricalElement.";
MissingAdhesion="Le d�marrage de \"/p1\" n'est pas �tendu � \"/p2\", bien qu'il s'agisse d'un �l�ment g�om�trique 3D ou qu'il est compatible avec CATI3DGeometricalElement.";
UnrelevantAdhesion="Le d�marrage de \"/p1\" est compatible avec l'interface \"/p2\", bien qu'il ne s'agisse pas d'un �l�ment g�om�trique 3D et qu'il n'est pas compatible avec CATI3DGeometricalElement.";
AdaptatorNotUsed="L'impl�mentation du d�marrage \"/p1\" de \"/p2\" ne d�coule pas de \"/p3\".";


//======================================================================
//   Errors messages relative to CATIInputDescription implementation
//======================================================================

UnsetSuccessfullGetFeatureType="\"/p\" a un type de fonction non d�fini.";
ValuedFeatureTypeFailedGetFeatureType="\"/p\" a un type de fonction d�fini mais la m�thode GetFeatureType a �chou�.";
CreationFeatureWithModifiedFeaturesNotReturningEFail="\"/p\" est une fonction de cr�ation et GetListOfModifiedFeatures ne renvoie pas E_FAIL.";
CreationFeatureWithNotVoidListOfModifiedFeatures="\"/p\" est une fonction de cr�ation et GetListOfModifiedFeatures ne renvoie pas une liste vide.";
CreationFeatureWithGetMainInputNotReturningEFail="\"/p\" est une fonction de cr�ation et GetMainInput ne renvoie pas E_FAIL.";
ContextualCreationFeatureWithModifiedFeaturesNotReturningEFail="\"/p\" est une fonction de cr�ation contextuelle et GetListOfModifiedFeatures ne renvoie pas E_FAIL.";
ContextualCreationFeatureWithNotVoidListOfModifiedFeatures="\"/p\" est une fonction de cr�ation contextuelle et GetListOfModifiedFeatures ne renvoie pas une liste vide.";
ContextualCreationFeatureWithGetMainInputReturningEFail="\"/p\" est une fonction de cr�ation contextuelle et GetMainInput renvoie E_FAIL.";
ContextualCreationFeatureWithGetMainInputReturningNul="\"/p\" est une fonction de cr�ation contextuelle et GetMainInput renvoie une fonction nulle.";
ModificationFeatureWithModifiedFeaturesReturningEFail="\"/p\" est une fonction de modification et GetListOfModifiedFeatures renvoie E_FAIL.";
ModificationFeatureWithVoidListOfModifiedFeatures="\"/p\" est une fonction de modification et GetListOfModifiedFeatures renvoie une liste vide.";
ModificationFeatureWithGetMainInputReturningEFail="\"/p\" est une fonction de modification et GetMainInput renvoie E_FAIL.";
ModificationFeatureWithGetMainInputReturningNul="\"/p\" est une fonction de modification et GetMainInput renvoie une fonction nulle.";
ModificationFeatureWithGetMainInputNotReturningModifiedFeature="\"/p\" est une fonction de modification et GetMainInput ne renvoie pas de fonction modifi�e.";
ModificationFeatureShapeWithInvalidTypeOfModifiedFeature="\"/p1\" est une fonction surfacique et la fonction modifi�e \"/p2\" n'est pas une surface.";
ModificationFeatureSolidWithInvalidTypeOfModifiedFeature="\"/p1\" est une fonction de modification solide et la fonction modifi�e \"/p2\" n'est pas un solide.";
ModificationFeatureVolumeWithInvalidTypeOfModifiedFeature="\"/p1\" est une fonction de modification volumique et la fonction modifi�e \"/p2\" est un volume.";
ModificationFeatureNotShapeWithInvalidTypeOfModifiedFeature="\"/p1\" n'est pas une fonction de modification surfacique et la fonction modifi�e \"/p2\" est une surface.";
ModificationFeatureNotSolidWithInvalidTypeOfModifiedFeature="\"/p1\" n'est pas une fonction de modification solide et la fonction modifi�e \"/p2\" est un solide.";
ModificationFeatureNotVolumeWithInvalidTypeOfModifiedFeature="\"/p1\" n'est pas une fonction de modification volumique et la fonction modifi�e \"/p2\" est un volume.";
GetAbsorbingsFailed="Echec de la m�thode d'extraction du composant annul� \"/p1\" � partir du composant absorb� \"/p2\"";
FeatureNotReturnedByGetAbsorbings="Le composant \"/p1\" n'a pas �t� extrait du composant absorb� \"/p2\".";
SolidResultINNotAbsorbed="Le solide \"/p1\" n'absorbe pas son ResultIN \"/p2\".";
SolidResultINNotAbsorbedBecauseOfDeactivation="Le solide \"/p1\" n'absorbe pas son ResultIN \"/p2\" car son entr�e principale est d�sactiv�e \"old way\".";
GetAbsorbingAndGetAbsorbedNotSymetric="\"/p1\" : le r�sultat fourni par GetAbsorbing (\"/p2\") ne se trouve pas dans la liste renvoy�e par GetAbsorbed";

//===================================================================================
//   Errors messages relative to CATIMf3DBehavior/CATIMf3DBehavior2 implementation
//===================================================================================

WrongReturnValueForIsAShape="La m�thode CATIMf3DBehavior: : IsAShape appel�e sur \"/p\" a retourn� un HRESULT erron�.";
WrongReturnValueForIsASolid="La m�thode CATIMf3DBehavior: : IsASolid appel�e sur \"/p\" a retourn� un HRESULT erron�.";
WrongReturnValueForIsADatum="La m�thode CATIMf3DBehavior: : IsADatum appel�e sur \"/p\" a retourn� un HRESULT erron�.";
WrongReturnValueForIsAVolume="La m�thode CATIMf3DBehavior: : IsAVolume appel�e sur \"/p\" a retourn� un HRESULT erron�.";
BothIsASolidAndIsAShape="\"/p\" est � la fois un solide et une forme.";
NeitherIsASolidAndIsAShape="\"/p\" n'est ni un solide, ni une forme.";
IsAVolumeButNotAShape="\"/p\" est un volume, mais pas une forme.";
IsADatumWithInAttr="\"/p1\" est un �l�ment sans historique, mais dispose de l'attribut \"/p2\" sp_IN.";
IsADatumWithOutAttr="\"/p1\" est un �l�ment sans historique, mais dispose de l'attribut \"/p2\" sp_OUT.";


//===================================================================================
//   Errors messages relative to CATIMechanicalTool implementation
//===================================================================================

AdhesionButNotMR="\"/p\" est compatible avec CATIMechanicalTool mais ne d�coule pas de MechanicalRoot.";
AdhesionButNotBasicTool="\"/p\" est compatible avec CATIMechanicalTool mais pas avec CATIBasicTool.";
ZeroGetVisuStatusNotMT="GetVisuStatus sur \"/p\" renvoie 0 alors qu'il ne s'agit pas d'un MechanicalTool.";
ZeroGetVisuStatusNotAggregatedUnderMP="GetVisuStatus sur \"/p\" renvoie 0 alors qu'il est agr�g� sous la pi�ce m�canique.";
ZeroGetVisuStatusNotOperated="GetVisuStatus sur \"/p\" renvoie 0 alors qu'il n'est op�r� par aucun �l�ment m�canique.";
ZeroGetVisuStatusOperatedInsideOrderedSet="GetVisuStatus sur \"/p\" renvoie 0 alors qu'il est op�r� par un �l�ment � l'int�rieur d'un set ordonn�.";
InvalidGetVisuStatus="GetVisuStatus sur \"/p\" renvoie une valeur non valide, seuls 0 et 1 sont valides.";
InvalidNullGetBooleanOperationForMT="GetBooleanOperation sur \"/p1\" renvoie NULL_var alors qu'il est op�r� par \"/p2\".";
InvalidValuatedGetBooleanOperationForMT="GetBooleanOperation sur \"/p1\" renvoie \"/p2\" alors qu'il n'est op�r� par aucun �l�ment m�canique.";
InvalidNulGetBooleanOperationForHB="GetBooleanOperation sur \"/p1\" est nul alors qu'il est op�r� par son parent \"/p2\".";
InvalidValuatedGetBooleanOperationFather="GetBooleanOperation sur \"/p1\" renvoie \"/p2\" et non son parent, \"/p3\".";
InvalidValuatedGetBooleanOperationOperating="GetBooleanOperation sur \"/p1\" renvoie \"/p2\" mais devrait �tre nul car il n'est pas op�r�.";
CyclicBooleanOperation="L'op�ration bool�enne \"/p1\" faisant r�f�rence � l'ensemble \"/p2\" g�n�re un cycle.";

//===================================================================================
//   Errors messages relative to CATISpecVisible implementation
//===================================================================================
PublicNotInSpecTree="L'�l�ment public \"/p\" au sens de CATISpecVisible est absent de l'arbre de sp�cifications.";
PrivateInSpecTree="L'�l�ment priv� \"/p\" au sens de CATISpecVisible appara�t dans l'arbre de sp�cifications.";
PrivateChildInSpecTree="L'impl�mentation de CATISpecVisibleChild d�clare par erreur l'�l�ment priv� \"/p1\" de \"/p2\" comme �tant priv� lorsqu'il appara�t dans l'arbre de sp�cifications.";
ParmNotAdhereCATISpecVisible="\"/p\" n'est pas compatible avec CATISpecVisible.";
PublicParmNotVisible="Le param�tre \"/p\" expos� � Knowledge est anormalement d�clar� comme �tant priv� par l'impl�mentation CATISpecVisibleChild.";

//===================================================================================
//   Errors messages relative to CATIReplace implementation
//===================================================================================
NoAdhesionForReplace="\"/p1\" n'est pas compilable avec CATIReplace bien qu'il pointe vers l'entr�e \"/p2\" interne par le biais de l'attribut \"/p3\".";
