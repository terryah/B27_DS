//=============================================================================
//                                     CNEXT - CXR1
//                          COPYRIGHT DASSAULT SYSTEMES 1998 
//-----------------------------------------------------------------------------
// FILENAME    :    CATPropEditView
// LOCATION    :    DraftingUI/CNext/resources/msgcatalog
// AUTHOR      :    tbe
// DATE        :    14/1/1998
//------------------------------------------------------------------------------
// DESCRIPTION :    Resources File for NLS purpose related to View property
//                  
//------------------------------------------------------------------------------
// COMMENTS    :	ENGLISH VERSION
//------------------------------------------------------------------------------
//MODIFICATIONS     user  date      purpose
//   HISTORY        ----  ----      -------
//		01			ogk		26.01.99	- ajout pour les dressups
//      02          ypr     10/05/00    - ajout pour thread
//		03			ogk		01.09.00	- ajout	viewNameLabel (correction)
//		04			ogk		24.10.00	- nouveau contenu (multi-sel)
//		05			lgk		26.06.02	Revision
//		06			lgk		15.10.02	Revision
//		07			lgk		17.12.02	Revision
//------------------------------------------------------------------------------


// Global Parameters 
// =================
globalParamsFrameTitle.Title="Echelle et orientation";
globalParamsFrame.Title="Param�tres globaux";
globalParamsFrame.LongHelp=
"Param�tres
Modifie les param�tres des vues s�lectionn�es : \"incr�ment angulaire\" et \"�chelle\"."; 


// Global Parameters / Scale
// =========================
globalParamsFrame.scaleEditors.scaleLabel.Title="     Echelle :";
globalParamsFrame.scaleEditors.scaleLabel.Help="Modifie l'�chelle des vues s�lectionn�es";
globalParamsFrame.scaleEditors.scaleLabel.ShortHelp="Echelle de vue";
globalParamsFrame.scaleEditors.scaleLabel.LongHelp="Modifie l'�chelle des vues s�lectionn�es ;  
La nouvelle valeur doit �tre un r�el.";
//
globalParamsFrame.scaleEditors.scaleSepLabel.Title=":";



// Global Parameters / Angle
// =========================
globalParamsFrame.angleLabel.Title="Rotation :";
globalParamsFrame.angleLabel.Help="Associe un incr�ment angulaire aux vues s�lectionn�es";
globalParamsFrame.angleLabel.ShortHelp="Rotation de vue";
globalParamsFrame.angleLabel.LongHelp="Rotation de vue
Associe une valeur angulaire aux vues s�lectionn�es.
Par d�faut, cette valeur est exprim�e selon l'unit� en cours dans la session.
Quand vous entrez la valeur, vous n'avez pas besoin d'indiquer l'unit�.
Cependant, vous pouvez explicitement en pr�ciser une autre si besoin est
(exemple : entrez \"180 deg\" au lieu de \"3. 14\" si l'unit� en session est en radians). ";
//
globalParamsFrame.angleEdt.Title="Rotation :";
globalParamsFrame.angleEdt.Help="Associe un incr�ment angulaire aux vues s�lectionn�es";
globalParamsFrame.angleEdt.ShortHelp="Rotation de vue ";
globalParamsFrame.angleEdt.LongHelp="Rotation de vue
Associe une valeur angulaire aux vues s�lectionn�es.
Par d�faut, cette valeur est exprim�e selon l'unit� en cours dans la session.
Quand vous entrez la valeur, vous n'avez pas besoin d'indiquer l'unit�.
Cependant, vous pouvez explicitement en pr�ciser une autre si besoin est
(exemple : entrez \"180 deg\" au lieu de \"3. 14\" si l'unit� en session est en radians). ";



// Name Parameters 
// ===============
nameParamsFrameTitle.Title="Nom de la vue";
nameParamsFrame.Title="Nom de la vue";
nameParamsFrame.LongHelp="Nom des vues s�lectionn�es
Affiche le nom des vues s�lectionn�es.";


// Name Parameters / Prefix
// ========================
nameParamsFrame.prefixLabel.Title="Pr�fixe :";
nameParamsFrame.prefixLabel.Help="Modifie le pr�fixe du nom des vues s�lectionn�es";
nameParamsFrame.prefixLabel.ShortHelp="Pr�fixe";
nameParamsFrame.prefixLabel.LongHelp="Pr�fixe
Modifie le pr�fixe du nom des vues s�lectionn�es.";


// Name Parameters / Ident
// =======================
nameParamsFrame.identLabel.Title="Identificateur :";
nameParamsFrame.identLabel.Help="Modifie l'identificateur du nom des vues s�lectionn�es";
nameParamsFrame.identLabel.ShortHelp="Identificateur de nom";
nameParamsFrame.identLabel.LongHelp="Identificateur de nom
Modifie l'identificateur du nom des vues s�lectionn�es.";


// Name Parameters / Suffix
// ========================
nameParamsFrame.suffixLabel.Title="Suffixe :";
nameParamsFrame.suffixLabel.Help="Modifie le suffixe du nom des vues s�lectionn�es";
nameParamsFrame.suffixLabel.ShortHelp="Suffixe du nom";
nameParamsFrame.suffixLabel.LongHelp="Suffixe du nom
Modifie le suffixe du nom des vues s�lectionn�es.";


// Name Parameters / Name Editor
// =============================
nameParamsFrame.nameCkeEditorFrame.Title="Editeur du nom avec formule :";
nameParamsFrame.nameCkeEditorFrame.Name.EnglobingFrame.IntermediateFrame.Label.Title=" Editeur : ";


// Detail Name 
// ===========
detailParamsFrame.Title="Nom de vue de d�tail";
detailParamsFrame.ShortHelp="Nom de vue de d�tail";


// Detail Name / Editor
// ===========
detailviewTitle.Title="Nom de d�tail";
detailParamsFrame.detailLabel.Title=" Nom : ";
detailParamsFrame.detailLabel.Help="Modifie le nom des vues de d�tail s�lectionn�es";
detailParamsFrame.detailLabel.ShortHelp="Nom de d�tail";
detailParamsFrame.detailLabel.LongHelp="Nom de d�tail
Modifie le nom des vues de d�tail s�lectionn�es.";

// Dressup
// =======
dressupTitle.Title="Habillage";
generativeParamsFrame.Title="Habillage";
generativeParamsFrame.LongHelp=
"Habillage
Ajoute des �l�ments sur la (les) vue(s) s�lectionn�e(s)."; 



// Fillets
// =======
filletFrame.Title="Cong�s d'ar�tes";
filletFrame.LongHelp=
" Cong�s d'ar�tes\nPermet de configurer l'affichage des cong�s d'ar�tes. "; 

// Dressup / Hidden Lines 
// ======================
generativeParamsFrame.hiddenLinesChkBtn.Title="Lignes cach�es";
generativeParamsFrame.hiddenLinesChkBtn.LongHelp="Lignes cach�es
Affiche ou non les lignes cach�es dans les vues s�lectionn�es.";



// Dressup / Axis
// ==============
generativeParamsFrame.axisChkBtn.Title="Axe";
generativeParamsFrame.axisChkBtn.LongHelp="Axe
Affiche ou non les axes dans les vues s�lectionn�es.";


// Dressup / Center Line
// ======================
generativeParamsFrame.centerLinesChkBtn.Title="Axe vu de dessus";
generativeParamsFrame.centerLinesChkBtn.LongHelp="Axe vu de dessus
Affiche ou non les axes vus de dessus dans les vues s�lectionn�es.";



// Dressup / Thread
// ================
generativeParamsFrame.threadChkBtn.Title="Taraudage - Filetage";
//npq: 18/10/2005 IR 0513559 LongHelp added
generativeParamsFrame.threadChkBtn.LongHelp="Taraudage - Filetage
Affiche ou non les taraudages ou filetages dans les vues s�lectionn�es.";


// Dressup / Fillets
// =========================
filletFrame.boundFilletChkBtn.Title="Cong�s d'ar�tes";
filletFrame.boundFilletChkBtn.LongHelp="Cong�s d'ar�tes
D�finit le mode de repr�sentation des cong�s d'ar�tes dans les vues s�lectionn�es.";

// Dressup / BoundaryFillets
// =========================
filletFrame.boundaryRdBtn.Title="Limites des cong�s";
filletFrame.boundaryRdBtn.LongHelp="Limites
Indique si les limites des cong�s d'ar�tes doivent �tre affich�es dans les vues s�lectionn�es.";

// Dressup / Original Edges
// =========================
filletFrame.ficFilletRdBtn.Title="Ar�tes symboliques";
filletFrame.ficFilletRdBtn.LongHelp="Ar�tes symboliques
Indique si le mode d'extraction \"Ar�tes symboliques\" doit �tre utilis� dans les vues s�lectionn�es.";

filletFrame.projoriginaledgeFilletRdBtn.Title="Ar�tes d'origine projet�es";
filletFrame.projoriginaledgeFilletRdBtn.LongHelp="Ar�tes d'origine projet�es
Indique si le mode d'extraction \"Ar�tes d'origine projet�es\" doit �tre utilis� dans les vues s�lectionn�es.";

filletFrame.originaledgeFilletRdBtn.Title = "Ar�tes d'origine";
filletFrame.originaledgeFilletRdBtn.LongHelp="Ar�tes d'origine
Indique si le mode d'extraction \"Ar�tes d'origine\" doit �tre utilis� dans les vues s�lectionn�es.";

// Dressup / 3D Spec
// ====================
generativeParamsFrame.uncutSpecChkBtn.Title="Sp�cificit�s d�finies dans le 3D";
generativeParamsFrame.uncutSpecChkBtn.LongHelp="Sp�cificit�s d�finies dans le 3D
Affiche ou non les \"sp�cificit�s d�finies dans le 3D\" sur les vues s�lectionn�es.";

// Dressup / 3D Wireframe
// =======================
generativeParamsFrame.3DWireframeChkBtn.Title="El�ments 3D filaires";
generativeParamsFrame.3DWireframeChkBtn.LongHelp="El�ments 3D filaires
Affiche ou non les \"�l�ments 3D filaires\" sur les vues s�lectionn�es.";

// Dressup / 3D Wireframe A PARTIR DE LA R11
// =========================================
3DPointsFrame.3DWireframeFrame.3DWireframeChkBtn.Title="El�ments 3D filaires";
3DPointsFrame.3DWireframeFrame.3DWireframeChkBtn.LongHelp="El�ments 3D filaires
Indique si les �l�ments 3D filaires peuvent �tre cach�s 
ou sont toujours visibles sur les vues s�lectionn�es.";
3DPointsFrame.3DWireframeFrame.3DWireframeHLRRdBtn.Title="Peut �tre cach�";
3DPointsFrame.3DWireframeFrame.3DWireframeProjectionRdBtn.Title="Est toujours visible";

// Dressup / 3D Points
// ====================
generativeParamsFrame.3DPointsChkBtn.Title="Points 3D";
generativeParamsFrame.3DPointsChkBtn.LongHelp="Points 3D
Affiche ou non les \"Points 3D\" sur les vues s�lectionn�es.";
3DPointsFrame.3DPointsChkBtn.Title="Points 3D :";
3DPointsFrame.3DPointsChkBtn.LongHelp="Points 3D
Affiche ou non les \"Points 3D\" sur les vues s�lectionn�es.";
3DPointsFrame.3DSymbolRdBtn.Title="H�ritage du symbole 3D";
3DPointsFrame.3DSymbolRdBtn.LongHelp="H�ritage du symbole 3D
Affiche le symbole h�rit� du 3D";
3DPointsFrame.UsrSymbolRdBtn.Title="Symbole";
3DPointsFrame.UsrSymbolRdBtn.LongHelp="Symbole
Affiche le symbole s�lectionn�";

// Dressup / 3D Colors
// ===================
generativeParamsFrame.3DColorsChkBtn.Title="Couleurs 3D";
generativeParamsFrame.3DColorsChkBtn.LongHelp="Couleurs 3D
Utilise ou non les \"Couleurs 3D\" sur les vues s�lectionn�es.";

// Visu & Behaviour 
// ================
visuTitle.Title="Visualisation et comportement";
visuBehaviourFrame.Title="Visualisation et comportement";
visuBehaviourFrame.ShortHelp="Visualisation et comportement des vues s�lectionn�es";
visuBehaviourFrame.Help="Permet d'activer ou non l'affichage du cadre et d�finit le comportement des vues s�lectionn�es";
visuBehaviourFrame.LongHelp="Visualisation et comportement des vues s�lectionn�es
Permet d'activer ou non l'affichage du cadre et d�finit le comportement des vues s�lectionn�es.";



// Visu & Behaviour  / Frame
// =========================
visuBehaviourFrame.viewFrameChkBtn.Title="Cadre de la vue";
visuBehaviourFrame.viewFrameChkBtn.ShortHelp="Afficher le cadre de la vue";
visuBehaviourFrame.viewFrameChkBtn.Help="Affiche ou non le cadre qui entoure les vues s�lectionn�es";
visuBehaviourFrame.viewFrameChkBtn.LongHelp="Afficher le cadre de la vue
Affiche ou non le cadre qui entoure les vues s�lectionn�es.";

// Visu & Behaviour / visual clipping
//===================================
visuBehaviourFrame.FramingChkBtn.Title="Recadrage visuel";
visuBehaviourFrame.FramingChkBtn.ShortHelp="Recadrage visuel";
visuBehaviourFrame.FramingChkBtn.Help="Permet d'effectuer un recadrage visuel des vues s�lectionn�es";
visuBehaviourFrame.FramingChkBtn.LongHelp="Recadrage visuel                                  Permet d'effectuer un recadrage visuel des vues s�lectionn�es";

// Visu & Behaviour  / Lock
// ========================
visuBehaviourFrame.viewLockChkBtn.Title="Verrouillage de la vue";
visuBehaviourFrame.viewLockChkBtn.ShortHelp="Verrouillage";
visuBehaviourFrame.viewLockChkBtn.Help="Permet de verrouiller les vues s�lectionn�es";
visuBehaviourFrame.viewLockChkBtn.LongHelp="Verrouillage
Permet de verrouiller les vues s�lectionn�es.";

// Generation Mode 
// ================
generationModeTitle.Title="Mode de g�n�ration";
generationModeFrame.Title="Mode de g�n�ration";
generationModeFrame.ShortHelp="Mode de g�n�ration des vues s�lectionn�es";
generationModeFrame.LongHelp=
"Indique si les vues s�lectionn�es sont g�n�r�es � partir des 
donn�es exactes ou des donn�es CGR, et les re-g�n�re si n�cessaire.";


generationModeFrame.DrwGenerationForBox.Title="G�n�rer uniquement les pi�ces plus grandes que";
generationModeFrame.DrwGenerationForBox.LongHelp="G�n�rer uniquement les pi�ces plus grandes que";
generationModeFrame.spinGenBox.LongHelp="G�n�rer uniquement les pi�ces plus grandes que";
generationModeFrame.OcclusionCullingChkBtn.Title="Exclure les objets cach�s";
generationModeFrame.OcclusionCullingChkBtn.LongHelp="Exclure les objets cach�s
Les objets cach�s ne sont pas pris en compte par le calcul";
generationModeFrame.Combolabel.Title = "Mode de projection de la vue";
generationModeFrame.GenerationModeOption.Title = "Options";
GenerationModeOptionPanelTitle= "Options du mode de g�n�ration";
GenerationModeOptionPanelTitleHRV= "Mode approxim�";

// Messages divers
// ===============

TabTitle="Vue";
TabTitle.LongHelp="D�finit la vue.";

ScaleText="Echelle :";
ScaleOutOfRangeTitle="Echelle incorrecte";
ScaleOutOfRangeText="L'�chelle doit �tre une valeur positive inf�rieure � 10000. 
L'�chelle incorrecte a �t� automatiquement remise � 
la valeur limite la plus proche.";

gvsFrame.buttonResetStyle.Title="Revenir aux valeurs du style";
gvsFrame.Title="Style de vue g�n�rative";
GVSTitle.Title="Style de vue g�n�rative";
GVSOverloadHelp="Param�tre surcharg� par l'utilisateur";
gvsFrame.buttonRemoveStyle.Title="Supprimer style";

