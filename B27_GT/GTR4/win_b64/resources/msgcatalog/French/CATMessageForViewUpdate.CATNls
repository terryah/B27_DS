//=============================================================================
//                                     CNEXT - CXR9
//                          COPYRIGHT DASSAULT SYSTEMES 2002 
//-----------------------------------------------------------------------------
// FILENAME    :    CATMessageForViewUpdate
// LOCATION    :    DraftingGenModeler/CNext/resources/msgcatalog
// AUTHOR      :    cna
// DATE        :    fevrier 2002
//------------------------------------------------------------------------------
// DESCRIPTION :    Resources File for NLS purpose
//                  
//------------------------------------------------------------------------------
// COMMENTS    :    Il s'agit de la definition des messages courts figurant
//                  dans la progress barre d'update de vue.
//------------------------------------------------------------------------------
//MODIFICATIONS     user  date      purpose
//   HISTORY        ----  ----      -------
//     01           cna   14/02/02  creation
//     02           emk   16/10/03  ajout des messages d'erreurs pouvant survenir durant l'update des vues
//------------------------------------------------------------------------------

Geo3D="Etape 1/5";
PrePro="Etape 2/5";
HLR="Etape 3/5";
PostPro="Etape 4/5";
Dressup="Etape 5/5";

ViewName = "Vue : ";
SheetName = "Calque : ";
Message = "Description : ";

UnspecifiedViewError = "La g�n�ration de la vue a �chou�.";
BooleanPartViewError = "Le calcul de l'intersection a �chou� pour le corps'/p2' de la pi�ce '/p1'";
BooleanModelViewError = "Le calcul de l'intersection a �chou� pour le mod�le '/p1'";
BooleanSectionCutViewError = "Le calcul de section a �chou� pour le corps '/p2' de la pi�ce '/p1'";
RasterViewError = "La vue est en mode Image bitmap et la taille de l'image (/p1 x /p2) d�passe les capacit�s de la m�moire. \nEssayez de r�duire l'�chelle de la vue et le niveau de d�tail dans les propri�t�s de la vue.";
RasterBuildImageViewError = "L'image ne peut pas �tre construite. \nEssayez de r�duire l'�chelle de la vue et le niveau de d�tail dans les propri�t�s de la vue.";
QuickDetailViewError = "Echec du calcul de la vue de d�tail rapide.";
NoGeometryViewError = "La vue ne contient aucun �l�ment g�om�trique.";
BrokenViewError = "Echec du calcul de la vue interrompue.";
MemoryViewError = "Echec d'allocation m�moire. \nLa quantit� d'informations � traiter et/ou les options de g�n�ration de vue d�passent les capacit�s m�moire. \nIl est conseill� d'enregistrer vos documents et de red�marrer une session.";
AIXApproximateViewError = "Le mode de g�n�ration de vue Approxim� n'est pas effectif sur les syst�mes d'exploitation AIX.";
BreakoutApproximateError = "Au moins une vue �corch�e ne concorde pas avec cette vue approxim�e. \nDans les vues approxim�es, la direction de la vue �corch�e doit �tre parall�le � la normale du plan de la vue et dans la direction inverse. \nSupprimez ces vues �corch�es pour g�n�rer la vue.";
BreakoutSectionApproximateError = "Au moins une vue �corch�e ne concorde pas avec cette vue en coupe approxim�e. \nDans les vues en coupe approxim�es, le plan arri�re de la vue �corch�e doit se trouver derri�re le plan en coupe distant. \nSupprimez cette vue �corch�e pour g�n�rer la vue.";
SectionProfileError = "La d�finition du section profil de coupe est manquante. Impossible de mettre � jour la vue.";
GetMaxViewportSizeError = "Erreur interne. Retour GetMaxViewportSize (/p1 x /p2)";
NoElementToProject = "Aucun �l�ment � traiter";
EScaleIncompatibleView = "Le mode d'affichage de la g�n�ration n'est pas pris en charge dans les modes � petite et grande �chelle";
LowMemoryError = "La limite d'utilisation m�moire a �t� atteinte.\nLe document '/p1' ne peut pas �tre charg� pour la cr�ation ou la mise � jour de la vue. ";

ApproxSectionViewWarning = "Avertissement : vue en coupe approxim�e. Le profil de coupe n'est pas associatif et sa position doit �tre v�rifi�e.";
SynchroDuringUpdate="Echec de la synchronisation de la vue FTA ou 2DL pendant la mise � jour";
