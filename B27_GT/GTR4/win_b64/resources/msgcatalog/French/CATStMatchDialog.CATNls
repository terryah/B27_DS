//=============================================================================
// COPYRIGHT DASSAULT SYSTEMES 1999
//=============================================================================
//
// Match Surface Panel :
//   Resource file for NLS purpose.
//
//=============================================================================
//
// Implementation Notes :
//
//=============================================================================
// Feb. 99   Creation                                   Jean-Michel PLOUHINEC
//=============================================================================

Title="Connexion de surface";
TypeOfMatchFrame.Title="Type";
TypeOfMatchCombo.Bas="Analytique   ";
TypeOfMatchCombo.Adv="Approch� ";
TypeOfMatchCombo.Aut="Auto       ";
More.Title="Plus >>";
Less.Title="Moins <<";

InformationsFrame.NbPatches.Title="Nombre de carreaux : ";
InformationsFrame.NbPatch.LongHelp=
"Affiche le nombre de carreaux
de la surface � connecter.";

InformationsFrame.Orders.Title="Ordre : ";
InformationsFrame.Order.LongHelp=
"Affiche l'ordre de
la surface � connecter.";

InformationsFrame.Types.Title="Type : ";
InformationsFrame.Type.LongHelp="Affiche le type de connexion.";

InformationsFrame.Deltas.Title="Delta : ";
InformationsFrame.Delta.LongHelp="Affiche la d�viation par rapport � la surface initiale.";

LocalTangent.Context.User.Title="Utilisateur";
LocalTangent.Context.Initial.Title="Initial";
LocalTangent.Context.Reference.Title="Cible";
LocalTangent.Context.Fix.Title="Fixe";
LocalTangent.Context.User.ShortTitle="Utilisateur";
LocalTangent.Context.Initial.ShortTitle="Init";
LocalTangent.Context.Reference.ShortTitle="Cible";
LocalTangent.Context.Fix.ShortTitle="Fixe";

DegreeAjustment.Auto.MenuTitle="Auto";
DegreeAjustment.Auto.DisplayedTitle="Automatique";

InformationsFrame.Title="Informations";
OptionsFrame.Title="Options";
MoreLessFrame.DisplayFrame.Title="Affichage";

OptionsFrame.Projection.Title="Projection des extr�mit�s";
OptionsFrame.Projection.ShortHelp="Projette les points extr�mit�s";
OptionsFrame.Projection.LongHelp=
"Projette les points extr�mit�s
de la fronti�re sur la courbe cible dans les limites de celle-ci.";

OptionsFrame.Planar.Title="Projection de la fronti�re";
OptionsFrame.Planar.ShortHelp="Projette la fronti�re";
OptionsFrame.Planar.LongHelp=
"Contraint les points de contr�le � se d�placer le long de l'axe principal de la boussole.";

OptionsFrame.ProjectEdge.Title="Projeter la limite";
OptionsFrame.ProjectEdge.ShortHelp="Projeter la limite";
OptionsFrame.ProjectEdge.LongHelp=
"Projette la limite sur la face cible.";

OptionsFrame.Diffusion.Title="Diffusion";
OptionsFrame.Diffusion.ShortHelp="Propage la d�formation";
OptionsFrame.Diffusion.LongHelp=
"Propage la d�formation le long
de la direction transversale.";

MoreLessFrame.DisplayFrame.ContinuityConnection.Title="Continuit� des fronti�res";
MoreLessFrame.DisplayFrame.ContinuityConnection.ShortHelp="Affiche la continuit� des fronti�res";
MoreLessFrame.DisplayFrame.ContinuityConnection.LongHelp="Affiche la continuit� des fronti�res.";

MoreLessFrame.DisplayFrame.TensionLocalTangents.Title="Tension globale et tangentes locales";
MoreLessFrame.DisplayFrame.TensionLocalTangents.ShortHelp="Affiche la tension globale et les tangentes locales";
MoreLessFrame.DisplayFrame.TensionLocalTangents.LongHelp=
"Affiche les manipulateurs de tension globale et de tangentes locales.";

MoreLessFrame.DisplayFrame.LightCCDegree.Title="Analyse rapide de continuit�";
MoreLessFrame.DisplayFrame.LightCCDegree.ShortHelp="Affiche la d�viation maximum";
MoreLessFrame.DisplayFrame.LightCCDegree.LongHelp=
"Affiche la d�viation maximum
entre les surfaces.";

MoreLessFrame.DisplayFrame.CouplingPoints.Title="Points de contact";
MoreLessFrame.DisplayFrame.CouplingPoints.ShortHelp="Affiche les points de contact";
MoreLessFrame.DisplayFrame.CouplingPoints.LongHelp="Affiche les points de contact.";

MoreLessFrame.DisplayFrame.CtrlPts.Title="Points de contr�le";
MoreLessFrame.DisplayFrame.CtrlPts.ShortHelp="Affiche les points de contr�le";
MoreLessFrame.DisplayFrame.CtrlPts.LongHelp=
"Affiche les points de contr�le
sur la surface � connecter
� des fins de modification.";
