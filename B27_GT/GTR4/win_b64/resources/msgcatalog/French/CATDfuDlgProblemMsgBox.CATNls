//=============================================================================
//                                     CNEXT - CXR1
//                          COPYRIGHT DASSAULT SYSTEMES 1997 
//-----------------------------------------------------------------------------
// FILENAME    :    CATDfuDlgProblemMsgBox
// LOCATION    :    DraftingFeature/CNext/resources/msgcatalog/
// AUTHOR      :    OGK
// DATE        :    08.07.98
//------------------------------------------------------------------------------
// DESCRIPTION :    Resources File for NLS purpose related to warning/error messages
//                  
//------------------------------------------------------------------------------
// COMMENTS    :
//------------------------------------------------------------------------------
//MODIFICATIONS     user  date      purpose
//   HISTORY        ----  ----      -------
//		01	ogk	22.03.99	ajout lock de view + nettoyage
//		02	ogk	21.10.99	detection d'update impossible si modele < 418
//		03	lhz	09.11.00	creation d'une sheet de detail                
//------------------------------------------------------------------------------

ProjectionCreationTitle="Cr�ation de vue de projection";
AuxiliaryCreationTitle="Cr�ation de vue auxiliaire";
SectionCreationTitle="Cr�ation de vue de coupe/section";
DetailCreationTitle="Cr�ation de vue de d�tail";
DimensionCreationTitle="Cr�ation de cotes";
TextCreationTitle="Cr�ation de texte";
ArrowCreationTitle="Cr�ation de fl�che";
ToleranceCreationTitle="Cr�ation de tol�rance g�om�trique";
DatumFCreationTitle="Cr�ation de surface de r�f�rence";
DatumTCreationTitle="Cr�ation de zone de r�f�rence partielle";
RoughnessCreationTitle="Cr�ation de symbole de rugosit�";
BalloonCreationTitle="Cr�ation de num�ro de pi�ce";
AxisLineCreationTitle="Cr�ation d'axe milieu ";
CenterLineCreationTitle="Cr�ation d'axe centr� ";
ThreadCreationTitle="Cr�ation de filetage";
AreaFillCreationTitle="Cr�ation de hachures";
TableCreationTitle="Cr�ation de table";
ViewCreationText="La vue activ�e n'est pas � jour ou son lien vers son
document ne peut pas �tre trouv�.";
WeldingCreationTitle="Cr�ation de soudure";
IntCreationText="La vue s�lectionn�e ou active n'est pas � jour.";
IntCreationText3="Le lien entre la vue s�lectionn�e ou active et son document 
ne peut �tre trouv�.";

ViewIsLockedTitle="Vue verrouill�e";

ViewIsLockedText="La vue s�lectionn�e ou active est verrouill�e. 
De ce fait, l'action courante est impossible � ex�cuter et va �tre annul�e.  Le verrouillage de la vue s'effectue dans le panneau des propri�t�s de la vue.";

ViewIsLockedText2="L'action courante n'a pas �t� appliqu�e � la /aux vue(s) verrouill�e(s).";

ViewIsLockedText3="L'action courante a �t� annul�e car il y a au moins une vue 
verrouill�e dans la s�lection.";

ViewIsLockedText4="
Le verrouillage est imm�diat. 
Les zones modifi�es n'ont pas �t� enregistr�es. 
Vous pouvez toutefois d�verrouiller la vue pour enregistrer les modifications.";

UndefinedUpdateTitle="Statut de mise � jour";
UndefinedUpdateText="La vue que vous avez g�n�r�e provient d'un mod�le dont la version est
ant�rieure � la version 4. 18. 
Il sera donc impossible dans le futur de d�terminer si cette vue est � jour ou non. 
Nous vous conseillons donc de mettre � jour ce mod�le avec une version �quivalente ou plus r�cente que la version V4. 18.";

Information="Information";
Warning="Avertissement";
ViewAlreadyIsolatedText="La vue est d�j� isol�e";
ViewWillBeIsolatedText="Confirmer l'isolation de la vue : tous les �l�ments d'habillage vont �tre d�truits";
SheetCreatedText="Aucun calque de composant 2D n'est trouv�: Un calque de composant 2D est cr��.";			


BrokenCreationTitle="Cr�ation de vue bris�e";
BrokenCreationText="Commande annul�e : la vue active peut ne pas �tre g�n�rative, ne pas �tre � jour ou est verrouill�e.";

ChamferDimNotEditable="Les cotes de chanfrein ne peuvent pas �tre �dit�es";

ViewIsNotUpToDate="La vue s�lectionn�e ou active n'est pas � jour.";

InsertBackgroundTitle="Echec de l'ouverture";
InsertBackgroundText="Impossible d'ouvrir le document s�lectionn�. 
La raison principale tient au fait que la version du dessin que vous essayez d'ouvrir
est plus r�cente que la version de CATIA utilis�e. 
Essayez d'ouvrir un autre document CATDrawing.";
