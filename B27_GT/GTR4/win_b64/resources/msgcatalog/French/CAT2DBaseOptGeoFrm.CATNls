//=====================================================================================
//                                     CNEXT - CXR1
//                          COPYRIGHT DASSAULT SYSTEMES 1999 
//-------------------------------------------------------------------------------------
// FILENAME    :    CATDrwOptGeo
// LOCATION    :    DraftingUI
// AUTHOR      :    fgx
// BUT         :    
// DATE        :    06.01.1999
//-------------------------------------------------------------------------------------
// DESCRIPTION :    Tools/Option du Drafting - Onglet Geometry
//-------------------------------------------------------------------------------------
// COMMENTS    :    
//-------------------------------------------------------------------------------------
// MODIFICATIONS     user  date        purpose
//    HISTORY        ----  ----        -------
//      00           fgx   06.01.1999  Creation 	
//      01           fgx   08.01.1999  Passage sur CATDrwOptTitle et CATDrwOptIcon
//      02           fgx   20.01.1999  Ajout des options de visu des cst
//      03           fgx   20.01.1999  Deplacement des options de la grid de geo a gal
//      04           fgx   20.01.1999  Ajout de CreateCenter
//      05           fgx   22.02.1999  Ajout de CstCreation, retrait de CreateConstraint
//      06           fgx   23.08.1999  Manipulation directe de la geometrie
//      07           lgk   18.12.02    revision
//=====================================================================================

Title="G�om�trie";

frameCenter.HeaderFrame.Global.Title = "G�om�trie";
frameCenter.HeaderFrame.Global.LongHelp = "Donne la possibilit� de cr�er \ndes �l�ments suppl�mentaires\nlors de la cr�ation de l'esquisse.";
frameCenter.IconAndOptionsFrame.OptionsFrame.checkCenter.Title = "Cr�er le centre des cercles et des ellipses";
frameCenter.IconAndOptionsFrame.OptionsFrame.checkCenter.LongHelp = "Cr�e le centre des cercles et des ellipses\n lors de la cr�ation de ces �l�ments.";
frameCenter.IconAndOptionsFrame.OptionsFrame._checkDragWithEndPts.Title = "D�placer les �l�ments incluant leurs points extr�mit�s";
frameCenter.IconAndOptionsFrame.OptionsFrame._checkDragWithEndPts.LongHelp = "D�place les �l�ments en incluant leurs points extr�mit�s.";
frameCenter.IconAndOptionsFrame.OptionsFrame.chkEndPtsDuplGenGeom.Title = "Cr�er les points extr�mit�s lors de la duplication de la g�om�trie g�n�r�e";
frameCenter.IconAndOptionsFrame.OptionsFrame.chkEndPtsDuplGenGeom.LongHelp = "Cr�er les points extr�mit�s lorsque de la g�om�trie g�n�r�e depuis le 3D est dupliqu�e.";
frameCenter.IconAndOptionsFrame.OptionsFrame.chkShowHV.Title = "Afficher les champs H et V dans la palette d'outils";
frameCenter.IconAndOptionsFrame.OptionsFrame.chkShowHV.LongHelp = "Affiche les champs H et V dans la palette d'outils\nlorsque vous cr�ez de la g�om�trie 2D\nou lorsque vous d�calez des �l�ments.";

//drag elements
frameCenter.IconAndOptionsFrame.OptionsFrame.checkGeomDrag.Title="Autoriser la manipulation directe ";
frameCenter.IconAndOptionsFrame.OptionsFrame.checkGeomDrag.LongHelp="Permet de d�placer la g�om�trie � l'aide de la souris.";
frameCenter.IconAndOptionsFrame.OptionsFrame.dragOptionPushButton.Title="Mode de r�solution...";
frameCenter.IconAndOptionsFrame.OptionsFrame.dragOptionPushButton.LongHelp = "D�finit le mode de r�solution pour le d�placement des �l�ments.";

frameCstCreation.HeaderFrame.Global.Title="Cr�ation des contraintes ";
frameCstCreation.HeaderFrame.Global.LongHelp=
"Cr�e les contraintes d�tect�es 
ainsi que celles internes aux objets.";
frameCstCreation.IconAndOptionsFrame.OptionsFrame.checkCstCreation.Title="Cr�er les contraintes ";
frameCstCreation.IconAndOptionsFrame.OptionsFrame.checkCstCreation.LongHelp=
"Cr�e les contraintes d�tect�es 
ainsi que celles internes aux objets.";
frameCstCreation.IconAndOptionsFrame.OptionsFrame.framePushButton.smartPickPushButton.Title="SmartPick...";
frameCstCreation.IconAndOptionsFrame.OptionsFrame.framePushButton.smartPickPushButton.LongHelp="D�tecte de fa�on dynamique les contraintes logiques.";

frameCstVisu.HeaderFrame.Global.Title="Visualisation des contraintes";
frameCstVisu.HeaderFrame.Global.LongHelp=
"Configure la visualisation des contraintes en 2D.";
frameCstVisu.IconAndOptionsFrame.OptionsFrame.LongHelp=
"Configure la visualisation des contraintes en 2D.";
frameCstVisu.IconAndOptionsFrame.OptionsFrame.checkCstVisu.Title="Visualisation des contraintes ";
frameCstVisu.IconAndOptionsFrame.OptionsFrame.checkCstVisu.LongHelp=
"Configure la visualisation des types de contraintes suivants.";
frameCstVisu.IconAndOptionsFrame.OptionsFrame.labelCstRefSize.Title="Taille de r�f�rence : ";
frameCstVisu.IconAndOptionsFrame.OptionsFrame.labelCstRefSize.LongHelp="Utilise la taille de r�f�rence
comme r�f�rence dans l'affichage des symboles de contraintes.  Sa modification entra�ne la modification de la taille
de toutes les repr�sentations des contraintes.";
frameCstVisu.IconAndOptionsFrame.OptionsFrame.editorCstRefSize.LongHelp="Utilise la taille de r�f�rence
comme r�f�rence dans l'affichage des symboles de contraintes.  Sa modification entra�ne la modification de la taille
de toutes les repr�sentations des contraintes.";
frameCstVisu.IconAndOptionsFrame.OptionsFrame.labelCstColor.Title="Couleur des contraintes : ";
frameCstVisu.IconAndOptionsFrame.OptionsFrame.comboCstColor.LongHelp="Utilise la couleur des contraintes dans l'affichage des symboles de contraintes. 
Sa modification entra�ne la modification de la couleur
de toutes les repr�sentations de contraintes.";
frameCstVisu.IconAndOptionsFrame.OptionsFrame.labelCstColor.LongHelp="Utilise la couleur des contraintes dans l'affichage des symboles de contraintes. 
Sa modification entra�ne la modification de la couleur
de toutes les repr�sentations de contraintes.";
frameCstVisu.IconAndOptionsFrame.OptionsFrame.frameCTPushButton.cstTypesPushButton.Title="Types de contraintes...";
frameCstVisu.IconAndOptionsFrame.OptionsFrame.frameCTPushButton.cstTypesPushButton.LongHelp="D�finit les diff�rents types\nde contraintes � visualiser";


frameGeomManipDirect.HeaderFrame.Global.Title="Manipulateurs ";
frameGeomManipDirect.HeaderFrame.Global.LongHelp="D�finit si les manipulateurs doivent �tre activ�s par un clic
avant que la g�om�trie puisse �tre boug�e.";

frameGeomManipDirect.IconAndOptionsFrame.OptionsFrame.checkGeomManipDirect.Title="Autoriser la manipulation directe ";
frameGeomManipDirect.IconAndOptionsFrame.OptionsFrame.checkGeomManipDirect.LongHelp="Autoriser la manipulation directe
D�finit si les manipulateurs doivent �tre activ�s par un clic
avant que la g�om�trie puisse �tre boug�e.";


frameDiagColors.HeaderFrame.Global.Title = "Couleurs  ";
frameDiagColors.HeaderFrame.Global.LongHelp = "D�finit les couleurs des �l�ments de l'esquisse.";
frameDiagColors.IconAndOptionsFrame.OptionsFrame.checkVisualizationDiag.Title = "Visualisation des diagnostics ";
frameDiagColors.IconAndOptionsFrame.OptionsFrame.checkVisualizationDiag.LongHelp = "Configure la visualisation des diagnostics\nsur les �l�ments contraints.";
frameDiagColors.IconAndOptionsFrame.OptionsFrame.diagColorsPushButton.Title = "Couleurs...";
frameDiagColors.IconAndOptionsFrame.OptionsFrame.diagColorsPushButton.LongHelp = "D�finit les couleurs des diagnostics.";
frameDiagColors.IconAndOptionsFrame.OptionsFrame.labelOtherColors.Title = "Autres couleurs des �l�ments";
frameDiagColors.IconAndOptionsFrame.OptionsFrame.labelOtherColors.LongHelp = "D�finit les autres couleurs des �l�ments.";
frameDiagColors.IconAndOptionsFrame.OptionsFrame.otherColorsPushButton.Title = "Couleurs...";
frameDiagColors.IconAndOptionsFrame.OptionsFrame.otherColorsPushButton.LongHelp = "D�finit les autres couleurs des �l�ments.";


// NLS push button solving mode dans onglet geometry
geoAllowGeomManip.Title="D�placement des �l�ments";
geoAllowGeomManip.frameButtonDragElts.HeaderFrame.Global.Title = "D�placement des �l�ments  ";
geoAllowGeomManip.frameButtonDragElts.HeaderFrame.Global.LongHelp = "D�finit la fa�on dont les �l�ments sont d�plac�s.";
geoAllowGeomManip.frameButtonDragElts.IconAndOptionsFrame.OptionsFrame.checkDragWithEndPts.Title = "D�placer les �l�ments incluant leurs points extr�mit�s";
geoAllowGeomManip.frameButtonDragElts.IconAndOptionsFrame.OptionsFrame.checkDragWithEndPts.LongHelp = "D�place des �l�ments en incluant leurs points extr�mit�s.";
geoAllowGeomManip.frameButtonDragElts.IconAndOptionsFrame.OptionsFrame.frameDragMode.Title = "Mode de r�solution ";//for moving elements";
geoAllowGeomManip.frameButtonDragElts.IconAndOptionsFrame.OptionsFrame.frameDragMode.frameDragModelocked.radioDragModeStd.Title = "Mode standard";
geoAllowGeomManip.frameButtonDragElts.IconAndOptionsFrame.OptionsFrame.frameDragMode.frameDragModelocked.radioDragModeStd.LongHelp = "Utilise le mode standard pour le d�placement des �l�ments.";
geoAllowGeomManip.frameButtonDragElts.IconAndOptionsFrame.OptionsFrame.frameDragMode.frameDragModelocked.radioDragModeMin.Title = "D�placements minimum";
geoAllowGeomManip.frameButtonDragElts.IconAndOptionsFrame.OptionsFrame.frameDragMode.frameDragModelocked.radioDragModeMin.LongHelp = "D�place le moins d'�l�ments possible.";
geoAllowGeomManip.frameButtonDragElts.IconAndOptionsFrame.OptionsFrame.frameDragMode.frameDragModelocked.radioDragModeRel.Title = "Relaxation";
geoAllowGeomManip.frameButtonDragElts.IconAndOptionsFrame.OptionsFrame.frameDragMode.frameDragModelocked.radioDragModeRel.LongHelp = "Utilise la relaxation.";

// Push Button SmartPick
geoSmartPick.Title="SmartPick";
geoSmartPick.frameSmartPick.HeaderFrame.Global.Title="SmartPick";
geoSmartPick.frameSmartPick.HeaderFrame.Global.LongHelp=
"SmartPick
geoSmartPick. Permet de d�tecter automatiquement l'emplacement des �l�ments de g�om�trie 2D.";
geoSmartPick.frameSmartPick.IconAndOptionsFrame.OptionsFrame.checkSmartPickCoincid.Title="Support des droites et cercles";
geoSmartPick.frameSmartPick.IconAndOptionsFrame.OptionsFrame.checkSmartPickCoincid.LongHelp=
"D�tecte le support des droites et cercles.";
geoSmartPick.frameSmartPick.IconAndOptionsFrame.OptionsFrame.checkSmartPickAlign.Title="Alignement ";
geoSmartPick.frameSmartPick.IconAndOptionsFrame.OptionsFrame.checkSmartPickAlign.LongHelp=
"D�termine que deux �l�ments seront align�s l'un par rapport � l'autre.";
geoSmartPick.frameSmartPick.IconAndOptionsFrame.OptionsFrame.checkSmartPickParaPerpenTang.Title="Parall�lisme, perpendicularit� et tangence ";
geoSmartPick.frameSmartPick.IconAndOptionsFrame.OptionsFrame.checkSmartPickParaPerpenTang.LongHelp=
"D�termine que deux �l�ments seront parall�les, perpendiculaires 
ou tangents l'un par rapport � l'autre.";
geoSmartPick.frameSmartPick.IconAndOptionsFrame.OptionsFrame.checkSmartPickHoriVerti.Title="Horizontalit� et verticalit�";
geoSmartPick.frameSmartPick.IconAndOptionsFrame.OptionsFrame.checkSmartPickHoriVerti.LongHelp=
"D�termine que deux �l�ments sont horizontaux ou verticaux l'un par rapport � l'autre.";

// Constraints types
geoTypeCst.Title="Types de contraintes";
geoTypeCst.frameConstraintsTypes.HeaderFrame.Global.Title="Types de contraintes";
geoTypeCst.frameConstraintsTypes.HeaderFrame.Global.LongHelp="D�finit les diff�rents types\nde contraintes � visualiser";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuHori.Title="Horizontalit� ";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuHori.LongHelp="Horizontalit� 
Affiche les contraintes d'horizontalit�.";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuVerti.Title="Verticaux";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuVerti.LongHelp="Verticalit� 
Affiche les contraintes de verticalit�.";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuPara.Title="Parall�lisme ";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuPara.LongHelp="Parall�lisme 
Affiche les contraintes de parall�lisme.";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuPerpen.Title="Perpendicularit� ";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuPerpen.LongHelp="Perpendicularit� 
Affiche les contraintes de perpendicularit�.";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuConcen.Title="Concentricit� ";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuConcen.LongHelp="Concentricit� 
Affiche les contraintes de concentricit�.";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuCoinci.Title="Co�ncidence ";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuCoinci.LongHelp="Co�ncidence 
Affiche les contraintes de co�ncidence.";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuTangen.Title="Tangence ";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuTangen.LongHelp="Tangence 
Affiche les contraintes de tangence.";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuSym.Title="Sym�trie ";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuSym.LongHelp="Sym�trie 
Affiche les contraintes de sym�trie.";

// NLS de l'onglet Geometry pour le diagnostic des couleurs
geoVisuDiagColor.Title="Couleurs des diagnostics";
geoVisuDiagColor.frameButtonDiagColors.HeaderFrame.Global.Title = "Couleurs des diagnostics";
geoVisuDiagColor.frameButtonDiagColors.HeaderFrame.Global.LongHelp = "Couleurs des diagnostics\nD�finit les couleurs des diagnostics.";
geoVisuDiagColor.frameButtonDiagColors.IconAndOptionsFrame.OptionsFrame.labelOverConsElts.Title = "El�ments surcontraints ";
geoVisuDiagColor.frameButtonDiagColors.IconAndOptionsFrame.OptionsFrame.labelOverConsElts.LongHelp = "El�ments surcontraints\nD�finit la couleur des �l�ments dans un mod�le poss�dant trop de contraintes.";
geoVisuDiagColor.frameButtonDiagColors.IconAndOptionsFrame.OptionsFrame.labelIsoConsElts.Title = "El�ments isocontraints ";
geoVisuDiagColor.frameButtonDiagColors.IconAndOptionsFrame.OptionsFrame.labelIsoConsElts.LongHelp = "El�ments isocontraints\nD�finit la couleur des �l�ments dans un mod�le poss�dant les contraintes n�cessaires.";
geoVisuDiagColor.frameButtonDiagColors.IconAndOptionsFrame.OptionsFrame.labelInconsElts.Title = "El�ments inconsistants ";
geoVisuDiagColor.frameButtonDiagColors.IconAndOptionsFrame.OptionsFrame.labelInconsElts.LongHelp = "El�ments inconsistants\nD�finit la couleur des �l�ments dans un mod�le o� se trouvent des donn�es inconsistantes.";
geoVisuDiagColor.frameButtonDiagColors.IconAndOptionsFrame.OptionsFrame.labelNotChanElts.Title = "El�ments irr�solus ";
geoVisuDiagColor.frameButtonDiagColors.IconAndOptionsFrame.OptionsFrame.labelNotChanElts.LongHelp = "El�ments irr�solus\nD�finit la couleur des �l�ments dans un mod�le irr�solu.";

// NLS de l'onglet Geometry pour Other Colors
geoOtherColor.Title="Couleurs d'autres �l�ments";
geoOtherColor.frameButtonOtherColors.HeaderFrame.Global.Title = "Couleurs  ";
geoOtherColor.frameButtonOtherColors.HeaderFrame.Global.LongHelp = "Couleurs\nD�finit les couleurs des �l�ments.";
geoOtherColor.frameButtonOtherColors.IconAndOptionsFrame.OptionsFrame.labelConstElts.Title = "El�ments de construction (pour les dessins ant�rieurs � la version V5R11) ";
geoOtherColor.frameButtonOtherColors.IconAndOptionsFrame.OptionsFrame.labelConstElts.LongHelp = "El�ments de construction\nPermet de d�finir la couleur des �l�ments de construction des dessins ant�rieurs � la version V5R11.";
geoOtherColor.frameButtonOtherColors.IconAndOptionsFrame.OptionsFrame.labelSmartPick.Title = "SmartPick ";
geoOtherColor.frameButtonOtherColors.IconAndOptionsFrame.OptionsFrame.labelSmartPick.LongHelp = "SmartPick\nD�finit la couleur SmartPick.";
