//=============================================================================
// COPYRIGHT DASSAULT SYSTEMES  2005
//=============================================================================
//
// CATStOLDTopoFilletDialog: Resource file for NLS purpose related to TopoFillet command
//
//=============================================================================
//
// Implementation Notes:
//
//=============================================================================
// March 05   Creation                                    Ritesh Kanetkar
//=============================================================================
DialogBoxTitle="Cong� de style";
TabCont.GeneralTab.Title="Entr�es du cong� de raccordement";
TabCont.ApproxTab.Title="Approximation";

TabCont.GeneralTab.MainFrame.ContinuityFrame.Title="Tol�rance";
TabCont.GeneralTab.MainFrame.RadiusFrame.Title="Rayon";
TabCont.GeneralTab.MainFrame.ArcTypeFrame.Title="Type d'arc";
TabCont.GeneralTab.MainFrame.ParameterFrame.Title="Param�tre";
TabCont.GeneralTab.MainFrame.ResultFrame.Title="R�sultat";
TabCont.GeneralTab.MainFrame.OptionFrame.Title="Option";
TabCont.GeneralTab.MainFrame.SelectionFrame.Title="S�lection";

TabCont.GeneralTab.MainFrame.ParameterFrame.ParameterCombo.PDefault.Title="D�faut";
TabCont.GeneralTab.MainFrame.ParameterFrame.ParameterCombo.PPatch1.Title="Correctif 1";
TabCont.GeneralTab.MainFrame.ParameterFrame.ParameterCombo.PPatch2.Title="Correctif 2";
TabCont.GeneralTab.MainFrame.ParameterFrame.ParameterCombo.PAverage.Title="Moyenne";
TabCont.GeneralTab.MainFrame.ParameterFrame.ParameterCombo.PBlend.Title="Surface de raccord";
TabCont.GeneralTab.MainFrame.ParameterFrame.ParameterCombo.PChordal.Title="Fl�che";

TabCont.GeneralTab.MainFrame.ResultFrame.TrimCombo.TrimFace.Title="Relimiter la face";
TabCont.GeneralTab.MainFrame.ResultFrame.TrimCombo.TrimApprox.Title="Approch�";

TabCont.GeneralTab.MainFrame.ResultFrame.SmallSurCombo.SSStitch.Title="Coudre";
TabCont.GeneralTab.MainFrame.ResultFrame.SmallSurCombo.SSRip.Title="R�partir";

TabCont.GeneralTab.MainFrame.ContinuityFrame.G0Radio.ShortHelp="G0";
TabCont.GeneralTab.MainFrame.ContinuityFrame.G0Radio.LongHelp="Continuit� G0 entre le cong� de raccordement et les �l�ments en entr�e";
TabCont.GeneralTab.MainFrame.ContinuityFrame.G1Radio.ShortHelp="G1";
TabCont.GeneralTab.MainFrame.ContinuityFrame.G1Radio.LongHelp="Continuit� G1 entre le cong� de raccordement et les �l�ments en entr�e";
TabCont.GeneralTab.MainFrame.ContinuityFrame.G2Radio.ShortHelp="G2";
TabCont.GeneralTab.MainFrame.ContinuityFrame.G2Radio.LongHelp="Continuit� G2 entre le cong� de raccordement et les �l�ments en entr�e";
TabCont.GeneralTab.MainFrame.ContinuityFrame.G3Radio.ShortHelp="G3";
TabCont.GeneralTab.MainFrame.ContinuityFrame.G3Radio.LongHelp="Continuit� G3 entre le cong� de raccordement et les �l�ments en entr�e";


TabCont.GeneralTab.MainFrame.RadiusFrame.Radius.ShortHelp="Rayon constant";
TabCont.GeneralTab.MainFrame.RadiusFrame.Radius.LongHelp="Rayon constant du cong� de raccordement";
TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadFlag.ShortHelp="Rayon minimal";
TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadFlag.LongHelp="Activer/d�sactiver le rayon minimal";
TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadius.ShortHelp="Valeur du rayon minimal";
TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadius.LongHelp="Valeur du rayon minimal";
TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadRelativeFlag.ShortHelp="Rayon relatif";
TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadRelativeFlag.LongHelp="Activer/d�sactiver le rayon relatif";
TabCont.GeneralTab.MainFrame.RadiusFrame.VariableRadChkBtn.ShortHelp="Rayon variable";
TabCont.GeneralTab.MainFrame.RadiusFrame.VariableRadChkBtn.LongHelp="Activer/d�sactiver le rayon variable";

TabCont.GeneralTab.MainFrame.ArcTypeFrame.BlendRadio.ShortHelp="Surface de raccord";
TabCont.GeneralTab.MainFrame.ArcTypeFrame.BlendRadio.LongHelp="Permet de cr�er des surfaces de raccord entre des �l�ments en entr�e.";
TabCont.GeneralTab.MainFrame.ArcTypeFrame.ApproxRadio.ShortHelp="Approch�";
TabCont.GeneralTab.MainFrame.ArcTypeFrame.ApproxRadio.LongHelp="Permet de cr�er une courbe de Bezier approximative circulaire entre des �l�ments en entr�e.";
TabCont.GeneralTab.MainFrame.ArcTypeFrame.ExactRadio.ShortHelp="Exact";
TabCont.GeneralTab.MainFrame.ArcTypeFrame.ExactRadio.LongHelp="Permet de cr�er une surface rationnelle avec de vraies sections circulaires.";

TabCont.GeneralTab.MainFrame.ParameterFrame.ParameterCombo.ShortHelp="Param�tre";
TabCont.GeneralTab.MainFrame.ParameterFrame.ParameterCombo.LongHelp=" Diff�rentes options de param�trisation de la surface de cong�";

TabCont.GeneralTab.MainFrame.ResultFrame.TrimSwitch.ShortHelp="R�sultat de la relimitation";
TabCont.GeneralTab.MainFrame.ResultFrame.TrimSwitch.LongHelp="Activation/d�sactivation de l'option de r�sultat de la relimitation";
TabCont.GeneralTab.MainFrame.ResultFrame.TrimCombo.ShortHelp="Relimitation de face / relimitation approx";
TabCont.GeneralTab.MainFrame.ResultFrame.TrimCombo.LongHelp="Type d'option de r�sultat de relimitation de l'option Relimitation de face - Le r�sultat est une face
                                                               Relimitation approx - Le r�sultat n'est pas une face";
TabCont.GeneralTab.MainFrame.ResultFrame.Extrapol.ShortHelp="Extrapolation";
TabCont.GeneralTab.MainFrame.ResultFrame.Extrapol.LongHelp="Permet d'extrapoler la surface de cong� obtenu.";
TabCont.GeneralTab.MainFrame.ResultFrame.SmallSur.ShortHelp="Petite surface";
TabCont.GeneralTab.MainFrame.ResultFrame.SmallSur.LongHelp="Si les deux ensembles d'entr�es contiennent plusieurs surfaces et que les contours de celles-ci ne sont
                                                              pas exactement � la m�me position, il en r�sulte une (tr�s) petite surface. Mode Activer/D�sactiver";
TabCont.GeneralTab.MainFrame.ResultFrame.SmallSurCombo.ShortHelp="Suturer//Partager";
TabCont.GeneralTab.MainFrame.ResultFrame.SmallSurCombo.LongHelp="Suturer : La petite surface peut �tre cousue � l'une des surfaces voisines
                                                                   Partager : La petite surface est divis�e (suivant sa diagonale) en deux surfaces";
TabCont.GeneralTab.MainFrame.ResultFrame.SmallSurfaceTolerancee.ShortHelp="Tol�rance de la petite surface";
TabCont.GeneralTab.MainFrame.ResultFrame.SmallSurfaceTolerancee.LongHelp="Tol�rance de cr�ation de la petite surface";

TabCont.GeneralTab.MainFrame.OptionFrame.TrimBase.ShortHelp="Simplification base";
TabCont.GeneralTab.MainFrame.OptionFrame.TrimBase.LongHelp="Permet de simplifier les �l�ments en entr�e du cong� de raccordement.";
TabCont.GeneralTab.MainFrame.OptionFrame.ChordalFillet.ShortHelp="Longueur de corde";
TabCont.GeneralTab.MainFrame.OptionFrame.ChordalFillet.LongHelp="La distance entre les contours du cong� de raccordement dans la direction longitudinale correspond � la distance de corde.";
TabCont.GeneralTab.MainFrame.OptionFrame.CtrlPts.ShortHelp="Points de contr�le";
TabCont.GeneralTab.MainFrame.OptionFrame.CtrlPts.LongHelp="Permet de modifier la forme de la surface du cong�";
TabCont.GeneralTab.MainFrame.OptionFrame.TrueMinimalRadius.ShortHelp="Minimum r�el";
TabCont.GeneralTab.MainFrame.OptionFrame.TrueMinimalRadius.LongHelp="Le rayon minimal est contr�l�.";
TabCont.GeneralTab.MainFrame.OptionFrame.IndependentApproximation.ShortHelp="Approximation ind�pendante";
TabCont.GeneralTab.MainFrame.OptionFrame.IndependentApproximation.LongHelp="Approximation interne ind�pendante";

TabCont.GeneralTab.MainFrame.SelectionFrame.FirstSetSur.ShortHelp="S�lectionner l'ensemble de surfaces 1";
TabCont.GeneralTab.MainFrame.SelectionFrame.FirstSetSur.LongHelp="S�lectionner le premier ensemble de surfaces pour la cr�ation de cong� de raccordement";
TabCont.GeneralTab.MainFrame.SelectionFrame.SecondSetSur.ShortHelp="S�lectionner l'ensemble de surfaces 2";
TabCont.GeneralTab.MainFrame.SelectionFrame.SecondSetSur.LongHelp="S�lectionner le deuxi�me ensemble de surfaces pour la cr�ation de cong� de raccordement";
TabCont.GeneralTab.MainFrame.SelectionFrame.FirstSetCur.ShortHelp="S�lectionner l'ensemble de courbes 1";
TabCont.GeneralTab.MainFrame.SelectionFrame.FirstSetCur.LongHelp="S�lectionner le premier ensemble de courbes pour la cr�ation de cong� de raccordement";
TabCont.GeneralTab.MainFrame.SelectionFrame.SecondSetCur.ShortHelp="S�lectionner l'ensemble de courbes 2";
TabCont.GeneralTab.MainFrame.SelectionFrame.SecondSetCur.LongHelp="S�lectionner le deuxi�me ensemble de courbes pour la cr�ation de cong� de raccordement";
TabCont.GeneralTab.MainFrame.SelectionFrame.SpineSet.ShortHelp="S�lectionner l'ensemble de courbes de contr�le";
TabCont.GeneralTab.MainFrame.SelectionFrame.SpineSet.LongHelp="S�lectionner la courbe de contr�le pour la cr�ation de cong� de raccordement";
TabCont.GeneralTab.MainFrame.SelectionFrame.PtOnSpineSet.ShortHelp="S�lectionner un point sur la courbe de contr�le";
TabCont.GeneralTab.MainFrame.SelectionFrame.PtOnSpineSet.LongHelp="S�lectionner un point sur la courbe de contr�le pour la cr�ation de cong� de raccordement";


// New UI

MainOuterFrameTabs.TabCont.GeneralTab.Title="Options";
// MainOuterFrameTabs.TabCont.ApproxTab.Title = "Advanced"; // WAP 08:08:26 R19SP4 HL2 - Nls entry commented.


// Continuity Frame
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContinuityFrame.Title="Continuit�";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContinuityFrame.G0Radio.ShortHelp="G0";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContinuityFrame.G0Radio.LongHelp="Continuit� G0 entre le cong� de raccordement et les �l�ments en entr�e";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContinuityFrame.G1Radio.ShortHelp="G1";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContinuityFrame.G1Radio.LongHelp="Continuit� G1 entre le cong� de raccordement et les �l�ments en entr�e";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContinuityFrame.G2Radio.ShortHelp="G2";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContinuityFrame.G2Radio.LongHelp="Continuit� G2 entre le cong� de raccordement et les �l�ments en entr�e";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContinuityFrame.G3Radio.ShortHelp="G3";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContinuityFrame.G3Radio.LongHelp="Continuit� G3 entre le cong� de raccordement et les �l�ments en entr�e";

// Radius Frame
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.Title="Param�tres de rayon";
//MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.RadiusLabel.Title = "Radius :"; // WAP 08:11:25 R19SP3 HL
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.RadiusLabel.ShortHelp="Rayon constant";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.RadiusLabel.LongHelp="Rayon constant du cong� de raccordement";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.Radius.ShortHelp="Rayon constant";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.Radius.LongHelp="Rayon constant du cong� de raccordement";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadFlag.Title="Rayon min :";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadFlag.ShortHelp="Rayon minimal";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadFlag.LongHelp="Activer/d�sactiver le rayon minimal";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadius.ShortHelp="Valeur du rayon minimal";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadius.LongHelp="Valeur du rayon minimal";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadRelativeFlag.ShortHelp="Rayon relatif";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadRelativeFlag.LongHelp="Activer/d�sactiver le rayon relatif";

// Arc Type 
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.ArcTypeFrame.Title="Type d'arc";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.ArcTypeFrame.BlendRadio.ShortHelp="Surface de raccord";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.ArcTypeFrame.BlendRadio.LongHelp="Permet de cr�er des surfaces de raccord entre des �l�ments en entr�e.";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.ArcTypeFrame.ApproxRadio.ShortHelp="Approch�";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.ArcTypeFrame.ApproxRadio.LongHelp="Permet de cr�er une courbe de Bezier approximative circulaire entre des �l�ments en entr�e.";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.ArcTypeFrame.ExactRadio.ShortHelp="Exact";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.ArcTypeFrame.ExactRadio.LongHelp="Permet de cr�er une surface rationnelle avec de vraies sections circulaires.";


MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.Title="R�sultat";

// Simplification Frame
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.Title="Simplification";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.SmallSurStich.Title="Coudre";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.SmallSurStich.ShortHelp="Petite surface - Suturer";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.SmallSurStich.LongHelp="Suturer : La petite surface peut �tre cousue � l'une des surfaces voisines.";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.SmallSurRip.Title="R�partir";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.SmallSurRip.ShortHelp="Petite surface - Partager";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.SmallSurRip.LongHelp="Partager : La petite surface est divis�e (suivant sa diagonale) en deux surfaces.";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.SmallSurfaceTolerancee.ShortHelp="Tol�rance de la petite surface";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.SmallSurfaceTolerancee.LongHelp="Tol�rance de cr�ation de la petite surface";

// Relimitation Frame
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.RelimitationFrame.Title="Relimitation";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.RelimitationFrame.TrimFace.Title="Relimiter la face";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.RelimitationFrame.TrimFace.ShortHelp="Relimiter la face";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.RelimitationFrame.TrimFace.LongHelp="Le r�sultat est une face.";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.RelimitationFrame.TrimApprox.Title="Relimiter l'approximation";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.RelimitationFrame.TrimApprox.ShortHelp="Relimiter l'approximation";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.RelimitationFrame.TrimApprox.LongHelp="Le r�sultat n'est pas une face.";
// For R18SP3 UI
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.Title="G�om�trie";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.Extrapol1.ShortHelp="Extrapoler c�t�1";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.Extrapol1.LongHelp="Le r�sultat est extrapol� sur le c�t�1";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.Extrapol2.ShortHelp="Extrapoler c�t�2";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.Extrapol2.LongHelp="Le r�sultat est extrapol� sur le c�t�2";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.Relimit1.ShortHelp="Relimiter c�t�1";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.Relimit1.LongHelp="Le r�sultat est relimit� sur le c�t�1";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.Relimit2.ShortHelp="Relimiter c�t�2";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.Relimit2.LongHelp="Le r�sultat est relimit� sur le c�t�2";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.TrimFace.Title="Relimiter la face";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.TrimFace.ShortHelp="Relimiter la face";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.TrimFace.LongHelp="Le r�sultat est une face.";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.TrimApprox.Title="Relimiter l'approximation";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.TrimApprox.ShortHelp="Relimiter l'approximation";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.TrimApprox.LongHelp="Le r�sultat n'est pas une face.";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.Merge.ShortHelp="Concat�nation";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.Merge.LongHelp="Permet de concat�ner un ruban de cong� multi-cellule pour un r�sultat mono-cellule (possible uniquement en cas de continuit� G2 entre les cellules)";

// Geometry Frame
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.GeometryFrame.Title="G�om�trie";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.GeometryFrame.Extrapol.Title="Extrapolation";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.GeometryFrame.Extrapol.ShortHelp="Extrapolation";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.GeometryFrame.Extrapol.LongHelp="Permet d'extrapoler la surface de cong� obtenu.";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.GeometryFrame.Merge.ShortHelp="Concat�nation";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.GeometryFrame.Merge.LongHelp="Permet de concat�ner un ruban de cong� multi-cellule pour un r�sultat mono-cellule (possible uniquement en cas de continuit� G2 entre les cellules)";
//--------------------------------------

// Options Frame
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.Title="Type de cong�";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.VariableRadChkBtn.ShortHelp="Rayon variable";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.VariableRadChkBtn.LongHelp="Activer/d�sactiver le rayon variable";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.TrimBase.ShortHelp="Simplification base";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.TrimBase.LongHelp="Permet de simplifier les �l�ments en entr�e du cong� de raccordement.";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.ChordalFillet.ShortHelp="Longueur de corde";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.ChordalFillet.LongHelp="La distance entre les contours du cong� de raccordement dans la direction longitudinale correspond � la distance de corde.";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.CtrlPts.ShortHelp="Points de contr�le";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.CtrlPts.LongHelp="Permet de modifier la forme de la surface du cong�";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.TrueMinimalRadius.ShortHelp="Minimum r�el";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.TrueMinimalRadius.LongHelp="Le rayon minimal est contr�l�.";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.IndependentApproximation.ShortHelp="Approximation ind�pendante";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.IndependentApproximation.LongHelp="Approximation interne ind�pendante";


// Surface-1 Curv-1
//MainOuterFrame.Surface1Curve1Frame.Title = "Support 1";
//MainOuterFrame.Surface1Curve1Frame.ShortHelp = "Select surface and curve set 1";
//MainOuterFrame.Surface1Curve1Frame.LongHelp = "Select first set of surfaces and curves for fillet operation";
//MainOuterFrame.Surface1Curve1Frame.Surface1Selector.ShortHelp = "Select surface set 1";
//MainOuterFrame.Surface1Curve1Frame.Surface1Selector.LongHelp = "Select first set of surfaces for fillet operation";
MainOuterFrame.Surface1Curve1Frame.Curve1Selector.ShortHelp="S�lectionner l'ensemble de courbes 1"; // WAP 09:01:23
MainOuterFrame.Surface1Curve1Frame.Curve1Selector.LongHelp="S�lectionner le premier ensemble de courbes pour la cr�ation de cong� de raccordement"; // WAP 09:01:23 
//MainOuterFrame.Surface1Curve1Frame.LabelSurface1.Title = "Surface :";
//MainOuterFrame.Surface1Curve1Frame.LabelSurface1.ShortHelp = "Select surface set 1";
//MainOuterFrame.Surface1Curve1Frame.LabelSurface1.LongHelp = "Select first set of surfaces for fillet operation";
MainOuterFrame.Surface1Curve1Frame.LabelCurve1.Title="Courbe :";
MainOuterFrame.Surface1Curve1Frame.LabelCurve1.ShortHelp="S�lectionner l'ensemble de courbes 1";
MainOuterFrame.Surface1Curve1Frame.LabelCurve1.LongHelp="S�lectionner le premier ensemble de courbes pour la cr�ation de cong� de raccordement";
MainOuterFrame.Surface1Curve1Frame.TrimBase1.ShortHelp="D�couper le support 1";
MainOuterFrame.Surface1Curve1Frame.TrimBase1.LongHelp="D�coupe les �l�ments d'entr�e (surfaces) dans l'ensemble de support 1 du cong�";
//MainOuterFrame.Surface1Curve1Frame.Curve1SelState.ShortHelp = "Enable curve 1 set";// WAP 09:01:23 
//MainOuterFrame.Surface1Curve1Frame.Curve1SelState.LongHelp = "Enables selection in the curve 1 set.";// WAP 09:01:23 

// Surface-2 Curv-2
MainOuterFrame.Surface2Curve2Frame.Title="Support 2";
MainOuterFrame.Surface2Curve2Frame.ShortHelp="S�lectionner l'ensemble de surfaces et de courbes 2";
MainOuterFrame.Surface2Curve2Frame.LongHelp="S�lectionner le deuxi�me ensemble de surfaces et de courbes pour la cr�ation de cong� de raccordement";
MainOuterFrame.Surface2Curve2Frame.Surface2Selector.ShortHelp="S�lectionner l'ensemble de surfaces 2";
MainOuterFrame.Surface2Curve2Frame.Surface2Selector.LongHelp="S�lectionner le deuxi�me ensemble de surfaces pour la cr�ation de cong� de raccordement";
//MainOuterFrame.Surface2Curve2Frame.Curve2Selector.ShortHelp = "Select curve set 2"; // WAP 09:01:23
//MainOuterFrame.Surface2Curve2Frame.Curve2Selector.LongHelp = "Select second set of curves for fillet operation"; // WAP 09:01:23
MainOuterFrame.Surface2Curve2Frame.LabelSurface2.Title="Surface :";
MainOuterFrame.Surface2Curve2Frame.LabelSurface2.ShortHelp="S�lectionner l'ensemble de surfaces 2";
MainOuterFrame.Surface2Curve2Frame.LabelSurface2.LongHelp="S�lectionner le deuxi�me ensemble de surfaces pour la cr�ation de cong� de raccordement";
MainOuterFrame.Surface2Curve2Frame.LabelCurve2.Title="Courbe :";
MainOuterFrame.Surface2Curve2Frame.LabelCurve2.ShortHelp="S�lectionner l'ensemble de courbes 2";
MainOuterFrame.Surface2Curve2Frame.LabelCurve2.LongHelp="S�lectionner le deuxi�me ensemble de courbes pour la cr�ation de cong� de raccordement";
MainOuterFrame.Surface2Curve2Frame.TrimBase2.ShortHelp="D�couper le support 2";
MainOuterFrame.Surface2Curve2Frame.TrimBase2.LongHelp="D�coupe les �l�ments d'entr�e (surfaces) dans l'ensemble de support 2 du cong�";
//MainOuterFrame.Surface2Curve2Frame.Curve2SelState.ShortHelp = "Enable curve 2 set";// WAP 09:01:23 
//MainOuterFrame.Surface2Curve2Frame.Curve2SelState.LongHelp = "Enables selection in the curve 2 set.";// WAP 09:01:23 

// Point-Spine
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.SpinePointFrame.SpineSelector.ShortHelp="S�lectionner l'ensemble de courbes de contr�le";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.SpinePointFrame.SpineSelector.LongHelp="S�lectionner la courbe de contr�le pour la cr�ation de cong� de raccordement";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.SpinePointFrame.PointSelector.ShortHelp="S�lectionner un point sur la courbe de contr�le";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.SpinePointFrame.PointSelector.LongHelp="S�lectionner un point sur la courbe de contr�le pour la cr�ation de cong� de raccordement";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.SpinePointFrame.LabelSpine.Title="Courbe de contr�le :";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.SpinePointFrame.LabelSpine.ShortHelp="S�lectionner l'ensemble de courbes de contr�le";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.SpinePointFrame.LabelSpine.LongHelp="S�lectionner la courbe de contr�le pour la cr�ation de cong� de raccordement";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.SpinePointFrame.LabelPoint.Title="Point :";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.SpinePointFrame.LabelPoint.ShortHelp="S�lectionner un point sur la courbe de contr�le";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.SpinePointFrame.LabelPoint.LongHelp="S�lectionner un point sur la courbe de contr�le pour la cr�ation de cong� de raccordement";

// Parameters
MainOuterFrameTabs.TabCont.ApproxTab.ParameterFrame.Title="Param�tre";
MainOuterFrameTabs.TabCont.ApproxTab.ParameterFrame.ParameterCombo.ShortHelp="Param�tre";
MainOuterFrameTabs.TabCont.ApproxTab.ParameterFrame.ParameterCombo.LongHelp=" Diff�rentes options de param�trisation de la surface de cong�";
MainOuterFrameTabs.TabCont.ApproxTab.ParameterFrame.ParameterCombo.PDefault.Title="D�faut";
MainOuterFrameTabs.TabCont.ApproxTab.ParameterFrame.ParameterCombo.PPatch1.Title="Correctif 1";
MainOuterFrameTabs.TabCont.ApproxTab.ParameterFrame.ParameterCombo.PPatch2.Title="Correctif 2";
MainOuterFrameTabs.TabCont.ApproxTab.ParameterFrame.ParameterCombo.PAverage.Title="Moyenne";
MainOuterFrameTabs.TabCont.ApproxTab.ParameterFrame.ParameterCombo.PBlend.Title="Surface de raccord";
MainOuterFrameTabs.TabCont.ApproxTab.ParameterFrame.ParameterCombo.PChordal.Title="Fl�che";

// Independent Approximation
MainOuterFrameTabs.TabCont.ApproxTab.IndependentApproximation.Title="Approximation ind�pendante";
MainOuterFrameTabs.TabCont.ApproxTab.IndependentApproximation.ShortHelp="Approximation ind�pendante";
MainOuterFrameTabs.TabCont.ApproxTab.IndependentApproximation.LongHelp="Approximation interne ind�pendante";

Surface1BagTitle.Message="S�lectionner la surface 1";
Curve1BagTitle.Message="S�lectionner la courbe 1";
Surface2BagTitle.Message="S�lectionner la surface 2";
Curve2BagTitle.Message="S�lectionner la courbe 2";
SpineBagTitle.Message="S�lectionner la courbe de contr�le";
PointOnSpineBagTitle.Message="S�lectionner le point sur la courbe de contr�le";

MaxDevOptionsFrame.MaxDevDisplayOptionsFrame.Title="Affichage de la d�viation";
MaxDevOptionsFrame.MaxDevDisplayOptionsFrame.DevBetFilletCellsChkB.ShortHelp="Connexion entre les cellules du cong� de raccordement";
MaxDevOptionsFrame.MaxDevDisplayOptionsFrame.DevBetFilletCellsChkB.LongHelp="Affiche la connexion entre les cellules du ruban du cong�.";
MaxDevOptionsFrame.MaxDevDisplayOptionsFrame.DevBetFilletAndSupportChkB.ShortHelp="Connexion entre le ruban du cong� et le support";
MaxDevOptionsFrame.MaxDevDisplayOptionsFrame.DevBetFilletAndSupportChkB.LongHelp="Affiche la connexion entre le ruban du cong� et le support.";
MaxDevOptionsFrame.MaxDevValueFrame.Title="D�viation max";
MaxDevOptionsFrame.MaxDevValueFrame.LongHelp="Permet d'afficher la valeur maximale pour chaque continuit�.";
MaxDevOptionsFrame.MaxDevValueFrame.G0TitleLabel.Title="G0 :";
MaxDevOptionsFrame.MaxDevValueFrame.G0Label="----   ";
MaxDevOptionsFrame.MaxDevValueFrame.G1TitleLabel.Title="G1 :";
MaxDevOptionsFrame.MaxDevValueFrame.G2TitleLabel.Title="G2 :";
MaxDevOptionsFrame.MaxDevValueFrame.G3TitleLabel.Title="G3 :";

// Start WAP 08:08:26 R19SP4 HL1 AND HL2

NewApproxTab.Message="Approximation";
ApproxTab.Message="Avanc�";
OutputResultFrame.Title="R�sultat en sortie";
OutputResultFrame.NbOfCellsTitleLabel.Title="Nb de cellules :";
OutputResultFrame.MaxOrderTitleLabel.Title="Ordre max :";
OutputResultFrame.MaxSegTitleLabel.Title="Nombre de segments max. par cellule :";
OutputResultFrame.OrdUTitleLabel.Title="U:";
OutputResultFrame.OrdVTitleLabel.Title="V :";
OutputResultFrame.SegUTitleLabel.Title="U:";
OutputResultFrame.SegVTitleLabel.Title="V :";
OutputResultFrame.NbOfCellsTitleLabel.ShortHelp="Affiche le nombre de cellules";
OutputResultFrame.NbOfCellsTitleLabel.LongHelp="Affiche le nombre de cellules dans le cong� cr��";
OutputResultFrame.MaxOrderTitleLabel.ShortHelp="Affiche l'ordre max. dans les directions U et V";
OutputResultFrame.MaxOrderTitleLabel.LongHelp="Affiche l'ordre maximal du r�sultat cr�� dans les directions U et V";
OutputResultFrame.MaxSegTitleLabel.ShortHelp="Affiche le nombre max. de segments par cellule dans les directions U et V";
OutputResultFrame.MaxSegTitleLabel.LongHelp="Affiche le nombre maximal de segments par cellule du r�sultat cr�� dans les directions U et V";
// End WAP 08:08:26	

// Start WAP 08:11:25 R19SP3 HL
LengthLabel.Message="Longueur :";
RadiusLabel.Message="Rayon";
// End WAP 08:11:25

// Start WAP 08:12:02 R19SP4 HL6
//Spine - Point Frame	
MainOuterFrame.SpinePointFrame.LabelSpine.Title="Courbe de contr�le :";
MainOuterFrame.SpinePointFrame.LabelPoint.Title="Point :";
MainOuterFrame.SpinePointFrame.SpineSelector.ShortHelp="S�lectionner l'ensemble de courbes de contr�le";
MainOuterFrame.SpinePointFrame.SpineSelector.LongHelp="S�lectionner la courbe de contr�le pour la cr�ation de cong� de raccordement";
MainOuterFrame.SpinePointFrame.PointSelector.ShortHelp="S�lectionner un point sur la courbe de contr�le";
MainOuterFrame.SpinePointFrame.PointSelector.LongHelp="S�lectionner un point sur la courbe de contr�le pour la cr�ation de cong� de raccordement";

// Geometry Frame
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.LabelExtrapolRelimit1.Title="Extrapoler/Relimiter c�t�1";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.LabelExtrapolRelimit1.ShortHelp="Extrapole/relimite sur le c�t�1";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.LabelExtrapolRelimit1.LongHelp="Le r�sultat est extrapol�/relimit� sur le c�t�1";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.LabelExtrapolRelimit2.Title="Extrapoler/Relimiter c�t�2";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.LabelExtrapolRelimit2.ShortHelp="Extrapole/relimite sur le c�t�2";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.LabelExtrapolRelimit2.LongHelp="Le r�sultat est extrapol�/relimit� sur le c�t�2";

// Result Frame
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.LogicalMerge.Title="Jointure logique";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.LogicalMerge.ShortHelp="Jointure logique";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.LogicalMerge.LongHelp="Assemble le r�sultat (ruban du cong�, surfaces entr�es relimit�es) selon une tol�rance fixe de 0,1 mm";

// Simplification Frame
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.Merge.Title="Concat�nation";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.Merge.ShortHelp="Concat�nation";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.Merge.LongHelp="Permet de concat�ner un ruban de cong� multi-cellule pour un r�sultat mono-cellule (possible uniquement en cas de continuit� G2 entre les cellules)";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.LogicalMerge.Title="Jointure logique";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.LogicalMerge.ShortHelp="Jointure logique";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.LogicalMerge.LongHelp="Assemble le r�sultat (ruban du cong�, surfaces entr�es relimit�es) selon une tol�rance fixe de 0,1 mm";
// End WAP 08:12:02

// Start WAP 09:01:23 R19SP4 HL5
Curve1SelectorShortHelp.Message="S�lectionner l'ensemble de courbes 1";
Curve1SelectorLongHelp.Message="S�lectionner le premier ensemble de courbes pour la cr�ation de cong� de raccordement";
Curve1SelStateShortHelp.Message="Activer l'ensemble de courbes 1";
Curve1SelStateLongHelp.Message="Active la s�lection dans l'ensemble de courbes 1.";

Curve2SelectorShortHelp.Message="S�lectionner l'ensemble de courbes 2";
Curve2SelectorLongHelp.Message="S�lectionner le deuxi�me ensemble de courbes pour la cr�ation de cong� de raccordement";
Curve2SelStateShortHelp.Message="Activer l'ensemble de courbes 2";
Curve2SelStateLongHelp.Message="Active la s�lection dans l'ensemble de courbes 2.";


EdgesToExtrapol1SelectorShortHelp.Message="S�lectionnez l'ar�te 1 � extrapoler";
EdgesToExtrapol1SelectorLongHelp.Message="S�lectionnez le premier ensemble d'ar�tes � extrapoler pour la cr�ation de cong� variable";
EdgesToExtrapol1SelStateShortHelp.Message="Activer l'ar�te 1 � extrapoler";
EdgesToExtrapol1SelStateLongHelp.Message="Active la s�lection du premier ensemble d'ar�tes � extrapoler pour la cr�ation de cong� variable";
EdgesToExtrapol2SelectorShortHelp.Message="S�lectionnez l'ar�te 2 � extrapoler";
EdgesToExtrapol2SelectorLongHelp.Message="S�lectionnez le deuxi�me ensemble d'ar�tes � extrapoler pour la cr�ation de cong� variable";
EdgesToExtrapol2SelStateShortHelp.Message="Activer l'ar�te 2 � extrapoler";
EdgesToExtrapol2SelectorLongHelp.Message="Active la s�lection du deuxi�me ensemble d'ar�tes � extrapoler pour la cr�ation de cong� variable";

// End WAP 09:01:23

// Start WAP 08:12:02 R19SP5
// Input Parameter Frame
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.InputParameterFrame.Title="Param�tres d'entr�e";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.InputParameterFrame.LabelMergeDist.Title="Distance de confusion : ";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.InputParameterFrame.SpinnerMergeDist.ShortHelp="Distance de confusion";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.InputParameterFrame.SpinnerMergeDist.LongHelp="D�finit la distance de confusion pour le cong� multi-cellule";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.InputParameterFrame.TolerantLaydownCheck.ShortHelp="Pose tol�rante";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.InputParameterFrame.TolerantLaydownCheck.LongHelp="Utilise la pose tol�rante";
TolerantLaydown.Title="Pose tol�rante";
// End WAP 08:12:02					   

MainOuterFrame.Surface2Curve2Frame.EdgeFilletMode.Title="Type de cong�";
MainOuterFrame.Surface2Curve2Frame.EdgeFilletMode.ShortHelp="Type de cong�";
MainOuterFrame.Surface2Curve2Frame.EdgeFilletMode.LongHelp="S�lectionner le type d'op�ration de cr�ation de cong� de raccordement. Cong� de raccordement de support/Cong� de raccordement d'ar�te";

EdgeFilletModeLabel.Message="Objet(s) pour lequel ou lesquels cr�er un cong� de raccordement";
SupportFilletMode.Message="Support 1";

Surface1Curve1FrameShortHelp.Message="S�lectionner l'ensemble de surfaces et de courbes 1";
Surface1Curve1FrameLongHelp.Message="S�lectionner le premier ensemble de surfaces et de courbes pour la cr�ation de cong� de raccordement";
ObjectsToFilletFrameShortHelp.Message="S�lectionner le ou les objets pour lesquels cr�er un cong� de raccordement";
ObjectsToFilletFrameLongHelp.Message="S�lectionner des faces/ar�tes vives pour l'op�ration de cr�ation de cong� de raccordement";

Surface1Title.Message="Surface :";
Surface1ShortHelp.Message="S�lectionner l'ensemble de surfaces 1";
Surface1LongHelp.Message="S�lectionner le premier ensemble de surfaces pour la cr�ation de cong� de raccordement";

ObjectToFilletTitle.Message="Objet(s) :";
ObjectToFilletShortHelp.Message="S�lectionner le ou les objets pour lesquels cr�er un cong� de raccordement";
ObjectToFilletLongHelp.Message="S�lectionner des faces/ar�tes vives pour l'op�ration de cr�ation de cong� de raccordement";

Surface1SelectorShortHelp.Message="S�lectionner l'ensemble de surfaces 1";
Surface1SelectorLongHelp.Message="S�lectionner le premier ensemble de surfaces pour la cr�ation de cong� de raccordement";
ObjectsToFilletSelectorShortHelp.Message="S�lectionner le ou les objets pour lesquels cr�er un cong� de raccordement";
ObjectsToFilletSelectorLongHelp.Message="S�lectionner des faces/ar�tes vives pour l'op�ration de cr�ation de cong� de raccordement";
