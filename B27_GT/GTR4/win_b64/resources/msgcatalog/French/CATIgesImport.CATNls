//NLS-ENGLISH VERSION
//=======================================================================
// MODULE :  IgesDataExchange
//=======================================================================

// JRX 2002-08-09 : Ajout de messages d'erreur pour CATIgesConicArc
// KVL 2002-06-04 : Creation de CATIgesImport.CATNls et refonte des anciens CATNls Iges.
// DFB 2001-11-15 : Ajout de messages d'erreur pour la detection de trous 3D 
//				   (CATIgesCurveOnParametricSurface et CATIgesBoundary)
// DFB 2001-11-22 : Ajout de Bad_GetEndPPoints, Hole2DLoop et OrientPCurves
// DFB 2001-12-11 : Ajout de Bad_ControlPoints2D
// JRX 2001-10-24 : Ajout de messages d'erreur pour le Parsing (fsbDataTreatment)
// MEF 2003-03-14 : Ajout de message d'erreur dans le traitement de la global data
// mef 2004-02-05 : Ajout d'un warning si probleme de licence sur l'insert
// CNY 2004-05-19 : Ajout d'un message info "mode CGR" pour CATIgesExportTypeManager
// OPC 2006-06-23 : Reecriture des messages niveau Customer pour meilleure compr�hension
// wbu 28/03/2007 : ajout des messages d'erreur sur le parsing utilisable par le protocole CATIReporter et GetLastError


// Numeros utilises : (import Iges : de [1001] a [1999]
// [1000] a [1099] : fsb*
// [1100] a [1149] : CATIgesCurveOnParametricSurface et CATIgesBoundary
// [1150] a [1199] : CATIgesTrimmedParametricSurface
// [1200] a [1299] : CATIgesElement (Actuellement utilises : 1200 a 1202) 
// [1300] a [1399] : fileIges*
// [1500] a [1549] : CATIgesBSplineSurface
// [1550] a [1599] : CATIgesBSplineCurve
// [1600] a [1649] : CATIgesCompositeCurve
// [1650] a [1699] : CATIgesParametricSplineCurve
// [1701] a [1750] : CATIgesConicArc
// [1751] a [1770] : CATEIgesShapeFileSelection
// [1800] a [1820] : CATIgesExportTypeManager
// [1850] a [1860] : CATIgesExchangeDocu CATIgesInterface


//--------------------------------------
//  Messages from CATIgesExchangeDocu (CATIgesInterface.cpp)
//--------------------------------------


CATIgesExchangeDocu.Error.IGES="   <E> [1850] Probl�me li� au fichier IGES.
            Cl�ture anormale de la phase d'analyse.";

CATIgesExchangeDocu.Error.IGSS="   <E> [1851] Probl�me li� � la section d'en-t�te.
            Cl�ture anormale de la phase d'analyse.";

CATIgesExchangeDocu.Error.IGGS="   <E> [1852] Probl�me li� � la section globale.
            Cl�ture anormale de la phase d'analyse.";

CATIgesExchangeDocu.Error.IGSPD="   <E> [1853] Probl�me li� � la section DE ou PD.
            Cl�ture anormale de la phase d'analyse.";


//--------------------------------------
//  Messages from CATIgesInterface
//--------------------------------------

CATDXFileServices.Error.InputFileNotReadable="!! <E> [1858] Impossible de lire le fichier IGES (erreur PathERR_1001) :
               Le fichier IGES n'existe pas ou il n'est pas lisible.
               V�rifiez si le fichier existe et si vous disposez des droits n�cessaires pour le lire.";

CATDXFileServices.Error.InvalidName="!! <E> [1859] Le nom du fichier IGES � ouvrir n'est pas valide (erreur PathERR_1021) :
               Le nom de fichier contient des caract�res non ASCII ou est vide.
               Recherchez les caract�res non admis dans le nom de fichier. Renommez le fichier IGES si n�cessaire et essayez de l'ouvrir � nouveau.";

CATIgesInterface.Error.TitreMsg="\n o=====================================================o";



//-----------------------------------------------
//  Messages from CATIgesCurveOnParametricSurface
//  Messages from CATIgesBoundary
//-----------------------------------------------

// Warning : Trou dans la loop 143 ou 144 3D du fichier iges - dfb
CATIgesCurveOnParametricSurface.Warning.HoledLoop3D="   <W> [1100] [T=/p2] [/p1] Donn�es incorrectes : il existe un trou dans le contour 3D :/p4 /p3";

// Warning : Trou dans la loop 143 ou 144 2D du fichier iges - dfb
CATIgesCurveOnParametricSurface.Warning.HoledLoop2D="   <W> [1101] [T=/p2] [/p1] Donn�es incorrectes : il existe un trou dans le contour param�trique /p4 /p3";

// Warning : Si la methode GetEndPPoints n'est pas implementee, on ne peut reorienter la loop 2D - dfb
CATIgesCurveOnParametricSurface.Warning.Bad_GetEndPPoints="   <W> [1102] [T=/p2] [/p1] Impossible de trouver les points d'extr�mit� ; il n'est pas possible d'inverser la repr�sentation 2D du contour /p4";

// Warning : Mauvaise orientation des PCurves 2D  - dfb
CATIgesCurveOnParametricSurface.Warning.OrientPCurves="   <W> [1103] [T=/p2] [/p1] Le contour contient une ou plusieurs courbes 2D invers�es /p3";


//-----------------------------------------------
//  Messages from CATIgesTrimmedParametricSurface
// [1150] a [1199]
//-----------------------------------------------

// Erreur : Pas de Surface pour cette face
CATIgesTrimmedParametricSurface.Error.NoSurface="   <E> [1150] Il n'y a PAS de surface pour cette face. Merci de v�rifier l'int�grit� du fichier IGES /p3";


//--------------------------------------
//  Messages from CATIgesElement
//--------------------------------------


CATIgesElement.Msg.Name_406="   <I> [1200] [T=/p2] [/p1] Le nom de l'entit� a �t� sp�cifi� au moyen d'une entit� 406 forme 15 : /p3";

CATIgesElement.Msg.Name_DE="   <I> [1201] [T=/p2] [/p1] Le nom de l'entit� a �t� sp�cifi� � l'aide des champs 18 et 19 de la DE : /p3";

CATIgesElement.Msg.Name_Undef="   <I> [1202] [T=/p2] [/p1] Aucun nom n'a �t� sp�cifi� pour cette entit�.";



//--------------------------
//  Messages from fileIges* 
//--------------------------


fileIGEStool.Error.IGES="   <E> [1317] Probl�me li� � la lecture de fichier IGES /p1.";


fileIGEStool.Error.IGBAD="   <E> [1318] Option erron�e pour l'ouverture du fichier /p1.";


fileIGEStool.Error.IGST="   <E> [1319] Erreur li�e au fichier IGES : enregistrement final manquant ou lignes suppl�mentaires en fin de fichier /p1";


fileIGEStool.Error.IGGS="   <E> [1320] Probl�me li� � la section globale du fichier IGES /p1.";


fileIGEStool.Error.IGSPD="   <E> [1321] Probl�me li� � la section PD du fichier IGES /p1.";


fileIGEStool.Error.EOF="   <E> [1301] Fin de fichier pr�matur�e lors de la lecture de l'enreg. /p2 de la PD du fichier /p1";


fileIGEStool.Error.IncBloc="   <E> [1302] Bloc incomplet lors de la lecture de l'enreg. /p2 de la PD du fichier /p1";


fileIGES.Error.IGFILE="Fichier IGES /p1 non trouv� ou probl�me d'acc�s.";


fileIGES.Error.IGESF="   <E> [1323] Le fichier /p1 n'est pas un fichier .igs.";


fileIGES.Error.IGESO="   <E> [1322] Impossible d'ouvrir le fichier IGES /p1.";


fileIGES.Error.IGESC="Impossible de fermer le fichier IGES /p1.";


fileIGES.Msg.IGESS="   <W> [1324] Attention : il doit y avoir au moins un enregistrement de d�marrage.";


fileIGES.Msg.IGProdIdSend="   <W> [1325] Erreur li�e au fichier IGES : probl�me li� � l'identification du produit de l'�metteur.";


fileIGES.Msg.IGFileN="   <W> [1326] Probl�me li� au nom de fichier.";


fileIGES.Msg.IGSysId="   <W> [1327] Probl�me li� � l'ID syst�me natif.";


fileIGES.Msg.IGPrepVers="   <W> [1328] Probl�me li� � la version du pr�processeur.";


fileIGES.Msg.IGIntRep="   <W> [1329] Probl�me li� � la repr�sentation de l'entier.";


fileIGES.Msg.IGSingPrecMag="   <W> [1330] Probl�me li� � la grandeur de simple pr�cision.";


fileIGES.Msg.IGSingPrecSig="   <W> [1331] Probl�me li� � l'importance de simple pr�cision.";


fileIGES.Msg.IGDbPrecMag="   <W> [1332] Probl�me li� � la grandeur de double pr�cision.";


fileIGES.Msg.IGDbPrecSig="   <W> [1333] Probl�me li� � l'importance de double pr�cision.";


fileIGES.Msg.IGProdIdRec="   <W> [1312] Probl�me li� � l'identification du produit du r�cepteur.";


fileIGES.Msg.IGSca="   <W> [1334] Probl�me li� � l'�chelle spatiale du mod�le.";


fileIGES.Msg.IGUnitFlg="   <W> [1335] Probl�me li� au marqueur d'unit�.";


fileIGES.Msg.IGUnitNam="   <W> [1336] Probl�me li� aux unit�s.";


fileIGES.Msg.IGMaxLinGrad="   <W> [1337] Probl�me li� aux graduations d'�paisseur de ligne maximales.";


fileIGES.Msg.IGMaxLinUnit="   <W> [1338] Probl�me li� � l'�paisseur de ligne maximale en unit�s.";


fileIGES.Msg.IGDateGen="   <W> [1339] Probl�me li� � la date/heure de g�n�ration du fichier d'�change.";


fileIGES.Msg.IGMinUserRes="   <W> [1340] Probl�me li� � la r�solution utilisateur minimale.";


fileIGES.Msg.IGAppMaxCoord="   <W> [1341] Probl�me li� � l'approximation de la valeur de coordonn�es maximale.";


fileIGES.Msg.IGNamAut="   <W> [1342] Fichier IGES : probl�me li� au nom de l'auteur.";


fileIGES.Msg.IGAutOrg="   <W> [1343] Probl�me li� � l'organisation de l'auteur.";


fileIGES.Msg.IGVersNum="   <W> [1344] Probl�me li� au num�ro de version.";


fileIGES.Msg.IGDraftStd="   <W> [1345] Probl�me li� au code de norme de dessin.";


fileIGES.Msg.IGDateMod="   <W> [1346] Probl�me li� � la date/heure de cr�ation/modification du mod�le.";


fileIGES.Msg.IGMilSpec="   <W> [1347] Probl�me li� � la sp�cification MIL.";


fileIGES.Msg.defaultParameter="   <W> [1348] Fichier IGES : les param�tres ont des valeurs par d�faut";


fileIGES.Msg.InvalidParameter="   <W> [1349] Attention ! Les valeurs des param�tres suivants sont incorrectes.";


fileIGES.Error.IGES="   <W> [1313] Probl�me li� � la lecture du fichier IGES /p1.";


fileIGES.Error.IGMissPD="   <W> [1314] Section PD inexistante (ou structure de fichier erron�e) dans le fichier IGES /p1";

IGMissPDERR_1001.Request="Le fichier IGES n'est pas compatible avec la norme IGES ";
IGMissPDERR_1001.Diagnostic="Section PD manquante ou illisible dans le fichier IGES ";
IGMissPDERR_1001.Advice="V�rifiez que le fichier comprend des lignes de 80 colonnes ou que dans les colonnes 66 � 72, toutes les lignes de param�tres contiennent le num�ro de s�quence de la premi�re ligne dans le DE du num�ro du type d'entit� ...";


fileIGESelement.Msg.IGPar="   <W> [1315] Param�tre invalide ligne /p1 dans la section PD.";

//erb
fileIGESelement.Msg.InvalidePD="   <W> [1316] Section PD non valide ou inexistante pour l'entit� DE : /p2 de type /p1.";


fileIGESelement.Msg.InvNurbsConv="Conversion invalide de /p1 en surface nurbs.";


fileIGESelement.Msg.UnavailableMethod="L'appel � la m�thode /p1 de l'objet /p2 n'est pas valable.";


fileIGESelement.Msg.ObjCreationKO="Echec pendant la cr�ation d'objet.  L'objet /p1 n'a pu �tre cr��.";


fileIGESelement.Msg.PbInMethod="   <W> [1303] Probl�me dans la m�thode /p1 de l'objet /p2.";


fileIGES.Error.IGWrite="Probl�me pour �crire le fichier IGES /p1.";


fileIGESelement.Msg.Knot="/p1 : Multiplicit� de vecteurs nodaux de courbe Bspline non support�e";

//Example of utilization of this message:
//The entity DE=1269 of type 104 form 2 is not supported.
fileIGESelement.Msg.MyForme=" forme ";

fileIGESopenElement.Reading.Entity="   <I> [1300] [T=/p2] [/p1] \"Parser\";


fileIGESopenElement.error.Ptr3DCurve="L'entit� de type 142 ne poss�de pas de pointeur sur courbe 3D";

fileIGESopenElement.Error.Traitement="\nL'entit� DE=/p1 de type /p2 n'est pas support�e ";

fileIGESopenElement.Error.dimention2="\n Attention : le fichier contient de la g�om�trie 2D \n";


fileIGEStool.Error.IgsControlChars="Le fichier /p1 n'est pas conforme � la norme : /p2 caract�re(s) non autoris�(s) rencontr�(s): une tentative de nettoyage est r�alis�e.";
fileIGEStool.Error.IgsNoASCIIChars="   <W> [1350] Le fichier /p1 n'est pas conforme � la norme : /p2 caract�res ne sont pas autoris�s. Ils ont �t� remplac�s.";

fileIGESopenElement.Warning.Copiousdata="   <W> [1304] !! Une entit� de type Copious Data n'est pas transf�r�e � cause du manque de m�moire";

fileIGEStool.Warning.PtrDE="!! <E> [1305] [T=/p2] [#/p1] Erreur dans le fichier IGES : le pointeur de l'entr�e de r�pertoire n'est pas autoris� (/p3) dans le bloc Parameter Data";

fileIGESopenElement.Warning.EntNonSup="!! <W> [1306] [T=/p2] [#/p1] Ce type d'entit� n'est pas support� actuellement.";

fileIGESopenElement.Warning.FormNonSup="!! <W> [1307] [T=/p2] [#/p1] Cette forme de l'entit� (/p3) n'est pas support�e actuellement.";

fileIGESopenElement.Error.MidDE="!! <E> [1308] [T=/p2] [#/p1] Erreur dans le fichier IGES : le pointeur de Parameter Data ne renvoie pas au premier enregistrement du bloc Parameter Data de l'entit�.";

fileIGESopenElement.Warning.ConicBadForm="   <W> [1309] [T=/p2] [#/p1] Donn�es invalides dans le fichier IGES : la forme indiqu�e pour cette entit� n'est pas correcte.  Elle a �t� remplac�e par 0.";

fileIGESelement.Msg.InterruptConversion="   <W> [1310] Le bouton d'interruption de transfert a �t� activ� : La conversion est arr�t�e.";

fileIGES.Error.GlobalDataDefault="****************************
!! <E> [1311] Donn�es non valides : Impossible de lire la section globale dans le fichier IGES ! Les valeurs par d�faut sont utilis�es...
****************************";


//---------------------
//  Messages from fsb*
//---------------------

fsb.Msg.GlobaleSection="\n ***** INFORMATIONS FICHIERS IGES : SECTION GLOBALE *****";

fsb.Msg.IdentificationSender="\nIdentification du produit venant de l'exp�diteur         : ";

fsb.Msg.FileName="\nNom du fichier                                           : ";

fsb.Msg.SystemID="\nIdentification du syst�me                                : ";

fsb.Msg.Preprocessor="\nVersion du pr�processeur                                 : ";

fsb.Msg.NbBinary="\nNombre de bits binaires pour la repr�sentation d'entier  : ";

fsb.Msg.SingleMagnitude="\nGrandeur des simples pr�cisions                          : ";

fsb.Msg.SingleSignifiance="\nImportance des simples pr�cisions                        : ";

fsb.Msg.DoubleMagnitude="\nGrandeur des doubles pr�cisions                          : ";

fsb.Msg.DoubleSignifiance="\nImportance des doubles pr�cisions                        : ";

fsb.Msg.IdentificationReceiver="\nIdentification du produit pour le destinataire           : ";

fsb.Msg.SpaceScale="\nEchelle spatiale du mod�le                               : ";

fsb.Msg.Flag="\nMarqueur d'unit�                                         : ";

fsb.Msg.Units="\nUnit�s                                                   : ";

fsb.Msg.Gradations="\nNombre maximum de graduations de l'�paisseur du trait    : ";

fsb.Msg.WidthUnits="\nLargeur maximale de l'�paisseur du trait en unit�s       : ";

fsb.Msg.DateTimeExchange="\nDate et heure de g�n�ration du fichier d'�change         : ";

fsb.Msg.Resolution="\nR�solution minimale demand�e par l'utilisateur           : ";

fsb.Msg.Coordinate="\nPlus grande valeur absolue                               : ";

fsb.Msg.Author="\nNom de l'auteur                                          : ";

fsb.Msg.Organization="\nNom de l'organisation                                    : ";

fsb.Msg.Version="\nNum�ro de version                                        : ";

fsb.Msg.Code="\nCode de la norme de dessin                               : ";

fsb.Msg.DateTimeModif="\nDate et heure de la cr�ation ou modification du mod�le   : ";

fsb.Msg.Specification="\nMil-specification ( protocole W. E. I. Y. I.  )               : ";

fsb.Msg.StartSection="\n\n\n ***** INFORMATIONS DU FICHIER IGES  : EN-TETE  *****\n";

fsb.Msg.Info="Pas d'information";

fsb.Msg.Error="
!! <E> [1007] La structure du fichier en entr�e n'est pas conforme � la norme IGES.
              Impossible de lire cette section. V�rifiez le fichier IGES.\n";

fsbDataTreatment.Warning.ThereIsInvalidData="\n *****  A T T E N T I O N  *****\n   <W> [1000] Ce fichier contient des DONNEES INVALIDES selon la norme IGES : /p3\n";

fsbDataTreatment.Warning.No3DCrvInLoop="   <W> [1001] Certaines fronti�res n'ont pas de courbe 3D /p3";

fsbDataTreatment.Warning.No2DCrvInLoop="   <W> [1002] Certaines fronti�res n'ont pas de courbe param�trique /p3";

fsbDataTreatement.Processtopo.Storing="   <I> [1003] [T=/p2] [/p1] \"Parser\";

fsbDataTreatement.Processtopo.514Invalid="   <E> [1004] [T=/p2] [/p1] \"Parser\";

fsbDataTreatement.Processtopo.186Invalid="   <E> [1005] [T=/p2] [/p1] \"Parser\";

fsbDataTreatment.Warning.3DCrvWithFlag2D="   <W> [1006] [T=/p2] [/p1] Le marqueur d'utilisation de l'entit� (2D) est incorrect.  Il n'a pas �t� pris en compte. ";

//----------------------------------------------
//  Messages from CATIgesBSplineSurface
//---------------------------------------------

CATIgesBSplineSurface.Warning.BSplSurfMLK="   <W> [1500] [T=/p2] [/p1] MLK /p3";

CATIgesBSplineSurface.Warning.CopyElemTabError="   <W> [1501] [T=/p2] [/p1] D�bordement de tableau /p3";



//-----------------------------------------------
//  Messages from CATIgesBSplineCurve
//-----------------------------------------------

CATIgesBSplineCurve.Warning.ActiveSplitKO="   <W> [1550] [T=/p2] [/p1] Exception d�tect�e lors du d�coupage des courbes 3D /p3";

CATIgesBSplineCurve.Warning.GetLimitsOnCvKO="   <W> [1551] [T=/p2] [/p1] Echec de la nouvelle m�thode pour r�cup�rer les limites /p3";



//---------------------------------------------
//  Messages from CATIgesParametricSplineCurve
//---------------------------------------------

// Warning : Probleme sur les points de controle de la loop 2D - dfb
CATIgesParametricSplineCurve.Warning.Bad_ControlPoints2D="   <W> [1650] [T=/p2] [/p1] La spline param�trique comprend des points de contr�le non autoris�s ; traitement du contour avec une repr�sentation 3D/p4";

CATIgesParametricSplineCurve.Error.Bad_Data="   <W> [1651] [T=/p2] [/p1] Les donn�es lues dans le fichier IGES sont invalides /p3";


//----------------------------------------------
//  Messages from CATIgesConicArc
//---------------------------------------------

CATIgesConicArc.Error.IncorrectEllipse="   <W> [1701] Donn�es invalides ou non support�es : la Conique de forme /p4 n'est pas reconnue comme une Ellipse /p3";

CATIgesConicArc.Error.IncorrectHyperbola="   <W> [1702] Donn�es invalides : la Conique de forme 2 n'est pas reconnue comme une Hyperbole /p3";

CATIgesConicArc.Error.IncorrectParabola="   <W> [1703] Donn�es invalides : la Conique de forme 3 n'est pas reconnue comme une Parabole /p3";


//--------------------------------------
//  Messages from CATEIgesShapeFileSelection 
//--------------------------------------

GlobalFrame.NoLicense="Une licence MultiCad est requise pour acc�der � cette extension";


//------------------------------------------
//  Messages from CATIgesExportTypeManager 
//------------------------------------------

// Info : Import en mode CGR
CATIgesExportTypeManager.Info.CGRMode="   <I> [1800] D�but de l'import en mode CGR";


//------------------------------------------
//  Messages for CATIReporter 
//------------------------------------------

PathERR_1001.Request="Impossible de lire le fichier IGES.";
PathERR_1001.Diagnostic="Le fichier IGES n'existe pas ou n'est pas accessible en lecture.";
PathERR_1001.Advice="V�rifiez si le fichier existe et si vous disposez des droits n�cessaires pour le lire.";

PathERR_1021.Request="Le nom du fichier IGES � ouvrir n'est pas valide.";
PathERR_1021.Diagnostic="Le nom du fichier IGES contient des caract�res non ASCII ou est vide.";
PathERR_1021.Advice="Recherchez les caract�res non admis dans le nom de fichier. Renommez le fichier IGES si n�cessaire et essayez de l'ouvrir � nouveau.";

IGProdIdSend_1001.Request="Probl�me li� � l'identification du produit de l'�metteur";
IGProdIdSend_1001.Diagnostic="Probl�me li� � la section globale du fichier IGES : l'identification du produit de l'�metteur est erron�e";
IGProdIdSend_1001.Advice="V�rifiez le nom d'identification du produit de l'�metteur � la section globale du fichier IGES.";

IGProdIdSend_1001.Request="Probl�me li� au nom de fichier";
IGProdIdSend_1001.Diagnostic="Probl�me li� � la section globale du fichier IGES : probl�me li� au nom du fichier";
IGProdIdSend_1001.Advice="V�rifiez le chemin l'acc�s au fichier � la section globale du fichier IGES.";

IGSysId_1001.Request="Probl�me li� � l'ID syst�me natif";
IGSysId_1001.Diagnostic="Probl�me li� � la section globale du fichier IGES : probl�me li� � l'ID syst�me natif";
IGSysId_1001.Advice="V�rifiez l'ID syst�me natif � la section globale du fichier IGES.";

IGPrepVers_1001.Request="Probl�me li� � la version du pr�processeur";
IGPrepVers_1001.Diagnostic="Probl�me li� � la section globale du fichier IGES : probl�me li� � la version du pr�processeur";
IGPrepVers_1001.Advice="V�rifiez la version du pr�processeur � la section globale du fichier IGES.";

InvalidParameter_1001.Request="Les valeurs des param�tres ne sont pas valides";
InvalidParameter_1001.Diagnostic="Probl�me li� � la section globale du fichier IGES : les valeurs des param�tres ne sont pas valides. Voir le fichier de messages pour plus de d�tails (*.err)";
InvalidParameter_1001.Advice="V�rifiez les param�tres de la section globale du fichier IGES.";

InvalidMandatoryParameters_1001.Request="Les valeurs des param�tres utiles ne sont pas valides";
InvalidMandatoryParameters_1001.Diagnostic="Probl�me li� � la section globale du fichier IGES : les valeurs des param�tres utiles ne sont pas valides. Voir le fichier de messages pour plus de d�tails (*.err)";
InvalidMandatoryParameters_1001.Advice="V�rifiez les param�tres utiles � la section globale du fichier IGES.";

TooManyLinesForIGES_1001.Request="La taille des donn�es � exporter d�passe la limite autoris�e.";
TooManyLinesForIGES_1001.Diagnostic="Le standard IGES n'autorise pas l'exportation de fichiers aussi volumineux.";
TooManyLinesForIGES_1001.Advice="R�duire la taille des donn�es et relancer l'exportation.";
