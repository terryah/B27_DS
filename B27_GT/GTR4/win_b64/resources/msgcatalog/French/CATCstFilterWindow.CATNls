// ==========================================================
// Constraint filtering frame
// ==========================================================

Title = "Filtre de contrainte" ;
Help = "D�finit le filtre de contrainte";
ShortHelp="Filtre de contrainte";
LongHelp="Filtre de contrainte. D�finit le filtre de contrainte.";

// ==========================================================
// Title
// ==========================================================

FrameGlobal.HeaderFrame.Global.Title = "Filtre";
FrameGlobal.Help = "D�finit le niveau de filtrage";
FrameGlobal.ShortHelp = "D�finit le niveau de filtrage";
FrameGlobal.LongHelp = "D�finit le niveau de filtrage. Si le filtre conditionnel est actif, les filtres ci-dessous sont disponibles.  ";

// ==========================================================
// Visible_all frame
// ==========================================================

FrameGlobal.IconAndOptionsFrame.OptionsFrame.Visible_all.Title = "Afficher tout";
FrameGlobal.IconAndOptionsFrame.OptionsFrame.Visible_all.Help = "Visualise toutes les contraintes";
FrameGlobal.IconAndOptionsFrame.OptionsFrame.Visible_all.ShortHelp = "Visualise toutes les contraintes";
FrameGlobal.IconAndOptionsFrame.OptionsFrame.Visible_all.LongHelp = "Visualise toutes les contraintes";

// ==========================================================
// Visible_not frame
// ==========================================================

FrameGlobal.IconAndOptionsFrame.OptionsFrame.Visible_not.Title = "Cacher tout";
FrameGlobal.IconAndOptionsFrame.OptionsFrame.Visible_not.Help = "Cache toutes les contraintes";
FrameGlobal.IconAndOptionsFrame.OptionsFrame.Visible_not.ShortHelp = "Cache toutes les contraintes";
FrameGlobal.IconAndOptionsFrame.OptionsFrame.Visible_not.LongHelp = "Cache toutes les contraintes";

// ==========================================================
// Visiblefiltre frame
// ==========================================================

FrameGlobal.IconAndOptionsFrame.OptionsFrame.Visiblefiltre.Title = "Filtre conditionnel";
FrameGlobal.IconAndOptionsFrame.OptionsFrame.Visiblefiltre.Help = "Visualise les contraintes en fonction des filtres d�finis ci-dessous";
FrameGlobal.IconAndOptionsFrame.OptionsFrame.Visiblefiltre.ShortHelp = "Visualise les contraintes en fonction des filtres d�finis ci-dessous";
FrameGlobal.IconAndOptionsFrame.OptionsFrame.Visiblefiltre.LongHelp = "Visualise les contraintes en fonction des filtres d�finis ci-dessous";

// ==========================================================
//  frame
// ==========================================================

FrameVerify.HeaderFrame.Global.Title = "Filtrer selon l'�tat";
FrameVerify.Help= "Visualise les contraintes en fonction de leur �tat";
FrameVerify.ShortHelp= "Visualise les contraintes en fonction de leur �tat";
FrameVerify.LongHelp= "Visualise les contraintes en fonction de leur �tat : v�rifi�es seulement, non v�rifi�es seulement\n ou toutes";

// ==========================================================
// Verify frame
// ==========================================================

FrameVerify.IconAndOptionsFrame.OptionsFrame.Verify.Title = "Filtrer les contraintes v�rifi�es";
FrameVerify.IconAndOptionsFrame.OptionsFrame.Verify.Help= "Ne visualise que les contraintes v�rifi�es";
FrameVerify.IconAndOptionsFrame.OptionsFrame.Verify.ShortHelp= "Ne visualise que les contraintes v�rifi�es";
FrameVerify.IconAndOptionsFrame.OptionsFrame.Verify.LongHelp= "Ne visualise que les contraintes v�rifi�es";

// ==========================================================
// Not_Verify frame
// ==========================================================

FrameVerify.IconAndOptionsFrame.OptionsFrame.Not_Verify.Title = "Filtrer les contraintes non v�rifi�es";
FrameVerify.IconAndOptionsFrame.OptionsFrame.Not_Verify.Help= "Ne visualise que les contraintes non v�rifi�es";
FrameVerify.IconAndOptionsFrame.OptionsFrame.Not_Verify.ShortHelp= "Ne visualise que les contraintes non v�rifi�es";
FrameVerify.IconAndOptionsFrame.OptionsFrame.Not_Verify.LongHelp= "Ne visualise que les contraintes non v�rifi�es";

// ==========================================================
// Undefined frame
// ==========================================================

FrameVerify.IconAndOptionsFrame.OptionsFrame.Undefined.Title = "Ignorer l'�tat";
FrameVerify.IconAndOptionsFrame.OptionsFrame.Undefined.Help= "Visualise les contraintes v�rifi�es et non v�rifi�es";
FrameVerify.IconAndOptionsFrame.OptionsFrame.Undefined.ShortHelp= "Visualise les contraintes v�rifi�es et non v�rifi�es";
FrameVerify.IconAndOptionsFrame.OptionsFrame.Undefined.LongHelp= "Visualise les contraintes v�rifi�es et non v�rifi�es";

// ==========================================================
// OtherOptions frame
// ==========================================================

OtherOptions.HeaderFrame.Global.Title = "Produit";
OtherOptions.Help= "Visualise les contraintes du produit actif dans Assembly Design";
OtherOptions.ShortHelp= "Visualise les contraintes du produit actif dans Assembly Design";
OtherOptions.LongHelp= "Visualise les contraintes du produit actif dans Assembly Design";

OtherOptions.IconAndOptionsFrame.OptionsFrame.OnFocus.Title = "Afficher les contraintes du produit actif";
OtherOptions.IconAndOptionsFrame.OptionsFrame.OnFocus.Help= "Affiche les contraintes du produit actif uniquement";
OtherOptions.IconAndOptionsFrame.OptionsFrame.OnFocus.ShortHelp= "Affiche les contraintes du produit actif uniquement";
OtherOptions.IconAndOptionsFrame.OptionsFrame.OnFocus.LongHelp= "Affiche les contraintes du produit actif uniquement";

OtherOptions.IconAndOptionsFrame.OptionsFrame.OnlyPartFocus.Title = "Afficher sur la pi�ce uniquement ";
OtherOptions.IconAndOptionsFrame.OptionsFrame.OnlyPartFocus.Help= "Affiche les contraintes uniquement lorsque la pi�ce est active ";
OtherOptions.IconAndOptionsFrame.OptionsFrame.OnlyPartFocus.ShortHelp= "Affiche les contraintes uniquement lorsque la pi�ce est active ";
OtherOptions.IconAndOptionsFrame.OptionsFrame.OnlyPartFocus.LongHelp= "Affiche les contraintes uniquement lorsque la pi�ce est active et masque les contraintes lorsque le produit est actif ";

// ==========================================================
// Type frame
// ==========================================================

ConstraintTypeFrame.HeaderFrame.Global.Title = "Filtre par type";
ConstraintTypeFrame.Help= "Visualise les contraintes en fonction de leur type";
ConstraintTypeFrame.ShortHelp= "Visualise les contraintes en fonction de leur type";
ConstraintTypeFrame.LongHelp= "Visualise les contraintes en fonction de leur type";

// ==========================================================
// Filter_Distance frame
// ==========================================================

ConstraintTypeFrame.IconAndOptionsFrame.OptionsFrame.Filter_Distance.Title = "Dimensions";
ConstraintTypeFrame.IconAndOptionsFrame.OptionsFrame.Filter_Distance.Help= "Visualise les contraintes de dimension";
ConstraintTypeFrame.IconAndOptionsFrame.OptionsFrame.Filter_Distance.ShortHelp= "Visualise les contraintes de dimension";
ConstraintTypeFrame.IconAndOptionsFrame.OptionsFrame.Filter_Distance.LongHelp= "Visualise les contraintes de dimension : \n distances, longueurs, rayons ...";

// ==========================================================
// Filter_Angle frame
// ==========================================================

ConstraintTypeFrame.IconAndOptionsFrame.OptionsFrame.Filter_Angle.Title = "Angles";
ConstraintTypeFrame.IconAndOptionsFrame.OptionsFrame.Filter_Angle.Help= "Visualise les contraintes d'angle";
ConstraintTypeFrame.IconAndOptionsFrame.OptionsFrame.Filter_Angle.ShortHelp= "Visualise les contraintes d'angle";
ConstraintTypeFrame.IconAndOptionsFrame.OptionsFrame.Filter_Angle.LongHelp= "Visualise les contraintes d'angle";

// ==========================================================
// Filter_FixedAngle frame
// ==========================================================

ConstraintTypeFrame.IconAndOptionsFrame.OptionsFrame.Filter_FixedAngle.Title = "Angles fix�s";
ConstraintTypeFrame.IconAndOptionsFrame.OptionsFrame.Filter_FixedAngle.Help= "Visualise les contraintes d'angle impos� (perpendicularit�, tangence ... ) ";
ConstraintTypeFrame.IconAndOptionsFrame.OptionsFrame.Filter_FixedAngle.ShortHelp= "Visualise les contraintes d'angle impos� (perpendicularit�, tangence ... ) ";
ConstraintTypeFrame.IconAndOptionsFrame.OptionsFrame.Filter_FixedAngle.LongHelp= "Visualise les contraintes d'angle impos� (perpendicularit�, tangence ... ) ";

// ==========================================================
// Filter_Tangency frame
// ==========================================================

ConstraintTypeFrame.IconAndOptionsFrame.OptionsFrame.Filter_Tangency.Title = "Tangences";
ConstraintTypeFrame.IconAndOptionsFrame.OptionsFrame.Filter_Tangency.Help= "Visualise les contraintes de tangence";
ConstraintTypeFrame.IconAndOptionsFrame.OptionsFrame.Filter_Tangency.ShortHelp= "Visualise les contraintes de tangence";
ConstraintTypeFrame.IconAndOptionsFrame.OptionsFrame.Filter_Tangency.LongHelp= "Visualise les contraintes de tangence";

// ==========================================================
// Filter_Coincidence frame
// ==========================================================

ConstraintTypeFrame.IconAndOptionsFrame.OptionsFrame.Filter_Coincidence.Title = "Co�ncidences";
ConstraintTypeFrame.IconAndOptionsFrame.OptionsFrame.Filter_Coincidence.Help= "Visualise les contraintes de co�ncidence";
ConstraintTypeFrame.IconAndOptionsFrame.OptionsFrame.Filter_Coincidence.ShortHelp= "Visualise les contraintes de co�ncidence";
ConstraintTypeFrame.IconAndOptionsFrame.OptionsFrame.Filter_Coincidence.LongHelp= "Visualise les contraintes de co�ncidence";

// ==========================================================
// Filter_Equality frame
// ==========================================================

ConstraintTypeFrame.IconAndOptionsFrame.OptionsFrame.Filter_Equality.Title = "Egalit�s";
ConstraintTypeFrame.IconAndOptionsFrame.OptionsFrame.Filter_Equality.Help= "Visualise les contraintes d'�galit�";
ConstraintTypeFrame.IconAndOptionsFrame.OptionsFrame.Filter_Equality.ShortHelp= "Visualise les contraintes d'�galit�";
ConstraintTypeFrame.IconAndOptionsFrame.OptionsFrame.Filter_Equality.LongHelp= "Visualise les contraintes d'�galit�";

// ==========================================================
// Filter_Fixity frame
// ==========================================================

ConstraintTypeFrame.IconAndOptionsFrame.OptionsFrame.Filter_Fixity.Title = "Fixit�s";
ConstraintTypeFrame.IconAndOptionsFrame.OptionsFrame.Filter_Fixity.Help= "Visualise les contraintes de fixit�";
ConstraintTypeFrame.IconAndOptionsFrame.OptionsFrame.Filter_Fixity.ShortHelp= "Visualise les contraintes de fixit�";
ConstraintTypeFrame.IconAndOptionsFrame.OptionsFrame.Filter_Fixity.LongHelp= "Visualise les contraintes de fixit�";

// ==========================================================
// Filter_Concentric frame
// ==========================================================

ConstraintTypeFrame.IconAndOptionsFrame.OptionsFrame.Filter_Concentric.Title = "Concentricit�s";
ConstraintTypeFrame.IconAndOptionsFrame.OptionsFrame.Filter_Concentric.Help= "Visualise les contraintes de concentricit�";
ConstraintTypeFrame.IconAndOptionsFrame.OptionsFrame.Filter_Concentric.ShortHelp= "Visualise les contraintes de concentricit�";
ConstraintTypeFrame.IconAndOptionsFrame.OptionsFrame.Filter_Concentric.LongHelp= "Visualise les contraintes de concentricit�";

// ==========================================================
// Filter_Chamfer frame
// ==========================================================

ConstraintTypeFrame.IconAndOptionsFrame.OptionsFrame.Filter_Chamfer.Title = "Chanfreins";
ConstraintTypeFrame.IconAndOptionsFrame.OptionsFrame.Filter_Chamfer.Help= "Visualise les contraintes de chanfrein";
ConstraintTypeFrame.IconAndOptionsFrame.OptionsFrame.Filter_Chamfer.ShortHelp= "Visualise les contraintes de chanfrein";
ConstraintTypeFrame.IconAndOptionsFrame.OptionsFrame.Filter_Chamfer.LongHelp= "Visualise les contraintes de chanfrein";

// ==========================================================
// Filter_Measured frame
// ==========================================================

ConstraintTypeFrame.IconAndOptionsFrame.OptionsFrame.Filter_Measured.Title = "Pilot�es";
ConstraintTypeFrame.IconAndOptionsFrame.OptionsFrame.Filter_Measured.Help= "Visualise les contraintes Pilot�es";
ConstraintTypeFrame.IconAndOptionsFrame.OptionsFrame.Filter_Measured.ShortHelp= "Visualise les contraintes Pilot�es";
ConstraintTypeFrame.IconAndOptionsFrame.OptionsFrame.Filter_Measured.LongHelp= "Visualise les contraintes Pilot�es";

// ==========================================================
// Filter_Constrained frame
// ==========================================================

ConstraintTypeFrame.IconAndOptionsFrame.OptionsFrame.Filter_Constrained.Title = "Directrices";
ConstraintTypeFrame.IconAndOptionsFrame.OptionsFrame.Filter_Constrained.Help= "Visualise les contraintes directrices";
ConstraintTypeFrame.IconAndOptionsFrame.OptionsFrame.Filter_Constrained.ShortHelp= "Visualise les contraintes directrices";
ConstraintTypeFrame.IconAndOptionsFrame.OptionsFrame.Filter_Constrained.LongHelp= "Visualise les contraintes directrices";




