// COPYRIGHT DASSAULT SYSTEMES 1998
//===========================================================================
//
// CATMatOptionsFrame (English)
//
//===========================================================================
// 
//
materialFrame.HeaderFrame.Global.Title = "Options";

materialFrame.IconAndOptionsFrame.OptionsFrame.materialWarningDisplay.Title = "Display a warning when adding properties to a material";
materialFrame.IconAndOptionsFrame.OptionsFrame.materialWarningDisplay.Help = "Turns on warning display when adding new properties to a material";
materialFrame.IconAndOptionsFrame.OptionsFrame.materialWarningDisplay.ShortHelp = "Turns on warning display when adding new properties";
materialFrame.IconAndOptionsFrame.OptionsFrame.materialWarningDisplay.LongHelp =
"Warning is displayed when new applicative 
properties are added to a material.";

materialFrame.IconAndOptionsFrame.OptionsFrame.materialLinkMode.Title = "Use Link mode by default when applying a material";
materialFrame.IconAndOptionsFrame.OptionsFrame.materialLinkMode.Help = "Turns link mode on when applying materials";
materialFrame.IconAndOptionsFrame.OptionsFrame.materialLinkMode.ShortHelp = "Turns link mode on";
materialFrame.IconAndOptionsFrame.OptionsFrame.materialLinkMode.LongHelp =
"Link mode is activated when applying materials on products, parts,
bodies  or surfaces.";

materialFrame.IconAndOptionsFrame.OptionsFrame.materialForceMode.Title = "Use Force mode as the default inheritance mode";
materialFrame.IconAndOptionsFrame.OptionsFrame.materialForceMode.Help = "Turns forced inheritance mode on when applying materials";
materialFrame.IconAndOptionsFrame.OptionsFrame.materialForceMode.ShortHelp = "Turns forced mode on";
materialFrame.IconAndOptionsFrame.OptionsFrame.materialForceMode.LongHelp =
"Forced inheritance mode is activated when applying materials on products, parts,
bodies  or surfaces.";

materialFrame.IconAndOptionsFrame.OptionsFrame.materialSynchroMode.Title = "Desynchronize visualization when modifying material attributes";
materialFrame.IconAndOptionsFrame.OptionsFrame.materialSynchroMode.Help = "Desynchronize visualization when modifying material";
materialFrame.IconAndOptionsFrame.OptionsFrame.materialSynchroMode.ShortHelp = "Desynchronize visualization";
materialFrame.IconAndOptionsFrame.OptionsFrame.materialSynchroMode.LongHelp =
"When modifying material attributes, model visualization is not synchronized.";

materialFrame.IconAndOptionsFrame.OptionsFrame.catalogReadOnlyMode.Title = "Open the catalog in read-write mode when applying a material";
materialFrame.IconAndOptionsFrame.OptionsFrame.catalogReadOnlyMode.Help = "Applying material opens material catalog in read/write mode";
materialFrame.IconAndOptionsFrame.OptionsFrame.catalogReadOnlyMode.ShortHelp = "Open catalog in read/write mode";
materialFrame.IconAndOptionsFrame.OptionsFrame.catalogReadOnlyMode.LongHelp =
"When applying a material, material catalog is opened in read/write mode instead of read only mode.";


materialFrame.IconAndOptionsFrame.OptionsFrame.displayBorkenLinkMode.Title = "Display broken material link in spectree";
materialFrame.IconAndOptionsFrame.OptionsFrame.displayBorkenLinkMode.Help = "Display broken material link in spectree";
materialFrame.IconAndOptionsFrame.OptionsFrame.displayBorkenLinkMode.ShortHelp = "Display broken material link in spectree";
materialFrame.IconAndOptionsFrame.OptionsFrame.displayBorkenLinkMode.LongHelp =
"When a link material is broken, in spectree we display a broken material link icon.";

parameterFrame.HeaderFrame.Global.Title = "Material Parameters";

parameterFrame.IconAndOptionsFrame.OptionsFrame.materialParameterCreation.Title = "Create a material parameter when creating a part, a product, a body or a surface";
parameterFrame.IconAndOptionsFrame.OptionsFrame.materialParameterCreation.Help = "Turns on material parameter automatic creation on every new part/body/surface";
parameterFrame.IconAndOptionsFrame.OptionsFrame.materialParameterCreation.ShortHelp = "Turns on material parameter automatic creation";
parameterFrame.IconAndOptionsFrame.OptionsFrame.materialParameterCreation.LongHelp =
"If creation is turned on, a material parameter will be automatically
created for every new part, new body or new surface even if no material is mapped.";

parameterFrame.IconAndOptionsFrame.OptionsFrame.materialParameterLink.Title = "Create a link to the catalog when modifying a material parameter";
parameterFrame.IconAndOptionsFrame.OptionsFrame.materialParameterLink.Help = "Parameters apply materials as link to catalog";
parameterFrame.IconAndOptionsFrame.OptionsFrame.materialParameterLink.ShortHelp = "Parameters apply materials as link";
parameterFrame.IconAndOptionsFrame.OptionsFrame.materialParameterLink.LongHelp =
"Define if material applied after material parameter modification 
is applied as link or not.";

parameterFrame.IconAndOptionsFrame.OptionsFrame.materialParameterUpdate.Title = "Synchronize material parameter and applied material after a PowerCopy";
parameterFrame.IconAndOptionsFrame.OptionsFrame.materialParameterUpdate.Help = "Synchronize material parameter and applied material inside a CATPart after PowerCopy";
parameterFrame.IconAndOptionsFrame.OptionsFrame.materialParameterUpdate.ShortHelp = "Synchornization of applied material and parameter after PowerCopy";
parameterFrame.IconAndOptionsFrame.OptionsFrame.materialParameterUpdate.LongHelp =
"PowerCopy can modify the material parameter. If update is turned on then each applied material insied a CATPart will be synchronized with its
associated material parameter. This update is already done by default for applied materials on Products and Part instances";


pathFrame.HeaderFrame.Global.Title = "Default Material Catalog Path";

pathFrame.IconAndOptionsFrame.OptionsFrame.materialCatalogEditor.Help = "Set the path for default material catalog";
pathFrame.IconAndOptionsFrame.OptionsFrame.materialCatalogEditor.ShortHelp = "Default material catalog path";
pathFrame.IconAndOptionsFrame.OptionsFrame.materialCatalogEditor.LongHelp =
"Define the path name for default material catalog used with Apply Material command.";
pathFrame.IconAndOptionsFrame.OptionsFrame.catalogPath.Title = "Material Catalog Path";
pathFrame.IconAndOptionsFrame.OptionsFrame.pathComment.Title = "(this path is used to retrieve the default catalog for applying materials";
pathFrame.IconAndOptionsFrame.OptionsFrame.pathComment2.Title = "and supersedes the path defined in CATStartupPath environment variable)";


envImageFrame.HeaderFrame.Global.Title = "Environment Image File";

envImageFrame.IconAndOptionsFrame.OptionsFrame.envImageEditor.Help = "Set the file used for environment image";
envImageFrame.IconAndOptionsFrame.OptionsFrame.envImageEditor.ShortHelp = "Sets the environment image file";
envImageFrame.IconAndOptionsFrame.OptionsFrame.envImageEditor.LongHelp =
"Define the image file used for environment mapping.";
envImageFrame.IconAndOptionsFrame.OptionsFrame.ImagePath.Title = "Environment Image";

envImageFrame.IconAndOptionsFrame.OptionsFrame.EnvPushButton.Help = "Environment image generator";
envImageFrame.IconAndOptionsFrame.OptionsFrame.EnvPushButton.ShortHelp = "Environment image generator";
envImageFrame.IconAndOptionsFrame.OptionsFrame.EnvPushButton.LongHelp =
"Environment image generator.";
