CATTPSQuickDimCmd.UndoTitle                    = "Create a dimension";
CATTPSQuickDimCmd.InitialState.Message         = "Select a geometric element or an annotation";
CATTPSQuickDimCmd.InitialState.Help            = "Select a geometric element or an annotation";
CATTPSQuickDimCmd.LoopState.Message            = "Enter the characteristics of the tolerance in the dialog box or select additional geometry";
CATTPSQuickDimCmd.LoopState.Help               = "Enter the characteristics of the tolerance in the dialog box or select additional geometry";
CATTPSQuickDimCmd.SelectTPS.UndoTitle          = "Element Selection";
CATTPSQuickDimCmd.TPSClickVide.UndoTitle       = "Empty Selection";
CATTPSQuickDimCmd.DModeAgent.UndoTitle         = "Design Mode";
CATTPSQuickDimCmd.SelectTPSView.UndoTitle      = "View Selection";
CATTPSQuickDimCmd.ElementCtx.UndoTitle         = "Contextual Menu";
CATTPSQuickDimCmd.EndCtx.UndoTitle             = "End Contextual Menu";
CATTPSQuickDimCmd.AgentPanel.UndoTitle         = "Panel";
CATTPSQuickDimCmd.CreateDim.Message            = "Create a dimension";
CATTPSQuickDimCmd.CreateDim.Help               = "Create a dimension";
CATTPSQuickDimCmd.OrientBarRessource.UndoTitle = "Orientation";
CATTPSQuickDimCmd.SystemBarRessource.UndoTitle = "System";

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Interactive Dimension Cmd : SwitchAgent related to Orientation purpose ...
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

CATTPSQuickDimCmd.OrientBarRessource.Bar.Title="Orientation";

//******************************************************************************
// Reference Element Button - V5R3 Only
//******************************************************************************
CATTPSQuickDimCmd.OrientBarRessource.RefElem.Title = "Reference Element";
CATTPSQuickDimCmd.OrientBarRessource.RefElem.ShortHelp="Reference Element";
CATTPSQuickDimCmd.OrientBarRessource.RefElem.Help=
"Dimension line orientation depends on the selected element";
CATTPSQuickDimCmd.OrientBarRessource.RefElem.LongHelp=
"Element Reference
Dimension line orientation depends on the selected element";

//******************************************************************************
// Reference View Button - V5R3 Only
//******************************************************************************
CATTPSQuickDimCmd.OrientBarRessource.RefVue.Title = "View Reference";
CATTPSQuickDimCmd.OrientBarRessource.RefVue.ShortHelp="View Reference";
CATTPSQuickDimCmd.OrientBarRessource.RefVue.Help=
"Dimension line orientation is linked to view directions";
CATTPSQuickDimCmd.OrientBarRessource.RefVue.LongHelp=
"View Reference
Dimension line orientation is linked to view directions";

//******************************************************************************
// Projected dimension Button
//******************************************************************************
CATTPSQuickDimCmd.OrientBarRessource._RefElem.Title = "Projected dimension";
CATTPSQuickDimCmd.OrientBarRessource._RefElem.ShortHelp="Projected dimension";
CATTPSQuickDimCmd.OrientBarRessource._RefElem.Help=
"Dimension line orientation depends on mouse position";
CATTPSQuickDimCmd.OrientBarRessource._RefElem.LongHelp=
"Projected dimension
Dimension line orientation depends on mouse position.";

//******************************************************************************
// Force dimension on element Button
//******************************************************************************
CATTPSQuickDimCmd.OrientBarRessource._RefAuto.Title = "Force dimension on element";
CATTPSQuickDimCmd.OrientBarRessource._RefAuto.ShortHelp="Force dimension on element";
CATTPSQuickDimCmd.OrientBarRessource._RefAuto.Help=
"Forces dimension line to be parallel to the dimensioned element";
CATTPSQuickDimCmd.OrientBarRessource._RefAuto.LongHelp=
"Force dimension on element
Forces dimension line to be parallel to the dimensioned element.";

//******************************************************************************
// Force horizontal dimension Button
//******************************************************************************
CATTPSQuickDimCmd.OrientBarRessource._RefHoriz.Title = "Force horizontal dimension in view";
CATTPSQuickDimCmd.OrientBarRessource._RefHoriz.ShortHelp="Force horizontal dimension in view";
CATTPSQuickDimCmd.OrientBarRessource._RefHoriz.Help=
"Forces dimension line to be horizontal in view";
CATTPSQuickDimCmd.OrientBarRessource._RefHoriz.LongHelp=
"Force horizontal dimension in view
Forces dimension line to be horizontal in view.";

//******************************************************************************
// Force vertical dimension Button
//******************************************************************************
CATTPSQuickDimCmd.OrientBarRessource._RefVert.Title = "Force vertical dimension in view";
CATTPSQuickDimCmd.OrientBarRessource._RefVert.ShortHelp="Force vertical dimension in view";
CATTPSQuickDimCmd.OrientBarRessource._RefVert.Help=
"Forces dimension line to be vertical in view";
CATTPSQuickDimCmd.OrientBarRessource._RefVert.LongHelp=
"Force vertical dimension in view
Forces dimension line to be vertical in view.";

//******************************************************************************
// Force user defined dimension Button
//******************************************************************************
CATTPSQuickDimCmd.OrientBarRessource._RefUser.Title = "Force dimension along a direction";
CATTPSQuickDimCmd.OrientBarRessource._RefUser.ShortHelp="Force dimension along a direction";
CATTPSQuickDimCmd.OrientBarRessource._RefUser.Help=
"Forces dimension line to be along a direction";
CATTPSQuickDimCmd.OrientBarRessource._RefUser.LongHelp=
"Force dimension along a direction
Forces dimension line to be along a direction.";

//******************************************************************************
// True length dimension Button
//******************************************************************************
CATTPSQuickDimCmd.OrientBarRessource.VraiGrandeur.Title = "True length dimension";
CATTPSQuickDimCmd.OrientBarRessource.VraiGrandeur.ShortHelp="True length dimension";
CATTPSQuickDimCmd.OrientBarRessource.VraiGrandeur.Help=
"Creates a dimension in true length dimension mode";
CATTPSQuickDimCmd.OrientBarRessource.VraiGrandeur.LongHelp=
"True length dimension
Creates a dimension in true length dimension mode.";

//******************************************************************************
// Force dimension along direction Button
//******************************************************************************
CATTPSQuickDimCmd.AlongDirBarRessource.DirectionParallel.Title = "Dimension along a direction";
CATTPSQuickDimCmd.AlongDirBarRessource.DirectionParallel.ShortHelp="Dimension along a direction";
CATTPSQuickDimCmd.AlongDirBarRessource.DirectionParallel.Help=
"Sets dimension line along a direction";
CATTPSQuickDimCmd.AlongDirBarRessource.DirectionParallel.LongHelp=
"Dimension along a direction
Sets dimension line along a direction.";

//******************************************************************************
// Force dimension perpendicular to direction Button
//******************************************************************************
CATTPSQuickDimCmd.AlongDirBarRessource.DirectionPerpendicular.Title = "Dimension perpendicular to a direction";
CATTPSQuickDimCmd.AlongDirBarRessource.DirectionPerpendicular.ShortHelp="Dimension perpendicular to a direction";
CATTPSQuickDimCmd.AlongDirBarRessource.DirectionPerpendicular.Help=
"Sets dimension line perpendicular to a direction";
CATTPSQuickDimCmd.AlongDirBarRessource.DirectionPerpendicular.LongHelp=
"Dimension perpendicular to a direction
Sets dimension line perpendicular to a direction.";

//******************************************************************************
// Force dimension along a fixed angle in view Button
//******************************************************************************
CATTPSQuickDimCmd.AlongDirBarRessource.DirectionAngle.Title = "Dimension along a fixed angle in view";
CATTPSQuickDimCmd.AlongDirBarRessource.DirectionAngle.ShortHelp="Dimension along a fixed angle in view";
CATTPSQuickDimCmd.AlongDirBarRessource.DirectionAngle.Help=
"Sets dimension line to a fixed angle in view";
CATTPSQuickDimCmd.AlongDirBarRessource.DirectionAngle.LongHelp=
"Dimension along a fixed angle in view
Sets dimension line to a fixed angle in view.";

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Interactive Dimension Cmd : SwitchAgent related to Dimension System
// purpose ...
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

CATTPSQuickDimCmd.SystemBarRessource.Bar.Title="Dimension System";

//******************************************************************************
// Chained Representation
//******************************************************************************
CATTPSQuickDimCmd.SystemBarRessource.Chained.Title = "Chained";
CATTPSQuickDimCmd.SystemBarRessource.Chained.ShortHelp="Chained";
CATTPSQuickDimCmd.SystemBarRessource.Chained.Help=
"Created dimensions will be gathered in a chained system mode";
CATTPSQuickDimCmd.SystemBarRessource.Chained.LongHelp=
"Chained
Created dimensions will be gathered in a chained system mode";

//******************************************************************************
// Horizontal View Button
//******************************************************************************
CATTPSQuickDimCmd.SystemBarRessource.Stacked.Title = "Stacked";
CATTPSQuickDimCmd.SystemBarRessource.Stacked.ShortHelp="Stacked";
CATTPSQuickDimCmd.SystemBarRessource.Stacked.Help=
"Created dimensions will be gathered in a stacked system mode";
CATTPSQuickDimCmd.SystemBarRessource.Stacked.LongHelp=
"Stacked
Created dimensions will be gathered in a stacked system mode";

//******************************************************************************
// Angle
//******************************************************************************
CATTPSQuickDimCmd.Angle.Dimension.EnglobingFrame.IntermediateFrame.Label.Title = "Angle: ";
CATTPSQuickDimCmd.Angle.Dimension.EnglobingFrame.IntermediateFrame.Label.ShortHelp = "Enter the angle from the direction";

//******************************************************************************
// Driving Dim Value
//******************************************************************************
CATTPSQuickDimCmd.Value.Dimension.EnglobingFrame.IntermediateFrame.Label.Title = "Value: ";
CATTPSQuickDimCmd.Value.Dimension.EnglobingFrame.IntermediateFrame.Label.ShortHelp = "Enter the value of the driving dimension";

//******************************************************************************
// Annotation Plane Validity
//******************************************************************************
TPSRuleAnnotationPlane.Title = "Annotation Plane Validity";
TPSRuleAnnotationPlane.Message = "The active view is not suitable for the dimension.
The dimension will be invalid.";

