// Running criteria Frame
RunningCriteriaFrmTitle = "Running Criteria";

ConvergenceCriteriaLblTitle = "Convergence speed : ";

ConvergenceSlow="Slow";
ConvergenceMedium="Medium";
ConvergenceFast="Fast";
ConvergenceInf="Infinite (hill-climbing)";

ConvergenceCmbHelp = "Defines the level of acceptation of bad solutions";
ConvergenceCmbShortHelp = "Defines the level of acceptation of bad solutions.\nThe lower the convergence speed, the higher the probability not to get stuck in local optima";
ConvergenceCmbLongHelp = "Defines the level of acceptation of bad solutions.\nIf your problems have many local optima, use the lowest speed.\nIf your problem has no local optima, choose infinite speed";

// Termination Criteria Frame
TerminationCriteriaFrmTitle = "Termination criteria";

MaxNbEvaluationsLblTitle = "Maximum number of updates";

MaxNoImprovementChkBTitle = "Consecutive updates without improvements";
MaxNoImprovementChkBHelp = "If this option is checked, the algorithm terminates if the best solution does not improve after a given number of iterations";
MaxNoImprovementChkBShortHelp = "Check this option if you want the algorithm to terminate if the best solution\ndoes not improve after a given number of iterations";
MaxNoImprovementChkBLongHelp = "When checked, enables the algorithm to terminate if the best solution\ndoes not improve after a given number of iterations";

MaxTimeChkBTitle = "Maximum time (minutes)";
MaxTimeChkBHelp = "Check this option to limit the optimization duration";
MaxTimeChkBShortHelp = "Check this option if you want to limit the optimization duration";
MaxTimeChkBLongHelp = "When checked, limits the optimization duration.\nWhen the duration is reached, the algorithm will finish the current iteration and stop";

NotPositivIntegerWg = "The given value must be an integer greater than 0. The editor will keep the old value.";
NotPositivRealWg = "The given value must be a real greater than 0. The editor will keep the old value.";

NotPositivIntegerWgHd = "User entry error";

//R14 stopping criteria for Objective, constraints and variables evolution over a sliding window of updates.
StopRelativeObjective = "Relative Objective";
StopAbsoluteObjective ="Absolute Objective";
StopRelativeCst ="Relative Constraints";
StopAbsoluteCst ="Absolute Constraints";
StopAbsoluteVariable ="Absolute Variables";
StopCriteriaWindow = "Criteria Window";
StopCriteria.Title = "Values Evolution Criteria";

StoppingCriteriaFrm.AdvancedStoppingCriteria.LongHelp = "Termination is based on the duration of the run and number of updates without improvement for all algorithms.\n Additional criteria are available for the algorithm using derivatives.\n Each criterion is evaluated on a number of updates specified by the Window criterion at the bottom of the list.\n 0 for any value means that the corresponding criterion is not taken into account.\n 0 for the window means that no criterion based on the objective/constraints/variables evolutions is taken into account.";
StoppingCriteriaFrm.AdvancedStoppingCriteria.ShortHelp = "Termination is based on the duration of the run and number of updates without improvement for all algorithms.\n Additional criteria are available for the algorithm using derivatives.\n Each criterion is evaluated on a number of updates specified by the Window criterion at the bottom of the list.\n 0 for any value means that the corresponding criterion is not taken into account.\n 0 for the window means that no criterion based on the objective/constraints/variables evolutions is taken into account.";



