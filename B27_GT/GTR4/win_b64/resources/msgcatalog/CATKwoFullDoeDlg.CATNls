Title = "Design Of Experiments";

TabContainer.DoeSettingsTabPage.Title = "Settings";
TabContainer.DoeSettingsTabPage.InParmsFrame.Title = "Select input parameters";
TabContainer.DoeSettingsTabPage.InParmsFrame.InParmsMultiList.ColumnTitle1 = "Name";
TabContainer.DoeSettingsTabPage.InParmsFrame.InParmsMultiList.ColumnTitle2 = "Inf. Range";
TabContainer.DoeSettingsTabPage.InParmsFrame.InParmsMultiList.ColumnTitle3 = "Sup. Range";
TabContainer.DoeSettingsTabPage.InParmsFrame.InParmsMultiList.ColumnTitle4 = "Nb of Levels";
TabContainer.DoeSettingsTabPage.InParmsFrame.InParmsMultiList.ColumnTitle4.Help = "Enter the number of points used to study this factor";
TabContainer.DoeSettingsTabPage.InParmsFrame.EmptyLabel2.Title = " ";
TabContainer.DoeSettingsTabPage.InParmsFrame.InParmsButtonsFrame.ModifyInParmsPushButton.Title = "Modify ranges and/or number of levels";
TabContainer.DoeSettingsTabPage.InParmsFrame.InParmsButtonsFrame.ModifyInParmsPushButton.Help = "Enables to add range(s) and/or number of levels to the selected parameter";
TabContainer.DoeSettingsTabPage.InParmsFrame.InParmsButtonsFrame.ModifyInParmsPushButton.ShortHelp = "Modify Range and/or Number of Levels";
TabContainer.DoeSettingsTabPage.InParmsFrame.InParmsButtonsFrame.ModifyInParmsPushButton.LongHelp = "Modify :\n- inf. range = inferior bound of the parameter's study range\n- sup. range = superior bound of the parameter's study range\n- nb of levels = how many points will be used to study the effect of this parameter (integer > 1))";
TabContainer.DoeSettingsTabPage.InParmsFrame.InParmsButtonsFrame.EditInParmsPushButton.Title = "Edit list";
TabContainer.DoeSettingsTabPage.InParmsFrame.InParmsButtonsFrame.EditInParmsPushButton.Help = "Enables to select the input parameters";
TabContainer.DoeSettingsTabPage.InParmsFrame.InParmsButtonsFrame.EditInParmsPushButton.ShortHelp = "Select Input Parameters";
TabContainer.DoeSettingsTabPage.InParmsFrame.InParmsButtonsFrame.EditInParmsPushButton.LongHelp = "Edit list\n To study the effects of the input parameters on your system, select the input parameters";
TabContainer.DoeSettingsTabPage.OutParmsFrame.Title = "Select output parameters";
TabContainer.DoeSettingsTabPage.OutParmsFrame.OutParmsMultiList.ColumnTitle1 = "Name";
TabContainer.DoeSettingsTabPage.OutParmsFrame.EditOutParmsPushButton.Title = "Edit list...";
TabContainer.DoeSettingsTabPage.InfosFrame.NbUpdatesLabel.Title = "Number of updates: ";
TabContainer.DoeSettingsTabPage.InfosFrame.DTFileLabel.Title =    "Output  file: ";
TabContainer.DoeSettingsTabPage.LaunchButtonsFrame.RunDoePushButton.Title = "Run DOE";
TabContainer.DoeSettingsTabPage.LaunchButtonsFrame.RunDoePushButton.Help = "Launches the DOE algorithm";
TabContainer.DoeSettingsTabPage.LaunchButtonsFrame.RunDoePushButton.ShortHelp = "Run the DOE";
TabContainer.DoeSettingsTabPage.LaunchButtonsFrame.RunDoePushButton.LongHelp = "Run DOE in order to :\n- create, display and save the table of experiments\n- compute the effect of each input factor on system's response\n- display curves to see these effects";
TabContainer.DoeSettingsTabPage.LaunchButtonsFrame.StopPushButton.Title = "Stop DOE";
TabContainer.DoeSettingsTabPage.LaunchButtonsFrame.StopPushButton.Help = "Stops the DOE algorithm";
TabContainer.DoeSettingsTabPage.LaunchButtonsFrame.StopPushButton.ShortHelp = "Stop DOE";
TabContainer.DoeSettingsTabPage.LaunchButtonsFrame.EmptyLabel01.Title = "  ";
TabContainer.DoeSettingsTabPage.LaunchButtonsFrame.EmptyLabel02.Title = "  ";
TabContainer.DoeSettingsTabPage.InfosFrame.EmptyLabel03.Title = "  ";
TabContainer.DoeSettingsTabPage.InfosFrame.BuildGraphCheckButton.Title = "Save curves in the output file (for Excel files only)";
TabContainer.DoeSettingsTabPage.InfosFrame.BuildGraphCheckButton.Help = "Saves the curves in the output file (for Excel files only) ";
//TabContainer.DoeSettingsTabPage.InfosFrame.WithGeomUpdateCheckButton.Title = "Show geometric updates during computations";
//TabContainer.DoeSettingsTabPage.InfosFrame.WithGeomUpdateCheckButton.Help = "Uncheck this option to increase performances";
TabContainer.DoeSettingsTabPage.InfosFrame.EmptyLabel04.Title = "  ";
TabContainer.DoeSettingsTabPage.InfosFrame.LightRunCheckButton.Title = "Run DOE without filling the undo log";
TabContainer.DoeSettingsTabPage.InfosFrame.LightRunCheckButton.Help = "If checked, enables to solve memory consumption problems";
TabContainer.DoeSettingsTabPage.InfosFrame.LightRunCheckButton.ShortHelp = "When this option is unchecked, the Undo log can be filled with much data, which can lead to memory saturations.\nCheck this option to avoid this problem.\nWARNING: after running the methods in this mode, you won't be able to undo all the actions done before the run.";
//TabContainer.DoeSettingsTabPage.InfosFrame.LightRunCheckButton.LongHelp = "When this option is unchecked, the Undo log can be filled with much data, which can lead to memory saturations.\nCheck this option to avoid this problem.\nWARNING: after running the methods in this mode, you won't be able to undo all the actions done before the run.";



TabContainer.DoeResultsTabPage.Title = "Results";
TabContainer.DoeResultsTabPage.DTFrame.Title = "Table of experiments";
TabContainer.DoeResultsTabPage.DTFrame.EmptyLabel23.Title = "  ";
TabContainer.DoeResultsTabPage.DTFrame.ApplyValuesPushButton.Title = "Apply values";
TabContainer.DoeResultsTabPage.DTFrame.EmptyLabel24.Title = "  ";
TabContainer.DoeResultsTabPage.CurvesFrame.Title = "Generated curves";
TabContainer.DoeResultsTabPage.CurvesFrame.SelectCurveFrame.SelectCurveLabel.Title = "Available curves ";
TabContainer.DoeResultsTabPage.CurvesFrame.SelectCurveFrame.EmptyLabel21.Title = "  ";
TabContainer.DoeResultsTabPage.CurvesFrame.SelectCurveFrame.EmptyLabel22.Title = "  ";

TabContainer.DoePredictionTabPage.Title = "Prediction";
TabContainer.DoePredictionTabPage.ValuateInParmsFrame.Title = "Valuate input parameters";
TabContainer.DoePredictionTabPage.ValuateInParmsFrame.InParmsValuesMultiList.ColumnTitle1 = "Name";
TabContainer.DoePredictionTabPage.ValuateInParmsFrame.InParmsValuesMultiList.ColumnTitle2 = "Inf. Range";
TabContainer.DoePredictionTabPage.ValuateInParmsFrame.InParmsValuesMultiList.ColumnTitle3 = "Sup. Range";
TabContainer.DoePredictionTabPage.ValuateInParmsFrame.InParmsValuesMultiList.ColumnTitle4 = "Value";
TabContainer.DoePredictionTabPage.ValuateInParmsFrame.InParmValueFrame.InParmValueLabel.Title = "Selected parameter value  ";
TabContainer.DoePredictionTabPage.ValuateInParmsFrame.InParmValueFrame.EmptyLabel33.Title = "  ";
TabContainer.DoePredictionTabPage.ValuateInParmsFrame.EmptyLabel31.Title = "  ";
TabContainer.DoePredictionTabPage.ValuateInParmsFrame.EmptyLabel32.Title = "  ";
TabContainer.DoePredictionTabPage.ValuateInParmsFrame.RunPredictionFrame.RunPredictionPushButton.Title = "Run prediction";
TabContainer.DoePredictionTabPage.OutParmsValuesFrame.Title = "Predicted response for each output";
TabContainer.DoePredictionTabPage.OutParmsValuesFrame.OutParmsValuesMultiList.ColumnTitle1 = "Name";
TabContainer.DoePredictionTabPage.OutParmsValuesFrame.OutParmsValuesMultiList.ColumnTitle2 = "Value";


//****** Pour le panel de choix des parms *****

ChooseInParmsDlgTitle="Select the input parameters";
ChooseOutParmsDlgTitle="Select the output parameters";
TooManyUpdates = "-- Error! Too many updates --";


//****** Pour le panel de choix du fichier de sortie *****

FileDlgTitle="Choose the DOE output file";
FileFailed="The output file could not be created.\nYou may not be allowed to write in the specified folder\n or you may try to overwrite an open file (close it to avoid access conflicts.)";
FileFailedHd = "Output file creation error";


//****** Pour le panel de DT/coubes et les panel de graphes *****

DTPageUpdateFailed = "The update of the results page which displays the Design table failed";
//DTPageUpdateFailedHd = "Updating DT Error ...";

CurvesPageUpdateFailed = "A problem occurred when generating the curves";
//CurvesPageUpdateFailedHd = "Updating curves page error ...";

AxeXEffectLabel = "Value of ";
AxeYEffectLabel = "Mean, min. and max. response of ";

EffectGraphDlgTitle_part1 = "Graph showing the mean effect of '";
EffectGraphDlgTitle_part2 = "' on the output : ";
EffectGraphDlgTitle_part3 = " (surrounded by min. and max. effect)";
EffectGraphDlgCaption_part1 = "Mean effect of ";
EffectGraphDlgCaption_part2 = " on ";
MinEffectGraphDlgCaption_part1 = "Min. effect of ";
MaxEffectGraphDlgCaption_part1 = "Max. effect of ";
MinMaxEffectGraphDlgCaption_part2 = " on ";
InteractionGraphDlgTitle_part1 = "Graph of interaction between '";
InteractionGraphDlgTitle_part2 = "' and '";
InteractionGraphDlgTitle_part3 = "' on the output : ";
InteractionGraphDlgCaption_part1 = "Effect of ";
InteractionGraphDlgCaption_part2 = " when ";
InteractionGraphDlgCaption_part3 = " = ";

//EffectLineCombo_part1 = "Plot mean effect of '";
EffectLineCombo_part2 = "' on the output : ";
InteractionLineCombo_part1 = "Plot interaction between '";
InteractionLineCombo_part2 = "' and '";
InteractionLineCombo_part3 = "' on the output : ";

EffectLineComboForRecord_part1 = "Plot mean effect of '";				// NE PAS MODIFIER : ce texte n'est visible qu'en mode record
EffectLineComboForRecord_part2 = "' on the output : ";					// ce sont les lignes qui apparaissent ds la
InteractionLineComboForRecord_part1 = "Plot interaction between '";		// combo box en mode record, comme �a, mm si
InteractionLineComboForRecord_part2 = "' and '";						// on modifie les lignes qui sont affich�es �
InteractionLineComboForRecord_part3 = "' on the output : ";				// l'utilisateur, �a ne casse pas les records !

EffectGraphsHelp = "Drag the mouse over a curve to get the slope at this point. Compare the slopes for different factors on the same output to get the most influent one in the considered space part";
InteractionGraphsHelp = "Drag the mouse over a curve to get the slope at this point. For a given abscissa, if all slopes are equal, there is no interaction in the space part (effect of factor A does not depend on factor B)";

AxeXInteractionLabel = "Value of ";
AxeYInteractionLabel = "Average response of ";

AskForReplaceDTFileTitle = "Running algorithm option ...";
AskForReplaceDTFileRestartOnly = "This DOE has already run but you are going to restart it : \n\n- click on 'OK' to restart the algorithm and overwrite the output file\n- click on 'Cancel' to abort the run";
AskForReplaceDTFileWithContinue = "This DOE was interrupted while running last time and you want to pursue it :\n\n- click on 'Yes' to launch the algorithm from the point where it was stop last time\n- click on 'No' to restart it from the beginning\n- click on 'Cancel' to abort the run";

Continue = "Continue";
Restart = "Restart";
Cancel = "Cancel";

ContinueWarning = "If the DOE context has been modified since the last run,\n the results may make no sense.";
ContinueWarningHd = "Running information ...";

ContinueErrorOfParmNb = "The number of parameters has changed since the last run.\n\nYou cannot continue the run and you have to restart it to overwrite your output file";
ContinueErrorHd = "Running error ...";

ContinueErrorOfParmId = "At least one input and/or output parameter is different from the ones used for the last run.\n\nRestart it to overwrite the output file";

ContinueErrorOfLevelsNb = "At least one factor levels number has changed since the last run.\n\nRestart it to overwrite the output file ";

ContinueErrorOfCurrentUnit = "At least one unit was changed in the Tools->Options dialog box.\n\nTo continue the run, reset the current units as they were during the last run or restart the run from the beginning).";

FileNotFoundError = "The output file has been moved or deleted since it was created, so that it cannot be completed.\n\nStop the run and restart it to generate a new file";
FileNotFoundErrorHd = "Output file not found ...";

FileNotFoundQuestion = "The output file has been moved or deleted since it was created, or it may not be in READ/WRITE mode. Do you want to define another output file path and restart the run?";
FileNotFoundQuestionHd = "Output file not found ...";

FileNotFoundWarning = "The output file has been moved or deleted since it was created.\nIt is impossible to display its content in the second page";

FileReadOnlyError = "Output file rights incorrect...";
FileReadOnlyErrorHd = "The output file is in Read only mode. \nChange the rights.";

FilePathEmptyError = "The output file path is empty.\nIndicate a new output file name before running the DOE\n\nIf you select an existing file name, the corresponding file will be overwritten.";
//FilePathEmptyErrorHd = "Empty file path error ...";

SynchroError = "The input parameters list may have been modified since the last run so that it is not synchronized with the table of experiments.\nIt is impossible to apply the selected values to the input parameters";
SynchroErrorHd = "Synchronization error ...";


//****** Pour le panel de modif des ranges *****

RangeDlgTitle="Modify ranges/number of levels";

InvalidParmValue = "One of the input parameters value is out of its range";
InvalidParmValueHd = "Invalid parameter value";

RunningPredictionError = "An error occurred during the prediction run";
//RunningPredictionErrorHd = "Running prediction error ...";

NoInfRangeHd = "Inf. range missing";
NoInfRange = "Enter an inf. range for all the parameters";

NoSupRangeHd = "Sup. range missing";
NoSupRange = "Enter a sup. range for all the parameters";

NoLevelNbHd = "Number of levels missing";
NoLevelNb = "Enter a step for all the parameters";

RangesInvalid="The inferior range must be greater than the superior one!";
RangesInvalidHd="Invalid ranges";

LevelNbInvalid="The number of levels must be an integer superior or equal to 2";
LevelNbInvalidHd="Invalid number of levels";

InParmModif = "The Attribute(s) of at least one input parameter was changed.\nThe results and the prediction functionalities will be unavailable until you launch a new run";
InParmModifHd = "Modification of input parameter's attributes ...";


//****** Pour les panels de modif des parms d'entr�e et de sortie *****

InParmsListModif = "The input parameters list has been modified since the last run.\nThe results and the prediction functionalities will be unavailable until you launch a new run.";
//InParmsListModifHd = "Modification of input parameters list ...";

OutParmsListModif = "The output parameters list has been modified since the last run.\nThe results and the prediction functionalities will be unavailable until you launch a new run.";
OutParmsListModifHd = "Modification of output parameters list ...";


//***** Divers ***************

yes = "YES";
no = "NO";

NoSolution = "The algorithm was not launched.\nTo access the Results and Prediction tabs, run it once.";
NoSolutionHd = "Status information ...";

LoadingErrorOfLevelsNb = "The number of levels of at least one input factor has changed since the last run.\n\nIf you want to access all functionalities,\nundo these modifications, click OK and reopen this dialog box, or\nkeep these modifications and launch a new run";

LoadingErrorOfExpNb = "The number of levels of at least one input factors has changed since the last run.\nSome functionalities may not be available until you launch a new run.";
LoadingDoeInfoHd = "Loading DOE ...";

LoadingErrorOfParmNb = "The number of parameters  has changed since the last run.\n\nTo access all functionalities,\n undo these modifications, click OK and reopen this dialog box,\nor keep these modifications and perform a new run.";
LoadingErrorOfParmId = "The list of input and output parameters has changed since the last run.\n\nTo access all functionalities,\nundo these modifications, click OK and reopen this dialog box\n or keep these modifications and perform a new run.";
LoadingErrorOfParmUnit = "The current unit of at least one parameter has changed since the last run.\n\nTo access all functionalities,\nundo these modifications, click OK and reopen this dialog box\nor keep these modifications and perform a new run.";

CancelWarning = "This DOE may have been interrupted, so that the file content does not match the problem definition.\nSome functionalities may not be available until you perform a new run.";

InterruptedAlgoInfo = "The algorithm was interrupted.\nSome functionalities may not be available until you perform a new run.";
InterruptedAlgoInfoHd = "";

NoGraphBuiltInfo = "Curves can be saved only in an Excel output file";
NoGraphBuiltInfoHd = "Saving curves option is unavailable ...";

StopInfo = "The algorithm was stopped:\n some functionalities may not be available now";
StopInfoHd = "Information";

InputNotFound = "At least one of the inputs cannot be found, so that it has been temporarily removed from the DOE definition.\nIn the DOE panel, click OK to remove the missing input(s), or CANCEL to keep it/them.\nNote that the problem may come from the fact that the \n-cache system is enabled on a model which was created before the Selective Loading integration and/or on a model which points publication(s) belonging to a document that is not loaded (load the impacted document to retrieve your input).\n- The referenced input was deleted.";
InputNotFoundHd = "DOE input(s) not found";
OutputNotFound = "At least one of the outputs cannot be found, so that it has been temporarily removed from the DOE definition.\nIn the DOE panel, click OK to remove the missing output(s), or CANCEL to keep it/them.\nnNote that the problem may come from the fact that the \n-cache system is enabled on a model which was been created before the Selective Loading integration and/or on a model which points publication(s) belonging to a document that is not loaded (load the impacted document to retrieve your output).\n- The referenced output was deleted.";
OutputNotFoundHd = "DOE output(s) not found";

IncompatibleInputSelection = "You have selected a couple of types (a publication and its published parameter). Since a publication and its published parameter values are always equal, DOE will not be able to make them vary independently. You cannot select both at the same time.\For each couple, only the publication is kept, the published parameter has been removed from the set of selected parameters.";
IncompatibleInputSelectionHd = "Incompatible Input Selection";

CanNotGenerateExcelCurves = "The curves cannot be generated in Excel with your language environment (Japanese ...).\nYou will get the CATIA curves in the Results tab.\nRefer to the CATIA documentation for more details.";
CanNotGenerateExcelCurvesHd = "Environment language incompatible";


TabContainer.DoeSettingsTabPage.InfosFrame.PEOLeanUpdateSlider.Title = "Update Mode";
TabContainer.DoeSettingsTabPage.InfosFrame.PEOLeanUpdateSlider.ShortHelp = "Activates several types of update mechanisms.\nIn CATPart:\n 1- Global update (slow, update of the full CATPart). \n 2- Mixed Variational Update. \n 3- Local Update (fast, update of the optimization outputs only). \n\n In CATProduct and CATAnalysis: \n  - Global update.\n";
TabContainer.DoeSettingsTabPage.InfosFrame.PEOLeanUpdateSlider.Help = "Activates several types of update mechanisms for memory and performance tunning.";
TabContainer.DoeSettingsTabPage.InfosFrame.PEOLeanUpdateSlider.LongHelp = "PEO Lean Update options: Activates several types of update mechanisms.\n \nIn CATPart:\n 1- Global update (slow, update of the full CATPart). \n 2- Mixed Variational Update. \n 3- Local Update (fast, update of the optimization outputs only). \n\n In CATProduct and CATAnalysis: \n - Global update. \n";

TabContainer.DoeSettingsTabPage.InfosFrame.PEOLeanUpdateLabel.Title = "Update Mode";
TabContainer.DoeSettingsTabPage.InfosFrame.PEOLeanUpdateLabel.ShortHelp = "Activates several types of update mechanisms.\n\nIn CATPart:\n 1- Global update (slow). \n 2- Mixed Variational Update. \n 3- Local Update (fast). \n\n In CATProduct and CATAnalysis: \n  - Global update. \n";
TabContainer.DoeSettingsTabPage.InfosFrame.PEOLeanUpdateLabel.Help = "Activates several types of update mechanisms for memory and performance tunning.";
TabContainer.DoeSettingsTabPage.InfosFrame.PEOLeanUpdateLabel.LongHelp = "PEO Lean Update options: Activates several types of update mechanisms.\n \nIn CATPart:\n\t  1- Global update (slow, update of the full CATPart). \n 2- Mixed Variational Update. \n 3- Local Update (fast). \n\n In CATProduct and CATAnalysis: \n - Global update (update of the \n optimization outputs and of the CATProduct or the CATPart.)\n";
