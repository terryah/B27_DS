//---------------------------------------
// Resource file for CATGSMUIBlendPanel class
// En_US
//---------------------------------------

Title="Blend Definition";

LimitFrame.StartLimFrame.StartCurveLabel.Title = "First curve: ";
LimitFrame.StartLimFrame.StartCurveLabel.LongHelp = "Specifies the first curve of the blend.";

LimitFrame.StartLimFrame.StartSupportLabel.Title = "First support: ";
LimitFrame.StartLimFrame.StartSupportLabel.LongHelp = "Specifies the first support of the blend.
The first curve must be layed on this support.";

LimitFrame.EndLimFrame.EndCurveLabel.Title = "Second curve: ";
LimitFrame.EndLimFrame.EndCurveLabel.LongHelp = "Specifies the second curve of the blend.";

LimitFrame.EndLimFrame.EndSupportLabel.Title = "Second support: ";
LimitFrame.EndLimFrame.EndSupportLabel.LongHelp = "Specifies the second support of the blend.
The second curve must be layed on this support.";

ParameterFrame.ParamTabContainer.BasicPage.Title = "Basic";
ParameterFrame.ParamTabContainer.BasicPage.LongHelp = "Specifies continuity, trim support for 
the two limits of the blend.";

ParameterFrame.ParamTabContainer.BasicPage.BasicFrame.ContFrame.StartContFrame.StartContLabel.Title = "First continuity: ";
ParameterFrame.ParamTabContainer.BasicPage.BasicFrame.ContFrame.StartContFrame.StartContLabel.LongHelp = 
"Specifies the continuity between the blend
and the first support.
Continuity can be point, tangent or curvature.";

ParameterFrame.ParamTabContainer.BasicPage.BasicFrame.ContFrame.StartTrimButton.Title = "Trim first support";
ParameterFrame.ParamTabContainer.BasicPage.BasicFrame.ContFrame.StartTrimButton.LongHelp = 
"Specifies if the first support is trimmed 
by the first curve and joined to the blend.";

ParameterFrame.ParamTabContainer.BasicPage.BasicFrame.ContFrame.StartBorderFrame.StartBorderLabel.Title = "First tangent borders: ";
ParameterFrame.ParamTabContainer.BasicPage.BasicFrame.ContFrame.StartBorderFrame.LongHelp = 
"Specifies if the blend borders are tangent
to the first support:
   - Both extremities: borders at both first curve extremities are tangent,
   - none: borders are not constraint in tangency,
   - Start extremity only: the border at start extremity of the first curve is tangent,
   - End extremity only: the border at end extremity of the first curve is tangent.";

BothExtreTitle="Both extremities";
NoneExtreTitle="None";
StartExtreTitle="Start extremity only";
EndExtreTitle="End extremity only";
FirstOrExtreTitle="Free First Curve Origin";
SecondOrExtreTitle="Free Second Curve Origin";
FirstEndExtreTitle="Free First Curve End";
SecondEndExtreTitle="Free Second Curve End";
BothExtreConnectTitle="Connect Both Extremities";

ParameterFrame.ParamTabContainer.BasicPage.BasicFrame.TrimFrame.EndContFrame.EndContLabel.Title = "Second continuity: ";
ParameterFrame.ParamTabContainer.BasicPage.BasicFrame.TrimFrame.EndContFrame.EndContLabel.LongHelp = 
"Specifies the continuity between the blend
and the second support.
Continuity can be point, tangent or curvature.";

ParameterFrame.ParamTabContainer.BasicPage.BasicFrame.TrimFrame.EndTrimButton.Title = "Trim second support";
ParameterFrame.ParamTabContainer.BasicPage.BasicFrame.TrimFrame.EndTrimButton.LongHelp = 
"Specifies if the second support is trimmed 
by the second curve and joined to the blend.";

ParameterFrame.ParamTabContainer.BasicPage.BasicFrame.TrimFrame.EndBorderFrame.EndBorderLabel.Title = "Second tangent borders: ";
ParameterFrame.ParamTabContainer.BasicPage.BasicFrame.TrimFrame.EndBorderFrame.LongHelp = 
"Specifies if the blend borders are tangent
to the second support: 
   - Both extremities: borders at both first curve extremities are tangent,
   - none: borders are not constraint in tangency,
   - Start extremity only: the border at start extremity of the first curve is tangent,
   - End extremity only: the border at end extremity of the first curve is tangent.";

ParameterFrame.ParamTabContainer.PointPage.Title = "Closing Points";
ParameterFrame.ParamTabContainer.PointPage.LongHelp = "Specifies closing point for each closed curve of the blend.";

ParameterFrame.ParamTabContainer.PointPage.PointFrame.StartPointFrame.StartPointLabel.Title = "First closing point: ";
ParameterFrame.ParamTabContainer.PointPage.PointFrame.StartPointFrame.StartPointLabel.LongHelp = 
"Specifies the closing point of the first curve.
The first curve must be closed.";

ParameterFrame.ParamTabContainer.PointPage.PointFrame.EndPointFrame.EndPointLabel.Title = "Second closing point: ";
ParameterFrame.ParamTabContainer.PointPage.PointFrame.EndPointFrame.EndPointLabel.LongHelp =
"Specifies the closing point of the second curve.
The second curve must be closed.";

ParameterFrame.ParamTabContainer.CouplingPage.Title = "Coupling / Spine";
ParameterFrame.ParamTabContainer.CouplingPageWithoutSpine.Title = "Coupling";
ParameterFrame.ParamTabContainer.CouplingPage.LongHelp = "Specifies how the curves are coupled:
- ratio: the curves are coupled accordind to the curvilinear abscissa ratio.
- Tangency: if each curve has the same number of tangency 
discontinuities points, then these points are coupled,
otherwise an error message is displayed.
- Tangency then curvature: if each curve has the same number 
of tangency and curvature discontinuities points,
then tangency discontinuities points are coupled,
and after curvature discontinuities points are coupled,
otherwise an error message is displayed.
- Vertices: if each curve has the same number of vertices, 
then these points are coupled,
otherwise an error message is displayed,
- spine: the coupling is driven by the spine curve.";

ParameterFrame.ParamTabContainer.TensionPage.Title = "Tension";
ParameterFrame.ParamTabContainer.TensionPage.LongHelp = 
"Specifies the tension on each support of the blend.";

ParameterFrame.ParamTabContainer.TensionPage.TensionFrame.StartTensionFrame.StartTensionButtonFrame.StartTension.Title = "First tension: ";
ParameterFrame.ParamTabContainer.TensionPage.TensionFrame.StartTensionFrame.StartTensionButtonFrame.StartTension.LongHelp =
"Specifies the tension on the first support of the blend.";

ParameterFrame.ParamTabContainer.TensionPage.TensionFrame.EndTensionFrame.EndTensionButtonFrame.EndTension.Title = "Second tension: ";
ParameterFrame.ParamTabContainer.TensionPage.TensionFrame.EndTensionFrame.EndTensionButtonFrame.EndTension.LongHelp =
"Specifies the tension on the second support of the blend.";

ParameterFrame.ParamTabContainer.TensionPage.TensionFrame.StartTensionFrame.StartTensionButtonFrame.StartDefault.Title = "Default";
ParameterFrame.ParamTabContainer.TensionPage.TensionFrame.StartTensionFrame.StartTensionButtonFrame.StartDefault.LongHelp = 
"Specifies that the tension on the first support 
of the blend is the default one.";

ParameterFrame.ParamTabContainer.TensionPage.TensionFrame.EndTensionFrame.EndTensionButtonFrame.EndDefault.Title = "Default";
ParameterFrame.ParamTabContainer.TensionPage.TensionFrame.EndTensionFrame.EndTensionButtonFrame.EndDefault.LongHelp = 
"Specifies that the tension on the second support 
of the blend is the default one.";

ActionFrame.ReplaceButton.Title = "Replace";
ActionFrame.RemoveButton.Title = "Remove";
ActionFrame.ReverseButton.Title = "Reverse";


PointContTitle="Point";
TangencyContTitle="Tangency";
CurvatureContTitle="Curvature";

ParameterFrame.ParamTabContainer.TensionPage.TensionFrame.StartTensionFrame.StartTensionButtonFrame.StartTensionComb.LongHelp = 
"Specifies that the tension on the first support 
of the blend is constant or linear.";

ParameterFrame.ParamTabContainer.TensionPage.TensionFrame.EndTensionFrame.EndTensionButtonFrame.EndTensionComb.LongHelp = 
"Specifies that the tension on the second support 
of the blend is constant or linear.";

ConstantTensionTitle="Constant";
LinearTensionTitle="Linear";
STypeTensionTitle="S type";

RatioCouplTitle="Ratio";
TangencyCouplTitle="Tangency";
CurvatureCouplTitle="Tangency then curvature";
VertexCouplTitleKey="Vertices";
SpineCouplTitleKey="Spine";
AvoidTwistsCouplTitleKey="Avoid Twists";

ParameterFrame.ParamTabContainer.TensionPage.TensionFrame.StartTensionFrame.StartValueFrame.StartParmFrame1.LongHelp = 
"Specifies the first tension value 
on the first support of the blend,
if tension type is constant or linear.";

ParameterFrame.ParamTabContainer.TensionPage.TensionFrame.StartTensionFrame.StartValueFrame.EndParmFrame1.LongHelp = 
"Specifies the second tension value 
on the first support of the blend,
if tension type is linear.";

ParameterFrame.ParamTabContainer.TensionPage.TensionFrame.EndTensionFrame.EndValueFrame.StartParmFrame2.LongHelp = 
"Specifies the first tension value 
on the second support of the blend,
if tension type is constant or linear.";

ParameterFrame.ParamTabContainer.TensionPage.TensionFrame.EndTensionFrame.EndValueFrame.EndParmFrame2.LongHelp = 
"Specifies the second tension value 
on the second support of the blend,
if tension type is linear.";

StartExtremityKey="T1: ";
EndExtremityKey="T2:  ";

ChaineDefaut="Default";
ParameterFrame.ParamTabContainer.CouplingPage.SpineFrame.SpineLabel.Title="Spine: ";
ParameterFrame.ParamTabContainer.CouplingPage.SpineFrame.SpineLabel.LongHelp="Specifies the spine curve taken into account\nwhen coupling mode is set to 'Spine'.";
ParameterFrame.ParamTabContainer.CouplingPage.SpineFrame.SpineSelector.LongHelp="Specifies the spine curve taken into account\nwhen coupling mode is set to 'Spine'.";

// Tolerant blend
//------------------
FrameSmooth.Title="Smooth parameters";

FrameSmooth.SmoothDevButton.Title="Deviation: ";
FrameSmooth.SmoothDevButton.LongHelp="Activate/Desactivate the deviation during the operation.";
FrameSmooth.FrameLayoutDev.FraLittSmoothDev.EnglobingFrame.IntermediateFrame.Spinner.Title="Deviation";

FrameSmooth.SmoothAngleButton.Title="Angular correction: ";
FrameSmooth.SmoothAngleButton.LongHelp="Activate/Desactivate the angular threshold during the operation.";
FrameSmooth.FrameLayoutAngle.FraLittSmoothAngle.EnglobingFrame.IntermediateFrame.Spinner.Title="Angular correction";

// Developable
ParameterFrame.ParamTabContainer.RuledDevPage.Title = "Developable";
ParameterFrame.ParamTabContainer.RuledDevPage.LongHelp = "Generate a ruled developable surface with or without Surface Boundary Isopar Connections.";

ParameterFrame.ParamTabContainer.RuledDevPage.RuledDevFrame.RuledDevButton.Title = "Create a ruled developable surface";
ParameterFrame.ParamTabContainer.RuledDevPage.RuledDevFrame.RuledDevButton.LongHelp = 
"Force the operator to generate a ruled developable surface.";

//ParameterFrame.ParamTabContainer.RuledDevPage.RuledDevFrame.IsoConnectionFrame.Title = "Surface Boundary Isopar Connections";
ParameterFrame.ParamTabContainer.RuledDevPage.RuledDevFrame.IsoConnectionFrame.LabelSepar.Title="Surface Boundary Isopar Connections";

ParameterFrame.ParamTabContainer.RuledDevPage.RuledDevFrame.IsoConnectionFrame.StartFrame.StartLabel.Title = "Start: ";
ParameterFrame.ParamTabContainer.RuledDevPage.RuledDevFrame.IsoConnectionFrame.StartFrame.LongHelp=
"Choose a condition of connection at start of generated surface";

ParameterFrame.ParamTabContainer.RuledDevPage.RuledDevFrame.IsoConnectionFrame.EndFrame.EndLabel.Title = "End:   ";
ParameterFrame.ParamTabContainer.RuledDevPage.RuledDevFrame.IsoConnectionFrame.EndFrame.EndLabel.LongHelp=
"Choose a condition of connection at end of generated surface";
