//=============================================================================
//                                     CNEXT - CXR1
//                          COPYRIGHT DASSAULT SYSTEMES 1997 
//-----------------------------------------------------------------------------
// FILENAME    :    CATDrwDimFrameValueFormat
// LOCATION    :    DraftingIntUI/CNext/resources/msgcatalog
// AUTHOR      :    jmt
// DATE        :    Nov. 03 1997
//------------------------------------------------------------------------------
// DESCRIPTION :    Resources File for NLS purpose related to Drafting WorkShop
//                  
//------------------------------------------------------------------------------
// COMMENTS    :
//------------------------------------------------------------------------------
//MODIFICATIONS     user  date      purpose
//   HISTORY        ----  ----      -------
//     01			fgx	  18.02.99	Ajout des unites d'angle MINUTE et SECOND
//     02			fgx	  08.03.99	Ajout du mode driving
//     03			fgx	  18.11.99	Dim type
//     04			LGK     25.06.02  Revision
//------------------------------------------------------------------------------

_titleDimType.Title="Dimension Type ";

ONE_PROJECTED_DIM = "Projected dimension";
SEV_PROJECTED_DIM = "Projected dimensions";
ONE_TRUE_DIM = "True dimension";
SEV_TRUE_DIM = "True dimensions";
UNDEFINED = "Undefined";

_frameDimType._checkDriving.Title="Driving";
_frameDimType._checkDriving.Help="Sets the driven/driving mode for dimension";
_frameDimType._checkDriving.LongHelp="Driving
Sets the driven/driving mode 
for dimension.";

_frameDimType._editorDriving.ShortHelp="Value";
_frameDimType._editorDriving.Help="Edits the dimension value when driving geometry";
_frameDimType._editorDriving.LongHelp="Value
Edits the dimension value 
when driving geometry.";

FrameBase.FrameLayoutTitle.LabelLayoutTitle.Title="Value Orientation ";
FrameBase.FrameDualValueTitle.LabelDualValueTitle.Title="Dual Value ";
FrameFormatTitle.LabelFormatTitle.Title="Format ";
FrameFakeTitle.CheckButtonFakeTitle.Title="Fake Dimension ";

FrameBase.FrameLayout.LabelReference.Title="Reference: ";
FrameBase.FrameLayout.LabelOrientation.Title="Orientation: ";
FrameBase.FrameLayout.LabelAngle.Title="Angle: ";
FrameBase.FrameLayout.LabelInOutValue.Title="Position: ";
FrameBase.FrameLayout.LabelOffset.Title="Offset: ";
FrameFormat.LabelValue.Title="Main value";
FrameFormat.LabelDualValue.Title="Dual value";

FrameFormat._labelUnitOrder.Title="Display: ";
FrameFormat._labelDescription.Title="Description: ";
FrameFormat._frameMoreInfoMain._pushMoreInfoMain.ShortHelp="Browse main description";
FrameFormat._frameMoreInfoDual._pushMoreInfoDual.ShortHelp="Browse dual description";

FrameFormat.LabelPrecision.Title="Precision: ";
FrameFormat.LabelFormat.Title="Format: ";
FrameFormat.LabelValueFrac.Title    ="   1 / ";
FrameFormat.LabelDualValueFrac.Title="   1 / ";
FrameFake.LabelFakeMain.Title="Main Value: ";
FrameFake.LabelFakeDual.Title="Dual Value: ";
FrameFake.RadioButtonFakeNum.Title="Numerical";
FrameFake.RadioButtonFakeAlpha.Title="Alphanumerical";

FrameBase.FrameDualValue.CheckButtonDualValue.Title="Show dual value ";

ComboReferenceLine0="Screen";
ComboReferenceLine1="View";
ComboReferenceLine2="Dimension Line";
ComboReferenceLine3="Extension Line";

ComboInOutValueAUTO="Auto";
ComboInOutValueIN="Inside";
ComboInOutValueOUT="Outside";

ComboDualValueLine0="Below";
ComboDualValueLine1="Fractional";
ComboDualValueLine2="Side-by-side";

ComboValueFormatLine0="Decimal";
ComboValueFormatLine1="Fractional";

ComboDualValueFormatLine0="Decimal";
ComboDualValueFormatLine1="Fractional";

ComboOrientationLine0="Parallel";
ComboOrientationLine1="Perpendicular";
ComboOrientationLine2="Fixed Angle";

ComboOrientationLine00="Horizontal";
ComboOrientationLine01="Vertical";
ComboOrientationLine02="Fixed Angle";

ONE_FACTOR="1 factor";
TWO_FACTORS="2 factors";
THREE_FACTORS="3 factors";

DefaultFakeText="*Fake*";

FrameBase.FrameLayout.ComboReference.LongHelp=
"Provides a reference
for the value positionning.";
FrameBase.FrameLayout.ComboOrientation.LongHelp=
"Defines the orientation
of the value relatively to its reference.";
FrameBase.FrameLayout.SpinnerAngle.LongHelp=
"Defines the angle
of the value relatively to its reference.";
FrameBase.FrameLayout.SpinnerOffset.LongHelp=
"Defines the vertical offset
of the value from dimension line.";
FrameBase.FrameDualValue.CheckButtonDualValue.LongHelp=
"Show dual value
Displays the dual value if checked.";
FrameBase.FrameDualValue.ComboDualValue.LongHelp=
"Selects the display mode of the dual value.";

FrameFormat._labelUnitOrder.LongHelp=
"Sets main and dual number of unit factors to display.";
FrameFormat._labelDescription.LongHelp=
"Sets main and dual description of numerical display.";

FrameFormat.LabelPrecision.LongHelp=
"Sets main and dual value precision.";
FrameFormat.LabelFormat.LongHelp=
"Sets main and dual value format.";

FrameFormat._comboUnitOrder.LongHelp=
"Sets the number of unit factors to display for main value.";
FrameFormat._comboDescriptionMain.LongHelp=
"Sets the main value description of numerical display.";
FrameFormat._frameMoreInfoMain._pushMoreInfoMain.LongHelp=
"Displays a dialog box to browse or to change
main description of numerical display.";
FrameFormat.ComboValuePrecision.LongHelp=
"Sets the main value precision.";
FrameFormat.ComboValueFormat.LongHelp=
"Sets the main value format.";

FrameFormat._comboDualUnitOrder.LongHelp=
"Sets the number of unit factors to display for dual value.";
FrameFormat._comboDescriptionDual.LongHelp=
"Sets the dual value description of numerical display.";
FrameFormat._frameMoreInfoDual._pushMoreInfoDual.LongHelp=
"Displays a dialog box to browse or to change
dual description of numerical display.";
FrameFormat.ComboDualValuePrecision.LongHelp=
"Sets the dual value precision.";
FrameFormat.ComboDualValueFormat.LongHelp=
"Sets the dual value format.";

FrameFakeTitle.CheckButtonFakeTitle.LongHelp=
"Change the dimension into a fake one.";

FrameFake.EditorFakeNumMain.LongHelp=
"Displays the main numerical fake value.";
FrameFake.EditorFakeAlphaMain.LongHelp=
"Displays the main alphanumerical fake value.";
FrameFake.EditorFakeNumDual.LongHelp=
"Displays the dual numerical fake value.";
FrameFake.EditorFakeAlphaDual.LongHelp=
"Displays the dual alphanumerical fake value.";

SeeChamfer="See Chamfer";

