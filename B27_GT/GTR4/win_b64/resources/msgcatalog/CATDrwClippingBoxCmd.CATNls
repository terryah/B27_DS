//=============================================================================
//                          COPYRIGHT Dassault Systemes 2005
//-----------------------------------------------------------------------------
// FILENAME    :    CATDrwClippingBoxCmd
// LOCATION    :    DraftingGenUI/CNext/resources/msgcatalog/
// AUTHOR      :    wsa
// DATE        :    May 2005
//------------------------------------------------------------------------------
// DESCRIPTION :    Resources File for NLS purpose related to CATDrwClippingBoxCmd
//                  
//------------------------------------------------------------------------------
// COMMENTS    :	 ENGLISH VERSION
//------------------------------------------------------------------------------
//MODIFICATIONS     user  date        purpose
//   HISTORY        ----  ----        -------
//   01.            apr3  19.04.2016  Revision
//------------------------------------------------------------------------------

CATDrwClippingBoxCmd.initialState.Message="Click anywhere in the viewer to exit";
CATDrwClippingBoxCmd.Title="Clipping Object";
CATDrwClippingBoxCmd.ModeFrame="Clipping Mode";
CATDrwClippingBoxCmd.Mode1="Clipping Box";
CATDrwClippingBoxCmd.Mode2="Clipping By Slice";
CATDrwClippingBoxCmd.Mode3="Back Clipping Plane";
CATDrwClippingBoxCmd.InputFrame="User Input";
CATDrwClippingBoxCmd.PointFrame.Title="Center Point";
CATDrwClippingBoxCmd.PointFrame.ShortHelp="Center point of the clipping object";
CATDrwClippingBoxCmd.PointFrame.Help="Center point of the clipping object";
CATDrwClippingBoxCmd.PointFrame.LongHelp="Center point of the clipping object";
CATDrwClippingBoxCmd.DimensionFrame.Title="Dimension";
CATDrwClippingBoxCmd.DimensionFrame.ShortHelp="Dimension of the clipping object. Note that minimum possible dimension(related to Drawing tolerance) is 1 mm.";
CATDrwClippingBoxCmd.DimensionFrame.Help="Dimension of the clipping object. Note that minimum possible dimension(related to Drawing tolerance) is 1 mm.";
CATDrwClippingBoxCmd.DimensionFrame.LongHelp="Dimension of the clipping object. Note that minimum possible dimension(related to Drawing tolerance) is 1 mm.";
CATDrwClippingBoxCmd.Label1=" X ";
CATDrwClippingBoxCmd.Label2=" Y ";
CATDrwClippingBoxCmd.Label3=" Z ";
CATDrwClippingBoxCmd.Label4=" Length ";
CATDrwClippingBoxCmd.Label5=" Width ";
CATDrwClippingBoxCmd.Label6=" Height ";
CATDrwClippingBoxCmd.CreateModifyPushButton.Title="      Create      ";
CATDrwClippingBoxCmd.CreateModifyPushButton.ShortHelp="Creates a clipping object";
CATDrwClippingBoxCmd.CreateModifyPushButton.Help="Creates a clipping object on the view";
CATDrwClippingBoxCmd.CreateModifyPushButton.LongHelp="Creates a clipping object on the view and updates it. Also closes the panel.";
CATDrwClippingBoxCmd.CommandMode2.Title="      Modify      ";
CATDrwClippingBoxCmd.OKPushButton.CommandMode2.ShortHelp="Modifies a clipping object";
CATDrwClippingBoxCmd.OKPushButton.CommandMode2.Help="Modifies a clipping object on the view";
CATDrwClippingBoxCmd.OKPushButton.CommandMode2.LongHelp="Modifies a clipping object on the view and updates it. Also closes the panel.";
CATDrwClippingBoxCmd.CancelPushButton.Title="      Cancel      ";
CATDrwClippingBoxCmd.CancelPushButton.ShortHelp="Cancels the command";
CATDrwClippingBoxCmd.CancelPushButton.Help="Cancels the command and closes the panel.";
CATDrwClippingBoxCmd.CancelPushButton.LongHelp="Cancels the command and closes the panel. Does not create or modify clipping object.";
CATDrwClippingBoxCmd.ApplyPushButton.Title="      Preview      ";
CATDrwClippingBoxCmd.ApplyPushButton.ShortHelp="Applies the clipping object";
CATDrwClippingBoxCmd.ApplyPushButton.Help="Applies the clipping object and updates the view.";
CATDrwClippingBoxCmd.ApplyPushButton.LongHelp="Applies the clipping object and updates the view. Does not close panel.";
CATDrwClippingBoxCmd.ModeComboBox.ShortHelp="Mode Selection";
CATDrwClippingBoxCmd.ModeComboBox.Help="Lets one select one of the three modes for clipping object.";
CATDrwClippingBoxCmd.ModeComboBox.LongHelp="Lets one select one of the three modes for clipping object.";
CATDrwClippingBoxCmd.PointSpinner1.ShortHelp="X coordinate of the center point";
CATDrwClippingBoxCmd.PointSpinner1.Help="X coordinate of the center point of clipping object.";
CATDrwClippingBoxCmd.PointSpinner1.LongHelp="X coordinate of the center point of clipping object.";
CATDrwClippingBoxCmd.PointSpinner2.ShortHelp="Y coordinate of the center point";
CATDrwClippingBoxCmd.PointSpinner2.Help="Y coordinate of the center point of clipping object.";
CATDrwClippingBoxCmd.PointSpinner2.LongHelp="Y coordinate of the center point of clipping object.";
CATDrwClippingBoxCmd.PointSpinner3.ShortHelp="Z coordinate of the center point";
CATDrwClippingBoxCmd.PointSpinner3.Help="Z coordinate of the center point of clipping object.";
CATDrwClippingBoxCmd.PointSpinner3.LongHelp="Z coordinate of the center point of clipping object.";
CATDrwClippingBoxCmd.DimensionSpinner1.ShortHelp="Length of the 3D Clipping. For ClippingBySlice mode, it is the distance between back and front clipping planes.";
CATDrwClippingBoxCmd.DimensionSpinner1.Help="Length of the 3D Clipping. For ClippingBySlice mode, it is the distance between back and front clipping planes.";
CATDrwClippingBoxCmd.DimensionSpinner1.LongHelp="Length of the 3D Clipping. For ClippingBySlice mode, it is the distance between back and front clipping planes.";
CATDrwClippingBoxCmd.DimensionSpinner2.ShortHelp="Width of the 3D Clipping";
CATDrwClippingBoxCmd.DimensionSpinner2.Help="Width of the 3D Clipping.";
CATDrwClippingBoxCmd.DimensionSpinner2.LongHelp="Width of the 3D Clipping.";
CATDrwClippingBoxCmd.DimensionSpinner3.ShortHelp="Height of the 3D Clipping";
CATDrwClippingBoxCmd.DimensionSpinner3.Help="Height of the 3D Clipping.";
CATDrwClippingBoxCmd.DimensionSpinner3.LongHelp="Height of the 3D Clipping.";
CATDrwClippingBoxCmd.3DViewer.ShortHelp="3D Part or Product with the clipping object";
CATDrwClippingBoxCmd.3DViewer.Help="3D Part or Product with the clipping object. One defines clipping object on 3D Viewer.";
CATDrwClippingBoxCmd.3DViewer.LongHelp="3D Part or Product with the clipping object. One defines clipping object on 3D Viewer. Blue arrow signifies the view direction.";
ProgressTaskUI.Title="Clipping Box";
CATDrwClippingBoxCmd.InitBox.Message = "Manipulate the Clipping Object.";
CATDrwClippingBoxCmd.ModifiedClippingObject.Message = "Manipulate the Clipping Object.";
CATDrwClippingBoxCmd.ResetManipButton="Clear Current Manipulator";
CATDrwClippingBoxCmd.UndoTitle="Add or Modify a 3D Clipping";

