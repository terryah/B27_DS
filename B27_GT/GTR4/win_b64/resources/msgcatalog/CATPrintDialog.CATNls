//-------------------------------------------------------------------------
// Resource file for CATPrintDialog
// fbq - Oct 97
//-------------------------------------------------------------------------

Title = "Print";
TabContainer.Layout.Title = "Layout";
TabContainer.Selection.Title = "Multi-Documents";

TabContainer.Layout.PositionAndSize.Title = "Position and Size";

TabContainer.Layout.LongHelp = "Specifies how the page is oriented with respect to the image
and how the image is positioned on the page, and the image size.
The current paper format is retrieved.";

TabContainer.Selection.LongHelp = "Selects the images to be printed.";

General.PrintArea.Title = "Print Area";
General.PrintArea.LongHelp = "Specifies the area to print.
'Whole Document' prints the whole image.
'Display' prints the image seen on the screen.
'Selection' prints the area selected with the button.
If a print area is defined on a Drafting document, the 'Document Area' option is proposed.
'Document Area' prints the print area defined on the sheet.";
General.PrintArea.Area.Title       = "Select Mode";
General.PrintArea.Area.ShortHelp   = "Select Mode";
General.PrintArea.Area.Help        = "Selects an area in the current viewer";
General.PrintArea.Area.LongHelp    = "Selects an area in the current viewer";

General.Tiling.Title = "Tiling";
General.Tiling.ShortHelp = "This option allows to print the image on several papers.";
General.Tiling.Help = "If this option is selected, the image will be cut and printed on several papers.";
General.Tiling.LongHelp = "If this option is selected, the image will be cut and printed on several papers.";

General.TilingDialog.Title = "Define... ";
General.TilingDialog.ShortHelp = "Sets up tiling options, such as the tile count, the tile size or the tile position.";
General.TilingDialog.Help = "Sets up tiling options, such as the tile count, the tile size or the tile position.";
General.TilingDialog.LongHelp = "Sets up tiling options, such as the tile count, the tile size or the tile position.";

General.Buttons.PageSetup.Title = "Page Setup...";
General.Buttons.PageSetup.LongHelp = "Page Setup.
Sets up the paper format and the margins.";

General.Buttons.Options.Title = "Options...";
General.Buttons.Options.LongHelp = "Options
Sets up printing options, such as printing in color,
in greyscale or monochrome, adding a banner, or setting
the quality factor.";

General.Buttons.Preview.Title = "Preview...";
General.Buttons.Preview.LongHelp = "Preview.
Shows how the image will look
with the selected options.";

General.CopiesFrame.Copies.Title = "Copies:";
General.CopiesFrame.LongHelp = "Specifies the number of copies to print.";

CannotPrintToFile.Title = "Print to File";
PreviewFrame.LongHelp ="
Shows and interactively modifies the image position and size.
'Origin' makes the image lower left corner and the left and
bottom margins intersection coincide, that is with 'Left' and
'Bottom' set to 0.
'Center' makes the image center and the paper center coincide.";

RangeFailed.PText = "Invalid Print Range.";
CannotPrintToFile.PText = "Cannot write to file /p1";
ReplaceFile.PText = "/p1 already exists.\nDo you want to replace it ?";
PrintFailed.Title = "Print";
NoPrinter.Title = "Warning";
NoPrinter.Text = "No printer is defined, printing is \ntherefore not allowed. Please use \nPrinter Setup to add printers.";
MarginsError.Title = "Error";
MarginsError.Text1 = "Margins cannot be negative.";
MarginsError.Text2 = "Horizontal margins exceed form width.";
MarginsError.Text3 = "Vertical margins exceed form height.";

FormSizeWarning.Title = "Warning";
FormSizeWarning.PText = "Form should not exceed maximum dimensions\nfor this printer (/p1).";

UseImageFormatWarning.Title = "Use Image Format Warning";
UseImageFormatWarning.PText = "\"Use Image Format\" option is not longer available since the printer has been changed.";

PrintToOneFileWarning.Title = "Warning";
PrintToOneFileWarning.PText = "The current driver does not allow the print of several images in a single file.";

MemoryWarning = "Not enough memory : print preview may be blank or some elements maybe missing";

DpiMax = "Memory allocation could failed because of DPI = /p1 too high. If you choose Retry button the max DPI = /p2 will be used instead but the result print raster will be different from expected one";

SmallDPIError = "Cannot print : the image dimensions are too large. \nThe following actions may solve this problem : \n   - decreasing the scale\n   - choosing \"Fit in page\" and a reasonable paper size\n   - unchecking \"Use image format\" option\n   - choosing a custom quality";

HighResolutionError = "Cannot print : the image dimensions are too large. \nThe following actions may solve this problem : \n   - decreasing the scale\n   - choosing \"Fit in page\" and a reasonable paper size\n   - unchecking \"Use image format\" option";

RenderingLimitationXScale = "Cannot print the document with those parameters. \nIn Extreme Scale mode, printing is only allowed in rasterization rendering mode.";

CannotPrintImage = "Cannot print image.";
