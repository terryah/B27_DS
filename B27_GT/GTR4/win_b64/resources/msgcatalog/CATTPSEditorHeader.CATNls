//-----------------------------------------------------------------------------
// Datum Creation Command
//-----------------------------------------------------------------------------
CATTPSEditorHeader.TPSCreateDatum.Title="Datum Feature";
CATTPSEditorHeader.TPSCreateDatum.ShortHelp="Datum Feature";
CATTPSEditorHeader.TPSCreateDatum.Help="Creates a datum feature on a geometric element";
CATTPSEditorHeader.TPSCreateDatum.LongHelp = "Datum Feature\nCreates a datum feature on a geometric element.";

//-----------------------------------------------------------------------------
// Datum Target Creation Command
//-----------------------------------------------------------------------------
CATTPSEditorHeader.TPSCreateTarget.Title="Datum Target";
CATTPSEditorHeader.TPSCreateTarget.ShortHelp="Datum Target";
CATTPSEditorHeader.TPSCreateTarget.Help="Creates a datum target";
CATTPSEditorHeader.TPSCreateTarget.LongHelp = "Datum Target\nCreates a datum target from a geometric element, already specified as datum element.";

//-----------------------------------------------------------------------------
// Reference Frame Creation Command
//-----------------------------------------------------------------------------
CATTPSEditorHeader.CATTPSRefFrameHdr.Title="Reference Frame";
CATTPSEditorHeader.CATTPSRefFrameHdr.ShortHelp="Reference Frame";
CATTPSEditorHeader.CATTPSRefFrameHdr.Help="Creates a Reference Frame";
CATTPSEditorHeader.CATTPSRefFrameHdr.LongHelp = "Reference Frame\nCreates Reference Frame.";

//-----------------------------------------------------------------------------
// GDT Creation Command
//-----------------------------------------------------------------------------
CATTPSEditorHeader.TPSCreateNonSemanticGDT.Title="Geometrical Tolerance";
CATTPSEditorHeader.TPSCreateNonSemanticGDT.ShortHelp="Geometrical Tolerance";
CATTPSEditorHeader.TPSCreateNonSemanticGDT.Help="Creates a geometrical tolerance";
CATTPSEditorHeader.TPSCreateNonSemanticGDT.LongHelp = "Geometrical Tolerance\nCreates a geometrical tolerance.";

//-----------------------------------------------------------------------------
// Roughness Creation Command
//-----------------------------------------------------------------------------
CATTPSEditorHeader.TPSCreateRoughness.Title="Roughness";
CATTPSEditorHeader.TPSCreateRoughness.ShortHelp="Roughness";
CATTPSEditorHeader.TPSCreateRoughness.Help="Creates a roughness";
CATTPSEditorHeader.TPSCreateRoughness.LongHelp = "Roughness\nCreates a roughness.";

//-----------------------------------------------------------------------------
// Noa Creation Command
//-----------------------------------------------------------------------------
CATTPSEditorHeader.CATTPSCreateNoa.Title="Note Object Attribute";
CATTPSEditorHeader.CATTPSCreateNoa.ShortHelp="Note Object Attribute";
CATTPSEditorHeader.CATTPSCreateNoa.Help="Creates a Note Object Attribute";
CATTPSEditorHeader.CATTPSCreateNoa.LongHelp = "Note Object Attribute\nCreates a Note Object Attribute.";

//-----------------------------------------------------------------------------
// Report Command
//-----------------------------------------------------------------------------
CATTPSEditorHeader.TPSReport.Title="Report";
CATTPSEditorHeader.TPSReport.ShortHelp="Report";
CATTPSEditorHeader.TPSReport.Help="Checks tolerancing rules";
CATTPSEditorHeader.TPSReport.LongHelp = "Report\Checks tolerancing rules and creates a report.";

//-----------------------------------------------------------------------------
// Settings Report Command
//-----------------------------------------------------------------------------
CATTPSEditorHeader.TPSReportSettings.Title = "Report Customisation";
CATTPSEditorHeader.TPSReportSettings.ShortHelp = "Report Customisation";
CATTPSEditorHeader.TPSReportSettings.Help = "Customises the generated report";
CATTPSEditorHeader.TPSReportSettings.LongHelp = "Report Customisation\Customises the generated report.";

//-----------------------------------------------------------------------------
// Default Annotation Command
//-----------------------------------------------------------------------------
CATTPSEditorHeader.CATTPSDefAnnotHdr.Title="Apply separately several times";
CATTPSEditorHeader.CATTPSDefAnnotHdr.ShortHelp="Apply the annotation to several geometrical features individually";
CATTPSEditorHeader.CATTPSDefAnnotHdr.Help="Manage the geometrical features to which the annotation is applied separately several times (add/remove/query individual feature, get the validity status of each individual feature)";
CATTPSEditorHeader.CATTPSDefAnnotHdr.LongHelp="Using this function is equivalent to define several identical annotations each one being applied to one of the geometrical features";

//-----------------------------------------------------------------------------
// Capture
//-----------------------------------------------------------------------------
CATTPSEditorHeader.TPSCapture.Title="Capture";
CATTPSEditorHeader.TPSCapture.ShortHelp="Capture";
CATTPSEditorHeader.TPSCapture.Help="Creates a capture";
CATTPSEditorHeader.TPSCapture.LongHelp="Capture\nCreates a capture.";

//-----------------------------------------------------------------------------
// Replace Datum Reference Frame Command
//-----------------------------------------------------------------------------
CATTPSEditorHeader.TPSReplaceDRF.Title="Replace Datum Reference Frame";
CATTPSEditorHeader.TPSReplaceDRF.ShortHelp="Replace Datum Reference Frame";
CATTPSEditorHeader.TPSReplaceDRF.Help="Replace a datum reference frame";
//CATTPSEditorHeader.TPSReplaceDRF.LongHelp =
//"Replace Datum Reference Frame
//Replace a datum reference frame.";

//-----------------------------------------------------------------------------
// SnapGrid
//-----------------------------------------------------------------------------
CATTPSEditorHeader.TPSSnapGrid.Title="Snap to Point";
CATTPSEditorHeader.TPSSnapGrid.ShortHelp="Snap to Point";
CATTPSEditorHeader.TPSSnapGrid.Help="Snap the current annotation anchor point to a point of the 3D grid.";
CATTPSEditorHeader.TPSSnapGrid.LongHelp =
"Snap to Point 
Snap the current annotation anchor point 
to a point of the 3D grid.";

//-----------------------------------------------------------------------------
// DisplayGrid
//-----------------------------------------------------------------------------
CATTPSEditorHeader.TPSDisplayGrid.Title="Display 3D Grid";
CATTPSEditorHeader.TPSDisplayGrid.ShortHelp="Display 3D Grid";
CATTPSEditorHeader.TPSDisplayGrid.Help="Display or hide the 3D grid associated with the current annotation or annotation view.";
CATTPSEditorHeader.TPSDisplayGrid.LongHelp =
"Display 3D Grid 
Display or hide the 3D grid associated with 
the current annotation or annotation view.";

//-----------------------------------------------------------------------------
// Clipping Plane
//-----------------------------------------------------------------------------
CATTPSEditorHeader.TPSClippingPlane.Title="Clipping Plane";
CATTPSEditorHeader.TPSClippingPlane.ShortHelp= "Clipping Plane";
CATTPSEditorHeader.TPSClippingPlane.Help="Clipping Plane from the active view/annotation plane";
CATTPSEditorHeader.TPSClippingPlane.LongHelp = "Clipping Plane\nAdds or Removes a clipping plane from plane of the active view/annotation plane";

//-----------------------------------------------------------------------------
// Restricted Area Command
//-----------------------------------------------------------------------------
CATTPSEditorHeader.CATTPSRestrictedAreaHdr.Title="Restricted Area";
CATTPSEditorHeader.CATTPSRestrictedAreaHdr.ShortHelp="Restricted Area";
CATTPSEditorHeader.CATTPSRestrictedAreaHdr.Help="Create a restricted area";
CATTPSEditorHeader.CATTPSRestrictedAreaHdr.LongHelp="Create a restricted area";

//-----------------------------------------------------------------------------
// Generative dimension Command
//-----------------------------------------------------------------------------
CATTPSEditorHeader.TPSCreateGenDimHdr.Title="Generative Dimension";
CATTPSEditorHeader.TPSCreateGenDimHdr.ShortHelp="Generative Dimension";
CATTPSEditorHeader.TPSCreateGenDimHdr.Help="Create Generative Dimension(s)";
CATTPSEditorHeader.TPSCreateGenDimHdr.LongHelp="Create Generative Dimension(s)";

//-----------------------------------------------------------------------------
// Assembly Update Command
//-----------------------------------------------------------------------------
CATTPSEditorHeader.CATTPSUpdateHeaderAsm.Title="Update";
CATTPSEditorHeader.CATTPSUpdateHeaderAsm.ShortHelp="Update All";
CATTPSEditorHeader.CATTPSUpdateHeaderAsm.Help="Update All";
//CATTPSEditorHeader.CATTPSUpdateHeaderAsm.LongHelp="Updates constraints and shape representations.";

//-----------------------------------------------------------------------------
// Constructed Geometry Command
//-----------------------------------------------------------------------------
CATTPSEditorHeader.CATTPSConstructedGeometryHdr.Title="Constructed Geometry Management";
CATTPSEditorHeader.CATTPSConstructedGeometryHdr.ShortHelp="Constructed Geometry Management";
CATTPSEditorHeader.CATTPSConstructedGeometryHdr.Help="Creates an association between geometric elements";
CATTPSEditorHeader.CATTPSConstructedGeometryHdr.LongHelp = "Constructed Geometry Management\nCreates an association between geometric elements.";

//-----------------------------------------------------------------------------
// Constructed Geometry Creation Command
//-----------------------------------------------------------------------------
CATTPSEditorHeader.CATTPSCGCreationHdr.Title="Constructed Geometry Creation";
CATTPSEditorHeader.CATTPSCGCreationHdr.ShortHelp="Constructed Geometry Creation";
CATTPSEditorHeader.CATTPSCGCreationHdr.Help="Creates geometric elements associated to surface";
CATTPSEditorHeader.CATTPSCGCreationHdr.LongHelp = "Constructed Geometry Creation\nCreates geometric elements associated to surface.";

//-----------------------------------------------------------------------------
// Thread
//-----------------------------------------------------------------------------
CATTPSEditorHeader.CATTPSCmdThreadGeometryHdr.Title="Thread Representation Creation";
CATTPSEditorHeader.CATTPSCmdThreadGeometryHdr.ShortHelp="Thread Representation Creation";
CATTPSEditorHeader.CATTPSCmdThreadGeometryHdr.Help="Creates geometric elements associated to surface";
CATTPSEditorHeader.CATTPSCmdThreadGeometryHdr.LongHelp = "Thread Representation Creation\nCreates geometric elements associated to surface.";

//-----------------------------------------------------------------------------
// Basic Dimension Creation Command
//-----------------------------------------------------------------------------
CATTPSEditorHeader.CATTPSBasicDimHdr.Title="Framed (Basic) Dimensions";
CATTPSEditorHeader.CATTPSBasicDimHdr.ShortHelp="Framed (Basic) Dimensions";
CATTPSEditorHeader.CATTPSBasicDimHdr.Help="Manages framed (basic) dimensions";
CATTPSEditorHeader.CATTPSBasicDimHdr.LongHelp = "Framed (Basic) Dimensions\nManages framed (basic) dimensions.";

//-----------------------------------------------------------------------------
// Capture Management Command
//-----------------------------------------------------------------------------
CATTPSEditorHeader.CATTPSCapMgt.Title="Capture Management";
CATTPSEditorHeader.CATTPSCapMgt.ShortHelp="Capture Management";
CATTPSEditorHeader.CATTPSCapMgt.Help="Capture Management";
CATTPSEditorHeader.CATTPSCapMgt.LongHelp = "Capture Management\Manage TPS ownership relatively to capture";

//-----------------------------------------------------------------------------
// Deviation
//-----------------------------------------------------------------------------
CATTPSEditorHeader.CATTPSDeviationHdr.Title= "Deviation...";
CATTPSEditorHeader.CATTPSDeviationHdr.Help ="Creates a deviation on points of component";
CATTPSEditorHeader.CATTPSDeviationHdr.ShortHelp ="Deviation";
CATTPSEditorHeader.CATTPSDeviationHdr.LongHelp ="Deviation\nCreates a deviation on points of component.";

//-----------------------------------------------------------------------------
// Correlated deviation
//-----------------------------------------------------------------------------
CATTPSEditorHeader.CATTPSCorrelatedHdr.Title= "Correlated Deviations...";
CATTPSEditorHeader.CATTPSCorrelatedHdr.Help ="Creates correlated deviations on points of component";
CATTPSEditorHeader.CATTPSCorrelatedHdr.ShortHelp ="Correlated Deviations";
CATTPSEditorHeader.CATTPSCorrelatedHdr.LongHelp ="Correlated Deviations\nCreates correlated deviations on points of component.";

//-----------------------------------------------------------------------------
// Distance between two points
//-----------------------------------------------------------------------------
CATTPSEditorHeader.CATTPSDistancePtPtHdr.Title ="Distance Between Two Points...";
CATTPSEditorHeader.CATTPSDistancePtPtHdr.Help="Distance between two points";
CATTPSEditorHeader.CATTPSDistancePtPtHdr.ShortHelp="Distance Between Two Points";
CATTPSEditorHeader.CATTPSDistancePtPtHdr.LongHelp="Distance between two points";

//-----------------------------------------------------------------------------
// Transform a Default Annotation in a Regular Annotation
//-----------------------------------------------------------------------------
CATTPSEditorHeader.CATTPSDefAnnotRmv.Title ="Apply once";
CATTPSEditorHeader.CATTPSDefAnnotRmv.Help="Modify the application mode of the annotation to have it no more applied to several geometrical features individually";
CATTPSEditorHeader.CATTPSDefAnnotRmv.ShortHelp="The annotation will be applied once to the first geometrical feature";
CATTPSEditorHeader.CATTPSDefAnnotRmv.LongHelp="By running this command the annotation will be applied once to the first geometrical feature of the list of features to which it is currently applied individually";

//-----------------------------------------------------------------------------
// Set Current Capture Command (Contextual Menu)
//-----------------------------------------------------------------------------
CATTPSEditorHeader.CATTPSSetCaptureCurrentHdr.Title="Set Current";
CATTPSEditorHeader.CATTPSSetCaptureCurrentHdr.ShortHelp="Set Capture Current";
CATTPSEditorHeader.CATTPSSetCaptureCurrentHdr.Help="Add newly elements created to capture";
CATTPSEditorHeader.CATTPSSetCaptureCurrentHdr.LongHelp = "Set Current Add newly elements created to capture.";

//-----------------------------------------------------------------------------
// Unset Current Capture Command (Contextual Menu)
//-----------------------------------------------------------------------------
CATTPSEditorHeader.CATTPSUnsetCaptureCurrentHdr.Title="Unset Current";
CATTPSEditorHeader.CATTPSUnsetCaptureCurrentHdr.ShortHelp="Unset Capture Current";
CATTPSEditorHeader.CATTPSUnsetCaptureCurrentHdr.Help="Stop adding new elements to capture";
CATTPSEditorHeader.CATTPSUnsetCaptureCurrentHdr.LongHelp = "Unset Current Stop adding new elements to capture.";

//-----------------------------------------------------------------------------
// Create Assembly Set
//-----------------------------------------------------------------------------
CATTPSEditorHeader.CATTPSNewPrdFTAHdr.Title="Create Assembly Set";
CATTPSEditorHeader.CATTPSNewPrdFTAHdr.ShortHelp="Create Assembly Set";
CATTPSEditorHeader.CATTPSNewPrdFTAHdr.Help="Create Assembly Set";
CATTPSEditorHeader.CATTPSNewPrdFTAHdr.LongHelp = "Create Assembly Set.";

//-----------------------------------------------------------------------------
// Upgrade UDF
//-----------------------------------------------------------------------------
CATTPSEditorHeader.CATFTAUpgradeUDFHdr.Title="User Feature Faces Identifiers Creation";
CATTPSEditorHeader.CATFTAUpgradeUDFHdr.ShortHelp="User Feature Faces Identifiers Creation";
CATTPSEditorHeader.CATFTAUpgradeUDFHdr.Help="Creates User Feature Faces Identifiers";
CATTPSEditorHeader.CATFTAUpgradeUDFHdr.LongHelp = "Creates User Feature Faces Identifiers before tolerancing schema creation";

//-----------------------------------------------------------------------------
// Create schema
//-----------------------------------------------------------------------------
CATTPSEditorHeader.CATFTACreateSchemaHdr.Title="User Feature Tolerancing Schema Creation";
CATTPSEditorHeader.CATFTACreateSchemaHdr.ShortHelp="User Feature Tolerancing Schema Creation";
CATTPSEditorHeader.CATFTACreateSchemaHdr.Help="Creates a User Feature Tolerancing Schema";
CATTPSEditorHeader.CATFTACreateSchemaHdr.LongHelp = "Creates a User Feature Tolerancing Schema";

// Import Annotation Set
CATTPSEditorHeader.CATFTAImportAnnotSetHdr.Title="Import Annotation Set";
CATTPSEditorHeader.CATFTAImportAnnotSetHdr.ShortHelp="Import Annotation Set";
CATTPSEditorHeader.CATFTAImportAnnotSetHdr.Help="Imports an Annotation Set into the current Part document.";
CATTPSEditorHeader.CATFTAImportAnnotSetHdr.LongHelp = "Import Annotation Set\nImports an Annotation Set into the current Part document.";

// 
CATTPSEditorHeader.CATTPSUpdateUnresolvedFeatureHdr.Title="Update Unresolved FTA Features Filter";
CATTPSEditorHeader.CATTPSUpdateUnresolvedFeatureHdr.ShortHelp="Update Unresolved FTA Features Filter";
CATTPSEditorHeader.CATTPSUpdateUnresolvedFeatureHdr.Help="Filter broken FTA Features and un-filter restored FTA Features";
CATTPSEditorHeader.CATTPSUpdateUnresolvedFeatureHdr.LongHelp="Update Unresolved FTA Features Filter\nFilter broken FTA Features and un-filter restored FTA Features";

//-----------------------------------------------------------------------------
// Global copy
//-----------------------------------------------------------------------------
CATTPSEditorHeader.CATFTAGlobalCopyHdr.Title="New Annotation Set From...";
CATTPSEditorHeader.CATFTAGlobalCopyHdr.ShortHelp="New Annotation Set From...";
CATTPSEditorHeader.CATFTAGlobalCopyHdr.Help="Copy an Annotation Set from another Part document.";
CATTPSEditorHeader.CATFTAGlobalCopyHdr.LongHelp = "New Annotation Set From...\nCopy an Annotation Set from another Part document.";

// Change capture Support
CATTPSEditorHeader.CATFTAChangeCaptureSupportHdr.Title="Change Capture Support";
CATTPSEditorHeader.CATFTAChangeCaptureSupportHdr.ShortHelp="Change Capture Support";
CATTPSEditorHeader.CATFTAChangeCaptureSupportHdr.Help="Changes the associated view and the viewpoint of capture.";
CATTPSEditorHeader.CATFTAChangeCaptureSupportHdr.LongHelp = "Change Capture Support\nChanges the associated view and the viewpoint of capture.";

EditFilterOf = "Edit Filter: ";

// Principal Views
CATTPSEditorHeader.CATFTAPrincipalViewHdr.Title="Principal Views";
CATTPSEditorHeader.CATFTAPrincipalViewHdr.ShortHelp="Principal Views";
CATTPSEditorHeader.CATFTAPrincipalViewHdr.Help="Creates front, left, right, bottom and top views according to box definition";
CATTPSEditorHeader.CATFTAPrincipalViewHdr.LongHelp = "Principal Views\nCreates front, left, right, bottom and top views according to box definition.";

// Annotation Sketch Create
CATTPSEditorHeader.CATFTAAnnotationSketchCreateHdr.Title="Create Annotation Sketch";
CATTPSEditorHeader.CATFTAAnnotationSketchCreateHdr.ShortHelp="Create Annotation Sketch";
CATTPSEditorHeader.CATFTAAnnotationSketchCreateHdr.Help="Creates an annotation sketch from a view";
CATTPSEditorHeader.CATFTAAnnotationSketchCreateHdr.LongHelp = "Create Annotation Sketch\nCreates an annotation sketch from a view.";

// Annotation Sketch Create
CATTPSEditorHeader.CATFTAAnnotationSketchWithSectionCurvesCreateHdr.Title="Create Annotation Sketch with Section Curves";
CATTPSEditorHeader.CATFTAAnnotationSketchWithSectionCurvesCreateHdr.ShortHelp="Create Annotation Sketch with Section Curves";
CATTPSEditorHeader.CATFTAAnnotationSketchWithSectionCurvesCreateHdr.Help="Creates an annotation sketch with section curves from a view";
CATTPSEditorHeader.CATFTAAnnotationSketchWithSectionCurvesCreateHdr.LongHelp = "Create Annotation Sketch with Section Curves\nCreates an annotation with section curves sketch from a view.";

// Annotation Sketch Edit
CATTPSEditorHeader.CATFTAAnnotationSketchEditHdr.Title="Edit";
CATTPSEditorHeader.CATFTAAnnotationSketchEditHdr.ShortHelp="Edit";
CATTPSEditorHeader.CATFTAAnnotationSketchEditHdr.Help="Edits annotation sketch of a view";
CATTPSEditorHeader.CATFTAAnnotationSketchEditHdr.LongHelp = "Edit\nEdits annotation sketch of a view.";

// Annotation Sketch Delete
CATTPSEditorHeader.CATFTAAnnotationSketchDeleteHdr.Title="Delete";
CATTPSEditorHeader.CATFTAAnnotationSketchDeleteHdr.ShortHelp="Delete";
CATTPSEditorHeader.CATFTAAnnotationSketchDeleteHdr.Help="Deletes an annotation sketch of a view";
CATTPSEditorHeader.CATFTAAnnotationSketchDeleteHdr.LongHelp = "Delete Annotation Sketch\nDeletes an annotation sketch of a view.";

// Annotation Sketch Show in 3D
CATTPSEditorHeader.CATFTAAnnotationSketchShowIn3DHdr.Title="Show in 3D";
CATTPSEditorHeader.CATFTAAnnotationSketchShowIn3DHdr.ShortHelp="Show in 3D";
CATTPSEditorHeader.CATFTAAnnotationSketchShowIn3DHdr.Help="Shows the annotation sketch of a view in 3D";
CATTPSEditorHeader.CATFTAAnnotationSketchShowIn3DHdr.LongHelp = "Show in 3D\nShows the annotation sketch of a view in 3D.";

// Annotation Sketch Hide in 3D
CATTPSEditorHeader.CATFTAAnnotationSketchHideIn3DHdr.Title="Hide in 3D";
CATTPSEditorHeader.CATFTAAnnotationSketchHideIn3DHdr.ShortHelp="Hide in 3D";
CATTPSEditorHeader.CATFTAAnnotationSketchHideIn3DHdr.Help="Hides the annotation sketch of a view in 3D";
CATTPSEditorHeader.CATFTAAnnotationSketchHideIn3DHdr.LongHelp = "Hide in 3D\nHides the annotation sketch of a view in 3D.";

// Delete View And Capture History
CATTPSEditorHeader.CATFTADeleteDisplayHistoryHdr.Title="Delete History Of Displayed Views And Captures";
CATTPSEditorHeader.CATFTADeleteDisplayHistoryHdr.Help="Deletes history of displayed views and captures in the tree";
CATTPSEditorHeader.CATFTADeleteDisplayHistoryHdr.ShortHelp="Delete History Of Displayed Views And Captures";
CATTPSEditorHeader.CATFTADeleteDisplayHistoryHdr.LongHelp="Delete History Of Displayed Views And Captures\nDeletes history of displayed views and captures in the tree.";

// Capture Display Area
CATTPSEditorHeader.CATFTACaptureDisplayAreaHdr.Title="Capture";
CATTPSEditorHeader.CATFTACaptureDisplayAreaHdr.Help="Captures current window size into the view display area definition";
CATTPSEditorHeader.CATFTACaptureDisplayAreaHdr.ShortHelp="Capture Display Area";
CATTPSEditorHeader.CATFTACaptureDisplayAreaHdr.LongHelp="Capture Display Area\nCaptures current window size into the view display area definition.";
 
// Remove Display Area
CATTPSEditorHeader.CATFTARemoveDisplayAreaHdr.Title="Remove";
CATTPSEditorHeader.CATFTARemoveDisplayAreaHdr.Help="Removes the display area of selected views";
CATTPSEditorHeader.CATFTARemoveDisplayAreaHdr.ShortHelp="Remove Display Area";
CATTPSEditorHeader.CATFTARemoveDisplayAreaHdr.LongHelp="Remove Display Area\nRemoves the display area of selected views.";

// Extended Cylinder Constructed Geometry
CATTPSEditorHeader.CATTPSExtendedCylinderCGHdr.Title="Extended Cylinder Construction Geometry";
CATTPSEditorHeader.CATTPSExtendedCylinderCGHdr.ShortHelp="Extended Cylinder Construction Geometry";
CATTPSEditorHeader.CATTPSExtendedCylinderCGHdr.Help="Creates geometric elements associated to surface";
CATTPSEditorHeader.CATTPSExtendedCylinderCGHdr.LongHelp = "Extended Cylinder Construction Geometry\nCreates geometric elements associated to surface.";
 
// View Layout
CATTPSEditorHeader.CATFTAViewLayoutFromFTAHdr.Title="View Layout";
CATTPSEditorHeader.CATFTAViewLayoutFromFTAHdr.Help="Creates 2DL views according to FTA structure and automatically layout them in different sheets.";
CATTPSEditorHeader.CATFTAViewLayoutFromFTAHdr.ShortHelp="View Layout";
CATTPSEditorHeader.CATFTAViewLayoutFromFTAHdr.LongHelp="View Layout\nCreates 2DL views according to FTA structure (FTA Captures and FTA Views) and automatically layout them in different sheets.";
