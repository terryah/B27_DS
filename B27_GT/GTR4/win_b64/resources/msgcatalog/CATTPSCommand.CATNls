CATTPS.NewViewCreatedTitle           ="Annotation Creation";
CATTPS.NewViewCreatedMessage         ="Existing annotation planes/views cannot be used for the annotation.
The annotation will be created on a new or another view.";
CATTPS.ViewNotActiveTitle            ="Annotation Creation";
CATTPS.ViewNotActiveMessage          ="The active view cannot be used for the annotation
The annotation lies on a non-active view.";
CATTPS.ViewCantDeleteTitle           ="View Deletion";
CATTPS.ViewNotEmptyCantDeleteMessage ="The view supports annotations and consequently cannot be deleted.
Transfer or remove annotations first.";
CATTPS.ViewNotEmptyFilterCantDeleteMessage ="Some annotations are filtered. Delete cannot be performed until all
the annotations are restored.";
CATTPS.ViewCantBeCopiedTitle         ="View Copy";
CATTPS.ViewCantBeCopiedMessage       ="The 3D View cannot be copied.";
CATTPS.ViewCompCantDeleteMessage     ="The View is part of an Offset view or of an aligned view 
and consequently cannot be deleted.
Remove the father view first.";
CATTPS.SetUsedByWeldCantDeleteTitle   ="Annotation Set Deletion";
CATTPS.SetUsedByWeldCantDeleteMessage ="The annotation set supports weld planners and consequently cannot be deleted.
Remove weld planners first.";
CATTPS.SetUsedByFilteredAnnotationCantDeleteMessage ="Some annotations are filtered. Delete cannot be performed until all
the annotations are restored.";
CATTPS.SetCantBeCopyTitle   ="Annotation Set Copy";
CATTPS.SetCantBeCopyMessage ="The Annotation Set cannot be copied.";
CATTPS.SubNodeCantBeCopyTitle   ="Sub Node Copy";
CATTPS.SubNodeCantBeCopyMessage ="The Sub Node cannot be copied.";
CATTPS.CGCantBeCopyTitle   ="Constructed Geometry Copy";
CATTPS.CGCantBeCopyMessage ="The Constructed Geometry cannot be copied.";
CATTPS.SomeAnnotNotTransferedTitle   ="Annotation Transfer";
CATTPS.SomeAnnotNotTransferedMessage ="The destination view cannot be used for some annotations.
These annotations cannot be transferred.";
CATTPS.TPSNoCreated                  ="The geometric element is not supported. The annotation is not created.
Select an other geometric element or a new command.";
CATTPS.TPSNoCreatedTitle             ="Annotation Plane Creation";
CATTPS.CompareSetTitle               ="Compare View";
CATTPS.CompareSetMessage             ="The selected view (annotation plane) cannot be used for the annotation.
This view is not in the same annotation set.
Select a new view in the same annotation set.";
CATTPS.DimensionLimitOfSizeInvalidTitle      ="Invalid Size Limit";
CATTPS.DimensionLimitOfSizeInvalidMessage    ="The tolerance limit of the dimension is invalid";
CATTPS.NoPlaneSelectedTitle = "Tolerance Zone Direction Definition";
CATTPS.NoPlaneSelectedMessage = "Please define the direction of the tolerance zone.";
CATTPS.NoPlaneSelectedBlock = "No definition element has been selected. \n Hence, the created geometrical tolerance applies to all the lines defined by \n all the possible intersection planes. This is not explicitly allowed by \n GD&T standards. Make sure it is really the specification you want to define.";
CATTPS.NoGeometricSelectedTitle = "Annotation Creation";
CATTPS.NoGeometricSelectedMessage = "Select a geometric element.";
CATTPS.NoDatumPlaneSelectedTitle = "Datum Plane Direction Definition";
CATTPS.NoDatumPlaneSelectedMessage = "A datum plane shall be defined for complex or prismatic surface datum feature.";
CATTPS.NoNameCantCreateCaptureTitle = "Capture creation";
CATTPS.NoNameCantCreateCaptureMessage = "You can`t enter a null string";
CATTPS.NoCaptureInDocTitle = "Capture management";
CATTPS.NoCaptureInDocMessage = "There are no capture in document";
CATTPS.InCaptureWorkshopTitle = "Capture management";
CATTPS.InCaptureWorkshopMessage = "You must be outside of the capture workshop";
CATTPS.NoCaptureCanManageTitle = "Capture management";
CATTPS.NoCaptureCanManageMessage = "No existing Capture can manage your selection 
of 3D annotations";
CATTPS.BadTTRSTitle = "Geometry Selection Error";
CATTPS.BadTTRSMessage = "You can't select this geometry.";
CATTPS.BadTTRSMessageDriving = "You can't select this geometry. It�s already a representing geometry element.";
CATTPS.BadCanonRespecTitle = "Canonicity Redefinition Error";
CATTPS.BadPlaneNormalMessage = "The plane normal is NULL.";
CATTPS.BadAxisMessage = "The axis direction is NULL.";
CATTPS.BadTTRSForRestrictedAreaTitle = "Bad Geometry Selection Error";
CATTPS.BadTTRSForRestrictedAreaMsg = "Another Restricted Area exist on this geometry.";
CATTPS.InvalidRestrictedAreaTitle = "Invalid Restricted Area";
CATTPS.InvalidRestrictedAreaMsg = "The restricting area is not included in the restricted area.";
CATTPS.SameRestrictedAreaMsg = "The restricting area is the same surface than the restricted area.";
CATTPS.SelectedPlaneInvalidTitle = "Datum Plane Direction Definition";
CATTPS.SelectedPlaneInvalidTPSNotExistMessage = "The selected plane represents already another User Surface or Group of User Surfaces. Do you want it to represent the User Surface or Group of User Surfaces on which the datum feature is being defined ? If yes, the existing representing link will be modified.";
CATTPS.SelectedPlaneInvalidTPSExistMessage = "The selected plane represents already another User Surface or Group of User Surfaces. The following annotation(s) is(are) impacted . Do you want it to represent the User Surface or Group of User Surfaces on which the datum is being defined  ? If yes, the existing representing link will be modified, and the impacted annotation(s) may become invalid.\n";
CATTPS.SelectedPlaneInvalidInformationMessage = "\n\nInformation: to visualize the geometric elements represented by the plane, you have to select it after the �3D-Annotation-Query Switch On/Switch Off� option has been checked on. The geometric elements will be highlighted. You can also utilize the �Constructed Geometry Management� command to visualize and to remove the representing links.";
CATTPS.NoNameCantCreateAssemblySetTitle = "Assembly Set Creation";
CATTPS.NoNameCantCreateAssemblySetMessage = "You can't enter a null string";
CATTPS.TTRSCantBeCopyMessage ="The Annotation geometric feature cannot be copied.";
CATTPS.AnnotationNonEditable="The annotation can't be edited";

CATTPS.NonUniformTitle           = "Non-Uniform Definition";
CATTPS.NonUniformLMBoundry       = "Please define the LM boundry of the nonuniform.";
CATTPS.NonUniformMMBoundry       = "Please define the MM boundry of the nonuniform.";
CATTPS.NonUniform                = "Please define boundries of the nonuniform.";
CATTPS.OrientationSpacTitle      = "Orientation specification ";
CATTPS.EachElement               = "Please define each element orientation.";
CATTPS.EachRadialElement         = "Please define each radial element orientation.";

CATTPSRefFrame.AxisSystemInvalidTitle = "Invalid Axis System";
CATTPSRefFrame.DOFWarningTitle = "Partial constraining degrees of freedom ";

CATTPSRefFrame.AxisSystemDeletedMessage = "Invalid associated axis system.\nThe axis system feature associated to the DRF has been deleted.\nEdit the DRF and either remove its associated axis system or select a new one.";
CATTPSRefFrame.SphericalRefFrameInvalidMessage = "Invalid axis system for spherical DRF.\nThe origin of the axis system shall be coincident with the center point of the spherical datum system (Datum Reference Frame).\nRemove the axis system or change the axis system origin or choose an axis system which origin is coincident with the center point of the spherical datum system.";
CATTPSRefFrame.PlanarRefFrameInvalidMessage = "Invalid axis system for planar DRF.\nOne plane of the axis system shall be parallel to the planar datum system (Datum Reference Frame).\nRemove the axis system or change the axis system orientation or choose an axis system which orientation is aligned with the planar datum system.";
CATTPSRefFrame.CylindricalRefFrameInvalidMessage = "Invalid axis system for cylindrical DRF.\nOne axis line of the axis system shall be coincident with the axis line of the cylindrical datum system (Datum Reference Frame).\nRemove the axis system or change the axis system orientation or choose an axis system which orientation is aligned with the cylindrical datum system.";
CATTPSRefFrame.RevoluteRefFrameInvalidMessage = "Invalid axis system for revolute DRF.\nOne axis line of the axis system shall be coincident with the axis line of the revolute datum system (Datum Reference Frame).\nRemove the axis system or change the axis system orientation or choose an axis system which orientation is aligned with the revolute datum system.";
CATTPSRefFrame.PrismaticRefFrameInvalidMessage = "Invalid axis system for prismatic DRF.\nOne axis line of the axis system shall be parallel to the extrusion direction of the prismatic datum system (Datum Reference Frame).\nRemove the axis system or change the axis system orientation or choose an axis system which orientation is aligned with the prismatic datum system.";

CATTPSRefFrame.PlanarPrimaryBoxDatumInvalidMessage        = "Invalid orientation of the DRF axis system for customization of planar primary datum.\nOne plane of the associated axis system shall be parallel to the planar primary datum of the DRF (Datum Reference Frame).\nChange the axis system orientation or choose an axis system which orientation is aligned with the planar primary datum of the DRF.";
CATTPSRefFrame.PlanarSecondaryBoxDatumInvalidMessage      = "Invalid orientation of the DRF axis system for customization of planar secondary datum.\nOne plane of the associated axis system shall be parallel to the planar secondary datum of the DRF (Datum Reference Frame).\nChange the axis system orientation or choose an axis system which orientation is aligned with the planar secondary datum of the DRF.";
CATTPSRefFrame.PlanarTertiaryBoxDatumInvalidMessage       = "Invalid orientation of the DRF axis system for customization of planar tertiary datum.\nOne plane of the associated axis system shall be parallel to the planar tertiary datum of the DRF (Datum Reference Frame).\nChange the axis system orientation or choose an axis system which orientation is aligned with the planar tertiary datum of the DRF.";
CATTPSRefFrame.CylindricalPrimaryBoxDatumInvalidMessage   = "Invalid orientation of the DRF axis system for customization of cylindrical primary datum.\nOne axis line of the associated axis system shall be parallel with the axis line of the cylindrical primary datum of the DRF (Datum Reference Frame).\nChange the axis system orientation or choose an axis system which orientation is aligned with the cylindrical primary datum of the DRF.";
CATTPSRefFrame.CylindricalSecondaryBoxDatumInvalidMessage = "Invalid orientation of the DRF axis system for customization of cylindrical secondary datum.\nOne axis line of the associated axis system shall be parallel with the axis line of the cylindrical secondary datum of the DRF (Datum Reference Frame).\nChange the axis system orientation or choose an axis system which orientation is aligned with the cylindrical secondary datum of the DRF.";
CATTPSRefFrame.CylindricalTertiaryBoxDatumInvalidMessage  = "Invalid orientation of the DRF axis system for customization of cylindrical tertiary datum.\nOne axis line of the associated axis system shall be parallel with the axis line of the cylindrical tertiary datum of the DRF (Datum Reference Frame).\nChange the axis system orientation or choose an axis system which orientation is aligned with the cylindrical tertiary datum of the DRF.";
CATTPSRefFrame.RevolutePrimaryBoxDatumInvalidMessage      = "Invalid orientation of the DRF axis system for customization of revolute primary datum.\nOne axis line of the associated axis system shall be parallel with the axis line of the revolute primary datum of the DRF (Datum Reference Frame).\nChange the axis system orientation or choose an axis system which orientation is aligned with the revolute primary datum of the DRF.";
CATTPSRefFrame.RevoluteSecondaryBoxDatumInvalidMessage    = "Invalid orientation of the DRF axis system for customization of revolute secondary datum.\nOne axis line of the associated axis system shall be parallel with the axis line of the revolute secondary datum of the DRF (Datum Reference Frame).\nChange the axis system orientation or choose an axis system which orientation is aligned with the revolute secondary datum of the DRF.";
CATTPSRefFrame.RevoluteTertiaryBoxDatumInvalidMessage     = "Invalid orientation of the DRF axis system for customization of revolute tertiary datum.\nOne axis line of the associated axis system shall be parallel with the axis line of the revolute tertiary datum of the DRF (Datum Reference Frame).\nChange the axis system orientation or choose an axis system which orientation is aligned with the revolute tertiary datum of the DRF.";
CATTPSRefFrame.PrismaticPrimaryBoxDatumInvalidMessage     = "Invalid orientation of the DRF axis system for customization of prismatic primary datum.\nOne axis line of the associated axis system shall be parallel with the direction  of the prismatic primary datum of the DRF (Datum Reference Frame).\nChange the axis system orientation or choose an axis system which orientation is aligned with the prismatic primary datum of the DRF.";
CATTPSRefFrame.PrismaticSecondaryBoxDatumInvalidMessage   = "Invalid orientation of the DRF axis system for customization of prismatic secondary datum.\nOne axis line of the associated axis system shall be parallel with the direction  of the prismatic secondary datum of the DRF (Datum Reference Frame).\nChange the axis system orientation or choose an axis system which orientation is aligned with the prismatic secondary datum of the DRF.";
CATTPSRefFrame.PrismaticTertiaryBoxDatumInvalidMessage    = "Invalid orientation of the DRF axis system for customization of prismatic tertiary datum.\nOne axis line of the associated axis system shall be parallel with the direction  of the prismatic tertiary datum of the DRF (Datum Reference Frame).\nChange the axis system orientation or choose an axis system which orientation is aligned with the prismatic tertiary datum of the DRF.";

CATTPSExtCylinderCG.InvalidReferencePlaneTitle = "Invalid reference plane selection";
CATTPSExtCylinderCG.InvalidReferencePlaneMessage = "Invalid reference plane selection: the selected element shall be a planar face perpendicular to the cylindrical pin/hole you want to define an extension. The selected element is ignored.";

CATTPSRefFrame.DRFInvalidTitle = "Invalid Datum Reference Frame";
CATTPSRefFrame.ModifiedDRFAfterRemovalofUselessParentheses = "Modify Datum Reference Frame After Removal Of Useless Parantheses";
CATTPSRefFrame.ModifiedDRFStringAfterRemovalofUselessParentheses="Useless parentheses have been removed.\n /P1 has been replaced by /P2.";

CATTPSRefFrame.ErrorLPFollowedByDForLP = "Invalid collection definition: left parenthesis shall be followed by either a datum feature identifier or another left parenthesis.\n Modify the following character string(s): /P1.";

CATTPSRefFrame.ErrorHYPrecededByDForRP = "Invalid collection definition: hyphens shall be preceded by either a datum feature and its own modifier(s) (if any) or a right parenthesis.\n Modify the following character string(s): /P1.";

CATTPSRefFrame.ErrorHYFollowedByDForLP = "Invalid collection definition: hyphens shall be followed by either a datum feature identifier or a left parenthesis.\n Modify the following character string(s): /P1";

CATTPSRefFrame.ErrorRPPrecededByDForRP = "Invalid collection definition: right parenthesis shall be preceded by either a datum feature and its own modifier(s) (if any) or a right parenthesis and the modifier(s) of the corresponding collection (if any).\n Modify the following character string(s): /P1";

CATTPSRefFrame.ErrorDFsSeperatedByHY = "Invalid collection definition: datum features shall be separated by hyphens. Modify the following character string(s): /P1";

CATTPSRefFrame.ErrorLPRPCompatibility = "Invalid collection(s) definition: There shall be as many right parentheses as left parentheses. \n Modify the text to conform this rule.";

CATTPSRefFrame.ErrorIdentifierCGPattrnFeature = "/P1 is the identifier of a datum feature established from a pattern of features.\n It shall be indicated in the Datum Reference Frame by doubling this identifier separated by a hyphen: /P1-/P1. \n Modify the /P1 character string to comply with this rule or remove it.";

CATTPSRefFrame.ErrorIdentifierSingleCG = "/P1 is an invalid datum feature identifier and/or modifier. Remove this character string or modify it.";

CATTPSRefFrame.ErrorIdentifierManyCG = "/P1 are invalid datum feature identifier(s) and/or modifier(s). Remove these character strings or modify them.";

CATTPSRefFrame.ErrorIdentifierDatumNotAvail = "There is no datum feature identifier defined. Modify the text by defining at least one datum feature identifier.";

CATTPSRefFrame.WarningAfterCheckUniquenessofDRF ="Uniqueness of Datum Reference Frame.";
CATTPSRefFrame.WarningStringAfterCheckUniquenessofDRF="The Datum Reference Frame DRF1 (/P1) is identical to the Datum Reference Frame DRF2 (/P2).";

CATTPSRefFrame.PrimaryDatumError ="Primary datum Error: ";
CATTPSRefFrame.SecondaryDatumError ="Secondary datum Error: ";
CATTPSRefFrame.TertiaryDatumError ="Tertiary datum Error: ";

CATTPSDatum.HelpOrientationPlaneSelection ="Plane selection for Orienatation plane option.";
CATTPSDatum.HelpStringOrientationPlaneSelection ="Select a planar or cylindrical or revolute invariance class \n surface, or press ESC to exit the creation command.";

CATTPSDatum.HelpOrientationPlaneSelectionForParallelChoice ="Plane selection for Orienatation plane option for parallel choice.";
CATTPSDatum.HelpStringOrientationPlaneSelectionForParallelChoice ="Select a planar invariance class surface, or press ESC \n to exit the creation command.";

CATTPS.NoIntersectionPlaneBlock = "No intersecation plane has been defined. \n Hence, the created geometrical tolerance applies to all the lines defined by \n all the possible intersection planes. This is not explicitly allowed by \n GD&T standards. Make sure it is really the specification you want to define.";
CATTPS.NoIntersectionDatumSelectedTitle = "Invalid auxiliary feature indicator";
CATTPS.NoIntersectionDatumSelectedMessage = "A datum feature must be specified for the definition of the intersection plane indicator of the geometrical tolerance";
CATTPS.NoOrientationDatumSelectedMessage = "A datum feature must be specified for the definition of the orientation plane indicator of the geometrical tolerance";
CATTPS.NoCollectionDatumSelectedMessage = "A datum feature must be specified for the definition of the collection plane indicator of the geometrical tolerance";

CATTPS.IntersectionPlaneCirculatiryMessage = "The chosen intersection plane for the created profile any line geometrical \n tolerance is equivalent to a roundness (circularity) tolerance. \n Make sure it is really the specification you want to define.";
CATTPS.IntersectionPlaneStraightnessMessage = "The chosen intersection plane for the created profile any line geometrical \n tolerance is equivalent to a generatrix straightness tolerance. \n Make sure it is really the specification you want to define.";

CATTPS.IntersectionPerpedicularSelectMessage = "Select a planar or cylindrical or revolute invariance class \n surface, or press 'ESC' to exit the creation command.";
CATTPS.IntersectionParallalSelectMessage = "Select a planar invariance class surface, or press 'ESC' \n to exit the creation command.";
CATTPS.IntersectionIncludingSelectMessage = "Select a cylindrical or revolute invariance class surface, \n or press 'ESC' to exit the creation command.";

CATTPS.OrientationPerpedicularSelectMessage = "Select a planar or cylindrical or revolute invariance class \n surface, or press 'ESC' to exit the creation command.";
CATTPS.OrientationParallalSelectMessage = "Select a planar invariance class surface, or press 'ESC' \n to exit the creation command.";
CATTPS.OrientationInclinedToSelectMessage = "Select a planar or cylindrical or revolute invariance class \n surface, or press 'ESC' to exit the creation command.";

CATTPS.CollectionPerpedicularIncludingSelectMessage = "Select a cylindrical or revolute invariance class surface, \n or press 'ESC' to exit the creation command.";
CATTPS.CollectionParallalSelectMessage = "Select a planar invariance class surface, or press 'ESC' \n to exit the creation command.";
