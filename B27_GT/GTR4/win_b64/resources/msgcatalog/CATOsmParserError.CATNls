
osmfeat.ParseError                = "Syntax error while parsing OSM template.";
osmfeat.ArgumentRequiredError     = "An argument is required after '/p' option.";
osmfeat.SyntaxError               = "Syntax error while parsing command-line (invalid option '/p'). Use the '-help' option for details about the command-line syntax.";
osmfeat.RightsError               = "Insufficient rights, permission denied.";
osmfeat.CreationError             = "Unable to create catalog '/p'.";
osmfeat.AccessAuthorizationError  = "Unauthorized to access catalog '/p'.";
osmfeat.CatalogGenerationError    = "Failed to generate catalog '/p'.";
osmfeat.SetClientIdError          = "Unable to set client identifier for catalog '/p'.";
osmfeat.SaveError                 = "Unable to save catalog '/p1' as '/p2'.";
osmfeat.OutputDirAccessError      = "Unable to access output directory '/p'.";
osmfeat.AlreadyInGraphicPathError = "Catalog '/p' already exists in 'CATGraphicPath' concatenation.";
osmfeat.ExistingCatalogWithDifferentCaseError = "The catalog '/p' exists in 'CATGraphicPath' concatenation. Catalog creation with the same name than an existing catalog is forbidden (even if the case is different).";
osmfeat.FileNameInconsistentWithMDCName = "Catalog file name '/p1' does not match catalog internal name '/p2'.";
osmfeat.IncorrectCaseInOsmfeatParameterCatalogName = "Catalog name '/p1' does not match the name passed in parameter of osmfeat '/p2'. The correct case needs to be specified.";
osmfeat.OutputDirCatError         = "Catalog '/p1' already exists in output directory '/p2'.";
osmfeat.OutputDirOsmError         = "OSM file '/p1' already exists in output directory '/p2'.";
osmfeatTemplateNotFoundError      = "Unable to find template file '/p.osm'. Only 'std' and 'delmia' template are allowed.";
osmfeat.TemplateOpenError         = "Unable to open template file '/p'.";
osmfeat.NotInGraphicPathError     = "Unable to find catalog '/p' in 'CATGraphicPath' concatenation.";
osmfeat.OsmDirAccessError         = "Unable to access OSM file directory '/p'.";
osmfeat.OsmNotFoundError          = "Unable to find OSM file '/p1' in directory '/p2'.";
osmfeat.OsmNotReadableError       = "OSM file '/p' is not readable.";
osmfeat.UpdateError               = "Unable to update catalog '/p'.";
osmfeat.UnidentifiedError         = "An unidentified error has occurred.";
osmfeat.ClientIdRequiredError     = "A client identifier is required (use '-with-client-id' option).";
osmfeat.CatalogNameExtensionError = "A file with extension '.feat' or '.CATfct' is required after '-catalog-name' option ('/p' is not a catalog file).";
osmfeat.CatalogNameFilenameError  = "A filename is required after '-catalog-name' option ('/p' is a file path, not a filename).";
osmfeat.WithOsmExtensionError     = "A file with extension '.osm' is required after '-with-osm' option ('/p' is not an OSM file).";
osmfeat.WithOsmPathError          = "A file path is required after '-with-osm' option ('/p' is a filename, not a file path).";
osmfeat.AsExtensionError          = "A file with extension '.osm' is required after '-as' option ('/p' is not an OSM file).";
osmfeat.AsFilenameError           = "A filename is required after '-as' option ('/p' is a file path, not a filename).";
osmfeat.CatalogNameRequiredError  = "Option '-catalog-name' is required for all operations.";
osmfeat.CatalogNotReadableError   = "Catalog '/p' is not readable.";
osmfeat.MissingMandatoryOption    = "An option is required after '/p' (use '-help' option).";

osmfeat.CannotSaveUpgradedCatalog = "Cannot save the updated catalog '/p'.";
osmfeat.CannotOpenCatalog = "Cannot open catalog '/p'.";
osmfeat.GeneratingOsmFile = "Generating OSM file '/p'.";
osmfeat.OsmFileGenerationError = "Cannot generate OSM file '/p'.";
osmfeat.CannotCreateBlock = "Cannot create block '/p'.";

osmfeat.IncorrectClientId = "The client identifier is incorrect (check '-with-client-id' option).";
osmfeat.CannotOpenRootContainer = "Cannot open root container for catalog '/p'.";
osmfeat.NewExtensionsActivation = "Robust extensions can be added on the features of the catalog '/p'.";
osmfeat.ExtensionNoFacetValue = "The parameter 'ExtensionFeature./p1' must be valued in the file '/p2.CATRsc'";
osmfeat.V6toV5CompliantCatalogUpdateInV6 = "This catalog is V6toV5 compliant, it must be updated in V5.";

catfct.CatalogNameError = "Invalid catalog ('/p' is not a CAA catalog).";
catfct.Help = 
            "\n",
            "Usage : \n",
            "\n",
            "CATfctEditorAssistant -create-new-catalog -catalog-name <name_of_catalog> -with-client-id <id> [-author <name>] [-comment <comment>] [-using-template <template>] [-into-directory <output_path>] \n",
            "CATfctEditorAssistant -update-catalog -catalog-name <name_of_catalog> -with-client-id <id> [-with-osm <path_of_osm>] [-author <name>] [-comment <comment>] [-into-directory <output_path>] \n",
            "CATfctEditorAssistant -describe-as-osm -catalog-name <name_of_catalog> -with-client-id <id> [-in-version <level>] -as <name_of_osm> [-into-directory <output_path>] \n",
            "CATfctEditorAssistant -simulate-catalog-update -catalog-name <name_of_catalog> -with-client-id <id> -in-version <level> -with-osm <path_of_osm> [-into-directory <output_path>] \n",
            "CATfctEditorAssistant -help \n",
            "\n",
            "where <name_of_xxx> stands for a filename with its extension, but not a path \n",
            " and  <path_of_xxx> stands for a file path. \n",
            "\n",
            "Details : \n",
            "\n",
            "   -create-new-catalog : create a new empty catalog and its associated osm file. \n",
            "\n",
            "     -catalog-name    (mandatory)  specify the name of the new catalog to create. \n",
            "     -with-client-id  (mandatory)  set the client identification key. \n",
            "     -author                       specify author's name in history. \n",
            "     -comment                      add a comment to history. \n",
            "     -using-template               use another template (default is standard template). \n",
            "     -into-directory               indicate the output directory, where the output files are stored. \n",
            "\n",
            "   -update-catalog : update an existing catalog with an osm file. \n",
            "\n",
            "     -catalog-name    (mandatory)  specify the name of the catalog to update. \n",
            "     -with-client-id  (mandatory)  give client identification key. \n",
            "     -with-osm                     specify the osm file's path used for update. \n",
            "     -comment                      add a comment to history. \n",
            "     -into-directory               indicate the output directory, where the output catalog is stored. \n",
            "\n",
            "   -describe-as-osm : describe a catalog by generating its associated osm file. \n",
            "\n",
            "     -catalog-name    (mandatory)  specify the name of the catalog to describe. \n",
            "     -with-client-id  (mandatory)  give client identification key. \n",
            "     -in-version                   will produce a description that is a V5 only OR V6 only view of the catalog content. Consequently such an osm description cannot be used for an update. \n",
            "     -as              (mandatory)  indicate the osm file to generate, containing the catalog description. \n",
            "     -into-directory               indicate the output directory, where the output osm file is stored. \n",
            "\n",
            "   -simulate-catalog-update : simulate a catalog update. \n",
            "\n",
            "     -catalog-name    (mandatory)  specify the name of the catalog to simulate. \n",
            "     -with-client-id  (mandatory)  give client identification key. \n",
            "     -in-version      (mandatory)  will produce a description that is a V5 only OR V6 only view of the catalog content. Consequently such an osm description cannot be used for an update. \n",
            "     -with-osm                     specify the osm file's path used for simulation. \n",
            "     -into-directory               indicate the output directory, where the output osm file (with extension .V#osm) is stored. \n",	
            "\n",
            "Notes : \n",
            "\n",
            " - Catalogs have extension '.CATfct'. \n",
            " - OSM files have extension '.osm'. \n",
            " - All input catalogs must be located in 'CATGraphicPath' concatenation.";
            
osmfeat.Help = 
            "\n",
            "Usage : \n",
            "\n",
            "osmfeat -create-new-catalog -catalog-name <name_of_catalog> [-with-client-id <id>] [-author <name>] [-comment <comment>] [-using-template <template>] [-into-directory <output_path>] \n",
            "osmfeat -update-catalog -catalog-name <name_of_catalog> [-with-client-id <id>] [-with-osm <path_of_osm>] [-author <name>] [-comment <comment>] [-into-directory <output_path>] \n",
            "osmfeat -describe-as-osm -catalog-name <name_of_catalog> [-with-client-id <id>] [-in-version <level>] -as <name_of_osm> [-into-directory <output_path>] \n",
            "osmfeat -simulate-catalog-update -catalog-name <name_of_catalog> [-with-client-id <id>] -in-version <level> -with-osm <path_of_osm> [-into-directory <output_path>] \n",
            "osmfeat -help \n",
            "\n",
            "where <name_of_xxx> stands for a filename with its extension, but not a path \n",
            " and  <path_of_xxx> stands for a file path. \n",
            "\n",
            "Details : \n",
            "\n",
            "   -create-new-catalog : create a new empty catalog and its associated osm file. \n",
            "\n",
            "     -catalog-name    (mandatory)  specify the name of the new catalog to create. \n",
            "     -with-client-id               set the client identification key. \n",
            "     -author                       specify author's name in history. \n",
            "     -comment                      add a comment to history. \n",
            "     -using-template               use another template (default is standard template). \n",
            "     -into-directory               indicate the output directory, where the output files are stored. \n",
            "\n",
            "   -update-catalog : update an existing catalog with an osm file. \n",
            "\n",
            "     -catalog-name    (mandatory)  specify the name of the catalog to update. \n",
            "     -with-client-id               give client identification key. \n",
            "     -with-osm                     specify the osm file's path used for update. \n",
            "     -comment                      add a comment to history. \n",
            "     -into-directory               indicate the output directory, where the output catalog is stored. \n",
            "\n",
            "   -describe-as-osm : describe a catalog by generating its associated osm file. \n",
            "\n",
            "     -catalog-name    (mandatory)  specify the name of the catalog to describe. \n",
            "     -with-client-id               give client identification key. \n",
            "     -in-version                   will produce a description that is a V5 only OR V6 only view of the catalog content. Consequently such an osm description cannot be used for an update. \n",
            "     -as              (mandatory)  indicate the osm file to generate. \n",
            "     -into-directory               indicate the output directory, where the output osm file is stored. \n",
            "\n",
            "   -simulate-catalog-update : simulate a catalog update. \n",
            "\n",
            "     -catalog-name    (mandatory)  specify the name of the catalog to simulate. \n",
            "     -with-client-id               give client identification key. \n",
            "     -in-version      (mandatory)  will produce a description that is a V5 only OR V6 only view of the catalog content. Consequently such an osm description cannot be used for an update. \n",
            "     -with-osm                     specify the osm file's path used for simulation. \n",
            "     -into-directory               indicate the output directory, where the output osm file (with extension .V#osm) is stored. \n",	
            "\n",
            "Notes : \n",
            "\n",
            " - Catalogs have extension '.feat' (DS) or '.CATfct' (CAA). \n",
            " - OSM files have extension '.osm'. \n",
            " - All input catalogs must be located in 'CATGraphicPath' concatenation.";

osmfeat.HelpAdmin = 
    "\n",
    "osmfeat -admin [-no-su-map] [-no-history] [-format-ducat] -forge-catalog -catalog-name <name_of_catalog> [-with-client-id <id>] -with-osm <path_of_osm> [-into-directory <output_path>] \n",
    "osmfeat -admin [-no-su-map] [-no-mlc-version] [-no-history] [-format-ducat] -create-new-catalog ... \n",
    "osmfeat -admin [-no-su-map] [-no-mlc-version] [-no-history] [-format-ducat] -describe-as-osm ... \n",
    "osmfeat -admin [-no-su-map] [-no-mlc-version] [-no-history] [-format-ducat] -update-catalog ... \n",
    "osmfeat -admin [-no-su-map] [-no-mlc-version] [-no-history] [-format-ducat] -simulate-catalog-update ... \n",
    "osmfeat -admin -extract-metadatacont -catalog-name <name_of_catalog> [-metadatacont-index <metadatacont_index>] -as <name_of_osm> [-into-directory <output_path>] \n",
    "osmfeat -admin -inject-metadatacont -catalog-name <name_of_catalog> [-metadatacont-index <metadatacont_index>] -with-osm <path_of_osm> \n",
    "osmfeat -admin -remove-metadatacont -catalog-name <name_of_catalog> [-into-directory <output_path>] \n",
    "osmfeat -admin -display-properties -catalog-name <catalog_name>\n",
    "osmfeat -admin -help \n",
    "\n",
    "  -admin           : perform an operation as an administrator (only for accredited users). \n",
    "  -no-su-map       : save the catalog without an SU map (meaningful for forge and update), \n",
    "  -no-mlc-version  : remove number indicating the Multi-Level Compatibility. \n",
    "  -no-history      : generate an osm file without history. \n", 
    "  -format-ducat    : generate an osm file containing additional data useful for DUCAT process (meaningful for describe). \n",
    "  -forge-catalog   : create a catalog from an existing osm file. \n",
    "\n",
    "All others options (above mentioned as '...') remain available in admin mode. Type 'osmfeat -help' for details about these options.";

