// CATAsmMassPage.CATNLs

// English Version
// tbu avril 1999

PageTitle="Mechanical";

// Characteristics 

GeneralFrame.Title                            ="Characteristics";
GeneralFrame.Help                             ="Displays the product characteristics";
GeneralFrame.LongHelp                         ="Characteristics\nDisplays the product characteristics.";

GeneralFrame.Density.Title                    ="Density:";
GeneralFrame.Density.Help                     ="Displays the product density";
GeneralFrame.Density.LongHelp                 ="Displays the product density.";

GeneralFrame.Mass.Title                       ="Mass:";
GeneralFrame.Mass.Help                        ="Displays the product mass";
GeneralFrame.Mass.LongHelp                    ="Displays the product mass.";

GeneralFrame.Volume.Title                     ="Volume:";
GeneralFrame.Volume.Help                      ="Displays the product volume";
GeneralFrame.Volume.LongHelp                  ="Displays the product volume.";

GeneralFrame.Surface.Title                    ="Surface:";
GeneralFrame.Surface.Help                     ="Displays the product surface";
GeneralFrame.Surface.LongHelp                 ="Displays the product surface";

// Inertia center

GravityFrame.Title                            ="Inertia center";
GravityFrame.Help                             ="Displays the coordinates of the inertia center";
GravityFrame.LongHelp                         ="Inertia center\nDisplays the coordinates of the inertia center.";

GravityFrame.Xlabel.Title                     ="x:";
GravityFrame.Xlabel.Help                      ="Displays the x coordinate of the inertia center";
GravityFrame.Xlabel.LongHelp                  ="Displays the x coordinate of the inertia center.";

GravityFrame.Xeditor.Help                     ="Displays the x coordinate of the inertia center";
GravityFrame.Xeditor.LongHelp                 ="Displays the x coordinate of the inertia center.";

GravityFrame.Ylabel.Title                     ="y:";
GravityFrame.Ylabel.Help                      ="Displays the y coordinate of the inertia center";
GravityFrame.Ylabel.LongHelp                  ="Displays the y coordinate of the inertia center.";

GravityFrame.Yeditor.Help                     ="Displays the y coordinate of the inertia center";
GravityFrame.Yeditor.LongHelp                 ="Displays the y coordinate of the inertia center.";

GravityFrame.Zlabel.Title                     ="z:";
GravityFrame.Zlabel.Help                      ="Displays the z coordinate of the inertia center";
GravityFrame.Zlabel.LongHelp                  ="Displays the z coordinate of the inertia center.";

GravityFrame.Zeditor.Help                     ="Displays the z coordinate of the inertia center";
GravityFrame.Zeditor.LongHelp                 ="Displays the z coordinate of the inertia center.";

// Inertia matrix

InertyFrame.Title                             ="Inertia matrix";
InertyFrame.Help                              ="Displays the components of the inertia matrix";
InertyFrame.LongHelp                          ="Inertia matrix\nDisplays the components of the inertia matrix.";

InertyFrame.IXXlabel.Title                    ="Ixx:";
InertyFrame.IXXlabel.Help                     ="Displays the Ixx component of the inertia matrix";
InertyFrame.IXXlabel.LongHelp                 ="Displays the Ixx component of the inertia matrix.";

InertyFrame.IXXeditor.Help                    ="Displays the Ixx component of the inertia matrix";
InertyFrame.IXXeditor.LongHelp                ="Displays the Ixx component of the inertia matrix.";

InertyFrame.IXYlabel.Title                    ="Ixy:";
InertyFrame.IXYlabel.Help                     ="Displays the Ixy component of the inertia matrix";
InertyFrame.IXYlabel.LongHelp                 ="Displays the Ixy component of the inertia matrix.";

InertyFrame.IXYeditor.Help                    ="Displays the Ixy component of the inertia matrix";
InertyFrame.IXYeditor.LongHelp                ="Displays the Ixy component of the inertia matrix.";

InertyFrame.IXZlabel.Title                    ="Ixz:";
InertyFrame.IXZlabel.Help                     ="Displays the Ixz component of the inertia matrix";
InertyFrame.IXZlabel.LongHelp                 ="Displays the Ixz component of the inertia matrix.";

InertyFrame.IXZeditor.Help                    ="Displays the Ixz component of the inertia matrix";
InertyFrame.IXZeditor.LongHelp                ="Displays the Ixz component of the inertia matrix.";

InertyFrame.IYXlabel.Title                    ="Iyx:";
InertyFrame.IYXlabel.Help                     ="Displays the Iyx component of the inertia matrix";
InertyFrame.IYXlabel.LongHelp                 ="Displays the Iyx component of the inertia matrix.";

InertyFrame.IYXeditor.Help                    ="Displays the Iyx component of the inertia matrix";
InertyFrame.IYXeditor.LongHelp                ="Displays the Iyx component of the inertia matrix.";

InertyFrame.IYYlabel.Title                    ="Iyy:";
InertyFrame.IYYlabel.Help                     ="Displays the Iyy component of the inertia matrix";
InertyFrame.IYYlabel.LongHelp                 ="Displays the Iyy component of the inertia matrix.";

InertyFrame.IYYeditor.Help                    ="Displays the Iyy component of the inertia matrix";
InertyFrame.IYYeditor.LongHelp                ="Displays the Iyy component of the inertia matrix.";

InertyFrame.IYZlabel.Title                    ="Iyz:";
InertyFrame.IYZlabel.Help                     ="Displays the Iyz component of the inertia matrix";
InertyFrame.IYZlabel.LongHelp                 ="Displays the Iyz component of the inertia matrix.";

InertyFrame.IYZeditor.Help                    ="Displays the Iyz component of the inertia matrix";
InertyFrame.IYZeditor.LongHelp                ="Displays the Iyz component of the inertia matrix.";

InertyFrame.IZXlabel.Title                    ="Izx:";
InertyFrame.IZXlabel.Help                     ="Displays the Izx component of the inertia matrix";
InertyFrame.IZXlabel.LongHelp                 ="Displays the Izx component of the inertia matrix.";

InertyFrame.IZXeditor.Help                    ="Displays the Izx component of the inertia matrix";
InertyFrame.IZXeditor.LongHelp                ="Displays the Izx component of the inertia matrix.";

InertyFrame.IZYlabel.Title                    ="Izy:";
InertyFrame.IZYlabel.Help                     ="Displays the Izy component of the inertia matrix";
InertyFrame.IZYlabel.LongHelp                 ="Displays the Izy component of the inertia matrix.";

InertyFrame.IZYeditor.Help                    ="Displays the Izy component of the inertia matrix";
InertyFrame.IZYeditor.LongHelp                ="Displays the Izy component of the inertia matrix.";

InertyFrame.IZZlabel.Title                    ="Izz:";
InertyFrame.IZZlabel.Help                     ="Displays the Izz component of the inertia matrix";
InertyFrame.IZZlabel.LongHelp                 ="Displays the Izz component of the inertia matrix.";

InertyFrame.IZZeditor.Help                    ="Displays the Izz component of the inertia matrix";
InertyFrame.IZZeditor.LongHelp                ="Displays the Izz component of the inertia matrix.";


// Granularity Mode (new R14)
OthersFrame.GranularityButton.Title           ="Only main bodies";
OthersFrame.GranularityButton.ShortHelp       ="Take only main part bodies into account";
OthersFrame.GranularityButton.Help            ="Take only main part bodies into account";
OthersFrame.GranularityButton.LongHelp        ="Take only main part bodies into account\nin calculation, else all bodies.";

OthersFrame.ThinPartChkButton.Title           ="Consider thin part attributes";
OthersFrame.ThinPartChkButton.ShortHelp       ="Take into account thin part attributes";
OthersFrame.ThinPartChkButton.Help            ="Take into account thin part attributes";
OthersFrame.ThinPartChkButton.LongHelp        ="Take into account thin part attributes in calculation.";

OthersFrame.WarningNeedRefreshLabel.Title     ="click Apply to relaunch calculation.";
OthersFrame.WarningVisuModeLabel1.Title       ="Parts in visualization mode have been detected.";
OthersFrame.WarningVisuModeLabel2.Title       ="For these parts, results are approximated, and take into account all their bodies.";
OthersFrame.WarningVisuModeLabel3.Title       ="Regarding materials defined inside parts, only densities saved in cgr are used.";

// drafting properties
//DraftingPropertiesFrame.Title                         ="Drafting properties";
//DraftingPropertiesFrame.Help                          ="Displays the drafting properties of the product";
//DraftingPropertiesFrame.LongHelp                      ="Drafting properties\nDisplays the drafting properties of the product.\n";
//
//DraftingPropertiesFrame.CutCheckButton.Title          ="Do not cut in section views";
//DraftingPropertiesFrame.UseCheckButton.Title          ="Do not use when projecting";
//DraftingPropertiesFrame.HiddenLineCheckButton.Title   ="Represented with hidden lines";
