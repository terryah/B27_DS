// COPYRIGHT DASSAULT SYSTEMES 1998
//===========================================================================
//
// CATRscShootingAnimationFrame
//
//===========================================================================

noAnimationLabel = "None";
noSimulateLabel = "None";

//--------------------------------------
// Time frame
//--------------------------------------
timeFrame.HeaderFrame.Global.Title = "Animation";

timeFrame.IconAndOptionsFrame.OptionsFrame.timeParametersFrame.animationLabel.Title = "Simulation:";
timeFrame.IconAndOptionsFrame.OptionsFrame.timeParametersFrame.animationCombo.Help = "Selects the simulation to render";
timeFrame.IconAndOptionsFrame.OptionsFrame.timeParametersFrame.animationCombo.ShortHelp = "Rendered Simulation";
timeFrame.IconAndOptionsFrame.OptionsFrame.timeParametersFrame.animationCombo.LongHelp =
"Defines the simulation to render.";

timeFrame.IconAndOptionsFrame.OptionsFrame.simulateFrame.simulateLabel.Title = "Animated objects";
timeFrame.IconAndOptionsFrame.OptionsFrame.simulateFrame.simulateList.Help = "Lists the animated objects";
timeFrame.IconAndOptionsFrame.OptionsFrame.simulateFrame.simulateList.ShortHelp = "Animated Objects";
timeFrame.IconAndOptionsFrame.OptionsFrame.simulateFrame.simulateList.LongHelp =
"Lists the animated objects.";

timeFrame.IconAndOptionsFrame.OptionsFrame.timeParametersFrame.durationLabel.Title = "Duration: ";
timeFrame.IconAndOptionsFrame.OptionsFrame.timeParametersFrame.durationSpinner.Help = "Defines animation duration";
timeFrame.IconAndOptionsFrame.OptionsFrame.timeParametersFrame.durationSpinner.ShortHelp = "Animation Duration";
timeFrame.IconAndOptionsFrame.OptionsFrame.timeParametersFrame.durationSpinner.LongHelp =
"Duration of complete animation, in seconds.";

timeFrame.IconAndOptionsFrame.OptionsFrame.timeParametersFrame.durationUnitLabel.Title = "second(s)";

timeFrame.IconAndOptionsFrame.OptionsFrame.timeParametersFrame.frequencyLabel.Title = "Frequency: ";

timeFrame.IconAndOptionsFrame.OptionsFrame.timeParametersFrame.frequencySpinner.Help = "Defines frame frequency";
timeFrame.IconAndOptionsFrame.OptionsFrame.timeParametersFrame.frequencySpinner.ShortHelp = "Frame Frequency";
timeFrame.IconAndOptionsFrame.OptionsFrame.timeParametersFrame.frequencySpinner.LongHelp =
"Animation frame frequency, in frames per second.";

timeFrame.IconAndOptionsFrame.OptionsFrame.timeParametersFrame.frequencyUnitLabel.Title = "frames/s";

timeFrame.IconAndOptionsFrame.OptionsFrame.timeParametersFrame.imagesCountLabel.Title = "Frame count:";
timeFrame.IconAndOptionsFrame.OptionsFrame.timeParametersFrame.imagesCountSpinner.Help = "Defines frame count";
timeFrame.IconAndOptionsFrame.OptionsFrame.timeParametersFrame.imagesCountSpinner.ShortHelp = "Frame Count";
timeFrame.IconAndOptionsFrame.OptionsFrame.timeParametersFrame.imagesCountSpinner.LongHelp =
"Number of frames replayed for complete animation.";

timeFrame.IconAndOptionsFrame.OptionsFrame.timeParametersFrame.computeLabel.Title = "Compute:";

timeFrame.IconAndOptionsFrame.OptionsFrame.timeParametersFrame.computeFrame.computeAllRadio.Title = "All frames";
timeFrame.IconAndOptionsFrame.OptionsFrame.timeParametersFrame.computeFrame.computeCustomRadio.Title = "From frame";


timeFrame.IconAndOptionsFrame.OptionsFrame.timeParametersFrame.computeFrame.computeStartSpinner.Help = "Defines start frame";
timeFrame.IconAndOptionsFrame.OptionsFrame.timeParametersFrame.computeFrame.computeStartSpinner.ShortHelp = "Start Frame";
timeFrame.IconAndOptionsFrame.OptionsFrame.timeParametersFrame.computeFrame.computeStartSpinner.LongHelp =
"Index of first frame to compute.";

timeFrame.IconAndOptionsFrame.OptionsFrame.timeParametersFrame.computeFrame.computeEndLabel.Title = "to ";
timeFrame.IconAndOptionsFrame.OptionsFrame.timeParametersFrame.computeFrame.computeEndSpinner.Help = "Defines end frame";
timeFrame.IconAndOptionsFrame.OptionsFrame.timeParametersFrame.computeFrame.computeEndSpinner.ShortHelp = "End Frame";
timeFrame.IconAndOptionsFrame.OptionsFrame.timeParametersFrame.computeFrame.computeEndSpinner.LongHelp =
"Index of last frame to compute.";

timeFrame.IconAndOptionsFrame.OptionsFrame.timeParametersFrame.computeFrame.computeStepLabel.Title = "every ";
timeFrame.IconAndOptionsFrame.OptionsFrame.timeParametersFrame.computeFrame.computeStepSpinner.Help = "Defines step between frames";
timeFrame.IconAndOptionsFrame.OptionsFrame.timeParametersFrame.computeFrame.computeStepSpinner.ShortHelp = "Step Between Frames";
timeFrame.IconAndOptionsFrame.OptionsFrame.timeParametersFrame.computeFrame.computeStepSpinner.LongHelp =
"Step between two computed frames.";

timeFrame.IconAndOptionsFrame.OptionsFrame.timeParametersFrame.computeFrame.computeStepUnitLabel.Title = "frame(s)";

timeFrame.IconAndOptionsFrame.OptionsFrame.timeParametersFrame.computeFrame.imagesComputedLabel.Title = "Computed frames: ";
