
// Constraint Commands
CATMmuConstraint.CATCom2DConstraint.Elem1.Message = "Select an element to be constrained";
CATMmuConstraint.CATCom2DConstraint.Elem2.Message = "Select another element";
CATMmuConstraint.CATCom2DConstraint.Elem2_Or_CstPos.Message = "Position the dimension or select another element";
CATMmuConstraint.CATCom2DConstraint.CstPos.Message = "Position the dimension";
CATMmuConstraint.CATCom2DConstraint.CstPos2.Message = "Position the dimension";
CATMmuConstraint.CATCom2DConstraint.SymElem_Or_CstPos.Message = "Position the dimension or select a symmetry line";
CATMmuConstraint.CATCom2DConstraint.SymElem.Message = "Select a symmetry line";
CATMmuConstraint.CATCom2DConstraint.MidPt_Or_CstPos.Message = "Position the dimension or select a point to be equidistant";
CATMmuConstraint.CATCom2DConstraint.MidPt.Message = "Select a point to be equidistant";
CATMmuConstraint.CATCom2DConstraint.AllowSymmetry.Message = "Allow symmetry line";
CATMmuConstraint.CATCom2DConstraint.UndoTitle = "Constraint";

CATMmuConstraint.CATCom2DConstraintPanel.Choice.Message = "Check a constraint in the dialog box";
CATMmuConstraint.CATCom2DConstraintPanel.TargetGeomState.Message = "Select an element for creating constraint(s) and select the constraint type.";
CATMmuConstraint.CATCom2DConstraintPanel.UndoTitle = "Constraint...";

CATMmuConstraint.ConstraintPanel.TargetGeomFrame.Title = "Create Multiple Constraints";

CATMmuConstraint.ConstraintPanel.TargetGeomFrame.TargetGeomCheckBtn.Title = "Target Element";
CATMmuConstraint.ConstraintPanel.TargetGeomFrame.TargetGeomCheckBtn.Help  = "Select the target element to create constraint(s)";
CATMmuConstraint.ConstraintPanel.TargetGeomFrame.TargetGeomCheckBtn.LongHelp  = "Select an element to create constraint(s) between the selected element(s) and the target element";

CATMmuConstraint.ConstraintPanel.TargetGeomFrame.TargetGeomSelector.DelTargetGeomCtxMenuItem.Title = "Clear Selection";

CATMmuConstraint.CATCom3DConstraint.Elem1.Message = "Select an element to be constrained";
CATMmuConstraint.CATCom3DConstraint.Elem2.Message = "Select another element";
CATMmuConstraint.CATCom3DConstraint.Elem2_Or_CstPos.Message = "Position the dimension or select another element";
CATMmuConstraint.CATCom3DConstraint.CstPos.Message = "Position the dimension";
CATMmuConstraint.CATCom3DConstraint.CstPos2.Message = "Position the dimension";
CATMmuConstraint.CATCom3DConstraint.SymElem_Or_CstPos.Message = "Position the dimension or select a symmetry line";
CATMmuConstraint.CATCom3DConstraint.SymElem.Message = "Select a symmetry line";
CATMmuConstraint.CATCom3DConstraint.MidPt_Or_CstPos.Message = "Position the dimension or select a point to be equidistant";
CATMmuConstraint.CATCom3DConstraint.MidPt.Message = "Select a point to be equidistant";
CATMmuConstraint.CATCom3DConstraint.UndoTitle = "Constraint...";

CATMmuConstraint.CATCom3DConstraintPanel.Choice.Message = "Check a constraint in the dialog box";
CATMmuConstraint.CATCom3DConstraintPanel.UndoTitle = "Constraint Defined in a Dialog Box...";

// Constraint Dialog Box
CATMmuConstraint.ConstraintPanel.Title = "Constraint Definition";
CATMmuConstraint.ConstraintPanel.Help = "Constraints available for the selected elements";
CATMmuConstraint.ConstraintPanel.LongHelp = "Check one of the constraints that are available on the selected elements.";

CATMmuConstraint.ConstraintPanel.Frame.Reference.Title = "Fix";
CATMmuConstraint.ConstraintPanel.Frame.Reference.Help = "Fixes the element size and location";
CATMmuConstraint.ConstraintPanel.Frame.Reference.LongHelp = "Fixes the element size and location.\nNevertheless, the endpoints can still be edited.";

CATMmuConstraint.ConstraintPanel.Frame.Distance.Title = "Distance";
CATMmuConstraint.ConstraintPanel.Frame.Distance.ShortHelp = "Distance constraint type";
CATMmuConstraint.ConstraintPanel.Frame.Distance.Help = "Creates a distance constraint between the selected elements";
CATMmuConstraint.ConstraintPanel.Frame.Distance.LongHelp = "Creates a distance constraint\nbetween the selected elements.";

CATMmuConstraint.ConstraintPanel.Frame.On.Title = "Coincidence";
CATMmuConstraint.ConstraintPanel.Frame.On.Help = "Creates a coincidence constraint between the selected elements";
CATMmuConstraint.ConstraintPanel.Frame.On.LongHelp = "Creates a coincidence constraint\nbetween the selected elements.";

CATMmuConstraint.ConstraintPanel.Frame.Concentric.Title = "Concentricity";
CATMmuConstraint.ConstraintPanel.Frame.Concentric.Help = "Creates a concentricity constraint between the selected elements";
CATMmuConstraint.ConstraintPanel.Frame.Concentric.LongHelp = "Creates a concentricity constraint\nbetween the selected elements.";

CATMmuConstraint.ConstraintPanel.Frame.Tangent.Title = "Tangency";
CATMmuConstraint.ConstraintPanel.Frame.Tangent.Help = "Creates a tangency constraint between the selected elements";
CATMmuConstraint.ConstraintPanel.Frame.Tangent.LongHelp = "Creates a tangency constraint between the selected elements";

CATMmuConstraint.ConstraintPanel.Frame.Length.Title = "Length";
CATMmuConstraint.ConstraintPanel.Frame.Length.Help = "Creates a length constraint on the selected element";
CATMmuConstraint.ConstraintPanel.Frame.Length.LongHelp = "Creates a length constraint\non the selected element.";

CATMmuConstraint.ConstraintPanel.Frame.Angle.Title = "Angle";
CATMmuConstraint.ConstraintPanel.Frame.Angle.Help = "Creates an angle constraint between the selected elements";
CATMmuConstraint.ConstraintPanel.Frame.Angle.LongHelp = "Creates an angle constraint\nbetween the selected elements.";

CATMmuConstraint.ConstraintPanel.Frame.AnglePlanar.Title = "Angle with axis";
CATMmuConstraint.ConstraintPanel.Frame.AnglePlanar.Help = "Creates an angle with axis constraint between the selected elements";
CATMmuConstraint.ConstraintPanel.Frame.AnglePlanar.LongHelp = "Creates an angle with axis constraint\nbetween the selected elements.";

CATMmuConstraint.ConstraintPanel.Frame.Parallel.Title = "Parallelism";
CATMmuConstraint.ConstraintPanel.Frame.Parallel.Help = "Creates a parallelism constraint between the selected elements";
CATMmuConstraint.ConstraintPanel.Frame.Parallel.LongHelp = "Creates a parallelism constraint\nbetween the selected elements.";

CATMmuConstraint.ConstraintPanel.Frame.ParallelAxis.Title = "Vertical";
CATMmuConstraint.ConstraintPanel.Frame.ParallelAxis.Help = "Creates a vertical constraint for the selected element";
CATMmuConstraint.ConstraintPanel.Frame.ParallelAxis.LongHelp = "Creates a vertical constraint\nfor the selected element.";

CATMmuConstraint.ConstraintPanel.Frame.Perpend.Title = "Perpendicular";
CATMmuConstraint.ConstraintPanel.Frame.Perpend.Help = "Creates a perpendicular constraint between the selected elements";
CATMmuConstraint.ConstraintPanel.Frame.Perpend.LongHelp = "Creates a perpendicular constraint\nbetween the selected elements.";

CATMmuConstraint.ConstraintPanel.Frame.PerpendAxis.Title = "Horizontal";
CATMmuConstraint.ConstraintPanel.Frame.PerpendAxis.Help = "Creates an horizontal constraint\nfor the selected element";
CATMmuConstraint.ConstraintPanel.Frame.PerpendAxis.LongHelp = "Creates an horizontal\nconstraint on the selected element.";

CATMmuConstraint.ConstraintPanel.Frame.Radius.Title = "Radius / Diameter";
CATMmuConstraint.ConstraintPanel.Frame.Radius.Help = "Creates a radius constraint or diameter constraint between the selected elements";
CATMmuConstraint.ConstraintPanel.Frame.Radius.LongHelp = "Creates a radius constraint or diameter\nconstraint between the selected elements.";

CATMmuConstraint.ConstraintPanel.Frame.Symmetry.Title = "Symmetry";
CATMmuConstraint.ConstraintPanel.Frame.Symmetry.Help = "Creates a symmetry constraint\nfor the selected elements";
CATMmuConstraint.ConstraintPanel.Frame.Symmetry.LongHelp = "Creates a symmetry constraint\nwith the selected elements.";
CATMmuConstraint.ConstraintPanel.Frame.AllowSymmetry.Title = "Allow symmetry line";
CATMmuConstraint.ConstraintPanel.Frame.AllowSymmetry.Help = "Creates a symmetry constraint for the selected elements";

CATMmuConstraint.ConstraintPanel.Frame.MiddlePoint.Title = "Midpoint";
CATMmuConstraint.ConstraintPanel.Frame.MiddlePoint.Help = "Creates a midpoint constraint for the selected elements";
CATMmuConstraint.ConstraintPanel.Frame.MiddlePoint.LongHelp = "Creates a midpoint constraint\nfor the selected elements.";

CATMmuConstraint.ConstraintPanel.Frame.EquidistantPoint.Title = "Equidistant point";
CATMmuConstraint.ConstraintPanel.Frame.EquidistantPoint.Help = "Creates an equidistant point constraint for the selected elements";
CATMmuConstraint.ConstraintPanel.Frame.EquidistantPoint.LongHelp = "Creates an equidistant point constraint\nfor the selected elements.";

CATMmuConstraint.ConstraintPanel.Frame.Chamfer.Title = "Chamfer";
CATMmuConstraint.ConstraintPanel.Frame.Chamfer.Help = "Creates a chamfer constraint between the selected elements";
CATMmuConstraint.ConstraintPanel.Frame.Chamfer.LongHelp = "Creates a chamfer constrain\nbetween the selected elements.";

CATMmuConstraint.ConstraintPanel.Frame.Horizontal.Title = "Horizontal";
CATMmuConstraint.ConstraintPanel.Frame.Horizontal.Help = "Creates an horizontal constraint on the selected element";
CATMmuConstraint.ConstraintPanel.Frame.Horizontal.LongHelp = "Creates an horizontal constraint\non the selected element.";

CATMmuConstraint.ConstraintPanel.Frame.Vertical.Title = "Vertical";
CATMmuConstraint.ConstraintPanel.Frame.Vertical.Help = "Creates a vertical constraint on the selected element";
CATMmuConstraint.ConstraintPanel.Frame.Vertical.LongHelp = "Creates a vertical constraint\non the selected element.";

CATMmuConstraint.ConstraintPanel.Frame.MajorRadius.Title = "Semimajor axis";
CATMmuConstraint.ConstraintPanel.Frame.MajorRadius.Help = "Creates a semimajor axis constraint on the selected ellipse";
CATMmuConstraint.ConstraintPanel.Frame.MajorRadius.LongHelp = "Creates a semimajor axis constraint\non the selected ellipse.";

CATMmuConstraint.ConstraintPanel.Frame.MinorRadius.Title = "Semiminor axis";
CATMmuConstraint.ConstraintPanel.Frame.MinorRadius.Help = "Creates a semiminor axis constraint for the selected ellipse";
CATMmuConstraint.ConstraintPanel.Frame.MinorRadius.LongHelp = "Creates a semiminor axis constraint\nfor the selected ellipse.";

CATMmuConstraint.ConstraintPanel.Frame.Fixed.Title = "Fix";
CATMmuConstraint.ConstraintPanel.Frame.Fixed.Help = "Fixes the element size and location";

CATMmuConstraint.ConstraintPanel.Frame.Unfixed.Title = "Unfix";
CATMmuConstraint.ConstraintPanel.Frame.Unfixed.Help = "Unfixes the element size and location";

CATMmuConstraint.ConstraintPanel.Frame.Constrained.Title = "Driving";
CATMmuConstraint.ConstraintPanel.Frame.Constrained.Help = "Sets the constraint mode to 'driving'";

CATMmuConstraint.ConstraintPanel.Frame.Measured.Title = "Reference";
CATMmuConstraint.ConstraintPanel.Frame.Measured.Help = "Sets the constraint mode to 'reference'";

CATMmuConstraint.ConstraintPanel.Frame.NoMeasureDir.Title = "No Measure Direction";
CATMmuConstraint.ConstraintPanel.Frame.NoMeasureDir.Help = "Reset the constraint measure direction";

CATMmuConstraint.ConstraintPanel.Frame.HorMeasureDir.Title = "Horizontal Measure Direction";
CATMmuConstraint.ConstraintPanel.Frame.HorMeasureDir.Help = "Sets the constraint measure direction to 'horizontal'";

CATMmuConstraint.ConstraintPanel.Frame.VerMeasureDir.Title = "Vertical Measure Direction";
CATMmuConstraint.ConstraintPanel.Frame.VerMeasureDir.Help = "Sets the constraint measure direction to 'vertical'";

CATMmuConstraint.ConstraintPanel.Frame.XMeasureDir.Title = "X Axis Measure Direction";
CATMmuConstraint.ConstraintPanel.Frame.XMeasureDir.Help = "Sets the constraint measure direction to 'X axis'";

CATMmuConstraint.ConstraintPanel.Frame.YMeasureDir.Title = "Y Axis Measure Direction";
CATMmuConstraint.ConstraintPanel.Frame.YMeasureDir.Help = "Sets the constraint measure direction to 'Y axis'";

CATMmuConstraint.ConstraintPanel.Frame.ZMeasureDir.Title = "Z Axis Measure Direction";
CATMmuConstraint.ConstraintPanel.Frame.ZMeasureDir.Help = "Sets the constraint measure direction to 'Z axis'";

CATMmuConstraint.ConstraintPanel.Frame.Radius_x1.Title = "Diameter";
CATMmuConstraint.ConstraintPanel.Frame.Radius_x1.Help = "Sets the constraint to Diameter dimension";

CATMmuConstraint.ConstraintPanel.Frame.Radius_x2.Title = "Radius";
CATMmuConstraint.ConstraintPanel.Frame.Radius_x2.Help = "Sets the constraint to radius dimension";

CATMmuConstraint.ConstraintPanel.Frame.SetPosition.Title = "Position Dimension";
CATMmuConstraint.ConstraintPanel.Frame.SetPosition.Help = "Allows you to position the dimension";

CATMmuConstraint.ConstraintPanel.Frame.Delete.Title = "Delete";
CATMmuConstraint.ConstraintPanel.Frame.Delete.Help = "Deletes the constraint";

CATMmuConstraint.ConstraintPanel.Frame.SwapLocation.Title = "Swap Location";
CATMmuConstraint.ConstraintPanel.Frame.SwapLocation.Help = "Swaps element location";

CATMmuConstraint.ConstraintPanel.Frame.CurvilinearDistance.Title = "Curvilinear distance";
CATMmuConstraint.ConstraintPanel.Frame.CurvilinearDistance.Help = "Creates a curvilinear distance constraint for the selected elements";
CATMmuConstraint.ConstraintPanel.Frame.CurvilinearDistance.LongHelp = "Creates a curvilinear distance constraint\nfor the selected elements.";

// Visu de Contraintes

CATMmuConstraint.ConstraintPanel.Frame.LeaderDisplay.Title = "Display with Leader";
CATMmuConstraint.ConstraintPanel.Frame.LeaderDisplay.Help = "Sets a leader to the corresponding constraint";

CATMmuConstraint.ConstraintPanel.Frame.NoLeaderDisplay.Title = "Display without Leader";
CATMmuConstraint.ConstraintPanel.Frame.NoLeaderDisplay.Help = "Removes the leader associated to the constraint";

CATMmuConstraint.ConstraintPanel.Frame.Value_Name_Formula_Display.Title = "Name/Value/Formula Display";
CATMmuConstraint.ConstraintPanel.Frame.Value_Name_Formula_Display.Help = "Sets the constraint display to 'name=value - formula'";

CATMmuConstraint.ConstraintPanel.Frame.Value_Name_Display.Title = "Name/Value Display";
CATMmuConstraint.ConstraintPanel.Frame.Value_Name_Display.Help = "Sets the constraint display to 'name=value'";

CATMmuConstraint.ConstraintPanel.Frame.Name_Display.Title = "Name Display";
CATMmuConstraint.ConstraintPanel.Frame.Name_Display.Help = "Sets the constraint display to 'name'";

CATMmuConstraint.ConstraintPanel.Frame.Value_Display.Title = "Value Display";
CATMmuConstraint.ConstraintPanel.Frame.Value_Display.Help = "Sets the constraint display to 'value'";

// Fin de Visu de Contraintes


// Warnings
CATMmuConstraint.ConstraintPanel.NoSelectedElement.Message = " Invalid selection \n Select valid element(s) to be constrained \n before activating the command.";
CATMmuConstraint.ConstraintPanel.NoPossibleConstraint.Message = " Unavailable constraint. \n Select new element(s).";
CATMmuConstraint.ConstraintPanel.NotImported.Message = " External element(s) can't be imported. \n Select new element(s).";
CATMmuConstraint.ConstraintPanel.NoFeature.Message = " External element(s) can't be imported. \n Select new element(s).";
CATMmuConstraint.ConstraintPanel.No2DConversion.Message = " 3D element(s) without projection result or \n intersection result in the current sketch. \n Select new element(s).";
CATMmuConstraint.ConstraintPanel.No3DConversion.Message = " Sketcher element(s) not converted in 3D. \n Select new element(s).";
Warning="Warning";
AlreadyExist="A similar constraint already exist between the two selected elements.";
TemporaryConstraint="Constraints created are temporary. If you want make it permanent, you need to activate constraints creation switches before clicking the OK button.";
NoSwapConstraint="This constraint cannot be swaped because it is a reference.";

// Change Sketch Support Command

//CATMmuConstraint.CATComChangeSketchSupport.Sketch.Message = "Select a sketch which support is to be changed";
//CATMmuConstraint.CATComChangeSketchSupport.Plane.Message = "Select the new sketch support plane";
//CATMmuConstraint.CATComChangeSketchSupport.AvoidLoop.Message = "Choose another plane whose parents are not defined in the sketch";

// Cst options for elements outside current sketch
intersection="- Intersection";
projection="- Projection";
