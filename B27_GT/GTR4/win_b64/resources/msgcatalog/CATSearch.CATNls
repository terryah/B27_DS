//
// ------------------------------------------------------------
// |  NLS Message Catalog
// |  Frame work: CATIAApplicationFrame
// |  Module : Search.m
// ------------------------------------------------------------
//
// ------------------------------------------------------------
// | Query Panel                  
// ------------------------------------------------------------

// --------------------
//  Window and Tab Pages titles 
// --------------------
Title    = "Search"; 
ContainerFrm.TabContainer.FavoriteFrame.Title   = "Favorites";
ContainerFrm.TabContainer.GeneralFrame.Title    = "General";
ContainerFrm.TabContainer.AdvancedFrame.Title   = "Advanced";

// --------------------
// BOTTOM FRAME
// --------------------
// Context/Scope Label & Combo 
ContextAndHistoryFrm.ContextLabel.Title = "Look:";
ContextAndHistoryFrm.ContextLabel.LongHelp = 
"Executes the search according to the following scope:

Everywhere: searches throughout the whole document.

In 'Element': searches only among the objects of 'Element' that 
              can be created using the current workbench.

From 'Element' to bottom: searches throughout 'Element', from top to bottom.

Visible on screen: searches from objects currently visible in the active window.

From current selection: searches from objects you selected to the bottom.

From last scope: searches from scope of objects of the last search (faster).

From search results: searches among the selected objects already found.";

ContextAndHistoryFrm.ContextCombo.LongHelp = 
"Executes the search according to the following scope:

Everywhere: searches throughout the whole document.

In 'Element': searches only among the objects of 'Element' that 
              can be created using the current workbench.

From 'Element' to bottom: searches throughout 'Element', from top to bottom.

Visible on screen: searches from objects currently visible in the active window.

From current selection: searches from objects you selected to the bottom.

From last scope: searches from scope of objects of the last search (faster).

From search results: searches among the selected objects already found.";

// PreHighlight Button 
ContextAndHistoryFrm.PreHighlightButton.Title = "Pre-highlight"; 
ContextAndHistoryFrm.PreHighlightButton.LongHelp =
"Pre-highlights elements selected in the Search results list.";

// Query Label & Combo 
ContextAndHistoryFrm.QueryLabel.Title = "Query:"; 
ContextAndHistoryFrm.QueryLabel.LongHelp =
"Lists all the generated queries in the current search session.
Selecting a query will relaunch the search.";

ContextAndHistoryFrm.QueryCombo.LongHelp =
"Lists all the generated queries in the current search session.
Selecting a query will relaunch the search.";

// List of selected objects 
ListMainFrame.ListFrm.ObjectFound   = "/P1 object found";
ListMainFrame.ListFrm.ObjectsFound  = "/P1 objects found";
ListMainFrame.ListFrm.ObjectPreselected   = "/P1 pre-selected";
//ListMainFrame.ListFrm.LabelsFrame.AlphabeticalButton.Title    = "Alphabetical sorting";
//ListMainFrame.ListFrm.LabelsFrame.AlphabeticalButton.ShortHelp = 
//"Sorts the list alphabetically";
ListMainFrame.ListFrm.NavigationFrame.TopButton.Title = "|<<";
ListMainFrame.ListFrm.NavigationFrame.TopButton.ShortHelp = "First Page";
ListMainFrame.ListFrm.NavigationFrame.UpButton.Title = "<";
ListMainFrame.ListFrm.NavigationFrame.UpButton.ShortHelp = "Previous Page";
ListMainFrame.ListFrm.NavigationFrame.PageEditor.ShortHelp = "Page Number";
ListMainFrame.ListFrm.NavigationFrame.DownButton.Title = ">";
ListMainFrame.ListFrm.NavigationFrame.DownButton.ShortHelp = "Next Page";
ListMainFrame.ListFrm.NavigationFrame.BottomButton.Title = ">>|";
ListMainFrame.ListFrm.NavigationFrame.BottomButton.ShortHelp = "Last Page";
ListMainFrame.ListFrm.NavigationFrame.DisplayAllButton.Title = "Display all";
ListMainFrame.ListFrm.NavigationFrame.DisplayAllButton.ShortHelp = "Displays all results on one page";
ListMainFrame.ListFrm.Results       = "results /P1 to /P2";
ListMainFrame.ListFrm.Title1 = "Name";
ListMainFrame.ListFrm.Title2 = "Path";
ListMainFrame.ListFrm.LongHelp = 
"The objects found are displayed in a list.
All the objects are selected by default.
Select or deselect objects by clicking. 
Click OK or Select to confirm the search results.";

// Buttons
SearchDialog.APPLYButton.Title  = "Select";
SearchDialog.CANCELButton.Title = "Close";

// include topology button 
BottomPart.ContextAndHistoryFrm.OptionsFrm.TopologyButton.Title = "Include topology";
BottomPart.ContextAndHistoryFrm.OptionsFrm.TopologyButton.ShortHelp = 
"Includes topological elements in the Search result";
BottomPart.ContextAndHistoryFrm.OptionsFrm.TopologyButton.LongHelp = 
"Usually, topological elements can be found only when searching for them specifically
through a query like \"Topology.*/Face/Edge/Vertex\".
Using this option allows you to find topological elements in any query, as any other element";

// published elements only button 
BottomPart.ContextAndHistoryFrm.OptionsFrm.PublicationsButton.Title = "Published elements only";
BottomPart.ContextAndHistoryFrm.OptionsFrm.PublicationsButton.ShortHelp = 
"Searches only for published elements";
BottomPart.ContextAndHistoryFrm.OptionsFrm.PublicationsButton.LongHelp = 
"Searches only for published elements.";

// deep search button 
BottomPart.ContextAndHistoryFrm.OptionsFrm.DeepSearchButton.Title = "Deep search";
BottomPart.ContextAndHistoryFrm.OptionsFrm.DeepSearchButton.ShortHelp = 
"Searches also for elements in visualization mode";
BottomPart.ContextAndHistoryFrm.OptionsFrm.DeepSearchButton.LongHelp = 
"This option is available only if the \"Work with the cache system\" option is set.
This option is active by default if the \"Automatic switch to design mode\" option is set.
Using this option allows you to find elements in documents in visu mode.";

// search button 
BottomPart.ContextAndHistoryFrm.SearchButton.ShortHelp = "Search";
BottomPart.ContextAndHistoryFrm.SearchButton.LongHelp = 
"Click the Search button to get the list of objects you are searching for.";

// search and select button 
BottomPart.ContextAndHistoryFrm.SearchAndSelectButton.ShortHelp = "Search and Select";
BottomPart.ContextAndHistoryFrm.SearchAndSelectButton.LongHelp = 
"Click the Search and Select button to get and select the list of objects you are searching for,
and close the Search dialog box.";

// favorites button 
BottomPart.ContextAndHistoryFrm.FavoritesButton.ShortHelp = "Add to favorites...";
BottomPart.ContextAndHistoryFrm.FavoritesButton.LongHelp = 
"Adds the query shown into the favorites.";

// --------------------
// FAVORITES FRAME
// --------------------
LabelAndButtonsFrame.FavoriteLabel.Title = "Favorites list";
LabelAndButtonsFrame.ButtonsFrame.UpButton.ShortHelp = 
"Moves up the selected query";
LabelAndButtonsFrame.ButtonsFrame.DownButton.ShortHelp = 
"Moves down the selected query";
LabelAndButtonsFrame.ButtonsFrame.DeleteButton.ShortHelp = 
"Deletes the selected query";
FavoriteMultiList.ShortHelp = 
"To launch a query, double-click it.
To edit a query name or syntax, slowly click twice the field to be modified.";
EditionDialog.Title = "Create a favorite query...";
EditionDialog.EditionFrame.NameLabel.Title = "Name";
EditionDialog.EditionFrame.ContentLabel.Title = "Query";
FavoriteFrame.EditionDialog.ConfirmationWindow.Title = "Confirmation";
ConfirmationWindow.Overwriting = "The previous query of the same name\n is going to be overwritten.";
ConfirmationWindow.Deletion = "Are you sure you want to delete the selected query or queries?";
FavoriteFrame.Name  = "Name";
FavoriteFrame.Query = "Query";
FavoriteFrame.Warning.Title = "Warning";
FavoriteFrame.Warning.NoSelection.Text  = "Select a favorite query before searching";
FavoriteFrame.Warning.SeveralSelections.Text  = "Select one and only one favorite query \nbefore searching";

// --------------------
// GENERAL FRAME
// --------------------
// query on Name 
SimpleFrame.NameLabel.Title = "Name:";
SimpleFrame.NameEditor.ShortHelp = 
"Enter the name of the objects you need to select";
SimpleFrame.NameEditor.LongHelp = 
"Use the character * to replace 0 to several characters.
If this criterion is meaningless, leave the default '*'.";

SimpleFrame.AsDisplayedInGraphButton.Title = "As displayed in graph";
SimpleFrame.AsDisplayedInGraphButton.ShortHelp =
"Click to search for names as displayed in the specification tree";
SimpleFrame.AsDisplayedInGraphButton.LongHelp =
"Click this option if you are searching for the 
names as displayed in the specification tree.";

SimpleFrame.CaseSensitivityCheckButton.Title = "Case sensitive";
SimpleFrame.CaseSensitivityCheckButton.ShortHelp = 
"Click when lower/UPPER case is meaningful";
SimpleFrame.CaseSensitivityCheckButton.LongHelp = 
"Click 'Case sensitive' if the case (lower/UPPER)
is meaningful in the text being typed in.";

// query on Type
SimpleFrame.AllWorkbenches = "*";
SimpleFrame.IntroductionLabel.Title = "Type:";

SimpleFrame.WorkbenchTitle.Title = "Workbench";
SimpleFrame.WorkbenchCombo.ShortHelp =
"Choose or enter the workbench in which you want to perform your search";
SimpleFrame.WorkbenchCombo.LongHelp = 
"The displayed types depend on the working context. 
Choose a context or select '*' to get all the loaded types."; 

SimpleFrame.TypeTitle.Title = "Type";
SimpleFrame.TypeCombo.ShortHelp =
"Choose or enter the type of objects you are searching for";
SimpleFrame.TypeCombo.LongHelp = 
"Use the character * to replace 0 to several characters.
Case is not sensitive.
If this criterion is meaningless, leave the default '*'.";

SimpleFrame.LoadTypesButton.Title = "Load all types";
SimpleFrame.LoadTypesButton.ShortHelp = 
"Loads all the types available for all workbenches";
SimpleFrame.LoadTypesButton.LongHelp = 
"Types corresponding to a workbench are loaded when this
workbench is selected. Click this button to make all the types available.";

// query on Graphic Properties
SimpleFrame.ColorLabel.Title = "Color:";
SimpleFrame.ColorCombo.ComboColor.LongHelp = 
"If this criterion is meaningless, leave the default '*'.";

SimpleFrame.MoreButton.Title    = "More...";
SimpleFrame.MoreButton.LongHelp = 
"Shows more graphic properties.";
SimpleFrame.LessButton.Title    = "Less...";
SimpleFrame.LessButton.LongHelp = 
"Shows less graphic properties.";

SimpleFrame.IntroLabel.Title = "Graphics:";
SimpleFrame.GeneralLabel.Title = "General properties";
SimpleFrame.SpecialLabel.Title = "Line properties";
SimpleFrame.PointLabel.Title = "Point properties";

SimpleFrame.LayerLabel.Title = "Layer";
SimpleFrame.LayerCombo.ShortHelp = 
"Choose the layer of the objects to be searched";
SimpleFrame.LayerCombo.LongHelp = 
"Choose the layer of the objects to be searched.
If this criterion is meaningless, leave the default '*'.";

SimpleFrame.VisibilityLabel.Title = "Visibility";
SimpleFrame.VisibilityCombo.ShortHelp = 
"Choose the visibility of the objects to be searched";
SimpleFrame.VisibilityCombo.LongHelp = 
"Choose the visibility of the objects to be searched.
The 'Visible' and 'Hidden' values are relative to the visibility attribute of an object (from a data point of view).
The 'Shown' and 'Invisible' values are relative to the real visibility of an object (from the display point of view).
An object is said to be 'Shown' if it is shown on the screen: the object as well as each of its parents have the \"Visible\" attribute.
An object is said to be 'Invisible' if it is not shown on the screen: the object or any of its parents have the \"Hidden\" attribute.
If this criterion is meaningless, leave the default '*'.";

SimpleFrame.DashedLabel.Title = "Dashed";
SimpleFrame.DashedCombo.ShortHelp = 
"Choose the type of line for the objects to be searched";
SimpleFrame.DashedCombo.LongHelp = 
"Choose the type of line for the objects to be searched.
If this criterion is meaningless, leave the default '*'.";

SimpleFrame.WeightLabel.Title = "Weight";
SimpleFrame.WeightCombo.ShortHelp = 
"Choose the line thickness for the objects to be searched";
SimpleFrame.WeightCombo.LongHelp = 
"Choose the line thickness for the objects to be searched.
If this criterion is meaningless, leave the default '*'.";

SimpleFrame.SymbolLabel.Title = "Symbol";
SimpleFrame.SymbolCombo.ShortHelp = 
"Choose the point symbol for the objects to be searched";
SimpleFrame.SymbolCombo.LongHelp = 
"Choose the point symbol for the objects to be searched.
If this criterion is meaningless, leave the default '*'.";

// --------------------
// ADVANCED FRAME
// --------------------
CombosAndButtonsFrame.CombosFrame.WorkbenchLabel.Title = "Workbench";
CombosAndButtonsFrame.CombosFrame.TypeLabel.Title      = "Type";
CombosAndButtonsFrame.CombosFrame.AttributeLabel.Title = "Attribute";

CombosAndButtonsFrame.CombosFrame.WorkbenchCombo.ShortHelp = 
"Choose or enter the name of the workbench containing 
the types and/or attributes you are searching for";
CombosAndButtonsFrame.CombosFrame.WorkbenchCombo.LongHelp = 
"Choose or enter the name of the workbench containing 
the types and/or attributes you are searching for.";

CombosAndButtonsFrame.CombosFrame.TypeCombo.ShortHelp = 
"Choose or enter the name of the type you are searching for";
CombosAndButtonsFrame.CombosFrame.TypeCombo.LongHelp = 
"Choose or enter the name of the type you are searching for.";

CombosAndButtonsFrame.CombosFrame.AttributeCombo.ShortHelp = 
"Choose or enter the name of the attribute you are searching for";
CombosAndButtonsFrame.CombosFrame.AttributeCombo.LongHelp = 
"Choose or enter the name of the attribute you are searching for.";

CombosAndButtonsFrame.CombosFrame.LoadTypesButton.Title = "Load all types";
CombosAndButtonsFrame.CombosFrame.LoadTypesButton.ShortHelp = 
"Loads types for all workbenches";
CombosAndButtonsFrame.CombosFrame.LoadTypesButton.LongHelp = 
"Types corresponding to a workbench are loaded when this
workbench is selected. Click this button to make all the types
available.";

CombosAndButtonsFrame.ButtonsFrame.AndButton.Title = "And";
CombosAndButtonsFrame.ButtonsFrame.OrButton.Title = "Or";
CombosAndButtonsFrame.ButtonsFrame.ExceptButton.Title = "Except";
CombosAndButtonsFrame.ButtonsFrame.AndButton.ShortHelp =
"Adds the And operator (&) between queries";
CombosAndButtonsFrame.ButtonsFrame.OrButton.ShortHelp = 
"Adds the Or operator (+) between queries";
CombosAndButtonsFrame.ButtonsFrame.ExceptButton.ShortHelp = 
"Adds the Except operator (-) between queries";

CompositionEditorFrame.EditorLabel.Title  = "Composed query";
CompositionEditorFrame.EditorButton.ShortHelp = "Erases the composed query";
CompositionEditorFrame.EditorButton.LongHelp = "Erases the whole composed query";
CompositionEditorFrame.Editor.ShortHelp    = 
"Compose your query here using
the +, -, & operators,...";
CompositionEditorFrame.Editor.LongHelp    = 
"Compose your query in this space.
You may only use the +, -, & operators as well as parenthesis.";

AttributeDialog.Title = "Attribute Criterion";

AdvancedFrame.NotificationClash.Title = "Clash";
AdvancedFrame.NotificationClashWk.Text = 
"Information:
The selected type belongs to more than one workbench:\n"; 
AdvancedFrame.NotificationClashTp.Text = 
"Warning:
The selected attribute belongs to more than one type:\n";
AdvancedFrame.NotificationClashWkTp.Text = 
"Warning:
The selected attribute belongs to more than one workbench/type couple:\n";
AdvancedFrame.NotificationMistake.Title = "Mistake"; 
AdvancedFrame.NotificationMistake.Text = 
"Error in the input string:
missing or extra parenthesis, typos, etc.";
AdvancedFrame.NotificationTypeProblem.Text.Title = "Type problem";
AdvancedFrame.NotificationTypeProblem.Text = 
"The selected type is not defined 
in this workshop/workbench.";

//Messages if the query is not correct and no criterion can be generated
ForNLSQuery = "If the query is NLS, here are the following mistakes:";
ForTFQuery =  "If the query is trans-format, here are the following mistakes:";

//Messages to kill the acquisition agent
SelectionAgent.Disabling.Title = "Stop Acquisition";
SelectionAgent.Disabling.Text1  = "An acquisition agent is active.";
SelectionAgent.Disabling.Text2  = "Click Close to return to";
SelectionAgent.Disabling.Text3  = "the Search dialog box without acquiring.";
SelectionAgent.Disabling.Text2bis  = "Click Close to stop acquisition.";

// V4Model Types
V4Model.Workshop = "CATIA V4"; 

// Topology Types
Topology.Workshop = "Topology";
CGMFace          = "Face";
CGMEdge          = "Edge";
CGMVertex        = "Vertex";
CGMTopology      = "Topology";

// --------------------
// ATTRIBUTE FRAME
// --------------------
AttributeFrame.DimensionReminder.Title = 
"The attribute is of dimensioned type. A unit must be specified.";
