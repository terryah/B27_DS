// COPYRIGHT DASSAULT SYSTEMES 1998
//===========================================================================
//
// CATRscLightIlluminationPropertyFrame
//
//===========================================================================

typeSepFrame.typeSepLabel.Title = "Source";
typeSepFrame.typeSepLabel.Help = "Sets light source type";
typeSepFrame.typeSepLabel.ShortHelp = "Light Source Type";
typeSepFrame.typeSepLabel.LongHelp =
"Changes the light source type.";

typeLabel.Title = "Type";
typeLabel.Help = "Defines the light source type";
typeLabel.ShortHelp = "Light Source Type";
typeLabel.LongHelp =
"Defines the light source type.";

typeFrame.typeBorderFrame.typeCombo.Help = "Defines the light source type";
typeFrame.typeBorderFrame.typeCombo.ShortHelp = "Light Source Type";
typeFrame.typeBorderFrame.typeCombo.LongHelp =
"Defines the light source type.";

SpotLight = "Spot";
PointLight = "Point";
DirectionalLight = "Directional";

angleLabel.Title = "Angle";
angleLabel.Help = "Defines the spot angle";
angleLabel.ShortHelp = "Spot Angle";
angleLabel.LongHelp =
"Defines the angle of the spot light.";

angleWheel.Title = "";
angleWheel.Help = "Defines the spot angle";
angleWheel.ShortHelp = "Spot Angle";
angleWheel.LongHelp =
"Defines the angle of the spot light.";

colorSepFrame.colorSepLabel.Title = "Color";
colorSepFrame.colorSepLabel.Help = "Sets light color and intensity";
colorSepFrame.colorSepLabel.ShortHelp = "Light Color and Intensity";
colorSepFrame.colorSepLabel.LongHelp =
"Changes the light color and intensity.";

colorLabel.Title = "Color";
colorLabel.Help = "Sets light color";
colorLabel.ShortHelp = "Light Color";
colorLabel.LongHelp =
"Changes the light color.";

colorSlider.Title = "";
colorSlider.Help = "Sets light color";
colorSlider.ShortHelp = "Light Color";
colorSlider.LongHelp =
"Changes the color of the light.";

intensityLabel.Title = "Intensity";
intensityLabel.Help = "Defines the light intensity";
intensityLabel.ShortHelp = "Light intensity";
intensityLabel.LongHelp =
"Defines the intensity of the light.";

intensitySlider.Help = "Defines the light intensity";
intensitySlider.ShortHelp = "Light intensity";
intensitySlider.LongHelp =
"Defines the intensity of the light.";

attenuationSepFrame.attenuationSepLabel.Title = "Attenuation";
attenuationSepFrame.attenuationSepLabel.Help = "Defines lighting attenuation";
attenuationSepFrame.attenuationSepLabel.ShortHelp = "Lighting Attenuation";
attenuationSepFrame.attenuationSepLabel.LongHelp =
"Changes the lighting attenuation.";

falloffLabel.Title = "Falloff";
falloffLabel.Help = "Defines the lighting falloff";
falloffLabel.ShortHelp = "Lighting Falloff";
falloffLabel.LongHelp =
"Defines the lighting attenuation type:
- None: the illumination is constant and infinite.
- Linear: the illumination starts to decrease linearly
at a certain distance (see 'Start Ratio') and is null at
the attenuation end.
- Realistic: the illumination decreases with the square
of the distance to the light center.
Note: For realistic falloff the light is infinite and
the end distance represents the limit where the lighting 
is negligible.";

falloffFrame.falloffBorderFrame.falloffCombo.Help = "Defines the lighting falloff";
falloffFrame.falloffBorderFrame.falloffCombo.ShortHelp = "Lighting Falloff";
falloffFrame.falloffBorderFrame.falloffCombo.LongHelp =
"Defines the lighting attenuation type:
- None: the illumination is constant and infinite.
- Linear: the illumination starts to decrease linearly
at a certain distance (see 'Start Ratio') and is null at
the attenuation end.
- Realistic: the illumination decreases with the square
of the distance to the light center.
Note: For realistic falloff the light is infinite and
the end distance represents the limit where the lighting 
is negligible.";

FalloffNone = "None";
FalloffInverse = "Linear";
FalloffInverseSquare = "Realistic";

endLabel.Title = "End";
endLabel.Help = "Defines the attenuation end";
endLabel.ShortHelp = "Attenuation End";
endLabel.LongHelp =
"Defines the distance to the center from which
the light illumination is null or negligible.";

endWheel.Title = "";
endWheel.Help = "Defines the attenuation end";
endWheel.ShortHelp = "Attenuation End";
endWheel.LongHelp =
"Defines the distance to the center from which
the light illumination is null or negligible.";

attenuationStartLabel.Title = "Start Ratio";
attenuationStartLabel.Help = "Defines the attenuation start ratio";
attenuationStartLabel.ShortHelp = "Attenuation Start Ratio";
attenuationStartLabel.LongHelp =
"Defines as a ratio from the center to the target the distance 
from which the light starts to attenuate.
ex: 0: the attenuation starts from the center,
    0.5: the attenuation starts from the middle,
    1: the attenuation starts from the end (i.e. no attenuation).";

attenuationStartSlider.Title = "";
attenuationStartSlider.Help = "Defines the attenuation start ratio";
attenuationStartSlider.ShortHelp = "Attenuation Start Ratio";
attenuationStartSlider.LongHelp =
"Defines as a ratio from the center to the target the distance 
from which the light starts to attenuate.
ex: 0: the attenuation starts from the center,
    0.5: the attenuation starts from the middle,
    1: the attenuation starts from the end (i.e. no attenuation).";

attenuationAngleLabel.Title = "Angle Ratio";
attenuationAngleLabel.Help = "Define the attenuation angle ratio";
attenuationAngleLabel.ShortHelp = "Attenuation Angle Ratio";
attenuationAngleLabel.LongHelp =
"Defines as a fraction of the light angle the angle to the light
axis from which the light starts to attenuate.
ex: 0: the angle attenuation starts from the axis,
    0.5: the angle attenuation starts from the half angle,
    1: the attenuation starts from the end (i.e. no attenuation).";

attenuationAngleSlider.Title = "";
attenuationAngleSlider.Help = "Define the attenuation angle ratio";
attenuationAngleSlider.ShortHelp = "Attenuation Angle Ratio";
attenuationAngleSlider.LongHelp =
"Defines as a fraction of the light angle the angle to the light
axis from which the light starts to attenuate.
ex: 0: the angle attenuation starts from the axis,
    0.5: the angle attenuation starts from the half angle,
    1: the attenuation starts from the end (i.e. no attenuation).";
 
     
AttenuationExponentLabel.Title = "Exponent ";
AttenuationExponentLabel.Help = "Define the exponent for the spotlight falloff";
AttenuationExponentLabel.ShortHelp = "Exponent falloff";
AttenuationExponentLabel.LongHelp =
"Defines the exponent for the spotlight falloff.
ex: 0: Minimum value,
    2: Maximum value.";
       
AttenuationExponentSlider.Title = "";
AttenuationExponentSlider.Help = "Define the exponent for the spotlight falloff";
AttenuationExponentSlider.ShortHelp = "exponent for the spotlight falloff";
AttenuationExponentSlider.LongHelp =
"Defines the exponent for the spotlight falloff.
ex: 0: Minimum value,
    128: Maximum value.";
    
shadowSepFrame.shadowSepLabel.Title = "Shadows";

shadowSepFrame.shadowSepLabel.Help = "Enables shadows";
shadowSepFrame.shadowSepLabel.ShortHelp = "Enable Shadows";
shadowSepFrame.shadowSepLabel.LongHelp =
"Enables/disables shadow casting.";

shadowCheck.Title = "Ray Traced";
shadowCheck.Help = "Enables Ray Traced Shadows";
shadowCheck.ShortHelp = "Enable Ray Traced Shadows";
shadowCheck.LongHelp =
"Enables/disables ray traced shadow casting 
(This feature is used for software rendering only).";

hardwareShadowCheck.Title = "Real Time";
hardwareShadowCheck.Help = "Enables Real Time Shadows";
hardwareShadowCheck.ShortHelp = "Enable Real Time Shadows";
hardwareShadowCheck.LongHelp =
"Enables/disables real time shadow casting
(This feature is used for real time rendering only).";

shadowSmoothingLabel.Title = "Smoothing";
shadowSmoothingLabel.Help = "Define the smoothing of the shadow limits";
shadowSmoothingLabel.ShortHelp = "Shadow Smoothing";
shadowSmoothingLabel.LongHelp =
"Define the smoothing of the shadow limits.";

shadowSmoothingSlider.Title = "";
shadowSmoothingSlider.Help = "Define the smoothing of the shadow limits";
shadowSmoothingSlider.ShortHelp = "Shadow Smoothing";
shadowSmoothingSlider.LongHelp =
"Define the smoothing of the shadow limits.";

shadowColorLabel.Title = "Color";
shadowColorLabel.Help = "Sets shadow color";
shadowColorLabel.ShortHelp = "Shadow Color";
shadowColorLabel.LongHelp =
"Changes the shadow color.";

shadowColorSlider.Title = "";
shadowColorSlider.Help = "Sets shadow color";
shadowColorSlider.ShortHelp = "Shadow Color";
shadowColorSlider.LongHelp =
"Changes the color of the shadow.";

shadowTransparencyLabel.Title = "Transparency";
shadowTransparencyLabel.Help = "Sets shadow transparency";
shadowTransparencyLabel.ShortHelp = "Shadow Transparency";
shadowTransparencyLabel.LongHelp =
"Changes the shadow transparency.";

shadowTransparencySlider.Title = "";
shadowTransparencySlider.Help = "Sets shadow transparency";
shadowTransparencySlider.ShortHelp = "Shadow Transparency";
shadowTransparencySlider.LongHelp =
"Changes the transparency of the shadow.";

EnvironmentCreationWarning = "There is no active environment for the shadows.\nDo you want to create a new transparent one?";

