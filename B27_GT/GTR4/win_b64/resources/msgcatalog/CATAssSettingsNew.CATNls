// CATAssSettingsNew.CATNls

// English Version
// tbu avril 1999

Help="Defines the assembly tools options";

// Reconnect constraint on Cut/Copy/Paste

RecCstOnCCPFrame.HeaderFrame.Global.Title="Paste components";
RecCstOnCCPFrame.Help="Paste components with or without the assembly constraints applying to them";
RecCstOnCCPFrame.LongHelp="Paste components\nAllows you to paste a components with or without the assembly constraints applying to them.";

RecCstOnCCPFrame.IconAndOptionsFrame.OptionsFrame.BtNone.Title="Without the assembly constraints";
RecCstOnCCPFrame.IconAndOptionsFrame.OptionsFrame.BtNone.Help="Paste components without the assembly constraints applying to them";
RecCstOnCCPFrame.IconAndOptionsFrame.OptionsFrame.BtNone.LongHelp="Allows you to paste a component without the assembly\nconstraints applying to them.";

RecCstOnCCPFrame.IconAndOptionsFrame.OptionsFrame.BtCopy.Title="With the assembly constraints only after a Copy";
RecCstOnCCPFrame.IconAndOptionsFrame.OptionsFrame.BtCopy.Help="Paste components with the assembly constraints applying to them only after a Copy";
RecCstOnCCPFrame.IconAndOptionsFrame.OptionsFrame.BtCopy.LongHelp="Allows you to paste a component with the assembly constraints\napplying to them only after a Copy.";

RecCstOnCCPFrame.IconAndOptionsFrame.OptionsFrame.BtCut.Title="With the assembly constraints only after a Cut";
RecCstOnCCPFrame.IconAndOptionsFrame.OptionsFrame.BtCut.Help="Paste a component with the assembly constraints applying to them only after a Cut";
RecCstOnCCPFrame.IconAndOptionsFrame.OptionsFrame.BtCut.LongHelp="Allows you to paste a component with the assembly constraints\napplying to them only after a Cut.";

RecCstOnCCPFrame.IconAndOptionsFrame.OptionsFrame.BtCopyAndCut.Title="Always with the assembly constraints";
RecCstOnCCPFrame.IconAndOptionsFrame.OptionsFrame.BtCopyAndCut.Help="Paste always components with the assembly constraints applying to them";
RecCstOnCCPFrame.IconAndOptionsFrame.OptionsFrame.BtCopyAndCut.LongHelp="Allows you to always paste components with the assembly\nconstraints applying to them.";

// Update

UpdateAutoFrame.HeaderFrame.Global.Title="Update";

UpdateAutoFrame.IconAndOptionsFrame.OptionsFrame._RadioAutoUpdate.Title="Automatic";
UpdateAutoFrame.IconAndOptionsFrame.OptionsFrame._RadioAutoUpdate.Help="Automatically updates the active component when it is no updated";
UpdateAutoFrame.IconAndOptionsFrame.OptionsFrame._RadioAutoUpdate.LongHelp="Automatically updates the active component when it is no updated.";

UpdateAutoFrame.IconAndOptionsFrame.OptionsFrame._RadioManualUpdate.Title="Manual";
UpdateAutoFrame.IconAndOptionsFrame.OptionsFrame._RadioManualUpdate.Help="The active component will be not automatically updated when it is modified";
UpdateAutoFrame.IconAndOptionsFrame.OptionsFrame._RadioManualUpdate.LongHelp="The active component will be not automatically updated when it is modified.";

UpdateDepthFrame.HeaderFrame.Global.Title="Update propagation depth";

UpdateDepthFrame.IconAndOptionsFrame.OptionsFrame.UpdateFstLevelButton.Title="Active level";
UpdateDepthFrame.IconAndOptionsFrame.OptionsFrame.UpdateFstLevelButton.Help="Update the constraints of active level of the active component";
UpdateDepthFrame.IconAndOptionsFrame.OptionsFrame.UpdateFstLevelButton.LongHelp="Update the constraints of active level of the active component.";

UpdateDepthFrame.IconAndOptionsFrame.OptionsFrame.UpdateDepthButton.Title="All the levels";
UpdateDepthFrame.IconAndOptionsFrame.OptionsFrame.UpdateDepthButton.Help="Update the constraints of all the levels of the active component";
UpdateDepthFrame.IconAndOptionsFrame.OptionsFrame.UpdateDepthButton.LongHelp="Update the constraints of all the levels of the active component.";

// Real Update Status : Begin

RealUpdtStatusFrame.HeaderFrame.Global.Title=" Compute exact update status at open ";

RealUpdtStatusFrame.IconAndOptionsFrame.OptionsFrame._RadioAutoUpdtStatus.Title="Automatic";
RealUpdtStatusFrame.IconAndOptionsFrame.OptionsFrame._RadioAutoUpdtStatus.Help="Load automaticaly necessary datas at open when cache is activated";
RealUpdtStatusFrame.IconAndOptionsFrame.OptionsFrame._RadioAutoUpdtStatus.LongHelp="Load automaticaly necessary datas at open when cache is activated";

RealUpdtStatusFrame.IconAndOptionsFrame.OptionsFrame._RadioManualUpdtStatus.Title="Manual";
RealUpdtStatusFrame.IconAndOptionsFrame.OptionsFrame._RadioManualUpdtStatus.Help="Do not load automaticaly necessary datas at open when cache is activated";
RealUpdtStatusFrame.IconAndOptionsFrame.OptionsFrame._RadioManualUpdtStatus.LongHelp="Model is opened as CGR";

// Real Update Status : End

// Constraints Creation

CreateCstFrame.HeaderFrame.Global.Title="Constraints creation";
CreateCstFrame.Help="Use or not the published geometry for the constraints creation";
CreateCstFrame.LongHelp="Allows you to only use or not the published geometry for the constraints creation.";

CreateCstFrame.IconAndOptionsFrame.OptionsFrame.rbPublishNone.Title="Use any geometry";
CreateCstFrame.IconAndOptionsFrame.OptionsFrame.rbPublishNone.Help="Use any geometry (published or not) of active component";
CreateCstFrame.IconAndOptionsFrame.OptionsFrame.rbPublishChild.Title="Use published geometry of child components only";
CreateCstFrame.IconAndOptionsFrame.OptionsFrame.rbPublishChild.Help="Use published geometry of child components for constraints creation only";
CreateCstFrame.IconAndOptionsFrame.OptionsFrame.rbPublishAll.Title="Use published geometry of any level";
CreateCstFrame.IconAndOptionsFrame.OptionsFrame.rbPublishAll.Help="Use published geometry of any level of active component for constraints creation";

// Redundancy Check 
CreateCstFrame.IconAndOptionsFrame.OptionsFrame.IDD_RedundancyLbl.Title="Redundancy check while constraint creation";
CreateCstFrame.IconAndOptionsFrame.OptionsFrame._cbRedundancyCheck.Title="Disable redundancy check";
CreateCstFrame.IconAndOptionsFrame.OptionsFrame._cbRedundancyCheck.Help="Disable redundancy check";
CreateCstFrame.IconAndOptionsFrame.OptionsFrame._cbRedundancyCheck.LongHelp="Enable/disable check for redundancy of constraints";

// Warning When moving a componant involved in a FixTogether

WarnMovingFixTFra.HeaderFrame.Global.Title="Move components involved in a FixTogether";
WarnMovingFixTFra.IconAndOptionsFrame.OptionsFrame.MoveFixTLab2.Title="Extend selection to all involved components ?";
WarnMovingFixTFra.IconAndOptionsFrame.OptionsFrame.MoveFixTAllwaysRd.Title="Always";
WarnMovingFixTFra.IconAndOptionsFrame.OptionsFrame.MoveFixTNeverRd.Title="Never";
WarnMovingFixTFra.IconAndOptionsFrame.OptionsFrame.MoveFixTAskRd.Title="Ask each time";
WarnMovingFixTFra.IconAndOptionsFrame.OptionsFrame.WarnMovingFixTFBtn.Title="Display warning when moving a component involved in a FixTogether";
WarnMovingFixTFra.IconAndOptionsFrame.OptionsFrame.WarnMovingFixTFBtn.Help="Display warning when moving a component involved in a FixTogether";
WarnMovingFixTFra.IconAndOptionsFrame.OptionsFrame.WarnMovingFixTFBtn.LongHelp="Allows you to display warning message when moving a component involved in a FixTogether.";

// Auto switch Visu -> Design mode

AUTOSWITCHFRM.HeaderFrame.Global.Title="Access to geometry";
AUTOSWITCHFRM.IconAndOptionsFrame.OptionsFrame.AUTOSWITCHCB.Title="Automatic switch to Design mode";
AUTOSWITCHFRM.IconAndOptionsFrame.OptionsFrame.AUTOSWITCHCB.Help="Allow to select geometry through shapes in Visualization mode";
AUTOSWITCHFRM.IconAndOptionsFrame.OptionsFrame.AUTOSWITCHCB.LongHelp="Access to geometry\nConvert automatically shapes in Visualisation mode on shapes in\nDesign mode in order to select geometry.";
