// COPYRIGHT DASSAULT SYSTEMES 2000
//=============================================================================
//
// FreeStyle 3D curve Command :
//   Resource file for NLS purpose.
//
//=============================================================================
//
// Implementation Notes :
//
//=============================================================================
// Sept.  00   Creation                                   Frederic LETZELTER
//=============================================================================
CATStCmd3DCurve.Title = "3D Curve";

CATStCmd3DCurve.UndoTitle = "3D Curve";
CATStCmd3DCurve.RedoTitle = "3D Curve";

CATStCmd3DCurve.initialState.Message = "Click anywhere to create your construction points. Scans of points can also be selected.";
CATStCmd3DCurve.InsertRemoveState.Message = "Select the segment where the point should be added or the point to be removed";
CATStCmd3DCurve.ConstraintState.Message = "Select the support on which the point is to be constrained. Use CTRL key to project it, or use the running point";
CATStCmd3DCurve.SelectScan.Message="Select all points in the scan";
CATStCmd3DCurve.SelectPoints.Message="Select all points in the geometrical set";

CATStCmd3DCurve.pbCreationTypeRecup.Message = "The creation type attribute cannot be retrieved.";
CATStCmd3DCurve.pbPointsRecup.Message = "The construction point features cannot be retrieved.";
CATStCmd3DCurve.pbClosureModeRecup.Message = "The closure mode attribute cannot be retrieved.";
CATStCmd3DCurve.pbSymmetryModeRecup.Message = "The symmetry mode attribute cannot be retrieved.";
CATStCmd3DCurve.pbOrderRecup.Message = "The order attribute cannot be retrieved.";
CATStCmd3DCurve.pbNbMaxArcsRecup.Message = "The max number of arcs attribute cannot be retrieved.";
CATStCmd3DCurve.pbFeatFactRecup.Message = "The features factory cannot be retrieved.";
CATStCmd3DCurve.pbSpecCreation.Message = "An error occurred while creating the feature.";

CATStCmd3DCurve.ProjectionType.MinProjection.Message = "Project using min distance";
CATStCmd3DCurve.ProjectionType.MouseProjection.Message = "Snap on this point";

CATStCmd3DCurve.CATSt3DCurveManip.ImposeTangency.Message = "Impose Tangency";
CATStCmd3DCurve.CATSt3DCurveManip.ImposeCurvature.Message = "Impose Curvature";
CATStCmd3DCurve.CATSt3DCurveManip.SetArcLimit.Message = "Set As Arc Limit";
CATStCmd3DCurve.CATSt3DCurveManip.Deviation.Message = "Dev : ";
CATStCmd3DCurve.CATSt3DCurveManip.Order.Message = "Order : ";
CATStCmd3DCurve.CATSt3DCurveManip.Edit.Message = "Edit";

CATStCmd3DCurve.CATSt3DCurveEltPick.InsertPoint.Message = "Insert a point here";
CATStCmd3DCurve.CATSt3DCurveEltPick.InsertFirstPoint.Message="Insert a point before this point";
CATStCmd3DCurve.CATSt3DCurveEltPick.RemovePoint.Message = "Remove this point";
CATStCmd3DCurve.CATSt3DCurveEltPick.ConstraintPoint.Message = "Constrain this point";
CATStCmd3DCurve.CATSt3DCurveEltPick.FreePoint.Message = "Free this point";

CATStCmd3DCurve.CATSt3DCurveEltPick.InterdictionConstraintPoint.Message = "Cannot constrain this point
because of continuity";

CATStCmd3DCurve.CATSt3DCurveEltPick.InterdictionRemovePoint.Message = "Cannot remove this point
because of continuity";

CATStCmd3DCurve.InsertPoint.Message = "Select the segment where the point should be inserted.";
CATStCmd3DCurve.RemovePoint.Message = "Select the point to be removed.";
CATStCmd3DCurve.ConstraintPoint.Message = "Select the point to be set free or constrained.";

CATStCmd3DCurve.GlobalMode.Title = "Global Mode";

