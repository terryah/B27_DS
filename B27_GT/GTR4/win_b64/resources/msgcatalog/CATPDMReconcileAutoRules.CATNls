// COPYRIGHT DASSAULT SYSTEMES 2004-2010

forceNew        = "Force mapped object to New? "      ;
forceOverwrite  = "Force all to Overwrite? "          ;
forceNewVersion = "Force all to New Version? "        ;
onlyOnDocuments = "Apply rule only on documents"      ;
vpdm            = "VPDM: "                            ;
structure       = "Structure exposed? "               ;
queryName       = "Query Choice: "                    ;
implicitRule    = "Apply implicit rules after query?" ;

// --- Supported environment:

ENOVIA5        = "ENOVIA V5 VPM" ;
VPM1           = "ENOVIA VPM V4" ;
TeamPDM        = "SmarTeam"      ;

reset                  = "Reset choice: "                                 ;
resetChoice1           = "only rules"                                     ;
resetChoice2           = "rules and mapping"                              ;
forceReload            = "Force all objects to Reload? "                  ;
forceNewRevision       = "Force all document to New Revision? "           ;
forceNewFrom           = "Force all document to New From?"                ;
forceKeepExternal      = "Force all document to KeepExternal?"            ;
forceOverwriteByDelta  = "Force all to Overwrite by Delta?"               ;
forceNewVersionByDelta = "Force all to New Version by Delta?"             ;
forcePersistency       = "Force all to automatic selected Storage mode?"  ;
forceRuleSetting       = "Force all to automatic selected Rule? "         ;

queryNameForPRC            = "Query Choice For root assembly Part: " ;
implicitRuleForVPDMMapping = "Apply implicit rules after mapping?"   ;
forceMappingForVPDMMapping = "Force mapping?"                        ;
skipWhenMapped             = "Do not run query on mapped object?"    ;

AllAtNew               = "All objects as New"                  ;
AllAtOverWrite         = "All objects as Overwrite"            ;
AllAtReload            = "All objects as Reload"               ;
AllAtNewVersion        = "All objects as New Version"          ;
AllAtNewRevision       = "All documents as New Revision"       ;
AllAtNewFrom           = "All documents as New From"           ;
AllAtKeepExternal      = "All documents as KeepExternal"       ;
AllAtOverwriteByDelta  = "All objects as Overwrite by Delta"   ;
AllAtNewVersionByDelta = "All objects as New Version by Delta" ;

GlobalEnv      = "Set storage mode" ;
GlobalQuery    = "Global query" ;
ResetAll       = "Reset all" ;
VPDMMapping    = "Mapping based on VPDM View";
// --- for magic Recon
MagicRecon                 = "Magic Reconciliation"      ;
VPMMagicRecon              = "VPM Magic Reconciliation"  ;

// --- Give any GS name to test, so we can test any GS with TestPDM
TestMagicRecon             = "Magic Reconciliation"      ;

AllAtNew.LongHelp               = "To set all unmapped objects to New.\nIf option is checked, mapped objects will be set to new also." ;
AllAtOverWrite.LongHelp         = "To set all objects with no rule to Overwrite.\nUse check option to ignore when a rule is or not already set." ;
AllAtReload.LongHelp            = "To set all objects with no rule to Reload.\nUse check option to ignore when a rule is or not already set." ;
AllAtKeepExternal.LongHelp      = "To set all documents with no rule to KeepExternal.\nUse check option to ignore when a rule is or not already set." ;
GlobalEnv.LongHelp              = "To set the storage mode on all documents." ;
GlobalQuery.LongHelp            = "To run the proposed query with default criteria." ;
ResetAll.LongHelp               = "To reset all the rules and undo the mapping depending on the selection." ;
VPDMMapping.LongHelp            = "To use VPDM View to set storage mode and mapping on objects to be reconciled.";
AllAtNewVersion.LongHelp        = "To set all objects with no rule to New Version.\nUse check option to ignore when a rule is or not already set." ;
AllAtNewRevision.LongHelp       = "To set all documents with no rule to New Revision.\nUse check option to ignore when a rule is or not already set." ;
AllAtNewFrom.LongHelp           = "To set all documents with no rule to New From.\nUse check option to ignore when a rule is or not already set." ;
AllAtOverwriteByDelta.LongHelp  = "To set all objects with no rule to Overwrite by delta.\nUse check option to ignore when a rule is or not already set." ;
AllAtNewVersionByDelta.LongHelp = "To set all objects with no rule to New Version by delta.\nUse check option to ignore when a rule is or not already set." ;
// --- For magic Recon
MagicRecon.LongHelp             = "To automate persistency, mapping and rule application." ;

AllAtNew.ShortHelp               = "Set all unmapped objects to New." ;
AllAtOverWrite.ShortHelp         = "Set all objects with no rule to Overwrite." ;
AllAtReload.ShortHelp            = "Set all objects with no rule to Reload." ;
AllAtKeepExternal.ShortHelp      = "Set all documents with no rule to KeepExternal." ;
GlobalEnv.ShortHelp              = "Set the storage mode on all documents." ;
GlobalQuery.ShortHelp            = "Run the proposed query with default criteria." ;
ResetAll.ShortHelp               = "Reset all the rules and undo the mapping." ;
VPDMMapping.ShortHelp            = "To use VPDM View to set mapping on objects to be reconciled.";
AllAtNewVersion.ShortHelp        = "Set all objects with no rule to New Version." ;
AllAtNewRevision.ShortHelp       = "Set all documents with no rule to New Revision." ;
AllAtNewFrom.ShortHelp           = "Set all documents with no rule to New From." ;
AllAtOverwriteByDelta.ShortHelp  = "Set all objects with no rule to Overwrite by delta." ;
AllAtNewVersionByDelta.ShortHelp = "Set all objects with no rule to New Version by delta." ;
// --- Magic Recon
MagicRecon.ShortHelp             = "Automate persistency, mapping and rule application." ;

WRN_GlobalQueryPrepareCrit         = "Execution of query /p1 failed while preparing query criterion.";
WRN_GlobalQueryPrepareCrit.Generic = "Execution of the specified query failed while preparing query criterion.";
WRN_GlobalQueryPrepareCrit.Detail  = "Query type:/p1";

WRN_GlobalQueryExecuteCrit         = "Execution of criterion /p1 failed at query execution." ;
WRN_GlobalQueryExecuteCrit.Generic = "Execution of the specified criterion failed at query execution." ;
WRN_GlobalQueryExecuteCrit.Detail  = "Criterion name:/p1";

mappedId = "Execute criterion on attribute: ";

// SearchNode :

FilterOnName_State     = "State: "                        ;
FilterOnName_Name      = "Name: "                         ;
FilterOnName_NameInput = "(Enter condition on name here)" ;

FilterOnDoc_Type     = "Type: "               ;
FilterOnDoc_DocExt   = "Document Extension: " ;
FilterOnDoc_FileName = "File Name: "          ;
FilterOnDoc_ANYDOC   = "all"                  ;
FilterOnDoc_DOC      = "Document"             ;
FilterOnDoc_PROXYDOC = "Proxy Document"       ;

// CATReconcileDefault.xml:

Locate               = "Locate"                      ;
FilterOnName         = "Filter On Name"              ;
FilterOnDocument     = "Filter On Document"          ;
FilterOnAttributes   = "Filter On Mapped Attributes" ;
SelectAllDocuments   = "Select All Documents"        ;
SelectSubTree        = "Select Sub Tree"             ;

// --- For magic Recon.
WRN_NoMagicReconGS            = "No valid Magic Reconciliation Global set."             ;
WRN_NoMagicReconGS.Generic    = "No valid Magic Reconciliation Global set."             ;
WRN_NoMagicReconGS.Detail     = "Magic Reconciliation Global set not found."            ;
WRN_NoMagicReconGS.Suggestion = "Please check name of Magic Reconciliation global set." ;

