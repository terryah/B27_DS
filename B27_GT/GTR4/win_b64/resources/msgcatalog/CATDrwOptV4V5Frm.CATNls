//=====================================================================================
//                                     CNEXT - CXRn
//                          COPYRIGHT DASSAULT SYSTEMES 1999
//-------------------------------------------------------------------------------------
// FILENAME    :    CATDrwOptV4V5
// LOCATION    :    DraftingUI
// AUTHOR      :    fgx
// BUT         :    
// DATE        :    09.09.1999
//-------------------------------------------------------------------------------------
// DESCRIPTION :    Tools/Option du Drafting - Onglet V4 -> V5
//-------------------------------------------------------------------------------------
// COMMENTS    :    
//-------------------------------------------------------------------------------------
// MODIFICATIONS     user  date        purpose
//    HISTORY        ----  ----        -------
//      01           fgx   18.11.1999  Ajout de ConvertDimensionAsGraphic
//		02			 dcf   27.06.2000  Ajout de ExplodeDitto
//=====================================================================================

Title="V4->V5";

frameConvertDimensionAsGraphic.HeaderFrame.Global.Title="Dimension and annotation conversion mode";
frameConvertDimensionAsGraphic.HeaderFrame.Global.LongHelp="Defines the way dimensions are converted in V5.";
frameConvertDimensionAsGraphic.IconAndOptionsFrame.OptionsFrame.checkConvertDimensionAsGraphic.Title="Convert dimension as a graphic";
frameConvertDimensionAsGraphic.IconAndOptionsFrame.OptionsFrame.checkConvertDimensionAsGraphic.LongHelp="Convert dimension as a graphic
Allows you to convert dimensions as a simple graphic element.";
frameConvertDimensionAsGraphic.IconAndOptionsFrame.OptionsFrame.chkConvertBalloonAsGraphic.Title="Convert balloon as a graphic";
frameConvertDimensionAsGraphic.IconAndOptionsFrame.OptionsFrame.chkConvertBalloonAsGraphic.LongHelp="Allows you to convert balloons as a simple graphic element.";
frameConvertDimensionAsGraphic.IconAndOptionsFrame.OptionsFrame.chkConvertOtherAnnotAsGraphic.Title="Convert other annotation as a graphic";
frameConvertDimensionAsGraphic.IconAndOptionsFrame.OptionsFrame.chkConvertOtherAnnotAsGraphic.LongHelp="Allows you to convert texts,GDT,datum,roughness as a simple graphic element.";

frameExplode.HeaderFrame.Global.Title="Explode mode ";
frameExplode.IconAndOptionsFrame.OptionsFrame.chkExplodeDitto.Title="Explode ditto when migrating to V5 elements";
frameExplode.IconAndOptionsFrame.OptionsFrame.chkExplodeSymbolo.Title="Explode symbolo when migrating to V5 elements";

frameBlinkRed.HeaderFrame.Global.Title="Blink element";
frameBlinkRed.IconAndOptionsFrame.OptionsFrame.checkBlinkRed.Title="Migrate blink element as red color";

frameGVSXml.HeaderFrame.Global.Title="Generative view style";
frameGVSXml.IconAndOptionsFrame.OptionsFrame.lbl.Title="Default generative view style";
frameGVSXml.IconAndOptionsFrame.OptionsFrame.chkGVSXml.LongHelp="Choose the default xml file for migration view";

frameGeom.HeaderFrame.Global.Title="Geometry import ";
frameGeom.HeaderFrame.Global.LongHelp="Parameters for 2D geometry import.";
frameGeom.IconAndOptionsFrame.OptionsFrame._checkCenterEndPoints.Title="Create centers and end points";
frameGeom.IconAndOptionsFrame.OptionsFrame._checkCenterEndPoints.LongHelp="Create centers and end points
Creates circles centers and curves end points 
when importing 2D geometry.";

frmDetailSheet.HeaderFrame.Global.Title="Detail sheet organization";
frmDetailSheet.HeaderFrame.Global.LongHelp="Detail sheet organization";
frmDetailSheet.IconAndOptionsFrame.OptionsFrame.chkAllDetailOneSheet.Title="Migrate details and symbols in one detail sheet";
frmDetailSheet.IconAndOptionsFrame.OptionsFrame.chkAllDetailOneSheet.LongHelp="Migrates details and symbols in one sheet of detail.";

toolAnnotBold.HeaderFrame.Global.Title="Annotation bold attribute";
toolAnnotBold.IconAndOptionsFrame.OptionsFrame.lblV4V5_LimitThickBold.Title="Limit V4 thickness for bold attribute"; 
toolAnnotBold.IconAndOptionsFrame.OptionsFrame.spnV4V5_LimitThickBold.LongHelp="Beyond this V4 thickness 
the migration will apply a bold attribute on the annotation.";
