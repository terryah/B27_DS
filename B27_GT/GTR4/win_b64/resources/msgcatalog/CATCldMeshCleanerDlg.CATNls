//==============================================================================================
// COPYRIGHT DASSAULT SYSTEMES PROVENCE 2003
//==============================================================================================
// 26-Mar-2003 - JLH - Creation
//==============================================================================================

Title = "Mesh Cleaner" ;

CorruptedSfx1     = " triangle" ;
CorruptedSfxN     = " triangles" ;
DuplicatedSfx1    = " triangle" ;
DuplicatedSfxN    = " triangles" ;
NonOrientableSfx1 = " triangle" ;
NonOrientableSfxN = " triangles" ;
NMFEdgesSfx1      = " edge" ;
NMFEdgesSfxN      = " edges" ;
NMFVerticesSfx1   = " vertex" ;
NMFVerticesSfxN   = " vertices" ;
NonOrientable     = "One non-orientable mesh." ;
NonOrientables    = " non-orientable meshes." ;
OnlyOneZone       = "There is only one zone -> no split" ;
SeveralZones      = " connected zones" ;

RootFrame.Pages.DeletionPag.Title     = "Deletion" ;
RootFrame.Pages.DeletionPag.LongHelp  = "Deletes corrupted/duplicated triangles, non-manifold edges and/or vertices." ;
RootFrame.Pages.StructurePag.Title    = "Structure" ;
RootFrame.Pages.StructurePag.LongHelp = "Treatments on the global structure of the mesh." ;
RootFrame.Pages.EditionPag.Title      = "Edition" ;
RootFrame.Pages.EditionPag.LongHelp   = "Edition of the mesh : thin triangles collapse." ;

RootFrame.Pages.DeletionPag.AnalyzeBtn.Title    = "Analyze" ;
RootFrame.Pages.DeletionPag.AnalyzeBtn.LongHelp = "Analyzes the mesh to prepare the miscelleaneous treatments." ;
RootFrame.Pages.DeletionPag.StatusLbl.Title     = "Statistics" ;
RootFrame.Pages.DeletionPag.StatusLbl.LongHelp  = "Gives the current statistics of the mesh." ;
RootFrame.Pages.DeletionPag.ColorsLbl.Title     = "Preview colors" ;
RootFrame.Pages.DeletionPag.ColorsLbl.LongHelp  = "Defines the colors for the pre-visualization." ;

RootFrame.Pages.DeletionPag.CorruptedBtn.Title    = "Corrupted Triangles" ;
RootFrame.Pages.DeletionPag.CorruptedBtn.LongHelp = "Deletes corrupted triangles." ;

RootFrame.Pages.DeletionPag.DuplicatedBtn.Title    = "Duplicated Triangles" ;
RootFrame.Pages.DeletionPag.DuplicatedBtn.LongHelp = "Deletes duplicated triangles." ;

RootFrame.Pages.DeletionPag.NonOrientableBtn.Title    = "Inconsistent Orientation" ;
RootFrame.Pages.DeletionPag.NonOrientableBtn.LongHelp = "Deletes couples of connected triangles with inconsistent orientation between them." ;

RootFrame.Pages.DeletionPag.NMFEdgesBtn.Title       = "Non-manifold Edges" ;
RootFrame.Pages.DeletionPag.NMFEdgesBtn.LongHelp    = "Deletes non-manifold edges." ;

RootFrame.Pages.DeletionPag.NMFVerticesBtn.Title    = "Non-manifold Vertices" ;
RootFrame.Pages.DeletionPag.NMFVerticesBtn.LongHelp = "Deletes non-manifold vertices." ;

RootFrame.Pages.DeletionPag.IsolatedBtn.Title    = "Isolated Triangles" ;
RootFrame.Pages.DeletionPag.IsolatedBtn.LongHelp = "Deletes isolated triangles." ;
RootFrame.Pages.DeletionPag.IsolatedSld.Title    = "Number of triangles" ;
RootFrame.Pages.DeletionPag.IsolatedSld.LongHelp = "Defines the maximum number of triangles in an isolated sub-mesh to delete." ;

RootFrame.Pages.DeletionPag.LongEdgesBtn.Title    = "Long Edges" ;
RootFrame.Pages.DeletionPag.LongEdgesBtn.LongHelp = "Deletes triangles with long edges." ;
RootFrame.Pages.DeletionPag.MaxLengthFrm.Title    = "Max. Length" ;
RootFrame.Pages.DeletionPag.MaxLengthFrm.LongHelp = "Defines the maximum length of edges." ;

RootFrame.Pages.DeletionPag.SmallAnglesBtn.Title    = "Small Angles" ;
RootFrame.Pages.DeletionPag.SmallAnglesBtn.LongHelp = "Deletes triangles with small angles." ;
RootFrame.Pages.DeletionPag.MinAngleFrm.Title       = "Min. Angle" ;
RootFrame.Pages.DeletionPag.MinAngleFrm.LongHelp    = "Defines the minimum angle." ;

RootFrame.Pages.StructurePag.DirectLbl.Title         = "Direct Triangles" ;
RootFrame.Pages.StructurePag.DirectLbl.LongHelp      = "Preview color for direct triangles." ;
RootFrame.Pages.StructurePag.OppositeLbl.Title       = "Indirect Triangles" ;
RootFrame.Pages.StructurePag.OppositeLbl.LongHelp    = "Preview color for indirect triangles." ;
RootFrame.Pages.StructurePag.OrientationBtn.Title    = "Orientation" ;
RootFrame.Pages.StructurePag.OrientationBtn.LongHelp = "Orientates triangles in the same way (if possible)." ;

RootFrame.Pages.StructurePag.CnxZonesBtn.Title    = "Split in Connected Zones" ;
RootFrame.Pages.StructurePag.CnxZonesBtn.LongHelp = "Splits the mesh into connected zones." ;
RootFrame.Pages.StructurePag.DistinctBtn.Title    = "Distinct" ;
RootFrame.Pages.StructurePag.DistinctBtn.LongHelp = "Splits in distinct entities." ;
RootFrame.Pages.StructurePag.GroupedBtn.Title     = "Grouped" ;
RootFrame.Pages.StructurePag.GroupedBtn.LongHelp  = "Splits in distinct cells grouped in one entity." ;

RootFrame.Pages.EditionPag.EditSmallAnglesBtn.Title    = "Small Angles" ;
RootFrame.Pages.EditionPag.EditSmallAnglesBtn.LongHelp = "Collapse triangles with small angles." ;
