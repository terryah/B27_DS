OptFeature = "Optimization Component";

OptOptimizationSet					= "Set Of Optimization Components";
OptOptimizationSet.Optimizations	= "List Of Optimization Components";

OptConstraint				= "Optimization Constraint";
OptConstraint.Satisfaction	= "Is Satisfied";
OptConstraint.Precision		= "Precision";
OptConstraint.Distance		= "Distance";
OptConstraint.Priority		= "Priority";

OptGoal				= "Optimization Goal";
OptGoal.Type		= "Type";
OptGoal.TargetValue	= "Target Value";
OptGoal.Precision	= "Precision";
OptGoal.Priority	= "Priority";
OptGoal.Parameter	= "Parameter";
OptGoal.Comment		= "Comment Upon Goal";

OptProblem				= "Optimization Problem";
OptProblem.Goals		= "List Of Goals";
OptProblem.Constraints	= "List Of Constraints";
OptProblem.Comment		= "Comment Upon Problem";

OptFreeParameter			= "Optimization Free Parameter";
OptFreeParameter.Value		= "Value";
OptFreeParameter.Parm		= "Parameter";
OptFreeParameter.InfRange	= "Lower Bound";
OptFreeParameter.SupRange	= "Upper Bound";
OptFreeParameter.Step		= "Step";
OptFreeParameter.HasRangeStep = "Has Ranges And/Or Step";

// PEO Algorithm generic Type
OptGenericAlgorithm						= "PEO Algorithm";
OptGenericAlgorithm.NbUpdatesMax		= "Maximum number of updates";
OptGenericAlgorithm.MaxTime				= "Maximum Time";
OptGenericAlgorithm.MaxWoImprovement	= "Maximum Number Of Updates Without Improvement";
OptGenericAlgorithm.StoppingCriterion	= "Stopping Criterion";

OptGenericAlgorithm.StopRelativeObjectiveChange = "Relative Objective Change";
OptGenericAlgorithm.StopAbsoluteObjectiveChange =  "Absolute Objective Change";
OptGenericAlgorithm.StopAbsoluteVariableChange = "Absolute Variable Change";
OptGenericAlgorithm.StopAbsoluteConstraintChange =  "Absolute Constraint Change";
OptGenericAlgorithm.StopRelativeConstraintChange = "Relative Constraint Change";
OptGenericAlgorithm.StopWindowChange = "Stop Window Change";

// one generic type for Optimization and one generic type for DoE
OptGenericOptimAlgorithm				= "Optimization Algorithm";
OptGenericDoeAlgorithm					= "DOE Algorithm";

// sub types of Optimization generic type
OptGradientAlgorithm		= "Gradient Algorithm Without Constraint";
OptGradientAlgorithm.ConvergenceSpeed = "Convergence Speed";

OptApproximationGradientAlgorithm = "Gradient Algorithm With Constraint(s)";
OptApproximationGradientAlgorithm.ConvergenceSpeed = "Convergence Speed";

OptSimAnnealingAlgorithm	= "Simulated Annealing Algorithm";
OptSimAnnealingAlgorithm.ConvergenceSpeed = "Convergence Speed";

// sub type of DoE generic type
FullDoeAlgorithm						= "Full Design Of Experiments";
FullDoeAlgorithm.ListOfLevels			= "List of parameters levels";
FullDoeAlgorithm.LastDoneExperimentNb	= "Index of the Last experiment done";

OptOptimization					= "Optimization";
OptOptimization.Problem			= "Problem";
OptOptimization.FreeParameters	= "List of free parameters";
OptOptimization.Algorithm		= "Algorithm";
OptOptimization.UpdateVisualization = "Update visualization flag";
OptOptimization.OptimizationLog	= "Optimization log";

//SOC - 08/10/02
OptimizationLog							= "Optimization Log";
OptimizationLog.DesignTable				= "Computations Management";
OptimizationLog.IndexOfBestSolInDT		= "Index Of Particular Results Of The Log";
OptimizationLog.BestParm				= "Parameter Giving The Best Computed Result";
OptimizationLog.NbEvalParm				= "Parameter Giving The Current Evaluation Number";

//SOC - 15/11/02 : les attributs du type suivant sont ceux de Set of Equations (Package Advisor)
//				   => on les trouve dans CATKnowledgeAdvisorSearch
OptConstraintSatisfaction	= "Constraints Satisfaction";

// sub types of Optimization generic type
OptApproximationAlgorithm		= "Approximation Algorithm For Constraints and Priorities";
OptLocalWithPrioritiesAlgorithm		= "Local Algorithm For Constraints and Priorities";
OptCstGradAlgorithm		= "Algorithm for Constraints & Derivatives Providers";


