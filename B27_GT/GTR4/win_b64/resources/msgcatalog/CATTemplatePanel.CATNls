//=============================================================================
//                                     CNEXT - CXR1
//                          COPYRIGHT DASSAULT SYSTEMES 1997 
//-----------------------------------------------------------------------------
// FILENAME    :    CATTemplatePanel
// LOCATION    :    DraftingGenUI/CNext/resources/msgcatalog/
// AUTHOR      :    jmt
// DATE        :    Oct. 29 1997
//------------------------------------------------------------------------------
// DESCRIPTION :    Resources File for NLS purpose related to Wizard Views
//                  Creation Panel.
//                  
//------------------------------------------------------------------------------
// COMMENTS    :	Suite a des modifs majeures, il faut comprendre ici
//					ISO=1er triedre .. ANSI=3e triedre
//------------------------------------------------------------------------------
//MODIFICATIONS     user  date      purpose
//   HISTORY        ----  ----      -------
//
//                  abr   27/04/98  mise � jour standards ergo sur commentaires
//                                  en accord avec ogk
//					ogk	  03.07.98  modif des steps + ajout de 2 variables
//					ogk	  07.07.98  modif de textes pour correction RI 197653
//------------------------------------------------------------------------------

WizardStep1="View Wizard (Step 1/2) : Predefined Configurations";
WizardStep2="View Wizard (Step 2/2) : Arranging the Configuration";
WizardStep3="View Wizard (Step 3/3) : Solid Selection";

UpFrm.ViewFrm.PreviewFrm.Title="Preview ";
UpFrm.HelpFrm.Title="Wizard Hints";

UpFrm.ViewFrm.PreviewFrm.LongHelp=
"Displays a preview with the selected configuration.";
UpFrm.HelpFrm.LongHelp=
"Dipslays wizard hints.";

Error1Text="You cannot insert more than two views 
of the same type into the layout.";
Error2Text="There must be at least one main view in the layout
in order to draw the view(s) into the Drafting window! 
Go back to previous step(s) to insert a main view 
(Front or Isometric).";

//  (Window Frame Zone)
//  AddStep1FrameHelpEditor=
//  "Inserts a combination of many views by pressing 
//  any button in the left panel. A front or isometric view 
//  can be set as the main view via the contextual menu. 
//  Then, the main view will be drawn in the green color.
//  Any view can be moved when selected, or deleted
//  using the contextual menu.";
//  AddStep2FrameHelpEditor=
//  "Inserts more views by selecting them from the left
//  window. Activating the link will make the projected views
//  linked to the main view. Increment the minimum distance
//  to leave more space between the views in the drawing.";
//  AddStep3FrameHelpEditor=
//  "Make sure you select a plane if there is at least one
//  front view in the preview.";

CheckLinkCATDlgCheckText="Projection views : linked to the main view";
CheckLinkCATDlgUncheckText="Projection views : not linked to the main view";

CheckLinkCATDlgCheckText.LongHelp=
"Sets that projection views are linked to the main view.";
CheckLinkCATDlgUncheckText.LongHelp=
"Sets that projection views are not linked to the main view.";

CurrentNameText="Projection views: not linked to the main view";
MainViewTypeNoneText="Main view is: [none]";
MainViewTypeFrontText="Main view is front view";
MainViewTypeIsometricText="Main view is isometric view";
AddStep3FrameStep3FrmSelectionText=
"Solid acquisition: click on the part or model.";

CurrentNameText.LongHelp=
"Defines that the projection views are not linked to the main view.";
MainViewTypeNoneText.LongHelp=
"Defines that there is no main view.";
MainViewTypeFrontText.LongHelp=
"Defines that the main view is the front view.";
MainViewTypeIsometricText.LongHelp=
"Defines that the main view is the isometric view.";
AddStep3FrameStep3FrmSelectionText.LongHelp=
"Acquires the solid by clicking on the part or model.";

NextTitle="Next >";
FinishTitle="Finish";
DownFrm.WindowButtonsFrm.Cancel.Title="Cancel";
DownFrm.WindowButtonsFrm.Previous.Title="< Back";
DownFrm.WindowButtonsFrm.Next.Title="Next >";
DownFrm.WindowButtonsFrm.Finish="Finish";
InContextDelete="Delete";
InContextMainView="Set as main view";
UpFrm.StepFrm.Step1Frm.Load.Title="Load...";
UpFrm.StepFrm.Step1Frm.CustomizedView1.Title=" ";
UpFrm.StepFrm.Step1Frm.CustomizedView2.Title=" ";
UpFrm.StepFrm.Step1Frm.CustomizedView3.Title=" ";
UpFrm.StepFrm.Step2Frm.FrontView.Title="Front View";
UpFrm.StepFrm.Step2Frm.RearView.Title="Rear View";
UpFrm.StepFrm.Step2Frm.TopView.Title="Top View";
UpFrm.StepFrm.Step2Frm.BottomView.Title="Bottom View";
UpFrm.StepFrm.Step2Frm.LeftView.Title="Left View";
UpFrm.StepFrm.Step2Frm.RightView.Title="Right View";
UpFrm.StepFrm.Step2Frm.IsomView.Title="Isometric";
UpFrm.StepFrm.Step2Frm.Clear.Title="Clear Layout";
UpFrm.ViewFrm.InfoFrm.LinkWith.Title="Link";
UpFrm.StepFrm.Step2Frm.SaveAs.Title="Save";

DownFrm.WindowButtonsFrm.Cancel.LongHelp=
"Cancels the previous choice.";
DownFrm.WindowButtonsFrm.Previous.LongHelp=
"Goes back to the previous window.";
DownFrm.WindowButtonsFrm.Next.LongHelp=
"Goes to the following window.";
DownFrm.WindowButtonsFrm.Finish.LongHelp=
"Ends.";
InContextDelete.LongHelp=
"Deletes the selected configuration.";
InContextMainView.LongHelp=
"Sets the view as the main view.";

UpFrm.StepFrm.Step1Frm.Text1.Title="Standard types:";
UpFrm.StepFrm.Step1Frm.Text2.Title="User-defined types:";
UpFrm.ViewFrm.InfoFrm.SpaceLabel.Title="Minimum distance between each view:";
UpFrm.StepFrm.Step2Frm.SpaceLabel2.Title="Between views:";

UpFrm.StepFrm.Step1Frm.Text1.LongHelp=
"Defines the standard types.";
UpFrm.StepFrm.Step1Frm.Text2.LongHelp=
"Defines the user-defined types.";
UpFrm.ViewFrm.InfoFrm.SpaceLabel.LongHelp=
"Applies a minimum distance between each view.";
UpFrm.StepFrm.Step2Frm.SpaceLabel2.LongHelp=
"Applies a distance between views.";


UpFrm.StepFrm.Step1Frm.Load.ShortHelp="Load Customized Configuration";
UpFrm.StepFrm.Step2Frm.FrontView.ShortHelp="Front View";
UpFrm.StepFrm.Step2Frm.RearView.ShortHelp="Rear View";
UpFrm.StepFrm.Step2Frm.TopView.ShortHelp="Top View";
UpFrm.StepFrm.Step2Frm.BottomView.ShortHelp="Bottom View";
UpFrm.StepFrm.Step2Frm.LeftView.ShortHelp="Left View";
UpFrm.StepFrm.Step2Frm.RightView.ShortHelp="Right View";
UpFrm.StepFrm.Step2Frm.IsomView.ShortHelp="Isometric View";
UpFrm.StepFrm.Step2Frm.Clear.ShortHelp="Clear Preview";
UpFrm.ViewFrm.InfoFrm.LinkWith.ShortHelp="Views Link";
UpFrm.StepFrm.Step2Frm.SaveAs.ShortHelp="Save Configuration";

UpFrm.StepFrm.Step1Frm.Load.Help="Loads a user-defined view layout";
UpFrm.StepFrm.Step2Frm.FrontView.Help="Creates a front view";
UpFrm.StepFrm.Step2Frm.RearView.Help="Creates a rear view";
UpFrm.StepFrm.Step2Frm.TopView.Help="Creates a top view";
UpFrm.StepFrm.Step2Frm.BottomView.Help="Creates a bottom view";
UpFrm.StepFrm.Step2Frm.LeftView.Help="Creates a left view";
UpFrm.StepFrm.Step2Frm.RightView.Help="Creates a right view";
UpFrm.StepFrm.Step2Frm.IsomView.Help="Creates an isometric view";
UpFrm.StepFrm.Step2Frm.Clear.Help="Removes all views from the preview window";
UpFrm.ViewFrm.InfoFrm.LinkWith.Help="Links projection views with parent view";
UpFrm.StepFrm.Step2Frm.SaveAs.Help="Saves preview configuration in a file";

UpFrm.StepFrm.Step1Frm.Load.LongHelp=
"Load User-Defined Configuration
Loads a user-defined view layout.";
UpFrm.StepFrm.Step2Frm.FrontView.LongHelp=
"Front View
Creates a front view.";
UpFrm.StepFrm.Step2Frm.RearView.LongHelp=
"Rear View
Creates a rear view.";
UpFrm.StepFrm.Step2Frm.TopView.LongHelp=
"Top View
Creates a top view.";
UpFrm.StepFrm.Step2Frm.BottomView.LongHelp=
"Bottom View
Creates a bottom view.";
UpFrm.StepFrm.Step2Frm.LeftView.LongHelp=
"Left View
Creates a left view.";
UpFrm.StepFrm.Step2Frm.RightView.LongHelp=
"Right View
Creates a right view.";
UpFrm.StepFrm.Step2Frm.IsomView.LongHelp=
"Isometric View
Creates an isometric view.";
UpFrm.StepFrm.Step2Frm.Clear.LongHelp=
"Clear Preview
Removes all views from the preview window.";
UpFrm.ViewFrm.InfoFrm.LinkWith.LongHelp=
"Views Link
Links the projection views to the parent view.";
UpFrm.StepFrm.Step2Frm.SaveAs.LongHelp=
"Save Configuration
Saves the preview configuration into file.";

ComposedView1CaptionISO = "Bottom, Front, Left";
ComposedView2CaptionISO = "Right, Bottom, Front";
ComposedView3CaptionISO = "Front, Left, Top";
ComposedView4CaptionISO = "Right, Front, Top";
ComposedView5CaptionISO = "Top, Left, Front, Bottom, Right, Isometric";
ComposedView6CaptionISO = "All Kinds of Views";
ComposedView1CaptionANSI = "Top, Front, Right";
ComposedView2CaptionANSI = "Left, Front, Top";
ComposedView3CaptionANSI = "Front, Bottom, Right";
ComposedView4CaptionANSI = "Left, Bottom, Front";
ComposedView5CaptionANSI = "Top, Left, Front, Bottom, Right, Isometric";
ComposedView6CaptionANSI = "All Kinds of Views";

ComposedView1ToolTipISO = "Configuration 1 using the 1st angle projection method";
ComposedView2ToolTipISO = "Configuration 2 using the 1st angle projection method";
ComposedView3ToolTipISO = "Configuration 3 using the 1st angle projection method";
ComposedView4ToolTipISO = "Configuration 4 using the 1st angle projection method";
ComposedView5ToolTipISO = "Configuration 5 using the 1st angle projection method";
ComposedView6ToolTipISO = "Configuration 6 using the 1st angle projection method";
ComposedView1ToolTipANSI = "Configuration 1 using the 3rd angle projection method";
ComposedView2ToolTipANSI = "Configuration 2 using the 3rd angle projection method";
ComposedView3ToolTipANSI = "Configuration 3 using the 3rd angle projection method";
ComposedView4ToolTipANSI = "Configuration 4 using the 3rd angle projection method";
ComposedView5ToolTipANSI = "Configuration 5 using the 3rd angle projection method";
ComposedView6ToolTipANSI = "Configuration 6 using the 3rd angle projection method";

ComposedView1HelpMessageISO="Creates a view layout based on bottom, front and left";
ComposedView2HelpMessageISO="Creates a view layout based on right, front and bottom";
ComposedView3HelpMessageISO="Creates a view layout based on front, top and left";
ComposedView4HelpMessageISO="Creates a view layout based on right, top and front";
ComposedView5HelpMessageISO="Creates a View layout based on top, left, front, bottom, right and isometric ";
ComposedView6HelpMessageISO="Creates a view layout based on all the views";
ComposedView1HelpMessageANSI="Creates a view layout based on top, front and right";
ComposedView2HelpMessageANSI="Creates a view layout based on left, front and top";
ComposedView3HelpMessageANSI="Creates a view layout based on front, bottom and right";
ComposedView4HelpMessageANSI="Creates a view layout based on left, bottom and front";
ComposedView5HelpMessageANSI="Creates a View layout based on top, left, front, bottom, right and isometric";
ComposedView6HelpMessageANSI="Creates a view layout based on all the views";

ComposedView1ContextualHelpISO=
"Configuration 1 (Bottom Front Left) using the 1st angle projection method
Creates a view layout based on bottom, front and left views";
ComposedView2ContextualHelpISO=
"Configuration 2 (Right Front Bottom) using the 1st angle projection method
Creates a view layout based on right, front and bottom views";
ComposedView3ContextualHelpISO=
"Configuration 3 (Front Top Left) using the 1st angle projection method
Creates a view layout based on front, top and left views";
ComposedView4ContextualHelpISO=
"Configuration 4 (Right Top Front) using the 1st angle projection method
Creates a view layout based on right, top and front views";
ComposedView5ContextualHelpISO=
"Configuration 5 (Top Left Front Bottom Right Isometric) using the 1st angle projection method
Creates a view layout based on top, left, front, bottom right and isometric views";
ComposedView6ContextualHelpISO=
"Configuration 6 (All Kinds of Views) using the 1st angle projection method
Creates a view layout based on all views";
ComposedView1ContextualHelpANSI=
"Configuration 1 (Top Front Right) using the 3rd angle projection method
Creates a view layout based on top, front and right views";
ComposedView2ContextualHelpANSI=
"Configuration 2 (Left Front Top) using the 3rd angle projection method
Creates a view layout based on left, front and top views";
ComposedView3ContextualHelpANSI=
"Configuration 3 (Front Bottom Right) using the 3rd angle projection method
Creates a view layout based on front, bottom and right views";
ComposedView4ContextualHelpANSI=
"Configuration 4 (Left Bottom Front) using the 3rd angle projection method
Creates a view layout based on left, bottom and front views";
ComposedView5ContextualHelpANSI=
"Configuration 5 (Top Left Front Bottom Right Isometric) using the 3rd angle projection method
Creates a view layout based on top, left, front, bottom right and isometric views";
ComposedView6ContextualHelpANSI=
"Configuration 6 (All Kinds of Views) using the 3rd angle projection method
Creates a view layout based on all views";

