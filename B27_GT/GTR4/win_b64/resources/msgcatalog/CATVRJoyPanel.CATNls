more = "More >>";
less = "<< Less";
MoreShortHelp = "Show advanced options";
MoreLongHelp = "Show advanced options.";
LessShortHelp = "Hide advanced options";
LessLongHelp = "Hide advanced options.";

increase = "Increase";
decrease = "Decrease";
speedstep = "Increase rate:  ";
speedupdown = "  Speed:            ";
goup = " Go Up ";
godown = " Go Down ";
goupdown = "Vertical Translation";
speed = "Speed";

noEffect = "No Effect";
button1 = "Mouse Button 1";
button2 = "Mouse Button 2";
button3 = "Mouse Button 3";
initialPOV = "Initial point of view";
reverseSpeed = "Reverse speed";
speedUp = "Speed up";
comboGoUp = "Go up";
speedDown = "Speed down";
comboGoDown = "Go Down";
stop = "Stop (speed = 0)";
NoSelection = "<no selection>";
Button = "  Button ";

SStepShortHelp = "Sets the increase rate of the speed";
SStepLongHelp = "Sets the increase rate of the speed. When you push the\njoystick button, the speed increases by this rate.";
IncreaseShortHelp = "Sets the button that increases speed";
IncreaseLongHelp = "Sets the button that increases speed.";
DecreaseShortHelp = "Sets the button that decreases speed";
DecreaseLongHelp = "Sets the button that decreases speed.";

GoUpShortHelp = "Sets the button to go up";
GoUpLongHelp = "Sets the button to go up.";
GoDownLongHelp = "Sets the button to go down.";
GoDownShortHelp = "Sets the button to go down";
SpeedUpDownShortHelp = "Sets the speed of vertical translation";
SpeedUpDownLongHelp = "Sets the speed of vertical translation.";

WalkButtonShortHelp = "Vertical Translation is controled by the buttons";
WalkButtonLongHelp = "Vertical Translation is controled by the buttons.";
WalkSliderLongHelp = "Unused";
WalkSliderShortHelp = "Unused";
FlySliderShortHelp = "Speed is controled by the slider";
FlySliderLongHelp = "Speed is controled by the slider.";
FlyButtonShortHelp = "Speed is controled by the buttons";
FlyButtonLongHelp = "Speed is controled by the buttons.";

Title = "Joystick Control Panel";
ShortHelp = "Configures the joystick";
LongHelp = "This panel allows you to configure the joystick.";

Blanc.Title = "     ";

Devices.Title = "Devices";
Devices.HMDTracking.Title = "HMD Tracking";
Devices.HMDTracking.ShortHelp = "Activates the head tracking";
Devices.HMDTracking.LongHelp = "Activates the head tracking\nto look the scene in any positions.";
Devices.Joystick.Title = "Joystick";
Devices.Joystick.ShortHelp = "Activates the joystick";
Devices.Joystick.LongHelp = "Activates the joystick to navigate in the scene.";

Navigation.Title = "Navigation";
Navigation.Walk.Title = "Walk";
Navigation.Walk.ShortHelp = "Selects the walk mode";
Navigation.Walk.LongHelp = "In the walk mode, push the joystick to advance and turn\nand use buttons to go up or down.";
Navigation.Fly.Title = "Fly";
Navigation.Fly.ShortHelp = "Selects the fly mode";
Navigation.Fly.LongHelp = "In the fly mode, you are in a plane.\nThe speed is controled by the slider or buttons\nand the orientation is changed by the joystick.";

Frame3A.Title = "Fly Sensitivity";
Frame3A.XAxis.Title = "X Axis:  ";
Frame3A.XAxis.ShortHelp = "Sets the X axis sensitivity";
Frame3A.XAxis.LongHelp = "Sets the X axis sensitivity.\nIt also refers to the head angle.";
Frame3A.YAxis.Title = "Y Axis:  ";
Frame3A.YAxis.ShortHelp = "Sets the Y axis sensitivity";
Frame3A.YAxis.LongHelp = "Sets the Y axis sensitivity.\nIt also refers to the pitch angle.";
Frame3A.RAxis.Title = "Z Rotation:  ";
Frame3A.RAxis.ShortHelp = "Sets the Z rotation sensitivity";
Frame3A.RAxis.LongHelp = "Sets the Z rotation sensitivity.\nIt also refers to the roll angle.";
Frame3A.SpeedX.ShortHelp = "Sets the X axis sensitivity";
Frame3A.SpeedY.ShortHelp = "Sets the Y axis sensitivity";
Frame3A.SpeedR.ShortHelp = "Sets the Z rotation sensitivity";
Frame3A.SpeedX.LongHelp = "Sets the X axis sensitivity.\nIt also refers to the head angle.";
Frame3A.SpeedY.LongHelp = "Sets the Y axis sensitivity.\nIt also refers to the pitch angle.";
Frame3A.SpeedR.LongHelp = "Sets the Z rotation sensitivity.\nIt also refers to the roll angle.";
Frame3A.Blanc.Title = "     ";
Frame3A.Joystick.ShortHelp = "Joystick axis position";
Frame3A.Joystick.LongHelp = "Joystick axis position.";

Frame3B.Title = "Walk Sensitivity";
Frame3B.XAxis.Title = "X Axis:  ";
Frame3B.XAxis.ShortHelp = "Sets the X axis max speed";
Frame3B.XAxis.LongHelp = "Sets the X axis max speed.\nThis is the speed of side translation.";
Frame3B.YAxis.Title = "Y Axis:  ";
Frame3B.YAxis.ShortHelp = "Sets the Y axis max speed";
Frame3B.YAxis.LongHelp = "Sets the Y axis max speed.\nThis is the speed of forward/backward translation.";
Frame3B.RAxis.Title = "Z Rotation:  ";
Frame3B.RAxis.ShortHelp = "Sets the Z rotation sensitivity";
Frame3B.RAxis.LongHelp = "Sets the Z rotation sensitivity.\nIt corresponds at the head angle.";
Frame3B.SpeedXb.ShortHelp = "Sets the X axis max speed";
Frame3B.SpeedYb.ShortHelp = "Sets the Y axis max speed";
Frame3B.SpeedR.ShortHelp = "Sets the Z rotation sensitivity";
Frame3B.SpeedXb.LongHelp = "Sets the X axis max speed.\nThis is the speed of side translation.";
Frame3B.SpeedYb.LongHelp = "Sets the Y axis max speed.\nThis is the speed of forward/backward translation.";
Frame3B.SpeedR.LongHelp = "Sets the Z rotation sensitivity.\nIt corresponds at the head angle.";
Frame3B.Blanc.Title = "     ";
Frame3B.Joystick.ShortHelp = "Joystick axis position";
Frame3B.Joystick.LongHelp = "Joystick axis position.";

Frame4.SpeedMax.Title = "Max Speed:  ";
Frame4.SpeedMax.ShortHelp = "Max speed of displacement";
Frame4.SpeedMax.LongHelp = "Max speed of displacement. This speed\nis reached when the slider is at its maximum.";
Frame4.SMax.ShortHelp = "Max speed of displacement";
Frame4.SMax.LongHelp = "Max speed of displacement. This speed\nis reached when the slider is at its maximum.";
Frame4.UseSlider.Title = "Use Slider";
Frame4.UseButtons.Title = "Use Buttons";
Frame4.Blanc.Title = "     ";
Frame4.Pourcent.Title = "%";

Buttons.Title = "Buttons Customization";
Buttons.Start.Title = "Choose a Button";
Buttons.Start.LongHelp = "Selects a button by pressing a joystick button.";
Buttons.Start.ShortHelp = "Selects a joystick button";
Buttons.buttonName.ShortHelp = "Button number";
Buttons.buttonName.LongHelp = "Button number.";
Buttons.boutonA.ShortHelp = "Action to be associated to the button";
Buttons.boutonA.LongHelp = "Action to be associated to the button.";

Frame7.Title = "Navigation Options";
Frame7.moveMode.Title = "Move in the sight direction";
Frame7.moveMode.ShortHelp = "You always advance in the sight direction.";
Frame7.moveMode.LongHelp = "You always advance in the sight direction and not in the feet direction.";
Frame7.TopBottom.Title = "Switch Top/Bottom";
Frame7.TopBottom.ShortHelp = "Top and bottom are inverted";
Frame7.TopBottom.LongHelp = "Top and bottom are inverted.";
Frame7.RollHead.Title = "Switch Roll/Head";
Frame7.RollHead.ShortHelp = "Y Axis and Z rotation are inverted";
Frame7.RollHead.LongHelp = "Y Axis and Z rotation are inverted.";
Frame7.RightLeft.Title = "Switch Right/Left";
Frame7.RightLeft.ShortHelp = "Right and Left are inverted";
Frame7.RightLeft.LongHelp = "Right and Left are inverted.";
Frame7.Gravity.Title = "Gravitational effects during navigation";
Frame7.Gravity.ShortHelp = "Indicate if gravity is enabled";
Frame7.Gravity.LongHelp = "Indicate if gravitational effects are enabled.";

TopHat.Title = "TopHat Options";
TopHat.LabelSpeedth.Title = "Speed:  ";
TopHat.LabelSpeedth.ShortHelp = "Sets the cursor speed";
TopHat.LabelSpeedth.LongHelp = "Sets the cursor speed.";
TopHat.Speedth.LongHelp = "Sets the cursor speed.";
TopHat.Speedth.ShortHelp = "Sets the cursor speed";
TopHat.Sphere.Title = "Stop at screen border";
TopHat.Sphere.ShortHelp = "The cursor will stop at the screen border";
TopHat.Sphere.LongHelp = "The cursor will stop at the screen border.\nIf unchecked, the cursor will pass from one edge of the screen to another.";
TopHat.Double.Title = "Double speed after a long impulse";
TopHat.Double.ShortHelp = "Doubles the speed after a long impulse";
TopHat.Double.LongHelp = "Doubles the speed after a long impulse";
TopHat.Activeth.Title = "Activates the cursor";
TopHat.Activeth.ShortHelp = "Activates the cursor to emulate the mouse.";
TopHat.Activeth.LongHelp = "Activates the cursor to emulate the mouse.\nYou can then select objects and use commands from toolbars.";

Frame9.Reduce.Title = "Reduce";
Frame9.Reduce.ShortHelp = "Minimize the window";
Frame9.Reduce.LongHelp = "Minimize the window";
Frame9.Default.Title = "Default";
Frame9.Default.ShortHelp = "Reset parameters to default values";
Frame9.Default.LongHelp = "Reset parameters to default values.";

Frame10.Restore.Title = "Restore";
Frame10.Restore.ShortHelp = "Maximize the window";
Frame10.Restore.LongHelp = "Maximize the window";
