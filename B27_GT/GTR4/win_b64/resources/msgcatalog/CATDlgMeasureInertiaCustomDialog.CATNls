Title = "Measure Inertia Customization";



VolumeFrame.Title                  =     "Inertia";
VolumeFrame.LongHelp               =     "Lets you customize the display in the Measure Inertia dialog box and define what will be exported to the text file";

VolumeFrame.Description.Title      =     "Description";
VolumeFrame.Description.LongHelp   =     "Click to identify the item measured and indicate whether it is a volume or surface";

VolumeFrame.Equivalent.Title       =     "Equivalent";
VolumeFrame.Equivalent.LongHelp    =     "Click to visualize whether inertia equivalents are taken into account";

VolumeFrame.Volume.Title           =     "Volume";
VolumeFrame.Volume.LongHelp        =     "Click to display volume";

VolumeFrame.Area.Title             =     "Area";
VolumeFrame.Area.LongHelp          =     "Click to display area";

VolumeFrame.COfG.Title             =     "Center of gravity (G)";
VolumeFrame.COfG.LongHelp          =     "Click to display center of gravity";

VolumeFrame.Density.Title          =     "Density / Surfacic mass";
VolumeFrame.Density.LongHelp       =     "Click to display density or surfacic mass\nDensity is a measure of an item's mass per unit volume expressed in kg/m3\nSurfacic mass is a measure of an item's mass per unit area expressed in kg/m2";

VolumeFrame.Mass.Title             =     "Mass";
VolumeFrame.Mass.LongHelp          =     "Click to display mass";

VolumeFrame.Moments.Title          =     "Principal moments / G";
VolumeFrame.Moments.LongHelp       =     "Click to display principal moments of inertia with respect to the center of gravity";

VolumeFrame.Axes.Title             =     "Principal axes";
VolumeFrame.Axes.LongHelp          =     "Click to display principal axes";

VolumeFrame.InertiaMatrix.Title    =     "Inertia matrix / G";
VolumeFrame.InertiaMatrix.LongHelp =     "Click to display inertia matrix with respect to the center of gravity";

VolumeFrame.Axis.Title             =     "Moment / axis";
VolumeFrame.Axis.LongHelp          =     "Click to display the moment of inertia about an axis";

VolumeFrame.InertiaMatrix_O.Title    =     "Inertia matrix / O";
VolumeFrame.InertiaMatrix_O.LongHelp =     "Click to display inertia matrix with respect to the origin of the document";

VolumeFrame.InertiaMatrix_P.Title    =     "Inertia matrix / P";
VolumeFrame.InertiaMatrix_P.LongHelp =     "Click to display the inertia matrix with respect to a point";

VolumeFrame.InertiaMatrix_AS.Title    =     "Inertia matrix / axis system";
VolumeFrame.InertiaMatrix_AS.LongHelp =     "Click to display the inertia matrix with respect to an axis system";
