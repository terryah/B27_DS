//=====================================================================================
//                                     CNEXT - CXR1
//                          COPYRIGHT DASSAULT SYSTEMES 1997 
//-------------------------------------------------------------------------------------
// FILENAME    :    CATDrwError
// LOCATION    :    
// AUTHOR      :    YPR
// DATE        :    05/05/98
//-------------------------------------------------------------------------------------
// DESCRIPTION :    Catalogue NLS pour les erreurs drafting de base 
//                  
// inheritance :    
//-------------------------------------------------------------------------------------
// COMMENTS    :    
//-------------------------------------------------------------------------------------
//MODIFICATIONS     user  date      purpose
//   HISTORY        ----  ----      -------
//   01             ppd   25.06.98  Error message for the command to create dimensions
//         modif_01_ppd             (CreDimViewNotUpdated)
//   02		  lgk	  25.06.02  Revision
//   03		  lgk	  14.10.02  Revision2
//	 04		  wsa	  25.07.05  Addition
//=====================================================================================

CATDrwError.PanelTitleError = "도면 오류";

CATDrwError.PanelTitleWarning = "도면 경고";

CATDrwError.TestError = "테스트 성공";

CATDrwError.OneSheetError = "마지막 시트를 삭제할 수 없습니다.";

CATDrwError.UpdateError = "뷰 생성 중 오류가 발생했습니다.";

CATDrwError.DocNotSavedError = "저장되지 않은 문서에서 뷰를 생성할 수 없습니다.
먼저 문서를 저장한 후 다시 시도하십시오.";

CATDrwError.CreDimViewNotUpdated = "최신이 아닌 뷰에서 치수를 작성할 수 없습니다.
먼저 뷰를 갱신한 후 다시 시도하십시오.";

CATDrwError.DocReadOneFontWarning = "/p2라는 글꼴이 시스템에 없습니다.

이 세션 중에 기본 글꼴 /p1을(를) 사용하여 이 글꼴을 참조하는 텍스트를 표시합니다.
글꼴 디렉토리를 확인하거나 도면 작성자에게 문의하십시오.";

CATDrwError.DocReadNFontWarning = "몇 가지 글꼴이 시스템에 없습니다.

해당 글꼴은 /p2입니다.
이 세션 중에 기본 글꼴 /p1을(를) 사용하여 이 글꼴을 참조하는 텍스트를 표시합니다.
글꼴 디렉토리를 확인하거나 도면 작성자에게 문의하십시오.";

CATDrwError.InvalidSelectionError = "유효하지 않은 선택.
이 요소에 대한 표시 및 선택 가능한 등록 정보를 선택하고
선택한 솔리드가 올바른지 확인하십시오.";

CATDrwError.DetailViewNoIntersection = "세부사항 정의 프로파일이 활성 뷰와 교차하지 않습니다.
세부사항 뷰를 작성할 뷰를 활성화하십시오.";

CATDrwError.DetailViewNoIntersection2 = "클리핑 정의 프로파일이 활성 뷰와 교차하지 않습니다.
클리핑 뷰를 작성할 뷰를 활성화하십시오.";

CATDrwError.NothingToExtract = "투영할 3D 요소가 사용 불가능합니다.\n모델, 프로덕트 또는 파트가 비어 있을 수 있습니다.
V4 모델의 경우 투영할 스킨 또는 Exact 솔리드 요소가 있는지 확인하십시오.
파트의 경우 최신 상태인지 확인하십시오.
3D 와이어프레임의 경우 도구 > 옵션 > 도면 > 뷰 탭으로 이동하여 3D 와이어프레임 프로젝트를 선택했는지 확인하십시오.
CATProcess 문서의 경우 평면 선택 이전에 프로덕트를 선택하십시오.";

CATDrwError.EmptyEncumbrancy = "유효하지 않은 선택. \n선택한 솔리드의 encumbrancy가 널(null)입니다. \n모델, 프로덕트 또는 파트가 비어 있을 수 있습니다. \n 프로덕트의 경우 디자인 모드인지 확인하십시오.";

CATDrwError.EncumbrancyUnfolded = "선택한 솔리드의 encumbrancy가 널(null)입니다. \n펼친 뷰 명령을 선택한 솔리드에 적용할 수 없습니다.";

CATDrwError.NotAGoodView = "이 작업을 수행할 수 없습니다. 현재 뷰는 생성 뷰여야 합니다.";

CATDrwError.WrongView = "이 작업을 수행할 수 없습니다. 뷰가 잠겨 있을 수 있습니다. \n현재 또는 선택한 뷰는 정확한 레프리젠테이션 모드에서 생성 뷰여야 합니다. \n DMU 상자가 있는 섹션 잘라내기, 펼쳐진 및 고급 전면 뷰는 허용되지 않습니다.";

CATDrwError.NotUpdatedView = "현재 또는 선택한 뷰를 갱신해야 합니다.";

CATDrwError.NoShowMode = "3D 파트 또는 프로덕트가 숨기기 모드에 있습니다.";

CATDrwError.EmptySketch = "프로파일이 비어 있습니다. 다시 시작하십시오.";

CATDrwError.ErrorType = "잘못된 유형의 치수.";

CATDrwError.ErrorParallel = "치수가 평행하지 않습니다.";

CATDrwError.EmptyView = "이 작업을 수행할 수 없습니다. 뷰가 비어 있습니다.";

CATDrwError.DetailPasteInDesignSheet = "디자인 시트에서 세부사항을 붙여넣을 수 없습니다.";

CATDrwError.ErrorProp = "이 오브젝트의 등록 정보를 복사할 수 없습니다.";

CATDrwError.ViewPasteInDetailSheet = "세부사항 시트에서 뷰를 붙여넣을 수 없습니다.";

CATDrwError.DittoPasteCycle = "대상 2D 컴포넌트에서 2D 컴포넌트 인스턴스를 붙여넣을 수 없습니다.";

CATDrwError.SectionCutNoIntersection = "섹션 잘라내기 프로파일이 활성 뷰와 교차하지 않습니다.
섹션 잘라내기 뷰를 작성할 뷰를 활성화하십시오.";

CATDrwError.SectionViewOrientation = "이 측면의 프로파일에서는 단면 뷰가 비게 됩니다.
다른 측면을 선택하십시오.";

CATDrwError.SaveAsCatalog = "카탈로그를 생성하기 전에 현재 도면을 .CATDrawing 파일로 저장해야 합니다.";

CATDrwError.InvalidProfile = "현재 프로파일이 유효하지 않습니다. \n편집 모드를 끝내기 전에 정정해야 합니다.";

CATDrwError.InvalidViewDefinition = "뷰 정의의 프로젝션 평면에서는 프로파일 수정이 허용되지 않습니다. \n이 보기의 정의 평면을 변경하십시오.";

CATDrwError.InvalidIsolatedView = "생성 뷰가 분리되었습니다. \n프로파일 수정이 허용되지 않습니다.";

CATDrwError.LockedView = "생성 뷰가 잠겨졌습니다. \n프로파일 수정이 허용되지 않습니다.";

CATDrwError.GenBalloonInvalidAssembly = "벌룬 생성은 어셈블리 뷰에서만 사용 가능합니다.";

CATDrwError.GenBalloonNoNumbering = "벌룬을 생성할 수 없음: 어셈블리 컴포넌트에 번호가 지정되었는지 확인하십시오.";

CATDrwError.SubOperatorWarning = "브레이크아웃이 이 콜아웃의 결과로 얻은 뷰에 적용되었습니다.
브레이크아웃 작업의 스펙이 동일한 상태로 있고 콜아웃 수정에 따라
지오메트리를 수정할 수 있습니다.";

CATDrwError.Invalid3DProfile = "3D에서 프로파일을 정의하는 스케치가 유효하지 않습니다.";

CATDrwError.ProfileDefinedIn3D = "편집하려는 프로파일이 3D로 정의되어 있습니다.
편집/링크를 사용하여 연계된 문서를 열고 프로파일을 수정하십시오.";

CATDrwError.ChangeExactToCgr = "이 작업 중에 도면에서 일부 드레스업 요소를 잃게 될 수 있습니다.";

CATDrwError.ViewIsFromCGR = "프로파일이 연계되지 않습니다.";

CATDrwError.CreateCGRView = "CGR에서 뷰를 생성하려고 합니다. 계속하시겠습니까?";

CATDrwError.CanNotResetToStandardProperties = "하나 이상의 요소가 선택된 경우,
표준 등록 정보를 사용하여 다시 설정할 수 없습니다.";

CATDrwError.WireAndOccCullWarning = "'프로젝트 3D 와이어프레임' 옵션이 활성화된 상태로 단절 버리기 기능을 사용하려고 합니다.
적절한 결과를 위해 와이어프레임 요소가 CGR 데이터로 존재하는지 확인하십시오.
CGR 데이터를 작성하기 전에 도구 > 옵션 > 일반 > 표시 > 성능에서
'캐시에 선형 요소 저장' 옵션을 활성화한 경우가 해당됩니다(예를 들어,
시각화 모드에서 파트 또는 프로덕트를 실행하기 전에).
그렇지 않은 경우, 이 옵션을 활성화한 후에 CGR 데이터를 다시 작성해야 합니다.
자세한 정보는 일반 도면 사용자 안내서를 참조하십시오.";

CATDrwError.IncompatibleRasterAndViewOrientation = "선택한 뷰 중 적어도 하나가 래스터로 뷰 생성과 호환되지 않는
특정 방향을 가지고 있습니다.
각도 값을 0으로 설정한 후 래스터 모드를 선택하십시오.";

CATDrwError.NotAccessibleDirMessage = "선택한 파일이 사용자 환경에서 사용 가능한지 확인하십시오.
자세한 정보는 대화식 도면 사용자 안내서의
관리 태스크 > 표준 매개변수 설정 > 패턴 정의를 참조하십시오.";

CATDrwError.ReplaceError = "뷰에서 참조되는 하나 이상의 파트 인스턴스를 새 문서에서 찾을 수 없습니다.";

CATDrwError.TPSView = "3D 뷰의 경우 프로파일 수정이 허용되지 않습니다.";

CATDrwError.NoNewScene = "새 화면에서 뷰를 생성할 수 없습니다.";

CATDrwError.EditCalloutWithBroken = "손상된 뷰에 속한 콜아웃을 편집하려고 합니다.
프로파일은 손상된 뷰에 있는 것처럼 해석되고 생성된 지오메트리는 손상된
사양 정보로 남습니다." ;

CATDrwError.EditDetailCalloutInterBroken = "손상된 뷰에 속한 콜아웃을 편집하려고 합니다. 프로파일이 손상된 면적과 교차합니다. \n해당되는 세부사항 뷰는 손상되지 않으므로 정의 뷰의 한 쪽만 표시합니다.\n정의 프로파일을 제대로 정의하는 데 문제가 있을 수 있습니다." ;

CATDrwError.EditDetailInBrokenArea = "세부사항 콜아웃이 손상된 뷰에 정의되어 있습니다.\n 콜아웃의 프로파일이 손상된 면적에 있으므로 편집할 수 없습니다.\n이를 편집하려면 먼저 손상된 정의를 제거해야 합니다." ;

CATDrwError.IncompatibleViewLinks = "Explode된 프로덕트의 여러 인스턴스를 참조하는 뷰를 작성하는 것이 ENOVIA V5 VPM 컨텍스트에서는 허용되지
않습니다(모든 생성 뷰가 동일한 3D 요소를 참조해야 합니다.).
VPM 세션, DMU 컴포넌트 또는 새 화면을 참조하는 뷰를 작성하는 것이 ENOVIA V5 VPM 컨텍스트에서는 허용되지 않습니다.";

CATDrwError.NotTheSameDoc = "동일한 3D 문서를 지시하지 않는 뷰 링크는 수정할 수 없습니다.";

CATDrwError.DocNotLoaded = "3D 지시 문서는 로드되지 않습니다.
링크\편집 명령이나 파일\데스크를 사용하여 로드하십시오.";

CATDrwError.CorruptedPreV5R15GAModel = "이 모델은 V5R15 GA 코드 레벨에서 더 이상 지원되지 않는 V5R15 GA 이전 데이터를 내포하고 있습니다. \nV5R15 GA 코드 레벨에서는 이 모델을 읽을 수 없습니다.";

CATDrwError.selectedviewsmodified = "선택된 뷰만 수정되었습니다.";

CATDrwError.selectinscene = "수정 중인 뷰는 화면의 뷰입니다.
화면이 편집될 때만 3D 선택이 고려됩니다.";

CATDrwError.XcaleCGMNotCompatible = "현재 글로벌(=/p1) 축척이 내부 문서 축척(=/p2)과 호환되지 않으므로 문서를 열거나 로드할 수 없습니다.";

CATDrwError.RefProductModified = "뷰를 프로젝션할 때 고려할 레퍼런스 프로덕트를 변경하려고 합니다.
뷰를 갱신하면 프로젝션 평면이 변경될 수 있습니다(원점 및 방향). 그 결과 수정된 뷰와 링크된 뷰
(섹션 콜아웃과 결과 섹션 뷰 사이, 세부사항 콜아웃과 ....) 사이에 몇 가지 불일치가 표시될 수 있습니다.";

CATDrwError.NoCompatibleView = "선택한 뷰가 치수 생성과 맞지 않습니다.";

CATDrwError.LowMemoryError = "메모리 사용이 한계에 도달했습니다.\n뷰 작성을 계속하기 위해 모든 3D 데이터를 로드할 수 없습니다.";

CATDrwError.LowMemInterrupt = "프로세스가 인터럽트되었습니다.\n메모리 한계를 초과했습니다.\n기계에 사용된 메모리를 줄이십시오.";

CATDrwError.MockUp = "V4 목업 지오메트리가 검출되었습니다.\n정확한 뷰 생성 모드와 맞지 않습니다.\n근사값 모드가 권장됩니다.";
