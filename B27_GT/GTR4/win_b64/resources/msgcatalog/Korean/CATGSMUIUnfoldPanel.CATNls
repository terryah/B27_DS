//-----------------------------------------------------------------------------
// COPYRIGHT DASSAULT SYSTEMES 2003
//-----------------------------------------------------------------------------
//
//                   CATGSMUIUnfoldPanel
//
//-----------------------------------------------------------------------------

UnfoldTitle="정의 펼치기";
UnfoldUVTitle="UV 매개변수화 정의";
TransferTitle="정의 전송";

SurfaceToUnfold.FraEdt.Lab.Title="펼칠 서피스: ";
SurfaceToUnfold.ShortHelp="펼칠 서피스.";
SurfaceToUnfold.LongHelp="펼칠 서피스.";

UnfoldedSurface.FraEdt.Lab.Title="펼쳐진 서피스: ";
UnfoldedSurface.ShortHelp="펼쳐진 서피스.";
UnfoldedSurface.LongHelp="펼쳐진 서피스.";

TabContainer.PositionTabPage.Title="위치";

TabContainer.PositionTabPage.Reference.Title="레퍼런스";

TabContainer.PositionTabPage.Reference.Origin.FraEdt.Lab.Title="원점: ";
TabContainer.PositionTabPage.Reference.Origin.ShortHelp="레퍼런스 2D 축의 원점.";
TabContainer.PositionTabPage.Reference.Origin.LongHelp="레퍼런스 2D 축의 원점.";
TabContainer.PositionTabPage.Reference.Origin.Flag="레퍼런스 원점";

TabContainer.PositionTabPage.Reference.Direction.FraEdt.Lab.Title="방향: ";
TabContainer.PositionTabPage.Reference.Direction.ShortHelp="레퍼런스의 방향.";
TabContainer.PositionTabPage.Reference.Direction.LongHelp="레퍼런스의 방향.";
TabContainer.PositionTabPage.Reference.Direction.Flag="레퍼런스 방향";

TabContainer.PositionTabPage.Reference.Side.FraEdt.Lab.Title="고정된 측면: ";
TabContainer.PositionTabPage.Reference.Side.ShortHelp="고정을 유지할 티어링 면.";
TabContainer.PositionTabPage.Reference.Side.LongHelp="고정을 유지할 티어링 면.";
TabContainer.PositionTabPage.Reference.Side.Flag="고정 면";
TabContainer.PositionTabPage.Reference.InvertOrientationButtonV5.Title="반전 방향";
TabContainer.PositionTabPage.Reference.InvertOrientationButton.Title="역방향";

TabContainer.PositionTabPage.Target.Title="대상";

TabContainer.PositionTabPage.Target.Plane.FraEdt.Lab.Title="평면: ";
TabContainer.PositionTabPage.Target.Plane.ShortHelp="펼치는 면.";
TabContainer.PositionTabPage.Target.Plane.LongHelp="펼치는 면.";
TabContainer.PositionTabPage.Target.Plane.Flag="대상 서피스";

TabContainer.PositionTabPage.Target.Origin.FraEdt.Lab.Title="원점: ";
TabContainer.PositionTabPage.Target.Origin.ShortHelp="대상 2D 축의 원점.";
TabContainer.PositionTabPage.Target.Origin.LongHelp="대상 2D 축의 원점.";
TabContainer.PositionTabPage.Target.Origin.Flag="대상 원점";

TabContainer.PositionTabPage.Target.FraDirection.FraEdt.Lab.Title="방향: ";
TabContainer.PositionTabPage.Target.FraDirection.ShortHelp="대상 방향.";
TabContainer.PositionTabPage.Target.FraDirection.LongHelp="대상 방향.";

TabContainer.SpecsToTearTabPage.Title="티어링할 커브";

TabContainer.SpecsToTearTabPage.SpecToTear.FraEdt.Lab.Title="티어링할 모서리 ";
TabContainer.SpecsToTearTabPage.SpecToTear.ShortHelp="티어링할 모서리.";
TabContainer.SpecsToTearTabPage.SpecToTear.LongHelp="티어링할 모서리.";

TabContainer.ToTransferTabPage.Title="전송";

TabContainer.ToTransferTabPage.ToTransferFrame.ElementToTransferSelectorLabel.Title="요소";
TabContainer.ToTransferTabPage.ToTransferFrame.ElementToTransferSelectorList.ShortHelp="전송할 점 또는/및 커브.";
TabContainer.ToTransferTabPage.ToTransferFrame.ElementToTransferSelectorList.LongHelp="전송할 점 또는/및 커브.";

TabContainer.ToTransferTabPage.ToTransferFrame.TypeOfTransferSelectorLabel.Title="Transfo";
TabContainer.ToTransferTabPage.ToTransferFrame.TypeOfTransferSelectorList.ShortHelp="전송할 점 또는 커브에 적용할 변환.";
TabContainer.ToTransferTabPage.ToTransferFrame.TypeOfTransferSelectorList.LongHelp="전송할 점 또는 커브에 적용할 변환.";

TabContainer.ToTransferTabPage.ToTransferFrame.ElementToTransferSelector.ShortHelp="전송할 점 또는 커브.";
TabContainer.ToTransferTabPage.ToTransferFrame.ElementToTransferSelector.LongHelp="전송할 점 또는 커브.";
TabContainer.ToTransferTabPage.ToTransferFrame.ElementToTransferSelector.Flag="전송할 점 또는 커브";

TabContainer.ToTransferTabPage.ToTransferFrame.TypeOfTransferCombo.FoldToUnfold="펼침";
TabContainer.ToTransferTabPage.ToTransferFrame.TypeOfTransferCombo.UnfoldToFold=" 접기 ";
TabContainer.ToTransferTabPage.ToTransferFrame.TypeOfTransferCombo.ShortHelp="전송할 점 또는 커브에 적용할 변환.";
TabContainer.ToTransferTabPage.ToTransferFrame.TypeOfTransferCombo.LongHelp="전송할 점 또는 커브에 적용할 변환.";

TabContainer.ToTransferTabPage.ToTransferButtonsFrame.RemoveButton.Title="제거 ";
TabContainer.ToTransferTabPage.ToTransferButtonsFrame.RemoveButton.ShortHelp="선택된 점 또는/및 커브를 제거합니다.";
TabContainer.ToTransferTabPage.ToTransferButtonsFrame.RemoveButton.LongHelp="선택된 점 또는/및 커브를 제거합니다.";
TabContainer.ToTransferTabPage.ToTransferButtonsFrame.ReplaceButton.Title="교체";
TabContainer.ToTransferTabPage.ToTransferButtonsFrame.ReplaceButton.ShortHelp="선택된 점 또는/및 커브를 교체합니다.";
TabContainer.ToTransferTabPage.ToTransferButtonsFrame.ReplaceButton.LongHelp="선택된 점 또는/및 커브를 교체합니다.";

MoreButtonFrame.MoreLessButton.ShortHelp="고급 옵션 숨기기 또는 표시";
MoreButtonFrame.MoreLessButton.LongHelp="고급 옵션 숨기기 또는 표시";

MoreButtonTitle="추가 >>";
LessButtonTitle="제거 <<";

AdvancedOptions.SurfaceTypeFrame.SurfaceTypeLabel.Title="서피스 유형: ";
AdvancedOptions.SurfaceTypeFrame.RuledButton.Title="룰 지정";
AdvancedOptions.SurfaceTypeFrame.RuledButton.ShortHelp="입력 서피스 유형 지정 허용: 룰 지정 또는 모두.";
AdvancedOptions.SurfaceTypeFrame.RuledButton.LongHelp="입력 서피스 유형 지정 허용: 룰 지정 또는 모두.";
AdvancedOptions.SurfaceTypeFrame.AnyButton.Title="모두";
AdvancedOptions.SurfaceTypeFrame.AnyButton.ShortHelp="입력 서피스 유형 지정 허용: 룰 지정 또는 모두.";
AdvancedOptions.SurfaceTypeFrame.AnyButton.LongHelp="입력 서피스 유형 지정 허용: 룰 지정 또는 모두.";

AdvancedOptions.DisplayOptionalEdgesButton.Title="티어링할 선택적 모서리 표시";
AdvancedOptions.DisplayOptionalEdgesButton.ShortHelp="티어링할 선택적 모서리 표시를 허용합니다.";
AdvancedOptions.DisplayOptionalEdgesButton.LongHelp="티어링할 선택적 모서리 표시를 허용합니다.";
AdvancedOptions.DistortionsFrame.Title="왜곡 색상표";
AdvancedOptions.DistortionsFrame.UnfoldedDistButton.Title="펼쳐진 서피스";
AdvancedOptions.DistortionsFrame.UnfoldedDistButton.ShortHelp="펴진 서피스의 길이 왜곡입니다.";
AdvancedOptions.DistortionsFrame.UnfoldedDistButton.LongHelp="펼쳐진 서피스의 길이 왜곡 색상표를 작업 영역에 표시합니다.
길이 왜곡은 펴진 출력 요소의 길이 매개변수와 입력 요소의 매개변수
사이의 비율(백분율)입니다.
양수 왜곡은 펴진 요소의 확장을 의미하고, 음수 왜곡은 축소를 의미합니다.";
AdvancedOptions.DistortionsFrame.UnfoldedPermanentDistButton.Title="영구";
AdvancedOptions.DistortionsFrame.UnfoldedPermanentDistButton.ShortHelp="펴진 서피스에 있는 왜곡의 영구 표시입니다.";
AdvancedOptions.DistortionsFrame.UnfoldedPermanentDistButton.LongHelp="펼쳐진 서피스의 왜곡 색상표를 영구적으로 표시합니다.";
AdvancedOptions.DistortionsFrame.SurfToUnfoldDistButton.Title="펼칠 서피스";
AdvancedOptions.DistortionsFrame.SurfToUnfoldDistButton.ShortHelp="입력 서피스의 길이 왜곡입니다.";
AdvancedOptions.DistortionsFrame.SurfToUnfoldDistButton.LongHelp="펼칠 서피스의 길이 왜곡 색상표를 작업 영역에 표시합니다.
길이 왜곡은 펴진 출력 요소의 길이 매개변수와 입력 요소의 매개변수
사이의 비율(백분율)입니다.
양수 왜곡은 펴진 요소의 확장을 의미하고, 음수 왜곡은 축소를 의미합니다.";

TabContainer.PositionTabPage.Target.ReverseUButton.Title="역 Uf";
TabContainer.PositionTabPage.Target.ReverseUButton.ShortHelp="Uf 방향 뒤집기를 허용합니다.";
TabContainer.PositionTabPage.Target.ReverseUButton.LongHelp="Uf 방향 뒤집기를 허용합니다.";
TabContainer.PositionTabPage.Target.ReverseVButton.Title="역 Vf";
TabContainer.PositionTabPage.Target.ReverseVButton.ShortHelp="Vf 방향 뒤집기를 허용합니다.";
TabContainer.PositionTabPage.Target.ReverseVButton.LongHelp="Vf 방향 뒤집기를 허용합니다.";
TabContainer.PositionTabPage.Target.SwapButton.Title="스왑";
TabContainer.PositionTabPage.Target.SwapButton.ShortHelp="Uf 및 Vf 방향 스와핑을 허용합니다.";
TabContainer.PositionTabPage.Target.SwapButton.LongHelp="Uf 및 Vf 방향 스와핑을 허용합니다.";

ReferenceOrigin="레퍼런스 원점";
ReferenceDirection="레퍼런스 방향";
TargetPlane="대상 면";
TargetOrigin="대상 원점";

DistortionsFrame.Title="왜곡 색상표";
DistortionsFrame.UnfoldedDistButton.Title="펼쳐진 서피스";
DistortionsFrame.UnfoldedDistButton.ShortHelp="펴진 서피스의 길이 왜곡입니다.";
DistortionsFrame.UnfoldedDistButton.LongHelp="펼쳐진 서피스의 길이 왜곡 색상표를 작업 영역에 표시합니다.
길이 왜곡은 펴진 출력 요소의 길이 매개변수와 입력 요소의 매개변수
사이의 비율(백분율)입니다.
양수 왜곡은 펴진 요소의 확장을 의미하고, 음수 왜곡은 축소를 의미합니다.";
DistortionsFrame.UnfoldedPermanentDistButton.Title="영구";
DistortionsFrame.UnfoldedPermanentDistButton.ShortHelp="펴진 서피스에 있는 왜곡의 영구 표시입니다.";
DistortionsFrame.UnfoldedPermanentDistButton.LongHelp="펼쳐진 서피스의 왜곡 색상표를 영구적으로 표시합니다.";

