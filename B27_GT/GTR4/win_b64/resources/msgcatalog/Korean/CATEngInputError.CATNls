// *--------------------------------------------*//
// *               Message example              *//
// *                                            *//
// *------------------------------------------- *//
// ENGINPUT_ERR_XXX.Request    = "Operation failed";
// ENGINPUT_ERR_XXX.Diagnostic = "Curve support /p1 is invalid";
// ENGINPUT_ERR_XXX.Advice     = "Choose a smoother curve";


///////////////////////////////////////////////////////////////////////////
//                            ATTRIBUTE ERRORS                           //
///////////////////////////////////////////////////////////////////////////
// Mandatory input
ENGINPUT_ERR_MANDATORYINPUT.Request    = "잘못된 기능 /p1. ";
ENGINPUT_ERR_MANDATORYINPUT.Diagnostic = "/p1 속성이 누락되었습니다. ";
ENGINPUT_ERR_MANDATORYINPUT.Advice     = "기능을 편집하여 이 속성을 설정하십시오. ";

// Null value
ENGINPUT_ERR_NULLVALUE.Request    = "잘못된 기능 /p1. ";
ENGINPUT_ERR_NULLVALUE.Diagnostic = "/p1 속성에 널값이 있습니다. ";
ENGINPUT_ERR_NULLVALUE.Advice     = "기능을 편집하여 널 값이 없도록 속성을 설정하십시오. ";

// Useless limit
ENGINPUT_ERR_USELESSLIMIT.Request    = "잘못된 기능 /p1. ";
ENGINPUT_ERR_USELESSLIMIT.Diagnostic = "한계 /p1이(가) 유용하지 않습니다. ";
ENGINPUT_ERR_USELESSLIMIT.Advice     = "불필요한 데이터를 제거하십시오. ";

// limit of trace
ENGINPUT_ERR_OFFSETORIENTLIMIT.Request    = "잘못된 기능 /p1. ";
ENGINPUT_ERR_OFFSETORIENTLIMIT.Diagnostic = "정의된 대로 한계를 사용할 수 없습니다. ";
ENGINPUT_ERR_OFFSETORIENTLIMIT.Advice     = "한계의 오프셋 또는 방향을 변경하십시오. ";


///////////////////////////////////////////////////////////////////////////
//                           TOPOLOGICAL ERRORS                          //
///////////////////////////////////////////////////////////////////////////
// No body associated
ENGINPUT_ERR_BODY.Request    = "잘못된 기능 /p1. ";
ENGINPUT_ERR_BODY.Diagnostic = "/p2에 본문이 없습니다. ";
ENGINPUT_ERR_BODY.Advice     = "지오메트리를 개선하거나 이 기능을 다시 빌드하십시오. ";

// Nb of domains wanted
ENGINPUT_ERR_DOM.Request    = "잘못된 기능 /p1. ";
ENGINPUT_ERR_DOM.Diagnostic = "/p1의 도메인 수가 /p2입니다. ";
ENGINPUT_ERR_DOM.Advice     = "도메인 수는 /p1이(가) 되어야 합니다. ";

// Multi-domains
ENGINPUT_ERR_MONODOM.Request    = "잘못된 기능 /p1. ";
ENGINPUT_ERR_MONODOM.Diagnostic = "다중 도메인 결과는 금지됩니다. ";
ENGINPUT_ERR_MONODOM.Advice     = "지오메트리를 개선하십시오. ";

// Dimension error
ENGINPUT_ERR_DIM.Request    = "잘못된 기능 /p1. ";
ENGINPUT_ERR_DIM.Diagnostic = "/p1의 차원이 /p2입니다. ";
ENGINPUT_ERR_DIM.Advice     = "/p1이(가) 되어야 합니다. ";

// Infinite plane
ENGINPUT_ERR_INFINITE.Request    = "잘못된 기능 /p1. ";
ENGINPUT_ERR_INFINITE.Diagnostic = "/p1이(가) 무한이 아닙니다. ";
ENGINPUT_ERR_INFINITE.Advice     = "무한 평면을 제공하십시오. ";

// Infinite geometry forbidden
ENGINPUT_ERR_FINITE.Request    = "잘못된 기능 /p1. ";
ENGINPUT_ERR_FINITE.Diagnostic = "/p1이(가) 무한입니다. ";
ENGINPUT_ERR_FINITE.Advice     = "유한 지오메트리를 제공하십시오. ";

// Opened wire
ENGINPUT_ERR_OPENEDWIRE.Request    = "잘못된 기능 /p1. ";
ENGINPUT_ERR_OPENEDWIRE.Diagnostic = "/p1은(는) 열린 와이어입니다. ";
ENGINPUT_ERR_OPENEDWIRE.Advice     = "지오메트리를 개선하십시오. ";

// Closed wire
ENGINPUT_ERR_CLOSEDWIRE.Request    = "잘못된 기능 /p1. ";
ENGINPUT_ERR_CLOSEDWIRE.Diagnostic = "/p1은(는) 닫힌 와이어입니다. ";
ENGINPUT_ERR_CLOSEDWIRE.Advice     = "지오메트리를 개선하십시오. ";

// Multi-intersections
ENGINPUT_ERR_MULTIINTERSECT.Request    = "잘못된 기능 /p1. ";
ENGINPUT_ERR_MULTIINTERSECT.Diagnostic = "/p1의 결과는 다중 교차가 됩니다. ";
ENGINPUT_ERR_MULTIINTERSECT.Advice     = "지오메트리를 개선하십시오. ";

// Projection ambiguity
ENGINPUT_ERR_PROJECTION.Request    = "프로젝션의 모호한 케이스";
ENGINPUT_ERR_PROJECTION.Diagnostic = "";
ENGINPUT_ERR_PROJECTION.Advice     = "3D 곡선은 3D 기준 서피스에 있어야 합니다. ";

// Projection gives no result
ENGINPUT_ERR_NULLPROJECTION.Request    = "프로젝션 실패. ";
ENGINPUT_ERR_NULLPROJECTION.Diagnostic = "/p1의 프로젝션이 실패했습니다. ";
ENGINPUT_ERR_NULLPROJECTION.Advice     = "입력을 수정하십시오. ";

// Intersection gives no result
ENGINPUT_ERR_NULLINTERSECTION.Request    = "교차 실패. ";
ENGINPUT_ERR_NULLINTERSECTION.Diagnostic = "/p1과(와) /p2의 교차가 실패했습니다. ";
ENGINPUT_ERR_NULLINTERSECTION.Advice     = "입력을 수정하십시오. ";

// Intersection gives multi-domains result
ENGINPUT_ERR_INTERSECTIONMULTIDOM.Request    = "다중 도메인 교차. ";
ENGINPUT_ERR_INTERSECTIONMULTIDOM.Diagnostic = "/p1과(와) /p2의 교차가 여러 솔루션을 제공합니다. ";
ENGINPUT_ERR_INTERSECTIONMULTIDOM.Advice     = "입력을 수정하십시오. ";

// Line of null length
ENGINPUT_ERR_NULLLENGTH.Request    = "작업 결과는 길이가 널(null)인 곡선입니다. ";
ENGINPUT_ERR_NULLLENGTH.Diagnostic = "두 점이 혼동됩니다. ";
ENGINPUT_ERR_NULLLENGTH.Advice     = "입력을 수정하십시오. ";

// Intersection Curve-Surface is not a point
ENGINPUT_ERR_INTERSECTCRVSURFNOPOINT.Request    = "교차 실패. ";
ENGINPUT_ERR_INTERSECTCRVSURFNOPOINT.Diagnostic = "/p1과(와) /p2의 교차가 점을 제공하지 않습니다. ";
ENGINPUT_ERR_INTERSECTCRVSURFNOPOINT.Advice     = "입력을 수정하십시오. ";

// Intersection Surface-Surface is not a curve
ENGINPUT_ERR_INTERSECTSURFSURFNOCRV.Request    = "교차 실패. ";
ENGINPUT_ERR_INTERSECTSURFSURFNOCRV.Diagnostic = "/p1과(와) /p2의 교차가 곡선을 제공하지 않습니다. ";
ENGINPUT_ERR_INTERSECTSURFSURFNOCRV.Advice     = "입력을 수정하십시오. ";

// Intersection Surface-Surface-Surface is not a point
ENGINPUT_ERR_INTERSECTSURFSURFNOPT.Request    = "교차 실패. ";
ENGINPUT_ERR_INTERSECTSURFSURFNOPT.Diagnostic = "/p1, /p2 및 /p3의 교차가 점을 제공하지 않습니다. ";
ENGINPUT_ERR_INTERSECTSURFSURFNOPT.Advice     = "입력을 수정하십시오. ";

// Offset too big
ENGINPUT_ERR_PTOFFSETOUTSIDECRV.Request    = "곡선의 점. ";
ENGINPUT_ERR_PTOFFSETOUTSIDECRV.Diagnostic = "오프셋 값 및 방향의 결과는 곡선 외부의 점입니다. ";
ENGINPUT_ERR_PTOFFSETOUTSIDECRV.Advice     = "입력을 수정하십시오. ";

// Minimal segment length
ENGINPUT_ERR_MINSEGMENTLENGTH.Request    = "최소 선분 길이. ";
ENGINPUT_ERR_MINSEGMENTLENGTH.Diagnostic = "곡선 /p1에서 최소 하나의 직선 선분이 최소 길이보다 작습니다. ";
ENGINPUT_ERR_MINSEGMENTLENGTH.Advice     = "스펙을 확인하십시오. ";

// Curvature check for the guide of the sweep
ENGINPUT_ERR_CURVESWEEPCURVATURE.Request    = "곡률 문제점. ";
ENGINPUT_ERR_CURVESWEEPCURVATURE.Diagnostic = "안내 곡선은 이 섹션 크기의 곡률이 너무 작음을 표시합니다. ";
ENGINPUT_ERR_CURVESWEEPCURVATURE.Advice     = "섹션의 크기 또는 안내 곡선의 곡률을 조정하십시오. ";

// Curve lay down on a surface
ENGINPUT_ERR_CURVELAYDOWN.Request    = "곡선이 곡면에 놓이지 않습니다. ";
ENGINPUT_ERR_CURVELAYDOWN.Diagnostic = "안내 곡선 /p1이(가) 선택된 표면 /p2에 놓이지 않습니다. ";
ENGINPUT_ERR_CURVELAYDOWN.Advice     = "지오메트리를 수정하십시오. ";
