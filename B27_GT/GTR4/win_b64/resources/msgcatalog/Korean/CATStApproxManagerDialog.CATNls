//=============================================================================
// COPYRIGHT DASSAULT SYSTEMES  1999
//=============================================================================
//
// CATStApproxManagerDialog: Resource file for NLS purpose related to ApproxManager ( ConverterWizard)  dialog
//
//=============================================================================
//
// Implementation Notes:
//
//=============================================================================
// Sept. 99   Creation                                   TRUONG
//=============================================================================
Title="변환기 마법사";

BMORE="추가>>";
BLESS="제거<<";
INFINITE="무한";

MainFrame.MainTolerFrame.TolerCheck.LongHelp  = "변환 공차 값을 설정하거나 시스템에서 편차를 자동으로 최소화합니다.";
MainFrame.MainTolerFrame.TolerCheck.ShortHelp  = "변환 공차 값을 설정합니다.";
MainFrame.MainTolerFrame.Tolerance.ToleranceLabel.Title     = "공차";
MainFrame.MainTolerFrame.Tolerance.ToleranceLabel.LongHelp  = "변환 공차 값을 설정합니다.";
MainFrame.MainTolerFrame.Tolerance.TolerSpinner.LongHelp  = "변환 공차 값을 설정합니다.
(이 편차 공차의 최대값은 '도구 / 옵션 / 형상 / 자유 양식'에서
정의합니다.)";

MainFrame.MainOrderFrame.OrdersCheck.LongHelp  = "최대 순서 값을 정의합니다. 그렇지 않으면, 시스템이 자동으로 순서를 최적화합니다.";
MainFrame.MainOrderFrame.OrdersCheck.ShortHelp  = "최대 순서 값을 정의합니다.";
MainFrame.MainOrderFrame.OrderFrame.Title      = "순서";
MainFrame.MainOrderFrame.OrderFrame.LongHelp   = "최대 순서 값을 U 및 V 방향으로 정의합니다.
(최대 한계는 '도구 / 옵션 / 형상 / 자유 양식'에서 정의합니다.)";
MainFrame.MainOrderFrame.OrderFrame.Priority1.Title     = "우선순위";
MainFrame.MainOrderFrame.OrderFrame.Priority1.LongHelp  = "순서의 우선순위";
MainFrame.MainOrderFrame.OrderFrame.Priority1.ShortHelp = "순서의 우선순위";

MainFrame.MainOrderFrame.OrderFrame.OrderULabel.Title    = "U에 일치";
MainFrame.MainOrderFrame.OrderFrame.OrderVLabel.Title    = "V에 일치";

MainFrame.MainOrderFrame.OrderFrame.OrderUSpinner.ShortHelp   = "U 방향으로 최대 순서 값을 정의합니다.";
MainFrame.MainOrderFrame.OrderFrame.OrderVSpinner.ShortHelp   = "V 방향으로 최대 순서 값을 정의합니다.";

MainFrame.MainSegmentFrame.SegmentCheck.LongHelp   = "세그먼트의 최대 수를 정의하거나 시스템에서 세그먼트 수를 자동으로 최적화합니다.";
MainFrame.MainSegmentFrame.SegmentCheck.ShortHelp  = "최대 선분 수를 정의합니다.";

MainFrame.MainSegmentFrame.Segmentation.Title      = "분할";

MainFrame.MainSegmentFrame.Segmentation.CheckSegmentFrame.Priority2.Title     = "우선순위";
MainFrame.MainSegmentFrame.Segmentation.CheckSegmentFrame.Priority2.LongHelp  = "분할의 우선순위";
MainFrame.MainSegmentFrame.Segmentation.CheckSegmentFrame.Priority2.ShortHelp = "분할의 우선순위";

MainFrame.MainSegmentFrame.Segmentation.CheckSegmentFrame.SingleCheck.Title   = "단순";
MainFrame.MainSegmentFrame.Segmentation.CheckSegmentFrame.LongHelp   = "단일 세그먼트 요소를 자동으로 변환합니다.";
MainFrame.MainSegmentFrame.Segmentation.CheckSegmentFrame.SingleCheck.ShortHelp  = "단일 세그먼트";

MainFrame.MainSegmentFrame.Segmentation.SegmentFrame.NbSegmentsUFrame.SegmentULabel.Title    = "U에 일치";
MainFrame.MainSegmentFrame.Segmentation.SegmentFrame.NbSegmentsVFrame.SegmentVLabel.Title    = "V에 일치";
MainFrame.MainSegmentFrame.Segmentation.SegmentFrame.NbSegmentsUFrame.LongHelp = "U에 따라 최대 선분 수를 정의합니다.";
MainFrame.MainSegmentFrame.Segmentation.SegmentFrame.NbSegmentsVFrame.LongHelp = "V에 따라 최대 선분 수를 정의합니다.";
MainFrame.MainSegmentFrame.Segmentation.SegmentFrame.NbSegmentsUFrame.SegmentUSpinner.ShortHelp = "U에 따라 최대 선분 수를 정의합니다.";
MainFrame.MainSegmentFrame.Segmentation.SegmentFrame.NbSegmentsVFrame.SegmentVSpinner.ShortHelp = "V에 따라 최대 선분 수를 정의합니다.";

MainFrame.3DConversionFrame.3DConversionRadioFrame.3DConversionRadio.LongHelp   = "2D 커브(서피스의 커브)에서 3D 커브로 변환을 허용합니다.";
MainFrame.3DConversionFrame.3DConversionRadioFrame.3DConversionRadio.ShortHelp  = "3D 변환";

MainFrame.3DConversionFrame.3DConversionRadioFrame.2DConversionRadio.LongHelp   = "2D 커브(서피스의 커브)의 변환 결과는 2D 커브입니다.";
MainFrame.3DConversionFrame.3DConversionRadioFrame.2DConversionRadio.ShortHelp  = "2D 변환";

MainFrame.3DConversionFrame.KeepCheck.Title              = "유지";
MainFrame.3DConversionFrame.KeepCheck.LongHelp           = "새 요소를 작성하고 초기 요소를 유지합니다.";
MainFrame.3DConversionFrame.KeepCheck.ShortHelp          = "새 요소를 작성하고 초기 요소를 유지합니다.";

MoreFrame.DisplayFrame.Title                       ="표시" ; 
MoreFrame.DisplayFrame.InformationCheck.Title      ="정보";
MoreFrame.DisplayFrame.InformationCheck.LongHelp   =
"편차 값, 순서 번호 및 세그먼트 수와 같은 정보를 표시합니다.";
MoreFrame.DisplayFrame.InformationCheck.ShortHelp   =
"편차 값, 순서 번호 및 세그먼트 수와 같은 정보를 표시합니다.";

MoreFrame.DisplayFrame.ControlPointsCheck.Title    ="제어점";
MoreFrame.DisplayFrame.ControlPointsCheck.LongHelp ="요소의 제어점을 표시합니다.";
MoreFrame.DisplayFrame.ControlPointsCheck.ShortHelp ="요소의 제어점을 표시합니다.";

MoreFrame.MorerightFrame.MaxDevLabel.Title                ="최대 편차";
MoreFrame.MorerightFrame.MinCurvatureRadiusLabel.Title    ="최소 반지름"; 
MoreFrame.MorerightFrame.MaxCurvatureRadiusLabel.Title    ="최대 반지름"; 
MoreFrame.MorerightFrame.MaxDevLabel.LongHelp             ="원본 요소와 변환 요소 간의 최대 편차입니다.";
MoreFrame.MorerightFrame.MinCurvatureRadiusLabel.LongHelp ="최소 곡률 반지름"; 
MoreFrame.MorerightFrame.MaxCurvatureRadiusLabel.LongHelp ="최대 곡률 반지름"; 

MoreFrame.MoreRightFrame.MaxDev.AutoCheck.Title    = "자동 적용";
MoreFrame.MoreRightFrame.MaxDev.AutoCheck.LongHelp    = 
"새 매개변수를 고려하여 요소를 동적으로 다시 계산 및 표시합니다.";
MoreFrame.MoreRightFrame.MaxDev.AutoCheck.ShortHelp     = 
"새 매개변수를 고려하여 요소를 동적으로 다시 계산 및 표시합니다.";

MoreFrame.ContinuityFrame.Title="연속성" ; 
MoreFrame.ContinuityFrame.Point.Title="점" ; 
MoreFrame.ContinuityFrame.Tg.Title="접점" ; 
MoreFrame.ContinuityFrame.Crv.Title="곡률" ;
