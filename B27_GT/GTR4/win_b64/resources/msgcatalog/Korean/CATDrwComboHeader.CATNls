//=============================================================================
//                                     CNEXT - CXRn
//                          COPYRIGHT DASSAULT SYSTEMES 1999 
//-----------------------------------------------------------------------------
// FILENAME    :    CATDrwComboHeader
// LOCATION    :    DraftingUI
// AUTHOR      :    fgx
// DATE        :    10.02.99
//------------------------------------------------------------------------------
// DESCRIPTION :    
//                  
//------------------------------------------------------------------------------
// COMMENTS    :	 
//------------------------------------------------------------------------------
//MODIFICATIONS     user  date      purpose
//   HISTORY        ----  ----      -------
//     01		  fgx	  03.03.99	cles pour le NLS des unites
//     02           lgk   14.10.02  Revision
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Font Name
//------------------------------------------------------------------------------
CATDrwComboHeader.DrwFontName.Title="글꼴 이름" ;
CATDrwComboHeader.DrwFontName.ShortHelp="글꼴 이름";
CATDrwComboHeader.DrwFontName.Help="텍스트 일부의 글꼴을 설정합니다." ;
CATDrwComboHeader.DrwFontName.LongHelp =
"글꼴 이름
텍스트 일부의 글꼴을 설정합니다.";

//------------------------------------------------------------------------------
// Font Size
//------------------------------------------------------------------------------
CATDrwComboHeader.DrwFontSize.Title="글꼴 크기" ;
CATDrwComboHeader.DrwFontSize.ShortHelp="글꼴 크기";
CATDrwComboHeader.DrwFontSize.Help="글꼴 크기를 설정합니다." ;
CATDrwComboHeader.DrwFontSize.LongHelp =
"글꼴 크기
글꼴 크기를 설정합니다.";

//------------------------------------------------------------------------------
// Tolerance
//------------------------------------------------------------------------------
CATDrwComboHeader.DrwTolerance.Title="공차" ;
CATDrwComboHeader.DrwTolerance.ShortHelp="공차";
CATDrwComboHeader.DrwTolerance.Help="치수에서 공차를 설정합니다." ;
CATDrwComboHeader.DrwTolerance.LongHelp =
"공차
치수에서 공차를 설정합니다. ";

//------------------------------------------------------------------------------
// Precision
//------------------------------------------------------------------------------
CATDrwComboHeader.DrwPrecision.Title="정밀도" ;
CATDrwComboHeader.DrwPrecision.ShortHelp="정밀도";
CATDrwComboHeader.DrwPrecision.Help="숫자 값을 표시하기 위해 사용되는 정밀도를 설정합니다." ;
CATDrwComboHeader.DrwPrecision.LongHelp =
"정밀도
숫자 값을 표시하기 위해 사용되는 정밀도를 설정합니다.";

//------------------------------------------------------------------------------
// Unit
//------------------------------------------------------------------------------
CATDrwComboHeader.DrwUnit.Title="단위" ;
CATDrwComboHeader.DrwUnit.ShortHelp="단위";
CATDrwComboHeader.DrwUnit.Help="치수 기본 단위를 설정합니다." ;
CATDrwComboHeader.DrwUnit.LongHelp =
"단위
치수 기본 단위를 설정합니다.";

//------------------------------------------------------------------------------
// Numerical Display
//------------------------------------------------------------------------------
CATDrwComboHeader.DrwNumDisp.Title="숫자 표시 설명" ;
CATDrwComboHeader.DrwNumDisp.ShortHelp="숫자 표시 설명";
CATDrwComboHeader.DrwNumDisp.Help="치수 기본 값에 사용되는 숫자 표시의 설명을 설정합니다. " ;
CATDrwComboHeader.DrwNumDisp.LongHelp =
"숫자 표시 설명
치수 기본 값에 사용되는 숫자 표시
설명을 설정합니다.";

//------------------------------------------------------------------------------
// Tolerance Type
//------------------------------------------------------------------------------
CATDrwComboHeader.DrwTolDescrip.Title="공차 설명" ;
CATDrwComboHeader.DrwTolDescrip.ShortHelp="공차 설명";
CATDrwComboHeader.DrwTolDescrip.Help="치수 기본 값에 사용되는 공차의 설명을 설정합니다." ;
CATDrwComboHeader.DrwTolDescrip.LongHelp =
"공차 설명
치수 기본 값에 사용되는
공차의 설명을 설정합니다.";

//------------------------------------------------------------------------------
// Style
//------------------------------------------------------------------------------
CATDrwComboHeader.DrwStyle.Title="양식" ;
CATDrwComboHeader.DrwStyle.ShortHelp="양식";
CATDrwComboHeader.DrwStyle.Help="새 오브젝트를 작성하기 위해 사용되는 스타일을 설정합니다. " ;
CATDrwComboHeader.DrwStyle.LongHelp =
"양식
새 오브젝트를 작성하기 위해 사용되는 양식을 설정합니다.
- 원본: 어플리케이션에 사전 정의된, 도구 모음 값에 의해 과부하된 값,
- 사용자 디폴트: 도구 모음 값에 의해 과부하된 사용자 정의 값,
- 사용자 디폴트만: 과부하되지 않은 사용자 정의 값.";
NORMAL="원본 등록 정보";
USER_DEFAULT="사용자 디폴트 등록 정보";
ONLY_USER_DEFAULT="사용자 디폴트 등록 정보만";

//------------------------------------------------------------------------------
// Keys for Units' NLS
//------------------------------------------------------------------------------
MM="mm";
INCH="inch";

DEG="Deg";
MIN="Deg-Min";
SEC="Deg-Min-Sec";
RAD="Rad";

NO_TOLERANCE="(공차 없음)";

