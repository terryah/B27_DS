//=====================================================================================
//                                     CNEXT - CXR1
//                          COPYRIGHT DASSAULT SYSTEMES 1999 
//-------------------------------------------------------------------------------------
// FILENAME    :    CATDrwOptGeo
// LOCATION    :    DraftingUI
// AUTHOR      :    fgx
// BUT         :    
// DATE        :    06.01.1999
//-------------------------------------------------------------------------------------
// DESCRIPTION :    Tools/Option du Drafting - Onglet Geometry
//-------------------------------------------------------------------------------------
// COMMENTS    :    
//-------------------------------------------------------------------------------------
// MODIFICATIONS     user  date        purpose
//    HISTORY        ----  ----        -------
//      00           fgx   06.01.1999  Creation 	
//      01           fgx   08.01.1999  Passage sur CATDrwOptTitle et CATDrwOptIcon
//      02           fgx   20.01.1999  Ajout des options de visu des cst
//      03           fgx   20.01.1999  Deplacement des options de la grid de geo a gal
//      04           fgx   20.01.1999  Ajout de CreateCenter
//      05           fgx   22.02.1999  Ajout de CstCreation, retrait de CreateConstraint
//      06           fgx   23.08.1999  Manipulation directe de la geometrie
//      07           lgk   18.12.02    revision
//=====================================================================================

Title="지오메트리";

frameCenter.HeaderFrame.Global.Title = "지오메트리";
frameCenter.HeaderFrame.Global.LongHelp = "지오메트리에 추가 요소를 \n작성할 수 있는 성능을 제공합니다.";
frameCenter.IconAndOptionsFrame.OptionsFrame.checkCenter.Title = "원 및 타원 중심 작성";
frameCenter.IconAndOptionsFrame.OptionsFrame.checkCenter.LongHelp = "원과 타원을 작성할 때 \n원 및 타원 중심을 작성합니다.";
frameCenter.IconAndOptionsFrame.OptionsFrame._checkDragWithEndPts.Title = "포함된 요소 끝점 드래그";
frameCenter.IconAndOptionsFrame.OptionsFrame._checkDragWithEndPts.LongHelp = "끝점을 포함하는 요소를 드래그합니다.";
frameCenter.IconAndOptionsFrame.OptionsFrame.chkEndPtsDuplGenGeom.Title = "생성된 지오메트리 중복 시 끝점 작성";
frameCenter.IconAndOptionsFrame.OptionsFrame.chkEndPtsDuplGenGeom.LongHelp = "중복된 3D에서 지오메트리가 생성될 때 끝점을 작성합니다.";
frameCenter.IconAndOptionsFrame.OptionsFrame.chkShowHV.Title = "도구 색상표에 H 및 V 필드 표시";
frameCenter.IconAndOptionsFrame.OptionsFrame.chkShowHV.LongHelp = "요소 오프셋 또는 2D 지오메트리 작성 시 \n도구 색상표에 H 및 V 필드를 표시합니다.";

//drag elements
frameCenter.IconAndOptionsFrame.OptionsFrame.checkGeomDrag.Title="직접 조작 허용 ";
frameCenter.IconAndOptionsFrame.OptionsFrame.checkGeomDrag.LongHelp="마우스를 사용하여 지오메트리를 이동할 수 있도록 합니다.";
frameCenter.IconAndOptionsFrame.OptionsFrame.dragOptionPushButton.Title="해결 모드...";
frameCenter.IconAndOptionsFrame.OptionsFrame.dragOptionPushButton.LongHelp = "이동 요소에 대한 해결 모드를 정의할 수 있도록 합니다.";

frameCstCreation.HeaderFrame.Global.Title="제약조건 작성 ";
frameCstCreation.HeaderFrame.Global.LongHelp=
"검출된 제약조건 및 피처 기반 제약조건
둘 다 작성합니다.";
frameCstCreation.IconAndOptionsFrame.OptionsFrame.checkCstCreation.Title="검출된 제약조건 및 피처 기반 제약조건 작성 ";
frameCstCreation.IconAndOptionsFrame.OptionsFrame.checkCstCreation.LongHelp=
"검출된 제약조건 및 피처 기반 제약조건
둘 다 작성합니다.";
frameCstCreation.IconAndOptionsFrame.OptionsFrame.framePushButton.smartPickPushButton.Title="SmartPick...";
frameCstCreation.IconAndOptionsFrame.OptionsFrame.framePushButton.smartPickPushButton.LongHelp="논리 제약조건의 동적 검출";

frameCstVisu.HeaderFrame.Global.Title="제약조건 표시";
frameCstVisu.HeaderFrame.Global.LongHelp=
"2D 제약조건 표시에 대한 옵션을 정의합니다.";
frameCstVisu.IconAndOptionsFrame.OptionsFrame.LongHelp=
"2D 제약조건 표시에 대한 옵션을 정의합니다.";
frameCstVisu.IconAndOptionsFrame.OptionsFrame.checkCstVisu.Title="제약조건 표시 ";
frameCstVisu.IconAndOptionsFrame.OptionsFrame.checkCstVisu.LongHelp=
"다음 종류의 제약조건을 시각화합니다.";
frameCstVisu.IconAndOptionsFrame.OptionsFrame.labelCstRefSize.Title="레퍼런스 크기: ";
frameCstVisu.IconAndOptionsFrame.OptionsFrame.labelCstRefSize.LongHelp="제약조건 기호를 표시하는 데
레퍼런스로 사용되는 크기를 정의합니다. 이 크기를 변경하면 모든 제약조건 레프리젠테이션의 크기가
변경됩니다.";
frameCstVisu.IconAndOptionsFrame.OptionsFrame.editorCstRefSize.LongHelp="제약조건 기호를 표시하는 데
레퍼런스로 사용되는 크기를 정의합니다. 이 크기를 변경하면 모든 제약조건 레프리젠테이션의 크기가
변경됩니다.";
frameCstVisu.IconAndOptionsFrame.OptionsFrame.labelCstColor.Title="제약조건 색상: ";
frameCstVisu.IconAndOptionsFrame.OptionsFrame.comboCstColor.LongHelp="제약조건 기호를 표시하는 데 사용되는 색상을 정의합니다.
이 색상을 변경하면 모든 제약조건 레프리젠테이션의
색상이 변경됩니다.";
frameCstVisu.IconAndOptionsFrame.OptionsFrame.labelCstColor.LongHelp="제약조건 기호를 표시하는 데 사용되는 색상을 정의합니다.
이 색상을 변경하면 모든 제약조건 레프리젠테이션의
색상이 변경됩니다.";
frameCstVisu.IconAndOptionsFrame.OptionsFrame.frameCTPushButton.cstTypesPushButton.Title="제약조건 유형...";
frameCstVisu.IconAndOptionsFrame.OptionsFrame.frameCTPushButton.cstTypesPushButton.LongHelp="표시할 제약조건의 \n다른 여러 유형을 정의합니다.";


frameGeomManipDirect.HeaderFrame.Global.Title="조작기";
frameGeomManipDirect.HeaderFrame.Global.LongHelp="지오메트리를 이동하기 전에 먼저 누르기로
조작기를 활성화해야 하는지 여부를 정의합니다.";

frameGeomManipDirect.IconAndOptionsFrame.OptionsFrame.checkGeomManipDirect.Title="직접 조작 허용 ";
frameGeomManipDirect.IconAndOptionsFrame.OptionsFrame.checkGeomManipDirect.LongHelp="직접 조작 허용
지오메트리를 이동하기 전에 먼저 누르기로 조작기를 활성화해야
하는지 여부를 정의합니다.";


frameDiagColors.HeaderFrame.Global.Title = "색상  ";
frameDiagColors.HeaderFrame.Global.LongHelp = "색상 \n요소 색상을 정의합니다.";
frameDiagColors.IconAndOptionsFrame.OptionsFrame.checkVisualizationDiag.Title = "진단 시각화 ";
frameDiagColors.IconAndOptionsFrame.OptionsFrame.checkVisualizationDiag.LongHelp = "제한된 요소에 대한 \n진단 시각화를 정의합니다.";
frameDiagColors.IconAndOptionsFrame.OptionsFrame.diagColorsPushButton.Title = "색상...";
frameDiagColors.IconAndOptionsFrame.OptionsFrame.diagColorsPushButton.LongHelp = "진단 색상을 정의합니다.";
frameDiagColors.IconAndOptionsFrame.OptionsFrame.labelOtherColors.Title = "요소의 기타 색상";
frameDiagColors.IconAndOptionsFrame.OptionsFrame.labelOtherColors.LongHelp = "요소의 기타 색상을 정의합니다.";
frameDiagColors.IconAndOptionsFrame.OptionsFrame.otherColorsPushButton.Title = "색상...";
frameDiagColors.IconAndOptionsFrame.OptionsFrame.otherColorsPushButton.LongHelp = "요소의 기타 색상을 정의합니다.";


// NLS push button solving mode dans onglet geometry
geoAllowGeomManip.Title="요소 드래그";
geoAllowGeomManip.frameButtonDragElts.HeaderFrame.Global.Title = "요소 드래그  ";
geoAllowGeomManip.frameButtonDragElts.HeaderFrame.Global.LongHelp = "요소 이동 방식을 정의합니다.";
geoAllowGeomManip.frameButtonDragElts.IconAndOptionsFrame.OptionsFrame.checkDragWithEndPts.Title = "포함된 요소 끝점 드래그";
geoAllowGeomManip.frameButtonDragElts.IconAndOptionsFrame.OptionsFrame.checkDragWithEndPts.LongHelp = "끝점을 포함하여 요소를 드래그합니다.";
geoAllowGeomManip.frameButtonDragElts.IconAndOptionsFrame.OptionsFrame.frameDragMode.Title = "해석 모드 ";//for moving elements";
geoAllowGeomManip.frameButtonDragElts.IconAndOptionsFrame.OptionsFrame.frameDragMode.frameDragModelocked.radioDragModeStd.Title = "표준 모드";
geoAllowGeomManip.frameButtonDragElts.IconAndOptionsFrame.OptionsFrame.frameDragMode.frameDragModelocked.radioDragModeStd.LongHelp = "요소를 이동할 때 표준 모드를 사용합니다.";
geoAllowGeomManip.frameButtonDragElts.IconAndOptionsFrame.OptionsFrame.frameDragMode.frameDragModelocked.radioDragModeMin.Title = "최소 이동";
geoAllowGeomManip.frameButtonDragElts.IconAndOptionsFrame.OptionsFrame.frameDragMode.frameDragModelocked.radioDragModeMin.LongHelp = "최소 이동을 수행합니다.";
geoAllowGeomManip.frameButtonDragElts.IconAndOptionsFrame.OptionsFrame.frameDragMode.frameDragModelocked.radioDragModeRel.Title = "완화";
geoAllowGeomManip.frameButtonDragElts.IconAndOptionsFrame.OptionsFrame.frameDragMode.frameDragModelocked.radioDragModeRel.LongHelp = "완화를 사용합니다.";

// Push Button SmartPick
geoSmartPick.Title="SmartPick";
geoSmartPick.frameSmartPick.HeaderFrame.Global.Title="SmartPick";
geoSmartPick.frameSmartPick.HeaderFrame.Global.LongHelp=
"SmartPick
geoSmartPick.2D 지오메트리 요소의 위치를 자동으로 검출합니다.";
geoSmartPick.frameSmartPick.IconAndOptionsFrame.OptionsFrame.checkSmartPickCoincid.Title="선 및 원 지원";
geoSmartPick.frameSmartPick.IconAndOptionsFrame.OptionsFrame.checkSmartPickCoincid.LongHelp=
"선 및 원 지원을 검출합니다.";
geoSmartPick.frameSmartPick.IconAndOptionsFrame.OptionsFrame.checkSmartPickAlign.Title="맞추기 ";
geoSmartPick.frameSmartPick.IconAndOptionsFrame.OptionsFrame.checkSmartPickAlign.LongHelp=
"두 요소가 서로 배열됨을 정의합니다.";
geoSmartPick.frameSmartPick.IconAndOptionsFrame.OptionsFrame.checkSmartPickParaPerpenTang.Title="평행, 수직 및 접점";
geoSmartPick.frameSmartPick.IconAndOptionsFrame.OptionsFrame.checkSmartPickParaPerpenTang.LongHelp=
"두 요소가 서로 평행하거나, 수직이거나,
접해 있음을 정의합니다.";
geoSmartPick.frameSmartPick.IconAndOptionsFrame.OptionsFrame.checkSmartPickHoriVerti.Title="수평 및 수직";
geoSmartPick.frameSmartPick.IconAndOptionsFrame.OptionsFrame.checkSmartPickHoriVerti.LongHelp=
"두 요소가 서로 수평이거나 수직임을 정의합니다.";

// Constraints types
geoTypeCst.Title="제약조건 유형";
geoTypeCst.frameConstraintsTypes.HeaderFrame.Global.Title="제약조건 유형";
geoTypeCst.frameConstraintsTypes.HeaderFrame.Global.LongHelp="시각화할 제약조건의 여러 \n유형을 정의합니다.";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuHori.Title="수평 ";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuHori.LongHelp="수평
수평 제약조건을 표시합니다.";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuVerti.Title="수직";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuVerti.LongHelp="수직
수직 제약조건을 표시합니다.";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuPara.Title="평행 ";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuPara.LongHelp="평행
평행 제약조건을 표시합니다.";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuPerpen.Title="수직 ";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuPerpen.LongHelp="수직
수직 제약조건을 표시합니다.";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuConcen.Title="등심성 ";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuConcen.LongHelp="등심성
등심성 제약조건을 표시합니다.";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuCoinci.Title="일치";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuCoinci.LongHelp="일치
일치 제약조건을 표시합니다.";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuTangen.Title="접합 ";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuTangen.LongHelp="접합
접합 제약조건을 표시합니다.";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuSym.Title="대칭 ";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuSym.LongHelp="대칭
대칭 제약조건을 표시합니다.";

// NLS de l'onglet Geometry pour le diagnostic des couleurs
geoVisuDiagColor.Title="진단 색상";
geoVisuDiagColor.frameButtonDiagColors.HeaderFrame.Global.Title = "진단 색상";
geoVisuDiagColor.frameButtonDiagColors.HeaderFrame.Global.LongHelp = "진단 색상 \n진단 색상을 정의합니다.";
geoVisuDiagColor.frameButtonDiagColors.IconAndOptionsFrame.OptionsFrame.labelOverConsElts.Title = "Over-constrained 요소 ";
geoVisuDiagColor.frameButtonDiagColors.IconAndOptionsFrame.OptionsFrame.labelOverConsElts.LongHelp = "Over-constrained 요소 \nOver-constrained 모델의 요소에 사용되는 색상을 정의합니다.";
geoVisuDiagColor.frameButtonDiagColors.IconAndOptionsFrame.OptionsFrame.labelIsoConsElts.Title = "Iso-constrained 요소 ";
geoVisuDiagColor.frameButtonDiagColors.IconAndOptionsFrame.OptionsFrame.labelIsoConsElts.LongHelp = "Iso-constrained 요소 \nIso-constrained 모델의 요소에 사용되는 색상을 정의합니다.";
geoVisuDiagColor.frameButtonDiagColors.IconAndOptionsFrame.OptionsFrame.labelInconsElts.Title = "일치하지 않는 요소 ";
geoVisuDiagColor.frameButtonDiagColors.IconAndOptionsFrame.OptionsFrame.labelInconsElts.LongHelp = "일치하지 않는 요소 \n일치하지 않는 모델의 요소에 사용되는 색상을 정의합니다.";
geoVisuDiagColor.frameButtonDiagColors.IconAndOptionsFrame.OptionsFrame.labelNotChanElts.Title = "변경되지 않은 요소 ";
geoVisuDiagColor.frameButtonDiagColors.IconAndOptionsFrame.OptionsFrame.labelNotChanElts.LongHelp = "변경되지 않은 요소 \n변경되지 않은 요소에 사용되는 색상을 정의합니다.";

// NLS de l'onglet Geometry pour Other Colors
geoOtherColor.Title="다른 요소의 색상";
geoOtherColor.frameButtonOtherColors.HeaderFrame.Global.Title = "색상  ";
geoOtherColor.frameButtonOtherColors.HeaderFrame.Global.LongHelp = "색상 \n요소의 색상을 정의합니다.";
geoOtherColor.frameButtonOtherColors.IconAndOptionsFrame.OptionsFrame.labelConstElts.Title = "구성 요소(V5R11 이전 도면의 경우) ";
geoOtherColor.frameButtonOtherColors.IconAndOptionsFrame.OptionsFrame.labelConstElts.LongHelp = "구성 요소 \nV5R11 이전 도면에 대한 구성 요소에 필요한 색상을 정의합니다.";
geoOtherColor.frameButtonOtherColors.IconAndOptionsFrame.OptionsFrame.labelSmartPick.Title = "SmartPick  ";
geoOtherColor.frameButtonOtherColors.IconAndOptionsFrame.OptionsFrame.labelSmartPick.LongHelp = "SmartPick 요소 \nSmartPick 요소에 사용되는 색상을 정의합니다.";
