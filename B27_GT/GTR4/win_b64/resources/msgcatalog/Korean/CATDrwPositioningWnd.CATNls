//=====================================================================================
//                                     CNEXT - CXRn
//                          COPYRIGHT DASSAULT SYSTEMES 1999 
//-------------------------------------------------------------------------------------
// FILENAME    :    CATDrwPositioningWnd
// LOCATION    :    DraftingIntCommands
// AUTHOR      :    fgx
// BUT         :    Panel de choix de positionnement
// DATE        :    15.12.1999
//-------------------------------------------------------------------------------------
// DESCRIPTION :    
//-------------------------------------------------------------------------------------
// COMMENTS    :    
//-------------------------------------------------------------------------------------
// MODIFICATIONS     user  date        purpose
//    HISTORY        ----  ----        -------
//   	01		    lgk   10.12.02 	   Revision
//=====================================================================================

Title="위치 지정";
Help="요소를 정렬하거나, 간격 지정을 정의하거나, 요소를 배포하거나 이동합니다.";

_titleAlign.Title="정렬";
_titleAlign.LongHelp="오브젝트 중 하나에 오브젝트를 정렬합니다.";
_frameAlign._pushLeft.ShortHelp="왼쪽 정렬";
_frameAlign._pushCenter.ShortHelp="중심 정렬";
_frameAlign._pushRight.ShortHelp="오른쪽 정렬";
_frameAlign._pushTop.ShortHelp="맨 위 정렬";
_frameAlign._pushMiddle.ShortHelp="가운데 정렬";
_frameAlign._pushBottom.ShortHelp="맨 아래 정렬";

_frameAlign._pushLeft.LongHelp="왼쪽 정렬
모든 오브젝트를 가장 왼쪽에 있는 오브젝트에 정렬합니다.";
_frameAlign._pushCenter.LongHelp="중심 정렬
모든 오브젝트를 가장 중심에 있는 오브젝트에 정렬합니다.";
_frameAlign._pushRight.LongHelp="오른쪽 정렬
모든 오브젝트를 가장 오른쪽에 있는 오브젝트에 정렬합니다.";
_frameAlign._pushTop.LongHelp="위 정렬
모든 오브젝트를 맨 위에 있는 오브젝트에 정렬합니다.";
_frameAlign._pushMiddle.LongHelp="가운데 정렬
모든 오브젝트를 가장 가운데에 있는 오브젝트에 정렬합니다.";
_frameAlign._pushBottom.LongHelp="아래 정렬
모든 오브젝트를 맨 아래에 있는 오브젝트에 정렬합니다.";



_titleSpace.Title="공간";
_titleSpace.LongHelp="오브젝트 간의 간격 지정을 정의합니다.";
_frameSpace._spinnerSpace.LongHelp="오브젝트 간 간격 지정을 정의할 때 고려해야 하는 값";
_frameSpace._pushMinX.ShortHelp="왼쪽에서 오른쪽으로의 공간";
_frameSpace._pushMaxX.ShortHelp="오른쪽에서 왼쪽으로의 공간";
_frameSpace._pushMinY.ShortHelp="맨 아래에서 맨 위로의 공간";
_frameSpace._pushMaxY.ShortHelp="맨 위에서 맨 아래로의 공간";

_frameSpace._pushMinX.LongHelp="왼쪽에서 오른쪽으로의 공간
가장 왼쪽에 있는 오브젝트를 레퍼런스로 취하며 오브젝트 간 간격 지정을 정의합니다. ";
_frameSpace._pushMaxX.LongHelp="오른쪽에서 왼쪽으로의 공간
가장 오른쪽에 있는 오브젝트를 레퍼런스로 취하며 오브젝트 간 간격 지정을 정의합니다. ";
_frameSpace._pushMinY.LongHelp="맨 아래에서 맨 위로의 공간
가장 아래에 있는 오브젝트를 레퍼런스로 취하며 오브젝트 간 간격 지정을 정의합니다. ";
_frameSpace._pushMaxY.LongHelp="맨 위에서 맨 아래로의 공간
가장 위에 있는 오브젝트를 레퍼런스로 취하며 오브젝트 간 간격 지정을 정의합니다. ";



_titleDistrib.Title="배포";
_titleDistrib.LongHelp="가장 거리가 먼 두 오브젝트 사이에 오브젝트를 배포합니다.";
_frameDistrib._pushDistribX.ShortHelp="수평으로 배포";
_frameDistrib._pushDistribY.ShortHelp="수직으로 배포";

_frameDistrib._pushDistribX.LongHelp="수평으로 배포
가장 왼쪽에 있는 오브젝트와 가장 오른쪽에 있는 오브젝트 사이에 오브젝트를 배포합니다. ";
_frameDistrib._pushDistribY.LongHelp="수직으로 배포
맨 위에 있는 오브젝트와 맨 아래에 있는 오브젝트 사이에 오브젝트를 배포합니다. ";



_titleMove.Title="이동";
_titleMove.LongHelp="오브젝트를 블록으로 이동합니다. ";
_frameMove._spinnerMove.LongHelp="오브젝트를 이동할 때 고려해야 하는 값. ";
_frameMove._pushIncX.ShortHelp="수직으로 이동";
_frameMove._pushIncY.ShortHelp="수평으로 이동";

_frameMove._pushIncX.LongHelp="수평으로 이동
오브젝트를 수평으로 이동합니다. ";
_frameMove._pushIncY.LongHelp="수직으로 이동
오브젝트를 수직으로 이동합니다. ";


