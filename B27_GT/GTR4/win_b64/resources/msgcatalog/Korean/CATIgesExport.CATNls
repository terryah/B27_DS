//NLS-ENGLISH VERSION
//=======================================================================
// MODULE :  IgesDataExchange
//=======================================================================


// KVL 2002-06-04 : Creation de CATIgesExport.CATNls et refonte des anciens CATNls Iges.
// DFB 2002-03-07 : Ajout de CircleReparam 
// KVL 2002-04-15 : CATIgesCompositeCurve  
// KVL 2002-05-30 : CATIgesInit : info sur le choix des options BSpline ou Standard a l'export 
// KVL 2002-06-07 : CATIgesInit : info sur le choix de l'export des entites en NoShow et des options wireframe ou Surfacic
// KVL 2002-06-19 : CATIgesVirtualBody et CATIgesElement : Info sur chaque element cree (Type, Show, Layer, Name, Color)
// KVL 2002-07-03 : CATIgesCurveOnParametricSurface : echec de creation des PCurve.
// OPC 2006-06-23 : Reecriture des messages niveau Customer pour meilleure comprehension

// Numeros utilises : (export Iges : de [6001] a [6999]
// [6001] a [6049] : CATIgesBSplineCurve
// [6050] a [6099] : CATIgesCompositeCurve
// [6100] a [6149] : CATIgesInit
// [6150] a [6199] : CATIgesVirtualBody
// [6200] a [6249] : CATIgesElement
// [6250] a [6299] : CATIgesCurveOnParametricSurface
// [6300] a [6349] : CATIgesTrimmedParametricSurface
// [6350] a [6370] : CATSaveAsIGS
// [6850] a [6860] : CATIgesInterface

//----------------------------------------------
//  Messages from CATIgesBSplineCurve
//---------------------------------------------

CATIgesBSplineCurve.Info.CircleReparam="   <I> [6001] [T=/p2] [/p1] 반출된 원의 매개변수가 재결정됨 - /p3";

CATIgesBSplineCurve.Info.ParamInverse="   <I> [6002] [T=/p2] [/p1] BSpline 매개변수가 전환됨 - /p3";

CATIgesBSplineCurve.Info.BSplineFermee="   <I> [6003] [T=/p2] [/p1] BSpline이 닫힘 - /p3";

//-----------------------------------------------
//  Messages from CATIgesCompositeCurve
//-----------------------------------------------

CATIgesCompositeCurve.Info.hierar="   <I> [6050] [T=/p2] [#/p4] 이 엔티티에 의존하는 모든 엔티티가 해당 속성을 상속함 - /p3";



//-----------------------------------------------
//  Messages from CATIgesInit
//-----------------------------------------------

CATIgesInit.Info.optionBSpline="   <I> [6100] BSpline 반출 옵션을 선택했음 - /p3 \n";

CATIgesInit.Info.optionStandard="   <I> [6101] 표준 반출 옵션을 선택했음 - /p3 \n";

CATIgesInit.Info.optionNoShowRef="   <I> [6102] NoShow 엔티티를 반출하지 않도록 선택했음 - /p3 \n";

CATIgesInit.Info.optionNoShowAcc="   <I> [6103] NoShow 엔티티를 반출하도록 선택했음 - /p3 \n";

CATIgesInit.Info.optionWireframe="   <I> [6104] 와이어프레임 반출 옵션을 선택했음 - /p3 \n";

CATIgesInit.Info.optionSurfacic="   <I> [6105] Surfacic 반출 옵션을 선택했음 - /p3 \n";

CATIgesInit.Info.optionMSBO="   <I> [6106] MSBO 반출 옵션을 선택했음(솔리드, 쉘 및 페이스에 대해) - /p3 \n";


//-----------------------------------------------
//  Messages from CATIgesVirtualBody (messages utilises parfois dans CATIgesCldBody)
//-----------------------------------------------

CATIgesVirtualBody.Info.ProcessingBody="\n***본문 처리 ";

CATIgesVirtualBody.Info.ProcessingElement="\n **요소 처리 [T=/p2] [#/p1]";

CATIgesVirtualBody.Info.Name="   <I> [6150] [T=/p2] [#/p1] V5 엔티티 이름이 /p3임";

CATIgesVirtualBody.Info.NoName="   <I> [6151] [T=/p2] [#/p1] 이 엔티티에 대해 이름이 지정되지 않았습니다.";

CATIgesVirtualBody.Info.NbElem="   <I> [6152] 본문이 /p2 /p1 /p3에 의해 구성됨";

CATIgesVirtualBody.Info.Wire="   <I> [6153] [T=/p2] [#/p1] 와이어가 닫힘";

CATIgesVirtualBody.Error.ElementNotExported="!! <E> [6154] [T=/p2] [#/p1] 유효하지 않은 입력 데이터로 인해 이 요소를 반출할 수 없습니다. /p3";


//-----------------------------------------------
//  Messages from CATIgesElement
//-----------------------------------------------

CATIgesElement.Info.NoShow="   <I> [6200] [T=/p2] [#/p1] NoShow 엔티티";

CATIgesElement.Info.Layer="   <I> [6201] [T=/p2] [#/p1] 레이어가 /p3임";

CATIgesElement.Info.Color="   <I> [6202] [T=/p2] [#/p1] 엔티티 색상이 R=/p3 G=/p4 B=/p5임";

CATIgesElement.Warning.IgsControlName="   <W> [6203] 엔티티 이름이 유효하지 않음 : /p1 문자가 이 쓰기 액세스에서 충족하도록 허용되지 않습니다. 정리 시도가 수행되었습니다.";

//-----------------------------------------------
//  Messages from CATIgesCurveOnParametricSurface
//-----------------------------------------------
CATIgesCurveOnParametricSurface.Warning.PCurveNotCreated="   <W> [6250] [T=/p2] [#/p1] PCurve IGES가 작성되지 않음 - /p3";


//-----------------------------------------------
//  Messages from CATIgesTrimmedParametricSurface
//-----------------------------------------------
CATIgesTrimmedParametricSurface.Warning.PCurveNotExported="   <W> [6300] [T=/p2] [#/p1] PCurve IGES가 예상되지 않음 - /p3";

//-----------------------------------------------
//  Messages from CATSaveAsIGS
//-----------------------------------------------
CATSaveAsIGS.TranscriptChild.NoChild="   <W> [6350] 하위가 없음! ";
CATSaveAsIGS.Error.ConvertV4ToV5="   <E> [6351] V4 모델을 CATPart로 변환에 실패! ";
CATSaveAsIGS.Info.ConvertV4ToV5="   <I> [6352] V4 모델 이름 지정: /p1이(가) 변환됨";

//--------------------------------------
//  Messages from CATIgesInterface
//--------------------------------------

CATDXFileServices.Error.InvalidName="!! <E> [6859] 저장하려는 IGES 파일의 이름이 올바르지 않음(오류 PathERR_1021):
               파일 이름이 비ASCII 문자를 포함하거나 비어 있습니다.
               파일 이름의 올바르지 않은 문자를 확인하십시오. 필요한 경우, 다른 이름을 선택한 다음 저장을 다시 시도하십시오.";


//------------------------------------------
//  Messages for CATIReporter 
//------------------------------------------

PathERR_1021.Request="저장하려는 IGES 파일의 이름이 올바르지 않습니다.";
PathERR_1021.Diagnostic="파일 이름이 비ASCII 문자를 포함하거나 비어 있습니다.";
PathERR_1021.Advice="파일 이름의 올바르지 않은 문자를 확인하십시오. 필요한 경우, 다른 이름을 선택한 다음 저장을 다시 시도하십시오.";

