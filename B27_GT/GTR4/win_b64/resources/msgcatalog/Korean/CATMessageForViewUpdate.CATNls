//=============================================================================
//                                     CNEXT - CXR9
//                          COPYRIGHT DASSAULT SYSTEMES 2002 
//-----------------------------------------------------------------------------
// FILENAME    :    CATMessageForViewUpdate
// LOCATION    :    DraftingGenModeler/CNext/resources/msgcatalog
// AUTHOR      :    cna
// DATE        :    fevrier 2002
//------------------------------------------------------------------------------
// DESCRIPTION :    Resources File for NLS purpose
//                  
//------------------------------------------------------------------------------
// COMMENTS    :    Il s'agit de la definition des messages courts figurant
//                  dans la progress barre d'update de vue.
//------------------------------------------------------------------------------
//MODIFICATIONS     user  date      purpose
//   HISTORY        ----  ----      -------
//     01           cna   14/02/02  creation
//     02           emk   16/10/03  ajout des messages d'erreurs pouvant survenir durant l'update des vues
//------------------------------------------------------------------------------

Geo3D="단계 1/5";
PrePro="단계 2/5";
HLR="단계 3/5";
PostPro="단계 4/5";
Dressup="단계 5/5";

ViewName = "뷰: ";
SheetName = "시트: ";
Message = "설명: ";

UnspecifiedViewError = "뷰 생성에 실패했습니다.";
BooleanPartViewError = "파트 '/p1'의 본문 '/p2'에 대한 교차 계산에 실패했습니다.";
BooleanModelViewError = "모델 '/p1'에 대한 교차 계산에 실패했습니다.";
BooleanSectionCutViewError = "파트 '/p1'의 본문 '/p2'에 대한 섹션 잘라내기 계산에 실패했습니다.";
RasterViewError = "뷰가 래스터 모드에 있으며, 이미지 크기(/p1 x /p2)가 메모리 용량을 초과합니다. \n뷰의 축척 및/또는 뷰 등록 정보에서 세부사항 레벨을 축소하려고 시도합니다.";
RasterBuildImageViewError = "이미지를 빌드할 수 없습니다. \n뷰의 축척 및/또는 뷰 등록 정보에서 세부사항 레벨을 축소하려고 시도합니다.";
QuickDetailViewError = "단순 모드 세부사항 뷰생성에 실패했습니다.";
NoGeometryViewError = "이 뷰에 지오메트리가 포함되지 않습니다.";
BrokenViewError = "중단된 뷰 생성에 실패했습니다.";
MemoryViewError = "메모리 할당에 실패했습니다. \n처리할 데이터의 양 및/또는 뷰 생성 설정이 메모리 용량을 초과합니다. \n작업을 저장하고 세션을 다시 시작해야 합니다.";
AIXApproximateViewError = "예측 뷰 생성 모드가 AIX 운영 체제에서 작동하지 않습니다.";
BreakoutApproximateError = "최소 하나의 브레이크아웃이 이 예측 뷰와 일치하지 않습니다. \n예측 뷰에서 브레이크아웃 방향은 뷰 평면의 법선 및 반대 방향에 평행해야 합니다. \n뷰를 생성하려면 브레이크아웃을 제거하십시오.";
BreakoutSectionApproximateError = "최소 하나의 브레이크아웃이 이 예측 섹션 뷰와 일치하지 않습니다. \n예측 섹션 뷰에서 브레이크아웃 배면은 리모터 섹션 평면 뒤에 있어야 합니다. \n뷰를 생성하려면 이 브레이크아웃을 제거하십시오.";
SectionProfileError = "섹션 프로파일 정의가 누락됩니다. 더 이상 뷰를 갱신할 수 없습니다.";
GetMaxViewportSizeError = "내부 오류입니다. GetMaxViewportSize를 리턴합니다(/p1 x /p2).";
NoElementToProject = "처리할 요소가 없습니다.";
EScaleIncompatibleView = "뷰 생성 모드는 소규모 및 대규모 축척 모드에서 지원되지 않음";
LowMemoryError = "메모리 사용이 한계에 도달했습니다.\n뷰 작성/갱신을 계속하기 위해 '/p1' 문서를 로드할 수 없습니다.";

ApproxSectionViewWarning = "경고: 예측 섹션 뷰입니다. 섹션 프로파일이 연계되지 않으며 해당 위치를 확인해야 합니다.";
SynchroDuringUpdate="갱신 프로세스 중에 FTA 또는 2DL 뷰의 동기화에 실패함";
