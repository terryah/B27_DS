INFO_HELP="
CATDMUDistributor는 목록에 제공된 DMU 데이터(CATProduct,
   관련 캐시 데이터 및 기타)를 현재 위치에서 먼 위치로 복사하거나 복사할 파일
   목록을 계산합니다. \n
사용법: \n
   도움말 정보 검색:
      CATDMUDistributor -h\n
   DMU 데이터를 현재 위치에서 먼 위치로 복사:
      CATDMUDistributor inputdir cachedir docdir [-s char]\n
   복사할 파일 목록 계산:
      CATDMUDistributor inputdir cachedir docdir -list file [-s char]\n
Windows 예제: \n
   CATDMUDistributor -h
   CATDMUDistributor c:\input e:\cache e:\document\n
Unix 예제: \n
   CATDMUDistributor -h
   CATDMUDistributor /u/input /v/cache /v/document\n
인수: \n
   -h            : 도움말.
   inputdir      : 문서를 나열하는 파일을 포함하는 디렉토리를 정의합니다.
                   이 파일에는 한 행에서부터 여러 행이 포함됩니다.
                   각 행에는 문서 ID가 있어야 합니다.
                   파일 기반 문서의 경우 ID는 문서의
                   경로입니다(결국 'CATDLN://'이 앞에 붙은 DLName을 포함합니다.).
                   VPM 기반 문서의 경우 ID는 'COID COMPID CAENV CATAB'입니다.
   cachedir      : 먼 캐시 디렉토리를 정의합니다.
   docdir        : 캐시 데이터를 제외하고 모든 문서를 포함할 먼 디렉토리를
                   정의합니다.
                   이 디렉토리는 앞에 'CATDLN://'이 붙은 DLName이 될 수 있습니다.
   -list file    : 복사할 문서 목록을 포함할 출력 파일.
                   각 행에는 원래 데이터의 경로와 복사 데이터의 경로가
                   공백으로 분리되어 있습니다.
   -s char       : 입력 또는 출력 파일의 행에서 필드를 분리하는 데 사용되는
                   문자를 정의합니다.
                   디폴트로 공백 문자가 사용됩니다.\n
진단: \n
   가능한 종료 상태 값: \n
      0   성공적인 완료. \n
      1   다음 이유로 실패:
          - 라이센스를 사용할 수 없음
          - 캐시 관리 비활성화
          - 인수 누락
          - 유효하지 않은 매개변수
          - 옵션 이후 매개변수 누락
          - 입력 디렉토리 누락
          - 캐시 디렉토리 누락
          - 문서 디렉토리 누락
          - 유효하지 않은 디렉토리
          - 입력 파일을 열 수 없음
          - 출력 파일에 쓸 수 없음
      2   처리 오류. \n
      3   부분적으로 처리됨. \n
문제점 해결: \n
   종료 상태 = 1:
      표준 오류 파일에 포함된 정보를 사용하여 명령행이나
      환경을 수정하십시오. \n
   종료 상태 = 2:
      로컬 지원에 문의하십시오.\n
   종료 상태 = 3:
      문제점을 야기하는 파일을 수정하십시오. 이름은 표준 오류 파일에
      있습니다. 파일의 대화식 사용에서 문제점을 알 수 있습니다.
";
ERR_PROCESSING="오류: CATDMUDistributor 처리에 실패했습니다.";
ERR_MISSING_INPUT_DIR="오류: 입력 디렉토리가 정의되어야 합니다.";
ERR_MISSING_CACHE_DIR="오류: 캐시 디렉토리가 정의되어야 합니다.";
ERR_MISSING_DOCUMENT_DIR="오류: 문서 디렉토리가 정의되어야 합니다.";
ERR_COPY="오류: 다음 문서에 해당되는 캐시 데이터를 복사할 수 없습니다. ";
OK_COPY="확인: 다음 문서에 해당되는 캐시 데이터가 복사되었습니다.";
NO_COPY="오류: 다음 문서에 해당되는 캐시 데이터가 없습니다.";
START_BATCH="CATDMUDistributor 시작 위치:";
STOP_BATCH="CATDMUDistributor는 다음 지속 기간 후 중지함 : ";
