//=====================================================================================
//                                     CNEXT - CXRn
//                          COPYRIGHT DASSAULT SYSTEMES 1999 
//-------------------------------------------------------------------------------------
// FILENAME    :    CATDrwFrameManipulator
// LOCATION    :    DraftingUI
// AUTHOR      :    fgx
// BUT         :    
// DATE        :    26.11.1999
//-------------------------------------------------------------------------------------
// DESCRIPTION :    Tools/Option du Drafting
//-------------------------------------------------------------------------------------
// COMMENTS    :    
//-------------------------------------------------------------------------------------
// MODIFICATIONS     user  date        purpose
//    HISTORY        ----  ----        -------
//=====================================================================================

frameManip.HeaderFrame.Global.Title="조작기 ";
frameManip.HeaderFrame.Global.LongHelp="조작기를 표시하기 위해 사용되는
디폴트 매개변수를 정의합니다.";

frameManip.IconAndOptionsFrame.OptionsFrame.labelManipRefSize.Title="레퍼런스 크기: ";
frameManip.IconAndOptionsFrame.OptionsFrame.labelManipRefSize.LongHelp="조작기를 표시하기 위해 사용되는
레퍼런스 크기를 정의합니다.";
frameManip.IconAndOptionsFrame.OptionsFrame.editorManipRefSize.LongHelp="조작기를 표시하기 위해 사용되는
레퍼런스 크기를 정의합니다.";

frameManip.IconAndOptionsFrame.OptionsFrame.checkManipZoom.Title="확대/축소 가능 ";
frameManip.IconAndOptionsFrame.OptionsFrame.checkManipZoom.LongHelp="조작기를 지오메트리처럼 확대/축소 가능한지 여부를 정의합니다.";


frameRotation.HeaderFrame.Global.Title="회전";
frameRotation.HeaderFrame.Global.LongHelp="회전 선택-회전 명령을 사용하여 요소를 회전할 때
고려할 매개변수를 정의합니다.";
frameRotation.IconAndOptionsFrame.OptionsFrame.checkRotSnapAuto.Title="자동 스냅";
frameRotation.IconAndOptionsFrame.OptionsFrame.labelRotSnapAngle.Title="회전 스냅 각도: ";
frameRotation.IconAndOptionsFrame.OptionsFrame.labelRotSnapAngle.LongHelp="회전 스냅 각도
선택-회전 명령을 사용하여 요소를 회전할 때
사용되는 스냅 값을 정의합니다.";


frameOrientation.HeaderFrame.Global.Title="방향";
frameOrientation.HeaderFrame.Global.LongHelp="지시선의 방향을 지정할 때 사용되는 매개변수를 정의합니다.";
frameOrientation.IconAndOptionsFrame.OptionsFrame.checkSnapOnDirection.Title="권한이 있는 방향에 스냅";
frameOrientation.IconAndOptionsFrame.OptionsFrame.checkSnapOnDirection.LongHelp="권한이 있는 방향(수평 또는 수직, 레퍼런스와 직각을 이루거나 레퍼런스에 접함)에 주석 지시선 또는 화살표를 스냅시킵니다.";


frameDim.HeaderFrame.Global.Title="치수 조작기";
frameDim.HeaderFrame.Global.LongHelp="치수를 작성하거나 수정할 때 표시할
디폴트 조작기를 정의합니다.";

frameDim.IconAndOptionsFrame.OptionsFrame._labelDimCreation.Title="작성";
frameDim.IconAndOptionsFrame.OptionsFrame._labelDimCreation.LongHelp="치수를 작성할 때 표시할
조작기를 정의합니다.";
frameDim.IconAndOptionsFrame.OptionsFrame._labelDimModification.Title="변경사항";
frameDim.IconAndOptionsFrame.OptionsFrame._labelDimModification.LongHelp="치수를 수정할 때 표시할
조작기를 정의합니다.";

frameDim.IconAndOptionsFrame.OptionsFrame._labelDimORun.Title="오버런 수정: ";
frameDim.IconAndOptionsFrame.OptionsFrame._labelDimORun.LongHelp="오버런 수정 조작기에서는 대화식으로 각 확장선의
오버런을 수정할 수 있습니다.";

frameDim.IconAndOptionsFrame.OptionsFrame._labelDimBlank.Title="공백화 수정: ";
frameDim.IconAndOptionsFrame.OptionsFrame._labelDimBlank.LongHelp="공백화 수정 조작기에서는 대화식으로 각 확장선의
공백화를 수정할 수 있습니다.";

frameDim.IconAndOptionsFrame.OptionsFrame._labelDimBefore.Title="앞에 텍스트 삽입: ";
frameDim.IconAndOptionsFrame.OptionsFrame._labelDimBefore.LongHelp="앞에 텍스트 삽입 조작기에서는 치수 기본 값 앞에
텍스트를 쉽게 삽입할 수 있습니다.";

frameDim.IconAndOptionsFrame.OptionsFrame._labelDimAfter.Title="뒤에 텍스트 삽입: ";
frameDim.IconAndOptionsFrame.OptionsFrame._labelDimAfter.LongHelp="뒤에 텍스트 삽입 조작기에서는 치수 기본 값 뒤에
텍스트를 쉽게 삽입할 수 있습니다.";

frameDim.IconAndOptionsFrame.OptionsFrame._labelDimMoveValue.Title="값 이동: ";
frameDim.IconAndOptionsFrame.OptionsFrame._labelDimMoveValue.LongHelp="값 이동 조작기에서는 치수 값(해당 값만)을 쉽게
이동할 수 있습니다.";

frameDim.IconAndOptionsFrame.OptionsFrame._labelDimMoveDimLine.Title="치수선 이동: ";
frameDim.IconAndOptionsFrame.OptionsFrame._labelDimMoveDimLine.LongHelp="치수선 이동 조작기에서는 치수선(해당 선만)을 쉽게
이동할 수 있습니다.";

frameDim.IconAndOptionsFrame.OptionsFrame._labelDimMove2dPart.Title="치수선 보조 파트 이동: ";
frameDim.IconAndOptionsFrame.OptionsFrame._labelDimMove2dPart.LongHelp="치수선 보조 파트 이동 조작기에서는 치수선의 보조
파트(해당 선만)를 쉽게 이동할 수 있습니다.";


frameDim.IconAndOptionsFrame.OptionsFrame._labelDimMoveLeader.Title="치수 지시선 이동 ";
frameDim.IconAndOptionsFrame.OptionsFrame._labelDimMoveLeader.LongHelp="치수 지시선 이동 조작기에서는 지시선(해당 파트만)\을 쉽게
이동할 수 있습니다.";

frameAnnotManip.HeaderFrame.Global.Title="주석 조작기";
frameAnnotManip.HeaderFrame.Global.LongHelp="텍스트 주석을 선택 또는 편집할 때 표시할 기본 조작기를 정의합니다.";

frameAnnotManip.IconAndOptionsFrame.OptionsFrame._labelAnnotSelection.Title="선택사항";
frameAnnotManip.IconAndOptionsFrame.OptionsFrame._labelAnnotSelection.LongHelp="주석을 선택할 때 표시할 조작기를 정의합니다.";
frameAnnotManip.IconAndOptionsFrame.OptionsFrame._labelAnnotEdition.Title="텍스트 수정";
frameAnnotManip.IconAndOptionsFrame.OptionsFrame._labelAnnotEdition.LongHelp="주석을 편집할 때 표시할 조작기를 정의합니다.";

frameAnnotManip.IconAndOptionsFrame.OptionsFrame._labelManipTranslationAnnot.Title="텍스트 신축: ";
frameAnnotManip.IconAndOptionsFrame.OptionsFrame._labelManipTranslationAnnot.LongHelp="텍스트 신축 조작기로 주석의 텍스트를 늘릴 수 있습니다.";

frameAnnotManip.IconAndOptionsFrame.OptionsFrame._labelManipResizeAnnot.Title="텍스트 크기 조정: ";
frameAnnotManip.IconAndOptionsFrame.OptionsFrame._labelManipResizeAnnot.LongHelp="텍스트 크기 조정 조작기로 주석의 크기를 조정할 수 있습니다.";

frameAnnotManip.IconAndOptionsFrame.OptionsFrame._labelManipMoveValueAnnot.Title="텍스트 슬라이드: ";
frameAnnotManip.IconAndOptionsFrame.OptionsFrame._labelManipMoveValueAnnot.LongHelp="텍스트 슬라이드 조작기로 텍스트(지시선이 아닌 텍스트만)를 슬라이드할 수 있습니다.";

frameAnnotManip.IconAndOptionsFrame.OptionsFrame._labelManipTranslationLeader.Title="지시선 첨부 크기 조정: ";
frameAnnotManip.IconAndOptionsFrame.OptionsFrame._labelManipTranslationLeader.LongHelp="지시선 첨부 크기 조정 조작기로 지시선 첨부(첨부만)의 크기를 조정할 수 있습니다.";

frameAnnotManip.IconAndOptionsFrame.OptionsFrame._labelManipMoveLeaderAnchor.Title="지시선 앵커 위치 이동: ";
frameAnnotManip.IconAndOptionsFrame.OptionsFrame._labelManipMoveLeaderAnchor.LongHelp="지시선 앵커 위치 이동 조작기로 지시선의 앵커 위치(앵커 위치만)를 이동할 수 있습니다.";

frameAnnotManip.IconAndOptionsFrame.OptionsFrame._labelManipMoveLeader.Title="지시선 이동: ";
frameAnnotManip.IconAndOptionsFrame.OptionsFrame._labelManipMoveLeader.LongHelp="지시선 이동 조작기로 지시선 말단(말단만)을 이동할 수 있습니다.";

frameAnnotManip.IconAndOptionsFrame.OptionsFrame._labelManipMovableDatum.Title="이동 가능한 데이터 표시: ";
frameAnnotManip.IconAndOptionsFrame.OptionsFrame._labelManipMovableDatum.LongHelp="이동 가능한 데이터 표시 핸들로 데이터의 이동 가능 여부를 나타낼 수 있습니다(데이터 대상에만 사용 가능).";


