//NLS-ENGLISH VERSION
//=======================================================================
// MODULE : CATCuiEditor 
//=======================================================================

//-----------------------------------------------------------------------
// Window title
//-----------------------------------------------------------------------

CriticalErrorWindow.Title = "Critical Error in Catalog";

FatalErrorWindow.Title = "Fatal Error in Catalog";

WarningWindow.Title = "Warning in Catalog";

InformationWindow.Title = "Information for Catalog";

CATCatalogEditor.title = "Chapter";

//-----------------------------------------------------------------------
// Critical Error Messages
//-----------------------------------------------------------------------

GraphNodeCreationFailed.CriticalErrorMessage = "The creation of a graph node failed.";

NoFrmEditor.CriticalErrorMessage = "The catalog-associated editor could not be retrieved.";

NoCatalogTreeWindow.CriticalErrorMessage = "No catalog window could be created.";

NoContainerForGraphNode.CriticalErrorMessage = "The tree view of the catalog could not be created.";

EndChapterSUCreationFailed.CriticalErrorMessage = "The creation of a startup for a terminal chapter failed.";

ChapterSUCreationFailed.CriticalErrorMessage = "The creation of a startup for an intermediate chapter failed.";

ChapterInstFailed.CriticalErrorMessage = "The instantiation of a chapter graph node failed.";

PersChapterIsNull.CriticalErrorMessage = "No persistent chapter";

OnlyOneChapterAtATime.CriticalErrorMessage = "Only one chapter at a time can be viewed.";

//-----------------------------------------------------------------------
// Fatal Error Messages
//-----------------------------------------------------------------------

NoChildrenList.FatalErrorMessage = "The graph node cannot be expanded.";

NolinkWithPersChapter.FatalErrorMessage = "No link with the persistent chapter \nwas found on the graph node.";

NolinkWithPersDesc.FatalErrorMessage = "No component was found in the chapter.";

PersChapterIsNull.FatalErrorMessage = "The graph node points to a null chapter.";

NoCATICatalogChapter.FatalErrorMessage = "The object is not a chapter.";

DocumentCreationFailed.FatalErrorMessage = "The catalog document creation failed.";

DocumentInitFailed.FatalErrorMessage = "The catalog document initialization failed.";

NoRootContainer.FatalErrorMessage = "The root container could not be retrieved from the catalog document.";

NoCATICatalogChapterFactory.FatalErrorMessage ="No chapter could be created in this document.";

ContainerCreationFailed.FatalErrorMessage = "The root container could not be created in the catalog in the session document.";

GraphWindowCreationFailed.FatalErrorMessage = "The creation of the catalog window failed.";

DescPointsToNullObject.FatalErrorMessage = "A description points to a null object.";

DescParentChapterRetrievalFailed.FatalErrorMessage = "The parent chapter of a description could not be retrieved.";

PointedChapterRetrievalFailed.FatalErrorMessage = "The pointed object of a description is not a catalog chapter.";

NoObjectFilterAttribute.FatalErrorMessage = "The description-associated filter could not be retrieved.";

NullQueryResult.FatalErrorMessage = "The table view could not be created for a null query result.";

NoSelectedObject.FatalErrorMessage = "No selected object to display.";

//-----------------------------------------------------------------------
// Warning Messages
//-----------------------------------------------------------------------

NoChaptersInDoc.WarningMessage = "No chapter was found in the catalog document.";

NoDescriptionInChapter.WarningMessage = "No description was found in the chapter.";

RefPartInFamilyOpened_Synchro.WarningMessage = "The generic CATPart is opened into an editor. \n The editor must be closed before launching the synchronization.";

RefPartInFamilyOpened_Creation.WarningMessage = "The generic CATPart is opened into an editor. \n The editor must be closed before launching the creation of this family.";

NODTFound.WarningMessage = "No Design Table found";
//-----------------------------------------------------------------------
// Information Messages
//-----------------------------------------------------------------------


//-----------------------------------------------------------------------
// CATCatalogEditorFrame Messages
//-----------------------------------------------------------------------

// ChapterFrame section
ChapterFrame.Title         = "Chapter Identification";
ChapterNameLabel.Title     = "Chapter name: ";
ChapterTypeLabel.Title     = "Chapter type: ";
ChapterLocationLabel.Title = "Chapter location: ";


// KeywordsFrame section
KeywordsFrame.Title  = "Keyword Information";

// TableFrame section
TableFrame.Title = "Chapter Table View";

// DescriptionFrame section
DescriptionFrame.Title = "Details for the selected row";

NoChapterInQueryResult.CriticalMessage = "No chapter to display";

NoKeywordsInQueryResult.CriticalMessage = "No keyword in chapter";

NoDescriptionsInQueryResult.CriticalMessage = "No description in chapter";

