
// -----------------------------
// Analysis Entities : Property
// -----------------------------
SAM3DSolid.IconName = "I_Prop3D";
SAMShell.IconName = "I_Prop2D";
SAMDesignComposite.IconName = "I_ImportedComposite";
SAMBeam.IconName = "I_Prop1D";
SAMImportedBeamProp.IconName = "I_ImportedBeam";
SAMMappedPropertyEntity.IconName = "I_MappingProperty";
SAMMappedPropertyEntityOverload.IconName = "I_MappingProperty";


// -----------------------------
// Analysis Entities : Material
// -----------------------------

SAMIsoMaterial.IconName ="I_Sphere";
SAMThermalMaterial.IconName = "I_ThermalMaterial";
SAMNonLinearMaterial.IconName = "I_Sphere";
SAMUserIsoMaterial.IconName = "I_Sphere";
SAMUserMaterial.IconName ="I_Sphere";

// -----------------------------
// Analysis Entities : Restraint
// -----------------------------
SAMClamp.IconName = "I_Clamp";
SAMPivot.IconName = "I_Analysis_Pivot";
SAMSlidingPivot.IconName = "I_Analysis_Slider_Pivot";
SAMSlider.IconName = "I_Analysis_Slider";
SAMSurfaceSlider.IconName = "I_Analysis_Surface_Slider";
SAMPinned.IconName = "I_Rigid";
SAMBallJoin.IconName = "I_Analysis_Ball_Join";
SAMRestraint.IconName = "I_Analysis_Restraint";
SAMRestIsostatic.IconName = "I_IsostaticRestraint";

// -----------------------------
// Analysis Entities : Groups
// -----------------------------
SAMVertexGroup.IconName = "I_GroupVertexSp";
SAMEdgeGroup.IconName = "I_GroupEdgeSp";
SAMSurfaceGroup.IconName = "I_GroupSurfaceSp";
SAMBodyGroup.IconName = "I_GroupBodySp";
SAMBoxFEGroup.IconName = "I_GroupBoxSp";
SAMSphereFEGroup.IconName = "I_GroupSphereSp2";
SAMNodeGroup.IconName = "I_GroupNodeSp";
SAMVertexByProxi.IconName = "I_GroupVertexProxy";
SAMLineByProxi.IconName = "I_GroupEdgeProxy";
SAMSurfaceByProxi.IconName = "I_GroupSurfaceProxy";
SAMLineBoundaryGroup.IconName = "I_LineGroupByBoundary";
SAMSurfBoundaryGroup.IconName = "I_SurfGroupByBoundary";

SAMVertexGroupMP.IconName = "I_GroupVertexSp";
SAMEdgeGroupMP.IconName = "I_GroupEdgeSp";
SAMSurfaceGroupMP.IconName = "I_GroupSurfaceSp";
SAMVertexByProxiMP.IconName = "I_GroupVertexProxy";
SAMLineByProxiMP.IconName = "I_GroupEdgeProxy";
SAMSurfaceByProxiMP.IconName = "I_GroupSurfaceProxy";
SAMLineBoundaryGroupMP.IconName = "I_LineGroupByBoundary";
SAMSurfBoundaryGroupMP.IconName = "I_SurfGroupByBoundary";


// -------------------------
// Analysis Entities : Loads
// -------------------------

SAMPressure.IconName = "I_Pressure";
SAMDistributedForce.IconName = "I_DistributedForce";
SAMLineicForce.IconName = "I_LineicForce";
SAMSurfacicForce.IconName = "I_SurfacicForce";
SAMVolumicForce.IconName = "I_VolumicForce";
SAMTransAcceleration.IconName = "I_Gravity";
SAMRotationForce.IconName = "I_RotationForce";
SAMMoment.IconName = "I_Moment";
SAMEnforcedDisp.IconName = "I_Rigid";
SAMTemperatureField.IconName = "I_TemperatureField";
SAMTempFromThermSolutionEntity.IconName = "I_TemperatureFieldFromThermalSolution";
SAMBearingLoad.IconName = "I_Bearing";
SAMImportedForce.IconName = "I_ImportedForce";
SAMImportedMoment.IconName = "I_ImportedMoment";
SAMForceDensity.IconName = "I_ForceDensity";

// -----------------------------
// Sensor
// -----------------------------

SAMGlobalSensor.IconName = "I_AnalysisMeasure";
SAMImageSensor.IconName = "I_LocalSensor";
SAMLoadPreproSensor.IconName = "I_LoadSensor";
SAMInternalLoadPreproSensor.IconName = "I_InternalLoadSensor";
SAMInternalLoadPreproSensorSingleLoad.IconName = "I_InternalLoadSensor";
SAMMassPreproSensor.IconName = "I_InertiaSensor";
SAMReactionSensor.IconName = "I_ReactionSensor";
// old reaction sensor
AnalysisMeasureWithAxis.IconName = "I_AnalysisMeasure";

// ------------------------------
// Analysis Entities : Equipments
// ------------------------------
SAMDistributedMass.IconName = "I_Mass";
SAMLineicMass.IconName = "I_LineicMass";
SAMSurfacicMass.IconName = "I_SurfacicMass";
SAMInertiaMass.IconName = "I_InertiaMass";

// -------------------------------
// Analysis Entities : Connections
// -------------------------------
SAMFaceFaceSlider.IconName ="I_SliderConnection";
SAMFaceFaceContact.IconName ="I_ContactConnection";
SAMFaceFaceFastened.IconName ="I_FastenedConnection";
SAMFaceFaceFastenedSpring.IconName ="I_FastenedSpringConnection";
SAMDistantRigid.IconName ="I_RigidConnection";
SAMDistantSmooth.IconName ="I_SmoothConnection";
SAMGenericDistant.IconName = "I_GenericConnection";

SAMPressureFitting.IconName ="I_FittingConnection";
SAMBoltTightening.IconName ="I_TighteningConnection";
SAMVirtBoltTightening.IconName ="I_VirtualBoltTighConnection";
SAMVirtSpringBoltTigh.IconName ="I_SamVirtualSpringBoltTighConnect";
SAMSpotWeldingConnect.IconName ="I_SamSpotWeldingConnection";
SAMSeamWeldConnect.IconName ="I_SamSeamWeldConnection";
SAMSurfWeldConnect.IconName ="I_SamSurfWeldConnection";
SAMHalfPointPropConnect.IconName ="I_PointHalfConnProp";
SAMPointPointPropConnect.IconName ="I_PointPointConnProp";
SAMGenericWeld.IconName= "I_GenericConnection";

SAMPeriodicConnection.IconName ="I_PeriodicConnection";

// --------------------------------
// Analysis Entities : Virtual Part
// --------------------------------
SAMVirPartContact.IconName = "I_ContactVirtualPart";
SAMVirPartRigid.IconName = "I_RigidVirtualPart";
SAMVirPartSmooth.IconName = "I_SmoothVirtualPart";
SAMVirPartRigidSpring.IconName = "I_RigidSpringVirtualPart";
SAMVirPartSmoothSpring.IconName = "I_SmoothSpringVirtualPart";

// --------------------------------
// Analysis Entities : Groups
// --------------------------------
SAMSpecifiedGroup.IconName = "I_GroupEntity";
SAMFiltredGroup.IconName = "I_GroupEntity";



// --------------------------------
// Adaptivity
// --------------------------------
SAMAdaptivitySet.IconName = "I_AdaptivitySet";
SAMAdaptivityEntity.IconName = "I_AdaptivityGlobal";
SAMLocalAdaptEntity.IconName = "I_Adaptivity";

// --------------------------------------
// Analysis Entities : Connections Design
// --------------------------------------
SAMGenericConnDesign.IconName = "I_ConnDesignGene";
SAMPointConnDesign.IconName = "I_ConnDesignPoint";
SAMPointConnDesignWOP.IconName = "I_ConnDesignPointWOP";
SAMLineConnDesign.IconName = "I_ConnDesignLine";
SAMLineConnDesignWOP.IconName = "I_ConnDesignLineWOP";
SAMSurfConnDesign.IconName = "I_ConnDesignSurf";
SAMSurfConnDesignWOP.IconName = "I_ConnDesignSurfWOP";
SAMPointHalfConnDesign.IconName = "I_ConnDesignHalfPoint";
SAMPointPointConnDesign.IconName = "I_ConnDesignPointPoint";

// --------------------------------------
// Multi LoadSet
// --------------------------------------
AnalysisMultiLoadSet.IconName = "I_MultiLoadSet";
