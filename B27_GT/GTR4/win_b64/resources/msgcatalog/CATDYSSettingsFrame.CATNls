GridFrameTitle.labelGridFrame.Title = "Grid  ";
GridFrameTitle.labelGridFrame.LongHelp = "Grid\nSets the options allowing to use a grid.";
GridFrame.DisplayGrid.Title = "Display";
GridFrame.DisplayGrid.LongHelp = "Displays a useful grid that guides\nyou as you create your sketch.";
GridFrame.SnapToPt.Title = "Snap to point";
GridFrame.SnapToPt.LongHelp = "Snaps the points you create\nto the nearest intersection\npoints of the grid.";
GridFrame.AdaptativeGrid.Title = "Adaptative Grid";
GridFrame.AdaptativeGrid.LongHelp = "Scales the grid primary cell size according to zoom level.";
GridFrame.PrimGridSpacing.Title = "Primary spacing : ";
GridFrame.PrimGridSpacing.LongHelp = "Specifies the spacing\nbetween major grid lines.";
GridFrame.SecondStep.Title = "Graduations : ";
GridFrame.SecondStep.LongHelp = "Specifies the graduations\nbetween major grid lines.";
GridFrame.PrimGridSpacingH.Title = "H: ";
GridFrame.PrimGridSpacingH.LongHelp = "Specifies the spacing\nbetween major grid lines.";
GridFrame.SecondStepH.Title = "H: ";
GridFrame.SecondStepH.LongHelp = "Specifies the graduations\nbetween major grid lines.";
GridFrame.PrimGridSpacingV.Title = "V: ";
GridFrame.PrimGridSpacingV.LongHelp = "Specifies the spacing\nbetween major grid lines.";
GridFrame.SecondStepV.Title = "V: ";
GridFrame.SecondStepV.LongHelp = "Specifies the graduations\nbetween major grid lines.";
GridFrame.AllowDistortions.Title = "Allow Distortions";
GridFrame.AllowDistortions.LongHelp = "Allow distortions\nAllow different graduations and spacing on H and V.";

GeomFrameTitle.labelGeomFrame.Title = "Geometry  ";
GeomFrameTitle.labelGeomFrame.LongHelp = "Geometry\nProvides the capability of creating\nadditional elements in the geometry.";
GeomFrame.CreateCenter.Title = "Create circle and ellipse centers";
GeomFrame.CreateCenter.LongHelp = "Creates circle and ellipse centers\nwhen creating circles and ellipses.";
GeomFrame.DragElts.Title = "Allow direct manipulation";
GeomFrame.DragElts.LongHelp = "Allow direct manipulation\nDrag elements.";
// ressources R5 a retirer en R6
GeomFrame.DragWithEndPts.Title = "Drag elements end points included";
GeomFrame.DragWithEndPts.LongHelp = "Drag elements end points included.";
GeomFrame.SolvingButton.Title = "Solving Mode ...";
GeomFrame.SolvingButton.LongHelp = "Solving Mode\n.";

MiscFrameTitle.labelMiscFrame.Title = "Sketch Plane  ";
MiscFrameTitle.labelMiscFrame.LongHelp = "Sketch Plane\nSets the options allowing to facilitate\nthe use of the sketch plane.";
MiscFrame.Ground.Title = "Shade sketch plane";
MiscFrame.Ground.LongHelp = "Shades the sketch plane\nto view it when rotating\nit in the workbench.";
MiscFrame.NormalToView.Title = "Position sketch plane parallel to screen";
MiscFrame.NormalToView.LongHelp = "Sets the sketch plane parallel\nto the screen.";
MiscFrame.NormalToViewMinTransfo.Title = "Minimize viewpoint transformation";
MiscFrame.NormalToViewMinTransfo.LongHelp = "Minimize viewpoint transformation \nwhen entering workbench";
MiscFrame.ShowCursorPos.Title = "Visualization of the cursor coordinates";
MiscFrame.ShowCursorPos.LongHelp = "Show the SmartPick position in sketch plane\nclose to the cursor.";
MiscFrame.ShowContextBar.Title = "Visualization of the Context Bar";
MiscFrame.ShowContextBar.LongHelp = "Show the Context Bar in Sketch\naccording to selected elements.";

CstFrameTitle.labelCstFrame.Title = "Constraint ";
CstFrameTitle.labelCstFrame.LongHelp = "Constraint\nProvides the capability of creating\nconstraints.";
CstFrame.CstFrame01.CreateCstCplt.Title = "Creates the geometrical constraints";
CstFrame.CstFrame01.CreateCstCplt.LongHelp = "Creates the constraints detected\nusing Autodetection\nand internal constraints.";
CstFrame.CstFrame01.CreateCstDim.Title = "Creates the dimensional constraints";
CstFrame.CstFrame01.CreateCstDim.LongHelp = "Creates the dimensional constraints\nby data entry of values.";
CstFrame.CstFrame01.AutoCreateDimCst.Title = "Automatic Dimensional Constraints Creation";
CstFrame.CstFrame01.AutoCreateDimCst.LongHelp ="Creates the detected dimensional constraints automatically\nCreates the detected dimensional constraints automatically\nwhile you are sketching.";
CstFrame.CstFrame02.SmartPickButton.Title = "SmartPick ...";
CstFrame.CstFrame02.SmartPickButton.LongHelp = "SmartPick\nDynamically detects logical constraints.";
CstFrame.CstFrame01.BckgdSmartPick.Title = "Detects the geometrical constraints on background elements";
CstFrame.CstFrame01.BckgdSmartPick.LongHelp = "SmartPick dynamically detects the geometrical constraints on background elements.";

ClrFrameTitle.labelClrFrame.Title = "Colors  ";
ClrFrameTitle.labelClrFrame.LongHelp = "Colors\nDefine colors of elements.";
ClrFrame.DiagElts.Title = "Visualization of diagnosis ";
ClrFrame.DiagElts.LongHelp = "Visualization of diagnosis\non the constrained elements.";
ClrFrame.DefaultClrEltsLbl.Title = "Default color of the elements";
ClrFrame.DefaultClrEltsLbl.LongHelp = "Default color of the elements.";
ClrFrame.DiagColorsButton.Title = "Colors ...";
ClrFrame.DiagColorsButton.LongHelp = "Defines diagnosis colors.";
ClrFrame.OtherColorsLbl.Title = "Other color of the elements";
ClrFrame.OtherColorsLbl.LongHelp = "Other color of the elements.";
ClrFrame.OtherColorsButton.Title = "Colors ...";
ClrFrame.OtherColorsButton.LongHelp = "Defines other colors of elements.";

// Diagnotic colors Windows
CATDYSDiagColorWindow.Title = "Diagnosis colors";
BCancel = "Close";
DiagColorFrameTitle.labelDiagColorFrame.Title = "Diagnosis colors  ";
DiagColorFrameTitle.labelDiagColorFrame.LongHelp = "Defines diagnosis colors.";
DiagColorFrame.OverDefinedColorEltsLbl.Title = "Over-constrained elements ";
DiagColorFrame.OverDefinedColorEltsLbl.LongHelp = "Over-constrained elements\nDefines the color used for the elements in over-constrained models.";
DiagColorFrame.WellDefinedColorEltsLbl.Title = "Iso-constrained elements ";
DiagColorFrame.WellDefinedColorEltsLbl.LongHelp = "Iso-constrained elements\nDefines the color used for the elements in iso-constrained models.";
DiagColorFrame.NotConsistentColorEltsLbl.Title = "Inconsistent elements ";
DiagColorFrame.NotConsistentColorEltsLbl.LongHelp = "Inconsistent elements\nDefines the color used for the elements in inconsistent models.";
DiagColorFrame.NotChangedColorEltsLbl.Title = "Not-changed elements ";
DiagColorFrame.NotChangedColorEltsLbl.LongHelp = "Not-changed elements\nDefines the color used for the not-changed elements.";

// Other colors Windows
CATDYSOtherColorsWindow.Title = "Colors of other elements";
OtherColorFrameTitle.labelOtherColorFrame.Title = "Colors  ";
OtherColorFrameTitle.labelOtherColorFrame.LongHelp = "Defines other colors of elements.";
OtherColorFrame.IsolatedColorEltsLbl.Title = "Isolated Elements ";
OtherColorFrame.IsolatedColorEltsLbl.LongHelp = "Isolated Elements\nDefines the color used for the isolated elements.";
OtherColorFrame.ProtectedColorEltsLbl.Title = "Protected elements ";
OtherColorFrame.ProtectedColorEltsLbl.LongHelp = "Protected elements\nDefines the color used for the protected elements.";
OtherColorFrame.ConstructColorEltsLbl.Title = "Construction elements ";
OtherColorFrame.ConstructColorEltsLbl.LongHelp = "Construction elements\nDefines the color used for the Construction elements.";
OtherColorFrame.SmartPickColorLbl.Title = "SmartPick ";
OtherColorFrame.SmartPickColorLbl.LongHelp = "Not-changed elements\nDefines the color used for the SmartPick elements.";

// SmartPick Windows
CATDYSSmartPickWindow.Title = "SmartPick";
CpltFrameTitle.labelCpltFrame.Title = "SmartPick  ";
CpltFrameTitle.labelCpltFrame.LongHelp = "Dynamically detects logical constraints\nand highlights them with smart symbols as\nyou point the cursor around the screen.";
CpltFrame.PosHorVertTo.Title = "Alignment";
CpltFrame.PosHorVertTo.LongHelp = "Dynamically detects the different elements\nalong which the sketch is aligned.";
CpltFrame.PosSupport.Title = "Support lines and circles";
CpltFrame.PosSupport.LongHelp = "Dynamically detects support lines and circles.";
CpltFrame.DirHorVert.Title = "Horizontal and vertical";
CpltFrame.DirHorVert.LongHelp = "Dynamically detects horizontal\nor vertical lines.";
CpltFrame.DirParallel.Title = "Parallelism";
CpltFrame.DirParallel.LongHelp = "Dynamically detects the different\nelements to which the sketch is\nparallel.";
CpltFrame.DirPerpend.Title = "Perpendicularity";
CpltFrame.DirPerpend.LongHelp = "Dynamically detects the different\nelements to which the sketch is\nnormal.";
CpltFrame.DirTangent.Title = "Tangency";
CpltFrame.DirTangent.LongHelp = "Dynamically detects the different\nelements to which the sketch is\ntangent.";

// ressources R6 � retirer en R5
// Solving Windows
CATDYSSolvingWindow.Title = "Dragging of elements";
SolvFrameTitle.labelSolvFrame.Title = "Dragging of elements  ";
SolvFrameTitle.labelSolvFrame.LongHelp = "Lets you define the solving mode for moving elements.";
SolvFrame.DragWithEndPts.Title = "Drag elements end points included             ";
SolvFrame.DragWithEndPts.LongHelp = "Drag elements end points included.";
SolvFrame.SolveLbl.Title =       "Solving mode for moving elements            ";
SolvFrame.SolvFrame01.StandardRadio.Title = "Standard mode";
SolvFrame.SolvFrame01.StandardRadio.LongHelp = "Solving standard mode.";
SolvFrame.SolvFrame01.MinMoveRadio.Title = "Minimum move";
SolvFrame.SolvFrame01.MinMoveRadio.LongHelp = "Minimum move.";
SolvFrame.SolvFrame01.RelaxationRadio.Title = "Relaxation";
SolvFrame.SolvFrame01.RelaxationRadio.LongHelp = "Relaxation.";

// Notification
DYS.Settings.Notify.Msg.NegativeGridSpacing = "Enter a value that is greater than zero.";
DYS.Settings.Notify.Msg.NegativeGridSecStep = "Enter a value that is greater than zero.";
DYS.Settings.Notify.Msg.BadColor = "Choose a correct color.";

//Update
UpdateFrameTitle.labelUpdateFrame.Title = "Update  ";
UpdateFrameTitle.labelUpdateFrame.LongHelp = "Update\nDefines the update's sketch behavior.";
UpdateFrame.UnderDefUpdateErr.Title = "Generate update errors when the sketch is under-constrained";
UpdateFrame.UnderDefUpdateErr.LongHelp = "An update error will be raised if the sketch is not fully-constrained.";

CstFrame.CstFrame01.AutoEditDimCst.Title = "Automatic Dimensional Constraints Edition";
CstFrame.CstFrame01.AutoEditDimCst.LongHelp ="Edits the dimensional constraints automatically\nEdits the constraint automatically\nwhile you are creating a new dimensional constraint.";

