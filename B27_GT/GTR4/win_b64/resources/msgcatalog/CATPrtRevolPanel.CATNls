ShaftTitle  = "Shaft Definition";
GrooveTitle = "Groove Definition";

LongHelp = "Defines a shaft or a groove.";
SketchAxis = "Sketch Axis";

FrmRoot.FrmMain.LimitFrame.Title = "Limits";
FrmRoot.FrmMain.LimitFrame.LabEndAngle.Title   = "First angle: ";
FrmRoot.FrmMain.LimitFrame.EndAngle.Spinner.Title   = "Angle1";
FrmRoot.FrmMain.LimitFrame.EndAngle.EnglobingFrame.IntermediateFrame.Spinner.Title   = "Angle1";
FrmRoot.FrmMain.LimitFrame.LabStartAngle.Title = "Second angle: ";
FrmRoot.FrmMain.LimitFrame.StartAngle.Spinner.Title = "Angle2";
FrmRoot.FrmMain.LimitFrame.StartAngle.EnglobingFrame.IntermediateFrame.Spinner.Title = "Angle2";

FrmRoot.FrmMain.FraEnd.Title = "First Limit";
CmbEndLab0 = "First Angle";
CmbEndLab1 = "Up to next";
CmbEndLab2 = "Up to last";
CmbEndLab3 = "Up to plane";
CmbEndLab4 = "Up to surface";
FrmRoot.FrmMain.FraEnd.FraEndOffsetFrame.LabEndOffset.Title = "Offset: ";

FrmRoot.FrmMain.FraStart.Title = "Second Limit";
CmbStartLab0 = "Second Angle";
CmbStartLab1 = "Up to next";
CmbStartLab2 = "Up to last";
CmbStartLab3 = "Up to plane";
CmbStartLab4 = "Up to surface";
FrmRoot.FrmMain.FraStart.FraStartOffsetFrame.LabStartOffset.Title = "Offset: ";

PlanarProfile="Profile"; 
NonPlanarProfile="Profile/Surface"; 

FrmRoot.FrmMain.ProfileFrame.Profile.Lab.Title="Selection: ";

FrmRoot.FrmMain.ProfileFrame.Profile.FraEdt.Lab.Title="Selection: ";

FrmRoot.FrmMain.ProfileFrame.Profile.CtxProf.ItmProf.Title="Go to profile definition";
FrmRoot.FrmMain.ProfileFrame.PshRevSide.Title = " Reverse Side       ";
FrmRoot.FrmMain.AxisFrame.Title="Axis";
FrmRoot.FrmMain.AxisFrame.Axis.FraEdt.Lab.Title="Selection: ";
FrmRoot.FrmMain.AxisFrame.PshRev.Title = " Reverse Direction ";


FrmRoot.FrmMain.LimitFrame.EndAngle.LongHelp=
"Specifies the value
of the first angle.";
FrmRoot.FrmMain.LimitFrame.LabEndAngle.LongHelp=
"Specifies the value
of the first angle.";

FrmRoot.FrmMain.LimitFrame.StartAngle.LongHelp=
"Specifies the value
of the second angle.";
FrmRoot.FrmMain.LimitFrame.LabStartAngle.LongHelp=
"Specifies the value
of the second angle.";

FrmRoot.FrmMain.ProfileFrame.Profile.LongHelp=
"Specifies the profile.";
FrmRoot.FrmMain.ProfileFrame.PshProf.LongHelp=
"Edits the profile.";
FrmRoot.FrmMain.ProfileFrame.PshRevSide.LongHelp=
"Reverses the material side shown by the arrow.";

FrmRoot.FrmMain.AxisFrame.Axis.LongHelp=
"Specifies the revolution axis.";

FrmRoot.FrmMain.AxisFrame.PshRev.LongHelp = 
"Reverses the direction of revolution pointed by the arrow.";

FrmRoot.FrmMain.ProfileFrame.ChkThin.Title="Thick Profile";

FrmMore.FrmThin.ChkMerge.Title="Merge Ends";

FrmMore.FrmThin.ChkFiber.Title="Neutral Fiber";

FrmMore.FrmThin.LabThin1.Title="Thickness1:";

FrmMore.FrmThin.LabThin2.Title="Thickness2:";


FrmThinShaftTitle="Thin Shaft";
FrmThinGrooveTitle="Thin Groove";

FrmRoot.FrmMain.ProfileFrame.ChkThin.LongHelp = "Creates a thin feature: the profile is thickened in its support plane";

FrmMore.FrmThin.ChkFiber.LongHelp="Creates a thin feature using a neutral fiber";
FrmMore.FrmThin.ChkMerge.LongHelp="Merges ends of thin feature with around material";


FrmMore.FrmThin.LabThin1.LongHelp="First thickness of thin feature";
FrmMore.FrmThin.FraThin1.Spinner.Title="Thickness1";
FrmMore.FrmThin.FraThin1.EnglobingFrame.IntermediateFrame.Spinner.Title="Thickness1";
FrmMore.FrmThin.FraThin1.Spinner.LongHelp="First thickness of thin feature";

FrmMore.FrmThin.LabThin2.LongHelp="Second thickness of thin feature";
FrmMore.FrmThin.FraThin2.Spinner.LongHelp="Second thickness of thin feature";
FrmMore.FrmThin.FraThin2.Spinner.Title="Thickness2";
FrmMore.FrmThin.FraThin2.EnglobingFrame.IntermediateFrame.Spinner.Title="Thickness2";

FrmRoot.FrmMain.FraEnd.LongHelp = 
"First Limit 
Specifies the limit defined 
in the direction of extrusion.";

FrmRoot.FrmMain.FraEnd.CmbEnd.LongHelp = 
"Type of relimitation 
for the first limit.";

FrmRoot.FrmMain.FraEnd.FraStartLimInp.FraStartLim.LongHelp = 
"Identifies the limiting element 
for the first limit";

FrmRoot.FrmMain.FraEnd.FraEndOffsetFrame.LabEndOffset.LongHelp = 
"Specifies the offset value 
for the first limit.";

FrmRoot.FrmMain.FraEnd.FraEndOffsetFrame.FraEndOffFrmSurf.LongHelp = 
"Specifies the offset value 
for the first limit.";

FrmRoot.FrmMain.FraStart.LongHelp = 
"Second Limit
Specifies the limit opposite 
to the direction of extrusion.";

FrmRoot.FrmMain.FraStart.CmbStart.LongHelp = 
"Type of relimitation 
for the second limit.";

FrmRoot.FrmMain.FraStart.FraStartLimInp.FraStartLim.LongHelp = 
"Identifies the limiting element 
for the second limit";

FrmRoot.FrmMain.FraStart.FraStartOffsetFrame.LabStartOffset.LongHelp = 
"Specifies the offset value 
for the second limit.";

FrmRoot.FrmMain.FraEnd.FraStartOffsetFrame.FraStartOffFrmSurf.LongHelp = 
"Specifies the offset value 
for the second limit.";

