// COPYRIGHT DASSAULT SYSTEMES 1998
//===========================================================================
//
// CATRdgDefaultCmdHeader
//
//===========================================================================

//--------------------------------------
// NewCamera
//--------------------------------------
CATRdgDefaultCmdHeader.NewCamera.Title = "New Camera";
CATRdgDefaultCmdHeader.NewCamera.UndoTitle = "(Create Camera)";
CATRdgDefaultCmdHeader.NewCamera.Help = "Creates a camera";
CATRdgDefaultCmdHeader.NewCamera.ShortHelp = "Create Camera";
CATRdgDefaultCmdHeader.NewCamera.LongHelp =
"Creates a new camera with default settings.";

//--------------------------------------
// NewBoxEnvironment
//--------------------------------------
CATRdgDefaultCmdHeader.NewBoxEnvironment.Title = "New Box Environment";
CATRdgDefaultCmdHeader.NewBoxEnvironment.UndoTitle = "(Create Box Environment)";
CATRdgDefaultCmdHeader.NewBoxEnvironment.Help = "Creates a box environment";
CATRdgDefaultCmdHeader.NewBoxEnvironment.ShortHelp = "Create Box Environment";
CATRdgDefaultCmdHeader.NewBoxEnvironment.LongHelp =
"Creates a new box environment with default settings.";

//--------------------------------------
// NewSphereEnvironment
//--------------------------------------
CATRdgDefaultCmdHeader.NewSphereEnvironment.Title = "New Sphere Environment";
CATRdgDefaultCmdHeader.NewSphereEnvironment.UndoTitle = "(Create Spherical Environment)";
CATRdgDefaultCmdHeader.NewSphereEnvironment.Help = "Creates a spherical environment";
CATRdgDefaultCmdHeader.NewSphereEnvironment.ShortHelp = "Create Spherical Environment";
CATRdgDefaultCmdHeader.NewSphereEnvironment.LongHelp =
"Creates a new spherical environment with default settings.";

//--------------------------------------
// NewCylinderEnvironment
//--------------------------------------
CATRdgDefaultCmdHeader.NewCylinderEnvironment.Title = "New Cylinder Environment";
CATRdgDefaultCmdHeader.NewCylinderEnvironment.UndoTitle = "(Create Cylindrical Environment)";
CATRdgDefaultCmdHeader.NewCylinderEnvironment.Help = "Creates a cylindrical environment";
CATRdgDefaultCmdHeader.NewCylinderEnvironment.ShortHelp = "Create Cylindrical Environment";
CATRdgDefaultCmdHeader.NewCylinderEnvironment.LongHelp =
"Creates a new cylindrical environment with default settings.";

//--------------------------------------
// ImportEnvironment
//--------------------------------------
CATRdgDefaultCmdHeader.ImportEnvironment.Title = "Import Environment";
CATRdgDefaultCmdHeader.ImportEnvironment.UndoTitle = "(Import Environment)";
CATRdgDefaultCmdHeader.ImportEnvironment.Help = "Imports an environment";
CATRdgDefaultCmdHeader.ImportEnvironment.ShortHelp = "Import Environment";
CATRdgDefaultCmdHeader.ImportEnvironment.LongHelp =
"Imports an environment.";
camLabel = "Realviz Stitcher� File (*.cam)";

//--------------------------------------
// NewSpotLight
//--------------------------------------
CATRdgDefaultCmdHeader.NewSpotLight.Title = "New Spot Light";
CATRdgDefaultCmdHeader.NewSpotLight.UndoTitle = "(Create Spot Light)";
CATRdgDefaultCmdHeader.NewSpotLight.Help = "Creates a spot light source";
CATRdgDefaultCmdHeader.NewSpotLight.ShortHelp = "Create Spot Light";
CATRdgDefaultCmdHeader.NewSpotLight.LongHelp =
"Creates a new spot light source with default settings.";

//--------------------------------------
// NewPointLight
//--------------------------------------
CATRdgDefaultCmdHeader.NewPointLight.Title = "New Point Light";
CATRdgDefaultCmdHeader.NewPointLight.UndoTitle = "(Create Point Light)";
CATRdgDefaultCmdHeader.NewPointLight.Help = "Creates a point light source";
CATRdgDefaultCmdHeader.NewPointLight.ShortHelp = "Create Point Light";
CATRdgDefaultCmdHeader.NewPointLight.LongHelp =
"Creates a new point light source with default settings.";

//--------------------------------------
// NewDirectionalLight
//--------------------------------------
CATRdgDefaultCmdHeader.NewDirectionalLight.Title = "New Directional Light";
CATRdgDefaultCmdHeader.NewDirectionalLight.UndoTitle = "(Create Directional Light)";
CATRdgDefaultCmdHeader.NewDirectionalLight.Help = "Creates a directional light source";
CATRdgDefaultCmdHeader.NewDirectionalLight.ShortHelp = "Create Directional Light";
CATRdgDefaultCmdHeader.NewDirectionalLight.LongHelp =
"Creates a new directional light source with default settings.";

//--------------------------------------
// NewRectangleLight
//--------------------------------------
CATRdgDefaultCmdHeader.NewRectangleLight.Title = "New Rectangle Area Light";
CATRdgDefaultCmdHeader.NewRectangleLight.UndoTitle = "(Create Rectangle Area Light)";
CATRdgDefaultCmdHeader.NewRectangleLight.Help = "Creates a rectangle area light source";
CATRdgDefaultCmdHeader.NewRectangleLight.ShortHelp = "Create Rectangle Area Light";
CATRdgDefaultCmdHeader.NewRectangleLight.LongHelp =
"Creates a new rectangle area light source with default settings.";

//--------------------------------------
// NewDiskLight
//--------------------------------------
CATRdgDefaultCmdHeader.NewDiskLight.Title = "New Disk Area Light";
CATRdgDefaultCmdHeader.NewDiskLight.UndoTitle = "(Create Disk Area Light)";
CATRdgDefaultCmdHeader.NewDiskLight.Help = "Creates a disk area light source";
CATRdgDefaultCmdHeader.NewDiskLight.ShortHelp = "Create Disk Area Light";
CATRdgDefaultCmdHeader.NewDiskLight.LongHelp =
"Creates a new disk area light source with default settings.";

//--------------------------------------
// NewSphereLight
//--------------------------------------
CATRdgDefaultCmdHeader.NewSphereLight.Title = "New Sphere Area Light";
CATRdgDefaultCmdHeader.NewSphereLight.UndoTitle = "(Create Sphere Area Light)";
CATRdgDefaultCmdHeader.NewSphereLight.Help = "Creates a sphere area light source";
CATRdgDefaultCmdHeader.NewSphereLight.ShortHelp = "Create Sphere Area Light";
CATRdgDefaultCmdHeader.NewSphereLight.LongHelp =
"Creates a new sphere area light source with default settings.";

//--------------------------------------
// NewCylinderLight
//--------------------------------------
CATRdgDefaultCmdHeader.NewCylinderLight.Title = "New Cylinder Area Light";
CATRdgDefaultCmdHeader.NewCylinderLight.UndoTitle = "(Create Cylinder Area Light)";
CATRdgDefaultCmdHeader.NewCylinderLight.Help = "Creates a cylinder area light source";
CATRdgDefaultCmdHeader.NewCylinderLight.ShortHelp = "Create Cylinder Area Light";
CATRdgDefaultCmdHeader.NewCylinderLight.LongHelp =
"Creates a new cylinder area light source with default settings.";

//--------------------------------------
// NewReview
//--------------------------------------
CATRdgDefaultCmdHeader.NewReview.Title = "Review";
CATRdgDefaultCmdHeader.NewReview.UndoTitle = "(Create Review)";
CATRdgDefaultCmdHeader.NewReview.Help = "Creates a review";
CATRdgDefaultCmdHeader.NewReview.ShortHelp = "Create Review";
CATRdgDefaultCmdHeader.NewReview.LongHelp =
"Creates a review.";

//--------------------------------------
// EditTrack
//--------------------------------------
CATRdgDefaultCmdHeader.EditTrack.Title = "Track";
CATRdgDefaultCmdHeader.EditTrack.UndoTitle = "(Edit Track)";
CATRdgDefaultCmdHeader.EditTrack.Help = "Edits a track";
CATRdgDefaultCmdHeader.EditTrack.ShortHelp = "Edit Track";
CATRdgDefaultCmdHeader.EditTrack.LongHelp =
"Edits a track.";

//--------------------------------------
// EditColorAction
//--------------------------------------
CATRdgDefaultCmdHeader.EditColorAction.Title = "Color Action";
CATRdgDefaultCmdHeader.EditColorAction.UndoTitle = "(Create Color Action)";
CATRdgDefaultCmdHeader.EditColorAction.Help = "Creates a color action";
CATRdgDefaultCmdHeader.EditColorAction.ShortHelp = "Color Action";
CATRdgDefaultCmdHeader.EditColorAction.LongHelp =
"Creates a color action.";

//--------------------------------------
// EditVisibilityAction
//--------------------------------------
CATRdgDefaultCmdHeader.EditVisibilityAction.Title = "Visibility Action";
CATRdgDefaultCmdHeader.EditVisibilityAction.UndoTitle = "(Create Visibility Action)";
CATRdgDefaultCmdHeader.EditVisibilityAction.Help = "Creates a visibility action";
CATRdgDefaultCmdHeader.EditVisibilityAction.ShortHelp = "Visibility Action";
CATRdgDefaultCmdHeader.EditVisibilityAction.LongHelp =
"Creates a visibility action.";

//--------------------------------------
// EditTurntable
//--------------------------------------
CATRdgDefaultCmdHeader.EditTurntable.Title = "Edit Turntable";
CATRdgDefaultCmdHeader.EditTurntable.UndoTitle = "(Edit Turntable)";
CATRdgDefaultCmdHeader.EditTurntable.Help = "Edits a turntable simulation";
CATRdgDefaultCmdHeader.EditTurntable.ShortHelp = "Edit Turntable";
CATRdgDefaultCmdHeader.EditTurntable.LongHelp =
"Edits a turntable simulation.";

//--------------------------------------
// NewTurntable
//--------------------------------------
CATRdgDefaultCmdHeader.NewTurntable.Title = "New Turntable";
CATRdgDefaultCmdHeader.NewTurntable.UndoTitle = "(Create Turntable)";
CATRdgDefaultCmdHeader.NewTurntable.Help = "Creates a turntable simulation";
CATRdgDefaultCmdHeader.NewTurntable.ShortHelp = "Create Turntable";
CATRdgDefaultCmdHeader.NewTurntable.LongHelp =
"Creates a turntable simulation";

//--------------------------------------
// SimulationExperiment
//--------------------------------------
CATRdgDefaultCmdHeader.SimulationExperiment.Title = "Simulation";
CATRdgDefaultCmdHeader.SimulationExperiment.Help = "Edits shuttle and kinematic simulations";
CATRdgDefaultCmdHeader.SimulationExperiment.ShortHelp = "Simulation";
CATRdgDefaultCmdHeader.SimulationExperiment.LongHelp = "Edits shuttle and kinematic simulations";

//--------------------------------------
// EditSequence
//--------------------------------------
CATRscDefaultCmdHeader.EditSequence.Title = "Edit Sequence";
CATRdgDefaultCmdHeader.EditSequence.UndoTitle = "(Edit a sequence)";
CATRdgDefaultCmdHeader.EditSequence.Help = "Edits a sequence";
CATRdgDefaultCmdHeader.EditSequence.ShortHelp = "Edit Sequence";
CATRdgDefaultCmdHeader.EditSequence.LongHelp =
"Edits a simulation sequence.";

//--------------------------------------
// CATNauVideoCompilatorHdr
//--------------------------------------
CATRdgDefaultCmdHeader.CATNauVideoCompilatorHdr.Title="Generate Video";
CATRdgDefaultCmdHeader.CATNauVideoCompilatorHdr.Help="Generates a video";
CATRdgDefaultCmdHeader.CATNauVideoCompilatorHdr.ShortHelp="Generate Video";
CATRdgDefaultCmdHeader.CATNauVideoCompilatorHdr.LongHelp="Generates a video from a simulation";

//--------------------------------------
// Player
//--------------------------------------
CATRdgDefaultCmdHeader.Player.Title="Player";
CATRdgDefaultCmdHeader.Player.Help="Plays a Simulation";
CATRdgDefaultCmdHeader.Player.ShortHelp="Simulation Player";
CATRdgDefaultCmdHeader.Player.LongHelp="Plays a simulation";

//--------------------------------------
// ApplyMaterial
//--------------------------------------
CATRdgDefaultCmdHeader.ApplyMaterial.Title = "Apply Material";
CATRdgDefaultCmdHeader.ApplyMaterial.UndoTitle = "(Apply Material)";
CATRdgDefaultCmdHeader.ApplyMaterial.Help = "Applies a material on geometry";
CATRdgDefaultCmdHeader.ApplyMaterial.ShortHelp = "Apply Material";
CATRdgDefaultCmdHeader.ApplyMaterial.LongHelp =
"Applies material on geometry.";

//--------------------------------------
// RenderingCatalog
//--------------------------------------
CATRdgDefaultCmdHeader.RenderingCatalog.Title = "Rendering Catalog";
CATRdgDefaultCmdHeader.RenderingCatalog.UndoTitle = "(Catalog Browser)";
CATRdgDefaultCmdHeader.RenderingCatalog.Help = "Imports components from catalog";
CATRdgDefaultCmdHeader.RenderingCatalog.ShortHelp = "Catalog Browser";
CATRdgDefaultCmdHeader.RenderingCatalog.LongHelp =
"Imports components from catalog.";

//--------------------------------------
// CopyParametersSubMenu
//--------------------------------------
CATRdgDefaultCmdHeader.CopyParametersSubMenu.Title = "Copy Rendering Data...";
CATRdgDefaultCmdHeader.CopyParametersSubMenu.Help = "Copy rendering parameters from another material";
CATRdgDefaultCmdHeader.CopyParametersSubMenu.ShortHelp = "Copy Rendering Parameters";
CATRdgDefaultCmdHeader.CopyParametersSubMenu.LongHelp =
"Copy rendering parameters from another existing material.";

//--------------------------------------
// ReplaceMaterialSubMenu
//--------------------------------------
CATRdgDefaultCmdHeader.ReplaceMaterialSubMenu.Title = "Replace Material Link...";
CATRdgDefaultCmdHeader.ReplaceMaterialSubMenu.Help = "Replace material by another material";
CATRdgDefaultCmdHeader.ReplaceMaterialSubMenu.ShortHelp = "Replace Material";
CATRdgDefaultCmdHeader.ReplaceMaterialSubMenu.LongHelp =
"Modify the material link to replace the applied material by another one.";

//--------------------------------------
// EditParametersSubMenu
//--------------------------------------
CATRdgDefaultCmdHeader.EditParametersSubMenu.Title = "Edit Material";
CATRdgDefaultCmdHeader.EditParametersSubMenu.Help = "Edit material parameters";
CATRdgDefaultCmdHeader.EditParametersSubMenu.ShortHelp = "Edit material parameters";
CATRdgDefaultCmdHeader.EditParametersSubMenu.LongHelp =
"Edit material parameters.";

//--------------------------------------
// CATNavigation4DCameraRep
//--------------------------------------
CATRdgDefaultCmdHeader.CATNavigation4DCameraRep.Title="Hide/Show Representation";
CATRdgDefaultCmdHeader.CATNavigation4DCameraRep.Help="Hide/Show graphic representation";
CATRdgDefaultCmdHeader.CATNavigation4DCameraRep.ShortHelp="Hide/Show";
CATRdgDefaultCmdHeader.CATNavigation4DCameraRep.LongHelp="Hide/Show\nHides or Shows the graphic representation of the camera\n";

//--------------------------------------
// CATNavigation4DUpdateCameraFromView
//--------------------------------------
CATRdgDefaultCmdHeader.CATNavigation4DUpdateCameraFromView.Title = "Update From View";
CATRdgDefaultCmdHeader.CATNavigation4DUpdateCameraFromView.Help = "Updates camera from viewpoint";
CATRdgDefaultCmdHeader.CATNavigation4DUpdateCameraFromView.ShortHelp = "Update From View";
CATRdgDefaultCmdHeader.CATNavigation4DUpdateCameraFromView.LongHelp ="Changes the camera to match the current viewpoint.";

//--------------------------------------
// NewShooting
//--------------------------------------
CATRdgDefaultCmdHeader.NewShooting.UndoTitle = "(Create Shooting)";
CATRdgDefaultCmdHeader.NewShooting.Help = "Creates a new shooting";
CATRdgDefaultCmdHeader.NewShooting.ShortHelp = "Create Shooting";
CATRdgDefaultCmdHeader.NewShooting.LongHelp =
"Creates a new shooting";

//--------------------------------------
// Render
//--------------------------------------
CATRdgDefaultCmdHeader.Render.UndoTitle = "(Render Shooting)";
CATRdgDefaultCmdHeader.Render.Help = "Renders a shooting";
CATRdgDefaultCmdHeader.Render.ShortHelp = "Render Shooting";
CATRdgDefaultCmdHeader.Render.LongHelp =
"Renders using shooting parameters.";

//--------------------------------------
// QuickRender
//--------------------------------------
CATRdgDefaultCmdHeader.QuickRender.UndoTitle = "(Quick Render)";
CATRdgDefaultCmdHeader.QuickRender.Help = "Renders the scene";
CATRdgDefaultCmdHeader.QuickRender.ShortHelp = "Quick Render";
CATRdgDefaultCmdHeader.QuickRender.LongHelp =
"Renders the scene using settings defined in Photo Studio options";

//--------------------------------------
// RedoRender
//--------------------------------------
CATRdgDefaultCmdHeader.RedoRender.UndoTitle = "(Redo Render)";
CATRdgDefaultCmdHeader.RedoRender.Help = "Renders last used shooting";
CATRdgDefaultCmdHeader.RedoRender.ShortHelp = "Redo Render";
CATRdgDefaultCmdHeader.RedoRender.LongHelp =
"Renders using the last used shooting parameters";

//--------------------------------------
// Camera Focale 
//--------------------------------------
CATRdgDefaultCmdHeader.CameraFocalCmd.Title = "Manipulate Focal";
CATRdgDefaultCmdHeader.CameraFocalCmd.Help = "Manipulate Focal";
CATRdgDefaultCmdHeader.CameraFocalCmd.ShortHelp = "Manipulate Focal";
CATRdgDefaultCmdHeader.CameraFocalCmd.LongHelp ="Manipulate the camera focal length.";


