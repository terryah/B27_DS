//=============================================================================================
// COPYRIGHT DASSAULT SYSTEMES PROVENCE 2007
//==============================================================================================
//
// CATCldDisplayOptionPageDlg : Resource file for NLS purpose related to Cloud Display Options
//
//==============================================================================================
//
// Implementation Notes:
//
//==============================================================================================
// Jan 2007    Creation        TTD
//==============================================================================================

MainPageTitle="Display Modes";

// POINT

PointFrame.HeaderFrame.Global.Title="Point";
PointFrame.HeaderFrame.Global.LongHelp="Defines the display options for points.";

PointFrame.IconAndOptionsFrame.OptionsFrame.PointSub.Sub1.SamplingLbl.Title="Sampling :";
PointFrame.IconAndOptionsFrame.OptionsFrame.PointSub.Sub1.SamplingLbl.LongHelp="Defines the percentage of points making the cloud to be displayed.";
PointFrame.IconAndOptionsFrame.OptionsFrame.PointSub.Sub2.CheckProtected.Title="Protected";
PointFrame.IconAndOptionsFrame.OptionsFrame.PointSub.Sub2.CheckProtected.LongHelp="Highlights the protected points.";
PointFrame.IconAndOptionsFrame.OptionsFrame.PointSub.Sub2.CheckColored.Title="Colored";
PointFrame.IconAndOptionsFrame.OptionsFrame.PointSub.Sub2.CheckColored.LongHelp="Available when color information has been imported.
When selected, displays the points with that color information.
When cleared, displays the points in the color defined in the
Graphic Properties.";
PointFrame.IconAndOptionsFrame.OptionsFrame.PointSub.Sub2.CheckSymbol.Title="Symbol";
PointFrame.IconAndOptionsFrame.OptionsFrame.PointSub.Sub2.CheckSymbol.LongHelp="Highlights the symbols if they exist in the imported file.";

// SCAN

ScanFrame.HeaderFrame.Global.Title="Scan or Grid";
ScanFrame.HeaderFrame.Global.LongHelp="Defines the display options for scans or grids.";

ScanFrame.IconAndOptionsFrame.OptionsFrame.ScanSub.ScanPolyline.Title="Polyline";
ScanFrame.IconAndOptionsFrame.OptionsFrame.ScanSub.ScanPolyline.LongHelp="Displays the scan as a polyline.";
ScanFrame.IconAndOptionsFrame.OptionsFrame.ScanSub.ScanPoint.Title="Point";
ScanFrame.IconAndOptionsFrame.OptionsFrame.ScanSub.ScanPoint.LongHelp="Displays the points contained in the scan.";
ScanFrame.IconAndOptionsFrame.OptionsFrame.ScanSub.ScanOrientation.Title="Orientation";
ScanFrame.IconAndOptionsFrame.OptionsFrame.ScanSub.ScanOrientation.LongHelp="Displays scan orientation.";

// MESH

MeshFrame.HeaderFrame.Global.Title="Mesh";
MeshFrame.HeaderFrame.Global.LongHelp="Defines the display options for meshes.";

MeshFrame.IconAndOptionsFrame.OptionsFrame.MeshSub.PolygonTriangles.Title="Triangles";
MeshFrame.IconAndOptionsFrame.OptionsFrame.MeshSub.PolygonTriangles.LongHelp="Displays mesh triangles.";
MeshFrame.IconAndOptionsFrame.OptionsFrame.MeshSub.PolygonVertex.Title="Vertex";
MeshFrame.IconAndOptionsFrame.OptionsFrame.MeshSub.PolygonVertex.LongHelp="Displays mesh vertices.";
MeshFrame.IconAndOptionsFrame.OptionsFrame.MeshSub.PolygonFreeEdges.Title="Free Edges";
MeshFrame.IconAndOptionsFrame.OptionsFrame.MeshSub.PolygonFreeEdges.LongHelp="Displays mesh free edges.";
MeshFrame.IconAndOptionsFrame.OptionsFrame.MeshSub.PolygonNonManifoldEdges.Title="Non-manifold Edges";
MeshFrame.IconAndOptionsFrame.OptionsFrame.MeshSub.PolygonNonManifoldEdges.LongHelp="Displays mesh non-manifold edges.";
MeshFrame.IconAndOptionsFrame.OptionsFrame.MeshSub.PolygonSchrink.Title="Shrink";
MeshFrame.IconAndOptionsFrame.OptionsFrame.MeshSub.PolygonSchrink.LongHelp="Displays mesh in shrunk mode.";
MeshFrame.IconAndOptionsFrame.OptionsFrame.MeshSub.PolygonFlat.Title="Flat";
MeshFrame.IconAndOptionsFrame.OptionsFrame.MeshSub.PolygonFlat.LongHelp="Displays mesh in flat mode.";
MeshFrame.IconAndOptionsFrame.OptionsFrame.MeshSub.PolygonSmooth.Title="Smooth";
MeshFrame.IconAndOptionsFrame.OptionsFrame.MeshSub.PolygonSmooth.LongHelp="Displays mesh in smooth mode.";
MeshFrame.IconAndOptionsFrame.OptionsFrame.MeshSub.PolygonNormal.Title="Normal";
MeshFrame.IconAndOptionsFrame.OptionsFrame.MeshSub.PolygonNormal.LongHelp="Displays mesh normal.";

// LIGHT VISU

LightFrame.HeaderFrame.Global.Title="Light Visualization";
LightFrame.HeaderFrame.Global.LongHelp="Defines the display options for mesh light visualization.";

LightFrame.IconAndOptionsFrame.OptionsFrame.LightSub.LightChk.Title="Activate light visualization of meshes";
LightFrame.IconAndOptionsFrame.OptionsFrame.LightSub.LightChk.LongHelp="Activates light visualization of meshes.";
LightFrame.IconAndOptionsFrame.OptionsFrame.LightSub.NbOfTrianglesLbl.Title="Number of triangles";
LightFrame.IconAndOptionsFrame.OptionsFrame.LightSub.NbOfTrianglesLbl.LongHelp="Activates light visualization if the number of triangles contained in the mesh is greater than this number.";

// DYNAMIC INFO

DynaFrame.HeaderFrame.Global.Title="Dynamic Display";
DynaFrame.HeaderFrame.Global.LongHelp="Defines dynamic display of information.";

DynaFrame.IconAndOptionsFrame.OptionsFrame.DynaSub.DynaInfo.Title="Dynamic display of information";
DynaFrame.IconAndOptionsFrame.OptionsFrame.DynaSub.DynaInfo.LongHelp="Activates dynamic display of information.";

// HIGHLIGHT MODE

HighlightModeFrame.HeaderFrame.Global.Title="Highlight mode";
HighlightModeFrame.HeaderFrame.Global.LongHelp="Defines highlight mode for clouds of points and/or meshes (bounding boxe vs. redrawing).";

HighlightModeFrame.IconAndOptionsFrame.OptionsFrame.HighlightModeSubFrame.HighlightMode.Title="Bounding Box";
HighlightModeFrame.IconAndOptionsFrame.OptionsFrame.HighlightModeSubFrame.HighlightMode.LongHelp="Highlights selected clouds of points and/or meshes as their bounding box.";
