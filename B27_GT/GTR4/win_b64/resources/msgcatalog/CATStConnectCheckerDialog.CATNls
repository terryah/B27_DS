//=============================================================================
// COPYRIGHT DASSAULT SYSTEMES  1998
//=============================================================================
//
// CATStConnectCheckerDialog: Resource file for NLS purposes: 
// Connect Checker Dialog Box
//
//=============================================================================
//
// Implementation Notes:
//
//=============================================================================
// April '98						Thierry Caro
//=============================================================================

ConnectCheckerDialog.ConnectCheckerDialogName.Title = "Connect Checker";


ConnectCheckerDialog.OKTitle.Title = "Close";


Frm1.InterpolCombo.LongHelp = "Interpolation type selection"; 


Frm1.InterpolCombo.InterpolComboLinear.Title = "Linear";
Frm1.InterpolCombo.InterpolComboLinear.LongHelp = "Defines a linear interpolation."; 

Frm1.InterpolCombo.InterpolComboSharpLeft.Title = "Sharp Left";
Frm1.InterpolCombo.InterpolComboSharpLeft.LongHelp = "Sets the interpolation to: sharp left"; 


Frm1.InterpolCombo.InterpolComboSharpCenter.Title = "Sharp Center";
Frm1.InterpolCombo.InterpolComboSharpCenter.LongHelp = "Sets the interpolation to: center"; 


Frm1.InterpolCombo.InterpolComboSharpRight.Title = "Sharp Right";
Frm1.InterpolCombo.InterpolComboSharpRight.LongHelp = "Sets the interpolation to: sharp right"; 


Frm1.LabelMinName.Title = "Min: ";
Frm1.LabelMinName.LongHelp = "Indicates the minimum computed value between the surfaces";


Frm1.LabelMaxName.Title = "Max: ";
Frm1.LabelMaxName.LongHelp = "Indicates the maximum computed value between the surfaces";


Frm1.UnitMillimeter.Title = " mm";
Frm1.UnitDegree.Title = " deg";
Frm1.UnitPercent.Title = " %";


FrmAnalType.LabelDACCombo.Title = "Analysis type";
FrmAnalType.LabelDACCombo.LongHelp =
"Selects the analysis type: distance,
tangency, or curvature analysis."; 


FrmAnalType.DACCombo.DACComboDistances.Title = "Distance";
FrmAnalType.DACCombo.DACComboDistances.LongHelp = "Performs a distance analysis"; 


FrmAnalType.DACCombo.DACComboAngles.Title = "Tangency";
FrmAnalType.DACCombo.DACComboAngles.LongHelp = "Performs a tangency analysis"; 


FrmAnalType.DACCombo.DACComboCurvatures.Title = "Curvature";
FrmAnalType.DACCombo.DACComboCurvatures.LongHelp = "Performs a curvature analysis"; 


FrmAnalType.InternalEdgesCheckB.Title = "Internal edges";
FrmAnalType.InternalEdgesCheckB.LongHelp = "Analyzes the internal connections in skins";


FrmDiscretization.Title = "Discretization";
FrmDiscretization.LongHelp = "Defines the discretization step";


FrmDiscretization.LightRadB.Title = "Light";
FrmDiscretization.LightRadB.LongHelp = "Sets the discretization type to light";


FrmDiscretization.CoarseRadB.Title = "Coarse";
FrmDiscretization.CoarseRadB.LongHelp = "Sets the discretization type to coarse";


FrmDiscretization.MediumRadB.Title = "Medium";
FrmDiscretization.MediumRadB.LongHelp = "Sets the discretization type to medium";


FrmDiscretization.FineRadB.Title = "Fine";
FrmDiscretization.FineRadB.LongHelp = "Sets the discretization type to fine";


FrmDisplay.Title = "Display";
FrmDisplay.LongHelp = "Displays options";


FrmTol.Title = "Maximum gap";
FrmTol.LongHelp = "Sets the tolerance for neighboring surfaces";


FrmDisplay.FrmCheckBDisplay.ColorScaleChkB.Title = "Color Scale";
FrmDisplay.FrmCheckBDisplay.ColorScaleChkB.LongHelp = "Displays the color scale";


FrmDisplay.FrmCheckBDisplay.CombChkB.Title = "Comb";
FrmDisplay.FrmCheckBDisplay.CombChkB.LongHelp = "Displays the analysis comb";


FrmDisplay.FrmCheckBDisplay.EnvelopeChkB.Title = "Envelope";
FrmDisplay.FrmCheckBDisplay.EnvelopeChkB.LongHelp = "Displays the envelope bounding the comb";


FrmDisplay.FrmCheckBDisplay.InfoChkB.Title = "Information";
FrmDisplay.FrmCheckBDisplay.InfoChkB.LongHelp = "Shows the minimum and maximum values locations";


FrmDisplay.FrmScale.Title = "Scaling";
FrmDisplay.FrmScale.LongHelp = "Defines the scaling behavior";


FrmDisplay.FrmScale.AutomaticScaleButton.Title = "Automatic";
FrmDisplay.FrmScale.AutomaticScaleButton.LongHelp = "Chooses an automatic scale to display the analysis comb";


FrmDisplay.FrmScale.SpinnerScaleButton.LongHelp = "Defines the scale value";

FrmDisplay.FrmScale.ScaleSpinner.LongHelp = "Defines the scale value";


FrmInfoNumbers.NbSelSurf.Title = " surface(s) selected";

FrmInfoNumbers.NbConnections.Title = " connection(s) detected";


Distance.ShortName = "Dt";
Tangency.ShortName = "Tg";
Curvature.ShortName = "Cv";






