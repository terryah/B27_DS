//---------------------------------------------------------------
// Resource file for Component Catalogs Commands
// Us
//---------------------------------------------------------------

// Editor/Reference
CATCuiCommands.Reference.Title                             = "Reference";
CATCuiCommands.Reference.Name.Title                        = "Name";
CATCuiCommands.Reference.Type.Title                        = "Type";
CATCuiCommands.Reference.Object.Title                      = "Object Name";


// Definition Dialog Box Titles
CATCuiCommands.ChapterDefinition.Title                     = "Chapter Definition";
CATCuiCommands.ComponentsFamilyDefinition.Title            = "Family Definition";
CATCuiCommands.PartsFamilyDefinition.Title                 = "Part Family Definition";
CATCuiCommands.DescriptionDefinition.Title                 = "Description Definition";
CATCuiCommands.KeywordDefinition.Title                     = "Keyword Definition";
CATCuiCommands.KeywordDiscreteValue.Title                  = "Legal Value Definition";

// Chapter/Description Definition Name Frame
CATCuiCommands.Definition.Name.Title                       = "Name: ";
CATCuiCommands.Definition.Name.LongHelp                    = "Give a name of the new object.";
CATCuiCommands.Definition.Selection.Title                  = "Type: ";
CATCuiCommands.Definition.Selection.LongHelp               = "Select an application type for this chapter.";
CATCuiCommands.Definition.SelectionCmb.LongHelp            = "Select an application type for this chapter.";

CATCuiCommands.Definition.Name.Default.Description         = "Description";
CATCuiCommands.Definition.Name.Default.DescriptionFolder   = "ComponentFamily";
CATCuiCommands.Definition.Name.Default.ChapterFolder       = "Chapter";
CATCuiCommands.Definition.Name.Default.DesignTableFolder   = "PartFamily";
CATCuiCommands.Definition.Name.Default.PersistentQuery     = "ResolvedCatalog";

//CATCuiCommands.Definition.Icon.Separator.Title             = "Icon  ";
//CATCuiCommands.Definition.Icon.Select.Title                = "Icon choice: ";


// Chapter/Description Definition Reference Frame
CATCuiCommands.Definition.Reference.Title                  = "Reference";
CATCuiCommands.Definition.Reference.LongHelp               = "Specify the referenced document or object.";
CATCuiCommands.Definition.Reference.FileName.Title         = "File name:";
CATCuiCommands.Definition.Reference.FileName.LongHelp      = "Display the name of the referenced document or object.";
CATCuiCommands.Definition.Reference.ObjectName.Title       = "Object name:";
CATCuiCommands.Definition.Reference.ObjectName.LongHelp    = "Display the name of referenced object in the document.";

CATCuiCommands.Definition.Reference.Type.Title             = "Type: ";
CATCuiCommands.Definition.Reference.Type.LongHelp          = "Display the kind of the reference.";
CATCuiCommands.Definition.Reference.Type.Unloaded          = "Unloaded";
CATCuiCommands.Definition.Reference.Type.Unknown           = "Unknown";
CATCuiCommands.Definition.Reference.Type.Unset             = "<Unset>";
CATCuiCommands.Definition.Reference.Type.Document          = "Document";
CATCuiCommands.Definition.Reference.Type.Feature           = "Feature";
CATCuiCommands.Definition.Reference.Type.Chapter           = "Chapter";
CATCuiCommands.Definition.Reference.Type.ExtChapter        = "External chapter";
CATCuiCommands.Definition.Reference.Type.Family            = "Family";
CATCuiCommands.Definition.Reference.Type.ExtFamily         = "External family";
CATCuiCommands.Definition.Reference.Type.Description       = "External description";
CATCuiCommands.Definition.Reference.Type.PartFamily            = "Part family configuration";
CATCuiCommands.Definition.Reference.Type.PartFamilyComputed    = "Resolved part family configuration";
CATCuiCommands.Definition.Reference.Type.PartFamilyComputedOK  = "Resolved part family configuration (updated)";
CATCuiCommands.Definition.Reference.Type.PartFamilyComputedKO  = "Resolved part family configuration (not updated)";

CATCuiCommands.Definition.Reference.ResMode.Title          = "Resolution mode";
CATCuiCommands.Definition.Reference.ResMode.LongHelp       = "Display the resolution mode for Part Family.";
CATCuiCommands.Definition.Reference.ResMode.CanBeResolved  = "Descriptions can be resolved";
CATCuiCommands.Definition.Reference.ResMode.ToResolve      = "Descriptions will be resolved";
CATCuiCommands.Definition.Reference.ResMode.CanNotBeResolved = "Descriptions cannot be resolved";
CATCuiCommands.Definition.Reference.ResMode.OLDToResolve      = "Descriptions are to resolve";
CATCuiCommands.Definition.Reference.ResMode.OLDCanNotBeResolved = "Descriptions can't be resolved";

CATCuiCommands.Definition.Reference.SyncMode.Title         = "Resolved Description synchronization mode";
CATCuiCommands.Definition.Reference.SyncMode.LongHelp      = "Display how to synchronize descriptions in a Part Family.";
CATCuiCommands.Definition.Reference.SyncMode.Always        = "Always regenerate the Part";
CATCuiCommands.Definition.Reference.SyncMode.Cdt           = "Regenerate Part only if the Design Table row has been modified";
CATCuiCommands.Definition.Reference.SyncMode.OLDCdt        = "Regenerate Part only if the Design Table row is modified";

CATCuiCommands.Definition.Reference.SelectFile.Title       = "Select Document";
CATCuiCommands.Definition.Reference.SelectFile.LongHelp    = "Allows to select the referenced document.";
CATCuiCommands.Definition.Reference.FileSelection.Title    = "Select File";
CATCuiCommands.Definition.Reference.FileSelection.LongHelp = "Select the file.";

CATCuiCommands.Definition.SyncChapters.Title               = "Synchronize Chapters";
CATCuiCommands.Definition.SyncChapters.SelectedChapters.Title    = "Selected Chapters/Families: ";
CATCuiCommands.Definition.SyncChapters.SubChapters.Title         = "Propagate to sub-catalogs";
CATCuiCommands.Definition.SyncChapters.Synchronize.Title         = "Synchronize";
CATCuiCommands.Definition.SyncChapters.Status.Title              = " ";
CATCuiCommands.Definition.SyncChapters.SyncReport.Title          = "Synchronization report";
CATCuiCommands.Definition.SyncChapters.SyncReport.Cl1            = "Chapter";
CATCuiCommands.Definition.SyncChapters.SyncReport.Cl2            = "From element";
CATCuiCommands.Definition.SyncChapters.SyncReport.Cl3            = "Type";
CATCuiCommands.Definition.SyncChapters.SyncReport.Cl4            = "Pointed document";
CATCuiCommands.Definition.SyncChapters.SyncReport.Cl5            = "Status";
CATCuiCommands.Definition.SyncChapters.SyncReport.Sync           = "Already synchronized";
CATCuiCommands.Definition.SyncChapters.SyncReport.OK             = "Succeeded";
CATCuiCommands.Definition.SyncChapters.SyncReport.KO             = "Failed";
CATCuiCommands.Definition.SyncChapters.SyncReport.PF             = "Part Family";
CATCuiCommands.Definition.SyncChapters.SyncReport.PFI            = "Part Family Instance";
CATCuiCommands.Definition.SyncChapters.SyncReport.Comp           = "Component";
CATCuiCommands.Definition.SyncChapters.SyncReport.Feat           = "Feature";
CATCuiCommands.Definition.SyncChapters.SyncReport.ErrMsg1        = "The Generative Part and/or the Design Table cannot be found.\n1. Check that the generative part exists and is pointing the design table\n2. Check the Design Table exists\nClose and reopen the catalog.";


CATCuiCommands.Definition.Visibility.Title                 = "Visibility";
CATCuiCommands.Definition.Visibility.LongHelp              = "Allows display in the Catalog Browser.";


// Chapter/Description Definition Keywords Frame
CATCuiCommands.Definition.Keywords.Title                   = "Keyword Values";


// Keyword Definition Frame
CATCuiCommands.Definition.Keyword.Type.Editor.LongHelp     = "Displays the type of the keyword.";
CATCuiCommands.Definition.Keyword.Type.Selector.LongHelp   = "Select the type of the keyword.";
CATCuiCommands.Definition.Keyword.Type.LongHelp            = "Keyword type definition.";
CATCuiCommands.Definition.Keyword.Type.Title               = "Type";
CATCuiCommands.Definition.Keyword.Value.Title              = "Default Value";
CATCuiCommands.Definition.Keyword.Value.LongHelp           = "Keyword value definition.";
CATCuiCommands.Definition.Keyword.Unset.Title              = "Unset";
CATCuiCommands.Definition.Keyword.Unset.LongHelp           = "Unset the default value.";
CATCuiCommands.Definition.Keyword.DiscreteValue.Title      = "With discrete list of values";
CATCuiCommands.Definition.Keyword.DiscreteValue.LongHelp   = "Check to allow legal values for the keyword.";


CATCuiCommands.Definition.Keyword.List.Title               = "Description Keyword Values";
CATCuiCommands.Definition.Keyword.List.LongHelp            = "Give a value to the keyword(s)\nin order to filter the displayed list\nusing a Search query on the chapter";


// Chapter/Description Definition Preview Frame
CATCuiCommands.Definition.Preview.Title                    = "Browser Preview";
CATCuiCommands.Definition.Preview.LongHelp                 = "Specify the preview for the catalog browser.";
CATCuiCommands.Definition.Preview.Mode.Default.Title       = "Default";
CATCuiCommands.Definition.Preview.Mode.Default.LongHelp    = "Sets the default catalog icon as catalog browser preview.";
CATCuiCommands.Definition.Preview.Mode.Document.Title      = "Referenced document preview";
CATCuiCommands.Definition.Preview.Mode.Document.LongHelp   = "Sets the referenced document preview as catalog browser preview.";
CATCuiCommands.Definition.Preview.Mode.External.Title      = "External file preview";
CATCuiCommands.Definition.Preview.Mode.External.LongHelp   = "Sets the external file as catalog browser preview.";
CATCuiCommands.Definition.Preview.Mode.PreviewEditor.LongHelp = "Display the selected external preview file name.";
CATCuiCommands.Definition.Preview.Mode.SelectFile.Title    = "Select an external preview file";
CATCuiCommands.Definition.Preview.Mode.SelectFile.LongHelp = "Allows to select an external preview file.";
CATCuiCommands.Definition.Preview.FileSelection.Title      = "Select Preview";
CATCuiCommands.Definition.Preview.FileSelection.LongHelp   = "Select a preview file.";
CATCuiCommands.Definition.Preview.Mode.Local.Title         = "Store preview in catalog";
CATCuiCommands.Definition.Preview.Mode.Local.LongHelp      = "Allows to save the preview in catalog.";
CATCuiCommands.Definition.Preview.Display.LongHelp         = "Display of the selected preview.";


// Design Table List
CATCuiCommands.DesignTableList.Title                       = "Part Family Design Table List";
CATCuiCommands.DesignTableList.LongHelp                    = "List of available Design Table(s) found in the selected reference document.";


// Resolved Filter Definition
CATCuiCommands.ResolutedFilter.Title                       = "Filter Definition";
CATCuiCommands.ResolutedFilter.CatalogPath.Title           = "Reference Catalog:";                   
CATCuiCommands.ResolutedFilter.CatalogPathEditor.ShortHelp = "Catalog to be filtered";
CATCuiCommands.ResolutedFilter.CatalogPathEditor.LongHelp  = "Enter the path of the catalog to be filtered";
CATCuiCommands.ResolutedFilter.CatalogButton.Title         = "Browse...";                   
CATCuiCommands.ResolutedFilter.CatalogButton.ShortHelp     = "Select a catalog file";
CATCuiCommands.ResolutedFilter.CatalogButton.LongHelp      = "Browse to select a catalog file to filter";
CATCuiCommands.ResolutedFilter.SolveButton.Title           = "Apply Filter";                   
CATCuiCommands.ResolutedFilter.SolveButton.ShortHelp       = "Resolve the expression that is defined in the filter";
CATCuiCommands.ResolutedFilter.SolveButton.LongHelp        = "Resolve the expression that is defined in the filter";

NotYetSelected = "A chapter need to be selected prior to this command";
Warning      = "Warning";
InvalidChapter = "The resolve chapter name is not vaild";
Error      = "Error";
UndefineQuery = "The filter expression is not defined";

Resolved = "Resolved";
NotResolved = "Not Resolved";


// Error & Warning Messages
CATCuiCommands.Error.Title                   = "Error";
CATCuiCommands.Error.InvalidName.Message     = "  Invalid name.\nThe 'Name' field is empty.";
CATCuiCommands.Error.DuplicateName.Message   = "  Invalid name: the name of the component already exist.";
CATCuiCommands.Error.AddFailed.Message       = "Process Failed.";
CATCuiCommands.Error.ValueNotUpdated.Message = "Keyword value not taken into account.";
CATCuiCommands.Error.InvalidDocument.Message = "Invalid document.\n The document must be a CATPart \n containing at least one design table.";
CATCuiCommands.Warning.Title                 = "Warning";
CATCuiCommands.Warning.NoKeyword.Message     = "No keyword have been defined on /P1 chapter.";

// Commands dialog messages
// ------------------------
// AddChapter / AddFamily
CATCuiCommands.CATCuiAddChapter.Panel.Message = "Enter a name in the panel";
CATCuiCommands.CATCuiAddChapter.Heritage.Title = "Copy Keywords";
CATCuiCommands.CATCuiAddChapter.Heritage.ShortHelp = "Keywords from the father chapter would be created on this new chapter";
CATCuiCommands.CATCuiAddChapter.Heritage.LongHelp = "Keywords from the father chapter would be created on this new chapter";


// AddExternalChapter
CATCuiCommands.CATCuiAddLinkToChapter.SelectExternalChapter.Message = "Select a chapter in another catalog document editor";
IncompatibilityWarning = "A BlackBox catalog cannot point an Exploded catalog";

// AddPartFamilyDesignTableChapter
CATCuiCommands.CATCuiAddChapterFromDesignTable.Panel.Message = "Enter part family information in the panel";
CATCuiCommands.CATCuiAddChapterFromDesignTable.SelDesignTable.Message = "Select one or more design table";


// CATCuiAddComponentsFromDesignTable
CATCuiCommands.CATCuiAddComponentsFromDesignTable.Panel.Message = "Enter part family information in the panel";
CATCuiCommands.CATCuiAddComponentsFromDesignTable.SelDesignTable.Message = "Select one or more design table";


// AddKeyword
CATCuiCommands.CATCuiAddKeyword.Panel.Message = "Enter a name, select a type and set a default value in the panel";
CATCuiCommands.CATCuiAddKeyword.AddDiscreteValues.Message = "Enter legal value list";

CATCuiCommands.CATCuiAddKeyword.NoKeywordType.Message = "Invalid Keyword type.";


// AddResolvedFilter
CATCuiCommands.CATCuiAddResolvedFilter.NoChapter.Message = "Catalog to be filtered no defined!"; 


// Compute Descriptions Document Preview
ProgressTaskUI.Title               = "Compute Browser Preview...";

// ReSynchronize Command
CATCuiCommands.ReSynchronize.Current.Title		= "Use catalog defined in Filter";
CATCuiCommands.ReSynchronize.Current.LongHelp		= "To keep the current Resolved catalog.";
CATCuiCommands.ReSynchronize.Session.Title		= "Select a catalog in session";
CATCuiCommands.ReSynchronize.Session.LongHelp		= "To use a catalog in session.";
// CATCuiCommands.ReSynchronize.Enovia.Title		= "Use an ENOVIA V5 based catalog";
// CATCuiCommands.ReSynchronize.Enovia.LongHelp		= "To Use a catalog stored in ENOVIA.";
CATCuiCommands.ReSynchronize.FileBased.Title		= "Use a File-Based catalog";
CATCuiCommands.ReSynchronize.FileBased.LongHelp		= "Change to another File-based catalog.";
CATCuiCommands.ReSynchronize.Browse.Title		= "Browse";
CATCuiCommands.ReSynchronize.Browse.LongHelp		= "Browse for a catalog";
CATCuiCommands.ReSynchronize.MainPanel.Title		= "Synchronize a catalog";
CATCuiCommands.ReSynchronize.MainPanel.LongHelp		= "Synchronize a catalog";

CATCuiCommands.Warning.DocAlreadyLoaded.Message = "A document with same identification is already loaded in the session from another location :\n '/p' \nThe Catalog Browser for this found document will be opened and not for the selected document";	


CATCuiCommands.CATCuiAddChapter.UndoTitle = "Add Chapter";
CATCuiCommands.CATCuiAddChapterFromDesignTable.UndoTitle = "Add Part Family";
CATCuiCommands.CATCuiAddComponentsFromDesignTable.UndoTitle = "Add Part Components";
CATCuiCommands.CATCuiAddKeyword.UndoTitle = "Add Keyword";
CATCuiCommands.CATCuiAddResolvedFilter.UndoTitle = "Filter Definition";
CATCuiCommands.CATCuiHeritageKeyword.UndoTitle = "Copy Keywords";
CATCuiCommands.CATCuiRECPublishCatalogObjectCmd.UndoTitle = "Publish Catalog";
CATCuiCommands.CATCuiReSynchronizeQueries.UndoTitle = "Synchronize Queries";
CATCuiCommands.CATCuiResetAllSubQueries.UndoTitle = "Reset";
CATCuiCommands.CATCuiEditChapter.UndoTitle = "Definition";
CATCuiCommands.CATCuiEditResolvedFilter.UndoTitle = "Resolve Filter";
