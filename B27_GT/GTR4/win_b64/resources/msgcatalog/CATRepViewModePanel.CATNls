//--------------------------------------
// Resource file 
// En_EN
//--------------------------------------
Title="Rep View Mode Customization";

//No Display frame
mainFrame.optionFrame.nodisplayFrame.noneBtn.Title="No Display";
mainFrame.optionFrame.nodisplayFrame.noneBtn.ShortHelp="Makes the object invisible but pickable. Object is displayed in highlight when selected with mouse click.";
mainFrame.optionFrame.nodisplayFrame.noneBtn.Help="Makes the object invisible but pickable. Object is displayed in highlight when selected with mouse click.";
//-----------------------------------------------------------------------------
// Edges frame
//-----------------------------------------------------------------------------
mainFrame.optionFrame.edgeOptionFrame.Title="Lines and points";

//--------------------------------------
// Edges
//--------------------------------------
mainFrame.optionFrame.edgeOptionFrame.edgeFrame.edgeBtn.Title="Edges and points";
mainFrame.optionFrame.edgeOptionFrame.edgeFrame.edgeBtn.ShortHelp="Displays the geometry with points and edges.";
mainFrame.optionFrame.edgeOptionFrame.edgeFrame.edgeBtn.Help="Displays the geometry with points and edges.";
//--------------------------------------
// All Edges
//--------------------------------------
mainFrame.optionFrame.edgeOptionFrame.edgeRadioFrame.allEdgeBtn.Title="All edges";
mainFrame.optionFrame.edgeOptionFrame.edgeRadioFrame.allEdgeBtn.ShortHelp="Displays the geometry with all edges, whatever their type (smooth or not).";
mainFrame.optionFrame.edgeOptionFrame.edgeRadioFrame.allEdgeBtn.Help="Displays the geometry with all edges, whatever their type (smooth or not).";

//--------------------------------------
//  withHalfLightedSmoothEdges
//--------------------------------------
mainFrame.optionFrame.edgeOptionFrame.edgeRadioFrame.halfLightedSmoothEdgeBtn.Title="Half visible smooth edges";
mainFrame.optionFrame.edgeOptionFrame.edgeRadioFrame.halfLightedSmoothEdgeBtn.ShortHelp="Partially displays smooth edges which are drawn in such
a way that they are less visible than sharp edges.";
mainFrame.optionFrame.edgeOptionFrame.edgeRadioFrame.halfLightedSmoothEdgeBtn.Help="Partially displays smooth edges which are drawn in such
a way that they are less visible than sharp edges.";
//--------------------------------------
// WithoutSmoothEdges 
//--------------------------------------
mainFrame.optionFrame.edgeOptionFrame.edgeRadioFrame.withoutSmoothEdgeBtn.Title="No smooth edges";
mainFrame.optionFrame.edgeOptionFrame.edgeRadioFrame.withoutSmoothEdgeBtn.ShortHelp="Hides smooth edges on the geometry.";
mainFrame.optionFrame.edgeOptionFrame.edgeRadioFrame.withoutSmoothEdgeBtn.Help="Hides smooth edges on the geometry.";
//--------------------------------------
//  All points
//--------------------------------------
mainFrame.optionFrame.edgeOptionFrame.pointRadioFrame.allPointBtn.Title="All points";
mainFrame.optionFrame.edgeOptionFrame.pointRadioFrame.allPointBtn.ShortHelp="Displays edges and all their points, including vertices.";
mainFrame.optionFrame.edgeOptionFrame.pointRadioFrame.allPointBtn.Help="Displays edges and all their points, including vertices.";
//--------------------------------------
//  Without vertices
//--------------------------------------
mainFrame.optionFrame.edgeOptionFrame.pointRadioFrame.withoutVertexBtn.Title="No vertices";
mainFrame.optionFrame.edgeOptionFrame.pointRadioFrame.withoutVertexBtn.ShortHelp="Deactivates the display of vertices for edges.
Only free points are displayed and can be selected.
This enhances the overall readibility for complex models
and has a positive impact on performance.";
mainFrame.optionFrame.edgeOptionFrame.pointRadioFrame.withoutVertexBtn.Help="Deactivates the display of vertices for edges.
Only free points are displayed and can be selected.
This enhances the overall readibility for complex models
and has a positive impact on performance.";


//-----------------------------------------------------------------------------
// Hidden frame
//-----------------------------------------------------------------------------
mainFrame.optionFrame.hiddenOptionsFrame.Title="Hide Elements";
//--------------------------------------
//  Nowire
//--------------------------------------
mainFrame.optionFrame.hiddenOptionsFrame.withoutwiresFrame.withoutwiresBtn.Title="No wires";
mainFrame.optionFrame.hiddenOptionsFrame.withoutwiresFrame.withoutwiresBtn.ShortHelp="Deactivates the display of wires.";
mainFrame.optionFrame.hiddenOptionsFrame.withoutwiresFrame.withoutwiresBtn.Help="Deactivates the display of wires.";
//--------------------------------------
//  Noaxis
//--------------------------------------
mainFrame.optionFrame.hiddenOptionsFrame.withoutaxisFrame.withoutaxisBtn.Title="No axes";
mainFrame.optionFrame.hiddenOptionsFrame.withoutaxisFrame.withoutaxisBtn.ShortHelp="Deactivates the display of axes and axis systems.";
mainFrame.optionFrame.hiddenOptionsFrame.withoutaxisFrame.withoutaxisBtn.Help="Deactivates the display of axes and axis systems.";
//--------------------------------------
//  NoPoints
//--------------------------------------
mainFrame.optionFrame.hiddenOptionsFrame.withoutpointsFrame.withoutpointsBtn.Title="No points";
mainFrame.optionFrame.hiddenOptionsFrame.withoutpointsFrame.withoutpointsBtn.ShortHelp="Deactivates the display of points.";
mainFrame.optionFrame.hiddenOptionsFrame.withoutpointsFrame.withoutpointsBtn.Help="Deactivates the display of points.";


//-----------------------------------------------------------------------------
// Mesh frame
//-----------------------------------------------------------------------------
mainFrame.optionFrame.meshOptionFrame.Title="Mesh";

//--------------------------------------
//  SimpleShading
//--------------------------------------
mainFrame.optionFrame.meshOptionFrame.simpleShadingFrame.simpleShadingBtn.Title="Shading";
mainFrame.optionFrame.meshOptionFrame.simpleShadingFrame.simpleShadingBtn.ShortHelp="Defines the type of shading to be used for displaying the geometry.";
mainFrame.optionFrame.meshOptionFrame.simpleShadingFrame.simpleShadingBtn.Help="Defines the type of shading to be used for displaying the geometry.";
//--------------------------------------
//  Gouraud
//--------------------------------------
mainFrame.optionFrame.meshOptionFrame.shadingRadioFrame.shadingFrame.shadingBtn.Title="Gouraud";
mainFrame.optionFrame.meshOptionFrame.shadingRadioFrame.shadingFrame.shadingBtn.ShortHelp="Displays the geometry in Gouraud shading.
This surface-shading technique linearly interpolates a color or a shade across a
polygon to make the geometry appear smoother.";
mainFrame.optionFrame.meshOptionFrame.shadingRadioFrame.shadingFrame.shadingBtn.Help="Displays the geometry in Gouraud shading.
This surface-shading technique linearly interpolates a color or a shade across a
polygon to make the geometry appear smoother.";

//--------------------------------------
// Polygons
//--------------------------------------
mainFrame.optionFrame.meshOptionFrame.shadingRadioFrame.polygonFrame.polygonBtn.Title="Triangles";
mainFrame.optionFrame.meshOptionFrame.shadingRadioFrame.polygonFrame.polygonBtn.ShortHelp="Displays the triangles on the geometry's faces.";
mainFrame.optionFrame.meshOptionFrame.shadingRadioFrame.polygonFrame.polygonBtn.Help="Displays the triangles on the geometry's faces.";

//-----------------------------------------------------------------------------
// Special Draw frame
//-----------------------------------------------------------------------------
mainFrame.optionFrame.specialDrawOptionsFrame.Title="Special Draw";

//--------------------------------------
//  outline
//--------------------------------------
mainFrame.optionFrame.specialDrawOptionsFrame.outlineFrame.outlineBtn.Title="Outlines";
mainFrame.optionFrame.specialDrawOptionsFrame.outlineFrame.outlineBtn.ShortHelp="Determines whether outlines are displayed or not.
This mode is especially useful when working with surfaces
of revolution (such as shafts or cylinders).
There is a price to pay in memory usage when using this mode.";
mainFrame.optionFrame.specialDrawOptionsFrame.outlineFrame.outlineBtn.Help="Determines whether outlines are displayed or not.
This mode is especially useful when working with surfaces
of revolution (such as shafts or cylinders).
There is a price to pay in memory usage when using this mode.";
//--------------------------------------
//  hrd
//--------------------------------------
mainFrame.optionFrame.specialDrawOptionsFrame.hrdFrame.hrdBtn.Title="Dynamic hidden line removal";
mainFrame.optionFrame.specialDrawOptionsFrame.hrdFrame.hrdBtn.ShortHelp="Displays the geometry in quick dynamic hidden line removal mode.
In this mode, 3D faces visually blank out any element located behind them,
including 1D elements (such as wireframe elements).";
mainFrame.optionFrame.specialDrawOptionsFrame.hrdFrame.hrdBtn.Help="Displays the geometry in quick dynamic hidden line removal mode.
In this mode, 3D faces visually blank out any element located behind them,
including 1D elements (such as wireframe elements).";
//--------------------------------------
// HiddenEdges & points 
//--------------------------------------
mainFrame.optionFrame.specialDrawOptionsFrame.hedgeFrame.hedgeBtn.Title="Hidden edges and points";
mainFrame.optionFrame.specialDrawOptionsFrame.hedgeFrame.hedgeBtn.ShortHelp="Displays the geometry in Shading mode with points and edges, whether hidden or not. Hidden edges are drawn as dotted.";
mainFrame.optionFrame.specialDrawOptionsFrame.hedgeFrame.hedgeBtn.Help="Displays the geometry in Shading mode with points and edges, whether hidden or not. Hidden edges are drawn as dotted.";

