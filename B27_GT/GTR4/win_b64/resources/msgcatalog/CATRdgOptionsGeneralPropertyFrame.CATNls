// COPYRIGHT DASSAULT SYSTEMES 2003
//===========================================================================
//
// CATRdgOptionsGeneralPropertyFrame
//
//===========================================================================


//--------------------------------------
// Environment creation options
//--------------------------------------
environmentFrame.HeaderFrame.Global.Title = "New Environment";

environmentFrame.IconAndOptionsFrame.OptionsFrame.environmentSnapCheck.Title = "Snap to Geometry";
environmentFrame.IconAndOptionsFrame.OptionsFrame.environmentSnapCheck.Help = "Snaps the environment to the geometry.";
environmentFrame.IconAndOptionsFrame.OptionsFrame.environmentSnapCheck.ShortHelp = "Snap Environment to Geometry";
environmentFrame.IconAndOptionsFrame.OptionsFrame.environmentSnapCheck.LongHelp =
"When checked, new environments will be created so that their bottom
wall (or their center in the case of spherical environments) is snapped to 
the geometry lowest point.";

//--------------------------------------
// Light creation options
//--------------------------------------
creationLightFrame.HeaderFrame.Global.Title = "New Light Position";

creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationRadio1.Title = "Default mode";
creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationRadio1.Help = "Light is oriented downstairs.";
creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationRadio1.ShortHelp = "Default light orientation";
creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationRadio1.LongHelp =
"With this mode, the light direction is perpendicular to current viewpoint direction,
and oriented downstairs.";

creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationRadio2.Title = "As viewpoint";
creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationRadio2.Help = "Light is oriented as current viewpoint";
creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationRadio2.ShortHelp = "Orientation similar as viewpoint";
creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationRadio2.LongHelp =
"With this mode, the light direction is confused with current viewpoint direction. 
Light origin is located at user's eyes origin.";

creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationRadio3.Title = "Gravitational";
creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationRadio3.Help = "Light is parallel to selected absolute axis";
creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationRadio3.ShortHelp = "Orientation along an absolute axis";
creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationRadio3.LongHelp =
"With this mode, the light direction is parallel to X, Y or Z absolute axis,
and oriented downstairs.";


creationLightFrame.IconAndOptionsFrame.OptionsFrame.axisLightFrame.lightCreationAxis1.Title = "X";
creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationAxis1.Help = "Absolute X axis";
creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationAxis1.ShortHelp = "Absolute X axis";
creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationAxis1.LongHelp =
"Light direction will be parallel to X absolute axis.";

creationLightFrame.IconAndOptionsFrame.OptionsFrame.axisLightFrame.lightCreationAxis2.Title = "Y";
creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationAxis2.Help = "Absolute Y axis";
creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationAxis2.ShortHelp = "Absolute Y axis";
creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationAxis2.LongHelp =
"Light direction will be parallel to Y absolute axis.";

creationLightFrame.IconAndOptionsFrame.OptionsFrame.axisLightFrame.lightCreationAxis3.Title = "Z";
creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationAxis3.Help = "Absolute Z axis";
creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationAxis3.ShortHelp = "Absolute Z axis";
creationLightFrame.IconAndOptionsFrame.OptionsFrame.lightCreationAxis3.LongHelp =
"Light direction will be parallel to Z absolute axis.";

viewPointFrame.HeaderFrame.Global.Title = "View Mode";

viewPointFrame.IconAndOptionsFrame.OptionsFrame.viewpointModeCheck.Title = "Activated";
viewPointFrame.IconAndOptionsFrame.OptionsFrame.viewpointModeCheck.Help = "Activate/Deactivate the viewpoint statement";
viewPointFrame.IconAndOptionsFrame.OptionsFrame.viewpointModeCheck.ShortHelp = "Activate/Deactivate the viewpoint statement";
viewPointFrame.IconAndOptionsFrame.OptionsFrame.viewpointModeCheck.LongHelp =
"Activate/Deactivate the viewpoint statement when the workbench is entered.";

viewPointFrame.IconAndOptionsFrame.OptionsFrame.viewPointRadio1.Title = "Parallel";
viewPointFrame.IconAndOptionsFrame.OptionsFrame.viewPointRadio1.Help = "Set the viewpoint in parallel mode";
viewPointFrame.IconAndOptionsFrame.OptionsFrame.viewPointRadio1.ShortHelp = "Set the viewpoint in parallel mode";
viewPointFrame.IconAndOptionsFrame.OptionsFrame.viewPointRadio1.LongHelp =
"Sets the viewpoint in parallel mode when the workbench is entered.";

viewPointFrame.IconAndOptionsFrame.OptionsFrame.viewPointRadio2.Title = "Perspective";
viewPointFrame.IconAndOptionsFrame.OptionsFrame.viewPointRadio2.Help = "Set the viewpoint in perspective mode";
viewPointFrame.IconAndOptionsFrame.OptionsFrame.viewPointRadio2.ShortHelp = "Set the viewpoint in perspective mode";
viewPointFrame.IconAndOptionsFrame.OptionsFrame.viewPointRadio2.LongHelp =
"Sets the viewpoint in perspective mode when the workbench is entered.";

viewPointFrame.IconAndOptionsFrame.OptionsFrame.viewPointRadio3.Title = "Last saved";
viewPointFrame.IconAndOptionsFrame.OptionsFrame.viewPointRadio3.Help = "Set the viewpoint in the last saved mode";
viewPointFrame.IconAndOptionsFrame.OptionsFrame.viewPointRadio3.ShortHelp = "Set the viewpoint in the last saved mode";
viewPointFrame.IconAndOptionsFrame.OptionsFrame.viewPointRadio3.LongHelp =
"Sets the viewpoint in the last saved mode when the workbench is entered.";

//-----------------------------------------------------
// Material View Setting Option
//-----------------------------------------------------
materialViewFrame.HeaderFrame.Global.Title = "Material Display";

materialViewFrame.IconAndOptionsFrame.OptionsFrame.materialViewModeCheck.Title = "View material";
materialViewFrame.IconAndOptionsFrame.OptionsFrame.materialViewModeCheck.Help = "View Material";
materialViewFrame.IconAndOptionsFrame.OptionsFrame.materialViewModeCheck.ShortHelp = "View Material";
materialViewFrame.IconAndOptionsFrame.OptionsFrame.materialViewModeCheck.LongHelp =
"Activates or deactivates the material display when the workbench is entered.";

//--------------------------------------
// Plug-in options
//--------------------------------------
pluginFrame.HeaderFrame.Global.Title = "Rendering Engine";

pluginFrame.IconAndOptionsFrame.OptionsFrame.pluginCombo.Help = "Selects the software rendering engine";
pluginFrame.IconAndOptionsFrame.OptionsFrame.pluginCombo.ShortHelp = "Select Rendering Engine";
pluginFrame.IconAndOptionsFrame.OptionsFrame.pluginCombo.LongHelp =
"Selects the engine used to render images.";

Default = "Default";
