Title = "Define Optimization Problem";
EmptyLabel1.Title = " ";
EmptyLabel2.Title = "    ";

LightRunCheckButton.Title = "Run methods without filling the Undo log";
LightRunCheckButton.Help = "Enables to run the optimization with minimum memory consumption";
LightRunCheckButton.ShortHelp = "Run optimization without filling the Undo log";
LightRunCheckButton.LongHelp = "Check this option to avoid memory saturation. If this option is unchecked, the Undo log can be filled with much data which may lead to memory saturations.\nWARNING: after running the methods in this mode, you will not be able to undo the actions done before the run.";
RunAllMethodsPB.Title = "Run";
WithUpdateVisuRB.Title = "With update visualization";
WithoutUpdateVisuRB.Title = "Without update visualization";

TabContainer.IOTabPage.Title = "I/O Definition";
TabContainer.IOTabPage.InputsFrame.Title = "Inputs Definition";
TabContainer.IOTabPage.InputsFrame.InputsMultiList.Help = "Shows the settings applied to the input parameters";
TabContainer.IOTabPage.InputsFrame.InputsMultiList.ShortHelp = "This list shows the settings applied to the input parameters :\n- if '---' appears in the last three columns, the input will be constrained by a list of values:\nEdit the input to view/modify the list.\n- If one of the last three cases is empty, the corresponding Inf. Bound/Sup. Bound/Step value will not be defined:\nThe input is not constrained for this criterium.\n- If there 'NO' displays in the 'Variable' column, the input will keep its nominal value when running the methods:\nEdit the input to view/modify the nominal value.";
TabContainer.IOTabPage.InputsFrame.InputsMultiList.LongHelp = "Shows the settings applied to the input parameters :\n- if '---' appears in the last three columns, the input will be constrained by a list of values:\nEdit the input to view/modify the list.\n- If one of the last three cases is empty, the corresponding Inf. Bound/Sup. Bound/Step value will not be defined:\nThe input will not be constrained for this criterium.\n-If 'NO' displays in the 'Variable' column, the input will keep its nominal value when running the methods:\nEdit the input to view/modify the nominal value.";
TabContainer.IOTabPage.InputsFrame.InputsMultiList.ColumnTitle0 = "Input";
TabContainer.IOTabPage.InputsFrame.InputsMultiList.ColumnTitle1 = "Ref. Parameter";
TabContainer.IOTabPage.InputsFrame.InputsMultiList.ColumnTitle2 = "Type";
TabContainer.IOTabPage.InputsFrame.InputsMultiList.ColumnTitle3 = "Current Value";
TabContainer.IOTabPage.InputsFrame.InputsMultiList.ColumnTitle4 = "Variable";
TabContainer.IOTabPage.InputsFrame.InputsMultiList.ColumnTitle5 = "Inf. Bound";
TabContainer.IOTabPage.InputsFrame.InputsMultiList.ColumnTitle6 = "Sup. Bound";
TabContainer.IOTabPage.InputsFrame.InputsMultiList.ColumnTitle7 = "Step";
TabContainer.IOTabPage.InputsFrame.InputsButtonsFrame.ChooseInputsPB.Title = "Edit list";
TabContainer.IOTabPage.InputsFrame.InputsButtonsFrame.DefineInputsSettingsPB.Title= "Define input(s) settings ...";
TabContainer.IOTabPage.InputsFrame.InputsButtonsFrame.DefineInputsSettingsPB.Help= "Defines the settings of the selected input(s).";
TabContainer.IOTabPage.InputsFrame.InputsButtonsFrame.DefineInputsSettingsPB.ShortHelp= "Click here to define the settings of the selected input(s).";
TabContainer.IOTabPage.InputsFrame.InputsButtonsFrame.DefineInputsSettingsPB.LongHelp= "Defines the settings for the selected input(s).";


TabContainer.IOTabPage.OutputsFrame.Title = "Outputs Definition";
TabContainer.IOTabPage.OutputsFrame.OutputsMultiList.Help = "Shows the settings defined on the output parameters";
TabContainer.IOTabPage.OutputsFrame.OutputsMultiList.ShortHelp = "This lists shows the settings defined on the output parameters";
TabContainer.IOTabPage.OutputsFrame.OutputsMultiList.LongHelp = "Shows the settings defined on the output parameters";
TabContainer.IOTabPage.OutputsFrame.OutputsMultiList.ColumnTitle0 = "Output";
TabContainer.IOTabPage.OutputsFrame.OutputsMultiList.ColumnTitle1 = "Ref. Parameter";
TabContainer.IOTabPage.OutputsFrame.OutputsMultiList.ColumnTitle2 = "Type";
TabContainer.IOTabPage.OutputsFrame.OutputsMultiList.ColumnTitle3 = "Current Value";
TabContainer.IOTabPage.OutputsFrame.OutputsMultiList.ColumnTitle4 = "Inf. Bound";
TabContainer.IOTabPage.OutputsFrame.OutputsMultiList.ColumnTitle5 = "Sup. Bound";
TabContainer.IOTabPage.OutputsFrame.OutputsMultiList.ColumnTitle6 = "Target Value";
TabContainer.IOTabPage.OutputsFrame.OutputsMultiList.ColumnTitle7 = "Weight";
TabContainer.IOTabPage.OutputsFrame.OutputsButtonsFrame.ChooseOutputsPB.Title = "Edit list";
TabContainer.IOTabPage.OutputsFrame.OutputsButtonsFrame.DefineOutputsSettingsPB.Title= "Define output(s) settings ...";
TabContainer.IOTabPage.OutputsFrame.OutputsButtonsFrame.DefineOutputsSettingsPB.Help= "Defines the settings of the selected output(s)";
TabContainer.IOTabPage.OutputsFrame.OutputsButtonsFrame.DefineOutputsSettingsPB.ShortHelp= "Click here to define the settings of the selected output(s).";
TabContainer.IOTabPage.OutputsFrame.OutputsButtonsFrame.DefineOutputsSettingsPB.LongHelp= "Shows the settings defined for the selected output(s).";

SimultaneousSettingsDefinitionError = "The types of the selected parameters are different:\nTo define settings simultaneously, parameters must have the same type.";
SimultaneousSettingsDefinitionErrorHd = "Simultaneous settings definition impossible";

NoMethodToRun = "No method to launch. Create one using the provided command(s).";
RunErrorHd = "Run Error";

Yes = "YES";
No = "NO";


PEOLeanUpdateSlider.Title = "Update Mode";
PEOLeanUpdateSlider.ShortHelp = "Activates several types of update mechanisms.\nIn CATPart:\n 1- Global update (slow, update of the full CATPart). \n 2- Mixed Variational Update. \n 3- Local Update (fast, update of the optimization outputs only). \n\n In CATProduct and CATAnalysis: \n  - Global update.\n";
PEOLeanUpdateSlider.Help = "Activates several types of update mechanisms for memory and performance tunning.";
PEOLeanUpdateSlider.LongHelp = "PEO Lean Update options: Activates several types of update mechanisms.\n \nIn CATPart:\n 1- Global update (slow, update of the full CATPart). \n 2- Mixed Variational Update. \n 3- Local Update (fast, update of the optimization outputs only). \n\n In CATProduct and CATAnalysis: \n - Global update. \n";

PEOLeanUpdateLabel.Title = "Update Mode";
PEOLeanUpdateLabel.ShortHelp = "Activates several types of update mechanisms.\n\nIn CATPart:\n 1- Global update (slow). \n 2- Mixed Variational Update. \n 3- Local Update (fast). \n\n In CATProduct and CATAnalysis: \n  - Global update. \n";
PEOLeanUpdateLabel.Help = "Activates several types of update mechanisms for memory and performance tunning.";
PEOLeanUpdateLabel.LongHelp = "PEO Lean Update options: Activates several types of update mechanisms.\n \nIn CATPart:\n\t  1- Global update (slow, update of the full CATPart). \n 2- Mixed Variational Update. \n 3- Local Update (fast). \n\n In CATProduct and CATAnalysis: \n - Global update (update of the \n optimization outputs and of the CATProduct or the CATPart.)\n";






