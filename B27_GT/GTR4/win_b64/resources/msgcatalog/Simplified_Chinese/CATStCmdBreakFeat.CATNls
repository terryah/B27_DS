// COPYRIGHT DASSAULT SYSTEMES  1999
//=============================================================================
//
// CATStCmdBreakFeat:  	Resource file for NLS purpose related to
//                      Break surface/curve
//
//=============================================================================
//
// Implementation Notes:
//
//=============================================================================
// April'13   Creation                                   Gangaram Dumbre (WAP)
//=============================================================================

CATStCmdBreakFeat.State1.Message="选择切除元素";
CATStCmdBreakFeat.State2.Message="选择切除元素";
CATStCmdBreakFeat.State3.Message="操作状态";

CATStCmdBreakFeat.Title="中断";

CATStCmdBreakFeat.UndoTitle="断开特征";
CATStCmdBreakFeat.RedoTitle="断开特征";

BreakCurve.Message="中断曲线";
BreakSurface.Message="中断曲面";

NoResult.Message="未在该配置中找到任何结果。
请参阅中断操作的报告以了解更多信息。";
// A19 11:11:25
NoProjection.Message="创建中断失败。
请尝试使用“投影”选项创建中断。
有关更多信息，请参阅中断操作的报告。";

ProjectionFail.Message="由于输入元素的投影失败，创建中断失败。
尝试使用另一个投影选项。
有关更多信息，请参阅中断操作的报告。";

NoExtrapolation.Message="创建中断失败。
请尝试使用“外插延伸”选项创建中断。
有关更多信息，请参阅中断操作的报告。";

ExtrapolationFail.Message="由于输入元素的外插延伸失败，创建中断失败。
请尝试使用另一个“外插延伸”选项。
有关更多信息，请参阅中断操作的报告。";

DisconnectResultFail.Message="对这些输入创建中断失败。
请尝试更改要中断的输入。
有关更多信息，请参阅中断操作的报告。";

RelimitationFail.Message="由于输入元素的重新限定失败，创建中断失败。
请尝试使用另一个修剪选项创建中断。
有关更多信息，请参阅中断操作的报告。";
// A19 11:11:25

NoProjOrExtrapol.Message="创建中断失败。
请尝试使用“投影”或“外插延伸”选项创建中断。
有关更多信息，请参阅中断操作的报告。"; // A19 12:07:17

ProjOrExtrapolFail.Message="由于输入元素的“投影”或“外插延伸”失败，创建中断失败。
请尝试使用“投影”或“外插延伸”选项创建中断。
有关更多信息，请参阅中断操作的报告。"; // A19 12:07:17

FragmentError.Message="无法分割所有选中的元素。
请检查它们是多单元元素、修剪元素还是非 NUPBS 元素。";

BreakIntoeqNoError.Message="无法将所有选定的元素分成相等的部分。
请检查它们是否为多单元元素。";

BreakMultiDomainError.Message="快速创建等参数曲线不允许选择多域支持面。
请选择一个有效的单域支持面。";

BreakMaxDeviation.Message="最大偏差： ";
BreakEdgeDeviation.Message="边线偏差： ";

//Start AV7 11:06:14
BreakAutoJoin.Assemble="装配";
BreakAutoJoin.Propagate="拓展等参数曲线";
//End AV7 11:06:14

//Start H67 11:12:02

CATStCmdBreakFeat.State1SelectionAgent.UndoTitle="选择元素 ";
CATStCmdBreakFeat.State1SelectionAgent.RedoTitle="选择元素 ";

CATStCmdBreakFeat.State2SelectionAgent.UndoTitle="选择限制元素";
CATStCmdBreakFeat.State2SelectionAgent.RedoTitle="选择限制元素";

CATStCmdBreakFeat.ChangeBreakType.UndoTitle="更改中断类型";
CATStCmdBreakFeat.ChangeBreakType.RedoTitle="更改中断类型";

CATStCmdBreakFeat.ChangeBreakBothType.UndoTitle="中断两者 ";
CATStCmdBreakFeat.ChangeBreakBothType.RedoTitle="中断两者 ";

CATStCmdBreakFeat.ChangeDisassembleResultType.UndoTitle="拆解";
CATStCmdBreakFeat.ChangeDisassembleResultType.RedoTitle="拆解";

CATStCmdBreakFeat.ChangeFragmentType.UndoTitle="分段 ";
CATStCmdBreakFeat.ChangeFragmentType.RedoTitle="分段 ";

CATStCmdBreakFeat.ChangeInvertSelType.UndoTitle="反转选择";
CATStCmdBreakFeat.ChangeInvertSelType.RedoTitle="反转选择";

CATStCmdBreakFeat.ChangeBreakIntoEqNoType.UndoTitle="分割成等量 ";
CATStCmdBreakFeat.ChangeBreakIntoEqNoType.RedoTitle="分割成等量 ";

CATStCmdBreakFeat.ChangeBreakIntoEqNoValType.UndoTitle="修改分割成等量值";
CATStCmdBreakFeat.ChangeBreakIntoEqNoValType.RedoTitle="修改分割成等量值";

CATStCmdBreakFeat.ChangeDeviationModeType.UndoTitle="偏差模式 ";
CATStCmdBreakFeat.ChangeDeviationModeType.RedoTitle="偏差模式 ";

CATStCmdBreakFeat.ChangeProjection.UndoTitle="更改投影类型 ";
CATStCmdBreakFeat.ChangeProjection.RedoTitle="更改投影类型 ";

CATStCmdBreakFeat.ChangeExtrapolation.UndoTitle="更改外插延伸类型 ";
CATStCmdBreakFeat.ChangeExtrapolation.RedoTitle="更改外插延伸类型 ";

CATStCmdBreakFeat.ChangeRelimitation.UndoTitle="更改修剪类型 ";
CATStCmdBreakFeat.ChangeRelimitation.RedoTitle="更改修剪类型 ";

CATStCmdBreakFeat.PointAcquisition.UndoTitle="创建等参数曲线";
CATStCmdBreakFeat.PointAcquisition.RedoTitle="创建等参数曲线";
//End H67 11:12:02

//Start SDE2 14:09:29
CATStCmdBreakFeat.ArrowManipulatorChange.UndoTitle="已修改箭头操作器";
CATStCmdBreakFeat.ArrowManipulatorChange.RedoTitle="已修改箭头操作器";
//End SDE2 14:09:26





