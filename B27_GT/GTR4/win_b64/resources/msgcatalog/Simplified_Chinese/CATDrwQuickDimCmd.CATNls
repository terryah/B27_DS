//=====================================================================================
//                                     CNEXT - CXRn
//                          COPYRIGHT DASSAULT SYSTEMES 1998 
//-------------------------------------------------------------------------------------
// FILENAME    :    CATDrwQuickDimCmd
// LOCATION    :    DraftingIntCommands
// AUTHOR      :    jmt
// BUT         :    Commande de creation de cote
// DATE        :    ??.10.1998
//-------------------------------------------------------------------------------------
// DESCRIPTION :    
//-------------------------------------------------------------------------------------
// COMMENTS    :    
//-------------------------------------------------------------------------------------
// MODIFICATIONS     user  date        purpose
//    HISTORY        ----  ----        -------
//      01           fgx   04.10.2000  Rearchitecturation CXR6
//=====================================================================================

//******************************************************************************
// Command
//******************************************************************************
CATDrwQuickDimCmd.UndoTitle="创建尺寸";

//******************************************************************************
// Agents
//******************************************************************************
CATDrwQuickDimCmd._agentSet.UndoTitle = "选择";

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Interactive Dimension Cmd : SwitchAgent related to Orientation purpose ...
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

CATDrwQuickDimCmd.OrientBarRessource.Bar.Title="方向";

//******************************************************************************
// Reference Element Button - V5R3 Only
//******************************************************************************
CATDrwQuickDimCmd.OrientBarRessource.RefElem.Title = "参考元素";
CATDrwQuickDimCmd.OrientBarRessource.RefElem.ShortHelp="参考元素";
CATDrwQuickDimCmd.OrientBarRessource.RefElem.Help=
"尺寸线的方向取决于选定的元素";
CATDrwQuickDimCmd.OrientBarRessource.RefElem.LongHelp=
"参考元素
尺寸线的方向取决于选定的元素";

//******************************************************************************
// Reference View Button - V5R3 Only
//******************************************************************************
CATDrwQuickDimCmd.OrientBarRessource.RefVue.Title = "参考视图";
CATDrwQuickDimCmd.OrientBarRessource.RefVue.ShortHelp="参考视图";
CATDrwQuickDimCmd.OrientBarRessource.RefVue.Help=
"将尺寸线的方向链接到视图方向";
CATDrwQuickDimCmd.OrientBarRessource.RefVue.LongHelp=
"参考视图
将尺寸线的方向链接到视图方向";

//******************************************************************************
// Projected dimension Button
//******************************************************************************
CATDrwQuickDimCmd.OrientBarRessource._RefElem.Title = "投影的尺寸";
CATDrwQuickDimCmd.OrientBarRessource._RefElem.ShortHelp="投影的尺寸";
CATDrwQuickDimCmd.OrientBarRessource._RefElem.Help=
"尺寸线的方向取决于鼠标位置";
CATDrwQuickDimCmd.OrientBarRessource._RefElem.LongHelp=
"投影的尺寸
尺寸线的方向取决于鼠标位置。";

//******************************************************************************
// Force dimension on element Button
//******************************************************************************
CATDrwQuickDimCmd.OrientBarRessource._RefAuto.Title = "强制标注元素尺寸";
CATDrwQuickDimCmd.OrientBarRessource._RefAuto.ShortHelp="强制标注元素尺寸";
CATDrwQuickDimCmd.OrientBarRessource._RefAuto.Help=
"强制尺寸线与已标注尺寸的元素平行";
CATDrwQuickDimCmd.OrientBarRessource._RefAuto.LongHelp=
"强制标注元素尺寸
强制尺寸线与已标注尺寸的元素平行。";

//******************************************************************************
// Force horizontal dimension Button
//******************************************************************************
CATDrwQuickDimCmd.OrientBarRessource._RefHoriz.Title = "强制在视图中水平标注尺寸";
CATDrwQuickDimCmd.OrientBarRessource._RefHoriz.ShortHelp="强制尺寸线在视图中水平";
CATDrwQuickDimCmd.OrientBarRessource._RefHoriz.Help=
"强制尺寸线在视图中水平";
CATDrwQuickDimCmd.OrientBarRessource._RefHoriz.LongHelp=
"强制在视图中水平标注尺寸
强制尺寸线在视图中水平。";

//******************************************************************************
// Force vertical dimension Button
//******************************************************************************
CATDrwQuickDimCmd.OrientBarRessource._RefVert.Title = "强制在视图中垂直标注尺寸";
CATDrwQuickDimCmd.OrientBarRessource._RefVert.ShortHelp="强制在视图中垂直标注尺寸";
CATDrwQuickDimCmd.OrientBarRessource._RefVert.Help=
"强制尺寸线在视图中垂直";
CATDrwQuickDimCmd.OrientBarRessource._RefVert.LongHelp=
"强制在视图中垂直标注尺寸
强制尺寸线在视图中垂直。";

//******************************************************************************
// Force user defined dimension Button
//******************************************************************************
CATDrwQuickDimCmd.OrientBarRessource._RefUser.Title = "强制沿同一方向标注尺寸";
CATDrwQuickDimCmd.OrientBarRessource._RefUser.ShortHelp="强制沿同一方向标注尺寸";
CATDrwQuickDimCmd.OrientBarRessource._RefUser.Help=
"强制尺寸线沿同一方向";
CATDrwQuickDimCmd.OrientBarRessource._RefUser.LongHelp=
"强制沿同一方向标注尺寸
强制尺寸线沿同一方向。";

//******************************************************************************
// True length dimension Button
//******************************************************************************
CATDrwQuickDimCmd.OrientBarRessource.VraiGrandeur.Title = "实长尺寸";
CATDrwQuickDimCmd.OrientBarRessource.VraiGrandeur.ShortHelp="实长尺寸";
CATDrwQuickDimCmd.OrientBarRessource.VraiGrandeur.Help=
"以实长尺寸模式创建尺寸";
CATDrwQuickDimCmd.OrientBarRessource.VraiGrandeur.LongHelp=
"实长尺寸
以实长尺寸模式创建尺寸。";

//******************************************************************************
// Force dimension along direction Button
//******************************************************************************
CATDrwQuickDimCmd.AlongDirBarRessource.DirectionParallel.Title = "沿同一方向标注尺寸";
CATDrwQuickDimCmd.AlongDirBarRessource.DirectionParallel.ShortHelp="沿同一方向标注尺寸";
CATDrwQuickDimCmd.AlongDirBarRessource.DirectionParallel.Help=
"将尺寸线设置为沿同一方向";
CATDrwQuickDimCmd.AlongDirBarRessource.DirectionParallel.LongHelp=
"沿同一方向标注尺寸
将尺寸线设置为沿同一方向。";

//******************************************************************************
// Force dimension perpendicular to direction Button
//******************************************************************************
CATDrwQuickDimCmd.AlongDirBarRessource.DirectionPerpendicular.Title = "垂直于同一方向标注尺寸";
CATDrwQuickDimCmd.AlongDirBarRessource.DirectionPerpendicular.ShortHelp="垂直于同一方向标注尺寸";
CATDrwQuickDimCmd.AlongDirBarRessource.DirectionPerpendicular.Help=
"将尺寸线设置为垂直于同一方向";
CATDrwQuickDimCmd.AlongDirBarRessource.DirectionPerpendicular.LongHelp=
"垂直于同一方向标注尺寸
将尺寸线设置为垂直于同一方向。";

//******************************************************************************
// Force dimension along a fixed angle in view Button
//******************************************************************************
CATDrwQuickDimCmd.AlongDirBarRessource.DirectionAngle.Title = "在视图中沿固定角度标注尺寸";
CATDrwQuickDimCmd.AlongDirBarRessource.DirectionAngle.ShortHelp="在视图中沿固定角度标注尺寸";
CATDrwQuickDimCmd.AlongDirBarRessource.DirectionAngle.Help=
"将视图中的尺寸线设置为沿某个固定角度";
CATDrwQuickDimCmd.AlongDirBarRessource.DirectionAngle.LongHelp=
"在视图中沿固定角度标注尺寸
将视图中的尺寸线设置为沿某个固定角度。";

//******************************************************************************
// Intersection detection switch
//******************************************************************************
CATDrwQuickDimCmd.IntersectionAgentResource.Check.Title="检测相交点";
CATDrwQuickDimCmd.IntersectionAgentResource.Check.ShortHelp="检测相交点";
CATDrwQuickDimCmd.IntersectionAgentResource.Check.Help=
"标注尺寸时检测相交点";
CATDrwQuickDimCmd.IntersectionAgentResource.Check.LongHelp=
"检测相交点
标注尺寸时检测相交点。";

//******************************************************************************
// Angle
//******************************************************************************
CATDrwQuickDimCmd.Angle.Dimension.EnglobingFrame.IntermediateFrame.Label.Title = "角度： ";
CATDrwQuickDimCmd.Angle.Dimension.EnglobingFrame.IntermediateFrame.Label.ShortHelp = "输入与方向所成角度";

//******************************************************************************
// Driving Dim Value
//******************************************************************************
CATDrwQuickDimCmd.Value.Dimension.EnglobingFrame.IntermediateFrame.Label.Title = "值： ";
CATDrwQuickDimCmd.Value.Dimension.EnglobingFrame.IntermediateFrame.Label.ShortHelp = "输入驱动尺寸值";

