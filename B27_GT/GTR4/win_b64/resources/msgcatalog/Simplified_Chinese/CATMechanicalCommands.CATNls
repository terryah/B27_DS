ReplaceDlg_.Title="替换";

DeleteDlg_.Title="删除";
DeleteDlg_.MoreFrame_.AdvancedChildrenManagement_.Title="高级子级管理";
DeleteDlg_.MoreFrame_.AdvancedChildrenManagement_.LongHelp="指定要替换的对象，确保\n更新受影响的子对象";

ReplaceFrame_.Replace_.Title="替换： ";
ReplaceFrame_.With_.Title="具有： ";
ReplaceFrame_.ReplaceColumn_.Title="替换";
ReplaceFrame_.WithColumn_.Title="具有";
ReplaceFrame_.CheckDelete_.Title="删除替换的元素和互斥的父级";

ImpactFrame_.ImpactedElements_.Title="受删除影响的元素";
ImpactFrame_.ImpactedElements_.LongHelp="列出可以删除的受影响子级";
ImpactFrame_.ElementColumn_.Title="元素";
ImpactFrame_.StatusColumn_.Title="状态";
ImpactFrame_.DeleteUndelete_.Title="删除/取消删除";
ImpactFrame_.DeleteUndeleteAll_.Title="全部删除/取消全部删除";

DeleteFrame_.SepList_.Title="选择";
DeleteFrame_.SepList_.LongHelp="指定要删除的对象";
DeleteFrame_.SepParents_.Title="父级";
DeleteFrame_.DeleteParents_.Title="删除互斥父级";
DeleteFrame_.SepChildren_.Title="子级";
DeleteFrame_.DeleteChildren_.Title="删除所有子级";
DeleteFrame_.SepAggregated_.Title="聚集";
DeleteFrame_.DeleteAggregated_.Title=" 删除聚集元素";

DeleteConfirmDlg_.Answer_.Title="确定要删除这些对象吗？";

CATMechanicalCommands.OK="确定";
CATMechanicalCommands.KO="失败";
CATMechanicalCommands.DEL="删除";
CATMechanicalCommands.ALWAYSDEL="自动删除";
CATMechanicalCommands.Keep="保留";
CATMechanicalCommands.KeepNeedReplace=" 保留，需要替换";
CATMechanicalCommands.KeepPropagate="保留，拓展";
CATMechanicalCommands.KeepSolid="保留，实体";
CATMechanicalCommands.KeepUsedBySolid="保留，由实体使用";
CATMechanicalCommands.YES="是";
CATMechanicalCommands.NO="否";
CATMechanicalCommands.Error="错误";
CATMechanicalCommands.Warning="警告";
CATMechanicalCommands.More="更多 >>";
CATMechanicalCommands.Less="<< 更少";
CATMechanicalCommands.NoSelection="无选择";
CATMechanicalCommands.InvalidSelection="无效选择";
CATMechanicalCommands.UselessSelection="指向元素均不能参考选定元素。
它将生成父/子循环或违反排序规则。";

CATMechanicalCommands.CATMcoDeleteCmd.UndoTitle="删除";
CATMechanicalCommands.CATMcoDeleteCmd.ReplaceState.Message="选择对象";
CATMechanicalCommands.CATMcoDeleteCmd.InitialState.Message="选择所需选项";
CATMechanicalCommands.CATMcoDeleteCmd.ConfirmState.Message="单击“是”或“否”";
CATMechanicalCommands.CATMcoDeleteCmd.ConfirmState2.Message="选择要保留的元素";

CATMechanicalCommands.CATMcoReplaceCmd.UndoTitle="替换";
CATMechanicalCommands.CATMcoReplaceCmd.ReplaceState.Message="选择对象";

Replace.NotAccept=" 此元素不能替换选定的元素。 ";
Replace.NotPossible="不可替换";
Replace.WarningImpact=" : 此元素生成所使用的几何子元素。\n对于更新进程，您必须替换它们。";
Replace.WarningPointed="未指向选定的元素";
Replace.CancelReplace="取消替换";

Delete.NotDelete="请重新考虑您的操作：您不能删除此元素。";
Delete.NoUseless="未找到无用元素。";
Delete.Elements="元素";
Delete.Deleted="已删除";
Delete.Kept="已保留";
Delete.Index="索引";

DeleteConfirmDlg_.Select_.SelectList_.LongHelp="删除 = 将删除元素
保留 = 将保留元素
保留，拓展 = 该元素由要保留的元素使用，
因此不能将其删除。
保留，由实体使用 = 该元素由某个实体使用，
而在此命令中不能删除实体，
因此不能将该元素删除。";

DeleteConfirmDlg_.Select_.Message_.Title="在选定行使用上下文菜单来保留元素。";

Delete.ConfirmDeletion="确认删除";
Delete.DeleteUseless="删除无用元素";
Delete.ConfirmDeletionUndoLog="无法撤消删除此特征。\n是否要继续？";
ColonMsg="： ";

ReplaceViewerDlg_.Title="替换查看器";
ReplaceViewerDlg_.ReplaceViewerFrame_.LinkCheck_.Title="链接查看器";

Delete.Publication="已发布一些要删除的已删除元素： ";
Delete.Published=" 发布为 ";
Delete.Continue="要继续删除吗？";

Replace.UselessPointingDeleted="无用：已删除子级";
Replace.UselessCycle="无用：循环中的子级";
Replace.UselessNotPointed="无用：未指向";
Replace.NotReplaced=" 未替换";

Exit.Continue="想要继续吗？";

Replace.WarningParentChildren="不允许同时替换多个具有父/子关系的元素。\n\n请逐个替换元素。";
Replace.PointingElements=" 指向元素： ";
Replace.PointingElement=" 指向元素： ";
Replace.Continue="要继续替换吗？";

Replace.Exit="有些元素尚未替换，\n仍要退出替换命令吗？";

Replace.Publication="已发布一些要替换的元素： ";
Replace.WarningSolid=": 不允许替换实体元素。";
Replace.WarningPointedCurrent="位于工作对象后的任何元素\n均不使用选定的元素。\n请更改工作对象。";
Replace.WarningNonUpdated=": 不允许替换未更新的元素。";
Replace.BreakesRules=": 不符合排序规则。";

CATMechanicalCommands.DevaluatedLinks="若要符合排序规则，则需要重设。";
CATMechanicalCommands.NoLinkReplace="为了遵守排序规则且不出现更新循环错误，\n没有执行替换操作。";

CATMechanicalCommands.CATMmuKeepCmd.UndoTitle="除...外全部删除";
CATMechanicalCommands.CATMmuKeepCmd.Title="除...外全部删除";
CATMechanicalCommands.CATMmuKeepCmd.State1.Message="请选择要保留的特征。";
CATMechanicalCommands.CATMmuKeepCmd.State2.Message="按“确定”删除所有列出的特征。";

KeepPanel.Title="除...外全部删除";
KeepPanel.OKButton="确定";
KeepPanel.KeepList.Col1Title="#";
KeepPanel.KeepList.Col2Title="特征";
KeepPanel.KeepList.Col3Title="状态";
KeepPanel.KeepList.Col4Title="类型";

KeepPanel.Preset.KeepSolidButton=" 实体特征";
KeepPanel.Preset.KeepPublishedButton=" 已发布特征";

KeepPanel.KeepFrame.KeepMultiList.ShortHelp="使用选定直线的上下文菜单更改状态。";
KeepPanel.KeepFrame.KeepMultiList.LongHelp="选择直线并使用其上下文菜单更改特征状态。 
此操作会禁用以上检查按钮。";

KeepPanel.KeepFrame.KeepSolidButton.ShortHelp="若已检查，则默认保留所有实体特征";
KeepPanel.KeepFrame.KeepSolidButton.LongHelp="若已检查，则默认保留所有实体特征。";
KeepPanel.KeepFrame.KeepPublishedButton.LongHelp="若已检查，则默认保留所有已发布特征。";
KeepPanel.KeepFrame.KeepPublishedButton.ShortHelp="若已检查，则默认保留所有已发布特征";

KeepConfPanel.Title="确认删除";
KeepConfPanel.YES="确定";
KeepConfPanel.NO="取消";
KeepConfPanel.NothingToRemove="没有要删除的内容";
KeepConfPanel.Label="要删除的特征：";
KeepConfPanel.KeepConfMultiList.ShortHelp="确认后将删除所有特征";
KeepConfPanel.KeepConfMultiList.LongHelp="确认后将删除所有特征。";

KeepStatus.KeepPropagated="保留（已拓展）";
KeepStatus.Keep="保留";
KeepStatus.Erase="删除";
KeepStatus.Reset="重置";
KeepStatus.ResetScanableComp="重置所有未参考的部件";
KeepStatus.KeepScanableComp="保留所有部件";
KeepStatus.KeepScanablePropagatedComp="保留所有部件（已拓展）";

KeepStatus.ErasePartBodyComp="";

KeepStatus.NoSwitchStatus="无法切换。由保留特征参考。";

InternalCopyPanel.InternalCopyLabel="内部副本 ";
InternalCopyPanel.OldReferenceLabel="旧参考 ";
InternalCopyPanel.NewReferenceLabel="新参考 ";
InternalCopyPanel.ReplaceViewerBox=" 允许手动重插";

FeaturesToUpgrade="要升级的特征";
NoFeatureToUpgradeFound="找不到要升级的特征";
Refresh="刷新";















