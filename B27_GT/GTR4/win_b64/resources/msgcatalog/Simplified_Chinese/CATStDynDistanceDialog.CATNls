//=============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2002
//=============================================================================
//
// Distance Analysis Command :
//   Resource file for NLS purpose. 
//
//=============================================================================
//
// Implementation Notes :
//
//=============================================================================
// Oct. 02   Creation                                          Minh-Duc TRUONG
//=============================================================================

Title="距离";

FrmLeft.FrmSelection.Title="选择状态";
FrmLeft.FrmType.Title="测量方向"; // R12 Modification
FrmLeft.FrmProjection.Title="投影空间"; // R12 Modification

FrmLeft.FrmSelection.SourcesRadB.Title="第一组";
FrmLeft.FrmSelection.SourcesRadB.LongHelp="
允许选择
第一组元素。";

FrmLeft.FrmSelection.TargetsRadB.Title="第二组";
FrmLeft.FrmSelection.TargetsRadB.LongHelp="
允许选择
第二组元素。";

FrmLeft.FrmInversion.InvertAnalysisPush.Title="反转分析";
FrmLeft.FrmInversion.InvertAnalysisPush.ShortHelp="反转分析计算";
FrmLeft.FrmInversion.InvertAnalysisPush.LongHelp="反转分析计算";

FrmLeft.FrmSelection.RunningPtRadio.Title="可变点";
FrmLeft.FrmSelection.RunningPtRadio.ShortHelp="启用可变点";
FrmLeft.FrmSelection.RunningPtRadio.LongHelp="
允许计算鼠标下的点
和源元素之间
的距离。";

FrmLeft.FrmCenterBis.FrmDispIcons.DiagramPush.ShortHelp="2D 图表";
FrmLeft.FrmCenterBis.FrmDispIcons.DiagramPush.LongHelp="显示 2D 图表
（仅用于一维单一单元元素）。";

FrmLeft.FrmCenterBis.FrmDispIcons.FullRadio.ShortHelp="完整颜色范围";
FrmLeft.FrmCenterBis.FrmDispIcons.FullRadio.LongHelp="以完整颜色范围显示分析。";

FrmLeft.FrmCenterBis.FrmDispIcons.QuickRadio.ShortHelp="有限颜色范围";
FrmLeft.FrmCenterBis.FrmDispIcons.QuickRadio.LongHelp="以有限的颜色范围
显示分析，以便进行快速诊断。";

FrmLeft.FrmCenterBis.FrmDispIcons.QuickCheck.ShortHelp="有限颜色范围"; // Light Mode
FrmLeft.FrmCenterBis.FrmDispIcons.QuickCheck.LongHelp="以有限的颜色范围
显示分析，以便进行快速诊断。";

FrmLeft.FrmProjection.FrmProjMode.Radio3D.ShortHelp="无元素投影";
FrmLeft.FrmProjection.FrmProjMode.Radio3D.LongHelp="无元素投影";
FrmLeft.FrmProjection.FrmProjMode.RadioX.ShortHelp="X 方向投影";
FrmLeft.FrmProjection.FrmProjMode.RadioX.LongHelp="计算沿 X 方向的元素投影之间的距离。";
FrmLeft.FrmProjection.FrmProjMode.RadioY.ShortHelp="Y 方向投影";
FrmLeft.FrmProjection.FrmProjMode.RadioY.LongHelp="计算沿 Y 方向的元素投影之间的距离。";
FrmLeft.FrmProjection.FrmProjMode.RadioZ.ShortHelp="Z 方向投影";
FrmLeft.FrmProjection.FrmProjMode.RadioZ.LongHelp="计算沿 Z 方向的元素投影之间的距离。";
FrmLeft.FrmProjection.FrmProjMode.RadioCompass.ShortHelp="指南针方向投影";
FrmLeft.FrmProjection.FrmProjMode.RadioCompass.LongHelp="计算沿指南针方向的元素投影之间的距离。";
FrmLeft.FrmProjection.FrmProjMode.PlanarDistRadio.ShortHelp="平面距离";
FrmLeft.FrmProjection.FrmProjMode.PlanarDistRadio.LongHelp="计算一维元素与包含
此元素和其他元素的平面
的相交之间的距离";

FrmLeft.FrmType.FrmTypeMode.NormalRadio.ShortHelp="法线距离";
FrmLeft.FrmType.FrmTypeMode.NormalRadio.LongHelp="计算最小法线距离。";

FrmLeft.FrmType.FrmTypeMode.RadioXMeasureDir.ShortHelp="X 方向距离"; // r12
FrmLeft.FrmType.FrmTypeMode.RadioXMeasureDir.LongHelp="计算沿 X 方向的距离。";

FrmLeft.FrmType.FrmTypeMode.RadioYMeasureDir.ShortHelp="Y 方向距离"; // r12
FrmLeft.FrmType.FrmTypeMode.RadioYMeasureDir.LongHelp="计算沿 Y 方向的距离。";

FrmLeft.FrmType.FrmTypeMode.RadioZMeasureDir.ShortHelp="Z 方向距离"; //r12
FrmLeft.FrmType.FrmTypeMode.RadioZMeasureDir.LongHelp="计算沿 Z 方向的距离。";

FrmLeft.FrmType.FrmTypeMode.RadioCompassMeasureDir.ShortHelp="指南针方向距离"; // r12
FrmLeft.FrmType.FrmTypeMode.RadioCompassMeasureDir.LongHelp="计算沿指南针方向的距离。";

FrmLeft.FrmCenterBis.Title="显示选项";
FrmLeft.FrmCenterBis.Espace1.Title=" ";
FrmLeft.FrmCenterBis.Espace2.Title="   ";
FrmLeft.FrmCenterBis.Espace3.Title="   ";
FrmLeft.FrmCenterBis.Espace4.Title="   ";

FrmLeft.FrmCenterBis.MorePsh.Title="更多...";
FrmLeft.FrmCenterBis.LessPsh.Title="更少...";

FrmRight.FrmDisplay.PointsChk.Title="点";
FrmRight.FrmDisplay.PointsChk.LongHelp="显示带颜色的点。";
FrmRight.FrmDisplay.MinMaxChk.Title="最小值/最大值";
FrmRight.FrmDisplay.MinMaxChk.LongHelp="显示最小值和最大值的位置。";
FrmRight.FrmDisplay.ExactMaxChk.Title="精确最大值";
FrmRight.FrmDisplay.ExactMaxChk.LongHelp="显示精确最大值位置。";

FrmRight.FrmDisplay.ColorScaleChk.Title="色标";
FrmRight.FrmDisplay.ColorScaleChk.LongHelp="显示/隐藏色标。";

FrmRight.FrmDisplay.StatChk.Title="统计分布";
FrmRight.FrmDisplay.StatChk.LongHelp="显示/隐藏统计分布。";

FrmRight.FrmDisplay.CombChk.Title="尖峰";
FrmRight.FrmDisplay.CombChk.LongHelp="显示距离尖峰。";

FrmRight.FrmDisplay.FrmCombOptions.FrmComb.Title="尖峰";

FrmRight.FrmDisplay.FrmCombOptions.DecalLbl.Title="     ";

FrmRight.FrmDisplay.FrmCombOptions.FrmComb.EnvelopChk.Title="包络";
FrmRight.FrmDisplay.FrmCombOptions.FrmComb.EnvelopChk.LongHelp="显示与梳状尖峰的尖端接合的折线。";

FrmRight.FrmDisplay.FrmCombOptions.FrmComb.InvertChk.Title="已反转";
FrmRight.FrmDisplay.FrmCombOptions.FrmComb.InvertChk.LongHelp="反转尖峰。";

FrmRight.FrmDisplay.FrmCombOptions.FrmComb.AutoScaleChk.Title="自动缩放";
FrmRight.FrmDisplay.FrmCombOptions.FrmComb.AutoScaleChk.LongHelp="显示长度与缩放无关的梳状尖峰。";

FrmRight.FrmDisplay.Mapping.Title="结构";
FrmRight.FrmDisplay.Mapping.LongHelp="显示结构映射。";


FrmRight.FrmDisplay.LimitsPoints.Title="曲线限制";
FrmRight.FrmDisplay.LimitsPoints.LongHelp="重新限定离散曲线。";

FrmRight.FrmDisplay.MaxDist.Title="最大距离";
FrmRight.FrmDisplay.MaxDist.LongHelp="定义最大距离";

FrmLeft.FrmCenterBis.FrmDispIcons.MaxDist.Title="最大距离"; // Light Mode 
FrmLeft.FrmCenterBis.FrmDispIcons.MaxDist.LongHelp="定义最大距离";




FrmRight.FrmDiscretisation.Title="离散化";

FrmRight.FrmDiscretisation.AutoTrapChk.Title="自动封闭曲线";
FrmRight.FrmDiscretisation.AutoTrapChk.ShortHelp="自动消除目标点";
FrmRight.FrmDiscretisation.AutoTrapChk.LongHelp="
授权自动消除
每个源曲面的不相关的
目标点。";
