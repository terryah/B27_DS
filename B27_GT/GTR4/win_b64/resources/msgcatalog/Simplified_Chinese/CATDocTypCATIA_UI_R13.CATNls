//  Messages for CATIA V5 File DocTyp adhesion - V5R13 implementation
//   MODULE :  DocTypAdhesion.m
//   US Version
//=====================================================================
CATIADocTyp.DocTypNLSName="文件";
CATIADocTyp.Title="选择文件";
// User Interfaces resources ==========================================
////////////////////////////////////////////// frame Input
CATIADocTyp.Frame.Input.Title="输入选项";
CATIADocTyp.Frame.Input.Help="输入选项";

CATIADocTyp.Lbl.Select.Title="选择";
CATIADocTyp.Lbl.Select.Help="选择";

CATIADocTyp.Btn.SelectMember.Title=" 成员... ";
CATIADocTyp.Btn.SelectMember.Help="选择一个或多个目录成员";

CATIADocTyp.Btn.SelectAll.Title="   所有...   ";
CATIADocTyp.Btn.SelectAll.Help="选择所有目录成员";

CATIADocTyp.BrowseDirBox.Title="浏览目录框";

CATIADocTyp.Btn.SelectString.Title="  字符串...  ";
CATIADocTyp.Btn.SelectString.Help="选择包含指定字符串的成员";

//////////////////////////////////////// selection Version resources
CATIADocTyp.Chk.VersionSelection.Title="根据修改版本选择";
CATIADocTyp.Chk.VersionSelection.Help="根据文档的 CATIA 修改版本选择 CATIA V5 文档";

CATIADocTyp.Edt.Version1Selection.Help=" 输入开始文档版本";
CATIADocTyp.Edt.Version2Selection.Help=" 输入结束文档版本";

CATIADocTyp.Cmb.Since = "之后";
CATIADocTyp.Cmb.Before = "之前";
CATIADocTyp.Cmb.Between = "之间";

//////////////////////////////////////// selection date resources
CATIADocTyp.Chk.DateSelection.Title="根据修改日期选择";
CATIADocTyp.Chk.DateSelection.Help="根据文档的修改日期选择 CATIA 文档";

CATIADocTyp.Edt.DocumentDate1Selection.Help="输入选择的开始日期";
CATIADocTyp.Edt.DocumentDate2Selection.Help="输入选择的结束日期";

CATIADocTyp.NoStringDefined="未定义字符串";

CATIADocTyp.BtnMML.Title="选择被指向的文档";
CATIADocTyp.BtnMML.Help="选择被指向的文档";

CATIADocTyp.Chk.ExtendedSelection.Title="将选择扩展至子目录";
CATIADocTyp.Chk.ExtendedSelection.Help="将选择扩展至子目录";

////////////////////////////////////////////// frame Rename
CATIADocTyp.Frame.Rename.Title="重命名条件";
CATIADocTyp.Frame.Rename.Help="重命名条件";

CATIADocTyp.Chk.Append.Title="附加字符串"; 
CATIADocTyp.Chk.Append.Help="在文档名末尾添加字符串";
CATIADocTyp.Edt.Append.Title="附加字符串";
CATIADocTyp.Edt.Append.Help="定义要添加的字符串";

CATIADocTyp.Chk.Insert.Title="插入";
CATIADocTyp.Chk.Insert.Help="将字符串插入文档名";

CATIADocTyp.Chk.Sub.Title="替代";
CATIADocTyp.Chk.Sub.Help="用已定义的字符串替换字符";

CATIADocTyp.Edt.Insert.Help="定义要插入/替代的字符串";
CATIADocTyp.Lbl.FromCol.Title="开始列";
CATIADocTyp.Edt.FromCol.Help="定义开始列";

CATIADocTyp.RadioBtn.NoRename.Title="未重命名";
CATIADocTyp.RadioBtn.NoRename.Help="保持初始文档名";

CATIADocTyp.RadioBtn.Rename.Title="重命名";
CATIADocTyp.RadioBtn.Rename.Help="重命名文档";

CATIADocTyp.RadioBtn.RenameifExist.Title="若已存在，则重命名";
CATIADocTyp.RadioBtn.RenameifExist.Help="仅当文档已存在时才对其重命名";

////////////////////////////////////////////// frame Output
CATIADocTyp.Frame.Output.Title="输出选项";
CATIADocTyp.Frame.Output.Help="输出选项";


CATIADocTyp.Chk.ReplaceDoc.Title="替换目标目录中的现有文档";
CATIADocTyp.Chk.ReplaceDoc.Help="替换目标目录中的已存在文档";

CATIADocTyp.Lbl.TargetDirectory.Title="默认目标目录";
CATIADocTyp.Lbl.TargetDirectory.Help="当前默认目标目录";

CATIADocTyp.Btn.ResetTargetDir.Title="重置";
CATIADocTyp.Btn.ResetTargetDir.Help="恢复在“数据生命周期”窗口中定义的目标目录";

CATIADocTyp.Btn.ChangeTargetDir.Title="更改目标目录...";
CATIADocTyp.Btn.ChangeTargetDir.Help="定义新的目标目录";

CATIADocTyp.BrowseChangeTargetDirBox.Title="浏览“更改目标目录”框";

CATIADocTyp.Chk.KeepFileTree.Title="保持目录结构";
CATIADocTyp.Chk.KeepFileTree.Help="将初始目录结构保持在目标目录下";

CATIADocTyp.Chk.KeepAtSameLocation.Title="保持在相同位置";
CATIADocTyp.Chk.KeepAtSameLocation.Help="将输入目录设为目标目录";

CATIADocTyp.Chk.ChkSaveNonModifiedDoc.Title="保存未修改的文档";
CATIADocTyp.Chk.ChkSaveNonModifiedDoc.Help="当目标目录不等同于输入目录时，将未修改的文档保存在目标目录中";

CATIADocTyp.Chk.ChkRenameNonModifiedDoc.Title="保存未修改的文档时应用重命名";
CATIADocTyp.Chk.ChkRenameNonModifiedDoc.Help="根据重命名规格对未修改的文档进行重命名";

CATIADocTyp.List.MappingTable.Title="映射表";
CATIADocTyp.List.MappingTable.Help="输入/目标目录映射";

CATIADocTyp.List.MappingTable.col1="输入目录";
CATIADocTyp.List.MappingTable.col1.H="来自输入目录的文档";
CATIADocTyp.List.MappingTable.col2="目标目录";
CATIADocTyp.List.MappingTable.col2.H="将保存在输出目录中";
CATIADocTyp.List.MappingTable.col3="初始输出目录";

CATIADocTyp.Btn.ResetMappingDir.Title="重置";
CATIADocTyp.Btn.ResetMappingDir.Help="恢复选定的第一个目标目录";

CATIADocTyp.Btn.DefaultMappingDir.Title="默认";
CATIADocTyp.Btn.DefaultMappingDir.Help="用默认目标目录替换选定目录";

CATIADocTyp.Btn.ChangeMappingDir.Title="更改...";
CATIADocTyp.Btn.ChangeMappingDir.Help="更改选定的目标目录";

CATIADocTyp.BrowseChangeMappingDirBox.Title="浏览“更改映射目录”框";

/////////////////////////////////// frame selection management 

CATIADocTyp.Chk.SaveBackUp.Title="保存文档的先前版本";
CATIADocTyp.Chk.SaveBackUp.Help="保存文档的先前版本";

CATIADocTyp.Edt.TargetDirectory.Help="当前目标目录";

/////////////////////////////////// Warning Selection criteria
CATIADocTyp.Warning.StartDateFormat = "输入的开始日期不正确，请使用 YYYY.MM.DD 格式";
CATIADocTyp.Warning.EndDateFormat = "输入的结束日期不正确，请使用 YYYY.MM.DD 格式";
CATIADocTyp.Warning.InvalidStartDate="提供的开始日期无效，请输入具有有效月/日的日期。";
CATIADocTyp.Warning.InvalidEndDate="提供的结束日期无效，请输入具有有效月/日的日期。";
//////////////////////////////////
CATIADocTyp.Warning.VersionBetweenStart="输入的开始版本必须介于 CATIA V5R10 与 /p1 之间。";
CATIADocTyp.Warning.VersionBetweenEnd="输入的最终版本必须介于 CATIA V5R10 与 /p1 之间。";
CATIADocTyp.Warning.IncorrectRelease="最终版本的发行版号低于开始版本的发行版号，请确保最终版本高于开始版本";
CATIADocTyp.Warning.IncorrectServicePack="最终版本的 Service Pack 号低于开始版本的 Service Pack 号，请确保最终版本高于开始版本";
CATIADocTyp.Warning.VersionFormatStart="输入的开始版本无效，请使用 CATIA V5 Rx 或 CATIA V5 Rx SPy 格式。";
CATIADocTyp.Warning.VersionFormatEnd ="输入的最终版本无效，请使用 CATIA V5 Rx 或 CATIA V5 Rx SPy 格式。";
CATIADocTyp.Warning.VersionFormat="输入的版本无效，请使用 CATIA V5 Rx 或 CATIA V5 Rx SPy 格式。";

//////////////////////////////////

CATIADocTyp.Warning.EmptyDirectorySub = "提供的目录及其子目录不包含任何 CATIA 文档，请输入包含至少一个 CATIA 文档的目录。";
CATIADocTyp.Warning.EmptyDirectory = "提供的目录不包含任何 CATIA 文档，请输入包含至少一个 CATIA 文档的目录。";

CATIADocTyp.Warning.StartDateBeforeFinal="开始日期应该在最终日期之前，请查看输入的日期。";
CATIADocTyp.Warning.StartDateSameFinal="开始日期和最终日期相同，请查看输入的日期。";
CATIADocTyp.Warning.StartDateBefore1970="开始日期不应该早于 1970 且迟于当前年份，请查看输入的日期。";
CATIADocTyp.Warning.StartDateBeforeCurrent="开始日期应该在当前日期之前，请查看输入的日期。";
CATIADocTyp.Warning.EndDateBefore1970="结束日期不应该早于 1970 且迟于当前年份，请查看输入的日期。";
CATIADocTyp.Warning.EndDateBeforeCurrent="结束日期应该在当前日期之前，请查看输入的日期。";
CATIADocTyp.Warning.DateDefault="输入的日期不正确，请使用 YYYY.MM.DD 格式";

CATIADocTyp.Warning.NoAppendString="无已定义的附加字符串。";
CATIADocTyp.Warning.NoInsertString="无已定义的插入/替代字符串。";
CATIADocTyp.Warning.EntryFromColumn="“自”列框中的项应为 [1-80] 之间的某个数字。";
CATIADocTyp.Warning.NoOptionSelected="必须选中附加/插入/替代字符串中的至少一个选项。";
CATIADocTyp.Warning.RenameFailedReason="用于插入/替代重命名条件的“自列条目”大于文档名长度。\n\n";
CATIADocTyp.Warning.RenameFailed="输出文档重命名失败...\n";

CATIADocTyp.Warning.Help.col1="以下文档路径不存在\n";
CATIADocTyp.Warning.Help.col2="请安装文档";
CATIADocTyp.Warning.Title="警告";
CATIADocTyp.Warning.NoBrowser="无浏览器";

