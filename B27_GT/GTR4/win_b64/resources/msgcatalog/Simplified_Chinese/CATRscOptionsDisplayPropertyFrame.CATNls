// COPYRIGHT DASSAULT SYSTEMES 1998
//===========================================================================
//
// CATRscOptionsDisplayPropertyFrame
//
//===========================================================================


//--------------------------------------
// Light options
//--------------------------------------
lightFrame.HeaderFrame.Global.Title = "活动光源";

lightFrame.IconAndOptionsFrame.OptionsFrame.activeLightDisplayRadio1.Title = "线框显示";
lightFrame.IconAndOptionsFrame.OptionsFrame.activeLightDisplayRadio1.Help = "打开活动光源的线框显示";
lightFrame.IconAndOptionsFrame.OptionsFrame.activeLightDisplayRadio1.ShortHelp = "线框显示";
lightFrame.IconAndOptionsFrame.OptionsFrame.activeLightDisplayRadio1.LongHelp =
"线框展示用于显示活动光源。";

lightFrame.IconAndOptionsFrame.OptionsFrame.activeLightDisplayRadio2.Title = "着色显示";
lightFrame.IconAndOptionsFrame.OptionsFrame.activeLightDisplayRadio2.Help = "打开活动光源的着色显示";
lightFrame.IconAndOptionsFrame.OptionsFrame.activeLightDisplayRadio2.ShortHelp = "着色显示";
lightFrame.IconAndOptionsFrame.OptionsFrame.activeLightDisplayRadio2.LongHelp =
"着色展示用于显示活动光源。";

lightFrame2.HeaderFrame.Global.Title = "非活动光源";

lightFrame2.IconAndOptionsFrame.OptionsFrame.inactiveLightDisplayRadio1.Title = "不显示";
lightFrame2.IconAndOptionsFrame.OptionsFrame.inactiveLightDisplayRadio1.Help = "关闭非活动光源显示";
lightFrame2.IconAndOptionsFrame.OptionsFrame.inactiveLightDisplayRadio1.ShortHelp = "关闭显示";
lightFrame2.IconAndOptionsFrame.OptionsFrame.inactiveLightDisplayRadio1.LongHelp =
"不显示非活动光源。";

lightFrame2.IconAndOptionsFrame.OptionsFrame.inactiveLightDisplayRadio2.Title = "全部显示";
lightFrame2.IconAndOptionsFrame.OptionsFrame.inactiveLightDisplayRadio2.Help = "打开非活动光源显示";
lightFrame2.IconAndOptionsFrame.OptionsFrame.inactiveLightDisplayRadio2.ShortHelp = "打开显示";
lightFrame2.IconAndOptionsFrame.OptionsFrame.inactiveLightDisplayRadio2.LongHelp =
"显示非活动光源。";

//--------------------------------------
// Environment options
//--------------------------------------
environmentFrame.HeaderFrame.Global.Title = "非活动环境";

environmentFrame.IconAndOptionsFrame.OptionsFrame.environmentDisplayRadio1.Title = "不显示";
environmentFrame.IconAndOptionsFrame.OptionsFrame.environmentDisplayRadio1.Help = "关闭非活动环境显示";
environmentFrame.IconAndOptionsFrame.OptionsFrame.environmentDisplayRadio1.ShortHelp = "关闭显示";
environmentFrame.IconAndOptionsFrame.OptionsFrame.environmentDisplayRadio1.LongHelp =
"不显示非活动环境。";

environmentFrame.IconAndOptionsFrame.OptionsFrame.environmentDisplayRadio2.Title = "简化显示";
environmentFrame.IconAndOptionsFrame.OptionsFrame.environmentDisplayRadio2.Help = "简化显示非活动环境";
environmentFrame.IconAndOptionsFrame.OptionsFrame.environmentDisplayRadio2.ShortHelp = "简化显示";
environmentFrame.IconAndOptionsFrame.OptionsFrame.environmentDisplayRadio2.LongHelp =
"以简化方式显示非活动环境。";

environmentFrame.IconAndOptionsFrame.OptionsFrame.environmentDisplayRadio3.Title = "全部显示";
environmentFrame.IconAndOptionsFrame.OptionsFrame.environmentDisplayRadio3.Help = "打开非活动环境显示";
environmentFrame.IconAndOptionsFrame.OptionsFrame.environmentDisplayRadio3.ShortHelp = "打开显示";
environmentFrame.IconAndOptionsFrame.OptionsFrame.environmentDisplayRadio3.LongHelp =
"显示非活动环境。";

//--------------------------------------
// Light manip options
//--------------------------------------
manipFrame.HeaderFrame.Global.Title = "光源控制器";

manipFrame.IconAndOptionsFrame.OptionsFrame.manipCheck.Title = "显示法线";
manipFrame.IconAndOptionsFrame.OptionsFrame.manipCheck.Help = "显示用于定位光源的曲面法线";
manipFrame.IconAndOptionsFrame.OptionsFrame.manipCheck.ShortHelp = "显示曲面法线";
manipFrame.IconAndOptionsFrame.OptionsFrame.manipCheck.LongHelp =
"显示用于定位光源的曲面法线。";

//--------------------------------------
// Environment reflections options
//--------------------------------------
reflectionFrame.HeaderFrame.Global.Title = "环境反射";

reflectionFrame.IconAndOptionsFrame.OptionsFrame.reflectionModeCheck.Title = "启用";
reflectionFrame.IconAndOptionsFrame.OptionsFrame.reflectionModeCheck.Help = "启用环境反射";
reflectionFrame.IconAndOptionsFrame.OptionsFrame.reflectionModeCheck.ShortHelp = "启用环境反射";
reflectionFrame.IconAndOptionsFrame.OptionsFrame.reflectionModeCheck.LongHelp =
"启用反射对象上的
活动环境反射。";

Lowest = "最低（128 x 128 像素）";
Low = "低（256 x 256 像素）";
Medium = "中（512 x 512 像素）";
High = "高（1024 x 1024 像素）";
Highest = "最高（2048 x 2048 像素）";

reflectionFrame.IconAndOptionsFrame.OptionsFrame.reflectionStaticSizeLabel.Title = "质量： ";
reflectionFrame.IconAndOptionsFrame.OptionsFrame.reflectionStaticSizeLabel.Help = "反射质量";
reflectionFrame.IconAndOptionsFrame.OptionsFrame.reflectionStaticSizeLabel.ShortHelp = "反射质量";
reflectionFrame.IconAndOptionsFrame.OptionsFrame.reflectionStaticSizeLabel.LongHelp =
"视点未移动时的反射质量。";

reflectionFrame.IconAndOptionsFrame.OptionsFrame.reflectionStaticSizeCombo.Help = "反射质量";
reflectionFrame.IconAndOptionsFrame.OptionsFrame.reflectionStaticSizeCombo.ShortHelp = "反射质量";
reflectionFrame.IconAndOptionsFrame.OptionsFrame.reflectionStaticSizeCombo.LongHelp =
"视点未移动时的反射质量。";

reflectionFrame.IconAndOptionsFrame.OptionsFrame.reflectionDynamicModeCheck.Title = "更改视点时更新反射";
reflectionFrame.IconAndOptionsFrame.OptionsFrame.reflectionDynamicModeCheck.Help = "启用反射动态更新";
reflectionFrame.IconAndOptionsFrame.OptionsFrame.reflectionDynamicModeCheck.ShortHelp = "启用反射动态更新";
reflectionFrame.IconAndOptionsFrame.OptionsFrame.reflectionDynamicModeCheck.LongHelp =
"更改视点时，
启用反射更新。";

reflectionFrame.IconAndOptionsFrame.OptionsFrame.reflectionDynamicSizeLabel.Title = "移动时的质量： ";
reflectionFrame.IconAndOptionsFrame.OptionsFrame.reflectionDynamicSizeLabel.Help = "视点移动时的反射质量";
reflectionFrame.IconAndOptionsFrame.OptionsFrame.reflectionDynamicSizeLabel.ShortHelp = "移动时的反射质量";
reflectionFrame.IconAndOptionsFrame.OptionsFrame.reflectionDynamicSizeLabel.LongHelp =
"视点移动时的反射质量。";

reflectionFrame.IconAndOptionsFrame.OptionsFrame.reflectionDynamicSizeCombo.Help = "视点移动时的反射质量";
reflectionFrame.IconAndOptionsFrame.OptionsFrame.reflectionDynamicSizeCombo.ShortHelp = "移动时的反射质量";
reflectionFrame.IconAndOptionsFrame.OptionsFrame.reflectionDynamicSizeCombo.LongHelp =
"视点移动时的反射质量。";

reflectionFrame.IconAndOptionsFrame.OptionsFrame.reflectionAngleLabel.Title = "更新阈值： ";
reflectionFrame.IconAndOptionsFrame.OptionsFrame.reflectionAngleLabel.Help = "反射更新阈值";
reflectionFrame.IconAndOptionsFrame.OptionsFrame.reflectionAngleLabel.ShortHelp = "反射更新阈值";
reflectionFrame.IconAndOptionsFrame.OptionsFrame.reflectionAngleLabel.LongHelp =
"视点旋转值大于阈值时，
更新反射。";

reflectionFrame.IconAndOptionsFrame.OptionsFrame.reflectionAngleSpinner.Help = "反射更新阈值";
reflectionFrame.IconAndOptionsFrame.OptionsFrame.reflectionAngleSpinner.ShortHelp = "反射更新阈值";
reflectionFrame.IconAndOptionsFrame.OptionsFrame.reflectionAngleSpinner.LongHelp =
"视点旋转值大于阈值时，
更新反射。";

reflectionFrame.IconAndOptionsFrame.OptionsFrame.reflectionPassesLabel.Title = "计算步骤： ";
reflectionFrame.IconAndOptionsFrame.OptionsFrame.reflectionPassesLabel.Help = "反射计算步骤";
reflectionFrame.IconAndOptionsFrame.OptionsFrame.reflectionPassesLabel.ShortHelp = "反射计算步骤";
reflectionFrame.IconAndOptionsFrame.OptionsFrame.reflectionPassesLabel.LongHelp =
"指示反射计算应分为
多少步。";

reflectionFrame.IconAndOptionsFrame.OptionsFrame.reflectionPassesSpinner.Help = "反射计算步骤";
reflectionFrame.IconAndOptionsFrame.OptionsFrame.reflectionPassesSpinner.ShortHelp = "反射计算步骤";
reflectionFrame.IconAndOptionsFrame.OptionsFrame.reflectionPassesSpinner.LongHelp =
"指示反射计算应分为
多少步。";


