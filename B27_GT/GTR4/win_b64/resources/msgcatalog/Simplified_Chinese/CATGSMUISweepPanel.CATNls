//--------------------------------------------
// Resource file for CATGSMUISweepPanel class
// En_US
//--------------------------------------------

// General texts
//---------------------------------------------------------------
TitleSURFACE="扫掠曲面定义";
TitleVOLUME="扫掠包络体定义";

SweepTypeUnspec="显式";
SweepTypeSegment="直线";
SweepTypeCircle="圆";
SweepTypeConic="二次曲线";

FraMenuBut.LabelList.Title="轮廓类型： ";
FraMenuBut.LabelList.LongHelp="列出可以扫掠的可用轮廓类型。";
FraMenuBut.LabelMini.Title="显式";
FraMenuBut.FraButtons.MenuBExpl.ShortHelp="显式";
FraMenuBut.FraButtons.MenuBExpl.LongHelp="使用用户定义的显式轮廓来创建扫掠曲面。";
FraMenuBut.FraButtons.MenuBCirc.ShortHelp="圆";
FraMenuBut.FraButtons.MenuBCirc.LongHelp="使用隐式圆弧轮廓来创建扫掠曲面。";
FraMenuBut.FraButtons.MenuBLine.ShortHelp="直线";
FraMenuBut.FraButtons.MenuBLine.LongHelp="使用隐式线性轮廓来创建扫掠曲面。";
FraMenuBut.FraButtons.MenuBConi.ShortHelp="二次曲线";
FraMenuBut.FraButtons.MenuBConi.LongHelp="使用隐式二次曲线轮廓来创建扫掠曲面。";

FraMenuButVol.LabelList.Title="轮廓类型： ";
FraMenuButVol.LabelList.LongHelp="列出可以扫掠的可用轮廓类型。";
FraMenuButVol.LabelMini.Title="显式";
FraMenuButVol.FraButtons.MenuBExpl.ShortHelp="显式";
FraMenuButVol.FraButtons.MenuBExpl.LongHelp="使用用户定义的显式轮廓来创建扫掠包络体。";
FraMenuButVol.FraButtons.MenuBCirc.ShortHelp="圆";
FraMenuButVol.FraButtons.MenuBCirc.LongHelp="使用隐式圆弧轮廓来创建扫掠包络体。";
FraMenuButVol.FraButtons.MenuBLine.ShortHelp="直线";
FraMenuButVol.FraButtons.MenuBLine.LongHelp="按拔模方向创建扫掠包络体";

FraSweep.Illustration.LongHelp="举例说明当前的扫掠输入。";

ChaineDefaut="默认";
ChaineProjection="投影";
ChaineMeanPlane="默认（平均平面）";

// Sweep unspec
//---------------------------------------------------------------
FraSweep.FraMandatory.LabProfile.Title="轮廓： ";
FraSweep.FraMandatory.LabProfile.LongHelp="指定要扫掠的轮廓。";
FraSweep.FraMandatory.Profile.LongHelp="指定要扫掠的轮廓。";
FraSweep.FraMandatory.LabGuideCurve.Title="引导曲线： ";
FraSweep.FraMandatory.LabGuideCurve.LongHelp="指定第一引导曲线。";
FraSweep.FraMandatory.GuideCurve.LongHelp="指定第一引导曲线。";

FraSweep.FraOptional.ListeOnglet.Onglet1.Title="参考";
FraSweep.FraOptional.ListeOnglet.Onglet1.LongHelp="允许指定参考曲面和角度， 
用于在扫掠过程中控制轮廓位置。";
FraSweep.FraOptional.ListeOnglet.Onglet1.SubFrameElt.LabReference.Title="曲面： ";
FraSweep.FraOptional.ListeOnglet.Onglet1.SubFrameElt.LabReference.LongHelp="指定参考曲面，
用于在扫掠过程中控制轮廓位置。
如果未指定曲面，则默认曲面为
通过脊线的平均平面。";
FraSweep.FraOptional.ListeOnglet.Onglet1.SubFrameElt.Reference.LongHelp="指定参考曲面，
用于在扫掠过程中控制轮廓位置。
如果未指定曲面，则默认曲面为
通过脊线的平均平面。";
FraSweep.FraOptional.ListeOnglet.Onglet1.SubFrameLit.LabRefAngle.Title="角度： ";
FraSweep.FraOptional.ListeOnglet.Onglet1.SubFrameLit.LabRefAngle.LongHelp="指定角度，
用于在扫掠过程中控制轮廓位置。";
FraSweep.FraOptional.ListeOnglet.Onglet1.SubFrameLit.FraRefAngle.EnglobingFrame.IntermediateFrame.Spinner.Title="角度";
FraSweep.FraOptional.ListeOnglet.Onglet1.SubFrameLit.LawButton.Title="法则曲线...";

FraSweep.FraOptional.ListeOnglet.Onglet2.Title="第二引导线";
FraSweep.FraOptional.ListeOnglet.Onglet2.LongHelp="允许指定第二引导曲线。";
FraSweep.FraOptional.ListeOnglet.Onglet2.LabGuideCurve2.Title="第二引导曲线： ";
FraSweep.FraOptional.ListeOnglet.Onglet2.LabGuideCurve2.LongHelp="指定第二引导曲线。";
FraSweep.FraOptional.ListeOnglet.Onglet2.GuideCurve2.LongHelp="指定第二引导曲线。";
FraSweep.FraOptional.ListeOnglet.Onglet2.LabelDirComp.Title="定位类型： ";
FraSweep.FraOptional.ListeOnglet.Onglet2.LabelDirComp.LongHelp="对于两导线显式扫掠曲面，允许指定
不同类型的定位选项。";
FraSweep.FraOptional.ListeOnglet.Onglet2.ComboDirComp.LongHelp="对于两导线显式扫掠曲面，列出
可用的定位选项类型。";
SweepExpBirailPoints="两个点";
SweepExpBirailPtDir="点和方向";
FraSweep.FraOptional.ListeOnglet.Onglet2.LabFittingPoint1.Title="引导线 1 定位点： ";
FraSweep.FraOptional.ListeOnglet.Onglet2.LabFittingPoint1.LongHelp="指定轮廓上的第一引导线定位点。";
FraSweep.FraOptional.ListeOnglet.Onglet2.FittingPoint1.LongHelp="指定轮廓上的第一引导线定位点。";
FraSweep.FraOptional.ListeOnglet.Onglet2.LabFittingPoint2.Title="引导线 2 定位点： ";
FraSweep.FraOptional.ListeOnglet.Onglet2.LabFittingPoint2.LongHelp="指定轮廓上的第二引导线定位点。";
FraSweep.FraOptional.ListeOnglet.Onglet2.FittingPoint2.LongHelp="指定轮廓上的第二引导线定位点。";
FraSweep.FraOptional.ListeOnglet.Onglet2.LabXAxisDirection.Title="定位方向： ";
FraSweep.FraOptional.ListeOnglet.Onglet2.LabXAxisDirection.LongHelp="指定轮廓上的定位方向。";
FraSweep.FraOptional.ListeOnglet.Onglet2.FraDirection.LongHelp="指定轮廓上的定位方向。";

FraSweep.FraOptional.FrameSpine.LabSpine.Title="脊线： ";
FraSweep.FraOptional.FrameSpine.LabSpine.LongHelp="指定脊线（如果您希望使用
与第一引导曲线不同的脊线）。";
FraSweep.FraOptional.FrameSpine.Spine.LongHelp="指定脊线（如果您希望使用
与第一引导曲线不同的脊线）。";

FraSweep.FraOptional.FrameSpine.LabRelimiter1.Title="  边界 1： ";
FraSweep.FraOptional.FrameSpine.LabRelimiter1.LongHelp="指定用于重新限定脊线的第一
元素（点或平面），以便进行扫掠。";
FraSweep.FraOptional.FrameSpine.Relimiter1.LongHelp="指定用于重新限定脊线的第一
元素（点或平面），以便进行扫掠。";

FraSweep.FraOptional.FrameSpine.LabRelimiter2.Title="  边界 2： ";
FraSweep.FraOptional.FrameSpine.LabRelimiter2.LongHelp="指定用于重新限定脊线的第二
元素（点或平面），以便进行扫掠。";
FraSweep.FraOptional.FrameSpine.Relimiter2.LongHelp="指定用于重新限定脊线的第二
元素（点或平面），以便进行扫掠。";

FraSweep.FraOptional.FrameTwist.SepStart.TitleLabel.Title="自交区域管理 ";
FraSweep.FraOptional.FrameTwist.TwistBut.Title="移除预览中的刀具";
FraSweep.FraOptional.FrameTwist.TwistBut.LongHelp="允许自动移除由自交区域管理添加的刀具。";
FraSweep.FraOptional.FrameTwist.SetbackLabel.Title="缩进 ";
FraSweep.FraOptional.FrameTwist.SetbackSlider.ShortHelp="0 - 20%";
FraSweep.FraOptional.FrameTwist.SetbackSlider.LongHelp="以导引线长度的百分比来设置缩进值（从 0 到 20%）。";
FraSweep.FraOptional.FrameTwist.PercentLabel.Title=" %";
FraSweep.FraOptional.FrameTwist.FillButton.Title="填充自交区域";
FraSweep.FraOptional.FrameTwist.FillButton.LongHelp="允许自动填充自交区域。";
FraSweep.FraOptional.FrameTwist.C0VerticesModeButton.Title="将 C0 顶点作为自交区域计算";
FraSweep.FraOptional.FrameTwist.C0VerticesModeButton.LongHelp="允许管理导引线上的 C0 顶点作为自交区域。";
FraSweep.FraOptional.FrameTwist.ConnectionStrategyLabel.Title="连接方式： ";
FraSweep.FraOptional.FrameTwist.ConnectionStrategyCombo.LongHelp="可以选择已填充区域的连接方式（自动、标准连接或是类似导引线）。";
ConnectionStrategyAutomatic="自动";
ConnectionStrategyStandard="标准";
ConnectionStrategySimilar="类似引导线";
FraSweep.FraOptional.FrameTwist.AddCutterButton.Title="添加刀具";
FraSweep.FraOptional.FrameTwist.AddCutterButton.LongHelp="允许添加刀具以移除或填充区域。";

// GSD_PULLINGDIREXPLICIT
//---------------------------------------------------------------
FraSweep.FraMandatory.FraComboExpl.LabComboExpl.Title="子类型： ";
FraSweep.FraMandatory.FraComboExpl.LabComboExpl.LongHelp="列出使用显式轮廓的扫掠曲面子类型。";
FraSweep.FraMandatory.FraComboExpl.ComboExpl.LongHelp="列出使用显式轮廓的扫掠曲面子类型。";

FraSweep.FraMandatory.FraComboExplVol.LabComboExplVol.Title="子类型： ";
FraSweep.FraMandatory.FraComboExplVol.LabComboExplVol.LongHelp="列出使用显式轮廓的扫掠包络体子类型。";
FraSweep.FraMandatory.FraComboExplVol.ComboExplVol.LongHelp="列出使用显式轮廓的扫掠包络体子类型。";

SweepExplicit1="使用参考曲面";
SweepExplicit2="使用两条引导曲线 ";
SweepExplicit3="使用拔模方向";

FraSweep.FraMandatory.SubFrameElt.LabProfile.Title="轮廓： ";
FraSweep.FraMandatory.SubFrameElt.LabProfile.LongHelp="指定要扫掠的轮廓。";
FraSweep.FraMandatory.SubFrameElt.Profile.LongHelp="指定要扫掠的轮廓。";

FraSweep.FraMandatory.SubFrameElt.LabGuideCurve.Title="引导曲线： ";
FraSweep.FraMandatory.SubFrameElt.LabGuideCurve.LongHelp="指定引导曲线。";
FraSweep.FraMandatory.SubFrameElt.GuideCurve.LongHelp="指定引导曲线。";
FraSweep.FraMandatory.SubFrameElt.LabGuideCurve1.Title="引导曲线 1： ";
FraSweep.FraMandatory.SubFrameElt.LabGuideCurve1.LongHelp="指定第一引导曲线。";
FraSweep.FraMandatory.SubFrameElt.GuideCurve1.LongHelp="指定第一引导曲线。";

FraSweep.FraMandatory.SubFrameElt.LabReference.Title="曲面： ";
FraSweep.FraMandatory.SubFrameElt.LabReference.LongHelp="指定参考曲面，
用于在扫掠过程中控制轮廓位置。
如果未指定曲面，则默认曲面为
通过脊线的平均平面。";
FraSweep.FraMandatory.SubFrameElt.Reference.LongHelp="指定参考曲面，
用于在扫掠过程中控制轮廓位置。
如果未指定曲面，则默认曲面为
通过脊线的平均平面。";
FraSweep.FraMandatory.SubFrameLit.LabRefAngle.Title="角度： ";
FraSweep.FraMandatory.SubFrameLit.LabRefAngle.LongHelp="指定角度，
用于在扫掠过程中控制轮廓位置。";
FraSweep.FraMandatory.SubFrameLit.FraRefAngle.EnglobingFrame.IntermediateFrame.Spinner.Title="角度";
FraSweep.FraMandatory.SubFrameLit.LawButton.Title="法则曲线...";

FraSweep.FraMandatory.SubFrameElt.LabGuideCurve2.Title="引导曲线 2： ";
FraSweep.FraMandatory.SubFrameElt.LabGuideCurve2.LongHelp="指定第二引导曲线。";
FraSweep.FraMandatory.SubFrameElt.GuideCurve2.LongHelp="指定第二引导曲线。";
FraSweep.FraMandatory.SubFrameElt.LabelDirComp.Title="定位类型： ";
FraSweep.FraMandatory.SubFrameElt.LabelDirComp.LongHelp="对于两导线显式扫掠曲面，允许指定
不同类型的定位选项。";
FraSweep.FraMandatory.SubFrameElt.ComboDirComp.LongHelp="对于两导线显式扫掠曲面，列出
可用的定位选项类型。";
FraSweep.FraMandatory.SubFrameElt.LabFittingPoint1.Title="- 定位点 1： ";
//FraSweep.FraMandatory.SubFrameElt.LabFittingPoint1.LongHelp="Specifies the first guide anchor point on the profile.";
//FraSweep.FraMandatory.SubFrameElt.FittingPoint1.LongHelp="Specifies the first guide anchor point on the profile.";
FraSweep.FraMandatory.SubFrameElt.LabFittingPoint2.Title="- 定位点 2： ";
//FraSweep.FraMandatory.SubFrameElt.LabFittingPoint2.LongHelp="Specifies the second guide anchor point on the profile.";
//FraSweep.FraMandatory.SubFrameElt.FittingPoint2.LongHelp="Specifies the second guide anchor point on the profile.";
FraSweep.FraMandatory.SubFrameElt.LabXAxisDirection.Title="- 定位方向： ";
//FraSweep.FraMandatory.SubFrameElt.LabXAxisDirection.LongHelp="Specifies the anchor direction on the profile.";
//FraSweep.FraMandatory.SubFrameElt.FraDirection.LongHelp="Specifies the anchor direction on the profile.";
FittingPoint1LongHelp="指定轮廓上的第一引导线定位点。";
FittingPoint2LongHelp="指定轮廓上的第二引导线定位点。";
LabXAxisDirectionLongHelp="指定轮廓上的定位方向。";
//Automated Anchor Elements Computation
Computed=" 经过计算所得的 ";
AutoFittingPoint1LongHelp="指定轮廓上的第一引导线定位点。默认情况下，它是轮廓平面与引导曲线 1 的相交点。";
AutoFittingPoint2LongHelp="指定轮廓上的第二引导线定位点。\n默认情况下，它是通过定位点 1 并与脊线垂直的平面与引导曲线 2 的相交点。";
AutoLabXAxisDirectionLongHelp="指定轮廓上的定位方向。默认情况下，在轮廓平面与引导曲线 1 和引导曲线 2 之间计算此方向。";

FraSweep.FraMandatory.SubFrameElt.LabPullingDir.Title="方向： ";
FraSweep.FraMandatory.SubFrameElt.LabPullingDir.LongHelp="指定拔模方向，
用于在扫掠过程中控制轮廓位置。";

FraSweep.FraOptional.GuideProjBut.Title="充当脊线的引导曲线投影";
FraSweep.FraOptional.GuideProjBut.LongHelp="允许将引导曲线投影至参考曲面
（如果参考曲面是平面），必要时也可投影至与拔模方向垂直的平面
以便使用投影作为脊线。";


// Tolerant sweep : explicite, lineaire, circulaire et conique
//---------------------------------------------------------------
FraSweep.FraOptional.FrameSmooth.SepStart.TitleLabel.Title="光顺扫掠 ";
FraSweep.FraOptional.FrameSmooth.FrameLayoutAngle.SmoothAngleButton.Title="角度修正： ";
FraSweep.FraOptional.FrameSmooth.FrameLayoutAngle.SmoothAngleButton.LongHelp="允许按照给定角度值移除不连续的部分
（在移动框架和相切网上）以执行光顺扫掠操作。";
FraSweep.FraOptional.FrameSmooth.FrameLayoutAngle.FraLittSmoothAngle.EnglobingFrame.IntermediateFrame.Spinner.Title="光顺角度";
FraSweep.FraOptional.FrameSmooth.FrameLayoutDev.SmoothDevButton.Title="与引导线偏差： ";
FraSweep.FraOptional.FrameSmooth.FrameLayoutDev.SmoothDevButton.LongHelp="允许通过在给定半径的管状曲面内
与引导曲线偏差来执行光顺扫掠操作。";
FraSweep.FraOptional.FrameSmooth.FrameLayoutDev.FraLittSmoothDev.EnglobingFrame.IntermediateFrame.Spinner.Title="光顺偏差";

// Sweep explicite : positionnement
//---------------------------------------------------------------
FraSweep.FraOptional.NotifFrame.StartSeparator.TitleLabel.Title="定位参数 ";

FraSweep.FraOptional.NotifFrame.FraButton.SelPosButton.Title="定位轮廓";
FraSweep.FraOptional.NotifFrame.FraButton.SelPosButton.LongHelp="默认情况下使用定位轮廓。
若要手动定位第一个扫掠平面中的轮廓，
请选中此框，并操作几何图形或修改定位参数。";
LabelShowPosParam="显示参数 >>";
LabelHidePosParam="隐藏参数 <<";

FraSweep.FraOptional.NotifFrame.AdvFrame.LabelExplPos1.Title="第一扫掠平面中的原点";

FraSweep.FraOptional.NotifFrame.AdvFrame.FraOriginMain.RadBOrigin1.Title="原点坐标";
FraSweep.FraOptional.NotifFrame.AdvFrame.FraOriginMain.RadBOrigin1.LongHelp="通过坐标指定第一扫掠平面中
定位轴系的原点。";
FraSweep.FraOptional.NotifFrame.AdvFrame.FraOriginMain.FraOrigin1.LabelLittX.Title="X： ";
FraSweep.FraOptional.NotifFrame.AdvFrame.FraOriginMain.FraOrigin1.FraLittX.EnglobingFrame.IntermediateFrame.Spinner.Title="X";
FraSweep.FraOptional.NotifFrame.AdvFrame.FraOriginMain.FraOrigin1.LabelLittY.Title="Y： ";
FraSweep.FraOptional.NotifFrame.AdvFrame.FraOriginMain.FraOrigin1.FraLittY.EnglobingFrame.IntermediateFrame.Spinner.Title="Y";

FraSweep.FraOptional.NotifFrame.AdvFrame.FraOriginMain.RadBOrigin2.Title="原点选择";
FraSweep.FraOptional.NotifFrame.AdvFrame.FraOriginMain.RadBOrigin2.LongHelp="通过点选择指定第一扫掠平面中
定位轴系的原点。";
FraSweep.FraOptional.NotifFrame.AdvFrame.FraOriginMain.FraOrigin2.LabOriginP1.Title="点： ";


FraSweep.FraOptional.NotifFrame.AdvFrame.SepAngleDir1.TitleLabel.Title="第一扫掠平面中的轴 ";

FraSweep.FraOptional.NotifFrame.AdvFrame.FraDirection.RadBDirection1.Title="旋转角度";
FraSweep.FraOptional.NotifFrame.AdvFrame.FraDirection.RadBDirection1.LongHelp="绕原点的初始位置
旋转定位轴系。";
FraSweep.FraOptional.NotifFrame.AdvFrame.FraDirection.FraDirection1.FraLittAngle.EnglobingFrame.IntermediateFrame.Spinner.Title="角度";

FraSweep.FraOptional.NotifFrame.AdvFrame.FraDirection.RadBDirection2.Title="第一轴线选择";
FraSweep.FraOptional.NotifFrame.AdvFrame.FraDirection.RadBDirection2.LongHelp="通过直线选择或部件
指定定位轴系的 X 轴。";
FraSweep.FraOptional.NotifFrame.AdvFrame.FraDirection.FraDirection2.LabDirection.Title="方向： ";
FraSweep.FraOptional.NotifFrame.AdvFrame.FraDirection.FraDirection2.LabDirection.LongHelp="通过直线选择或部件
指定定位轴系的 X 轴。";

FraSweep.FraOptional.NotifFrame.AdvFrame.SwapXAxisButtonV5.Title="已反转 X 轴";
FraSweep.FraOptional.NotifFrame.AdvFrame.SwapXAxisButtonV5.LongHelp="反转 X 轴方向。";
FraSweep.FraOptional.NotifFrame.AdvFrame.SwapYAxisButtonV5.Title="已反转 Y 轴";
FraSweep.FraOptional.NotifFrame.AdvFrame.SwapYAxisButtonV5.LongHelp="反转 Y 轴方向。";

FraSweep.FraOptional.NotifFrame.AdvFrame.SwapXAxisButtonBirailV5.Title="已反转轮廓端点";
FraSweep.FraOptional.NotifFrame.AdvFrame.SwapXAxisButtonBirailV5.LongHelp="反转连接到引导线的轮廓端点。";
FraSweep.FraOptional.NotifFrame.AdvFrame.SwapYAxisButtonBirailV5.Title="已反转垂直方向";
FraSweep.FraOptional.NotifFrame.AdvFrame.SwapYAxisButtonBirailV5.LongHelp="反转轮廓的垂直方向。";

FraSweep.FraOptional.NotifFrame.AdvFrame.SwapXAxisButton.Title="已反转 X 轴";
FraSweep.FraOptional.NotifFrame.AdvFrame.SwapXAxisButton.LongHelp="反转 X 轴方向。";
FraSweep.FraOptional.NotifFrame.AdvFrame.SwapYAxisButton.Title="已反转 Y 轴";
FraSweep.FraOptional.NotifFrame.AdvFrame.SwapYAxisButton.LongHelp="反转 Y 轴方向。";

FraSweep.FraOptional.NotifFrame.AdvFrame.SwapXAxisButtonBirail.Title="已反转轮廓端点";
FraSweep.FraOptional.NotifFrame.AdvFrame.SwapXAxisButtonBirail.LongHelp="反转连接到引导线的轮廓端点。";
FraSweep.FraOptional.NotifFrame.AdvFrame.SwapYAxisButtonBirail.Title="已反转垂直方向";
FraSweep.FraOptional.NotifFrame.AdvFrame.SwapYAxisButtonBirail.LongHelp="反转轮廓的垂直方向。";


FraSweep.FraOptional.NotifFrame.AdvFrame.SepSdAxisSyst2.TitleLabel.Title="轮廓上的定位元素 ";

FraSweep.FraOptional.NotifFrame.AdvFrame.FraOriginSd.LabOriginP2.Title="点： ";
FraSweep.FraOptional.NotifFrame.AdvFrame.FraOriginSd.LabOriginP2.LongHelp="指定轮廓上轴系的原点。";
FraSweep.FraOptional.NotifFrame.AdvFrame.FraOriginSd.SelOriginP2.LongHelp="指定轮廓上轴系的原点。";

FraSweep.FraOptional.NotifFrame.AdvFrame.FraOriginSd.LabDirection2.Title="X 轴方向： ";
FraSweep.FraOptional.NotifFrame.AdvFrame.FraOriginSd.LabDirection2.LongHelp="指定轮廓上轴系的 X 轴方向。";
FraSweep.FraOptional.NotifFrame.AdvFrame.FraOriginSd.FraDirection.LongHelp="指定轮廓上轴系的原点。";



// Sweep segment
//---------------------------------------------------------------
FraSweep.FraLayout1.FraComboLine.LabelComboLine.Title="子类型： ";
FraSweep.FraLayout1.FraComboLine.LabelComboLine.LongHelp="列出使用线性轮廓的扫掠曲面子类型。";
FraSweep.FraLayout1.FraComboLine.ComboLine.LongHelp="列出使用线性轮廓的扫掠曲面子类型。";
FraSweep.FraLayout1.FraMandatory.FraLayout2.FraDirection.LongHelp="指定直线，其方向决定了拔模方向，\n或指定平面，其法线决定了拔模方向。\n您也可以使用上下文菜单指定\n拔模方向的 X、Y、Z 分量。";

SweepLine1="两极限";
SweepLine2="极限和中间";
SweepLine3="使用参考曲面";
SweepLine4="使用参考曲线";
SweepLine5="使用切面";
SweepLine6="使用拔模方向";
SweepLine7="使用双切面";

// Good for sweep circle
FraSweep.FraLayout1.FraMandatory.Title="必选元素";

FraSweep.FraLayout1.FraMandatory.FraLayout2.LabGuide.Title="引导线： ";
FraSweep.FraLayout1.FraMandatory.FraLayout2.LabGuide.LongHelp="指定引导线。";
FraSweep.FraLayout1.FraMandatory.FraLayout2.Guide.LongHelp="指定引导线。";

FraSweep.FraLayout1.FraMandatory.FraLayout2.LabGuideCurve1.Title="引导曲线 1： ";
FraSweep.FraLayout1.FraMandatory.FraLayout2.LabGuideCurve1.LongHelp="指定第一引导曲线。";
FraSweep.FraLayout1.FraMandatory.FraLayout2.GuideCurve1.LongHelp="指定第一引导曲线。";

FraSweep.FraLayout1.FraMandatory.FraLayout2.LabLimitCurve.Title="限制曲线： ";
FraSweep.FraLayout1.FraMandatory.FraLayout2.LabLimitCurve.LongHelp="指定限制曲线。";
FraSweep.FraLayout1.FraMandatory.FraLayout2.LimitCurve.LongHelp="指定限制曲线。";

FraSweep.FraLayout1.FraMandatory.FraLayout2.LabGuideCurve2.Title="引导曲线 2： ";
FraSweep.FraLayout1.FraMandatory.FraLayout2.LabGuideCurve2.LongHelp="指定第二引导曲线。";
FraSweep.FraLayout1.FraMandatory.FraLayout2.GuideCurve2.LongHelp="指定第二引导曲线。";

FraSweep.FraLayout1.FraMandatory.FraLayout2.LabCrvRef.Title="参考曲线： ";
FraSweep.FraLayout1.FraMandatory.FraLayout2.LabCrvRef.LongHelp="指定用于定义角度的参考曲线。";
FraSweep.FraLayout1.FraMandatory.FraLayout2.CrvRef.LongHelp="指定用于定义角度的参考曲线。";
FraSweep.FraLayout1.FraMandatory.FraLayout2.LabSurfRef.Title="参考曲面： ";
FraSweep.FraLayout1.FraMandatory.FraLayout2.LabSurfRef.LongHelp="指定用于定义角度的参考曲面。";
FraSweep.FraLayout1.FraMandatory.FraLayout2.SurfRef.LongHelp="指定用于定义角度的参考曲面。";

FraSweep.FraLayout1.FraMandatory.FraLayout2.LabSurfTan.Title="切面： ";
FraSweep.FraLayout1.FraMandatory.FraLayout2.LabSurfTan.LongHelp="指定与结果相切的曲面。";
FraSweep.FraLayout1.FraMandatory.FraLayout2.SurfTan.LongHelp="指定与结果相切的曲面。";

FraSweep.FraLayout1.FraMandatory.FraLayout2.LabDirection.Title="拔模方向： ";
FraSweep.FraLayout1.FraMandatory.FraLayout2.LabDirection.LongHelp="指定拔模方向：直线、平面或坐标。";

FraSweep.FraLayout1.FraMandatory.FrameDraftMode.LabDraftMode.Title="拔模计算模式： ";
FraSweep.FraLayout1.FraMandatory.FrameDraftMode.LabDraftMode.LongHelp="指定拔模计算模式。";
FraSweep.FraLayout1.FraMandatory.FrameDraftMode.RadBModeSquare.Title="正方形";
FraSweep.FraLayout1.FraMandatory.FrameDraftMode.RadBModeSquare.LongHelp="激活拔模的正方形计算模式。";
FraSweep.FraLayout1.FraMandatory.FrameDraftMode.RadBModeCone.Title="圆锥面";
FraSweep.FraLayout1.FraMandatory.FrameDraftMode.RadBModeCone.LongHelp="激活拔模的二次曲线计算模式。";

FraSweep.FraLayout1.FraMandatory.FraLittLayout0.LabRefAngle.Title="角度： ";
FraSweep.FraLayout1.FraMandatory.FraLittLayout0.FraRefAngle.EnglobingFrame.IntermediateFrame.Spinner.Title="角度";
FraSweep.FraLayout1.FraMandatory.FraLittLayout0.LawButton.Title="法则曲线...";

FraSweep.FraLayout1.FraMandatory.FraLittLayout1.LabLength1.Title="长度 1： ";
FraSweep.FraLayout1.FraMandatory.FraLittLayout1.FraLength1.EnglobingFrame.IntermediateFrame.Spinner.Title="长度 1";
FraSweep.FraLayout1.FraMandatory.FraLittLayout1.LawButton1.Title="法则曲线...";
FraSweep.FraLayout1.FraMandatory.FraLittLayout1.LabLength2.Title="长度 2： ";
FraSweep.FraLayout1.FraMandatory.FraLittLayout1.FraLength2.EnglobingFrame.IntermediateFrame.Spinner.Title="长度 2";
FraSweep.FraLayout1.FraMandatory.FraLittLayout1.LawButton2.Title="法则曲线...";

FraSweep.FraLayout1.FraMandatory.FraLayout2.LabSpine.Title="脊线： ";
FraSweep.FraLayout1.FraMandatory.FraLayout2.LabSpine.LongHelp="指定脊线。";
FraSweep.FraLayout1.FraMandatory.FraLayout2.Spine.LongHelp="指定脊线。";

FraSweep.FraLayout1.FraMandatory.FraLayout2.LabSurfTan1.Title="第一切面： ";
FraSweep.FraLayout1.FraMandatory.FraLayout2.LabSurfTan1.LongHelp="指定与结果相切的第一曲面。";
FraSweep.FraLayout1.FraMandatory.FraLayout2.SurfTan1.LongHelp="指定与结果相切的第一曲面。";

FraSweep.FraLayout1.FraMandatory.FraLayout2.LabSurfTan2.Title="第二切面： ";
FraSweep.FraLayout1.FraMandatory.FraLayout2.LabSurfTan2.LongHelp="指定与结果相切的第二曲面。";
FraSweep.FraLayout1.FraMandatory.FraLayout2.SurfTan2.LongHelp="指定与结果相切的第二曲面。";

// Good for sweep circle
FraSweep.FraOptional.Title="可选元素";

FraSweep.FraOptional.FraSpineLayout.LabSpine.Title="脊线： ";
FraSweep.FraOptional.FraSpineLayout.LabSpine.LongHelp="指定脊线（如果您希望使用
与第一引导曲线不同的脊线）。";
FraSweep.FraOptional.FraSpineLayout.Spine.LongHelp="指定脊线（如果您希望使用
与第一引导曲线不同的脊线）。";

FraSweep.FraOptional.FraSpineLayout.LabRelimiter1.Title="  边界 1： ";
FraSweep.FraOptional.FraSpineLayout.LabRelimiter1.LongHelp="指定用于重新限定脊线的第一
元素（点或平面），以便进行扫掠。";
FraSweep.FraOptional.FraSpineLayout.Relimiter1.LongHelp="指定用于重新限定脊线的第一
元素（点或平面），以便进行扫掠。";

FraSweep.FraOptional.FraSpineLayout.LabRelimiter2.Title="  边界 2： ";
FraSweep.FraOptional.FraSpineLayout.LabRelimiter2.LongHelp="指定用于重新限定脊线的第二
元素（点或平面），以便进行扫掠。";
FraSweep.FraOptional.FraSpineLayout.Relimiter2.LongHelp="指定用于重新限定脊线的第二
元素（点或平面），以便进行扫掠。";

FraSweep.FraOptional.SegmentButton.Title="第二曲线作为中间曲线";
FraSweep.FraOptional.SegmentButton.LongHelp="将第二引导线作为中间曲线。
每个扫掠平面中扫掠曲面的宽度将
是引导线和中间曲线之间距离的两倍。";

FraSweep.FraOptional.TrimOptionButton.Title="使用切面修剪";
FraSweep.FraOptional.TrimOptionButton.LongHelp="使用切面修剪扫掠曲面。";

FraSweep.FraOptional.TrimOptionButton1.Title="使用第一切面修剪";
FraSweep.FraOptional.TrimOptionButton1.LongHelp="使用第一切面修剪扫掠曲面。";

FraSweep.FraOptional.TrimOptionButton2.Title="使用第二切面修剪";
FraSweep.FraOptional.TrimOptionButton2.LongHelp="使用第二切面修剪扫掠曲面。";

FraSweep.FraOptional.FraLittLayout2.SolutionSelectorFrame.EspaceLabel1.Title="解法： ";
FraSweep.FraOptional.SolutionSelectorFrame.EspaceLabel1.Title="解法： ";
FraSweep.FraOptional.SolutionSelectorFrame.EspaceLabel1.LongHelp="允许浏览解法集。";

FraSweep.FraOptional.FraLittLayout1.LabLength1.Title="长度 1： ";
FraSweep.FraOptional.FraLittLayout1.FraLength1.EnglobingFrame.IntermediateFrame.Spinner.Title="长度 1";
FraSweep.FraOptional.FraLittLayout1.LawButton1.Title="法则曲线...";
FraSweep.FraOptional.FraLittLayout1.LabLength2.Title="长度 2： ";
FraSweep.FraOptional.FraLittLayout1.FraLength2.EnglobingFrame.IntermediateFrame.Spinner.Title="长度 2";
FraSweep.FraOptional.FraLittLayout1.LawButton2.Title="法则曲线...";

FraSweep.FraOptional.FraLittLayout2.LabLength2.Title="长度 2： ";
FraSweep.FraOptional.FraLittLayout2.FraLength2.EnglobingFrame.IntermediateFrame.Spinner.Title="长度 2";

FraSweep.FraOptional.FraLittLayout1.LabLengthElem1.Title="重新限定元素 1： ";
FraSweep.FraOptional.FraLittLayout1.LabLengthElem1.LongHelp="指定定义扫掠曲面重新限定的点或平面，以替换长度 1。";
FraSweep.FraOptional.FraLittLayout1.LengthElem1.LongHelp="指定定义扫掠曲面重新限定的点或平面，以替换长度 1。";
FraSweep.FraOptional.FraLittLayout2.LabLengthElem2.Title="重新限定元素 2： ";
FraSweep.FraOptional.FraLittLayout2.LabLengthElem2.LongHelp="指定定义扫掠曲面重新限定的点或平面，以替换长度 2。";
FraSweep.FraOptional.FraLittLayout2.LengthElem2.LongHelp="指定定义扫掠曲面重新限定的点或平面，以替换长度 2。";

FraSweep.FraOptional.SolutionSelectorFrame.CurrentEditor.Title="解法";
FraSweep.FraOptional.SolutionSelectorFrame.CurrentEditor.LongHelp="显示并允许键入当前解法的编号。";

FraSweep.FraOptional.OngletsDraftAngle.OngletCst.Title="全部定义";
FraSweep.FraOptional.OngletsDraftAngle.OngletCst.LongHelp="允许为整个扫掠操作指定拔模斜度法则曲线。";
FraSweep.FraOptional.OngletsDraftAngle.OngletCst.FraLittLayout0.LabRefAngle.Title="角度： ";
FraSweep.FraOptional.OngletsDraftAngle.OngletCst.FraLittLayout0.FraRefAngle.EnglobingFrame.IntermediateFrame.Spinner.Title="角度";
FraSweep.FraOptional.OngletsDraftAngle.OngletCst.FraLittLayout0.LawButton.Title="法则曲线...";

FraSweep.FraOptional.OngletsDraftAngle.OngletCstG1.Title="G1-常量";
FraSweep.FraOptional.OngletsDraftAngle.OngletCstG1.LongHelp="允许为引导曲线的任意连续相切部分指定拔模斜度值。";
FraSweep.FraOptional.OngletsDraftAngle.OngletCstG1.FraG1Angle.LabG1Angle.Title="当前角度： ";
FraSweep.FraOptional.OngletsDraftAngle.OngletCstG1.FraG1Angle.LabG1Angle.LongHelp="为引导曲线当前的 G1 部分指定拔模斜度。";
FraSweep.FraOptional.OngletsDraftAngle.OngletCstG1.FraG1Angle.CkeFraG1Angle.LongHelp="为引导曲线当前的 G1 部分指定拔模斜度。";
FraSweep.FraOptional.OngletsDraftAngle.OngletCstG1.FraG1Angle.CkeFraG1Angle.EnglobingFrame.IntermediateFrame.Spinner.Title="当前角度";

FraSweep.FraOptional.OngletsDraftAngle.OngletVar.Title="位置值";
FraSweep.FraOptional.OngletsDraftAngle.OngletVar.LongHelp="允许为引导曲线上的给定点指定拔模斜度值。";
FraSweep.FraOptional.OngletsDraftAngle.OngletVar.FraLocValAngle.LabLocValAngle.Title="当前角度： ";
FraSweep.FraOptional.OngletsDraftAngle.OngletVar.FraLocValAngle.LabLocValAngle.LongHelp="为当前点指定拔模斜度。";
FraSweep.FraOptional.OngletsDraftAngle.OngletVar.FraLocValAngle.CkeFraLocValAngle.LongHelp="为当前点指定拔模斜度。";
FraSweep.FraOptional.OngletsDraftAngle.OngletVar.FraLocValAngle.CkeFraLocValAngle.EnglobingFrame.IntermediateFrame.Spinner.Title="当前角度";
DraftLocationTitreColonne1="位置";
DraftLocationTitreColonne2="值";
FraSweep.FraOptional.OngletsDraftAngle.OngletVar.RemoveLocBut.Title="移除当前位置";
FraSweep.FraOptional.OngletsDraftAngle.OngletVar.RemoveLocBut.LongHelp="允许移除选定的角度位置。";

FraSweep.FraOptional.FraDraftLengthBut1.LabLengthType.Title="长度类型 1： ";
FraSweep.FraOptional.FraDraftLengthBut1.LabLengthType.LongHelp="指定长度定义的类型。";
FraSweep.FraOptional.FraDraftLengthBut1.RadBFromCurve.LongHelp="从曲线：扫略曲面起始于该曲线。";
FraSweep.FraOptional.FraDraftLengthBut1.RadBFromCurve.ShortHelp="从曲线";
FraSweep.FraOptional.FraDraftLengthBut1.RadBStandard.LongHelp="标准：在扫掠平面中计算长度（0 等价于“从曲线”选项）。";
FraSweep.FraOptional.FraDraftLengthBut1.RadBStandard.ShortHelp="标准";
FraSweep.FraOptional.FraDraftLengthBut1.RadBFromUpTo.LongHelp="从/到：通过相交平面或曲面计算长度。";
FraSweep.FraOptional.FraDraftLengthBut1.RadBFromUpTo.ShortHelp="从/到";
FraSweep.FraOptional.FraDraftLengthBut1.RadBExtremum.LongHelp="从极值：该长度从极值平面开始沿拔模方向取得。";
FraSweep.FraOptional.FraDraftLengthBut1.RadBExtremum.ShortHelp="从极值";
FraSweep.FraOptional.FraDraftLengthBut1.RadBLengthAlong.LongHelp="沿曲面：该长度用作重新限定扫掠曲面的欧几里得平行曲线距离。";
FraSweep.FraOptional.FraDraftLengthBut1.RadBLengthAlong.ShortHelp="沿曲面";

FraSweep.FraOptional.FraDraftLengthBut2.LabLengthType.Title="长度类型 2： ";
FraSweep.FraOptional.FraDraftLengthBut2.LabLengthType.LongHelp="指定长度定义的类型。";
FraSweep.FraOptional.FraDraftLengthBut2.RadBFromCurve.LongHelp="从曲线：扫略曲面起始于该曲线。";
FraSweep.FraOptional.FraDraftLengthBut2.RadBFromCurve.ShortHelp="从曲线";
FraSweep.FraOptional.FraDraftLengthBut2.RadBStandard.LongHelp="标准：在扫掠平面中计算长度（0 等价于“从曲线”选项）。";
FraSweep.FraOptional.FraDraftLengthBut2.RadBStandard.ShortHelp="标准";
FraSweep.FraOptional.FraDraftLengthBut2.RadBFromUpTo.LongHelp="从/到：通过相交平面或曲面计算长度。";
FraSweep.FraOptional.FraDraftLengthBut2.RadBFromUpTo.ShortHelp="从/到";
FraSweep.FraOptional.FraDraftLengthBut2.RadBExtremum.LongHelp="从极值：该长度从极值平面开始沿拔模方向取得。";
FraSweep.FraOptional.FraDraftLengthBut2.RadBExtremum.ShortHelp="从极值";
FraSweep.FraOptional.FraDraftLengthBut2.RadBLengthAlong.LongHelp="沿曲面：该长度用作重新限定扫掠曲面的欧几里得平行曲线距离。";
FraSweep.FraOptional.FraDraftLengthBut2.RadBLengthAlong.ShortHelp="沿曲面";


// Sweep circle
//---------------------------------------------------------------
FraSweep.FraLayout1.FraComboCircle.LabelComboCircle.Title="子类型： ";
FraSweep.FraLayout1.FraComboCircle.LabelComboCircle.LongHelp="列出使用圆弧轮廓的扫掠曲面子类型。";
FraSweep.FraLayout1.FraComboCircle.ComboCircle.LongHelp="列出使用圆弧轮廓的扫掠曲面子类型。";

FraSweep.FraLayout1.FraComboCircleVol.LabelComboCircleVol.Title="子类型： ";
FraSweep.FraLayout1.FraComboCircleVol.LabelComboCircleVol.LongHelp="列出使用圆弧轮廓的扫掠包络体子类型。";
FraSweep.FraLayout1.FraComboCircleVol.ComboCircleVol.LongHelp="列出使用圆弧轮廓的扫掠包络体子类型。";

SweepCircle1="三条引导线";
SweepCircle2="两个点和半径";
SweepCircle3="中心和两个角度";
VolumeSweepCircle3="中心和参考曲线";
SweepCircle4="圆心和半径";
SweepCircle5="两条引导线和切面";
SweepCircle6="一条引导线和切面";
SweepCircleLimitCurveTangencySurface="限制曲线和切面";

FraSweep.FraLayout1.FraMandatory.FraLayout2.LabGuideCurve3.Title="引导曲线 3： ";
FraSweep.FraLayout1.FraMandatory.FraLayout2.LabGuideCurve3.LongHelp="指定第三引导曲线。";
FraSweep.FraLayout1.FraMandatory.FraLayout2.GuideCurve3.LongHelp="指定第三引导曲线。";

FraSweep.FraLayout1.FraMandatory.FraLayout2.LabCenterCurve.Title="中心曲线： ";
FraSweep.FraLayout1.FraMandatory.FraLayout2.LabCenterCurve.LongHelp="指定中心曲线。";
FraSweep.FraLayout1.FraMandatory.FraLayout2.CenterCurve.LongHelp="指定中心曲线。";

FraSweep.FraLayout1.FraMandatory.FraLayout2.LabAngleRefCurve.Title="参考曲线： ";
FraSweep.FraLayout1.FraMandatory.FraLayout2.LabAngleRefCurve.LongHelp="指定参考角度曲线。";
FraSweep.FraLayout1.FraMandatory.FraLayout2.AngleRefCurve.LongHelp="指定参考角度曲线。";

FraSweep.FraLayout1.FraMandatory.FraLayout2.LabGuideCurveWithTangency.Title="相切的限制曲线： ";
FraSweep.FraLayout1.FraMandatory.FraLayout2.LabGuideCurveWithTangency.LongHelp="指定强制相切的引导曲线。";
FraSweep.FraLayout1.FraMandatory.FraLayout2.GuideCurveWithTangency.LongHelp="指定强制相切的引导曲线。";

FraSweep.FraLayout1.FraMandatory.FraLayout2.LabLimitCurve.Title="限制曲线： ";
FraSweep.FraLayout1.FraMandatory.FraLayout2.LabLimitCurve.LongHelp="指定另一条限制曲线。";
FraSweep.FraLayout1.FraMandatory.FraLayout2.LimitCurve.LongHelp="指定另一条限制曲线。";

FraSweep.FraLayout1.FraMandatory.LimitCurveAngles.LawButton.Title="法则曲线...";

FraSweep.FraLayout1.FraMandatory.LimitCurveAngles.LabAngle1.Title="角度 1： ";
FraSweep.FraLayout1.FraMandatory.LimitCurveAngles.LabAngle1.LongHelp="指定第一角度值。";
FraSweep.FraLayout1.FraMandatory.LimitCurveAngles.FraAngle1.EnglobingFrame.IntermediateFrame.Spinner.Title="角度 1";

FraSweep.FraLayout1.FraMandatory.LimitCurveAngles.LabAngle2.Title="角度 2： ";
FraSweep.FraLayout1.FraMandatory.LimitCurveAngles.LabAngle2.LongHelp="指定第二角度值。";
FraSweep.FraLayout1.FraMandatory.LimitCurveAngles.FraAngle2.EnglobingFrame.IntermediateFrame.Spinner.Title="角度 2";


FraSweep.FraLayout1.FraMandatory.FraLittLayout1.LabRadius.Title="半径： ";
FraSweep.FraLayout1.FraMandatory.FraLittLayout1.LabRadius.LongHelp="指定半径值。";
FraSweep.FraLayout1.FraMandatory.FraLittLayout1.FraRadius.EnglobingFrame.IntermediateFrame.Spinner.Title="半径";
FraSweep.FraLayout1.FraMandatory.FraLittLayout1.LawButton.Title="法则曲线...";

FraSweep.FraLayout1.FraMandatory.FraLittLayout1.LabAngle1.Title="角度 1： ";
FraSweep.FraLayout1.FraMandatory.FraLittLayout1.LabAngle1.LongHelp="指定第一角度值。";
FraSweep.FraLayout1.FraMandatory.FraLittLayout1.FraAngle1.EnglobingFrame.IntermediateFrame.Spinner.Title="角度 1";

FraSweep.FraLayout1.FraMandatory.FraLittLayout1.LabAngle2.Title="角度 2： ";
FraSweep.FraLayout1.FraMandatory.FraLittLayout1.LabAngle2.LongHelp="指定第二角度值。";
FraSweep.FraLayout1.FraMandatory.FraLittLayout1.FraAngle2.EnglobingFrame.IntermediateFrame.Spinner.Title="角度 2";

FraSweep.FraOptional.FraLittLayout2.ButOptionRadius.Title="使用固定半径： ";
FraSweep.FraOptional.FraLittLayout2.ButOptionRadius.LongHelp="允许指定圆半径。
如果未激活，则圆半径将与
中心和参考曲线间的距离相对应。";
FraSweep.FraOptional.FraLittLayout2.FraRadius.EnglobingFrame.IntermediateFrame.Spinner.Title="半径";
FraSweep.FraOptional.FraLittLayout2.LawButton.Title="法则曲线...";


// Sweep conique
//---------------------------------------------------------------
FraSweep.FraLayout1.FraComboConic.LabelComboConic.Title="子类型： ";
FraSweep.FraLayout1.FraComboConic.LabelComboConic.LongHelp="列出使用二次曲线轮廓的扫掠曲面子类型。";
FraSweep.FraLayout1.FraComboConic.ComboConic.LongHelp="列出使用二次曲线轮廓的扫掠曲面子类型。";

SweepConic1="两条引导曲线";
SweepConic2="三条引导曲线";
SweepConic3="四条引导曲线";
SweepConic4="五条引导曲线";

FraSweep.FraLayout1.FraMandatory.FraGuide1.LabGuideCurve1.Title="引导曲线 1： ";
FraSweep.FraLayout1.FraMandatory.FraGuide1.LabGuideCurve1.LongHelp="指定第一引导曲线。";
FraSweep.FraLayout1.FraMandatory.FraGuide1.GuideCurve1.LongHelp="指定第一引导曲线。";

FraSweep.FraLayout1.FraMandatory.FraTan1.Title="关联的相切元素";
FraSweep.FraLayout1.FraMandatory.FraTan1.SubFra1.LabTangencyElement1.Title="相切： ";
FraSweep.FraLayout1.FraMandatory.FraTan1.SubFra1.LabTangencyElement1.LongHelp="指定第一引导曲线的相切元素（曲线或曲面）。";
FraSweep.FraLayout1.FraMandatory.FraTan1.SubFra1.TangencyElement1.LongHelp="指定第一引导曲线的相切元素（曲线或曲面）。";
FraSweep.FraLayout1.FraMandatory.FraTan1.SubFra2.LabRefAngle1.Title="角度： ";
FraSweep.FraLayout1.FraMandatory.FraTan1.SubFra2.LabRefAngle1.LongHelp="指定第一引导线相切角度。";
FraSweep.FraLayout1.FraMandatory.FraTan1.SubFra2.FraAngle1.EnglobingFrame.IntermediateFrame.Spinner.Title="角度 1";
FraSweep.FraLayout1.FraMandatory.FraTan1.SubFra2.LawButton.Title="法则曲线...";

FraSweep.FraLayout1.FraMandatory.FraGuideFin.LabGuideCurveFin.Title="结束引导曲线： ";
FraSweep.FraLayout1.FraMandatory.FraGuideFin.LabGuideCurveFin.LongHelp="指定结束引导曲线。";
FraSweep.FraLayout1.FraMandatory.FraGuideFin.GuideCurveFin.LongHelp="指定结束引导曲线。";

FraSweep.FraLayout1.FraMandatory.FraTanFin.Title="关联的相切元素";
FraSweep.FraLayout1.FraMandatory.FraTanFin.SubFra1.LabTangencyElementFin.Title="相切： ";
FraSweep.FraLayout1.FraMandatory.FraTanFin.SubFra1.LabTangencyElementFin.LongHelp="指定结束引导曲线的相切元素（曲线或曲面）。";
FraSweep.FraLayout1.FraMandatory.FraTanFin.SubFra1.TangencyElementFin.LongHelp="指定结束引导曲线的相切元素（曲线或曲面）。";
FraSweep.FraLayout1.FraMandatory.FraTanFin.SubFra2.LabRefAngleFin.Title="角度： ";
FraSweep.FraLayout1.FraMandatory.FraTanFin.SubFra2.LabRefAngleFin.LongHelp="指定结束引导线相切角度。";
FraSweep.FraLayout1.FraMandatory.FraTanFin.SubFra2.FraAngleFin.EnglobingFrame.IntermediateFrame.Spinner.Title="角度 2";
FraSweep.FraLayout1.FraMandatory.FraTanFin.SubFra2.LawButton.Title="法则曲线...";

FraSweep.FraLayout1.FraMandatory.FraExtra.LabGuideCurve2.Title="引导曲线 2： ";
FraSweep.FraLayout1.FraMandatory.FraExtra.LabGuideCurve2.LongHelp="指定第二引导曲线。";
FraSweep.FraLayout1.FraMandatory.FraExtra.GuideCurve2.LongHelp="指定第二引导曲线。";

FraSweep.FraLayout1.FraMandatory.FraExtra.LabGuideCurve3.Title="引导曲线 3： ";
FraSweep.FraLayout1.FraMandatory.FraExtra.LabGuideCurve3.LongHelp="指定第三引导曲线。";
FraSweep.FraLayout1.FraMandatory.FraExtra.GuideCurve3.LongHelp="指定第三引导曲线。";

FraSweep.FraLayout1.FraMandatory.FraExtra.LabGuideCurve4.Title="引导曲线 4： ";
FraSweep.FraLayout1.FraMandatory.FraExtra.LabGuideCurve4.LongHelp="指定第四引导曲线。";
FraSweep.FraLayout1.FraMandatory.FraExtra.GuideCurve4.LongHelp="指定第四引导曲线。";

FraSweep.FraLayout1.FraMandatory.FraExtra.LabParam.Title="参数： ";
FraSweep.FraLayout1.FraMandatory.FraExtra.LabParam.LongHelp="指定二次曲线参数。";
FraSweep.FraLayout1.FraMandatory.FraExtra.FraParam.EnglobingFrame.IntermediateFrame.Spinner.Title="参数";
FraSweep.FraLayout1.FraMandatory.FraExtra.FraParam.LongHelp="指定二次曲线参数。";
FraSweep.FraLayout1.FraMandatory.FraExtra.LawButton.Title="法则曲线...";

//sweep angular sector
FraSweep.FraOptional.OngletsDraftAngle.OngletCst.FraLittLayout0.LabRefAngle.Title="角度： ";
FraSweep.FraOptional.OngletsDraftAngle.OngletCst.FraLittLayout0.LawButton.Title="法则曲线...";
FraSweep.FraOptional.OngletsDraftAngle.OngletCst.FraLittLayout0.SolutionSelectorFrame.EspaceLabel1.Title="角扇形： ";

FraSweep.FraLayout1.FraMandatory.FraLittLayoutSol.SolutionSelectorFrame.EspaceLabel1.Title="角扇形： ";
FraSweep.FraLayout1.FraMandatory.FraLittLayoutSol.SolutionSelectorFrame.EspaceLabel1.LongHelp="允许浏览解法集。";
FraSweep.FraMandatory.SolutionSelectorFrame.EspaceLabel1.Title="角扇形： ";
FraSweep.FraMandatory.SolutionSelectorFrame.EspaceLabel1.LongHelp="允许浏览解法集。";
FraSweep.FraOptional.OngletsDraftAngle.OngletVar.SolutionSelectorFrame.EspaceLabel1.Title="角扇形： ";
FraSweep.FraOptional.OngletsDraftAngle.OngletVar.SolutionSelectorFrame.EspaceLabel1.LongHelp="允许浏览解法集。";
FraSweep.FraOptional.OngletsDraftAngle.OngletCstG1.SolutionSelectorFrame.EspaceLabel1.Title="角扇形： ";
FraSweep.FraOptional.OngletsDraftAngle.OngletCstG1.SolutionSelectorFrame.EspaceLabel1.LongHelp="允许浏览解法集。";

//end 

//For Canonical Shape Detection Check Button Frame
FraSweep.CanonicalFrame.CanonicalCheckButton.LongHelp="切换按此按钮可以启用/禁用规范形状检测模式。";
FraSweep.CanonicalFrame.CanonicalCheckButton.Title="规范形状检测";

//For Canonical Shape Detection Check Button of Explicit Sweep Frame
FraSweep.CanonicalFrame.CanonicalCheckButtonForNonCanonical.LongHelp="切换此复选按钮可以启用/禁用用于获取不规范形状结果的规范形状检测模式。";
FraSweep.CanonicalFrame.CanonicalCheckButtonForNonCanonical.Title="用于获取不规范形状结果的规范形状检测";
