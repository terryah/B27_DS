// 3DAxisSystem Commands
CATMmuAxisSystem.CATAxisSystemCommand.GetDirectionOrPoint.Message="选择点、线或平面";
CATMmuAxisSystem.CATAxisSystemCommand.GetPoint.Message="选择点";
CATMmuAxisSystem.CATAxisSystemCommand.GetRefAxisSystem.Message="选择参考轴系";

CATMmuAxisSystem.CATAxisSystemCommand.UndoTitle="轴系";
CATMmuAxisSystem.CATCom3DAxisSystemSetCurrent.UndoTitle="当前轴系";

// 3DAxisSystem Panel
CATMmuAxisSystem.AxisSystemPanel.Title="轴系定义";

CATMmuAxisSystem.AxisSystemPanel.AxisSystemType.Title="轴系类型： ";
CATMmuAxisSystem.AxisSystemPanel.AxisSystemType.LongHelp="指定创建轴系的方法。";
CATMmuAxisSystem.AxisSystemPanel.TypeCombo.LongHelp="列出可用的轴系创建方法。";
CATMmuAxisSystem.AxisSystemPanel.Standard="标准";
CATMmuAxisSystem.AxisSystemPanel.AxisRotation="轴旋转";
CATMmuAxisSystem.AxisSystemPanel.EulerAngles="欧拉角";
CATMmuAxisSystem.AxisSystemPanel.Explicit="显式";

CATMmuAxisSystem.AxisSystemPanel.Origin="原点";
CATMmuAxisSystem.AxisSystemPanel.Origin.Title="原点： ";
CATMmuAxisSystem.AxisSystemPanel.Origin.LongHelp="指定原点。";
CATMmuAxisSystem.AxisSystemPanel.DirectionSelectorList0.LongHelp="选择用于定位轴系原点的点。";
CATMmuAxisSystem.AxisSystemPanel.XAxis.Title="X 轴： ";
CATMmuAxisSystem.AxisSystemPanel.YAxis.Title="Y 轴： ";
CATMmuAxisSystem.AxisSystemPanel.ZAxis.Title="Z 轴： ";

CATMmuAxisSystem.AxisSystemPanel.XAxis.LongHelp="指定 X 轴。";
CATMmuAxisSystem.AxisSystemPanel.YAxis.LongHelp="指定 Y 轴。";
CATMmuAxisSystem.AxisSystemPanel.ZAxis.LongHelp="指定 Z 轴。";
CATMmuAxisSystem.AxisSystemPanel.DirectionSelectorList1.LongHelp="选择点、直线或平面作为 X 轴。";
CATMmuAxisSystem.AxisSystemPanel.DirectionSelectorList2.LongHelp="选择点、直线或平面作为 Y 轴。";
CATMmuAxisSystem.AxisSystemPanel.DirectionSelectorList3.LongHelp="选择点、直线或平面作为 Z 轴。";

CATMmuAxisSystem.AxisSystemPanel.OriginCoordPanel.Title="原点";
CATMmuAxisSystem.AxisSystemPanel.OriginCoordPanel.LongHelp="指定原点坐标。";

CATMmuAxisSystem.AxisSystemPanel.XAxisCoordPanel.Title="X 轴";
CATMmuAxisSystem.AxisSystemPanel.YAxisCoordPanel.Title="Y 轴";
CATMmuAxisSystem.AxisSystemPanel.ZAxisCoordPanel.Title="Z 轴";

CATMmuAxisSystem.AxisSystemPanel.XAxisCoordPanel.LongHelp="为 X 轴指定坐标。";
CATMmuAxisSystem.AxisSystemPanel.YAxisCoordPanel.LongHelp="为 Y 轴指定坐标。";
CATMmuAxisSystem.AxisSystemPanel.ZAxisCoordPanel.LongHelp="为 Z 轴指定坐标。";

CATMmuAxisSystem.AxisSystemPanel.XAxisRotationPanel.Title="X 轴旋转";
CATMmuAxisSystem.AxisSystemPanel.YAxisRotationPanel.Title="Y 轴旋转";
CATMmuAxisSystem.AxisSystemPanel.ZAxisRotationPanel.Title="Z 轴旋转";

CATMmuAxisSystem.AxisSystemPanel.Coordinates="坐标";
CATMmuAxisSystem.AxisSystemPanel.CoordinatesEtc="坐标...";
CATMmuAxisSystem.AxisSystemPanel.NoSelection="无选择";
CATMmuAxisSystem.AxisSystemPanel.Rotation="旋转...";

CATMmuAxisSystem.AxisSystemPanel.Reverse="反转";
CATMmuAxisSystem.AxisSystemPanel.Reverse0.LongHelp="选择按钮反转指定的 X 方向。";
CATMmuAxisSystem.AxisSystemPanel.Reverse1.LongHelp="选择按钮反转指定的 Y 方向。";
CATMmuAxisSystem.AxisSystemPanel.Reverse2.LongHelp="选择按钮反转指定的 Z 方向。";

// indispensable de laisser les blancs sinon "not orthogonal" s'affiche tronque 
CATMmuAxisSystem.AxisSystemPanel.Direct="右手坐标系";
CATMmuAxisSystem.AxisSystemPanel.Indirect="左手坐标系";
CATMmuAxisSystem.AxisSystemPanel.NotOrthogonal="非正交";

CATMmuAxisSystem.AxisSystemPanel.Current.Title="当前";
CATMmuAxisSystem.AxisSystemPanel.Current.LongHelp="设置轴系作为当前的参考轴系。";
CATMmuAxisSystem.AxisSystemPanel.More.Title="更多...";
CATMmuAxisSystem.AxisSystemPanel.More="更多...";

CATMmuAxisSystem.AxisSystemPanel.UnderAxisSystemsNode.Title="在轴系节点下";
CATMmuAxisSystem.AxisSystemPanel.UnderAxisSystemsNode.ShortHelp="在轴系节点下或工作中对象后的几何集中插入";
CATMmuAxisSystem.AxisSystemPanel.UnderAxisSystemsNode.LongHelp="选项：将轴系插入结构树中的轴系节点下或插入几何集中的工作对象后";

CATMmuAxisSystem.AxisSystemPanel.X.Title="X =";
CATMmuAxisSystem.AxisSystemPanel.Y.Title="Y =";
CATMmuAxisSystem.AxisSystemPanel.Z.Title="Z =";

CATMmuAxisSystem.AxisSystemPanel.X.LongHelp="指定 X 坐标。";
CATMmuAxisSystem.AxisSystemPanel.Y.LongHelp="指定 Y 坐标。";
CATMmuAxisSystem.AxisSystemPanel.Z.LongHelp="指定 Z 坐标。";

CATMmuAxisSystem.AxisSystemPanel.Angle.Title="角度： ";
CATMmuAxisSystem.AxisSystemPanel.Angle1.Title="角度 1： ";
CATMmuAxisSystem.AxisSystemPanel.Angle2.Title="角度 2： ";
CATMmuAxisSystem.AxisSystemPanel.Angle3.Title="角度 3： ";

CATMmuAxisSystem.AxisSystemPanel.Angle1.LongHelp="修改角度 1 的值。";
CATMmuAxisSystem.AxisSystemPanel.Angle2.LongHelp="修改角度 2 的值。";
CATMmuAxisSystem.AxisSystemPanel.Angle3.LongHelp="修改角度 3 的值。";

CATMmuAxisSystem.AxisSystemPanel.LabelRefAxis.Title="轴系： ";
CATMmuAxisSystem.AxisSystemPanel.LabelSepar.Title="参考";


// 3DAxisSystem Errors
CATMmuAxisSystem.Error.NotOrthogonal.Title="不是正交轴系";
CATMmuAxisSystem.Error.NotOrthogonal.Message="轴系不是正交坐标系。请检查轴的坐标。";
CATMmuAxisSystem.Error.Unexpected.Title="意外错误";
CATMmuAxisSystem.Error.Unexpected.Message="出现意外错误";

CATMmuAxisSystem.Warning.SelectionIgnored.Title="选择可能被忽略";
CATMmuAxisSystem.Warning.SelectionIgnored.Message="选择可能被忽略，因为已定义其他轴。\n\n您可以使用上下文菜单中的“无选择”将其解除。";

CATMmuAxisSystem.Error.InvalidSelection.Message="选择无效。";

CATMmuAxisSystem.Error.AxisRotation.Message="未定义旋转轴和参考，或两者不兼容。";

NonAssociativeWarning="缺少 /P1 已使用其他输入计算为坐标。
/P1 /P2 未关联到其他输入。";
Axis="轴";
Origin="原点";
XAxis="X";
YAxis="Y";
ZAxis="Z";
Input="输入";
Inputs="输入";
Is="是";
Are="是";
NotUptoDateErrorTitle="检测到错误";
NotUptoDateError="所选元素 /p1 不是最新。\n输入元素必须是最新。\n请更新此元素。";
