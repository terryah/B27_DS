//=============================================================================
// COPYRIGHT DASSAULT SYSTEMES 1999
//=============================================================================
//
// Match Surface Panel :
//   Resource file for NLS purpose.
//
//=============================================================================
//
// Implementation Notes :
//
//=============================================================================
// Feb. 99   Creation                                   Jean-Michel PLOUHINEC
//=============================================================================

Title="匹配曲面";
TypeOfMatchFrame.Title="类型";
TypeOfMatchCombo.Bas="分析";
TypeOfMatchCombo.Adv="近似";
TypeOfMatchCombo.Aut="自动";
More.Title="更多 >>";
Less.Title="更少 <<";

InformationsFrame.NbPatches.Title="补面数： ";
InformationsFrame.NbPatch.LongHelp=
"显示要匹配的曲面
的补面数。";

InformationsFrame.Orders.Title="阶次： ";
InformationsFrame.Order.LongHelp=
"显示要匹配的曲面
的阶次。";

InformationsFrame.Types.Title="类型： ";
InformationsFrame.Type.LongHelp="显示匹配类型。";

InformationsFrame.Deltas.Title="增量： ";
InformationsFrame.Delta.LongHelp="显示与初始曲面的偏差。";

LocalTangent.Context.User.Title="用户";
LocalTangent.Context.Initial.Title="初始";
LocalTangent.Context.Reference.Title="目标";
LocalTangent.Context.Fix.Title="固定";
LocalTangent.Context.User.ShortTitle="用户";
LocalTangent.Context.Initial.ShortTitle="初始";
LocalTangent.Context.Reference.ShortTitle="目标";
LocalTangent.Context.Fix.ShortTitle="固定";

DegreeAjustment.Auto.MenuTitle="自动";
DegreeAjustment.Auto.DisplayedTitle="自动";

InformationsFrame.Title="信息";
OptionsFrame.Title="选项";
MoreLessFrame.DisplayFrame.Title="显示";

OptionsFrame.Projection.Title="投影终点";
OptionsFrame.Projection.ShortHelp="投影终点";
OptionsFrame.Projection.LongHelp=
"投影目标曲线上的
边界终点。";

OptionsFrame.Planar.Title="在主轴上移动";
OptionsFrame.Planar.ShortHelp="沿指南针主轴移动控制点";
OptionsFrame.Planar.LongHelp=
"约束控制点，使其
仅沿指南针
主轴移动。";

OptionsFrame.ProjectEdge.Title="投影边界";
OptionsFrame.ProjectEdge.ShortHelp="投影边界";
OptionsFrame.ProjectEdge.LongHelp=
"投影目标面上的
边界。";

OptionsFrame.Diffusion.Title="扩散";
OptionsFrame.Diffusion.ShortHelp="拓展变形";
OptionsFrame.Diffusion.LongHelp=
"沿横向方向
拓展变形。";

MoreLessFrame.DisplayFrame.ContinuityConnection.Title="连续连接";
MoreLessFrame.DisplayFrame.ContinuityConnection.ShortHelp="显示连续类型";
MoreLessFrame.DisplayFrame.ContinuityConnection.LongHelp="显示连续类型。";

MoreLessFrame.DisplayFrame.TensionLocalTangents.Title="全局张度和局部切线";
MoreLessFrame.DisplayFrame.TensionLocalTangents.ShortHelp="显示全局张度和局部切线";
MoreLessFrame.DisplayFrame.TensionLocalTangents.LongHelp=
"显示全局张度
和局部切线操作器。";

MoreLessFrame.DisplayFrame.LightCCDegree.Title="快速连接检查器";
MoreLessFrame.DisplayFrame.LightCCDegree.ShortHelp="显示最大偏差";
MoreLessFrame.DisplayFrame.LightCCDegree.LongHelp=
"显示曲面之间的
最大偏差。";

MoreLessFrame.DisplayFrame.CouplingPoints.Title="耦合点";
MoreLessFrame.DisplayFrame.CouplingPoints.ShortHelp="显示重新限定点操作器";
MoreLessFrame.DisplayFrame.CouplingPoints.LongHelp="显示重新限定点操作器。";

MoreLessFrame.DisplayFrame.CtrlPts.Title="控制点";
MoreLessFrame.DisplayFrame.CtrlPts.ShortHelp="显示控制点";
MoreLessFrame.DisplayFrame.CtrlPts.LongHelp=
"显示要匹配的曲面上
的控制点，
从而可以修改曲面。";
