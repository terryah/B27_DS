//=============================================================================
// COPYRIGHT DASSAULT SYSTEMES  2005
//=============================================================================
//
// CATStTopoFilletDialog: Resource file for NLS purpose related to TopoFillet command
//
//=============================================================================
//
// Implementation Notes:
//
//=============================================================================
// March 05   Creation                                    Ritesh Kanetkar
//=============================================================================
DialogBoxTitle="样式圆角";
TabCont.GeneralTab.Title="圆角输入";
TabCont.ApproxTab.Title="近似值";

TabCont.GeneralTab.MainFrame.ContinuityFrame.Title="公差";
TabCont.GeneralTab.MainFrame.RadiusFrame.Title="半径";
TabCont.GeneralTab.MainFrame.ArcTypeFrame.Title="弧类型";
TabCont.GeneralTab.MainFrame.ParameterFrame.Title="参数";
TabCont.GeneralTab.MainFrame.ResultFrame.Title="结果";
TabCont.GeneralTab.MainFrame.OptionFrame.Title="选项";

TabCont.GeneralTab.MainFrame.ParameterFrame.ParameterCombo.PDefault.Title="默认";
TabCont.GeneralTab.MainFrame.ParameterFrame.ParameterCombo.PPatch1.Title="补面 1";
TabCont.GeneralTab.MainFrame.ParameterFrame.ParameterCombo.PPatch2.Title="补面 2";
TabCont.GeneralTab.MainFrame.ParameterFrame.ParameterCombo.PAverage.Title="平均值";
TabCont.GeneralTab.MainFrame.ParameterFrame.ParameterCombo.PBlend.Title="桥接";
TabCont.GeneralTab.MainFrame.ParameterFrame.ParameterCombo.PChordal.Title="弦";

TabCont.GeneralTab.MainFrame.ResultFrame.TrimCombo.TrimFace.Title="修剪面";
TabCont.GeneralTab.MainFrame.ResultFrame.TrimCombo.TrimApprox.Title="近似值";

TabCont.GeneralTab.MainFrame.ResultFrame.SmallSurCombo.SSStitch.Title="缝合";
TabCont.GeneralTab.MainFrame.ResultFrame.SmallSurCombo.SSRip.Title="裂缝";

TabCont.GeneralTab.MainFrame.ContinuityFrame.G0Radio.ShortHelp="G0";
TabCont.GeneralTab.MainFrame.ContinuityFrame.G0Radio.LongHelp="圆角和输入元素之间的 G0 连续";
TabCont.GeneralTab.MainFrame.ContinuityFrame.G1Radio.ShortHelp="G1";
TabCont.GeneralTab.MainFrame.ContinuityFrame.G1Radio.LongHelp="圆角和输入元素之间的 G1 连续";
TabCont.GeneralTab.MainFrame.ContinuityFrame.G2Radio.ShortHelp="G2";
TabCont.GeneralTab.MainFrame.ContinuityFrame.G2Radio.LongHelp="圆角和输入元素之间的 G2 连续";
TabCont.GeneralTab.MainFrame.ContinuityFrame.G3Radio.ShortHelp="G3";
TabCont.GeneralTab.MainFrame.ContinuityFrame.G3Radio.LongHelp="圆角和输入元素之间的 G3 连续";


TabCont.GeneralTab.MainFrame.RadiusFrame.Radius.ShortHelp="固定半径";
TabCont.GeneralTab.MainFrame.RadiusFrame.Radius.LongHelp="圆角的固定半径";
TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadFlag.ShortHelp="最小半径";
TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadFlag.LongHelp="打开/关闭最小半径";
TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadius.ShortHelp="最小半径值";
TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadius.LongHelp="最小半径值";
TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadRelativeFlag.ShortHelp="相对半径";
TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadRelativeFlag.LongHelp="打开/关闭相对半径";
TabCont.GeneralTab.MainFrame.RadiusFrame.VariableRadChkBtn.ShortHelp="可变/部分圆角";
TabCont.GeneralTab.MainFrame.RadiusFrame.VariableRadChkBtn.LongHelp="显示用于可变/部分圆角的操作器";

TabCont.GeneralTab.MainFrame.ArcTypeFrame.BlendRadio.ShortHelp="桥接";
TabCont.GeneralTab.MainFrame.ArcTypeFrame.BlendRadio.LongHelp="在输入元素间创建桥接曲面";
TabCont.GeneralTab.MainFrame.ArcTypeFrame.ApproxRadio.ShortHelp="近似值";
TabCont.GeneralTab.MainFrame.ArcTypeFrame.ApproxRadio.LongHelp="在输入元素间创建圆形贝赛尔近似值";
TabCont.GeneralTab.MainFrame.ArcTypeFrame.ExactRadio.ShortHelp="精确值";
TabCont.GeneralTab.MainFrame.ArcTypeFrame.ExactRadio.LongHelp="使用真圆形截面创建有理曲面";

TabCont.GeneralTab.MainFrame.ParameterFrame.ParameterCombo.ShortHelp="参数";
TabCont.GeneralTab.MainFrame.ParameterFrame.ParameterCombo.LongHelp=" 为圆角曲面的参数化提供了不同选项";

TabCont.GeneralTab.MainFrame.ResultFrame.TrimSwitch.ShortHelp="修剪结果";
TabCont.GeneralTab.MainFrame.ResultFrame.TrimSwitch.LongHelp="打开/关闭修剪结果选项";
TabCont.GeneralTab.MainFrame.ResultFrame.TrimCombo.ShortHelp="修剪面/修剪近似值";
TabCont.GeneralTab.MainFrame.ResultFrame.TrimCombo.LongHelp="修剪结果选项的类型。修剪面 - 结果为面
修剪近似值 - 结果不为面";
TabCont.GeneralTab.MainFrame.ResultFrame.Extrapol.ShortHelp="外插延伸";
TabCont.GeneralTab.MainFrame.ResultFrame.Extrapol.LongHelp="对结果圆角曲面进行外插延伸";
TabCont.GeneralTab.MainFrame.ResultFrame.SmallSur.ShortHelp="小曲面";
TabCont.GeneralTab.MainFrame.ResultFrame.SmallSur.LongHelp="如果两个输入集都有多个曲面，且这些曲面的边界
不完全在同一位置，则结果为一个（非常）小的曲面。它的“打开/关闭”模式";
TabCont.GeneralTab.MainFrame.ResultFrame.SmallSurCombo.ShortHelp="缝合/裂缝";
TabCont.GeneralTab.MainFrame.ResultFrame.SmallSurCombo.LongHelp="缝合：可以将这个小曲面与相邻的一个曲面缝合
裂缝：将这个小曲面（沿对角方向）分为两个曲面";
TabCont.GeneralTab.MainFrame.ResultFrame.SmallSurfaceTolerancee.ShortHelp="小曲面公差";
TabCont.GeneralTab.MainFrame.ResultFrame.SmallSurfaceTolerancee.LongHelp="用于创建小曲面的公差";

TabCont.GeneralTab.MainFrame.OptionFrame.TrimBase.ShortHelp="修剪输入";
TabCont.GeneralTab.MainFrame.OptionFrame.TrimBase.LongHelp="修剪圆角的输入元素";
TabCont.GeneralTab.MainFrame.OptionFrame.ChordalFillet.ShortHelp="弦圆角";
TabCont.GeneralTab.MainFrame.OptionFrame.ChordalFillet.LongHelp="沿滚球方向的圆角边线之间的距离为弦距";
TabCont.GeneralTab.MainFrame.OptionFrame.CtrlPts.ShortHelp="控制点";
TabCont.GeneralTab.MainFrame.OptionFrame.CtrlPts.LongHelp="启用圆角曲面的外形修改";
TabCont.GeneralTab.MainFrame.OptionFrame.TrueMinimalRadius.ShortHelp="最小真值";
TabCont.GeneralTab.MainFrame.OptionFrame.TrueMinimalRadius.LongHelp="已控制最小半径";
TabCont.GeneralTab.MainFrame.OptionFrame.IndependentApproximation.ShortHelp="独立近似值";
TabCont.GeneralTab.MainFrame.OptionFrame.IndependentApproximation.LongHelp="内部独立近似值";
// New UI

MainOuterFrameTabs.TabCont.GeneralTab.Title="选项";
// MainOuterFrameTabs.TabCont.ApproxTab.Title = "Advanced"; // WAP 08:08:26 R19SP4 HL2 - Nls entry commented.


// Continuity Frame
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContinuityFrame.Title="连续";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContinuityFrame.G0Radio.ShortHelp="G0";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContinuityFrame.G0Radio.LongHelp="圆角和输入元素之间的 G0 连续";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContinuityFrame.G1Radio.ShortHelp="G1";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContinuityFrame.G1Radio.LongHelp="圆角和输入元素之间的 G1 连续";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContinuityFrame.G2Radio.ShortHelp="G2";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContinuityFrame.G2Radio.LongHelp="圆角和输入元素之间的 G2 连续";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContinuityFrame.G3Radio.ShortHelp="G3";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContinuityFrame.G3Radio.LongHelp="圆角和输入元素之间的 G3 连续";

// Radius Frame
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.Title="半径参数";
//MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.RadiusLabel.Title = "Radius :"; // WAP 08:11:25 R19SP3 HL
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.RadiusLabel.ShortHelp="固定半径";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.RadiusLabel.LongHelp="圆角的固定半径";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.Radius.ShortHelp="固定半径";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.Radius.LongHelp="圆角的固定半径";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadFlag.Title="最小半径：";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadFlag.ShortHelp="最小半径";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadFlag.LongHelp="打开/关闭最小半径";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadius.ShortHelp="最小半径值";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadius.LongHelp="最小半径值";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadRelativeFlag.ShortHelp="相对半径";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadRelativeFlag.LongHelp="打开/关闭相对半径";

// Arc Type 
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.ArcTypeFrame.Title="弧类型";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.ArcTypeFrame.BlendRadio.ShortHelp="桥接";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.ArcTypeFrame.BlendRadio.LongHelp="在输入元素间创建桥接曲面";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.ArcTypeFrame.ApproxRadio.ShortHelp="近似值";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.ArcTypeFrame.ApproxRadio.LongHelp="在输入元素间创建圆形贝赛尔近似值";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.ArcTypeFrame.ExactRadio.ShortHelp="精确值";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.ArcTypeFrame.ExactRadio.LongHelp="使用真圆形截面创建有理曲面";


MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.Title="结果";

// Simplification Frame
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.Title="简化";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.SmallSurStich.Title="缝合";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.SmallSurStich.ShortHelp="小曲面 - 缝合";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.SmallSurStich.LongHelp="缝合：可以将这个小曲面与相邻的一个曲面缝合";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.SmallSurRip.Title="裂缝";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.SmallSurRip.ShortHelp="小曲面 - 裂缝";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.SmallSurRip.LongHelp="裂缝：将这个小曲面（沿对角方向）分为两个曲面";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.SmallSurfaceTolerancee.ShortHelp="小曲面公差";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.SmallSurfaceTolerancee.LongHelp="用于创建小曲面的公差";

// Relimitation Frame
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.RelimitationFrame.Title="重新限定";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.RelimitationFrame.TrimFace.Title="修剪面";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.RelimitationFrame.TrimFace.ShortHelp="修剪面";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.RelimitationFrame.TrimFace.LongHelp="结果为面";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.RelimitationFrame.TrimApprox.Title="修剪近似值";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.RelimitationFrame.TrimApprox.ShortHelp="修剪近似值";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.RelimitationFrame.TrimApprox.LongHelp="结果不为面";
// For R18SP3 UI
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.Title="几何图形";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.Extrapol1.ShortHelp="外插延伸第 1 面";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.Extrapol1.LongHelp="将结果外插延伸到第 1 面上";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.Extrapol2.ShortHelp="外插延伸第 2 面";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.Extrapol2.LongHelp="将结果外插延伸到第 2 面上";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.Relimit1.ShortHelp="重新限定第 1 面";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.Relimit1.LongHelp="将结果重新限定到第 1 面上";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.Relimit2.ShortHelp="重新限定第 2 面";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.Relimit2.LongHelp="将结果重新限定到第 2 面上";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.TrimFace.Title="修剪面";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.TrimFace.ShortHelp="修剪面";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.TrimFace.LongHelp="结果为面";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.TrimApprox.Title="修剪近似值";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.TrimApprox.ShortHelp="修剪近似值";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.TrimApprox.LongHelp="结果不为面";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.Merge.ShortHelp="连接";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.Merge.LongHelp="将多单元圆角筋链接到单一单元结果（仅限于单元间的 G2 连续）";

// Geometry Frame
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.GeometryFrame.Title="几何图形";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.GeometryFrame.Extrapol.Title="外插延伸";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.GeometryFrame.Extrapol.ShortHelp="外插延伸";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.GeometryFrame.Extrapol.LongHelp="对结果圆角曲面进行外插延伸";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.GeometryFrame.Merge.ShortHelp="连接";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.GeometryFrame.Merge.LongHelp="将多单元圆角筋链接到单一单元结果（仅限于单元间的 G2 连续）";
//--------------------------------------

// Options Frame
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.Title="圆角类型";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.VariableRadChkBtn.ShortHelp="可变/部分圆角";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.VariableRadChkBtn.LongHelp="显示用于可变/部分圆角的操作器";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.TrimBase.ShortHelp="修剪输入";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.TrimBase.LongHelp="修剪圆角的输入元素";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.ChordalFillet.ShortHelp="弦圆角";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.ChordalFillet.LongHelp="沿滚球方向的圆角边线之间的距离为弦距";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.CtrlPts.ShortHelp="控制点";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.CtrlPts.LongHelp="启用圆角曲面的外形修改";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.TrueMinimalRadius.ShortHelp="最小真值";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.TrueMinimalRadius.LongHelp="已控制最小半径";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.IndependentApproximation.ShortHelp="独立近似值";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.IndependentApproximation.LongHelp="内部独立近似值";


// Surface-1 Curv-1
MainOuterFrame.Surface1Curve1Frame.LabelCurve1.Title="曲线：";
MainOuterFrame.Surface1Curve1Frame.LabelCurve1.ShortHelp="选择曲线集 1";
MainOuterFrame.Surface1Curve1Frame.LabelCurve1.LongHelp="选择用于圆角操作的第一组曲线";
MainOuterFrame.Surface1Curve1Frame.TrimBase1.ShortHelp="修剪支持面 1";
MainOuterFrame.Surface1Curve1Frame.TrimBase1.LongHelp="在圆角的支持面集 1 中修剪输入元素（曲面）。";

// Surface-2 Curv-2
MainOuterFrame.Surface2Curve2Frame.Title="支持面 2";
MainOuterFrame.Surface2Curve2Frame.ShortHelp="选择曲面和曲线集 2";
MainOuterFrame.Surface2Curve2Frame.LongHelp="选择用于圆角操作的第二组曲面和曲线";
MainOuterFrame.Surface2Curve2Frame.LabelSurface2.Title="曲面：";
MainOuterFrame.Surface2Curve2Frame.LabelSurface2.ShortHelp="选择曲面集 2";
MainOuterFrame.Surface2Curve2Frame.LabelSurface2.LongHelp="选择用于圆角操作的第二组曲面";
MainOuterFrame.Surface2Curve2Frame.LabelCurve2.Title="曲线：";
MainOuterFrame.Surface2Curve2Frame.LabelCurve2.ShortHelp="选择曲线集 2";
MainOuterFrame.Surface2Curve2Frame.LabelCurve2.LongHelp="选择用于圆角操作的第二组曲线";
MainOuterFrame.Surface2Curve2Frame.TrimBase2.ShortHelp="修剪支持面 2";
MainOuterFrame.Surface2Curve2Frame.TrimBase2.LongHelp="修剪圆角支持面 2 集中的输入元素（曲面）。";

// Point-Spine
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.SpinePointFrame.SpineSelector.ShortHelp="选择脊线集";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.SpinePointFrame.SpineSelector.LongHelp="选择用于圆角操作的脊线";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.SpinePointFrame.PointSelector.ShortHelp="在脊线上选择点";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.SpinePointFrame.PointSelector.LongHelp="选择脊线上的点，用于圆角操作";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.SpinePointFrame.LabelSpine.Title="脊线：";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.SpinePointFrame.LabelSpine.ShortHelp="选择脊线集";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.SpinePointFrame.LabelSpine.LongHelp="选择用于圆角操作的脊线";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.SpinePointFrame.LabelPoint.Title="点：";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.SpinePointFrame.LabelPoint.ShortHelp="在脊线上选择点";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.SpinePointFrame.LabelPoint.LongHelp="选择脊线上的点，用于圆角操作";

// Parameters
MainOuterFrameTabs.TabCont.ApproxTab.ParameterFrame.Title="参数";
MainOuterFrameTabs.TabCont.ApproxTab.ParameterFrame.ParameterCombo.ShortHelp="参数";
MainOuterFrameTabs.TabCont.ApproxTab.ParameterFrame.ParameterCombo.LongHelp=" 为圆角曲面的参数化提供了不同选项";
MainOuterFrameTabs.TabCont.ApproxTab.ParameterFrame.ParameterCombo.PDefault.Title="默认";
MainOuterFrameTabs.TabCont.ApproxTab.ParameterFrame.ParameterCombo.PPatch1.Title="补面 1";
MainOuterFrameTabs.TabCont.ApproxTab.ParameterFrame.ParameterCombo.PPatch2.Title="补面 2";
MainOuterFrameTabs.TabCont.ApproxTab.ParameterFrame.ParameterCombo.PAverage.Title="平均值";
MainOuterFrameTabs.TabCont.ApproxTab.ParameterFrame.ParameterCombo.PBlend.Title="桥接";
MainOuterFrameTabs.TabCont.ApproxTab.ParameterFrame.ParameterCombo.PChordal.Title="弦";

// Independent Approximation
MainOuterFrameTabs.TabCont.ApproxTab.IndependentApproximation.Title="独立近似值";
MainOuterFrameTabs.TabCont.ApproxTab.IndependentApproximation.ShortHelp="独立近似值";
MainOuterFrameTabs.TabCont.ApproxTab.IndependentApproximation.LongHelp="内部独立近似值";

Surface1BagTitle.Message="选择曲面 1";
Curve1BagTitle.Message="选择曲线 1";
Surface2BagTitle.Message="选择曲面 2";
Curve2BagTitle.Message="选择曲线 2";
SpineBagTitle.Message="选择脊线";
PointOnSpineBagTitle.Message="在脊线上选择点";

//Propagate contextual menu
Propagate.Message="切线延伸"; //AV7 11:01:17

MaxDevOptionsFrame.MaxDevDisplayOptionsFrame.Title="显示偏差";
MaxDevOptionsFrame.MaxDevDisplayOptionsFrame.DevBetFilletCellsChkB.ShortHelp="圆角单元之间的连接";
MaxDevOptionsFrame.MaxDevDisplayOptionsFrame.DevBetFilletCellsChkB.LongHelp="只显示圆角筋单元之间的连接";
MaxDevOptionsFrame.MaxDevDisplayOptionsFrame.DevBetFilletAndSupportChkB.ShortHelp="圆角筋和支持面之间的连接";
MaxDevOptionsFrame.MaxDevDisplayOptionsFrame.DevBetFilletAndSupportChkB.LongHelp="显示圆角筋和支持面之间的连接";
MaxDevOptionsFrame.MaxDevValueFrame.Title="最大偏差";
MaxDevOptionsFrame.MaxDevValueFrame.LongHelp="显示每个连续的最大值和最小值";
MaxDevOptionsFrame.MaxDevValueFrame.G0TitleLabel.Title="G0：";
MaxDevOptionsFrame.MaxDevValueFrame.G0Label="----  ";
MaxDevOptionsFrame.MaxDevValueFrame.G1TitleLabel.Title="G1：";
MaxDevOptionsFrame.MaxDevValueFrame.G2TitleLabel.Title="G2：";
MaxDevOptionsFrame.MaxDevValueFrame.G3TitleLabel.Title="G3：";

// Start WAP 08:08:26 R19SP4 HL1 AND HL2

NewApproxTab.Message="近似值";
ApproxTab.Message="高级";
OutputResultFrame.Title="输出结果";
OutputResultFrame.MaxOrderTitleLabel.Title="最大阶次：";
OutputResultFrame.MaxSegTitleLabel.Title="每个单元的最大线段数：";
OutputResultFrame.OrdUTitleLabel.Title="U：";
OutputResultFrame.OrdVTitleLabel.Title="V：";
OutputResultFrame.SegUTitleLabel.Title="U：";
OutputResultFrame.SegVTitleLabel.Title="V：";
//OutputResultFrame.NbOfCellsTitleLabel.LongHelp = "Displays the Number of cells in entire fillet result created";
OutputResultFrame.MaxOrderTitleLabel.ShortHelp="显示 U 方向和 V 方向上的最大阶次";
//OutputResultFrame.MaxOrderTitleLabel.LongHelp = "Displays the maximum order of the created entire fillet result in both U and V directions"; 
OutputResultFrame.MaxSegTitleLabel.ShortHelp="显示 U 方向和 V 方向上每个单元的最大线段数";
//OutputResultFrame.MaxSegTitleLabel.LongHelp = "Displays the maximum number of segments per cell of the created entire fillet result in both U and V directions"; 
// End WAP 08:08:26	

// Start WAP 08:11:25 R19SP3 HL
LengthLabel.Message="长度：";
RadiusLabel.Message="半径：";
// End WAP 08:11:25

// Start WAP 08:12:02 R19SP4 HL6
//Spine - Point Frame	
MainOuterFrame.SpinePointFrame.LabelSpine.Title="脊线：";
MainOuterFrame.SpinePointFrame.LabelPoint.Title="点：";
MainOuterFrame.SpinePointFrame.SpineSelector.ShortHelp="选择脊线集";
MainOuterFrame.SpinePointFrame.SpineSelector.LongHelp="选择用于圆角操作的脊线";
MainOuterFrame.SpinePointFrame.PointSelector.ShortHelp="在脊线上选择点";
MainOuterFrame.SpinePointFrame.PointSelector.LongHelp="选择脊线上的点，用于圆角操作";

// Geometry Frame
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.LabelExtrapolRelimit1.Title="外插延伸/重新限定第 1 面";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.LabelExtrapolRelimit1.ShortHelp="外插延伸/重新限定第 1 面";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.LabelExtrapolRelimit1.LongHelp="将结果外插延伸/重新限定到第 1 面上";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.LabelExtrapolRelimit2.Title="外插延伸/重新限定第 2 面";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.LabelExtrapolRelimit2.ShortHelp="外插延伸/重新限定第 2 面";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.LabelExtrapolRelimit2.LongHelp="将结果外插延伸/重新限定到第 2 面上";

// Result Frame
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.LogicalMerge.Title="逻辑接合";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.LogicalMerge.ShortHelp="逻辑接合";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.LogicalMerge.LongHelp="使用 0.1 mm 的固定公差装配结果（圆角筋、修剪输入曲面）";

// Simplification Frame
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.Merge.Title="连接";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.Merge.ShortHelp="连接";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.Merge.LongHelp="将多单元圆角筋链接到单一单元结果（仅限于单元间的 G2 连续）";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.LogicalMerge.Title="逻辑接合";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.LogicalMerge.ShortHelp="逻辑接合";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.LogicalMerge.LongHelp="使用 0.1 mm 的固定公差装配结果（圆角筋、修剪输入曲面）";
// End WAP 08:12:02

// Start WAP 09:01:23 R19SP4 HL5
Curve1SelectorShortHelp.Message="选择曲线集 1";
Curve1SelectorLongHelp.Message="选择用于圆角操作的第一组曲线";
Curve1SelStateShortHelp.Message="启用曲线集 1";
Curve1SelStateLongHelp.Message="在曲线集 1 中启用选择。";

Curve2SelectorShortHelp.Message="选择曲线集 2";
Curve2SelectorLongHelp.Message="选择用于圆角操作的第二组曲线";
Curve2SelStateShortHelp.Message="启用曲线集 2";
Curve2SelStateLongHelp.Message="启用曲线 2 集中的选择。";


EdgesToExtrapol1SelectorShortHelp.Message="选择要外插延伸的边线 1";
EdgesToExtrapol1SelectorLongHelp.Message="选择要外插延伸的第一组边线，以进行可变/部分圆角操作";
EdgesToExtrapol1SelStateShortHelp.Message="启用要外插延伸的边线 1";
EdgesToExtrapol1SelStateLongHelp.Message="启用所选的要外插延伸的第一组边线，以进行可变/部分圆角操作";
EdgesToExtrapol2SelectorShortHelp.Message="选择要外插延伸的边线 2";
EdgesToExtrapol2SelectorLongHelp.Message="选择要外插延伸的第二组边线，以进行可变/部分圆角操作";
EdgesToExtrapol2SelStateShortHelp.Message="启用要外插延伸的边线 2";
EdgesToExtrapol2SelStateLongHelp.Message="启用所选的要外插延伸的第二组边线，以进行可变/部分圆角操作";

// End WAP 09:01:23

// Start WAP 08:12:02 R19SP5
// Input Parameter Frame
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.InputParameterFrame.Title="输入参数";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.InputParameterFrame.LabelMergeDist.Title="合并距离：";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.InputParameterFrame.SpinnerMergeDist.ShortHelp="合并距离";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.InputParameterFrame.SpinnerMergeDist.LongHelp="设置多单元圆角的合并距离";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.InputParameterFrame.TolerantLaydownCheck.ShortHelp="容差放置";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.InputParameterFrame.TolerantLaydownCheck.LongHelp="使用容差放置";
TolerantLaydown.Title="容差放置";
// End WAP 08:12:02					   

MainOuterFrame.Surface2Curve2Frame.EdgeFilletMode.Title="圆角类型";
MainOuterFrame.Surface2Curve2Frame.EdgeFilletMode.ShortHelp="圆角类型";
MainOuterFrame.Surface2Curve2Frame.EdgeFilletMode.LongHelp="选择圆角操作的类型。支持面圆角/倒圆角";

EdgeFilletModeLabel.Message="要圆角化的对象";
SupportFilletMode.Message="支持面 1";

Surface1Curve1FrameShortHelp.Message="选择曲面和曲线集 1";
Surface1Curve1FrameLongHelp.Message="选择用于圆角操作的第一组曲面和曲线";
ObjectsToFilletFrameShortHelp.Message="选择要圆角化的对象";
ObjectsToFilletFrameLongHelp.Message="选择用于圆角操作的锐边/具有锐边的面";

Surface1Title.Message="曲面：";
Surface1ShortHelp.Message="选择曲面集 1";
Surface1LongHelp.Message="选择用于圆角操作的第一组曲面";

ObjectToFilletTitle.Message="要圆角化的对象：";
ObjectToFilletShortHelp.Message="选择要圆角化的对象";
ObjectToFilletLongHelp.Message="选择用于圆角操作的锐边/具有锐边的面";

Surface1SelectorShortHelp.Message="选择曲面集 1";
Surface1SelectorLongHelp.Message="选择用于圆角操作的第一组曲面";
ObjectsToFilletSelectorShortHelp.Message="选择要圆角化的对象";
ObjectsToFilletSelectorLongHelp.Message="选择用于圆角操作的锐边/具有锐边的面";

// Start WAP 10:06:09 - R19SP3HF
OutputResultFrame.NewNbOfCellsTitleLabel.Title="单元 数       ：";
OutputResultFrame.NewNbOfCellsTitleLabel.ShortHelp="显示单元数";
OutputResultFrame.NbOfDomainsTitleLabel.Title="域 数：";
OutputResultFrame.NbOfDomainsTitleLabel.ShortHelp="显示域数";
OutputResultFrame.NbOfDomainsTitleLabel.LongHelp="显示已创建的圆角筋中的域数";
// End WAP 10:06:09

// Start WAP 10:12:21
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContiTolernaceFrame.Title="连续公差";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContiTolernaceFrame.LabelG0Tol.Title="G0：";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContiTolernaceFrame.LabelG1Tol.Title="G1：";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContiTolernaceFrame.LabelG2Tol.Title="G2：";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContiTolernaceFrame.LabelG3Tol.Title="G3：";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContiTolernaceFrame.CkeG0TolFrame.ShortHelp="更改参数值：所需的 G0 公差值。";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContiTolernaceFrame.CkeG0TolFrame.LongHelp="定义 G0 公差值";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContiTolernaceFrame.CkeG1TolFrame.ShortHelp="更改参数值：所需的 G1 公差值。";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContiTolernaceFrame.CkeG1TolFrame.LongHelp="定义 G1 公差值";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContiTolernaceFrame.CkeG2TolFrame.ShortHelp="更改参数值：所需的 G2 公差值。";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContiTolernaceFrame.CkeG2TolFrame.LongHelp="定义 G2 公差值";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContiTolernaceFrame.CkeG3TolFrame.ShortHelp="更改参数值：所需的 G3 公差值。";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContiTolernaceFrame.CkeG3TolFrame.LongHelp="定义 G3 公差值";
// End WAP 10:12:21

// Start WAP 11:06:27
NbOfCellsOnFilletRibbonLongHelp.Message="显示已创建的圆角筋中的单元数。";
MaxOrderOnFilletRibbonLongHelp.Message="显示 U 和 V 方向上已创建的圆角筋的最大阶次。";
MaxSegOnFilletRibbonLongHelp.Message="显示 U 和 V 方向上已创建的圆角筋的每个单元的最大线段数。";
NbOfCellsOnEntireFilletLongHelp.Message="显示整个已创建的圆角筋中的单元数。";
MaxOrderOnEntireFilletLongHelp.Message="显示 U 和 V 方向上整个已创建的圆角筋的最大阶次。";
MaxSegOnEntireFilletLongHelp.Message="显示 U 和 V 方向上整个已创建的圆角筋的每个单元的最大线段数。";
// End WAP 11:06:27

