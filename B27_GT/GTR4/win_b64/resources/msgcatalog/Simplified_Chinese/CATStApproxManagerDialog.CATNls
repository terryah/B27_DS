//=============================================================================
// COPYRIGHT DASSAULT SYSTEMES  1999
//=============================================================================
//
// CATStApproxManagerDialog: Resource file for NLS purpose related to ApproxManager ( ConverterWizard)  dialog
//
//=============================================================================
//
// Implementation Notes:
//
//=============================================================================
// Sept. 99   Creation                                   TRUONG
//=============================================================================
Title="转换器向导";

BMORE="更多 >>";
BLESS="更少 <<";
INFINITE="无限";

MainFrame.MainTolerFrame.TolerCheck.LongHelp  = "请设置转换公差值，否则系统将自动最小化偏差。";
MainFrame.MainTolerFrame.TolerCheck.ShortHelp  = "设置转换公差值。";
MainFrame.MainTolerFrame.Tolerance.ToleranceLabel.Title     = "公差";
MainFrame.MainTolerFrame.Tolerance.ToleranceLabel.LongHelp  = "设置转换公差值。";
MainFrame.MainTolerFrame.Tolerance.TolerSpinner.LongHelp  = "设置转换公差值。
（可以在“工具/选项/外形/自由样式”
中定义此偏差公差的最大值。）";

MainFrame.MainOrderFrame.OrdersCheck.LongHelp  = "请定义最大阶次值，否则系统将自动优化阶次。";
MainFrame.MainOrderFrame.OrdersCheck.ShortHelp  = "定义最大阶次值。";
MainFrame.MainOrderFrame.OrderFrame.Title      = "阶次";
MainFrame.MainOrderFrame.OrderFrame.LongHelp   = "定义 U 和 V 方向上的最大阶次值。
（可以在“工具/选项/外形/自由样式”中定义最大限制。）";
MainFrame.MainOrderFrame.OrderFrame.Priority1.Title     = "优先级";
MainFrame.MainOrderFrame.OrderFrame.Priority1.LongHelp  = "阶次的优先级";
MainFrame.MainOrderFrame.OrderFrame.Priority1.ShortHelp = "阶次的优先级";

MainFrame.MainOrderFrame.OrderFrame.OrderULabel.Title    = "沿 U";
MainFrame.MainOrderFrame.OrderFrame.OrderVLabel.Title    = "沿 V";

MainFrame.MainOrderFrame.OrderFrame.OrderUSpinner.ShortHelp   = "定义 U 方向上的最大阶次值。";
MainFrame.MainOrderFrame.OrderFrame.OrderVSpinner.ShortHelp   = "定义 V 方向上的最大阶次值。";

MainFrame.MainSegmentFrame.SegmentCheck.LongHelp   = "请定义最大线段数，否则系统将自动优化线段数。";
MainFrame.MainSegmentFrame.SegmentCheck.ShortHelp  = "定义最大线段数。";

MainFrame.MainSegmentFrame.Segmentation.Title      = "分割";

MainFrame.MainSegmentFrame.Segmentation.CheckSegmentFrame.Priority2.Title     = "优先级";
MainFrame.MainSegmentFrame.Segmentation.CheckSegmentFrame.Priority2.LongHelp  = "分割优先级";
MainFrame.MainSegmentFrame.Segmentation.CheckSegmentFrame.Priority2.ShortHelp = "分割优先级";

MainFrame.MainSegmentFrame.Segmentation.CheckSegmentFrame.SingleCheck.Title   = "单个";
MainFrame.MainSegmentFrame.Segmentation.CheckSegmentFrame.LongHelp   = "自动转化为单线段元素。";
MainFrame.MainSegmentFrame.Segmentation.CheckSegmentFrame.SingleCheck.ShortHelp  = "单线段。";

MainFrame.MainSegmentFrame.Segmentation.SegmentFrame.NbSegmentsUFrame.SegmentULabel.Title    = "沿 U";
MainFrame.MainSegmentFrame.Segmentation.SegmentFrame.NbSegmentsVFrame.SegmentVLabel.Title    = "沿 V";
MainFrame.MainSegmentFrame.Segmentation.SegmentFrame.NbSegmentsUFrame.LongHelp = "沿 U 定义最大线段数。";
MainFrame.MainSegmentFrame.Segmentation.SegmentFrame.NbSegmentsVFrame.LongHelp = "沿 V 定义最大线段数。";
MainFrame.MainSegmentFrame.Segmentation.SegmentFrame.NbSegmentsUFrame.SegmentUSpinner.ShortHelp = "沿 U 定义最大线段数。";
MainFrame.MainSegmentFrame.Segmentation.SegmentFrame.NbSegmentsVFrame.SegmentVSpinner.ShortHelp = "沿 V 定义最大线段数。";

MainFrame.3DConversionFrame.3DConversionRadioFrame.3DConversionRadio.LongHelp   = "可以将 2D 曲线（曲面上的曲线）转换为 3D 曲线。";
MainFrame.3DConversionFrame.3DConversionRadioFrame.3DConversionRadio.ShortHelp  = "3D 转换。";

MainFrame.3DConversionFrame.3DConversionRadioFrame.2DConversionRadio.LongHelp   = "2D 曲线（曲面上的曲线）的转换结果是 2D 曲线。";
MainFrame.3DConversionFrame.3DConversionRadioFrame.2DConversionRadio.ShortHelp  = "2D 转换。";

MainFrame.3DConversionFrame.KeepCheck.Title              = "保留";
MainFrame.3DConversionFrame.KeepCheck.LongHelp           = "创建新元素并保留初始元素。";
MainFrame.3DConversionFrame.KeepCheck.ShortHelp          = "创建新元素并保留初始元素。";

MoreFrame.DisplayFrame.Title                       ="显示" ; 
MoreFrame.DisplayFrame.InformationCheck.Title      ="信息";
MoreFrame.DisplayFrame.InformationCheck.LongHelp   =
"显示以下信息：偏差值、
序号和线段数。";
MoreFrame.DisplayFrame.InformationCheck.ShortHelp   =
"显示以下信息：偏差值、
序号和线段数。";

MoreFrame.DisplayFrame.ControlPointsCheck.Title    ="控制点";
MoreFrame.DisplayFrame.ControlPointsCheck.LongHelp ="显示元素的控制点。";
MoreFrame.DisplayFrame.ControlPointsCheck.ShortHelp ="显示元素的控制点。";

MoreFrame.MorerightFrame.MaxDevLabel.Title                ="最大偏差";
MoreFrame.MorerightFrame.MinCurvatureRadiusLabel.Title    ="最小半径"; 
MoreFrame.MorerightFrame.MaxCurvatureRadiusLabel.Title    ="最大半径"; 
MoreFrame.MorerightFrame.MaxDevLabel.LongHelp             ="原始元素与已转换元素之间的最大偏差。";
MoreFrame.MorerightFrame.MinCurvatureRadiusLabel.LongHelp ="最小曲率半径"; 
MoreFrame.MorerightFrame.MaxCurvatureRadiusLabel.LongHelp ="最大曲率半径"; 

MoreFrame.MoreRightFrame.MaxDev.AutoCheck.Title    = "自动应用";
MoreFrame.MoreRightFrame.MaxDev.AutoCheck.LongHelp    = 
"根据元素的新参数
重算并显示该元素。";
MoreFrame.MoreRightFrame.MaxDev.AutoCheck.ShortHelp     = 
"根据元素的新参数
重算并显示该元素。";

MoreFrame.ContinuityFrame.Title="连续" ; 
MoreFrame.ContinuityFrame.Point.Title="点" ; 
MoreFrame.ContinuityFrame.Tg.Title="相切" ; 
MoreFrame.ContinuityFrame.Crv.Title="曲率" ;
