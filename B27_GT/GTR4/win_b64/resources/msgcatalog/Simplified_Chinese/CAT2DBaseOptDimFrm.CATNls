//=====================================================================================
//                                     CNEXT - CXRn
//                          COPYRIGHT DASSAULT SYSTEMES 1999 
//-------------------------------------------------------------------------------------
// FILENAME    :    CATDrwDimensionFrame
// LOCATION    :    DraftingUI
// AUTHOR      :    fgx
// BUT         :    
// DATE        :    26.11.1999
//-------------------------------------------------------------------------------------
// DESCRIPTION :    Tools/Option du Drafting
//-------------------------------------------------------------------------------------
// COMMENTS    :    
//-------------------------------------------------------------------------------------
// MODIFICATIONS     user  date        purpose
//    HISTORY        ----  ----        -------
// 01			lgk 17.12.02 Revision
//=====================================================================================


frameDimSketch.HeaderFrame.Global.Title="创建尺寸 ";
frameDimSketch.HeaderFrame.Global.LongHelp="在创建过程中，定义尺寸的放置方式
以及尺寸的队列方式。";

frameDimSketch.IconAndOptionsFrame.OptionsFrame.checkDimSketch.Title="跟随光标（CTRL 切换）的尺寸";
frameDimSketch.IconAndOptionsFrame.OptionsFrame.checkDimSketch.LongHelp="尺寸跟随鼠标光标。
按“CTRL”键暂时激活或取消激活
该模式。";

frameDimSketch.IconAndOptionsFrame.OptionsFrame.checkDimLinePosRelative.Title="尺寸线和几何图形间的偏移恒定";
frameDimSketch.IconAndOptionsFrame.OptionsFrame.checkDimLinePosRelative.LongHelp="几何图形移动时，使尺寸线与几何图形之间保持
所定义的距离。";

frameDimSketch.IconAndOptionsFrame.OptionsFrame.frameLinePosValue.labelDimLinePosValue.Title="默认尺寸线/几何图形距离： ";
frameDimSketch.IconAndOptionsFrame.OptionsFrame.frameLinePosValue.labelDimLinePosValue.LongHelp="在创建和/或移动几何图形时，定义尺寸线
和几何图形之间的默认距离。";

frameDimSketch.IconAndOptionsFrame.OptionsFrame._checkDimLineUpEnds.Title="在排列中终止创建尺寸";
frameDimSketch.IconAndOptionsFrame.OptionsFrame._checkDimLineUpEnds.LongHelp="为排列选择其他尺寸时
终止创建尺寸流程。";

frameDimSketch.IconAndOptionsFrame.OptionsFrame.frameDimAssoc3D.pushAssoc3D.Title="3D 上的关联性...";

frameDimSketch.IconAndOptionsFrame.OptionsFrame.frm.checkDimDriveAuto.Title="创建驱动尺寸";
frameDimSketch.IconAndOptionsFrame.OptionsFrame.frm.checkDimDriveAuto.LongHelp="在 2D 几何图形和 2D 或 3D 生成的几何图形之间，创建
驱动尺寸";

frameDimSketch.IconAndOptionsFrame.OptionsFrame.frm.chkDetectChamfer.Title="检测倒角";
frameDimSketch.IconAndOptionsFrame.OptionsFrame.frm.chkDetectChamfer.LongHelp="检测倒角并创建适当的尺寸。";

frameDimSketch.IconAndOptionsFrame.OptionsFrame._frameDimCircle._labelDimCircle.Title="默认情况下，在圆上创建尺寸：";

frameDimSketch.IconAndOptionsFrame.OptionsFrame._frameDimCircle._frameDimCircleRadio._frameDimCircleRadiolocked._radioDimCircleCenter.Title="中心";
frameDimSketch.IconAndOptionsFrame.OptionsFrame._frameDimCircle._frameDimCircleRadio._frameDimCircleRadiolocked._radioDimCircleCenter.LongHelp="默认情况下，在圆心上创建尺寸。";

frameDimSketch.IconAndOptionsFrame.OptionsFrame._frameDimCircle._frameDimCircleRadio._frameDimCircleRadiolocked._radioDimCircleEdge.Title="边线";
frameDimSketch.IconAndOptionsFrame.OptionsFrame._frameDimCircle._frameDimCircleRadio._frameDimCircleRadiolocked._radioDimCircleEdge.LongHelp="默认情况下，在圆的边线上创建尺寸。";

_frameMove.HeaderFrame.Global.Title="移动";
_frameMove.HeaderFrame.Global.LongHelp="定义尺寸移动方式。
另请参阅“操作器”选项卡。";

_frameMove.IconAndOptionsFrame.OptionsFrame._checkDimSnap.Title="默认捕捉（SHIFT 切换） ";
_frameMove.IconAndOptionsFrame.OptionsFrame._checkDimSnap.LongHelp="移动尺寸时激活捕捉。
使用“配置捕捉” 按钮自定义
尺寸捕捉的方式。
按“Shift”键可以
暂时激活或取消激活模式。";
_frameMove.IconAndOptionsFrame.OptionsFrame._frameDimSnap._pushDimSnap.Title="配置捕捉";
_frameMove.IconAndOptionsFrame.OptionsFrame._frameDimSnap._pushDimSnap.LongHelp="配置在网格上捕捉尺寸的方式
在符号间捕捉尺寸值的方式";
_frameMove.IconAndOptionsFrame.OptionsFrame._checkDimMoveSubPart.Title="仅移动所选子零件 ";
_frameMove.IconAndOptionsFrame.OptionsFrame._checkDimMoveSubPart.LongHelp="仅允许移动尺寸
（尺寸线、值或次要尺寸线）的选定子零件。";



_frameLineUp.HeaderFrame.Global.Title="排列";
_frameLineUp.HeaderFrame.Global.LongHelp="在创建中或在“排列”命令中，
定义用于排列尺寸的默认参数。您可以标识系统或忽略它们。";

_frameLineUp.IconAndOptionsFrame.OptionsFrame._labelLineUpBaseOffset.Title="参考的默认偏移值：";
_frameLineUp.IconAndOptionsFrame.OptionsFrame._labelLineUpBaseOffset.LongHelp="为长度/距离/角度尺寸、半径/直径尺寸
定义参考的默认偏移值。";
_frameLineUp.IconAndOptionsFrame.OptionsFrame._editorLineUpBaseOffsetLength.LongHelp="为长度/距离/角度尺寸
定义参考的默认偏移值。";
_frameLineUp.IconAndOptionsFrame.OptionsFrame._editorLineUpBaseOffsetAngle.LongHelp="为半径/直径尺寸
定义参考的默认偏移值。";

_frameLineUp.IconAndOptionsFrame.OptionsFrame._labelLineUpOffset.Title="尺寸之间的默认偏移值：";
_frameLineUp.IconAndOptionsFrame.OptionsFrame._labelLineUpOffset.LongHelp="为长度/距离/角度尺寸和半径/直径尺寸
定义尺寸之间的
默认偏移值。";
_frameLineUp.IconAndOptionsFrame.OptionsFrame._editorLineUpOffsetLength.LongHelp="长度/距离/角度尺寸间尺寸的
默认偏移值。";
_frameLineUp.IconAndOptionsFrame.OptionsFrame._editorLineUpOffsetAngle.LongHelp="为半径/直径尺寸定义
尺寸间的默认偏移值。";

_frameLineUp.IconAndOptionsFrame.OptionsFrame.checkLineUpStackVal.Title="对齐堆叠式尺寸值";
_frameLineUp.IconAndOptionsFrame.OptionsFrame.checkLineUpStackVal.LongHelp="根据组中最小的尺寸值对齐
该组中所有的堆叠式尺寸值。";
_frameLineUp.IconAndOptionsFrame.OptionsFrame.checkLineUpCumulVal.Title="对齐累积尺寸值";
_frameLineUp.IconAndOptionsFrame.OptionsFrame.checkLineUpCumulVal.LongHelp="根据组中最小的尺寸值对齐
该组中所有累积尺寸值。";
_frameLineUp.IconAndOptionsFrame.OptionsFrame.tmpFrame.checkLineUpAutoFunnel.Title="自动添加一个尺寸标注（适用于 V5R14 之前的累积尺寸）";
_frameLineUp.IconAndOptionsFrame.OptionsFrame.tmpFrame.checkLineUpAutoFunnel.LongHelp="自动添加一个尺寸标注。
否则，无法正确显示
尺寸值。
仅适用于 V5R14 之前的累积尺寸。";


_frameDesignMode.HeaderFrame.Global.Title="分析显示模式 ";
_frameDesignMode.HeaderFrame.Global.LongHelp="定义以不同的、自定义的颜色
来展示各类尺寸的模式。";

_frameDesignMode.IconAndOptionsFrame.OptionsFrame._checkDesignMode.Title="激活分析显示方式";
_frameDesignMode.IconAndOptionsFrame.OptionsFrame._checkDesignMode.LongHelp="以不同的、可自定义的颜色
显示各种类型的尺寸。";

_frameDesignMode.IconAndOptionsFrame.OptionsFrame.diagColorsPushButton.Title="类型和颜色...";
_frameDesignMode.IconAndOptionsFrame.OptionsFrame.diagColorsPushButton.LongHelp="以不同的、可自定义的颜色
显示各种类型的尺寸。";




dimAssocWnd.Title="3D 尺寸关联性";
//frameDimAssoc3D.IconAndOptionsFrame.OptionsFrame._frameDimForce2D._frameDimForce2Dlocked.Title="Associativity on 3D";
dimAssocWnd.frameDimAssoc3D.HeaderFrame.Global.Title="3D 关联";

dimAssocWnd.frameDimAssoc3D.IconAndOptionsFrame.OptionsFrame._frameDimForce2D._frameDimForce2Dlocked._radioDimForce2DAlways.Title="仅创建无关联性的尺寸";
dimAssocWnd.frameDimAssoc3D.IconAndOptionsFrame.OptionsFrame._frameDimForce2D._frameDimForce2Dlocked._radioDimForce2DAlways.LongHelp="仅在 3D 上创建无关联性的尺寸。";

dimAssocWnd.frameDimAssoc3D.IconAndOptionsFrame.OptionsFrame._frameDimForce2D._frameDimForce2Dlocked._radioDimForce2DWhenPb.Title="允许无关联性的尺寸";
dimAssocWnd.frameDimAssoc3D.IconAndOptionsFrame.OptionsFrame._frameDimForce2D._frameDimForce2Dlocked._radioDimForce2DWhenPb.LongHelp="仅当无法创建 3D 关联性时，
才能在 3D 上创建无关联性的尺寸。";

dimAssocWnd.frameDimAssoc3D.IconAndOptionsFrame.OptionsFrame._frameDimForce2D._frameDimForce2Dlocked._radioDimForce2DNever.Title="请勿创建无关联性的尺寸";
dimAssocWnd.frameDimAssoc3D.IconAndOptionsFrame.OptionsFrame._frameDimForce2D._frameDimForce2Dlocked._radioDimForce2DNever.LongHelp="请勿在 3D 上创建无关联性的尺寸。";

dimAssocWnd.Title="3D 尺寸关联性";
//frameDimAssoc3D.IconAndOptionsFrame.OptionsFrame._frameDimForce2D._frameDimForce2Dlocked.Title="Associativity on 3D";
dimAssocWnd.frameAssocDimApprox.HeaderFrame.Global.Title="“近似”模式视图中的关联性";

dimAssocWnd.frameAssocDimApprox.IconAndOptionsFrame.OptionsFrame.chkAssocDimOnApproximate.Title="允许在近似视图中创建关联性尺寸";
dimAssocWnd.frameAssocDimApprox.IconAndOptionsFrame.OptionsFrame.chkAssocDimOnApproximate.LongHelp="若选择该项，则将在近似视图中应用“3D 上的关联性”截面中定义的尺寸关联性规则，\n否则，在那些视图中这些尺寸将是非关联的。";

dimSnapWnd.Title="尺寸捕捉";
dimSnapWnd.frameDimSnap.HeaderFrame.Global.Title="捕捉尺寸";
dimSnapWnd.frameDimSnap.HeaderFrame.Global.LongHelp="配置尺寸捕捉的方式。";
dimSnapWnd.frameDimSnap.IconAndOptionsFrame.OptionsFrame.frameRadioDimSnap.frameRadioDimSnaplocked._radioDimSnapOnGrid.Title="在网格上";
dimSnapWnd.frameDimSnap.IconAndOptionsFrame.OptionsFrame.frameRadioDimSnap.frameRadioDimSnaplocked._radioDimSnapOnGrid.LongHelp="在网格上捕捉尺寸的位置。";
dimSnapWnd.frameDimSnap.IconAndOptionsFrame.OptionsFrame.frameRadioDimSnap.frameRadioDimSnaplocked._radioDimSnapValue.Title="符号之间的值";
dimSnapWnd.frameDimSnap.IconAndOptionsFrame.OptionsFrame.frameRadioDimSnap.frameRadioDimSnaplocked._radioDimSnapValue.LongHelp="如果值在符号之间，
则捕捉处于标准位置上的值。";
dimSnapWnd.frameDimSnap.IconAndOptionsFrame.OptionsFrame.frameRadioDimSnap.frameRadioDimSnaplocked._radioDimSnapBoth.Title="两者";
dimSnapWnd.frameDimSnap.IconAndOptionsFrame.OptionsFrame.frameRadioDimSnap.frameRadioDimSnaplocked._radioDimSnapBoth.LongHelp="既捕捉网格上的位置，
也捕捉符号之间的值。";





// NLS de l'onglet Dimension pour Analysis Colors
dimColor.Title="类型和颜色";
dimColor.frameDimColor.HeaderFrame.Global.Title="类型和颜色";
dimColor.frameDimColor.HeaderFrame.Global.LongHelp="您可以定义并自定义用于各种
类型的尺寸的不同颜色。";
dimColor.frameDimColor.IconAndOptionsFrame.OptionsFrame.chkDimNotUpToDate.Title="非最新的尺寸";
dimColor.frameDimColor.IconAndOptionsFrame.OptionsFrame.chkDimNotUpToDate.LongHelp="定义分析显示模式中所使用的颜色，
以展示非最新的尺寸。";
dimColor.frameDimColor.IconAndOptionsFrame.OptionsFrame.chkDimNonAssoc.Title="（3D 上的）非关联性尺寸";
dimColor.frameDimColor.IconAndOptionsFrame.OptionsFrame.chkDimNonAssoc.LongHelp="定义分析显示模式中所使用的颜色，
展示与 3D 几何图形不关联的
尺寸。";
dimColor.frameDimColor.IconAndOptionsFrame.OptionsFrame.chkDimBroken.Title="转换为图形元素的尺寸";
dimColor.frameDimColor.IconAndOptionsFrame.OptionsFrame.chkDimBroken.LongHelp="定义分析显示模式中所使用的颜色，
表示转换为图形元素的尺寸。";
dimColor.frameDimColor.IconAndOptionsFrame.OptionsFrame.chkDimFake.Title="假尺寸";
dimColor.frameDimColor.IconAndOptionsFrame.OptionsFrame.chkDimFake.LongHelp="定义分析显示模式中使用的颜色，
表示假尺寸。";
dimColor.frameDimColor.IconAndOptionsFrame.OptionsFrame.chkDimDriving.Title="驱动 2D 几何图形的尺寸";
dimColor.frameDimColor.IconAndOptionsFrame.OptionsFrame.chkDimDriving.LongHelp="定义分析显示模式中使用的颜色，
表示驱动 2D 几何图形的尺寸。";
dimColor.frameDimColor.IconAndOptionsFrame.OptionsFrame.chkDim3DCst.Title="3D 约束中生成的尺寸";
dimColor.frameDimColor.IconAndOptionsFrame.OptionsFrame.chkDim3DCst.LongHelp="定义分析显示模式中使用的颜色，
表示 3D 约束中生成的尺寸。";
dimColor.frameDimColor.IconAndOptionsFrame.OptionsFrame.chkDimNUTD3DCst.Title="驱动非最新 3D 约束的尺寸";
dimColor.frameDimColor.IconAndOptionsFrame.OptionsFrame.chkDimNUTD3DCst.LongHelp="定义分析显示模式中使用的颜色，
表示驱动非最新 3D 约束
的尺寸。";
dimColor.frameDimColor.IconAndOptionsFrame.OptionsFrame.chkDimTrue.Title="真实尺寸";
dimColor.frameDimColor.IconAndOptionsFrame.OptionsFrame.chkDimTrue.LongHelp="定义分析显示模式中使用的颜色，
表示实长尺寸。";
dimColor.frameDimColor.IconAndOptionsFrame.OptionsFrame.chkDimIsolated.Title="已隔离尺寸";
dimColor.frameDimColor.IconAndOptionsFrame.OptionsFrame.chkDimIsolated.LongHelp="定义分析显示模式中使用的颜色，
表示未链接到任何几何图形的尺寸。";
dimColor.frameDimColor.IconAndOptionsFrame.OptionsFrame.chkDimOnNonVisGeom.Title="非可视几何图形上的尺寸";
dimColor.frameDimColor.IconAndOptionsFrame.OptionsFrame.chkDimOnNonVisGeom.LongHelp="定义分析显示方式中所使用的颜色，
表示附加到非可视几何图形上的尺寸。";
dimColor.frameDimColor.IconAndOptionsFrame.OptionsFrame.chkDimClipped.Title="被裁剪尺寸";
dimColor.frameDimColor.IconAndOptionsFrame.OptionsFrame.chkDimClipped.LongHelp="定义分析显示方式中所使用的颜色，
表示被裁剪尺寸。";
dimColor.frameDimColor.IconAndOptionsFrame.OptionsFrame.chkTechFeatDim.Title="技术特征尺寸";
dimColor.frameDimColor.IconAndOptionsFrame.OptionsFrame.chkTechFeatDim.LongHelp="定义分析显示方式中所使用的颜色，
表示技术特征尺寸。";

frmDimOriented.HeaderFrame.Global.Title="与一个原点相关的尺寸";
frmDimOriented.IconAndOptionsFrame.OptionsFrame.chkDimOriginSymb.Title="使用尺寸原点符号";
frmDimOriented.IconAndOptionsFrame.OptionsFrame.chkDimOriginSymb.LongHelp="使用尺寸原点符号";
