Title="保存";
OptionsTopFrame.UITableSelectionLabel.Title="视图";
OptionsTopFrame.VoidLabel1.Title="  ";
OptionsTopFrame.VoidLabel3.Title="  ";


CreateVersion.ShortHelp="创建版本";
CreateVersion.LongHelp="创建版本";

CreateIteration.ShortHelp="创建修订版";
CreateIteration.LongHelp="创建修订版";

OptionsTopFrame.SaveScopeCombo.ShortHelp="保存范围";
OptionsTopFrame.SaveScopeCombo.LongHelp="选择要用于保存的范围。";
OptionsTopFrame.SaveScopeCombo.Help="选择要用于保存的范围。";

SaveScopeCombo_Session.Title="会话";
SaveScopeCombo_Editor.Title="当前编辑器";
SaveScopeCombo_ActiveDoc.Title="活动文档";

OptionsTopFrame.FilterButton.ShortHelp="仅显示已修改的文档（如已选定）。";
OptionsTopFrame.FilterButton.LongHelp="仅显示已修改的文档（如已选定）。";
OptionsTopFrame.FilterButton.Help="仅显示已修改的文档（如已选定）。";

SaveFrame.Title="保存列表框架";
SaveFrame.PreviewAndStatsFrame.VoidLabel.Title="  ";
SaveFrame.PreviewAndStatsFrame.VoidLabel6.Title=" ";
SaveFrame.Help="文档列表";
SaveFrame.ShortHelp="文档列表";
SaveFrame.LongHelp="文档列表";

FilteredObj=" 已过滤的对象";
NewObjs=" 新对象";
NewObj=" 新对象";
ModifiedObjs=" 已修改的对象";
ModifiedObj=" 已修改的对象";
SaveScope="保存范围包括：";
ExcludedObjs=" 已排除的对象";
ExcludedObj=" 已排除的对象";
IncludedObjs=" 已包括的对象";
IncludedObj=" 已包括的对象";

// Error Message Box Title
MessageTitle="ENOVIA V6 - 保存";
3DEXPMessageTitle="3DEXPERIENCE - 保存";

ServerErrorTitle=" - 服务器错误";
ErrorTitle=" - 错误";

NewFilename="新文件名";

// Introduction message for each of the cases where an error might popup
SaveDialogBuildFailed="尝试构建“保存”对话框时发生了错误：\n\n";
SaveFailed="尝试保存选定文档时发生了错误：\n\n";
ExploreFailed="尝试浏览文档时发生了错误：\n\n";
TableSwitchFailed="更改“保存”对话框的表时发生了错误：\n\n";
WorkSpaceExpandNodeFailed="展开节点时发生错误：\n\n";

MakeDirFailed.Request="未能访问 /P2 目录\n\n";
MakeDirFailed.Diagnostic="/P1 不存在，且无法创建。\n";
MakeDirFailed.Advice="请转至“工具”/“选项”>“常规”/“兼容性”部分 >“ENOVIA V6”选项卡，然后设置有效的 /P2 目录\n";

3DEXPMakeDirFailed.Request="未能访问 /P2 目录\n\n";
3DEXPMakeDirFailed.Diagnostic="/P1 不存在，且无法创建。\n";
3DEXPMakeDirFailed.Advice="请转至“工具”/“选项”>“常规”/“兼容性”部分 >“3DEXPERIENCE”选项卡，然后设置有效的 /P2 目录\n";


// Detailed Error cases
DefaultErrorMessage="抱歉，未能识别问题。";

NewNotSavedNodesConfirmation="这些文件未保存在磁盘上：\n/P1\n它们将使用默认文件名保存在/P3目录中：\n/P2\n\n是否继续？";
NewNotSavedNodesConfirmationOneFile="此文件未保存在磁盘上：\n/P1\n它将使用默认文件名保存在/P3目录中：\n/P2\n\n是否继续？";
NewNotSavedNodesConfirmationManyFiles="/P1 文件未保存在磁盘上。\n\n它们将使用默认文件名保存在/P3目录中：\n/P2\n\n是否继续？";

NewNotSavedNodesError.Request="无法将新文档保存到磁盘。";
NewNotSavedNodesError.Diagnostic="这些文件未保存到磁盘上：\n/P1\n目录中/P3已存在一些同名文件：\n/P2";
NewNotSavedNodesError.Advice="在启动“保存”之前，请使用适当的名称保存这些文件。";

NewNotSavedNodesSaveError.Request="无法将新文档保存到磁盘。";
NewNotSavedNodesSaveError.Diagnostic="尝试保存以下新文档失败：\n/P1";
NewNotSavedNodesSaveError.Advice="请检查磁盘空间和您对 /P2 目录的访问权限。\n必须在启动“保存”之前将新文档保存到本地磁盘。";

NewNotSavedCGRSaveError.Request="无法将新文档保存到磁盘。";
NewNotSavedCGRSaveError.Diagnostic="以下文档无法自动保存：\n/P1";
NewNotSavedCGRSaveError.Advice="请使用“CATIA -> 保存管理”保存这些文档。";

NothingToSave.Request="没有要保存的内容。";
NothingToSave.Diagnostic="未打开任何文档。";
NothingToSave.Advice="要在 ENOVIA 中保存文档，请先在会话中打开它。";

NoModificationToSave.Request="所有文档均已保存。";
NoModificationToSave.Diagnostic="本地文档不包含任何尚未保存的修改。";
NoModificationToSave.Advice="如果愿意，您仍可选择现有文档来创建新的次要版本或新的主要修订版。";

VTINoModificationToSave.Request="所有文档均已保存。";
VTINoModificationToSave.Diagnostic="本地文档不包含任何尚未保存的修改。";
VTINoModificationToSave.Advice="如果愿意，您仍然可以选择现有文档来创建新的次要修订版或新的主要修订版本。";

ShowSavePanel.Title="错误";
ShowSavePanel.Message="\n无法激活“高级保存”命令。\n已禁用“高级保存”命令。";

ObsoleteUUIDDocs.Request="无法保存 UI 选择的文档。";
ObsoleteUUIDDocs.Diagnostic="某些文档已作废或存在 UUID 冲突。";
ObsoleteUUIDDocs.Advice="如果要保存，请解决冲突。";

LockedByOtherUser.Request="无法保存 UI 选择的文档。";
LockedByOtherUser.Diagnostic="某些文档被其他用户锁定且仍在修改。";
LockedByOtherUser.Advice="如果要保存，请解决文档状态。";

TableError.Request="无法将表 /P1 用于“保存”对话框。";
TableError.Diagnostic="表中缺少某些必填字段：\n/P2";
TableError.Advice="请先在表 /P1 中定义这些字段，然后再将其用于“保存”对话框。有关自定义“保存”对话框表的其他信息可在文档中找到。";

SaveSuccess="
		已成功保存 /P1 个对象。";
SingleObjSaveSuccess="已成功保存 1 个对象。";

OptionsTopFrame.WorkspaceFolderButton.Title="工作区文件夹按钮";
OptionsTopFrame.WorkspaceFolderButton.LongHelp="文件夹选择";
OptionsTopFrame.WorkspaceFolderButton.ShortHelp="文件夹选择";
OptionsTopFrame.SaveTo.Title="保存到";

ECValidationFailed.Request="无法保存 UI 选择的文档。";
ECValidationFailed.Diagnostic="验证嵌入式部件时，检测到问题。";
ECValidationFailed.Advice="如果要保存，请解决嵌入式部件问题。";

SelectionValidationFailed.Request="无法保存 UI 选择的文档。";
SelectionValidationFailed.Diagnostic="所有这些项目均无权创建/覆盖版本。";
SelectionValidationFailed.Advice="如果要保存，请修改您的选择。";

VTISelectionValidationFailed.Request="无法保存 UI 选择的文档。";
VTISelectionValidationFailed.Diagnostic="所有这些项目均无权创建或覆盖修订版。";
VTISelectionValidationFailed.Advice="如果要保存，请修改您的选择。";

UnlockedModified.Request="保存 UI 选定文档需要用户确认。";
UnlockedModified.Diagnostic="将保存您未锁定的一些修改的文档。";
UnlockedModified.Advice="请在确认前确定没有其他用户正在修改这些文档。";

UnlockedModifiedDocs="将保存您未锁定的一些修改的文档。\n\n其他用户可能在执行相冲突的修改或您可能从未打算保存这些修改。\n\n确定要保存这些修改吗？\n";
OptionsTopFrame.RetainLockButton.Title="保持锁定";

EmptyConsistentSet="当前范围中没有要保存的内容。范围中的所有文档都可能被排除。";

InvalidDocName.Request="无法保存 UI 选择的文档。";
InvalidDocName.Diagnostic="某些文档的文件名称中包含集成或管理员限制的不支持字符。";
InvalidDocName.Advice="请使用有效名称重命名文档。";

InvalidObjName.Request="无法保存 UI 选择的文档";
InvalidObjName.Diagnostic="某些文档的对象名称中包含集成或管理员限制的不支持字符";
InvalidObjName.Advice="请提供有效对象名称";

InvalidTitle.Request="无法保存 UI 选择的文档";
InvalidTitle.Diagnostic="某些文档的标题中包含集成或管理员限制的不支持字符";
InvalidTitle.Advice="请提供有效标题";

Frame1.SaveComment.Title="注释";
Frame1.SaveComment.ShortHelp="保存注释";
Frame1.SaveComment.LongHelp="将与文档一起保存的注释。";

ReadOnlyFile.Request="无法保存 UI 选择的文档。";
ReadOnlyFile.Diagnostic="部分文档具有只读权限";
ReadOnlyFile.Advice="如果要保存，请更改只读文档的权限";
SaveFrame.PreviewAndStatsFrame.LongHelp="显示统计信息";
SaveFrame.PreviewAndStatsFrame.Help="显示统计信息";
Preview.LongHelp="选定文档预览";

3DEXPNothingToSave.Request="没有要保存的内容。";
3DEXPNothingToSave.Diagnostic="未打开任何文档。";
3DEXPNothingToSave.Advice="要保存 3DEXPERIENCE 中的文档，请先在会话中打开它。";

NoObjectsFound="无法识别此对象。";

ExternalizeECs="保存范围包括一些嵌入式部件。\n有必要将其外化以保存到 3DEXPERIENCE 平台。\n是否将其外化？";
RequiredExternalizingEC.Request="保存范围包括一些嵌入式部件";
RequiredExternalizingEC.Diagnostic="有必要将其外化以保存到 3DEXPERIENCE 平台";
RequiredExternalizingEC.Advice="“保存”操作之前请先将其外化";

SaveIntoFolder.Request="请在文件夹中保存文档";
SaveIntoFolder.Diagnostic="保存范围内的所有文档未在文件夹中得到保存";
SaveIntoFolder.Advice="您可以在保存对话框中通过 RMB 命令选择文件夹";

3DEXPFolder="3DEXPERIENCE V5 高速缓存";
LocalFolder="ENOVIA 本地工作区";
SaveFrame.Frame2.Label4.Title="  请锁定所有包含的对象";
SaveFrame.Frame2.Label5.Title="  请为所有对象分配文件夹";
SaveFrame.Frame2.Label6.Title="  在部分对象上没有读取权限";
SaveFrame.Frame2.LongHelp="“确定”按钮已禁用，因为至少有一个必需条件未满足"; //F68 TODO


InvalidSecurityContext.Title="错误";
InvalidSecurityContext.Request="无法设置用户退出返回的安全上下文。\n";
InvalidSecurityContext.Diagnostic="安全上下文未分配给用户。\n";
InvalidSecurityContext.Advice="选择其他文件夹";

SameFilePresentOnDisk="同一磁盘位置中已存在具有此名称的文件。\n是否要覆盖它？";
OverwriteConfirmation="确认覆盖";

Same_Filenames_As_Children="某些对象具有文件名相同的两个或多个文档子级";
