//=============================================================================
// COPYRIGHT DASSAULT SYSTEMES  2005
//=============================================================================
//
// CATStCCKAnalysisDialog: Resource file for NLS purposes: 
// Connect Checker Dialog Box
//
//=============================================================================
//
// Implementation Notes:
//
//=============================================================================
// Nov 2004						   Ritesh Kanetkar
//=============================================================================

Title="连接检查器";
MainFrame.CCKTypeFrame.Title="类型";
MainFrame.GapFrame.Title="间隔";
MainFrame.GapFrame.Connection.Title="连接";
MainFrame.GapFrame.UpperLimit.Title="上限";
//MainFrame.TabContainer.Search.FullTabPage.Title="Search";
MainFrame.TabContainer.QuickTabPage.Title="快速";
MainFrame.TabContainer.FullTabPage.Title="完全";
MainFrame.TabContainer.FullTabPage.ContinuityFrame.Title="连续";
MainFrame.TabContainer.FullTabPage.DisplayFrame.Title="显示";
MainFrame.TabContainer.FullTabPage.ScalingFrame.Title="振幅";

MainFrame.TabContainer.QuickTabPage.QuickModeFrame.G0Label = "            ";
MainFrame.TabContainer.QuickTabPage.QuickModeFrame.G1Label = "            ";
MainFrame.TabContainer.QuickTabPage.QuickModeFrame.G2Label = "            ";
MainFrame.TabContainer.QuickTabPage.QuickModeFrame.G3Label = "            ";
MainFrame.TabContainer.QuickTabPage.QuickModeFrame.G4Label = "            ";

//Start HRY 
MainFrame.CCKTypeFrame.SurSurCCKRadio.ShortHelp = "曲面-曲面";
MainFrame.CCKTypeFrame.SurSurCCKRadio.LongHelp = "检查两个或多个曲面间的连接";
MainFrame.CCKTypeFrame.CurCurCCKRadio.ShortHelp = "曲线-曲线";
MainFrame.CCKTypeFrame.CurCurCCKRadio.LongHelp = "检查两条或多条曲线间的连接";
MainFrame.CCKTypeFrame.SurCurCCKRadio.ShortHelp = "曲面-曲线";
MainFrame.CCKTypeFrame.SurCurCCKRadio.LongHelp = "检查曲面和曲线间的连接";

MainFrame.GapFrame.Connection.LongHelp = "设置最小间隔值，所有小于该值的间隔分析时均看作已连接";
MainFrame.GapFrame.UpperLimit.LongHelp = "设置最大间隔值，若间距大于此值则不执行任何分析";
MainFrame.GapFrame.GapRadio.ShortHelp = "间隔";
MainFrame.GapFrame.GapRadio.LongHelp = "用户可以选择间隔定义";
MainFrame.GapFrame.InternalEdgesRadio.ShortHelp = "内部边线";
MainFrame.GapFrame.InternalEdgesRadio.LongHelp = "执行分析时考虑接合元素的内部边线";

MainFrame.TabContainer.FullTabPage.ContinuityFrame.G0Radio.ShortHelp = "G0";
MainFrame.TabContainer.FullTabPage.ContinuityFrame.G0Radio.LongHelp = "检查 G0 类型连续";
MainFrame.TabContainer.FullTabPage.ContinuityFrame.G1Radio.ShortHelp = "G1";
MainFrame.TabContainer.FullTabPage.ContinuityFrame.G1Radio.LongHelp = "检查 G1 类型连续";
MainFrame.TabContainer.FullTabPage.ContinuityFrame.G2Radio.ShortHelp = "G2";
MainFrame.TabContainer.FullTabPage.ContinuityFrame.G2Radio.LongHelp = "检查 G2 类型连续";
MainFrame.TabContainer.FullTabPage.ContinuityFrame.FullG2Concavity.ShortHelp = "凹性缺陷";
MainFrame.TabContainer.FullTabPage.ContinuityFrame.FullG2Concavity.LongHelp = "在 G2 模式下检查凹性";
MainFrame.TabContainer.FullTabPage.ContinuityFrame.G3Radio.ShortHelp = "G3";
MainFrame.TabContainer.FullTabPage.ContinuityFrame.G3Radio.LongHelp = "检查 G3 类型连续";
MainFrame.TabContainer.FullTabPage.ContinuityFrame.OverlapRadio.ShortHelp = "交叠";
MainFrame.TabContainer.FullTabPage.ContinuityFrame.OverlapRadio.LongHelp = "检查交叠类型连续";

MainFrame.TabContainer.FullTabPage.DisplayFrame.Comb.ShortHelp = "梳";
MainFrame.TabContainer.FullTabPage.DisplayFrame.Comb.LongHelp = "显示梳";
MainFrame.TabContainer.FullTabPage.DisplayFrame.Envelop.ShortHelp = "包络";
MainFrame.TabContainer.FullTabPage.DisplayFrame.Envelop.LongHelp = "显示包络";
MainFrame.TabContainer.FullTabPage.DisplayFrame.MinInfo.ShortHelp = "最小值信息";
MainFrame.TabContainer.FullTabPage.DisplayFrame.MinInfo.LongHelp = "显示最小值";
MainFrame.TabContainer.FullTabPage.DisplayFrame.MaxInfo.ShortHelp = "最大值信息";
MainFrame.TabContainer.FullTabPage.DisplayFrame.MaxInfo.LongHelp = "显示最大值";
MainFrame.TabContainer.FullTabPage.DisplayFrame.Discretization.ShortHelp = "离散化";
MainFrame.TabContainer.FullTabPage.DisplayFrame.Discretization.LongHelp = "用户可以在四个预定义的离散化系数间切换";
MainFrame.TabContainer.FullTabPage.DisplayFrame.ColorScale.ShortHelp = "色标";
MainFrame.TabContainer.FullTabPage.DisplayFrame.ColorScale.LongHelp = "在完整颜色范围内显示分析";

MainFrame.TabContainer.FullTabPage.ScalingFrame.AutoScaling.ShortHelp = "自动缩放";
MainFrame.TabContainer.FullTabPage.ScalingFrame.AutoScaling.LongHelp = "启用自动缩放";
MainFrame.TabContainer.FullTabPage.ScalingFrame.MultiplyTwo.ShortHelp = "乘以 2";
MainFrame.TabContainer.FullTabPage.ScalingFrame.MultiplyTwo.LongHelp = "缩放系数乘以 2";
MainFrame.TabContainer.FullTabPage.ScalingFrame.DivideTwo.ShortHelp = "除以 2";
MainFrame.TabContainer.FullTabPage.ScalingFrame.DivideTwo.LongHelp = "缩放系数除以 2";

MainFrame.TabContainer.QuickTabPage.QuickModeFrame.G0Radio.ShortHelp = "G0";
MainFrame.TabContainer.QuickTabPage.QuickModeFrame.G0Radio.LongHelp = "检查 G0 类型连续";
MainFrame.TabContainer.QuickTabPage.QuickModeFrame.G1Radio.ShortHelp = "G1";
MainFrame.TabContainer.QuickTabPage.QuickModeFrame.G1Radio.LongHelp = "检查 G1 类型连续";
MainFrame.TabContainer.QuickTabPage.QuickModeFrame.G2Radio.ShortHelp = "G2";
MainFrame.TabContainer.QuickTabPage.QuickModeFrame.G2Radio.LongHelp = "检查 G2 类型连续";
MainFrame.TabContainer.QuickTabPage.QuickModeFrame.G3Radio.ShortHelp = "G3";
MainFrame.TabContainer.QuickTabPage.QuickModeFrame.G3Radio.LongHelp = "检查 G3 类型连续";
MainFrame.TabContainer.QuickTabPage.QuickModeFrame.OverlapRadio.ShortHelp = "交叠";
MainFrame.TabContainer.QuickTabPage.QuickModeFrame.OverlapRadio.LongHelp = "检查交叠类型连续";
//End HRY 

ResultOverlapping.Title = "交叠";
ResultNoOverlapping.Title = "无交叠";

SurfaceSelected.Message = " 曲面";
CurveSelected.Message = " 曲线";
ConnectionsDetected.Message = " 连接";

SingularCurve.Title = "单一曲线";

// New Dialog box for R17SP02 Icons

MainFrame.InputElementsFrame.InputElementsLable.Title = "元素： ";
MainFrame.InputElementsFrame.InputElementsSelector.ShortHelp = "显示选定元素的数目";
MainFrame.InputElementsFrame.InputElementsSelector.LongHelp = "显示选定元素的总数目以便进行连接分析";
InputElementsListTitle.Message = "输入元素列表";

MainFrame.SecondRowDummyFrame.CCKTypeFrame.Title="类型";
MainFrame.SecondRowDummyFrame.CCKTypeFrame.LongHelp = "要检测 3 个不同类型的连接";
MainFrame.SecondRowDummyFrame.CCKTypeFrame.SurSurCCKChkButton.ShortHelp = "曲面-曲面连接";
MainFrame.SecondRowDummyFrame.CCKTypeFrame.SurSurCCKChkButton.LongHelp = "检查两个或多个曲面间的连接";
MainFrame.SecondRowDummyFrame.CCKTypeFrame.CurCurCCKChkButton.ShortHelp = "曲线-曲线连接";
MainFrame.SecondRowDummyFrame.CCKTypeFrame.CurCurCCKChkButton.LongHelp = "检查两条或多条曲线间的连接";
MainFrame.SecondRowDummyFrame.CCKTypeFrame.SurCurCCKChkButton.ShortHelp = "曲面-曲线连接";
MainFrame.SecondRowDummyFrame.CCKTypeFrame.SurCurCCKChkButton.LongHelp = "检查曲面和曲线间的连接";

MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.Title="快速";
MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.OverlapChkButton.ShortHelp = "交叠缺陷";
MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.OverlapChkButton.LongHelp = "检查交叠类型连续";
MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.G0ChkButton.ShortHelp = "G0 连续";
MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.G0ChkButton.LongHelp = "检查 G0 类型连续";
MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.G1ChkButton.ShortHelp = "G1 连续";
MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.G1ChkButton.LongHelp = "检查 G1 类型连续";
MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.G2ChkButton.ShortHelp = "G2 连续";
MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.G2ChkButton.LongHelp = "检查 G2 类型连续";
MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.G3ChkButton.ShortHelp = "G3 连续";
MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.G3ChkButton.LongHelp = "检查 G3 类型连续";

MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.G0Text.Title = ">";
MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.G1Text.Title = ">";
MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.G2Text.Title = ">";
MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.G3Text.Title = ">";

MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.Title="完全";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDummyDisplayFrame.FullModeDisplayOptionsFrame.Title="显示";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDummyDisplayFrame.FullModeDisplayOptionsFrame.LongHelp = "已检测到连接的各种显示选项";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDummyDisplayFrame.FullModeDisplayOptionsFrame.LimitedColorScale.ShortHelp = "有限色标";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDummyDisplayFrame.FullModeDisplayOptionsFrame.LimitedColorScale.LongHelp = "在限制颜色范围内显示分析";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDummyDisplayFrame.FullModeDisplayOptionsFrame.FullColorScale.ShortHelp = "完整色标";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDummyDisplayFrame.FullModeDisplayOptionsFrame.FullColorScale.LongHelp = "在完整颜色范围内显示分析";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDummyDisplayFrame.FullModeDisplayOptionsFrame.Comb.ShortHelp = "梳";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDummyDisplayFrame.FullModeDisplayOptionsFrame.Comb.LongHelp = "显示梳";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDummyDisplayFrame.FullModeDisplayOptionsFrame.Envelop.ShortHelp = "包络";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDummyDisplayFrame.FullModeDisplayOptionsFrame.Envelop.LongHelp = "显示包络";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDummyDisplayFrame.FullModeDisplayOptionsFrame.FullG2Concavity.ShortHelp = "凹性缺陷";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDummyDisplayFrame.FullModeDisplayOptionsFrame.FullG2Concavity.LongHelp = "在 G2 模式下检查凹性缺陷";

MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDummyDisplayFrame.AmplitudeFrame.Title="振幅";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDummyDisplayFrame.AmplitudeFrame.LongHelp = "定义梳的振幅";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDummyDisplayFrame.AmplitudeFrame.AutoScaling.ShortHelp = "自动缩放";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDummyDisplayFrame.AmplitudeFrame.AutoScaling.LongHelp = "启用梳的自动缩放";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDummyDisplayFrame.AmplitudeFrame.MultiplyTwo.ShortHelp = "乘以 2";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDummyDisplayFrame.AmplitudeFrame.MultiplyTwo.LongHelp = "缩放系数乘以 2";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDummyDisplayFrame.AmplitudeFrame.DivideTwo.ShortHelp = "除以 2";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDummyDisplayFrame.AmplitudeFrame.DivideTwo.LongHelp = "缩放系数除以 2";

MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeContinuityFrame.OverlapRadio.ShortHelp = "交叠缺陷";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeContinuityFrame.OverlapRadio.LongHelp = "检查交叠类型连续";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeContinuityFrame.G0Radio.ShortHelp = "G0 连续";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeContinuityFrame.G0Radio.LongHelp = "检查 G0 类型连续";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeContinuityFrame.G1Radio.ShortHelp = "G1 连续";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeContinuityFrame.G1Radio.LongHelp = "检查 G1 类型连续";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeContinuityFrame.G2Radio.ShortHelp = "G2 连续";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeContinuityFrame.G2Radio.LongHelp = "检查 G2 类型连续";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeContinuityFrame.G3Radio.ShortHelp = "G3 连续";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeContinuityFrame.G3Radio.LongHelp = "检查 G3 类型连续";

MainFrame.ThirdRowDummyFrame.DummyDisplayFrame.MinMaxInfoDisplayFrame.Title = "信息";
MainFrame.ThirdRowDummyFrame.DummyDisplayFrame.MinMaxInfoDisplayFrame.LongHelp = "显示已找到的每一连接的最小值和最大值";
MainFrame.ThirdRowDummyFrame.DummyDisplayFrame.MinMaxInfoDisplayFrame.MinInfo.ShortHelp = "最小值信息";
MainFrame.ThirdRowDummyFrame.DummyDisplayFrame.MinMaxInfoDisplayFrame.MinInfo.LongHelp = "显示最小值";
MainFrame.ThirdRowDummyFrame.DummyDisplayFrame.MinMaxInfoDisplayFrame.MaxInfo.ShortHelp = "最大值信息";
MainFrame.ThirdRowDummyFrame.DummyDisplayFrame.MinMaxInfoDisplayFrame.MaxInfo.LongHelp = "显示最大值";

MainFrame.ThirdRowDummyFrame.DummyDisplayFrame.MaxValuesDisplayFrame.Title = "最大值";
MainFrame.ThirdRowDummyFrame.DummyDisplayFrame.MaxValuesDisplayFrame.LongHelp = "显示当前连接中的每一连续的最大值";
MainFrame.ThirdRowDummyFrame.DummyDisplayFrame.MaxValuesDisplayFrame.G0Label = "----  ";
MainFrame.ThirdRowDummyFrame.DummyDisplayFrame.MaxValuesDisplayFrame.G1Label = "----  ";
MainFrame.ThirdRowDummyFrame.DummyDisplayFrame.MaxValuesDisplayFrame.G2Label = "----  ";
MainFrame.ThirdRowDummyFrame.DummyDisplayFrame.MaxValuesDisplayFrame.G3Label = "----  ";

MainFrame.FourthRowDummyFrame.ConnectionFrame.Title = "最大间隔";
MainFrame.FourthRowDummyFrame.ConnectionFrame.LongHelp = "定义参数以便搜索输入元素间的连接";
MainFrame.FourthRowDummyFrame.ConnectionFrame.InternalEdgeChkButton.ShortHelp = "内部边线";
MainFrame.FourthRowDummyFrame.ConnectionFrame.InternalEdgeChkButton.LongHelp = "执行分析时考虑接合元素的内部边线";

MainFrame.FourthRowDummyFrame.DiscretizationFrame.Title = "离散化";
MainFrame.FourthRowDummyFrame.DiscretizationFrame.LongHelp = "用户可以在四个预定义的离散化步长间切换";
MainFrame.FourthRowDummyFrame.DiscretizationFrame.LightDiscretRadio.ShortHelp = "轻度离散化";
MainFrame.FourthRowDummyFrame.DiscretizationFrame.LightDiscretRadio.LongHelp = "设置离散化类型为轻度";
MainFrame.FourthRowDummyFrame.DiscretizationFrame.CoarseDiscretRadio.ShortHelp = "粗糙离散化";
MainFrame.FourthRowDummyFrame.DiscretizationFrame.CoarseDiscretRadio.LongHelp = "设置离散化类型为粗糙";
MainFrame.FourthRowDummyFrame.DiscretizationFrame.MediumDiscretRadio.ShortHelp = "中度离散化";
MainFrame.FourthRowDummyFrame.DiscretizationFrame.MediumDiscretRadio.LongHelp = "将离散类型设置为中度";
MainFrame.FourthRowDummyFrame.DiscretizationFrame.FineDiscretRadio.ShortHelp = "精细离散化";
MainFrame.FourthRowDummyFrame.DiscretizationFrame.FineDiscretRadio.LongHelp = "设置离散类型为精细";

