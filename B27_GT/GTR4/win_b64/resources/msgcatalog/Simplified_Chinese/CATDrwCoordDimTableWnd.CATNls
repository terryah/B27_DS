//=============================================================================
//                                     CNEXT - CXR2
//                          COPYRIGHT DASSAULT SYSTEMES 1999 
//-----------------------------------------------------------------------------
// FILENAME    :    CATDrwCoordDimTableCmd
// LOCATION    :    DraftingIntCommands/CNext/resources/msgcatalog
// AUTHOR      :    HBH
// DATE        :    Octobre 2001
//------------------------------------------------------------------------------
// DESCRIPTION :    Resources File for NLS purpose related to Interactive
//                  Drafting Commands.
//                  
//------------------------------------------------------------------------------
// COMMENTS    :
//------------------------------------------------------------------------------
//MODIFICATION     user  date          purpose
//   HISTORY       
//    01		lgk	09.10.2002 Revision
//------------------------------------------------------------------------------

Title="轴系和表参数";

_axisSystemComboFrame._axisSystemLabel.Title="轴系： ";

_axisSystemFrame._xLabel.Title="X： ";
_axisSystemFrame._yLabel.Title="Y： ";
_axisSystemFrame._zLabel.Title="Z： ";
_axisSystemFrame._xSpinner.LongHelp="轴系原点的 X 坐标。";
_axisSystemFrame._ySpinner.LongHelp="轴系原点的 Y 坐标。";
_axisSystemFrame._zSpinner.LongHelp="轴系原点的 Z 坐标。";

_axisSystemFrame._angleFrame._angleLabel.Title="角度： ";
_axisSystemFrame._angleFrame._angleLabel.LongHelp="定义 2D 轴系的旋转角度。";

_axisSystemFrame._flipFrame._flipLabel.Title="翻转： ";
_axisSystemFrame._flipFrame._flipHorizontalPush.ShortHelp="水平翻转";
_axisSystemFrame._flipFrame._flipHorizontalPush.LongHelp="水平翻转 2D 轴系。";
_axisSystemFrame._flipFrame._flipVerticalPush.ShortHelp="垂直翻转";
_axisSystemFrame._flipFrame._flipVerticalPush.LongHelp="垂直翻转 2D 轴系。";

_axisSystemFrame._axisCreationCheck.Title="创建展示";
_axisSystemFrame._axisCreationCheck.LongHelp="创建 2D 轴系的展示。";

_tableContentTitle.Title="表内容";

_tableTitleFrame._tableTitleLabel.Title="标题： ";
_tableTitleFrame._tableTitleEditor.LongHelp="表标题";

_tableFormatTitle.Title="表格式";
_tableFormatFrame._InvertCheckButton.Title="转置表";
_tableFormatFrame._InvertCheckButton.LongHelp="反转表的行和列";
_tableFormatFrame._sortFrame._sortCheck.Title="对表内容进行排序";
_tableFormatFrame._sortFrame._sortCheck.LongHelp="根据指定条件对表的列进行排序。";
_tableFormatFrame._sortFrame._sortPush.Title="...";
_tableFormatFrame._sortFrame._sortPush.LongHelp="根据指定条件对表的列进行排序。";

_tableFormatFrame._splitFrame._splitCheck.Title="分割表";
_tableFormatFrame._splitFrame._splitCheck.LongHelp="将表分割为多个表。";
_tableFormatFrame._splitFrame._splitPush.Title="...";
_tableFormatFrame._splitFrame._splitPush.LongHelp="将表分割为多个表。";

ColumnsFrame.Title2.Title="标题";
ColumnsFrame.Column.Title="列";

ReferenceComboNone="无";
ReferenceComboUpperCase="标注：A, B, C, ...";
ReferenceComboLowerCase="标注：a, b, c, ...";
ReferenceComboIndex="索引：1, 2, 3, ...";

ColumnsFrame.X.Title="X";
ColumnsFrame.Y.Title="Y";
ColumnsFrame.Z.Title="Z";
REF="参考";
XTITLE="X";
YTITLE="Y";
ZTITLE="Z";
ColumnsFrame._ReferenceOption.Title="...";
ColumnsFrame._ReferenceOption.LongHelp="表示参考的第一个字母
或第一个索引。";
