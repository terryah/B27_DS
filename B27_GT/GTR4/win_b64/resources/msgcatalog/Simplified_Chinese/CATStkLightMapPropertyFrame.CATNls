// COPYRIGHT DASSAULT SYSTEMES 2001
//===================================================================
//
// CATStkLightMapPropertyFrame.CATNls
// The dialog : CATStkLightMapPropertyFrame
//
//===================================================================
//
// Usage notes:
//
//===================================================================
//
//  03 Oct 2001  Creation STX
//===================================================================

panelFrame.propertiesFrame.selectorFrame.selectorButtonsFrame.nbSelectionLabel.Title = "选择";

Face  = " 元素";
Faces = " 元素";

ApplyErrorTitle = "错误";
ApplyErrorMsg   = "贴画只能应用于“产品”";

EditErrorTitle = "错误";
EditErrorMsg   = "未选定贴画";

panelFrame.containerFrame.textureFrame.textureLabel.Title = "纹理";
panelFrame.containerFrame.textureFrame.textureLabel.Help = "定义阴影贴画纹理的路径";
panelFrame.containerFrame.textureFrame.textureLabel.ShortHelp = "定义阴影贴画纹理";
panelFrame.containerFrame.textureFrame.textureLabel.LongHelp =
"定义阴影贴画纹理的路径。
渲染阴影贴画时，
该文件无需存在，因此即将创建该文件。";

panelFrame.containerFrame.textureFrame.textureSelector.Help = "定义阴影贴画纹理的路径";
panelFrame.containerFrame.textureFrame.textureSelector.ShortHelp = "定义阴影贴画纹理";
panelFrame.containerFrame.textureFrame.textureSelector.LongHelp =
"定义阴影贴画纹理的路径。
渲染阴影贴画时，
该文件无需存在，因此即将创建该文件。";

panelFrame.containerFrame.textureFrame.sizeLabel.Title = "大小";
panelFrame.containerFrame.textureFrame.sizeLabel.Help = "定义纹理的大小";
panelFrame.containerFrame.textureFrame.sizeLabel.ShortHelp = "纹理大小";
panelFrame.containerFrame.textureFrame.sizeLabel.LongHelp =
"定义阴影贴画的大小（单位：像素）。
渲染结束后，创建阴影贴画时
需要使用大小。";

panelFrame.containerFrame.textureFrame.sizeFrame.widthSpinner.Help = "定义纹理的大小";
panelFrame.containerFrame.textureFrame.sizeFrame.widthSpinner.ShortHelp = "纹理大小";
panelFrame.containerFrame.textureFrame.sizeFrame.widthSpinner.LongHelp =
"定义阴影贴画的大小（单位：像素）。
渲染结束后，创建阴影贴画时
需要使用大小。";

panelFrame.containerFrame.textureFrame.sizeFrame.heightSpinner.Help = "定义纹理的大小";
panelFrame.containerFrame.textureFrame.sizeFrame.heightSpinner.ShortHelp = "纹理大小";
panelFrame.containerFrame.textureFrame.sizeFrame.heightSpinner.LongHelp =
"定义阴影贴画的大小（单位：像素）。
渲染结束后，创建阴影贴画时
需要使用大小。";

panelFrame.containerFrame.textureFrame.sizeFrame.sizeXLabel.Title = " x ";
panelFrame.containerFrame.textureFrame.sizeFrame.pixelsLabel.Title = " 像素";

panelFrame.containerFrame.textureFrame.useNormalCheck.Title = "使用法线";
panelFrame.containerFrame.textureFrame.useNormalCheck.Help = "使用曲面法线";
panelFrame.containerFrame.textureFrame.useNormalCheck.ShortHelp = "使用曲面法线";
panelFrame.containerFrame.textureFrame.useNormalCheck.LongHelp =
"使用曲面法线。 
选中该选项后，仅计算曲面外部面
的照明，因而
加快了渲染速度。";

panelFrame.containerFrame.textureFrame.reverseNormalCheck.Title = "反转法线";
panelFrame.containerFrame.textureFrame.reverseNormalCheck.Help = "反转法线方向";
panelFrame.containerFrame.textureFrame.reverseNormalCheck.ShortHelp = "反转法线方向";
panelFrame.containerFrame.textureFrame.reverseNormalCheck.LongHelp =
"反转法线方向。";

panelFrame.containerFrame.textureFrame.transparencyLabel.Title = "透明度";
panelFrame.containerFrame.textureFrame.transparencyLabel.Help = "定义纹理透明度";
panelFrame.containerFrame.textureFrame.transparencyLabel.ShortHelp = "纹理透明度";
panelFrame.containerFrame.textureFrame.transparencyLabel.LongHelp =
"定义纹理透明度。";

panelFrame.containerFrame.textureFrame.transparencySlider.Help = "定义纹理透明度";
panelFrame.containerFrame.textureFrame.transparencySlider.ShortHelp = "纹理透明度";
panelFrame.containerFrame.textureFrame.transparencySlider.LongHelp =
"定义纹理透明度。";

panelFrame.containerFrame.textureFrame.optionsFrame.Title = "效果";

panelFrame.containerFrame.textureFrame.optionsFrame.shadowsRadio.Title = "对象阴影";
panelFrame.containerFrame.textureFrame.optionsFrame.shadowsRadio.Help = "渲染对象阴影";
panelFrame.containerFrame.textureFrame.optionsFrame.shadowsRadio.ShortHelp = "仅对象阴影";
panelFrame.containerFrame.textureFrame.optionsFrame.shadowsRadio.LongHelp =
"渲染纹理时仅计算对象阴影。";

panelFrame.containerFrame.textureFrame.optionsFrame.occlusionRadio.Title = "环境遮挡";
panelFrame.containerFrame.textureFrame.optionsFrame.occlusionRadio.Help = "渲染环境遮挡";
panelFrame.containerFrame.textureFrame.optionsFrame.occlusionRadio.ShortHelp = "仅环境遮挡";
panelFrame.containerFrame.textureFrame.optionsFrame.occlusionRadio.LongHelp =
"渲染纹理时仅计算环境遮挡。";

panelFrame.containerFrame.textureFrame.optionsFrame.bothRadio.Title = "对象阴影和环境遮挡";
panelFrame.containerFrame.textureFrame.optionsFrame.bothRadio.Help = "渲染对象阴影和环境遮挡";
panelFrame.containerFrame.textureFrame.optionsFrame.bothRadio.ShortHelp = "对象阴影和环境遮挡";
panelFrame.containerFrame.textureFrame.optionsFrame.bothRadio.LongHelp =
"渲染纹理时将计算对象阴影
和环境遮挡。";

panelFrame.containerFrame.textureFrame.optionsFrame.samplesFrame.samplesLabel.Title = "环境遮挡示例";
panelFrame.containerFrame.textureFrame.optionsFrame.samplesFrame.samplesLabel.Help = "环境遮挡示例";
panelFrame.containerFrame.textureFrame.optionsFrame.samplesFrame.samplesLabel.ShortHelp = "环境遮挡示例";
panelFrame.containerFrame.textureFrame.optionsFrame.samplesFrame.samplesLabel.LongHelp =
"环境遮挡示例。";

panelFrame.containerFrame.textureFrame.optionsFrame.samplesFrame.samplesSpinner.Help = "环境遮挡示例";
panelFrame.containerFrame.textureFrame.optionsFrame.samplesFrame.samplesSpinner.ShortHelp = "环境遮挡示例";
panelFrame.containerFrame.textureFrame.optionsFrame.samplesFrame.samplesSpinner.LongHelp =
"环境遮挡示例。";

