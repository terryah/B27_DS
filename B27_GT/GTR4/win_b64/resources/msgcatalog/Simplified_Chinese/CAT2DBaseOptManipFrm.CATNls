//=====================================================================================
//                                     CNEXT - CXRn
//                          COPYRIGHT DASSAULT SYSTEMES 1999 
//-------------------------------------------------------------------------------------
// FILENAME    :    CATDrwFrameManipulator
// LOCATION    :    DraftingUI
// AUTHOR      :    fgx
// BUT         :    
// DATE        :    26.11.1999
//-------------------------------------------------------------------------------------
// DESCRIPTION :    Tools/Option du Drafting
//-------------------------------------------------------------------------------------
// COMMENTS    :    
//-------------------------------------------------------------------------------------
// MODIFICATIONS     user  date        purpose
//    HISTORY        ----  ----        -------
//=====================================================================================

frameManip.HeaderFrame.Global.Title="操作器 ";
frameManip.HeaderFrame.Global.LongHelp="定义用来显示操作器的
默认参数。";

frameManip.IconAndOptionsFrame.OptionsFrame.labelManipRefSize.Title="参考大小： ";
frameManip.IconAndOptionsFrame.OptionsFrame.labelManipRefSize.LongHelp="定义用于显示操作器
的参考大小。";
frameManip.IconAndOptionsFrame.OptionsFrame.editorManipRefSize.LongHelp="定义用于显示操作器
的参考大小。";

frameManip.IconAndOptionsFrame.OptionsFrame.checkManipZoom.Title="可缩放 ";
frameManip.IconAndOptionsFrame.OptionsFrame.checkManipZoom.LongHelp="定义操作器是否像几何图形那样可缩放。";


frameRotation.HeaderFrame.Global.Title="旋转";
frameRotation.HeaderFrame.Global.LongHelp="旋转
定义使用“选择”-“旋转”命令旋转元素时
要考虑的参数。";
frameRotation.IconAndOptionsFrame.OptionsFrame.checkRotSnapAuto.Title="自动捕捉";
frameRotation.IconAndOptionsFrame.OptionsFrame.labelRotSnapAngle.Title="旋转捕捉角度： ";
frameRotation.IconAndOptionsFrame.OptionsFrame.labelRotSnapAngle.LongHelp="旋转捕捉角度
定义使用“选择”-“旋转”命令旋转元素时
要考虑的捕捉值。";


frameOrientation.HeaderFrame.Global.Title="方向";
frameOrientation.HeaderFrame.Global.LongHelp="定义确定引出线方向时所使用的参数。";
frameOrientation.IconAndOptionsFrame.OptionsFrame.checkSnapOnDirection.Title="捕捉优先方向";
frameOrientation.IconAndOptionsFrame.OptionsFrame.checkSnapOnDirection.LongHelp="捕捉优先方向上的标注引出线或箭头（水平或垂直，与参考垂直或相切）。";


frameDim.HeaderFrame.Global.Title="尺寸操作器";
frameDim.HeaderFrame.Global.LongHelp="定义在创建和/或修改尺寸时
要显示的默认操作器。";

frameDim.IconAndOptionsFrame.OptionsFrame._labelDimCreation.Title="创建";
frameDim.IconAndOptionsFrame.OptionsFrame._labelDimCreation.LongHelp="定义在修改尺寸时
要显示的操作器。";
frameDim.IconAndOptionsFrame.OptionsFrame._labelDimModification.Title="修改";
frameDim.IconAndOptionsFrame.OptionsFrame._labelDimModification.LongHelp="定义在修改尺寸时
要显示的操作器。";

frameDim.IconAndOptionsFrame.OptionsFrame._labelDimORun.Title="修改超限： ";
frameDim.IconAndOptionsFrame.OptionsFrame._labelDimORun.LongHelp="通过修改超限操作器，您可以交互地修改
每条尺寸界线的超限。";

frameDim.IconAndOptionsFrame.OptionsFrame._labelDimBlank.Title="修改消隐： ";
frameDim.IconAndOptionsFrame.OptionsFrame._labelDimBlank.LongHelp="通过修改消隐操作器，您可以交互地修改
每条尺寸界线的消隐。";

frameDim.IconAndOptionsFrame.OptionsFrame._labelDimBefore.Title="在此之前插入文本： ";
frameDim.IconAndOptionsFrame.OptionsFrame._labelDimBefore.LongHelp="通过在操作器之前插入文本，您可以轻松地
在尺寸主值之前插入文本。";

frameDim.IconAndOptionsFrame.OptionsFrame._labelDimAfter.Title="在此之后插入文本： ";
frameDim.IconAndOptionsFrame.OptionsFrame._labelDimAfter.LongHelp="通过在操作器之后插入文本，您可以轻松地
在尺寸主值之后插入文本。";

frameDim.IconAndOptionsFrame.OptionsFrame._labelDimMoveValue.Title="移动值： ";
frameDim.IconAndOptionsFrame.OptionsFrame._labelDimMoveValue.LongHelp="通过移动值操作器，您可以轻松地
移动尺寸值（且只限此值）。";

frameDim.IconAndOptionsFrame.OptionsFrame._labelDimMoveDimLine.Title="移动尺寸线： ";
frameDim.IconAndOptionsFrame.OptionsFrame._labelDimMoveDimLine.LongHelp="通过移动尺寸线操作器，您可以轻松地
移动尺寸线（且只限此线）。";

frameDim.IconAndOptionsFrame.OptionsFrame._labelDimMove2dPart.Title="移动尺寸线次要零件： ";
frameDim.IconAndOptionsFrame.OptionsFrame._labelDimMove2dPart.LongHelp="通过移动尺寸线次要零件操纵器，使您可以轻松地
移动尺寸线的次要零件（且只限此零件）。";


frameDim.IconAndOptionsFrame.OptionsFrame._labelDimMoveLeader.Title="移动尺寸引出线 ";
frameDim.IconAndOptionsFrame.OptionsFrame._labelDimMoveLeader.LongHelp="通过移动尺寸引出线操作器，您可以轻松地
移动引出线（且只限此零件）。";

frameAnnotManip.HeaderFrame.Global.Title="标注操作器";
frameAnnotManip.HeaderFrame.Global.LongHelp="定义选择和/或编辑文本标注时要显示的默认操作器。";

frameAnnotManip.IconAndOptionsFrame.OptionsFrame._labelAnnotSelection.Title="选择";
frameAnnotManip.IconAndOptionsFrame.OptionsFrame._labelAnnotSelection.LongHelp="定义选择标注时要显示的操作器。";
frameAnnotManip.IconAndOptionsFrame.OptionsFrame._labelAnnotEdition.Title="编辑文本";
frameAnnotManip.IconAndOptionsFrame.OptionsFrame._labelAnnotEdition.LongHelp="定义编辑标注时要显示的操作器。";

frameAnnotManip.IconAndOptionsFrame.OptionsFrame._labelManipTranslationAnnot.Title="拉伸文本： ";
frameAnnotManip.IconAndOptionsFrame.OptionsFrame._labelManipTranslationAnnot.LongHelp="拉伸文本操作器允许拉伸标注的文本。";

frameAnnotManip.IconAndOptionsFrame.OptionsFrame._labelManipResizeAnnot.Title="调整文本大小： ";
frameAnnotManip.IconAndOptionsFrame.OptionsFrame._labelManipResizeAnnot.LongHelp="调整文本大小操作器允许调整标注大小。";

frameAnnotManip.IconAndOptionsFrame.OptionsFrame._labelManipMoveValueAnnot.Title="滑动文本： ";
frameAnnotManip.IconAndOptionsFrame.OptionsFrame._labelManipMoveValueAnnot.LongHelp="滑动文本操作器允许滑动文本（仅文本，非文本引出线）。";

frameAnnotManip.IconAndOptionsFrame.OptionsFrame._labelManipTranslationLeader.Title="调整引出线附件大小： ";
frameAnnotManip.IconAndOptionsFrame.OptionsFrame._labelManipTranslationLeader.LongHelp="调整引出线附件大小操作器允许调整引出线附件大小（且仅调整附件大小）。";

frameAnnotManip.IconAndOptionsFrame.OptionsFrame._labelManipMoveLeaderAnchor.Title="移动引出线定位点： ";
frameAnnotManip.IconAndOptionsFrame.OptionsFrame._labelManipMoveLeaderAnchor.LongHelp="移动引出线定位点操作器允许移动引出线的定位点（且仅移动定位点）。";

frameAnnotManip.IconAndOptionsFrame.OptionsFrame._labelManipMoveLeader.Title="移动引出线： ";
frameAnnotManip.IconAndOptionsFrame.OptionsFrame._labelManipMoveLeader.LongHelp="移动引出线操作器允许移动引出线端点（且仅移动引出线端点）。";

frameAnnotManip.IconAndOptionsFrame.OptionsFrame._labelManipMovableDatum.Title="指示可移动基准： ";
frameAnnotManip.IconAndOptionsFrame.OptionsFrame._labelManipMovableDatum.LongHelp="指示可移动基准句柄，让您可指示基准是否可移动（仅可用于基准目标）。";


