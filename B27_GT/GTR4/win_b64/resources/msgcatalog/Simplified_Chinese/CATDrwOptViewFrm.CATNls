//=====================================================================================
//                                     CNEXT - CXRn
//                          COPYRIGHT DASSAULT SYSTEMES 2000 
//-------------------------------------------------------------------------------------
// FILENAME    :    CATDrwOptManip
// LOCATION    :    DraftingUI
// AUTHOR      :    fgx
// BUT         :    
// DATE        :    30.10.2000
//-------------------------------------------------------------------------------------
// DESCRIPTION :    Tools/Option du Drafting
//-------------------------------------------------------------------------------------
// COMMENTS    :    
//-------------------------------------------------------------------------------------
// MODIFICATIONS     user  date        purpose
//    HISTORY        ----  ----        -------
//	01			lgk		17.12.02	Revision
//=====================================================================================

Title="视图";


frameGenElem.HeaderFrame.Global.Title="生成/修饰几何图形";
frameGenElem.HeaderFrame.Global.LongHelp=
"生成几何图形
定义生成哪些几何元素。";
frameGenElem.IconAndOptionsFrame.OptionsFrame._checkGenAxis.Title="生成轴";
frameGenElem.IconAndOptionsFrame.OptionsFrame._checkGenAxis.LongHelp="生成轴线";
frameGenElem.IconAndOptionsFrame.OptionsFrame._checkGenCenter.Title="生成中心线";
frameGenElem.IconAndOptionsFrame.OptionsFrame._checkGenCenter.LongHelp="生成中心线";
frameGenElem.IconAndOptionsFrame.OptionsFrame._checkGenThread.Title="生成螺纹";
frameGenElem.IconAndOptionsFrame.OptionsFrame._checkGenThread.LongHelp="生成螺纹";
frameGenElem.IconAndOptionsFrame.OptionsFrame._checkGenHiddenLines.Title="生成隐藏线";
frameGenElem.IconAndOptionsFrame.OptionsFrame._checkGenHiddenLines.LongHelp="生成隐藏线";
frameGenElem.IconAndOptionsFrame.OptionsFrame.frameButtonGenFil._checkGenFil.Title="生成圆角";
frameGenElem.IconAndOptionsFrame.OptionsFrame.frameButtonGenFil._checkGenFil.LongHelp="生成圆角";
frameGenElem.IconAndOptionsFrame.OptionsFrame.frameButtonGenFil.pushGenFil.Title="配置";
frameGenElem.IconAndOptionsFrame.OptionsFrame.frameButtonGenFil.pushGenFil.LongHelp="配置圆角投影";
frameGenElem.IconAndOptionsFrame.OptionsFrame._checkGen3DColors.Title="继承 3D 颜色";
frameGenElem.IconAndOptionsFrame.OptionsFrame._checkGen3DColors.LongHelp="继承 3D 颜色";
frameGenElem.IconAndOptionsFrame.OptionsFrame.frmWireFrame.checkGenWireFrame.Title="投影 3D 线框";
frameGenElem.IconAndOptionsFrame.OptionsFrame.frmWireFrame.checkGenWireFrame.LongHelp="投影 3D 线框";
frameGenElem.IconAndOptionsFrame.OptionsFrame.frmWireFrame.pushGenWireFrameMod.Title="配置";
frameGenElem.IconAndOptionsFrame.OptionsFrame.frmWireFrame.pushGenWireFrameMod.LongHelp="配置线框投影";
frameGenElem.IconAndOptionsFrame.OptionsFrame.checkGenApplyUncut.Title="应用 3D 规格";
frameGenElem.IconAndOptionsFrame.OptionsFrame.checkGenApplyUncut.LongHelp="应用 3D 规格";
frameGenElem.IconAndOptionsFrame.OptionsFrame.frameButtonLinetype.labelLinetype.Title="视图线型";
frameGenElem.IconAndOptionsFrame.OptionsFrame.frameButtonLinetype.labelLinetype.LongHelp="视图线型";
frameGenElem.IconAndOptionsFrame.OptionsFrame.frameButtonLinetype.linetypePushButton.Title="配置";
frameGenElem.IconAndOptionsFrame.OptionsFrame.frameButtonLinetype.linetypePushButton.LongHelp="配置线型和线宽";
frameGenElem.IconAndOptionsFrame.OptionsFrame.frameProject3DPts.checkProject3DPts.Title="投影 3D 点";
frameGenElem.IconAndOptionsFrame.OptionsFrame.frameProject3DPts.checkProject3DPts.LongHelp="投影 3D 点";
frameGenElem.IconAndOptionsFrame.OptionsFrame.frameProject3DPts.project3DPtsPushButton.Title="配置";
frameGenElem.IconAndOptionsFrame.OptionsFrame.frameProject3DPts.project3DPtsPushButton.LongHelp="配置 3D 点投影";

genGeomMngt.HeaderFrame.Global.Title="生成的几何图形";
genGeomMngt.HeaderFrame.Global.LongHelp="生成的几何图形";
genGeomMngt.IconAndOptionsFrame.OptionsFrame.chkGMECreation.Title="保留在几何图形上手动设置的图形修饰";
genGeomMngt.IconAndOptionsFrame.OptionsFrame.chkGMECreation.LongHelp="保留在几何图形上手动设置的图形修饰。";


frameGenView.HeaderFrame.Global.Title="生成视图";
frameGenView.HeaderFrame.Global.LongHelp="定义用于创建创成式视图的参数。";        
frameGenView.IconAndOptionsFrame.OptionsFrame.frmViewMod.lbl.Title="视图生成模式";
frameGenView.IconAndOptionsFrame.OptionsFrame.frmViewMod.lbl.LongHelp="定义将如何生成视图";
frameGenView.IconAndOptionsFrame.OptionsFrame.frmViewMod.combViewMod.LongHelp="定义将如何生成视图";
ExactView="精确视图";
CGRView="CGR";
PictureView="光栅";
HRV="近似";
frameGenView.IconAndOptionsFrame.OptionsFrame.frmViewMod.pushRasterOptions.Title="配置";
frameGenView.IconAndOptionsFrame.OptionsFrame.frmViewMod.pushRasterOptions.LongHelp="配置光栅选项";
frameGenView.IconAndOptionsFrame.OptionsFrame.frmViewMod.frmPict.chkAutoLOD.Title="自动细节级别";
frameGenView.IconAndOptionsFrame.OptionsFrame.frmViewMod.frmPict.chkAutoLOD.LongHelp="自动适合要可视化和
打印的 DPI。";
frameGenView.IconAndOptionsFrame.OptionsFrame.frmViewMod.frmPict.lbl1.Title="要可视化的 DPI";
frameGenView.IconAndOptionsFrame.OptionsFrame.frmViewMod.frmPict.lbl1.LongHelp="要可视化的 DPI";
frameGenView.IconAndOptionsFrame.OptionsFrame.frmViewMod.frmPict.lbl2.Title="要打印的 DPI";
frameGenView.IconAndOptionsFrame.OptionsFrame.frmViewMod.frmPict.lbl2.LongHelp="要打印的 DPI";
frameGenView.IconAndOptionsFrame.OptionsFrame.chkExactPreview.Title="视图生成的精确预览";
frameGenView.IconAndOptionsFrame.OptionsFrame.chkExactPreview.LongHelp="即使正在以可视化模式工作，
您也可以以设计模式预览视图。
如果不选中此选项，则使用
嵌入在 3D 文档中的可视化数据进行预览。";
frameGenView.IconAndOptionsFrame.OptionsFrame.frmGenBox.checkGenBox.Title="仅生成大于以下值的零件";
frameGenView.IconAndOptionsFrame.OptionsFrame.frmGenBox.checkGenBox.LongHelp="仅生成大于指定大小的零件。";
frameGenView.IconAndOptionsFrame.OptionsFrame.chkOcclusionCulling.Title="启用遮挡剔除";
frameGenView.IconAndOptionsFrame.OptionsFrame.chkOcclusionCulling.LongHelp="激活或取消激活遮挡剔除。 
遮挡剔除通过避免重新显示隐藏元素来提高显示性能，
这在查看诸如厂房和建筑物之类的高度分隔的场景时尤其有用。";
frameGenView.IconAndOptionsFrame.OptionsFrame.chkSelBodyInAss.Title="在装配中选择几何体";
frameGenView.IconAndOptionsFrame.OptionsFrame.chkSelBodyInAss.LongHelp="激活或取消激活装配中的几何体选择。&#x2028;";

frameGenView.IconAndOptionsFrame.OptionsFrame._frameViewStatus._labelViewStatus.Title="近似/光栅/CGR 视图更新状态";
frameGenView.IconAndOptionsFrame.OptionsFrame._frameViewStatus._labelViewStatus.LongHelp="定义要使用的产品信息，以确定是否需要更新视图";
frameGenView.IconAndOptionsFrame.OptionsFrame._frameViewStatus._frameViewStatusRadio._frameViewStatusRadiolocked._radioViewUpdateStatusDataVisu.Title="基于可视化数据";
frameGenView.IconAndOptionsFrame.OptionsFrame._frameViewStatus._frameViewStatusRadio._frameViewStatusRadiolocked._radioViewUpdateStatusDataVisu.LongHelp="使用零件可视化数据时间戳（cgr 数据）";
frameGenView.IconAndOptionsFrame.OptionsFrame._frameViewStatus._frameViewStatusRadio._frameViewStatusRadiolocked._radioViewUpdateStatusDataDesign.Title="基于设计数据";
frameGenView.IconAndOptionsFrame.OptionsFrame._frameViewStatus._frameViewStatusRadio._frameViewStatusRadiolocked._radioViewUpdateStatusDataDesign.LongHelp="使用零件设计数据时间戳";


frameClipping.HeaderFrame.Global.Title="裁剪视图";
frameClipping.HeaderFrame.Global.LongHelp="定义在剪裁视图中使用的参数。";        
frameClipping.IconAndOptionsFrame.OptionsFrame.chkDimClipNoShow.Title="将不显示尺寸放到非可视几何图形上";
frameClipping.IconAndOptionsFrame.OptionsFrame.chkDimClipNoShow.LongHelp="将不显示尺寸放到非可视几何图形以及与位置关联的标注上。";

frameView3D.HeaderFrame.Global.Title="3D 视图";
frameView3D.HeaderFrame.Global.LongHelp="定义从 3D 中提取的视图中使用的参数。";        
frameView3D.IconAndOptionsFrame.OptionsFrame.chkFTAAnnotLay.Title="保留 2D 提取标注的布局和修饰";
frameView3D.IconAndOptionsFrame.OptionsFrame.chkFTAAnnotLay.LongHelp="保留 2D 提取标注的布局和修饰。";
frameView3D.IconAndOptionsFrame.OptionsFrame.chkGeomOf2DLView.Title="生成 2D 几何图形";
frameView3D.IconAndOptionsFrame.OptionsFrame.chkGeomOf2DLView.LongHelp="从布局视图中生成 2D 几何图形。";
frameView3D.IconAndOptionsFrame.OptionsFrame.chkGenRedCross.Title="在标注上生成红叉";
frameView3D.IconAndOptionsFrame.OptionsFrame.chkGenRedCross.LongHelp="若未提取标注的几何图形或
其几何图形隐藏在当前视图中，
则用红叉标记从 3D 中提取的标注。";
frameView3D.IconAndOptionsFrame.OptionsFrame.chkSyncUpdate.Title="更新时同步";
frameView3D.IconAndOptionsFrame.OptionsFrame.chkSyncUpdate.LongHelp="更新时，同步工程图中的所有视图和图纸。";


viewGenFillet.Title="生成圆角";
viewGenFillet.frameButtonGenFil.HeaderFrame.Global.Title="配置圆角生成";
viewGenFillet.frameButtonGenFil.HeaderFrame.Global.LongHelp="配置圆角生成
您可以配置圆角生成。";
viewGenFillet.frameButtonGenFil.IconAndOptionsFrame.OptionsFrame.frameRadioGenFil.frameRadioGenFillocked.radioBoundaries.Title="边界";
viewGenFillet.frameButtonGenFil.IconAndOptionsFrame.OptionsFrame.frameRadioGenFil.frameRadioGenFillocked.radioBoundaries.LongHelp="边界
定义是否要在视图上显示“圆角边界”。";
viewGenFillet.frameButtonGenFil.IconAndOptionsFrame.OptionsFrame.frameRadioGenFil.frameRadioGenFillocked.radioSymbo.Title="符号";
viewGenFillet.frameButtonGenFil.IconAndOptionsFrame.OptionsFrame.frameRadioGenFil.frameRadioGenFillocked.radioSymbo.LongHelp="符号
定义是否要在视图上显示“符号圆角”。";
viewGenFillet.frameButtonGenFil.IconAndOptionsFrame.OptionsFrame.frameRadioGenFil.frameRadioGenFillocked.radioProjEd.Title="投影的原始边线";
viewGenFillet.frameButtonGenFil.IconAndOptionsFrame.OptionsFrame.frameRadioGenFil.frameRadioGenFillocked.radioProjEd.LongHelp="投影的原始边线
定义是否要在视图上显示“投影的原始边线”。";
viewGenFillet.frameButtonGenFil.IconAndOptionsFrame.OptionsFrame.frameRadioGenFil.frameRadioGenFillocked.radioOrigEd.Title="近似原始边线";
viewGenFillet.frameButtonGenFil.IconAndOptionsFrame.OptionsFrame.frameRadioGenFil.frameRadioGenFillocked.radioOrigEd.LongHelp="近似原始边线
定义是否要在视图上显示“原始边线”。";


viewGenWireFrame.Title="3D 线框投影模式";
viewGenWireFrame.frmWireFrameMod.HeaderFrame.Global.Title="投影的 3D 线框";
viewGenWireFrame.frmWireFrameMod.HeaderFrame.Global.LongHelp="投影的 3D 线框\n指示投影的 3D 线框\n可隐藏还是始终可视。";
viewGenWireFrame.frmWireFrameMod.IconAndOptionsFrame.OptionsFrame.radioFrmWireFrameMod.radioFrmWireFrameModlocked.radioHidden.Title="可隐藏";
viewGenWireFrame.frmWireFrameMod.IconAndOptionsFrame.OptionsFrame.radioFrmWireFrameMod.radioFrmWireFrameModlocked.radioHidden.LongHelp="可隐藏\n指示投影的 3D 线框可隐藏。";
viewGenWireFrame.frmWireFrameMod.IconAndOptionsFrame.OptionsFrame.radioFrmWireFrameMod.radioFrmWireFrameModlocked.radioVisible.Title="始终可视";
viewGenWireFrame.frmWireFrameMod.IconAndOptionsFrame.OptionsFrame.radioFrmWireFrameMod.radioFrmWireFrameModlocked.radioVisible.LongHelp="始终可视\n指示投影的 3D 线框始终可视。";


viewGen3DPoints.Title="3D 点投影";
viewGen3DPoints.frameButtonProject3DPts.HeaderFrame.Global.Title="投影 3D 点";
viewGen3DPoints.frameButtonProject3DPts.HeaderFrame.Global.LongHelp="投影 3D 点
指示投影的 3D 点显示的是继承自 3D 的符号
还是显示选定的符号。";
viewGen3DPoints.frameButtonProject3DPts.IconAndOptionsFrame.OptionsFrame.frameRadio3DSymbol.frameRadio3DSymbollocked.radioSymbolInherit.Title="3D 符号继承";
viewGen3DPoints.frameButtonProject3DPts.IconAndOptionsFrame.OptionsFrame.frameRadio3DSymbol.frameRadio3DSymbollocked.radioSymbolInherit.LongHelp="3D 符号继承
显示继承自 3D 的符号。";
viewGen3DPoints.frameButtonProject3DPts.IconAndOptionsFrame.OptionsFrame.frameRadio3DSymbol.frameRadio3DSymbollocked.radioSymbol.Title="符号";
viewGen3DPoints.frameButtonProject3DPts.IconAndOptionsFrame.OptionsFrame.frameRadio3DSymbol.frameRadio3DSymbollocked.radioSymbol.LongHelp="符号
显示选定的符号。";
viewGen3DPoints.frameButtonProject3DPts.IconAndOptionsFrame.OptionsFrame.frameRadio3DSymbol.frameRadio3DSymbollocked.comboSymbol.Title="符号";
viewGen3DPoints.frameButtonProject3DPts.IconAndOptionsFrame.OptionsFrame.frameRadio3DSymbol.frameRadio3DSymbollocked.comboSymbol.LongHelp="符号
显示选定的符号。";


viewLineType.Title="线型和线宽";
viewLineType.frameButtonLinetype.HeaderFrame.Global.Title="线型和线宽";
viewLineType.frameButtonLinetype.HeaderFrame.Global.LongHelp="线型和线宽";
viewLineType.frameButtonLinetype.IconAndOptionsFrame.OptionsFrame.lblSection.Title="剖视图";
viewLineType.frameButtonLinetype.IconAndOptionsFrame.OptionsFrame.lblSection.LongHelp="剖视图";
viewLineType.frameButtonLinetype.IconAndOptionsFrame.OptionsFrame.lblDetail.Title="详细视图";
viewLineType.frameButtonLinetype.IconAndOptionsFrame.OptionsFrame.lblDetail.LongHelp="详细视图";
viewLineType.frameButtonLinetype.IconAndOptionsFrame.OptionsFrame.lblBroken.Title="局部视图";
viewLineType.frameButtonLinetype.IconAndOptionsFrame.OptionsFrame.lblBroken.LongHelp="局部视图";
viewLineType.frameButtonLinetype.IconAndOptionsFrame.OptionsFrame.lblBreakout.Title="剖面视图";
viewLineType.frameButtonLinetype.IconAndOptionsFrame.OptionsFrame.lblBreakout.LongHelp="剖面视图";
viewLineType.frameButtonLinetype.IconAndOptionsFrame.OptionsFrame.lblSkin.Title="蒙皮截面视图";
viewLineType.frameButtonLinetype.IconAndOptionsFrame.OptionsFrame.lblSkin.LongHelp="蒙皮截面视图";
viewLineType.frameButtonLinetype.IconAndOptionsFrame.OptionsFrame.comboSectionLinetype.LongHelp="选择线型";
viewLineType.frameButtonLinetype.IconAndOptionsFrame.OptionsFrame.comboDetailLinetype.LongHelp="选择线型";
viewLineType.frameButtonLinetype.IconAndOptionsFrame.OptionsFrame.comboBrokenLinetype.LongHelp="选择线型";
viewLineType.frameButtonLinetype.IconAndOptionsFrame.OptionsFrame.comboBreakoutLinetype.LongHelp="选择线型";
viewLineType.frameButtonLinetype.IconAndOptionsFrame.OptionsFrame.comboSkinLinetype.LongHelp="选择线型";
viewLineType.frameButtonLinetype.IconAndOptionsFrame.OptionsFrame.comboSectionThickness.LongHelp="选择线宽";
viewLineType.frameButtonLinetype.IconAndOptionsFrame.OptionsFrame.comboDetailThickness.LongHelp="选择线宽";
viewLineType.frameButtonLinetype.IconAndOptionsFrame.OptionsFrame.comboBrokenThickness.LongHelp="选择线宽";
viewLineType.frameButtonLinetype.IconAndOptionsFrame.OptionsFrame.comboBreakoutThickness.LongHelp="选择线宽";
viewLineType.frameButtonLinetype.IconAndOptionsFrame.OptionsFrame.comboSkinThickness.LongHelp="选择线宽";


viewGenModApproximate.Title="近似模式";
viewGenModApproximate.frmHRVOptions.HeaderFrame.Global.Title="近似模式选项";
viewGenModApproximate.frmHRVOptions.HeaderFrame.Global.LongHelp="配置近似模式的选项";
viewGenModApproximate.frmHRVOptions.IconAndOptionsFrame.OptionsFrame.lblHRVLOD.Title = "细节级别：";
viewGenModApproximate.frmHRVOptions.IconAndOptionsFrame.OptionsFrame.lblHRVLOD.LongHelp = "细节级别：";
XGraph = "LOD";
YGraph = "时间";

viewGenModRaster.Title="光栅模式选项";
viewGenModRaster.frmRasterOptions.HeaderFrame.Global.Title="光栅模式选项";
viewGenModRaster.frmRasterOptions.HeaderFrame.Global.LongHelp="配置光栅视图选项";
viewGenModRaster.frmRasterOptions.IconAndOptionsFrame.OptionsFrame.lblRasterMode.Title="模式";
viewGenModRaster.frmRasterOptions.IconAndOptionsFrame.OptionsFrame.lblRasterMode.LongHelp="模式";
viewGenModRaster.frmRasterOptions.IconAndOptionsFrame.OptionsFrame.lblLOD.Title="细节级别：";
viewGenModRaster.frmRasterOptions.IconAndOptionsFrame.OptionsFrame.lblLOD.LongHelp="细节级别：";
viewGenModRaster.frmRasterOptions.IconAndOptionsFrame.OptionsFrame.lblLODVisu.Title="用于可视化";
viewGenModRaster.viewGenModRaster.IconAndOptionsFrame.OptionsFrame.lblLODVisu.LongHelp="用于可视化";
viewGenModRaster.frmRasterOptions.IconAndOptionsFrame.OptionsFrame.lblLODPrint.Title="用于打印";
viewGenModRaster.frmRasterOptions.IconAndOptionsFrame.OptionsFrame.lblLODPrint.LongHelp="用于打印";

HRD = "动态移除隐藏线";
Shading = "着色";
ShadingWithEdges = "含边线着色";
ShadingNoLight = "着色，无光源";
ShadingWithEdgesNoLight = "含边线着色，无光源";

LowQualityMode       = "低质量";
NormalQualityMode    = "正常质量";
HighQualityMode      = "高质量";
CustomizeMode        = "自定义";

