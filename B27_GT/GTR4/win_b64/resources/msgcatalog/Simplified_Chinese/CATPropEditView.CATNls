//=============================================================================
//                                     CNEXT - CXR1
//                          COPYRIGHT DASSAULT SYSTEMES 1998 
//-----------------------------------------------------------------------------
// FILENAME    :    CATPropEditView
// LOCATION    :    DraftingUI/CNext/resources/msgcatalog
// AUTHOR      :    tbe
// DATE        :    14/1/1998
//------------------------------------------------------------------------------
// DESCRIPTION :    Resources File for NLS purpose related to View property
//                  
//------------------------------------------------------------------------------
// COMMENTS    :	ENGLISH VERSION
//------------------------------------------------------------------------------
//MODIFICATIONS     user  date      purpose
//   HISTORY        ----  ----      -------
//		01			ogk		26.01.99	- ajout pour les dressups
//      02          ypr     10/05/00    - ajout pour thread
//		03			ogk		01.09.00	- ajout	viewNameLabel (correction)
//		04			ogk		24.10.00	- nouveau contenu (multi-sel)
//		05			lgk		26.06.02	Revision
//		06			lgk		15.10.02	Revision
//		07			lgk		17.12.02	Revision
//------------------------------------------------------------------------------


// Global Parameters 
// =================
globalParamsFrameTitle.Title="比例和方向";
globalParamsFrame.Title="全局参数";
globalParamsFrame.LongHelp=
"参数
修改视图参数：“旋转”和“缩放”。"; 


// Global Parameters / Scale
// =========================
globalParamsFrame.scaleEditors.scaleLabel.Title="     缩放：";
globalParamsFrame.scaleEditors.scaleLabel.Help="使用正值修改视图比例";
globalParamsFrame.scaleEditors.scaleLabel.ShortHelp="视图比例";
globalParamsFrame.scaleEditors.scaleLabel.LongHelp="视图比例
使用正值修改视图比例。";
//
globalParamsFrame.scaleEditors.scaleSepLabel.Title=":";



// Global Parameters / Angle
// =========================
globalParamsFrame.angleLabel.Title="角度：";
globalParamsFrame.angleLabel.Help="为选定的视图指定一个“旋转”角度值";
globalParamsFrame.angleLabel.ShortHelp="视图角度";
globalParamsFrame.angleLabel.LongHelp="视图角度
为选定的视图指定一个“旋转”角度值。
在默认情况下，该值使用当前会话单元来表示。 
当输入该值时，您无需指定会话单元。但是，
如有需要，您可以临时更改该单元。
（例如，如果当前会话单元为“弧度”，则输入“180 度”而非“3.14”）。 ";
//
globalParamsFrame.angleEdt.Title="角度：";
globalParamsFrame.angleEdt.Help="为选定的视图指定一个“旋转”角度值";
globalParamsFrame.angleEdt.ShortHelp="视图角度";
globalParamsFrame.angleEdt.LongHelp="视图角度
为选定的视图指定一个“旋转”角度值。
在默认情况下，该值使用当前会话单元来表示。 
当输入该值时，您无需指定会话单元。但是，
如有需要，您可以临时更改该单元。
（例如，如果当前会话单元为“弧度”，则输入“180 度”而非“3.14”）。 ";



// Name Parameters 
// ===============
nameParamsFrameTitle.Title="视图名称";
nameParamsFrame.Title="视图名称";
nameParamsFrame.LongHelp="视图名称
修改关于选定视图名称的参数。";


// Name Parameters / Prefix
// ========================
nameParamsFrame.prefixLabel.Title="前缀";
nameParamsFrame.prefixLabel.Help="为选定视图名称指定前缀";
nameParamsFrame.prefixLabel.ShortHelp="名称前缀";
nameParamsFrame.prefixLabel.LongHelp="名称前缀
为选定视图名称指定前缀。";


// Name Parameters / Ident
// =======================
nameParamsFrame.identLabel.Title="ID";
nameParamsFrame.identLabel.Help="为选定视图名称指定 ID";
nameParamsFrame.identLabel.ShortHelp="名称 ID";
nameParamsFrame.identLabel.LongHelp="名称 ID
为选定视图名称指定 ID。";


// Name Parameters / Suffix
// ========================
nameParamsFrame.suffixLabel.Title="后缀";
nameParamsFrame.suffixLabel.Help="为选定视图名称指定后缀";
nameParamsFrame.suffixLabel.ShortHelp="名称后缀";
nameParamsFrame.suffixLabel.LongHelp="名称后缀
为选定视图名称指定后缀。";


// Name Parameters / Name Editor
// =============================
nameParamsFrame.nameCkeEditorFrame.Title="带公式的名称编辑器：";
nameParamsFrame.nameCkeEditorFrame.Name.EnglobingFrame.IntermediateFrame.Label.Title=" ";


// Detail Name 
// ===========
detailParamsFrame.Title="2D 部件";
detailParamsFrame.ShortHelp="2D 部件";


// Detail Name / Editor
// ===========
detailviewTitle.Title="2D 部件";
detailParamsFrame.detailLabel.Title=" ";
detailParamsFrame.detailLabel.Help="为选定的详细视图指定名称";
detailParamsFrame.detailLabel.ShortHelp="详细视图";
detailParamsFrame.detailLabel.LongHelp="详细视图
为选定的详细视图指定名称。";

// Dressup
// =======
dressupTitle.Title="修饰";
generativeParamsFrame.Title="修饰";
generativeParamsFrame.LongHelp=
"修饰
将元素添加到选定的创成式视图上。"; 



// Fillets
// =======
filletFrame.Title="圆角";
filletFrame.LongHelp=
" "; 

// Dressup / Hidden Lines 
// ======================
generativeParamsFrame.hiddenLinesChkBtn.Title="隐藏线";
generativeParamsFrame.hiddenLinesChkBtn.LongHelp="隐藏线
定义是否在选定视图上显示“隐藏线”。";



// Dressup / Axis
// ==============
generativeParamsFrame.axisChkBtn.Title="轴";
generativeParamsFrame.axisChkBtn.LongHelp="轴
定义是否在选定视图上显示“轴”。";


// Dressup / Center Line
// ======================
generativeParamsFrame.centerLinesChkBtn.Title="中心线";
generativeParamsFrame.centerLinesChkBtn.LongHelp="中心线
定义是否在选定视图上显示“中心线”。";



// Dressup / Thread
// ================
generativeParamsFrame.threadChkBtn.Title="螺纹";
//npq: 18/10/2005 IR 0513559 LongHelp added
generativeParamsFrame.threadChkBtn.LongHelp="螺纹
定义是否在选定视图上显示“螺纹”。";


// Dressup / Fillets
// =========================
filletFrame.boundFilletChkBtn.Title="圆角";
filletFrame.boundFilletChkBtn.LongHelp="圆角
在选定视图上定义圆角展示模式。";

// Dressup / BoundaryFillets
// =========================
filletFrame.boundaryRdBtn.Title="边界";
filletFrame.boundaryRdBtn.LongHelp="边界
定义是否在选定视图上显示圆角边界。";

// Dressup / Original Edges
// =========================
filletFrame.ficFilletRdBtn.Title="符号";
filletFrame.ficFilletRdBtn.LongHelp="符号
定义是否在选定视图上使用“符号圆角”提取模式。";

filletFrame.projoriginaledgeFilletRdBtn.Title="投影的原始边线";
filletFrame.projoriginaledgeFilletRdBtn.LongHelp="投影的原始边线
定义是否在选定视图上使用“投影的原始边线”提取模式。";

filletFrame.originaledgeFilletRdBtn.Title = "近似原始边线";
filletFrame.originaledgeFilletRdBtn.LongHelp="近似原始边线
定义是否在选定视图上使用“近似原始边线”提取模式。";

// Dressup / 3D Spec
// ====================
generativeParamsFrame.uncutSpecChkBtn.Title="3D 规格";
generativeParamsFrame.uncutSpecChkBtn.LongHelp="3D 规格
定义是否在选定视图上显示“3D 规格”。";

// Dressup / 3D Wireframe
// =======================
generativeParamsFrame.3DWireframeChkBtn.Title="3D 线框";
generativeParamsFrame.3DWireframeChkBtn.LongHelp="3D 线框
定义是否在选定视图上显示“3D 线框”。";

// Dressup / 3D Wireframe A PARTIR DE LA R11
// =========================================
3DPointsFrame.3DWireframeFrame.3DWireframeChkBtn.Title="3D 线框";
3DPointsFrame.3DWireframeFrame.3DWireframeChkBtn.LongHelp="3D 线框
指示已投影的 3D 线框在选定视图上的状态为
隐藏还是始终可视。";
3DPointsFrame.3DWireframeFrame.3DWireframeHLRRdBtn.Title="可隐藏";
3DPointsFrame.3DWireframeFrame.3DWireframeProjectionRdBtn.Title="始终可视";

// Dressup / 3D Points
// ====================
generativeParamsFrame.3DPointsChkBtn.Title="3D 点";
generativeParamsFrame.3DPointsChkBtn.LongHelp="3D 点
定义是否在选定视图上显示“ 3D 点”。";
3DPointsFrame.3DPointsChkBtn.Title="3D 点";
3DPointsFrame.3DPointsChkBtn.LongHelp="3D 点
定义是否在选定视图上显示“ 3D 点”。";
3DPointsFrame.3DSymbolRdBtn.Title="3D 符号继承";
3DPointsFrame.3DSymbolRdBtn.LongHelp="3D 符号继承
显示继承自 3D 的符号";
3DPointsFrame.UsrSymbolRdBtn.Title="符号";
3DPointsFrame.UsrSymbolRdBtn.LongHelp="符号
显示选定的符号";

// Dressup / 3D Colors
// ===================
generativeParamsFrame.3DColorsChkBtn.Title="3D 颜色";
generativeParamsFrame.3DColorsChkBtn.LongHelp="3D 颜色
定义是否在选定视图上显示“ 3D 颜色”。";

// Visu & Behaviour 
// ================
visuTitle.Title="可视化和操作";
visuBehaviourFrame.Title="可视化和操作";
visuBehaviourFrame.ShortHelp="选定视图的可视化和操作";
visuBehaviourFrame.Help="定义是否显示视图框架以及是否锁定视图";
visuBehaviourFrame.LongHelp="选定视图的可视化和操作
定义是否显示视图框架以及是否锁定视图。";



// Visu & Behaviour  / Frame
// =========================
visuBehaviourFrame.viewFrameChkBtn.Title="显示视图框架";
visuBehaviourFrame.viewFrameChkBtn.ShortHelp="显示视图框架";
visuBehaviourFrame.viewFrameChkBtn.Help="定义是否显示视图框架";
visuBehaviourFrame.viewFrameChkBtn.LongHelp="显示视图框架
定义是否显示视图框架。";

// Visu & Behaviour / visual clipping
//===================================
visuBehaviourFrame.FramingChkBtn.Title="可视裁剪";
visuBehaviourFrame.FramingChkBtn.ShortHelp="使用可视裁剪";
visuBehaviourFrame.FramingChkBtn.Help="定义是否在视图中执行可视裁剪";
visuBehaviourFrame.FramingChkBtn.LongHelp="可视裁剪
定义是否在视图中执行可视裁剪";

// Visu & Behaviour  / Lock
// ========================
visuBehaviourFrame.viewLockChkBtn.Title="锁定视图";
visuBehaviourFrame.viewLockChkBtn.ShortHelp="锁定选定的视图";
visuBehaviourFrame.viewLockChkBtn.Help="定义是否锁定选定的视图";
visuBehaviourFrame.viewLockChkBtn.LongHelp="锁定选定的视图
定义是否锁定选定的视图。";

// Generation Mode 
// ================
generationModeTitle.Title="生成模式";
generationModeFrame.Title="生成模式";
generationModeFrame.ShortHelp="选定视图的生成模式";
generationModeFrame.LongHelp=
"指示选定的视图是从拓扑数据还是从 CGR 数据中生成，
并在必要时重新生成视图。";


generationModeFrame.DrwGenerationForBox.Title="仅生成大于以下值的零件";
generationModeFrame.DrwGenerationForBox.LongHelp="仅生成大于以下值的零件";
generationModeFrame.spinGenBox.LongHelp="仅生成大于以下值的零件";
generationModeFrame.OcclusionCullingChkBtn.Title="启用遮挡剔除";
generationModeFrame.OcclusionCullingChkBtn.LongHelp="启用遮挡剔除
在计算时将不考虑完全隐藏的对象";
generationModeFrame.Combolabel.Title = "视图生成模式";
generationModeFrame.GenerationModeOption.Title = "选项";
GenerationModeOptionPanelTitle= "生成模式选项";
GenerationModeOptionPanelTitleHRV= "近似模式";

// Messages divers
// ===============

TabTitle="视图";
TabTitle.LongHelp="定义视图。";

ScaleText="缩放：";
ScaleOutOfRangeTitle="超出缩放范围";
ScaleOutOfRangeText="标度必须是一个小于 10000 的正数值。
已将错误标度自动设置为
最接近的授权值。";

gvsFrame.buttonResetStyle.Title="重置为样式值";
gvsFrame.Title="创成式视图样式";
GVSTitle.Title="创成式视图样式";
GVSOverloadHelp="用户重载参数";
gvsFrame.buttonRemoveStyle.Title="移除样式";


