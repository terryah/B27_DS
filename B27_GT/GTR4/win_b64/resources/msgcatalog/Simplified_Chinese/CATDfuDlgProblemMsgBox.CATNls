//=============================================================================
//                                     CNEXT - CXR1
//                          COPYRIGHT DASSAULT SYSTEMES 1997 
//-----------------------------------------------------------------------------
// FILENAME    :    CATDfuDlgProblemMsgBox
// LOCATION    :    DraftingFeature/CNext/resources/msgcatalog/
// AUTHOR      :    OGK
// DATE        :    08.07.98
//------------------------------------------------------------------------------
// DESCRIPTION :    Resources File for NLS purpose related to warning/error messages
//                  
//------------------------------------------------------------------------------
// COMMENTS    :
//------------------------------------------------------------------------------
//MODIFICATIONS     user  date      purpose
//   HISTORY        ----  ----      -------
//		01	ogk	22.03.99	ajout lock de view + nettoyage
//		02	ogk	21.10.99	detection d'update impossible si modele < 418
//		03	lhz	09.11.00	creation d'une sheet de detail                
//------------------------------------------------------------------------------

ProjectionCreationTitle="创建投影视图";
AuxiliaryCreationTitle="创建辅助视图";
SectionCreationTitle="创建剖视图/截面分割视图";
DetailCreationTitle="创建详细视图";
DimensionCreationTitle="创建尺寸";
TextCreationTitle="创建文本";
ArrowCreationTitle="创建箭头";
ToleranceCreationTitle="创建形位公差";
DatumFCreationTitle="创建基准特征";
DatumTCreationTitle="创建基准目标";
RoughnessCreationTitle="创建粗糙度符号";
BalloonCreationTitle="创建零件序号";
AxisLineCreationTitle="创建轴线";
CenterLineCreationTitle="创建中心线";
ThreadCreationTitle="创建螺纹";
AreaFillCreationTitle="创建区域填充";
TableCreationTitle="创建表";
ViewCreationText="活动视图不是最新的，或
找不到指向其文档的链接。";
WeldingCreationTitle="创建焊接";
IntCreationText="选定的视图或活动视图不是最新的。";
IntCreationText3="找不到选定的视图或活动视图与其文档
之间的链接。";

ViewIsLockedTitle="锁定的视图";

ViewIsLockedText="选定的视图或活动视图当前已锁定。
因此，无法执行并将异常中止当前工作指令。
请参考视图属性，锁定或解锁视图。";

ViewIsLockedText2="当前工作指令不采用已锁定的视图。";

ViewIsLockedText3="当前工作指令已异常中止，
因为选择中至少有一个已锁定的视图。";

ViewIsLockedText4="
即将执行锁定工作指令。
因此，未保存已修改的字段。
但是，您可以暂时解锁视图，然后保存修改。";

UndefinedUpdateTitle="更新状态";
UndefinedUpdateText="生成的视图来自 V4.18
之前的模型版本。
因此，将无法检测此视图是否是最新的。
建议您使用 V4.18 或更高版本来更新模型。";

Information="信息";
Warning="警告";
ViewAlreadyIsolatedText="已对视图进行隔离";
ViewWillBeIsolatedText="确认视图隔离：将删除所有修饰";
SheetCreatedText="未找到详图：
已创建详图。";			


BrokenCreationTitle="创建局部视图";
BrokenCreationText="命令异常中止：活动视图可能不是创成式的、
不是最新的，或已被锁定。";

ChamferDimNotEditable="无法编辑倒角尺寸";

ViewIsNotUpToDate="选定的视图或活动视图不是最新的。";

InsertBackgroundTitle="打开失败";
InsertBackgroundText="无法打开选定的文档。
主要原因是您想打开的工程图版本
高于您当前使用的 CATIA 版本。
请尝试打开其他 CATDrawing 文档。";
