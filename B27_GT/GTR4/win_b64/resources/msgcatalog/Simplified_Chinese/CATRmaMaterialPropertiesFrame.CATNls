// COPYRIGHT DASSAULT SYSTEMES 1998
//===========================================================================
//
// CATRmaMaterialPropertiesFrame (English)
//
//===========================================================================


previewFrame.actionFrame.sizeFrame.sizeLabel.Title = "材料大小：";
previewFrame.actionFrame.sizeFrame.sizeLabel.Help = "设置材料的大小";
previewFrame.actionFrame.sizeFrame.sizeLabel.LongHelp =
"设置材料的大小。
此值可在将材料应用于对象时
调整材料的缩放。
注意：材料大小不会对预览产生任何影响。";

previewFrame.actionFrame.sizeFrame.sizeEditor.Title = "预览编辑器";
previewFrame.actionFrame.sizeFrame.sizeEditor.Help = "设置材料的大小";
previewFrame.actionFrame.sizeFrame.sizeEditor.LongHelp =
"设置材料的大小。
此值可在将材料应用于对象时
调整材料的缩放。
注意：材料大小不会对预览产生任何影响。";

previewFrame.actionFrame.buttonsFrame.iconBox.Title = "更改预览类型";
previewFrame.actionFrame.buttonsFrame.iconBox.Help = "更改预览的支持面";
previewFrame.actionFrame.buttonsFrame.iconBox.ShortHelp = "更改预览";
previewFrame.actionFrame.buttonsFrame.iconBox.LongHelp =
"在预览窗口中
更改应用材料的支持面。";

previewFrame.actionFrame.buttonsFrame.iconBox.planRadioButton.Title = "平面支持面";
previewFrame.actionFrame.buttonsFrame.iconBox.planRadioButton.Help = "已预览的支持面是平面";
previewFrame.actionFrame.buttonsFrame.iconBox.planRadioButton.ShortHelp = "平面支持面";
previewFrame.actionFrame.buttonsFrame.iconBox.planRadioButton.LongHelp =
"在预览窗口中，
将材料应用于平面支持面。";

previewFrame.actionFrame.buttonsFrame.iconBox.sphereRadioButton.Title = "球面支持面";
previewFrame.actionFrame.buttonsFrame.iconBox.sphereRadioButton.Help = "已预览的支持面是球面";
previewFrame.actionFrame.buttonsFrame.iconBox.sphereRadioButton.ShortHelp = "球面支持面";
previewFrame.actionFrame.buttonsFrame.iconBox.sphereRadioButton.LongHelp =
"在预览窗口中，将材料应用于球面。";

previewFrame.actionFrame.buttonsFrame.iconBox.cylinderRadioButton.Title = "圆柱支持面";
previewFrame.actionFrame.buttonsFrame.iconBox.cylinderRadioButton.Help = "已预览的支持面是圆柱面";
previewFrame.actionFrame.buttonsFrame.iconBox.cylinderRadioButton.ShortHelp = "圆柱支持面";
previewFrame.actionFrame.buttonsFrame.iconBox.cylinderRadioButton.LongHelp =
"在预览窗口中，将材料应用于圆柱面。";

previewFrame.actionFrame.buttonsFrame.iconBox.cubeRadioButton.Title = "盒子支持面";
previewFrame.actionFrame.buttonsFrame.iconBox.cubeRadioButton.Help = "已预览的支持面是盒子";
previewFrame.actionFrame.buttonsFrame.iconBox.cubeRadioButton.ShortHelp = "立方支持面";
previewFrame.actionFrame.buttonsFrame.iconBox.cubeRadioButton.LongHelp =
"在预览窗口中，将材料应用于盒子。";

previewFrame.actionFrame.buttonsFrame.iconBox.automaticRadioButton.Title = "曲线支持面";
previewFrame.actionFrame.buttonsFrame.iconBox.automaticRadioButton.Help = "已预览的支持面是曲面";
previewFrame.actionFrame.buttonsFrame.iconBox.automaticRadioButton.ShortHelp = "曲线支持面";
previewFrame.actionFrame.buttonsFrame.iconBox.automaticRadioButton.LongHelp =
"在预览窗口中，将材料应用于曲线补面。";

previewFrame.actionFrame.buttonsFrame.reframeButton.Title = "适屏幕预览";
previewFrame.actionFrame.buttonsFrame.reframeButton.Help = "放大或缩小几何图形，使其符合预览区域的大小";
previewFrame.actionFrame.buttonsFrame.reframeButton.ShortHelp = "适屏幕预览";
previewFrame.actionFrame.buttonsFrame.reframeButton.LongHelp =
"放大或缩小几何图形，使其符合可用空间的大小。";

previewFrame.actionFrame.buttonsFrame.previewModeCheck.Title = "已跟踪的射线";
previewFrame.actionFrame.buttonsFrame.previewModeCheck.Help = "启用已跟踪的射线预览";
previewFrame.actionFrame.buttonsFrame.previewModeCheck.ShortHelp = "已跟踪射线预览";
previewFrame.actionFrame.buttonsFrame.previewModeCheck.LongHelp =
"启用/禁用已跟踪的射线预览。
此模式较慢，但可以在此查看
诸如折射、过程化结构或凹凸等效果。";

previewFrame.actionFrame.synchroFrame.synchroButton.Title = "实时预览";
previewFrame.actionFrame.synchroFrame.synchroButton.Help = "激活实时预览";
previewFrame.actionFrame.synchroFrame.synchroButton.ShortHelp = "激活实时预览";
previewFrame.actionFrame.synchroFrame.synchroButton.LongHelp =
"激活实时预览。";

previewFrame.actionFrame.typeFrame.typeLabel.Title = "类型：";
previewFrame.actionFrame.typeFrame.typeLabel.Help = "更改着色器类型";
previewFrame.actionFrame.typeFrame.typeLabel.ShortHelp = "着色器类型";
previewFrame.actionFrame.typeFrame.typeLabel.LongHelp =
"更改材料的着色器类型。";

previewFrame.actionFrame.typeFrame.typeCombo.Help = "更改着色器类型";
previewFrame.actionFrame.typeFrame.typeCombo.ShortHelp = "着色器类型";
previewFrame.actionFrame.typeFrame.typeCombo.LongHelp =
"更改材料的着色器类型。";

DefaultShader = "默认";

CreateShaderTitle = "错误";
CreateShaderError = "无法创建着色器，\n请重置到默认着色器。";
