// Cree par cdz, January 2004
// COPYRIGHT DASSAULT SYSTEMES 1999
//===========================================================================
//   ENGLISH ENGLISH ENGLISH ENGLISH ENGLISH ENGLISH ENGLISH ENGLISH ENGLISH 
//===========================================================================
// Resource catalog for the CAADegCreateBoxPaletteHeader class
// used in the CAADegCreateBoxPaletteHeader command for the palette
//===========================================================================

CATDesCmdSkinDefineCompassPaletteHeader.TranslateHdrCATNls.Help=
"Translates the selected vertices along the manipulator directions defined by the robot.";
CATDesCmdSkinDefineCompassPaletteHeader.TranslateHdrCATNls.LongHelp=
"Translates the selected vertices along the manipulator directions defined by the robot.";
CATDesCmdSkinDefineCompassPaletteHeader.TranslateHdrCATNls.ShortHelp="Translation";
CATDesCmdSkinDefineCompassPaletteHeader.TranslateHdrCATNls.Title="IMA_Translation";

CATDesCmdSkinDefineCompassPaletteHeader.RotateHdrCATNls.Help=
"Rotates the selected vertices along the manipulator directions defined by the robot.";
CATDesCmdSkinDefineCompassPaletteHeader.RotateHdrCATNls.LongHelp=
"Rotates the selected vertices along the manipulator directions defined by the robot.";
CATDesCmdSkinDefineCompassPaletteHeader.RotateHdrCATNls.ShortHelp="Rotation";
CATDesCmdSkinDefineCompassPaletteHeader.RotateHdrCATNls.Title="IMA_Rotation";

CATDesCmdSkinDefineCompassPaletteHeader.EdgePropaHdrCATNls.Help="Propagates the selection to the neighbouring edges.";
CATDesCmdSkinDefineCompassPaletteHeader.EdgePropaHdrCATNls.LongHelp="Propagates the selection to the neighbouring edges.";
CATDesCmdSkinDefineCompassPaletteHeader.EdgePropaHdrCATNls.ShortHelp="Propagation";
CATDesCmdSkinDefineCompassPaletteHeader.EdgePropaHdrCATNls.Title="IMA_Propagation";

CATDesCmdSkinDefineCompassPaletteHeader.Between2EdgePropaHdrCATNls.Help="Selects two edges of the mesh.";
CATDesCmdSkinDefineCompassPaletteHeader.Between2EdgePropaHdrCATNls.LongHelp="Selects two edges of the mesh.";
CATDesCmdSkinDefineCompassPaletteHeader.Between2EdgePropaHdrCATNls.ShortHelp="Between 2 Edges";
CATDesCmdSkinDefineCompassPaletteHeader.Between2EdgePropaHdrCATNls.Title="IMA_Select two edges in the mesh.";

CATDesCmdSkinDefineCompassPaletteHeader.PickHdrCATNls.Help=
"Activates/Desactivates the selection of elements not belonging to the subdivision surface.";
CATDesCmdSkinDefineCompassPaletteHeader.PickHdrCATNls.LongHelp=
"Activates/Desactivates the selection of elements not belonging to the subdivision surface.";
CATDesCmdSkinDefineCompassPaletteHeader.PickHdrCATNls.ShortHelp="Pick";
CATDesCmdSkinDefineCompassPaletteHeader.PickHdrCATNls.Title="IMA_Pick";

CATDesCmdSkinDefineCompassPaletteHeader.EditTransfoValueHdrCATNls.Help=
"Defines the position, angles, dimensions, and weight related to the selected vertices.";
CATDesCmdSkinDefineCompassPaletteHeader.EditTransfoValueHdrCATNls.LongHelp=
"Defines the position, angles, dimensions, and weight related to the selected vertices.";
CATDesCmdSkinDefineCompassPaletteHeader.EditTransfoValueHdrCATNls.ShortHelp="Edition";
CATDesCmdSkinDefineCompassPaletteHeader.EditTransfoValueHdrCATNls.Title="IMA_Edit";

CATDesCmdSkinDefineCompassPaletteHeader.CompassDefinitionHdrCATNls.Help=
"Defines the robot position using vertices, edges or faces.";
CATDesCmdSkinDefineCompassPaletteHeader.CompassDefinitionHdrCATNls.LongHelp=
"Defines the robot position using vertices, edges or faces.";
CATDesCmdSkinDefineCompassPaletteHeader.CompassDefinitionHdrCATNls.ShortHelp="Robot Definition";
CATDesCmdSkinDefineCompassPaletteHeader.CompassDefinitionHdrCATNls.Title="IMA_Robot Definition";

CATDesCmdSkinDefineCompassPaletteHeader.NoCompassDefinitionHdrCATNls.Help=
"Robot is unactive.";
CATDesCmdSkinDefineCompassPaletteHeader.NoCompassDefinitionHdrCATNls.LongHelp=
"Robot is unactive.";
CATDesCmdSkinDefineCompassPaletteHeader.NoCompassDefinitionHdrCATNls.ShortHelp="Inactive Robot";
CATDesCmdSkinDefineCompassPaletteHeader.NoCompassDefinitionHdrCATNls.Title="IMA_Robot Definition";

CATDesCmdSkinDefineCompassPaletteHeader.ResetCompassHdrCATNls.Help=
"Resets the robot definition.";
CATDesCmdSkinDefineCompassPaletteHeader.ResetCompassHdrCATNls.LongHelp=
"Resets the robot definition.";
CATDesCmdSkinDefineCompassPaletteHeader.ResetCompassHdrCATNls.ShortHelp="Reset Robot";
CATDesCmdSkinDefineCompassPaletteHeader.ResetCompassHdrCATNls.Title="IMA_Reset Robot";

CATDesCmdSkinDefineCompassPaletteHeader.NoResetCompassHdrCATNls.Help=
"Robot is inactive.";
CATDesCmdSkinDefineCompassPaletteHeader.NoResetCompassHdrCATNls.LongHelp=
"Robot is inactive.";
CATDesCmdSkinDefineCompassPaletteHeader.NoResetCompassHdrCATNls.ShortHelp="Inactive Robot";
CATDesCmdSkinDefineCompassPaletteHeader.NoResetCompassHdrCATNls.Title="IMA_Reset Robot";

CATDesCmdSkinDefineCompassPaletteHeader.CompassPermuteHdrCATNls.Help=
"Flips x, y and z axes of the robot.";
CATDesCmdSkinDefineCompassPaletteHeader.CompassPermuteHdrCATNls.LongHelp=
"Flips x, y and z axes of the robot.";
CATDesCmdSkinDefineCompassPaletteHeader.CompassPermuteHdrCATNls.ShortHelp="Axes Permutation";
CATDesCmdSkinDefineCompassPaletteHeader.CompassPermuteHdrCATNls.Title="IMA_Robot Axes Permutation";

CATDesCmdSkinDefineCompassPaletteHeader.CompassCreationHdrCATNls.Help=
"Creates points, lines and planes using the robot.";
CATDesCmdSkinDefineCompassPaletteHeader.CompassCreationHdrCATNls.LongHelp=
"Creates points, lines and planes using the robot.";
CATDesCmdSkinDefineCompassPaletteHeader.CompassCreationHdrCATNls.ShortHelp="Wireframe Creation";
CATDesCmdSkinDefineCompassPaletteHeader.CompassCreationHdrCATNls.Title="IMA_Wireframe Creation";

CATDesCmdSkinDefineCompassPaletteHeader.SelectAllHdrCATNls.Help=
"Selects all elements.";
CATDesCmdSkinDefineCompassPaletteHeader.SelectAllHdrCATNls.LongHelp=
"Selects all elements.";
CATDesCmdSkinDefineCompassPaletteHeader.SelectAllHdrCATNls.ShortHelp="All Elements Selection";
CATDesCmdSkinDefineCompassPaletteHeader.SelectAllHdrCATNls.Title="IMA_All Elements Selection";

