// COPYRIGHT DASSAULT SYSTEMES 2003
//=============================================================================
//
// Resources File for NLS purpose related to FTA cleaner
//
//=============================================================================
// Usage notes:
//
//=============================================================================
// Apr. 2003  Creation                                               Z. Y. Gong
//=============================================================================

FTA_1.ShortMessage = "Validity of features in TTRS container.";
FTA_1.ErrorMessage = "Some features in TTRS container aren't valid.";
FTA_1.CleanMessage = "The invalid features in TTRS container are removed from the document.";

FTA_2.ShortMessage = "Utilization of RGE by 3D annotation.";
FTA_2.ErrorMessage = "The RGE feature isn't utilized by any 3D annotation.";
FTA_2.CleanMessage = "The non utilized RGE feature is removed from the document.";

FTA_3.ShortMessage = "Utilization of TTRS by 3D annotation.";
FTA_3.ErrorMessage = "The TTRS feature isn't utilized by any 3D annotation.";
FTA_3.CleanMessage = "The non utilized TTRS feature is removed from the document.";

FTA_4.ShortMessage = "Links between Drafting annotation and 3D annotation in the Set.";
FTA_4.ErrorMessage = "One or several Drafting annotation is(are) not used by any 3D annotation.";
FTA_4.CleanMessage = "The non used Drafting annotation(s) is(are) removed from the document.";

FTA_5.ShortMessage = "Link between the 3D annotation and Tolerancing Set.";
FTA_5.ErrorMessage = "The 3D annotation is not utilized by any Tolerancing Set.";
FTA_5.CleanMessage = "The 3D annotation is removed from the document.";

FTA_6.ShortMessage = "Inconsistant Dimension Representation.";
FTA_6.ErrorMessage = "FTA dimension features are not fully rerooted on new geometry.";
FTA_6.CleanMessage = "FTA dimension is correctly reconnected to the geometry.";

FTA_7.ShortMessage = "Redundant Geometry under a TTRS.";
FTA_7.Error1Message = "User Surface has several components that point to the same geometry. => /p1 needs to be manually reconnected with the geometry.";
FTA_7.Error2Message = "User Surface has several components that point to the same geometry. => /p1 needs to be manually reconnected with the geometry. /p2 is created on this user surface.";
FTA_7.Error3Message = "User Surface has several components that point to the same geometry. => The components '/p3' and '/p4' of '/p1' are pointing to the same geometry. '/p1' needs to be manually reconnected with the geometry. '/p2' is created on this user surface.";

FTA_8.ShortMessage = "CG Thread status computation error.";
FTA_8.ErrorMessage = "TTRS Represented of CG thread is not correctly redefined.";

FTA_9.ShortMessage = "Incorrect geometrical tolerance display.";
FTA_9.ErrorMessage = "The 3D display of the geometrical tolerance is not consistent with its semantic attribute.
The tolerance zone is a cylinder according to the semantic attributes but the diameter symbol is not displayed in the callout.
To fix the error, you have to edit the geometrical tolerance (you will see that the diameter symbol is automatically added) and either:
1. Press the OK button if the design intent is to define a cylinder tolerance zone or
2. Select a Tolerance Zone Direction if the design intent is to define a 2 parallel planes tolerance zone and then press the OK button.";

FTA_10.ShortMessage = "Inconsistent TTRS utilisation by FTA View.";
FTA_10.ErrorMessage = "FTA View created on the 3D axis system is utilising the TTRS object from the different document.";
FTA_10.CleanMessage = "The TTRS utilised by the view is corrected.";

FTA_11.ShortMessage = "Inconsistent URL Link.";
FTA_11.ErrorMessage = "At least one URL Link from FlagNote or NOA feature is empty.";
FTA_11.CleanMessage = "Empty URL Link has been removed.";

FTA_12.ShortMessage = "Link between the 3D Annotation and a 3D Annotation View.";
FTA_12.ErrorMessage = "No 3D Annotation View has been found for the 3D Annotation.";
FTA_12.CleanMessage = "A new 3D Annotation view has been created and reconnected to Annotation.";

FTA_13.ShortMessage = "Inconsistent Datum Target.";
FTA_13.ErrorMessage = "Semantic Datum Target is not associated with any semantic Datum.";
FTA_13.CleanMessage = "The inconsistent Datum Target without any Semantic Datum associated with it, is removed.";

FTA_14.ShortMessage = "Inconsistent links on visualization dimension feature.";
FTA_14.ErrorMessage = "Visualization dimension feature is linked to by more than one annotation.";
FTA_14.CleanMessage = "The invalid link to this visualization dimension feature has been removed.";

FTA_15.ShortMessage = "FTA Capture External Links.";
FTA_15.ErrorMessage = "At least one External Link of Capture is pointing to a non existing document.";
FTA_15.CleanMessage = "Non existing External Links have been removed.";

FTA_16.ShortMessage = "FTA NOA External Links.";
FTA_16.ErrorMessage = "An External Link of Noa is pointing to a non existing document.";
FTA_16.CleanMessage = "Non existing External Link has been removed.";

FTA_17.ShortMessage = "FTA Noa with Ditto.";
FTA_17.ErrorMessage = "Ditto of Noa isn't created on background view.";
FTA_17.CleanMessage = "Ditto of Noa is rerouted to background view.";

FTA_18.ShortMessage = "FTA Text External Links.";
FTA_18.ErrorMessage = "An External Link of Text is pointing to a non existing document.";
FTA_18.CleanMessage = "Non existing External Links have been removed.";

FTA_19.ShortMessage = "Inconsistent Repetitive Feature Identifier.";
FTA_19.ErrorMessage = "Either the repetitive feature identifier does not correspond to the number of pattern elements the dimension is linked with, or there is none or several repetitive feature identifiers, or there is no space (� �) character between the repetitive feature identifier and the text placed before or after it.";
FTA_19.CleanMessage = "The Inconsistent Repetitive Feature Identifier of a pattern dimension has been corrected.";

FTA_20.ShortMessage = "Not up to date Drafting Dimension associated with FTA dimension.";
FTA_20.ErrorMessage = "Drafting Dimension associated with FTA dimension is not up to date";
FTA_20.CleanMessage = "The Drafting dimension has been updated.";

FTA_21.ShortMessage = "Validity of features in TTRS container.";
FTA_21.ErrorMessage = "Some features in TTRS container aren't valid.";
FTA_21.CleanMessage = "The invalid features in TTRS container are removed from the document.";
