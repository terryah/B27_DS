//Pour l'editeur de base
//-------------------------------
frMode.rbmode_select.Title="Select Feature";
frMode.rbmode_select.ShortHelp = "Select in 3D or in feature tree, will edit the text area";
frMode.rbmode_select.Help = "Select in 3D or in feature tree, will edit the text area";
frMode.rbmode_select.LongHelp = "Select in 3D or in feature tree, will edit the text area";

frMode.rbmode_filter.Title = "Filter";
frMode.rbmode_filter.ShortHelp = "Select in 3D or in feature tree, will filter the parameters list";
frMode.rbmode_filter.Help = "Select in 3D or in feature tree, will filter the parameters list";
frMode.rbmode_filter.LongHelp = "Select in 3D or in feature tree, will filter the parameters list";

frMode.cbincremental.ShortHelp = "Check to work in optimized mode (incremental parameters list construction with selection)";
frMode.cbincremental.LongHelp = "In this editor, you can work in optimized mode (incremental mode) or not.\n",
												 "Incremental means that you have to select progressively features\n",
												 "to access their parameters. It is a performing mode.\n",
												 "Non incremental means that all the parameters of the application\n",
												 "are known at the beginning.";
												 
frMode.AutoformatButton.ShortHelp="Check to have an automatic text formatting";
frMode.ErrorButton.ShortHelp="Check to have an automatic expression parsing, with visual notifications in case of syntax errors";	
frMode.ButtonsFrame.ShowLineButton.ShortHelp="Highlights syntax error line and gives visual notifications for syntax errors";
frMode.ButtonsFrame.TypeWizardButton.ShortHelp="Shows Language browser panel";

frEditor.text_editor.Help = "Edit the text";

frMode.ButtonsFrame.pbclear.Title = "Clear";
frMode.ButtonsFrame.pbclear.Help = "Erases the text field";
frMode.ButtonsFrame.pbclear.LongHelp = "Erases the text field.";
frMode.ButtonsFrame.pbclear.ShortHelp = "Erases the text field.";

frMode.ButtonsFrame.Knowl.Title = "URLs & Comment...";
frMode.ButtonsFrame.Knowl.Help = "Accesses the URLs and Comment editor...";
frMode.ButtonsFrame.Knowl.LongHelp = "Accesses the URLs and Comment editor...";
frMode.ButtonsFrame.Knowl.ShortHelp = "URLs & Comment...";

pbwizard.Title = "Wizard";
pbwizard.Help = "Open the edition wizard";
pbwizard.LongHelp = "Opens the wizard which contains the edition dictionary. Through filter lists,\n",
						      "you'll be able to access parameters, math functions, string functions, units ...\n",
							  "By clicking items in the result list you will edit the formula.";

//Pour l'editeur de formules
//-------------------------------------
FormulaEditorDlg.Title = "Formula Editor : /P1";
FormulaEditorDlg.LongHelp = "In this editor, you are able to rename a parameter and to edit its formula.\n\n",
                                    		"To edit the text, you can:\n\n",
											"  - use the keyboard,\n",	
											"  or\n",
											"  - select dimensions in the model (display dimensions by selecting\n",
											"    a feature in the tree view or in the model),\n",
											"  or\n",
											"  - open the wizard and select items in the result list.\n\n",
											"Once you have opened the wizard, it remains.\n";
param_editor.LongHelp = "You can rename the out parameter of the formula by typing the new name in the field.";
FormulaEditorDlg.text_editor.LongHelp = " This field is the formula editor.\n\n",
                                  " What is a formula ? : \n",
								  " A formula is used to define a valuation for a parameter as a\n",
								  " combination of functions, operators, constants and other parameters.\n\n",
								  " The result of this combination must match the parameter type.\n",
								  " For example, if the parameter to be valuated is a length, \n",
								  " formula result must be a length. \n\n" ,
								  " Examples of formula that you can key in :\n",
								  "  - sin (x * PI) * 3 + 12\n",
								  "  - max (x,y)\n",
								  "  - etc.... \n\n",
								  " We advise you to specify precise units for constants, otherwise\n",
								  " the International System Units will be the default.\n",
								  " (Ex: 10->10mm or MyRealParameter->MyRealParameter*1mm)\n\n",
								  " To edit the formula, use the keyboard, select dimensions in the model or\n",
								  " use the wizard. To display dimensions, select the desired feature in the model\n",
								  " or in the feature tree.\n";
param_name.error.Title = "Error";
param_name.error.Text = "Another parameter has this name already";

SyncQuestion="This relation has links to non parameters features and so will not be updated when inputs will.\nDo you want this relation to be updated when global Update is performed?\n(You can modify this behaviour with the relation properties)";
SyncQuestionHd="Automatic update?";

ValSyncQuestion="This relation will be asynchronous and so will not be updated when inputs will.\nDo you want this relation to be updated when global Update is performed?\n(You can modify this behaviour with the relation properties)";
ValSyncQuestionHd="Automatic update?";

NonFeatureSyncQuestion="This relation is synchronous and contains only parameters.\nDo you want this relation to be updated anyway when global Update is performed as specified in the options?\n(You can modify this behaviour with the relation properties)";
NonFeatureSyncQuestionHd="Automatic update?";

ErrorQuestion="\nDo you want to go back to the editor (otherwise the relation will be broken and deactivated)\n";
RestoreButton="Show panel";	

ParseOK="No syntax error";	
Reframe="Reframe on object under cursor";
CenterGraph="Center graph on object under cursor";
GotoLine="Line:";

EditorClosedContainsModificationsTitle="Modifications Warning";
EditorClosedContainsModificationsMsg="Some modifications will be lost if you close the editor now.\nDo you really want to close it ?";	

