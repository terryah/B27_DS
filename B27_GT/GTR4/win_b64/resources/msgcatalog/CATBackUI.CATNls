//  Messages for CATIA V5 (Downward)Compatibility
//  MODULE :  DownwardCompatibility
//  US Version
//====================================================================
Title="CATIA V5 Downward Compatibility Batch";
Main.Title="Downward Compatibility";
BatchMonitor.Title="Downward compatibility between the most recent release down to V5R6"; 
Downward.Abstract="Downward compatibility between the most recent release down to V5R6"; 
Downward.Domain="Data Compatibility"; 

// User Interfaces resources
CATBack.comboVersion.Title="CATIA Level";
CATBack.comboAction.Title="Action";

CATBack.BrowseDir.Title="...";
CATBack.BrowseDirBox.Title="Directory Selection";
CATBack.LblTargetDirectory.Title="Target Directory";
CATBack.BtnTargetDirectory.Title="...";

CATBack.WarningPanel.Title="Warning";
CATBack.LblDocSelected.Title="Selected V5 Document(s):";
CATBack.LblDocProcessed.Title="V5 Document(s) to Process:";

CATBack.BtnBrowse.Title="Browse File";
CATBack.CheckMMLSupport.Title="Pointed documents";
CATBack.CheckMMLSupport.Help="Select Pointed documents";
CATBack.BtnRemoveDoc.Title="Remove";
CATBack.BtnReplaceDoc.Title="Replace";
CATBack.Replace.Help = "Replace the already existing components";

CATBack.ListSelDoc.Name="Name";
CATBack.ListSelDoc.Path="Path";

ComboAction.Convert="Convert";
ComboAction.Info="Info";
ComboAction.Synchro="Synchronize";
CATBack.CheckHtmlOpen.Title="Open HTML Report";

//============================================================
// Error messages
//============================================================
WarningDoublonMessage="This file already belongs to the set of documents to process";
WarningDoublonMsgMultiSel="These files already belong to the set of documents to process:";

WarningBadTypeMessage="This type of file cannot be processed";
WarningBadTypeMsgMultiSel="These files cannot be processed:";
WarningInvalidFormat="file format is not valid";
//============================================================
// Report messages
//============================================================
ReportBatchAborted1="Batch aborted - ReturnCode=";
ReportBatchAborted2="No document processed";
ReportBatchPartial="Batch partially processed - ReturnCode=";
ReportBatchSuccess="Batch successfully processed - ReturnCode=";
ReportBegin="Report";
ReportDocumentTitle="Document to process";
ReportDocumentKO="Document not processed";
ReportDocumentOK="Document successfully processed";
ReportEnd="End of Report";
ReportAction.Convert= "Conversion of \"/p1\"" ;	  
ReportAction.Synchro= "Synchronization  of \"/p1\"" ;	  
ReportAction.Info= "Information of \"/p1\"" ;	  
ReportAction.Undef= "Error: no action defined
\t  Need to declar <Convert> or <Synchro> or <Info> action
\t  Use '-h' option to consult the list of known and mandatory parameters";
ReportAction.Unknown= "Warning: Action \"/p1\" is unknown,
\t  Supported Actions are:" ;	  
ReportVersion.Unknown= "Warning: Version \"/p1\" is not supported or is not a CATIA Version,
\t  Supported Versions are:" ;	  
Report.Convert.CurrentVersion="Current version: \"/p1\"";
Report.Convert.OutputDirectory="Output directory: \"/p1\"";
Report.Convert.TargetedVersion="Targeted version: \"/p1\"";

//============================================================
// Specific messages    "Info" action : Infoxxx
//============================================================
Info.DocVersion="   Minimal Version to open the document: \"/p1\"";
Info.DocNotFound="   Document does not exist or cannot be found ";
Info.RefPath="   Document is a converted image from file: \"/p1\"";
Info.RefNotFound="   Reference does not exist or cannot be found ";
Info.RefVersion="   Minimal Version to open the reference: \"/p1\"";
Info.InfoKO="Document cannot be processed";
Info.InfoOK="Document successfully processed";
//============================================================
// Specific messages "Convert" action : Convertxxx
//============================================================
ConvertSelectionDocKo1="This file is already a converted image:";
ConvertSelectionDocKo2="Selection aborted, select the original file:";
ConvertSelectionDocKo3="But the original file is not found or cannot be read:";
Convert.NotADoc="   Document to convert is not a CATIA V5 Document";
Convert.DocNotFound="   Document does not exist or cannot be found ";
Convert.MinimalVersion="   Minimal Version to open the document: \"/p1\"";
Convert.Needed="   Document needs to be converted";
Convert.NotNeeded="   Document doesn't need to be converted";
Convert.NoReadPermission="   No Read access. \nChange the access permission";
Convert.ReadAborted="   Document cannot be read";
Convert.CreateAborted="   Output Document \"/p1\" cannot be created";
Convert.AlreadyExist="   Document already exists and cannot be overwritten. \nMove the document.";
Convert.WriteAborted="   Document cannot be written. \nCheck the access permission";
Convert.ErrorDummy="   Internal Error during conversion";
Convert.ConvertKO="Document cannot be converted";
Convert.ConvertOK="Document successfully processed";
//============================================================
// Specific messages "Synchro" action : Synchroxxx
//============================================================
SynchroSelectionDocKo1="Document cannot be synchronized:";
SynchroSelectionDocKo2="Selection aborted, this file is not a converted image";
SynchroSelectionDocKo3="selection aborted, the original file is not found or cannot be read";
Synchro.NotADoc="   Document to synchonize is not a CATIA V5 Document";
Synchro.DocNotFound="   Document does not exist or cannot be found";
Synchro.NotAConvertedDoc="   Document is not a converted image";
Synchro.NoReadPermission="   No Read access. \nChange the access permission";
Synchro.NoWritePermission="   No Write access. \nChange the access permission";
Synchro.RefNotADoc="   Reference is not a CATIA V5 Document";
Synchro.RefNotFound="   Reference does not exist or cannot be found";
Synchro.RefNoReadPermission="   No Read access on the reference. \nChange the access permission";
Synchro.RefPath="   Document is a converted image from file: /p1";
Synchro.RefVersion="   Minimal Version to open the reference: \"/p1\"";
Synchro.DocVersion="   Minimal Version to open the document: \"/p1\"";
Synchro.ReadAborted="   Document cannot be read";
Synchro.ReadRefAborted="   Reference cannot be read";
Synchro.WriteAborted="   Document cannot be written. \nCheck the access permission";
Synchro.NoModif="   Document doesn't need to be synchronized.";
Synchro.ErrorDummy="   Internal Error during synchronization";
Synchro.SynchroKO="Document cannot be synchronized";
Synchro.SynchroOK="Document successfully processed";

Downward.Compatibility = "Downward Compatibility";

//============================================================
// Errors messages 
//============================================================
Error.Title="Error";
ErrorDummy="Internal Error - Code=";

HelpMsg="USAGE: 
    CATDownwardCompatibility -id inputDirectory [-if inputfile/-il inputlist] -od outputDirectory 
                             -action [Convert/Synchro/Info] -version CATIAV5Ri 
                             -report reportFile.txt

       -id	 : Intput Directory or DLNAME
       -if       : Input File containing a list of document to process
       -il       : Input list of document to process 
                   -if or -il parameters are mandatory 
       -action   : action requested 
                   'Convert' : Converts a document to a document readable on version defined in -version parameter
                   'Synchro' : Synchronizes a converted document on the original file 
                   'Info'    : Gives information on the selected members
                   -action parameter is mandatory
       -version  : Targeted CATIA version
                   mandatory for 'Convert' action (not use in 'Synchro' & 'Info' action mode)                   
       -od	 : Output Directory or DLNAME 
                   mandatory for 'Convert' action (not use in 'Synchro' & 'Info' action mode)
       -report   : Report File name"; 

