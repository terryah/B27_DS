//=============================================================================
// COPYRIGHT DASSAULT SYSTEMES  2005
//=============================================================================
//
// CATStOLDTopoFilletDialog: Resource file for NLS purpose related to TopoFillet command
//
//=============================================================================
//
// Implementation Notes:
//
//=============================================================================
// March 05   Creation                                    Ritesh Kanetkar
//=============================================================================
DialogBoxTitle="Styling Fillet";
TabCont.GeneralTab.Title = "Fillet Inputs";
TabCont.ApproxTab.Title = "Approximation";

TabCont.GeneralTab.MainFrame.ContinuityFrame.Title  = "Tolerance";
TabCont.GeneralTab.MainFrame.RadiusFrame.Title  = "Radius";
TabCont.GeneralTab.MainFrame.ArcTypeFrame.Title  = "Arc Type";
TabCont.GeneralTab.MainFrame.ParameterFrame.Title  = "Parameter";
TabCont.GeneralTab.MainFrame.ResultFrame.Title  = "Result";
TabCont.GeneralTab.MainFrame.OptionFrame.Title  = "Option";
TabCont.GeneralTab.MainFrame.SelectionFrame.Title  = "Selection";

TabCont.GeneralTab.MainFrame.ParameterFrame.ParameterCombo.PDefault.Title = "Default";
TabCont.GeneralTab.MainFrame.ParameterFrame.ParameterCombo.PPatch1.Title = "Patch 1";
TabCont.GeneralTab.MainFrame.ParameterFrame.ParameterCombo.PPatch2.Title = "Patch 2";
TabCont.GeneralTab.MainFrame.ParameterFrame.ParameterCombo.PAverage.Title = "Average";
TabCont.GeneralTab.MainFrame.ParameterFrame.ParameterCombo.PBlend.Title = "Blend";
TabCont.GeneralTab.MainFrame.ParameterFrame.ParameterCombo.PChordal.Title = "Chordal";

TabCont.GeneralTab.MainFrame.ResultFrame.TrimCombo.TrimFace.Title = "Trim Face";
TabCont.GeneralTab.MainFrame.ResultFrame.TrimCombo.TrimApprox.Title = "Approx";

TabCont.GeneralTab.MainFrame.ResultFrame.SmallSurCombo.SSStitch.Title = "Stitch";
TabCont.GeneralTab.MainFrame.ResultFrame.SmallSurCombo.SSRip.Title = "Rip";

TabCont.GeneralTab.MainFrame.ContinuityFrame.G0Radio.ShortHelp = "G0";
TabCont.GeneralTab.MainFrame.ContinuityFrame.G0Radio.LongHelp = "G0 continuity between the fillet and input elements";
TabCont.GeneralTab.MainFrame.ContinuityFrame.G1Radio.ShortHelp = "G1";
TabCont.GeneralTab.MainFrame.ContinuityFrame.G1Radio.LongHelp = "G1 continuity between the fillet and input elements";
TabCont.GeneralTab.MainFrame.ContinuityFrame.G2Radio.ShortHelp = "G2";
TabCont.GeneralTab.MainFrame.ContinuityFrame.G2Radio.LongHelp = "G2 continuity between the fillet and input elements";
TabCont.GeneralTab.MainFrame.ContinuityFrame.G3Radio.ShortHelp = "G3";
TabCont.GeneralTab.MainFrame.ContinuityFrame.G3Radio.LongHelp = "G3 continuity between the fillet and input elements";


TabCont.GeneralTab.MainFrame.RadiusFrame.Radius.ShortHelp = "Constant Radius";
TabCont.GeneralTab.MainFrame.RadiusFrame.Radius.LongHelp = "Constant Radius for the fillet";
TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadFlag.ShortHelp = "Minimum Radius";
TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadFlag.LongHelp = "Minimum radius ON/OFF";
TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadius.ShortHelp = "Minimum radius value";
TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadius.LongHelp = "Minimum radius value";
TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadRelativeFlag.ShortHelp = "Relative Radius";
TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadRelativeFlag.LongHelp = "Relative Radius ON/OFF";
TabCont.GeneralTab.MainFrame.RadiusFrame.VariableRadChkBtn.ShortHelp = "Variable Radius";
TabCont.GeneralTab.MainFrame.RadiusFrame.VariableRadChkBtn.LongHelp = "Variable Radius ON/OFF";

TabCont.GeneralTab.MainFrame.ArcTypeFrame.BlendRadio.ShortHelp = "Blend";
TabCont.GeneralTab.MainFrame.ArcTypeFrame.BlendRadio.LongHelp = "Creates a blend surface between input elements";
TabCont.GeneralTab.MainFrame.ArcTypeFrame.ApproxRadio.ShortHelp = "Approx";
TabCont.GeneralTab.MainFrame.ArcTypeFrame.ApproxRadio.LongHelp = "Creates a circular Bezier approximation between input elements";
TabCont.GeneralTab.MainFrame.ArcTypeFrame.ExactRadio.ShortHelp = "Exact";
TabCont.GeneralTab.MainFrame.ArcTypeFrame.ExactRadio.LongHelp = "Creates a rational surface with true circular sections";

TabCont.GeneralTab.MainFrame.ParameterFrame.ParameterCombo.ShortHelp = "Parameter";
TabCont.GeneralTab.MainFrame.ParameterFrame.ParameterCombo.LongHelp = " Different options for parameterization of the fillet surface";

TabCont.GeneralTab.MainFrame.ResultFrame.TrimSwitch.ShortHelp = "Trim Result";
TabCont.GeneralTab.MainFrame.ResultFrame.TrimSwitch.LongHelp = "Trim Result option ON/OFF";
TabCont.GeneralTab.MainFrame.ResultFrame.TrimCombo.ShortHelp = "TrimFace / TrimApprox";
TabCont.GeneralTab.MainFrame.ResultFrame.TrimCombo.LongHelp = "Type of the trim result option Trim Face - The result is a face
                                                               Trim Approx - Result is not a face";
TabCont.GeneralTab.MainFrame.ResultFrame.Extrapol.ShortHelp = "Extrapolate";
TabCont.GeneralTab.MainFrame.ResultFrame.Extrapol.LongHelp = "Extrapolates the result fillet surface";
TabCont.GeneralTab.MainFrame.ResultFrame.SmallSur.ShortHelp = "Small Surface";
TabCont.GeneralTab.MainFrame.ResultFrame.SmallSur.LongHelp = "If both input sets have more than one surface and the boundaries of the surfaces are 
                                                              not exactly on the same position, the result is a (very) small surface. Its ON/OFF mode";
TabCont.GeneralTab.MainFrame.ResultFrame.SmallSurCombo.ShortHelp = "Stitch/Rip";
TabCont.GeneralTab.MainFrame.ResultFrame.SmallSurCombo.LongHelp = "Stitch: The small surface can be stitched with one of the neighbor surfaces
                                                                   Rip: The small surface is (along its diagonal) divided into two surfaces";
TabCont.GeneralTab.MainFrame.ResultFrame.SmallSurfaceTolerancee.ShortHelp = "Small Surface Tolerance";
TabCont.GeneralTab.MainFrame.ResultFrame.SmallSurfaceTolerancee.LongHelp = "Tolerance for creating the small surface";

TabCont.GeneralTab.MainFrame.OptionFrame.TrimBase.ShortHelp = "Trim Input";
TabCont.GeneralTab.MainFrame.OptionFrame.TrimBase.LongHelp = "Trims the input elements of the fillet";
TabCont.GeneralTab.MainFrame.OptionFrame.ChordalFillet.ShortHelp = "Chordal Fillet";
TabCont.GeneralTab.MainFrame.OptionFrame.ChordalFillet.LongHelp = "The distance between the edges of the fillet in direction of the rolling ball is the chordal distance";
TabCont.GeneralTab.MainFrame.OptionFrame.CtrlPts.ShortHelp = "Control points";
TabCont.GeneralTab.MainFrame.OptionFrame.CtrlPts.LongHelp = "Enables shape modifications of the fillet surface";
TabCont.GeneralTab.MainFrame.OptionFrame.TrueMinimalRadius.ShortHelp = "True Minimum";
TabCont.GeneralTab.MainFrame.OptionFrame.TrueMinimalRadius.LongHelp = "Minimum radius is controlled";
TabCont.GeneralTab.MainFrame.OptionFrame.IndependentApproximation.ShortHelp = "Independent Approximation";
TabCont.GeneralTab.MainFrame.OptionFrame.IndependentApproximation.LongHelp = "Independent internal approximation";

TabCont.GeneralTab.MainFrame.SelectionFrame.FirstSetSur.ShortHelp = "Select surface set 1";
TabCont.GeneralTab.MainFrame.SelectionFrame.FirstSetSur.LongHelp = "Select first set of surfaces for fillet operation";
TabCont.GeneralTab.MainFrame.SelectionFrame.SecondSetSur.ShortHelp = "Select surface set 2";
TabCont.GeneralTab.MainFrame.SelectionFrame.SecondSetSur.LongHelp = "Select second set of surfaces for fillet operation";
TabCont.GeneralTab.MainFrame.SelectionFrame.FirstSetCur.ShortHelp = "Select curve set 1";
TabCont.GeneralTab.MainFrame.SelectionFrame.FirstSetCur.LongHelp = "Select first set of curves for fillet operation";
TabCont.GeneralTab.MainFrame.SelectionFrame.SecondSetCur.ShortHelp = "Select curve set 2";
TabCont.GeneralTab.MainFrame.SelectionFrame.SecondSetCur.LongHelp = "Select second set of curves for fillet operation";
TabCont.GeneralTab.MainFrame.SelectionFrame.SpineSet.ShortHelp = "Select spine set";
TabCont.GeneralTab.MainFrame.SelectionFrame.SpineSet.LongHelp = "Select the spine for fillet operation";
TabCont.GeneralTab.MainFrame.SelectionFrame.PtOnSpineSet.ShortHelp = "Select point on spine";
TabCont.GeneralTab.MainFrame.SelectionFrame.PtOnSpineSet.LongHelp = "Select a point on spine for fillet operation";


// New UI

MainOuterFrameTabs.TabCont.GeneralTab.Title = "Options";
// MainOuterFrameTabs.TabCont.ApproxTab.Title = "Advanced"; // WAP 08:08:26 R19SP4 HL2 - Nls entry commented.


// Continuity Frame
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContinuityFrame.Title  = "Continuity";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContinuityFrame.G0Radio.ShortHelp = "G0";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContinuityFrame.G0Radio.LongHelp = "G0 continuity between the fillet and input elements";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContinuityFrame.G1Radio.ShortHelp = "G1";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContinuityFrame.G1Radio.LongHelp = "G1 continuity between the fillet and input elements";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContinuityFrame.G2Radio.ShortHelp = "G2";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContinuityFrame.G2Radio.LongHelp = "G2 continuity between the fillet and input elements";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContinuityFrame.G3Radio.ShortHelp = "G3";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContinuityFrame.G3Radio.LongHelp = "G3 continuity between the fillet and input elements";

// Radius Frame
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.Title  = "Radius Parameters";
//MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.RadiusLabel.Title = "Radius :"; // WAP 08:11:25 R19SP3 HL
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.RadiusLabel.ShortHelp = "Constant Radius";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.RadiusLabel.LongHelp = "Constant Radius for the fillet";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.Radius.ShortHelp = "Constant Radius";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.Radius.LongHelp = "Constant Radius for the fillet";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadFlag.Title = "Min Radius :";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadFlag.ShortHelp = "Minimum Radius";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadFlag.LongHelp = "Minimum radius ON/OFF";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadius.ShortHelp = "Minimum radius value";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadius.LongHelp = "Minimum radius value";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadRelativeFlag.ShortHelp = "Relative Radius";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadRelativeFlag.LongHelp = "Relative Radius ON/OFF";

// Arc Type 
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.ArcTypeFrame.Title  = "Arc Type";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.ArcTypeFrame.BlendRadio.ShortHelp = "Blend";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.ArcTypeFrame.BlendRadio.LongHelp = "Creates a blend surface between input elements";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.ArcTypeFrame.ApproxRadio.ShortHelp = "Approx";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.ArcTypeFrame.ApproxRadio.LongHelp = "Creates a circular Bezier approximation between input elements";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.ArcTypeFrame.ExactRadio.ShortHelp = "Exact";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.ArcTypeFrame.ExactRadio.LongHelp = "Creates a rational surface with true circular sections";


MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.Title  = "Result";

// Simplification Frame
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.Title = "Simplification";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.SmallSurStich.Title = "Stitch";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.SmallSurStich.ShortHelp = "Small Surface - Stitch";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.SmallSurStich.LongHelp = "Stitch: The small surface can be stitched with one of the neighbor surfaces";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.SmallSurRip.Title = "Rip";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.SmallSurRip.ShortHelp = "Small Surface - Rip";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.SmallSurRip.LongHelp = "Rip: The small surface is (along its diagonal) divided into two surfaces";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.SmallSurfaceTolerancee.ShortHelp = "Small Surface Tolerance";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.SmallSurfaceTolerancee.LongHelp = "Tolerance for creating the small surface";
                                                              
// Relimitation Frame
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.RelimitationFrame.Title = "Relimitation";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.RelimitationFrame.TrimFace.Title = "Trim Face";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.RelimitationFrame.TrimFace.ShortHelp = "Trim Face";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.RelimitationFrame.TrimFace.LongHelp = "The result is a face";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.RelimitationFrame.TrimApprox.Title = "Trim Approx";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.RelimitationFrame.TrimApprox.ShortHelp = "Trim Approx";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.RelimitationFrame.TrimApprox.LongHelp = "The result is not a face";
// For R18SP3 UI
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.Title = "Geometry";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.Extrapol1.ShortHelp = "Extrapolate Side1";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.Extrapol1.LongHelp = "The result is extrapolated on side1";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.Extrapol2.ShortHelp = "Extrapolate Side2";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.Extrapol2.LongHelp = "The result is extrapolated on side2";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.Relimit1.ShortHelp = "Relimit Side1";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.Relimit1.LongHelp = "The result is relimited on side1";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.Relimit2.ShortHelp = "Relimit Side2";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.Relimit2.LongHelp = "The result is relimited on side2";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.TrimFace.Title = "Trim Face";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.TrimFace.ShortHelp = "Trim Face";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.TrimFace.LongHelp = "The result is a face";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.TrimApprox.Title = "Trim Approx";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.TrimApprox.ShortHelp = "Trim Approx";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.TrimApprox.LongHelp = "The result is not a face";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.Merge.ShortHelp = "Concatenate";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.Merge.LongHelp = "Concatenates a multi-cell fillet ribbon into a mono-cell result (only possible in case of G2 continuity between cells)";

// Geometry Frame
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.GeometryFrame.Title = "Geometry";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.GeometryFrame.Extrapol.Title = "Extrapolate";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.GeometryFrame.Extrapol.ShortHelp = "Extrapolate";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.GeometryFrame.Extrapol.LongHelp = "Extrapolates the result fillet surface";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.GeometryFrame.Merge.ShortHelp = "Concatenate";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.GeometryFrame.Merge.LongHelp = "Concatenates a multi-cell fillet ribbon into a mono-cell result (only possible in case of G2 continuity between cells)";
//--------------------------------------
                                                                      
// Options Frame
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.Title  = "Fillet Type";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.VariableRadChkBtn.ShortHelp = "Variable Radius";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.VariableRadChkBtn.LongHelp = "Variable Radius ON/OFF";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.TrimBase.ShortHelp = "Trim Input";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.TrimBase.LongHelp = "Trims the input elements of the fillet";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.ChordalFillet.ShortHelp = "Chordal Fillet";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.ChordalFillet.LongHelp = "The distance between the edges of the fillet in direction of the rolling ball is the chordal distance";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.CtrlPts.ShortHelp = "Control points";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.CtrlPts.LongHelp = "Enables shape modifications of the fillet surface";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.TrueMinimalRadius.ShortHelp = "True Minimum";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.TrueMinimalRadius.LongHelp = "Minimum radius is controlled";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.IndependentApproximation.ShortHelp = "Independent Approximation";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.IndependentApproximation.LongHelp = "Independent internal approximation";


// Surface-1 Curv-1
//MainOuterFrame.Surface1Curve1Frame.Title = "Support 1";
//MainOuterFrame.Surface1Curve1Frame.ShortHelp = "Select surface and curve set 1";
//MainOuterFrame.Surface1Curve1Frame.LongHelp = "Select first set of surfaces and curves for fillet operation";
//MainOuterFrame.Surface1Curve1Frame.Surface1Selector.ShortHelp = "Select surface set 1";
//MainOuterFrame.Surface1Curve1Frame.Surface1Selector.LongHelp = "Select first set of surfaces for fillet operation";
MainOuterFrame.Surface1Curve1Frame.Curve1Selector.ShortHelp = "Select curve set 1";// WAP 09:01:23
MainOuterFrame.Surface1Curve1Frame.Curve1Selector.LongHelp = "Select first set of curves for fillet operation"; // WAP 09:01:23 
//MainOuterFrame.Surface1Curve1Frame.LabelSurface1.Title = "Surface :";
//MainOuterFrame.Surface1Curve1Frame.LabelSurface1.ShortHelp = "Select surface set 1";
//MainOuterFrame.Surface1Curve1Frame.LabelSurface1.LongHelp = "Select first set of surfaces for fillet operation";
MainOuterFrame.Surface1Curve1Frame.LabelCurve1.Title = "Curve :";
MainOuterFrame.Surface1Curve1Frame.LabelCurve1.ShortHelp = "Select curve set 1";
MainOuterFrame.Surface1Curve1Frame.LabelCurve1.LongHelp = "Select first set of curves for fillet operation";
MainOuterFrame.Surface1Curve1Frame.TrimBase1.ShortHelp = "Trim Support 1";
MainOuterFrame.Surface1Curve1Frame.TrimBase1.LongHelp = "Trims the input elements (surfaces) in Support 1 set of the fillet.";
//MainOuterFrame.Surface1Curve1Frame.Curve1SelState.ShortHelp = "Enable curve 1 set";// WAP 09:01:23 
//MainOuterFrame.Surface1Curve1Frame.Curve1SelState.LongHelp = "Enables selection in the curve 1 set.";// WAP 09:01:23 

// Surface-2 Curv-2
MainOuterFrame.Surface2Curve2Frame.Title = "Support 2";
MainOuterFrame.Surface2Curve2Frame.ShortHelp = "Select surface and curve set 2";
MainOuterFrame.Surface2Curve2Frame.LongHelp = "Select second set of surfaces and curves for fillet operation";
MainOuterFrame.Surface2Curve2Frame.Surface2Selector.ShortHelp = "Select surface set 2";
MainOuterFrame.Surface2Curve2Frame.Surface2Selector.LongHelp = "Select second set of surfaces for fillet operation";
//MainOuterFrame.Surface2Curve2Frame.Curve2Selector.ShortHelp = "Select curve set 2"; // WAP 09:01:23
//MainOuterFrame.Surface2Curve2Frame.Curve2Selector.LongHelp = "Select second set of curves for fillet operation"; // WAP 09:01:23
MainOuterFrame.Surface2Curve2Frame.LabelSurface2.Title = "Surface :";
MainOuterFrame.Surface2Curve2Frame.LabelSurface2.ShortHelp = "Select surface set 2";
MainOuterFrame.Surface2Curve2Frame.LabelSurface2.LongHelp = "Select second set of surfaces for fillet operation";
MainOuterFrame.Surface2Curve2Frame.LabelCurve2.Title = "Curve :";
MainOuterFrame.Surface2Curve2Frame.LabelCurve2.ShortHelp = "Select curve set 2";
MainOuterFrame.Surface2Curve2Frame.LabelCurve2.LongHelp = "Select second set of curves for fillet operation";
MainOuterFrame.Surface2Curve2Frame.TrimBase2.ShortHelp = "Trim Support 2";
MainOuterFrame.Surface2Curve2Frame.TrimBase2.LongHelp = "Trims the input elements (surfaces) in Support 2 set of the fillet.";
//MainOuterFrame.Surface2Curve2Frame.Curve2SelState.ShortHelp = "Enable curve 2 set";// WAP 09:01:23 
//MainOuterFrame.Surface2Curve2Frame.Curve2SelState.LongHelp = "Enables selection in the curve 2 set.";// WAP 09:01:23 

// Point-Spine
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.SpinePointFrame.SpineSelector.ShortHelp = "Select spine set";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.SpinePointFrame.SpineSelector.LongHelp = "Select the spine for fillet operation";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.SpinePointFrame.PointSelector.ShortHelp = "Select point on spine";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.SpinePointFrame.PointSelector.LongHelp = "Select a point on spine for fillet operation";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.SpinePointFrame.LabelSpine.Title = "Spine :";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.SpinePointFrame.LabelSpine.ShortHelp = "Select spine set";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.SpinePointFrame.LabelSpine.LongHelp = "Select the spine for fillet operation";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.SpinePointFrame.LabelPoint.Title = "Point :";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.SpinePointFrame.LabelPoint.ShortHelp = "Select point on spine";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.SpinePointFrame.LabelPoint.LongHelp = "Select a point on spine for fillet operation";

// Parameters
MainOuterFrameTabs.TabCont.ApproxTab.ParameterFrame.Title  = "Parameter";
MainOuterFrameTabs.TabCont.ApproxTab.ParameterFrame.ParameterCombo.ShortHelp = "Parameter";
MainOuterFrameTabs.TabCont.ApproxTab.ParameterFrame.ParameterCombo.LongHelp = " Different options for parameterization of the fillet surface";
MainOuterFrameTabs.TabCont.ApproxTab.ParameterFrame.ParameterCombo.PDefault.Title = "Default";
MainOuterFrameTabs.TabCont.ApproxTab.ParameterFrame.ParameterCombo.PPatch1.Title = "Patch 1";
MainOuterFrameTabs.TabCont.ApproxTab.ParameterFrame.ParameterCombo.PPatch2.Title = "Patch 2";
MainOuterFrameTabs.TabCont.ApproxTab.ParameterFrame.ParameterCombo.PAverage.Title = "Average";
MainOuterFrameTabs.TabCont.ApproxTab.ParameterFrame.ParameterCombo.PBlend.Title = "Blend";
MainOuterFrameTabs.TabCont.ApproxTab.ParameterFrame.ParameterCombo.PChordal.Title = "Chordal";

// Independent Approximation
MainOuterFrameTabs.TabCont.ApproxTab.IndependentApproximation.Title = "Independent Approximation";
MainOuterFrameTabs.TabCont.ApproxTab.IndependentApproximation.ShortHelp = "Independent Approximation";
MainOuterFrameTabs.TabCont.ApproxTab.IndependentApproximation.LongHelp = "Independent internal approximation";

Surface1BagTitle.Message = "Select Surface 1";
Curve1BagTitle.Message = "Select Curve 1";
Surface2BagTitle.Message = "Select Surface 2";
Curve2BagTitle.Message = "Select Curve 2";
SpineBagTitle.Message = "Select Spine";
PointOnSpineBagTitle.Message = "Select Point On Spine";

MaxDevOptionsFrame.MaxDevDisplayOptionsFrame.Title = "Deviation Display";
MaxDevOptionsFrame.MaxDevDisplayOptionsFrame.DevBetFilletCellsChkB.ShortHelp = "Connection Between Fillet Cells";
MaxDevOptionsFrame.MaxDevDisplayOptionsFrame.DevBetFilletCellsChkB.LongHelp = "Displays Connection Between Fillet Ribbon Cells only";
MaxDevOptionsFrame.MaxDevDisplayOptionsFrame.DevBetFilletAndSupportChkB.ShortHelp = "Connection Between Fillet ribbon and Support";
MaxDevOptionsFrame.MaxDevDisplayOptionsFrame.DevBetFilletAndSupportChkB.LongHelp = "Displays Connection Between Fillet ribbon and Support";
MaxDevOptionsFrame.MaxDevValueFrame.Title = "Max Deviation";
MaxDevOptionsFrame.MaxDevValueFrame.LongHelp = "Shows Max of Max value for each continuity";
MaxDevOptionsFrame.MaxDevValueFrame.G0TitleLabel.Title = "G0:";
MaxDevOptionsFrame.MaxDevValueFrame.G0Label = "----   ";
MaxDevOptionsFrame.MaxDevValueFrame.G1TitleLabel.Title = "G1:";
MaxDevOptionsFrame.MaxDevValueFrame.G2TitleLabel.Title = "G2:";
MaxDevOptionsFrame.MaxDevValueFrame.G3TitleLabel.Title = "G3:";

// Start WAP 08:08:26 R19SP4 HL1 AND HL2

NewApproxTab.Message = "Approximation";
ApproxTab.Message = "Advanced";
OutputResultFrame.Title = "Output Result";
OutputResultFrame.NbOfCellsTitleLabel.Title = "No. of cells:";
OutputResultFrame.MaxOrderTitleLabel.Title = "Max. Order:";
OutputResultFrame.MaxSegTitleLabel.Title = "Max. Segments per Cell:";
OutputResultFrame.OrdUTitleLabel.Title = "U:";
OutputResultFrame.OrdVTitleLabel.Title = "V:";
OutputResultFrame.SegUTitleLabel.Title = "U:";
OutputResultFrame.SegVTitleLabel.Title = "V:";
OutputResultFrame.NbOfCellsTitleLabel.ShortHelp = "Displays the Number of cells";
OutputResultFrame.NbOfCellsTitleLabel.LongHelp = "Displays the Number of cells in fillet created";
OutputResultFrame.MaxOrderTitleLabel.ShortHelp = "Displays Max order in U and V direction";
OutputResultFrame.MaxOrderTitleLabel.LongHelp = "Displays the maximum order of the created result in both U and V directions"; 
OutputResultFrame.MaxSegTitleLabel.ShortHelp = "Displays Max segments per cell in U and V direction";
OutputResultFrame.MaxSegTitleLabel.LongHelp = "Displays the maximum number of segments per cell of the created result in both U and V directions"; 
// End WAP 08:08:26	

// Start WAP 08:11:25 R19SP3 HL
LengthLabel.Message = "Length :"; 
RadiusLabel.Message = "Radius :";
// End WAP 08:11:25

// Start WAP 08:12:02 R19SP4 HL6
//Spine - Point Frame	
MainOuterFrame.SpinePointFrame.LabelSpine.Title = "Spine:";
MainOuterFrame.SpinePointFrame.LabelPoint.Title = "Point:";
MainOuterFrame.SpinePointFrame.SpineSelector.ShortHelp = "Select spine set";
MainOuterFrame.SpinePointFrame.SpineSelector.LongHelp = "Select the spine for fillet operation";
MainOuterFrame.SpinePointFrame.PointSelector.ShortHelp = "Select point on spine";
MainOuterFrame.SpinePointFrame.PointSelector.LongHelp = "Select a point on spine for fillet operation";

// Geometry Frame
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.LabelExtrapolRelimit1.Title ="Extrapolate/Relimit Side1";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.LabelExtrapolRelimit1.ShortHelp = "Extrapolate/Relimit on Side1";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.LabelExtrapolRelimit1.LongHelp ="The result is extrapolated/Relimited on side1";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.LabelExtrapolRelimit2.Title ="Extrapolate/Relimit Side2";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.LabelExtrapolRelimit2.ShortHelp = "Extrapolate/Relimit on Side2";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.LabelExtrapolRelimit2.LongHelp ="The result is extrapolated/Relimited on side2";

// Result Frame
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.LogicalMerge.Title = "Logical Join";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.LogicalMerge.ShortHelp = "Logical Join";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.LogicalMerge.LongHelp = "Assembles the result (Fillet ribbon, trimmed input surfaces) with fixed tolerance 0.1 mm";

// Simplification Frame
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.Merge.Title = "Concatenate";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.Merge.ShortHelp = "Concatenate";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.Merge.LongHelp = "Concatenates a multi-cell fillet ribbon into a mono-cell result (only possible in case of G2 continuity between cells)";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.LogicalMerge.Title = "Logical Join";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.LogicalMerge.ShortHelp = "Logical Join";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.LogicalMerge.LongHelp = "Assembles the result (Fillet ribbon, trimmed input surfaces) with fixed tolerance 0.1 mm";
// End WAP 08:12:02

// Start WAP 09:01:23 R19SP4 HL5
Curve1SelectorShortHelp.Message = "Select curve set 1";
Curve1SelectorLongHelp.Message = "Select first set of curves for fillet operation";
Curve1SelStateShortHelp.Message = "Enable curve 1 set";
Curve1SelStateLongHelp.Message = "Enables selection in the curve 1 set.";

Curve2SelectorShortHelp.Message = "Select curve set 2";
Curve2SelectorLongHelp.Message = "Select second set of curves for fillet operation";
Curve2SelStateShortHelp.Message = "Enable curve 2 set"; 
Curve2SelStateLongHelp.Message = "Enables selection in the curve 2 set.";


EdgesToExtrapol1SelectorShortHelp.Message = "Select Edge to Extrapolate 1";
EdgesToExtrapol1SelectorLongHelp.Message = "Select first set of edges to extrapolate for variable fillet operation";
EdgesToExtrapol1SelStateShortHelp.Message = "Enable Edge to Extrapolate 1";
EdgesToExtrapol1SelStateLongHelp.Message = "Enables selection of first set of edges to extrapolate for variable fillet operation";
EdgesToExtrapol2SelectorShortHelp.Message = "Select Edge to Extrapolate 2";
EdgesToExtrapol2SelectorLongHelp.Message = "Select second set of edges to extrapolate for variable fillet operation";
EdgesToExtrapol2SelStateShortHelp.Message = "Enable Edge to Extrapolate 2";
EdgesToExtrapol2SelectorLongHelp.Message = "Enables selection of second set of edges to extrapolate for variable fillet operation";

// End WAP 09:01:23

// Start WAP 08:12:02 R19SP5
// Input Parameter Frame
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.InputParameterFrame.Title = "Input Parameters";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.InputParameterFrame.LabelMergeDist.Title = "Merging Distance: ";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.InputParameterFrame.SpinnerMergeDist.ShortHelp = "Merging Distance";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.InputParameterFrame.SpinnerMergeDist.LongHelp = "Set Merging Distance for MultipleCell Fillet";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.InputParameterFrame.TolerantLaydownCheck.ShortHelp = "Tolerant Laydown";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.InputParameterFrame.TolerantLaydownCheck.LongHelp = "Use Tolerant laydown";
TolerantLaydown.Title = "Tolerant Laydown";
// End WAP 08:12:02					   

MainOuterFrame.Surface2Curve2Frame.EdgeFilletMode.Title = "Fillet Type";
MainOuterFrame.Surface2Curve2Frame.EdgeFilletMode.ShortHelp = "Fillet Type";
MainOuterFrame.Surface2Curve2Frame.EdgeFilletMode.LongHelp = "Select the type of fillet operations. Support Fillet / Edge Fillet";

EdgeFilletModeLabel.Message = "Object(s) to fillet"; 
SupportFilletMode.Message = "Support 1";

Surface1Curve1FrameShortHelp.Message = "Select surface and curve set 1";
Surface1Curve1FrameLongHelp.Message = "Select first set of surfaces and curves for fillet operation";
ObjectsToFilletFrameShortHelp.Message = "Select object(s) to fillet";
ObjectsToFilletFrameLongHelp.Message = "Select sharp edges / faces with sharp edges for fillet operation";

Surface1Title.Message = "Surface :";
Surface1ShortHelp.Message = "Select surface set 1";
Surface1LongHelp.Message = "Select first set of surfaces for fillet operation";

ObjectToFilletTitle.Message = "Object(s) to fillet :";
ObjectToFilletShortHelp.Message = "Select object(s) to fillet";
ObjectToFilletLongHelp.Message = "Select sharp edges / faces with sharp edges for fillet operation";

Surface1SelectorShortHelp.Message = "Select surface set 1";
Surface1SelectorLongHelp.Message = "Select first set of surfaces for fillet operation";
ObjectsToFilletSelectorShortHelp.Message = "Select object(s) to fillet";
ObjectsToFilletSelectorLongHelp.Message = "Select sharp edges / faces with sharp edges for fillet operation";
