//------------------------------------------------------
// Resource file for input topological objects errors 
// En_EN
//================================================================
//                           INPUT
// THM  2016/05/12  error 4508 suppress highlighted from the text
//================================================================
//----------------------------------Null or invalid input
//
TopObInNullPointer="Il puntatore dei dati immessi /P1 � nullo.
 Definire questi dati o verificare che il puntatore non sia stato spostato.";
//
TopObInNullData="I dati immessi /P1 sono nulli.";
//
TopObInEmptyObject="L'oggetto /P1 � vuoto.";
//
TopObInInvalidType="/p1 � un tipo di dati immessi /p2 non valido. Utilizzare il tipo /p3.";
//
TopObInIndex="L'indice /p1 non � compreso nell'intervallo. Valore massimo: /p2";
//
TopObInMixPolyExactInputs="La combinazione di immissioni poliedriche ed esatte non � consentita per questo operatore. Convertire le immissioni esatte in dati poliedrici per eseguire l'operazione.";
//----------------------------------Orientation
//
TopObInBadOrientation="Valore di orientamento non valido: /p1 non � un orientamento valido per /p2.";
//
TopObInBadMatterSideValue="Valore di orientamento del materiale non valido: /P1 non � un valore di orientamento del materiale valido per /p2.";
//
TopObInBadLocationValue="Valore di posizione non valido: /P1 non � un valore di posizione valido per /p2.";
//
TopObInBadSideValue="Valore del lato non valido: /P1 non � un valore di lato valido per /p2.";
//
//----------------------------------Invalid dimension
//                               /p1 = cell, domain, .. /p2= good dimension
TopObInInvalidDimension="La quota /p1 non � valida: utilizzare /p1 di quota /p2.";
//
TopObInTooSmallDimension="La quota /p1 � troppo piccola: la quota deve essere almeno /p2.";
//
TopObInTooLargeDimension="La quota /p1 � troppo grande: la quota deve essere al massimo /p2.";
//
TopObInIncompatibleElement=" /p1 � incompatibile con /p2.";
//  
//----------------------------------invalid element                          
//
TopObInNonModifiableElement=" /p1 non � modificabile.";
//
TopObInNoGeometry=" /p1 non presenta nessuna geometria associata.";
//
TopObInIncomplete=" /p1 � incompleto.";
//
TopObInIncompleteSplit=" Operazione di taglio: la faccia non taglia completamente il body.
Selezionare un'altra faccia.";
//
TopObInSurSurUndetectedConfusion=" Non � stata rilevata una confusione di tipo geometrico tra le due facce.
Verificare il contatto tra i body.";
//
//----------------------------------All topological objects
//
TopObInCell="cella";
//
TopObInDomain="dominio";
//
TopObInBody="body";
//
TopObInVertex="vertice";
//
TopObInEdge="spigolo";
//
TopObInFace="faccia";
//
TopObInVolume="volume";
//
TopObInLoop="loop";
//
TopObInLump="imbozzatura";
//
TopObInVertexInVolume="VertexInVolume";
//
TopObInVertexInFace="VertexInFace";
//
TopObInWire="figura";
//================================================================
//                           INTERNAL
//================================================================
//----------------------------------Unvailable
//
TopObIntUnvailable="Il metodo /P1 per l'oggetto /P2 non � ancora disponibile.";
//
TopObIntobjectCreationFailed="Non � possibile creare un oggetto della classe /p1.";
//
TopObIntInterfaceFailed="Il bind dell'implementazione /p1 all'interfaccia /p2 non � riuscito.
      Controllare il dizionario.";
//
TopObIntImplFailed="Il bind dell'interfaccia /p1 all'implementazione /p2 non � riuscito.
      Controllare il dizionario.";
//
TopObIntMethodCallOrder="Ordine di richiami di metodi non valido. Il metodo /p1 deve essere richiamato dopo /p2.";
//
TopObIntObjectType="Nessuna classe pu� corrispondere a un tipo di valore /p1.";
//
TopObIntBadTopology="� stata rilevata una configurazione topologica non valida.";
//
TopObIntImpossibleWriting="Non � possibile scrivere l'oggetto /p1.";
//
TopObIntAllocation="Impossibile assegnare memoria.";
//
TopObIntNoSubd="L'elemento /p1 non ha un dominio secondario /p2.";
//
TopObIntSubd="Errore interno /p1, /p2.";
//
TopObIntBadGeometry="Un errore interno /p1 provoca una configurazione geometrica errata.";
//
TopObIntMath="Errore /p1 nel pacchetto matematico.";
//
// Topological model error 4000-4499
//
TopObInCheckJournal_ERR_4000.Request="Verifica del giornale topologico non riuscita.";
TopObInCheckJournal_ERR_4000.Diagnostic="Gli ordini del giornale non portano dai body di immissione al body di emissione.";
TopObInCheckJournal_ERR_4000.Advice="Verificare gli ordini del giornale o copiare/non copiare i body di immissione.";
//
TopObInLyingOnAmbiguousGeometry_ERR_4100.Request="Riferimento geometrico ambiguo di LyingOn.";
TopObInLyingOnAmbiguousGeometry_ERR_4100.Diagnostic="/P1 giace su /P2 con riferimenti ambigui /P3 e /P4.";
TopObInLyingOnAmbiguousGeometry_ERR_4100.Advice="Evitare le geometrie LyingOn ambigue.";
//
//
// Boolean Operators 4500-4999
//
TopObInBoolIncompleteSplit_ERR_4500.Request="Operatore booleano: spezzato da uno skin.";
TopObInBoolIncompleteSplit_ERR_4500.Diagnostic="Uno skin non taglia completamente uno dei volumi: l'operazione spezza non pu� essere calcolata";
TopObInBoolIncompleteSplit_ERR_4500.Advice="Selezionare uno skin che tagli completamente un volume del body.";
//
TopObInBoolImpossibleSplit_ERR_4501.Request="Operatore booleano: spezzato da uno skin.";
TopObInBoolImpossibleSplit_ERR_4501.Diagnostic="Uno skin ha un foro e tale foro � all'interno del volume: l'operazione spezza non pu� essere calcolata";
TopObInBoolImpossibleSplit_ERR_4501.Advice="Scegliere uno skin i cui fori (se presenti) non siano all'interno del volume del body.";
//
TopObInBoolUnresolvedSpec_ERR_4502.Request="Operatore booleano: specifiche non risolte.";
TopObInBoolUnresolvedSpec_ERR_4502.Diagnostic="Non � possibile risolvere la specifica: l'operazione di relimitazione non viene eseguita.";
TopObInBoolUnresolvedSpec_ERR_4502.Advice="Modificare la selezione delle facce.";
//
TopObInBoolIncompleteCut_ERR_4503.Request="Operatore booleano: taglio impossibile.";
TopObInBoolIncompleteCut_ERR_4503.Diagnostic="Uno body non taglia completamente l'altro: l'operazione spezza non pu� essere calcolata.";
TopObInBoolIncompleteCut_ERR_4503.Advice="Modificare i contatti del body in modo che si taglino reciprocamente in modo completo.";
//
TopObInBoolIncompleteLayDown_ERR_4504.Request="Operatore booleano: bordo incompleto.";
TopObInBoolIncompleteLayDown_ERR_4504.Diagnostic="Il bordo dello skin non giace completamente sul body di relimitazione.";
TopObInBoolIncompleteLayDown_ERR_4504.Advice="Modificare i contatti del bordo dello skin in modo che il bordo poggi completamente. Provare ad aggiungere alcuni vincoli.";
//
TopObInBoolImpossibleSewing_ERR_4505.Request="Operatore di cucitura: continuazione impossibile per orientamenti incompatibili.";
TopObInBoolImpossibleSewing_ERR_4505.Diagnostic="L'orientamento dello skin di cucitura produce un risultato non valido.";
TopObInBoolImpossibleSewing_ERR_4505.Advice="Invertire l'orientamento dello skin di cucitura.";
//
TopObInBoolImpossibleReport_ERR_4506.Request="Operatore booleano: calcolo dell'intersezione geometrica impossibile intorno allo spigolo.";
TopObInBoolImpossibleReport_ERR_4506.Diagnostic="Calcolo dell'intersezione geometrica impossibile: la tangente o gli elementi piccoli producono intersezioni di cattiva qualit�.";
TopObInBoolImpossibleReport_ERR_4506.Advice="Modificare i contatti del body (utilizzare taglia o estrudere uno dei body).";
//
TopObInBoolMissedConfusion_ERR_4507.Request="Operatore booleano: rilevato disegno tangente.";
TopObInBoolMissedConfusion_ERR_4507.Diagnostic="L'area tangente non pu� essere risolta.";
TopObInBoolMissedConfusion_ERR_4507.Advice="Modificare i contatti del body (utilizzare taglia o cucire uno dei body).";
//
TopObInBoolFaceFaceFailed_ERR_4508.Request="Operatore booleano: intersezione faccia/faccia non riuscita.";
TopObInBoolFaceFaceFailed_ERR_4508.Diagnostic="Il confronto tra le facce non pu� essere risolto.";
TopObInBoolFaceFaceFailed_ERR_4508.Advice="Modificare la qualit� dei contatti.";
//
TopObInBoolNoLayDown_ERR_4509.Request="Operatore booleano: Nessun bordo.";
TopObInBoolNoLayDown_ERR_4509.Diagnostic="Nessuno dei bordi dello skin giace sul body di relimitazione.";
TopObInBoolNoLayDown_ERR_4509.Advice="Modificare i contatti del bordo dello skin in modo che il bordo poggi completamente. ";
//
TopObInBoolTwistedVolume_ERR_4510.Request="Operatore booleano: � stato generato un volume a spirale.";
TopObInBoolTwistedVolume_ERR_4510.Diagnostic="Le posizioni relative dei body cos� come i contatti sono ambigui.";
TopObInBoolTwistedVolume_ERR_4510.Advice="Modificare l'ordine delle operazioni.";
//
TopObInBoolInaccurateContact_ERR_4511.Request="Operatore booleano: Contatto tra le facce impreciso.";
TopObInBoolInaccurateContact_ERR_4511.Diagnostic="Il confronto tra le facce non pu� essere risolto.";
TopObInBoolInaccurateContact_ERR_4511.Advice="Modificare la qualit� dei contatti o provare a cucire uno dei body all'altro.";
//
TopObInBoolImpossibleCut_ERR_4512.Request="Operatore booleano: l'elemento non pu� essere tagliato.";
TopObInBoolImpossibleCut_ERR_4512.Diagnostic="L'elemento non pu� essere tagliato: verificare i contatti di tangenza.";
TopObInBoolImpossibleCut_ERR_4512.Advice="Modificare i contatti del body (utilizzare taglia o estrudere uno dei body).";
//
TopObInSmartProjIncompleteSolution_ERR_4513.Request="Operatore di proiezione smart";
TopObInSmartProjIncompleteSolution_ERR_4513.Diagnostic="Impossibile completare la proiezione della figura";
TopObInSmartProjIncompleteSolution_ERR_4513.Advice="Estrudere lo svuotamento originale o utilizzare la proiezione e selezionare le parti corrette.";
//
TopObInSmartProjNoInitialisation_ERR_4514.Request="Operatore di proiezione smart";
TopObInSmartProjNoInitialisation_ERR_4514.Diagnostic="Impossibile iniziare la proiezione della figura";
TopObInSmartProjNoInitialisation_ERR_4514.Advice="La proiezione fornisce solo punti o nessuna soluzione oppure sono presenti troppe soluzioni senza selezioni possibili.";
//
TopObInSmartProjCheckDistance_ERR_4515.Request="Operatore di proiezione smart";
TopObInSmartProjCheckDistance_ERR_4515.Diagnostic="Una proiezione smart della figura fornisce diverse soluzioni";
TopObInSmartProjCheckDistance_ERR_4515.Advice="Nessuna proiezione � sempre la proiezione pi� vicina. Utilizzare la proiezione e selezionare la soluzione corretta.";
//
TopObInBoolImpossibleSplit_ERR_4516.Request="Operatore booleano: spezzato da uno skin.";
TopObInBoolImpossibleSplit_ERR_4516.Diagnostic="Lo skin di taglio non � valido: � composto da domini che si intersecano.";
TopObInBoolImpossibleSplit_ERR_4516.Advice="Eseguire diversi tagli oppure risolvere lo skin con l'operatore Relimitazione.";
//
TopObInBoolImpossibleMultiTrim_ERR_4517.Request="Operatore booleano: Relimitazione";
TopObInBoolImpossibleMultiTrim_ERR_4517.Diagnostic="Body di immissione non validi";
TopObInBoolImpossibleMultiTrim_ERR_4517.Advice="I body di immissione devono contenere un solo dominio.";
//
TopObInBoolImpossibleMultiTrim_ERR_4518.Request="Operatore booleano: Relimitazione";
TopObInBoolImpossibleMultiTrim_ERR_4518.Diagnostic="Selezione non valida: l'elemento (numero di immissione /p1) � stato tagliato in /p2 parti; la parte /p3 non pu� essere conservata.";
TopObInBoolImpossibleMultiTrim_ERR_4518.Advice="Sostituire l'immissione.";
//
TopObInBoolPositionComputation_ERR_4519.Request="Operatore booleano: impossibile ricomporre il volume";
TopObInBoolPositionComputation_ERR_4519.Diagnostic="Uno body di immissione pu� essere composto da un volume sottile non desiderato. Non � possibile decidere se � necessario conservarlo.";
TopObInBoolPositionComputation_ERR_4519.Advice="Verificare la composizione dei body con la feature GSD/Separa e utilizzare la feature Rimuovi imbozzatura per conservare solo i domini utili. Quindi, eseguire nuovamente l'operazione.";
//
TopObInSmartProjEqualSolution_ERR_4520.Request="Operatore di proiezione smart";
TopObInSmartProjEqualSolution_ERR_4520.Diagnostic="Due soluzioni presentano la stessa distanza massima";
TopObInSmartProjEqualSolution_ERR_4520.Advice="Utilizzare la proiezione e selezionare il risultato ottimale.";
//
TopObInBoolLayDownBadOrientation_ERR_4521.Request="Operatore Supporto";
TopObInBoolLayDownBadOrientation_ERR_4521.Diagnostic="Definizione di pi� figure connesse con orientamento non corretto";
TopObInBoolLayDownBadOrientation_ERR_4521.Advice="Creare un body per ogni figura, posizionarli e unire i risultati.";
//
TopObInSmartProjIncompleteSolution_ERR_4522.Request="Operatore di proiezione smart.";
TopObInSmartProjIncompleteSolution_ERR_4522.Diagnostic="La figura iniziale non pu� essere proiettata completamente sullo svuotamento.";
TopObInSmartProjIncompleteSolution_ERR_4522.Advice="Modificare l'opzione di proiezione smart per consentire una soluzione parziale o suddividere la figura iniziale."; // @validatedUse CRE 10:11:13 option
//
TopObInBoolFullResult_ERR_4523.Request="Operatore compatibile";
TopObInBoolFullResult_ERR_4523.Diagnostic="Il risultato contiene spigoli completi e/o vertici completi";
TopObInBoolFullResult_ERR_4523.Advice="Rimuovere gli elementi completi dai body di immissione.";
//
TopObInBoolFullResult_ERR_4524.Request="Operatore compatibile";
TopObInBoolFullResult_ERR_4524.Diagnostic="I body di immissione sono uguali";
TopObInBoolFullResult_ERR_4524.Advice="Selezionare body differenti.";
//
TopObInProjNoOrientedResult_ERR_4525.Request="Operatore di proiezione";
TopObInProjNoOrientedResult_ERR_4525.Diagnostic="Il risultato della proiezione non pu� essere orientato";
TopObInProjNoOrientedResult_ERR_4525.Advice="Modificare la selezione immessa.";
//
TopObInTopoInaccurateContact_ERR_4526.Request="Operatore topologico: contatto impreciso.";
TopObInTopoInaccurateContact_ERR_4526.Diagnostic="Il confronto tra gli elementi non pu� essere risolto.";
TopObInTopoInaccurateContact_ERR_4526.Advice="Controllare la validit� del body o modificare la qualit� dei contatti.";
//
TopObIntCellNotInBody_ERR_4600.Request="/p1: la cella non appartiene al body.";
TopObIntCellNotInBody_ERR_4600.Diagnostic="La cella /p1 non appartiene al body /p2. L'operazione richiesta non pu� essere eseguita.";
TopObIntCellNotInBody_ERR_4600.Advice="Modificare la selezione.";
//
TopObIntCATGeoClean_ERR_4601.Request="La verifica geometrica rileva anomalie.";
TopObIntCATGeoClean_ERR_4601.Diagnostic="/p1 rilevato su /p2.";
TopObIntCATGeoClean_ERR_4601.Advice="Ripulire il documento di immissione e correggere l'operatore.";
//
TopObIntCATGeoClean_ERR_4602.Request="Verifica geometrica interrotta.";
TopObIntCATGeoClean_ERR_4602.Diagnostic="Errore inatteso durante la verifica.";
TopObIntCATGeoClean_ERR_4602.Advice="Rivolgersi al supporto locale.";
//
TopObIntImpossibleSituation_ERR_4603.Request="Dati locali invalidi per l'algoritmo.";
TopObIntImpossibleSituation_ERR_4603.Diagnostic="Le operazioni producono situazioni impossibili.";
TopObIntImpossibleSituation_ERR_4603.Advice="Modificare la selezione.";
//
TopObInBoolWarningStabilization_ERR_4700.Request="Impossibile stabilizzare l'orientamento di alcune curve risultanti.";
TopObInBoolWarningStabilization_ERR_4700.Diagnostic="L'orientamento della curva rossa non pu� essere stabile perch� � prodotto da diverse curve di immissione o � il risultato di una proiezione multipla di una singola curva di immissione (le curve di immissione sono evidenziate in blu).";
TopObInBoolWarningStabilization_ERR_4700.Advice="Utilizzare una direzione di proiezione oppure estrarre solo le facce utili dal riferimento.";
//
TopObInBoolWarningBoundaryHealing_ERR_4701.Request="Alcune curve non valide creano un incrocio automatico del profilo.";
TopObInBoolWarningBoundaryHealing_ERR_4701.Diagnostic="Le curve evidenziate non sono valide. Il profilo non pu� essere corretto.";
TopObInBoolWarningBoundaryHealing_ERR_4701.Advice="Creare di nuovo le curve.";
//
TopObInBoolWarningRegularizator_ERR_4702.Request="Alcune facce selezionate si incrociano tra loro. Regolarizzazione impossibile.";
TopObInBoolWarningRegularizator_ERR_4702.Diagnostic="Le facce evidenziate non sono tangenti. Impossibile regolarizzare il body.";
TopObInBoolWarningRegularizator_ERR_4702.Advice="Riconsiderare il disegno del body.";
//
TopObInBoolWarningInaccurateContact_ERR_4703.Request="Un contatto non preciso tra i body produce un risultato non connesso.";
TopObInBoolWarningInaccurateContact_ERR_4703.Diagnostic="Le facce evidenziate sono tangenti ma non unite: il risultato � composto da diversi domini.";
TopObInBoolWarningInaccurateContact_ERR_4703.Advice="Riconsiderare il disegno del body o utilizzare la feature Rimuovi imbozzatura per eliminare alcuni domini.";
//
TopObInBoolWarningNonManifoldEdges_ERR_4704.Request="Sono state create delle configurazioni di tangente durante l'operazione booleana.";
TopObInBoolWarningNonManifoldEdges_ERR_4704.Diagnostic="Le configurazioni di tangente potrebbero rivelare contatti non precisi tra i body.";
TopObInBoolWarningNonManifoldEdges_ERR_4704.Advice="Controllare il disegno del body.";
//
TopObInBoolWarningClosedWirePositionning_ERR_4705.Request="Il posizionamento delle figure chiuse � ambiguo.";
TopObInBoolWarningClosedWirePositionning_ERR_4705.Diagnostic="Il risultato dipende dalla posizione del punto di chiusura evidenziato rispetto alla posizione dell'intersezione. La selezione pu� cambiare durante l'aggiornamento.";
TopObInBoolWarningClosedWirePositionning_ERR_4705.Advice="Utilizzare l'opzione Conserva/Rimuovi per ottenere un risultato stabile."; // @validatedUse CRE 10:11:13 option
//
TopObInBoolWarningGapInMissing_ERR_4706.Request="� stata creata una distanza.";
TopObInBoolWarningGapInMissing_ERR_4706.Diagnostic="Lo spigolo presenta una distanza interna di /p1 mm.";
TopObInBoolWarningGapInMissing_ERR_4706.Advice="Il contatto tra i due body non � abbastanza preciso.";
//
TopObInBoolWarningKeepAndDelete_ERR_4707.Request="Il volume viene conservato e rimosso.";
TopObInBoolWarningKeepAndDelete_ERR_4707.Diagnostic="Una faccia conservata e una faccia rimossa sono contenute nello stesso volume.";
TopObInBoolWarningKeepAndDelete_ERR_4707.Advice="La faccia non verr� conservata come le altre facce del volume.";
//
TopObInBoolWarningToleranceValue_ERR_4708.Request="Valore di tolleranza oltre 0.1";
TopObInBoolWarningToleranceValue_ERR_4708.Diagnostic="I valori superiori a 0.1 possono essere incompatibili con le prove e gli operatori seguenti.";
TopObInBoolWarningToleranceValue_ERR_4708.Advice="Utilizzare un valore di tolleranza inferiore a 0.1";
//
TopObInBoolWarningIndexingOfSolutions_ERR_4709.Request="L'indicizzazione delle shell risultanti � ambigua.";
TopObInBoolWarningIndexingOfSolutions_ERR_4709.Diagnostic="La presenza di una figura chiusa come immissione rende instabile l'indicizzazione del risultato.";
TopObInBoolWarningIndexingOfSolutions_ERR_4709.Advice="Dividere in due la figura.";
//
TopObInBoolConfusedWiresInSplitShellByWires_ERR_4710.Request="Due figure immesse sono confuse.";
TopObInBoolConfusedWiresInSplitShellByWires_ERR_4710.Diagnostic="Impossibile eseguire il calcolo dell'orientamento se due figure di immissione sono confuse.";
TopObInBoolConfusedWiresInSplitShellByWires_ERR_4710.Advice="Specificare figure non confuse come immissione.";
//
TopObInvalidCurve_ERR_4711.Request="Curva di immissione non valida per operatore topologico.";
TopObInvalidCurve_ERR_4711.Diagnostic="Gli spigoli evidenziati sono definiti in curve non valide.";
TopObInvalidCurve_ERR_4711.Advice="Modificare l'immissione per rimuovere l'invalidit�.";
//
TopObSimplifyTSInputToleranceIsNotSupported_ERR_4800.Request="Tolleranza non supportata.";
TopObSimplifyTSInputToleranceIsNotSupported_ERR_4800.Diagnostic="La tolleranza di immissione non � compresa nell'intervallo di tolleranza supportato.";
TopObSimplifyTSInputToleranceIsNotSupported_ERR_4800.Advice="Regola la tolleranza.";
