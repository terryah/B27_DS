//=============================================================================
//                                     CNEXT - CXR2
//                          COPYRIGHT DASSAULT SYSTEMES 1999 
//-----------------------------------------------------------------------------
// FILENAME    :    CATDrwCoordDimTableCmd
// LOCATION    :    DraftingIntCommands/CNext/resources/msgcatalog
// AUTHOR      :    HBH
// DATE        :    Octobre 2001
//------------------------------------------------------------------------------
// DESCRIPTION :    Resources File for NLS purpose related to Interactive
//                  Drafting Commands.
//                  
//------------------------------------------------------------------------------
// COMMENTS    :
//------------------------------------------------------------------------------
//MODIFICATION     user  date          purpose
//   HISTORY       
//    01		lgk	09.10.2002 Revision
//------------------------------------------------------------------------------

Title="Sistema di assi e Parametri della tabella";

_axisSystemComboFrame._axisSystemLabel.Title="Sistema di assi: ";

_axisSystemFrame._xLabel.Title="X: ";
_axisSystemFrame._yLabel.Title="Y: ";
_axisSystemFrame._zLabel.Title="Z: ";
_axisSystemFrame._xSpinner.LongHelp="Coordinata X dell'origine del sistema di assi.";
_axisSystemFrame._ySpinner.LongHelp="Coordinata Y dell'origine del sistema di assi.";
_axisSystemFrame._zSpinner.LongHelp="Coordinata Z dell'origine del sistema di assi.";

_axisSystemFrame._angleFrame._angleLabel.Title="Angolo: ";
_axisSystemFrame._angleFrame._angleLabel.LongHelp="Definisce l'angolo di rotazione del sistema di assi 2D.";

_axisSystemFrame._flipFrame._flipLabel.Title="Ribalta: ";
_axisSystemFrame._flipFrame._flipHorizontalPush.ShortHelp="Ribalta in orizzontale";
_axisSystemFrame._flipFrame._flipHorizontalPush.LongHelp="Ribalta in orizzontale il sistema di assi 2D.";
_axisSystemFrame._flipFrame._flipVerticalPush.ShortHelp="Ribalta in verticale";
_axisSystemFrame._flipFrame._flipVerticalPush.LongHelp="Ribalta in verticale il sistema di assi 2D.";

_axisSystemFrame._axisCreationCheck.Title="Crea rappresentazione";
_axisSystemFrame._axisCreationCheck.LongHelp="Crea una rappresentazione del sistema di assi 2D.";

_tableContentTitle.Title="Contenuto della tabella";

_tableTitleFrame._tableTitleLabel.Title="Titolo: ";
_tableTitleFrame._tableTitleEditor.LongHelp="Titolo della tabella";

_tableFormatTitle.Title="Formato della tabella";
_tableFormatFrame._InvertCheckButton.Title="Trasponi tabella";
_tableFormatFrame._InvertCheckButton.LongHelp="Inverte le linee e le colonne della tabella";
_tableFormatFrame._sortFrame._sortCheck.Title="Ordina il contenuto della tabella";
_tableFormatFrame._sortFrame._sortCheck.LongHelp="Ordina le colonne della tabella in base ai criteri specificati.";
_tableFormatFrame._sortFrame._sortPush.Title="...";
_tableFormatFrame._sortFrame._sortPush.LongHelp="Ordina le colonne della tabella in base ai criteri specificati.";

_tableFormatFrame._splitFrame._splitCheck.Title="Taglia tabella";
_tableFormatFrame._splitFrame._splitCheck.LongHelp="Suddivide la la tabella in diverse tabelle.";
_tableFormatFrame._splitFrame._splitPush.Title="...";
_tableFormatFrame._splitFrame._splitPush.LongHelp="Suddivide la la tabella in diverse tabelle.";

ColumnsFrame.Title2.Title="Titoli";
ColumnsFrame.Column.Title="Colonne";

ReferenceComboNone="Nessuna";
ReferenceComboUpperCase="Etichetta: A, B, C, ...";
ReferenceComboLowerCase="Etichetta: a, b, c, ...";
ReferenceComboIndex="Indice   : 1, 2, 3, ...";

ColumnsFrame.X.Title="X";
ColumnsFrame.Y.Title="Y";
ColumnsFrame.Z.Title="Z";
REF="RIF.";
XTITLE="X";
YTITLE="Y";
ZTITLE="Z";
ColumnsFrame._ReferenceOption.Title="...";
ColumnsFrame._ReferenceOption.LongHelp="Indica la prima lettera o il primo
indice dei riferimenti.";
