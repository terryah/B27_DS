// Error Messages catalog, english version.
//----------------------------------------------------------------------------
//
//  Catalogue des erreurs NLS MechanicalModeler
//
//----------------------------------------------------------------------------

//--- Messages generaux erreurs NLS MecahnicalModeler -------------------

MfError="Si � verificato un errore in MechanicalModeler Framework.";

MfErrAssertFailed="Asserzione non riuscita.";

MfErrCCP="Operazione Incolla non possibile : /p1";

MfErrGeometryIsOutOfDesignRange="La geometria � al di fuori dell'intervallo del progetto";

MfErrGeometryIsOutOfBoundingBox="Geometria ESTERNA al riquadro";

MfErrSolidVolumeNotAllowedWithBoundingBox="Solido/volume non consentito con il riquadro";

MfErrImpossibleOperation="Questa operazione non � consentita.";

MfErrInternalError="Errore interno in Mechanical Modeler.";

MfErrJournalCheckError="Errore in Mechanical Modeler: alcuni elementi non possono essere selezionati.";

MfErrUnresolveBRep="Non � pi� possibile riconoscere una faccia, un margine o un vertice.";

MfErrInvalidBRepFeature="Feature BRep non valida.";

MfErrInvalidBRepSupportKO="Non � pi� possibile riconoscere una faccia, un margine o un vertice: La feature del contesto di risoluzione viene eliminata.";

MfErrInvalidBRepLabelKO="Non � pi� possibile riconoscere una faccia, un margine o un vertice.";

MfErrInvalidBRepObject="Oggetto selezionato non valido.";

MfErrInvalidElement="L'elemento non � valido";

MfErrInvalidFeature="La feature non � valida.";

MfErrInvalidName="La variabile non ha un nome valido.";

MfErrInvalidType="La variabile non ha un tipo valido.";

MfErrNotYetImplemented="Non ancora implementato.";

MfErrNullPointerAccess="Il puntatore � nullo.";

MfErrOutOfRange="La variabile non � compresa nell'intervallo.";

MfErrUpdate="Errore durante l'aggiornamento.";

MfErrBRepUpdate="Non � pi� possibile riconoscere una faccia, un margine o un vertice.";

MfErrUpdateFailed="Aggiornamento non riuscito.";

MfErrCstConfig="Geometria non congruente o con eccesso di vincoli.";

MfErrUpdateCycle="Feature coinvolta in un ciclo di aggiornamento.";

MfErrInOutCycle="Ciclo In-Out nella struttura della feature.";

MfErrSelfDescendingImport="Impossibile creare un riferimento esterno: ciclo individuato.";

MfErrSelfExternalizeImport="Impossibile creare un riferimento esterno riferito a se stesso.";

MfErrSynchroMissingReference="Impossibile sincronizzare: l'elemento, da cui viene creato il riferimento esterno /p1, � mancante.";

MfErrSynchroInconsistent="Impossibile sincronizzare: /p1 non � compatibile con il riferimento esterno /p2";

MfErrSynchroNonUpdatableReference="Impossibile sincronizzare: /p1, da cui viene creato il riferimento esterno /p2, non pu� essere aggiornato.";

MfErrImportCreation="Impossibile creare un riferimento esterno:\n/p1";


MfErrAssociativeImportCreation="Impossibile creare un riferimento associativo esterno.";

MfErrImportCATShape="Da CATShape: impossibile creare un riferimento esterno ad uno spigolo, ad una faccia o ad un vertice. Utilizzare una feature di Estrazione.";

MfErrUnpublishedImport="Impossibile creare un riferimento esterno dall'elemento non pubblicato.";

MfErrUnpublishedImportButCellPublished="La feature non � pubblica, lo � solo una sua cella.\n Modificare \"Selezione del filtro utente\" per imporre il \"Filtro dell'elemento geometrico\" o modificare la modalit� di pubblicazione.";

MfErrUnpublishedImportButFeaturePublished="L'elemento secondario non � pubblico, lo � solo la sua feature federata.\n Modificare \"Selezione del filtro utente\" per imporre il \"Filtro dell'elemento feature\" o modificare la modalit� di pubblicazione.";

MfErrUnsavedInDatabase="Impossibile creare un riferimento esterno: elemento non salvato nel database";

MfErrMultiContext="Impossibile creare un riferimento esterno: la selezione in /p3 non � consentita perch� /p2 � stato disegnato nel contesto /p1";

MfErrSeveralDimensions="Impossibile creare un riferimento esterno: estrarre gli elementi secondari di dimensioni differenti ";

MfErrImportPasteDRMRights="Operazione Importa o Incolla non consentita a causa della gestione dei diritti digitali sul documento";

MfErrDocumentNotFound="Documento non nella sessione";

MfErrSubProductMoved="Prodotto secondario spostato nell'assieme.";

MfErrReplaceLinkOnContextualImport="Impossibile sostituire il collegamento dall'importazione contestuale";

MfErrReplaceLinkOnDifferentDocument="Impossibile sostituire il collegamento con questo documento (� consentito solo Salva con nome)";

MfErrInvalidDestination="La destinazione non � valida.";

MfErrInvalidDIWO="La destinazione non pu� essere definita nell'oggetto di lavoro.";

MfErrCheckKO="Verifica di Mechanical Modeler non riuscita per questo elemento";

MfErrCheckJournalTopoKO="Verifica di Mechanical Modeler non riuscita: giornale di topologia non valido per questa feature.";

MfErrCheckScopeBodyKO="Verifica di Mechanical Modeler non riuscita: body di topologia non congruente con l'ambito di denominazione generica per questa feature.";

MfErrRelaunchUpdate="Riavviare la richiesta di aggiornamento.";

MfErrCutPaste="La funzione Copia e Incolla non � attiva in questo caso.\nPer i riferimenti diversi da punti, linee o piani, utilizzare la funzione di Copie a Incolla prima di cancellare gli oggetti copiati originariamente.\nPer il formato \"Come specificato...\",  se si rimane nello stesso documento � possibile utilizzare i comandi Modifica gruppo geometrico o Riordina.";

MfErrMissingRefPlane="Il CATPart dovrebbe contenere 3 piani di riferimento.";

MfErrDestPrivateTool="L'operazione Incolla non � consentita in uno strumento privato.";

MfErrDestShapeBody="L'operazione Incolla come specificato in questo gruppo lineare non � consentita per questo elemento.";

MfErrPasteLGSUnderGS="L'inserimento di un gruppo geometrico ordinato al di sotto di un gruppo geometrico non � consentito.";

MfErrPasteGSUnderLGS="L'inserimento di un gruppo geometrico al di sotto di un gruppo geometrico ordinato non � consentito.";

MfErrPasteGSUnderHybrid="L'inserimento di un gruppo geometrico al di sotto di un body non � consentito.";

MfErrPasteLGSUnderBody="L'inserimento di un gruppo geometrico ordinato al di sotto di un body meccanico non � consentito.";

MfErrPasteInExternalRef="L'inserimento in un body Riferimenti esterni non � consentito.";

MfErrPasteHybridUnderGS="L'inserimento di un body ordinato al di sotto di un gruppo geometrico non � consentito.";

MfErrPasteHybridUnderBody="L'inserimento di un body ordinato al di sotto di un body meccanico non � consentito.";

MfErrPasteBodyUnderBody="L'inserimento di un body meccanico al di sotto di un body meccanico non � consentito.";

MfErrPasteBodyUnderGS="L'inserimento di un body meccanico al di sotto di un gruppo geometrico non � consentito.";

MfErrPasteBodyUnderLGS="L'inserimento di un body meccanico al di sotto di un gruppo geometrico ordinato non � consentito.";

MfErrPasteBodyUnderHybrid="L'inserimento di un body meccanico al di sotto di un body ordinato non � consentito.";

MfErrPasteBody="L'inserimento di un body meccanico in questa posizione non � consentito.";

MfErrPasteMultiSelUnderBody="L'inserimento di un risultato multiplo al di sotto di un body meccanico non � consentito.";

MfErrPasteMultiSelUnderPart="L'inserimento di un risultato multiplo al di sotto di una parte non � consentito.";

MfErrExternalReference="Un elemento � una ricorrenza di una feature in un altro documento.";

MfErrExternalReference1="/p1 o uno dei suoi componenti \n � una ricorrenza di una feature in un altro documento.";

MfErrAxisSystemNoSelection="Nessuna selezione per l'origine o per un asse.\nModificare il sistema di assi o ripetere la selezione.";

MfErrFromSolidToShape="La feature copiata proviene da un body meccanico e non pu� essere incollata su un body di superficie.";

MfErrPasteSHMTool="Un body con feature Sheet Metal non pu� essere un body secondario.\nTutte le feature Sheet Metal devono essere nel PartBody.";

MfErrSelFutureGeometry="Impossibile selezionare l'oggetto perch� � posizionato dopo la feature \"/p1\".";

MfErrSelNotDrawnGeometry="Impossibile selezionare l'oggetto perch� � assorbito dalla feature \"/p1\".";

MfErrSelPotentialAbsorbedFeature="Impossibile selezionare l'oggetto perch� potrebbe essere assorbito dalla feature \"/p1\".";

MfErrSelAggreagatedFeature="Impossibile selezionare l'oggetto perch� � una feature contestuale.";

MfErrNonConsecutive="Le feature non sono consecutive o non appartengono al Body.";

MfErrParentBelowChild="/p1 � al di sotto del relativo figlio /p2.";

MfErrMultipleModifChildren="Esiste pi� di un figlio di modifica per /p1.";

MfErrReplaceSubElem="Sostituzione di /p1 mediante /p2 non riuscita.";

MfNoSuitableReplug="La nuova feature non pu� essere incollata nella posizione desiderata senza influire sulle regole di ordinamento.";

MfErrCCPConsecutive="Le feature selezionate durante la copia non sono consecutive.";

MfReplaceFailureAtCCP="Comando Incolla annullato: Sostituzione non riuscita per alcuni elementi.";

MfErrMultiSelV4V5="Non � possibile incollare da feature appartenenti a diversi modelli V4 nella stessa transazione.";

MfNoImportChildBeforeRef="Un'importazione interna non pu� essere incollata prima del suo riferimento senza influire sulle regole di ordinamento.";

MfNoImportChildAfterRefAbsorbed="Impossibile incollare l'importazione interna dopo \"/p1\", che assorbe il suo riferimento.\nCi� influir� sulle regole di ordinamento.";

MfErrDestOGS="L'operazione Incolla come specificato in un componente diverso da un gruppo geometrico non � consentita per questo elemento.";

MfErrCCPGenericNaming="Errore relativo alla copia per /p1";

MfErrInterPartCopyValidContext="InterPartCopy: /p1";

MfErrInterPartCopyOtherError="InterPartCopy: /p1";

MfErrPasteGeomUnderPart="Non � consentito incollare una feature geometrica nel Part.";

MfErrAggregation="\n    /p1 non pu� includere /p2.\n    /p2 o il relativo padre diretto infrangono le regole di inclusione.";

MfErrVolumeInHybridBody="Si sta tentando di incollare un volume direttamente in un body. L'operazione non � consentita.";

MfErrExternalLink="Questa azione potrebbe causare un errore: \nNon creare ricorrenze di una PowerCopy che contiene un collegamento esterno \nnel part che contiene il riferimento del collegamento esterno.";

MfErrShapeLink="I collegamenti contestuali non sono supportati in CATShape.\nPertanto, verr� applicato il formato AS RESULT. ";

//--- Messages NLS specifiques pour UpdateError ----------------------------


//--- Messages NLS specifiques pour UpdateWarning ----------------------------
MfWarningOrientationUnstable="L'orientamento delle celle non � stabile. ";
MfWarningOrientationUnstableMaster="Feature principale ";
//--- Messages NLS specifiques pour UpdateWarning FIN ----------------------------

//--- Messages NLS specifiques pour Warning a l'unstream du CATBRepModeContainer ----------------------------
MfWarningInvalidBRepModeCont="Contenitore BRepMode non valido: la parte deve essere salvata dopo qualsiasi modifica.";
//--- Messages NLS specifiques pour Warning a l'unstream du CATBRepModeContainer FIN ------------------------

// Messages generated in source MF0CST.m/src/CATFCallBackSolver.cpp
// case of overconstrained or not consistent geometry

StrError1="Configurazione sovravincolata o non congruente: alcuni vincoli vengono ignorati";

StrError2="Vincolo risolto ma non coinvolto nella configurazione sovravincolata o incongruente";

StrError3="Vincolo ignorato: il sistema di vincoli � incongruente o sovravincolato";

StrError4="Geometria parzialmente risolta";

StrError5="Loop tra geometria e vincoli";

//----------------------------------------------------------------------------

SickFeatures="Feature errata: ";
IndefiniteFeature="feature indefinita";
Comma=", ";
None="Nessuno";
Message=". Messaggio: ";

//--- Internal Import Edition -----------------------
MfErrInputObjectNull="Impossibile modificare il riferimento di una copia interna in un oggetto NULL. ";
MfErrCurrentInternalCopyNotSupported="Il riferimento di una determinata copia interna non pu� essere modificato. Solo le importazioni del solido interno possono essere modificate. ";
MfErrCurrentInternalCopyFrozen="Il risultato di una determinata copia interna � congelato. Il relativo riferimento non pu� essere modificato se non viene prima annullato il congelamento. ";
MfErrGenericEditSetLink="Impossibile impostare il collegamento su una determinata immissione.";
MfErrGenericEditNoMatchingInput="Nessuna immissione corrispondente trovata su una determinata specifica.";
MfErrGenericEditInputNull="Impossibile creare una specifica con un oggetto NULL.";
MfErrGenericEditAttrAccKey="Impossibile richiamare le chiavi e gli accessi di un determinato oggetto.";
MfErrGenericEditSpecCreation="Impossibile creare la specifica.";
MfErrGenericEditInputCreation="Impossibile creare l'elenco di immissioni per una determinata specifica.";
MfErrGenericEditObjectForLinkNull="Impossibile creare un collegamento con un oggetto NULL.";
MfErrGenericEditLinkCreation="Creazione del collegamento non riuscita.";




