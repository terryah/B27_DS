//=============================================================================
//                                     CNEXT - CXR2
//                          COPYRIGHT DASSAULT SYSTEMES 2014
//-----------------------------------------------------------------------------
// FILENAME    :    CATTPSStandardProperties
// AUTHOR      :    P. RENUSHE
// DATE        :    16/07/2014
//-----------------------------------------------------------------------------
// DESCRIPTION :    Resources File for NLS purpose CATTPSStandardProperties 
//-----------------------------------------------------------------------------
//StandardV1 refers to Annotation set created upto R209
//StandardV2 refers to Annotation set created since R210 but before R2015x
//StandardV3 refers to Annotation set created since R2015x
//-----------------------------------------------------------------------------

StandardV1.Code1.ASME="ASME Y14.5M";
StandardV1.Code2.ASME="ASME Y14.41M";
StandardV1.Code3.ASME="ANSI B4.2";
StandardV1.Code4.ASME="Y14.36M";

StandardV1.Year1.ASME="1994";
StandardV1.Year2.ASME="2003";
StandardV1.Year3.ASME="1978";
StandardV1.Year4.ASME="1996";

StandardV1.Title1.ASME="Quotatura e tolleranze";
StandardV1.Title2.ASME="Pratiche dati definizione prodotto digitale";
StandardV1.Title3.ASME="Limiti e adattamenti metrici preferiti";
StandardV1.Title4.ASME="Simboli trama superficie";

StandardV1.Code1.ISO="ISO 129-1";
StandardV1.Code2.ISO="ISO 286-1";
StandardV1.Code3.ISO="ISO 286-2";
StandardV1.Code4.ISO="ISO 406";
StandardV1.Code5.ISO="ISO 1101";
StandardV1.Code6.ISO="ISO 1660";
StandardV1.Code7.ISO="ISO 2692";
StandardV1.Code8.ISO="ISO 2692";
StandardV1.Code9.ISO="ISO 2768-1";
StandardV1.Code10.ISO="ISO 5458";
StandardV1.Code11.ISO="ISO 5459";
StandardV1.Code12.ISO="ISO 8015";
StandardV1.Code13.ISO="ISO 10578";
StandardV1.Code14.ISO="ISO 10579";
StandardV1.Code15.ISO="ISO/TR 16570";
StandardV1.Code16.ISO="ISO 16792";

StandardV1.Year1.ISO="2004";
StandardV1.Year2.ISO="1988";
StandardV1.Year3.ISO="1988";
StandardV1.Year4.ISO="1987";
StandardV1.Year5.ISO="1983";
StandardV1.Year6.ISO="1987";
StandardV1.Year7.ISO="1988";
StandardV1.Year8.ISO="1988/Amd1 1992";
StandardV1.Year9.ISO="1989";
StandardV1.Year10.ISO="1998";
StandardV1.Year11.ISO="1981";
StandardV1.Year12.ISO="1985";
StandardV1.Year13.ISO="1992";
StandardV1.Year14.ISO="1993";
StandardV1.Year15.ISO="2004";
StandardV1.Year16.ISO="2006";

StandardV1.Title1.ISO="Disegni tecnici -- Indicazione di quote e tolleranze -- Parte 1: Principi generali.";
StandardV1.Title2.ISO="Sistema ISO di limiti e adattamenti -- Parte 1: Basi di tolleranze, deviazioni e adattamenti.";
StandardV1.Title3.ISO="Sistema ISO di limiti e adattamenti -- Parte 2: Tabelle di livelli di tolleranza e deviazioni dai limiti standard per fori e alberi (compresi ISO 286-2/AC1:2006).";
StandardV1.Title4.ISO="Disegni tecnici -- Tolleranze per le quote lineari e angolari.";
StandardV1.Title5.ISO="Geometrical Product Specifications (GPS) -- Tolleranze geometriche -- Tolleranze per forma, orientamento, posizione e run-out.";
StandardV1.Title6.ISO="Disegni tecnici -- Quotatura e tolleranza dei profili.";
StandardV1.Title7.ISO="Disegni tecnici -- Tolleranze geometriche -- Principio del materiale massimo.";
StandardV1.Title8.ISO="Disegni tecnici. Tolleranze geometriche. Principio del materiale massimo - Emendamento 1: Requisito di materiale minimo.";
StandardV1.Title9.ISO="Tolleranze generali -- Parte 1: Tolleranze per dimensioni lineari e angolari senza indicazioni per le singole tolleranze.";
StandardV1.Title10.ISO="Geometrical Product Specifications (GPS) -- Tolleranze geometriche -- Tolleranza posizionale.";
StandardV1.Title11.ISO="Disegni tecnici -- Tolleranze geometriche -- Riferimenti e sistemi di riferimenti per le tolleranze geometriche.";
StandardV1.Title12.ISO="Disegni tecnici -- Principi fondamentali della tolleranza.";
StandardV1.Title13.ISO="Disegni tecnici -- Tolleranza di orientamento e posizione -- Area di tolleranza proiettata.";
StandardV1.Title14.ISO="Disegni tecnici -- Quotatura e tolleranze -- Parti non rigide.";
StandardV1.Title15.ISO="Geometrical Product Specifications (GPS) -- Quotatura e tolleranze lineari e angolari: Specifiche dei limiti +/- -- Quote passo, distanze, quote angolari e raggi.";
StandardV1.Title16.ISO="Documentazione tecnica del prodotto -- Pratiche dati definizione prodotto digitale.";

StandardV2.Code1.ASME="ASME Y14.5";
StandardV2.Code2.ASME="ASME Y14.41M";
StandardV2.Code3.ASME="ANSI B4.2";
StandardV2.Code4.ASME="Y14.36M";

StandardV2.Year1.ASME="2009";
StandardV2.Year2.ASME="2003";
StandardV2.Year3.ASME="1978";
StandardV2.Year4.ASME="1996";

StandardV2.Title1.ASME="Quotatura e tolleranze";
StandardV2.Title2.ASME="Pratiche dati definizione prodotto digitale";
StandardV2.Title3.ASME="Limiti e adattamenti metrici preferiti";
StandardV2.Title4.ASME="Simboli trama superficie";

StandardV2.Code1.ISO="ISO 129-1";
StandardV2.Code2.ISO="ISO 286-1";
StandardV2.Code3.ISO="ISO 286-2";
StandardV2.Code4.ISO="ISO 406";
StandardV2.Code5.ISO="ISO 1101";
StandardV2.Code6.ISO="ISO 1660";
StandardV2.Code7.ISO="ISO 2692";
StandardV2.Code8.ISO="ISO 2768-1";
StandardV2.Code9.ISO="ISO 5458";
StandardV2.Code10.ISO="ISO 5459";
StandardV2.Code11.ISO="ISO 8015";
StandardV2.Code12.ISO="ISO 10578";
StandardV2.Code13.ISO="ISO 10579";
StandardV2.Code14.ISO="ISO 16792";

StandardV2.Year1.ISO="2004";
StandardV2.Year2.ISO="1988";
StandardV2.Year3.ISO="1988";
StandardV2.Year4.ISO="1987";
StandardV2.Year5.ISO="2004";
StandardV2.Year6.ISO="1987";
StandardV2.Year7.ISO="2006";
StandardV2.Year8.ISO="1989";
StandardV2.Year9.ISO="1998";
StandardV2.Year10.ISO="1981";
StandardV2.Year11.ISO="1985";
StandardV2.Year12.ISO="1992";
StandardV2.Year13.ISO="1993";
StandardV2.Year14.ISO="2006";

StandardV2.Title1.ISO="Disegni tecnici -- Indicazione di quote e tolleranze -- Parte 1: Principi generali.";
StandardV2.Title2.ISO="Sistema ISO di limiti e adattamenti -- Parte 1: Basi di tolleranze, deviazioni e adattamenti.";
StandardV2.Title3.ISO="Sistema ISO di limiti e adattamenti -- Parte 2: Tabelle di livelli di tolleranza e deviazioni dai limiti standard per fori e alberi (compresi ISO 286-2/AC1:2006).";
StandardV2.Title4.ISO="Disegni tecnici -- Tolleranze per le quote lineari e angolari.";
StandardV2.Title5.ISO="Geometrical Product Specifications (GPS) -- Tolleranze geometriche -- Tolleranze per forma, orientamento, posizione e run-out.";
StandardV2.Title6.ISO="Disegni tecnici -- Quotatura e tolleranza dei profili.";
StandardV2.Title7.ISO="Geometrical product specifications (GPS) - Tolleranze geometriche - Requisito materiale massimo (MMR), requisito materiale minimo (LMR) e requisito di reciprocitÓ (RPR).";
StandardV2.Title8.ISO="Tolleranze generali -- Parte 1: Tolleranze per dimensioni lineari e angolari senza indicazioni per le singole tolleranze.";
StandardV2.Title9.ISO="Geometrical Product Specifications (GPS) -- Tolleranze geometriche -- Tolleranza posizionale.";
StandardV2.Title10.ISO="Disegni tecnici -- Tolleranze geometriche -- Riferimenti e sistemi di riferimenti per le tolleranze geometriche.";
StandardV2.Title11.ISO="Disegni tecnici -- Principi fondamentali della tolleranza.";
StandardV2.Title12.ISO="Disegni tecnici -- Tolleranza di orientamento e posizione -- Area di tolleranza proiettata.";
StandardV2.Title13.ISO="Disegni tecnici -- Quotatura e tolleranze -- Parti non rigide.";
StandardV2.Title14.ISO="Documentazione tecnica del prodotto -- Pratiche dati definizione prodotto digitale.";

StandardV3.Code1.ISO="ISO 129-1";
StandardV3.Code2.ISO="ISO 286-1";
StandardV3.Code3.ISO="ISO 286-2";
StandardV3.Code4.ISO="ISO 1101";
StandardV3.Code5.ISO="ISO 1660";
StandardV3.Code6.ISO="ISO 2692";
StandardV3.Code7.ISO="ISO 2768-1";
StandardV3.Code8.ISO="ISO 5458";
StandardV3.Code9.ISO="ISO 5459";
StandardV3.Code10.ISO="ISO 8015";
StandardV3.Code11.ISO="ISO 10578";
StandardV3.Code12.ISO="ISO 10579";
StandardV3.Code13.ISO="ISO 14405-1";
StandardV3.Code14.ISO="ISO 14405-2";
StandardV3.Code15.ISO="ISO 16792";

StandardV3.Year1.ISO="2004";
StandardV3.Year2.ISO="2010";
StandardV3.Year3.ISO="2010";
StandardV3.Year4.ISO="2004";
StandardV3.Year5.ISO="1987";
StandardV3.Year6.ISO="2006";
StandardV3.Year7.ISO="1989";
StandardV3.Year8.ISO="1998";
StandardV3.Year9.ISO="2011";
StandardV3.Year10.ISO="2011";
StandardV3.Year11.ISO="1992";
StandardV3.Year12.ISO="2010";
StandardV3.Year13.ISO="2010";
StandardV3.Year14.ISO="2011";
StandardV3.Year15.ISO="2006";

StandardV3.Title1.ISO="Disegni tecnici -- Indicazione di quote e tolleranze -- Parte 1: Principi generali.";
StandardV3.Title2.ISO="Geometrical product specifications (GPS) -- Sistema di codici ISO per le tolleranze su quote lineari -- Parte 1: Base di tolleranze, deviazioni e adattamenti.";
StandardV3.Title3.ISO="Geometrical product specifications (GPS) -- Sistema di codici ISO per le tolleranze su quote lineari -- Parte 2: Tabella delle classi di tolleranza e delle deviazioni limite per fori e alberi.";
StandardV3.Title4.ISO="Geometrical Product Specifications (GPS) -- Tolleranze geometriche -- Tolleranze per forma, orientamento, posizione e run-out.";
StandardV3.Title5.ISO="Disegni tecnici -- Quotatura e tolleranza dei profili.";
StandardV3.Title6.ISO="Geometrical product specifications (GPS) - Tolleranze geometriche - Requisito materiale massimo (MMR), requisito materiale minimo (LMR) e requisito di reciprocitÓ (RPR).";
StandardV3.Title7.ISO="Tolleranze generali -- Parte 1: Tolleranze per dimensioni lineari e angolari senza indicazioni per le singole tolleranze.";
StandardV3.Title8.ISO="Geometrical Product Specifications (GPS) -- Tolleranze geometriche -- Tolleranza posizionale. ";
StandardV3.Title9.ISO="Geometrical Product Specifications (GPS) -- Tolleranze geometriche -- Riferimenti e sistemi di riferimenti.";
StandardV3.Title10.ISO="Geometric product specifications (GPS) - Principi fondamentali - Concetti, principi e regole.";
StandardV3.Title11.ISO="Disegni tecnici -- Tolleranza di orientamento e posizione -- Area di tolleranza proiettata.";
StandardV3.Title12.ISO="Geometrical product specifications (GPS) -- Quotatura e tolleranze -- Parti non rigide.";
StandardV3.Title13.ISO="Geometrical product specifications (GPS) - Tolleranze dimensionali - Parte 1: Dimensioni lineari.";
StandardV3.Title14.ISO="Geometrical product specifications (GPS) - Tolleranze dimensionali - Parte 2: Quote diverse dalle dimensioni lineari.";
StandardV3.Title15.ISO="Documentazione tecnica del prodotto -- Pratiche dati definizione prodotto digitale.";

StandardV4.Code1.ISO="ISO 129-1";
StandardV4.Code2.ISO="ISO 286-1";
StandardV4.Code3.ISO="ISO 286-2";
StandardV4.Code4.ISO="ISO 1101";
StandardV4.Code5.ISO="ISO 1660";
StandardV4.Code6.ISO="ISO 2692";
StandardV4.Code7.ISO="ISO 2768-1";
StandardV4.Code8.ISO="ISO 5458";
StandardV4.Code9.ISO="ISO 5459";
StandardV4.Code10.ISO="ISO 8015";
StandardV4.Code11.ISO="ISO 10579";
StandardV4.Code12.ISO="ISO 14405-1";
StandardV4.Code13.ISO="ISO 14405-2";
StandardV4.Code14.ISO="ISO 16792";

StandardV4.Year1.ISO="2004";
StandardV4.Year2.ISO="2010";
StandardV4.Year3.ISO="2010";
StandardV4.Year4.ISO="2012";
StandardV4.Year5.ISO="1987";
StandardV4.Year6.ISO="2014";
StandardV4.Year7.ISO="1989";
StandardV4.Year8.ISO="1998";
StandardV4.Year9.ISO="2011";
StandardV4.Year10.ISO="2011";
StandardV4.Year11.ISO="2010";
StandardV4.Year12.ISO="2010";
StandardV4.Year13.ISO="2011";
StandardV4.Year14.ISO="2006";

StandardV4.Title1.ISO="Disegni tecnici -- Indicazione di quote e tolleranze -- Parte 1: Principi generali.";
StandardV4.Title2.ISO="Geometrical product specifications (GPS) -- Sistema di codici ISO per le tolleranze su quote lineari -- Parte 1: Base di tolleranze, deviazioni e adattamenti.";
StandardV4.Title3.ISO="Geometrical product specifications (GPS) -- Sistema di codici ISO per le tolleranze su quote lineari -- Parte 2: Tabella delle classi di tolleranza e delle deviazioni limite per fori e alberi.";
StandardV4.Title4.ISO="Geometrical Product Specifications (GPS) -- Tolleranze geometriche -- Tolleranze per forma, orientamento, posizione e run-out.";
StandardV4.Title5.ISO="Disegni tecnici -- Quotatura e tolleranza dei profili.";
StandardV4.Title6.ISO="Geometrical product specifications (GPS) - Tolleranze geometriche - Requisito materiale massimo (MMR), requisito materiale minimo (LMR) e requisito di reciprocitÓ (RPR).";
StandardV4.Title7.ISO="Tolleranze generali -- Parte 1: Tolleranze per dimensioni lineari e angolari senza indicazioni per le singole tolleranze.";
StandardV4.Title8.ISO="Geometrical Product Specifications (GPS) -- Tolleranze geometriche -- Tolleranza posizionale. ";
StandardV4.Title9.ISO="Geometrical Product Specifications (GPS) -- Tolleranze geometriche -- Riferimenti e sistemi di riferimenti.";
StandardV4.Title10.ISO="Geometric product specifications (GPS) - Principi fondamentali - Concetti, principi e regole.";
StandardV4.Title11.ISO="Geometrical product specifications (GPS) -- Quotatura e tolleranze -- Parti non rigide.";
StandardV4.Title12.ISO="Geometrical product specifications (GPS) - Tolleranze dimensionali - Parte 1: Dimensioni lineari.";
StandardV4.Title13.ISO="Geometrical product specifications (GPS) - Tolleranze dimensionali - Parte 2: Quote diverse dalle dimensioni lineari.";
StandardV4.Title14.ISO="Documentazione tecnica del prodotto -- Pratiche dati definizione prodotto digitale.";

