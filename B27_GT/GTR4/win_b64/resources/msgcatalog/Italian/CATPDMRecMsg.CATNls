// COPYRIGHT DASSAULT SYSTEMES 2004-2010
// Error messages : ERR_, WRN_ or INF_ + identifier, /p1, /p2.. for parameters.

WRN_RecVpmNoConnection="Nessuna connessione a ENOVIA V5 VPM.\nEseguire la connessione manuale a ENOVIA V5 VPM prima di eseguire nuovamente l'interrogazione.";
WRN_RecVpmNoConnection.Generic="Nessuna connessione a ENOVIA V5 VPM.";
WRN_RecVpmNoConnection.Suggestion="Eseguire la connessione manuale a ENOVIA V5 VPM prima di eseguire nuovamente l'interrogazione.";


WRN_RecPDMInvalidRule="Regola non valida per l'oggetto.\nLa regola di sincronizzazione /p1 selezionata per l'oggetto non � compatibile con il tipo /p2";
WRN_RecPDMInvalidRule.Generic="Regola non valida per questo oggetto.\nLa regola di sincronizzazione selezionata per l'oggetto non � compatibile con il relativo tipo";
WRN_RecPDMInvalidRule.Detail="Regola di sincronizzazione: /p1 e tipo di oggetto: /p2";
WRN_RecPDMInvalidRule.Suggestion="Selezionare la regola di sincronizzazione appropriata";


WRN_RecVpmCharacDBAccessKO="Accesso nel database alle caratteristiche non eseguito. \nNessun confronto tra le caratteristiche.";
WRN_RecVpmCharacDBAccessKO.Generic="Accesso nel database alle caratteristiche non eseguito. \nNessun confronto tra le caratteristiche.";


WRN_RecVpmCharacCV5AccessKO="Accesso in sessione alle caratteristiche non eseguito. \nNessun confronto tra le caratteristiche.";
WRN_RecVpmCharacCV5AccessKO.Generic="Accesso in sessione alle caratteristiche non eseguito. \nNessun confronto tra le caratteristiche.";


WRN_RecPDMReloadDiffUuids="Non � consentito ricaricare un documento diverso.\nTale operazione � autorizzata solo se il documento nella sessione \nviene sostituito da un documento PDM della stessa origine (UUID interni identici).";
WRN_RecPDMReloadDiffUuids.Generic="Non � consentito ricaricare un documento diverso.\nTale operazione � autorizzata solo se il documento nella sessione \nviene sostituito da un documento PDM della stessa origine (UUID interni identici).";


WRN_RecPDMReloadForceDiffUuids="L'opzione che forza la possibilit� di ricaricare i documenti incongruenti (UUID interni diversi) � attivata nel lavoro CATIA corrente. \nIn questo momento, il documento /p1 \ncon un UUID interno diverso dal precedente nella sessione � stato caricato da PDM. \nDi conseguenza, alcuni collegamenti che includono il documento ricaricato sono andati perduti. \nRicrearli e rivedere il contenuto della sessione prima salvare i dati in PDM. ";
WRN_RecPDMReloadForceDiffUuids.Generic="L'opzione che forza la possibilit� di ricaricare i documenti incongruenti (UUID interni diversi) � attivata nel lavoro di CATIA corrente. \nAl momento, il documento specificato\n con un UUID interno diverso dal precedente nella sessione � stato caricato da PDM. \nPertanto, alcuni collegamenti che includono il documento ricaricato sono andati perduti. \nRicrearli ed esaminare il contenuto della sessione prima salvare i dati in PDM. ";
WRN_RecPDMReloadForceDiffUuids.Detail="/p1";
WRN_RecPDMReloadForceDiffUuids.Suggestion="Ricreare i collegamenti ed esaminare il contenuto della sessione prima salvare i dati in PDM.";


WRN_RecPDMReloadDocEditorToClose="Il nuovo caricamento non ha rimosso dalla sessione i documenti sostituiti. \nTali documenti con prefisso RecPrepareReloadDocToClose_ in File-Desk sono mantenuti aperti dagli editor e vanno chiusi manualmente.\nEseguire tale operazione prima di salvare la sessione in ENOVIA V5 VPM. ";
WRN_RecPDMReloadDocEditorToClose.Generic="Il nuovo caricamento non ha rimosso dalla sessione i documenti sostituiti. \nTali documenti con prefisso RecPrepareReloadDocToClose_ in File-Desk sono mantenuti aperti dagli editor e vanno chiusi manualmente.\nEseguire tale operazione prima di salvare la sessione in ENOVIA V5 VPM. ";
WRN_RecPDMReloadDocEditorToClose.Suggestion="Chiudere manualmente i documenti con prefisso RecPrepareReloadDocToClose_ in File-Desk prima di salvare la sessione in ENOVIA V5 VPM";

WRN_RecVpmInternalError="Errore interno.\n";
WRN_RecVpmInternalError.Generic="Errore interno.\n";


WRN_RecVpmSimpleQueryInvalid="Tali criteri non sono stati eseguiti per questo tipo di entit�.";
WRN_RecVpmSimpleQueryInvalid.Generic="Tali criteri non sono stati eseguiti per questo tipo di entit�.";


WRN_RecVpmMemoryAllocation="Assegnazione di memoria non eseguita correttamente.";
WRN_RecVpmMemoryAllocation.Generic="Assegnazione di memoria non eseguita correttamente.";


WRN_RecVpmNotDocumentSupported="� stato selezionato un documento non supportato dalla sincronizzazione (/p1).";
WRN_RecVpmNotDocumentSupported.Generic="� stato selezionato un documento non supportato dalla sincronizzazione.";
WRN_RecVpmNotDocumentSupported.Detail="Nome del documento: /p1";

WRN_RecVpmCriteriaNotEnoughArg="Il criterio selezionato richiede /p1 argomenti.";
WRN_RecVpmCriteriaNotEnoughArg.Generic="Il criterio selezionato richiede un numero specifico di argomenti.";
WRN_RecVpmCriteriaNotEnoughArg.Detail="Numero di argomenti: /p1";


WRN_RecVpmCommandAborted="Accesso al sistema VPDM non riuscito (comando /p1 terminato in modo anomalo).\nVerificare che l'applicazione VPDM o il server siano ancora attivi.";
WRN_RecVpmCommandAborted.Generic="Accesso al sistema VPDM non riuscito (il comando dato � stato interrotto). ";
WRN_RecVpmCommandAborted.Detail=" * Nome del comando: /p1";
WRN_RecVpmCommandAborted.Suggestion="Verificare che l'applicazione VPDM o il server sia ancora in esecuzione. ";


WRN_RecVpmDecodeXML="Errore interno.\nImpossibile decodificare il flusso di dati XML per la richiesta VPM /p1.";
WRN_RecVpmDecodeXML.Generic="Errore interno.\nImpossibile decodificare il flusso XML per la richiesta VPM.";
WRN_RecVpmDecodeXML.Detail="/p1";

WRN_RecErrorVPMExpand="Impossibile accedere alla definizione di struttura di prodotto in ENOVIA V5 VPM:\n   /p1";
WRN_RecErrorVPMExpand.Generic="Impossibile accedere alla definizione di struttura di prodotto in ENOVIA V5 VPM:\n ";
WRN_RecErrorVPMExpand.Detail="/p1";

WRN_RecNoRootAsmDefined="L'assieme principale non � interamente definito.\nErrore interno\n";
WRN_RecNoRootAsmDefined.Generic="L'assieme principale non � interamente definito.\nErrore interno\n";


WRN_RecVPDMCompareFailed="Il confronto dell'assieme tra la vista CATIA e la vista VPDM non � stato completato per /p1\n";
WRN_RecVPDMCompareFailed.Generic="Il confronto dell'assieme tra la vista di CATIA e la vista di VPDM non � stato eseguito correttamente per l'oggetto dato\n";
WRN_RecVPDMCompareFailed.Detail="Nome dell'oggetto: /p1";

WRN_RecRelationNotFound="Vista struttura assieme VPDM incongruente per la parte /p1\n";
WRN_RecRelationNotFound.Generic="Vista della struttura dell'assieme VPDM incongruente per la parte specificata\n";
WRN_RecRelationNotFound.Detail="/p1";

WRN_RecVpmDupDocumentsNames="Tutti i documenti sincronizzati anche con estensioni diverse devono avere nomi diversi.\nFornire dei nomi di documenti diversi per tutti i documenti sincronizzati della sessione. ";
WRN_RecVpmDupDocumentsNames.Generic="Tutti i documenti sincronizzati anche con estensioni diverse devono avere nomi diversi.";
WRN_RecVpmDupDocumentsNames.Suggestion="Fornire dei nomi di documenti diversi per tutti i documenti sincronizzati della sessione. ";

WRN_RecEV4ErrorReport="Errore durante l'esecuzione della interrogazione intermedia: /p1\n";
WRN_RecEV4ErrorReport.Generic="Errore durante l'esecuzione della interrogazione intermedia: \n";
WRN_RecEV4ErrorReport.Detail="/p1";

WRN_RecNoPredicatesDefine="Non � definito nessun predicato valido per il criterio /p1.\nVerificare che le impostazioni di accesso VPDM siano ben definite o che il sistema di accesso VPDM sia disponibile.";
WRN_RecNoPredicatesDefine.Generic="Per il criterio specificato non � definito nessun predicato valido.";
WRN_RecNoPredicatesDefine.Detail="Nome del criterio: /p1";
WRN_RecNoPredicatesDefine.Suggestion="Verificare che le impostazioni di accesso VPDM siano ben definite o che il sistema di accesso VPDM sia disponibile.";

// for magic recon

// ---ambiguity Name and comment.

RuleAmbiguity.Name="Regola";
RuleAmbiguity.Comment="Selezionare una regola appropriata, poich� � possibile applicare le regole /p1.";

MappingAmbiguity.Name="Mappa";
MappingAmbiguity.Comment="Selezionare un risultato dell'interrogazione appropriato poich� sono stati trovati /p1 risultati dell'interrogazione.";

PersistencyAmbiguity.Name="Permanenza";
PersistencyAmbiguity.Comment="Selezionare una modalit� di archiviazione appropriata, poich� � possibile utilizzare la modalit� di archiviazione /p1.";

// --- Magic Recon- Active PDM not found.

ERR_NoConnectedPDM="PDM non connesso. Effettuare la connessione a /p1 prima di eseguire la sincronizzazione.";
ERR_NoConnectedPDM.Generic="PDM non connesso. Effettuare la connessione a PDM prima di eseguire la sincronizzazione.";
ERR_NoConnectedPDM.Detail="� necessaria una connessione a /P1.";
ERR_NoConnectedPDM.Suggestion="Annullare la sincronizzazione, connettersi a PDM e riavviare la sincronizzazione.";

// --- Magic Recon- MagicRecon Execution Failed.

ERR_MagicReconFailed="Errore durante l'esecuzione del gruppo globale /p1.";
ERR_MagicReconFailed.Generic="Errore durante l'esecuzione del gruppo globale.";
ERR_MagicReconFailed.Detail="/p1 Errore gruppo globale.";
ERR_MagicReconFailed.Suggestion="";

WRN_NoPRCFound="Impossibile trovare il prodotto principale /p1 nell'archivio ENOVIA.";
WRN_NoPRCFound.Generic="Impossibile trovare il prodotto principale nell'archivio ENOVIA:";
WRN_NoPRCFound.Detail="  /p1";
WRN_NoPRCFound.Suggestion="Verificare se questi prodotti principali sono presenti nell'archivio ENOVIA.";
