//=====================================================================================
//                                     CNEXT - CXR1
//                          COPYRIGHT DASSAULT SYSTEMES 1998 
//-------------------------------------------------------------------------------------
// FILENAME    :    DrwBG
// LOCATION    :    DraftingUI
// AUTHOR      :    fgx
// BUT         :    Workbench Drafting Background
// DATE        :    17.11.1998
//-------------------------------------------------------------------------------------
// DESCRIPTION :    
//-------------------------------------------------------------------------------------
// COMMENTS    :    
//-------------------------------------------------------------------------------------
// MODIFICATIONS     user  date        purpose
//    HISTORY        ----  ----        -------
//      00           fgx   ??.05.98    Creation
//      01           fgx   23.11.98    Nouvelle version CXR2
//      02           fgx   10.12.98    Changement de nom des toolbars background pour les records
//      03           fgx   26.02.99    Ajout tb de style
//      04           fgx   29.03.99    Nouvelles commandes sketcher
//      05           fgx   04.08.99    Nouveaux workbenches
//      06           fgx   17.11.99    Ajout des tb measure et browser
//      07           fgx   09.12.99    Ajout de tb analyze + container lines
//	  08	         lgk   15.10.02	   Revision
//=====================================================================================

// Configuration
// -------------
DrwBG.Title="Drafting";
DrwBG.Help = "Workbench di Drafting per la vista di sfondo";
DrwBG.ShortHelp = "Drafting";

// Toolbars
// --------
// Right
contTbSelectBG.Title="Seleziona";
contTbDrawingBG.Title="Disegno";
contTbViewsBG.Title="Viste";
contTbDimensioningBG.Title="Quotatura";
contTbGenerationBG.Title="Creazione";
contTbAnnotationsBG.Title="Annotazioni";
contTbDressupBG.Title="Dettagliatura";
contTbGeometryCreationBG.Title="Crea geometria";
contTbGeometryEditionBG.Title="Modifica geometria";
// Top
contTbStyleBG.Title="Stile";
contTbTextPropertiesBG.Title="ProprietÓ del testo";
contTbDimensionPropertiesBG.Title="ProprietÓ di quota";
CATDrwMumericalPropertiesBGTlb.Title="ProprietÓ numeriche";
contTbGraphicPropertiesBG.Title="ProprietÓ grafiche";
// Bottom
contTbUpdateBG.Title="Aggiorna";
contTbToolsBG.Title="Strumenti";
CATDrwVisualizationBckTlb.Title="Visualizzazione";
contTbMeasureBG.Title="Misura";
contTbBrowserBG.Title="Browser del catalogo";
// UnDock
contTbOrientationBG.Title="Posizione e orientamento";
contTbPositionBG.Title="Posizione";
contTbAnalyzeBG.Title="Analisi";

// Sub-Toolbars
// ------------
contSheets.Title="Fogli";
contProjections.Title="Proiezioni";
contSections.Title="Sezioni";
contDetails.Title="Dettagli";
contClippings.Title="Mascheratura";
contBroken.Title="Vista spezzata";
contViewWizard.Title="Procedura guidata";
contDimensions.Title="Quote";
CATDrwFeatDimIcb.Title="Quote di feature tecnologiche";
CATDrwContInterruptTlb.Title="Modifica della quota";
contTolerancing.Title="Tolleranza";
contGenDim.Title="Creazione di quote";
contTexts.Title="Testo";
contSymbols.Title="Simboli";
CATDrwContTableIcb.Title="Tabella";
contAxis.Title="Assi e filettature";
contPoint.Title="Punti";
contLine.Title="Linee";
contCircle.Title="Circonferenze ed ellissi";
contProfiles.Title="Profili";
contCurves.Title="Curve";
contRelimit.Title="Relimitazioni";
contTransfor.Title="Trasformazioni";
contCst.Title="Vincoli";
CATDrwAreaFillIcb.Title="Tratteggio";
CATDrwSktAnalyseIcb.Title="Strumenti di analisi 2D";
CATDrwBOMIcb.Title="Distinta base";

// Sub-Menus
// ---------
// First level
contMenuViews.Title="Viste";
contMenuViews.Mnemonic="V";
contMenuDrawing.Title="Disegno";
contMenuDrawing.Mnemonic="s";
contMenuDimensioning.Title="Quotatura";
contMenuDimensioning.Mnemonic="q";
contMenuGeneration.Title="Creazione";
contMenuGeneration.Mnemonic="n";
contMenuAnnotations.Title="Annotazioni";
contMenuAnnotations.Mnemonic="A";
contMenuDressup.Title="Dettagliatura";
contMenuDressup.Mnemonic="u";
contMenuGeometryCreation.Title="Crea geometria";
contMenuGeometryCreation.Mnemonic="g";
contMenuGeometryEdition.Title="Modifica geometria";
contMenuGeometryEdition.Mnemonic="M";
contMenuDrwGenDimTools.Title="Creazione di quote";
contMenuDrwGenDimTools.Mnemonic="t";
contMenuDrwPositioning.Title="Posizione";
contMenuDrwPositioning.Mnemonic="P";
contMenuDrwAnalyze.Title="Analisi";
contMenuDrwAnalyze.Mnemonic="A";
contMenuCst.Title="Vincoli";
contMenuCst.Mnemonic="h";

// Second level
contMenuProjections.Title="Proiezioni";
contMenuProjections.Mnemonic="P";
contMenuSections.Title="Sezioni";
contMenuSections.Mnemonic="S";
contMenuDetails.Title="Dettagli";
contMenuDetails.Mnemonic="q";
contMenuClippings.Title="Mascheratura";
contMenuClippings.Mnemonic="h";
contMenuViewWizard.Title="Procedura guidata";
contMenuViewWizard.Mnemonic="W";
// ---
contMenuSheets.Title="Fogli";
contMenuSheets.Mnemonic="S";
// ---
contMenuDimensions.Title="Quote";
contMenuDimensions.Mnemonic="q";
CATDrwFeatDimSnu.Title="Quote di feature tecnologiche";
CATDrwFeatDimSnu.Mnemonic="f";
contMenuTolerancing.Title="Tolleranza";
contMenuTolerancing.Mnemonic="T";
// ---
contMenuTexts.Title="Testo";
contMenuTexts.Mnemonic="T";
contMenuSymbols.Title="Simboli";
contMenuSymbols.Mnemonic="S";
CATDrwContTableMenuSnu.Title="Tabella";
CATDrwContTableMenuSnu.Mnemonic="a";
CATDrwAreaFillSnu.Title="Tratteggio";

// ---
contMenuAxis.Title="Assi e filettature";
contMenuAxis.Mnemonic="A";
// ---
contMenuPoint.Title="Punti";
contMenuPoint.Mnemonic="P";
contMenuLine.Title="Linee";
contMenuLine.Mnemonic="L";
contMenuCircle.Title="Circonferenze ed ellissi";
contMenuCircle.Mnemonic="h";
contMenuProfiles.Title="Profili";
contMenuProfiles.Mnemonic="f";
contMenuCurves.Title="Curve";
contMenuCurves.Mnemonic="u";
// ---
contMenuRelimit.Title="Relimitazioni";
contMenuRelimit.Mnemonic="R";
contMenuTransfor.Title="Trasformazioni";
contMenuTransfor.Mnemonic="T";
CATDrwBOMSnu.Title="Distinta base";

// Third level
contMenuInterrupt.Title="Modifica della quota";
contMenuInterrupt.Mnemonic="d";

// Contextual Sub-Menus
// --------------------
DrwPositionalLink.Title="Associazione di posizione";
DrwOrientationLink.Title="Associazione di orientamento";
DrwViewPositioning.Title="Posizionamento della vista";
DrwDimInterruptSubMenu.Title="Interruzione";
CATDrwViewPositioningTlb.Title="Posizionamento della vista";
CATDrwGVSSnu.Title="Stile vista generativa";
CATDrwDimensionRepresentation.Title="Rappresentazione della quota";
CATDrwMeasureMode.Title="ModalitÓ misure";
CATDrwRepresentationMode.Title="ModalitÓ di rappresentazione";
CATDrwAngleSector.Title="Settore di angolo";
CATDrwClipBoxSnu.Title="Schermatura 3D";
CATDrwPrintAreaSnu.Title="Stampa area";
