//=============================================================================
//                                     CNEXT - CXR1
//                          COPYRIGHT DASSAULT SYSTEMES 1999 
//-----------------------------------------------------------------------------
// FILENAME    :    CATDrwCheckOptionHeader
// LOCATION    :    DraftingUI
// AUTHOR      :    fgx
// DATE        :    02.02.99
//------------------------------------------------------------------------------
// DESCRIPTION :    
//                  
//------------------------------------------------------------------------------
// COMMENTS    :	 ENGLISH VERSION
//------------------------------------------------------------------------------
//MODIFICATIONS     user  date      purpose
//   HISTORY        ----  ----      -------
//     01			fgx	  22.02.99	Ajout de CstCreation
//     02			jbb	  23.03.99	Ajout de DrwHLRFilter
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Dimensions Design Mode
//------------------------------------------------------------------------------
CATDrwCheckOptionHeader.DrwDimDesignMode.Title="Modalit� visualizzazione analisi";
CATDrwCheckOptionHeader.DrwDimDesignMode.ShortHelp="Modalit� visualizzazione analisi";
CATDrwCheckOptionHeader.DrwDimDesignMode.Help=
"Visualizza i vari tipi di quote in colori diversi e personalizzabili";
CATDrwCheckOptionHeader.DrwDimDesignMode.LongHelp =
"Modalit� visualizzazione analisi
Visualizza i vari tipi di quote in colori diversi e personalizzabili.";

//------------------------------------------------------------------------------
// Show / No Show 2D Constraints
//------------------------------------------------------------------------------
CATDrwCheckOptionHeader.DrwShowCst.Title="Visualizza i vincoli" ;
CATDrwCheckOptionHeader.DrwShowCst.ShortHelp="Visualizza i vincoli";
CATDrwCheckOptionHeader.DrwShowCst.Help="Visualizza i vincoli 2D nella vista attiva" ;
CATDrwCheckOptionHeader.DrwShowCst.LongHelp =
"Visualizza i vincoli
Visualizza i vincoli 2D nella vista attiva.";

//------------------------------------------------------------------------------
// Create all Constraints
//------------------------------------------------------------------------------
CATDrwCheckOptionHeader.DrwCstCreation.Title="Crea i vincoli rilevati" ;
CATDrwCheckOptionHeader.DrwCstCreation.ShortHelp="Crea i vincoli rilevati";
CATDrwCheckOptionHeader.DrwCstCreation.Help="Crea i vincoli 2D rilevati nella vista attiva" ;
CATDrwCheckOptionHeader.DrwCstCreation.LongHelp =
"Crea i vincoli rilevati
Crea i vincoli 2D rilevati nella vista attiva.";

//------------------------------------------------------------------------------
// Filter Generated elements
//------------------------------------------------------------------------------
CATDrwCheckOptionHeader.DrwHLRFilter.Title="Filtra gli elementi creati" ;
CATDrwCheckOptionHeader.DrwHLRFilter.ShortHelp="Filtra gli elementi creati";
CATDrwCheckOptionHeader.DrwHLRFilter.Help="Applica un filtro visivo agli elementi creati" ;
CATDrwCheckOptionHeader.DrwHLRFilter.LongHelp =
"Filtra gli elementi creati
Applica un filtro visivo agli elementi creati.";

//------------------------------------------------------------------------------
// Move Only Selected Dim Subpart
//------------------------------------------------------------------------------
CATDrwCheckOptionHeader.DrwDimMoveSubPart.Title="Sposta solo il componente (Subpart)" ;
CATDrwCheckOptionHeader.DrwDimMoveSubPart.ShortHelp="Sposta solo il componente (Subpart)";
CATDrwCheckOptionHeader.DrwDimMoveSubPart.Help="Sposta solo il componente selezionato della quota" ;
CATDrwCheckOptionHeader.DrwDimMoveSubPart.LongHelp =
"Sposta solo il componente
Sposta solo il componente selezionato della quota.";

//------------------------------------------------------------------------------
// Dimension system selection mode
//------------------------------------------------------------------------------
CATDrwCheckOptionHeader.CATDrwDimSystemSelectModHdr.Title="Modalit� di selezione del sistema di quotatura";
CATDrwCheckOptionHeader.CATDrwDimSystemSelectModHdr.ShortHelp="Modalit� di selezione del sistema di quotatura";
CATDrwCheckOptionHeader.CATDrwDimSystemSelectModHdr.Help="Modalit� di selezione del sistema di quotatura" ;
CATDrwCheckOptionHeader.CATDrwDimSystemSelectModHdr.LongHelp ="Modalit� di selezione del sistema di quotatura";

//------------------------------------------------------------------------------
// Display view frame
//------------------------------------------------------------------------------
CATDrwCheckOptionHeader.CATDrwDisplayViewFrameHdr.Title="Visualizza la cornice della vista come specificato per ciascuna vista";
CATDrwCheckOptionHeader.CATDrwDisplayViewFrameHdr.ShortHelp= "Visualizza la cornice della vista come specificato per ciascuna vista";
CATDrwCheckOptionHeader.CATDrwDisplayViewFrameHdr.Help="Visualizza la cornice della vista come specificato in ciascuna propriet� della vista";
CATDrwCheckOptionHeader.CATDrwDisplayViewFrameHdr.LongHelp ="Visualizza la cornice della vista come specificato per ciascuna vista\nVisualizza la cornice della vista come specificato nelle propriet� della vista.";

//------------------------------------------------------------------------------
// GME creation
//------------------------------------------------------------------------------
CATDrwCheckOptionHeader.CATDrwGMECreationHdr.Title="Crea dettagliatura associativa" ;
CATDrwCheckOptionHeader.CATDrwGMECreationHdr.ShortHelp="Crea dettagliatura associativa";
CATDrwCheckOptionHeader.CATDrwGMECreationHdr.Help="Rende le specifiche di dettagliatura persistenti mediante gli aggiornamenti." ;
CATDrwCheckOptionHeader.CATDrwGMECreationHdr.LongHelp =
"Crea dettagliatura associativa
Rende le specifiche di dettagliatura persistenti mediante gli aggiornamenti.";


//------------------------------------------------------------------------------
// Display grid
//------------------------------------------------------------------------------
CATDrwCheckOptionHeader.DisplayGrid.Title="Visualizza griglia";
CATDrwCheckOptionHeader.DisplayGrid.Help="Visualizza o nasconde la griglia dello Sketcher";
CATDrwCheckOptionHeader.DisplayGrid.ShortHelp="Griglia dello Sketcher";
CATDrwCheckOptionHeader.DisplayGrid.LongHelp="Visualizza griglia\nVisualizza una griglia che fa da guida durante la creazione dello schizzo.";

//------------------------------------------------------------------------------
// SnapGrid
//------------------------------------------------------------------------------
CATDrwCheckOptionHeader.SnapGrid.Title="Snap al punto";
CATDrwCheckOptionHeader.SnapGrid.Help="Esegue lo snap ad un punto della griglia";
CATDrwCheckOptionHeader.SnapGrid.ShortHelp="Snap al punto";
CATDrwCheckOptionHeader.SnapGrid.LongHelp="Esegue lo snap dei punti creati\nai pi� vicini punti di\nintersezione della griglia.";
