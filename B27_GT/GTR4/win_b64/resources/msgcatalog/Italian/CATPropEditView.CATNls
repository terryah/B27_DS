//=============================================================================
//                                     CNEXT - CXR1
//                          COPYRIGHT DASSAULT SYSTEMES 1998 
//-----------------------------------------------------------------------------
// FILENAME    :    CATPropEditView
// LOCATION    :    DraftingUI/CNext/resources/msgcatalog
// AUTHOR      :    tbe
// DATE        :    14/1/1998
//------------------------------------------------------------------------------
// DESCRIPTION :    Resources File for NLS purpose related to View property
//                  
//------------------------------------------------------------------------------
// COMMENTS    :	ENGLISH VERSION
//------------------------------------------------------------------------------
//MODIFICATIONS     user  date      purpose
//   HISTORY        ----  ----      -------
//		01			ogk		26.01.99	- ajout pour les dressups
//      02          ypr     10/05/00    - ajout pour thread
//		03			ogk		01.09.00	- ajout	viewNameLabel (correction)
//		04			ogk		24.10.00	- nouveau contenu (multi-sel)
//		05			lgk		26.06.02	Revision
//		06			lgk		15.10.02	Revision
//		07			lgk		17.12.02	Revision
//------------------------------------------------------------------------------


// Global Parameters 
// =================
globalParamsFrameTitle.Title="Scala e orientamento";
globalParamsFrame.Title="Parametri globali";
globalParamsFrame.LongHelp=
"Parametri
Modifica i parametri della vista: \"rotazione\" e \"scala\"."; 


// Global Parameters / Scale
// =========================
globalParamsFrame.scaleEditors.scaleLabel.Title="     Scala:";
globalParamsFrame.scaleEditors.scaleLabel.Help="Modifica la scala delle viste con un valore positivo";
globalParamsFrame.scaleEditors.scaleLabel.ShortHelp="Scala delle viste";
globalParamsFrame.scaleEditors.scaleLabel.LongHelp="Scala
Modifica la scala delle viste con un valore positivo";
//
globalParamsFrame.scaleEditors.scaleSepLabel.Title=":";



// Global Parameters / Angle
// =========================
globalParamsFrame.angleLabel.Title="Angolo:";
globalParamsFrame.angleLabel.Help="Assegna un valore di angolo di \"rotazione\" alle viste selezionate";
globalParamsFrame.angleLabel.ShortHelp="Angolo di visuale";
globalParamsFrame.angleLabel.LongHelp="Angolo di visuale
Assegna un valore di angolo di \"rotazione\" alle viste selezionate.
Per impostazione predefinita, questo valore viene espresso utilizzando l'unit� della sessione corrente.
Quando si inserisce un valore non � necessario specificare l'unit�. Tuttavia,
� possibile cambiare temporaneamente l'unit�
(ad esempio, inserire \"180 gradi\" invece di \"3.14\" se l'unit� della sessione corrente � \"rad\"). ";
//
globalParamsFrame.angleEdt.Title="Angolo:";
globalParamsFrame.angleEdt.Help="Assegna un valore di angolo di \"rotazione\" alle viste selezionate";
globalParamsFrame.angleEdt.ShortHelp="Angolo di visuale ";
globalParamsFrame.angleEdt.LongHelp="Angolo di visuale
Assegna un valore di angolo di \"rotazione\" alle viste selezionate.
Per impostazione predefinita, questo valore viene espresso utilizzando l'unit� della sessione corrente.
Quando si inserisce un valore non � necessario specificare l'unit�. Tuttavia,
� possibile cambiare temporaneamente l'unit�
(ad esempio, inserire \"180 gradi\" invece di \"3.14\" se l'unit� della sessione corrente � \"rad\"). ";



// Name Parameters 
// ===============
nameParamsFrameTitle.Title="Nome vista";
nameParamsFrame.Title="Nome vista";
nameParamsFrame.LongHelp="Nome vista
Modifica i parametri relativi al nome delle viste selezionate.";


// Name Parameters / Prefix
// ========================
nameParamsFrame.prefixLabel.Title="Prefisso";
nameParamsFrame.prefixLabel.Help="Assegna un prefisso al nome delle viste selezionate";
nameParamsFrame.prefixLabel.ShortHelp="Prefisso del nome";
nameParamsFrame.prefixLabel.LongHelp="Prefisso
Assegna un prefisso al nome delle viste selezionate.";


// Name Parameters / Ident
// =======================
nameParamsFrame.identLabel.Title="ID";
nameParamsFrame.identLabel.Help="Assegna un \"ID\" al nome delle viste selezionate";
nameParamsFrame.identLabel.ShortHelp="ID nome";
nameParamsFrame.identLabel.LongHelp="Nome ID
Assegna un \"ID\" al nome delle viste selezionate.";


// Name Parameters / Suffix
// ========================
nameParamsFrame.suffixLabel.Title="Suffisso";
nameParamsFrame.suffixLabel.Help="Assegna un \"suffisso\" al nome delle viste selezionate";
nameParamsFrame.suffixLabel.ShortHelp="Suffisso del nome";
nameParamsFrame.suffixLabel.LongHelp="Suffisso
Assegna un \"suffisso\" al nome delle viste selezionate.";


// Name Parameters / Name Editor
// =============================
nameParamsFrame.nameCkeEditorFrame.Title="Editor dei nomi con formula:";
nameParamsFrame.nameCkeEditorFrame.Name.EnglobingFrame.IntermediateFrame.Label.Title=" ";


// Detail Name 
// ===========
detailParamsFrame.Title="Componenti 2D";
detailParamsFrame.ShortHelp="Componenti 2D";


// Detail Name / Editor
// ===========
detailviewTitle.Title="Componenti 2D";
detailParamsFrame.detailLabel.Title=" ";
detailParamsFrame.detailLabel.Help="Assegna un nome alle viste di dettaglio selezionate";
detailParamsFrame.detailLabel.ShortHelp="Vista di dettaglio";
detailParamsFrame.detailLabel.LongHelp="Vista di dettaglio
Assegna un nome alle viste di dettaglio selezionate.";

// Dressup
// =======
dressupTitle.Title="Dettagliatura";
generativeParamsFrame.Title="Dettagliatura";
generativeParamsFrame.LongHelp=
"Dettagliatura
Aggiunge elementi alle viste generative selezionate."; 



// Fillets
// =======
filletFrame.Title="Raccordi";
filletFrame.LongHelp=
" "; 

// Dressup / Hidden Lines 
// ======================
generativeParamsFrame.hiddenLinesChkBtn.Title="Linee nascoste";
generativeParamsFrame.hiddenLinesChkBtn.LongHelp="Linee nascoste
Indica se le \"linee nascoste\" devono essere visualizzate nelle viste selezionate.";



// Dressup / Axis
// ==============
generativeParamsFrame.axisChkBtn.Title="Asse";
generativeParamsFrame.axisChkBtn.LongHelp="Asse
Indica se gli \"assi\" devono essere visualizzati nelle vista selezionate.";


// Dressup / Center Line
// ======================
generativeParamsFrame.centerLinesChkBtn.Title="Linea di centro";
generativeParamsFrame.centerLinesChkBtn.LongHelp="Linea di centro
Indica se le \"linee di centro\" devono essere visualizzate nella viste selezionate.";



// Dressup / Thread
// ================
generativeParamsFrame.threadChkBtn.Title="Filettatura";
//npq: 18/10/2005 IR 0513559 LongHelp added
generativeParamsFrame.threadChkBtn.LongHelp="Filettatura
Definisce se le \"filettature\" devono essere visualizzate nelle viste selezionate.";


// Dressup / Fillets
// =========================
filletFrame.boundFilletChkBtn.Title="Raccordi:";
filletFrame.boundFilletChkBtn.LongHelp="Raccordi
Definisce la modalit� di rappresentazione dei raccordi nelle viste selezionate.";

// Dressup / BoundaryFillets
// =========================
filletFrame.boundaryRdBtn.Title="Bordi";
filletFrame.boundaryRdBtn.LongHelp="Bordi
Indica se visualizzare i bordi dei raccordi nelle viste selezionate.";

// Dressup / Original Edges
// =========================
filletFrame.ficFilletRdBtn.Title="Simbolico";
filletFrame.ficFilletRdBtn.LongHelp="Simbolico
Indica se � necessario utilizzare la modalit� di estrazione \"raccordi simbolici\" nelle viste selezionate.";

filletFrame.projoriginaledgeFilletRdBtn.Title="Spigoli originali proiettati";
filletFrame.projoriginaledgeFilletRdBtn.LongHelp="Spigoli originali proiettati
Indica se � necessario utilizzare la modalit� di estrazione \"Spigoli originali proiettati\" nelle viste selezionate.";

filletFrame.originaledgeFilletRdBtn.Title = "Spigoli originali approssimati";
filletFrame.originaledgeFilletRdBtn.LongHelp="Spigoli originali approssimati
Indica se � necessario utilizzare la modalit� di estrazione \"Spigoli originali approssimati\" nelle viste selezionate.";

// Dressup / 3D Spec
// ====================
generativeParamsFrame.uncutSpecChkBtn.Title="Spec 3D";
generativeParamsFrame.uncutSpecChkBtn.LongHelp="Spec 3D
Indica se visualizzare \"Spec 3D\" nelle viste selezionate.";

// Dressup / 3D Wireframe
// =======================
generativeParamsFrame.3DWireframeChkBtn.Title="Wireframe 3D";
generativeParamsFrame.3DWireframeChkBtn.LongHelp="Wireframe 3D
Indica se visualizzare il \"wireframe 3D\" nelle viste selezionate.";

// Dressup / 3D Wireframe A PARTIR DE LA R11
// =========================================
3DPointsFrame.3DWireframeFrame.3DWireframeChkBtn.Title="Wireframe 3D";
3DPointsFrame.3DWireframeFrame.3DWireframeChkBtn.LongHelp="Wireframe 3D
Indica se i wireframe 3D proiettati possono essere nascosti
o sempre visibili nelle viste selezionate.";
3DPointsFrame.3DWireframeFrame.3DWireframeHLRRdBtn.Title="Pu� essere nascosto";
3DPointsFrame.3DWireframeFrame.3DWireframeProjectionRdBtn.Title="Sempre visibile";

// Dressup / 3D Points
// ====================
generativeParamsFrame.3DPointsChkBtn.Title="Punti 3D";
generativeParamsFrame.3DPointsChkBtn.LongHelp="Punti 3D
Indica se i \"Punti 3D\" devono essere visualizzati nelle viste selezionate.";
3DPointsFrame.3DPointsChkBtn.Title="Punti 3D:";
3DPointsFrame.3DPointsChkBtn.LongHelp="Punti 3D
Indica se i \"Punti 3D\" devono essere visualizzati nelle viste selezionate.";
3DPointsFrame.3DSymbolRdBtn.Title="Eredita simboli 3D";
3DPointsFrame.3DSymbolRdBtn.LongHelp="Eredita simboli 3D
Visualizza il simbolo ereditato dal 3D";
3DPointsFrame.UsrSymbolRdBtn.Title="Simbolo";
3DPointsFrame.UsrSymbolRdBtn.LongHelp="Simbolo
Visualizza il simbolo selezionato";

// Dressup / 3D Colors
// ===================
generativeParamsFrame.3DColorsChkBtn.Title="Colori 3D";
generativeParamsFrame.3DColorsChkBtn.LongHelp="Colori 3D
Definisce se utilizzare i \"Colori 3D\" nelle viste selezionate.";

// Visu & Behaviour 
// ================
visuTitle.Title="Visualizzazione e funzionamento";
visuBehaviourFrame.Title="Visualizzazione e funzionamento";
visuBehaviourFrame.ShortHelp="Visualizzazione e funzionamento delle viste selezionate";
visuBehaviourFrame.Help="Indica se le cornici delle viste devono essere visualizzate e se le viste devono essere bloccate";
visuBehaviourFrame.LongHelp="Visualizzazione e funzionamento delle viste selezionate
Indica se le cornici delle viste devono essere visualizzate e se le viste devono essere bloccate.";



// Visu & Behaviour  / Frame
// =========================
visuBehaviourFrame.viewFrameChkBtn.Title="Visualizza la cornice della vista";
visuBehaviourFrame.viewFrameChkBtn.ShortHelp="Visualizza la cornice della vista";
visuBehaviourFrame.viewFrameChkBtn.Help="Indica se le cornici delle viste devono essere visualizzate o meno";
visuBehaviourFrame.viewFrameChkBtn.LongHelp="Cornice della vista
Indica se le cornici delle viste devono essere visualizzate o meno.";

// Visu & Behaviour / visual clipping
//===================================
visuBehaviourFrame.FramingChkBtn.Title="Ritaglio visuale";
visuBehaviourFrame.FramingChkBtn.ShortHelp="Utilizza un ritaglio visuale";
visuBehaviourFrame.FramingChkBtn.Help="Indica se utilizzare o meno un ritaglio visuale sulle viste";
visuBehaviourFrame.FramingChkBtn.LongHelp="Ritaglio visuale
Indica se utilizzare o meno un ritaglio visuale sulle viste";

// Visu & Behaviour  / Lock
// ========================
visuBehaviourFrame.viewLockChkBtn.Title="Blocca la vista";
visuBehaviourFrame.viewLockChkBtn.ShortHelp="Blocca le viste selezionate";
visuBehaviourFrame.viewLockChkBtn.Help="Indica se le cornici delle viste devono essere bloccate o meno";
visuBehaviourFrame.viewLockChkBtn.LongHelp="Blocca la vista
Indica se le cornici delle viste devono essere bloccate o meno.";

// Generation Mode 
// ================
generationModeTitle.Title="Modalit� creazione";
generationModeFrame.Title="Modalit� creazione";
generationModeFrame.ShortHelp="Modalit� di creazione delle viste selezionate";
generationModeFrame.LongHelp=
"Indica se le viste selezionato sono generate da dati topologici
o da dati CGR e le ricrea, se necessario.";


generationModeFrame.DrwGenerationForBox.Title="Genera solo parti pi� grandi di";
generationModeFrame.DrwGenerationForBox.LongHelp="Genera solo parti pi� grandi di";
generationModeFrame.spinGenBox.LongHelp="Genera solo parti pi� grandi di";
generationModeFrame.OcclusionCullingChkBtn.Title="Abilita l'eliminazione delle parti non in vista";
generationModeFrame.OcclusionCullingChkBtn.LongHelp="Abilita l'eliminazione delle parti non in vista
Gli oggetti nascosti non vengono presi in considerazione durante il calcolo";
generationModeFrame.Combolabel.Title = "Modalit� di creazione della vista";
generationModeFrame.GenerationModeOption.Title = "Opzioni";
GenerationModeOptionPanelTitle= "Opzioni della modalit� Creazione";
GenerationModeOptionPanelTitleHRV= "Modalit� approssimata";

// Messages divers
// ===============

TabTitle="Vista";
TabTitle.LongHelp="Definisce la vista.";

ScaleText="Scala:";
ScaleOutOfRangeTitle="Scala fuori intervallo";
ScaleOutOfRangeText="La scala deve essere un valore positivo minore di 10000.
Eventuali valori in scala non corretti sono stati automaticamente impostati
sul pi� vicino valore consentito.";

gvsFrame.buttonResetStyle.Title="Ripristina i valori di stile";
gvsFrame.Title="Stile della vista generativa";
GVSTitle.Title="Stile della vista generativa";
GVSOverloadHelp="Parametro sovraccaricato dall'utente";
gvsFrame.buttonRemoveStyle.Title="Rimuovi stile";

