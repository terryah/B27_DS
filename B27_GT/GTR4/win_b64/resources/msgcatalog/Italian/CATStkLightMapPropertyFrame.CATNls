// COPYRIGHT DASSAULT SYSTEMES 2001
//===================================================================
//
// CATStkLightMapPropertyFrame.CATNls
// The dialog : CATStkLightMapPropertyFrame
//
//===================================================================
//
// Usage notes:
//
//===================================================================
//
//  03 Oct 2001  Creation STX
//===================================================================

panelFrame.propertiesFrame.selectorFrame.selectorButtonsFrame.nbSelectionLabel.Title = "Selezione ";

Face  = " elemento";
Faces = " elementi";

ApplyErrorTitle = "Errore";
ApplyErrorMsg   = "L'adesivo pu� essere applicato solo ad un prodotto";

EditErrorTitle = "Errore";
EditErrorMsg   = "Nessun adesivo selezionato";

panelFrame.containerFrame.textureFrame.textureLabel.Title = "Tessitura ";
panelFrame.containerFrame.textureFrame.textureLabel.Help = "Definisce il percorso della tessitura dell'adesivo dell'ombra";
panelFrame.containerFrame.textureFrame.textureLabel.ShortHelp = "Definisce la tessitura dell'adesivo dell'ombra";
panelFrame.containerFrame.textureFrame.textureLabel.LongHelp =
"Definisce il percorso della tessitura dell'adesivo dell'ombra.
Il file non deve esistere, infatti verr� creato
durante la resa dell'adesivo dell'ombra.";

panelFrame.containerFrame.textureFrame.textureSelector.Help = "Definisce il percorso della tessitura dell'adesivo dell'ombra";
panelFrame.containerFrame.textureFrame.textureSelector.ShortHelp = "Definisce la tessitura dell'adesivo dell'ombra";
panelFrame.containerFrame.textureFrame.textureSelector.LongHelp =
"Definisce il percorso della tessitura dell'adesivo dell'ombra.
Il file non deve esistere, infatti verr� creato
durante la resa dell'adesivo dell'ombra.";

panelFrame.containerFrame.textureFrame.sizeLabel.Title = "Dimensione ";
panelFrame.containerFrame.textureFrame.sizeLabel.Help = "Definisce la dimensione della tessitura";
panelFrame.containerFrame.textureFrame.sizeLabel.ShortHelp = "Dimensione della tessitura";
panelFrame.containerFrame.textureFrame.sizeLabel.LongHelp =
"Definisce la dimensione dell'adesivo dell'ombra in pixel.
La dimensione viene utilizzata quando viene creato l'adesivo dell'ombra
alla fine della resa.";

panelFrame.containerFrame.textureFrame.sizeFrame.widthSpinner.Help = "Definisce la dimensione della tessitura";
panelFrame.containerFrame.textureFrame.sizeFrame.widthSpinner.ShortHelp = "Dimensione della tessitura";
panelFrame.containerFrame.textureFrame.sizeFrame.widthSpinner.LongHelp =
"Definisce la dimensione dell'adesivo dell'ombra in pixel.
La dimensione viene utilizzata quando viene creato l'adesivo dell'ombra
alla fine della resa.";

panelFrame.containerFrame.textureFrame.sizeFrame.heightSpinner.Help = "Definisce la dimensione della tessitura";
panelFrame.containerFrame.textureFrame.sizeFrame.heightSpinner.ShortHelp = "Dimensione della tessitura";
panelFrame.containerFrame.textureFrame.sizeFrame.heightSpinner.LongHelp =
"Definisce la dimensione dell'adesivo dell'ombra in pixel.
La dimensione viene utilizzata quando viene creato l'adesivo dell'ombra
alla fine della resa.";

panelFrame.containerFrame.textureFrame.sizeFrame.sizeXLabel.Title = " x ";
panelFrame.containerFrame.textureFrame.sizeFrame.pixelsLabel.Title = " Pixel";

panelFrame.containerFrame.textureFrame.useNormalCheck.Title = "Utilizza la normale";
panelFrame.containerFrame.textureFrame.useNormalCheck.Help = "Utilizza la normale alla superficie";
panelFrame.containerFrame.textureFrame.useNormalCheck.ShortHelp = "Utilizza la normale alla superficie";
panelFrame.containerFrame.textureFrame.useNormalCheck.LongHelp =
"Utilizza la normale alla superficie.
Quando questa opzione � selezionata, l'illuminazione viene calcolata solo per
la faccia esterna delle superfici in modo che la resa sia
pi� rapida.";

panelFrame.containerFrame.textureFrame.reverseNormalCheck.Title = "Inverti normale";
panelFrame.containerFrame.textureFrame.reverseNormalCheck.Help = "Inverte l'orientamento della normale";
panelFrame.containerFrame.textureFrame.reverseNormalCheck.ShortHelp = "Inverte l'orientamento della normale";
panelFrame.containerFrame.textureFrame.reverseNormalCheck.LongHelp =
"Inverte l'orientamento della normale.";

panelFrame.containerFrame.textureFrame.transparencyLabel.Title = "Trasparenza";
panelFrame.containerFrame.textureFrame.transparencyLabel.Help = "Definisce la trasparenza della tessitura";
panelFrame.containerFrame.textureFrame.transparencyLabel.ShortHelp = "Trasparenza della tessitura";
panelFrame.containerFrame.textureFrame.transparencyLabel.LongHelp =
"Definisce la trasparenza della tessitura.";

panelFrame.containerFrame.textureFrame.transparencySlider.Help = "Definisce la trasparenza della tessitura";
panelFrame.containerFrame.textureFrame.transparencySlider.ShortHelp = "Trasparenza della tessitura";
panelFrame.containerFrame.textureFrame.transparencySlider.LongHelp =
"Definisce la trasparenza della tessitura.";

panelFrame.containerFrame.textureFrame.optionsFrame.Title = "Effetti";

panelFrame.containerFrame.textureFrame.optionsFrame.shadowsRadio.Title = "Ombre dell'oggetto";
panelFrame.containerFrame.textureFrame.optionsFrame.shadowsRadio.Help = "Rende le ombre dell'oggetto";
panelFrame.containerFrame.textureFrame.optionsFrame.shadowsRadio.ShortHelp = "Solo le ombre dell'oggetto";
panelFrame.containerFrame.textureFrame.optionsFrame.shadowsRadio.LongHelp =
"La resa della tessitura calcola solo le ombre dell'oggetto.";

panelFrame.containerFrame.textureFrame.optionsFrame.occlusionRadio.Title = "Occlusione dell'ambiente";
panelFrame.containerFrame.textureFrame.optionsFrame.occlusionRadio.Help = "Rende l'occlusione dell'ambiente";
panelFrame.containerFrame.textureFrame.optionsFrame.occlusionRadio.ShortHelp = "Solo occlusione dell'ambiente";
panelFrame.containerFrame.textureFrame.optionsFrame.occlusionRadio.LongHelp =
"La resa della tessitura calcola solo l'occlusione dell'ambiente.";

panelFrame.containerFrame.textureFrame.optionsFrame.bothRadio.Title = "Ombre dell'oggetto e occlusione dell'ambiente";
panelFrame.containerFrame.textureFrame.optionsFrame.bothRadio.Help = "Rende le ombre dell'oggetto e l'occlusione dell'ambiente";
panelFrame.containerFrame.textureFrame.optionsFrame.bothRadio.ShortHelp = "Ombre dell'oggetto e occlusione dell'ambiente";
panelFrame.containerFrame.textureFrame.optionsFrame.bothRadio.LongHelp =
"La resa della tessitura calcola le ombre dell'oggetto e
l'occlusione dell'ambiente.";

panelFrame.containerFrame.textureFrame.optionsFrame.samplesFrame.samplesLabel.Title = "Esempi di occlusione dell'ambiente  ";
panelFrame.containerFrame.textureFrame.optionsFrame.samplesFrame.samplesLabel.Help = "Esempi di occlusione dell'ambiente";
panelFrame.containerFrame.textureFrame.optionsFrame.samplesFrame.samplesLabel.ShortHelp = "Esempi di occlusione dell'ambiente";
panelFrame.containerFrame.textureFrame.optionsFrame.samplesFrame.samplesLabel.LongHelp =
"Esempi di occlusione dell'ambiente.";

panelFrame.containerFrame.textureFrame.optionsFrame.samplesFrame.samplesSpinner.Help = "Esempi di occlusione dell'ambiente";
panelFrame.containerFrame.textureFrame.optionsFrame.samplesFrame.samplesSpinner.ShortHelp = "Esempi di occlusione dell'ambiente";
panelFrame.containerFrame.textureFrame.optionsFrame.samplesFrame.samplesSpinner.LongHelp =
"Esempi di occlusione dell'ambiente.";
