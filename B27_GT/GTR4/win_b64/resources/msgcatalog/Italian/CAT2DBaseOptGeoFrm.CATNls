//=====================================================================================
//                                     CNEXT - CXR1
//                          COPYRIGHT DASSAULT SYSTEMES 1999 
//-------------------------------------------------------------------------------------
// FILENAME    :    CATDrwOptGeo
// LOCATION    :    DraftingUI
// AUTHOR      :    fgx
// BUT         :    
// DATE        :    06.01.1999
//-------------------------------------------------------------------------------------
// DESCRIPTION :    Tools/Option du Drafting - Onglet Geometry
//-------------------------------------------------------------------------------------
// COMMENTS    :    
//-------------------------------------------------------------------------------------
// MODIFICATIONS     user  date        purpose
//    HISTORY        ----  ----        -------
//      00           fgx   06.01.1999  Creation 	
//      01           fgx   08.01.1999  Passage sur CATDrwOptTitle et CATDrwOptIcon
//      02           fgx   20.01.1999  Ajout des options de visu des cst
//      03           fgx   20.01.1999  Deplacement des options de la grid de geo a gal
//      04           fgx   20.01.1999  Ajout de CreateCenter
//      05           fgx   22.02.1999  Ajout de CstCreation, retrait de CreateConstraint
//      06           fgx   23.08.1999  Manipulation directe de la geometrie
//      07           lgk   18.12.02    revision
//=====================================================================================

Title="Geometria";

frameCenter.HeaderFrame.Global.Title = "Geometria";
frameCenter.HeaderFrame.Global.LongHelp = "Fornisce gli strumenti per la creazione\ndi elementi aggiuntivi nella geometria.";
frameCenter.IconAndOptionsFrame.OptionsFrame.checkCenter.Title = "Crea centri di ellissi e di cerchi";
frameCenter.IconAndOptionsFrame.OptionsFrame.checkCenter.LongHelp = "Crea centri di ellissi e di cerchi\ndurante la creazione di cerchi e di ellissi.";
frameCenter.IconAndOptionsFrame.OptionsFrame._checkDragWithEndPts.Title = "Trascina gli estremi degli elementi inseriti";
frameCenter.IconAndOptionsFrame.OptionsFrame._checkDragWithEndPts.LongHelp = "Trascina gli elementi inclusi gli estremi.";
frameCenter.IconAndOptionsFrame.OptionsFrame.chkEndPtsDuplGenGeom.Title = "Crea gli estremi durante la duplicazione della geometria generata";
frameCenter.IconAndOptionsFrame.OptionsFrame.chkEndPtsDuplGenGeom.LongHelp = "Crea gli estremi quando la geometria generata dal 3D viene duplicata.";
frameCenter.IconAndOptionsFrame.OptionsFrame.chkShowHV.Title = "Visualizza campi H e V nella tavolozza Strumenti";
frameCenter.IconAndOptionsFrame.OptionsFrame.chkShowHV.LongHelp = "Visualizza i campi H e V nella tavolozza Strumenti\ndurante la creazione della geometria 2D o durante l'offset degli elementi.";

//drag elements
frameCenter.IconAndOptionsFrame.OptionsFrame.checkGeomDrag.Title="Consenti la manipolazione diretta ";
frameCenter.IconAndOptionsFrame.OptionsFrame.checkGeomDrag.LongHelp="Consente di spostare la geometria utilizzando il mouse.";
frameCenter.IconAndOptionsFrame.OptionsFrame.dragOptionPushButton.Title="Modalità di soluzione...";
frameCenter.IconAndOptionsFrame.OptionsFrame.dragOptionPushButton.LongHelp = "Consente di definire la modalità di soluzione per lo spostamento degli elementi.";

frameCstCreation.HeaderFrame.Global.Title="Creazione di vincoli ";
frameCstCreation.HeaderFrame.Global.LongHelp=
"Crea i vincoli individuati e
quelli basati sulla feature.";
frameCstCreation.IconAndOptionsFrame.OptionsFrame.checkCstCreation.Title="Crea i vincoli individuati e quelli basati sulla feature ";
frameCstCreation.IconAndOptionsFrame.OptionsFrame.checkCstCreation.LongHelp=
"Crea i vincoli individuati e
quelli basati sulla feature.";
frameCstCreation.IconAndOptionsFrame.OptionsFrame.framePushButton.smartPickPushButton.Title="SmartPick...";
frameCstCreation.IconAndOptionsFrame.OptionsFrame.framePushButton.smartPickPushButton.LongHelp="Individua dinamicamente i vincoli logici.";

frameCstVisu.HeaderFrame.Global.Title="Visualizzazione dei vincoli";
frameCstVisu.HeaderFrame.Global.LongHelp=
"Definisce le opzioni per la visualizzazione dei vincoli 2D";
frameCstVisu.IconAndOptionsFrame.OptionsFrame.LongHelp=
"Definisce le opzioni per la visualizzazione dei vincoli 2D";
frameCstVisu.IconAndOptionsFrame.OptionsFrame.checkCstVisu.Title="Visualizza i vincoli ";
frameCstVisu.IconAndOptionsFrame.OptionsFrame.checkCstVisu.LongHelp=
"Visualizza i seguenti tipi di vincoli.";
frameCstVisu.IconAndOptionsFrame.OptionsFrame.labelCstRefSize.Title="Dimensione di riferimento: ";
frameCstVisu.IconAndOptionsFrame.OptionsFrame.labelCstRefSize.LongHelp="Definisce la dimensione utilizzata come riferimento
per visualizzare i simboli dei vincoli. Modificandola, verrà modificata
la dimensione di tutte le rappresentazioni dei vincoli.";
frameCstVisu.IconAndOptionsFrame.OptionsFrame.editorCstRefSize.LongHelp="Definisce la dimensione utilizzata come riferimento
per visualizzare i simboli dei vincoli. Modificandola, verrà modificata
la dimensione di tutte le rappresentazioni dei vincoli.";
frameCstVisu.IconAndOptionsFrame.OptionsFrame.labelCstColor.Title="Colore dei vincoli: ";
frameCstVisu.IconAndOptionsFrame.OptionsFrame.comboCstColor.LongHelp="Definisce il utilizzato per visualizzare i simboli di vincolo.
Cambiandolo, viene modificato il colore di tutte le rappresentazioni
dei vincoli.";
frameCstVisu.IconAndOptionsFrame.OptionsFrame.labelCstColor.LongHelp="Definisce il utilizzato per visualizzare i simboli di vincolo.
Cambiandolo, viene modificato il colore di tutte le rappresentazioni
dei vincoli.";
frameCstVisu.IconAndOptionsFrame.OptionsFrame.frameCTPushButton.cstTypesPushButton.Title="Tipi di vincolo...";
frameCstVisu.IconAndOptionsFrame.OptionsFrame.frameCTPushButton.cstTypesPushButton.LongHelp="Definisce i diversi tipi\ndi vincolo da visualizzare.";


frameGeomManipDirect.HeaderFrame.Global.Title="Manipolatori ";
frameGeomManipDirect.HeaderFrame.Global.LongHelp="Indica se è necessario attivare
i manipolatori mediante un clic prima di spostare la geometria.";

frameGeomManipDirect.IconAndOptionsFrame.OptionsFrame.checkGeomManipDirect.Title="Consenti la manipolazione diretta ";
frameGeomManipDirect.IconAndOptionsFrame.OptionsFrame.checkGeomManipDirect.LongHelp="Consenti la manipolazione diretta
Indica se è necessario attivare i manipolatori mediante un clic
prima di spostare la geometria.";


frameDiagColors.HeaderFrame.Global.Title = "Colori  ";
frameDiagColors.HeaderFrame.Global.LongHelp = "Colori\nDefinisce i colori degli elementi.";
frameDiagColors.IconAndOptionsFrame.OptionsFrame.checkVisualizationDiag.Title = "Visualizzazione della diagnostica ";
frameDiagColors.IconAndOptionsFrame.OptionsFrame.checkVisualizationDiag.LongHelp = "Definisce la visualizzazione della diagnostica\nsugli elementi vincolati.";
frameDiagColors.IconAndOptionsFrame.OptionsFrame.diagColorsPushButton.Title = "Colori...";
frameDiagColors.IconAndOptionsFrame.OptionsFrame.diagColorsPushButton.LongHelp = "Definisce i colori per la diagnostica.";
frameDiagColors.IconAndOptionsFrame.OptionsFrame.labelOtherColors.Title = "Colore diverso per gli elementi";
frameDiagColors.IconAndOptionsFrame.OptionsFrame.labelOtherColors.LongHelp = "Definisce altri i colori degli elementi.";
frameDiagColors.IconAndOptionsFrame.OptionsFrame.otherColorsPushButton.Title = "Colori...";
frameDiagColors.IconAndOptionsFrame.OptionsFrame.otherColorsPushButton.LongHelp = "Definisce altri i colori degli elementi.";


// NLS push button solving mode dans onglet geometry
geoAllowGeomManip.Title="Trascinamento degli elementi";
geoAllowGeomManip.frameButtonDragElts.HeaderFrame.Global.Title = "Trascinamento degli elementi  ";
geoAllowGeomManip.frameButtonDragElts.HeaderFrame.Global.LongHelp = "Definisce le modalità di spostamento degli elementi.";
geoAllowGeomManip.frameButtonDragElts.IconAndOptionsFrame.OptionsFrame.checkDragWithEndPts.Title = "Trascina gli estremi degli elementi inseriti";
geoAllowGeomManip.frameButtonDragElts.IconAndOptionsFrame.OptionsFrame.checkDragWithEndPts.LongHelp = "Trascina gli elementi inclusi gli estremi.";
geoAllowGeomManip.frameButtonDragElts.IconAndOptionsFrame.OptionsFrame.frameDragMode.Title = "Modalità di soluzione ";//for moving elements";
geoAllowGeomManip.frameButtonDragElts.IconAndOptionsFrame.OptionsFrame.frameDragMode.frameDragModelocked.radioDragModeStd.Title = "Standard";
geoAllowGeomManip.frameButtonDragElts.IconAndOptionsFrame.OptionsFrame.frameDragMode.frameDragModelocked.radioDragModeStd.LongHelp = "Utilizza la modalità standard durante lo spostamento degli elementi.";
geoAllowGeomManip.frameButtonDragElts.IconAndOptionsFrame.OptionsFrame.frameDragMode.frameDragModelocked.radioDragModeMin.Title = "Spostamento minimo";
geoAllowGeomManip.frameButtonDragElts.IconAndOptionsFrame.OptionsFrame.frameDragMode.frameDragModelocked.radioDragModeMin.LongHelp = "Esegue uno spostamento minimo.";
geoAllowGeomManip.frameButtonDragElts.IconAndOptionsFrame.OptionsFrame.frameDragMode.frameDragModelocked.radioDragModeRel.Title = "Modalità elastica";
geoAllowGeomManip.frameButtonDragElts.IconAndOptionsFrame.OptionsFrame.frameDragMode.frameDragModelocked.radioDragModeRel.LongHelp = "Utilizza la modalità elastica.";

// Push Button SmartPick
geoSmartPick.Title="SmartPick";
geoSmartPick.frameSmartPick.HeaderFrame.Global.Title="SmartPick";
geoSmartPick.frameSmartPick.HeaderFrame.Global.LongHelp=
"SmartPick
geoSmartPick.Rileva automaticamente la posizione degli elementi geometrici 2D.";
geoSmartPick.frameSmartPick.IconAndOptionsFrame.OptionsFrame.checkSmartPickCoincid.Title="Cerchi e linee di riferimento";
geoSmartPick.frameSmartPick.IconAndOptionsFrame.OptionsFrame.checkSmartPickCoincid.LongHelp=
"Individua le circonferenze e le linee di riferimento.";
geoSmartPick.frameSmartPick.IconAndOptionsFrame.OptionsFrame.checkSmartPickAlign.Title="Allineamento ";
geoSmartPick.frameSmartPick.IconAndOptionsFrame.OptionsFrame.checkSmartPickAlign.LongHelp=
"Indica che due elementi sono allineati tra loro.";
geoSmartPick.frameSmartPick.IconAndOptionsFrame.OptionsFrame.checkSmartPickParaPerpenTang.Title="Parallelismo, perpendicolarità e tangenza ";
geoSmartPick.frameSmartPick.IconAndOptionsFrame.OptionsFrame.checkSmartPickParaPerpenTang.LongHelp=
"Indica che due elementi sono paralleli,
perpendicolari o tangenti tra loro.";
geoSmartPick.frameSmartPick.IconAndOptionsFrame.OptionsFrame.checkSmartPickHoriVerti.Title="Orizzontalità e verticalità";
geoSmartPick.frameSmartPick.IconAndOptionsFrame.OptionsFrame.checkSmartPickHoriVerti.LongHelp=
"Indica che due elementi sono orizzontali o verticali tra loro.";

// Constraints types
geoTypeCst.Title="Tipi di vincoli";
geoTypeCst.frameConstraintsTypes.HeaderFrame.Global.Title="Tipi di vincoli";
geoTypeCst.frameConstraintsTypes.HeaderFrame.Global.LongHelp="Definisce i diversi tipi\ndi vincolo da visualizzare.";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuHori.Title="Orizzontale ";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuHori.LongHelp="Orizzontale
Visualizza i vincoli di orizzontalità.";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuVerti.Title="Verticale";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuVerti.LongHelp="Verticale
Visualizza i vincoli di verticalità.";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuPara.Title="Parallelismo ";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuPara.LongHelp="Parallelismo
Visualizza i vincoli di parallelismo.";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuPerpen.Title="Perpendicolarità ";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuPerpen.LongHelp="Perpendicolarità
Visualizza i vincoli di perpendicolarità.";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuConcen.Title="Concentricità ";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuConcen.LongHelp="Concentricità
Visualizza i vincoli di concentricità.";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuCoinci.Title="Coincidenza ";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuCoinci.LongHelp="Coincidenza
Visualizza i vincoli di coincidenza.";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuTangen.Title="Tangenza ";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuTangen.LongHelp="Tangenza
Visualizza i vincoli di tangenza.";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuSym.Title="Simmetria ";
geoTypeCst.frameConstraintsTypes.IconAndOptionsFrame.OptionsFrame.checkCstVisuSym.LongHelp="Simmetria
Visualizza i vincoli di simmetria.";

// NLS de l'onglet Geometry pour le diagnostic des couleurs
geoVisuDiagColor.Title="Colori per la diagnostica";
geoVisuDiagColor.frameButtonDiagColors.HeaderFrame.Global.Title = "Colori per la diagnostica";
geoVisuDiagColor.frameButtonDiagColors.HeaderFrame.Global.LongHelp = "Colori per la diagnostica\nDefinisce i colori per la diagnostica.";
geoVisuDiagColor.frameButtonDiagColors.IconAndOptionsFrame.OptionsFrame.labelOverConsElts.Title = "Elementi sovravincolati ";
geoVisuDiagColor.frameButtonDiagColors.IconAndOptionsFrame.OptionsFrame.labelOverConsElts.LongHelp = "Elementi sovravincolati\nDefinisce il colore utilizzato per gli elementi nei modelli sovravincolati.";
geoVisuDiagColor.frameButtonDiagColors.IconAndOptionsFrame.OptionsFrame.labelIsoConsElts.Title = "Elementi isovincolati ";
geoVisuDiagColor.frameButtonDiagColors.IconAndOptionsFrame.OptionsFrame.labelIsoConsElts.LongHelp = "Elementi isovincolati\nDefinisce il colore utilizzato per gli elementi nei modelli isovincolati.";
geoVisuDiagColor.frameButtonDiagColors.IconAndOptionsFrame.OptionsFrame.labelInconsElts.Title = "Elementi non congruenti ";
geoVisuDiagColor.frameButtonDiagColors.IconAndOptionsFrame.OptionsFrame.labelInconsElts.LongHelp = "Elementi non congruenti\nDefinisce il colore utilizzato per gli elementi nei modelli non congruenti.";
geoVisuDiagColor.frameButtonDiagColors.IconAndOptionsFrame.OptionsFrame.labelNotChanElts.Title = "Elementi non modificati ";
geoVisuDiagColor.frameButtonDiagColors.IconAndOptionsFrame.OptionsFrame.labelNotChanElts.LongHelp = "Elementi non modificati\nDefinisce il colore utilizzato per gli elementi non modificati.";

// NLS de l'onglet Geometry pour Other Colors
geoOtherColor.Title="Colori degli elementi";
geoOtherColor.frameButtonOtherColors.HeaderFrame.Global.Title = "Colori  ";
geoOtherColor.frameButtonOtherColors.HeaderFrame.Global.LongHelp = "Colori\nDefinisce i colori degli elementi.";
geoOtherColor.frameButtonOtherColors.IconAndOptionsFrame.OptionsFrame.labelConstElts.Title = "Elementi di costruzione (per disegni pre-V5R11) ";
geoOtherColor.frameButtonOtherColors.IconAndOptionsFrame.OptionsFrame.labelConstElts.LongHelp = "Elementi di costruzione\nDefinisce il colore utilizzato per gli elementi di costruzione per i disegni pre-V5R11.";
geoOtherColor.frameButtonOtherColors.IconAndOptionsFrame.OptionsFrame.labelSmartPick.Title = "SmartPick ";
geoOtherColor.frameButtonOtherColors.IconAndOptionsFrame.OptionsFrame.labelSmartPick.LongHelp = "Elementi SmartPick\nDefinisce il colore utilizzato per gli elementi SmartPick.";
