//=============================================================================
//                                     CNEXT - CXR1
//                          COPYRIGHT DASSAULT SYSTEMES 1998 
//-----------------------------------------------------------------------------
// FILENAME    :    CATDrwCheckItemHeader
// LOCATION    :    DraftingUI/CNext/resources/msgcatalog/
// AUTHOR      :    ogk
// DATE        :    05.01.99
//------------------------------------------------------------------------------
// DESCRIPTION :    Resources File for NLS purpose related to the overview
//                  
//------------------------------------------------------------------------------
// COMMENTS    :	 ENGLISH VERSION
//------------------------------------------------------------------------------
//MODIFICATIONS     user  date      purpose
//   HISTORY        ----  ----      -------
//    01            lgk   14.10.02  Revision
//------------------------------------------------------------------------------


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Menu View
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//------------------------------------------------------------------------------
// Drafting' Overview
//------------------------------------------------------------------------------
CATDrwCheckItemHeader.DrwToggleOverView.Title="Panoramica del disegno";
CATDrwCheckItemHeader.DrwToggleOverView.ShortHelp="Panoramica della geometria";
CATDrwCheckItemHeader.DrwToggleOverView.Help="Visualizza un'anteprima del disegno";
CATDrwCheckItemHeader.DrwToggleOverView.LongHelp="Panoramica della geometria
Visualizza un'anteprima del disegno.";

CATDrwCheckItemHeader.CATDrwDimCheckToCenterHdr.Title="Estendi al centro";
CATDrwCheckItemHeader.CATDrwDimCheckToCenterHdr.ShortHelp="Estendi al centro";
CATDrwCheckItemHeader.CATDrwDimCheckToCenterHdr.Help="Estende al centro un raggio o una linea di quota di diametro con simbolo unico";
CATDrwCheckItemHeader.CATDrwDimCheckToCenterHdr.LongHelp="Estendi al centro
Estende al centro un raggio o una linea di quota di diametro con simbolo unico";

//------------------------------------------------------------------------------
// Change Orientation Command
//------------------------------------------------------------------------------
CATDrwCheckItemHeader.CATDrwDimCheckAutoHdr.Title="Forza la quota sull'elemento";
CATDrwCheckItemHeader.CATDrwDimCheckHorizHdr.Title="Forza dimensione orizzontale nella vista";
CATDrwCheckItemHeader.CATDrwDimCheckVertHdr.Title="Forza la quota verticale nella vista";
CATDrwCheckItemHeader.CATDrwDimCheckLinearHdr.Title="Lineare";
CATDrwCheckItemHeader.CATDrwDimCheckOffsetHdr.Title="Offset";
CATDrwCheckItemHeader.CATDrwDimCheckParallelHdr.Title="Parallelo";
CATDrwCheckItemHeader.CATDrwDimCheckTrueDimHdr.Title="Quota lunghezza effettiva";

CATDrwCheckItemHeader.CATDrwDimCheckRepHorHdr.Title="Lineare orizzontale";
CATDrwCheckItemHeader.CATDrwDimCheckRepVertHdr.Title="Lineare verticale";
CATDrwCheckItemHeader.CATDrwDimCheckRepLinearHdr.Title="Lineare";
CATDrwCheckItemHeader.CATDrwDimCheckRepIsometricHdr.Title="Vista isometrica";
CATDrwCheckItemHeader.CATDrwDimCheckRepParallelHdr.Title="Parallelo";
CATDrwCheckItemHeader.CATDrwDimCheckRepOffsetHdr.Title="Offset";


//------------------------------------------------------------------------------
// Change Angular Sector Command
//------------------------------------------------------------------------------
CATDrwCheckItemHeader.CATDrwDimCheckSector1Hdr.Title="1° quadrante";
CATDrwCheckItemHeader.CATDrwDimCheckSector2Hdr.Title="2° quadrante";
CATDrwCheckItemHeader.CATDrwDimCheckSector3Hdr.Title="3° quadrante";
CATDrwCheckItemHeader.CATDrwDimCheckSector4Hdr.Title="4° quadrante";
CATDrwCheckItemHeader.CATDrwDimCheckComplementaryHdr.Title="Complementare";

CATDrwCheckItemHeader.CATDrwCheckAssocPosRigidHdr.Title="Rigido";

CATDrwCheckItemHeader.CATDrwCalloutTextFreeCheckHdr.Title="Posiziona i testi manualmente";
CATDrwCheckItemHeader.CATDrwCalloutTextFreeCheckHdr.ShortHelp="Posizionare i testi simbolo manualmente";
CATDrwCheckItemHeader.CATDrwCalloutTextFreeCheckHdr.Help="Attiva i testi simbolo in modalità di posizionamento automatica o manuale";
CATDrwCheckItemHeader.CATDrwCalloutTextFreeCheckHdr.LongHelp="Posiziona i testi manualmente\nAttiva i testi simbolo in modalità di posizionamento automatica o manuale.";
