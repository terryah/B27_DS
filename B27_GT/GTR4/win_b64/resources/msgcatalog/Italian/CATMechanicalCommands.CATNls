ReplaceDlg_.Title="Sostituisci";

DeleteDlg_.Title="Elimina";
DeleteDlg_.MoreFrame_.AdvancedChildrenManagement_.Title="Gestione avanzata degli elementi figli";
DeleteDlg_.MoreFrame_.AdvancedChildrenManagement_.LongHelp="Specifica gli oggetti da sostituire per verificare\nche gli elementi figli interessati verranno aggiornati";

ReplaceFrame_.Replace_.Title="Sostituisci: ";
ReplaceFrame_.With_.Title="Con: ";
ReplaceFrame_.ReplaceColumn_.Title="Sostituisci";
ReplaceFrame_.WithColumn_.Title="Con";
ReplaceFrame_.CheckDelete_.Title="Elimina elementi sostituiti e Padri esclusivi";

ImpactFrame_.ImpactedElements_.Title="Elementi sottoposti ad eliminazione";
ImpactFrame_.ImpactedElements_.LongHelp="Elenca i figli che sono stati eliminati";
ImpactFrame_.ElementColumn_.Title="Elementi";
ImpactFrame_.StatusColumn_.Title="Stato";
ImpactFrame_.DeleteUndelete_.Title="Elimina/Annulla eliminazione";
ImpactFrame_.DeleteUndeleteAll_.Title="Elimina tutto/Annulla eliminazione";

DeleteFrame_.SepList_.Title="Selezione";
DeleteFrame_.SepList_.LongHelp="Specifica l'oggetto da eliminare";
DeleteFrame_.SepParents_.Title="Padri";
DeleteFrame_.DeleteParents_.Title="Elimina Padri esclusivi";
DeleteFrame_.SepChildren_.Title="Figli";
DeleteFrame_.DeleteChildren_.Title="Elimina tutti i figli";
DeleteFrame_.SepAggregated_.Title="Aggregati";
DeleteFrame_.DeleteAggregated_.Title=" Elimina gli elementi aggregati";

DeleteConfirmDlg_.Answer_.Title="Si � certi di voler eliminare questi oggetti?";

CATMechanicalCommands.OK="OK";
CATMechanicalCommands.KO="KO";
CATMechanicalCommands.DEL="Elimina";
CATMechanicalCommands.ALWAYSDEL="Eliminazione automatica";
CATMechanicalCommands.Keep="Conserva";
CATMechanicalCommands.KeepNeedReplace=" Conserva, da sostituire";
CATMechanicalCommands.KeepPropagate="Conserva, propaga";
CATMechanicalCommands.KeepSolid="Conserva, solido";
CATMechanicalCommands.KeepUsedBySolid="Conserva, utilizzato da solido";
CATMechanicalCommands.YES="S�";
CATMechanicalCommands.NO="No";
CATMechanicalCommands.Error="Errore";
CATMechanicalCommands.Warning="Avvertenza";
CATMechanicalCommands.More="Altro >>";
CATMechanicalCommands.Less="<<Riduci";
CATMechanicalCommands.NoSelection="Nessuna selezione";
CATMechanicalCommands.InvalidSelection="Selezione non valida";
CATMechanicalCommands.UselessSelection="Nessuno degli elementi che puntano pu� fare riferimento all'elemento selezionato.
Verr� generato un loop Padre/Figlio o un'interruzione nelle regole di ordinamento.";

CATMechanicalCommands.CATMcoDeleteCmd.UndoTitle="Elimina";
CATMechanicalCommands.CATMcoDeleteCmd.ReplaceState.Message="Selezionare un oggetto";
CATMechanicalCommands.CATMcoDeleteCmd.InitialState.Message="Selezionare le opzioni necessarie";
CATMechanicalCommands.CATMcoDeleteCmd.ConfirmState.Message="Selezionare S� o No";
CATMechanicalCommands.CATMcoDeleteCmd.ConfirmState2.Message="Selezionare gli elementi da conservare";

CATMechanicalCommands.CATMcoReplaceCmd.UndoTitle="Sostituisci";
CATMechanicalCommands.CATMcoReplaceCmd.ReplaceState.Message="Selezionare un oggetto";

Replace.NotAccept=" Questo elemento non pu� sostituire l'elemento selezionato. ";
Replace.NotPossible="Non sostituibile";
Replace.WarningImpact=" : questo elemento crea elementi geometrici secondari che verranno utilizzati successivamente.\nPer aggiornare la procedura, � necessario sostituirli.";
Replace.WarningPointed="L'elemento selezionato non � puntato";
Replace.CancelReplace="Annulla sostituzione";

Delete.NotDelete="Non � possibile eliminare l'elemento.";
Delete.NoUseless="Non � stato trovato nessun elemento inutile.";
Delete.Elements="elementi";
Delete.Deleted="eliminati";
Delete.Kept="conservati";
Delete.Index="Indice";

DeleteConfirmDlg_.Select_.SelectList_.LongHelp="Elimina = L'elemento verr� eliminato\nConserva = L'elemento verr� conservato\nConserva, propaga = L'elemento viene utilizzato da un elemento che si desidera conservare,\n                    Pertanto, non � possibile eliminarlo.\nConserva, usato da solido = L'elemento viene utilizzato da un solido.\n                            Il solido non pu� essere eliminato in questo comando.\n                            Pertanto, non � possibile eliminare l'elemento.";

DeleteConfirmDlg_.Select_.Message_.Title="Utilizzare il menu contestuale sulla riga selezionata per conservare un elemento.";

Delete.ConfirmDeletion="Conferma eliminazione";
Delete.DeleteUseless="Elimina elementi inutili";
Delete.ConfirmDeletionUndoLog="L'eliminazione di questa feature non pu� essere annullata.\nContinuare?";
ColonMsg=": ";

ReplaceViewerDlg_.Title="Visualizzatore di sostituzioni";
ReplaceViewerDlg_.ReplaceViewerFrame_.LinkCheck_.Title="Visualizzatori di collegamenti";

Delete.Publication="Alcuni degli elementi eliminati sono pubblici: ";
Delete.Published=" � reso pubblico come ";
Delete.Continue="Si desidera continuare con la cancellazione?";

Replace.UselessPointingDeleted="inutile: elementi figli eliminati";
Replace.UselessCycle="inutile: elementi figli in ciclo";
Replace.UselessNotPointed="inutile: non puntato";
Replace.NotReplaced=" non sostituito";

Exit.Continue="Si desidera continuare?";

Replace.WarningParentChildren="La sostituzione multipla non � consentita per gli elementi con relazioni Padre/Figli.\n\n Sostituire ciascun elemento individualmente.";
Replace.PointingElements=" elementi che puntano: ";
Replace.PointingElement=" elemento che punta: ";
Replace.Continue="Si desidera continuare con la sostituzione?";

Replace.Exit="Si desidera uscire dal comando di sostituzione\n anche se alcuni elementi non sono stati sostituiti?";

Replace.Publication="Alcuni degli elementi da sostituire sono pubblici: ";
Replace.WarningSolid=": sostituzione non consentita su un elemento solido.";
Replace.WarningPointedCurrent="L'elemento selezionato non � utilizzato da alcun elemento\nposizionato dopo l'oggetto di lavoro.\nModificare l'oggetto di lavoro.";
Replace.WarningNonUpdated=": sostituzione non consentita su un elemento non aggiornato.";
Replace.BreakesRules=": le regole di ordinamento non sono rispettate.";

CATMechanicalCommands.DevaluatedLinks="Per rispettare le regole di ordinamento, � necessario un reinstradamento.";
CATMechanicalCommands.NoLinkReplace="Per rispettare le regole di ordinamento e non creare errori del ciclo di aggiornamento,\n la sostituzione non ha avuto alcun effetto.";

CATMechanicalCommands.CATMmuKeepCmd.UndoTitle="Elimina tutto tranne";
CATMechanicalCommands.CATMmuKeepCmd.Title="Elimina tutto tranne...";
CATMechanicalCommands.CATMmuKeepCmd.State1.Message="Scegliere le feature da conservare.";
CATMechanicalCommands.CATMmuKeepCmd.State2.Message="Premere OK per eliminare tutte le feature elencate.";

KeepPanel.Title="Elimina tutto tranne...";
KeepPanel.OKButton="OK";
KeepPanel.KeepList.Col1Title="#";
KeepPanel.KeepList.Col2Title="Feature";
KeepPanel.KeepList.Col3Title="Stato";
KeepPanel.KeepList.Col4Title="Tipo";

KeepPanel.Preset.KeepSolidButton=" feature solido";
KeepPanel.Preset.KeepPublishedButton=" feature pubblicate";

KeepPanel.KeepFrame.KeepMultiList.ShortHelp="Utilizzare il menu contestuale di una riga selezionata per modificare uno stato.";
KeepPanel.KeepFrame.KeepMultiList.LongHelp="Selezionare una riga e utilizzare il relativo menu contestuale per modificare lo stato di una feature.
In tal modo, verranno disabilitati i pulsanti precedenti.";

KeepPanel.KeepFrame.KeepSolidButton.ShortHelp="Se selezionata, per impostazione predefinita vengono conservate tutte le feature solido";
KeepPanel.KeepFrame.KeepSolidButton.LongHelp="Se selezionata, per impostazione predefinita vengono conservate tutte le feature solido.";
KeepPanel.KeepFrame.KeepPublishedButton.LongHelp="Se selezionata, per impostazione predefinita vengono conservate tutte le feature pubblicate.";
KeepPanel.KeepFrame.KeepPublishedButton.ShortHelp="Se selezionata, per impostazione predefinita vengono conservate tutte le feature pubblicate";

KeepConfPanel.Title="Conferma eliminazione";
KeepConfPanel.YES="OK";
KeepConfPanel.NO="Annulla";
KeepConfPanel.NothingToRemove="Nessun elemento da eliminare";
KeepConfPanel.Label="Feature che verranno eliminate:";
KeepConfPanel.KeepConfMultiList.ShortHelp="Tutte queste feature verranno eliminate dopo la conferma";
KeepConfPanel.KeepConfMultiList.LongHelp="Tutte queste feature verranno eliminate dopo la conferma.";

KeepStatus.KeepPropagated="Conserva (propagato)";
KeepStatus.Keep="Conserva";
KeepStatus.Erase="Elimina";
KeepStatus.Reset="Ripristina";
KeepStatus.ResetScanableComp="Reimposta tutti i componenti senza riferimento";
KeepStatus.KeepScanableComp="Conserva tutti i componenti";
KeepStatus.KeepScanablePropagatedComp="Conserva tutti i componenti (propagato)";

KeepStatus.ErasePartBodyComp="";

KeepStatus.NoSwitchStatus="Impossibile eseguire lo scambio. � indicato da una feature conservata.";

InternalCopyPanel.InternalCopyLabel="Copia interna ";
InternalCopyPanel.OldReferenceLabel="Riferimento precedente ";
InternalCopyPanel.NewReferenceLabel="Nuovo riferimento ";
InternalCopyPanel.ReplaceViewerBox=" Consenti replug manuale";

FeaturesToUpgrade="Feature da aggiornare";
NoFeatureToUpgradeFound="Non sono state trovate feature da aggiornare";
Refresh="Aggiorna";















