//=============================================================================
//                                     CNEXT - CXR1
//                          COPYRIGHT DASSAULT SYSTEMES 1997 
//-----------------------------------------------------------------------------
// FILENAME    :    CATDrwDimFrameDimensionLineFormat
// LOCATION    :    DraftingIntUI/CNext/resources/msgcatalog
// AUTHOR      :    jmt
// DATE        :    Nov. 03 1997
//------------------------------------------------------------------------------
// DESCRIPTION :    Resources File for NLS purpose related to Drafting WorkShop
//                  
//------------------------------------------------------------------------------
// COMMENTS    :
//------------------------------------------------------------------------------
//MODIFICATIONS     user  date      purpose
//   HISTORY        ----  ----      -------
//------------------------------------------------------------------------------

ComboRepresentation0="Regular" ;
ComboRepresentation1="Two Parts";
ComboRepresentation2="With Leader";
ComboRepresentation3="Leader one Part";
ComboRepresentation4="Leader two Parts";

ComboReferenceLine0="Screen";
ComboReferenceLine1="View";
ComboReferenceLine2="Dimension Line";

ComboOrientationLine0="Parallel";
ComboOrientationLine1="Perpendicular";
ComboOrientationLine2="Fixed Angle";

ComboOrientationLine00="Horizontal";
ComboOrientationLine01="Vertical";
ComboOrientationLine02="Fixed Angle";

NO_SYMBOL="No Symbol";
ARROW="Open Arrow";
CLOSED_ARROW="Outlined Arrow";
FILLED_ARROW="Filled Arrow";
SYMETRIC_ARROW="Transparent Arrow";
SLASH="Slash";
CIRCLE="Outlined Circle";
FILLED_CIRCLE="Filled Circle";
SCORED_CIRCLE="Transparent Circle";
CROSSED_CIRCLE="Crossed Circle";
TRIANGLE="Outlined Triangle";
FILLED_TRIANGLE="Filled Triangle";
PLUS="Plus";
CROSS="Cross";
DOUBLE_FILLED_ARROW="Double Filled Arrow";

ComboSymbolModeLine0="Auto";
ComboSymbolModeLine1="Inside";
ComboSymbolModeLine2="Outside";
ComboSymbolModeLine3="Symbol 1 In / Symbol 2 Out";
ComboSymbolModeLine4="Symbol 1 Out / Symbol 2 In";

ComboExtendToCenterMode0="From standard";
ComboExtendToCenterMode1="Till center";
ComboExtendToCenterMode2="Not till center";

FrameSymbolTitle.LabelSymbolTitle.Title = "Symbols ";

FrameDimensionLine.SubFrameDimLineMain.LabelRepresentation.Title = "Representation: ";
FrameDimensionLine.SubFrameDimLineMain.LabelColorLine.Title = "Color: ";
FrameDimensionLine.SubFrameDimLineMain.LabelThicknessLine.Title = "Thickness: ";


SubFrameDimLeader.SubFrameDimLineLeaderAngle.Title = "Leader";
SubFrameDimLeader.SubFrameDimLineLeaderAngle.LabelLeaderAngle.Title = "Angle: ";
SubFrameDimLeader.SubFrameDimLineLeaderAngle.LabelLeaderLength.Title = "Length: ";

SubFrameDimLeader.SubFrameDimLine2Part.Title = "Second Part";
SubFrameDimLeader.SubFrameDimLine2Part.LabelReference.Title = "Reference: ";
SubFrameDimLeader.SubFrameDimLine2Part.LabelOrientation.Title = "Orientation: ";
SubFrameDimLeader.SubFrameDimLine2Part.LabelAngle.Title = "Angle: ";

FrameDimensionLine.SubFrameDimLineMain.LabelExtendToCenter.Title = "Extension:";


FrameSymbol.SubFrameSymbol.LabelSymbol.Title = "Symbol 1";
FrameSymbol.SubFrameSymbol.LabelShape.Title = "Shape: ";
FrameSymbol.SubFrameSymbol.LabelThicknessSymbol.Title = "Thickness: ";
FrameSymbol.SubFrameSymbol.LabelColorSymbol.Title = "Color: ";
FrameSymbol.SubFrameSymbol.LabelSymbol2.Title = "Symbol 2";
FrameSymbol.SubFrameSymbol.LabelSymbol3.Title = "Leader Symbol";

FrameSymbol.SubFrameSymbolMore.LabelSymbolMode.Title = "Reversal: ";
FrameSymbol.SubFrameSymbolMore.CheckButtonDisplaySymb2.Title = "Display Symbol 2";

FrameSymbol.LabelSymbol.Title = "Symbol 1";
FrameSymbol.LabelShape.Title = "Shape: ";
FrameSymbol.LabelThicknessSymbol.Title = "Thickness: ";
FrameSymbol.LabelColorSymbol.Title = "Color: ";
FrameSymbol.LabelSymbol3.Title = "Leader Symbol";

FrameSymbol.LabelSymbolMode.Title = "Reversal: ";
FrameSymbol.CheckButtonDisplaySymb2.Title = "Symbol 2";

FrameDimensionLine.SubFrameDimLineMain.ComboRepresentation.LongHelp=
"Provides a representation for the dimension line.";
FrameDimensionLine.SubFrameDimLineMain.ComboColorLine.LongHelp=
"Defines the color of the dimension line.";
FrameDimensionLine.SubFrameDimLineMain.ComboThicknessLine.LongHelp=
"Defines the thickness of the dimension line.";

FrameDimensionLine.SubFrameDimLine2Part.ComboReference.LongHelp=
"Provides a reference for positionning the secondary part(s) of the dimension line.";
FrameDimensionLine.SubFrameDimLine2Part.ComboOrientation.LongHelp=
"Defines an orientation for the secondary part(s) of the dimension line in relation to its \"Reference\".";
FrameDimensionLine.SubFrameDimLine2Part.SpinnerAngle.LongHelp=
"Defines an angle for the secondary part(s) of the dimension line in relation to its \"Reference\".";

FrameSymbol.SubFrameSymbol.ComboSymbol.LongHelp=
"Defines the shape of the symbols.";
FrameSymbol.SubFrameSymbol.ComboSymbol2.LongHelp=
"Defines the shape of the symbols.";
FrameSymbol.SubFrameSymbol.ComboSymbol3.LongHelp=
"Defines the shape of the symbols.";

FrameSymbol.SubFrameSymbol.ComboThicknessSymbol.LongHelp=
"Defines a thickness for the symbols.";
FrameSymbol.SubFrameSymbol.ComboThicknessSymbol2.LongHelp=
"Defines a thickness for the symbols.";
FrameSymbol.SubFrameSymbol.ComboThicknessSymbol3.LongHelp=
"Defines a thickness for the symbols.";

FrameSymbol.SubFrameSymbol.ComboColorSymbol.LongHelp=
"Defines a color for the symbols.";
FrameSymbol.SubFrameSymbol.ComboColorSymbol2.LongHelp=
"Defines a color for the symbols.";
FrameSymbol.SubFrameSymbol.ComboColorSymbol3.LongHelp=
"Defines a color for the symbols.";

FrameSymbol.SubFrameSymbolMore.ComboSymbolMode.LongHelp=
"Reverses symbol position in relation to extension lines.";
