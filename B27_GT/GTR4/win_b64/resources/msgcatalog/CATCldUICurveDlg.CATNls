// COPYRIGHT DASSAULT SYSTEMES PROVENCE 2006
// =============================================================================
// CATCldUICurveDlg.CATNls

// =============================================================================
// 20-Dec-2006 CQO: Adaptation
// 08-Nov-2006 YSN: Creation
// =============================================================================
Title="Curve On Mesh";

MainFrame.CloudFrame.CloudLabel.Title="Support Mesh ";
MainFrame.CloudFrame.CloudLabel.LongHelp="Displays the mesh selected as support for the curve.";
MainFrame.CloudFrame.CloudSelector.Title="Support Mesh ";
MainFrame.CloudFrame.CloudSelector.LongHelp="Displays the mesh selected as support for the curve.";

//MainFrame.ProjFrame.Title="Projection Mode";
//MainFrame.ProjFrame.ProjViewDirB.Title="Along View Direction";
//MainFrame.ProjFrame.ProjViewDirB.LongHelp="The curve is projected at the support mesh along the view direction (at the moment of picking the point) for each selected point.";
//MainFrame.ProjFrame.ProjNormalB.Title="Normal";
//MainFrame.ProjFrame.ProjNormalB.LongHelp="The curve is projected normal to the support mesh.";

MainFrame.ParametersFrame.Title="Parameters";
MainFrame.ParametersFrame.LongHelp="Sets the curve creation parameters.";
MainFrame.ParametersFrame.Tolerance.Title="Tolerance";
MainFrame.ParametersFrame.Tolerance.LongHelp="Sets the smoothing tolerance.";
MainFrame.ParametersFrame.ToleranceEditor.LongHelp="Sets the smoothing tolerance.";
MainFrame.ParametersFrame.MaxOrder.Title="Max. Order";
MainFrame.ParametersFrame.MaxOrder.LongHelp="Sets the maximum order of the curve.";
MainFrame.ParametersFrame.MaxOrderEditor.LongHelp="Sets the maximum order of the curve.";
MainFrame.ParametersFrame.MaxSegments.Title="Max. Segments";
MainFrame.ParametersFrame.MaxSegments.LongHelp="Sets the maximum number of segments of the curve.";
MainFrame.ParametersFrame.MaxSegmentsEditor.LongHelp="Sets the maximum number of segments of the curve.";
MainFrame.ParametersFrame.SplitAngle.Title="Split Angle";
MainFrame.ParametersFrame.SplitAngle.LongHelp="If the angle between two consecutive segments formed by picked points is greater than or equal to this value, by default the continuity at this point is tangent.";
MainFrame.ParametersFrame.SplitAngleEditor.LongHelp="If the angle between two consecutive segments formed by picked points is greater than or equal to this value, by default the continuity at this point is tangent.";

MainFrame.VisuFrame.Title="Display";
MainFrame.VisuFrame.CurvatureAnalysisCheckB.Title="Curvature Analysis";
MainFrame.VisuFrame.CurvatureAnalysisCheckB.ShortHelp="Analyzes the curvature of the resulting curves.";
MainFrame.VisuFrame.CurvatureAnalysisCheckB.LongHelp="Analyzes the curvature of the resulting curves.";
MainFrame.VisuFrame.DevMaxCheckB.Title="Maximum deviation";
MainFrame.VisuFrame.DevMaxCheckB.ShortHelp="Displays the maximum deviation.";
MainFrame.VisuFrame.DevMaxCheckB.LongHelp="Displays the maximum deviation.";
MainFrame.VisuFrame.SegmentationCheckB.Title="Segmentation";
MainFrame.VisuFrame.SegmentationCheckB.ShortHelp="Displays the order and the number of segments.";
MainFrame.VisuFrame.SegmentationCheckB.LongHelp="Displays the order and the number of segments.";
MainFrame.VisuFrame.RunningPtCheckB.Title="Hide curve preview";
MainFrame.VisuFrame.RunningPtCheckB.LongHelp="Allows you to hide the preview of the curve.";
MainFrame.VisuFrame.Bid.Title="   ";

MainFrame.QuickModeCheckB.Title="Curve Optimization";
MainFrame.QuickModeCheckB.LongHelp="Curve is optimized in regards of order and number of segments. This option makes the computation more greedy and has an influence over performance";
