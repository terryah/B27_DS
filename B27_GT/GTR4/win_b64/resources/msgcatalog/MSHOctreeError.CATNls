
ERRO0001.Request    = "Unable to allocate memory.";
ERRO0001.Diagnostic = "Unable to allocate memory for meshing.";
ERRO0001.Advice     = "Mesh the part with a greater mesh size or mesh sag.";

ERRO0002.Request="Internal error";
ERRO0002.Diagnostic="Parameter: /P01
/P02";
ERRO0002.Advice		= "Contact your local support.";

ERRO0003.Request	  = "Run cancelled.";
ERRO0003.Diagnostic = "Cancel button press.";
ERRO0003.Advice     = "Check parameters.";

ERRO0032.Request	= "Mesh size too small.";
ERRO0032.Diagnostic	= "The mesh size /P11  must be greater or equal to /P12 ";
ERRO0032.Advice		= "Increase mesh size.";
 
WARN0037.Request	= "Edges can not be split.";
WARN0037.Diagnostic = "Impossible to split /P01 edges on the highlighted
geometry with split local condition.";
WARN0037.Advice 	= "Check local configuration.";
 
ERRO0103.Request	= "Detail suppression failed for face.";
ERRO0103.Diagnostic = "Contour /P01 of face : /P21
Contains only one boundary curve of length greater than
the identical curves tolerance.";
ERRO0103.Advice		= "Modify either the contour of the face or the standard
tolerance model, so as to either zero or at least two
curves of length greater than the tolerance for this
contour.";

ERRO0104.Request    = "Imposed point /P01 failed";
ERRO0104.Diagnostic = "Imposed point at location (/P01 /P02 /P03 ) is outside meshed geometry.";
ERRO0104.Advice     = "Move point on geometry or delete imposed point.";

ERRO0105.Request	= "Imposed point failed.";
ERRO0105.Diagnostic = "Impossible to imposed a node on point:
/P21
support  : /P22";
ERRO0105.Advice		= "Check if the point if not too close of the boundary of the support
or try to mesh more fine around the point.";

ERRO0107.Request    = "Imposed point /P01 failed";
ERRO0107.Diagnostic = "Imposed point at location (/P01 /P02 /P03 ) is doubly defined or is already in geometry.";
sERRO0107.Advice		= "Suppress the duplicate imposed point.";

ERRO0108.Request	= "Surface mesh connection failed.";
ERRO0108.Diagnostic = "The surface connection between imposed mesh
on curve and face mesh failed :
curve : /P21
face  : /P22";
ERRO0108.Advice		= "Modify the mesh on the highlighted geometry
or try to remesh with a slightly different global size.";

ERRO0109.Request	= "Volume mesh connection failed.";
ERRO0109.Diagnostic = "The volume connection between imposed mesh on face and
volume mesh failed :
face : /P21";
ERRO0109.Advice		= "Modify the imposed mesh on the highlighted geometry
or try to remesh with a slightly different local size.";
 
ERRO0110.Request	= "Invalid imposed mesh on face or curve.";
ERRO0110.Diagnostic = "The mesh imposed on : /P21
intersects the existing mesh on : /P22";
ERRO0110.Advice		= "Modify the imposed mesh on the highlighted geometry.";
 
ERRO0111.Request	= "Invalid imposed mesh on face.";
ERRO0111.Diagnostic	= "The imposed mesh is self-intersected on : /P21";
ERRO0111.Advice	= "Modify the imposed mesh on the highlighted geometry.";
 
ERRO0112.Request	= "Imposed mesh on face not complete.";
ERRO0112.Diagnostic	= "The imposed mesh must fully cover the face : /P21";
ERRO0112.Advice	= "Modify the imposed mesh on the highlighted geometry.";

ERRO0114.Request	= "Impossible mesh on closed curve : /P21";
ERRO0114.Diagnostic	= "The mesh connexion is impossible on closed curve :
/P21";
ERRO0114.Advice		= "Possible bypass :
- Break the curve and remesh it.";

ERRO0115.Request	= "Incompatible local specifications.";
ERRO0115.Diagnostic	= "/P31 and /P32 are applied to the same geometry :
/P21";
ERRO0115.Advice		= "Delete the unnecessary local specifications.";

ERRO0117.Request	= "Impossible mesh connexion on 1d geometry.";
ERRO0117.Diagnostic	= "Impossible to connect the imposed mesh on geometry :
/P21";
ERRO0117.Advice		= "Check the mesh around the circle :
- mesh to large compared with the size of geometry.
- bad associativity of the faces bounding the edge.";
 
WARN0127.Request	= "Bad mesh continuity.";
WARN0127.Diagnostic	= "Bad angular continuity :
- max angle = /P11 degrees
- geometry  = /P21";
WARN0127.Advice		= "Possible by-pass :
- modify the global size."; 

WARN0128.Request     = "Sag constraint is not respected.";
WARN0128.Diagnostic  = "The sag constraint cannot be respected in the whole part 
because the geometry is too sharp or too curved, or the value of the �Min. size for sag specs�
parameter (in the Others tab) is too large.
The number of edges with a bad sag = /P01 which represents /P11 % of total 
edges. The red point indicates the edge with the worst sag.";
WARN0128.Advice     = "To respect the sag constraint and, therefore,
 to refine the mesh, reduce the value of the �Min. size for sag specs� parameter.";

ERRO1003.Request	= "Invalid topology.";
ERRO1003.Diagnostic	= "Invalid topology
geometric elements topologically unlinked are in contact :
/P21
/P22";
ERRO1003.Advice		= "Modify the highlighted geometry or
try to remesh with a slightly different global size.";
 
ERRO1004.Request	= "Interference or tangency detected.";
ERRO1004.Diagnostic	= "Interference or tangency is detected between the following
geometric elements :
/P21
/P22";
ERRO1004.Advice		= "Modify the highlighted geometry or
try to remesh with a slightly different global size.";
 
ERRO1005.Request	= "Impossible to mesh.";
ERRO1005.Diagnostic	= "Impossible to mesh
the following geometric elements to mesh are in contact :
/P21 ON /P23
/P22 ON /P24";
ERRO1005.Advice		= "Modify the highlighted geometry or mesh separatly.";
 
ERRO1008.Request	= "A miss intersection is detected.";
ERRO1008.Diagnostic	= "A miss intersection is detected on geometry :
/P21";
ERRO1008.Advice		= "Try to mesh with an other global size.";

ERRO1010.Request	= "Returned or negative element.";
ERRO1010.Diagnostic	= "Returned or negative element in the area of the red circle.";
ERRO1010.Advice		= "Check and modify the geometry around the circle or
try to remesh with a slightly different global size.";
 
ERRO1012.Request	= "Impossible to mesh.";
ERRO1012.Diagnostic	= "Impossible to mesh the geometry in the area of the red circle.";
ERRO1012.Advice		= "Check and modify the geometry around the circle or
try to remesh with a slightly different global size.";
 
ERRO1013.Request	= "Impossible to mesh on geometry.";
ERRO1013.Diagnostic	= "Impossible to mesh the geometry around the red circle :
/P21";
ERRO1013.Advice		= "Check and modify the geometry around the circle or
try to remesh with a slightly different global size.";

ERRO1016.Request	= "Problem occurred on geometric entity.";
ERRO1016.Diagnostic	= "Problem occurred on :
/P21";
ERRO1016.Advice		= "Modify the highlighted geometry or
try to remesh with a slightly different global size.";
 
ERRO1018.Request	= "Impossible to connect.";
ERRO1018.Diagnostic	= "Impossible to connect mesh in the area of the red circle.";
ERRO1018.Advice		= "Check and modify the imposed mesh around the circle or
try to remesh with a slightly different global size.";

ERRO1100.Request	= "Too many errors !";
ERRO1100.Diagnostic = "Too many errors have been detected...";
ERRO1100.Advice		= "Check the geometry definition or try to remesh
with slightly different parameters.";

ERRO1120.Request	= "Invalid topology.";
ERRO1120.Diagnostic	= "Geometric faces connected by edge with same matter side.";
ERRO1120.Advice		= "Check faces orientation.";

