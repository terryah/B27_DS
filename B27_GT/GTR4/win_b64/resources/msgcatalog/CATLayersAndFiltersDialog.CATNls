Title="Layers and Filters";

_TabContainer._CurrentTabPage.Title="Current";
_TabContainer._CurrentTabPage._CurrentLayerLabel.Title="Current layer:";
_TabContainer._CurrentTabPage._CurrentFilterLabel.Title="Current Filter:";

_TabContainer._FiltersTabPage.Title="Filters";
_TabContainer._FiltersTabPage.FiltersListLabel.Title="Filters";
_TabContainer._FiltersTabPage._CreateFilterPB.Title="Create";
_TabContainer._FiltersTabPage._ModifyFilterPB.Title="Modify";
_TabContainer._FiltersTabPage._DeleteFilterPB.Title="Delete";

_TabContainer._LayersTabPage.Title="Layers";
_TabContainer._LayersTabPage._AddLayerPB.Title="Add";
_TabContainer._LayersTabPage._RemoveLayerPB.Title="Remove";
_TabContainer._LayersTabPage._ApplyLayerPB.Title="Apply";

LayersList.Column1.Title   = "Num";
LayersList.Column2.Title   = "Name";
LayersList.Column3.Title   = "Comment";
Filter.None.Name           = "No filter applied";
Filter.All.Name            = "All layers visible";
Filter.Laycur.Name         = "Only current layer visible";
Layer.General.Name         = "General";
Layer.General.Comment      = "Default layer";
LayersList.Visible.Title   = "Visible layers";
LayersList.Unvisible.Title = "Unvisible layers";
LayersList.Default.Title   = "Filter characteristics";
LayersList.Combination.Title = "Combination";

CurrentLayer.DefaultName   = "Default name";
CurrentLayer.DefaultComment= 
  "Layer automatically created because current layer was not found";

_TabContainer._CurrentTabPage._LayersCombo.LongHelp=
"Current layer name.
This combo lists all the existing layers.
Selecting a layer in this list
changes the current layer.
Selecting an object in a graphic viewer
sets the corresponding layer as current.";
_TabContainer._CurrentTabPage._FiltersCombo.LongHelp=
"Current filter name.
This combo lists all the existing filters.
Selecting a filter in this list
changes the current filter.";
_TabContainer._CurrentTabPage._CurrentLayerNumberEditor.LongHelp=
"Current layer number.
Entering the number of an existing layer
sets this layer as the current layer.";

_TabContainer._FiltersTabPage._FiltersList.LongHelp=
"List of filters.
Lists all the existing layer filters.
Selecting a filter in this list
displays its characteristics.
The three first filters in this list are
standard: you can not modify or delete them.";
_TabContainer._FiltersTabPage._FilterLayersList.LongHelp=
"Characteristics of the selected filter.
Displays the way the selected filter is composed:
list of visible or unvisible layers,
or logical combination of layers and filters.";
_TabContainer._FiltersTabPage._CreateFilterPB.LongHelp=
"Create a new filter.";
_TabContainer._FiltersTabPage._ModifyFilterPB.LongHelp=
"Modify the selected filter.
The current filter can not be modified.";
_TabContainer._FiltersTabPage._DeleteFilterPB.LongHelp=
"Delete the selected filter.
The current filter can not be deleted.";

_TabContainer._LayersTabPage._LayersList.LongHelp=
"List of layers.
Selecting a layer highlights its objects (if visible).
Selecting an object in a graphic viewer selects
the corresponding layer in this list.";
_TabContainer._LayersTabPage._AddLayerPB.LongHelp=
"Add a layer to the list.
Enter the characteristics of the layer
before using this command.";
_TabContainer._LayersTabPage._RemoveLayerPB.LongHelp=
"Remove the selected layer from the list.";
_TabContainer._LayersTabPage._ApplyLayerPB.LongHelp=
"Apply the selected layer to objects.
Select this button and then the objects.";
_TabContainer._LayersTabPage._LayerNumberEditor.LongHelp=
"Number of the selected layer.
Enter a new number to add a layer to the list.";
_TabContainer._LayersTabPage._LayerNameEditor.LongHelp=
"Name of the selected layer.
Enter a new name to add a layer to the list
or to modify the name of the selected layer.";
_TabContainer._LayersTabPage._LayerCommentEditor.LongHelp=
"Comment of the selected layer.
Enter a new comment to modify
the comment of the selected layer.";

Keyword.SetSeparator    = ",";
Keyword.OpenExpression  = "(";
Keyword.CloseExpression = ")";

DocumentTypeError = "This command is not implemented for this document type.";
