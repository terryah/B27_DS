
// COPYRIGHT DASSAULT SYSTEMES 2000

//
// NLS Message for the CATMacroEditor Application window
//
 
Editor.Title                     	= "Macros Editor v1.0";

// Menu Management

MacroEditorBarMenu.File.Title = "File";
MacroEditorBarMenu.File.New.Title = "New";
MacroEditorBarMenu.File.Open.Title = "Open...";
MacroEditorBarMenu.File.Save.Title = "Save";
MacroEditorBarMenu.File.SaveAs.Title = "Save As...";
MacroEditorBarMenu.File.Exit.Title = "Exit";

MacroEditorBarMenu.Edit.Title = "Edit";
MacroEditorBarMenu.Edit.GoToLine.Title = "Go To Line...";
MacroEditorBarMenu.Edit.Find.Title = "Find...";

// Status Bar

StatusBar.Line							= "Line : ";
StatusBar.Column						= "Column : ";

// Tool Bar

MacroEditorToolBar.Title					= "Macros Editor";
MacroEditorToolBar.New.Title			= "New";
MacroEditorToolBar.New.ShortHelp	= "New file";
MacroEditorToolBar.Open.Title			= "Open";
MacroEditorToolBar.Open.ShortHelp	= "Open file";
MacroEditorToolBar.Save.Title			= "Save";
MacroEditorToolBar.Save.ShortHelp	= "Save file";

// Browser

OpenDlgFile.Title						= "Open";
SaveAsDlgFile.Title					= "Save As";

TypeDescription.CATScript		= "Basic Script Files (*.CATScript)";
TypeDescription.Text				= "Text Files (*.txt)";
TypeDescription.AllFiles		= "All Files (*)";

// Prompt For Save

PromptForSave.Title					= "Macros Editor";
PromptForSave.Text					= "The text in the /p1 file has changed.\nDo you want to save the changes?";

// Prompt For Create

PromptForCreate.Title					= "Macros Editor";
PromptForCreate.Text					= "The file /p1 does not exist. Do you want to create it?";

// Go To Line Input

GoToLineInputDlg.Title										= "Go to line";
GoToLineInputDlg.InputLineLabel.Title			=	"Line number : ";
GoToLineInputDlg.InputLineLabel.LongHelp	= "Provides a space for you to enter the line number you want to go to.";
GoToLineInputDlg.InputLineEditor.LongHelp	= "Provides a space for you to enter the line number you want to go to.";

// Find Input

FindInputDlg.Title																			= 	"Find";
FindInputDlg.BOK.Title																	= 	"Find Next";
FindInputDlg.BCancel.Title															= 	"Close";
FindInputDlg.FindLabel.Title														= 	"Find what : ";
FindInputDlg.FindLabel.LongHelp													= 	"Provides a space for you to enter the text you want to find.";
FindInputDlg.FindEditor.LongHelp												= 	"Provides a space for you to enter the text you want to find.";
FindInputDlg.GroupRadioButtons.Title										= 	"Direction";
FindInputDlg.GroupRadioButtons.LongHelp									= 	
"Specifies which direction to search starting from the cursor in the document. 
 . Choose Up to search backward, toward the beginning of the document.
 . Choose Down to search forward, toward the end of the document.";
FindInputDlg.GroupRadioButtons.UpRadioButton.Title			= 	"Up";
FindInputDlg.GroupRadioButtons.DownRadioButton.Title		= 	"Down";

// FindErrorWindow

FindErrorWindow.Title						= 	"Macros Editor";
FindErrorWindow.Text						= 	"Cannot find the string \"/p1\".";

// File Error Management

ErrorWindow.Title 					= "Error in /p1";

FileError.TooManyOpenFiles	= "Too many files are open.";
FileError.FileExists				= "The file \"/p1\" already exists.";
FileError.AccessDenied			= "You do not have the access permission on the file \"/p1\".";
FileError.InvalidName				= "Invalid name for file \"/p1\".";
FileError.FileNotFound			= "File \"/p1\" not found.";
FileError.DiskFull					= "Disk full.";
FileError.InvalidParameter	= "Invalid parameters.";
FileError.InvalidHandle			= "Invalid file handle for file \"/p1\".";
FileError.OperationAborted	= "Operation aborted.";
FileError.UnknownError			= "Unknown error.";

