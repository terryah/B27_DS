// COPYRIGHT DASSAULT SYSTEMES 1998
//===========================================================================
//
// CATRscLightShadowPropertyFrame
//
//===========================================================================

EnvironmentCreationWarning = "There is no active environment for the shadows.\nDo you want to create a new transparent one?";
LightObjectShadowWarning = "More than three lights with object's shadows will decrease the display performances. \n";
LightObjectShadowWarningOnlyOne = "Only one light can support the object's shadows. No other one will be activated. \n";

small  = "Small";
medium = "Medium";
large  = "Large"; 

quality1 = "Standard";
quality2 = "Good";

nearPlaneMode1 = "Automatic";
nearPlaneMode2 = "Manual";

shadowPHSSepFrame.shadowPHSSepLabel.Title = "Ray Traced";
shadowPHSSepFrame.shadowPHSSepLabel.Help = "Enables ray traced shadows";
shadowPHSSepFrame.shadowPHSSepLabel.ShortHelp = "Enable Ray Traced Shadows";
shadowPHSSepFrame.shadowPHSSepLabel.LongHelp =
"Enables/disables ray traced shadows
cast by this light.";

shadowCheck.Title = "Enable";
shadowCheck.Help = "Enables ray traced shadows";
shadowCheck.ShortHelp = "Enable Ray Traced Shadows";
shadowCheck.LongHelp =
"Enables/disables ray traced shadows
cast by this light.";

shadowRTRSepFrame.shadowRTRSepLabel.Title = "Real Time";
shadowRTRSepFrame.shadowRTRSepLabel.Help = "Enables real time shadows";
shadowRTRSepFrame.shadowRTRSepLabel.ShortHelp = "Enable Real Time Shadows";
shadowRTRSepFrame.shadowRTRSepLabel.LongHelp =
"Enables/disables real time shadows
cast by this light.";

hardwareShadowCheck.Title = "On Environment";
hardwareShadowCheck.Help = "Enables real time shadows on environment";
hardwareShadowCheck.ShortHelp = "Real Time Shadows On Environment";
hardwareShadowCheck.LongHelp =
"Enables/disables real time shadows on environment.";

objectsShadowCheck.Title = "On Objects";
objectsShadowCheck.Help = "Enables real time shadows on objects";
objectsShadowCheck.ShortHelp = "Real Time Shadows On Objects";
objectsShadowCheck.LongHelp =
"Enables/disables real time shadows on geometry.";

shadowSmoothingLabel.Title = "Smoothing";
shadowSmoothingLabel.Help = "Defines the smoothing of the shadow limits";
shadowSmoothingLabel.ShortHelp = "Shadow Smoothing";
shadowSmoothingLabel.LongHelp =
"Define the smoothing of the shadow limits.";

shadowSmoothingSlider.Title = "";
shadowSmoothingSlider.Help = "Defines the smoothing of the shadow limits";
shadowSmoothingSlider.ShortHelp = "Shadow Smoothing";
shadowSmoothingSlider.LongHelp =
"Define the smoothing of the shadow limits.";

shadowColorLabel.Title = "Color";
shadowColorLabel.Help = "Sets shadow color";
shadowColorLabel.ShortHelp = "Shadow Color";
shadowColorLabel.LongHelp =
"Changes the shadow color.";

shadowColorSlider.Title = "";
shadowColorSlider.Help = "Sets shadow color";
shadowColorSlider.ShortHelp = "Shadow Color";
shadowColorSlider.LongHelp =
"Changes the color of the shadow.";

shadowTransparencyLabel.Title = "Transparency";
shadowTransparencyLabel.Help = "Sets shadow transparency";
shadowTransparencyLabel.ShortHelp = "Shadow Transparency";
shadowTransparencyLabel.LongHelp =
"Changes the shadow transparency.";

shadowTransparencySlider.Title = "";
shadowTransparencySlider.Help = "Sets shadow transparency";
shadowTransparencySlider.ShortHelp = "Shadow Transparency";
shadowTransparencySlider.LongHelp =
"Changes the transparency of the shadow.";

imageShadowSizeLabel.Title = "Map Precision ";
imageShadowSizeLabel.Help  = "Defines the shadow map precision";
imageShadowSizeLabel.ShortHelp = "Shadow Map Precision";
imageShadowSizeLabel.LongHelp =
"Define the map size of the shadow. 
The shadow will be more accurate as 
the size gets bigger.";

imageShadowSizeCombo.Title = "Map Precision ";
imageShadowSizeCombo.Help  = "Defines the shadow map precision";
imageShadowSizeCombo.ShortHelp = "Shadow Map Precision";
imageShadowSizeCombo.LongHelp =
"Define the map size of the shadow. 
The shadow will be more accurate as 
the size gets bigger.";

shadowFittingModeLabel.Title = "Quality ";
shadowFittingModeLabel.Help  = "Defines the shadow quality mode";
shadowFittingModeLabel.ShortHelp = "Shadow Quality Mode";
shadowFittingModeLabel.LongHelp =
"Define the quality mode of the shadow. 
If the quality mode is better, 
the shadow will be more accurate but
the performance will be slower.";

shadowFittingModeCombo.Title = "Quality ";
shadowFittingModeCombo.Help  = "Defines the shadow quality mode";
shadowFittingModeCombo.ShortHelp = "Shadow Quality Mode";
shadowFittingModeCombo.LongHelp =
"Define the quality mode of the shadow. 
If the quality mode is better, 
the shadow will be more accurate but
the performance will be slower.";

nearPlaneModeLabel.Title = "Near Plane ";
nearPlaneModeLabel.Help  = "Defines the shadow quality mode";
nearPlaneModeLabel.ShortHelp = "Shadow Quality Mode";
nearPlaneModeLabel.LongHelp =
"Define the quality mode of the shadow. 
If the quality mode is better, 
the shadow will be more accurate but
the performance will be slower.";

nearPlaneModeCombo.Title = "Near Plane ";
nearPlaneModeCombo.Help  = "Defines the shadow quality mode";
nearPlaneModeCombo.ShortHelp = "Shadow Quality Mode";
nearPlaneModeCombo.LongHelp =
"Define the quality mode of the shadow. 
If the quality mode is better, 
the shadow will be more accurate but
the performance will be slower.";

