DialogBoxTitle="Match Constraint";

TabPageContainer.MatchPage.Title	="Match";

TabPageContainer.MatchPage.ElementFrame.Title ="Elements";
TabPageContainer.MatchPage.ElementFrame.FirstElementLbl.Title="Source";
TabPageContainer.MatchPage.ElementFrame.SecondElementLbl.Title="Target";

TabPageContainer.MatchPage.ContinuityFrame.Title="Continuity";
TabPageContainer.MatchPage.ContinuityFrame.SurfaceContLbl.ShortHelp		="Orders of continuity";
TabPageContainer.MatchPage.ContinuityFrame.SurfaceContLbl.LongHelp      ="Orders of continuity - surfaces/curves";
TabPageContainer.MatchPage.ContinuityFrame.EdgeContLbl.ShortHelp		="Edge continuities";
TabPageContainer.MatchPage.ContinuityFrame.EdgeContLbl.LongHelp			="Edge continuities - Only for Surface Matching";
TabPageContainer.MatchPage.ContinuityFrame.SurfG0ChkBtn.ShortHelp		="G0 / Gap Matching";
TabPageContainer.MatchPage.ContinuityFrame.SurfG0ChkBtn.LongHelp		="G0 - Uncheck to perform a gap matching";
TabPageContainer.MatchPage.ContinuityFrame.SurfG1ChkBtn.ShortHelp		="G1";
TabPageContainer.MatchPage.ContinuityFrame.SurfG2ChkBtn.ShortHelp		="G2";
TabPageContainer.MatchPage.ContinuityFrame.SurfG3ChkBtn.ShortHelp		="G3";
TabPageContainer.MatchPage.ContinuityFrame.EdgeG1ChkBtn.ShortHelp		="G1";
TabPageContainer.MatchPage.ContinuityFrame.EdgeG2ChkBtn.ShortHelp		="G2";
TabPageContainer.MatchPage.ContinuityFrame.EdgeG3ChkBtn.ShortHelp		="G3";
TabPageContainer.MatchPage.ContinuityFrame.EndEdgeG1ChkBtn.ShortHelp		="G1";
TabPageContainer.MatchPage.ContinuityFrame.EndEdgeG2ChkBtn.ShortHelp		="G2";
TabPageContainer.MatchPage.ContinuityFrame.EndEdgeG3ChkBtn.ShortHelp		="G3";

TabPageContainer.MatchPage.ExactFrame.Title="Exact";
TabPageContainer.MatchPage.ExactFrame.LongHelp="Request is better fullfilled but Parametrization is not kept. Check Deviation";
TabPageContainer.MatchPage.ExactFrame.ExactLbl.LongHelp="Request is better fullfilled but Parametrization is not kept. Check Deviation";
TabPageContainer.MatchPage.ExactFrame.ExactG0ChkBtn.ShortHelp			="G0";
TabPageContainer.MatchPage.ExactFrame.ExactG1ChkBtn.ShortHelp			="G1";
TabPageContainer.MatchPage.ExactFrame.ExactG2ChkBtn.ShortHelp			="G2";
TabPageContainer.MatchPage.ExactFrame.ExactG3ChkBtn.ShortHelp			="G3";

TabPageContainer.MatchPage.MatchOptionFrame.Title="Options";
TabPageContainer.MatchPage.MatchOptionFrame.LongHelp					="Variants for the Matching result";
TabPageContainer.MatchPage.MatchOptionFrame.PartlyBtn.ShortHelp			="Partly";
TabPageContainer.MatchPage.MatchOptionFrame.PartlyBtn.LongHelp			="Matching surface meets the edge of the reference surface only partly";
TabPageContainer.MatchPage.MatchOptionFrame.DiffusionBtn.ShortHelp		="Diffusion";
TabPageContainer.MatchPage.MatchOptionFrame.DiffusionBtn.LongHelp		="Maximum number of rows of Control Points is modified";
TabPageContainer.MatchPage.MatchOptionFrame.InsideChkBtn.ShortHelp		="Inside";
TabPageContainer.MatchPage.MatchOptionFrame.InsideChkBtn.LongHelp		="Edge of matching surface is projected onto the reference surface";
TabPageContainer.MatchPage.MatchOptionFrame.LockBoundariesChkBtn.ShortHelp ="Lock boundaries";
TabPageContainer.MatchPage.MatchOptionFrame.LockBoundariesChkBtn.LongHelp  ="Lock the transversal boundaries in order to keep
the G0 in the transversal continuities
To be used for small deformation matchings";
TabPageContainer.MatchPage.MatchOptionFrame.BasicSurfaceChkBtn.ShortHelp="Basic Surfaces";
TabPageContainer.MatchPage.MatchOptionFrame.BasicSurfaceChkBtn.LongHelp ="Matches Basic Surface";
TabPageContainer.MatchPage.MatchOptionFrame.InvertChkBtn.ShortHelp		="Invert";
TabPageContainer.MatchPage.MatchOptionFrame.InvertChkBtn.LongHelp		="Switches the source element and the target element";

TabPageContainer.MatchPage.AutoApplyChkBtn.Title				="Auto Apply";
TabPageContainer.MatchPage.AlignmentFrame.Title					="Alignment";

TabPageContainer.MatchPage.InfoContDeviationInfoFrame.Title="Deviation";
TabPageContainer.MatchPage.InfoContDeviationInfoFrame.G0Lbl.Title="G0: ";
TabPageContainer.MatchPage.InfoContDeviationInfoFrame.G1Lbl.Title="G1: ";
TabPageContainer.MatchPage.InfoContDeviationInfoFrame.G2Lbl.Title="G2: ";
TabPageContainer.MatchPage.InfoContDeviationInfoFrame.G3Lbl.Title="G3: ";


TabPageContainer.AdvancedOptionPage.Title="Advanced";

TabPageContainer.AdvancedOptionPage.PlaneFrame.Title								="Mirror Matching";
TabPageContainer.AdvancedOptionPage.PlaneFrame.PlaneMatchingChkBtn.ShortHelp		="Mirror Matching";

TabPageContainer.AdvancedOptionPage.BothFrame.Title="Both";
TabPageContainer.AdvancedOptionPage.BothFrame.Title="Modify Source and Target";
TabPageContainer.AdvancedOptionPage.BothFrame.BothModeChkBtn.Title=" ";
TabPageContainer.AdvancedOptionPage.BothFrame.BothModeChkBtn.Title="Modify Source and Target";
TabPageContainer.AdvancedOptionPage.BothFrame.G0BothFrame.Title="G0";
TabPageContainer.AdvancedOptionPage.BothFrame.G0BothFrame.LongHelp="Law for the first row of both objects";
TabPageContainer.AdvancedOptionPage.BothFrame.G0BothFrame.MeanRdBtn.ShortHelp			   ="Mean";
TabPageContainer.AdvancedOptionPage.BothFrame.G0BothFrame.MeanRdBtn.LongHelp			   ="The common edge of the two newly deformed objects 
is at the middle of the two original edges";
TabPageContainer.AdvancedOptionPage.BothFrame.G0BothFrame.ExtensionRdBtn.ShortHelp		   ="Extension";
TabPageContainer.AdvancedOptionPage.BothFrame.G0BothFrame.ExtensionRdBtn.LongHelp		   ="The common edge of the two newly created surfaces
is at the intersection of the extrapolations of the two original surfaces
Does not work for curves";
TabPageContainer.AdvancedOptionPage.BothFrame.G1AndMoreBothFrame.Title="G1";
TabPageContainer.AdvancedOptionPage.BothFrame.G1AndMoreBothFrame.CommonEdgeChkBtn.ShortHelp ="Common Edge";
TabPageContainer.AdvancedOptionPage.BothFrame.G1AndMoreBothFrame.CommonEdgeChkBtn.LongHelp  ="Works G1 if G0 is already performed
Only the common edge is modified. The G1 rows of both surfaces are not modified";

TabPageContainer.AdvancedOptionPage.GapMatchingFrame.Title="Gap Options";
TabPageContainer.AdvancedOptionPage.GapMatchingFrame.GapMatchingOptionCombo.LongHelp="Form of the Gap";
TabPageContainer.AdvancedOptionPage.GapMatchingFrame.TempoOffsetValueFrame.Title="Offset Value";
TabPageContainer.AdvancedOptionPage.GapMatchingFrame.OffsetValueFrame.Title="Offset Value";

TabPageContainer.AdvancedOptionPage.G3OptionFrame.Title="G3 Options";
TabPageContainer.AdvancedOptionPage.G3OptionFrame.G3With3RowsChkBtn.ShortHelp="G3 with 3 rows";
TabPageContainer.AdvancedOptionPage.G3OptionFrame.G3With3RowsChkBtn.LongHelp="G3 continuity, without modifying the 4th row of control points";
TabPageContainer.AdvancedOptionPage.G3OptionFrame.G3WithFixCurvatureChkBtn.ShortHelp="G3 with fix curvature";
TabPageContainer.AdvancedOptionPage.G3OptionFrame.G3WithFixCurvatureChkBtn.LongHelp="G3 Continuity, without G2 Continuity if G3 is required";

TabPageContainer.AdvancedOptionPage.SupportFrame.Title="Support";
TabPageContainer.AdvancedOptionPage.SupportFrame.LongHelp = "Support - Direction of Projection";
TabPageContainer.AdvancedOptionPage.SupportFrame.ProjOnNormalRdBtn.ShortHelp="Normal";
TabPageContainer.AdvancedOptionPage.SupportFrame.ProjOnPlaneRdBtn.ShortHelp="On plane";

TabPageContainer.ApproximationPage.Title="Approximation";

Gap.GapExtrapol                                        = "Extrapolation";
Gap.GapParallel                                        = "Parallel";
Gap.GapOffFix                                          = "Offset Fix";
Gap.GapOffNormal                                       = "Offset Norm";

Alignment.MeanPlane									= "Mean Plane";
Alignment.ThreePoints								= "3-points Plane";

MainFrame.OptionsFrame.Title = "Options";
MainFrame.OptionsFrame.DiffusionCheck.Title = "Diffusion";
MainFrame.OptionsFrame.EndPointsCheck.Title = "Project end points";

P2Title = "Continuity constraint";



