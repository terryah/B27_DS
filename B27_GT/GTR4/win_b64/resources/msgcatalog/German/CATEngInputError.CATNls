// *--------------------------------------------*//
// *               Message example              *//
// *                                            *//
// *------------------------------------------- *//
// ENGINPUT_ERR_XXX.Request    = "Operation failed";
// ENGINPUT_ERR_XXX.Diagnostic = "Curve support /p1 is invalid";
// ENGINPUT_ERR_XXX.Advice     = "Choose a smoother curve";


///////////////////////////////////////////////////////////////////////////
//                            ATTRIBUTE ERRORS                           //
///////////////////////////////////////////////////////////////////////////
// Mandatory input
ENGINPUT_ERR_MANDATORYINPUT.Request    = "Fehlerhafte Komponente /p1";
ENGINPUT_ERR_MANDATORYINPUT.Diagnostic = "Das Attribut /p1 fehlt.";
ENGINPUT_ERR_MANDATORYINPUT.Advice     = "Die Komponente bearbeiten und dieses Attribut festlegen.";

// Null value
ENGINPUT_ERR_NULLVALUE.Request    = "Fehlerhafte Komponente /p1";
ENGINPUT_ERR_NULLVALUE.Diagnostic = "Das Attribut /p1 hat einen Nullwert.";
ENGINPUT_ERR_NULLVALUE.Advice     = "Die Komponente bearbeiten und dieses Attribut auf einen Wert ungleich dem Nullwert festlegen.";

// Useless limit
ENGINPUT_ERR_USELESSLIMIT.Request    = "Fehlerhafte Komponente /p1";
ENGINPUT_ERR_USELESSLIMIT.Diagnostic = "Die Begrenzung /p1 ist unbrauchbar.";
ENGINPUT_ERR_USELESSLIMIT.Advice     = "Diese �berfl�ssigen Daten entfernen.";

// limit of trace
ENGINPUT_ERR_OFFSETORIENTLIMIT.Request    = "Fehlerhafte Komponente /p1";
ENGINPUT_ERR_OFFSETORIENTLIMIT.Diagnostic = "Die Begrenzung kann nicht so verwendet werden, wie sie definiert ist.";
ENGINPUT_ERR_OFFSETORIENTLIMIT.Advice     = "Den Offset oder die Ausrichtung der Begrenzung �ndern";


///////////////////////////////////////////////////////////////////////////
//                           TOPOLOGICAL ERRORS                          //
///////////////////////////////////////////////////////////////////////////
// No body associated
ENGINPUT_ERR_BODY.Request    = "Fehlerhafte Komponente /p1";
ENGINPUT_ERR_BODY.Diagnostic = "/p2 hat keinen K�rper.";
ENGINPUT_ERR_BODY.Advice     = "Die Geometrie verbessern oder diese Komponente erneut erzeugen.";

// Nb of domains wanted
ENGINPUT_ERR_DOM.Request    = "Fehlerhafte Komponente /p1";
ENGINPUT_ERR_DOM.Diagnostic = "Die Anzahl der Dom�nen von /p1 ist gleich /p2.";
ENGINPUT_ERR_DOM.Advice     = "Die Anzahl der Dom�nen muss gleich /p1 sein.";

// Multi-domains
ENGINPUT_ERR_MONODOM.Request    = "Fehlerhafte Komponente /p1";
ENGINPUT_ERR_MONODOM.Diagnostic = "Ergebnis mit mehreren Dom�nen ist nicht zul�ssig.";
ENGINPUT_ERR_MONODOM.Advice     = "Die Geometrie muss verbessert werden.";

// Dimension error
ENGINPUT_ERR_DIM.Request    = "Fehlerhafte Komponente /p1";
ENGINPUT_ERR_DIM.Diagnostic = "Das Ma� von /p1 ist gleich /p2.";
ENGINPUT_ERR_DIM.Advice     = "Es muss gleich /p1 sein.";

// Infinite plane
ENGINPUT_ERR_INFINITE.Request    = "Fehlerhafte Komponente /p1";
ENGINPUT_ERR_INFINITE.Diagnostic = "/p1 ist nicht unendlich.";
ENGINPUT_ERR_INFINITE.Advice     = "Eine unendliche Ebene angeben";

// Infinite geometry forbidden
ENGINPUT_ERR_FINITE.Request    = "Fehlerhafte Komponente /p1";
ENGINPUT_ERR_FINITE.Diagnostic = "/p1 ist unendlich.";
ENGINPUT_ERR_FINITE.Advice     = "Eine endliche Geometrie angeben";

// Opened wire
ENGINPUT_ERR_OPENEDWIRE.Request    = "Fehlerhafte Komponente /p1";
ENGINPUT_ERR_OPENEDWIRE.Diagnostic = "/p1 ist ein ge�ffneter Draht.";
ENGINPUT_ERR_OPENEDWIRE.Advice     = "Die Geometrie muss verbessert werden.";

// Closed wire
ENGINPUT_ERR_CLOSEDWIRE.Request    = "Fehlerhafte Komponente /p1";
ENGINPUT_ERR_CLOSEDWIRE.Diagnostic = "/p1 ist ein geschlossener Draht.";
ENGINPUT_ERR_CLOSEDWIRE.Advice     = "Die Geometrie muss verbessert werden.";

// Multi-intersections
ENGINPUT_ERR_MULTIINTERSECT.Request    = "Fehlerhafte Komponente /p1";
ENGINPUT_ERR_MULTIINTERSECT.Diagnostic = "/p1 bringt ein Ergebnis mit mehreren Schnittpunkten.";
ENGINPUT_ERR_MULTIINTERSECT.Advice     = "Die Geometrie muss verbessert werden.";

// Projection ambiguity
ENGINPUT_ERR_PROJECTION.Request    = "Mehrdeutige Projektion";
ENGINPUT_ERR_PROJECTION.Diagnostic = "";
ENGINPUT_ERR_PROJECTION.Advice     = "Die 3D-Kurve muss auf der 3D-St�tzfl�che abgelegt werden.";

// Projection gives no result
ENGINPUT_ERR_NULLPROJECTION.Request    = "Projektion fehlgeschlagen";
ENGINPUT_ERR_NULLPROJECTION.Diagnostic = "Die Projektion auf /p1 ist fehlgeschlagen.";
ENGINPUT_ERR_NULLPROJECTION.Advice     = "Die Eingaben �ndern";

// Intersection gives no result
ENGINPUT_ERR_NULLINTERSECTION.Request    = "Verschneidung fehlgeschlagen";
ENGINPUT_ERR_NULLINTERSECTION.Diagnostic = "Die Verschneidung zwischen /p1 und /p2 ist fehlgeschlagen.";
ENGINPUT_ERR_NULLINTERSECTION.Advice     = "Die Eingaben �ndern";

// Intersection gives multi-domains result
ENGINPUT_ERR_INTERSECTIONMULTIDOM.Request    = "Verschneidung mit mehreren Bereichen";
ENGINPUT_ERR_INTERSECTIONMULTIDOM.Diagnostic = "Die Verschneidung zwischen /p1 und /p2 bringt mehrere Ergebnisse.";
ENGINPUT_ERR_INTERSECTIONMULTIDOM.Advice     = "Die Eingaben �ndern";

// Line of null length
ENGINPUT_ERR_NULLLENGTH.Request    = "Die Operation f�hrt zu einer Kurve mit der L�nge null.";
ENGINPUT_ERR_NULLLENGTH.Diagnostic = "Zwei Punkte sind vertauscht.";
ENGINPUT_ERR_NULLLENGTH.Advice     = "Die Eingaben �ndern";

// Intersection Curve-Surface is not a point
ENGINPUT_ERR_INTERSECTCRVSURFNOPOINT.Request    = "Verschneidung fehlgeschlagen";
ENGINPUT_ERR_INTERSECTCRVSURFNOPOINT.Diagnostic = "Die Verschneidung zwischen /p1 und /p2 ergibt keinen Punkt.";
ENGINPUT_ERR_INTERSECTCRVSURFNOPOINT.Advice     = "Die Eingaben �ndern";

// Intersection Surface-Surface is not a curve
ENGINPUT_ERR_INTERSECTSURFSURFNOCRV.Request    = "Verschneidung fehlgeschlagen";
ENGINPUT_ERR_INTERSECTSURFSURFNOCRV.Diagnostic = "Die Verschneidung zwischen /p1 und /p2 ergibt keine Kurve.";
ENGINPUT_ERR_INTERSECTSURFSURFNOCRV.Advice     = "Die Eingaben �ndern";

// Intersection Surface-Surface-Surface is not a point
ENGINPUT_ERR_INTERSECTSURFSURFNOPT.Request    = "Verschneidung fehlgeschlagen";
ENGINPUT_ERR_INTERSECTSURFSURFNOPT.Diagnostic = "Die Verschneidung zwischen /p1, /p2 und /p3 ergibt keinen Punkt.";
ENGINPUT_ERR_INTERSECTSURFSURFNOPT.Advice     = "Die Eingaben �ndern";

// Offset too big
ENGINPUT_ERR_PTOFFSETOUTSIDECRV.Request    = "Punkt auf Kurve";
ENGINPUT_ERR_PTOFFSETOUTSIDECRV.Diagnostic = "Der Offsetwert und die Ausrichtung f�hren zu einem Punkt, der nicht auf der Kurve liegt.";
ENGINPUT_ERR_PTOFFSETOUTSIDECRV.Advice     = "Die Eingaben �ndern";

// Minimal segment length
ENGINPUT_ERR_MINSEGMENTLENGTH.Request    = "Segmentmindestl�nge";
ENGINPUT_ERR_MINSEGMENTLENGTH.Diagnostic = "Mindestens eines der geraden Segmente der Kurve /p1 ist k�rzer als die Mindestl�nge.";
ENGINPUT_ERR_MINSEGMENTLENGTH.Advice     = "Die Spezifikationen pr�fen";

// Curvature check for the guide of the sweep
ENGINPUT_ERR_CURVESWEEPCURVATURE.Request    = "Kr�mmungsproblem";
ENGINPUT_ERR_CURVESWEEPCURVATURE.Diagnostic = "Die F�hrungskurve hat eine Kr�mmung, die f�r die Gr��e dieses Abschnitts zu klein ist.";
ENGINPUT_ERR_CURVESWEEPCURVATURE.Advice     = "Die Gr��e des Abschnitts oder die Kr�mmung der F�hrungskurve anpassen";

// Curve lay down on a surface
ENGINPUT_ERR_CURVELAYDOWN.Request    = "Kurve wurde nicht auf der Fl�che abgelegt.";
ENGINPUT_ERR_CURVELAYDOWN.Diagnostic = "Die F�hrungskurve /p1 wurde nicht auf der ausgew�hlten Fl�che /p2 abgelegt.";
ENGINPUT_ERR_CURVELAYDOWN.Advice     = "Die Geometrie korrigieren";
