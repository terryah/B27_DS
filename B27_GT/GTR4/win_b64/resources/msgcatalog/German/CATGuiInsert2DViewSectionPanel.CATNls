//-----------------------------------------------------------------------------
// COPYRIGHT DASSAULT SYSTEMES 2003
//-----------------------------------------------------------------------------
//
//                    InsertSection Panel Resources
//
//-----------------------------------------------------------------------------

Title="Definition der 2D-Schnittansicht";

PartDefault="Standard (";

Separator1.TitleLabel.Title="Allgemeine Parameter";

frame1.Lab2DViewPlane.Title="2D-Ansichtsebene: ";
frame1.Lab2DViewPlane.LongHelp="Gibt die Ebene oder die ebene Komponente an, auf der die 2D-Ansicht angezeigt wird.";
frame1.Edt2DViewPlane.LongHelp="Gibt die Ebene oder die ebene Komponente an, auf der die 2D-Ansicht angezeigt wird.";

frame1.LabCuttingPlane.Title="Verschneidungsebene: ";
frame1.LabCuttingPlane.LongHelp="Gibt die Ebene oder die ebene Komponente an, die zum Schneiden der Schnittelemente in der 3D-Schnittansicht verwendet wird.";
frame1.EdtCuttingPlane.LongHelp="Gibt die Ebene oder die ebene Komponente an, die zum Schneiden der Schnittelemente in der 3D-Schnittansicht verwendet wird.";

frame1.Radio2D.Title="Nur 2D-Ansicht";
frame1.Radio2D.LongHelp="3D-Schnittpunkte werden erzeugt, sind jedoch ausgeblendet, w�hrend die 2D-Ansicht erzeugt und angezeigt wird.";
frame1.Radio2D3D.Title="2D-Ansicht und Verschneidung";
frame1.Radio2D3D.LongHelp="Sowohl die 3D-Schnittpunkte als auch die 2D-Ansicht wurden erzeugt und werden angezeigt.";

Separator2.TitleLabel.Title="Gruppenparameter";

frame2.ListeOnglets.OngletElements.Title="Elemente schneiden";
frame2.ListeOnglets.OngletTransfer.Title="2D-Ansicht -> Schnitt�bertragung";

frame2.ListeOnglets.OngletElements.LabGroup.Title="Inhalt von: ";
frame2.ListeOnglets.OngletElements.LabGroupColor.Title="Gruppenfarbe: ";
frame2.ListeOnglets.OngletElements.LabGroupColor.LongHelp="Gibt die Farbe des Inhalts der Gruppe an.";
frame2.ListeOnglets.OngletElements.GroupColorCombo.LongHelp="Gibt die Farbe des Inhalts der Gruppe an.";
frame2.ListeOnglets.OngletElements.Separator3b.TitleLabel.Title="Elemente in der Gruppe schneiden";

frame2.ListeOnglets.OngletElements.ButChangeGroup.Title="Verschieben nach";
frame2.ListeOnglets.OngletElements.ButChangeGroup.LongHelp="Verschiebt die Auswahl zur angegebenen Gruppe.";
frame2.ListeOnglets.OngletElements.ChangeGroupCombo.LongHelp="Verschiebt die Auswahl zur angegebenen Gruppe.";

frame2.ListeOnglets.OngletElements.Separator3.TitleLabel.Title="Ecken";

frame2.ListeOnglets.OngletElements.CornersTabContainer.TabCornersManagement.Title="Eckenverwaltung";
frame2.ListeOnglets.OngletElements.CornersTabContainer.TabCornersManagement.LongHelp="Eckenberechnungsverwaltung";
frame2.ListeOnglets.OngletElements.CornersTabContainer.TabCornersManagement.frameCorner.LabCorner.Title="Standardwert f�r Eckenradius: ";
frame2.ListeOnglets.OngletElements.CornersTabContainer.TabCornersManagement.frameCorner.LabCorner.LongHelp="Gibt den Eckenradiuswert an, der auf scharfe Scheitelpunkte angewendet wird,
die �brig bleiben, wenn gespeicherte und angezeigte Ecken angewendet wurden.
Dieser Wert wird auch verwendet, wenn auf 'Ecken berechnen' geklickt wird.";
frame2.ListeOnglets.OngletElements.CornersTabContainer.TabCornersManagement.frameCorner.ChkAutoCorners.Title="Nicht definierte Ecken automatisch berechnen";
frame2.ListeOnglets.OngletElements.CornersTabContainer.TabCornersManagement.frameCorner.ChkAutoCorners.LongHelp="Wenn diese Option aktiviert ist, wird der Standardeckenradius f�r scharfe Scheitelpunkte verwendet,
die �brig bleiben, wenn gespeicherte und angezeigte Ecken angewendet wurden.";
frame2.ListeOnglets.OngletElements.CornersTabContainer.TabCornersManagement.frameCorner.ComputeCornersButton.Title="Ecken berechnen";
frame2.ListeOnglets.OngletElements.CornersTabContainer.TabCornersManagement.frameCorner.ComputeCornersButton.LongHelp="Sucht nach scharfen Scheitelpunkten und
erzeugt f�r jeden von ihnen einen Standardeckenradiuswert.";
frame2.ListeOnglets.OngletElements.CornersTabContainer.TabCornersManagement.frameCorner.RemoveCornersButton.Title="Ecken entfernen";
frame2.ListeOnglets.OngletElements.CornersTabContainer.TabCornersManagement.frameCorner.RemoveCornersButton.LongHelp="Entfernt alle mit der Schaltfl�che 'Ecken berechnen' erzeugten und angezeigten Ecken.";

frame2.ListeOnglets.OngletElements.CornersTabContainer.TabCornersManagement.Separator4.TitleLabel.Title="Aufma�";
frame2.ListeOnglets.OngletElements.CornersTabContainer.TabCornersManagement.frameThickness.ChkComputeThick.Title="Aufma�: ";
frame2.ListeOnglets.OngletElements.CornersTabContainer.TabCornersManagement.frameThickness.ChkComputeThick.LongHelp="Wenn diese Option aktiviert ist, wird der Offsetwert der angewendeten parallelen Kurve angegeben.";
frame2.ListeOnglets.OngletElements.CornersTabContainer.TabCornersManagement.frameThickness.ChkClosureThick.Title="Endpunkte �ffnen-schlie�en";
frame2.ListeOnglets.OngletElements.CornersTabContainer.TabCornersManagement.frameThickness.ChkClosureThick.LongHelp="Wenn diese Option aktiviert ist, k�nnen Aufma�endpunkte ge�ffnet oder geschlossen werden.";
frame2.ListeOnglets.OngletElements.CornersTabContainer.TabCornersManagement.frameThickness.InvertOrientButtonV5.Title="Aufma�ausrichtung umkehren";
frame2.ListeOnglets.OngletElements.CornersTabContainer.TabCornersManagement.frameThickness.InvertOrientButtonV5.LongHelp="L�st die Umkehrung der Ausrichtung der angewendeten parallelen Kurve aus.";
frame2.ListeOnglets.OngletElements.CornersTabContainer.TabCornersManagement.frameThickness.InvertOrientButton.Title="Aufma�richtung umkehren";
frame2.ListeOnglets.OngletElements.CornersTabContainer.TabCornersManagement.frameThickness.InvertOrientButton.LongHelp="Ausrichtung der angewendeten parallelen Kurve umkehren.";

frame2.ListeOnglets.OngletElements.CornersTabContainer.VerticesList.Title="Scheitelpunktliste";
frame2.ListeOnglets.OngletElements.CornersTabContainer.VerticesList.LongHelp="Liste der Scheitelpunkte, die mit den Schaltfl�chen 'Nach oben verschieben' und 'Nach unten verschieben' neu angeordnet werden kann.";
frame2.ListeOnglets.OngletElements.CornersTabContainer.VerticesList.FrameCornersList.LabCornersList.Title="Scheitelpunkte";
frame2.ListeOnglets.OngletElements.CornersTabContainer.VerticesList.FrameCornersList.pPBMoveUp.Title="Nach oben verschieben";
frame2.ListeOnglets.OngletElements.CornersTabContainer.VerticesList.FrameCornersList.pPBMoveUp.LongHelp="Ausgew�hlten Scheitelpunkt um eine Position nach oben verschieben.";
frame2.ListeOnglets.OngletElements.CornersTabContainer.VerticesList.FrameCornersList.pPBMoveDown.Title="Nach unten verschieben";
frame2.ListeOnglets.OngletElements.CornersTabContainer.VerticesList.FrameCornersList.pPBMoveDown.LongHelp="Ausgew�hlten Scheitelpunkt um eine Position nach unten verschieben.";

frame2.ListeOnglets.OngletTransfer.Lab2DTransferred.Title="�bertragen";
frame2.ListeOnglets.OngletTransfer.2DTransferredList.LongHelp="Gibt die Liste der von der 2D- in die 3D-Ansicht �bertragenen Elemente an.";
frame2.ListeOnglets.OngletTransfer.LabNot2DTransferred.Title="Nicht �bertragen";
frame2.ListeOnglets.OngletTransfer.Not2DTransferredList.LongHelp="Gibt die Liste der Elemente an, die aus der 2D-Ansicht in die 3D-Ansicht �bertragen werden k�nnen.";
frame3.ListeOnglets.OngletTransfer.Push2DTransfer.LongHelp="Verschiebt Elemente in die Liste der �bertragenen Elemente.";
frame3.ListeOnglets.OngletTransfer.PushUndo2DTransfer.LongHelp="Verschiebt Elemente in die Liste der Elemente, die nicht �bertragen werden.";

frame2.ListeOnglets.OngletTransfer.ButAdd2DEltToGroup.Title="Hinzuf�gen zu";
frame2.ListeOnglets.OngletTransfer.ButAdd2DEltToGroup.LongHelp="F�gt ein beliebiges Element einer Gruppe f�r Operationen an Ecken und am Aufma� hinzu.";
frame2.ListeOnglets.OngletTransfer.Add2DEltToCombo.LongHelp="F�gt ein beliebiges Element einer Gruppe f�r Operationen an Ecken und am Aufma� hinzu.";

Panel2DViewSectionNewGroup="Neue Gruppe";
Panel2DViewSectionGroup="Gruppe ";

frame5.TabContRefPtMove.TabSectionMove.pframeint1.pLabCombo.Title="Inkrementwert ausgerichtet an: ";
frame5.TabContRefPtMove.TabSectionMove.pframeint1.pLabCombo.LongHelp="Die Verschiebung der Schnittinformationen entlang des Gitters f�r die Arbeit am 3D-St�tzelement oder der Verschneidungsschnittrichtung";

frame5.TabContRefPtMove.TabSectionMove.pframeint1.PanningCombo.LongHelp="Die Verschiebung der Schnittinformationen entlang des Gitters 'Arbeiten an 3D-St�tzelement' oder der Verschneidungsschnittrichtung";

frame5.TabContRefPtMove.TabSectionMove.pframeint2.pLabNameH.Title="Ansichtsebene H:";
frame5.TabContRefPtMove.TabSectionMove.pframeint2.pLabNameH.LongHelp="Verschiebung entlang der Richtung der Ansichtsebene H";
frame5.TabContRefPtMove.TabSectionMove.pframeint2.pLabNameV.Title="Ansichtsebene V:";
frame5.TabContRefPtMove.TabSectionMove.pframeint2.pLabNameV.LongHelp="Verschiebung entlang der Richtung der Ansichtsebene V";

NumberColumnTitle="Nr.";
VerticesColumn="Scheitelpunkte";
Bansen="Ansichtsebene";
Intersection="Verschneidungsschnittrichtung";

frame5.TabContRefPtMove.TabCutAreaRefPt.Title="Bema�ung des Schnittbereichs";
frame5.TabContRefPtMove.TabSectionMove.Title="2D-Schnittverschiebung";

frame5.TabContRefPtMove.TabCutAreaRefPt.FrmInterMediate1.LabRefPoint.Title="Referenzpunkt: ";
frame5.TabContRefPtMove.TabCutAreaRefPt.FrmInterMediate1.LabRefPoint.LongHelp="Gibt den Punkt an, der als Mittelpunkt des Schnittbereichs verwendet wird.";
frame5.TabContRefPtMove.TabCutAreaRefPt.FrmInterMediate1.EdtRefPt.LongHelp="Gibt den Punkt an, der als Mittelpunkt des Schnittbereichs verwendet wird.";

frame5.TabContRefPtMove.TabCutAreaRefPt.FrmInterMediate2.LabWidth.Title="Breite des Schnittbereichs: ";
frame5.TabContRefPtMove.TabCutAreaRefPt.FrmInterMediate2.LabWidth.LongHelp="Gibt die Breite des Schnittrechtecks vom Referenzpunkt des Schnittbereichs an. Sie ist auf beiden Seiten des Referenzpunkts symmetrisch.";

frame5.TabContRefPtMove.TabCutAreaRefPt.FrmInterMediate2.LabLength.Title="L�nge des Schnittbereichs: ";
frame5.TabContRefPtMove.TabCutAreaRefPt.FrmInterMediate2.LabLength.LongHelp="Gibt die L�nge des Schnittrechtecks vom Referenzpunkt des Schnittbereichs an. Sie ist auf beiden Seiten des Referenzpunkts symmetrisch.";

RefPoint="Referenzpunkt";

Bansen="Gitter f�r Arbeit an 3D-St�tzelement";
Intersection="Verschneidungsschnittrichtung";

