//-----------------------------------------------------------------------------
// COPYRIGHT DASSAULT SYSTEMES 2003
//-----------------------------------------------------------------------------
//
//                   CATGSMUIUnfold
//
//-----------------------------------------------------------------------------

// Command Title.
// -------
CATGSMUIUnfold.Title="Befehl 'Abwickeln'";
CATGSMUIUnfold.UndoTitle="Befehl 'Abwickeln'";

// States.
// -------
CATGSMUIUnfold.StateOfSurfaceToUnfold.Message="Die abzuwickelnde Fl�che ausw�hlen.";
CATGSMUIUnfold.StateOfOriginToUnfold.Message="Den abzuwickelnden Ursprung ausw�hlen.";
CATGSMUIUnfold.StateOfDirectionToUnfold.Message="Die abzuwickelnde Richtung ausw�hlen.";

CATGSMUIUnfold.StateOfAcqTargetSurface.Message="Die Zielebene ausw�hlen.";
CATGSMUIUnfold.StateOfTargetOrigin.Message="Den Zielursprung ausw�hlen.";
CATGSMUIUnfold.StateOfTargetDirection.Message="Die Zielrichtung ausw�hlen.";

CATGSMUIUnfold.StateOfAcqSpecToTear.Message="Eine einrei�ende Ecke oder Kurve ausw�hlen.";
CATGSMUIUnfold.StateOfAcqSpecToTransfer.Message="Die zu �bertragenden Punkte oder Kurven ausw�hlen.";

// Representations.
// ----------------
CATGSMUIUnfold.Result.ShortHelp="Ergebnis der Abwicklung";

CATGSMUIUnfold.SurfaceToUnfold.Axis.Title="Referenz";
CATGSMUIUnfold.TargetPlane.Axis.Title="Ziel";

CATGSMUIUnfold.EdgeToTear.Request="Einrei�ende Ecke";
CATGSMUIUnfold.EdgeToTear.Diagnostic="Die Mehrdeutigkeit kann durch Einrei�en dieser Ecke aufgel�st werden.";

CATGSMUIUnfold.TransferedSpecifications.Request="�bertragener Punkt oder �bertragene Kurve";

// Notifications.
// ----------------
CATGSMUIUnfold.ReplaceDimension.Title="Ung�ltiges Ersatzelement.";
CATGSMUIUnfold.ReplaceDimension.Message="Ersatzelement muss dieselbe Bema�ung aufweisen wie das ersetzte Element.";

CATGSMUIUnfold.InvalidEdgeToTear.Title="Ung�ltige einrei�ende Kurve(n)/Ecke(n)";
CATGSMUIUnfold.InvalidEdgeToTear.Message="Die einrei�ende Kurve(n)/Ecke(n) /p1 ist/sind ung�ltig oder kann/k�nnen zu einem nicht verbindungsf�higen Ergebnis f�hren.
Soll(en) diese entfernt werden?";

CATGSMUIUnfold.OriginNotOnSurface.Title="Ung�ltiger Referenzursprung.";
CATGSMUIUnfold.OriginNotOnSurface.Message="Der ausgew�hlte Punkt liegt nicht auf der abzuwickelnden Fl�che.";

CATGSMUIUnfold.DirectionNotOnSurface.Title="Ung�ltige Referenzrichtung.";
CATGSMUIUnfold.DirectionNotOnSurface.Message="Die ausgew�hlte Richtung liegt nicht auf der abzuwickelnden Fl�che.";

CATGSMUIUnfold.DirectionNotContainsOrigin.Title="Ung�ltige Referenzrichtung.";
CATGSMUIUnfold.DirectionNotContainsOrigin.Message="Die ausgew�hlte Richtung ist nicht mit dem ausgew�hlten Ursprung verkn�pft.";

CATGSMUIUnfold.ReferenceFaceNotOnSurface.Title="Ung�ltige Referenzteilfl�che.";
CATGSMUIUnfold.ReferenceFaceNotOnSurface.Message="Die ausgew�hlte Teilfl�che befindet sich nicht in der abzuwickelnden Fl�che.";

CATGSMUIUnfold.ReferenceFaceNotContainsOrigin.Title="Ung�ltige Referenzteilfl�che.";
CATGSMUIUnfold.ReferenceFaceNotContainsOrigin.Message="Die ausgew�hlte Teilfl�che ist nicht mit dem ausgew�hlten Ursprung verkn�pft.";

CATGSMUIUnfold.ReferenceFaceNotContainsDirection.Title="Ung�ltige Referenzteilfl�che.";
CATGSMUIUnfold.ReferenceFaceNotContainsDirection.Message="Die ausgew�hlte Teilfl�che ist nicht mit der ausgew�hlten Richtung verkn�pft.";

CATGSMUIUnfold.FixedSideNeeded.Title="Nicht eindeutige Referenzrichtung";
CATGSMUIUnfold.FixedSideNeeded.Message="Die ausgew�hlte Referenzrichtung liegt auf einer einrei�enden Ecke.
Eine Teilfl�che ausw�hlen, um zu bestimmen, welche Seite der Ecke fixiert werden soll.";

// Agents Title
// ----------------
CATGSMUIUnfold.AgentOfIndSurfaceToUnfold.UndoTitle="Angabe der abzuwickelnden Fl�che";
CATGSMUIUnfold.AgentOfAcqSurfaceToUnfold.UndoTitle="Auswahl der abzuwickelnden Fl�che";
CATGSMUIUnfold.AgentOfRemSurfaceToUnfold.UndoTitle="Die abzuwickelnde Fl�che l�schen";

CATGSMUIUnfold.AgentOfIndOriginToUnfold.UndoTitle="Angabe des Ursprungs der Referenz";
CATGSMUIUnfold.AgentOfAcqOriginToUnfold.UndoTitle="Auswahl des Ursprungs der Referenz";
CATGSMUIUnfold.AgentOfRemOriginToUnfold.UndoTitle="Den Ursprung der Referenz l�schen";

CATGSMUIUnfold.AgentOfIndDirectionToUnfold.UndoTitle="Angabe der Richtung der Referenz";
CATGSMUIUnfold.AgentOfAcqDirectionToUnfold.UndoTitle="Auswahl der Richtung der Referenz";
CATGSMUIUnfold.AgentOfRemDirectionToUnfold.UndoTitle="Die Richtung der Referenz l�schen";

CATGSMUIUnfold.AgentOfIndReferenceFaceToUnfold.UndoTitle="Angabe der Referenzteilfl�che";
CATGSMUIUnfold.AgentOfAcqReferenceFaceToUnfold.UndoTitle="Auswahl der Referenzteilfl�che";
CATGSMUIUnfold.AgentOfRemReferenceFaceToUnfold.UndoTitle="Die Referenzteilfl�che l�schen";

CATGSMUIUnfold.AgentOfIndTargetSurface.UndoTitle="Angabe der Zielebene";
CATGSMUIUnfold.AgentOfAcqTargetSurface.UndoTitle="Auswahl der Zielebene";
CATGSMUIUnfold.AgentOfRemTargetSurface.UndoTitle="Die Zielebene l�schen";

CATGSMUIUnfold.AgentOfIndTargetOrigin.UndoTitle="Angabe des Zielursprungs";
CATGSMUIUnfold.AgentOfAcqTargetOrigin.UndoTitle="Auswahl des Zielursprungs";
CATGSMUIUnfold.AgentOfRemTargetOrigin.UndoTitle="Den Zielursprung l�schen";

CATGSMUIUnfold.AgentOfIndTargetDirection.UndoTitle="Angabe der Zielrichtung";
CATGSMUIUnfold.AgentOfIndCoordinatesTargetDirection.UndoTitle="Auswahl der Zielrichtung";
CATGSMUIUnfold.AgentOfAcqTargetDirection.UndoTitle="Auswahl der Zielrichtung";
CATGSMUIUnfold.AgentOfRemTargetDirection.UndoTitle="Die Zielrichtung l�schen";

CATGSMUIUnfold.AgentOfIndSpecToTear.UndoTitle="Angabe der einrei�enden Ecken";
CATGSMUIUnfold.AgentOfAcqEdgeToTear.UndoTitle="Auswahl der einrei�enden Ecken";
CATGSMUIUnfold.AgentOfAcqCurveToTear.UndoTitle="Auswahl der einrei�enden Kurven";
CATGSMUIUnfold.AgentOfRemSpecToTear.UndoTitle="Einrei�ende Ecken l�schen";

CATGSMUIUnfold.AgentOfIndSpecToTransfer.UndoTitle="Angabe der zu �bertragenen Punkte oder Kurven";
CATGSMUIUnfold.AgentOfAcqSpecToTransfer.UndoTitle="Auswahl der zu �bertragenden Punkte oder Kurven";
CATGSMUIUnfold.AgentOfRemSpecToTransfer.UndoTitle="Zu �bertragende Punkte oder Kurven entfernen";
CATGSMUIUnfold.AgentOfRepTypeOfTransfer.UndoTitle="Zu �bertragenden Typ �ndern";

CATGSMUIUnfold.SurfaceTypeAgent.UndoTitle="Auswahl des Fl�chentyps";
CATGSMUIUnfold.2DDistortionAgent.UndoTitle="Abgewickelte Verzerrung anzeigen";
CATGSMUIUnfold.3DDistortionAgent.UndoTitle="Fl�che anzeigen, um Verzerrung abzuwickeln";
CATGSMUIUnfold.Permanent2DDistortionAgent.UndoTitle="Permanent abgewickelte Verzerrung";
CATGSMUIUnfold.OptionalEdgesAgent.UndoTitle="Optional einrei�ende Kanten anzeigen";
CATGSMUIUnfold.TargetOrientationAgent.UndoTitle="Ausrichtung der Zielfl�che";
CATGSMUIUnfold.EdgeToTearPosAgentV5.UndoTitle="Ausrichtung umkehren";
CATGSMUIUnfold.EdgeToTearPosAgent.UndoTitle="Richtung umkehren";

CATGSMUIUnfold.UiAxis="Ui";
CATGSMUIUnfold.ViAxis="Vi";

CATGSMUIUnfold.UfAxis="Uf";
CATGSMUIUnfold.VfAxis="Vf";

CATGSMUIUnfold.TilteWarning="Achtung";
CATGSMUIUnfold.NoMaterialMode="Falscher Anzeigemodus.
In den Modus 'Schattierung mit Material' wechseln, um Farbzuordnungen f�r die Verzerrung anzuzeigen";
