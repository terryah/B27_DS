// COPYRIGHT DASSAULT SYSTEMES 1998
//===========================================================================
//
// CATRdgShootingPictureFrame
//
//===========================================================================

currentViewpointLabel="Aktueller Blickpunkt";
noEnvironmentLabel="Keiner";
noLightLabel="Keiner";

//--------------------------------------
// Scene frame
//--------------------------------------
sceneFrame.HeaderFrame.Global.Title="Szene";

sceneFrame.IconAndOptionsFrame.OptionsFrame.sceneParametersFrame.cameraLabel.Title="Kamera:";
sceneFrame.IconAndOptionsFrame.OptionsFrame.sceneParametersFrame.cameraCombo.Help="Definiert die f�r die Wiedergabe verwendete aktive Kamera";
sceneFrame.IconAndOptionsFrame.OptionsFrame.sceneParametersFrame.cameraCombo.ShortHelp="Bei der Wiedergabe aktive Kamera";
sceneFrame.IconAndOptionsFrame.OptionsFrame.sceneParametersFrame.cameraCombo.LongHelp="Definiert den f�r die Wiedergabe geltenden Blickpunkt.";

sceneFrame.IconAndOptionsFrame.OptionsFrame.sceneParametersFrame.environmentLabel.Title="Umgebung: ";
sceneFrame.IconAndOptionsFrame.OptionsFrame.sceneParametersFrame.environmentCombo.Help="Definiert die bei der Wiedergabe aktive Umgebung";
sceneFrame.IconAndOptionsFrame.OptionsFrame.sceneParametersFrame.environmentCombo.ShortHelp="Bei der Wiedergabe aktive Umgebung";
sceneFrame.IconAndOptionsFrame.OptionsFrame.sceneParametersFrame.environmentCombo.LongHelp="Definiert die bei der Wiedergabe zu verwendende Umgebung.";

sceneFrame.IconAndOptionsFrame.OptionsFrame.sceneParametersFrame.lightLabel.Title="Lichtquellen:";

sceneFrame.IconAndOptionsFrame.OptionsFrame.sceneParametersFrame.lightSelectedLabel.Title="Ausgew�hlt";
sceneFrame.IconAndOptionsFrame.OptionsFrame.sceneParametersFrame.lightList.Help="Definiert die f�r die Wiedergabe verwendeten aktiven Lichtquellen";
sceneFrame.IconAndOptionsFrame.OptionsFrame.sceneParametersFrame.lightList.ShortHelp="F�r Wiedergabe verwendete aktive Lichtquellen";
sceneFrame.IconAndOptionsFrame.OptionsFrame.sceneParametersFrame.lightList.LongHelp="Definiert die f�r die Wiedergabe verwendeten aktiven Lichtquellen.";

sceneFrame.IconAndOptionsFrame.OptionsFrame.sceneParametersFrame.lightAvailableLabel.Title="Verf�gbar";
sceneFrame.IconAndOptionsFrame.OptionsFrame.sceneParametersFrame.lightAvailableList.Help="Liste der verf�gbaren Lichtquellen";
sceneFrame.IconAndOptionsFrame.OptionsFrame.sceneParametersFrame.lightAvailableList.ShortHelp="Verf�gbare Lichtquellen";
sceneFrame.IconAndOptionsFrame.OptionsFrame.sceneParametersFrame.lightAvailableList.LongHelp="Listet die Lichtquellen auf, die f�r die Wiedergabe aktiviert werden k�nnen.";

sceneFrame.IconAndOptionsFrame.OptionsFrame.sceneParametersFrame.lightButtonsFrame.lightAddButton.Help="F�gt ausgew�hlte Lichtquellen hinzu";
sceneFrame.IconAndOptionsFrame.OptionsFrame.sceneParametersFrame.lightButtonsFrame.lightAddButton.ShortHelp="Hinzuf�gen";
sceneFrame.IconAndOptionsFrame.OptionsFrame.sceneParametersFrame.lightButtonsFrame.lightAddButton.LongHelp="F�gt der Liste mit den f�r die Wiedergabe aktivierten Lichtquellen neue Lichtquellen hinzu.";

sceneFrame.IconAndOptionsFrame.OptionsFrame.sceneParametersFrame.lightButtonsFrame.lightRemoveButton.Help="Entfernt Lichtquellen, die bereits ausgew�hlt worden waren";
sceneFrame.IconAndOptionsFrame.OptionsFrame.sceneParametersFrame.lightButtonsFrame.lightRemoveButton.ShortHelp="Entfernen";
sceneFrame.IconAndOptionsFrame.OptionsFrame.sceneParametersFrame.lightButtonsFrame.lightRemoveButton.LongHelp="Entfernt Lichtquellen aus der Liste mit den f�r die Wiedergabe aktivierten Lichtquellen.";

sceneFrame.IconAndOptionsFrame.OptionsFrame.previewButton.Help="Kameraansicht anzeigen";
sceneFrame.IconAndOptionsFrame.OptionsFrame.previewButton.ShortHelp="Kameraansicht";
sceneFrame.IconAndOptionsFrame.OptionsFrame.previewButton.LongHelp="Zeigt die Kameraansicht an. Bei gro�en
Szenen kann die Aktualisierung einige Zeit in Anspruch nehmen.";

//--------------------------------------
// Size frame
//--------------------------------------
sizeFrame.HeaderFrame.Global.Title="Bildgr��e";

sizeFrame.IconAndOptionsFrame.OptionsFrame.sizeParametersFrame.sizeParameters1Frame.sizePredefinedSmallLabel.Title="Klein";
sizeFrame.IconAndOptionsFrame.OptionsFrame.sizeParametersFrame.sizeParameters1Frame.sizePredefinedLargeLabel.Title="Gro�";
sizeFrame.IconAndOptionsFrame.OptionsFrame.sizeParametersFrame.sizeParameters1Frame.sizePredefinedSlider.Help="Vordefinierte Bildgr��en";
sizeFrame.IconAndOptionsFrame.OptionsFrame.sizeParametersFrame.sizeParameters1Frame.sizePredefinedSlider.ShortHelp="Vordefinierte Gr��en";
sizeFrame.IconAndOptionsFrame.OptionsFrame.sizeParametersFrame.sizeParameters1Frame.sizePredefinedSlider.LongHelp="Listet alle vordefinierten Gr��en auf.";

sizeFrame.IconAndOptionsFrame.OptionsFrame.sizeParametersFrame.sizeParameters1Frame.sizeRatioButton.Help="Sperrt das Gr��enverh�ltnis";
sizeFrame.IconAndOptionsFrame.OptionsFrame.sizeParametersFrame.sizeParameters1Frame.sizeRatioButton.ShortHelp="Gr��enverh�ltnis sperren";
sizeFrame.IconAndOptionsFrame.OptionsFrame.sizeParametersFrame.sizeParameters1Frame.sizeRatioButton.LongHelp="Sperrt das Verh�ltnis zwischen Bildh�he und -l�nge.";

sizeFrame.IconAndOptionsFrame.OptionsFrame.sizeParametersFrame.sizeParameters1Frame.sizeUserWidthSpinner.Help="Definiert die Bildbreite";
sizeFrame.IconAndOptionsFrame.OptionsFrame.sizeParametersFrame.sizeParameters1Frame.sizeUserWidthSpinner.ShortHelp="Bildbreite";
sizeFrame.IconAndOptionsFrame.OptionsFrame.sizeParametersFrame.sizeParameters1Frame.sizeUserWidthSpinner.LongHelp="Definiert die Bildbreite in Pixel.";

sizeFrame.IconAndOptionsFrame.OptionsFrame.sizeParametersFrame.sizeParameters1Frame.sizeUserHeightSpinner.Help="Definiert die Bildh�he";
sizeFrame.IconAndOptionsFrame.OptionsFrame.sizeParametersFrame.sizeParameters1Frame.sizeUserHeightSpinner.ShortHelp="Bildh�he";
sizeFrame.IconAndOptionsFrame.OptionsFrame.sizeParametersFrame.sizeParameters1Frame.sizeUserHeightSpinner.LongHelp="Definiert die Bildh�he in Pixel.";

sizeFrame.IconAndOptionsFrame.OptionsFrame.sizeParametersFrame.sizeParameters1Frame.sizeUserXLabel.Title=" x ";
sizeFrame.IconAndOptionsFrame.OptionsFrame.sizeParametersFrame.sizeParameters1Frame.sizeUserUnitLabel.Title="Pixel  ";

sizeFrame.IconAndOptionsFrame.OptionsFrame.sizeParametersFrame.sizeParameters2Frame.sizeRatioLabel.Title="Faktor";
sizeFrame.IconAndOptionsFrame.OptionsFrame.sizeParametersFrame.sizeParameters2Frame.sizeRatioPredefinedRadio.Title="Vordefiniert:";
sizeFrame.IconAndOptionsFrame.OptionsFrame.sizeParametersFrame.sizeParameters2Frame.sizeRatioCustomRadio.Title="Angepasst: ";

sizeFrame.IconAndOptionsFrame.OptionsFrame.sizeParametersFrame.sizeParameters2Frame.sizePredefinedCombo.Help="Vordefinierte Verh�ltnisse";
sizeFrame.IconAndOptionsFrame.OptionsFrame.sizeParametersFrame.sizeParameters2Frame.sizePredefinedCombo.ShortHelp="Vordefinierte Verh�ltnisse";
sizeFrame.IconAndOptionsFrame.OptionsFrame.sizeParametersFrame.sizeParameters2Frame.sizePredefinedCombo.LongHelp="Listet die vordefinierten Verh�ltnisse auf.";

size1="A4";
size2="6 x 6";
size3="Standard";
size4="4/3";
size5="24 x 36";
size6="16 mm";
size7="16/9";
size8="HDTV";
size9="Panavision";
size10="Panorama";
size11="CCD 1/2";
size12="CCD 1/4";

sizeFrame.IconAndOptionsFrame.OptionsFrame.sizeParametersFrame.sizeParameters2Frame.sizeRatioSpinner.Help="Verh�ltnis zwischen H�he und Breite";
sizeFrame.IconAndOptionsFrame.OptionsFrame.sizeParametersFrame.sizeParameters2Frame.sizeRatioSpinner.ShortHelp="Verh�ltnis der Bildgr��e";
sizeFrame.IconAndOptionsFrame.OptionsFrame.sizeParametersFrame.sizeParameters2Frame.sizeRatioSpinner.LongHelp="Verh�ltnis zwischen H�he und Breite.";

sizePreviewLabel="Bildschirm";

//--------------------------------------
// Output frame
//--------------------------------------
outputFrame.HeaderFrame.Global.Title="Ausgabe";

outputFrame.IconAndOptionsFrame.OptionsFrame.outputParametersFrame.outputScreenRadio.Title="Am Bildschirm";
outputFrame.IconAndOptionsFrame.OptionsFrame.outputParametersFrame.outputScreenRadio.Help="Wiedergabe erfolgt am Bildschirm";
outputFrame.IconAndOptionsFrame.OptionsFrame.outputParametersFrame.outputScreenRadio.ShortHelp="Ausgabe am Bildschirm";
outputFrame.IconAndOptionsFrame.OptionsFrame.outputParametersFrame.outputScreenRadio.LongHelp="Wiedergabe erfolgt am Bildschirm.";

outputFrame.IconAndOptionsFrame.OptionsFrame.outputParametersFrame.outputDiskRadio.Title="Auf Datentr�ger";
outputFrame.IconAndOptionsFrame.OptionsFrame.outputParametersFrame.outputDiskRadio.Help="Wiedergabe erfolgt auf Datentr�ger";
outputFrame.IconAndOptionsFrame.OptionsFrame.outputParametersFrame.outputDiskRadio.ShortHelp="Ausgabe auf Datentr�ger";
outputFrame.IconAndOptionsFrame.OptionsFrame.outputParametersFrame.outputDiskRadio.LongHelp="Wiedergabe erfolgt auf Datentr�ger.";

outputFrame.IconAndOptionsFrame.OptionsFrame.outputParametersFrame.outputFileFrame.outputDirectoryLabel.Title="Verzeichnis:";

outputFrame.IconAndOptionsFrame.OptionsFrame.outputParametersFrame.outputFileFrame.outputDirectoryEditor.Help="Ausgabeverzeichnis";
outputFrame.IconAndOptionsFrame.OptionsFrame.outputParametersFrame.outputFileFrame.outputDirectoryEditor.ShortHelp="Ausgabeverzeichnis";
outputFrame.IconAndOptionsFrame.OptionsFrame.outputParametersFrame.outputFileFrame.outputDirectoryEditor.LongHelp="Definiert das Verzeichnis, in dem wiedergegebene Bilder gesichert werden.";

outputFrame.IconAndOptionsFrame.OptionsFrame.outputParametersFrame.outputFileFrame.outputDirectoryButton.Help="Durchsuchen";
outputFrame.IconAndOptionsFrame.OptionsFrame.outputParametersFrame.outputFileFrame.outputDirectoryButton.ShortHelp="Durchsuchen";
outputFrame.IconAndOptionsFrame.OptionsFrame.outputParametersFrame.outputFileFrame.outputDirectoryButton.LongHelp="Durchsucht das Verzeichnis, in dem wiedergegebene Bilder gesichert werden.";

outputFrame.IconAndOptionsFrame.OptionsFrame.outputParametersFrame.outputFileFrame.outputNameLabel.Title="Name:";

outputFrame.IconAndOptionsFrame.OptionsFrame.outputParametersFrame.outputFileFrame.outputNameEditor.Help="Dateiname des wiedergegebenen Bilds";
outputFrame.IconAndOptionsFrame.OptionsFrame.outputParametersFrame.outputFileFrame.outputNameEditor.ShortHelp="Name des wiedergegebenen Bilds";
outputFrame.IconAndOptionsFrame.OptionsFrame.outputParametersFrame.outputFileFrame.outputNameEditor.LongHelp="Definiert den Namen des wiedergegebenen Bilds.";

outputFrame.IconAndOptionsFrame.OptionsFrame.outputParametersFrame.outputFileFrame.outputSingleTypeLabel.Title="Bildformat:";

outputFrame.IconAndOptionsFrame.OptionsFrame.outputParametersFrame.outputFileFrame.outputSingleTypeCombo.Help="Dateiformat von gesicherten Bildern";
outputFrame.IconAndOptionsFrame.OptionsFrame.outputParametersFrame.outputFileFrame.outputSingleTypeCombo.ShortHelp="Ausgabeformat f�r Bilder";
outputFrame.IconAndOptionsFrame.OptionsFrame.outputParametersFrame.outputFileFrame.outputSingleTypeCombo.LongHelp="Definiert das Dateiformat der gesicherten Bilder.";

outputFrame.IconAndOptionsFrame.OptionsFrame.outputParametersFrame.outputFileFrame.outputAnimationTypeLabel.Title="Format der Animation:";

outputFrame.IconAndOptionsFrame.OptionsFrame.outputParametersFrame.outputFileFrame.outputAnimationTypeCombo.Help="Dateiformat von gesicherten Animationen";
outputFrame.IconAndOptionsFrame.OptionsFrame.outputParametersFrame.outputFileFrame.outputAnimationTypeCombo.ShortHelp="Ausgabeformat f�r Animationen";
outputFrame.IconAndOptionsFrame.OptionsFrame.outputParametersFrame.outputFileFrame.outputAnimationTypeCombo.LongHelp="Definiert das Dateiformat der gesicherten Animationen.";

allLabel="Alle Bilder (*.*)";
tifLabel="TIFF-Dateien (echte Farbe) (*.tif)";
tifPackbitsLabel="TIFF-Dateien (echte Farbe, komprimiert) (*.tif)";
rgbLabel="SGI-Format (*.rgb)";
bmpLabel="Windows-Bitmap (*.bmp)";
jpgLabel="JPEG (*.jpg)";
jpgFairQualityLabel="JPEG-Dateien (Normale Qualit�t)(*.jpg)";
jpgMediumQualityLabel="JPEG-Dateien (Mittlere Qualit�t)(*.jpg)";
jpgHighQualityLabel="JPEG-Dateien (Hohe Qualit�t)(*.jpg)";
picLabel="Apple Macintosh-Format (*.pic)";
psdLabel="Adobe Photoshop-Format (*.psd)";
pngLabel="Portable Network Graphics (*.png)";
tgaLabel="Truevision Targa (*.tga)";

sequenceLabel="Bildfolge";
mpegLabel="MPEG-1 Video (*.mpg)";
aviLabel="Microsoft AVI (*.avi)";
quickTimeLabel="Apple QuickTime (*.mov)";
movieLabel="SGI Movie (*.movie)";

outputFrame.IconAndOptionsFrame.OptionsFrame.outputParametersFrame.outputFileFrame.dpiLabel.Title="DPI: ";

outputFrame.IconAndOptionsFrame.OptionsFrame.outputParametersFrame.outputFileFrame.dpiSpinner.Help="Pixeldichte:";
outputFrame.IconAndOptionsFrame.OptionsFrame.outputParametersFrame.outputFileFrame.dpiSpinner.ShortHelp="Pixeldichte:";
outputFrame.IconAndOptionsFrame.OptionsFrame.outputParametersFrame.outputFileFrame.dpiSpinner.LongHelp="Definiert die Pixeldichte in Pixel pro Zoll.";

outputFrame.IconAndOptionsFrame.OptionsFrame.outputParametersFrame.outputFileFrame.colorProfileLabel.Title="Farbmodus: ";

outputFrame.IconAndOptionsFrame.OptionsFrame.outputParametersFrame.outputFileFrame.colorProfileCombo.Help="Farbmodus";
outputFrame.IconAndOptionsFrame.OptionsFrame.outputParametersFrame.outputFileFrame.colorProfileCombo.ShortHelp="Farbmodus";
outputFrame.IconAndOptionsFrame.OptionsFrame.outputParametersFrame.outputFileFrame.colorProfileCombo.LongHelp="Definiert den Farbmodus f�r das Sichern von Bildern.";
