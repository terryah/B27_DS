//=============================================================================
// COPYRIGHT DASSAULT SYSTEMES  2005
//=============================================================================
//
// CATStOLDTopoFilletDialog: Resource file for NLS purpose related to TopoFillet command
//
//=============================================================================
//
// Implementation Notes:
//
//=============================================================================
// March 05   Creation                                    Ritesh Kanetkar
//=============================================================================
DialogBoxTitle="Styling-Verrundung";
TabCont.GeneralTab.Title="Eingaben f�r die Verrundung";
TabCont.ApproxTab.Title="N�herung";

TabCont.GeneralTab.MainFrame.ContinuityFrame.Title="Toleranz";
TabCont.GeneralTab.MainFrame.RadiusFrame.Title="Radius";
TabCont.GeneralTab.MainFrame.ArcTypeFrame.Title="Bogentyp";
TabCont.GeneralTab.MainFrame.ParameterFrame.Title="Parameter";
TabCont.GeneralTab.MainFrame.ResultFrame.Title="Ergebnis";
TabCont.GeneralTab.MainFrame.OptionFrame.Title="Option";
TabCont.GeneralTab.MainFrame.SelectionFrame.Title="Auswahl";

TabCont.GeneralTab.MainFrame.ParameterFrame.ParameterCombo.PDefault.Title="Standard";
TabCont.GeneralTab.MainFrame.ParameterFrame.ParameterCombo.PPatch1.Title="Fl�chenst�ck 1";
TabCont.GeneralTab.MainFrame.ParameterFrame.ParameterCombo.PPatch2.Title="Fl�chenst�ck 2";
TabCont.GeneralTab.MainFrame.ParameterFrame.ParameterCombo.PAverage.Title="Durchschnittlich";
TabCont.GeneralTab.MainFrame.ParameterFrame.ParameterCombo.PBlend.Title="�bergang";
TabCont.GeneralTab.MainFrame.ParameterFrame.ParameterCombo.PChordal.Title="Abstand";

TabCont.GeneralTab.MainFrame.ResultFrame.TrimCombo.TrimFace.Title="Teilfl�che trimmen";
TabCont.GeneralTab.MainFrame.ResultFrame.TrimCombo.TrimApprox.Title="Angen�hert";

TabCont.GeneralTab.MainFrame.ResultFrame.SmallSurCombo.SSStitch.Title="Heften";
TabCont.GeneralTab.MainFrame.ResultFrame.SmallSurCombo.SSRip.Title="Auftrennung";

TabCont.GeneralTab.MainFrame.ContinuityFrame.G0Radio.ShortHelp="G0";
TabCont.GeneralTab.MainFrame.ContinuityFrame.G0Radio.LongHelp="G0-Stetigkeit zwischen der Verrundung und den Eingabeelementen";
TabCont.GeneralTab.MainFrame.ContinuityFrame.G1Radio.ShortHelp="G1";
TabCont.GeneralTab.MainFrame.ContinuityFrame.G1Radio.LongHelp="G1-Stetigkeit zwischen der Verrundung und den Eingabeelementen";
TabCont.GeneralTab.MainFrame.ContinuityFrame.G2Radio.ShortHelp="G2";
TabCont.GeneralTab.MainFrame.ContinuityFrame.G2Radio.LongHelp="G2-Stetigkeit zwischen der Verrundung und den Eingabeelementen";
TabCont.GeneralTab.MainFrame.ContinuityFrame.G3Radio.ShortHelp="G3";
TabCont.GeneralTab.MainFrame.ContinuityFrame.G3Radio.LongHelp="G3-Stetigkeit zwischen der Verrundung und den Eingabeelementen";


TabCont.GeneralTab.MainFrame.RadiusFrame.Radius.ShortHelp="Konstanter Radius";
TabCont.GeneralTab.MainFrame.RadiusFrame.Radius.LongHelp="Konstanter Radius f�r die Verrundung";
TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadFlag.ShortHelp="Minimaler Radius";
TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadFlag.LongHelp="Minimalen Radius ein-/ausschalten";
TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadius.ShortHelp="Wert des minimalen Radius";
TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadius.LongHelp="Wert des minimalen Radius";
TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadRelativeFlag.ShortHelp="Relativer Radius";
TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadRelativeFlag.LongHelp="Relativen Radius ein-/ausschalten";
TabCont.GeneralTab.MainFrame.RadiusFrame.VariableRadChkBtn.ShortHelp="Variabler Radius";
TabCont.GeneralTab.MainFrame.RadiusFrame.VariableRadChkBtn.LongHelp="Variablen Radius ein-/ausschalten";

TabCont.GeneralTab.MainFrame.ArcTypeFrame.BlendRadio.ShortHelp="�bergang";
TabCont.GeneralTab.MainFrame.ArcTypeFrame.BlendRadio.LongHelp="Erzeugt eine �bergangsfl�che zwischen den Eingabeelementen";
TabCont.GeneralTab.MainFrame.ArcTypeFrame.ApproxRadio.ShortHelp="Angen�hert";
TabCont.GeneralTab.MainFrame.ArcTypeFrame.ApproxRadio.LongHelp="Erzeugt eine Bezier-Ann�herungskurve zwischen den Eingabeelementen";
TabCont.GeneralTab.MainFrame.ArcTypeFrame.ExactRadio.ShortHelp="Exakt";
TabCont.GeneralTab.MainFrame.ArcTypeFrame.ExactRadio.LongHelp="Erzeugt eine rationale Fl�che mit ma�genauen kreisf�rmigen Schnitten";

TabCont.GeneralTab.MainFrame.ParameterFrame.ParameterCombo.ShortHelp="Parameter";
TabCont.GeneralTab.MainFrame.ParameterFrame.ParameterCombo.LongHelp=" Unterschiedliche Optionen zur Parameterangabe f�r die Verrundungsfl�che";

TabCont.GeneralTab.MainFrame.ResultFrame.TrimSwitch.ShortHelp="Ergebnis trimmen";
TabCont.GeneralTab.MainFrame.ResultFrame.TrimSwitch.LongHelp="Option 'Ergebnis trimmen' ein-/ausschalten";
TabCont.GeneralTab.MainFrame.ResultFrame.TrimCombo.ShortHelp="Teilfl�che trimmen/Ann�herung trimmen";
TabCont.GeneralTab.MainFrame.ResultFrame.TrimCombo.LongHelp="Typ des Ergebnisses der Option 'Teilfl�che trimmen' - Ergebnis ist eine Teilfl�che
                                                                Ann�herung trimmen - Ergebnis ist keine Teilfl�che";
TabCont.GeneralTab.MainFrame.ResultFrame.Extrapol.ShortHelp="Extrapolieren";
TabCont.GeneralTab.MainFrame.ResultFrame.Extrapol.LongHelp="Extrapoliert die resultierende Verrundungsfl�che";
TabCont.GeneralTab.MainFrame.ResultFrame.SmallSur.ShortHelp="Kleine Fl�che";
TabCont.GeneralTab.MainFrame.ResultFrame.SmallSur.LongHelp="Wenn beide Eingabesets mehr als eine Fl�che enthalten und sich die Begrenzungen der Fl�chen 
nicht an derselben Position befinden, ist das Ergebnis eine (sehr) kleine Fl�che. Modus: Ein/Aus";
TabCont.GeneralTab.MainFrame.ResultFrame.SmallSurCombo.ShortHelp="Heften/Auftrennen";
TabCont.GeneralTab.MainFrame.ResultFrame.SmallSurCombo.LongHelp="Heften: Die kleine Fl�che kann mit einer der benachbarten gr��eren Fl�chen zusammengeheftet werden
     Auftrennen: Die kleine Fl�che wird (entlang ihrer Diagonalen) in zwei Fl�chen aufgeteilt";
TabCont.GeneralTab.MainFrame.ResultFrame.SmallSurfaceTolerancee.ShortHelp="Toleranzwert f�r kleine Fl�che";
TabCont.GeneralTab.MainFrame.ResultFrame.SmallSurfaceTolerancee.LongHelp="Toleranzwert f�r die Erzeugung der kleinen Fl�che";

TabCont.GeneralTab.MainFrame.OptionFrame.TrimBase.ShortHelp="Eingabeelemente trimmen";
TabCont.GeneralTab.MainFrame.OptionFrame.TrimBase.LongHelp="Trimmt die Eingabeelemente der Verrundung";
TabCont.GeneralTab.MainFrame.OptionFrame.ChordalFillet.ShortHelp="Abstandsverrundung";
TabCont.GeneralTab.MainFrame.OptionFrame.ChordalFillet.LongHelp="Der Abstand zwischen den Kanten der Verrundung in Richtung des Rollballs stellt den Sehnenabstand dar";
TabCont.GeneralTab.MainFrame.OptionFrame.CtrlPts.ShortHelp="Kontrollpunkte";
TabCont.GeneralTab.MainFrame.OptionFrame.CtrlPts.LongHelp="Aktiviert die Form�nderungen f�r die Verrundungsfl�che";
TabCont.GeneralTab.MainFrame.OptionFrame.TrueMinimalRadius.ShortHelp="Ma�st�bliches Minimum";
TabCont.GeneralTab.MainFrame.OptionFrame.TrueMinimalRadius.LongHelp="Mindestradius wird gesteuert";
TabCont.GeneralTab.MainFrame.OptionFrame.IndependentApproximation.ShortHelp="Unabh�ngige Ann�herung";
TabCont.GeneralTab.MainFrame.OptionFrame.IndependentApproximation.LongHelp="Unabh�ngige interne Ann�herung";

TabCont.GeneralTab.MainFrame.SelectionFrame.FirstSetSur.ShortHelp="Fl�chenset ausw�hlen";
TabCont.GeneralTab.MainFrame.SelectionFrame.FirstSetSur.LongHelp="Erstes Fl�chenset f�r die Verrundungsoperation ausw�hlen";
TabCont.GeneralTab.MainFrame.SelectionFrame.SecondSetSur.ShortHelp="Fl�chenset ausw�hlen";
TabCont.GeneralTab.MainFrame.SelectionFrame.SecondSetSur.LongHelp="Zweites Fl�chenset f�r die Verrundungsoperation ausw�hlen";
TabCont.GeneralTab.MainFrame.SelectionFrame.FirstSetCur.ShortHelp="Kurvenset ausw�hlen";
TabCont.GeneralTab.MainFrame.SelectionFrame.FirstSetCur.LongHelp="Erstes Kurvenset f�r die Verrundungsoperation ausw�hlen";
TabCont.GeneralTab.MainFrame.SelectionFrame.SecondSetCur.ShortHelp="Kurvenset ausw�hlen";
TabCont.GeneralTab.MainFrame.SelectionFrame.SecondSetCur.LongHelp="Zweites Kurvenset f�r die Verrundungsoperation ausw�hlen";
TabCont.GeneralTab.MainFrame.SelectionFrame.SpineSet.ShortHelp="Leitkurvenset ausw�hlen";
TabCont.GeneralTab.MainFrame.SelectionFrame.SpineSet.LongHelp="Die Leitkurve f�r die Verrundungsoperation ausw�hlen";
TabCont.GeneralTab.MainFrame.SelectionFrame.PtOnSpineSet.ShortHelp="Punkt auf Leitkurve ausw�hlen";
TabCont.GeneralTab.MainFrame.SelectionFrame.PtOnSpineSet.LongHelp="F�r die Verrundungsoperation einen Punkt auf der Leitkurve ausw�hlen";


// New UI

MainOuterFrameTabs.TabCont.GeneralTab.Title="Optionen";
// MainOuterFrameTabs.TabCont.ApproxTab.Title = "Advanced"; // WAP 08:08:26 R19SP4 HL2 - Nls entry commented.


// Continuity Frame
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContinuityFrame.Title="Durchgang";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContinuityFrame.G0Radio.ShortHelp="G0";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContinuityFrame.G0Radio.LongHelp="G0-Stetigkeit zwischen der Verrundung und den Eingabeelementen";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContinuityFrame.G1Radio.ShortHelp="G1";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContinuityFrame.G1Radio.LongHelp="G1-Stetigkeit zwischen der Verrundung und den Eingabeelementen";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContinuityFrame.G2Radio.ShortHelp="G2";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContinuityFrame.G2Radio.LongHelp="G2-Stetigkeit zwischen der Verrundung und den Eingabeelementen";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContinuityFrame.G3Radio.ShortHelp="G3";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ContinuityFrame.G3Radio.LongHelp="G3-Stetigkeit zwischen der Verrundung und den Eingabeelementen";

// Radius Frame
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.Title="Radiusparameter";
//MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.RadiusLabel.Title = "Radius :"; // WAP 08:11:25 R19SP3 HL
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.RadiusLabel.ShortHelp="Konstanter Radius";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.RadiusLabel.LongHelp="Konstanter Radius f�r die Verrundung";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.Radius.ShortHelp="Konstanter Radius";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.Radius.LongHelp="Konstanter Radius f�r die Verrundung";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadFlag.Title="Min. Radius:";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadFlag.ShortHelp="Minimaler Radius";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadFlag.LongHelp="Minimalen Radius ein-/ausschalten";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadius.ShortHelp="Wert des minimalen Radius";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadius.LongHelp="Wert des minimalen Radius";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadRelativeFlag.ShortHelp="Relativer Radius";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.RadiusFrame.MinRadRelativeFlag.LongHelp="Relativen Radius ein-/ausschalten";

// Arc Type 
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.ArcTypeFrame.Title="Bogentyp";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.ArcTypeFrame.BlendRadio.ShortHelp="�bergang";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.ArcTypeFrame.BlendRadio.LongHelp="Erzeugt eine �bergangsfl�che zwischen den Eingabeelementen";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.ArcTypeFrame.ApproxRadio.ShortHelp="Angen�hert";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.ArcTypeFrame.ApproxRadio.LongHelp="Erzeugt eine Bezier-Ann�herungskurve zwischen den Eingabeelementen";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.ArcTypeFrame.ExactRadio.ShortHelp="Exakt";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.ArcTypeFrame.ExactRadio.LongHelp="Erzeugt eine rationale Fl�che mit ma�genauen kreisf�rmigen Schnitten";


MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.Title="Ergebnis";

// Simplification Frame
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.Title="Vereinfachung";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.SmallSurStich.Title="Heften";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.SmallSurStich.ShortHelp="Kleine Fl�che - Heftung";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.SmallSurStich.LongHelp="Heftung: Die kleine Fl�che kann mit einer der benachbarten Fl�chen zusammengeheftet werden";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.SmallSurRip.Title="Auftrennung";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.SmallSurRip.ShortHelp="Kleine Fl�che - Auftrennung";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.SmallSurRip.LongHelp="Auftrennung: Die kleine Fl�che wird (entlang ihrer Diagonalen) in zwei Fl�chen aufgeteilt";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.SmallSurfaceTolerancee.ShortHelp="Toleranzwert f�r kleine Fl�che";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.SmallSurfaceTolerancee.LongHelp="Toleranzwert f�r die Erzeugung der kleinen Fl�che";

// Relimitation Frame
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.RelimitationFrame.Title="Neubegrenzung";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.RelimitationFrame.TrimFace.Title="Teilfl�che trimmen";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.RelimitationFrame.TrimFace.ShortHelp="Teilfl�che trimmen";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.RelimitationFrame.TrimFace.LongHelp="Das Ergebnis ist eine Teilfl�che";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.RelimitationFrame.TrimApprox.Title="Ann�herung trimmen";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.RelimitationFrame.TrimApprox.ShortHelp="Ann�herung trimmen";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.RelimitationFrame.TrimApprox.LongHelp="Das Ergebnis ist keine Teilfl�che";
// For R18SP3 UI
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.Title="Geometrie";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.Extrapol1.ShortHelp="Seite1 extrapolieren";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.Extrapol1.LongHelp="Das Ergebnis wird auf Seite1 extrapoliert";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.Extrapol2.ShortHelp="Seite2 extrapolieren";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.Extrapol2.LongHelp="Das Ergebnis wird auf Seite2 extrapoliert";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.Relimit1.ShortHelp="Seite1 neu begrenzen";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.Relimit1.LongHelp="Das Ergebnis wird auf Seite1 neu begrenzt";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.Relimit2.ShortHelp="Seite2 neu begrenzen";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.Relimit2.LongHelp="Das Ergebnis wird auf Seite2 neu begrenzt";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.TrimFace.Title="Teilfl�che trimmen";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.TrimFace.ShortHelp="Teilfl�che trimmen";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.TrimFace.LongHelp="Das Ergebnis ist eine Teilfl�che";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.TrimApprox.Title="Ann�herung trimmen";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.TrimApprox.ShortHelp="Ann�herung trimmen";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.TrimApprox.LongHelp="Das Ergebnis ist keine Teilfl�che";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.Merge.ShortHelp="Verketten";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.Merge.LongHelp="Verkettet ein Verrundungsband mit mehreren Zellen zu einem Ergebnis mit einer Zelle (nur bei G2-Stetigkeit zwischen Zellen m�glich)";

// Geometry Frame
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.GeometryFrame.Title="Geometrie";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.GeometryFrame.Extrapol.Title="Extrapolieren";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.GeometryFrame.Extrapol.ShortHelp="Extrapolieren";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.GeometryFrame.Extrapol.LongHelp="Extrapoliert die resultierende Verrundungsfl�che";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.GeometryFrame.Merge.ShortHelp="Verketten";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.GeometryFrame.Merge.LongHelp="Verkettet ein Verrundungsband mit mehreren Zellen zu einem Ergebnis mit einer Zelle (nur bei G2-Stetigkeit zwischen Zellen m�glich)";
//--------------------------------------

// Options Frame
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.Title="Verrundungstyp";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.VariableRadChkBtn.ShortHelp="Variabler Radius";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.VariableRadChkBtn.LongHelp="Variablen Radius ein-/ausschalten";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.TrimBase.ShortHelp="Eingabeelemente trimmen";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.TrimBase.LongHelp="Trimmt die Eingabeelemente der Verrundung";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.ChordalFillet.ShortHelp="Abstandsverrundung";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.ChordalFillet.LongHelp="Der Abstand zwischen den Kanten der Verrundung in Richtung des Rollballs stellt den Sehnenabstand dar";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.CtrlPts.ShortHelp="Kontrollpunkte";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.CtrlPts.LongHelp="Aktiviert die Form�nderungen f�r die Verrundungsfl�che";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.TrueMinimalRadius.ShortHelp="Ma�st�bliches Minimum";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.TrueMinimalRadius.LongHelp="Mindestradius wird gesteuert";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.IndependentApproximation.ShortHelp="Unabh�ngige Ann�herung";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.FrameArcTypeAndOpt.OptionFrame.IndependentApproximation.LongHelp="Unabh�ngige interne Ann�herung";


// Surface-1 Curv-1
//MainOuterFrame.Surface1Curve1Frame.Title = "Support 1";
//MainOuterFrame.Surface1Curve1Frame.ShortHelp = "Select surface and curve set 1";
//MainOuterFrame.Surface1Curve1Frame.LongHelp = "Select first set of surfaces and curves for fillet operation";
//MainOuterFrame.Surface1Curve1Frame.Surface1Selector.ShortHelp = "Select surface set 1";
//MainOuterFrame.Surface1Curve1Frame.Surface1Selector.LongHelp = "Select first set of surfaces for fillet operation";
MainOuterFrame.Surface1Curve1Frame.Curve1Selector.ShortHelp="Kurvenset ausw�hlen"; // WAP 09:01:23
MainOuterFrame.Surface1Curve1Frame.Curve1Selector.LongHelp="Erstes Kurvenset f�r die Verrundungsoperation ausw�hlen"; // WAP 09:01:23 
//MainOuterFrame.Surface1Curve1Frame.LabelSurface1.Title = "Surface :";
//MainOuterFrame.Surface1Curve1Frame.LabelSurface1.ShortHelp = "Select surface set 1";
//MainOuterFrame.Surface1Curve1Frame.LabelSurface1.LongHelp = "Select first set of surfaces for fillet operation";
MainOuterFrame.Surface1Curve1Frame.LabelCurve1.Title="Kurve:";
MainOuterFrame.Surface1Curve1Frame.LabelCurve1.ShortHelp="Kurvenset ausw�hlen";
MainOuterFrame.Surface1Curve1Frame.LabelCurve1.LongHelp="Erstes Kurvenset f�r die Verrundungsoperation ausw�hlen";
MainOuterFrame.Surface1Curve1Frame.TrimBase1.ShortHelp="St�tzelement 1 trimmen";
MainOuterFrame.Surface1Curve1Frame.TrimBase1.LongHelp="Trimmt die Eingabeelemente (Fl�chen) im St�tzelementeset 1 der Verrundung.";
//MainOuterFrame.Surface1Curve1Frame.Curve1SelState.ShortHelp = "Enable curve 1 set";// WAP 09:01:23 
//MainOuterFrame.Surface1Curve1Frame.Curve1SelState.LongHelp = "Enables selection in the curve 1 set.";// WAP 09:01:23 

// Surface-2 Curv-2
MainOuterFrame.Surface2Curve2Frame.Title="St�tzelement 2";
MainOuterFrame.Surface2Curve2Frame.ShortHelp="Fl�chen- und Kurvenset 2 ausw�hlen";
MainOuterFrame.Surface2Curve2Frame.LongHelp="Zweites Fl�chen- und Kurvenset f�r die Verrundungsoperation ausw�hlen";
MainOuterFrame.Surface2Curve2Frame.Surface2Selector.ShortHelp="Fl�chenset ausw�hlen";
MainOuterFrame.Surface2Curve2Frame.Surface2Selector.LongHelp="Zweites Fl�chenset f�r die Verrundungsoperation ausw�hlen";
//MainOuterFrame.Surface2Curve2Frame.Curve2Selector.ShortHelp = "Select curve set 2"; // WAP 09:01:23
//MainOuterFrame.Surface2Curve2Frame.Curve2Selector.LongHelp = "Select second set of curves for fillet operation"; // WAP 09:01:23
MainOuterFrame.Surface2Curve2Frame.LabelSurface2.Title="Fl�che:";
MainOuterFrame.Surface2Curve2Frame.LabelSurface2.ShortHelp="Fl�chenset ausw�hlen";
MainOuterFrame.Surface2Curve2Frame.LabelSurface2.LongHelp="Zweites Fl�chenset f�r die Verrundungsoperation ausw�hlen";
MainOuterFrame.Surface2Curve2Frame.LabelCurve2.Title="Kurve:";
MainOuterFrame.Surface2Curve2Frame.LabelCurve2.ShortHelp="Kurvenset ausw�hlen";
MainOuterFrame.Surface2Curve2Frame.LabelCurve2.LongHelp="Zweites Kurvenset f�r die Verrundungsoperation ausw�hlen";
MainOuterFrame.Surface2Curve2Frame.TrimBase2.ShortHelp="St�tzelement 2 trimmen";
MainOuterFrame.Surface2Curve2Frame.TrimBase2.LongHelp="Trimmt die Eingabeelemente (Fl�chen) im St�tzelementeset 2 der Verrundung.";
//MainOuterFrame.Surface2Curve2Frame.Curve2SelState.ShortHelp = "Enable curve 2 set";// WAP 09:01:23 
//MainOuterFrame.Surface2Curve2Frame.Curve2SelState.LongHelp = "Enables selection in the curve 2 set.";// WAP 09:01:23 

// Point-Spine
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.SpinePointFrame.SpineSelector.ShortHelp="Leitkurvenset ausw�hlen";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.SpinePointFrame.SpineSelector.LongHelp="Die Leitkurve f�r die Verrundungsoperation ausw�hlen";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.SpinePointFrame.PointSelector.ShortHelp="Punkt auf Leitkurve ausw�hlen";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.SpinePointFrame.PointSelector.LongHelp="F�r die Verrundungsoperation einen Punkt auf der Leitkurve ausw�hlen";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.SpinePointFrame.LabelSpine.Title="Leitkurve:";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.SpinePointFrame.LabelSpine.ShortHelp="Leitkurvenset ausw�hlen";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.SpinePointFrame.LabelSpine.LongHelp="Die Leitkurve f�r die Verrundungsoperation ausw�hlen";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.SpinePointFrame.LabelPoint.Title="Punkt:";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.SpinePointFrame.LabelPoint.ShortHelp="Punkt auf Leitkurve ausw�hlen";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.SpinePointFrame.LabelPoint.LongHelp="F�r die Verrundungsoperation einen Punkt auf der Leitkurve ausw�hlen";

// Parameters
MainOuterFrameTabs.TabCont.ApproxTab.ParameterFrame.Title="Parameter";
MainOuterFrameTabs.TabCont.ApproxTab.ParameterFrame.ParameterCombo.ShortHelp="Parameter";
MainOuterFrameTabs.TabCont.ApproxTab.ParameterFrame.ParameterCombo.LongHelp=" Unterschiedliche Optionen zur Parameterangabe f�r die Verrundungsfl�che";
MainOuterFrameTabs.TabCont.ApproxTab.ParameterFrame.ParameterCombo.PDefault.Title="Standard";
MainOuterFrameTabs.TabCont.ApproxTab.ParameterFrame.ParameterCombo.PPatch1.Title="Fl�chenst�ck 1";
MainOuterFrameTabs.TabCont.ApproxTab.ParameterFrame.ParameterCombo.PPatch2.Title="Fl�chenst�ck 2";
MainOuterFrameTabs.TabCont.ApproxTab.ParameterFrame.ParameterCombo.PAverage.Title="Durchschnittlich";
MainOuterFrameTabs.TabCont.ApproxTab.ParameterFrame.ParameterCombo.PBlend.Title="�bergang";
MainOuterFrameTabs.TabCont.ApproxTab.ParameterFrame.ParameterCombo.PChordal.Title="Abstand";

// Independent Approximation
MainOuterFrameTabs.TabCont.ApproxTab.IndependentApproximation.Title="Unabh�ngige Ann�herung";
MainOuterFrameTabs.TabCont.ApproxTab.IndependentApproximation.ShortHelp="Unabh�ngige Ann�herung";
MainOuterFrameTabs.TabCont.ApproxTab.IndependentApproximation.LongHelp="Unabh�ngige interne Ann�herung";

Surface1BagTitle.Message="Fl�che 1 ausw�hlen";
Curve1BagTitle.Message="Kurve 1 ausw�hlen";
Surface2BagTitle.Message="Fl�che 2 ausw�hlen";
Curve2BagTitle.Message="Kurve 2 ausw�hlen";
SpineBagTitle.Message="Leitkurve ausw�hlen";
PointOnSpineBagTitle.Message="Punkt auf Leitkurve ausw�hlen";

MaxDevOptionsFrame.MaxDevDisplayOptionsFrame.Title="Abweichungsanzeige";
MaxDevOptionsFrame.MaxDevDisplayOptionsFrame.DevBetFilletCellsChkB.ShortHelp="Verbindung zwischen Verrundungszellen";
MaxDevOptionsFrame.MaxDevDisplayOptionsFrame.DevBetFilletCellsChkB.LongHelp="Zeigt nur die Verbindung zwischen Verrundungsbandzellen an";
MaxDevOptionsFrame.MaxDevDisplayOptionsFrame.DevBetFilletAndSupportChkB.ShortHelp="Verbindung zwischen Verrundungsband und St�tzelement";
MaxDevOptionsFrame.MaxDevDisplayOptionsFrame.DevBetFilletAndSupportChkB.LongHelp="Zeigt die Verbindung zwischen Verrundungsband und St�tzelement an";
MaxDevOptionsFrame.MaxDevValueFrame.Title="Max. Abweichung";
MaxDevOptionsFrame.MaxDevValueFrame.LongHelp="Zeigt das Maximum des H�chstwerts f�r jede Stetigkeit an";
MaxDevOptionsFrame.MaxDevValueFrame.G0TitleLabel.Title="G0:";
MaxDevOptionsFrame.MaxDevValueFrame.G0Label="----   ";
MaxDevOptionsFrame.MaxDevValueFrame.G1TitleLabel.Title="G1:";
MaxDevOptionsFrame.MaxDevValueFrame.G2TitleLabel.Title="G2:";
MaxDevOptionsFrame.MaxDevValueFrame.G3TitleLabel.Title="G3:";

// Start WAP 08:08:26 R19SP4 HL1 AND HL2

NewApproxTab.Message="N�herung";
ApproxTab.Message="Erweitert";
OutputResultFrame.Title="Ausgabeergebnis";
OutputResultFrame.NbOfCellsTitleLabel.Title="Anz. Zellen:";
OutputResultFrame.MaxOrderTitleLabel.Title="Max. Grad:";
OutputResultFrame.MaxSegTitleLabel.Title="Max. Anzahl Segmente pro Zelle:";
OutputResultFrame.OrdUTitleLabel.Title="U:";
OutputResultFrame.OrdVTitleLabel.Title="V:";
OutputResultFrame.SegUTitleLabel.Title="U:";
OutputResultFrame.SegVTitleLabel.Title="V:";
OutputResultFrame.NbOfCellsTitleLabel.ShortHelp="Zeigt die Anzahl der Zellen an.";
OutputResultFrame.NbOfCellsTitleLabel.LongHelp="Zeigt die Anzahl der Zellen in der erzeugten Verrundung an.";
OutputResultFrame.MaxOrderTitleLabel.ShortHelp="Zeigt den max. Grad in U- und V-Richtung an.";
OutputResultFrame.MaxOrderTitleLabel.LongHelp="Zeigt den maximalen Grad des erzeugten Ergebnisses in U- und V-Richtung an.";
OutputResultFrame.MaxSegTitleLabel.ShortHelp="Zeigt die max. Anzahl von Segmenten pro Zelle in U- und V-Richtung an.";
OutputResultFrame.MaxSegTitleLabel.LongHelp="Zeigt die maximale Anzahl von Segmenten pro Zelle des erzeugten Ergebnisses in U- und V-Richtung an.";
// End WAP 08:08:26	

// Start WAP 08:11:25 R19SP3 HL
LengthLabel.Message="L�nge:";
RadiusLabel.Message="Radius:";
// End WAP 08:11:25

// Start WAP 08:12:02 R19SP4 HL6
//Spine - Point Frame	
MainOuterFrame.SpinePointFrame.LabelSpine.Title="Leitkurve:";
MainOuterFrame.SpinePointFrame.LabelPoint.Title="Punkt:";
MainOuterFrame.SpinePointFrame.SpineSelector.ShortHelp="Leitkurvenset ausw�hlen";
MainOuterFrame.SpinePointFrame.SpineSelector.LongHelp="Die Leitkurve f�r die Verrundungsoperation ausw�hlen";
MainOuterFrame.SpinePointFrame.PointSelector.ShortHelp="Punkt auf Leitkurve ausw�hlen";
MainOuterFrame.SpinePointFrame.PointSelector.LongHelp="F�r die Verrundungsoperation einen Punkt auf der Leitkurve ausw�hlen";

// Geometry Frame
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.LabelExtrapolRelimit1.Title="Extrapolieren/Seite1 neu begrenzen";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.LabelExtrapolRelimit1.ShortHelp="Extrapolieren/Neubegrenzung auf Seite1";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.LabelExtrapolRelimit1.LongHelp="Das Ergebnis wird extrapoliert/auf Seite1 neu begrenzt";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.LabelExtrapolRelimit2.Title="Extrapolieren/Seite2 neu begrenzen";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.LabelExtrapolRelimit2.ShortHelp="Extrapolieren/Neubegrenzung auf Seite2";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.GeomRelimFrame.LabelExtrapolRelimit2.LongHelp="Das Ergebnis wird extrapoliert/auf Seite2 neu begrenzt";

// Result Frame
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.LogicalMerge.Title="Logische Verbindung";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.LogicalMerge.ShortHelp="Logische Verbindung";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.LogicalMerge.LongHelp="Das Ergebnis (Verrundungsband, getrimmte Eingabefl�chen) wird mit einer festen Toleranz von 0,1 mm zusammengesetzt";

// Simplification Frame
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.Merge.Title="Verketten";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.Merge.ShortHelp="Verketten";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.Merge.LongHelp="Verkettet ein Verrundungsband mit mehreren Zellen zu einem Ergebnis mit einer Zelle (nur bei G2-Stetigkeit zwischen Zellen m�glich)";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.LogicalMerge.Title="Logische Verbindung";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.LogicalMerge.ShortHelp="Logische Verbindung";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.ResultFrame.SimplificationFrame.LogicalMerge.LongHelp="Das Ergebnis (Verrundungsband, getrimmte Eingabefl�chen) wird mit einer festen Toleranz von 0,1 mm zusammengesetzt";
// End WAP 08:12:02

// Start WAP 09:01:23 R19SP4 HL5
Curve1SelectorShortHelp.Message="Kurvenset ausw�hlen";
Curve1SelectorLongHelp.Message="Erstes Kurvenset f�r die Verrundungsoperation ausw�hlen";
Curve1SelStateShortHelp.Message="Kurvenset 1 aktivieren";
Curve1SelStateLongHelp.Message="Erm�glicht die Auswahl in Kurvenset 1";

Curve2SelectorShortHelp.Message="Kurvenset ausw�hlen";
Curve2SelectorLongHelp.Message="Zweites Kurvenset f�r die Verrundungsoperation ausw�hlen";
Curve2SelStateShortHelp.Message="Kurvenset 2 aktivieren";
Curve2SelStateLongHelp.Message="Erm�glicht die Auswahl in Kurvenset 2";


EdgesToExtrapol1SelectorShortHelp.Message="Zu extrapolierende Kante 1 ausw�hlen";
EdgesToExtrapol1SelectorLongHelp.Message="Erstes zu extrapolierendes Kantenset f�r die variable Verrundungsoperation ausw�hlen";
EdgesToExtrapol1SelStateShortHelp.Message="Zu extrapolierende Kante 1 aktivieren";
EdgesToExtrapol1SelStateLongHelp.Message="Erm�glicht die Auswahl des ersten zu extrapolierenden Kantensets f�r die variable Verrundungsoperation";
EdgesToExtrapol2SelectorShortHelp.Message="Zu extrapolierende Kante 2 ausw�hlen";
EdgesToExtrapol2SelectorLongHelp.Message="Zweites zu extrapolierendes Kantenset f�r die variable Verrundungsoperation ausw�hlen";
EdgesToExtrapol2SelStateShortHelp.Message="Zu extrapolierende Kante 2 aktivieren";
EdgesToExtrapol2SelectorLongHelp.Message="Erm�glicht die Auswahl des zweiten zu extrapolierenden Kantensets f�r die variable Verrundungsoperation";

// End WAP 09:01:23

// Start WAP 08:12:02 R19SP5
// Input Parameter Frame
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.InputParameterFrame.Title="Eingabeparameter";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.InputParameterFrame.LabelMergeDist.Title="Abstand f�r Zusammenf�hrung: ";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.InputParameterFrame.SpinnerMergeDist.ShortHelp="Abstand bei der Zusammenf�hrung";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.InputParameterFrame.SpinnerMergeDist.LongHelp="Abstand bei der Zusammenf�hrung f�r Verrundung mit mehreren Zellen festlegen";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.InputParameterFrame.TolerantLaydownCheck.ShortHelp="Tolerante Ausf�hrung";
MainOuterFrameTabs.TabCont.GeneralTab.MainFrame.InputParameterFrame.TolerantLaydownCheck.LongHelp="Tolerante Ausf�hrung verwenden";
TolerantLaydown.Title="Tolerante Ausf�hrung";
// End WAP 08:12:02					   

MainOuterFrame.Surface2Curve2Frame.EdgeFilletMode.Title="Verrundungstyp";
MainOuterFrame.Surface2Curve2Frame.EdgeFilletMode.ShortHelp="Verrundungstyp";
MainOuterFrame.Surface2Curve2Frame.EdgeFilletMode.LongHelp="Typ der Verrundungsoperationen ausw�hlen. St�tzverrundung/Kantenverrundung";

EdgeFilletModeLabel.Message="Zu verrundende(s) Objekt(e)";
SupportFilletMode.Message="St�tzelement 1";

Surface1Curve1FrameShortHelp.Message="Fl�chen- und Kurvenset 1 ausw�hlen";
Surface1Curve1FrameLongHelp.Message="Erstes Fl�chen- und Kurvenset f�r die Verrundungsoperation ausw�hlen";
ObjectsToFilletFrameShortHelp.Message="Zu verrundende(s) Objekt(e) ausw�hlen";
ObjectsToFilletFrameLongHelp.Message="Die scharfen Kanten/Teilfl�chen mit scharfen Kanten f�r die Verrundungsoperation ausw�hlen";

Surface1Title.Message="Fl�che:";
Surface1ShortHelp.Message="Fl�chenset ausw�hlen";
Surface1LongHelp.Message="Erstes Fl�chenset f�r die Verrundungsoperation ausw�hlen";

ObjectToFilletTitle.Message="Zu verrundende(s) Objekt(e):";
ObjectToFilletShortHelp.Message="Zu verrundende(s) Objekt(e) ausw�hlen";
ObjectToFilletLongHelp.Message="Die scharfen Kanten/Teilfl�chen mit scharfen Kanten f�r die Verrundungsoperation ausw�hlen";

Surface1SelectorShortHelp.Message="Fl�chenset ausw�hlen";
Surface1SelectorLongHelp.Message="Erstes Fl�chenset f�r die Verrundungsoperation ausw�hlen";
ObjectsToFilletSelectorShortHelp.Message="Zu verrundende(s) Objekt(e) ausw�hlen";
ObjectsToFilletSelectorLongHelp.Message="Die scharfen Kanten/Teilfl�chen mit scharfen Kanten f�r die Verrundungsoperation ausw�hlen";
