//----------------------------------------------
// Resource file for CATGSMUIHealingPanel class
// En_US
//----------------------------------------------

Title="Definition der Reparatur";

FraGeneral.Title="Zu reparierende Elemente:";
FraGeneral.LongHelp="Gibt die zu reparierenden Elemente an.";
FraGeneral.ButtonsFrame.Append.ShortHelp="W�hrend der Multiselektion im Modus 'Hinzuf�gen' sperren.";
FraGeneral.ButtonsFrame.Append.LongHelp="W�hrend der Multiselektion im Modus 'Hinzuf�gen' sperren.";
FraGeneral.ButtonsFrame.Remove.ShortHelp="W�hrend der Multiselektion im Modus 'Entfernen' sperren.";
FraGeneral.ButtonsFrame.Remove.LongHelp="W�hrend der Multiselektion im Modus 'Entfernen' sperren.";

TabContainer.ParametersTabPage.Title="Parameter";

TabContainer.ParametersTabPage.LabContinuityType.Title="Stetigkeit:";
TabContainer.ParametersTabPage.LabContinuityType.LongHelp="Listet die verf�gbaren Stetigkeitstypen zwischen den zu reparierenden Elementen auf.";
TabContainer.ParametersTabPage.CmbContinuityType.LongHelp="Gibt die verf�gbaren Stetigkeitstypen zwischen den zu reparierenden Elementen an.";
PointType="Punkt";
TangentType="Tangente";

TabContainer.ParametersTabPage.LabDevUser.Title="Abstand f�r Zusammenf�gung";
TabContainer.ParametersTabPage.FraDevUser.EnglobingFrame.IntermediateFrame.Spinner.Title="Abstand";
TabContainer.ParametersTabPage.LabDevUser.LongHelp="Maximaler Abstand, unterhalb dessen zwei Elemente
als ein einziges Element interpretiert werden.";

TabContainer.ParametersTabPage.LabG0Obj.Title="Objektiver Abstand:";
TabContainer.ParametersTabPage.FraG0Obj.EnglobingFrame.IntermediateFrame.Spinner.Title="Abstand";
TabContainer.ParametersTabPage.LabG0Obj.LongHelp="Objektiver Abstand bei Reparaturen in Bezug auf die Punktstetigkeit.";

TabContainer.ParametersTabPage.LabTangencyAngle.Title="Tangentenstetigkeitswinkel:";
TabContainer.ParametersTabPage.FraTangencyAngle.EnglobingFrame.IntermediateFrame.Spinner.Title="Winkel";
TabContainer.ParametersTabPage.LabTangencyAngle.LongHelp="Maximaler Winkel, unterhalb dessen zwei Elemente
als tangentenstetig interpretiert werden.";

TabContainer.ParametersTabPage.LabG1Obj.Title="Objektive Tangentenstetigkeit:";
TabContainer.ParametersTabPage.FraG1Obj.EnglobingFrame.IntermediateFrame.Spinner.Title="Winkel";
TabContainer.ParametersTabPage.LabG1Obj.LongHelp="Objektiver Winkel bei Reparaturen in Bezug auf die Punktstetigkeit.";

TabContainer.ParametersTabPage.LabComputeMode.Title="Berechnungsmodus:";
TabContainer.ParametersTabPage.LabComputeMode.LongHelp="Gibt den Berechnungsmodus f�r die Reparatur an: Standard oder Genau.";

TabContainer.FixTabPage.Title="Festschreiben";
TabContainer.FixTabPage.Frame_Freeze.Title="Festzuschreibende Elemente";
TabContainer.FixTabPage.Frame_Freeze.LongHelp="Gibt die Kanten oder Teilfl�chen an, die bei der Reparatur nicht ber�cksichtigt werden sollen.";
TabContainer.FixTabPage.Frame_Freeze.FixList.LongHelp="Gibt die Kanten oder Teilfl�chen an, die bei der Reparatur nicht ber�cksichtigt werden sollen.";

TabContainer.FixTabPage.CheckFacesPlanes.Title="Ebene Elemente festschreiben";
TabContainer.FixTabPage.CheckFacesPlanes.LongHelp="Gibt an, ob die ebenen Elemente bei der Reparatur ber�cksichtigt werden sollen.";

TabContainer.FixTabPage.CheckCanonics.Title="Kanonische Elemente festschreiben";
TabContainer.FixTabPage.CheckCanonics.LongHelp="Gibt an, ob die kanonischen Elemente bei der Reparatur ber�cksichtigt werden sollen.";

TabContainer.SharpTabPage.Title="Sch�rfe";
TabContainer.SharpTabPage.Frame_Sharp.Title="Kanten, die scharf bleiben sollen";
TabContainer.SharpTabPage.Frame_Sharp.LongHelp="Gibt die Kanten an, die bei der Reparatur nicht ber�cksichtigt werden sollen.";
TabContainer.SharpTabPage.Frame_Sharp.SharpList.LongHelp="Gibt die Kanten an, die bei der Reparatur nicht ber�cksichtigt werden sollen.";

TabContainer.SharpTabPage.FraTolSharpness.LabFrame_Sharpness.Title="Sch�rfewinkel:";
TabContainer.SharpTabPage.FraTolSharpness.FraSharpness.EnglobingFrame.IntermediateFrame.Spinner.Title="Winkel";
TabContainer.SharpTabPage.FraTolSharpness.LabFrame_Sharpness.LongHelp="Maximaler Winkelwert; Winkel mit darunter liegenden Werten werden als scharfe Winkel interpretiert.";


TabContainer.OngletVisu.Title="Darstellung";

TabContainer.OngletVisu.LabelVisuRadio.Title="Angezeigte L�sung(en): ";
TabContainer.OngletVisu.LabelVisuRadio.LongHelp="";
TabContainer.OngletVisu.VisuRadioFrame.SelVisuAll.Title="Alle";
TabContainer.OngletVisu.VisuRadioFrame.SelVisuAll.LongHelp="Es werden alle Nachrichten angezeigt, die angeben, wo eine Unstetigkeit bestehen bleibt und wo sich der Unstetigkeitstyp ge�ndert hat.";
TabContainer.OngletVisu.VisuRadioFrame.SelVisuNotCorrected.Title="Nicht korrigiert";
TabContainer.OngletVisu.VisuRadioFrame.SelVisuNotCorrected.LongHelp="Es werden nur die Nachrichten angezeigt, die angeben, wo keine Korrektur der Unstetigkeit erfolgt ist und diese daher weiterhin besteht.";
TabContainer.OngletVisu.VisuRadioFrame.SelVisuNone.Title="Keine";
TabContainer.OngletVisu.VisuRadioFrame.SelVisuNone.LongHelp="Es wird keine Nachricht angezeigt.";

TabContainer.OngletVisu.SelVisuInteractive.Title="Informationen interaktiv anzeigen";
TabContainer.OngletVisu.SelVisuInteractive.LongHelp="Es werden nur die Zeiger in der Geometrie angezeigt, auf denen Text erscheint, wenn sie bewegt werden.";

TabContainer.OngletVisu.SelVisuSequential.Title="Informationen nacheinander anzeigen";
TabContainer.OngletVisu.SelVisuSequential.LongHelp="In der Geometrie wird nur ein Zeiger und Text angezeigt, und der Benutzer kann mit Hilfe der Schaltfl�chen 'Vorw�rts/R�ckw�rts' von einem Zeiger zum n�chsten springen.";

//continuity diagnosis reps
PtDiscontinuityLabel="punkunstetig";
TgDiscontinuityLabel="tangentenunstetig";
