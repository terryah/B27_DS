//=============================================================================
//                                     CNEXT - CXR2
//                          COPYRIGHT DASSAULT SYSTEMES 1998 
//-----------------------------------------------------------------------------
// FILENAME    :    CATDrwGDTWind
// LOCATION    :    DraftingIntDSA/CNext/resources/msgcatalog/
// AUTHOR      :    pbr
// DATE        :    June 12, 1998
//------------------------------------------------------------------------------
// DESCRIPTION :    Resources File for NLS purpose related to Interactive GDT
//                  Creation Window.
//                  
//------------------------------------------------------------------------------
// COMMENTS    :
//------------------------------------------------------------------------------
//MODIFICATIONS     user  date      purpose
//   HISTORY        ----  ----      -------
//   Modified   : Philip Berlioz (pbr)
//   Date       : Jan. 1999
//   Chg id     : RI# 0210215 - CXR2
//   Chg nature : Rework NLS text handling, implement Contextual help
//--------------------------------------------------------------------
//   Modified   : Philip Berlioz (pbr)
//   Date       : January 04, 2000 
//   Chg id     : 0248358 A - CXR3 SP3 & CXR4
//   Chg nature : Update GDT creation panel.
//------------------------------------------------------------------------------

Title = "Parameter f�r geometrische Toleranz";

cadre2.Title = "Symbol";
cadre3.Title = "Wert f�r Toleranzbereich";
cadre4.Title = "Bezugselemente";
cadre35.Title = "Projizierter Bereich";
cadre36.Title = "Filtersymbole";

cadre36.radio36.Title = "Ja";
cadre36.radio37.Title = "Nein ";

cadre2.label2.Title  = "Spez 1...";
cadre2.label38.Title = "Spez 2...";

cadre2.images09.ShortHelp = "Prim�re geometrische Eigenschaft";
cadre2.images09.boutonA0.ShortHelp = "Keine";
cadre2.images09.boutonA1.ShortHelp = "Winkelf�rmigkeit";
cadre2.images09.boutonA2.ShortHelp = "Kreisf�rmigkeit";
cadre2.images09.boutonA3.ShortHelp = "Konzentrizit�t ";
cadre2.images09.boutonA4.ShortHelp = "Zylinderf�rmigkeit";
cadre2.images09.boutonA6.ShortHelp = "Lauf gesamt";
cadre2.images09.boutonA7.ShortHelp = "Geradheit";
cadre2.images09.boutonA8.ShortHelp = "Parallelit�t ";
cadre2.images09.boutonA9.ShortHelp = "Orthogonalit�t ";
cadre2.images09.boutonA10.ShortHelp = "Ebenheit";
cadre2.images09.boutonA11.ShortHelp = "Position";
cadre2.images09.boutonA12.ShortHelp = "Linienprofil";
cadre2.images09.boutonA13.ShortHelp = "Fl�chenprofil";
cadre2.images09.boutonA14.ShortHelp = "Kreisf�rmiger Lauf";
cadre2.images09.boutonA15.ShortHelp = "Symmetrie";

cadre3.images12.ShortHelp = "Durchmesserbereich";
cadre3.images12.boutonB0.ShortHelp = "Keiner";
cadre3.images12.boutonB1.ShortHelp = "Durchmesser";
cadre3.champ15.ShortHelp = "Wert f�r Toleranzbereich";
cadre3.images16.ShortHelp = "�nderungswert f�r Toleranzbereich";
cadre3.images16.boutonC0.ShortHelp = "Keiner";
cadre3.images16.boutonC1.ShortHelp = "Freier Status";
cadre3.images16.boutonC2.ShortHelp = "Bedingung 'Geringstes Material'";
cadre3.images16.boutonC3.ShortHelp = "Bedingung 'Maximales Material'";
cadre3.images16.boutonC5.ShortHelp = "Gr��enunabh�ngig";
cadre3.images16.boutonC6.ShortHelp = "Tangentenebene";

cadre4.champ19.ShortHelp = "Text f�r prim�res Bezugselement";
cadre4.images22.ShortHelp = "�nderungswert f�r prim�res Bezugselement";
cadre4.images22.boutonD0.ShortHelp = "Keiner";
cadre4.images22.boutonD1.ShortHelp = "Freier Status";
cadre4.images22.boutonD2.ShortHelp = "Bedingung 'Geringstes Material'";
cadre4.images22.boutonD3.ShortHelp = "Bedingung 'Maximales Material'";
cadre4.images22.boutonD5.ShortHelp = "Gr��enunabh�ngig";
cadre4.images22.boutonD6.ShortHelp = "Tangentenebene";
cadre4.champ24.ShortHelp = "Text f�r sekund�res Bezugselement";
cadre4.images25.ShortHelp = "�nderungswert f�r sekund�res Bezugselement";
cadre4.images25.boutonE0.ShortHelp = "Keiner";
cadre4.images25.boutonE1.ShortHelp = "Freier Status";
cadre4.images25.boutonE2.ShortHelp = "Bedingung 'Geringstes Material'";
cadre4.images25.boutonE3.ShortHelp = "Bedingung 'Maximales Material'";
cadre4.images25.boutonE5.ShortHelp = "Gr��enunabh�ngig";
cadre4.images25.boutonE6.ShortHelp = "Tangentenebene";
cadre4.champ28.ShortHelp = "Text f�r terti�res Bezugselement";
cadre4.images29.ShortHelp = "�nderungswert f�r terti�res Bezugselement";
cadre4.images29.boutonF0.ShortHelp = "Keiner";
cadre4.images29.boutonF1.ShortHelp = "Freier Status";
cadre4.images29.boutonF2.ShortHelp = "Bedingung 'Geringstes Material'";
cadre4.images29.boutonF3.ShortHelp = "Bedingung 'Maximales Material'";
cadre4.images29.boutonF5.ShortHelp = "Gr��enunabh�ngig";
cadre4.images29.boutonF6.ShortHelp = "Tangentenebene";

cadre35.champ35.ShortHelp = "Wert f�r projizierten Toleranzbereich";
cadre35.images35.ShortHelp = "Projizierter Toleranzbereich";
cadre35.images35.boutonM0.ShortHelp = "Keiner";
cadre35.images35.boutonM1.ShortHelp = "Projizierter Toleranzbereich";

cadre36.radio36.ShortHelp = "Symbol f�r geometrische Eigenschaften - Symbole gefiltert";
cadre36.radio37.ShortHelp = "Symbol f�r geometrische Eigenschaften - Symbole nicht gefiltert";

cadre2.images39.ShortHelp = "Sekund�re geometrische Eigenschaft";
cadre2.images39.boutonG0.ShortHelp = "Keine";
cadre2.images39.boutonG1.ShortHelp = "Winkelf�rmigkeit";
cadre2.images39.boutonG2.ShortHelp = "Kreisf�rmigkeit";
cadre2.images39.boutonG3.ShortHelp = "Konzentrizit�t ";
cadre2.images39.boutonG5.ShortHelp = "Zylinderf�rmigkeit";
cadre2.images39.boutonG6.ShortHelp = "Lauf gesamt";
cadre2.images39.boutonG7.ShortHelp = "Geradheit";
cadre2.images39.boutonG8.ShortHelp = "Parallelit�t ";
cadre2.images39.boutonG9.ShortHelp = "Orthogonalit�t ";
cadre2.images39.boutonG10.ShortHelp = "Ebenheit";
cadre2.images39.boutonG11.ShortHelp = "Position";
cadre2.images39.boutonG12.ShortHelp = "Linienprofil";
cadre2.images39.boutonG13.ShortHelp = "Fl�chenprofil";
cadre2.images39.boutonG14.ShortHelp = "Kreisf�rmiger Lauf";
cadre2.images39.boutonG15.ShortHelp = "Symmetrie";

cadre3.images43.ShortHelp = "Durchmesserbereich";
cadre3.images43.boutonH0.ShortHelp = "Keiner";
cadre3.images43.boutonH1.ShortHelp = "Durchmesser";
cadre3.champ46.ShortHelp = "Wert f�r Toleranzbereich";
cadre3.images47.ShortHelp = "�nderungswert f�r Toleranzbereich";
cadre3.images47.boutonI0.ShortHelp = "Keiner";
cadre3.images47.boutonI1.ShortHelp = "Freier Status";
cadre3.images47.boutonI2.ShortHelp = "Bedingung 'Geringstes Material'";
cadre3.images47.boutonI3.ShortHelp = "Bedingung 'Maximales Material'";
cadre3.images47.boutonI5.ShortHelp = "Gr��enunabh�ngig";
cadre3.images47.boutonI6.ShortHelp = "Tangentenebene";

cadre4.champ51.ShortHelp = "Text f�r prim�res Bezugselement";
cadre4.images53.ShortHelp = "�nderungswert f�r prim�res Bezugselement";
cadre4.images53.boutonJ0.ShortHelp = "Keiner";
cadre4.images53.boutonJ1.ShortHelp = "Freier Status";
cadre4.images53.boutonJ2.ShortHelp = "Bedingung 'Geringstes Material'";
cadre4.images53.boutonJ3.ShortHelp = "Bedingung 'Maximales Material'";
cadre4.images53.boutonJ5.ShortHelp = "Gr��enunabh�ngig";
cadre4.images53.boutonJ6.ShortHelp = "Tangentenebene";
cadre4.champ56.ShortHelp = "Text f�r sekund�res Bezugselement";
cadre4.images57.ShortHelp = "�nderungswert f�r sekund�res Bezugselement";
cadre4.images57.boutonK0.ShortHelp = "Keiner";
cadre4.images57.boutonK1.ShortHelp = "Freier Status";
cadre4.images57.boutonK2.ShortHelp = "Bedingung 'Geringstes Material'";
cadre4.images57.boutonK3.ShortHelp = "Bedingung 'Maximales Material'";
cadre4.images57.boutonK5.ShortHelp = "Gr��enunabh�ngig";
cadre4.images57.boutonK6.ShortHelp = "Tangentenebene";
cadre4.champ61.ShortHelp = "Text f�r terti�res Bezugselement";
cadre4.images62.ShortHelp = "�nderungswert f�r terti�res Bezugselement";
cadre4.images62.boutonL0.ShortHelp = "Keiner";
cadre4.images62.boutonL1.ShortHelp = "Freier Status";
cadre4.images62.boutonL2.ShortHelp = "Bedingung 'Geringstes Material'";
cadre4.images62.boutonL3.ShortHelp = "Bedingung 'Maximales Material'";
cadre4.images62.boutonL5.ShortHelp = "Gr��enunabh�ngig";
cadre4.images62.boutonL6.ShortHelp = "Tangentenebene";

cadre2.images09.Help = "Symbolmen� f�r Symbole f�r geometrische Eigenschaften";
cadre2.images09.boutonA0.Help = "Keine geometrische Eigenschaft";
cadre2.images09.boutonA1.Help = "F�gt geometrische Ausrichtungstoleranz hinzu: Winkelf�rmigkeit";
cadre2.images09.boutonA2.Help = "F�gt geometrische Formtoleranz hinzu: Kreisf�rmigkeit";
cadre2.images09.boutonA3.Help = "F�gt geometrische Lagetoleranz hinzu: Konzentrizit�t";
cadre2.images09.boutonA4.Help = "F�gt geometrische Formtoleranz hinzu: Zylinderf�rmigkeit";
cadre2.images09.boutonA6.Help = "F�gt geometrische Lauftoleranz hinzu: Lauf gesamt";
cadre2.images09.boutonA7.Help = "F�gt geometrische Formtoleranz hinzu: Geradheit";
cadre2.images09.boutonA8.Help = "F�gt geometrische Ausrichtungstoleranz hinzu: Parallelit�t";
cadre2.images09.boutonA9.Help = "F�gt geometrische Ausrichtungstoleranz hinzu: Orthogonalit�t";
cadre2.images09.boutonA10.Help = "F�gt geometrische Formtoleranz hinzu: Ebenheit";
cadre2.images09.boutonA11.Help = "F�gt geometrische Lagetoleranz hinzu: Position";
cadre2.images09.boutonA12.Help = "F�gt geometrische Profiltoleranz hinzu: Profil einer Linie";
cadre2.images09.boutonA13.Help = "F�gt geometrische Profiltoleranz hinzu: Profil einer Fl�che";
cadre2.images09.boutonA14.Help = "F�gt geometrische Lauftoleranz hinzu: Lauf kreisf�rmig";
cadre2.images09.boutonA15.Help = "F�gt geometrische Lagetoleranz hinzu: Symmetrie";

cadre3.images12.Help = "Symbolmen� f�r Durchmesserbereich";
cadre3.images12.boutonB0.Help = "Keine Durchmessertoleranz";
cadre3.images12.boutonB1.Help = "Symbol f�r Durchmesserbereich";
cadre3.champ15.Help = "Toleranzwerteditor";
cadre3.images16.Help = "Symbolmen� f�r Symbole f�r �nderungswert f�r Toleranzbereich";
cadre3.images16.boutonC0.Help = "Kein �nderungswert f�r Toleranz";
cadre3.images16.boutonC1.Help = "�nderungswert f�r freien Status";
cadre3.images16.boutonC2.Help = "�nderungswert f�r Bedingung 'Geringstes Material'";
cadre3.images16.boutonC3.Help = "�nderungswert f�r Bedingung 'Maximales Material'";
cadre3.images16.boutonC5.Help = "�nderungswert f�r 'Gr��enunabh�ngig'";
cadre3.images16.boutonC6.Help = "�nderungswert f�r Tangentenebene";

cadre4.champ19.Help = "Editor f�r Text f�r prim�res Bezugselement";
cadre4.images22.Help = "Symbolmen� f�r Symbole f�r �nderungswert f�r prim�res Bezugselement";
cadre4.images22.boutonD0.Help = "Kein �nderungswert f�r Toleranz";
cadre4.images22.boutonD1.Help = "�nderungswert f�r freien Status";
cadre4.images22.boutonD2.Help = "�nderungswert f�r Bedingung 'Geringstes Material'";
cadre4.images22.boutonD3.Help = "�nderungswert f�r Bedingung 'Maximales Material'";
cadre4.images22.boutonD5.Help = "�nderungswert f�r 'Gr��enunabh�ngig'";
cadre4.images22.boutonD6.Help = "�nderungswert f�r Tangentenebene";
cadre4.champ24.Help = "Editor f�r Text f�r sekund�res Bezugselement";
cadre4.images25.Help = "Symbolmen� f�r Symbole f�r �nderungswert f�r sekund�res Bezugselement";
cadre4.images25.boutonE0.Help = "Kein �nderungswert f�r Toleranz";
cadre4.images25.boutonE1.Help = "�nderungswert f�r freien Status";
cadre4.images25.boutonE2.Help = "�nderungswert f�r Bedingung 'Geringstes Material'";
cadre4.images25.boutonE3.Help = "�nderungswert f�r Bedingung 'Maximales Material'";
cadre4.images25.boutonE5.Help = "�nderungswert f�r 'Gr��enunabh�ngig'";
cadre4.images25.boutonE6.Help = "�nderungswert f�r Tangentenebene";
cadre4.champ28.Help = "Editor f�r Text f�r terti�res Bezugselement";
cadre4.images29.Help = "Symbolmen� f�r Symbole f�r �nderungswert f�r terti�res Bezugselement";
cadre4.images29.boutonF0.Help = "Kein �nderungswert f�r Toleranz";
cadre4.images29.boutonF1.Help = "�nderungswert f�r freien Status";
cadre4.images29.boutonF2.Help = "�nderungswert f�r Bedingung 'Geringstes Material'";
cadre4.images29.boutonF3.Help = "�nderungswert f�r Bedingung 'Maximales Material'";
cadre4.images29.boutonF5.Help = "�nderungswert f�r 'Gr��enunabh�ngig'";
cadre4.images29.boutonF6.Help = "�nderungswert f�r Tangentenebene";

cadre35.champ35.Help = "Wert f�r projizierten Toleranzbereich";
cadre35.images35.Help = "Symbolmen� f�r projizierten Toleranzbereich";
cadre35.images35.boutonM0.Help = "Kein projizierter Toleranzbereich";
cadre35.images35.boutonM1.Help = "Symbol f�r projizierten Toleranzbereich";

cadre36.radio36.Help = "Men� 'Geometrische Toleranz' mit Symbolen gefiltert auf Elementbasis";
cadre36.radio37.Help = "Vollst�ndige Liste mit im Men� angezeigten Symbolen f�r geometrische Toleranz";

cadre2.images39.Help = "Symbolmen� f�r Symbole f�r geometrische Eigenschaften";
cadre2.images39.boutonG0.Help = "Keine geometrische Eigenschaft";
cadre2.images39.boutonG1.Help = "F�gt geometrische Ausrichtungstoleranz hinzu: Winkelf�rmigkeit";
cadre2.images39.boutonG2.Help = "F�gt geometrische Formtoleranz hinzu: Kreisf�rmigkeit";
cadre2.images39.boutonG3.Help = "F�gt geometrische Lagetoleranz hinzu: Konzentrizit�t";
cadre2.images39.boutonG5.Help = "F�gt geometrische Formtoleranz hinzu: Zylinderf�rmigkeit";
cadre2.images39.boutonG6.Help = "F�gt geometrische Lauftoleranz hinzu: Lauf gesamt";
cadre2.images39.boutonG7.Help = "F�gt geometrische Formtoleranz hinzu: Geradheit";
cadre2.images39.boutonG8.Help = "F�gt geometrische Ausrichtungstoleranz hinzu: Parallelit�t";
cadre2.images39.boutonG9.Help = "F�gt geometrische Ausrichtungstoleranz hinzu: Orthogonalit�t";
cadre2.images39.boutonG10.Help = "F�gt geometrische Formtoleranz hinzu: Ebenheit";
cadre2.images39.boutonG11.Help = "F�gt geometrische Lagetoleranz hinzu: Position";
cadre2.images39.boutonG12.Help = "F�gt geometrische Profiltoleranz hinzu: Profil einer Linie";
cadre2.images39.boutonG13.Help = "F�gt geometrische Profiltoleranz hinzu: Profil einer Fl�che";
cadre2.images39.boutonG14.Help = "F�gt geometrische Lauftoleranz hinzu: Lauf kreisf�rmig";
cadre2.images39.boutonG15.Help = "F�gt geometrische Lagetoleranz hinzu: Symmetrie";

cadre3.images43.Help = "Symbolmen� f�r Durchmesserbereich";
cadre3.images43.boutonH0.Help = "Keine Durchmessertoleranz";
cadre3.images43.boutonH1.Help = "Symbol f�r Durchmesserbereich";
cadre3.champ46.Help = "Toleranzwerteditor";
cadre3.images47.Help = "Symbolmen� f�r Symbole f�r �nderungswert f�r Toleranzbereich";
cadre3.images47.boutonI0.Help = "Kein �nderungswert f�r Toleranz";
cadre3.images47.boutonI1.Help = "�nderungswert f�r freien Status";
cadre3.images47.boutonI2.Help = "�nderungswert f�r Bedingung 'Geringstes Material'";
cadre3.images47.boutonI3.Help = "�nderungswert f�r Bedingung 'Maximales Material'";
cadre3.images47.boutonI5.Help = "�nderungswert f�r 'Gr��enunabh�ngig'";
cadre3.images47.boutonI6.Help = "�nderungswert f�r Tangentenebene";

cadre4.champ51.Help = "Editor f�r Text f�r prim�res Bezugselement";
cadre4.images53.Help = "Symbolmen� f�r Symbole f�r �nderungswert f�r prim�res Bezugselement";
cadre4.images53.boutonJ0.Help = "Kein �nderungswert f�r Toleranz";
cadre4.images53.boutonJ1.Help = "�nderungswert f�r freien Status";
cadre4.images53.boutonJ2.Help = "�nderungswert f�r Bedingung 'Geringstes Material'";
cadre4.images53.boutonJ3.Help = "�nderungswert f�r Bedingung 'Maximales Material'";
cadre4.images53.boutonJ5.Help = "�nderungswert f�r 'Gr��enunabh�ngig'";
cadre4.images53.boutonJ6.Help = "�nderungswert f�r Tangentenebene";
cadre4.champ56.Help = "Editor f�r Text f�r sekund�res Bezugselement";
cadre4.images57.Help = "Symbolmen� f�r Symbole f�r �nderungswert f�r sekund�res Bezugselement";
cadre4.images57.boutonK0.Help = "Kein �nderungswert f�r Toleranz";
cadre4.images57.boutonK1.Help = "�nderungswert f�r freien Status";
cadre4.images57.boutonK2.Help = "�nderungswert f�r Bedingung 'Geringstes Material'";
cadre4.images57.boutonK3.Help = "�nderungswert f�r Bedingung 'Maximales Material'";
cadre4.images57.boutonK5.Help = "�nderungswert f�r 'Gr��enunabh�ngig'";
cadre4.images57.boutonK6.Help = "�nderungswert f�r Tangentenebene";
cadre4.champ61.Help = "Editor f�r Text f�r terti�res Bezugselement";
cadre4.images62.Help = "Symbolmen� f�r Symbole f�r �nderungswert f�r terti�res Bezugselement";
cadre4.images62.boutonL0.Help = "Kein �nderungswert f�r Toleranz";
cadre4.images62.boutonL1.Help = "�nderungswert f�r freien Status";
cadre4.images62.boutonL2.Help = "�nderungswert f�r Bedingung 'Geringstes Material'";
cadre4.images62.boutonL3.Help = "�nderungswert f�r Bedingung 'Maximales Material'";
cadre4.images62.boutonL5.Help = "�nderungswert f�r 'Gr��enunabh�ngig'";
cadre4.images62.boutonL6.Help = "�nderungswert f�r Tangentenebene";

cadre2.images09.LongHelp = "Prim�re geometrische Eigenschaft
Definiert die geometrische Eigenschaft der Komponente.";
cadre2.images09.boutonA0.LongHelp = "Keine
Keine geometrische Eigenschaft definiert.";
cadre2.images09.boutonA1.LongHelp = "Winkelf�rmigkeit
Eine Achse oder eine Fl�che bildet einen angegebenen Winkel mit
einer anderen Achse oder Ebene eines Bezugselements.";
cadre2.images09.boutonA2.LongHelp = "Kreisf�rmigkeit
Alle Punkte einer Rotationsfl�che liegen �quidistant zu
einer Achse, durch die eine Schnittebene verl�uft,
die mit dieser Achse einen rechten Winkel bildet.";
cadre2.images09.boutonA3.LongHelp = "Konzentrizit�t
Alle Elemente in einem Querschnitt haben eine
gemeinsame Bezugselementachse.";
cadre2.images09.boutonA4.LongHelp = "Zylinderf�rmigkeit
Alle Punkte
einer Rotationsfl�che liegen �quidistant zu einer gemeinsamen Achse.";
cadre2.images09.boutonA6.LongHelp = "Gesamtlauf
Alle einer
Bezugselementachse zugeordneten Elemente einer Rotationsfl�che.";
cadre2.images09.boutonA7.LongHelp = "Geradheit
Die Achse eines Rotationselements oder eines Fl�chenelements
entspricht einer geraden Linie.";
cadre2.images09.boutonA8.LongHelp = "Parallelit�t
Eine Achse oder ebene Fl�che verl�uft �quidistant
zur Achse eines Bezugselements oder zur Ebene eines Bezugselements.";
cadre2.images09.boutonA9.LongHelp = "Orthogonalit�t
Eine Achse oder ebene Fl�che bildet einen rechten Winkel mit der Achse eines
Bezugselements oder der Ebene eines Bezugselements.";
cadre2.images09.boutonA10.LongHelp = "Ebenheit Alle Element einer
Fl�che liegen in einer Ebene.";
cadre2.images09.boutonA11.LongHelp = "Position
F�r eine Zentralachse oder Zentralebene gibt es einen Toleranzbereich.";
cadre2.images09.boutonA12.LongHelp = "Profil einer Linie
Ein 3D-Element wird auf eine Ebene projiziert.";
cadre2.images09.boutonA13.LongHelp = "Profil einer Fl�che
Eine 3D-Fl�che wird auf eine Ebene projiziert.";
cadre2.images09.boutonA14.LongHelp = "Gesamtlauf
Der Achse eines Bezugselements zugeordnete kreisf�rmige Elemente einer
Rotationsfl�che.";
cadre2.images09.boutonA15.LongHelp = "Spiegeln
Die Position einer Komponente relativ zu einer Zentralebene.";

cadre3.images12.LongHelp = "Durchmesserbereich
Gibt an, dass sich die Toleranz auf einen Durchmesser bezieht.";
cadre3.images12.boutonB0.LongHelp = "Keine
Toleranz bezieht sich nicht auf einen Durchmesser.";
cadre3.images12.boutonB1.LongHelp = "Durchmesser
Die folgenden Werte beziehen sich auf einen Durchmesser.";
cadre3.champ15.LongHelp = "Prim�rer Toleranzwert
Editor zur Eingabe des prim�ren Toleranzwerts.";
cadre3.images16.LongHelp = "�nderungswert f�r Toleranz
Definiert einen �nderungswert, der auf den prim�ren Toleranzwert
angewendet werden soll.";
cadre3.images16.boutonC0.LongHelp = "Keiner
Toleranz hat keinen �nderungswert.";
cadre3.images16.boutonC1.LongHelp = "Freier Status
Die Verzerrung eines Teils NACH dem Entfernen von Kr�ften, die w�hrend
der Herstellung eingewirkt haben.";
cadre3.images16.boutonC2.LongHelp = "Bedingung 'Geringstes Material'
Diese Komponente enth�lt die geringste Menge an Material,
die innerhalb der festgelegten Toleranzgrenzen zul�ssig ist.";
cadre3.images16.boutonC3.LongHelp = "Bedingung 'Maximales Material'
Diese Komponente enth�lt die maximale Menge an Material,
die innerhalb der festgelegten Toleranzgrenzen zul�ssig ist.";
cadre3.images16.boutonC5.LongHelp = "Gr��enunabh�ngig
Die Lagetoleranz muss unabh�ngig von der tats�chlichen Gr��e
der Komponente beibehalten werden.";
cadre3.images16.boutonC6.LongHelp = "Tangentenebene
�nderungswert f�r die Tangentenebene.";

cadre4.champ19.LongHelp = "Text f�r prim�res Bezugselement
Editor zur Eingabe von Text f�r ein prim�res Bezugselement.";
cadre4.images22.LongHelp = "�nderungswert f�r prim�res Bezugselement
Definiert einen �nderungswert, der auf das prim�re Bezugselement
angewendet werden soll.";
cadre4.images22.boutonD0.LongHelp = "Keiner
Toleranz hat keinen �nderungswert.";
cadre4.images22.boutonD1.LongHelp = "Freier Status
Die Verzerrung eines Teils NACH dem Entfernen von Kr�ften, die w�hrend
der Herstellung eingewirkt haben.";
cadre4.images22.boutonD2.LongHelp = "Bedingung 'Geringstes Material'
Diese Komponente enth�lt die geringste Menge an Material,
die innerhalb der festgelegten Toleranzgrenzen zul�ssig ist.";
cadre4.images22.boutonD3.LongHelp = "Bedingung 'Maximales Material'
Diese Komponente enth�lt die maximale Menge an Material,
die innerhalb der festgelegten Toleranzgrenzen zul�ssig ist.";
cadre4.images22.boutonD5.LongHelp = "Gr��enunabh�ngig
Die Lagetoleranz muss unabh�ngig von der tats�chlichen Gr��e
der Komponente beibehalten werden.";
cadre4.images22.boutonD6.LongHelp = "Tangentenebene
�nderungswert f�r die Tangentenebene.";
cadre4.champ24.LongHelp = "Text f�r sekund�res Bezugselement
Editor zur Eingabe von Text f�r ein sekund�res Bezugselement.";
cadre4.images25.LongHelp = "�nderungswert f�r sekund�res Bezugselement
Definiert einen �nderungswert, der auf das sekund�re
Bezugselement angewendet werden soll.";
cadre4.images25.boutonE0.LongHelp = "Keiner
Toleranz hat keinen �nderungswert.";
cadre4.images25.boutonE1.LongHelp = "Freier Status
Die Verzerrung eines Teils NACH dem Entfernen von Kr�ften, die w�hrend
der Herstellung eingewirkt haben.";
cadre4.images25.boutonE2.LongHelp = "Bedingung 'Geringstes Material'
Diese Komponente enth�lt die geringste Menge an Material,
die innerhalb der festgelegten Toleranzgrenzen zul�ssig ist.";
cadre4.images25.boutonE3.LongHelp = "Bedingung 'Maximales Material'
Diese Komponente enth�lt die maximale Menge an Material,
die innerhalb der festgelegten Toleranzgrenzen zul�ssig ist.";
cadre4.images25.boutonE5.LongHelp = "Gr��enunabh�ngig
Die Positionaltoleranz muss unabh�ngig von der tats�chlichen Gr��e
der Komponente beibehalten werden.";
cadre4.images25.boutonE6.LongHelp = "Tangentenebene
�nderungswert f�r die Tangentenebene.";
cadre4.champ28.LongHelp = "Text f�r terti�res Bezugselement
Editor zur Eingabe von Text f�r ein terti�res Bezugselement.";
cadre4.images29.LongHelp = "�nderungswert f�r terti�res Bezugselement
Definiert einen �nderungswert, der auf das
terti�re Bezugselement angewendet werden soll.";
cadre4.images29.boutonF0.LongHelp = "Keiner
Toleranz hat keinen �nderungswert.";
cadre4.images29.boutonF1.LongHelp = "Freier Status
Die Verzerrung eines Teils NACH dem Entfernen von Kr�ften, die
w�hrend der Herstellung eingewirkt haben.";
cadre4.images29.boutonF2.LongHelp = "Bedingung 'Geringstes Material'
Diese Komponente enth�lt die geringste Menge an Material,
die innerhalb der festgelegten Toleranzgrenzen zul�ssig ist.";
cadre4.images29.boutonF3.LongHelp = "Bedingung 'Maximales Material'
Diese Komponente enth�lt die maximale Menge an Material,
die innerhalb der festgelegten Toleranzgrenzen zul�ssig ist.";
cadre4.images29.boutonF5.LongHelp = "Gr��enunabh�ngig
Die Positionaltoleranz muss unabh�ngig von der tats�chlichen Gr��e
der Komponente beibehalten werden.";
cadre4.images29.boutonF6.LongHelp = "Tangentenebene
�nderungswert f�r die Tangentenebene.";

cadre35.champ35.LongHelp = "Wert f�r projizierten Toleranzbereich
Editor zur Eingabe des Werts f�r den projizierten Toleranzbereich.";
cadre35.images35.LongHelp = "Projizierter Toleranzbereich
Definiert, ob ein projizierter Toleranzbereich angewendet
werden soll.";
cadre35.images35.boutonM0.LongHelp = "Keiner
Kein projizierter Toleranzbereich aktiv.";
cadre35.images35.boutonM1.LongHelp = "Projizierter Toleranzbereich
Der Toleranzwert wird auf der Basis des angegebenen Abstands
projiziert.";

cadre36.radio36.LongHelp = "Gefiltert
Die Liste der verf�gbaren Symbole f�r geometrische Toleranzbereiche
wird wie festgelegt nach der Komponente durchsucht.";
cadre36.radio37.LongHelp = "Nicht gefiltert
Im Aktionssymbolmen� steht die vollst�ndige Liste mit
Symbolen f�r geometrische Toleranz zur Verf�gung.";

cadre2.images39.LongHelp = "Sekund�re geometrische Eigenschaft
Definiert die sekund�re geometrische Eigenschaft der Komponente.";
cadre2.images39.boutonG0.LongHelp = "Keine
Keine geometrische Eigenschaft definiert.";
cadre2.images39.boutonG1.LongHelp = "Winkelf�rmigkeit
Eine Achse oder eine Fl�che bildet einen angegebenen Winkel mit
einer anderen Achse oder Ebene eines Bezugselements.";
cadre2.images39.boutonG2.LongHelp = "Kreisf�rmigkeit
Alle Punkte einer Rotationsfl�che liegen �quidistant zu
einer Achse, durch die eine Schnittebene verl�uft,
die mit dieser Achse einen rechten Winkel bildet.";
cadre2.images39.boutonG3.LongHelp = "Konzentrizit�t
Alle Elemente in einem Querschnitt haben eine
gemeinsame Bezugselementachse.";
cadre2.images39.boutonG5.LongHelp = "Zylinderf�rmigkeit
Alle Punkte
einer Rotationsfl�che liegen �quidistant zu einer gemeinsamen Achse.";
cadre2.images39.boutonG6.LongHelp = "Gesamtlauf
Alle einer
Bezugselementachse zugeordneten Elemente einer Rotationsfl�che.";
cadre2.images39.boutonG7.LongHelp = "Geradheit
Die Achse eines Rotationselements oder eines Fl�chenelements
entspricht einer geraden Linie.";
cadre2.images39.boutonG8.LongHelp = "Parallelit�t
Eine Achse oder ebene Fl�che verl�uft �quidistant
zur Achse eines Bezugselements oder zur Ebene eines Bezugselements.";
cadre2.images39.boutonG9.LongHelp = "Orthogonalit�t
Eine Achse oder ebene Fl�che bildet einen rechten Winkel mit der Achse eines
Bezugselements oder der Ebene eines Bezugselements.";
cadre2.images39.boutonG10.LongHelp = "Ebenheit Alle Element einer
Fl�che liegen in einer Ebene.";
cadre2.images39.boutonG11.LongHelp = "Position
F�r eine Zentralachse oder Zentralebene gibt es einen Toleranzbereich.";
cadre2.images39.boutonG12.LongHelp = "Profil einer Linie
Ein 3D-Element wird auf eine Ebene projiziert.";
cadre2.images39.boutonG13.LongHelp = "Profil einer Fl�che
Eine 3D-Fl�che wird auf eine Ebene projiziert.";
cadre2.images39.boutonG14.LongHelp = "Gesamtlauf
Der Achse eines Bezugselements zugeordnete kreisf�rmige Elemente einer
Rotationsfl�che.";
cadre2.images39.boutonG15.LongHelp = "Spiegeln
Die Position einer Komponente relativ zu einer Zentralebene.";

cadre3.images43.LongHelp = "Durchmesserbereich
Gibt an, dass sich die Toleranz auf einen Durchmesser bezieht.";
cadre3.images43.boutonH0.LongHelp = "Keine
Toleranz bezieht sich nicht auf einen Durchmesser.";
cadre3.images43.boutonH1.LongHelp = "Durchmesser
Die folgenden Werte beziehen sich auf einen Durchmesser.";
cadre3.champ46.LongHelp = "Sekund�rer Toleranzwert
Editor zur Eingabe des sekund�ren Toleranzwerts.";
cadre3.images47.LongHelp = "Sekund�rer �nderungswert f�r Toleranz
Definiert einen �nderungswert, der auf den sekund�ren Toleranzwert
angewendet werden soll.";
cadre3.images47.boutonI0.LongHelp = "Keiner
Toleranz hat keinen �nderungswert.";
cadre3.images47.boutonI1.LongHelp = "Freier Status
Die Verzerrung eines Teils NACH dem Entfernen von Kr�ften, die
w�hrend der Herstellung eingewirkt haben.";
cadre3.images47.boutonI2.LongHelp = "Bedingung 'Geringstes Material'
Diese Komponente enth�lt die geringste Menge an Material,
die innerhalb der festgelegten Toleranzgrenzen zul�ssig ist.";
cadre3.images47.boutonI3.LongHelp = "Bedingung 'Maximales Material'
Diese Komponente enth�lt die maximale Menge an Material,
die innerhalb der festgelegten Toleranzgrenzen zul�ssig ist.";
cadre3.images47.boutonI5.LongHelp = "Gr��enunabh�ngig
Die Positionaltoleranz muss unabh�ngig von der tats�chlichen Gr��e
der Komponente beibehalten werden.";
cadre3.images47.boutonI6.LongHelp = "Tangentenebene
�nderungswert f�r die Tangentenebene.";

cadre4.champ51.LongHelp = "Text f�r prim�res Bezugselement
Editor zur Eingabe von Text f�r ein prim�res Bezugselement.";
cadre4.images53.LongHelp = "�nderungswert f�r prim�res Bezugselement
Definiert einen �nderungswert, der auf das prim�re Bezugselement
angewendet werden soll.";
cadre4.images53.boutonJ0.LongHelp = "Keiner
Toleranz hat keinen �nderungswert.";
cadre4.images53.boutonJ1.LongHelp = "Freier Status
Die Verzerrung eines Teils NACH dem Entfernen von Kr�ften, die
w�hrend der Herstellung eingewirkt haben.";
cadre4.images53.boutonJ2.LongHelp = "Bedingung 'Geringstes Material'
Diese Komponente enth�lt die geringste Menge an Material,
die innerhalb der festgelegten Toleranzgrenzen zul�ssig ist.";
cadre4.images53.boutonJ3.LongHelp = "Bedingung 'Maximales Material'
Diese Komponente enth�lt die maximale Menge an Material,
die innerhalb der festgelegten Toleranzgrenzen zul�ssig ist.";
cadre4.images53.boutonJ5.LongHelp = "Gr��enunabh�ngig
Die Positionaltoleranz muss unabh�ngig von der tats�chlichen Gr��e
der Komponente beibehalten werden.";
cadre4.images53.boutonJ6.LongHelp = "Tangentenebene
�nderungswert f�r die Tangentenebene.";
cadre4.champ56.LongHelp = "Text f�r sekund�res Bezugselement
Editor zur Eingabe von Text f�r ein sekund�res Bezugselement.";
cadre4.images57.LongHelp = "�nderungswert f�r sekund�res Bezugselement
Definiert einen �nderungswert, der auf das sekund�re
Bezugselement angewendet werden soll.";
cadre4.images57.boutonK0.LongHelp = "Keiner
Toleranz hat keinen �nderungswert.";
cadre4.images57.boutonK1.LongHelp = "Freier Status
Die Verzerrung eines Teils NACH dem Entfernen von Kr�ften, die
w�hrend der Herstellung eingewirkt haben.";
cadre4.images57.boutonK2.LongHelp = "Bedingung 'Geringstes Material'
Diese Komponente enth�lt die geringste Menge an Material,
die innerhalb der festgelegten Toleranzgrenzen zul�ssig ist.";
cadre4.images57.boutonK3.LongHelp = "Bedingung 'Maximales Material'
Diese Komponente enth�lt die maximale Menge an Material,
die innerhalb der festgelegten Toleranzgrenzen zul�ssig ist.";
cadre4.images57.boutonK5.LongHelp = "Gr��enunabh�ngig
Die Positionaltoleranz muss unabh�ngig von der tats�chlichen Gr��e
der Komponente beibehalten werden.";
cadre4.images57.boutonK6.LongHelp = "Tangentenebene
�nderungswert f�r die Tangentenebene.";
cadre4.champ61.LongHelp = "Text f�r terti�res Bezugselement
Editor zur Eingabe von Text f�r ein terti�res Bezugselement.";
cadre4.images62.LongHelp = "�nderungswert f�r terti�res Bezugselement
Definiert einen �nderungswert, der auf das
terti�re Bezugselement angewendet werden soll.";
cadre4.images62.boutonL0.LongHelp = "Keiner
Toleranz hat keinen �nderungswert.";
cadre4.images62.boutonL1.LongHelp = "Freier Status
Die Verzerrung eines Teils NACH dem Entfernen von Kr�ften, die
w�hrend der Herstellung eingewirkt haben.";
cadre4.images62.boutonL2.LongHelp = "Bedingung 'Geringstes Material'
Diese Komponente enth�lt die geringste Menge an Material,
die innerhalb der festgelegten Toleranzgrenzen zul�ssig ist.";
cadre4.images62.boutonL3.LongHelp = "Bedingung 'Maximales Material'
Diese Komponente enth�lt die maximale Menge an Material,
die innerhalb der festgelegten Toleranzgrenzen zul�ssig ist.";
cadre4.images62.boutonL5.LongHelp = "Gr��enunabh�ngig
Die Positionaltoleranz muss unabh�ngig von der tats�chlichen Gr��e
der Komponente beibehalten werden.";
cadre4.images62.boutonL6.LongHelp = "Tangentenebene
�nderungswert f�r die Tangentenebene.";
