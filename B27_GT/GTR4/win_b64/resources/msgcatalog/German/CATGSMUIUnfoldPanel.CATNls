//-----------------------------------------------------------------------------
// COPYRIGHT DASSAULT SYSTEMES 2003
//-----------------------------------------------------------------------------
//
//                   CATGSMUIUnfoldPanel
//
//-----------------------------------------------------------------------------

UnfoldTitle="Definition der Abwicklung";
UnfoldUVTitle="UV-Parameterangaben-Definition";
TransferTitle="Definition der Übertragung";

SurfaceToUnfold.FraEdt.Lab.Title="Abzuwickelnde Fläche: ";
SurfaceToUnfold.ShortHelp="Die abzuwickelnde Fläche.";
SurfaceToUnfold.LongHelp="Die abzuwickelnde Fläche.";

UnfoldedSurface.FraEdt.Lab.Title="Abgewickelte Fläche: ";
UnfoldedSurface.ShortHelp="Die abgewickelte Fläche.";
UnfoldedSurface.LongHelp="Die abgewickelte Fläche.";

TabContainer.PositionTabPage.Title="Position";

TabContainer.PositionTabPage.Reference.Title="Referenz";

TabContainer.PositionTabPage.Reference.Origin.FraEdt.Lab.Title="Ursprung: ";
TabContainer.PositionTabPage.Reference.Origin.ShortHelp="Der Ursprung der 2D-Referenzachse.";
TabContainer.PositionTabPage.Reference.Origin.LongHelp="Der Ursprung der 2D-Referenzachse.";
TabContainer.PositionTabPage.Reference.Origin.Flag="Referenzursprung";

TabContainer.PositionTabPage.Reference.Direction.FraEdt.Lab.Title="Richtung: ";
TabContainer.PositionTabPage.Reference.Direction.ShortHelp="Die Richtung der Referenz.";
TabContainer.PositionTabPage.Reference.Direction.LongHelp="Die Richtung der Referenz.";
TabContainer.PositionTabPage.Reference.Direction.Flag="Referenzrichtung";

TabContainer.PositionTabPage.Reference.Side.FraEdt.Lab.Title="Fixierte Seite: ";
TabContainer.PositionTabPage.Reference.Side.ShortHelp="Seite des einreißenden Elements, die fixiert werden soll.";
TabContainer.PositionTabPage.Reference.Side.LongHelp="Seite des einreißenden Elements, die fixiert werden soll.";
TabContainer.PositionTabPage.Reference.Side.Flag="Fixierte Seite";
TabContainer.PositionTabPage.Reference.InvertOrientationButtonV5.Title="Ausrichtung umkehren";
TabContainer.PositionTabPage.Reference.InvertOrientationButton.Title="Richtung umkehren";

TabContainer.PositionTabPage.Target.Title="Ziel";

TabContainer.PositionTabPage.Target.Plane.FraEdt.Lab.Title="Ebene: ";
TabContainer.PositionTabPage.Target.Plane.ShortHelp="Die Ebene, sofern abgewickelt.";
TabContainer.PositionTabPage.Target.Plane.LongHelp="Die Ebene, sofern abgewickelt.";
TabContainer.PositionTabPage.Target.Plane.Flag="Zielfläche";

TabContainer.PositionTabPage.Target.Origin.FraEdt.Lab.Title="Ursprung: ";
TabContainer.PositionTabPage.Target.Origin.ShortHelp="Der Ursprung der 2D-Zielachse.";
TabContainer.PositionTabPage.Target.Origin.LongHelp="Der Ursprung der 2D-Zielachse.";
TabContainer.PositionTabPage.Target.Origin.Flag="Zielursprung";

TabContainer.PositionTabPage.Target.FraDirection.FraEdt.Lab.Title="Richtung: ";
TabContainer.PositionTabPage.Target.FraDirection.ShortHelp="Zielrichtung";
TabContainer.PositionTabPage.Target.FraDirection.LongHelp="Zielrichtung";

TabContainer.SpecsToTearTabPage.Title="Einreißende Kurven";

TabContainer.SpecsToTearTabPage.SpecToTear.FraEdt.Lab.Title="Einreißende Ecken ";
TabContainer.SpecsToTearTabPage.SpecToTear.ShortHelp="Die einreißenden Ecken";
TabContainer.SpecsToTearTabPage.SpecToTear.LongHelp="Die einreißenden Ecken";

TabContainer.ToTransferTabPage.Title="Übertragen";

TabContainer.ToTransferTabPage.ToTransferFrame.ElementToTransferSelectorLabel.Title="Elemente";
TabContainer.ToTransferTabPage.ToTransferFrame.ElementToTransferSelectorList.ShortHelp="Zu übertragende Punkte und/oder Kurven.";
TabContainer.ToTransferTabPage.ToTransferFrame.ElementToTransferSelectorList.LongHelp="Zu übertragende Punkte und/oder Kurven.";

TabContainer.ToTransferTabPage.ToTransferFrame.TypeOfTransferSelectorLabel.Title="Konvertieren";
TabContainer.ToTransferTabPage.ToTransferFrame.TypeOfTransferSelectorList.ShortHelp="Konvertierungen, die auf die zu übertragenden Punkte oder Kurven anzuwenden sind.";
TabContainer.ToTransferTabPage.ToTransferFrame.TypeOfTransferSelectorList.LongHelp="Konvertierungen, die auf die zu übertragenden Punkte oder Kurven anzuwenden sind.";

TabContainer.ToTransferTabPage.ToTransferFrame.ElementToTransferSelector.ShortHelp="Zu übertragender Punkt oder Kurve.";
TabContainer.ToTransferTabPage.ToTransferFrame.ElementToTransferSelector.LongHelp="Zu übertragender Punkt oder Kurve.";
TabContainer.ToTransferTabPage.ToTransferFrame.ElementToTransferSelector.Flag="Zu übertragender Punkt oder Kurve";

TabContainer.ToTransferTabPage.ToTransferFrame.TypeOfTransferCombo.FoldToUnfold="Abwickeln";
TabContainer.ToTransferTabPage.ToTransferFrame.TypeOfTransferCombo.UnfoldToFold=" Falten ";
TabContainer.ToTransferTabPage.ToTransferFrame.TypeOfTransferCombo.ShortHelp="Konvertierung, die auf den zu übertragenden Punkt oder die Kurve anzuwenden ist.";
TabContainer.ToTransferTabPage.ToTransferFrame.TypeOfTransferCombo.LongHelp="Konvertierung, die auf den zu übertragenden Punkt oder die Kurve anzuwenden ist.";

TabContainer.ToTransferTabPage.ToTransferButtonsFrame.RemoveButton.Title="Entfernen ";
TabContainer.ToTransferTabPage.ToTransferButtonsFrame.RemoveButton.ShortHelp="Den oder die ausgewählten Punkte und/oder Kurven entfernen.";
TabContainer.ToTransferTabPage.ToTransferButtonsFrame.RemoveButton.LongHelp="Den oder die ausgewählten Punkte und/oder Kurven entfernen.";
TabContainer.ToTransferTabPage.ToTransferButtonsFrame.ReplaceButton.Title="Ersetzen";
TabContainer.ToTransferTabPage.ToTransferButtonsFrame.ReplaceButton.ShortHelp="Den oder die ausgewählten Punkte und/oder Kurven ersetzen.";
TabContainer.ToTransferTabPage.ToTransferButtonsFrame.ReplaceButton.LongHelp="Den oder die ausgewählten Punkte und/oder Kurven ersetzen.";

MoreButtonFrame.MoreLessButton.ShortHelp="Erweiterte Optionen anzeigen oder verdecken";
MoreButtonFrame.MoreLessButton.LongHelp="Erweiterte Optionen anzeigen oder verdecken";

MoreButtonTitle="Mehr >>";
LessButtonTitle="Weniger <<";

AdvancedOptions.SurfaceTypeFrame.SurfaceTypeLabel.Title="Flächentyp: ";
AdvancedOptions.SurfaceTypeFrame.RuledButton.Title="Regelfläche";
AdvancedOptions.SurfaceTypeFrame.RuledButton.ShortHelp="Ermöglicht die Angabe des Typs der Eingabefläche: 'Regelfläche' oder 'Alle'.";
AdvancedOptions.SurfaceTypeFrame.RuledButton.LongHelp="Ermöglicht die Angabe des Typs der Eingabefläche: 'Regelfläche' oder 'Alle'.";
AdvancedOptions.SurfaceTypeFrame.AnyButton.Title="Alle";
AdvancedOptions.SurfaceTypeFrame.AnyButton.ShortHelp="Ermöglicht die Angabe des Typs der Eingabefläche: 'Regelfläche' oder 'Alle'.";
AdvancedOptions.SurfaceTypeFrame.AnyButton.LongHelp="Ermöglicht die Angabe des Typs der Eingabefläche: 'Regelfläche' oder 'Alle'.";

AdvancedOptions.DisplayOptionalEdgesButton.Title="Optional einreißende Kanten anzeigen";
AdvancedOptions.DisplayOptionalEdgesButton.ShortHelp="Ermöglicht die Anzeige der optional einreißenden Kanten.";
AdvancedOptions.DisplayOptionalEdgesButton.LongHelp="Ermöglicht die Anzeige der optional einreißenden Kanten.";
AdvancedOptions.DistortionsFrame.Title="Farbzuordnungen für Verzerrungen";
AdvancedOptions.DistortionsFrame.UnfoldedDistButton.Title="Abgewickelte Fläche";
AdvancedOptions.DistortionsFrame.UnfoldedDistButton.ShortHelp="Längenverzerrungen der abgewickelten Fläche.";
AdvancedOptions.DistortionsFrame.UnfoldedDistButton.LongHelp="Zeigt die Farbzuordnung für die Längenverzerrung der abgewickelten Fläche im Arbeitsbereich an.
Die Längenverzerrung ist das Verhältnis zwischen dem Längenparameter des abgewickelten
Ausgabeelements und dem Längenparameter des Eingabeelements (in Prozent).
Bei einer positiven Verzerrung wird das abgewickelte Element gedehnt, bei einer negativen Verzerrung verkleinert.";
AdvancedOptions.DistortionsFrame.UnfoldedPermanentDistButton.Title="Dauerhaft";
AdvancedOptions.DistortionsFrame.UnfoldedPermanentDistButton.ShortHelp="Ermöglicht die dauerhafte Anzeige der Verzerrungen auf der abgewickelten Fläche.";
AdvancedOptions.DistortionsFrame.UnfoldedPermanentDistButton.LongHelp="Zeigt dauerhaft die Farbzuordnungen der Verzerrungen der abgewickelten Fläche an.";
AdvancedOptions.DistortionsFrame.SurfToUnfoldDistButton.Title="Abzuwickelnde Fläche";
AdvancedOptions.DistortionsFrame.SurfToUnfoldDistButton.ShortHelp="Zeigt die Verzerrungen der Eingabefläche an.";
AdvancedOptions.DistortionsFrame.SurfToUnfoldDistButton.LongHelp="Zeigt die Farbzuordnung für die Längenverzerrung der abzuwickelnden Fläche im Arbeitsbereich an.
Die Längenverzerrung ist das Verhältnis zwischen dem Längenparameter des abgewickelten
Ausgabeelements und dem Längenparameter des Eingabeelements (in Prozent).
Bei einer positiven Verzerrung wird das abgewickelte Element gedehnt, bei einer negativen Verzerrung verkleinert.";

TabContainer.PositionTabPage.Target.ReverseUButton.Title="Uf umkehren";
TabContainer.PositionTabPage.Target.ReverseUButton.ShortHelp="Ermöglicht das Umkehren der Uf-Richtung.";
TabContainer.PositionTabPage.Target.ReverseUButton.LongHelp="Ermöglicht das Umkehren der Uf-Richtung.";
TabContainer.PositionTabPage.Target.ReverseVButton.Title="Vf umkehren";
TabContainer.PositionTabPage.Target.ReverseVButton.ShortHelp="Ermöglicht das Umkehren der Vf-Richtung.";
TabContainer.PositionTabPage.Target.ReverseVButton.LongHelp="Ermöglicht das Umkehren der Vf-Richtung.";
TabContainer.PositionTabPage.Target.SwapButton.Title="Austauschen";
TabContainer.PositionTabPage.Target.SwapButton.ShortHelp="Ermöglicht das Austauschen der Uf- und Vf-Richtung.";
TabContainer.PositionTabPage.Target.SwapButton.LongHelp="Ermöglicht das Austauschen der Uf- und Vf-Richtung.";

ReferenceOrigin="Referenzursprung";
ReferenceDirection="Referenzrichtung";
TargetPlane="Zielebene";
TargetOrigin="Zielursprung";

DistortionsFrame.Title="Farbzuordnungen für Verzerrungen";
DistortionsFrame.UnfoldedDistButton.Title="Abgewickelte Fläche";
DistortionsFrame.UnfoldedDistButton.ShortHelp="Längenverzerrungen der abgewickelten Fläche.";
DistortionsFrame.UnfoldedDistButton.LongHelp="Zeigt die Farbzuordnung für die Längenverzerrung der abgewickelten Fläche im Arbeitsbereich an.
Die Längenverzerrung ist das Verhältnis zwischen dem Längenparameter des abgewickelten
Ausgabeelements und dem Längenparameter des Eingabeelements (in Prozent).
Bei einer positiven Verzerrung wird das abgewickelte Element gedehnt, bei einer negativen Verzerrung verkleinert.";
DistortionsFrame.UnfoldedPermanentDistButton.Title="Dauerhaft";
DistortionsFrame.UnfoldedPermanentDistButton.ShortHelp="Ermöglicht die dauerhafte Anzeige der Verzerrungen auf der abgewickelten Fläche.";
DistortionsFrame.UnfoldedPermanentDistButton.LongHelp="Zeigt dauerhaft die Farbzuordnungen der Verzerrungen der abgewickelten Fläche an.";

