//
//
//  COPYRIGHT DASSAULT SYSTEMES 2000
//  NLS Of UpdateError for SketcherModeler
//
// LGK 24/07/02 Revision
// LGK 15/10/02 -


SpecError="Die Spezifikationen k�nnen nicht ber�cksichtigt werden. Die Skizze �ndern.";

DatumError="Kopie ist fehlerhaft. Dieses Element kann isoliert oder inaktiviert werden";

DatumError1="Die aus der Referenzskizze erhaltene Geometrie kann nicht verwaltet werden. Zun�chst pr�fen, ob die Geometrie die Voraussetzungen f�r das geometrische Modellierungsprogramm von CATIA erf�llt. Dazu die Geometrie z. B. mit dem Projektionsbefehl der Fl�chenerzeugungsumgebung auf eine koplanare Ebene projizieren. \nWenn die Projektion unterst�tzt wird, muss die Referenzskizze bearbeitet und die Geometrie mit dem Befehl 'Skizzieranalyse' so korrigiert werden, dass eine Kopie vom Typ 'Als Ergebnis mit Verkn�pfung' unterst�tzt wird.";

DatumError2="Die Kopie kann nicht verwaltet werden. Die Referenzskizze hat kein Ergebnis.";

DatumWarning="Die Referenzkomponente ist keine Skizze mehr. Diese neue Referenz muss weiterhin in der Skizzierebene definiert sein.";

OverCstError="Diese Skizze ist �berbestimmt oder inkonsistent. Vor der Bearbeitung dieser Skizze \nkann mit 'Tools > Skizzieranalyse > Diagnose' ermittelt werden, welches Skizzierelement die Probleme verursacht.";

GeomError1="Keine Projektion zwischen /p1 und der Skizzenebene.";

GeomError2="Kein Schnittpunkt zwischen /p1 und der Skizzenebene.";

GeomError3="/p1 sind keine Geometriespezifikationen zugeordnet.";

GeomError4="Die Assoziativit�t mit mehreren resultierenden Geometrien aus /p1 kann nicht gew�hrleistet werden.";

GeomError5="Das 3D-Element, mit dem diese externe Erzeugungsgeometrie erzeugt wurde, wurde gel�scht.";

GeomError6="Die resultierende Geometrie unterscheidet sich von der vorherigen.";

GeomError7="Zwischen /p1 und /p2 und der Skizzierebene wurde keine �berschneidung festgestellt.";

GeomError8="Die Offsetgeometrie von /p1 kann in der Skizzierebene nicht bewertet werden.";

GeomError9="Die Offsetgeometrie von /p1 von /p2 kann in der Skizzierebene nicht bewertet werden.";

GeomError10="Die Offsetgeometrie von /p1 kann mit /p2 als Fortf�hrungsreferenz in der Skizzierebene nicht bewertet werden.";

GeomError11="Die Offsetgeometrie von /p1 aus /p2 kann mit /p3 als Fortf�hrungsreferenz in der Skizzierebene nicht bewertet werden.";

GeomError12="Die Offsetgeometrie von /p1 kann mit /p2 aus /p3 als Fortf�hrungsreferenz in der Skizzierebene nicht bewertet werden.";

GeomError13="Die Offsetgeometrie von /p1 aus /p2 kann mit /p3 aus /p4 als Fortf�hrungsreferenz in der Skizzierebene nicht bewertet werden.";

GeomDefault="Dieses Element kann gel�scht, isoliert oder inaktiviert werden. Alternativ kann ein neues Referenzelement ausgew�hlt werden.";

GeomPropag="Dieses Element kann gel�scht, isoliert oder inaktiviert werden. Alternativ kann eine nicht mehrdeutige Fortf�hrungsreferenz oder ein neues Referenzelement ausgew�hlt werden.";

GeomDelete="Diese Operation ist nur beim Bearbeiten der Skizze zul�ssig. Bitte die Skizze bearbeiten, um Geometrien zu l�schen.";

IncompCstError="Mindestens eine Bedingung ist mit der resultierenden Geometrie nicht kompatibel.";

IntersectDefault="Die Skizzierebene bewegen oder die verschneidende Komponente �ndern.";

SupportError="Ung�ltiges oder leeres Eingabeelement.\nIm Kontextmen� f�r diese Skizze den Befehl 'St�tzelement f�r Skizze �ndern' ausw�hlen.";

SupportErrorExternalReference="Das als Skizzenst�tzelement verwendete Element geh�rt nicht zum aktuellen Teil. Im Kontextmen� dieser Skizze den Befehl 'St�tzelement f�r Skizze �ndern' ausw�hlen.";

SupportErrorGeneral="Die Operation ist fehlgeschlagen, weil die Elemente ung�ltig sind. Im Kontextmen� dieser Skizze den Befehl 'St�tzelement f�r Skizze �ndern' ausw�hlen.";

SupportErrorTwoLinesIntersected="Zwischen /p1 und /p2 wurde keine Verschneidung festgestellt. Die zwei Linien m�ssen koplanar sein, um als Skizzenst�tzelement verwendet werden zu k�nnen. Im Kontextmen� dieser Skizze den Befehl 'St�tzelement f�r Skizze �ndern' ausw�hlen.";

SupportErrorTwoLines="/p1 und /p2 sind parallel. Im Kontextmen� dieser Skizze den Befehl 'St�tzelement f�r Skizze �ndern' ausw�hlen.";

SupportErrorRefNotAPlane="Die Referenzkomponente erlaubt nicht, die Skizzenebene zu definieren. Im Kontextmen� dieser Skizze den Befehl 'St�tzelement f�r Skizze �ndern' ausw�hlen.";

OriginErrorIntersectionOfTwoLines="Der Ursprung wurde als Schnittpunkt zweier Linien definiert, es wurde zwischen /p1 und /p2 jedoch keine Verschneidung festgestellt. Im Kontextmen� dieser Skizze den Befehl 'St�tzelement f�r Skizze �ndern' ausw�hlen.";

OriginErrorIntersectionCurveSupport="Der Ursprung wurde als Schnittpunkt zwischen einer Kurve mit der St�tzebene definiert, es wurde zwischen /p1 und der St�tzebene jedoch keine Verschneidung festgestellt. Im Kontextmen� dieser Skizze den Befehl 'St�tzelement f�r Skizze �ndern' ausw�hlen.";

OriginErrorExternalReference="Das Element, das den Ursprung definiert, geh�rt nicht zum aktuellen Teil. Im Kontextmen� dieser Skizze den Befehl 'St�tzelement f�r Skizze �ndern' ausw�hlen.";

OriginErrorUndefined="Bei der Ursprungsdefinition ist ein interner Fehler aufgetreten. Im Kontextmen� dieser Skizze den Befehl 'St�tzelement f�r Skizze �ndern' ausw�hlen.";

OrientationErrorProjectionIsNotALine="Die Projektion der Richtung auf der St�tzebene ist keine Linie. Deshalb kann die Ausrichtung nicht definiert werden. Im Kontextmen� dieser Skizze den Befehl 'St�tzelement f�r Skizze �ndern' ausw�hlen.";

OrientationErrorThroughAPoint="Die Projektion von /p1 ist mit dem Ursprung der St�tzebene vermischt. Deshalb kann die Ausrichtung nicht definiert werden. Im Kontextmen� dieser Skizze den Befehl 'St�tzelement f�r Skizze �ndern' ausw�hlen.";

OrientationErrorParallelToIntersectionWithPlane="Zwischen /p1 und der St�tzebene wurde keine Verschneidung festgestellt. Deshalb kann die Ausrichtung nicht definiert werden. Im Kontextmen� dieser Skizze den Befehl 'St�tzelement f�r Skizze �ndern' ausw�hlen.";

OrientationErrorExternalReference="Das Element, das diese Ausrichtung definiert, geh�rt nicht zum aktuellen Teil. Im Kontextmen� dieser Skizze den Befehl 'St�tzelement f�r Skizze �ndern' ausw�hlen.";

NotConsistentCst="Diese Bedingung ist inkonsistent. Die Elemente korrigieren, die Probleme verursachen (Geometrie oder Bedingung).";

OverDefinedCst="Dieses Element ist �berbestimmt. Die nicht erforderlichen Bedingungen l�schen.";

Unknown="Es kann keine Diagnose bereitgestellt werden.";

GeomData="Die geometrischen Daten sind verloren gegangen. Diese Geometrie muss gel�scht werden.";

EditSketch="Zum Beheben dieser Probleme muss die Skizze bearbeitet werden.";

AbsoluteAxisError="Die Definition der absoluten Achse muss ge�ndert werden, um diese Probleme zu beheben.";

NotConsistentCurve="Die Kurve kann nicht ausgewertet werden. Die Skizze muss bearbeitet und die Inkonsistenzen bei diesem Element m�ssen korrigiert werden.";

OutputDegeneratedGeometry="Die Referenzgeometrie ist degeneriert. Zum Beheben dieser Probleme muss die Skizze bearbeitet werden.";

OutputDeletedGeometry="Die Referenzgeometrie wurde gel�scht. Zum Beheben dieser Probleme muss die Skizze bearbeitet werden.";

BrokenCstError="Die Bedingung weist eine fehlerhafte Definition auf und kann nicht �ber die Skizze verwaltet werden. Die Teiledaten m�ssen bereinigt oder die Bedingung muss gel�scht werden.";

NotAllowedOperation="F�r diese Operation liegt keine Berechtigung vor.";

ProfileCopy="Diese Operation ist beim Bearbeiten der Skizze nicht zul�ssig. Die Skizzenbearbeitung beenden, um die Profilkomponente zu kopieren.";

ProfileDelete="Diese Operation ist nur beim Bearbeiten der Skizze zul�ssig. Die Skizze bearbeiten, um die Profilkomponente zu l�schen.";

ProfileNoResult="Das Profil kann nicht ausgewertet werden. Die Profildefinition �berpr�fen.";

ProfileConnexity="Das Profil ist nicht wie gefordert verbindungsf�hig. Die Profildefinition �berpr�fen oder die �berpr�fung der Punktstetigkeit inaktivieren (Konnektivit�t).";

ProfileManifold="Das Profil ist nicht wie gefordert vielf�ltig. Die Profildefinition �berpr�fen, um ein vielf�ltiges Ergebnis zu erhalten.";

ProfileTangency="Das Profil ist nicht wie gefordert tangentenstetig. Die Profildefinition �berpr�fen oder die �berpr�fung der Tangentenstetigkeit inaktivieren.";

ProfileCurvature="Das Profil ist nicht wie gefordert kr�mmungsstetig. Die Profildefinition �berpr�fen oder die �berpr�fung der Kr�mmungsstetigkeit inaktivieren.";

ProfileNotHomogeneous="Das Profil ist nicht homogen, sondern enth�lt Punkte und Drahtelemente. Die Profildefinition �ndern, um ein Profilergebnis zu erhalten, das nur Punkte oder nur Drahtelemente enth�lt.";

ProfileDomainsNbChange="Die Anzahl der Drahtelemente oder Punkte des Profils wurde ge�ndert. Das Profil bearbeiten, um die �nderungen zu �bernehmen, oder die Definition des Profils korrigieren.";

ProfileIntersection="Zwischen Drahtelementen bzw. in einem Drahtelement wurde eine Verschneidung erkannt (Selbst�berschreitung). Das Profil bearbeiten, um dessen Definition zu korrigieren.";

ProfileGeomRemoved="Bei der Aktualisierung des Profils bleiben nicht alle geometrischen Elemente erhalten, die zuvor verwendet wurden. Die �nderungen �berpr�fen, die am Profil vorgenommen wurden.";

UnderdefinedGeometries="Die Skizze ist nicht vollst�ndig bestimmt (unterbestimmt). Die erforderlichen Bedingungen festlegen (dazu 'Bearbeiten' anklicken) oder \nin 'Tools > Optionen > Skizzierer' die Option 'Aktualisierungsfehler generieren, wenn die Skizze unterbestimmt ist' inaktivieren, um zu verhindern, dass die Anwendung Aktualisierungsfehler generiert.";

Instability="Diese Operation ist m�glicherweise von algorithmischer Instabilit�t betroffen.";

InstabilityUpdate="Das Ergebnis einer potenziellen Umkehrung der geometrischen Position pr�fen.";

InstabilityCreate="Der Benutzer sollte den Entwurf neu �berdenken.";

Plane2DLUpdateError="Die Ebene kann nicht ausgewertet werden. In der Layoutansicht wurde keine Liniendefinition gefunden. Damit dieses Aktualisierungsproblem gel�st werden kann, muss die Ebene isoliert werden.";

Axis2DLUpdateError="Die Linie kann nicht ausgewertet werden. In der Layoutansicht wurde keine Punktdefinition gefunden. Damit dieses Aktualisierungsproblem gel�st werden kann, muss die Linie isoliert werden.";

Profile2DLNoSubset="Das Profil kann nicht ausgewertet werden. In der Layoutansicht wurde keine Profildefinition gefunden. Damit dieses Aktualisierungsproblem gel�st werden kann, muss das Profil isoliert werden.";

Profile2DLNonParallelPlane="Das Profil kann nicht ausgewertet werden. Die St�tzebene muss parallel zu der Layoutansicht sein, in der die Profilgeometrie definiert ist.";

ExternalLinkValuateError1="Das Profil hat keine externe Verkn�pfung.";

ExternalLinkValuateError2="Ersetzen der Verkn�pfung von der Profilkomponente nicht m�glich.";

ExternalLinkValuateError3="Ersetzen der Verkn�pfung durch dieses Dokument nicht m�glich.";

LMGEditionRequired="Die LMG-Skizze kann nicht aktualisiert werden, weil eine �nderung in der Topologie des Schnitts festgestellt wurde. Die entsprechenden Spezifikationen bearbeiten, anschlie�end ggf. die Fasen oder Ecken aktualisieren und OK anklicken.";

MultiNear="Es wurden mehrere nahe Elemente gefunden.";

InvalidOutputInput="Eingabedaten sind ung�ltig.";

OutputPlaneError01="Das Referenzelement muss eine Linie sein.";

OutputPlaneError02="Die Eingabelinie ist degeneriert.";

OutputAxisError01="Das Referenzelement muss ein Punkt sein.";

OutputAxisError02="Der Eingabepunkt ist degeneriert.";

SolverWarning="Skizze wird mit neuem Solver aktualisiert. Bedingungen und Geometrien sind davon m�glicherweise betroffen. Sichtpr�fung wird empfohlen.";
