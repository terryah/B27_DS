
// COPYRIGHT DASSAULT SYSTEMES 2000

//
// NLS Message for the CATScriptInternalEditorWindow dialog
//
 
Editor.Title                     	= "Makroeditor";

// Menu Management

MacroEditorBarMenu.File.Title = "Datei";
MacroEditorBarMenu.File.New.Title = "Neu";
MacroEditorBarMenu.File.Open.Title = "�ffnen...";
MacroEditorBarMenu.File.Save.Title = "Sichern";
MacroEditorBarMenu.File.SaveAs.Title = "Sichern unter...";
MacroEditorBarMenu.File.Exit.Title = "Beenden";

MacroEditorBarMenu.View.Title = "Ansicht";
MacroEditorBarMenu.View.ObjBrowser.Title = "Objektbrowser...";
MacroEditorBarMenu.View.TlbBrowser.Title = "Typbibliotheksbrowser...";
MacroEditorBarMenu.View.TlbDependencies.Title = "Grafik �ber die Abh�ngigkeiten der Typbibliothek...";

MacroEditorBarMenu.Edit.Title = "Bearbeiten";
MacroEditorBarMenu.Edit.GoToLine.Title = "Gehe zu Zeile:...";
MacroEditorBarMenu.Edit.Find.Title = "Suchen...";
MacroEditorBarMenu.Edit.Resolution.Title = "Objektaufl�sung einf�gen...";

MacroEditorBarMenu.Help.Title = "Hilfe";
MacroEditorBarMenu.Help.Help.Title = "Hilfe zur V5-Automatisierung...";

// Status Bar

StatusBar.Line							= "Zeile: ";
StatusBar.Column						= "Spalte: ";

// Tool Bar

MacroEditorToolBar.Title								= "Makroeditor";
MacroEditorToolBar.New.Title						= "Neu";
MacroEditorToolBar.New.ShortHelp				= "Neue Datei";
MacroEditorToolBar.Open.Title						= "�ffnen";
MacroEditorToolBar.Open.ShortHelp				= "Datei �ffnen";
MacroEditorToolBar.Save.Title						= "Sichern";
MacroEditorToolBar.Save.ShortHelp				= "Datei sichern";
MacroEditorToolBar.GoToLine.ShortHelp		= "Gehe zu Zeile:...";
MacroEditorToolBar.Find.ShortHelp				= "Suchen...";
MacroEditorToolBar.Resolution.ShortHelp	= "Objektaufl�sung einf�gen...";
MacroEditorToolBar.Resolution.LongHelp	= "F�gt einen Codeschnipsel ein, der einen genau festgelegten Zugriff auf das Objekt gew�hrt, das der Benutzer interaktiv ausgew�hlt hat.";
MacroEditorToolBar.ObjBrowser.ShortHelp	= "Objektbrowser anzeigen...";

// Browser

OpenDlgFile.Title						= "�ffnen";
SaveAsDlgFile.Title					= "Sichern unter";

TypeDescription.catvbs			= "MS VBScriptdateien (*.catvbs)";
TypeDescription.CATScript		= "Basic Scriptdatei (*.CATScript)";
TypeDescription.java				= "java-Datein (*.java)";
TypeDescription.AllFiles		= "Alle Dateien (*)";

// Prompt For Save

PromptForSave.Title					= "Makroeditor";
PromptForSave.Text					= "Der Text in Datei /p1 wurde ge�ndert.\nSollen die �nderungen gesichert werden?";

// Prompt For Create

PromptForCreate.Title					= "Makroeditor";
PromptForCreate.Text					= "Die Datei /p1 existiert nicht. Soll sie erzeugt werden?";

// Go To Line Input

GoToLineInputDlg.Title										= "Gehe zu Zeile";
GoToLineInputDlg.InputLineLabel.Title			=	"Zeilennummer: ";
GoToLineInputDlg.InputLineLabel.LongHelp	= "Enth�lt ein leeres Feld, in das die gew�nschte Zeilennummer eingegeben werden kann.";
GoToLineInputDlg.InputLineEditor.LongHelp	= "Enth�lt ein leeres Feld, in das die gew�nschte Zeilennummer eingegeben werden kann.";

// Find Input

FindInputDlg.Title																			= 	"Suchen";
FindInputDlg.BOK.Title																	= 	"Weitersuchen";
FindInputDlg.BCancel.Title															= 	"Schlie�en";
FindInputDlg.FindLabel.Title														= 	"Suchen nach:";
FindInputDlg.FindLabel.LongHelp													= 	"Enth�lt ein leeres Feld, in das der gesuchte Text eingegeben werden kann.";
FindInputDlg.FindEditor.LongHelp												= 	"Enth�lt ein leeres Feld, in das der gesuchte Text eingegeben werden kann.";
FindInputDlg.GroupRadioButtons.Title										= 	"Richtung";
FindInputDlg.GroupRadioButtons.LongHelp									= 	
"Gibt die Richtung an, in die die Suche von der Cursorposition im Dokument aus gestartet werden soll.
 . 'Nach oben' ausw�hlen, um r�ckw�rts zu suchen (in Richtung Dokumentanfang).
 . 'Nach unten' ausw�hlen, um vorw�rts zu suchen (in Richtung Dokumentende).";
FindInputDlg.GroupRadioButtons.UpRadioButton.Title			= 	"Nach oben";
FindInputDlg.GroupRadioButtons.DownRadioButton.Title		= 	"Nach unten";

// FindErrorWindow

FindErrorWindow.Title						= 	"Makroeditor";
FindErrorWindow.Text						= 	"Zeichenfolge \"/p1\" kann nicht gefunden werden.";

// File Error Management

ErrorWindow.Title 					= "Fehler in /p1";

FileError.TooManyOpenFiles	= "Zu viele ge�ffnete Dateien.";
FileError.FileExists				= "Die Datei \"/p1\" ist bereits vorhanden.";
FileError.AccessDenied			= "F�r die Datei \"/p1\" liegt keine Zugriffsberechtigung vor.";
FileError.InvalidName				= "Ung�ltiger Name f�r Datei \"/p1\".";
FileError.FileNotFound			= "Datei \"/p1\" nicht gefunden.";
FileError.DiskFull					= "Datentr�ger voll.";
FileError.InvalidParameter	= "Ung�ltige Parameter.";
FileError.InvalidHandle			= "Ung�ltige Dateikennung f�r Datei \"/p1\".";
FileError.OperationAborted	= "Operation abgebrochen.";
FileError.UnknownError			= "Unbekannter Fehler.";

Untitled = "Nicht benannt";

vbaResolutionMenuItemCaption = "Objektaufl�sung...";
vbaResolutionMenuItemDescription = "F�gt einen Codeschnipsel ein, der einen genau festgelegten Zugriff auf das Objekt gew�hrt, das der Benutzer interaktiv ausgew�hlt hat.";
vbaResolutionMenuItemToolTip = "F�gt einen Codeschnipsel ein, der einen genau festgelegten Zugriff auf das Objekt gew�hrt, das der Benutzer interaktiv ausgew�hlt hat.";
vbResolutionTxt = "\n'---- Anfang des Aufl�sungsscripts f�r Objekt: /p1\n\n/p2'---- Ende des Aufl�sungsscripts\n\n";
javaResolutionTxt = "\n    //---- Anfang des Aufl�sungsscripts f�r Objekt: /p1\n\n/p2    //---- Ende des Aufl�sungsscripts\n\n";
