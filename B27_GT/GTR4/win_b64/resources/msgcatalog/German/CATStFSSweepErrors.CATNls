//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES  1999
//==============================================================================
//
// CATStFSSweepErrors: Resource file for NLS purpose related to sweep errors 
//
//==============================================================================
//
// Implementation Notes:
//
//==============================================================================
// Feb 00   Creation                                    F. LETZELTER
//==============================================================================
UpdateError="Bei der Berechnung der Translationsfl�che ist ein Fehler aufgetreten.
F�r diese Operation gibt es keine L�sung.";
GeoFactoryError.Message="Die Geometrical Factory kann nicht abgerufen werden.";
ProfileRecupError.Message="Das Profil kann nicht abgerufen werden.";
RefProfilesRecupError.Message="Die Referenzprofile k�nnen nicht abgerufen werden.";
SpineRecupError.Message="Die Leitkurve kann nicht abgerufen werden.";
ContProfileRecupError.Message="Die Profilstetigkeit kann nicht abgerufen werden.";
ContSpineRecupError.Message="Die Leitkurvenstetigkeit kann nicht abgerufen werden.";
ContGuideRecupError.Message="Die F�hrungselementstetigkeit kann nicht abgerufen werden.";
MFTypeRecupError.Message="Der Typ des bewegten Dreibeins kann nicht abgerufen werden.";
MFLawRecupError.Message="Die Regel f�r das bewegte Dreibein kann nicht abgerufen werden.";
FirstLPRecupError.Message="Der erste Begrenzungspunkt kann nicht abgerufen werden";
SecondLPRecupError.Message="Der zweite Begrenzungspunkt kann nicht abgerufen werden";
MaxOrdersRecupError.Message="Die maximalen Grade k�nnen nicht abgerufen werden.";
MaxToleranceRecupError.Message="Die maximale Toleranz kann nicht abgerufen werden.";
OpCreationError.Message="Fehler beim Erzeugen des Translationsoperators.";
OpAllocationError.Message="Fehler beim Zuordnen des Translationsoperators.";
ProfileBodyError.Message="Der K�rper unter der Profilkomponente kann nicht abgerufen werden.";
SpineBodyError.Message="Der K�rper unter der Leitkurvenkomponente kann nicht abgerufen werden.";
SpineSupportBodyError.Message="Der K�rper des St�tzelements unter der Leitkurvenkomponente kann nicht abgerufen werden.";
GuideBodyError.Message="Der K�rper unter der F�hrungselementkomponente kann nicht abgerufen werden.";
GuideSupportBodyError.Message="Der K�rper des St�tzelements unter der F�hrungselementkomponente kann nicht abgerufen werden.";
ParamOpError.Message="Fehler beim Initialisieren der Parameter des Translationsoperators.";
CheckRunErrorM6.Message="An mehreren Ecken besteht Inkompatibilit�t.";
CheckRunErrorM5.Message="Bei dieser Konfiguration ist keine L�sung verf�gbar (Stetigkeiten pr�fen).";
CheckRunErrorM4.Message="Genauigkeit nicht erreicht.";
CheckRunErrorM3.Message="Die Berechnung wurde vor ihrem Abschluss gestoppt, m�glicherweise auf Grund einer Degenerierung.";
CheckRunErrorM2.Message="Eines der Referenzprofile besitzt weder eine G1- noch eine G2-Verbindung mit der St�tzfl�che der Leitkurve.";
CheckRunErrorM1.Message="Eines der Referenzprofile besitzt weder eine G1- noch eine G2-Verbindung mit der St�tzfl�che des F�hrungselements.";
CheckRunError1.Message="Der freie Teil der Leitkurve besitzt keine G1-Verbindung mit seiner partiellen St�tzfl�che.";
CheckRunError2.Message="Der freie Teil des F�hrungselements besitzt keine G1-Verbindung mit seiner partiellen St�tzfl�che.";
CheckRunError3.Message="Der freie Teil des Profils besitzt keine G1-Verbindung mit seiner partiellen St�tzfl�che.";
CheckRunError4.Message="Keine Verbindung zwischen Profil und erster vertikaler Verbindungsfl�che.";
CheckRunError5.Message="Keine Verbindung zwischen Profil und zweiter vertikaler Verbindungsfl�che.";
CheckRunError6.Message="Eines der Profile (Profil oder Referenzprofil) befindet sich nicht auf der
St�tzfl�che der Leitkurve oder der St�tzfl�che des F�hrungselements.";
CheckRunError7.Message="Die vertikale Verbindungsfl�che ist kein Gitter.";
CheckRunError8.Message="Die beiden Punkte, die die Begrenzungspunkte definieren, sind in Richtung des Profils falsch platziert.";
CheckRunError9.Message="Eine der Konturen weist keine G0-Stetigkeit zwischen den sie bildenden Kurven auf.";
CheckRunError10.Message="Das Profil ist auf einen Punkt zwischen Leitkurve und F�hrungselement reduziert. M�glicherweise sind F�hrungselement und Profil identisch.";
CheckRunError11.Message="Fehlerhafte Eingabedaten. Die Dimension der Eingabedaten �berpr�fen. Die Eingabedaten d�rfen nur aus einer Dom�ne stammen. Die Symbolleiste 'Benutzerauswahlfilter' dazu verwenden, ein Teil von Multidom�nenkomponenten auszuw�hlen.";
CheckRunError12.Message="Der Richtungsparameter ist ungeeignet: Er ist entweder null oder nicht rechtwinklig zur Tangentialrichtung des Profils.";
CheckRunError13.Message="Die festgelegte erforderliche geometrische Bedingung ist inkompatibel mit den erforderlichen Verbindungsbedingungen.";
CheckRunError14.Message="Die St�tzfl�che der Leitkurve oder des Profils besteht aus mehreren Teilen.";
CheckRunError15.Message="In dieser Situation ist keine Konstruktion m�glich. Ein anderes bewegtes Dreibein verwenden und die Systembedingungen pr�fen.";
CheckRunError16.Message="Es gibt keinen Schnittpunkt zwischen der Leitkurve und einem der Profile (Profil oder Referenzprofil).";
CheckRunError17.Message="Es gibt keinen Schnittpunkt zwischen dem F�hrungselement und einem der Profile (Profil oder Referenzprofil).";
CheckRunError18.Message="Das F�hrungselement ist keine Ebene.";
CheckRunError19.Message="Keine St�tzfl�che unter dem F�hrungselement oder unter dem Profil.";
CheckRunError20.Message="Die Profilfl�che kann nicht erzeugt werden. 
Das Profil stimmt vermutlich mit einer geraden oder einer kreisf�rmigen Linie in der N�he des Schnittpunkts mit dem F�hrungselement �berein.
Die Option Profil an zwei Kurven mit Skalierung verwenden.";
CheckRunError21.Message="Die erste vertikale Verbindungsfl�che besitzt weder eine G1- noch eine G2-Verbindung zu der Fl�che, die das Profil st�tzt (am Kontaktpunkt).";
CheckRunError22.Message="Die zweite vertikale Verbindungsfl�che besitzt weder eine G1- noch eine G2-Verbindung zu der Fl�che, die das Profil st�tzt (am Kontaktpunkt).";
CheckRunError23.Message="Das Profil oder eines der Referenzprofile besitzt weder eine G1- noch eine G2-Verbindung mit der St�tzfl�che der Leitkurve.";
CheckRunError24.Message="Das Profil oder eines der Referenzprofile besitzt weder eine G1- noch eine G2-Verbindung mit der St�tzfl�che des F�hrungselements.";
CheckRunError25.Message="Die Leitkurve besitzt keine G1-Verbindung mit der St�tzfl�che des Profils.";
CheckRunError26.Message="Das F�hrungselement besitzt keine G1-Verbindung mit der St�tzfl�che des Profils.";
