// COPYRIGHT DASSAULT SYSTEMES 1998
//===========================================================================
//
// CATRdgShootingEffectsFrame
//
//===========================================================================

//--------------------------------------
// Depth of field frame
//--------------------------------------
dofFrame.HeaderFrame.Global.Title = "Schärfentiefe";

dofFrame.IconAndOptionsFrame.OptionsFrame.dofParametersFrame.dofCheck.Title = "Aktiv";
dofFrame.IconAndOptionsFrame.OptionsFrame.dofParametersFrame.dofCheck.Help = "Schaltet Schärfentiefe ein/aus";
dofFrame.IconAndOptionsFrame.OptionsFrame.dofParametersFrame.dofCheck.ShortHelp = "Schärfentiefe ein-/ausschalten";
dofFrame.IconAndOptionsFrame.OptionsFrame.dofParametersFrame.dofCheck.LongHelp =
"Schaltet die Schärfentiefe ein/aus.
Die Ebene, in der Objekte scharf abgebildet werden, wird vom Kameraziel definiert.
Hinweis: Dieser Effekt wird nur mit Sichtkameras erzielt.";

dofFrame.IconAndOptionsFrame.OptionsFrame.dofParametersFrame.dofRadiusLabel.Title = "Radius der Verwaschung:";
dofFrame.IconAndOptionsFrame.OptionsFrame.dofParametersFrame.dofRadiusLabel.Help = "Definiert den Radius der Verwaschung";
dofFrame.IconAndOptionsFrame.OptionsFrame.dofParametersFrame.dofRadiusLabel.ShortHelp = "Radius der Verwaschung";
dofFrame.IconAndOptionsFrame.OptionsFrame.dofParametersFrame.dofRadiusLabel.LongHelp =
"Definiert den Radius der Verwaschung bei einer entsprechenden Brennweite. Hohe Werte vergrößern die Unschärfe. Die Ebene, in der Objekte scharf abgebildet werden, wird vom Kameraziel definiert.";

dofFrame.IconAndOptionsFrame.OptionsFrame.dofParametersFrame.dofRadiusSpinner.Help = "Definiert den Radius der Verwaschung";
dofFrame.IconAndOptionsFrame.OptionsFrame.dofParametersFrame.dofRadiusSpinner.ShortHelp = "Radius der Verwaschung";
dofFrame.IconAndOptionsFrame.OptionsFrame.dofParametersFrame.dofRadiusSpinner.LongHelp =
"Definiert den Radius der Verwaschung bei einer entsprechenden Brennweite. Hohe Werte vergrößern die Unschärfe. Die Ebene, in der Objekte scharf abgebildet werden, wird vom Kameraziel definiert.";

//--------------------------------------
// Glow frame
//--------------------------------------
glowFrame.HeaderFrame.Global.Title = "Glüherscheinung";

glowFrame.IconAndOptionsFrame.OptionsFrame.glowParametersFrame.glowCheck.Title = "Aktiv";
glowFrame.IconAndOptionsFrame.OptionsFrame.glowParametersFrame.glowCheck.Help = "Aktiviert die Glüherscheinung";
glowFrame.IconAndOptionsFrame.OptionsFrame.glowParametersFrame.glowCheck.ShortHelp = "Glüherscheinung ein-/ausschalten";
glowFrame.IconAndOptionsFrame.OptionsFrame.glowParametersFrame.glowCheck.LongHelp =
"Schaltet die Glüherscheinung ein/aus.";

glowFrame.IconAndOptionsFrame.OptionsFrame.glowParametersFrame.glowThresholdLabel.Title = "Schwellenwert:";
glowFrame.IconAndOptionsFrame.OptionsFrame.glowParametersFrame.glowThresholdLabel.Help = "Schwellenwert für die Glüherscheinung";
glowFrame.IconAndOptionsFrame.OptionsFrame.glowParametersFrame.glowThresholdLabel.ShortHelp = "Schwellenwert für die Glüherscheinung";
glowFrame.IconAndOptionsFrame.OptionsFrame.glowParametersFrame.glowThresholdLabel.LongHelp =
"Definiert den Schwellenwert für die Glüherscheinung. Ein Pixel mit einer Farbkomponente (rot, grün oder blau) über diesem Schwellenwert wird glühend angezeigt. Dieser Schwellenwert bestimmt die
Anzahl Pixel, die bei einem Bild mit einem Glüheffekt angezeigt werden. Hinweis: Eine größere Anzahl zu bearbeitender Pixel kann die Bearbeitungszeit beachtlich verlängern.";

glowFrame.IconAndOptionsFrame.OptionsFrame.glowParametersFrame.glowThresholdSpinner.Help = "Schwellenwert für die Glüherscheinung";
glowFrame.IconAndOptionsFrame.OptionsFrame.glowParametersFrame.glowThresholdSpinner.ShortHelp = "Schwellenwert für die Glüherscheinung";
glowFrame.IconAndOptionsFrame.OptionsFrame.glowParametersFrame.glowThresholdSpinner.LongHelp =
"Definiert den Schwellenwert für die Glüherscheinung. Ein Pixel mit einer Farbkomponente (rot, grün oder blau) über diesem Schwellenwert wird glühend angezeigt. Dieser Schwellenwert bestimmt die
Anzahl Pixel, die bei einem Bild mit einem Glüheffekt angezeigt werden. Hinweis: Eine größere Anzahl zu bearbeitender Pixel kann die Bearbeitungszeit beachtlich verlängern.";

glowFrame.IconAndOptionsFrame.OptionsFrame.glowParametersFrame.glowIntensityLabel.Title = "Intensität:";
glowFrame.IconAndOptionsFrame.OptionsFrame.glowParametersFrame.glowIntensityLabel.Help = "Intensität der Glüherscheinung";
glowFrame.IconAndOptionsFrame.OptionsFrame.glowParametersFrame.glowIntensityLabel.ShortHelp = "Intensität der Glüherscheinung";
glowFrame.IconAndOptionsFrame.OptionsFrame.glowParametersFrame.glowIntensityLabel.LongHelp =
"Definiert die Intensität der Glüherscheinung. Dieser Wert beeinflusst die Helligkeit der Glüherscheinung. Dieser Parameter hat keinen Einfluss auf die Bearbeitungszeit.";

glowFrame.IconAndOptionsFrame.OptionsFrame.glowParametersFrame.glowIntensitySpinner.Help = "Intensität der Glüherscheinung";
glowFrame.IconAndOptionsFrame.OptionsFrame.glowParametersFrame.glowIntensitySpinner.ShortHelp = "Intensität der Glüherscheinung";
glowFrame.IconAndOptionsFrame.OptionsFrame.glowParametersFrame.glowIntensitySpinner.LongHelp =
"Definiert die Intensität der Glüherscheinung. Dieser Wert beeinflusst die Helligkeit der Glüherscheinung. Dieser Parameter hat keinen Einfluss auf die Bearbeitungszeit.";

glowFrame.IconAndOptionsFrame.OptionsFrame.glowParametersFrame.glowSizeLabel.Title = "Größe: ";
glowFrame.IconAndOptionsFrame.OptionsFrame.glowParametersFrame.glowSizeLabel.Help = "Größe der Glüherscheinung";
glowFrame.IconAndOptionsFrame.OptionsFrame.glowParametersFrame.glowSizeLabel.ShortHelp = "Größe der Glüherscheinung";
glowFrame.IconAndOptionsFrame.OptionsFrame.glowParametersFrame.glowSizeLabel.LongHelp =
"Definiert die Größe der Glüherscheinung. Hinweis: Große Größenwerte führen möglicherweise zu einer beachtlichen Verlängerung der Bearbeitungszeit.";

glowFrame.IconAndOptionsFrame.OptionsFrame.glowParametersFrame.glowSizeSlider.Help = "Größe der Glüherscheinung";
glowFrame.IconAndOptionsFrame.OptionsFrame.glowParametersFrame.glowSizeSlider.ShortHelp = "Größe der Glüherscheinung";
glowFrame.IconAndOptionsFrame.OptionsFrame.glowParametersFrame.glowSizeSlider.LongHelp =
"Definiert die Größe der Glüherscheinung. Hinweis: Große Größenwerte führen möglicherweise zu einer beachtlichen Verlängerung der Bearbeitungszeit.";

glowFrame.IconAndOptionsFrame.OptionsFrame.glowParametersFrame.glowFlareFactorLabel.Title = "Intensität des Lichtblitzes";
glowFrame.IconAndOptionsFrame.OptionsFrame.glowParametersFrame.glowFlareFactorLabel.Help = "Intensität des Lichtblitzes";
glowFrame.IconAndOptionsFrame.OptionsFrame.glowParametersFrame.glowFlareFactorLabel.ShortHelp = "Intensität des Lichtblitzes:";
glowFrame.IconAndOptionsFrame.OptionsFrame.glowParametersFrame.glowFlareFactorLabel.LongHelp =
"Definiert die Intensität des Lichtblitzes. Der Lichtblitz ist die farbige Scheibe. Diese ist in der Regel kaum wahrnehmbar, daher sollten niedrige Werte verwendet werden. Der Wert 0 inaktiviert den
Lichtblitz. Dieser Parameter hat keine Auswirkung auf die Bearbeitungszeit.";

glowFrame.IconAndOptionsFrame.OptionsFrame.glowParametersFrame.glowFlareFactorSlider.Help = "Intensität des Lichtblitzes";
glowFrame.IconAndOptionsFrame.OptionsFrame.glowParametersFrame.glowFlareFactorSlider.ShortHelp = "Intensität des Lichtblitzes:";
glowFrame.IconAndOptionsFrame.OptionsFrame.glowParametersFrame.glowFlareFactorSlider.LongHelp =
"Definiert die Intensität des Lichtblitzes. Der Lichtblitz ist die farbige Scheibe. Diese ist in der Regel kaum wahrnehmbar, daher sollten niedrige Werte verwendet werden. Der Wert 0 inaktiviert den
Lichtblitz. Dieser Parameter hat keine Auswirkung auf die Bearbeitungszeit.";

glowFrame.IconAndOptionsFrame.OptionsFrame.glowParametersFrame.glowFlareDiffusionLabel.Title = "Streulicht";
glowFrame.IconAndOptionsFrame.OptionsFrame.glowParametersFrame.glowFlareDiffusionLabel.Help = "Streulicht";
glowFrame.IconAndOptionsFrame.OptionsFrame.glowParametersFrame.glowFlareDiffusionLabel.ShortHelp = "Streulicht:";
glowFrame.IconAndOptionsFrame.OptionsFrame.glowParametersFrame.glowFlareDiffusionLabel.LongHelp =
"Definiert die Breite des Lichtblitzes. Dieser Parameter hat keine Auswirkung auf die Bearbeitungszeit.";

glowFrame.IconAndOptionsFrame.OptionsFrame.glowParametersFrame.glowFlareDiffusionSlider.Help = "Streulicht";
glowFrame.IconAndOptionsFrame.OptionsFrame.glowParametersFrame.glowFlareDiffusionSlider.ShortHelp = "Streulicht:";
glowFrame.IconAndOptionsFrame.OptionsFrame.glowParametersFrame.glowFlareDiffusionSlider.LongHelp =
"Definiert die Breite des Lichtblitzes. Dieser Parameter hat keine Auswirkung auf die Bearbeitungszeit.";

glowFrame.IconAndOptionsFrame.OptionsFrame.glowParametersFrame.glowRadialLineSizeLabel.Title = "Größe der radialen Linien:";
glowFrame.IconAndOptionsFrame.OptionsFrame.glowParametersFrame.glowRadialLineSizeLabel.Help = "Größe der radialen Linien";
glowFrame.IconAndOptionsFrame.OptionsFrame.glowParametersFrame.glowRadialLineSizeLabel.ShortHelp = "Größe der radialen Linien";
glowFrame.IconAndOptionsFrame.OptionsFrame.glowParametersFrame.glowRadialLineSizeLabel.LongHelp =
"Definiert die maximale Länge der radialen Linien. Der Wert 0 inaktiviert die radialen Linien. Dieser Parameter hat keine Auswirkung auf die Bearbeitungszeit.";

glowFrame.IconAndOptionsFrame.OptionsFrame.glowParametersFrame.glowRadialLineSizeSlider.Help = "Größe der radialen Linien";
glowFrame.IconAndOptionsFrame.OptionsFrame.glowParametersFrame.glowRadialLineSizeSlider.ShortHelp = "Größe der radialen Linien";
glowFrame.IconAndOptionsFrame.OptionsFrame.glowParametersFrame.glowRadialLineSizeSlider.LongHelp =
"Definiert die maximale Länge der radialen Linien. Der Wert 0 inaktiviert die radialen Linien. Dieser Parameter hat keine Auswirkung auf die Bearbeitungszeit.";

glowFrame.IconAndOptionsFrame.OptionsFrame.glowParametersFrame.glowStarEffectLabel.Title = "Sterneneffekt:";
glowFrame.IconAndOptionsFrame.OptionsFrame.glowParametersFrame.glowStarEffectLabel.Help = "Sterneneffekt";
glowFrame.IconAndOptionsFrame.OptionsFrame.glowParametersFrame.glowStarEffectLabel.ShortHelp = "Sterneneffekt";
glowFrame.IconAndOptionsFrame.OptionsFrame.glowParametersFrame.glowStarEffectLabel.LongHelp =
"Definiert die Form der radialen Linien. Ein Wert von 0 bewirkt eine willkürliche Verteilung der radialen Linien, und ein Wert von 1 bewirkt kreuzförmige radiale Linien. Dieser Parameter hat keine
Auswirkung auf die Bearbeitungszeit.";

glowFrame.IconAndOptionsFrame.OptionsFrame.glowParametersFrame.glowStarEffectSlider.Help = "Sterneneffekt";
glowFrame.IconAndOptionsFrame.OptionsFrame.glowParametersFrame.glowStarEffectSlider.ShortHelp = "Sterneneffekt";
glowFrame.IconAndOptionsFrame.OptionsFrame.glowParametersFrame.glowStarEffectSlider.LongHelp =
"Definiert die Form der radialen Linien. Ein Wert von 0 bewirkt eine willkürliche Verteilung der radialen Linien, und ein Wert von 1 bewirkt kreuzförmige radiale Linien. Dieser Parameter hat keine
Auswirkung auf die Bearbeitungszeit.";

//--------------------------------------
// Cartoon frame
//--------------------------------------
cartoonFrame.HeaderFrame.Global.Title = "Zeichentrick";

cartoonFrame.IconAndOptionsFrame.OptionsFrame.cartoonParametersFrame.cartoonCheck.Title = "Aktiv";
cartoonFrame.IconAndOptionsFrame.OptionsFrame.cartoonParametersFrame.cartoonCheck.Help = "Schaltet die Zeichentrickwiedergabe ein/aus";
cartoonFrame.IconAndOptionsFrame.OptionsFrame.cartoonParametersFrame.cartoonCheck.ShortHelp = "Die Zeichentrickwiedergabe ein-/ausschalten";
cartoonFrame.IconAndOptionsFrame.OptionsFrame.cartoonParametersFrame.cartoonCheck.LongHelp =
"Schaltet die Zeichentrickwiedergabe ein/aus.";

cartoonFrame.IconAndOptionsFrame.OptionsFrame.cartoonParametersFrame.cartoonShadingCheck.Title = "Nur Konturen";
cartoonFrame.IconAndOptionsFrame.OptionsFrame.cartoonParametersFrame.cartoonShadingCheck.Help = "Schaltet die Flächenschattierung ein/aus";
cartoonFrame.IconAndOptionsFrame.OptionsFrame.cartoonParametersFrame.cartoonShadingCheck.ShortHelp = "Flächenschattierung ein-/ausschalten";
cartoonFrame.IconAndOptionsFrame.OptionsFrame.cartoonParametersFrame.cartoonShadingCheck.LongHelp =
"Schaltet die Flächenschattierung ein/aus.
Ist diese Option markiert, werden nur Konturen wiedergegeben,
und die Schattierung der Flächen wird während der Nachbearbeitung der Wiedergabe verworfen.";

cartoonFrame.IconAndOptionsFrame.OptionsFrame.cartoonParametersFrame.cartoonThicknessLabel.Title = "Konturstärke:";
cartoonFrame.IconAndOptionsFrame.OptionsFrame.cartoonParametersFrame.cartoonThicknessLabel.Help = "Konturstärke";
cartoonFrame.IconAndOptionsFrame.OptionsFrame.cartoonParametersFrame.cartoonThicknessLabel.ShortHelp = "Konturstärke";
cartoonFrame.IconAndOptionsFrame.OptionsFrame.cartoonParametersFrame.cartoonThicknessLabel.LongHelp =
"Definiert die Konturstärke, die als bestimmter Prozentsatz der Bildgröße angegeben wird.";

cartoonFrame.IconAndOptionsFrame.OptionsFrame.cartoonParametersFrame.cartoonThicknessSpinner.Help = "Konturstärke";
cartoonFrame.IconAndOptionsFrame.OptionsFrame.cartoonParametersFrame.cartoonThicknessSpinner.ShortHelp = "Konturstärke";
cartoonFrame.IconAndOptionsFrame.OptionsFrame.cartoonParametersFrame.cartoonThicknessSpinner.LongHelp =
"Definiert die Konturstärke, die als bestimmter Prozentsatz der Bildgröße angegeben wird.";

cartoonFrame.IconAndOptionsFrame.OptionsFrame.cartoonParametersFrame.cartoonStrokeCheck.Title = "Federhaltereffekt";
cartoonFrame.IconAndOptionsFrame.OptionsFrame.cartoonParametersFrame.cartoonStrokeCheck.Help = "Schaltet den Federhaltereffekt ein/aus";
cartoonFrame.IconAndOptionsFrame.OptionsFrame.cartoonParametersFrame.cartoonStrokeCheck.ShortHelp = "Federhaltereffekt ein-/ausschalten";
cartoonFrame.IconAndOptionsFrame.OptionsFrame.cartoonParametersFrame.cartoonStrokeCheck.LongHelp =
"Schaltet den Federhaltereffekt ein/aus.
Wenn diese Option markiert ist, ist die Stärke der Konturlinie von ihrer Ausrichtung abhängig
Die Konturlinien sind in Strichrichtung breiter.";
