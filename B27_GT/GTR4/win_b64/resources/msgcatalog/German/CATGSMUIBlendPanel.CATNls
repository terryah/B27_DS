//---------------------------------------
// Resource file for CATGSMUIBlendPanel class
// En_US
//---------------------------------------

Title="Definition des �bergangs";

LimitFrame.StartLimFrame.StartCurveLabel.Title = "Erste Kurve: ";
LimitFrame.StartLimFrame.StartCurveLabel.LongHelp = "Gibt die erste Kurve der Rundung an.";

LimitFrame.StartLimFrame.StartSupportLabel.Title = "Erstes St�tzelement: ";
LimitFrame.StartLimFrame.StartSupportLabel.LongHelp = "Gibt das erste St�tzelement der Rundung an. Die erste Kurve muss auf diesem St�tzelement aufliegen.";

LimitFrame.EndLimFrame.EndCurveLabel.Title = "Zweite Kurve: ";
LimitFrame.EndLimFrame.EndCurveLabel.LongHelp = "Gibt die zweite Kurve der Rundung an.";

LimitFrame.EndLimFrame.EndSupportLabel.Title = "Zweites St�tzelement: ";
LimitFrame.EndLimFrame.EndSupportLabel.LongHelp = "Gibt das zweite St�tzelement der Rundung an. Die zweite Kurve muss auf diesem St�tzelement aufliegen.";

ParameterFrame.ParamTabContainer.BasicPage.Title = "Basis";
ParameterFrame.ParamTabContainer.BasicPage.LongHelp = "Angaben zur Stetigkeit und zum Trimmen der St�tzelemente f�r die beiden Begrenzungen der Rundung.";

ParameterFrame.ParamTabContainer.BasicPage.BasicFrame.ContFrame.StartContFrame.StartContLabel.Title = "Erste Stetigkeit: ";
ParameterFrame.ParamTabContainer.BasicPage.BasicFrame.ContFrame.StartContFrame.StartContLabel.LongHelp = 
"Gibt die Stetigkeit zwischen der Rundung und dem ersten St�tzelement an.
Bei der Stetigkeit kann es sich um Punkt-, Tangenten- oder Kr�mmungsstetigkeit handeln.";

ParameterFrame.ParamTabContainer.BasicPage.BasicFrame.ContFrame.StartTrimButton.Title = "Erstes St�tzelement trimmen";
ParameterFrame.ParamTabContainer.BasicPage.BasicFrame.ContFrame.StartTrimButton.LongHelp = 
"Gibt an, ob das erste St�tzelement von der ersten Kurve getrimmt und mit der Rundung verkn�pft wird.";

ParameterFrame.ParamTabContainer.BasicPage.BasicFrame.ContFrame.StartBorderFrame.StartBorderLabel.Title = "Begrenzungen der ersten Tangente:";
ParameterFrame.ParamTabContainer.BasicPage.BasicFrame.ContFrame.StartBorderFrame.LongHelp = 
"Gibt an, ob die �bergangsbegrenzungen tangential zum ersten St�tzelement sind:
   - Beide �u�ere Punkte: die Begrenzungen an beiden �u�eren Punkten der ersten Kurve sind tangential
   - Keine: Begrenzungen sind nicht tangentenstetig
   - Nur am �u�eren Startpunkt: die Begrenzung am �u�eren Startpunkt der ersten Kurve ist tangential
   - Nur am �u�eren Endpunkt: die Begrenzung am �u�eren Endpunkt der ersten Kurve ist tangential.";

BothExtreTitle="Beide Endpunkte";
NoneExtreTitle="Keine";
StartExtreTitle="Nur erster Endpunkt";
EndExtreTitle="Nur letzter Endpunkt";
FirstOrExtreTitle="Ursprung der freien ersten Kurve";
SecondOrExtreTitle="Ursprung der freien zweiten Kurve";
FirstEndExtreTitle="Ende der freien ersten Kurve";
SecondEndExtreTitle="Ende der freien zweiten Kurve";
BothExtreConnectTitle="Beide Extremwerte verbinden";

ParameterFrame.ParamTabContainer.BasicPage.BasicFrame.TrimFrame.EndContFrame.EndContLabel.Title = "Zweite Stetigkeit: ";
ParameterFrame.ParamTabContainer.BasicPage.BasicFrame.TrimFrame.EndContFrame.EndContLabel.LongHelp = 
"Gibt die Stetigkeit zwischen der Rundung und dem zweiten St�tzelement an.
Bei der Stetigkeit kann es sich um Punkt-, Tangenten- oder Kr�mmungsstetigkeit handeln.";

ParameterFrame.ParamTabContainer.BasicPage.BasicFrame.TrimFrame.EndTrimButton.Title = "Zweites St�tzelement trimmen";
ParameterFrame.ParamTabContainer.BasicPage.BasicFrame.TrimFrame.EndTrimButton.LongHelp = 
"Gibt an, ob das zweite St�tzelement von der zweiten Kurve getrimmt und mit der Rundung verkn�pft wird.";

ParameterFrame.ParamTabContainer.BasicPage.BasicFrame.TrimFrame.EndBorderFrame.EndBorderLabel.Title = "Begrenzungen der zweiten Tangente:";
ParameterFrame.ParamTabContainer.BasicPage.BasicFrame.TrimFrame.EndBorderFrame.LongHelp = 
"Gibt an, ob die �bergangsbegrenzungen tangential zum zweiten St�tzelement sind:
   - Beide �u�ere Punkte: die Begrenzungen an beiden �u�eren Punkten der ersten Kurve sind tangential
   - Keine: Begrenzungen sind nicht tangentenstetig
   - Nur am �u�eren Startpunkt: die Begrenzung am �u�eren Startpunkt der ersten Kurve ist tangential
   - Nur am �u�eren Endpunkt: die Begrenzung am �u�eren Endpunkt der ersten Kurve ist tangential.";

ParameterFrame.ParamTabContainer.PointPage.Title = "Endpunkte";
ParameterFrame.ParamTabContainer.PointPage.LongHelp = "Gibt den Endpunkt f�r jede geschlossene Kurve der Rundung an.";

ParameterFrame.ParamTabContainer.PointPage.PointFrame.StartPointFrame.StartPointLabel.Title = "Erster Endpunkt: ";
ParameterFrame.ParamTabContainer.PointPage.PointFrame.StartPointFrame.StartPointLabel.LongHelp = 
"Gibt den Endpunkt der ersten Kurve an. Die erste Kurve muss geschlossen sein.";

ParameterFrame.ParamTabContainer.PointPage.PointFrame.EndPointFrame.EndPointLabel.Title = "Zweiter Endpunkt: ";
ParameterFrame.ParamTabContainer.PointPage.PointFrame.EndPointFrame.EndPointLabel.LongHelp =
"Gibt den Endpunkt der zweiten Kurve an. Die zweite Kurve muss geschlossen sein.";

ParameterFrame.ParamTabContainer.CouplingPage.Title = "Verbindung / Leitkurve";
ParameterFrame.ParamTabContainer.CouplingPageWithoutSpine.Title = "Verbindung";
ParameterFrame.ParamTabContainer.CouplingPage.LongHelp = "Gibt an, wie die Kurven miteinander gekoppelt werden: - Faktor: Die Kurven werden entsprechend dem Verh�ltnis (Faktor) der kurvenf�rmigen Abszisse gekoppelt.
- Tangential: Wenn jede der Kurven die gleiche Anzahl Tangentenunstetigkeitspunkte aufweist, werden diese Punkte miteinander gekoppelt. Andernfalls wird eine Fehlernachricht angezeigt.
- Tangential, dann Kr�mmung: Wenn jede der Kurven die gleiche Anzahl Tangentenunstetigkeitspunkte und Kr�mmungsunstetigkeitspunkte aufweist, werden zuerst die Tangentenunstetigkeitspunkte und danach die Kr�mmungsunstetigkeitspunkte miteinander gekoppelt.
Andernfalls wird eine Fehlernachricht angezeigt.
- Scheitelpunkte: Wenn jede der Kurven die gleiche Anzahl Scheitelpunkte aufweist, werden diese Punkte miteinander gekoppelt.
Andernfalls wird eine Fehlernachricht angezeigt.
- Leitkurve: Die Verbindung wird entsprechend der Leitkurve erzeugt.";

ParameterFrame.ParamTabContainer.TensionPage.Title = "Spannung";
ParameterFrame.ParamTabContainer.TensionPage.LongHelp = 
"Gibt die Spannung an, die auf jedem St�tzelement der Rundung lastet.";

ParameterFrame.ParamTabContainer.TensionPage.TensionFrame.StartTensionFrame.StartTensionButtonFrame.StartTension.Title = "Erste Spannung: ";
ParameterFrame.ParamTabContainer.TensionPage.TensionFrame.StartTensionFrame.StartTensionButtonFrame.StartTension.LongHelp =
"Gibt die Spannung an, die auf dem ersten St�tzelement der Rundung lastet.";

ParameterFrame.ParamTabContainer.TensionPage.TensionFrame.EndTensionFrame.EndTensionButtonFrame.EndTension.Title = "Zweite Spannung: ";
ParameterFrame.ParamTabContainer.TensionPage.TensionFrame.EndTensionFrame.EndTensionButtonFrame.EndTension.LongHelp =
"Gibt die Spannung an, die auf dem zweiten St�tzelement der Rundung lastet.";

ParameterFrame.ParamTabContainer.TensionPage.TensionFrame.StartTensionFrame.StartTensionButtonFrame.StartDefault.Title = "Standard";
ParameterFrame.ParamTabContainer.TensionPage.TensionFrame.StartTensionFrame.StartTensionButtonFrame.StartDefault.LongHelp = 
"Gibt an, dass die Spannung auf dem ersten St�tzelement die der Rundung die Standardspannung darstellt.";

ParameterFrame.ParamTabContainer.TensionPage.TensionFrame.EndTensionFrame.EndTensionButtonFrame.EndDefault.Title = "Standard";
ParameterFrame.ParamTabContainer.TensionPage.TensionFrame.EndTensionFrame.EndTensionButtonFrame.EndDefault.LongHelp = 
"Gibt an, dass die Spannung auf dem zweiten St�tzelement der Rundung die Standardspannung darstellt.";

ActionFrame.ReplaceButton.Title = "Ersetzen";
ActionFrame.RemoveButton.Title = "Entfernen";
ActionFrame.ReverseButton.Title = "Umkehren";


PointContTitle="Punkt";
TangencyContTitle="Tangentenstetigkeit";
CurvatureContTitle="Kr�mmung";

ParameterFrame.ParamTabContainer.TensionPage.TensionFrame.StartTensionFrame.StartTensionButtonFrame.StartTensionComb.LongHelp = 
"Gibt an, dass die Spannung auf dem ersten St�tzelement der Rundung konstant oder linear ist.";

ParameterFrame.ParamTabContainer.TensionPage.TensionFrame.EndTensionFrame.EndTensionButtonFrame.EndTensionComb.LongHelp = 
"Gibt an, dass die Spannung auf dem zweiten St�tzelement der Rundung konstant oder linear ist.";

ConstantTensionTitle="Konstant";
LinearTensionTitle="Linear";
STypeTensionTitle=" S-Typ  ";

RatioCouplTitle="Faktor";
TangencyCouplTitle="Tangentenstetigkeit";
CurvatureCouplTitle="Tangentenstetigkeit, dann Kr�mmung";
VertexCouplTitleKey="Scheitelpunkte";
SpineCouplTitleKey="Leitkurve";
AvoidTwistsCouplTitleKey="Verdrehungen vermeiden";

ParameterFrame.ParamTabContainer.TensionPage.TensionFrame.StartTensionFrame.StartValueFrame.StartParmFrame1.LongHelp = 
"Gibt den Wert f�r die erste Spannung auf dem ersten St�tzelement der Rundung an, wenn die Spannungsart konstant oder linear ist.";

ParameterFrame.ParamTabContainer.TensionPage.TensionFrame.StartTensionFrame.StartValueFrame.EndParmFrame1.LongHelp = 
"Gibt den Wert f�r die zweite Spannung auf dem ersten St�tzelement der Rundung an, wenn die Spannungsart linear ist.";

ParameterFrame.ParamTabContainer.TensionPage.TensionFrame.EndTensionFrame.EndValueFrame.StartParmFrame2.LongHelp = 
"Gibt den Wert f�r die erste Spannung auf dem zweiten St�tzelement der Rundung an, wenn die Spannungsart konstant oder linear ist.";

ParameterFrame.ParamTabContainer.TensionPage.TensionFrame.EndTensionFrame.EndValueFrame.EndParmFrame2.LongHelp = 
"Gibt den Wert f�r die zweite Spannung auf dem zweiten St�tzelement der Rundung an, wenn die Spannungsart linear ist.";

StartExtremityKey="T1: ";
EndExtremityKey="T2:  ";

ChaineDefaut="Standard";
ParameterFrame.ParamTabContainer.CouplingPage.SpineFrame.SpineLabel.Title="Leitkurve:";
ParameterFrame.ParamTabContainer.CouplingPage.SpineFrame.SpineLabel.LongHelp="Gibt die Leitkurve an, die ber�cksichtigt werden soll,\nwenn der Verbindungsmodus auf 'Leitkurve' festgelegt ist.";
ParameterFrame.ParamTabContainer.CouplingPage.SpineFrame.SpineSelector.LongHelp="Gibt die Leitkurve an, die ber�cksichtigt werden soll,\nwenn der Verbindungsmodus auf 'Leitkurve' festgelegt ist.";

// Tolerant blend
//------------------
FrameSmooth.Title="Gl�ttungsparameter";

FrameSmooth.SmoothDevButton.Title="Abweichung: ";
FrameSmooth.SmoothDevButton.LongHelp="Die Abweichung w�hrend der Operation aktivieren/inaktivieren.";
FrameSmooth.FrameLayoutDev.FraLittSmoothDev.EnglobingFrame.IntermediateFrame.Spinner.Title="Abweichung";

FrameSmooth.SmoothAngleButton.Title="Winkelkorrektur: ";
FrameSmooth.SmoothAngleButton.LongHelp="Den Schwellenwert f�r Winkel w�hrend der Operation aktivieren/inaktivieren.";
FrameSmooth.FrameLayoutAngle.FraLittSmoothAngle.EnglobingFrame.IntermediateFrame.Spinner.Title="Winkelkorrektur";

// Developable
ParameterFrame.ParamTabContainer.RuledDevPage.Title = "Abwickelbar";
ParameterFrame.ParamTabContainer.RuledDevPage.LongHelp = "Eine abwickelbare Regelfl�che mit oder ohne isoparametrische Verbindungen f�r Fl�chenbegrenzung generieren.";

ParameterFrame.ParamTabContainer.RuledDevPage.RuledDevFrame.RuledDevButton.Title = "Abwickelbare Regelfl�che erzeugen";
ParameterFrame.ParamTabContainer.RuledDevPage.RuledDevFrame.RuledDevButton.LongHelp = 
"Den Bediener zwingen, eine abwickelbare Regelfl�che zu generieren.";

//ParameterFrame.ParamTabContainer.RuledDevPage.RuledDevFrame.IsoConnectionFrame.Title = "Surface Boundary Isopar Connections";
ParameterFrame.ParamTabContainer.RuledDevPage.RuledDevFrame.IsoConnectionFrame.LabelSepar.Title="Isoparametrische Verbindungen f�r Fl�chenbegrenzung";

ParameterFrame.ParamTabContainer.RuledDevPage.RuledDevFrame.IsoConnectionFrame.StartFrame.StartLabel.Title = "Anfang: ";
ParameterFrame.ParamTabContainer.RuledDevPage.RuledDevFrame.IsoConnectionFrame.StartFrame.LongHelp=
"Eine Verbindungsbedingung f�r den Anfang der generierten Fl�che w�hlen.";

ParameterFrame.ParamTabContainer.RuledDevPage.RuledDevFrame.IsoConnectionFrame.EndFrame.EndLabel.Title = "Ende: ";
ParameterFrame.ParamTabContainer.RuledDevPage.RuledDevFrame.IsoConnectionFrame.EndFrame.EndLabel.LongHelp=
"Eine Verbindungsbedingung f�r das Ende der generierten Fl�che w�hlen.";
