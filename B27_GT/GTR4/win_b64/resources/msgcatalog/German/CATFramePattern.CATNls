//=============================================================================
//                                     CNEXT - CXR1
//                          COPYRIGHT DASSAULT SYSTEMES 1997 
//-----------------------------------------------------------------------------
// FILENAME    :    CATFramePattern
// LOCATION    :    DraftingIntUI/CNext/resources/msgcatalog
// AUTHOR      :    Hichem BEN CHEIKH
// DATE        :    Janv. 01 1998
//------------------------------------------------------------------------------
// DESCRIPTION :    Resources File for NLS purpose related to Drafting WorkShop
//                  
//------------------------------------------------------------------------------
// COMMENTS    :
//------------------------------------------------------------------------------
//MODIFICATIONS     user  date      purpose
//   HISTORY        ----  ----      -------
//     01		lgk 17.12.02 Revision
//------------------------------------------------------------------------------

// * Label *
EditorMaterialTitle="Zeichnung";

Pattern.Hatching.labelNbHatching.Title="Anzahl Linien:";

Pattern.Dotting.DottinglabelPitch.Title="Dichte:";
Pattern.Dotting.DottinglabelCol.Title="Farbe:";
Pattern.Dotting.DottinglabelZig.Title="Zickzack";

Pattern.Coloring.ColoringlabelCol.Title="Farbe:";

Pattern.Motif.BrowseButton.Title="Durchsuchen...";
Pattern.Motif.ModifAngle.Title="Winkel:";
Pattern.Motif.ModifScale.Title="     Ma�stab:";

Pattern.None.NonelabelCol.Title="Kein Muster zugeordnet";

Pattern.Hatching.labelNbHatching.LongHelp="Definiert die Anzahl der Linien.";
Pattern.Dotting.DottinglabelPitch.LongHelp="Legt die Dichte f�r das Muster fest.";
Pattern.Dotting.DottinglabelCol.LongHelp="Legt die Musterfarbe fest.";
Pattern.Dotting.DottinglabelZig.LongHelp="Legt Zickzack als Muster fest.";
Pattern.Coloring.ColoringlabelCol.LongHelp="Legt die Musterfarbe fest.";
Pattern.Motif.BrowseButton.LongHelp="Erm�glicht die Auswahl einer Datei zur Definition des Bilds, das als Muster verwendet werden soll.";
Pattern.Motif.ModifAngle.LongHelp="Legt den Bildwinkel fest.";
Pattern.Motif.ModifScale.LongHelp="Legt den Ma�stab des Bildes fest.";
Pattern.None.NonelabelCol.LongHelp="Gibt an, dass kein Muster zugeordnet wurde.";

Parameters.PatternNameLabel.Title="    Name:";
Parameters.TypePatternLabel.Title="Typ:"; 

labelAngle="Winkel:";
labelPitch="Dichte:";
labelOffset="Offset: ";
labelThickness="St�rke:";
labelColor="Farbe:";
labelTxt="Linientyp:";

labelAngle.LongHelp="Legt den Winkel fest.";
labelPitch.LongHelp="Legt die Steigung fest.";
labelOffset.LongHelp="Legt den Offset fest.";
labelThickness.LongHelp="Legt die Strichst�rke fest.";
labelColor.LongHelp="Legt die Farbe fest.";
labelTxt.LongHelp="Legt den Linientyp fest.";

Parameters.TypePattern.ShortHelp="Typ"; 
Pattern.Hatching.labelThk.Title="Anzahl Linien";
TypeHatching="Linien";
TypeDotting="Punkte";
TypeColoring="Farbe";
TypeMotif="Bild";
TypeNone="Kein";
TabTitle="Muster";

Parameters.TypePattern.LongHelp="Legt den Typ fest."; 
Pattern.Hatching.labelThk.LongHelp="Legt die Anzahl der Linien fest.";
TypeHatching.LongHelp="Legt die Linien fest.";
TypeDotting.LongHelp="Legt die Punkte fest.";
TypeColoring.LongHelp="Legt die Farbe fest.";
TypeNone.LongHelp="Verwendet kein Muster.";

Parameters.PatternTable.Title="...";
Parameters.PatternTable.LongHelp="Legt die Mustertabelle fest.";
TabTitle.LongHelp="Legt das Muster fest.";

MaterialTitle.Title="Material";
PreviewTitle.Title=" Voranzeige";

HatchingPage="Linien";
PatternChooser="Musterauswahl";

Material.LongHelp="Liest die Materialparameter.";
Parameters.LongHelp="Legt die Parameter fest.";
Pattern.LongHelp="Legt das Muster fest.";
Pattern.Hatching.LongHelp="Legt die Linien fest.";
Pattern.Dotting.LongHelp="Legt die Punkte fest.";
Pattern.Coloring.LongHelp="Legt die Farbe fest.";
Pattern.None.LongHelp="Verwendet kein Muster.";
Preview.LongHelp="Legt die Voranzeige fest.";
HatchingPage.LongHelp="Legt die Linien fest.";
PatternChooser.LongHelp="Legt die Musterauswahl fest.";

Material.MaterialButton.Title="Zur�cksetzen unter Verwendung des Teilematerialmusters";
Material.HasNOMaterial.Title="Kein Material f�r Teil";

Material.MaterialButton.LongHelp="Setzt die Werte unter Verwendung des Musters zur�ck, das f�r das Teilematerial verwendet wurde";
