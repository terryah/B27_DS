//NLS-ENGLISH VERSION
//=======================================================================
// MODULE :  IgesDataExchange
//=======================================================================


// KVL 2002-06-04 : Creation de CATIgesExport.CATNls et refonte des anciens CATNls Iges.
// DFB 2002-03-07 : Ajout de CircleReparam 
// KVL 2002-04-15 : CATIgesCompositeCurve  
// KVL 2002-05-30 : CATIgesInit : info sur le choix des options BSpline ou Standard � l'export 
// KVL 2002-06-07 : CATIgesInit : info sur le choix de l'export des entit�s en NoShow et des options wireframe ou Surfacic
// KVL 2002-06-19 : CATIgesVirtualBody et CATIgesElement : Info sur chaque �l�ment cr�e (Type, Show, Layer, Name, Color)
// KVL 2002-07-03 : CATIgesCurveOnParametricSurface : �chec de cr�ation des PCurve.
// OPC 2006-06-23 : Reecriture des messages niveau Customer pour meilleure compr�hension

// Numeros utilises : (export Iges : de [6001] a [6999]
// [6001] a [6049] : CATIgesBSplineCurve
// [6050] a [6099] : CATIgesCompositeCurve
// [6100] a [6149] : CATIgesInit
// [6150] a [6199] : CATIgesVirtualBody
// [6200] a [6249] : CATIgesElement
// [6250] a [6299] : CATIgesCurveOnParametricSurface
// [6300] a [6349] : CATIgesTrimmedParametricSurface
// [6350] a [6370] : CATSaveAsIGS
// [6850] a [6860] : CATIgesInterface

//----------------------------------------------
//  Messages from CATIgesBSplineCurve
//---------------------------------------------

CATIgesBSplineCurve.Info.CircleReparam="   <I> [6001] [T=/p2] [/p1] Der exportierte Kreis wird erneut parametrisiert /p3";

CATIgesBSplineCurve.Info.ParamInverse="   <I> [6002] [T=/p2] [/p1] Die B-Spline-Parameter werden umgekehrt /p3";

CATIgesBSplineCurve.Info.BSplineFermee="   <I> [6003] [T=/p2] [/p1] Die BSpline ist geschlossen /p3";

//-----------------------------------------------
//  Messages from CATIgesCompositeCurve
//-----------------------------------------------

CATIgesCompositeCurve.Info.hierar="   <I> [6050] [T=/p2] [#/p4] Alle von diesem Element abh�ngigen Elemente �bernehmen ihre Attribute /p3";



//-----------------------------------------------
//  Messages from CATIgesInit
//-----------------------------------------------

CATIgesInit.Info.optionBSpline="   <I> [6100] Die BSpline-Exportoption wurde ausgew�hlt /p3 \n";

CATIgesInit.Info.optionStandard="   <I> [6101] Die Standardexportoption wurde ausgew�hlt /p3 \n";

CATIgesInit.Info.optionNoShowRef="   <I> [6102] Es wurde gew�hlt, die Einheiten im Modus Verdecken nicht zu exportieren /p3 \n";

CATIgesInit.Info.optionNoShowAcc="   <I> [6103] Es wurde gew�hlt, die Einheiten im Modus Verdecken zu exportieren /p3 \n";

CATIgesInit.Info.optionWireframe="   <I> [6104] Es wurde die Exportoption f�r Drahtmodelle ausgew�hlt /p3 \n";

CATIgesInit.Info.optionSurfacic="   <I> [6105] Es wurde die Exportoption f�r Fl�chen ausgew�hlt /p3 \n";

CATIgesInit.Info.optionMSBO="   <I> [6106] Die Option MSBO Export wurde ausgew�hlt (f�r Volumenk�rper, Schalenelemente und Teilfl�chen) /p3 \n";


//-----------------------------------------------
//  Messages from CATIgesVirtualBody (messages utilises parfois dans CATIgesCldBody)
//-----------------------------------------------

CATIgesVirtualBody.Info.ProcessingBody="\n***K�RPER WIRD VERARBEITET ";

CATIgesVirtualBody.Info.ProcessingElement="\n **ELEMENT WIRD VERARBEITET [T=/p2] [/p1]";

CATIgesVirtualBody.Info.Name="   <I> [6150] [T=/p2] [#/p1] Der Name der V5-Einheit lautet /p3.";

CATIgesVirtualBody.Info.NoName="   <I> [6151] [T=/p2] [#/p1] F�r dieses Element wurde kein Name angegeben.";

CATIgesVirtualBody.Info.NbElem="   <I> [6152] Der K�rper besteht aus /p2 /p1 /p3";

CATIgesVirtualBody.Info.Wire="   <I> [6153] [T=/p2] [#/p1] Das Drahtelement ist geschlossen";

CATIgesVirtualBody.Error.ElementNotExported="!! <E> [6154] [T=/p2] [#/p1] Dieses Element kann (wegen ung�ltiger Eingabedaten) nicht exportiert werden. /p3";


//-----------------------------------------------
//  Messages from CATIgesElement
//-----------------------------------------------

CATIgesElement.Info.NoShow="   <I> [6200] [T=/p2] [#/p1] Einheit im Modus Verdecken";

CATIgesElement.Info.Layer="   <I> [6201] [T=/p2] [#/p1] Der Layer ist /p3";

CATIgesElement.Info.Color="   <I> [6202] [T=/p2] [#/p1] Die Farbe der Einheit ist R=/p3 G=/p4 B=/p5";

CATIgesElement.Warning.IgsControlName="   <W> [6203] Der Einheitenname ist ung�ltig: /p1 Nicht zul�ssige Zeichen beim Schreibzugriff erkannt. Es wird ein Bereinigungsversuch durchgef�hrt.";

//-----------------------------------------------
//  Messages from CATIgesCurveOnParametricSurface
//-----------------------------------------------
CATIgesCurveOnParametricSurface.Warning.PCurveNotCreated="   <W> [6250] [T=/p2] [#/p1] Die IGES-Kurve wurde nicht erzeugt /p3";


//-----------------------------------------------
//  Messages from CATIgesTrimmedParametricSurface
//-----------------------------------------------
CATIgesTrimmedParametricSurface.Warning.PCurveNotExported="   <W> [6300] [T=/p2] [#/p1] Die IGES-Kurve wurde nicht exportiert /p3";

//-----------------------------------------------
//  Messages from CATSaveAsIGS
//-----------------------------------------------
CATSaveAsIGS.TranscriptChild.NoChild="   <W> [6350] Kein Kind gefunden! ";
CATSaveAsIGS.Error.ConvertV4ToV5="   <E> [6351] V4-Modell konnte nicht in CATPart konvertiert werden! ";
CATSaveAsIGS.Info.ConvertV4ToV5="   <I> [6352] Das V4-Modell mit dem Namen /p1 wurde konvertiert.";

//--------------------------------------
//  Messages from CATIgesInterface
//--------------------------------------

CATDXFileServices.Error.InvalidName="!! <E> [6859] Der Name der zu sichernden IGES-Datei ist nicht g�ltig (Fehler PathERR_1021):
 Der Dateiname enth�lt Nicht-ASCII-Zeichen oder ist leer.
 Den Dateinamen auf ung�ltige Zeichen pr�fen. Datei ggf. umbenennen und erneut sichern.";


//------------------------------------------
//  Messages for CATIReporter 
//------------------------------------------

PathERR_1021.Request="Der Name der zu sichernden IGES-Datei ist nicht g�ltig.";
PathERR_1021.Diagnostic="Der Dateiname enth�lt Nicht-ASCII-Zeichen oder ist leer.";
PathERR_1021.Advice="Den Dateinamen auf ung�ltige Zeichen pr�fen. Datei ggf. umbenennen und erneut sichern.";

