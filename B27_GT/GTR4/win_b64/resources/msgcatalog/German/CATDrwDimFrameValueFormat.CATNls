//=============================================================================
//                                     CNEXT - CXR1
//                          COPYRIGHT DASSAULT SYSTEMES 1997 
//-----------------------------------------------------------------------------
// FILENAME    :    CATDrwDimFrameValueFormat
// LOCATION    :    DraftingIntUI/CNext/resources/msgcatalog
// AUTHOR      :    jmt
// DATE        :    Nov. 03 1997
//------------------------------------------------------------------------------
// DESCRIPTION :    Resources File for NLS purpose related to Drafting WorkShop
//                  
//------------------------------------------------------------------------------
// COMMENTS    :
//------------------------------------------------------------------------------
//MODIFICATIONS     user  date      purpose
//   HISTORY        ----  ----      -------
//     01			fgx	  18.02.99	Ajout des unites d'angle MINUTE et SECOND
//     02			fgx	  08.03.99	Ajout du mode driving
//     03			fgx	  18.11.99	Dim type
//     04			LGK     25.06.02  Revision
//------------------------------------------------------------------------------

_titleDimType.Title="Bema�ungstyp ";

ONE_PROJECTED_DIM = "Projizierte Bema�ung";
SEV_PROJECTED_DIM = "Projizierte Bema�ungen";
ONE_TRUE_DIM = "Ma�st�blich dargestelltes Ma�";
SEV_TRUE_DIM = "Ma�st�blich dargestellte Ma�e";
UNDEFINED = "Nicht definiert";

_frameDimType._checkDriving.Title="�bergeordnet";
_frameDimType._checkDriving.Help="Setzt den abh�ngigen/�bergeordneten Bema�ungsmodus";
_frameDimType._checkDriving.LongHelp="�bergeordnet
Setzt den abh�ngigen/�bergeordneten Bema�ungsmodus.";

_frameDimType._editorDriving.ShortHelp="Wert";
_frameDimType._editorDriving.Help="Bearbeiten des Bema�ungswerts f�r Geometriesteuerung";
_frameDimType._editorDriving.LongHelp="Wert
Bearbeiten des Bema�ungswerts f�r Geometriesteuerung.";

FrameBase.FrameLayoutTitle.LabelLayoutTitle.Title="Wertausrichtung ";
FrameBase.FrameDualValueTitle.LabelDualValueTitle.Title="Zweitwert ";
FrameFormatTitle.LabelFormatTitle.Title="Format";
FrameFakeTitle.CheckButtonFakeTitle.Title="Unma�st�bliches Ma� ";

FrameBase.FrameLayout.LabelReference.Title="Referenz:";
FrameBase.FrameLayout.LabelOrientation.Title="Ausrichtung:";
FrameBase.FrameLayout.LabelAngle.Title="Winkel:  ";
FrameBase.FrameLayout.LabelInOutValue.Title="Position: ";
FrameBase.FrameLayout.LabelOffset.Title="Offset: ";
FrameFormat.LabelValue.Title="Hauptwert";
FrameFormat.LabelDualValue.Title="Zweitwert";

FrameFormat._labelUnitOrder.Title="Anzeige: ";
FrameFormat._labelDescription.Title="Beschreibung: ";
FrameFormat._frameMoreInfoMain._pushMoreInfoMain.ShortHelp="Beschreibung des Hauptwerts anzeigen";
FrameFormat._frameMoreInfoDual._pushMoreInfoDual.ShortHelp="Beschreibung des Zweitwerts anzeigen";

FrameFormat.LabelPrecision.Title="Genauigkeit: ";
FrameFormat.LabelFormat.Title="Format: ";
FrameFormat.LabelValueFrac.Title    ="   1 / ";
FrameFormat.LabelDualValueFrac.Title="   1 / ";
FrameFake.LabelFakeMain.Title="Hauptwert: ";
FrameFake.LabelFakeDual.Title="Zweitwert: ";
FrameFake.RadioButtonFakeNum.Title="Numerisch";
FrameFake.RadioButtonFakeAlpha.Title="Alphanumerisch";

FrameBase.FrameDualValue.CheckButtonDualValue.Title="Zweitwert anzeigen";

ComboReferenceLine0="Anzeigefl�che";
ComboReferenceLine1="Ansicht";
ComboReferenceLine2="Ma�linie";
ComboReferenceLine3="Ma�hilfslinie";

ComboInOutValueAUTO="Automat.";
ComboInOutValueIN="Innen";
ComboInOutValueOUT="Au�en";

ComboDualValueLine0="Darunter";
ComboDualValueLine1="Bruch";
ComboDualValueLine2="Nebeneinander";

ComboValueFormatLine0="Dezimal";
ComboValueFormatLine1="Bruch";

ComboDualValueFormatLine0="Dezimal";
ComboDualValueFormatLine1="Bruch";

ComboOrientationLine0="Parallel";
ComboOrientationLine1="Rechtwinklig";
ComboOrientationLine2="Fester Winkel";

ComboOrientationLine00="Horizontal";
ComboOrientationLine01="Vertikal";
ComboOrientationLine02="Fester Winkel";

ONE_FACTOR="1 Faktor";
TWO_FACTORS="2 Faktoren";
THREE_FACTORS="3 Faktoren";

DefaultFakeText="*Unma�st�blich*";

FrameBase.FrameLayout.ComboReference.LongHelp=
"Stellt eine Referenz f�r die
Wertpositionierung zur Verf�gung.";
FrameBase.FrameLayout.ComboOrientation.LongHelp=
"Definiert die Ausrichtung
des Werts relativ zu seiner Referenz.";
FrameBase.FrameLayout.SpinnerAngle.LongHelp=
"Definiert den Winkel
des Werts relativ zu seiner Referenz.";
FrameBase.FrameLayout.SpinnerOffset.LongHelp=
"Definiert den vertikalen Offset
des Werts der Ma�linie.";
FrameBase.FrameDualValue.CheckButtonDualValue.LongHelp=
"Zweitwert anzeigen
Zeigt den Zweitwert an, sofern markiert.";
FrameBase.FrameDualValue.ComboDualValue.LongHelp=
"W�hlt den Anzeigemodus des Zweitwerts aus.";

FrameFormat._labelUnitOrder.LongHelp=
"Definiert die anzuzeigenden Einheitenfaktoren des Haupt- und des Zweitwerts.";
FrameFormat._labelDescription.LongHelp=
"Definiert die Beschreibung der numerischen Anzeige f�r Haupt- und Zweitwert.";

FrameFormat.LabelPrecision.LongHelp=
"Definiert die Genauigkeit des Haupt- und des Zweitwerts.";
FrameFormat.LabelFormat.LongHelp=
"Definiert das Format des Haupt- und des Zweitwerts.";

FrameFormat._comboUnitOrder.LongHelp=
"Definiert die Anzahl der f�r den Hauptwert anzuzeigenden Einheitenfaktoren.";
FrameFormat._comboDescriptionMain.LongHelp=
"Definiert die Beschreibung der numerischen Anzeige f�r den Hauptwert.";
FrameFormat._frameMoreInfoMain._pushMoreInfoMain.LongHelp=
"Zeigt ein Dialogfenster zum Anzeigen oder �ndern der Beschreibung der numerischen Anzeige f�r den Hauptwert an.";
FrameFormat.ComboValuePrecision.LongHelp=
"Definiert die Genauigkeit des Hauptwerts.";
FrameFormat.ComboValueFormat.LongHelp=
"Definiert das Format des Hauptwerts.";

FrameFormat._comboDualUnitOrder.LongHelp=
"Definiert die Anzahl der f�r den Zweitwert anzuzeigenden Einheitenfaktoren.";
FrameFormat._comboDescriptionDual.LongHelp=
"Definiert die Beschreibung der numerischen Anzeige f�r den Zweitwert.";
FrameFormat._frameMoreInfoDual._pushMoreInfoDual.LongHelp=
"Zeigt ein Dialogfenster zum Anzeigen oder �ndern der Beschreibung der numerischen Anzeige f�r den Zweitwert an.";
FrameFormat.ComboDualValuePrecision.LongHelp=
"Definiert die Genauigkeit des Zweitwerts.";
FrameFormat.ComboDualValueFormat.LongHelp=
"Definiert das Format des Zweitwerts.";

FrameFakeTitle.CheckButtonFakeTitle.LongHelp=
"Die Bema�ung in eine unma�st�bliche Bema�ung �ndern.";

FrameFake.EditorFakeNumMain.LongHelp=
"Zeigt den numerischen unma�st�blichen Hauptwert an.";
FrameFake.EditorFakeAlphaMain.LongHelp=
"Zeigt den alphanumerischen unma�st�blichen Hauptwert an.";
FrameFake.EditorFakeNumDual.LongHelp=
"Zeigt den numerischen unma�st�blichen Zweitwert an.";
FrameFake.EditorFakeAlphaDual.LongHelp=
"Zeigt den alphanumerischen unma�st�blichen Zweitwert an.";

SeeChamfer="Siehe Fase";

