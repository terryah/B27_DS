ShePart="Blechparameter";

Wall="Wand";
WallOnEdge="Wand an Kante";
Bend="Biegung";
ConicBend="Konische Biegung";
EvolutiveExtrude="Extrusion";
Recognize="Wanderkennung";
BendCreator="Automatische Biegungen";
BendingWizard="Assistent f�r Biegungen";

BendExtremityType="Extremwerte f�r die Biegung";
BendAngle="Biegungswinkel";

Stamp="Stempel";
PointStamp="Punktstempel";
ExtrudedHole="Extrudiertes Loch";
CurveStamp="Kurvenstempel";
SurfaceStamp="Fl�chenstempel";
Bridge="Br�cke";
Louver="Luftklappe";
StiffeningRib="Versteifende Rippe";
UserStamp="Benutzerstempel";

NumFlange="Flansch";
Hem="Umschlag";
TearDrop="Tropfen";
UsrFlange="Benutzerdefinierter Flansch";
Flange="Flansch";

ShePocket="Ausschnitt";
SheChamfer="Fase";
SheEdgeFillet="Ecken";

SheRectPatt="Rechteckmuster";
SheCircPatt="Kreismuster";
SheUserPatt="Benutzermuster";

CornerRelief="Eckenfreistellung";
ManualCornerRelief="Manuelle Eckenfreistellung";
CircularCornerRelief="Kreisf�rmige Eckenfreistellung";
RectangularCornerRelief="Rechteckige Eckenfreistellung";
TriangularCornerRelief="Dreieckige Eckenfreistellung";

RadialNumber="Kreisnummer";
AngularNumber="Winkelnummer";
RadialStep="Kreisabstand";
AngularStep="Winkelabstand";
RadialPosition="Zeile in Radialricht.";
AngularPosition="Zeile in Winkelricht.";
Nb1="Anzahl in Richt1";
Nb2="Anzahl in Richt2";
Step1="Abstand1";
Step2="Abstand2";
Nu="Zeile in Richt1";
Nv="Zeile in Richt2";
AngleXY="Rotationswinkel";
i="i";
j="j";

ExtrudeLength1="Extrusionsl�nge 1";
ExtrudeLength2="Extrusionsl�nge 2";
Activity="Aktivit�t";
Thickness="Aufma�";
DefaultBendRadius="Standardradius der Biegung";
MinimumBendRadius="Kleinster Biegungsradius";
BendRadius="Biegungsradius";
CornerReliefRadiusFormula="Formel f�r die Initialisierung des Radius von Eckenfreistellungen";
KFactor="K-Faktor";
DINNormaFormula="Formel f�r DIN-Standard";
ReliefRadialLength="Radiale L�nge der Freistellung";
ReliefAxialLength="Axiale L�nge der Freistellung";
BendRadiusFormula="Formel f�r Biegeradius";
CornerReliefRuleBase="Regelbasis";
CornerReliefRuleSet="Regelsatz f�r Eckenfreistellung";
CornerReliefRule="Regel f�r Eckenfreistellung";

CurveMapping="Kurvenzuordnung";
FoldCurve="Kurve falten";
UnfoldCurve="Kurve abwickeln";

FlatBend="Biegung aus Linie";

WarningMinimumBendRadius="Der Biegungsradius ist kleiner als der kleinste Biegungsradius, der in den Sheet Metal-Parametern definiert ist";
WarningBendRadMin1=" ist der maximal garantierte Wert f�r den Biegungsradius";
WarningBendRadMin2=" wenn ein Ausschnitt auf der Biegung positioniert werden muss";

WarningMinimumFlangeRadius="Der Flanschradius ist kleiner als der kleinste Biegungsradius, der in den Sheet Metal-Parametern definiert ist";
WarningFlangeRadMin1=" ist der maximal garantierte Wert f�r den Radius des Flansches";
WarningFlangeRadMin2=" wenn ein Ausschnitt auf dem Flansch positioniert werden muss";


WarningIfFilletWrong="Der Radiuswert der Verrundung zwischen dem Stempel und seinem St�tzelement stimmt nicht mit dem angegebenen Wert �berein. Der Stempel schneidet m�glicherweise einen anderen Stempel und sollte deshalb verschoben werden.";

WarningIfFilletFailed="Der Prozess der Verrundung zwischen dem Stempel und seinem St�tzelement ist fehlgeschlagen. Der Stempel schneidet m�glicherweise einen anderen Stempel und sollte deshalb verschoben werden.";

WarningIfCornerReliefNotPossible="Bei einem Feld des Teils, das nicht abgewickelt werden kann, ist keine Erstellung der Eckenfreistellung m�glich.";

ChamferAlias="Fase";

MiniNoRelief="Minimal - Keine Freistellung";
MiniSquareRelief="Minimum - Quadratische Kontur";
MiniRoundRelief="Minimum - Runde Kontur";
LimitedLinear="Begrenzt - Linear";
LimitedCurved="Begrenzt - Kurve";
Maxi="Maximal";
LimitedFace="Begrenzt - Teilfl�che";

MiniNoRelief_MiniNoRelief="Minimum - Keine Kontur und Minimum - Keine Kontur";
MiniNoRelief_MiniSquareRelief="Minium - Keine Kontur und Minimum - Quadratische Kontur";
MiniNoRelief_MiniRoundRelief="Minimum - Keine Kontur und Minimum - Runde Kontur";
MiniNoRelief_LimitedLinear="Minimum - Keine Kontur und Begrenzt - Linear";
MiniNoRelief_LimitedCurved="Minimum - Keine Kontur und Minimum - Kurve";
MiniNoRelief_Maxi="Minimum - Keine Kontur - und Maximum";
MiniNoRelief_LimitedFace="Minimum - Keine Kontur und Begrenzt - Fl�che";

MiniSquareRelief_MiniSquareRelief="Minimum - Quadratische Kontur und Minimum - Quadratische Kontur";
MiniSquareRelief_MiniRoundRelief="Minimum - Quadratische Kontur und Minimum - Runde Kontur";
MiniSquareRelief_LimitedLinear="Minimum - Quadratische Kontur und Begrenzt - Linear";
MiniSquareRelief_LimitedCurved="Minimum - Quadratische Kontur und Begrenzt - Kurve";
MiniSquareRelief_Maxi="Minimum - Quadratische Kontur und Maximum";
MiniSquareRelief_LimitedFace="Minimum - Quadratische Kontur und Begrenzt - Fl�che";

MiniRoundRelief_MiniRoundRelief="Minimum - Runde Kontur und Minimum - Runde Kontur";
MiniRoundRelief_LimitedLinear="Minimum - Runde Kontur und Begrenzt - Linear";
MiniRoundRelief_LimitedCurved="Minimum - Runde Kontur und Begrenzt - Kurve";
MiniRoundRelief_Maxi="Minimum - Runde Kontur und Maximum";
MiniRoundRelief_LimitedFace="Minimum - Runde Kontur und Begrenzt - Fl�che";

LimitedLinear_LimitedLinear="Begrenzt - Linear und Begrenzt - Linear";
LimitedLinear_LimitedCurved="Begrenzt - Linear und Begrenzt - Kurve";
LimitedLinear_Maxi="Begrenzt - Linear und Maximum";
LimitedLinear_LimitedFace="Begrenzt - Linear und Begrenzt - Fl�che";

LimitedCurved_LimitedCurved="Begrenzt - Kurve und Begrenzt - Kurve";
LimitedCurved_Maxi="Begrenzt - Kurve und Maximum";
LimitedCurved_LimitedFace="Begrenzt - Kurve und Begrenzt - Fl�che";

Maxi_Maxi="Maximum und Maximum";
Maxi_LimitedFace="Maximum und Begrenzt - Fl�che";

LimitedFace_LimitedFace="Begrenzt - Fl�che und Begrenzt - Fl�che";

ConicBendRadius="Konische Biegung - Radius1";
ConicBendRadius2="Konische Biegung - Radius2";
ConicBendAngle="Konische Biegung - Winkel";
ConicBendExtremity="Konische Biegung - Extremwerte";

FlatBendAngle="Flache Biegung - Winkel";
FlatBendRadius="Flache Biegung - Radius";
StampRadiusConnect="Stempelradius - Verbindung";

Angle="Winkel";
Radius="Radius";
Clearance="Sicherheitsbereich";
BuildWithBend="Mit Biegung erstellt";

Pattern="Muster";

FPToFDMapping="Falten";
FDToFPMapping="Abwickeln";

// FDD - 22.10.01 - Pour le nouveau parametre Activity V5R8
NewActivity="Aktivit�t";

ShfSplit="Trennen";
