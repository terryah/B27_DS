CATPublishCmd.SelectElement.Message="Das zu veröffentlichende Element auswählen";
CATPublishCmd.UndoTitle="Veröffentlichung";

Publication="Veröffentlichung";
ComponentPublication="Veröffentlichung von Komponenten";
Options="Optionen";
Name="Name";
Status="Status";
Element="Element";


PublishFrame.Buttons.Remove.Title="Entfernen";
PublishFrame.Buttons.Remove.ShortHelp="Entfernt ein veröffentlichtes Element aus der Liste";
PublishFrame.Buttons.Remove.LongHelp="Entfernt ein veröffentlichtes Element aus der Liste";

PublishFrame.Buttons.Browse.Title="Durchsuchen...";
PublishFrame.Buttons.Browse.ShortHelp="Wählt ein durch eine Komponente veröffentlichtes Element aus";
PublishFrame.Buttons.Browse.LongHelp="Wählt ein durch eine Komponente veröffentlichtes Element aus";

PublishFrame.Buttons.Options.Title="Optionen...";
PublishFrame.Buttons.Options.ShortHelp="Veröffentlichungsoptionen";
PublishFrame.Buttons.Options.LongHelp="Veröffentlichungsoptionen";

PublishFrame.Buttons.Import.Title="Importieren...";
PublishFrame.Buttons.Import.ShortHelp="Importiert die Veröffentlichungsnamen aus einer Ascii-Datei (.txt)";
PublishFrame.Buttons.Import.LongHelp="Importiert die Veröffentlichungsnamen aus einer Ascii-Datei (.txt)";

PublishFrame.Buttons.Export.Title="Exportieren...";
PublishFrame.Buttons.Export.ShortHelp="Exportiert die Veröffentlichungsnamen in eine Ascii-Datei (.txt)";
PublishFrame.Buttons.Export.LongHelp="Exportiert die Veröffentlichungsnamen in eine Ascii-Datei (.txt)";

PublishFrame.Buttons.Parameter.Title="Parameter...";
PublishFrame.Buttons.Parameter.ShortHelp="Veröffentlicht die Parameter eines Objekts";
PublishFrame.Buttons.Parameter.LongHelp="Veröffentlicht die Parameter eines Objekts";

ImportFile.Title="Importieren";
ImportFile.FilterType.1="Textdatei (*.txt)";

ExportFile.Title="Export";
ExportFile.FilterType.1="Textdatei (*.txt)";


OptionsFrame.RenameFrame.RenameLabel.Title="Das Element umbenennen: ";
OptionsFrame.PublishBodyFeatureFrame.BodyFeatureSelection.Title="Aktivieren, um die Komponenten eines Körpers zu veröffentlichen";

RenameNever="Nie";
RenameAlways="Immer";
RenameAsk="Nachfragen";

AskFeatRnmMsg="Soll Element '/P1' in '/P2' umbenannt werden?";
AskFeatRnmTtl="Element umbenennen";

PubOKTag="OK";
PubNoFinalEltTag="Kein Element";
PubNamePathBrokenTag="Keine Komponente";
PubProtectedEltTag="Geschützt";


PubUnknownNameTag="Unbekannte Komponente: /p";
PublicationNameTag="/p1: /p2";

PublishRenameReplaceTag="Zum Veröffentlichen ein Element auswählen. Zum Umbenennen oder Ersetzen eine Zeile in der Liste auswählen.";
RenameReplaceTag="Zum Umbenennen die Namenszelle auswählen. Zum Ersetzen des Elements das neue Element auswählen.";
RenameTag="Umbenennen: die Namenszelle auswählen und den neuen Namen eingeben.";

Messages.PublishCSOMessage="Sollen die ausgewählten Elemente veröffentlicht werden?";

Messages.PublishAlready="Element /p ist bereits veröffentlicht";

Messages.PublishInvalidMsg="Element /p kann nicht veröffentlicht werden.
Folgendes überprüfen:
- Gültigkeit des zu veröffentlichenden Objekts
- Elementtyp";


Messages.PublishVisuModeMsg="Element des Darstellungstyps 'rep' kann nicht veröffentlicht werden,
Überprüfen Sie Folgendes:
- Darstellungsmodus des Teils, das das Objekt enthält.";

Messages.PublishErrorMessage="Bei dem Versuch, das Element zu veröffentlichen, ist ein Fehler aufgetreten. Das Element kann nicht veröffentlicht werden. Den Namen '/p' des Objekts prüfen. Der Name ist für eine Veröffentlichung ungültig, wenn Folgendes zutrifft: - Er besteht aus einer leeren Zeichenfolge - oder enthält eines der Zeichen  '!', ':', '<' oder '>'
- oder hat am Anfang oder Ende der Zeichenfolge ein Leerzeichen.";



Messages.PublishDeleteNoAllowed="Geschützte elektrische Veröffentlichung. Entfernen oder Ersetzen ist in diesem Kontext nicht zulässig. Veröffentlichungsobjekt in der Electrical-Produktlinie '/p' prüfen und
Lebenszyklus des Objekts in Electrical-Umgebungen verwalten";


ReplaceElementTitle="Element ersetzen";
Messages.ReplaceElementMessage="Soll das veröffentlichte Element durch '/P1' ersetzt werden?";
Messages.ReplaceGeometricNotAllowedMessage="Ein geometrisches Element von '/P1' darf nicht durch einen Parameter ersetzt werden.";
Messages.ReplaceParameterNotAllowedMessage="Ein Parameter von '/P1' darf nicht durch ein geometrisches Element ersetzt werden.";

//Useless
//Messages.DatabaseSaveNotify=" will not be saved in database structure document.\nThis restriction concerns any publication of publication.";

IsValidToPublishTag="Das Element '/p' kann veröffentlicht werden.";
PublishedByComponentTag="Das Element '/p1' wurde bereits mit der Komponente '/p2' veröffentlicht.";
CannotBePublishedTag="Das Element '/p' kann nicht veröffentlicht werden.";
IsPublishedTag="Das Element '/p1' wurde unter dem Namen '/p2' veröffentlicht.";
IsRemovedTag="Das Element /p wird entfernt."; //Do not use '/p' since it is already done in the code.
AreRemovedTag="Die Elemente /p werden entfernt."; //Do not use '/p' since it is already done in the code.
ReplaceCanceled="Das Ersetzen der Elemente /p wird abgebrochen.";

BrowsePublishTag="Veröffentlichen: die Veröffentlichung einer Komponente auswählen";
BrowseReplaceTag="Das Element ersetzen: die Veröffentlichung einer Komponente auswählen";

RenameError="Dieser Name ist bereits vorhanden.\nEin anderer Name muss eingegeben werden.";
RenameProtected="Das Umbenennen von '/p' ist nicht zulässig. Es handelt sich um eine 'geschützte' Veröffentlichung";

RenameErrorCharacter="Bei dem Versuch, das Element umzubenennen, ist ein Fehler aufgetreten:
Der Name '/p' ist nicht gültig:
- Er besteht aus einer leeren Zeichenfolge
- oder enthält eines der Zeichen  '!', ':', '<' oder '>
- oder hat am Anfang oder Ende der Zeichenfolge ein Leerzeichen.";

RenameForbidden="Ein Umbenennen der Veröffentlichung ist nicht zulässig. In einem externen Link wird auf die Veröffentlichung verwiesen. Daher kann sie nicht umbenannt werden.";

Messages.PublishRenameInVpmSession="Warnung: Das Umbenennen einer Veröffentlichung kann sich auf Dokumente auswirken, auf die verwiesen wird und die nicht in der Sitzung geladen sind.
Mit dem Tool für die Suche, wo eine Veröffentlichung verwendet wird, können die betroffenen Dokumente überprüft werden (sofern eine Verbindung zur Datenbank besteht).";

Messages.PublishRenameInContext="Warnung: Das Umbenennen einer Veröffentlichung kann sich auf Dokumente auswirken, auf die verwiesen wird und die nicht im Kontext geladen sind.";

Messages.PublishRenameElementParameterExists="Warnung: Der Parameter kann nicht umbenannt werden. Es gibt bereits einen Parameter mit diesem Namen.";
