//=====================================================================================
//                                     CNEXT - CXR1
//                          COPYRIGHT DASSAULT SYSTEMES 1999 
//-------------------------------------------------------------------------------------
// FILENAME    :    CATDrwOptGen
// LOCATION    :    DraftingUI
// AUTHOR      :    fgx
// BUT         :    
// DATE        :    06.01.1999
//-------------------------------------------------------------------------------------
// DESCRIPTION :    Tools/Option du Drafting - Onglet Generative
//-------------------------------------------------------------------------------------
// COMMENTS    :    
//-------------------------------------------------------------------------------------
// MODIFICATIONS     user  date        purpose
//    HISTORY        ----  ----        -------
//      00           fgx   06.01.1999  Creation 	
//      01           fgx   08.01.1999  Passage sur CATDrwOptTitle et CATDrwOptIcon
//      02           fgx   13.01.1999  Nouvelles options gendim
//      03           fgx   21.01.1999  Nouvelles options elements generes
//      04           fgx   28.01.1999  Ajout de GenDimFilter et GenDimAnal
//      05           fgx   05.02.1999  Ajout de GenDimAutoTrans
//      06           fgx   14.04.1999  Modif de GenGeo
//	  08		   lgk   26.06.2002  Revision
//	  09 	    	   lgk   15.10.02    Revision
//=====================================================================================

Title="Generation";

frameGenDim.HeaderFrame.Global.Title="Dimension generation ";
frameGenDim.HeaderFrame.Global.LongHelp=
"Dimension generation
Defines whether you will generate dimensions from the
constraints of a 3D part.
The following constraints may be generated: distance, 
length, angle, radius and diameter. ";
frameGenDim.IconAndOptionsFrame.OptionsFrame.checkGenDim.Title="Generate dimensions when updating the sheet ";

frameGenDim.IconAndOptionsFrame.OptionsFrame.checkGenDimFilter.Title="Filters before generation";
frameGenDim.IconAndOptionsFrame.OptionsFrame.checkGenDimFilter.LongHelp=
"Filters before generation
Displays the \"Dimension generation filters\" 
dialog box before generation.";

frameGenDim.IconAndOptionsFrame.OptionsFrame.checkGenDimAutoPos.Title="Automatic positioning after generation";
frameGenDim.IconAndOptionsFrame.OptionsFrame.checkGenDimAutoPos.LongHelp=
"Automatic positioning after generation
Automatically positions dimensions after generation.";

frameGenDim.IconAndOptionsFrame.OptionsFrame.checkGenDimAutoTrans.Title="Allow automatic transfer between views";
frameGenDim.IconAndOptionsFrame.OptionsFrame.checkGenDimAutoTrans.LongHelp=
"Allow automatic transfer between views
Automatically transfers dimensions to the best view when regenerating.";

frameGenDim.IconAndOptionsFrame.OptionsFrame.checkGenDimAnal.Title="Analysis after generation";
frameGenDim.IconAndOptionsFrame.OptionsFrame.checkGenDimAnal.LongHelp=
"Analysis after generation
Displays the \"Dimension generation analysis\"
dialog box after generation.";

frameGenDim.IconAndOptionsFrame.OptionsFrame.checkGenDimFromPart.Title="Generate dimensions from parts included in assembly views";
frameGenDim.IconAndOptionsFrame.OptionsFrame.checkGenDimFromPart.LongHelp=
"Generate dimensions from parts included in assembly views
Generates dimensions from parts included in assembly views.";

frameGenDim.IconAndOptionsFrame.OptionsFrame.labelGenDimSemiAuto.Title="Delay between generations for step-by-step mode: ";
frameGenDim.IconAndOptionsFrame.OptionsFrame.labelGenDimSemiAuto.LongHelp=
"Delay between generations for step-by-step mode
Specifies the delay between each dimension generation 
when generating dimensions step-by-step.";



frameGenBal.HeaderFrame.Global.Title="Balloon generation";
frameGenBal.HeaderFrame.Global.LongHelp=
"Defines the options used when generating balloons.";
frameGenBal.IconAndOptionsFrame.OptionsFrame.checkGenBalloonByInstance.Title="Creation of a balloon for each instance of a product";
frameGenBal.IconAndOptionsFrame.OptionsFrame.checkGenBalloonByInstance.LongHelp="Creation of a balloon for each instance of a product
Creates a balloon for each instance of a product.";
