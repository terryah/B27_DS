MultiPadTitle = "Multi-Pad Definition";
MultiPocketTitle = "Multi-Pocket Definition";

LongHelp= "Creates or modifies multi-pads or multi-pockets.";

PadLabDim = "Length: ";
PocketLabDim = "Depth: ";

DomLst0 = "Nr";
DomLst1 = "Domain";
DomLst2 = "Thickness";

FrmRoot.FrmMain.FraEnd.Title = "First Limit";
FrmRoot.FrmMain.FraEnd.LabEndType.Title = "Type: ";
CmbEndLab0 = "Dimension";
CmbEndLab1 = "Up to next";
CmbEndLab2 = "Up to last";
CmbEndLab3 = "Up to plane";
CmbEndLab4 = "Up to surface";
FrmRoot.FrmMain.FraEnd.ParamFrame.Spinner.Title = "Dim1"; 
FrmRoot.FrmMain.FraEnd.ParamFrame.EnglobingFrame.IntermediateFrame.Spinner.Title = "Dim1"; 
FrmRoot.FrmMain.FraEnd.LabEndLim.Title = "Limit: ";
FrmRoot.FrmMain.FraEnd.FraEnd4.Spinner.Title = "Off1"; 
FrmRoot.FrmMain.FraEnd.FraEnd4.EnglobingFrame.IntermediateFrame.Spinner.Title = "Off1"; 
FrmRoot.FrmMain.FraEnd.LabEndOffset.Title = "Offset: ";
FrmRoot.FrmMain.FraSym.ChkSym.Title = "Mirrored extent";
FrmRoot.FrmMain.DomainsFrame.Title = "Domains";

FrmMore.FraStart.Title = "Second Limit";
FrmMore.FraStart.LabStartType.Title = "Type: ";
CmbStartLab0 = "Dimension";
CmbStartLab1 = "Up to next";
CmbStartLab2 = "Up to last";
CmbStartLab3 = "Up to plane";
CmbStartLab4 = "Up to surface";
FrmMore.FraStart.ParamFrame.Spinner.Title = "Dim2";
FrmMore.FraStart.ParamFrame.EnglobingFrame.IntermediateFrame.Spinner.Title = "Dim2";
FrmMore.FraStart.LabStartLim.Title = "Limit: ";
FrmMore.FraStart.FraStart4.Spinner.Title = "Off2";
FrmMore.FraStart.FraStart4.EnglobingFrame.IntermediateFrame.Spinner.Title = "Off2";
FrmMore.FraStart.LabStartOffset.Title = "Offset: ";

FrmMore.FraDir.Title = "Direction";
FrmMore.FraDir.FraDir1.ChkNormal.Title = "Normal to sketch";
FrmMore.FraDir.FraDir2.Lab.Title = "Reference:"; 
FrmMore.FraRev.PshRev.Title = " Reverse Direction ";

FrmMore.FraVis.Title = "Visualization";
FrmMore.FraVis.FraHan.ChkHan.Title = "Hide handles";
FrmMore.FraVis.FraCot.ChkCot.Title = "Hide dimensions";

FrmRoot.FrmMain.FraEnd.LongHelp = 
"First Limit 
Specifies the limit defined 
in the direction of extrusion.";

FrmRoot.FrmMain.FraEnd.LabEndType.LongHelp = 
"Type of relimitation 
for the first limit.";
FrmRoot.FrmMain.FraEnd.CmbEnd.LongHelp = 
"Type of relimitation 
for the first limit.";

FrmRoot.FrmMain.FraEnd.LabEndValue.LongHelp = 
"Specifies the dimension 
for the first limit.";
FrmRoot.FrmMain.FraEnd.ParamFrame.LongHelp = 
"Specifies the dimension 
for the first limit.";
FrmRoot.FrmMain.FraEnd.ParamFrame.EnglobingFrame.IntermediateFrame.LongHelp = 
"Specifies the dimension 
for the first limit.";

FrmRoot.FrmMain.FraEnd.LabEndLim.LongHelp = 
"Identifies the limiting element 
for the first limit";
FrmRoot.FrmMain.FraEnd.FraEnd3.LongHelp = 
"Identifies the limiting element 
for the first limit";

FrmRoot.FrmMain.FraEnd.LabEndOffset.LongHelp = 
"Specifies the offset value 
for the first limit.";
FrmRoot.FrmMain.FraEnd.FraEnd4.LongHelp = 
"Specifies the offset value 
for the first limit.";

FrmRoot.FrmMain.FraSym.ChkSym.LongHelp = "Mirrors the extrusion offset.";
FrmRoot.FrmMain.FraRev.PshRev.LongHelp = "Reverses the direction of extrusion.";

FrmMore.FraStart.LongHelp = 
"Second Limit
Specifies the limit opposite 
to the direction of extrusion.";

FrmMore.FraStart.LabStartType.LongHelp = 
"Type of relimitation 
for the second limit.";
FrmMore.FraStart.CmbStart.LongHelp = 
"Type of relimitation 
for the second limit.";

FrmMore.FraStart.LabStartValue.LongHelp = 
"Specifies the dimension 
for the second limit.";
FrmMore.FraStart.ParamFrame.LongHelp = 
"Specifies the dimension 
for the second limit.";
FrmMore.FraStart.ParamFrame.EnglobingFrame.IntermediateFrame.LongHelp = 
"Specifies the dimension 
for the second limit.";

FrmMore.FraStart.LabStartLim.LongHelp = 
"Identifies the limiting element 
for the second limit";
FrmMore.FraStart.FraStart3.LongHelp = 
"Identifies the limiting element 
for the second limit";

FrmMore.FraStart.LabStartOffset.LongHelp = 
"Specifies the offset value 
for the second limit.";
FrmMore.FraStart.FraStart4.LongHelp = 
"Specifies the offset value 
for the second limit.";

FrmMore.FraDir.LongHelp = 
"Direction 
Specifies the direction of extrusion.";

FrmMore.FraDir.FraDir1.ChkNormal.LongHelp = 
"Makes the direction of extrusion
 normal to the sketch.";
FrmMore.FraDir.FraDir2.Lab.LongHelp = 
"Specifies the element used  
to define the direction.";
FrmMore.FraDir.FraDir2.Edt.LongHelp = 
"Specifies the element used  
to define the direction.";

_pUIRepDir.FieldName="Profile";
