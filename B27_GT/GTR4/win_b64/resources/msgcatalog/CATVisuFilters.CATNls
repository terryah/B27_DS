Title="Visualization Filters";

AllVisibleFilter.Title="All visible";
DefaultPrefix="Filter";

mainFrame.cmdFrame.newFilter.Title="New";
mainFrame.cmdFrame.deleteFilter.Title="Delete";
mainFrame.cmdFrame.editFilter.Title="Edit";

mainFrame.cmdFrame.newFilter.ShortHelp="Filter creation";
mainFrame.cmdFrame.deleteFilter.ShortHelp="Filter(s) Deletion";
mainFrame.cmdFrame.editFilter.ShortHelp="Filter Edition";

mainFrame.cmdFrame.newFilter.LongHelp=
"New Filter.
Displays a dialog to create a new filter.
A default name is given to the filter
that can be changed in place in the list.";

mainFrame.cmdFrame.deleteFilter.LongHelp=
"Delete Filter(s).
This button will erase all the selected filters.";

mainFrame.cmdFrame.editFilter.LongHelp=
"Edit Filter.
Displays a dialog describing the selected filter.
This allows to modify all characteristics but the name.
Double-click in the list also edits the filter.";

FiltersList.ShortHelp="List of filters";
FiltersList.Document.ShortHelp = "Definition located in the document";
FiltersList.Settings.ShortHelp = "Definition located in the CATSettings repository";

Column.Title="Filter name";

mainFrame.listFrame.FiltersList.LongHelp= 
"List of Visualization filters.
Rename a filter in place by selecting it twice.
Sort the list by selecting the column title.
Create, delete or edit filters by selecting
the buttons on the right.
Double-click on a filter also edits it.
'All visible' is a default filter that sets
all the layers visible.
This default filter and the current filter
can not be deleted or modified.
The filter which is selected when this dialog
appears is the current filter.";

BOK="OK";
BCancel="Close";
BApply="Apply";

Filter.NoRemove = "Some filters can not be deleted :
 - the current filter;
 - the default filter (All Visible);
 - the filters protected by your administrator.";
