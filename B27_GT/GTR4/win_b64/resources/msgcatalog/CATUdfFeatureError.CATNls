//---------------------------------------------------------------
// Resource file for CATUdfFeatureError class
// Us
//---------------------------------------------------------------

ErrorNoOutputs               = "- A default output cannot be computed.\n    Go to the 'Outputs' tab to define one.\n";
ErrorBRepKO                  = "- Some sub-element(s) of the selected feature cannot be updated during the instantiation\nTry to include the corresponding support to avoid this problem.\n";
WarningBRepKO                = "- Some sub-element(s) of the selected feature will need to be rerouted after the instantiation.\n";
ErrorBRep0                   = " from ";
ErrorBRep1                   = " needs ";
ErrorBRep2                   = " to be solved.";
WarningBRep1                 = " will be in error";
ErrorReplaceKO               = "- Some elements could not be replaced during instantiation\nInclude them in the Powercopy or in the UDF to avoid this problem.\n";
ErrorReplace1                = " does not implement replace for ";
ErrorInstantiate             = "Instantiation cannot be performed";
ErrorInstantiateSel          = "Instantiate error: Some selected features are not authorized.";
ErrorInstantiateCreate       = "Instantiate error: Some selected features can not be instantiated.";
ErrorInstantiateMovable      = "Instantiate error: Selected features can not be categorized.";
ErrorConsecutivity           = "Features to duplicate must be consecutive";
ErrorInsert                  = "Insert error: Some instantiated features can not be inerted at selected destination.";
ErrorCopy                    = "This element cannot be instantiated";
ErrorNeedToBeSaved           = "Save your current document first.";
ErrorNoPowerCopy             = "No powercopy, user feature, and document template in this document.";
ErrorNoDescObject            = "No associated object, use catalog editor to reconnect it.";
ErrorExternalLink            = "- An element containing a link to another document has been included\nRemove this element to avoid this problem.\n";
ErrorChapterNotAllowed       = "Components catalog chapter cannot be paste in a Product!";
ErrorNoDocumentToInstantiate = "No document to instantiate!\nUse catalog editor to check document links.";
ErrorRemove                  = " Needs to be removed ";
ErrorDatumKO                 = "Some included elements are datum elements.\n";
ErrorDatum                   = "Remove this element or replace it by a computed one.";
ErrorRelationKO              = "Some inputs are output of an included formula.\n";
ErrorRelation                = "Add this element.";
ErrorEmpty                   = "The user feature is empty.";
ErrorInvalidInput            = "Invalid userfeature input.";
ErrorNoGeometry              = "No associated geometry in userfeature.";
WarningMultiNaming           = "There are more than one component to be renamed ; we won't apply the user naming : /p1.";
WarningNaming                = "A component already exists with the name : /p1. Default name will be applied.";
WarningNamingPC              = "A component already exists with this name: /p1. An extension will be added.";
ErrorInternalImport          = " is an internal copy with link and its reference is not in the Power Copy";
ErrorExternalImport          = " is an external reference.";
ErrorImportLink              = "- An element with a link to an component external to the Power Copy has been included\nRemove or Isolate this element to avoid this problem.\n";
ErrorPartSelectionOrCreationImpossible = "It is impossible to select or create a part as UserFeature instance destination";
ErrorPartNumber              = "Two documents with same Part Number: instantiation aborted";
ErrorMaterial1               = "Power Copy does not support materials";
ErrorMaterial2               = "has a material";
ErrorCopyUdfNode             = "Copy or Cut is not allowed for /p1 node";
ErrorInsertionFailed         = "Instantiation error: the operation can not be performed: \nOrdering rules would be broken or the geometry could not be recomputed.\nThe command is canceled.";
LinearityError               = "The features selected for the Power Copy creation break the ordering rules.\n";
LinearityAdvise              = "You should assign a new order to the selected features before creating the Power Copy.";
AlreadyLoadedWarning         = "The reference document /p1
is already loaded and displayed in session.
If it has been modified since the creation of the Power Copy Instance,
its display in the right viewer of the comparison window might be inconsistent
with the display of the duplicated features in the left viewer.";
WarningLinksDevaluated       = "Some links pointing template inputs are unset to respect ordering rules.";
WarningMultiBranch			 = "The main Output will display only the first solid result";
ErrorMultiContainerLink      = "/p1 can't be a component of this template: it has an unsupported link to /p2.";

CleanerInputType0      = "UserFeature /p1 has a wrong declaration of its inputs (type 0).";
CleanerInputType1      = "UserFeature /p1 has a wrong declaration of its inputs (type 1).";
CleanerInputType2      = "UserFeature /p1 has a wrong declaration of its inputs (type 2).";
CleanerInputType2.Help = "\n /p1 input is corrupted.";
CleanerInputType3      = "UserFeature /p1 has a wrong declaration of its inputs (type 3).";
CleanerInputType4      = "UserFeature /p1 has a wrong declaration of its inputs (type 4).";
CleanerInputType5      = "UserFeature /p1 has a wrong declaration of its inputs (type 5).";
CleanerInputType5.Help = "\n An input is referenced twice.";
CleanerInputType6      = "UserFeature /p1 has a wrong declaration of its inputs (type 6).";
CleanerInputType6.Help = "\n /p1 input is useless.";
CleanerInputType7      = "UserFeature /p1 has a wrong declaration of its inputs (type 7).";
CleanerInputType7.Help = "\n /p1 input is useless due to intermediate geometry inside relation /p2 .";
CleanerInputType8      = "UserFeature /p1 has a wrong declaration of its inputs (type 8).";
CleanerInputType8.Help = "\n /p1 input was pointing to a deleted attribute of a geometrical intermediate feature inside relation /p2 .";
CleanerInputCleaned    = "UserFeature /p1 correctly cleaned.";
CleanerType            = "UserFeature /p1 has a wrong declaration of its type.";
CleanerInvalidParm     = "UserFeature published parameter /p1 is invalid.";
CleanerInvalidParm.Help= "It is pointing to an external document. It will be removed.";
CleanerParmCleaned     = "User Feature published parameter /p1 has been correctly cleaned.";

Warning = "Warning";
Information = "Information";
Error = "Error";
FatalError = "Error";

WarningSpecAlreadySelected = "This element has already been selected as input.\nSelect an other one.\n";

EditMappingResult		= "UserFeature has been edited and some previous information could not be retrieved.\n";
EditRetrieveInputRole		= "Can not retrieve Input(s) reference for role(s) :\n"; 
EditRetrieveOutputs		= "Can not retrieve Reference for Output(s) :\n"; 
EditAddOutputs			= "Can not add Output(s) :\n"; 
EditRetrieveOutputRole		= "Can not retrieve Output(s) for role(s) :\n"; 
EditRetrieveParameterRole	= "Can not retrieve published Parmeter(s) :\n"; 
EditWarning			= "Warning";
PCInsertionFailed      = "The destination /p1 is not valid with the position /p2 for the instantiation of this template reference.
The template contains the feature /p3 which can not be inserted at this position because of ordering rules.
The destination will be unset. Please select another one.";
PCInsertionWarning     = "The destination /p1 can be chosen as destination of insertion of this template reference .
However these following elements will be inserted directly under another location:";
UdfInsertionFailed      = "The destination /p1 is not valid with the position /p2 for the instantiation of the template reference /p3.
The feature /p3 can not be inserted at this position because of ordering rules.
The destination will be unset. Please select another one.";
PCInsertionImpossible   = "You cannot select /p1 as destination since it belongs to a Geometrical Set.
The destination will be unset. Please select another one.";

ErrorNoDuplicatedParam = "Error when duplicating the following parameter: /p1.";
ErrorOnParameters = "Internal error during the management of the parameters.";

WarningBRepColorAttKO = "Some colors or attributs on sub-elements of the selected features will be lost during the instantiation.\n";
ErrorNoGeometricFtr   = "No geometrical feature has been selected.\nPush the 'Change/Update Components' button to modify the definition of the UserFeature and add a geometrical feature or a set with a geometrical feature inside.";

OrderedInputsError = "Invalid ordered inputs list due to the modification of the components.
The inputs list is no longer ordered. You should reorder it.";

Inside = "Inside";
After = "After";

InvalidPowerCopyDeletion = "Power Copy /p1 has been deleted.";
InvalidPowerCopyDefinition = "Power Copy /p1 contains invalid components.";
InvalidPowerCopyDefinition.Help = "\nThe components belong to another document. It leads to potential Ghost Links.
The Power Copy will be deleted.";

SameInput = "You cannot select this feature as new input
because it is already pointed by another input.
Select another element or modify the value of the other input.";

ImpossibleDeleteUDF = "Impossible delete of Userfeature output\nDelete its father.";
ImpossibleCopyUDF = "Impossible copy of Userfeature reference.";

RemovedElement = "Removed element";

WarningUpgradeUDF = "Variable UDF_RECURSIVE_UPGRADE has been exported. Recursive upgrade of User Features internals is enabled. \n
This mode is not officially supported and may destabilize User Feature update. Please check result before saving. \n
This warning will be shown only once but remains relevant for future use.";
