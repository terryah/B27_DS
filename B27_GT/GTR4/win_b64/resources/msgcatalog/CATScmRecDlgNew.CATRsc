// COPYRIGHT DASSAULT SYSTEMES 2003-2010
// --- Resources for CATIA Reconciliator & Synchronization GUI

// ---> View Comparison mask for both Reconcile & Synchronize

GraphViewStatus.InstanceUnknown              = "I_ScmRecAssemblyUnknownStatusIcon"                ;
GraphViewStatus.InstanceNew                  = "I_ScmRecAssemblyNewInstanceStatusIcon"            ;
GraphViewStatus.InstanceIdentical            = "I_ScmRecAssemblyIdenticalStatusIcon"              ;
GraphViewStatus.InstanceMoved                = "I_ScmRecAssemblyInstanceMovedStatusIcon"          ;
GraphViewStatus.InstanceToBeDeleted          = "I_ScmRecAssemblySuppressInstanceStatusIcon"       ;
GraphViewStatus.InstanceToBeReplaced         = "I_ScmRecAssemblyInstanceReplacedStatusIcon"       ;
GraphViewStatus.InstanceToBeMovedReplaced    = "I_ScmRecAssemblyInstanceReplacedMovedStatusIcon"  ;
GraphViewStatus.InstanceToBeVersionned       = "I_ScmRecAssemblyInstanceVersionStatusIcon"        ;
GraphViewStatus.InstanceToBeMovedVersionned  = "I_ScmRecAssemblyInstanceVersionMovedStatusIcon"   ;
GraphViewStatus.InstanceToBeRevised          = "I_ScmRecAssemblyInstanceVersionStatusIcon"        ;
GraphViewStatus.InstanceToBeMovedRevised     = "I_ScmRecAssemblyInstanceVersionMovedStatusIcon"   ;

GraphViewStatus.InhibitedInstanceIdentical   = "I_ScmRecAssemblyInhibitedIdenticalStatusIcon"     ;
GraphViewStatus.MovedInhibited               = "I_ScmRecAssemblyInhibitedMovedStatusIcon"         ;
GraphViewStatus.NewInhibited                 = "I_ScmRecAssemblyNewInhibitedStatusIcon"           ;
GraphViewStatus.DeletedInhibited             = "I_ScmRecAssemblySuppressInhibitedStatusIcon"      ;
GraphViewStatus.InhibitedToBeReplaced        = "I_ScmRecAssemblyInhibitedReplacedStatusIcon"      ;
GraphViewStatus.InhibitedToBeMovedReplaced   = "I_ScmRecAssemblyInhibitedReplacedMovedStatusIcon" ;
GraphViewStatus.InhibitedToBeVersionned      = "I_ScmRecAssemblyInhibitedVersionStatusIcon"       ;
GraphViewStatus.InhibitedToBeMovedVersionned = "I_ScmRecAssemblyInhibitedVersionMovedStatusIcon"  ;
GraphViewStatus.InhibitedToBeRevised         = "I_ScmRecAssemblyInhibitedVersionStatusIcon"       ;
GraphViewStatus.InhibitedToBeMovedRevised    = "I_ScmRecAssemblyInhibitedVersionMovedStatusIcon"  ;

// ---> Views Document, Instance and Part & Document: Mask for Reconcile rules

GraphViewStatus.DocumentDelegate        = "I_ScmRecDelegateDocIcon"  ;
GraphViewStatus.DocumentReload          = "I_MaskReload"             ;
GraphViewStatus.DocumentOverwrite       = "I_MaskOverwrite"          ;
GraphViewStatus.DocumentNew             = "I_MaskSaveAsNew"          ;
GraphViewStatus.DocumentNewFrom         = "I_MaskSaveAsNewFrom"      ;
GraphViewStatus.DocumentKeepExt         = "I_MaskKeepExternalRef"    ;
GraphViewStatus.DocumentMapped          = "I_MaskVPDMObject"         ;
GraphViewStatus.DocumentUnknown         = "I_ScmRecIncompDocIcon"    ;
GraphViewStatus.DocumentBrokenLinkPrd   = "I_PRDBrokenGearsMask"     ;
GraphViewStatus.DocumentBrokenLinkGen   = "I_PRDBrokenMask"          ;
GraphViewStatus.PartDelegate            = "I_ScmRecDelegatePartIcon" ;
GraphViewStatus.PartReload              = "I_MaskReload"             ;
GraphViewStatus.PartOverwrite           = "I_MaskOverwrite"          ;
GraphViewStatus.PartNew                 = "I_MaskSaveAsNew"          ;
GraphViewStatus.DocumentAsNewRevision   = "I_MaskNewRevision"        ;
GraphViewStatus.PartAsNewVersion        = "I_MaskNewVersion"         ;
GraphViewStatus.PartMapped              = "I_MaskVPDMObject"         ;
GraphViewStatus.PartUnknown             = "I_ScmRecIncompPartIcon"   ;
GraphViewStatus.VPDMObject              = "I_VPMGraphObject"         ;
GraphViewStatus.FilterVPDMObject        = "I_VPMGraphFilterObject"   ;
GraphViewStatus.Modified                = "I_MaskModifiedObject"     ;
GraphViewStatus.Filtered                = "I_VPMGraphFilterObject"   ;
GraphViewStatus.PartOverwriteByDelta    = "I_MaskOverwriteByDelta"  ;
GraphViewStatus.PartAsNewVersionByDelta = "I_MaskNewVersionByDelta" ;

// ---> Reconciliator toolbar icons

RulesFrame.OverwriteButton.Icon          = "I_Overwrite"            ;
RulesFrame.ReloadButton.Icon             = "I_Reload"               ;
RulesFrame.NewButton.Icon                = "I_SaveAsNew"            ;
RulesFrame.NewFromButton.Icon            = "I_SaveAsNewFrom"        ;
RulesFrame.KeepPushBtn.Icon              = "I_BrokenDocumentLink"   ;
RulesFrame.GlobalRulePushBtn.Icon        = "I_ScmGlobalRule"        ;
RulesFrame.SaveContainerBtn.Icon         = "I_ApplicativeContainer" ;
RulesFrame.SearchButton.Icon             = "I_ScmSearch"            ;
RulesFrame.QueryItem.Icon                = "I_PLMAdvancedSearch"    ;
RulesFrame.PrintButton.Icon              = "I_ScmReport"            ;
RulesFrame.NewVersionPushBtn.Icon        = "I_RecNewVersion"        ;
RulesFrame.NewRevisionPushBtn.Icon       = "I_RecNewRevision"       ;
RulesFrame.FilterPushBtn.Icon            = "I_VPMNavFilter"         ;
RulesFrame.OverwriteByDeltaPushBtn.Icon  = "I_OverwriteByDelta"     ;
RulesFrame.NewVersionByDeltaPushBtn.Icon = "I_NewVersionByDelta"    ;
RulesFrame.DebugScmButtonButton.Icon     = "I_ScmDebugTool"         ;

// ---> Save Reconcile Session : Icons to be replaced

RulesFrame.SaveSessionButton.Icon        = "I_SaveRECSession"       ; 
RulesFrame.RestoreSessionButton.Icon     = "I_RestoreRECSession"    ; 

// ---> Magic Recon Button Icon

RulesFrame.MagicReconButton.Icon         = "I_MagicRecon"           ; 
