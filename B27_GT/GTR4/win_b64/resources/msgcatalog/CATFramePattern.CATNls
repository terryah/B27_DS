//=============================================================================
//                                     CNEXT - CXR1
//                          COPYRIGHT DASSAULT SYSTEMES 1997 
//-----------------------------------------------------------------------------
// FILENAME    :    CATFramePattern
// LOCATION    :    DraftingIntUI/CNext/resources/msgcatalog
// AUTHOR      :    Hichem BEN CHEIKH
// DATE        :    Janv. 01 1998
//------------------------------------------------------------------------------
// DESCRIPTION :    Resources File for NLS purpose related to Drafting WorkShop
//                  
//------------------------------------------------------------------------------
// COMMENTS    :
//------------------------------------------------------------------------------
//MODIFICATIONS     user  date      purpose
//   HISTORY        ----  ----      -------
//     01		lgk 17.12.02 Revision
//------------------------------------------------------------------------------

// * Label *
EditorMaterialTitle="Drawing";

Pattern.Hatching.labelNbHatching.Title="Number of hatchings:";

Pattern.Dotting.DottinglabelPitch.Title="Pitch:";
Pattern.Dotting.DottinglabelCol.Title="Color:";
Pattern.Dotting.DottinglabelZig.Title="Zigzag";

Pattern.Coloring.ColoringlabelCol.Title="Color:";

Pattern.Motif.BrowseButton.Title="Browse...";
Pattern.Motif.ModifAngle.Title="Angle:";
Pattern.Motif.ModifScale.Title="Scale:";

Pattern.None.NonelabelCol.Title="No pattern associated";

Pattern.Hatching.labelNbHatching.LongHelp="Defines the number of hatchings.";
Pattern.Dotting.DottinglabelPitch.LongHelp="Sets the pattern pitch.";
Pattern.Dotting.DottinglabelCol.LongHelp="Sets the pattern color.";
Pattern.Dotting.DottinglabelZig.LongHelp="Sets the pattern to zigzag.";
Pattern.Coloring.ColoringlabelCol.LongHelp="Sets the pattern color.";
Pattern.Motif.BrowseButton.LongHelp="Lets you select a file to define the image to use as a pattern.";
Pattern.Motif.ModifAngle.LongHelp="Sets the image angle.";
Pattern.Motif.ModifScale.LongHelp="Sets the image scale.";
Pattern.None.NonelabelCol.LongHelp="Indicates that no pattern is associated.";

Parameters.PatternNameLabel.Title="Name:";
Parameters.TypePatternLabel.Title="Type:"; 

labelAngle="Angle:";
labelPitch="Pitch:";
labelOffset="Offset:";
labelThickness="Thickness:";
labelColor="Color:";
labelTxt="Linetype:";

labelAngle.LongHelp="Sets the angle.";
labelPitch.LongHelp="Sets the pitch.";
labelOffset.LongHelp="Sets the offset.";
labelThickness.LongHelp="Sets the thickness.";
labelColor.LongHelp="Sets the color.";
labelTxt.LongHelp="Sets the linetype.";

Parameters.TypePattern.ShortHelp="Type"; 
Pattern.Hatching.labelThk.Title="Number of hatchings";
TypeHatching="Hatching";
TypeDotting="Dotting";
TypeColoring="Coloring";
TypeMotif="Image";
TypeNone="None";
TabTitle="Pattern";

Parameters.TypePattern.LongHelp="Sets the type."; 
Pattern.Hatching.labelThk.LongHelp="Sets the number of hatchings.";
TypeHatching.LongHelp="Sets the hatching.";
TypeDotting.LongHelp="Sets the dotting.";
TypeColoring.LongHelp="Sets the coloring.";
TypeNone.LongHelp="Uses no pattern.";

Parameters.PatternTable.Title="...";
Parameters.PatternTable.LongHelp="Sets the pattern table.";
TabTitle.LongHelp="Sets the pattern.";

MaterialTitle.Title="Material ";
PreviewTitle.Title=" Preview";

HatchingPage="Hatching";
PatternChooser="Pattern Chooser";

Material.LongHelp="Reads the material parameters.";
Parameters.LongHelp="Sets the parameters.";
Pattern.LongHelp="Sets the pattern.";
Pattern.Hatching.LongHelp="Sets the hatching.";
Pattern.Dotting.LongHelp="Sets the dotting.";
Pattern.Coloring.LongHelp="Sets the coloring.";
Pattern.None.LongHelp="Uses no pattern.";
Preview.LongHelp="Sets the preview.";
HatchingPage.LongHelp="Sets the hatching.";
PatternChooser.LongHelp="Sets the pattern chooser.";

Material.MaterialButton.Title="Reset using Part Material Pattern";
Material.HasNOMaterial.Title="No Material On Part";

Material.MaterialButton.LongHelp="Resets using the pattern defined for the part material.";
