SmallAssemblyName	OffsetPanel(in)	OffsetCoaming(in)	OffsetCurtain(in)
End Post 1in Welded	0.125	0	0
End Post 1in Riveted	0.0625	0	0
End Post 2in Riveted	0.0625	0	0
Corner Assembly 90 Deg 0.75in Radius	0.563	0.563	0.563
Corner Assembly 45 Deg 0.75in Radius	0.125	0.500	0.500
Corner EM 4in Outside	4.125	4.875	4
Corner EM 4in Inside	4.125	4.875	4
Phantom_Corner Assembly 90 Deg 0.75in Radius 	0.563	0.563	0.563
Phantom_Corner Assembly 45 Deg 0.75in Radius	0.125	0.500	0.500
Phantom_Corner EM 4in Outside	4.125	4.875	4
Phantom_Corner EM 4in Inside	4.125	4.875	4
Angle Bar	0.125	0	0
Angle Bar with Clips	0.125	0	0
Angle Bar with Clips 2	0.125	0	0
None	0	0	0
