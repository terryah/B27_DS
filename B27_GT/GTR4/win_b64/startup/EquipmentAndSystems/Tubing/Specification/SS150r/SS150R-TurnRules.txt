NominalSize	BendRadius(in)	DiameterFactor	NominalBendRadius
3/16in	0	2	2D
3/16in	0	3	3D
1/4in	0	2.5	2.5D
1/4in	0.5	0	
5/16in	0.625	0	
3/8in	0.75	0	
1/2in	1	0	
1/2in	1.25	0	
1/2in	0	3	3D
1/2in	0	3.5	3.5D
5/8in	0	2	2D
3/4in	0	2	2D
1in	2	0	
1in	3	0	
1 1/4in	2.5	0	
1 1/4in	3.75	0
1 1/2in	3	0
2in	3	0
2in	4	0
2in	6	0
2 1/2in	5	0
2 1/2in	6.25	0
3in	6	0
