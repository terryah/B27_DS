NominalSize	BendRadius(mm)	DiameterFactor	NominalBendRadius
4mm	0	2	2D
5mm	0	2	2D
5mm	0	3	3D
6mm	0	2.5	2.5D
6mm	12	0	
8mm	16	0	
10mm	20	0	
12mm	0	2	2D
13mm	26	0	
13mm	30	0	
13mm	0	3	3D
13mm	0	3.5	3.5D
14mm	0	2	2D
15mm	0	2	2D
16mm	0	2	2D
18mm	0	2	2D
20mm	0	2	2D
22mm	50.8	2	2D
25mm	50	0	
25mm	75	0	
28mm	60	0	
30mm	76.2	2	2D
35mm	100	0	
38mm	76	0	
42mm	0	2	2D
50mm	100	0	
50mm	150	0	
60mm	0	2	2D
75mm	0	2	2D
