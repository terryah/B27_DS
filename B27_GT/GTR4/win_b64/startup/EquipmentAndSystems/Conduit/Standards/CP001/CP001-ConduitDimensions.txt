NominalSize	BendDiameter(in)	OutsideDiameter(in)	WallThickness(in)	InsideDiameter(in)
1/2in	0.5	0.84	0.17	0.5
3/4in	0.75	1.05	0.15	0.75
1in	1	1.315	0.1575	1
1 1/4in	1.25	1.66	0.205	1.25
1 1/2in	1.5	1.9	0.2	1.5
2in	2	2.375	0.1875	2
2 1/2in	2.5	2.875	0.1875	2.5
3in	3	3.5	0.25	3
3 1/2in	3.5	4	0.25	3.5
4in	4	4.5	0.25	4
5in	5	5.563	0.2815	5
6in	6	6.625	0.3125	6
