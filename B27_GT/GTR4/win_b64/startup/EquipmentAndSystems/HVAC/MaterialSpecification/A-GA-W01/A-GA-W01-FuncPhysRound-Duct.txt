Function	MinSize(in)	MaxSize(in)	PhysicalPart
HVACDuctFunc	1in	48in	HVACRndStDuct
HVACDuctFunc	1in	48in	HVACRndBendDuct
HVACDuctFunc	1in	48in	HVACRndMiteredElbow
HVACDuctFunc	1in	48in	HVACRndSmoothElbow
HVACDuctFunc	1in	48in	HVACRndCollar
HVACDuctFunc	1in	48in	HVACRndCoupling
HVACDuctFunc	1in	48in	HVACRndEndCap
HVACDuctFunc	1in	48in	HVACRndExpJoint
HVACDuctFunc	1in	48in	HVACRndOutlet
HVACDuctFunc	1in	48in	HVACRndLFlange
HVACDuctFunc	1in	48in	HVACRndIFlange
HVACDuctFunc	1in	48in	HVACRndGasket
HVACDuctFunc	1in	48in	HVACRndNReducOffset
HVACDuctFunc	1in	48in	HVACRndOgee
HVACDuctFunc	1in	48in	HVACRndToRnd
HVACDuctFunc	1in	48in	HVACRndAccessCvrRnd
HVACDuctFunc	1in	48in	HVACRndAccessCvrRect


