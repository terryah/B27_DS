NominalSize	BendRadius(in)	DiameterFactor	NominalBendRadius
1in	2	0	
1in	3	0	
2in	3	0	
2in	4	0	
2in	6	0	
4in	0	2	2D
6in	0	1.5	1.5D
8in	8	0	
8in	8.5	0	
8in	0	1.5	1.5D
8in	0	2	2D
10in	0	1.5	1.5D
12in	0	2	2D
14in	0	2	2D
16in	0	2	2D
18in	0	2	2D
20in	0	2	2D
22in	0	2	2D
24in	0	2	2D
