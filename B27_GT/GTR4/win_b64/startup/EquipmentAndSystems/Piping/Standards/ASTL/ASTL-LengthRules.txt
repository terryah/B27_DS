MaterialCategory	MinNominalSize	MaxNominalSize	MinStraightLength(in)	MaxLength(in)	MaxBendAngle(deg)	StandardLength(in)
Carbon steel	1/2in	2in	12 	192	145	48
Carbon steel	2in	4in	24 	192 	145	48
Carbon steel	6in	48in	48 	192 	145	48
Stainless steel	1/2in	2in	12 	192 	145	48
Stainless steel	2 1/2in	4in	24 	192	145	48
Stainless steel	6in	48in	48 	192	145	48
