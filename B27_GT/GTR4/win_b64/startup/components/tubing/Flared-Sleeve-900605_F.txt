CATRouPartNumber	CATRouSize	CATRouOutsideDiameter	SleeveDiameter	FlareDiameter	FaceToFaceLength	FlareEndLength	FlareAngle
900605-3S	3/16"	0.1875in	0.23in	0.27in	0.34in	0.09in	37deg
900605-4S	1/4"	0.2500in	0.30in	0.35in	0.41in	0.09in	37deg
900605-5S	5/16"	0.3125in	0.36in	0.41in	0.44in	0.10in	37deg
900605-6S	3/8"	0.3750in	0.425in	0.47in	0.50in	0.10in	37deg
900605-8S	1/2"	0.5000in	0.56in	0.62in	0.56in	0.12in	37deg
900605-10S	5/8"	0.6250in	0.74in	0.85in	0.66in	0.14in	37deg
900605-12S	3/4"	0.7500in	0.85in	0.95in	0.69in	0.15in	37deg
900605-14S	7/8"	0.8750in	0.96in	1.00in	0.76in	0.15in	37deg
900605-16S	1"	1.0000in	1.05in	1.10in	0.78in	0.16in	37deg
900605-20S	1 1/4"	1.2500in	1.375in	1.50in	0.91in	0.18in	37deg
900605-24S	1 1/2"	1.5000in	1.625in	1.75in	1.12in	0.21in	37deg
900605-32S	2"	2.0000in	2.15in	2.30in	1.19in	0.23in	37deg
