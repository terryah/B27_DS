CATRouPartNumber	CATRouSize	CATRouOutsideDiameter	InsideDiameter	FlareAngle	FlareDiameter
TUBE-F1-3S	3/16"	0.1875in	0.11in	37deg	0.27in
TUBE-F1-4S	1/4"	0.2500in	0.17in	37deg	0.35in
TUBE-F1-5S	5/16"	0.3125in	0.23in	37deg	0.41in
TUBE-F1-6S	3/8"	0.3750in	0.30in	37deg	0.47in
TUBE-F1-8S	1/2"	0.5000in	0.40in	37deg	0.62in
TUBE-F1-10S	5/8"	0.6250in	0.51in	37deg	0.85in
TUBE-F1-12S	3/4"	0.7500in	0.61in	37deg	0.95in
TUBE-F1-14S	7/8"	0.8750in	0.73in	37deg	0.95in
TUBE-F1-16S	1"	1.0000in	0.84in	37deg	1.10in
TUBE-F1-20S	1 1/4"	1.2500in	1.08in	37deg	1.50in
TUBE-F1-24S	1 1/2"	1.5000in	1.31in	37deg	1.75in
TUBE-F1-32S	2"	2.0000in	1.80in	37deg	2.25in
