CATRouPartNumber	CATRouSize	CATRouOutsideDiameter
RD-DAMPER-LBD-03	3"	3in
RD-DAMPER-LBD-04	4"	4in
RD-DAMPER-LBD-05	5"	5in
RD-DAMPER-LBD-06	6"	6in
RD-DAMPER-LBD-07	7"	7in
RD-DAMPER-LBD-08	8"	8in
RD-DAMPER-LBD-09	9"	9in
RD-DAMPER-LBD-10	10"	10in
RD-DAMPER-LBD-12	12"	12in
RD-DAMPER-LBD-14	14"	14in
RD-DAMPER-LBD-16	16"	16in
RD-DAMPER-LBD-18	18"	18in
RD-DAMPER-LBD-20	20"	20in
RD-DAMPER-LBD-22	22"	22in
RD-DAMPER-LBD-24	24"	24in
RD-DAMPER-LBD-26	26"	26in
RD-DAMPER-LBD-28	28"	28in
RD-DAMPER-LBD-30	30"	30in
