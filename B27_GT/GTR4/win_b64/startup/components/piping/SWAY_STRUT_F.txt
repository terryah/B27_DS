CATRouPartNumber	CATRouSize	CATRouOutsideDiameter	ExtensionPieceMinimumLength	ExtensionPieceWeld	ExtensionPieceRodEndDiameter	ExtensionPiecePipeDiameter	PivotJointRadius	PivotToExtensionPieceLength	PivotHoleDiameter	PivotOffset	JamNutDiameter	JamNutHeight	BracketHeight	BracketLength	BracketWidth	PipeClampCenterToOutsideLength	PipeClampGap	PipeClampThickness	RodTakeOut	PipeClampCenterToClampBoltLength	HexHeadDiameter	HexHeadHeight
SWAY-STRUT-00.75in	3/4"	0.75in	12in	0.1875in	0.75in	1.0in	0.625in	2.6875in	0.374in	1.0in	1.299in	0.421875in	2.875in	0.25in	1.25in	1.375in	0.625in	0.1875in	2.4375in	0.9375in	0.65in	0.328125in
SWAY-STRUT-01.00in	1"	1.0in	12in	0.1875in	0.75in	1.0in	0.625in	2.6875in	0.374in	1.0in	1.299in	0.421875in	2.875in	0.25in	1.25in	1.5in	0.625in	0.1875in	2.5625in	1.0625in	0.65in	0.328125in
SWAY-STRUT-01.25in	1 1/4"	1.25in	12in	0.1875in	0.75in	1.0in	0.625in	2.6875in	0.374in	1.0in	1.299in	0.421875in	2.875in	0.25in	1.25in	1.6875in	0.625in	0.1875in	2.6875in	1.25in	0.65in	0.328125in
SWAY-STRUT-01.50in	1 1/2"	1.5in	12in	0.1875in	0.75in	1.0in	0.625in	2.6875in	0.374in	1.0in	1.299in	0.421875in	2.875in	0.25in	1.25in	2.375in	1.0in	0.25in	4.125in	1.8125in	0.65in	0.328125in
SWAY-STRUT-02.00in	2"	2.0in	16.875in	0.3125in	1.5in	2.5in	1.5in	4.25in	0.999in	2.5in	2.598in	0.84375in	4.5in	0.75in	3.0in	2.6875in	1.0in	0.25in	6.375in	2.125in	1.732in	0.859375in
SWAY-STRUT-02.50in	2 1/2"	2.5in	20.5in	0.375in	2.0in	3.0in	2.0in	5.25in	1.249in	3.25in	3.608in	1.09375in	5.375in	1.0in	3.5625in	2.9375in	1.0in	0.25in	8.125in	2.3125in	2.165in	1.0625in
SWAY-STRUT-03.00in	3"	3.0in	20.5in	0.375in	2.0in	3.0in	2.0in	5.25in	1.249in	3.25in	3.608in	1.09375in	5.375in	1.0in	3.5625in	3.5in	1.0in	0.25in	8.125in	2.75in	2.165in	1.0625in
SWAY-STRUT-03.50in	3 1/2"	3.5in	20.5in	0.375in	2.0in	3.0in	2.0in	5.25in	1.249in	3.25in	3.608in	1.09375in	5.375in	1.0in	3.5625in	4.0in	1.0in	0.25in	8.125in	3.0625in	2.165in	1.0625in
SWAY-STRUT-04.00in	4"	4.0in	20.5in	0.375in	2.0in	3.0in	2.0in	5.25in	1.249in	3.25in	3.608in	1.09375in	5.375in	1.0in	3.5625in	4.5in	1.0in	0.3125in	8.375in	3.375in	2.165in	1.0625in
SWAY-STRUT-05.00in	5"	5.0in	20.5in	0.375in	2.0in	3.0in	2.0in	5.25in	1.249in	3.25in	3.608in	1.09375in	5.375in	1.0in	3.5625in	5.0in	1.0in	0.3125in	9.125in	3.9375in	2.165in	1.0625in
SWAY-STRUT-06.00in	6"	6.0in	23.75in	0.375in	2.5in	4.0in	3.0in	6.875in	1.749in	5.0in	4.474in	1.453125in	7.875in	1.75in	5.375in	6.0in	1.75in	0.375in	11.875in	4.75in	3.175in	1.71875in
SWAY-STRUT-08.00in	8"	8.0in	23.75in	0.375in	2.5in	4.0in	3.0in	6.875in	1.749in	5.0in	4.474in	1.453125in	7.875in	1.75in	5.375in	7.125in	1.25in	0.5in	12.625in	6.0in	3.175in	1.71875in
SWAY-STRUT-10.00in	10"	10.0in	32.25in	0.75in	4.0in	6.0in	4.75in	9.25in	2.499in	7.25in	7.073in	2.1875in	14.0in	2.25in	8.75in	9.0in	2.25in	0.5in	16.25in	7.25in	4.474in	2.453125in
SWAY-STRUT-12.00in	12"	12.0in	32.25in	0.75in	4.0in	6.0in	4.75in	9.25in	2.499in	7.25in	7.073in	2.1875in	14.0in	2.25in	8.75in	10.375in	2.5in	0.625in	17.25in	8.625in	4.474in	2.453125in
SWAY-STRUT-14.00in	14"	14.0in	32.25in	0.75in	4.0in	6.0in	4.75in	9.25in	2.499in	7.25in	7.073in	2.1875in	14.0in	2.25in	8.75in	11.625in	2.5in	0.75in	18.0in	9.625in	4.474in	2.453125in
SWAY-STRUT-16.00in	16"	16.0in	32.25in	0.75in	4.0in	6.0in	4.75in	9.25in	2.499in	7.25in	7.073in	2.1875in	14.0in	2.25in	8.75in	13.125in	3.0in	0.75in	19.0in	10.875in	4.474in	2.453125in
SWAY-STRUT-18.00in	18"	18.0in	32.25in	0.75in	4.0in	6.0in	4.75in	9.25in	2.499in	7.25in	7.073in	2.1875in	14.0in	2.25in	8.75in	14.5in	3.5in	1.0in	20.25in	12.5in	4.474in	2.453125in
SWAY-STRUT-20.00in	20"	20.0in	32.25in	0.75in	4.0in	6.0in	4.75in	9.25in	2.499in	7.25in	7.073in	2.1875in	14.0in	2.25in	8.75in	16.0in	3.5in	1.0in	21.5in	13.5in	4.474in	2.453125in
SWAY-STRUT-24.00in	24"	24.0in	32.25in	0.75in	4.0in	6.0in	4.75in	9.25in	2.499in	7.25in	7.073in	2.1875in	14.0in	2.25in	8.75in	18.5in	3.5in	1.0in	24.0in	15.5in	4.474in	2.453125in
SWAY-STRUT-30.00in	30"	30.0in	32.25in	0.75in	4.0in	6.0in	4.75in	9.25in	2.499in	7.25in	7.073in	2.1875in	14.0in	2.25in	8.75in	24.375in	4.25in	1.0in	31.5in	19.875in	4.474in	2.453125in
SWAY-STRUT-36.00in	36"	36.0in	32.25in	0.75in	4.0in	6.0in	4.75in	9.25in	2.499in	7.25in	7.073in	2.1875in	14.0in	2.25in	8.75in	30.125in	4.5in	1.5in	34.75in	24.625in	4.474in	2.453125in
