CATRouPartNumber	CATRouSize	CATRouOutsideDiameter	Rating	FaceToFaceLength	HandWheelHeight	HandWheelDiameter	FlangeDiameter	FlangeThickness
VALVE-GATE-RF-HW-R150-02.00in	2"	2in	150	7.00in	15.75in	8in	6in	0.75in
VALVE-GATE-RF-HW-R150-02.50in	2 1/2"	2.5in	150	7.50in	16.50in	8in	7in	0.88in
VALVE-GATE-RF-HW-R150-03.00in	3"	3in	150	8.00in	18.88in	9in	7.5in	0.94in
VALVE-GATE-RF-HW-R150-04.00in	4"	4in	150	9.00in	23.00in	10in	9in	0.94in
VALVE-GATE-RF-HW-R150-05.00in	5"	5in	150	10.0in	27.88in	12in	10in	0.94in
VALVE-GATE-RF-HW-R150-06.00in	6"	6in	150	10.5in	31.00in	12in	11in	1.00in
VALVE-GATE-RF-HW-R150-08.00in	8"	8in	150	11.5in	38.75in	14in	13.5in	1.12in
VALVE-GATE-RF-HW-R150-10.00in	10"	10in	150	13.0in	46.75in	16in	16in	1.19in
VALVE-GATE-RF-HW-R150-12.00in	12"	12in	150	14.0in	55.00in	18in	19in	1.25in
VALVE-GATE-RF-HW-R150-14.00in	14"	14in	150	15.0in	60.50in	20in	21in	1.38in
VALVE-GATE-RF-HW-R150-16.00in	16"	16in	150	16.0in	66.75in	20in	23.5in	1.44in
VALVE-GATE-RF-HW-R150-18.00in	18"	18in	150	17.0in	76.50in	24in	25in	1.56in
VALVE-GATE-RF-HW-R150-20.00in	20"	20in	150	18.0in	89.00in	30in	27.5in	1.69in
VALVE-GATE-RF-HW-R150-24.00in	24"	24in	150	20.0in	104.0in	36in	32in	1.88in
