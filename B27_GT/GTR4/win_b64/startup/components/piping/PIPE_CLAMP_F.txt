CATRouPartNumber	CATRouSize	CATRouOutsideDiameter	PipeClampGap	RodTakeOut	BoltSize	PipeClampThickness	PipeClampWidth	BoltHeadDiameter	BoltHeadHeight	CenterToTopHeight
PIPE-CLAMP-00.50in	1/2"	0.5in	0.75in	1.125in	0.3125in	0.125in	1.0in	0.577in	0.2656in	1.65625in
PIPE-CLAMP-00.75in	3/4"	0.75in	0.75in	1.1875in	0.3125in	0.125in	1.0in	0.577in	0.2656in	1.71875in
PIPE-CLAMP-01.00in	1"	1.0in	0.75in	1.25in	0.3125in	0.125in	1.0in	0.577in	0.2656in	1.78125in
PIPE-CLAMP-01.25in	1 1/4"	1.125in	0.75in	1.3125in	0.3125in	0.125in	1.0in	0.577in	0.2656in	1.84375in
PIPE-CLAMP-01.50in	1 1/2"	1.5in	0.75in	1.625in	0.3125in	0.125in	1.0in	0.577in	0.2656in	2.15625in
PIPE-CLAMP-02.00in	2"	2.0in	0.75in	2.125in	0.5in	0.25in	1.0in	0.866in	0.4375in	2.75in
PIPE-CLAMP-02.50in	2 1/2"	2.5in	0.75in	2.625in	0.5in	0.25in	1.0in	0.866in	0.4375in	3.25in
PIPE-CLAMP-03.00in	3"	3.0in	0.75in	2.9375in	0.5in	0.25in	1.0in	0.866in	0.4375in	3.5625in
PIPE-CLAMP-03.50in	3 1/2"	3.5in	0.75in	3.1875in	0.5in	0.25in	1.0in	0.866in	0.4375in	3.8125in
PIPE-CLAMP-04.00in	4"	4.0in	0.75in	3.625in	0.5in	0.25in	1.25in	0.866in	0.4375in	4.375in
PIPE-CLAMP-05.00in	5"	5.0in	0.75in	4.1875in	0.5in	0.25in	1.25in	0.866in	0.4375in	4.9375in
PIPE-CLAMP-06.00in	6"	6.0in	1.0in	5.0in	0.5in	0.375in	1.5in	0.866in	0.4375in	5.875in
PIPE-CLAMP-08.00in	8"	8.0in	1.0in	6.125in	0.75in	0.375in	1.5in	1.299in	0.6406in	7.0in
PIPE-CLAMP-10.00in	10"	10.0in	1.25in	7.4375in	0.875in	0.5in	2.0in	1.516in	0.75in	8.5625in
PIPE-CLAMP-12.00in	12"	12.0in	1.25in	8.4375in	0.875in	0.5in	2.0in	1.516in	0.75in	9.5625in
PIPE-CLAMP-14.00in	14"	14.0in	1.25in	9.25in	0.875in	0.5in	2.5in	1.516in	0.75in	10.625in
PIPE-CLAMP-16.00in	16"	16.0in	1.25in	10.25in	0.875in	0.5in	2.5in	1.516in	0.75in	11.625in
PIPE-CLAMP-18.00in	18"	18.0in	1.25in	11.625in	1.0in	0.625in	2.5in	1.732in	0.8594in	13.0in
PIPE-CLAMP-20.00in	20"	20.0in	1.5in	12.75in	1.125in	0.625in	2.5in	1.949in	0.9688in	14.125in
PIPE-CLAMP-24.00in	24"	24.0in	1.5in	15.25in	1.25in	0.625in	3.0in	2.165in	1.0625in	16.875in
PIPE-CLAMP-30.00in	30"	30.0in	2.0in	19.0in	1.75in	0.75in	4.0in	3.175in	1.7188in	21.125in
