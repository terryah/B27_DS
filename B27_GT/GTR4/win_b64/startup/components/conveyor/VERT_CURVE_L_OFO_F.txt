CATRouPartNumber	CATRouSize	CATRouOutsideHeight	CATRouAngle	CATRouTurnRadius	RadiusOffset	YokeSpacing	FreeTrackHeight	FreeTrackWidth	FreeTrackThicknessHeight	FreeTrackThicknessWidth	FreeTrackGap	YokeHeight	YokeWidth	YokeThickness	YokeInsideHeight	YokeOffset	YokeCornerRadiusOutsideTop	YokeCornerRadiusOutsideBottom	YokeCornerRadiusInsideTop	YokeCornerRadiusInsideBottom	YokeExtraInsideHeight	YokeExtraInsideWidth
OFO444SP24-VCL3012	444	4in	30deg	12ft	0in	24in	4in	5.75in	0.3125in	0.1875in	2.5in	11in	15in	0.375in	2.5in	0in	5in	3in	1in	1in	0.5in	0.1in
OFO444SP24-VCL3015	444	4in	30deg	15ft	0in	24in	4in	5.75in	0.3125in	0.1875in	2.5in	11in	15in	0.375in	2.5in	0in	5in	3in	1in	1in	0.5in	0.1in
OFO444SP24-VCL4515	444	4in	45deg	15ft	0in	24in	4in	5.75in	0.3125in	0.1875in	2.5in	11in	15in	0.375in	2.5in	0in	5in	3in	1in	1in	0.5in	0.1in
OFO466SP24-VCL3012	466	6in	30deg	12ft	0in	24in	6in	8.375in	0.3125in	0.1875in	4.625in	14.5in	19.5in	0.5in	3in	0in	5in	3in	1in	1in	0.5in	0.1in
OFO466SP24-VCL3015	466	6in	30deg	15ft	0in	24in	6in	8.375in	0.3125in	0.1875in	4.625in	14.5in	19.5in	0.5in	3in	0in	5in	3in	1in	1in	0.5in	0.1in
OFO466SP24-VCL4515	466	6in	45deg	15ft	0in	24in	6in	8.375in	0.3125in	0.1875in	4.625in	14.5in	19.5in	0.5in	3in	0in	5in	3in	1in	1in	0.5in	0.1in
