CATRouPartNumber	CATRouSize	CATRouOutsideHeight	CATRouOutsideWidth	InsideWidth	LoadingDepth	Radius	TangentLength	Length	Length2	RailFlangeWidth	RailFlangeThickness	RailWebThickness	RungSpacing	RungHeight	RungLength
B34AL-06-HYR12	B34x6"	4.2in	7.5in	6in	3.08in	12in	3in	23.4375in	8in	1.75in	0.12in	0.1in	6in	1in	1in
B34AL-09-HYR12	B34x9"	4.2in	10.5in	9in	3.08in	12in	3in	27.6875in	11in	1.75in	0.12in	0.1in	6in	1in	1in
B34AL-12-HYR12	B34x12"	4.2in	13.5in	12in	3.08in	12in	3in	31.9375in	14in	1.75in	0.12in	0.1in	6in	1in	1in
B34AL-18-HYR12	B34x18"	4.2in	19.5in	18in	3.08in	12in	3in	40.4375in	20in	1.75in	0.12in	0.1in	6in	1in	1in
B34AL-24-HYR12	B34x24"	4.2in	25.5in	24in	3.08in	12in	3in	48.9375in	26in	1.75in	0.12in	0.1in	6in	1in	1in
B34AL-30-HYR12	B34x30"	4.2in	31.5in	30in	3.08in	12in	3in	57.4375in	32in	1.75in	0.12in	0.1in	6in	1in	1in
B34AL-36-HYR12	B34x36"	4.2in	37.5in	36in	3.08in	12in	3in	65.9375in	38in	1.75in	0.12in	0.1in	6in	1in	1in
B34AL-42-HYR12	B34x42"	4.2in	43.5in	42in	3.08in	12in	3in	74.4375in	44in	1.75in	0.12in	0.1in	6in	1in	1in
B35AL-06-HYR12	B35x6"	5.06in	7.49in	6in	3.96in	12in	3in	23.4375in	8in	1.75in	0.1in	0.09in	6in	1in	1in
B35AL-09-HYR12	B35x9"	5.06in	10.49in	9in	3.96in	12in	3in	27.6875in	11in	1.75in	0.1in	0.09in	6in	1in	1in
B35AL-12-HYR12	B35x12"	5.06in	13.49in	12in	3.96in	12in	3in	31.9375in	14in	1.75in	0.1in	0.09in	6in	1in	1in
B35AL-18-HYR12	B35x18"	5.06in	19.49in	18in	3.96in	12in	3in	40.4375in	20in	1.75in	0.1in	0.09in	6in	1in	1in
B35AL-24-HYR12	B35x24"	5.06in	25.49in	24in	3.96in	12in	3in	48.9375in	26in	1.75in	0.1in	0.09in	6in	1in	1in
B35AL-30-HYR12	B35x30"	5.06in	31.49in	30in	3.96in	12in	3in	57.4375in	32in	1.75in	0.1in	0.09in	6in	1in	1in
B35AL-36-HYR12	B35x36"	5.06in	37.49in	36in	3.96in	12in	3in	65.9375in	38in	1.75in	0.1in	0.09in	6in	1in	1in
B35AL-42-HYR12	B35x42"	5.06in	43.49in	42in	3.96in	12in	3in	74.4375in	44in	1.75in	0.1in	0.09in	6in	1in	1in
B36AL-06-HYR12	B36x6"	6.17in	7.45in	6in	5.06in	12in	3in	23.4375in	8in	2in	0.11in	0.075in	6in	1in	1in
B36AL-09-HYR12	B36x9"	6.17in	10.45in	9in	5.06in	12in	3in	27.6875in	11in	2in	0.11in	0.075in	6in	1in	1in
B36AL-12-HYR12	B36x12"	6.17in	13.45in	12in	5.06in	12in	3in	31.9375in	14in	2in	0.11in	0.075in	6in	1in	1in
B36AL-18-HYR12	B36x18"	6.17in	19.45in	18in	5.06in	12in	3in	40.4375in	20in	2in	0.11in	0.075in	6in	1in	1in
B36AL-24-HYR12	B36x24"	6.17in	25.45in	24in	5.06in	12in	3in	48.9375in	26in	2in	0.11in	0.075in	6in	1in	1in
B36AL-30-HYR12	B36x30"	6.17in	31.45in	30in	5.06in	12in	3in	57.4375in	32in	2in	0.11in	0.075in	6in	1in	1in
B36AL-36-HYR12	B36x36"	6.17in	37.45in	36in	5.06in	12in	3in	65.9375in	38in	2in	0.11in	0.075in	6in	1in	1in
B36AL-42-HYR12	B36x42"	6.17in	43.45in	42in	5.06in	12in	3in	74.4375in	44in	2in	0.11in	0.075in	6in	1in	1in
B37AL-06-HYR12	B37x6"	7.14in	7.5in	6in	6.05in	12in	3in	23.4375in	8in	2in	0.09in	0.075in	6in	1in	1in
B37AL-09-HYR12	B37x9"	7.14in	10.5in	9in	6.05in	12in	3in	27.6875in	11in	2in	0.09in	0.075in	6in	1in	1in
B37AL-12-HYR12	B37x12"	7.14in	13.5in	12in	6.05in	12in	3in	31.9375in	14in	2in	0.09in	0.075in	6in	1in	1in
B37AL-18-HYR12	B37x18"	7.14in	19.5in	18in	6.05in	12in	3in	40.4375in	20in	2in	0.09in	0.075in	6in	1in	1in
B37AL-24-HYR12	B37x24"	7.14in	25.5in	24in	6.05in	12in	3in	48.9375in	26in	2in	0.09in	0.075in	6in	1in	1in
B37AL-30-HYR12	B37x30"	7.14in	31.5in	30in	6.05in	12in	3in	57.4375in	32in	2in	0.09in	0.075in	6in	1in	1in
B37AL-36-HYR12	B37x36"	7.14in	37.5in	36in	6.05in	12in	3in	65.9375in	38in	2in	0.09in	0.075in	6in	1in	1in
B37AL-42-HYR12	B37x42"	7.14in	43.5in	42in	6.05in	12in	3in	74.4375in	44in	2in	0.09in	0.075in	6in	1in	1in
