Option Explicit
' ----------------------------------------------------------------------------
' Function Name:   SaveInstances
' Purpose:         Save Instances In SmarTeam DB
'
' Note:            Version is hard-coded
' ----------------------------------------------------------------------------
' 2008/11/14 CJD : Changed the way the temporary directory is created

' GLOBAL VARIABLES
Const BATCH_TEMP_DIRECTORY As String = "BatchTemp"  ' Name of the temporary directory
Const LogMode As Boolean = False                    ' Log for debug ?
Const ScriptName As String = "Save Instances"       ' Script Name

' System
Dim fs As Object            'Scripting.FileSystemObject
Dim CatiaFileSys As Object  ' CATIA FileSystem

' Temporary directory
Dim TempFolder As Object

' SmarTeam constants
Dim SmSession As ISmSession                               ' SmarTeam Session
Dim CATIAProductSupportedClasses As SmApplic.ISmClasses   ' Product supported classes
Dim RetrieveProductsQueryDefinition As ISmQueryDefinition ' Query to retrieve product children
Dim FileCatalog As SmartFileCatalog.ISmFileCatalog        ' File Catalog
Dim ClientContextService As SmartClientContextService.SmClientContextService ' Client Context Service

' Applications
Dim appSTI As Object        ' CATStiVB application
Dim oTheStiEngine As Object ' CAIEngine application

' Options
Dim PropagateSave As Boolean ' Propagate operation to children ?
Dim TempDirectory As String      ' Temporary directory where to copy files from vault

' Processed items
Dim ListObjectIdToProcess() As Long   ' List of the processed items' objectid
Dim ListClassIdToProcess() As Integer ' List of the processed items' classid
Dim NumberItemsToProcess As Long      ' Estimation of the total number of items to process


' Log Text to Debug (LogMode to activate/deactivate)
Sub LogText (iLog As String)
    If (LogMode) Then
        MsgBox iLog
    End If
End Sub

' Display the list of processed items (for debug)
Sub DisplayList()
    Dim StringProcessed As String
    Dim j As Long
    For j = 0 To (NumberItemsToProcess-1)
        StringProcessed = StringProcessed & "[" & ListObjectIdToProcess(j) & " " & ListClassIdToProcess(j) & "]"
    Next
    LogText "Liste = " & StringProcessed
End Sub

' Get the CATIA NLS text from key
Function NLSMsg(iKey As String, iDefaultMsgAsString As String)
    NLSMsg = appSTI.GetNLSMessage("CATSmarTeamIntegration", "SaveInstances." & iKey, iDefaultMsgAsString)
End Function

' Get the SmarTeam NLS text from key
Sub ShowSmErrorMessage(MessageCode As Integer, Params As ISmStrings)
	
	Dim GUIServices As ISmGUIServices
	Dim SmButtons As Object
	Dim ErrERROR
	Dim MESSCOMMON
	Dim Result

	' 07894 is information of success
	If MessageCode <> 07894 Then
		ErrERROR = 2
	End If

	MESSCOMMON = 1

	Set SmButtons = SmSession.NewVariantList()
	SmButtons.Value("0") = tmbOk
	Set GUIServices = SmSession.GetService("SmGUISrv.SmGUIServices")

	Result = GUIServices.GUIStore.DisplayMessage(MESSCOMMON, MessageCode, ErrERROR, 0, SmButtons, Params)	
End Sub

' Does the classid correspond to a product ?
Function IsProductClassId(iClassId As Integer)
    Dim k As Integer
    Dim NumberOfProductClassId As Long ' Number of Product supported classes
    NumberOfProductClassId = CATIAProductSupportedClasses.Count
    IsProductClassId = False
    For k = 0 To (NumberOfProductClassId-1)
        LogText "comparing to class id " & CATIAProductSupportedClasses.Item(k).ClassId
        If (iClassId = CATIAProductSupportedClasses.Item(k).ClassId) Then
            IsProductClassId = True
            Exit Function
        End If
    Next
End Function

' Retrieve the index of a given (objectid, classid) couple in the objectid/classid lists of processed items (-1 if not found)
Function IndexOf(iObjectId As Long, iClassId As Integer)
    Dim l As Long
    IndexOf = -1
    For l = 0 To (NumberItemsToProcess-1)
        If ((ListObjectIdToProcess(l) = iObjectId) And (ListClassIdToProcess(l) = iClassId)) Then
            IndexOf = l
            Exit Function
        End If
    Next
End Function

' Delete a file from its path
Sub DeleteFile(iFilePath)
	Dim myFile
	If fs.FileExists(iFilePath) Then
	    Set myFile = fs.GetFile(iFilePath)
        myFile.Attributes = myFile.Attributes And Not 1 'FileAttributes.ReadOnly
        myFile.Delete
    End If
    Set myFile = Nothing
End Sub

' remove all the files in a folder
'Sub RemoveFilesInFolder (iFolder As Object)
Sub RemoveFilesInFolder (iFolder)
    Dim ListOfFiles
    Dim indexFile As Integer
	Dim f1
    Set ListOfFiles = iFolder.Files
    'For indexFile = 1 To ListOfFiles.Count
    '    DeleteFile(ListOfFiles.Item(1).Path)
	For Each f1 in ListOfFiles
		DeleteFile(f1.Path)
    Next

End Sub

' Empty a whole folder recursively
Sub EmptyFolder (iFolder As Object)
    RemoveFilesInFolder(iFolder)

    Dim ListOfFolders
    Dim indexFolder As Integer
    Set ListOfFolders = iFolder.SubFolders
	For Each folder1 in ListOfFolders
        EmptyFolder(folder1)
        CatiaFileSys.DeleteFolder folder1.Path
    Next
End Sub

Sub AppendItem(iObjectId As Long, iClassId As Integer)

    ' Current Object data   	
    Dim CurrentObject As ISmObject ' The current object
    Dim State As Integer           ' Its state
    Dim Children As ISmObjects     ' Its children
    Dim NbOfChildren As Integer    ' The number of children

	' VERIFY THE ITEM IS NOT ALREADY IN THE LIST OF ITEMS TO PROCESS
	If (IndexOf( iObjectId, iClassId ) <> -1) Then
		Exit Sub
	End If

    ' RETRIEVE THE ITEM
	Set CurrentObject = SmSession.ObjectStore.NewObject(iClassId)
	CurrentObject.ObjectId = iObjectId
	CurrentObject.Retrieve

   	' VERIFY STATE (1 = checked in / 3 = approved / 4 = obsolete)
    State = CurrentObject.Data.ValueAsInteger("STATE")
	If ((State <> 1) And (State <> 3) And (State <> 4)) Then
		Exit Sub
	End If

	' ADD ITEM TO THE PROCESSED LIST
	NumberItemsToProcess = NumberItemsToProcess + 1
	ReDim Preserve ListObjectIdToProcess (NumberItemsToProcess)
	ReDim Preserve ListClassIdToProcess  (NumberItemsToProcess)
	ListObjectIdToProcess (NumberItemsToProcess - 1) = iObjectId
	ListClassIdToProcess  (NumberItemsToProcess - 1) = iClassId

	' CALL THE FUNCTION ON CHILDREN
    If (PropagateSave) Then
	    Dim iChild As Integer

	    Set Children = CurrentObject.RetrieveChildren(RetrieveProductsQueryDefinition)
	    NbOfChildren = Children.Count    	
    	
        For iChild = 0 To NbOfChildren-1

            Dim CurrentDbChild As ISmObject
            Set CurrentDbChild = Children.Item(iChild)
            ' if this is a product append it
            If (IsProductClassId(CurrentDbChild.ClassId)) Then 
                AppendItem CurrentDbChild.ObjectId, CurrentDbChild.ClassId
            End If
        Next

    End If

End Sub

' Process an item to save (recursive to process its children in case of PropagateSave=True)
Sub ProcessItem(iObjectId As Long, iClassId As Integer)

	' File Catalog
    Dim SmFiles As SmartFileCatalog.ISmFiles
    Dim SmFile As SmartFileCatalog.ISmFile
    Dim References As SmartFileCatalog.ISmReferences

    ' Current Object data   	
    Dim CurrentObject As ISmObject ' The current object
	Dim FileName As String         ' The destination file name after copy from vault
   	Dim FullPath As String         ' The file full path after copy from vault
	Dim newStiDBItem As Object     ' The corresponding CATIA DB item
		
	LogText "Object Id: " & iObjectId & " Class Id: " & iClassId

    ' RETRIEVE THE ITEM
	Set CurrentObject = SmSession.ObjectStore.NewObject(iClassId)
	CurrentObject.ObjectId = iObjectId
	CurrentObject.Retrieve

	' COPY FILE FROM VAULT
	FileName = CurrentObject.Data.ValueAsString("CAD_REF_FILE_NAME")
    On Error Resume Next
	CurrentObject.CopyFileFromVault FileName, TempDirectory
    On Error GoTo 0
    FullPath = CatiaFileSys.ConcatenatePaths (TempDirectory, FileName)
    If Not(fs.FileExists(FullPath)) Then
        Exit Sub
    End If
	LogText "object copied to " & FullPath

    ' UPDATE FILE CATALOG
    Set SmFiles = FileCatalog.NewSmFiles
    Set SmFile = SmFiles.Add(FullPath)
    SmFile.FileId = CurrentObject.Data.ValueAsString("TDM_FILE_ID")
    SmFile.Owner = SmSession.UserMetaInfo.UserLogin
    SmFile.LocalState = 3 ' NotEditable � checked in state
    'SmFile.Masks.Add "$F.CATProduct.cgr"
    Set References = FileCatalog.NewSmReferences
    'FileCatalog.RetrieveMasks = True
    FileCatalog.Update SmFiles, References, False, False

	' SAVE INSTANCES FROM DBITEM
	Set newStiDBItem = oTheStiEngine.GetStiDBItemFromCATBSTR(FullPath)
	appSTI.SaveInstances newStiDBItem	

	' DELETE FILES IN THE TEMPORARY FOLDER
    Dim FileCat As Object
    Set FileCat = fs.GetFolder(CatiaFileSys.ConcatenatePaths (TempDirectory, "SMARTEAM-FileCatalog"))
    RemoveFilesInFolder FileCat
    RemoveFilesInFolder TempFolder
	
End Sub


Function SaveInstances(ApplHndl As Long,Sstr As String,FirstPar As Long,SecondPar As Long,ThirdPar As Long ) As Integer
' FirstPar  - Record List, containing all attributes of the current object
' SecondPar - nil
' ThirdPar  - nothing


	' Records
	Dim Record1 As Object ' The list of items selected by the user
	Dim Record2 As Object
	Dim Record3 As Object
	
    Dim CATIA As Object                         ' CATIA application
	Dim SysConfig As Object                     ' System Configuration
	Dim myVersion, myRelease, mySP As String    ' Version, Release, Service Pack
    Dim oIsConnected As Boolean                 ' is CATIA connected to SmarTeam
    Dim PropagationValue As Integer             ' Propagation MsgBox value
	Dim NumberSelectedItems As Long             ' Number of records selected by the user

	' SmObject Data
    Dim ProcessingItemIndex As Long    ' The index of the record being processed
	Dim ClassId As Integer             ' The classid  of the record being processed
	Dim ObjectId As Long               ' The objectid of the record being processed

	Set SmSession = SCREXT_ObjectForInterface(ApplHndl)
	CONV_RecListToComRecordList FirstPar,Record1 
	CONV_RecListToComRecordList SecondPar,Record2
	CONV_RecListToComRecordList ThirdPar,Record3
    
    ' Set CATIA Application (Verify CATIA is running)
    On Error Resume Next
	Set CATIA = GetObject(,"CATIA.Application")
	On Error GoTo 0
	If CATIA Is Nothing Then
		' MsgBox "CATIA must be started before running this script" & Chr(13) & Chr(10),, ScriptName
		Dim Params As ISmStrings
		Set Params = Nothing
		ShowSmErrorMessage 07888, Params
		Exit Function
    End If	
	Set appSTI = CATIA.GetItem("CATStiVB")

	' Compute Version, Release, SP
	Set SysConfig = CATIA.SystemConfiguration
	if (SysConfig Is Nothing) Then
		MsgBox "SystemConfiguration was not found" & Chr(13) & Chr(10),, ScriptName
		Exit Function
	End If
	myVersion = SysConfig.Version
	myRelease = SysConfig.Release
	mySP      = SysConfig.ServicePack

	' Determine if the functionality is available
	If ( myVersion <> 5 ) Or ((myRelease < 19) Or ((myRelease =19) And (mySP <2))) Then
		MsgBox "CATIA V" & myVersion & "R" & myRelease & "SP" & mySP & NLSMsg ("CATIAWrongVersion", "detected => this script is not supported before V5R19SP2") & Chr(13) & Chr(10),, ScriptName
		Exit Function
	End If

    ' Propagate to children ?
    PropagateSave = False
    PropagationValue = MsgBox (NLSMsg ("PropagateToChildren", "Propagate the Save Instances to children ?") & Chr(13) & Chr(10), 3, ScriptName)
    If (PropagationValue = 2) Then ' MsgBoxResult.Cancel
        Exit Function
    ElseIf (PropagationValue = 6) Then ' MsgBoxResult.Yes
        PropagateSave = True
    End If
	
    ' GLOBAL VARIABLES
	Set oTheStiEngine = CATIA.GetItem("CAIEngine") 
	Set fs = CreateObject("Scripting.FileSystemObject")
    Set CatiaFileSys = CATIA.FileSystem
	Set RetrieveProductsQueryDefinition = Nothing

    ' List of classid to process
    Set CATIAProductSupportedClasses = SmSession.MetaInfo.ClassMechanismByName("TDM_CATIA_PRODUCT").SupportedClasses

    ' FILE CATALOG
    Set ClientContextService = SmSession.GetService("SmartClientContextService.SmClientContextService")
    Set FileCatalog = ClientContextService.ClientContext.FileCatalog
    
    ' CONNECTION CATIA-SMARTEAM
    oIsConnected = oTheStiEngine.IsConnected
    If Not oIsConnected Then
        Msg.Open NLSMsg("Connect", "Connecting CATIA to SmarTeam..."),0,False,False
        oTheStiEngine.Connect "", ""
        Msg.Close
    End If

    ' Create temporary directory
    'TempPath = IIf(Environ$("tmp") <> "", Environ$("tmp"), Environ$("temp"))
    TempDirectory = CatiaFileSys.ConcatenatePaths (CatiaFileSys.TemporaryDirectory.Path, BATCH_TEMP_DIRECTORY)
    If CatiaFileSys.FolderExists(TempDirectory) Then
        'Set TempFolder = CatiaFileSys.GetFolder(TempDirectory)
        'EmptyFolder TempFolder
        Set TempFolder = fs.GetFolder(TempDirectory)
        EmptyFolder TempFolder
    Else
        'TempFolder = CatiaFileSys.CreateFolder(TempDirectory)
        Set TempFolder = fs.CreateFolder(TempDirectory)
    End If

    'Initialization
    NumberItemsToProcess = 0
    NumberSelectedItems = Record1.RecordCount

    Msg.Open NLSMsg("Preprocessing", "(step 1/2) preprocessing..."),0,True,True

    On Error Goto ErrorTrap

    ' APPEND THE RECORDS TO THE LIST OF ITEMS TO PROCESS
    For ProcessingItemIndex = 0 To (NumberSelectedItems - 1)

		ClassId = Record1.ValueAsInteger("CLASS_ID", ProcessingItemIndex)
		ObjectId = Record1.ValueAsInteger("OBJECT_ID", ProcessingItemIndex)	
		AppendItem ObjectId, ClassId
        Msg.Thermometer = 100*(ProcessingItemIndex+1)/NumberSelectedItems
        DoEvents
    		
	Next

    Msg.Text = NLSMsg("ProcessingData", "(step 2/2) processing data...")

    For ProcessingItemIndex = 0 To (NumberItemsToProcess - 1)

		ProcessItem ListObjectIdToProcess(ProcessingItemIndex), ListClassIdToProcess(ProcessingItemIndex)
        Msg.Thermometer = 100*(ProcessingItemIndex+1)/NumberItemsToProcess
        DoEvents

	Next

    On Error GoTo 0
    Msg.Close
    MsgBox NLSMsg("Success", "Operation finished successfully") & Chr(13) & Chr(10),, ScriptName
    Exit Function

    ErrorTrap:
        Msg.Close
    If Err = 18 Then
	    MsgBox NLSMsg("Cancel", "Operation cancelled by user") & Chr(13) & Chr(10),, ScriptName
	    Exit Function
    Else
		oIsConnected = oTheStiEngine.IsConnected
		If Not oIsConnected Then
			MsgBox NLSMsg("Disconnect", "SmarTeam has been disconnected from CATIA session") & Chr(13) & Chr(10),, ScriptName
		Else
			MsgBox NLSMsg("Unknown", "Unknown error occurred") & Chr(13) & Chr(10),, ScriptName
		End If
	    Exit Function
	End If


End Function
