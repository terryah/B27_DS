'-----------------------------------------------------------------------------
' Script to do a review process in a DMU context
' Select the document to be review in SmarTeam
' Launch if necessary the DMU session
' Insert this one in a new Product 
'
'     Script Requires:  SmarTeam Record List Library
'						ENOVIA DMU Document Interfaces Object Library
'
'     Author: Dassault-Systemes
'
'     Notes: DMU /RegServer -env...
'
' (C) Copyright Dassault Systemes 2002   All Rights Reserved
'-----------------------------------------------------------------------------
' History:
' MM/DD/YYYY
' 07/06/2005 - use of NM_CAD_REF_FILE_NAME instead of NM_FILE_NAME
'            - use of ExecuteOperationOnObjectTree to copy files out of vault
'            - correction in case product was already out of vault
' 24/07/2008 - In CopyFileDirectory, If DMU is not registered, CATIA will be launched.
'              So , CATIA has to be registered to make this script work.  
'-----------------------------------------------------------------------------


' ----------------------------------------------------------------------------
' GLOBAL VARIABLES
' ----------------------------------------------------------------------------
' These variables can be used anywhere in this script.
' All the corresponding values are set at the beginning of the script
' and won't be modified during the process of the script.
' ----------------------------------------------------------------------------
'
' The running ENOVIA_DMU session.
'
Dim ENOVIA_DMU As Object
'
' The SmarTeam session.
'
Dim SmSession As SmApplic.SmSession
' ----------------------------------------------------------------------------


'Declaration of procedures.
'--------------------------
Declare Sub CONV_RecListToComRecordList Lib "SmTdm32" (ByVal RecList As Long, ByRef COMRecList As ISmRecordList)
Declare Sub CONV_ComRecListToRecordList Lib "SmTdm32" (ByVal ComRecList As ISmRecordList, ByRef RecList As Long)


'-----------------------------------------------------------------------------
' Copy File Directory
'	Retrieving the user directory	
'   Coping the selected object and the soon(s) of the selected object. 
'   Launch/Retrieve the ENOVIA DMU session.
'
'	In : SmarTeamSelectedObject As Object = the Product to be review
'	Out: FullPathName As String	= The full path name where the document is
'								  copied 
'-----------------------------------------------------------------------------
Sub CopyFileDirectory(ByRef SmarTeamSelectedObject As Object, ByRef FullPathName As String)
	'
	' New version based on CopyFile operation
	' inputs: S_GetOperIdByName, newRecList
	'
	Dim SmObject           As Object
	Dim shortName          As String
	Dim state              As Integer
	Dim iDestinationPath   As String
	Dim Operation          As Object
	Dim DefTaskList        As Object
	Dim ObjectAndTreeTasks As Object
	Dim SmSessionUtil      As Object

	Set SmObject = SmarTeamSelectedObject.Clone
	SmObject.AddAllAttributes
	SmObject.RetrieveAttributes

	shortName = SmObject.Data.ValueAsString(NM_CAD_REF_FILE_NAME)
	state = SmObject.Data.ValueAsInteger(NM_STATE)
	If ( state = 0 Or state = 2 ) Then
		iDestinationPath = SmObject.Data.ValueAsString(NM_DIRECTORY)
		FullPathName = iDestinationPath + "\" + shortName
	Else
		iDestinationPath = SmSession.Config.Value(CFGUSERWORKINGDIRECTORY)

		Set Operation = SmSession.MetaInfo.OperationsForClass(SmObject.ClassId, False).ItemByName("CopyFile")

		Set DefTaskList = Nothing
		Set ObjectAndTreeTasks = Nothing

		Set SmSessionUtil = SmSession.GetService("SmUtil.SmSessionUtil")
		SmSessionUtil.ExecuteOperationOnObjectTree SmObject, Operation, true, ObjectAndTreeTasks, DefTaskList
		FullPathName = iDestinationPath + "\" + shortName
	End If
	
	'Launch the ENOVIA DMU session
	'Switch-off the default system error 
	On Error Resume Next
	Set ENOVIA_DMU = GetObject(,"DMU.Application")           
	If (IsEmpty(ENOVIA_DMU) = FALSE) Then
            
		Set ENOVIA_DMU = CreateObject("DMU.Application")        
		If Err Then        
			Err.Clear
                Set ENOVIA_DMU = GetObject(,"CATIA.Application")
         		If Err Then
					Err.Clear                 		        
	    	   		'switch-on the default system error                        
		          	 On Error GoTo 0
            	     Set ENOVIA_DMU = CreateObject("CATIA.Application") 
			    End If                   

         End If                   
       	 ENOVIA_DMU.Visible = True
         Err.Clear
	End If

End Sub


'-----------------------------------------------------------------------------
' Create New DMU Review	
'   Coping the selected object and the soon(s) of the selected object. 
'   Launch the ENOVIA DMU session.
'   Create a new Product 
'   Insert the SmarTeam document in the Product 
'
'	In : SmarTeamSelectedObject As Object = the Product to be review
'-----------------------------------------------------------------------------
Sub CreateNewDMUReview(ByRef SmarTeamSelectedObject As Object)
 	'Global variables necessary in this function.
	'Dim ENOVIA_DMU As Object				 
	'Declaration of variables.
	'--------------------------------------------------
	Dim FullPathName As String
	Dim DMU_Documents As Object
	Dim Prd_DMU_Document As Object
	Dim ProductsDMU	As Object
	Dim ProductDMU As Object
	Dim NbFile As Integer
	'Retrieve the user directory and copy all the files
	CopyFileDirectory SmarTeamSelectedObject, FullPathName
		
	'Create a new Product
	Set DMU_Documents = ENOVIA_DMU.Documents
	Set Prd_DMU_Document = DMU_Documents.Add("Product")
		
	'Insert the SmarTeam document in the Product
	Set ProductDMU = Prd_DMU_Document.Product
	Set ProductsDMU = ProductDMU.Products
	NbFile = 1
	ReDim ValueSTFileArray(NbFile)
	ValueSTFileArray(NbFile-1) = FullPathName
	ProductsDMU.AddComponentsFromFiles ValueSTFileArray, "All"

End Sub


'-----------------------------------------------------------------------------
' Select DMU Review	
'
'	In : SmarTeamSelectedObject As Object = the Product to be review
'   In : ParentsSelectedObject As Object = the parent of the selected Product 
'-----------------------------------------------------------------------------
Sub SelectDMUReview(ByRef SmarTeamSelectedObject As Object,ByRef CompositesDMUObjects As Object)
 	'Global variables necessary in this function.
	'Dim ENOVIA_DMU As Object				
	'Dim SmSession As SmApplic.SmSession 
	'Declaration of variables.
	'--------------------------------------------------
  	Dim GUISrv As Object
  	Dim SmView As Object
  	Dim SmViewWindow As Object
	Dim SmObjects As Object
	Dim Size As Integer  
	Dim ItemDMUObject As Object
	Dim FullPathName As String
	Dim DMU_Documents As Object

	'Openning a modal view with list of Items
    'Retrieving the GUI Services object.
    Set GUISrv = SmSession.GetService("SmGUISrv.SmGUIServices")
    'Creating a new custom view
    Set SmView = GUISrv.NewView(vwtCustom) 	
    'Assigning result objects.
    Set SmView.DisplayObjects.CompositeObjects = CompositesDMUObjects   
    Set SmViewWindow = GUISrv.NewViewWindow 
   	Set SmViewWindow.SmView = SmView
	
    'Openning the view
    SmViewWindow.ShowModal  

	'If user selected ok
    If SmViewWindow.ModalResult = mrOK Then 
		'Retrieving selected object
		'Switch-off the default system error 
		On Error Resume Next
		Set SmObjects = SmView.Selected.Objects
		Size = SmObjects.Count
		'switch-on the default system error
		On Error GoTo 0

		If (Size > 0) Then 
			Set ItemDMUObject = SmView.Selected.SingleObject
			CopyFileDirectory ItemDMUObject, FullPathName
			Set DMU_Documents = ENOVIA_DMU.Documents
			DMU_Documents.Open FullPathName
		End If
    End If 

End Sub


'-----------------------------------------------------------------------------
' Main function of ENOVIADMUReview
'-----------------------------------------------------------------------------
Function ENOVIADMUReview(ApplHndl As Long,Sstr As String,FirstPar As Long,SecondPar As Long,ThirdPar As Long ) As Integer
 	'Global variables necessary in this function.				
	'Dim SmSession As SmApplic.SmSession 
	'--------------------------------------------------	
	'Declaration of variables.
	Dim COMFirstList As Object
	Dim COMSecondList As Object
	Dim COMThirdList As Object
	Dim COMFirstSmObject As Object	
	Dim SmQueryDefinition As Object
	Dim Size As Integer
	Dim ParentsObjects As Object
	Dim ParentObject As Object
	Dim Parent As Object
	Dim CompositesDMUObject As Object
	Dim CompositesDMUObjects As Object
	Dim ClassId As Integer
	Dim Class As Object
	Dim ClassName As String
	Dim DMUReview As Boolean
	Dim DoAReview As String
	Dim ButtonTest As Integer
	Dim rc As Integer
	
'ViewPort.Open "Debug window",3000,3000,500,500
'ViewPort.Clear

	On Error GoTo AssignErrorCode
	'========================================================================
	'Converting procedural script arguments into COM ones.
	'========================================================================
		'Get Session from Application handle.
   		Set SmSession = SCREXT_ObjectForInterface(ApplHndl)
   		'Convert Record lists to COM Representation.
		CONV_RecListToComRecordList	FirstPar,COMFirstList
		CONV_RecListToComRecordList	SecondPar,COMSecondList
		CONV_RecListToComRecordList	ThirdPar,COMThirdList

	'========================================================================
	'Get selected object.
	'========================================================================
		'Impossible case.
		If COMFirstList.RecordCount <= 0 Then
			GoTo NoSelectedObject
		End If

	'========================================================================
	'One selected object ?
	'========================================================================
		'Warning one document selected.
		If COMFirstList.RecordCount > 1 Then
			'Warning.
			MsgBox "Warning : You have selected more than one object."+(Chr(13))+"Only the first document is going to be reviewed in the DMU session."
		End If	

	'========================================================================
	'Retrieving all the review created for this document 
	'========================================================================
		Set COMFirstSmObject = SmSession.ObjectStore.ObjectFromData(COMFirstList.GetRecord(0),True).Clone
		COMFirstSmObject.Retrieve

		Set ParentsObjects = COMFirstSmObject.RetrieveParents(SmQueryDefinition)
		Size = ParentsObjects.Count
		'In all cases, we create a new review if we haven't found another review of the selected object
		DMUReview = False
		'Create a new SmObjects 
		Set CompositesDMUObject = SmSession.ObjectStore.NewCompositeObject
		For i = 0 To Size-1 
			Set ParentObject = ParentsObjects.Item(i)
			Set Parent = ParentObject.Clone
			Parent.Retrieve		
			'Display in a SmarTeam window only the DMU Product class
			ClassId = Parent.Data.ValueAsInteger("CLASS_ID")
			Set Class = SmSession.MetaInfo.SmClass(ClassId)
			ClassName = Class.ExternalName
			'If the Name of your class isn't DMU Product, please change the line below
			If (ClassName = "DMU Product") Then
				DMUReview = True
				CompositesDMUObject.AddObject Parent	
			End If
		Next i

		If (DMUReview = True) Then
			'Reviews of the selected document already exist in the database.
			'Do you want to use one of them ?
			'If not, a new one will be created.  
			ButtonTest = 3 'vbYesNoCancel
			rc = MsgBox("Reviews of the selected document already exist in the database."+(Chr(13))+"Do you want to use one of them ?"+(Chr(13))+"If not, a new one will be created.",ButtonTest,"ENOVIA DMU Review")
			'rc = 6 => Yes
			'rc = 7 => No
			'rc = 2 => Cancel
			If (rc = 7) Then
				'User select No button
				DMUReview = False
			Else 
				If (rc= 2) Then
					'User select Cancel button
					GoTo EndFunction
				End If
			End If	
		End If

		If (DMUReview = True) Then
			'One or more reviews...
			Set CompositesDMUObjects = SmSession.ObjectStore.NewCompositeObjects
			CompositesDMUObjects.Add CompositesDMUObject
			SelectDMUReview COMFirstSmObject, CompositesDMUObjects
		Else
			'Create a new DMU review
			CreateNewDMUReview(COMFirstSmObject)
		End If

	EndFunction :
	'========================================================================
	'Converting COM Variable into procedural script output.
	'========================================================================
		CONV_ComRecListToRecordList	COMThirdList,ThirdPar

	'========================================================================
	'End Function.
	'========================================================================

		ENOVIADMUReview = Err_None ' the function should return a value.
			              		   ' Err_None=0
		Exit Function

	'========================================================================
	'Error Management.
	'========================================================================
	NoSelectedObject:
		MsgBox "No selected object in the SmarTeam session."
		ENOVIADMUReview = Err_Gen
		Exit Function

	AssignErrorCode:
		MsgBox "Internal Error. The Script ENOVIA DMU Review ended. "+(Chr(13))+"Contact your administrator."
		ENOVIADMUReview = Err_Gen
		Exit Function

End Function

