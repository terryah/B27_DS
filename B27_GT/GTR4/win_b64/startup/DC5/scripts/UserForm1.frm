VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} UserForm1 
   Caption         =   "Refresh Context"
   ClientHeight    =   4665
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   7050
   OleObjectBlob   =   "UserForm1.frx":0000
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "UserForm1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Public Sub UserForm_Initialize()

'Fill the ListView of Dialog
   If SourceRevisionsCount > 0 Then
        Dim Count As Long
        For Count = 0 To (SourceRevisionsCount - 1)
ListBox1.AddItem
   ListBox1.List(Count, 0) = Revs(Count)
   ListBox1.List(Count, 1) = SourceTitles(Count)
Next
 Else
        Exit Sub
    End If
lbl_Exit:

    'Filling TextBoxes
    SelectedElementType_TextBox1.Text = SelectedElementType
    SelectedElementName_TextBox2.Text = SelectedElementName
    SelectedElementRevision_TextBox3.Text = SelectedElementRevision
    SourceType_TextBox4.Text = SourceType
    SourceName_TextBox5.Text = SourceName
    SourceRevision_TextBox6.Text = SourceRevision


End Sub


Private Sub CommandButton1_Click()



'Get the Root Active product
    Dim CurrentProduct As Product
    Call Getproduct(CurrentProduct)
    If CurrentProduct Is Nothing Then
        Exit Sub
    End If


    
    ProductsCounter = 0

    'Here we will prepare an array of all the immediate parents of the selected element in case there are multiple
    'instances under different parents in the CATIA tree. If we wish to replace only the selected element and not other
    'instances, we will just need to find the immediate parent of the selected element and run ReplaceComponent on it with
    '3rd argument as False

    'Retrieve the Array of Products collection to which the selected element belongs
    On Error Resume Next
    Call GetAllProducts(CurrentProduct)
    If Err Then
        MsgBox "GetAllProducts failed!!!"
        Exit Sub
    End If

    'Get User's choice from ListView
    If ListBox1.ListIndex = -1 Then
        MsgBox "Nothing was selected!"
        Exit Sub
    Else
        MsgBox "User's choice is " & SourceName & " Revision " & ListBox1.List(ListBox1.ListIndex, 0)
    End If


    'Download the context to replace with at ENOVIA Local Workspace

    'Create Operation
    Dim Operation
    If Not V6Engine Is Nothing Then
        On Error Resume Next
        Set Operation = V6Engine.CreateOpenOperation
        If Err Then
            MsgBox "Create Opeartion failed!!!"
            Exit Sub
        End If
    Else
        Exit Sub
    End If


    'Get ID of the selected item

    'Get Full revision of selected Source
    Dim Fullrev
    Fullrev = ListBox1.List(ListBox1.ListIndex, 0)

    'Split the SelectedItemRevision in order to get Revision and Iteration. Search for "."
    Dim position As Integer
    position = InStr(Fullrev, ".")

    Dim Revision, Iteration
    Revision = Left(Fullrev, (position - 1))

    Dim length
    length = Len(Fullrev)

    Iteration = Mid(Fullrev, (position + 1), length)


    Dim SelectedItemID
    Set SelectedItemID = V6Engine.GetIDFromTNRI(SourceType, SourceName, Revision, Iteration)


    'Add root item to open operation
    If Not Operation Is Nothing Then
        On Error Resume Next
        Operation.AddRoot (SelectedItemID)
        If Err Then
            MsgBox "AddRoot failed!!!"
            Exit Sub
        End If
    Else
        Exit Sub
    End If

    'Get OpenItems Collection
    Dim OpenItems
    Set OpenItems = Operation.RootItems
    If Err Then
        MsgBox "Creation of Open Item Failed!!!"
        Exit Sub
    End If

    'Get size of Open Items
    Dim oSize
    oSize = OpenItems.Count
    If oSize < 1 Then
        Exit Sub
    End If

    'Get the corresponding open item
    Dim OpenItem
    For i = 1 To oSize
        If Not OpenItems Is Nothing Then
            If OpenItems.Item(i).V6ID = SelectedItemID Then
                Set OpenItem = OpenItems.Item(i)
            End If
        Else
            Exit Sub
        End If
    Next


    'Include the Open Item in Open Operation
    OpenItem.Included = True



    'Download the selected context
    If Not Operation Is Nothing Then
        On Error Resume Next
    Operation.OverWrite = V6Operation_Overwrite_Always
        Call Operation.Download
        If Err Then
            MsgBox "ERROR: " & Err.Description
            Exit Sub
        End If
    Else
        Exit Sub
    End If


    'Fetch path of downloaded Item
    If Not OpenItem Is Nothing Then
        On Error Resume Next
        Dim Path As String
        Path = OpenItem.FilePath
        If Err Then
            MsgBox "Filepath of downloaded document can not be fetched!!!"
            Exit Sub
        End If
    Else
        Exit Sub
    End If

    'Check if the document in Work FOlder is same as the Selected one in the list

    If Not V6Engine.GetIDFromFileName(Path).RI = Fullrev Then
        MsgBox "Revision of file in Work folder is different than the one of selected item in list as user cancelled the overwrite option.", , "Overwrite has been cancelled."
        Exit Sub
    End If



    'Replace Context
    'If we don't want to replace all the instances, we dont need to traverse in loop here and only on the immediate parent
    'ReplaceComponent can be run with 3rd argument as False. 3rd Argument in this API determines whether to replace or not
    'all instances under a given immediate parent

    For Num = 1 To UBound(ProductsArray)
        'Get the selected element product
        If Not ProductsArray(Num) Is Nothing Then
            Set product2 = ProductsArray(Num).Item(SE.LeafProduct.Name)
        Else
            Exit Sub
        End If

        'Replace
        On Error Resume Next
        Set Nothing1 = ProductsArray(Num).ReplaceComponent(product2, Path, True)
        If Err Then
            MsgBox "Replace Component API failed!!!"
            Exit Sub
        End If
    Next

    'Show message after successful Replace
    If Err = "Nothing" Then
        MsgBox SelectedElementName & " Revision " & SelectedElementRevision & vbCrLf & " has been replaced by " & vbCrLf & SourceName & " Revision " & Fullrev, , "Refresh Context Report"
    Else
        MsgBox "Replace Context failed"
        Exit Sub
    End If
    Erase ProductsArray
    CommandButton2_Click


End Sub

Private Sub CommandButton2_Click()
    Unload Me
End Sub


