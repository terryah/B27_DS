
Information:
------------

The CATDrwStandard file format for standard files is obsolete in V5R9. It is replaced by XML standard files.
The default location of these files is $install_root\$OS\resources\standard\drafting.

You will find more information on this V5R9 enhancement in Interactive Drafting Users's guide / Customizing section (Manage standards):
 - format and content of standard XML files
 - How to customize the files using Tools/standards new interactive editor 
 - How to upgrade your CATDrwStandard files in XML files
 - How to administrate standard files and control their use

