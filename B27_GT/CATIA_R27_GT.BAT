@echo off

cd /d %~dp0

REM ============================================================================================
REM CATIA 3DEXPERIENCE installation path
REM ============================================================================================


set CAT_CODE=%~dp0GTR4

set CATPath1=%~dp0GTR4
set CATPath2=%~dp0GTR5
set CATPath3=%~dp0GTR7
set CATPath4=%~dp0GTR8
set CATPath5=%~dp0GTR9
set CATPath6=%~dp0GTR10
set CATPath7=%~dp0GTR11
set CATPath8=%~dp0GTR12
set CATPath9=%~dp0GTR13

SET CheckCODE=false
If NOT DEFINED CAT_CODE SET CheckCODE=true
IF NOT EXIST "%CAT_CODE%" SET CheckCODE=true
IF %CheckCODE%==true ( 
ECHO CAT_CODE hasn't been set well, please set it to right CATIA installation path first! 
ECHO Please contact administrator. 
PAUSE
EXIT
)

REM ============================================================================================
REM set caa code env 
REM ============================================================================================
IF NOT EXIST C:\Temp MD	C:\Temp

REM ============================================================================================
REM Start of CATIA
REM ============================================================================================

"%CAT_CODE%\win_b64\code\bin\CATSTART.exe" -run "DigitalProject.exe" -env Env9_GT -direnv "%~dp0CATEnv" -nowindow

REM "%CAT_CODE%\win_b64\code\bin\CATSTART.exe" -run "CNEXT.exe" -env Env9 -direnv "%~dp0CATEnv" -nowindow

:CATIA
REM "%CAT_CODE%\win_b64\code\bin\CATSTART.exe" -run "CNEXT.exe" -env Env9 -direnv "%~dp0CATEnv" -nowindow
REM "%CAT_CODE%\win_b64\code\bin\CATSTART.exe" -run "CNEXT.exe" -env Env9 -direnv "%~dp0CATEnv" -nowindow
REM "%CAT_CODE%\win_b64\code\bin\CATSTART.exe"  -run "CNEXT.exe" -env Env9 -direnv "%~dp0CATEnv" -nowindow 
:DP
REM "%CAT_CODE%\win_b64\code\bin\CATSTART.exe" -run "DigitalProject.exe" -env Env9 -direnv "%~dp0CATEnv" -nowindow
:Tools
REM "%CAT_CODE%\win_b64\code\bin\CATUTIL.exe"  -env Env9 -direnv "%~dp0CATEnv"
REM "%CAT_CODE%\win_b64\code\bin\CATIAENV.exe"  -env Env9 -direnv "%~dp0CATEnv"
REM "%CAT_CODE%\win_b64\code\bin\DSLicMgt.exe"  -env Env9 -direnv "%~dp0CATEnv"
REM "%CAT_CODE%\win_b64\code\bin\CATNodelockMgt.exe"  -env Env9 -direnv "%~dp0CATEnv"
REM "%CAT_CODE%\win_b64\code\bin\CATPrinterManager.exe"  -env Env9 -direnv "%~dp0CATEnv"
REM "%CAT_CODE%\win_b64\code\bin\CATOptionsMgt.exe"  -env Env9 -direnv "%~dp0CATEnv"
REM "%CAT_CODE%\win_b64\code\bin\CATSoftwaREMgt.exe"  -env Env9 -direnv "%~dp0CATEnv"
REM "%CAT_CODE%\win_b64\code\bin\CATSTART.exe"  -run VaultClientSetup.EXE -env Env9 -direnv "%~dp0CATEnv" -nowindow

pause
