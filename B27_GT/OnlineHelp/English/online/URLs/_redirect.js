// redirect template
// all html files with this text have the form: _htmPageName.htm  where 
// htmPageName is the actual name of the file
//
// This script jumps to the appropriate page in the book.
//
// for example the CATNls reference is 
//	CATPRDCmdWorkshop.AsmAddNewPart="mergedProjects/project/_pstugbt0303.htm";
// the page will actually be referenced as
//	mergedProjects/project/_pstugbt0303.htm#CATPRDCmdWorkshop.AsmAddNewPart
//  This will be converted by _pstugbt0303.htm to 
//  	_book?pstugbt0303.htm,CATPRDCmdWorkshop.AsmAddNewPart
// in the same directory 
//	mergedProjects/project
// the first reference to two questions marks is replaced by name of the file with the underscore:
//	_pstugbt0303.htm
//
// when a # reference is passed eg _fred.htm#fred, add the internal reference
var thePage = location.href;




// prepare to get the file name and extension, stripping off the directory information
var pageRef = thePage.lastIndexOf("/");

// eduardo
var bookname = thePage.substring(0,pageRef);
bookname = bookname.substring(bookname.lastIndexOf("/")+1,bookname.length);


var thePage = thePage.substr(pageRef+2);
var ref = thePage.indexOf("#");
var COMMA = ",";
if (ref < 0)
	theRef = "";
else {
        // there is a bookmark reference
	// get it and store it in theRef
	theRef = COMMA + thePage.substring(ref+1);
	// and remove the bookmark reference from thePage
	thePage = thePage.substring(0, ref);
}
var relativepath = "http://www.gtwiki.org/mwiki/r4doc/English/online/books/";

// case sensitive in linux wiki server
if(bookname=="photoStudio")
{
	bookname = "PhotoStudio";
}

if(bookname=="productEngineeringOptimizer")
{
	bookname = "ProductEngineeringOptimizer";
}


if(bookname=="DigitalProjectHome")
{
	bookname = "../DigitalProjectHome";
}


// Open a new window, opening the book at the appropriate page
if(bookname == "../DigitalProjectHome")
{
window.top.location.href=relativepath + bookname + "/_book.htm?";
}
else
{
window.top.location.href=relativepath + bookname + "/" + thePage;
}
// window.top.location.href=relativepath + bookname + "/_book.htm?" + thePage + theRef;
//document.write(relativepath + bookname + "/_book.htm?" + thePage + theRef);
//OpenWin = this.open("_book.htm?" + thePage + theRef, "DigitalProject", "toolbar=yes,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
//-->
