# README


#### Software Architecture

There are two versions of APP 
- The original CATIA version, but free to use all workbench, in which the DP workbench is embedded, and the DP workbench has been specially adjusted for this version to adapt to the settings in the options;

- CATIA version customized by GT is automatically activated and has good stability, but due to software options, it cannot be mixed with the above version and exists independently;


#### INTRO

##### DS_CATIA & DP
- GTR3=DS CATIA full version
- GTR6=GT DP AEC WORKBENCH -customized for full version of DS CATIA

##### GT_CATIA & DP

- GTR4=GT CATIA -only the B27 version of GT
- GTR5=GT DP AEC WORKBENCH -customized only for GT CATIA

##### ADD IN
- GTR7=ADDIN 1
- GTR8=ADDIN 2
- GTR9=ADDIN 3
- GTR10=ADDIN 4
 ......


##### DS Env

ENV.txt set 9 folders can supported. if more need to reset the environment variables;


##### SET LICENSE

FOR DS CATIA ,Please do not select the following three licenses ；
    DIC \ ED2 \ I3D

in the software \options\license, cancel them;


