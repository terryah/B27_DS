#ifdef LOCAL_DEFINITION_FOR_IID
LINK_WITH_FOR_IID = \
GeometricObjectsUUID
#else
LINK_WITH_FOR_IID =
#endif
#
BUILT_OBJECT_TYPE=SHARED LIBRARY
#
INCLUDED_MODULES_COMMON = AdvTopoUtil FrFTopologicalOpe PowerTopologicalOpe \
                          AdvThickness AdvTopoOpeError AdvExtrude AdvTopoOpeItf GeometrySizeOptimization \
                          Silhouette Replay_AdvancedTopologicalOpe ShellFlattener CATCGMVolumetricSweepCmp \
						  PrintSetup

#if defined (CATIAV5R23) || defined (CATIAR213)
INCLUDED_MODULES = \
  $(INCLUDED_MODULES_COMMON) \
  Hatching
#else
INCLUDED_MODULES = \
  $(INCLUDED_MODULES_COMMON)
#endif

LINK_WITH_COMMON = \
  $(LINK_WITH_FOR_IID) \
  JS0GROUP \
  JS03TRA \
  CATMathematics \
  CATMathStream \
  CATGeometricObjects \
  CATCGMGeoMath \
  AdvancedMathematics \
  CATGeometricOperators \
  CATTopologicalObjects \
  CATTopologicalOperators \
  CATTopologicalOperatorsLight \
  CATFreeFormOperators \
  TessAPI \
  TessPolygon \
  BOIMPOPE \
  HLRTools \
  CATTesHLRUtilities \
  CATGMModelInterfaces \
  CATGMOperatorsInterfaces \
  CATAdvancedTopologicalOpeLight \
  HLREngine

#if defined (CATIAV5R23) || defined (CATIAR213)
#if defined (CATIAV5R27) || defined (CATIAR419)
#if defined (CATIAV5R28) || defined (CATIAR420)
LINK_WITH = \
  $(LINK_WITH_COMMON) \
  CATTechTools \
  PolygonalBoolean \
  PolygonalHatching \
  PolyhedralModel \
  PolyhedralAttributes \
  PolyMathArithmetic \
  PolyMeshImpl \
  PolyhedralModel \
  PolyBodyServices
#else
  LINK_WITH = \
  $(LINK_WITH_COMMON) \
  CATTechTools \
  PolygonalBoolean \
  PolygonalHatching \
  PolyhedralModel \
  PolyhedralAttributes \
  PolyMathArithmetic \
  PolyMeshImpl \
  PolyhedralModel
#endif
#else
LINK_WITH = \
  $(LINK_WITH_COMMON) \
  CATTechTools \
  PolygonalBoolean \
  PolygonalHatching \
  PolyhedralModel \
  PolyhedralAttributes \
  PolyMathArithmetic
#endif
#else
LINK_WITH = \
  $(LINK_WITH_COMMON)
#endif


#ifdef CATIAV5R20
ALIASES_ON_IMPORT=CATAdvancedTopologicalOpe CATAdvancedTopologicalOpeLight CATGMModelInterfaces CATGMOperatorsInterfaces
#endif

# +++ PR64
# la ligne precedente remplace:
#	  TessAPI   BOIMPOPE
# +++ PR64
#
OS = Windows_NT
#if os win_b64
#else
OPTIMIZATION_CPP = /O2
#endif
#
 

