#include "CATIACGMLevel.h"
#ifdef CATIACGMR420CAA
#if defined __AdvTopoUtil
/** @CAA2Required */
/*---------------------------------------------------------------------*/
/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */
/*---------------------------------------------------------------------*/
// COPYRIGHT DASSAULT SYSTEMES 1999
#define ExportedByAdvTopoUtil DSYExport
#else
#define ExportedByAdvTopoUtil DSYImport
#endif
#include "DSYExport.h"
#else
#ifdef	_WINDOWS_SOURCE
#ifdef	__AdvTopoUtil

/** @CAA2Required */
/*---------------------------------------------------------------------*/
/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */
/*---------------------------------------------------------------------*/
// COPYRIGHT DASSAULT SYSTEMES 1999
#define	ExportedByAdvTopoUtil	__declspec(dllexport)
#else
#define	ExportedByAdvTopoUtil	__declspec(dllimport)
#endif
#else
#define	ExportedByAdvTopoUtil
#endif
#endif
#include <AdvCommonDec.h>
