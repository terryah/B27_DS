#include "CATIACGMLevel.h"
#ifndef AdvThickness_h
#define AdvThickness_h
// COPYRIGHT DASSAULT SYSTEMES 1999
/** @CAA2Required */
/*---------------------------------------------------------------------*/
/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */
/*---------------------------------------------------------------------*/
#ifdef CATIACGMR420CAA
#if defined(__AdvThickness)
#define ExportedByAdvThickness DSYExport
#else
#define ExportedByAdvThickness DSYImport
#endif
#include "DSYExport.h"
#else
#ifdef	_WINDOWS_SOURCE
#ifdef	__AdvThickness
#define	ExportedByAdvThickness	__declspec(dllexport)
#else
#define	ExportedByAdvThickness	__declspec(dllimport)
#endif
#else
#define	ExportedByAdvThickness
#endif
#endif
#include <AdvCommonDec.h>
#endif
