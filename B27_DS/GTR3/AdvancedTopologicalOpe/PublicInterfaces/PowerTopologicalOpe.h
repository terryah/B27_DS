#include "CATIACGMLevel.h"
#ifdef CATIACGMR420CAA
#ifdef	__PowerTopologicalOpe


// COPYRIGHT DASSAULT SYSTEMES 1999

/** @CAA2Required */
#define ExportedByPowerTopologicalOpe DSYExport
#else
#define ExportedByPowerTopologicalOpe DSYImport
#endif
#include "DSYExport.h"
#else
#ifdef	_WINDOWS_SOURCE
#ifdef	__PowerTopologicalOpe
// COPYRIGHT DASSAULT SYSTEMES 1999
/** @CAA2Required */
#define	ExportedByPowerTopologicalOpe	__declspec(dllexport)
#else
#define	ExportedByPowerTopologicalOpe	__declspec(dllimport)
#endif
#else
#define	ExportedByPowerTopologicalOpe
#endif
#endif
#include <AdvCommonDec.h>
