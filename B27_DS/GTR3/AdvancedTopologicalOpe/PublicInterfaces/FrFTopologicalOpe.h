#include "CATIACGMLevel.h"
#ifdef CATIACGMR420CAA
#ifdef	__FrFTopologicalOpe


// COPYRIGHT DASSAULT SYSTEMES 1999

/** @CAA2Required */
#define ExportedByFrFTopologicalOpe DSYExport
#else
#define ExportedByFrFTopologicalOpe DSYImport
#endif
#include "DSYExport.h"
#else
#ifdef	_WINDOWS_SOURCE
#ifdef	__FrFTopologicalOpe
// COPYRIGHT DASSAULT SYSTEMES 1999
/** @CAA2Required */
#define	ExportedByFrFTopologicalOpe	__declspec(dllexport)
#else
#define	ExportedByFrFTopologicalOpe	__declspec(dllimport)
#endif
#else
#define	ExportedByFrFTopologicalOpe
#endif
#endif
#include <AdvCommonDec.h>
