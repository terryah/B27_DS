#include "CATIACGMLevel.h"
#ifdef CATIACGMR420CAA
#ifdef	__GeometrySizeOptimization


/** @CAA2Required */

// COPYRIGHT DASSAULT SYSTEMES 2006
#define ExportedByGeometrySizeOptimization DSYExport
#else
#define ExportedByGeometrySizeOptimization DSYImport
#endif
#include "DSYExport.h"
#else
#ifdef	_WINDOWS_SOURCE
#ifdef	__GeometrySizeOptimization
/** @CAA2Required */
// COPYRIGHT DASSAULT SYSTEMES 2006
#define	ExportedByGeometrySizeOptimization	__declspec(dllexport)
#else
#define	ExportedByGeometrySizeOptimization	__declspec(dllimport)
#endif
#else
#define	ExportedByGeometrySizeOptimization
#endif
#endif
#include <AdvCommonDec.h>

