// Declarations communes tous modules AdvancedTopologicalOpe.
// COPYRIGHT DASSAULT SYSTEMES 2007
/** @CAA2Required */
//**********************************************************************
//* DON T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS *
//* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPEAR AT ANY TIME *
//**********************************************************************
// 21/02/07 NLD Creation
// 28/03/07 NLD On utilise CATTrackNSTOL au lieu de FrFTrackNSTOL
// 19/04/11 NLD Ajout         CATIACGMLevel  
//#include <FrFTrackNSTOL.h>
#include <CATTrackNSTOL.h>
#include <CATIACGMLevel.h>

