#ifndef CreateTopologicalBlend_H 
#define CreateTopologicalBlend_H 

// COPYRIGHT DASSAULT SYSTEMES  1999
 /** @CAA2Required */ 

#include "FrFTopologicalOpe.h"

#include "CATSkillValue.h"
#include "CATIACGMLevel.h"
class CATGeoFactory;
class CATBody;
class CATTopologicalBlend;
class CATCGMJournalList;


#endif


