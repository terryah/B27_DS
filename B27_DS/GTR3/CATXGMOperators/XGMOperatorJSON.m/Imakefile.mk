#
BUILT_OBJECT_TYPE=SHARED LIBRARY	
#
LINK_WITH_COMMON =  JS0GROUP CATSysTS \
                    YN000MAT YP00IMPL YN000FUN \
					CATMathStream \
					XGMOpe
  
#ifdef CATIAR418
LINK_WITH = $(LINK_WITH_COMMON)  \
  ECMAScriptKernel
#else
LINK_WITH = $(LINK_WITH_COMMON) ObjectModelerSystem
#endif
 
  
#
OS = Windows_NT
#if os win_b64
#else
OPTIMIZATION_CPP = /O2
#endif
#

