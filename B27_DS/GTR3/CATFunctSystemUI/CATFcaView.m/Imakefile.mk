#ifdef LOCAL_DEFINITION_FOR_IID
LINK_WITH_FOR_IID = \
CATFunctSystemItfUUID \
CATVisUUID \
ObjectModelerBaseUUID
#else
LINK_WITH_FOR_IID =
#endif
# COPYRIGHT DASSAULT SYSTEMES 1999
#======================================================================
# Imakefile for module CATFcaView.m
#======================================================================
#
#  Aug 1999  Creation: Code generated by the CAA wizard  PHB
#======================================================================
#
# SHARED LIBRARY 
#
BUILT_OBJECT_TYPE=SHARED LIBRARY 
 
# DO NOT EDIT :: THE CAA2 WIZARDS WILL ADD CODE HERE
WIZARD_LINK_MODULES =  \
JS0GROUP \
ON0MAIN  \
ON0GREXT  \
CATFunctSystemUIUUID 
# END WIZARD EDITION ZONE

LINK_WITH=$(LINK_WITH_FOR_IID)  $(WIZARD_LINK_MODULES) \
    ApplicationFrame \
    CATFunctSystem \
    CATMathematics CATMathStream \
    CATObjectSpecsModeler \
    CATViz CATVisualization CATFunctSystemItf

# System dependant variables
#
OS = AIX
#
OS = HP-UX
#
OS = IRIX
#
OS = SunOS
#
OS = Windows_NT
