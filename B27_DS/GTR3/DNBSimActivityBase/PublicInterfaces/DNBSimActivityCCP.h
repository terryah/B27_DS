#ifndef DNBSimActivityCCP_H
#define DNBSimActivityCCP_H

/**
 * @quickReview mli 02:01:31
 */

/* -*-c++-*- */
//======================================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2001
//======================================================================================
//
//  Class DNBSimActivityCCP.h:
//
//======================================================================================
//  Usage:
//
//  Modification History:
//     
//      cre     dcg     06/01/2001      original implementation
//      mod     mli     08/20/2001      Updated header
//      mod     dcg     01/31/2002      Fix RI 333555. Removed unused include in order to fix
//                                      problem with items and resources not being copied
//                                      when performing a drag and drop.
//      mod     rtl     02/22/2007      Move header to PublicInterfaces for CAA Exposure
//======================================================================================

/** 
* @CAA2Level L0
* @CAA2Usage U2 
*/

#include <CATSPPPhysicalActivityCCPExt.h>
#include <DNBSimActivityBase.h>

class CATListValCATUnicodeString;

class ExportedByDNBSimActivityBase DNBSimActivityCCP : public CATSPPPhysicalActivityCCPExt
{
    CATDeclareClass;
    
public:
    
    // Constructor and destructor
    
    DNBSimActivityCCP ()  { /* Do nothing */ }
    virtual ~DNBSimActivityCCP ()  { /* Do nothing */ }
    
    
    // Need to have this implementation so simulation activities 
    // do not have any children.  Simulation activities can only be
    // leaf nodes.  If they could have children things would be rather
    // confusing, especially when trying to perform a simulation.
    
    virtual ListOfVarBaseUnknown Paste (ListOfVarBaseUnknown& iObjectToCopy,
                                        ListOfVarBaseUnknown* iToCurObjects,
                                        const CATFormat* iAnImposedFormat=NULL);
};

#endif // DNBSimActivityCCP_H

