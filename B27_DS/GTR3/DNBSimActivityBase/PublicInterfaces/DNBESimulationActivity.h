/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

#ifndef DNBESimulationActivity_H
#define DNBESimulationActivity_H

/* -*-c++-*- */
//=============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2001
//=============================================================================
//
//  Class DNBESimulationActivity.h:
//
//=============================================================================
//  Usage:
//   Implemtation of the DNBISimulationActivity
//
//  Modification History:
//     
//      cre     mli     12/01/2000      original implementation
//      mod     mli     08/20/2001      Updated header
//      mod     mmh     09/18/2003      Migrate to BOA
//      mod     sha     12/16/2003      Add new methods: IsSimulationAuthorized
//                                      and GetSimulationType
//=============================================================================

#include <CATBaseUnknown.h>
#include <CATBooleanDef.h>
#include <DNBISimulationActivity.h>
#include <DNBSimActivityBase.h>

class ExportedByDNBSimActivityBase DNBESimulationActivity : 
                                                public DNBISimulationActivity
{
    CATDeclareKindOf;

public:
    
    DNBESimulationActivity( const CATString sSimType = "DNBSimulateOther");
    virtual ~DNBESimulationActivity();
    
    virtual boolean 
    IsSimulationAuthorized();

    virtual CATString
    GetSimulationType();

protected:

    CATString           _sSimType;
    DNBSimulationType   _eSimType;
    int                 _nIsSimAuthorized;
    boolean             _bDisableLicenseCheck;
};

#endif // DNBESimulationActivity_H









