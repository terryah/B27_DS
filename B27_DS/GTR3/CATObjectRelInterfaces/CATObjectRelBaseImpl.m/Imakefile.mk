# COPYRIGHT DASSAULT SYSTEMES 2003
#======================================================================
# Imakefile for module CATObjectRelBaseImpl.m
#======================================================================
#
#  Jun 2003  Creation: Code generated by the CAA wizard  s223254
#======================================================================
#
# SHARED LIBRARY 
#
BUILT_OBJECT_TYPE=SHARED LIBRARY 
 
# DO NOT EDIT :: THE CAA2 WIZARDS WILL ADD CODE HERE
WIZARD_LINK_MODULES = \
JS0GROUP JS0FM SystemUUID \
CATInteractiveInterfaces \
CATObjectModelerBase \
CATObjectSpecsModeler \
CATProductStructure1 \
CATWBS \
CATProductStructureInterfaces \


# END WIZARD EDITION ZONE

LINK_WITH = $(WIZARD_LINK_MODULES)

# System dependant variables
#
OS = AIX
#
OS = HP-UX
#
OS = IRIX
#
OS = SunOS
#
OS = Windows_NT
