#ifndef __TIE_DNBIAEKPAssignmentServices
#define __TIE_DNBIAEKPAssignmentServices

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIAEKPAssignmentServices.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIAEKPAssignmentServices */
#define declare_TIE_DNBIAEKPAssignmentServices(classe) \
 \
 \
class TIEDNBIAEKPAssignmentServices##classe : public DNBIAEKPAssignmentServices \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIAEKPAssignmentServices, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall GetAssignedRequirementName(CATIAActivity * iOperation, ItemAssignmentType iType, CATSafeArrayVariant *& oItemList); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIAEKPAssignmentServices(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall GetAssignedRequirementName(CATIAActivity * iOperation, ItemAssignmentType iType, CATSafeArrayVariant *& oItemList); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIAEKPAssignmentServices(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::GetAssignedRequirementName(CATIAActivity * iOperation, ItemAssignmentType iType, CATSafeArrayVariant *& oItemList) \
{ \
return (ENVTIECALL(DNBIAEKPAssignmentServices,ENVTIETypeLetter,ENVTIELetter)GetAssignedRequirementName(iOperation,iType,oItemList)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIAEKPAssignmentServices,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIAEKPAssignmentServices,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIAEKPAssignmentServices,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIAEKPAssignmentServices,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIAEKPAssignmentServices,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIAEKPAssignmentServices(classe)    TIEDNBIAEKPAssignmentServices##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIAEKPAssignmentServices(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIAEKPAssignmentServices, classe) \
 \
 \
CATImplementTIEMethods(DNBIAEKPAssignmentServices, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIAEKPAssignmentServices, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIAEKPAssignmentServices, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIAEKPAssignmentServices, classe) \
 \
HRESULT __stdcall  TIEDNBIAEKPAssignmentServices##classe::GetAssignedRequirementName(CATIAActivity * iOperation, ItemAssignmentType iType, CATSafeArrayVariant *& oItemList) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&iOperation,&iType,&oItemList); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAssignedRequirementName(iOperation,iType,oItemList); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&iOperation,&iType,&oItemList); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAEKPAssignmentServices##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAEKPAssignmentServices##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAEKPAssignmentServices##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAEKPAssignmentServices##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAEKPAssignmentServices##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIAEKPAssignmentServices(classe) \
 \
 \
declare_TIE_DNBIAEKPAssignmentServices(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAEKPAssignmentServices##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAEKPAssignmentServices,"DNBIAEKPAssignmentServices",DNBIAEKPAssignmentServices::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAEKPAssignmentServices(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIAEKPAssignmentServices, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAEKPAssignmentServices##classe(classe::MetaObject(),DNBIAEKPAssignmentServices::MetaObject(),(void *)CreateTIEDNBIAEKPAssignmentServices##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIAEKPAssignmentServices(classe) \
 \
 \
declare_TIE_DNBIAEKPAssignmentServices(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAEKPAssignmentServices##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAEKPAssignmentServices,"DNBIAEKPAssignmentServices",DNBIAEKPAssignmentServices::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAEKPAssignmentServices(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIAEKPAssignmentServices, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAEKPAssignmentServices##classe(classe::MetaObject(),DNBIAEKPAssignmentServices::MetaObject(),(void *)CreateTIEDNBIAEKPAssignmentServices##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIAEKPAssignmentServices(classe) TIE_DNBIAEKPAssignmentServices(classe)
#else
#define BOA_DNBIAEKPAssignmentServices(classe) CATImplementBOA(DNBIAEKPAssignmentServices, classe)
#endif

#endif
