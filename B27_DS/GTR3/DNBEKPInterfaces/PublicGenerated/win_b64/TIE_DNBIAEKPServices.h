#ifndef __TIE_DNBIAEKPServices
#define __TIE_DNBIAEKPServices

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIAEKPServices.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIAEKPServices */
#define declare_TIE_DNBIAEKPServices(classe) \
 \
 \
class TIEDNBIAEKPServices##classe : public DNBIAEKPServices \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIAEKPServices, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall AssignER(CATBaseDispatch * iGeometricFeature, CATBaseDispatch * iFTA, CATIAActivity * iOperation, ItemAssignmentType iType); \
      virtual HRESULT __stdcall AssignERWithFTA(CATBaseDispatch * iFTA, CATIAActivity * iOperation, ItemAssignmentType iType); \
      virtual HRESULT __stdcall RemoveERAssignment(CATBaseDispatch * iGeometricFeature, CATBaseDispatch * iFTA, CATIAActivity * iOperation, ItemAssignmentType iType); \
      virtual HRESULT __stdcall RemoveERWithFTA(CATBaseDispatch * iFTA, CATIAActivity * iOperation, ItemAssignmentType iType); \
      virtual HRESULT __stdcall GetAssignedER(CATIAActivity * iOperation, ItemAssignmentType iType, CATSafeArrayVariant *& oItemList); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIAEKPServices(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall AssignER(CATBaseDispatch * iGeometricFeature, CATBaseDispatch * iFTA, CATIAActivity * iOperation, ItemAssignmentType iType); \
virtual HRESULT __stdcall AssignERWithFTA(CATBaseDispatch * iFTA, CATIAActivity * iOperation, ItemAssignmentType iType); \
virtual HRESULT __stdcall RemoveERAssignment(CATBaseDispatch * iGeometricFeature, CATBaseDispatch * iFTA, CATIAActivity * iOperation, ItemAssignmentType iType); \
virtual HRESULT __stdcall RemoveERWithFTA(CATBaseDispatch * iFTA, CATIAActivity * iOperation, ItemAssignmentType iType); \
virtual HRESULT __stdcall GetAssignedER(CATIAActivity * iOperation, ItemAssignmentType iType, CATSafeArrayVariant *& oItemList); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIAEKPServices(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::AssignER(CATBaseDispatch * iGeometricFeature, CATBaseDispatch * iFTA, CATIAActivity * iOperation, ItemAssignmentType iType) \
{ \
return (ENVTIECALL(DNBIAEKPServices,ENVTIETypeLetter,ENVTIELetter)AssignER(iGeometricFeature,iFTA,iOperation,iType)); \
} \
HRESULT __stdcall  ENVTIEName::AssignERWithFTA(CATBaseDispatch * iFTA, CATIAActivity * iOperation, ItemAssignmentType iType) \
{ \
return (ENVTIECALL(DNBIAEKPServices,ENVTIETypeLetter,ENVTIELetter)AssignERWithFTA(iFTA,iOperation,iType)); \
} \
HRESULT __stdcall  ENVTIEName::RemoveERAssignment(CATBaseDispatch * iGeometricFeature, CATBaseDispatch * iFTA, CATIAActivity * iOperation, ItemAssignmentType iType) \
{ \
return (ENVTIECALL(DNBIAEKPServices,ENVTIETypeLetter,ENVTIELetter)RemoveERAssignment(iGeometricFeature,iFTA,iOperation,iType)); \
} \
HRESULT __stdcall  ENVTIEName::RemoveERWithFTA(CATBaseDispatch * iFTA, CATIAActivity * iOperation, ItemAssignmentType iType) \
{ \
return (ENVTIECALL(DNBIAEKPServices,ENVTIETypeLetter,ENVTIELetter)RemoveERWithFTA(iFTA,iOperation,iType)); \
} \
HRESULT __stdcall  ENVTIEName::GetAssignedER(CATIAActivity * iOperation, ItemAssignmentType iType, CATSafeArrayVariant *& oItemList) \
{ \
return (ENVTIECALL(DNBIAEKPServices,ENVTIETypeLetter,ENVTIELetter)GetAssignedER(iOperation,iType,oItemList)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIAEKPServices,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIAEKPServices,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIAEKPServices,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIAEKPServices,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIAEKPServices,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIAEKPServices(classe)    TIEDNBIAEKPServices##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIAEKPServices(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIAEKPServices, classe) \
 \
 \
CATImplementTIEMethods(DNBIAEKPServices, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIAEKPServices, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIAEKPServices, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIAEKPServices, classe) \
 \
HRESULT __stdcall  TIEDNBIAEKPServices##classe::AssignER(CATBaseDispatch * iGeometricFeature, CATBaseDispatch * iFTA, CATIAActivity * iOperation, ItemAssignmentType iType) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&iGeometricFeature,&iFTA,&iOperation,&iType); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->AssignER(iGeometricFeature,iFTA,iOperation,iType); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&iGeometricFeature,&iFTA,&iOperation,&iType); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEKPServices##classe::AssignERWithFTA(CATBaseDispatch * iFTA, CATIAActivity * iOperation, ItemAssignmentType iType) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&iFTA,&iOperation,&iType); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->AssignERWithFTA(iFTA,iOperation,iType); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&iFTA,&iOperation,&iType); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEKPServices##classe::RemoveERAssignment(CATBaseDispatch * iGeometricFeature, CATBaseDispatch * iFTA, CATIAActivity * iOperation, ItemAssignmentType iType) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&iGeometricFeature,&iFTA,&iOperation,&iType); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->RemoveERAssignment(iGeometricFeature,iFTA,iOperation,iType); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&iGeometricFeature,&iFTA,&iOperation,&iType); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEKPServices##classe::RemoveERWithFTA(CATBaseDispatch * iFTA, CATIAActivity * iOperation, ItemAssignmentType iType) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&iFTA,&iOperation,&iType); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->RemoveERWithFTA(iFTA,iOperation,iType); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&iFTA,&iOperation,&iType); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEKPServices##classe::GetAssignedER(CATIAActivity * iOperation, ItemAssignmentType iType, CATSafeArrayVariant *& oItemList) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&iOperation,&iType,&oItemList); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAssignedER(iOperation,iType,oItemList); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&iOperation,&iType,&oItemList); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAEKPServices##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAEKPServices##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAEKPServices##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAEKPServices##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAEKPServices##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIAEKPServices(classe) \
 \
 \
declare_TIE_DNBIAEKPServices(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAEKPServices##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAEKPServices,"DNBIAEKPServices",DNBIAEKPServices::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAEKPServices(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIAEKPServices, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAEKPServices##classe(classe::MetaObject(),DNBIAEKPServices::MetaObject(),(void *)CreateTIEDNBIAEKPServices##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIAEKPServices(classe) \
 \
 \
declare_TIE_DNBIAEKPServices(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAEKPServices##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAEKPServices,"DNBIAEKPServices",DNBIAEKPServices::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAEKPServices(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIAEKPServices, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAEKPServices##classe(classe::MetaObject(),DNBIAEKPServices::MetaObject(),(void *)CreateTIEDNBIAEKPServices##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIAEKPServices(classe) TIE_DNBIAEKPServices(classe)
#else
#define BOA_DNBIAEKPServices(classe) CATImplementBOA(DNBIAEKPServices, classe)
#endif

#endif
