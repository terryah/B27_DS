//=================================================================== 
//  COPYRIGHT  Dassault  Systemes  2010
//=================================================================== 
#ifdef  _WINDOWS_SOURCE
#ifdef  __DNBEKPItf
#define ExportedByDNBEKPItf     __declspec(dllexport)
#else
#define ExportedByDNBEKPItf     __declspec(dllimport)
#endif
#else
#define ExportedByDNBEKPItf
#endif

/**  
*  @CAA2Level  L0
*  @CAA2Usage  U2  
*/ 

