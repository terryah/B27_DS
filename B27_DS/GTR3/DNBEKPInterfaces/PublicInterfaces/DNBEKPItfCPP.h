//=================================================================== 
//  COPYRIGHT  Dassault  Systemes  2010
//=================================================================== 
#ifdef  _WINDOWS_SOURCE
#ifdef  __DNBEKPItfCPP
#define ExportedByDNBEKPItfCPP     __declspec(dllexport)
#else
#define ExportedByDNBEKPItfCPP     __declspec(dllimport)
#endif
#else
#define ExportedByDNBEKPItfCPP
#endif

/**  
*  @CAA2Level  L0
*  @CAA2Usage  U2  
*/ 

