#ifndef CATIAHybridShapeCircle2PointsRad_IDL
#define CATIAHybridShapeCircle2PointsRad_IDL
/*IDLREP*/

// COPYRIGHT DASSAULT SYSTEMES 2000

/**
  * @CAA2Level L1
  * @CAA2Usage U3
  */


//=================================================================
//=================================================================
//           
// CATIAHybridShapeCircle:         
// Exposed interface for HybridShape Circle 2PointsRadius
//           
//=================================================================
// Usage notes:
//
//=================================================================
// Avril98 Creation
//=================================================================

#include "CATIAHybridShapeCircle.idl"
#include "CATIAReference.idl"

/**
 * Represents the hybrid shape circle object defined using two points and a radius.
 * <b>Role</b>: To access the data of the hybrid shape circle object.
 * <p>This data includes:
 * <ul>
 * <li>The circle two passing points</li>
 * <li>The circle radius</li>
 * <li>The surface that supports the circle</li>
 * <li>The circle orientation</li>
 * </ul>
 * <p>Use the CATIAHybridShapeFactory to create a HybridShapeCircle2PointsRad object.
 * @see CATIAHybridShapeFactory
 */
interface CATIAHybridShapeCircle2PointsRad : CATIAHybridShapeCircle
{
    //        =============
    //        == METHODS ==
    //        =============
 /**
  * Returns or sets the circle first passing point.
  * <br>Sub-element(s) supported (see @href CATIABoundary object): 
  * @href CATIAVertex.
  * <dl>
  * <dt><b>Example</b>:
  * <dd>
  * This example retrieves the first passing point
  * of the <code>HybShpCircle</code> hybrid shape circle
  * in <code>HybShpCircleFirstPassingPoint</code> point.
  * <pre>
  * Dim HybShpCircleFirstPassingPoint As Reference
  * Set HybShpCircleFirstPassingPoint = HybShpCircle.<font color="red">Pt1</font>
  * </pre>
  * </dl>
  */ 
#pragma PROPERTY Pt1
 HRESULT get_Pt1 (out /*IDLRETVAL*/ CATIAReference   oPt); 
 HRESULT put_Pt1 (in     CATIAReference   iPt);

 /**
  * Returns or sets the circle second passing point.
  * <br>Sub-element(s) supported (see @href CATIABoundary object): 
  * @href CATIAVertex.
  * <dl>
  * <dt><b>Example</b>:
  * <dd>
  * This example sets the second passing point
  * of the <code>HybShpCircle</code> hybrid shape circle
  * as the <code>Point12</code> point.
  * <pre>
  * HybShpCircle.<font color="red">Pt2</font> Point12
  * </pre>
  * </dl>
  */ 
#pragma PROPERTY Pt2
 HRESULT get_Pt2 (out /*IDLRETVAL*/ CATIAReference   oPt);
 HRESULT put_Pt2 (in     CATIAReference   iPt);

 /**
  * Returns or sets the circle support surface.
  * <br>Sub-element(s) supported (see @href CATIABoundary object): 
  * @href CATIAFace.
  * <dl>
  * <dt><b>Example</b>:
  * <dd>
  * This example retrieves in <code>HybShpCircleSupportSurf</code> the support surface
  * of the <code>HybShpCircle</code> hybrid shape circle.
  * <pre>
  * Dim HybShpCircleSupportSurf As Reference 
  * HybShpCircleSupportSurf = HybShpCircle.<font color="red">Support</font>
  * </pre>
  * </dl>
  */ 
#pragma PROPERTY Support
 HRESULT get_Support (out /*IDLRETVAL*/ CATIAReference   oSupport);
 HRESULT put_Support (in                CATIAReference   iSupport);

 /**
  * Returns or sets the circle orientation.
  * <br><b>Role</b>: The circle orientation indicates which side of the line made up
  * using the two passing points is used to create the major part of the circle.
  * It is determined using the cross product of the normal to the suppport
  * and the vector made up using the two passing points (Pt1-Pt2).
  * <br><b>Legal values</b>: 1 to state that the major part of the circle is or
  * should be created on the side of the line shown by the vector resulting from this
  * cross product, and -1 otherwise.
  * <dl>
  * <dt><b>Example</b>:
  * <dd>
  * This example retrieves in <code>HybShpCircleOrientation</code> the orientation
  * of the <code>HybShpCircle</code> hybrid shape circle.
  * <pre>
  * HybShpCircleOrientation = HybShpCircle.<font color="red">Orientation</font>
  * </pre>
  * </dl>
  */ 
#pragma PROPERTY Orientation
 HRESULT get_Orientation (out /*IDLRETVAL*/ long   oOri);
 HRESULT put_Orientation (in                long   iOri);

 /**
  *  Returns or sets the DiameterMode.
  * <br><b>Legal values</b>: <b>True</b> implies diameter
  * <b>False</b> implies radius (default).
  *  When DiameterMode is changed, Radius/Diameter value, which is stored will not be modified.
  *  @sample
  *  This example sets that the DiameterMode of
  *  the <code>HybShpCircle</code> hybrid shape circle feature
  *  <pre>
  *  HybShpCircle.<font color="red">DiameterMode</font> = True
  *  </pre>
  */ 

#pragma PROPERTY DiameterMode
 HRESULT get_DiameterMode (out /*IDLRETVAL*/ boolean oDiameterMode);
 HRESULT put_DiameterMode (in         boolean iDiameterMode);

 /**
  * Returns the circle radius.
  * @param Radius 
  *   The circle radius, expressed as a @href CATIALength literal. Succeeds only if DiameterMode is set to False.
  * </dl>
  * <dt><b>Example</b>:
  * <dd>
  * This example retrieves in <code>HybShpCircleRadius</code> the radius
  * of the <code>HybShpCircle</code> hybrid shape circle.
  * <pre>
  * Dim HybShpCircleRadius As Length
  * HybShpCircleRadius = HybShpCircle.<font color="red">Radius</font>
  * </pre>
  * </dl>
  */ 
#pragma PROPERTY Radius
    HRESULT get_Radius   ( out /*IDLRETVAL*/ CATIALength    oRadius );
  
  /**
  * Returns the circle diameter.
  * It is expressed as a @href CATIALength literal. Succeeds only if DiameterMode is set to True.
  * </dl>
  * <dt><b>Example</b>:
  * <dd>
  * This example retrieves in <code>HybShpCircleDiameter</code> the diameter
  * of the <code>HybShpCircle</code> hybrid shape circle feature
  * <pre>
  * Dim HybShpCircleDiameter As Length
  * HybShpCircleDiameter = HybShpCircle.<font color="red">Diameter</font>
  * </pre>
  * </dl>
  */
#pragma PROPERTY Diameter
 HRESULT get_Diameter (out /*IDLRETVAL*/ CATIALength oDiameter);

 /**
  * Queries whether the circle is geodesic or not.
  *   @param oGeod 
  *      geodesic type : when TRUE, the circle is geodesic.
  */
  HRESULT IsGeodesic(out /*IDLRETVAL*/ boolean  oGeod);
 /**
  * Sets GeometryOnSupport of circle. 
  * <br> It puts the circle on the surface.
  *      S_OK if OK, E_FAIL if fail
  */
  HRESULT SetGeometryOnSupport();

 /**
  * Inactivates GeometryOnSupport of circle. 
  * <br>Note: The circle becomes euclidean.
  */
  HRESULT UnsetGeometryOnSupport();
};

// Interface name : CATIAHybridShapeCircle2PointsRad
#pragma ID CATIAHybridShapeCircle2PointsRad "DCE:8d7e9c80-934f-0000-0280020e60000000"
#pragma DUAL CATIAHybridShapeCircle2PointsRad


// VB object name : HybridShapeCircle2PointsRad (Id used in Visual Basic)
#pragma ID HybridShapeCircle2PointsRad "DCE:8d7e9c85-8707-0000-0280020e60000000"
#pragma ALIAS CATIAHybridShapeCircle2PointsRad HybridShapeCircle2PointsRad
#endif


