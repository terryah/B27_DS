/* -*-c++-*- */
#ifndef CATIAHybridShapePointBetween_IDL
#define CATIAHybridShapePointBetween_IDL
/*IDLREP*/

// COPYRIGHT DASSAULT SYSTEMES 2000

/**
  * @CAA2Level L1
  * @CAA2Usage U3
  */

//=================================================================
//           
// CATIAHybridShapePointBetween:         
// Exposed interface for HybridShapePointBetween
//           
// Janvier 2000 Creation
//=================================================================
#include "CATIAHybridShapePoint.idl"
#include "CATIAReference.idl"
#include "CATIALength.idl"
#include "CATIARealParam.idl"

/**
* Represents the hybrid shape PointBetween feature object.
* <b>Role</b>: To access the data of the hybrid shape PointBetween feature object.<BR>
* This data includes:
*<ul>
*<li>The first reference point
*<li>The second reference point
*</ul>
* @see CATIAHybridShapeFactory 
*/

//-----------------------------------------------------------------
interface CATIAHybridShapePointBetween : CATIAHybridShapePoint
{

 /**
 * Returns or sets the first reference point.
 * <br>Sub-element(s) supported (see @href CATIABoundary object): 
 * @href CATIAVertex.
 *<dl>
 *<dt><b>Example</b>:
 *<dd>
 * This example retrieves in <code>RefPoint1</code> the first reference point for the
 *<code>PointBetween</code> hybrid shape feature.
 *<pre>
 * Dim RefPoint1 As Reference
 * Set RefPoint1 = PointBetween.<font color="red">FirstPoint</font>
 *</pre>
 *</dl>
 */ 
#pragma PROPERTY FirstPoint
 HRESULT get_FirstPoint ( out /*IDLRETVAL*/  CATIAReference   oPt1);
 HRESULT put_FirstPoint ( in           CATIAReference   iPt1);




 /**
 * Returns or sets the second reference point.
 * <br>Sub-element(s) supported (see @href CATIABoundary object): 
 * @href CATIAVertex.
 *<dl>
 *<dt><b>Example</b>:
 *<dd>
 * This example retrieves in <code>RefPoint2</code> the second reference point for the
 *<code>PointBetween</code> hybrid shape feature.
 *<pre>
 * Dim RefPoint2 As Reference
 * Set RefPoint2 = PointBetween.<font color="red">SecondPoint</font>
 *</pre>
 *</dl>
 */ 
#pragma PROPERTY SecondPoint
 HRESULT get_SecondPoint ( out /*IDLRETVAL*/  CATIAReference   oPt2);
 HRESULT put_SecondPoint ( in           CATIAReference   iPt2);




 /**
 *  Get the ratio.
 * <b>Role</b>: <br>
 *   if  d1 is the distance between the first point and the created point,
 *   and d2 is the distance between the first point and the second point,
 *   then ratio = d1/d2.
 * <dl>
 * <dt><b>Example</b>:
 * <dd>
 * This example retrieves in <code>ratio</code> the orientation for the <code>PointBetween</code> hybrid shape feature.
 *<pre>
 * Dim ratio  As CATIARealParam
 * Get ratio = PointBetween.<font color="red">Ratio</font>
 * </pre>
 * </dl> 
 */ 
#pragma PROPERTY Ratio
 HRESULT get_Ratio ( out /*IDLRETVAL*/  CATIARealParam   oRatio);




 /**
 * Returns or sets the orientation.
 * <b>Role</b>:<br>
 *  Orientation = 1 means that distance is measured from the second point
 * <dl>
 * <dt><b>Example</b>:
 * <dd>
 * This example retrieves in <code>Orient</code> the orientation for the <code>PointBetween</code> hybrid shape feature.
 *<pre>
 * Dim Orient As long
 * Set Orient = PointBetween.<font color="red">Orientation</font>
 * </pre>
 * </dl> 
 */
#pragma PROPERTY Orientation 
 HRESULT get_Orientation (out /*IDLRETVAL*/  long       oOrientation);
 HRESULT put_Orientation (in     long       iOrientation);

 /**
 * Returns or Sets the support.
 * <br> Note: the support can be surface or curve. It is not mandatory<br>
 * <br>Sub-element(s) supported (see @href CATIABoundary object): 
 * @href CATIAFace and @href CATIATriDimFeatEdge and @href CATIABiDimFeatEdge.
 * <dl>
 * <dt><b>Example</b>:
 * <dd>
 * This example retrieves in <code>oSupport</code> the support(if it exist)  
 * for the <code>PointBetween</code> hybrid shape feature.
 * <pre>
 * Dim oSupport As Reference 
 * Set oSupport = PointBetween.<font color="red">Support</font>
 * </pre>
 * </dl>
 */ 
#pragma PROPERTY Support
 HRESULT get_Support ( out /*IDLRETVAL*/  CATIAReference   oSupport);
 HRESULT put_Support ( in           CATIAReference   iSupport);
};

// Interface name : CATIAHybridShapePointBetween
#pragma ID CATIAHybridShapePointBetween "DCE:44a43790-ea99-11d3-84ef0000863e1bce"            
#pragma DUAL CATIAHybridShapePointBetween

// VB object name : HybridShapePointBetween (Id used in Visual Basic)
#pragma ID HybridShapePointBetween "DCE:9c99f290-ea99-11d3-84ef0000863e1bce"
#pragma ALIAS CATIAHybridShapePointBetween HybridShapePointBetween
#endif

// CATIAHybridShapePointBetween_IDL         


