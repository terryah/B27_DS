/* -*-c++-*- */
#ifndef CATIAHybridShapeLineNormal_IDL
#define CATIAHybridShapeLineNormal_IDL
/*IDLREP*/

// COPYRIGHT DASSAULT SYSTEMES 2000

/**
  * @CAA2Level L1
  * @CAA2Usage U3
  */


//=================================================================
//           
// CATIAHybridShapeLineNormal:         
// Exposed interface for HybridShapeLineNormal
//           
// Janvier 2000 Creation
//=================================================================
#include "CATIAHybridShapeLine.idl"
#include "CATIALength.idl"
#include "CATIAReference.idl"


/**
 * Line defined by a point and a direction normal to a surface.
 * <b>Role</b>: To access data of the line feature created by using 
 * the normal direction of a surface
 * <p>Use the  CATIAHybridShapeFactory to create a HybridShapeLineNormal object.
 * @see CATIAReference 
 * @see CATIALength 
 * @see CATIAHybridShapeDirection 
 * @see CATIAHybridShapeFactory
 */


//-----------------------------------------------------------------
interface CATIAHybridShapeLineNormal : CATIAHybridShapeLine
{





 /**
 * Returns or Sets the starting point of the line.
 * <br>Sub-element(s) supported (see @href CATIABoundary object): 
 * @href CATIAVertex.
 * <dl>
 * <dt><b>Example</b>:
 * <dd>
 * This example retrieves in <code>oPoint</code> the starting point    
 * for the <code>LineNormal</code> hybrid shape feature.
 * <pre>
 * Dim oPoint As Reference
 * Set oPoint = LineNormal.<font color="red">Point</font>
 * </pre>
 * </dl>
 */ 

#pragma PROPERTY Point
 HRESULT get_Point ( out /*IDLRETVAL*/  CATIAReference   oPoint);
 HRESULT put_Point ( in           CATIAReference   iPoint);



 /**
 * Returns or Sets the surface to which the line will be normal.
 * <br>Sub-element(s) supported (see @href CATIABoundary object): 
 * @href CATIAFace.
 * <dl>
 * <dt><b>Example</b>:
 * <dd>
 * This example retrieves in <code>oSurface</code> the surface 
 * for the <code>LineNormal</code> hybrid shape feature.
 * <pre>
 * Dim oSurface As Reference 
 * Set oSurface = LineNormal.<font color="red">Surface</font>
 * </pre>
 * </dl>
 */ 
#pragma PROPERTY Surface
 HRESULT get_Surface ( out /*IDLRETVAL*/  CATIAReference   oSurface);
 HRESULT put_Surface ( in           CATIAReference   iSurface);


 /**
 * Returns or Sets the line orientation.
 * <br> Orientation allows to reverse the line direction from the reference point.
 * <br> For a line of L length, it is the same as creating this line with -L length :
 *  Orientation : can be 1 or -1
 * <dl>
 * <dt><b>Example</b>:
 * <dd>
 * This example retrieves in <code>oOrientation</code> the starting length   
 * for the <code>LineNormal</code> hybrid shape feature.
 * <pre>
 * Dim oOrientation As long
 * Set oOrientation = LineNormal.<font color="red">Orientation</font>
 * </pre>
 * </dl>
 */ 
#pragma PROPERTY Orientation
 HRESULT get_Orientation (out /*IDLRETVAL*/  long       oOrientation);
 HRESULT put_Orientation (in       long       iOrientation);



 /**
 * Returns the start length of the line.
 * <br> Start length : extension of the line, beginning at the starting point
 * <dl>
 * <dt><b>Example</b>:
 * <dd>
 * This example retrieves in <code>oStart</code> the beginning offset length   
 * for the <code>LineNormal</code> hybrid shape feature.
 * <pre>
 * Dim oStart As  CATIALength 
 * Set oStart = LineNormal.<font color="red">BeginOffset</font>
 * </pre>
 * </dl>
 */ 
#pragma PROPERTY BeginOffset
 HRESULT get_BeginOffset ( out /*IDLRETVAL*/  CATIALength   oStart);

 /**
 * Returns the end length of the line.
 * <br> End length : extension of the line, beginning at the ending point
 * <dl>
 * <dt><b>Example</b>:
 * <dd>
 * This example retrieves in <code>oEnd</code> the starting length   
 * for the <code>LineNormal</code> hybrid shape feature.
 * <pre>
 * Dim oEnd As  CATIALength 
 * Set oEnd = LineNormal.<font color="red">EndOffset</font>
 * </pre>
 * </dl>
 */ 
#pragma PROPERTY EndOffset
 HRESULT get_EndOffset ( out /*IDLRETVAL*/  CATIALength   oEnd);

 //  ================
 //  == METHODS ==
 //  ================
 /**
 * Gets the length type
 * Default is 0.
 *   @param oType
 *   The length type = 0 : length               - the line is limited by its extremities
 *                   = 1 : infinite             - the line is infinite
 *                   = 2 : infinite start point - the line is infinite on the side of the start point
 *                   = 3 : infinite end point   - the line is infinite on the side of the end point
 */
 HRESULT GetLengthType (out /*IDLRETVAL*/  long  oType);
 /**
 * Sets the length type
 * Default is 0.
 *   @param iType
 *   The length type = 0 : length               - the line is limited by its extremities
 *                   = 1 : infinite             - the line is infinite
 *                   = 2 : infinite start point - the line is infinite on the side of the start point
 *                   = 3 : infinite end point   - the line is infinite on the side of the end point
 */
 HRESULT SetLengthType(in long iType);
 /**
 * Sets the symmetrical extension of the line (start = -end).
 *   @param iSym
 *       Symetry flag  
 */
 HRESULT SetSymmetricalExtension(in boolean iSym);

 /**
 * Gets whether the symmetrical extension of the line is active.
 *   @param oSym
 *       Symetry flag  
 */
 HRESULT GetSymmetricalExtension(out /*IDLRETVAL*/ boolean oSym);

};


// Interface name : CATIAHybridShapeLineNormal
#pragma ID CATIAHybridShapeLineNormal "DCE:89d765f1-e7bb-0000-0280020e60000000"
#pragma DUAL CATIAHybridShapeLineNormal

// VB object name : HybridShapeLineNormal (Id used in Visual Basic)
#pragma ID HybridShapeLineNormal "DCE:89d768ee-cd51-0000-0280020e60000000"
#pragma ALIAS CATIAHybridShapeLineNormal HybridShapeLineNormal


#endif


// CATIAHybridShapeLineNormal_IDL
