#ifndef CATIAHybridShapeCircleTritangent_IDL
#define CATIAHybridShapeCircleTritangent_IDL
/*IDLREP*/

// COPYRIGHT DASSAULT SYSTEMES 2000

/**
  * @CAA2Level L1
  * @CAA2Usage U3
  */


//=================================================================
//=================================================================
//           
// CATIAHybridShapeCircle:         
// Exposed interface for HybridShape Circle Tritangent
//           
//=================================================================
// Usage notes:
//
//=================================================================
// Avril98 Creation
//=================================================================

#include "CATIAHybridShapeCircle.idl"

/**
 * Represents the hybrid shape circle object tangent to three curves.
 * <b>Role</b>: To access the data of the hybrid shape circle object.
 * <p>This data includes:
 * <ul>
 * <li>The three curves to which the circle is tangent</li>
 * <li>The surface that supports the circle</li>
 * <li>The orientation of each curve</li>
 * </ul>
 * <p>Use the CATIAHybridShapeFactory to create a HybridShapeCircleTritangent object.
 * @see CATIAHybridShapeFactory
 */
interface CATIAHybridShapeCircleTritangent : CATIAHybridShapeCircle
{

 /**
  * Returns or sets the first curve to which the circle is or will be tangent.
  * <br>Sub-element(s) supported (see @href CATIABoundary object): 
  * @href CATIATriDimFeatEdge or @href CATIABiDimFeatEdge.
  * <dl>
  * <dt><b>Example</b>:
  * <dd>
  * This example retrieves in <code>HybShpCircleFirstCurve</code> the first curve to which
  * the <code>HybShpCircle</code> hybrid shape circle is tangent.
  * <pre>
  * Dim HybShpCircleFirstCurve As Reference 
  * HybShpCircleFirstCurve = HybShpCircle.<font color="red">Curve1</font>
  * </pre>
  * </dl>
  */ 
#pragma PROPERTY Curve1
 HRESULT get_Curve1 (out /*IDLRETVAL*/ CATIAReference   oCrv);
 HRESULT put_Curve1 (in                CATIAReference   iCrv);

 /**
  * Returns or sets the second curve to which the circle is or will be tangent.
  * <br>Sub-element(s) supported (see @href CATIABoundary object): 
  * @href CATIATriDimFeatEdge or @href CATIABiDimFeatEdge.
  * <dl>
  * <dt><b>Example</b>:
  * <dd>
  * This example sets the second curve to which
  * the <code>HybShpCircle</code> hybrid shape circle will be tangent to Crv5.
  * <pre>
  * HybShpCircle.<font color="red">Curve2</font> Crv5
  * </pre>
  * </dl>
  */
#pragma PROPERTY Curve2
 HRESULT get_Curve2 (out /*IDLRETVAL*/ CATIAReference   oCrv);
 HRESULT put_Curve2 (in                CATIAReference   iCrv);

 /**
  * Returns or sets the thirs curve to which the circle is or will be tangent.
  * <br>Sub-element(s) supported (see @href CATIABoundary object): 
  * @href CATIATriDimFeatEdge or @href CATIABiDimFeatEdge.
  * <dl>
  * <dt><b>Example</b>:
  * <dd>
  * This example retrieves in <code>HybShpCircleThirsCurve</code> the third curve to which
  * the <code>HybShpCircle</code> hybrid shape circle is tangent.
  * <pre>
  * Dim HybShpCircleThirdCurve As Reference 
  * HybShpCircleThirdCurve = HybShpCircle.<font color="red">Curve3</font>
  * </pre>
  * </dl>
  */ 
#pragma PROPERTY Curve3
 HRESULT get_Curve3 (out /*IDLRETVAL*/ CATIAReference   oCrv);
 HRESULT put_Curve3 (in                CATIAReference   iCrv);

 /**
  * Returns or sets the circle support surface.
  * <br>Sub-element(s) supported (see @href CATIABoundary object): 
  * @href CATIAFace.
  * <dl>
  * <dt><b>Example</b>:
  * <dd>
  * This example retrieves in <code>HybShpCircleSupportSurf</code> the support surface
  * of the <code>HybShpCircle</code> hybrid shape circle.
  * <pre>
  * Dim HybShpCircleSupportSurf As Reference 
  * HybShpCircleSupportSurf = HybShpCircle.<font color="red">Support</font>
  * </pre>
  * </dl>
  */ 
#pragma PROPERTY Support
 HRESULT get_Support (out /*IDLRETVAL*/ CATIAReference   oSupport);
 HRESULT put_Support (in                CATIAReference   iSupport);

 /**
  * Returns or sets the orientation of the first curve to which the circle is tangent.
  * <br><b>Role</b>: The orientation of the first curve determines the side of this curve
  * taken into account to find the point where the circle is tangent to the curve.
  * This side is determined by the cross product of the normal to the support and
  * a tangent to the curve oriented using the curve orientation. 
  * <br><b>Legal values</b>: 1 to state that the side of the curve to be taken into
  * account is the side shown by the vector resulting from this
  * cross product, and -1 otherwise.
  * <dl>
  * <dt><b>Example</b>:
  * <dd>
  * This example sets the orientation of the first curve to which
  * the <code>HybShpCircle</code> hybrid shape circle is tangent
  * to reverse.
  * <pre>
  * HybShpCircle.<font color="red">Orientation1</font> -1
  * </pre>
  * </dl>
  */
#pragma PROPERTY Orientation1
 HRESULT get_Orientation1 (out /*IDLRETVAL*/ long   oOri);
 HRESULT put_Orientation1 (in                long   iOri);

 /**
  * Returns or sets the orientation of the second curve to which the circle is tangent.
  * <br><b>Role</b>: The orientation of the second curve determines the side of this curve
  * taken into account to find the point where the circle is tangent to the curve.
  * This side is determined by the cross product of the normal to the support and
  * a tangent to the curve oriented using the curve orientation. 
  * <br><b>Legal values</b>: 1 to state that the side of the curve to be taken into
  * account is the side shown by the vector resulting from this
  * cross product, and -1 otherwise.
  * <dl>
  * <dt><b>Example</b>:
  * <dd>
  * This example retrieves in <code>HybShpCircleOrientation</code> the orientation
  * of the second curve to which the <code>HybShpCircle</code> hybrid shape circle is tangent.
  * <pre>
  * HybShpCircleOrientation = HybShpCircle.<font color="red">Orientation2</font>
  * </pre>
  * </dl>
  */
#pragma PROPERTY Orientation2
 HRESULT get_Orientation2 (out /*IDLRETVAL*/ long   oOri);
 HRESULT put_Orientation2 (in                long   iOri);

 /**
  * Returns or sets the orientation of the third curve to which the circle is tangent.
  * <br><b>Role</b>: The orientation of the third curve determines the side of this curve
  * taken into account to find the point where the circle is tangent to the curve.
  * This side is determined by the cross product of the normal to the support and
  * a tangent to the curve oriented using the curve orientation. 
  * <br><b>Legal values</b>: 1 to state that the side of the curve to be taken into
  * account is the side shown by the vector resulting from this
  * cross product, and -1 otherwise.
  * <dl>
  * <dt><b>Example</b>:
  * <dd>
  * This example sets the orientation of the third curve to which
  * the <code>HybShpCircle</code> hybrid shape circle is tangent
  * to reverse.
  * <pre>
  * HybShpCircle.<font color="red">Orientation3</font> -1
  * </pre>
  * </dl>
  */
#pragma PROPERTY Orientation3
 HRESULT get_Orientation3 (out /*IDLRETVAL*/ long   oOri);
 HRESULT put_Orientation3 (in                long   iOri);

/**
 * Return or set the discrimination index of the current circle.
 * Several resulting solutions produced by the operator can be same 
 * oriented regarding to the input wire bodies. 
 * In such a case, they are sorted in order to distinguish them.
 * The Sequence FirstOrientation - SecondOrientation - DiscriminationIndex 
 * allows you to identifie a unique one-domain solution.
 * @sample
 * This example set the discrimination index of
 * the <code>hybShpcircle</code> hybrid shape circle 
 * <pre>
 * hybShpcircle.<font color="red">DiscriminationIndex</font> = 2
 * </pre>
 */ 
#pragma PROPERTY DiscriminationIndex
    HRESULT get_DiscriminationIndex (out /*IDLRETVAL*/ long oDiscriminationIndex) ;
    HRESULT put_DiscriminationIndex (in                long iDiscriminationIndex) ;

  /**
  * Return or Set the number of the beginning curve of the circle.
  * This parameter is used to stabilize the resulting circle
  * @sample
  * This example set the beginning wire index of
  * the <code>hybShpcircle</code> hybrid shape circle 
  * <pre>
  * hybShpcircle.<font color="red">BeginOfCircle</font> = 1
  * </pre>
  */
#pragma PROPERTY BeginOfCircle
    HRESULT get_BeginOfCircle (out /*IDLRETVAL*/ long oNumWireBegin) ;
    HRESULT put_BeginOfCircle (in                long iNumWireBegin) ;

  /**
  * Returns or sets the tangent orientation of the circle first reference element.
  * compared to the circle itself
  * @sample
  * This example retrieves the tangent orientation of first reference element of
  * the <code>hybShpcircle</code> hybrid shape circle in <code>firstOrient</code>.
  * <pre>
  * Dim firstOrient As long
  * firstOrient = hybShpcircle.<font color="red">FirstTangentOrientation</font>
  * </pre>
  */
#pragma PROPERTY TangentOrientation1
    HRESULT get_TangentOrientation1 (out /*IDLRETVAL*/ long oOrientation) ;
    HRESULT put_TangentOrientation1 (in                long iOrientation) ;

  /**
  * Returns or sets the tangent orientation of the circle second reference element.
  * compared to the corner itself
  * @sample
  * This example retrieves the tangent orientation of second reference element of
  * the <code>hybShpcircle</code> hybrid shape circle in <code>secondOrient</code>.
  * <pre>
  * Dim secondOrient As long
  * secondOrient = hybShpcircle.<font color="red">SecondTangentOrientation</font>
  * </pre>
  */
#pragma PROPERTY TangentOrientation2
    HRESULT get_TangentOrientation2 (out /*IDLRETVAL*/ long oOrientation) ;
    HRESULT put_TangentOrientation2 (in                long iOrientation) ;

  /**
  * Returns or sets the tangent orientation of the circle third reference element.
  * compared to the corner itself
  * @sample
  * This example retrieves the tangent orientation of third reference element of
  * the <code>hybShpcircle</code> hybrid shape circle in <code>thirdOrient</code>.
  * <pre>
  * Dim thirdOrient As long
  * thirdOrient = hybShpcircle.<font color="red">ThirdTangentOrientation</font>
  * </pre>
  */
#pragma PROPERTY TangentOrientation3
    HRESULT get_TangentOrientation3 (out /*IDLRETVAL*/ long oOrientation) ;
    HRESULT put_TangentOrientation3 (in                long iOrientation) ;

  /**
  * Returns or sets whether the circle reference curves are or should be trimmed.
  * <br><b>Legal values</b>: 
  * <b>0</b> if the circle reference curves are not or should not be trimmed,
  * <b>1</b> if the circle reference curves are or should be trimmed,
  * <b>2</b> if only the first circle reference curve is or should be trimmed,
  * <b>3</b> if only the second circle reference curve is or should be trimmed,
  * @sample
  * This example sets that the reference curves of
  * the <code>hybShpCircle</code> hybrid shape circle should be trimmed.
  * <pre>
  * hybShpCircle.<font color="red">TrimMode</font> = 1
  * </pre>
  */ 
#pragma PROPERTY TrimMode
    HRESULT get_TrimMode(out /*IDLRETVAL*/ long oTrim) ;
    HRESULT put_TrimMode(in                long iTrim) ;


};


// Interface name : CATIAHybridShapeCircleTritangent
#pragma ID CATIAHybridShapeCircleTritangent "DCE:8d7e9b7c-1d32-0000-0280020e60000000"
#pragma DUAL CATIAHybridShapeCircleTritangent

// VB object name : HybridShapeCircleTritangent (Id used in Visual Basic)
#pragma ID HybridShapeCircleTritangent "DCE:8d7e9b81-1056-0000-0280020e60000000"
#pragma ALIAS CATIAHybridShapeCircleTritangent HybridShapeCircleTritangent
#endif


