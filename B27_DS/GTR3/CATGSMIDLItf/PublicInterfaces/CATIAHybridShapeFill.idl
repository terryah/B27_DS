#ifndef CATIAHybridShapeFill_IDL
#define CATIAHybridShapeFill_IDL
/*IDLREP*/

// COPYRIGHT DASSAULT SYSTEMES 2000

/**
  * @CAA2Level L1
  * @CAA2Usage U3
  */


//=================================================================
//=================================================================
//           
// CATIAHybridShapeFill:         
// Exposed interface for HybridShape Fill
//           
//=================================================================
// Usage notes:
//
//=================================================================
// Avril98 Creation       
//=================================================================
#include "CATIAHybridShape.idl"
#include "CATIAReference.idl"

/**
 * The Fill feature : an Fill is made up of a face to process
 * and one Fill parameter.
 */

//-----------------------------------------------------------------
interface CATIAHybridShapeFill : CATIAHybridShape
{
 //  ================
 //  == PROPERTIES ==
 //  ================
// ON Fill

 /**
  * Returns or sets the Canonical portion detection option. 
  * <br><b>Legal values</b>:
  * <dl>
  * <dt>0</dt>
  * <dd>No detection of canonical surface</dd>
  * <dt>2</dt>
  * <dd>Detection of canonical surfaces</dd>
  * </dl>
  */
#pragma PROPERTY Detection
  HRESULT get_Detection (out /*IDLRETVAL*/ short oDetection);
  HRESULT put_Detection (in				short iDetection);

  /**
  * Returns or sets the continuity between the support and fill. 
  * <br><b>Legal values</b>:
  * <dl>
  * <dt>0</dt>
  * <dd>Continuity in point (C0)</dd>
  * <dt>1</dt>
  * <dd>Continuity in tangency (C1)</dd>
  * <dt>2</dt>
  * <dd>Continuity in curvature (C2)</dd>
  * </dl>
  * <dl>
  * <dt><b>Example</b>:
  * <dd>
  * This example retrieves in <code>oContinuity</code> the continuity type
  * for the <code>Fill</code> hybrid shape feature.
  * <pre>
  * Dim oContinuity
  * Set oContinuity = Fill.<font color="red">Continuity</font>
  * </pre>
  * </dl>
  */
#pragma PROPERTY Continuity
  HRESULT get_Continuity (out /*IDLRETVAL*/ long oContinuity);
  HRESULT put_Continuity (in				long iContinuity);

 /**
  * Returns or sets the passing point for the Fill. 
  * <dl>
  * <dt><b>Example</b>:
  * <dd>
  * This example retrieves in <code>Element</code> the passing point
  * for the <code>Fill</code> hybrid shape feature.
  * <pre>
  * Dim Element As Reference 
  * Set Element = Fill.<font color="red">Constraint</font>
  * </pre>
  * </dl>
  */
#pragma PROPERTY Constraint
  HRESULT get_Constraint (out /*IDLRETVAL*/ CATIAReference oConstraint);
  HRESULT put_Constraint (in                CATIAReference iConstraint);

 /**
  * Returns or sets whether Planar Boundaries only should be considered during fill operation.
  * <br><b>Legal values</b>:
  * <dl>
  * <dt>TRUE</dt>
  * <dd>Planar boundaries are only considered during Fill operation</dd>
  * <dt>FALSE</dt>
  * <dd>Non-Planar boundaries are also considered during Fill operation</dd>
  * </dl>
  */
#pragma PROPERTY PlaneOnlyMode
  HRESULT get_PlaneOnlyMode(out /*IDLRETVAL*/ boolean oPlaneOnly);
  HRESULT put_PlaneOnlyMode(in				  boolean iPlaneOnly);

/**
 * @deprecated V5R26 
 * Use @href CATIAHybridShapeFill#AdvancedTolerantMode
 * Returns or sets the Tolerant mode option.
 * <b>Role</b>: To activate or not the tolerant mode option 
 *      TRUE  : Tolerant mode is active. Uses deviation parameter to do tolerant fill.
 *      FALSE : Tolerant mode is not active.
 * <dl>
 * <dt><b>Example</b>:
 * This example retrieves in <code>tolMode</code> the tolerant mode for the
 * <code>Fill</code> hybrid shape feature.
 * <pre>
 * Dim tolMode As boolean
 * Set tolMode = Fill.<font color="red">TolerantMode</font>
 * </pre>
 * </dl>
 */ 
#pragma PROPERTY TolerantMode  
    HRESULT get_TolerantMode (out /*IDLRETVAL*/ boolean oTolerantMode);
    HRESULT put_TolerantMode (in                boolean iTolerantMode);


/**
 * Sets or Gets the maximum deviation allowed for smoothing operation in fill commnd.
 * This value must be set in SI unit (m).
 * <dl>
 * <dt><b>Example</b>:
 * This example retrieves in <code>DeviationValue</code> the maximum deviation value for the
 * <code>Fill</code> hybrid shape feature.
 * <pre>
 * Dim DeviationValue As double
 * Set DeviationValue = Fill.<font color="red">MaximumDeviationValue</font>
 * </pre>
 * </dl> 
 */
#pragma PROPERTY MaximumDeviationValue 
    HRESULT get_MaximumDeviationValue (out /*IDLRETVAL*/ double oDevValue);
    HRESULT put_MaximumDeviationValue (in                double iDevValue);

 //  ================
 //  == METHODS ==
 //  ================

 /**
  * Adds an boundary to the hybrid shape fill feature object.
  * @param iBoundary
  *		The boundary(curve) to be added to the hybrid shape fill feature object.
  * </dl>
  * <dt><b>Example</b>:
  * <dd>
  * The following example adds the <code>iBoundary</code> curve to the 
  * <code>Fill</code> object.
  * <pre>
  * Fill.<font color="red">AddBound</font> iBoundary
  * </pre>
  */ 

 HRESULT AddBound  (in CATIAReference  iBoundary);

 /**
  * Inserts the boundary after specified position in the Fill.
  * @param iBoundary 
  *     Reference of the boundary object to be inserted.
  * @param iPos
  *     Position after which the element should be inserted.
  * <dl>
  * <dt><b>Example</b>:
  * <dd>
  * This example inserts the boundary in the Fill feature <code>Fill</code>
  * after position <code>iPos</code>
  * <pre>
  * Fill.<font color="red">InsertBoundAfterPosition iBoundary,iPos </font>
  * </pre>
  * </dl>
  */

  HRESULT InsertBoundAfterPosition(in CATIAReference iBoundary, in long iPos);

 /**
  * Retrieves the position of a boundary used by the hybrid shape fill feature object.
  * @param iBoundary
  *		The boundary whose position has to be retrieved.
  * </dl>
  * <dt><b>Example</b>:
  * <dd>
  * The following example gets the <code>oPos</code> position of the <code>iBoundary</code> 
  * boundary in the <code>Fill</code> object.
  * <pre>
  * Dim oPos As  long
  * oPos = Fill.<font color="red">GetBoundPosition</font> (iBoundary).
  * </pre>
  */ 
  HRESULT GetBoundPosition(in CATIAReference iBoundary, out /*IDLRETVAL*/ long oPos);

 /**
  * Retrieves the boundary at specified position in the hybrid shape fill feature object.
  * @param iPos
  *		The position of the boundary to retrieve.
  * </dl>
  * <dt><b>Example</b>:
  * <dd>
  * The following example gets the <code>oBoundary</code> boundary of the 
  * <code>Fill</code> object at the position <code>iPos</code>.
  * <pre>
  * Dim oBoundary As Reference
  * Set oBoundary = Fill.<font color="red">GetBoundAtPosition</font> (iPos).
  * </pre>
  */
  HRESULT GetBoundAtPosition(in long iPos, out /*IDLRETVAL*/ CATIAReference  oBoundary);

 /**
  * Removes boundary at specified position in hybrid shape fill feature object.
  * @param iPos
  *		The position of the boundary to remove.
  * </dl>
  * <dt><b>Example</b>:
  * <dd>
  * The following example removes the boundary object from the 
  * <code>Fill</code> object at the position <code>iPos</code>.
  * <pre>
  * Fill.<font color="red">RemoveBoundAtPosition</font> iPos.
  * </pre>
  */ 
  HRESULT RemoveBoundAtPosition(in long iPos);

 /**
  * Removes all boundaries of the hybrid shape fill feature object.
  * </dl>
  * <dt><b>Example</b>:
  * <dd>
  * The following example removes all boundaries of the <code>Fill</code> object.
  * <pre>
  * Fill.<font color="red">RemoveAllBound</font>
  * </pre>
  */ 
  HRESULT RemoveAllBound();

 /**
  * Replaces the boundary at specified position in the Fill.
  * @param iBoundary 
  *     Reference of the boundary object to be replaced.
  * @param iPos
  *     Position at which the boundary should be replaced.
  * <dl>
  * <dt><b>Example</b>:
  * <dd>
  * This example replaces the boundary in the Fill feature <code>Fill</code>
  * at specified position <code>iPos</code>
  * <pre>
  * Fill.<font color="red">ReplaceBoundAtPosition iBoundary,iPos </font>
  * </pre>
  * </dl>
  */
  HRESULT ReplaceBoundAtPosition(in CATIAReference iBoundary, in long iPos);

 /**
  * Returns the number of boundaries in the Fill object.
  * @param oSize
  *     Number of boundaries in the Fill.
  * <dl>
  * <dt><b>Example</b>:
  * <dd>
  * This example retrieves the number of boundaries
  * in the <code>Fill</code> hybrid shape fill.
  * <pre>
  * Dim oSize As  long
  * oSize = Fill.<font color="red">GetBoundSize</font>
  * </pre>
  * </dl>
  */
  HRESULT GetBoundSize(out /*IDLRETVAL*/ long oSize);

 /**
  * Inserts the support at specified boundary in the Fill.
  * @param iBoundary 
  *     Reference of the boundary object to which support has to be added.
  * @param iSupport
  *		Reference of the support object to be added.
  * <dl>
  * <dt><b>Example</b>:
  * <dd>
  * This example adds supports in the Fill feature <code>Fill</code>
  * to specified <code>iBoundary</code> boundary
  * <pre>
  * Fill.<font color="red">AddSupportAtBound iBoundary,iSupport </font>
  * </pre>
  * </dl>
  */
  HRESULT AddSupportAtBound(in CATIAReference iBoundary, in CATIAReference iSupport);

 /**
  * Retrieves the support at specified position in the hybrid shape fill feature object.
  * @param iPos
  *		The position of the support to retrieve.
  * </dl>
  * <dt><b>Example</b>:
  * <dd>
  * The following example gets the <code>oSupport</code> support of the 
  * <code>Fill</code> object at the position <code>iPos</code>.
  * <pre>
  * Dim oSupport As Reference
  * Set oSupport = Fill.<font color="red">GetSupportAtPosition</font> (iPos).
  * </pre>
  */
  HRESULT GetSupportAtPosition(in long iPos, out /*IDLRETVAL*/ CATIAReference oSupport);

 /**
  * Removes support at specified position in hybrid shape fill feature object.
  * @param iPos
  *		The position of the support to remove.
  * </dl>
  * <dt><b>Example</b>:
  * <dd>
  * The following example removes the support object from the 
  * <code>Fill</code> object at the position <code>iPos</code>.
  * <pre>
  * Fill.<font color="red">RemoveSupportAtPosition</font> iPos.
  * </pre>
  */
  HRESULT RemoveSupportAtPosition(in long iPos);

 /**
  * Replaces the support at specified position in the Fill.
  * @param iSupport 
  *     Reference of the support object to be replaced.
  * @param iPos
  *     Position at which the support should be replaced.
  * <dl>
  * <dt><b>Example</b>:
  * <dd>
  * This example replaces the support in the Fill feature <code>Fill</code>
  * at specified position <code>iPos</code>
  * <pre>
  * Fill.<font color="red">ReplaceSupportAtPosition iSupport,iPos </font>
  * </pre>
  * </dl>
  */
  HRESULT ReplaceSupportAtPosition(in CATIAReference iSupport, in long iPos);

 /**
  * Sets the continuity mode for a boundary at specified position in the Fill.
  * @param iContinuity 
  *     Continuity between the support and the fill.
  * <br><b>Legal values</b>:
  * <dl>
  * <dt>0</dt>
  * <dd>Continuity in point (C0)</dd>
  * <dt>1</dt>
  * <dd>Continuity in tangency (C1)</dd>
  * <dt>2</dt>
  * <dd>Continuity in curvature (C2)</dd>
  * </dl>
  * @param iPos
  *     Position at which the continuity should be set.
  * <dl>
  * <dt><b>Example</b>:
  * <dd>
  * This example sets the continuity in the Fill feature <code>Fill</code>
  * at specified position <code>iPos</code>
  * <pre>
  * Fill.<font color="red">SetBoundaryContinuity iContinuity,iPos </font>
  * </pre>
  * </dl>
  */
  HRESULT SetBoundaryContinuity(in long iContinuity, in long iPos);

 /**
  * Returns the continuity mode for a boundary at specified position in the Fill.
  * @param iPos
  *     Position at which the continuity should be retrieved.
  * @param oContinuity 
  *     Continuity retrieved between the support and the fill.
  * <br><b>Legal values</b>:
  * <dl>
  * <dt>0</dt>
  * <dd>Continuity in point (C0)</dd>
  * <dt>1</dt>
  * <dd>Continuity in tangency (C1)</dd>
  * <dt>2</dt>
  * <dd>Continuity in curvature (C2)</dd>
  * </dl>
  * </dl>
  * <dt><b>Example</b>:
  * <dd>
  * This example retrieves in <code>oContinuity</code> the continuity
  * at the specified position of <code>Fill</code> hybrid shape fill feature.
  * <pre>
  * oContinuity = Fill.<font color="red">GetBoundaryContinuity</font> iPos
  * </pre>
  * </dl>
  */
  HRESULT GetBoundaryContinuity(in long iPos, out /*IDLRETVAL*/ long oContinuity);

 /**
  * Returns the number of constraints in the Fill object.
  * @param oSize
  *     Number of constraints in the Fill.
  * <dl>
  * <dt><b>Example</b>:
  * <dd>
  * This example retrieves the number of constraints
  * in the <code>Fill</code> hybrid shape fill.
  * <pre>
  * Dim oSize As  long
  * oSize = Fill.<font color="red">GetConstraintsSize</font>
  * </pre>
  * </dl>
  */
  HRESULT GetConstraintsSize(out /*IDLRETVAL*/ long oSize);

 /**
  * Retrieves the constraint at specified position in the hybrid shape fill feature object.
  * ======== ONLY USE IN FSS ==============
  * @param iPos
  *		The position of the constraint to retrieve.
  * </dl>
  * <dt><b>Example</b>:
  * <dd>
  * The following example gets the <code>oConstraint</code> constraint of the 
  * <code>Fill</code> object at the position <code>iPos</code>.
  * <pre>
  * Dim oConstraint As Reference
  * Set oConstraint = Fill.<font color="red">GetConstraintAtPosition</font> (iPos).
  * </pre>
  */
  HRESULT GetConstraintAtPosition(in long iPos, out /*IDLRETVAL*/ CATIAReference  oConstraint);

 /**
  * Appends an constraint to the hybrid shape fill feature object.
  * ======== ONLY USE IN FSS ==============
  * @param iConstraint
  *		The constraint to be appended.
  * </dl>
  * <dt><b>Example</b>:
  * <dd>
  * The following example appends the <code>iConstraint</code> constraint to the 
  * <code>Fill</code> object.
  * <pre>
  * Fill.<font color="red">AppendConstraint</font> iConstraint
  * </pre>
  */ 
  HRESULT AppendConstraint  (in CATIAReference  iConstraint);

 /**
  * Removes constraint at specified position in hybrid shape fill feature object.
  * ======== ONLY USE IN FSS ==============
  * @param iPos
  *		The position of the constraint to remove.
  * </dl>
  * <dt><b>Example</b>:
  * <dd>
  * The following example removes the constraint object from the 
  * <code>Fill</code> object at the position <code>iPos</code>.
  * <pre>
  * Fill.<font color="red">RemoveConstraint</font> iPos.
  * </pre>
  */ 
  HRESULT RemoveConstraint(in long iPos);

 /**
  * Replaces the constraint at specified position in the Fill.
  * @param iPos
  *     Position at which the constraint should be replaced.
  * @param iConstraint 
  *     Reference of the constraint object to be replaced.
  * <dl>
  * <dt><b>Example</b>:
  * <dd>
  * This example replaces the constraint in the Fill feature <code>Fill</code>
  * at specified position <code>iPos</code>
  * <pre>
  * Fill.<font color="red">ReplaceConstraint iPos,iConstraint </font>
  * </pre>
  * </dl>
  */
  HRESULT ReplaceConstraint(in long iPos, in CATIAReference iConstraint);

  /**
  * Removes all constraints.
  */
  HRESULT RemoveAllConstraints();

  /**
  * Returns or sets the tolerant mode taken into account during fill construction.
  * <br><b>Legal values</b>:
  * <dl>
  * <dt>0</dt>
  * <dd>Unknown tolerant mode.</dd>
  * <dt>1</dt>
  * <dd>None tolerant mode. Error thrown if maximum deviation exceeds CATIA resolution.</dd>
  * <dt>2</dt>
  * <dd>Automatic tolerant mode. Error thrown if maximum deviation exceeds 100 times CATIA resolution.</dd>
  * <dt>3</dt>
  * <dd>Manual tolerant mode. Error thrown if maximum deviation exceeds input user deviation.</dd>
  * </dl>
  * <dl>
  * <dt><b>Example</b>:
  * <dd>
  * This example retrieves in <code>oMode</code> the tolerant mode
  * for the <code>Fill</code> hybrid shape feature.
  * <pre>
  * Dim oMode
  * Set oMode = Fill.<font color="red">AdvancedTolerantMode</font>
  * </pre>
  * </dl>
  */
#pragma PROPERTY AdvancedTolerantMode
  HRESULT get_AdvancedTolerantMode (out /*IDLRETVAL*/ long oMode);
  HRESULT put_AdvancedTolerantMode (in				long iMode);
};


// Interface name : CATIAHybridShapeFill
#pragma ID CATIAHybridShapeFill "DCE:8b6f88c6-a14c-0000-0280020e60000000"
#pragma DUAL CATIAHybridShapeFill


// VB object name : HybridShapeFill (Id used in Visual Basic)
#pragma ID HybridShapeFill "DCE:8b6f88cb-cea5-0000-0280020e60000000"
#pragma ALIAS CATIAHybridShapeFill HybridShapeFill

#endif
// CATIAHybridShapeFill_IDL

