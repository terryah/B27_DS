#ifndef CATIAHybridShapeSplit_IDL
#define CATIAHybridShapeSplit_IDL
/*IDLREP*/

// COPYRIGHT DASSAULT SYSTEMES 2000

/**
  * @CAA2Level L1
  * @CAA2Usage U3
  */
//=================================================================
//=================================================================
//           
// CATIAHybridShapeSplit:         
// Exposed interface for HybridShape Split
//           
//=================================================================
// Usage notes:
//
//=================================================================
// Avril98 Creation    
//=================================================================
#include "CATIAHybridShape.idl"
#include "CATIAReference.idl"

 /**
 * Represents the hybrid shape split feature object.
 * <b>Role</b>: To access data of the hybrid shape split feature.
 * This data includes:
 * <ul>
 * <li> The element to be cut (surface or curve) </li>
 * <li> The cutting element ( surface, curve or point) </li>
 * <li> An orientation to specify which side has to be kept </li>
 * </ul>
 * <p>  LICENSING INFORMATION: Creation of volume result requires GSO License
 * <br> if GSO License is not granted , settting of Volume context has not effect 
 * <br> 
 * Use the CATIAHybridShapeFactory to create HybridShapeFeature object.
 * @see CATIAHybridShapeFactory
 */
//-----------------------------------------------------------------
interface CATIAHybridShapeSplit : CATIAHybridShape
{

 /**
 * Returns or sets the element to cut.
 * <dl>
 * <dt><b>Example</b>:
 * <dd>
 * This example retrieves in <code>Element</code> the element to cut for the <code>Split</code> hybrid shape feature.
 * <pre>
 * Dim Element As Reference
 * Set Element = Split.<font color="red">ElemToCut</font>
 * </pre>
 * </dl>
 */ 
#pragma PROPERTY ElemToCut	
    HRESULT get_ElemToCut (out /*IDLRETVAL*/ CATIAReference oElem) ;
    HRESULT put_ElemToCut (in                CATIAReference iElem)  ;


 /**
 * Returns or sets  the cutting element.  
 * <br>Sub-element(s) supported (see @href CATIABoundary object): 
 * @href CATIAFace, @href CATIATriDimFeatEdge, @href CATIABiDimFeatEdge or @href CATIAVertex.  
 * <dl>
 * <dt><b>Example</b>:
 * <dd>
 * This example retrieves in <code>CuttingElement</code> the cutting element for the <code>Split</code> hybrid shape feature.
 * <pre>
 * Dim CuttingElement As Reference
 * Set CuttingElement = Split.<font color="red">CuttingElem</font>
 * </pre>
 * </dl> 
 */
#pragma PROPERTY CuttingElem 
    HRESULT get_CuttingElem(out /*IDLRETVAL*/ CATIAReference oElem) ;
    HRESULT put_CuttingElem(in                CATIAReference iElem)  ;



 /**
 * Returns or sets the orientation used to compute the split.
 * <b>Role</b>:<br>
 * Orientation specifies kept parts of cut feature.<BR>
 *<BR>
 * When splitting a surface by a surface :<BR> 
 * - If orientation value is 1: kept parts are specified by the "natural" normal to the cutting feature<BR> 
 * - If orientation value -1: kept parts are specified by the inverse of the "natural" normal to the cutting feature<BR> 
 *<BR> 
 * When splitting a surface by a curve :<BR> 
 * - If orientation value is 1:
 *  kept parts are specified by the result of the cross product : normal(surface)^tangent(curve)<BR> 
 * - If orientation value is -1: 
 *       Kept parts are specified by the inverse of the result of the cross product : normal(surface)^tangent(curve)<BR> 
 * <BR> 
 * When splitting a curve by a point or a curve (without support specified) : <BR> 
 * - If orientation value is 1: 
 *       Kept parts are from beginning of the curve to the first intersection, <BR> 
 * and, if there is one, from the second to the third intersection and so on until the end of the curve.<BR> 
 * - If orientation value is -1: 
 *       Kept parts are from the first intersection to the second (if there is one), <BR> 
 * and, if there is one, from the third to the fourth and so on until the end of the curve.<BR> 
 *<BR> 
 * When splitting a curve on support: <BR> 
 * - If orientation value is 1: 
 *		 Kept parts are specified by the result of the cross product : normal(support surface)^tangent(cutting curve)<BR> 
 * - If orientation value is -1: 
 *       Kept parts are specified by the inverse of the result of the cross product : normal(support surface)^tangent(cutting curve)<BR> 
 *<BR> 
 * When splitting a curve by a surface: <BR> 
 * - If orientation value is 1: 
 *		 Kept parts are specified by the inverse of the normal to the surface<BR> 
 * - If orientation value is -1: 
 *		 Kept parts are specified by the normal to the surface<BR> 
 *<BR>
 * <dl>
 * <dt><b>Example</b>
 * <dd>
 * This example retrieves in <code>OrientValue</code> the orientation value for the <code>Split</code> hybrid shape feature.
 * <pre>
 * Dim OrientValue As long
 * Set OrientValue = Split.<font color="red">Orientation</font>
 * </pre>
 * </dl> 
 */ 
#pragma PROPERTY Orientation
    HRESULT get_Orientation(out /*IDLRETVAL*/ long oOrientation) ;
    HRESULT put_Orientation(in long iOrientation)  ;


 /**
 * Inverts the orientation used to compute the split.
 */ 
    HRESULT     InvertOrientation()  ;


/**
 * Returns or sets the support element.
 * <br>This support element may not exist.
 * <br>Sub-element(s) supported (see @href CATIABoundary object): 
 * @href CATIAFace.
 * <dl>
 * <dt><b>Example</b>:
 * <dd>
 * This example retrieves in <code>Element</code> the support element for the <code>Split</code> hybrid shape feature.
 * <pre>
 * Dim Element As Reference
 * Set Element = Split.<font color="red">Support</font>
 * </pre>
 * </dl>
 */ 
#pragma PROPERTY Support	
    HRESULT get_Support (out /*IDLRETVAL*/ CATIAReference oElem) ;
    HRESULT put_Support (in                CATIAReference iElem)  ;


/**
 * Returns or sets the Result Type. 
 * <br> Result type: 
 * <ul> 
 * <li> : 0 -> Surface </li> 
 * <li> : 1 -> Volume</li>, The resultant split will be volume. If input is element to cut is volume.
 * </ul> 
 * <br> Note: Setting volume result requires GSO License. 
 * <dl>
 * <dt><b>Example</b>:
 * This example retrieves in <code>ResultType</code> the result type for the
 * <code>Split</code> hybrid shape feature.
 * <pre>
 * Dim RType As long
 * Set RType = Split.<font color="red">ResultType</font>
 * </pre>
 * </dl>
 */
#pragma PROPERTY VolumeResult 
    HRESULT get_VolumeResult (out /*IDLRETVAL*/ long oType);
    HRESULT put_VolumeResult (in                long iType);

/**
 * Adds a cutting feature.
 *   @param iElem
 *      cutting feature 
 *   @param iOrientation 
 *      Orientation
 *      iOrientation =  1 : SameOrientation
 *					 = -1 : InvertOrientation
 *					 =  2 : KoOrientation
 */
    HRESULT AddCuttingElem( in CATIAReference iElem, in long iOrientation);

/**
 * Adds an element to specifications. This element will be kept.
 * @param iElement
 *   Element to keep.  
 */ 
    HRESULT AddElementToKeep(in CATIAReference iElement);

/**
 * Adds an element to specifications. This element will be removed.
 * @param iElement
 *   Element to remove.  
 */ 
    HRESULT AddElementToRemove(in CATIAReference iElement);

/**
 * Gets the intersection at a given index.
 *   @param oElem
 *      Intersection
 *   @param iRank
 *      Index of one of the intersection features  
 */
    HRESULT GetIntersection(in long iRank, out /*IDLRETVAL*/ CATIAReference oElem);

/**
 * Gets the kept feature at a given index.
 *   @param oElem
 *      Kept feature 
 *   @param iRank
 *      Index of one of the kept features  
 */
    HRESULT GetKeptElem(in long iRank, out /*IDLRETVAL*/ CATIAReference oElem);

/**
 * Gets the cutting feature at a given index (a point, a curve or a surface).  
 *   @param oElem
 *      cutting feature 
 *   @param iRank
 *      Index of one of the cutting features  
 */
    HRESULT GetCuttingElem(in long iRank, out /*IDLRETVAL*/ CATIAReference oElem);

/**
 * Gets the number of cutting features.
 *   @param oNbCuttingElem
 *      Number of  cutting features  
 */
    HRESULT GetNbCuttingElem (out/*IDLRETVAL*/ long oNbCuttingElem);

/**
 * Gets the number of elements to keep.
 *   @param oNbElementsToKeep
 *      Number of elements to keep
 */
    HRESULT GetNbElementsToKeep(out /*IDLRETVAL*/ long oNbElementsToKeep);

/**
 * Gets the number of elements to remove.
 *   @param oNbElementsToRemove
 *      Number of elements to remove
 */
    HRESULT GetNbElementsToRemove(out /*IDLRETVAL*/ long oNbElementsToRemove);

/**
 * Gets the other side.
 *   @param oElem
 *      Other side
 */
    HRESULT GetOtherSide(out /*IDLRETVAL*/ CATIAReference oElem);

/**
 * Gets the removed feature at a given index.
 *   @param oElem
 *      Removed feature 
 *   @param iRank
 *      Index of one of the removed features  
 */
    HRESULT GetRemovedElem(in long iRank, out /*IDLRETVAL*/ CATIAReference oElem);

/**
 * Removes a cutting feature.
 *   @param iElem
 *      cutting feature 
 */
    HRESULT RemoveCuttingElem (in CATIAReference iElem); 

/**
 * Removes an element from specifications.
 * @param iRank
 *   Index of the kept element.
 */ 
    HRESULT RemoveElementToKeep(in long iRank);

/**
 * Removes an element from specifications.
 * @param iRank
 *   Index of the removed element.
 */ 
    HRESULT RemoveElementToRemove(in long iRank);
/**
 * Gets Orientation used to compute the split.
 *   @param oOrientation  
 *      Orientation
 *   @param iRank 
 *      index of the cutting feature 
 *      oOrientation =  1 : SameOrientation
 *					 = -1 : InvertOrientation
 *					 =  2 : KoOrientation
 */
    HRESULT GetOrientation(in long iRank, out /*IDLRETVAL*/ long oOrientation);
/**
 * Sets the orientation used to compute the split.
 *   @param iOrientation  
 *      Orientation
 *   @param iRank 
 *      index of the cutting feature 
 *      iOrientation =  1 : SameOrientation
 *					 = -1 : InvertOrientation
 *					 =  2 : KoOrientation
 */
    HRESULT SetOrientation(in long iRank, in long iOrientation);

/**
 * Gets or sets the automatic extrapolation mode status.
 *      AutomaticExtrapolationMode = TRUE  : Automatic extrapolation mode is on.
 *                                 = FALSE : Automatic extrapolation mode is off.
 * This example retrieves in <code>AutoExtrapolMode</code> the automatic extrapolation mode status 
 * for the <code>Split</code> hybrid shape feature.
 * <pre>
 * Dim AutoExtrapolMode As boolean
 * AutoExtrapolMode = Split.<font color="red">AutomaticExtrapolationMode</font>
 * </pre>
 * </dl>
 */
#pragma PROPERTY AutomaticExtrapolationMode
    HRESULT get_AutomaticExtrapolationMode (out /*IDLRETVAL*/ boolean oMode);
    HRESULT put_AutomaticExtrapolationMode (in                boolean iMode);

    /**
 * Gets or sets the extrapolation type.
 *      ExtrapolationType = CATGSMExtrapolationType_None(0).
 *                        = CATGSMExtrapolationType_Tangent(1).
 *                        = CATGSMExtrapolationType_Curvature(2).
 * This example retrieves in <code>ExtrapolateType</code> the  extrapolation type  
 * for the <code>Split</code> hybrid shape feature.
 */
#pragma PROPERTY ExtrapolationType
    HRESULT get_ExtrapolationType (out /*IDLRETVAL*/ long oMode);
    HRESULT put_ExtrapolationType (in                long iMode);
/**
 * Gets or sets both sides computation mode.
 *      BothSidesMode = TRUE  : Both sides are computed.
 *                    = FALSE : Both sides are not computed.
 * This example retrieves in <code>BothSides</code> the both sides computation mode 
 * for the <code>Split</code> hybrid shape feature.
 * <pre>
 * Dim BothSides As boolean
 * BothSides = Split.<font color="red">BothSidesMode</font>
 * </pre>
 * </dl>
 */
#pragma PROPERTY BothSidesMode
    HRESULT get_BothSidesMode (out /*IDLRETVAL*/ boolean oMode);
    HRESULT put_BothSidesMode (in                boolean iMode);

/**
 * Gets or sets Intersection computation mode.
 *      IntersectionComputation = TRUE  : Intersection is computed.
 *                              = FALSE : Intersection is not computed.
 * This example retrieves in <code>Intersection</code> the Intersection computation mode 
 * for the <code>Split</code> hybrid shape feature.
 * <pre>
 * Dim Intersection As boolean
 * Intersection = Split.<font color="red">IntersectionComputation</font>
 * </pre>
 * </dl>
 */
#pragma PROPERTY IntersectionComputation
    HRESULT get_IntersectionComputation(out /*IDLRETVAL*/ boolean oMode);
    HRESULT put_IntersectionComputation(in                boolean iMode);

};


// Interface name : CATIAHybridShapeSplit
#pragma ID CATIAHybridShapeSplit "DCE:8c89b265-4a22-0000-0280020e60000000"
#pragma DUAL CATIAHybridShapeSplit

// VB object name : HybridShapeSplit (Id used in Visual Basic)
#pragma ID HybridShapeSplit "DCE:8c89b26a-46c9-0000-0280020e60000000"
#pragma ALIAS CATIAHybridShapeSplit HybridShapeSplit

#endif
// CATIAHybridShapeSplit_IDL



