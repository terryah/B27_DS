#ifndef CATIAHybridShapeCombine_IDL
#define CATIAHybridShapeCombine_IDL
/*IDLREP*/

// COPYRIGHT DASSAULT SYSTEMES 2000

/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */


//=================================================================
// COPYRIGHT DASSAULT SYSTEMES 2000
//=================================================================
//           
// CATIAHybridShapeCombine:         
// Exposed interface for HybridShape Combine
//           
//=================================================================
// Usage notes:
//
//=================================================================
// Janvier2000 Creation       
//=================================================================
#include "CATIAHybridShape.idl"
#include "CATIAReference.idl"
#include "CATIAHybridShapeDirection.idl"

/**
 * Represents the hybrid shape combined curve object.
 * <b>Role</b>: To access the data of the hybrid shape combined curve object.
 * This data includes:
 * <ul>
 * <li>The three curves to which the circle is tangent</li>
 * <li>The surface that supports the circle</li>
 * <li>The orientation of each curve.</li>
 * </ul>
 * <p>Use the @href CATIAHybridShapeFactory object to create a HybridShapeCombine object.
 */

//-----------------------------------------------------------------
interface CATIAHybridShapeCombine : CATIAHybridShape
{

/**
 * Returns or sets the first curve used to create the combined curve.
 * <br>Sub-element(s) supported (see @href CATIABoundary object): 
 * @href CATIATriDimFeatEdge or @href CATIABiDimFeatEdge.
 * @sample
 * This example sets <code>firstCurve</code> as the first curve to create the combined curve
 * <code>hybCombCurve</code>.
 * <pre>
 * hybCombCurve.<font color="red">Elem1</font> = firstCurve
 * </pre>
 */ 
#pragma PROPERTY Elem1
    HRESULT get_Elem1 (out /*IDLRETVAL*/ CATIAReference   oElem);
    HRESULT put_Elem1 (in                CATIAReference   iElem);

/**
 * Returns or sets the first direction used to create the combined curve.
 * The first direction is the direction along which the first curve is extruded.
 * @sample
 * This example sets <code>firstDir</code> as the first direction to create the combined curve
 * <code>hybCombCurve</code>.
 * <pre>
 * hybCombCurve.<font color="red">Direction1</font> = firstDir
 * </pre>
 */ 
#pragma PROPERTY Direction1
    HRESULT get_Direction1 (out /*IDLRETVAL*/ CATIAHybridShapeDirection  oElem)  ;
    HRESULT put_Direction1 (in                CATIAHybridShapeDirection  iElem)  ;

/**
 * Returns or sets the second curve used to create the combined curve.
 * <br>Sub-element(s) supported (see @href CATIABoundary object): 
 * @href CATIATriDimFeatEdge or @href CATIABiDimFeatEdge.
 * @sample
 * This example retrieves in <code>secondCurve</code> the second curve used to create
 * the combined curve <code>hybCombCurve</code>.
 * <pre>
 * Dim secondCurve As CATIAReference
 * Set secondCurve = hybCombCurve.<font color="red">Elem2</font>
 * </pre>
 */ 
#pragma PROPERTY Elem2
    HRESULT get_Elem2 (out /*IDLRETVAL*/ CATIAReference   oElem); 
    HRESULT put_Elem2 (in                CATIAReference   iElem);

/**
 * Returns or sets the second direction used to create the combined curve. 
 * @sample
 * This example retrieves in <code>secondDir</code> the second direction used
 * to create the combined curve <code>hybCombCurve</code>.
 * <pre>
 * Dim secondDir As CATIAHybridShapeDirection
 * Set secondDir = hybCombCurve.<font color="red">Direction2</font>
 * </pre>
 */
#pragma PROPERTY Direction2
    HRESULT get_Direction2 (out /*IDLRETVAL*/ CATIAHybridShapeDirection  oElem)  ;
    HRESULT put_Direction2 (in                CATIAHybridShapeDirection  iElem)  ;

/**
 * Returns or sets whether the combined curve is or should be created as the curve closest
 * to the first curve.
 * <br><b>Role</b>: The nearest solution indicates whether the created combined curve
 * is the one closest to the first curve if there are several possible combined curves,
 * or if all these possible combined curves are created..
 * <br><b>Legal values</b>: 0 for the nearest solution and 1 for all possible solutions.
 * @sample
 * This example sets the nearest solution mode to create the combined curve
 * <code>hybCombCurve</code> closest to the first curve.
 * <pre>
 * hybCombCurve.<font color="red">NearestSolution</font> = 1
 * </pre>
 * </dl> 
 */
#pragma PROPERTY NearestSolution
    HRESULT get_NearestSolution (out /*IDLRETVAL*/ long oSolutionType);
    HRESULT put_NearestSolution (in                long iSolutionType); 

/**
 * Returns or sets whether the curves that create the combined curve are or should be
 * extruded along normals to the curve planes or along given directions.
 * <br><b>Role</b>: The curves that make up the combined curve are each extruded along
 * a direction. This direction can be the normal to the curve plane, or can be set to
 * a given direction. This is valid for the two curves altogether.
 * <br><b>Legal values</b>: 0 for the normal to the curve planes (default mode),
 * 1 for given directions
 * @sample
 * This example sets that the combined curve <code>hybCombCurve</code> should be created
 * by extruding the two curves along the normals to their planes.
 * <pre>
 * hybCombCurve.<font color="red">SolutionTypeCombine</font> = 0
 * </pre>
 */
#pragma PROPERTY SolutionTypeCombine
    HRESULT get_SolutionTypeCombine (out /*IDLRETVAL*/ long oSolutionTypeCombine);
    HRESULT put_SolutionTypeCombine (in                long iSolutionTypeCombine); 
};


// Interface name : CATIAHybridShapeCombine
#pragma ID CATIAHybridShapeCombine "DCE:8fbccf3e-5722-0000-0280020e60000000"
#pragma DUAL CATIAHybridShapeCombine

// VB object name : HybridShapeCombine (Id used in Visual Basic)
#pragma ID HybridShapeCombine "DCE:8fbcd0e5-f08c-0000-0280020e60000000"
#pragma ALIAS CATIAHybridShapeCombine HybridShapeCombine

#endif
// CATIAHybridShapeCombine_IDL
