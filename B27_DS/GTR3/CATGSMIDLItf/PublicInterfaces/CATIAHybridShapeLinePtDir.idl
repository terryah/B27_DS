/* -*-c++-*- */
#ifndef CATIAHybridShapeLinePtDir_IDL
#define CATIAHybridShapeLinePtDir_IDL
/*IDLREP*/

// COPYRIGHT DASSAULT SYSTEMES 2000

/**
  * @CAA2Level L1
  * @CAA2Usage U3
  */


//=================================================================
//           
// CATIAHybridShapeLinePtDir:         
// Exposed interface for HybridShapeLinePtDir
//           
// Janvier 2000 Creation
//=================================================================
#include "CATIAHybridShapeLine.idl"
#include "CATIAHybridShapeDirection.idl"
#include "CATIAReference.idl"
#include "CATIALength.idl"

/**
 * Line defined by a point and a direction.
 * <b>Role</b>: To access data of the line feature created by using 
 * a passing point and a direction.
 * <p>Use the  CATIAHybridShapeFactory to create a HybridShapeLinePtDir object.
 * @see CATIAReference 
 * @see CATIALength 
 * @see CATIAHybridShapeDirection 
 * @see CATIAHybridShapeFactory
 */

//-----------------------------------------------------------------
interface CATIAHybridShapeLinePtDir : CATIAHybridShapeLine
{



 /**
 * Returns or Sets the starting point of the line.
 * <br>Sub-element(s) supported (see @href CATIABoundary object): 
 * @href CATIAVertex.
 * <dl>
 * <dt><b>Example</b>:
 * <dd>
 * This example retrieves in <code>oPoint</code> the starting point    
 * for the <code>LinePtDir</code> hybrid shape feature.
 * <pre>
 * Dim oPoint As Reference
 * Set oPoint = LinePtDir.<font color="red">Point</font>
 * </pre>
 * </dl>
 */ 
#pragma PROPERTY Point
 HRESULT get_Point ( out /*IDLRETVAL*/  CATIAReference   oPoint);
 HRESULT put_Point ( in           CATIAReference   iPoint);



 /**
 * Returns the start length of the line.
 * <br> Start length : extension of the line, beginning at the starting point
 * <dl>
 * <dt><b>Example</b>:
 * <dd>
 * This example retrieves in <code>oStart</code> the beginning offset length   
 * for the <code>LinePtDir</code> hybrid shape feature.
 * <pre>
 * Dim oStart As  CATIALength 
 * Set oStart = LinePtDir.<font color="red">BeginOffset</font>
 * </pre>
 * </dl>
 */ 
#pragma PROPERTY BeginOffset
 HRESULT get_BeginOffset ( out /*IDLRETVAL*/  CATIALength   oStart);


 /**
 * Returns the end length of the line.
 * <br> End length : extension of the line, beginning at the ending point
 * <dl>
 * <dt><b>Example</b>:
 * <dd>
 * This example retrieves in <code>oEnd</code> the starting length   
 * for the <code>LinePtDir</code> hybrid shape feature.
 * <pre>
 * Dim oEnd As  CATIALength 
 * Set oEnd = LinePtDir.<font color="red">EndOffset</font>
 * </pre>
 * </dl>
 */ 
#pragma PROPERTY EndOffset
 HRESULT get_EndOffset ( out /*IDLRETVAL*/  CATIALength   oEnd);






 /**
 * Returns or Sets the direction of the line.
 * <dl>
 * <dt><b>Example</b>:
 * <dd>
 * This example retrieves in <code>oDir</code> the direction     
 * for the <code>LinePtDir</code> hybrid shape feature.
 * <pre>
 * Dim oDir As CATIAHybridShapeDirection
 * Set oDir = LinePtDir.<font color="red">Dir</font>
 * </pre>
 * </dl>
 */ 
#pragma PROPERTY Dir
 HRESULT get_Dir ( out /*IDLRETVAL*/  CATIAHybridShapeDirection   oDir);
 HRESULT put_Dir ( in            CATIAHybridShapeDirection   iDir);



 /**
 * Returns or Sets the supporting  surface.
 * <br> Note: the support surface is not mandatory for LinePtDir<br>
 * <br>Sub-element(s) supported (see @href CATIABoundary object): 
 * @href CATIAFace.
 * <dl>
 * <dt><b>Example</b>:
 * <dd>
 * This example retrieves in <code>oSurface</code> the supporting surface (if it exist)  
 * for the <code>LinePtDir</code> hybrid shape feature.
 * <pre>
 * Dim oSurface As Reference 
 * Set oSurface = LinePtDir.<font color="red">Surface</font>
 * </pre>
 * </dl>
 */ 
#pragma PROPERTY Support
 HRESULT get_Support ( out /*IDLRETVAL*/  CATIAReference   oSurface);
 HRESULT put_Support ( in           CATIAReference   iSurface);




 /**
 * Returns or Sets the line orientation.
 * <br> Orientation allows to reverse the line direction from the reference point.
 * <br> For a line of L length, it is the same as creating this line with -L length :
 *  Orientation : can be 1 or -1
 * <dl>
 * <dt><b>Example</b>:
 * <dd>
 * This example retrieves in <code>oOrientation</code> the starting length   
 * for the <code>LinePtDir</code> hybrid shape feature.
 * <pre>
 * Dim oOrientation As long
 * Set oOrientation = LinePtDir.<font color="red">Orientation</font>
 * </pre>
 * </dl>
 */ 
#pragma PROPERTY Orientation
 HRESULT get_Orientation (out /*IDLRETVAL*/  long       oOrientation);
 HRESULT put_Orientation (in       long       iOrientation);

 //  ================
 //  == METHODS ==
 //  ================
 /**
 * Gets the length type
 * Default is 0.
 *   @param oType
 *   The length type = 0 : length               - the line is limited by its extremities
 *                   = 1 : infinite             - the line is infinite
 *                   = 2 : infinite start point - the line is infinite on the side of the start point
 *                   = 3 : infinite end point   - the line is infinite on the side of the end point
 */
 HRESULT GetLengthType (out /*IDLRETVAL*/  long  oType);
 /**
 * Sets the length type
 * Default is 0.
 *   @param iType
 *   The length type = 0 : length               - the line is limited by its extremities
 *                   = 1 : infinite             - the line is infinite
 *                   = 2 : infinite start point - the line is infinite on the side of the start point
 *                   = 3 : infinite end point   - the line is infinite on the side of the end point
 */
 HRESULT SetLengthType(in long iType);
 /**
 * Sets the symmetrical extension of the line (start = -end).
 *   @param iSym
 *       Symetry flag  
 */
 HRESULT SetSymmetricalExtension(in boolean iSym);

 /**
 * Gets whether the symmetrical extension of the line is active.
 *   @param oSym
 *       Symetry flag  
 */
 HRESULT GetSymmetricalExtension(out /*IDLRETVAL*/ boolean oSym); 
 /**
 * Removes the support surface.
 */
 HRESULT RemoveSupport();

};


// Interface name : CATIAHybridShapeLinePtDir
#pragma ID CATIAHybridShapeLinePtDir "DCE:89d765fa-b434-0000-0280020e60000000"
#pragma DUAL CATIAHybridShapeLinePtDir

// VB object name : HybridShapeLinePtDir (Id used in Visual Basic)
#pragma ID HybridShapeLinePtDir "DCE:89d768f8-64f9-0000-0280020e60000000"
#pragma ALIAS CATIAHybridShapeLinePtDir HybridShapeLinePtDir


#endif


// CATIAHybridShapeLinePtDir_IDL         
