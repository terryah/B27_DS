#ifndef CATIAHybridShapeHealing_IDL
#define CATIAHybridShapeHealing_IDL
/*IDLREP*/

// COPYRIGHT DASSAULT SYSTEMES 2000

/**
  * @CAA2Level L1
  * @CAA2Usage U3
  */


//=================================================================
//           
// CATIAHybridShapeHealing:         
// Exposed interface for HybridShape Healing
//           
//=================================================================

#include "CATIAHybridShape.idl"
#include "CATIALength.idl"
#include "CATIAAngle.idl"
#include "CATIAReference.idl"

/**
 * Represents the hybrid shape healing feature object.
 * <b>Role</b>: Allows to access to the body to process for a Healing feature.
 * Use the CATIAHybridShapeFactory to create HybridShapeFeature object.
 * @see CATIAHybridShapeFactory#AddNewHealing
 */

//-----------------------------------------------------------------
interface CATIAHybridShapeHealing : CATIAHybridShape
{


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 *							Properties
 *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 */

/*----------------------------------------------------------------
 *							NoOfBodiesToHeal Property
 *----------------------------------------------------------------
 */ 

/**
  * Returns the number of bodies to heal of the healing.
  *   @param NumberOfbodies
  *     Number of bodies to heal in the healing.
  * <dl>
  * <dt><b>Example</b>:
  * <dd>
  * This example retrieves the number of bodies to heal 
  * of the <code>HybShpHealing</code> hybrid shape Healing.
  * <pre>
  * Dim NoOfBodiesToHeal As  long
  * NoOfBodiesToHeal = HybShpHealing.<font color="red">NoOfBodiesToHeal</font>
  * </pre>
  * </dl>
  */

#pragma PROPERTY NoOfBodiesToHeal

  HRESULT get_NoOfBodiesToHeal (out /*IDLRETVAL*/ long  oNoOfBodies); 

/*----------------------------------------------------------------
 *							NoOfElementsToFreeze Property
 *----------------------------------------------------------------
 */ 

/**
  * Returns the number of elements to heal of the healing.
  *   @param NumberOfElements
  *     Number of elements to freeze in the healing.
  * <dl>
  * <dt><b>Example</b>:
  * <dd>
  * This example retrieves the number of elements to freeze 
  * of the <code>HybShpHealing</code> hybrid shape Healing.
  * <pre>
  * Dim NoOfElementsToFreeze As  long
  * NoOfElementsToFreeze = HybShpHealing.<font color="red">NoOfElementsToFreeze</font>
  * </pre>
  * </dl>
  */

#pragma PROPERTY NoOfElementsToFreeze

    HRESULT get_NoOfElementsToFreeze( out /*IDLRETVAL*/ long  oNoOfElements ); 

/*----------------------------------------------------------------
 *							NoOfEdgesToKeepSharp Property
 *----------------------------------------------------------------
 */ 

/**
  * Returns the number of edges to keep sharp of the healing.
  *   @param NumberOfEdges
  *     Number of edges to keep sharp.
  * <dl>
  * <dt><b>Example</b>:
  * <dd>
  * This example retrieves the number of edges to keep sharp 
  * of the <code>HybShpHealing</code> hybrid shape Healing.
  * <pre>
  * Dim NoOfEdges As  long
  * NoOfEdges = HybShpHealing.<font color="red">NoOfEdgesToKeepSharp</font>
  * </pre>
  * </dl>
  */

#pragma PROPERTY NoOfEdgesToKeepSharp

    HRESULT get_NoOfEdgesToKeepSharp (out /*IDLRETVAL*/ long  oNoOfEdges); 

/*----------------------------------------------------------------
 *                Continuity Property
 *----------------------------------------------------------------
 */ 

/**
  * Returns or sets the continuity type of the healing.
  *   @param Continuity 
  *     Parameter for the continuity. Legal values are 0 and 1
  * <dl>
  * <dt><b>Example</b>:
  * <dd>
  * This example sets and retrieves the Continuity of the healing
  * of the <code>HybShpHealing</code> hybrid shape healing.
  * <pre>
  * Dim HybShpHealCont As  Long
  * HybShpHealCont = ..set appropriate value
  * HybShpHealing.<font color="red">Continuity</font> = HybShpHealCont
  * HybShpHealCont = HybShpHealing.<font color="red">Continuity</font>
  * </pre>
  * </dl>
  */
#pragma PROPERTY Continuity
  HRESULT put_Continuity (in  				      long	iContinuity);
  HRESULT get_Continuity (out /*IDLRETVAL*/ long 	oContinuity);


/*----------------------------------------------------------------
 *							CanonicFreeMode Property
 *----------------------------------------------------------------
 */ 

/**
  * Returns or sets the Canonic Free Mode of the healing.
  *   @param oMode (For get_CanonicFreeMode)
  *     Long parameter for retrieving the CanonicFreeMode.
  *   @param iMode (For set_CanonicFreeMode)
  *     Long parameter for settingthe CanonicFreeMode.
  * <dl>
  * <dt><b>Example</b>:
  * <dd>
  * This example sets and retrieves the CanonicFreeMode of the healing
  * of the <code>HybShpHealing</code> hybrid shape healing.
  * <pre>
  * Dim HybShpHealMode As  Long
  * HybShpHealMode = ..set appropriate value
  * HybShpHealing.<font color="red">CanonicFreeMode</font> = HybShpHealMode
  * HybShpHealCont = HybShpHealing.<font color="red">CanonicFreeMode</font>
  * </pre>
  * </dl>
  */
#pragma PROPERTY CanonicFreeMode
  HRESULT put_CanonicFreeMode ( in 		            long  iMode);
  HRESULT get_CanonicFreeMode ( out /*IDLRETVAL*/ long  oMode);


/*----------------------------------------------------------------
 *							MergingDistance Property
 *----------------------------------------------------------------
 */ 
/**
  * Returns the Merging Distance of the healing.
  *   @param MergingDistance 
  *     Length parameter for retrieving the Merging Distance.
  * <dl>
  * <dt><b>Example</b>:
  * <dd>
  * This example retrieves the MergingDistance of the healing
  * of the <code>HybShpHealing</code> hybrid shape healing.
  * <pre>
  * Dim HybShpHealMergeDist As Length
  * Set HybShpHealMergeDist = HybShpHealing.<font color="red">MergingDistance</font>
  * </pre>
  * </dl>
  */
#pragma PROPERTY MergingDistance
  HRESULT get_MergingDistance	(out  /*IDLRETVAL*/ CATIALength oMergingDistance);

/*----------------------------------------------------------------
 *							DistanceObjective Property
 *----------------------------------------------------------------
 */ 
/**
  * Returns the Distance Objective of the healing.
  *   @param DistanceObjective 
  *     Length parameter for retrieving the Distance Objective.
  * <dl>
  * <dt><b>Example</b>:
  * <dd>
  * This example retrieves the DistanceObjective of the healing
  * of the <code>HybShpHealing</code> hybrid shape healing.
  * <pre>
  * Dim HybShpHealDistObjective As Length
  * Set HybShpHealDistObjective = HybShpHealing.<font color="red">DistanceObjective</font>
  * </pre>
  * </dl>
  */

#pragma PROPERTY DistanceObjective
  HRESULT get_DistanceObjective (out  /*IDLRETVAL*/ CATIALength oDistanceObjective);

/*----------------------------------------------------------------
 *							TangencyAngle Property
 *----------------------------------------------------------------
 */ 
/**
  * Returns the Tangency Angle of the healing.
  *   @param TangencyAngle 
  *     Angle parameter for retrieving the TangencyAngle.
  * <dl>
  * <dt><b>Example</b>:
  * <dd>
  * This example retrieves the TangencyAngle of the healing
  * of the <code>HybShpHealing</code> hybrid shape healing.
  * <pre>
  * Dim HybShpHealTangencyAngle As Angle
  * Set HybShpHealTangencyAngle = HybShpHealing.<font color="red">TangencyAngle</font>
  * </pre>
  * </dl>
  */

#pragma PROPERTY TangencyAngle
  HRESULT get_TangencyAngle	(out  /*IDLRETVAL*/ CATIAAngle  oTangencyAngle);

/*----------------------------------------------------------------
 *							TangencyObjective Property
 *----------------------------------------------------------------
 */ 
/**
  * Returns the Tangency Objective of the healing.
  *   @param TangencyObjective 
  *     Length parameter for retrieving the Tangency Objective.
  * <dl>
  * <dt><b>Example</b>:
  * <dd>
  * This example retrieves the TangencyObjective of the healing
  * of the <code>HybShpHealing</code> hybrid shape healing.
  * <pre>
  * Dim HybShpHealTangencyObjective As Length
  * Set HybShpHealTangencyObjective = HybShpHealing.<font color="red">TangencyObjective</font>
  * </pre>
  * </dl>
  */


#pragma PROPERTY TangencyObjective
  HRESULT get_TangencyObjective (out  /*IDLRETVAL*/ CATIALength oTangencyObjective);


/*----------------------------------------------------------------
 *							SharpnessAngle Property
 *----------------------------------------------------------------
 */ 
/**
  * Returns the Sharpness Angle of the healing.
  *   @param SharpnessAngle 
  *     Angle parameter for retrieving the Sharpness Angle.
  * <dl>
  * <dt><b>Example</b>:
  * <dd>
  * This example retrieves the Sharpness Angle of the healing
  * of the <code>HybShpHealing</code> hybrid shape healing.
  * <pre>
  * Dim HybShpHealSharpnessAngle As Angle
  * Set HybShpHealSharpnessAngle = HybShpHealing.<font color="red">SharpnessAngle</font>
  * </pre>
  * </dl>
  */

#pragma PROPERTY SharpnessAngle
  HRESULT get_SharpnessAngle  (out  /*IDLRETVAL*/ CATIAAngle  oSharpnessAngle);



/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 *							Methods
 *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 */

/*----------------------------------------------------------------
 *							SetMergingDistance Method
 *----------------------------------------------------------------
 */

/**
  * Sets the Merging distance for healing entity.
  *   @param MergingDistance  
  *     Parameter containg the value of the merging distance to be set.
  * <dl>
  * <dt><b>Example</b>:
  * <dd>
  * This example sets the merging distance for the healing
  * of the <code>HybShpHealing</code> hybrid shape healing.
  * <pre>
  * HybShpHealing.<font color="red">SetMergingDistance 2.5 </font>
  * </pre>
  * </dl>
  */

	HRESULT SetMergingDistance  (in double  iMergingDistance);
/*----------------------------------------------------------------
 *							SetDistanceObjective Method
 *----------------------------------------------------------------
 */

/**
  * Sets the distance objective for healing entity.
  *   @param DistanceObjective  
  *     Parameter containg the value of the distance objective to be set.
  * <dl>
  * <dt><b>Example</b>:
  * <dd>
  * This example sets the distance objective for the healing
  * of the <code>HybShpHealing</code> hybrid shape healing.
  * <pre>
  * HybShpHealing.<font color="red">SetDistanceObjective 2.5 </font>
  * </pre>
  * </dl>
  */

  HRESULT SetDistanceObjective 	(in  double iDistanceObjective);
/*----------------------------------------------------------------
 *							SetTangencyAngle Method
 *----------------------------------------------------------------
 */

/**
  * Sets the distance objective for healing entity.
  *   @param TangencyAngle  
  *     Parameter containg the value of the Tangency Angle to be set.
  * <dl>
  * <dt><b>Example</b>:
  * <dd>
  * This example sets the Tangency Angle for the healing
  * of the <code>HybShpHealing</code> hybrid shape healing.
  * <pre>
  * HybShpHealing.<font color="red">SetTangencyAngle 2.5 </font>
  * </pre>
  * </dl>
  */

  HRESULT SetTangencyAngle 	(in double  iTangencyAngle);

/*----------------------------------------------------------------
 *							SetTangencyObjective Method
 *----------------------------------------------------------------
 */

/**
  * Sets the tangency objective for healing entity.
  *   @param TangencyObjective  
  *     Parameter containg the value of the Tangency Objective to be set.
  * <dl>
  * <dt><b>Example</b>:
  * <dd>
  * This example sets the Tangency Objective for the healing
  * of the <code>HybShpHealing</code> hybrid shape healing.
  * <pre>
  * HybShpHealing.<font color="red">SetTangencyObjective 2.5 </font>
  * </pre>
  * </dl>
  */

  HRESULT SetTangencyObjective 	(in double  iTangencyObjective);


/*----------------------------------------------------------------
 *							AddBodyToHeal Method
 *----------------------------------------------------------------
 */

/**
  * Adds the body to be healed to the list.
  *   @param Body  
  *     Reference to the body to be added to the list.
  * <dl>
  * <dt><b>Example</b>:
  * <dd>
  * This example adds the body to the list.
  * of the <code>HybShpHealing</code> hybrid shape healing.
  * <pre>
  * HybShpHealing.<font color="red">AddBodyToHeal refBody </font>
  * </pre>
  * </dl>
  */

  HRESULT AddBodyToHeal (in CATIAReference  iBody);

/*----------------------------------------------------------------
 *					GetBodyToHeal Method
 *----------------------------------------------------------------
 */

/**
  * Returns the body to be healed from the list at specified position.
  *   @param Position  
  *     Position at which the body is to be obtained
 .*   @param Body  
  *     Reference to the body obtained at specified position.
  * <dl>
  * <dt><b>Example</b>:
  * <dd>
  * This example gets the body from the list by specifying the position.
  * of the <code>HybShpHealing</code> hybrid shape healing.
  * <pre>
  * set refBody = HybShpHealing.<font color="red">GetBodyToHeal  1 </font>
  * </pre>
  * </dl>
  */
	
  HRESULT GetBodyToHeal (in long  iPosition,  out /*IDLRETVAL*/ CATIAReference  oBody);

/*----------------------------------------------------------------
 *							RemoveBodyToHeal Method
 *----------------------------------------------------------------
 */

/**
  * Removes the body to be healed from the list at specified position.
  *   @param iPosition  
  *     Position at which the body is to be removed
  * <dl>
  * <dt><b>Example</b>:
  * <dd>
  * This example removes the body from the list at specifying the position.
  * of the <code>HybShpHealing</code> hybrid shape healing.
  * <pre>
  * HybShpHealing.<font color="red">RemoveBodyToHeal  1  </font>
  * </pre>
  * </dl>
  */
	
  HRESULT RemoveBodyToHeal  (in long  iPosition);

/*----------------------------------------------------------------
 *							AddElementsToFreeze Method
 *----------------------------------------------------------------
 */

/**
  * Adds the body to be freezed while healing, to the list.
  *   @param Element  
  *     Reference to the element to be freezed.
  * <dl>
  * <dt><b>Example</b>:
  * <dd>
  * This example adds the body to the list of bodies to be freezed.
  * of the <code>HybShpHealing</code> hybrid shape healing.
  * <pre>
  * HybShpHealing.<font color="red">AddElementsToFreeze refElement </font>
  * </pre>
  * </dl>
  */

  HRESULT AddElementsToFreeze	(in 	CATIAReference 	iElement);

/*----------------------------------------------------------------
 *							GetElementToFreeze Method
 *----------------------------------------------------------------
 */

/**
  * Returns the element to be freezed from the list at specified position.
  *   @param Position  
  *     Position at which the element is to be obtained
 .*   @param Element  
  *     Reference to the element obtained at specified position.
  * <dl>
  * <dt><b>Example</b>:
  * <dd>
  * This example gets the element from the list of bodies to be freezed 
  * by specifying the position of the <code>HybShpHealing</code> 
  * hybrid shape healing.
  * <pre>
  * set refElement = HybShpHealing.<font color="red">GetElementToFreeze  1 </font>
  * </pre>
  * </dl>
  */

  HRESULT GetElementToFreeze	(in long  iPosition,  out  /*IDLRETVAL*/  CATIAReference oElement);

/*----------------------------------------------------------------
 *							RemoveElementToFreeze Method
 *----------------------------------------------------------------
 */

/**
  * Removes the element from the list of elements to be freezed at specified position.
  *   @param Position  
  *     Position at which the element is to be removed
  * <dl>
  * <dt><b>Example</b>:
  * <dd>
  * This example removes the element from the list at specifying the position.
  * of the <code>HybShpHealing</code> hybrid shape healing.
  * <pre>
  * HybShpHealing.<font color="red">RemoveElementToFreeze  1  </font>
  * </pre>
  * </dl>
  */

  HRESULT RemoveElementToFreeze	(in long  iPosition);

/*----------------------------------------------------------------
 *							AddEdgeToKeepSharp Method
 *----------------------------------------------------------------
 */

/**
  * Adds the edge to be kept sharp while healing, to the list.
  *   @param Edge  
  *     Reference to the Edge to be kept sharp
  * <dl>
  * <dt><b>Example</b>:
  * <dd>
  * This example adds the Edge to the list of Edges to be kept sharp.
  * of the <code>HybShpHealing</code> hybrid shape healing.
  * <pre>
  * HybShpHealing.<font color="red">AddEdgeToKeepSharp refEdge </font>
  * </pre>
  * </dl>
  */

  HRESULT AddEdgeToKeepSharp  (in CATIAReference  iEdge);

/*----------------------------------------------------------------
 *							GetEdgeToKeepSharp Method
 *----------------------------------------------------------------
 */

/**
  * Returns the edge to be kept sharp from the list at specified position.
  *   @param Position  
  *     Position at which the element is to be obtained
 .*   @param Edge  
  *     Reference to the element obtained at specified position.
  * <dl>
  * <dt><b>Example</b>:
  * <dd>
  * This example gets the Edge from the list of Edges to be kept sharp 
  * by specifying the position of the <code>HybShpHealing</code> 
  * hybrid shape healing.
  * <pre>
  * set refEdge = HybShpHealing.<font color="red">GetEdgeToKeepSharp  1  </font>
  * </pre>
  * </dl>
  */
  HRESULT GetEdgeToKeepSharp	(in long  iPosition,  out /*IDLRETVAL*/ CATIAReference oEdge);

/*----------------------------------------------------------------
 *							RemoveEdgeToKeepSharp Method
 *----------------------------------------------------------------
 */

/**
  * Removes the edge from the list of edges to be kept sharp at specified position.
  *   @param iPosition  
  *     Position at which the edge is to be removed
  * <dl>
  * <dt><b>Example</b>:
  * <dd>
  * This example removes the edge from the list at specified position.
  * of the <code>HybShpHealing</code> hybrid shape healing.
  * <pre>
  * HybShpHealing.<font color="red">RemoveEdgeToKeepSharp  1  </font>
  * </pre>
  * </dl>
  */

  HRESULT RemoveEdgeToKeepSharp	(in long  iPosition);

/*----------------------------------------------------------------
 *							SetSharpnessAngle Method
 *----------------------------------------------------------------
 */

/**
  * Sets the Sharpness Angle  for healing entity.
  *   @param SharpnessAngle  
  *     Parameter containg the value of the Sharpness Angle to be set.
  * <dl>
  * <dt><b>Example</b>:
  * <dd>
  * This example sets the Sharpness Angle for the healing
  * of the <code>HybShpHealing</code> hybrid shape healing.
  * <pre>
  * HybShpHealing.<font color="red">SetSharpnessAngle 2.5 </font>
  * </pre>
  * </dl>
  */

  HRESULT SetSharpnessAngle (in double  iSharpnessAngle);

/**
  * Replaces an element to heal.
  *   @param iIndex 
  *        The position of the element to replace.
  *   @param iNewHeal 
  *        The new element.
  */ 
  HRESULT ReplaceToHealElement(in long iIndex,in CATIAReference iNewHeal);

};


// Interface name : CATIAHybridShapeHealing
#pragma ID   CATIAHybridShapeHealing "DCE:8c0f417a-b42c-0000-0280020e70000000"
#pragma DUAL CATIAHybridShapeHealing

// VB object name : HybridShapeHealing (Id used in Visual Basic)
#pragma ID    HybridShapeHealing "DCE:8c0f417f-af38-0000-0280020e70000000"
#pragma ALIAS CATIAHybridShapeHealing HybridShapeHealing


#endif
// CATIAHybridShapeHealing_IDL

