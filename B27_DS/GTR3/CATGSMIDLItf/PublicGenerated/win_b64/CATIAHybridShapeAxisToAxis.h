/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAHybridShapeAxisToAxis_h
#define CATIAHybridShapeAxisToAxis_h

#include "CATCORBABoolean.h"
#include "CAT_VARIANT_BOOL.h"

#ifndef ExportedByGSMPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __GSMPubIDL
#define ExportedByGSMPubIDL __declspec(dllexport)
#else
#define ExportedByGSMPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByGSMPubIDL
#endif
#endif

#include "CATIAHybridShape.h"

class CATIAReference;

extern ExportedByGSMPubIDL IID IID_CATIAHybridShapeAxisToAxis;

class ExportedByGSMPubIDL CATIAHybridShapeAxisToAxis : public CATIAHybridShape
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_ElemToTransform(CATIAReference *& oElem)=0;

    virtual HRESULT __stdcall put_ElemToTransform(CATIAReference * iElem)=0;

    virtual HRESULT __stdcall get_ReferenceAxis(CATIAReference *& oAxis)=0;

    virtual HRESULT __stdcall put_ReferenceAxis(CATIAReference * iAxis)=0;

    virtual HRESULT __stdcall get_TargetAxis(CATIAReference *& oAxis)=0;

    virtual HRESULT __stdcall put_TargetAxis(CATIAReference * iAxis)=0;

    virtual HRESULT __stdcall get_VolumeResult(CAT_VARIANT_BOOL & oType)=0;

    virtual HRESULT __stdcall put_VolumeResult(CAT_VARIANT_BOOL iType)=0;

    virtual HRESULT __stdcall get_CreationMode(CAT_VARIANT_BOOL & oCreation)=0;

    virtual HRESULT __stdcall put_CreationMode(CAT_VARIANT_BOOL iCreation)=0;


};

CATDeclareHandler(CATIAHybridShapeAxisToAxis, CATIAHybridShape);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATGeometricType.h"
#include "CATIABase.h"
#include "CATIAGeometricElement.h"
#include "CATIAReference.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
