/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAHybridShapeSurfaceExplicit_h
#define CATIAHybridShapeSurfaceExplicit_h

#ifndef ExportedByGSMPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __GSMPubIDL
#define ExportedByGSMPubIDL __declspec(dllexport)
#else
#define ExportedByGSMPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByGSMPubIDL
#endif
#endif

#include "CATIAHybridShape.h"

extern ExportedByGSMPubIDL IID IID_CATIAHybridShapeSurfaceExplicit;

class ExportedByGSMPubIDL CATIAHybridShapeSurfaceExplicit : public CATIAHybridShape
{
    CATDeclareInterface;

public:

};

CATDeclareHandler(CATIAHybridShapeSurfaceExplicit, CATIAHybridShape);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATGeometricType.h"
#include "CATIABase.h"
#include "CATIAGeometricElement.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
