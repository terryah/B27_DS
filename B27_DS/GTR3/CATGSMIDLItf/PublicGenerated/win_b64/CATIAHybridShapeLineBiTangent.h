/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAHybridShapeLineBiTangent_h
#define CATIAHybridShapeLineBiTangent_h

#ifndef ExportedByGSMPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __GSMPubIDL
#define ExportedByGSMPubIDL __declspec(dllexport)
#else
#define ExportedByGSMPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByGSMPubIDL
#endif
#endif

#include "CATIAHybridShapeLine.h"

class CATIAReference;

extern ExportedByGSMPubIDL IID IID_CATIAHybridShapeLineBiTangent;

class ExportedByGSMPubIDL CATIAHybridShapeLineBiTangent : public CATIAHybridShapeLine
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_Curve1(CATIAReference *& oCurve)=0;

    virtual HRESULT __stdcall put_Curve1(CATIAReference * iCurve)=0;

    virtual HRESULT __stdcall get_Element2(CATIAReference *& oElement)=0;

    virtual HRESULT __stdcall put_Element2(CATIAReference * iElement)=0;

    virtual HRESULT __stdcall get_Support(CATIAReference *& oSurface)=0;

    virtual HRESULT __stdcall put_Support(CATIAReference * iSurface)=0;

    virtual HRESULT __stdcall GetChoiceNo(CATLONG & val1, CATLONG & val2, CATLONG & val3, CATLONG & val4, CATLONG & val5)=0;

    virtual HRESULT __stdcall SetChoiceNo(CATLONG val1, CATLONG val2, CATLONG val3, CATLONG val4, CATLONG val5)=0;

    virtual HRESULT __stdcall GetLengthType(CATLONG & oType)=0;

    virtual HRESULT __stdcall SetLengthType(CATLONG iType)=0;


};

CATDeclareHandler(CATIAHybridShapeLineBiTangent, CATIAHybridShapeLine);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATGeometricType.h"
#include "CATIABase.h"
#include "CATIADimension.h"
#include "CATIAGeometricElement.h"
#include "CATIAHybridShape.h"
#include "CATIALength.h"
#include "CATIAParameter.h"
#include "CATIARealParam.h"
#include "CATIAReference.h"
#include "CATSafeArray.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
