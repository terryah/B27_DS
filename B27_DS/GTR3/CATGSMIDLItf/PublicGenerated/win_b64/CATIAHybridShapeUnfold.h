/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAHybridShapeUnfold_h
#define CATIAHybridShapeUnfold_h

#ifndef ExportedByGSMPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __GSMPubIDL
#define ExportedByGSMPubIDL __declspec(dllexport)
#else
#define ExportedByGSMPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByGSMPubIDL
#endif
#endif

#include "CATIAHybridShape.h"

class CATIAReference;

extern ExportedByGSMPubIDL IID IID_CATIAHybridShapeUnfold;

class ExportedByGSMPubIDL CATIAHybridShapeUnfold : public CATIAHybridShape
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_SurfaceToUnfold(CATIAReference *& oElem)=0;

    virtual HRESULT __stdcall put_SurfaceToUnfold(CATIAReference * iElem)=0;

    virtual HRESULT __stdcall get_OriginToUnfold(CATIAReference *& oElem)=0;

    virtual HRESULT __stdcall put_OriginToUnfold(CATIAReference * iElem)=0;

    virtual HRESULT __stdcall get_DirectionToUnfold(CATIAReference *& oElem)=0;

    virtual HRESULT __stdcall put_DirectionToUnfold(CATIAReference * iElem)=0;

    virtual HRESULT __stdcall get_TargetPlane(CATIAReference *& oElem)=0;

    virtual HRESULT __stdcall put_TargetPlane(CATIAReference * iElem)=0;

    virtual HRESULT __stdcall AddEdgeToTear(CATIAReference * iElement)=0;

    virtual HRESULT __stdcall GetEdgeToTear(CATLONG iRank, CATIAReference *& oElement)=0;

    virtual HRESULT __stdcall RemoveEdgeToTear(CATLONG iRank)=0;

    virtual HRESULT __stdcall get_SurfaceType(CATLONG & oType)=0;

    virtual HRESULT __stdcall put_SurfaceType(CATLONG iType)=0;

    virtual HRESULT __stdcall get_TargetOrientationMode(CATLONG & oMode)=0;

    virtual HRESULT __stdcall put_TargetOrientationMode(CATLONG iMode)=0;

    virtual HRESULT __stdcall get_EdgeToTearPositioningOrientation(CATLONG & oMode)=0;

    virtual HRESULT __stdcall put_EdgeToTearPositioningOrientation(CATLONG iMode)=0;

    virtual HRESULT __stdcall AddElementToTransfer(CATIAReference * iElement, CATLONG iTypeOfTransfer)=0;

    virtual HRESULT __stdcall GetElementToTransfer(CATLONG iRank, CATIAReference *& opElement, CATLONG & oTypeOfTransfer)=0;

    virtual HRESULT __stdcall RemoveElementToTransfer(CATLONG iRank)=0;

    virtual HRESULT __stdcall ReplaceElementsToTransfer(CATLONG iRank, CATIAReference * iElement)=0;


};

CATDeclareHandler(CATIAHybridShapeUnfold, CATIAHybridShape);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATGeometricType.h"
#include "CATIAAngle.h"
#include "CATIABase.h"
#include "CATIADimension.h"
#include "CATIAGeometricElement.h"
#include "CATIALength.h"
#include "CATIAParameter.h"
#include "CATIARealParam.h"
#include "CATIAReference.h"
#include "CATSafeArray.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
