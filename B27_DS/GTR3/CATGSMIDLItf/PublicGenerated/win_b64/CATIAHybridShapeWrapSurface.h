/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAHybridShapeWrapSurface_h
#define CATIAHybridShapeWrapSurface_h

#ifndef ExportedByGSMPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __GSMPubIDL
#define ExportedByGSMPubIDL __declspec(dllexport)
#else
#define ExportedByGSMPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByGSMPubIDL
#endif
#endif

#include "CATIAHybridShape.h"

class CATIAReference;

extern ExportedByGSMPubIDL IID IID_CATIAHybridShapeWrapSurface;

class ExportedByGSMPubIDL CATIAHybridShapeWrapSurface : public CATIAHybridShape
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_Surface(CATIAReference *& oSurface)=0;

    virtual HRESULT __stdcall put_Surface(CATIAReference * iSurface)=0;

    virtual HRESULT __stdcall get_ReferenceSurface(CATIAReference *& oReferenceSurface)=0;

    virtual HRESULT __stdcall put_ReferenceSurface(CATIAReference * iReferenceSurface)=0;

    virtual HRESULT __stdcall get_TargetSurface(CATIAReference *& oTargetSurface)=0;

    virtual HRESULT __stdcall put_TargetSurface(CATIAReference * iTargetSurface)=0;

    virtual HRESULT __stdcall get_DeformationMode(CATLONG & oDeformationMode)=0;

    virtual HRESULT __stdcall put_DeformationMode(CATLONG iDeformationMode)=0;


};

CATDeclareHandler(CATIAHybridShapeWrapSurface, CATIAHybridShape);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATGeometricType.h"
#include "CATIABase.h"
#include "CATIAGeometricElement.h"
#include "CATIAReference.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
