/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAHybridShapeCurvePar_h
#define CATIAHybridShapeCurvePar_h

#include "CATCORBABoolean.h"
#include "CAT_VARIANT_BOOL.h"

#ifndef ExportedByGSMPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __GSMPubIDL
#define ExportedByGSMPubIDL __declspec(dllexport)
#else
#define ExportedByGSMPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByGSMPubIDL
#endif
#endif

#include "CATIAHybridShape.h"
#include "CATSafeArray.h"

class CATIALength;
class CATIAReference;

extern ExportedByGSMPubIDL IID IID_CATIAHybridShapeCurvePar;

class ExportedByGSMPubIDL CATIAHybridShapeCurvePar : public CATIAHybridShape
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_Offset(CATIALength *& oCurvePar)=0;

    virtual HRESULT __stdcall get_Offset2(CATIALength *& oCurvePar2)=0;

    virtual HRESULT __stdcall get_CurveOffseted(CATIAReference *& oFaceToCurvePar)=0;

    virtual HRESULT __stdcall put_CurveOffseted(CATIAReference * iFaceToCurvePar)=0;

    virtual HRESULT __stdcall get_SmoothingType(CATLONG & oType)=0;

    virtual HRESULT __stdcall put_SmoothingType(CATLONG iType)=0;

    virtual HRESULT __stdcall get_MaximumDeviationValue(double & oDevValue)=0;

    virtual HRESULT __stdcall put_MaximumDeviationValue(double iDevValue)=0;

    virtual HRESULT __stdcall get_p3DSmoothing(CAT_VARIANT_BOOL & o3DSmoothing)=0;

    virtual HRESULT __stdcall put_p3DSmoothing(CAT_VARIANT_BOOL i3DSmoothing)=0;

    virtual HRESULT __stdcall get_PassingPoint(CATIAReference *& oPassingPoint)=0;

    virtual HRESULT __stdcall put_PassingPoint(CATIAReference * iPassingPoint)=0;

    virtual HRESULT __stdcall get_KeepBothSides(CAT_VARIANT_BOOL & oKeepBothSides)=0;

    virtual HRESULT __stdcall put_KeepBothSides(CAT_VARIANT_BOOL iKeepBothSides)=0;

    virtual HRESULT __stdcall get_OtherSide(CATIAReference *& oOtherSide)=0;

    virtual HRESULT __stdcall get_Support(CATIAReference *& oElem)=0;

    virtual HRESULT __stdcall put_Support(CATIAReference * iElem)=0;

    virtual HRESULT __stdcall get_Geodesic(CAT_VARIANT_BOOL & oMode)=0;

    virtual HRESULT __stdcall put_Geodesic(CAT_VARIANT_BOOL iMode)=0;

    virtual HRESULT __stdcall get_LawType(CATLONG & oLawType)=0;

    virtual HRESULT __stdcall put_LawType(CATLONG iLawType)=0;

    virtual HRESULT __stdcall get_CurveParType(CATLONG & oCurveParType)=0;

    virtual HRESULT __stdcall put_CurveParType(CATLONG iCurveParType)=0;

    virtual HRESULT __stdcall get_CurveParLaw(CATIAReference *& oLaw)=0;

    virtual HRESULT __stdcall put_CurveParLaw(CATIAReference * iLaw)=0;

    virtual HRESULT __stdcall get_InvertMappingLaw(CAT_VARIANT_BOOL & oInvert)=0;

    virtual HRESULT __stdcall put_InvertMappingLaw(CAT_VARIANT_BOOL iInvert)=0;

    virtual HRESULT __stdcall get_InvertDirection(CAT_VARIANT_BOOL & oInvert)=0;

    virtual HRESULT __stdcall put_InvertDirection(CAT_VARIANT_BOOL iInvert)=0;

    virtual HRESULT __stdcall GetPlaneNormal(CATSafeArrayVariant & oNormal)=0;

    virtual HRESULT __stdcall PutPlaneNormal(const CATSafeArrayVariant & iNormal)=0;


};

CATDeclareHandler(CATIAHybridShapeCurvePar, CATIAHybridShape);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATGeometricType.h"
#include "CATIABase.h"
#include "CATIADimension.h"
#include "CATIAGeometricElement.h"
#include "CATIALength.h"
#include "CATIAParameter.h"
#include "CATIARealParam.h"
#include "CATIAReference.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
