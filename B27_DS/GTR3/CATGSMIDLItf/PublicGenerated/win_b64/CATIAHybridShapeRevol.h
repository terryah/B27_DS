/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAHybridShapeRevol_h
#define CATIAHybridShapeRevol_h

#include "CATCORBABoolean.h"
#include "CAT_VARIANT_BOOL.h"

#ifndef ExportedByGSMPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __GSMPubIDL
#define ExportedByGSMPubIDL __declspec(dllexport)
#else
#define ExportedByGSMPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByGSMPubIDL
#endif
#endif

#include "CATIAHybridShape.h"

class CATIAAngle;
class CATIAReference;

extern ExportedByGSMPubIDL IID IID_CATIAHybridShapeRevol;

class ExportedByGSMPubIDL CATIAHybridShapeRevol : public CATIAHybridShape
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_BeginAngle(CATIAAngle *& oAngle)=0;

    virtual HRESULT __stdcall get_EndAngle(CATIAAngle *& oAngle)=0;

    virtual HRESULT __stdcall get_Axis(CATIAReference *& oDir)=0;

    virtual HRESULT __stdcall put_Axis(CATIAReference * iDir)=0;

    virtual HRESULT __stdcall get_Profil(CATIAReference *& oProfil)=0;

    virtual HRESULT __stdcall put_Profil(CATIAReference * oProfil)=0;

    virtual HRESULT __stdcall get_Context(CATLONG & oContext)=0;

    virtual HRESULT __stdcall put_Context(CATLONG iContext)=0;

    virtual HRESULT __stdcall put_Orientation(CAT_VARIANT_BOOL iOrientation)=0;

    virtual HRESULT __stdcall get_Orientation(CAT_VARIANT_BOOL & oOrientation)=0;

    virtual HRESULT __stdcall get_FirstLimitType(CATLONG & oLim1Type)=0;

    virtual HRESULT __stdcall put_FirstLimitType(CATLONG iLim1Type)=0;

    virtual HRESULT __stdcall get_SecondLimitType(CATLONG & oLim2Type)=0;

    virtual HRESULT __stdcall put_SecondLimitType(CATLONG iLim2Type)=0;

    virtual HRESULT __stdcall get_FirstUptoElement(CATIAReference *& oLim1Elem)=0;

    virtual HRESULT __stdcall put_FirstUptoElement(CATIAReference * iLim1Elem)=0;

    virtual HRESULT __stdcall get_SecondUptoElement(CATIAReference *& oLim2Elem)=0;

    virtual HRESULT __stdcall put_SecondUptoElement(CATIAReference * iLim2Elem)=0;


};

CATDeclareHandler(CATIAHybridShapeRevol, CATIAHybridShape);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATGeometricType.h"
#include "CATIAAngle.h"
#include "CATIABase.h"
#include "CATIADimension.h"
#include "CATIAGeometricElement.h"
#include "CATIAHybridShapeDirection.h"
#include "CATIALength.h"
#include "CATIAParameter.h"
#include "CATIARealParam.h"
#include "CATIAReference.h"
#include "CATSafeArray.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
