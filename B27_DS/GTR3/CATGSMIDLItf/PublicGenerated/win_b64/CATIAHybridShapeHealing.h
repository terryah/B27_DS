/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAHybridShapeHealing_h
#define CATIAHybridShapeHealing_h

#ifndef ExportedByGSMPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __GSMPubIDL
#define ExportedByGSMPubIDL __declspec(dllexport)
#else
#define ExportedByGSMPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByGSMPubIDL
#endif
#endif

#include "CATIAHybridShape.h"

class CATIAAngle;
class CATIALength;
class CATIAReference;

extern ExportedByGSMPubIDL IID IID_CATIAHybridShapeHealing;

class ExportedByGSMPubIDL CATIAHybridShapeHealing : public CATIAHybridShape
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_NoOfBodiesToHeal(CATLONG & oNoOfBodies)=0;

    virtual HRESULT __stdcall get_NoOfElementsToFreeze(CATLONG & oNoOfElements)=0;

    virtual HRESULT __stdcall get_NoOfEdgesToKeepSharp(CATLONG & oNoOfEdges)=0;

    virtual HRESULT __stdcall put_Continuity(CATLONG iContinuity)=0;

    virtual HRESULT __stdcall get_Continuity(CATLONG & oContinuity)=0;

    virtual HRESULT __stdcall put_CanonicFreeMode(CATLONG iMode)=0;

    virtual HRESULT __stdcall get_CanonicFreeMode(CATLONG & oMode)=0;

    virtual HRESULT __stdcall get_MergingDistance(CATIALength *& oMergingDistance)=0;

    virtual HRESULT __stdcall get_DistanceObjective(CATIALength *& oDistanceObjective)=0;

    virtual HRESULT __stdcall get_TangencyAngle(CATIAAngle *& oTangencyAngle)=0;

    virtual HRESULT __stdcall get_TangencyObjective(CATIALength *& oTangencyObjective)=0;

    virtual HRESULT __stdcall get_SharpnessAngle(CATIAAngle *& oSharpnessAngle)=0;

    virtual HRESULT __stdcall SetMergingDistance(double iMergingDistance)=0;

    virtual HRESULT __stdcall SetDistanceObjective(double iDistanceObjective)=0;

    virtual HRESULT __stdcall SetTangencyAngle(double iTangencyAngle)=0;

    virtual HRESULT __stdcall SetTangencyObjective(double iTangencyObjective)=0;

    virtual HRESULT __stdcall AddBodyToHeal(CATIAReference * iBody)=0;

    virtual HRESULT __stdcall GetBodyToHeal(CATLONG iPosition, CATIAReference *& oBody)=0;

    virtual HRESULT __stdcall RemoveBodyToHeal(CATLONG iPosition)=0;

    virtual HRESULT __stdcall AddElementsToFreeze(CATIAReference * iElement)=0;

    virtual HRESULT __stdcall GetElementToFreeze(CATLONG iPosition, CATIAReference *& oElement)=0;

    virtual HRESULT __stdcall RemoveElementToFreeze(CATLONG iPosition)=0;

    virtual HRESULT __stdcall AddEdgeToKeepSharp(CATIAReference * iEdge)=0;

    virtual HRESULT __stdcall GetEdgeToKeepSharp(CATLONG iPosition, CATIAReference *& oEdge)=0;

    virtual HRESULT __stdcall RemoveEdgeToKeepSharp(CATLONG iPosition)=0;

    virtual HRESULT __stdcall SetSharpnessAngle(double iSharpnessAngle)=0;

    virtual HRESULT __stdcall ReplaceToHealElement(CATLONG iIndex, CATIAReference * iNewHeal)=0;


};

CATDeclareHandler(CATIAHybridShapeHealing, CATIAHybridShape);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATGeometricType.h"
#include "CATIAAngle.h"
#include "CATIABase.h"
#include "CATIADimension.h"
#include "CATIAGeometricElement.h"
#include "CATIALength.h"
#include "CATIAParameter.h"
#include "CATIARealParam.h"
#include "CATIAReference.h"
#include "CATSafeArray.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
