//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//*
//* FILE:
//*     DNBExpressionParser.h    - public header file
//*
//* MODULE:
//*     DNBExpressionMD     
//*
//* OVERVIEW:
//*     This class will parse an expression string into a DNBExpression tree
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     BPL         03/14/2000  Initial Implementation
//*     BPL         09/15/2000  Added support of '=' and ';'
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     DWB, VKA    05/08/2000  
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 2000 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _DNB_DNBEXPRESSIONPARSER_H_
#define _DNB_DNBEXPRESSIONPARSER_H_

#include <DNBSystemBase.h>
#include <scl_list.h>           
#include <scl_string.h>          
#include <DNBSystemDefs.h>

#include <DNBSimulationLanguage.h>
#include <DNBExpression.h>

// Binary and unary operators
#include <DNBExpressionPlus.h>
#include <DNBExpressionMinus.h>
#include <DNBExpressionMult.h>
#include <DNBExpressionDivide.h>
#include <DNBExpressionExp.h>
#include <DNBExpressionMod.h>
#include <DNBExpressionUnaryMinus.h>
#include <DNBExpressionEquals.h>
#include <DNBExpressionSemiColon.h>

#include <DNBExpressionRPNStack.h>

// Parser support classes
#include <DNBExpressionStringHandler.h>
#include <DNBExpressionToken.h>
#include <DNBExpressionSymbolExtractor.h>

// Symbol table
#include <DNBSymbol.h>
#include <DNBSymbolTable.h>
//#include <DNBRealDataSymbol.h>

#include <DNBException.h>

//*
//* CLASS:
//*     DNBExpressionParser
//*
//* SUMMARY:
//*     This class will parse an expression string into a DNBExpression tree
//*
//* DESCRIPTION:
//*     This parser is a bottom up parser, creating nodes at the bottom of
//* tree first.  Individual operators and their operands are reduced into a 
//* new single operand untill only a single operand remains.  
//* 
//* 3+2*1*3-3+3^2+4*3-2
//* 3+ 2 *3-3+3^2+4*3-2
//* 3+  6  -3+3^2+4*3-2
//*  9     -3+3^2+4*3-2
//*        6 +3^2+4*3-2
//*        6 + 9 +4*3-2
//*         15   +4*3-2
//*         15   + 12-2
//*             27   -2
//*                 25
//* 
//* The parser first converts the string into a set of tokens.  Tokens are  
//* recognized based on the following spotting features:
//*
//*     Number: Start with either a number or a period and contain only 
//* numbers and periods. (Scientific notation is not supported.)
//*
//*     Function: Start with a letter and contain only numbers or letters. 
//* Functions follow the format NAME ( ARG, ARG, . . . )
//*
//*     Symbols ID: Start with a symbol delimiter (defaults to ") and ends 
//* with a symbol delimiter.  The symbol ID is an interger value that is 
//* used to access the symbol name stored in the 
//* DNBExpressionSymbolExtractor class.  The extraction of the symbols 
//* is done by the pre-parser.
//*
//*     Math Operators: +,-,*,/,^, and % are reconized.  Unary plus and unary 
//* minus are seperated from binary plus and minus by checking to see if there is 
//* another operator or open parenthesis to its left.
//* 
//*     General: White space and tabs are ignored.
//*
//* Each token has a category and type.
//* Category:
//*   OPERAND: A value, symbol, or expression that is self contained and 
//* can be resolved to a single value.
//*   OPERATOR: A symbol that operators of the operands adjacent to it.
//*
//*   The following types are operands:
//*     CONSTANT: A numerical value
//*     SYMBOL: A symbol used to access a value in the world data model
//*     DNB_EXPRESSION: A token that contains a pointer to a DNBExpression
//* node that is stored on the heap.
//*
//*   The following types are operators.  There are three groups of 
//* operators, though they are not stored differently in the program.
//*
//* Unary operators take a single operand on the right hand side:
//*     UNARYMINUS    -
//*     UNARYPLUS     +
//*
//* Binary operators take two operands, a left and a right.
//*     PLUS          +
//*     MINUS         -
//*     MULT          *
//*     DIVIDE        /
//*     EXP           ^
//*     MOD           %
//*     EQUALS        =
//*     SEMICOLON     ;
//*
//*   Signal operators do not operate on operands in traditional methods.  
//* Instead they signal that a special case must be dealt with.
//*
//*     FUNCTION: A function token stores a name as a string and is a signal  
//* that the next set of tokens are function arguments.  When the parsing of 
//* the arguments is completed, a DNBExpressoinFunction node is created and 
//* stored in a DNBEXPRESSION token.
//*
//*     OPEN_PAREN: An open parenthesis used to control precedence or enclose 
//* function arguments.
//*
//*     CLOSE_PAREN: A close parenthesis used to match open parenthesis 
//* or signal the end of a function argument block.
//*
//*     COMMA: A comma delimits function arguments.
//*
//*     END_OF_LINE: The end of line operator signals that the end of the 
//* token list has been reached.  Each list of tokens must end in this token.
//*
//*
//*     Precedence determines in what order operators are executed.  The 
//* parser checks to see if the current operator has a precedence value 
//* lower than the next operator.  If so, the current operator is 
//* reduced.  If the precedence is equal, the operator is reduce if it has 
//* left to right execution with the next operator.  When an operator is  
//* reduced, the pointer is reset to the beginning of the token list.  If the
//* operator is not reduced, the pointer is advance to the next operator.
//*
//* Precedence values for operators
//* OPEN_PAREN  0
//* FUNCTION    1
//* UNARYMINUS  2
//* UNARYPLUS   2
//* EXP         3    (right to left evaluation)
//* MOD         4
//* MULT        4
//* DIVIDE      4
//* PLUS        5
//* MINUS       5
//* EQUALS      6    (right to left evaluation)
//* SEMICOLON   7
//* COMMA       97
//* CLOSE_PAREN 98
//* END_OF_LINE 99
//*
//*     A function token is always reduced immediately without being 
//* compared to the next operator.  Its arguments are collected by 
//* making a recursive call to the parsing function on the token list.
//* A DNBExpressionFunction node is then created. 
//*
//* EXAMPLE:
//*    scl_wstring expressionString = L"1 + 3 - sin(\"x\")";
//*    scl_wstring preParsedExpression;
//*    wchar_t pathDelimiter = L'/';
//*    wchar_t attrDelimiter = L':';
//*    wchar_t symbolDelimiter = L'\"';
//*    DNBExpression<DNBReal>::pointer topNode = NULL;
//*
//*    DNBExpressionSymbolExtractor::extractorPtr extractedSymbols = 
//*        new DNBExpressionSymbolExtractor( expressionString, 
//*                                          preParsedExpression, 
//*                                          pathDelimiter, 
//*                                          attrDelimiter, 
//*                                          symbolDelimiter );
//* 
//*    DNBExpressionParser p( preParsedExpression, 
//*                           topNode, 
//*                           symbolTable, 
//*                           extractedSymbols );
//*
class 
ExportedByDNBSimulationLanguage DNBExpressionParser
{
public:
    //*
    //* TYPEDEF:
    //*     listOfTokens
    //*
    //* SUMMARY:
    //*     A list for storing pointers to tokens
    //*
    //* DESCRIPTION:
    //*     This type definition specifies a list for storing pointers to 
    //* DNBExpressionToken instances on the heap.
    //*
    //*
    typedef 
    scl_list< DNBExpressionToken::tokenPointer, DNB_ALLOCATOR( DNBExpressionToken::tokenPointer ) > 
    listOfTokens;
    
    //*
    //* FUNCTION:
    //*    DNBExpressionParser
    //*
    //* SUMMARY:
    //*    Initialization constructor
    //*
    //* PARAMETERS:
    //*     A \param[in]{scl_wstring} 
    //*         The expression string to be parsed
    //*     A \param[in]{scl_wstring} 
    //*         The origonal non-preparsed expression string 
    //*     A \param[out]{DNBExpression<DNBReal>::pointer} 
    //*         A pointer to the top node of the DNBExpression tree that is created
    //*     A \param[in]{DNBSymbolTable} 
    //*         The symbol table used to create the DNBSymbol based on the symbol name.
    //*     A \param[in]{DNBExpressionSymbolExtractor::extractorPtr} 
    //*         The symbol extractor class that stores the symbol name based on the symbol ID
    //*
    //* RETURNS:
    //*    Nothing
    //*
    //* DESCRIPTION:
    //*    This constructor loads in an expression string, converts to tokens, parses it into 
    //* a DNBExpression tree, and passes back a pointer to the top node of the tree.
    //*
    //* EXCEPTIONS:
    //*    \param{scl_bad_alloc}
    //*         This function creates a DNBExpression nodes on the heap and 
    //* uses STL lists, scl_bad_alloc is thrown if there is not enough memory
    //*    \param{DNBEInvalidFormat}
    //*         Thrown if the expression has any syntax errors
    //*
    DNBExpressionParser( 
                    const scl_wstring expressionString, 
                    const scl_wstring originalExpressionString,
                    DNBExpression<DNBReal>::pointer& topNode, 
                    DNBSymbolTable& symbolTable,
                    DNBExpressionSymbolExtractor::extractorPtr& extractedSymbolsPtr )
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEInvalidFormat ));

    //*
    //* FUNCTION:
    //*    ~DNBExpressionParser
    //*
    //* SUMMARY:
    //*    Destructor
    //*
    //* PARAMETERS:
    //*     None 
    //*
    //* RETURNS:
    //*    Nothing
    //*
    //* DESCRIPTION:
    //*    Destructor for DNBExpressionParser.
    //*
    //* EXCEPTIONS:
    //*    None
    //*
    ~DNBExpressionParser()
    DNB_THROW_SPEC_NULL;

private:
    // 
    // Default constructor, private
    // 
    DNBExpressionParser()
    DNB_THROW_SPEC_NULL;

    // 
    // Copy constructor, private, empty implemtation
    // 
    DNBExpressionParser( const DNBExpressionParser &right)
    DNB_THROW_SPEC_NULL;

    // 
    // Converts a string into a set of tokens that are stored in the token list
    // 
    void 
    convertToTokens( DNBExpressionSymbolExtractor::extractorPtr& extractedSymbolsPtr )
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEInvalidFormat ));

    // 
    // Parses the token list to create a DNBExpression tree
    // 
    void 
    createTree(     DNBExpression<DNBReal>::pointer& expNode, 
                    DNBSymbolTable& symbolTable,
                    DNBExpressionSymbolExtractor::extractorPtr& extractedSymbolsPtr )
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEInvalidFormat ));

    // 
    // Parses the token list into a DNBExpression tree untill it reaches a 
    // END_OF_LINE, a comma, or a unmatched close paren
    // 
    void 
    parseTillBreak( listOfTokens::iterator& currentOperator, 
                    DNBExpression<DNBReal>::pointer& expNode )
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEInvalidFormat ));

    // 
    // Creates a DNBExpression node to represent the unary operator token 
    // pointed to by currentOperator
    // 
    void 
    reduceUnaryOperator( 
                    listOfTokens::iterator& currentOperator, 
                    listOfTokens::iterator startingPoint,
                    DNBInteger32& parenCount )
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEInvalidFormat ));

    // 
    // Creates a DNBExpression node to represent the binary operator token 
    // pointed to by currentOperator
    // 
    void 
    reduceBinaryOperator( 
                    listOfTokens::iterator& currentOperator, 
                    listOfTokens::iterator startingPoint,
                    DNBInteger32& parenCount )
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEInvalidFormat ));

    // 
    //     Handles with open parenthesis tokens.  If the next operator token
    // is a close parenthesis token, both are removed.  Other wise the program 
    // moves on to the next token
    // 
    void 
    reduceOpenParen( 
                    listOfTokens::iterator& currentOperator, 
                    listOfTokens::iterator startingPoint,
                    DNBInteger32& parenCount )
    DNB_THROW_SPEC_NULL;

    // 
    // Creates a DNBExpression node to represent the function token pointed 
    // to by currentOperator
    // 
    void 
    reduceFunction( 
                    listOfTokens::iterator& currentOperator, 
                    listOfTokens::iterator startingPoint,
                    DNBInteger32& parenCount )
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEInvalidFormat ));

    // 
    // Advances the nextOperator iterator to the next active operator token 
    // in the token list.  Throws an exception if list.end() is reached.
    // 
    void 
    getNextOperator( listOfTokens::iterator& nextOperator )
    DNB_THROW_SPEC(( DNBEInvalidFormat ));

    // 
    // Advances the nextOperator iterator to the next active token in the token 
    // list.  Throws an exception if list.end() is reached.
    // 
    void 
    getNextToken( listOfTokens::iterator& nextToken )
    DNB_THROW_SPEC(( DNBEInvalidFormat ));

    // 
    // Backs up the nextOperator iterator to the previous active token in the 
    // token list.  Throws an exception the front of the list is walked off of.
    // 
    void
    getPreviousToken( listOfTokens::iterator& previousToken )
    DNB_THROW_SPEC(( DNBEInvalidFormat ));

    // 
    // Check that all equals signs have a symbol on the left hand side
    // 
    void 
    checkEqualsSigns()
        DNB_THROW_SPEC(( DNBEInvalidFormat ));

    // 
    // Converts all the constant tokens in the token list to DNBExpressionConstant nodes
    // 
    void 
    convertConstantsToDNBExpression()
    DNB_THROW_SPEC(( scl_bad_alloc  ));

    // 
    // 
    // Converts all the symbol tokens in the token list to DNBExpressionSymbol 
    // nodes, include getting the DNBSymbol from the symbol table
    void 
    convertSymbolsToDNBExpression(  
                    DNBSymbolTable& symbolTable,
                    DNBExpressionSymbolExtractor::extractorPtr& extractedSymbolsPtr )
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEInvalidFormat ));

    // 
    // Travels through the token list examining each minus token to see if it 
    // should be a unary minus.
    // 
    void 
    identifyUnaryMinusTokens()
    DNB_THROW_SPEC_NULL;

    // 
    // Travels through the token list examining each plus token to see if it 
    // should be a unary plus.
    // 
    void 
    identifyUnaryPlusTokens()
    DNB_THROW_SPEC_NULL;

    // 
    // Data:
    // 
    // The expression string to be parsed
    // 
    scl_wstring 
    expressionString_;
    
    // 
    // A SLT list used to store the tokens created for the expression string
    // 
    listOfTokens 
    tokenList_;
};


#endif  /* _DNB_DNBEXPRESSIONPARSER_H_ */
