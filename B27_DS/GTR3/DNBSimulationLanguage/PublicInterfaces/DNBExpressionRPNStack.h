//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//*
//* FILE:
//*     DNBExpressionRPNStack.h    - public header file
//*
//* MODULE:
//*     DNBExpressionMD
//*
//* OVERVIEW:
//*     This is the class for the RPN stack operand that inherits from DNBExpressionOperand
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     BPL         11/10/1999  Initial Implementation
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1999, 2000 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _DNB_DNBEXPRESSIONRPNSTACK_H_
#define _DNB_DNBEXPRESSIONRPNSTACK_H_

#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>

#include <DNBSimulationLanguage.h>
#include <DNBExpressionOperand.h>
#include <DNBExpressionOperator.h>
#include <DNBExpressionConstant.h>
#include <DNBListStack.h>

#include <DNBException.h>

//*
//* CLASS:
//*     DNBExpressionRPNStack
//*
//* SUMMARY:
//*     Node in DNBExpression inheritance for representing the RPN stack
//*
//* TEMPLATING:
//*    \param{ T }
//*        <T> is the numerical type that calculations are being
//*     done in.
//*
//* DESCRIPTION:
//*     This class is used to represent the result of compiling an
//* expression tree of DNBExpression nodes into an PRN stack.  This
//* class is evaluated to find the value of the expression using
//* getValue().  This evaluation will be faster than the evaluation of a
//* expression tree.
//*
//* EXAMPLE:
//*     The pointer to the top of a expression tree is passed to the
//* constructor.
//*     DNBExpression<double>::pointer ptrc1 =
//*            DNB_NEW DNBExpressionConstant<double>(5);
//*     DNBExpression<double>::pointer ptrs1 =
//*            DNB_NEW DNBExpressionRPNStack<double>(ptrc1);
//*     double result = ptrs1->getValue();
//*
template<class T>
class DNBExpressionRPNStack
    : public DNBExpressionOperand<T>
{
	DNB_DECLARE_EXPRESSION(DNBExpressionRPNStack);
public:
    //*
    //* FUNCTION:
    //*    DNBExpressionRPNStack
    //*
    //* SUMMARY:
    //*    Default constructor
    //*
    //* PARAMETERS:
    //*    \param{pointer}
    //*         The top node in the tree to be converted into a stack
    //*
    //* RETURNS:
    //*    Nothing
    //*
    //* DESCRIPTION:
    //*    The default constructor for DNBExpressionRPNStack takes as input
    //* the top node in the expression tree.
    //*
    //* EXCEPTIONS:
    //*    \param{scl_bad_alloc}
    //*        This function pushes pointers onto a STL list that will
    //* throw a scl_bad_alloc if there is not enough memory.
    //*    \param{DNBEDoesNotExist}
    //*        Thrown if a leaf pointer is null
    //*
    DNBExpressionRPNStack (pointer topNode)
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEDoesNotExist ));

    //*
    //* FUNCTION:
    //*    ~DNBExpressionRPNStack
    //*
    //* SUMMARY:
    //*    Destructor
    //*
    //* PARAMETERS:
    //*     None
    //*
    //* RETURNS:
    //*    Nothing
    //*
    //* DESCRIPTION:
    //*    Destructor for DNBExpressionRPNStack.
    //*
    //* EXCEPTIONS:
    //*    None
    //*
    virtual
    ~DNBExpressionRPNStack()
    DNB_THROW_SPEC_NULL;

    //*
    //* FUNCTION:
    //*    getValue
    //*
    //* SUMMARY:
    //*    Evaluates a node
    //*
    //* PARAMETERS:
    //*    None
    //*
    //* RETURNS:
    //*    Value of type <T>
    //*
    //* DESCRIPTION:
    //*    Causes the node to evaluate itself and return the resulting
    //* value.  For DNBExpressionRPNStack, getValue() calls evaluateStack().
    //*
    //* EXCEPTIONS:
    //*    \param{scl_bad_alloc}
    //*         The getValue implentation for DNBExpressionRPNStack
    //* calls EvalutateStack(), which creates new DNBExpressionConstant
    //* nodes, which may throw scl_bad_alloc if memory is not available.
    //*    \param{DNBEDoesNotExist}
    //*         Thrown if any of the leaf pointers are NULL.
    //*    \param{DNBEZeroDivide}
    //*         Thrown if a divide node divides by zero.
    //*    \param{DNBEOverflowError}
    //*         Thrown for overflow.
    //*    \param{DNBEUnderflowError}
    //*         Thrown for underflow.
    //*
    virtual
    T
    getValue()
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEDoesNotExist, DNBEZeroDivide, DNBEOverflowError, DNBEUnderflowError ));

    //*
    //* FUNCTION:
    //*    evaluateStack
    //*
    //* SUMMARY:
    //*    Evaluates the RPN stack
    //*
    //* PARAMETERS:
    //*     A \param[out]{ <T> }
    //*         Result of the evaluation of the stack.
    //*
    //* RETURNS:
    //*    None
    //*
    //* DESCRIPTION:
    //*    This function evaluates the RPN stack and returns the result
    //*
    //* EXCEPTIONS:
    //*    \param{scl_bad_alloc}
    //*         EvalutateStack() creates new DNBExpressionConstant
    //* nodes, which may throw scl_bad_alloc if memory is not available.
    //*    \param{DNBEDoesNotExist}
    //*         Thrown if any of the leaf pointers are NULL.
    //*    \param{DNBEZeroDivide}
    //*         Thrown if a divide node divides by zero.
    //*    \param{DNBEOverflowError}
    //*         Thrown for overflow.
    //*    \param{DNBEUnderflowError}
    //*         Thrown for underflow.
    //*
    void
    evaluateStack( T& result )
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEDoesNotExist, DNBEZeroDivide, DNBEOverflowError, DNBEUnderflowError ));

    //*
    //* FUNCTION:
    //*    turnToStack
    //*
    //* SUMMARY:
    //*    Creates a RPN stack from an expression tree.
    //*
    //* PARAMETERS:
    //*     A \param[out]{ RPNStackType& }
    //*         The RPN stack that is this filled.
    //*
    //* RETURNS:
    //*    None
    //*
    //* DESCRIPTION:
    //*    Recursively traces a tree depth first to create a RPN stack
    //* representation of the expression.  Each node pushes a pointer
    //* to itself onto the RPN stack.
    //*
    //* EXCEPTIONS:
    //*    \param{scl_bad_alloc}
    //*        This function pushes pointers onto a STL list that will
    //* throw a scl_bad_alloc if there is not enough memory.
    //*    \param{DNBEDoesNotExist}
    //*        Thrown if a leaf pointer is null
    //*
    virtual
    void
    turnToStack( RPNStackType& RPNstack )
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEDoesNotExist ));

private:
    //
    // Default Constructor, made private so no one can use it
    //
    DNBExpressionRPNStack()
    DNB_THROW_SPEC_NULL;

    //
    // Copy Constructor, made private so no one can use it
    //
    DNBExpressionRPNStack( const DNBExpressionRPNStack<T> &right )
    DNB_THROW_SPEC_NULL;

    //
    // The DNBListStack that stores everything
    //
    DNBListStack<pointer> RPNStack_;
};

//
//  Include the public definition file.
//
#include "DNBExpressionRPNStack.cc"

#endif  /* _DNB_DNBEXPRESSIONRPNSTACK_H_ */
