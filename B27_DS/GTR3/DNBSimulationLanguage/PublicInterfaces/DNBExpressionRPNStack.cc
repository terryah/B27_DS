//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview czo/BPL 01:11:13
 */


//*
//* FILE:
//*     DNBExpressionRPNStack.cc  - public definition file
//*
//* MODULE:
//*     DNBExpressionMD
//*
//* OVERVIEW:
//*     This module provides templates for storing RPN stacks in the DNBExpression Class
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     bperles     11/10/1999  Initial Implementation
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1999, 2000 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*


#include "DNBExpressionOperator.h"

template<class T>
DNBExpressionRPNStack<T>::DNBExpressionRPNStack(pointer topNode)
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEDoesNotExist ))
{
    //
    // Starting at the top node, travel through the tree recursively
    //   converting it into a RPN stack.
    //
    topNode->turnToStack(RPNStack_);

    //
    // Flip the RPNStack so it can be executed (do only once per stack!)
    //
    RPNStack_.flipStack();
}

template<class T>
DNBExpressionRPNStack<T>::~DNBExpressionRPNStack()
    DNB_THROW_SPEC_NULL
{
}

template<class T>
T
DNBExpressionRPNStack<T>::getValue()
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEDoesNotExist, DNBEZeroDivide, DNBEOverflowError, DNBEUnderflowError ))
{
    T result;
    evaluateStack( result );
    return( result );
}

template<class T>
void
DNBExpressionRPNStack<T>::evaluateStack (T &result)
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEDoesNotExist, DNBEZeroDivide, DNBEOverflowError, DNBEUnderflowError ))
{
    //
    // A non-recursive call that evaluates the RPN stack.
    //   (It assumes that stack has already been flipped)
    //
    PostFixStackType PostFix; // A true stack, not a list
    pointer current;          // The current node

    if ( RPNStack_.isEmpty() )
    {
        DNBEDoesNotExist  eNotAvailable(
            DNB_FORMAT( "The RPNStack can not be evaluated because it is empty" ) );
        throw eNotAvailable;
    }

    while ( !RPNStack_.isEmpty() )
    {
        current = RPNStack_.pop();
		current->evaluateType( PostFix );
    }

    //
    // Check to ensure that only a single value remains on the PostFix stack
    //
    if ( PostFix.size() != 1 )
    {
        // The postfix stack does not have a single node on it
        DNBEDoesNotExist  eNotAvailable(
            DNB_FORMAT( "The RPN stack did not reduce to a single operand during evaluation" ) );
        throw eNotAvailable;
    }

    //
    // The single value on the postfix stack is the result of the expressoin
    //
    current = PostFix.front();
    PostFix.pop_front();
    result = current->getValue();

    //
    // Reset the ListStack pointer to the top of the list to the RPN stack
    //   can be reused
    //
    RPNStack_.resetStackPointer();
}

template<class T>
void
DNBExpressionRPNStack<T>::turnToStack( RPNStackType& RPNstack )
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEDoesNotExist ))
{
    pointer newPtr;
    newPtr = this;
    RPNstack.push (newPtr);
}


template<class T>
DNBExpressionRPNStack<T>::DNBExpressionRPNStack()
    DNB_THROW_SPEC_NULL
{
}

template<class T>
DNBExpressionRPNStack<T>::DNBExpressionRPNStack( const DNBExpressionRPNStack<T> &right )
    DNB_THROW_SPEC_NULL
{
}
