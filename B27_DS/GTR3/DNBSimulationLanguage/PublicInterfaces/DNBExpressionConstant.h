//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//*
//* FILE:
//*     DNBExpressionConstant.h    - public header file
//*
//* MODULE:
//*     DNBExpressionMD
//*
//* OVERVIEW:
//*     This is the class for Constant operands that inherits from DNBExpressionOperand
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     BPL         11/10/1999  Initial Implementation
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1999, 2000 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _DNB_DNBEXPRESSIONCONSTANT_H_
#define _DNB_DNBEXPRESSIONCONSTANT_H_

#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>

#include <DNBSimulationLanguage.h>
#include <DNBExpressionOperand.h>
#include <DNBListStack.h>

#include <DNBException.h>

//*
//* CLASS:
//*     DNBExpressionConstant
//*
//* SUMMARY:
//*     Constant node for DNBExpression class DNBListStack Class
//*
//* TEMPLATING:
//*    \param{ T }
//*        <T> is the numerical type that calculations are being
//*     done in.
//*
//* DESCRIPTION:
//*     This class provides for the storage of constant values of type
//* <T>.  Values are loaded on construction.  Values may be retrieved
//* using the getValue() function.
//*
//* USE:
//*     DNBExpression<DNBReal>::pointer ptrc1 =
//*         DNB_NEW DNBExpressionConstant<double>(5);
//*     DNBReal result = ptrc1.getValue();
//*
template<class T>
class DNBExpressionConstant
    : public DNBExpressionOperand<T>
{
	DNB_DECLARE_EXPRESSION(DNBExpressionConstant);
public:
    //*
    //* FUNCTION:
    //*    DNBExpressionConstant
    //*
    //* SUMMARY:
    //*    Initialization constructor
    //*
    //* PARAMETERS:
    //*    A \param[in]{<T>}
    //*        The value to be stored in the node.
    //*
    //* RETURNS:
    //*    Nothing
    //*
    //* DESCRIPTION:
    //*    The initialization constructor takes as an argument the value to be
    //* stored in the node.
    //*
    //* EXCEPTIONS:
    //*    None
    //*
    DNBExpressionConstant( const T& value )
    DNB_THROW_SPEC_NULL;

    //*
    //* FUNCTION:
    //*    ~DNBExpressionConstant
    //*
    //* SUMMARY:
    //*    Destructor
    //*
    //* PARAMETERS:
    //*     None
    //*
    //* RETURNS:
    //*    Nothing
    //*
    //* DESCRIPTION:
    //*    Destructor for DNBExpressionConstant
    //*
    //* EXCEPTIONS:
    //*    None
    //*
    virtual
    ~DNBExpressionConstant()
    DNB_THROW_SPEC_NULL;

    //*
    //* FUNCTION:
    //*    getValue
    //*
    //* SUMMARY:
    //*    Evaluates a node
    //*
    //* PARAMETERS:
    //*    None
    //*
    //* RETURNS:
    //*    Value of type <T>
    //*
    //* DESCRIPTION:
    //*    Causes the node to evaluate itself and return the resulting
    //* value.  For DNBExpressionConstant the value stored in the node
    //* is returned.
    //*
    //* EXCEPTIONS:
    //*    \param{scl_bad_alloc}
    //*         The getValue() implentation for DNBExpressionRPNStack
    //* calls EvalutateStack(), which creates new DNBExpressionConstant
    //* nodes, which may throw scl_bad_alloc if memory is not available.
    //*    \param{DNBEDoesNotExist}
    //*         Thrown if any of the leaf pointers are NULL.
    //*    \param{DNBEZeroDivide}
    //*         Thrown if a divide node divides by zero.
    //*    \param{DNBEOverflowError}
    //*         Thrown for overflow.
    //*    \param{DNBEUnderflowError}
    //*         Thrown for underflow.
    //*
    virtual
    T
    getValue()
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEDoesNotExist, DNBEZeroDivide, DNBEOverflowError, DNBEUnderflowError ));

    //*
    //* FUNCTION:
    //*    turnToStack
    //*
    //* SUMMARY:
    //*    Creates a RPN stack from an expression tree.
    //*
    //* PARAMETERS:
    //*     A \param[out]{ RPNStackType& }
    //*         The RPN stack that is this filled.
    //*
    //* RETURNS:
    //*    None
    //*
    //* DESCRIPTION:
    //*    Recursively traces a tree depth first to create a RPN stack
    //* representation of the expression.  Each node pushes a pointer
    //* to itself onto the RPN stack.
    //*
    //* EXCEPTIONS:
    //*    \param{scl_bad_alloc}
    //*        This function pushes pointers onto a STL list that will
    //* throw a scl_bad_alloc if there is not enough memory.
    //*    \param{DNBEDoesNotExist}
    //*         Thrown if any of the leaf pointers are NULL.
    //*
    virtual
    void
    turnToStack( RPNStackType& RPNstack )
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEDoesNotExist ));

private:
    //
    // Default Constructor, made private so no one can use it
    //
    DNBExpressionConstant()
    DNB_THROW_SPEC_NULL;

    //
    // Copy Constructor, made private so no one can use it, empty implantation
    //
    DNBExpressionConstant( const DNBExpressionConstant<T> &right )
    DNB_THROW_SPEC_NULL;

    //
    // Value stored by node
    //
    T value_;
};

//
//  Include the public definition file.
//
#include "DNBExpressionConstant.cc"

#endif  /* _DNB_DNBEXPRESSIONCONSTANT_H_ */
