//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//*
//* FILE:
//*     DNBExpressionToken.h    - public header file
//*
//* MODULE:
//*     DNBExpressionMD     
//*
//* OVERVIEW:
//*     Provides tokens for use while parsing expression strings
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     BPL         03/14/2000  Initial Implementation
//*     BPL         09/15/2000  Added support of '=' and ';'
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     DWB, VKA    05/08/2000  
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 2000 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _DNB_DNBEXPRESSIONTOKEN_H_
#define _DNB_DNBEXPRESSIONTOKEN_H_

#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>

#include <DNBSimulationLanguage.h>
#include <DNBExpression.h>

// operands
#include <DNBExpressionConstant.h>
#include <DNBExpressionSymbol.h>
#include <DNBExpressionFunction.h>

// Although this class does not directly call the DNBExpression*ArgumentFunction 
// classes, there is a circular dependancey between these classes and 
// DNBExpressionFunction.  Since these classes are derived from DNBExpressionFunction, 
// the only way for DNBExpressionFunction to know about them is to include them here.
#include <DNBExpressionOneArgumentFunction.h>
#include <DNBExpressionTwoArgumentFunction.h>
#include <DNBExpressionThreeArgumentFunction.h>
#include <DNBExpressionFiveArgumentFunction.h>

#include <DNBSymbol.h>
#include <DNBSymbolTable.h>

#include <DNBExpressionSymbolExtractor.h>

#include <DNBException.h>

#include <DNBExpressionWrapperFunctions.h>

//*
//* CLASS:
//*     DNBExpressionToken
//*
//* SUMMARY:
//*     Tokens for use by the expression solver's parser
//*
//* TEMPLATING:
//*     None:
//*
//* DESCRIPTION:
//*     Tokens for use by the expression solver's parser.  See
//* DNBExpressionParser.h for full details
//*
//* EXAMPLE:
//*     DNBExpressionToken::tokenPointer token;
//*     token = new DNBExpressionToken();
//*     token->setTokenAsEndOfLine();
//*
class ExportedByDNBSimulationLanguage DNBExpressionToken
{
public:
    //*
    //* ENUM:
    //*     tokenCategory
    //*
    //* SUMMARY:
    //*     Identifies whether a node is an operator or operand.
    //*
    //* DESCRIPTION:
    //*     This enumeration type represents whether token
    //* is an operator or operand.
    //*
    enum tokenCategory
    {
        //*
        //* SUMMARY:
        //*     The token is an operator.
        //*
        //* DESCRIPTION:
        //*     The token is an operator.  It takes one or
        //* operands and performs an operation on it/them to return a value.
        //*
        OPERATOR, 

        //*
        //* SUMMARY:
        //*     The token is an operand.
        //*
        //* DESCRIPTION:
        //*     The token is an operand.  It is a self 
        //* contained object that can be evaluated to return a value.
        //*
        OPERAND,

        //*
        //* SUMMARY:
        //*     The token has not had it category set yet.
        //*
        UNDEFINED_CATEGORY
    };

    //*
    //* ENUM:
    //*     tokenType
    //*
    //* SUMMARY:
    //*     Identifies the expression object that the token represents.
    //*
    //* DESCRIPTION:
    //*     This enumeration type identifies the expression object that 
    //* the token represents.
    //*
    enum tokenType
    {
        //*
        //* SUMMARY:
        //*     A set numerical value
        //*
        CONSTANT,

        //*
        //* SUMMARY:
        //*     A value stored in the world model as an attribute
        //*
        SYMBOL,

        //*
        //* SUMMARY:
        //*     A pointer to a ENBExpression node on the heap.  It will either
        //* be the top node in an expression tree of a RPN stack node
        //*
        DNB_EXPRESSION,

        //*
        //* SUMMARY:
        //*     The unary minus operator.
        //*
        UNARYMINUS,

        //*
        //* SUMMARY:
        //*     The unary plus operator.
        //*
        UNARYPLUS,

        //*
        //* SUMMARY:
        //*     The plus operator.
        //*
        PLUS,

        //*
        //* SUMMARY:
        //*     The minus operator.
        //*
        MINUS,

        //*
        //* SUMMARY:
        //*     The multiplication operator.
        //*
        MULT,

        //*
        //* SUMMARY:
        //*     The division operator.
        //*
        DIVIDE,

        //*
        //* SUMMARY:
        //*     The exponential operator.
        //*
        EXP,

        //*
        //* SUMMARY:
        //*     The modulus operator.
        //*
        MOD,

        //*
        //* SUMMARY:
        //*     The equals operator.
        //*
        EQUALS,

        //*
        //* SUMMARY:
        //*     The semicolon operator.
        //*
        SEMICOLON,

        //*
        //* SUMMARY:
        //*     The name of a function.  Also signals that the function 
        //* arguments are stored in the following tokens
        //*
        FUNCTION,

        //*
        //* SUMMARY:
        //*     An open parenthesis.
        //*
        OPEN_PAREN,

        //*
        //* SUMMARY:
        //*     A close parenthesis.
        //*
        CLOSE_PAREN,

        //*
        //* SUMMARY:
        //*     A comma, used to seperate function arguments
        //*
        COMMA,

        //*
        //* SUMMARY:
        //*     The last token is a token list.
        //*
        END_OF_LINE,

        //*
        //* SUMMARY:
        //*     A token that has not had it type set yet.
        //*
        UNDEFINED_TYPE
    };

    //*
    //* TYPEDEF:
    //*     self
    //*
    //* SUMMARY:
    //*     A shorthand notation for defining instances of this class inside itself.
    //*
    //* DESCRIPTION:
    //*     This type definition specifies a shorthand notation for defining 
    //* instances of the DNBExpressionToken class inside itself.
    //*
    //*
    typedef 
    DNBExpressionToken 
    self;

    //*
    //* TYPEDEF:
    //*     tokenPointer
    //*
    //* SUMMARY:
    //*     A pointer to an instance of DNBExpressionToken.
    //*
    //* DESCRIPTION:
    //*     This type definition specifies a pointer to an instance of 
    //* DNBExpressionToken that is most likely stored on the heap.
    //*
    typedef 
    self* 
    tokenPointer;

    //*
    //* TYPEDEF:
    //*     listOfArguments
    //*
    //* SUMMARY:
    //*     The list used to store function arguments.
    //*
    //* DESCRIPTION:
    //*     This type definition specifies the a RPN list used to 
    //* store pointers to tokens of type DNB_EXPRESSION and contain
    //* the argument trees or RPN stacks for function arguments.
    //*
    typedef 
    scl_list< DNBExpression<DNBReal>::pointer, DNB_ALLOCATOR( DNBExpression<DNBReal>::pointer ) > 
    listOfArguments;

    //*
    //* FUNCTION:
    //*    DNBExpressionToken
    //*
    //* SUMMARY:
    //*    Default constructor
    //*
    //* PARAMETERS:
    //*    None
    //*
    //* RETURNS:
    //*    Nothing
    //*
    //* DESCRIPTION:
    //*    The default constructor creates a token with its type and category 
    //* set to unknown.
    //*
    //* EXCEPTIONS:
    //*    None
    //*
    DNBExpressionToken()
    DNB_THROW_SPEC_NULL;

    //*
    //* FUNCTION:
    //*    ~DNBExpression
    //*
    //* SUMMARY:
    //*    Destructor
    //*
    //* PARAMETERS:
    //*     None 
    //*
    //* RETURNS:
    //*    Nothing
    //*
    //* DESCRIPTION:
    //*    Destructor for DNBExpression.
    //*
    //* EXCEPTIONS:
    //*    None
    //*
    ~DNBExpressionToken()
    DNB_THROW_SPEC_NULL;

    //*
    //* FUNCTION:
    //*    getType
    //*
    //* SUMMARY:
    //*    Returns the current type of the token.
    //*
    //* PARAMETERS:
    //*     None 
    //*
    //* RETURNS:
    //*    Value of type tokenType
    //*
    //* DESCRIPTION:
    //*    Returns the current type of the token.
    //*
    //* EXCEPTIONS:
    //*    None
    //*
    tokenType 
    getType()
    const
    DNB_THROW_SPEC_NULL;
    
    //*
    //* FUNCTION:
    //*    getCategory
    //*
    //* SUMMARY:
    //*    Returns the current category of the token.
    //*
    //* PARAMETERS:
    //*     None 
    //*
    //* RETURNS:
    //*    Value of type tokenCategory
    //*
    //* DESCRIPTION:
    //*    Returns the current category of the token.
    //*
    //* EXCEPTIONS:
    //*    None
    //*
    tokenCategory 
    getCategory()
    const
    DNB_THROW_SPEC_NULL;

    //*
    //* FUNCTION:
    //*    deactivateToken
    //*
    //* SUMMARY:
    //*    Sets a token to be inactive so that a pointer can not be advanced to it.
    //*
    //* PARAMETERS:
    //*    None 
    //*
    //* RETURNS:
    //*    None
    //*
    //* DESCRIPTION:
    //*    This function allows a token to be removed from the token list without
    //* removing an element from the list.  When the token is set to inactive, it 
    //* will be skipped over when the pointer is advanced.
    //*
    //* EXCEPTIONS:
    //*    None
    //*
    void 
    deactivateToken()
    DNB_THROW_SPEC_NULL;

    //*
    //* FUNCTION:
    //*    isActive
    //*
    //* SUMMARY:
    //*    Returns whether a token is active or not
    //*
    //* PARAMETERS:
    //*     None 
    //*
    //* RETURNS:
    //*    bool 
    //*
    //* DESCRIPTION:
    //*    Returns whether a token is active or not
    //*
    //* EXCEPTIONS:
    //*    None
    //*
    bool 
    isActive()
    const
    DNB_THROW_SPEC_NULL;

    //*
    //* FUNCTION:
    //*    getPrecedence
    //*
    //* SUMMARY:
    //*    Returns the precedence of the the token based on this type
    //*
    //* PARAMETERS:
    //*     None 
    //*
    //* RETURNS:
    //*    Value of type DNBInteger32 
    //*
    //* DESCRIPTION:
    //*    Returns the precedence of the token based on this type.  Valid only 
    //* for operators.
    //*
    //* EXCEPTIONS:
    //*    None
    //*
    DNBInteger32 
    getPrecedence()
    const
    DNB_THROW_SPEC_NULL;

    //*
    //* FUNCTION:
    //*    getExpressionNode
    //*
    //* SUMMARY:
    //*    Returns the pointer to a DNBExpression node stored in the token.
    //*
    //* PARAMETERS:
    //*    None 
    //*
    //* RETURNS:
    //*    pointer of type DNBExpression<DNBReal>::pointer
    //*
    //* DESCRIPTION:
    //*    Returns the pointer to a DNBExpression node stored in the token.  
    //* Valid only for tokens of type DNB_EXPRESSION.
    //*
    //* EXCEPTIONS:
    //*    None
    //*
    DNBExpression<DNBReal>::pointer
    getExpressionNode()
    const
    DNB_THROW_SPEC_NULL;

    //*
    //* FUNCTION:
    //*    setTokenAsOperator
    //*
    //* SUMMARY:
    //*    Sets the token to be an operator and the type passed in.
    //*
    //* PARAMETERS:
    //*    A \param[in]{tokenType} 
    //*         The new type of token
    //*
    //* RETURNS:
    //*    None
    //*
    //* DESCRIPTION:
    //*    Sets the token to be an operator and the type passed in.
    //*
    //* EXCEPTIONS:
    //*    None
    //*
    void 
    setTokenAsOperator( tokenType type )
    DNB_THROW_SPEC_NULL;

    //*
    //* FUNCTION:
    //*    setTokenAsConstant
    //*
    //* SUMMARY:
    //*    Sets the token to be an operand, a constant, and loads in a value to store.
    //*
    //* PARAMETERS:
    //*    A \param[in]{DNBReal} 
    //*         Value of the constant
    //*
    //* RETURNS:
    //*    None
    //*
    //* DESCRIPTION:
    //*    Sets the token to be an operand, a constant, and loads in a value to store.
    //*
    //* EXCEPTIONS:
    //*    None
    //*
    void 
    setTokenAsConstant( DNBReal value )
    DNB_THROW_SPEC_NULL;

    //*
    //* FUNCTION:
    //*    setTokenAsFunction
    //*
    //* SUMMARY:
    //*    Sets the token to be an operand, a function, and loads in the 
    //* name of the function
    //*
    //* PARAMETERS:
    //*    A \param[in]{scl_wstring} 
    //*         Name of the function
    //*
    //* RETURNS:
    //*    None
    //*
    //* DESCRIPTION:
    //*    Sets the token to be an operand, a function, and loads in the 
    //* name of the function
    //*
    //* EXCEPTIONS:
    //*    None
    //*
    void 
    setTokenAsFunction( scl_wstring functionName )
    DNB_THROW_SPEC_NULL;

    //*
    //* FUNCTION:
    //*    setTokenAsSymbol
    //*
    //* SUMMARY:
    //*    Sets the token to be an operand, a symbol, and loads in the symbol 
    //* ID for the symbol
    //*
    //* PARAMETERS:
    //*     A \param[in]{DNBInteger32} 
    //*         ID of the symbol, used to reference name stored in 
    //* DNBExpressionSymbolExtractor class
    //*
    //* RETURNS:
    //*    None
    //*
    //* DESCRIPTION:
    //*    Sets the token to be an operand, a symbol, and loads in the symbol 
    //* ID for the symbol
    //*
    //* EXCEPTIONS:
    //*    None
    //*
    void 
    setTokenAsSymbol( DNBInteger32 value )
    DNB_THROW_SPEC_NULL;
    
    //*
    //* FUNCTION:
    //*    setTokenAsDNBExpression
    //*
    //* SUMMARY:
    //*    Sets the token to be an operand, a DNBExpression, and loads a 
    //* pointer to a DNBExpression node on the heap
    //*
    //* PARAMETERS:
    //*     A \param[in]{DNBExpression<DNBReal>::pointer} 
    //*         Pointer to a DNBExpression node on the heap
    //*
    //* RETURNS:
    //*    None
    //*
    //* DESCRIPTION:
    //*    Sets the token to be an operand, a DNBExpression, and loads a 
    //* pointer to a DNBExpression node on the heap
    //*
    //* EXCEPTIONS:
    //*    None
    //*
    void 
    setTokenAsDNBExpression( DNBExpression<DNBReal>::pointer expNode)
    DNB_THROW_SPEC_NULL;
    
    //*
    //* FUNCTION:
    //*    setTokenAsEndOfLine
    //*
    //* SUMMARY:
    //*    Sets the token to be an operand and a end of line token
    //*
    //* PARAMETERS:
    //*    None
    //*
    //* RETURNS:
    //*    None
    //*
    //* DESCRIPTION:
    //*    Sets the token to be an operand and a end of line token
    //*
    //* EXCEPTIONS:
    //*    None
    //*
    void 
    setTokenAsEndOfLine( )
    DNB_THROW_SPEC_NULL;

    //*
    //* FUNCTION:
    //*    pushFunctionArgument
    //*
    //* SUMMARY:
    //*    Pushes a pointer of a DNBExpression node onto a list.
    //*
    //* PARAMETERS:
    //*     A \param[in]{DNBExpression<DNBReal>::pointer} 
    //*         Pointer to a DNBExpression node on the heap
    //*
    //* RETURNS:
    //*    None
    //*
    //* DESCRIPTION:
    //*    Pushes a pointer to DNBExpression node on the argument list for 
    //* this token.
    //*
    //* EXCEPTIONS:
    //*    \param{scl_bad_alloc}
    //*         Thrown if there is not enough room to add the pointer to 
    //* the list
    //*
    void 
    pushFunctionArgument( DNBExpression<DNBReal>::pointer argument )
    DNB_THROW_SPEC(( scl_bad_alloc ));

    //*
    //* FUNCTION:
    //*    convertFunctionToDNBExpression
    //*
    //* SUMMARY:
    //*    Converts a function token into a DNBExpression token
    //*
    //* PARAMETERS:
    //*     None
    //*
    //* RETURNS:
    //*    None
    //*
    //* DESCRIPTION:
    //*    The function converts a function token into a DNBExpression token.
    //* This results in the creation of a DNBExpressionFunction node being  
    //* created on the heap containing the function and the arguments stored 
    //* in the function token.
    //*
    //* EXCEPTIONS:
    //*    \param{scl_bad_alloc}
    //*         This function creates a DNBExpressionFunction node on the heap, 
    //* scl_bad_alloc is thrown if there is not enough memory
    //*    \param{DNBEInvalidFormat}
    //*         Thrown if the function name stored in this token can not be 
    //* matched to a valid function pointer
    //*
    void 
    convertFunctionToDNBExpression()
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEInvalidFormat ));

    //*
    //* FUNCTION:
    //*    convertConstantToDNBExpression
    //*
    //* SUMMARY:
    //*    Converts a constant token into a DNBExpression token
    //*
    //* PARAMETERS:
    //*     None
    //*
    //* RETURNS:
    //*    None
    //*
    //* DESCRIPTION:
    //*    The function converts a constant token into a DNBExpression token.
    //* This results in the creation of a DNBExpressionConstant node being  
    //* created on the heap containing the value stored in the constant token.
    //*
    //* EXCEPTIONS:
    //*    \param{scl_bad_alloc}
    //*         This function creates a DNBExpressionConstant node on the heap, 
    //* scl_bad_alloc is thrown if there is not enough memory
    //*
    void 
    convertConstantToDNBExpression()
    DNB_THROW_SPEC(( scl_bad_alloc ));

    //*
    //* FUNCTION:
    //*    convertSymbolToDNBExpression
    //*
    //* SUMMARY:
    //*    Converts a symbol token into a DNBExpression token
    //*
    //* PARAMETERS:
    //*     A \param[in]{DNBSymbolTable} 
    //*         The symbol table used to create the DNBSymbol based on the symbol name.
    //*     A \param[in]{DNBExpressionSymbolExtractor::extractorPtr} 
    //*         The symbol extractor class that stores the symbol name based on the symbol ID
    //*
    //* RETURNS:
    //*    None
    //*
    //* DESCRIPTION:
    //*    The function converts a symbol token into a DNBExpression token.
    //* This results in the creation of a DNBExpressionSymbol node being created 
    //* on the heap containing the symbol stored in the symbol token.
    //*
    //* EXCEPTIONS:
    //*    \param{scl_bad_alloc}
    //*         This function creates a DNBExpressionSymbol node on the heap, 
    //* scl_bad_alloc is thrown if there is not enough memory
    //*    \param{DNBEInvalidFormat}
    //*         Thrown inside DNBExpressionSymbol if the symbol table does 
    //* not conatain the symbol name stored in this token
    //*
    void 
    convertSymbolToDNBExpression(
                    DNBSymbolTable& symbolTable,
                    DNBExpressionSymbolExtractor::extractorPtr& extractedSymbolsPtr )
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEInvalidFormat ));

private:
    //
    // Function pointer used to get the functions for the function names parsed
    //
    typedef DNBReal ( *functionPointer1Arg )( DNBReal );
    typedef DNBReal ( *functionPointer2Arg )( DNBReal, DNBReal );
    typedef DNBReal ( *functionPointer3Arg )( DNBReal, DNBReal, DNBReal );
    typedef DNBReal ( *functionPointer5Arg )( DNBReal, DNBReal, DNBReal, DNBReal, DNBReal );

    // 
    // Copy constructor, private, empty implemtation
    // 
    DNBExpressionToken( const DNBExpressionToken &right )
    DNB_THROW_SPEC_NULL;
    
    //
    // If the name matches a function that takes one argument and is supported 
    // by the parser, the function pointer is passed back and 1 is returned.
    //
    bool 
    findFunctionPointer1Arg( scl_wstring name, functionPointer1Arg &ref )
    DNB_THROW_SPEC_NULL;

    //
    // If the name matches a function that takes one argument and is supported 
    // by the parser, the function pointer is passed back and 1 is returned.
    //
    bool 
    findFunctionPointer2Arg( scl_wstring name, functionPointer2Arg &ref )
    DNB_THROW_SPEC_NULL;

    //
    // If the name matches a function that takes one argument and is supported 
    // by the parser, the function pointer is passed back and 1 is returned.
    //
    bool 
    findFunctionPointer3Arg( scl_wstring name, functionPointer3Arg &ref )
    DNB_THROW_SPEC_NULL;
    
    //
    // If the name matches a function that takes one argument and is supported 
    // by the parser, the function pointer is passed back and 1 is returned.
    //
    bool 
    findFunctionPointer5Arg( scl_wstring name, functionPointer5Arg &ref )
    DNB_THROW_SPEC_NULL;

    //
    // This function access the argument list stored in the class and returns 
    // the Nth argument stored
    //
    DNBExpression<DNBReal>::pointer 
    getNthArgument( DNBInteger32 n )
    DNB_THROW_SPEC_NULL;

    //
    // Data
    // 
    // The type of the token
    //
    tokenType 
    type_;

    //
    // The category of the token
    //
    tokenCategory 
    category_;

    //
    // The DNBExpression node on the heap that DNBEXPRESSION tokens point to 
    //
    DNBExpression<DNBReal>::pointer 
    expressionNode_;

    //
    // The value stored in a CONSTANT node
    //
    DNBReal 
    value_;

    //
    // ID to a symbol stored in the DNBExpressionSymbolExtractor class, 
    // used by SYMBOL tokens
    //
    DNBInteger32 
    symbolId_;

    //
    // Name of a function, used by FUNCTION tokens
    //
    scl_wstring 
    functionName_;

    //
    // Argument list for functions, used by FUNCTION tokens
    //
    listOfArguments 
    argumentList_;

    //
    // If 0, keeps pointers from pointing to it.  Allows tokens to be 
    // removed from the list without deleting them (and messing up the iterators)
    //
    bool 
    active_;    
};


#endif  /* _DNB_DNBEXPRESSIONTOKEN_H_ */

