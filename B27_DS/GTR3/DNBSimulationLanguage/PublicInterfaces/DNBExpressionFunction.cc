//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview czo/BPL 01:11:13
 */


//*
//* FILE:
//*     DNBExpressionFunction.cc  - public definition file
//*
//* MODULE:
//*     DNBExpressionMD
//*
//* OVERVIEW:
//*     This inherited class of DNBExpression stores functions using a functor.
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     BPL         11/10/1999  Initial Implementation
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1999, 2000 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*


template<class T>
DNBExpressionFunction<T>::DNBExpressionFunction()
    DNB_THROW_SPEC_NULL
{
}

template<class T>
DNBExpressionFunction<T>::DNBExpressionFunction( DNBExpressionFunction<T> &right )
    DNB_THROW_SPEC_NULL
{
}

template<class T>
DNBExpressionFunction<T>::DNBExpressionFunction( T (*in_function) (T), pointer arg1 )
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    //
    // This constructor takes in the pointer to the function and the arguments and
    // then creates and stores the functor
    //

    //
    // Create a wrapper to call
    //
    DNBExpressionOneArgumentFunction<T>* my_func_ptr =
        DNB_NEW DNBExpressionOneArgumentFunction<T>( in_function );

    //
    // Store a copy of the pointer to keep the reference count above zero
    //
    wrapperClassInstance_ = my_func_ptr;

    //
    // Create the functor
    //
    typedef T (DNBExpressionOneArgumentFunction<T>::*FuncPtr)( pointer );
    functor_ = DNBMakeFunctorM
#if     defined(__IBMCPP__) && (__IBMCPP__ >= 500)
        <T, DNBExpressionOneArgumentFunction<T>, FuncPtr>
#endif
    (
        ( DNBFunctor0wRet<T>* ) 0,
        &( *my_func_ptr ),
        (FuncPtr) &DNBExpressionOneArgumentFunction<T>::callFunction,
        arg1
    );
}

template<class T>
DNBExpressionFunction<T>::DNBExpressionFunction( T (*in_function) (T, T), pointer arg1, pointer arg2 )
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    //
    // This constructor takes in the pointer to the function and the arguments and
    // then creates and stores the functor
    //

    //
    // Create a wrapper to call
    //
    DNBExpressionTwoArgumentFunction<T>* my_func_ptr =
        DNB_NEW DNBExpressionTwoArgumentFunction<T>( in_function );

    //
    // Store a copy of the pointer to keep the reference count above zero
    //
    wrapperClassInstance_ = my_func_ptr;

    //
    // Create the functor
    //
    typedef T (DNBExpressionTwoArgumentFunction<T>::*FuncPtr)( pointer,
        pointer );
    functor_ = DNBMakeFunctorM
#if     defined(__IBMCPP__) && (__IBMCPP__ >= 500)
        <T, DNBExpressionTwoArgumentFunction<T>, FuncPtr>
#endif
    (
        ( DNBFunctor0wRet<T>* ) 0,
        &( *my_func_ptr ),
        (FuncPtr) &DNBExpressionTwoArgumentFunction<T>::callFunction,
        arg1,
        arg2
    );
}

template<class T>
DNBExpressionFunction<T>::DNBExpressionFunction( T (*in_function) (T, T, T), pointer arg1, pointer arg2, pointer arg3 )
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    //
    // This constructor takes in the pointer to the function and the arguments and
    // then creates and stores the functor
    //

    //
    // Create a wrapper to call
    //
    DNBExpressionThreeArgumentFunction<T>* my_func_ptr =
        DNB_NEW DNBExpressionThreeArgumentFunction<T>( in_function );

    //
    // Store a copy of the pointer to keep the reference count above zero
    //
    wrapperClassInstance_ = my_func_ptr;

    //
    // Create the functor
    //
    typedef T (DNBExpressionThreeArgumentFunction<T>::*FuncPtr)( pointer,
        pointer, pointer );
    functor_ = DNBMakeFunctorM
#if     defined(__IBMCPP__) && (__IBMCPP__ >= 500)
        <T, DNBExpressionThreeArgumentFunction<T>, FuncPtr>
#endif
    (
        ( DNBFunctor0wRet<T>* ) 0,
        &( *my_func_ptr ),
        (FuncPtr) &DNBExpressionThreeArgumentFunction<T>::callFunction,
        arg1,
        arg2,
        arg3
    );
}

template<class T>
DNBExpressionFunction<T>::DNBExpressionFunction( T (*in_function) (T, T, T, T, T), pointer arg1, pointer arg2, pointer arg3, pointer arg4, pointer arg5 )
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    //
    // This constructor takes in the pointer to the function and the arguments and
    // then creates and stores the functor
    //

    //
    // Create a wrapper to call
    //
    DNBExpressionFiveArgumentFunction<T>* my_func_ptr =
        DNB_NEW DNBExpressionFiveArgumentFunction<T>( in_function );

    //
    // Store a copy of the pointer to keep the reference count above zero
    //
    wrapperClassInstance_ = my_func_ptr;

    //
    // Create the functor
    //
    typedef T (DNBExpressionFiveArgumentFunction<T>::*FuncPtr)( pointer,
        pointer, pointer, pointer, pointer );
    functor_ = DNBMakeFunctorM
#if     defined(__IBMCPP__) && (__IBMCPP__ >= 500)
        <T, DNBExpressionFiveArgumentFunction<T>, FuncPtr>
#endif
    (
        ( DNBFunctor0wRet<T>* ) 0,
        &( *my_func_ptr ),
        (FuncPtr) &DNBExpressionFiveArgumentFunction<T>::callFunction,
        arg1,
        arg2,
        arg3,
        arg4,
        arg5
    );
}

template<class T>
T
DNBExpressionFunction<T>::getValue()
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEDoesNotExist, DNBEZeroDivide, DNBEOverflowError, DNBEUnderflowError ))
{
    return functor_();
}

template<class T>
void
DNBExpressionFunction<T>::turnToStack( RPNStackType& RPNstack )
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEDoesNotExist ))
{
    pointer newPtr;
    newPtr = this;
    RPNstack.push ( newPtr );
}

