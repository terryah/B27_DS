//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//*
//* FILE:
//*     DNBExpressionStringHandler.h    - public header file
//*
//* MODULE:
//*     DNBExpressionMD     
//*
//* OVERVIEW:
//*     This class proivdes for the coverting of a scl_wstring into a set of 
//* tokens that can be used by the parser
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     BPL         03/14/2000  Initial Implementation
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     DWB, VKA    05/08/2000  
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 2000 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _DNB_DNBEXPRESSIONSTRINGHANDLER_H_
#define _DNB_DNBEXPRESSIONSTRINGHANDLER_H_

#include <DNBSystemBase.h>
#include <scl_string.h>          
#include <DNBSystemDefs.h>

#include <DNBSimulationLanguage.h>
#include <DNBExpression.h>
#include <DNBSymbol.h>
#include <DNBSymbolTable.h>
#include <DNBExpressionToken.h>

#include <DNBException.h>
#include <DNBTempBuffer.h>  // For DNBWCharBuffer


//*
//* CLASS:
//*     DNBExpressionStringHandler
//*
//* SUMMARY:
//*     String tokenizer for the expression solver's parser
//*
//* TEMPLATING:
//*     None:
//*
//* DESCRIPTION:
//*     The class converts objects in string into tokens.  See 
//* DNBExpressionParser.h for a description of the big picture.
//*
//*     The integer variable 'currentPtr_' is used as a pointer to walk 
//* through the string, character by character.  Once the begining of an 
//* object has been localed in the string, one of the 'advanceTo*'  
//* functions is used to find the end of it.
//*
//* EXAMPLE:
//*    DNBExpressionStringHandler handledString( expressionString_, 
//*                                              extractedSymbolsPtr );
//*    DNBExpressionToken::tokenPointer token;
//*    token = new DNBExpressionToken();
//*    while ( handledString.findNextToken( token ) )
//*    {
//*        tokenList_.push_back( token );
//*        token = new DNBExpressionToken();
//*    }
//*    token->setTokenAsEndOfLine();
//*    tokenList_.push_back( token );
//*
class ExportedByDNBSimulationLanguage DNBExpressionStringHandler
{
public:
    //*
    //* FUNCTION:
    //*    DNBExpressionStringHandler
    //*
    //* SUMMARY:
    //*    Initialization constructor
    //*
    //* PARAMETERS:
    //*     A \param[in]{scl_wstring} 
    //*         The string to be tokenized
    //*     A \param[in]{DNBExpressionSymbolExtractor::extractorPtr} 
    //*         The class that holds the symbol names that match the symbol 
    //* IDs stored in the expression string
    //*
    //* RETURNS:
    //*    Nothing
    //*
    //* DESCRIPTION:
    //*    This constructor loads the string and extracted symbols into the 
    //* class.
    //*
    //* EXCEPTIONS:
    //*    None
    //*
    DNBExpressionStringHandler( const scl_wstring expressionString,
                                DNBExpressionSymbolExtractor::extractorPtr& extractedSymbolsPtr )
    DNB_THROW_SPEC_NULL;
    
    //*
    //* FUNCTION:
    //*    ~DNBExpressionStringHandler
    //*
    //* SUMMARY:
    //*    Destructor
    //*
    //* PARAMETERS:
    //*     None 
    //*
    //* RETURNS:
    //*    Nothing
    //*
    //* DESCRIPTION:
    //*    Destructor for DNBExpressionStringHandler.
    //*
    //* EXCEPTIONS:
    //*    None
    //*
    ~DNBExpressionStringHandler()
    DNB_THROW_SPEC_NULL;

    //*
    //* FUNCTION:
    //*    findNextToken
    //*
    //* SUMMARY:
    //*    Identifies the next token in the expression string
    //*
    //* PARAMETERS:
    //*     A \param[in]{DNBExpressionToken::tokenPointer} 
    //*         A pointer to the token instance on the heap that is to be 
    //* loaded with the next token information
    //*
    //* RETURNS:
    //*    bool
    //*
    //* DESCRIPTION:
    //*    Sets the passed token to represent the next object in the string 
    //* based on the current possition of the pointer stored by the class. 
    //* Returns false if token could not be set because the end of the string 
    //* has been reached.  Returns true otherwise.
    //*
    //* EXCEPTIONS:
    //*    \param{DNBEInvalidFormat}
    //*         Thrown if the leading character of an object can't be reconized.
    //*
    bool 
    findNextToken( DNBExpressionToken::tokenPointer& token)
    DNB_THROW_SPEC(( DNBEInvalidFormat ));

private:
    // 
    // Returns 1 if the current object pointed to in the string is an operator 
    // character.  Also does the work of setting the correct type of the token.
    //
    bool 
    isOperator( DNBExpressionToken::tokenPointer& token )
    DNB_THROW_SPEC_NULL;

    //
    // Returns a pointer to the first non-white space character found.
    // Result can be the character initaly pointed to.
    //
    void 
    skipWhiteSpace()
    DNB_THROW_SPEC_NULL;

    //
    // Continue as long as the next digit is a:
    //      letter or number 
    // The pointer is left pointing to the last alphaNumeric character in the object
    //
    DNBInteger32 
    advanceToEndOfNumber( DNBInteger32 ptr )
    DNB_THROW_SPEC(( DNBEInvalidFormat ));

    //
    // Continue as long as the next digit is a:
    //      number 
    // Does not handle scientic notation
    // The pointer is left pointing to the last numeric character in the object
    //
    DNBInteger32
    advanceToEndOfAlphaNumeric( DNBInteger32 ptr )
    DNB_THROW_SPEC_NULL;

    //
    // Continue as long as the next digit is a:
    //      '"' or current symbol ID delimiter
    // The pointer is left pointing to the '"' that ends the symbol
    //
    DNBInteger32 
    advanceToEndOfSymbol( DNBInteger32 ptr )
    DNB_THROW_SPEC_NULL;

    // 
    // Data
    // 
    // The string to be tokenized
    // 
    scl_wstring 
    expressionString_;
    
    // 
    // The index of the current character in the string being tokenized
    // 
    DNBInteger32 
    currentPtr_;

    // 
    // The first character of the current object in the string being tokenized
    // 
    DNBInteger32 
    startOfCurrentToken_;

    // 
    // The character that has been inserted before and after the symbol IDs in the expression string
    // 
    wchar_t 
    symbolIDDelimiter_;

    // 
    // The character used when looking for periods
    // 
    wchar_t 
    period_;

    // 
    // The character used when looking for spaces
    // 
    wchar_t 
    space_;

    // 
    // The character used when looking for tabs
    // 
    wchar_t 
    tab_;

    // 
    // The characters that represent the operators
    // 
    scl_wstring 
    operators_;

};

#endif  /* _DNB_DNBEXPRESSIONSTRINGHANDLER_H_ */
