//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//*
//* FILE:
//*     DNBExpressionMinus.h    - protected header file
//*
//* MODULE:
//*     DNBExpressionMD
//*
//* OVERVIEW:
//*     This is the class for the subtraction operator that inherits from
//* DNBExpressionBinary
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     bperles     03/03/2000  Initial Implementation
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1999, 2000 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _DNB_DNBEXPRESSIONMINUS_H_
#define _DNB_DNBEXPRESSIONMINUS_H_

#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>

#include <DNBSimulationLanguage.h>
#include <DNBExpressionBinary.h>
#include <DNBListStack.h>

#include <DNBException.h>

//*
//* CLASS:
//*     DNBExpressionMinus
//*
//* SUMMARY:
//*     Node in DNBExpression inheritance for representing the subtraction
//* operation.
//*
//* TEMPLATING:
//*    \param{ T }
//*        <T> is the numerical type that calculations are being
//*     done in.  All math operations and functions supported by
//*     the calculator must be defined for type <T>.
//*
//* DESCRIPTION:
//*     This class provides for the representation of the subtraction
//* operation in expressions.  The value of the operation on its
//* operands may be found using the getValue() function.
//*
//* EXAMPLE:
//*     The pointers to two expression trees are passed in as arguments
//* to the constructor.
//*     DNBExpression<DNBReal>::pointer ptrc1 =
//*         DNB_NEW DNBExpressionConstant<DNBReal>(5);
//*     DNBExpression<DNBReal>::pointer ptrc2 =
//*         DNB_NEW DNBExpressionConstant<DNBReal>(7);
//*     DNBExpression<DNBReal>::pointer ptrm1 =
//*         DNB_NEW DNBExpressionMinus<DNBReal>(ptrc1, ptrc2);
//*
template<class T>
class DNBExpressionMinus
    : public DNBExpressionBinary<T>
{
	DNB_DECLARE_EXPRESSION(DNBExpressionMinus);
public:
    //*
    //* FUNCTION:
    //*    DNBExpressionMinus
    //*
    //* SUMMARY:
    //*    Initialization constructor
    //*
    //* PARAMETERS:
    //*     A \ref{pointer}
    //*         Left operand leaf
    //*     A \ref{pointer}
    //*         Right operand leaf
    //*
    //* RETURNS:
    //*    Nothing
    //*
    //* DESCRIPTION:
    //*    The constructor for DNBExpressionMinus takes as arguments the
    //* operands that are on the left and right side of the operator.  The
    //* constructor for DNBExpressionBinary is then called and the operands
    //* passed to it.
    //*
    //* EXCEPTIONS:
    //*    None
    //*
    DNBExpressionMinus( const pointer operand1, const pointer operand2 )
    DNB_THROW_SPEC_NULL;

    //*
    //* FUNCTION:
    //*    ~DNBExpressionMinus
    //*
    //* SUMMARY:
    //*    Destructor
    //*
    //* PARAMETERS:
    //*     None
    //*
    //* RETURNS:
    //*    Nothing
    //*
    //* DESCRIPTION:
    //*    Destructor for DNBExpressionMinus
    //*
    //* EXCEPTIONS:
    //*    None
    //*
    virtual
    ~DNBExpressionMinus()
    DNB_THROW_SPEC_NULL;

    //*
    //* FUNCTION:
    //*    getValue
    //*
    //* SUMMARY:
    //*    Evaluates a node
    //*
    //* PARAMETERS:
    //*    None
    //*
    //* RETURNS:
    //*    Value of type <T>
    //*
    //* DESCRIPTION:
    //*    Causes the node to evaluate itself and return the resulting
    //* value.  For DNBExpressionMinus, getValue() is called on the two
    //* operand leaves, and the result of the subtraction of these two values
    //* is returned.
    //*
    //* EXCEPTIONS:
    //*    \param{scl_bad_alloc}
    //*         The getValue() implentation for DNBExpressionRPNStack
    //* calls EvalutateStack(), which creates new DNBExpressionConstant
    //* nodes, which may throw scl_bad_alloc if memory is not available.
    //*    \param{DNBEDoesNotExist}
    //*         Thrown if any of the leaf pointers are NULL.
    //*    \param{DNBEZeroDivide}
    //*         Thrown if a divide node divides by zero.
    //*    \param{DNBEOverflowError}
    //*         Thrown for overflow.
    //*    \param{DNBEUnderflowError}
    //*         Thrown for underflow.
    //*
    virtual
    T
    getValue()
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEDoesNotExist, DNBEZeroDivide, DNBEOverflowError, DNBEUnderflowError ));

    //*
    //* FUNCTION:
    //*    performOperation
    //*
    //* SUMMARY:
    //*    Evaluates an object on the RPN stack
    //*
    //* PARAMETERS:
    //*     A \ref{PostFixStackType}
    //*         The postfix stack that contains the operators to be operated on.
    //*
    //* RETURNS:
    //*    Value of type <T>
    //*
    //* DESCRIPTION:
    //*    The function allows a operator node on the RPN stack to evaluate
    //* itself when it is poped off the stack.  The needed operators are popped
    //* off postfix stack and the result of the operation is returned.
    //*
    //* EXCEPTIONS:
    //*    \param{DNBEDoesNotExist}
    //*         Thrown if there is not enough operands on the post fix stack
    //*    \param{DNBEZeroDivide}
    //*         Thrown if a divide node divides by zero.
    //*    \param{DNBEOverflowError}
    //*         Thrown for overflow.
    //*    \param{DNBEUnderflowError}
    //*         Thrown for underflow.
    //*
    virtual
    T
    performOperation( PostFixStackType& PostFix )
    DNB_THROW_SPEC(( DNBEDoesNotExist, DNBEZeroDivide, DNBEOverflowError, DNBEUnderflowError )) ;

private:
    //
    // Default Constructor, made private so no one can use it
    //
    DNBExpressionMinus()
    DNB_THROW_SPEC_NULL;

    //
    // Copy Constructor, made private so no one can use it, empty implantation
    //
    DNBExpressionMinus( const DNBExpressionMinus<T> &right )
    DNB_THROW_SPEC_NULL;
};

//
//  Include the public definition file.
//
#include "DNBExpressionMinus.cc"

#endif  /* _DNB_DNBEXPRESSIONMINUS_H_ */
