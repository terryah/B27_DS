//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//*
//* FILE:
//*     DNBExpressionOperator.h    - public header file
//*
//* MODULE:
//*     DNBExpressionMD
//*
//* OVERVIEW:
//*     This is the class for operators that inherits from DNBExpression
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     BPL         11/10/1999  Initial Implementation
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1999, 2000 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _DNB_DNBEXPRESSIONOPERATOR_H_
#define _DNB_DNBEXPRESSIONOPERATOR_H_

#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>

#include <DNBSimulationLanguage.h>
#include <DNBExpression.h>
#include <DNBListStack.h>
#include <DNBExpressionConstant.h>

#include <DNBException.h>

//*
//* CLASS:
//*     DNBExpressionOperator
//*
//* SUMMARY:
//*     Operator node for DNBExpression class DNBListStack Class
//*
//* PREREQUISITES:
//*     DNBExpression class
//*
//* TEMPLATING:
//*    \param{ T }
//*        <T> is the numerical type that calculations are being
//*     done in.  All math operations and functions supported by
//*     the calculator must be defined for type <T>.
//*
//* DESCRIPTION:
//*     This class is one link in the inheritance tree for the
//* DNBExpression class.  All Operators inherit from it.
//*
template<class T>
class DNBExpressionOperator
    : public DNBExpression<T>
{
	DNB_DECLARE_EXPRESSION(DNBExpressionOperator);

public:
    //*
    //* FUNCTION:
    //*    DNBExpressionOperator
    //*
    //* SUMMARY:
    //*    Default constructor
    //*
    //* PARAMETERS:
    //*    None
    //*
    //* RETURNS:
    //*    Nothing
    //*
    //* TEMPLATING:
    //*    \param{ T }
    //*        <T> is the type the math is being done in.
    //*
    //* DESCRIPTION:
    //*    The default constructor calls the DNBExpression constructor
    //* and passes in the OPERATOR argument.
    //*
    //* EXCEPTIONS:
    //*    None
    //*
    DNBExpressionOperator()
    DNB_THROW_SPEC_NULL;

    //*
    //* FUNCTION:
    //*    ~DNBExpressionOperator
    //*
    //* SUMMARY:
    //*    Destructor
    //*
    //* PARAMETERS:
    //*     None
    //*
    //* RETURNS:
    //*    Nothing
    //*
    //* TEMPLATING:
    //*    \param{ T }
    //*        <T> is the type the math is being done in.
    //*
    //* DESCRIPTION:
    //*    Destructor for DNBExpressionOperator
    //*
    //* EXCEPTIONS:
    //*    None
    //*
    virtual
    ~DNBExpressionOperator()
    DNB_THROW_SPEC_NULL;

    //*
    //* FUNCTION:
    //*    performOperation
    //*
    //* SUMMARY:
    //*    Evaluates an object on the RPN stack
    //*
    //* PARAMETERS:
    //*     A \ref{PostFixStackType}
    //*         The postfix stack that contains the operators to be operated on.
    //*
    //* RETURNS:
    //*    Value of type <T>
    //*
    //* DESCRIPTION:
    //*    The function allows a operator node on the RPN stack to evaluate
    //* itself when it is poped off the stack.  The needed operators are popped
    //* off postfix stack and the result of the operation is returned.
    //*
    //* EXCEPTIONS:
    //*    \param{DNBEDoesNotExist}
    //*         Thrown if there is not enough operands on the post fix stack
    //*    \param{DNBEZeroDivide}
    //*         Thrown if a divide node divides by zero.
    //*    \param{DNBEOverflowError}
    //*         Thrown for overflow.
    //*    \param{DNBEUnderflowError}
    //*         Thrown for underflow.
    //*
    virtual
    T
    performOperation( PostFixStackType& PostFix )
    DNB_THROW_SPEC(( DNBEDoesNotExist, DNBEZeroDivide, DNBEOverflowError, DNBEUnderflowError )) = 0;

    //*
    //* FUNCTION:
    //*    evaluateType
    //*
    //* SUMMARY:
    //*    Causes the evaluation of a node based on its type (operator or operand)
    //*
    //* PARAMETERS:
    //*    \param{PostFixStackType}
    //*         The post fix stack that stores the operands already poped of 
    //* the RPN stack.
    //*
    //* RETURNS:
    //*    None
    //*
    //* DESCRIPTION:
    //*    This function evaluates an operator node by callings its 
    //* performOperation function
    //*
    //* EXCEPTIONS:
    //*    \param{scl_bad_alloc}
    //*         The evaluateType implentation for DNBExpressionOperator
    //* creates new DNBExpressionConstand nodes, which may cause scl_bad_alloc to 
    //* be thrown if memory is not available.
    //*    \param{DNBEDoesNotExist}
    //*         Thrown if any of the leaf pointers are NULL.
    //*    \param{DNBEZeroDivide}
    //*         Thrown if a divide node divides by zero.
    //*    \param{DNBEOverflowError}
    //*         Thrown for overflow.
    //*    \param{DNBEUnderflowError}
    //*         Thrown for underflow.
    //*
    virtual
    void 
    evaluateType( PostFixStackType& PostFix )
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEDoesNotExist, DNBEZeroDivide, DNBEOverflowError, DNBEUnderflowError ));

private:
    //*
    //* FUNCTION:
    //*    DNBExpressionOperator
    //*
    //* SUMMARY:
    //*    Copy constructor
    //*
    //* PARAMETERS:
    //*     A \ref{DNBExpression}
    //*
    //* RETURNS:
    //*    Nothing
    //*
    //* TEMPLATING:
    //*    \param{ T }
    //*        <T> is the type the math is being done in.
    //*
    //* DESCRIPTION:
    //*    The default constructor is declared private so that it can
    //* not be used. It is an empty implantation.
    //*
    //* EXCEPTIONS:
    //*    None
    //*
    DNBExpressionOperator( const DNBExpressionOperator<T> &right )
    DNB_THROW_SPEC_NULL;

    //
    // This section used by the dynamic_cast()
    //
    typedef
    DNBExpressionOperator<T>*
    operatorPtrType;

};

//
//  Include the public definition file.
//
#include "DNBExpressionOperator.cc"


#endif  /* _DNB_DNBEXPRESSIONOPERATOR_H_ */
