//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//*
//* FILE:
//*     DNBExpressionUnary.h    - public header file
//*
//* MODULE:
//*     DNBExpressionMD
//*
//* OVERVIEW:
//*     This is the class for unary operators that inherits from DNBExpressionOperator
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     BPL         11/10/1999  Initial Implementation
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1999, 2000 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _DNB_DNBEXPRESSIONUNARY_H_
#define _DNB_DNBEXPRESSIONUNARY_H_

#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>

#include<DNBException.h>
#include<DNBListStack.h>
#include<DNBExpressionOperator.h>

//*
//* CLASS:
//*     DNBExpressionUnary
//*
//* SUMMARY: 
//*     Unary operator node for DNBExpression class
//*
//* TEMPLATING:
//*    \param{ T }
//*        <T> is the numerical type that calculations are being 
//*     done in.  All math operations and functions supported by 
//*     the calculator must be defined for type <T>.
//*
//* DESCRIPTION:
//*     This class is one link in the inheritance tree for the 
//* DNBExpression class.  All unary operators inherit from it.
//*
template<class T>
class DNBExpressionUnary : public DNBExpressionOperator<T> 
{
	DNB_DECLARE_EXPRESSION(DNBExpressionUnary);
public:
    //*
    //* FUNCTION:
    //*    DNBExpressionUnary
    //*
    //* SUMMARY:
    //*    Initialization constructor
    //*
    //* PARAMETERS:
    //*     A \ref{pointer} 
    //*         operand leaf, the operand on the right side of the operator.
    //*
    //* RETURNS:
    //*    Nothing
    //*
    //* DESCRIPTION:
    //*    This constructor is called by the constructor for 
    //* derived classes  and takes as input the operands that are on the 
    //* left and right side of the operator. 
    //*
    //* EXCEPTIONS:
    //*    None
    //*
    DNBExpressionUnary ( pointer operand1 ) 
    DNB_THROW_SPEC_NULL;

    //*
    //* FUNCTION:
    //*    turnToStack
    //*
    //* SUMMARY:
    //*    Creates a RPN stack from an expression tree.
    //*
    //* PARAMETERS: 
    //*     A \param[out]{ RPNStackType& } 
    //*         The RPN stack that is this filled.
    //*
    //* RETURNS:
    //*    None
    //*
    //* DESCRIPTION: 
    //*    Recursively traces a tree depth first to create a RPN stack
    //* representation of the expression.  Each node pushes a pointer 
    //* to itself onto the RPN stack.
    //*
    //* EXCEPTIONS:
    //*    \param{scl_bad_alloc}
    //*        This function pushes pointers onto a STL list that will 
    //* throw a scl_bad_alloc if there is not enough memory.
    //*    \param{DNBEDoesNotExist}
    //*         Thrown if any of the leaf pointers are NULL.
    //*
    virtual
    void 
    turnToStack( RPNStackType& RPNstack ) 
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEDoesNotExist ));

protected:
    DNBExpressionUnary() {}

    //
    // operand leaf, the operand to the right of the operator
    //
    pointer operand1_;
};

//
//  Include the public definition file.
//
#include "DNBExpressionUnary.cc"


#endif  /* _DNB_DNBEXPRESSIONUNARY_H_ */
