//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview czo/BPL 01:11:13
 */


//*
//* FILE:
//*     DNBExpressionOperand.cc  - public definition file
//*
//* MODULE:
//*     DNBExpressionMD
//*
//* OVERVIEW:
//*     This class provides templates for operands in the DNBExpression Class
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     BPL         11/10/1999  Initial Implementation
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1999, 2000 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*


template<class T>
DNBExpressionOperand<T>::DNBExpressionOperand() 
    DNB_THROW_SPEC_NULL 
{
}

template<class T>
DNBExpressionOperand<T>::~DNBExpressionOperand() 
    DNB_THROW_SPEC_NULL
{
}

template<class T>
DNBExpressionOperand<T>::DNBExpressionOperand( const DNBExpressionOperand<T> &right ) 
    DNB_THROW_SPEC_NULL 
{
}

template<class T>
void 
DNBExpressionOperand<T>::evaluateType( PostFixStackType& PostFix )
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEDoesNotExist, DNBEZeroDivide, DNBEOverflowError, DNBEUnderflowError ))
{
    //
    // Push operands on to the postfix stack untill they are needed
    //
    pointer current;
    current = this;
    PostFix.push_front( current );
}
