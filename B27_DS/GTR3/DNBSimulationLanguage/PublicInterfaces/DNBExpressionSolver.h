//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//*
//* FILE:
//*     DNBExpressionSolver.h
//*
//* MODULE:
//*     DNBExpressionMD
//*
//* OVERVIEW:
//*     This is the class that the end user uses to interface with the expression solver
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     BPL         01/18/2000  Initial Implementation
//*     bpl         09/19/2000  Added support for '=', ';', int(), range(), inrange(), and ramp()
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 2000 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _DNB_DNBEXPRESSIONSOLVER_H_
#define _DNB_DNBEXPRESSIONSOLVER_H_

#include <DNBSystemBase.h>
#include <scl_string.h>
#include <DNBSystemDefs.h>

#include <DNBSimulationLanguage.h>
#include <DNBExpressionOperand.h>
#include <DNBExpressionRPNStack.h>
#include <DNBListStack.h>
#include <DNBExpressionParserLink.h>
#include <DNBException.h>
#include <DNBCountedPointer.h>


//*
//* CLASS:
//*     DNBExpressionSolver
//*
//* SUMMARY:
//*     User interface to the expression solver
//*
//* TEMPLATING:
//*    \param{ T }
//*        <T> is the numerical type that calculations are being
//*     done in.
//*
//* DESCRIPTION:
//*   This class is the user interface to the expression solver.
//* An instance of this class is created and given an expression string
//* and a copy of a symbol table.  Internaly, the string is parsed, compiled, 
//* and stored as a RPN stack.  The current value of the expression can be
//* requested using the getValue() method.
//*
//*   The expression solver is designed to convert string based
//* mathematical expressions into a form that can be evaluated by the
//* computer.  These expressions are then stored in a compiled form
//* until they are needed for evaluation.
//*
//* Supported functionality:
//*
//* Basic math:
//*   add(+), subtract(-), multiply(*), divide(/), exponential(^),
//* modulus(%), unary minus(-), unary plus(+), parenthesis, 
//* and decimal numbers.
//*
//* Functions:
//*   Function format matches C++: <name>(<arg>, <arg>, <arg>)
//* Supported functions are: abs, acos, acosd, asin, asind, atan, atand,
//* cos, cosd, exp, log, log10, sin, sind, int, sqrt, tan, tand, floor, ceil,
//* atan2, atand2, max, min, mod, pow, range, inrange, ramp, sass, sasa, sssa, 
//* sasssa1, sasssa2, sasasa, and sasass.  Trig functions are in radians unless
//* they have a 'd' tacked onto their name, which indicates degrees.
//*
//* Symbols:
//*   Symbols provide access to the value of attributes in the world
//* model.  Symbols must be enclosed in symbol delimiters.  The
//* delimiter character is a user specified option that defaults to
//* double quotes (").  The world data model determines the formatting
//* of symbols.
//*   Note: A hack currently allows the expression solver to create symbols 
//* that show up in the expressions but are not found in the WDM.  The values
//* are stored in DNBRealData instances stored on the heap.  This results in  
//* a memory leak that will not be fixed untill the symbols section is redesigned.
//*   Note: Temporary symbols (symbols created by the expression solver) are 
//* stored at the device level.  Because of this, all expressions owned by a 
//* device use the same symbol space and can share data though and overwrite 
//* each other's symbols.
//*   Note: Constants (PI, DEG, RAD, etc) are not currently supported by the WDM.
//* D5 equations are currently put through a pre-parser when loaded that replaces
//* the constant with its value.
//*
//* Other operators:
//*   The equals sign(=) allows a value to be assigned to a symbol.  A symbol 
//* must be on the left hand side of the equals sign.  The semicolon(;) is 
//* used to seperate multiple expressions stored in the same string.  The 
//* result of the right most expresion is returned.  The other expressions 
//* may be used to set symbol values.
//*
//* Changes from D5 IGCALC and other limitations:
//*   Scientific notation is not currently supported.
//*   Although the expression class is templated, the parser is fixed to 
//* work in the type DNBReal.  This can be changed when the functionality
//* is needed.
//*
//* Example expression:
//* -0.500000*(0.793703 - sssa(150.166245 + "dof(1)", 168.16352, 208.33683 ))
//*
//* USE:
//*   DNBExpressionSolver is accessed through a solverPointer that is a 
//* smart pointer to a DNBExpressionSolver instance.  A DNBExpression::pointer
//* is not used because the getString and setString methods are not members 
//* of DNBExpression and would require a dynamic cast if 'pointer' was used.
//*
//*     BasicEntity3D::Pointer pEnt( hEnt1 ) = getentity();
//*     DNBWDMSymbolTable table( hEnt1 );
//*     DNBExpressionSolver<DNBReal>::solverPointer ptr =
//*            new DNBExpressionSolver<DNBReal>( L"1+2*3", symbolTable );
//*     DNBReal result = ptr->getValue();
//*     scl_wstring str1 = ptr->getString();
//*     ptr->setString( str1, symbolTable );
//*
template<class T>
class DNBExpressionSolver
    : public DNBExpressionOperand<T>
{
	DNB_DECLARE_EXPRESSION(DNBExpressionSolver);
public:
    //*
    //* TYPEDEF:
    //*     self
    //*
    //* SUMMARY:
    //*     A shorthand notation for defining instances of this class inside itself.
    //*
    //* DESCRIPTION:
    //*     This type definition specifies a shorthand notation for defining
    //* instances of the DNBExpression class inside itself.
    //*
    //*
    typedef
    DNBExpressionSolver<T>
    self;

    //*
    //* TYPEDEF:
    //*     pointer
    //*
    //* SUMMARY:
    //*     The smart pointer to an instance of DNBExpressionSolver.
    //*
    //* DESCRIPTION:
    //*     This type definition specifies the smart pointer that points
    //* to an instance of DNBExpressionSolver on the heap.  DNBExpressionSolver
    //* has its own pointer because it has methods that are not part of 
    //* DNBExpression.  A DNBExpression pointer would require a dynamic cast.
    //*
    typedef
    DNBCountedPointer< self >
    solverPointer;
    
    //*
    //* FUNCTION:
    //*    DNBExpressionSolver
    //*
    //* SUMMARY:
    //*    Default constructor
    //*
    //* PARAMETERS:
    //*    A \param[in]{scl_wstring}
    //*        The expression string to be evaluated.
    //*    A \param[in]{DNBSymbolTable}
    //*        A symbol tabel that provides access to attributes in the world 
    //* model.
    //*    A \param[in]{const wchar_t}
    //*        The character that delimits the names of entites that make up 
    //* the path for a symbol.  Defaults to '/'.
    //*    A \param[in]{const wchar_t}
    //*        The character that delimits the attributes of symbols.  
    //* Defaults to ':'.
    //*    A \param[in]{const wchar_t}
    //*        The character that proceds and follows a symbol (and path) to 
    //* delimit from the rest of the expression.  Defaults to '".'
    //*
    //* RETURNS:
    //*    Nothing
    //*
    //* DESCRIPTION:
    //*    The initialization constructor automatically parses the expression 
    //* into an tree and RPN Stack.  The SymbolTable passed in is used to 
    //* access symbols stored in the work data model.  If the parsing can not 
    //* be completed, an exception is thrown.
    //*
    //* EXCEPTIONS:
    //*    \param{scl_bad_alloc}
    //*         This function causes the creation of a expression tree on the heap.
    //* If sufficient space is not available, scl_bad_alloc is thrown.
    //*    \param{DNBEInvalidFormat}
    //*          A syntax error was found during parsing of the expression
    //*    \param{DNBEDoesNotExist}
    //*         Thrown if a pointer is unexpectedly NULL.
    //*
    DNBExpressionSolver( const scl_wstring& expressionString,
                         DNBSymbolTable& symbolTable,
                         const wchar_t  pathDelimiter = L'/',
                         const wchar_t  attrDelimiter = L':',
                         const wchar_t  symbolDelimiter = L'\"' )
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEInvalidFormat, DNBEDoesNotExist ));

    //*
    //* FUNCTION:
    //*    DNBExpressionSolver
    //*
    //* SUMMARY:
    //*    Copy constructor
    //*
    //* PARAMETERS:
    //*     None
    //*
    //* RETURNS:
    //*    Nothing
    //*
    //* DESCRIPTION:
    //*    Copy constructor, makes this instance point to the same tree
    //* and RPN stack as the source.
    //*
    //* EXCEPTIONS:
    //*    None
    //*
    DNBExpressionSolver( const DNBExpressionSolver<T> &right )
    DNB_THROW_SPEC_NULL;

    //*
    //* FUNCTION:
    //*    ~DNBExpressionSolver
    //*
    //* SUMMARY:
    //*    Destructor
    //*
    //* PARAMETERS:
    //*     None
    //*
    //* RETURNS:
    //*    Nothing
    //*
    //* DESCRIPTION:
    //*    Destructor for DNBExpressionSolver
    //*
    //* EXCEPTIONS:
    //*    None
    //*
    virtual
    ~DNBExpressionSolver()
    DNB_THROW_SPEC_NULL;

    //*
    //* FUNCTION:
    //*    GetString
    //*
    //* SUMMARY:
    //*    Returns the expression string stored in the class
    //*
    //* PARAMETERS:
    //*     None
    //*
    //* RETURNS:
    //*    The expression in the form of a scl_wstring
    //*
    //* DESCRIPTION:
    //*    This function returns the current expression string stored in the class.
    //*
    //* EXCEPTIONS:
    //*    None
    //*
    scl_wstring
    getString()
    DNB_THROW_SPEC_NULL;

    //*
    //* FUNCTION:
    //*    SetString
    //*
    //* SUMMARY:
    //*    Loads a new expression string in the class
    //*
    //* PARAMETERS:
    //*    A \param[in]{scl_wstring}
    //*        The expression string to be evaluated.
    //*    A \param[in]{DNBSymbolTable}
    //*        A symbol tabel that provides access to attributes in the world model.
    //*    A \param[in]{const wchar_t}
    //*        The character that delimits the names of entites that make up the path for a symbol.  Defaults to '/'.
    //*    A \param[in]{const wchar_t}
    //*        The character that delimits the attributes of symbols.  Defaults to ':'.
    //*    A \param[in]{const wchar_t}
    //*        The character that proceds and follows a symbol (and path) to delimit from the rest of the expression.  Defaults to '".'
    //*
    //* RETURNS:
    //*    Nothing
    //*
    //* DESCRIPTION:
    //*    This function loads a new expression string into the class. The string
    //* is parsed and compiled.  Any previous string, tree, and RPN stack is
    //* destroyed.
    //*
    //* EXCEPTIONS:
    //*    \param{scl_bad_alloc}
    //*         This function causes the creation of a expression tree on the heap.
    //* If sufficient space is not available, scl_bad_alloc is thrown.
    //*    \param{DNBEInvalidFormat}
    //*          A syntax error was found during parsing of the expression
    //*    \param{DNBEDoesNotExist}
    //*         Thrown if a pointer is unexpectedly NULL.
    //*
    void
    setString( const scl_wstring& expressionString,
               DNBSymbolTable& symbolTable,
               const wchar_t  pathDelimiter = L'/',
               const wchar_t  attrDelimiter = L':',
               const wchar_t  symbolDelimiter = L'\"' )
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEInvalidFormat, DNBEDoesNotExist ));

    //*
    //* FUNCTION:
    //*    getValue
    //*
    //* SUMMARY:
    //*    Evaluates a node
    //*
    //* PARAMETERS:
    //*    None
    //*
    //* RETURNS:
    //*    Value of type <T>
    //*
    //* DESCRIPTION:
    //*    Causes the node to evaluate itself and return the resulting
    //* value.
    //*
    //* EXCEPTIONS:
    //*    \param{scl_bad_alloc}
    //*         The getValue implentation for DNBExpressionRPNStack
    //* calls EvalutateStack(), which creates new DNBExpressionConstant
    //* nodes, which may throw scl_bad_alloc if memory is not available.
    //*    \param{DNBEDoesNotExist}
    //*         Thrown if any of the leaf pointers are NULL.
    //*    \param{DNBEZeroDivide}
    //*         Thrown if a divide node divides by zero.
    //*    \param{DNBEOverflowError}
    //*         Thrown for overflow.
    //*    \param{DNBEUnderflowError}
    //*         Thrown for underflow.
    //*
    virtual
    T
    getValue()
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEDoesNotExist, DNBEZeroDivide, DNBEOverflowError, DNBEUnderflowError ));

    //*
    //* FUNCTION:
    //*    turnToStack
    //*
    //* SUMMARY:
    //*    Creates a RPN stack from an expression tree.
    //*
    //* PARAMETERS:
    //*     A \param[out]{ RPNStackType& }
    //*         The RPN stack that is this filled.
    //*
    //* RETURNS:
    //*    None
    //*
    //* DESCRIPTION:
    //*    Recursively traces a tree depth first to create a RPN stack
    //* representation of the expression.  Each node pushes a pointer
    //* to itself onto the RPN stack.
    //*
    //* EXCEPTIONS:
    //*    \param{scl_bad_alloc}
    //*        This function pushes pointers onto a STL list that will
    //* throw a scl_bad_alloc if there is not enough memory.
    //*    \param{DNBEDoesNotExist}
    //*         Thrown if any of the leaf pointers are NULL.
    //*
    virtual
    void
    turnToStack( RPNStackType& RPNstack )
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEDoesNotExist ));

private:
    //
    // Default Constructor, made private so no one can use it
    //
    DNBExpressionSolver()
    DNB_THROW_SPEC_NULL;

    //
    // Data
    //
    // The expression string
    //
    scl_wstring
    expressionString_;

    //
    // A pointer to the top node in the expression tree
    //
    pointer
    topNode_;

    //
    // The RPN stack containing pointers to the expression tree nodes
    //
    pointer
    RPNStack_;

    //
    // The delimiter that seperates names in the scope path
    //
    wchar_t
    pathDelimiter_;

    //
    // The delimiter that seperates symbol attributes from symbols???
    //
    wchar_t
    attrDelimiter_;

    //
    // The delimiter that seperates complete symbols from the rest of the expression
    //
    wchar_t
    symbolDelimiter_;
};

//
//  Include the public definition file.
//
#include "DNBExpressionSolver.cc"

#endif  /* _DNB_DNBEXPRESSIONSOLVER_H_ */
