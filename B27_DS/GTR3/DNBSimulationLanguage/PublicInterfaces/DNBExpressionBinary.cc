//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview czo/BPL 01:11:13
 */


//*
//* FILE:
//*     DNBExpressionBinary.cc  - public definition file
//*
//* MODULE:
//*     DNBExpressionMD
//*
//* OVERVIEW:
//*     This module provides templates for Binary Operators in the DNBExpression Class
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     BPL         11/10/1999  Initial Implementation
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1999, 2000 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*


template<class T>
DNBExpressionBinary<T>::DNBExpressionBinary( pointer operand1, pointer operand2 ) 
    DNB_THROW_SPEC_NULL
	:  operand1_( operand1 ), operand2_( operand2 )
{
}

template<class T>
DNBExpressionBinary<T>::~DNBExpressionBinary() 
    DNB_THROW_SPEC_NULL
{
}

template<class T>
void
DNBExpressionBinary<T>::turnToStack( RPNStackType& RPNstack ) 
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEDoesNotExist ))
{
    if ( ( !operand1_ ) || ( !operand2_ ) )
    {
        DNBEDoesNotExist  eNotAvailable(
            DNB_FORMAT( "The expression tree can not be compiled into a RPN stack because one of the leaf pointers is NULL" ) );
  	    throw eNotAvailable;
    }

    //
    // Create a new pointer to this node and push it onto the RPN stack
    //
    operand1_->turnToStack( RPNstack );
    operand2_->turnToStack( RPNstack );
    pointer newPtr;	
    newPtr = this;
    RPNstack.push ( newPtr );
}

template<class T>
DNBExpressionBinary<T>::DNBExpressionBinary() 
    DNB_THROW_SPEC_NULL 
{
}

template<class T>
DNBExpressionBinary<T>::DNBExpressionBinary( const DNBExpressionBinary<T> &right ) 
    DNB_THROW_SPEC_NULL 
{
}
