//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview czo/BPL 01:11:13
 * @quickreview mmg/rtl 09:09:29 IR A0665132WIM Fix
 */


//*
//* FILE:
//*     DNBExpressionOperator.cc  - public definition file
//*
//* MODULE:
//*     DNBExpressionMD
//*
//* OVERVIEW:
//*     This module provides templates for operators in the DNBExpression Class
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     BPL         11/10/1999  Initial Implementation
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1999, 2000 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*


template<class T>
DNBExpressionOperator<T>::DNBExpressionOperator()
    DNB_THROW_SPEC_NULL 
{
}

template<class T>
DNBExpressionOperator<T>::~DNBExpressionOperator() 
    DNB_THROW_SPEC_NULL
{
}

template<class T>
DNBExpressionOperator<T>::DNBExpressionOperator( const DNBExpressionOperator<T> &right ) 
    DNB_THROW_SPEC_NULL 
{
}

template<class T>
void 
DNBExpressionOperator<T>::evaluateType( PostFixStackType& PostFix )
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEDoesNotExist, DNBEZeroDivide, DNBEOverflowError, DNBEUnderflowError ))
{
    T heldResult;           // The result of each operation
    /*
    pointer current;        // Pointer to self

    //
    // These variables are used by the dynamic_cast()
    //
    operatorPtrType operatorPtr;
    DNBExpression<T>* expPtr;

    //
    // Uses dynamic_cast to convert the pointer to type DNBExpressionOperator
    //    so that the performOperation function can be found.
    //
    current = this;
    expPtr = &(*current);    // Copy to non-RW pointer
    operatorPtr = dynamic_cast<operatorPtrType>(expPtr);
    if ( operatorPtr == NULL )
    {
        DNBEDoesNotExist  error(
            DNB_FORMAT( "Internal Evaluation Error: Can not cast pointer to DNBExpressionOperator*" ) );
        throw error;
    }

    //
    // Evaluate the operator using operands on the postfix stack
    //
    heldResult = operatorPtr->performOperation( PostFix );
    */

    //IR A0665132WIM Fix. Unnecessary upcasting and downcasting not required
    heldResult = this->performOperation( PostFix );

    //
    // Create new node to store the new value and stick a pointer to
    //   it on the PostFix stack
    //
//    self* temp = DNB_NEW DNBExpressionConstant<T>(heldResult);
    pointer tempPtr = DNB_NEW DNBExpressionConstant<T>(heldResult);
    PostFix.push_front( tempPtr );
}

