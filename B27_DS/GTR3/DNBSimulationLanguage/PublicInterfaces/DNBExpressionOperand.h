//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//*
//* FILE:
//*     DNBExpressionOperand.h    - public header file
//*
//* MODULE:
//*     DNBExpressionMD      
//*
//* OVERVIEW:
//*     This is the class for operands that inherits from DNBExpression
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     BPL         11/10/1999  Initial Implementation
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1999, 2000 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _DNB_DNBEXPRESSIONOPERAND_H_
#define _DNB_DNBEXPRESSIONOPERAND_H_

#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>

#include <DNBSimulationLanguage.h>
#include <DNBExpression.h>
#include <DNBListStack.h>

#include <DNBException.h>

//*
//* CLASS:
//*     DNBExpressionOperand
//*
//* SUMMARY: 
//*     Operand node for DNBExpression class DNBListStack Class 
//*
//* TEMPLATING:
//*    \param{ T }
//*        <T> is the numerical type that calculations are being 
//*     done in.  All math operations and functions supported by 
//*     the calculator must be defined for type <T>.
//*
//* DESCRIPTION:
//*     This class is one link in the inheritance tree for the 
//* DNBExpression class.  All Operands inherit from it.
//*
template<class T>
class DNBExpressionOperand 
    : public DNBExpression<T> 
{
	DNB_DECLARE_EXPRESSION(DNBExpressionOperand);
public:
    //*
    //* FUNCTION:
    //*    DNBExpressionOperand
    //*
    //* SUMMARY:
    //*    Default constructor
    //*
    //* PARAMETERS:
    //*    None
    //*
    //* RETURNS:
    //*    Nothing
    //*
    //* DESCRIPTION:
    //*    The default constructor for DNBExpressionOperand
    //*
    //* EXCEPTIONS:
    //*    None
    //*
    DNBExpressionOperand()
    DNB_THROW_SPEC_NULL;

    //*
    //* FUNCTION:
    //*    ~DNBExpressionOperand
    //*
    //* SUMMARY:
    //*    Destructor
    //*
    //* PARAMETERS:
    //*     None 
    //*
    //* RETURNS:
    //*    Nothing
    //*
    //* DESCRIPTION:
    //*    Destructor for DNBExpressionOperand
    //*
    //* EXCEPTIONS:
    //*    None
    //*
    virtual
    ~DNBExpressionOperand()
    DNB_THROW_SPEC_NULL;

    //*
    //* FUNCTION:
    //*    evaluateType
    //*
    //* SUMMARY:
    //*    Causes an operand node to be evaluated
    //*
    //* PARAMETERS:
    //*    \param{PostFixStackType}
    //*         The post fix stack that stores the operands already poped of 
    //* the RPN stack.
    //*
    //* RETURNS:
    //*    None
    //*
    //* DESCRIPTION:
    //*    This function pushes an operand node onto the post fix stack
    //*
    //* EXCEPTIONS:
    //*    \param{scl_bad_alloc}
    //*         The evaluateType implentation for DNBExpressionOperator
    //* creates new DNBExpressionConstand nodes, which may cause scl_bad_alloc to 
    //* be thrown if memory is not available.
    //*    \param{DNBEDoesNotExist}
    //*         Thrown if any of the leaf pointers are NULL.
    //*    \param{DNBEZeroDivide}
    //*         Thrown if a divide node divides by zero.
    //*    \param{DNBEOverflowError}
    //*         Thrown for overflow.
    //*    \param{DNBEUnderflowError}
    //*         Thrown for underflow.
    //*
    virtual
    void 
    evaluateType( PostFixStackType& PostFix )
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEDoesNotExist, DNBEZeroDivide, DNBEOverflowError, DNBEUnderflowError ));

private:
    // 
    // Copy Constructor, made private so no one can use it, empty implantation
    // 
    DNBExpressionOperand( const DNBExpressionOperand<T> &right )
    DNB_THROW_SPEC_NULL;
};

//
//  Include the public definition file.
//
#include "DNBExpressionOperand.cc"


#endif  /* _DNB_DNBEXPRESSIONOPERAND_H_ */
