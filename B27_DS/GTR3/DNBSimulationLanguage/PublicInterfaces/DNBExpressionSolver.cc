//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview czo/BPL 01:11:13
 */


//*
//* FILE:
//*     DNBExpressionSolver.cc  - public definition file
//*
//* MODULE:
//*     DNBExpressionMD
//*
//* OVERVIEW:
//*     This module provides
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     BPL         01/18/2000  Initial Implementation
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     bpl         06/15/2000  All exceptions are caught, writen to cout, and 
//*                             then re-thrown.  Is that was we want to do?
//*
//* COPYRIGHT:
//*     Copyright (C) 2000 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*

template<class T>
DNBExpressionSolver<T>::DNBExpressionSolver( const scl_wstring& expressionString,
                                             DNBSymbolTable& symbolTable,
                                             const wchar_t pathDelimiter,
                                             const wchar_t attrDelimiter,
                                             const wchar_t symbolDelimiter )
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEInvalidFormat, DNBEDoesNotExist ))
    : expressionString_(expressionString),
      pathDelimiter_(pathDelimiter),
      attrDelimiter_(attrDelimiter),
      symbolDelimiter_(symbolDelimiter)
{
    //
    // Set pointers to null
    //
    topNode_ = NULL;
    RPNStack_ = NULL;

    //
    // Create the expression tree through pre-parsing and parsing
    //
    try
    {
        CallParser( expressionString_,
                    topNode_,
                    symbolTable,
                    pathDelimiter_,
                    attrDelimiter_,
                    symbolDelimiter_ );
    }
    catch( DNBEInvalidFormat& error )
    {
        // This can be caused by both user syntax errors and problems with the parser
        cout << "Invalid Format exception caught by DNBExpressionSolver during parsing.  Exiting parser." << endl;
        cout << "Error Reads: " << error.what() << endl;

        // Pass the exception on up
        throw error;
    }

    //
    // Create and store the RPN stack
    //
    try
    {
        RPNStack_ = new DNBExpressionRPNStack<T>( topNode_ );
    }
    catch( DNBEDoesNotExist& error )
    {
        // This should be only caused by internal problems with the parser
        cout << "DNBEDoesNotExist error caught while trying to create RPN stack" << endl;
        cout << "Error Reads: " << error.what() << endl;

        // Pass the exception on up
        throw error;
    }
}

template<class T>
DNBExpressionSolver<T>::~DNBExpressionSolver()
    DNB_THROW_SPEC_NULL
{
}

template<class T>
scl_wstring 
DNBExpressionSolver<T>::getString()
    DNB_THROW_SPEC_NULL
{
    return( expressionString_ );
}

template<class T>
void
DNBExpressionSolver<T>::setString( const scl_wstring& expressionString,
                                   DNBSymbolTable& symbolTable,
                                   const wchar_t pathDelimiter,
                                   const wchar_t attrDelimiter,
                                   const wchar_t symbolDelimiter )
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEInvalidFormat, DNBEDoesNotExist ))
{
    //
    // Delete the old data
    //
    topNode_ = NULL;
    RPNStack_ = NULL;

    //
    // Store new data
    //
    expressionString_ = expressionString;
    pathDelimiter_ = pathDelimiter;
    attrDelimiter_ = attrDelimiter;
    symbolDelimiter_ = symbolDelimiter;

    //
    // Create the expression tree through pre-parsing and parsing
    //
    try
    {
        CallParser( expressionString_,
                    topNode_,
                    symbolTable,
                    pathDelimiter_,
                    attrDelimiter_,
                    symbolDelimiter_ );
    }
    catch( DNBEInvalidFormat& error )
    {
        // This can be caused by both user syntax errors and problems with the parser
        cout << "Invalid Format exception caught by DNBExpressionSolver during parsing.  Exiting parser." << endl;
        cout << "Error Reads: " << error.what() << endl;

        // Pass the exception on up
        throw error;
    }

    //
    // Create and store a RPN stack
    //
    try
    {
        RPNStack_ = new DNBExpressionRPNStack<T>( topNode_ );
    }
    catch( DNBEDoesNotExist& error )
    {
        // This should be only caused by internal problems with the parser
        cout << "DNBEDoesNotExist error caught while trying to create RPN stack" << endl;
        cout << "Error Reads: " << error.what() << endl;

        // Pass the exception on up
        throw error;
    }
}

template<class T>
T
DNBExpressionSolver<T>::getValue()
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEDoesNotExist, DNBEZeroDivide, DNBEOverflowError, DNBEUnderflowError ))
{
    T value;

    if ( !RPNStack_ )
    {
        // When running ODTs to test the syntax error catching code, this needs to be
        // turned off so this exception does not stop the ODT from running.

        DNBEDoesNotExist eNotAvailable(
            DNB_FORMAT( "There is no valid RPN stack that can be evaluated for this expression" ) );
        throw eNotAvailable;
    }

    try
    {
        value = RPNStack_->getValue();
    }
    catch ( DNBEDoesNotExist& error )
    {
        cout << "DNBEDoesNotExist error caught in expression solver getValue" << endl;
        cout << "Error Reads: " << error.what() << endl;
        throw error;
    }
    catch ( DNBEZeroDivide& error )
    {
        cout << "DNBEZeroDivide error caught in expression solver getValue" << endl;
        cout << "Error Reads: " << error.what() << endl;
        throw error;
    }
    catch ( DNBEUnderflowError& error )
    {
        cout << "DNBUnderflowError error caught in expression solver getValue" << endl;
        cout << "Error Reads: " << error.what() << endl;
        throw error;
    }
    catch ( DNBEOverflowError& error )
    {
        cout << "DNBEOverflowError error caught in expression solver getValue" << endl;
        cout << "Error Reads: " << error.what() << endl;
        throw error;
    }
    catch ( ... )
    {
        cout << "Unexpected Exception Caught Inside DNBExpressionSolver.cc: " << endl;
    }
    return( value );
}

template<class T>
void
DNBExpressionSolver<T>::turnToStack( RPNStackType& RPNstack )
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEDoesNotExist ))
{
    //
    // Create a new pointer to this node and push it onto the RPN stack
    //
    pointer newPtr;
    newPtr = this;
    RPNstack.push ( newPtr );
}

template<class T>
DNBExpressionSolver<T>::DNBExpressionSolver()
    DNB_THROW_SPEC_NULL
{
}

template<class T>
DNBExpressionSolver<T>::DNBExpressionSolver( const DNBExpressionSolver<T> &right )
    DNB_THROW_SPEC_NULL
{
    expressionString_ = right.expressionString_;
    pathDelimiter_ = right.pathDelimiter_;
    attrDelimiter_ = right.attrDelimiter_;
    symbolDelimiter_ = right.symbolDelimiter_;

    topNode_ = right.topNode_;
    RPNStack_ = right.RPNStack_;
}

