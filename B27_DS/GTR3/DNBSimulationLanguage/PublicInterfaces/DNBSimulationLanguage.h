//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//
//    This Header file is included by all the Module.h header files in 
//    PublicInterfaces
//

#ifndef DNBSIMULATIONLANGUAGE_H

#define DNBSIMULATIONLANGUAGE_H DNBSimulationLanguage

#ifdef _WINDOWS_SOURCE
#if defined(__DNBSimulationLanguage)
#define ExportedByDNBSimulationLanguage __declspec(dllexport)
#else
#define ExportedByDNBSimulationLanguage __declspec(dllimport)
#endif
#else
#define ExportedByDNBSimulationLanguage
#endif

#endif /* DNBSIMULATIONLANGUAGE_H */
