//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//*
//* FILE:
//*     DNBExpressionSymbol.h    - public header file
//*
//* MODULE:
//*     DNBExpressionMD
//*
//* OVERVIEW:
//*     This is the class for Symbol operands that inherits from DNBExpressionOperand
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     BPL         02/28/2000  Initial Implementation
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1999, 2000 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_DNBEXPRESSIONSYMBOL_H_
#define _DNB_DNBEXPRESSIONSYMBOL_H_


#include <DNBSystemBase.h>
#include <scl_list.h>
#include <scl_stack.h>
#include <scl_vector.h>
#include <DNBSystemDefs.h>

#include <DNBSimulationLanguage.h>
#include <DNBExpressionOperand.h>
#include <DNBListStack.h>

#include <DNBSymbol.h>
#include <DNBSymbolTable.h>

#include <DNBException.h>

//*
//* CLASS:
//*     DNBExpressionSymbol
//*
//* SUMMARY:
//*     Symbol node for DNBExpression class
//*
//* TEMPLATING:
//*    \param{ T }
//*        <T> is the numerical type that calculations are being
//*     done in.
//*
//* DESCRIPTION:
//*     This class provides for the storage of Symbols of type
//* <T>.  Values are loaded on construction.  Values may be retrieved
//* using the getValue() function.
//*
//* USE:
//*     DNBExpression<DNBReal>::pointer ptrc1 =
//*         DNB_NEW DNBExpressionSymbol<double>( name, symbol );
//*     DNBReal result = ptrc1.getValue();
//*
template<class T>
class DNBExpressionSymbol
    : public DNBExpressionOperand<T>
{

	DNB_DECLARE_EXPRESSION(DNBExpressionSymbol);
public:
    //
    // This section used by the dynamic_cast()
    //
    typedef
    DNBExpressionSymbol<T>*
    symbolPointer;

    //*
    //* FUNCTION:
    //*    DNBExpressionSymbol
    //*
    //* SUMMARY:
    //*    Initialization constructor
    //*
    //* PARAMETERS:
    //*    A \param[in]{scl_wstring}
    //*        The name (and possibly the path) of the symbol.
    //*    A \param[in]{DNBSymbolTable}
    //*         The link to the world model where the value of the symbol is stored
    //*    A \param[in]{wchar_t}
    //*        The character that delimits the names of entites that make up the path for a symbol.  Defaults to '/'.
    //*    A \param[in]{wchar_t}
    //*        The character that delimits the attributes of symbols.  Defaults to ':'.
    //*
    //* RETURNS:
    //*    Nothing
    //*
    //* DESCRIPTION:
    //*    The initialization constructor takes as an argument the value to be
    //* stored in the node.
    //*
    //* EXCEPTIONS:
    //*    None
    //*
    DNBExpressionSymbol( const scl_wstring& symbolName,
                         DNBSymbolTable& symbolTable,
                         const wchar_t  pathDelimiter = L'/',
                         const wchar_t  attrDelimiter = L':')
    DNB_THROW_SPEC(( DNBEInvalidFormat ));

    //*
    //* FUNCTION:
    //*    ~DNBExpressionSymbol
    //*
    //* SUMMARY:
    //*    Destructor
    //*
    //* PARAMETERS:
    //*     None
    //*
    //* RETURNS:
    //*    Nothing
    //*
    //* DESCRIPTION:
    //*    Destructor for DNBExpressionSymbol
    //*
    //* EXCEPTIONS:
    //*    None
    //*
    virtual
    ~DNBExpressionSymbol()
    DNB_THROW_SPEC_NULL;

    //*
    //* FUNCTION:
    //*    getValue
    //*
    //* SUMMARY:
    //*    Evaluates a node
    //*
    //* PARAMETERS:
    //*    None
    //*
    //* RETURNS:
    //*    Value of type <T>
    //*
    //* DESCRIPTION:
    //*    Causes the node to evaluate itself and return the resulting
    //* value.  For DNBExpressionSymbol the value of the symbol is retrived
    //* from the world model and return
    //*
    //* EXCEPTIONS:
    //*    \param{scl_bad_alloc}
    //*         The getValue() implentation for DNBExpressionRPNStack
    //* calls EvalutateStack(), which creates new DNBExpressionConstant
    //* nodes, which may throw scl_bad_alloc if memory is not available.
    //*    \param{DNBEDoesNotExist}
    //*         Thrown if any of the leaf pointer is NULL.
    //*    \param{DNBEZeroDivide}
    //*         An attempt was made to divide by zero
    //*    \param{DNBEOverflowError}
    //*         Calculation resulted in value that was too large
    //*    \param{DNBEUnderflowError}
    //*         Calculation resulted in value that was too small
    //*
    virtual
    T
    getValue()
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEDoesNotExist, DNBEZeroDivide, DNBEOverflowError, DNBEUnderflowError ));

    //*
    //* FUNCTION:
    //*    turnToStack
    //*
    //* SUMMARY:
    //*    Creates a RPN stack from an expression tree.
    //*
    //* PARAMETERS:
    //*     A \param[out]{ RPNStackType& }
    //*         The RPN stack that is this filled.
    //*
    //* RETURNS:
    //*    None
    //*
    //* DESCRIPTION:
    //*    Recursively traces a tree depth first to create a RPN stack
    //* representation of the expression.  Each node pushes a pointer
    //* to itself onto the RPN stack.
    //*
    //* EXCEPTIONS:
    //*    \param{scl_bad_alloc}
    //*        This function pushes pointers onto a STL list that will
    //* throw a scl_bad_alloc if there is not enough memory.
    //*
    virtual
    void
    turnToStack( RPNStackType& RPNstack )
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEDoesNotExist ));

    void
    setSymbolValue( T value )
    DNB_THROW_SPEC_NULL;

private:
    //
    // Default Constructor, made private so no one can use it
    //
    DNBExpressionSymbol()
    DNB_THROW_SPEC_NULL;

    //
    // Copy Constructor, made private so no one can use it, empty implantation
    //
    DNBExpressionSymbol( const DNBExpressionSymbol<T> &right )
    DNB_THROW_SPEC_NULL;

    //
    // Symbol table which gives us access to symbols in the World Data Model
    //
    scl_wstring symbolName_;

    //
    // Symbol table which gives us access to symbols in the World Data Model
    //
    DNBSymbol<T> symbol_;

    wchar_t pathDelimiter_;

};

//
//  Include the public definition file.
//
#include "DNBExpressionSymbol.cc"

#endif  /* _DNB_DNBEXPRESSIONSYMBOL_H_ */
