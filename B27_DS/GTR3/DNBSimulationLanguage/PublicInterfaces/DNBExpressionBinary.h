//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//*
//* FILE:
//*     DNBExpressionBinary.h    - public header file
//*
//* MODULE:
//*     DNBExpressionMD     
//*
//* OVERVIEW:
//*     This is the class for binary operators that inherits from DNBExpressionOperator
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     BPL         11/10/1999  Initial Implementation
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1999, 2000 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_DNBEXPRESSIONBINARY_H_
#define _DNB_DNBEXPRESSIONBINARY_H_

#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>

#include <DNBSimulationLanguage.h>
#include <DNBExpressionOperator.h>
#include <DNBListStack.h>

#include <DNBException.h>

//*
//* CLASS:
//*     DNBExpressionBinary
//*
//* SUMMARY: 
//*     Binary operator node for DNBExpression class
//*
//* TEMPLATING:
//*    \param{ T }
//*        <T> is the numerical type that calculations are being 
//*     done in.  All math operations and functions supported by 
//*     the calculator must be defined for type <T>.
//*
//* DESCRIPTION:
//*     This class is one link in the inheritance tree for the 
//* DNBExpression class.  All binary operators inherit from it.
//*
template<class T>
class DNBExpressionBinary 
    : public DNBExpressionOperator<T> 
{
	DNB_DECLARE_EXPRESSION(DNBExpressionBinary);
public:
    //*
    //* FUNCTION:
    //*    DNBExpressionBinary
    //*
    //* SUMMARY:
    //*    Initialization constructor
    //*
    //* PARAMETERS:
    //*     A \ref{pointer} 
    //*         Left operand leaf, the operand on the left side of the operator.
    //*     A \ref{pointer} 
    //*         Right operand leaf, the operand on the right side of the operator.
    //*
    //* RETURNS:
    //*    Nothing
    //*
    //* DESCRIPTION:
    //*    This constructor is called by the constructor for 
    //* derived classes  and takes as input the operands that are on the 
    //* left and right side of the operator. 
    //*
    //* EXCEPTIONS:
    //*    None
    //*
    DNBExpressionBinary(pointer operand1, pointer operand2)
    DNB_THROW_SPEC_NULL;

    //*
    //* FUNCTION:
    //*    ~DNBExpressionBinary
    //*
    //* SUMMARY:
    //*    Destructor
    //*
    //* PARAMETERS:
    //*     None 
    //*
    //* RETURNS:
    //*    Nothing
    //*
    //* TEMPLATING:
    //*    \param{ T }
    //*        <T> is the type the math is being done in.
    //*
    //* DESCRIPTION:
    //*    Destructor for DNBExpressionBinary
    //*
    //* EXCEPTIONS:
    //*    None
    //*
    virtual
    ~DNBExpressionBinary()
    DNB_THROW_SPEC_NULL;

    //*
    //* FUNCTION:
    //*    turnToStack
    //*
    //* SUMMARY:
    //*    Creates a RPN stack from an expression tree.
    //*
    //* PARAMETERS: 
    //*     A \param[out]{ RPNStackType& } 
    //*         The RPN stack that is this filled.
    //*
    //* RETURNS:
    //*    None
    //*
    //* DESCRIPTION: 
    //*    Recursively traces a tree depth first to create a RPN stack
    //* representation of the expression.  Each node pushes a pointer 
    //* to itself onto the RPN stack.
    //*
    //* EXCEPTIONS:
    //*    \param{scl_bad_alloc}
    //*        This function pushes pointers onto a STL list that will 
    //* throw a scl_bad_alloc if there is not enough memory.
    //*    \param{DNBEDoesNotExist}
    //*         Thrown if any of the leaf pointers are NULL.
    //*
    virtual
    void
    turnToStack( RPNStackType& RPNstack )
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEDoesNotExist ));

protected:
    // 
    // Left hand leaf.  This is the operand on the left side of the operator.
    // 
    pointer operand1_;

    // 
    // Right hand leaf.  This is the operand on the right side of the operator.
    // 
    pointer operand2_;

    // 
    // Default Constructor 
    // 
    DNBExpressionBinary()
    DNB_THROW_SPEC_NULL;

private:
    // 
    // Copy Constructor, made private so no one can use it
    // 
    DNBExpressionBinary( const DNBExpressionBinary<T> &right )
    DNB_THROW_SPEC_NULL;
};

//
//  Include the public definition file.
//
#include "DNBExpressionBinary.cc"


#endif  /* _DNB_DNBEXPRESSIONBINARY_H_ */
