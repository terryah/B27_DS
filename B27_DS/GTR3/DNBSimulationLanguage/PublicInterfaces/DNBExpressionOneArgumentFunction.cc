//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview czo/BPL 01:11:13
 */


//*
//* FILE:
//*     DNBExpressionOneArgumentFunction.cc  - public definition file
//*
//* MODULE:
//*     DNBExpressionMD
//*
//* OVERVIEW:
//*     This inherited class of DNBExpression stores pointers to functions 
//*         and acts as a wrapper class that DNBExpressionFunction can 
//*         creation functors to call.  The wrapper function evaluates all 
//*         arguments and calls the function.  This particular class is 
//*         for functions that take one argument.
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     BPL         03/16/2000  Initial Implementation
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1999, 2000 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*


template<class T>
DNBExpressionOneArgumentFunction<T>::DNBExpressionOneArgumentFunction( T (*function)(T) ) 
    DNB_THROW_SPEC_NULL
    : function_( function)
{
}

template<class T>
DNBExpressionOneArgumentFunction<T>::DNBExpressionOneArgumentFunction() 
    DNB_THROW_SPEC_NULL
{
}

template<class T>
DNBExpressionOneArgumentFunction<T>::DNBExpressionOneArgumentFunction( DNBExpressionOneArgumentFunction<T> &right )
    DNB_THROW_SPEC_NULL
{
}

template<class T>
T 
DNBExpressionOneArgumentFunction<T>::callFunction( pointer arg1 )
    DNB_THROW_SPEC_NULL
{
    return( (*function_)( arg1->getValue() ) );
}

