//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//*
//* FILE:
//*     DNBExpressionWrapperFunctions.h    - protected header file
//*
//* MODULE:
//*     DNBExpressionMD
//*
//* OVERVIEW:
//*     This file provides wrapper functions for functions supported by the expression solver.
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     BPL         03/15/2000  Initial Implementation
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 2000 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_EXPRESSIONWRAPPERFUNCTIONS_H_
#define _DNB_EXPRESSIONWRAPPERFUNCTIONS_H_


#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>

#include <DNBSimulationLanguage.h>    // Needed for ExportedBy*

#include <DNBTriangle.h>        // ssa functions
#include <DNBQuadrilateral.h>   // sasssa functions
#include <DNBSIUnit.h>
#include <DNBIGCALCFunctions.h> // ramp, range, inrange, int

//
//     These are wrapper functions used by the DNBExpression parser.  Some functions 
// are templated for type that create problems when setting function pointer equal 
// to them.  Using a wrapper function fixes the problem.
//

// 1 arg
DNBReal exp_abs( DNBReal a );
DNBReal exp_acosD( DNBReal a );
DNBReal exp_acosR( DNBReal a );
DNBReal exp_asinD( DNBReal a );
DNBReal exp_asinR( DNBReal a );
DNBReal exp_atanD( DNBReal a );
DNBReal exp_atanR( DNBReal a );
DNBReal exp_cosD( DNBReal a );
DNBReal exp_cosR( DNBReal a );
DNBReal exp_exp( DNBReal a );
DNBReal exp_log( DNBReal a );
DNBReal exp_log10( DNBReal a );
DNBReal exp_sinD( DNBReal a );
DNBReal exp_sinR( DNBReal a );
DNBReal exp_sqrt( DNBReal a );
DNBReal exp_tanD( DNBReal a );
DNBReal exp_tanR( DNBReal a );
DNBReal exp_floor( DNBReal a );
DNBReal exp_ceil( DNBReal a );
DNBReal exp_DNBInt( DNBReal a );


// 2 arg
DNBReal exp_atanD2( DNBReal a, DNBReal b );
DNBReal exp_atanR2( DNBReal a, DNBReal b );
DNBReal exp_max( DNBReal a, DNBReal b );
DNBReal exp_min( DNBReal a, DNBReal b );
DNBReal exp_mod( DNBReal a, DNBReal b );
DNBReal exp_pow( DNBReal a, DNBReal b );

// 3 arg
DNBReal exp_DNBTriangleSASS( DNBReal , DNBReal , DNBReal  );
DNBReal exp_DNBTriangleSASA( DNBReal , DNBReal , DNBReal  );
DNBReal exp_DNBTriangleSSSA( DNBReal , DNBReal , DNBReal  );
DNBReal exp_DNBRange( DNBReal , DNBReal , DNBReal  );
DNBReal exp_DNBInrange( DNBReal , DNBReal , DNBReal  );

// 5 arg
DNBReal exp_DNBQuadrilateralSASSSA1( DNBReal , DNBReal , DNBReal , DNBReal , DNBReal  );
DNBReal exp_DNBQuadrilateralSASSSA2( DNBReal , DNBReal , DNBReal , DNBReal , DNBReal  );
DNBReal exp_DNBQuadrilateralSASASA( DNBReal , DNBReal , DNBReal , DNBReal , DNBReal  );
DNBReal exp_DNBQuadrilateralSASASS( DNBReal , DNBReal , DNBReal , DNBReal , DNBReal  );
DNBReal exp_DNBRamp( DNBReal , DNBReal , DNBReal , DNBReal , DNBReal  );


#endif  /* _DNB_EXPRESSIONWRAPPERFUNCTIONS_H_ */
