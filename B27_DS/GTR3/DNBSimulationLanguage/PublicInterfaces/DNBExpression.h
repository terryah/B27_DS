//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//*
//* FILE:
//*     DNBExpression.h    - public header file
//*
//* MODULE:
//*     DNBExpressionMD
//*
//* OVERVIEW:
//*     This is the base class for DNBExpression, the expression solver
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     BPL         11/10/1999  Initial Implementation
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1999, 2000 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _DNBDNBEXPRESSION_H_
#define _DNBDNBEXPRESSION_H_

#include <DNBSystemBase.h>
#include <scl_deque.h>
#include <DNBSystemDefs.h>

#include <DNBSimulationLanguage.h>
#include <DNBListStack.h>
#include <DNBCountedObject.h>
#include <DNBCountedPointer.h>


#define DNB_DECLARE_EXPRESSION(_CLASS_)                                     \
public:                                                                     \
    typedef DNBCountedPointer<DNBExpression<T> >    pointer;                \
    typedef DNBListStack<pointer>                   RPNStackType;           \
    typedef scl_deque <pointer, DNB_ALLOCATOR(pointer) > PostFixStackType


//*
//* CLASS:
//*     DNBExpression
//*
//* SUMMARY:
//*     Tree and RPN stack based representation of an expression string.
//*
//* TEMPLATING:
//*    \param{ T }
//*        <T> is the numerical type that calculations are being
//*     done in.  All math operations and functions supported by
//*     the calculator must be defined for type <T>.
//*
//* DESCRIPTION:
//*     This class is the base for the DNBExpression class
//* inheritance hierarchy.  The class provides for parsing, compiling, and
//* execution of expression strings.  It contains data structures
//* for storing expressions in the form of trees or RPN stacks.
//* The RPN stack format is optimized for faster execution than the
//* tree structure.
//*     Each object in the expression is stored as a
//* DNBExpression node on the heap with the tree connected using
//* DNB smart pointers.  The user stores only a pointer to the top node of
//* the tree.  The RPN stack is a stack of pointers to the
//* DNBExpression nodes that make up the trees.  The getValue()
//* function may be used to evaluate an expression stored in either a tree
//* or an RPN stack.
//*
//* EXAMPLE: External User
//*     scl_wstring s1( L"2+3*asin(.5)" );
//*     DNBExpressionSolver<DNBReal>::solverPointer ptr =
//*            new DNBExpressionSolver<DNBReal>( s1, symbolTable );
//*     DNBReal result = ptr->getValue();
//*
//* EXAMPLE: Internal User
//*     DNBExpression<DNBReal>::pointer ptrc1 =
//*            DNB_NEW DNBExpressionConstant<DNBReal>(5);
//*     DNBExpression<DNBReal>::pointer ptrc2 =
//*            DNB_NEW DNBExpressionConstant<DNBReal>(7);
//*     DNBExpression<DNBReal>::pointer ptrp1 =
//*            DNB_NEW DNBExpressionPlus<DNBReal>(ptrc1, ptrc2);
//*     DNBExpression<DNBReal>::pointer ptrs1 =
//*            DNB_NEW DNBExpressionRPNStack<DNBReal>(ptrp1);
//*     DNBReal result = ptrs1->getValue();
//*
template<class T>
class DNBExpression : public DNBCountedObject
{
    DNB_DECLARE_EXPRESSION(DNBExpression);

public:
    //*
    //* TYPEDEF:
    //*     self
    //*
    //* SUMMARY:
    //*     A shorthand notation for defining instances of this class inside
    //* itself.
    //*
    //* DESCRIPTION:
    //*     This type definition specifies a shorthand notation for defining
    //* instances of the DNBExpression class inside itself.
    //*
    //*
    typedef DNBExpression<T> self;

#if 0
    //*
    //* TYPEDEF:
    //*     pointer
    //*
    //* SUMMARY:
    //*     The smart pointer type used to link the tree together.
    //*
    //* DESCRIPTION:
    //*     This type definition specifies the smart pointer type that
    //* is used in the DNBExpression class to link together the
    //* expression tree and RPN stack.
    //*
    typedef DNBCountedPointer<self> pointer;

    //*
    //* TYPEDEF:
    //*     RPNStackType
    //*
    //* SUMMARY:
    //*     The stack used to store the RPN stack.
    //*
    //* DESCRIPTION:
    //*     This type definition specifies the special stack used to
    //* store the RPN stack that can be evaluated non-destructively.
    //*
    typedef DNBListStack<pointer>   RPNStackType;

    //*
    //* TYPEDEF:
    //*     PostFixStackType
    //*
    //* SUMMARY:
    //*     The STL stack used to store the PostFix stack.
    //*
    //* DESCRIPTION:
    //*     This type definition specifies the STL stack used to
    //* store the PostFix stack used during RPN stack evaluation.
    //*
    typedef scl_deque <pointer, DNB_ALLOCATOR(pointer) > PostFixStackType;
#endif

    //*
    //* FUNCTION:
    //*    DNBExpression
    //*
    //* SUMMARY:
    //*    Default constructor
    //*
    //* PARAMETERS:
    //*    None
    //*
    //* RETURNS:
    //*    Nothing
    //*
    //* DESCRIPTION:
    //*    The default constructor for DNBExpression
    //*
    //* EXCEPTIONS:
    //*    None
    //*
    DNBExpression( )
    DNB_THROW_SPEC_NULL;

    //*
    //* FUNCTION:
    //*    ~DNBExpression
    //*
    //* SUMMARY:
    //*    Destructor
    //*
    //* PARAMETERS:
    //*     None
    //*
    //* RETURNS:
    //*    Nothing
    //*
    //* DESCRIPTION:
    //*    Destructor for DNBExpression.
    //*
    //* EXCEPTIONS:
    //*    None
    //*
    virtual
    ~DNBExpression()
    DNB_THROW_SPEC_NULL;

    //*
    //* FUNCTION:
    //*    turnToStack
    //*
    //* SUMMARY:
    //*    Creates a RPN stack from an expression tree.
    //*
    //* PARAMETERS:
    //*     A \param[out]{ RPNStackType& }
    //*         The RPN stack that is this filled.
    //*
    //* RETURNS:
    //*    None
    //*
    //* DESCRIPTION:
    //*    Recursively traces a tree depth first to create a RPN stack
    //* representation of the expression.  Each node pushes a pointer
    //* to itself onto the RPN stack.
    //*
    //* EXCEPTIONS:
    //*    \param{scl_bad_alloc}
    //*        This function pushes pointers onto a STL list that will
    //* throw a scl_bad_alloc if there is not enough memory.
    //*    \param{DNBEDoesNotExist}
    //*         Thrown if any of the leaf pointers are NULL.
    //*
    virtual
    void
    turnToStack( RPNStackType& RPNstack )
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEDoesNotExist )) = 0;

    //*
    //* FUNCTION:
    //*    getValue
    //*
    //* SUMMARY:
    //*    Evaluates a node.
    //*
    //* PARAMETERS:
    //*    None
    //*
    //* RETURNS:
    //*    A value of type <T> that is the value of the evaluated node.
    //*
    //* DESCRIPTION:
    //*    Causes the node to evaluate itself and return the resulting
    //* value.  Can be called on any node type that represents a object in
    //* an expression, including a RPN stack.
    //*
    //* EXCEPTIONS:
    //*    \param{scl_bad_alloc}
    //*         The getValue implentation for DNBExpressionRPNStack
    //* calls EvalutateStack(), which creates new DNBExpressionConstant
    //* nodes, which may throw scl_bad_alloc if memory is not available.
    //*    \param{DNBEDoesNotExist}
    //*         Thrown if any of the leaf pointers are NULL.
    //*    \param{DNBEZeroDivide}
    //*         Thrown if a divide node divides by zero.
    //*    \param{DNBEOverflowError}
    //*         Thrown for overflow.
    //*    \param{DNBEUnderflowError}
    //*         Thrown for underflow.
    //*
    virtual
    T
    getValue()
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEDoesNotExist, DNBEZeroDivide, DNBEOverflowError, DNBEUnderflowError )) = 0;

    //*
    //* FUNCTION:
    //*    evaluateType
    //*
    //* SUMMARY:
    //*    Causes the evaluation of a node based on its type (operator or operand)
    //*
    //* PARAMETERS:
    //*    \param{PostFixStackType}
    //*         The post fix stack that stores the operands already poped of
    //* the RPN stack.
    //*
    //* RETURNS:
    //*    None
    //*
    //* DESCRIPTION:
    //*    This function causes the evaluation of a node based on whether
    //* it is an operator or operand
    //*
    //* EXCEPTIONS:
    //*    \param{scl_bad_alloc}
    //*         The evaluateType implentation for DNBExpressionOperator
    //* creates new DNBExpressionConstand nodes, which may cause scl_bad_alloc to
    //* be thrown if memory is not available.
    //*    \param{DNBEDoesNotExist}
    //*         Thrown if any of the leaf pointers are NULL.
    //*    \param{DNBEZeroDivide}
    //*         Thrown if a divide node divides by zero.
    //*    \param{DNBEOverflowError}
    //*         Thrown for overflow.
    //*    \param{DNBEUnderflowError}
    //*         Thrown for underflow.
    //*
    virtual
    void
    evaluateType( PostFixStackType& PostFix )
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEDoesNotExist, DNBEZeroDivide, DNBEOverflowError, DNBEUnderflowError )) = 0;

private:
    //*
    //* FUNCTION:
    //*    DNBExpression
    //*
    //* SUMMARY:
    //*    Copy constructor
    //*
    //* PARAMETERS:
    //*     A \ref{DNBExpression}
    //*
    //* RETURNS:
    //*    Nothing
    //*
    //* DESCRIPTION:
    //*    The copy constructor is declared private so that it can
    //* not be used. It is an empty implantation.
    //*
    //* EXCEPTIONS:
    //*    None
    //*
    DNBExpression( const DNBExpression<T> &right )
    DNB_THROW_SPEC_NULL;

};


//
//  Include the public definition file.
//
#include <DNBExpression.cc>


#endif  /* _DNBDNBEXPRESSION_H_ */
