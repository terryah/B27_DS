//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//*
//* FILE:
//*     DNBExpressionTwoArgumentFunction.h    - public header file
//*
//* MODULE:
//*     DNBExpressionMD
//*
//* OVERVIEW:
//*     This inherited class of DNBExpression stores pointers to functions 
//*         and acts as a wrapper class that DNBExpressionFunction can 
//*         creation functors to call.  The wrapper function evaluates all 
//*         arguments and calls the function.  This particular class is 
//*         for functions that take two arguments.
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     BPL         11/10/1999  Initial Implementation
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1999, 2000 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _DNB_DNBEXPRESSIONTWOARGUMENTFUNCTION_H_
#define _DNB_DNBEXPRESSIONTWOARGUMENTFUNCTION_H_

#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>

#include <DNBException.h>
#include <DNBListStack.h>
#include <DNBExpressionFunction.h>


//*
//* CLASS:
//*     DNBExpressionTwoArgumentFunction
//*
//* SUMMARY:
//*     Node in DNBExpression inheritance that is used as a wrapper 
//* function for representing functions that take two arguments of type 
//* <T>
//*
//* TEMPLATING:
//*    \param{ T }
//*        <T> is the numerical type that calculations are being 
//*     done in.  All math operations and functions supported by 
//*     the calculator must be defined for type <T>.
//*
//* DESCRIPTION: 
//*     This class is needed to that the arguments to functions stored 
//* in the DNBExpressionFunction nodes can be evaluated before the 
//* functions are called.  The class contains the function 
//* callFunction() that takes as arguments two pointers to DNBExpression 
//* nodes.  When it is called, the argument expressions are evaluated 
//* and the function is called.  The DNBExpressionFunction class creates 
//* functors that call this function.
//*
template<class T>
class DNBExpressionTwoArgumentFunction 
    : public DNBExpressionFunction <T> 
{
	DNB_DECLARE_EXPRESSION(DNBExpressionTwoArgumentFunction);
public:
    //*
    //* FUNCTION:
    //*    DNBExpressionTwoArgumentFunction
    //*
    //* SUMMARY:
    //*    Initialization constructor
    //*
    //* PARAMETERS:
    //*     A \ref{(*function)(T, T)} 
    //*         Function pointer
    //*
    //* RETURNS:
    //*    Nothing
    //*
    //* DESCRIPTION:
    //*    The constructor takes an as argument a pointer to function that 
    //* wrapper class is to call.
    //*
    //* EXCEPTIONS:
    //*    None
    //*
    DNBExpressionTwoArgumentFunction( T (*function)(T, T) )
    DNB_THROW_SPEC_NULL;

    //*
    //* FUNCTION:
    //*     callFunction
    //*
    //* SUMMARY:
    //*     Wrapper call used by functors stored in DNBExpressionFunction
    //*
    //* PARAMETERS: 
    //*    \param[in]{pointer}
    //*        1st arugment for the function
    //*    \param[in]{pointer}
    //*        2st arugment for the function
    //*
    //* RETURNS:
    //*    Value of type <T> that is the result of the function.
    //*
    //* DESCRIPTION:
    //*    This function is the wrapper function that is used to 
    //* evaluate the arguments to a function node before that function 
    //* is called.  The functor created by DNBExpressionFunction 
    //* constructors point to this function.  When called, the 
    //* DNBExpression arguments are evaluated and the function is called 
    //* with those values.
    //*
    //* EXCEPTIONS:
    //*     None
    //*
    virtual 
    T 
    callFunction( pointer arg1, pointer arg2 ) 
    DNB_THROW_SPEC_NULL;

private:
    // 
    // Default constructor, 
    // 
    DNBExpressionTwoArgumentFunction()
    DNB_THROW_SPEC_NULL;

    // 
    // Copy constructor, 
    // 
    DNBExpressionTwoArgumentFunction( DNBExpressionTwoArgumentFunction<T> &right )
    DNB_THROW_SPEC_NULL;

    // 
    // Pointer to function
    // 
    T (*function_)(T, T);
};

//
//  Include the public definition file.
//
#include "DNBExpressionTwoArgumentFunction.cc"

#endif  /* _DNB_DNBEXPRESSIONTWOARGUMENTFUNCTION_H_ */
