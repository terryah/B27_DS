//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//*
//* FILE:
//*     DNBExpressionUnaryMinus.h    - public header file
//*
//* MODULE:
//*     DNBExpressionMD
//*
//* OVERVIEW:
//*     This is the class for the unary minus operator that inherits from DNBExpressionBinary
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     BPL         11/29/1999  Initial Implementation
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1999, 2000 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _DNB_DNBEXPRESSIONUNARYMINUS_H_
#define _DNB_DNBEXPRESSIONUNARYMINUS_H_

#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>

#include <DNBException.h>
#include <DNBListStack.h>
#include <DNBExpressionUnary.h>

//*
//* CLASS:
//*     DNBExpressionUnaryMinus
//*
//* SUMMARY:
//*     Node in DNBExpression inheritance for representing the unary
//* minus operation.
//*
//* TEMPLATING:
//*    \param{ T }
//*        <T> is the numerical type that calculations are being
//*     done in.  All math operations and functions supported by
//*     the calculator must be defined for type <T>.
//*
//* DESCRIPTION:
//*     This class provides for the representation of the addition
//* operation in expressions.  The value of the operation on its
//* operands may be found using the getValue() function.
//*
//* EXAMPLE:
//*     The pointers to two expression trees are passed in as arguments
//* to the constructor.
//*     DNBExpression<DNBReal>::pointer ptrc1 =
//*         DNB_NEW DNBExpressionConstant<DNBReal>(5);
//*     DNBExpression<DNBReal>::pointer ptrum1 =
//*         DNB_NEW DNBExpressionUnaryMinus<DNBReal>(ptrc1);
//*
template<class T>
class DNBExpressionUnaryMinus
    : public DNBExpressionUnary<T>
{
	DNB_DECLARE_EXPRESSION(DNBExpressionUnaryMinus);
public:
    //*
    //* FUNCTION:
    //*    DNBExpressionUnaryMinus
    //*
    //* SUMMARY:
    //*    Initialization constructor
    //*
    //* PARAMETERS:
    //*     A \ref{pointer}
    //*         Operand leaf
    //*
    //* RETURNS:
    //*    Nothing
    //*
    //* DESCRIPTION:
    //*    The constructor for DNBExpressionUnaryMinus takes as an argument the
    //* operand on the right side of the operator.  The
    //* constructor for DNBExpressionUnary is then call and the operand
    //* passed to it.
    //*
    //* EXCEPTIONS:
    //*    None
    //*
    DNBExpressionUnaryMinus( pointer operand1 )
    DNB_THROW_SPEC_NULL;

    //*
    //* FUNCTION:
    //*    getValue
    //*
    //* SUMMARY:
    //*    Evaluates a node
    //*
    //* PARAMETERS:
    //*    None
    //*
    //* RETURNS:
    //*    Value of type <T>
    //*
    //* DESCRIPTION:
    //*    Causes the node to evaluate itself and return the resulting
    //* value.  For DNBExpressionUnaryMinus, getValue() is called on the
    //* operand leaf, and the result of the negitive of its value.
    //*
    //* EXCEPTIONS:
    //*    \param{scl_bad_alloc}
    //*         The getValue implentation for DNBExpressionRPNStack
    //* calls EvalutateStack(), which creates new DNBExpressionConstant
    //* nodes, which may throw scl_bad_alloc if memory is not available.
    //*    \param{DNBEDoesNotExist}
    //*         Thrown if any of the leaf pointers are NULL.
    //*    \param{DNBEZeroDivide}
    //*         Thrown if a divide node divides by zero.
    //*    \param{DNBEOverflowError}
    //*         Thrown for overflow.
    //*    \param{DNBEUnderflowError}
    //*         Thrown for underflow.
    //*
    virtual
    T
    getValue()
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEDoesNotExist, DNBEZeroDivide, DNBEOverflowError, DNBEUnderflowError ));

    //*
    //* FUNCTION:
    //*    performOperation
    //*
    //* SUMMARY:
    //*    Evaluates an object on the RPN stack
    //*
    //* PARAMETERS:
    //*     A \ref{PostFixStackType}
    //*         The postfix stack that contains the operators to be operated on.
    //*
    //* RETURNS:
    //*    Value of type <T>
    //*
    //* DESCRIPTION:
    //*    The function allows a operator node on the RPN stack to evaluate
    //* itself when it is poped off the stack.  The needed operators are popped
    //* off postfix stack and the result of the operation is returned.
    //*
    //* EXCEPTIONS:
    //*    \param{DNBEDoesNotExist}
    //*         Thrown if there is not enough operands on the post fix stack
    //*    \param{DNBEZeroDivide}
    //*         Thrown if a divide node divides by zero.
    //*    \param{DNBEOverflowError}
    //*         Thrown for overflow.
    //*    \param{DNBEUnderflowError}
    //*         Thrown for underflow.
    //*
    virtual
    T
    performOperation( PostFixStackType& PostFix )
    DNB_THROW_SPEC(( DNBEDoesNotExist, DNBEZeroDivide, DNBEOverflowError, DNBEUnderflowError ));

public:
    //
    // default constructor, private so that it can not be used.
    //
    DNBExpressionUnaryMinus()
    DNB_THROW_SPEC_NULL;

    //
    // copy constructor, private so that it can not be used, empty implemtation
    //
    DNBExpressionUnaryMinus( DNBExpressionUnaryMinus<T> &right )
    DNB_THROW_SPEC_NULL;
};


//
//  Include the public definition file.
//
#include "DNBExpressionUnaryMinus.cc"


#endif  /* _DNB_DNBEXPRESSIONUNARYMINUS_H_ */
