//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview czo/BPL 01:11:13
 */


//*
//* FILE:
//*     DNBExpressionUnaryMinus.cc  - public definition file
//*
//* MODULE:
//*     DNBExpressionMD
//*
//* OVERVIEW:
//*     This module provides templates for storing unary minus operators in the DNBExpression Class
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     BPL         11/29/1999  Initial Implementation
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1999, 2000 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*


template<class T>
DNBExpressionUnaryMinus<T>::DNBExpressionUnaryMinus( pointer operand1 )
    DNB_THROW_SPEC_NULL
    : DNBExpressionUnary<T>( operand1 )
{
}

template<class T>
DNBExpressionUnaryMinus<T>::DNBExpressionUnaryMinus()
    DNB_THROW_SPEC_NULL
{
}

template<class T>
DNBExpressionUnaryMinus<T>::DNBExpressionUnaryMinus( DNBExpressionUnaryMinus<T> &right )
    DNB_THROW_SPEC_NULL
{
}

template<class T>
T
DNBExpressionUnaryMinus<T>::getValue()
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEDoesNotExist, DNBEZeroDivide, DNBEOverflowError, DNBEUnderflowError ))
{
    if ( !this->operand1_ )
    {
        DNBEDoesNotExist  eNotAvailable(
            DNB_FORMAT( "The expression tree can not be evaluated because one of the leaf pointers is NULL" ) );
        throw eNotAvailable;
    }

//  The choice of the (-1) over the built in unary minus operator was arbitrary
    return( (-1)*(this->operand1_->getValue()) );
}

template<class T>
T
DNBExpressionUnaryMinus<T>::performOperation( PostFixStackType& PostFix )
    DNB_THROW_SPEC(( DNBEDoesNotExist, DNBEZeroDivide, DNBEOverflowError, DNBEUnderflowError ))
{
    //
    // Pops the needed values off the PostFix start and returns the result of the operation
    // Righthand side operator pops off first
    //
    T rhsValue;    // Right hand side value
    pointer rhsNode;    // Right hand side node

    if ( PostFix.size() < 1 )
    {
        DNBEDoesNotExist  eNotAvailable(
            DNB_FORMAT( "The expression's RPN stack can not be evaluated because of insufficicent operands on postfix stack" ) );
        throw eNotAvailable;
    }

    rhsNode = PostFix.front();
    PostFix.pop_front();
    rhsValue = rhsNode->getValue();

//  The choice of the (-1) over the built in unary minus operator was arbitrary
    return (-1) * rhsValue;
}
