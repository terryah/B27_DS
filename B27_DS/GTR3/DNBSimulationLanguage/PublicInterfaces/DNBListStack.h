//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//*
//* FILE:
//*     DNBListStack.h    - public header file
//*
//* MODULE:
//*     DNBExpressionMD
//*
//* OVERVIEW:
//*     This is the class for the special stack used by DNBExpression that
//* can be evaluated non-distructivly
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     bperles     11/10/1999  Initial Implementation
//*     rtl         06/23/2003  Provide method to return 
//*                             size of listStack_
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1999, 2000 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _DNB_LISTSTACK_H_
#define _DNB_LISTSTACK_H_

#include <DNBSystemBase.h>
#include <scl_list.h>
#include <scl_stack.h>
#include <scl_vector.h>
#include <scl_algorithm.h>      // for scl_reverse
#include <DNBSystemDefs.h>

#include <DNBSimulationLanguage.h>

#include <DNBException.h>
#include <DNBCountedObject.h>
#include <DNBCountedPointer.h>


//*
//* CLASS:
//*     DNBListStack
//*
//* SUMMARY:
//*     A stack that can be evaluated non-destructively
//*
//* TEMPLATING:
//*    \param{ T }
//*        <T> is the numerical type that calculations are being
//*     done in.  All math operations and functions supported by
//*     the calculator must be defined for type <T>.
//*
//* DESCRIPTION:
//*      A stack that can be evaluated non-destructively.  A list
//* is used for storage and a pointer is used to point to the current
//* 'top' element.  The pointer can be reset to the top of the list
//* with the resetStackPointer() command.  Note: elements can not be
//* safely added to the 'stack' if the pointer is not at the end of the list.
//*
//* EXAMPLE:
//*     DNBListStack<pointer> RPNStack_;
//*     pointer newPtr;	
//*     newPtr = this;
//*     RPNstack.push (newPtr);
//*     RPNStack_.flipStack();
//*     T rhsValue;
//*     rhsNode = PostFix.top();
//*
template<class T>
class DNBListStack : public DNBCountedObject
{
public:
    //
    // Default Constructor
    //
    DNBListStack()
    DNB_THROW_SPEC(( scl_bad_alloc ));

    //
    // Destructor
    //
    ~DNBListStack()
    DNB_THROW_SPEC_NULL;

    //
    // Resets the pointer so the stack can be re-evaluated
    //
    void
    resetStackPointer()
    DNB_THROW_SPEC_NULL;

    //
    // Checks for empty stack during non-destructive evaluation
    //
    bool
    isEmpty()
    DNB_THROW_SPEC_NULL;

    //
    // Flips the stack so that it can be evaluated after creation
    //
    void
    flipStack()
    DNB_THROW_SPEC_NULL;

    //
    // Return element and move pointer down
    //
    T
    pop()
    DNB_THROW_SPEC_NULL;

    //
    // Return element without moving pointer
    //
    T
    getCurrent()
    DNB_THROW_SPEC_NULL;

    //
    // Add object to list and move pointer up
    //
    void
    push( T object )
    DNB_THROW_SPEC(( scl_bad_alloc ));

    //
    // Return the size of listStack_
    //
    size_t
    length()
    DNB_THROW_SPEC_ANY;

private:
    //
    // Copy Constructor, empty implemtation
    //
    DNBListStack( const DNBListStack<T> &right )
    DNB_THROW_SPEC(( scl_bad_alloc ));

    typedef scl_list< T, DNB_ALLOCATOR(T) > listOfT;

    //
    // The STL list that stores everything
    //
    listOfT listStack_;

    //
    // During evaluation, current top of list
    //
    typename listOfT::iterator currentTop_;

    //
    // The top most data element ever entered
    //
    typename listOfT::iterator permanentTop_;
};

//
//  Include the public definition file.
//
#include "DNBListStack.cc"

#endif  /* _DNB_LISTSTACK_H_ */
