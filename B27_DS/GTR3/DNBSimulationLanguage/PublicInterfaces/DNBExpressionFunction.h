//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//*
//* FILE:
//*     DNBExpressionFunction.h    - public header file
//*
//* MODULE:
//*     DNBExpressionMD
//*
//* OVERVIEW:
//*     This inherited class of DNBExpression stores functions using a functor.
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     BPL         11/10/1999  Initial Implementation
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1999, 2000 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _DNB_DNBEXPRESSIONFUNCTION_H_
#define _DNB_DNBEXPRESSIONFUNCTION_H_

#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>

#include <DNBException.h>
#include <DNBCallback.h>    // DNBFunctors

#include <DNBListStack.h>
#include <DNBExpressionOperand.h>

template<class T>
class DNBExpressionOneArgumentFunction;
template<class T>
class DNBExpressionTwoArgumentFunction;
template<class T>
class DNBExpressionThreeArgumentFunction;
template<class T>
class DNBExpressionFourArgumentFunction;
template<class T>
class DNBExpressionFiveArgumentFunction;

//*
//* CLASS:
//*     DNBExpressionFunction
//*
//* SUMMARY:
//*     Node in DNBExpression inheritance for representing functions
//*
//* TEMPLATING:
//*    \param{ T }
//*        <T> is the numerical type that calculations are being
//*     done in.  All math operations and functions supported by
//*     the calculator must be defined for type <T>.
//*
//* DESCRIPTION:
//*     This class is for represent functions in expression trees and
//* RPN stacks.  Functions and their arguments are stored in a functor.
//* Arguments to the function can be in the form of trees or RPN stacks.
//* The fuctor created actual calls a wrapper class that evaluates the
//* arguments and then call the function.
//*
//* EXAMPLE:
//*     There is a DNBExpressionFunction constructor available for each
//* possible number of arguments that the supported functions can have.
//* Pointers to the function and each argument are passed into the
//* constructor.  When a function node is pushed onto a RPN stack, the
//* argument expression trees are not converted into RPN stacks.  If the
//* user wishes to avoid having the argument expression trees evaluated
//* in the slower tree form, it is recommended that all function
//* arguments be converted into RPN stacks and the stacks then used as
//* the arguments.
//*     DNBExpression<DNBReal>::pointer ptrc1 =
//*            DNB_NEW DNBExpressionConstant<DNBReal>(3.14);
//*     DNBExpression<DNBReal>::pointer ptrc2 =
//*            DNB_NEW DNBExpressionConstant<DNBReal>(.412);
//*     DNBExpression<DNBReal>::pointer ptrp1 =
//*            DNB_NEW DNBExpressionPlus<DNBReal>(ptrc1, ptrc2);
//*     DNBExpression<DNBReal>::pointer ptrs1 =
//*            DNB_NEW DNBExpressionRPNStack <DNBReal>(ptrp1);
//*     DNBExpression<DNBReal>::pointer ptrf1 =
//*            DNB_NEW DNBExpressionFunction<DNBReal>(sin, ptrs1);
//*     DNBExpression<DNBReal>::pointer ptrs2 =
//*            DNB_NEW DNBExpressionRPNStack<DNBReal>(ptrf1);
//*
template<class T>
class DNBExpressionFunction
    : public DNBExpressionOperand<T>
{
	DNB_DECLARE_EXPRESSION(DNBExpressionFunction);
public:
    //*
    //* FUNCTION:
    //*    DNBExpressionFunction
    //*
    //* SUMMARY:
    //*    Default constructor
    //*
    //* PARAMETERS:
    //*    None
    //*
    //* RETURNS:
    //*    Nothing
    //*
    //* DESCRIPTION:
    //*    The default constructor is used for functions that take no arguments.
    //*
    //* EXCEPTIONS:
    //*    None
    //*
    DNBExpressionFunction()
    DNB_THROW_SPEC_NULL;

    //*
    //* FUNCTION:
    //*    DNBExpressionFunction
    //*
    //* SUMMARY:
    //*    Initialization constructor for 1 argument functions
    //*
    //* PARAMETERS:
    //*    A \param[in]{<(*in_function )( T )>}
    //*        Function pointer to be called
    //*    A \param[in]{<pointer>}
    //*        1st argument for function
    //*
    //* RETURNS:
    //*    Nothing
    //*
    //* DESCRIPTION:
    //*    This constructor is for functions that take 1 argument.
    //*
    //* EXCEPTIONS:
    //*    \param{scl_bad_alloc}
    //*         If the functor runs out of memory
    //*
    DNBExpressionFunction( T (*in_function )( T ), pointer arg1 )
    DNB_THROW_SPEC(( scl_bad_alloc ));

    //*
    //* FUNCTION:
    //*    DNBExpressionFunction
    //*
    //* SUMMARY:
    //*    Initialization  constructor for 2 argument functions
    //*
    //* PARAMETERS:
    //*    A \param[in]{<(*in_function )( T, T )>}
    //*        Function pointer to be called
    //*    A \param[in]{<pointer>}
    //*        1st argument for function
    //*    A \param[in]{<pointer>}
    //*        2nd argument for function
    //*
    //* RETURNS:
    //*    Nothing
    //*
    //* DESCRIPTION:
    //*    This constructor is for functions that take 2 arguments.
    //*
    //* EXCEPTIONS:
    //*    \param{scl_bad_alloc}
    //*         If the functor runs out of memory
    //*
    DNBExpressionFunction( T (*in_function )( T, T ), pointer arg1, pointer arg2 )
    DNB_THROW_SPEC(( scl_bad_alloc ));

    //*
    //* FUNCTION:
    //*    DNBExpressionFunction
    //*
    //* SUMMARY:
    //*    Initialization  constructor for 3 argument functions
    //*
    //* PARAMETERS:
    //*    A \param[in]{<(*in_function )( T, T, T )>}
    //*        Function pointer to be called
    //*    A \param[in]{<pointer>}
    //*        1st argument for function
    //*    A \param[in]{<pointer>}
    //*        2nd argument for function
    //*    A \param[in]{<pointer>}
    //*        3rd argument for function
    //*
    //* RETURNS:
    //*    Nothing
    //*
    //* DESCRIPTION:
    //*    This constructor is for functions that take 4 arguments.
    //*
    //* EXCEPTIONS:
    //*    \param{scl_bad_alloc}
    //*         If the functor runs out of memory
    //*
    DNBExpressionFunction( T (*in_function )( T, T, T ), pointer arg1, pointer arg2, pointer arg3 )
    DNB_THROW_SPEC(( scl_bad_alloc ));

    //*
    //* FUNCTION:
    //*    DNBExpressionFunction
    //*
    //* SUMMARY:
    //*    Initialization  constructor for 5 argument functions
    //*
    //* PARAMETERS:
    //*    A \param[in]{<(*in_function )( T, T, T, T, T )>}
    //*        Function pointer to be called
    //*    A \param[in]{<pointer>}
    //*        1st argument for function
    //*    A \param[in]{<pointer>}
    //*        2nd argument for function
    //*    A \param[in]{<pointer>}
    //*        3rd argument for function
    //*    A \param[in]{<pointer>}
    //*        4th argument for function
    //*    A \param[in]{<pointer>}
    //*        5th argument for function
    //*
    //* RETURNS:
    //*    Nothing
    //*
    //* DESCRIPTION:
    //*    This constructor is for functions that take 5 arguments.
    //*
    //* EXCEPTIONS:
    //*    \param{scl_bad_alloc}
    //*         If the functor runs out of memory
    //*
    DNBExpressionFunction( T (*in_function )( T, T, T, T, T ), pointer arg1, pointer arg2, pointer arg3, pointer arg4, pointer arg5 )
    DNB_THROW_SPEC(( scl_bad_alloc ));

    //*
    //* FUNCTION:
    //*    getValue
    //*
    //* SUMMARY:
    //*    Evaluates a node
    //*
    //* PARAMETERS:
    //*    None
    //*
    //* RETURNS:
    //*    Value of type <T>
    //*
    //* DESCRIPTION:
    //*    Causes the node to evaluate itself and return the resulting
    //* value.  For DNBExpressionExp, getValue() is called on the two
    //* operand leaves, and the result of the multiplication of these two
    //* values is returned.
    //*
    //* EXCEPTIONS:
    //*    \param{scl_bad_alloc}
    //*         The getValue implentation for DNBExpressionRPNStack
    //* calls EvalutateStack(), which creates new DNBExpressionConstant
    //* nodes, which may throw scl_bad_alloc if memory is not available.
    //*    \param{DNBEDoesNotExist}
    //*         Thrown if any of the leaf pointers are NULL.
    //*    \param{DNBEZeroDivide}
    //*         Thrown if a divide node divides by zero.
    //*    \param{DNBEOverflowError}
    //*         Thrown for overflow.
    //*    \param{DNBEUnderflowError}
    //*         Thrown for underflow.
    //*
    virtual
    T
    getValue()
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEDoesNotExist, DNBEZeroDivide, DNBEOverflowError, DNBEUnderflowError ));

    //*
    //* FUNCTION:
    //*    turnToStack
    //*
    //* SUMMARY:
    //*    Creates a RPN stack from an expression tree.
    //*
    //* PARAMETERS:
    //*     A \param[out]{ RPNStackType& }
    //*         The RPN stack that is this filled.
    //*
    //* RETURNS:
    //*    None
    //*
    //* DESCRIPTION:
    //*    Recursively traces a tree depth first to create a RPN stack
    //* representation of the expression.  Each node pushes a pointer
    //* to itself onto the RPN stack.
    //*
    //* EXCEPTIONS:
    //*    \param{scl_bad_alloc}
    //*        This function pushes pointers onto a STL list that will
    //* throw a scl_bad_alloc if there is not enough memory.
    //*    \param{DNBEDoesNotExist}
    //*         Thrown if any of the leaf pointers are NULL.
    //*
    virtual
    void
    turnToStack( RPNStackType& RPNstack )
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEDoesNotExist ));

private:
    //
    // copy constructor, private so it can not be used, empty implemtation
    //
    DNBExpressionFunction( DNBExpressionFunction<T> &right )
    DNB_THROW_SPEC_NULL;

    //
    // The functor that is used to store the function.
    //
    DNBFunctor0wRet<T> functor_;

    //
    // This pointer points to the *ArgumentFunction instance and keeps the
    //   reference count above -1.  It is never used to access the instance.
    //
    pointer wrapperClassInstance_;
};

//
//  Include the public definition file.
//
#include "DNBExpressionFunction.cc"

#endif  /* _DNB_DNBEXPRESSIONFUNCTION_H_ */
