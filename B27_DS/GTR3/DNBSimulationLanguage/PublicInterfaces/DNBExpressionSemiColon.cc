//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview czo/BPL 01:11:13
 */


//*
//* FILE:
//*     DNBExpressionSemiColon.cc
//*
//* MODULE:
//*     DNBExpressionMD
//*
//* OVERVIEW:
//*     This module provides templates for storing addition operators in the DNBExpression Class
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     bpl         09/15/2000  Initial Implementation
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 2000 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*


template<class T>
DNBExpressionSemiColon<T>::DNBExpressionSemiColon ( pointer operand1, pointer operand2 )
    DNB_THROW_SPEC_NULL
        : DNBExpressionBinary<T>( operand1, operand2 )
{
}

template<class T>
DNBExpressionSemiColon<T>::~DNBExpressionSemiColon()
    DNB_THROW_SPEC_NULL
{
}

template<class T>
T
DNBExpressionSemiColon<T>::getValue()
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEDoesNotExist, DNBEZeroDivide, DNBEOverflowError, DNBEUnderflowError ))
{
    T lhsValue;    // Left hand side value
    T rhsValue;    // Right hand side value

    if ( ( !this->operand1_ ) || ( !this->operand2_ ) )
    {
        DNBEDoesNotExist  eNotAvailable(
            DNB_FORMAT( "The expression tree can not be evaluated because one of the leaf pointers is NULL" ) );
        throw eNotAvailable;
    }

    rhsValue = this->operand2_->getValue();
    lhsValue = this->operand1_->getValue();

    // Return the right hand side value
    return rhsValue;
}

template<class T>
T
DNBExpressionSemiColon<T>::performOperation( PostFixStackType& PostFix )
    DNB_THROW_SPEC(( DNBEDoesNotExist, DNBEZeroDivide, DNBEOverflowError, DNBEUnderflowError ))
{
    //
    // Pops the needed values off the PostFix start and returns the result
    // of the operation.  The righthand side operator pops off first
    //
    T lhsValue;    // Left hand side value
    T rhsValue;    // Right hand side value
    pointer lhsNode;    // Left hand side node
    pointer rhsNode;    // Right hand side node

    if ( PostFix.size() < 2 )
    {
        DNBEDoesNotExist  eNotAvailable(
            DNB_FORMAT( "The expression's RPN stack can not be evaluated because of insufficicent operands on postfix stack" ) );
        throw eNotAvailable;
    }

    rhsNode = PostFix.front();
    PostFix.pop_front();
    rhsValue = rhsNode->getValue();

    lhsNode = PostFix.front();
    PostFix.pop_front();
    lhsValue = lhsNode->getValue();

    // Return the right hand side value
    return rhsValue;
}

template<class T>
DNBExpressionSemiColon<T>::DNBExpressionSemiColon()
    DNB_THROW_SPEC_NULL
{
}

template<class T>
DNBExpressionSemiColon<T>::DNBExpressionSemiColon( const DNBExpressionSemiColon<T> &right )
    DNB_THROW_SPEC_NULL
{
}
