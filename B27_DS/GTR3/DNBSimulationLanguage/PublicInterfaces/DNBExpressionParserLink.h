//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//*
//* FILE:
//*     DNBExpressionParserLink.h    - protected header file
//*
//* MODULE:
//*     DNBExpressionMD
//*
//* OVERVIEW:
//*     This is file is a function for calling the parser
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     BPL         01/19/2000  Initial Implementation
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     DWB, VKA    05/08/2000  
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 2000 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _DNB_EXPRESSIONPARSERLINK_H_
#define _DNB_EXPRESSIONPARSERLINK_H_

#include <DNBSystemBase.h>
#include <scl_string.h>
#include <DNBSystemDefs.h>

#include <DNBSimulationLanguage.h>
#include <DNBExpression.h>

#include <DNBExpressionSymbolExtractor.h>
#include <DNBExpressionParser.h>

#include <DNBCountedPointer.h>
#include <DNBSymbol.h>
#include <DNBSymbolTable.h>

    //*
    //* FUNCTION:
    //*    CallParser
    //*
    //* SUMMARY:
    //*    Non-menber function that calls the parser
    //*
    //* PARAMETERS:
    //*     A \param[in]{scl_wstring} 
    //*         The expression string to be parsed
    //*     A \param[out]{DNBExpression<DNBReal>::pointer} 
    //*         This pointer is set to point to the expression tree that is 
    //* created
    //*     A \param[in]{DNBSymbolTable} 
    //*         The symbol table that allows access to attributes stored in 
    //* the world model
    //*     A \param[in]{const wchar_t}
    //*         The character that delimits the names of entites that make 
    //* up the path for a symbol.  Defaults to '/'.
    //*     A \param[in]{const wchar_t}
    //*         The character that delimits the attributes of symbols.  
    //* Defaults to ':'.
    //*     A \param[in]{const wchar_t}
    //*         The character that proceds and follows a symbol (and path) 
    //* to delimit from the rest of the expression.  Defaults to '".'
    //*
    //* RETURNS:
    //*    Nothing
    //*
    //* DESCRIPTION:
    //*    This function is a hold over from the lex/yacc parser that needed an 
    //* intermediately function to call the parser.  It calls the pre-parser and 
    //* then the parser.
    //*
    //* EXCEPTIONS:
    //*    \param{scl_bad_alloc}
    //*         This function causes the creation of a expression tree on the heap.
    //* If sufficient space is not available, scl_bad_alloc is thrown.
    //*    \param{DNBEInvalidFormat}
    //*         Thrown if there is a syntax error in the expression string
    //*    \param{DNBEDoesNotExist}
    //*         Not used for anything currently
    //*
ExportedByDNBSimulationLanguage 
void 
CallParser( scl_wstring& expressionString, 
            DNBExpression<DNBReal>::pointer& topNode, 
            DNBSymbolTable& symbolTable,
            const wchar_t  pathDelimiter = L'/',
            const wchar_t  attrDelimiter = L':', 
            const wchar_t  symbolDelimiter = L'\"')
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEInvalidFormat, DNBEDoesNotExist ));


#endif  /* _DNB_EXPRESSIONPARSERLINK_H_ */
