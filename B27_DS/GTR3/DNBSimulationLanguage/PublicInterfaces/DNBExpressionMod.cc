//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview czo/BPL 01:11:13
 */

/**
 * @quickReview ZHS 08:07:25
 * @quickReview RTL 08:07:31
 */
//*
//* FILE:
//*     DNBExpressionMod.cc
//*
//* MODULE:
//*     DNBExpressionMD
//*
//* OVERVIEW:
//*     This module provides templates for storing modulus operators in the DNBExpression Class
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     bperles     03/16/2000  Initial Implementation
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1999, 2000 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*


template<class T>
DNBExpressionMod<T>::DNBExpressionMod ( const pointer operand1, const pointer operand2 )
    DNB_THROW_SPEC_NULL
        : DNBExpressionBinary<T>( operand1, operand2 )
{
}

template<class T>
DNBExpressionMod<T>::~DNBExpressionMod()
    DNB_THROW_SPEC_NULL
{
}

template<class T>
T
DNBExpressionMod<T>::getValue()
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEDoesNotExist, DNBEZeroDivide, DNBEOverflowError, DNBEUnderflowError ))
{
    //
    // Note: Mod used to works only on real operands.
    // To be consistent with D5 convention, Mod
    // works on real and return real.
    T lhsValue;    // Left hand side value
    T rhsValue;    // Right hand side value

    if ( ( !this->operand1_ ) || ( !this->operand2_ ) )
    {
        DNBEDoesNotExist  eNotAvailable(
            DNB_FORMAT( "The expression tree can not be evaluated because one of the leaf pointers is NULL" ) );
        throw eNotAvailable;
    }

    rhsValue = this->operand1_->getValue();
    lhsValue = this->operand2_->getValue();

    if ( 0 == rhsValue )
    {
       DNBEZeroDivide  eZeroDivide(
          DNB_FORMAT( "Divide by zero error" ) );
       throw eZeroDivide;
    }


    DNBInteger32 tmpInt(lhsValue/rhsValue);

    return (lhsValue - tmpInt*rhsValue);
}

template<class T>
T
DNBExpressionMod<T>::performOperation( PostFixStackType& PostFix )
    DNB_THROW_SPEC(( DNBEDoesNotExist, DNBEZeroDivide, DNBEOverflowError, DNBEUnderflowError ))
{
    //
    // Pops the needed values off the PostFix start and returns the result
    // of the operation.  The righthand side operator pops off first
    //
    // Note: Mod used to works only on real operands.
    // To be consistent with D5 convention, Mod
    // works on real and return real.
    T lhsValue;    // Left hand side value
    T rhsValue;    // Right hand side value

    pointer lhsNode;    // Left hand side node
    pointer rhsNode;    // Right hand side node

    if ( PostFix.size() < 2 )
    {
        DNBEDoesNotExist  eNotAvailable(
            DNB_FORMAT( "The expression's RPN stack can not be evaluated because of insufficicent operands on postfix stack" ) );
        throw eNotAvailable;
    }

    rhsNode = PostFix.front();
    PostFix.pop_front();
    rhsValue = rhsNode->getValue();

    lhsNode = PostFix.front();
    PostFix.pop_front();
    lhsValue = lhsNode->getValue();

    if ( 0 == rhsValue )
    {
       DNBEZeroDivide  eZeroDivide(
          DNB_FORMAT( "Divide by zero error" ) );
       throw eZeroDivide;
    }

    DNBInteger32 tmpInt(lhsValue/rhsValue);

    return (lhsValue - tmpInt*rhsValue);
}

template<class T>
DNBExpressionMod<T>::DNBExpressionMod()
    DNB_THROW_SPEC_NULL
{
}

template<class T>
DNBExpressionMod<T>::DNBExpressionMod( const DNBExpressionMod<T> &right )
    DNB_THROW_SPEC_NULL
{
}
