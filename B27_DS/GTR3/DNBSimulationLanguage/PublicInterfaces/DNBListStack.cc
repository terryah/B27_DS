//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview BPL czhao 01:11:14
 * @error UNP Y
 * @error UNP Y
 * @quickreview mmg 03:06:24
 */

//*
//* FILE:
//*     DNBListStack.cc  - public definition file
//*
//* MODULE:
//*     DNBExpressionMD
//*
//* OVERVIEW:
//*     This module provides templates for the special stack used by 
//* DNBExpression that can be non-distructivly evaluated
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     bperles     11/10/1999  Initial Implementation
//*     rtl         06/23/2003  Provide method to return 
//*                             size of listStack_
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1999, 2000 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*


template<class T>
DNBListStack<T>::DNBListStack() 
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    //
    // Push a null item on the front of the list so the interators have 
    //   something to point to when the 'stack' is 'empty'.
    //
    T t_null_;
    listStack_.push_front(t_null_);
    currentTop_ = listStack_.begin();
    permanentTop_ = listStack_.begin();
}

template<class T>
DNBListStack<T>::DNBListStack(const DNBListStack<T> &right) 
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    //
    // Copy constructor does not currently do anything
    //
}

template<class T>
DNBListStack<T>::~DNBListStack() 
    DNB_THROW_SPEC_NULL
{
}

template<class T>
void
DNBListStack<T>::resetStackPointer() 
    DNB_THROW_SPEC_NULL
{
    currentTop_ = permanentTop_;
}

template<class T>
bool
DNBListStack<T>::isEmpty() 
    DNB_THROW_SPEC_NULL
{
    if ( currentTop_ == listStack_.begin() )
    {	
        return( true );
    }
    return( false );
}

template<class T>
void
DNBListStack<T>::flipStack() 
    DNB_THROW_SPEC_NULL
{
    //
    // Reverse the order of the list except for the front null element
    //
    typename listOfT::iterator temp = listStack_.begin();
    temp++;	
    scl_reverse( temp, listStack_.end() );
}

template<class T>
T
DNBListStack<T>::pop() 
    DNB_THROW_SPEC_NULL
{
    T object;

    if ( isEmpty() )
    {
        // 
        // To avoid returning the bottom NULL element, return the last valid element
        // 
        typename listOfT::iterator temp = listStack_.begin();

	if( listStack_.size() > 1)
	{
            temp++;	
	}

        object = *temp;
    }
    else 
	{
        object = *currentTop_;
        currentTop_--;	    // Move down pointer
	}

    return(object);
}

template<class T>
T
DNBListStack<T>::getCurrent() 
    DNB_THROW_SPEC_NULL
{
    T object;

    if ( isEmpty() )
    {
        // 
        // To avoid returning the bottom NULL element, return the last valid element
        // 
        typename listOfT::iterator temp = listStack_.begin();

	if( listStack_.size() > 1)
	{
            temp++;	
	}
        
        object = *temp;
    }
    else 
	{
        object = *currentTop_;
	}

    return(object);
}

template<class T>
void 
DNBListStack<T>::push( T object ) 
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    //
    // The allocator will through scl_bad_alloc if it does not have space
    //
    listStack_.push_back( object );

    //
    // advace pointers
    //
    currentTop_++;
    permanentTop_++;
}


template<class T>
size_t
DNBListStack<T>::length()
DNB_THROW_SPEC_ANY
{
    return listStack_.size();
}

