//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview czo/BPL 01:11:13
 */



//*
//* FILE:
//*     DNBExpressionSymbol.cc  - public definition file
//*
//* MODULE:
//*     DNBExpressionMD
//*
//* OVERVIEW:
//*     This is the class for Symbol operands that inherits from DNBExpressionOperand
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     BPL         02/28/2000  Initial Implementation
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1999 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*


template<class T>
DNBExpressionSymbol<T>::DNBExpressionSymbol( const scl_wstring& symbolName,
                                             DNBSymbolTable& symbolTable,
                                             const wchar_t  pathDelimiter,
                                             const wchar_t  attrDelimiter )
    DNB_THROW_SPEC(( DNBEInvalidFormat ))
    : symbolName_( symbolName ),
      pathDelimiter_( pathDelimiter )
{
    symbolTable.getSymbol( symbol_, symbolName_, pathDelimiter, attrDelimiter );

    if ( ! (symbol_) )
    {
        DNBEInvalidFormat eFormat(
            DNB_FORMAT( "The symbol name \"%1\" could not be located in the symbol table" ) );
        eFormat << symbolName_;
        throw eFormat;
    }

    symbol_.setValue( symbol_.getValue() );
}

template<class T>
DNBExpressionSymbol<T>::~DNBExpressionSymbol()
    DNB_THROW_SPEC_NULL
{
}

template<class T>
T
DNBExpressionSymbol<T>::getValue()
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEDoesNotExist, DNBEZeroDivide, DNBEOverflowError, DNBEUnderflowError ))
{
    return( symbol_.getValue() );
}

template<class T>
void
DNBExpressionSymbol<T>::turnToStack( RPNStackType& RPNstack )
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEDoesNotExist ))
{
    //
    // Create a new pointer to this node and push it onto the RPN stack
    //
    pointer newPtr;
    newPtr = this;
    RPNstack.push (newPtr);
}

template<class T>
void
DNBExpressionSymbol<T>::setSymbolValue( T value )
    DNB_THROW_SPEC_NULL
{
    symbol_.setValue( value );
}

template<class T>
DNBExpressionSymbol<T>::DNBExpressionSymbol()
    DNB_THROW_SPEC_NULL
{
}

template<class T>
DNBExpressionSymbol<T>::DNBExpressionSymbol( const DNBExpressionSymbol<T> &right )
    DNB_THROW_SPEC_NULL
{
}
