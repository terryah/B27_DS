//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//*
//* FILE:
//*     DNBExpressionSymbolExtractor.h    - public header file
//*
//* MODULE:
//*     DNBExpressionMD
//*
//* OVERVIEW:
//*     This module provides extraction of symbol names from expression strings
//* and storage of delimiter characters
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     BPL         02/29/2000  Initial Implementation
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     DWB, VKA    05/08/2000  
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 2000 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _DNB_EXPRESSIONSYMBOLEXTRACTOR_H_
#define _DNB_EXPRESSIONSYMBOLEXTRACTOR_H_

#include <DNBSystemBase.h>
#include <scl_string.h>
#include <scl_map.h>
#include <scl_utility.h>
#include <DNBSystemDefs.h>
#include <DNBSimulationLanguage.h>

#include <DNBCountedObject.h>
#include <DNBCountedPointer.h>

#include <DNBStandardLib.h>

//*
//* CLASS:
//*     DNBExpressionSymbolExtractor
//*
//* SUMMARY: 
//*     Identifies, removes, and stores symbols from expression strings 
//*
//* PREREQUISITES:
//*     DNBSymbolTable, DNBSymbol, DNBCountedObject 
//*
//* DESCRIPTION:
//*     This class pre-parses expression strings for the DNBExpression 
//* class so that the lexer does not have to identify symbols names 
//* during parsing.  This ensures that the lexer will not get confused
//* by symbols that contain mathematical operators.  Symbols names are 
//* identified and stored inside this 
//* class.  A new expression string is passed back to the caller with 
//* the symbol names replaced with the key(ID) values.  The symbol names  
//* can then be retrieved using the key values.
//*     To allow identification of symbol names, these names must be 
//* proceeded and followed by a delimiter symbol.  The default delimiter
//* is the double quote ("), but it can be set by the user.  The key 
//* values used to identify stored symbols are integer values that are 
//* generated based on the order that names are stored.  In the new 
//* string that is passed back to the user, the key values replace the 
//* symbol names.  So that the key values can be identified by the lexer, 
//* they are also contained within a delimiter.  This delimiter is the 
//* same as origonal symbol delimiter.  The 
//* string with the name replaced with key values is used only for 
//* parsing and is not stored beyond that stage.
//*
//*     This class is also used to pass the path and attribute delimiters
//* into the parser so that they are available when a symbol is requested
//* during creation of the DNBExpressionSymbol node.
//*
//* EXAMPLE:
//*    scl_wstring expressionString = L"1 + 3 - sin(\"x\")";
//*    scl_wstring preParsedExpression;
//*    wchar_t pathDelimiter = L'/';
//*    wchar_t attrDelimiter = L':';
//*    wchar_t symbolDelimiter = L'\"';
//*    DNBExpressionSymbolExtractor::extractorPtr extractedSymbols = 
//*             new DNBExpressionSymbolExtractor( expressionString, 
//*                                               preParsedExpression, 
//*                                               pathDelimiter, 
//*                                               attrDelimiter, 
//*                                               symbolDelimiter );
//*
class ExportedByDNBSimulationLanguage DNBExpressionSymbolExtractor 
    : public DNBCountedObject
{
public:
    //*
    //* TYPEDEF:
    //*     extractorPtr
    //*
    //* SUMMARY:
    //*     Smart pointer to an instance of the DNBExpressionSymbolExtractor class
    //*
    //* DESCRIPTION:
    //*     This type definition specifies Smart pointer to an instance of the 
    //* DNBExpressionSymbolExtractor class, which is used to store the symbol 
    //* names extracted from the expression string
    //*
    typedef 
    DNBCountedPointer< DNBExpressionSymbolExtractor >  
    extractorPtr;   
    
    //*
    //* FUNCTION:
    //*    DNBExpressionSymbolExtractor
    //*
    //* SUMMARY:
    //*    Initiation constructor for DNBExpressionSymbolExtractor
    //*
    //* PARAMETERS: 
    //*     A \param[in]{ const scl_wstring }
    //*         The expression string to have its symbol names extracted.
    //*     A \param[out]{ scl_wstring& }
    //*         The resulting expression string with its symbol names 
    //* replaced with key values.
    //*     A \param[in]{const wchar_t}
    //*        The character that delimits the names of entites that make up 
    //* the path for a symbol.  Defaults to '/'.
    //*     A \param[in]{const wchar_t}
    //*        The character that delimits the attributes of symbols.  
    //* Defaults to ':'.
    //*     A \param[in]{const wchar_t}
    //*        The character that proceds and follows a symbol (and path) to 
    //* delimit from the rest of the expression.  Defaults to '".'
    //*
    //* RETURNS:
    //*    Nothing
    //*
    //* DESCRIPTION:
    //*    This initiation constructor takes in an expression string, 
    //* identifies the symbols names, and stores the names inside the 
    //* class.  A new expression string is passed back to the caller
    //* with the symbol names replaced with the key values.
    //*
    //* EXCEPTIONS:
    //*    \param{scl_bad_alloc}
    //*         This class stores symbol names in a STL map.  If 
    //* insufficient memory is available to add a string to the map, 
    //* scl_bad_alloc is thrown.
    //*    \param{DNBEInvalidFormat}
    //*         Thrown if there is an unballenced symbol delimiter.
    //*
    DNBExpressionSymbolExtractor( const scl_wstring expression, 
                                  scl_wstring& preParsedExpression,
                                  const wchar_t  pathDelimiter = L'/',
                                  const wchar_t  attrDelimiter = L':',
                                  const wchar_t  symbolDelimiter = L'\"' )
        DNB_THROW_SPEC(( scl_bad_alloc, DNBEInvalidFormat ));

    //*
    //* FUNCTION:
    //*    ~DNBExpressionSymbolExtractor
    //*
    //* SUMMARY:
    //*    Destructor for DNBExpressionSymbolExtractor
    //*
    //* PARAMETERS:
    //*     None 
    //*
    //* RETURNS:
    //*    Nothing
    //*
    //* DESCRIPTION:
    //*    Destructor for DNBExpressionSymbolExtractor
    //*
    //* EXCEPTIONS:
    //*    None
    //*
    ~DNBExpressionSymbolExtractor()
        DNB_THROW_SPEC_NULL;

    //*
    //* FUNCTION:
    //*    getSymbolNameWithKey
    //*
    //* SUMMARY:
    //*    Reterives a symbol name based on a key value.
    //*
    //* PARAMETERS:
    //*     A \param[in]{DNBInteger32}
    //*         The key value for the symbol name
    //*
    //* RETURNS:
    //*     A string that is the name of the symbol.
    //*
    //* DESCRIPTION:
    //*    This function returns a string containing the symbol name 
    //* stored under the key that the user passes in.
    //*
    //* EXCEPTIONS:
    //*    None
    //*
    scl_wstring 
    getSymbolNameWithKey( const DNBInteger32 n )
        DNB_THROW_SPEC_NULL;


    //*
    //* FUNCTION:
    //*    getPathDelimiter
    //*
    //* SUMMARY:
    //*    Returns the path delimiter.
    //*
    //* PARAMETERS:
    //*     None:
    //*
    //* RETURNS:
    //*     wchar_t
    //*
    //* DESCRIPTION:
    //*    This function returns the character that is the path delimiter 
    //* for symbols.
    //*
    //* EXCEPTIONS:
    //*    None
    //*
    wchar_t
    getPathDelimiter()
        const
        DNB_THROW_SPEC_NULL;

    //*
    //* FUNCTION:
    //*    getAttrDelimiter
    //*
    //* SUMMARY:
    //*    Returns the attribute delimiter
    //*
    //* PARAMETERS:
    //*     None:
    //*
    //* RETURNS:
    //*     wchar_t
    //*
    //* DESCRIPTION:
    //*    This function returns the attribute delimiter.
    //*
    //* EXCEPTIONS:
    //*    None
    //*
    wchar_t
    getAttrDelimiter()
        const
        DNB_THROW_SPEC_NULL;

    //*
    //* FUNCTION:
    //*    getSymbolIDDelimiter
    //*
    //* SUMMARY:
    //*    Returns the delimiter for symbol ID values
    //*
    //* PARAMETERS:
    //*     None:
    //*
    //* RETURNS:
    //*     wchar_t
    //*
    //* DESCRIPTION:
    //*    This function returns the character that delimiter for symbol 
    //* ID values.
    //*
    //* EXCEPTIONS:
    //*    None
    //*
    wchar_t
    getSymbolIDDelimiter()
        const
        DNB_THROW_SPEC_NULL;

private:
    // 
    // Default Constructor, made private so no one can use it
    // 
    DNBExpressionSymbolExtractor()
        DNB_THROW_SPEC_NULL;

    // 
    // Copy Constructor, made private so no one can use it, empty implemtation
    // 
    DNBExpressionSymbolExtractor( const DNBExpressionSymbolExtractor &right )
        DNB_THROW_SPEC_NULL;

    // 
    // This function does the actual work of extracting symbol names, 
    //     storing them, and replacing them with keys in the new string.
    // 
    void 
    ExtractSymbols( const scl_wstring expression, 
                                                  scl_wstring& preParsedExpression )
        DNB_THROW_SPEC(( scl_bad_alloc, DNBEInvalidFormat ));

    // 
    // This function stores a string in a map based on a key value of 
    //    the number of strings already stored in the map.  i.e.: the 
    //    1st string's key is '0', the 2nd's is '1', etc.  The key value 
    //    is returned.
    // 
    DNBInteger32 
    InsertSymbolName( const scl_wstring symbolName )
        DNB_THROW_SPEC(( scl_bad_alloc ));

    typedef
    scl_pair < const DNBInteger32, scl_wstring >
    mapSize;

    // 
    // The map used to store the symbol names and their key value
    // 
    scl_map < DNBInteger32, scl_wstring, scl_less<DNBInteger32> > symbolNames_;

    // 
    // The delimiter used to seperate the names in the path of the symbol
    // 
    wchar_t 
    pathDelimiter_;

    // 
    // The delimiter used to seperate the attributes from the symbols
    // 
    wchar_t 
    attrDelimiter_;

    // 
    // The delimiter used to recognize symbol names.  The delimiter should 
    // be the character immediately before and after the symbol name.
    // 
    wchar_t 
    symbolDelimiter_;

    // 
    // The delimiter used by the lexer during parsing to recognize symbol 
    // name keys.  The delimiter should be the character immediately before 
    // and after the key value.
    // 
    wchar_t 
    keyDelimiter_;

};


#endif  /* _DNB_EXPRESSIONSYMBOLEXTRACTOR_H_ */
