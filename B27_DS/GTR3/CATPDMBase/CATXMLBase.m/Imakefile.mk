# COPYRIGHT DASSAULT SYSTEMES 2000
#======================================================================
# Imakefile for module CATXMLBase.m
#======================================================================
#
#  Sep 2003  Creation: Code generated by the CAA wizard  LDY
#======================================================================
#
# INCLUDED SHARED LIBRARY 
#

BUILT_OBJECT_TYPE=SHARED LIBRARY

LINK_WITH= JS0GROUP AS0STARTUP AC0SPBAS CATPDMBase CATPDMBaseItfCPP \
           CATPrsRep CATObjectModelerBase \
           XMLUtils xmlxerces xmlicuuc CATXMLParserItf

################
OS = Windows_NT
CXX_EXCEPTION=
LOCAL_CCFLAGS = -GX -DNATIVE_EXCEPTION

OS = win_b64
CXX_EXCEPTION=
LOCAL_CCFLAGS = -DNATIVE_EXCEPTION

################
OS            = win_a
CXX_EXCEPTION=
LOCAL_CCFLAGS = -D_WIN9X_SOURCE -DNATIVE_EXCEPTION

################
OS = IRIX
LOCAL_CCFLAGS      = -DIRIX -DNATIVE_EXCEPTION
CXX_EXCEPTION=
LOCAL_LDFLAGS = -v
CXX_TEMPLATE_INC   = -auto_include
CXX_TEMPLATE_PRELK = -prelink
CXX_TEMPLATE_INST  = -ptused

################
OS = HP-UX
CXX_EXCEPTION =
LOCAL_CCFLAGS = -DNATIVE_EXCEPTION

################
OS = AIX
LOCAL_CCFLAGS = -DNATIVE_EXCEPTION
LOCAL_LDFLAGS = -brtl

################
OS = SunOS
LOCAL_CCFLAGS =  -DNATIVE_EXCEPTION







