// COPYRIGHT Dassault Systemes 2007
//===================================================================
//
// CATPDMCATIAServices.h
// Header definition of CATPDMCATIAServices
//
//===================================================================
//
// Usage notes:
//
//===================================================================
//
//  Mar 2007  Creation: Code generated by the CAA wizard  IRW
//===================================================================

/**
  * @CAA2Level L1
  * @CAA2Usage U1
  */

#ifndef CATPDMCATIAServices_H
#define CATPDMCATIAServices_H

#include "ExportedByCATPDMBaseEnoviaV5CAA.h"
#include "CATBaseUnknown.h"
#include "CATIPLMIdentificator.h"
#include "CATListOfCATIPLMIdentificator.h"

//-----------------------------------------------------------------------

/**
* Services to interface with CATIA client Modeler. 
*
* <br><b>Role:</b>Provide various services to support the CATIA client modeler 
* <br>
*
**/
class ExportedByCATPDMBaseEnoviaV5CAA CATPDMCATIAServices: public CATBaseUnknown
{
    public:
/**
* Enable the client to go from a CATIPLMIdentificator to a CATIA modeler object already loaded in session.
*
*<br><b>Role:</b>This method enables to bind an existing CATIA modeler object from a CATIPLMIdentificator   
*<p>Commonly supported ENOVIA ids are ENOVIA_VPMTPDocumentVersion, ENOVIA_VPMPartVersion, 
*   ENOVIA_VPMItemInstance, ENOVIA_VPMProductRootClass
*<br><b>restrictions apply</b>: not all ENOVIA ids can be binded to CATIA. 
*<p>
*@param ipId
*  Identificator of the object
*
*  @param opObj
*  Modeler object in session
* 
* @return 
*   <ul>
*     <li><tt>S_OK</tt>: identificator exist as a modeler object in current CATIA session</li> 
*     <li><tt>E_FAIL</tt>: object cannot be binded to an existing modeler object 
*  </ul>
*/

static HRESULT BindEnoviaIdToCATIA(CATIPLMIdentificator* ipId, CATBaseUnknown** opObj);  

/**
* Enable the client to go from a CATIA Modeler object and get its identificator.
*
*<br><b>Role:</b>This method enables to get a CATIPLMIdentificator from an existing CATIA modeler object   
*<p>Commonly supported CATIA Object are CATDocument or CATIProduct (root being PRC, intermediate level II, leave PV)
*<br><b>restrictions apply</b>: not all CATIA Object correspond to ENOVIA persistant ids. 
*<p>
*<p> 
* @param ipObj
*  CATIA modeler object
*
*  @param opId
*  identificator for ENOVIA
* 
* @return 
*   <ul>
*     <li><tt>S_OK</tt>: identificator exist as a modeler object in current CATIA session</li> 
*     <li><tt>E_FAIL</tt>: object cannot convert to an ENOVIA persistant Ids 
*  </ul>
*/

static HRESULT ExtractEnoviaIdFromCATIA(CATBaseUnknown* ipObj, CATIPLMIdentificator** opId); 

  
};

#endif
