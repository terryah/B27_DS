// COPYRIGHT Dassault Systemes 2008
//===================================================================
//
// CATPDMPVRServices.h
// Header definition of CATPDMPVRServices
//
//===================================================================
//
// Usage notes:
//    CAA exposure of services on Product View Result
//
//===================================================================
//
//  Oct 2008  Creation: Code generated by the CAA wizard  PPN
//===================================================================
/**
  * @CAA2Level L0
  * @CAA2Usage U1
  */

#ifndef CATPDMPVRServices_H
#define CATPDMPVRServices_H

#include "ExportedByCATPDMBaseEnoviaV5CAA.h"
#include "IUnknown.h"   

class CATDocument;
class CATUnicodeString;

//-----------------------------------------------------------------------

/** 
* Create PVR API
*
* <br><b>Role</b>: a CATIA API to create a Product View Result document in the CATIA session.
* This API must be used in a correctly initialized CATIA session, previously connected to ENOVIA
* will *not* achieve save in EV5
*
* @param iOriginalPVRDoc [in]
*   The original PVR document (must have been previously loaded in CATIA session)
*   This input document is used as a template to create the NewPVR document
*   NULL input pointer is invalid arg in R18 release
*   // for later Release (>=20) : if NULL input pointer, the PVR doc will be create "from scratch" (new CATPRoduct document)
*  This pointer will be reset (to NULL) during the implementation, 
*     to reflect the original PVR doc will be no longer available in the session
*
* @param iPVName [in]
*   value of V_ID attribute of the already existing PV in DB, where to attach the new PVR doc
*
* @param iPVVersion [in]
*   value of V_VERSION attribute of the already existing PV in DB, where to attach the new PVR doc 
*    May be empty string in case where only one version exists for this part
*    In case where several versions exist, this arg must be fed
*
* @param iNewPVRDocName [in]
*   Name of the new PVR document in ENOVIA. Must be different from iOriginalPVDocName. 
*      If null, same default than interactive (PVName and �_PVR� suffix)
*
* @param iPVSCaptureName [in]
*   Name of the new PVS to be used for the new PVR.
*   If null, same no PVS management (unfiltered PVR).
*   This method never creates PVS in server
*
* @param iWithSynchronization [in]
*   if 0 , create the PVR without automatically synchronizing it
*   if 1 , synchronize the PVR immediately after the creation
*  other values are not supported
*
* @param ocreatedPVRDocument [out]
*   on the created PVR document (In CATIA session)
*  NB: Document is ready to be saved in ENOVIA V5.
*  pointer must have been initialized to NULL prior to call
*
* @return
*    S_OK if all is OK
*    E_INVALIDARG if error while checking input arg
*    E_FAIL if API failed
*
**/

     ExportedByCATPDMBaseEnoviaV5CAA HRESULT CAACreatePVR ( CATDocument * &iOriginalPVRDoc   ,
                                 const CATUnicodeString & iPVName , 
                                 const CATUnicodeString & iPVVersion ,  
                                 const CATUnicodeString & iNewPVRDocName ,
                                 const CATUnicodeString & iPVSCaptureName ,
                                 int iWithSynchronization, 
                                 CATDocument * & ocreatedPVRDocument );


/** 
* Synchronize  PVR API
* will *not* achieve save in EV5
*
* <br><b>Role</b>: a CATIA API to synchronize  a Product View Result document in the CATIA session, from the corresponding exposed strucure in DataBase
* This API must be used in a correctly initialized CATIA session, previously connected to ENOVIA
*
* @param ioPVRDocToSync  [in-out]
*   The original PVR document (must have been previously loaded in CATIA session)
*   This document will be modified in CATIA session in order to be synchronized with 
*   the corresponding exploded structure in Database
*   if NULL return E_INVALIDARG
*
* @param iSimul  [in]
*   if 0, will really execute the synchronisation
*   if 1, will build the comparison but without applying the synchronization. 
*         This simulation mode will kept the PVR document unchanged
*  others values are invalid ()return E_INVALIDARG
*
* @ CATUnicodeString oXMLReport  [out]
*   the report will be an XML string, 
*   having same tags/rules than the report generated by the already existing PVR batch utility
*
* @return
*    S_OK if all is OK
*    E_INVALIDARG if error while checking input arguments (eg ioPVRDocToSync  null or not a PVR doc)
*    E_FAIL if API failed
*
*/
     ExportedByCATPDMBaseEnoviaV5CAA HRESULT CAASynchronizePVR ( CATDocument * ioPVRDocToSync ,
                                      int iSimul , 
                                      CATUnicodeString &oXMLReport );

//-----------------------------------------------------------------------

#endif
