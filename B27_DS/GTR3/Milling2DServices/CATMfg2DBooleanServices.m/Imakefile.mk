# ******************************************************************************
#
# --- ID -----------------------------------------------------------------------
#
# Owner     : UTD [BTI]
# Framework : Milling2DServices
# Module    : CATMfg2DBooleanServices.m
# File      : Imakefile.mk
#
#
# --- DESCRIPTION --------------------------------------------------------------
#
# 2D contours modelization and services.
#
#
# --- HISTORY ------------------------------------------------------------------
#
# -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   R19  -   -   -
#
# 2007-06-28 : LAM : # Create file.
#
# ******************************************************************************

BUILT_OBJECT_TYPE = SHARED LIBRARY

#ifdef LOCAL_DEFINITION_FOR_IID
LINK_WITH_FOR_IID =       \
  CATAfrUUID
#else
LINK_WITH_FOR_IID =
#endif

LINK_WITH =               \
  $(LINK_WITH_FOR_IID)    \
  CATCGMGeoMath           \
  CATGeometricObjects     \
  CATMathStream           \
  CATMathematics          \
  CATMfg2DBooleanBase     \
  CATMilling3DToolPathItf \
  CATV4System             \
  CATVisualization        \
  CATViz                  \
  CO0LSTPV                \
  CO0LSTST                \
  Collections             \
  JS03TRA                 \
  JS0CORBA                \
  JS0ERROR                \
  JS0GROUP                \
  JS0SCBAK                \
  MfgNurbsOutput          \
  NS0S3STR                \
  NS0SI18N                \
  VE0MDL                  \
  YN000MFL

INCLUDED_MODULES =            \
  DebugTools                  \
  Mfg2DBooleanImplementation  \
  Mfg2DBooleanOperator        \
  Mfg2DGraph

IMPACT_ON_IMPORT = YES

OS = AIX
SYS_LIBS = -lxlf -lxlf90 -lxlfpad
LOCAL_CFLAGS = -D_AIX_SOURCE

OS = IRIX
SYS_LIBS = -lftn

OS = HP-UX
# -lf is for HP 10 whereas -lF90 is for HP 11
#if os hpux_a
SYS_LIBS = -lf
#else
SYS_LIBS = -lF90
#endif

OS = hpux_b64
# -lf is for HP 10 whereas -lF90 is for HP 11
#if os hpux_a
SYS_LIBS = -lf
#else
SYS_LIBS = -lF90 -lcps
#endif

OS = SunOS
#SYS_LIBS = -lF77 -lM77

OS = Windows_NT
