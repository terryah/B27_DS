# ******************************************************************************
#
# --- ID -----------------------------------------------------------------------
#
# Owner     : UTD [BTI]
# Framework : Milling2DServices
# Module    : CATMfgNurbsServices.m
# File      : Imakefile.mk
#
#
# --- DESCRIPTION --------------------------------------------------------------
#
# Nurbs management.
#
#
# --- HISTORY ------------------------------------------------------------------
#
# -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   R19  -   -   -
#
# 2007-06-29 : LAM : # Create file.
#
# 2007-09-25 : LAM : # Link with CATMfgAlgosServices.
#
# ******************************************************************************

BUILT_OBJECT_TYPE = SHARED LIBRARY

LINK_WITH =               \
  CATCGMGeoMath           \
  CATMathStream           \
  CATMfg2DBooleanServices \
  CATMfgAlgosServices     \
  CO0LSTPV                \
  CO0LSTST                \
  Collections             \
  JS03TRA                 \
  JS0CORBA                \
  JS0ERROR                \
  JS0GROUP                \
  JS0SCBAK                \
  YN000FUN                \
  YN000MAT                \
  YP00IMPL

INCLUDED_MODULES = \
  MfgNurbsOutput
