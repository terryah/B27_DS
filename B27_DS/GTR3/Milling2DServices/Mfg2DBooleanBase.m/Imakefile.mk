# ******************************************************************************
#
# --- ID -----------------------------------------------------------------------
#
# Owner     : UTD [BTI]
# Framework : Milling2DServices
# Module    : Mfg2DBooleanBase.m
# File      : Imakefile.mk
#
#
# --- DESCRIPTION --------------------------------------------------------------
#
# Helical generator and services for 2D contours.
#
#
# --- HISTORY ------------------------------------------------------------------
#
# -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   R05  -   -   -
#
# 2000-04-26 : *** : # Create file.
#
# ******************************************************************************

BUILT_OBJECT_TYPE = NONE

OS = AIX
SYS_LIBS = -lxlf -lxlf90 -lxlfpad
LOCAL_CFLAGS = -D_AIX_SOURCE

OS = IRIX
SYS_LIBS = -lftn

OS = HP-UX
SYS_LIBS = -lf

OS = SunOS
SYS_LIBS = -lF77 -lM77

OS = Windows_NT
