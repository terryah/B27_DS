# ******************************************************************************
#
# --- ID -----------------------------------------------------------------------
#
# Owner     : UTD [BTI]
# Framework : Milling2DServices
# Module    : CATMfgAlgosServices.m
# File      : Imakefile.mk
#
#
# --- DESCRIPTION --------------------------------------------------------------
#
# Services on geometry and geometric modelization of polycurve objects.
#
#
# --- HISTORY ------------------------------------------------------------------
#
# -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   R19  -   -   -
#
# 2007-09-21 : LAM : # Create file.
#
# 2009-03-18 : BTI : # Link with FrFTopologicalOpe.
#
# ******************************************************************************

BUILT_OBJECT_TYPE = SHARED LIBRARY

LINK_WITH =                  \
  CATBasicTopologicalOpe     \
  CATCGMGeoMath              \
  CATFreeFormOperators       \
  CATGeometricOperators      \
  CATManufacturingInterfaces \
  CATMathStream              \
  CATMfg2DBooleanServices    \
  CATTopologicalOperators    \
  CO0LSTPV                   \
  CO0LSTST                   \
  Collections                \
  FrFTopologicalOpe          \
  JS03TRA                    \
  JS0CORBA                   \
  JS0ERROR                   \
  JS0GROUP                   \
  JS0SCBAK                   \
  YI00IMPL                   \
  YN000FUN                   \
  YN000MAT                   \
  YP00IMPL

INCLUDED_MODULES = \
  MfgGeoAlgo       \
  MfgPolyCurve
