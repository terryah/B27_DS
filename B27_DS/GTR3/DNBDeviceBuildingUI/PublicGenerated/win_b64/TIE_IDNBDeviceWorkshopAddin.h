#ifndef __TIE_IDNBDeviceWorkshopAddin
#define __TIE_IDNBDeviceWorkshopAddin

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "IDNBDeviceWorkshopAddin.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface IDNBDeviceWorkshopAddin */
#define declare_TIE_IDNBDeviceWorkshopAddin(classe) \
 \
 \
class TIEIDNBDeviceWorkshopAddin##classe : public IDNBDeviceWorkshopAddin \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(IDNBDeviceWorkshopAddin, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual void              CreateCommands () ; \
      virtual CATCmdContainer * CreateToolbars () ; \
};



#define ENVTIEdeclare_IDNBDeviceWorkshopAddin(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual void              CreateCommands () ; \
virtual CATCmdContainer * CreateToolbars () ; \


#define ENVTIEdefine_IDNBDeviceWorkshopAddin(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
void               ENVTIEName::CreateCommands ()  \
{ \
 (ENVTIECALL(IDNBDeviceWorkshopAddin,ENVTIETypeLetter,ENVTIELetter)CreateCommands ()); \
} \
CATCmdContainer *  ENVTIEName::CreateToolbars ()  \
{ \
return (ENVTIECALL(IDNBDeviceWorkshopAddin,ENVTIETypeLetter,ENVTIELetter)CreateToolbars ()); \
} \


/* Name of the TIE class */
#define class_TIE_IDNBDeviceWorkshopAddin(classe)    TIEIDNBDeviceWorkshopAddin##classe


/* Common methods inside a TIE */
#define common_TIE_IDNBDeviceWorkshopAddin(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(IDNBDeviceWorkshopAddin, classe) \
 \
 \
CATImplementTIEMethods(IDNBDeviceWorkshopAddin, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(IDNBDeviceWorkshopAddin, classe, 1) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(IDNBDeviceWorkshopAddin, classe) \
CATImplementCATBaseUnknownMethodsForTIE(IDNBDeviceWorkshopAddin, classe) \
 \
void               TIEIDNBDeviceWorkshopAddin##classe::CreateCommands ()  \
{ \
   ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateCommands (); \
} \
CATCmdContainer *  TIEIDNBDeviceWorkshopAddin##classe::CreateToolbars ()  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateToolbars ()); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_IDNBDeviceWorkshopAddin(classe) \
 \
 \
declare_TIE_IDNBDeviceWorkshopAddin(classe) \
 \
 \
CATMetaClass * __stdcall TIEIDNBDeviceWorkshopAddin##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_IDNBDeviceWorkshopAddin,"IDNBDeviceWorkshopAddin",IDNBDeviceWorkshopAddin::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_IDNBDeviceWorkshopAddin(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(IDNBDeviceWorkshopAddin, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicIDNBDeviceWorkshopAddin##classe(classe::MetaObject(),IDNBDeviceWorkshopAddin::MetaObject(),(void *)CreateTIEIDNBDeviceWorkshopAddin##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_IDNBDeviceWorkshopAddin(classe) \
 \
 \
declare_TIE_IDNBDeviceWorkshopAddin(classe) \
 \
 \
CATMetaClass * __stdcall TIEIDNBDeviceWorkshopAddin##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_IDNBDeviceWorkshopAddin,"IDNBDeviceWorkshopAddin",IDNBDeviceWorkshopAddin::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_IDNBDeviceWorkshopAddin(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(IDNBDeviceWorkshopAddin, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicIDNBDeviceWorkshopAddin##classe(classe::MetaObject(),IDNBDeviceWorkshopAddin::MetaObject(),(void *)CreateTIEIDNBDeviceWorkshopAddin##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_IDNBDeviceWorkshopAddin(classe) TIE_IDNBDeviceWorkshopAddin(classe)
#else
#define BOA_IDNBDeviceWorkshopAddin(classe) CATImplementBOA(IDNBDeviceWorkshopAddin, classe)
#endif

#endif
