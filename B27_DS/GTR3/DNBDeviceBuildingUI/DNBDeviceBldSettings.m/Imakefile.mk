#
#   Imakefile.mk for DNBDeviceBldSettings
#   Copyright (C) DELMIA Corp., 2002
#
#
BUILT_OBJECT_TYPE=SHARED LIBRARY
#
LINK_WITH    =          DI0PANV2        \
                        JS0CORBA        \
                        JS0FM           \
                        AD0XXBAS        \
                        JS0GROUP        \
                        JS0FM           \
                        CATDlgStandard  \
                        OM0EDPRO        \

#
# Define the build options for the current module.
#
OS      = Windows_NT
BUILD   = YES

OS      = IRIX
BUILD   = YES

OS      = SunOS
BUILD   = YES

OS      = AIX
BUILD   = YES

OS      = HP-UX
BUILD   = YES

OS      = win_a
BUILD   = NO
