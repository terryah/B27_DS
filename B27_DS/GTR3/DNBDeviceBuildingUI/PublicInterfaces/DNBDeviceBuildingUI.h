// COPYRIGHT Dassault Systemes 2006
/** @CAA2Required */
/*---------------------------------------------------------------------*/
/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */
/*---------------------------------------------------------------------*/

#ifdef  _WINDOWS_SOURCE
#ifdef  __DNBDeviceBuildingUI
#define ExportedByDNBDeviceBuildingUI     __declspec(dllexport)
#else
#define ExportedByDNBDeviceBuildingUI     __declspec(dllimport)
#endif
#else
#define ExportedByDNBDeviceBuildingUI
#endif
