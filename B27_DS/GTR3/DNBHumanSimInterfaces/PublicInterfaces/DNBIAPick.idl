/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */

#ifndef DNBIAPick_IDL
#define DNBIAPick_IDL
/*IDLREP*/

//==============================================================================
// COPYRIGHT Dassault Systemes 2003
//==============================================================================
//
// DNBIAPick.idl
//    Automation interface for the PickActivity element 
//
//==============================================================================
//  Usage:
//  ------
//
//
//==============================================================================
//  Inheritance:
//  ------------
//
//==============================================================================
// HISTORY:
//
//    Author  Date(yr/mm/dd)  Purpose
//    ------  --------------  -------
//    LFS     2003/07/01      Creation
//    LFS     2003/07/28      Changes to include the IDL file instead of forward
//                            declaration. The generated *.h file doesn't show
//                            forward declartion except baseclasses, SafeArray
//                            classes.
//    SEO     2008/01/24      CAA migration
//==============================================================================

//---Product structure
#include "CATIAProduct.idl"
#include "DNBIAMfgAssembly.idl"
// --- DNBHumanSimInterfaces
#include "DNBHumanSimDefs.idl"
#include "DNBIAWorkerActivity.idl"

/**
* The object that represents an PickActivity.
*/
interface DNBIAPick : DNBIAWorkerActivity 
{

   //---------------------------------------------------------------------------
   // PickedProducts
   //---------------------------------------------------------------------------
   /**
   * Returns or Sets Picked Products
   */
   HRESULT GetPickedProducts(inout  CATSafeArrayVariant pPickedProds);
   HRESULT SetPickedProducts(in     CATSafeArrayVariant pPickedProds);

   //---------------------------------------------------------------------------
   // PickingHand
   //---------------------------------------------------------------------------
   /**
   * Returns or Sets "Picking Hand"
   * (see @href HTSHand for list of possible values )
   */
#pragma PROPERTY PickingHand
   HRESULT get_PickingHand(out /*IDLRETVAL*/ HTSHand eHtsHand);
   HRESULT put_PickingHand(in HTSHand eHtsHand);

   /**
   * Returns or Sets�Pick Type�
   * (see @href HTSPickType for list of possible values)
   * SINGLE_HAND for Single-Handed Pick, BOTH_HANDS for Double-Handed Pick 
   */
#pragma PROPERTY PickType
   HRESULT get_PickType(out /*IDLRETVAL*/ HTSPickType ePickType);
   HRESULT put_PickType(in HTSPickType ePickType);

   /** 
   * Adds a product to the List of Picked Items  
   */
   HRESULT AddPickedProduct(in CATIAProduct pPickedItem);

   /**  
   * Removes a product from the list of Picked items 
   * (see @href CATIAProduct for list of possible values)
   */
   HRESULT RemovePickedProduct(in CATIAProduct pPickedItem);

   /** 
   * Adds a Manufacturing Assembly to the List of Picked Items  
   * (see @href DNBIAMfgAssembly for list of possible values)
   */
   HRESULT AddPickedMfgAssembly(in DNBIAMfgAssembly pPickedItem);

   /**  
   * Removes a Manufacturing Assembly from the list of Picked items 
   * (see @href DNBIAMfgAssembly for list of possible values)
   */
   HRESULT RemovePickedMfgAssembly(in DNBIAMfgAssembly pPickedItem);


   //---------------------------------------------------------------------------
   // Place Link
   //---------------------------------------------------------------------------
   /**
   * Sets or append a link to a Place activity
   * (see @href CATIAActivity for list of possible values)
   */
   HRESULT SetPlaceAct(in CATIAActivity placeAct );

};

// Interface name : DNBIAPick
#pragma ID DNBIAPick "DCE:c178788d-f24a-420b-a58cb2917f9a6d76"

#pragma DUAL DNBIAPick

// VB object name : Pick (Id used in Visual Basic)
#pragma ID PickActivity "DCE:e189d1ed-c8d9-4774-9dcad46557e3913b"

#pragma ALIAS DNBIAPick PickActivity

#endif
