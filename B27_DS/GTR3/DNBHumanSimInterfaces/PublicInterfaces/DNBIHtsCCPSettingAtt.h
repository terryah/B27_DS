/**
 * @CAA2Level L0
 * @CAA2Usage U3
 */
#ifndef DNBIHtsCCPSettingAtt_H
#define DNBIHtsCCPSettingAtt_H
//==============================================================================
// COPYRIGHT Dassault Systemes 2004
//==============================================================================
//
// DNBIHtsCCPSettingAtt.h
//
//==============================================================================
//  Usage:
//  ------
//
//
//==============================================================================
// HISTORY:
//
//    Author  Date(yr/mm/dd)  Purpose
//    ------  --------------  -------
//    ATJ     2004/03/01      Creation
//    CTA     2005/04/15      LFS: Adding option for AutomatizedCCP
//    SEO     2008/01/31      CAA migration
//==============================================================================

//#include "DNBHumanSimItfCPP.h"
#include "DNBHumanSimInterfaces.h"
#include "CATBaseUnknown.h"

extern ExportedByDNBHumanSimInterfaces IID IID_DNBIHtsCCPSettingAtt;

class CATSettingInfo;
class CATUnicodeString;

//------------------------------------------------------------------
class ExportedByDNBHumanSimInterfaces DNBIHtsCCPSettingAtt: public CATBaseUnknown
{
	CATDeclareInterface;
	
public:

	/*******************
	* Compass Position *
	*******************/
	virtual HRESULT GetCompassPos( unsigned int & compassPos ) = 0;
	virtual HRESULT SetCompassPos( unsigned int & compassPos ) = 0;

	/**
	* Retrieves the state of the CompassPos parameter.
	* @param oInfo
	*	Address of an object CATSettingInfo.
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
	virtual HRESULT GetCompassPosInfo( CATSettingInfo * oInfo ) = 0;
	
	/** 
	* Locks or unlocks the CompassPos parameter.
	* <br><b>Role</b>: Locks or unlocks the CompassPos parameter if the
	* operation is allowed in the current administrated environment. In user mode 
	* this method will always return E_FAIL.
	* @param iLocked
	*	the locking operation to be performed
	*	<b>Legal values</b>:
	*	<br><tt>1 :</tt>   to lock the parameter.
	* 	<br><tt>0:</tt>   to unlock the parameter.
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
	virtual HRESULT SetCompassPosLock( unsigned char iLocked ) = 0;


	/*******************
	* Current Position *
	*******************/
	virtual HRESULT GetCurrentPos( unsigned int & currentPos ) = 0;
	virtual HRESULT SetCurrentPos( unsigned int & currentPos ) = 0;

	/**
	* Retrieves the state of the CurrentPos parameter.
	* @param oInfo
	*	Address of an object CATSettingInfo.
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
	virtual HRESULT GetCurrentPosInfo( CATSettingInfo * oInfo ) = 0;
	
	/** 
	* Locks or unlocks the CurrentPos parameter.
	* <br><b>Role</b>: Locks or unlocks the CurrentPos parameter if the
	* operation is allowed in the current administrated environment. In user mode 
	* this method will always return E_FAIL.
	* @param iLocked
	*	the locking operation to be performed
	*	<b>Legal values</b>:
	*	<br><tt>1 :</tt>   to lock the parameter.
	* 	<br><tt>0:</tt>   to unlock the parameter.
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
	virtual HRESULT SetCurrentPosLock( unsigned char iLocked ) = 0;


	/**************************
	* First Activity Position *
	**************************/
	virtual HRESULT GetFirstAct( unsigned int & firstAct ) = 0;
	virtual HRESULT SetFirstAct( unsigned int & firstAct ) = 0;

	/**
	* Retrieves the state of the FirstAct parameter.
	* @param oInfo
	*	Address of an object CATSettingInfo.
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
	virtual HRESULT GetFirstActInfo( CATSettingInfo * oInfo ) = 0;
	
	/** 
	* Locks or unlocks the FirstAct parameter.
	* <br><b>Role</b>: Locks or unlocks the FirstAct parameter if the
	* operation is allowed in the current administrated environment. In user mode 
	* this method will always return E_FAIL.
	* @param iLocked
	*	the locking operation to be performed
	*	<b>Legal values</b>:
	*	<br><tt>1 :</tt>   to lock the parameter.
	* 	<br><tt>0:</tt>   to unlock the parameter.
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
	virtual HRESULT SetFirstActLock( unsigned char iLocked ) = 0;


	/*************************
	* Last Activity Position *
	*************************/
	virtual HRESULT GetLastAct( unsigned int & lastAct ) = 0;
	virtual HRESULT SetLastAct( unsigned int & lastAct ) = 0;

	/**
	* Retrieves the state of the LastAct parameter.
	* @param oInfo
	*	Address of an object CATSettingInfo.
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
	virtual HRESULT GetLastActInfo( CATSettingInfo * oInfo ) = 0;
	
	/** 
	* Locks or unlocks the LastAct parameter.
	* <br><b>Role</b>: Locks or unlocks the LastAct parameter if the
	* operation is allowed in the current administrated environment. In user mode 
	* this method will always return E_FAIL.
	* @param iLocked
	*	the locking operation to be performed
	*	<b>Legal values</b>:
	*	<br><tt>1 :</tt>   to lock the parameter.
	* 	<br><tt>0:</tt>   to unlock the parameter.
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
	virtual HRESULT SetLastActLock( unsigned char iLocked ) = 0;

	/***********************
	* Automatized CCP Task *
	***********************/
	virtual HRESULT GetAutoCCP( unsigned int & ioAutoCCP ) = 0;
	virtual HRESULT SetAutoCCP( unsigned int & iAutoCCP ) = 0;

	virtual HRESULT GetAutoCCPInfo( CATSettingInfo * oInfo) = 0;
	virtual HRESULT SetAutoCCPLock( unsigned char iLocked ) = 0;
};
//------------------------------------------------------------------

CATDeclareHandler( DNBIHtsCCPSettingAtt, CATBaseUnknown );

#endif
