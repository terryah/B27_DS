/**
 * @CAA2Level L0
 * @CAA2Usage U3
 */
#ifndef DNBIClimbStair_H
#define DNBIClimbStair_H
//==============================================================================
/**
 * @fullreview JAT lfrancis 03:04:03
 */
//==============================================================================
// COPYRIGHT Dassault Systemes 2003
//==============================================================================
//
// DNBIClimbStair.h - interface declaration
//
//==============================================================================
//  Abstract:
//  ---------
//
//      
//==============================================================================
//  Usage:
//  ------
//
//
//==============================================================================
//  Inheritance:
//  ------------
//
//
//==============================================================================
// HISTORY:
//
//    Author  Date(yr/mm/dd)  Purpose
//    ------  --------------  -------
//    LFS     2003/03/01      Creation
//    LFS     2003/06/19      Addition of S(G)etFirstSteppingLeg
//    LFS     2003/06/25      Data Model for Stair
//    SEO     2008/01/31      CAA migration
//
//==============================================================================
//#include "DNBHumanSimItfCPP.h"
#include "DNBHumanSimInterfaces.h"
#include "DNBIWorkerActivity.h"

class CATMathLine;

extern ExportedByDNBHumanSimInterfaces IID IID_DNBIClimbStair ;

/**
* Interface representing ClimbStair activity
*
*/

class ExportedByDNBHumanSimInterfaces DNBIClimbStair: public DNBIWorkerActivity
{
	CATDeclareInterface;	
	// No constructors or destructors on this pure virtual base class
	// --------------------------------------------------------------
public:
  /**
   * Sets and Gets the number of steps to be climbed in ClimbStair activity
   * <br><b>Role</b>: Sets and Gets the number of steps to be climbed in ClimbStair activity
   * @param iNoOfStep
   * number of steps to be climbed
   * @return
   *	<b>Legal values</b>:
   *	<br><tt>S_OK :</tt>   on Success
   * 	<br><tt>E_FAIL:</tt>  on failure
   */
   virtual HRESULT SetNoOfSteps(int iNoOfStep)=0;
   virtual HRESULT GetNoOfSteps(int* piNoOfStep)=0;

   /**
   * Sets and Gets the arm swing during ClimbStair activity
   * <br><b>Role</b>: Sets and Gets the arm swing during ClimbStair activity 
   * @param iInSwing
   * 0 = SWING_BOTHARMS
   * 1 = SWING_LEFTARM
   * 2 = SWING_RIGHTARM
   * 3 = SWING_CURRENT
   * @return
   *	<b>Legal values</b>:
   *	<br><tt>S_OK :</tt>   on Success
   * 	<br><tt>E_FAIL:</tt>  on failure
   */
   virtual HRESULT SetSwing(int iInSwing)=0;
   virtual HRESULT GetSwing(int* piInSwing)=0;

   /**
   * Sets and Gets the climb type (climb up or climb down)in ClimbStair activity
   * <br><b>Role</b>: Sets and Gets the climb type(climb up or climb down) in ClimbStair activity
   * @param iClimbType
   * 0 = Climb up
   * 1 = Climb down
   * @return
   *	<b>Legal values</b>:
   *	<br><tt>S_OK :</tt>   on Success
   * 	<br><tt>E_FAIL:</tt>  on failure
   */
   virtual HRESULT SetClimbType(int iClimbType)=0;
   virtual HRESULT GetClimbType(int* piClimbType)=0;

   /**
   * Sets and Gets the first stepping leg in ClimbStair activity
   * <br><b>Role</b>: Sets and Gets the first stepping leg in ClimbStair activity
   * @param iFirstSteppingLeg
   * 0 = Right leg
   * 1 = Left leg
   * @return
   *	<b>Legal values</b>:
   *	<br><tt>S_OK :</tt>   on Success
   * 	<br><tt>E_FAIL:</tt>  on failure
   */
   virtual HRESULT SetFirstSteppingLeg(int iFirstSteppingLeg)=0;
   virtual HRESULT GetFirstSteppingLeg(int *piFirstSteppingLeg)=0;

   /**
   * Sets and Gets the depth of step (based on stair geometry) in ClimbStair activity
   * <br><b>Role</b>: Sets and Gets the depth of step (based on stair geometry) in ClimbStair activity
   * @param dStepDepth
   * @return
   *	<b>Legal values</b>:
   *	<br><tt>S_OK :</tt>   on Success
   * 	<br><tt>E_FAIL:</tt>  on failure
   */
   virtual HRESULT SetStepDepth(const double dStepDepth)=0;
   virtual HRESULT GetStepDepth(double *pdStepDepth)=0;

   /**
   * Sets and Gets the height of step (based on stair geometry) in ClimbStair activity
   * <br><b>Role</b>: Sets and Gets the height of step (based on stair geometry) in ClimbStair activity
   * @param dStepHeight
   * @return
   *	<b>Legal values</b>:
   *	<br><tt>S_OK :</tt>   on Success
   * 	<br><tt>E_FAIL:</tt>  on failure
   */
   virtual HRESULT SetStepHeight(const double dStepHeight)=0;
   virtual HRESULT GetStepHeight(double*pdStepHeight)=0;

   /**
   * Sets and Gets the step edge based on stair geometry) in ClimbStair activity
   * <br><b>Role</b>: Sets and Gets the height of step (based on stair geometry in ClimbStair activity
   * @param lineInput
   * @return
   *	<b>Legal values</b>:
   *	<br><tt>S_OK :</tt>   on Success
   * 	<br><tt>E_FAIL:</tt>  on failure
   */
   virtual HRESULT SetFirstEdge(const CATMathLine& lineInput)=0;
   virtual HRESULT GetFirstEdge(CATMathLine& lineInOut)=0;

   /**
   * Sets and Gets the first step depth for ClimbStair activity
   * <br><b>Role</b>: Sets and Gets the first step depth for ClimbStair activity
   * @param dStepDepth
   * @return
   *	<b>Legal values</b>:
   *	<br><tt>S_OK :</tt>   on Success
   * 	<br><tt>E_FAIL:</tt>  on failure
   */
   virtual HRESULT SetFirstStepDepth(const double dStepDepth)=0;
   virtual HRESULT GetFirstStepDepth(double *pdStepDepth)=0;

   /**
   * Sets and Gets the first step height for ClimbStair activity
   * <br><b>Role</b>: Sets and Gets the height of first step 
   * @param dStepHeight
   * @return
   *	<b>Legal values</b>:
   *	<br><tt>S_OK :</tt>   on Success
   * 	<br><tt>E_FAIL:</tt>  on failure
   */
   virtual HRESULT SetFirstStepHeight(const double dStepHeight)=0;
   virtual HRESULT GetFirstStepHeight(double*pdStepHeight)=0;
};

//------------------------------------------------------------------
CATDeclareHandler( DNBIClimbStair, CATBaseUnknown );

#endif
