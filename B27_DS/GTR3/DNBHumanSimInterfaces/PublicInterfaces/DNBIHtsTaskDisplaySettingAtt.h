/**
 * @CAA2Level L0
 * @CAA2Usage U3
 */
// COPYRIGHT Dassault Systemes 2004
//===================================================================
//
// DNBIHtsTaskDisplaySettingAtt.h
// Define the DNBIHtsTaskDisplaySettingAtt interface
//
//===================================================================
//
// Usage notes:
//
//===================================================================
// HISTORY:
//
//     Author       Date         Purpose
//     ------       ----         -------
//     ATJ          Mar 2004	 Initial implementation
//     SEO     2008/01/31      CAA migration
//===================================================================
#ifndef DNBIHtsTaskDisplaySettingAtt_H
#define DNBIHtsTaskDisplaySettingAtt_H

//#include "DNBHumanSimItfCPP.h"
#include "DNBHumanSimInterfaces.h"
#include "CATBaseUnknown.h"

extern ExportedByDNBHumanSimInterfaces IID IID_DNBIHtsTaskDisplaySettingAtt;

class CATSettingInfo;
class CATUnicodeString;

//------------------------------------------------------------------
class ExportedByDNBHumanSimInterfaces DNBIHtsTaskDisplaySettingAtt: public CATBaseUnknown
{
	CATDeclareInterface;
	
public:
	
	/*****************
	* Walk Line Type *
	*****************/
    virtual HRESULT GetWalkActLineType( unsigned  int & oWalkActLineType ) = 0;
    virtual HRESULT SetWalkActLineType( unsigned  int & iWalkActLineType ) = 0;
	
	/**
	* Retrieves the state of the WalkActLineType parameter.
	* @param oInfo
	*	Address of an object CATSettingInfo.
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
	virtual HRESULT GetWalkActLineTypeInfo( CATSettingInfo * oInfo ) = 0;
	
	/** 
	* Locks or unlocks the WalkActLineType parameter.
	* <br><b>Role</b>: Locks or unlocks the WalkActLineType parameter if the
	* operation is allowed in the current administrated environment. In user mode 
	* this method will always return E_FAIL.
	* @param iLocked
	*	the locking operation to be performed
	*	<b>Legal values</b>:
	*	<br><tt>1 :</tt>   to lock the parameter.
	* 	<br><tt>0:</tt>   to unlock the parameter.
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
	virtual HRESULT SetWalkActLineTypeLock( unsigned char iLocked ) = 0;
	
	
	/*********************
	* AutoWalk Line Type *
	*********************/
    virtual HRESULT GetAutoWalkActLineType( unsigned  int & oAutoWalkActLineType ) = 0;
    virtual HRESULT SetAutoWalkActLineType( unsigned  int & iAutoWalkActLineType ) = 0;
	
	/**
	* Retrieves the state of the AutoWalkActLineType parameter.
	* @param oInfo
	*	Address of an object CATSettingInfo.
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
	virtual HRESULT GetAutoWalkActLineTypeInfo( CATSettingInfo * oInfo ) = 0;
	
	/** 
	* Locks or unlocks the AutoWalkActLineType parameter.
	* <br><b>Role</b>: Locks or unlocks the AutoWalkActLineType parameter if the
	* operation is allowed in the current administrated environment. In user mode 
	* this method will always return E_FAIL.
	* @param iLocked
	*	the locking operation to be performed
	*	<b>Legal values</b>:
	*	<br><tt>1 :</tt>   to lock the parameter.
	* 	<br><tt>0:</tt>   to unlock the parameter.
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
	virtual HRESULT SetAutoWalkActLineTypeLock( unsigned char iLocked ) = 0;
	
	
	/*******************
	* Walk Line Weight *
	*******************/
    virtual HRESULT GetWalkActLineWeight( unsigned  int & oWalkActLineWeight ) = 0;
    virtual HRESULT SetWalkActLineWeight( unsigned  int & iWalkActLineWeight ) = 0;
	
	/**
	* Retrieves the state of the WalkActLineWeight parameter.
	* @param oInfo
	*	Address of an object CATSettingInfo.
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
	virtual HRESULT GetWalkActLineWeightInfo( CATSettingInfo * oInfo ) = 0;
	
	/** 
	* Locks or unlocks the WalkActLineWeight parameter.
	* <br><b>Role</b>: Locks or unlocks the WalkActLineWeight parameter if the
	* operation is allowed in the current administrated environment. In user mode 
	* this method will always return E_FAIL.
	* @param iLocked
	*	the locking operation to be performed
	*	<b>Legal values</b>:
	*	<br><tt>1 :</tt>   to lock the parameter.
	* 	<br><tt>0:</tt>   to unlock the parameter.
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
	virtual HRESULT SetWalkActLineWeightLock( unsigned char iLocked ) = 0;
	
	
	/***********************
	* AutoWalk Line Weight *
	***********************/
    virtual HRESULT GetAutoWalkActLineWeight( unsigned  int & oAutoWalkActLineWeight ) = 0;
    virtual HRESULT SetAutoWalkActLineWeight( unsigned  int & iAutoWalkActLineWeight ) = 0;
	
	/**
	* Retrieves the state of the AutoWalkActLineWeight parameter.
	* @param oInfo
	*	Address of an object CATSettingInfo.
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
	virtual HRESULT GetAutoWalkActLineWeightInfo( CATSettingInfo * oInfo ) = 0;
	
	/** 
	* Locks or unlocks the AutoWalkActLineWeight parameter.
	* <br><b>Role</b>: Locks or unlocks the AutoWalkActLineWeight parameter if the
	* operation is allowed in the current administrated environment. In user mode 
	* this method will always return E_FAIL.
	* @param iLocked
	*	the locking operation to be performed
	*	<b>Legal values</b>:
	*	<br><tt>1 :</tt>   to lock the parameter.
	* 	<br><tt>0:</tt>   to unlock the parameter.
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
	virtual HRESULT SetAutoWalkActLineWeightLock( unsigned char iLocked ) = 0;
	
	
	/*************
	* MTP Symbol *
	*************/
    virtual HRESULT GetMTPActSymbol( unsigned  int & oMTPActSymbol ) = 0;
    virtual HRESULT SetMTPActSymbol( unsigned  int & iMTPActSymbol ) = 0;
	
	/**
	* Retrieves the state of the MTPActSymbol parameter.
	* @param oInfo
	*	Address of an object CATSettingInfo.
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
	virtual HRESULT GetMTPActSymbolInfo( CATSettingInfo * oInfo ) = 0;
	
	/** 
	* Locks or unlocks the MTPActSymbol parameter.
	* <br><b>Role</b>: Locks or unlocks the MTPActSymbol parameter if the
	* operation is allowed in the current administrated environment. In user mode 
	* this method will always return E_FAIL.
	* @param iLocked
	*	the locking operation to be performed
	*	<b>Legal values</b>:
	*	<br><tt>1 :</tt>   to lock the parameter.
	* 	<br><tt>0:</tt>   to unlock the parameter.
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
	virtual HRESULT SetMTPActSymbolLock( unsigned char iLocked ) = 0;

    virtual HRESULT GetMTPActColor( unsigned  int & oMTPActColor ) = 0;
    virtual HRESULT GetMTPActColor( unsigned int & poRed, 
							        unsigned int & poGreen, 
                                    unsigned int & poBlue ) = 0;
    virtual HRESULT SetMTPActColor( unsigned  int & iMTPActColor ) = 0;
	
	/**
	* Retrieves the state of the MTPActColor parameter.
	* @param oInfo
	*	Address of an object CATSettingInfo.
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
	virtual HRESULT GetMTPActColorInfo( CATSettingInfo * oInfo ) = 0;
	
	/** 
	* Locks or unlocks the MTPActColor parameter.
	* <br><b>Role</b>: Locks or unlocks the MTPActColor parameter if the
	* operation is allowed in the current administrated environment. In user mode 
	* this method will always return E_FAIL.
	* @param iLocked
	*	the locking operation to be performed
	*	<b>Legal values</b>:
	*	<br><tt>1 :</tt>   to lock the parameter.
	* 	<br><tt>0:</tt>   to unlock the parameter.
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
	virtual HRESULT SetMTPActColorLock( unsigned char iLocked ) = 0;


    virtual HRESULT GetWalkActColor( unsigned  int & oWalkActColor ) = 0;
    virtual HRESULT GetWalkActColor(  unsigned int & poRed, 
							  unsigned int & poGreen, 
							  unsigned int & poBlue ) = 0;
    virtual HRESULT SetWalkActColor( unsigned  int & iWalkActColor ) = 0;
	
	/**
	* Retrieves the state of the WalkActColor parameter.
	* @param oInfo
	*	Address of an object CATSettingInfo.
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
	virtual HRESULT GetWalkActColorInfo( CATSettingInfo * oInfo ) = 0;
	
	/** 
	* Locks or unlocks the WalkActColor parameter.
	* <br><b>Role</b>: Locks or unlocks the WalkActColor parameter if the
	* operation is allowed in the current administrated environment. In user mode 
	* this method will always return E_FAIL.
	* @param iLocked
	*	the locking operation to be performed
	*	<b>Legal values</b>:
	*	<br><tt>1 :</tt>   to lock the parameter.
	* 	<br><tt>0:</tt>   to unlock the parameter.
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
	virtual HRESULT SetWalkActColorLock( unsigned char iLocked ) = 0;


    virtual HRESULT GetAutoWalkActColor( unsigned  int & oAutoWalkActColor ) = 0;
    virtual HRESULT GetAutoWalkActColor( unsigned int & poRed, 
								         unsigned int & poGreen, 
								         unsigned int & poBlue) = 0;
    virtual HRESULT SetAutoWalkActColor( unsigned  int & iAutoWalkActColor ) = 0;
	
	/**
	* Retrieves the state of the AutoWalkActColor parameter.
	* @param oInfo
	*	Address of an object CATSettingInfo.
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
	virtual HRESULT GetAutoWalkActColorInfo( CATSettingInfo * oInfo ) = 0;
	
	/** 
	* Locks or unlocks the AutoWalkActColor parameter.
	* <br><b>Role</b>: Locks or unlocks the AutoWalkActColor parameter if the
	* operation is allowed in the current administrated environment. In user mode 
	* this method will always return E_FAIL.
	* @param iLocked
	*	the locking operation to be performed
	*	<b>Legal values</b>:
	*	<br><tt>1 :</tt>   to lock the parameter.
	* 	<br><tt>0:</tt>   to unlock the parameter.
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
	virtual HRESULT SetAutoWalkActColorLock( unsigned char iLocked ) = 0;
};
//------------------------------------------------------------------

CATDeclareHandler( DNBIHtsTaskDisplaySettingAtt, CATBaseUnknown );

#endif
