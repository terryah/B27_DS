/**
 * @CAA2Level L0
 * @CAA2Usage U3
 */
// COPYRIGHT Dassault Systemes 2004
//===================================================================
//
// DNBIHtsGeneralSettingAtt.h
// Define the DNBIHtsGeneralSettingAtt interface
//
//===================================================================
//
// Usage notes:
//
//===================================================================
// HISTORY:
//
//     Author       Date         Purpose
//     ------       ----         -------
//     ATJ          Mar 2004	 Initial implementation
//     SEO          Jan 2007	 Support for sync between speed & time for MTP
//     SEO     2008/01/31      CAA migration
//===================================================================
#ifndef DNBIHtsGeneralSettingAtt_H
#define DNBIHtsGeneralSettingAtt_H

//#include "DNBHumanSimItfCPP.h"
#include "DNBHumanSimInterfaces.h"
#include "CATBaseUnknown.h"

extern ExportedByDNBHumanSimInterfaces IID IID_DNBIHtsGeneralSettingAtt;

class CATSettingInfo;
class CATUnicodeString;

//------------------------------------------------------------------
class ExportedByDNBHumanSimInterfaces DNBIHtsGeneralSettingAtt: public CATBaseUnknown
{
	CATDeclareInterface;
	
public:

	/**********************************
	* Honor Constraints in Simulation *
	**********************************/
	virtual HRESULT GetConstraintsSimul( unsigned int & constrSimul ) = 0;
	virtual HRESULT SetConstraintsSimul( unsigned int & constrSimul ) = 0;

	/**
	* Retrieves the state of the ConstraintsSimul parameter.
	* @param oInfo
	*	Address of an object CATSettingInfo.
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
	virtual HRESULT GetConstraintsSimulInfo( CATSettingInfo * oInfo ) = 0;
	
	/** 
	* Locks or unlocks the ConstraintsSimul parameter.
	* <br><b>Role</b>: Locks or unlocks the ConstraintsSimul parameter if the
	* operation is allowed in the current administrated environment. In user mode 
	* this method will always return E_FAIL.
	* @param iLocked
	*	the locking operation to be performed
	*	<b>Legal values</b>:
	*	<br><tt>1 :</tt>   to lock the parameter.
	* 	<br><tt>0:</tt>   to unlock the parameter.
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
	virtual HRESULT SetConstraintsSimulLock( unsigned char iLocked ) = 0;


	/********************************
	* Update Analysis in Simulation *
	********************************/
	virtual HRESULT GetUpdateAnalysisInSimulation( unsigned int & updateAnalysis ) = 0;
	virtual HRESULT SetUpdateAnalysisInSimulation( unsigned int & updateAnalysis ) = 0;

	/**
	* Retrieves the state of the UpdateAnalysisInSimulation parameter.
	* @param oInfo
	*	Address of an object CATSettingInfo.
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
	virtual HRESULT GetUpdateAnalysisInSimulationInfo( CATSettingInfo * oInfo ) = 0;
	
	/** 
	* Locks or unlocks the UpdateAnalysisInSimulation parameter.
	* <br><b>Role</b>: Locks or unlocks the UpdateAnalysisInSimulation parameter if the
	* operation is allowed in the current administrated environment. In user mode 
	* this method will always return E_FAIL.
	* @param iLocked
	*	the locking operation to be performed
	*	<b>Legal values</b>:
	*	<br><tt>1 :</tt>   to lock the parameter.
	* 	<br><tt>0:</tt>   to unlock the parameter.
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
	virtual HRESULT SetUpdateAnalysisInSimulationLock( unsigned char iLocked ) = 0;


	/********************************
	* UserWalkSpeedStatus in Simulation *
	********************************/
	virtual HRESULT GetUserWalkSpeedStatus( unsigned int & UserWalkSpeedStatus ) = 0;
	virtual HRESULT SetUserWalkSpeedStatus( unsigned int & UserWalkSpeedStatus ) = 0;

	/**
	* Retrieves the state of the UserWalkSpeedStatus parameter.
	* @param oInfo
	*	Address of an object CATSettingInfo.
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
	virtual HRESULT GetUserWalkSpeedStatusInfo( CATSettingInfo * oInfo ) = 0;
	
	/** 
	* Locks or unlocks the UserWalkSpeedStatus parameter.
	* <br><b>Role</b>: Locks or unlocks the UserWalkSpeedStatus parameter if the
	* operation is allowed in the current administrated environment. In user mode 
	* this method will always return E_FAIL.
	* @param iLocked
	*	the locking operation to be performed
	*	<b>Legal values</b>:
	*	<br><tt>1 :</tt>   to lock the parameter.
	* 	<br><tt>0:</tt>   to unlock the parameter.
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
	virtual HRESULT SetUserWalkSpeedStatusLock( unsigned char iLocked ) = 0;

	/************************
	* Walk Forward Distance *
	************************/
	virtual HRESULT GetWalkForwardDistance( double & walkFwdDist ) = 0;
	virtual HRESULT SetWalkForwardDistance( double & walkFwdDist ) = 0;

	/**
	* Retrieves the state of the WalkForwardDistance parameter.
	* @param oInfo
	*	Address of an object CATSettingInfo.
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
	virtual HRESULT GetWalkForwardDistanceInfo( CATSettingInfo * oInfo ) = 0;
	
	/** 
	* Locks or unlocks the WalkForwardDistance parameter.
	* <br><b>Role</b>: Locks or unlocks the WalkForwardDistance parameter if the
	* operation is allowed in the current administrated environment. In user mode 
	* this method will always return E_FAIL.
	* @param iLocked
	*	the locking operation to be performed
	*	<b>Legal values</b>:
	*	<br><tt>1 :</tt>   to lock the parameter.
	* 	<br><tt>0:</tt>   to unlock the parameter.
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
	virtual HRESULT SetWalkForwardDistanceLock( unsigned char iLocked ) = 0;


	/*********************
	* Walk Forward Angle *
	*********************/
	virtual HRESULT GetWalkForwardAngle( double & walkFwdAngle ) = 0;
	virtual HRESULT SetWalkForwardAngle( double & walkFwdAngle ) = 0;

	/**
	* Retrieves the state of the WalkForwardAngle parameter.
	* @param oInfo
	*	Address of an object CATSettingInfo.
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
	virtual HRESULT GetWalkForwardAngleInfo( CATSettingInfo * oInfo ) = 0;
	
	/** 
	* Locks or unlocks the WalkForwardAngle parameter.
	* <br><b>Role</b>: Locks or unlocks the WalkForwardAngle parameter if the
	* operation is allowed in the current administrated environment. In user mode 
	* this method will always return E_FAIL.
	* @param iLocked
	*	the locking operation to be performed
	*	<b>Legal values</b>:
	*	<br><tt>1 :</tt>   to lock the parameter.
	* 	<br><tt>0:</tt>   to unlock the parameter.
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
	virtual HRESULT SetWalkForwardAngleLock( unsigned char iLocked ) = 0;


	/******************
	* Side Step Angle *
	******************/
	virtual HRESULT GetSideStepAngle( double & sideStepAngle ) = 0;
	virtual HRESULT SetSideStepAngle( double & sideStepAngle ) = 0;

	/**
	* Retrieves the state of the SideStepAngle parameter.
	* @param oInfo
	*	Address of an object CATSettingInfo.
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
	virtual HRESULT GetSideStepAngleInfo( CATSettingInfo * oInfo ) = 0;
	
	/** 
	* Locks or unlocks the SideStepAngle parameter.
	* <br><b>Role</b>: Locks or unlocks the SideStepAngle parameter if the
	* operation is allowed in the current administrated environment. In user mode 
	* this method will always return E_FAIL.
	* @param iLocked
	*	the locking operation to be performed
	*	<b>Legal values</b>:
	*	<br><tt>1 :</tt>   to lock the parameter.
	* 	<br><tt>0:</tt>   to unlock the parameter.
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
	virtual HRESULT SetSideStepAngleLock( unsigned char iLocked ) = 0;

	/******************
	* Default Walk Speed *
	******************/
	virtual HRESULT GetDefaultWalkSpeed( double & oWalkSpeed ) = 0;
	virtual HRESULT SetDefaultWalkSpeed( double & iWalkSpeed ) = 0;

	/**
	* Retrieves the state of the DefaultWalkSpeed parameter.
	* @param oInfo
	*	Address of an object CATSettingInfo.
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
	virtual HRESULT GetDefaultWalkSpeedInfo( CATSettingInfo * oInfo ) = 0;
	
	/** 
	* Locks or unlocks the DefaultWalkSpeed parameter.
	* <br><b>Role</b>: Locks or unlocks the DefaultWalkSpeed parameter if the
	* operation is allowed in the current administrated environment. In user mode 
	* this method will always return E_FAIL.
	* @param iLocked
	*	the locking operation to be performed
	*	<b>Legal values</b>:
	*	<br><tt>1 :</tt>   to lock the parameter.
	* 	<br><tt>0:</tt>   to unlock the parameter.
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
	virtual HRESULT SetDefaultWalkSpeedLock( unsigned char iLocked ) = 0;

	// SEO - R18MHT011 (sync between speed and time for MTP)
	/********************************
	* Sync time & speed for MTP during simulation
	********************************/
	virtual HRESULT GetSyncTimeAndSpeedInSimulation( unsigned int & syncTimeAndSpeed ) = 0;
	virtual HRESULT SetSyncTimeAndSpeedInSimulation( unsigned int & syncTimeAndSpeed ) = 0;

	/**
	* Retrieves the state of the SyncTimeAndSpeed parameter.
	* @param oInfo
	*	Address of an object CATSettingInfo.
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
	virtual HRESULT GetSyncTimeAndSpeedInSimulationInfo( CATSettingInfo * oInfo ) = 0;
	
	/** 
	* Locks or unlocks the SyncTimeAndSpeed parameter.
	* <br><b>Role</b>: Locks or unlocks the SyncTimeAndSpeed parameter if the
	* operation is allowed in the current administrated environment. In user mode 
	* this method will always return E_FAIL.
	* @param iLocked
	*	the locking operation to be performed
	*	<b>Legal values</b>:
	*	<br><tt>1 :</tt>   to lock the parameter.
	* 	<br><tt>0:</tt>   to unlock the parameter.
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
	virtual HRESULT SetSyncTimeAndSpeedInSimulationLock( unsigned char iLocked ) = 0;

	//Collision free walk attributes

	virtual HRESULT GetCollisionSearchIntensity( unsigned int & iSearchIntensity ) = 0;
	virtual HRESULT SetCollisionSearchIntensity( unsigned int & iSearchIntensity ) = 0;
	virtual HRESULT GetCollisionSearchIntensityInfo( CATSettingInfo * oInfo ) = 0;
	virtual HRESULT SetCollisionSearchIntensityLock( unsigned char iLocked ) = 0;

	
	virtual HRESULT GetCollWalkClearance( double & iClearance ) = 0;
	virtual HRESULT SetCollWalkClearance( double & iClearance ) = 0;
	virtual HRESULT GetCollWalkClearanceInfo( CATSettingInfo * oInfo ) = 0;
	virtual HRESULT SetCollWalkClearanceLock( unsigned char iLocked ) = 0;
};
//------------------------------------------------------------------

CATDeclareHandler( DNBIHtsGeneralSettingAtt, CATBaseUnknown );

#endif
