/**
 * @CAA2Level L0
 * @CAA2Usage U3
 */
#ifndef DNBIPlace_H
#define DNBIPlace_H
//==============================================================================
// COPYRIGHT Dassault Systemes 2001
//==============================================================================
//
// DNBIPlace.h
//
//==============================================================================
//  Usage:
//  ------
//
//
//==============================================================================
// HISTORY:
//
//    Author  Date(yr/mm/dd)  Purpose
//    ------  --------------  -------
//    VGL     2001/03/01      Creation
//     SEO     2008/01/31      CAA migration
//==============================================================================

//#include "DNBHumanSimItfCPP.h"
#include "DNBHumanSimInterfaces.h"
#include "DNBIWorkerActivity.h"
#include "CATMathTransformation.h"
#include "CATLISTV_CATBaseUnknown.h"


extern ExportedByDNBHumanSimInterfaces IID IID_DNBIPlace ;

class CATListPtrCATMathTransformation;
class DNBIPick_var;

/**
* Interface representing Place activity
*/

//------------------------------------------------------------------
class ExportedByDNBHumanSimInterfaces DNBIPlace: public DNBIWorkerActivity
{
   CATDeclareInterface;

public:

  /**
   * Sets and Gets the Place offset to be maintained between the end-effector segment and the objects to be placed
   * <br><b>Role</b>: Sets and Gets the Place offset to be maintained between the end-effector segment and the objects to be placed
   * @param offsetTransList
   * List of offset transformation 
   * @return
   *	<b>Legal values</b>:
   *	<br><tt>S_OK :</tt>   on Success
   * 	<br><tt>E_FAIL:</tt>  on failure
   */
   virtual HRESULT    SetOffset(CATListPtrCATMathTransformation& offsetTransList) = 0;
   virtual HRESULT GetOffset(CATListPtrCATMathTransformation& offsetTransList) = 0;

  /**
   * Gets if there is any place offset defined between the end-effector segment and the object to be placed
   * <br><b>Role</b>: Gets if there is any place offset defined between the end-effector segment and the object to be placed
   * @param HasPlaceOffset
   * TRUE = place offset is defined
   * FALSE = place offset is not defined
   * @return
   *	<b>Legal values</b>:
   *	<br><tt>S_OK :</tt>   on Success
   * 	<br><tt>E_FAIL:</tt>  on failure
   */
   virtual HRESULT GetHasPlaceOffset(boolean  &HasPlaceOffset)= 0;

   /**
   * Gets and Sets place offset defined between the end-effector segment and the object to be placed
   * <br><b>Role</b>: Gets and Sets place offset defined between the end-effector segment and the object to be placed
   * @param offset
   * CATMathTransformation defining the place offset to be set/get
   * @return
   *	<b>Legal values</b>:
   *	<br><tt>S_OK :</tt>   on Success
   * 	<br><tt>E_FAIL:</tt>  on failure
   */
   virtual void    SetOffset(CATMathTransformation& offset) = 0;
   virtual boolean GetOffset(CATMathTransformation& offset) = 0;

   /**
   * Gets and Sets place offset defined between the end-effector segment and the specified object to be placed
   * <br><b>Role</b>: Gets and Sets place offset defined between the end-effector segment and the specified object to be placed
   * @param iPlacedObj
   * object to be placed
   * iOffsetTrans
   * CATMathTransformation defining the place offset to be set/get
   * @return
   *	<b>Legal values</b>:
   *	<br><tt>S_OK :</tt>   on Success
   * 	<br><tt>E_FAIL:</tt>  on failure
   */
   virtual HRESULT SetOffset(CATBaseUnknown_var& iPlacedObj, CATMathTransformation& iOffsetTrans) = 0;
   virtual HRESULT GetOffset(CATBaseUnknown_var& iPlacedObj, CATMathTransformation& oOffsetTrans) = 0;

 /**
   * Gets and Sets pick activity corresponding to the place activity.
   * <br><b>Role</b>: Gets and Sets pick activity corresponding to the place activity. Pick/Place activities cannot exisit individually.
   * For every pick activity, there has to be a corresponding place activity and vice versa. These two activities
   * maintain a link between them. Deleting the pick activity deletes the corresponding place activity and vice versa.
   * @param pickAct
   * Pick activity to be associated with the place activity
   * @return
   *	<b>Legal values</b>:
   *	<br><tt>S_OK :</tt>   on Success
   * 	<br><tt>E_FAIL:</tt>  on failure
   */
   virtual HRESULT SetPickAct( DNBIPick_var pickAct ) = 0;
   virtual HRESULT GetPickAct(DNBIPick_var& pickAct,int iIndex) = 0;

   /**
   * Gets and Sets the list of picked objects
   * <br><b>Role</b>: Gets and Sets the list of picked objects
   * @param oPlacedObj
   * List of placed objects
   * @return
   *	<b>Legal values</b>:
   *	<br><tt>S_OK :</tt>   on Success
   * 	<br><tt>E_FAIL:</tt>  on failure
   */
   virtual HRESULT GetListOfPlacedObj(CATListValCATBaseUnknown_var &oPlacedObj) = 0;
   virtual HRESULT SetListOfPlacedObj(CATListValCATBaseUnknown_var &oPlacedObj) = 0;

   /**
   * Gets the pick activity that places the specified object
   * <br><b>Role</b>: Gets the pick activity that places the specified object
   * @param pickAct
   * pick activity for the specified object that will be placed
   * @placedObject
   * object to be placed
   * @return
   *	<b>Legal values</b>:
   *	<br><tt>S_OK :</tt>   on Success
   * 	<br><tt>E_FAIL:</tt>  on failure
   */
   virtual HRESULT GetPickActForPlacedObject(DNBIPick_var& pickAct,CATBaseUnknown_var& placedobject) = 0;

   /**
   * Gets the end-effector (placing hand) that places the object specified
   * <br><b>Role</b>: Gets the end-effector (placing hand) that places the object specified
   * @param placedProduct
   * Product that is placed
   * @pickingHand
   * Hand that places the specified product (right hand/left hand or both hands)
   * @return
   *	<b>Legal values</b>:
   *	<br><tt>S_OK :</tt>   on Success
   * 	<br><tt>E_FAIL:</tt>  on failure
   */
   virtual HRESULT GetPlacingHand(CATBaseUnknown_var& placedProduct, CATUnicodeString& pickingHand) = 0;

   /**
   * Updates the old place activity to the new model
   * <br><b>Role</b>: Updates the old place activity to the new model
   * @return
   *	<b>Legal values</b>:
   *	<br><tt>S_OK :</tt>   on Success
   * 	<br><tt>E_FAIL:</tt>  on failure
   */
   virtual HRESULT UpdateOldPlaceAct() = 0;

   /**
   * Removes pick place links
   * <br><b>Role</b>: Removes pick place links
   * @return
   *	<b>Legal values</b>:
   *	<br><tt>S_OK :</tt>   on Success
   * 	<br><tt>E_FAIL:</tt>  on failure
   */
   virtual HRESULT RemovePickPlaceLinks() = 0;

   /**
   * Removes the pick activity from the list
   * <br><b>Role</b>: Removes the pick activity from the list
   * @param pickAct
   * PickActivity
   * @return
   *	<b>Legal values</b>:
   *	<br><tt>S_OK :</tt>   on Success
   * 	<br><tt>E_FAIL:</tt>  on failure
   */
   virtual HRESULT RemovePickActFromLinkList( DNBIPick_var pickAct ) = 0;

   /**
   * Removes the object specified from the place list.
   * <br><b>Role</b>:  Removes the object specified from the place list; edits the place activity
   * @param placedobject
   * Product that is to be placed is removed from the list of placed objects.
   * @return
   *	<b>Legal values</b>:
   *	<br><tt>S_OK :</tt>   on Success
   * 	<br><tt>E_FAIL:</tt>  on failure
   */
   virtual HRESULT RemoveObjectFromPlaceList(CATBaseUnknown_var& placedobject) = 0;

private:


};

//------------------------------------------------------------------
CATDeclareHandler( DNBIPlace, CATBaseUnknown );

#endif
