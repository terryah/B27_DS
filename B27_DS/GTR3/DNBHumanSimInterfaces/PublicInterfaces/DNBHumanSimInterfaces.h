// COPYRIGHT DASSAULT SYSTEMES 2001
// COPYRIGHT DASSAULT SYSTEMES 2000
// COPYRIGHT SAFEWORK INC. 2000
//
//    This Header file is included by all the Module.h header files in 
//    PublicInterfaces
//

#ifndef DNBHUMANSIMINTERFACES_H

#define DNBHUMANSIMINTERFACES_H DNBHumanSimInterfaces

#ifdef _WINDOWS_SOURCE
#if defined(__DNBHumanSimInterfaces)
#define ExportedByDNBHumanSimInterfaces __declspec(dllexport)
#else
#define ExportedByDNBHumanSimInterfaces __declspec(dllimport)
#endif
#else
#define ExportedByDNBHumanSimInterfaces
#endif

#endif /* DNBHUMANSIMINTERFACES_H */
