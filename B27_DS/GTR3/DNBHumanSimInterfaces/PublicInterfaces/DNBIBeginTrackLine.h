/**
 * @CAA2Level L0
 * @CAA2Usage U3
 */
// COPYRIGHT Dassault Systemes 2003
//===================================================================
//
// DNBIBeginTrackLine.h
// Define the DNBIBeginTrackLine interface
//
//===================================================================
//
// Usage notes:
// 
//
//===================================================================
//
//  Feb 2003  Creation: Code generated by the CAA wizard  atandon
//    SEO     2008/01/31      CAA migration
//===================================================================
#ifndef DNBIBeginTrackLine_H
#define DNBIBeginTrackLine_H

//#include "DNBHumanSimItfCPP.h"
#include "DNBHumanSimInterfaces.h"
#include "DNBIWorkerActivity.h"
//#include "CATMathTransformation.h"

class CATIMovable;
class DNBIEndTrackLine_var;

extern ExportedByDNBHumanSimInterfaces IID IID_DNBIBeginTrackLine ;

/**
* Interface representing Begin Track Line activity
*
*/

//------------------------------------------------------------------
class ExportedByDNBHumanSimInterfaces DNBIBeginTrackLine: public DNBIWorkerActivity
{
	CATDeclareInterface;
	
public:
	
  /**
   * Sets the object to be tracked by the manikin resource in track line activity
   * <br><b>Role</b>: Sets the object to be tracked by manikin product during track line activity
   * @param trackObj
   * object to be tracked by the manikin in the track line activity
   * @return
   *	<b>Legal values</b>:
   *	<br><tt>S_OK :</tt>   on Success
   * 	<br><tt>E_FAIL:</tt>  on failure
   */
	virtual void					SetObject( CATIMovable * trackObj ) = 0; 

	/**
   * Gets the tracked object  
   * <br><b>Role</b>: Gets the tracked objecthe track line activity
   * @return
   * Object tracked by the manikin
   */
	virtual CATIMovable * 			GetObject() = 0;

  /**
   * Sets the walk type on the manikin while executing the track line activity
   * <br><b>Role</b>: Sets the walk type on the manikin while executing the track line activity
   * @param walkType
   * walkType = 0 implies WalkForward 
   * walkType = 1 implies WalkBackward
   * walkType = 2 implies SideStep
   * @return
   *	<b>Legal values</b>:
   *	<br><tt>S_OK :</tt>   on Success
   * 	<br><tt>E_FAIL:</tt>  on failure
   */
	virtual void					SetWalkType( int walkType ) = 0;

	/**
   * Gets the walk type on the manikin while executing the track line activity
   * <br><b>Role</b>: Gets the walk type on the manikin while executing the track line activity
   * @return
   * walkType = 0 implies WalkForward 
   * walkType = 1 implies WalkBackward
   * walkType = 2 implies SideStep
   */
	virtual int						GetWalkType() = 0;

  /**
   * Sets the End Track Line Activity 
   * <br><b>Role</b>: Begin and End track line activities enclose a set of activities.
   * There cannot be a just Begin or End track line activity. Both have to be present
   */
	virtual void					SetETLAct( DNBIEndTrackLine_var etlAct ) = 0;

	/**
   * Gets the End Track Line Activity 
   * <br><b>Role</b>: Begin and End track line activities enclose a set of activities.
   * There cannot be a just Begin or End track line activity. Both have to be present
   * @return
   * EndTrackLineActivity
   */
	virtual DNBIEndTrackLine_var	GetETLAct() = 0;
};
//------------------------------------------------------------------

CATDeclareHandler(DNBIBeginTrackLine, CATBaseUnknown);

#endif
