/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */

#ifndef DNBIAWorkerActivity_IDL
#define DNBIAWorkerActivity_IDL
/*IDLREP*/

//==============================================================================
// COPYRIGHT Dassault Systemes 2003
//==============================================================================
//
// DNBIAWorkerActivity.idl
//    Automation interface for the WorkerActivity
//
//==============================================================================
//  Usage:
//  ------
//
//
//==============================================================================
//  Inheritance:
//  ------------
//
//==============================================================================
// HISTORY:
//
//    Author  Date(yr/mm/dd)  Purpose
//    ------  --------------  -------
//    LFS     2003/07/01      Creation
//    LFS     2003/07/28      Changes to include the IDL file instead of forward
//                            declaration. The generated *.h file doesn't show
//                            forward declartion except baseclasses, SafeArray
//                            classes.
//    SEO     2008/01/24      CAA migration
//==============================================================================

// 
// --- DMAPSInterfaces
#include "CATIAActivity.idl"
// 
// --- DNBHumanModelingInterfaces
#include "SWKIAManikin.idl"
//
// --- DNBHumanSimInterfaces
#include "DNBIAHumanTask.idl"

/**
 * The object that represents an Worker activity. 
* <br>
 * Following activity types qualify as worker activities:
 * <o1>
 *    <li>MoveToPostureActivity</li>
 *    <li>WalkActivity (Forward/Backward/SideStep)</li>
 *    <li>AutoWalkActivity</li>
 *    <li>PickActivity</li>
 *    <li>PlaceActivity</li>
 *    <li>HumanActivityGroup</li>
 *    <li>HumanCallTask</li>
 *    <li>CollisionFreeWalk (Forward and Backward)</li>
 * </ol>
 */
 
interface DNBIAWorkerActivity : CATIAActivity 
{
   /**
    * Returns the Worker-Resource associated with this worker-activity.
    * @return oManikin
	* (see @href SWKIAManikin for more details)
    */ 
#pragma PROPERTY WorkerResource
    HRESULT get_WorkerResource(
        out /*IDLRETVAL*/ SWKIAManikin oManikin); 
   /**
    * Returns the parent HumanTask 
    * @return oHumanTask
	* (see @href DNBIAHumanTask for more details)
    */ 
#pragma PROPERTY HumanTask
    HRESULT get_HumanTask(
        out /*IDLRETVAL*/ DNBIAHumanTask oHumanTask); 
};

// Interface name : DNBIAWorkerActivity
#pragma ID DNBIAWorkerActivity "DCE:0dddbcef-76c0-40d8-979479b4c73e0b3d"
#pragma DUAL DNBIAWorkerActivity

// VB object name : WorkerActivity (Id used in Visual Basic)
#pragma ID WorkerActivity "DCE:28dc87c4-b260-488d-817da7e5f23367de"
#pragma ALIAS DNBIAWorkerActivity WorkerActivity

#endif
