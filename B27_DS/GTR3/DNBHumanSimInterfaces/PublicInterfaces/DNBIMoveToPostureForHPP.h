#ifndef DNBIMoveToPostureForHPP_H
#define DNBIMoveToPostureForHPP_H

// COPYRIGHT DASSAULT SYSTEMES 2010

/**
 * @CAA2Level L0
 * @CAA2Usage U3
 */

#include "DNBHumanSimInterfaces.h"
#include "CATBaseUnknown.h"
#include "CATListOfCATBaseUnknown.h"
#include "SWShortNames.h"

class CATMathTransformation;

extern ExportedByDNBHumanSimInterfaces IID IID_DNBIMoveToPostureForHPP;

/**
 * Interface to set relative interpolation frame on a MoveToPosture activity. At the most,
 * two interpolation frames may be set on a MTP activity (one for each hand segment)
 * <b>Role:</b>
 * This Interface provides methods for setting the interpolation frame for the hand segments
 * that will be used during simulation.
 */
class ExportedByDNBHumanSimInterfaces DNBIMoveToPostureForHPP: public CATBaseUnknown
{
  /**
   * @nodoc
   */
   CATDeclareInterface;
   
public:
   
   /**
    * Sets the relative transformation for a move to posture activity
    * @param iSegName
    *   The segment name for which this relative transformation is to be set.
    * @param iRelXform
    *   The transformation.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
    *   </dl> 
    */
   virtual HRESULT SetRelativeInterpolationFrame(const SWShortName &iSegName,
                                         CATMathTransformation& iRelXform)=0;
};

/**
 * @nodoc
 */
CATDeclareHandler( DNBIMoveToPostureForHPP, CATBaseUnknown );

#endif
