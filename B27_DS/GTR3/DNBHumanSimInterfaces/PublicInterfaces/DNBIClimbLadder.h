/**
 * @CAA2Level L0
 * @CAA2Usage U3
 */
#ifndef DNBIClimbLadder_H
#define DNBIClimbLadder_H
//==============================================================================
/**
 * @fullreview JAT lfrancis 03:04:03
 */
//==============================================================================
// COPYRIGHT Dassault Systemes 2003
//==============================================================================
//
// DNBIClimbLadder.h - interface declaration
//
//==============================================================================
//  Abstract:
//  ---------
//
//      
//==============================================================================
//  Usage:
//  ------
//
//
//==============================================================================
//  Inheritance:
//  ------------
//
//
//==============================================================================
// HISTORY:
//
//    Author  Date(yr/mm/dd)  Purpose
//    ------  --------------  -------
//    LFS     2003/03/01      Creation
//    LFS     2003/06/19      Addition of S(G)etFirstSteppingLeg,
//                            S(G)etManLegsOnRung
//    LFS     2003/06/20      CATBooleanDef.h - to fix aix error
//    LFS     2003/06/25      Data Model for Ladder
//    SEO     2008/01/31      CAA migration
//
//==============================================================================
//#include "DNBHumanSimItfCPP.h"
#include "DNBHumanSimInterfaces.h"
#include "CATBooleanDef.h"
#include "DNBIWorkerActivity.h"

extern ExportedByDNBHumanSimInterfaces IID IID_DNBIClimbLadder ;

class CATMathLine;
class CATMathVector;

/**
* Interface representing ClimbLadder activity
*
*/

class ExportedByDNBHumanSimInterfaces DNBIClimbLadder: public DNBIWorkerActivity
{
	CATDeclareInterface;	
	// No constructors or destructors on this pure virtual base class
	// --------------------------------------------------------------
public:

  /**
   * Sets and Gets the number of rungs to be climbed in ClimbLadder activity
   * <br><b>Role</b>: Sets and Gets the number of rungs to be climbed in ClimbLadder activity
   * @param iInRungs
   * number of rungs to be climbed in the ladder by climbladder activity
   * @return
   *	<b>Legal values</b>:
   *	<br><tt>S_OK :</tt>   on Success
   * 	<br><tt>E_FAIL:</tt>  on failure
   */
   virtual HRESULT SetNoOfRungs(int iInRungs)=0;
   virtual HRESULT GetNoOfRungs(int* piInRungs)=0;

   	/**
   * Sets and Gets the climb type (ascending or descending) in ClimbLadder activity
   * <br><b>Role</b>: Sets and Gets the climb type(ascending or descending) in ClimbLadder activity
   * @param iInType
   * 0 = climb up the ladder
   * 1 = climb down the ladder
   * number of rungs to be climbed in the ladder by climbladder activity
   * @return
   *	<b>Legal values</b>:
   *	<br><tt>S_OK :</tt>   on Success
   * 	<br><tt>E_FAIL:</tt>  on failure
   */
   virtual HRESULT SetClimbType(int iInType)=0;
   virtual HRESULT GetClimbType(int* piInType)=0;

   	/**
   * Sets and Gets the first stepping leg in ClimbLadder activity
   * <br><b>Role</b>: Sets and Gets the first stepping leg in ClimbLadder activity
   * @param iFirstSteppingLeg
   * iFirstSteppingLeg = 0 (Right Leg)
   * iFirstSteppingLeg = 0 (Left Leg)
   * @return
   *	<b>Legal values</b>:
   *	<br><tt>S_OK :</tt>   on Success
   * 	<br><tt>E_FAIL:</tt>  on failure
   */
   virtual HRESULT SetFirstSteppingLeg(int iFirstSteppingLeg)=0;
   virtual HRESULT GetFirstSteppingLeg(int *piFirstSteppingLeg)=0;

   	/**
   * Sets and Gets the Manikin legs on rung for ClimbLadder activity
   * <br><b>Role</b>: This is used to calculate first posture for limb ladder for the 
   * case where manikin is standing on the ground.
   * Climbladder activity can aslo be crated when done when manikin is already on a rung
   * @param bManLegsOnRung
   * TRUE = when manikin is already standing on a rung
   * FALSE = when manikin is standing on the ground
   * @return
   *	<b>Legal values</b>:
   *	<br><tt>S_OK :</tt>   on Success
   * 	<br><tt>E_FAIL:</tt>  on failure
   */
   virtual HRESULT SetManLegsOnRung(boolean bManLegsOnRung)=0;
   virtual HRESULT GetManLegsOnRung(boolean *pbManLegsOnRung)=0;
   
   	/**
   * Sets and Gets the radius of the rungs in ClimbLadder activity
   * <br><b>Role</b>: Sets and Gets the radius of the rungs in ClimbLadder activity
   * @param dValue
   * radius of the rungs
   * @return
   *	<b>Legal values</b>:
   *	<br><tt>S_OK :</tt>   on Success
   * 	<br><tt>E_FAIL:</tt>  on failure
   */
   virtual HRESULT SetRadiusOfRung(const double dValue)=0;
   virtual HRESULT GetRadiusOfRung(double *pdValue)=0;

   	/**
   * Sets and Gets the rung spacing in ClimbLadder activity
   * <br><b>Role</b>: Sets and Gets the rung spacing in ClimbLadder activity
   * @param dValue
   * spacing between the rungs
   * @return
   *	<b>Legal values</b>:
   *	<br><tt>S_OK :</tt>   on Success
   * 	<br><tt>E_FAIL:</tt>  on failure
   */
   virtual HRESULT SetRungSpacing(const double dValue)=0;
   virtual HRESULT GetRungSpacing(double *pdValue)=0;

   	/**
   * Sets and Gets the first rung to be climbed in ClimbLadder activity
   * <br><b>Role</b>: Sets and Gets the first rung to be climbe in ClimbLadder activity
   * @param lineInput
   * Rung object (line)
   * @return
   *	<b>Legal values</b>:
   *	<br><tt>S_OK :</tt>   on Success
   * 	<br><tt>E_FAIL:</tt>  on failure
   */
   virtual HRESULT SetFirstRung(const CATMathLine& lineInput)=0;
   virtual HRESULT GetFirstRung(CATMathLine& lineInOut)=0;

   	/**
   * Sets and Gets the side railing direction in ClimbLadder activity
   * <br><b>Role</b>: The SideRailDirection vector has the vector direction of the side railing in 
   * product coordinates for the ladder product. This is required to create the posture for hands. 
   * @param vecValue
   * Railing vector value
   * @return
   *	<b>Legal values</b>:
   *	<br><tt>S_OK :</tt>   on Success
   * 	<br><tt>E_FAIL:</tt>  on failure
   */
   virtual HRESULT SetSideRailDir(const CATMathVector& vecValue)=0;
   virtual HRESULT GetSideRailDir(CATMathVector& vecValue)=0;
};

//------------------------------------------------------------------
CATDeclareHandler( DNBIClimbLadder, CATBaseUnknown );

#endif
