/**
 * @CAA2Level L0
 * @CAA2Usage U3
 */
// COPYRIGHT Dassault Systemes 2005
//===================================================================
//
// DNBIOperateActivity.h
// Define the DNBIOperateActivity interface
//
//===================================================================
//
// Usage notes:
//   New interface: describe its use here
//
//===================================================================
//
//  Nov 2005  Creation: Code generated by the CAA wizard  bqq
//     SEO     2008/01/31      CAA migration
//===================================================================
#ifndef DNBIOperateActivity_H
#define DNBIOperateActivity_H

//#include "DNBHumanSimItfCPP.h"
#include "DNBHumanSimInterfaces.h"
#include "DNBIWorkerActivity.h"
class SWManikinPart;

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByDNBHumanSimInterfaces IID IID_DNBIOperateActivity;
#else
extern "C" const IID IID_DNBIOperateActivity ;
#endif

//------------------------------------------------------------------

/**
* Interface representing OperateStanding activity
*
*/
class ExportedByDNBHumanSimInterfaces DNBIOperateActivity: public CATBaseUnknown
{
    CATDeclareInterface;

public:

	/**
	* Sets and Gets the kinematic activity associated with operate
	* <br><b>Role</b>: Sets and Gets the kinematic activity associated with operate
	* @param act
	* kinematic activity associated with operate (can be an assembly move activity,device task or robot task)
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
    virtual HRESULT setKinematicAct(CATBaseUnknown_var & act)=0;
    virtual HRESULT getKinematicAct(CATBaseUnknown_var & act)=0;

	/**
	* Sets and Gets the start shot number and end shot numbers in operate activity
	* <br><b>Role</b>: Sets and Gets the start shot number and end shot numbers in operate activity
	* @param shotNum
	* shot number of assembly move activity (applicable only when operate activity is created with assembly move activity)
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
    virtual HRESULT setStartShotNum(int shotNum)=0;
    virtual HRESULT getStartShotNum(int& shotNum)=0;
    virtual HRESULT setEndShotNum(int shotNum)=0;
    virtual HRESULT getEndShotNum(int& shotNum)=0;

	/**
	* Sets and Gets the motion basis for the operate activity
	* <br><b>Role</b>: Sets and Gets the motion basis for the operate activity
	* @param motionBasis
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
    virtual void setMotionBasis( CATUnicodeString motionBasis )=0;
    virtual void getMotionBasis( CATUnicodeString& motionBasis )=0;

	/**
	* Sets and Gets the activity time for the operate activity
	* <br><b>Role</b>: Sets and Gets the activity time for the operate activity
	* @param actTime
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
    virtual HRESULT setActivityTime( double actTime )=0;
    virtual HRESULT getActivityTime( double& actTime )=0;

	/**
	* Sets and Gets the IK behaviour for OperateActivity
	* <br><b>Role</b>: Sets and Gets the IK behaviour for OperateActivity
	* @param IKBehavior
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
    virtual HRESULT setIKBehavior(int IKBehavior)=0;
    virtual HRESULT getIKBehavior(int& IKBehavior)=0;
    /**
	* Sets and Gets the preferred angles on the manikin part for operate activity
	* <br><b>Role</b>: Sets and Gets the preferred angles on the manikin part for operate activity
	* @param pPrefAnglePart
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
    virtual HRESULT setPreferredAngles(const SWManikinPart* pPrefAnglePart)=0;
    virtual HRESULT getPreferredAngles(SWManikinPart*& pPrefAnglePart)=0;

	/**
	* Sets and Gets the joint limits 
	* <br><b>Role</b>: Sets and Gets the joint limits 
	* @param pJointLtPart
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
    virtual HRESULT setJointLimit(const SWManikinPart* pJointLtPart)=0;
    virtual HRESULT getJointLimit(SWManikinPart*& pJointLtPart)=0;

	/**
	* Sets and Gets the colour of operate activity track that will be displayed in the 3D viewer
	* <br><b>Role</b>: Sets and Gets the colour of operate activity track that will be displayed in the 3D viewer
	* @param iColor
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
	virtual HRESULT setOperateActColor( unsigned int iColor)=0;
	virtual HRESULT getOperateActColor(unsigned int & poRed, unsigned int & poGreen,
		                               unsigned int & poBlue)=0;

	/**
	* Sets and Gets the thickness of the line that denotes the track of operate activity
	* <br><b>Role</b>: Sets and Gets the thickness of the line that denotes the track of operate activity
	* @param iWeight
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
	virtual HRESULT setOperateDisplayWeight( unsigned int iWeight)=0;
	virtual HRESULT getOperateDisplayWeight(unsigned int & oWeight)=0;

	/**
	* Sets and Gets the line type to be displayed for the track of operate activity
	* <br><b>Role</b>: Sets and Gets the line type to be displayed for the track of operate activity
	* @param oOperateDisplayType
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
	virtual HRESULT setOperateDisplayType(  unsigned  int  oOperateDisplayType ) = 0;
    virtual HRESULT getOperateDisplayType( unsigned  int & iOperateDisplayType ) = 0;

	/**
	* Sets and Gets the reverse mode 
	* <br><b>Role</b>: Sets and Gets the reverse mode 
	* @param iReverseMode
	* TRUE = denotes reverse operate activity
	* FALSE = denotes operate activity (forward)
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
	virtual HRESULT setReverseMode(  const CATBoolean  iReverseMode ) = 0;
    virtual HRESULT getReverseMode( CATBoolean&  iReverseMode ) = 0;




    // No constructors or destructors on this pure virtual base class
    // --------------------------------------------------------------
};
CATDeclareHandler(DNBIOperateActivity, CATBaseUnknown );
//------------------------------------------------------------------

#endif
