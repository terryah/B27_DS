#ifndef DNBIHumanTaskFactory_H
#define DNBIHumanTaskFactory_H

// COPYRIGHT DASSAULT SYSTEMES 2010

/**
 * @CAA2Level L0
 * @CAA2Usage U3
 */

#include "DNBHumanSimInterfaces.h"
#include "CATBaseUnknown.h"
#include "CATListOfCATBaseUnknown.h"

class CATUnicodeString;
class DNBITask;

extern ExportedByDNBHumanSimInterfaces IID IID_DNBIHumanTaskFactory;

/**
 * Interface to create and access HumanTask.
 * <b>Role:</b>
 * This Interface provides methods for Creating/Getting/Removing Human Tasks.
 */
class ExportedByDNBHumanSimInterfaces DNBIHumanTaskFactory: public CATBaseUnknown
{
  /**
   * @nodoc
   */
   CATDeclareInterface;
   
public:
   
   /**
    * Creates a Human Task
    * @param iName
    *   The Human Task Nmae.
    * @param oHumanTask
    *   The Created Human Task.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
    *   </dl> 
    */
   virtual HRESULT CreateHumanTask(const CATUnicodeString & iName,
                                         DNBITask** oHumanTask)=0;

   /**
    * Retrieves the Underlying List of Human Tasks.
    * @param oHumanTaskList
    *   The Human Task List.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
    *   </dl> 
    */
   virtual HRESULT GetAllHumanTasks(CATListValCATBaseUnknown_var & oHumanTaskList)=0;
   
   /**
    * Removes the required Human Task.
    * @param iHumanTask
    *   The Human Task to be Removed.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
    *   </dl> 
    */
   virtual HRESULT RemoveHumanTask( CATBaseUnknown* iHumanTask)=0;
};

/**
 * @nodoc
 */
CATDeclareHandler( DNBIHumanTaskFactory, CATBaseUnknown );

#endif
