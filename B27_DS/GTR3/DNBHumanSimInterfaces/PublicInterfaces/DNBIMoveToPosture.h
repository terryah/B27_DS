/**
 * @CAA2Level L0
 * @CAA2Usage U3
 */
#ifndef DNBIMoveToPosture_H
#define DNBIMoveToPosture_H
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2000
//==============================================================================
//
// DNBIMoveToPosture.h
//
//==============================================================================
// HISTORY:
//
//    Author  Date(yr/mm/dd)  Purpose
//    ------  --------------  -------
//    VSV     2001/03/01      Initial implementation
//    LFS     2004/05/03      Adding method GetSegmentValues, SetSegmentValues
//                            for Automation Interfaces requirement
//    LFS     2004/06/14      Added - GetNumberOfConstraints, GetConstraint
//                            CreateFixOnConstraint
//    LFS     2005/06/17      Added - ApplyPostureToManikin
//	  SEO	  2007/02/01	  Added - SetAsKeyPosture & IsKeyPostureMTP (for R18MHT003)
//     SEO     2008/01/31      CAA migration
//==============================================================================
#include "DNBHumanSimInterfaces.h"
#include "DNBIWorkerActivity.h"
#include "CATListOfDouble.h"
#include "CATISpecObject.h"
#include "SWShortNames.h"

class CATIProduct;
class CATIMovable;
class CATMathAxis;
class CATMathPoint;
class CATMathTransformation;
class SWKIConstraintNode;
class SWKIIKConstraint;

/**
* Interface representing MoveToPosture Activity
*
*/
extern ExportedByDNBHumanSimInterfaces IID IID_DNBIMoveToPosture ;

//------------------------------------------------------------------
class ExportedByDNBHumanSimInterfaces DNBIMoveToPosture: public DNBIWorkerActivity
{
   CATDeclareInterface;
   
public:
   
   /**
	* Sets and Gets the manikin joint values for MTP
	* <br><b>Role</b>: Sets and Gets the manikin joint values for MTP
	* @param jointVals
	* list of joint values of manikin segments 
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
   virtual void SetJointValues( CATListOfDouble *jointVals ) = 0;
   virtual CATListOfDouble * GetJointValues() = 0;
   
   /**
	* Sets and Gets the motion attribs for the MTP
	* <br><b>Role</b>: Sets and Gets the motion attribs for the MTP
	* @param cornerRounding
	* @param speedPercent
	* @param accel
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
   virtual void SetMotionAttribs( double cornerRounding, double speedPercent, double accel ) = 0;
   virtual void GetMotionAttribs(double *cornerRounding, double *speedPercent, double *accel) = 0;
   
   /**
	* Sets and Gets the motion basis for MTP
	* <br><b>Role</b>: Sets and Gets the motion basis for MTP
	* @param motionBasis
	* (see @href HTSMotionBasis for list of possible values)
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
   virtual void SetMotionBasis( CATUnicodeString motionBasis ) = 0;
   virtual void GetMotionBasis( CATUnicodeString *motionBasis ) = 0;
   
   /**
	* Sets and Gets the manikin referential 
	* <br><b>Role</b>: Gets and sets the manikin referential
	* @param referential
	* (see @href HTSManikinReferential for list of possible values)
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
   virtual void SetManikinReferential(int referential, CATMathPoint& refPosition) = 0;
   virtual void GetManikinReferential(int& referential, CATMathPoint& refPosition) = 0;
   /**
	* Gets the constraint node under MTP (if defined)
	* <br><b>Role</b>: Gets the constraint node under MTP (if defined)
	* @param piCreateIfNecessary
	* TRUE = creates constraint node under MTP
	* FALSE = does not create constraint under MTP if not present
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
   virtual SWKIConstraintNode * GetConstraintNode(boolean piCreateIfNecessary = FALSE) = 0;
   /**
	* Sets and Gets the constraint set for the MTP
	* <br><b>Role</b>: Sets and Gets the constraint set for the MTP
	* @param poConstraintSet
	* list of constraints
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
   virtual HRESULT GetConstraintSet(CATListValCATBaseUnknown_var & poConstraintSet) = 0;
   virtual HRESULT SetConstraintSet(CATListValCATBaseUnknown_var & piConstraintSet) = 0;

   /**
	* Loads the constraints under manikin node under the MTP 
	* <br><b>Role</b>: Loads the constraints under manikin node under the MTP 
	* @param bDeleteCons
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
   virtual HRESULT LoadCurrentConstraintSet(boolean bDeleteCons = TRUE) = 0;

   /**
	* Adds or removes specfied constraint to MTP 
	* <br><b>Role</b>: Adds or removes specfied constraint to MTP 
	* @param pInConstraint
	* constraint to be added/removed under MTP
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>S_OK :</tt>   on Success
	* 	<br><tt>E_FAIL:</tt>  on failure
	*/
   virtual HRESULT AddConstraint(SWKIIKConstraint *pInConstraint)=0;
   virtual HRESULT RemoveConstraint(SWKIIKConstraint* pInConstraint)=0;
   /**
	* Returns if the MTP has constraints
	* @return
	*	<b>Legal values</b>:
	*	<br><tt>TRUE :</tt>  constraints under MTP
	* 	<br><tt>FALSE:</tt>  no constraints under MTP
	*/
   virtual boolean HasConstraints() = 0;
   /**
	* Gets the MTP component container
	* <br><b>Role</b>: Gets the MTP component container
	* @param
	* @return
	* CATIContainer
	*/
   virtual CATIContainer * GetMTPComponentsContainer() = 0;

   /**
    * Returns the number of constraints on the MTP
    *
    * @return iNumber
    * Number of Constraints
    */
   virtual HRESULT GetNumberOfConstraints(long& iNumConstraints)=0;
   /**
    * Returns the constraint at given index
    *
    * @param iIndex
    * Index in the constraint-list to be retrieved
    * @return pioConstraint
    * Constraint at given index
    */
   virtual HRESULT GetConstraint(const long iIndex, SWKIIKConstraint *& pioConstraint)=0;
	/**
	  * This function Created constraint in the MTP containter. T
     * <br>The current IK offset of End Effector is obtained and used in computation
     * of Constraint's CST offset.
	  *
	  * @param pioConstraint
	  *     The created constraint
	  * @param snEE
	  *     End Effector
	  * @param snStartSeg
	  *     Start Segment
	  * @param pMovable
	  *     The object to which FixOn Constraint would be associated
	  * @param xObjWRTEndEff
	  *     The relative offset of Object w.r.to End Effector( EndPoint of EE is the origin)
	  * @param cusName
	  *     The Name of the constraint - if not given constraint will be created with default name
	  **/
   virtual HRESULT CreateFixOnConstraint (
      SWKIIKConstraint *& pioConstraint,
      const SWShortName snEE, const SWShortName snStartSeg,
      CATIProduct * pProduct, const CATMathTransformation& xObjWRTEndEff,
      CATUnicodeString cusName=CATUnicodeString()
      )=0;
   
   /**
    * Gets the DOF values of the given segment from MTP
    *
    * @param inSegName
    *    Name of the Segment 
    * @param oDofVals
    *    Dof values of constraint, array of size 3
    *    <br>
    *    <br><strong>Example:</strong><br>
    *    <br> Following is the call to get Segment values of Righ Thigh, whose short name is "SWRSLeTh"
    *    <br>
    *    <pre>
    *      pMoveToPostureAct->GetSegmentValues( "RSLeTh" , arr3Doubles)
    *     </pre>
    */
   virtual HRESULT GetSegmentValues(const CATUnicodeString& cusSegName, double* pSegmentValues)=0;
   /**
    * Sets the given DOF values for Segments from MTP
    *
    * @param inSegName
    *    Name of the Segment
    * @param oDofVals
    *    Dof values of constraint, array of size 3
    * @param pErrMsg
    *    Error Message contains detail if the DOF value exceeds the min-max limits
    *    <br>
    *    <br><strong>Example:</strong><br>
    *    <br> Following is the call to set Segment values of Righ Thigh, whose short name is "SWRSLeTh"
    *    <br>
    *    <pre>
    *      pMoveToPostureAct->GetSegmentValues( "RSLeTh" , arr3Doubles)
    *     </pre>
    */
   virtual HRESULT SetSegmentValues(const CATUnicodeString& cusSegName, const double* pSegmentValues,
      CATUnicodeString *pErrMsg=NULL)=0;
   /**
    * Set the MTP's DOF onto Manikin (which owns this MTP).
    */
   virtual HRESULT ApplyPostureToManikin()=0;
   
   /**
    * DEPRECATED. DO NOT USE
    */
   virtual void SetLeftHandPartRelation ( CATIMovable * part, CATMathAxis& offset ) = 0;
   virtual void SetRightHandPartRelation ( CATIMovable * part, CATMathAxis& offset ) = 0;
   virtual void SetLeftLegPartRelation ( CATIMovable * part, CATMathAxis& offset ) = 0;
   virtual void SetRightLegPartRelation ( CATIMovable * part, CATMathAxis& offset ) = 0;
   virtual void SetLineOfSightRelation ( CATIMovable * part ) = 0;
   virtual void SetBodyPartRelation ( CATIMovable * part, CATMathAxis& offset ) = 0;
   virtual void SetNeckPartRelation ( CATIMovable * part, CATMathAxis& offset ) = 0;
   
   virtual void GetLeftHandPartRelation ( CATIMovable* & part, CATMathAxis & offset ) = 0;
   virtual void GetRightHandPartRelation ( CATIMovable* & part, CATMathAxis & offset ) = 0;
   virtual void GetLeftLegPartRelation ( CATIMovable* & part, CATMathAxis & offset ) = 0;
   virtual void GetRightLegPartRelation ( CATIMovable* & part, CATMathAxis & offset ) = 0;
   virtual void GetLineOfSightRelation ( CATIMovable* & part ) = 0;
   virtual void GetBodyPartRelation ( CATIMovable* & part, CATMathAxis & offset ) = 0;
   virtual void GetNeckPartRelation ( CATIMovable* & part, CATMathAxis & offset ) = 0;
   
   virtual boolean HasPartRelation() = 0;
   /**
    * End DEPRECATED. DO NOT USE
    */

   // R18MHT003
   virtual HRESULT SetAsKeyPosture(int bIsKeyPosture) = 0;
   virtual HRESULT IsKeyPostureMTP(int &bIsKeyPosture) = 0;


   virtual HRESULT SynchTimeOrSpeed( CATListOfDouble PrevPosture, double& time, double &speed,CATBoolean speedBasis=TRUE)=0;


};

//------------------------------------------------------------------
CATDeclareHandler( DNBIMoveToPosture, CATBaseUnknown );

#endif
