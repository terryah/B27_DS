#==============================================================================
#
# COPYRIGHT DASSAULT SYSTEMES / DELMIA 2000
#==============================================================================
#
# Imakefile for module DNBHumanSimInterfaces.m
#
#==============================================================================
# March 2001  Creation                                                     vsv
#==============================================================================
BUILT_OBJECT_TYPE=SHARED LIBRARY
#-----------------------------------------------
INCLUDED_MODULES = DNBHumanSimItfCPP \
                   DNBHumanSimItfPubIDL \
                   DNBHumanSimItfProIDL
#-----------------------------------------------
LINK_WITH = \ 
JS0GROUP JS0FM CO0RCDBL JS0CORBA \ # basic - CATBaseUnknown, CATMetaClas
CD0SCCP            \   # CATICSOFilter is super-class of DNBIHtsCSOFilter
ProcessInterfaces  \   # CATIAActivity is super-class of DNBIAHumanTask, DNBIAWorkerActivty
CP0CLIP				\	# CATICutAndPastable
SWKHumanModelingItf \
#-----------------------------------------------
OS=IRIX
BUILD   = YES
CXX_EXCEPTION      =       # to accept EXCEPTIONS
CXX_TEMPLATE_INC   = 
CXX_TEMPLATE_PRELK =
LOCAL_CCFLAGS=-D__DNBHumanSimInterfaces
LOCAL_LDFLAGS=
SYS_INCPATH=
SYS_LIBPATH=
SYS_LIBS=

OS=SunOS
BUILD   = YES
LOCAL_CCFLAGS=-D__DNBHumanSimInterfaces
LOCAL_LDFLAGS=
SYS_INCPATH=
SYS_LIBPATH=
SYS_LIBS=

OS=Windows_NT
BUILD   = YES
LOCAL_CCFLAGS= 
LOCAL_LDFLAGS=
SYS_INCPATH=
SYS_LIBPATH=
SYS_LIBS=

OS=HP-UX
BUILD=YES
LOCAL_CCFLAGS=-D__DNBHumanSimInterfaces
LOCAL_LDFLAGS=
SYS_INCPATH=
SYS_LIBPATH=
SYS_LIBS=

OS=AIX
BUILD=YES
LOCAL_CCFLAGS=-D__DNBHumanSimInterfaces
LOCAL_LDFLAGS=
SYS_INCPATH=
SYS_LIBPATH=
SYS_LIBS=

OS=win_a
BUILD=NO
