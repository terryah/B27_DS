#ifndef __TIE_DNBIAMoveToPosture
#define __TIE_DNBIAMoveToPosture

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIAMoveToPosture.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIAMoveToPosture */
#define declare_TIE_DNBIAMoveToPosture(classe) \
 \
 \
class TIEDNBIAMoveToPosture##classe : public DNBIAMoveToPosture \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIAMoveToPosture, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall get_CornerRounding(double & dCorRound); \
      virtual HRESULT __stdcall put_CornerRounding(double dCorRound); \
      virtual HRESULT __stdcall get_SpeedPercent(double & dSpeedPer); \
      virtual HRESULT __stdcall put_SpeedPercent(double dSpeedPer); \
      virtual HRESULT __stdcall get_AccelerationPercent(double & dAccelPer); \
      virtual HRESULT __stdcall put_AccelerationPercent(double dAccelPer); \
      virtual HRESULT __stdcall get_MotionBasis(HTSMotionBasis & eMotBasis); \
      virtual HRESULT __stdcall put_MotionBasis(HTSMotionBasis eMotBasis); \
      virtual HRESULT __stdcall get_Referential(HTSManikinReferential & eReferential); \
      virtual HRESULT __stdcall put_Referential(HTSManikinReferential eReferential); \
      virtual HRESULT __stdcall GetPosition(CATSafeArrayVariant & oTransMatrix); \
      virtual HRESULT __stdcall SetPosition(const CATSafeArrayVariant & oTransMatrix); \
      virtual HRESULT __stdcall GetPostureValues(CATSafeArrayVariant & oPosVals); \
      virtual HRESULT __stdcall SetPostureValues(const CATSafeArrayVariant & oPosVals); \
      virtual HRESULT __stdcall GetJointValues(CATSafeArrayVariant & oJointVals); \
      virtual HRESULT __stdcall SetJointValues(const CATSafeArrayVariant & oJointVals); \
      virtual HRESULT __stdcall SetPartRelation(HTSEndEffector eEE, CATIAProduct * oProduct); \
      virtual HRESULT __stdcall SetPartRelationWithOffset(HTSEndEffector eEE, CATIAProduct * oProduct, const CATSafeArrayVariant & oOffsetTrans); \
      virtual HRESULT __stdcall GetPartRelation(HTSEndEffector eEE, CATIAProduct *& oProduct, CATSafeArrayVariant & oOffsetTrans, CAT_VARIANT_BOOL & bRet); \
      virtual HRESULT __stdcall HasPartRelation(CAT_VARIANT_BOOL & bFlag); \
      virtual HRESULT __stdcall SetCurrentConstraintSet(); \
      virtual HRESULT __stdcall GetNumberOfConstraints(CATLONG & iNumber); \
      virtual HRESULT __stdcall GetConstraint(CATLONG iIndex, SWKIAIKConstraint *& pioConstraint); \
      virtual HRESULT __stdcall AddConstraint(SWKIAIKConstraint * piConstraint); \
      virtual HRESULT __stdcall RemoveConstraint(SWKIAIKConstraint * piConstraint); \
      virtual HRESULT __stdcall GetSegmentValues(const CATBSTR & inSegName, CATSafeArrayVariant & oDofVals); \
      virtual HRESULT __stdcall SetSegmentValues(const CATBSTR & inSegName, const CATSafeArrayVariant & oDofVals); \
      virtual HRESULT __stdcall ApplyPostureToManikin(); \
      virtual HRESULT __stdcall get_WorkerResource(SWKIAManikin *& oManikin); \
      virtual HRESULT __stdcall get_HumanTask(DNBIAHumanTask *& oHumanTask); \
      virtual HRESULT __stdcall IsSubTypeOf(const CATBSTR & iName, CAT_VARIANT_BOOL & oVal); \
      virtual HRESULT __stdcall AttrValue(const CATVariant & iIndex, CATVariant & oAttVal); \
      virtual HRESULT __stdcall AttrName(CATLONG iIndex, CATBSTR & oName); \
      virtual HRESULT __stdcall get_Type(CATBSTR & oType); \
      virtual HRESULT __stdcall get_Description(CATBSTR & oDescriptionBSTR); \
      virtual HRESULT __stdcall put_Description(const CATBSTR & iDescriptionBSTR); \
      virtual HRESULT __stdcall get_CycleTime(double & oCT); \
      virtual HRESULT __stdcall put_CycleTime(double iCT); \
      virtual HRESULT __stdcall get_CalculatedCycleTime(double & oCCT); \
      virtual HRESULT __stdcall get_BeginningDate(double & oBegin); \
      virtual HRESULT __stdcall put_BeginningDate(double iSBT); \
      virtual HRESULT __stdcall get_EndDate(double & oEnd); \
      virtual HRESULT __stdcall get_ChildrenActivities(CATIAActivities *& oChildren); \
      virtual HRESULT __stdcall CreateChild(const CATBSTR & iTypeOfChild, CATIAActivity *& oCreatedChild); \
      virtual HRESULT __stdcall CreateLink(CATIAActivity * iSecondActivity); \
      virtual HRESULT __stdcall RemoveLink(CATIAActivity * iSecondActivity); \
      virtual HRESULT __stdcall get_NextCFActivities(CATIAActivities *& oNextCF); \
      virtual HRESULT __stdcall get_PreviousCFActivities(CATIAActivities *& oPreviousCF); \
      virtual HRESULT __stdcall get_NextPRFActivities(CATIAActivities *& oNextPRF); \
      virtual HRESULT __stdcall get_PreviousPRFActivities(CATIAActivities *& oPreviousPRF); \
      virtual HRESULT __stdcall get_AttrCount(CATLONG & oNbAttr); \
      virtual HRESULT __stdcall get_Items(CATIAItems *& oItems); \
      virtual HRESULT __stdcall get_Outputs(CATIAOutputs *& oOutputs); \
      virtual HRESULT __stdcall get_Resources(CATIAResources *& oResources); \
      virtual HRESULT __stdcall get_Relations(CATIARelations *& oRelations); \
      virtual HRESULT __stdcall get_Parameters(CATIAParameters *& oParameters); \
      virtual HRESULT __stdcall GetTechnologicalObject(const CATBSTR & iApplicationType, CATBaseDispatch *& oApplicativeObj); \
      virtual HRESULT __stdcall get_PrecedenceActivities(CATIAActivities *& oActivities); \
      virtual HRESULT __stdcall get_PossiblePrecedenceActivities(CATIAActivities *& oActivities); \
      virtual HRESULT __stdcall get_ProcessID(CATBSTR & oProcessID); \
      virtual HRESULT __stdcall SetProcessID(const CATBSTR & iProcessID, CAT_VARIANT_BOOL iCheckUnique); \
      virtual HRESULT __stdcall get_CalculatedBeginTime(double & oCBT); \
      virtual HRESULT __stdcall AddAttr(const CATBSTR & iAttributeName, SPPProcessAttributeType AttrType, const CATBSTR & iAttributePromptName); \
      virtual HRESULT __stdcall RemoveAttr(const CATBSTR & iAttributeName); \
      virtual HRESULT __stdcall AddActivityConstraint(CATIAActivity * iActivity, SPPProcessConstraintType iConstraintType); \
      virtual HRESULT __stdcall RemoveActivityConstraint(CATIAActivity * iActivity, SPPProcessConstraintType iConstraintType); \
      virtual HRESULT __stdcall GetActivityConstraints(SPPProcessConstraintType iConstraintType, CATIAActivities *& oConstrtList); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIAMoveToPosture(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall get_CornerRounding(double & dCorRound); \
virtual HRESULT __stdcall put_CornerRounding(double dCorRound); \
virtual HRESULT __stdcall get_SpeedPercent(double & dSpeedPer); \
virtual HRESULT __stdcall put_SpeedPercent(double dSpeedPer); \
virtual HRESULT __stdcall get_AccelerationPercent(double & dAccelPer); \
virtual HRESULT __stdcall put_AccelerationPercent(double dAccelPer); \
virtual HRESULT __stdcall get_MotionBasis(HTSMotionBasis & eMotBasis); \
virtual HRESULT __stdcall put_MotionBasis(HTSMotionBasis eMotBasis); \
virtual HRESULT __stdcall get_Referential(HTSManikinReferential & eReferential); \
virtual HRESULT __stdcall put_Referential(HTSManikinReferential eReferential); \
virtual HRESULT __stdcall GetPosition(CATSafeArrayVariant & oTransMatrix); \
virtual HRESULT __stdcall SetPosition(const CATSafeArrayVariant & oTransMatrix); \
virtual HRESULT __stdcall GetPostureValues(CATSafeArrayVariant & oPosVals); \
virtual HRESULT __stdcall SetPostureValues(const CATSafeArrayVariant & oPosVals); \
virtual HRESULT __stdcall GetJointValues(CATSafeArrayVariant & oJointVals); \
virtual HRESULT __stdcall SetJointValues(const CATSafeArrayVariant & oJointVals); \
virtual HRESULT __stdcall SetPartRelation(HTSEndEffector eEE, CATIAProduct * oProduct); \
virtual HRESULT __stdcall SetPartRelationWithOffset(HTSEndEffector eEE, CATIAProduct * oProduct, const CATSafeArrayVariant & oOffsetTrans); \
virtual HRESULT __stdcall GetPartRelation(HTSEndEffector eEE, CATIAProduct *& oProduct, CATSafeArrayVariant & oOffsetTrans, CAT_VARIANT_BOOL & bRet); \
virtual HRESULT __stdcall HasPartRelation(CAT_VARIANT_BOOL & bFlag); \
virtual HRESULT __stdcall SetCurrentConstraintSet(); \
virtual HRESULT __stdcall GetNumberOfConstraints(CATLONG & iNumber); \
virtual HRESULT __stdcall GetConstraint(CATLONG iIndex, SWKIAIKConstraint *& pioConstraint); \
virtual HRESULT __stdcall AddConstraint(SWKIAIKConstraint * piConstraint); \
virtual HRESULT __stdcall RemoveConstraint(SWKIAIKConstraint * piConstraint); \
virtual HRESULT __stdcall GetSegmentValues(const CATBSTR & inSegName, CATSafeArrayVariant & oDofVals); \
virtual HRESULT __stdcall SetSegmentValues(const CATBSTR & inSegName, const CATSafeArrayVariant & oDofVals); \
virtual HRESULT __stdcall ApplyPostureToManikin(); \
virtual HRESULT __stdcall get_WorkerResource(SWKIAManikin *& oManikin); \
virtual HRESULT __stdcall get_HumanTask(DNBIAHumanTask *& oHumanTask); \
virtual HRESULT __stdcall IsSubTypeOf(const CATBSTR & iName, CAT_VARIANT_BOOL & oVal); \
virtual HRESULT __stdcall AttrValue(const CATVariant & iIndex, CATVariant & oAttVal); \
virtual HRESULT __stdcall AttrName(CATLONG iIndex, CATBSTR & oName); \
virtual HRESULT __stdcall get_Type(CATBSTR & oType); \
virtual HRESULT __stdcall get_Description(CATBSTR & oDescriptionBSTR); \
virtual HRESULT __stdcall put_Description(const CATBSTR & iDescriptionBSTR); \
virtual HRESULT __stdcall get_CycleTime(double & oCT); \
virtual HRESULT __stdcall put_CycleTime(double iCT); \
virtual HRESULT __stdcall get_CalculatedCycleTime(double & oCCT); \
virtual HRESULT __stdcall get_BeginningDate(double & oBegin); \
virtual HRESULT __stdcall put_BeginningDate(double iSBT); \
virtual HRESULT __stdcall get_EndDate(double & oEnd); \
virtual HRESULT __stdcall get_ChildrenActivities(CATIAActivities *& oChildren); \
virtual HRESULT __stdcall CreateChild(const CATBSTR & iTypeOfChild, CATIAActivity *& oCreatedChild); \
virtual HRESULT __stdcall CreateLink(CATIAActivity * iSecondActivity); \
virtual HRESULT __stdcall RemoveLink(CATIAActivity * iSecondActivity); \
virtual HRESULT __stdcall get_NextCFActivities(CATIAActivities *& oNextCF); \
virtual HRESULT __stdcall get_PreviousCFActivities(CATIAActivities *& oPreviousCF); \
virtual HRESULT __stdcall get_NextPRFActivities(CATIAActivities *& oNextPRF); \
virtual HRESULT __stdcall get_PreviousPRFActivities(CATIAActivities *& oPreviousPRF); \
virtual HRESULT __stdcall get_AttrCount(CATLONG & oNbAttr); \
virtual HRESULT __stdcall get_Items(CATIAItems *& oItems); \
virtual HRESULT __stdcall get_Outputs(CATIAOutputs *& oOutputs); \
virtual HRESULT __stdcall get_Resources(CATIAResources *& oResources); \
virtual HRESULT __stdcall get_Relations(CATIARelations *& oRelations); \
virtual HRESULT __stdcall get_Parameters(CATIAParameters *& oParameters); \
virtual HRESULT __stdcall GetTechnologicalObject(const CATBSTR & iApplicationType, CATBaseDispatch *& oApplicativeObj); \
virtual HRESULT __stdcall get_PrecedenceActivities(CATIAActivities *& oActivities); \
virtual HRESULT __stdcall get_PossiblePrecedenceActivities(CATIAActivities *& oActivities); \
virtual HRESULT __stdcall get_ProcessID(CATBSTR & oProcessID); \
virtual HRESULT __stdcall SetProcessID(const CATBSTR & iProcessID, CAT_VARIANT_BOOL iCheckUnique); \
virtual HRESULT __stdcall get_CalculatedBeginTime(double & oCBT); \
virtual HRESULT __stdcall AddAttr(const CATBSTR & iAttributeName, SPPProcessAttributeType AttrType, const CATBSTR & iAttributePromptName); \
virtual HRESULT __stdcall RemoveAttr(const CATBSTR & iAttributeName); \
virtual HRESULT __stdcall AddActivityConstraint(CATIAActivity * iActivity, SPPProcessConstraintType iConstraintType); \
virtual HRESULT __stdcall RemoveActivityConstraint(CATIAActivity * iActivity, SPPProcessConstraintType iConstraintType); \
virtual HRESULT __stdcall GetActivityConstraints(SPPProcessConstraintType iConstraintType, CATIAActivities *& oConstrtList); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIAMoveToPosture(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::get_CornerRounding(double & dCorRound) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)get_CornerRounding(dCorRound)); \
} \
HRESULT __stdcall  ENVTIEName::put_CornerRounding(double dCorRound) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)put_CornerRounding(dCorRound)); \
} \
HRESULT __stdcall  ENVTIEName::get_SpeedPercent(double & dSpeedPer) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)get_SpeedPercent(dSpeedPer)); \
} \
HRESULT __stdcall  ENVTIEName::put_SpeedPercent(double dSpeedPer) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)put_SpeedPercent(dSpeedPer)); \
} \
HRESULT __stdcall  ENVTIEName::get_AccelerationPercent(double & dAccelPer) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)get_AccelerationPercent(dAccelPer)); \
} \
HRESULT __stdcall  ENVTIEName::put_AccelerationPercent(double dAccelPer) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)put_AccelerationPercent(dAccelPer)); \
} \
HRESULT __stdcall  ENVTIEName::get_MotionBasis(HTSMotionBasis & eMotBasis) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)get_MotionBasis(eMotBasis)); \
} \
HRESULT __stdcall  ENVTIEName::put_MotionBasis(HTSMotionBasis eMotBasis) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)put_MotionBasis(eMotBasis)); \
} \
HRESULT __stdcall  ENVTIEName::get_Referential(HTSManikinReferential & eReferential) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)get_Referential(eReferential)); \
} \
HRESULT __stdcall  ENVTIEName::put_Referential(HTSManikinReferential eReferential) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)put_Referential(eReferential)); \
} \
HRESULT __stdcall  ENVTIEName::GetPosition(CATSafeArrayVariant & oTransMatrix) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)GetPosition(oTransMatrix)); \
} \
HRESULT __stdcall  ENVTIEName::SetPosition(const CATSafeArrayVariant & oTransMatrix) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)SetPosition(oTransMatrix)); \
} \
HRESULT __stdcall  ENVTIEName::GetPostureValues(CATSafeArrayVariant & oPosVals) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)GetPostureValues(oPosVals)); \
} \
HRESULT __stdcall  ENVTIEName::SetPostureValues(const CATSafeArrayVariant & oPosVals) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)SetPostureValues(oPosVals)); \
} \
HRESULT __stdcall  ENVTIEName::GetJointValues(CATSafeArrayVariant & oJointVals) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)GetJointValues(oJointVals)); \
} \
HRESULT __stdcall  ENVTIEName::SetJointValues(const CATSafeArrayVariant & oJointVals) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)SetJointValues(oJointVals)); \
} \
HRESULT __stdcall  ENVTIEName::SetPartRelation(HTSEndEffector eEE, CATIAProduct * oProduct) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)SetPartRelation(eEE,oProduct)); \
} \
HRESULT __stdcall  ENVTIEName::SetPartRelationWithOffset(HTSEndEffector eEE, CATIAProduct * oProduct, const CATSafeArrayVariant & oOffsetTrans) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)SetPartRelationWithOffset(eEE,oProduct,oOffsetTrans)); \
} \
HRESULT __stdcall  ENVTIEName::GetPartRelation(HTSEndEffector eEE, CATIAProduct *& oProduct, CATSafeArrayVariant & oOffsetTrans, CAT_VARIANT_BOOL & bRet) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)GetPartRelation(eEE,oProduct,oOffsetTrans,bRet)); \
} \
HRESULT __stdcall  ENVTIEName::HasPartRelation(CAT_VARIANT_BOOL & bFlag) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)HasPartRelation(bFlag)); \
} \
HRESULT __stdcall  ENVTIEName::SetCurrentConstraintSet() \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)SetCurrentConstraintSet()); \
} \
HRESULT __stdcall  ENVTIEName::GetNumberOfConstraints(CATLONG & iNumber) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)GetNumberOfConstraints(iNumber)); \
} \
HRESULT __stdcall  ENVTIEName::GetConstraint(CATLONG iIndex, SWKIAIKConstraint *& pioConstraint) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)GetConstraint(iIndex,pioConstraint)); \
} \
HRESULT __stdcall  ENVTIEName::AddConstraint(SWKIAIKConstraint * piConstraint) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)AddConstraint(piConstraint)); \
} \
HRESULT __stdcall  ENVTIEName::RemoveConstraint(SWKIAIKConstraint * piConstraint) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)RemoveConstraint(piConstraint)); \
} \
HRESULT __stdcall  ENVTIEName::GetSegmentValues(const CATBSTR & inSegName, CATSafeArrayVariant & oDofVals) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)GetSegmentValues(inSegName,oDofVals)); \
} \
HRESULT __stdcall  ENVTIEName::SetSegmentValues(const CATBSTR & inSegName, const CATSafeArrayVariant & oDofVals) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)SetSegmentValues(inSegName,oDofVals)); \
} \
HRESULT __stdcall  ENVTIEName::ApplyPostureToManikin() \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)ApplyPostureToManikin()); \
} \
HRESULT __stdcall  ENVTIEName::get_WorkerResource(SWKIAManikin *& oManikin) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)get_WorkerResource(oManikin)); \
} \
HRESULT __stdcall  ENVTIEName::get_HumanTask(DNBIAHumanTask *& oHumanTask) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)get_HumanTask(oHumanTask)); \
} \
HRESULT __stdcall  ENVTIEName::IsSubTypeOf(const CATBSTR & iName, CAT_VARIANT_BOOL & oVal) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)IsSubTypeOf(iName,oVal)); \
} \
HRESULT __stdcall  ENVTIEName::AttrValue(const CATVariant & iIndex, CATVariant & oAttVal) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)AttrValue(iIndex,oAttVal)); \
} \
HRESULT __stdcall  ENVTIEName::AttrName(CATLONG iIndex, CATBSTR & oName) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)AttrName(iIndex,oName)); \
} \
HRESULT __stdcall  ENVTIEName::get_Type(CATBSTR & oType) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)get_Type(oType)); \
} \
HRESULT __stdcall  ENVTIEName::get_Description(CATBSTR & oDescriptionBSTR) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)get_Description(oDescriptionBSTR)); \
} \
HRESULT __stdcall  ENVTIEName::put_Description(const CATBSTR & iDescriptionBSTR) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)put_Description(iDescriptionBSTR)); \
} \
HRESULT __stdcall  ENVTIEName::get_CycleTime(double & oCT) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)get_CycleTime(oCT)); \
} \
HRESULT __stdcall  ENVTIEName::put_CycleTime(double iCT) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)put_CycleTime(iCT)); \
} \
HRESULT __stdcall  ENVTIEName::get_CalculatedCycleTime(double & oCCT) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)get_CalculatedCycleTime(oCCT)); \
} \
HRESULT __stdcall  ENVTIEName::get_BeginningDate(double & oBegin) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)get_BeginningDate(oBegin)); \
} \
HRESULT __stdcall  ENVTIEName::put_BeginningDate(double iSBT) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)put_BeginningDate(iSBT)); \
} \
HRESULT __stdcall  ENVTIEName::get_EndDate(double & oEnd) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)get_EndDate(oEnd)); \
} \
HRESULT __stdcall  ENVTIEName::get_ChildrenActivities(CATIAActivities *& oChildren) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)get_ChildrenActivities(oChildren)); \
} \
HRESULT __stdcall  ENVTIEName::CreateChild(const CATBSTR & iTypeOfChild, CATIAActivity *& oCreatedChild) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)CreateChild(iTypeOfChild,oCreatedChild)); \
} \
HRESULT __stdcall  ENVTIEName::CreateLink(CATIAActivity * iSecondActivity) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)CreateLink(iSecondActivity)); \
} \
HRESULT __stdcall  ENVTIEName::RemoveLink(CATIAActivity * iSecondActivity) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)RemoveLink(iSecondActivity)); \
} \
HRESULT __stdcall  ENVTIEName::get_NextCFActivities(CATIAActivities *& oNextCF) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)get_NextCFActivities(oNextCF)); \
} \
HRESULT __stdcall  ENVTIEName::get_PreviousCFActivities(CATIAActivities *& oPreviousCF) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)get_PreviousCFActivities(oPreviousCF)); \
} \
HRESULT __stdcall  ENVTIEName::get_NextPRFActivities(CATIAActivities *& oNextPRF) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)get_NextPRFActivities(oNextPRF)); \
} \
HRESULT __stdcall  ENVTIEName::get_PreviousPRFActivities(CATIAActivities *& oPreviousPRF) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)get_PreviousPRFActivities(oPreviousPRF)); \
} \
HRESULT __stdcall  ENVTIEName::get_AttrCount(CATLONG & oNbAttr) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)get_AttrCount(oNbAttr)); \
} \
HRESULT __stdcall  ENVTIEName::get_Items(CATIAItems *& oItems) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)get_Items(oItems)); \
} \
HRESULT __stdcall  ENVTIEName::get_Outputs(CATIAOutputs *& oOutputs) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)get_Outputs(oOutputs)); \
} \
HRESULT __stdcall  ENVTIEName::get_Resources(CATIAResources *& oResources) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)get_Resources(oResources)); \
} \
HRESULT __stdcall  ENVTIEName::get_Relations(CATIARelations *& oRelations) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)get_Relations(oRelations)); \
} \
HRESULT __stdcall  ENVTIEName::get_Parameters(CATIAParameters *& oParameters) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)get_Parameters(oParameters)); \
} \
HRESULT __stdcall  ENVTIEName::GetTechnologicalObject(const CATBSTR & iApplicationType, CATBaseDispatch *& oApplicativeObj) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)GetTechnologicalObject(iApplicationType,oApplicativeObj)); \
} \
HRESULT __stdcall  ENVTIEName::get_PrecedenceActivities(CATIAActivities *& oActivities) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)get_PrecedenceActivities(oActivities)); \
} \
HRESULT __stdcall  ENVTIEName::get_PossiblePrecedenceActivities(CATIAActivities *& oActivities) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)get_PossiblePrecedenceActivities(oActivities)); \
} \
HRESULT __stdcall  ENVTIEName::get_ProcessID(CATBSTR & oProcessID) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)get_ProcessID(oProcessID)); \
} \
HRESULT __stdcall  ENVTIEName::SetProcessID(const CATBSTR & iProcessID, CAT_VARIANT_BOOL iCheckUnique) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)SetProcessID(iProcessID,iCheckUnique)); \
} \
HRESULT __stdcall  ENVTIEName::get_CalculatedBeginTime(double & oCBT) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)get_CalculatedBeginTime(oCBT)); \
} \
HRESULT __stdcall  ENVTIEName::AddAttr(const CATBSTR & iAttributeName, SPPProcessAttributeType AttrType, const CATBSTR & iAttributePromptName) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)AddAttr(iAttributeName,AttrType,iAttributePromptName)); \
} \
HRESULT __stdcall  ENVTIEName::RemoveAttr(const CATBSTR & iAttributeName) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)RemoveAttr(iAttributeName)); \
} \
HRESULT __stdcall  ENVTIEName::AddActivityConstraint(CATIAActivity * iActivity, SPPProcessConstraintType iConstraintType) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)AddActivityConstraint(iActivity,iConstraintType)); \
} \
HRESULT __stdcall  ENVTIEName::RemoveActivityConstraint(CATIAActivity * iActivity, SPPProcessConstraintType iConstraintType) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)RemoveActivityConstraint(iActivity,iConstraintType)); \
} \
HRESULT __stdcall  ENVTIEName::GetActivityConstraints(SPPProcessConstraintType iConstraintType, CATIAActivities *& oConstrtList) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)GetActivityConstraints(iConstraintType,oConstrtList)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIAMoveToPosture,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIAMoveToPosture(classe)    TIEDNBIAMoveToPosture##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIAMoveToPosture(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIAMoveToPosture, classe) \
 \
 \
CATImplementTIEMethods(DNBIAMoveToPosture, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIAMoveToPosture, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIAMoveToPosture, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIAMoveToPosture, classe) \
 \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::get_CornerRounding(double & dCorRound) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&dCorRound); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_CornerRounding(dCorRound); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&dCorRound); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::put_CornerRounding(double dCorRound) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&dCorRound); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_CornerRounding(dCorRound); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&dCorRound); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::get_SpeedPercent(double & dSpeedPer) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&dSpeedPer); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_SpeedPercent(dSpeedPer); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&dSpeedPer); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::put_SpeedPercent(double dSpeedPer) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&dSpeedPer); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_SpeedPercent(dSpeedPer); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&dSpeedPer); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::get_AccelerationPercent(double & dAccelPer) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&dAccelPer); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_AccelerationPercent(dAccelPer); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&dAccelPer); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::put_AccelerationPercent(double dAccelPer) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&dAccelPer); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_AccelerationPercent(dAccelPer); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&dAccelPer); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::get_MotionBasis(HTSMotionBasis & eMotBasis) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&eMotBasis); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_MotionBasis(eMotBasis); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&eMotBasis); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::put_MotionBasis(HTSMotionBasis eMotBasis) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&eMotBasis); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_MotionBasis(eMotBasis); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&eMotBasis); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::get_Referential(HTSManikinReferential & eReferential) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&eReferential); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Referential(eReferential); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&eReferential); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::put_Referential(HTSManikinReferential eReferential) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&eReferential); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Referential(eReferential); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&eReferential); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::GetPosition(CATSafeArrayVariant & oTransMatrix) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,11,&_Trac2,&oTransMatrix); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetPosition(oTransMatrix); \
   ExitAfterCall(this,11,_Trac2,&_ret_arg,&oTransMatrix); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::SetPosition(const CATSafeArrayVariant & oTransMatrix) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,12,&_Trac2,&oTransMatrix); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetPosition(oTransMatrix); \
   ExitAfterCall(this,12,_Trac2,&_ret_arg,&oTransMatrix); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::GetPostureValues(CATSafeArrayVariant & oPosVals) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,13,&_Trac2,&oPosVals); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetPostureValues(oPosVals); \
   ExitAfterCall(this,13,_Trac2,&_ret_arg,&oPosVals); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::SetPostureValues(const CATSafeArrayVariant & oPosVals) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,14,&_Trac2,&oPosVals); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetPostureValues(oPosVals); \
   ExitAfterCall(this,14,_Trac2,&_ret_arg,&oPosVals); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::GetJointValues(CATSafeArrayVariant & oJointVals) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,15,&_Trac2,&oJointVals); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetJointValues(oJointVals); \
   ExitAfterCall(this,15,_Trac2,&_ret_arg,&oJointVals); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::SetJointValues(const CATSafeArrayVariant & oJointVals) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,16,&_Trac2,&oJointVals); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetJointValues(oJointVals); \
   ExitAfterCall(this,16,_Trac2,&_ret_arg,&oJointVals); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::SetPartRelation(HTSEndEffector eEE, CATIAProduct * oProduct) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,17,&_Trac2,&eEE,&oProduct); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetPartRelation(eEE,oProduct); \
   ExitAfterCall(this,17,_Trac2,&_ret_arg,&eEE,&oProduct); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::SetPartRelationWithOffset(HTSEndEffector eEE, CATIAProduct * oProduct, const CATSafeArrayVariant & oOffsetTrans) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,18,&_Trac2,&eEE,&oProduct,&oOffsetTrans); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetPartRelationWithOffset(eEE,oProduct,oOffsetTrans); \
   ExitAfterCall(this,18,_Trac2,&_ret_arg,&eEE,&oProduct,&oOffsetTrans); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::GetPartRelation(HTSEndEffector eEE, CATIAProduct *& oProduct, CATSafeArrayVariant & oOffsetTrans, CAT_VARIANT_BOOL & bRet) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,19,&_Trac2,&eEE,&oProduct,&oOffsetTrans,&bRet); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetPartRelation(eEE,oProduct,oOffsetTrans,bRet); \
   ExitAfterCall(this,19,_Trac2,&_ret_arg,&eEE,&oProduct,&oOffsetTrans,&bRet); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::HasPartRelation(CAT_VARIANT_BOOL & bFlag) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,20,&_Trac2,&bFlag); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->HasPartRelation(bFlag); \
   ExitAfterCall(this,20,_Trac2,&_ret_arg,&bFlag); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::SetCurrentConstraintSet() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,21,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetCurrentConstraintSet(); \
   ExitAfterCall(this,21,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::GetNumberOfConstraints(CATLONG & iNumber) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,22,&_Trac2,&iNumber); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetNumberOfConstraints(iNumber); \
   ExitAfterCall(this,22,_Trac2,&_ret_arg,&iNumber); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::GetConstraint(CATLONG iIndex, SWKIAIKConstraint *& pioConstraint) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,23,&_Trac2,&iIndex,&pioConstraint); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetConstraint(iIndex,pioConstraint); \
   ExitAfterCall(this,23,_Trac2,&_ret_arg,&iIndex,&pioConstraint); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::AddConstraint(SWKIAIKConstraint * piConstraint) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,24,&_Trac2,&piConstraint); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->AddConstraint(piConstraint); \
   ExitAfterCall(this,24,_Trac2,&_ret_arg,&piConstraint); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::RemoveConstraint(SWKIAIKConstraint * piConstraint) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,25,&_Trac2,&piConstraint); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->RemoveConstraint(piConstraint); \
   ExitAfterCall(this,25,_Trac2,&_ret_arg,&piConstraint); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::GetSegmentValues(const CATBSTR & inSegName, CATSafeArrayVariant & oDofVals) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,26,&_Trac2,&inSegName,&oDofVals); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetSegmentValues(inSegName,oDofVals); \
   ExitAfterCall(this,26,_Trac2,&_ret_arg,&inSegName,&oDofVals); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::SetSegmentValues(const CATBSTR & inSegName, const CATSafeArrayVariant & oDofVals) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,27,&_Trac2,&inSegName,&oDofVals); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetSegmentValues(inSegName,oDofVals); \
   ExitAfterCall(this,27,_Trac2,&_ret_arg,&inSegName,&oDofVals); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::ApplyPostureToManikin() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,28,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ApplyPostureToManikin(); \
   ExitAfterCall(this,28,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::get_WorkerResource(SWKIAManikin *& oManikin) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,29,&_Trac2,&oManikin); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_WorkerResource(oManikin); \
   ExitAfterCall(this,29,_Trac2,&_ret_arg,&oManikin); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::get_HumanTask(DNBIAHumanTask *& oHumanTask) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,30,&_Trac2,&oHumanTask); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_HumanTask(oHumanTask); \
   ExitAfterCall(this,30,_Trac2,&_ret_arg,&oHumanTask); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::IsSubTypeOf(const CATBSTR & iName, CAT_VARIANT_BOOL & oVal) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,31,&_Trac2,&iName,&oVal); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->IsSubTypeOf(iName,oVal); \
   ExitAfterCall(this,31,_Trac2,&_ret_arg,&iName,&oVal); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::AttrValue(const CATVariant & iIndex, CATVariant & oAttVal) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,32,&_Trac2,&iIndex,&oAttVal); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->AttrValue(iIndex,oAttVal); \
   ExitAfterCall(this,32,_Trac2,&_ret_arg,&iIndex,&oAttVal); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::AttrName(CATLONG iIndex, CATBSTR & oName) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,33,&_Trac2,&iIndex,&oName); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->AttrName(iIndex,oName); \
   ExitAfterCall(this,33,_Trac2,&_ret_arg,&iIndex,&oName); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::get_Type(CATBSTR & oType) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,34,&_Trac2,&oType); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Type(oType); \
   ExitAfterCall(this,34,_Trac2,&_ret_arg,&oType); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::get_Description(CATBSTR & oDescriptionBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,35,&_Trac2,&oDescriptionBSTR); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Description(oDescriptionBSTR); \
   ExitAfterCall(this,35,_Trac2,&_ret_arg,&oDescriptionBSTR); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::put_Description(const CATBSTR & iDescriptionBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,36,&_Trac2,&iDescriptionBSTR); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Description(iDescriptionBSTR); \
   ExitAfterCall(this,36,_Trac2,&_ret_arg,&iDescriptionBSTR); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::get_CycleTime(double & oCT) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,37,&_Trac2,&oCT); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_CycleTime(oCT); \
   ExitAfterCall(this,37,_Trac2,&_ret_arg,&oCT); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::put_CycleTime(double iCT) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,38,&_Trac2,&iCT); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_CycleTime(iCT); \
   ExitAfterCall(this,38,_Trac2,&_ret_arg,&iCT); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::get_CalculatedCycleTime(double & oCCT) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,39,&_Trac2,&oCCT); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_CalculatedCycleTime(oCCT); \
   ExitAfterCall(this,39,_Trac2,&_ret_arg,&oCCT); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::get_BeginningDate(double & oBegin) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,40,&_Trac2,&oBegin); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_BeginningDate(oBegin); \
   ExitAfterCall(this,40,_Trac2,&_ret_arg,&oBegin); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::put_BeginningDate(double iSBT) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,41,&_Trac2,&iSBT); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_BeginningDate(iSBT); \
   ExitAfterCall(this,41,_Trac2,&_ret_arg,&iSBT); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::get_EndDate(double & oEnd) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,42,&_Trac2,&oEnd); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_EndDate(oEnd); \
   ExitAfterCall(this,42,_Trac2,&_ret_arg,&oEnd); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::get_ChildrenActivities(CATIAActivities *& oChildren) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,43,&_Trac2,&oChildren); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_ChildrenActivities(oChildren); \
   ExitAfterCall(this,43,_Trac2,&_ret_arg,&oChildren); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::CreateChild(const CATBSTR & iTypeOfChild, CATIAActivity *& oCreatedChild) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,44,&_Trac2,&iTypeOfChild,&oCreatedChild); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateChild(iTypeOfChild,oCreatedChild); \
   ExitAfterCall(this,44,_Trac2,&_ret_arg,&iTypeOfChild,&oCreatedChild); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::CreateLink(CATIAActivity * iSecondActivity) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,45,&_Trac2,&iSecondActivity); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateLink(iSecondActivity); \
   ExitAfterCall(this,45,_Trac2,&_ret_arg,&iSecondActivity); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::RemoveLink(CATIAActivity * iSecondActivity) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,46,&_Trac2,&iSecondActivity); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->RemoveLink(iSecondActivity); \
   ExitAfterCall(this,46,_Trac2,&_ret_arg,&iSecondActivity); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::get_NextCFActivities(CATIAActivities *& oNextCF) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,47,&_Trac2,&oNextCF); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_NextCFActivities(oNextCF); \
   ExitAfterCall(this,47,_Trac2,&_ret_arg,&oNextCF); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::get_PreviousCFActivities(CATIAActivities *& oPreviousCF) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,48,&_Trac2,&oPreviousCF); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_PreviousCFActivities(oPreviousCF); \
   ExitAfterCall(this,48,_Trac2,&_ret_arg,&oPreviousCF); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::get_NextPRFActivities(CATIAActivities *& oNextPRF) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,49,&_Trac2,&oNextPRF); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_NextPRFActivities(oNextPRF); \
   ExitAfterCall(this,49,_Trac2,&_ret_arg,&oNextPRF); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::get_PreviousPRFActivities(CATIAActivities *& oPreviousPRF) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,50,&_Trac2,&oPreviousPRF); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_PreviousPRFActivities(oPreviousPRF); \
   ExitAfterCall(this,50,_Trac2,&_ret_arg,&oPreviousPRF); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::get_AttrCount(CATLONG & oNbAttr) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,51,&_Trac2,&oNbAttr); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_AttrCount(oNbAttr); \
   ExitAfterCall(this,51,_Trac2,&_ret_arg,&oNbAttr); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::get_Items(CATIAItems *& oItems) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,52,&_Trac2,&oItems); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Items(oItems); \
   ExitAfterCall(this,52,_Trac2,&_ret_arg,&oItems); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::get_Outputs(CATIAOutputs *& oOutputs) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,53,&_Trac2,&oOutputs); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Outputs(oOutputs); \
   ExitAfterCall(this,53,_Trac2,&_ret_arg,&oOutputs); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::get_Resources(CATIAResources *& oResources) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,54,&_Trac2,&oResources); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Resources(oResources); \
   ExitAfterCall(this,54,_Trac2,&_ret_arg,&oResources); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::get_Relations(CATIARelations *& oRelations) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,55,&_Trac2,&oRelations); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Relations(oRelations); \
   ExitAfterCall(this,55,_Trac2,&_ret_arg,&oRelations); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::get_Parameters(CATIAParameters *& oParameters) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,56,&_Trac2,&oParameters); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parameters(oParameters); \
   ExitAfterCall(this,56,_Trac2,&_ret_arg,&oParameters); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::GetTechnologicalObject(const CATBSTR & iApplicationType, CATBaseDispatch *& oApplicativeObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,57,&_Trac2,&iApplicationType,&oApplicativeObj); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetTechnologicalObject(iApplicationType,oApplicativeObj); \
   ExitAfterCall(this,57,_Trac2,&_ret_arg,&iApplicationType,&oApplicativeObj); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::get_PrecedenceActivities(CATIAActivities *& oActivities) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,58,&_Trac2,&oActivities); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_PrecedenceActivities(oActivities); \
   ExitAfterCall(this,58,_Trac2,&_ret_arg,&oActivities); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::get_PossiblePrecedenceActivities(CATIAActivities *& oActivities) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,59,&_Trac2,&oActivities); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_PossiblePrecedenceActivities(oActivities); \
   ExitAfterCall(this,59,_Trac2,&_ret_arg,&oActivities); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::get_ProcessID(CATBSTR & oProcessID) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,60,&_Trac2,&oProcessID); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_ProcessID(oProcessID); \
   ExitAfterCall(this,60,_Trac2,&_ret_arg,&oProcessID); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::SetProcessID(const CATBSTR & iProcessID, CAT_VARIANT_BOOL iCheckUnique) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,61,&_Trac2,&iProcessID,&iCheckUnique); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetProcessID(iProcessID,iCheckUnique); \
   ExitAfterCall(this,61,_Trac2,&_ret_arg,&iProcessID,&iCheckUnique); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::get_CalculatedBeginTime(double & oCBT) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,62,&_Trac2,&oCBT); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_CalculatedBeginTime(oCBT); \
   ExitAfterCall(this,62,_Trac2,&_ret_arg,&oCBT); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::AddAttr(const CATBSTR & iAttributeName, SPPProcessAttributeType AttrType, const CATBSTR & iAttributePromptName) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,63,&_Trac2,&iAttributeName,&AttrType,&iAttributePromptName); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->AddAttr(iAttributeName,AttrType,iAttributePromptName); \
   ExitAfterCall(this,63,_Trac2,&_ret_arg,&iAttributeName,&AttrType,&iAttributePromptName); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::RemoveAttr(const CATBSTR & iAttributeName) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,64,&_Trac2,&iAttributeName); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->RemoveAttr(iAttributeName); \
   ExitAfterCall(this,64,_Trac2,&_ret_arg,&iAttributeName); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::AddActivityConstraint(CATIAActivity * iActivity, SPPProcessConstraintType iConstraintType) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,65,&_Trac2,&iActivity,&iConstraintType); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->AddActivityConstraint(iActivity,iConstraintType); \
   ExitAfterCall(this,65,_Trac2,&_ret_arg,&iActivity,&iConstraintType); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::RemoveActivityConstraint(CATIAActivity * iActivity, SPPProcessConstraintType iConstraintType) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,66,&_Trac2,&iActivity,&iConstraintType); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->RemoveActivityConstraint(iActivity,iConstraintType); \
   ExitAfterCall(this,66,_Trac2,&_ret_arg,&iActivity,&iConstraintType); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMoveToPosture##classe::GetActivityConstraints(SPPProcessConstraintType iConstraintType, CATIAActivities *& oConstrtList) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,67,&_Trac2,&iConstraintType,&oConstrtList); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetActivityConstraints(iConstraintType,oConstrtList); \
   ExitAfterCall(this,67,_Trac2,&_ret_arg,&iConstraintType,&oConstrtList); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMoveToPosture##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,68,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,68,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMoveToPosture##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,69,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,69,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMoveToPosture##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,70,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,70,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMoveToPosture##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,71,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,71,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMoveToPosture##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,72,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,72,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIAMoveToPosture(classe) \
 \
 \
declare_TIE_DNBIAMoveToPosture(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAMoveToPosture##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAMoveToPosture,"DNBIAMoveToPosture",DNBIAMoveToPosture::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAMoveToPosture(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIAMoveToPosture, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAMoveToPosture##classe(classe::MetaObject(),DNBIAMoveToPosture::MetaObject(),(void *)CreateTIEDNBIAMoveToPosture##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIAMoveToPosture(classe) \
 \
 \
declare_TIE_DNBIAMoveToPosture(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAMoveToPosture##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAMoveToPosture,"DNBIAMoveToPosture",DNBIAMoveToPosture::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAMoveToPosture(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIAMoveToPosture, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAMoveToPosture##classe(classe::MetaObject(),DNBIAMoveToPosture::MetaObject(),(void *)CreateTIEDNBIAMoveToPosture##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIAMoveToPosture(classe) TIE_DNBIAMoveToPosture(classe)
#else
#define BOA_DNBIAMoveToPosture(classe) CATImplementBOA(DNBIAMoveToPosture, classe)
#endif

#endif
