#ifndef __TIE_DNBIAHtsJointSpeedSettingAtt
#define __TIE_DNBIAHtsJointSpeedSettingAtt

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIAHtsJointSpeedSettingAtt.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIAHtsJointSpeedSettingAtt */
#define declare_TIE_DNBIAHtsJointSpeedSettingAtt(classe) \
 \
 \
class TIEDNBIAHtsJointSpeedSettingAtt##classe : public DNBIAHtsJointSpeedSettingAtt \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIAHtsJointSpeedSettingAtt, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall get_AllJointsSpeed(CATBSTR & cusJointSpeed); \
      virtual HRESULT __stdcall put_AllJointsSpeed(const CATBSTR & cusJointSpeed); \
      virtual HRESULT __stdcall GetAllJointsSpeedInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetAllJointsSpeedLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_Rating(CATBSTR & cusRating); \
      virtual HRESULT __stdcall put_Rating(const CATBSTR & cusRating); \
      virtual HRESULT __stdcall GetRatingInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetRatingLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall Commit(); \
      virtual HRESULT __stdcall Rollback(); \
      virtual HRESULT __stdcall ResetToAdminValues(); \
      virtual HRESULT __stdcall ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList); \
      virtual HRESULT __stdcall SaveRepository(); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIAHtsJointSpeedSettingAtt(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall get_AllJointsSpeed(CATBSTR & cusJointSpeed); \
virtual HRESULT __stdcall put_AllJointsSpeed(const CATBSTR & cusJointSpeed); \
virtual HRESULT __stdcall GetAllJointsSpeedInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetAllJointsSpeedLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_Rating(CATBSTR & cusRating); \
virtual HRESULT __stdcall put_Rating(const CATBSTR & cusRating); \
virtual HRESULT __stdcall GetRatingInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetRatingLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall Commit(); \
virtual HRESULT __stdcall Rollback(); \
virtual HRESULT __stdcall ResetToAdminValues(); \
virtual HRESULT __stdcall ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList); \
virtual HRESULT __stdcall SaveRepository(); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIAHtsJointSpeedSettingAtt(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::get_AllJointsSpeed(CATBSTR & cusJointSpeed) \
{ \
return (ENVTIECALL(DNBIAHtsJointSpeedSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_AllJointsSpeed(cusJointSpeed)); \
} \
HRESULT __stdcall  ENVTIEName::put_AllJointsSpeed(const CATBSTR & cusJointSpeed) \
{ \
return (ENVTIECALL(DNBIAHtsJointSpeedSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_AllJointsSpeed(cusJointSpeed)); \
} \
HRESULT __stdcall  ENVTIEName::GetAllJointsSpeedInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAHtsJointSpeedSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetAllJointsSpeedInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetAllJointsSpeedLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAHtsJointSpeedSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetAllJointsSpeedLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_Rating(CATBSTR & cusRating) \
{ \
return (ENVTIECALL(DNBIAHtsJointSpeedSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_Rating(cusRating)); \
} \
HRESULT __stdcall  ENVTIEName::put_Rating(const CATBSTR & cusRating) \
{ \
return (ENVTIECALL(DNBIAHtsJointSpeedSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_Rating(cusRating)); \
} \
HRESULT __stdcall  ENVTIEName::GetRatingInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAHtsJointSpeedSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetRatingInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetRatingLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAHtsJointSpeedSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetRatingLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::Commit() \
{ \
return (ENVTIECALL(DNBIAHtsJointSpeedSettingAtt,ENVTIETypeLetter,ENVTIELetter)Commit()); \
} \
HRESULT __stdcall  ENVTIEName::Rollback() \
{ \
return (ENVTIECALL(DNBIAHtsJointSpeedSettingAtt,ENVTIETypeLetter,ENVTIELetter)Rollback()); \
} \
HRESULT __stdcall  ENVTIEName::ResetToAdminValues() \
{ \
return (ENVTIECALL(DNBIAHtsJointSpeedSettingAtt,ENVTIETypeLetter,ENVTIELetter)ResetToAdminValues()); \
} \
HRESULT __stdcall  ENVTIEName::ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList) \
{ \
return (ENVTIECALL(DNBIAHtsJointSpeedSettingAtt,ENVTIETypeLetter,ENVTIELetter)ResetToAdminValuesByName(iAttList)); \
} \
HRESULT __stdcall  ENVTIEName::SaveRepository() \
{ \
return (ENVTIECALL(DNBIAHtsJointSpeedSettingAtt,ENVTIETypeLetter,ENVTIELetter)SaveRepository()); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIAHtsJointSpeedSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIAHtsJointSpeedSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIAHtsJointSpeedSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIAHtsJointSpeedSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIAHtsJointSpeedSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIAHtsJointSpeedSettingAtt(classe)    TIEDNBIAHtsJointSpeedSettingAtt##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIAHtsJointSpeedSettingAtt(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIAHtsJointSpeedSettingAtt, classe) \
 \
 \
CATImplementTIEMethods(DNBIAHtsJointSpeedSettingAtt, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIAHtsJointSpeedSettingAtt, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIAHtsJointSpeedSettingAtt, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIAHtsJointSpeedSettingAtt, classe) \
 \
HRESULT __stdcall  TIEDNBIAHtsJointSpeedSettingAtt##classe::get_AllJointsSpeed(CATBSTR & cusJointSpeed) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&cusJointSpeed); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_AllJointsSpeed(cusJointSpeed); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&cusJointSpeed); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsJointSpeedSettingAtt##classe::put_AllJointsSpeed(const CATBSTR & cusJointSpeed) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&cusJointSpeed); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_AllJointsSpeed(cusJointSpeed); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&cusJointSpeed); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsJointSpeedSettingAtt##classe::GetAllJointsSpeedInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAllJointsSpeedInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsJointSpeedSettingAtt##classe::SetAllJointsSpeedLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetAllJointsSpeedLock(iLocked); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsJointSpeedSettingAtt##classe::get_Rating(CATBSTR & cusRating) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&cusRating); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Rating(cusRating); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&cusRating); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsJointSpeedSettingAtt##classe::put_Rating(const CATBSTR & cusRating) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&cusRating); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Rating(cusRating); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&cusRating); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsJointSpeedSettingAtt##classe::GetRatingInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetRatingInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsJointSpeedSettingAtt##classe::SetRatingLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetRatingLock(iLocked); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsJointSpeedSettingAtt##classe::Commit() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Commit(); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsJointSpeedSettingAtt##classe::Rollback() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Rollback(); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsJointSpeedSettingAtt##classe::ResetToAdminValues() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,11,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ResetToAdminValues(); \
   ExitAfterCall(this,11,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsJointSpeedSettingAtt##classe::ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,12,&_Trac2,&iAttList); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ResetToAdminValuesByName(iAttList); \
   ExitAfterCall(this,12,_Trac2,&_ret_arg,&iAttList); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsJointSpeedSettingAtt##classe::SaveRepository() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,13,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SaveRepository(); \
   ExitAfterCall(this,13,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAHtsJointSpeedSettingAtt##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,14,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,14,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAHtsJointSpeedSettingAtt##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,15,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,15,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAHtsJointSpeedSettingAtt##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,16,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,16,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAHtsJointSpeedSettingAtt##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,17,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,17,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAHtsJointSpeedSettingAtt##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,18,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,18,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIAHtsJointSpeedSettingAtt(classe) \
 \
 \
declare_TIE_DNBIAHtsJointSpeedSettingAtt(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAHtsJointSpeedSettingAtt##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAHtsJointSpeedSettingAtt,"DNBIAHtsJointSpeedSettingAtt",DNBIAHtsJointSpeedSettingAtt::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAHtsJointSpeedSettingAtt(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIAHtsJointSpeedSettingAtt, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAHtsJointSpeedSettingAtt##classe(classe::MetaObject(),DNBIAHtsJointSpeedSettingAtt::MetaObject(),(void *)CreateTIEDNBIAHtsJointSpeedSettingAtt##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIAHtsJointSpeedSettingAtt(classe) \
 \
 \
declare_TIE_DNBIAHtsJointSpeedSettingAtt(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAHtsJointSpeedSettingAtt##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAHtsJointSpeedSettingAtt,"DNBIAHtsJointSpeedSettingAtt",DNBIAHtsJointSpeedSettingAtt::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAHtsJointSpeedSettingAtt(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIAHtsJointSpeedSettingAtt, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAHtsJointSpeedSettingAtt##classe(classe::MetaObject(),DNBIAHtsJointSpeedSettingAtt::MetaObject(),(void *)CreateTIEDNBIAHtsJointSpeedSettingAtt##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIAHtsJointSpeedSettingAtt(classe) TIE_DNBIAHtsJointSpeedSettingAtt(classe)
#else
#define BOA_DNBIAHtsJointSpeedSettingAtt(classe) CATImplementBOA(DNBIAHtsJointSpeedSettingAtt, classe)
#endif

#endif
