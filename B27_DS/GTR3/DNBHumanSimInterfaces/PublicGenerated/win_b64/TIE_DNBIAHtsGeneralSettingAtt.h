#ifndef __TIE_DNBIAHtsGeneralSettingAtt
#define __TIE_DNBIAHtsGeneralSettingAtt

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIAHtsGeneralSettingAtt.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIAHtsGeneralSettingAtt */
#define declare_TIE_DNBIAHtsGeneralSettingAtt(classe) \
 \
 \
class TIEDNBIAHtsGeneralSettingAtt##classe : public DNBIAHtsGeneralSettingAtt \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIAHtsGeneralSettingAtt, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall get_ConstraintsSimul(short & oConstraintsSimul); \
      virtual HRESULT __stdcall put_ConstraintsSimul(short iConstraintsSimul); \
      virtual HRESULT __stdcall GetConstraintsSimulInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetConstraintsSimulLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_UpdateAnalysisInSimulation(short & oUpdateAnalysisInSimulation); \
      virtual HRESULT __stdcall put_UpdateAnalysisInSimulation(short iUpdateAnalysisInSimulation); \
      virtual HRESULT __stdcall GetUpdateAnalysisInSimulationInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetUpdateAnalysisInSimulationLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_UserWalkSpeedStatus(short & oUserWalkSpeedStatus); \
      virtual HRESULT __stdcall put_UserWalkSpeedStatus(short iUserWalkSpeedStatus); \
      virtual HRESULT __stdcall GetUserWalkSpeedStatusInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetUserWalkSpeedStatusLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_WalkForwardDistance(double & oWalkForwardDistance); \
      virtual HRESULT __stdcall put_WalkForwardDistance(double iWalkForwardDistance); \
      virtual HRESULT __stdcall GetWalkForwardDistanceInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetWalkForwardDistanceLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_WalkForwardAngle(double & oWalkForwardAngle); \
      virtual HRESULT __stdcall put_WalkForwardAngle(double iWalkForwardAngle); \
      virtual HRESULT __stdcall GetWalkForwardAngleInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetWalkForwardAngleLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_SideStepAngle(double & oSideStepAngle); \
      virtual HRESULT __stdcall put_SideStepAngle(double iSideStepAngle); \
      virtual HRESULT __stdcall GetSideStepAngleInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetSideStepAngleLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_DefaultWalkSpeed(double & WalkSpeed); \
      virtual HRESULT __stdcall put_DefaultWalkSpeed(double WalkSpeed); \
      virtual HRESULT __stdcall GetDefaultWalkSpeedInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetDefaultWalkSpeedLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_SyncTimeAndSpeedInSimulation(short & oUpdateAnalysisInSimulation); \
      virtual HRESULT __stdcall put_SyncTimeAndSpeedInSimulation(short iUpdateAnalysisInSimulation); \
      virtual HRESULT __stdcall GetSyncTimeAndSpeedInSimulationInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetSyncTimeAndSpeedInSimulationLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_CollisionSearchIntensity(short & oUpdateAnalysisInSimulation); \
      virtual HRESULT __stdcall put_CollisionSearchIntensity(short iUpdateAnalysisInSimulation); \
      virtual HRESULT __stdcall GetCollisionSearchIntensityInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetCollisionSearchIntensityLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_CollWalkClearance(double & oUpdateAnalysisInSimulation); \
      virtual HRESULT __stdcall put_CollWalkClearance(double iUpdateAnalysisInSimulation); \
      virtual HRESULT __stdcall GetCollWalkClearanceInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetCollWalkClearanceLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall Commit(); \
      virtual HRESULT __stdcall Rollback(); \
      virtual HRESULT __stdcall ResetToAdminValues(); \
      virtual HRESULT __stdcall ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList); \
      virtual HRESULT __stdcall SaveRepository(); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIAHtsGeneralSettingAtt(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall get_ConstraintsSimul(short & oConstraintsSimul); \
virtual HRESULT __stdcall put_ConstraintsSimul(short iConstraintsSimul); \
virtual HRESULT __stdcall GetConstraintsSimulInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetConstraintsSimulLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_UpdateAnalysisInSimulation(short & oUpdateAnalysisInSimulation); \
virtual HRESULT __stdcall put_UpdateAnalysisInSimulation(short iUpdateAnalysisInSimulation); \
virtual HRESULT __stdcall GetUpdateAnalysisInSimulationInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetUpdateAnalysisInSimulationLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_UserWalkSpeedStatus(short & oUserWalkSpeedStatus); \
virtual HRESULT __stdcall put_UserWalkSpeedStatus(short iUserWalkSpeedStatus); \
virtual HRESULT __stdcall GetUserWalkSpeedStatusInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetUserWalkSpeedStatusLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_WalkForwardDistance(double & oWalkForwardDistance); \
virtual HRESULT __stdcall put_WalkForwardDistance(double iWalkForwardDistance); \
virtual HRESULT __stdcall GetWalkForwardDistanceInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetWalkForwardDistanceLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_WalkForwardAngle(double & oWalkForwardAngle); \
virtual HRESULT __stdcall put_WalkForwardAngle(double iWalkForwardAngle); \
virtual HRESULT __stdcall GetWalkForwardAngleInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetWalkForwardAngleLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_SideStepAngle(double & oSideStepAngle); \
virtual HRESULT __stdcall put_SideStepAngle(double iSideStepAngle); \
virtual HRESULT __stdcall GetSideStepAngleInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetSideStepAngleLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_DefaultWalkSpeed(double & WalkSpeed); \
virtual HRESULT __stdcall put_DefaultWalkSpeed(double WalkSpeed); \
virtual HRESULT __stdcall GetDefaultWalkSpeedInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetDefaultWalkSpeedLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_SyncTimeAndSpeedInSimulation(short & oUpdateAnalysisInSimulation); \
virtual HRESULT __stdcall put_SyncTimeAndSpeedInSimulation(short iUpdateAnalysisInSimulation); \
virtual HRESULT __stdcall GetSyncTimeAndSpeedInSimulationInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetSyncTimeAndSpeedInSimulationLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_CollisionSearchIntensity(short & oUpdateAnalysisInSimulation); \
virtual HRESULT __stdcall put_CollisionSearchIntensity(short iUpdateAnalysisInSimulation); \
virtual HRESULT __stdcall GetCollisionSearchIntensityInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetCollisionSearchIntensityLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_CollWalkClearance(double & oUpdateAnalysisInSimulation); \
virtual HRESULT __stdcall put_CollWalkClearance(double iUpdateAnalysisInSimulation); \
virtual HRESULT __stdcall GetCollWalkClearanceInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetCollWalkClearanceLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall Commit(); \
virtual HRESULT __stdcall Rollback(); \
virtual HRESULT __stdcall ResetToAdminValues(); \
virtual HRESULT __stdcall ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList); \
virtual HRESULT __stdcall SaveRepository(); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIAHtsGeneralSettingAtt(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::get_ConstraintsSimul(short & oConstraintsSimul) \
{ \
return (ENVTIECALL(DNBIAHtsGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_ConstraintsSimul(oConstraintsSimul)); \
} \
HRESULT __stdcall  ENVTIEName::put_ConstraintsSimul(short iConstraintsSimul) \
{ \
return (ENVTIECALL(DNBIAHtsGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_ConstraintsSimul(iConstraintsSimul)); \
} \
HRESULT __stdcall  ENVTIEName::GetConstraintsSimulInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAHtsGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetConstraintsSimulInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetConstraintsSimulLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAHtsGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetConstraintsSimulLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_UpdateAnalysisInSimulation(short & oUpdateAnalysisInSimulation) \
{ \
return (ENVTIECALL(DNBIAHtsGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_UpdateAnalysisInSimulation(oUpdateAnalysisInSimulation)); \
} \
HRESULT __stdcall  ENVTIEName::put_UpdateAnalysisInSimulation(short iUpdateAnalysisInSimulation) \
{ \
return (ENVTIECALL(DNBIAHtsGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_UpdateAnalysisInSimulation(iUpdateAnalysisInSimulation)); \
} \
HRESULT __stdcall  ENVTIEName::GetUpdateAnalysisInSimulationInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAHtsGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetUpdateAnalysisInSimulationInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetUpdateAnalysisInSimulationLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAHtsGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetUpdateAnalysisInSimulationLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_UserWalkSpeedStatus(short & oUserWalkSpeedStatus) \
{ \
return (ENVTIECALL(DNBIAHtsGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_UserWalkSpeedStatus(oUserWalkSpeedStatus)); \
} \
HRESULT __stdcall  ENVTIEName::put_UserWalkSpeedStatus(short iUserWalkSpeedStatus) \
{ \
return (ENVTIECALL(DNBIAHtsGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_UserWalkSpeedStatus(iUserWalkSpeedStatus)); \
} \
HRESULT __stdcall  ENVTIEName::GetUserWalkSpeedStatusInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAHtsGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetUserWalkSpeedStatusInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetUserWalkSpeedStatusLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAHtsGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetUserWalkSpeedStatusLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_WalkForwardDistance(double & oWalkForwardDistance) \
{ \
return (ENVTIECALL(DNBIAHtsGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_WalkForwardDistance(oWalkForwardDistance)); \
} \
HRESULT __stdcall  ENVTIEName::put_WalkForwardDistance(double iWalkForwardDistance) \
{ \
return (ENVTIECALL(DNBIAHtsGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_WalkForwardDistance(iWalkForwardDistance)); \
} \
HRESULT __stdcall  ENVTIEName::GetWalkForwardDistanceInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAHtsGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetWalkForwardDistanceInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetWalkForwardDistanceLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAHtsGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetWalkForwardDistanceLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_WalkForwardAngle(double & oWalkForwardAngle) \
{ \
return (ENVTIECALL(DNBIAHtsGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_WalkForwardAngle(oWalkForwardAngle)); \
} \
HRESULT __stdcall  ENVTIEName::put_WalkForwardAngle(double iWalkForwardAngle) \
{ \
return (ENVTIECALL(DNBIAHtsGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_WalkForwardAngle(iWalkForwardAngle)); \
} \
HRESULT __stdcall  ENVTIEName::GetWalkForwardAngleInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAHtsGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetWalkForwardAngleInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetWalkForwardAngleLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAHtsGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetWalkForwardAngleLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_SideStepAngle(double & oSideStepAngle) \
{ \
return (ENVTIECALL(DNBIAHtsGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_SideStepAngle(oSideStepAngle)); \
} \
HRESULT __stdcall  ENVTIEName::put_SideStepAngle(double iSideStepAngle) \
{ \
return (ENVTIECALL(DNBIAHtsGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_SideStepAngle(iSideStepAngle)); \
} \
HRESULT __stdcall  ENVTIEName::GetSideStepAngleInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAHtsGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetSideStepAngleInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetSideStepAngleLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAHtsGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetSideStepAngleLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_DefaultWalkSpeed(double & WalkSpeed) \
{ \
return (ENVTIECALL(DNBIAHtsGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_DefaultWalkSpeed(WalkSpeed)); \
} \
HRESULT __stdcall  ENVTIEName::put_DefaultWalkSpeed(double WalkSpeed) \
{ \
return (ENVTIECALL(DNBIAHtsGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_DefaultWalkSpeed(WalkSpeed)); \
} \
HRESULT __stdcall  ENVTIEName::GetDefaultWalkSpeedInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAHtsGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetDefaultWalkSpeedInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetDefaultWalkSpeedLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAHtsGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetDefaultWalkSpeedLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_SyncTimeAndSpeedInSimulation(short & oUpdateAnalysisInSimulation) \
{ \
return (ENVTIECALL(DNBIAHtsGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_SyncTimeAndSpeedInSimulation(oUpdateAnalysisInSimulation)); \
} \
HRESULT __stdcall  ENVTIEName::put_SyncTimeAndSpeedInSimulation(short iUpdateAnalysisInSimulation) \
{ \
return (ENVTIECALL(DNBIAHtsGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_SyncTimeAndSpeedInSimulation(iUpdateAnalysisInSimulation)); \
} \
HRESULT __stdcall  ENVTIEName::GetSyncTimeAndSpeedInSimulationInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAHtsGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetSyncTimeAndSpeedInSimulationInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetSyncTimeAndSpeedInSimulationLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAHtsGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetSyncTimeAndSpeedInSimulationLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_CollisionSearchIntensity(short & oUpdateAnalysisInSimulation) \
{ \
return (ENVTIECALL(DNBIAHtsGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_CollisionSearchIntensity(oUpdateAnalysisInSimulation)); \
} \
HRESULT __stdcall  ENVTIEName::put_CollisionSearchIntensity(short iUpdateAnalysisInSimulation) \
{ \
return (ENVTIECALL(DNBIAHtsGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_CollisionSearchIntensity(iUpdateAnalysisInSimulation)); \
} \
HRESULT __stdcall  ENVTIEName::GetCollisionSearchIntensityInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAHtsGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetCollisionSearchIntensityInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetCollisionSearchIntensityLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAHtsGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetCollisionSearchIntensityLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_CollWalkClearance(double & oUpdateAnalysisInSimulation) \
{ \
return (ENVTIECALL(DNBIAHtsGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_CollWalkClearance(oUpdateAnalysisInSimulation)); \
} \
HRESULT __stdcall  ENVTIEName::put_CollWalkClearance(double iUpdateAnalysisInSimulation) \
{ \
return (ENVTIECALL(DNBIAHtsGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_CollWalkClearance(iUpdateAnalysisInSimulation)); \
} \
HRESULT __stdcall  ENVTIEName::GetCollWalkClearanceInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAHtsGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetCollWalkClearanceInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetCollWalkClearanceLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAHtsGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetCollWalkClearanceLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::Commit() \
{ \
return (ENVTIECALL(DNBIAHtsGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)Commit()); \
} \
HRESULT __stdcall  ENVTIEName::Rollback() \
{ \
return (ENVTIECALL(DNBIAHtsGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)Rollback()); \
} \
HRESULT __stdcall  ENVTIEName::ResetToAdminValues() \
{ \
return (ENVTIECALL(DNBIAHtsGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)ResetToAdminValues()); \
} \
HRESULT __stdcall  ENVTIEName::ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList) \
{ \
return (ENVTIECALL(DNBIAHtsGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)ResetToAdminValuesByName(iAttList)); \
} \
HRESULT __stdcall  ENVTIEName::SaveRepository() \
{ \
return (ENVTIECALL(DNBIAHtsGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)SaveRepository()); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIAHtsGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIAHtsGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIAHtsGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIAHtsGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIAHtsGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIAHtsGeneralSettingAtt(classe)    TIEDNBIAHtsGeneralSettingAtt##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIAHtsGeneralSettingAtt(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIAHtsGeneralSettingAtt, classe) \
 \
 \
CATImplementTIEMethods(DNBIAHtsGeneralSettingAtt, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIAHtsGeneralSettingAtt, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIAHtsGeneralSettingAtt, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIAHtsGeneralSettingAtt, classe) \
 \
HRESULT __stdcall  TIEDNBIAHtsGeneralSettingAtt##classe::get_ConstraintsSimul(short & oConstraintsSimul) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&oConstraintsSimul); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_ConstraintsSimul(oConstraintsSimul); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&oConstraintsSimul); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsGeneralSettingAtt##classe::put_ConstraintsSimul(short iConstraintsSimul) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&iConstraintsSimul); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_ConstraintsSimul(iConstraintsSimul); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&iConstraintsSimul); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsGeneralSettingAtt##classe::GetConstraintsSimulInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetConstraintsSimulInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsGeneralSettingAtt##classe::SetConstraintsSimulLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetConstraintsSimulLock(iLocked); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsGeneralSettingAtt##classe::get_UpdateAnalysisInSimulation(short & oUpdateAnalysisInSimulation) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&oUpdateAnalysisInSimulation); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_UpdateAnalysisInSimulation(oUpdateAnalysisInSimulation); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&oUpdateAnalysisInSimulation); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsGeneralSettingAtt##classe::put_UpdateAnalysisInSimulation(short iUpdateAnalysisInSimulation) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&iUpdateAnalysisInSimulation); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_UpdateAnalysisInSimulation(iUpdateAnalysisInSimulation); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&iUpdateAnalysisInSimulation); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsGeneralSettingAtt##classe::GetUpdateAnalysisInSimulationInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetUpdateAnalysisInSimulationInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsGeneralSettingAtt##classe::SetUpdateAnalysisInSimulationLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetUpdateAnalysisInSimulationLock(iLocked); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsGeneralSettingAtt##classe::get_UserWalkSpeedStatus(short & oUserWalkSpeedStatus) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&oUserWalkSpeedStatus); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_UserWalkSpeedStatus(oUserWalkSpeedStatus); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&oUserWalkSpeedStatus); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsGeneralSettingAtt##classe::put_UserWalkSpeedStatus(short iUserWalkSpeedStatus) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&iUserWalkSpeedStatus); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_UserWalkSpeedStatus(iUserWalkSpeedStatus); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&iUserWalkSpeedStatus); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsGeneralSettingAtt##classe::GetUserWalkSpeedStatusInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,11,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetUserWalkSpeedStatusInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,11,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsGeneralSettingAtt##classe::SetUserWalkSpeedStatusLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,12,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetUserWalkSpeedStatusLock(iLocked); \
   ExitAfterCall(this,12,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsGeneralSettingAtt##classe::get_WalkForwardDistance(double & oWalkForwardDistance) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,13,&_Trac2,&oWalkForwardDistance); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_WalkForwardDistance(oWalkForwardDistance); \
   ExitAfterCall(this,13,_Trac2,&_ret_arg,&oWalkForwardDistance); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsGeneralSettingAtt##classe::put_WalkForwardDistance(double iWalkForwardDistance) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,14,&_Trac2,&iWalkForwardDistance); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_WalkForwardDistance(iWalkForwardDistance); \
   ExitAfterCall(this,14,_Trac2,&_ret_arg,&iWalkForwardDistance); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsGeneralSettingAtt##classe::GetWalkForwardDistanceInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,15,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetWalkForwardDistanceInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,15,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsGeneralSettingAtt##classe::SetWalkForwardDistanceLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,16,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetWalkForwardDistanceLock(iLocked); \
   ExitAfterCall(this,16,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsGeneralSettingAtt##classe::get_WalkForwardAngle(double & oWalkForwardAngle) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,17,&_Trac2,&oWalkForwardAngle); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_WalkForwardAngle(oWalkForwardAngle); \
   ExitAfterCall(this,17,_Trac2,&_ret_arg,&oWalkForwardAngle); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsGeneralSettingAtt##classe::put_WalkForwardAngle(double iWalkForwardAngle) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,18,&_Trac2,&iWalkForwardAngle); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_WalkForwardAngle(iWalkForwardAngle); \
   ExitAfterCall(this,18,_Trac2,&_ret_arg,&iWalkForwardAngle); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsGeneralSettingAtt##classe::GetWalkForwardAngleInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,19,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetWalkForwardAngleInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,19,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsGeneralSettingAtt##classe::SetWalkForwardAngleLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,20,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetWalkForwardAngleLock(iLocked); \
   ExitAfterCall(this,20,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsGeneralSettingAtt##classe::get_SideStepAngle(double & oSideStepAngle) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,21,&_Trac2,&oSideStepAngle); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_SideStepAngle(oSideStepAngle); \
   ExitAfterCall(this,21,_Trac2,&_ret_arg,&oSideStepAngle); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsGeneralSettingAtt##classe::put_SideStepAngle(double iSideStepAngle) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,22,&_Trac2,&iSideStepAngle); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_SideStepAngle(iSideStepAngle); \
   ExitAfterCall(this,22,_Trac2,&_ret_arg,&iSideStepAngle); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsGeneralSettingAtt##classe::GetSideStepAngleInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,23,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetSideStepAngleInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,23,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsGeneralSettingAtt##classe::SetSideStepAngleLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,24,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetSideStepAngleLock(iLocked); \
   ExitAfterCall(this,24,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsGeneralSettingAtt##classe::get_DefaultWalkSpeed(double & WalkSpeed) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,25,&_Trac2,&WalkSpeed); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_DefaultWalkSpeed(WalkSpeed); \
   ExitAfterCall(this,25,_Trac2,&_ret_arg,&WalkSpeed); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsGeneralSettingAtt##classe::put_DefaultWalkSpeed(double WalkSpeed) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,26,&_Trac2,&WalkSpeed); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_DefaultWalkSpeed(WalkSpeed); \
   ExitAfterCall(this,26,_Trac2,&_ret_arg,&WalkSpeed); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsGeneralSettingAtt##classe::GetDefaultWalkSpeedInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,27,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetDefaultWalkSpeedInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,27,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsGeneralSettingAtt##classe::SetDefaultWalkSpeedLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,28,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetDefaultWalkSpeedLock(iLocked); \
   ExitAfterCall(this,28,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsGeneralSettingAtt##classe::get_SyncTimeAndSpeedInSimulation(short & oUpdateAnalysisInSimulation) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,29,&_Trac2,&oUpdateAnalysisInSimulation); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_SyncTimeAndSpeedInSimulation(oUpdateAnalysisInSimulation); \
   ExitAfterCall(this,29,_Trac2,&_ret_arg,&oUpdateAnalysisInSimulation); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsGeneralSettingAtt##classe::put_SyncTimeAndSpeedInSimulation(short iUpdateAnalysisInSimulation) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,30,&_Trac2,&iUpdateAnalysisInSimulation); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_SyncTimeAndSpeedInSimulation(iUpdateAnalysisInSimulation); \
   ExitAfterCall(this,30,_Trac2,&_ret_arg,&iUpdateAnalysisInSimulation); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsGeneralSettingAtt##classe::GetSyncTimeAndSpeedInSimulationInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,31,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetSyncTimeAndSpeedInSimulationInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,31,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsGeneralSettingAtt##classe::SetSyncTimeAndSpeedInSimulationLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,32,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetSyncTimeAndSpeedInSimulationLock(iLocked); \
   ExitAfterCall(this,32,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsGeneralSettingAtt##classe::get_CollisionSearchIntensity(short & oUpdateAnalysisInSimulation) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,33,&_Trac2,&oUpdateAnalysisInSimulation); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_CollisionSearchIntensity(oUpdateAnalysisInSimulation); \
   ExitAfterCall(this,33,_Trac2,&_ret_arg,&oUpdateAnalysisInSimulation); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsGeneralSettingAtt##classe::put_CollisionSearchIntensity(short iUpdateAnalysisInSimulation) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,34,&_Trac2,&iUpdateAnalysisInSimulation); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_CollisionSearchIntensity(iUpdateAnalysisInSimulation); \
   ExitAfterCall(this,34,_Trac2,&_ret_arg,&iUpdateAnalysisInSimulation); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsGeneralSettingAtt##classe::GetCollisionSearchIntensityInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,35,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetCollisionSearchIntensityInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,35,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsGeneralSettingAtt##classe::SetCollisionSearchIntensityLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,36,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetCollisionSearchIntensityLock(iLocked); \
   ExitAfterCall(this,36,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsGeneralSettingAtt##classe::get_CollWalkClearance(double & oUpdateAnalysisInSimulation) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,37,&_Trac2,&oUpdateAnalysisInSimulation); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_CollWalkClearance(oUpdateAnalysisInSimulation); \
   ExitAfterCall(this,37,_Trac2,&_ret_arg,&oUpdateAnalysisInSimulation); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsGeneralSettingAtt##classe::put_CollWalkClearance(double iUpdateAnalysisInSimulation) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,38,&_Trac2,&iUpdateAnalysisInSimulation); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_CollWalkClearance(iUpdateAnalysisInSimulation); \
   ExitAfterCall(this,38,_Trac2,&_ret_arg,&iUpdateAnalysisInSimulation); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsGeneralSettingAtt##classe::GetCollWalkClearanceInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,39,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetCollWalkClearanceInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,39,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsGeneralSettingAtt##classe::SetCollWalkClearanceLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,40,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetCollWalkClearanceLock(iLocked); \
   ExitAfterCall(this,40,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsGeneralSettingAtt##classe::Commit() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,41,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Commit(); \
   ExitAfterCall(this,41,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsGeneralSettingAtt##classe::Rollback() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,42,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Rollback(); \
   ExitAfterCall(this,42,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsGeneralSettingAtt##classe::ResetToAdminValues() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,43,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ResetToAdminValues(); \
   ExitAfterCall(this,43,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsGeneralSettingAtt##classe::ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,44,&_Trac2,&iAttList); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ResetToAdminValuesByName(iAttList); \
   ExitAfterCall(this,44,_Trac2,&_ret_arg,&iAttList); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsGeneralSettingAtt##classe::SaveRepository() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,45,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SaveRepository(); \
   ExitAfterCall(this,45,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAHtsGeneralSettingAtt##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,46,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,46,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAHtsGeneralSettingAtt##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,47,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,47,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAHtsGeneralSettingAtt##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,48,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,48,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAHtsGeneralSettingAtt##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,49,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,49,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAHtsGeneralSettingAtt##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,50,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,50,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIAHtsGeneralSettingAtt(classe) \
 \
 \
declare_TIE_DNBIAHtsGeneralSettingAtt(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAHtsGeneralSettingAtt##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAHtsGeneralSettingAtt,"DNBIAHtsGeneralSettingAtt",DNBIAHtsGeneralSettingAtt::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAHtsGeneralSettingAtt(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIAHtsGeneralSettingAtt, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAHtsGeneralSettingAtt##classe(classe::MetaObject(),DNBIAHtsGeneralSettingAtt::MetaObject(),(void *)CreateTIEDNBIAHtsGeneralSettingAtt##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIAHtsGeneralSettingAtt(classe) \
 \
 \
declare_TIE_DNBIAHtsGeneralSettingAtt(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAHtsGeneralSettingAtt##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAHtsGeneralSettingAtt,"DNBIAHtsGeneralSettingAtt",DNBIAHtsGeneralSettingAtt::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAHtsGeneralSettingAtt(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIAHtsGeneralSettingAtt, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAHtsGeneralSettingAtt##classe(classe::MetaObject(),DNBIAHtsGeneralSettingAtt::MetaObject(),(void *)CreateTIEDNBIAHtsGeneralSettingAtt##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIAHtsGeneralSettingAtt(classe) TIE_DNBIAHtsGeneralSettingAtt(classe)
#else
#define BOA_DNBIAHtsGeneralSettingAtt(classe) CATImplementBOA(DNBIAHtsGeneralSettingAtt, classe)
#endif

#endif
