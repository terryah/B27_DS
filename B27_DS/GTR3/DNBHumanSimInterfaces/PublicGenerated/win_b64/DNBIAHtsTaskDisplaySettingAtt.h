/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef DNBIAHtsTaskDisplaySettingAtt_h
#define DNBIAHtsTaskDisplaySettingAtt_h

#include "CATCORBABoolean.h"
#include "CAT_VARIANT_BOOL.h"

#ifndef ExportedByDNBHumanSimItfPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __DNBHumanSimItfPubIDL
#define ExportedByDNBHumanSimItfPubIDL __declspec(dllexport)
#else
#define ExportedByDNBHumanSimItfPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByDNBHumanSimItfPubIDL
#endif
#endif

#include "CATBSTR.h"
#include "CATIASettingController.h"

extern ExportedByDNBHumanSimItfPubIDL IID IID_DNBIAHtsTaskDisplaySettingAtt;

class ExportedByDNBHumanSimItfPubIDL DNBIAHtsTaskDisplaySettingAtt : public CATIASettingController
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_WalkActLineType(CATLONG & oWalkActLineType)=0;

    virtual HRESULT __stdcall put_WalkActLineType(CATLONG iWalkActLineType)=0;

    virtual HRESULT __stdcall GetWalkActLineTypeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetWalkActLineTypeLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_AutoWalkActLineType(CATLONG & oAutoWalkActLineType)=0;

    virtual HRESULT __stdcall put_AutoWalkActLineType(CATLONG iAutoWalkActLineType)=0;

    virtual HRESULT __stdcall GetAutoWalkActLineTypeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetAutoWalkActLineTypeLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_WalkActLineWeight(CATLONG & oWalkActLineWeight)=0;

    virtual HRESULT __stdcall put_WalkActLineWeight(CATLONG iWalkActLineWeight)=0;

    virtual HRESULT __stdcall GetWalkActLineWeightInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetWalkActLineWeightLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_AutoWalkActLineWeight(CATLONG & oAutoWalkActLineWeight)=0;

    virtual HRESULT __stdcall put_AutoWalkActLineWeight(CATLONG iAutoWalkActLineWeight)=0;

    virtual HRESULT __stdcall GetAutoWalkActLineWeightInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetAutoWalkActLineWeightLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_MTPActSymbol(CATLONG & oMTPActSymbol)=0;

    virtual HRESULT __stdcall put_MTPActSymbol(CATLONG iMTPActSymbol)=0;

    virtual HRESULT __stdcall GetMTPActSymbolInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetMTPActSymbolLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_MTPActColor(CATLONG & oMTPActColor)=0;

    virtual HRESULT __stdcall put_MTPActColor(CATLONG iMTPActColor)=0;

    virtual HRESULT __stdcall GetMTPActColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetMTPActColorLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_WalkActColor(CATLONG & oWalkActColor)=0;

    virtual HRESULT __stdcall put_WalkActColor(CATLONG iWalkActColor)=0;

    virtual HRESULT __stdcall GetWalkActColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetWalkActColorLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_AutoWalkActColor(CATLONG & oAutoWalkActColor)=0;

    virtual HRESULT __stdcall put_AutoWalkActColor(CATLONG iAutoWalkActColor)=0;

    virtual HRESULT __stdcall GetAutoWalkActColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetAutoWalkActColorLock(CAT_VARIANT_BOOL iLocked)=0;


};

CATDeclareHandler(DNBIAHtsTaskDisplaySettingAtt, CATIASettingController);

#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIABase.h"
#include "CATSafeArray.h"
#include "CATVariant.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
