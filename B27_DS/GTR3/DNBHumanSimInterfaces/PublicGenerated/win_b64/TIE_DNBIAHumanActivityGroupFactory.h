#ifndef __TIE_DNBIAHumanActivityGroupFactory
#define __TIE_DNBIAHumanActivityGroupFactory

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIAHumanActivityGroupFactory.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIAHumanActivityGroupFactory */
#define declare_TIE_DNBIAHumanActivityGroupFactory(classe) \
 \
 \
class TIEDNBIAHumanActivityGroupFactory##classe : public DNBIAHumanActivityGroupFactory \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIAHumanActivityGroupFactory, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall CreateMoveToPosture(CATIAActivity * iPrevAct, DNBIAMoveToPosture *& oCreatedMTP); \
      virtual HRESULT __stdcall CreateWalkFwdOnArrArea(CATIAActivity * iPrevAct, CATIAProduct * iArrArea, CATLONG iNumPoints, const CATSafeArrayVariant & iPoints, DNBIAWalk *& oCreatedWalk); \
      virtual HRESULT __stdcall CreateWalkFwdOnPlane(CATIAActivity * iPrevAct, CATIAProduct * iPlaneProd, const CATSafeArrayVariant & iPlaneDef, CATLONG iNumPoints, const CATSafeArrayVariant & iPoints, DNBIAWalk *& oCreatedWalk); \
      virtual HRESULT __stdcall CreateWalkBwdOnArrArea(CATIAActivity * iPrevAct, CATIAProduct * iArrArea, CATLONG iNumPoints, const CATSafeArrayVariant & iPoints, DNBIAWalk *& oCreatedWalk); \
      virtual HRESULT __stdcall CreateWalkBwdOnPlane(CATIAActivity * iPrevAct, CATIAProduct * iPlaneProd, const CATSafeArrayVariant & iPlaneDef, CATLONG iNumPoints, const CATSafeArrayVariant & iPoints, DNBIAWalk *& oCreatedWalk); \
      virtual HRESULT __stdcall CreateSideStepOnArrArea(CATIAActivity * iPrevAct, CATIAProduct * iArrArea, const CATSafeArrayVariant & iStartPt, const CATSafeArrayVariant & iEndPt, DNBIAWalk *& oCreatedWalk); \
      virtual HRESULT __stdcall CreateSideStepOnPlane(CATIAActivity * iPrevAct, CATIAProduct * iPlaneProd, const CATSafeArrayVariant & iPlaneDef, const CATSafeArrayVariant & iStartPt, const CATSafeArrayVariant & iEndPt, DNBIAWalk *& oCreatedWalk); \
      virtual HRESULT __stdcall CreateAutoWalk(CATIAActivity * iPrevAct, DNBIAAutoWalk *& oCreatedAutoWalk); \
      virtual HRESULT __stdcall CreatePick(CATIAActivity * iPrevAct, HTSPickType iPickType, CAT_VARIANT_BOOL bCreateCstWithPickingHand, HTSHand iPickingHand, const CATSafeArrayVariant & iPickedProducts, DNBIAPick *& oCreatedPick); \
      virtual HRESULT __stdcall CreatePlace(CATIAActivity * iPrevAct, const CATSafeArrayVariant & iPickedProducts, const CATSafeArrayVariant & iOffset, DNBIAPlace *& oCreatedPlace); \
      virtual HRESULT __stdcall CreateCollisionFreeWalkFwdOnArrArea(CATIAActivity * iPrevAct, CATIAProduct * iArrArea, CATLONG iNumPoints, HTSSearchIntensity iSearchInt, double iClearence, const CATSafeArrayVariant & iPoints, DNBIACollisionFreeWalk *& oCreatedWalk); \
      virtual HRESULT __stdcall CreateCollisionFreeWalkFwdOnPlane(CATIAActivity * iPrevAct, CATIAProduct * iPlaneProd, const CATSafeArrayVariant & iPlaneDef, CATLONG iNumPoints, HTSSearchIntensity iSearchInt, double iClearence, const CATSafeArrayVariant & iPoints, DNBIACollisionFreeWalk *& oCreatedWalk); \
      virtual HRESULT __stdcall CreateCollisionFreeWalkBwdOnArrArea(CATIAActivity * iPrevAct, CATIAProduct * iArrArea, CATLONG iNumPoints, HTSSearchIntensity iSearchInt, double iClearence, const CATSafeArrayVariant & iPoints, DNBIACollisionFreeWalk *& oCreatedWalk); \
      virtual HRESULT __stdcall CreateCollisionFreeWalkBwdOnPlane(CATIAActivity * iPrevAct, CATIAProduct * iPlaneProd, const CATSafeArrayVariant & iPlaneDef, CATLONG iNumPoints, HTSSearchIntensity iSearchInt, double iClearence, const CATSafeArrayVariant & iPoints, DNBIACollisionFreeWalk *& oCreatedWalk); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIAHumanActivityGroupFactory(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall CreateMoveToPosture(CATIAActivity * iPrevAct, DNBIAMoveToPosture *& oCreatedMTP); \
virtual HRESULT __stdcall CreateWalkFwdOnArrArea(CATIAActivity * iPrevAct, CATIAProduct * iArrArea, CATLONG iNumPoints, const CATSafeArrayVariant & iPoints, DNBIAWalk *& oCreatedWalk); \
virtual HRESULT __stdcall CreateWalkFwdOnPlane(CATIAActivity * iPrevAct, CATIAProduct * iPlaneProd, const CATSafeArrayVariant & iPlaneDef, CATLONG iNumPoints, const CATSafeArrayVariant & iPoints, DNBIAWalk *& oCreatedWalk); \
virtual HRESULT __stdcall CreateWalkBwdOnArrArea(CATIAActivity * iPrevAct, CATIAProduct * iArrArea, CATLONG iNumPoints, const CATSafeArrayVariant & iPoints, DNBIAWalk *& oCreatedWalk); \
virtual HRESULT __stdcall CreateWalkBwdOnPlane(CATIAActivity * iPrevAct, CATIAProduct * iPlaneProd, const CATSafeArrayVariant & iPlaneDef, CATLONG iNumPoints, const CATSafeArrayVariant & iPoints, DNBIAWalk *& oCreatedWalk); \
virtual HRESULT __stdcall CreateSideStepOnArrArea(CATIAActivity * iPrevAct, CATIAProduct * iArrArea, const CATSafeArrayVariant & iStartPt, const CATSafeArrayVariant & iEndPt, DNBIAWalk *& oCreatedWalk); \
virtual HRESULT __stdcall CreateSideStepOnPlane(CATIAActivity * iPrevAct, CATIAProduct * iPlaneProd, const CATSafeArrayVariant & iPlaneDef, const CATSafeArrayVariant & iStartPt, const CATSafeArrayVariant & iEndPt, DNBIAWalk *& oCreatedWalk); \
virtual HRESULT __stdcall CreateAutoWalk(CATIAActivity * iPrevAct, DNBIAAutoWalk *& oCreatedAutoWalk); \
virtual HRESULT __stdcall CreatePick(CATIAActivity * iPrevAct, HTSPickType iPickType, CAT_VARIANT_BOOL bCreateCstWithPickingHand, HTSHand iPickingHand, const CATSafeArrayVariant & iPickedProducts, DNBIAPick *& oCreatedPick); \
virtual HRESULT __stdcall CreatePlace(CATIAActivity * iPrevAct, const CATSafeArrayVariant & iPickedProducts, const CATSafeArrayVariant & iOffset, DNBIAPlace *& oCreatedPlace); \
virtual HRESULT __stdcall CreateCollisionFreeWalkFwdOnArrArea(CATIAActivity * iPrevAct, CATIAProduct * iArrArea, CATLONG iNumPoints, HTSSearchIntensity iSearchInt, double iClearence, const CATSafeArrayVariant & iPoints, DNBIACollisionFreeWalk *& oCreatedWalk); \
virtual HRESULT __stdcall CreateCollisionFreeWalkFwdOnPlane(CATIAActivity * iPrevAct, CATIAProduct * iPlaneProd, const CATSafeArrayVariant & iPlaneDef, CATLONG iNumPoints, HTSSearchIntensity iSearchInt, double iClearence, const CATSafeArrayVariant & iPoints, DNBIACollisionFreeWalk *& oCreatedWalk); \
virtual HRESULT __stdcall CreateCollisionFreeWalkBwdOnArrArea(CATIAActivity * iPrevAct, CATIAProduct * iArrArea, CATLONG iNumPoints, HTSSearchIntensity iSearchInt, double iClearence, const CATSafeArrayVariant & iPoints, DNBIACollisionFreeWalk *& oCreatedWalk); \
virtual HRESULT __stdcall CreateCollisionFreeWalkBwdOnPlane(CATIAActivity * iPrevAct, CATIAProduct * iPlaneProd, const CATSafeArrayVariant & iPlaneDef, CATLONG iNumPoints, HTSSearchIntensity iSearchInt, double iClearence, const CATSafeArrayVariant & iPoints, DNBIACollisionFreeWalk *& oCreatedWalk); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIAHumanActivityGroupFactory(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::CreateMoveToPosture(CATIAActivity * iPrevAct, DNBIAMoveToPosture *& oCreatedMTP) \
{ \
return (ENVTIECALL(DNBIAHumanActivityGroupFactory,ENVTIETypeLetter,ENVTIELetter)CreateMoveToPosture(iPrevAct,oCreatedMTP)); \
} \
HRESULT __stdcall  ENVTIEName::CreateWalkFwdOnArrArea(CATIAActivity * iPrevAct, CATIAProduct * iArrArea, CATLONG iNumPoints, const CATSafeArrayVariant & iPoints, DNBIAWalk *& oCreatedWalk) \
{ \
return (ENVTIECALL(DNBIAHumanActivityGroupFactory,ENVTIETypeLetter,ENVTIELetter)CreateWalkFwdOnArrArea(iPrevAct,iArrArea,iNumPoints,iPoints,oCreatedWalk)); \
} \
HRESULT __stdcall  ENVTIEName::CreateWalkFwdOnPlane(CATIAActivity * iPrevAct, CATIAProduct * iPlaneProd, const CATSafeArrayVariant & iPlaneDef, CATLONG iNumPoints, const CATSafeArrayVariant & iPoints, DNBIAWalk *& oCreatedWalk) \
{ \
return (ENVTIECALL(DNBIAHumanActivityGroupFactory,ENVTIETypeLetter,ENVTIELetter)CreateWalkFwdOnPlane(iPrevAct,iPlaneProd,iPlaneDef,iNumPoints,iPoints,oCreatedWalk)); \
} \
HRESULT __stdcall  ENVTIEName::CreateWalkBwdOnArrArea(CATIAActivity * iPrevAct, CATIAProduct * iArrArea, CATLONG iNumPoints, const CATSafeArrayVariant & iPoints, DNBIAWalk *& oCreatedWalk) \
{ \
return (ENVTIECALL(DNBIAHumanActivityGroupFactory,ENVTIETypeLetter,ENVTIELetter)CreateWalkBwdOnArrArea(iPrevAct,iArrArea,iNumPoints,iPoints,oCreatedWalk)); \
} \
HRESULT __stdcall  ENVTIEName::CreateWalkBwdOnPlane(CATIAActivity * iPrevAct, CATIAProduct * iPlaneProd, const CATSafeArrayVariant & iPlaneDef, CATLONG iNumPoints, const CATSafeArrayVariant & iPoints, DNBIAWalk *& oCreatedWalk) \
{ \
return (ENVTIECALL(DNBIAHumanActivityGroupFactory,ENVTIETypeLetter,ENVTIELetter)CreateWalkBwdOnPlane(iPrevAct,iPlaneProd,iPlaneDef,iNumPoints,iPoints,oCreatedWalk)); \
} \
HRESULT __stdcall  ENVTIEName::CreateSideStepOnArrArea(CATIAActivity * iPrevAct, CATIAProduct * iArrArea, const CATSafeArrayVariant & iStartPt, const CATSafeArrayVariant & iEndPt, DNBIAWalk *& oCreatedWalk) \
{ \
return (ENVTIECALL(DNBIAHumanActivityGroupFactory,ENVTIETypeLetter,ENVTIELetter)CreateSideStepOnArrArea(iPrevAct,iArrArea,iStartPt,iEndPt,oCreatedWalk)); \
} \
HRESULT __stdcall  ENVTIEName::CreateSideStepOnPlane(CATIAActivity * iPrevAct, CATIAProduct * iPlaneProd, const CATSafeArrayVariant & iPlaneDef, const CATSafeArrayVariant & iStartPt, const CATSafeArrayVariant & iEndPt, DNBIAWalk *& oCreatedWalk) \
{ \
return (ENVTIECALL(DNBIAHumanActivityGroupFactory,ENVTIETypeLetter,ENVTIELetter)CreateSideStepOnPlane(iPrevAct,iPlaneProd,iPlaneDef,iStartPt,iEndPt,oCreatedWalk)); \
} \
HRESULT __stdcall  ENVTIEName::CreateAutoWalk(CATIAActivity * iPrevAct, DNBIAAutoWalk *& oCreatedAutoWalk) \
{ \
return (ENVTIECALL(DNBIAHumanActivityGroupFactory,ENVTIETypeLetter,ENVTIELetter)CreateAutoWalk(iPrevAct,oCreatedAutoWalk)); \
} \
HRESULT __stdcall  ENVTIEName::CreatePick(CATIAActivity * iPrevAct, HTSPickType iPickType, CAT_VARIANT_BOOL bCreateCstWithPickingHand, HTSHand iPickingHand, const CATSafeArrayVariant & iPickedProducts, DNBIAPick *& oCreatedPick) \
{ \
return (ENVTIECALL(DNBIAHumanActivityGroupFactory,ENVTIETypeLetter,ENVTIELetter)CreatePick(iPrevAct,iPickType,bCreateCstWithPickingHand,iPickingHand,iPickedProducts,oCreatedPick)); \
} \
HRESULT __stdcall  ENVTIEName::CreatePlace(CATIAActivity * iPrevAct, const CATSafeArrayVariant & iPickedProducts, const CATSafeArrayVariant & iOffset, DNBIAPlace *& oCreatedPlace) \
{ \
return (ENVTIECALL(DNBIAHumanActivityGroupFactory,ENVTIETypeLetter,ENVTIELetter)CreatePlace(iPrevAct,iPickedProducts,iOffset,oCreatedPlace)); \
} \
HRESULT __stdcall  ENVTIEName::CreateCollisionFreeWalkFwdOnArrArea(CATIAActivity * iPrevAct, CATIAProduct * iArrArea, CATLONG iNumPoints, HTSSearchIntensity iSearchInt, double iClearence, const CATSafeArrayVariant & iPoints, DNBIACollisionFreeWalk *& oCreatedWalk) \
{ \
return (ENVTIECALL(DNBIAHumanActivityGroupFactory,ENVTIETypeLetter,ENVTIELetter)CreateCollisionFreeWalkFwdOnArrArea(iPrevAct,iArrArea,iNumPoints,iSearchInt,iClearence,iPoints,oCreatedWalk)); \
} \
HRESULT __stdcall  ENVTIEName::CreateCollisionFreeWalkFwdOnPlane(CATIAActivity * iPrevAct, CATIAProduct * iPlaneProd, const CATSafeArrayVariant & iPlaneDef, CATLONG iNumPoints, HTSSearchIntensity iSearchInt, double iClearence, const CATSafeArrayVariant & iPoints, DNBIACollisionFreeWalk *& oCreatedWalk) \
{ \
return (ENVTIECALL(DNBIAHumanActivityGroupFactory,ENVTIETypeLetter,ENVTIELetter)CreateCollisionFreeWalkFwdOnPlane(iPrevAct,iPlaneProd,iPlaneDef,iNumPoints,iSearchInt,iClearence,iPoints,oCreatedWalk)); \
} \
HRESULT __stdcall  ENVTIEName::CreateCollisionFreeWalkBwdOnArrArea(CATIAActivity * iPrevAct, CATIAProduct * iArrArea, CATLONG iNumPoints, HTSSearchIntensity iSearchInt, double iClearence, const CATSafeArrayVariant & iPoints, DNBIACollisionFreeWalk *& oCreatedWalk) \
{ \
return (ENVTIECALL(DNBIAHumanActivityGroupFactory,ENVTIETypeLetter,ENVTIELetter)CreateCollisionFreeWalkBwdOnArrArea(iPrevAct,iArrArea,iNumPoints,iSearchInt,iClearence,iPoints,oCreatedWalk)); \
} \
HRESULT __stdcall  ENVTIEName::CreateCollisionFreeWalkBwdOnPlane(CATIAActivity * iPrevAct, CATIAProduct * iPlaneProd, const CATSafeArrayVariant & iPlaneDef, CATLONG iNumPoints, HTSSearchIntensity iSearchInt, double iClearence, const CATSafeArrayVariant & iPoints, DNBIACollisionFreeWalk *& oCreatedWalk) \
{ \
return (ENVTIECALL(DNBIAHumanActivityGroupFactory,ENVTIETypeLetter,ENVTIELetter)CreateCollisionFreeWalkBwdOnPlane(iPrevAct,iPlaneProd,iPlaneDef,iNumPoints,iSearchInt,iClearence,iPoints,oCreatedWalk)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIAHumanActivityGroupFactory,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIAHumanActivityGroupFactory,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIAHumanActivityGroupFactory,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIAHumanActivityGroupFactory,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIAHumanActivityGroupFactory,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIAHumanActivityGroupFactory(classe)    TIEDNBIAHumanActivityGroupFactory##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIAHumanActivityGroupFactory(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIAHumanActivityGroupFactory, classe) \
 \
 \
CATImplementTIEMethods(DNBIAHumanActivityGroupFactory, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIAHumanActivityGroupFactory, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIAHumanActivityGroupFactory, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIAHumanActivityGroupFactory, classe) \
 \
HRESULT __stdcall  TIEDNBIAHumanActivityGroupFactory##classe::CreateMoveToPosture(CATIAActivity * iPrevAct, DNBIAMoveToPosture *& oCreatedMTP) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&iPrevAct,&oCreatedMTP); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateMoveToPosture(iPrevAct,oCreatedMTP); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&iPrevAct,&oCreatedMTP); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHumanActivityGroupFactory##classe::CreateWalkFwdOnArrArea(CATIAActivity * iPrevAct, CATIAProduct * iArrArea, CATLONG iNumPoints, const CATSafeArrayVariant & iPoints, DNBIAWalk *& oCreatedWalk) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&iPrevAct,&iArrArea,&iNumPoints,&iPoints,&oCreatedWalk); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateWalkFwdOnArrArea(iPrevAct,iArrArea,iNumPoints,iPoints,oCreatedWalk); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&iPrevAct,&iArrArea,&iNumPoints,&iPoints,&oCreatedWalk); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHumanActivityGroupFactory##classe::CreateWalkFwdOnPlane(CATIAActivity * iPrevAct, CATIAProduct * iPlaneProd, const CATSafeArrayVariant & iPlaneDef, CATLONG iNumPoints, const CATSafeArrayVariant & iPoints, DNBIAWalk *& oCreatedWalk) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&iPrevAct,&iPlaneProd,&iPlaneDef,&iNumPoints,&iPoints,&oCreatedWalk); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateWalkFwdOnPlane(iPrevAct,iPlaneProd,iPlaneDef,iNumPoints,iPoints,oCreatedWalk); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&iPrevAct,&iPlaneProd,&iPlaneDef,&iNumPoints,&iPoints,&oCreatedWalk); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHumanActivityGroupFactory##classe::CreateWalkBwdOnArrArea(CATIAActivity * iPrevAct, CATIAProduct * iArrArea, CATLONG iNumPoints, const CATSafeArrayVariant & iPoints, DNBIAWalk *& oCreatedWalk) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&iPrevAct,&iArrArea,&iNumPoints,&iPoints,&oCreatedWalk); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateWalkBwdOnArrArea(iPrevAct,iArrArea,iNumPoints,iPoints,oCreatedWalk); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&iPrevAct,&iArrArea,&iNumPoints,&iPoints,&oCreatedWalk); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHumanActivityGroupFactory##classe::CreateWalkBwdOnPlane(CATIAActivity * iPrevAct, CATIAProduct * iPlaneProd, const CATSafeArrayVariant & iPlaneDef, CATLONG iNumPoints, const CATSafeArrayVariant & iPoints, DNBIAWalk *& oCreatedWalk) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&iPrevAct,&iPlaneProd,&iPlaneDef,&iNumPoints,&iPoints,&oCreatedWalk); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateWalkBwdOnPlane(iPrevAct,iPlaneProd,iPlaneDef,iNumPoints,iPoints,oCreatedWalk); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&iPrevAct,&iPlaneProd,&iPlaneDef,&iNumPoints,&iPoints,&oCreatedWalk); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHumanActivityGroupFactory##classe::CreateSideStepOnArrArea(CATIAActivity * iPrevAct, CATIAProduct * iArrArea, const CATSafeArrayVariant & iStartPt, const CATSafeArrayVariant & iEndPt, DNBIAWalk *& oCreatedWalk) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&iPrevAct,&iArrArea,&iStartPt,&iEndPt,&oCreatedWalk); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateSideStepOnArrArea(iPrevAct,iArrArea,iStartPt,iEndPt,oCreatedWalk); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&iPrevAct,&iArrArea,&iStartPt,&iEndPt,&oCreatedWalk); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHumanActivityGroupFactory##classe::CreateSideStepOnPlane(CATIAActivity * iPrevAct, CATIAProduct * iPlaneProd, const CATSafeArrayVariant & iPlaneDef, const CATSafeArrayVariant & iStartPt, const CATSafeArrayVariant & iEndPt, DNBIAWalk *& oCreatedWalk) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&iPrevAct,&iPlaneProd,&iPlaneDef,&iStartPt,&iEndPt,&oCreatedWalk); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateSideStepOnPlane(iPrevAct,iPlaneProd,iPlaneDef,iStartPt,iEndPt,oCreatedWalk); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&iPrevAct,&iPlaneProd,&iPlaneDef,&iStartPt,&iEndPt,&oCreatedWalk); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHumanActivityGroupFactory##classe::CreateAutoWalk(CATIAActivity * iPrevAct, DNBIAAutoWalk *& oCreatedAutoWalk) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&iPrevAct,&oCreatedAutoWalk); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateAutoWalk(iPrevAct,oCreatedAutoWalk); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&iPrevAct,&oCreatedAutoWalk); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHumanActivityGroupFactory##classe::CreatePick(CATIAActivity * iPrevAct, HTSPickType iPickType, CAT_VARIANT_BOOL bCreateCstWithPickingHand, HTSHand iPickingHand, const CATSafeArrayVariant & iPickedProducts, DNBIAPick *& oCreatedPick) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&iPrevAct,&iPickType,&bCreateCstWithPickingHand,&iPickingHand,&iPickedProducts,&oCreatedPick); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreatePick(iPrevAct,iPickType,bCreateCstWithPickingHand,iPickingHand,iPickedProducts,oCreatedPick); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&iPrevAct,&iPickType,&bCreateCstWithPickingHand,&iPickingHand,&iPickedProducts,&oCreatedPick); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHumanActivityGroupFactory##classe::CreatePlace(CATIAActivity * iPrevAct, const CATSafeArrayVariant & iPickedProducts, const CATSafeArrayVariant & iOffset, DNBIAPlace *& oCreatedPlace) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&iPrevAct,&iPickedProducts,&iOffset,&oCreatedPlace); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreatePlace(iPrevAct,iPickedProducts,iOffset,oCreatedPlace); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&iPrevAct,&iPickedProducts,&iOffset,&oCreatedPlace); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHumanActivityGroupFactory##classe::CreateCollisionFreeWalkFwdOnArrArea(CATIAActivity * iPrevAct, CATIAProduct * iArrArea, CATLONG iNumPoints, HTSSearchIntensity iSearchInt, double iClearence, const CATSafeArrayVariant & iPoints, DNBIACollisionFreeWalk *& oCreatedWalk) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,11,&_Trac2,&iPrevAct,&iArrArea,&iNumPoints,&iSearchInt,&iClearence,&iPoints,&oCreatedWalk); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateCollisionFreeWalkFwdOnArrArea(iPrevAct,iArrArea,iNumPoints,iSearchInt,iClearence,iPoints,oCreatedWalk); \
   ExitAfterCall(this,11,_Trac2,&_ret_arg,&iPrevAct,&iArrArea,&iNumPoints,&iSearchInt,&iClearence,&iPoints,&oCreatedWalk); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHumanActivityGroupFactory##classe::CreateCollisionFreeWalkFwdOnPlane(CATIAActivity * iPrevAct, CATIAProduct * iPlaneProd, const CATSafeArrayVariant & iPlaneDef, CATLONG iNumPoints, HTSSearchIntensity iSearchInt, double iClearence, const CATSafeArrayVariant & iPoints, DNBIACollisionFreeWalk *& oCreatedWalk) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,12,&_Trac2,&iPrevAct,&iPlaneProd,&iPlaneDef,&iNumPoints,&iSearchInt,&iClearence,&iPoints,&oCreatedWalk); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateCollisionFreeWalkFwdOnPlane(iPrevAct,iPlaneProd,iPlaneDef,iNumPoints,iSearchInt,iClearence,iPoints,oCreatedWalk); \
   ExitAfterCall(this,12,_Trac2,&_ret_arg,&iPrevAct,&iPlaneProd,&iPlaneDef,&iNumPoints,&iSearchInt,&iClearence,&iPoints,&oCreatedWalk); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHumanActivityGroupFactory##classe::CreateCollisionFreeWalkBwdOnArrArea(CATIAActivity * iPrevAct, CATIAProduct * iArrArea, CATLONG iNumPoints, HTSSearchIntensity iSearchInt, double iClearence, const CATSafeArrayVariant & iPoints, DNBIACollisionFreeWalk *& oCreatedWalk) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,13,&_Trac2,&iPrevAct,&iArrArea,&iNumPoints,&iSearchInt,&iClearence,&iPoints,&oCreatedWalk); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateCollisionFreeWalkBwdOnArrArea(iPrevAct,iArrArea,iNumPoints,iSearchInt,iClearence,iPoints,oCreatedWalk); \
   ExitAfterCall(this,13,_Trac2,&_ret_arg,&iPrevAct,&iArrArea,&iNumPoints,&iSearchInt,&iClearence,&iPoints,&oCreatedWalk); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHumanActivityGroupFactory##classe::CreateCollisionFreeWalkBwdOnPlane(CATIAActivity * iPrevAct, CATIAProduct * iPlaneProd, const CATSafeArrayVariant & iPlaneDef, CATLONG iNumPoints, HTSSearchIntensity iSearchInt, double iClearence, const CATSafeArrayVariant & iPoints, DNBIACollisionFreeWalk *& oCreatedWalk) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,14,&_Trac2,&iPrevAct,&iPlaneProd,&iPlaneDef,&iNumPoints,&iSearchInt,&iClearence,&iPoints,&oCreatedWalk); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateCollisionFreeWalkBwdOnPlane(iPrevAct,iPlaneProd,iPlaneDef,iNumPoints,iSearchInt,iClearence,iPoints,oCreatedWalk); \
   ExitAfterCall(this,14,_Trac2,&_ret_arg,&iPrevAct,&iPlaneProd,&iPlaneDef,&iNumPoints,&iSearchInt,&iClearence,&iPoints,&oCreatedWalk); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAHumanActivityGroupFactory##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,15,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,15,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAHumanActivityGroupFactory##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,16,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,16,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAHumanActivityGroupFactory##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,17,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,17,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAHumanActivityGroupFactory##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,18,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,18,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAHumanActivityGroupFactory##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,19,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,19,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIAHumanActivityGroupFactory(classe) \
 \
 \
declare_TIE_DNBIAHumanActivityGroupFactory(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAHumanActivityGroupFactory##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAHumanActivityGroupFactory,"DNBIAHumanActivityGroupFactory",DNBIAHumanActivityGroupFactory::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAHumanActivityGroupFactory(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIAHumanActivityGroupFactory, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAHumanActivityGroupFactory##classe(classe::MetaObject(),DNBIAHumanActivityGroupFactory::MetaObject(),(void *)CreateTIEDNBIAHumanActivityGroupFactory##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIAHumanActivityGroupFactory(classe) \
 \
 \
declare_TIE_DNBIAHumanActivityGroupFactory(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAHumanActivityGroupFactory##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAHumanActivityGroupFactory,"DNBIAHumanActivityGroupFactory",DNBIAHumanActivityGroupFactory::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAHumanActivityGroupFactory(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIAHumanActivityGroupFactory, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAHumanActivityGroupFactory##classe(classe::MetaObject(),DNBIAHumanActivityGroupFactory::MetaObject(),(void *)CreateTIEDNBIAHumanActivityGroupFactory##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIAHumanActivityGroupFactory(classe) TIE_DNBIAHumanActivityGroupFactory(classe)
#else
#define BOA_DNBIAHumanActivityGroupFactory(classe) CATImplementBOA(DNBIAHumanActivityGroupFactory, classe)
#endif

#endif
