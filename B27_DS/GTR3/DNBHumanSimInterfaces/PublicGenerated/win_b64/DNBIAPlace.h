/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef DNBIAPlace_h
#define DNBIAPlace_h

#ifndef ExportedByDNBHumanSimItfPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __DNBHumanSimItfPubIDL
#define ExportedByDNBHumanSimItfPubIDL __declspec(dllexport)
#else
#define ExportedByDNBHumanSimItfPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByDNBHumanSimItfPubIDL
#endif
#endif

#include "CATSafeArray.h"
#include "DNBIAWorkerActivity.h"

class CATIAActivity;
class CATIAProduct;
class DNBIAMfgAssembly;

extern ExportedByDNBHumanSimItfPubIDL IID IID_DNBIAPlace;

class ExportedByDNBHumanSimItfPubIDL DNBIAPlace : public DNBIAWorkerActivity
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall GetPlacedProducts(CATSafeArrayVariant & pPlacedProds)=0;

    virtual HRESULT __stdcall SetPlacedProducts(const CATSafeArrayVariant & pPlacedProds)=0;

    virtual HRESULT __stdcall SetOffset(const CATSafeArrayVariant & oOffsetTransList)=0;

    virtual HRESULT __stdcall GetOffset(CATSafeArrayVariant & oOffsetTransList)=0;

    virtual HRESULT __stdcall SetPickAct(CATIAActivity * pickAct)=0;

    virtual HRESULT __stdcall AddPlacedProduct(CATIAProduct * pPickedItem, const CATSafeArrayVariant & vbSAV)=0;

    virtual HRESULT __stdcall RemovePlacedProduct(CATIAProduct * pPickedItem)=0;

    virtual HRESULT __stdcall AddPlacedMfgAssembly(DNBIAMfgAssembly * pPickedItem, const CATSafeArrayVariant & vbSAV)=0;

    virtual HRESULT __stdcall RemovePlacedMfgAssembly(DNBIAMfgAssembly * pPickedItem)=0;


};

CATDeclareHandler(DNBIAPlace, DNBIAWorkerActivity);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIAActivity.h"
#include "CATIAAnalyze.h"
#include "CATIABase.h"
#include "CATIACollection.h"
#include "CATIAItems.h"
#include "CATIAMove.h"
#include "CATIAOutputs.h"
#include "CATIAParameters.h"
#include "CATIAPosition.h"
#include "CATIAProduct.h"
#include "CATIAPublications.h"
#include "CATIARelations.h"
#include "CATSPPDeclarations.h"
#include "CATVariant.h"
#include "CatFileType.h"
#include "CatProductSource.h"
#include "CatRepType.h"
#include "CatWorkModeType.h"
#include "DNBHumanSimDefs.h"
#include "DNBIAHumanTask.h"
#include "DNBIAMfgAssembly.h"
#include "DNBIAMfgAssemblyType.h"
#include "DNBItemAssignmentType.h"
#include "IDispatch.h"
#include "IUnknown.h"
#include "SWKIAManikin.h"
#include "SWKIAManikinPart.h"


#endif
