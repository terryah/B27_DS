/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef DNBIAHtsGeneralSettingAtt_h
#define DNBIAHtsGeneralSettingAtt_h

#include "CATCORBABoolean.h"
#include "CAT_VARIANT_BOOL.h"

#ifndef ExportedByDNBHumanSimItfPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __DNBHumanSimItfPubIDL
#define ExportedByDNBHumanSimItfPubIDL __declspec(dllexport)
#else
#define ExportedByDNBHumanSimItfPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByDNBHumanSimItfPubIDL
#endif
#endif

#include "CATBSTR.h"
#include "CATIASettingController.h"

extern ExportedByDNBHumanSimItfPubIDL IID IID_DNBIAHtsGeneralSettingAtt;

class ExportedByDNBHumanSimItfPubIDL DNBIAHtsGeneralSettingAtt : public CATIASettingController
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_ConstraintsSimul(short & oConstraintsSimul)=0;

    virtual HRESULT __stdcall put_ConstraintsSimul(short iConstraintsSimul)=0;

    virtual HRESULT __stdcall GetConstraintsSimulInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetConstraintsSimulLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_UpdateAnalysisInSimulation(short & oUpdateAnalysisInSimulation)=0;

    virtual HRESULT __stdcall put_UpdateAnalysisInSimulation(short iUpdateAnalysisInSimulation)=0;

    virtual HRESULT __stdcall GetUpdateAnalysisInSimulationInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetUpdateAnalysisInSimulationLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_UserWalkSpeedStatus(short & oUserWalkSpeedStatus)=0;

    virtual HRESULT __stdcall put_UserWalkSpeedStatus(short iUserWalkSpeedStatus)=0;

    virtual HRESULT __stdcall GetUserWalkSpeedStatusInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetUserWalkSpeedStatusLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_WalkForwardDistance(double & oWalkForwardDistance)=0;

    virtual HRESULT __stdcall put_WalkForwardDistance(double iWalkForwardDistance)=0;

    virtual HRESULT __stdcall GetWalkForwardDistanceInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetWalkForwardDistanceLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_WalkForwardAngle(double & oWalkForwardAngle)=0;

    virtual HRESULT __stdcall put_WalkForwardAngle(double iWalkForwardAngle)=0;

    virtual HRESULT __stdcall GetWalkForwardAngleInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetWalkForwardAngleLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_SideStepAngle(double & oSideStepAngle)=0;

    virtual HRESULT __stdcall put_SideStepAngle(double iSideStepAngle)=0;

    virtual HRESULT __stdcall GetSideStepAngleInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetSideStepAngleLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_DefaultWalkSpeed(double & WalkSpeed)=0;

    virtual HRESULT __stdcall put_DefaultWalkSpeed(double WalkSpeed)=0;

    virtual HRESULT __stdcall GetDefaultWalkSpeedInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetDefaultWalkSpeedLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_SyncTimeAndSpeedInSimulation(short & oUpdateAnalysisInSimulation)=0;

    virtual HRESULT __stdcall put_SyncTimeAndSpeedInSimulation(short iUpdateAnalysisInSimulation)=0;

    virtual HRESULT __stdcall GetSyncTimeAndSpeedInSimulationInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetSyncTimeAndSpeedInSimulationLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_CollisionSearchIntensity(short & oUpdateAnalysisInSimulation)=0;

    virtual HRESULT __stdcall put_CollisionSearchIntensity(short iUpdateAnalysisInSimulation)=0;

    virtual HRESULT __stdcall GetCollisionSearchIntensityInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetCollisionSearchIntensityLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_CollWalkClearance(double & oUpdateAnalysisInSimulation)=0;

    virtual HRESULT __stdcall put_CollWalkClearance(double iUpdateAnalysisInSimulation)=0;

    virtual HRESULT __stdcall GetCollWalkClearanceInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetCollWalkClearanceLock(CAT_VARIANT_BOOL iLocked)=0;


};

CATDeclareHandler(DNBIAHtsGeneralSettingAtt, CATIASettingController);

#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIABase.h"
#include "CATSafeArray.h"
#include "CATVariant.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
