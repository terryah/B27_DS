/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef DNBIAWalk_h
#define DNBIAWalk_h

#include "CATCORBABoolean.h"
#include "CAT_VARIANT_BOOL.h"

#ifndef ExportedByDNBHumanSimItfPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __DNBHumanSimItfPubIDL
#define ExportedByDNBHumanSimItfPubIDL __declspec(dllexport)
#else
#define ExportedByDNBHumanSimItfPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByDNBHumanSimItfPubIDL
#endif
#endif

#include "CATSafeArray.h"
#include "DNBHumanSimDefs.h"
#include "DNBIAWorkerActivity.h"

extern ExportedByDNBHumanSimItfPubIDL IID IID_DNBIAWalk;

class ExportedByDNBHumanSimItfPubIDL DNBIAWalk : public DNBIAWorkerActivity
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_Swing(HTSSwingOptions & eOption)=0;

    virtual HRESULT __stdcall put_Swing(HTSSwingOptions eOption)=0;

    virtual HRESULT __stdcall get_Stride(HTSStrideOptions & eOption)=0;

    virtual HRESULT __stdcall put_Stride(HTSStrideOptions eOption)=0;

    virtual HRESULT __stdcall get_BodyPose(HTSBodyPoseOptions & eOption)=0;

    virtual HRESULT __stdcall put_BodyPose(HTSBodyPoseOptions eOption)=0;

    virtual HRESULT __stdcall get_MotionBasis(HTSWalkMotionBasis & eOption)=0;

    virtual HRESULT __stdcall put_MotionBasis(HTSWalkMotionBasis eOption)=0;

    virtual HRESULT __stdcall get_UserTime(double & dValue)=0;

    virtual HRESULT __stdcall put_UserTime(double dValue)=0;

    virtual HRESULT __stdcall get_UserSpeed(double & dValue)=0;

    virtual HRESULT __stdcall put_UserSpeed(double dValue)=0;

    virtual HRESULT __stdcall get_WalkLength(double & dValue)=0;

    virtual HRESULT __stdcall SetWalkCurveDefPoints(CATLONG iNumPoints, const CATSafeArrayVariant & adPoints)=0;

    virtual HRESULT __stdcall GetWalkCurveDefPoints(CATLONG & iNumPoints, CATSafeArrayVariant & adPoints)=0;

    virtual HRESULT __stdcall HasPartRelation(CAT_VARIANT_BOOL & bFlag)=0;

    virtual HRESULT __stdcall Update()=0;


};

CATDeclareHandler(DNBIAWalk, DNBIAWorkerActivity);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIAActivity.h"
#include "CATIAAnalyze.h"
#include "CATIABase.h"
#include "CATIACollection.h"
#include "CATIAItems.h"
#include "CATIAMove.h"
#include "CATIAOutputs.h"
#include "CATIAParameters.h"
#include "CATIAPosition.h"
#include "CATIAProduct.h"
#include "CATIAPublications.h"
#include "CATIARelations.h"
#include "CATSPPDeclarations.h"
#include "CATVariant.h"
#include "CatFileType.h"
#include "CatProductSource.h"
#include "CatRepType.h"
#include "CatWorkModeType.h"
#include "DNBIAHumanTask.h"
#include "DNBItemAssignmentType.h"
#include "IDispatch.h"
#include "IUnknown.h"
#include "SWKIAManikin.h"
#include "SWKIAManikinPart.h"


#endif
