#ifndef __TIE_DNBIAHtsTaskDisplaySettingAtt
#define __TIE_DNBIAHtsTaskDisplaySettingAtt

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIAHtsTaskDisplaySettingAtt.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIAHtsTaskDisplaySettingAtt */
#define declare_TIE_DNBIAHtsTaskDisplaySettingAtt(classe) \
 \
 \
class TIEDNBIAHtsTaskDisplaySettingAtt##classe : public DNBIAHtsTaskDisplaySettingAtt \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIAHtsTaskDisplaySettingAtt, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall get_WalkActLineType(CATLONG & oWalkActLineType); \
      virtual HRESULT __stdcall put_WalkActLineType(CATLONG iWalkActLineType); \
      virtual HRESULT __stdcall GetWalkActLineTypeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetWalkActLineTypeLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_AutoWalkActLineType(CATLONG & oAutoWalkActLineType); \
      virtual HRESULT __stdcall put_AutoWalkActLineType(CATLONG iAutoWalkActLineType); \
      virtual HRESULT __stdcall GetAutoWalkActLineTypeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetAutoWalkActLineTypeLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_WalkActLineWeight(CATLONG & oWalkActLineWeight); \
      virtual HRESULT __stdcall put_WalkActLineWeight(CATLONG iWalkActLineWeight); \
      virtual HRESULT __stdcall GetWalkActLineWeightInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetWalkActLineWeightLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_AutoWalkActLineWeight(CATLONG & oAutoWalkActLineWeight); \
      virtual HRESULT __stdcall put_AutoWalkActLineWeight(CATLONG iAutoWalkActLineWeight); \
      virtual HRESULT __stdcall GetAutoWalkActLineWeightInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetAutoWalkActLineWeightLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_MTPActSymbol(CATLONG & oMTPActSymbol); \
      virtual HRESULT __stdcall put_MTPActSymbol(CATLONG iMTPActSymbol); \
      virtual HRESULT __stdcall GetMTPActSymbolInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetMTPActSymbolLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_MTPActColor(CATLONG & oMTPActColor); \
      virtual HRESULT __stdcall put_MTPActColor(CATLONG iMTPActColor); \
      virtual HRESULT __stdcall GetMTPActColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetMTPActColorLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_WalkActColor(CATLONG & oWalkActColor); \
      virtual HRESULT __stdcall put_WalkActColor(CATLONG iWalkActColor); \
      virtual HRESULT __stdcall GetWalkActColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetWalkActColorLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_AutoWalkActColor(CATLONG & oAutoWalkActColor); \
      virtual HRESULT __stdcall put_AutoWalkActColor(CATLONG iAutoWalkActColor); \
      virtual HRESULT __stdcall GetAutoWalkActColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetAutoWalkActColorLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall Commit(); \
      virtual HRESULT __stdcall Rollback(); \
      virtual HRESULT __stdcall ResetToAdminValues(); \
      virtual HRESULT __stdcall ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList); \
      virtual HRESULT __stdcall SaveRepository(); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIAHtsTaskDisplaySettingAtt(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall get_WalkActLineType(CATLONG & oWalkActLineType); \
virtual HRESULT __stdcall put_WalkActLineType(CATLONG iWalkActLineType); \
virtual HRESULT __stdcall GetWalkActLineTypeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetWalkActLineTypeLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_AutoWalkActLineType(CATLONG & oAutoWalkActLineType); \
virtual HRESULT __stdcall put_AutoWalkActLineType(CATLONG iAutoWalkActLineType); \
virtual HRESULT __stdcall GetAutoWalkActLineTypeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetAutoWalkActLineTypeLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_WalkActLineWeight(CATLONG & oWalkActLineWeight); \
virtual HRESULT __stdcall put_WalkActLineWeight(CATLONG iWalkActLineWeight); \
virtual HRESULT __stdcall GetWalkActLineWeightInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetWalkActLineWeightLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_AutoWalkActLineWeight(CATLONG & oAutoWalkActLineWeight); \
virtual HRESULT __stdcall put_AutoWalkActLineWeight(CATLONG iAutoWalkActLineWeight); \
virtual HRESULT __stdcall GetAutoWalkActLineWeightInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetAutoWalkActLineWeightLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_MTPActSymbol(CATLONG & oMTPActSymbol); \
virtual HRESULT __stdcall put_MTPActSymbol(CATLONG iMTPActSymbol); \
virtual HRESULT __stdcall GetMTPActSymbolInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetMTPActSymbolLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_MTPActColor(CATLONG & oMTPActColor); \
virtual HRESULT __stdcall put_MTPActColor(CATLONG iMTPActColor); \
virtual HRESULT __stdcall GetMTPActColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetMTPActColorLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_WalkActColor(CATLONG & oWalkActColor); \
virtual HRESULT __stdcall put_WalkActColor(CATLONG iWalkActColor); \
virtual HRESULT __stdcall GetWalkActColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetWalkActColorLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_AutoWalkActColor(CATLONG & oAutoWalkActColor); \
virtual HRESULT __stdcall put_AutoWalkActColor(CATLONG iAutoWalkActColor); \
virtual HRESULT __stdcall GetAutoWalkActColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetAutoWalkActColorLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall Commit(); \
virtual HRESULT __stdcall Rollback(); \
virtual HRESULT __stdcall ResetToAdminValues(); \
virtual HRESULT __stdcall ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList); \
virtual HRESULT __stdcall SaveRepository(); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIAHtsTaskDisplaySettingAtt(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::get_WalkActLineType(CATLONG & oWalkActLineType) \
{ \
return (ENVTIECALL(DNBIAHtsTaskDisplaySettingAtt,ENVTIETypeLetter,ENVTIELetter)get_WalkActLineType(oWalkActLineType)); \
} \
HRESULT __stdcall  ENVTIEName::put_WalkActLineType(CATLONG iWalkActLineType) \
{ \
return (ENVTIECALL(DNBIAHtsTaskDisplaySettingAtt,ENVTIETypeLetter,ENVTIELetter)put_WalkActLineType(iWalkActLineType)); \
} \
HRESULT __stdcall  ENVTIEName::GetWalkActLineTypeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAHtsTaskDisplaySettingAtt,ENVTIETypeLetter,ENVTIELetter)GetWalkActLineTypeInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetWalkActLineTypeLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAHtsTaskDisplaySettingAtt,ENVTIETypeLetter,ENVTIELetter)SetWalkActLineTypeLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_AutoWalkActLineType(CATLONG & oAutoWalkActLineType) \
{ \
return (ENVTIECALL(DNBIAHtsTaskDisplaySettingAtt,ENVTIETypeLetter,ENVTIELetter)get_AutoWalkActLineType(oAutoWalkActLineType)); \
} \
HRESULT __stdcall  ENVTIEName::put_AutoWalkActLineType(CATLONG iAutoWalkActLineType) \
{ \
return (ENVTIECALL(DNBIAHtsTaskDisplaySettingAtt,ENVTIETypeLetter,ENVTIELetter)put_AutoWalkActLineType(iAutoWalkActLineType)); \
} \
HRESULT __stdcall  ENVTIEName::GetAutoWalkActLineTypeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAHtsTaskDisplaySettingAtt,ENVTIETypeLetter,ENVTIELetter)GetAutoWalkActLineTypeInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetAutoWalkActLineTypeLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAHtsTaskDisplaySettingAtt,ENVTIETypeLetter,ENVTIELetter)SetAutoWalkActLineTypeLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_WalkActLineWeight(CATLONG & oWalkActLineWeight) \
{ \
return (ENVTIECALL(DNBIAHtsTaskDisplaySettingAtt,ENVTIETypeLetter,ENVTIELetter)get_WalkActLineWeight(oWalkActLineWeight)); \
} \
HRESULT __stdcall  ENVTIEName::put_WalkActLineWeight(CATLONG iWalkActLineWeight) \
{ \
return (ENVTIECALL(DNBIAHtsTaskDisplaySettingAtt,ENVTIETypeLetter,ENVTIELetter)put_WalkActLineWeight(iWalkActLineWeight)); \
} \
HRESULT __stdcall  ENVTIEName::GetWalkActLineWeightInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAHtsTaskDisplaySettingAtt,ENVTIETypeLetter,ENVTIELetter)GetWalkActLineWeightInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetWalkActLineWeightLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAHtsTaskDisplaySettingAtt,ENVTIETypeLetter,ENVTIELetter)SetWalkActLineWeightLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_AutoWalkActLineWeight(CATLONG & oAutoWalkActLineWeight) \
{ \
return (ENVTIECALL(DNBIAHtsTaskDisplaySettingAtt,ENVTIETypeLetter,ENVTIELetter)get_AutoWalkActLineWeight(oAutoWalkActLineWeight)); \
} \
HRESULT __stdcall  ENVTIEName::put_AutoWalkActLineWeight(CATLONG iAutoWalkActLineWeight) \
{ \
return (ENVTIECALL(DNBIAHtsTaskDisplaySettingAtt,ENVTIETypeLetter,ENVTIELetter)put_AutoWalkActLineWeight(iAutoWalkActLineWeight)); \
} \
HRESULT __stdcall  ENVTIEName::GetAutoWalkActLineWeightInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAHtsTaskDisplaySettingAtt,ENVTIETypeLetter,ENVTIELetter)GetAutoWalkActLineWeightInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetAutoWalkActLineWeightLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAHtsTaskDisplaySettingAtt,ENVTIETypeLetter,ENVTIELetter)SetAutoWalkActLineWeightLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_MTPActSymbol(CATLONG & oMTPActSymbol) \
{ \
return (ENVTIECALL(DNBIAHtsTaskDisplaySettingAtt,ENVTIETypeLetter,ENVTIELetter)get_MTPActSymbol(oMTPActSymbol)); \
} \
HRESULT __stdcall  ENVTIEName::put_MTPActSymbol(CATLONG iMTPActSymbol) \
{ \
return (ENVTIECALL(DNBIAHtsTaskDisplaySettingAtt,ENVTIETypeLetter,ENVTIELetter)put_MTPActSymbol(iMTPActSymbol)); \
} \
HRESULT __stdcall  ENVTIEName::GetMTPActSymbolInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAHtsTaskDisplaySettingAtt,ENVTIETypeLetter,ENVTIELetter)GetMTPActSymbolInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetMTPActSymbolLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAHtsTaskDisplaySettingAtt,ENVTIETypeLetter,ENVTIELetter)SetMTPActSymbolLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_MTPActColor(CATLONG & oMTPActColor) \
{ \
return (ENVTIECALL(DNBIAHtsTaskDisplaySettingAtt,ENVTIETypeLetter,ENVTIELetter)get_MTPActColor(oMTPActColor)); \
} \
HRESULT __stdcall  ENVTIEName::put_MTPActColor(CATLONG iMTPActColor) \
{ \
return (ENVTIECALL(DNBIAHtsTaskDisplaySettingAtt,ENVTIETypeLetter,ENVTIELetter)put_MTPActColor(iMTPActColor)); \
} \
HRESULT __stdcall  ENVTIEName::GetMTPActColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAHtsTaskDisplaySettingAtt,ENVTIETypeLetter,ENVTIELetter)GetMTPActColorInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetMTPActColorLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAHtsTaskDisplaySettingAtt,ENVTIETypeLetter,ENVTIELetter)SetMTPActColorLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_WalkActColor(CATLONG & oWalkActColor) \
{ \
return (ENVTIECALL(DNBIAHtsTaskDisplaySettingAtt,ENVTIETypeLetter,ENVTIELetter)get_WalkActColor(oWalkActColor)); \
} \
HRESULT __stdcall  ENVTIEName::put_WalkActColor(CATLONG iWalkActColor) \
{ \
return (ENVTIECALL(DNBIAHtsTaskDisplaySettingAtt,ENVTIETypeLetter,ENVTIELetter)put_WalkActColor(iWalkActColor)); \
} \
HRESULT __stdcall  ENVTIEName::GetWalkActColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAHtsTaskDisplaySettingAtt,ENVTIETypeLetter,ENVTIELetter)GetWalkActColorInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetWalkActColorLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAHtsTaskDisplaySettingAtt,ENVTIETypeLetter,ENVTIELetter)SetWalkActColorLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_AutoWalkActColor(CATLONG & oAutoWalkActColor) \
{ \
return (ENVTIECALL(DNBIAHtsTaskDisplaySettingAtt,ENVTIETypeLetter,ENVTIELetter)get_AutoWalkActColor(oAutoWalkActColor)); \
} \
HRESULT __stdcall  ENVTIEName::put_AutoWalkActColor(CATLONG iAutoWalkActColor) \
{ \
return (ENVTIECALL(DNBIAHtsTaskDisplaySettingAtt,ENVTIETypeLetter,ENVTIELetter)put_AutoWalkActColor(iAutoWalkActColor)); \
} \
HRESULT __stdcall  ENVTIEName::GetAutoWalkActColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAHtsTaskDisplaySettingAtt,ENVTIETypeLetter,ENVTIELetter)GetAutoWalkActColorInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetAutoWalkActColorLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAHtsTaskDisplaySettingAtt,ENVTIETypeLetter,ENVTIELetter)SetAutoWalkActColorLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::Commit() \
{ \
return (ENVTIECALL(DNBIAHtsTaskDisplaySettingAtt,ENVTIETypeLetter,ENVTIELetter)Commit()); \
} \
HRESULT __stdcall  ENVTIEName::Rollback() \
{ \
return (ENVTIECALL(DNBIAHtsTaskDisplaySettingAtt,ENVTIETypeLetter,ENVTIELetter)Rollback()); \
} \
HRESULT __stdcall  ENVTIEName::ResetToAdminValues() \
{ \
return (ENVTIECALL(DNBIAHtsTaskDisplaySettingAtt,ENVTIETypeLetter,ENVTIELetter)ResetToAdminValues()); \
} \
HRESULT __stdcall  ENVTIEName::ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList) \
{ \
return (ENVTIECALL(DNBIAHtsTaskDisplaySettingAtt,ENVTIETypeLetter,ENVTIELetter)ResetToAdminValuesByName(iAttList)); \
} \
HRESULT __stdcall  ENVTIEName::SaveRepository() \
{ \
return (ENVTIECALL(DNBIAHtsTaskDisplaySettingAtt,ENVTIETypeLetter,ENVTIELetter)SaveRepository()); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIAHtsTaskDisplaySettingAtt,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIAHtsTaskDisplaySettingAtt,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIAHtsTaskDisplaySettingAtt,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIAHtsTaskDisplaySettingAtt,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIAHtsTaskDisplaySettingAtt,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIAHtsTaskDisplaySettingAtt(classe)    TIEDNBIAHtsTaskDisplaySettingAtt##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIAHtsTaskDisplaySettingAtt(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIAHtsTaskDisplaySettingAtt, classe) \
 \
 \
CATImplementTIEMethods(DNBIAHtsTaskDisplaySettingAtt, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIAHtsTaskDisplaySettingAtt, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIAHtsTaskDisplaySettingAtt, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIAHtsTaskDisplaySettingAtt, classe) \
 \
HRESULT __stdcall  TIEDNBIAHtsTaskDisplaySettingAtt##classe::get_WalkActLineType(CATLONG & oWalkActLineType) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&oWalkActLineType); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_WalkActLineType(oWalkActLineType); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&oWalkActLineType); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsTaskDisplaySettingAtt##classe::put_WalkActLineType(CATLONG iWalkActLineType) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&iWalkActLineType); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_WalkActLineType(iWalkActLineType); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&iWalkActLineType); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsTaskDisplaySettingAtt##classe::GetWalkActLineTypeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetWalkActLineTypeInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsTaskDisplaySettingAtt##classe::SetWalkActLineTypeLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetWalkActLineTypeLock(iLocked); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsTaskDisplaySettingAtt##classe::get_AutoWalkActLineType(CATLONG & oAutoWalkActLineType) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&oAutoWalkActLineType); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_AutoWalkActLineType(oAutoWalkActLineType); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&oAutoWalkActLineType); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsTaskDisplaySettingAtt##classe::put_AutoWalkActLineType(CATLONG iAutoWalkActLineType) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&iAutoWalkActLineType); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_AutoWalkActLineType(iAutoWalkActLineType); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&iAutoWalkActLineType); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsTaskDisplaySettingAtt##classe::GetAutoWalkActLineTypeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAutoWalkActLineTypeInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsTaskDisplaySettingAtt##classe::SetAutoWalkActLineTypeLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetAutoWalkActLineTypeLock(iLocked); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsTaskDisplaySettingAtt##classe::get_WalkActLineWeight(CATLONG & oWalkActLineWeight) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&oWalkActLineWeight); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_WalkActLineWeight(oWalkActLineWeight); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&oWalkActLineWeight); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsTaskDisplaySettingAtt##classe::put_WalkActLineWeight(CATLONG iWalkActLineWeight) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&iWalkActLineWeight); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_WalkActLineWeight(iWalkActLineWeight); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&iWalkActLineWeight); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsTaskDisplaySettingAtt##classe::GetWalkActLineWeightInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,11,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetWalkActLineWeightInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,11,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsTaskDisplaySettingAtt##classe::SetWalkActLineWeightLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,12,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetWalkActLineWeightLock(iLocked); \
   ExitAfterCall(this,12,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsTaskDisplaySettingAtt##classe::get_AutoWalkActLineWeight(CATLONG & oAutoWalkActLineWeight) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,13,&_Trac2,&oAutoWalkActLineWeight); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_AutoWalkActLineWeight(oAutoWalkActLineWeight); \
   ExitAfterCall(this,13,_Trac2,&_ret_arg,&oAutoWalkActLineWeight); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsTaskDisplaySettingAtt##classe::put_AutoWalkActLineWeight(CATLONG iAutoWalkActLineWeight) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,14,&_Trac2,&iAutoWalkActLineWeight); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_AutoWalkActLineWeight(iAutoWalkActLineWeight); \
   ExitAfterCall(this,14,_Trac2,&_ret_arg,&iAutoWalkActLineWeight); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsTaskDisplaySettingAtt##classe::GetAutoWalkActLineWeightInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,15,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAutoWalkActLineWeightInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,15,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsTaskDisplaySettingAtt##classe::SetAutoWalkActLineWeightLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,16,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetAutoWalkActLineWeightLock(iLocked); \
   ExitAfterCall(this,16,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsTaskDisplaySettingAtt##classe::get_MTPActSymbol(CATLONG & oMTPActSymbol) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,17,&_Trac2,&oMTPActSymbol); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_MTPActSymbol(oMTPActSymbol); \
   ExitAfterCall(this,17,_Trac2,&_ret_arg,&oMTPActSymbol); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsTaskDisplaySettingAtt##classe::put_MTPActSymbol(CATLONG iMTPActSymbol) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,18,&_Trac2,&iMTPActSymbol); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_MTPActSymbol(iMTPActSymbol); \
   ExitAfterCall(this,18,_Trac2,&_ret_arg,&iMTPActSymbol); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsTaskDisplaySettingAtt##classe::GetMTPActSymbolInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,19,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetMTPActSymbolInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,19,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsTaskDisplaySettingAtt##classe::SetMTPActSymbolLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,20,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetMTPActSymbolLock(iLocked); \
   ExitAfterCall(this,20,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsTaskDisplaySettingAtt##classe::get_MTPActColor(CATLONG & oMTPActColor) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,21,&_Trac2,&oMTPActColor); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_MTPActColor(oMTPActColor); \
   ExitAfterCall(this,21,_Trac2,&_ret_arg,&oMTPActColor); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsTaskDisplaySettingAtt##classe::put_MTPActColor(CATLONG iMTPActColor) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,22,&_Trac2,&iMTPActColor); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_MTPActColor(iMTPActColor); \
   ExitAfterCall(this,22,_Trac2,&_ret_arg,&iMTPActColor); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsTaskDisplaySettingAtt##classe::GetMTPActColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,23,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetMTPActColorInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,23,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsTaskDisplaySettingAtt##classe::SetMTPActColorLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,24,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetMTPActColorLock(iLocked); \
   ExitAfterCall(this,24,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsTaskDisplaySettingAtt##classe::get_WalkActColor(CATLONG & oWalkActColor) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,25,&_Trac2,&oWalkActColor); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_WalkActColor(oWalkActColor); \
   ExitAfterCall(this,25,_Trac2,&_ret_arg,&oWalkActColor); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsTaskDisplaySettingAtt##classe::put_WalkActColor(CATLONG iWalkActColor) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,26,&_Trac2,&iWalkActColor); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_WalkActColor(iWalkActColor); \
   ExitAfterCall(this,26,_Trac2,&_ret_arg,&iWalkActColor); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsTaskDisplaySettingAtt##classe::GetWalkActColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,27,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetWalkActColorInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,27,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsTaskDisplaySettingAtt##classe::SetWalkActColorLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,28,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetWalkActColorLock(iLocked); \
   ExitAfterCall(this,28,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsTaskDisplaySettingAtt##classe::get_AutoWalkActColor(CATLONG & oAutoWalkActColor) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,29,&_Trac2,&oAutoWalkActColor); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_AutoWalkActColor(oAutoWalkActColor); \
   ExitAfterCall(this,29,_Trac2,&_ret_arg,&oAutoWalkActColor); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsTaskDisplaySettingAtt##classe::put_AutoWalkActColor(CATLONG iAutoWalkActColor) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,30,&_Trac2,&iAutoWalkActColor); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_AutoWalkActColor(iAutoWalkActColor); \
   ExitAfterCall(this,30,_Trac2,&_ret_arg,&iAutoWalkActColor); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsTaskDisplaySettingAtt##classe::GetAutoWalkActColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,31,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAutoWalkActColorInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,31,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsTaskDisplaySettingAtt##classe::SetAutoWalkActColorLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,32,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetAutoWalkActColorLock(iLocked); \
   ExitAfterCall(this,32,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsTaskDisplaySettingAtt##classe::Commit() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,33,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Commit(); \
   ExitAfterCall(this,33,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsTaskDisplaySettingAtt##classe::Rollback() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,34,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Rollback(); \
   ExitAfterCall(this,34,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsTaskDisplaySettingAtt##classe::ResetToAdminValues() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,35,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ResetToAdminValues(); \
   ExitAfterCall(this,35,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsTaskDisplaySettingAtt##classe::ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,36,&_Trac2,&iAttList); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ResetToAdminValuesByName(iAttList); \
   ExitAfterCall(this,36,_Trac2,&_ret_arg,&iAttList); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsTaskDisplaySettingAtt##classe::SaveRepository() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,37,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SaveRepository(); \
   ExitAfterCall(this,37,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAHtsTaskDisplaySettingAtt##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,38,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,38,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAHtsTaskDisplaySettingAtt##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,39,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,39,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAHtsTaskDisplaySettingAtt##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,40,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,40,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAHtsTaskDisplaySettingAtt##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,41,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,41,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAHtsTaskDisplaySettingAtt##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,42,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,42,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIAHtsTaskDisplaySettingAtt(classe) \
 \
 \
declare_TIE_DNBIAHtsTaskDisplaySettingAtt(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAHtsTaskDisplaySettingAtt##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAHtsTaskDisplaySettingAtt,"DNBIAHtsTaskDisplaySettingAtt",DNBIAHtsTaskDisplaySettingAtt::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAHtsTaskDisplaySettingAtt(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIAHtsTaskDisplaySettingAtt, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAHtsTaskDisplaySettingAtt##classe(classe::MetaObject(),DNBIAHtsTaskDisplaySettingAtt::MetaObject(),(void *)CreateTIEDNBIAHtsTaskDisplaySettingAtt##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIAHtsTaskDisplaySettingAtt(classe) \
 \
 \
declare_TIE_DNBIAHtsTaskDisplaySettingAtt(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAHtsTaskDisplaySettingAtt##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAHtsTaskDisplaySettingAtt,"DNBIAHtsTaskDisplaySettingAtt",DNBIAHtsTaskDisplaySettingAtt::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAHtsTaskDisplaySettingAtt(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIAHtsTaskDisplaySettingAtt, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAHtsTaskDisplaySettingAtt##classe(classe::MetaObject(),DNBIAHtsTaskDisplaySettingAtt::MetaObject(),(void *)CreateTIEDNBIAHtsTaskDisplaySettingAtt##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIAHtsTaskDisplaySettingAtt(classe) TIE_DNBIAHtsTaskDisplaySettingAtt(classe)
#else
#define BOA_DNBIAHtsTaskDisplaySettingAtt(classe) CATImplementBOA(DNBIAHtsTaskDisplaySettingAtt, classe)
#endif

#endif
