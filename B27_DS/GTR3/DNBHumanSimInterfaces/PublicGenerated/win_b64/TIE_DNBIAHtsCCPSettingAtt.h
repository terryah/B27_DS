#ifndef __TIE_DNBIAHtsCCPSettingAtt
#define __TIE_DNBIAHtsCCPSettingAtt

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIAHtsCCPSettingAtt.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIAHtsCCPSettingAtt */
#define declare_TIE_DNBIAHtsCCPSettingAtt(classe) \
 \
 \
class TIEDNBIAHtsCCPSettingAtt##classe : public DNBIAHtsCCPSettingAtt \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIAHtsCCPSettingAtt, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall get_CompassPos(short & oCompassPos); \
      virtual HRESULT __stdcall put_CompassPos(short iCompassPos); \
      virtual HRESULT __stdcall GetCompassPosInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetCompassPosLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_CurrentPos(short & oCurrentPos); \
      virtual HRESULT __stdcall put_CurrentPos(short iCurrentPos); \
      virtual HRESULT __stdcall GetCurrentPosInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetCurrentPosLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_FirstAct(short & oFirstAct); \
      virtual HRESULT __stdcall put_FirstAct(short iFirstAct); \
      virtual HRESULT __stdcall GetFirstActInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetFirstActLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_LastAct(short & oLastAct); \
      virtual HRESULT __stdcall put_LastAct(short iLastAct); \
      virtual HRESULT __stdcall GetLastActInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetLastActLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_AutomatizedCCP(short & oAutoCCP); \
      virtual HRESULT __stdcall put_AutomatizedCCP(short iAutoCCP); \
      virtual HRESULT __stdcall GetAutoCCPInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetAutoCCPLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall Commit(); \
      virtual HRESULT __stdcall Rollback(); \
      virtual HRESULT __stdcall ResetToAdminValues(); \
      virtual HRESULT __stdcall ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList); \
      virtual HRESULT __stdcall SaveRepository(); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIAHtsCCPSettingAtt(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall get_CompassPos(short & oCompassPos); \
virtual HRESULT __stdcall put_CompassPos(short iCompassPos); \
virtual HRESULT __stdcall GetCompassPosInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetCompassPosLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_CurrentPos(short & oCurrentPos); \
virtual HRESULT __stdcall put_CurrentPos(short iCurrentPos); \
virtual HRESULT __stdcall GetCurrentPosInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetCurrentPosLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_FirstAct(short & oFirstAct); \
virtual HRESULT __stdcall put_FirstAct(short iFirstAct); \
virtual HRESULT __stdcall GetFirstActInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetFirstActLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_LastAct(short & oLastAct); \
virtual HRESULT __stdcall put_LastAct(short iLastAct); \
virtual HRESULT __stdcall GetLastActInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetLastActLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_AutomatizedCCP(short & oAutoCCP); \
virtual HRESULT __stdcall put_AutomatizedCCP(short iAutoCCP); \
virtual HRESULT __stdcall GetAutoCCPInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetAutoCCPLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall Commit(); \
virtual HRESULT __stdcall Rollback(); \
virtual HRESULT __stdcall ResetToAdminValues(); \
virtual HRESULT __stdcall ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList); \
virtual HRESULT __stdcall SaveRepository(); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIAHtsCCPSettingAtt(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::get_CompassPos(short & oCompassPos) \
{ \
return (ENVTIECALL(DNBIAHtsCCPSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_CompassPos(oCompassPos)); \
} \
HRESULT __stdcall  ENVTIEName::put_CompassPos(short iCompassPos) \
{ \
return (ENVTIECALL(DNBIAHtsCCPSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_CompassPos(iCompassPos)); \
} \
HRESULT __stdcall  ENVTIEName::GetCompassPosInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAHtsCCPSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetCompassPosInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetCompassPosLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAHtsCCPSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetCompassPosLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_CurrentPos(short & oCurrentPos) \
{ \
return (ENVTIECALL(DNBIAHtsCCPSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_CurrentPos(oCurrentPos)); \
} \
HRESULT __stdcall  ENVTIEName::put_CurrentPos(short iCurrentPos) \
{ \
return (ENVTIECALL(DNBIAHtsCCPSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_CurrentPos(iCurrentPos)); \
} \
HRESULT __stdcall  ENVTIEName::GetCurrentPosInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAHtsCCPSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetCurrentPosInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetCurrentPosLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAHtsCCPSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetCurrentPosLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_FirstAct(short & oFirstAct) \
{ \
return (ENVTIECALL(DNBIAHtsCCPSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_FirstAct(oFirstAct)); \
} \
HRESULT __stdcall  ENVTIEName::put_FirstAct(short iFirstAct) \
{ \
return (ENVTIECALL(DNBIAHtsCCPSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_FirstAct(iFirstAct)); \
} \
HRESULT __stdcall  ENVTIEName::GetFirstActInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAHtsCCPSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetFirstActInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetFirstActLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAHtsCCPSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetFirstActLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_LastAct(short & oLastAct) \
{ \
return (ENVTIECALL(DNBIAHtsCCPSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_LastAct(oLastAct)); \
} \
HRESULT __stdcall  ENVTIEName::put_LastAct(short iLastAct) \
{ \
return (ENVTIECALL(DNBIAHtsCCPSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_LastAct(iLastAct)); \
} \
HRESULT __stdcall  ENVTIEName::GetLastActInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAHtsCCPSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetLastActInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetLastActLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAHtsCCPSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetLastActLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_AutomatizedCCP(short & oAutoCCP) \
{ \
return (ENVTIECALL(DNBIAHtsCCPSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_AutomatizedCCP(oAutoCCP)); \
} \
HRESULT __stdcall  ENVTIEName::put_AutomatizedCCP(short iAutoCCP) \
{ \
return (ENVTIECALL(DNBIAHtsCCPSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_AutomatizedCCP(iAutoCCP)); \
} \
HRESULT __stdcall  ENVTIEName::GetAutoCCPInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAHtsCCPSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetAutoCCPInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetAutoCCPLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAHtsCCPSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetAutoCCPLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::Commit() \
{ \
return (ENVTIECALL(DNBIAHtsCCPSettingAtt,ENVTIETypeLetter,ENVTIELetter)Commit()); \
} \
HRESULT __stdcall  ENVTIEName::Rollback() \
{ \
return (ENVTIECALL(DNBIAHtsCCPSettingAtt,ENVTIETypeLetter,ENVTIELetter)Rollback()); \
} \
HRESULT __stdcall  ENVTIEName::ResetToAdminValues() \
{ \
return (ENVTIECALL(DNBIAHtsCCPSettingAtt,ENVTIETypeLetter,ENVTIELetter)ResetToAdminValues()); \
} \
HRESULT __stdcall  ENVTIEName::ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList) \
{ \
return (ENVTIECALL(DNBIAHtsCCPSettingAtt,ENVTIETypeLetter,ENVTIELetter)ResetToAdminValuesByName(iAttList)); \
} \
HRESULT __stdcall  ENVTIEName::SaveRepository() \
{ \
return (ENVTIECALL(DNBIAHtsCCPSettingAtt,ENVTIETypeLetter,ENVTIELetter)SaveRepository()); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIAHtsCCPSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIAHtsCCPSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIAHtsCCPSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIAHtsCCPSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIAHtsCCPSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIAHtsCCPSettingAtt(classe)    TIEDNBIAHtsCCPSettingAtt##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIAHtsCCPSettingAtt(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIAHtsCCPSettingAtt, classe) \
 \
 \
CATImplementTIEMethods(DNBIAHtsCCPSettingAtt, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIAHtsCCPSettingAtt, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIAHtsCCPSettingAtt, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIAHtsCCPSettingAtt, classe) \
 \
HRESULT __stdcall  TIEDNBIAHtsCCPSettingAtt##classe::get_CompassPos(short & oCompassPos) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&oCompassPos); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_CompassPos(oCompassPos); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&oCompassPos); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsCCPSettingAtt##classe::put_CompassPos(short iCompassPos) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&iCompassPos); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_CompassPos(iCompassPos); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&iCompassPos); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsCCPSettingAtt##classe::GetCompassPosInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetCompassPosInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsCCPSettingAtt##classe::SetCompassPosLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetCompassPosLock(iLocked); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsCCPSettingAtt##classe::get_CurrentPos(short & oCurrentPos) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&oCurrentPos); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_CurrentPos(oCurrentPos); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&oCurrentPos); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsCCPSettingAtt##classe::put_CurrentPos(short iCurrentPos) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&iCurrentPos); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_CurrentPos(iCurrentPos); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&iCurrentPos); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsCCPSettingAtt##classe::GetCurrentPosInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetCurrentPosInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsCCPSettingAtt##classe::SetCurrentPosLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetCurrentPosLock(iLocked); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsCCPSettingAtt##classe::get_FirstAct(short & oFirstAct) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&oFirstAct); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_FirstAct(oFirstAct); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&oFirstAct); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsCCPSettingAtt##classe::put_FirstAct(short iFirstAct) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&iFirstAct); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_FirstAct(iFirstAct); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&iFirstAct); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsCCPSettingAtt##classe::GetFirstActInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,11,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetFirstActInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,11,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsCCPSettingAtt##classe::SetFirstActLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,12,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetFirstActLock(iLocked); \
   ExitAfterCall(this,12,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsCCPSettingAtt##classe::get_LastAct(short & oLastAct) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,13,&_Trac2,&oLastAct); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_LastAct(oLastAct); \
   ExitAfterCall(this,13,_Trac2,&_ret_arg,&oLastAct); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsCCPSettingAtt##classe::put_LastAct(short iLastAct) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,14,&_Trac2,&iLastAct); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_LastAct(iLastAct); \
   ExitAfterCall(this,14,_Trac2,&_ret_arg,&iLastAct); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsCCPSettingAtt##classe::GetLastActInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,15,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetLastActInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,15,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsCCPSettingAtt##classe::SetLastActLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,16,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetLastActLock(iLocked); \
   ExitAfterCall(this,16,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsCCPSettingAtt##classe::get_AutomatizedCCP(short & oAutoCCP) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,17,&_Trac2,&oAutoCCP); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_AutomatizedCCP(oAutoCCP); \
   ExitAfterCall(this,17,_Trac2,&_ret_arg,&oAutoCCP); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsCCPSettingAtt##classe::put_AutomatizedCCP(short iAutoCCP) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,18,&_Trac2,&iAutoCCP); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_AutomatizedCCP(iAutoCCP); \
   ExitAfterCall(this,18,_Trac2,&_ret_arg,&iAutoCCP); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsCCPSettingAtt##classe::GetAutoCCPInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,19,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAutoCCPInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,19,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsCCPSettingAtt##classe::SetAutoCCPLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,20,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetAutoCCPLock(iLocked); \
   ExitAfterCall(this,20,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsCCPSettingAtt##classe::Commit() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,21,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Commit(); \
   ExitAfterCall(this,21,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsCCPSettingAtt##classe::Rollback() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,22,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Rollback(); \
   ExitAfterCall(this,22,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsCCPSettingAtt##classe::ResetToAdminValues() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,23,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ResetToAdminValues(); \
   ExitAfterCall(this,23,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsCCPSettingAtt##classe::ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,24,&_Trac2,&iAttList); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ResetToAdminValuesByName(iAttList); \
   ExitAfterCall(this,24,_Trac2,&_ret_arg,&iAttList); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHtsCCPSettingAtt##classe::SaveRepository() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,25,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SaveRepository(); \
   ExitAfterCall(this,25,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAHtsCCPSettingAtt##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,26,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,26,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAHtsCCPSettingAtt##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,27,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,27,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAHtsCCPSettingAtt##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,28,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,28,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAHtsCCPSettingAtt##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,29,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,29,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAHtsCCPSettingAtt##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,30,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,30,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIAHtsCCPSettingAtt(classe) \
 \
 \
declare_TIE_DNBIAHtsCCPSettingAtt(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAHtsCCPSettingAtt##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAHtsCCPSettingAtt,"DNBIAHtsCCPSettingAtt",DNBIAHtsCCPSettingAtt::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAHtsCCPSettingAtt(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIAHtsCCPSettingAtt, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAHtsCCPSettingAtt##classe(classe::MetaObject(),DNBIAHtsCCPSettingAtt::MetaObject(),(void *)CreateTIEDNBIAHtsCCPSettingAtt##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIAHtsCCPSettingAtt(classe) \
 \
 \
declare_TIE_DNBIAHtsCCPSettingAtt(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAHtsCCPSettingAtt##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAHtsCCPSettingAtt,"DNBIAHtsCCPSettingAtt",DNBIAHtsCCPSettingAtt::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAHtsCCPSettingAtt(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIAHtsCCPSettingAtt, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAHtsCCPSettingAtt##classe(classe::MetaObject(),DNBIAHtsCCPSettingAtt::MetaObject(),(void *)CreateTIEDNBIAHtsCCPSettingAtt##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIAHtsCCPSettingAtt(classe) TIE_DNBIAHtsCCPSettingAtt(classe)
#else
#define BOA_DNBIAHtsCCPSettingAtt(classe) CATImplementBOA(DNBIAHtsCCPSettingAtt, classe)
#endif

#endif
