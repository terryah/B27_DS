#ifndef __TIE_DNBIAHumanActsFactory
#define __TIE_DNBIAHumanActsFactory

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIAHumanActsFactory.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIAHumanActsFactory */
#define declare_TIE_DNBIAHumanActsFactory(classe) \
 \
 \
class TIEDNBIAHumanActsFactory##classe : public DNBIAHumanActsFactory \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIAHumanActsFactory, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall CreateMoveToPosture(CATIAActivity * iPrevAct, DNBIAMoveToPosture *& oCreatedMTP); \
      virtual HRESULT __stdcall CreateWalkFwdOnArrArea(CATIAActivity * iPrevAct, CATIAProduct * iArrArea, CATLONG iNumPoints, const CATSafeArrayVariant & iPoints, DNBIAWalk *& oCreatedWalk); \
      virtual HRESULT __stdcall CreateWalkFwdOnPlane(CATIAActivity * iPrevAct, CATIAProduct * iProd, const CATSafeArrayVariant & iPlaneDef, CATLONG iNumPoints, const CATSafeArrayVariant & iPoints, DNBIAWalk *& oCreatedWalk); \
      virtual HRESULT __stdcall CreateWalkBwdOnArrArea(CATIAActivity * iPrevAct, CATIAProduct * iArrArea, CATLONG iNumPoints, const CATSafeArrayVariant & iPoints, DNBIAWalk *& oCreatedWalk); \
      virtual HRESULT __stdcall CreateWalkBwdOnPlane(CATIAActivity * iPrevAct, CATIAProduct * iPlaneProd, const CATSafeArrayVariant & iPlaneDef, CATLONG iNumPoints, const CATSafeArrayVariant & iPoints, DNBIAWalk *& oCreatedWalk); \
      virtual HRESULT __stdcall CreateSideStepOnArrArea(CATIAActivity * iPrevAct, CATIAProduct * iArrArea, const CATSafeArrayVariant & iStartPt, const CATSafeArrayVariant & iEndPt, DNBIAWalk *& oCreatedWalk); \
      virtual HRESULT __stdcall CreateSideStepOnPlane(CATIAActivity * iPrevAct, CATIAProduct * iPlaneProd, const CATSafeArrayVariant & iPlaneDef, const CATSafeArrayVariant & iStartPt, const CATSafeArrayVariant & idEndPt, DNBIAWalk *& oCreatedWalk); \
      virtual HRESULT __stdcall CreateAutoWalk(CATIAActivity * oPrevAct, DNBIAAutoWalk *& oCreatedAutoWalk); \
      virtual HRESULT __stdcall CreatePick(CATIAActivity * iPrevAct, HTSPickType iPickType, CAT_VARIANT_BOOL bCreateCstWithPickingHand, HTSHand iPickingHand, const CATSafeArrayVariant & iPickedProducts, DNBIAPick *& oCreatedPick); \
      virtual HRESULT __stdcall CreatePlace(CATIAActivity * iPrevAct, const CATSafeArrayVariant & iPlacedProducts, const CATSafeArrayVariant & iOffset, DNBIAPlace *& oCreatedPlace); \
      virtual HRESULT __stdcall CreateCollisionFreeWalkFwdOnArrArea(CATIAActivity * iPrevAct, CATIAProduct * iArrArea, CATLONG iNumPoints, HTSSearchIntensity iSearchInt, double iClearence, const CATSafeArrayVariant & iPoints, DNBIACollisionFreeWalk *& oCreatedWalk); \
      virtual HRESULT __stdcall CreateCollisionFreeWalkFwdOnPlane(CATIAActivity * iPrevAct, CATIAProduct * iPlaneProd, const CATSafeArrayVariant & iPlaneDef, CATLONG iNumPoints, HTSSearchIntensity iSearchInt, double iClearence, const CATSafeArrayVariant & iPoints, DNBIACollisionFreeWalk *& oCreatedWalk); \
      virtual HRESULT __stdcall CreateCollisionFreeWalkBwdOnArrArea(CATIAActivity * iPrevAct, CATIAProduct * iArrArea, CATLONG iNumPoints, HTSSearchIntensity iSearchInt, double iClearence, const CATSafeArrayVariant & iPoints, DNBIACollisionFreeWalk *& oCreatedWalk); \
      virtual HRESULT __stdcall CreateCollisionFreeWalkBwdOnPlane(CATIAActivity * iPrevAct, CATIAProduct * iPlaneProd, const CATSafeArrayVariant & iPlaneDef, CATLONG iNumPoints, HTSSearchIntensity iSearchInt, double iClearence, const CATSafeArrayVariant & iPoints, DNBIACollisionFreeWalk *& oCreatedWalk); \
      virtual HRESULT __stdcall CreateHumanActivityGroup(CATIAActivity * iPrevAct, DNBIAHumanActivityGroup *& oCreatedHAG); \
      virtual HRESULT __stdcall CreateHumanCallTask(CATIAActivity * iPreviousActivity, DNBIAHumanTask * iCalledTask, DNBIAHumanCallTaskActivity *& oCreatedHumanCallTaskAct); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIAHumanActsFactory(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall CreateMoveToPosture(CATIAActivity * iPrevAct, DNBIAMoveToPosture *& oCreatedMTP); \
virtual HRESULT __stdcall CreateWalkFwdOnArrArea(CATIAActivity * iPrevAct, CATIAProduct * iArrArea, CATLONG iNumPoints, const CATSafeArrayVariant & iPoints, DNBIAWalk *& oCreatedWalk); \
virtual HRESULT __stdcall CreateWalkFwdOnPlane(CATIAActivity * iPrevAct, CATIAProduct * iProd, const CATSafeArrayVariant & iPlaneDef, CATLONG iNumPoints, const CATSafeArrayVariant & iPoints, DNBIAWalk *& oCreatedWalk); \
virtual HRESULT __stdcall CreateWalkBwdOnArrArea(CATIAActivity * iPrevAct, CATIAProduct * iArrArea, CATLONG iNumPoints, const CATSafeArrayVariant & iPoints, DNBIAWalk *& oCreatedWalk); \
virtual HRESULT __stdcall CreateWalkBwdOnPlane(CATIAActivity * iPrevAct, CATIAProduct * iPlaneProd, const CATSafeArrayVariant & iPlaneDef, CATLONG iNumPoints, const CATSafeArrayVariant & iPoints, DNBIAWalk *& oCreatedWalk); \
virtual HRESULT __stdcall CreateSideStepOnArrArea(CATIAActivity * iPrevAct, CATIAProduct * iArrArea, const CATSafeArrayVariant & iStartPt, const CATSafeArrayVariant & iEndPt, DNBIAWalk *& oCreatedWalk); \
virtual HRESULT __stdcall CreateSideStepOnPlane(CATIAActivity * iPrevAct, CATIAProduct * iPlaneProd, const CATSafeArrayVariant & iPlaneDef, const CATSafeArrayVariant & iStartPt, const CATSafeArrayVariant & idEndPt, DNBIAWalk *& oCreatedWalk); \
virtual HRESULT __stdcall CreateAutoWalk(CATIAActivity * oPrevAct, DNBIAAutoWalk *& oCreatedAutoWalk); \
virtual HRESULT __stdcall CreatePick(CATIAActivity * iPrevAct, HTSPickType iPickType, CAT_VARIANT_BOOL bCreateCstWithPickingHand, HTSHand iPickingHand, const CATSafeArrayVariant & iPickedProducts, DNBIAPick *& oCreatedPick); \
virtual HRESULT __stdcall CreatePlace(CATIAActivity * iPrevAct, const CATSafeArrayVariant & iPlacedProducts, const CATSafeArrayVariant & iOffset, DNBIAPlace *& oCreatedPlace); \
virtual HRESULT __stdcall CreateCollisionFreeWalkFwdOnArrArea(CATIAActivity * iPrevAct, CATIAProduct * iArrArea, CATLONG iNumPoints, HTSSearchIntensity iSearchInt, double iClearence, const CATSafeArrayVariant & iPoints, DNBIACollisionFreeWalk *& oCreatedWalk); \
virtual HRESULT __stdcall CreateCollisionFreeWalkFwdOnPlane(CATIAActivity * iPrevAct, CATIAProduct * iPlaneProd, const CATSafeArrayVariant & iPlaneDef, CATLONG iNumPoints, HTSSearchIntensity iSearchInt, double iClearence, const CATSafeArrayVariant & iPoints, DNBIACollisionFreeWalk *& oCreatedWalk); \
virtual HRESULT __stdcall CreateCollisionFreeWalkBwdOnArrArea(CATIAActivity * iPrevAct, CATIAProduct * iArrArea, CATLONG iNumPoints, HTSSearchIntensity iSearchInt, double iClearence, const CATSafeArrayVariant & iPoints, DNBIACollisionFreeWalk *& oCreatedWalk); \
virtual HRESULT __stdcall CreateCollisionFreeWalkBwdOnPlane(CATIAActivity * iPrevAct, CATIAProduct * iPlaneProd, const CATSafeArrayVariant & iPlaneDef, CATLONG iNumPoints, HTSSearchIntensity iSearchInt, double iClearence, const CATSafeArrayVariant & iPoints, DNBIACollisionFreeWalk *& oCreatedWalk); \
virtual HRESULT __stdcall CreateHumanActivityGroup(CATIAActivity * iPrevAct, DNBIAHumanActivityGroup *& oCreatedHAG); \
virtual HRESULT __stdcall CreateHumanCallTask(CATIAActivity * iPreviousActivity, DNBIAHumanTask * iCalledTask, DNBIAHumanCallTaskActivity *& oCreatedHumanCallTaskAct); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIAHumanActsFactory(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::CreateMoveToPosture(CATIAActivity * iPrevAct, DNBIAMoveToPosture *& oCreatedMTP) \
{ \
return (ENVTIECALL(DNBIAHumanActsFactory,ENVTIETypeLetter,ENVTIELetter)CreateMoveToPosture(iPrevAct,oCreatedMTP)); \
} \
HRESULT __stdcall  ENVTIEName::CreateWalkFwdOnArrArea(CATIAActivity * iPrevAct, CATIAProduct * iArrArea, CATLONG iNumPoints, const CATSafeArrayVariant & iPoints, DNBIAWalk *& oCreatedWalk) \
{ \
return (ENVTIECALL(DNBIAHumanActsFactory,ENVTIETypeLetter,ENVTIELetter)CreateWalkFwdOnArrArea(iPrevAct,iArrArea,iNumPoints,iPoints,oCreatedWalk)); \
} \
HRESULT __stdcall  ENVTIEName::CreateWalkFwdOnPlane(CATIAActivity * iPrevAct, CATIAProduct * iProd, const CATSafeArrayVariant & iPlaneDef, CATLONG iNumPoints, const CATSafeArrayVariant & iPoints, DNBIAWalk *& oCreatedWalk) \
{ \
return (ENVTIECALL(DNBIAHumanActsFactory,ENVTIETypeLetter,ENVTIELetter)CreateWalkFwdOnPlane(iPrevAct,iProd,iPlaneDef,iNumPoints,iPoints,oCreatedWalk)); \
} \
HRESULT __stdcall  ENVTIEName::CreateWalkBwdOnArrArea(CATIAActivity * iPrevAct, CATIAProduct * iArrArea, CATLONG iNumPoints, const CATSafeArrayVariant & iPoints, DNBIAWalk *& oCreatedWalk) \
{ \
return (ENVTIECALL(DNBIAHumanActsFactory,ENVTIETypeLetter,ENVTIELetter)CreateWalkBwdOnArrArea(iPrevAct,iArrArea,iNumPoints,iPoints,oCreatedWalk)); \
} \
HRESULT __stdcall  ENVTIEName::CreateWalkBwdOnPlane(CATIAActivity * iPrevAct, CATIAProduct * iPlaneProd, const CATSafeArrayVariant & iPlaneDef, CATLONG iNumPoints, const CATSafeArrayVariant & iPoints, DNBIAWalk *& oCreatedWalk) \
{ \
return (ENVTIECALL(DNBIAHumanActsFactory,ENVTIETypeLetter,ENVTIELetter)CreateWalkBwdOnPlane(iPrevAct,iPlaneProd,iPlaneDef,iNumPoints,iPoints,oCreatedWalk)); \
} \
HRESULT __stdcall  ENVTIEName::CreateSideStepOnArrArea(CATIAActivity * iPrevAct, CATIAProduct * iArrArea, const CATSafeArrayVariant & iStartPt, const CATSafeArrayVariant & iEndPt, DNBIAWalk *& oCreatedWalk) \
{ \
return (ENVTIECALL(DNBIAHumanActsFactory,ENVTIETypeLetter,ENVTIELetter)CreateSideStepOnArrArea(iPrevAct,iArrArea,iStartPt,iEndPt,oCreatedWalk)); \
} \
HRESULT __stdcall  ENVTIEName::CreateSideStepOnPlane(CATIAActivity * iPrevAct, CATIAProduct * iPlaneProd, const CATSafeArrayVariant & iPlaneDef, const CATSafeArrayVariant & iStartPt, const CATSafeArrayVariant & idEndPt, DNBIAWalk *& oCreatedWalk) \
{ \
return (ENVTIECALL(DNBIAHumanActsFactory,ENVTIETypeLetter,ENVTIELetter)CreateSideStepOnPlane(iPrevAct,iPlaneProd,iPlaneDef,iStartPt,idEndPt,oCreatedWalk)); \
} \
HRESULT __stdcall  ENVTIEName::CreateAutoWalk(CATIAActivity * oPrevAct, DNBIAAutoWalk *& oCreatedAutoWalk) \
{ \
return (ENVTIECALL(DNBIAHumanActsFactory,ENVTIETypeLetter,ENVTIELetter)CreateAutoWalk(oPrevAct,oCreatedAutoWalk)); \
} \
HRESULT __stdcall  ENVTIEName::CreatePick(CATIAActivity * iPrevAct, HTSPickType iPickType, CAT_VARIANT_BOOL bCreateCstWithPickingHand, HTSHand iPickingHand, const CATSafeArrayVariant & iPickedProducts, DNBIAPick *& oCreatedPick) \
{ \
return (ENVTIECALL(DNBIAHumanActsFactory,ENVTIETypeLetter,ENVTIELetter)CreatePick(iPrevAct,iPickType,bCreateCstWithPickingHand,iPickingHand,iPickedProducts,oCreatedPick)); \
} \
HRESULT __stdcall  ENVTIEName::CreatePlace(CATIAActivity * iPrevAct, const CATSafeArrayVariant & iPlacedProducts, const CATSafeArrayVariant & iOffset, DNBIAPlace *& oCreatedPlace) \
{ \
return (ENVTIECALL(DNBIAHumanActsFactory,ENVTIETypeLetter,ENVTIELetter)CreatePlace(iPrevAct,iPlacedProducts,iOffset,oCreatedPlace)); \
} \
HRESULT __stdcall  ENVTIEName::CreateCollisionFreeWalkFwdOnArrArea(CATIAActivity * iPrevAct, CATIAProduct * iArrArea, CATLONG iNumPoints, HTSSearchIntensity iSearchInt, double iClearence, const CATSafeArrayVariant & iPoints, DNBIACollisionFreeWalk *& oCreatedWalk) \
{ \
return (ENVTIECALL(DNBIAHumanActsFactory,ENVTIETypeLetter,ENVTIELetter)CreateCollisionFreeWalkFwdOnArrArea(iPrevAct,iArrArea,iNumPoints,iSearchInt,iClearence,iPoints,oCreatedWalk)); \
} \
HRESULT __stdcall  ENVTIEName::CreateCollisionFreeWalkFwdOnPlane(CATIAActivity * iPrevAct, CATIAProduct * iPlaneProd, const CATSafeArrayVariant & iPlaneDef, CATLONG iNumPoints, HTSSearchIntensity iSearchInt, double iClearence, const CATSafeArrayVariant & iPoints, DNBIACollisionFreeWalk *& oCreatedWalk) \
{ \
return (ENVTIECALL(DNBIAHumanActsFactory,ENVTIETypeLetter,ENVTIELetter)CreateCollisionFreeWalkFwdOnPlane(iPrevAct,iPlaneProd,iPlaneDef,iNumPoints,iSearchInt,iClearence,iPoints,oCreatedWalk)); \
} \
HRESULT __stdcall  ENVTIEName::CreateCollisionFreeWalkBwdOnArrArea(CATIAActivity * iPrevAct, CATIAProduct * iArrArea, CATLONG iNumPoints, HTSSearchIntensity iSearchInt, double iClearence, const CATSafeArrayVariant & iPoints, DNBIACollisionFreeWalk *& oCreatedWalk) \
{ \
return (ENVTIECALL(DNBIAHumanActsFactory,ENVTIETypeLetter,ENVTIELetter)CreateCollisionFreeWalkBwdOnArrArea(iPrevAct,iArrArea,iNumPoints,iSearchInt,iClearence,iPoints,oCreatedWalk)); \
} \
HRESULT __stdcall  ENVTIEName::CreateCollisionFreeWalkBwdOnPlane(CATIAActivity * iPrevAct, CATIAProduct * iPlaneProd, const CATSafeArrayVariant & iPlaneDef, CATLONG iNumPoints, HTSSearchIntensity iSearchInt, double iClearence, const CATSafeArrayVariant & iPoints, DNBIACollisionFreeWalk *& oCreatedWalk) \
{ \
return (ENVTIECALL(DNBIAHumanActsFactory,ENVTIETypeLetter,ENVTIELetter)CreateCollisionFreeWalkBwdOnPlane(iPrevAct,iPlaneProd,iPlaneDef,iNumPoints,iSearchInt,iClearence,iPoints,oCreatedWalk)); \
} \
HRESULT __stdcall  ENVTIEName::CreateHumanActivityGroup(CATIAActivity * iPrevAct, DNBIAHumanActivityGroup *& oCreatedHAG) \
{ \
return (ENVTIECALL(DNBIAHumanActsFactory,ENVTIETypeLetter,ENVTIELetter)CreateHumanActivityGroup(iPrevAct,oCreatedHAG)); \
} \
HRESULT __stdcall  ENVTIEName::CreateHumanCallTask(CATIAActivity * iPreviousActivity, DNBIAHumanTask * iCalledTask, DNBIAHumanCallTaskActivity *& oCreatedHumanCallTaskAct) \
{ \
return (ENVTIECALL(DNBIAHumanActsFactory,ENVTIETypeLetter,ENVTIELetter)CreateHumanCallTask(iPreviousActivity,iCalledTask,oCreatedHumanCallTaskAct)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIAHumanActsFactory,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIAHumanActsFactory,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIAHumanActsFactory,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIAHumanActsFactory,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIAHumanActsFactory,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIAHumanActsFactory(classe)    TIEDNBIAHumanActsFactory##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIAHumanActsFactory(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIAHumanActsFactory, classe) \
 \
 \
CATImplementTIEMethods(DNBIAHumanActsFactory, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIAHumanActsFactory, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIAHumanActsFactory, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIAHumanActsFactory, classe) \
 \
HRESULT __stdcall  TIEDNBIAHumanActsFactory##classe::CreateMoveToPosture(CATIAActivity * iPrevAct, DNBIAMoveToPosture *& oCreatedMTP) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&iPrevAct,&oCreatedMTP); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateMoveToPosture(iPrevAct,oCreatedMTP); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&iPrevAct,&oCreatedMTP); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHumanActsFactory##classe::CreateWalkFwdOnArrArea(CATIAActivity * iPrevAct, CATIAProduct * iArrArea, CATLONG iNumPoints, const CATSafeArrayVariant & iPoints, DNBIAWalk *& oCreatedWalk) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&iPrevAct,&iArrArea,&iNumPoints,&iPoints,&oCreatedWalk); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateWalkFwdOnArrArea(iPrevAct,iArrArea,iNumPoints,iPoints,oCreatedWalk); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&iPrevAct,&iArrArea,&iNumPoints,&iPoints,&oCreatedWalk); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHumanActsFactory##classe::CreateWalkFwdOnPlane(CATIAActivity * iPrevAct, CATIAProduct * iProd, const CATSafeArrayVariant & iPlaneDef, CATLONG iNumPoints, const CATSafeArrayVariant & iPoints, DNBIAWalk *& oCreatedWalk) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&iPrevAct,&iProd,&iPlaneDef,&iNumPoints,&iPoints,&oCreatedWalk); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateWalkFwdOnPlane(iPrevAct,iProd,iPlaneDef,iNumPoints,iPoints,oCreatedWalk); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&iPrevAct,&iProd,&iPlaneDef,&iNumPoints,&iPoints,&oCreatedWalk); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHumanActsFactory##classe::CreateWalkBwdOnArrArea(CATIAActivity * iPrevAct, CATIAProduct * iArrArea, CATLONG iNumPoints, const CATSafeArrayVariant & iPoints, DNBIAWalk *& oCreatedWalk) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&iPrevAct,&iArrArea,&iNumPoints,&iPoints,&oCreatedWalk); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateWalkBwdOnArrArea(iPrevAct,iArrArea,iNumPoints,iPoints,oCreatedWalk); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&iPrevAct,&iArrArea,&iNumPoints,&iPoints,&oCreatedWalk); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHumanActsFactory##classe::CreateWalkBwdOnPlane(CATIAActivity * iPrevAct, CATIAProduct * iPlaneProd, const CATSafeArrayVariant & iPlaneDef, CATLONG iNumPoints, const CATSafeArrayVariant & iPoints, DNBIAWalk *& oCreatedWalk) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&iPrevAct,&iPlaneProd,&iPlaneDef,&iNumPoints,&iPoints,&oCreatedWalk); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateWalkBwdOnPlane(iPrevAct,iPlaneProd,iPlaneDef,iNumPoints,iPoints,oCreatedWalk); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&iPrevAct,&iPlaneProd,&iPlaneDef,&iNumPoints,&iPoints,&oCreatedWalk); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHumanActsFactory##classe::CreateSideStepOnArrArea(CATIAActivity * iPrevAct, CATIAProduct * iArrArea, const CATSafeArrayVariant & iStartPt, const CATSafeArrayVariant & iEndPt, DNBIAWalk *& oCreatedWalk) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&iPrevAct,&iArrArea,&iStartPt,&iEndPt,&oCreatedWalk); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateSideStepOnArrArea(iPrevAct,iArrArea,iStartPt,iEndPt,oCreatedWalk); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&iPrevAct,&iArrArea,&iStartPt,&iEndPt,&oCreatedWalk); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHumanActsFactory##classe::CreateSideStepOnPlane(CATIAActivity * iPrevAct, CATIAProduct * iPlaneProd, const CATSafeArrayVariant & iPlaneDef, const CATSafeArrayVariant & iStartPt, const CATSafeArrayVariant & idEndPt, DNBIAWalk *& oCreatedWalk) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&iPrevAct,&iPlaneProd,&iPlaneDef,&iStartPt,&idEndPt,&oCreatedWalk); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateSideStepOnPlane(iPrevAct,iPlaneProd,iPlaneDef,iStartPt,idEndPt,oCreatedWalk); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&iPrevAct,&iPlaneProd,&iPlaneDef,&iStartPt,&idEndPt,&oCreatedWalk); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHumanActsFactory##classe::CreateAutoWalk(CATIAActivity * oPrevAct, DNBIAAutoWalk *& oCreatedAutoWalk) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&oPrevAct,&oCreatedAutoWalk); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateAutoWalk(oPrevAct,oCreatedAutoWalk); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&oPrevAct,&oCreatedAutoWalk); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHumanActsFactory##classe::CreatePick(CATIAActivity * iPrevAct, HTSPickType iPickType, CAT_VARIANT_BOOL bCreateCstWithPickingHand, HTSHand iPickingHand, const CATSafeArrayVariant & iPickedProducts, DNBIAPick *& oCreatedPick) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&iPrevAct,&iPickType,&bCreateCstWithPickingHand,&iPickingHand,&iPickedProducts,&oCreatedPick); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreatePick(iPrevAct,iPickType,bCreateCstWithPickingHand,iPickingHand,iPickedProducts,oCreatedPick); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&iPrevAct,&iPickType,&bCreateCstWithPickingHand,&iPickingHand,&iPickedProducts,&oCreatedPick); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHumanActsFactory##classe::CreatePlace(CATIAActivity * iPrevAct, const CATSafeArrayVariant & iPlacedProducts, const CATSafeArrayVariant & iOffset, DNBIAPlace *& oCreatedPlace) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&iPrevAct,&iPlacedProducts,&iOffset,&oCreatedPlace); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreatePlace(iPrevAct,iPlacedProducts,iOffset,oCreatedPlace); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&iPrevAct,&iPlacedProducts,&iOffset,&oCreatedPlace); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHumanActsFactory##classe::CreateCollisionFreeWalkFwdOnArrArea(CATIAActivity * iPrevAct, CATIAProduct * iArrArea, CATLONG iNumPoints, HTSSearchIntensity iSearchInt, double iClearence, const CATSafeArrayVariant & iPoints, DNBIACollisionFreeWalk *& oCreatedWalk) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,11,&_Trac2,&iPrevAct,&iArrArea,&iNumPoints,&iSearchInt,&iClearence,&iPoints,&oCreatedWalk); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateCollisionFreeWalkFwdOnArrArea(iPrevAct,iArrArea,iNumPoints,iSearchInt,iClearence,iPoints,oCreatedWalk); \
   ExitAfterCall(this,11,_Trac2,&_ret_arg,&iPrevAct,&iArrArea,&iNumPoints,&iSearchInt,&iClearence,&iPoints,&oCreatedWalk); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHumanActsFactory##classe::CreateCollisionFreeWalkFwdOnPlane(CATIAActivity * iPrevAct, CATIAProduct * iPlaneProd, const CATSafeArrayVariant & iPlaneDef, CATLONG iNumPoints, HTSSearchIntensity iSearchInt, double iClearence, const CATSafeArrayVariant & iPoints, DNBIACollisionFreeWalk *& oCreatedWalk) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,12,&_Trac2,&iPrevAct,&iPlaneProd,&iPlaneDef,&iNumPoints,&iSearchInt,&iClearence,&iPoints,&oCreatedWalk); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateCollisionFreeWalkFwdOnPlane(iPrevAct,iPlaneProd,iPlaneDef,iNumPoints,iSearchInt,iClearence,iPoints,oCreatedWalk); \
   ExitAfterCall(this,12,_Trac2,&_ret_arg,&iPrevAct,&iPlaneProd,&iPlaneDef,&iNumPoints,&iSearchInt,&iClearence,&iPoints,&oCreatedWalk); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHumanActsFactory##classe::CreateCollisionFreeWalkBwdOnArrArea(CATIAActivity * iPrevAct, CATIAProduct * iArrArea, CATLONG iNumPoints, HTSSearchIntensity iSearchInt, double iClearence, const CATSafeArrayVariant & iPoints, DNBIACollisionFreeWalk *& oCreatedWalk) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,13,&_Trac2,&iPrevAct,&iArrArea,&iNumPoints,&iSearchInt,&iClearence,&iPoints,&oCreatedWalk); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateCollisionFreeWalkBwdOnArrArea(iPrevAct,iArrArea,iNumPoints,iSearchInt,iClearence,iPoints,oCreatedWalk); \
   ExitAfterCall(this,13,_Trac2,&_ret_arg,&iPrevAct,&iArrArea,&iNumPoints,&iSearchInt,&iClearence,&iPoints,&oCreatedWalk); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHumanActsFactory##classe::CreateCollisionFreeWalkBwdOnPlane(CATIAActivity * iPrevAct, CATIAProduct * iPlaneProd, const CATSafeArrayVariant & iPlaneDef, CATLONG iNumPoints, HTSSearchIntensity iSearchInt, double iClearence, const CATSafeArrayVariant & iPoints, DNBIACollisionFreeWalk *& oCreatedWalk) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,14,&_Trac2,&iPrevAct,&iPlaneProd,&iPlaneDef,&iNumPoints,&iSearchInt,&iClearence,&iPoints,&oCreatedWalk); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateCollisionFreeWalkBwdOnPlane(iPrevAct,iPlaneProd,iPlaneDef,iNumPoints,iSearchInt,iClearence,iPoints,oCreatedWalk); \
   ExitAfterCall(this,14,_Trac2,&_ret_arg,&iPrevAct,&iPlaneProd,&iPlaneDef,&iNumPoints,&iSearchInt,&iClearence,&iPoints,&oCreatedWalk); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHumanActsFactory##classe::CreateHumanActivityGroup(CATIAActivity * iPrevAct, DNBIAHumanActivityGroup *& oCreatedHAG) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,15,&_Trac2,&iPrevAct,&oCreatedHAG); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateHumanActivityGroup(iPrevAct,oCreatedHAG); \
   ExitAfterCall(this,15,_Trac2,&_ret_arg,&iPrevAct,&oCreatedHAG); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHumanActsFactory##classe::CreateHumanCallTask(CATIAActivity * iPreviousActivity, DNBIAHumanTask * iCalledTask, DNBIAHumanCallTaskActivity *& oCreatedHumanCallTaskAct) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,16,&_Trac2,&iPreviousActivity,&iCalledTask,&oCreatedHumanCallTaskAct); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateHumanCallTask(iPreviousActivity,iCalledTask,oCreatedHumanCallTaskAct); \
   ExitAfterCall(this,16,_Trac2,&_ret_arg,&iPreviousActivity,&iCalledTask,&oCreatedHumanCallTaskAct); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAHumanActsFactory##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,17,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,17,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAHumanActsFactory##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,18,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,18,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAHumanActsFactory##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,19,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,19,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAHumanActsFactory##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,20,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,20,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAHumanActsFactory##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,21,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,21,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIAHumanActsFactory(classe) \
 \
 \
declare_TIE_DNBIAHumanActsFactory(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAHumanActsFactory##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAHumanActsFactory,"DNBIAHumanActsFactory",DNBIAHumanActsFactory::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAHumanActsFactory(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIAHumanActsFactory, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAHumanActsFactory##classe(classe::MetaObject(),DNBIAHumanActsFactory::MetaObject(),(void *)CreateTIEDNBIAHumanActsFactory##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIAHumanActsFactory(classe) \
 \
 \
declare_TIE_DNBIAHumanActsFactory(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAHumanActsFactory##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAHumanActsFactory,"DNBIAHumanActsFactory",DNBIAHumanActsFactory::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAHumanActsFactory(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIAHumanActsFactory, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAHumanActsFactory##classe(classe::MetaObject(),DNBIAHumanActsFactory::MetaObject(),(void *)CreateTIEDNBIAHumanActsFactory##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIAHumanActsFactory(classe) TIE_DNBIAHumanActsFactory(classe)
#else
#define BOA_DNBIAHumanActsFactory(classe) CATImplementBOA(DNBIAHumanActsFactory, classe)
#endif

#endif
