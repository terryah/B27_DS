#ifndef __TIE_DNBIAHumanProgram
#define __TIE_DNBIAHumanProgram

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIAHumanProgram.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIAHumanProgram */
#define declare_TIE_DNBIAHumanProgram(classe) \
 \
 \
class TIEDNBIAHumanProgram##classe : public DNBIAHumanProgram \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIAHumanProgram, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall get_TaskList(DNBIAHumanTaskList *& oTaskList); \
      virtual HRESULT __stdcall CreateHumanTask(DNBIAHumanTask *& oCreatedHumanTask); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIAHumanProgram(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall get_TaskList(DNBIAHumanTaskList *& oTaskList); \
virtual HRESULT __stdcall CreateHumanTask(DNBIAHumanTask *& oCreatedHumanTask); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIAHumanProgram(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::get_TaskList(DNBIAHumanTaskList *& oTaskList) \
{ \
return (ENVTIECALL(DNBIAHumanProgram,ENVTIETypeLetter,ENVTIELetter)get_TaskList(oTaskList)); \
} \
HRESULT __stdcall  ENVTIEName::CreateHumanTask(DNBIAHumanTask *& oCreatedHumanTask) \
{ \
return (ENVTIECALL(DNBIAHumanProgram,ENVTIETypeLetter,ENVTIELetter)CreateHumanTask(oCreatedHumanTask)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIAHumanProgram,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIAHumanProgram,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIAHumanProgram,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIAHumanProgram,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIAHumanProgram,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIAHumanProgram(classe)    TIEDNBIAHumanProgram##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIAHumanProgram(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIAHumanProgram, classe) \
 \
 \
CATImplementTIEMethods(DNBIAHumanProgram, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIAHumanProgram, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIAHumanProgram, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIAHumanProgram, classe) \
 \
HRESULT __stdcall  TIEDNBIAHumanProgram##classe::get_TaskList(DNBIAHumanTaskList *& oTaskList) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&oTaskList); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_TaskList(oTaskList); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&oTaskList); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHumanProgram##classe::CreateHumanTask(DNBIAHumanTask *& oCreatedHumanTask) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&oCreatedHumanTask); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateHumanTask(oCreatedHumanTask); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&oCreatedHumanTask); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAHumanProgram##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAHumanProgram##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAHumanProgram##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAHumanProgram##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAHumanProgram##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIAHumanProgram(classe) \
 \
 \
declare_TIE_DNBIAHumanProgram(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAHumanProgram##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAHumanProgram,"DNBIAHumanProgram",DNBIAHumanProgram::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAHumanProgram(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIAHumanProgram, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAHumanProgram##classe(classe::MetaObject(),DNBIAHumanProgram::MetaObject(),(void *)CreateTIEDNBIAHumanProgram##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIAHumanProgram(classe) \
 \
 \
declare_TIE_DNBIAHumanProgram(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAHumanProgram##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAHumanProgram,"DNBIAHumanProgram",DNBIAHumanProgram::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAHumanProgram(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIAHumanProgram, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAHumanProgram##classe(classe::MetaObject(),DNBIAHumanProgram::MetaObject(),(void *)CreateTIEDNBIAHumanProgram##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIAHumanProgram(classe) TIE_DNBIAHumanProgram(classe)
#else
#define BOA_DNBIAHumanProgram(classe) CATImplementBOA(DNBIAHumanProgram, classe)
#endif

#endif
