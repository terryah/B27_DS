#include "CATIACGMLevel.h"
#ifdef CATIACGMR420CAA
#ifdef	__CATGMAdvancedOperatorsInterfaces


// COPYRIGHT DASSAULT SYSTEMES 2006

/** @CAA2Required */

/*---------------------------------------------------------------------*/

/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */

/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */

/*---------------------------------------------------------------------*/
#define ExportedByCATGMAdvancedOperatorsInterfaces DSYExport
#else
#define ExportedByCATGMAdvancedOperatorsInterfaces DSYImport
#endif
#include "DSYExport.h"
#else
#ifdef	_WINDOWS_SOURCE
#ifdef	__CATGMAdvancedOperatorsInterfaces
// COPYRIGHT DASSAULT SYSTEMES 2006
/** @CAA2Required */
/*---------------------------------------------------------------------*/
/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */
/*---------------------------------------------------------------------*/
#define	ExportedByCATGMAdvancedOperatorsInterfaces	__declspec(dllexport)
#else
#define	ExportedByCATGMAdvancedOperatorsInterfaces	__declspec(dllimport)
#endif
#else
#define	ExportedByCATGMAdvancedOperatorsInterfaces
#endif
#endif


