//*
//* FILE:
//*     DNBCompoundJointOpcode.h    - protected header file
//*
//* MODULE:
//*     DNBDeviceInterfaces
//*
//* OVERVIEW:
//*
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     xxx         03/16/2000  Initial Implementation
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1999, 2000 Delmia, CORP.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Delmia, CORP., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*

#ifndef _DNB_COMPOUNDJOINTOPCODE_H_
#define _DNB_COMPOUNDJOINTOPCODE_H_

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

#include "DNBDeviceItfCPP.h"


typedef enum
{
    DNB_Return        = 1,
    DNB_Identity      = 2,
    DNB_PushMat       = 3,
    DNB_PopMat        = 4,
    DNB_SetHome       = 5,
    DNB_CatHome       = 6,
    DNB_GetHome       = 7,
    DNB_TranslateXYZ  = 12,
    DNB_TranslateX    = 13,
    DNB_TranslateY    = 14,
    DNB_TranslateZ    = 15,
    DNB_RotateX       = 16,
    DNB_RotateY       = 17,
    DNB_RotateZ       = 18,
    DNB_RotateRPY     = 19,
    DNB_TranslateNEGX = -13,
    DNB_TranslateNEGY = -14,
    DNB_TranslateNEGZ = -15,
    DNB_RotateNEGX    = -16,
    DNB_RotateNEGY    = -17,
    DNB_RotateNEGZ    = -18
} DNBCompoundJointOpcode;


#endif /* _DNB_COMPOUNDJOINTOPCODE_H_ */
