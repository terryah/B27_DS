//*
//* FILE:
//*     DNBIBasicCompoundJointList.h    - protected header file
//*
//* MODULE:
//*     DNBDeviceInterfaces
//*
//* OVERVIEW:
//*
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     xxx         03/16/2000  Initial Implementation
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1999, 2000 Delmia, CORP.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Delmia, CORP., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*

#ifndef _DNBIBASICCOMPOUNDJOINTLIST_H_
#define _DNBIBASICCOMPOUNDJOINTLIST_H_

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */


#include "DNBDeviceItfCPP.h"

#include "DNBIBasicCompoundJoint.h"
#include <CATLISTV_Clean.h>
#include <CATLISTV_AllFunct.h>
#include <CATLISTV_Declare.h>

#undef  CATCOLLEC_ExportedBy
#define CATCOLLEC_ExportedBy    ExportedByDNBDeviceItfCPP


CATLISTV_DECLARE(DNBIBasicCompoundJoint_var)
typedef CATLISTV(DNBIBasicCompoundJoint_var) DNBIBasicCompoundJointList ;


#endif /* _DNBIBASICCOMPOUNDJOINTLIST_H_ */
