// COPYRIGHT Dassault Systemes 2002
//=============================================================================
//
// DNBIDeviceMgr.h
// Interface dedicated to get the mechanism from the current implementation.
//
//=============================================================================
//
// Usage notes:
//   This interface should be used to retrieve the mechanism associated to the
// current product instance. It should not be used for selection or filtering
// purpose (use DNBIDeviceManager). This interface supports :
//  - D5 device
//  - V5 mechanism
//  - V4 mechanism
// This interface should not be reimplemented without the owner approval.
//=============================================================================
//  Oct 2002  Creation:                                                     awn
//=============================================================================
#ifndef DNBIDeviceMgr_H
#define DNBIDeviceMgr_H

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

#include "DNBDeviceItfCPP.h"
#include "CATBaseUnknown.h"
#include "DNBDeviceType.h"
#include "CATLISTV_CATBaseUnknown.h"
#include "CATBooleanDef.h"

extern ExportedByDNBDeviceItfCPP IID IID_DNBIDeviceMgr ;

//------------------------------------------------------------------

/**
 * Provide the correct mechanism and its associated father if needed.
 * This interface is returning the first mechanism (D5,V5,V4) found.
 * <p>
 * 
 */
class ExportedByDNBDeviceItfCPP DNBIDeviceMgr: public CATBaseUnknown
{
  CATDeclareInterface;

  public:

/**
  * Provide the correct mechanism object.
  *   @param opMechanism
  *      CATBaseUnknown pointer. Must be released. Contains the mechanism.
  * It is an output parameter.
  *   @param opMechanism
  *      CATBaseUnknown pointer. Must be released. Points to the correct
  * father instance of the mechanism. It is an output parameter.
  *   @return
  *      RC = S_OK if the method succeeds.
  *
  *
  * <dl>
  * <dt><b>Example:</b>
  * <pre>
  *  CATIProduct* piProduct = XXXX;// product instance pointer 
  *  ...
  *  CATBaseUnknown* pBUMechanism = NULL;
  *  CATBaseUnknown* pBUFather = NULL;
  *  DNBIDeviceMgr* piDeviceMgr = NULL;
  *  if(SUCCEEDED(piProduct -> QueryInterface(IID_DNBIDeviceMgr,
  *                                           (void**)&piDeviceMgr)))
  *  {
  *    if(SUCCEEDED(piDeviceMgr -> GetMechanism(&opMechanism,&opFather)))
  *    {
  *      // do what ever you want..
  *    }
  *    piDeviceMgr -> Release();
  *    piDeviceMgr = NULL;
  *  }
  *  if(pBUMechanism)
  *  {
  *    pBUMechanism -> Release();
  *    pBUMechanism = NULL;
  *  }
  *  if(pBUFather)
  *  {
  *    pBUFather -> Release();
  *    pBUFather = NULL;
  *  }
  * 
  * </pre>
  * </dl>
  */
  virtual HRESULT GetMechanism( CATBaseUnknown** opMechanism,
                                CATBaseUnknown** opFather)=0;


/**
  * Provide the correct mechanism object.
  *   @param ioListMech
  *      List of CATBaseUnknown smart pointer. Contains the mechanism list.
  * It is an input/output parameter.
  *   @param ioListFather
  *      List of CATBaseUnknown smart pointer. Contains the mechanism father
  * list father instance of the mechanism.
  * It is an input/output parameter.
  *   @param iType
  *      DNBDeviceType enum. Indicates the type of device to filter. You can
  * define several kind with |.
  *   @param ibRefreshList
  *      Boolean. Forces the cached list to be refreshed. Use it carefully!
  * It has cost in terms of performances.
  * define several kind with |.
  *   @return
  *      RC = S_OK if the method succeeds.
  */
  virtual HRESULT GetAllMechanism(CATListValCATBaseUnknown_var& ioListMech,
                                  CATListValCATBaseUnknown_var& ioListFather,
                                  DNBDeviceType iType=AllDeviceType,
                                  boolean ibRefreshList=FALSE)=0;


  // No constructors or destructors on this pure virtual base class
  // --------------------------------------------------------------
};

CATDeclareHandler( DNBIDeviceMgr, CATBaseUnknown );


//------------------------------------------------------------------

#endif
