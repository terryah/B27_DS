/* -*-c++-*- */
// COPYRIGHT DASSAULT SYSTEMES / DELMIA Corp 2001
//=============================================================================
//
// DNBIGenericMotionProfile.h
//
//=============================================================================
//
// Usage notes:
//
//=============================================================================
// ???. ????  Creation                                                      ???
//
// Jun. 2001  Modification                                                  awn
//              Implementation of new methods for XML streaming
//                -> SaveXML
//                -> LoadXML
// Jun  2005 Modification
//              Moved enum declarations to DNBMotionBasisType.h         shl
//=============================================================================
//*		xya			10/01/2003	remove XML support
//      pjr         06/21/2005  Added two APIs, to set and get the 
//                              the attribute "ProfileIndex"
//=============================================================================

#ifndef DNBIGENERICMOTIONPROFILE_H
#define DNBIGENERICMOTIONPROFILE_H

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

#include <CATBaseUnknown.h>
#include <CATBooleanDef.h>
#include <DNBDeviceItfCPP.h>
#include <DNBMotionBasisType.h>


class CATUnicodeString;
class DNBIGenericMotionProfile_var;
class DNBIRobGenericController_var;
//class DOM_Document;
//class DOM_Element;


extern ExportedByDNBDeviceItfCPP IID IID_DNBIGenericMotionProfile;


//------------------------------------------------------------------
/**
 * Interface to access Motion Profile.
 * <b>Role:</b>
 * This interface provides methods to get/set data related to the
 * Motion Profile.
 */
class ExportedByDNBDeviceItfCPP DNBIGenericMotionProfile: public CATBaseUnknown 
{
/**
 * @nodoc
 */
    CATDeclareInterface;

    public:
    
   /**
    * Set name of the Motion Profile.
    * @param name
    *   name of the profile to be set.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */      
	virtual HRESULT
    SetName( CATUnicodeString name ) = 0;

   /**
    * Retrieves the name of the Profile.
    * @param name
    *   This out parameter contains name of the Profile.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual HRESULT
    GetName( CATUnicodeString& name ) const = 0;

   /**
    * Set motion basis of the Profile.
    * @param basis
    *   motion basis, one of following: MOTION_ABSOLUTE, MOTION_PERCENT, MOTION_TIME
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual HRESULT
    SetMotionBasis( MotionBasis basis ) = 0;
	
   /**
    * Retrieves motion basis of the Profile.
    * @param mBasis
    *   This out parameter contains motion basis of the Profile.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual HRESULT 
    GetMotionBasis( MotionBasis& mBasis ) const = 0;
 
   /**
    * Set speed value of the Profile.
    * @param value
    *   meaning of the value is based on the motion basis:
	*  MOTION_ABSOLUTE	: absolute speed value. 
	*  MOTION_PERCENT	: 0-1, percent of max speed of the device
	*  MOTION_TIME		: in seconds
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual HRESULT
    SetSpeedValue( double value ) = 0;
	
   /**
    * Retrieves speed value of the Profile.
    * @param value
    *   This out parameter contains motion speed of the Profile.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual HRESULT 
    GetSpeedValue( double& value ) const = 0;

   /**
    * Set acceleration value of the Profile.
    * @param value
    *   meaning of the value is based on the motion basis:
	*  MOTION_ABSOLUTE	: absolute acceleration value. 
	*  MOTION_PERCENT	: 0-1, percent of max acceleration of the device
	*  MOTION_TIME		: percent of total move time used on acceleration/deceleration
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual HRESULT
    SetAccelerationValue( double value ) = 0;
	
   /**
    * Retrieves acceleration value of the Profile.
    * @param value
    *   This out parameter contains motion acceleration of the Profile.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual HRESULT 
    GetAccelerationValue( double& value ) const = 0;

   /**
    * Set Angular Speed value of the Profile.
    * @param value
    *   Percentage of max angular speed of robot. 
	*   The value is used when robot TCP rotate against a axis.
	* @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual HRESULT
    SetAngularSpeedValue( double value ) = 0;
	
   /**
    * Retrieves Angular Speed value of the Profile.
    * @param value
    *   This out parameter contains Angular Speed of the Profile.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual HRESULT 
    GetAngularSpeedValue( double& value ) const = 0;

   /**
    * Set Angular acceleration value of the Profile.
    * @param value
    *   Percentage of max angular acceleration of robot. 
	*   The value is used when robot TCP rotate against a axis.
	* @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual HRESULT
    SetAngularAccelerationValue( double value ) = 0;
	
   /**
    * Retrieves Angular acceleration value of the Profile.
    * @param value
    *   This out parameter contains Angular acceleration of the Profile.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual HRESULT 
    GetAngularAccelerationValue( double& value ) const = 0;

   /**
    * Retrieves controller which contains the profile.
    * @param spICntlr
    *   This out parameter contains smart pointer to controller.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual  HRESULT 
    GetController( DNBIRobGenericController_var& spICntlr ) const = 0;




  /**@nodoc
    * Internal Use. ONLY FOR WORKCELL SEQUENCING.
    * METHODS : 
    *   <b>SaveXML</b>
    * <br>
    * DESCRIPTION :
    *   Stream the motion profile data in an XML node. The method needs
    * to know in which XML document nodes can be created and under
    * which node the motion profile data will be added.
    * 
    *@param <i>doc</i>
    *   DOM_Document. XML document where data will be saved.
    *@param <i>rootElem</i>
    *   DOM_Element. XML node where data will be plugged under.
    *
    *@see DOM_Document in XMLParser.
    *@see DOM_Element in XMLParser.
    *
    */
//    virtual HRESULT SaveXML(DOM_Document& doc, DOM_Element& rootElem) = 0;



  /**@nodoc
    * Internal Use. ONLY FOR WORKCELL SEQUENCING.
    * METHODS : 
    *   <b>LoadXML</b>
    * <br>
    * DESCRIPTION :
    *   Unstream the motion profile data from an XML node. The method will
    * create a new motion profile based on the data contained in the XML
    * node.
    * 
    *@param <i>rootElem</i>
    *   DOM_Element. XML node where data will be read.
    *
    *@see DOM_Document in XMLParser.
    *@see DOM_Element in XMLParser.
    *
    */
  //  virtual HRESULT LoadXML(DOM_Element& rootElem) = 0;

	/**
    * Sets the (unique) Motion profile index
    * @param iProfileIndex ( integer )
    *   This input parameter is the user defined motion profile index
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>If the motion profile index has been successfull set
    *     </dd>
    *     <dt>E_FAIL </dt>
    *     <dd>If the setting of the motion profile index failed </dd>
    *   </dl> 
    */

   virtual HRESULT
	   SetMotionProfileIndex( const int& iProfileIndex ) = 0;

   /**
    * Returns the (unique) motion profile index
    * @param oProfileIndex ( integer )
    *   This output parameter contains the motion profile index
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>If the motion profile index has been successfull returned
    *     </dd>
    *     <dt>E_FAIL </dt>
    *     <dd>If the retrieval of the motion profile index failed </dd>
    *   </dl> 
    */

   virtual HRESULT
	   GetMotionProfileIndex( int& iProfileIndex ) const = 0;

};

//------------------------------------------------------------------
/**
 * @nodoc
 */
CATDeclareHandler( DNBIGenericMotionProfile, CATBaseUnknown );

#endif // DNBIGENERICTOOLPROFILE_H
