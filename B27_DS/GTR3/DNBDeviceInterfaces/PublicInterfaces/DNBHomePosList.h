// COPYRIGHT DASSAULT SYSTEMES 2000
//===================================================================
//
// DNBHomePosList.h
//   This interface allows the management of a list of Commands.
//
//===================================================================
//
//  feb 2000  Creation: BCL
//
//===================================================================
#ifndef DNBHomePosList_H
#define DNBHomePosList_H
/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */


/**
 * @collection DNBHomePosList 
 * Collection class for pointers to HomePos Structure.
 * Only the following methods of pointer collection classes are available:
 * <ul>
 * <li><tt>Append</tt></li>
 * <li><tt>Size</tt></li>
 * <li><tt>Operator[]</tt></li>
 * <li><tt>RemovePosition</tt></li>
 * <li><tt>RemoveAll</tt></li>
 * </ul>
 * Refer to the articles dealing with collections in the encyclopedia.
 */

#include "DNBDeviceItfCPP.h"
class DNBIHomePosition_var;
#include "DNBIHomePosition.h"


// clean previous functions requests
#include  <CATLISTV_Clean.h>

// require needed functions
#define CATLISTV_Append
#define CATLISTV_RemovePosition
#define CATLISTV_RemoveAll

// get macros
#include  <CATLISTV_Declare.h>

// generate interface of collection-class
// (functions declarations)
#undef	CATCOLLEC_ExportedBy
#define	CATCOLLEC_ExportedBy	ExportedByDNBDeviceItfCPP

/**
 * @nodoc
 */
CATLISTV_DECLARE(DNBIHomePosition_var)
typedef CATLISTV(DNBIHomePosition_var) DNBHomePosList ;
#endif

