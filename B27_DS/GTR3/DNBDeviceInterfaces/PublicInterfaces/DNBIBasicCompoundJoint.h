//*
//* FILE:
//*     DNBIBasicCompoundJoint.h    - protected header file
//*
//* MODULE:
//*     DNBDeviceInterfaces
//*
//* OVERVIEW:
//*
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     xxx         03/16/2000  Initial Implementation
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1999, 2000 Delmia, CORP.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Delmia, CORP., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*

#ifndef _DNBIBASICCOMPOUNDJOINT_H_
#define _DNBIBASICCOMPOUNDJOINT_H_
/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */



#include "DNBCompoundJointOpcode.h"

#include <CATBaseUnknown.h>
#include <CATBoolean.h>
#include <CATListOfCATUnicodeString.h>
#include <CATListOfInt.h>
#include <CATIProduct.h>
#include <CATMathTransformation.h>

#include "DNBDeviceItfCPP.h"


#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByDNBDeviceItfCPP IID IID_DNBIBasicCompoundJoint;
#else
extern "C" const IID IID_DNBIBasicCompoundJoint;
#endif

/**
 * Interface to manipulate compound joint.
 * <b>Role:</b>
 * This interface provides methods to access attributes and operation related 
 * to the compound joint.
 */
class ExportedByDNBDeviceItfCPP DNBIBasicCompoundJoint : public CATBaseUnknown
{
    CATDeclareInterface;

public:
    /**
    * Get number of Opcodes.
    * @return size_t
    */
    virtual size_t
    GetOpcodeCount() const = 0;

    /**
    * Get number of Expressions.
    * @return int
    */
    virtual int
    GetExpressionCount() const = 0;

    /**
    * get Opcode of indexed position in list for this Compound joint.
    * @param idx
    *   This parameter contains index of the opcode in list to get.
    * @param opcode
    *   This outer parameter contains opcode retrieved.
    * @param expressions
    *   This outer parameter contains expression of the operation.
    * @param dofIdx
    *   This outer parameter contains index of this opcode in the list.
    * @param inExpression
    *   This outer parameter contains whether use expression.
    * @return HRESULT indicating success or failure
	*  
    */
    virtual HRESULT
    GetOpcode( size_t index, DNBCompoundJointOpcode &opcode,
               CATListOfCATUnicodeString &expressions,
               CATListOfInt &dofIdx,
	       CATBoolean &inExpression ) const = 0;

    /**
    * Get inner part of the joint.
    * @return 
	*  interface pointer to inner part.
    */
   virtual CATIProduct_var
    GetInnerPart( ) const = 0;

    /**
    * Get outer part of the joint.
    * @return 
	*  interface pointer to outer part.
    */
    virtual CATIProduct_var
    GetOuterPart( ) const = 0;

    /**
    * Get position of inner part of the joint.
    * @return 
	*  transformation of inner part location.
    */
    virtual CATMathTransformation
    GetInnerOffset( ) const = 0;

    /**
    * Get position of outer part of the joint.
    * @return 
	*  transformation of outer part location.
    */
    virtual CATMathTransformation
    GetOuterOffset( ) const = 0;

    /**
    * Add a new Opcode to end of list for this Compound joint.
    * @param opcode
    *   This parameter contains opcode to add.
    * @param expressions
    *   This parameter contains expression of the operation.
    * @param dofIdx
    *   This outer parameter contains index of this new opcode in the list.
    * @param inExpression
    *   This parameter contains whether use expression.
    * @return HRESULT indicating success or failure
	*  
    */
    virtual HRESULT
    AddOpcode( DNBCompoundJointOpcode opcode,
               const CATListOfCATUnicodeString &expressions,
               const CATListOfInt &dofIdx,
	       CATBoolean inExpression ) = 0;

 
    /**
    * insert a new Opcode before index in list for this Compound joint.
    * @param idx
    *   This parameter contains index in opcode list to insert before.
    * @param opcode
    *   This parameter contains opcode to insert.
    * @param expressions
    *   This parameter contains expression of the operation.
    * @param dofIdx
    *   This outer parameter contains index of this new opcode in the list.
    * @param inExpression
    *   This parameter contains whether use expression.
    * @return HRESULT indicating success or failure
	*  
    */

    virtual HRESULT
    InsertOpcode( size_t idx,
	          DNBCompoundJointOpcode opcode,
                  const CATListOfCATUnicodeString &expressions,
                  const CATListOfInt &dofIdx,
	          CATBoolean inExpression ) = 0;

   /**
    * Remove Opcode applied to the compound joint.
    * @param idx
    *   This parameter contains index of the Opcode to be removed.    
    * @return HRESULT indicating success or failure
    */
    virtual HRESULT
    RemoveOpcode( size_t idx ) = 0;

    /**
    * modify Opcode of indexed position in list for this Compound joint.
    * @param idx
    *   This parameter contains index in opcode list to be modified.
    * @param opcode
    *   This parameter contains new opcode .
    * @param expressions
    *   This parameter contains expression of the operation.
    * @param dofIdx
    *   This outer parameter contains index of this opcode in the list.
    * @param inExpression
    *   This parameter contains whether use expression.
    * @return HRESULT indicating success or failure
	*  
    */
    virtual HRESULT
    ModifyOpcode( size_t idx,
	          DNBCompoundJointOpcode opcode,
                  const CATListOfCATUnicodeString &expressions,
                  const CATListOfInt &dofIdx,
	          CATBoolean inExpression ) = 0;

    /**
    * Sets the inner part of the joint
    * @param innerPart
    *   This parameter contains link to inner part.
    * @return  HRESULT indicating success or failure
	*  
    */
    virtual HRESULT
    SetInnerPart( CATIProduct_var innerPart ) = 0;

    /**
    * Sets the outer part of the joint
    * @param outerPart
    *   This parameter contains link to outer part.
    * @return  HRESULT indicating success or failure
	*  
    */
    virtual HRESULT
    SetOuterPart( CATIProduct_var outerPart ) = 0;

    /**
    * Sets the position of inner part of the joint
    * @param xform
    *   This parameter contains position transformation.
    * @return  HRESULT indicating success or failure
	*  
    */
    virtual HRESULT
    SetInnerOffset( const CATMathTransformation &xform ) = 0;

    /**
    * Sets the position of outer part of the joint
    * @param xform
    *   This parameter contains position transformation.
    * @return  HRESULT indicating success or failure
	*  
    */
    virtual HRESULT
    SetOuterOffset( const CATMathTransformation & xform ) = 0;
};


CATDeclareHandler( DNBIBasicCompoundJoint, CATBaseUnknown );


#endif /* _DNBIBASICCOMPOUNDJOINT_H_ */
