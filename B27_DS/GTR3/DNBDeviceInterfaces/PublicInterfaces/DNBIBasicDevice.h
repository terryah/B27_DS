// COPYRIGHT Dassault Systemes 2002
//*
//* FILE:
//*     DNBIBasicDevice.h    - protected header file
//*
//* MODULE:
//*     DNBDeviceInterfaces
//*
//* OVERVIEW:
//*
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     xxx         03/16/2000  Initial Implementation
//*     gsx         02/09/2003  Added functions for limit margin
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1999, 2000 Delmia, CORP.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Delmia, CORP., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*

#ifndef _DNBIBASICDEVICE_H_
#define _DNBIBASICDEVICE_H_

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */


#include <CATBaseUnknown.h>
#include <CATBooleanDef.h>
//#include <CATISiLink.h>
#include <CATMathTransformationList.h>
#include "DNBHomePosList.h"
#include "DNBIBasicCompoundJointList.h"

#include "DNBDeviceItfCPP.h"


#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByDNBDeviceItfCPP IID IID_DNBIBasicDevice;
#else
extern "C" const IID IID_DNBIBasicDevice;
#endif

/**
 * Interface to access Device attributes.
 * <b>Role:</b>
 * This interface provides methods to get/set data related to the
 * device.
 */

class ExportedByDNBDeviceItfCPP DNBIBasicDevice : public CATBaseUnknown
{
 /**
 * @nodoc
 */
   CATDeclareInterface;

public:
    /**
    * Get number of home positions the device has.
    * @return size_t
    */
    virtual size_t
    GetHomePositionCount( ) const = 0;

   /**
    * Get list of home positions of the device.
    * @param result
    *   This outer parameter contains list of home position .    
    * @return void 
    */
    virtual HRESULT 
    GetHomePositions( DNBHomePosList &result ) const = 0;

   /**
    * Set home position of the device.
    * @param hName
    *   This parameter contains name of home position .    
    * @param dbTrans
    *   This parameter contains distance data in Meters 
	*    or angular data in degree radian.    
    * @return bool
	*  indicate whether function succeed.
    */

    virtual boolean
    SetHomePosition (CATUnicodeString hName, CATListOfDouble & dbTrans) = 0;

   /**
    * Remove home position of the device.
    * @param hName
    *   This parameter contains name of home position to be removed.    
    * @return bool
	*  indicate whether function succeed.
    */
    virtual boolean
    RemoveHomePosition(CATUnicodeString hName) = 0;

   /**
    * Set DOF valuses of the device.
    * @param dbTrans
    *   This parameter contains DOF values to be set.
    * @return bool
	*  indicate whether function succeed.
    */
    virtual boolean
    SetDeviceDOFValues (CATListOfDouble & dbTrans)= 0;

    /**
    * Get DOF valuses of the device.
    * @param dbTrans
    *   This outer parameter contains DOF values of device.
    * @return bool
	*  indicate whether function succeed.
    */
   virtual boolean
    GetDeviceDOFValues (CATListOfDouble & dbTrans) const = 0;

    /**
    * Get default DOF rounding valuses of the device.
    * @return double
	*  default DOF rounding value, from 0-1.
    */
    virtual double
    GetDefaultDOFRounding( ) const = 0;
/*
    virtual DNBSimTime
    GetDOFPlannerPeriod( ) const = 0;

    virtual DNBSimTime
    GetControllerPeriod( ) const = 0;

    virtual DNBSimTime
    GetMinDOFMotionTime( ) const = 0;
*/
    /**
    * Get default DOF speed for given DOF.
    * @param idx
    *   This parameter contains index of DOF.
    * @return double
	*  default DOF speed for the joint.
    */
    virtual double
    GetDefaultDOFSpeed( size_t idx ) const = 0;

    /**
    * Get max DOF speed for given DOF.
    * @param idx
    *   This parameter contains index of DOF.
    * @return double
	*  max DOF speed for the joint.
    */
    virtual double
    GetMaxDOFSpeed( size_t idx ) const = 0;
  
    /**
    * Get max DOF acceleration for given DOF.
    * @param idx
    *   This parameter contains index of DOF.
    * @return double
	*  max DOF acceleration for the joint.
    */
    virtual double
    GetMaxDOFAccel( size_t idx ) const = 0;
/*
    virtual DNBSimTime
    GetMaxDOFAccelTime( size_t idx ) const = 0;
*/
    /**
    * @nodoc  GetCmdLimitExpressions should be used in its place.
    * Get DOF travel limit for given DOF.
    * @param index
    *   This parameter contains index of DOF.
    * @param minDefined
    *   This outer parameter contains whether low boundary is defined.
    * @param min
    *   This outer parameter contains minimal value for travel limit.
    * @param maxDefined
    *   This outer parameter contains whether high boundary is defined.
    * @param max
    *   This puter parameter contains maximal value for travel limit.
    * @return void
	*  
    */
    virtual HRESULT
    GetDOFTravelLimits( size_t index, boolean &minDefined, double &min,
                        boolean &maxDefined, double &max ) const = 0; 

    /**
    * @nodoc  GetCmdLimitExpressions should be used in its place.
    * Get DOF travel limit in string format for given DOF.
    * @param index
    *   This parameter contains index of DOF.
    * @param minDefined
    *   This outer parameter contains whether low boundary is defined.
    * @param min
    *   This outer parameter contains minimum for travel limit.
    * @param maxDefined
    *   This outer parameter contains whether high boundary is defined.
    * @param max
    *   This puter parameter contains maximal value for travel limit.
    * @return void
	*  
    */
    virtual HRESULT
    GetDOFTravelExpressions( size_t index,
                     boolean &minDefined, CATUnicodeString &min,
                     boolean &maxDefined, CATUnicodeString &max ) const = 0;
/*
    virtual void
    GetDOFLagSettleTimes( size_t index,
                          DNBSimTime &lag, DNBSimTime &settle ) const = 0;
*/
    /**
    * Get fixed part of the device.
    * @return 
	*  interface pointer to fixed part.
    */
    virtual CATBaseUnknown_var
    GetFixedPart( ) const = 0;

    /**
    * Get base part of the device.
    * @return 
	*  interface pointer to base part.
    */
    virtual CATBaseUnknown_var
    GetBaseLink( ) const = 0;

    /**
    * Get mount part of the device.
    * @return 
	*  interface pointer to mount part.
    */
    virtual CATBaseUnknown_var
    GetMountLink( ) const = 0;

    /**
    * Get mount offset of the device.
    * @return 
	*  transformation of mount offset.
    */

    virtual CATMathTransformation
    GetMountOffset( ) const = 0;

    /**
    * Get tool frame of the device.
    * @return 
	*  transformation of device tool frame.
    */
    virtual CATMathTransformation
    GetToolFrame( ) const = 0;

    /**
    * Get list of tool frames of the device.
	* @param toolFrameList
	*  the outer parammeter contains list of raw pointers point to
	*  CATMathTransformations. caller need to delete these CATMathTransformations
    * @return void 
	*  
    */
    virtual HRESULT
    GetToolFrameList( CATMathTransformationList &toolFrameList ) const = 0;

    /**
    * Get number of Compound Joint deivce has.
    * @return size_t 
	*  number of Compound Joint.
	*/
    virtual size_t
    GetCompoundJointCount() const = 0;

    /**
    * Get list of compound joints the device has, and size of it.
	* @param result
	*  the outer parameter contains list of compound joints
	* @param expressionJointCount
	*  the outer parameter contains size of the list. 
    * @return void 
	*  
    */
    virtual HRESULT
    GetCompoundJoints( DNBIBasicCompoundJointList &result,
	               size_t &expressionJointCount ) const = 0;

    /**
    * Get the limit margin for a given DOF
    * @param position
    *   This parameter contains index of DOF.
    * @param oIsAbsoluteValue
    *   This outer parameter tells whether the limit margin was specified
    *	in terms of absolute value or not
    * @param oMarginValue
    *   This outer parameter is the margin value for given DOF.
    * @return  HRESULT indicating success or failure
	*  
    */
    virtual HRESULT GetLimitMargin( int position, boolean &oIsAbsoluteValue, 
			    double &oMarginValue) = 0;


    /**
    * Sets the limit margin for a given DOF
    * @param position
    *   This parameter contains index of DOF.
    * @param iMarginValue
    *   This parameter is the margin value to be set for given DOF.
    * @return  HRESULT indicating success or failure
	*  
    */
    virtual HRESULT SetLimitMargin( int position, double iMarginValue ) = 0;

    /**
    * Sets the limit margin mode for a given DOF
    * @param isAbsoluteValue
    *   This parameter is the margin mode to be set for given DOF.
    * @return  HRESULT indicating success or failure
	*  
    */
    virtual HRESULT SetLimitMarginMode( boolean isAbsoluteValue ) = 0;

    /**
     * @nodoc
     * Get the manufacturers limits for a given DOF.  This is used with the reset 
     * button of the travel limits dialog (i.e. internal use only).
     */
    virtual HRESULT GetManufacturersLimits( int Position, double &lowerLimit, 
					double &upperLimit ) = 0;



	/**
     * Set user defined base placement frame
	 * @param Frame
	 *	Base placement frame
	 * @return  HRESULT indicating success or failure
     */
	 virtual HRESULT SetBasePlacementFrame ( const CATBaseUnknown_var &Frame ) = 0;

	 /**
     * Get base placement frame
	 * @param Frame
	 *	Base placement frame
	 * @param spFather
	 *   Father of base placement frame
	 * @return  HRESULT indicating success or failure
     */
     virtual HRESULT GetBasePlacementFrame ( CATBaseUnknown_var & Frames , CATBaseUnknown_var  spFather = NULL_var) const = 0;
     
	 /**
	  * Remove base placement frame
      *  @return HRESULT indicating success or failure  
	  */
	 virtual HRESULT RemoveBasePlacementFrame (  ) const = 0;

	 /**
     * Set user defined mount placement frame
	 * @param Frame
	 *	Mount placement frame
	 * @return  HRESULT indicating success or failure
     */

	 virtual HRESULT SetMountPlacementFrame ( const CATBaseUnknown_var &Frame ) = 0;

	 /**
     * Get all mount placement frames
	 * @param Frames
	 *	Mount placement frames
	 * @param spFather
	 *   Father of mount placement frames
	 * @return  HRESULT indicating success or failure
     */
     virtual HRESULT GetMountPlacementFrames (CATListValCATBaseUnknown_var & Frames , CATBaseUnknown_var  spFather = NULL_var) const = 0 ;
	
	 /**
	  * Remove all mount placement frames
      *  @return HRESULT indicating success or failure  
	  */

	virtual HRESULT RemoveAllMountPlacementFrames ( ) const = 0; 

    /**
    * Set max DOF speed for given DOF.
    * @param idx
    *   This parameter contains index of DOF.
    * @param MaxDOFSpeed
    *   This parameter is the value to be set as Max.
    *   DOF speed
    *
    * NOTE: Units are determined by session tools-->Options settings
    */

    virtual HRESULT SetMaxDOFSpeed( size_t idx, double MaxDOFSpeed ) = 0;
  
    /**
    * Set max DOF acceleration for given DOF.
    * @param idx
    *   This parameter contains index of DOF.
    * @param MaxDOFAccel
    *   This parameter is the value to be set as Max.
    *   DOF acceleration
    *
    * NOTE: Units are determined by session tools-->Options settings
    */
    virtual HRESULT SetMaxDOFAccel( size_t idx, double MaxDOFAccel ) = 0;


    /**
     * Sets the expressions for given Joint's travel limit.
     * @param jtNumber
     *   Used to specify which joint that the travel limits are for.
     * @param hasLowerLimit
     *   Used to indicate if the lower boundary is defined.
     * @param lowerLimitExpr
     *   Contains the minimum travel limit expression.
     * @param hasUpperLimit
     *   Used to indicate if the low boundary is defined.
     * @param upperLimitExpr
     *   Contains the maximum travel limit expression.
     *
     * <br>Note: Expression must start with an equals sign (that is '=').
     * If no equals sign is found then the limit is assumed to be a constant
     * value.
     */
    virtual HRESULT SetCmdLimitExpressions (int jtNumber,
                                            CATBoolean hasLowerLimit, CATUnicodeString lowerLimitExpr,
                                            CATBoolean hasUpperLimit, CATUnicodeString upperLimitExpr) = 0;

    /**
     * Gets the expressions for given Joint's travel limit.
     * @param jtNumber
     *   Used to specify which joint that the travel limits are for.
     * @param hasLowerLimit
     *   Used to indicate if the lower boundary is defined.
     * @param lowerLimitExpr
     *   Contains the minimum travel limit expression.
     * @param hasUpperLimit
     *   Used to indicate if the low boundary is defined.
     * @param upperLimitExpr
     *   Contains the maximum travel limit expression.
     * @param iForWDM
     *   Used to specify if constant limits should be returned either
     *   in radians (true) or degrees (false).
     *
     * <br>Note: Expression start with an equals sign (that is '='). If no
     * equals sign is found then the limit is assumed to be a constant
     * value.
     */
    virtual HRESULT GetCmdLimitExpressions (int jtNumber,
                                            CATBoolean &hasLowerLimit, CATUnicodeString &lowerLimitExpr,
                                            CATBoolean &hasUpperLimit, CATUnicodeString &upperLimitExpr,
                                            CATBoolean iForWDM) = 0;


    /**
     * @nodoc
     * Get the manufacturers limits for a given DOF.  This is used with the reset 
     * button of the travel limits dialog (i.e. internal use only).
     */
     virtual HRESULT GetManufacturersLimits (int Position,
                                             CATUnicodeString &lowerLimitExpr,
                                             CATUnicodeString &upperLimitExpr) = 0;

};

/**
 * @nodoc
 */

CATDeclareHandler( DNBIBasicDevice, CATBaseUnknown );


#endif /* _DNBIBASICDEVICE_H_ */
