/* -*-c++-*- */
// COPYRIGHT DASSAULT SYSTEMES / DELMIA Corp 2001
//=============================================================================
//
// DNBIGenericToolProfile.h
//
//=============================================================================
//
// Usage notes:
//
//=============================================================================
// ???. ????  Creation                                                      ???
//
// Jun. 2001  Modification                                                  awn
//              Implementation of new methods for XML streaming
//                -> SaveXML
//                -> LoadXML
//*		xya			10/01/2003	remove XML support
// Mar  2005  Non RTCP ToolProfile changes                                  NPJ
// June 2005  Added two APIs, to set and get the 
//            the attribute "ProfileIndex"                                  pjr
// March 2006 Added two APIs to get and get Kinematic Chain 
//            for the tool profile                                          pjr
//=============================================================================
#ifndef DNBIGENERICTOOLPROFILE_H
#define DNBIGENERICTOOLPROFILE_H

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */


#include <CATBaseUnknown.h>
#include <CATBooleanDef.h>
#include <DNBDeviceItfCPP.h>
#include <CATLISTV_CATBaseUnknown.h>

class CATUnicodeString;
class DNBIRobGenericController_var;
//class DOM_Document;
//class DOM_Element;


extern ExportedByDNBDeviceItfCPP IID IID_DNBIGenericToolProfile;



//------------------------------------------------------------------
/**
 * Interface to access Tool Profile.
 * <b>Role:</b>
 * This interface provides methods to get/set data related to the
 * Tool Profile.
 */
class ExportedByDNBDeviceItfCPP DNBIGenericToolProfile: public CATBaseUnknown 
{
/**
 * @nodoc
 */
	CATDeclareInterface;

  public:

   /**
    * Set name of the Tool Profile.
    * @param Name
    *   name of the profile to be set.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */      
    virtual HRESULT 
    SetName( const CATUnicodeString& Name) = 0;

   /**
    * Retrieves the name of the Profile.
    * @param Name
    *   This out parameter contains name of the Profile.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual HRESULT 
    GetName( CATUnicodeString& Name) const =0;
    
   /**
    * Set tool mobility.
    * @param mobile
    *   set whether tool is mobile or not.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */  
    virtual HRESULT
    SetToolMobility( CATBoolean mobile )  const = 0;

   /**
    * Retrieves tool mobility.
    * @param mobile
    *   This out parameter contains whether tool is mobiled.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual HRESULT 
    GetToolMobility( CATBoolean& mobile ) const = 0;

   /**
    * Set the underlying x,y,z, roll, pitch, yaw of the tool TCP offset.
    * @param x,
    *   The X Coordinate.
    * @param y,
    *   The Y Coordinate.
    * @param z,
    *   The Z Coordinate.
    * @param roll,
    *   The roll Coordinate.
    * @param pitch,
    *   The pitch Coordinate.
    * @param yaw,
    *   The yaw Coordinate.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
    *   </dl> 
    */
    virtual HRESULT
    SetTCPOffset( double x, double y, double z,
	          double roll, double pitch, double yaw ) const = 0;
	
   /**
    * Retrieve the underlying x,y,z, roll, pitch, yaw of the Tool TCP offset.
    * @param x,
    *   The out parameter contains X Coordinate.
    * @param y,
    *   The out parameter contains Y Coordinate.
    * @param z,
    *   The out parameter contains Z Coordinate.
    * @param roll,
    *   The out parameter contains roll Coordinate.
    * @param pitch,
    *   The out parameter contains pitch Coordinate.
    * @param yaw,
    *   The out parameter contains yaw Coordinate.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
    *   </dl> 
    */
    virtual HRESULT
    GetTCPOffset( double& x, double& y, double& z,
	          double& roll, double& pitch, double& yaw ) const = 0;

//GRN : Added this Method
    
   /**
    * Set the reference of the Tool Profile.
    * @param Protocol
    *   This parameter contains protocol to use.
    * @param iRef
    *   This parameter contains pointer to reference.
    * @param index
    *   This parameter contains index.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual HRESULT
    SetReference( int Protocol, CATBaseUnknown * iRef, int index ) = 0;

//GRN : End Addition

    // SAX Added this method
   /**
    * Retrieves the reference of the Profile.
    * @param oRef
    *   This out parameter contains pointer to reference.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual HRESULT
    GetReference( CATBaseUnknown ** oRef ) = 0;

   /**
    * Set tool mass.
    * @param mass
    *   set mass of tool .
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */  
    virtual HRESULT
    SetMass( double mass ) const = 0;

   /**
    * Retrieves the mass of the tool.
    * @param mass
    *   This out parameter contains mass of the tool.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual HRESULT
    GetMass( double& mass ) const = 0;

   /**
    * Set the tool centroid of Profile.
    * @param cx,
    *   The X Coordinate of tool centroid.
    * @param cy,
    *   The Y Coordinate of tool centroid.
    * @param cz,
    *   The Z Coordinate of tool centroid.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
    *   </dl> 
    */    virtual HRESULT
    SetCentroid( double cx, double cy, double cz ) = 0;
 
   /**
    * Retrieve the underlying x,y,z of the Tool centroid.
    * @param cx,
    *   The out parameter contains X Coordinate of the Tool centroid.
    * @param cy,
    *   The out parameter contains Y Coordinate of the Tool centroid.
    * @param cz,
    *   The out parameter contains Z Coordinate of the Tool centroid.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
    *   </dl> 
    */
    virtual HRESULT
    GetCentroid( double& cx, double& cy, double& cz ) const = 0;

   /**
    * Set the underlying coefficient of the tool inertia.
    * @param Ixx,
    *   The Ixx coefficient.
    * @param Iyy,
    *   The Ixx coefficient.
    * @param Izz,
    *   The Ixx coefficient.
    * @param Ixy,
    *   The Ixx coefficient.
    * @param Iyz,
    *   The Ixx coefficient.
    * @param Izx,
    *   The Ixx coefficient.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
    *   </dl> 
    */
    virtual HRESULT
    SetInertia( double Ixx, double Iyy, double Izz,
	        double Ixy, double Iyz, double Izx ) = 0;
 
   /**
    * Get the underlying coefficient of the tool inertia.
    * @param Ixx,
    *   The Ixx coefficient.
    * @param Iyy,
    *   The Ixx coefficient.
    * @param Izz,
    *   The Ixx coefficient.
    * @param Ixy,
    *   The Ixx coefficient.
    * @param Iyz,
    *   The Ixx coefficient.
    * @param Izx,
    *   The Ixx coefficient.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
    *   </dl> 
    */ 
	virtual HRESULT
    GetInertia( double& Ixx, double& Iyy, double& Izz,
	        double& Ixy, double& Iyz, double& Izx ) const = 0;

   /**
    * Retrieves controller which contains the profile.
    * @param spICntlr
    *   This out parameter contains smart pointer to controller.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual  HRESULT 
    GetController( DNBIRobGenericController_var& spICntlr ) const = 0;


    /**
    * Sets the Fixed and Mobile Tool profiles.
    * @param iToolProfiles
    *   This in parameter(List) contains Fixed and Mobile Tool profiles.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
    *   </dl> 
    */
   virtual HRESULT
      SetNonRTCPProfiles(CATListValCATBaseUnknown_var& iToolProfiles) = 0;

   /**
    * Retrieves the Fixed and Mobile Tool profiles.
    * @param iToolProfiles
    *   This Out parameter(List) contains Fixed and Mobile Tool profiles.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
    *   </dl> 
    */
   virtual HRESULT
      GetNonRTCPProfiles(CATListValCATBaseUnknown_var& oToolProfiles) const = 0;

  /**@nodoc
    * Internal Use. ONLY FOR WORKCELL SEQUENCING.
    * METHODS : 
    *   <b>SaveXML</b>
    * <br>
    * DESCRIPTION :
    *   Stream the tool profile data in an XML node. The method needs
    * to know in which XML document nodes can be created and under
    * which node the tool profile data will be added.
    * 
    *@param <i>doc</i>
    *   DOM_Document. XML document where data will be saved.
    *@param <i>rootElem</i>
    *   DOM_Element. XML node where data will be plugged under.
    *
    *@see DOM_Document in XMLParser.
    *@see DOM_Element in XMLParser.
    *
    */
  //  virtual HRESULT SaveXML(DOM_Document& doc, DOM_Element& rootElem) = 0;



  /**@nodoc
    * Internal Use. ONLY FOR WORKCELL SEQUENCING.
    * METHODS : 
    *   <b>LoadXML</b>
    * <br>
    * DESCRIPTION :
    *   Unstream the tool profile data from an XML node. The method will
    * create a new tool profile based on the data contained in the XML
    * node.
    * 
    *@param <i>rootElem</i>
    *   DOM_Element. XML node where data will be read.
    *
    *@see DOM_Document in XMLParser.
    *@see DOM_Element in XMLParser.
    *
    */
//    virtual HRESULT LoadXML(DOM_Element& rootElem) = 0;

	/**
    * Sets the (unique) tool profile index
    * @param iProfileIndex ( integer )
    *   This input parameter is the user defined tool profile index
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>If tool profile index has been successfull set
    *     </dd>
    *     <dt>E_FAIL </dt>
    *     <dd>If the setting of tool profile index failed </dd>
    *   </dl> 
    */

   virtual HRESULT
	   SetToolProfileIndex( const int& iProfileIndex ) = 0;

   /**
    * Returns the (unique) tool profile index
    * @param oProfileIndex ( integer )
    *   This output parameter contains the tool profile index
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>If the tool profile index has been successfull returned
    *     </dd>
    *     <dt>E_FAIL </dt>
    *     <dd>If the retrieval of the tool profile index failed </dd>
    *   </dl> 
    */

   virtual HRESULT
	   GetToolProfileIndex( int& iProfileIndex ) const = 0;

   /**
    * Sets an unique Kinematic Chain to the tool profile
    * @param iSpCBUKinChain ( CATBaseUnknown_var )
    *   This input parameter is the handle to the Kinematic Chain
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>If the kinematic chain is successfully set for the 
	*          tool profile.
    *     </dd>
    *     <dt>E_FAIL </dt>
    *     <dd>If the setting of kinematic chain for the tool profile 
	*          is not successful </dd>
    *   </dl> 
    */
   virtual HRESULT 
	   SetKinChain( const CATBaseUnknown_var& iSpCBUKinChain ) = 0;

   /**
    * To get the unique Kinematic Chain of the tool profile
    * @param oSpCBUKinChain ( CATBaseUnknown_var )
    *   This output parameter is the handle obtained to the Kinematic Chain
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>If the kinematic chain is successfully obtained for the 
	*          tool profile.
    *     </dd>
    *     <dt>E_FAIL </dt>
    *     <dd>If the retrieval of kinematic chain for the tool profile 
	*          is not successful </dd>
    *   </dl> 
    */
   virtual HRESULT 
	   GetKinChain( CATBaseUnknown_var& oSpCBUKinChain )const = 0;

   /**
   * @nodoc
   */
   virtual HRESULT
      GetProtocol( int &oProtocol) = 0;

   /**
   * @nodoc
   */
   virtual HRESULT
      SetProtocol( int iProtocol) = 0;
   
};

//------------------------------------------------------------------
/**
 * @nodoc
 */
CATDeclareHandler( DNBIGenericToolProfile, CATBaseUnknown );

#endif // DNBIGENERICTOOLPROFILE_H

