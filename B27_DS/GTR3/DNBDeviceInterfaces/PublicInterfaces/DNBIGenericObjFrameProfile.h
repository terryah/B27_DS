/* -*-c++-*- */
// COPYRIGHT DASSAULT SYSTEMES / DELMIA Corp 2001
//=============================================================================
//
// DNBIGenericObjFrameProfile.h
//
//=============================================================================
//
// Usage notes:
//
//=============================================================================
// ???. ????  Creation                                                      ???
//
// Jun. 2001  Modification                                                  awn
//              Implementation of new methods for XML streaming
//                -> SaveXML
//                -> LoadXML
//*		xya			10/01/2003	remove XML support
// June 2005  Added two APIs, to set and get the 
//            the attribute "ProfileIndex"                                  pjr
//=============================================================================
#ifndef DNBIGENERICOBJFRAMEPROFILE_H
#define DNBIGENERICOBJFRAMEPROFILE_H

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

#include <CATBaseUnknown.h>
#include <CATBooleanDef.h>
#include <DNBDeviceItfCPP.h>


class CATUnicodeString;
class DNBIGenericObjFrameProfile_var;
class DNBIRobGenericController_var;
//class DOM_Document;
//class DOM_Element;


extern ExportedByDNBDeviceItfCPP IID IID_DNBIGenericObjFrameProfile;



//------------------------------------------------------------------
/**
 * Interface to access Object Frame Profile.
 * <b>Role:</b>
 * This interface provides methods to get/set data related to the
 * Object Frame Profile.
 */
class ExportedByDNBDeviceItfCPP DNBIGenericObjFrameProfile: public CATBaseUnknown 
{
  /**
 * @nodoc
 */
	CATDeclareInterface;

    public:

   /**
    * Set name of the Object Frame Profile.
    * @param Name
    *   name of the profile to be set.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */      
    virtual HRESULT
    SetName( CATUnicodeString Name) = 0;

   /**
    * Retrieves the name of the Profile.
    * @param Name
    *   This out parameter contains name of the Profile.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual HRESULT
    GetName( CATUnicodeString& Name) const = 0;

   /**
    * Set the underlying x,y,z, roll, pitch, yaw of the Object Frame.
    * @param x,
    *   The X Coordinate.
    * @param y,
    *   The Y Coordinate.
    * @param z,
    *   The Z Coordinate.
    * @param roll,
    *   The roll Coordinate.
    * @param pitch,
    *   The pitch Coordinate.
    * @param yaw,
    *   The yaw Coordinate.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
    *   </dl> 
    */
    virtual HRESULT
    SetObjectFrame( double x, double y, double z,
		    double roll, double pitch, double yaw ) const = 0;
	
   /**
    * Retrieve the underlying x,y,z, roll, pitch, yaw of the Object Frame.
    * @param x,
    *   The out parameter contains X Coordinate.
    * @param y,
    *   The out parameter contains Y Coordinate.
    * @param z,
    *   The out parameter contains Z Coordinate.
    * @param roll,
    *   The out parameter contains roll Coordinate.
    * @param pitch,
    *   The out parameter contains pitch Coordinate.
    * @param yaw,
    *   The out parameter contains yaw Coordinate.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
    *   </dl> 
    */
    virtual HRESULT
    GetObjectFrame( double& x, double& y, double& z,
	            double& roll, double& pitch, double& yaw ) const = 0;
 
   /**
    * Retrieves controller which contains the profile.
    * @param spICntlr
    *   This out parameter contains smart pointer to controller.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual  HRESULT 
    GetController( DNBIRobGenericController_var& spICntlr ) const = 0;

    /**
    * Sets the offset mode of the Object Frame.
    * @param iOffsetMode
    *   This in parameter defines offset mode for the targets.
    * if iOffsetMode is TRUE offset will be applied to all Targets
    * defined in the Cartesian Space, that is Tag, Weld.
    * if iOffsetMode is FALSE then the offset will be applied 
    * to the "Cartesian Targets" only.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
    *   </dl> 
    */
    virtual  HRESULT 
    SetOffsetTargetMode( CATBoolean& iOffsetMode ) = 0;

    /**
    * Retrieves the offset mode of the Object Frame.
    * @param oOffsetMode
    *   This out parameter containes offset mode for the targets.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
    *   </dl> 
    */
    virtual  HRESULT 
    GetOffsetTargetMode( CATBoolean& oOffsetMode ) const = 0;

  /**@nodoc
    * Internal Use. ONLY FOR WORKCELL SEQUENCING.
    * METHODS : 
    *   <b>SaveXML</b>
    * <br>
    * DESCRIPTION :
    *   Stream the object frame profile data in an XML node. The method needs
    * to know in which XML document nodes can be created and under
    * which node the object frame profile data will be added.
    * 
    *@param <i>doc</i>
    *   DOM_Document. XML document where data will be saved.
    *@param <i>rootElem</i>
    *   DOM_Element. XML node where data will be plugged under.
    *
    *@see DOM_Document in XMLParser.
    *@see DOM_Element in XMLParser.
    *
    */
//    virtual HRESULT SaveXML(DOM_Document& doc, DOM_Element& rootElem) = 0;



  /**@nodoc
    * Internal Use. ONLY FOR WORKCELL SEQUENCING.
    * METHODS : 
    *   <b>LoadXML</b>
    * <br>
    * DESCRIPTION :
    *   Unstream the object frame profile data from an XML node. The method will
    * create a new object frame profile based on the data contained in the XML
    * node.
    * 
    *@param <i>rootElem</i>
    *   DOM_Element. XML node where data will be read.
    *
    *@see DOM_Document in XMLParser.
    *@see DOM_Element in XMLParser.
    *
    */
  //  virtual HRESULT LoadXML(DOM_Element& rootElem) = 0;

	/**
    * Sets the (unique) ObjFrame profile index
    * @param iProfileIndex ( integer )
    *   This input parameter is the user defined ObjFrame profile index
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>If the ObjFrame profile index has been successfull set
    *     </dd>
    *     <dt>E_FAIL </dt>
    *     <dd>If the setting of the ObjFrame profile index failed </dd>
    *   </dl> 
    */

   virtual HRESULT
	   SetObjFrameProfileIndex( const int& iProfileIndex ) = 0;

   /**
    * Returns the (unique) ObjFrame profile index
    * @param oProfileIndex ( integer )
    *   This output parameter contains the ObjFrame profile index
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>If the ObjFrame profile index has been successfull returned
    *     </dd>
    *     <dt>E_FAIL </dt>
    *     <dd>If the retrieval of the ObjFrame profile index failed </dd>
    *   </dl> 
    */

   virtual HRESULT
	   GetObjFrameProfileIndex( int& iProfileIndex ) const = 0;

};

//------------------------------------------------------------------
/**
 * @nodoc
 */
CATDeclareHandler( DNBIGenericObjFrameProfile, CATBaseUnknown );

#endif // DNBIGENERICOBJFRAMEPROFILE_H
