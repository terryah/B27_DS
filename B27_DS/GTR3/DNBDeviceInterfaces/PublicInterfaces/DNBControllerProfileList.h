//===================================================================
//
// DNBControllerProfileList.h
//   This interface allows the management of a list of Commands.
//
//===================================================================
//
//
//===================================================================
#ifndef DNBCONTROLLERPROFILELIST_H
#define DNBCONTROLLERPROFILELIST_H


/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

#include "DNBDeviceItfCPP.h"
#include "DNBIGenericToolProfile.h"
#include "DNBIGenericMotionProfile.h"
#include "DNBIGenericAccuracyProfile.h"
#include "DNBIGenericObjFrameProfile.h"

class DNBIGenericToolProfile_var;
class DNBIGenericMotionProfile_var;
class DNBIGenericAccuracyProfile_var;
class DNBIGenericObjFrameProfile_var;

// clean previous functions requests
#include  <CATLISTV_Clean.h>

// require needed functions
#define CATLISTV_Append
#define CATLISTV_Locate
#define CATLISTV_RemoveValue
#define CATLISTV_RemoveAll

// get macros
#include  <CATLISTV_Declare.h>

// generate interface of collection-class
// (functions declarations)
#undef	CATCOLLEC_ExportedBy
#define	CATCOLLEC_ExportedBy	ExportedByDNBDeviceItfCPP

/**
 * @nodoc
 */
CATLISTV_DECLARE(DNBIGenericToolProfile_var)
typedef CATLISTV(DNBIGenericToolProfile_var) DNBGenericToolProfileList ;
CATLISTV_DECLARE(DNBIGenericMotionProfile_var)
typedef CATLISTV(DNBIGenericMotionProfile_var) DNBGenericMotionProfileList ;
CATLISTV_DECLARE(DNBIGenericAccuracyProfile_var)
typedef CATLISTV(DNBIGenericAccuracyProfile_var) DNBGenericAccuracyProfileList ;
CATLISTV_DECLARE(DNBIGenericObjFrameProfile_var)
typedef CATLISTV(DNBIGenericObjFrameProfile_var) DNBGenericObjFrameProfileList ;
#endif

