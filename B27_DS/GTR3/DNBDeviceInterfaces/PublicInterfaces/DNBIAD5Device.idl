
//===================================================================
// COPYRIGHT Dassault Systemes 2007
//===================================================================
//
// DNBIAD5Device.idl
// Automation interface for a D5 Device element 
//
//===================================================================
//
// Usage notes:
//
//===================================================================
//
//  01/08/2007  DCG     Initial Implementation
//
//===================================================================

#ifndef DNBIAD5Device_IDL
#define DNBIAD5Device_IDL

/*IDLREP*/

/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */

#include "CATIABase.idl"
#include "CATSafeArray.idl"
#include "CATBSTR.idl"

#include "DNBIABasicDevice.idl"


/**
 * Interface representing a D5 Device.
 *
 * <br><b>Role</b>: This interface is used to interact with devices that are available
 * in the Device Building workbench.  This interfaces is derived from BasicDevice to
 * support specific properties for D5 devices.
 * 
 * <br>The following code snippet can be used to obtain a device in a CATProduct 
 * document.
 * <pre>
 *   Dim objDevice As BasicDevice
 *   set objDevice = CATIA.ActiveDocument.Product.GetTechnologicalObject("BasicDevice")
 *   Dim objD5Device as D5Device
 *   Set objD5Device = objDevice
 * </pre>
 */

interface DNBIAD5Device : DNBIABasicDevice 
{

    /**
     * Get the Linked D5 Device File.
     * @param oFileName
     *   This will contain the full path to the file (file name and path) to the D5 
     *   device file.
     */
    HRESULT GetLinkedDeviceFile (inout /*IDLRETVAL*/ CATBSTR oFileName);

};


// Interface name : DNBIAD5Device
#pragma ID DNBIAD5Device "DCE:0cfaf64b-34c2-4b71-a9d3d6fea5b8b8cc"
#pragma DUAL DNBIAD5Device

// VB object name : D5Device (Id used in Visual Basic)
//#pragma ID D5Device "DCE:0cfaf64b-34c2-4b71-a9d3d6fea5b8b8cc"
#pragma ID D5Device "DCE:7eb4be4f-2e4f-4628-bcd344edb51cc945"
#pragma ALIAS DNBIAD5Device D5Device


#endif

