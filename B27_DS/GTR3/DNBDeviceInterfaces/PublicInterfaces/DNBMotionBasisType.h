#ifndef DNBMotionBasisType_h
#define DNBMotionBasisType_h

// COPYRIGHT DASSAULT SYSTEMES 2005
//--------------------------------------------------------------------------
// DNBMotionBasisType Enum
//--------------------------------------------------------------------------

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

enum MotionBasis { MOTION_ABSOLUTE, MOTION_PERCENT, MOTION_TIME };
#endif
