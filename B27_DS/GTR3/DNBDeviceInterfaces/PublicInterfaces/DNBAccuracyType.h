#ifndef DNBAccuracyType_h
#define DNBAccuracyType_h

// COPYRIGHT DASSAULT SYSTEMES 2005
//--------------------------------------------------------------------------
// DNBAccuracyType Enum
//--------------------------------------------------------------------------

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

enum AccuracyType { ACCURACY_TYPE_DISTANCE, ACCURACY_TYPE_SPEED };

#endif
