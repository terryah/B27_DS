#ifndef DNBDeviceType_H
#define DNBDeviceType_H

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

enum DNBDeviceType
{
  DNBDeviceType_Unknown,
  AllDeviceType,
  D5Devices,
  D5DevicesOnly,
  D5IKSystem,
  D5IKSystemOnly,
  D5Robot,
  V5Mechanism,
  V4Mechanism,
  V5Manikin
};

#endif
