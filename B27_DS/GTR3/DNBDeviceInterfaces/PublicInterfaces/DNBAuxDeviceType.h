#ifndef DNBAuxDeviceType_H
#define DNBAuxDeviceType_H

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

enum DNBAuxDeviceType
{
  AuxType_RailTrackGantry,
  AuxType_EndOfArmTooling,
  AuxType_WorkpiecePositioner,
  AuxType_Conveyor
};

#endif
