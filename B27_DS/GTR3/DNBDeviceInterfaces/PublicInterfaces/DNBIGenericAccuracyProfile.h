/* -*-c++-*- */
// COPYRIGHT DASSAULT SYSTEMES / DELMIA Corp 2001
//=============================================================================
//
// DNBIGenericAccuracyProfile.h
//
//=============================================================================
//
// Usage notes:
//
//=============================================================================
// ???. ????  Creation                                                      ???
//
// Jun. 2001  Modification                                                  awn
//              Implementation of new methods for XML streaming
//                -> SaveXML
//                -> LoadXML
// Jun  2005 Modification : Moved enum declarations to DNBAccuracyType.h   shl
//=============================================================================
//*		xya			10/01/2003	remove XML support
//      pjr         06/21/2005  Added two APIs, to set and get the 
//                              the attribute "ProfileIndex"
//=============================================================================
#ifndef DNBIGENERICACCURACYPROFILE_H
#define DNBIGENERICACCURACYPROFILE_H

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

#include <CATBaseUnknown.h>
#include <CATBooleanDef.h>
#include <DNBDeviceItfCPP.h>
#include <DNBAccuracyType.h>


class CATUnicodeString;
class DNBIGenericAccuracyProfile_var;
class DNBIRobGenericController_var;
//class DOM_Document;
//class DOM_Element;


extern ExportedByDNBDeviceItfCPP IID IID_DNBIGenericAccuracyProfile;


//------------------------------------------------------------------
/**
 * Interface to access Accuracy Profile.
 * <b>Role:</b>
 * This interface provides methods to get/set data related to the
 * Accuracy Profile.
 */
class ExportedByDNBDeviceItfCPP DNBIGenericAccuracyProfile: public CATBaseUnknown 
{
/**
 * @nodoc
 */
    CATDeclareInterface;

    public:
   /**
    * Set name of the Accuracy Profile.
    * @param name
    *   name of the profile to be set.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */  
    virtual HRESULT 
    SetName( CATUnicodeString name ) = 0;

   /**
    * Retrieves the name of the Profile.
    * @param name
    *   This out parameter contains name of the Profile.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual  HRESULT
    GetName( CATUnicodeString& name ) const = 0;

   /**
    * Set flyby mode of the Profile.
    * @param mode
    *   This parameter switch mode  on/off.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual  HRESULT
    SetFlyByMode( CATBoolean mode ) = 0;

   /**
    * Retrieves the flyby mode of this profile.
    * @param flymode
    *   This out parameter contains the flyby mode.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual  HRESULT 
    GetFlyByMode( CATBoolean& flyby ) const = 0;

   /**
    * Set the accuracy type for the profile.
    * @param accurancy
    *   accurancy type to set, could be ACCURACY_TYPE_DISTANCE / ACCURACY_TYPE_SPEED
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual  HRESULT
    SetAccuracyType( AccuracyType accuracy ) = 0;
   
	/**
    * Retrieves the accurancy type of profile.
    * @param type
    *   This out parameter contains accurancy type.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual  HRESULT 
    GetAccuracyType( AccuracyType& type ) const = 0;

   /**
    * Set accurancy value of the profile.
    * @param value
    *   This parameter is percentage or absolute value, depending on accurancy type.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual  HRESULT
    SetAccuracyValue( double value ) = 0;

   /**
    * Retrieves accurancy value.
    * @param value
    *   This out parameter contains accurancy value.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual  HRESULT 
    GetAccuracyValue( double& value ) const = 0;

   /**
    * Retrieves controller which contains the profile.
    * @param spICntlr
    *   This out parameter contains smart pointer to controller.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual  HRESULT 
    GetController( DNBIRobGenericController_var& spICntlr ) const = 0;
 


  /**@nodoc
    * Internal Use. ONLY FOR WORKCELL SEQUENCING.
    * METHODS : 
    *   <b>SaveXML</b>
    * <br>
    * DESCRIPTION :
    *   Stream the accuracy profile data in an XML node. The method needs
    * to know in which XML document nodes can be created and under
    * which node the accuracy profile data will be added.
    * 
    *@param <i>doc</i>
    *   DOM_Document. XML document where data will be saved.
    *@param <i>rootElem</i>
    *   DOM_Element. XML node where data will be plugged under.
    *
    *@see DOM_Document in XMLParser.
    *@see DOM_Element in XMLParser.
    *
    */
//    virtual HRESULT SaveXML(DOM_Document& doc, DOM_Element& rootElem) = 0;



  /**@nodoc
    * Internal Use. ONLY FOR WORKCELL SEQUENCING.
    * METHODS : 
    *   <b>LoadXML</b>
    * <br>
    * DESCRIPTION :
    *   Unstream the accuracy profile data from an XML node. The method will
    * create a new accuracy profile based on the data contained in the XML
    * node.
    * 
    *@param <i>rootElem</i>
    *   DOM_Element. XML node where data will be read.
    *
    *@see DOM_Document in XMLParser.
    *@see DOM_Element in XMLParser.
    *
    */
  //  virtual HRESULT LoadXML(DOM_Element& rootElem) = 0;

	/**
    * Sets the (unique) Accuracy profile index
    * @param iProfileIndex ( integer )
    *   This input parameter is the user defined Accuracy profile index
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>If the Accuracy profile index has been successfull set
    *     </dd>
    *     <dt>E_FAIL </dt>
    *     <dd>If the setting of the Accuracy profile index failed </dd>
    *   </dl> 
    */

   virtual HRESULT
	   SetAccuracyProfileIndex( const int& iProfileIndex ) = 0;

   /**
    * Returns the (unique) Accuracy profile index
    * @param oProfileIndex ( integer )
    *   This output parameter contains the Accuracy profile index
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>If the Accuracy profile index has been successfull returned
    *     </dd>
    *     <dt>E_FAIL </dt>
    *     <dd>If the retrieval of the Accuracy profile index failed </dd>
    *   </dl> 
    */

   virtual HRESULT
	   GetAccuracyProfileIndex( int& iProfileIndex ) const = 0;
};

//------------------------------------------------------------------
/**
 * @nodoc
 */
CATDeclareHandler( DNBIGenericAccuracyProfile, CATBaseUnknown );

#endif // DNBIGENERICACCURACYPROFILE_H
