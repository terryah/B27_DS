// COPYRIGHT Dassault Systemes 2004
//===================================================================
//
// DNBIADevAnalysisSettingAtt.idl
// Automation interface for the DevAnalysisSettingAtt element 
//
//===================================================================
//
// Usage notes:
//   New interface: describe its use here
//
//===================================================================
//
//  Mar 2004  Creation: Code generated by the CAA wizard  mmh
//===================================================================
#ifndef DNBIADevAnalysisSettingAtt_IDL
#define DNBIADevAnalysisSettingAtt_IDL
/*IDLREP*/

/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */

#include "CATIASettingController.idl"
#include "CATVariant.idl"
#include "CATSafeArray.idl"
#include "CATBSTR.idl"
#include "DNBAnalysisLevel.idl"

/**  
 * Interface to handle parameters of Infrastructure-DELMIA Infrastructure-Device Analysis Tools Options Tab page.
 * <b>Role</b>: This interface is implemented by a component which 
 * represents the controller of Device Analysis Tools Options parameter settings.
 * <ul>
 * <li>Methods to set value of each parameter xxx</li>
 * <li>Methods to get value of each parameter xxx</li>
 * <li>Methods to get information on each parameter xxx</li>
 * <li>Methods to lock/unlock value of each parameter xxx</li>
 * </ul>
 * Here is the list of parameters to use and their meaning:
 * <ul>
 * <li><b>TravelLimit</b>: defines what behavior you can expect when the DOF travel limits are exeeded.</li>
 * <li><b>VelocityLimit</b>: defines what behavior you can expect when the DOF speed limits are exeeded.</li>
 * <li><b>AccelLimit</b>: defines what behavior you can expect when the DOF acceleration limits are exeeded.</li>
 * <li><b>CautionZone</b>: defines what behavior you can expect when the DOF caution zone limits are exeeded.</li>
 * <li><b>TravelColor</b>: defines the highlight color to be used when the DOF travel limits are exceeded.</li>
 * <li><b>VelocityColor</b>: defines the highlight color to be used when the DOF speed limits are exceeded.</li>
 * <li><b>AccelColor</b>: defines the highlight color to be used when the DOF acceleration limits are exceeded.</li>
 * <li><b>CautionColor</b>: defines the highlight color to be used when the DOF caution zone limits are exceeded.</li>
 * </ul>
 */
interface DNBIADevAnalysisSettingAtt : CATIASettingController 
{
#pragma PROPERTY TravelLimit
    /**
     * Returns or sets the TravelLimit parameter.
     * <p>
     * Ensure consistency with the C++ interface to which the work is delegated.
     */
    HRESULT get_TravelLimit( out /*IDLRETVAL*/ DNBAnalysisLevel oLevel );

    HRESULT put_TravelLimit( in                DNBAnalysisLevel iLevel );

    /** 
     * Retrieves environment informations for the TravelLimit parameter.
     * <br><b>Role</b>:Retrieves the state of the TravelLimit parameter 
     * in the current environment. 
     * @param ioAdminLevel
     *       <br>If the parameter is locked, AdminLevel gives the administration
     *       level that imposes the value of the parameter.
     *  <br>If the parameter is not locked, AdminLevel gives the administration
     *       level that will give the value of the parameter after a reset.
     * @param ioLocked
     *      Indicates if the parameter has been locked.
     * @return 
     *      Indicates if the parameter has been explicitly modified or remain
     *      to the administrated value.
     */
    HRESULT GetTravelLimitInfo( inout             CATBSTR ioAdminLevel
                              , inout             CATBSTR ioLocked
                              , out /*IDLRETVAL*/ boolean oModified
                              );

    /**
     * Locks or unlocks the TravelLimit parameter.
     * <br><b>Role</b>:Locks or unlocks the TravelLimit parameter if it is possible
     * in the current administrative context. In user mode this method will always
     * return E_FAIL.
     * @param iLocked
     *  the locking operation to be performed
     *  <b>Legal values</b>:
     *  <br><tt>TRUE :</tt>   to lock the parameter.
     *  <br><tt>FALSE:</tt>   to unlock the parameter.
     */
    HRESULT SetTravelLimitLock( in boolean iLocked );

#pragma PROPERTY VelocityLimit
    /**
     * Returns or sets the VelocityLimit parameter.
     * <p>
     * Ensure consistency with the C++ interface to which the work is delegated.
     */
    HRESULT get_VelocityLimit( out /*IDLRETVAL*/ DNBAnalysisLevel oLevel );

    HRESULT put_VelocityLimit( in                DNBAnalysisLevel iLevel );

    /** 
     * Retrieves environment informations for the VelocityLimit parameter.
     * <br><b>Role</b>:Retrieves the state of the VelocityLimit parameter 
     * in the current environment. 
     * @param ioAdminLevel
     *       <br>If the parameter is locked, AdminLevel gives the administration
     *       level that imposes the value of the parameter.
     *  <br>If the parameter is not locked, AdminLevel gives the administration
     *       level that will give the value of the parameter after a reset.
     * @param ioLocked
     *      Indicates if the parameter has been locked.
     * @return 
     *      Indicates if the parameter has been explicitly modified or remain
     *      to the administrated value.
     */
    HRESULT GetVelocityLimitInfo( inout             CATBSTR ioAdminLevel
                                , inout             CATBSTR ioLocked
                                , out /*IDLRETVAL*/ boolean oModified
                                );

    /**
     * Locks or unlocks the VelocityLimit parameter.
     * <br><b>Role</b>:Locks or unlocks the VelocityLimit parameter if it is possible
     * in the current administrative context. In user mode this method will always
     * return E_FAIL.
     * @param iLocked
     *  the locking operation to be performed
     *  <b>Legal values</b>:
     *  <br><tt>TRUE :</tt>   to lock the parameter.
     *  <br><tt>FALSE:</tt>   to unlock the parameter.
     */
    HRESULT SetVelocityLimitLock( in boolean iLocked );

#pragma PROPERTY AccelLimit
    /**
     * Returns or sets the AccelLimit parameter.
     * <p>
     * Ensure consistency with the C++ interface to which the work is delegated.
     */
    HRESULT get_AccelLimit( out /*IDLRETVAL*/ DNBAnalysisLevel oLevel );

    HRESULT put_AccelLimit( in                DNBAnalysisLevel iLevel );

    /** 
     * Retrieves environment informations for the AccelLimit parameter.
     * <br><b>Role</b>:Retrieves the state of the AccelLimit parameter 
     * in the current environment. 
     * @param ioAdminLevel
     *       <br>If the parameter is locked, AdminLevel gives the administration
     *       level that imposes the value of the parameter.
     *  <br>If the parameter is not locked, AdminLevel gives the administration
     *       level that will give the value of the parameter after a reset.
     * @param ioLocked
     *      Indicates if the parameter has been locked.
     * @return 
     *      Indicates if the parameter has been explicitly modified or remain
     *      to the administrated value.
     */
    HRESULT GetAccelLimitInfo( inout             CATBSTR ioAdminLevel
                             , inout             CATBSTR ioLocked
                             , out /*IDLRETVAL*/ boolean oModified
                             );

    /**
     * Locks or unlocks the AccelLimit parameter.
     * <br><b>Role</b>:Locks or unlocks the AccelLimit parameter if it is possible
     * in the current administrative context. In user mode this method will always
     * return E_FAIL.
     * @param iLocked
     *  the locking operation to be performed
     *  <b>Legal values</b>:
     *  <br><tt>TRUE :</tt>   to lock the parameter.
     *  <br><tt>FALSE:</tt>   to unlock the parameter.
     */
    HRESULT SetAccelLimitLock( in boolean iLocked );

#pragma PROPERTY CautionZone
    /**
     * Returns or sets the CautionZone parameter.
     * <p>
     * Ensure consistency with the C++ interface to which the work is delegated.
     */
    HRESULT get_CautionZone( out /*IDLRETVAL*/ DNBAnalysisLevel oLevel );

    HRESULT put_CautionZone( in                DNBAnalysisLevel iLevel );

    /** 
     * Retrieves environment informations for the CautionZone parameter.
     * <br><b>Role</b>:Retrieves the state of the CautionZone parameter 
     * in the current environment. 
     * @param ioAdminLevel
     *       <br>If the parameter is locked, AdminLevel gives the administration
     *       level that imposes the value of the parameter.
     *  <br>If the parameter is not locked, AdminLevel gives the administration
     *       level that will give the value of the parameter after a reset.
     * @param ioLocked
     *      Indicates if the parameter has been locked.
     * @return 
     *      Indicates if the parameter has been explicitly modified or remain
     *      to the administrated value.
     */
    HRESULT GetCautionZoneInfo( inout             CATBSTR ioAdminLevel
                              , inout             CATBSTR ioLocked
                              , out /*IDLRETVAL*/ boolean oModified
                              );

    /**
     * Locks or unlocks the CautionZone parameter.
     * <br><b>Role</b>:Locks or unlocks the CautionZone parameter if it is possible
     * in the current administrative context. In user mode this method will always
     * return E_FAIL.
     * @param iLocked
     *  the locking operation to be performed
     *  <b>Legal values</b>:
     *  <br><tt>TRUE :</tt>   to lock the parameter.
     *  <br><tt>FALSE:</tt>   to unlock the parameter.
     */
    HRESULT SetCautionZoneLock( in boolean iLocked );

#pragma PROPERTY TravelColor
    /**
     * Returns or sets the TravelColor parameter.
     * <p>
     * Ensure consistency with the C++ interface to which the work is delegated.
     */
    HRESULT get_TravelColor( out /*IDLRETVAL*/ CATSafeArrayVariant oColor );

    HRESULT put_TravelColor( in                CATSafeArrayVariant iColor );

    /** 
     * Retrieves environment informations for the TravelColor parameter.
     * <br><b>Role</b>:Retrieves the state of the TravelColor parameter 
     * in the current environment. 
     * @param ioAdminLevel
     *       <br>If the parameter is locked, AdminLevel gives the administration
     *       level that imposes the value of the parameter.
     *  <br>If the parameter is not locked, AdminLevel gives the administration
     *       level that will give the value of the parameter after a reset.
     * @param ioLocked
     *      Indicates if the parameter has been locked.
     * @return 
     *      Indicates if the parameter has been explicitly modified or remain
     *      to the administrated value.
     */
    HRESULT GetTravelColorInfo( inout             CATBSTR ioAdminLevel
                              , inout             CATBSTR ioLocked
                              , out /*IDLRETVAL*/ boolean oModified
                              );

    /**
     * Locks or unlocks the TravelColor parameter.
     * <br><b>Role</b>:Locks or unlocks the TravelColor parameter if it is possible
     * in the current administrative context. In user mode this method will always
     * return E_FAIL.
     * @param iLocked
     *  the locking operation to be performed
     *  <b>Legal values</b>:
     *  <br><tt>TRUE :</tt>   to lock the parameter.
     *  <br><tt>FALSE:</tt>   to unlock the parameter.
     */
    HRESULT SetTravelColorLock( in boolean iLocked );

#pragma PROPERTY VelocityColor
    /**
     * Returns or sets the VelocityColor parameter.
     * <p>
     * Ensure consistency with the C++ interface to which the work is delegated.
     */
    HRESULT get_VelocityColor( out /*IDLRETVAL*/ CATSafeArrayVariant oColor );

    HRESULT put_VelocityColor( in                CATSafeArrayVariant iColor );

    /** 
     * Retrieves environment informations for the VelocityColor parameter.
     * <br><b>Role</b>:Retrieves the state of the VelocityColor parameter 
     * in the current environment. 
     * @param ioAdminLevel
     *       <br>If the parameter is locked, AdminLevel gives the administration
     *       level that imposes the value of the parameter.
     *  <br>If the parameter is not locked, AdminLevel gives the administration
     *       level that will give the value of the parameter after a reset.
     * @param ioLocked
     *      Indicates if the parameter has been locked.
     * @return 
     *      Indicates if the parameter has been explicitly modified or remain
     *      to the administrated value.
     */
    HRESULT GetVelocityColorInfo( inout             CATBSTR ioAdminLevel
                                , inout             CATBSTR ioLocked
                                , out /*IDLRETVAL*/ boolean oModified
                                );

    /**
     * Locks or unlocks the VelocityColor parameter.
     * <br><b>Role</b>:Locks or unlocks the VelocityColor parameter if it is possible
     * in the current administrative context. In user mode this method will always
     * return E_FAIL.
     * @param iLocked
     *  the locking operation to be performed
     *  <b>Legal values</b>:
     *  <br><tt>TRUE :</tt>   to lock the parameter.
     *  <br><tt>FALSE:</tt>   to unlock the parameter.
     */
    HRESULT SetVelocityColorLock( in boolean iLocked );

#pragma PROPERTY AccelColor
    /**
     * Returns or sets the AccelColor parameter.
     * <p>
     * Ensure consistency with the C++ interface to which the work is delegated.
     */
    HRESULT get_AccelColor( out /*IDLRETVAL*/ CATSafeArrayVariant oColor );

    HRESULT put_AccelColor( in                CATSafeArrayVariant iColor );

    /** 
     * Retrieves environment informations for the AccelColor parameter.
     * <br><b>Role</b>:Retrieves the state of the AccelColor parameter 
     * in the current environment. 
     * @param ioAdminLevel
     *       <br>If the parameter is locked, AdminLevel gives the administration
     *       level that imposes the value of the parameter.
     *  <br>If the parameter is not locked, AdminLevel gives the administration
     *       level that will give the value of the parameter after a reset.
     * @param ioLocked
     *      Indicates if the parameter has been locked.
     * @return 
     *      Indicates if the parameter has been explicitly modified or remain
     *      to the administrated value.
     */
    HRESULT GetAccelColorInfo( inout             CATBSTR ioAdminLevel
                             , inout             CATBSTR ioLocked
                             , out /*IDLRETVAL*/ boolean oModified
                             );

    /**
     * Locks or unlocks the AccelColor parameter.
     * <br><b>Role</b>:Locks or unlocks the AccelColor parameter if it is possible
     * in the current administrative context. In user mode this method will always
     * return E_FAIL.
     * @param iLocked
     *  the locking operation to be performed
     *  <b>Legal values</b>:
     *  <br><tt>TRUE :</tt>   to lock the parameter.
     *  <br><tt>FALSE:</tt>   to unlock the parameter.
     */
    HRESULT SetAccelColorLock( in boolean iLocked );

#pragma PROPERTY CautionColor
    /**
     * Returns or sets the CautionColor parameter.
     * <p>
     * Ensure consistency with the C++ interface to which the work is delegated.
     */
    HRESULT get_CautionColor( out /*IDLRETVAL*/ CATSafeArrayVariant oColor );

    HRESULT put_CautionColor( in                CATSafeArrayVariant iColor );

    /** 
     * Retrieves environment informations for the CautionColor parameter.
     * <br><b>Role</b>:Retrieves the state of the CautionColor parameter 
     * in the current environment. 
     * @param ioAdminLevel
     *       <br>If the parameter is locked, AdminLevel gives the administration
     *       level that imposes the value of the parameter.
     *  <br>If the parameter is not locked, AdminLevel gives the administration
     *       level that will give the value of the parameter after a reset.
     * @param ioLocked
     *      Indicates if the parameter has been locked.
     * @return 
     *      Indicates if the parameter has been explicitly modified or remain
     *      to the administrated value.
     */
    HRESULT GetCautionColorInfo( inout             CATBSTR ioAdminLevel
                               , inout             CATBSTR ioLocked
                               , out /*IDLRETVAL*/ boolean oModified
                               );

    /**
     * Locks or unlocks the CautionColor parameter.
     * <br><b>Role</b>:Locks or unlocks the CautionColor parameter if it is possible
     * in the current administrative context. In user mode this method will always
     * return E_FAIL.
     * @param iLocked
     *  the locking operation to be performed
     *  <b>Legal values</b>:
     *  <br><tt>TRUE :</tt>   to lock the parameter.
     *  <br><tt>FALSE:</tt>   to unlock the parameter.
     */
    HRESULT SetCautionColorLock( in boolean iLocked );

#pragma PROPERTY ToolTipMode
    /**
     * Returns or sets the ToolTipMode parameter.
     * <p>
     * Ensure consistency with the C++ interface to which the work is delegated.
     */
     HRESULT get_ToolTipMode( out /*IDLRETVAL*/ boolean oToolTipMode );

     HRESULT put_ToolTipMode( in                boolean iToolTipMode );

     /** 
     * Retrieves environment informations for the ToolTipMode parameter.
     * <br><b>Role</b>:Retrieves the state of the ToolTipMode parameter 
     * in the current environment. 
     * @param ioAdminLevel
     *     <br>If the parameter is locked, AdminLevel gives the administration
     *     level that imposes the value of the parameter.
     *     <br>If the parameter is not locked, AdminLevel gives the administration
     *     level that will give the value of the parameter after a reset.
     * @param ioLocked
     *     Indicates if the parameter has been locked.
     * @return 
     *     Indicates if the parameter has been explicitly modified or remain
     *     to the administrated value.
     */
     HRESULT GetToolTipModeInfo( inout              CATBSTR ioAdminLevel
                            , inout              CATBSTR ioLocked
                            , out  /*IDLRETVAL*/ boolean oModified
                            );

    /**
     * Locks or unlocks the ToolTipMode parameter.
     * <br><b>Role</b>:Locks or unlocks the ToolTipMode parameter if it is possible
     * in the current administrative context. In user mode this method will always
     * return E_FAIL.
     * @param iLocked
     *     the locking operation to be performed
     *     <b>Legal values</b>:
     *     <br><tt>TRUE :</tt>   to lock the parameter.
     *     <br><tt>FALSE:</tt>   to unlock the parameter.
     */
     HRESULT SetToolTipModeLock( in boolean iLocked );

};

// Interface name : DNBIADevAnalysisSettingAtt
#pragma ID DNBIADevAnalysisSettingAtt "DCE:4c2e9c73-0dda-49d8-b51b594806742314"
#pragma DUAL DNBIADevAnalysisSettingAtt

// VB object name : DevAnalysisSettingAtt (Id used in Visual Basic)
#pragma ID DevAnalysisSettingAtt "DCE:7d9f6095-531c-40a1-901545bcaffdebbd"
#pragma ALIAS DNBIADevAnalysisSettingAtt DevAnalysisSettingAtt

#endif
