/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef DNBIADevAnalysisSettingAtt_h
#define DNBIADevAnalysisSettingAtt_h

#include "CATCORBABoolean.h"
#include "CAT_VARIANT_BOOL.h"

#ifndef ExportedByDNBDevicePubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __DNBDevicePubIDL
#define ExportedByDNBDevicePubIDL __declspec(dllexport)
#else
#define ExportedByDNBDevicePubIDL __declspec(dllimport)
#endif
#else
#define ExportedByDNBDevicePubIDL
#endif
#endif

#include "CATBSTR.h"
#include "CATIASettingController.h"
#include "CATSafeArray.h"
#include "DNBAnalysisLevel.h"

extern ExportedByDNBDevicePubIDL IID IID_DNBIADevAnalysisSettingAtt;

class ExportedByDNBDevicePubIDL DNBIADevAnalysisSettingAtt : public CATIASettingController
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_TravelLimit(DNBAnalysisLevel & oLevel)=0;

    virtual HRESULT __stdcall put_TravelLimit(DNBAnalysisLevel iLevel)=0;

    virtual HRESULT __stdcall GetTravelLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetTravelLimitLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_VelocityLimit(DNBAnalysisLevel & oLevel)=0;

    virtual HRESULT __stdcall put_VelocityLimit(DNBAnalysisLevel iLevel)=0;

    virtual HRESULT __stdcall GetVelocityLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetVelocityLimitLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_AccelLimit(DNBAnalysisLevel & oLevel)=0;

    virtual HRESULT __stdcall put_AccelLimit(DNBAnalysisLevel iLevel)=0;

    virtual HRESULT __stdcall GetAccelLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetAccelLimitLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_CautionZone(DNBAnalysisLevel & oLevel)=0;

    virtual HRESULT __stdcall put_CautionZone(DNBAnalysisLevel iLevel)=0;

    virtual HRESULT __stdcall GetCautionZoneInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetCautionZoneLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_TravelColor(CATSafeArrayVariant *& oColor)=0;

    virtual HRESULT __stdcall put_TravelColor(const CATSafeArrayVariant & iColor)=0;

    virtual HRESULT __stdcall GetTravelColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetTravelColorLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_VelocityColor(CATSafeArrayVariant *& oColor)=0;

    virtual HRESULT __stdcall put_VelocityColor(const CATSafeArrayVariant & iColor)=0;

    virtual HRESULT __stdcall GetVelocityColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetVelocityColorLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_AccelColor(CATSafeArrayVariant *& oColor)=0;

    virtual HRESULT __stdcall put_AccelColor(const CATSafeArrayVariant & iColor)=0;

    virtual HRESULT __stdcall GetAccelColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetAccelColorLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_CautionColor(CATSafeArrayVariant *& oColor)=0;

    virtual HRESULT __stdcall put_CautionColor(const CATSafeArrayVariant & iColor)=0;

    virtual HRESULT __stdcall GetCautionColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetCautionColorLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_ToolTipMode(CAT_VARIANT_BOOL & oToolTipMode)=0;

    virtual HRESULT __stdcall put_ToolTipMode(CAT_VARIANT_BOOL iToolTipMode)=0;

    virtual HRESULT __stdcall GetToolTipModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetToolTipModeLock(CAT_VARIANT_BOOL iLocked)=0;


};

CATDeclareHandler(DNBIADevAnalysisSettingAtt, CATIASettingController);

#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIABase.h"
#include "CATVariant.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
