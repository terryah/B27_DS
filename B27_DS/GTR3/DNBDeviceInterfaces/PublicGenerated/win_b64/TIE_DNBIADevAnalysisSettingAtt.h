#ifndef __TIE_DNBIADevAnalysisSettingAtt
#define __TIE_DNBIADevAnalysisSettingAtt

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIADevAnalysisSettingAtt.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIADevAnalysisSettingAtt */
#define declare_TIE_DNBIADevAnalysisSettingAtt(classe) \
 \
 \
class TIEDNBIADevAnalysisSettingAtt##classe : public DNBIADevAnalysisSettingAtt \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIADevAnalysisSettingAtt, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall get_TravelLimit(DNBAnalysisLevel & oLevel); \
      virtual HRESULT __stdcall put_TravelLimit(DNBAnalysisLevel iLevel); \
      virtual HRESULT __stdcall GetTravelLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetTravelLimitLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_VelocityLimit(DNBAnalysisLevel & oLevel); \
      virtual HRESULT __stdcall put_VelocityLimit(DNBAnalysisLevel iLevel); \
      virtual HRESULT __stdcall GetVelocityLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetVelocityLimitLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_AccelLimit(DNBAnalysisLevel & oLevel); \
      virtual HRESULT __stdcall put_AccelLimit(DNBAnalysisLevel iLevel); \
      virtual HRESULT __stdcall GetAccelLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetAccelLimitLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_CautionZone(DNBAnalysisLevel & oLevel); \
      virtual HRESULT __stdcall put_CautionZone(DNBAnalysisLevel iLevel); \
      virtual HRESULT __stdcall GetCautionZoneInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetCautionZoneLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_TravelColor(CATSafeArrayVariant *& oColor); \
      virtual HRESULT __stdcall put_TravelColor(const CATSafeArrayVariant & iColor); \
      virtual HRESULT __stdcall GetTravelColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetTravelColorLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_VelocityColor(CATSafeArrayVariant *& oColor); \
      virtual HRESULT __stdcall put_VelocityColor(const CATSafeArrayVariant & iColor); \
      virtual HRESULT __stdcall GetVelocityColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetVelocityColorLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_AccelColor(CATSafeArrayVariant *& oColor); \
      virtual HRESULT __stdcall put_AccelColor(const CATSafeArrayVariant & iColor); \
      virtual HRESULT __stdcall GetAccelColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetAccelColorLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_CautionColor(CATSafeArrayVariant *& oColor); \
      virtual HRESULT __stdcall put_CautionColor(const CATSafeArrayVariant & iColor); \
      virtual HRESULT __stdcall GetCautionColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetCautionColorLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_ToolTipMode(CAT_VARIANT_BOOL & oToolTipMode); \
      virtual HRESULT __stdcall put_ToolTipMode(CAT_VARIANT_BOOL iToolTipMode); \
      virtual HRESULT __stdcall GetToolTipModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetToolTipModeLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall Commit(); \
      virtual HRESULT __stdcall Rollback(); \
      virtual HRESULT __stdcall ResetToAdminValues(); \
      virtual HRESULT __stdcall ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList); \
      virtual HRESULT __stdcall SaveRepository(); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIADevAnalysisSettingAtt(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall get_TravelLimit(DNBAnalysisLevel & oLevel); \
virtual HRESULT __stdcall put_TravelLimit(DNBAnalysisLevel iLevel); \
virtual HRESULT __stdcall GetTravelLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetTravelLimitLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_VelocityLimit(DNBAnalysisLevel & oLevel); \
virtual HRESULT __stdcall put_VelocityLimit(DNBAnalysisLevel iLevel); \
virtual HRESULT __stdcall GetVelocityLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetVelocityLimitLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_AccelLimit(DNBAnalysisLevel & oLevel); \
virtual HRESULT __stdcall put_AccelLimit(DNBAnalysisLevel iLevel); \
virtual HRESULT __stdcall GetAccelLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetAccelLimitLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_CautionZone(DNBAnalysisLevel & oLevel); \
virtual HRESULT __stdcall put_CautionZone(DNBAnalysisLevel iLevel); \
virtual HRESULT __stdcall GetCautionZoneInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetCautionZoneLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_TravelColor(CATSafeArrayVariant *& oColor); \
virtual HRESULT __stdcall put_TravelColor(const CATSafeArrayVariant & iColor); \
virtual HRESULT __stdcall GetTravelColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetTravelColorLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_VelocityColor(CATSafeArrayVariant *& oColor); \
virtual HRESULT __stdcall put_VelocityColor(const CATSafeArrayVariant & iColor); \
virtual HRESULT __stdcall GetVelocityColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetVelocityColorLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_AccelColor(CATSafeArrayVariant *& oColor); \
virtual HRESULT __stdcall put_AccelColor(const CATSafeArrayVariant & iColor); \
virtual HRESULT __stdcall GetAccelColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetAccelColorLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_CautionColor(CATSafeArrayVariant *& oColor); \
virtual HRESULT __stdcall put_CautionColor(const CATSafeArrayVariant & iColor); \
virtual HRESULT __stdcall GetCautionColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetCautionColorLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_ToolTipMode(CAT_VARIANT_BOOL & oToolTipMode); \
virtual HRESULT __stdcall put_ToolTipMode(CAT_VARIANT_BOOL iToolTipMode); \
virtual HRESULT __stdcall GetToolTipModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetToolTipModeLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall Commit(); \
virtual HRESULT __stdcall Rollback(); \
virtual HRESULT __stdcall ResetToAdminValues(); \
virtual HRESULT __stdcall ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList); \
virtual HRESULT __stdcall SaveRepository(); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIADevAnalysisSettingAtt(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::get_TravelLimit(DNBAnalysisLevel & oLevel) \
{ \
return (ENVTIECALL(DNBIADevAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_TravelLimit(oLevel)); \
} \
HRESULT __stdcall  ENVTIEName::put_TravelLimit(DNBAnalysisLevel iLevel) \
{ \
return (ENVTIECALL(DNBIADevAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_TravelLimit(iLevel)); \
} \
HRESULT __stdcall  ENVTIEName::GetTravelLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIADevAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetTravelLimitInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetTravelLimitLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIADevAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetTravelLimitLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_VelocityLimit(DNBAnalysisLevel & oLevel) \
{ \
return (ENVTIECALL(DNBIADevAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_VelocityLimit(oLevel)); \
} \
HRESULT __stdcall  ENVTIEName::put_VelocityLimit(DNBAnalysisLevel iLevel) \
{ \
return (ENVTIECALL(DNBIADevAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_VelocityLimit(iLevel)); \
} \
HRESULT __stdcall  ENVTIEName::GetVelocityLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIADevAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetVelocityLimitInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetVelocityLimitLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIADevAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetVelocityLimitLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_AccelLimit(DNBAnalysisLevel & oLevel) \
{ \
return (ENVTIECALL(DNBIADevAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_AccelLimit(oLevel)); \
} \
HRESULT __stdcall  ENVTIEName::put_AccelLimit(DNBAnalysisLevel iLevel) \
{ \
return (ENVTIECALL(DNBIADevAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_AccelLimit(iLevel)); \
} \
HRESULT __stdcall  ENVTIEName::GetAccelLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIADevAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetAccelLimitInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetAccelLimitLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIADevAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetAccelLimitLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_CautionZone(DNBAnalysisLevel & oLevel) \
{ \
return (ENVTIECALL(DNBIADevAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_CautionZone(oLevel)); \
} \
HRESULT __stdcall  ENVTIEName::put_CautionZone(DNBAnalysisLevel iLevel) \
{ \
return (ENVTIECALL(DNBIADevAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_CautionZone(iLevel)); \
} \
HRESULT __stdcall  ENVTIEName::GetCautionZoneInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIADevAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetCautionZoneInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetCautionZoneLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIADevAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetCautionZoneLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_TravelColor(CATSafeArrayVariant *& oColor) \
{ \
return (ENVTIECALL(DNBIADevAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_TravelColor(oColor)); \
} \
HRESULT __stdcall  ENVTIEName::put_TravelColor(const CATSafeArrayVariant & iColor) \
{ \
return (ENVTIECALL(DNBIADevAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_TravelColor(iColor)); \
} \
HRESULT __stdcall  ENVTIEName::GetTravelColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIADevAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetTravelColorInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetTravelColorLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIADevAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetTravelColorLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_VelocityColor(CATSafeArrayVariant *& oColor) \
{ \
return (ENVTIECALL(DNBIADevAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_VelocityColor(oColor)); \
} \
HRESULT __stdcall  ENVTIEName::put_VelocityColor(const CATSafeArrayVariant & iColor) \
{ \
return (ENVTIECALL(DNBIADevAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_VelocityColor(iColor)); \
} \
HRESULT __stdcall  ENVTIEName::GetVelocityColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIADevAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetVelocityColorInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetVelocityColorLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIADevAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetVelocityColorLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_AccelColor(CATSafeArrayVariant *& oColor) \
{ \
return (ENVTIECALL(DNBIADevAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_AccelColor(oColor)); \
} \
HRESULT __stdcall  ENVTIEName::put_AccelColor(const CATSafeArrayVariant & iColor) \
{ \
return (ENVTIECALL(DNBIADevAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_AccelColor(iColor)); \
} \
HRESULT __stdcall  ENVTIEName::GetAccelColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIADevAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetAccelColorInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetAccelColorLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIADevAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetAccelColorLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_CautionColor(CATSafeArrayVariant *& oColor) \
{ \
return (ENVTIECALL(DNBIADevAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_CautionColor(oColor)); \
} \
HRESULT __stdcall  ENVTIEName::put_CautionColor(const CATSafeArrayVariant & iColor) \
{ \
return (ENVTIECALL(DNBIADevAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_CautionColor(iColor)); \
} \
HRESULT __stdcall  ENVTIEName::GetCautionColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIADevAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetCautionColorInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetCautionColorLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIADevAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetCautionColorLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_ToolTipMode(CAT_VARIANT_BOOL & oToolTipMode) \
{ \
return (ENVTIECALL(DNBIADevAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_ToolTipMode(oToolTipMode)); \
} \
HRESULT __stdcall  ENVTIEName::put_ToolTipMode(CAT_VARIANT_BOOL iToolTipMode) \
{ \
return (ENVTIECALL(DNBIADevAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_ToolTipMode(iToolTipMode)); \
} \
HRESULT __stdcall  ENVTIEName::GetToolTipModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIADevAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetToolTipModeInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetToolTipModeLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIADevAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetToolTipModeLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::Commit() \
{ \
return (ENVTIECALL(DNBIADevAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)Commit()); \
} \
HRESULT __stdcall  ENVTIEName::Rollback() \
{ \
return (ENVTIECALL(DNBIADevAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)Rollback()); \
} \
HRESULT __stdcall  ENVTIEName::ResetToAdminValues() \
{ \
return (ENVTIECALL(DNBIADevAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)ResetToAdminValues()); \
} \
HRESULT __stdcall  ENVTIEName::ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList) \
{ \
return (ENVTIECALL(DNBIADevAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)ResetToAdminValuesByName(iAttList)); \
} \
HRESULT __stdcall  ENVTIEName::SaveRepository() \
{ \
return (ENVTIECALL(DNBIADevAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)SaveRepository()); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIADevAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIADevAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIADevAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIADevAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIADevAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIADevAnalysisSettingAtt(classe)    TIEDNBIADevAnalysisSettingAtt##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIADevAnalysisSettingAtt(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIADevAnalysisSettingAtt, classe) \
 \
 \
CATImplementTIEMethods(DNBIADevAnalysisSettingAtt, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIADevAnalysisSettingAtt, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIADevAnalysisSettingAtt, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIADevAnalysisSettingAtt, classe) \
 \
HRESULT __stdcall  TIEDNBIADevAnalysisSettingAtt##classe::get_TravelLimit(DNBAnalysisLevel & oLevel) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&oLevel); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_TravelLimit(oLevel); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&oLevel); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIADevAnalysisSettingAtt##classe::put_TravelLimit(DNBAnalysisLevel iLevel) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&iLevel); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_TravelLimit(iLevel); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&iLevel); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIADevAnalysisSettingAtt##classe::GetTravelLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetTravelLimitInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIADevAnalysisSettingAtt##classe::SetTravelLimitLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetTravelLimitLock(iLocked); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIADevAnalysisSettingAtt##classe::get_VelocityLimit(DNBAnalysisLevel & oLevel) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&oLevel); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_VelocityLimit(oLevel); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&oLevel); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIADevAnalysisSettingAtt##classe::put_VelocityLimit(DNBAnalysisLevel iLevel) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&iLevel); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_VelocityLimit(iLevel); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&iLevel); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIADevAnalysisSettingAtt##classe::GetVelocityLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetVelocityLimitInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIADevAnalysisSettingAtt##classe::SetVelocityLimitLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetVelocityLimitLock(iLocked); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIADevAnalysisSettingAtt##classe::get_AccelLimit(DNBAnalysisLevel & oLevel) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&oLevel); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_AccelLimit(oLevel); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&oLevel); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIADevAnalysisSettingAtt##classe::put_AccelLimit(DNBAnalysisLevel iLevel) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&iLevel); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_AccelLimit(iLevel); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&iLevel); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIADevAnalysisSettingAtt##classe::GetAccelLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,11,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAccelLimitInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,11,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIADevAnalysisSettingAtt##classe::SetAccelLimitLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,12,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetAccelLimitLock(iLocked); \
   ExitAfterCall(this,12,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIADevAnalysisSettingAtt##classe::get_CautionZone(DNBAnalysisLevel & oLevel) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,13,&_Trac2,&oLevel); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_CautionZone(oLevel); \
   ExitAfterCall(this,13,_Trac2,&_ret_arg,&oLevel); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIADevAnalysisSettingAtt##classe::put_CautionZone(DNBAnalysisLevel iLevel) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,14,&_Trac2,&iLevel); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_CautionZone(iLevel); \
   ExitAfterCall(this,14,_Trac2,&_ret_arg,&iLevel); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIADevAnalysisSettingAtt##classe::GetCautionZoneInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,15,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetCautionZoneInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,15,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIADevAnalysisSettingAtt##classe::SetCautionZoneLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,16,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetCautionZoneLock(iLocked); \
   ExitAfterCall(this,16,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIADevAnalysisSettingAtt##classe::get_TravelColor(CATSafeArrayVariant *& oColor) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,17,&_Trac2,&oColor); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_TravelColor(oColor); \
   ExitAfterCall(this,17,_Trac2,&_ret_arg,&oColor); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIADevAnalysisSettingAtt##classe::put_TravelColor(const CATSafeArrayVariant & iColor) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,18,&_Trac2,&iColor); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_TravelColor(iColor); \
   ExitAfterCall(this,18,_Trac2,&_ret_arg,&iColor); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIADevAnalysisSettingAtt##classe::GetTravelColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,19,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetTravelColorInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,19,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIADevAnalysisSettingAtt##classe::SetTravelColorLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,20,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetTravelColorLock(iLocked); \
   ExitAfterCall(this,20,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIADevAnalysisSettingAtt##classe::get_VelocityColor(CATSafeArrayVariant *& oColor) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,21,&_Trac2,&oColor); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_VelocityColor(oColor); \
   ExitAfterCall(this,21,_Trac2,&_ret_arg,&oColor); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIADevAnalysisSettingAtt##classe::put_VelocityColor(const CATSafeArrayVariant & iColor) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,22,&_Trac2,&iColor); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_VelocityColor(iColor); \
   ExitAfterCall(this,22,_Trac2,&_ret_arg,&iColor); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIADevAnalysisSettingAtt##classe::GetVelocityColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,23,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetVelocityColorInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,23,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIADevAnalysisSettingAtt##classe::SetVelocityColorLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,24,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetVelocityColorLock(iLocked); \
   ExitAfterCall(this,24,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIADevAnalysisSettingAtt##classe::get_AccelColor(CATSafeArrayVariant *& oColor) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,25,&_Trac2,&oColor); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_AccelColor(oColor); \
   ExitAfterCall(this,25,_Trac2,&_ret_arg,&oColor); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIADevAnalysisSettingAtt##classe::put_AccelColor(const CATSafeArrayVariant & iColor) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,26,&_Trac2,&iColor); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_AccelColor(iColor); \
   ExitAfterCall(this,26,_Trac2,&_ret_arg,&iColor); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIADevAnalysisSettingAtt##classe::GetAccelColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,27,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAccelColorInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,27,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIADevAnalysisSettingAtt##classe::SetAccelColorLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,28,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetAccelColorLock(iLocked); \
   ExitAfterCall(this,28,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIADevAnalysisSettingAtt##classe::get_CautionColor(CATSafeArrayVariant *& oColor) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,29,&_Trac2,&oColor); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_CautionColor(oColor); \
   ExitAfterCall(this,29,_Trac2,&_ret_arg,&oColor); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIADevAnalysisSettingAtt##classe::put_CautionColor(const CATSafeArrayVariant & iColor) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,30,&_Trac2,&iColor); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_CautionColor(iColor); \
   ExitAfterCall(this,30,_Trac2,&_ret_arg,&iColor); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIADevAnalysisSettingAtt##classe::GetCautionColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,31,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetCautionColorInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,31,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIADevAnalysisSettingAtt##classe::SetCautionColorLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,32,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetCautionColorLock(iLocked); \
   ExitAfterCall(this,32,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIADevAnalysisSettingAtt##classe::get_ToolTipMode(CAT_VARIANT_BOOL & oToolTipMode) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,33,&_Trac2,&oToolTipMode); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_ToolTipMode(oToolTipMode); \
   ExitAfterCall(this,33,_Trac2,&_ret_arg,&oToolTipMode); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIADevAnalysisSettingAtt##classe::put_ToolTipMode(CAT_VARIANT_BOOL iToolTipMode) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,34,&_Trac2,&iToolTipMode); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_ToolTipMode(iToolTipMode); \
   ExitAfterCall(this,34,_Trac2,&_ret_arg,&iToolTipMode); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIADevAnalysisSettingAtt##classe::GetToolTipModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,35,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetToolTipModeInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,35,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIADevAnalysisSettingAtt##classe::SetToolTipModeLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,36,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetToolTipModeLock(iLocked); \
   ExitAfterCall(this,36,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIADevAnalysisSettingAtt##classe::Commit() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,37,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Commit(); \
   ExitAfterCall(this,37,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIADevAnalysisSettingAtt##classe::Rollback() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,38,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Rollback(); \
   ExitAfterCall(this,38,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIADevAnalysisSettingAtt##classe::ResetToAdminValues() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,39,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ResetToAdminValues(); \
   ExitAfterCall(this,39,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIADevAnalysisSettingAtt##classe::ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,40,&_Trac2,&iAttList); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ResetToAdminValuesByName(iAttList); \
   ExitAfterCall(this,40,_Trac2,&_ret_arg,&iAttList); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIADevAnalysisSettingAtt##classe::SaveRepository() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,41,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SaveRepository(); \
   ExitAfterCall(this,41,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIADevAnalysisSettingAtt##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,42,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,42,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIADevAnalysisSettingAtt##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,43,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,43,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIADevAnalysisSettingAtt##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,44,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,44,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIADevAnalysisSettingAtt##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,45,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,45,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIADevAnalysisSettingAtt##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,46,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,46,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIADevAnalysisSettingAtt(classe) \
 \
 \
declare_TIE_DNBIADevAnalysisSettingAtt(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIADevAnalysisSettingAtt##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIADevAnalysisSettingAtt,"DNBIADevAnalysisSettingAtt",DNBIADevAnalysisSettingAtt::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIADevAnalysisSettingAtt(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIADevAnalysisSettingAtt, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIADevAnalysisSettingAtt##classe(classe::MetaObject(),DNBIADevAnalysisSettingAtt::MetaObject(),(void *)CreateTIEDNBIADevAnalysisSettingAtt##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIADevAnalysisSettingAtt(classe) \
 \
 \
declare_TIE_DNBIADevAnalysisSettingAtt(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIADevAnalysisSettingAtt##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIADevAnalysisSettingAtt,"DNBIADevAnalysisSettingAtt",DNBIADevAnalysisSettingAtt::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIADevAnalysisSettingAtt(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIADevAnalysisSettingAtt, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIADevAnalysisSettingAtt##classe(classe::MetaObject(),DNBIADevAnalysisSettingAtt::MetaObject(),(void *)CreateTIEDNBIADevAnalysisSettingAtt##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIADevAnalysisSettingAtt(classe) TIE_DNBIADevAnalysisSettingAtt(classe)
#else
#define BOA_DNBIADevAnalysisSettingAtt(classe) CATImplementBOA(DNBIADevAnalysisSettingAtt, classe)
#endif

#endif
