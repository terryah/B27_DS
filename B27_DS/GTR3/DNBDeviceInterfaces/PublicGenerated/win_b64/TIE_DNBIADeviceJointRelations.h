#ifndef __TIE_DNBIADeviceJointRelations
#define __TIE_DNBIADeviceJointRelations

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "DNBIADeviceJointRelations.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIADeviceJointRelations */
#define declare_TIE_DNBIADeviceJointRelations(classe) \
 \
 \
class TIEDNBIADeviceJointRelations##classe : public DNBIADeviceJointRelations \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIADeviceJointRelations, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall GetJointRelationExpression(CATIAJoint * joint, CATBSTR & expr); \
      virtual HRESULT __stdcall SetJointRelationExpression(CATIAJoint * joint, const CATBSTR & expr); \
      virtual HRESULT __stdcall SetUserVariableExpr(const CATBSTR & user_expr); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIADeviceJointRelations(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall GetJointRelationExpression(CATIAJoint * joint, CATBSTR & expr); \
virtual HRESULT __stdcall SetJointRelationExpression(CATIAJoint * joint, const CATBSTR & expr); \
virtual HRESULT __stdcall SetUserVariableExpr(const CATBSTR & user_expr); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIADeviceJointRelations(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::GetJointRelationExpression(CATIAJoint * joint, CATBSTR & expr) \
{ \
return (ENVTIECALL(DNBIADeviceJointRelations,ENVTIETypeLetter,ENVTIELetter)GetJointRelationExpression(joint,expr)); \
} \
HRESULT __stdcall  ENVTIEName::SetJointRelationExpression(CATIAJoint * joint, const CATBSTR & expr) \
{ \
return (ENVTIECALL(DNBIADeviceJointRelations,ENVTIETypeLetter,ENVTIELetter)SetJointRelationExpression(joint,expr)); \
} \
HRESULT __stdcall  ENVTIEName::SetUserVariableExpr(const CATBSTR & user_expr) \
{ \
return (ENVTIECALL(DNBIADeviceJointRelations,ENVTIETypeLetter,ENVTIELetter)SetUserVariableExpr(user_expr)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIADeviceJointRelations,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIADeviceJointRelations,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIADeviceJointRelations,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIADeviceJointRelations,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIADeviceJointRelations,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIADeviceJointRelations(classe)    TIEDNBIADeviceJointRelations##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIADeviceJointRelations(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIADeviceJointRelations, classe) \
 \
 \
CATImplementTIEMethods(DNBIADeviceJointRelations, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIADeviceJointRelations, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIADeviceJointRelations, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIADeviceJointRelations, classe) \
 \
HRESULT __stdcall  TIEDNBIADeviceJointRelations##classe::GetJointRelationExpression(CATIAJoint * joint, CATBSTR & expr) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetJointRelationExpression(joint,expr)); \
} \
HRESULT __stdcall  TIEDNBIADeviceJointRelations##classe::SetJointRelationExpression(CATIAJoint * joint, const CATBSTR & expr) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetJointRelationExpression(joint,expr)); \
} \
HRESULT __stdcall  TIEDNBIADeviceJointRelations##classe::SetUserVariableExpr(const CATBSTR & user_expr) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetUserVariableExpr(user_expr)); \
} \
HRESULT  __stdcall  TIEDNBIADeviceJointRelations##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication)); \
} \
HRESULT  __stdcall  TIEDNBIADeviceJointRelations##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent)); \
} \
HRESULT  __stdcall  TIEDNBIADeviceJointRelations##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  TIEDNBIADeviceJointRelations##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  TIEDNBIADeviceJointRelations##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj)); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIADeviceJointRelations(classe) \
 \
 \
declare_TIE_DNBIADeviceJointRelations(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIADeviceJointRelations##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIADeviceJointRelations,"DNBIADeviceJointRelations",DNBIADeviceJointRelations::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIADeviceJointRelations(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIADeviceJointRelations, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIADeviceJointRelations##classe(classe::MetaObject(),DNBIADeviceJointRelations::MetaObject(),(void *)CreateTIEDNBIADeviceJointRelations##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIADeviceJointRelations(classe) \
 \
 \
declare_TIE_DNBIADeviceJointRelations(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIADeviceJointRelations##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIADeviceJointRelations,"DNBIADeviceJointRelations",DNBIADeviceJointRelations::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIADeviceJointRelations(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIADeviceJointRelations, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIADeviceJointRelations##classe(classe::MetaObject(),DNBIADeviceJointRelations::MetaObject(),(void *)CreateTIEDNBIADeviceJointRelations##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIADeviceJointRelations(classe) TIE_DNBIADeviceJointRelations(classe)
#else
#define BOA_DNBIADeviceJointRelations(classe) CATImplementBOA(DNBIADeviceJointRelations, classe)
#endif

#endif
