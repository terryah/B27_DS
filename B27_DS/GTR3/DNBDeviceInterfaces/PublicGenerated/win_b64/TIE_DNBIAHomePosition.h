#ifndef __TIE_DNBIAHomePosition
#define __TIE_DNBIAHomePosition

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIAHomePosition.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIAHomePosition */
#define declare_TIE_DNBIAHomePosition(classe) \
 \
 \
class TIEDNBIAHomePosition##classe : public DNBIAHomePosition \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIAHomePosition, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall GetAssociatedToolTip(CATSafeArrayVariant *& oTipList); \
      virtual HRESULT __stdcall SetAssociatedToolTip(const CATSafeArrayVariant & iTipList); \
      virtual HRESULT __stdcall GetDOFValues(CATSafeArrayVariant *& oValues); \
      virtual HRESULT __stdcall SetDOFValues(const CATSafeArrayVariant & iValues); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIAHomePosition(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall GetAssociatedToolTip(CATSafeArrayVariant *& oTipList); \
virtual HRESULT __stdcall SetAssociatedToolTip(const CATSafeArrayVariant & iTipList); \
virtual HRESULT __stdcall GetDOFValues(CATSafeArrayVariant *& oValues); \
virtual HRESULT __stdcall SetDOFValues(const CATSafeArrayVariant & iValues); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIAHomePosition(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::GetAssociatedToolTip(CATSafeArrayVariant *& oTipList) \
{ \
return (ENVTIECALL(DNBIAHomePosition,ENVTIETypeLetter,ENVTIELetter)GetAssociatedToolTip(oTipList)); \
} \
HRESULT __stdcall  ENVTIEName::SetAssociatedToolTip(const CATSafeArrayVariant & iTipList) \
{ \
return (ENVTIECALL(DNBIAHomePosition,ENVTIETypeLetter,ENVTIELetter)SetAssociatedToolTip(iTipList)); \
} \
HRESULT __stdcall  ENVTIEName::GetDOFValues(CATSafeArrayVariant *& oValues) \
{ \
return (ENVTIECALL(DNBIAHomePosition,ENVTIETypeLetter,ENVTIELetter)GetDOFValues(oValues)); \
} \
HRESULT __stdcall  ENVTIEName::SetDOFValues(const CATSafeArrayVariant & iValues) \
{ \
return (ENVTIECALL(DNBIAHomePosition,ENVTIETypeLetter,ENVTIELetter)SetDOFValues(iValues)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIAHomePosition,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIAHomePosition,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIAHomePosition,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIAHomePosition,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIAHomePosition,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIAHomePosition(classe)    TIEDNBIAHomePosition##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIAHomePosition(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIAHomePosition, classe) \
 \
 \
CATImplementTIEMethods(DNBIAHomePosition, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIAHomePosition, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIAHomePosition, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIAHomePosition, classe) \
 \
HRESULT __stdcall  TIEDNBIAHomePosition##classe::GetAssociatedToolTip(CATSafeArrayVariant *& oTipList) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&oTipList); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAssociatedToolTip(oTipList); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&oTipList); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHomePosition##classe::SetAssociatedToolTip(const CATSafeArrayVariant & iTipList) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&iTipList); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetAssociatedToolTip(iTipList); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&iTipList); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHomePosition##classe::GetDOFValues(CATSafeArrayVariant *& oValues) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&oValues); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetDOFValues(oValues); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&oValues); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAHomePosition##classe::SetDOFValues(const CATSafeArrayVariant & iValues) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&iValues); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetDOFValues(iValues); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&iValues); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAHomePosition##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAHomePosition##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAHomePosition##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAHomePosition##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAHomePosition##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIAHomePosition(classe) \
 \
 \
declare_TIE_DNBIAHomePosition(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAHomePosition##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAHomePosition,"DNBIAHomePosition",DNBIAHomePosition::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAHomePosition(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIAHomePosition, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAHomePosition##classe(classe::MetaObject(),DNBIAHomePosition::MetaObject(),(void *)CreateTIEDNBIAHomePosition##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIAHomePosition(classe) \
 \
 \
declare_TIE_DNBIAHomePosition(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAHomePosition##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAHomePosition,"DNBIAHomePosition",DNBIAHomePosition::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAHomePosition(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIAHomePosition, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAHomePosition##classe(classe::MetaObject(),DNBIAHomePosition::MetaObject(),(void *)CreateTIEDNBIAHomePosition##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIAHomePosition(classe) TIE_DNBIAHomePosition(classe)
#else
#define BOA_DNBIAHomePosition(classe) CATImplementBOA(DNBIAHomePosition, classe)
#endif

#endif
