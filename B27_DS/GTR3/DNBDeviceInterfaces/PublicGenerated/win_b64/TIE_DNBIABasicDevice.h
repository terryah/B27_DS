#ifndef __TIE_DNBIABasicDevice
#define __TIE_DNBIABasicDevice

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIABasicDevice.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIABasicDevice */
#define declare_TIE_DNBIABasicDevice(classe) \
 \
 \
class TIEDNBIABasicDevice##classe : public DNBIABasicDevice \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIABasicDevice, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall GetHomePositions(CATSafeArrayVariant *& oHomePosList); \
      virtual HRESULT __stdcall SetHomePosition(const CATBSTR & iName, const CATSafeArrayVariant & idbTrans); \
      virtual HRESULT __stdcall GetDOFValues(CATSafeArrayVariant *& oValues); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIABasicDevice(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall GetHomePositions(CATSafeArrayVariant *& oHomePosList); \
virtual HRESULT __stdcall SetHomePosition(const CATBSTR & iName, const CATSafeArrayVariant & idbTrans); \
virtual HRESULT __stdcall GetDOFValues(CATSafeArrayVariant *& oValues); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIABasicDevice(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::GetHomePositions(CATSafeArrayVariant *& oHomePosList) \
{ \
return (ENVTIECALL(DNBIABasicDevice,ENVTIETypeLetter,ENVTIELetter)GetHomePositions(oHomePosList)); \
} \
HRESULT __stdcall  ENVTIEName::SetHomePosition(const CATBSTR & iName, const CATSafeArrayVariant & idbTrans) \
{ \
return (ENVTIECALL(DNBIABasicDevice,ENVTIETypeLetter,ENVTIELetter)SetHomePosition(iName,idbTrans)); \
} \
HRESULT __stdcall  ENVTIEName::GetDOFValues(CATSafeArrayVariant *& oValues) \
{ \
return (ENVTIECALL(DNBIABasicDevice,ENVTIETypeLetter,ENVTIELetter)GetDOFValues(oValues)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIABasicDevice,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIABasicDevice,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIABasicDevice,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIABasicDevice,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIABasicDevice,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIABasicDevice(classe)    TIEDNBIABasicDevice##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIABasicDevice(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIABasicDevice, classe) \
 \
 \
CATImplementTIEMethods(DNBIABasicDevice, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIABasicDevice, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIABasicDevice, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIABasicDevice, classe) \
 \
HRESULT __stdcall  TIEDNBIABasicDevice##classe::GetHomePositions(CATSafeArrayVariant *& oHomePosList) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&oHomePosList); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetHomePositions(oHomePosList); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&oHomePosList); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIABasicDevice##classe::SetHomePosition(const CATBSTR & iName, const CATSafeArrayVariant & idbTrans) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&iName,&idbTrans); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetHomePosition(iName,idbTrans); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&iName,&idbTrans); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIABasicDevice##classe::GetDOFValues(CATSafeArrayVariant *& oValues) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&oValues); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetDOFValues(oValues); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&oValues); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIABasicDevice##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIABasicDevice##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIABasicDevice##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIABasicDevice##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIABasicDevice##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIABasicDevice(classe) \
 \
 \
declare_TIE_DNBIABasicDevice(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIABasicDevice##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIABasicDevice,"DNBIABasicDevice",DNBIABasicDevice::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIABasicDevice(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIABasicDevice, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIABasicDevice##classe(classe::MetaObject(),DNBIABasicDevice::MetaObject(),(void *)CreateTIEDNBIABasicDevice##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIABasicDevice(classe) \
 \
 \
declare_TIE_DNBIABasicDevice(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIABasicDevice##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIABasicDevice,"DNBIABasicDevice",DNBIABasicDevice::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIABasicDevice(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIABasicDevice, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIABasicDevice##classe(classe::MetaObject(),DNBIABasicDevice::MetaObject(),(void *)CreateTIEDNBIABasicDevice##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIABasicDevice(classe) TIE_DNBIABasicDevice(classe)
#else
#define BOA_DNBIABasicDevice(classe) CATImplementBOA(DNBIABasicDevice, classe)
#endif

#endif
