#ifndef __TIE_DNBIAUserVariableExpr
#define __TIE_DNBIAUserVariableExpr

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIAUserVariableExpr.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIAUserVariableExpr */
#define declare_TIE_DNBIAUserVariableExpr(classe) \
 \
 \
class TIEDNBIAUserVariableExpr##classe : public DNBIAUserVariableExpr \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIAUserVariableExpr, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall GetUserVariableExpr(CATSafeArrayVariant *& user_expr_list); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIAUserVariableExpr(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall GetUserVariableExpr(CATSafeArrayVariant *& user_expr_list); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIAUserVariableExpr(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::GetUserVariableExpr(CATSafeArrayVariant *& user_expr_list) \
{ \
return (ENVTIECALL(DNBIAUserVariableExpr,ENVTIETypeLetter,ENVTIELetter)GetUserVariableExpr(user_expr_list)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIAUserVariableExpr,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIAUserVariableExpr,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIAUserVariableExpr,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIAUserVariableExpr,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIAUserVariableExpr,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIAUserVariableExpr(classe)    TIEDNBIAUserVariableExpr##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIAUserVariableExpr(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIAUserVariableExpr, classe) \
 \
 \
CATImplementTIEMethods(DNBIAUserVariableExpr, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIAUserVariableExpr, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIAUserVariableExpr, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIAUserVariableExpr, classe) \
 \
HRESULT __stdcall  TIEDNBIAUserVariableExpr##classe::GetUserVariableExpr(CATSafeArrayVariant *& user_expr_list) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&user_expr_list); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetUserVariableExpr(user_expr_list); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&user_expr_list); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAUserVariableExpr##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAUserVariableExpr##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAUserVariableExpr##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAUserVariableExpr##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAUserVariableExpr##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIAUserVariableExpr(classe) \
 \
 \
declare_TIE_DNBIAUserVariableExpr(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAUserVariableExpr##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAUserVariableExpr,"DNBIAUserVariableExpr",DNBIAUserVariableExpr::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAUserVariableExpr(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIAUserVariableExpr, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAUserVariableExpr##classe(classe::MetaObject(),DNBIAUserVariableExpr::MetaObject(),(void *)CreateTIEDNBIAUserVariableExpr##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIAUserVariableExpr(classe) \
 \
 \
declare_TIE_DNBIAUserVariableExpr(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAUserVariableExpr##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAUserVariableExpr,"DNBIAUserVariableExpr",DNBIAUserVariableExpr::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAUserVariableExpr(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIAUserVariableExpr, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAUserVariableExpr##classe(classe::MetaObject(),DNBIAUserVariableExpr::MetaObject(),(void *)CreateTIEDNBIAUserVariableExpr##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIAUserVariableExpr(classe) TIE_DNBIAUserVariableExpr(classe)
#else
#define BOA_DNBIAUserVariableExpr(classe) CATImplementBOA(DNBIAUserVariableExpr, classe)
#endif

#endif
