#ifndef __TIE_DNBIAD5Device
#define __TIE_DNBIAD5Device

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIAD5Device.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIAD5Device */
#define declare_TIE_DNBIAD5Device(classe) \
 \
 \
class TIEDNBIAD5Device##classe : public DNBIAD5Device \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIAD5Device, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall GetLinkedDeviceFile(CATBSTR & oFileName); \
      virtual HRESULT __stdcall GetHomePositions(CATSafeArrayVariant *& oHomePosList); \
      virtual HRESULT __stdcall SetHomePosition(const CATBSTR & iName, const CATSafeArrayVariant & idbTrans); \
      virtual HRESULT __stdcall GetDOFValues(CATSafeArrayVariant *& oValues); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIAD5Device(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall GetLinkedDeviceFile(CATBSTR & oFileName); \
virtual HRESULT __stdcall GetHomePositions(CATSafeArrayVariant *& oHomePosList); \
virtual HRESULT __stdcall SetHomePosition(const CATBSTR & iName, const CATSafeArrayVariant & idbTrans); \
virtual HRESULT __stdcall GetDOFValues(CATSafeArrayVariant *& oValues); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIAD5Device(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::GetLinkedDeviceFile(CATBSTR & oFileName) \
{ \
return (ENVTIECALL(DNBIAD5Device,ENVTIETypeLetter,ENVTIELetter)GetLinkedDeviceFile(oFileName)); \
} \
HRESULT __stdcall  ENVTIEName::GetHomePositions(CATSafeArrayVariant *& oHomePosList) \
{ \
return (ENVTIECALL(DNBIAD5Device,ENVTIETypeLetter,ENVTIELetter)GetHomePositions(oHomePosList)); \
} \
HRESULT __stdcall  ENVTIEName::SetHomePosition(const CATBSTR & iName, const CATSafeArrayVariant & idbTrans) \
{ \
return (ENVTIECALL(DNBIAD5Device,ENVTIETypeLetter,ENVTIELetter)SetHomePosition(iName,idbTrans)); \
} \
HRESULT __stdcall  ENVTIEName::GetDOFValues(CATSafeArrayVariant *& oValues) \
{ \
return (ENVTIECALL(DNBIAD5Device,ENVTIETypeLetter,ENVTIELetter)GetDOFValues(oValues)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIAD5Device,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIAD5Device,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIAD5Device,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIAD5Device,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIAD5Device,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIAD5Device(classe)    TIEDNBIAD5Device##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIAD5Device(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIAD5Device, classe) \
 \
 \
CATImplementTIEMethods(DNBIAD5Device, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIAD5Device, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIAD5Device, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIAD5Device, classe) \
 \
HRESULT __stdcall  TIEDNBIAD5Device##classe::GetLinkedDeviceFile(CATBSTR & oFileName) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&oFileName); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetLinkedDeviceFile(oFileName); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&oFileName); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAD5Device##classe::GetHomePositions(CATSafeArrayVariant *& oHomePosList) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&oHomePosList); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetHomePositions(oHomePosList); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&oHomePosList); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAD5Device##classe::SetHomePosition(const CATBSTR & iName, const CATSafeArrayVariant & idbTrans) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&iName,&idbTrans); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetHomePosition(iName,idbTrans); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&iName,&idbTrans); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAD5Device##classe::GetDOFValues(CATSafeArrayVariant *& oValues) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&oValues); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetDOFValues(oValues); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&oValues); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAD5Device##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAD5Device##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAD5Device##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAD5Device##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAD5Device##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIAD5Device(classe) \
 \
 \
declare_TIE_DNBIAD5Device(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAD5Device##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAD5Device,"DNBIAD5Device",DNBIAD5Device::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAD5Device(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIAD5Device, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAD5Device##classe(classe::MetaObject(),DNBIAD5Device::MetaObject(),(void *)CreateTIEDNBIAD5Device##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIAD5Device(classe) \
 \
 \
declare_TIE_DNBIAD5Device(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAD5Device##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAD5Device,"DNBIAD5Device",DNBIAD5Device::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAD5Device(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIAD5Device, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAD5Device##classe(classe::MetaObject(),DNBIAD5Device::MetaObject(),(void *)CreateTIEDNBIAD5Device##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIAD5Device(classe) TIE_DNBIAD5Device(classe)
#else
#define BOA_DNBIAD5Device(classe) CATImplementBOA(DNBIAD5Device, classe)
#endif

#endif
