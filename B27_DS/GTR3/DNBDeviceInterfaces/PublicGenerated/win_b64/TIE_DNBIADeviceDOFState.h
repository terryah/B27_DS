#ifndef __TIE_DNBIADeviceDOFState
#define __TIE_DNBIADeviceDOFState

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIADeviceDOFState.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIADeviceDOFState */
#define declare_TIE_DNBIADeviceDOFState(classe) \
 \
 \
class TIEDNBIADeviceDOFState##classe : public DNBIADeviceDOFState \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIADeviceDOFState, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall GetDeviceDOFValues(CATSafeArrayVariant *& oValues); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIADeviceDOFState(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall GetDeviceDOFValues(CATSafeArrayVariant *& oValues); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIADeviceDOFState(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::GetDeviceDOFValues(CATSafeArrayVariant *& oValues) \
{ \
return (ENVTIECALL(DNBIADeviceDOFState,ENVTIETypeLetter,ENVTIELetter)GetDeviceDOFValues(oValues)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIADeviceDOFState,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIADeviceDOFState,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIADeviceDOFState,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIADeviceDOFState,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIADeviceDOFState,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIADeviceDOFState(classe)    TIEDNBIADeviceDOFState##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIADeviceDOFState(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIADeviceDOFState, classe) \
 \
 \
CATImplementTIEMethods(DNBIADeviceDOFState, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIADeviceDOFState, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIADeviceDOFState, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIADeviceDOFState, classe) \
 \
HRESULT __stdcall  TIEDNBIADeviceDOFState##classe::GetDeviceDOFValues(CATSafeArrayVariant *& oValues) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&oValues); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetDeviceDOFValues(oValues); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&oValues); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIADeviceDOFState##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIADeviceDOFState##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIADeviceDOFState##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIADeviceDOFState##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIADeviceDOFState##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIADeviceDOFState(classe) \
 \
 \
declare_TIE_DNBIADeviceDOFState(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIADeviceDOFState##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIADeviceDOFState,"DNBIADeviceDOFState",DNBIADeviceDOFState::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIADeviceDOFState(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIADeviceDOFState, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIADeviceDOFState##classe(classe::MetaObject(),DNBIADeviceDOFState::MetaObject(),(void *)CreateTIEDNBIADeviceDOFState##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIADeviceDOFState(classe) \
 \
 \
declare_TIE_DNBIADeviceDOFState(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIADeviceDOFState##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIADeviceDOFState,"DNBIADeviceDOFState",DNBIADeviceDOFState::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIADeviceDOFState(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIADeviceDOFState, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIADeviceDOFState##classe(classe::MetaObject(),DNBIADeviceDOFState::MetaObject(),(void *)CreateTIEDNBIADeviceDOFState##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIADeviceDOFState(classe) TIE_DNBIADeviceDOFState(classe)
#else
#define BOA_DNBIADeviceDOFState(classe) CATImplementBOA(DNBIADeviceDOFState, classe)
#endif

#endif
