#ifndef __TIE_DNBIADeviceSim
#define __TIE_DNBIADeviceSim

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIADeviceSim.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIADeviceSim */
#define declare_TIE_DNBIADeviceSim(classe) \
 \
 \
class TIEDNBIADeviceSim##classe : public DNBIADeviceSim \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIADeviceSim, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall Initialize(); \
      virtual HRESULT __stdcall SetDOFValues(CATBaseDispatch * iMechanism, const CATSafeArrayVariant & iValues, CAT_VARIANT_BOOL iIsRelative); \
      virtual HRESULT __stdcall GetDOFValues(CATBaseDispatch * iMechanism, CATSafeArrayVariant *& oValues); \
      virtual HRESULT __stdcall Finalize(); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIADeviceSim(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall Initialize(); \
virtual HRESULT __stdcall SetDOFValues(CATBaseDispatch * iMechanism, const CATSafeArrayVariant & iValues, CAT_VARIANT_BOOL iIsRelative); \
virtual HRESULT __stdcall GetDOFValues(CATBaseDispatch * iMechanism, CATSafeArrayVariant *& oValues); \
virtual HRESULT __stdcall Finalize(); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIADeviceSim(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::Initialize() \
{ \
return (ENVTIECALL(DNBIADeviceSim,ENVTIETypeLetter,ENVTIELetter)Initialize()); \
} \
HRESULT __stdcall  ENVTIEName::SetDOFValues(CATBaseDispatch * iMechanism, const CATSafeArrayVariant & iValues, CAT_VARIANT_BOOL iIsRelative) \
{ \
return (ENVTIECALL(DNBIADeviceSim,ENVTIETypeLetter,ENVTIELetter)SetDOFValues(iMechanism,iValues,iIsRelative)); \
} \
HRESULT __stdcall  ENVTIEName::GetDOFValues(CATBaseDispatch * iMechanism, CATSafeArrayVariant *& oValues) \
{ \
return (ENVTIECALL(DNBIADeviceSim,ENVTIETypeLetter,ENVTIELetter)GetDOFValues(iMechanism,oValues)); \
} \
HRESULT __stdcall  ENVTIEName::Finalize() \
{ \
return (ENVTIECALL(DNBIADeviceSim,ENVTIETypeLetter,ENVTIELetter)Finalize()); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIADeviceSim,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIADeviceSim,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIADeviceSim,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIADeviceSim,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIADeviceSim,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIADeviceSim(classe)    TIEDNBIADeviceSim##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIADeviceSim(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIADeviceSim, classe) \
 \
 \
CATImplementTIEMethods(DNBIADeviceSim, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIADeviceSim, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIADeviceSim, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIADeviceSim, classe) \
 \
HRESULT __stdcall  TIEDNBIADeviceSim##classe::Initialize() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Initialize(); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIADeviceSim##classe::SetDOFValues(CATBaseDispatch * iMechanism, const CATSafeArrayVariant & iValues, CAT_VARIANT_BOOL iIsRelative) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&iMechanism,&iValues,&iIsRelative); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetDOFValues(iMechanism,iValues,iIsRelative); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&iMechanism,&iValues,&iIsRelative); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIADeviceSim##classe::GetDOFValues(CATBaseDispatch * iMechanism, CATSafeArrayVariant *& oValues) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&iMechanism,&oValues); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetDOFValues(iMechanism,oValues); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&iMechanism,&oValues); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIADeviceSim##classe::Finalize() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Finalize(); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIADeviceSim##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIADeviceSim##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIADeviceSim##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIADeviceSim##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIADeviceSim##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIADeviceSim(classe) \
 \
 \
declare_TIE_DNBIADeviceSim(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIADeviceSim##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIADeviceSim,"DNBIADeviceSim",DNBIADeviceSim::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIADeviceSim(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIADeviceSim, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIADeviceSim##classe(classe::MetaObject(),DNBIADeviceSim::MetaObject(),(void *)CreateTIEDNBIADeviceSim##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIADeviceSim(classe) \
 \
 \
declare_TIE_DNBIADeviceSim(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIADeviceSim##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIADeviceSim,"DNBIADeviceSim",DNBIADeviceSim::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIADeviceSim(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIADeviceSim, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIADeviceSim##classe(classe::MetaObject(),DNBIADeviceSim::MetaObject(),(void *)CreateTIEDNBIADeviceSim##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIADeviceSim(classe) TIE_DNBIADeviceSim(classe)
#else
#define BOA_DNBIADeviceSim(classe) CATImplementBOA(DNBIADeviceSim, classe)
#endif

#endif
