# COPYRIGHT DASSAULT SYSTEMES 2004
#======================================================================
# Imakefile for module DNBDevicePubIdl.m
# Module for compilation of the public IDL interfaces
#======================================================================
#
#  Mar 2004  Creation: Code generated by the CAA wizard  mmh
#======================================================================
#
# NO BUILD           
#

BUILT_OBJECT_TYPE=NONE

SOURCES_PATH=PublicInterfaces
COMPILATION_IDL=YES

