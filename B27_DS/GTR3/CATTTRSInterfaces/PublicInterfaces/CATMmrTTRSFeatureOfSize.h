#ifndef CATMmrTTRSFeatureOfSize_H
#define CATMmrTTRSFeatureOfSize_H

// COPYRIGHT DASSAULT SYSTEMES 2002

/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */

/**
 * Typedef to describe the kind of feature of size for TTRS.
 */

enum CATMmrTTRSFeatureOfSize
{
  CATMmrTTRSFeatureOfSizeUnknown = 0,
  CATMmrTTRSHasFeatureOfSize = 1,
  CATMmrTTRSNotFeatureOfSize = 2
};

#endif
