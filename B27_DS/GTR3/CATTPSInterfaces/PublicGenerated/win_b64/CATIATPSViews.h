/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIATPSViews_h
#define CATIATPSViews_h

#ifndef ExportedByCATTPSPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __CATTPSPubIDL
#define ExportedByCATTPSPubIDL __declspec(dllexport)
#else
#define ExportedByCATTPSPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByCATTPSPubIDL
#endif
#endif

#include "CATIACollection.h"
#include "CATVariant.h"

class CATIABase;

extern ExportedByCATTPSPubIDL IID IID_CATIATPSViews;

class ExportedByCATTPSPubIDL CATIATPSViews : public CATIACollection
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall Item(const CATVariant & iIndex, CATIABase *& oAnnot)=0;


};

CATDeclareHandler(CATIATPSViews, CATIACollection);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIABase.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
