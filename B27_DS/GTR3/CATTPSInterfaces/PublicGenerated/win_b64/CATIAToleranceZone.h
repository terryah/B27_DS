/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAToleranceZone_h
#define CATIAToleranceZone_h

#ifndef ExportedByCATTPSPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __CATTPSPubIDL
#define ExportedByCATTPSPubIDL __declspec(dllexport)
#else
#define ExportedByCATTPSPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByCATTPSPubIDL
#endif
#endif

#include "CATBSTR.h"
#include "CATIABase.h"

extern ExportedByCATTPSPubIDL IID IID_CATIAToleranceZone;

class ExportedByCATTPSPubIDL CATIAToleranceZone : public CATIABase
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_Value(double & oValue)=0;

    virtual HRESULT __stdcall put_Value(double iValue)=0;

    virtual HRESULT __stdcall get_Form(CATBSTR & oForm)=0;

    virtual HRESULT __stdcall put_Form(const CATBSTR & iForm)=0;


};

CATDeclareHandler(CATIAToleranceZone, CATIABase);

#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
