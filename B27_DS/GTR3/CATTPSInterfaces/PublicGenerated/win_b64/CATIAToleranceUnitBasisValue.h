/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAToleranceUnitBasisValue_h
#define CATIAToleranceUnitBasisValue_h

#ifndef ExportedByCATTPSPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __CATTPSPubIDL
#define ExportedByCATTPSPubIDL __declspec(dllexport)
#else
#define ExportedByCATTPSPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByCATTPSPubIDL
#endif
#endif

#include "CATIABase.h"

extern ExportedByCATTPSPubIDL IID IID_CATIAToleranceUnitBasisValue;

class ExportedByCATTPSPubIDL CATIAToleranceUnitBasisValue : public CATIABase
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall Values(double & oValue1, double & oValue2)=0;

    virtual HRESULT __stdcall SetValues(double iValue1, double iValue2)=0;


};

CATDeclareHandler(CATIAToleranceUnitBasisValue, CATIABase);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
