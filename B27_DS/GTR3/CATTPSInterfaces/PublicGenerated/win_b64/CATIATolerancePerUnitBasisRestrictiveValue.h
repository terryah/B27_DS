/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIATolerancePerUnitBasisRestrictiveValue_h
#define CATIATolerancePerUnitBasisRestrictiveValue_h

#ifndef ExportedByCATTPSPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __CATTPSPubIDL
#define ExportedByCATTPSPubIDL __declspec(dllexport)
#else
#define ExportedByCATTPSPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByCATTPSPubIDL
#endif
#endif

#include "CATIABase.h"

extern ExportedByCATTPSPubIDL IID IID_CATIATolerancePerUnitBasisRestrictiveValue;

class ExportedByCATTPSPubIDL CATIATolerancePerUnitBasisRestrictiveValue : public CATIABase
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_Value(double & oValue)=0;

    virtual HRESULT __stdcall put_Value(double iValue)=0;


};

CATDeclareHandler(CATIATolerancePerUnitBasisRestrictiveValue, CATIABase);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
