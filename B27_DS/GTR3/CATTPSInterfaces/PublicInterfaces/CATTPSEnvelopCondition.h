#ifndef CATTPSEnvelopCondition_H
#define CATTPSEnvelopCondition_H

// COPYRIGHT DASSAULT SYSTEMES 1999

/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */

/**
 * Envelop Condition modifier values.
 */
enum CATTPSEnvelopCondition
{
  CATTPSECUnsupported = -1,
  CATTPSECNoModifier = 0,
  CATTPSECEnvelopCondition = 1
};

#endif
