#ifndef CATITPSMaterialCondition_H
#define CATITPSMaterialCondition_H

// COPYRIGHT DASSAULT SYSTEMES 1999

/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */

#include "IUnknown.h"
#include "CATTPSMaterialCondition.h"

extern "C" const IID IID_CATITPSMaterialCondition;

class CATITPSDatumSimple ;

/**
 * Interface for accessing Material Condition modifier on a TPS.
 *
 * Note that second argument is a default one ; it has only to be valuated,
 * when handled object is a CATITPSDatumSystem instance in order to define the
 * Simple Datum in the Reference Frame concerned by the Material Condition.
 *
 *   @see CATTPSMaterialCondition
 */
class CATITPSMaterialCondition : public IUnknown
{
  public:

    /**
     * Sets Material Condition modifier.
     * @return E_FAIL if iDatum not belongs to Reference Frame.
     */
    virtual HRESULT SetModifier (const CATTPSMaterialCondition iModifier,
                                 const CATITPSDatumSimple * iDatum = NULL) = 0;

    /**
     * Retrieves Material Condition modifier.
     * @return E_FAIL if iDatum not belongs to Reference Frame.
     */
    virtual HRESULT GetModifier (
                           CATTPSMaterialCondition* oModifier,
                           const CATITPSDatumSimple * iDatum = NULL) const = 0;

};
#endif
