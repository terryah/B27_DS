#ifndef CATTPSScanComposedOfMode_H
#define CATTPSScanComposedOfMode_H

// COPYRIGHT DASSAULT SYSTEMES 1999

/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */

/**
 * Describes list of Scanned processing allowed in method 
 * GetTTRSComposedOfTTRS. @see CATITPSTTRSServices
 */
enum CATTPSScanComposedOfMode
{  
  CATTPSScanComposedOfAllTTRS = 0,
  CATTPSScanComposedOfNoPositionnal = 1
};

#endif
