#ifndef CATTPSFreeState_H
#define CATTPSFreeState_H

/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */

// COPYRIGHT Dassault Systemes 2006

/**
 * Free State modifier values.
 */
enum CATTPSFreeState
{
  CATTPSFSUnsupported = -1,
  CATTPSFSFreeState = 0,
  CATTPSFSNotFreeState = 1
};

#endif
