#ifndef CATITPSDatum_H
#define CATITPSDatum_H

// COPYRIGHT DASSAULT SYSTEMES 1999

/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */

#include "IUnknown.h"

extern "C" const IID IID_CATITPSDatum;

/**
 * Typing interface for Datum entities TPS.
 * TPS for Technological Product Specifications.
 */
class CATITPSDatum : public IUnknown
{
  public:

};
#endif

