#ifndef CATIAFreeState_IDL
#define CATIAFreeState_IDL

// COPYRIGHT Dassault Systemes 2002

/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */

#include "CATIABase.idl"
#include "CATVariant.idl"

/**
 * Interface for accessing Free State modifier on a TPS.
 */
interface CATIAFreeState : CATIABase
{
    /**
     * Retrieves Free State modifier.
     */
  #pragma PROPERTY Modifier
   HRESULT get_Modifier (inout /*IDLRETVAL*/ CATBSTR oModifier);
   HRESULT put_Modifier (in CATBSTR iModifier);
};

// Interface name : CATIAFreeState
#pragma ID CATIAFreeState "DCE:cf8b8c66-2f89-11d6-be8a0002b341dd3a"
#pragma DUAL CATIAFreeState

// VB object name : FreeState (Id used in Visual Basic)
#pragma ID FreeState "DCE:cf8b8c67-2f89-11d6-be8a0002b341dd3a"
#pragma ALIAS CATIAFreeState FreeState

#endif
