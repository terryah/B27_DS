#ifndef CATIAProjectedToleranceZone_IDL
#define CATIAProjectedToleranceZone_IDL

// COPYRIGHT Dassault Systemes 2002

/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */

#include "CATIABase.idl"
#include "CATSafeArray.idl"

/**
 * Interface for accessing projected tolerance zone informations of a TPS.
 *
 *         ========|   Position      Length     
 *         /       |<----------->|<----------->|
 *  Toleranced     |             |             |
 *  Surface  - - - +------->     +=============+
 *      \          |\       \               \
 *        \        | Origin Direction       Projected Tolerance Zone
 *         ========|
 */
interface CATIAProjectedToleranceZone : CATIABase 
{
//    virtual HRESULT Reference (CATMathPoint * oOrigin,
//                               CATMathVector * oDirection);
    /**
     * Retrieves position of the projected tolerance zone (in millimeters).
     * The position defines the starting point of the tolerance zone.
     * This point can be computed by using Origin and Direction of the axis.
     */
  #pragma PROPERTY Position
   HRESULT get_Position (out /*IDLRETVAL*/ double oPosition);
    /**
     * Retrieves length of the projected tolerance zone (in millimeters).
     * The length defines the ending point of the tolerance zone. 
     * This point can be computed by using Origin and Direction of the axis.
     */
  #pragma PROPERTY Length
   HRESULT get_Length (out /*IDLRETVAL*/ double oLength);

   /**
   * Retrieves reference axis of the projected tolerance zone.
   * The returned point and vector define a axis system that is used to 
   * defined the 3D position of the tolerance zone.
   *   @param opReference
   *     The first 3 values of opReference correspond to the X,Y and Z
   *     values of the origin point respectively and the next 3 values
   *     correspond to the X,Y and Z values of the direction respectively.
   * @sample
   * This example gets the Projected Tolerance Zone reference in a VB Script
   * Dim oTab(6) As CATSafeArrayVariant
   * Set projTol = annotation.ProjectedToleranceZone
   *  projTol.GetProjectedTolZoneReference(oTab)
   *  oStream.Write "Projected Tol Zone Reference Point : " & oTab(0) & " " & oTab(1) & " " & oTab(2) & sLF
   *  oStream.Write "Projected Tol Zone Reference Vector : " & oTab(3) & " " & oTab(4) & " " & oTab(5) & sLF
   */
   HRESULT GetProjectedTolZoneReference (inout CATSafeArrayVariant opReference);
};

// Interface name : CATIAProjectedToleranceZone
#pragma ID CATIAProjectedToleranceZone "DCE:dab40c90-2f76-11d6-be890002b341dd3a"
#pragma DUAL CATIAProjectedToleranceZone

// VB object name : ProjectedToleranceZone (Id used in Visual Basic)
#pragma ID ProjectedToleranceZone "DCE:dab40c91-2f76-11d6-be890002b341dd3a"
#pragma ALIAS CATIAProjectedToleranceZone ProjectedToleranceZone

#endif
