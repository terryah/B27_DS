// COPYRIGHT Dassault Systemes 2006
//===================================================================
//
// CATITPSSemanticGDTTolerance.h
// Define the CATITPSSemanticGDTTolerance interface
//
//===================================================================
//
// Usage notes:
//   New interface: manage the properties precision, separator,
//                  trailing zero, leading zero for a
//                  Semantic Geometrical Tolerance
//
//===================================================================
//
//  Jan 2006                                            Creation: sob
//===================================================================
#ifndef CATITPSSemanticGDTTolerance_H
#define CATITPSSemanticGDTTolerance_H

/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */


#include "IUnknown.h"

extern "C" const IID IID_CATITPSSemanticGDTTolerance ;

//------------------------------------------------------------------

/**
* Interface representing Semantic Geometrical Tolerance.
*
*
* @example
*  // example is optional
*  CATITPSSemanticGDTTolerance* piTPSGDTTol = NULL;
*  rc = piTPS->QueryInterface(IID_CATITPSSemanticGDTTolerance,
*                             (void**) &piTPSGDTTol);
*
*/
class CATITPSSemanticGDTTolerance: public IUnknown
{
  public:

    /**
      * Retrieves the semantic Geometrical Tolerance precision.
	    *  @param oPrecision
     	*     semantic GDT precision
     */
    	virtual HRESULT GetPrecision (int * oPrecision) = 0;

    /**
     	* Value  the semantic Geometrical Tolerance  precision.
     	*  @param iPrecision
     	*     semantic GDT precision
     	*/
    	virtual HRESULT SetPrecision (int iPrecision) = 0;

    /**
      * Retrieves the semantic Geometrical Tolerance  separator.
	    *  @param oSeparator
     	*     semantic GDT separator
	    * 	all possible returns values with symbols corresponding are describe in the comment
      *	of the method  SetSeparator
      */
    	virtual HRESULT GetSeparator (int * oSeparator) = 0;

    /**
     	* Value  the semantic Geometrical Tolerance  separator.
     	*  @param iSeparator
     	*     semantic GDT separator
	    *     integer value of iSeparator corresponding to:
      *	0 	""
      *	1	  "/"
    	*	2	  ":"
    	*	3	  "("
    	*	4	  ")"
    	*	5	  "\\"
    	*	6	  ","
    	*	7	  "<"
    	*	8	  ">"
    	*	9	  "X"
    	*	10	"*"
    	*	11	"."
    	*	12	";"
    	*	13	"+"
    	*	14	"["
    	*	15	"]"
    	*	16	"-"
    	*	17	"_"
    	*	18	" "
     	*/
    	virtual HRESULT SetSeparator (int iSeparator) = 0;

    /**
      * Retrieves the semantic Geometrical Tolerance  DisplayTrailingZero.
	    *  @param oDisplayTrailingZero
      *     semantic GDT DisplayTrailingZero
	    *	0 : the TrailingZero is not displayed
	    *	1 : the TrailingZero is displayed
      */
    	virtual HRESULT GetDisplayTrailingZero (int * oDisplayTrailingZero) = 0;

    /**
      * Value  the semantic Geometrical Tolerance  DisplayTrailingZero.
      *  @param iTrailingZero
      *     semantic GDT DisplayTrailingZero
      *	0 : to not display the TrailingZero
	    *	1 : to display the TrailingZero
      */
    	virtual HRESULT SetDisplayTrailingZero (int iDisplayTrailingZero) = 0;

    /**
      * Retrieves the semantic Geometrical Tolerance  DisplayLeadingZero.
	    *  @param oDisplayLeadingZero
      *     semantic GDT LeadingZero
      *	0 : the LeadingZero is not displayed
	    *	1 : the LeadingZero is displayed
      */
    	virtual HRESULT GetDisplayLeadingZero (int * oDisplayLeadingZero) = 0;

    /**
      * Value  the semantic Geometrical Tolerance  DisplayLeadingZero.
      *  @param iDisplayLeadingZero
      *     semantic GDT LeadingZero
      *	0 : to not display the LeadingZero
	    *	1 : to display the LeadingZero
      */
    	virtual HRESULT SetDisplayLeadingZero (int iDisplayLeadingZero) = 0;


    // No constructors or destructors on this pure virtual base class
    // --------------------------------------------------------------
};

#endif
