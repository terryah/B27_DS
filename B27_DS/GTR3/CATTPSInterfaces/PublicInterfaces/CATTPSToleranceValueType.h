#ifndef CATTPSToleranceValueType_H
#define CATTPSToleranceValueType_H

// COPYRIGHT DASSAULT SYSTEMES 2001

enum CATTPSToleranceValueType
{
  CATTPSTolNumericalValue   = 0,
  CATTPSTolTabulatedValue   = 1,
  CATTPSTolNonUniformValue  = 2
};

#endif
