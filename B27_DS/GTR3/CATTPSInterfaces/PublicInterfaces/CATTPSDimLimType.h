#ifndef CATTPSDimLimType_H
#define CATTPSDimLimType_H

// COPYRIGHT DASSAULT SYSTEMES 2003

/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */

/**
 * Describes dimension limit type.
 */
enum CATTPSDimLimType
{
  CATTPSDLNotDefined  = 0,
  CATTPSDLNumerical   = 1,
  CATTPSDLTabulated   = 2,
  CATTPSDLSingleLimit = 3,
  CATTPSDLThreadValue = 4,
  CATTPSDLTolGenValue = 5,
  CATTPSDLInformation = 6
};
#endif
