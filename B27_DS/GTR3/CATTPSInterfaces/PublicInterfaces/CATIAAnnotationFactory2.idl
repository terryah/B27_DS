#ifndef CATIAAnnotationFactory2_IDL
#define CATIAAnnotationFactory2_IDL

// COPYRIGHT DASSAULT SYSTEMES 1999

/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */

#include "CATIAFactory.idl"
#include "CATVariant.idl"

interface CATIAUserSurface;
interface CATIAAnnotation2;
interface CATIAReference;
interface CATIADrawingComponent;

/**
 * Interface for the TPS Factory. This factory is implemented on the Set 
 * object. All the created specifications are added to the Set from which this 
 * interface is retrieved.
 */
interface CATIAAnnotationFactory2 : CATIAFactory
{
   /**
    * Create a Text.
    *   @param iSurf
    *      User surface needed to construct the Text.
    *   @param oText
    *      The new created Text.
    */
    HRESULT CreateText (in CATIAUserSurface iSurf, out /*IDLRETVAL*/ CATIAAnnotation2 oText);

    /**
    * Create a Text grouped to an annotation.
    *   @param iText
    *      Character string that makes up the text.
    *   @param iAnnot
    *      Annotation reference needed to group the Text.
    *   @param oText
    *      The new created Text.
    */
    HRESULT CreateTextOnAnnot (in CATBSTR iText,in CATIAAnnotation2 iAnnot, out /*IDLRETVAL*/ CATIAAnnotation2 oText);

   /**
    * Create a Text.
    *   @param iSurf
    *      User surface needed to construct the Text.
    *   @param iX
    *      X coordinate.
    *   @param iY
    *      Y coordinate.
    *   @param iZ
    *      Z coordinate.
    *   @param iWithLeader
    *      Create or not a leader on the annotation.
    *      If the leader is requested: The activated TPSView shall not be parallel to the surface pointed by the annotation Text.
    *      If the activated TPSView is parallel to the surface pointed:
    *                    - The leader will be disconnected
    *                    - The extremity of the leader will be positioned at the origin of the part
    *                    - The annotation Text is created but its status will be KO.
    *   @param oText
    *      The new created Text.
    */
    HRESULT CreateEvoluateText (in CATIAUserSurface iSurf, in double  iX, in double  iY, in double  iZ, in boolean iWithLeader, out /*IDLRETVAL*/ CATIAAnnotation2 oText);

   /**
    * Create a Datum Feature.
    *   @param iSurf
    *      User surface needed to construct the Datum Feature.
    *   @param oDatum
    *      The new created Datum Feature.
    */
    HRESULT CreateDatum (in CATIAUserSurface iSurf, out /*IDLRETVAL*/ CATIAAnnotation2 oDatum);

   /**
    * Create a Datum Feature.
    *   @param iSurf
    *      User surface needed to construct the Datum Feature.
    *   @param iX
    *      X coordinate.
    *   @param iY
    *      Y coordinate.
    *   @param iZ
    *      Z coordinate.
    *   @param iWithLeader
    *      Create or not a leader on the annotation.
    *      If the leader is requested: The activated TPSView shall not be parallel to the surface pointed by the annotation Datum.
    *      If the activated TPSView is parallel to the surface pointed:
    *                    - The leader will be disconnected
    *                    - The extremity of the leader will be positioned at the origin of the part
    *                    - The annotation Datum is created but its status will be KO.
    *   @param oDatum
    *      The new created Datum Feature.
    */
    HRESULT CreateEvoluateDatum (in CATIAUserSurface iSurf, in double  iX, in double  iY, in double  iZ, in boolean iWithLeader, out /*IDLRETVAL*/ CATIAAnnotation2 oDatum);

   /**
    * Create a Roughness.
    *   @param iSurf
    *      User surface needed to construct the Roughness.
    *   @param oRoughness
    *      The new created Roughness.
    */
    HRESULT CreateRoughness (in CATIAUserSurface iSurf, out /*IDLRETVAL*/ CATIAAnnotation2 oRoughness);

   /**
    * Create a FlagNote.
    *   @param iSurf
    *      User surface needed to construct the Flag Note.
    *   @param oFlagNote
    *      The new created Flag Note.
    */
    HRESULT CreateFlagNote (in CATIAUserSurface iSurf, out /*IDLRETVAL*/ CATIAAnnotation2 oFlagNote);

   /**
    * Create a Datum Target.
    *   @param iSurf
    *      User surface needed to construct the Datum Target.
    *   @param iDatum
    *      Datume Feature that is in relatino with the Datum Target.
    *   @param oDatum
    *      The new created Datum Target.
    */
    HRESULT CreateDatumTarget (in CATIAUserSurface iSurf, in CATIAAnnotation2  iDatum, out /*IDLRETVAL*/ CATIAAnnotation2 oDatum);

   /**
    * Creates a non semantic Dimension specification.
    *   @param iSurf
    *      User surface needed to construct the Dimension.
    *  @param iSubType : 1 CATTPSDiameterDimension
    *                    2 CATTPSRadiusDimension
    *  @param iType : 1 Linear Dimension
    *                 2 Angular Dimension
    *                 3 Second Linear Dim (Small diameter/radius for torus)
    *   @param oDimension
    *      The new created Dimension.
    */
    HRESULT CreateNonSemanticDimension (in CATIAUserSurface iSurf, in CATVariant iType, in CATVariant iSubType, out /*IDLRETVAL*/ CATIAAnnotation2 oDimension);

    /**
     * Creates a semantic Dimension specification.
     *   @param oDimension
     *      The new created Dimension.
     */
    HRESULT CreateSemanticDimension (in CATIAUserSurface iSurf, in CATVariant iType, in CATVariant iSubType, out /*IDLRETVAL*/ CATIAAnnotation2 oDimension);

   /**
    * Create a Tolerance Without a Reference Frame (DRF).
    *
    * iType = 1 : Straightness
    *         2 : AxisStraightness
    *         3 : Flatness
    *         4 : Circularity
    *         5 : Cylindricity
    *         6 : ProfileOfALine
    *         7 : ProfileOfASurface
    *         8 : Position
    */
    HRESULT CreateToleranceWithoutDRF(in CATVariant iIndex, in CATIAUserSurface iSurf, out /*IDLRETVAL*/ CATIAAnnotation2 oTolWoDRF);

   /**
    * Create a Reference Frame (DRF).
    *
    * iType = 1 : Straightness
    *         2 : AxisStraightness
    *         3 : Flatness
    *         4 : Circularity
    *         5 : Cylindricity
    *         6 : ProfileOfALine
    *         7 : ProfileOfASurface
    *         8 : Position
    */
    HRESULT CreateDatumReferenceFrame (out /*IDLRETVAL*/ CATIAAnnotation2 opDRF);

   /**
    * Create a Tolerance With a Reference Frame DRF.
    *
    * iType = 1 : Angularity
              2 : Perpendicularity
              3 : Parallelism
              4 : Position
              5 : Concentricity
              6 : Symetry
              7 : ProfileOfALine
              8 : ProfileOfASurface
              9 : Runout
    */
    HRESULT CreateToleranceWithDRF (in CATVariant iIndex, in CATIAUserSurface  iSurf, in CATIAAnnotation2 iDRF, out /*IDLRETVAL*/ CATIAAnnotation2  oTolWiDRF);

   /**
    * Instanciate an NOA from a Reference NOA.
    *   @param iNOA
    *      Reference NOA.
    *   @param iSurf
    *      User surface needed to construct the Dimension.
    *   @param oNOA
    *      The new instanciated NOA.
    */
    HRESULT InstanciateNOA (in CATIAAnnotation2 iNoa, in CATIAUserSurface iSurf, out /*IDLRETVAL*/ CATIAAnnotation2 oNoa);

    /**
     * Create a "Text" NOA 
     *   @param iSurf
     *      The user surface on which you apply the created NOA.
     *   @param oNoa
     *      The new created NOA.
     */
     HRESULT /*IDLHIDDEN*/ CreateTextNOA (in CATIAUserSurface iSurf,
                                          out /*IDLRETVAL*/ CATIAAnnotation2 oNoa) ;

    /**
     * Create a "Text" NOA 
     *   @param iSurf
     *      The user surface on which you apply the created NOA.
     *   @param iNOAType
     *      Type of the created NOA; this string defines the Type of Noa.
     *      This type can be filtered using the Filter command.
     *   @param oNoa
     *      The new created NOA.
     */
     HRESULT CreateTextNoteObjectAttribute( in CATIAUserSurface                iSurf,
                                            in CATBSTR                         iNOAType,
                                            out /*IDLRETVAL*/ CATIAAnnotation2 oNoa ) ;

    /**
     * Create a "Ditto" NOA 
     *   @param iSurf
     *      The user surface on which you apply the created NOA.
     *   @param iNOAType
     *      Type of the created NOA; this string defines the Type of Noa.
     *      This type can be filtered using the Filter command.
     *   @param iDitto
     *      The drawing component selected to provide NOA display.
     *   @param iStickToGeometryOption
     *      This flag is TRUE to tell the factory that Stick Ditto perpendicularly to geometry option is selected.
     *      In this case, the ditto is instantiated without frame or leader and its origin point is stuck and set on the selected geometry.
     *      In addition, the default anchor point position is the middle center.
     *      This argument is to be set to FALSE if leader attachment to point out geometry is expected.
     *   @param oNoa
     *      The new created NOA.
     */
     HRESULT CreateDittoNOA (in CATIAUserSurface                iSurf,
                             in CATBSTR                         iNOAType,
                             in CATIADrawingComponent           iDitto,
                             in boolean                         iStickToGeometryOption,
                             out /*IDLRETVAL*/ CATIAAnnotation2 oNoa );

    /**
     * Create a Weld 
     *   @param iSurf
     *      The user surface on which you apply the new Weld.
     *   @param oWeld
     *      The new created Weld Feature.
     */
     HRESULT CreateWeld ( in CATIAUserSurface iSurf,
                          out /*IDLRETVAL*/ CATIAAnnotation2 oWeld );

};

// Interface name : CATIAAnnotationFactory2
#pragma ID CATIAAnnotationFactory2 "DCE:fcf2c8f6-7a2b-41f7-af9493a8db4e3fca"
#pragma DUAL CATIAAnnotationFactory2

// VB object name : AnnotationFactory 
#pragma ID AnnotationFactory2 "DCE:5cd82db9-810c-4b3f-944ca216d2064d80"
#pragma ALIAS CATIAAnnotationFactory2 AnnotationFactory2

#endif
