#ifndef CATITPSExtendedCylinder_H
#define CATITPSExtendedCylinder_H

// COPYRIGHT DASSAULT SYSTEMES 2014

/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */

#include "CATTPSItfCPP.h"
#include "CATBaseUnknown.h"

class CATITTRS;
class CATMathPoint;
class CATUnicodeString;

/**
* Interface for the TPSCGExtendedCylinder object. The purpose of the interface
  is to Get and Get the various attributes of the TPSCGExtendedCylinder feature.
  The interface can be retrieved by doing a QI on the TPSCGExtendedCylinder component.
*/

extern ExportedByCATTPSItfCPP  IID IID_CATITPSExtendedCylinder;
class ExportedByCATTPSItfCPP CATITPSExtendedCylinder: public CATBaseUnknown
{
CATDeclareInterface;

public:

  /**
  * Gets the center point of the extended cylinder.
  *  @param oPoint
  *   canter point of the extended cylinder.
  *  returns S_OK when the point has been correctly retrieved, 
  *  returns E_FAIL otherwise.
  */
  virtual HRESULT GetPosition(CATMathPoint &oPoint) const = 0;

  /**
  * Sets the center point of the extended cylinder.
  *  @param iPoint
  *   canter point of the extended cylinder.
  *  returns S_OK when the point has been correctly set, 
  *  returns E_FAIL otherwise.
  */
  virtual HRESULT SetPosition(const CATMathPoint &iPoint) = 0;

  /**
  * Gets the start point of the extended cylinder.
  *  @param oStartPoint
  *   start point of the extended cylinder.
  *  returns S_OK when the point has been correctly retrieved, 
  *  returns E_FAIL otherwise.
  */
  virtual HRESULT GetStartPoint(CATMathPoint &oStartPoint) const = 0;

  /**
  * Sets the start point of the extended cylinder.
  *  @param iStartPoint
  *   start point of the extended cylinder.
  *  returns S_OK when the point has been correctly set, 
  *  returns E_FAIL otherwise.
  */
  virtual HRESULT SetStartPoint(const CATMathPoint &iStartPoint) = 0;

  /**
  * Gets the end point of the extended cylinder.
  *  @param oEndPoint
  *   end point of the extended cylinder.
  *  returns S_OK when the point has been correctly retrieved, 
  *  returns E_FAIL otherwise.
  */
  virtual HRESULT GetEndPoint(CATMathPoint &oEndPoint) const = 0;

  /**
  * Sets the end point of the extended cylinder.
  *  @param iEndPoint
  *   End point of the extended cylinder.
  *  returns S_OK when the point has been correctly set, 
  *  returns E_FAIL otherwise.
  */
  virtual HRESULT SetEndPoint(const CATMathPoint &iEndPoint) = 0;

  /**
  * Gets the diameter of the extended cylinder.
  *  @param oDiameter
  *   diameter of the extended cylinder
  *  returns S_OK when the diameter has been correctly retrieved, 
  *  returns E_FAIL otherwise.
  */
  virtual HRESULT GetDiameter(double &oDiameter) = 0;

  /**
  * Sets the diameter of the extended cylinder.
  *  @param iDiameter
  *   Diameter of the extended cylinder.
  *  returns S_OK when the diameter has been correctly set, 
  *  returns E_FAIL otherwise.
  */
  virtual HRESULT SetDiameter(const double &iDiameter) = 0;

  /**
  * Gets the offset of the extended cylinder.
  *  @param oOffset
  *   offset of the extended cylinder.
  *  returns S_OK when the offset has been correctly retrieved, 
  *  returns E_FAIL otherwise.
  */
  virtual HRESULT GetOffset(double &oOffset) = 0;

  /**
  * Sets the offset of the extended cylinder.
  *  @param iOffset
  *   offset of the extended cylinder.
  *  returns S_OK when the offset has been correctly set, 
  *  returns E_FAIL otherwise.
  */
  virtual HRESULT SetOffset(const double &iOffset) = 0;

  /**
  * Gets the length of the extended cylinder.
  *  @param oLength
  *   length of the extended cylinder.
  *  returns S_OK when the length has been correctly retrieved, 
  *  returns E_FAIL otherwise.
  */
  virtual HRESULT GetLength(double &oLength) = 0;

  /**
  * Sets the length of the extended cylinder.
  *  @param iLength
  *   Length of the extended cylinder.
  *  returns S_OK when the length has been correctly set, 
  *  returns E_FAIL otherwise.
  */
  virtual HRESULT SetLength(const double &iLength) = 0;

  /**
  * Gets the reference plane TTRS of the extended cylinder.
  *  @param opiReferencePlaneTTRS
  *   Reference plane TTRS of the extended cylinder.
  *  returns S_OK when the TTRS has been correctly retrieved, 
  *  returns E_FAIL otherwise.
  */
  virtual HRESULT GetReferencePlaneTTRS(CATITTRS **opiReferencePlaneTTRS) = 0;

  /**
  * Gets the represented TTRS of the extended cylinder.
  *  @param opiRepresentedCylinderTTRS
  *   Represented TTRS of the extended cylinder.
  *  returns S_OK when the TTRS has been correctly retrieved, 
  *  returns E_FAIL otherwise.
  */
  virtual HRESULT GetRepresentedCylinderTTRS(CATITTRS **opiRepresentedCylinderTTRS) = 0;
};
//-----------------------------------------------------------------------
CATDeclareHandler(CATITPSExtendedCylinder, CATBaseUnknown);
#endif
