#ifndef CATTPSSingleLimit_H
#define CATTPSSingleLimit_H

// COPYRIGHT DASSAULT SYSTEMES 1999

/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */

/**
 * Describes single limits values.
 */
enum CATTPSSingleLimit
{
  CATTPSSLUnsupported = -1,
  CATTPSSLNotDefined = 0,
  CATTPSSLMaximum = 1,
  CATTPSSLMinimum = 2,
  CATTPSSLAsInformation = 3
};

#endif

