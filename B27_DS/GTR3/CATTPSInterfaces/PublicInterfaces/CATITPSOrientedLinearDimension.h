#ifndef CATITPSOrientedLinearDimension_H
#define CATITPSOrientedLinearDimension_H

// COPYRIGHT DASSAULT SYSTEMES 2013

/**
* @CAA2Level L1
* @CAA2Usage U3
*/


#include "IUnknown.h"

extern "C" const IID IID_CATITPSOrientedLinearDimension;

/**
 * Interface implemented by oriented linear dimension tolerance.
 * <b>Role</b>: Objects that implement this typing interface are oriented
 * linear dimension tolerances. This type belongs to the dimension super type.
 */

class CATITPSOrientedLinearDimension : public IUnknown
{
  public:

};
#endif
