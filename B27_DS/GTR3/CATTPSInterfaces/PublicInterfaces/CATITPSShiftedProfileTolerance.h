#ifndef CATITPSShiftedProfileTolerance_H
#define CATITPSShiftedProfileTolerance_H

// COPYRIGHT DASSAULT SYSTEMES 2001

/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */

#include "CATTPSItfCPP.h"
#include "CATBaseUnknown.h"

class CATMathPoint;

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByCATTPSItfCPP IID IID_CATITPSShiftedProfileTolerance;
#else
extern "C" const IID IID_CATITPSShiftedProfileTolerance ;
#endif

/**
 * Interface for accessing shifted tolerance zone informations of a TPS.
 */
class ExportedByCATTPSItfCPP CATITPSShiftedProfileTolerance : public CATBaseUnknown
{
  CATDeclareInterface;

  public:

    /**
     * Retrieves shift value of tolerance zone (in millimeters).
     * The shift value is the distance between the toleranced surface
     * and the median surface of tolerance zone.
     *   @param oValue
     *     Always positive because shift side is given by GetShiftSide
     *     method.
     */
    virtual HRESULT GetShiftValue (double* oValue) const = 0;

    /**
     * Retrieves shift side.
     *   @param opPoint
     *     a mathematical point located on the shift side of surface.
     */
    virtual HRESULT GetShiftSide (CATMathPoint ** opPoint) const = 0;

    /**
     * Retrieves the shift direction by two points.
     *   @param opOrigin
     *     a mathematical point located on the surface.
     *   @param opEnd
     *     a mathematical point located that define the shift direction.
     */
    virtual HRESULT GetShiftDirection ( CATMathPoint ** opOrigin, 
                                        CATMathPoint ** opEnd) const = 0;

};

CATDeclareHandler (CATITPSShiftedProfileTolerance, CATBaseUnknown);

#endif
