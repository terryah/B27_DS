#ifndef CATITPSView_H
#define CATITPSView_H

// COPYRIGHT DASSAULT SYSTEMES 2000

/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */

#include "CATBaseUnknown.h"
#include "CATTPSItfCPP.h"
#include "CATBoolean.h"
#include "CATTPSViewType.h"

class CATITPSSet;
class CATITPSList;
class CATIDftView;
class CATITTRSList;
class CATMathPlane;
class CATMathPoint2D;

extern "C" const IID IID_CATITPSView;

/**
 * Interface to TPS View object.
 * <b>Role</b>: Created and manipulated in 3D, a TPS View allows to define
 * support plane for TPS annotations.
 * <p>
 * A TPSView is associated with a drafting view.
 */
class CATITPSView : public CATBaseUnknown
{
  CATDeclareInterface;

  public:

    /**
     * Retrieves tolerancing set the TPSView belongs too.
     */
    virtual HRESULT GetSet (CATITPSSet ** opiSet) const = 0;

    /**
     * Retrieves the TPS that are using this TPSView as support.
     *   @param ioList
     *     List of returned TPS
     */
    virtual HRESULT GetTPSs (CATITPSList** iopiList) const = 0;

    /**
     * Retrieves the drafting view associated to the TPSView.
     *   @param oView
     *     Drafting view
     */
    virtual HRESULT GetDraftingView (CATIDftView ** opiView) const = 0;

    /**
     * Retrieves the view activity. Allows to known if the TPSView is the 
     * active one (or current) in the set.
     */
    virtual HRESULT IsActive (CATBoolean * oActivity) const = 0;

    /**
     * Sets the TTRS list associated to the Specification.
     */
    virtual HRESULT SetTTRS (const CATITTRSList * iTTRS) = 0;

    /**
     * Retrieves a CATITTRSList to read a list of features
     * upon which is applied the current specification.
     *
     * Note that this list may have a null count; in such a case,
     * we have to consider this specification as a TPS applied to
     * the whole part.
     *
     * Result of this call will mostly be a singleton, but it can
     * also happen situations where the list size is higher than 1;
     * this way, we have defined a specification related to
     * several surfaces (regarless the order in which the surfaces
     * are stored)
     */
    virtual HRESULT GetTTRS (CATITTRSList ** oTTRS) const = 0;


    /**
     * Retrieves the Associativity state, saved or Not. 
     * if TRUE, the view updates when referenced geometry moves 
     * if FALSE, there is no update of the view according geometry.
     *   @param oAssociativeState
     *     The Associativity state.
     */
    virtual HRESULT GetAssociativeState (CATBoolean * oAssociativeState) const = 0;

    /**
     * Sets the Associativity state, saved or Not. 
     *   @param iAssociativeState
     *     The new Associativity state.
     */
    virtual HRESULT SetAssociativeState (
                                      const CATBoolean iAssociativeState) = 0;

    /**
     * Retrieves Plane on which this item
     * lies on (The Plane of the annotation).
     *   @param oPlane
     *     The offset of the TPS.
     *   @return
     *      S_OK if succeded (oPlane must be deleted after use)
     *      E_FAIL if oPlane is not valuated.
     */
    virtual HRESULT GetMathPlane (CATMathPlane ** opPlane) const = 0;

    /**
     * Retrieves the type of the TPSView.
     *   @param oViewType
     *     The Type of the TPSView
     *   @return
     *      S_OK if succeded.
     *      E_FAIL if view type fails.
     */
    virtual HRESULT GetViewType (CATTPSViewType * oViewType) const = 0;

    /**
     * Sets the DisplayRatio for the TPSView.
     *   @param iDisplayRatio
     *     The display ratio of the View.
     *   @return
     *      S_OK if succeded
     *      E_FAIL otherwise
     */
    virtual HRESULT SetDisplayRatio (const double iDisplayRatio) = 0;

    /**
     * Retrieves DisplayRatio of the TPSView.
     *   @param oDisplayRatio
     *     The display ratio of the View.
     *   @return
     *      S_OK if succeded
     *      E_FAIL if the DisplayRatio is not valuated.
     */
    virtual HRESULT GetDisplayRatio (double * oDisplayRatio) const = 0;

    /**
     * Retrieves the ViewBound of the TPSView. ViewBound will include all the
     * annotations contained in the view.
     *   @param oBottomLeftCorner
     *     Position of the Bottom Left corner of the view bound in the form of
     *     CATMathPoint2D, in the view plane. This value will be with respect
     *     to the view coordinate system.
     *   @param oTopRightCorner
     *     Position of the Top Right corner of the view bound in the form of
     *     CATMathPoint2D, in the view plane. This value will be with respect
     *     to the view coordinate system.
     *   @return
     *      S_OK if succeded
     *      E_FAIL if the ViewBound failed to be computed.
     */
    virtual HRESULT GetViewBound(CATMathPoint2D &oBottomLeftCorner,
                                 CATMathPoint2D &oTopRightCorner) = 0;

};

CATDeclareHandler(CATITPSView, CATBaseUnknown);

#endif
