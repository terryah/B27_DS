#ifndef CATITPSOrientedAngularDimension_H
#define CATITPSOrientedAngularDimension_H

// COPYRIGHT DASSAULT SYSTEMES 2013

/**
* @CAA2Level L1
* @CAA2Usage U3
*/

#include "IUnknown.h"

extern "C" const IID IID_CATITPSOrientedAngularDimension;

/**
 * Interface implemented by oriented angular dimension tolerance.
 * <b>Role</b>: Objects that implement this typing interface are oriented
 * angular dimension tolerances. This type belongs to the dimension super type.
 */

class CATITPSOrientedAngularDimension : public IUnknown
{
  public:

};
#endif
