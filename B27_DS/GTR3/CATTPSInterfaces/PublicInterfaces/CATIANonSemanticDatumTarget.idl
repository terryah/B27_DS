#ifndef CATIANonSemanticDatumTarget_IDL
#define CATIANonSemanticDatumTarget_IDL

// COPYRIGHT Dassault Systemes 2002

/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */

#include "CATIABase.idl"

/**
 * Interface Managing Non Semantic Datum Target.
 */
interface CATIANonSemanticDatumTarget : CATIABase
{
  /**
  * Retrieves or sets Upper Label.
  */
#pragma PROPERTY UpLabel
  HRESULT get_UpLabel (inout /*IDLRETVAL*/ CATBSTR oUpLabel);
  HRESULT put_UpLabel (in CATBSTR iUpLabel);

  /**
  * Retrieves or sets Lower Label.
  */
#pragma PROPERTY LowLabel
  HRESULT get_LowLabel (inout /*IDLRETVAL*/ CATBSTR oLowLabel);
  HRESULT put_LowLabel (in CATBSTR iLowLabel);


  /**
  * Retrieves or sets the type of specifier.
  * <br><b>Legal values</b>:
  * <ul>
  *<li>None</li>
  *<li>Square</li>
  *<li>Diameter</li>
  */
#pragma PROPERTY TypeSpecifier
  HRESULT get_TypeSpecifier (inout /*IDLRETVAL*/ CATBSTR oSpecName);
  HRESULT put_TypeSpecifier (in CATBSTR iSpecName);

};

// Interface name : CATIANonSemanticDatumTarget
#pragma ID CATIANonSemanticDatumTarget "DCE:abfb79b8-a6aa-41b3-a8cb9680335e0e50"
#pragma DUAL CATIANonSemanticDatumTarget

// VB object name : DatumTarget (Id used in Visual Basic)
#pragma ID NonSemanticDatumTarget "DCE:df862846-b510-4b16-8857b4f6719ef0bd"
#pragma ALIAS CATIANonSemanticDatumTarget NonSemanticDatumTarget

#endif
