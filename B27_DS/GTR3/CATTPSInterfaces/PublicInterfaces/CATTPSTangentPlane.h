#ifndef CATTPSTangentPlane_H
#define CATTPSTangentPlane_H


/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */

// COPYRIGHT Dassault Systemes 2006

/**
 * Tangent Plane modifier values.
 */
enum CATTPSTangentPlane
{
  CATTPSTPUnsupported = -1,
  CATTPSTPNotTangentPlane = 0,
  CATTPSTPTangentPlane = 1
};

#endif
