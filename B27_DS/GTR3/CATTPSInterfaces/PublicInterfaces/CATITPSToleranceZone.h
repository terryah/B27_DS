#ifndef CATITPSToleranceZone_H
#define CATITPSToleranceZone_H

// COPYRIGHT DASSAULT SYSTEMES 1999

/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */

#include "IUnknown.h"
#include "CATTPSToleranceZoneForm.h"
#include "CATTPSToleranceValueType.h"

class CATMathVector;
class CATMathAxis;
class CATITTRS;

extern "C" const IID IID_CATITPSToleranceZone;

/**
 * Interface for accessing tolerance zone informations of a TPS.
 * Warning <tt>CATMathSetOfVectors</tt> is a protected interface (not exposed)
 * of Mathematics framework. Subject to change.
 *   @see CATTPSToleranceZoneForm
 */
class CATITPSToleranceZone : public IUnknown
{
  public:

    /**
     * Sets tolerance zone value (in millimeters).
     */
    virtual HRESULT SetValue (const double iValue) = 0;

    /**
     * Retrieves tolerance zone value (in millimeters).
     */
    virtual HRESULT GetValue (double* oValue) const = 0;

    /**
     * Sets tolerance zone form.
     */
    virtual HRESULT SetForm (const CATTPSToleranceZoneForm iForm) = 0;

    /**
     * Retrieves tolerance zone form.
     */
    virtual HRESULT GetForm (CATTPSToleranceZoneForm* oForm) const = 0;

    /**
     * @deprecated V5R15
     */
    virtual HRESULT SetDirections (const int iVectorCount,
                                   const CATMathVector * iVectors,
                                   const CATMathAxis * iAxis) = 0;

    /**
     * @deprecated V5R15
     */
    virtual HRESULT GetDirections (int * oVectorCount,
                                   CATMathVector ** oVectors,
                                   CATMathAxis * oAxis) const = 0;

    /**
     * Get the direction in which we must apply the tolerance zone.
     * If the class of the TTRS is cylindrical, the normal is the direction of the cylindre.
     * If the class of the TTRS is planar, the normal is the normal of the plane.
     */
    virtual HRESULT GetDirection (CATMathVector  * oNormal,
                                  CATITTRS      ** opiDirection) const = 0;

    /**
     * Enum to describe the value type of tolerance zone.
     * @param Numerical  
     *   represent Numerical tolerance zone type.
     * @param Tabulated  
     *   represent Tabulated tolerance zone type.
     * @param NonUniform  
     *   represent NonUniform tolerance zone type.
     */
    enum ValueType {
      Numerical,
      Tabulated,
      NonUniform
    };
    
    /**
     * Sets the value type of tolerance zone.     
     *  @param iType
     *   value type.
     *  returns S_OK when the value type has been correctly set, 
     *  returns E_FAIL otherwise.
     */
    virtual HRESULT SetToleranceValueType (const CATTPSToleranceValueType iType) = 0;

    /**
     * Retrieve the value type of tolerance zone.     
     *  @param oType
     *   value type.
     *  returns S_OK when the value type has been correctly retrieved, 
     *  returns E_FAIL otherwise.
     */
    virtual HRESULT GetToleranceValueType (CATTPSToleranceValueType& oType) const = 0;

    /**
     * Sets boundaries of Non Uniform tolerance zone.   
     *  @param ipMMBoundary
     *   Maximum material boundary. 
     *  @param ipLMBoundary
     *   Least material boundary.
     *  returns S_OK when the boundaries have been correctly set, 
     *  returns E_FAIL otherwise.
     */
    virtual HRESULT SetNonUniformToleranceZone (const CATITTRS* ipMMBoundary, const CATITTRS* ipLMBoundary) = 0;

    /**
     * Retrieves  boundaries of Non Uniform tolerance zone.  
     *  @param opMMBoundary
     *   Maximum material boundary. 
     *  @param opLMBoundary
     *   Least material boundary.
     *  returns S_OK when the boundaries have been correctly retrieved, 
     *  returns E_FAIL otherwise.
     */
    virtual HRESULT GetNonUniformToleranceZone (CATITTRS** opMMBoundary, CATITTRS** opLMBoundary) const = 0;

    /**
     * Sets Any Cross Section symbol.
     *  @param iValue
     *   Any Cross Section.
     *  returns S_OK when the value has been correctly set, 
     *  returns E_FAIL otherwise.
     */
    virtual HRESULT SetAnyCrossSection (const CATBoolean iValue) = 0;

    /**
     * Retrieves Any Cross Section symbol.
     *  @param oValue
     *   Any Cross Section.
     *  returns S_OK when the value has been correctly retrieved, 
     *  returns E_FAIL otherwise.
     */
    virtual HRESULT GetAnyCrossSection (CATBoolean& oValue) const = 0;

    /**
     * Checks if the options for ACS will be proposed of not
     *  @param obACSCreation     
     *  returns S_OK when the ACS can be proposed
     *  returns E_FAIL otherwise.
     */
    virtual HRESULT CheckACSCreation (CATBoolean &obACSCreation) = 0;


};
#endif
