#ifndef CATITPSNonSemantic_H
#define CATITPSNonSemantic_H

// COPYRIGHT DASSAULT SYSTEMES 1999

/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */

#include "IUnknown.h"

extern "C" const IID IID_CATITPSNonSemantic;

/**
 * Typing interface for all Non Semantic tolerances TPS.
 * TPS for Technological Product Specifications.
 */
class CATITPSNonSemantic: public IUnknown
{
  public:

};
#endif
