#ifndef CATIAAnnotationFactory_IDL
#define CATIAAnnotationFactory_IDL

// COPYRIGHT DASSAULT SYSTEMES 1999

/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */

#include "CATIAFactory.idl"
#include "CATVariant.idl"

interface CATIAUserSurface;
interface CATIAAnnotation;
interface CATIAReference;
interface CATIANoa;

/**
 * Interface for the TPS Factory. This factory is implemented on the Set 
 * object. All the created specifications are added to the Set from which this 
 * interface is retrieved.
 */
interface CATIAAnnotationFactory : CATIAFactory
{

   /**
    * Create a Text.
    *   @param iAnnotation
    *      Annotation on which the Text will be .
    *   @param oText
    *      The new created Text.
    */
    HRESULT CreateText (in CATIAUserSurface iSurf,
                        out /*IDLRETVAL*/ CATIAAnnotation oText);

   /**
    * Create a Text grouped to an annotation.
    *   @param iText
    *      Character string that makes up the text.
    *   @param iAnnot
    *      Annotation reference needed to group the Text.
    *   @param oText
    *      The new created Text.
    */
    HRESULT CreateTextOnAnnot (in CATBSTR iText,in CATIAAnnotation iAnnot,
                               out /*IDLRETVAL*/ CATIAAnnotation oText);

   /**
    * Create a Text.
    *   @param iSurf
    *      User surface needed to construct the Text.
    *   @param iX
    *      X coordinate.
    *   @param iY
    *      Y coordinate.
    *   @param iZ
    *      Z coordinate.
    *   @param iWithLeader
    *      Create or not a leader on the annotation.
    *      If the leader is requested: The activated TPSView shall not be parallel to the surface pointed by the annotation Text.
    *      If the activated TPSView is parallel to the surface pointed:
    *                     - The leader will be disconnected
    *                     - The extremity of the leader will be positioned at the origin of the part
    *                     - The annotation Text is created but its status will be KO.
    *   @param oText
    *      The new created Text.
    */
    HRESULT CreateEvoluateText (in CATIAUserSurface iSurf,
                                in double  iX,
                                in double  iY,
                                in double  iZ,
                                in boolean iWithLeader,
                                out /*IDLRETVAL*/ CATIAAnnotation oText);

   /**
    * Create a Datum Feature.
    *   @param iSurf
    *      User surface needed to construct the Datum Feature.
    *   @param oDatum
    *      The new created Datum Feature.
    */
    HRESULT CreateDatum (in CATIAUserSurface iSurf,
                         out /*IDLRETVAL*/ CATIAAnnotation oDatum);

   /**
    * Create a Datum Feature.
    *   @param iSurf
    *      User surface needed to construct the Datum Feature.
    *   @param iX
    *      X coordinate.
    *   @param iY
    *      Y coordinate.
    *   @param iZ
    *      Z coordinate.
    *   @param iWithLeader
    *      Create or not a leader on the annotation.
    *      If the leader is requested: The activated TPSView shall not be parallel to the surface pointed by the annotation Datum.
    *      If the activated TPSView is parallel to the surface pointed:
    *                     - The leader will be disconnected
    *                     - The extremity of the leader will be positioned at the origin of the part
    *                     - The annotation Datum is created but its status will be KO.
    *   @param oDatum
    *      The new created Datum Feature.
    */
    HRESULT CreateEvoluateDatum (in CATIAUserSurface iSurf,
                                 in double  iX,
                                 in double  iY,
                                 in double  iZ,
                                 in boolean iWithLeader,
                                 out /*IDLRETVAL*/ CATIAAnnotation oDatum);

   /**
    * Create a Roughness.
    *   @param iSurf
    *      User surface needed to construct the Roughness.
    *   @param oRoughness
    *      The new created Roughness.
    */
    HRESULT CreateRoughness (in CATIAUserSurface iSurf,
                             out /*IDLRETVAL*/ CATIAAnnotation oRoughness);

   /**
    * Create a FlagNote.
    *   @param iSurf
    *      User surface needed to construct the Flag Note.
    *   @param oFlagNote
    *      The new created Flag Note.
    */
    HRESULT CreateFlagNote (in CATIAUserSurface iSurf,
                            out /*IDLRETVAL*/ CATIAAnnotation oFlagNote);

   /**
    * Create a Datum Target.
    *   @param iSurf
    *      User surface needed to construct the Datum Target.
    *   @param iDatum
    *      Datume Feature that is in relatino with the Datum Target.
    *   @param oDatum
    *      The new created Datum Target.
    */
    HRESULT CreateDatumTarget (in CATIAUserSurface iSurf,
                               in CATIAAnnotation  iDatum,
                               out /*IDLRETVAL*/ CATIAAnnotation oDatum);

   /**
    * Creates a non semantic Dimension specification.
    *   @param iSurf
    *      User surface needed to construct the Dimension.
    *  @param iDimensionType
    *      Type of the Dimension
    *                0 : CATTPSUndefDimension
    *                1 : CATTPSLinearDimension
    *                2 : CATTPSAngularDimension
    *                3 : CATTPSSecondLinearDim
    *                4 : CATTPSChamferDimension
    *                5 : CATTPSOrientedLinearDimension
    *                6 : CATTPSOrientedAngularDimension
    *  @param iLinearDimSubType
    *      Sub type of LinearDimension type
    *                0 : CATTPSDistanceDimension
    *                1 : CATTPSDiameterDimension
    *                2 : CATTPSRadiusDimension
    *                3 : CATTPSThreadDimension
    *                4 : CATTPSChamfDistDistDimension
    *                5 : CATTPSChamfDistAngDimension
    *   @param oDimension
    *      The new created Dimension.
    */
    HRESULT CreateNonSemanticDimension (in CATIAUserSurface iSurf,
                                        in CATVariant iDimensionType,
                                        in CATVariant iLinearDimSubType,
                                        out /*IDLRETVAL*/ CATIAAnnotation oDimension);

    /**
     * Creates a semantic Dimension specification.
     *   @param oDimension
     *      The new created Dimension.
     */
    HRESULT CreateSemanticDimension (in CATIAUserSurface iSurf,
                                     in CATVariant iType,
                                     in CATVariant iSubType,
                                     out /*IDLRETVAL*/ CATIAAnnotation oDimension);

   /**
    * Create a Tolerance Without a Reference Frame (DRF).
    *
    * iType = 1 : Straightness
    *         2 : AxisStraightness
    *         3 : Flatness
    *         4 : Circularity
    *         5 : Cylindricity
    *         6 : ProfileOfALine
    *         7 : ProfileOfASurface
    *         8 : Position
    */
    HRESULT CreateToleranceWithoutDRF(
                                  in CATVariant                     iIndex,
                                  in CATIAUserSurface               iSurf,
                                  out /*IDLRETVAL*/ CATIAAnnotation oTolWoDRF);

   /**
    * Create a Reference Frame (DRF).
    *
    * iType = 1 : Straightness
    *         2 : AxisStraightness
    *         3 : Flatness
    *         4 : Circularity
    *         5 : Cylindricity
    *         6 : ProfileOfALine
    *         7 : ProfileOfASurface
    *         8 : Position
    */
    HRESULT CreateDatumReferenceFrame (out /*IDLRETVAL*/ CATIAAnnotation opDRF);

   /**
    * Create a Tolerance With a Reference Frame DRF.
    *
    * iType = 1 : Angularity
              2 : Perpendicularity
              3 : Parallelism
              4 : Position
              5 : Concentricity
              6 : Symetry
              7 : ProfileOfALine
              8 : ProfileOfASurface
              9 : Runout
    */
    HRESULT CreateToleranceWithDRF (in CATVariant                      iIndex,
                                    in CATIAUserSurface                iSurf,
                                    in CATIAAnnotation                 iDRF,
                                    out /*IDLRETVAL*/ CATIAAnnotation  oTolWiDRF);

   /**
    * Instanciate an NOA from a Reference NOA.
    *   @param iNOA
    *      Reference NOA.
    *   @param iSurf
    *      User surface needed to construct the Dimension.
    *   @param oNOA
    *      The new instanciated NOA.
    */
    HRESULT InstanciateNOA (in CATIANoa                       iNoa,
                            in CATIAUserSurface               iSurf,
                            out /*IDLRETVAL*/ CATIAAnnotation oNoa);

    /**
     * Create a "Text" NOA 
     *   @param iSurf
     *      The user surface on which you apply the created NOA.
     *   @param oNoa
     *      The new created NOA.
     */
     HRESULT /*IDLHIDDEN*/ CreateTextNOA (in CATIAUserSurface iSurf,
                                          out /*IDLRETVAL*/ CATIANoa oNoa) ;

     /**
     * Create a "Text" NOA (Note Object Attribute)
     *   @param iSurf
     *      The user surface on which you apply the created NOA.
     *   @param iNOAType
     *      Type of the created NOA; this string defines the Type of Noa.
     *      This type can be filtered using the Filter command.
     *   @param oNoa
     *      The new created NOA.
     */
     HRESULT CreateTextNoteObjectAttribute (in CATIAUserSurface iSurf,
                                            in CATBSTR iNOAType,
                                            out /*IDLRETVAL*/ CATIANoa oNoa);

};

// Interface name : CATIAAnnotationFactory
#pragma ID CATIAAnnotationFactory "DCE:88d2652f-18ad-0000-0280020cc3000000"
#pragma DUAL CATIAAnnotationFactory

// VB object name : AnnotationFactory 
#pragma ID AnnotationFactory "DCE:88d26558-7fba-0000-0280020cc3000001"
#pragma ALIAS CATIAAnnotationFactory AnnotationFactory

#endif
