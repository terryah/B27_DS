#ifndef CATITPSPatternTruePos_H
#define CATITPSPatternTruePos_H

// COPYRIGHT DASSAULT SYSTEMES 1999

/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */

#include "IUnknown.h"

extern "C" const IID IID_CATITPSPatternTruePos;

/**
 * Typing interface for Pattern True Position TPS (form or position tolerance).
 * TPS for Technological Product Specifications.
 */
class CATITPSPatternTruePos : public IUnknown
{
  public:

};
#endif

