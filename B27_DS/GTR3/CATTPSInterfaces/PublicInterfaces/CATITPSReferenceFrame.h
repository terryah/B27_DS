#ifndef CATITPSReferenceFrame_H
#define CATITPSReferenceFrame_H

// COPYRIGHT DASSAULT SYSTEMES 1999

/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */

#include "IUnknown.h"
#include "CATTPSConstrainingDOF.h"

class CATITPSList;
class CATITTRS;
class CATITPSDatum;
class CATITTRS_var;

extern "C" const IID IID_CATITPSReferenceFrame;

/**
 * Interface designed to manage reference frame associated to a TPS.
 * Reference frame is composed of three boxes.
 * <pre>
 * <r>                        Reference Frame
 * <r>                       / 
 * <r>                 _____|_____
 * <r>                /           \
 * <r>        ---------------------
 * <r>        |   |   |Box|Box|Box|
 * <r>        |   |   | 1 | 2 | 3 |
 * <r>        ---------------------
 * <pre>
 * Reference Frame must be defined with the SetFrame method that returns a 
 * semantic diagnostic on the input frame. When not in a definition context,
 * this diagnostic on frame is included in global diagnostic returned by 
 * CATITPSSemanticValidity::Check() method.
 */
class CATITPSReferenceFrame : public IUnknown
{
  public:

    /**
     * Set Frame of the TPS. Frame is defined by a string in each box.
     *   @param iFirstBox
     *   @param iSecondBox
     *   @param iThirdBox
     *     Texts in first, second and third boxes.
     *   @param oDiagnostic
     *     Message that contains semantic diagnostic on Reference Frame.
     *     It is composed of [0...n] NLS Keys separated by blank.
     *     <tt>oDiagnostic</tt> must be deleted after use.
     */
    virtual HRESULT SetFrame (const wchar_t * iFirstBox,
                              const wchar_t * iSecondBox,
                              const wchar_t * iThirdBox,
                              wchar_t ** oDiagnostic) = 0;

    /**
     * Retrieves Frame of the TPS.
     *   @param oFirstBox
     *   @param oSecondBox
     *   @param oThirdBox
     *     Texts in first, second and third boxes.
     */
    virtual HRESULT GetFrame (wchar_t ** oFirstBox,
                              wchar_t ** oSecondBox,
                              wchar_t ** oThirdBox) const = 0;

    /**
     * Retrieves all datums simple used in Reference Frame.
     *   @param oDatums
     *     All objects of the collection adhere to CATITPSDatumSimple.
     */
    virtual HRESULT GetAllDatumsSimple (CATITPSList ** oDatums) const = 0;

    /**
     * @nodoc
     *  Use instead for V4 tolerances GetReferenceFrame
     *                    of interface CATITPSAssociatedRefFrame, and then
     *                    use GetTTRS with interface CATITPS.
     */
//    virtual HRESULT GetRefTTRS (CATITTRS ** oRefFrameTTRS) const = 0;

    /**
     * Sets the AxisSystem TTRS.
     *  @param ispAxisSystemTTRS
     *   AxisSystem TTRS. If it is NULL, the AxisSystem TTRS in the model
     *   will be removed.
     *  returns S_OK when the TTRS has been correctly set, 
     *  returns E_FAIL otherwise.
     */
    virtual HRESULT SetAxisSystemTTRS(const CATITTRS_var& ispAxisSystemTTRS) = 0;

    /**
     * Gets the AxisSystem TTRS.
     *  @param ospAxisSystemTTRS
     *   AxisSystem TTRS
     *  returns S_OK when the TTRS has been correctly retrieved, 
     *  returns E_FAIL otherwise.
     */
    virtual HRESULT GetAxisSystemTTRS(CATITTRS_var &ospAxisSystemTTRS) = 0;

    /**
     * Sets the values of Degrees Of Freedom(DOF) [x,y,z,u,v,w].
     * Is only defined when "Axis System" attribute is valued.
     * Only for ASME 2009 (does not exist in ISO).
     *  @param inBox
     *   First, Second or the Third Box of the DRF on which
     *   the Degrees Of Freedom is to be set.
     *  @param iValue
     *   Legal values are:-
     *   CATTPSConstrainX,
     *   CATTPSConstrainY,
     *   CATTPSConstrainZ,
     *   CATTPSConstrainU,
     *   CATTPSConstrainV,
     *   CATTPSConstrainW
     *   E.G.:- To set [x,z] as the DOF:-
     *   iValue = CATTPSConstrainX|CATTPSConstrainZ;
     *  @return HRESULT
     *    S_OK : the Degrees Of Freedom has been correctly set.
     *    E_FAIL or E_NOIMPL : the Degrees Of Freedom cannot be set.
     */
    virtual HRESULT SetDegreesOfFreedom (const int inBox,
                                         const CATTPSConstrainingDOF iValue) = 0; 

    /**
     * Retrieves the values of Degrees Of Freedom(DOF) [x,y,z,u,v,w].
     * Is only defined when "Axis System" attribute is valued.
     * Only for ASME 2009 (does not exist in ISO).
     *  @param inBox
     *   First, Second or the Third Box of the DRF on which
     *   the Degrees Of Freedom is to be retrieved.
     *  @param oValue
     *   Legal values are:-
     *   CATTPSConstrainX,
     *   CATTPSConstrainY,
     *   CATTPSConstrainZ,
     *   CATTPSConstrainU,
     *   CATTPSConstrainV,
     *   CATTPSConstrainW
     *  @return HRESULT
     *    S_OK : the Degrees Of Freedom has been correctly retrieved.
     *    E_FAIL or E_NOIMPL : the Degrees Of Freedom cannot be retrieved.
     */
    virtual HRESULT GetDegreesOfFreedom (const int inBox,
                                         CATTPSConstrainingDOF &oValue) const = 0;

};
#endif

