BUILT_OBJECT_TYPE = SHARED LIBRARY

INCLUDED_MODULES  =  G60E1OM CATFitPathFinder CATFitSmooth CATFitSampled CATFitTrack CATFitDic CATFitActions CATFitDraftingIntegration

COMMON_LINK_WITH  = VE0BASE                         \ # Visualization
                    VE0MDL                          \ # Visualization
                    CATViz                          \ # VisualizationBase
                    CATObjectModelerBase            \ # ObjectModelerBase
                    Mathematics                     \ # Mathematics
                    CATMathStream                   \ # Mathematics
                    CATCGMGeoMath                   \ # Mathematics
                    JS0FM                           \ # System
                    JS0GROUP                        \ # System
                    DI0STATE                        \ # DialogEngine
                    DI0PANV2                        \ # Dialog
                    CATDlgStandard                  \
                    AD0XXBAS                        \ # ObjectModelerBase
                    AC0SPBAS                        \ # ObjectSpecsModeler
                    AS0STARTUP                      \
                    CP0SPEC                         \
                    CD0FRAME                        \ # ApplicationFrame
                    OM0EDPRO                        \
                    O20COLLI                        \
                    O203DMAP                        \
                    O203DMAPOperators               \
                    CATPrsRep                       \
                    DNBSimulationBaseLegacy         \
                    CATFittingInterfaces            \
                    SimulationItf                   \
                    CATSimulationCommand            \
                    CATDMUManip                     \
                    CK0FEAT                         \
                    CK0UNIT                         \
                    CATDMUBase                      \ # Navigator4DBase
                    CATNavigatorItf                 \ # NavigatorInterfaces
                    CATNavigator2Itf                \ # NavigatorInterfaces
                    InfItf                          \
                    CATSIMSequenceCommand           \
                    CATSIMSequenceModel             \
                    CATSimulationBase               \ # SimulationBase
                    CATDynClash                     \ # SimulationBase
                    CATSimVoxelServices             \ # SimulationBase
                    CATGraph                        \
                    SpaceAnalysisItf                \
                    CATGviUtilities                 \
                    CATProductStructureInterfaces   \
                    CATLiteralFeatures              \
                    CATKnowledgeModeler             \
                    CATSMTInterfaces                \
                    CATAdvancedMathematics          \
                    CATGeometricObjects             \
                    CATGeometricOperators           \
                    CATSIMPlayerCommand             \
                    XMLUtils                        \ # XMLParser
                    xmlxerces                       \ # XMLParser
                    CATV4Geometry                   \
                    CATCdbEntity                    \
                    SIMItf                          \
                    CATV4gUtilities                 \
                    KnowledgeItf                    \ # KnowledgeInterfaces
                    CATGraphicProperties            \ # CATGraphicProperties
                    CATFileMenu                     \ # CATFileMenuMethods
                    MecModItfCPP                    \ # MecModInterfaces
                    DNBInfrastructure               \ # DNBInfrastructure
                    DNBSimulationItf                \ # DNBSimulationInterfaces
                    DNBBuild                        \ # DNBBuild
                    DNBSystem                       \ # DNBSystem
                    Scene                           \ # ObjectSceneManagement
                    CATPrsWksPRDWorkshop            \ # ObjectSceneManagement
                    CATDMUWorkBench                 \ # ObjectSceneManagement
                    OSMInterfacesItf                \ # OSMInterfaces
                    CATArrangementItfCPP            \ # CATArrangementInterfaces
                    CATTopologicalObjects           \ # NewTopologicalOperators
                    DNBManufacturingLayoutItf       \ # DNBManufacturingLayoutItf
                    DraftingItfCPP					\ # CATDftGenRequestAdpt
                    Primitives                      \ # CATWirePolyline
                    YI00IMPL                        \ # CATBody
                    SystemUUID                      \ # System for CATIUnknownList
                    CATV4iServices					\ # CreateNewV4Model IR-046358V5R20

OS = COMMON

# DO NOT DELETE THIS LINE -- dnb_imake depends on it.

#
# The lines below are automatically generated.  Do not edit them!
#
# $Id: Framework.mk,v 1.28 2003/06/13 22:36:31 joed Exp $
#
# To specify additional compiler/linker options, you may define the following
# variables at the beginning of the Imakefile.
#
#   BUILD             = NO
#   DNB_LOCAL_CCFLAGS = -foo -Dbar
#   DNB_LOCAL_LDFLAGS = -baz
#   DNB_LOCAL_LIBPATH = -L/usr/local/lib
#   DNB_LOCAL_LIBS    = -lgnu
#
# These variables may appear at global scope or within an OS-specific section.
#
# To specify temporary compiler/linker options, you may define the following
# environment variables in the development shell.
#
#   export DNB_SHELL_CCFLAGS="-foo -Dbar"
#   export DNB_SHELL_LDFLAGS="-baz"
#
OS                  = COMMON            # Terminate the previous section.
CXX_EXCEPTION       =                   # Enable exception handling.

DNB_SYSTYPE         = $(MkmkOS_Buildtime)
DNB_MODULE_OBJDIR   = $(FWPATH)/$(MODNAME)/Objects/$(DNB_SYSTYPE)
DNB_MASTER_OBJDIR   = $(FWPATH)/$(MOD_Container)/Objects/$(DNB_SYSTYPE)
DNB_BUILD_SHARED    = 1

DNB_RW_CONFIG       = 12d                       # 12d = release, 15d = debug
DNB_RW_DEFS         = -DDNB_RWLIBS_CONFIG=1     #   1 = release,   0 = debug
DNB_RW_MODULES      = std$(DNB_RW_CONFIG) tls$(DNB_RW_CONFIG) \
    bla$(DNB_RW_CONFIG) mth$(DNB_RW_CONFIG) thr$(DNB_RW_CONFIG)

DNB_DEBUG_DEFS      = -DDNB_VERIFY=1 -UNDEBUG
DNB_RELEASE_DEFS    = -DDNB_VERIFY=0 -DNDEBUG
DNB_CONFIG_DEFS     = $(MKMK_DEBUG:+DNB_DEBUG_DEFS)
DNB_STD_CCFLAGS     = $(DNB_RW_DEFS) $(DNB_CONFIG_DEFS:-DNB_RELEASE_DEFS) \
    -DCAT_ENABLE_NATIVE_EXCEPTION


OS                  = Windows_NT
DNB_BUILD_DEFS      = -D__$(FWNAME)     # Mkmk defines -D__$(MODNAME).
DNB_EXPORT_DEFS     = $(DNB_BUILD_LOAD:+" ")
LOCAL_CCFLAGS       = $(DNB_STD_CCFLAGS) $(DNB_EXPORT_DEFS:-DNB_BUILD_DEFS)
LOCAL_POST_CCFLAGS  = -GR -DNOMINMAX -DRWMS42_INCLUDE="$(MkmkMSVCDIR)/include" \
    -D_CATNoWarningPromotion_ $(DNB_LOCAL_CCFLAGS) $(DNB_SHELL_CCFLAGS)
LINK_WITH           = $(COMMON_LINK_WITH) $(DNB_RW_MODULES)     # Note ordering!
LOCAL_LDFLAGS       = $(DNB_LOCAL_LDFLAGS) $(DNB_SHELL_LDFLAGS)
SYS_LIBPATH         = $(DNB_LOCAL_LIBPATH)
SYS_LIBS            = $(DNB_LOCAL_LIBS)


OS                  = IRIX
OPTIMIZATION_CPP    = -O0               # Disable optimizations.
CXX_TEMPLATE_INC    = -auto_include
CXX_TEMPLATE_PRELK  = -prelink
CXX_TEMPLATE_INST   = -ptused
LOCAL_CCFLAGS       = $(DNB_STD_CCFLAGS)
LOCAL_POST_CCFLAGS  = -LANG:ansi-for-init-scope=off $(DNB_LOCAL_CCFLAGS) \
    $(DNB_SHELL_CCFLAGS)
LINK_WITH           = $(COMMON_LINK_WITH) $(DNB_RW_MODULES)     # Note ordering!
LOCAL_LDFLAGS       = -Wl,-woff,84 $(DNB_LOCAL_LDFLAGS) $(DNB_SHELL_LDFLAGS)
SYS_LIBPATH         = $(DNB_LOCAL_LIBPATH)
SYS_LIBS            = -lm -lpthread $(DNB_LOCAL_LIBS)


OS                  = SunOS
OPTIMIZATION_CPP    = -DDNB_DUMMY_=1    # Disable optimizations.
LOCAL_CCFLAGS       = $(DNB_STD_CCFLAGS)
LOCAL_POST_CCFLAGS  = -instances=global -template=no%wholeclass,no%extdef \
    -features=%none,anachronisms,except,mutable,explicit,rtti \
    $(DNB_LOCAL_CCFLAGS) $(DNB_SHELL_CCFLAGS)
LINK_WITH           = $(DNB_RW_MODULES) $(COMMON_LINK_WITH)
LOCAL_LDFLAGS       = $(DNB_LOCAL_LDFLAGS) $(DNB_SHELL_LDFLAGS)
SYS_LIBPATH         = $(DNB_LOCAL_LIBPATH)
SYS_LIBS            = -lsocket -lnsl $(DNB_LOCAL_LIBS)


OS                  = AIX
OPTIMIZATION_CPP    = -qnooptimize      # Disable optimizations.
LOCAL_CCFLAGS       = -F:xlC_r $(DNB_STD_CCFLAGS)
LOCAL_POST_CCFLAGS  = -qeh -qrtti=all -qansialias -qmaxmem=-1 -qfuncsect \
    $(DNB_LOCAL_CCFLAGS) $(DNB_SHELL_CCFLAGS)
LINK_WITH           = $(DNB_RW_MODULES) $(COMMON_LINK_WITH)
LOCAL_LDFLAGS       = -bh:5 $(DNB_LOCAL_LDFLAGS) $(DNB_SHELL_LDFLAGS)
SYS_LIBPATH         = -L/usr/lib/threads -L/usr/vacpp/lib $(DNB_LOCAL_LIBPATH)
SYS_LIBS            = -lC -lpthreads -lm -lc $(DNB_LOCAL_LIBS)


OS                  = HP-UX
LOCAL_CCFLAGS       = $(DNB_STD_CCFLAGS)
LOCAL_POST_CCFLAGS  = -ext -Wc,-ansi_for_scope,off +inst_used \
    +W67,523,552,652,740,749,829,863 $(DNB_LOCAL_CCFLAGS) $(DNB_SHELL_CCFLAGS)
LINK_WITH           = $(DNB_RW_MODULES) $(COMMON_LINK_WITH)
LOCAL_LDFLAGS       = $(DNB_LOCAL_LDFLAGS) $(DNB_SHELL_LDFLAGS)
SYS_LIBPATH         = $(DNB_LOCAL_LIBPATH)
SYS_LIBS            = -lpthread $(DNB_LOCAL_LIBS)


OS                  = win_a
BUILD               = NO


#
# The lines above are automatically generated.  Do not edit them!
#
