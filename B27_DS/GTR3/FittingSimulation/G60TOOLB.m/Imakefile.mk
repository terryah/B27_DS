#ifdef LOCAL_DEFINITION_FOR_IID
LINK_WITH_FOR_IID = \
                    ProductStructureUIUUID
#else
LINK_WITH_FOR_IID =
#endif
BUILT_OBJECT_TYPE=SHARED LIBRARY

LINK_WITH=$(LINK_WITH_FOR_IID)  \
                                JS0FM                               \ # System
                                JS0GROUP                            \ # System
                                AD0XXBAS                            \ # ObjectModelerBase
                                CD0FRAME                            \ # ApplicationFrame
                                CATPrsWksPRDWorkshop                \
                                SELECT                              \
                                CATNavigatorItf                     \ # NavigatorInterfaces
                                CATNavigator2Itf                    \ # NavigatorInterfaces
                                CATDMUWorkBench                     \ # Navigator4DUI
                                CATCamController                    \ # CATCamera
                                CATProductStructureInterfaces       \ #ProductStructureInterfaces
                                CATFittingInterfaces                \ # FittingInterfaces


OS=COMMON
