#ifdef LOCAL_DEFINITION_FOR_IID
LINK_WITH_FOR_IID = \
CATIAApplicationFrameUUID 
#else
LINK_WITH_FOR_IID =
#endif
#BUILT_OBJECT_TYPE = NONE
BUILT_OBJECT_TYPE = SHARED LIBRARY

LINK_WITH=$(LINK_WITH_FOR_IID)  JS0CORBA                        \
                                JS0FM                           \
                                JS0STR                          \
                                NS0S3STR                        \
                                AD0XXBAS                        \
                                DI0PANV2                        \
                                OM0EDPRO                        \
                                CATDMUBase                      \ # Navigator4DBase
                                CATDMUManip                     \ # Navigator4DBase
                                CK0FEAT                         \
                                CK0UNIT                         \
                                CATDlgStandard                  \
                                KnowledgeItf                    \ # KnowledgeInterfaces
                                G60I0FIT                        \ # FittingInterfaces
                                CATFittingSimulation            \ # FittingSimulation - MUST BE REMOVED!!
