#
#   Imakefile.mk for CATFitV4Entities.m
#

#ifdef CATIAV5R8
#
BUILT_OBJECT_TYPE = SHARED LIBRARY
INCLUDED_MODULES =  CATFitV4Fortran
#
###########  AECInterfaces \

DUMMY_LINK_WITH   = JS0CORBA                        \ # System
                    CATObjectModelerBase            \ # ObjectModelerBase
                    AD0XXBAS                        \ # ObjectModelerBase
                    CATFittingSimulation            \ # FittingSimulation
                    CATFittingInterfaces            \ # FittingInterfaces
                    SimulationItf                   \ # SimulationInterfaces
                    AC0SPBAS                        \ # ObjectSpecsModeler
                    CD0WIN                          \ # ApplicationFrame
                    G60I0FIT                        \ # FittingInterfaces
                    SB0COMF                         \ # SimulationBase
                    DNBSimulationBaseLegacy         \ # DNBSimulationBaseLegacy
                    CATDMUBase                      \ # Navigator4DBase
                    CATVisualization                \ # Visualization
                    CATViz                          \ # VisualizationBase
                    CO0LSTPV                        \ # System
                    CATV4Maths                      \ # CATV4Maths
                    CATV4epsilon                    \ # CATV4Geometry
                    CATV4gUtilities                 \ # CATV4Geometry
                    CATV4System                     \ # CATV4System
                    CATV4Topology                   \ # CATV4Topology
                    CATObjectModelerCATIA           \ # ObjectModelerCATIA
                    CATCdbEntity                    \ # CATIADataBasics
                    CATIABaseIAO                    \ # CATIABaseIAO
                    AS0STARTUP                      \ # ProductStructure
                    AC0V4LINK                       \ # CATV4IntegrationBase
                    CATInteractiveInterfaces        \ # CATV4IntegrationBase
                    Mathematics                     \ # Mathematics
                    CATMathStream                   \ # Mathematics
                    SIMItf                          \ # SIMInterfaces

#
COMDYN_MODULE     = V4SysCOMD
#
OS = COMMON
LINK_WITH = $(DUMMY_LINK_WITH)
SYS_LIBPATH = 

OS = AIX
SYS_LIBS = -lxlf -lxlf90 -lxlfpad

OS = HP-UX
SYS_INCPATH =
#if os hpux_a
SYS_LIBS = -lf
#else
SYS_LIBS = -lF90
#endif

OS = hpux_b64 
SYS_INCPATH =
#if os hpux_a
SYS_LIBS = -lf
#else
SYS_LIBS = -lF90 -lcps 
#endif

OS = IRIX
LINK_WITH = $(COMDYN_MODULE) $(DUMMY_LINK_WITH)
SYS_LIBS = -lftn

OS = SunOS
SYS_INCPATH = 
SYS_LIBS = -lF77
SYS_LIBPATH =

OS = Windows_NT
#
#else
BUILT_OBJECT_TYPE = NONE

OS = COMMON
BUILD=NO
#endif
