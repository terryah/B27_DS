// COPYRIGHT Dassault Systemes 2002
//===================================================================
//
// CATIFunctObjects.h
// Define the CATIFunctObjects interface
//
//===================================================================
//
// Usage notes:
//   New interface: describe its use here
//
//===================================================================
//
//  Jan 2002  Creation: Code generated by the CAA wizard  phb
//===================================================================
#ifndef CATIFunctObjects_H
#define CATIFunctObjects_H

/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */

#include "FunctionalAnalysisItfCPP.h"
#include "CATBaseUnknown.h"
#include "CATLISTV_CATBaseUnknown.h"

class CATIFunctObject;
class CATIFunctObjectProxy;
class CATIFunctDescription;

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByFunctionalAnalysisItfCPP IID IID_CATIFunctObjects;
#else
extern "C" const IID IID_CATIFunctObjects ;
#endif

//------------------------------------------------------------------
/**
 * The interface to access the functional objects of a system.
 */
class ExportedByFunctionalAnalysisItfCPP CATIFunctObjects: public CATBaseUnknown
{
  CATDeclareInterface;

  public:

    /**
     * Get the list of objects. 
	 * <p>
     * @param ioLst
     *   The list to be filled with the objects.
	 */
    virtual HRESULT GetListOfObjects( CATLISTV(CATBaseUnknown_var)&	ioLst ) = 0 ;

    /**
     * Create an Object.
     */
    virtual HRESULT Create
            ( const CATUnicodeString&	iName
            , CATIFunctObject*&			oObject ) = 0 ;

    /**
     * Create an Object.
     */
    virtual HRESULT CreateProxy
            ( const CATUnicodeString&	iName
			, CATIFunctDescription*		iDesc
            , CATIFunctObjectProxy*&	oObject ) = 0 ;

    /**
     * Delete an Object.
     */
    virtual HRESULT Delete( CATBaseUnknown*	iObject ) = 0 ;

};

//------------------------------------------------------------------
CATDeclareHandler( CATIFunctObjects, CATBaseUnknown );

#endif
