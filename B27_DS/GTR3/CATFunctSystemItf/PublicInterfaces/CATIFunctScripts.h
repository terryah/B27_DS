// COPYRIGHT Dassault Systemes 2002
//===================================================================
//
// CATIFunctScripts.h
// Define the CATIFunctScripts interface
//
//===================================================================
//
// Usage notes:
//   New interface: describe its use here
//
//===================================================================
//
//  Jan 2002  Creation: Code generated by the CAA wizard  phb
//===================================================================
#ifndef CATIFunctScripts_H
#define CATIFunctScripts_H

/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */

#include "FunctionalAnalysisItfCPP.h"
#include "CATBaseUnknown.h"
#include "CATLISTV_CATBaseUnknown.h"

class CATIFunctScript;

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByFunctionalAnalysisItfCPP IID IID_CATIFunctScripts;
#else
extern "C" const IID IID_CATIFunctScripts ;
#endif

//------------------------------------------------------------------
/**
 * The interface to access a set of Functional Scripts.
 * <p>
 * It is managed on a Functional Element, thru the GenerativeKnowledge Facet Manager (GKW).
 */
class ExportedByFunctionalAnalysisItfCPP CATIFunctScripts: public CATBaseUnknown
{
  CATDeclareInterface;

  public:

    /**
     * Get the list of scripts. 
	 * <p>
     * @param ioLst
     *   The list to be filled with the scripts.
	 */
    virtual HRESULT GetListOfScripts( CATLISTV(CATBaseUnknown_var)&	ioLst ) = 0 ;

    /**
     * Create a Script.
     */
    virtual HRESULT Create
            ( const CATUnicodeString&	iName
            , CATIFunctScript*&			oScript ) = 0 ;

    /**
     * Delete a Script.
     */
    virtual HRESULT Delete( CATBaseUnknown*	iScript ) = 0 ;

};

//------------------------------------------------------------------
CATDeclareHandler( CATIFunctScripts, CATBaseUnknown );

#endif
