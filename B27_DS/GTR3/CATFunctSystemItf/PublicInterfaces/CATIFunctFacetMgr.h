// COPYRIGHT Dassault Systemes 2002
//===================================================================
//
// CATIFunctFacetMgr.h
// Define the CATIFunctFacetMgr interface
//
//===================================================================
//
// Usage notes:
//   New interface: describe its use here
//
//===================================================================
//
//  Jan 2002  Creation: Code generated by the CAA wizard  phb
//===================================================================
#ifndef CATIFunctFacetMgr_H
#define CATIFunctFacetMgr_H

/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */

#include "FunctionalAnalysisItfCPP.h"
#include "CATBaseUnknown.h"

class CATUnicodeString;

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByFunctionalAnalysisItfCPP IID IID_CATIFunctFacetMgr;
#else
extern "C" const IID IID_CATIFunctFacetMgr ;
#endif

//------------------------------------------------------------------
/**
 * The interface to access a Functional Facet Manager.
 */
class ExportedByFunctionalAnalysisItfCPP CATIFunctFacetMgr: public CATBaseUnknown
{
  CATDeclareInterface;

  public:

    /**
     * Get the Name. 
	 * <p>
     * Today, it might be UML, IMC, GKW, MRM.
	 */
    virtual HRESULT GetName (CATUnicodeString&  oName ) = 0 ;
};

//------------------------------------------------------------------
CATDeclareHandler( CATIFunctFacetMgr, CATBaseUnknown );

#endif
