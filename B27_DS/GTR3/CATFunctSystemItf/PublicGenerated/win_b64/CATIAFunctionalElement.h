/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAFunctionalElement_h
#define CATIAFunctionalElement_h

#ifndef ExportedByCATFunctSystemPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __CATFunctSystemPubIDL
#define ExportedByCATFunctSystemPubIDL __declspec(dllexport)
#else
#define ExportedByCATFunctSystemPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByCATFunctSystemPubIDL
#endif
#endif

#include "CATIABase.h"

class CATIAFunctionalDocument;
class CATIAParameters;

extern ExportedByCATFunctSystemPubIDL IID IID_CATIAFunctionalElement;

class ExportedByCATFunctSystemPubIDL CATIAFunctionalElement : public CATIABase
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_Document(CATIAFunctionalDocument *& oDocument)=0;

    virtual HRESULT __stdcall get_Parameters(CATIAParameters *& oParameters)=0;


};

CATDeclareHandler(CATIAFunctionalElement, CATIABase);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIACollection.h"
#include "CATIAParameters.h"
#include "CATVariant.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
