/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAFunctionalFacet_h
#define CATIAFunctionalFacet_h

#ifndef ExportedByCATFunctSystemPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __CATFunctSystemPubIDL
#define ExportedByCATFunctSystemPubIDL __declspec(dllexport)
#else
#define ExportedByCATFunctSystemPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByCATFunctSystemPubIDL
#endif
#endif

#include "CATIABase.h"

class CATIAFunctionalElement;

extern ExportedByCATFunctSystemPubIDL IID IID_CATIAFunctionalFacet;

class ExportedByCATFunctSystemPubIDL CATIAFunctionalFacet : public CATIABase
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall Init()=0;

    virtual HRESULT __stdcall Free()=0;

    virtual HRESULT __stdcall get_FunctionalElement(CATIAFunctionalElement *& oElem)=0;


};

CATDeclareHandler(CATIAFunctionalFacet, CATIABase);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
