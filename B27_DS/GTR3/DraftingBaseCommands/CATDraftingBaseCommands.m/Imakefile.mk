BUILT_OBJECT_TYPE = SHARED LIBRARY

LINK_WITH=  JS0GROUP \
            JS0FM \
            SystemUUID \
            CATDraftingInfrastructureUI \
            CATDraftingInfrastructure \
            CATVisualization \
            CATMathematics \
            DI0PANV2 \
            CD0FRAME \
            CATDialogEngine \
            CATSktAssistant \
            CATDraftingBaseInfra \
            DraftingBaseCommandsUUID \
            SELECT \
            DraftingItfCPP \
            CATAnnotationModeler \
            CATSketcherInterfaces \
            CATMecModInterfaces \
            CATDraftingAnnotationModeler \
            CATAnnotationModelerUI \
            CATSkuCommands \
            CATObjectSpecsModeler \
            CATDraftingUI \
            CATObjectModelerBase \
            PRDCommands \
            CATProductStructure1 \
            CATFileMenu \
            CATDraftingBaseUI \
            IDCTOOLBARS\
            PartItf \
            CATGitInterfaces \
            CATGeometricObjects \
            CATMeasureGeometryInterfaces \
            CATMechanicalModeler \
            YP00IMPL\
            CATMmrBrepModel\
            Infra2DItfCPP\
            CATSktToolbox \
            CATSkuBase \
            CATLifDictionary \
            CATSketcherToolsUI \
            CATCclInterfaces \
            CATDraftingSktBaseCommands \
            CATSktSettings \
			CATAdvancedMathematics \
			CATDraftingFeature \
            CATSurfacicInterfaces \
            CATDlgStandard \
            CATGMModelInterfaces \


INCLUDED_MODULES = CATDbcLineUp \
                   CATDbcManipulators \
                   CATDbcMemDialog \
                   CATDbcPrintArea \
                   CATDbcUtilities \
                   CATDbcGeometry \
                   CATDbcGeomModification \
                   CATDbcSettingsToolbox \
                   CATDbcAdapters \
                   CATDbcView \
                   CATDbcTable \
                   CATDbcReorder \
                   DraftingBaseCommandsItfCPP \
                   CATDbcGeneralCmd \
                   CATDbcImportFromFile \
                   CATDbcCallout \
                   CATDbcText \
                   CATDbcDimensionEdition \
                   CATDbcLeader \
                   CATDbcDisplayView \
                   CATDbcAlignView \
                   CATDbcDetailDitto \
                   CATDbcReps \
                   CATDbcSheet \
                   CATDbcDimension \
                   CATDbcArrow \
                   CATDbcBalloon\
                   CATDbcGDT\
                   CATDbcSktIntegration\
                   CATDbcClipping\
                   CATDbc3DClipping\
                   CATDbcArea \
