/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAMaterialFamily_h
#define CATIAMaterialFamily_h

#ifndef ExportedByCATMatPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __CATMatPubIDL
#define ExportedByCATMatPubIDL __declspec(dllexport)
#else
#define ExportedByCATMatPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByCATMatPubIDL
#endif
#endif

#include "CATIABase.h"

class CATIAMaterials;

extern ExportedByCATMatPubIDL IID IID_CATIAMaterialFamily;

class ExportedByCATMatPubIDL CATIAMaterialFamily : public CATIABase
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_Materials(CATIAMaterials *& cMaterials)=0;


};

CATDeclareHandler(CATIAMaterialFamily, CATIABase);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIAAnalysisMaterial.h"
#include "CATIACollection.h"
#include "CATIAMaterial.h"
#include "CATIAMaterials.h"
#include "CATIARenderingMaterial.h"
#include "CATSafeArray.h"
#include "CATVariant.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
