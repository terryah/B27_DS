#ifndef CATSchGeneralEnum2_H
#define CATSchGeneralEnum2_H

//	COPYRIGHT DASSAULT SYSTEMES 2003

/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */

/**
 * Flag for whether or not the route shape is fixed.
 * <b>Role</b>: Indicate whether or not the route shape is fixed for the
 * purpose of reshaping the route.
 */
enum CATSchGRRRouteReshapeMode
{
   SchFixedShapeOff   = 0,
   SchFixedShapeOn    = 1
};

#endif
