#ifndef CATScuFlowAllMode_H
#define CATScuFlowAllMode_H
// COPYRIGHT DASSAULT SYSTEMES 2004

/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */

/**
 * The schematics CatScuFlowAllModes Mode.
 * @param ScuFlowAllMode_ShowAll
 *   Show Flow Type
 * @param ScuFlowAllMode_NoShowAll
 *   Noshow Flow type
 */

enum CATScuFlowAllMode
{
    ScuFlowAllMode_ShowAll   = 1,
	ScuFlowAllMode_NoShowAll = 2
};
#endif

