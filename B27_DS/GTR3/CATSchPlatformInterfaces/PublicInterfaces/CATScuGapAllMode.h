#ifndef CATScuGapAllMode_H
#define CATScuGapAllMode_H
// COPYRIGHT DASSAULT SYSTEMES 2004

/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */

/**
 * The schematics CATScuGapAllMode Mode.
 * @param ScuGapAllMode_Show
 *   Show Gap type
 * @paramScuGapAllMode_Noshow
 *   Noshow Gap type
 */

enum CATScuGapAllMode
{
    ScuGapAllMode_Show   = 1,
	ScuGapAllMode_Noshow = 2
};
#endif

