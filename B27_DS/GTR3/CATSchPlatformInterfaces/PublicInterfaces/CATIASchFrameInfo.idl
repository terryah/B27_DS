#ifndef CATIASchFrameInfo_IDL 
#define CATIASchFrameInfo_IDL 
    
/*IDLREP*/

/**
* @CAA2Level L1
* @CAA2Usage U3
*/

//	COPYRIGHT DASSAULT SYSTEMES 2004

#include "CATBSTR.idl"
    
#include "CATIABase.idl"

 /** 
  * Manage the background view of a schematic viewer.
  */

interface CATIASchFrameInfo : CATIABase
{
  /**
  * Get the frame origin corner code.  
  * @param oOriginCorner
  *   Origin corner code.
  *    
  * @sample
  * <pre>
  * Dim objThisIntf As SchFrameInfo
  * Dim strVar1 As String
  *  ...
  * objThisIntf.<font color="red">GetOriginCornerCode</font>strVar1
  * </pre>
  *    
  */
  HRESULT GetOriginCornerCode ( 
    inout CATBSTR oOriginCorner );
    
  /**
  * Set the frame origin corner code.  
  * @param iOriginCorner
  *   Origin corner code.
  *    
  * @sample
  * <pre>
  * Dim objThisIntf As SchFrameInfo
  * Dim strVar1 As String
  *  ...
  * objThisIntf.<font color="red">SetOriginCornerCode</font>strVar1
  * </pre>
  *    
  */
  HRESULT SetOriginCornerCode ( 
    in CATBSTR iOriginCorner );
    
  /**
  * Get the frame spacing code.  
  * @param oSpacing
  *   Spacing code.
  * @param iBHoriz
  *   If TRUE, then the spacing is for horizontal, else, the spacing is for 
  *   vertical.
  *    
  * @sample
  * <pre>
  * Dim objThisIntf As SchFrameInfo
  * Dim strVar1 As String
  * Dim bVar2 As boolean
  *  ...
  * objThisIntf.<font color="red">GetSpacingCode</font>strVar1,bVar2
  * </pre>
  *    
  */
  HRESULT GetSpacingCode ( 
    inout CATBSTR oSpacing, 
    in boolean iBHoriz );
    
  /**
  * Set the frame spacing code.  
  * @param iSpacing
  *   Spacing code.
  * @param iBHoriz
  *   If TRUE, then the spacing is for horizontal, else, the spacing is for 
  *   vertical.
  *    
  * @sample
  * <pre>
  * Dim objThisIntf As SchFrameInfo
  * Dim strVar1 As String
  * Dim bVar2 As boolean
  *  ...
  * objThisIntf.<font color="red">SetSpacingCode</font>strVar1,bVar2
  * </pre>
  *    
  */
  HRESULT SetSpacingCode ( 
    in CATBSTR iSpacing, 
    in boolean iBHoriz );
    
  /**
  * Get the frame label code.  
  * @param oLabel
  *   Label code.
  * @param iBHoriz
  *   If TRUE, then the labels are for horizontal spacing, else, they are for 
  *   vertical spacing.
  *    
  * @sample
  * <pre>
  * Dim objThisIntf As SchFrameInfo
  * Dim strVar1 As String
  * Dim bVar2 As boolean
  *  ...
  * objThisIntf.<font color="red">GetLabelCode</font>strVar1,bVar2
  * </pre>
  *    
  */
  HRESULT GetLabelCode ( 
    inout CATBSTR oLabel, 
    in boolean iBHoriz );
    
  /**
  * Set the frame label code.  
  * @param iLabel
  *   Label code.
  * @param iBHoriz
  *   If TRUE, then the labels are for horizontal spacing, else, they are for 
  *   vertical spacing.
  *    
  * @sample
  * <pre>
  * Dim objThisIntf As SchFrameInfo
  * Dim strVar1 As String
  * Dim bVar2 As boolean
  *  ...
  * objThisIntf.<font color="red">SetLabelCode</font>strVar1,bVar2
  * </pre>
  *    
  */
  HRESULT SetLabelCode ( 
    in CATBSTR iLabel, 
    in boolean iBHoriz );
    
};
    
// Interface name : CATIASchFrameInfo
#pragma ID CATIASchFrameInfo "DCE:72b92323-c26c-4156-a654fba4552041f5"
#pragma DUAL CATIASchFrameInfo
    
// VB object name : SchFrameInfo
#pragma ID SchFrameInfo "DCE:707b668f-51ed-49eb-abb42e6681898d83"
#pragma ALIAS CATIASchFrameInfo SchFrameInfo
    
#endif 
    
