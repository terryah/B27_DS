#ifndef CATScuRotateMode_H
#define CATScuRotateMode_H
// COPYRIGHT DASSAULT SYSTEMES 2004

/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */

/**
 * The schematics Rotate Mode.
 * @param ScuRotateMode_RotateRight
 *  Rotate right type
 * @param ScuRotateMode_RotateLeft
 *  Rotate left type
 */

enum CATScuRotateMode
{
  ScuRotateMode_RotateRight = 1,
  ScuRotateMode_RotateLeft  = 2
};
#endif

