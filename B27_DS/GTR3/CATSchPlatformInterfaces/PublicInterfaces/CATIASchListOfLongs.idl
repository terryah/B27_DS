#ifndef CATIASchListOfLongs_IDL
#define CATIASchListOfLongs_IDL

/*IDLREP*/

/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */

//	COPYRIGHT DASSAULT SYSTEMES 2004       


#include "CATIABase.idl"

 /** 
  * Represent a non-persistant list of LONGs to be used with schematic IDLs.
  */

interface CATIASchListOfLongs : CATIABase
{
    /**
     * Returns the number of long integers in the list.
     * <! @sample >
     * <dl>
     * <dt><b>Example:</b>
     * <dd>
     * This example retrieves in  <tt>NumberOfLongs</tt> the number of long integers
     * currently gathered in <tt>MyList</tt>.
     * <pre>
     * NumberOfLongs = MyList.<font color="red">Count</font>
     * </pre>
     * </dl>
     */
#pragma PROPERTY Count
  HRESULT get_Count (out /*IDLRETVAL*/ long oNbItems);

    /**
     * Returns a long integer from its index in the list.
     * @param iIndex
     *   The index of the first long integer in the collection is 1, and
     *   the index of the last long integer is Count.
     * @return the retrieved long integer.
     *
     * @sample
     * The following example returns in the third long integer in the list.
     * <pre>
     * Dim MyLong As Integer
	 * Dim MyList As SchListOfLongs
     * Set MyLong = SchListOfLongs.<font color="red">Item</font>(2)
     * </pre>
     */
  HRESULT Item (in long iIndex,  out /*IDLRETVAL*/ long oLong);

    /**
     * Adds an long integer to the end of the list.
     * @param iLong
     *   The long integer to be added to the list.
     *
     * @sample
     * The following example appends a long integer to the list.
     * <pre>
     * Dim MInteger As Long
	 * MInteger = 4
	 * Dim MyList As SchListOfLongs
     * MyList.<font color="red">Append</font>(MInteger)
     * </pre>
     */
  HRESULT Append (in long iLong);

    /**
     * Remove a long integer from the list by specifying its position in the list.
     * @param iIndex
     *   The position of the long integer to be removed in the list.
     *
     * @sample
     * The following example removes the second entry in the list. Please note that the
	 * list index starts with 1.
     * <pre>
	 * Dim MyList As SchListOfLongs
     * MyList.<font color="red">Remove</font>(1)
     * </pre>
     */

  HRESULT RemoveByIndex (in long iIndex);

};

// Interface name : CATIASchListOfLongs
#pragma ID CATIASchListOfLongs "DCE:39287c36-2922-4491-a9e0cacdec452ae7"
#pragma DUAL CATIASchListOfLongs

// VB object name : SchListOfLongs
#pragma ID SchListOfLongs "DCE:71b95509-16d9-486d-8fb0fdf0e9183d3a"
#pragma ALIAS CATIASchListOfLongs SchListOfLongs 

#endif
