#ifndef CATScuAlignMode_H
#define CATScuAlignMode_H
// COPYRIGHT DASSAULT SYSTEMES 2004

/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */

/**
 * The schematics CATScuAlignMode.
 * @param ScuAlignMode_Horizontal
 *  Align Horizontally type
 * @param ScuAlignMode_Vertical
 *  Align Vertically type
 */

enum CATScuAlignMode
{
    ScuAlignMode_Horizontal = 1,
	ScuAlignMode_Vertical = 2
};
#endif

