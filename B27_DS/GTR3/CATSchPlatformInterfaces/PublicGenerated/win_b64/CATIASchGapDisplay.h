/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIASchGapDisplay_h
#define CATIASchGapDisplay_h

#include "CATCORBABoolean.h"
#include "CAT_VARIANT_BOOL.h"

#ifndef ExportedByCATSchematicPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __CATSchematicPubIDL
#define ExportedByCATSchematicPubIDL __declspec(dllexport)
#else
#define ExportedByCATSchematicPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByCATSchematicPubIDL
#endif
#endif

#include "CATIABase.h"

class CATIASchListOfObjects;

extern ExportedByCATSchematicPubIDL IID IID_CATIASchGapDisplay;

class ExportedByCATSchematicPubIDL CATIASchGapDisplay : public CATIABase
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall SetGap(CATIASchListOfObjects * iLUKRoutes)=0;

    virtual HRESULT __stdcall UnsetGap(CATIASchListOfObjects * iLUKRoutes)=0;

    virtual HRESULT __stdcall IsGapShown(CAT_VARIANT_BOOL & oBYes)=0;


};

CATDeclareHandler(CATIASchGapDisplay, CATIABase);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
