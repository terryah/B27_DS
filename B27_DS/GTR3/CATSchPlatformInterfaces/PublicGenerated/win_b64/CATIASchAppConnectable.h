/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIASchAppConnectable_h
#define CATIASchAppConnectable_h

#ifndef ExportedByCATSchematicPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __CATSchematicPubIDL
#define ExportedByCATSchematicPubIDL __declspec(dllexport)
#else
#define ExportedByCATSchematicPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByCATSchematicPubIDL
#endif
#endif

#include "CATBSTR.h"
#include "CATIABase.h"

class CATIASchAppConnector;
class CATIASchListOfBSTRs;
class CATIASchListOfObjects;

extern ExportedByCATSchematicPubIDL IID IID_CATIASchAppConnectable;

class ExportedByCATSchematicPubIDL CATIASchAppConnectable : public CATIABase
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall AppListConnectors(CATIASchListOfBSTRs * iLCntrClassFilter, CATIASchListOfObjects *& oLCntrs)=0;

    virtual HRESULT __stdcall AppListConnectables(CATIASchListOfBSTRs * iLCntbleClassFilter, CATIASchListOfObjects *& oLCntbles, CATIASchListOfObjects *& oLCntrsOnThisObj, CATIASchListOfObjects *& oLCntrsOnConnected)=0;

    virtual HRESULT __stdcall AppAddConnector(const CATBSTR & iClassType, CATIASchAppConnector *& oNewAppCntr)=0;

    virtual HRESULT __stdcall AppRemoveConnector(CATIASchAppConnector * iCntrToRemove)=0;

    virtual HRESULT __stdcall AppListValidCntrTypes(CATIASchListOfBSTRs *& oLValidCntrTypes)=0;

    virtual HRESULT __stdcall AppGetReferenceName(CATBSTR & oReferenceName)=0;

    virtual HRESULT __stdcall AppSetReferenceName(const CATBSTR & iReferenceName)=0;


};

CATDeclareHandler(CATIASchAppConnectable, CATIABase);

#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
