/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIASchematicExtension_h
#define CATIASchematicExtension_h

#ifndef ExportedByCATSchematicPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __CATSchematicPubIDL
#define ExportedByCATSchematicPubIDL __declspec(dllexport)
#else
#define ExportedByCATSchematicPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByCATSchematicPubIDL
#endif
#endif

#include "CATIABase.h"
#include "CatSchIDLExtensionType.h"

class CATIASchListOfObjects;

extern ExportedByCATSchematicPubIDL IID IID_CATIASchematicExtension;

class ExportedByCATSchematicPubIDL CATIASchematicExtension : public CATIABase
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall AddExtension(CATIABase * iAppObjToBeExtended, CatSchIDLExtensionType iExtensionType, CATIASchListOfObjects * iLGRR)=0;

    virtual HRESULT __stdcall RemoveExtension(CATIABase * iAppExtendedObj, CatSchIDLExtensionType iExtensionType)=0;


};

CATDeclareHandler(CATIASchematicExtension, CATIABase);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
