/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIASchGRR_h
#define CATIASchGRR_h

#ifndef ExportedByCATSchematicPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __CATSchematicPubIDL
#define ExportedByCATSchematicPubIDL __declspec(dllexport)
#else
#define ExportedByCATSchematicPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByCATSchematicPubIDL
#endif
#endif

#include "CATBSTR.h"
#include "CATIABase.h"

class CATIASchAppConnectable;
class CATIASchAppConnector;
class CATIASchListOfBSTRs;
class CATIASchListOfObjects;

extern ExportedByCATSchematicPubIDL IID IID_CATIASchGRR;

class ExportedByCATSchematicPubIDL CATIASchGRR : public CATIABase
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall GetSchObjOwner(CATIASchAppConnectable *& oGRROwner)=0;

    virtual HRESULT __stdcall GetGRRName(CATBSTR & oGRRName)=0;

    virtual HRESULT __stdcall SetGRRName(const CATBSTR & iGRRName)=0;

    virtual HRESULT __stdcall GetSchCntrOwners(CATIASchAppConnector *& oCntrOwner, CATIASchGRR *& oGRROwner)=0;

    virtual HRESULT __stdcall ListConnectedGRRs(CATIASchAppConnectable * iGRROwner, CATIASchListOfBSTRs * iLCntbleClassFilter, CATIASchListOfObjects *& oLCntbleGRRs, CATIASchListOfObjects *& oLCntbles, CATIASchListOfObjects *& oLCntrsOnThisObj, CATIASchListOfObjects *& oLCntrsOnConnected)=0;


};

CATDeclareHandler(CATIASchGRR, CATIABase);

#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
