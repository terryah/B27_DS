/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CatSchIDLGRRRouteReshapeMode_h
#define CatSchIDLGRRRouteReshapeMode_h

enum CatSchIDLGRRRouteReshapeMode {
        catSchIDLFixedShapeOff,
        catSchIDLFixedShapeOn
};

#endif
