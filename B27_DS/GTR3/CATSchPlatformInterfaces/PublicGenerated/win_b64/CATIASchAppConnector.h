/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIASchAppConnector_h
#define CATIASchAppConnector_h

#include "CATCORBABoolean.h"
#include "CAT_VARIANT_BOOL.h"

#ifndef ExportedByCATSchematicPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __CATSchematicPubIDL
#define ExportedByCATSchematicPubIDL __declspec(dllexport)
#else
#define ExportedByCATSchematicPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByCATSchematicPubIDL
#endif
#endif

#include "CATIABase.h"

class CATIASchAppConnectable;
class CATIASchAppConnection;
class CATIASchListOfBSTRs;
class CATIASchListOfObjects;

extern ExportedByCATSchematicPubIDL IID IID_CATIASchAppConnector;

class ExportedByCATSchematicPubIDL CATIASchAppConnector : public CATIABase
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall AppGetAssociatedConnectable(CATIASchAppConnectable *& oConnectable)=0;

    virtual HRESULT __stdcall AppListCompatibleTypes(CATIASchListOfBSTRs *& oLCntrCompatClassTypes)=0;

    virtual HRESULT __stdcall AppListConnections(CATIASchListOfBSTRs * iLCntnClassFilter, CATIASchListOfObjects *& oLConnections)=0;

    virtual HRESULT __stdcall AppConnect(CATIASchAppConnector * iCntrToConnect, CATIASchAppConnection *& oConnection)=0;

    virtual HRESULT __stdcall AppConnectBranch(CATIASchAppConnector * iCntrToConnect, CATIASchAppConnection *& oConnection)=0;

    virtual HRESULT __stdcall AppDisconnect(CATIASchAppConnector * iCntrToDisConnect)=0;

    virtual HRESULT __stdcall AppOKToNoShowConnectedCntr(CAT_VARIANT_BOOL & oBYes)=0;

    virtual HRESULT __stdcall AppIsCntrConnected(CAT_VARIANT_BOOL & oBYes)=0;


};

CATDeclareHandler(CATIASchAppConnector, CATIABase);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
