/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIASchComponentGroup_h
#define CATIASchComponentGroup_h

#ifndef ExportedByCATSchematicPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __CATSchematicPubIDL
#define ExportedByCATSchematicPubIDL __declspec(dllexport)
#else
#define ExportedByCATSchematicPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByCATSchematicPubIDL
#endif
#endif

#include "CATIABase.h"

class CATIASchAppConnectable;
class CATIASchListOfObjects;

extern ExportedByCATSchematicPubIDL IID IID_CATIASchComponentGroup;

class ExportedByCATSchematicPubIDL CATIASchComponentGroup : public CATIABase
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall AddMember(CATIASchAppConnectable * iCntblToAdd)=0;

    virtual HRESULT __stdcall RemoveMember(CATIASchAppConnectable * iCntblToRemove)=0;

    virtual HRESULT __stdcall ListMembers(CATIASchListOfObjects *& oLCntbl)=0;


};

CATDeclareHandler(CATIASchComponentGroup, CATIABase);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
