/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIASchDropOffView_h
#define CATIASchDropOffView_h

#ifndef ExportedByCATSchematicPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __CATSchematicPubIDL
#define ExportedByCATSchematicPubIDL __declspec(dllexport)
#else
#define ExportedByCATSchematicPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByCATSchematicPubIDL
#endif
#endif

#include "CATIABase.h"
#include "CATSafeArray.h"

class CATIADrawingView;
class CATIASchListOfObjects;

extern ExportedByCATSchematicPubIDL IID IID_CATIASchDropOffView;

class ExportedByCATSchematicPubIDL CATIASchDropOffView : public CATIABase
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall AddDropOffView(CATIADrawingView * iView, CATIADrawingView *& oView, const CATSafeArrayVariant & iDb2PosXY, double iDb1Scale, double iDb1Angl)=0;

    virtual HRESULT __stdcall RemoveDropOffView(CATIADrawingView * iViewToRemove)=0;

    virtual HRESULT __stdcall ListDropOffViews(CATIASchListOfObjects *& oLDropOffViews)=0;


};

CATDeclareHandler(CATIASchDropOffView, CATIABase);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
