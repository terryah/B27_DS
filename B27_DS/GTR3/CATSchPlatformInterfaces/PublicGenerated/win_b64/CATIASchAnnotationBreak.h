/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIASchAnnotationBreak_h
#define CATIASchAnnotationBreak_h

#ifndef ExportedByCATSchematicPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __CATSchematicPubIDL
#define ExportedByCATSchematicPubIDL __declspec(dllexport)
#else
#define ExportedByCATSchematicPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByCATSchematicPubIDL
#endif
#endif

#include "CATIABase.h"

extern ExportedByCATSchematicPubIDL IID IID_CATIASchAnnotationBreak;

class ExportedByCATSchematicPubIDL CATIASchAnnotationBreak : public CATIABase
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall Scale(double iDbScaleFactor)=0;

    virtual HRESULT __stdcall FlipOverLine()=0;

    virtual HRESULT __stdcall FlipOverOrthogonalLine()=0;


};

CATDeclareHandler(CATIASchAnnotationBreak, CATIABase);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
