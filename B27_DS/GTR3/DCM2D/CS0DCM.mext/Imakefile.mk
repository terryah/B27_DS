# COPYRIGHT DASSAULT SYSTEMES 2011
#
BUILT_OBJECT_TYPE = ARCHIVE
#

#if os aix_a64
#if (defined MK_IBMVAVER) && (MK_IBMVAVER >= 11)
LOCAL_SRCPATH=aix_a64/IBMVAVER11
#else
LOCAL_SRCPATH=aix_a64/IBMVAVER10
#endif
#endif

#if os win_b64
#if (defined MK_MSCVER) && (MK_MSCVER >= 1700)
LOCAL_SRCPATH=win_b64/vc11.0
#else
LOCAL_SRCPATH=win_b64/vc9.0
#endif
#endif

#if os intel_a
#if (defined MK_MSCVER) && (MK_MSCVER >= 1700)
LOCAL_SRCPATH=intel_a/vc11.0
#else
LOCAL_SRCPATH=intel_a/vc9.0
#endif
#endif

DELIVERABLE = NO
#
