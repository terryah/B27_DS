/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef DNBIA3DState_h
#define DNBIA3DState_h

#ifndef ExportedByDNBStatePubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __DNBStatePubIDL
#define ExportedByDNBStatePubIDL __declspec(dllexport)
#else
#define ExportedByDNBStatePubIDL __declspec(dllimport)
#endif
#else
#define ExportedByDNBStatePubIDL
#endif
#endif

#include "CATBSTR.h"
#include "CATIABase.h"
#include "CATSafeArray.h"

class CATBaseDispatch;
class CATIAActivity;
class DNBIA3DStatePosition;

extern ExportedByDNBStatePubIDL IID IID_DNBIA3DState;

class ExportedByDNBStatePubIDL DNBIA3DState : public CATIABase
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_StateName(CATBSTR & oStateName)=0;

    virtual HRESULT __stdcall put_StateName(const CATBSTR & iStateName)=0;

    virtual HRESULT __stdcall Add3DState(DNBIA3DState * iState)=0;

    virtual HRESULT __stdcall Add3DPosition(DNBIA3DStatePosition * iPosition)=0;

    virtual HRESULT __stdcall Remove3DState(DNBIA3DState * iState)=0;

    virtual HRESULT __stdcall Remove3DPosition(DNBIA3DStatePosition * iPosition)=0;

    virtual HRESULT __stdcall Assign3DState(CATIAActivity * iProcess, const CATBSTR & iCondition)=0;

    virtual HRESULT __stdcall Unassign3DState(CATIAActivity * iProcess)=0;

    virtual HRESULT __stdcall GetChildren(const CATBSTR & iType, CATSafeArrayVariant & oChildrenArray)=0;

    virtual HRESULT __stdcall GetNumberOfChildren(const CATBSTR & iType, CATLONG & oNumberOfChild)=0;

    virtual HRESULT __stdcall GetRelatedObject(CATBaseDispatch *& oRelatedObj)=0;

    virtual HRESULT __stdcall GetRelatedActivities(const CATBSTR & iConditionType, CATSafeArrayVariant & oActivitiesArray)=0;

    virtual HRESULT __stdcall GetNumberofRelatedActivities(const CATBSTR & iConditionType, CATLONG & oNumActivities)=0;

    virtual HRESULT __stdcall GetConditionName(CATIAActivity * iRelatedActivity, CATBSTR & oConditonName)=0;

    virtual HRESULT __stdcall GetParent(CATBaseDispatch *& oParent)=0;


};

CATDeclareHandler(DNBIA3DState, CATIABase);

#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIAAnalyze.h"
#include "CATIACollection.h"
#include "CATIAMove.h"
#include "CATIAParameters.h"
#include "CATIAPosition.h"
#include "CATIAProduct.h"
#include "CATIAPublications.h"
#include "CATVariant.h"
#include "CatFileType.h"
#include "CatProductSource.h"
#include "CatRepType.h"
#include "CatWorkModeType.h"
#include "DNBIA3DStatePosition.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
