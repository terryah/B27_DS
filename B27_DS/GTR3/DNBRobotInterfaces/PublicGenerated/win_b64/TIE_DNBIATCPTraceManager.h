#ifndef __TIE_DNBIATCPTraceManager
#define __TIE_DNBIATCPTraceManager

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIATCPTraceManager.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIATCPTraceManager */
#define declare_TIE_DNBIATCPTraceManager(classe) \
 \
 \
class TIEDNBIATCPTraceManager##classe : public DNBIATCPTraceManager \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIATCPTraceManager, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall get_TCPTraceMode(CAT_VARIANT_BOOL & oTCPTraceMode); \
      virtual HRESULT __stdcall put_TCPTraceMode(CAT_VARIANT_BOOL iTCPTraceMode); \
      virtual HRESULT __stdcall get_InitStatus(CAT_VARIANT_BOOL & oInitStatus); \
      virtual HRESULT __stdcall put_InitStatus(CAT_VARIANT_BOOL iInitStatus); \
      virtual HRESULT __stdcall GetNbPath(CATLONG & oNb); \
      virtual HRESULT __stdcall GetPath(CATLONG iIndex, DNBIATCPTrace *& oTCPPath); \
      virtual HRESULT __stdcall RemovePathByObject(DNBIATCPTrace * TCPPath); \
      virtual HRESULT __stdcall RemovePathByIndex(CATLONG iIndex); \
      virtual HRESULT __stdcall RemoveAllPath(); \
      virtual HRESULT __stdcall GetAttachedOwner(CATIABase *& oOwner); \
      virtual HRESULT __stdcall SetAttachedOwner(CATIABase * iOwner); \
      virtual HRESULT __stdcall ResetAttachedOwner(); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIATCPTraceManager(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall get_TCPTraceMode(CAT_VARIANT_BOOL & oTCPTraceMode); \
virtual HRESULT __stdcall put_TCPTraceMode(CAT_VARIANT_BOOL iTCPTraceMode); \
virtual HRESULT __stdcall get_InitStatus(CAT_VARIANT_BOOL & oInitStatus); \
virtual HRESULT __stdcall put_InitStatus(CAT_VARIANT_BOOL iInitStatus); \
virtual HRESULT __stdcall GetNbPath(CATLONG & oNb); \
virtual HRESULT __stdcall GetPath(CATLONG iIndex, DNBIATCPTrace *& oTCPPath); \
virtual HRESULT __stdcall RemovePathByObject(DNBIATCPTrace * TCPPath); \
virtual HRESULT __stdcall RemovePathByIndex(CATLONG iIndex); \
virtual HRESULT __stdcall RemoveAllPath(); \
virtual HRESULT __stdcall GetAttachedOwner(CATIABase *& oOwner); \
virtual HRESULT __stdcall SetAttachedOwner(CATIABase * iOwner); \
virtual HRESULT __stdcall ResetAttachedOwner(); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIATCPTraceManager(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::get_TCPTraceMode(CAT_VARIANT_BOOL & oTCPTraceMode) \
{ \
return (ENVTIECALL(DNBIATCPTraceManager,ENVTIETypeLetter,ENVTIELetter)get_TCPTraceMode(oTCPTraceMode)); \
} \
HRESULT __stdcall  ENVTIEName::put_TCPTraceMode(CAT_VARIANT_BOOL iTCPTraceMode) \
{ \
return (ENVTIECALL(DNBIATCPTraceManager,ENVTIETypeLetter,ENVTIELetter)put_TCPTraceMode(iTCPTraceMode)); \
} \
HRESULT __stdcall  ENVTIEName::get_InitStatus(CAT_VARIANT_BOOL & oInitStatus) \
{ \
return (ENVTIECALL(DNBIATCPTraceManager,ENVTIETypeLetter,ENVTIELetter)get_InitStatus(oInitStatus)); \
} \
HRESULT __stdcall  ENVTIEName::put_InitStatus(CAT_VARIANT_BOOL iInitStatus) \
{ \
return (ENVTIECALL(DNBIATCPTraceManager,ENVTIETypeLetter,ENVTIELetter)put_InitStatus(iInitStatus)); \
} \
HRESULT __stdcall  ENVTIEName::GetNbPath(CATLONG & oNb) \
{ \
return (ENVTIECALL(DNBIATCPTraceManager,ENVTIETypeLetter,ENVTIELetter)GetNbPath(oNb)); \
} \
HRESULT __stdcall  ENVTIEName::GetPath(CATLONG iIndex, DNBIATCPTrace *& oTCPPath) \
{ \
return (ENVTIECALL(DNBIATCPTraceManager,ENVTIETypeLetter,ENVTIELetter)GetPath(iIndex,oTCPPath)); \
} \
HRESULT __stdcall  ENVTIEName::RemovePathByObject(DNBIATCPTrace * TCPPath) \
{ \
return (ENVTIECALL(DNBIATCPTraceManager,ENVTIETypeLetter,ENVTIELetter)RemovePathByObject(TCPPath)); \
} \
HRESULT __stdcall  ENVTIEName::RemovePathByIndex(CATLONG iIndex) \
{ \
return (ENVTIECALL(DNBIATCPTraceManager,ENVTIETypeLetter,ENVTIELetter)RemovePathByIndex(iIndex)); \
} \
HRESULT __stdcall  ENVTIEName::RemoveAllPath() \
{ \
return (ENVTIECALL(DNBIATCPTraceManager,ENVTIETypeLetter,ENVTIELetter)RemoveAllPath()); \
} \
HRESULT __stdcall  ENVTIEName::GetAttachedOwner(CATIABase *& oOwner) \
{ \
return (ENVTIECALL(DNBIATCPTraceManager,ENVTIETypeLetter,ENVTIELetter)GetAttachedOwner(oOwner)); \
} \
HRESULT __stdcall  ENVTIEName::SetAttachedOwner(CATIABase * iOwner) \
{ \
return (ENVTIECALL(DNBIATCPTraceManager,ENVTIETypeLetter,ENVTIELetter)SetAttachedOwner(iOwner)); \
} \
HRESULT __stdcall  ENVTIEName::ResetAttachedOwner() \
{ \
return (ENVTIECALL(DNBIATCPTraceManager,ENVTIETypeLetter,ENVTIELetter)ResetAttachedOwner()); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIATCPTraceManager,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIATCPTraceManager,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIATCPTraceManager,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIATCPTraceManager,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIATCPTraceManager,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIATCPTraceManager(classe)    TIEDNBIATCPTraceManager##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIATCPTraceManager(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIATCPTraceManager, classe) \
 \
 \
CATImplementTIEMethods(DNBIATCPTraceManager, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIATCPTraceManager, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIATCPTraceManager, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIATCPTraceManager, classe) \
 \
HRESULT __stdcall  TIEDNBIATCPTraceManager##classe::get_TCPTraceMode(CAT_VARIANT_BOOL & oTCPTraceMode) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&oTCPTraceMode); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_TCPTraceMode(oTCPTraceMode); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&oTCPTraceMode); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIATCPTraceManager##classe::put_TCPTraceMode(CAT_VARIANT_BOOL iTCPTraceMode) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&iTCPTraceMode); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_TCPTraceMode(iTCPTraceMode); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&iTCPTraceMode); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIATCPTraceManager##classe::get_InitStatus(CAT_VARIANT_BOOL & oInitStatus) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&oInitStatus); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_InitStatus(oInitStatus); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&oInitStatus); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIATCPTraceManager##classe::put_InitStatus(CAT_VARIANT_BOOL iInitStatus) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&iInitStatus); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_InitStatus(iInitStatus); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&iInitStatus); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIATCPTraceManager##classe::GetNbPath(CATLONG & oNb) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&oNb); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetNbPath(oNb); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&oNb); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIATCPTraceManager##classe::GetPath(CATLONG iIndex, DNBIATCPTrace *& oTCPPath) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&iIndex,&oTCPPath); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetPath(iIndex,oTCPPath); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&iIndex,&oTCPPath); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIATCPTraceManager##classe::RemovePathByObject(DNBIATCPTrace * TCPPath) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&TCPPath); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->RemovePathByObject(TCPPath); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&TCPPath); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIATCPTraceManager##classe::RemovePathByIndex(CATLONG iIndex) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&iIndex); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->RemovePathByIndex(iIndex); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&iIndex); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIATCPTraceManager##classe::RemoveAllPath() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->RemoveAllPath(); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIATCPTraceManager##classe::GetAttachedOwner(CATIABase *& oOwner) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&oOwner); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAttachedOwner(oOwner); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&oOwner); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIATCPTraceManager##classe::SetAttachedOwner(CATIABase * iOwner) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,11,&_Trac2,&iOwner); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetAttachedOwner(iOwner); \
   ExitAfterCall(this,11,_Trac2,&_ret_arg,&iOwner); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIATCPTraceManager##classe::ResetAttachedOwner() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,12,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ResetAttachedOwner(); \
   ExitAfterCall(this,12,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIATCPTraceManager##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,13,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,13,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIATCPTraceManager##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,14,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,14,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIATCPTraceManager##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,15,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,15,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIATCPTraceManager##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,16,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,16,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIATCPTraceManager##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,17,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,17,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIATCPTraceManager(classe) \
 \
 \
declare_TIE_DNBIATCPTraceManager(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIATCPTraceManager##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIATCPTraceManager,"DNBIATCPTraceManager",DNBIATCPTraceManager::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIATCPTraceManager(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIATCPTraceManager, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIATCPTraceManager##classe(classe::MetaObject(),DNBIATCPTraceManager::MetaObject(),(void *)CreateTIEDNBIATCPTraceManager##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIATCPTraceManager(classe) \
 \
 \
declare_TIE_DNBIATCPTraceManager(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIATCPTraceManager##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIATCPTraceManager,"DNBIATCPTraceManager",DNBIATCPTraceManager::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIATCPTraceManager(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIATCPTraceManager, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIATCPTraceManager##classe(classe::MetaObject(),DNBIATCPTraceManager::MetaObject(),(void *)CreateTIEDNBIATCPTraceManager##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIATCPTraceManager(classe) TIE_DNBIATCPTraceManager(classe)
#else
#define BOA_DNBIATCPTraceManager(classe) CATImplementBOA(DNBIATCPTraceManager, classe)
#endif

#endif
