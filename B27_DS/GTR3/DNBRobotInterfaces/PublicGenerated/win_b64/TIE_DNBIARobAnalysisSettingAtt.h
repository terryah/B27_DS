#ifndef __TIE_DNBIARobAnalysisSettingAtt
#define __TIE_DNBIARobAnalysisSettingAtt

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIARobAnalysisSettingAtt.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIARobAnalysisSettingAtt */
#define declare_TIE_DNBIARobAnalysisSettingAtt(classe) \
 \
 \
class TIEDNBIARobAnalysisSettingAtt##classe : public DNBIARobAnalysisSettingAtt \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIARobAnalysisSettingAtt, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall get_LinSpeedLimit(DNBAnalysisLevel & oLevel); \
      virtual HRESULT __stdcall put_LinSpeedLimit(DNBAnalysisLevel iLevel); \
      virtual HRESULT __stdcall GetLinSpeedLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetLinSpeedLimitLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_RotSpeedLimit(DNBAnalysisLevel & oLevel); \
      virtual HRESULT __stdcall put_RotSpeedLimit(DNBAnalysisLevel iLevel); \
      virtual HRESULT __stdcall GetRotSpeedLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetRotSpeedLimitLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_LinAccelLimit(DNBAnalysisLevel & oLevel); \
      virtual HRESULT __stdcall put_LinAccelLimit(DNBAnalysisLevel iLevel); \
      virtual HRESULT __stdcall GetLinAccelLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetLinAccelLimitLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_RotAccelLimit(DNBAnalysisLevel & oLevel); \
      virtual HRESULT __stdcall put_RotAccelLimit(DNBAnalysisLevel iLevel); \
      virtual HRESULT __stdcall GetRotAccelLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetRotAccelLimitLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_LinSpeedColor(CATSafeArrayVariant *& oColor); \
      virtual HRESULT __stdcall put_LinSpeedColor(const CATSafeArrayVariant & iColor); \
      virtual HRESULT __stdcall GetLinSpeedColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetLinSpeedColorLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_RotSpeedColor(CATSafeArrayVariant *& oColor); \
      virtual HRESULT __stdcall put_RotSpeedColor(const CATSafeArrayVariant & iColor); \
      virtual HRESULT __stdcall GetRotSpeedColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetRotSpeedColorLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_LinAccelColor(CATSafeArrayVariant *& oColor); \
      virtual HRESULT __stdcall put_LinAccelColor(const CATSafeArrayVariant & iColor); \
      virtual HRESULT __stdcall GetLinAccelColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetLinAccelColorLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_RotAccelColor(CATSafeArrayVariant *& oColor); \
      virtual HRESULT __stdcall put_RotAccelColor(const CATSafeArrayVariant & iColor); \
      virtual HRESULT __stdcall GetRotAccelColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetRotAccelColorLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_ReachColor(CATSafeArrayVariant *& oColor); \
      virtual HRESULT __stdcall put_ReachColor(const CATSafeArrayVariant & iColor); \
      virtual HRESULT __stdcall GetReachColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetReachColorLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_SingularColor(CATSafeArrayVariant *& oColor); \
      virtual HRESULT __stdcall put_SingularColor(const CATSafeArrayVariant & iColor); \
      virtual HRESULT __stdcall GetSingularColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetSingularColorLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall Commit(); \
      virtual HRESULT __stdcall Rollback(); \
      virtual HRESULT __stdcall ResetToAdminValues(); \
      virtual HRESULT __stdcall ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList); \
      virtual HRESULT __stdcall SaveRepository(); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIARobAnalysisSettingAtt(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall get_LinSpeedLimit(DNBAnalysisLevel & oLevel); \
virtual HRESULT __stdcall put_LinSpeedLimit(DNBAnalysisLevel iLevel); \
virtual HRESULT __stdcall GetLinSpeedLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetLinSpeedLimitLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_RotSpeedLimit(DNBAnalysisLevel & oLevel); \
virtual HRESULT __stdcall put_RotSpeedLimit(DNBAnalysisLevel iLevel); \
virtual HRESULT __stdcall GetRotSpeedLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetRotSpeedLimitLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_LinAccelLimit(DNBAnalysisLevel & oLevel); \
virtual HRESULT __stdcall put_LinAccelLimit(DNBAnalysisLevel iLevel); \
virtual HRESULT __stdcall GetLinAccelLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetLinAccelLimitLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_RotAccelLimit(DNBAnalysisLevel & oLevel); \
virtual HRESULT __stdcall put_RotAccelLimit(DNBAnalysisLevel iLevel); \
virtual HRESULT __stdcall GetRotAccelLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetRotAccelLimitLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_LinSpeedColor(CATSafeArrayVariant *& oColor); \
virtual HRESULT __stdcall put_LinSpeedColor(const CATSafeArrayVariant & iColor); \
virtual HRESULT __stdcall GetLinSpeedColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetLinSpeedColorLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_RotSpeedColor(CATSafeArrayVariant *& oColor); \
virtual HRESULT __stdcall put_RotSpeedColor(const CATSafeArrayVariant & iColor); \
virtual HRESULT __stdcall GetRotSpeedColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetRotSpeedColorLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_LinAccelColor(CATSafeArrayVariant *& oColor); \
virtual HRESULT __stdcall put_LinAccelColor(const CATSafeArrayVariant & iColor); \
virtual HRESULT __stdcall GetLinAccelColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetLinAccelColorLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_RotAccelColor(CATSafeArrayVariant *& oColor); \
virtual HRESULT __stdcall put_RotAccelColor(const CATSafeArrayVariant & iColor); \
virtual HRESULT __stdcall GetRotAccelColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetRotAccelColorLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_ReachColor(CATSafeArrayVariant *& oColor); \
virtual HRESULT __stdcall put_ReachColor(const CATSafeArrayVariant & iColor); \
virtual HRESULT __stdcall GetReachColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetReachColorLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_SingularColor(CATSafeArrayVariant *& oColor); \
virtual HRESULT __stdcall put_SingularColor(const CATSafeArrayVariant & iColor); \
virtual HRESULT __stdcall GetSingularColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetSingularColorLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall Commit(); \
virtual HRESULT __stdcall Rollback(); \
virtual HRESULT __stdcall ResetToAdminValues(); \
virtual HRESULT __stdcall ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList); \
virtual HRESULT __stdcall SaveRepository(); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIARobAnalysisSettingAtt(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::get_LinSpeedLimit(DNBAnalysisLevel & oLevel) \
{ \
return (ENVTIECALL(DNBIARobAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_LinSpeedLimit(oLevel)); \
} \
HRESULT __stdcall  ENVTIEName::put_LinSpeedLimit(DNBAnalysisLevel iLevel) \
{ \
return (ENVTIECALL(DNBIARobAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_LinSpeedLimit(iLevel)); \
} \
HRESULT __stdcall  ENVTIEName::GetLinSpeedLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIARobAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetLinSpeedLimitInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetLinSpeedLimitLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIARobAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetLinSpeedLimitLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_RotSpeedLimit(DNBAnalysisLevel & oLevel) \
{ \
return (ENVTIECALL(DNBIARobAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_RotSpeedLimit(oLevel)); \
} \
HRESULT __stdcall  ENVTIEName::put_RotSpeedLimit(DNBAnalysisLevel iLevel) \
{ \
return (ENVTIECALL(DNBIARobAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_RotSpeedLimit(iLevel)); \
} \
HRESULT __stdcall  ENVTIEName::GetRotSpeedLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIARobAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetRotSpeedLimitInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetRotSpeedLimitLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIARobAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetRotSpeedLimitLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_LinAccelLimit(DNBAnalysisLevel & oLevel) \
{ \
return (ENVTIECALL(DNBIARobAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_LinAccelLimit(oLevel)); \
} \
HRESULT __stdcall  ENVTIEName::put_LinAccelLimit(DNBAnalysisLevel iLevel) \
{ \
return (ENVTIECALL(DNBIARobAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_LinAccelLimit(iLevel)); \
} \
HRESULT __stdcall  ENVTIEName::GetLinAccelLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIARobAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetLinAccelLimitInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetLinAccelLimitLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIARobAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetLinAccelLimitLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_RotAccelLimit(DNBAnalysisLevel & oLevel) \
{ \
return (ENVTIECALL(DNBIARobAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_RotAccelLimit(oLevel)); \
} \
HRESULT __stdcall  ENVTIEName::put_RotAccelLimit(DNBAnalysisLevel iLevel) \
{ \
return (ENVTIECALL(DNBIARobAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_RotAccelLimit(iLevel)); \
} \
HRESULT __stdcall  ENVTIEName::GetRotAccelLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIARobAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetRotAccelLimitInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetRotAccelLimitLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIARobAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetRotAccelLimitLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_LinSpeedColor(CATSafeArrayVariant *& oColor) \
{ \
return (ENVTIECALL(DNBIARobAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_LinSpeedColor(oColor)); \
} \
HRESULT __stdcall  ENVTIEName::put_LinSpeedColor(const CATSafeArrayVariant & iColor) \
{ \
return (ENVTIECALL(DNBIARobAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_LinSpeedColor(iColor)); \
} \
HRESULT __stdcall  ENVTIEName::GetLinSpeedColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIARobAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetLinSpeedColorInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetLinSpeedColorLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIARobAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetLinSpeedColorLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_RotSpeedColor(CATSafeArrayVariant *& oColor) \
{ \
return (ENVTIECALL(DNBIARobAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_RotSpeedColor(oColor)); \
} \
HRESULT __stdcall  ENVTIEName::put_RotSpeedColor(const CATSafeArrayVariant & iColor) \
{ \
return (ENVTIECALL(DNBIARobAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_RotSpeedColor(iColor)); \
} \
HRESULT __stdcall  ENVTIEName::GetRotSpeedColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIARobAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetRotSpeedColorInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetRotSpeedColorLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIARobAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetRotSpeedColorLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_LinAccelColor(CATSafeArrayVariant *& oColor) \
{ \
return (ENVTIECALL(DNBIARobAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_LinAccelColor(oColor)); \
} \
HRESULT __stdcall  ENVTIEName::put_LinAccelColor(const CATSafeArrayVariant & iColor) \
{ \
return (ENVTIECALL(DNBIARobAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_LinAccelColor(iColor)); \
} \
HRESULT __stdcall  ENVTIEName::GetLinAccelColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIARobAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetLinAccelColorInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetLinAccelColorLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIARobAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetLinAccelColorLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_RotAccelColor(CATSafeArrayVariant *& oColor) \
{ \
return (ENVTIECALL(DNBIARobAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_RotAccelColor(oColor)); \
} \
HRESULT __stdcall  ENVTIEName::put_RotAccelColor(const CATSafeArrayVariant & iColor) \
{ \
return (ENVTIECALL(DNBIARobAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_RotAccelColor(iColor)); \
} \
HRESULT __stdcall  ENVTIEName::GetRotAccelColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIARobAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetRotAccelColorInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetRotAccelColorLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIARobAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetRotAccelColorLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_ReachColor(CATSafeArrayVariant *& oColor) \
{ \
return (ENVTIECALL(DNBIARobAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_ReachColor(oColor)); \
} \
HRESULT __stdcall  ENVTIEName::put_ReachColor(const CATSafeArrayVariant & iColor) \
{ \
return (ENVTIECALL(DNBIARobAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_ReachColor(iColor)); \
} \
HRESULT __stdcall  ENVTIEName::GetReachColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIARobAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetReachColorInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetReachColorLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIARobAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetReachColorLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_SingularColor(CATSafeArrayVariant *& oColor) \
{ \
return (ENVTIECALL(DNBIARobAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_SingularColor(oColor)); \
} \
HRESULT __stdcall  ENVTIEName::put_SingularColor(const CATSafeArrayVariant & iColor) \
{ \
return (ENVTIECALL(DNBIARobAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_SingularColor(iColor)); \
} \
HRESULT __stdcall  ENVTIEName::GetSingularColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIARobAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetSingularColorInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetSingularColorLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIARobAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetSingularColorLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::Commit() \
{ \
return (ENVTIECALL(DNBIARobAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)Commit()); \
} \
HRESULT __stdcall  ENVTIEName::Rollback() \
{ \
return (ENVTIECALL(DNBIARobAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)Rollback()); \
} \
HRESULT __stdcall  ENVTIEName::ResetToAdminValues() \
{ \
return (ENVTIECALL(DNBIARobAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)ResetToAdminValues()); \
} \
HRESULT __stdcall  ENVTIEName::ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList) \
{ \
return (ENVTIECALL(DNBIARobAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)ResetToAdminValuesByName(iAttList)); \
} \
HRESULT __stdcall  ENVTIEName::SaveRepository() \
{ \
return (ENVTIECALL(DNBIARobAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)SaveRepository()); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIARobAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIARobAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIARobAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIARobAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIARobAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIARobAnalysisSettingAtt(classe)    TIEDNBIARobAnalysisSettingAtt##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIARobAnalysisSettingAtt(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIARobAnalysisSettingAtt, classe) \
 \
 \
CATImplementTIEMethods(DNBIARobAnalysisSettingAtt, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIARobAnalysisSettingAtt, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIARobAnalysisSettingAtt, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIARobAnalysisSettingAtt, classe) \
 \
HRESULT __stdcall  TIEDNBIARobAnalysisSettingAtt##classe::get_LinSpeedLimit(DNBAnalysisLevel & oLevel) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&oLevel); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_LinSpeedLimit(oLevel); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&oLevel); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobAnalysisSettingAtt##classe::put_LinSpeedLimit(DNBAnalysisLevel iLevel) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&iLevel); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_LinSpeedLimit(iLevel); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&iLevel); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobAnalysisSettingAtt##classe::GetLinSpeedLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetLinSpeedLimitInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobAnalysisSettingAtt##classe::SetLinSpeedLimitLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetLinSpeedLimitLock(iLocked); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobAnalysisSettingAtt##classe::get_RotSpeedLimit(DNBAnalysisLevel & oLevel) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&oLevel); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_RotSpeedLimit(oLevel); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&oLevel); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobAnalysisSettingAtt##classe::put_RotSpeedLimit(DNBAnalysisLevel iLevel) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&iLevel); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_RotSpeedLimit(iLevel); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&iLevel); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobAnalysisSettingAtt##classe::GetRotSpeedLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetRotSpeedLimitInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobAnalysisSettingAtt##classe::SetRotSpeedLimitLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetRotSpeedLimitLock(iLocked); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobAnalysisSettingAtt##classe::get_LinAccelLimit(DNBAnalysisLevel & oLevel) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&oLevel); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_LinAccelLimit(oLevel); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&oLevel); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobAnalysisSettingAtt##classe::put_LinAccelLimit(DNBAnalysisLevel iLevel) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&iLevel); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_LinAccelLimit(iLevel); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&iLevel); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobAnalysisSettingAtt##classe::GetLinAccelLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,11,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetLinAccelLimitInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,11,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobAnalysisSettingAtt##classe::SetLinAccelLimitLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,12,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetLinAccelLimitLock(iLocked); \
   ExitAfterCall(this,12,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobAnalysisSettingAtt##classe::get_RotAccelLimit(DNBAnalysisLevel & oLevel) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,13,&_Trac2,&oLevel); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_RotAccelLimit(oLevel); \
   ExitAfterCall(this,13,_Trac2,&_ret_arg,&oLevel); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobAnalysisSettingAtt##classe::put_RotAccelLimit(DNBAnalysisLevel iLevel) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,14,&_Trac2,&iLevel); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_RotAccelLimit(iLevel); \
   ExitAfterCall(this,14,_Trac2,&_ret_arg,&iLevel); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobAnalysisSettingAtt##classe::GetRotAccelLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,15,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetRotAccelLimitInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,15,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobAnalysisSettingAtt##classe::SetRotAccelLimitLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,16,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetRotAccelLimitLock(iLocked); \
   ExitAfterCall(this,16,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobAnalysisSettingAtt##classe::get_LinSpeedColor(CATSafeArrayVariant *& oColor) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,17,&_Trac2,&oColor); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_LinSpeedColor(oColor); \
   ExitAfterCall(this,17,_Trac2,&_ret_arg,&oColor); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobAnalysisSettingAtt##classe::put_LinSpeedColor(const CATSafeArrayVariant & iColor) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,18,&_Trac2,&iColor); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_LinSpeedColor(iColor); \
   ExitAfterCall(this,18,_Trac2,&_ret_arg,&iColor); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobAnalysisSettingAtt##classe::GetLinSpeedColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,19,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetLinSpeedColorInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,19,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobAnalysisSettingAtt##classe::SetLinSpeedColorLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,20,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetLinSpeedColorLock(iLocked); \
   ExitAfterCall(this,20,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobAnalysisSettingAtt##classe::get_RotSpeedColor(CATSafeArrayVariant *& oColor) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,21,&_Trac2,&oColor); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_RotSpeedColor(oColor); \
   ExitAfterCall(this,21,_Trac2,&_ret_arg,&oColor); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobAnalysisSettingAtt##classe::put_RotSpeedColor(const CATSafeArrayVariant & iColor) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,22,&_Trac2,&iColor); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_RotSpeedColor(iColor); \
   ExitAfterCall(this,22,_Trac2,&_ret_arg,&iColor); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobAnalysisSettingAtt##classe::GetRotSpeedColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,23,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetRotSpeedColorInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,23,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobAnalysisSettingAtt##classe::SetRotSpeedColorLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,24,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetRotSpeedColorLock(iLocked); \
   ExitAfterCall(this,24,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobAnalysisSettingAtt##classe::get_LinAccelColor(CATSafeArrayVariant *& oColor) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,25,&_Trac2,&oColor); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_LinAccelColor(oColor); \
   ExitAfterCall(this,25,_Trac2,&_ret_arg,&oColor); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobAnalysisSettingAtt##classe::put_LinAccelColor(const CATSafeArrayVariant & iColor) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,26,&_Trac2,&iColor); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_LinAccelColor(iColor); \
   ExitAfterCall(this,26,_Trac2,&_ret_arg,&iColor); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobAnalysisSettingAtt##classe::GetLinAccelColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,27,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetLinAccelColorInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,27,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobAnalysisSettingAtt##classe::SetLinAccelColorLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,28,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetLinAccelColorLock(iLocked); \
   ExitAfterCall(this,28,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobAnalysisSettingAtt##classe::get_RotAccelColor(CATSafeArrayVariant *& oColor) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,29,&_Trac2,&oColor); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_RotAccelColor(oColor); \
   ExitAfterCall(this,29,_Trac2,&_ret_arg,&oColor); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobAnalysisSettingAtt##classe::put_RotAccelColor(const CATSafeArrayVariant & iColor) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,30,&_Trac2,&iColor); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_RotAccelColor(iColor); \
   ExitAfterCall(this,30,_Trac2,&_ret_arg,&iColor); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobAnalysisSettingAtt##classe::GetRotAccelColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,31,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetRotAccelColorInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,31,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobAnalysisSettingAtt##classe::SetRotAccelColorLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,32,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetRotAccelColorLock(iLocked); \
   ExitAfterCall(this,32,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobAnalysisSettingAtt##classe::get_ReachColor(CATSafeArrayVariant *& oColor) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,33,&_Trac2,&oColor); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_ReachColor(oColor); \
   ExitAfterCall(this,33,_Trac2,&_ret_arg,&oColor); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobAnalysisSettingAtt##classe::put_ReachColor(const CATSafeArrayVariant & iColor) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,34,&_Trac2,&iColor); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_ReachColor(iColor); \
   ExitAfterCall(this,34,_Trac2,&_ret_arg,&iColor); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobAnalysisSettingAtt##classe::GetReachColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,35,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetReachColorInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,35,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobAnalysisSettingAtt##classe::SetReachColorLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,36,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetReachColorLock(iLocked); \
   ExitAfterCall(this,36,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobAnalysisSettingAtt##classe::get_SingularColor(CATSafeArrayVariant *& oColor) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,37,&_Trac2,&oColor); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_SingularColor(oColor); \
   ExitAfterCall(this,37,_Trac2,&_ret_arg,&oColor); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobAnalysisSettingAtt##classe::put_SingularColor(const CATSafeArrayVariant & iColor) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,38,&_Trac2,&iColor); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_SingularColor(iColor); \
   ExitAfterCall(this,38,_Trac2,&_ret_arg,&iColor); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobAnalysisSettingAtt##classe::GetSingularColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,39,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetSingularColorInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,39,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobAnalysisSettingAtt##classe::SetSingularColorLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,40,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetSingularColorLock(iLocked); \
   ExitAfterCall(this,40,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobAnalysisSettingAtt##classe::Commit() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,41,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Commit(); \
   ExitAfterCall(this,41,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobAnalysisSettingAtt##classe::Rollback() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,42,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Rollback(); \
   ExitAfterCall(this,42,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobAnalysisSettingAtt##classe::ResetToAdminValues() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,43,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ResetToAdminValues(); \
   ExitAfterCall(this,43,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobAnalysisSettingAtt##classe::ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,44,&_Trac2,&iAttList); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ResetToAdminValuesByName(iAttList); \
   ExitAfterCall(this,44,_Trac2,&_ret_arg,&iAttList); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobAnalysisSettingAtt##classe::SaveRepository() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,45,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SaveRepository(); \
   ExitAfterCall(this,45,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIARobAnalysisSettingAtt##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,46,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,46,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIARobAnalysisSettingAtt##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,47,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,47,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIARobAnalysisSettingAtt##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,48,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,48,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIARobAnalysisSettingAtt##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,49,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,49,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIARobAnalysisSettingAtt##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,50,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,50,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIARobAnalysisSettingAtt(classe) \
 \
 \
declare_TIE_DNBIARobAnalysisSettingAtt(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIARobAnalysisSettingAtt##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIARobAnalysisSettingAtt,"DNBIARobAnalysisSettingAtt",DNBIARobAnalysisSettingAtt::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIARobAnalysisSettingAtt(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIARobAnalysisSettingAtt, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIARobAnalysisSettingAtt##classe(classe::MetaObject(),DNBIARobAnalysisSettingAtt::MetaObject(),(void *)CreateTIEDNBIARobAnalysisSettingAtt##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIARobAnalysisSettingAtt(classe) \
 \
 \
declare_TIE_DNBIARobAnalysisSettingAtt(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIARobAnalysisSettingAtt##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIARobAnalysisSettingAtt,"DNBIARobAnalysisSettingAtt",DNBIARobAnalysisSettingAtt::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIARobAnalysisSettingAtt(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIARobAnalysisSettingAtt, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIARobAnalysisSettingAtt##classe(classe::MetaObject(),DNBIARobAnalysisSettingAtt::MetaObject(),(void *)CreateTIEDNBIARobAnalysisSettingAtt##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIARobAnalysisSettingAtt(classe) TIE_DNBIARobAnalysisSettingAtt(classe)
#else
#define BOA_DNBIARobAnalysisSettingAtt(classe) CATImplementBOA(DNBIARobAnalysisSettingAtt, classe)
#endif

#endif
