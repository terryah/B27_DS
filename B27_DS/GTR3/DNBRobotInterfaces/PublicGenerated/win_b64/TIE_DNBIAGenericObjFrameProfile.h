#ifndef __TIE_DNBIAGenericObjFrameProfile
#define __TIE_DNBIAGenericObjFrameProfile

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIAGenericObjFrameProfile.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIAGenericObjFrameProfile */
#define declare_TIE_DNBIAGenericObjFrameProfile(classe) \
 \
 \
class TIEDNBIAGenericObjFrameProfile##classe : public DNBIAGenericObjFrameProfile \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIAGenericObjFrameProfile, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall SetName(const CATBSTR & iName); \
      virtual HRESULT __stdcall GetName(CATBSTR & oName); \
      virtual HRESULT __stdcall SetObjectFrame(double x, double y, double z, double roll, double pitch, double yaw); \
      virtual HRESULT __stdcall GetObjectFrame(double & x, double & y, double & z, double & roll, double & pitch, double & yaw); \
      virtual HRESULT __stdcall GetController(DNBIARobGenericController *& oController); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIAGenericObjFrameProfile(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall SetName(const CATBSTR & iName); \
virtual HRESULT __stdcall GetName(CATBSTR & oName); \
virtual HRESULT __stdcall SetObjectFrame(double x, double y, double z, double roll, double pitch, double yaw); \
virtual HRESULT __stdcall GetObjectFrame(double & x, double & y, double & z, double & roll, double & pitch, double & yaw); \
virtual HRESULT __stdcall GetController(DNBIARobGenericController *& oController); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIAGenericObjFrameProfile(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::SetName(const CATBSTR & iName) \
{ \
return (ENVTIECALL(DNBIAGenericObjFrameProfile,ENVTIETypeLetter,ENVTIELetter)SetName(iName)); \
} \
HRESULT __stdcall  ENVTIEName::GetName(CATBSTR & oName) \
{ \
return (ENVTIECALL(DNBIAGenericObjFrameProfile,ENVTIETypeLetter,ENVTIELetter)GetName(oName)); \
} \
HRESULT __stdcall  ENVTIEName::SetObjectFrame(double x, double y, double z, double roll, double pitch, double yaw) \
{ \
return (ENVTIECALL(DNBIAGenericObjFrameProfile,ENVTIETypeLetter,ENVTIELetter)SetObjectFrame(x,y,z,roll,pitch,yaw)); \
} \
HRESULT __stdcall  ENVTIEName::GetObjectFrame(double & x, double & y, double & z, double & roll, double & pitch, double & yaw) \
{ \
return (ENVTIECALL(DNBIAGenericObjFrameProfile,ENVTIETypeLetter,ENVTIELetter)GetObjectFrame(x,y,z,roll,pitch,yaw)); \
} \
HRESULT __stdcall  ENVTIEName::GetController(DNBIARobGenericController *& oController) \
{ \
return (ENVTIECALL(DNBIAGenericObjFrameProfile,ENVTIETypeLetter,ENVTIELetter)GetController(oController)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIAGenericObjFrameProfile,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIAGenericObjFrameProfile,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIAGenericObjFrameProfile,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIAGenericObjFrameProfile,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIAGenericObjFrameProfile,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIAGenericObjFrameProfile(classe)    TIEDNBIAGenericObjFrameProfile##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIAGenericObjFrameProfile(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIAGenericObjFrameProfile, classe) \
 \
 \
CATImplementTIEMethods(DNBIAGenericObjFrameProfile, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIAGenericObjFrameProfile, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIAGenericObjFrameProfile, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIAGenericObjFrameProfile, classe) \
 \
HRESULT __stdcall  TIEDNBIAGenericObjFrameProfile##classe::SetName(const CATBSTR & iName) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&iName); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetName(iName); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&iName); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAGenericObjFrameProfile##classe::GetName(CATBSTR & oName) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&oName); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetName(oName); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&oName); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAGenericObjFrameProfile##classe::SetObjectFrame(double x, double y, double z, double roll, double pitch, double yaw) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&x,&y,&z,&roll,&pitch,&yaw); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetObjectFrame(x,y,z,roll,pitch,yaw); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&x,&y,&z,&roll,&pitch,&yaw); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAGenericObjFrameProfile##classe::GetObjectFrame(double & x, double & y, double & z, double & roll, double & pitch, double & yaw) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&x,&y,&z,&roll,&pitch,&yaw); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetObjectFrame(x,y,z,roll,pitch,yaw); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&x,&y,&z,&roll,&pitch,&yaw); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAGenericObjFrameProfile##classe::GetController(DNBIARobGenericController *& oController) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&oController); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetController(oController); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&oController); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAGenericObjFrameProfile##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAGenericObjFrameProfile##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAGenericObjFrameProfile##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAGenericObjFrameProfile##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAGenericObjFrameProfile##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIAGenericObjFrameProfile(classe) \
 \
 \
declare_TIE_DNBIAGenericObjFrameProfile(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAGenericObjFrameProfile##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAGenericObjFrameProfile,"DNBIAGenericObjFrameProfile",DNBIAGenericObjFrameProfile::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAGenericObjFrameProfile(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIAGenericObjFrameProfile, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAGenericObjFrameProfile##classe(classe::MetaObject(),DNBIAGenericObjFrameProfile::MetaObject(),(void *)CreateTIEDNBIAGenericObjFrameProfile##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIAGenericObjFrameProfile(classe) \
 \
 \
declare_TIE_DNBIAGenericObjFrameProfile(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAGenericObjFrameProfile##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAGenericObjFrameProfile,"DNBIAGenericObjFrameProfile",DNBIAGenericObjFrameProfile::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAGenericObjFrameProfile(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIAGenericObjFrameProfile, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAGenericObjFrameProfile##classe(classe::MetaObject(),DNBIAGenericObjFrameProfile::MetaObject(),(void *)CreateTIEDNBIAGenericObjFrameProfile##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIAGenericObjFrameProfile(classe) TIE_DNBIAGenericObjFrameProfile(classe)
#else
#define BOA_DNBIAGenericObjFrameProfile(classe) CATImplementBOA(DNBIAGenericObjFrameProfile, classe)
#endif

#endif
