#ifndef __TIE_DNBIARobCalibOffsets
#define __TIE_DNBIARobCalibOffsets

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIARobCalibOffsets.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIARobCalibOffsets */
#define declare_TIE_DNBIARobCalibOffsets(classe) \
 \
 \
class TIEDNBIARobCalibOffsets##classe : public DNBIARobCalibOffsets \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIARobCalibOffsets, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall SetReferenceBasePosition(double x, double y, double z, double roll, double pitch, double yaw); \
      virtual HRESULT __stdcall GetReferenceBasePosition(double & x, double & y, double & z, double & roll, double & pitch, double & yaw); \
      virtual HRESULT __stdcall SetNominalBaseOffset(double x, double y, double z, double roll, double pitch, double yaw); \
      virtual HRESULT __stdcall GetNominalBaseOffset(double & x, double & y, double & z, double & roll, double & pitch, double & yaw); \
      virtual HRESULT __stdcall SetNominalToolOffset(double x, double y, double z, double roll, double pitch, double yaw); \
      virtual HRESULT __stdcall GetNominalToolOffset(double & x, double & y, double & z, double & roll, double & pitch, double & yaw); \
      virtual HRESULT __stdcall SetCorrectedBaseOffset(double x, double y, double z, double roll, double pitch, double yaw); \
      virtual HRESULT __stdcall GetCorrectedBaseOffset(double & x, double & y, double & z, double & roll, double & pitch, double & yaw); \
      virtual HRESULT __stdcall SetCorrectedToolOffset(double x, double y, double z, double roll, double pitch, double yaw); \
      virtual HRESULT __stdcall GetCorrectedToolOffset(double & x, double & y, double & z, double & roll, double & pitch, double & yaw); \
      virtual HRESULT __stdcall SetCalibReferenceFile(const CATBSTR & iCalibReferenceFile); \
      virtual HRESULT __stdcall GetCalibReferenceFile(CATBSTR & oCalibReferenceFile); \
      virtual HRESULT __stdcall ApplyNominalBaseOffset(); \
      virtual HRESULT __stdcall ApplyCorrectedBaseOffset(); \
      virtual HRESULT __stdcall ApplyNominalToolOffset(); \
      virtual HRESULT __stdcall ApplyCorrectedToolOffset(); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIARobCalibOffsets(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall SetReferenceBasePosition(double x, double y, double z, double roll, double pitch, double yaw); \
virtual HRESULT __stdcall GetReferenceBasePosition(double & x, double & y, double & z, double & roll, double & pitch, double & yaw); \
virtual HRESULT __stdcall SetNominalBaseOffset(double x, double y, double z, double roll, double pitch, double yaw); \
virtual HRESULT __stdcall GetNominalBaseOffset(double & x, double & y, double & z, double & roll, double & pitch, double & yaw); \
virtual HRESULT __stdcall SetNominalToolOffset(double x, double y, double z, double roll, double pitch, double yaw); \
virtual HRESULT __stdcall GetNominalToolOffset(double & x, double & y, double & z, double & roll, double & pitch, double & yaw); \
virtual HRESULT __stdcall SetCorrectedBaseOffset(double x, double y, double z, double roll, double pitch, double yaw); \
virtual HRESULT __stdcall GetCorrectedBaseOffset(double & x, double & y, double & z, double & roll, double & pitch, double & yaw); \
virtual HRESULT __stdcall SetCorrectedToolOffset(double x, double y, double z, double roll, double pitch, double yaw); \
virtual HRESULT __stdcall GetCorrectedToolOffset(double & x, double & y, double & z, double & roll, double & pitch, double & yaw); \
virtual HRESULT __stdcall SetCalibReferenceFile(const CATBSTR & iCalibReferenceFile); \
virtual HRESULT __stdcall GetCalibReferenceFile(CATBSTR & oCalibReferenceFile); \
virtual HRESULT __stdcall ApplyNominalBaseOffset(); \
virtual HRESULT __stdcall ApplyCorrectedBaseOffset(); \
virtual HRESULT __stdcall ApplyNominalToolOffset(); \
virtual HRESULT __stdcall ApplyCorrectedToolOffset(); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIARobCalibOffsets(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::SetReferenceBasePosition(double x, double y, double z, double roll, double pitch, double yaw) \
{ \
return (ENVTIECALL(DNBIARobCalibOffsets,ENVTIETypeLetter,ENVTIELetter)SetReferenceBasePosition(x,y,z,roll,pitch,yaw)); \
} \
HRESULT __stdcall  ENVTIEName::GetReferenceBasePosition(double & x, double & y, double & z, double & roll, double & pitch, double & yaw) \
{ \
return (ENVTIECALL(DNBIARobCalibOffsets,ENVTIETypeLetter,ENVTIELetter)GetReferenceBasePosition(x,y,z,roll,pitch,yaw)); \
} \
HRESULT __stdcall  ENVTIEName::SetNominalBaseOffset(double x, double y, double z, double roll, double pitch, double yaw) \
{ \
return (ENVTIECALL(DNBIARobCalibOffsets,ENVTIETypeLetter,ENVTIELetter)SetNominalBaseOffset(x,y,z,roll,pitch,yaw)); \
} \
HRESULT __stdcall  ENVTIEName::GetNominalBaseOffset(double & x, double & y, double & z, double & roll, double & pitch, double & yaw) \
{ \
return (ENVTIECALL(DNBIARobCalibOffsets,ENVTIETypeLetter,ENVTIELetter)GetNominalBaseOffset(x,y,z,roll,pitch,yaw)); \
} \
HRESULT __stdcall  ENVTIEName::SetNominalToolOffset(double x, double y, double z, double roll, double pitch, double yaw) \
{ \
return (ENVTIECALL(DNBIARobCalibOffsets,ENVTIETypeLetter,ENVTIELetter)SetNominalToolOffset(x,y,z,roll,pitch,yaw)); \
} \
HRESULT __stdcall  ENVTIEName::GetNominalToolOffset(double & x, double & y, double & z, double & roll, double & pitch, double & yaw) \
{ \
return (ENVTIECALL(DNBIARobCalibOffsets,ENVTIETypeLetter,ENVTIELetter)GetNominalToolOffset(x,y,z,roll,pitch,yaw)); \
} \
HRESULT __stdcall  ENVTIEName::SetCorrectedBaseOffset(double x, double y, double z, double roll, double pitch, double yaw) \
{ \
return (ENVTIECALL(DNBIARobCalibOffsets,ENVTIETypeLetter,ENVTIELetter)SetCorrectedBaseOffset(x,y,z,roll,pitch,yaw)); \
} \
HRESULT __stdcall  ENVTIEName::GetCorrectedBaseOffset(double & x, double & y, double & z, double & roll, double & pitch, double & yaw) \
{ \
return (ENVTIECALL(DNBIARobCalibOffsets,ENVTIETypeLetter,ENVTIELetter)GetCorrectedBaseOffset(x,y,z,roll,pitch,yaw)); \
} \
HRESULT __stdcall  ENVTIEName::SetCorrectedToolOffset(double x, double y, double z, double roll, double pitch, double yaw) \
{ \
return (ENVTIECALL(DNBIARobCalibOffsets,ENVTIETypeLetter,ENVTIELetter)SetCorrectedToolOffset(x,y,z,roll,pitch,yaw)); \
} \
HRESULT __stdcall  ENVTIEName::GetCorrectedToolOffset(double & x, double & y, double & z, double & roll, double & pitch, double & yaw) \
{ \
return (ENVTIECALL(DNBIARobCalibOffsets,ENVTIETypeLetter,ENVTIELetter)GetCorrectedToolOffset(x,y,z,roll,pitch,yaw)); \
} \
HRESULT __stdcall  ENVTIEName::SetCalibReferenceFile(const CATBSTR & iCalibReferenceFile) \
{ \
return (ENVTIECALL(DNBIARobCalibOffsets,ENVTIETypeLetter,ENVTIELetter)SetCalibReferenceFile(iCalibReferenceFile)); \
} \
HRESULT __stdcall  ENVTIEName::GetCalibReferenceFile(CATBSTR & oCalibReferenceFile) \
{ \
return (ENVTIECALL(DNBIARobCalibOffsets,ENVTIETypeLetter,ENVTIELetter)GetCalibReferenceFile(oCalibReferenceFile)); \
} \
HRESULT __stdcall  ENVTIEName::ApplyNominalBaseOffset() \
{ \
return (ENVTIECALL(DNBIARobCalibOffsets,ENVTIETypeLetter,ENVTIELetter)ApplyNominalBaseOffset()); \
} \
HRESULT __stdcall  ENVTIEName::ApplyCorrectedBaseOffset() \
{ \
return (ENVTIECALL(DNBIARobCalibOffsets,ENVTIETypeLetter,ENVTIELetter)ApplyCorrectedBaseOffset()); \
} \
HRESULT __stdcall  ENVTIEName::ApplyNominalToolOffset() \
{ \
return (ENVTIECALL(DNBIARobCalibOffsets,ENVTIETypeLetter,ENVTIELetter)ApplyNominalToolOffset()); \
} \
HRESULT __stdcall  ENVTIEName::ApplyCorrectedToolOffset() \
{ \
return (ENVTIECALL(DNBIARobCalibOffsets,ENVTIETypeLetter,ENVTIELetter)ApplyCorrectedToolOffset()); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIARobCalibOffsets,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIARobCalibOffsets,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIARobCalibOffsets,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIARobCalibOffsets,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIARobCalibOffsets,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIARobCalibOffsets(classe)    TIEDNBIARobCalibOffsets##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIARobCalibOffsets(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIARobCalibOffsets, classe) \
 \
 \
CATImplementTIEMethods(DNBIARobCalibOffsets, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIARobCalibOffsets, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIARobCalibOffsets, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIARobCalibOffsets, classe) \
 \
HRESULT __stdcall  TIEDNBIARobCalibOffsets##classe::SetReferenceBasePosition(double x, double y, double z, double roll, double pitch, double yaw) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&x,&y,&z,&roll,&pitch,&yaw); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetReferenceBasePosition(x,y,z,roll,pitch,yaw); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&x,&y,&z,&roll,&pitch,&yaw); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobCalibOffsets##classe::GetReferenceBasePosition(double & x, double & y, double & z, double & roll, double & pitch, double & yaw) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&x,&y,&z,&roll,&pitch,&yaw); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetReferenceBasePosition(x,y,z,roll,pitch,yaw); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&x,&y,&z,&roll,&pitch,&yaw); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobCalibOffsets##classe::SetNominalBaseOffset(double x, double y, double z, double roll, double pitch, double yaw) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&x,&y,&z,&roll,&pitch,&yaw); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetNominalBaseOffset(x,y,z,roll,pitch,yaw); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&x,&y,&z,&roll,&pitch,&yaw); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobCalibOffsets##classe::GetNominalBaseOffset(double & x, double & y, double & z, double & roll, double & pitch, double & yaw) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&x,&y,&z,&roll,&pitch,&yaw); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetNominalBaseOffset(x,y,z,roll,pitch,yaw); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&x,&y,&z,&roll,&pitch,&yaw); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobCalibOffsets##classe::SetNominalToolOffset(double x, double y, double z, double roll, double pitch, double yaw) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&x,&y,&z,&roll,&pitch,&yaw); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetNominalToolOffset(x,y,z,roll,pitch,yaw); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&x,&y,&z,&roll,&pitch,&yaw); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobCalibOffsets##classe::GetNominalToolOffset(double & x, double & y, double & z, double & roll, double & pitch, double & yaw) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&x,&y,&z,&roll,&pitch,&yaw); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetNominalToolOffset(x,y,z,roll,pitch,yaw); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&x,&y,&z,&roll,&pitch,&yaw); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobCalibOffsets##classe::SetCorrectedBaseOffset(double x, double y, double z, double roll, double pitch, double yaw) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&x,&y,&z,&roll,&pitch,&yaw); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetCorrectedBaseOffset(x,y,z,roll,pitch,yaw); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&x,&y,&z,&roll,&pitch,&yaw); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobCalibOffsets##classe::GetCorrectedBaseOffset(double & x, double & y, double & z, double & roll, double & pitch, double & yaw) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&x,&y,&z,&roll,&pitch,&yaw); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetCorrectedBaseOffset(x,y,z,roll,pitch,yaw); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&x,&y,&z,&roll,&pitch,&yaw); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobCalibOffsets##classe::SetCorrectedToolOffset(double x, double y, double z, double roll, double pitch, double yaw) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&x,&y,&z,&roll,&pitch,&yaw); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetCorrectedToolOffset(x,y,z,roll,pitch,yaw); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&x,&y,&z,&roll,&pitch,&yaw); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobCalibOffsets##classe::GetCorrectedToolOffset(double & x, double & y, double & z, double & roll, double & pitch, double & yaw) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&x,&y,&z,&roll,&pitch,&yaw); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetCorrectedToolOffset(x,y,z,roll,pitch,yaw); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&x,&y,&z,&roll,&pitch,&yaw); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobCalibOffsets##classe::SetCalibReferenceFile(const CATBSTR & iCalibReferenceFile) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,11,&_Trac2,&iCalibReferenceFile); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetCalibReferenceFile(iCalibReferenceFile); \
   ExitAfterCall(this,11,_Trac2,&_ret_arg,&iCalibReferenceFile); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobCalibOffsets##classe::GetCalibReferenceFile(CATBSTR & oCalibReferenceFile) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,12,&_Trac2,&oCalibReferenceFile); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetCalibReferenceFile(oCalibReferenceFile); \
   ExitAfterCall(this,12,_Trac2,&_ret_arg,&oCalibReferenceFile); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobCalibOffsets##classe::ApplyNominalBaseOffset() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,13,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ApplyNominalBaseOffset(); \
   ExitAfterCall(this,13,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobCalibOffsets##classe::ApplyCorrectedBaseOffset() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,14,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ApplyCorrectedBaseOffset(); \
   ExitAfterCall(this,14,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobCalibOffsets##classe::ApplyNominalToolOffset() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,15,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ApplyNominalToolOffset(); \
   ExitAfterCall(this,15,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobCalibOffsets##classe::ApplyCorrectedToolOffset() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,16,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ApplyCorrectedToolOffset(); \
   ExitAfterCall(this,16,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIARobCalibOffsets##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,17,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,17,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIARobCalibOffsets##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,18,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,18,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIARobCalibOffsets##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,19,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,19,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIARobCalibOffsets##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,20,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,20,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIARobCalibOffsets##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,21,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,21,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIARobCalibOffsets(classe) \
 \
 \
declare_TIE_DNBIARobCalibOffsets(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIARobCalibOffsets##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIARobCalibOffsets,"DNBIARobCalibOffsets",DNBIARobCalibOffsets::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIARobCalibOffsets(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIARobCalibOffsets, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIARobCalibOffsets##classe(classe::MetaObject(),DNBIARobCalibOffsets::MetaObject(),(void *)CreateTIEDNBIARobCalibOffsets##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIARobCalibOffsets(classe) \
 \
 \
declare_TIE_DNBIARobCalibOffsets(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIARobCalibOffsets##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIARobCalibOffsets,"DNBIARobCalibOffsets",DNBIARobCalibOffsets::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIARobCalibOffsets(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIARobCalibOffsets, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIARobCalibOffsets##classe(classe::MetaObject(),DNBIARobCalibOffsets::MetaObject(),(void *)CreateTIEDNBIARobCalibOffsets##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIARobCalibOffsets(classe) TIE_DNBIARobCalibOffsets(classe)
#else
#define BOA_DNBIARobCalibOffsets(classe) CATImplementBOA(DNBIARobCalibOffsets, classe)
#endif

#endif
