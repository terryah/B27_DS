#ifndef __TIE_DNBIARobControllerFactory
#define __TIE_DNBIARobControllerFactory

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIARobControllerFactory.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIARobControllerFactory */
#define declare_TIE_DNBIARobControllerFactory(classe) \
 \
 \
class TIEDNBIARobControllerFactory##classe : public DNBIARobControllerFactory \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIARobControllerFactory, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall CreateGenericToolProfile(DNBIAGenericToolProfile *& oToolProfile); \
      virtual HRESULT __stdcall CreateGenericMotionProfile(DNBIAGenericMotionProfile *& oMotionProfile); \
      virtual HRESULT __stdcall CreateGenericAccuracyProfile(DNBIAGenericAccuracyProfile *& oAccuracyProfile); \
      virtual HRESULT __stdcall CreateGenericObjFrameProfile(DNBIAGenericObjFrameProfile *& oObjFrameProfile); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIARobControllerFactory(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall CreateGenericToolProfile(DNBIAGenericToolProfile *& oToolProfile); \
virtual HRESULT __stdcall CreateGenericMotionProfile(DNBIAGenericMotionProfile *& oMotionProfile); \
virtual HRESULT __stdcall CreateGenericAccuracyProfile(DNBIAGenericAccuracyProfile *& oAccuracyProfile); \
virtual HRESULT __stdcall CreateGenericObjFrameProfile(DNBIAGenericObjFrameProfile *& oObjFrameProfile); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIARobControllerFactory(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::CreateGenericToolProfile(DNBIAGenericToolProfile *& oToolProfile) \
{ \
return (ENVTIECALL(DNBIARobControllerFactory,ENVTIETypeLetter,ENVTIELetter)CreateGenericToolProfile(oToolProfile)); \
} \
HRESULT __stdcall  ENVTIEName::CreateGenericMotionProfile(DNBIAGenericMotionProfile *& oMotionProfile) \
{ \
return (ENVTIECALL(DNBIARobControllerFactory,ENVTIETypeLetter,ENVTIELetter)CreateGenericMotionProfile(oMotionProfile)); \
} \
HRESULT __stdcall  ENVTIEName::CreateGenericAccuracyProfile(DNBIAGenericAccuracyProfile *& oAccuracyProfile) \
{ \
return (ENVTIECALL(DNBIARobControllerFactory,ENVTIETypeLetter,ENVTIELetter)CreateGenericAccuracyProfile(oAccuracyProfile)); \
} \
HRESULT __stdcall  ENVTIEName::CreateGenericObjFrameProfile(DNBIAGenericObjFrameProfile *& oObjFrameProfile) \
{ \
return (ENVTIECALL(DNBIARobControllerFactory,ENVTIETypeLetter,ENVTIELetter)CreateGenericObjFrameProfile(oObjFrameProfile)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIARobControllerFactory,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIARobControllerFactory,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIARobControllerFactory,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIARobControllerFactory,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIARobControllerFactory,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIARobControllerFactory(classe)    TIEDNBIARobControllerFactory##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIARobControllerFactory(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIARobControllerFactory, classe) \
 \
 \
CATImplementTIEMethods(DNBIARobControllerFactory, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIARobControllerFactory, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIARobControllerFactory, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIARobControllerFactory, classe) \
 \
HRESULT __stdcall  TIEDNBIARobControllerFactory##classe::CreateGenericToolProfile(DNBIAGenericToolProfile *& oToolProfile) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&oToolProfile); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateGenericToolProfile(oToolProfile); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&oToolProfile); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobControllerFactory##classe::CreateGenericMotionProfile(DNBIAGenericMotionProfile *& oMotionProfile) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&oMotionProfile); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateGenericMotionProfile(oMotionProfile); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&oMotionProfile); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobControllerFactory##classe::CreateGenericAccuracyProfile(DNBIAGenericAccuracyProfile *& oAccuracyProfile) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&oAccuracyProfile); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateGenericAccuracyProfile(oAccuracyProfile); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&oAccuracyProfile); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobControllerFactory##classe::CreateGenericObjFrameProfile(DNBIAGenericObjFrameProfile *& oObjFrameProfile) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&oObjFrameProfile); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateGenericObjFrameProfile(oObjFrameProfile); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&oObjFrameProfile); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIARobControllerFactory##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIARobControllerFactory##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIARobControllerFactory##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIARobControllerFactory##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIARobControllerFactory##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIARobControllerFactory(classe) \
 \
 \
declare_TIE_DNBIARobControllerFactory(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIARobControllerFactory##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIARobControllerFactory,"DNBIARobControllerFactory",DNBIARobControllerFactory::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIARobControllerFactory(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIARobControllerFactory, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIARobControllerFactory##classe(classe::MetaObject(),DNBIARobControllerFactory::MetaObject(),(void *)CreateTIEDNBIARobControllerFactory##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIARobControllerFactory(classe) \
 \
 \
declare_TIE_DNBIARobControllerFactory(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIARobControllerFactory##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIARobControllerFactory,"DNBIARobControllerFactory",DNBIARobControllerFactory::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIARobControllerFactory(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIARobControllerFactory, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIARobControllerFactory##classe(classe::MetaObject(),DNBIARobControllerFactory::MetaObject(),(void *)CreateTIEDNBIARobControllerFactory##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIARobControllerFactory(classe) TIE_DNBIARobControllerFactory(classe)
#else
#define BOA_DNBIARobControllerFactory(classe) CATImplementBOA(DNBIARobControllerFactory, classe)
#endif

#endif
