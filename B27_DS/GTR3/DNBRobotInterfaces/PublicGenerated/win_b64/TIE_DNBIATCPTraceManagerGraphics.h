#ifndef __TIE_DNBIATCPTraceManagerGraphics
#define __TIE_DNBIATCPTraceManagerGraphics

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIATCPTraceManagerGraphics.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIATCPTraceManagerGraphics */
#define declare_TIE_DNBIATCPTraceManagerGraphics(classe) \
 \
 \
class TIEDNBIATCPTraceManagerGraphics##classe : public DNBIATCPTraceManagerGraphics \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIATCPTraceManagerGraphics, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall SetTraceNextColor(DNBTCPTraceReps iRep, CATLONG iR, CATLONG iG, CATLONG iB, CATLONG iA); \
      virtual HRESULT __stdcall SetNextType(DNBTCPTraceReps iRep, CATLONG iType); \
      virtual HRESULT __stdcall SetNextThickness(DNBTCPTraceReps iRep, CATLONG iThickness); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIATCPTraceManagerGraphics(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall SetTraceNextColor(DNBTCPTraceReps iRep, CATLONG iR, CATLONG iG, CATLONG iB, CATLONG iA); \
virtual HRESULT __stdcall SetNextType(DNBTCPTraceReps iRep, CATLONG iType); \
virtual HRESULT __stdcall SetNextThickness(DNBTCPTraceReps iRep, CATLONG iThickness); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIATCPTraceManagerGraphics(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::SetTraceNextColor(DNBTCPTraceReps iRep, CATLONG iR, CATLONG iG, CATLONG iB, CATLONG iA) \
{ \
return (ENVTIECALL(DNBIATCPTraceManagerGraphics,ENVTIETypeLetter,ENVTIELetter)SetTraceNextColor(iRep,iR,iG,iB,iA)); \
} \
HRESULT __stdcall  ENVTIEName::SetNextType(DNBTCPTraceReps iRep, CATLONG iType) \
{ \
return (ENVTIECALL(DNBIATCPTraceManagerGraphics,ENVTIETypeLetter,ENVTIELetter)SetNextType(iRep,iType)); \
} \
HRESULT __stdcall  ENVTIEName::SetNextThickness(DNBTCPTraceReps iRep, CATLONG iThickness) \
{ \
return (ENVTIECALL(DNBIATCPTraceManagerGraphics,ENVTIETypeLetter,ENVTIELetter)SetNextThickness(iRep,iThickness)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIATCPTraceManagerGraphics,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIATCPTraceManagerGraphics,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIATCPTraceManagerGraphics,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIATCPTraceManagerGraphics,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIATCPTraceManagerGraphics,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIATCPTraceManagerGraphics(classe)    TIEDNBIATCPTraceManagerGraphics##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIATCPTraceManagerGraphics(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIATCPTraceManagerGraphics, classe) \
 \
 \
CATImplementTIEMethods(DNBIATCPTraceManagerGraphics, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIATCPTraceManagerGraphics, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIATCPTraceManagerGraphics, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIATCPTraceManagerGraphics, classe) \
 \
HRESULT __stdcall  TIEDNBIATCPTraceManagerGraphics##classe::SetTraceNextColor(DNBTCPTraceReps iRep, CATLONG iR, CATLONG iG, CATLONG iB, CATLONG iA) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&iRep,&iR,&iG,&iB,&iA); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetTraceNextColor(iRep,iR,iG,iB,iA); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&iRep,&iR,&iG,&iB,&iA); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIATCPTraceManagerGraphics##classe::SetNextType(DNBTCPTraceReps iRep, CATLONG iType) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&iRep,&iType); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetNextType(iRep,iType); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&iRep,&iType); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIATCPTraceManagerGraphics##classe::SetNextThickness(DNBTCPTraceReps iRep, CATLONG iThickness) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&iRep,&iThickness); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetNextThickness(iRep,iThickness); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&iRep,&iThickness); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIATCPTraceManagerGraphics##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIATCPTraceManagerGraphics##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIATCPTraceManagerGraphics##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIATCPTraceManagerGraphics##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIATCPTraceManagerGraphics##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIATCPTraceManagerGraphics(classe) \
 \
 \
declare_TIE_DNBIATCPTraceManagerGraphics(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIATCPTraceManagerGraphics##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIATCPTraceManagerGraphics,"DNBIATCPTraceManagerGraphics",DNBIATCPTraceManagerGraphics::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIATCPTraceManagerGraphics(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIATCPTraceManagerGraphics, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIATCPTraceManagerGraphics##classe(classe::MetaObject(),DNBIATCPTraceManagerGraphics::MetaObject(),(void *)CreateTIEDNBIATCPTraceManagerGraphics##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIATCPTraceManagerGraphics(classe) \
 \
 \
declare_TIE_DNBIATCPTraceManagerGraphics(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIATCPTraceManagerGraphics##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIATCPTraceManagerGraphics,"DNBIATCPTraceManagerGraphics",DNBIATCPTraceManagerGraphics::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIATCPTraceManagerGraphics(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIATCPTraceManagerGraphics, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIATCPTraceManagerGraphics##classe(classe::MetaObject(),DNBIATCPTraceManagerGraphics::MetaObject(),(void *)CreateTIEDNBIATCPTraceManagerGraphics##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIATCPTraceManagerGraphics(classe) TIE_DNBIATCPTraceManagerGraphics(classe)
#else
#define BOA_DNBIATCPTraceManagerGraphics(classe) CATImplementBOA(DNBIATCPTraceManagerGraphics, classe)
#endif

#endif
