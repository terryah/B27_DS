#ifndef __TIE_DNBIAAuxDevicesMgt
#define __TIE_DNBIAAuxDevicesMgt

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIAAuxDevicesMgt.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIAAuxDevicesMgt */
#define declare_TIE_DNBIAAuxDevicesMgt(classe) \
 \
 \
class TIEDNBIAAuxDevicesMgt##classe : public DNBIAAuxDevicesMgt \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIAAuxDevicesMgt, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall DefineAuxDevices(CATIABase * iAuxDeviceObj, DNBAuxilliaryDeviceType iAuxDeviceType); \
      virtual HRESULT __stdcall GetNbAuxDevices(CATLONG & nbAuxDevices); \
      virtual HRESULT __stdcall GetAllAuxDevices(CATSafeArrayVariant *& oAuxDeviceList); \
      virtual HRESULT __stdcall GetAuxDevices(CATLONG iAuxDeviceNum, CATIABase *& oAuxDevice); \
      virtual HRESULT __stdcall GetAuxDevicesType(CATLONG iAuxDeviceNum, DNBAuxilliaryDeviceType & oAuxDeviceType); \
      virtual HRESULT __stdcall RemoveAll(); \
      virtual HRESULT __stdcall RemoveAuxDevicesByPosition(CATLONG iAuxDeviceNum); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIAAuxDevicesMgt(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall DefineAuxDevices(CATIABase * iAuxDeviceObj, DNBAuxilliaryDeviceType iAuxDeviceType); \
virtual HRESULT __stdcall GetNbAuxDevices(CATLONG & nbAuxDevices); \
virtual HRESULT __stdcall GetAllAuxDevices(CATSafeArrayVariant *& oAuxDeviceList); \
virtual HRESULT __stdcall GetAuxDevices(CATLONG iAuxDeviceNum, CATIABase *& oAuxDevice); \
virtual HRESULT __stdcall GetAuxDevicesType(CATLONG iAuxDeviceNum, DNBAuxilliaryDeviceType & oAuxDeviceType); \
virtual HRESULT __stdcall RemoveAll(); \
virtual HRESULT __stdcall RemoveAuxDevicesByPosition(CATLONG iAuxDeviceNum); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIAAuxDevicesMgt(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::DefineAuxDevices(CATIABase * iAuxDeviceObj, DNBAuxilliaryDeviceType iAuxDeviceType) \
{ \
return (ENVTIECALL(DNBIAAuxDevicesMgt,ENVTIETypeLetter,ENVTIELetter)DefineAuxDevices(iAuxDeviceObj,iAuxDeviceType)); \
} \
HRESULT __stdcall  ENVTIEName::GetNbAuxDevices(CATLONG & nbAuxDevices) \
{ \
return (ENVTIECALL(DNBIAAuxDevicesMgt,ENVTIETypeLetter,ENVTIELetter)GetNbAuxDevices(nbAuxDevices)); \
} \
HRESULT __stdcall  ENVTIEName::GetAllAuxDevices(CATSafeArrayVariant *& oAuxDeviceList) \
{ \
return (ENVTIECALL(DNBIAAuxDevicesMgt,ENVTIETypeLetter,ENVTIELetter)GetAllAuxDevices(oAuxDeviceList)); \
} \
HRESULT __stdcall  ENVTIEName::GetAuxDevices(CATLONG iAuxDeviceNum, CATIABase *& oAuxDevice) \
{ \
return (ENVTIECALL(DNBIAAuxDevicesMgt,ENVTIETypeLetter,ENVTIELetter)GetAuxDevices(iAuxDeviceNum,oAuxDevice)); \
} \
HRESULT __stdcall  ENVTIEName::GetAuxDevicesType(CATLONG iAuxDeviceNum, DNBAuxilliaryDeviceType & oAuxDeviceType) \
{ \
return (ENVTIECALL(DNBIAAuxDevicesMgt,ENVTIETypeLetter,ENVTIELetter)GetAuxDevicesType(iAuxDeviceNum,oAuxDeviceType)); \
} \
HRESULT __stdcall  ENVTIEName::RemoveAll() \
{ \
return (ENVTIECALL(DNBIAAuxDevicesMgt,ENVTIETypeLetter,ENVTIELetter)RemoveAll()); \
} \
HRESULT __stdcall  ENVTIEName::RemoveAuxDevicesByPosition(CATLONG iAuxDeviceNum) \
{ \
return (ENVTIECALL(DNBIAAuxDevicesMgt,ENVTIETypeLetter,ENVTIELetter)RemoveAuxDevicesByPosition(iAuxDeviceNum)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIAAuxDevicesMgt,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIAAuxDevicesMgt,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIAAuxDevicesMgt,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIAAuxDevicesMgt,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIAAuxDevicesMgt,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIAAuxDevicesMgt(classe)    TIEDNBIAAuxDevicesMgt##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIAAuxDevicesMgt(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIAAuxDevicesMgt, classe) \
 \
 \
CATImplementTIEMethods(DNBIAAuxDevicesMgt, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIAAuxDevicesMgt, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIAAuxDevicesMgt, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIAAuxDevicesMgt, classe) \
 \
HRESULT __stdcall  TIEDNBIAAuxDevicesMgt##classe::DefineAuxDevices(CATIABase * iAuxDeviceObj, DNBAuxilliaryDeviceType iAuxDeviceType) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&iAuxDeviceObj,&iAuxDeviceType); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->DefineAuxDevices(iAuxDeviceObj,iAuxDeviceType); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&iAuxDeviceObj,&iAuxDeviceType); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAuxDevicesMgt##classe::GetNbAuxDevices(CATLONG & nbAuxDevices) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&nbAuxDevices); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetNbAuxDevices(nbAuxDevices); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&nbAuxDevices); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAuxDevicesMgt##classe::GetAllAuxDevices(CATSafeArrayVariant *& oAuxDeviceList) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&oAuxDeviceList); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAllAuxDevices(oAuxDeviceList); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&oAuxDeviceList); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAuxDevicesMgt##classe::GetAuxDevices(CATLONG iAuxDeviceNum, CATIABase *& oAuxDevice) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&iAuxDeviceNum,&oAuxDevice); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAuxDevices(iAuxDeviceNum,oAuxDevice); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&iAuxDeviceNum,&oAuxDevice); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAuxDevicesMgt##classe::GetAuxDevicesType(CATLONG iAuxDeviceNum, DNBAuxilliaryDeviceType & oAuxDeviceType) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&iAuxDeviceNum,&oAuxDeviceType); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAuxDevicesType(iAuxDeviceNum,oAuxDeviceType); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&iAuxDeviceNum,&oAuxDeviceType); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAuxDevicesMgt##classe::RemoveAll() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->RemoveAll(); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAuxDevicesMgt##classe::RemoveAuxDevicesByPosition(CATLONG iAuxDeviceNum) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&iAuxDeviceNum); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->RemoveAuxDevicesByPosition(iAuxDeviceNum); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&iAuxDeviceNum); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAAuxDevicesMgt##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAAuxDevicesMgt##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAAuxDevicesMgt##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAAuxDevicesMgt##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,11,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,11,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAAuxDevicesMgt##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,12,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,12,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIAAuxDevicesMgt(classe) \
 \
 \
declare_TIE_DNBIAAuxDevicesMgt(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAAuxDevicesMgt##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAAuxDevicesMgt,"DNBIAAuxDevicesMgt",DNBIAAuxDevicesMgt::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAAuxDevicesMgt(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIAAuxDevicesMgt, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAAuxDevicesMgt##classe(classe::MetaObject(),DNBIAAuxDevicesMgt::MetaObject(),(void *)CreateTIEDNBIAAuxDevicesMgt##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIAAuxDevicesMgt(classe) \
 \
 \
declare_TIE_DNBIAAuxDevicesMgt(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAAuxDevicesMgt##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAAuxDevicesMgt,"DNBIAAuxDevicesMgt",DNBIAAuxDevicesMgt::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAAuxDevicesMgt(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIAAuxDevicesMgt, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAAuxDevicesMgt##classe(classe::MetaObject(),DNBIAAuxDevicesMgt::MetaObject(),(void *)CreateTIEDNBIAAuxDevicesMgt##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIAAuxDevicesMgt(classe) TIE_DNBIAAuxDevicesMgt(classe)
#else
#define BOA_DNBIAAuxDevicesMgt(classe) CATImplementBOA(DNBIAAuxDevicesMgt, classe)
#endif

#endif
