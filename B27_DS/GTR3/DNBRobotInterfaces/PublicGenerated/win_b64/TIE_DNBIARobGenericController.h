#ifndef __TIE_DNBIARobGenericController
#define __TIE_DNBIARobGenericController

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIARobGenericController.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIARobGenericController */
#define declare_TIE_DNBIARobGenericController(classe) \
 \
 \
class TIEDNBIARobGenericController##classe : public DNBIARobGenericController \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIARobGenericController, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall SetName(const CATBSTR & name); \
      virtual HRESULT __stdcall GetName(CATBSTR & oName); \
      virtual HRESULT __stdcall AddToolProfile(DNBIAGenericToolProfile * iProfile); \
      virtual HRESULT __stdcall GetToolProfile(const CATBSTR & name, DNBIAGenericToolProfile *& profile); \
      virtual HRESULT __stdcall GetToolProfiles(CATSafeArrayVariant & profiles); \
      virtual HRESULT __stdcall HasToolProfile(const CATBSTR & name, CAT_VARIANT_BOOL & flag); \
      virtual HRESULT __stdcall RemoveToolProfile(const CATBSTR & name); \
      virtual HRESULT __stdcall RemoveToolProfiles(); \
      virtual HRESULT __stdcall SetCurrentToolProfile(const CATBSTR & profile); \
      virtual HRESULT __stdcall GetCurrentToolProfile(CATBSTR & profile); \
      virtual HRESULT __stdcall GetToolProfileCount(CATLONG & count); \
      virtual HRESULT __stdcall AddMotionProfile(DNBIAGenericMotionProfile * profile); \
      virtual HRESULT __stdcall GetMotionProfile(CATBSTR & name, DNBIAGenericMotionProfile *& profile); \
      virtual HRESULT __stdcall GetMotionProfiles(CATSafeArrayVariant & profiles); \
      virtual HRESULT __stdcall HasMotionProfile(const CATBSTR & name, CAT_VARIANT_BOOL & flag); \
      virtual HRESULT __stdcall RemoveMotionProfile(const CATBSTR & name); \
      virtual HRESULT __stdcall RemoveMotionProfiles(); \
      virtual HRESULT __stdcall SetCurrentMotionProfile(const CATBSTR & profile); \
      virtual HRESULT __stdcall GetCurrentMotionProfile(CATBSTR & profile); \
      virtual HRESULT __stdcall GetMotionProfileCount(CATLONG & count); \
      virtual HRESULT __stdcall AddAccuracyProfile(DNBIAGenericAccuracyProfile * profile); \
      virtual HRESULT __stdcall GetAccuracyProfile(CATBSTR & name, DNBIAGenericAccuracyProfile *& profile); \
      virtual HRESULT __stdcall GetAccuracyProfiles(CATSafeArrayVariant & profiles); \
      virtual HRESULT __stdcall HasAccuracyProfile(const CATBSTR & name, CAT_VARIANT_BOOL & flag); \
      virtual HRESULT __stdcall RemoveAccuracyProfile(const CATBSTR & name); \
      virtual HRESULT __stdcall RemoveAccuracyProfiles(); \
      virtual HRESULT __stdcall SetCurrentAccuracyProfile(const CATBSTR & profile); \
      virtual HRESULT __stdcall GetCurrentAccuracyProfile(CATBSTR & profile); \
      virtual HRESULT __stdcall GetAccuracyProfileCount(CATLONG & count); \
      virtual HRESULT __stdcall AddObjFrameProfile(DNBIAGenericObjFrameProfile * profile); \
      virtual HRESULT __stdcall GetObjFrameProfile(CATBSTR & name, DNBIAGenericObjFrameProfile *& profile); \
      virtual HRESULT __stdcall GetObjFrameProfiles(CATSafeArrayVariant & profiles); \
      virtual HRESULT __stdcall HasObjFrameProfile(const CATBSTR & name, CAT_VARIANT_BOOL & flag); \
      virtual HRESULT __stdcall RemoveObjFrameProfile(const CATBSTR & name); \
      virtual HRESULT __stdcall RemoveObjFrameProfiles(); \
      virtual HRESULT __stdcall SetCurrentObjFrameProfile(const CATBSTR & profile); \
      virtual HRESULT __stdcall GetCurrentObjFrameProfile(CATBSTR & profile); \
      virtual HRESULT __stdcall GetObjFrameProfileCount(CATLONG & count); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIARobGenericController(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall SetName(const CATBSTR & name); \
virtual HRESULT __stdcall GetName(CATBSTR & oName); \
virtual HRESULT __stdcall AddToolProfile(DNBIAGenericToolProfile * iProfile); \
virtual HRESULT __stdcall GetToolProfile(const CATBSTR & name, DNBIAGenericToolProfile *& profile); \
virtual HRESULT __stdcall GetToolProfiles(CATSafeArrayVariant & profiles); \
virtual HRESULT __stdcall HasToolProfile(const CATBSTR & name, CAT_VARIANT_BOOL & flag); \
virtual HRESULT __stdcall RemoveToolProfile(const CATBSTR & name); \
virtual HRESULT __stdcall RemoveToolProfiles(); \
virtual HRESULT __stdcall SetCurrentToolProfile(const CATBSTR & profile); \
virtual HRESULT __stdcall GetCurrentToolProfile(CATBSTR & profile); \
virtual HRESULT __stdcall GetToolProfileCount(CATLONG & count); \
virtual HRESULT __stdcall AddMotionProfile(DNBIAGenericMotionProfile * profile); \
virtual HRESULT __stdcall GetMotionProfile(CATBSTR & name, DNBIAGenericMotionProfile *& profile); \
virtual HRESULT __stdcall GetMotionProfiles(CATSafeArrayVariant & profiles); \
virtual HRESULT __stdcall HasMotionProfile(const CATBSTR & name, CAT_VARIANT_BOOL & flag); \
virtual HRESULT __stdcall RemoveMotionProfile(const CATBSTR & name); \
virtual HRESULT __stdcall RemoveMotionProfiles(); \
virtual HRESULT __stdcall SetCurrentMotionProfile(const CATBSTR & profile); \
virtual HRESULT __stdcall GetCurrentMotionProfile(CATBSTR & profile); \
virtual HRESULT __stdcall GetMotionProfileCount(CATLONG & count); \
virtual HRESULT __stdcall AddAccuracyProfile(DNBIAGenericAccuracyProfile * profile); \
virtual HRESULT __stdcall GetAccuracyProfile(CATBSTR & name, DNBIAGenericAccuracyProfile *& profile); \
virtual HRESULT __stdcall GetAccuracyProfiles(CATSafeArrayVariant & profiles); \
virtual HRESULT __stdcall HasAccuracyProfile(const CATBSTR & name, CAT_VARIANT_BOOL & flag); \
virtual HRESULT __stdcall RemoveAccuracyProfile(const CATBSTR & name); \
virtual HRESULT __stdcall RemoveAccuracyProfiles(); \
virtual HRESULT __stdcall SetCurrentAccuracyProfile(const CATBSTR & profile); \
virtual HRESULT __stdcall GetCurrentAccuracyProfile(CATBSTR & profile); \
virtual HRESULT __stdcall GetAccuracyProfileCount(CATLONG & count); \
virtual HRESULT __stdcall AddObjFrameProfile(DNBIAGenericObjFrameProfile * profile); \
virtual HRESULT __stdcall GetObjFrameProfile(CATBSTR & name, DNBIAGenericObjFrameProfile *& profile); \
virtual HRESULT __stdcall GetObjFrameProfiles(CATSafeArrayVariant & profiles); \
virtual HRESULT __stdcall HasObjFrameProfile(const CATBSTR & name, CAT_VARIANT_BOOL & flag); \
virtual HRESULT __stdcall RemoveObjFrameProfile(const CATBSTR & name); \
virtual HRESULT __stdcall RemoveObjFrameProfiles(); \
virtual HRESULT __stdcall SetCurrentObjFrameProfile(const CATBSTR & profile); \
virtual HRESULT __stdcall GetCurrentObjFrameProfile(CATBSTR & profile); \
virtual HRESULT __stdcall GetObjFrameProfileCount(CATLONG & count); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIARobGenericController(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::SetName(const CATBSTR & name) \
{ \
return (ENVTIECALL(DNBIARobGenericController,ENVTIETypeLetter,ENVTIELetter)SetName(name)); \
} \
HRESULT __stdcall  ENVTIEName::GetName(CATBSTR & oName) \
{ \
return (ENVTIECALL(DNBIARobGenericController,ENVTIETypeLetter,ENVTIELetter)GetName(oName)); \
} \
HRESULT __stdcall  ENVTIEName::AddToolProfile(DNBIAGenericToolProfile * iProfile) \
{ \
return (ENVTIECALL(DNBIARobGenericController,ENVTIETypeLetter,ENVTIELetter)AddToolProfile(iProfile)); \
} \
HRESULT __stdcall  ENVTIEName::GetToolProfile(const CATBSTR & name, DNBIAGenericToolProfile *& profile) \
{ \
return (ENVTIECALL(DNBIARobGenericController,ENVTIETypeLetter,ENVTIELetter)GetToolProfile(name,profile)); \
} \
HRESULT __stdcall  ENVTIEName::GetToolProfiles(CATSafeArrayVariant & profiles) \
{ \
return (ENVTIECALL(DNBIARobGenericController,ENVTIETypeLetter,ENVTIELetter)GetToolProfiles(profiles)); \
} \
HRESULT __stdcall  ENVTIEName::HasToolProfile(const CATBSTR & name, CAT_VARIANT_BOOL & flag) \
{ \
return (ENVTIECALL(DNBIARobGenericController,ENVTIETypeLetter,ENVTIELetter)HasToolProfile(name,flag)); \
} \
HRESULT __stdcall  ENVTIEName::RemoveToolProfile(const CATBSTR & name) \
{ \
return (ENVTIECALL(DNBIARobGenericController,ENVTIETypeLetter,ENVTIELetter)RemoveToolProfile(name)); \
} \
HRESULT __stdcall  ENVTIEName::RemoveToolProfiles() \
{ \
return (ENVTIECALL(DNBIARobGenericController,ENVTIETypeLetter,ENVTIELetter)RemoveToolProfiles()); \
} \
HRESULT __stdcall  ENVTIEName::SetCurrentToolProfile(const CATBSTR & profile) \
{ \
return (ENVTIECALL(DNBIARobGenericController,ENVTIETypeLetter,ENVTIELetter)SetCurrentToolProfile(profile)); \
} \
HRESULT __stdcall  ENVTIEName::GetCurrentToolProfile(CATBSTR & profile) \
{ \
return (ENVTIECALL(DNBIARobGenericController,ENVTIETypeLetter,ENVTIELetter)GetCurrentToolProfile(profile)); \
} \
HRESULT __stdcall  ENVTIEName::GetToolProfileCount(CATLONG & count) \
{ \
return (ENVTIECALL(DNBIARobGenericController,ENVTIETypeLetter,ENVTIELetter)GetToolProfileCount(count)); \
} \
HRESULT __stdcall  ENVTIEName::AddMotionProfile(DNBIAGenericMotionProfile * profile) \
{ \
return (ENVTIECALL(DNBIARobGenericController,ENVTIETypeLetter,ENVTIELetter)AddMotionProfile(profile)); \
} \
HRESULT __stdcall  ENVTIEName::GetMotionProfile(CATBSTR & name, DNBIAGenericMotionProfile *& profile) \
{ \
return (ENVTIECALL(DNBIARobGenericController,ENVTIETypeLetter,ENVTIELetter)GetMotionProfile(name,profile)); \
} \
HRESULT __stdcall  ENVTIEName::GetMotionProfiles(CATSafeArrayVariant & profiles) \
{ \
return (ENVTIECALL(DNBIARobGenericController,ENVTIETypeLetter,ENVTIELetter)GetMotionProfiles(profiles)); \
} \
HRESULT __stdcall  ENVTIEName::HasMotionProfile(const CATBSTR & name, CAT_VARIANT_BOOL & flag) \
{ \
return (ENVTIECALL(DNBIARobGenericController,ENVTIETypeLetter,ENVTIELetter)HasMotionProfile(name,flag)); \
} \
HRESULT __stdcall  ENVTIEName::RemoveMotionProfile(const CATBSTR & name) \
{ \
return (ENVTIECALL(DNBIARobGenericController,ENVTIETypeLetter,ENVTIELetter)RemoveMotionProfile(name)); \
} \
HRESULT __stdcall  ENVTIEName::RemoveMotionProfiles() \
{ \
return (ENVTIECALL(DNBIARobGenericController,ENVTIETypeLetter,ENVTIELetter)RemoveMotionProfiles()); \
} \
HRESULT __stdcall  ENVTIEName::SetCurrentMotionProfile(const CATBSTR & profile) \
{ \
return (ENVTIECALL(DNBIARobGenericController,ENVTIETypeLetter,ENVTIELetter)SetCurrentMotionProfile(profile)); \
} \
HRESULT __stdcall  ENVTIEName::GetCurrentMotionProfile(CATBSTR & profile) \
{ \
return (ENVTIECALL(DNBIARobGenericController,ENVTIETypeLetter,ENVTIELetter)GetCurrentMotionProfile(profile)); \
} \
HRESULT __stdcall  ENVTIEName::GetMotionProfileCount(CATLONG & count) \
{ \
return (ENVTIECALL(DNBIARobGenericController,ENVTIETypeLetter,ENVTIELetter)GetMotionProfileCount(count)); \
} \
HRESULT __stdcall  ENVTIEName::AddAccuracyProfile(DNBIAGenericAccuracyProfile * profile) \
{ \
return (ENVTIECALL(DNBIARobGenericController,ENVTIETypeLetter,ENVTIELetter)AddAccuracyProfile(profile)); \
} \
HRESULT __stdcall  ENVTIEName::GetAccuracyProfile(CATBSTR & name, DNBIAGenericAccuracyProfile *& profile) \
{ \
return (ENVTIECALL(DNBIARobGenericController,ENVTIETypeLetter,ENVTIELetter)GetAccuracyProfile(name,profile)); \
} \
HRESULT __stdcall  ENVTIEName::GetAccuracyProfiles(CATSafeArrayVariant & profiles) \
{ \
return (ENVTIECALL(DNBIARobGenericController,ENVTIETypeLetter,ENVTIELetter)GetAccuracyProfiles(profiles)); \
} \
HRESULT __stdcall  ENVTIEName::HasAccuracyProfile(const CATBSTR & name, CAT_VARIANT_BOOL & flag) \
{ \
return (ENVTIECALL(DNBIARobGenericController,ENVTIETypeLetter,ENVTIELetter)HasAccuracyProfile(name,flag)); \
} \
HRESULT __stdcall  ENVTIEName::RemoveAccuracyProfile(const CATBSTR & name) \
{ \
return (ENVTIECALL(DNBIARobGenericController,ENVTIETypeLetter,ENVTIELetter)RemoveAccuracyProfile(name)); \
} \
HRESULT __stdcall  ENVTIEName::RemoveAccuracyProfiles() \
{ \
return (ENVTIECALL(DNBIARobGenericController,ENVTIETypeLetter,ENVTIELetter)RemoveAccuracyProfiles()); \
} \
HRESULT __stdcall  ENVTIEName::SetCurrentAccuracyProfile(const CATBSTR & profile) \
{ \
return (ENVTIECALL(DNBIARobGenericController,ENVTIETypeLetter,ENVTIELetter)SetCurrentAccuracyProfile(profile)); \
} \
HRESULT __stdcall  ENVTIEName::GetCurrentAccuracyProfile(CATBSTR & profile) \
{ \
return (ENVTIECALL(DNBIARobGenericController,ENVTIETypeLetter,ENVTIELetter)GetCurrentAccuracyProfile(profile)); \
} \
HRESULT __stdcall  ENVTIEName::GetAccuracyProfileCount(CATLONG & count) \
{ \
return (ENVTIECALL(DNBIARobGenericController,ENVTIETypeLetter,ENVTIELetter)GetAccuracyProfileCount(count)); \
} \
HRESULT __stdcall  ENVTIEName::AddObjFrameProfile(DNBIAGenericObjFrameProfile * profile) \
{ \
return (ENVTIECALL(DNBIARobGenericController,ENVTIETypeLetter,ENVTIELetter)AddObjFrameProfile(profile)); \
} \
HRESULT __stdcall  ENVTIEName::GetObjFrameProfile(CATBSTR & name, DNBIAGenericObjFrameProfile *& profile) \
{ \
return (ENVTIECALL(DNBIARobGenericController,ENVTIETypeLetter,ENVTIELetter)GetObjFrameProfile(name,profile)); \
} \
HRESULT __stdcall  ENVTIEName::GetObjFrameProfiles(CATSafeArrayVariant & profiles) \
{ \
return (ENVTIECALL(DNBIARobGenericController,ENVTIETypeLetter,ENVTIELetter)GetObjFrameProfiles(profiles)); \
} \
HRESULT __stdcall  ENVTIEName::HasObjFrameProfile(const CATBSTR & name, CAT_VARIANT_BOOL & flag) \
{ \
return (ENVTIECALL(DNBIARobGenericController,ENVTIETypeLetter,ENVTIELetter)HasObjFrameProfile(name,flag)); \
} \
HRESULT __stdcall  ENVTIEName::RemoveObjFrameProfile(const CATBSTR & name) \
{ \
return (ENVTIECALL(DNBIARobGenericController,ENVTIETypeLetter,ENVTIELetter)RemoveObjFrameProfile(name)); \
} \
HRESULT __stdcall  ENVTIEName::RemoveObjFrameProfiles() \
{ \
return (ENVTIECALL(DNBIARobGenericController,ENVTIETypeLetter,ENVTIELetter)RemoveObjFrameProfiles()); \
} \
HRESULT __stdcall  ENVTIEName::SetCurrentObjFrameProfile(const CATBSTR & profile) \
{ \
return (ENVTIECALL(DNBIARobGenericController,ENVTIETypeLetter,ENVTIELetter)SetCurrentObjFrameProfile(profile)); \
} \
HRESULT __stdcall  ENVTIEName::GetCurrentObjFrameProfile(CATBSTR & profile) \
{ \
return (ENVTIECALL(DNBIARobGenericController,ENVTIETypeLetter,ENVTIELetter)GetCurrentObjFrameProfile(profile)); \
} \
HRESULT __stdcall  ENVTIEName::GetObjFrameProfileCount(CATLONG & count) \
{ \
return (ENVTIECALL(DNBIARobGenericController,ENVTIETypeLetter,ENVTIELetter)GetObjFrameProfileCount(count)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIARobGenericController,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIARobGenericController,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIARobGenericController,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIARobGenericController,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIARobGenericController,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIARobGenericController(classe)    TIEDNBIARobGenericController##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIARobGenericController(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIARobGenericController, classe) \
 \
 \
CATImplementTIEMethods(DNBIARobGenericController, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIARobGenericController, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIARobGenericController, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIARobGenericController, classe) \
 \
HRESULT __stdcall  TIEDNBIARobGenericController##classe::SetName(const CATBSTR & name) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&name); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetName(name); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&name); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobGenericController##classe::GetName(CATBSTR & oName) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&oName); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetName(oName); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&oName); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobGenericController##classe::AddToolProfile(DNBIAGenericToolProfile * iProfile) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&iProfile); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->AddToolProfile(iProfile); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&iProfile); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobGenericController##classe::GetToolProfile(const CATBSTR & name, DNBIAGenericToolProfile *& profile) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&name,&profile); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetToolProfile(name,profile); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&name,&profile); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobGenericController##classe::GetToolProfiles(CATSafeArrayVariant & profiles) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&profiles); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetToolProfiles(profiles); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&profiles); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobGenericController##classe::HasToolProfile(const CATBSTR & name, CAT_VARIANT_BOOL & flag) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&name,&flag); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->HasToolProfile(name,flag); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&name,&flag); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobGenericController##classe::RemoveToolProfile(const CATBSTR & name) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&name); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->RemoveToolProfile(name); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&name); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobGenericController##classe::RemoveToolProfiles() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->RemoveToolProfiles(); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobGenericController##classe::SetCurrentToolProfile(const CATBSTR & profile) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&profile); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetCurrentToolProfile(profile); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&profile); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobGenericController##classe::GetCurrentToolProfile(CATBSTR & profile) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&profile); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetCurrentToolProfile(profile); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&profile); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobGenericController##classe::GetToolProfileCount(CATLONG & count) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,11,&_Trac2,&count); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetToolProfileCount(count); \
   ExitAfterCall(this,11,_Trac2,&_ret_arg,&count); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobGenericController##classe::AddMotionProfile(DNBIAGenericMotionProfile * profile) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,12,&_Trac2,&profile); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->AddMotionProfile(profile); \
   ExitAfterCall(this,12,_Trac2,&_ret_arg,&profile); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobGenericController##classe::GetMotionProfile(CATBSTR & name, DNBIAGenericMotionProfile *& profile) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,13,&_Trac2,&name,&profile); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetMotionProfile(name,profile); \
   ExitAfterCall(this,13,_Trac2,&_ret_arg,&name,&profile); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobGenericController##classe::GetMotionProfiles(CATSafeArrayVariant & profiles) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,14,&_Trac2,&profiles); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetMotionProfiles(profiles); \
   ExitAfterCall(this,14,_Trac2,&_ret_arg,&profiles); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobGenericController##classe::HasMotionProfile(const CATBSTR & name, CAT_VARIANT_BOOL & flag) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,15,&_Trac2,&name,&flag); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->HasMotionProfile(name,flag); \
   ExitAfterCall(this,15,_Trac2,&_ret_arg,&name,&flag); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobGenericController##classe::RemoveMotionProfile(const CATBSTR & name) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,16,&_Trac2,&name); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->RemoveMotionProfile(name); \
   ExitAfterCall(this,16,_Trac2,&_ret_arg,&name); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobGenericController##classe::RemoveMotionProfiles() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,17,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->RemoveMotionProfiles(); \
   ExitAfterCall(this,17,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobGenericController##classe::SetCurrentMotionProfile(const CATBSTR & profile) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,18,&_Trac2,&profile); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetCurrentMotionProfile(profile); \
   ExitAfterCall(this,18,_Trac2,&_ret_arg,&profile); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobGenericController##classe::GetCurrentMotionProfile(CATBSTR & profile) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,19,&_Trac2,&profile); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetCurrentMotionProfile(profile); \
   ExitAfterCall(this,19,_Trac2,&_ret_arg,&profile); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobGenericController##classe::GetMotionProfileCount(CATLONG & count) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,20,&_Trac2,&count); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetMotionProfileCount(count); \
   ExitAfterCall(this,20,_Trac2,&_ret_arg,&count); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobGenericController##classe::AddAccuracyProfile(DNBIAGenericAccuracyProfile * profile) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,21,&_Trac2,&profile); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->AddAccuracyProfile(profile); \
   ExitAfterCall(this,21,_Trac2,&_ret_arg,&profile); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobGenericController##classe::GetAccuracyProfile(CATBSTR & name, DNBIAGenericAccuracyProfile *& profile) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,22,&_Trac2,&name,&profile); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAccuracyProfile(name,profile); \
   ExitAfterCall(this,22,_Trac2,&_ret_arg,&name,&profile); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobGenericController##classe::GetAccuracyProfiles(CATSafeArrayVariant & profiles) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,23,&_Trac2,&profiles); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAccuracyProfiles(profiles); \
   ExitAfterCall(this,23,_Trac2,&_ret_arg,&profiles); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobGenericController##classe::HasAccuracyProfile(const CATBSTR & name, CAT_VARIANT_BOOL & flag) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,24,&_Trac2,&name,&flag); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->HasAccuracyProfile(name,flag); \
   ExitAfterCall(this,24,_Trac2,&_ret_arg,&name,&flag); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobGenericController##classe::RemoveAccuracyProfile(const CATBSTR & name) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,25,&_Trac2,&name); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->RemoveAccuracyProfile(name); \
   ExitAfterCall(this,25,_Trac2,&_ret_arg,&name); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobGenericController##classe::RemoveAccuracyProfiles() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,26,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->RemoveAccuracyProfiles(); \
   ExitAfterCall(this,26,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobGenericController##classe::SetCurrentAccuracyProfile(const CATBSTR & profile) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,27,&_Trac2,&profile); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetCurrentAccuracyProfile(profile); \
   ExitAfterCall(this,27,_Trac2,&_ret_arg,&profile); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobGenericController##classe::GetCurrentAccuracyProfile(CATBSTR & profile) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,28,&_Trac2,&profile); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetCurrentAccuracyProfile(profile); \
   ExitAfterCall(this,28,_Trac2,&_ret_arg,&profile); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobGenericController##classe::GetAccuracyProfileCount(CATLONG & count) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,29,&_Trac2,&count); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAccuracyProfileCount(count); \
   ExitAfterCall(this,29,_Trac2,&_ret_arg,&count); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobGenericController##classe::AddObjFrameProfile(DNBIAGenericObjFrameProfile * profile) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,30,&_Trac2,&profile); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->AddObjFrameProfile(profile); \
   ExitAfterCall(this,30,_Trac2,&_ret_arg,&profile); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobGenericController##classe::GetObjFrameProfile(CATBSTR & name, DNBIAGenericObjFrameProfile *& profile) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,31,&_Trac2,&name,&profile); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetObjFrameProfile(name,profile); \
   ExitAfterCall(this,31,_Trac2,&_ret_arg,&name,&profile); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobGenericController##classe::GetObjFrameProfiles(CATSafeArrayVariant & profiles) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,32,&_Trac2,&profiles); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetObjFrameProfiles(profiles); \
   ExitAfterCall(this,32,_Trac2,&_ret_arg,&profiles); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobGenericController##classe::HasObjFrameProfile(const CATBSTR & name, CAT_VARIANT_BOOL & flag) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,33,&_Trac2,&name,&flag); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->HasObjFrameProfile(name,flag); \
   ExitAfterCall(this,33,_Trac2,&_ret_arg,&name,&flag); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobGenericController##classe::RemoveObjFrameProfile(const CATBSTR & name) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,34,&_Trac2,&name); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->RemoveObjFrameProfile(name); \
   ExitAfterCall(this,34,_Trac2,&_ret_arg,&name); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobGenericController##classe::RemoveObjFrameProfiles() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,35,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->RemoveObjFrameProfiles(); \
   ExitAfterCall(this,35,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobGenericController##classe::SetCurrentObjFrameProfile(const CATBSTR & profile) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,36,&_Trac2,&profile); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetCurrentObjFrameProfile(profile); \
   ExitAfterCall(this,36,_Trac2,&_ret_arg,&profile); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobGenericController##classe::GetCurrentObjFrameProfile(CATBSTR & profile) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,37,&_Trac2,&profile); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetCurrentObjFrameProfile(profile); \
   ExitAfterCall(this,37,_Trac2,&_ret_arg,&profile); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobGenericController##classe::GetObjFrameProfileCount(CATLONG & count) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,38,&_Trac2,&count); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetObjFrameProfileCount(count); \
   ExitAfterCall(this,38,_Trac2,&_ret_arg,&count); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIARobGenericController##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,39,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,39,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIARobGenericController##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,40,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,40,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIARobGenericController##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,41,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,41,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIARobGenericController##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,42,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,42,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIARobGenericController##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,43,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,43,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIARobGenericController(classe) \
 \
 \
declare_TIE_DNBIARobGenericController(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIARobGenericController##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIARobGenericController,"DNBIARobGenericController",DNBIARobGenericController::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIARobGenericController(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIARobGenericController, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIARobGenericController##classe(classe::MetaObject(),DNBIARobGenericController::MetaObject(),(void *)CreateTIEDNBIARobGenericController##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIARobGenericController(classe) \
 \
 \
declare_TIE_DNBIARobGenericController(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIARobGenericController##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIARobGenericController,"DNBIARobGenericController",DNBIARobGenericController::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIARobGenericController(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIARobGenericController, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIARobGenericController##classe(classe::MetaObject(),DNBIARobGenericController::MetaObject(),(void *)CreateTIEDNBIARobGenericController##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIARobGenericController(classe) TIE_DNBIARobGenericController(classe)
#else
#define BOA_DNBIARobGenericController(classe) CATImplementBOA(DNBIARobGenericController, classe)
#endif

#endif
