#ifndef __TIE_DNBIAGenericMotionProfile
#define __TIE_DNBIAGenericMotionProfile

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIAGenericMotionProfile.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIAGenericMotionProfile */
#define declare_TIE_DNBIAGenericMotionProfile(classe) \
 \
 \
class TIEDNBIAGenericMotionProfile##classe : public DNBIAGenericMotionProfile \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIAGenericMotionProfile, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall SetName(const CATBSTR & iName); \
      virtual HRESULT __stdcall GetName(CATBSTR & oName); \
      virtual HRESULT __stdcall SetMotionBasis(MotionBasis basis); \
      virtual HRESULT __stdcall GetMotionBasis(MotionBasis & basis); \
      virtual HRESULT __stdcall SetSpeedValue(double value); \
      virtual HRESULT __stdcall GetSpeedValue(double & value); \
      virtual HRESULT __stdcall SetAccelerationValue(double value); \
      virtual HRESULT __stdcall GetAccelerationValue(double & value); \
      virtual HRESULT __stdcall SetAngularSpeedValue(double value); \
      virtual HRESULT __stdcall GetAngularSpeedValue(double & value); \
      virtual HRESULT __stdcall SetAngularAccelerationValue(double value); \
      virtual HRESULT __stdcall GetAngularAccelerationValue(double & value); \
      virtual HRESULT __stdcall GetController(DNBIARobGenericController *& oController); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIAGenericMotionProfile(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall SetName(const CATBSTR & iName); \
virtual HRESULT __stdcall GetName(CATBSTR & oName); \
virtual HRESULT __stdcall SetMotionBasis(MotionBasis basis); \
virtual HRESULT __stdcall GetMotionBasis(MotionBasis & basis); \
virtual HRESULT __stdcall SetSpeedValue(double value); \
virtual HRESULT __stdcall GetSpeedValue(double & value); \
virtual HRESULT __stdcall SetAccelerationValue(double value); \
virtual HRESULT __stdcall GetAccelerationValue(double & value); \
virtual HRESULT __stdcall SetAngularSpeedValue(double value); \
virtual HRESULT __stdcall GetAngularSpeedValue(double & value); \
virtual HRESULT __stdcall SetAngularAccelerationValue(double value); \
virtual HRESULT __stdcall GetAngularAccelerationValue(double & value); \
virtual HRESULT __stdcall GetController(DNBIARobGenericController *& oController); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIAGenericMotionProfile(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::SetName(const CATBSTR & iName) \
{ \
return (ENVTIECALL(DNBIAGenericMotionProfile,ENVTIETypeLetter,ENVTIELetter)SetName(iName)); \
} \
HRESULT __stdcall  ENVTIEName::GetName(CATBSTR & oName) \
{ \
return (ENVTIECALL(DNBIAGenericMotionProfile,ENVTIETypeLetter,ENVTIELetter)GetName(oName)); \
} \
HRESULT __stdcall  ENVTIEName::SetMotionBasis(MotionBasis basis) \
{ \
return (ENVTIECALL(DNBIAGenericMotionProfile,ENVTIETypeLetter,ENVTIELetter)SetMotionBasis(basis)); \
} \
HRESULT __stdcall  ENVTIEName::GetMotionBasis(MotionBasis & basis) \
{ \
return (ENVTIECALL(DNBIAGenericMotionProfile,ENVTIETypeLetter,ENVTIELetter)GetMotionBasis(basis)); \
} \
HRESULT __stdcall  ENVTIEName::SetSpeedValue(double value) \
{ \
return (ENVTIECALL(DNBIAGenericMotionProfile,ENVTIETypeLetter,ENVTIELetter)SetSpeedValue(value)); \
} \
HRESULT __stdcall  ENVTIEName::GetSpeedValue(double & value) \
{ \
return (ENVTIECALL(DNBIAGenericMotionProfile,ENVTIETypeLetter,ENVTIELetter)GetSpeedValue(value)); \
} \
HRESULT __stdcall  ENVTIEName::SetAccelerationValue(double value) \
{ \
return (ENVTIECALL(DNBIAGenericMotionProfile,ENVTIETypeLetter,ENVTIELetter)SetAccelerationValue(value)); \
} \
HRESULT __stdcall  ENVTIEName::GetAccelerationValue(double & value) \
{ \
return (ENVTIECALL(DNBIAGenericMotionProfile,ENVTIETypeLetter,ENVTIELetter)GetAccelerationValue(value)); \
} \
HRESULT __stdcall  ENVTIEName::SetAngularSpeedValue(double value) \
{ \
return (ENVTIECALL(DNBIAGenericMotionProfile,ENVTIETypeLetter,ENVTIELetter)SetAngularSpeedValue(value)); \
} \
HRESULT __stdcall  ENVTIEName::GetAngularSpeedValue(double & value) \
{ \
return (ENVTIECALL(DNBIAGenericMotionProfile,ENVTIETypeLetter,ENVTIELetter)GetAngularSpeedValue(value)); \
} \
HRESULT __stdcall  ENVTIEName::SetAngularAccelerationValue(double value) \
{ \
return (ENVTIECALL(DNBIAGenericMotionProfile,ENVTIETypeLetter,ENVTIELetter)SetAngularAccelerationValue(value)); \
} \
HRESULT __stdcall  ENVTIEName::GetAngularAccelerationValue(double & value) \
{ \
return (ENVTIECALL(DNBIAGenericMotionProfile,ENVTIETypeLetter,ENVTIELetter)GetAngularAccelerationValue(value)); \
} \
HRESULT __stdcall  ENVTIEName::GetController(DNBIARobGenericController *& oController) \
{ \
return (ENVTIECALL(DNBIAGenericMotionProfile,ENVTIETypeLetter,ENVTIELetter)GetController(oController)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIAGenericMotionProfile,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIAGenericMotionProfile,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIAGenericMotionProfile,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIAGenericMotionProfile,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIAGenericMotionProfile,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIAGenericMotionProfile(classe)    TIEDNBIAGenericMotionProfile##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIAGenericMotionProfile(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIAGenericMotionProfile, classe) \
 \
 \
CATImplementTIEMethods(DNBIAGenericMotionProfile, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIAGenericMotionProfile, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIAGenericMotionProfile, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIAGenericMotionProfile, classe) \
 \
HRESULT __stdcall  TIEDNBIAGenericMotionProfile##classe::SetName(const CATBSTR & iName) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&iName); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetName(iName); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&iName); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAGenericMotionProfile##classe::GetName(CATBSTR & oName) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&oName); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetName(oName); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&oName); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAGenericMotionProfile##classe::SetMotionBasis(MotionBasis basis) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&basis); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetMotionBasis(basis); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&basis); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAGenericMotionProfile##classe::GetMotionBasis(MotionBasis & basis) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&basis); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetMotionBasis(basis); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&basis); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAGenericMotionProfile##classe::SetSpeedValue(double value) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&value); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetSpeedValue(value); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&value); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAGenericMotionProfile##classe::GetSpeedValue(double & value) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&value); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetSpeedValue(value); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&value); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAGenericMotionProfile##classe::SetAccelerationValue(double value) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&value); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetAccelerationValue(value); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&value); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAGenericMotionProfile##classe::GetAccelerationValue(double & value) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&value); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAccelerationValue(value); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&value); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAGenericMotionProfile##classe::SetAngularSpeedValue(double value) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&value); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetAngularSpeedValue(value); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&value); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAGenericMotionProfile##classe::GetAngularSpeedValue(double & value) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&value); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAngularSpeedValue(value); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&value); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAGenericMotionProfile##classe::SetAngularAccelerationValue(double value) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,11,&_Trac2,&value); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetAngularAccelerationValue(value); \
   ExitAfterCall(this,11,_Trac2,&_ret_arg,&value); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAGenericMotionProfile##classe::GetAngularAccelerationValue(double & value) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,12,&_Trac2,&value); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAngularAccelerationValue(value); \
   ExitAfterCall(this,12,_Trac2,&_ret_arg,&value); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAGenericMotionProfile##classe::GetController(DNBIARobGenericController *& oController) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,13,&_Trac2,&oController); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetController(oController); \
   ExitAfterCall(this,13,_Trac2,&_ret_arg,&oController); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAGenericMotionProfile##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,14,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,14,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAGenericMotionProfile##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,15,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,15,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAGenericMotionProfile##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,16,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,16,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAGenericMotionProfile##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,17,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,17,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAGenericMotionProfile##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,18,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,18,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIAGenericMotionProfile(classe) \
 \
 \
declare_TIE_DNBIAGenericMotionProfile(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAGenericMotionProfile##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAGenericMotionProfile,"DNBIAGenericMotionProfile",DNBIAGenericMotionProfile::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAGenericMotionProfile(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIAGenericMotionProfile, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAGenericMotionProfile##classe(classe::MetaObject(),DNBIAGenericMotionProfile::MetaObject(),(void *)CreateTIEDNBIAGenericMotionProfile##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIAGenericMotionProfile(classe) \
 \
 \
declare_TIE_DNBIAGenericMotionProfile(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAGenericMotionProfile##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAGenericMotionProfile,"DNBIAGenericMotionProfile",DNBIAGenericMotionProfile::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAGenericMotionProfile(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIAGenericMotionProfile, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAGenericMotionProfile##classe(classe::MetaObject(),DNBIAGenericMotionProfile::MetaObject(),(void *)CreateTIEDNBIAGenericMotionProfile##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIAGenericMotionProfile(classe) TIE_DNBIAGenericMotionProfile(classe)
#else
#define BOA_DNBIAGenericMotionProfile(classe) CATImplementBOA(DNBIAGenericMotionProfile, classe)
#endif

#endif
