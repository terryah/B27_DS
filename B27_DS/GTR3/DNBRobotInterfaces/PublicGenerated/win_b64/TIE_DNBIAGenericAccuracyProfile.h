#ifndef __TIE_DNBIAGenericAccuracyProfile
#define __TIE_DNBIAGenericAccuracyProfile

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIAGenericAccuracyProfile.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIAGenericAccuracyProfile */
#define declare_TIE_DNBIAGenericAccuracyProfile(classe) \
 \
 \
class TIEDNBIAGenericAccuracyProfile##classe : public DNBIAGenericAccuracyProfile \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIAGenericAccuracyProfile, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall SetName(const CATBSTR & iName); \
      virtual HRESULT __stdcall GetName(CATBSTR & oName); \
      virtual HRESULT __stdcall SetFlyByMode(CAT_VARIANT_BOOL iMode); \
      virtual HRESULT __stdcall GetFlyByMode(CAT_VARIANT_BOOL & oMode); \
      virtual HRESULT __stdcall SetAccuracyType(AccuracyType accuracy); \
      virtual HRESULT __stdcall GetAccuracyType(AccuracyType & accuracy); \
      virtual HRESULT __stdcall SetAccuracyValue(double value); \
      virtual HRESULT __stdcall GetAccuracyValue(double & value); \
      virtual HRESULT __stdcall GetController(DNBIARobGenericController *& oController); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIAGenericAccuracyProfile(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall SetName(const CATBSTR & iName); \
virtual HRESULT __stdcall GetName(CATBSTR & oName); \
virtual HRESULT __stdcall SetFlyByMode(CAT_VARIANT_BOOL iMode); \
virtual HRESULT __stdcall GetFlyByMode(CAT_VARIANT_BOOL & oMode); \
virtual HRESULT __stdcall SetAccuracyType(AccuracyType accuracy); \
virtual HRESULT __stdcall GetAccuracyType(AccuracyType & accuracy); \
virtual HRESULT __stdcall SetAccuracyValue(double value); \
virtual HRESULT __stdcall GetAccuracyValue(double & value); \
virtual HRESULT __stdcall GetController(DNBIARobGenericController *& oController); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIAGenericAccuracyProfile(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::SetName(const CATBSTR & iName) \
{ \
return (ENVTIECALL(DNBIAGenericAccuracyProfile,ENVTIETypeLetter,ENVTIELetter)SetName(iName)); \
} \
HRESULT __stdcall  ENVTIEName::GetName(CATBSTR & oName) \
{ \
return (ENVTIECALL(DNBIAGenericAccuracyProfile,ENVTIETypeLetter,ENVTIELetter)GetName(oName)); \
} \
HRESULT __stdcall  ENVTIEName::SetFlyByMode(CAT_VARIANT_BOOL iMode) \
{ \
return (ENVTIECALL(DNBIAGenericAccuracyProfile,ENVTIETypeLetter,ENVTIELetter)SetFlyByMode(iMode)); \
} \
HRESULT __stdcall  ENVTIEName::GetFlyByMode(CAT_VARIANT_BOOL & oMode) \
{ \
return (ENVTIECALL(DNBIAGenericAccuracyProfile,ENVTIETypeLetter,ENVTIELetter)GetFlyByMode(oMode)); \
} \
HRESULT __stdcall  ENVTIEName::SetAccuracyType(AccuracyType accuracy) \
{ \
return (ENVTIECALL(DNBIAGenericAccuracyProfile,ENVTIETypeLetter,ENVTIELetter)SetAccuracyType(accuracy)); \
} \
HRESULT __stdcall  ENVTIEName::GetAccuracyType(AccuracyType & accuracy) \
{ \
return (ENVTIECALL(DNBIAGenericAccuracyProfile,ENVTIETypeLetter,ENVTIELetter)GetAccuracyType(accuracy)); \
} \
HRESULT __stdcall  ENVTIEName::SetAccuracyValue(double value) \
{ \
return (ENVTIECALL(DNBIAGenericAccuracyProfile,ENVTIETypeLetter,ENVTIELetter)SetAccuracyValue(value)); \
} \
HRESULT __stdcall  ENVTIEName::GetAccuracyValue(double & value) \
{ \
return (ENVTIECALL(DNBIAGenericAccuracyProfile,ENVTIETypeLetter,ENVTIELetter)GetAccuracyValue(value)); \
} \
HRESULT __stdcall  ENVTIEName::GetController(DNBIARobGenericController *& oController) \
{ \
return (ENVTIECALL(DNBIAGenericAccuracyProfile,ENVTIETypeLetter,ENVTIELetter)GetController(oController)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIAGenericAccuracyProfile,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIAGenericAccuracyProfile,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIAGenericAccuracyProfile,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIAGenericAccuracyProfile,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIAGenericAccuracyProfile,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIAGenericAccuracyProfile(classe)    TIEDNBIAGenericAccuracyProfile##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIAGenericAccuracyProfile(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIAGenericAccuracyProfile, classe) \
 \
 \
CATImplementTIEMethods(DNBIAGenericAccuracyProfile, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIAGenericAccuracyProfile, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIAGenericAccuracyProfile, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIAGenericAccuracyProfile, classe) \
 \
HRESULT __stdcall  TIEDNBIAGenericAccuracyProfile##classe::SetName(const CATBSTR & iName) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&iName); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetName(iName); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&iName); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAGenericAccuracyProfile##classe::GetName(CATBSTR & oName) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&oName); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetName(oName); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&oName); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAGenericAccuracyProfile##classe::SetFlyByMode(CAT_VARIANT_BOOL iMode) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&iMode); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetFlyByMode(iMode); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&iMode); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAGenericAccuracyProfile##classe::GetFlyByMode(CAT_VARIANT_BOOL & oMode) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&oMode); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetFlyByMode(oMode); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&oMode); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAGenericAccuracyProfile##classe::SetAccuracyType(AccuracyType accuracy) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&accuracy); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetAccuracyType(accuracy); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&accuracy); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAGenericAccuracyProfile##classe::GetAccuracyType(AccuracyType & accuracy) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&accuracy); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAccuracyType(accuracy); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&accuracy); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAGenericAccuracyProfile##classe::SetAccuracyValue(double value) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&value); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetAccuracyValue(value); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&value); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAGenericAccuracyProfile##classe::GetAccuracyValue(double & value) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&value); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAccuracyValue(value); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&value); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAGenericAccuracyProfile##classe::GetController(DNBIARobGenericController *& oController) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&oController); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetController(oController); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&oController); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAGenericAccuracyProfile##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAGenericAccuracyProfile##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,11,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,11,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAGenericAccuracyProfile##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,12,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,12,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAGenericAccuracyProfile##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,13,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,13,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAGenericAccuracyProfile##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,14,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,14,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIAGenericAccuracyProfile(classe) \
 \
 \
declare_TIE_DNBIAGenericAccuracyProfile(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAGenericAccuracyProfile##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAGenericAccuracyProfile,"DNBIAGenericAccuracyProfile",DNBIAGenericAccuracyProfile::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAGenericAccuracyProfile(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIAGenericAccuracyProfile, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAGenericAccuracyProfile##classe(classe::MetaObject(),DNBIAGenericAccuracyProfile::MetaObject(),(void *)CreateTIEDNBIAGenericAccuracyProfile##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIAGenericAccuracyProfile(classe) \
 \
 \
declare_TIE_DNBIAGenericAccuracyProfile(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAGenericAccuracyProfile##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAGenericAccuracyProfile,"DNBIAGenericAccuracyProfile",DNBIAGenericAccuracyProfile::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAGenericAccuracyProfile(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIAGenericAccuracyProfile, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAGenericAccuracyProfile##classe(classe::MetaObject(),DNBIAGenericAccuracyProfile::MetaObject(),(void *)CreateTIEDNBIAGenericAccuracyProfile##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIAGenericAccuracyProfile(classe) TIE_DNBIAGenericAccuracyProfile(classe)
#else
#define BOA_DNBIAGenericAccuracyProfile(classe) CATImplementBOA(DNBIAGenericAccuracyProfile, classe)
#endif

#endif
