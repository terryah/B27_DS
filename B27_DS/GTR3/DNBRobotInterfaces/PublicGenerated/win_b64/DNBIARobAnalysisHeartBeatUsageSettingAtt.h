/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef DNBIARobAnalysisHeartBeatUsageSettingAtt_h
#define DNBIARobAnalysisHeartBeatUsageSettingAtt_h

#include "CATCORBABoolean.h"
#include "CAT_VARIANT_BOOL.h"

#ifndef ExportedByDNBRobotPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __DNBRobotPubIDL
#define ExportedByDNBRobotPubIDL __declspec(dllexport)
#else
#define ExportedByDNBRobotPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByDNBRobotPubIDL
#endif
#endif

#include "CATBSTR.h"
#include "CATIASettingController.h"

extern ExportedByDNBRobotPubIDL IID IID_DNBIARobAnalysisHeartBeatUsageSettingAtt;

class ExportedByDNBRobotPubIDL DNBIARobAnalysisHeartBeatUsageSettingAtt : public CATIASettingController
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_EnableHeartBeat(CAT_VARIANT_BOOL & oLevel)=0;

    virtual HRESULT __stdcall put_EnableHeartBeat(CAT_VARIANT_BOOL iLevel)=0;

    virtual HRESULT __stdcall GetEnableHeartBeatInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetEnableHeartBeatLock(CAT_VARIANT_BOOL iLocked)=0;


};

CATDeclareHandler(DNBIARobAnalysisHeartBeatUsageSettingAtt, CATIASettingController);

#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIABase.h"
#include "CATSafeArray.h"
#include "CATVariant.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
