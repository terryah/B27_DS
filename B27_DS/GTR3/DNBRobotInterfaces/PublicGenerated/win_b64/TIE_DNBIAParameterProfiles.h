#ifndef __TIE_DNBIAParameterProfiles
#define __TIE_DNBIAParameterProfiles

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIAParameterProfiles.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIAParameterProfiles */
#define declare_TIE_DNBIAParameterProfiles(classe) \
 \
 \
class TIEDNBIAParameterProfiles##classe : public DNBIAParameterProfiles \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIAParameterProfiles, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall SetName(const CATBSTR & iNewInstanceName); \
      virtual HRESULT __stdcall GetName(CATBSTR & oInstanceName); \
      virtual HRESULT __stdcall SetAttributeValue(const CATBSTR & iAttributeName, const CATBSTR & iEncapsulatedAttributeValue); \
      virtual HRESULT __stdcall GetAttributeValue(const CATBSTR & iAttributeName, CATBSTR & oEncapsulatedAttributeValue); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIAParameterProfiles(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall SetName(const CATBSTR & iNewInstanceName); \
virtual HRESULT __stdcall GetName(CATBSTR & oInstanceName); \
virtual HRESULT __stdcall SetAttributeValue(const CATBSTR & iAttributeName, const CATBSTR & iEncapsulatedAttributeValue); \
virtual HRESULT __stdcall GetAttributeValue(const CATBSTR & iAttributeName, CATBSTR & oEncapsulatedAttributeValue); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIAParameterProfiles(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::SetName(const CATBSTR & iNewInstanceName) \
{ \
return (ENVTIECALL(DNBIAParameterProfiles,ENVTIETypeLetter,ENVTIELetter)SetName(iNewInstanceName)); \
} \
HRESULT __stdcall  ENVTIEName::GetName(CATBSTR & oInstanceName) \
{ \
return (ENVTIECALL(DNBIAParameterProfiles,ENVTIETypeLetter,ENVTIELetter)GetName(oInstanceName)); \
} \
HRESULT __stdcall  ENVTIEName::SetAttributeValue(const CATBSTR & iAttributeName, const CATBSTR & iEncapsulatedAttributeValue) \
{ \
return (ENVTIECALL(DNBIAParameterProfiles,ENVTIETypeLetter,ENVTIELetter)SetAttributeValue(iAttributeName,iEncapsulatedAttributeValue)); \
} \
HRESULT __stdcall  ENVTIEName::GetAttributeValue(const CATBSTR & iAttributeName, CATBSTR & oEncapsulatedAttributeValue) \
{ \
return (ENVTIECALL(DNBIAParameterProfiles,ENVTIETypeLetter,ENVTIELetter)GetAttributeValue(iAttributeName,oEncapsulatedAttributeValue)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIAParameterProfiles,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIAParameterProfiles,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIAParameterProfiles,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIAParameterProfiles,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIAParameterProfiles,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIAParameterProfiles(classe)    TIEDNBIAParameterProfiles##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIAParameterProfiles(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIAParameterProfiles, classe) \
 \
 \
CATImplementTIEMethods(DNBIAParameterProfiles, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIAParameterProfiles, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIAParameterProfiles, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIAParameterProfiles, classe) \
 \
HRESULT __stdcall  TIEDNBIAParameterProfiles##classe::SetName(const CATBSTR & iNewInstanceName) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&iNewInstanceName); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetName(iNewInstanceName); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&iNewInstanceName); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAParameterProfiles##classe::GetName(CATBSTR & oInstanceName) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&oInstanceName); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetName(oInstanceName); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&oInstanceName); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAParameterProfiles##classe::SetAttributeValue(const CATBSTR & iAttributeName, const CATBSTR & iEncapsulatedAttributeValue) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&iAttributeName,&iEncapsulatedAttributeValue); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetAttributeValue(iAttributeName,iEncapsulatedAttributeValue); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&iAttributeName,&iEncapsulatedAttributeValue); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAParameterProfiles##classe::GetAttributeValue(const CATBSTR & iAttributeName, CATBSTR & oEncapsulatedAttributeValue) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&iAttributeName,&oEncapsulatedAttributeValue); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAttributeValue(iAttributeName,oEncapsulatedAttributeValue); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&iAttributeName,&oEncapsulatedAttributeValue); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAParameterProfiles##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAParameterProfiles##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAParameterProfiles##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAParameterProfiles##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAParameterProfiles##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIAParameterProfiles(classe) \
 \
 \
declare_TIE_DNBIAParameterProfiles(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAParameterProfiles##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAParameterProfiles,"DNBIAParameterProfiles",DNBIAParameterProfiles::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAParameterProfiles(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIAParameterProfiles, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAParameterProfiles##classe(classe::MetaObject(),DNBIAParameterProfiles::MetaObject(),(void *)CreateTIEDNBIAParameterProfiles##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIAParameterProfiles(classe) \
 \
 \
declare_TIE_DNBIAParameterProfiles(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAParameterProfiles##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAParameterProfiles,"DNBIAParameterProfiles",DNBIAParameterProfiles::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAParameterProfiles(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIAParameterProfiles, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAParameterProfiles##classe(classe::MetaObject(),DNBIAParameterProfiles::MetaObject(),(void *)CreateTIEDNBIAParameterProfiles##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIAParameterProfiles(classe) TIE_DNBIAParameterProfiles(classe)
#else
#define BOA_DNBIAParameterProfiles(classe) CATImplementBOA(DNBIAParameterProfiles, classe)
#endif

#endif
