#ifndef __TIE_DNBIAGenericToolProfile
#define __TIE_DNBIAGenericToolProfile

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIAGenericToolProfile.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIAGenericToolProfile */
#define declare_TIE_DNBIAGenericToolProfile(classe) \
 \
 \
class TIEDNBIAGenericToolProfile##classe : public DNBIAGenericToolProfile \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIAGenericToolProfile, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall SetName(const CATBSTR & iName); \
      virtual HRESULT __stdcall GetName(CATBSTR & oName); \
      virtual HRESULT __stdcall SetToolMobility(CAT_VARIANT_BOOL iMobile); \
      virtual HRESULT __stdcall GetToolMobility(CAT_VARIANT_BOOL & oMobile); \
      virtual HRESULT __stdcall SetTCPOffset(double x, double y, double z, double roll, double pitch, double yaw); \
      virtual HRESULT __stdcall GetTCPOffset(double & x, double & y, double & z, double & roll, double & pitch, double & yaw); \
      virtual HRESULT __stdcall SetMass(double mass); \
      virtual HRESULT __stdcall GetMass(double & mass); \
      virtual HRESULT __stdcall SetCentroid(double cx, double cy, double cz); \
      virtual HRESULT __stdcall GetCentroid(double & cx, double & cy, double & cz); \
      virtual HRESULT __stdcall SetInertia(double Ixx, double Iyy, double Izz, double Ixy, double Iyz, double Izx); \
      virtual HRESULT __stdcall GetInertia(double & Ixx, double & Iyy, double & Izz, double & Ixy, double & Iyz, double & Izx); \
      virtual HRESULT __stdcall GetController(DNBIARobGenericController *& oController); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIAGenericToolProfile(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall SetName(const CATBSTR & iName); \
virtual HRESULT __stdcall GetName(CATBSTR & oName); \
virtual HRESULT __stdcall SetToolMobility(CAT_VARIANT_BOOL iMobile); \
virtual HRESULT __stdcall GetToolMobility(CAT_VARIANT_BOOL & oMobile); \
virtual HRESULT __stdcall SetTCPOffset(double x, double y, double z, double roll, double pitch, double yaw); \
virtual HRESULT __stdcall GetTCPOffset(double & x, double & y, double & z, double & roll, double & pitch, double & yaw); \
virtual HRESULT __stdcall SetMass(double mass); \
virtual HRESULT __stdcall GetMass(double & mass); \
virtual HRESULT __stdcall SetCentroid(double cx, double cy, double cz); \
virtual HRESULT __stdcall GetCentroid(double & cx, double & cy, double & cz); \
virtual HRESULT __stdcall SetInertia(double Ixx, double Iyy, double Izz, double Ixy, double Iyz, double Izx); \
virtual HRESULT __stdcall GetInertia(double & Ixx, double & Iyy, double & Izz, double & Ixy, double & Iyz, double & Izx); \
virtual HRESULT __stdcall GetController(DNBIARobGenericController *& oController); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIAGenericToolProfile(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::SetName(const CATBSTR & iName) \
{ \
return (ENVTIECALL(DNBIAGenericToolProfile,ENVTIETypeLetter,ENVTIELetter)SetName(iName)); \
} \
HRESULT __stdcall  ENVTIEName::GetName(CATBSTR & oName) \
{ \
return (ENVTIECALL(DNBIAGenericToolProfile,ENVTIETypeLetter,ENVTIELetter)GetName(oName)); \
} \
HRESULT __stdcall  ENVTIEName::SetToolMobility(CAT_VARIANT_BOOL iMobile) \
{ \
return (ENVTIECALL(DNBIAGenericToolProfile,ENVTIETypeLetter,ENVTIELetter)SetToolMobility(iMobile)); \
} \
HRESULT __stdcall  ENVTIEName::GetToolMobility(CAT_VARIANT_BOOL & oMobile) \
{ \
return (ENVTIECALL(DNBIAGenericToolProfile,ENVTIETypeLetter,ENVTIELetter)GetToolMobility(oMobile)); \
} \
HRESULT __stdcall  ENVTIEName::SetTCPOffset(double x, double y, double z, double roll, double pitch, double yaw) \
{ \
return (ENVTIECALL(DNBIAGenericToolProfile,ENVTIETypeLetter,ENVTIELetter)SetTCPOffset(x,y,z,roll,pitch,yaw)); \
} \
HRESULT __stdcall  ENVTIEName::GetTCPOffset(double & x, double & y, double & z, double & roll, double & pitch, double & yaw) \
{ \
return (ENVTIECALL(DNBIAGenericToolProfile,ENVTIETypeLetter,ENVTIELetter)GetTCPOffset(x,y,z,roll,pitch,yaw)); \
} \
HRESULT __stdcall  ENVTIEName::SetMass(double mass) \
{ \
return (ENVTIECALL(DNBIAGenericToolProfile,ENVTIETypeLetter,ENVTIELetter)SetMass(mass)); \
} \
HRESULT __stdcall  ENVTIEName::GetMass(double & mass) \
{ \
return (ENVTIECALL(DNBIAGenericToolProfile,ENVTIETypeLetter,ENVTIELetter)GetMass(mass)); \
} \
HRESULT __stdcall  ENVTIEName::SetCentroid(double cx, double cy, double cz) \
{ \
return (ENVTIECALL(DNBIAGenericToolProfile,ENVTIETypeLetter,ENVTIELetter)SetCentroid(cx,cy,cz)); \
} \
HRESULT __stdcall  ENVTIEName::GetCentroid(double & cx, double & cy, double & cz) \
{ \
return (ENVTIECALL(DNBIAGenericToolProfile,ENVTIETypeLetter,ENVTIELetter)GetCentroid(cx,cy,cz)); \
} \
HRESULT __stdcall  ENVTIEName::SetInertia(double Ixx, double Iyy, double Izz, double Ixy, double Iyz, double Izx) \
{ \
return (ENVTIECALL(DNBIAGenericToolProfile,ENVTIETypeLetter,ENVTIELetter)SetInertia(Ixx,Iyy,Izz,Ixy,Iyz,Izx)); \
} \
HRESULT __stdcall  ENVTIEName::GetInertia(double & Ixx, double & Iyy, double & Izz, double & Ixy, double & Iyz, double & Izx) \
{ \
return (ENVTIECALL(DNBIAGenericToolProfile,ENVTIETypeLetter,ENVTIELetter)GetInertia(Ixx,Iyy,Izz,Ixy,Iyz,Izx)); \
} \
HRESULT __stdcall  ENVTIEName::GetController(DNBIARobGenericController *& oController) \
{ \
return (ENVTIECALL(DNBIAGenericToolProfile,ENVTIETypeLetter,ENVTIELetter)GetController(oController)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIAGenericToolProfile,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIAGenericToolProfile,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIAGenericToolProfile,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIAGenericToolProfile,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIAGenericToolProfile,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIAGenericToolProfile(classe)    TIEDNBIAGenericToolProfile##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIAGenericToolProfile(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIAGenericToolProfile, classe) \
 \
 \
CATImplementTIEMethods(DNBIAGenericToolProfile, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIAGenericToolProfile, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIAGenericToolProfile, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIAGenericToolProfile, classe) \
 \
HRESULT __stdcall  TIEDNBIAGenericToolProfile##classe::SetName(const CATBSTR & iName) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&iName); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetName(iName); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&iName); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAGenericToolProfile##classe::GetName(CATBSTR & oName) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&oName); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetName(oName); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&oName); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAGenericToolProfile##classe::SetToolMobility(CAT_VARIANT_BOOL iMobile) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&iMobile); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetToolMobility(iMobile); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&iMobile); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAGenericToolProfile##classe::GetToolMobility(CAT_VARIANT_BOOL & oMobile) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&oMobile); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetToolMobility(oMobile); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&oMobile); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAGenericToolProfile##classe::SetTCPOffset(double x, double y, double z, double roll, double pitch, double yaw) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&x,&y,&z,&roll,&pitch,&yaw); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetTCPOffset(x,y,z,roll,pitch,yaw); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&x,&y,&z,&roll,&pitch,&yaw); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAGenericToolProfile##classe::GetTCPOffset(double & x, double & y, double & z, double & roll, double & pitch, double & yaw) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&x,&y,&z,&roll,&pitch,&yaw); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetTCPOffset(x,y,z,roll,pitch,yaw); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&x,&y,&z,&roll,&pitch,&yaw); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAGenericToolProfile##classe::SetMass(double mass) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&mass); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetMass(mass); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&mass); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAGenericToolProfile##classe::GetMass(double & mass) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&mass); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetMass(mass); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&mass); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAGenericToolProfile##classe::SetCentroid(double cx, double cy, double cz) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&cx,&cy,&cz); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetCentroid(cx,cy,cz); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&cx,&cy,&cz); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAGenericToolProfile##classe::GetCentroid(double & cx, double & cy, double & cz) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&cx,&cy,&cz); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetCentroid(cx,cy,cz); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&cx,&cy,&cz); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAGenericToolProfile##classe::SetInertia(double Ixx, double Iyy, double Izz, double Ixy, double Iyz, double Izx) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,11,&_Trac2,&Ixx,&Iyy,&Izz,&Ixy,&Iyz,&Izx); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetInertia(Ixx,Iyy,Izz,Ixy,Iyz,Izx); \
   ExitAfterCall(this,11,_Trac2,&_ret_arg,&Ixx,&Iyy,&Izz,&Ixy,&Iyz,&Izx); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAGenericToolProfile##classe::GetInertia(double & Ixx, double & Iyy, double & Izz, double & Ixy, double & Iyz, double & Izx) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,12,&_Trac2,&Ixx,&Iyy,&Izz,&Ixy,&Iyz,&Izx); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetInertia(Ixx,Iyy,Izz,Ixy,Iyz,Izx); \
   ExitAfterCall(this,12,_Trac2,&_ret_arg,&Ixx,&Iyy,&Izz,&Ixy,&Iyz,&Izx); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAGenericToolProfile##classe::GetController(DNBIARobGenericController *& oController) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,13,&_Trac2,&oController); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetController(oController); \
   ExitAfterCall(this,13,_Trac2,&_ret_arg,&oController); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAGenericToolProfile##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,14,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,14,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAGenericToolProfile##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,15,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,15,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAGenericToolProfile##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,16,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,16,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAGenericToolProfile##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,17,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,17,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAGenericToolProfile##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,18,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,18,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIAGenericToolProfile(classe) \
 \
 \
declare_TIE_DNBIAGenericToolProfile(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAGenericToolProfile##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAGenericToolProfile,"DNBIAGenericToolProfile",DNBIAGenericToolProfile::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAGenericToolProfile(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIAGenericToolProfile, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAGenericToolProfile##classe(classe::MetaObject(),DNBIAGenericToolProfile::MetaObject(),(void *)CreateTIEDNBIAGenericToolProfile##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIAGenericToolProfile(classe) \
 \
 \
declare_TIE_DNBIAGenericToolProfile(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAGenericToolProfile##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAGenericToolProfile,"DNBIAGenericToolProfile",DNBIAGenericToolProfile::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAGenericToolProfile(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIAGenericToolProfile, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAGenericToolProfile##classe(classe::MetaObject(),DNBIAGenericToolProfile::MetaObject(),(void *)CreateTIEDNBIAGenericToolProfile##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIAGenericToolProfile(classe) TIE_DNBIAGenericToolProfile(classe)
#else
#define BOA_DNBIAGenericToolProfile(classe) CATImplementBOA(DNBIAGenericToolProfile, classe)
#endif

#endif
