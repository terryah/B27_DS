#ifndef __TIE_DNBIARobAnalysisHeartBeatUsageSettingAtt
#define __TIE_DNBIARobAnalysisHeartBeatUsageSettingAtt

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIARobAnalysisHeartBeatUsageSettingAtt.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIARobAnalysisHeartBeatUsageSettingAtt */
#define declare_TIE_DNBIARobAnalysisHeartBeatUsageSettingAtt(classe) \
 \
 \
class TIEDNBIARobAnalysisHeartBeatUsageSettingAtt##classe : public DNBIARobAnalysisHeartBeatUsageSettingAtt \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIARobAnalysisHeartBeatUsageSettingAtt, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall get_EnableHeartBeat(CAT_VARIANT_BOOL & oLevel); \
      virtual HRESULT __stdcall put_EnableHeartBeat(CAT_VARIANT_BOOL iLevel); \
      virtual HRESULT __stdcall GetEnableHeartBeatInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetEnableHeartBeatLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall Commit(); \
      virtual HRESULT __stdcall Rollback(); \
      virtual HRESULT __stdcall ResetToAdminValues(); \
      virtual HRESULT __stdcall ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList); \
      virtual HRESULT __stdcall SaveRepository(); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIARobAnalysisHeartBeatUsageSettingAtt(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall get_EnableHeartBeat(CAT_VARIANT_BOOL & oLevel); \
virtual HRESULT __stdcall put_EnableHeartBeat(CAT_VARIANT_BOOL iLevel); \
virtual HRESULT __stdcall GetEnableHeartBeatInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetEnableHeartBeatLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall Commit(); \
virtual HRESULT __stdcall Rollback(); \
virtual HRESULT __stdcall ResetToAdminValues(); \
virtual HRESULT __stdcall ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList); \
virtual HRESULT __stdcall SaveRepository(); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIARobAnalysisHeartBeatUsageSettingAtt(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::get_EnableHeartBeat(CAT_VARIANT_BOOL & oLevel) \
{ \
return (ENVTIECALL(DNBIARobAnalysisHeartBeatUsageSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_EnableHeartBeat(oLevel)); \
} \
HRESULT __stdcall  ENVTIEName::put_EnableHeartBeat(CAT_VARIANT_BOOL iLevel) \
{ \
return (ENVTIECALL(DNBIARobAnalysisHeartBeatUsageSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_EnableHeartBeat(iLevel)); \
} \
HRESULT __stdcall  ENVTIEName::GetEnableHeartBeatInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIARobAnalysisHeartBeatUsageSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetEnableHeartBeatInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetEnableHeartBeatLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIARobAnalysisHeartBeatUsageSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetEnableHeartBeatLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::Commit() \
{ \
return (ENVTIECALL(DNBIARobAnalysisHeartBeatUsageSettingAtt,ENVTIETypeLetter,ENVTIELetter)Commit()); \
} \
HRESULT __stdcall  ENVTIEName::Rollback() \
{ \
return (ENVTIECALL(DNBIARobAnalysisHeartBeatUsageSettingAtt,ENVTIETypeLetter,ENVTIELetter)Rollback()); \
} \
HRESULT __stdcall  ENVTIEName::ResetToAdminValues() \
{ \
return (ENVTIECALL(DNBIARobAnalysisHeartBeatUsageSettingAtt,ENVTIETypeLetter,ENVTIELetter)ResetToAdminValues()); \
} \
HRESULT __stdcall  ENVTIEName::ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList) \
{ \
return (ENVTIECALL(DNBIARobAnalysisHeartBeatUsageSettingAtt,ENVTIETypeLetter,ENVTIELetter)ResetToAdminValuesByName(iAttList)); \
} \
HRESULT __stdcall  ENVTIEName::SaveRepository() \
{ \
return (ENVTIECALL(DNBIARobAnalysisHeartBeatUsageSettingAtt,ENVTIETypeLetter,ENVTIELetter)SaveRepository()); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIARobAnalysisHeartBeatUsageSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIARobAnalysisHeartBeatUsageSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIARobAnalysisHeartBeatUsageSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIARobAnalysisHeartBeatUsageSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIARobAnalysisHeartBeatUsageSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIARobAnalysisHeartBeatUsageSettingAtt(classe)    TIEDNBIARobAnalysisHeartBeatUsageSettingAtt##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIARobAnalysisHeartBeatUsageSettingAtt(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIARobAnalysisHeartBeatUsageSettingAtt, classe) \
 \
 \
CATImplementTIEMethods(DNBIARobAnalysisHeartBeatUsageSettingAtt, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIARobAnalysisHeartBeatUsageSettingAtt, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIARobAnalysisHeartBeatUsageSettingAtt, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIARobAnalysisHeartBeatUsageSettingAtt, classe) \
 \
HRESULT __stdcall  TIEDNBIARobAnalysisHeartBeatUsageSettingAtt##classe::get_EnableHeartBeat(CAT_VARIANT_BOOL & oLevel) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&oLevel); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_EnableHeartBeat(oLevel); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&oLevel); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobAnalysisHeartBeatUsageSettingAtt##classe::put_EnableHeartBeat(CAT_VARIANT_BOOL iLevel) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&iLevel); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_EnableHeartBeat(iLevel); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&iLevel); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobAnalysisHeartBeatUsageSettingAtt##classe::GetEnableHeartBeatInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetEnableHeartBeatInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobAnalysisHeartBeatUsageSettingAtt##classe::SetEnableHeartBeatLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetEnableHeartBeatLock(iLocked); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobAnalysisHeartBeatUsageSettingAtt##classe::Commit() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Commit(); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobAnalysisHeartBeatUsageSettingAtt##classe::Rollback() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Rollback(); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobAnalysisHeartBeatUsageSettingAtt##classe::ResetToAdminValues() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ResetToAdminValues(); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobAnalysisHeartBeatUsageSettingAtt##classe::ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&iAttList); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ResetToAdminValuesByName(iAttList); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&iAttList); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARobAnalysisHeartBeatUsageSettingAtt##classe::SaveRepository() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SaveRepository(); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIARobAnalysisHeartBeatUsageSettingAtt##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIARobAnalysisHeartBeatUsageSettingAtt##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,11,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,11,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIARobAnalysisHeartBeatUsageSettingAtt##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,12,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,12,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIARobAnalysisHeartBeatUsageSettingAtt##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,13,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,13,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIARobAnalysisHeartBeatUsageSettingAtt##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,14,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,14,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIARobAnalysisHeartBeatUsageSettingAtt(classe) \
 \
 \
declare_TIE_DNBIARobAnalysisHeartBeatUsageSettingAtt(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIARobAnalysisHeartBeatUsageSettingAtt##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIARobAnalysisHeartBeatUsageSettingAtt,"DNBIARobAnalysisHeartBeatUsageSettingAtt",DNBIARobAnalysisHeartBeatUsageSettingAtt::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIARobAnalysisHeartBeatUsageSettingAtt(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIARobAnalysisHeartBeatUsageSettingAtt, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIARobAnalysisHeartBeatUsageSettingAtt##classe(classe::MetaObject(),DNBIARobAnalysisHeartBeatUsageSettingAtt::MetaObject(),(void *)CreateTIEDNBIARobAnalysisHeartBeatUsageSettingAtt##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIARobAnalysisHeartBeatUsageSettingAtt(classe) \
 \
 \
declare_TIE_DNBIARobAnalysisHeartBeatUsageSettingAtt(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIARobAnalysisHeartBeatUsageSettingAtt##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIARobAnalysisHeartBeatUsageSettingAtt,"DNBIARobAnalysisHeartBeatUsageSettingAtt",DNBIARobAnalysisHeartBeatUsageSettingAtt::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIARobAnalysisHeartBeatUsageSettingAtt(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIARobAnalysisHeartBeatUsageSettingAtt, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIARobAnalysisHeartBeatUsageSettingAtt##classe(classe::MetaObject(),DNBIARobAnalysisHeartBeatUsageSettingAtt::MetaObject(),(void *)CreateTIEDNBIARobAnalysisHeartBeatUsageSettingAtt##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIARobAnalysisHeartBeatUsageSettingAtt(classe) TIE_DNBIARobAnalysisHeartBeatUsageSettingAtt(classe)
#else
#define BOA_DNBIARobAnalysisHeartBeatUsageSettingAtt(classe) CATImplementBOA(DNBIARobAnalysisHeartBeatUsageSettingAtt, classe)
#endif

#endif
