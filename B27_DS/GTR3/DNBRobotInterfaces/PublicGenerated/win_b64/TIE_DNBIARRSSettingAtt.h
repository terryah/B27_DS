#ifndef __TIE_DNBIARRSSettingAtt
#define __TIE_DNBIARRSSettingAtt

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIARRSSettingAtt.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIARRSSettingAtt */
#define declare_TIE_DNBIARRSSettingAtt(classe) \
 \
 \
class TIEDNBIARRSSettingAtt##classe : public DNBIARRSSettingAtt \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIARRSSettingAtt, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall get_RRSLibDir(CATBSTR & oLibDir); \
      virtual HRESULT __stdcall put_RRSLibDir(const CATBSTR & iLibDir); \
      virtual HRESULT __stdcall GetRRSLibDirInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetRRSLibDirLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_RRSServerFile(CATBSTR & oServerFile); \
      virtual HRESULT __stdcall put_RRSServerFile(const CATBSTR & iServerFile); \
      virtual HRESULT __stdcall GetRRSServerFileInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetRRSServerFileLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_RCSDataLibDir(CATBSTR & oDataDir); \
      virtual HRESULT __stdcall put_RCSDataLibDir(const CATBSTR & iDataDir); \
      virtual HRESULT __stdcall GetRCSDataLibDirInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetRCSDataLibDirLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_RRS2MotionPlannerActiveDuringTeachOLP(CAT_VARIANT_BOOL & oRRS2MotionPlannerActiveDuringTeachOLP); \
      virtual HRESULT __stdcall put_RRS2MotionPlannerActiveDuringTeachOLP(CAT_VARIANT_BOOL iRRS2MotionPlannerActiveDuringTeachOLP); \
      virtual HRESULT __stdcall GetRRS2MotionPlannerActiveDuringTeachOLPInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetRRS2MotionPlannerActiveDuringTeachOLPLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_RRS2UseVRCTurnLimits(CAT_VARIANT_BOOL & oRRS2UseVRCTurnLimits); \
      virtual HRESULT __stdcall put_RRS2UseVRCTurnLimits(CAT_VARIANT_BOOL iRRS2UseVRCTurnLimits); \
      virtual HRESULT __stdcall GetRRS2UseVRCTurnLimitsInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetRRS2UseVRCTurnLimitsLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_RRS2DownloadLogFileName(CATBSTR & oRRS2DownloadLogFileName); \
      virtual HRESULT __stdcall put_RRS2DownloadLogFileName(const CATBSTR & iRRS2DownloadLogFileName); \
      virtual HRESULT __stdcall GetRRS2DownloadLogFileNameInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetRRS2DownloadLogFileNameLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_RRS2DfltControllerIdentifier(CATBSTR & oRRS2DfltControllerIdentifier); \
      virtual HRESULT __stdcall put_RRS2DfltControllerIdentifier(const CATBSTR & iRRS2DfltControllerIdentifier); \
      virtual HRESULT __stdcall GetRRS2DfltControllerIdentifierInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetRRS2DfltControllerIdentifierLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_RRS2ControllerIdentifiers(CATBSTR & oRRS2ControllerIdentifiers); \
      virtual HRESULT __stdcall put_RRS2ControllerIdentifiers(const CATBSTR & iRRS2ControllerIdentifiers); \
      virtual HRESULT __stdcall GetRRS2ControllerIdentifiersInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetRRS2ControllerIdentifiersLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_RRS2ConnectDownloaders(CATBSTR & oRRS2ConnectDownloaders); \
      virtual HRESULT __stdcall put_RRS2ConnectDownloaders(const CATBSTR & iRRS2ConnectDownloaders); \
      virtual HRESULT __stdcall GetRRS2ConnectDownloadersInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetRRS2ConnectDownloadersLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_RRS2SimRunDownloaders(CATBSTR & oRRS2SimRunDownloaders); \
      virtual HRESULT __stdcall put_RRS2SimRunDownloaders(const CATBSTR & iRRS2SimRunDownloaders); \
      virtual HRESULT __stdcall GetRRS2SimRunDownloadersInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetRRS2SimRunDownloadersLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_RRS2ConsistencyCmdDownloaders(CATBSTR & oRRS2ConsistencyCmdDownloaders); \
      virtual HRESULT __stdcall put_RRS2ConsistencyCmdDownloaders(const CATBSTR & iRRS2ConsistencyCmdDownloaders); \
      virtual HRESULT __stdcall GetRRS2ConsistencyCmdDownloadersInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetRRS2ConsistencyCmdDownloadersLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_RRS2DownloadDuringConnect(CATBSTR & oRRS2DownloadDuringConnect); \
      virtual HRESULT __stdcall put_RRS2DownloadDuringConnect(const CATBSTR & iRRS2DownloadDuringConnect); \
      virtual HRESULT __stdcall GetRRS2DownloadDuringConnectInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetRRS2DownloadDuringConnectLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_RRS2DownloadDuringSimRun(CATBSTR & oRRS2DownloadDuringSimRun); \
      virtual HRESULT __stdcall put_RRS2DownloadDuringSimRun(const CATBSTR & iRRS2DownloadDuringSimRun); \
      virtual HRESULT __stdcall GetRRS2DownloadDuringSimRunInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetRRS2DownloadDuringSimRunLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_RRS2UpdateProgramOnDownload(CATBSTR & oRRS2UpdateProgramOnDownload); \
      virtual HRESULT __stdcall put_RRS2UpdateProgramOnDownload(const CATBSTR & iRRS2UpdateProgramOnDownload); \
      virtual HRESULT __stdcall GetRRS2UpdateProgramOnDownloadInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetRRS2UpdateProgramOnDownloadLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_RRS2DownloadInPartCoordinates(CATBSTR & oRRS2DownloadInPartCoordinates); \
      virtual HRESULT __stdcall put_RRS2DownloadInPartCoordinates(const CATBSTR & iRRS2DownloadInPartCoordinates); \
      virtual HRESULT __stdcall GetRRS2DownloadInPartCoordinatesInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetRRS2DownloadInPartCoordinatesLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall AddNewRRS2ControllerIdentifier(const CATBSTR & iControllerIdentifier); \
      virtual HRESULT __stdcall GetRRS2ControllerIdentifierAttributes(const CATBSTR & iControllerIdentifier, CATBSTR & oRRS2ConnectDownloader, CATBSTR & oRRS2SimRunDownloader, CATBSTR & oRRS2ConsistencyCmdDownloader, CAT_VARIANT_BOOL & oRRS2DownloadDuringConnect, CAT_VARIANT_BOOL & oRRS2DownloadDuringSimRun, CAT_VARIANT_BOOL & oRRS2UpdateProgramOnDownload, CAT_VARIANT_BOOL & oRRS2DownloadInPartCoordinates); \
      virtual HRESULT __stdcall get_RRS2UseVRCInverseKinematics(CAT_VARIANT_BOOL & oRRS2UseVRCInverseKinematics); \
      virtual HRESULT __stdcall put_RRS2UseVRCInverseKinematics(CAT_VARIANT_BOOL iRRS2UseVRCInverseKinematics); \
      virtual HRESULT __stdcall GetRRS2UseVRCInverseKinematicsInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetRRS2UseVRCInverseKinematicsLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_RRS2FreePlaySimStepSize(float & oRRS2FreePlaySimStepSize); \
      virtual HRESULT __stdcall put_RRS2FreePlaySimStepSize(float iRRS2FreePlaySimStepSize); \
      virtual HRESULT __stdcall GetRRS2FreePlaySimStepSizeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetRRS2FreePlaySimStepSizeLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_RRS2StopVRCWhenSimStops(CAT_VARIANT_BOOL & oRRS2StopVRCWhenSimStops); \
      virtual HRESULT __stdcall put_RRS2StopVRCWhenSimStops(CAT_VARIANT_BOOL iRRS2StopVRCWhenSimStops); \
      virtual HRESULT __stdcall GetRRS2StopVRCWhenSimStopsInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetRRS2StopVRCWhenSimStopsLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_RRS2MainTaskProcessing(CAT_VARIANT_BOOL & oRRS2MainTaskProcessing); \
      virtual HRESULT __stdcall put_RRS2MainTaskProcessing(CAT_VARIANT_BOOL iRRS2MainTaskProcessing); \
      virtual HRESULT __stdcall GetRRS2MainTaskProcessingInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetRRS2MainTaskProcessingLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_RRS2DeleteAutoDownloadFilesAfterXfer(CAT_VARIANT_BOOL & oRRS2DeleteAutoDownloadFilesAfterXfer); \
      virtual HRESULT __stdcall put_RRS2DeleteAutoDownloadFilesAfterXfer(CAT_VARIANT_BOOL iRRS2DeleteAutoDownloadFilesAfterXfer); \
      virtual HRESULT __stdcall GetRRS2DeleteAutoDownloadFilesAfterXferInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetRRS2DeleteAutoDownloadFilesAfterXferLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall Commit(); \
      virtual HRESULT __stdcall Rollback(); \
      virtual HRESULT __stdcall ResetToAdminValues(); \
      virtual HRESULT __stdcall ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList); \
      virtual HRESULT __stdcall SaveRepository(); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIARRSSettingAtt(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall get_RRSLibDir(CATBSTR & oLibDir); \
virtual HRESULT __stdcall put_RRSLibDir(const CATBSTR & iLibDir); \
virtual HRESULT __stdcall GetRRSLibDirInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetRRSLibDirLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_RRSServerFile(CATBSTR & oServerFile); \
virtual HRESULT __stdcall put_RRSServerFile(const CATBSTR & iServerFile); \
virtual HRESULT __stdcall GetRRSServerFileInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetRRSServerFileLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_RCSDataLibDir(CATBSTR & oDataDir); \
virtual HRESULT __stdcall put_RCSDataLibDir(const CATBSTR & iDataDir); \
virtual HRESULT __stdcall GetRCSDataLibDirInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetRCSDataLibDirLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_RRS2MotionPlannerActiveDuringTeachOLP(CAT_VARIANT_BOOL & oRRS2MotionPlannerActiveDuringTeachOLP); \
virtual HRESULT __stdcall put_RRS2MotionPlannerActiveDuringTeachOLP(CAT_VARIANT_BOOL iRRS2MotionPlannerActiveDuringTeachOLP); \
virtual HRESULT __stdcall GetRRS2MotionPlannerActiveDuringTeachOLPInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetRRS2MotionPlannerActiveDuringTeachOLPLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_RRS2UseVRCTurnLimits(CAT_VARIANT_BOOL & oRRS2UseVRCTurnLimits); \
virtual HRESULT __stdcall put_RRS2UseVRCTurnLimits(CAT_VARIANT_BOOL iRRS2UseVRCTurnLimits); \
virtual HRESULT __stdcall GetRRS2UseVRCTurnLimitsInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetRRS2UseVRCTurnLimitsLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_RRS2DownloadLogFileName(CATBSTR & oRRS2DownloadLogFileName); \
virtual HRESULT __stdcall put_RRS2DownloadLogFileName(const CATBSTR & iRRS2DownloadLogFileName); \
virtual HRESULT __stdcall GetRRS2DownloadLogFileNameInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetRRS2DownloadLogFileNameLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_RRS2DfltControllerIdentifier(CATBSTR & oRRS2DfltControllerIdentifier); \
virtual HRESULT __stdcall put_RRS2DfltControllerIdentifier(const CATBSTR & iRRS2DfltControllerIdentifier); \
virtual HRESULT __stdcall GetRRS2DfltControllerIdentifierInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetRRS2DfltControllerIdentifierLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_RRS2ControllerIdentifiers(CATBSTR & oRRS2ControllerIdentifiers); \
virtual HRESULT __stdcall put_RRS2ControllerIdentifiers(const CATBSTR & iRRS2ControllerIdentifiers); \
virtual HRESULT __stdcall GetRRS2ControllerIdentifiersInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetRRS2ControllerIdentifiersLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_RRS2ConnectDownloaders(CATBSTR & oRRS2ConnectDownloaders); \
virtual HRESULT __stdcall put_RRS2ConnectDownloaders(const CATBSTR & iRRS2ConnectDownloaders); \
virtual HRESULT __stdcall GetRRS2ConnectDownloadersInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetRRS2ConnectDownloadersLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_RRS2SimRunDownloaders(CATBSTR & oRRS2SimRunDownloaders); \
virtual HRESULT __stdcall put_RRS2SimRunDownloaders(const CATBSTR & iRRS2SimRunDownloaders); \
virtual HRESULT __stdcall GetRRS2SimRunDownloadersInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetRRS2SimRunDownloadersLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_RRS2ConsistencyCmdDownloaders(CATBSTR & oRRS2ConsistencyCmdDownloaders); \
virtual HRESULT __stdcall put_RRS2ConsistencyCmdDownloaders(const CATBSTR & iRRS2ConsistencyCmdDownloaders); \
virtual HRESULT __stdcall GetRRS2ConsistencyCmdDownloadersInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetRRS2ConsistencyCmdDownloadersLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_RRS2DownloadDuringConnect(CATBSTR & oRRS2DownloadDuringConnect); \
virtual HRESULT __stdcall put_RRS2DownloadDuringConnect(const CATBSTR & iRRS2DownloadDuringConnect); \
virtual HRESULT __stdcall GetRRS2DownloadDuringConnectInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetRRS2DownloadDuringConnectLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_RRS2DownloadDuringSimRun(CATBSTR & oRRS2DownloadDuringSimRun); \
virtual HRESULT __stdcall put_RRS2DownloadDuringSimRun(const CATBSTR & iRRS2DownloadDuringSimRun); \
virtual HRESULT __stdcall GetRRS2DownloadDuringSimRunInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetRRS2DownloadDuringSimRunLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_RRS2UpdateProgramOnDownload(CATBSTR & oRRS2UpdateProgramOnDownload); \
virtual HRESULT __stdcall put_RRS2UpdateProgramOnDownload(const CATBSTR & iRRS2UpdateProgramOnDownload); \
virtual HRESULT __stdcall GetRRS2UpdateProgramOnDownloadInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetRRS2UpdateProgramOnDownloadLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_RRS2DownloadInPartCoordinates(CATBSTR & oRRS2DownloadInPartCoordinates); \
virtual HRESULT __stdcall put_RRS2DownloadInPartCoordinates(const CATBSTR & iRRS2DownloadInPartCoordinates); \
virtual HRESULT __stdcall GetRRS2DownloadInPartCoordinatesInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetRRS2DownloadInPartCoordinatesLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall AddNewRRS2ControllerIdentifier(const CATBSTR & iControllerIdentifier); \
virtual HRESULT __stdcall GetRRS2ControllerIdentifierAttributes(const CATBSTR & iControllerIdentifier, CATBSTR & oRRS2ConnectDownloader, CATBSTR & oRRS2SimRunDownloader, CATBSTR & oRRS2ConsistencyCmdDownloader, CAT_VARIANT_BOOL & oRRS2DownloadDuringConnect, CAT_VARIANT_BOOL & oRRS2DownloadDuringSimRun, CAT_VARIANT_BOOL & oRRS2UpdateProgramOnDownload, CAT_VARIANT_BOOL & oRRS2DownloadInPartCoordinates); \
virtual HRESULT __stdcall get_RRS2UseVRCInverseKinematics(CAT_VARIANT_BOOL & oRRS2UseVRCInverseKinematics); \
virtual HRESULT __stdcall put_RRS2UseVRCInverseKinematics(CAT_VARIANT_BOOL iRRS2UseVRCInverseKinematics); \
virtual HRESULT __stdcall GetRRS2UseVRCInverseKinematicsInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetRRS2UseVRCInverseKinematicsLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_RRS2FreePlaySimStepSize(float & oRRS2FreePlaySimStepSize); \
virtual HRESULT __stdcall put_RRS2FreePlaySimStepSize(float iRRS2FreePlaySimStepSize); \
virtual HRESULT __stdcall GetRRS2FreePlaySimStepSizeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetRRS2FreePlaySimStepSizeLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_RRS2StopVRCWhenSimStops(CAT_VARIANT_BOOL & oRRS2StopVRCWhenSimStops); \
virtual HRESULT __stdcall put_RRS2StopVRCWhenSimStops(CAT_VARIANT_BOOL iRRS2StopVRCWhenSimStops); \
virtual HRESULT __stdcall GetRRS2StopVRCWhenSimStopsInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetRRS2StopVRCWhenSimStopsLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_RRS2MainTaskProcessing(CAT_VARIANT_BOOL & oRRS2MainTaskProcessing); \
virtual HRESULT __stdcall put_RRS2MainTaskProcessing(CAT_VARIANT_BOOL iRRS2MainTaskProcessing); \
virtual HRESULT __stdcall GetRRS2MainTaskProcessingInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetRRS2MainTaskProcessingLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_RRS2DeleteAutoDownloadFilesAfterXfer(CAT_VARIANT_BOOL & oRRS2DeleteAutoDownloadFilesAfterXfer); \
virtual HRESULT __stdcall put_RRS2DeleteAutoDownloadFilesAfterXfer(CAT_VARIANT_BOOL iRRS2DeleteAutoDownloadFilesAfterXfer); \
virtual HRESULT __stdcall GetRRS2DeleteAutoDownloadFilesAfterXferInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetRRS2DeleteAutoDownloadFilesAfterXferLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall Commit(); \
virtual HRESULT __stdcall Rollback(); \
virtual HRESULT __stdcall ResetToAdminValues(); \
virtual HRESULT __stdcall ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList); \
virtual HRESULT __stdcall SaveRepository(); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIARRSSettingAtt(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::get_RRSLibDir(CATBSTR & oLibDir) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_RRSLibDir(oLibDir)); \
} \
HRESULT __stdcall  ENVTIEName::put_RRSLibDir(const CATBSTR & iLibDir) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_RRSLibDir(iLibDir)); \
} \
HRESULT __stdcall  ENVTIEName::GetRRSLibDirInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetRRSLibDirInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetRRSLibDirLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetRRSLibDirLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_RRSServerFile(CATBSTR & oServerFile) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_RRSServerFile(oServerFile)); \
} \
HRESULT __stdcall  ENVTIEName::put_RRSServerFile(const CATBSTR & iServerFile) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_RRSServerFile(iServerFile)); \
} \
HRESULT __stdcall  ENVTIEName::GetRRSServerFileInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetRRSServerFileInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetRRSServerFileLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetRRSServerFileLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_RCSDataLibDir(CATBSTR & oDataDir) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_RCSDataLibDir(oDataDir)); \
} \
HRESULT __stdcall  ENVTIEName::put_RCSDataLibDir(const CATBSTR & iDataDir) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_RCSDataLibDir(iDataDir)); \
} \
HRESULT __stdcall  ENVTIEName::GetRCSDataLibDirInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetRCSDataLibDirInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetRCSDataLibDirLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetRCSDataLibDirLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_RRS2MotionPlannerActiveDuringTeachOLP(CAT_VARIANT_BOOL & oRRS2MotionPlannerActiveDuringTeachOLP) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_RRS2MotionPlannerActiveDuringTeachOLP(oRRS2MotionPlannerActiveDuringTeachOLP)); \
} \
HRESULT __stdcall  ENVTIEName::put_RRS2MotionPlannerActiveDuringTeachOLP(CAT_VARIANT_BOOL iRRS2MotionPlannerActiveDuringTeachOLP) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_RRS2MotionPlannerActiveDuringTeachOLP(iRRS2MotionPlannerActiveDuringTeachOLP)); \
} \
HRESULT __stdcall  ENVTIEName::GetRRS2MotionPlannerActiveDuringTeachOLPInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetRRS2MotionPlannerActiveDuringTeachOLPInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetRRS2MotionPlannerActiveDuringTeachOLPLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetRRS2MotionPlannerActiveDuringTeachOLPLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_RRS2UseVRCTurnLimits(CAT_VARIANT_BOOL & oRRS2UseVRCTurnLimits) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_RRS2UseVRCTurnLimits(oRRS2UseVRCTurnLimits)); \
} \
HRESULT __stdcall  ENVTIEName::put_RRS2UseVRCTurnLimits(CAT_VARIANT_BOOL iRRS2UseVRCTurnLimits) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_RRS2UseVRCTurnLimits(iRRS2UseVRCTurnLimits)); \
} \
HRESULT __stdcall  ENVTIEName::GetRRS2UseVRCTurnLimitsInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetRRS2UseVRCTurnLimitsInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetRRS2UseVRCTurnLimitsLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetRRS2UseVRCTurnLimitsLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_RRS2DownloadLogFileName(CATBSTR & oRRS2DownloadLogFileName) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_RRS2DownloadLogFileName(oRRS2DownloadLogFileName)); \
} \
HRESULT __stdcall  ENVTIEName::put_RRS2DownloadLogFileName(const CATBSTR & iRRS2DownloadLogFileName) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_RRS2DownloadLogFileName(iRRS2DownloadLogFileName)); \
} \
HRESULT __stdcall  ENVTIEName::GetRRS2DownloadLogFileNameInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetRRS2DownloadLogFileNameInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetRRS2DownloadLogFileNameLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetRRS2DownloadLogFileNameLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_RRS2DfltControllerIdentifier(CATBSTR & oRRS2DfltControllerIdentifier) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_RRS2DfltControllerIdentifier(oRRS2DfltControllerIdentifier)); \
} \
HRESULT __stdcall  ENVTIEName::put_RRS2DfltControllerIdentifier(const CATBSTR & iRRS2DfltControllerIdentifier) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_RRS2DfltControllerIdentifier(iRRS2DfltControllerIdentifier)); \
} \
HRESULT __stdcall  ENVTIEName::GetRRS2DfltControllerIdentifierInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetRRS2DfltControllerIdentifierInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetRRS2DfltControllerIdentifierLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetRRS2DfltControllerIdentifierLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_RRS2ControllerIdentifiers(CATBSTR & oRRS2ControllerIdentifiers) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_RRS2ControllerIdentifiers(oRRS2ControllerIdentifiers)); \
} \
HRESULT __stdcall  ENVTIEName::put_RRS2ControllerIdentifiers(const CATBSTR & iRRS2ControllerIdentifiers) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_RRS2ControllerIdentifiers(iRRS2ControllerIdentifiers)); \
} \
HRESULT __stdcall  ENVTIEName::GetRRS2ControllerIdentifiersInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetRRS2ControllerIdentifiersInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetRRS2ControllerIdentifiersLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetRRS2ControllerIdentifiersLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_RRS2ConnectDownloaders(CATBSTR & oRRS2ConnectDownloaders) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_RRS2ConnectDownloaders(oRRS2ConnectDownloaders)); \
} \
HRESULT __stdcall  ENVTIEName::put_RRS2ConnectDownloaders(const CATBSTR & iRRS2ConnectDownloaders) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_RRS2ConnectDownloaders(iRRS2ConnectDownloaders)); \
} \
HRESULT __stdcall  ENVTIEName::GetRRS2ConnectDownloadersInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetRRS2ConnectDownloadersInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetRRS2ConnectDownloadersLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetRRS2ConnectDownloadersLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_RRS2SimRunDownloaders(CATBSTR & oRRS2SimRunDownloaders) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_RRS2SimRunDownloaders(oRRS2SimRunDownloaders)); \
} \
HRESULT __stdcall  ENVTIEName::put_RRS2SimRunDownloaders(const CATBSTR & iRRS2SimRunDownloaders) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_RRS2SimRunDownloaders(iRRS2SimRunDownloaders)); \
} \
HRESULT __stdcall  ENVTIEName::GetRRS2SimRunDownloadersInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetRRS2SimRunDownloadersInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetRRS2SimRunDownloadersLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetRRS2SimRunDownloadersLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_RRS2ConsistencyCmdDownloaders(CATBSTR & oRRS2ConsistencyCmdDownloaders) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_RRS2ConsistencyCmdDownloaders(oRRS2ConsistencyCmdDownloaders)); \
} \
HRESULT __stdcall  ENVTIEName::put_RRS2ConsistencyCmdDownloaders(const CATBSTR & iRRS2ConsistencyCmdDownloaders) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_RRS2ConsistencyCmdDownloaders(iRRS2ConsistencyCmdDownloaders)); \
} \
HRESULT __stdcall  ENVTIEName::GetRRS2ConsistencyCmdDownloadersInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetRRS2ConsistencyCmdDownloadersInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetRRS2ConsistencyCmdDownloadersLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetRRS2ConsistencyCmdDownloadersLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_RRS2DownloadDuringConnect(CATBSTR & oRRS2DownloadDuringConnect) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_RRS2DownloadDuringConnect(oRRS2DownloadDuringConnect)); \
} \
HRESULT __stdcall  ENVTIEName::put_RRS2DownloadDuringConnect(const CATBSTR & iRRS2DownloadDuringConnect) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_RRS2DownloadDuringConnect(iRRS2DownloadDuringConnect)); \
} \
HRESULT __stdcall  ENVTIEName::GetRRS2DownloadDuringConnectInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetRRS2DownloadDuringConnectInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetRRS2DownloadDuringConnectLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetRRS2DownloadDuringConnectLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_RRS2DownloadDuringSimRun(CATBSTR & oRRS2DownloadDuringSimRun) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_RRS2DownloadDuringSimRun(oRRS2DownloadDuringSimRun)); \
} \
HRESULT __stdcall  ENVTIEName::put_RRS2DownloadDuringSimRun(const CATBSTR & iRRS2DownloadDuringSimRun) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_RRS2DownloadDuringSimRun(iRRS2DownloadDuringSimRun)); \
} \
HRESULT __stdcall  ENVTIEName::GetRRS2DownloadDuringSimRunInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetRRS2DownloadDuringSimRunInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetRRS2DownloadDuringSimRunLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetRRS2DownloadDuringSimRunLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_RRS2UpdateProgramOnDownload(CATBSTR & oRRS2UpdateProgramOnDownload) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_RRS2UpdateProgramOnDownload(oRRS2UpdateProgramOnDownload)); \
} \
HRESULT __stdcall  ENVTIEName::put_RRS2UpdateProgramOnDownload(const CATBSTR & iRRS2UpdateProgramOnDownload) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_RRS2UpdateProgramOnDownload(iRRS2UpdateProgramOnDownload)); \
} \
HRESULT __stdcall  ENVTIEName::GetRRS2UpdateProgramOnDownloadInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetRRS2UpdateProgramOnDownloadInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetRRS2UpdateProgramOnDownloadLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetRRS2UpdateProgramOnDownloadLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_RRS2DownloadInPartCoordinates(CATBSTR & oRRS2DownloadInPartCoordinates) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_RRS2DownloadInPartCoordinates(oRRS2DownloadInPartCoordinates)); \
} \
HRESULT __stdcall  ENVTIEName::put_RRS2DownloadInPartCoordinates(const CATBSTR & iRRS2DownloadInPartCoordinates) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_RRS2DownloadInPartCoordinates(iRRS2DownloadInPartCoordinates)); \
} \
HRESULT __stdcall  ENVTIEName::GetRRS2DownloadInPartCoordinatesInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetRRS2DownloadInPartCoordinatesInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetRRS2DownloadInPartCoordinatesLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetRRS2DownloadInPartCoordinatesLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::AddNewRRS2ControllerIdentifier(const CATBSTR & iControllerIdentifier) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)AddNewRRS2ControllerIdentifier(iControllerIdentifier)); \
} \
HRESULT __stdcall  ENVTIEName::GetRRS2ControllerIdentifierAttributes(const CATBSTR & iControllerIdentifier, CATBSTR & oRRS2ConnectDownloader, CATBSTR & oRRS2SimRunDownloader, CATBSTR & oRRS2ConsistencyCmdDownloader, CAT_VARIANT_BOOL & oRRS2DownloadDuringConnect, CAT_VARIANT_BOOL & oRRS2DownloadDuringSimRun, CAT_VARIANT_BOOL & oRRS2UpdateProgramOnDownload, CAT_VARIANT_BOOL & oRRS2DownloadInPartCoordinates) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetRRS2ControllerIdentifierAttributes(iControllerIdentifier,oRRS2ConnectDownloader,oRRS2SimRunDownloader,oRRS2ConsistencyCmdDownloader,oRRS2DownloadDuringConnect,oRRS2DownloadDuringSimRun,oRRS2UpdateProgramOnDownload,oRRS2DownloadInPartCoordinates)); \
} \
HRESULT __stdcall  ENVTIEName::get_RRS2UseVRCInverseKinematics(CAT_VARIANT_BOOL & oRRS2UseVRCInverseKinematics) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_RRS2UseVRCInverseKinematics(oRRS2UseVRCInverseKinematics)); \
} \
HRESULT __stdcall  ENVTIEName::put_RRS2UseVRCInverseKinematics(CAT_VARIANT_BOOL iRRS2UseVRCInverseKinematics) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_RRS2UseVRCInverseKinematics(iRRS2UseVRCInverseKinematics)); \
} \
HRESULT __stdcall  ENVTIEName::GetRRS2UseVRCInverseKinematicsInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetRRS2UseVRCInverseKinematicsInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetRRS2UseVRCInverseKinematicsLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetRRS2UseVRCInverseKinematicsLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_RRS2FreePlaySimStepSize(float & oRRS2FreePlaySimStepSize) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_RRS2FreePlaySimStepSize(oRRS2FreePlaySimStepSize)); \
} \
HRESULT __stdcall  ENVTIEName::put_RRS2FreePlaySimStepSize(float iRRS2FreePlaySimStepSize) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_RRS2FreePlaySimStepSize(iRRS2FreePlaySimStepSize)); \
} \
HRESULT __stdcall  ENVTIEName::GetRRS2FreePlaySimStepSizeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetRRS2FreePlaySimStepSizeInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetRRS2FreePlaySimStepSizeLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetRRS2FreePlaySimStepSizeLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_RRS2StopVRCWhenSimStops(CAT_VARIANT_BOOL & oRRS2StopVRCWhenSimStops) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_RRS2StopVRCWhenSimStops(oRRS2StopVRCWhenSimStops)); \
} \
HRESULT __stdcall  ENVTIEName::put_RRS2StopVRCWhenSimStops(CAT_VARIANT_BOOL iRRS2StopVRCWhenSimStops) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_RRS2StopVRCWhenSimStops(iRRS2StopVRCWhenSimStops)); \
} \
HRESULT __stdcall  ENVTIEName::GetRRS2StopVRCWhenSimStopsInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetRRS2StopVRCWhenSimStopsInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetRRS2StopVRCWhenSimStopsLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetRRS2StopVRCWhenSimStopsLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_RRS2MainTaskProcessing(CAT_VARIANT_BOOL & oRRS2MainTaskProcessing) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_RRS2MainTaskProcessing(oRRS2MainTaskProcessing)); \
} \
HRESULT __stdcall  ENVTIEName::put_RRS2MainTaskProcessing(CAT_VARIANT_BOOL iRRS2MainTaskProcessing) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_RRS2MainTaskProcessing(iRRS2MainTaskProcessing)); \
} \
HRESULT __stdcall  ENVTIEName::GetRRS2MainTaskProcessingInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetRRS2MainTaskProcessingInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetRRS2MainTaskProcessingLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetRRS2MainTaskProcessingLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_RRS2DeleteAutoDownloadFilesAfterXfer(CAT_VARIANT_BOOL & oRRS2DeleteAutoDownloadFilesAfterXfer) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_RRS2DeleteAutoDownloadFilesAfterXfer(oRRS2DeleteAutoDownloadFilesAfterXfer)); \
} \
HRESULT __stdcall  ENVTIEName::put_RRS2DeleteAutoDownloadFilesAfterXfer(CAT_VARIANT_BOOL iRRS2DeleteAutoDownloadFilesAfterXfer) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_RRS2DeleteAutoDownloadFilesAfterXfer(iRRS2DeleteAutoDownloadFilesAfterXfer)); \
} \
HRESULT __stdcall  ENVTIEName::GetRRS2DeleteAutoDownloadFilesAfterXferInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetRRS2DeleteAutoDownloadFilesAfterXferInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetRRS2DeleteAutoDownloadFilesAfterXferLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetRRS2DeleteAutoDownloadFilesAfterXferLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::Commit() \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)Commit()); \
} \
HRESULT __stdcall  ENVTIEName::Rollback() \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)Rollback()); \
} \
HRESULT __stdcall  ENVTIEName::ResetToAdminValues() \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)ResetToAdminValues()); \
} \
HRESULT __stdcall  ENVTIEName::ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)ResetToAdminValuesByName(iAttList)); \
} \
HRESULT __stdcall  ENVTIEName::SaveRepository() \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)SaveRepository()); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIARRSSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIARRSSettingAtt(classe)    TIEDNBIARRSSettingAtt##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIARRSSettingAtt(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIARRSSettingAtt, classe) \
 \
 \
CATImplementTIEMethods(DNBIARRSSettingAtt, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIARRSSettingAtt, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIARRSSettingAtt, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIARRSSettingAtt, classe) \
 \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::get_RRSLibDir(CATBSTR & oLibDir) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&oLibDir); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_RRSLibDir(oLibDir); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&oLibDir); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::put_RRSLibDir(const CATBSTR & iLibDir) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&iLibDir); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_RRSLibDir(iLibDir); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&iLibDir); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::GetRRSLibDirInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetRRSLibDirInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::SetRRSLibDirLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetRRSLibDirLock(iLocked); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::get_RRSServerFile(CATBSTR & oServerFile) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&oServerFile); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_RRSServerFile(oServerFile); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&oServerFile); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::put_RRSServerFile(const CATBSTR & iServerFile) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&iServerFile); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_RRSServerFile(iServerFile); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&iServerFile); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::GetRRSServerFileInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetRRSServerFileInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::SetRRSServerFileLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetRRSServerFileLock(iLocked); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::get_RCSDataLibDir(CATBSTR & oDataDir) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&oDataDir); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_RCSDataLibDir(oDataDir); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&oDataDir); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::put_RCSDataLibDir(const CATBSTR & iDataDir) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&iDataDir); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_RCSDataLibDir(iDataDir); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&iDataDir); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::GetRCSDataLibDirInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,11,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetRCSDataLibDirInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,11,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::SetRCSDataLibDirLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,12,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetRCSDataLibDirLock(iLocked); \
   ExitAfterCall(this,12,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::get_RRS2MotionPlannerActiveDuringTeachOLP(CAT_VARIANT_BOOL & oRRS2MotionPlannerActiveDuringTeachOLP) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,13,&_Trac2,&oRRS2MotionPlannerActiveDuringTeachOLP); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_RRS2MotionPlannerActiveDuringTeachOLP(oRRS2MotionPlannerActiveDuringTeachOLP); \
   ExitAfterCall(this,13,_Trac2,&_ret_arg,&oRRS2MotionPlannerActiveDuringTeachOLP); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::put_RRS2MotionPlannerActiveDuringTeachOLP(CAT_VARIANT_BOOL iRRS2MotionPlannerActiveDuringTeachOLP) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,14,&_Trac2,&iRRS2MotionPlannerActiveDuringTeachOLP); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_RRS2MotionPlannerActiveDuringTeachOLP(iRRS2MotionPlannerActiveDuringTeachOLP); \
   ExitAfterCall(this,14,_Trac2,&_ret_arg,&iRRS2MotionPlannerActiveDuringTeachOLP); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::GetRRS2MotionPlannerActiveDuringTeachOLPInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,15,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetRRS2MotionPlannerActiveDuringTeachOLPInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,15,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::SetRRS2MotionPlannerActiveDuringTeachOLPLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,16,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetRRS2MotionPlannerActiveDuringTeachOLPLock(iLocked); \
   ExitAfterCall(this,16,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::get_RRS2UseVRCTurnLimits(CAT_VARIANT_BOOL & oRRS2UseVRCTurnLimits) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,17,&_Trac2,&oRRS2UseVRCTurnLimits); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_RRS2UseVRCTurnLimits(oRRS2UseVRCTurnLimits); \
   ExitAfterCall(this,17,_Trac2,&_ret_arg,&oRRS2UseVRCTurnLimits); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::put_RRS2UseVRCTurnLimits(CAT_VARIANT_BOOL iRRS2UseVRCTurnLimits) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,18,&_Trac2,&iRRS2UseVRCTurnLimits); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_RRS2UseVRCTurnLimits(iRRS2UseVRCTurnLimits); \
   ExitAfterCall(this,18,_Trac2,&_ret_arg,&iRRS2UseVRCTurnLimits); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::GetRRS2UseVRCTurnLimitsInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,19,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetRRS2UseVRCTurnLimitsInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,19,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::SetRRS2UseVRCTurnLimitsLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,20,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetRRS2UseVRCTurnLimitsLock(iLocked); \
   ExitAfterCall(this,20,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::get_RRS2DownloadLogFileName(CATBSTR & oRRS2DownloadLogFileName) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,21,&_Trac2,&oRRS2DownloadLogFileName); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_RRS2DownloadLogFileName(oRRS2DownloadLogFileName); \
   ExitAfterCall(this,21,_Trac2,&_ret_arg,&oRRS2DownloadLogFileName); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::put_RRS2DownloadLogFileName(const CATBSTR & iRRS2DownloadLogFileName) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,22,&_Trac2,&iRRS2DownloadLogFileName); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_RRS2DownloadLogFileName(iRRS2DownloadLogFileName); \
   ExitAfterCall(this,22,_Trac2,&_ret_arg,&iRRS2DownloadLogFileName); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::GetRRS2DownloadLogFileNameInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,23,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetRRS2DownloadLogFileNameInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,23,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::SetRRS2DownloadLogFileNameLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,24,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetRRS2DownloadLogFileNameLock(iLocked); \
   ExitAfterCall(this,24,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::get_RRS2DfltControllerIdentifier(CATBSTR & oRRS2DfltControllerIdentifier) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,25,&_Trac2,&oRRS2DfltControllerIdentifier); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_RRS2DfltControllerIdentifier(oRRS2DfltControllerIdentifier); \
   ExitAfterCall(this,25,_Trac2,&_ret_arg,&oRRS2DfltControllerIdentifier); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::put_RRS2DfltControllerIdentifier(const CATBSTR & iRRS2DfltControllerIdentifier) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,26,&_Trac2,&iRRS2DfltControllerIdentifier); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_RRS2DfltControllerIdentifier(iRRS2DfltControllerIdentifier); \
   ExitAfterCall(this,26,_Trac2,&_ret_arg,&iRRS2DfltControllerIdentifier); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::GetRRS2DfltControllerIdentifierInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,27,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetRRS2DfltControllerIdentifierInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,27,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::SetRRS2DfltControllerIdentifierLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,28,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetRRS2DfltControllerIdentifierLock(iLocked); \
   ExitAfterCall(this,28,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::get_RRS2ControllerIdentifiers(CATBSTR & oRRS2ControllerIdentifiers) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,29,&_Trac2,&oRRS2ControllerIdentifiers); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_RRS2ControllerIdentifiers(oRRS2ControllerIdentifiers); \
   ExitAfterCall(this,29,_Trac2,&_ret_arg,&oRRS2ControllerIdentifiers); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::put_RRS2ControllerIdentifiers(const CATBSTR & iRRS2ControllerIdentifiers) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,30,&_Trac2,&iRRS2ControllerIdentifiers); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_RRS2ControllerIdentifiers(iRRS2ControllerIdentifiers); \
   ExitAfterCall(this,30,_Trac2,&_ret_arg,&iRRS2ControllerIdentifiers); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::GetRRS2ControllerIdentifiersInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,31,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetRRS2ControllerIdentifiersInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,31,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::SetRRS2ControllerIdentifiersLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,32,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetRRS2ControllerIdentifiersLock(iLocked); \
   ExitAfterCall(this,32,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::get_RRS2ConnectDownloaders(CATBSTR & oRRS2ConnectDownloaders) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,33,&_Trac2,&oRRS2ConnectDownloaders); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_RRS2ConnectDownloaders(oRRS2ConnectDownloaders); \
   ExitAfterCall(this,33,_Trac2,&_ret_arg,&oRRS2ConnectDownloaders); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::put_RRS2ConnectDownloaders(const CATBSTR & iRRS2ConnectDownloaders) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,34,&_Trac2,&iRRS2ConnectDownloaders); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_RRS2ConnectDownloaders(iRRS2ConnectDownloaders); \
   ExitAfterCall(this,34,_Trac2,&_ret_arg,&iRRS2ConnectDownloaders); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::GetRRS2ConnectDownloadersInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,35,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetRRS2ConnectDownloadersInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,35,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::SetRRS2ConnectDownloadersLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,36,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetRRS2ConnectDownloadersLock(iLocked); \
   ExitAfterCall(this,36,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::get_RRS2SimRunDownloaders(CATBSTR & oRRS2SimRunDownloaders) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,37,&_Trac2,&oRRS2SimRunDownloaders); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_RRS2SimRunDownloaders(oRRS2SimRunDownloaders); \
   ExitAfterCall(this,37,_Trac2,&_ret_arg,&oRRS2SimRunDownloaders); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::put_RRS2SimRunDownloaders(const CATBSTR & iRRS2SimRunDownloaders) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,38,&_Trac2,&iRRS2SimRunDownloaders); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_RRS2SimRunDownloaders(iRRS2SimRunDownloaders); \
   ExitAfterCall(this,38,_Trac2,&_ret_arg,&iRRS2SimRunDownloaders); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::GetRRS2SimRunDownloadersInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,39,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetRRS2SimRunDownloadersInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,39,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::SetRRS2SimRunDownloadersLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,40,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetRRS2SimRunDownloadersLock(iLocked); \
   ExitAfterCall(this,40,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::get_RRS2ConsistencyCmdDownloaders(CATBSTR & oRRS2ConsistencyCmdDownloaders) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,41,&_Trac2,&oRRS2ConsistencyCmdDownloaders); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_RRS2ConsistencyCmdDownloaders(oRRS2ConsistencyCmdDownloaders); \
   ExitAfterCall(this,41,_Trac2,&_ret_arg,&oRRS2ConsistencyCmdDownloaders); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::put_RRS2ConsistencyCmdDownloaders(const CATBSTR & iRRS2ConsistencyCmdDownloaders) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,42,&_Trac2,&iRRS2ConsistencyCmdDownloaders); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_RRS2ConsistencyCmdDownloaders(iRRS2ConsistencyCmdDownloaders); \
   ExitAfterCall(this,42,_Trac2,&_ret_arg,&iRRS2ConsistencyCmdDownloaders); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::GetRRS2ConsistencyCmdDownloadersInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,43,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetRRS2ConsistencyCmdDownloadersInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,43,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::SetRRS2ConsistencyCmdDownloadersLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,44,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetRRS2ConsistencyCmdDownloadersLock(iLocked); \
   ExitAfterCall(this,44,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::get_RRS2DownloadDuringConnect(CATBSTR & oRRS2DownloadDuringConnect) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,45,&_Trac2,&oRRS2DownloadDuringConnect); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_RRS2DownloadDuringConnect(oRRS2DownloadDuringConnect); \
   ExitAfterCall(this,45,_Trac2,&_ret_arg,&oRRS2DownloadDuringConnect); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::put_RRS2DownloadDuringConnect(const CATBSTR & iRRS2DownloadDuringConnect) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,46,&_Trac2,&iRRS2DownloadDuringConnect); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_RRS2DownloadDuringConnect(iRRS2DownloadDuringConnect); \
   ExitAfterCall(this,46,_Trac2,&_ret_arg,&iRRS2DownloadDuringConnect); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::GetRRS2DownloadDuringConnectInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,47,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetRRS2DownloadDuringConnectInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,47,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::SetRRS2DownloadDuringConnectLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,48,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetRRS2DownloadDuringConnectLock(iLocked); \
   ExitAfterCall(this,48,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::get_RRS2DownloadDuringSimRun(CATBSTR & oRRS2DownloadDuringSimRun) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,49,&_Trac2,&oRRS2DownloadDuringSimRun); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_RRS2DownloadDuringSimRun(oRRS2DownloadDuringSimRun); \
   ExitAfterCall(this,49,_Trac2,&_ret_arg,&oRRS2DownloadDuringSimRun); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::put_RRS2DownloadDuringSimRun(const CATBSTR & iRRS2DownloadDuringSimRun) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,50,&_Trac2,&iRRS2DownloadDuringSimRun); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_RRS2DownloadDuringSimRun(iRRS2DownloadDuringSimRun); \
   ExitAfterCall(this,50,_Trac2,&_ret_arg,&iRRS2DownloadDuringSimRun); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::GetRRS2DownloadDuringSimRunInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,51,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetRRS2DownloadDuringSimRunInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,51,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::SetRRS2DownloadDuringSimRunLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,52,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetRRS2DownloadDuringSimRunLock(iLocked); \
   ExitAfterCall(this,52,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::get_RRS2UpdateProgramOnDownload(CATBSTR & oRRS2UpdateProgramOnDownload) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,53,&_Trac2,&oRRS2UpdateProgramOnDownload); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_RRS2UpdateProgramOnDownload(oRRS2UpdateProgramOnDownload); \
   ExitAfterCall(this,53,_Trac2,&_ret_arg,&oRRS2UpdateProgramOnDownload); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::put_RRS2UpdateProgramOnDownload(const CATBSTR & iRRS2UpdateProgramOnDownload) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,54,&_Trac2,&iRRS2UpdateProgramOnDownload); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_RRS2UpdateProgramOnDownload(iRRS2UpdateProgramOnDownload); \
   ExitAfterCall(this,54,_Trac2,&_ret_arg,&iRRS2UpdateProgramOnDownload); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::GetRRS2UpdateProgramOnDownloadInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,55,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetRRS2UpdateProgramOnDownloadInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,55,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::SetRRS2UpdateProgramOnDownloadLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,56,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetRRS2UpdateProgramOnDownloadLock(iLocked); \
   ExitAfterCall(this,56,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::get_RRS2DownloadInPartCoordinates(CATBSTR & oRRS2DownloadInPartCoordinates) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,57,&_Trac2,&oRRS2DownloadInPartCoordinates); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_RRS2DownloadInPartCoordinates(oRRS2DownloadInPartCoordinates); \
   ExitAfterCall(this,57,_Trac2,&_ret_arg,&oRRS2DownloadInPartCoordinates); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::put_RRS2DownloadInPartCoordinates(const CATBSTR & iRRS2DownloadInPartCoordinates) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,58,&_Trac2,&iRRS2DownloadInPartCoordinates); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_RRS2DownloadInPartCoordinates(iRRS2DownloadInPartCoordinates); \
   ExitAfterCall(this,58,_Trac2,&_ret_arg,&iRRS2DownloadInPartCoordinates); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::GetRRS2DownloadInPartCoordinatesInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,59,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetRRS2DownloadInPartCoordinatesInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,59,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::SetRRS2DownloadInPartCoordinatesLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,60,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetRRS2DownloadInPartCoordinatesLock(iLocked); \
   ExitAfterCall(this,60,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::AddNewRRS2ControllerIdentifier(const CATBSTR & iControllerIdentifier) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,61,&_Trac2,&iControllerIdentifier); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->AddNewRRS2ControllerIdentifier(iControllerIdentifier); \
   ExitAfterCall(this,61,_Trac2,&_ret_arg,&iControllerIdentifier); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::GetRRS2ControllerIdentifierAttributes(const CATBSTR & iControllerIdentifier, CATBSTR & oRRS2ConnectDownloader, CATBSTR & oRRS2SimRunDownloader, CATBSTR & oRRS2ConsistencyCmdDownloader, CAT_VARIANT_BOOL & oRRS2DownloadDuringConnect, CAT_VARIANT_BOOL & oRRS2DownloadDuringSimRun, CAT_VARIANT_BOOL & oRRS2UpdateProgramOnDownload, CAT_VARIANT_BOOL & oRRS2DownloadInPartCoordinates) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,62,&_Trac2,&iControllerIdentifier,&oRRS2ConnectDownloader,&oRRS2SimRunDownloader,&oRRS2ConsistencyCmdDownloader,&oRRS2DownloadDuringConnect,&oRRS2DownloadDuringSimRun,&oRRS2UpdateProgramOnDownload,&oRRS2DownloadInPartCoordinates); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetRRS2ControllerIdentifierAttributes(iControllerIdentifier,oRRS2ConnectDownloader,oRRS2SimRunDownloader,oRRS2ConsistencyCmdDownloader,oRRS2DownloadDuringConnect,oRRS2DownloadDuringSimRun,oRRS2UpdateProgramOnDownload,oRRS2DownloadInPartCoordinates); \
   ExitAfterCall(this,62,_Trac2,&_ret_arg,&iControllerIdentifier,&oRRS2ConnectDownloader,&oRRS2SimRunDownloader,&oRRS2ConsistencyCmdDownloader,&oRRS2DownloadDuringConnect,&oRRS2DownloadDuringSimRun,&oRRS2UpdateProgramOnDownload,&oRRS2DownloadInPartCoordinates); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::get_RRS2UseVRCInverseKinematics(CAT_VARIANT_BOOL & oRRS2UseVRCInverseKinematics) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,63,&_Trac2,&oRRS2UseVRCInverseKinematics); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_RRS2UseVRCInverseKinematics(oRRS2UseVRCInverseKinematics); \
   ExitAfterCall(this,63,_Trac2,&_ret_arg,&oRRS2UseVRCInverseKinematics); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::put_RRS2UseVRCInverseKinematics(CAT_VARIANT_BOOL iRRS2UseVRCInverseKinematics) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,64,&_Trac2,&iRRS2UseVRCInverseKinematics); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_RRS2UseVRCInverseKinematics(iRRS2UseVRCInverseKinematics); \
   ExitAfterCall(this,64,_Trac2,&_ret_arg,&iRRS2UseVRCInverseKinematics); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::GetRRS2UseVRCInverseKinematicsInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,65,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetRRS2UseVRCInverseKinematicsInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,65,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::SetRRS2UseVRCInverseKinematicsLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,66,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetRRS2UseVRCInverseKinematicsLock(iLocked); \
   ExitAfterCall(this,66,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::get_RRS2FreePlaySimStepSize(float & oRRS2FreePlaySimStepSize) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,67,&_Trac2,&oRRS2FreePlaySimStepSize); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_RRS2FreePlaySimStepSize(oRRS2FreePlaySimStepSize); \
   ExitAfterCall(this,67,_Trac2,&_ret_arg,&oRRS2FreePlaySimStepSize); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::put_RRS2FreePlaySimStepSize(float iRRS2FreePlaySimStepSize) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,68,&_Trac2,&iRRS2FreePlaySimStepSize); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_RRS2FreePlaySimStepSize(iRRS2FreePlaySimStepSize); \
   ExitAfterCall(this,68,_Trac2,&_ret_arg,&iRRS2FreePlaySimStepSize); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::GetRRS2FreePlaySimStepSizeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,69,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetRRS2FreePlaySimStepSizeInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,69,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::SetRRS2FreePlaySimStepSizeLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,70,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetRRS2FreePlaySimStepSizeLock(iLocked); \
   ExitAfterCall(this,70,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::get_RRS2StopVRCWhenSimStops(CAT_VARIANT_BOOL & oRRS2StopVRCWhenSimStops) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,71,&_Trac2,&oRRS2StopVRCWhenSimStops); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_RRS2StopVRCWhenSimStops(oRRS2StopVRCWhenSimStops); \
   ExitAfterCall(this,71,_Trac2,&_ret_arg,&oRRS2StopVRCWhenSimStops); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::put_RRS2StopVRCWhenSimStops(CAT_VARIANT_BOOL iRRS2StopVRCWhenSimStops) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,72,&_Trac2,&iRRS2StopVRCWhenSimStops); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_RRS2StopVRCWhenSimStops(iRRS2StopVRCWhenSimStops); \
   ExitAfterCall(this,72,_Trac2,&_ret_arg,&iRRS2StopVRCWhenSimStops); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::GetRRS2StopVRCWhenSimStopsInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,73,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetRRS2StopVRCWhenSimStopsInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,73,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::SetRRS2StopVRCWhenSimStopsLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,74,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetRRS2StopVRCWhenSimStopsLock(iLocked); \
   ExitAfterCall(this,74,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::get_RRS2MainTaskProcessing(CAT_VARIANT_BOOL & oRRS2MainTaskProcessing) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,75,&_Trac2,&oRRS2MainTaskProcessing); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_RRS2MainTaskProcessing(oRRS2MainTaskProcessing); \
   ExitAfterCall(this,75,_Trac2,&_ret_arg,&oRRS2MainTaskProcessing); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::put_RRS2MainTaskProcessing(CAT_VARIANT_BOOL iRRS2MainTaskProcessing) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,76,&_Trac2,&iRRS2MainTaskProcessing); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_RRS2MainTaskProcessing(iRRS2MainTaskProcessing); \
   ExitAfterCall(this,76,_Trac2,&_ret_arg,&iRRS2MainTaskProcessing); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::GetRRS2MainTaskProcessingInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,77,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetRRS2MainTaskProcessingInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,77,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::SetRRS2MainTaskProcessingLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,78,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetRRS2MainTaskProcessingLock(iLocked); \
   ExitAfterCall(this,78,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::get_RRS2DeleteAutoDownloadFilesAfterXfer(CAT_VARIANT_BOOL & oRRS2DeleteAutoDownloadFilesAfterXfer) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,79,&_Trac2,&oRRS2DeleteAutoDownloadFilesAfterXfer); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_RRS2DeleteAutoDownloadFilesAfterXfer(oRRS2DeleteAutoDownloadFilesAfterXfer); \
   ExitAfterCall(this,79,_Trac2,&_ret_arg,&oRRS2DeleteAutoDownloadFilesAfterXfer); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::put_RRS2DeleteAutoDownloadFilesAfterXfer(CAT_VARIANT_BOOL iRRS2DeleteAutoDownloadFilesAfterXfer) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,80,&_Trac2,&iRRS2DeleteAutoDownloadFilesAfterXfer); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_RRS2DeleteAutoDownloadFilesAfterXfer(iRRS2DeleteAutoDownloadFilesAfterXfer); \
   ExitAfterCall(this,80,_Trac2,&_ret_arg,&iRRS2DeleteAutoDownloadFilesAfterXfer); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::GetRRS2DeleteAutoDownloadFilesAfterXferInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,81,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetRRS2DeleteAutoDownloadFilesAfterXferInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,81,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::SetRRS2DeleteAutoDownloadFilesAfterXferLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,82,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetRRS2DeleteAutoDownloadFilesAfterXferLock(iLocked); \
   ExitAfterCall(this,82,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::Commit() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,83,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Commit(); \
   ExitAfterCall(this,83,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::Rollback() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,84,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Rollback(); \
   ExitAfterCall(this,84,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::ResetToAdminValues() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,85,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ResetToAdminValues(); \
   ExitAfterCall(this,85,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,86,&_Trac2,&iAttList); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ResetToAdminValuesByName(iAttList); \
   ExitAfterCall(this,86,_Trac2,&_ret_arg,&iAttList); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIARRSSettingAtt##classe::SaveRepository() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,87,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SaveRepository(); \
   ExitAfterCall(this,87,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIARRSSettingAtt##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,88,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,88,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIARRSSettingAtt##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,89,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,89,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIARRSSettingAtt##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,90,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,90,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIARRSSettingAtt##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,91,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,91,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIARRSSettingAtt##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,92,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,92,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIARRSSettingAtt(classe) \
 \
 \
declare_TIE_DNBIARRSSettingAtt(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIARRSSettingAtt##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIARRSSettingAtt,"DNBIARRSSettingAtt",DNBIARRSSettingAtt::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIARRSSettingAtt(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIARRSSettingAtt, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIARRSSettingAtt##classe(classe::MetaObject(),DNBIARRSSettingAtt::MetaObject(),(void *)CreateTIEDNBIARRSSettingAtt##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIARRSSettingAtt(classe) \
 \
 \
declare_TIE_DNBIARRSSettingAtt(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIARRSSettingAtt##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIARRSSettingAtt,"DNBIARRSSettingAtt",DNBIARRSSettingAtt::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIARRSSettingAtt(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIARRSSettingAtt, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIARRSSettingAtt##classe(classe::MetaObject(),DNBIARRSSettingAtt::MetaObject(),(void *)CreateTIEDNBIARRSSettingAtt##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIARRSSettingAtt(classe) TIE_DNBIARRSSettingAtt(classe)
#else
#define BOA_DNBIARRSSettingAtt(classe) CATImplementBOA(DNBIARRSSettingAtt, classe)
#endif

#endif
