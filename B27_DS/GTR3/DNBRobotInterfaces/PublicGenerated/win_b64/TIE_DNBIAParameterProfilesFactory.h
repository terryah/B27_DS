#ifndef __TIE_DNBIAParameterProfilesFactory
#define __TIE_DNBIAParameterProfilesFactory

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIAParameterProfilesFactory.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIAParameterProfilesFactory */
#define declare_TIE_DNBIAParameterProfilesFactory(classe) \
 \
 \
class TIEDNBIAParameterProfilesFactory##classe : public DNBIAParameterProfilesFactory \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIAParameterProfilesFactory, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall CreateProfileInstance(const CATBSTR & iProfileType, const CATBSTR & iInstanceName, DNBIAParameterProfiles *& oAppParameterProfile); \
      virtual HRESULT __stdcall DeleteProfileInstance(DNBIAParameterProfiles * ioParameterProfile); \
      virtual HRESULT __stdcall GetProfileInstance(const CATBSTR & iInstanceName, DNBIAParameterProfiles *& oProfileInstance); \
      virtual HRESULT __stdcall GetProfileInstances(const CATBSTR & iProfileType, CATSafeArrayVariant *& oProfiles); \
      virtual HRESULT __stdcall GetAllProfileInstances(CATSafeArrayVariant *& oAllInstancesOnProduct); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIAParameterProfilesFactory(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall CreateProfileInstance(const CATBSTR & iProfileType, const CATBSTR & iInstanceName, DNBIAParameterProfiles *& oAppParameterProfile); \
virtual HRESULT __stdcall DeleteProfileInstance(DNBIAParameterProfiles * ioParameterProfile); \
virtual HRESULT __stdcall GetProfileInstance(const CATBSTR & iInstanceName, DNBIAParameterProfiles *& oProfileInstance); \
virtual HRESULT __stdcall GetProfileInstances(const CATBSTR & iProfileType, CATSafeArrayVariant *& oProfiles); \
virtual HRESULT __stdcall GetAllProfileInstances(CATSafeArrayVariant *& oAllInstancesOnProduct); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIAParameterProfilesFactory(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::CreateProfileInstance(const CATBSTR & iProfileType, const CATBSTR & iInstanceName, DNBIAParameterProfiles *& oAppParameterProfile) \
{ \
return (ENVTIECALL(DNBIAParameterProfilesFactory,ENVTIETypeLetter,ENVTIELetter)CreateProfileInstance(iProfileType,iInstanceName,oAppParameterProfile)); \
} \
HRESULT __stdcall  ENVTIEName::DeleteProfileInstance(DNBIAParameterProfiles * ioParameterProfile) \
{ \
return (ENVTIECALL(DNBIAParameterProfilesFactory,ENVTIETypeLetter,ENVTIELetter)DeleteProfileInstance(ioParameterProfile)); \
} \
HRESULT __stdcall  ENVTIEName::GetProfileInstance(const CATBSTR & iInstanceName, DNBIAParameterProfiles *& oProfileInstance) \
{ \
return (ENVTIECALL(DNBIAParameterProfilesFactory,ENVTIETypeLetter,ENVTIELetter)GetProfileInstance(iInstanceName,oProfileInstance)); \
} \
HRESULT __stdcall  ENVTIEName::GetProfileInstances(const CATBSTR & iProfileType, CATSafeArrayVariant *& oProfiles) \
{ \
return (ENVTIECALL(DNBIAParameterProfilesFactory,ENVTIETypeLetter,ENVTIELetter)GetProfileInstances(iProfileType,oProfiles)); \
} \
HRESULT __stdcall  ENVTIEName::GetAllProfileInstances(CATSafeArrayVariant *& oAllInstancesOnProduct) \
{ \
return (ENVTIECALL(DNBIAParameterProfilesFactory,ENVTIETypeLetter,ENVTIELetter)GetAllProfileInstances(oAllInstancesOnProduct)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIAParameterProfilesFactory,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIAParameterProfilesFactory,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIAParameterProfilesFactory,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIAParameterProfilesFactory,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIAParameterProfilesFactory,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIAParameterProfilesFactory(classe)    TIEDNBIAParameterProfilesFactory##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIAParameterProfilesFactory(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIAParameterProfilesFactory, classe) \
 \
 \
CATImplementTIEMethods(DNBIAParameterProfilesFactory, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIAParameterProfilesFactory, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIAParameterProfilesFactory, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIAParameterProfilesFactory, classe) \
 \
HRESULT __stdcall  TIEDNBIAParameterProfilesFactory##classe::CreateProfileInstance(const CATBSTR & iProfileType, const CATBSTR & iInstanceName, DNBIAParameterProfiles *& oAppParameterProfile) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&iProfileType,&iInstanceName,&oAppParameterProfile); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateProfileInstance(iProfileType,iInstanceName,oAppParameterProfile); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&iProfileType,&iInstanceName,&oAppParameterProfile); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAParameterProfilesFactory##classe::DeleteProfileInstance(DNBIAParameterProfiles * ioParameterProfile) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&ioParameterProfile); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->DeleteProfileInstance(ioParameterProfile); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&ioParameterProfile); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAParameterProfilesFactory##classe::GetProfileInstance(const CATBSTR & iInstanceName, DNBIAParameterProfiles *& oProfileInstance) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&iInstanceName,&oProfileInstance); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetProfileInstance(iInstanceName,oProfileInstance); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&iInstanceName,&oProfileInstance); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAParameterProfilesFactory##classe::GetProfileInstances(const CATBSTR & iProfileType, CATSafeArrayVariant *& oProfiles) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&iProfileType,&oProfiles); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetProfileInstances(iProfileType,oProfiles); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&iProfileType,&oProfiles); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAParameterProfilesFactory##classe::GetAllProfileInstances(CATSafeArrayVariant *& oAllInstancesOnProduct) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&oAllInstancesOnProduct); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAllProfileInstances(oAllInstancesOnProduct); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&oAllInstancesOnProduct); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAParameterProfilesFactory##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAParameterProfilesFactory##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAParameterProfilesFactory##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAParameterProfilesFactory##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAParameterProfilesFactory##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIAParameterProfilesFactory(classe) \
 \
 \
declare_TIE_DNBIAParameterProfilesFactory(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAParameterProfilesFactory##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAParameterProfilesFactory,"DNBIAParameterProfilesFactory",DNBIAParameterProfilesFactory::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAParameterProfilesFactory(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIAParameterProfilesFactory, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAParameterProfilesFactory##classe(classe::MetaObject(),DNBIAParameterProfilesFactory::MetaObject(),(void *)CreateTIEDNBIAParameterProfilesFactory##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIAParameterProfilesFactory(classe) \
 \
 \
declare_TIE_DNBIAParameterProfilesFactory(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAParameterProfilesFactory##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAParameterProfilesFactory,"DNBIAParameterProfilesFactory",DNBIAParameterProfilesFactory::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAParameterProfilesFactory(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIAParameterProfilesFactory, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAParameterProfilesFactory##classe(classe::MetaObject(),DNBIAParameterProfilesFactory::MetaObject(),(void *)CreateTIEDNBIAParameterProfilesFactory##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIAParameterProfilesFactory(classe) TIE_DNBIAParameterProfilesFactory(classe)
#else
#define BOA_DNBIAParameterProfilesFactory(classe) CATImplementBOA(DNBIAParameterProfilesFactory, classe)
#endif

#endif
