#ifndef __TIE_DNBIATCPTrace
#define __TIE_DNBIATCPTrace

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIATCPTrace.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIATCPTrace */
#define declare_TIE_DNBIATCPTrace(classe) \
 \
 \
class TIEDNBIATCPTrace##classe : public DNBIATCPTrace \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIATCPTrace, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall get_TraceName(CATBSTR & oName); \
      virtual HRESULT __stdcall put_TraceName(const CATBSTR & iName); \
      virtual HRESULT __stdcall get_Visibility(CAT_VARIANT_BOOL & oVisibility); \
      virtual HRESULT __stdcall put_Visibility(CAT_VARIANT_BOOL iVisibility); \
      virtual HRESULT __stdcall get_TotalPoints(CATLONG & oNbPoints); \
      virtual HRESULT __stdcall Export(const CATBSTR & iName, CATLONG & oError); \
      virtual HRESULT __stdcall GetAttachedOwner(CATIABase *& oOwner); \
      virtual HRESULT __stdcall SetAttachedOwner(CATIABase * iOwner); \
      virtual HRESULT __stdcall ResetAttachedOwner(); \
      virtual HRESULT __stdcall RefreshDisplay(); \
      virtual HRESULT __stdcall SetVisibility(DNBTCPTraceReps iRep, CAT_VARIANT_BOOL ibVisible); \
      virtual HRESULT __stdcall GetVisibility(DNBTCPTraceReps iRep, CAT_VARIANT_BOOL & obVisible); \
      virtual HRESULT __stdcall SetColor(DNBTCPTraceReps iRep, CATLONG iR, CATLONG iG, CATLONG iB, CATLONG iA); \
      virtual HRESULT __stdcall GetColor(DNBTCPTraceReps iRep, CATLONG & oR, CATLONG & oG, CATLONG & oB, CATLONG & oA); \
      virtual HRESULT __stdcall SetType(DNBTCPTraceReps iRep, CATLONG iType); \
      virtual HRESULT __stdcall GetType(DNBTCPTraceReps iRep, CATLONG & oType); \
      virtual HRESULT __stdcall SetThickness(DNBTCPTraceReps iRep, CATLONG iThickness); \
      virtual HRESULT __stdcall GetThickness(DNBTCPTraceReps iRep, CATLONG & oThickness); \
      virtual HRESULT __stdcall GetLegendsVisibility(DNBTCPTraceLegends iType, CAT_VARIANT_BOOL & obVisible); \
      virtual HRESULT __stdcall SetLegendsVisibility(DNBTCPTraceLegends iType, CAT_VARIANT_BOOL ibVisible); \
      virtual HRESULT __stdcall ResetGraphics(CAT_VARIANT_BOOL bSetting); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIATCPTrace(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall get_TraceName(CATBSTR & oName); \
virtual HRESULT __stdcall put_TraceName(const CATBSTR & iName); \
virtual HRESULT __stdcall get_Visibility(CAT_VARIANT_BOOL & oVisibility); \
virtual HRESULT __stdcall put_Visibility(CAT_VARIANT_BOOL iVisibility); \
virtual HRESULT __stdcall get_TotalPoints(CATLONG & oNbPoints); \
virtual HRESULT __stdcall Export(const CATBSTR & iName, CATLONG & oError); \
virtual HRESULT __stdcall GetAttachedOwner(CATIABase *& oOwner); \
virtual HRESULT __stdcall SetAttachedOwner(CATIABase * iOwner); \
virtual HRESULT __stdcall ResetAttachedOwner(); \
virtual HRESULT __stdcall RefreshDisplay(); \
virtual HRESULT __stdcall SetVisibility(DNBTCPTraceReps iRep, CAT_VARIANT_BOOL ibVisible); \
virtual HRESULT __stdcall GetVisibility(DNBTCPTraceReps iRep, CAT_VARIANT_BOOL & obVisible); \
virtual HRESULT __stdcall SetColor(DNBTCPTraceReps iRep, CATLONG iR, CATLONG iG, CATLONG iB, CATLONG iA); \
virtual HRESULT __stdcall GetColor(DNBTCPTraceReps iRep, CATLONG & oR, CATLONG & oG, CATLONG & oB, CATLONG & oA); \
virtual HRESULT __stdcall SetType(DNBTCPTraceReps iRep, CATLONG iType); \
virtual HRESULT __stdcall GetType(DNBTCPTraceReps iRep, CATLONG & oType); \
virtual HRESULT __stdcall SetThickness(DNBTCPTraceReps iRep, CATLONG iThickness); \
virtual HRESULT __stdcall GetThickness(DNBTCPTraceReps iRep, CATLONG & oThickness); \
virtual HRESULT __stdcall GetLegendsVisibility(DNBTCPTraceLegends iType, CAT_VARIANT_BOOL & obVisible); \
virtual HRESULT __stdcall SetLegendsVisibility(DNBTCPTraceLegends iType, CAT_VARIANT_BOOL ibVisible); \
virtual HRESULT __stdcall ResetGraphics(CAT_VARIANT_BOOL bSetting); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIATCPTrace(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::get_TraceName(CATBSTR & oName) \
{ \
return (ENVTIECALL(DNBIATCPTrace,ENVTIETypeLetter,ENVTIELetter)get_TraceName(oName)); \
} \
HRESULT __stdcall  ENVTIEName::put_TraceName(const CATBSTR & iName) \
{ \
return (ENVTIECALL(DNBIATCPTrace,ENVTIETypeLetter,ENVTIELetter)put_TraceName(iName)); \
} \
HRESULT __stdcall  ENVTIEName::get_Visibility(CAT_VARIANT_BOOL & oVisibility) \
{ \
return (ENVTIECALL(DNBIATCPTrace,ENVTIETypeLetter,ENVTIELetter)get_Visibility(oVisibility)); \
} \
HRESULT __stdcall  ENVTIEName::put_Visibility(CAT_VARIANT_BOOL iVisibility) \
{ \
return (ENVTIECALL(DNBIATCPTrace,ENVTIETypeLetter,ENVTIELetter)put_Visibility(iVisibility)); \
} \
HRESULT __stdcall  ENVTIEName::get_TotalPoints(CATLONG & oNbPoints) \
{ \
return (ENVTIECALL(DNBIATCPTrace,ENVTIETypeLetter,ENVTIELetter)get_TotalPoints(oNbPoints)); \
} \
HRESULT __stdcall  ENVTIEName::Export(const CATBSTR & iName, CATLONG & oError) \
{ \
return (ENVTIECALL(DNBIATCPTrace,ENVTIETypeLetter,ENVTIELetter)Export(iName,oError)); \
} \
HRESULT __stdcall  ENVTIEName::GetAttachedOwner(CATIABase *& oOwner) \
{ \
return (ENVTIECALL(DNBIATCPTrace,ENVTIETypeLetter,ENVTIELetter)GetAttachedOwner(oOwner)); \
} \
HRESULT __stdcall  ENVTIEName::SetAttachedOwner(CATIABase * iOwner) \
{ \
return (ENVTIECALL(DNBIATCPTrace,ENVTIETypeLetter,ENVTIELetter)SetAttachedOwner(iOwner)); \
} \
HRESULT __stdcall  ENVTIEName::ResetAttachedOwner() \
{ \
return (ENVTIECALL(DNBIATCPTrace,ENVTIETypeLetter,ENVTIELetter)ResetAttachedOwner()); \
} \
HRESULT __stdcall  ENVTIEName::RefreshDisplay() \
{ \
return (ENVTIECALL(DNBIATCPTrace,ENVTIETypeLetter,ENVTIELetter)RefreshDisplay()); \
} \
HRESULT __stdcall  ENVTIEName::SetVisibility(DNBTCPTraceReps iRep, CAT_VARIANT_BOOL ibVisible) \
{ \
return (ENVTIECALL(DNBIATCPTrace,ENVTIETypeLetter,ENVTIELetter)SetVisibility(iRep,ibVisible)); \
} \
HRESULT __stdcall  ENVTIEName::GetVisibility(DNBTCPTraceReps iRep, CAT_VARIANT_BOOL & obVisible) \
{ \
return (ENVTIECALL(DNBIATCPTrace,ENVTIETypeLetter,ENVTIELetter)GetVisibility(iRep,obVisible)); \
} \
HRESULT __stdcall  ENVTIEName::SetColor(DNBTCPTraceReps iRep, CATLONG iR, CATLONG iG, CATLONG iB, CATLONG iA) \
{ \
return (ENVTIECALL(DNBIATCPTrace,ENVTIETypeLetter,ENVTIELetter)SetColor(iRep,iR,iG,iB,iA)); \
} \
HRESULT __stdcall  ENVTIEName::GetColor(DNBTCPTraceReps iRep, CATLONG & oR, CATLONG & oG, CATLONG & oB, CATLONG & oA) \
{ \
return (ENVTIECALL(DNBIATCPTrace,ENVTIETypeLetter,ENVTIELetter)GetColor(iRep,oR,oG,oB,oA)); \
} \
HRESULT __stdcall  ENVTIEName::SetType(DNBTCPTraceReps iRep, CATLONG iType) \
{ \
return (ENVTIECALL(DNBIATCPTrace,ENVTIETypeLetter,ENVTIELetter)SetType(iRep,iType)); \
} \
HRESULT __stdcall  ENVTIEName::GetType(DNBTCPTraceReps iRep, CATLONG & oType) \
{ \
return (ENVTIECALL(DNBIATCPTrace,ENVTIETypeLetter,ENVTIELetter)GetType(iRep,oType)); \
} \
HRESULT __stdcall  ENVTIEName::SetThickness(DNBTCPTraceReps iRep, CATLONG iThickness) \
{ \
return (ENVTIECALL(DNBIATCPTrace,ENVTIETypeLetter,ENVTIELetter)SetThickness(iRep,iThickness)); \
} \
HRESULT __stdcall  ENVTIEName::GetThickness(DNBTCPTraceReps iRep, CATLONG & oThickness) \
{ \
return (ENVTIECALL(DNBIATCPTrace,ENVTIETypeLetter,ENVTIELetter)GetThickness(iRep,oThickness)); \
} \
HRESULT __stdcall  ENVTIEName::GetLegendsVisibility(DNBTCPTraceLegends iType, CAT_VARIANT_BOOL & obVisible) \
{ \
return (ENVTIECALL(DNBIATCPTrace,ENVTIETypeLetter,ENVTIELetter)GetLegendsVisibility(iType,obVisible)); \
} \
HRESULT __stdcall  ENVTIEName::SetLegendsVisibility(DNBTCPTraceLegends iType, CAT_VARIANT_BOOL ibVisible) \
{ \
return (ENVTIECALL(DNBIATCPTrace,ENVTIETypeLetter,ENVTIELetter)SetLegendsVisibility(iType,ibVisible)); \
} \
HRESULT __stdcall  ENVTIEName::ResetGraphics(CAT_VARIANT_BOOL bSetting) \
{ \
return (ENVTIECALL(DNBIATCPTrace,ENVTIETypeLetter,ENVTIELetter)ResetGraphics(bSetting)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIATCPTrace,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIATCPTrace,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIATCPTrace,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIATCPTrace,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIATCPTrace,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIATCPTrace(classe)    TIEDNBIATCPTrace##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIATCPTrace(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIATCPTrace, classe) \
 \
 \
CATImplementTIEMethods(DNBIATCPTrace, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIATCPTrace, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIATCPTrace, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIATCPTrace, classe) \
 \
HRESULT __stdcall  TIEDNBIATCPTrace##classe::get_TraceName(CATBSTR & oName) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&oName); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_TraceName(oName); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&oName); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIATCPTrace##classe::put_TraceName(const CATBSTR & iName) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&iName); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_TraceName(iName); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&iName); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIATCPTrace##classe::get_Visibility(CAT_VARIANT_BOOL & oVisibility) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&oVisibility); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Visibility(oVisibility); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&oVisibility); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIATCPTrace##classe::put_Visibility(CAT_VARIANT_BOOL iVisibility) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&iVisibility); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Visibility(iVisibility); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&iVisibility); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIATCPTrace##classe::get_TotalPoints(CATLONG & oNbPoints) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&oNbPoints); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_TotalPoints(oNbPoints); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&oNbPoints); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIATCPTrace##classe::Export(const CATBSTR & iName, CATLONG & oError) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&iName,&oError); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Export(iName,oError); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&iName,&oError); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIATCPTrace##classe::GetAttachedOwner(CATIABase *& oOwner) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&oOwner); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAttachedOwner(oOwner); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&oOwner); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIATCPTrace##classe::SetAttachedOwner(CATIABase * iOwner) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&iOwner); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetAttachedOwner(iOwner); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&iOwner); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIATCPTrace##classe::ResetAttachedOwner() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ResetAttachedOwner(); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIATCPTrace##classe::RefreshDisplay() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->RefreshDisplay(); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIATCPTrace##classe::SetVisibility(DNBTCPTraceReps iRep, CAT_VARIANT_BOOL ibVisible) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,11,&_Trac2,&iRep,&ibVisible); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetVisibility(iRep,ibVisible); \
   ExitAfterCall(this,11,_Trac2,&_ret_arg,&iRep,&ibVisible); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIATCPTrace##classe::GetVisibility(DNBTCPTraceReps iRep, CAT_VARIANT_BOOL & obVisible) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,12,&_Trac2,&iRep,&obVisible); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetVisibility(iRep,obVisible); \
   ExitAfterCall(this,12,_Trac2,&_ret_arg,&iRep,&obVisible); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIATCPTrace##classe::SetColor(DNBTCPTraceReps iRep, CATLONG iR, CATLONG iG, CATLONG iB, CATLONG iA) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,13,&_Trac2,&iRep,&iR,&iG,&iB,&iA); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetColor(iRep,iR,iG,iB,iA); \
   ExitAfterCall(this,13,_Trac2,&_ret_arg,&iRep,&iR,&iG,&iB,&iA); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIATCPTrace##classe::GetColor(DNBTCPTraceReps iRep, CATLONG & oR, CATLONG & oG, CATLONG & oB, CATLONG & oA) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,14,&_Trac2,&iRep,&oR,&oG,&oB,&oA); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetColor(iRep,oR,oG,oB,oA); \
   ExitAfterCall(this,14,_Trac2,&_ret_arg,&iRep,&oR,&oG,&oB,&oA); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIATCPTrace##classe::SetType(DNBTCPTraceReps iRep, CATLONG iType) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,15,&_Trac2,&iRep,&iType); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetType(iRep,iType); \
   ExitAfterCall(this,15,_Trac2,&_ret_arg,&iRep,&iType); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIATCPTrace##classe::GetType(DNBTCPTraceReps iRep, CATLONG & oType) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,16,&_Trac2,&iRep,&oType); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetType(iRep,oType); \
   ExitAfterCall(this,16,_Trac2,&_ret_arg,&iRep,&oType); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIATCPTrace##classe::SetThickness(DNBTCPTraceReps iRep, CATLONG iThickness) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,17,&_Trac2,&iRep,&iThickness); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetThickness(iRep,iThickness); \
   ExitAfterCall(this,17,_Trac2,&_ret_arg,&iRep,&iThickness); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIATCPTrace##classe::GetThickness(DNBTCPTraceReps iRep, CATLONG & oThickness) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,18,&_Trac2,&iRep,&oThickness); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetThickness(iRep,oThickness); \
   ExitAfterCall(this,18,_Trac2,&_ret_arg,&iRep,&oThickness); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIATCPTrace##classe::GetLegendsVisibility(DNBTCPTraceLegends iType, CAT_VARIANT_BOOL & obVisible) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,19,&_Trac2,&iType,&obVisible); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetLegendsVisibility(iType,obVisible); \
   ExitAfterCall(this,19,_Trac2,&_ret_arg,&iType,&obVisible); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIATCPTrace##classe::SetLegendsVisibility(DNBTCPTraceLegends iType, CAT_VARIANT_BOOL ibVisible) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,20,&_Trac2,&iType,&ibVisible); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetLegendsVisibility(iType,ibVisible); \
   ExitAfterCall(this,20,_Trac2,&_ret_arg,&iType,&ibVisible); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIATCPTrace##classe::ResetGraphics(CAT_VARIANT_BOOL bSetting) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,21,&_Trac2,&bSetting); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ResetGraphics(bSetting); \
   ExitAfterCall(this,21,_Trac2,&_ret_arg,&bSetting); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIATCPTrace##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,22,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,22,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIATCPTrace##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,23,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,23,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIATCPTrace##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,24,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,24,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIATCPTrace##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,25,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,25,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIATCPTrace##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,26,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,26,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIATCPTrace(classe) \
 \
 \
declare_TIE_DNBIATCPTrace(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIATCPTrace##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIATCPTrace,"DNBIATCPTrace",DNBIATCPTrace::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIATCPTrace(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIATCPTrace, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIATCPTrace##classe(classe::MetaObject(),DNBIATCPTrace::MetaObject(),(void *)CreateTIEDNBIATCPTrace##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIATCPTrace(classe) \
 \
 \
declare_TIE_DNBIATCPTrace(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIATCPTrace##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIATCPTrace,"DNBIATCPTrace",DNBIATCPTrace::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIATCPTrace(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIATCPTrace, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIATCPTrace##classe(classe::MetaObject(),DNBIATCPTrace::MetaObject(),(void *)CreateTIEDNBIATCPTrace##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIATCPTrace(classe) TIE_DNBIATCPTrace(classe)
#else
#define BOA_DNBIATCPTrace(classe) CATImplementBOA(DNBIATCPTrace, classe)
#endif

#endif
