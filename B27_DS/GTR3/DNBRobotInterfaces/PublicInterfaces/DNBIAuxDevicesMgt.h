// COPYRIGHT Dassault Systemes 2001 / DELMIA Corp.
//=============================================================================
//
// DNBIAuxDevicesMgt.h
//  Define the interface on the D5 robot allowing the management of the 
// auxiliary device on the D5 robot.
//
//=============================================================================
//
// Usage notes:
// An implementation of this interface is supplied and you must use it as is.
// you MUST not reimplement it. 
//
//=============================================================================
//  Jan.  2002                                                              awn
//=============================================================================
#ifndef DNBIAuxDevicesMgt_H
#define DNBIAuxDevicesMgt_H

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */


#include "DNBRobotItfCPP.h"
#include "CATBaseUnknown.h"
#include "CATBooleanDef.h"



#include "DNBIAuxDevicesPlug.h"
#include "DNBAuxDeviceType.h"
class CATListValCATBaseUnknown_var;

#include "DNBIAuxDevicesPlugListP.h"
#include "DNBIAuxDevicesPlugListV.h"
#include "CATBooleanDef.h"
extern ExportedByDNBRobotItfCPP IID IID_DNBIAuxDevicesMgt ;


/**
  * INTERFACE : 
  *  DNBIAuxDevicesMgt.
  *<br>
  * DESCRIPTION :
  *  This interface allows the management of the auxiliary devices. You can use
  * it on any robot. The following methods can help you to interact with
  * auxiliary devices :
  *<ul>
  *<li>DefineAuxDevices   : the only way to define an auxiliary device
  *<li>GetAuxDevices
  *<li>GetAllAuxDevices
  *<li>GetAuxDevicesByType
  *<li>GetAuxDevicesByName
  *<li>GetAuxDevicesByDOF
  *<li>LocateAuxDevicesPosition
  *<li>ReorderAuxDevicesByPosition
  *<li>ReorderAuxDevices
  *<li>RemoveAll
  *</ul>
  *<br>
  * The following methods are those which interacts with the plug used to
  * link the auxiliary devices and the robot :
  *<ul>
  *<li>GetNbAuxDevicesPlug
  *<li>GetAuxDevicesPlug
  *<li>GetAllAuxDevicesPlug
  *<li>GetAuxDevicesPlugByType
  *<li>GetAuxDevicesPlugByName
  *<li>GetAuxDevicesPlugByDOF
  *<li>LocateAuxDevicesPlugPosition
  *<li>LocateAuxDevicesPlugByName
  *<li>RemoveAuxDevices
  *<li>RemoveAuxDevicesByPosition
  *<li>RemoveAuxDevicesByType
  *<li>RemoveAuxDevicesByName
  *<li>RemoveAuxDevicesByDOF
  *<li>RemoveAll
  *<li>RemoveAuxDevicesPlugByPosition
  *</ul>
  *
  */
class CATUnicodeString;

class ExportedByDNBRobotItfCPP DNBIAuxDevicesMgt: public CATBaseUnknown
{
  CATDeclareInterface;

  public:


/**
  * METHOD: 
  *   <b>GetNbAuxDevices</b>.
  *<br>
  * DESCRIPTION: 
  *   Get the number of auxiliary device plugs. This number is the same as the
  * number of auxiliary devices.
  *
  *@param <i>oNb</i>
  *   Integer. Return the number of auxiliary devices. It is an <b>output</b>
  *parameter.
  *
  *@return HRESULT
  *   Returns S_OK if succeeds, else E_FAIL. Use SUCCEEDED and FAILED macro
  * to test result.
  *<br>
  */
    virtual HRESULT GetNbAuxDevices(int& oNb)=0;


/**
  * METHOD: 
  *   <b>GetAuxDevicesPlug</b>.
  *<br>
  * DESCRIPTION: 
  *   Get an auxiliary devices at a specified location. Index starting 
  * at 1. If the input value is higher than expected, the method will
  * fail.
  *
  *@param <i>iposition</i>
  *   Integer. Position to look for. A check on the index is done.
  *It is an <b>input</b> parameter.
  *@param <i>opAuxDevices</i>
  *   DNBIAuxDevicesPlug. Pointer on the plug. It is an 
  *<b>output</b> parameter.
  *
  *@return HRESULT
  *   Returns S_OK if succeeds, else E_FAIL. Use SUCCEEDED and FAILED macro
  * to test result.
  *<br>
  */
    virtual HRESULT GetAuxDevicesPlug(int iPosition,DNBIAuxDevicesPlug** opAuxDevices)=0;


/**
  * METHOD: 
  *   <b>GetAllAuxDevicesPlug</b>.
  *<br>
  * DESCRIPTION: 
  *   Get all auxiliary devices plug in a list of pointers.
  *
  *@param <i>opList</i>
  *   DNBIAuxDevicesPlugListP. Pointer on a list of DNBIAuxDevicesPlug pointer.
  *It is an <b>output</b> parameter.
  *
  *@return HRESULT
  *   Returns S_OK if succeeds, else E_FAIL. Use SUCCEEDED and FAILED macro
  * to test result.
  *<br>
  */
    virtual HRESULT GetAllAuxDevicesPlug(DNBIAuxDevicesPlugListP** opList)=0;


/**
  * METHOD: 
  *   <b>GetAllAuxDevicesPlug</b>.
  *<br>
  * DESCRIPTION: 
  *   Get all auxiliary devices plug in a list of smart pointers.
  *
  *@param <i>opList</i>
  *   DNBIAuxDevicesPlugListP. Pointer on a list of DNBIAuxDevicesPlug pointer.
  *It is an <b>output</b> parameter.
  *
  *@return HRESULT
  *   Returns S_OK if succeeds, else E_FAIL. Use SUCCEEDED and FAILED macro
  * to test result.
  *<br>
  */
    virtual HRESULT GetAllAuxDevicesPlug(DNBIAuxDevicesPlugListV** opList)=0;

/**
  * METHOD: 
  *   <b>GetAllAuxDevicesPlug</b>.
  *<br>
  * DESCRIPTION: 
  *   Get all auxiliary devices plug in a list of samrt pointers.
  *
  *@param <i>opList</i>
  *   DNBIAuxDevicesPlugListP. Pointer on a list of DNBIAuxDevicesPlug pointer.
  *It is an <b>output</b> parameter.
  *
  *@return HRESULT
  *   Returns S_OK if succeeds, else E_FAIL. Use SUCCEEDED and FAILED macro
  * to test result.
  *<br>
  */
    virtual HRESULT GetAllAuxDevicesPlug(DNBIAuxDevicesPlugListP& opList)=0;

/**
  * METHOD: 
  *   <b>GetAllAuxDevicesPlug</b>.
  *<br>
  * DESCRIPTION: 
  *   Get all auxiliary devices plug in a list of samrt pointers.
  *
  *@param <i>opList</i>
  *   DNBIAuxDevicesPlugListP. Pointer on a list of DNBIAuxDevicesPlug pointer.
  *It is an <b>output</b> parameter.
  *
  *@return HRESULT
  *   Returns S_OK if succeeds, else E_FAIL. Use SUCCEEDED and FAILED macro
  * to test result.
  *<br>
  */
    virtual HRESULT GetAllAuxDevicesPlug(DNBIAuxDevicesPlugListV& opList)=0;

/**@nodoc deprecated
  * METHOD: 
  *   <b>GetAuxDevicesPlugByType</b>.
  *<br>
  * DESCRIPTION: 
  *   Get all auxiliary devices plug in a list of pointers based on the defined
  * type for the auxiliary device.
  *
  *@param <i>iType</i>
  *   CATUnicodeString. Type to look for. It can be "EndOfArmTooling" or "Rail".
  *It is an <b>input</b> parameter.
  *@param <i>opList</i>
  *   DNBIAuxDevicesPlugListP. Pointer on a list of DNBIAuxDevicesPlug pointer.
  *It is an <b>output</b> parameter.
  *
  *@return HRESULT
  *   Returns S_OK if succeeds, else E_FAIL. Use SUCCEEDED and FAILED macro
  * to test result.
  *<br>
  */
    virtual HRESULT GetAuxDevicesPlugByType( 
                            CATUnicodeString iType,
                            DNBIAuxDevicesPlugListP** opList)=0;
  
/**@nodoc : deprecated
  * METHOD: 
  *   <b>GetAuxDevicesPlugByType</b>.
  *<br>
  * DESCRIPTION: 
  *   Get all auxiliary devices plug in a list of smart pointers based on the
  *defined type for the auxiliary device.
  *
  *@param <i>iType</i>
  *   CATUnicodeString. Type to look for.
  *It is an <b>input</b> parameter.
  *@param <i>opList</i>
  *   DNBIAuxDevicesPlugListV. Pointer on a list of DNBIAuxDevicesPlug smart 
  *pointers. It is an <b>output</b> parameter.
  *
  *@return HRESULT
  *   Returns S_OK if succeeds, else E_FAIL. Use SUCCEEDED and FAILED macro
  * to test result.
  *<br>
  */
    virtual HRESULT GetAuxDevicesPlugByType( 
                            DNBAuxDeviceType iType,
                            DNBIAuxDevicesPlugListV** opList)=0;

/**
  * METHOD: 
  *   <b>GetAuxDevicesPlugByType</b>.
  *<br>
  * DESCRIPTION: 
  *   Get all auxiliary devices plug in a list of pointers based on the defined
  * type for the auxiliary device.
  *
  *@param <i>iType</i>
  *   AuxDeviceType. Type to look for. It can be EndOfArmTooling or Rail.
  *It is an <b>input</b> parameter.
  *@param <i>opList</i>
  *   DNBIAuxDevicesPlugListP. Pointer on a list of DNBIAuxDevicesPlug pointer.
  *It is an <b>output</b> parameter.
  *
  *@return HRESULT
  *   Returns S_OK if succeeds, else E_FAIL. Use SUCCEEDED and FAILED macro
  * to test result.
  *<br>
  */
    virtual HRESULT GetAuxDevicesPlugByType( 
                            DNBAuxDeviceType iType,
                            DNBIAuxDevicesPlugListP& opList)=0;
  
/**
  * METHOD: 
  *   <b>GetAuxDevicesPlugByType</b>.
  *<br>
  * DESCRIPTION: 
  *   Get all auxiliary devices plug in a list of smart pointers based on the
  *defined type for the auxiliary device.
  *
  *@param <i>iType</i>
  *   AuxDeviceType. Type to look for in an enumeration.
  *It is an <b>input</b> parameter.
  *@param <i>opList</i>
  *   DNBIAuxDevicesPlugListV. Pointer on a list of DNBIAuxDevicesPlug smart 
  *pointers. It is an <b>output</b> parameter.
  *
  *@return HRESULT
  *   Returns S_OK if succeeds, else E_FAIL. Use SUCCEEDED and FAILED macro
  * to test result.
  *<br>
  */
    virtual HRESULT GetAuxDevicesPlugByType( 
                            DNBAuxDeviceType iType,
                            DNBIAuxDevicesPlugListV& opList)=0;
/**
  * METHOD: 
  *   <b>GetAuxDevicesPlugByName</b>.
  *<br>
  * DESCRIPTION: 
  *   Get the auxiliary devices plug related to the name of its device.
  *
  *@param <i>iDeviceName</i>
  *   CATUnicodeString. Name of the device as auxiliary device to look for.
  *It is an <b>input</b> parameter.
  *@param <i>opiAuxDevices</i>
  *   DNBIAuxDevicesPlug. DNBIAuxDevicesPlug pointer.
  *It is an <b>output</b> parameter.
  *
  *@return HRESULT
  *   Returns S_OK if succeeds, else E_FAIL. Use SUCCEEDED and FAILED macro
  * to test result.
  *<br>
  */
    virtual HRESULT GetAuxDevicesPlugByName( CATUnicodeString iDeviceName,
                                      DNBIAuxDevicesPlug** opiAuxDevices)=0;
  
/**
  * METHOD: 
  *   <b>GetAuxDevicesPlugByDOF</b>.
  *<br>
  * DESCRIPTION: 
  *   Get all auxiliary devices plug in a list of pointers based on the defined
  * DOF for the auxiliary device.
  *
  *@param <i>iDOF</i>
  *   Integer. DOF to look for.
  *It is an <b>input</b> parameter.
  *@param <i>opList</i>
  *   DNBIAuxDevicesPlugListP. Pointer on a list of DNBIAuxDevicesPlug pointer.
  *It is an <b>output</b> parameter.
  *
  *@return HRESULT
  *   Returns S_OK if succeeds, else E_FAIL. Use SUCCEEDED and FAILED macro
  * to test result.
  *<br>
  */
    virtual HRESULT GetAuxDevicesPlugByDOF(  int iDOF,
                                      DNBIAuxDevicesPlugListP** opList)=0;


/**
  * METHOD: 
  *   <b>GetAuxDevicesPlugByDOF</b>.
  *<br>
  * DESCRIPTION: 
  *   Get all auxiliary devices plug in a list of pointers based on the defined
  * DOF for the auxiliary device.
  *
  *@param <i>iDOF</i>
  *   Integer. DOF to look for.
  *It is an <b>input</b> parameter.
  *@param <i>opList</i>
  *   DNBIAuxDevicesPlugListP. Pointer on a list of DNBIAuxDevicesPlug pointer.
  *It is an <b>output</b> parameter.
  *
  *@return HRESULT
  *   Returns S_OK if succeeds, else E_FAIL. Use SUCCEEDED and FAILED macro
  * to test result.
  *<br>
  */
    virtual HRESULT GetAuxDevicesPlugByDOF(  int iDOF,
                                      DNBIAuxDevicesPlugListP& opList)=0;

    ///////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////
    //                          LOCATE                                   //
    ///////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////

/**
  * METHOD: 
  *   <b>LocateAuxDevicesPlugPosition</b>.
  *<br>
  * DESCRIPTION: 
  *   Locate a specific auxiliary devices plug by returning its position. 
  *
  *@param <i>ipAuxDevices</i>
  *   DNBIAuxDevicesPlug. Pointer on the auxiliary devices plug. It is an
  * <b>input</b> parameter.
  *@param <i>oPosition</i>
  *   Integer. Position of the auxiliary device plug. It is an <b>output</b>
  *parameter.
  *
  *@return HRESULT
  *   Returns S_OK if succeeds, else E_FAIL. Use SUCCEEDED and FAILED macro
  * to test result.
  *<br>
  */
    virtual HRESULT LocateAuxDevicesPlugPosition(DNBIAuxDevicesPlug* ipAuxDevices,
                                                 int &oPosition)=0;

/**
  * METHOD: 
  *   <b>LocateAuxDevicesPlugByName</b>.
  *<br>
  * DESCRIPTION: 
  *   Locate a specific auxiliary devices plug by returning its position
  * according to its name. 
  *
  *@param <i>iDeviceName</i>
  *   CATUnicodeString. Name of the auxiliary device plug to locate.
  * It is an <b>input</b> parameter.
  *@param <i>oPosition</i>
  *   Integer. Position of the auxiliary devices plug. It is an <b>output</b>
  *parameter.
  *
  *@return HRESULT
  *   Returns S_OK if succeeds, else E_FAIL. Use SUCCEEDED and FAILED macro
  * to test result.
  *<br>
  */
    virtual HRESULT LocateAuxDevicesPlugByName( CATUnicodeString iDeviceName,
                                             int &oPosition)=0;


    ///////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////
    //                          REORDER                                  //
    ///////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////
    
/**
  * METHOD: 
  *   <b>ReorderAuxDevicesByPosition</b>.
  *<br>
  * DESCRIPTION: 
  *   Change the position of a auxiliary device from its original position
  * in the list to the new one.
  *
  *
  *@param <i>iOriginalPosition</i>
  *   Integer. Original position of the auxiliary device.
  * It is an <b>input</b> parameter.
  *@param <i>iFinalPosition</i>
  *   Integer. Final position of the auxiliary device.
  * It is an <b>input</b> parameter.
  *
  *@return HRESULT
  *   Returns S_OK if succeeds, else E_FAIL. Use SUCCEEDED and FAILED macro
  * to test result.
  *<br>
  */
    virtual HRESULT ReorderAuxDevicesByPosition( int iOriginalPosition,
                                              int iFinalPosition)=0;


/**
  * METHOD: 
  *   <b>ReorderAuxDevices</b>.
  *<br>
  * DESCRIPTION: 
  *   Change the position of a auxiliary device from its original position
  * in the list to the new one.
  *
  *
  *@param <i>ipiAuxDevices</i>
  *   DNBIAuxDevicesPlug. Pointer on the auxiliary device to reorder.
  * It is an <b>input</b> parameter.
  *@param <i>iFinalPosition</i>
  *   Integer. Final position of the auxiliary device.
  * It is an <b>input</b> parameter.
  *
  *@return HRESULT
  *   Returns S_OK if succeeds, else E_FAIL. Use SUCCEEDED and FAILED macro
  * to test result.
  *<br>
  */
    virtual HRESULT ReorderAuxDevices( DNBIAuxDevicesPlug* ipiAuxDevices,
                                    int iFinalPosition)=0;

    ///////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////
    //                          SERVICES                                 //
    ///////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////
/**
  * METHOD: 
  *   <b>DefineAuxDevices</b>.
  *<br>
  * DESCRIPTION: 
  *   Define a auxiliary device.
  *
  *
  *@param <i>ipBUDevice</i>
  *   CATBaseUnknown. Pointer on device to define as auxiliary device.
  * It is an <b>input</b> parameter.
  *@param <i>iType</i>
  *   CATUnicodeString. Type of the auxiliary device.
  * It is an <b>input</b> parameter.
  *@param <i>opAxis</i>
  *   DNBIAuxDevicesPlug. Pointer on new the auxiliary device plug.
  * It is an <b>output</b> parameter.
  *@param <i>iInsertPosition</i>
  *   Integer. Index to insert the auxiliary device. Equal to 0 by default, 
  * device is inserted at the end. It is an <b>input</b> parameter.
  *@param <i>bUpdate</i>
  *   Boolean. Indicate if the method DNBIAuxDevicesPlug::UpdateAuxDevices is called
  * or not. By default, it is not. It is an <b>input</b> parameter.
  *
  *@return HRESULT
  *   Returns S_OK if succeeds, else E_FAIL. Use SUCCEEDED and FAILED macro
  * to test result.
  *<br>
  */    
    virtual HRESULT DefineAuxDevices(  CATBaseUnknown* ipBUDevice,
                                    CATUnicodeString iType,
                                    DNBIAuxDevicesPlug** opAxis,
                                    int iInsertPosition = 0,
                                    boolean bUpdate=FALSE)=0;

/**
  * METHOD: 
  *   <b>DefineAuxDevices</b>.
  *<br>
  * DESCRIPTION: 
  *   Define a auxiliary device.
  *
  *
  *@param <i>ipBUDevice</i>
  *   CATBaseUnknown. Pointer on device to define as auxiliary device.
  * It is an <b>input</b> parameter.
  *@param <i>iType</i>
  *   AuxDeviceType. Type of the auxiliary device.
  * It is an <b>input</b> parameter.
  *@param <i>opAxis</i>
  *   DNBIAuxDevicesPlug. Pointer on new the auxiliary device plug.
  * It is an <b>output</b> parameter.
  *@param <i>iInsertPosition</i>
  *   Integer. Index to insert the auxiliary device. Equal to 0 by default, 
  * device is inserted at the end. It is an <b>input</b> parameter.
  *@param <i>bUpdate</i>
  *   Boolean. Indicate if the method DNBIAuxDevicesPlug::UpdateAuxDevices is called
  * or not. By default, it is not. It is an <b>input</b> parameter.
  *
  *@return HRESULT
  *   Returns S_OK if succeeds, else E_FAIL. Use SUCCEEDED and FAILED macro
  * to test result.
  *<br>
  */    
    virtual HRESULT DefineAuxDevices(  CATBaseUnknown* ipBUDevice,
                                    DNBAuxDeviceType iType,
                                    DNBIAuxDevicesPlug** opAxis,
                                    int iInsertPosition = 0,
                                    boolean bUpdate=FALSE)=0;                                    
/**
  * METHOD: 
  *   <b>RemoveAuxDevices</b>.
  *<br>
  * DESCRIPTION: 
  *   Remove a specific auxiliary devices plug and its associated devices.
  *
  *@param <i>ipAuxDevices</i>
  *   DNBIAuxDevicesPlug. Pointer on the auxiliary devices to remove.
  *It is an <b>input</b> parameter.
  *
  *@return HRESULT
  *   Returns S_OK if succeeds, else E_FAIL. Use SUCCEEDED and FAILED macro
  * to test result.
  *<br>
  */
    virtual HRESULT RemoveAuxDevices(DNBIAuxDevicesPlug* ipAuxDevices)=0;

/**
  * METHOD: 
  *   <b>RemoveAuxDevicesByPosition</b>.
  *<br>
  * DESCRIPTION: 
  *   Remove an auxiliary device based on its position. A check of the index
  * is done.
  *
  *@param <i>iPosition</i>
  *   Integer. The index of the auxiliary device to remove.
  *It is an <b>input</b> parameter.
  *
  *@return HRESULT
  *   Returns S_OK if succeeds, else E_FAIL. Use SUCCEEDED and FAILED macro
  * to test result.
  *<br>
  */
    virtual HRESULT RemoveAuxDevicesByPosition(int iPosition)=0;

/**@nodoc : deprecated
  * METHOD: 
  *   <b>RemoveAuxDevicesByType</b>.
  *<br>
  * DESCRIPTION: 
  *   Remove an auxiliary device plug and its device based on its type.
  *
  *@param <i>iType</i>
  *   CATUnicodeString. The type of the auxiliary device to remove.
  *It is an <b>input</b> parameter. I can be either "EndOfArmTooling" or 
  *"Rail".
  *
  *@return HRESULT
  *   Returns S_OK if succeeds, else E_FAIL. Use SUCCEEDED and FAILED macro
  * to test result.
  *<br>
  */
    virtual HRESULT RemoveAuxDevicesByType( CATUnicodeString iType)=0;


/**
  * METHOD: 
  *   <b>RemoveAuxDevicesByType</b>.
  *<br>
  * DESCRIPTION: 
  *   Remove an auxiliary device plug and its device based on its type.
  *
  *@param <i>iType</i>
  *   AuxDeviceType. Enumeration of available type.
  *It is an <b>input</b> parameter.
  *
  *@return HRESULT
  *   Returns S_OK if succeeds, else E_FAIL. Use SUCCEEDED and FAILED macro
  * to test result.
  *<br>
  */
    virtual HRESULT RemoveAuxDevicesByType( DNBAuxDeviceType iType)=0;

    
/**
  * METHOD: 
  *   <b>RemoveAuxDevicesByName</b>.
  *<br>
  * DESCRIPTION: 
  *   Remove an auxiliary device plug based on its auxiliary device name.
  *
  *@param <i>iDeviceName</i>
  *   CATUnicodeString. The name of the auxiliary device to remove.
  *It is an <b>input</b> parameter.
  *
  *@return HRESULT
  *   Returns S_OK if succeeds, else E_FAIL. Use SUCCEEDED and FAILED macro
  * to test result.
  *<br>
  */
    virtual HRESULT RemoveAuxDevicesByName( CATUnicodeString iDeviceName)=0;

/**
  * METHOD: 
  *   <b>RemoveAuxDevicesByDOF</b>.
  *<br>
  * DESCRIPTION: 
  *   Remove an auxiliary devices based on the device DOF.
  *
  *@param <i>iDOF</i>
  *   Integer. The DOF value.
  *It is an <b>input</b> parameter.
  *
  *@return HRESULT
  *   Returns S_OK if succeeds, else E_FAIL. Use SUCCEEDED and FAILED macro
  * to test result.
  *<br>
  */
    virtual HRESULT RemoveAuxDevicesByDOF( int iDOF )=0;


/**
  * METHOD: 
  *   <b>RemoveAll</b>.
  *<br>
  * DESCRIPTION: 
  *   Remove all auxiliary devices.
  *
  *@return HRESULT
  *   Returns S_OK if succeeds, else E_FAIL. Use SUCCEEDED and FAILED macro
  * to test result.
  *<br>
  */
    virtual HRESULT RemoveAll()=0;

  ///////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////
  //                     DIRECT ACCESS TO DEVICES                      //
  ///////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////

/**
  * METHOD: 
  *   <b>GetAuxDevices</b>.
  *<br>
  * DESCRIPTION: 
  *   Get the auxiliary device at a specified location. Index starting 
  * at 1. If the input value is higher than expected, the method will
  * fail.
  *
  *@param <i>iposition</i>
  *   Integer. Position to look for. A check on the index is done.
  *It is an <b>input</b> parameter.
  *@param <i>opDevice</i>
  *   CATBaseUnknown. Pointer on the found device. It is an 
  *<b>output</b> parameter.
  *
  *@return HRESULT
  *   Returns S_OK if succeeds, else E_FAIL. Use SUCCEEDED and FAILED macro
  * to test result.
  *<br>
  */  
    virtual HRESULT GetAuxDevices(int iPosition, CATBaseUnknown** opAuxDevices)=0;


/**
  * METHOD: 
  *   <b>GetAllAuxDevices</b>.
  *<br>
  * DESCRIPTION: 
  *   Get all auxiliary devices in a list of smart pointers.
  *
  *@param <i>opList</i>
  *   CATListValCATBaseUnknown_var. Pointer on a list of smart pointer on
  * CATBaseUnknown. It is an <b>output</b> parameter.
  *
  *@return HRESULT
  *   Returns S_OK if succeeds, else E_FAIL. Use SUCCEEDED and FAILED macro
  * to test result.
  *<br>
  */
  virtual HRESULT GetAllAuxDevices(CATListValCATBaseUnknown_var** opList)=0;

/**
  * METHOD: 
  *   <b>GetAllAuxDevices</b>.
  *<br>
  * DESCRIPTION: 
  *   Get all auxiliary devices in a list of smart pointers.
  *
  *@param <i>opList</i>
  *   CATListValCATBaseUnknown_var. Pointer on a list of smart pointer on
  * CATBaseUnknown. It is an <b>output</b> parameter.
  *
  *@return HRESULT
  *   Returns S_OK if succeeds, else E_FAIL. Use SUCCEEDED and FAILED macro
  * to test result.
  *<br>
  */
  virtual HRESULT GetAllAuxDevices(CATListValCATBaseUnknown_var& opList)=0;


/**
  * METHOD: 
  *   <b>GetAuxDevicesByType</b>.
  *<br>
  * DESCRIPTION: 
  *   Get all auxiliary devices in a list of pointers based on the defined type
  * on the auxiliary device.
  *
  *@param <i>iType</i>
  *   CATUnicodeString. Type to look for. It can be either 
  *AuxType_EndOfArmTooling or AuxType_RailTrackGantry. It is an <b>input</b> 
  *parameter.
  *@param <i>opList</i>
  *   CATListValCATBaseUnknown_var. Pointer on a list of smart pointer on
  * CATBaseUnknown. It is an <b>output</b> parameter.
  *
  *@return HRESULT
  *   Returns S_OK if succeeds, else E_FAIL. Use SUCCEEDED and FAILED macro
  * to test result.
  *<br>
  */
  virtual HRESULT GetAuxDevicesByType( CATUnicodeString iType,
                            CATListValCATBaseUnknown_var** opList)=0;

  
/**
  * METHOD: 
  *   <b>GetAuxDevicesByType</b>.
  *<br>
  * DESCRIPTION: 
  *   Get all auxiliary devices in a list of pointers based on the defined type
  * on the auxiliary device.
  *
  *@param <i>iType</i>
  *   AuxDeviceType. Type to look for. It can be either 
  *AuxType_EndOfArmTooling or AuxType_RailTrackGantry. It is an <b>input</b> 
  *parameter.
  *@param <i>opList</i>
  *   CATListValCATBaseUnknown_var. Pointer on a list of smart pointer on
  * CATBaseUnknown. It is an <b>output</b> parameter.
  *
  *@return HRESULT
  *   Returns S_OK if succeeds, else E_FAIL. Use SUCCEEDED and FAILED macro
  * to test result.
  *<br>
  */
  virtual HRESULT GetAuxDevicesByType( DNBAuxDeviceType iType,
                            CATListValCATBaseUnknown_var& opList)=0;
  

/**
  * METHOD: 
  *   <b>GetAuxDevicesByName</b>.
  *<br>
  * DESCRIPTION: 
  *   Get the auxiliary devices related to its name.
  *
  *@param <i>iDeviceName</i>
  *   CATUnicodeString. Name of the device as auxiliary device to look for.
  *It is an <b>input</b> parameter.
  *@param <i>opAuxDevices</i>
  *   CATBaseUnknown. CATBaseUnknown pointer.
  *It is an <b>output</b> parameter.
  *
  *@return HRESULT
  *   Returns S_OK if succeeds, else E_FAIL. Use SUCCEEDED and FAILED macro
  * to test result.
  *<br>
  */
  virtual HRESULT GetAuxDevicesByName( CATUnicodeString iDeviceName,
                            CATBaseUnknown** opAuxDevices)=0;


/**
  * METHOD: 
  *   <b>GetAuxDevicesByDOF</b>.
  *<br>
  * DESCRIPTION: 
  *   Get all auxiliary devices in a list of pointers based on the defined DOF.
  *
  *@param <i>iDOF</i>
  *   Integer. DOF to look for.
  *It is an <b>input</b> parameter.
  *@param <i>opList</i>
  *   CATListValCATBaseUnknown_var. Pointer on a list of CATBaseUnknown_var
  * pointer. It is an <b>output</b> parameter.
  *
  *@return HRESULT
  *   Returns S_OK if succeeds, else E_FAIL. Use SUCCEEDED and FAILED macro
  * to test result.
  *<br>
  */
  virtual HRESULT GetAuxDevicesByDOF(  int iDOF,
                            CATListValCATBaseUnknown_var** opList)=0;

/**
  * METHOD: 
  *   <b>GetAuxDevicesByDOF</b>.
  *<br>
  * DESCRIPTION: 
  *   Get all auxiliary devices in a list of pointers based on the defined DOF.
  *
  *@param <i>iDOF</i>
  *   Integer. DOF to look for.
  *It is an <b>input</b> parameter.
  *@param <i>opList</i>
  *   CATListValCATBaseUnknown_var. Pointer on a list of CATBaseUnknown_var
  * pointer. It is an <b>output</b> parameter.
  *
  *@return HRESULT
  *   Returns S_OK if succeeds, else E_FAIL. Use SUCCEEDED and FAILED macro
  * to test result.
  *<br>
  */
  virtual HRESULT GetAuxDevicesByDOF(  int iDOF,
                            CATListValCATBaseUnknown_var& opList)=0;

/**
  * METHOD: 
  *   <b>LocateAuxDevicesPosition</b>.
  *<br>
  * DESCRIPTION: 
  *   Locate the position of an auxiliary device.
  *
  *@param <i>ipBU</i>
  *   CATBaseUnknown. CATBaseUnknown pointer on the auxiliary device.
  *It is an <b>input</b> parameter.
  *@param <i>oPosition</i>
  *   Integer. Contains the found position. It is an <b>output</b> parameter.
  * Return 0 if not found.
  *
  *@return HRESULT
  *   Returns S_OK if succeeds, else E_FAIL. Use SUCCEEDED and FAILED macro
  * to test result.
  *<br>
  */
  virtual HRESULT LocateAuxDevicesPosition( CATBaseUnknown* ipBU,int &oPosition)=0;



  // No constructors or destructors on this pure virtual base class
  // --------------------------------------------------------------
};

//------------------------------------------------------------------
CATDeclareHandler(DNBIAuxDevicesMgt, CATBaseUnknown);
#endif

