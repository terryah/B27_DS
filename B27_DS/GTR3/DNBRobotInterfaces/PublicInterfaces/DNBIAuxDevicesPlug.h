// COPYRIGHT Dassault Systemes 2001 / DELMIA Corp.
//=============================================================================
//
// DNBIAuxDevicesPlug.h
//  Define the interface on the DNBAuxDevicesPlug
//
//=============================================================================
//
// Usage notes:
// An implementation of this interface is supplied and you must use it as is.
// you MUST not reimplement it. 
//
//=============================================================================
//  Jan.  2002                                                              awn
//=============================================================================
#ifndef DNBIAuxDevicesPlug_H
#define DNBIAuxDevicesPlug_H

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

#include "DNBRobotItfCPP.h"
#include "CATBaseUnknown.h"
#include "DNBAuxDeviceType.h"


extern ExportedByDNBRobotItfCPP IID IID_DNBIAuxDevicesPlug ;




class CATUnicodeString;

/**
  * INTERFACE : 
  *  DNBIAuxDevicesPlug.
  *<br>
  * DESCRIPTION :
  *  This interface allows the management of the DNBAuxDevicesPlug.
  *<Describe the methods available>
  *
  *
  */


class ExportedByDNBRobotItfCPP DNBIAuxDevicesPlug: public CATBaseUnknown
{
  CATDeclareInterface;

  public:


/**
  * METHOD: 
  *   <b>GetAuxDeviceName</b>.
  *<br>
  * DESCRIPTION: 
  *   Get the name of the device attached as auxiliary device.
  *
  *@param <i>oName</i>
  *   CATUnicodeString. Name of the device attached as auxiliary device. It is
  * an <b>output</b> parameter.
  *
  *@return HRESULT
  *   Returns S_OK if succeeds, else E_FAIL. Use SUCCEEDED and FAILED macro
  * to test result.
  *<br>
  */
    virtual HRESULT GetAuxDeviceName(CATUnicodeString &oName) = 0;

/**
  * METHOD: 
  *   <b>SetAuxDeviceName</b>.
  *<br>
  * DESCRIPTION: 
  *   Set the name of the device attached as auxiliary device.
  *
  *@param <i>iName</i>
  *   CATUnicodeString. Name of the device attached as auxiliary device. It is 
  * an <b>input</b> parameter.
  *
  *@return HRESULT
  *   Returns S_OK if succeeds, else E_FAIL. Use SUCCEEDED and FAILED macro
  * to test result.
  *<br>
  */
    virtual HRESULT SetAuxDeviceName(CATUnicodeString iName) = 0;

/**
  * METHOD: 
  *   <b>GetNbDOF</b>.
  *<br>
  * DESCRIPTION: 
  *   Get the DOF of the device attached as auxiliary device.Used for backup
  * only.
  *
  *@param <i>oDOF</i>
  *   Integer. Number of DOF the device attached as auxiliary device. It is 
  * an <b>output</b> parameter.
  *
  *@return HRESULT
  *   Returns S_OK if succeeds, else E_FAIL. Use SUCCEEDED and FAILED macro
  * to test result.
  *<br>
  */
    virtual HRESULT GetNbDOF(int &oDOF) = 0;

/**
  * METHOD: 
  *   <b>SetNbDOF</b>.
  *<br>
  * DESCRIPTION: 
  *   Get the DOF of the device attached as auxiliary axis. Used for backup
  * only.
  *
  *@param <i>iDOF</i>
  *   Integer. Number of DOF the device attached as auxiliary device. It is 
  * an <b>input</b> parameter.
  *
  *@return HRESULT
  *   Returns S_OK if succeeds, else E_FAIL. Use SUCCEEDED and FAILED macro
  * to test result.
  *<br>
  */
    virtual HRESULT SetNbDOF(int iDOF) = 0;

/**@nodoc
  * METHOD: 
  *   <b>GetAuxDeviceType</b>.
  *<br>
  * DESCRIPTION: 
  *   Get the type of the device attached as auxiliary device. Internal use
  *only
  *
  *@param <i>oType</i>
  *   CATUnicodeString. Type of the device attached as auxiliary device. It is
  * an <b>output</b> parameter.
  *
  *@return HRESULT
  *   Returns S_OK if succeeds, else E_FAIL. Use SUCCEEDED and FAILED macro
  * to test result.
  *<br>
  */
    virtual HRESULT GetAuxDeviceType(CATUnicodeString &oType) = 0;

/**@nodoc
  * METHOD: 
  *   <b>SetAuxDeviceType</b>.
  *<br>
  * DESCRIPTION: 
  *   Set the type of the device attached as auxiliary device. Internal use
  *only
  *
  *@param <i>iType</i>
  *   CATUnicodeString. Type of the device attached as auxiliary device. It is 
  * an <b>input</b> parameter.
  *
  *@return HRESULT
  *   Returns S_OK if succeeds, else E_FAIL. Use SUCCEEDED and FAILED macro
  * to test result.
  *<br>
  */
    virtual HRESULT SetAuxDeviceType(CATUnicodeString iType) = 0;

/**
  * METHOD: 
  *   <b>GetAuxDeviceType</b>.
  *<br>
  * DESCRIPTION: 
  *   Get the type of the device attached as auxiliary device.
  *
  *@param <i>oType</i>
  *   AuxDeviceType. Enumeration of possible values. It is
  * an <b>output</b> parameter.
  *
  *@return HRESULT
  *   Returns S_OK if succeeds, else E_FAIL. Use SUCCEEDED and FAILED macro
  * to test result.
  *<br>
  */
    virtual HRESULT GetAuxDeviceType(DNBAuxDeviceType &oType) = 0;

/**
  * METHOD: 
  *   <b>SetAuxDeviceType</b>.
  *<br>
  * DESCRIPTION: 
  *   Set the type of the device attached as auxiliary device.
  *
  *@param <i>oType</i>
  *   AuxDeviceType. Enumeration of possible values. It is
  * an <b>output</b> parameter.
  *
  *@return HRESULT
  *   Returns S_OK if succeeds, else E_FAIL. Use SUCCEEDED and FAILED macro
  * to test result.
  *<br>
  */
    virtual HRESULT SetAuxDeviceType(DNBAuxDeviceType iType) = 0;

/**
  * METHOD: 
  *   <b>GetAuxDeviceNLSType</b>.
  *<br>
  * DESCRIPTION: 
  *   Get the type of the device attached as auxiliary device based on its NLS
  *ID.
  *
  *@param <i>oType</i>
  *   CATUnicodeString. Type of the device attached as auxiliary device. It is
  * an <b>output</b> parameter.
  *
  *@return HRESULT
  *   Returns S_OK if succeeds, else E_FAIL. Use SUCCEEDED and FAILED macro
  * to test result.
  *<br>
  */
    virtual HRESULT GetAuxDeviceNLSType(CATUnicodeString &oType) = 0;



  /**
  * METHOD: 
  *   <b>GetAuxDevice</b>.
  *<br>
  * DESCRIPTION: 
  *   Get the device defined as auxiliary device.
  *
  *@param <i>opDevice</i>
  *   CATBaseUnknown. Device defined as auxiliary device. It is 
  * an <b>output</b> parameter.
  *
  *@return HRESULT
  *   Returns S_OK if succeeds, else E_FAIL. Use SUCCEEDED and FAILED macro
  * to test result.
  *<br>
  */
    virtual HRESULT GetAuxDevice(CATBaseUnknown** opDevice) = 0;

/**
  * METHOD: 
  *   <b>SetAuxDevice</b>.
  *<br>
  * DESCRIPTION: 
  *   Set the device defined as auxiliary device.
  *
  *@param <i>ipDevice</i>
  *   CATBaseUnknown. Device defined as auxiliary device. It is 
  * an <b>input</b> parameter.
  *
  *@return HRESULT
  *   Returns S_OK if succeeds, else E_FAIL. Use SUCCEEDED and FAILED macro
  * to test result.
  *<br>
  */
    virtual HRESULT SetAuxDevice(CATBaseUnknown* ipDevice) = 0;


    ///////////////////////////////////////////////////////////////
    // SERVICES
    ///////////////////////////////////////////////////////////////

/**
  * METHOD: 
  *   <b>UpdateAuxDevicesPlug</b>.
  *<br>
  * DESCRIPTION: 
  *   Update the attributes of the DNBAuxDevicesPlug based on the device
  * defined as auxiliary device. The type is not changed.
  *
  *@param <i>ipDevice</i>
  *   CATBaseUnknown. Device defined as auxiliary device. It is 
  * an <b>input</b> parameter. NULL by default, it will consider the device
  * defined previously
  *
  *@return HRESULT
  *   Returns S_OK if succeeds, else E_FAIL. Use SUCCEEDED and FAILED macro
  * to test result.
  *<br>
  */
    virtual HRESULT UpdateAuxDevicesPlug(CATBaseUnknown* ipDevice=NULL) = 0;


/**
  * METHOD: 
  *   <b>GetOwnerRobot</b>.
  *<br>
  * DESCRIPTION: 
  *   Retrun the owner robot of the auxiliary device. It is not the device
  * defined as auxiliary device.
  *
  *@param <i>opDevice</i>
  *   CATBaseUnknown. Robot owning the auxiliary device. It is 
  * an <b>output</b> parameter. 
  *
  *@return HRESULT
  *   Returns S_OK if succeeds, else E_FAIL. Use SUCCEEDED and FAILED macro
  * to test result.
  *<br>
  */
    virtual HRESULT GetOwnerRobot(CATBaseUnknown** opDevice) = 0;

  // No constructors or destructors on this pure virtual base class
  // --------------------------------------------------------------
};

//------------------------------------------------------------------
CATDeclareHandler(DNBIAuxDevicesPlug, CATBaseUnknown);
#endif

