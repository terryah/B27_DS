// COPYRIGHT Dassault Systemes 2006
#ifndef _DNBIBASICROBOT_H_
#define _DNBIBASICROBOT_H_

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */
#include <CATBaseUnknown.h>

#include "DNBRobotItfCPP.h"

class CATMathTransformation;
class CATListValCATBaseUnknown_var;
#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByDNBRobotItfCPP IID IID_DNBIBasicRobot;
#else
extern "C" const IID IID_DNBIBasicRobot;
#endif

/**
 * Interface to access robot attributes.
 * <b>Role:</b>
 * This interface provides methods to get/set data related to the
 * robot.
 */

class ExportedByDNBRobotItfCPP DNBIBasicRobot : public CATBaseUnknown
{
 /**
 * @nodoc
 */
   CATDeclareInterface;

public:    
     /**
     * Do not use this method - Deprecated from R17 onwards. 
     * Use GetMaxTCPLinSpeed() method instead of 
     * this method.
    * Get the max linear speed of robot TCP.
    * @param 
	*	none
    * @return double
    */
	virtual double
    getMaxLinearSpeed( ) const = 0;


	/**
    * Get the mount link of the "index"th Kin chain
	* default: first kin chain ( or the only existing kin chain )
    * @param
	*   Input: index ( index of the kin chain [ 1 default ] )
    * @return
    *  Mount link of the "index"th kin chain
    */
    virtual CATBaseUnknown_var
    GetMountLink( const int &iIndex = 0 ) const = 0;

  
    /**
    * Get the mount offset of the "index"th Kin chain
	* default: first kin chain ( or the only existing kin chain )
    * @param
	*   Input: index ( index of the kin chain [ 1 default ] )
    * @return
    *  Mount offset of the "index"th kin chain
    */
	virtual CATMathTransformation
    GetMountOffset( const int &iIndex = 0 ) const = 0;

    /**
    * Get the max linear speed of robot TCP
    * @param
	*	none
    * @return
    *  double
    */
    virtual double GetMaxTCPLinearSpeed( ) const = 0;

    /**
    * Get the max linear acceleration of robot  TCP
    * @param
	*	none
    * @return
    *  double
    */
    virtual double GetMaxTCPLinearAcceleration( ) const = 0;

     /**
    * Get the max angular speed(three components) of robot TCP.
    * @param
	*	In ---->Empty array of size three.
    *  Out--> Array containing the three components of max.
    *              angular speed.
    * @return
    *  none

    *    Example:
    *  
    *    double MaxAngSpeed[3] = {0.,0.,0.};
    *    pBasicRobot->GetMaxTCPAngSpeed(MaxAngSpeed);
    *                  :
    *                  :
    *    MaxAngularSpeedS1 = MaxAngSpeed[0];
    *    MaxAngularSpeedS2 = MaxAngSpeed[1];
    *   MaxAngularSpeedS3 = MaxAngSpeed[2];
    */
    virtual void GetMaxTCPAngularSpeed(double* oMaxTCPAngSpeed ) const = 0;

    /**
    * Get the max angular acceleration(three components) of robot TCP.
    * @param
	*	In ---->Empty array of size three.
    *  Out--> Array containing the three components of max.
    *              angular acceleration. 
    * @return
    *  none

    *    Example:
    *  
    *    double MaxAngAccl[3] = {0.,0.,0.};
    *    pBasicRobot->GetMaxTCPAngAcceleration(MaxAngAccl);
    *                  :
    *                  :
    *    MaxAngularAccelA1 = MaxAngAccl[0];
    *    MaxAngularAccelA2 = MaxAngAccl[1];
    *   MaxAngularAccelA3 = MaxAngAccl[2];
    */
    virtual void  GetMaxTCPAngularAcceleration(double* oMaxTCPAngAccl ) const = 0;

	/**
	* Get the list of Kinenamtic Chains
	* <br><b>Role</b>:Get the list of Kinenamtic Chains
	* @param oListOfChains (out)
	*	List of Kinematic Chains (CATBaseUnknown smart pointers)
	*	@return
	* <b>Legal values</b>:
	*	<br><tt>S_OK :</tt> If the kinematic chains are obtained successfully
	* 	<br><tt>E_FAIL:</tt>  if the kinematic chains are not obtained
	*/
	virtual HRESULT GetKinematicChains( CATListValCATBaseUnknown_var &oListOfChains ) const = 0;

};


CATDeclareHandler( DNBIBasicRobot, CATBaseUnknown );


#endif /* _DNBIBASICROBOT_H_ */

