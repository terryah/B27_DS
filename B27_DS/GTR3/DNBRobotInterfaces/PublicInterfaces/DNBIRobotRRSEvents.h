// COPYRIGHT DASSAULT SYSTEMES 2007
//===================================================================
//
// DNBIRobotRRSEvents.h
// Robot RRS Events Interface Definition
//
//===================================================================
//
// Usage notes:
//
//===================================================================
//
//  Dec 2007  Created   akp
//===================================================================
#ifndef _DNBIROBOTRRSEVENTS_H_
#define _DNBIROBOTRRSEVENTS_H_

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */
#include <CATBaseUnknown.h>
#include <CATBoolean.h>
#include <CATUnicodeString.h>
#include <CATListOfCATUnicodeString.h>

#include <DNBRobotItfCPP.h>

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByDNBRobotItfCPP IID IID_DNBIRobotRRSEvents;
#else
extern "C" const IID IID_DNBIRobotRRSEvents;
#endif


/**
 * Interface to manipulate robot's RRS events
 * <b>Role:</b>
 * This interface provides methods to manipulate robot's RRS events.
 */

class ExportedByDNBRobotItfCPP DNBIRobotRRSEvents : public CATBaseUnknown
{
    /**
     * @nodoc
     */
    CATDeclareInterface;

public: 

    // Public type definitions
    //

    /**
      * Specifies RRS Event and Target association type.
      * 
      * <br><B>Description</B><br>
      * This enumeration type specifies which targets the RRS Event should 
      * be associated with (see RRS-I spec for details).
      */
    enum RRSEventTargetType
    {
        /**
          * Specifies RRS Event should be associated with all future targets.
          * 
          * <br><B>Description</B><br>
          * This enumeration constant specifies that the RRS Event should be 
          * associated with all future targets.
          */
        RRS_EVENT_TIED_TO_ALL_TARGETS = 0,

        /**
          * Specifies RRS Event should be associated with only the next target.
          * 
          * <br><B>Description</B><br>
          * This enumeration constant specifies that the RRS Event should be 
          * associated with only the next target.
          */
        RRS_EVENT_TIED_TO_NEXT_TARGET,

        /**
          * Specifies RRS Event should be associated with only the target/tag
          * named "target_name".
          * 
          * <br><B>Description</B><br>
          * This enumeration constant specifies that the RRS Event should be 
          * associated with only the target/tag named "target_name".
          */
        RRS_EVENT_TIED_TO_SELECT_TARGET
    };

    /**
      * Specifies the type of the RRS Event.
      * 
      * <br><B>Description</B><br>
      * This enumeration type specifies the type of the RRS Event (see RRS-I 
      * spec for details).
      */
    enum RRSEventType
    {
        /**
          * Specifies an RRS Time Event.
          * 
          * <br><B>Description</B><br>
          * This enumeration constant specifies an RRS Time Event.
          */
        RRS_TIME_EVENT = 0,

        /**
          * Specifies RRS Distance Event.
          * 
          * <br><B>Description</B><br>
          * This enumeration constant specifies an RRS Distance Event.
          */
        RRS_DISTANCE_EVENT,

        /**
          * Specifies RRS Time and Distance Event.
          * 
          * <br><B>Description</B><br>
          * This enumeration constant specifies an RRS Time and Distance Event.
          */
        RRS_TIME_AND_DISTANCE_EVENT,

        /**
          * Specifies RRS Condition Event.
          * 
          * <br><B>Description</B><br>
          * This enumeration constant specifies an RRS Condition Event.
          */
        RRS_CONDITION_EVENT,

        /**
          * Specifies RRS Relative Distance Event.
          * 
          * <br><B>Description</B><br>
          * This enumeration constant specifies an RRS Relative Distance Event.
          */
        RRS_RELATIVE_DISTANCE_EVENT,

        /**
          * Specifies RRS Time and Relative Distance Event.
          * 
          * <br><B>Description</B><br>
          * This enumeration constant specifies an RRS Time and Relative 
          * Distance Event.
          */
        RRS_TIME_AND_RELATIVE_DISTANCE_EVENT
    };

    /**
      * Specifies a condition axis type for use as event_spec_1 in an RRS 
      * Condition Event definition.
      * 
      * <br><B>Description</B><br>
      * This enumeration type specifies a condition axis type for use as 
      * event_spec_1 in an RRS Condition Event definition (see RRS-I spec 
      * for details).
      */
    enum RRSConditionAxisEventSpecType
    {
        /**
          * Specifies a "X axis" RRS Condition Event event_spec_1 type.
          * 
          * <br><B>Description</B><br>
          * This enumeration constant specifies a "X axis" RRS Condition 
          * Event event_spec_1 type.
          */
        RRS_CONDITION_EVENT_AXIS_X = 1,

        /**
          * Specifies a "Y axis" RRS Condition Event event_spec_1 type.
          * 
          * <br><B>Description</B><br>
          * This enumeration constant specifies a "Y axis" RRS Condition 
          * Event event_spec_1 type.
          */
        RRS_CONDITION_EVENT_AXIS_Y = 2,

        /**
          * Specifies a "Z axis" RRS Condition Event event_spec_1 type.
          * 
          * <br><B>Description</B><br>
          * This enumeration constant specifies a "Z axis" RRS Condition 
          * Event event_spec_1 type.
          */
        RRS_CONDITION_EVENT_AXIS_Z = 3
    };

    /**
      * Specifies a condition type for use as event_spec_2 in an RRS 
      * Condition Event definition.
      * 
      * <br><B>Description</B><br>
      * This enumeration type specifies a condition type for use as event_spec_2 
      * in an RRS Condition Event definition (see RRS-I spec for details).
      */
    enum RRSConditionEventSpecType
    {
        /**
          * Specifies a "greater than" RRS Condition Event event_spec_2 type.
          * 
          * <br><B>Description</B><br>
          * This enumeration constant specifies a "greater than" RRS Condition 
          * Event event_spec_2 type.
          */
        RRS_CONDITION_EVENT_SPEC_GREATER_THAN = 0,

        /**
          * Specifies a "less than" RRS Condition Event event_spec_2 type.
          * 
          * <br><B>Description</B><br>
          * This enumeration constant specifies a "less than" RRS Condition 
          * Event event_spec_2 type.
          */
        RRS_CONDITION_EVENT_SPEC_LESS_THAN = 1
    };


    // Public methods
    //

    /**
     * Checks if RRS Events is supported (by the RCS module).
     * @param 
     *  None.
     * @return
     *  TRUE if RRS Events is supported. FALSE otherwise.
     */
    virtual CATBoolean
    IsRRSEventsSupported (void) = 0;

    /**
     * Checks if given RRS Event "target_type" is supported (by the RCS module).
     * @param target_type
     *  RRS-I event target type (see RRS-I spec).
     * @return
     *  TRUE if given "target_type" is supported. FALSE otherwise.
     */    
    virtual CATBoolean
    IsTargetTypeSupported (
        RRSEventTargetType target_type) = 0;

    /**
     * Checks if given RRS Event "event_type" is supported (by the RCS module).
     * @param event_type
     *  RRS-I event type (see RRS-I spec).
     * @return
     *  TRUE if given "event_type" is supported. FALSE otherwise.
     */    
    virtual CATBoolean
    IsEventTypeSupported (
        RRSEventType event_type) = 0;

    /**
     * Checks if a "target_type == RRS_EVENT_TIED_TO_ALL_TARGETS" RRS Event 
     * is automatically deleted by the RCS module when such an Event is triggered
     * the very first time -- per RRS-I spec this should not happen but some RCS
     * modules operate this way.
     * @param 
     *  None.
     * @return
     *  TRUE if RCS automatically deletes such RRS Event upon their first trigger. 
     *  FALSE otherwise.
     */    
    virtual CATBoolean
    IsAllTargetsEventDeletedWhenFirstTriggered (void) = 0;

    /**
     * Returns a currently unused Event ID.
     * @param 
     *  None.
     * @return
     *  Returns a currently unused Event ID. If <0 is returned, this indicates 
     *  could not find an unsused Event ID.
     */    
    virtual int
    GetUnusedEventID (void) = 0;

    /**
     * Define a new RRS-I event based on passed input parameters (see RRS-I spec).
     * @param target_type
     *  RRS-I event target type.
     * @param target_name
     *  If target_type is RRS_EVENT_TIED_TO_SELECT_TARGET, name of target/tag. Otherwise,
     *  ignored.
     * @param event_type
     *  RRS-I event type.
     * @param event_spec_1
     *  RRS-I event spec #1. If event_type is RRS_CONDITION_EVENT must be set to an
     *  RRSConditionAxisEventSpecType enumeration constant value.
     * @param event_spec_2
     *  RRS-I event spec #2. If event_type is RRS_CONDITION_EVENT must be set to an
     *  RRSConditionEventSpecType enumeration constant value.
     * @param event_spec_3
     *  RRS-I event spec #3. 
      * @param interrupt_sim_when_event_triggers
     *  If TRUE, simulation will be interrupted when this event triggers (FALSE by default).
     * @return
     *  event_id (internal RRS event ID assigned to/identifying this event [>=0]). If 
     *  <0 returned, this indicates the event could not be defined.
     */
    virtual int
    DefineRRSEvent (
        RRSEventTargetType target_type, 
        const CATUnicodeString& target_name, 
        RRSEventType event_type, 
        double event_spec_1, 
        double event_spec_2, 
        double event_spec_3,
        CATBoolean interrupt_sim_when_event_triggers = FALSE) = 0;

    /**
     * Cancel specified (event_id >=0) or all (event_id < 0) previously
     * defined RRS-I event(s).
     * @param event_id
     *  Internal RRS event ID of event to cancel or -1 for all events.
     * @return
     *  None
     */
    virtual void
    CancelRRSEvent  (
        int event_id) = 0;

    /**
     * Check if previously defined RRS-I event has trigerred.
     * @param event_id
     *  Internal RRS event ID of event being checked for triggering.
     * @return
     *  TRUE if event has trigerred and FALSE otherwise.
     */
    virtual CATBoolean
    ChkRRSEventTriggered  (
        int event_id) const = 0;

    /**
     * Get list of currently active RRS-I events and their attributes.
     * @param event_ids
     *  List of internal RRS event IDs for each active RRS Event.
     * @param target_names
     *  List of target names for each active RRS Event.
     * @param event_specs
     *  List of event specifications for each active RRS Event.
     * @param trigger_actions
     *  List of trigger actions for each active RRS Event.
     * @return
     *  The number of active RRS Events.
     */
    virtual int
    GetActiveRRSEvents (
        CATListOfCATUnicodeString& event_ids, 
        CATListOfCATUnicodeString& target_names, 
        CATListOfCATUnicodeString& event_specs, 
        CATListOfCATUnicodeString& trigger_actions) const = 0;
};

CATDeclareHandler( DNBIRobotRRSEvents, CATBaseUnknown );

#endif /* _DNBIROBOTRRSEVENTS_H_ */

