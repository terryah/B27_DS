/* -*-c++-*- */
// COPYRIGHT DELMIA INC. 2002
//=============================================================================
// DNBIAuxDevicesPlugListP.h
//   This interface allows the management of a list of auxiliary devices.
//=============================================================================
// Usage Notes:
//
//=============================================================================
// Jan. 2002  Creation                                                      awn
//=============================================================================

#ifndef DNBIAuxDevicesPlugListP_H
#define DNBIAuxDevicesPlugListP_H

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */


/**
 * @collection DNBIAuxDevicesPlugListP
 * Collection class for pointers to DNBIAuxDevicesPlug.
 * Refer to the articles dealing with collections in the encyclopedia.
 */

#include "DNBRobotItfCPP.h"
class DNBIAuxDevicesPlug;

// clean previous functions requests
#include <CATLISTP_Clean.h>

// require needed functions
#include <CATLISTP_AllFunct.h>

// get macros
#include  <CATLISTP_Declare.h>

// generate interface of collection-class
// (functions declarations)
#undef  CATCOLLEC_ExportedBy
#define CATCOLLEC_ExportedBy    ExportedByDNBRobotItfCPP

/**
 * @nodoc
 */
CATLISTP_DECLARE(DNBIAuxDevicesPlug)
typedef CATLISTP(DNBIAuxDevicesPlug) DNBIAuxDevicesPlugListP ;
#endif

