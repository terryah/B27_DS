// COPYRIGHT DASSAULT SYSTEMES 2008
//===================================================================
//
// DNBIRobotRRS2ApplicativeCommands.h
// Robot RRS Status Interface Definition
//
//===================================================================
//
// Usage notes:
//
//===================================================================
//
//  Jan 2009  Created   akp
//===================================================================
#ifndef _DNBIROBOTRRS2APPLICATIVECOMMANDS_H_
#define _DNBIROBOTRRS2APPLICATIVECOMMANDS_H_

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */
#include <CATBaseUnknown.h>
#include <CATBoolean.h>
#include <CATUnicodeString.h>

#include <DNBRobotItfCPP.h>

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByDNBRobotItfCPP IID IID_DNBIRobotRRS2ApplicativeCommands;
#else
extern "C" const IID IID_DNBIRobotRRS2ApplicativeCommands;
#endif


/**
 * Interface to be notified about RRS-II Applicative Commands sent by 
 * the VRC module for on an RRS-II connected robot.
 * <b>Role:</b>
 * This interface provides methods to be notified about RRS-II 
 * Applicative Commands sent by the VRC module for on an RRS-II connected 
 * robot.
 */

class ExportedByDNBRobotItfCPP DNBIRobotRRS2ApplicativeCommands : public CATBaseUnknown
{
    /**
     * @nodoc
     */
    CATDeclareInterface;

public: 

    // Public methods
    //

    /**
     * Requests RRS motion controller to invoke 
     * "rrs2_applicative_cmd_cb (callback_id, app_cmd)"
     * when the VRC module sends an RRS-II applicative command with 
     * app_cmd containing the sent sent RRS-II applicative command . This 
     * should be called in the "begin" method of the proclet after calling 
     * the motion planner "resetProcess" method.
     * @param  rrs2_applicative_cmd_cb
     *  Callback with void* and CATUnicodeString& arguments and returning nothing 
     *  that is to be invoked when the VRC sends an RRS-II applicative command.
     * @param  callback_id
     *  ID that the caller associated with this callback registration. 
     *  "rrs2_applicative_cmd_cb" will be invoked with this value as the first argument.
     * @return
     *  TRUE if this request was processed successfully. FALSE otherwise.
     */

    virtual CATBoolean 
    RegisterRRS2ApplicativeCmdCallback (
        void* rrs2_applicative_cmd_cb /* void (*rrs2_applicative_cmd_cb)(void* callback_id, CATUnicodeString& app_cmd) */,
        void* callback_id) = 0;
};

CATDeclareHandler( DNBIRobotRRS2ApplicativeCommands, CATBaseUnknown );

#endif /* _DNBIROBOTRRS2APPLICATIVECOMMANDS_H_ */

