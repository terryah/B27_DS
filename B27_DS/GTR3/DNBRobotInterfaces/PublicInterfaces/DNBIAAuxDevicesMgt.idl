//=============================================================================
// COPYRIGHT DASSAULT SYSTEMES DELMIA Corp. 2010
//=============================================================================
//
// DNBIAAuxDevicesMgt:
// Exposed interface for AuxDevicesMgt. 
//
//=============================================================================
// Usage notes:
//
//=============================================================================
// Jan 2010     Creation                                            GJA 
//
//=============================================================================

#ifndef DNBIAAuxDevicesMgt_IDL
#define DNBIAAuxDevicesMgt_IDL

/*IDLREP*/
/* -*-idl-*- */

/**
  * @CAA2Level L1
  * @CAA2Usage U3
  */

#include "CATSafeArray.idl"
#include "DNBAuxilliaryDeviceType.idl"
#include "CATIABase.idl"

/** 
 * INTERFACE :
 * DNBIAAuxDevicesMgt
 * <br>
 * DESCRIPTION
 * This allows add/remove and get/set of aux devices for a robot
 */

interface DNBIAAuxDevicesMgt: CATIABase
{

    /**
     * Add a aux device to the robot.
     * @param iAuxDeviceObj
     *   This parameter should be the device to add as aux device.
     * @param iAuxDeviceType
     *   This parameter should be the type of aux device.
     * @return
     *  an HRESULT value.
     * <br><b>Legal values</b>:
     * <ul>
     * <li>S_OK if the operation succeeds </li>
     * <li>E_FAIL otherwise </li>.
     * </ul>
     */
    HRESULT DefineAuxDevices (in CATIABase iAuxDeviceObj, in DNBAuxilliaryDeviceType iAuxDeviceType);

    /**
     * Get the number of aux devices for the robot.
     * @param nbAuxDevices
     *   This outer parameter contains the number of aux devices.
     * @return
     *  an HRESULT value.
     * <br><b>Legal values</b>:
     * <ul>
     * <li>S_OK if the operation succeeds </li>
     * <li>E_FAIL otherwise </li>.
     * </ul>
     */
    HRESULT GetNbAuxDevices (out /*IDLRETVAL*/ long nbAuxDevices);

    /**
     * Get all aux devices defined for the robot.
     * @param oAuxDeviceList
     *   This outer parameter contains list of aux devices.
     * @return
     *  an HRESULT value.
     * <br><b>Legal values</b>:
     * <ul>
     * <li>S_OK if the operation succeeds </li>
     * <li>E_FAIL otherwise </li>.
     * </ul>
     */
    HRESULT GetAllAuxDevices (out CATSafeArrayVariant oAuxDeviceList);

    /**
     * Get the aux device given the the index.
     * @param iAuxDeviceNum
     *   This parameter should be the index of the aux device. Valid values are from 1 to nbAuxDevices.
     * @param oAuxDevice
     *   This outer parameter contains aux device at the given index iAuxDeviceNum.
     * @return
     *  an HRESULT value.
     * <br><b>Legal values</b>:
     * <ul>
     * <li>S_OK if the operation succeeds </li>
     * <li>E_FAIL otherwise </li>.
     * </ul>
     */
    HRESULT GetAuxDevices (in long iAuxDeviceNum, out /*IDLRETVAL*/ CATIABase oAuxDevice);

    /**
     * Get the aux device given the the index.
     * @param iAuxDeviceNum
     *   This parameter should be the index of the aux device. Valid values are from 1 to nbAuxDevices.
     * @param oAuxDeviceType
     *   This outer parameter contains aux device type for the aux device at the given index iAuxDeviceNum.
     * @return
     *  an HRESULT value.
     * <br><b>Legal values</b>:
     * <ul>
     * <li>S_OK if the operation succeeds </li>
     * <li>E_FAIL otherwise </li>.
     * </ul>
     */

    HRESULT GetAuxDevicesType (in long iAuxDeviceNum, out /*IDLRETVAL*/ DNBAuxilliaryDeviceType oAuxDeviceType);
    /**
     * Remove all the aux devices defined for the robot.
     * @return
     *  an HRESULT value.
     * <br><b>Legal values</b>:
     * <ul>
     * <li>S_OK if the operation succeeds </li>
     * <li>E_FAIL otherwise </li>.
     * </ul>
     */
    HRESULT RemoveAll();

    /**
     * Remove the aux device at the given index.
     * @param iAuxDeviceNum
     *   This parameter should be the index of the aux device to be removed. 
     *   Valid values are from 1 to nbAuxDevices. 
     * @return
     *  an HRESULT value.
     * <br><b>Legal values</b>:
     * <ul>
     * <li>S_OK if the operation succeeds </li>
     * <li>E_FAIL otherwise </li>.
     * </ul>
     */
    HRESULT RemoveAuxDevicesByPosition (in long iAuxDeviceNum);
    
};

// Interface name : DNBIAAuxDevicesMgt
#pragma ID DNBIAAuxDevicesMgt "DCE:73EF5B05-2623-499c-9646A9F29DFAEF6A"
#pragma DUAL DNBIAAuxDevicesMgt

// VB object name : AuxDevicesMgt (Id used in Visual Basic)
#pragma ID AuxDevicesMgt "DCE:697CD310-ACD8-425c-9A51BCF85A6ABC39"
#pragma ALIAS DNBIAAuxDevicesMgt AuxDevicesMgt

#endif

