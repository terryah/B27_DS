// COPYRIGHT DASSAULT SYSTEMES 2008
//===================================================================
//
// DNBIRobotRRS2Operations.h
// Robot RRS Status Interface Definition
//
//===================================================================
//
// Usage notes:
//
//===================================================================
//
//  Apr 2008  Created   akp
//===================================================================
#ifndef _DNBIROBOTRRS2OPERATIONS_H_
#define _DNBIROBOTRRS2OPERATIONS_H_

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */
#include <CATBaseUnknown.h>
#include <CATBoolean.h>
#include <CATUnicodeString.h>

#include <DNBRobotItfCPP.h>

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByDNBRobotItfCPP IID IID_DNBIRobotRRS2Operations;
#else
extern "C" const IID IID_DNBIRobotRRS2Operations;
#endif


/**
 * Interface to perform operations on an RRS-II connected robot.
 * <b>Role:</b>
 * This interface provides methods to perform operations on an 
 * RRS-II connected robot.
 */

class ExportedByDNBRobotItfCPP DNBIRobotRRS2Operations : public CATBaseUnknown
{
    /**
     * @nodoc
     */
    CATDeclareInterface;

public: 

    // Public methods
    //

    /**
     * Requests the VRC module to load/activate given VRC program file/name. 
     * @param vrc_program_path
     *  Path of VRC program file to be loaded. 
     * @param vrc_program_name
     *  Path of VRC program file to be loaded. 
     * @return
     *  TRUE if the the request was successfully issued and FALSE if not 
     *  (due to robot not being RRS-II connected).
     */    
    virtual CATBoolean 
    LoadActivateVRCProgram (
        const CATUnicodeString& vrc_program_path,
        const CATUnicodeString& vrc_program_name) = 0;
};

CATDeclareHandler( DNBIRobotRRS2Operations, CATBaseUnknown );

#endif /* _DNBIROBOTRRS2OPERATIONS_H_ */

