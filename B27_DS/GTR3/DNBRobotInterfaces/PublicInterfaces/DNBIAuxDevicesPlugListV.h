/* -*-c++-*- */
// COPYRIGHT DELMIA INC. 2002
//=============================================================================
// DNBIAuxDevicesPlugListV.h
//   This interface allows the management of a list of auxiliary devices.
//=============================================================================
// Usage Notes:
//
//=============================================================================
// Jan. 2002  Creation                                                      awn
//=============================================================================

#ifndef DNBIAuxDevicesPlugListV_H
#define DNBIAuxDevicesPlugListV_H

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

/**
 * @collection DNBIAuxDevicesPlugListV
 * Collection class for pointers to DNBIAuxDevicesPlug.
 * Refer to the articles dealing with collections in the encyclopedia.
 */

#include "DNBRobotItfCPP.h"
#include "DNBIAuxDevicesPlug.h"

// clean previous functions requests
#include <CATLISTV_Clean.h>

// require needed functions
//#include  <CATLISTV_AllFunct.h>
#define	CATLISTV_AppendList
#define	CATLISTV_InsertAfter
#define	CATLISTV_InsertBefore
#define	CATLISTV_InsertAt
#define	CATLISTV_ReSize
#define	CATLISTV_Locate
#define	CATLISTV_RemoveValue
#define	CATLISTV_RemoveList
#define	CATLISTV_RemovePosition
#define	CATLISTV_RemoveDuplicatesExtract
#define	CATLISTV_RemoveDuplicatesCount
#define	CATLISTV_Compare
#define	CATLISTV_Replace
#define	CATLISTV_Swap
#define	CATLISTV_NbOccur
#define	CATLISTV_Intersection
#define CATLISTV_Append
#define	CATLISTV_RemoveAll


// get macros
#include  <CATLISTV_Declare.h>

// generate interface of collection-class
// (functions declarations)
#undef  CATCOLLEC_ExportedBy
#define CATCOLLEC_ExportedBy    ExportedByDNBRobotItfCPP

/**
 * @nodoc
 */
CATLISTV_DECLARE(DNBIAuxDevicesPlug_var)
typedef CATLISTV(DNBIAuxDevicesPlug_var) DNBIAuxDevicesPlugListV ;
#endif

