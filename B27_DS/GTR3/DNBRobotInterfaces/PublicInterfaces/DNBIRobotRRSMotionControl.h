// COPYRIGHT DASSAULT SYSTEMES 2008
//===================================================================
//
// DNBIRobotRRSMotionControl.h
// Robot RRS Motion Control Interface Definition
//
//===================================================================
//
// Usage notes:
//
//===================================================================
//
//  Jan 2008  Created   akp
//===================================================================
#ifndef _DNBIROBOTRRSMOTIONCONTROL_H_
#define _DNBIROBOTRRSMOTIONCONTROL_H_

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */
#include <CATBaseUnknown.h>
#include <CATBoolean.h>
#include <CATUnicodeString.h>

#include <DNBRobotItfCPP.h>

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByDNBRobotItfCPP IID IID_DNBIRobotRRSMotionControl;
#else
extern "C" const IID IID_DNBIRobotRRSMotionControl;
#endif


/**
 * Interface to manipulate robot's RRS events
 * <b>Role:</b>
 * This interface provides methods to manipulate robot's RRS events.
 */

class ExportedByDNBRobotItfCPP DNBIRobotRRSMotionControl : public CATBaseUnknown
{
    /**
     * @nodoc
     */
    CATDeclareInterface;

public: 

    // Public methods
    //

    /**
     * Sets a V5 RRS attribute on the next RRS move (similar to a V5 attribute
     * defined on a Robot Move activity by the user). Should be called in
     * the begin () method of a proclet that invokes the (RRS) motion planner 
     * and after the motion planner's "resetProcess ()" method has been called.
     * @param  name
     *  Name of the RRS attribute.
     * @param  value
     *  Value of the RRS attribute.
     * @param  type
     *  Type of the RRS attribute. Must be one of "" (meaning just setting
     *  the value of an attribute set in a previous RRS move during the same
     *  simulation run), "String", "Integer", or "Double".
     * @return
     *  TRUE if attribute was successfully set. FALSE otherwise.
     */
    virtual CATBoolean
    SetRRSAttribute  (
        const CATUnicodeString& name,
        const CATUnicodeString& value,
        const CATUnicodeString& type = "") = 0;
    
    /**
     * Gets the value of the named V5 RRS attribute.
     * @param  name
     *  Name of RRS attribute.
     * @param  value
     *  Value of RRS attribute.
     * @return
     *  TRUE if attribute was found and the returned value is valid. FALSE 
     *  otherwise.
     */
    virtual CATBoolean
    GetRRSAttribute  (
        const CATUnicodeString& name,
        CATUnicodeString& value) = 0; 

    /**
     * Disables default "RCS look ahead handling" by the RRS motion planner during
     * a simulation run. This should be called in the "begin" method of the proclet
     * at the start of a new simulation run immediately after calling motion 
     * planner's "resetProcess" method.
     * @param  
     *  None.
     * @return
     *  TRUE if this request was processed successfully. FALSE otherwise.
     */

    virtual CATBoolean 
    DisableDfltRCSLookAheadHandling (void) = 0;

    /**
     * Requests RRS motion controller to invoke "robot_is_here_functor (event_id)"
     * when the robot reaches the next target to be sent to the RCS module. This 
     * should be called in the "begin" method of the proclet after calling 
     * "DisableDfltRCSLookAheadHandling" and motion planner "resetProcess" method.
     * @param  robot_is_here_callback
     *  Callback with one integer argument and returning nothing that is to 
     *  be invoked when the robot reaches the next target sent to the RCS module.
     * @param  event_id
     *  ID that the caller associated with this event. "robot_is_here_callback"
     *  will be invoked with this parameter as its only argument.
     * @return
     *  TRUE if this request was processed successfully. FALSE otherwise.
     */

    virtual CATBoolean 
    NotifyWhenRRSRobotIsHere (
        void* robot_is_here_callback /* void (*robot_is_here_callback)(int) */,
        int event_id) = 0;
};

CATDeclareHandler( DNBIRobotRRSMotionControl, CATBaseUnknown );

#endif /* _DNBIROBOTRRSMOTIONCONTROL_H_ */

