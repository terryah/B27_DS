//
// COPYRIGHT Dassault Systemes 2005
//

/** @CAA2Required */
/*---------------------------------------------------------------------*/
/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */
/*---------------------------------------------------------------------*/	

#ifdef  _WINDOWS_SOURCE
#ifdef  __DNBRobotItfCPP
#define ExportedByDNBRobotItfCPP     __declspec(dllexport)
#else
#define ExportedByDNBRobotItfCPP     __declspec(dllimport)
#endif
#else
#define ExportedByDNBRobotItfCPP
#endif
