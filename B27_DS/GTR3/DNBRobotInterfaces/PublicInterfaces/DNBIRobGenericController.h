//
// COPYRIGHT Dassault Systemes 2005
//
//*
//* FILE:
//*     DNBIBasicRobotGenericCtlr.h    - protected header file
//*
//* MODULE:
//*     DNBRobotInterfaces
//*
//* OVERVIEW:
//*
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     xxx         02/07/2001  Initial Implementation
//*		xya			10/01/2003	remove XML support
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1999, 2001 Delmia, CORP.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Delmia, CORP., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*
// Jun. 2001  Modification                                                  awn
//              Implementation of new methods for XML streaming
//                -> SaveXML
//                -> LoadXML
// Apr  2005    Added 2 new APIs to set and get the attribute 
//              "SingularityTolerance" for the robot Controller.            pjr
// June 2005   Added 4 new APIs to set and get SampleRate and 
//			   Acceleration Mode											pjr
//  JAN 2006   Added APIs to get/set the response delay, settle time,            cub
//                        joint interpolation mode, time-based motion type
//=============================================================================

#ifndef _DNBIROBGENERICCONTROLLER_H_
#define _DNBIROBGENERICCONTROLLER_H_

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */


#include <CATBaseUnknown.h>
#include "DNBControllerProfileList.h"
#include "DNBRobotItfCPP.h"
//#include "DNBControllerDefs.h"
enum AccelerationMode
{
	Variable_Time = 0,
	Constant_Time = 1
};

enum MotionType
{
	Exact = 0,
	Constrained = 1
};

enum JointInterpolationMode
{
    Shortest_Angles = 0,
    Solution_Angles = 1,
    Turn_Numbers = 2,
    Absolute_Shortest_Angles = 3,
    Turn_Signs = 4
};

class DNBIGenericToolProfile_var;
class DNBIGenericMotionProfile_var;
class DNBIGenericAccuracyProfile_var;
class DNBIGenericObjFrameProfile_var;

extern ExportedByDNBRobotItfCPP IID IID_DNBIRobGenericController;

//------------------------------------------------------------------
/**
 * Interface to manipulate Robot Controller.
 * <b>Role:</b>
 * This interface provides methods to operate attributes related to
 * robot controller, especially profiles.
 */

class ExportedByDNBRobotItfCPP DNBIRobGenericController : public CATBaseUnknown 
{
    CATDeclareInterface;

public:
   
   /**
    * Set name of the generic controller.
    * @param name
    *   name of the controller to be set.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */      
    virtual HRESULT
    SetName( CATUnicodeString name ) = 0;

   /**
    * Retrieves the name of the controller.
    * @param name
    *   This out parameter contains name of the controller.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual HRESULT
    GetName( CATUnicodeString& name ) const = 0;


    //
    // Tool Profile related api's
    //
   /**
    * Add a new Tool Profile into controller.
    * @param profile
    *   smart interface pointer to tool profile
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual HRESULT 
    AddToolProfile( DNBIGenericToolProfile_var& profile ) = 0;

    /**
    * Get tool profile given name.
    * @param name
    *   This parameter contains name of the  tool profile.
    * @param profile
    *   This outer parameter contains interface pointer to tool profile.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual HRESULT
    GetToolProfile( const CATUnicodeString& name, DNBIGenericToolProfile_var& profile ) const = 0;

    /**
    * Get tool profiles list in controller.
    * @param profiles
    *   This outer parameter contains tool profile list.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual HRESULT
    GetToolProfiles( DNBGenericToolProfileList& profiles ) const = 0;

    /**
    * Check whether given tool profile exists.
    * @param name
    *   This parameter contains name of the tool profile.
    * @param flag
    *   This outer parameter contains whether tool profile exists.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual HRESULT 
    HasToolProfile( const CATUnicodeString& name, CATBoolean& flag ) const = 0;

    /**
    * Remove tool profile given name in controller.
    * @param name
    *   This parameter contains tool profile name.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual HRESULT 
    RemoveToolProfile( const CATUnicodeString& name ) = 0;

    /**
    * Remove all tool profiles in controller.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual HRESULT 
    RemoveToolProfiles( ) = 0;

    /**
    * Make tool profile with given name as current tool profile being used by controller.
    * @param profile
    *   This parameter contains tool profile name.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual HRESULT
    SetCurrentToolProfile( const CATUnicodeString& profile ) = 0;

    /**
    * Get current tool profile name used in controller.
    * @param profile
    *   This outer parameter contains tool profile name.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual HRESULT
    GetCurrentToolProfile( CATUnicodeString& profile ) const = 0;

    /**
    * Get number of tool profiles in controller.
    * @param count
    *   This outer parameter contains number of tool profiles.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
  
    virtual HRESULT 
    GetToolProfileCount( size_t& count ) const = 0;

    //
    // Motion Profile related api's
    //
   /**
    * Add a new Motion Profile into controller.
    * @param profile
    *   smart interface pointer to motion profile
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual HRESULT 
    AddMotionProfile( DNBIGenericMotionProfile_var& profile ) = 0;

    /**
    * Get motion profile given name.
    * @param name
    *   This parameter contains name of the motion profile.
    * @param profile
    *   This outer parameter contains interface pointer to motion profile.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual HRESULT
    GetMotionProfile( const CATUnicodeString& name, DNBIGenericMotionProfile_var& profile ) const = 0;

    /**
    * Get motion profiles list in controller.
    * @param profiles
    *   This outer parameter contains motion profile list.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual HRESULT
    GetMotionProfiles( DNBGenericMotionProfileList& profiles ) const = 0;

    /**
    * Check whether given motion profile exists.
    * @param name
    *   This parameter contains name of the motion profile.
    * @param flag
    *   This outer parameter contains whether motion profile exists.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual HRESULT 
    HasMotionProfile( const CATUnicodeString& name, CATBoolean& flag ) const = 0;

    /**
    * Remove motion profile given name in controller.
    * @param name
    *   This parameter contains motion profile name.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual HRESULT 
    RemoveMotionProfile( const CATUnicodeString& name ) = 0;

    /**
    * Remove all motion profiles in controller.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual HRESULT 
    RemoveMotionProfiles( ) = 0;

    /**
    * Make motion profile with given name as current.
    * @param profile
    *   This parameter contains motion profile name.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
     virtual HRESULT
    SetCurrentMotionProfile( const CATUnicodeString& profile ) = 0;

    /**
    * Get current motion profile name used in controller.
    * @param profile
    *   This outer parameter contains motion profile name.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
      virtual HRESULT
    GetCurrentMotionProfile( CATUnicodeString& profile ) const = 0;

    /**
    * Get number of motion profiles in controller.
    * @param count
    *   This outer parameter contains number of motion profiles.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
     virtual HRESULT 
    GetMotionProfileCount( size_t& count ) const = 0;

    //
    // Accuracy Profile related api's
    //
   /**
    * Add a new Accuracy Profile into controller.
    * @param profile
    *   smart interface pointer to accuracy profile
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual HRESULT 
    AddAccuracyProfile( DNBIGenericAccuracyProfile_var& profile ) = 0;

    /**
    * Get accuracy profile given name.
    * @param name
    *   This parameter contains name of the accuracy profile.
    * @param profile
    *   This outer parameter contains interface pointer to accuracy profile.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual HRESULT
    GetAccuracyProfile( const CATUnicodeString& name, 
			DNBIGenericAccuracyProfile_var& profile ) const = 0;

    /**
    * Get accurancy profiles list in controller.
    * @param profiles
    *   This outer parameter contains accurancy profile list.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual HRESULT
    GetAccuracyProfiles( DNBGenericAccuracyProfileList& profiles ) const = 0;

    /**
    * Check whether given accuracy profile exists.
    * @param name
    *   This parameter contains name of the accuracy profile.
    * @param flag
    *   This outer parameter contains whether accurancy profile exists.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual HRESULT 
    HasAccuracyProfile( const CATUnicodeString& name, CATBoolean& flag ) const = 0;

    /**
    * Remove accuracy profile given name in controller.
    * @param name
    *   This parameter contains accuracy profile name.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual HRESULT 
    RemoveAccuracyProfile( const CATUnicodeString& name ) = 0;

    /**
    * Remove all accuracy profiles in controller.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual HRESULT 
    RemoveAccuracyProfiles( ) = 0;

    /**
    * Make accuracy profile with given name as current.
    * @param profile
    *   This parameter contains accuracy profile name.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual HRESULT
    SetCurrentAccuracyProfile( const CATUnicodeString& profile ) = 0;

    /**
    * Get current accuracy profile name used in controller.
    * @param profile
    *   This outer parameter contains accuracy profile name.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
      virtual HRESULT
    GetCurrentAccuracyProfile( CATUnicodeString& profile ) const = 0;


    /**
    * Get number of accuracy profiles in controller.
    * @param count
    *   This outer parameter contains number of accuracy profiles.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
 
	  virtual HRESULT 
    GetAccuracyProfileCount( size_t& count ) const = 0;

    //
    // Object Profile related api's
    //
   /**
    * Add a new Object Frame Profile into controller.
    * @param profile
    *   smart interface pointer to object frame profile
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual HRESULT 
    AddObjFrameProfile( DNBIGenericObjFrameProfile_var& profile ) = 0;

    /**
    * Get obj frame profile given name.
    * @param name
    *   This parameter contains name of the obj frame profile.
    * @param profile
    *   This outer parameter contains interface pointer to objframe profile.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual HRESULT
    GetObjFrameProfile( const CATUnicodeString& name, DNBIGenericObjFrameProfile_var& profile ) const = 0;

    /**
    * Get objframe profiles list in controller.
    * @param profiles
    *   This outer parameter contains objframe profiles list.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual HRESULT
    GetObjFrameProfiles( DNBGenericObjFrameProfileList& profiles ) const = 0;

    /**
    * Check whether given objframe profile exists.
    * @param name
    *   This parameter contains name of the objframe profile.
    * @param flag
    *   This outer parameter contains whether objframe profile exists.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual HRESULT 
    HasObjFrameProfile( const CATUnicodeString& name, CATBoolean& flag ) const = 0;

    /**
    * Remove objframe profile given name in controller.
    * @param name
    *   This parameter contains objframe profile name.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual HRESULT 
    RemoveObjFrameProfile( const CATUnicodeString& name ) = 0;

    /**
    * Remove all objframe profiles in controller.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual HRESULT 
    RemoveObjFrameProfiles( ) = 0;

    /**
    * Make objframe profile with given name as current.
    * @param profile
    *   This parameter contains objframe profile name.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
    virtual HRESULT
    SetCurrentObjFrameProfile( const CATUnicodeString& profile ) = 0;

    /**
    * Get current objframe profile name used in controller.
    * @param profile
    *   This outer parameter contains objframe profile name.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
      virtual HRESULT
    GetCurrentObjFrameProfile( CATUnicodeString& profile ) const = 0;

    /**
    * Get number of objframe profiles in controller.
    * @param count
    *   This outer parameter contains number of objframe profiles.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl> 
    */
     virtual HRESULT 
    GetObjFrameProfileCount( size_t& count ) const = 0;

  /**@nodoc
    * Internal Use. ONLY FOR WORKCELL SEQUENCING.
    * METHODS : 
    *   <b>SaveControllerInXML</b>
    * <br>
    * DESCRIPTION :
    *   Stream the robot controller data in an XML node. The method needs
    * to know in which XML document nodes can be created and under
    * which node the controller data will be added.
    * 
    *@param <i>doc</i>
    *   DOM_Document. XML document where data will be saved.
    *@param <i>rootElem</i>
    *   DOM_Element. XML node where data will be plugged under.
    *
    *@see DOM_Document in XMLParser.
    *@see DOM_Element in XMLParser.
    *
    */
//    virtual HRESULT SaveControllerInXML(DOM_Document& doc,
  //                                      DOM_Element& resElem)=0;

  /**@nodoc
    * Internal Use. ONLY FOR WORKCELL SEQUENCING.
    * METHODS : 
    *   <b>LoadControllerInXML</b>
    * <br>
    * DESCRIPTION :
    *   Unstream the robot controller data from an XML node. The method
    * needs will create all profiles declared under the current node.
    *<br></br>
    * WARNING : the XML structure should be the same as the one when the
    * controller data are saved.
    * 
    *@param <i>doc</i>
    *   DOM_Document. XML document where data will be read.
    *@param <i>rootElem</i>
    *   DOM_Element. XML node where data will be found.
    *
    *@see DOM_Document in XMLParser.
    *@see DOM_Element in XMLParser.
    *
    */
//    virtual HRESULT LoadControllerFromXML(DOM_Document& doc,
  //                                        DOM_Element& resElem)=0;

         /**
     * Set the Sample Rate value in the controller( type :double precision, unit: seconds).
     * @param i_sampleRate
     *   This parameter defines the controller clock rate. The time required
	 *	 to complete all motion commands will always be an integral multiple
	 *	 of this value
     * @return
     *   An HRESULT.
     *   <br><b>Legal values</b>:
     *   <dl>
     *     <dt>S_OK</dt>
     *     <dd>The value for the attribute "SampleRate"
     *         aggregated by the robot controller is successfully set</dd>
     *     <dt>E_FAIL </dt>
     *     <dd>If setting fails
     *   </dl> 
     */
     virtual HRESULT 
     SetSampleRate( const double& i_sampleRate ) = 0;


     /**
     * Get the Sample Rate value set in the controller ( type: double precision, unit: seconds )
     * @param o_double
     *   This parameter defines the controller clock rate. The time required
	 *	 to complete all motion commands will always be an integral multiple
	 *	 of this value. This outer parameter contains the sampleRate
     * @return
     *   An HRESULT.
     *   <br><b>Legal values</b>:
     *   <dl>
     *     <dt>S_OK</dt>
     *     <dd>The value at the attribute "SampleRate"
     *         aggregated by the robot controller is successfully returned</dd>
     *     <dt>E_FAIL </dt>
     *     <dd>If it can not be retrieved successfully
     *   </dl> 
     */
     virtual HRESULT 
     GetSampleRate( double& o_sampleRate ) const = 0;

	 /**
     * Set the Acceleration Mode value in the controller( type : integer (enum) )
	 *	A value of "0" means "Variable_time" and "1" means "Constant_Time"
     * @param i_mode
     *   This parameter controls the way in which acceleration time is calculated
	 *	 for each move. A value of "0" means "Variable_time" and "1" means "Constant_Time"
     * @return
     *   An HRESULT.
     *   <br><b>Legal values</b>:
     *   <dl>
     *     <dt>S_OK</dt>
     *     <dd>The value for the attribute "AccelerationMode"
     *         aggregated by the robot controller is successfully set</dd>
     *     <dt>E_FAIL </dt>
     *     <dd>If it can not be set successfully
     *   </dl> 
     */
     virtual HRESULT 
     SetAccelerationMode( const AccelerationMode& i_mode ) = 0;


     /**
     * Get the Acceleration Mode value from the controller( type : integer (enum) )
	 *	A value of "0" means "Variable_time" and "1" means "Constant_Time"
     * @param o_mode
     *   This outer parameter contains the Acceleration Mode. 
	 *    A value of "0" means "Variable_time" and "1" means "Constant_Time"
     * @return
     *   An HRESULT.
     *   <br><b>Legal values</b>:
     *   <dl>
     *     <dt>S_OK</dt>
     *     <dd>The value at the attribute "AccelerationMode"
     *         aggregated by the robot controller is successfully returned</dd>
     *     <dt>E_FAIL </dt>
     *     <dd>If it can not be retrieved
     *   </dl> 
     */
     virtual HRESULT 
     GetAccelerationMode( AccelerationMode& o_mode ) const = 0;


	 /**
     * Set the singularity tolerance value in the controller( type :double precision).
     * @param i_double
     *   This inner parameter contains the new singularity tolerance value to be set
     * @return
     *   An HRESULT.
     *   <br><b>Legal values</b>:
     *   <dl>
     *     <dt>S_OK</dt>
     *     <dd>The value for the attribute "SingularityTolerance"
     *         aggregated by the robot controller is successfully set</dd>
     *     <dt>E_FAIL </dt>
     *     <dd>If it can not be set
     *   </dl> 
     */
     virtual HRESULT 
     SetSingularityTolerance( const double& i_double ) = 0;


     /**
     * Get the singularity tolerance value set in the controller ( type: double precision )
     * @param o_double
     *   This outer parameter contains the singularity tolerance
     * @return
     *   An HRESULT.
     *   <br><b>Legal values</b>:
     *   <dl>
     *     <dt>S_OK</dt>
     *     <dd>The value at the attribute "SingularityTolerance"
     *         aggregated by the robot controller is successfully returned</dd>
     *     <dt>E_FAIL </dt>
     *     <dd>If it can not be retrieved
     *   </dl> 
     */
     virtual HRESULT 
     GetSingularityTolerance( double& o_double ) const = 0;

/**
     * Set the robot controller type
     * @param i_ControllerType
     *   This inner parameter is the controller type to be set to the robot
     * @return
     *   An HRESULT.
     *   <br><b>Legal values</b>:
     *   <dl>
     *     <dt>S_OK</dt>
     *     <dd>The robot controller type is set successfully set</dd>
     *     <dt>E_FAIL </dt>
     *     <dd>If it can not be set
     *   </dl> 
     */
     virtual HRESULT 
     SetControllerType(const CATUnicodeString& i_ControllerType) = 0;


     /**
     * Get the controller type of the robot
     * @param o_ControllerType
     *   This outer parameter is the controller type of the robot
     * @return
     *   An HRESULT.
     *   <br><b>Legal values</b>:
     *   <dl>
     *     <dt>S_OK</dt>
     *     <dd>The robot controller type is successfully returned</dd>
     *     <dt>E_FAIL </dt>
     *     <dd>If it can not be retrieved
     *   </dl> 
     */
     virtual HRESULT 
     GetControllerType(CATUnicodeString& o_ControllerType ) const = 0;


/**
     * Set the Time-based motion type in the controller( type : integer (enum) ).
     * Exact(0) -------------> no speed limits taken into account when performing motion.
     * Constrained(1) ---> speed limits taken into account when performing motion.
     *
	 *	A value of "0" means "Exact" and "1" means "Constrained"
     * @param i_mode
     *   This parameter determines the motion type to be used 
     *    during robot motion planning.
	 *
     * @return
     *   An HRESULT.
     *   <br><b>Legal values</b>:
     *   <dl>
     *     <dt>S_OK</dt>
     *     <dd>The value for the attribute "TimeBasedMotionType"
     *         aggregated by the robot controller is successfully set</dd>
     *     <dt>E_FAIL </dt>
     *     <dd>If it can not be set successfully
     *   </dl> 
     */
     virtual HRESULT 
     SetTimeBasedMotionType( const MotionType& i_MotionType ) = 0;


     /**
     * Get the Time-based motion type from the controller( type : integer (enum) )
    * Exact(0) -------------> no speed limits taken into account when performing motion.
     * Constrained(1) ---> speed limits taken into account when performing motion.
     *
	 *	A value of "0" means "Exact" and "1" means "Constrained"
     * @param o_mode
     *   This outer parameter contains the Motion Type. 
     * @return
     *   An HRESULT.
     *   <br><b>Legal values</b>:
     *   <dl>
     *     <dt>S_OK</dt>
     *     <dd>The value at the attribute "TimeBasedMotionType"
     *         aggregated by the robot controller is successfully returned</dd>
     *     <dt>E_FAIL </dt>
     *     <dd>If it can not be retrieved
     *   </dl> 
     */
     virtual HRESULT 
     GetTimeBasedMotionType( MotionType& o_MotionType ) const = 0;

/**
     * Set the Response Delay value in the controller( type :double precision).	The amount of time 
        between the activation of the motion command and the actual motion of the robot is called 
        Response Delay.
     *
     * @param i_double
     *   This inner parameter contains the new response delay value to be set
     * @return
     *   An HRESULT.
     *   <br><b>Legal values</b>:
     *   <dl>
     *     <dt>S_OK</dt>
     *     <dd>The value for the attribute "Response Delay"
     *         aggregated by the robot controller is successfully set</dd>
     *     <dt>E_FAIL </dt>
     *     <dd>If it can not be set
     *   </dl> 
     */
     virtual HRESULT 
     SetResponseDelay( const double& i_double ) = 0;


     /**
     * Get the Response Delay value set in the controller ( type: double precision ).The amount of time 
        between the activation of the motion command and the actual motion of the robot is called 
        Response Delay.
     *
     * @param o_double
     *   This outer parameter contains the response delay.
     * @return
     *   An HRESULT.
     *   <br><b>Legal values</b>:
     *   <dl>
     *     <dt>S_OK</dt>
     *     <dd>The value at the attribute "Response Delay"
     *         aggregated by the robot controller is successfully returned</dd>
     *     <dt>E_FAIL </dt>
     *     <dd>If it can not be retrieved
     *   </dl> 
     */
     virtual HRESULT 
     GetResponseDelay( double& o_double ) const = 0;

     /**
     * Set the Settle Time value in the controller( type :double precision). The estimated 
     * amount of time required for settling of manipulator vibrations after reaching the point 
     * is called Settle Time.
     *
     * @param i_double
     *   This inner parameter contains the new settle time value to be set
     * @return
     *   An HRESULT.
     *   <br><b>Legal values</b>:
     *   <dl>
     *     <dt>S_OK</dt>
     *     <dd>The value for the attribute "Settle Time"
     *         aggregated by the robot controller is successfully set</dd>
     *     <dt>E_FAIL </dt>
     *     <dd>If it can not be set
     *   </dl> 
     */
     virtual HRESULT 
     SetSettleTime( const double& i_double ) = 0;


     /**
     * Get the Settle Time  value set in the controller ( type: double precision ).The estimated 
     * amount of time required for settling of manipulator vibrations after reaching the point 
     * is called Settle Time.
     *
     * @param o_double
     *   This outer parameter contains the settle time
     * @return
     *   An HRESULT.
     *   <br><b>Legal values</b>:
     *   <dl>
     *     <dt>S_OK</dt>
     *     <dd>The value at the attribute "Settle Time"
     *         aggregated by the robot controller is successfully returned</dd>
     *     <dt>E_FAIL </dt>
     *     <dd>If it can not be retrieved
     *   </dl> 
     */
     virtual HRESULT 
     GetSettleTime( double& o_double ) const = 0;

/**
     * Set the default Joint Interpolation Mode in the controller( type : integer (enum) ).
     * Shortest_Angles(0)
     * Solution_Angles(1)
     * Turn_Numbers(2)
     * Absolute_Shortest_Angles(3)
     * Turn_Signs(4)
     *
     * @param i_mode
     *   This parameter determines the default joint interpolation mode to be used 
	 *
     * @return
     *   An HRESULT.
     *   <br><b>Legal values</b>:
     *   <dl>
     *     <dt>S_OK</dt>
     *     <dd>The value for the attribute "JointInterpolationMode"
     *         aggregated by the robot controller is successfully set</dd>
     *     <dt>E_FAIL </dt>
     *     <dd>If it can not be set successfully
     *   </dl> 
     */
     virtual HRESULT 
     SetJointInterpolationMode( const JointInterpolationMode& i_InterpolMode ) = 0;


     /**
     * Get the default Joint Interpolation Mode from the controller( type : integer (enum) )
     * Shortest_Angles(0)
     * Solution_Angles(1)
     * Turn_Numbers(2)
     * Absolute_Shortest_Angles(3)
     * Turn_Signs(4)
     *
     * @param o_mode
     *   This outer parameter contains the joint interpolation mode. 
     * @return
     *   An HRESULT.
     *   <br><b>Legal values</b>:
     *   <dl>
     *     <dt>S_OK</dt>
     *     <dd>The value at the attribute "JointInterpolationMode"
     *         aggregated by the robot controller is successfully returned</dd>
     *     <dt>E_FAIL </dt>
     *     <dd>If it can not be retrieved
     *   </dl> 
     */
     virtual HRESULT 
     GetJointInterpolationMode( JointInterpolationMode& o_InterpolMode ) const = 0;

};


CATDeclareHandler( DNBIRobGenericController, CATBaseUnknown );


#endif /* _DNBIROBGENERICCONTROLLER_H_ */
