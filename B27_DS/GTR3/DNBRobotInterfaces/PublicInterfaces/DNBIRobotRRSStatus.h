// COPYRIGHT DASSAULT SYSTEMES 2008
//===================================================================
//
// DNBIRobotRRSStatus.h
// Robot RRS Status Interface Definition
//
//===================================================================
//
// Usage notes:
//
//===================================================================
//
//  Jan 2008  Created   akp
//  Apr 2008  IR B0617926: Added new methods to connect/disconnect 
//            RRS   akp
//===================================================================
#ifndef _DNBIROBOTRRSSTATUS_H_
#define _DNBIROBOTRRSSTATUS_H_

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */
#include <CATBaseUnknown.h>
#include <CATBoolean.h>
#include <CATUnicodeString.h>
#include <CATListOfCATUnicodeString.h>

#include <DNBRobotItfCPP.h>

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByDNBRobotItfCPP IID IID_DNBIRobotRRSStatus;
#else
extern "C" const IID IID_DNBIRobotRRSStatus;
#endif


/**
 * Interface to manipulate robot's RRS connectedness status
 * <b>Role:</b>
 * This interface provides methods to manipulate robot's RRS connectedness status.
 */

class ExportedByDNBRobotItfCPP DNBIRobotRRSStatus : public CATBaseUnknown
{
    /**
     * @nodoc
     */
    CATDeclareInterface;

public: 

    // Public methods
    //

    /**
     * Checks if RRS is enabled for the robot.
     * @param 
     *  None.
     * @return
     *  TRUE if RRS is enabled for the robot. FALSE otherwise.
     */
    virtual CATBoolean
    IsRRSConnected (void) = 0;

    /**
     * Gets the RRS controller type used for communication with the
     * RRS server. 
     * @param rrs_controller_type
     *  RRS controller type (i.e. the RRS interface name). 
     * @return
     *  TRUE if RRS is enabled for the robot. FALSE otherwise.
     */    
    virtual CATBoolean
    GetRRSControllerType (
        CATUnicodeString& rrs_controller_type) = 0;

    /**
     * Gets the name of the RRS server robot is connected to. 
     * @param rrs_server_name
     *  Name of the RRS server the robot is connected to.
     * @return
     *  TRUE if RRS is enabled for the robot. FALSE otherwise.
     */    
    virtual CATBoolean
    GetRRSServerName (
        CATUnicodeString& rrs_server_name) = 0;

    /**
     * Checks if an RRS-II (as opposed to RRS-I) connection is enabled for 
     * the robot.
     * @param 
     *  None.
     * @return
     *  TRUE if an RRS-II connection is enabled for the robot. FALSE otherwise.
     */    
    virtual CATBoolean
    UsingRRS2Server (void) = 0;
    
    /**
     * Gets the complete list of RRS server decorated names from
     * the "rrs.servers" file.
     * @param rrs_server_decorated_names
     *  Returned list of RRS server decorated names. Note that, decorated
     *  names also contain host and TCP port/RPC program number besides the
     *  raw RRS server name.
     * @return
     *  TRUE if successful and the returned rrs_server_decorated_names is
     *  valid. FALSE otherwise.
     */ 
    virtual CATBoolean
    GetRRSServerDecoratedNames (
        CATListOfCATUnicodeString& rrs_server_decorated_names) = 0;
    
    /**
     * Checks if given rrs_server_decorated_name corresponds to an RRS-I server 
     * (as opposed to an RRS-II server).
     * @param rrs_server_decorated_name
     *  Decorated name of RRS server. Note that, decorated
     *  names also contain host and TCP port/RPC program number besides the
     *  raw RRS server name.
     * @param is_rrs1_server
     *  Returned indication of whether rrs_server_name corresponds to 
     *  an RRS-I server (if TRUE) or an RRS-II server (if FALSE).
     * @return
     *  TRUE if rrs_server_name found in rrs.servers file and the returned
     *  is_rrs1_server value is valid. FALSE otherwise.
     */ 
    virtual CATBoolean
    ChkRRSServerType (
        const CATUnicodeString& rrs_server_decorated_name,
        CATBoolean& is_rrs1_server) = 0;
    
    /**
     * First call to establish RRS-I connection to given RRS-I RCS server 
     * using given connection parameters.
     * @param rrs_server_decorated_name
     *  Decorated name of RRS server. Note that, decorated
     *  names also contain host and TCP port/RPC program number besides the
     *  raw RRS server name.
     * @param dflt_robot_num
     *  Returned default RRS-I robot number.
     * @param min_robot_num
     *  Returned minimum acceptable RRS-I robot number.
     * @param max_robot_num
     *  Returned maximum acceptable RRS-I robot number.
     * @param rrs_server_host_name
     *  Returned RRS server host machine name.
     * @param rcs_home_directory
     *  Returned RCS home directory.
     * @param rcs_data_home_dir_list
     *  Returned RCS data home directory list.
     * @param dflt_relative_robot_path
     *  Returned default relative robot path.
     * @param manipulator_list
     *  Returned supported RRS-I manipulator strings list.
     * @param dflt_manipulator_index_in_manipulator_list
     *  Returned index of the default manipulator in manipulator_list  
     *  with a 1 corresponding to first item in the list.
     * @param manipulator_user_selectable
     *  Returned indication of whether or not manipulator string
     *  is user selectable. 
     * @return
     *  TRUE if step1 of RRS-I connection attempt was successful. 
     *  FALSE otherwise.
     */ 
    virtual CATBoolean
    RRS1ConnectStep1 (
        const CATUnicodeString& rrs_server_decorated_name,
        int& dflt_robot_num,
        int& min_robot_num,
        int& max_robot_num,
        CATUnicodeString& rrs_server_host_name,
        CATUnicodeString& rcs_home_directory,
        CATListOfCATUnicodeString& rcs_data_home_dir_list,
        int& dflt_rcs_data_home_dir_index,
        CATUnicodeString& dflt_relative_robot_path,
        CATListOfCATUnicodeString& manipulator_list,
        int& dflt_manipulator_index_in_manipulator_list,
        CATBoolean& manipulator_user_selectable) = 0;
    
    /**
     * Second (and final) call to establish RRS-I connection to given 
     * RRS-I RCS server using given connection parameters.
     * @param robot_num
     *  The RRS-I robot number to use.
     * @param rcs_data_home_directory
     *  The RRS-I RCS data home directory to use. 
     * @param relative_robot_path
     *  The RRS-I robot path directory to use (when combined with 
     *  rcs_data_home_directory). 
     * @param manipulator
     *  The RRS-I manipulator string to use. 
     * @param init_debug_enabled
     *  Indicates if RRS-I initialization debugging should be enabled or
     *  not. 
     * @return
     *  TRUE if RRS-I connection attempt was successful. FALSE otherwise.
     */ 
    virtual CATBoolean
    RRS1ConnectStep2 (
        int robot_num,
        const CATUnicodeString& rcs_data_home_directory,
        const CATUnicodeString& relative_robot_path,
        const CATUnicodeString& manipulator,
        CATBoolean init_debug_enabled = FALSE) = 0;
    
    /**
     * First call to establish RRS-II connection to given RRS-II VRC module 
     * using given connection parameters.
     * @param rrs_server_decorated_name
     *  Decorated name of RRS server. Note that, decorated
     *  names also contain host and TCP port/RPC program number besides the
     *  raw RRS server name.
     * @param controller_sw_version_list
     *  Returned supported VRC controller software versions list.
     * @param dflt_controller_sw_version_index
     *  Returned index of the default controller software version in 
     *  controller_sw_version_list with a 1 corresponding to first item 
     *  in the list.
     * @return
     *  TRUE if step1 of RRS-II connection attempt was successful. 
     *  FALSE otherwise.
     */ 
    virtual CATBoolean
    RRS2ConnectStep1 (
        const CATUnicodeString& rrs_server_decorated_name,
        CATListOfCATUnicodeString& controller_sw_version_list,
        int& dflt_controller_sw_version_index) = 0;

    /**
     * Second call to establish RRS-II connection to given RRS-II VRC module 
     * using given connection parameters.
     * @param controller_sw_version
     *  The VRC controller software version to use.
     * @param user_language_list
     *  Returned supported VRC user languages list.
     * @param dflt_user_language_index
     *  Returned index of the default VRC user language in 
     *  user_language_list with a 1 corresponding to first item 
     *  in the list.
     * @param controller_config_list
     *  Returned supported VRC controller configurations list.
     * @param dflt_controller_config_index
     *  Returned index of the default VRC controller configuration in 
     *  controller_config_list with a 1 corresponding to first item 
     *  in the list.
     * @return
     *  TRUE if step2 of RRS-II connection attempt was successful. 
     *  FALSE otherwise.
     */ 
    virtual CATBoolean
    RRS2ConnectStep2 (
        const CATUnicodeString& controller_sw_version,
        CATListOfCATUnicodeString& user_language_list,
        int& dflt_user_language_index,
        CATListOfCATUnicodeString& controller_config_list,
        int& dflt_controller_config_index) = 0;

    /**
     * Third (and final) call to establish RRS-II connection to given 
     * RRS-II VRC module using given connection parameters.
     * @param user_language
     *  The VRC user language to use.
     * @param controller_config
     *  The VRC controller configuration to use.
     * @param file_system_full_path
     *  The full path of VRC file system to use in initializing the
     *  VRC instance. Note that, for controller_config-based initialization,
     *  this parameter should be set to an empty string
     * @return
     *  TRUE if RRS-II connection attempt was successful. 
     *  FALSE otherwise.
     */ 
    virtual CATBoolean
    RRS2ConnectStep3 (
        const CATUnicodeString& user_language,
        const CATUnicodeString& controller_config,
        const CATUnicodeString& file_system_full_path = "") = 0;

    /**
     * Automatically RRS (RRS-I or -II) connects robots using RRS attributes
     * stored in persistent store (based on previous RRS connection).
     * @param 
     *  None.
     * @return
     *  TRUE if RRS connection attempt was successful. 
     *  FALSE otherwise.
     */ 
    virtual CATBoolean
    AutoRRSConnect (void) = 0;

    /**
     * Disconnects RRS (RRS-I or -II) for robot.
     * @param 
     *  None.
     * @return
     *  TRUE if RRS disconnection attempt was successful. 
     *  FALSE otherwise.
     */ 
    virtual CATBoolean                                
    RRSDisconnect (void) = 0;
};

CATDeclareHandler( DNBIRobotRRSStatus, CATBaseUnknown );

#endif /* _DNBIROBOTRRSSTATUS_H_ */

