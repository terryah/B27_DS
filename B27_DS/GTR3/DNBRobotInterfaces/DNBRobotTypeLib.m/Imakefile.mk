# COPYRIGHT DASSAULT SYSTEMES 2003
#======================================================================
# Imakefile for module DNBRobotTypeLib.m
# Module for compilation of the typelib
#======================================================================
#
#  Dec 2003  Creation: Code generated by the CAA wizard  sathish
#======================================================================
#
# TYPELIB          
#

BUILT_OBJECT_TYPE=TYPELIB
# no more BUILD_PRIORITY=4
 
# DO NOT EDIT :: THE CAA2 WIZARDS WILL ADD CODE HERE
WIZARD_LINK_MODULES = InfTypeLib
# END WIZARD EDITION ZONE

LINK_WITH = $(WIZARD_LINK_MODULES)
