# COPYRIGHT DASSAULT SYSTEMES 2004
#======================================================================
# Imakefile for module AUTLciVMItfCpp.m
# Module containing the C++ source included in your interface framework
#======================================================================
#
#  Sep 2004  Creation: Code generated by the CAA wizard  evc
#======================================================================
#
# NO BUILD            
#

BUILT_OBJECT_TYPE=NONE

# System dependant variables
#
OS = AIX
BUILD=NO
#
OS = HP-UX
BUILD=NO
#
OS = IRIX
BUILD=NO
#
OS = SunOS
BUILD=NO
#
OS = Windows_NT
BUILD=YES
#
OS = win_b64
BUILD=YES
