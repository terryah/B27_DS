# COPYRIGHT DASSAULT SYSTEMES 2002
#======================================================================
# Imakefile for module CATHealingAssistantFeature.m
#======================================================================
#
#  Feb 2002  Creation: Code generated by the CAA wizard  mlh
#  09/12/2002 : JFI ; Mise a jour suivant DDR
#  24/01/2003 : mlh ; ajout Pour CATHASVisuFeatures
#  16/04/2003 : mlh ; ajout Pour CATHASIntegration.
#  06/08/2003 : mlh ; ajout CATSurfacicUIResources
#  02/12/2005 : mlh ; report menage R201
#  14/03/2006 : mlh ; ajout CATHASUtility.
#  03/11/2006 : mlh ; R18 suppression CATConstraintModeler, CATConstraintModelerItf, CATHealingAssistantAlgos
#======================================================================
#
# SHARED LIBRARY
#
BUILT_OBJECT_TYPE=SHARED LIBRARY

# DO NOT EDIT :: THE CAA2 WIZARDS WILL ADD CODE HERE
WIZARD_LINK_MODULES = 
# END WIZARD EDITION ZONE

INCLUDED_MODULES =      CATHASLocalHealing \
                        CATHASLocalJoinImp \
                        CATHASFactory      \
                        CATHASError        \
                        CATHASJournal      \
                        CATHASUtility      \
                        CATHASVisuFeatures \
						CATHASIntegration

LINK_WITH =                         \
          AC0SPBAS                  \  # ObjectSpecsModeler
          AD0XXBAS                  \  # ObjectModelerBase
          BOHYBOPE                  \  # TopologicalOperators
          CATCGMGeoMath             \  # GeometricObjects : Module [CATCGMGeoMath.m], which is define as an alias, was automatically added in LINK_WITH.
          CATGitInterfaces          \  # GSMInterfaces 
          CATGmoError               \  # GSMModel
          CATGmoIntegration         \  # GSMModel
          CATGmoInterfaces          \  # GSMModel
          CATGmoUtilities           \  # GSMModel
          CATHASItf                 \  # CATHealingAssistantInterfaces 
          CATHealingServices        \  # CATHealingAssistantServices
          CATInteractiveInterfaces  \  # InteractiveInterfaces
		  CATLifDictionary		    \
		  CATLifRelations           \
          CATMathematics            \  # Mathematics
          CATMathStream             \  # Mathematics : Module [CATMathStream.m], which is define as an alias, was automatically added in LINK_WITH.
          CATMechanicalModeler      \  # MechanicalModeler
          CATSurfacicTopoOperators  \  # SurfacicTopoOperators
          CATTlgServices            \  # CATTlgServices
          CATTopologicalObjects     \  # NewTopologicalObjects
          CATTopologicalOperators   \  # NewTopologicalOperators
          CATVisualization          \  # Visualization
          CATViz                    \  # VisualizationBase
          JS0CORBA                  \  # System
          JS0GROUP                  \  # System
          KnowledgeItf              \  # KnowledgeInterfaces
          MecModItfCPP              \  # MecModInterfaces
          MF0STARTUP                \  # MechanicalModeler
          ON0GREXT                  \  # ObjectModelerBase
          SI0REPER                  \  # GenericNaming
          TopoOperError             \  # TopologicalOperators
          YI00IMPL                  \  # NewTopologicalObjects
          YN000MAT                  \  # Mathematics
          YP00IMPL                  \  # GeometricObjects


# System dependant variables
#
OS = AIX
#
OS = HP-UX
#
OS = IRIX
#
OS = SunOS
#
OS = Windows_NT
