#ifndef __TIE_DELIAssignmentAssistantNodeDisplay
#define __TIE_DELIAssignmentAssistantNodeDisplay

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "DELIAssignmentAssistantNodeDisplay.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DELIAssignmentAssistantNodeDisplay */
#define declare_TIE_DELIAssignmentAssistantNodeDisplay(classe) \
 \
 \
class TIEDELIAssignmentAssistantNodeDisplay##classe : public DELIAssignmentAssistantNodeDisplay \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DELIAssignmentAssistantNodeDisplay, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT IsNodeValidForDisplay(CATBoolean &IsValid) ; \
};



#define ENVTIEdeclare_DELIAssignmentAssistantNodeDisplay(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT IsNodeValidForDisplay(CATBoolean &IsValid) ; \


#define ENVTIEdefine_DELIAssignmentAssistantNodeDisplay(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT  ENVTIEName::IsNodeValidForDisplay(CATBoolean &IsValid)  \
{ \
return (ENVTIECALL(DELIAssignmentAssistantNodeDisplay,ENVTIETypeLetter,ENVTIELetter)IsNodeValidForDisplay(IsValid)); \
} \


/* Name of the TIE class */
#define class_TIE_DELIAssignmentAssistantNodeDisplay(classe)    TIEDELIAssignmentAssistantNodeDisplay##classe


/* Common methods inside a TIE */
#define common_TIE_DELIAssignmentAssistantNodeDisplay(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DELIAssignmentAssistantNodeDisplay, classe) \
 \
 \
CATImplementTIEMethods(DELIAssignmentAssistantNodeDisplay, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DELIAssignmentAssistantNodeDisplay, classe, 1) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DELIAssignmentAssistantNodeDisplay, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DELIAssignmentAssistantNodeDisplay, classe) \
 \
HRESULT  TIEDELIAssignmentAssistantNodeDisplay##classe::IsNodeValidForDisplay(CATBoolean &IsValid)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->IsNodeValidForDisplay(IsValid)); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DELIAssignmentAssistantNodeDisplay(classe) \
 \
 \
declare_TIE_DELIAssignmentAssistantNodeDisplay(classe) \
 \
 \
CATMetaClass * __stdcall TIEDELIAssignmentAssistantNodeDisplay##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DELIAssignmentAssistantNodeDisplay,"DELIAssignmentAssistantNodeDisplay",DELIAssignmentAssistantNodeDisplay::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DELIAssignmentAssistantNodeDisplay(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DELIAssignmentAssistantNodeDisplay, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDELIAssignmentAssistantNodeDisplay##classe(classe::MetaObject(),DELIAssignmentAssistantNodeDisplay::MetaObject(),(void *)CreateTIEDELIAssignmentAssistantNodeDisplay##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DELIAssignmentAssistantNodeDisplay(classe) \
 \
 \
declare_TIE_DELIAssignmentAssistantNodeDisplay(classe) \
 \
 \
CATMetaClass * __stdcall TIEDELIAssignmentAssistantNodeDisplay##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DELIAssignmentAssistantNodeDisplay,"DELIAssignmentAssistantNodeDisplay",DELIAssignmentAssistantNodeDisplay::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DELIAssignmentAssistantNodeDisplay(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DELIAssignmentAssistantNodeDisplay, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDELIAssignmentAssistantNodeDisplay##classe(classe::MetaObject(),DELIAssignmentAssistantNodeDisplay::MetaObject(),(void *)CreateTIEDELIAssignmentAssistantNodeDisplay##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DELIAssignmentAssistantNodeDisplay(classe) TIE_DELIAssignmentAssistantNodeDisplay(classe)
#else
#define BOA_DELIAssignmentAssistantNodeDisplay(classe) CATImplementBOA(DELIAssignmentAssistantNodeDisplay, classe)
#endif

#endif
