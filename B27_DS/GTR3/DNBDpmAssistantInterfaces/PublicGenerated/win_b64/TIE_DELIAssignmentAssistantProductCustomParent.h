#ifndef __TIE_DELIAssignmentAssistantProductCustomParent
#define __TIE_DELIAssignmentAssistantProductCustomParent

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "DELIAssignmentAssistantProductCustomParent.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DELIAssignmentAssistantProductCustomParent */
#define declare_TIE_DELIAssignmentAssistantProductCustomParent(classe) \
 \
 \
class TIEDELIAssignmentAssistantProductCustomParent##classe : public DELIAssignmentAssistantProductCustomParent \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DELIAssignmentAssistantProductCustomParent, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT GetCustomParentName(CATUnicodeString & oParentName); \
};



#define ENVTIEdeclare_DELIAssignmentAssistantProductCustomParent(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT GetCustomParentName(CATUnicodeString & oParentName); \


#define ENVTIEdefine_DELIAssignmentAssistantProductCustomParent(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT  ENVTIEName::GetCustomParentName(CATUnicodeString & oParentName) \
{ \
return (ENVTIECALL(DELIAssignmentAssistantProductCustomParent,ENVTIETypeLetter,ENVTIELetter)GetCustomParentName(oParentName)); \
} \


/* Name of the TIE class */
#define class_TIE_DELIAssignmentAssistantProductCustomParent(classe)    TIEDELIAssignmentAssistantProductCustomParent##classe


/* Common methods inside a TIE */
#define common_TIE_DELIAssignmentAssistantProductCustomParent(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DELIAssignmentAssistantProductCustomParent, classe) \
 \
 \
CATImplementTIEMethods(DELIAssignmentAssistantProductCustomParent, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DELIAssignmentAssistantProductCustomParent, classe, 1) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DELIAssignmentAssistantProductCustomParent, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DELIAssignmentAssistantProductCustomParent, classe) \
 \
HRESULT  TIEDELIAssignmentAssistantProductCustomParent##classe::GetCustomParentName(CATUnicodeString & oParentName) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetCustomParentName(oParentName)); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DELIAssignmentAssistantProductCustomParent(classe) \
 \
 \
declare_TIE_DELIAssignmentAssistantProductCustomParent(classe) \
 \
 \
CATMetaClass * __stdcall TIEDELIAssignmentAssistantProductCustomParent##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DELIAssignmentAssistantProductCustomParent,"DELIAssignmentAssistantProductCustomParent",DELIAssignmentAssistantProductCustomParent::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DELIAssignmentAssistantProductCustomParent(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DELIAssignmentAssistantProductCustomParent, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDELIAssignmentAssistantProductCustomParent##classe(classe::MetaObject(),DELIAssignmentAssistantProductCustomParent::MetaObject(),(void *)CreateTIEDELIAssignmentAssistantProductCustomParent##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DELIAssignmentAssistantProductCustomParent(classe) \
 \
 \
declare_TIE_DELIAssignmentAssistantProductCustomParent(classe) \
 \
 \
CATMetaClass * __stdcall TIEDELIAssignmentAssistantProductCustomParent##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DELIAssignmentAssistantProductCustomParent,"DELIAssignmentAssistantProductCustomParent",DELIAssignmentAssistantProductCustomParent::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DELIAssignmentAssistantProductCustomParent(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DELIAssignmentAssistantProductCustomParent, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDELIAssignmentAssistantProductCustomParent##classe(classe::MetaObject(),DELIAssignmentAssistantProductCustomParent::MetaObject(),(void *)CreateTIEDELIAssignmentAssistantProductCustomParent##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DELIAssignmentAssistantProductCustomParent(classe) TIE_DELIAssignmentAssistantProductCustomParent(classe)
#else
#define BOA_DELIAssignmentAssistantProductCustomParent(classe) CATImplementBOA(DELIAssignmentAssistantProductCustomParent, classe)
#endif

#endif
