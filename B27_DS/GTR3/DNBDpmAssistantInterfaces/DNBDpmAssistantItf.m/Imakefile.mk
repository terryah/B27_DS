# COPYRIGHT DASSAULT SYSTEMES 2006

#======================================================================
# Imakefile for module DNBDpmAssistantItf.m
#======================================================================

BUILT_OBJECT_TYPE=SHARED LIBRARY 
 
# DO NOT EDIT :: THE CAA2 WIZARDS WILL ADD CODE HERE
WIZARD_LINK_MODULES = 
# END WIZARD EDITION ZONE

LINK_WITH = JS0GROUP  \
            JS0CTYP   \
            DI0PANV2  \
            JS0CORBA  \            
            JS0FM     \
            CD0FRAME  \
            CATViz \
            ProcessInterfaces \
            CATVisualization \
            ON0MAIN \
            ON0FRAME
           


# System dependant variables
#
OS = AIX
#
OS = HP-UX
#
OS = IRIX
#
OS = SunOS
#
OS = Windows_NT
