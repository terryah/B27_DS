BUILT_OBJECT_TYPE=SHARED LIBRARY
PPRDCCServices_LINK_WITH =	JS03TRA JS0GROUP PPRDCCCORBA CATCsbCORBA CATCrmOrbInteractiveServices CATCrmMarkers

LINK_WITH = $(PPRDCCServices_LINK_WITH) orbix
PPRDCCCORBA = SERVER
#PPRDCCCORBA = SERVER
CATCsbCORBA = SERVER

OS = AIX
LOCAL_CCFLAGS = -DNATIVE_EXCEPTION -D_AIX_PTHREADS_D7
LOCAL_LDFLAGS = -brtl -bnoquiet

OS = HP-UX
LOCAL_CCFLAGS = -D_REENTRANT -DNATIVE_EXCEPTION
CXX_EXCEPTION =

OS = IRIX
LOCAL_CCFLAGS = -DNATIVE_EXCEPTION
CXX_EXCEPTION=

OS = SunOS


OS = Windows_NT
LINK_WITH = $(PPRDCCServices_LINK_WITH) ITMi
# PPRDCCServicesCORBA = CLIENT
# ECB0CORBA = CLIENT
SYS_LIBS = Wsock32.LIB advapi32.lib user32.lib
LOCAL_CCFLAGS = -DORBIX_DLL -DWIN32_LEAN_AND_MEAN -DWIN32 -DIT_EX_MACROS -D_WINDOWS_SOURCE -DNATIVE_EXCEPTION
CXX_EXCEPTION =
