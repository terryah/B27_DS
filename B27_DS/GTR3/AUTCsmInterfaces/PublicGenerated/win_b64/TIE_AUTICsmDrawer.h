#ifndef __TIE_AUTICsmDrawer
#define __TIE_AUTICsmDrawer

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "AUTICsmDrawer.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface AUTICsmDrawer */
#define declare_TIE_AUTICsmDrawer(classe) \
 \
 \
class TIEAUTICsmDrawer##classe : public AUTICsmDrawer \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(AUTICsmDrawer, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT GetBRep(CAT2DRep **oppBrep); \
      virtual HRESULT SetMode(const Mode iMode); \
      virtual HRESULT GetMode(Mode &oMode); \
      virtual HRESULT Save(const CATUnicodeString  &iFilePath,const CATUnicodeString &iFormat="CGM"); \
};



#define ENVTIEdeclare_AUTICsmDrawer(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT GetBRep(CAT2DRep **oppBrep); \
virtual HRESULT SetMode(const Mode iMode); \
virtual HRESULT GetMode(Mode &oMode); \
virtual HRESULT Save(const CATUnicodeString  &iFilePath,const CATUnicodeString &iFormat="CGM"); \


#define ENVTIEdefine_AUTICsmDrawer(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT  ENVTIEName::GetBRep(CAT2DRep **oppBrep) \
{ \
return (ENVTIECALL(AUTICsmDrawer,ENVTIETypeLetter,ENVTIELetter)GetBRep(oppBrep)); \
} \
HRESULT  ENVTIEName::SetMode(const Mode iMode) \
{ \
return (ENVTIECALL(AUTICsmDrawer,ENVTIETypeLetter,ENVTIELetter)SetMode(iMode)); \
} \
HRESULT  ENVTIEName::GetMode(Mode &oMode) \
{ \
return (ENVTIECALL(AUTICsmDrawer,ENVTIETypeLetter,ENVTIELetter)GetMode(oMode)); \
} \
HRESULT  ENVTIEName::Save(const CATUnicodeString  &iFilePath,const CATUnicodeString &iFormat) \
{ \
return (ENVTIECALL(AUTICsmDrawer,ENVTIETypeLetter,ENVTIELetter)Save(iFilePath,iFormat)); \
} \


/* Name of the TIE class */
#define class_TIE_AUTICsmDrawer(classe)    TIEAUTICsmDrawer##classe


/* Common methods inside a TIE */
#define common_TIE_AUTICsmDrawer(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(AUTICsmDrawer, classe) \
 \
 \
CATImplementTIEMethods(AUTICsmDrawer, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(AUTICsmDrawer, classe, 1) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(AUTICsmDrawer, classe) \
CATImplementCATBaseUnknownMethodsForTIE(AUTICsmDrawer, classe) \
 \
HRESULT  TIEAUTICsmDrawer##classe::GetBRep(CAT2DRep **oppBrep) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetBRep(oppBrep)); \
} \
HRESULT  TIEAUTICsmDrawer##classe::SetMode(const Mode iMode) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetMode(iMode)); \
} \
HRESULT  TIEAUTICsmDrawer##classe::GetMode(Mode &oMode) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetMode(oMode)); \
} \
HRESULT  TIEAUTICsmDrawer##classe::Save(const CATUnicodeString  &iFilePath,const CATUnicodeString &iFormat) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Save(iFilePath,iFormat)); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_AUTICsmDrawer(classe) \
 \
 \
declare_TIE_AUTICsmDrawer(classe) \
 \
 \
CATMetaClass * __stdcall TIEAUTICsmDrawer##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_AUTICsmDrawer,"AUTICsmDrawer",AUTICsmDrawer::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_AUTICsmDrawer(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(AUTICsmDrawer, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicAUTICsmDrawer##classe(classe::MetaObject(),AUTICsmDrawer::MetaObject(),(void *)CreateTIEAUTICsmDrawer##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_AUTICsmDrawer(classe) \
 \
 \
declare_TIE_AUTICsmDrawer(classe) \
 \
 \
CATMetaClass * __stdcall TIEAUTICsmDrawer##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_AUTICsmDrawer,"AUTICsmDrawer",AUTICsmDrawer::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_AUTICsmDrawer(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(AUTICsmDrawer, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicAUTICsmDrawer##classe(classe::MetaObject(),AUTICsmDrawer::MetaObject(),(void *)CreateTIEAUTICsmDrawer##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_AUTICsmDrawer(classe) TIE_AUTICsmDrawer(classe)
#else
#define BOA_AUTICsmDrawer(classe) CATImplementBOA(AUTICsmDrawer, classe)
#endif

#endif
