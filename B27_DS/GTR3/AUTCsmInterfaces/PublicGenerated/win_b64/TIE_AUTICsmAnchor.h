#ifndef __TIE_AUTICsmAnchor
#define __TIE_AUTICsmAnchor

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "AUTICsmAnchor.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface AUTICsmAnchor */
#define declare_TIE_AUTICsmAnchor(classe) \
 \
 \
class TIEAUTICsmAnchor##classe : public AUTICsmAnchor \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(AUTICsmAnchor, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT GetOrCreateLogicWorkspace(WorkspaceType iType, const CATUnicodeString &iBaseClass,const CATUnicodeString &iRootName="", AUTICsmWorkspace **oppWork=0); \
      virtual HRESULT GetLogicWorkspace(WorkspaceType iType,AUTICsmWorkspace **oppWork); \
      virtual HRESULT GetLogicWorkspaces(CATListValCATBaseUnknown_var &oList); \
      virtual HRESULT GetName(CATUnicodeString &oName); \
      virtual HRESULT SetName(const CATUnicodeString &iName); \
      virtual HRESULT NewChildAnchor(const CATUnicodeString &iName, AUTICsmAnchor_var &ospAnchor,CATBoolean iReuse=FALSE); \
      virtual HRESULT Duplicate(AUTICsmAnchor_var &ospNewAnchor); \
      virtual HRESULT GetFather(CATBaseUnknown_var &ospFather); \
};



#define ENVTIEdeclare_AUTICsmAnchor(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT GetOrCreateLogicWorkspace(WorkspaceType iType, const CATUnicodeString &iBaseClass,const CATUnicodeString &iRootName="", AUTICsmWorkspace **oppWork=0); \
virtual HRESULT GetLogicWorkspace(WorkspaceType iType,AUTICsmWorkspace **oppWork); \
virtual HRESULT GetLogicWorkspaces(CATListValCATBaseUnknown_var &oList); \
virtual HRESULT GetName(CATUnicodeString &oName); \
virtual HRESULT SetName(const CATUnicodeString &iName); \
virtual HRESULT NewChildAnchor(const CATUnicodeString &iName, AUTICsmAnchor_var &ospAnchor,CATBoolean iReuse=FALSE); \
virtual HRESULT Duplicate(AUTICsmAnchor_var &ospNewAnchor); \
virtual HRESULT GetFather(CATBaseUnknown_var &ospFather); \


#define ENVTIEdefine_AUTICsmAnchor(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT  ENVTIEName::GetOrCreateLogicWorkspace(WorkspaceType iType, const CATUnicodeString &iBaseClass,const CATUnicodeString &iRootName, AUTICsmWorkspace **oppWork) \
{ \
return (ENVTIECALL(AUTICsmAnchor,ENVTIETypeLetter,ENVTIELetter)GetOrCreateLogicWorkspace(iType,iBaseClass,iRootName,oppWork)); \
} \
HRESULT  ENVTIEName::GetLogicWorkspace(WorkspaceType iType,AUTICsmWorkspace **oppWork) \
{ \
return (ENVTIECALL(AUTICsmAnchor,ENVTIETypeLetter,ENVTIELetter)GetLogicWorkspace(iType,oppWork)); \
} \
HRESULT  ENVTIEName::GetLogicWorkspaces(CATListValCATBaseUnknown_var &oList) \
{ \
return (ENVTIECALL(AUTICsmAnchor,ENVTIETypeLetter,ENVTIELetter)GetLogicWorkspaces(oList)); \
} \
HRESULT  ENVTIEName::GetName(CATUnicodeString &oName) \
{ \
return (ENVTIECALL(AUTICsmAnchor,ENVTIETypeLetter,ENVTIELetter)GetName(oName)); \
} \
HRESULT  ENVTIEName::SetName(const CATUnicodeString &iName) \
{ \
return (ENVTIECALL(AUTICsmAnchor,ENVTIETypeLetter,ENVTIELetter)SetName(iName)); \
} \
HRESULT  ENVTIEName::NewChildAnchor(const CATUnicodeString &iName, AUTICsmAnchor_var &ospAnchor,CATBoolean iReuse) \
{ \
return (ENVTIECALL(AUTICsmAnchor,ENVTIETypeLetter,ENVTIELetter)NewChildAnchor(iName,ospAnchor,iReuse)); \
} \
HRESULT  ENVTIEName::Duplicate(AUTICsmAnchor_var &ospNewAnchor) \
{ \
return (ENVTIECALL(AUTICsmAnchor,ENVTIETypeLetter,ENVTIELetter)Duplicate(ospNewAnchor)); \
} \
HRESULT  ENVTIEName::GetFather(CATBaseUnknown_var &ospFather) \
{ \
return (ENVTIECALL(AUTICsmAnchor,ENVTIETypeLetter,ENVTIELetter)GetFather(ospFather)); \
} \


/* Name of the TIE class */
#define class_TIE_AUTICsmAnchor(classe)    TIEAUTICsmAnchor##classe


/* Common methods inside a TIE */
#define common_TIE_AUTICsmAnchor(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(AUTICsmAnchor, classe) \
 \
 \
CATImplementTIEMethods(AUTICsmAnchor, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(AUTICsmAnchor, classe, 1) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(AUTICsmAnchor, classe) \
CATImplementCATBaseUnknownMethodsForTIE(AUTICsmAnchor, classe) \
 \
HRESULT  TIEAUTICsmAnchor##classe::GetOrCreateLogicWorkspace(WorkspaceType iType, const CATUnicodeString &iBaseClass,const CATUnicodeString &iRootName, AUTICsmWorkspace **oppWork) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetOrCreateLogicWorkspace(iType,iBaseClass,iRootName,oppWork)); \
} \
HRESULT  TIEAUTICsmAnchor##classe::GetLogicWorkspace(WorkspaceType iType,AUTICsmWorkspace **oppWork) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetLogicWorkspace(iType,oppWork)); \
} \
HRESULT  TIEAUTICsmAnchor##classe::GetLogicWorkspaces(CATListValCATBaseUnknown_var &oList) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetLogicWorkspaces(oList)); \
} \
HRESULT  TIEAUTICsmAnchor##classe::GetName(CATUnicodeString &oName) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetName(oName)); \
} \
HRESULT  TIEAUTICsmAnchor##classe::SetName(const CATUnicodeString &iName) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetName(iName)); \
} \
HRESULT  TIEAUTICsmAnchor##classe::NewChildAnchor(const CATUnicodeString &iName, AUTICsmAnchor_var &ospAnchor,CATBoolean iReuse) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->NewChildAnchor(iName,ospAnchor,iReuse)); \
} \
HRESULT  TIEAUTICsmAnchor##classe::Duplicate(AUTICsmAnchor_var &ospNewAnchor) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Duplicate(ospNewAnchor)); \
} \
HRESULT  TIEAUTICsmAnchor##classe::GetFather(CATBaseUnknown_var &ospFather) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetFather(ospFather)); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_AUTICsmAnchor(classe) \
 \
 \
declare_TIE_AUTICsmAnchor(classe) \
 \
 \
CATMetaClass * __stdcall TIEAUTICsmAnchor##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_AUTICsmAnchor,"AUTICsmAnchor",AUTICsmAnchor::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_AUTICsmAnchor(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(AUTICsmAnchor, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicAUTICsmAnchor##classe(classe::MetaObject(),AUTICsmAnchor::MetaObject(),(void *)CreateTIEAUTICsmAnchor##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_AUTICsmAnchor(classe) \
 \
 \
declare_TIE_AUTICsmAnchor(classe) \
 \
 \
CATMetaClass * __stdcall TIEAUTICsmAnchor##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_AUTICsmAnchor,"AUTICsmAnchor",AUTICsmAnchor::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_AUTICsmAnchor(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(AUTICsmAnchor, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicAUTICsmAnchor##classe(classe::MetaObject(),AUTICsmAnchor::MetaObject(),(void *)CreateTIEAUTICsmAnchor##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_AUTICsmAnchor(classe) TIE_AUTICsmAnchor(classe)
#else
#define BOA_AUTICsmAnchor(classe) CATImplementBOA(AUTICsmAnchor, classe)
#endif

#endif
