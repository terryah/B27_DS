#ifndef __TIE_AUTICsmDirectCall
#define __TIE_AUTICsmDirectCall

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "AUTICsmDirectCall.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface AUTICsmDirectCall */
#define declare_TIE_AUTICsmDirectCall(classe) \
 \
 \
class TIEAUTICsmDirectCall##classe : public AUTICsmDirectCall \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(AUTICsmDirectCall, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT Send(const CATUnicodeString & iMess, const int iVal) ; \
      virtual HRESULT Send(const CATUnicodeString & iMess, const float iVal) ; \
      virtual HRESULT Send(const CATUnicodeString & iMess, const double iVal) ; \
      virtual HRESULT Send(const CATUnicodeString & iMess, const CATUnicodeString & iVal) ; \
};



#define ENVTIEdeclare_AUTICsmDirectCall(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT Send(const CATUnicodeString & iMess, const int iVal) ; \
virtual HRESULT Send(const CATUnicodeString & iMess, const float iVal) ; \
virtual HRESULT Send(const CATUnicodeString & iMess, const double iVal) ; \
virtual HRESULT Send(const CATUnicodeString & iMess, const CATUnicodeString & iVal) ; \


#define ENVTIEdefine_AUTICsmDirectCall(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT  ENVTIEName::Send(const CATUnicodeString & iMess, const int iVal)  \
{ \
return (ENVTIECALL(AUTICsmDirectCall,ENVTIETypeLetter,ENVTIELetter)Send(iMess,iVal)); \
} \
HRESULT  ENVTIEName::Send(const CATUnicodeString & iMess, const float iVal)  \
{ \
return (ENVTIECALL(AUTICsmDirectCall,ENVTIETypeLetter,ENVTIELetter)Send(iMess,iVal)); \
} \
HRESULT  ENVTIEName::Send(const CATUnicodeString & iMess, const double iVal)  \
{ \
return (ENVTIECALL(AUTICsmDirectCall,ENVTIETypeLetter,ENVTIELetter)Send(iMess,iVal)); \
} \
HRESULT  ENVTIEName::Send(const CATUnicodeString & iMess, const CATUnicodeString & iVal)  \
{ \
return (ENVTIECALL(AUTICsmDirectCall,ENVTIETypeLetter,ENVTIELetter)Send(iMess,iVal)); \
} \


/* Name of the TIE class */
#define class_TIE_AUTICsmDirectCall(classe)    TIEAUTICsmDirectCall##classe


/* Common methods inside a TIE */
#define common_TIE_AUTICsmDirectCall(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(AUTICsmDirectCall, classe) \
 \
 \
CATImplementTIEMethods(AUTICsmDirectCall, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(AUTICsmDirectCall, classe, 1) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(AUTICsmDirectCall, classe) \
CATImplementCATBaseUnknownMethodsForTIE(AUTICsmDirectCall, classe) \
 \
HRESULT  TIEAUTICsmDirectCall##classe::Send(const CATUnicodeString & iMess, const int iVal)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Send(iMess,iVal)); \
} \
HRESULT  TIEAUTICsmDirectCall##classe::Send(const CATUnicodeString & iMess, const float iVal)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Send(iMess,iVal)); \
} \
HRESULT  TIEAUTICsmDirectCall##classe::Send(const CATUnicodeString & iMess, const double iVal)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Send(iMess,iVal)); \
} \
HRESULT  TIEAUTICsmDirectCall##classe::Send(const CATUnicodeString & iMess, const CATUnicodeString & iVal)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Send(iMess,iVal)); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_AUTICsmDirectCall(classe) \
 \
 \
declare_TIE_AUTICsmDirectCall(classe) \
 \
 \
CATMetaClass * __stdcall TIEAUTICsmDirectCall##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_AUTICsmDirectCall,"AUTICsmDirectCall",AUTICsmDirectCall::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_AUTICsmDirectCall(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(AUTICsmDirectCall, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicAUTICsmDirectCall##classe(classe::MetaObject(),AUTICsmDirectCall::MetaObject(),(void *)CreateTIEAUTICsmDirectCall##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_AUTICsmDirectCall(classe) \
 \
 \
declare_TIE_AUTICsmDirectCall(classe) \
 \
 \
CATMetaClass * __stdcall TIEAUTICsmDirectCall##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_AUTICsmDirectCall,"AUTICsmDirectCall",AUTICsmDirectCall::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_AUTICsmDirectCall(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(AUTICsmDirectCall, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicAUTICsmDirectCall##classe(classe::MetaObject(),AUTICsmDirectCall::MetaObject(),(void *)CreateTIEAUTICsmDirectCall##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_AUTICsmDirectCall(classe) TIE_AUTICsmDirectCall(classe)
#else
#define BOA_AUTICsmDirectCall(classe) CATImplementBOA(AUTICsmDirectCall, classe)
#endif

#endif
