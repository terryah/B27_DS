#ifndef __TIE_AUTICsmExplorePage
#define __TIE_AUTICsmExplorePage

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "AUTICsmExplorePage.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface AUTICsmExplorePage */
#define declare_TIE_AUTICsmExplorePage(classe) \
 \
 \
class TIEAUTICsmExplorePage##classe : public AUTICsmExplorePage \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(AUTICsmExplorePage, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT Update(CATDlgTabPage *ipPage,CATBaseUnknown *ipObj); \
      virtual HRESULT Build(CATDlgTabContainer *ipContainer,CATListPtrCATBaseUnknown &oPages); \
      virtual HRESULT Close(); \
};



#define ENVTIEdeclare_AUTICsmExplorePage(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT Update(CATDlgTabPage *ipPage,CATBaseUnknown *ipObj); \
virtual HRESULT Build(CATDlgTabContainer *ipContainer,CATListPtrCATBaseUnknown &oPages); \
virtual HRESULT Close(); \


#define ENVTIEdefine_AUTICsmExplorePage(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT  ENVTIEName::Update(CATDlgTabPage *ipPage,CATBaseUnknown *ipObj) \
{ \
return (ENVTIECALL(AUTICsmExplorePage,ENVTIETypeLetter,ENVTIELetter)Update(ipPage,ipObj)); \
} \
HRESULT  ENVTIEName::Build(CATDlgTabContainer *ipContainer,CATListPtrCATBaseUnknown &oPages) \
{ \
return (ENVTIECALL(AUTICsmExplorePage,ENVTIETypeLetter,ENVTIELetter)Build(ipContainer,oPages)); \
} \
HRESULT  ENVTIEName::Close() \
{ \
return (ENVTIECALL(AUTICsmExplorePage,ENVTIETypeLetter,ENVTIELetter)Close()); \
} \


/* Name of the TIE class */
#define class_TIE_AUTICsmExplorePage(classe)    TIEAUTICsmExplorePage##classe


/* Common methods inside a TIE */
#define common_TIE_AUTICsmExplorePage(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(AUTICsmExplorePage, classe) \
 \
 \
CATImplementTIEMethods(AUTICsmExplorePage, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(AUTICsmExplorePage, classe, 1) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(AUTICsmExplorePage, classe) \
CATImplementCATBaseUnknownMethodsForTIE(AUTICsmExplorePage, classe) \
 \
HRESULT  TIEAUTICsmExplorePage##classe::Update(CATDlgTabPage *ipPage,CATBaseUnknown *ipObj) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Update(ipPage,ipObj)); \
} \
HRESULT  TIEAUTICsmExplorePage##classe::Build(CATDlgTabContainer *ipContainer,CATListPtrCATBaseUnknown &oPages) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Build(ipContainer,oPages)); \
} \
HRESULT  TIEAUTICsmExplorePage##classe::Close() \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Close()); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_AUTICsmExplorePage(classe) \
 \
 \
declare_TIE_AUTICsmExplorePage(classe) \
 \
 \
CATMetaClass * __stdcall TIEAUTICsmExplorePage##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_AUTICsmExplorePage,"AUTICsmExplorePage",AUTICsmExplorePage::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_AUTICsmExplorePage(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(AUTICsmExplorePage, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicAUTICsmExplorePage##classe(classe::MetaObject(),AUTICsmExplorePage::MetaObject(),(void *)CreateTIEAUTICsmExplorePage##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_AUTICsmExplorePage(classe) \
 \
 \
declare_TIE_AUTICsmExplorePage(classe) \
 \
 \
CATMetaClass * __stdcall TIEAUTICsmExplorePage##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_AUTICsmExplorePage,"AUTICsmExplorePage",AUTICsmExplorePage::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_AUTICsmExplorePage(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(AUTICsmExplorePage, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicAUTICsmExplorePage##classe(classe::MetaObject(),AUTICsmExplorePage::MetaObject(),(void *)CreateTIEAUTICsmExplorePage##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_AUTICsmExplorePage(classe) TIE_AUTICsmExplorePage(classe)
#else
#define BOA_AUTICsmExplorePage(classe) CATImplementBOA(AUTICsmExplorePage, classe)
#endif

#endif
