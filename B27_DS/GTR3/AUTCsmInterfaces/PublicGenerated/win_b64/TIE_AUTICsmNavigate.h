#ifndef __TIE_AUTICsmNavigate
#define __TIE_AUTICsmNavigate

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "AUTICsmNavigate.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface AUTICsmNavigate */
#define declare_TIE_AUTICsmNavigate(classe) \
 \
 \
class TIEAUTICsmNavigate##classe : public AUTICsmNavigate \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(AUTICsmNavigate, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT ModifyShortHelp(CATUnicodeString & ioText); \
      virtual HRESULT GetNodeBitmap (CATUnicodeString &oBmp); \
      virtual int GetNodeColor (); \
      virtual HRESULT GetChildren (CATListValCATBaseUnknown_var &oLChild); \
      virtual HRESULT GetIdentificator (CATUnicodeString &oId); \
};



#define ENVTIEdeclare_AUTICsmNavigate(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT ModifyShortHelp(CATUnicodeString & ioText); \
virtual HRESULT GetNodeBitmap (CATUnicodeString &oBmp); \
virtual int GetNodeColor (); \
virtual HRESULT GetChildren (CATListValCATBaseUnknown_var &oLChild); \
virtual HRESULT GetIdentificator (CATUnicodeString &oId); \


#define ENVTIEdefine_AUTICsmNavigate(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT  ENVTIEName::ModifyShortHelp(CATUnicodeString & ioText) \
{ \
return (ENVTIECALL(AUTICsmNavigate,ENVTIETypeLetter,ENVTIELetter)ModifyShortHelp(ioText)); \
} \
HRESULT  ENVTIEName::GetNodeBitmap (CATUnicodeString &oBmp) \
{ \
return (ENVTIECALL(AUTICsmNavigate,ENVTIETypeLetter,ENVTIELetter)GetNodeBitmap (oBmp)); \
} \
int  ENVTIEName::GetNodeColor () \
{ \
return (ENVTIECALL(AUTICsmNavigate,ENVTIETypeLetter,ENVTIELetter)GetNodeColor ()); \
} \
HRESULT  ENVTIEName::GetChildren (CATListValCATBaseUnknown_var &oLChild) \
{ \
return (ENVTIECALL(AUTICsmNavigate,ENVTIETypeLetter,ENVTIELetter)GetChildren (oLChild)); \
} \
HRESULT  ENVTIEName::GetIdentificator (CATUnicodeString &oId) \
{ \
return (ENVTIECALL(AUTICsmNavigate,ENVTIETypeLetter,ENVTIELetter)GetIdentificator (oId)); \
} \


/* Name of the TIE class */
#define class_TIE_AUTICsmNavigate(classe)    TIEAUTICsmNavigate##classe


/* Common methods inside a TIE */
#define common_TIE_AUTICsmNavigate(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(AUTICsmNavigate, classe) \
 \
 \
CATImplementTIEMethods(AUTICsmNavigate, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(AUTICsmNavigate, classe, 1) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(AUTICsmNavigate, classe) \
CATImplementCATBaseUnknownMethodsForTIE(AUTICsmNavigate, classe) \
 \
HRESULT  TIEAUTICsmNavigate##classe::ModifyShortHelp(CATUnicodeString & ioText) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ModifyShortHelp(ioText)); \
} \
HRESULT  TIEAUTICsmNavigate##classe::GetNodeBitmap (CATUnicodeString &oBmp) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetNodeBitmap (oBmp)); \
} \
int  TIEAUTICsmNavigate##classe::GetNodeColor () \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetNodeColor ()); \
} \
HRESULT  TIEAUTICsmNavigate##classe::GetChildren (CATListValCATBaseUnknown_var &oLChild) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetChildren (oLChild)); \
} \
HRESULT  TIEAUTICsmNavigate##classe::GetIdentificator (CATUnicodeString &oId) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetIdentificator (oId)); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_AUTICsmNavigate(classe) \
 \
 \
declare_TIE_AUTICsmNavigate(classe) \
 \
 \
CATMetaClass * __stdcall TIEAUTICsmNavigate##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_AUTICsmNavigate,"AUTICsmNavigate",AUTICsmNavigate::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_AUTICsmNavigate(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(AUTICsmNavigate, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicAUTICsmNavigate##classe(classe::MetaObject(),AUTICsmNavigate::MetaObject(),(void *)CreateTIEAUTICsmNavigate##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_AUTICsmNavigate(classe) \
 \
 \
declare_TIE_AUTICsmNavigate(classe) \
 \
 \
CATMetaClass * __stdcall TIEAUTICsmNavigate##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_AUTICsmNavigate,"AUTICsmNavigate",AUTICsmNavigate::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_AUTICsmNavigate(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(AUTICsmNavigate, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicAUTICsmNavigate##classe(classe::MetaObject(),AUTICsmNavigate::MetaObject(),(void *)CreateTIEAUTICsmNavigate##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_AUTICsmNavigate(classe) TIE_AUTICsmNavigate(classe)
#else
#define BOA_AUTICsmNavigate(classe) CATImplementBOA(AUTICsmNavigate, classe)
#endif

#endif
