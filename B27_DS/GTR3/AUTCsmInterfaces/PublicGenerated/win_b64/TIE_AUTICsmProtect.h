#ifndef __TIE_AUTICsmProtect
#define __TIE_AUTICsmProtect

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "AUTICsmProtect.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface AUTICsmProtect */
#define declare_TIE_AUTICsmProtect(classe) \
 \
 \
class TIEAUTICsmProtect##classe : public AUTICsmProtect \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(AUTICsmProtect, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual  CATBoolean Remove(void); \
      virtual  CATBoolean RemoveInstance(CATBaseUnknown *ipInst); \
      virtual  void BeforeUndo(void); \
      virtual  void BeforeRedo(void); \
      virtual  void AfterUndo(void); \
      virtual  void AfterRedo(void); \
      virtual  void BeforeRemove(void); \
      virtual  void BeforeAdd(void); \
      virtual  void AfterAdd(void); \
      virtual  void BeforeCopy(void); \
      virtual  void AfterCopy(void); \
      virtual CATBoolean CouldBeRemoved(void ); \
      virtual CATBoolean CouldAddChild(void ); \
      virtual HRESULT GetPasswd(CATUnicodeString &oPasswd ); \
      virtual void AfterLoadReference(AUTICsmWorkspace *ipExMod); \
      virtual void AfterUpdate(); \
      virtual CATBoolean IsEditable(); \
      virtual CATBoolean AcceptDocumentForLink(const CATUnicodeString &iDocPath,CATUnicodeString &oReason); \
      virtual HRESULT SetDocumentForLink(const CATUnicodeString &iDocPath); \
};



#define ENVTIEdeclare_AUTICsmProtect(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual  CATBoolean Remove(void); \
virtual  CATBoolean RemoveInstance(CATBaseUnknown *ipInst); \
virtual  void BeforeUndo(void); \
virtual  void BeforeRedo(void); \
virtual  void AfterUndo(void); \
virtual  void AfterRedo(void); \
virtual  void BeforeRemove(void); \
virtual  void BeforeAdd(void); \
virtual  void AfterAdd(void); \
virtual  void BeforeCopy(void); \
virtual  void AfterCopy(void); \
virtual CATBoolean CouldBeRemoved(void ); \
virtual CATBoolean CouldAddChild(void ); \
virtual HRESULT GetPasswd(CATUnicodeString &oPasswd ); \
virtual void AfterLoadReference(AUTICsmWorkspace *ipExMod); \
virtual void AfterUpdate(); \
virtual CATBoolean IsEditable(); \
virtual CATBoolean AcceptDocumentForLink(const CATUnicodeString &iDocPath,CATUnicodeString &oReason); \
virtual HRESULT SetDocumentForLink(const CATUnicodeString &iDocPath); \


#define ENVTIEdefine_AUTICsmProtect(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
CATBoolean  ENVTIEName::Remove(void) \
{ \
return (ENVTIECALL(AUTICsmProtect,ENVTIETypeLetter,ENVTIELetter)Remove()); \
} \
CATBoolean  ENVTIEName::RemoveInstance(CATBaseUnknown *ipInst) \
{ \
return (ENVTIECALL(AUTICsmProtect,ENVTIETypeLetter,ENVTIELetter)RemoveInstance(ipInst)); \
} \
void  ENVTIEName::BeforeUndo(void) \
{ \
 (ENVTIECALL(AUTICsmProtect,ENVTIETypeLetter,ENVTIELetter)BeforeUndo()); \
} \
void  ENVTIEName::BeforeRedo(void) \
{ \
 (ENVTIECALL(AUTICsmProtect,ENVTIETypeLetter,ENVTIELetter)BeforeRedo()); \
} \
void  ENVTIEName::AfterUndo(void) \
{ \
 (ENVTIECALL(AUTICsmProtect,ENVTIETypeLetter,ENVTIELetter)AfterUndo()); \
} \
void  ENVTIEName::AfterRedo(void) \
{ \
 (ENVTIECALL(AUTICsmProtect,ENVTIETypeLetter,ENVTIELetter)AfterRedo()); \
} \
void  ENVTIEName::BeforeRemove(void) \
{ \
 (ENVTIECALL(AUTICsmProtect,ENVTIETypeLetter,ENVTIELetter)BeforeRemove()); \
} \
void  ENVTIEName::BeforeAdd(void) \
{ \
 (ENVTIECALL(AUTICsmProtect,ENVTIETypeLetter,ENVTIELetter)BeforeAdd()); \
} \
void  ENVTIEName::AfterAdd(void) \
{ \
 (ENVTIECALL(AUTICsmProtect,ENVTIETypeLetter,ENVTIELetter)AfterAdd()); \
} \
void  ENVTIEName::BeforeCopy(void) \
{ \
 (ENVTIECALL(AUTICsmProtect,ENVTIETypeLetter,ENVTIELetter)BeforeCopy()); \
} \
void  ENVTIEName::AfterCopy(void) \
{ \
 (ENVTIECALL(AUTICsmProtect,ENVTIETypeLetter,ENVTIELetter)AfterCopy()); \
} \
CATBoolean  ENVTIEName::CouldBeRemoved(void ) \
{ \
return (ENVTIECALL(AUTICsmProtect,ENVTIETypeLetter,ENVTIELetter)CouldBeRemoved()); \
} \
CATBoolean  ENVTIEName::CouldAddChild(void ) \
{ \
return (ENVTIECALL(AUTICsmProtect,ENVTIETypeLetter,ENVTIELetter)CouldAddChild()); \
} \
HRESULT  ENVTIEName::GetPasswd(CATUnicodeString &oPasswd ) \
{ \
return (ENVTIECALL(AUTICsmProtect,ENVTIETypeLetter,ENVTIELetter)GetPasswd(oPasswd )); \
} \
void  ENVTIEName::AfterLoadReference(AUTICsmWorkspace *ipExMod) \
{ \
 (ENVTIECALL(AUTICsmProtect,ENVTIETypeLetter,ENVTIELetter)AfterLoadReference(ipExMod)); \
} \
void  ENVTIEName::AfterUpdate() \
{ \
 (ENVTIECALL(AUTICsmProtect,ENVTIETypeLetter,ENVTIELetter)AfterUpdate()); \
} \
CATBoolean  ENVTIEName::IsEditable() \
{ \
return (ENVTIECALL(AUTICsmProtect,ENVTIETypeLetter,ENVTIELetter)IsEditable()); \
} \
CATBoolean  ENVTIEName::AcceptDocumentForLink(const CATUnicodeString &iDocPath,CATUnicodeString &oReason) \
{ \
return (ENVTIECALL(AUTICsmProtect,ENVTIETypeLetter,ENVTIELetter)AcceptDocumentForLink(iDocPath,oReason)); \
} \
HRESULT  ENVTIEName::SetDocumentForLink(const CATUnicodeString &iDocPath) \
{ \
return (ENVTIECALL(AUTICsmProtect,ENVTIETypeLetter,ENVTIELetter)SetDocumentForLink(iDocPath)); \
} \


/* Name of the TIE class */
#define class_TIE_AUTICsmProtect(classe)    TIEAUTICsmProtect##classe


/* Common methods inside a TIE */
#define common_TIE_AUTICsmProtect(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(AUTICsmProtect, classe) \
 \
 \
CATImplementTIEMethods(AUTICsmProtect, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(AUTICsmProtect, classe, 1) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(AUTICsmProtect, classe) \
CATImplementCATBaseUnknownMethodsForTIE(AUTICsmProtect, classe) \
 \
CATBoolean  TIEAUTICsmProtect##classe::Remove(void) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Remove()); \
} \
CATBoolean  TIEAUTICsmProtect##classe::RemoveInstance(CATBaseUnknown *ipInst) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->RemoveInstance(ipInst)); \
} \
void  TIEAUTICsmProtect##classe::BeforeUndo(void) \
{ \
   ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->BeforeUndo(); \
} \
void  TIEAUTICsmProtect##classe::BeforeRedo(void) \
{ \
   ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->BeforeRedo(); \
} \
void  TIEAUTICsmProtect##classe::AfterUndo(void) \
{ \
   ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->AfterUndo(); \
} \
void  TIEAUTICsmProtect##classe::AfterRedo(void) \
{ \
   ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->AfterRedo(); \
} \
void  TIEAUTICsmProtect##classe::BeforeRemove(void) \
{ \
   ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->BeforeRemove(); \
} \
void  TIEAUTICsmProtect##classe::BeforeAdd(void) \
{ \
   ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->BeforeAdd(); \
} \
void  TIEAUTICsmProtect##classe::AfterAdd(void) \
{ \
   ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->AfterAdd(); \
} \
void  TIEAUTICsmProtect##classe::BeforeCopy(void) \
{ \
   ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->BeforeCopy(); \
} \
void  TIEAUTICsmProtect##classe::AfterCopy(void) \
{ \
   ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->AfterCopy(); \
} \
CATBoolean  TIEAUTICsmProtect##classe::CouldBeRemoved(void ) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CouldBeRemoved()); \
} \
CATBoolean  TIEAUTICsmProtect##classe::CouldAddChild(void ) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CouldAddChild()); \
} \
HRESULT  TIEAUTICsmProtect##classe::GetPasswd(CATUnicodeString &oPasswd ) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetPasswd(oPasswd )); \
} \
void  TIEAUTICsmProtect##classe::AfterLoadReference(AUTICsmWorkspace *ipExMod) \
{ \
   ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->AfterLoadReference(ipExMod); \
} \
void  TIEAUTICsmProtect##classe::AfterUpdate() \
{ \
   ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->AfterUpdate(); \
} \
CATBoolean  TIEAUTICsmProtect##classe::IsEditable() \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->IsEditable()); \
} \
CATBoolean  TIEAUTICsmProtect##classe::AcceptDocumentForLink(const CATUnicodeString &iDocPath,CATUnicodeString &oReason) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->AcceptDocumentForLink(iDocPath,oReason)); \
} \
HRESULT  TIEAUTICsmProtect##classe::SetDocumentForLink(const CATUnicodeString &iDocPath) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetDocumentForLink(iDocPath)); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_AUTICsmProtect(classe) \
 \
 \
declare_TIE_AUTICsmProtect(classe) \
 \
 \
CATMetaClass * __stdcall TIEAUTICsmProtect##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_AUTICsmProtect,"AUTICsmProtect",AUTICsmProtect::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_AUTICsmProtect(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(AUTICsmProtect, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicAUTICsmProtect##classe(classe::MetaObject(),AUTICsmProtect::MetaObject(),(void *)CreateTIEAUTICsmProtect##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_AUTICsmProtect(classe) \
 \
 \
declare_TIE_AUTICsmProtect(classe) \
 \
 \
CATMetaClass * __stdcall TIEAUTICsmProtect##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_AUTICsmProtect,"AUTICsmProtect",AUTICsmProtect::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_AUTICsmProtect(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(AUTICsmProtect, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicAUTICsmProtect##classe(classe::MetaObject(),AUTICsmProtect::MetaObject(),(void *)CreateTIEAUTICsmProtect##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_AUTICsmProtect(classe) TIE_AUTICsmProtect(classe)
#else
#define BOA_AUTICsmProtect(classe) CATImplementBOA(AUTICsmProtect, classe)
#endif

#endif
