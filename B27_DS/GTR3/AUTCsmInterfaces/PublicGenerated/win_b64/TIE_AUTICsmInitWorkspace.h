#ifndef __TIE_AUTICsmInitWorkspace
#define __TIE_AUTICsmInitWorkspace

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "AUTICsmInitWorkspace.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface AUTICsmInitWorkspace */
#define declare_TIE_AUTICsmInitWorkspace(classe) \
 \
 \
class TIEAUTICsmInitWorkspace##classe : public AUTICsmInitWorkspace \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(AUTICsmInitWorkspace, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT GetRootModuleLateType(CATUnicodeString &olate); \
      virtual HRESULT InitRootModule(CATBaseUnknown *root,CATBoolean iCreate); \
      virtual HRESULT Init(AUTICsmWorkspace *ipWork); \
      virtual HRESULT ModifyShortHelp(CATBaseUnknown *root,CATUnicodeString & ioText); \
      virtual HRESULT GetNodeBitmap (CATBaseUnknown *root,CATUnicodeString &oBmp); \
      virtual int GetNodeColor (CATBaseUnknown *root); \
      virtual HRESULT GetChildren (CATBaseUnknown *root,CATListValCATBaseUnknown_var &oLChild); \
      virtual HRESULT GetIdentificator (CATBaseUnknown *root,CATUnicodeString &oId); \
      virtual HRESULT AddType(const CATClassId type,const CATClassId supertype,const AUTCsmRights iRights=((1 << 6)|(1 << 4)|(1 << 11)|(1 << 9))); \
      virtual HRESULT AddKey(const CATUnicodeString &iKey,CATClassId type,const AUTCsmRights iRight=((1 << 1)|(1 << 3))); \
      virtual HRESULT GetRep(AUTICsmWorkspace *ipWork,CAT2DRep **oRep); \
      virtual HRESULT GetWorkspaceRootsForNav(CATBaseUnknown *ipWork,CATListValCATBaseUnknown_var &oListe); \
};



#define ENVTIEdeclare_AUTICsmInitWorkspace(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT GetRootModuleLateType(CATUnicodeString &olate); \
virtual HRESULT InitRootModule(CATBaseUnknown *root,CATBoolean iCreate); \
virtual HRESULT Init(AUTICsmWorkspace *ipWork); \
virtual HRESULT ModifyShortHelp(CATBaseUnknown *root,CATUnicodeString & ioText); \
virtual HRESULT GetNodeBitmap (CATBaseUnknown *root,CATUnicodeString &oBmp); \
virtual int GetNodeColor (CATBaseUnknown *root); \
virtual HRESULT GetChildren (CATBaseUnknown *root,CATListValCATBaseUnknown_var &oLChild); \
virtual HRESULT GetIdentificator (CATBaseUnknown *root,CATUnicodeString &oId); \
virtual HRESULT AddType(const CATClassId type,const CATClassId supertype,const AUTCsmRights iRights=((1 << 6)|(1 << 4)|(1 << 11)|(1 << 9))); \
virtual HRESULT AddKey(const CATUnicodeString &iKey,CATClassId type,const AUTCsmRights iRight=((1 << 1)|(1 << 3))); \
virtual HRESULT GetRep(AUTICsmWorkspace *ipWork,CAT2DRep **oRep); \
virtual HRESULT GetWorkspaceRootsForNav(CATBaseUnknown *ipWork,CATListValCATBaseUnknown_var &oListe); \


#define ENVTIEdefine_AUTICsmInitWorkspace(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT  ENVTIEName::GetRootModuleLateType(CATUnicodeString &olate) \
{ \
return (ENVTIECALL(AUTICsmInitWorkspace,ENVTIETypeLetter,ENVTIELetter)GetRootModuleLateType(olate)); \
} \
HRESULT  ENVTIEName::InitRootModule(CATBaseUnknown *root,CATBoolean iCreate) \
{ \
return (ENVTIECALL(AUTICsmInitWorkspace,ENVTIETypeLetter,ENVTIELetter)InitRootModule(root,iCreate)); \
} \
HRESULT  ENVTIEName::Init(AUTICsmWorkspace *ipWork) \
{ \
return (ENVTIECALL(AUTICsmInitWorkspace,ENVTIETypeLetter,ENVTIELetter)Init(ipWork)); \
} \
HRESULT  ENVTIEName::ModifyShortHelp(CATBaseUnknown *root,CATUnicodeString & ioText) \
{ \
return (ENVTIECALL(AUTICsmInitWorkspace,ENVTIETypeLetter,ENVTIELetter)ModifyShortHelp(root,ioText)); \
} \
HRESULT  ENVTIEName::GetNodeBitmap (CATBaseUnknown *root,CATUnicodeString &oBmp) \
{ \
return (ENVTIECALL(AUTICsmInitWorkspace,ENVTIETypeLetter,ENVTIELetter)GetNodeBitmap (root,oBmp)); \
} \
int  ENVTIEName::GetNodeColor (CATBaseUnknown *root) \
{ \
return (ENVTIECALL(AUTICsmInitWorkspace,ENVTIETypeLetter,ENVTIELetter)GetNodeColor (root)); \
} \
HRESULT  ENVTIEName::GetChildren (CATBaseUnknown *root,CATListValCATBaseUnknown_var &oLChild) \
{ \
return (ENVTIECALL(AUTICsmInitWorkspace,ENVTIETypeLetter,ENVTIELetter)GetChildren (root,oLChild)); \
} \
HRESULT  ENVTIEName::GetIdentificator (CATBaseUnknown *root,CATUnicodeString &oId) \
{ \
return (ENVTIECALL(AUTICsmInitWorkspace,ENVTIETypeLetter,ENVTIELetter)GetIdentificator (root,oId)); \
} \
HRESULT  ENVTIEName::AddType(const CATClassId type,const CATClassId supertype,const AUTCsmRights iRights) \
{ \
return (ENVTIECALL(AUTICsmInitWorkspace,ENVTIETypeLetter,ENVTIELetter)AddType(type,supertype,iRights)); \
} \
HRESULT  ENVTIEName::AddKey(const CATUnicodeString &iKey,CATClassId type,const AUTCsmRights iRight) \
{ \
return (ENVTIECALL(AUTICsmInitWorkspace,ENVTIETypeLetter,ENVTIELetter)AddKey(iKey,type,iRight)); \
} \
HRESULT  ENVTIEName::GetRep(AUTICsmWorkspace *ipWork,CAT2DRep **oRep) \
{ \
return (ENVTIECALL(AUTICsmInitWorkspace,ENVTIETypeLetter,ENVTIELetter)GetRep(ipWork,oRep)); \
} \
HRESULT  ENVTIEName::GetWorkspaceRootsForNav(CATBaseUnknown *ipWork,CATListValCATBaseUnknown_var &oListe) \
{ \
return (ENVTIECALL(AUTICsmInitWorkspace,ENVTIETypeLetter,ENVTIELetter)GetWorkspaceRootsForNav(ipWork,oListe)); \
} \


/* Name of the TIE class */
#define class_TIE_AUTICsmInitWorkspace(classe)    TIEAUTICsmInitWorkspace##classe


/* Common methods inside a TIE */
#define common_TIE_AUTICsmInitWorkspace(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(AUTICsmInitWorkspace, classe) \
 \
 \
CATImplementTIEMethods(AUTICsmInitWorkspace, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(AUTICsmInitWorkspace, classe, 1) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(AUTICsmInitWorkspace, classe) \
CATImplementCATBaseUnknownMethodsForTIE(AUTICsmInitWorkspace, classe) \
 \
HRESULT  TIEAUTICsmInitWorkspace##classe::GetRootModuleLateType(CATUnicodeString &olate) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetRootModuleLateType(olate)); \
} \
HRESULT  TIEAUTICsmInitWorkspace##classe::InitRootModule(CATBaseUnknown *root,CATBoolean iCreate) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->InitRootModule(root,iCreate)); \
} \
HRESULT  TIEAUTICsmInitWorkspace##classe::Init(AUTICsmWorkspace *ipWork) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Init(ipWork)); \
} \
HRESULT  TIEAUTICsmInitWorkspace##classe::ModifyShortHelp(CATBaseUnknown *root,CATUnicodeString & ioText) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ModifyShortHelp(root,ioText)); \
} \
HRESULT  TIEAUTICsmInitWorkspace##classe::GetNodeBitmap (CATBaseUnknown *root,CATUnicodeString &oBmp) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetNodeBitmap (root,oBmp)); \
} \
int  TIEAUTICsmInitWorkspace##classe::GetNodeColor (CATBaseUnknown *root) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetNodeColor (root)); \
} \
HRESULT  TIEAUTICsmInitWorkspace##classe::GetChildren (CATBaseUnknown *root,CATListValCATBaseUnknown_var &oLChild) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetChildren (root,oLChild)); \
} \
HRESULT  TIEAUTICsmInitWorkspace##classe::GetIdentificator (CATBaseUnknown *root,CATUnicodeString &oId) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetIdentificator (root,oId)); \
} \
HRESULT  TIEAUTICsmInitWorkspace##classe::AddType(const CATClassId type,const CATClassId supertype,const AUTCsmRights iRights) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->AddType(type,supertype,iRights)); \
} \
HRESULT  TIEAUTICsmInitWorkspace##classe::AddKey(const CATUnicodeString &iKey,CATClassId type,const AUTCsmRights iRight) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->AddKey(iKey,type,iRight)); \
} \
HRESULT  TIEAUTICsmInitWorkspace##classe::GetRep(AUTICsmWorkspace *ipWork,CAT2DRep **oRep) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetRep(ipWork,oRep)); \
} \
HRESULT  TIEAUTICsmInitWorkspace##classe::GetWorkspaceRootsForNav(CATBaseUnknown *ipWork,CATListValCATBaseUnknown_var &oListe) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetWorkspaceRootsForNav(ipWork,oListe)); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_AUTICsmInitWorkspace(classe) \
 \
 \
declare_TIE_AUTICsmInitWorkspace(classe) \
 \
 \
CATMetaClass * __stdcall TIEAUTICsmInitWorkspace##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_AUTICsmInitWorkspace,"AUTICsmInitWorkspace",AUTICsmInitWorkspace::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_AUTICsmInitWorkspace(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(AUTICsmInitWorkspace, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicAUTICsmInitWorkspace##classe(classe::MetaObject(),AUTICsmInitWorkspace::MetaObject(),(void *)CreateTIEAUTICsmInitWorkspace##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_AUTICsmInitWorkspace(classe) \
 \
 \
declare_TIE_AUTICsmInitWorkspace(classe) \
 \
 \
CATMetaClass * __stdcall TIEAUTICsmInitWorkspace##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_AUTICsmInitWorkspace,"AUTICsmInitWorkspace",AUTICsmInitWorkspace::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_AUTICsmInitWorkspace(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(AUTICsmInitWorkspace, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicAUTICsmInitWorkspace##classe(classe::MetaObject(),AUTICsmInitWorkspace::MetaObject(),(void *)CreateTIEAUTICsmInitWorkspace##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_AUTICsmInitWorkspace(classe) TIE_AUTICsmInitWorkspace(classe)
#else
#define BOA_AUTICsmInitWorkspace(classe) CATImplementBOA(AUTICsmInitWorkspace, classe)
#endif

#endif
