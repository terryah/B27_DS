#ifndef __TIE_AUTICsmFilter
#define __TIE_AUTICsmFilter

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "AUTICsmFilter.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface AUTICsmFilter */
#define declare_TIE_AUTICsmFilter(classe) \
 \
 \
class TIEAUTICsmFilter##classe : public AUTICsmFilter \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(AUTICsmFilter, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual CATBoolean IsOk() ; \
      virtual CATBoolean OkPath(const CATListOfCATUnicodeString & iIdPath) ; \
      virtual CATBoolean UseAlias() ; \
};



#define ENVTIEdeclare_AUTICsmFilter(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual CATBoolean IsOk() ; \
virtual CATBoolean OkPath(const CATListOfCATUnicodeString & iIdPath) ; \
virtual CATBoolean UseAlias() ; \


#define ENVTIEdefine_AUTICsmFilter(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
CATBoolean  ENVTIEName::IsOk()  \
{ \
return (ENVTIECALL(AUTICsmFilter,ENVTIETypeLetter,ENVTIELetter)IsOk()); \
} \
CATBoolean  ENVTIEName::OkPath(const CATListOfCATUnicodeString & iIdPath)  \
{ \
return (ENVTIECALL(AUTICsmFilter,ENVTIETypeLetter,ENVTIELetter)OkPath(iIdPath)); \
} \
CATBoolean  ENVTIEName::UseAlias()  \
{ \
return (ENVTIECALL(AUTICsmFilter,ENVTIETypeLetter,ENVTIELetter)UseAlias()); \
} \


/* Name of the TIE class */
#define class_TIE_AUTICsmFilter(classe)    TIEAUTICsmFilter##classe


/* Common methods inside a TIE */
#define common_TIE_AUTICsmFilter(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(AUTICsmFilter, classe) \
 \
 \
CATImplementTIEMethods(AUTICsmFilter, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(AUTICsmFilter, classe, 1) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(AUTICsmFilter, classe) \
CATImplementCATBaseUnknownMethodsForTIE(AUTICsmFilter, classe) \
 \
CATBoolean  TIEAUTICsmFilter##classe::IsOk()  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->IsOk()); \
} \
CATBoolean  TIEAUTICsmFilter##classe::OkPath(const CATListOfCATUnicodeString & iIdPath)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->OkPath(iIdPath)); \
} \
CATBoolean  TIEAUTICsmFilter##classe::UseAlias()  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->UseAlias()); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_AUTICsmFilter(classe) \
 \
 \
declare_TIE_AUTICsmFilter(classe) \
 \
 \
CATMetaClass * __stdcall TIEAUTICsmFilter##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_AUTICsmFilter,"AUTICsmFilter",AUTICsmFilter::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_AUTICsmFilter(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(AUTICsmFilter, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicAUTICsmFilter##classe(classe::MetaObject(),AUTICsmFilter::MetaObject(),(void *)CreateTIEAUTICsmFilter##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_AUTICsmFilter(classe) \
 \
 \
declare_TIE_AUTICsmFilter(classe) \
 \
 \
CATMetaClass * __stdcall TIEAUTICsmFilter##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_AUTICsmFilter,"AUTICsmFilter",AUTICsmFilter::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_AUTICsmFilter(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(AUTICsmFilter, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicAUTICsmFilter##classe(classe::MetaObject(),AUTICsmFilter::MetaObject(),(void *)CreateTIEAUTICsmFilter##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_AUTICsmFilter(classe) TIE_AUTICsmFilter(classe)
#else
#define BOA_AUTICsmFilter(classe) CATImplementBOA(AUTICsmFilter, classe)
#endif

#endif
