#ifndef __TIE_AUTICsmISNavigate
#define __TIE_AUTICsmISNavigate

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "AUTICsmISNavigate.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface AUTICsmISNavigate */
#define declare_TIE_AUTICsmISNavigate(classe) \
 \
 \
class TIEAUTICsmISNavigate##classe : public AUTICsmISNavigate \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(AUTICsmISNavigate, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT ModifyShortHelp(CATUnicodeString & ioText,CATBoolean iForEdit=FALSE); \
      virtual HRESULT GetNodeBitmap (CATUnicodeString &oBmp); \
      virtual HRESULT ResetNodeBitmap (); \
      virtual HRESULT GetNodeBitmapFirst (CATUnicodeString &oBmp); \
      virtual HRESULT SetNodeBitmap (const CATUnicodeString &iBmp); \
      virtual int GetNodeColor (); \
      virtual HRESULT GetChildren (CATListValCATBaseUnknown_var &oLChild); \
      virtual HRESULT GetChildrenOfList(void *data,CATListValCATBaseUnknown_var &oLChild); \
      virtual HRESULT GetChildren1 (CATListValCATBaseUnknown_var &oLChild); \
      virtual HRESULT GetIdentificator (CATUnicodeString &oId); \
      virtual HRESULT SetVisible(CATBoolean iSee); \
      virtual CATBoolean IsVisible(CATBoolean withoutopt=FALSE); \
      virtual void ForceRedraw(); \
};



#define ENVTIEdeclare_AUTICsmISNavigate(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT ModifyShortHelp(CATUnicodeString & ioText,CATBoolean iForEdit=FALSE); \
virtual HRESULT GetNodeBitmap (CATUnicodeString &oBmp); \
virtual HRESULT ResetNodeBitmap (); \
virtual HRESULT GetNodeBitmapFirst (CATUnicodeString &oBmp); \
virtual HRESULT SetNodeBitmap (const CATUnicodeString &iBmp); \
virtual int GetNodeColor (); \
virtual HRESULT GetChildren (CATListValCATBaseUnknown_var &oLChild); \
virtual HRESULT GetChildrenOfList(void *data,CATListValCATBaseUnknown_var &oLChild); \
virtual HRESULT GetChildren1 (CATListValCATBaseUnknown_var &oLChild); \
virtual HRESULT GetIdentificator (CATUnicodeString &oId); \
virtual HRESULT SetVisible(CATBoolean iSee); \
virtual CATBoolean IsVisible(CATBoolean withoutopt=FALSE); \
virtual void ForceRedraw(); \


#define ENVTIEdefine_AUTICsmISNavigate(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT  ENVTIEName::ModifyShortHelp(CATUnicodeString & ioText,CATBoolean iForEdit) \
{ \
return (ENVTIECALL(AUTICsmISNavigate,ENVTIETypeLetter,ENVTIELetter)ModifyShortHelp(ioText,iForEdit)); \
} \
HRESULT  ENVTIEName::GetNodeBitmap (CATUnicodeString &oBmp) \
{ \
return (ENVTIECALL(AUTICsmISNavigate,ENVTIETypeLetter,ENVTIELetter)GetNodeBitmap (oBmp)); \
} \
HRESULT  ENVTIEName::ResetNodeBitmap () \
{ \
return (ENVTIECALL(AUTICsmISNavigate,ENVTIETypeLetter,ENVTIELetter)ResetNodeBitmap ()); \
} \
HRESULT  ENVTIEName::GetNodeBitmapFirst (CATUnicodeString &oBmp) \
{ \
return (ENVTIECALL(AUTICsmISNavigate,ENVTIETypeLetter,ENVTIELetter)GetNodeBitmapFirst (oBmp)); \
} \
HRESULT  ENVTIEName::SetNodeBitmap (const CATUnicodeString &iBmp) \
{ \
return (ENVTIECALL(AUTICsmISNavigate,ENVTIETypeLetter,ENVTIELetter)SetNodeBitmap (iBmp)); \
} \
int  ENVTIEName::GetNodeColor () \
{ \
return (ENVTIECALL(AUTICsmISNavigate,ENVTIETypeLetter,ENVTIELetter)GetNodeColor ()); \
} \
HRESULT  ENVTIEName::GetChildren (CATListValCATBaseUnknown_var &oLChild) \
{ \
return (ENVTIECALL(AUTICsmISNavigate,ENVTIETypeLetter,ENVTIELetter)GetChildren (oLChild)); \
} \
HRESULT  ENVTIEName::GetChildrenOfList(void *data,CATListValCATBaseUnknown_var &oLChild) \
{ \
return (ENVTIECALL(AUTICsmISNavigate,ENVTIETypeLetter,ENVTIELetter)GetChildrenOfList(data,oLChild)); \
} \
HRESULT  ENVTIEName::GetChildren1 (CATListValCATBaseUnknown_var &oLChild) \
{ \
return (ENVTIECALL(AUTICsmISNavigate,ENVTIETypeLetter,ENVTIELetter)GetChildren1 (oLChild)); \
} \
HRESULT  ENVTIEName::GetIdentificator (CATUnicodeString &oId) \
{ \
return (ENVTIECALL(AUTICsmISNavigate,ENVTIETypeLetter,ENVTIELetter)GetIdentificator (oId)); \
} \
HRESULT  ENVTIEName::SetVisible(CATBoolean iSee) \
{ \
return (ENVTIECALL(AUTICsmISNavigate,ENVTIETypeLetter,ENVTIELetter)SetVisible(iSee)); \
} \
CATBoolean  ENVTIEName::IsVisible(CATBoolean withoutopt) \
{ \
return (ENVTIECALL(AUTICsmISNavigate,ENVTIETypeLetter,ENVTIELetter)IsVisible(withoutopt)); \
} \
void  ENVTIEName::ForceRedraw() \
{ \
 (ENVTIECALL(AUTICsmISNavigate,ENVTIETypeLetter,ENVTIELetter)ForceRedraw()); \
} \


/* Name of the TIE class */
#define class_TIE_AUTICsmISNavigate(classe)    TIEAUTICsmISNavigate##classe


/* Common methods inside a TIE */
#define common_TIE_AUTICsmISNavigate(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(AUTICsmISNavigate, classe) \
 \
 \
CATImplementTIEMethods(AUTICsmISNavigate, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(AUTICsmISNavigate, classe, 1) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(AUTICsmISNavigate, classe) \
CATImplementCATBaseUnknownMethodsForTIE(AUTICsmISNavigate, classe) \
 \
HRESULT  TIEAUTICsmISNavigate##classe::ModifyShortHelp(CATUnicodeString & ioText,CATBoolean iForEdit) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ModifyShortHelp(ioText,iForEdit)); \
} \
HRESULT  TIEAUTICsmISNavigate##classe::GetNodeBitmap (CATUnicodeString &oBmp) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetNodeBitmap (oBmp)); \
} \
HRESULT  TIEAUTICsmISNavigate##classe::ResetNodeBitmap () \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ResetNodeBitmap ()); \
} \
HRESULT  TIEAUTICsmISNavigate##classe::GetNodeBitmapFirst (CATUnicodeString &oBmp) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetNodeBitmapFirst (oBmp)); \
} \
HRESULT  TIEAUTICsmISNavigate##classe::SetNodeBitmap (const CATUnicodeString &iBmp) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetNodeBitmap (iBmp)); \
} \
int  TIEAUTICsmISNavigate##classe::GetNodeColor () \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetNodeColor ()); \
} \
HRESULT  TIEAUTICsmISNavigate##classe::GetChildren (CATListValCATBaseUnknown_var &oLChild) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetChildren (oLChild)); \
} \
HRESULT  TIEAUTICsmISNavigate##classe::GetChildrenOfList(void *data,CATListValCATBaseUnknown_var &oLChild) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetChildrenOfList(data,oLChild)); \
} \
HRESULT  TIEAUTICsmISNavigate##classe::GetChildren1 (CATListValCATBaseUnknown_var &oLChild) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetChildren1 (oLChild)); \
} \
HRESULT  TIEAUTICsmISNavigate##classe::GetIdentificator (CATUnicodeString &oId) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetIdentificator (oId)); \
} \
HRESULT  TIEAUTICsmISNavigate##classe::SetVisible(CATBoolean iSee) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetVisible(iSee)); \
} \
CATBoolean  TIEAUTICsmISNavigate##classe::IsVisible(CATBoolean withoutopt) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->IsVisible(withoutopt)); \
} \
void  TIEAUTICsmISNavigate##classe::ForceRedraw() \
{ \
   ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ForceRedraw(); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_AUTICsmISNavigate(classe) \
 \
 \
declare_TIE_AUTICsmISNavigate(classe) \
 \
 \
CATMetaClass * __stdcall TIEAUTICsmISNavigate##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_AUTICsmISNavigate,"AUTICsmISNavigate",AUTICsmISNavigate::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_AUTICsmISNavigate(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(AUTICsmISNavigate, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicAUTICsmISNavigate##classe(classe::MetaObject(),AUTICsmISNavigate::MetaObject(),(void *)CreateTIEAUTICsmISNavigate##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_AUTICsmISNavigate(classe) \
 \
 \
declare_TIE_AUTICsmISNavigate(classe) \
 \
 \
CATMetaClass * __stdcall TIEAUTICsmISNavigate##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_AUTICsmISNavigate,"AUTICsmISNavigate",AUTICsmISNavigate::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_AUTICsmISNavigate(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(AUTICsmISNavigate, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicAUTICsmISNavigate##classe(classe::MetaObject(),AUTICsmISNavigate::MetaObject(),(void *)CreateTIEAUTICsmISNavigate##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_AUTICsmISNavigate(classe) TIE_AUTICsmISNavigate(classe)
#else
#define BOA_AUTICsmISNavigate(classe) CATImplementBOA(AUTICsmISNavigate, classe)
#endif

#endif
