// COPYRIGHT Dassault Systemes 2005
//===================================================================
//
// AUTICsmType.h
// Define the AUTICsmType interface
//
//===================================================================
//
// Usage notes:
//   Interface dedicated to CSM Type management
//
//===================================================================
//
//  Apr 2005  Creation: Code generated by the CAA wizard  azh
//===================================================================

/**
* @CAA2Level L0
* @CAA2Usage U3
*/

/**
 * @fullreview AZH IME 05:04:08
 * @quickreview AZG 06:04:14
 */

#ifndef AUTICsmType_H
#define AUTICsmType_H

// Local (AUTCsmInterfaces) framework
#include "AUTCsmItfCPP.h"
#include "AUTICsmBase.h"
class AUTICsmISTypeExpression_var;

// System framework
#include "CATUnicodeString.h"


#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByAUTCsmItfCPP IID IID_AUTICsmType;
#else
extern "C" const IID IID_AUTICsmType ;
#endif

//------------------------------------------------------------------

/**
* Interface dedicated to CSM Type management.
* <b>Role</b>: manages information on CSM Types.
*/
class ExportedByAUTCsmItfCPP AUTICsmType : public AUTICsmBase
{
	CATDeclareInterface;

public:
    /**
    * kind of CSM Type.
    * Used to define CSM Types.<br>
    * <br><b>Legal values</b>:
    * <tt>Basic</tt> one of the core types: pure, bool, string, int, float or double
    * <tt>Extern</tt> reference to a type defined in the host language
    * <tt>Enum</tt> an enum type is given by a finite set of values
    * <tt>Concrete</tt> it is an enum type with only one value
    */
    enum Kind {Basic,
        Extern,
        Enum,
        Concrete};

    /** 
    * Set the name of the CSM Type (No name checking)
    * @param iName [in]
    *   Name of the CSM Type
    * @return
    *   <code>S_OK</code> if everything ran ok, <code>E_FAIL</code> if an error occurs. Use CATGetLastError() to retrieve error message.
    */
    virtual HRESULT SetId(const CATUnicodeString &iName)=0;
    /** 
    * Set the name of the CSM Object and get another name if it is already used
    * @param iName [in]
    *   Input Name
    * @param oName [in]
    *   New unique name
    * @return
    *   <code>S_OK</code> if everything ran ok, <code>E_FAIL</code> if an error occurs. Use CATGetLastError() to retrieve error message.
    * <b>Warning</b> This method is time-consuming when used for huge data because it checks the name unicity and fond a new one if needed. 
	* Use SetId() and manage unicity by yourself.
    */
    virtual	HRESULT SetFreshId(const CATUnicodeString &iName,CATUnicodeString &oName=CATUnicodeString())=0;
    /** 
    * Retrieve the name of the CSM Type.
    * @param oName [out]
    *   Name of the CSM Type
    * @return
    *   <code>S_OK</code> if everything ran ok, <code>E_FAIL</code> if an error occurs. Use CATGetLastError() to retrieve error message.
    */
    virtual HRESULT GetId(CATUnicodeString &oName)=0;
    /** 
    * Retrieve the kind of the CSM Type.
    * @param okind [out]
    *   kind of the CSM Type. @see Kind for available values
    * @return
    *   <code>S_OK</code> if everything ran ok, <code>E_FAIL</code> if an error occurs. Use CATGetLastError() to retrieve error message.
    */
     virtual HRESULT GetKind(Kind &oKind)=0 ;
    /** 
    * Get the @href AUTICsmISExpression to which this type is aliased.
    * Works only if kind is Basic.
    * @param ospExpr [out]
    *   The expression of the CSM Type
    * @return
    *   <code>S_OK</code> if everything ran ok, <code>E_FAIL</code> if an error occurs. Use CATGetLastError() to retrieve error message.
    */
    virtual HRESULT GetTypeExpr(AUTICsmISTypeExpression_var &ospExpr) = 0;
    /** 
    * Check if a type is using "pure" base type , directly or by inheritrance, or in its tree
    * @return
    *   <code>S_OK</code> if teh type use pure, <code>S_FALSE</code> is the type do not use pure, <code>E_FAIL</code> if an error occurs. Use CATGetLastError() to retrieve error message.
    */
    virtual HRESULT IsUsingPure() = 0;

};
CATDeclareHandler(AUTICsmType, CATBaseUnknown);
//------------------------------------------------------------------

#endif
