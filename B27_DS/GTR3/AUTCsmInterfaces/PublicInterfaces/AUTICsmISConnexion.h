// COPYRIGHT Dassault Systemes 2005
//===================================================================
//
// AUTICsmISConnexion.h
// Define the AUTICsmISConnexion interface
//
//===================================================================
//
// Usage notes:
//   New interface: describe its use here
//
//===================================================================
//
//  Sep 2005  Creation: Code generated by the CAA wizard  szw
//===================================================================

/**
* @CAA2Level L0
* @CAA2Usage U3
*/

/**
 * @quickreview AZG 06:04:14
 */

#ifndef AUTICsmISConnexion_H
#define AUTICsmISConnexion_H

// Local (AUTCsmInterfaces) framework
#include "AUTCsmItfCPP.h"
class AUTICsmPattern_var;

// System framework
#include "CATBaseUnknown.h"


#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByAUTCsmItfCPP IID IID_AUTICsmISConnexion;
#else
extern "C" const IID IID_AUTICsmISConnexion ;
#endif

//------------------------------------------------------------------

/**
* Interface dedicated to manage CSM Connexions between patterns (Ports, signals or instance ports).
* <b>Role</b>: 
* This interface allows to perform connexions between the following objects:AUTICsmISPort,
* AUTICsmISSignal and AUTICsmISInstancePort
* A Connexion is first created in a block then attached to patterns. One or both plug or the connexion
* may be let open (i.e. not mapped to a pattern)
*
* @example
*   AUTICsmISBlock_var spBlock;
*   AUTICsmISPort_var spPort;
*   AUTICsmISInstancePort_var spInstPort;
*   AUTICsmISConnexion_var spConnect;
*   rc = spBlock->AddConnexion(CATUnicodeString(),spPort,spInstPort,spConnect);
*   if(SUCCEEDED(rc) && (spConnect==NULL_var)) 
*   {...}
*   
*
* @href ClassReference, Class#MethodReference, #InternalMethod...
*/
class ExportedByAUTCsmItfCPP AUTICsmISConnexion: public CATBaseUnknown
{
	CATDeclareInterface;

public:

	/** 
	* Get the connected pattern (port, signal or instance port)
	* @param ospPlug1    [out]    Left  connected pattern
	* @param ospPlug2    [out]    Right connected pattern
	* @return
    *   <code>S_OK</code> if everything ran ok, 
    *   <code>E_FAIL</code> if an error occurs. 
    *       Use CATGetLastError() to retrieve error message.
	*/    
	virtual HRESULT GetConnectedPlugs(AUTICsmPattern_var &ospPlug1,
        AUTICsmPattern_var &ospPlug2) = 0;
	/** 
	* Deconnect the given pattern of the connexion. The pattern may be a left or right plug of the connexion
	* @param ospPlug    [in]  Pattern to deconnect
	* @return
    *   <code>S_OK</code> if everything ran ok, 
    *   <code>E_FAIL</code> if an error occurs. 
    *       Use CATGetLastError() to retrieve error message.
	*/    
	virtual HRESULT DeconnectPlug  (AUTICsmPattern_var ispPlug) = 0;
	/** 
	* Modify the left plug of the connexion. 
	* @param ospPlug    [in]  Pattern to connect to the left plug
	* @return
    *   <code>S_OK</code> if everything ran ok, 
    *   <code>E_FAIL</code> if an error occurs. 
    *       Use CATGetLastError() to retrieve error message.
	*/    
	virtual HRESULT ModifyLeftPlug (AUTICsmPattern_var ispPlug) = 0;
	/** 
	* Modify the right plug of the connexion. 
	* @param ospPlug    [in]  Pattern to connect to the right plug
	* @return
    *   <code>S_OK</code> if everything ran ok, 
    *   <code>E_FAIL</code> if an error occurs. 
    *       Use CATGetLastError() to retrieve error message.
	*/    
	virtual HRESULT ModifyRightPlug(AUTICsmPattern_var ispPlug) = 0;
	/** 
	* Remove the connexion. 
	* @return
    *   <code>S_OK</code> if everything ran ok, 
    *   <code>E_FAIL</code> if an error occurs. 
    *       Use CATGetLastError() to retrieve error message.
	*/    
	virtual HRESULT Remove() = 0;

};
CATDeclareHandler(AUTICsmISConnexion,CATBaseUnknown);

//------------------------------------------------------------------

#endif
