// COPYRIGHT Dassault Systemes 2005
//===================================================================
//
// AUTICsmISCommon.h
// Define the AUTICsmISCommon interface
//
//===================================================================
//
// Usage notes:
//   New interface: describe its use here
//
//===================================================================
//
//  Mar 2005  Creation: Code generated by the CAA wizard  azh
//===================================================================

/**
* @CAA2Level L0
* @CAA2Usage U3
*/

/**
 * @fullreview AZH 05:03:24
 * @fullreview AZH IME 05:04:08
 * @quickreview AZH IME 05:12:02
 * @quickreview AZG 06:04:14
 */

#ifndef AUTICsmISCommon_H
#define AUTICsmISCommon_H

// Local (AUTCsmInterfaces) framework
class AUTICsmPathes;
#include "AUTCsmItfCPP.h"
#include "AUTICsmBase.h"
#include "AUTICsmFilter.h"
class AUTICsmAnchor_var;
class AUTICsmISCommon_var;
class AUTICsmWorkspace_var;

// System framework
class CATListValCATBaseUnknown_var;
class CATListValCATUnicodeString;
class CATUnicodeString;
#include "CATError.h"


#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByAUTCsmItfCPP IID IID_AUTICsmISCommon;
#else
extern "C" const IID IID_AUTICsmISCommon ;
#endif

#ifdef _WINDOWS_SOURCE
typedef unsigned __int64 AUTCsmRedrawOptions;
#else
typedef unsigned long long AUTCsmRedrawOptions;
#endif

#define AUTCsmRedrawWithError             (1 << 0)
#define AUTCsmRedrawForceChildren         (1 << 2)
#define AUTCsmRedrawForceChildrenRecur    (1 << 3)
#define AUTCsmRedrawSimple    (1 << 4)
#define AUTCsmRedrawParent    (1 << 5)
#define AUTCsmRedrawChildren    (1 << 6)
#define AUTCsmRedrawNode    (1 << 7)
#define AUTCsmRedrawDirty    (1 << 8)
#define AUTCsmRedrawDefault (AUTCsmRedrawWithError|AUTCsmRedrawSimple)
#define AUTCsmRedrawNoDefault (1 << 9)

//------------------------------------------------------------------
/**
* Interface dedicated to manage CSM Objects.
* <b>Role</b>: manages information on most of CSM Objects: Module, Constant,Type,Ports, Signals, Map,...
* <br>
* 
* Do not reimplement this interface
*/
class ExportedByAUTCsmItfCPP AUTICsmISCommon: public AUTICsmBase
{
    CATDeclareInterface;

public:
    /** 
    * Set the name to the CSM Object.
    * This method returns S_FALSE  for objects which are a record or tuple (AUTICsmISPort, AUTICsmISTypeExpression)
    * @param iName [in] Name for the CSM Object
    * @return
    *   <code>S_OK</code> if everything ran ok, 
    *   <code>S_FALSE</code> if the object has no name, 
    *   <code>E_FAIL</code> if an error occurs. 
    *   Use CATGetLastError() to retrieve error message.
    */
    virtual HRESULT SetId(const CATUnicodeString &iName)=0;
    /** 
    * Get the name of the CSM Object.
    * This method returns S_FALSE for objects which are a record or tuple (AUTICsmISPort, AUTICsmISTypeExpression)
    * @param oName [in] Id of the CSM Object
    * @return
    *   <code>S_OK</code> if everything ran ok, 
    *   <code>S_FALSE</code> if the object has no name, 
    *   <code>E_FAIL</code> if an error occurs. 
    *   Use CATGetLastError() to retrieve error message.
    */    
    virtual HRESULT GetId(CATUnicodeString &oName)=0;
    /** 
    * Set the name of the CSM Object and check if it already used for another CSM Object.
    * In that case, set a unique name to this object
    * This method returns S_FALSE  for objects which are a record or tuple (AUTICsmISPort, AUTICsmISTypeExpression)
    * @param iName [in] Name for the CSM Object
    * @param oOkName [Out] New Name for the CSM Object
    * @return
    *   <code>S_OK</code> if everything ran ok, 
    *   <code>S_FALSE</code> if the object has no ID, 
    *   <code>E_FAIL</code> if an error occurs. 
    *   Use CATGetLastError() to retrieve error message.
    */
    virtual	HRESULT SetFreshId(const CATUnicodeString &iName,
        CATUnicodeString &oOkName=CATUnicodeString())=0;
    /** 
    * Get the @href AUTICsmWorkspace that the CSM Object belongs to
    * @param ospWorksSpace [out] the returned Workspace
    * @return
    *   <code>S_OK</code> if everything ran ok, 
    *   <code>E_FAIL</code> if an error occurs. 
    *   Use CATGetLastError() to retrieve error message.
    */    
    virtual HRESULT GetWorkspace(AUTICsmWorkspace_var &ospWorksSpace,
        const CATBoolean iInCurrentLayout=FALSE)=0;
    /** 
    * Get the @href AUTICsmWorkspace list that the CSM Object belongs to. In case of libraries in context, a Csm Object may owned to several worskpaces
    * @param olWorkspaces [out] List of Workspaces
    * @param iInCurrentLayout [in] 
    * @return
    *   <code>S_OK</code> if everything ran ok, 
    *   <code>E_FAIL</code> if an error occurs. 
    *   Use CATGetLastError() to retrieve error message.
    */    
    virtual HRESULT GetWorkspaces(CATListValCATBaseUnknown_var &olWorkspaces,
        const CATBoolean iInCurrentLayout=FALSE)=0;
    /** 
    * Get the @href AUTICsmAnchor that the CSM Object is attached
    * @param ospAnchor [out] the returned CSM Anchor
    * @return
    *   <code>S_OK</code> if everything ran ok, 
    *   <code>E_FAIL</code> if an error occurs. 
    *   Use CATGetLastError() to retrieve error message.
    */    
    virtual HRESULT GetAnchor(AUTICsmAnchor_var &ospAnchor,
        const CATBoolean iInCurrentLayout=FALSE)=0;
    virtual HRESULT GetAnchors(CATListValCATBaseUnknown_var &ospListAnchor,
        const CATBoolean iInCurrentLayout=FALSE)=0;
    /** 
    * Set the CSM object to dirty
    * Warning, if this object is a library, only the document where is used this library is set dirty
    * @return
    *   <code>S_OK</code> if everything ran ok, 
    *   <code>E_FAIL</code> if an error occurs. 
    *   Use CATGetLastError() to retrieve error message.
    */    
    virtual void SetDirty(void)=0;
    /** 
    * Test if the CSM object is up to date
    * @return
    *   <code>TRUE</code> if it is ok, <code>FALSE</code> if the CSM object is dirty.
    */    
    virtual CATBoolean IsUptodate(void)=0;
    /** 
    * Build the path to a CSM Object
    * @param ipTo [in]      CSM Object
    * @param oPath [out]    Path to the CSM Object
    * @return
    *   <code>TRUE</code> if it is ok, <code>FALSE</code> if the CSM object is dirty.
    */    
    virtual HRESULT GetPathTo(const CATBaseUnknown *ipTo,
        CATListOfCATUnicodeString &oPath)=0;
    /** 
    * Get the comment of the CSM Object.
    * @param oComment [out] Comment of the CSM Object
    * @return
    *   <code>S_OK</code> if everything ran ok, 
    *   <code>E_FAIL</code> if an error occurs. 
    *   Use CATGetLastError() to retrieve error message.
    */    
    virtual HRESULT GetComment (CATUnicodeString &oComment)=0;
    /** 
    * Set the comment of the CSM Object.
    * @param iComment [in] Comment of the CSM Object
    * @return
    *   <code>S_OK</code> if everything ran ok, 
    *   <code>E_FAIL</code> if an error occurs. 
    *   Use CATGetLastError() to retrieve error message.
    */    
    virtual HRESULT SetComment (const CATUnicodeString&  iComment )=0;

	/** 
    * <b>Warning</b> This method may be time-consuming when processing huge data because it triggers typing if the workspace is dirty
	*/
	virtual HRESULT GetVisible(BaseKind kind,CATListOfCATUnicodeString &oListeName,const IID &iId=IID_CATBaseUnknown)=0;
	/** 
    * <b>Warning</b> This method may be time-consuming when processing huge data because it triggers typing if the workspace is dirty
	*/
    virtual HRESULT GetVisible(BaseKind kind,CATListOfCATUnicodeString &oListeName,CATListValCATBaseUnknown_var &oListeBlocks,const IID &iId=IID_CATBaseUnknown)=0;
    
	/** 
	 * DEPRECATED
	 * Used GetVisiblePathesWithFilter
	 */
	virtual HRESULT GetVisiblePathes(BaseKind kind,AUTICsmPathes **oppPathes)=0;
    virtual HRESULT GetVisiblePathesWithFilter(BaseKind kind,AUTICsmPathes **oppPathes,const IID &iId=IID_AUTICsmFilter)=0;  
    /** 
    * Test if the CSM Object derived from a dedicated LateType
    * @param iLate [in] LateType
    * @return
    *   <code>S_TRUE</code> if it is, <code>E_FALSE</code> if not
    */
    virtual CATBoolean IsDerivedFrom(const CATUnicodeString &iLate)=0;
    /** 
    * Get CSM type of the CSM object. @href AUTICsmBase::BaseKind for available values
    * @param kind [out] The CSM Object kind (module, Constant,...)
    * @return
    *   <code>S_OK</code> if everything ran ok, 
    *   <code>E_FAIL</code> if an error occurs. 
    *   Use CATGetLastError() to retrieve error message.
    */
    virtual HRESULT GetBaseKind(BaseKind &kind)=0;
    /** 
    * Get the pointer of the object defined by its kind and by its absolute path.
    * @param iKind [in]  The CSM Object kind to get (@href AUTICsmBase::BaseKind))
    * @param iPath [in]  The absolute path (@href AUTICsmBase::BaseKind))
    * @param oppObj [out]  The pointer on the CSM Object
    * @param iIID [in]   The interface identifier for which a pointer is requested   
    * @return
    *   <code>S_OK</code> if everything ran ok, 
    *   <code>E_FAIL</code> if an error occurs. 
    *   Use CATGetLastError() to retrieve error message.
    * <b>Warning</b> This method may be time-consuming when processing huge data because it triggers typing if the workspace is dirty
    */  
    virtual HRESULT GetObject(BaseKind iKind,CATListOfCATUnicodeString &iPath,
        CATBaseUnknown **oppObj,const IID &iIID=IID_CATBaseUnknown)=0;

    /** 
    * Deprecated. Use GetPath
    */  
    virtual HRESULT GetObjectPath(CATListOfCATUnicodeString &oPath)=0;
    /** 
    * Deprecated. Use GetPath
    */  
    virtual HRESULT GetObjectPath(CATUnicodeString &oPath)=0;
    /** 
    * Get the absolute or relative path of the object in its workspace
    * @param iAbs [in]  True to get an absolute path
    * @param oPath [out]  The object path as a list of string
    * @return
    *   <code>S_OK</code> if everything ran ok, 
    *   <code>E_FAIL</code> if an error occurs. 
    *   Use CATGetLastError() to retrieve error message.
    */  
    virtual HRESULT GetPath (CATBoolean iAbs, CATListOfCATUnicodeString &oPath)=0;
    /** 
    * Get the absolute or relative path of the object in its workspace
    * @param iAbs [in]  True to get an absolute path
    * @param oPath [out]  The object path as a list of string
    * @return
    *   <code>S_OK</code> if everything ran ok, 
    *   <code>E_FAIL</code> if an error occurs. 
    *   Use CATGetLastError() to retrieve error message.
    */  
    virtual HRESULT GetPath (CATBoolean iAbs, CATUnicodeString &oPath)=0;
    /** 
    * Get the first ancestor of a given class inside the module hierarchy
    * @param iId [in] Class identifier          
    * @param oppAncestor [out] Ancestor if it has been found, else NULL
    * @return
    *   <code>S_OK</code> if everything ran ok, 
    *   <code>E_FAIL</code> if an error occurs. 
    *   Use CATGetLastError() to retrieve error message.
    */
    virtual HRESULT GetAncestor(IID iId,CATBaseUnknown **oppAncestor)=0;
    /** 
    * Get the late type linked to the CSM Object
    * @param oLate [out] The late type
    * @return
    *   <code>S_OK</code> if everything ran ok, 
    *   <code>E_FAIL</code> if an error occurs. 
    *   Use CATGetLastError() to retrieve error message.
    */
    virtual HRESULT GetLateType(CATUnicodeString &oLate)=0;
    /** 
    * Get the Object path
    * @param oIdPath [out] Path defined as a CATListOfCATUnicodeString
    * @return
    *   <code>S_OK</code> if everything ran ok, 
    *   <code>E_FAIL</code> if an error occurs. 
    *   Use CATGetLastError() to retrieve error message.
    */
    virtual HRESULT IdPath( CATListOfCATUnicodeString &oIdPath)=0;
    /** 
    * Get the complete object path
    * @param oIdPath [out] Path defined as a CATUnicodeString
    * @return
    *   <code>S_OK</code> if everything ran ok, 
    *   <code>E_FAIL</code> if an error occurs. 
    *   Use CATGetLastError() to retrieve error message.
    */
    virtual HRESULT IdPath( CATUnicodeString &oIdPath)=0;
    /** 
    * Get all the Object pathes
    * @param oIdPath [out] List of all Pathes
    * @return
    *   <code>S_OK</code> if everything ran ok, 
    *   <code>E_FAIL</code> if an error occurs. 
    *   Use CATGetLastError() to retrieve error message.
    */
    virtual HRESULT AllIdPathes(CATListOfCATUnicodeString &oIdPath)=0;
    /** 
    * Get the LCM text of the object
    * @param oLCMText [out] LCM text
    * @param iComplet [in]  Legal values are given in @href AUTICsmBase::TextMode
    * @return
    *   <code>S_OK</code> if everything ran ok, 
    *   <code>E_FAIL</code> if an error occurs. 
    *   Use CATGetLastError() to retrieve error message.
    */
    virtual HRESULT GetLcmText (CATUnicodeString &oLCMText,
        TextMode iComplet=TextWithAssocDataAndUUID)=0;
    /**
    * Returns true if this object contains an error.
    * @param b true if this object contains an error
    * @return E_FAIL if an error occurs. Use CATGetLastError() to retrieve error message.
    * <b>Warning</b> This method may be time-consuming when processing huge data because it triggers typing if the workspace is dirty
    */
    virtual HRESULT HasError(CATBoolean &b) = 0;
    /**
    * Get error message optionnaly attached to this objects.
    * @param k the error kind, see @href AUTICsmBase#error_kind
    * @param s the error severiry 
    * @param opKey the key to the error catalog AUTLciCompiler.CATNls if error kind is not
    * NoError, else NULL
    * @param opMessage the error message if error kind is not NoError, else NULL
    * @return E_FAIL if an error occurs. Use CATGetLastError() to retrieve error message.
    */
    virtual HRESULT GetError(AUTICsmBase::error_kind &k, CATErrorType &s, 
        CATUnicodeString &oKey, CATUnicodeString &oMessage) = 0;
    /** 
    * Get the UID of the CSM object
    * @param oUid [out] UID of the object
    * @return
    *   <code>S_OK</code> if everything ran ok, 
    *   <code>E_FAIL</code> if an error occurs. 
    *   Use CATGetLastError() to retrieve error message.
    */
    virtual HRESULT GetUid(CATUnicodeString &oUid)=0;
    /** 
    * Get a child object by its UID
    * @param iUid [in] UID of the object to retrieve
    * @param ospObj [out] Pointer of the object identified by its UID or NULL_var
    * @return
    *   <code>S_OK</code> if everything ran ok, 
    *   <code>E_FAIL</code> if an error occurs. 
    *   Use CATGetLastError() to retrieve error message.
    */
    virtual HRESULT GetByUid(const CATUnicodeString &iUid,AUTICsmISCommon_var &ospObj)=0;
    /** 
    * Get a child object by its UID
    * @param iUid [in] UID of the object to retrieve
    * @param oppObj [out]  The pointer on the CSM Object or NULL if the object UID 
    * does not exist
    * @param iIID [in]   The interface identifier for which a pointer is requested   
    * @return
    *   <code>S_OK</code> if everything ran ok, <code>E_FAIL</code> if an error occurs. 
    *   Use CATGetLastError() to retrieve error message.
    */
    virtual HRESULT GetByUid(const CATUnicodeString &iUid,CATBaseUnknown **oppObj,
        const IID &iID=IID_CATBaseUnknown)=0;
    /** 
    * Remove the CSM Object
    * @return
    *   <code>S_OK</code> if everything ran ok, <code>E_FAIL</code> if an error occurs. 
    *   Use CATGetLastError() to retrieve error message.
    */
    virtual HRESULT Remove()=0;
    /** 
    * Set the LCM Text of the CSM object
    * @param iText [in] Text which should respect LCM text grammar
    * @return
    *   <code>S_TRUE</code> if the object is valid, 
    *   <code>E_FALSE</code> if there is compilation errors.
    */
    virtual HRESULT SetFromText(const CATUnicodeString &iText)=0;
    virtual HRESULT CreateUndoRequest(CATBaseUnknown *iObj,
        const CATUnicodeString  &undoTitle,CATBoolean MakeStep = true)=0;
    /** 
    * Get the signature of the object if the object is valid else get the error message
    * @param oSig [out] Signature or error message if the object is not valid
    * @return
    *   <code>S_TRUE</code> if the object is valid, 
    *   <code>E_FALSE</code> if there is compilation errors.
    */
    virtual CATBoolean GetSigOrError(CATUnicodeString &oSig)=0;
    
    /**
    * Deprecated
    */
    virtual HRESULT GetStreamOfModule(CATUnicodeString &oText)=0;
    /**
    * True is object still belong the modeler
    * @param o no doc for this param
    * @return E_FAIL if an error occurs. Use CATGetLastError() to retrieve error message.
    */
    virtual HRESULT IsAlive(CATBoolean &o) = 0;
    virtual HRESULT SetImLink(const CATUnicodeString &iPath,
        const CATUnicodeString &ikey="AUTCsmImage")=0;
    virtual HRESULT RemoveImLink(const CATUnicodeString &ikey="AUTCsmImage")=0;
    virtual HRESULT GetImLink(CATUnicodeString &oPath,
        const CATUnicodeString &ikey="AUTCsmImage")=0;
    /** 
    * Redraw the navigation tree of the object
    * @param iOptions [in] Options to modify the behavior
    * is a logical or of following value
    *  AUTCsmRedrawSimple: Call redraw of the object @See CATIRedrawEvent::Redraw
    *  AUTCsmRedrawParent: Call redraw of the parent @See CATIRedrawEvent::RedrawParent
    *  AUTCsmRedrawChildren: call redraw of children @See CATIRedrawEvent::RedrawChildren
    *  AUTCsmRedrawNode: Call redraw only of the node @See CATIRedrawEvent::RedrawGrphicNode
    *  AUTCsmRedrawWithError: Also Update node of all objects in error or prviously in error
    *  AUTCsmRedrawForceChildren: Force redraw of children of the object
    *  AUTCsmRedrawForceChildrenRecur: Force recursively redraw of all descendant 
    * (bad performancies)
    *  AUTCsmRedrawDirty: Also set the model dirty
    * 
    * Default is AUTCsmRedrawDefault (AUTCsmRedrawWithError|AUTCsmRedrawSimple)
    *
    * @return
    *   <code>S_OK</code> if redraw is done, <code>E_FAIL</code> in case of error.
    */
    virtual	 HRESULT Redraw(AUTCsmRedrawOptions iOptions=AUTCsmRedrawDefault)=0;
    /** 
    * Get an ancestor of the object in the tree vizualisation
    * @param ospfather [out] The ancestor found
    * Could be : the object itself if it is in the tree
    * A Csm Object
    * A AUTICsmWorkspace
    * A AUTICsmAnchor (CATIProduct in R17)
    *
    * @return
    *   <code>S_OK</code> if found, <code>S_FALSE </code> if no ancestor is found,
    *   <code>E_FAIL</code> in case of error.
    */
    virtual HRESULT GetFatherInTree(CATBaseUnknown_var &ospFather)=0;

};
CATDeclareHandler(AUTICsmISCommon,CATBaseUnknown);
//------------------------------------------------------------------

#endif
