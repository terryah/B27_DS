// COPYRIGHT Dassault Systemes 2005
//===================================================================
//
// AUTICsmISMod.h
// Define the AUTICsmISMod interface
//
//===================================================================
//
// Usage notes:
//   Interface dedicated to CSM Module management
//
//===================================================================
//
//  Mar 2005  Creation: Code generated by the CAA wizard  azh
//===================================================================

/**
* @CAA2Level L0
* @CAA2Usage U3
*/

/**
 * @fullreview AZH 05:03:24
 * @fullreview AZH IME 05:04:08
 * @quickreview AZG 06:04:14
 */

#ifndef AUTICsmISMod_H
#define AUTICsmISMod_H

// Local (AUTCsmInterfaces) framework
#include "AUTCsmItfCPP.h"
#include "AUTICsmBase.h"
class AUTICsmISMod_var;

// System framework
class CATListValCATBaseUnknown_var;
#include "CATUnicodeString.h"


#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByAUTCsmItfCPP IID IID_AUTICsmISMod;
#else
extern "C" const IID IID_AUTICsmISMod ;
#endif

//------------------------------------------------------------------

/**
* Interface dedicated to manage CSM Modules.
* <b>Role</b>: manages information on CSM Modules.
* <br>
*/
class ExportedByAUTCsmItfCPP AUTICsmISMod : public AUTICsmBase
{
    CATDeclareInterface;
public:
    /** 
    * Get the father module.
    * @param ospFather [out] The father module handler
    * @return
    *   <code>S_OK</code> if everything ran ok, <code>E_FAIL</code> if an error occurs.
    *       Use CATGetLastError() to retrieve error message.
    */
    virtual HRESULT GetFather(AUTICsmISMod_var &ospFather)=0;
    /**
    * @nodoc
    * Deprecated. Used AUTICsmISComponent::GetChildren
    */
    virtual HRESULT GetChildren(CATListValCATBaseUnknown_var &oList)=0;
    /**
    * @nodoc
    * Deprecated. Used AUTICsmISComponent::GetNbChildren
    */
    virtual int GetNbChildren(void)=0;
    /**
    * @nodoc
    * Deprecated. Used AUTICsmISComponent::GetChildren
    */
    virtual HRESULT GetChild(int i,CATBaseUnknown_var  &spChild)=0;
    /** 
    * Get the position of the CSM object in the tree.
    * @param iChild [in] The CSM object
    * @return Position in the tree. -1 means it is at the end of the list.
    */
    virtual int GetChildPos(CATBaseUnknown *iChild)=0;

    /** 
    * @nodoc
    */         
    virtual HRESULT GetNewIdForChild (const CATUnicodeString &iBase,
        CATUnicodeString &oName)=0;
    /** 
    * Remove a CSM object.
    * @param iChild [in] The CSM object to remove
    * @return
    *   <code>S_OK</code> if everything ran ok, <code>E_FAIL</code> if an error occurs.
    *       Use CATGetLastError() to retrieve error message.
    */
    virtual HRESULT RemoveChild(CATBaseUnknown *iChild)=0;
    /** 
    * Redraw the module.
    * @param recur [in] 
    *   <code>TRUE</code> each child is redrawn, 
    *   <code>E_FAIL</code>Only the module is redrawn .
    * @return none
    */
    virtual void Redraw(CATBoolean recur=TRUE)=0;
    /** 
    * Set the name of the CSM Object.
    * * @param iName [in] Input name
    * @return
    *   <code>S_OK</code> if everything ran ok, <code>E_FAIL</code> if an error occurs.
    *       Use CATGetLastError() to retrieve error message.
    */
    virtual HRESULT SetId(const CATUnicodeString &iName)=0;
    /** 
    * Set the name of the CSM Object and get another name if it is already used
    * @param iName [in]
    *   Input Name
    * @param oName [in]
    *   New unique name
    * @return
    *   <code>S_OK</code> if everything ran ok, <code>E_FAIL</code> if an error occurs.
    *       Use CATGetLastError() to retrieve error message.
    * <b>Warning</b> This method is time-consuming when used for huge data because it checks the name unicity and fond a new one if needed. 
	* Use SetId() and manage unicity by yourself.
    */
    virtual	HRESULT SetFreshId(const CATUnicodeString &iName,
        CATUnicodeString &oName=CATUnicodeString())=0;
    /** 
    * Retrieve the Id of the CSM module.
    * @param oid [out] Id of the CSM module
    * @return
    *   <code>S_OK</code> if everything ran ok, <code>E_FAIL</code> if an error occurs.
    *       Use CATGetLastError() to retrieve error message.
    */
    virtual HRESULT GetId(CATUnicodeString &oid)=0;
    /**
    * @nodoc
    * Deprecated. Used AUTICsmISComponent::SetPrivate
    */
    virtual HRESULT SetPrivate(const CATBoolean iPrivate=TRUE) = 0;
    /**
    * @nodoc
    * Deprecated. Used AUTICsmISComponent::GetPrivate
    */
    virtual HRESULT GetPrivate(CATBoolean &oPrivate) = 0;

};
CATDeclareHandler(AUTICsmISMod,CATBaseUnknown);
//------------------------------------------------------------------

#endif
