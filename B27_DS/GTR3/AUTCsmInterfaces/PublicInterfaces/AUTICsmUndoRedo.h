// COPYRIGHT Dassault Systemes 2005
//===================================================================
//
// AUTICsmUndoRedo.h
// Define the AUTICsmUndoRedo interface
//
//===================================================================
//
// Usage notes:
//   New interface: describe its use here
//
//===================================================================
//
//  Apr 2005  Creation: Code generated by the CAA wizard  azh
//===================================================================

/**
 * @CAA2Level L0
 * @CAA2Usage U3
 */

/**
 * @fullreview AZH IME 05:04:19
 * @quickreview AZG 06:04:14
 */

#ifndef AUTICsmUndoRedo_H
#define AUTICsmUndoRedo_H

// Local (AUTCsmInterfaces) framework
#include "AUTCsmItfCPP.h"
#include "AUTICsmBase.h"


#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByAUTCsmItfCPP IID IID_AUTICsmUndoRedo;
#else
extern "C" const IID IID_AUTICsmUndoRedo ;
#endif

//------------------------------------------------------------------

/**
 * Interface class for Undo Redo
 */
class ExportedByAUTCsmItfCPP AUTICsmUndoRedo: public AUTICsmBase
{
  CATDeclareInterface;

  public:
         /**
          * make step for undo/redo command
          * <br><b> Role: </b> 
          * @return 
          *        E_FAIL if an error occurs.
          */
         virtual HRESULT MakeStep() = 0;
         
         /**
          * Undo for undo/redo command 
          * <br><b> Role: </b> 
          * @return 
          *        E_FAIL if an error occurs.
          */
         virtual HRESULT UndoOnce() = 0;
         
         /**
          * Redo for undo/redo command
          * <br><b> Role: </b> 
          * @return 
          *        E_FAIL if an error occurs.
          */
         virtual HRESULT RedoOnce() = 0;
         
};
CATDeclareHandler(AUTICsmUndoRedo, CATBaseUnknown);

//------------------------------------------------------------------

#endif
