// COPYRIGHT Dassault Systemes 2005
//===================================================================
//
// AUTICsmNavigate.h
// Define the AUTICsmNavigate interface
//
//===================================================================
//
// Usage notes:
//   New interface: describe its use here
//
//===================================================================
//
//  Dec 2005  Creation: Code generated by the CAA wizard  azh
//===================================================================

/**
* @CAA2Level L0
* @CAA2Usage U4
*/

/**
 * @fullreview AZH IME 05:04:19
 * @quickreview AZG 06:04:14
 */

#ifndef AUTICsmNavigate_H
#define AUTICsmNavigate_H

// Local (AUTCsmInterfaces) framework
#include "AUTCsmItfCPP.h"

// System framework
class CATListValCATBaseUnknown_var;
class CATUnicodeString;
#include "CATBaseUnknown.h"


#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByAUTCsmItfCPP IID IID_AUTICsmNavigate;
#else
extern "C" const IID IID_AUTICsmNavigate ;
#endif

//------------------------------------------------------------------

/**
* Interface representing xxx.
*
* <br><b>Role</b>: Components that implement
* AUTICsmNavigate are ...
* <p>
* Do not use the AUTICsmNavigate interface for such and such

*
* @example
*  // example is optional
*  AUTICsmNavigate* currentDisplay = NULL;
*  rc = window-&gt;QueryInterface(IID_AUTICsmNavigate,
*                                     (void**) &amp;currentDisplay);
*
* @href ClassReference, Class#MethodReference, #InternalMethod...
*/
class ExportedByAUTCsmItfCPP AUTICsmNavigate : public CATBaseUnknown
{
  CATDeclareInterface;

  public:
	
	virtual HRESULT ModifyShortHelp(CATUnicodeString & ioText)=0;
	virtual HRESULT GetNodeBitmap (CATUnicodeString &oBmp)=0;
	virtual int GetNodeColor ()=0;
	virtual HRESULT GetChildren (CATListValCATBaseUnknown_var &oLChild)=0;
	virtual HRESULT GetIdentificator (CATUnicodeString &oId)=0;
	
};
CATDeclareHandler(AUTICsmNavigate,CATBaseUnknown);

//------------------------------------------------------------------

#endif
