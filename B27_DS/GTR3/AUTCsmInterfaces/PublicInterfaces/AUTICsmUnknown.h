//===================================================================
// COPYRIGHT Dassault Systemes 2004-
//===================================================================
//
// AUTICsmUnknown.h
// Define the AUTICsmUnknown interface
//
//===================================================================
//
// Usage notes: DO NOT EDIT THIS FILE
//
//===================================================================
//
//  Creation: this code has been created by the genmodel tool 
//===================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/

/**
* @fullreview AZH IME 06:01:31
 * @fullreview DWL 06:06:16
*/
#ifndef AUTICsmUnknown_H
#define AUTICsmUnknown_H

#include "AUTCsmItfCPP.h"
#include "CATBaseUnknown.h"
#include "CATBoolean.h"
class CATUnicodeString;
#include "CATStreamArea.h"
#include "CATCollec.h"
#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByAUTCsmItfCPP IID IID_AUTICsmUnknown;
#else
extern "C" const IID IID_AUTICsmUnknown ;
#endif
// dependencies for attributes and methods

//-----------------------------------------------------------------------

/**
 * 
*/
class ExportedByAUTCsmItfCPP AUTICsmUnknown: public CATBaseUnknown
{
  // CATDeclareInterface;
   
   public:
      /**
      */
      enum KernelKind {KError=-1,
					   KModule,
                       KTopModule,
                       KModSig,
                       KModType,
                       KModDef,
                       KIntern,
                       KExtern,
                       KType,
                       KDimension,
                       KModulComplex,
                       KModul,
                       KModuleAlias,
                       KOpenOrInclude,
                       KModuleType,
                       KModElement,
                       KTypeDef,
                       KInstruction,
                       KStep,
                       KTransition,
                       KInstanceAct,
                       KConnexion,
                       KExpr,
                       KValuePath,
                       KLiteral,
                       KBlock,
                       KLadder,
                       KAccess,
                       KApply,
                       KFunction,
                       KProcedure,
                       KArraySize,
                       KArray,
                       KArrayAccess,
                       KArrayFill,
                       KTypeExpr,
                       KTypeVar,
                       KTypePath,
                       KTypeArray,
                       KTypeFunction,
                       KTypeBlock,
                       KTypeProcedure,
                       KDimExpr,
                       KDirection,
                       KMapping,
                       KPort,
                       KPattern,
                       KBlockSignal,
                       KBlockPort,
                       KInstancePort,
                       KInstanceActivation,
                       KInstanceReset,
                       KFunParam,
                       KLadderInput,
                       KLadderOutput,
                       KLadderLocal,
                       KLadderRung,
                       KLadderConnexion,
                       KLadderInConnexion,
                       KLadderOutConnexion,
                       KLadderNode,
                       KIdent,
                       KAssocData,
                       KFlow,
                       KEquation};
      
      
      
      
}; 

/*CATDeclareHandler(AUTICsmUnknown, CATBaseUnknown);
//------------------------------------------------------------------
#include "CATLISTHand_Clean.h"
#include "CATLISTHand_AllFunct.h"
#include "CATLISTHand_Declare.h"

#undef	CATCOLLEC_ExportedBy
#define	CATCOLLEC_ExportedBy	ExportedByAUTCsmItfCPP
CATLISTHand_DECLARE(AUTICsmUnknown_var)*/

#endif

