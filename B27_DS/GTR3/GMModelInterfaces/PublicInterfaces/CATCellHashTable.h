/* -*-c++-*- */
// COPYRIGHT DASSAULT SYSTEMES  1999
/** @CAA2Required */ 
/**
* @nodoc
*/
//===========================================================================
//
// Avr. 2002   Migration vers CATCGMHashTable                       HCN
//
//===========================================================================
#ifndef CATCellHashTable_H
#define CATCellHashTable_H

#include "CATCGMHashTable.h"
#include "CATCGMNewArray.h"

class CATCell;

// exporting module
#include  "CATGMModelInterfaces.h"

typedef		int	         (*PFCompareFunction) ( CATCell *, CATCell * );	
typedef		unsigned int (*PFHashKeyFunction) ( CATCell * ) ;


//#define OLD_HASH_TABLE_CODE_CELL

#ifdef OLD_HASH_TABLE_CODE_CELL

//=============================================================================
// ANCIEN CODE
//=============================================================================
/*
class   ExportedByCATGMModelInterfaces   CATCellHashTable
{
public:

  CATCellHashTable ( PFHashKeyFunction iPFH, PFCompareFunction iPFC, int SizeEstimate=0);
  ~CATCellHashTable();
  CATCGMNewClassArrayDeclare;
  
  int               Insert(CATCell*);

	
  CATCell*         Next  (CATCell*) const;

	
  inline CATCell*  Get   (int iPos) const;
  
  void * Locate(CATCell*) const;

	
  CATCell* KeyLocate(unsigned int) const;

	
  int   Remove( CATCell* );

	
  void   RemoveAll();

	
  int    Size() const; 

	
  void   PrintStats();
  
private:
  
  PFCompareFunction	_PFC ;
  PFHashKeyFunction	_PFH ;
  void  ReAllocate();
  int UpToNiceModulo (int);
  
  
  int* AllocatedZone;   // address of the allocated memory  
  int  DimArray;        // dimension of the hashtab (maximum number of bucket to be stored in the Hashtable)
  int  ArrayOrig[93];   // static array (93 = 3*DimArray with DimArray =31) 
  int* LastDirecBloc;   // Address of the first free bucket
  int  NbFreeBucket;	// Number of free buckets 
  int  NbCellStocke;    //  number of bucket stored in the hashtable
  int* OldAllocatedZone;// data used for reallocation (recursive)
  int  OldDimArray;
  int  OldNbCellStocke; 
  
};


inline CATCell*  CATCellHashTable::Get   (int iPos) const
{
  if ( iPos < 0 || iPos >= NbCellStocke)
    return NULL;
  else
    return (CATCell* )  *(AllocatedZone + DimArray +2*iPos);
};
*/
//=============================================================================
// FIN ANCIEN CODE
//=============================================================================
#else
class   ExportedByCATGMModelInterfaces  CATCellHashTable : public CATCGMHashTable
{
public:
  CATCellHashTable (PFHashKeyFunction ipHashKeyFunction, PFCompareFunction ipCompareFunction, int iEstimatedSize = 0);
  ~CATCellHashTable();
  CATCGMNewClassArrayDeclare;
  
  inline int      Insert(CATCell* ipCell);
  inline CATCell* Get(int iPos) const;
  inline void*    Locate(CATCell* ipCell) const;
  inline CATCell* KeyLocate(unsigned int iKey) const; 
  inline int      Remove(CATCell* ipCell);

  CATCell*        Next(CATCell* ipCell) const;
  int             Size() const;
};

//-----------------------------------------------------------------------------
// Insert
//-----------------------------------------------------------------------------
inline int CATCellHashTable::Insert(CATCell* ipCell)
{
  return (this->CATCGMHashTable::Insert((void*)ipCell));
}

//-----------------------------------------------------------------------------
// Get
//-----------------------------------------------------------------------------
inline CATCell* CATCellHashTable::Get(int iPos) const
{
  return ((CATCell*)this->CATCGMHashTable::Get(iPos));
}

//-----------------------------------------------------------------------------
// Locate
//-----------------------------------------------------------------------------
inline void* CATCellHashTable::Locate(CATCell* ipCell) const
{
  return (this->CATCGMHashTable::Locate((void*)ipCell));
}

//-----------------------------------------------------------------------------
// KeyLocate
//-----------------------------------------------------------------------------
inline CATCell* CATCellHashTable::KeyLocate(unsigned int iKey) const
{
  return ((CATCell*)this->CATCGMHashTable::KeyLocate(iKey));
}

//-----------------------------------------------------------------------------
// Remove
//-----------------------------------------------------------------------------
inline int CATCellHashTable::Remove(CATCell* ipCell)
{
  return (this->CATCGMHashTable::Remove((void*)ipCell));
}



#endif


#define CATHashTabCATCell CATCellHashTable

ExportedByCATGMModelInterfaces unsigned int HashKeyFunction( CATCell * iCell );

ExportedByCATGMModelInterfaces int CompareFunction( CATCell * iCell1, CATCell *iCell2 );

#endif 
