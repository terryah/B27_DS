/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: scl_ios.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Oct-2004     Initial implementation.
//
//  NOTICE:
//      This header implements functionality in the Standard C++ Library.
//      It may be included directly in application code.
//
//==============================================================================
#ifndef _SCL_IOS_H_
#define _SCL_IOS_H_


#ifndef  _SCL_CONFIG_H_
#include <scl_config.h>
#endif


#ifdef  SCL_USE_STD_IOSTREAM
#include <ios>

#ifdef  SCL_HAS_NAMESPACES
// types
using SCL_VENDOR_STD::streamoff;
using SCL_VENDOR_STD::streamsize;
using SCL_VENDOR_STD::fpos;
using SCL_VENDOR_STD::streampos;
using SCL_VENDOR_STD::ios_base;
using SCL_VENDOR_STD::ios;
using SCL_VENDOR_STD::streambuf;

// manipulators
using SCL_VENDOR_STD::boolalpha;
using SCL_VENDOR_STD::noboolalpha;
using SCL_VENDOR_STD::showbase;
using SCL_VENDOR_STD::noshowbase;
using SCL_VENDOR_STD::showpoint;
using SCL_VENDOR_STD::noshowpoint;
using SCL_VENDOR_STD::showpos;
using SCL_VENDOR_STD::noshowpos;
using SCL_VENDOR_STD::skipws;
using SCL_VENDOR_STD::noskipws;
using SCL_VENDOR_STD::uppercase;
using SCL_VENDOR_STD::nouppercase;

// adjustfield
using SCL_VENDOR_STD::internal;
using SCL_VENDOR_STD::left;
using SCL_VENDOR_STD::right;

// basefield
using SCL_VENDOR_STD::dec;
using SCL_VENDOR_STD::hex;
using SCL_VENDOR_STD::oct;

// floatfield
using SCL_VENDOR_STD::fixed;
using SCL_VENDOR_STD::scientific;
#endif  /* SCL_HAS_NAMESPACES */

#else
#ifdef  _WINDOWS_SOURCE
#include <ios.h>
#else
#include <iostream.h>
#endif

#endif  /* SCL_USE_STD_IOSTREAM */


#endif  /* _SCL_IOS_H_ */
