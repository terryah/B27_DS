//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview RTL trangara 01:11:13
 */
// --------------------------------------------------------
//*
//* FILE:
//*     StandardLib.cc    - public definition file
//*
//* MODULE:
//*     StandardLib       - collection of global functions
//*
//* OVERVIEW:
//*     This module provides several helper functions for the Standard C++
//*     Library.
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     jad         08/18/97    Initial implementation.
//*     jad         07/13/99    Added conversion routines for vectors and lists.
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1997-99 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*
template <class T>
void
DNBDestroy( T *ptr )
    DNB_THROW_SPEC_ANY
{
    ptr->~T();
}


template <class FwdIt>
void
DNBRelease( FwdIt first, FwdIt last )
    DNB_THROW_SPEC_ANY
{
    while ( first != last )
    {
        DNB_DELETE *first;
        *first = NULL;
        first++;
    }
}


template <class Container>
void
DNBRelease( Container &cont )
    DNB_THROW_SPEC_ANY
{
    DNBRelease( cont.begin(), cont.end() );
}


template <class Item, class Allocator>
void
DNBAssign( scl_vector<Item, Allocator> &dst, const scl_list<Item, Allocator> &src )
    DNB_THROW_SPEC((scl_bad_alloc))
{
    typedef scl_vector<Item, Allocator> Vector;
    typedef scl_list<Item, Allocator>   List;

    dst.clear( );

    dst.reserve( src.size() );

    typename List::const_iterator    iter = src.begin( );
    typename List::const_iterator    last = src.end( );

    for ( ; iter != last; ++iter )
        dst.push_back( *iter );
}


template <class Item, class Allocator>
void
DNBAssign( scl_list<Item, Allocator> &dst, const scl_vector<Item, Allocator> &src )
    DNB_THROW_SPEC((scl_bad_alloc))
{
    typedef scl_vector<Item, Allocator> Vector;
    typedef scl_list<Item, Allocator>   List;

    dst.clear( );

    typename Vector::const_iterator  iter = src.begin( );
    typename Vector::const_iterator  last = src.end( );

    for ( ; iter != last; ++iter )
        dst.push_back( *iter );
}
