/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: sclp_uninitialized.cc
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Dec-2004     Initial implementation.
//      jod     06-Jun-2008     Added optimized specializations of
//                              scl_uninitialized_copy, scl_uninitialized_fill,
//                              and scl_uninitialized_fill_n.
//
//==============================================================================


template <class Type_>
class scl_allocator;


//
//  20.4.4.1 Template function scl_uninitialized_copy
//
template <class InpIter_, class FwdIter_>
FwdIter_
scl_uninitialized_copy(InpIter_ first, InpIter_ last, FwdIter_ result)
{
    FwdIter_ save = result;

    try
    {
        for (; first != last; ++first, ++result)
            _scl_construct(&*result, *first);
    }
    catch (...)
    {
        _scl_destroy(save, result);
        throw;
    }

    return result;
}

template <class InpIter_, class FwdIter_, class Alloc_>
FwdIter_
_scl_uninitialized_copy(InpIter_ first, InpIter_ last, FwdIter_ result, Alloc_& alloc)
{
    FwdIter_ save = result;

    try
    {
        for (; first != last; ++first, ++result)
            alloc.construct(&*result, *first);
    }
    catch (...)
    {
        _scl_destroy(save, result, alloc);
        throw;
    }

    return result;
}


//
//  20.4.4.2 Template function scl_uninitialized_fill
//
template <class FwdIter_, class Type_>
void
scl_uninitialized_fill(FwdIter_ first, FwdIter_ last, const Type_& value)
{
    FwdIter_ save = first;

    try
    {
        for (; first != last; ++first)
            _scl_construct(&*first, value);
    }
    catch (...)
    {
        _scl_destroy(save, first);
        throw;
    }
}

template <class FwdIter_, class Type_, class Alloc_>
void
_scl_uninitialized_fill(FwdIter_ first, FwdIter_ last, const Type_& value, Alloc_& alloc)
{
    FwdIter_ save = first;

    try
    {
        for (; first != last; ++first)
            alloc.construct(&*first, value);
    }
    catch (...)
    {
        _scl_destroy(save, first, alloc);
        throw;
    }
}


//
//  20.4.4.3 Template function scl_uninitialized_fill_n
//
template <class FwdIter_, class Size_, class Type_>
void
scl_uninitialized_fill_n(FwdIter_ first, Size_ len, const Type_& value)
{
    FwdIter_ save = first;

    try
    {
        for (; len > 0; --len, ++first)
            _scl_construct(&*first, value);
    }
    catch (...)
    {
        _scl_destroy(save, first);
        throw;
    }
}

template <class FwdIter_, class Size_, class Type_, class Alloc_>
void
_scl_uninitialized_fill_n(FwdIter_ first, Size_ len, const Type_& value, Alloc_& alloc)
{
    FwdIter_ save = first;

    try
    {
        for (; len > 0; --len, ++first)
            alloc.construct(&*first, value);
    }
    catch (...)
    {
        _scl_destroy(save, first, alloc);
        throw;
    }
}


//
//  Define optimized specializations of scl_uninitialized_copy, scl_uninitialized_fill,
//  and scl_uninitialized_fill_n for scalar types.
//
#define SCL_SPECIALIZE_UCOPY(Type_)                                             \
SCL_SPECIALIZE_FUNC inline Type_*                                               \
scl_uninitialized_copy(const Type_* first, const Type_* last, Type_* result)    \
{                                                                               \
    return _scl_scalar_copy(result, first, last - first);                       \
}                                                                               \
SCL_SPECIALIZE_FUNC inline Type_*                                               \
scl_uninitialized_copy(      Type_* first,       Type_* last, Type_* result)    \
{                                                                               \
    return _scl_scalar_copy(result, first, last - first);                       \
}                                                                               \
SCL_SPECIALIZE_FUNC inline Type_*                                               \
_scl_uninitialized_copy(const Type_* first, const Type_* last, Type_* result,   \
    scl_allocator<Type_>&)                                                      \
{                                                                               \
    return _scl_scalar_copy(result, first, last - first);                       \
}                                                                               \
SCL_SPECIALIZE_FUNC inline Type_*                                               \
_scl_uninitialized_copy(      Type_* first,       Type_* last, Type_* result,   \
    scl_allocator<Type_>&)                                                      \
{                                                                               \
    return _scl_scalar_copy(result, first, last - first);                       \
}                                                                               \
SCL_SPECIALIZE_FUNC inline void                                                 \
scl_uninitialized_fill(Type_* first, Type_* last, const Type_& value)           \
{                                                                               \
    _scl_scalar_fill(first, value, last - first);                               \
}                                                                               \
SCL_SPECIALIZE_FUNC inline void                                                 \
_scl_uninitialized_fill(Type_* first, Type_* last, const Type_& value,          \
    scl_allocator<Type_>&)                                                      \
{                                                                               \
    _scl_scalar_fill(first, value, last - first);                               \
}                                                                               \
SCL_SPECIALIZE_FUNC inline void                                                 \
scl_uninitialized_fill_n(Type_* first, size_t len, const Type_& value)          \
{                                                                               \
    _scl_scalar_fill(first, value, len);                                        \
}                                                                               \
SCL_SPECIALIZE_FUNC inline void                                                 \
_scl_uninitialized_fill_n(Type_* first, size_t len, const Type_& value,         \
    scl_allocator<Type_>&)                                                      \
{                                                                               \
    _scl_scalar_fill(first, value, len);                                        \
}

SCL_SPECIALIZE_UCOPY(char)
SCL_SPECIALIZE_UCOPY(signed char)
SCL_SPECIALIZE_UCOPY(unsigned char)

#ifdef  SCL_HAS_DISTINCT_WCHAR
SCL_SPECIALIZE_UCOPY(wchar_t)
#endif

SCL_SPECIALIZE_UCOPY(short)
SCL_SPECIALIZE_UCOPY(unsigned short)

SCL_SPECIALIZE_UCOPY(int)
SCL_SPECIALIZE_UCOPY(unsigned int)

SCL_SPECIALIZE_UCOPY(long)
SCL_SPECIALIZE_UCOPY(unsigned long)

#ifdef  SCL_HAS_LLONG
SCL_SPECIALIZE_UCOPY(SCL_LLONG_TYPE)
SCL_SPECIALIZE_UCOPY(unsigned SCL_LLONG_TYPE)
#endif

#ifdef  SCL_HAS_DISTINCT_BOOL
SCL_SPECIALIZE_UCOPY(bool)
#endif

SCL_SPECIALIZE_UCOPY(float)
SCL_SPECIALIZE_UCOPY(double)
SCL_SPECIALIZE_UCOPY(long double)

#undef  SCL_SPECIALIZE_UCOPY
