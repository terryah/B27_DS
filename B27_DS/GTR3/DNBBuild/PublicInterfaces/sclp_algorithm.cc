/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: sclp_algorithm.cc
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Aug-2004     Initial implementation.
//
//==============================================================================


//
//  25.1 Non-Modifying Sequence Operations
//
template <class InpIter_, class Func_>
Func_
scl_for_each(InpIter_ first, InpIter_ last, Func_ func)
{
    for (; first != last; ++first)
        func(*first);

    return func;
}

template <class InpIter_, class Type_>
InpIter_
scl_find(InpIter_ first, InpIter_ last, const Type_& value)
{
    while (first != last && !(*first == value))
        ++first;

    return first;
}

template <class InpIter_, class Pred_>
InpIter_
scl_find_if(InpIter_ first, InpIter_ last, Pred_ pred)
{
    while (first != last && !pred(*first))
        ++first;

    return first;
}

template <class FwdIter_>
FwdIter_
scl_adjacent_find(FwdIter_ first, FwdIter_ last)
{
    if (first == last)
        return last;

    FwdIter_    iter = first;
    for (++iter; iter != last; ++iter)
    {
        if (*first == *iter)
            return first;
        first = iter;
    }

    return last;
}

template <class FwdIter_, class Pred_>
FwdIter_
scl_adjacent_find(FwdIter_ first, FwdIter_ last, Pred_ pred)
{
    if (first == last)
        return last;

    FwdIter_    iter = first;
    for (++iter; iter != last; ++iter)
    {
        if (pred(*first, *iter))
            return first;
        first = iter;
    }

    return last;
}


//
//  25.2 Modifying Sequence Operations
//
template <class FwdIter1_, class FwdIter2_>
FwdIter2_
scl_swap_ranges(FwdIter1_ first, FwdIter1_ last, FwdIter2_ result)
{
    for (; first != last; ++first, ++result)
        scl_iter_swap(first, result);

    return result;
}

template <class InpIter_, class OutIter_, class UnaOp_>
OutIter_
scl_transform(InpIter_ first, InpIter_ last, OutIter_ result, UnaOp_ oper)
{
    for (; first != last; ++first, ++result)
        *result = oper(*first);
    return result;
}

template <class InpIter1_, class InpIter2_, class OutIter_, class BinOp_>
OutIter_
scl_transform(InpIter1_ first1, InpIter1_ last1, InpIter2_ first2, OutIter_ result, BinOp_ oper)
{
    for (; first1 != last1; ++first1, ++first2, ++result)
        *result = oper(*first1, *first2);
    return result;
}

template <class FwdIter_>
FwdIter_
scl_unique(FwdIter_ first, FwdIter_ last)
{
    first = scl_adjacent_find(first, last);
    if (first == last)
        return last;

    FwdIter_    iter = first;
    ++first;
    while (++first != last)
    {
        if (!(*iter == *first))
            *++iter = *first;
    }

    return ++iter;
}

template <class FwdIter_, class Pred_>
FwdIter_
scl_unique(FwdIter_ first, FwdIter_ last, Pred_ pred)
{
    first = scl_adjacent_find(first, last, pred);
    if (first == last)
        return last;

    FwdIter_    iter = first;
    ++first;
    while (++first != last)
    {
        if (!pred(*iter, *first))
            *++iter = *first;
    }

    return ++iter;
}

template <class BidIter_>
void
_scl_reverse(BidIter_ first, BidIter_ last, const scl_bidirectional_iterator_tag&)
{
    for (; first != last && first != --last; ++first)
        scl_iter_swap(first, last);
}

template <class RanIter_>
void
_scl_reverse(RanIter_ first, RanIter_ last, const scl_random_access_iterator_tag&)
{
    for (; first < last; ++first)
        scl_iter_swap(first, --last);
}

template <class BidIter_>
void
scl_reverse(BidIter_ first, BidIter_ last)
{
    _scl_reverse(first, last, _scl_category(first));
}

template <class BidIter_, class OutIter_>
OutIter_
scl_reverse_copy(BidIter_ first, BidIter_ last, OutIter_ result)
{
    for (; first != last; ++result)
        *result = *--last;
    return result;
}

// This function returns the greatest common divisor of two integer values.
template <class Ring_>
Ring_
_scl_gcd(Ring_ val1, Ring_ val2)
{
    SCL_PRECONDITION(val1 >= val2);

    while (val2 != 0)
    {
        Ring_ tmp = val1 % val2;
        val1 = val2;
        val2 = tmp;
    }

    return val1;
}

template <class FwdIter_>
void
_scl_rotate(FwdIter_ first, FwdIter_ middle, FwdIter_ last,
    const scl_forward_iterator_tag&)
{
    FwdIter_    first2 = middle;

    do
    {
        scl_iter_swap(first, first2);
        ++first;
        ++first2;
        if (first == middle)
            middle = first2;
    }
    while (first2 != last);

    first2 = middle;

    while (first2 != last)
    {
        scl_iter_swap(first, first2);
        ++first;
        ++first2;
        if (first == middle)
            middle = first2;
        else if (first2 == last)
            first2 = middle;
    }
}

template <class BidIter_>
void
_scl_rotate(BidIter_ first, BidIter_ middle, BidIter_ last,
    const scl_bidirectional_iterator_tag& tag)
{
    _scl_reverse(first , middle, tag);
    _scl_reverse(middle, last  , tag);

    while (first != middle && middle != last)
    {
        scl_iter_swap(first, --last);
        ++first;
    }

    if (first == middle)
        _scl_reverse(middle, last  , tag);
    else
        _scl_reverse(first , middle, tag);
}

template <class RanIter_, class Dist_, class Type_>
void
_scl_rotate_ran(RanIter_ first, RanIter_ middle, RanIter_ last, Dist_*, Type_*)
{
    const Dist_ dist = last   - first;
    const Dist_ seg1 = middle - first;
    const Dist_ seg2 = dist   - seg1;

    if (seg1 == seg2)
    {
        scl_swap_ranges(first, middle, middle);
        return;
    }

    const Dist_ gcd = _scl_gcd(dist, seg1);

    for (Dist_ idx = 0; idx < gcd; idx++)
    {
        Type_    tmp  = *first;
        RanIter_ iter = first;

        if (seg1 < seg2)
        {
            for (Dist_ jdx = 0; jdx < seg2 / gcd; jdx++)
            {
                if (iter > first + seg2)
                {
                    *iter = *(iter - seg2);
                    iter -= seg2;
                }

                *iter = *(iter + seg1);
                iter += seg1;
            }
        }
        else
        {
            for (Dist_ jdx = 0; jdx < seg1 / gcd - 1; jdx++)
            {
                if (iter < last - seg1)
                {
                    *iter = *(iter + seg1);
                    iter += seg1;
                }
                *iter = *(iter - seg2);
                iter -= seg2;
            }
        }

        *iter = tmp;
        ++first;
    }
}

template <class RanIter_>
void
_scl_rotate(RanIter_ first, RanIter_ middle, RanIter_ last,
    const scl_random_access_iterator_tag&)
{
    _scl_rotate_ran(first, middle, last, _scl_distance_type(first),
        _scl_value_type(first));
}

template <class FwdIter_>
void
scl_rotate(FwdIter_ first, FwdIter_ middle, FwdIter_ last)
{
    if (first == middle || middle == last)
        return;

    _scl_rotate(first, middle, last, _scl_category(first));
}


//
//  25.3 Sorting and Related Operations
//
template <class RanIter_>
void
scl_sort(RanIter_ first, RanIter_ last)
{
    // FIXME: Use a more efficient algorithm.
    scl_make_heap(first, last);
    scl_sort_heap(first, last);
}

template <class RanIter_, class Comp_>
void
scl_sort(RanIter_ first, RanIter_ last, Comp_ comp)
{
    // FIXME: Use a more efficient algorithm.
    scl_make_heap(first, last, comp);
    scl_sort_heap(first, last, comp);
}

template <class RanIter_, class Type_>
void
_scl_linear_insert(RanIter_ last, Type_ value)
{
    RanIter_    next = last;
    --next;

    while (value < *next)
    {
        *last = *next;
         last =  next;
        --next;
    }

    *last = value;
}

template <class RanIter_, class Type_, class Comp_>
void
_scl_linear_insert(RanIter_ last, Type_ value, Comp_ comp)
{
    RanIter_    next = last;
    --next;

    while (comp(value, *next))
    {
        *last = *next;
         last =  next;
        --next;
    }

    *last = value;
}

template <class RanIter_, class Type_>
void
_scl_insertion_sort(RanIter_ first, RanIter_ last, Type_*)
{
    for (RanIter_ iter = first + 1; iter != last; ++iter)
    {
        Type_   value = *iter;
        if (value < *first)
        {
            scl_copy_backward(first, iter, iter + 1);
            *first = value;
        }
        else
        {
            _scl_linear_insert(iter, value);
        }
    }
}

template <class RanIter_, class Comp_, class Type_>
void
_scl_insertion_sort(RanIter_ first, RanIter_ last, Comp_ comp, Type_*)
{
    for (RanIter_ iter = first + 1; iter != last; ++iter)
    {
        Type_   value = *iter;
        if (comp(value, *first))
        {
            scl_copy_backward(first, iter, iter + 1);
            *first = value;
        }
        else
        {
            _scl_linear_insert(iter, value, comp);
        }
    }
}

template <class RanIter_>
void
scl_stable_sort(RanIter_ first, RanIter_ last)
{
    if (first == last)
        return;

    // FIXME: Use a more efficient algorithm.
    _scl_insertion_sort(first, last, _scl_value_type(first));
}

template <class RanIter_, class Comp_>
void
scl_stable_sort(RanIter_ first, RanIter_ last, Comp_ comp)
{
    if (first == last)
        return;

    // FIXME: Use a more efficient algorithm.
    _scl_insertion_sort(first, last, comp, _scl_value_type(first));
}

template <class FwdIter_, class Type_>
FwdIter_
scl_lower_bound(FwdIter_ first, FwdIter_ last, const Type_& value)
{
    typedef _SCL_DIFFERENCE_TYPE(FwdIter_)  Dist_;
    Dist_   dist = scl_distance(first, last);

    while (dist > 0)
    {
        const Dist_ half = dist / 2;
        FwdIter_    iter = first;
        scl_advance(iter, half);

        if (*iter < value)
        {
            first = ++iter;
            dist -= half + 1;
        }
        else
        {
            dist  = half;
        }
    }

    return first;
}

template <class FwdIter_, class Type_, class Comp_>
FwdIter_
scl_lower_bound(FwdIter_ first, FwdIter_ last, const Type_& value, Comp_ comp)
{
    typedef _SCL_DIFFERENCE_TYPE(FwdIter_)  Dist_;
    Dist_   dist = scl_distance(first, last);

    while (dist > 0)
    {
        const Dist_ half = dist / 2;
        FwdIter_    iter = first;
        scl_advance(iter, half);

        if (comp(*iter, value))
        {
            first = ++iter;
            dist -= half + 1;
        }
        else
        {
            dist  = half;
        }
    }

    return first;
}

template <class FwdIter_, class Type_>
FwdIter_
scl_upper_bound(FwdIter_ first, FwdIter_ last, const Type_& value)
{
    typedef _SCL_DIFFERENCE_TYPE(FwdIter_)  Dist_;
    Dist_   dist = scl_distance(first, last);

    while (dist > 0)
    {
        const Dist_ half = dist / 2;
        FwdIter_    iter = first;
        scl_advance(iter, half);

        if (value < *iter)
        {
            dist  = half;
        }
        else
        {
            first = ++iter;
            dist -= half + 1;
        }
    }

    return first;
}

template <class FwdIter_, class Type_, class Comp_>
FwdIter_
scl_upper_bound(FwdIter_ first, FwdIter_ last, const Type_& value, Comp_ comp)
{
    typedef _SCL_DIFFERENCE_TYPE(FwdIter_)  Dist_;
    Dist_   dist = scl_distance(first, last);

    while (dist > 0)
    {
        const Dist_ half = dist / 2;
        FwdIter_    iter = first;
        scl_advance(iter, half);

        if (comp(value, *iter))
        {
            dist  = half;
        }
        else
        {
            first = ++iter;
            dist -= half + 1;
        }
    }

    return first;
}
