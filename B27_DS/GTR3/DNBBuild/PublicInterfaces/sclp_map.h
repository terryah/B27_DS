/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: sclp_map.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-May-2005     Initial implementation.
//
//  NOTICE:
//      This header is used internally by the Standard C++ Library.
//      It should not be included directly in application code.
//
//==============================================================================
#ifndef _SCLP_MAP_H_
#define _SCLP_MAP_H_


//
//  Include section
//
#ifndef  _SCLP_ALLOCATOR_H_
#include <sclp_allocator.h>             // for scl_allocator
#endif

#ifndef  _SCLP_FUNCTIONAL_H_
#include <sclp_functional.h>            // for scl_less, scl_select1st
#endif

#ifndef  _SCLP_RBTREE_H_
#include <sclp_rbtree.h>                // for scl_rbtree
#endif

#ifndef  _SCLP_BASE_ALGO_H_
#include <sclp_base_algo.h>             // for scl_equal(), scl_lexicographical_compare()
#endif


//
//  Template class scl_map
//
#ifdef  SCL_HAS_DEFAULT_TMPL_ARGS
template <class Key_, class Value_, class Comp_ = scl_less<Key_>,
    class Alloc_ = scl_allocator<SCL_PAIR(Key_, Value_) > >
#else
template <class Key_, class Value_, class Comp_, class Alloc_>
#endif
class scl_map
{
public:
    // Private types
    typedef scl_map<Key_, Value_, Comp_, Alloc_>        Self_;
    typedef SCL_PAIR(Key_, Value_)                      Pair_;
    typedef scl_rbtree<Key_, Pair_,
        scl_select1st<Pair_>, Comp_, Alloc_>            Tree_;
    enum { Multi_ = false };

    // Public types
    typedef Key_                                        key_type;
    typedef Value_                                      mapped_type;
    typedef Pair_                                       value_type;
    typedef Comp_                                       key_compare;
    typedef Alloc_                                      allocator_type;
    typedef typename Alloc_::pointer                    pointer;
    typedef typename Alloc_::const_pointer              const_pointer;
    typedef typename Alloc_::reference                  reference;
    typedef typename Alloc_::const_reference            const_reference;
    typedef typename Alloc_::size_type                  size_type;
    typedef typename Alloc_::difference_type            difference_type;

    typedef typename Tree_::iterator                    iterator;
    typedef typename Tree_::const_iterator              const_iterator;
    typedef typename Tree_::reverse_iterator            reverse_iterator;
    typedef typename Tree_::const_reverse_iterator      const_reverse_iterator;

    class value_compare :
        public scl_binary_function<value_type, value_type, bool>
    {
    public:
        value_compare(key_compare cmp) : comp(cmp) {}

        bool    operator()(const value_type& lhs, const value_type& rhs) const
        {
            return comp(lhs.first, rhs.first);
        }

    protected:
        key_compare     comp;
    };

    // 23.3.1.1 construct/copy/destroy
    scl_map() :
        tree_(Multi_)
    {
        // Nothing
    }

    explicit
    scl_map(const Comp_& comp) :
        tree_(Multi_, comp)
    {
        // Nothing
    }

    scl_map(const Comp_& comp, const Alloc_& alloc) :
        tree_(Multi_, comp, alloc)
    {
        // Nothing
    }

#ifdef  SCL_HAS_MEMBER_TEMPLATES
    template <class InputIterator_>
    scl_map(InputIterator_ first, InputIterator_ last) :
        tree_(Multi_)
    {
        tree_.insert(first, last);
    }

    template <class InputIterator_>
    scl_map(InputIterator_ first, InputIterator_ last, const Comp_& comp) :
        tree_(Multi_, comp)
    {
        tree_.insert(first, last);
    }

    template <class InputIterator_>
    scl_map(InputIterator_ first, InputIterator_ last, const Comp_& comp, const Alloc_& alloc) :
        tree_(Multi_, comp, alloc)
    {
        tree_.insert(first, last);
    }
#else
    scl_map(const_iterator first, const_iterator last) :
        tree_(Multi_)
    {
        tree_.insert(first, last);
    }

    scl_map(const_iterator first, const_iterator last, const Comp_& comp) :
        tree_(Multi_, comp)
    {
        tree_.insert(first, last);
    }

    scl_map(const_iterator first, const_iterator last, const Comp_& comp, const Alloc_& alloc) :
        tree_(Multi_, comp, alloc)
    {
        tree_.insert(first, last);
    }

    scl_map(const_pointer  first, const_pointer  last) :
        tree_(Multi_)
    {
        tree_.insert(first, last);
    }

    scl_map(const_pointer  first, const_pointer  last, const Comp_& comp) :
        tree_(Multi_, comp)
    {
        tree_.insert(first, last);
    }

    scl_map(const_pointer  first, const_pointer  last, const Comp_& comp, const Alloc_& alloc) :
        tree_(Multi_, comp, alloc)
    {
        tree_.insert(first, last);
    }
#endif

    scl_map(const Self_& other) :
        tree_(other.tree_)
    {
        // Nothing
    }

    ~scl_map()                                          { }
    Self_&          operator=(const Self_& other)       { tree_ = other.tree_; return *this;}
    allocator_type  get_allocator() const               { return tree_.get_allocator();     }

    // iterators
    iterator        begin()                             { return tree_.begin();             }
    const_iterator  begin() const                       { return tree_.begin();             }
    iterator        end()                               { return tree_.end();               }
    const_iterator  end() const                         { return tree_.end();               }

    reverse_iterator        rbegin()                    { return tree_.rbegin();            }
    const_reverse_iterator  rbegin() const              { return tree_.rbegin();            }
    reverse_iterator        rend()                      { return tree_.rend();              }
    const_reverse_iterator  rend() const                { return tree_.rend();              }

    // capacity
    bool            empty() const                       { return tree_.empty();             }
    size_type       size() const                        { return tree_.size();              }
    size_type       max_size() const                    { return tree_.max_size();          }

    // 23.3.1.2 element access:
    mapped_type&    operator[](const key_type& key)     { return find_or_insert(key);       }

    // modifiers
    scl_pair<iterator, bool>
    insert(const value_type& value)                     { return tree_.insert(value);       }

    iterator
    insert(iterator pos, const value_type& value)       { return tree_.insert(pos, value);  }

#ifdef  SCL_HAS_MEMBER_TEMPLATES
    template <class InputIterator_>
    void
    insert(InputIterator_ first, InputIterator_ last)   { tree_.insert(first, last);        }
#else
    void
    insert(const_iterator first, const_iterator last)   { tree_.insert(first, last);        }

    void
    insert(const_pointer  first, const_pointer  last)   { tree_.insert(first, last);        }
#endif

    void            erase(iterator pos)                 { tree_.erase(pos);                 }
    size_type       erase(const Key_& key)              { return tree_.erase(key);          }
    void            erase(iterator first, iterator last){ tree_.erase(first, last);         }
    void            swap(Self_& other)                  { tree_.swap(other.tree_);          }
    void            clear()                             { tree_.clear();                    }

    // observers
    key_compare     key_comp() const                    { return tree_.key_comp();          }

    value_compare   value_comp() const
    {
        return value_compare(tree_.key_comp());
    }

    // 23.3.1.3 map operations
    iterator        find(const Key_& key)               { return tree_.find(key);           }
    const_iterator  find(const Key_& key) const         { return tree_.find(key);           }
    size_type       count(const Key_& key) const        { return tree_.count(key);          }
    iterator        lower_bound(const Key_& key)        { return tree_.lower_bound(key);    }
    const_iterator  lower_bound(const Key_& key) const  { return tree_.lower_bound(key);    }
    iterator        upper_bound(const Key_& key)        { return tree_.upper_bound(key);    }
    const_iterator  upper_bound(const Key_& key) const  { return tree_.upper_bound(key);    }

    scl_pair<iterator, iterator>
    equal_range(const Key_& key)                        { return tree_.equal_range(key);    }

    scl_pair<const_iterator, const_iterator>
    equal_range(const Key_& key) const                  { return tree_.equal_range(key);    }

    // Test class invariants (nonstandard method).
    bool            _validate() const                   { return tree_._validate();         }

protected:
    mapped_type&    find_or_insert(const key_type& key)
    {
        iterator    iter = lower_bound(key);

        // iter->first is greater than or equal to key.
        if (iter == end() || key_comp()(key, (*iter).first))
        {
            Value_  value = Value_();           // Properly initializes POD types.
            iter = insert(iter, Pair_(key, value));
        }

        return (*iter).second;
    }

    Tree_           tree_;      // Red-black tree with unique keys.
};


template <class Key_, class Value_, class Comp_, class Alloc_>
bool
operator==(const scl_map<Key_, Value_, Comp_, Alloc_>& lhs,
           const scl_map<Key_, Value_, Comp_, Alloc_>& rhs)
{
    return (lhs.size() == rhs.size()) && scl_equal(lhs.begin(), lhs.end(), rhs.begin());
}

template <class Key_, class Value_, class Comp_, class Alloc_>
bool
operator< (const scl_map<Key_, Value_, Comp_, Alloc_>& lhs,
           const scl_map<Key_, Value_, Comp_, Alloc_>& rhs)
{
    return scl_lexicographical_compare(lhs.begin(), lhs.end(), rhs.begin(), rhs.end());
}

template <class Key_, class Value_, class Comp_, class Alloc_>
bool
operator!=(const scl_map<Key_, Value_, Comp_, Alloc_>& lhs,
           const scl_map<Key_, Value_, Comp_, Alloc_>& rhs)
{
    return !(lhs == rhs);
}

template <class Key_, class Value_, class Comp_, class Alloc_>
bool
operator<=(const scl_map<Key_, Value_, Comp_, Alloc_>& lhs,
           const scl_map<Key_, Value_, Comp_, Alloc_>& rhs)
{
    return !(rhs < lhs);
}

template <class Key_, class Value_, class Comp_, class Alloc_>
bool
operator>=(const scl_map<Key_, Value_, Comp_, Alloc_>& lhs,
           const scl_map<Key_, Value_, Comp_, Alloc_>& rhs)
{
    return !(lhs < rhs);
}

template <class Key_, class Value_, class Comp_, class Alloc_>
bool
operator> (const scl_map<Key_, Value_, Comp_, Alloc_>& lhs,
           const scl_map<Key_, Value_, Comp_, Alloc_>& rhs)
{
    return (rhs < lhs);
}

#ifdef  SCL_HAS_PARTIAL_SPEC_OVERLOAD
template <class Key_, class Value_, class Comp_, class Alloc_>
void
scl_swap(scl_map<Key_, Value_, Comp_, Alloc_>& lhs,
         scl_map<Key_, Value_, Comp_, Alloc_>& rhs)
{
    lhs.swap(rhs);
}
#endif


#endif  /* _SCLP_MAP_H_ */
