/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: sclp_base_iter.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Oct-2004     Initial implementation.
//
//  NOTICE:
//      This header is used internally by the Standard C++ Library.
//      It should not be included directly in application code.
//
//==============================================================================
#ifndef _SCLP_BASE_ITER_H_
#define _SCLP_BASE_ITER_H_


//
//  24.3.1 Iterator traits
//
template <class Iter_>
struct scl_iterator_traits
{
    typedef typename Iter_::iterator_category   iterator_category;
    typedef typename Iter_::value_type          value_type;
    typedef typename Iter_::difference_type     difference_type;
    typedef typename Iter_::pointer             pointer;
    typedef typename Iter_::reference           reference;
};


#ifdef  SCL_HAS_PARTIAL_SPEC
#define _SCL_DIFFERENCE_TYPE(Iter_) typename scl_iterator_traits<Iter_>::difference_type
#else
#define _SCL_DIFFERENCE_TYPE(Iter_) ptrdiff_t
#endif


//
//  24.3.2 Basic iterator
//
template <class ICat_,
          class Type_,
          class Dist_ SCL_DEFAULT_ARG(ptrdiff_t),
          class TPtr_ SCL_DEFAULT_ARG(Type_*),
          class TRef_ SCL_DEFAULT_ARG(Type_&) >
struct scl_iterator
{
    typedef ICat_   iterator_category;
    typedef Type_   value_type;
    typedef Dist_   difference_type;
    typedef TPtr_   pointer;
    typedef TRef_   reference;
};


//
//  24.3.3 Standard iterator tags
//
struct scl_input_iterator_tag   {};
struct scl_output_iterator_tag  {};
struct scl_forward_iterator_tag       : public scl_input_iterator_tag         {};
struct scl_bidirectional_iterator_tag : public scl_forward_iterator_tag       {};
struct scl_random_access_iterator_tag : public scl_bidirectional_iterator_tag {};

// Distinguishes integer types from iterators.
struct _scl_integral_iterator_tag {};


//
//  24.3.4 Iterator operations
//
template <class Iter_, class Dist_>
void
scl_advance(Iter_& iter, Dist_ dist);

template <class Iter_>
_SCL_DIFFERENCE_TYPE(Iter_)
scl_distance(Iter_ first, Iter_ last);


//
//  24.4.1 Reverse iterators
//
template <class Iter_>
class scl_reverse_iterator : public scl_iterator<
    typename scl_iterator_traits<Iter_>::iterator_category,
    typename scl_iterator_traits<Iter_>::value_type,
    typename scl_iterator_traits<Iter_>::difference_type,
    typename scl_iterator_traits<Iter_>::pointer,
    typename scl_iterator_traits<Iter_>::reference>
{
public:
    typedef scl_reverse_iterator<Iter_>         Self_;
    typedef scl_iterator_traits<Iter_>          Traits_;
    typedef Iter_                               iterator_type;

    // typedefs
    typedef typename Traits_::iterator_category iterator_category;
    typedef typename Traits_::value_type        value_type;
    typedef typename Traits_::difference_type   difference_type;
    typedef typename Traits_::pointer           pointer;
    typedef typename Traits_::reference         reference;

    // construction, assignment, destruction
    scl_reverse_iterator() :
        current()
    {
        // Nothing
    }

    explicit
    scl_reverse_iterator(Iter_ iter) :
        current(iter)
    {
        // Nothing
    }

    scl_reverse_iterator(const Self_& other) :
        current(other.current)
    {
        // Nothing
    }

    Self_& operator=(const Self_& other)
    {
        current = other.current;
        return *this;
    }

#ifdef  SCL_HAS_MEMBER_TEMPLATES
    template <class Other_>
    scl_reverse_iterator(const scl_reverse_iterator<Other_>& other) :
        current(other.base())
    {
        // Nothing
    }

    template <class Other_>
    Self_& operator=(const scl_reverse_iterator<Other_>& other)
    {
        current = other.base();
        return *this;
    }
#endif

    ~scl_reverse_iterator()
    {
        // Nothing
    }

    Iter_ base() const
    {
        return current;
    }

    // forward iterator requirements
    reference operator*() const
    {
        Iter_   tmp(current);
        --tmp;
        return *tmp;
    }

    pointer operator->() const
    {
        Iter_   tmp(current);
        --tmp;
        return &*tmp;
    }

    Self_& operator++()
    {
        --current;
        return *this;
    }

    Self_  operator++(int)
    {
        Self_   tmp(*this);
        --current;
        return tmp;
    }

    // Bidirectional iterator requirements
    Self_& operator--()
    {
        ++current;
        return *this;
    }

    Self_  operator--(int)
    {
        Self_   tmp(*this);
        ++current;
        return tmp;
    }

    // Random access iterator requirements
    Self_  operator+ (difference_type rhs) const
    {
        return Self_(current - rhs);
    }

    Self_& operator+=(difference_type rhs)
    {
        current -= rhs;
        return *this;
    }

    Self_  operator- (difference_type rhs) const
    {
        return Self_(current + rhs);
    }

    Self_& operator-=(difference_type rhs)
    {
        current += rhs;
        return *this;
    }

    reference operator[](difference_type rhs) const
    {
        return *(*this + rhs);
    }

    // The following data-member name is explicitly defined by the Standard.
protected:
    Iter_   current;
};


template <class Iter_>
bool
operator==(const scl_reverse_iterator<Iter_> &lhs,
           const scl_reverse_iterator<Iter_> &rhs)
{
    return (lhs.base() == rhs.base());
}

template <class Iter_>
bool
operator< (const scl_reverse_iterator<Iter_> &lhs,
           const scl_reverse_iterator<Iter_> &rhs)
{
    return (lhs.base() >  rhs.base());
}

template <class Iter_>
bool
operator!=(const scl_reverse_iterator<Iter_> &lhs,
           const scl_reverse_iterator<Iter_> &rhs)
{
    return (lhs.base() != rhs.base());
}

template <class Iter_>
bool
operator> (const scl_reverse_iterator<Iter_> &lhs,
           const scl_reverse_iterator<Iter_> &rhs)
{
    return (lhs.base() <  rhs.base());
}

template <class Iter_>
bool
operator>=(const scl_reverse_iterator<Iter_> &lhs,
           const scl_reverse_iterator<Iter_> &rhs)
{
    return (lhs.base() <= rhs.base());
}

template <class Iter_>
bool
operator<=(const scl_reverse_iterator<Iter_> &lhs,
           const scl_reverse_iterator<Iter_> &rhs)
{
    return (lhs.base() >= rhs.base());
}

template <class Iter_>
typename scl_reverse_iterator<Iter_>::difference_type
operator- (const scl_reverse_iterator<Iter_> &lhs,
           const scl_reverse_iterator<Iter_> &rhs)
{
    return (rhs.base() - lhs.base());
}

template <class Iter_>
scl_reverse_iterator<Iter_>
operator+(typename scl_reverse_iterator<Iter_>::difference_type lhs,
          const scl_reverse_iterator<Iter_> &rhs)
{
    return scl_reverse_iterator<Iter_>(rhs.base() - lhs);
}


#include <sclp_base_iter.cc>


#endif  /* _SCLP_BASE_ITER_H_ */
