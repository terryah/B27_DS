/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: scl_cwchar.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Aug-2004     Initial implementation.
//
//  NOTICE:
//      This header implements functionality in the Standard C++ Library.
//      It may be included directly in application code.
//
//==============================================================================
#ifndef _SCL_CWCHAR_H_
#define _SCL_CWCHAR_H_


#ifndef  _SCL_CONFIG_H_
#include <scl_config.h>
#endif

#include <wchar.h>


#ifdef  SCL_HAS_WSTR_FUNCTIONS

#define scl_wmemcpy             ::wmemcpy
#define scl_wmemmove            ::wmemmove
#define scl_wmemset             ::wmemset
#define scl_wmemcmp             ::wmemcmp
#define scl_wmemchr             ::wmemchr

#else

SCL_STD_EXPORT  wchar_t*
scl_wmemcpy(wchar_t* dst, const wchar_t* src, size_t len);

SCL_STD_EXPORT  wchar_t*
scl_wmemmove(wchar_t *dst, const wchar_t *src, size_t len);

SCL_STD_EXPORT  wchar_t*
scl_wmemset(wchar_t *dst, wchar_t src, size_t len);

SCL_STD_EXPORT  int
scl_wmemcmp(const wchar_t *lhs, const wchar_t *rhs, size_t len);

SCL_STD_EXPORT  const wchar_t*
scl_wmemchr(const wchar_t *lhs, wchar_t rhs, size_t len);

#endif  /* SCL_HAS_WSTR_FUNCTIONS */


#ifndef WCHAR_MIN
#define WCHAR_MIN       ((wchar_t) 0)           // Assume wchar_t is unsigned
#endif

#ifndef WCHAR_MAX
#define WCHAR_MAX       (~WCHAR_MIN)            // Assume wchar_t is unsigned
#endif


#endif  /* _SCL_CWCHAR_H_ */
