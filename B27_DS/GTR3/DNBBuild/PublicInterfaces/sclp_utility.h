/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: sclp_utility.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Aug-2004     Initial implementation.
//
//  NOTICE:
//      This header is used internally by the Standard C++ Library.
//      It should not be included directly in application code.
//
//==============================================================================
#ifndef _SCLP_UTILITY_H_
#define _SCLP_UTILITY_H_


//
//  20.2.1 Template relational operators
//
#ifdef  SCL_USE_STD_RELOPS

template <class Type_>
bool
operator!=(const Type_& lhs, const Type_& rhs)
{
    return !(lhs == rhs);
}

template <class Type_>
bool
operator> (const Type_& lhs, const Type_& rhs)
{
    return (rhs < lhs);
}

template <class Type_>
bool
operator<=(const Type_& lhs, const Type_& rhs)
{
    return !(rhs < lhs);
}

template <class Type_>
bool
operator>=(const Type_& lhs, const Type_& rhs)
{
    return !(lhs < rhs);
}

#endif  /* SCL_USE_STD_RELOPS */


//
//  20.2.2 scl_pair
//
template <class Type1_, class Type2_>
struct scl_pair
{
    typedef scl_pair<Type1_, Type2_>    Self_;
    typedef Type1_                      first_type;
    typedef Type2_                      second_type;

    scl_pair() :
        first(Type1_()), second(Type2_())
    {
        // Nothing
    }

    scl_pair(const Type1_& val1, const Type2_& val2) :
        first(val1), second(val2)
    {
        // Nothing
    }

    scl_pair(const Self_& other) :
        first(other.first), second(other.second)
    {
        // Nothing
    }

#ifdef  SCL_HAS_MEMBER_TEMPLATES
    template <class Other1_, class Other2_>
    scl_pair(const scl_pair<Other1_, Other2_>& other) :
        first(other.first), second(other.second)
    {
        // Nothing
    }
#endif

    void swap(Self_& other)
    {
        scl_swap(first,  other.first );
        scl_swap(second, other.second);
    }

    // Public data members
    Type1_      first;
    Type2_      second;
};


template <class Type1_, class Type2_>
bool
operator==(const scl_pair<Type1_, Type2_>& lhs,
           const scl_pair<Type1_, Type2_>& rhs)
{
    return (lhs.first == rhs.first && lhs.second == rhs.second);
}

template <class Type1_, class Type2_>
bool
operator< (const scl_pair<Type1_, Type2_>& lhs,
           const scl_pair<Type1_, Type2_>& rhs)
{
    return (lhs.first < rhs.first) ||
        (!(rhs.first < lhs.first) && (lhs.second < rhs.second));
}

template <class Type1_, class Type2_>
bool
operator!=(const scl_pair<Type1_, Type2_>& lhs,
           const scl_pair<Type1_, Type2_>& rhs)
{
    return !(lhs == rhs);
}

template <class Type1_, class Type2_>
bool
operator> (const scl_pair<Type1_, Type2_>& lhs,
           const scl_pair<Type1_, Type2_>& rhs)
{
    return (rhs < lhs);
}

template <class Type1_, class Type2_>
bool
operator<=(const scl_pair<Type1_, Type2_>& lhs,
           const scl_pair<Type1_, Type2_>& rhs)
{
    return !(rhs < lhs);
}

template <class Type1_, class Type2_>
bool
operator>=(const scl_pair<Type1_, Type2_>& lhs,
           const scl_pair<Type1_, Type2_>& rhs)
{
    return !(lhs < rhs);
}

template <class Type1_, class Type2_>
scl_pair<Type1_, Type2_>
scl_make_pair(Type1_ val1, Type2_ val2)
{
    return scl_pair<Type1_, Type2_>(val1, val2);
}


#endif  /* _SCLP_UTILITY_H_ */
