/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: sclp_temp_buffer.cc
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Dec-2004     Initial implementation.
//
//==============================================================================
template <class Type_>
scl_pair<Type_*, ptrdiff_t>
_scl_get_temporary_buffer(ptrdiff_t len, Type_*)
{
    typedef scl_pair<Type_*, ptrdiff_t> Result_;

    ptrdiff_t   max_len = SCL_PTRDIFF_MAX / sizeof(Type_);

    if (len > max_len)
        len = max_len;

    while (len > 0)
    {
        Type_*  ptr = (Type_*) scl_malloc(len * sizeof(Type_));

        if (ptr != NULL)
            return Result_(ptr, len);

        len /= 2;
    }

    return Result_(NULL, 0);
}

#ifdef  SCL_HAS_FUNC_TMPL_ARGS
template <class Type_>
scl_pair<Type_*, ptrdiff_t>
scl_get_temporary_buffer(ptrdiff_t len)
{
    return _scl_get_temporary_buffer(len, (Type_*) NULL);
}
#endif

template <class Type_>
scl_pair<Type_*, ptrdiff_t>
scl_get_temporary_buffer(ptrdiff_t len, Type_*)
{
    return _scl_get_temporary_buffer(len, (Type_*) NULL);
}

template <class Type_>
void
scl_return_temporary_buffer(Type_* ptr)
{
    scl_free(ptr);
}
