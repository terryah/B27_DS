/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: sclp_uninitialized.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Dec-2004     Initial implementation.
//
//  NOTICE:
//      This header is used internally by the Standard C++ Library.
//      It should not be included directly in application code.
//
//==============================================================================
#ifndef _SCLP_UNINITIALIZED_H_
#define _SCLP_UNINITIALIZED_H_


//
//  Include section
//
#ifndef  _SCLP_CONSTRUCT_H_
#include <sclp_construct.h>             // for _scl_construct, _scl_destroy
#endif


//
//  20.4.4 Specialized Algorithms
//
template <class InpIter_, class FwdIter_>
FwdIter_
scl_uninitialized_copy(InpIter_ first, InpIter_ last, FwdIter_ result);

template <class FwdIter_, class Type_>
void
scl_uninitialized_fill(FwdIter_ first, FwdIter_ last, const Type_& value);

template <class FwdIter_, class Size_, class Type_>
void
scl_uninitialized_fill_n(FwdIter_ first, Size_ len, const Type_& value);


#include <sclp_uninitialized.cc>


#endif  /* _SCLP_UNINITIALIZED_H_ */
