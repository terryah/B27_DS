/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: scl_typeinfo.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Aug-2004     Initial implementation.
//
//  NOTICE:
//      This header implements functionality in the Standard C++ Library.
//      It may be included directly in application code.
//
//==============================================================================
#ifndef _SCL_TYPEINFO_H_
#define _SCL_TYPEINFO_H_


#ifndef  _SCL_CONFIG_H_
#include <scl_config.h>
#endif


#ifdef  SCL_HAS_STD_TYPEINFO
#include <typeinfo>
#else
#include <typeinfo.h>
#endif  /* SCL_HAS_STD_TYPEINFO */


#define scl_type_info                   SCL_VENDOR_STD::type_info
#define scl_bad_cast                    SCL_VENDOR_STD::bad_cast
#define scl_bad_typeid                  SCL_VENDOR_STD::bad_typeid


#endif  /* _SCL_TYPEINFO_H_ */
