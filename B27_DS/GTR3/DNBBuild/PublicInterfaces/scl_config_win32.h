/** @CAA2Required */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: scl_config_win32.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Oct-2004     Initial implementation.
//      jod     26-Jun-2008     Added support for MS Visual Studio 2005
//
//  NOTICE:
//      This header is used internally by the Standard C++ Library.
//      It should not be included directly in application code.
//
//==============================================================================
#ifndef _CPPRTTI
#error  "Runtime Type Information (RTTI) is not enabled."
#endif

#ifndef _CPPUNWIND
#error  "Exception handling is not enabled."
#endif

#ifndef _MT
#error  "Multithreading support is not enabled."
#endif


//
//  Compiler definitions.
//
#if     (_MSC_VER >= 1400)
// Microsoft Platform SDK Build 1830
// Microsoft Visual Studio .Net 2005
#define SCL_HAS_BOOL
#define SCL_HAS_DISTINCT_BOOL
#undef  SCL_HAS_DISTINCT_WCHAR
#undef  SCL_HAS_IOSTREAM_WCHAR
#define SCL_HAS_LLONG
#define SCL_HAS_EXPLICIT
#define SCL_HAS_TYPENAME
#define SCL_HAS_NAMESPACES
#define SCL_HAS_RTTI
#define SCL_HAS_EXCEPTIONS
#define SCL_HAS_THROW_SPECS
#define SCL_HAS_CLASS_USING_DECL
#define SCL_HAS_DEFAULT_TMPL_ARGS
#define SCL_HAS_NONTYPE_TMPL_ARGS
#define SCL_HAS_FUNC_TMPL_ARGS
#undef  SCL_HAS_MEMBER_TEMPLATES
#undef  SCL_HAS_PARTIAL_SPEC
#define SCL_HAS_PARTIAL_SPEC_OVERLOAD
#define SCL_HAS_SPEC_SYNTAX
#define SCL_HAS_STATIC_CONST_INIT
#define SCL_HAS_EXPLICIT_CLASS_INST
#define SCL_HAS_EXPLICIT_FUNC_INST
#define SCL_HAS_STD_NEW
#define SCL_HAS_STD_EXCEPTION
#define SCL_HAS_STD_TYPEINFO
#define SCL_HAS_STD_IOSTREAM
#define SCL_HAS_WSTR_FUNCTIONS

#else
#error  "Unsupported compiler"

#endif  /* Compiler definitions */


//
//  Platform configuration.
//
#define SCL_USE_POOL_ALLOCATOR          /* Use pool allocator in container classes */


//
//  Platform definitions.
//
#define SCL_BYTE_ORDER          SCL_LITTLE_ENDIAN
#define SCL_FLOAT_SIZE          32
#define SCL_DOUBLE_SIZE         64
#define SCL_LONG_DOUBLE_SIZE    64

#define SCL_LLONG_TYPE          __int64
#define SCL_LLONG_MIN           _I64_MIN
#define SCL_LLONG_MAX           _I64_MAX
#define SCL_LLONG_FMT           "%I64d"
#define SCL_LLONG_CONST(_X)     SCL_CONCAT2(_X,i64)

#define SCL_ULLONG_TYPE         unsigned __int64
#define SCL_ULLONG_MAX          _UI64_MAX
#define SCL_ULLONG_FMT          "%I64u"
#define SCL_ULLONG_CONST(_X)    SCL_CONCAT2(_X,Ui64)
