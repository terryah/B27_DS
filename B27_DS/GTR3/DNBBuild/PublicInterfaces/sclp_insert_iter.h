/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: sclp_insert_iter.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Oct-2004     Initial implementation.
//
//  NOTICE:
//      This header is used internally by the Standard C++ Library.
//      It should not be included directly in application code.
//
//==============================================================================
#ifndef _SCLP_INSERT_ITER_H_
#define _SCLP_INSERT_ITER_H_


//
//  Include section
//
#ifndef  _SCLP_BASE_ITER_H_
#include <sclp_base_iter.h>             // for scl_iterator and friends
#endif


//
//  24.4.2.1 Template class scl_back_insert_iterator
//
template <class Cntr_>
class scl_back_insert_iterator : public
    scl_iterator<scl_output_iterator_tag, void, void, void, void>
{
public:
    // Private types
    typedef
    scl_iterator<scl_output_iterator_tag, void, void, void, void>
                                                Base_;
    typedef scl_back_insert_iterator<Cntr_>     Self_;

    // Public types
    typedef Cntr_                               container_type;
    SCL_ITERATOR_TYPEDEFS(Base_);

    explicit
    scl_back_insert_iterator(Cntr_& cntr) :
        container(&cntr)
    {
        // Nothing
    }

    Self_& operator=(typename Cntr_::const_reference value)
    {
        container->push_back(value);
        return *this;
    }

    Self_& operator*()
    {
        return *this;
    }

    Self_& operator++()
    {
        return *this;
    }

    Self_  operator++(int)
    {
        return *this;
    }

    // The following data-member name is explicitly defined by the Standard.
protected:
    Cntr_*  container;
};


template <class Cntr_>
scl_back_insert_iterator<Cntr_>
scl_back_inserter(Cntr_& cntr)
{
    return scl_back_insert_iterator<Cntr_>(cntr);
}


//
//  24.4.2.3 Template class scl_front_insert_iterator
//
template <class Cntr_>
class scl_front_insert_iterator : public
    scl_iterator<scl_output_iterator_tag, void, void, void, void>
{
public:
    // Private types
    typedef
    scl_iterator<scl_output_iterator_tag, void, void, void, void>
                                                Base_;
    typedef scl_front_insert_iterator<Cntr_>    Self_;

    // Public types
    typedef Cntr_                               container_type;
    SCL_ITERATOR_TYPEDEFS(Base_);

    explicit
    scl_front_insert_iterator(Cntr_& cntr) :
        container(&cntr)
    {
        // Nothing
    }

    Self_& operator=(typename Cntr_::const_reference value)
    {
        container->push_front(value);
        return *this;
    }

    Self_& operator*()
    {
        return *this;
    }

    Self_& operator++()
    {
        return *this;
    }

    Self_  operator++(int)
    {
        return *this;
    }

    // The following data-member name is explicitly defined by the Standard.
protected:
    Cntr_*  container;
};


template <class Cntr_>
scl_front_insert_iterator<Cntr_>
scl_front_inserter(Cntr_& cntr)
{
    return scl_front_insert_iterator<Cntr_>(cntr);
}


//
//  24.4.2.5 Template class scl_insert_iterator
//
template <class Cntr_>
class scl_insert_iterator : public
    scl_iterator<scl_output_iterator_tag, void, void, void, void>
{
public:
    // Private types
    typedef
    scl_iterator<scl_output_iterator_tag, void, void, void, void>
                                                Base_;
    typedef scl_insert_iterator<Cntr_>          Self_;
    typedef typename Cntr_::iterator            Iter_;

    // Public types
    typedef Cntr_                               container_type;
    SCL_ITERATOR_TYPEDEFS(Base_);

    scl_insert_iterator(Cntr_& cntr, Iter_ itr) :
        container(&cntr),
        iter     (itr)
    {
        // Nothing
    }

    Self_& operator=(typename Cntr_::const_reference value)
    {
        iter = container->insert(iter, value);
        ++iter;
        return *this;
    }

    Self_& operator*()
    {
        return *this;
    }

    Self_& operator++()
    {
        return *this;
    }

    Self_& operator++(int)
    {
        return *this;
    }

    // The following data-member names are explicitly defined by the Standard.
protected:
    Cntr_*  container;
    Iter_   iter;
};


template <class Cntr_, class Iter_>
scl_insert_iterator<Cntr_>
scl_inserter(Cntr_& cntr, Iter_ itr)
{
    typedef typename Cntr_::iterator    CIter_;
    return scl_insert_iterator<Cntr_>(cntr, CIter_(itr));
}


#endif  /* _SCLP_INSERT_ITER_H_ */
