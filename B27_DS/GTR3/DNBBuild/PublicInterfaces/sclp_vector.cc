/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: sclp_vector.cc
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Aug-2004     Initial implementation.
//
//==============================================================================
#define SCL_VECTOR_TEMPL        template <class Type_, class Alloc_>
#define SCL_VECTOR_CLASS        scl_vector<Type_, Alloc_>
#define SCL_VECTOR_CTOR_BEGIN   try {
#define SCL_VECTOR_CTOR_END     } catch (...) { destroy(); throw; }


//
//  23.2.4.1 construct/copy/destroy
//
SCL_VECTOR_TEMPL
SCL_VECTOR_CLASS::scl_vector() :
    alloc_()
{
    SCL_VALIDATE_CTOR(Self_, this);
    initialize(0);
}

SCL_VECTOR_TEMPL
SCL_VECTOR_CLASS::scl_vector(const Alloc_& alloc) :
    alloc_(alloc)
{
    SCL_VALIDATE_CTOR(Self_, this);
    initialize(0);
}

SCL_VECTOR_TEMPL
SCL_VECTOR_CLASS::scl_vector(size_type len) :
    alloc_()
{
    SCL_VALIDATE_CTOR(Self_, this);
    initialize(len);

    SCL_VECTOR_CTOR_BEGIN
        Type_   value = Type_();
        append_aux(len, value);
    SCL_VECTOR_CTOR_END
}

SCL_VECTOR_TEMPL
SCL_VECTOR_CLASS::scl_vector(size_type len, const Type_& value) :
    alloc_()
{
    SCL_VALIDATE_CTOR(Self_, this);
    initialize(len);

    SCL_VECTOR_CTOR_BEGIN
        append_aux(len, value);
    SCL_VECTOR_CTOR_END
}

SCL_VECTOR_TEMPL
SCL_VECTOR_CLASS::scl_vector(size_type len, const Type_& value, const Alloc_& alloc) :
    alloc_(alloc)
{
    SCL_VALIDATE_CTOR(Self_, this);
    initialize(len);

    SCL_VECTOR_CTOR_BEGIN
        append_aux(len, value);
    SCL_VECTOR_CTOR_END
}

#ifdef  SCL_HAS_MEMBER_TEMPLATES
SCL_VECTOR_TEMPL
template <class InputIterator_>
SCL_VECTOR_CLASS::scl_vector(InputIterator_ first, InputIterator_ last) :
#else
SCL_VECTOR_TEMPL
SCL_VECTOR_CLASS::scl_vector(const_iterator first, const_iterator last) :
#endif
    alloc_()
{
    SCL_VALIDATE_CTOR(Self_, this);
    const size_type len = scl_distance(first, last);
    initialize(len);

    SCL_VECTOR_CTOR_BEGIN
        append_aux(first, last);
    SCL_VECTOR_CTOR_END
}

#ifdef  SCL_HAS_MEMBER_TEMPLATES
SCL_VECTOR_TEMPL
template <class InputIterator_>
SCL_VECTOR_CLASS::scl_vector(InputIterator_ first, InputIterator_ last, const Alloc_& alloc) :
#else
SCL_VECTOR_TEMPL
SCL_VECTOR_CLASS::scl_vector(const_iterator first, const_iterator last, const Alloc_& alloc) :
#endif
    alloc_(alloc)
{
    SCL_VALIDATE_CTOR(Self_, this);
    const size_type len = scl_distance(first, last);
    initialize(len);

    SCL_VECTOR_CTOR_BEGIN
        append_aux(first, last);
    SCL_VECTOR_CTOR_END
}

SCL_VECTOR_TEMPL
SCL_VECTOR_CLASS::scl_vector(const Self_& other) :
    alloc_(other.alloc_)
{
    SCL_VALIDATE_CTOR(Self_, this);
    const size_type len = other.size();
    initialize(len);

    SCL_VECTOR_CTOR_BEGIN
        append_aux(other.begin(), other.end());
    SCL_VECTOR_CTOR_END
}

SCL_VECTOR_TEMPL
SCL_VECTOR_CLASS::~scl_vector()
{
    SCL_VALIDATE_DTOR(Self_, this);
    destroy();
}

SCL_VECTOR_TEMPL
SCL_VECTOR_CLASS &
SCL_VECTOR_CLASS::operator=(const Self_& other)
{
    SCL_VALIDATE(Self_, this);

    if (this != &other)
        assign(other.begin(), other.end());

    return *this;
}

#ifdef  SCL_HAS_MEMBER_TEMPLATES
SCL_VECTOR_TEMPL
template <class InputIterator_>
void
SCL_VECTOR_CLASS::assign(InputIterator_ first, InputIterator_ last)
#else
SCL_VECTOR_TEMPL
void
SCL_VECTOR_CLASS::assign(const_iterator first, const_iterator last)
#endif
{
    SCL_VALIDATE(Self_, this);
    const size_type msize = size();
    const size_type len   = scl_distance(first, last);

    if (len <= msize)
    {
        scl_copy(first, last, pfirst_);
        erase_end(msize - len);
    }
    else if (len <= capacity())
    {
#ifdef  SCL_HAS_MEMBER_TEMPLATES
        InputIterator_  mid = first;
#else
        const_iterator  mid = first;
#endif
        scl_advance(mid, msize);
        scl_copy(first, mid, pfirst_);
        append_aux(mid, last);
    }
    else
    {
        reserve_aux(len, false);
        append_aux(first, last);
    }

    SCL_POSTCONDITION(size() == len);
}

SCL_VECTOR_TEMPL
void
SCL_VECTOR_CLASS::assign(size_type len, const Type_& value)
{
    SCL_VALIDATE(Self_, this);
    const size_type msize = size();

    if (len <= msize)
    {
        scl_fill_n(pfirst_, len, value);
        erase_end(msize - len);
    }
    else if (len <= capacity())
    {
        scl_fill_n(pfirst_, msize, value);
        append_aux(len - msize, value);
    }
    else
    {
        reserve_aux(len, false);
        append_aux(len, value);
    }

    SCL_POSTCONDITION(size() == len);
}

SCL_VECTOR_TEMPL
typename SCL_VECTOR_CLASS::allocator_type
SCL_VECTOR_CLASS::get_allocator() const
{
    return alloc_;
}


//
//  iterators
//
SCL_VECTOR_TEMPL
typename SCL_VECTOR_CLASS::iterator
SCL_VECTOR_CLASS::begin()
{
    return iterator(pfirst_);
}

SCL_VECTOR_TEMPL
typename SCL_VECTOR_CLASS::const_iterator
SCL_VECTOR_CLASS::begin() const
{
    return const_iterator(pfirst_);
}

SCL_VECTOR_TEMPL
typename SCL_VECTOR_CLASS::iterator
SCL_VECTOR_CLASS::end()
{
    return iterator(plast_);
}

SCL_VECTOR_TEMPL
typename SCL_VECTOR_CLASS::const_iterator
SCL_VECTOR_CLASS::end() const
{
    return const_iterator(plast_);
}

SCL_VECTOR_TEMPL
typename SCL_VECTOR_CLASS::reverse_iterator
SCL_VECTOR_CLASS::rbegin()
{
    return reverse_iterator(end());
}

SCL_VECTOR_TEMPL
typename SCL_VECTOR_CLASS::const_reverse_iterator
SCL_VECTOR_CLASS::rbegin() const
{
    return const_reverse_iterator(end());
}

SCL_VECTOR_TEMPL
typename SCL_VECTOR_CLASS::reverse_iterator
SCL_VECTOR_CLASS::rend()
{
    return reverse_iterator(begin());
}

SCL_VECTOR_TEMPL
typename SCL_VECTOR_CLASS::const_reverse_iterator
SCL_VECTOR_CLASS::rend() const
{
    return const_reverse_iterator(begin());
}


//
//  23.2.4.2 capacity
//
SCL_VECTOR_TEMPL
void
SCL_VECTOR_CLASS::resize(size_type len)
{
    SCL_VALIDATE(Self_, this);
    Type_   value = Type_();
    resize(len, value);
}

SCL_VECTOR_TEMPL
void
SCL_VECTOR_CLASS::resize(size_type len, const Type_& value)
{
    SCL_VALIDATE(Self_, this);
    const size_type msize = size();

    if (len < msize)
    {
        erase_end(msize - len);
    }
    else if (len > msize)
    {
        reserve_aux(len, true);
        append_aux(len - msize, value);
    }

    SCL_POSTCONDITION(size() == len);
}

SCL_VECTOR_TEMPL
void
SCL_VECTOR_CLASS::reserve(size_type len)
{
    SCL_VALIDATE(Self_, this);
    reserve_aux(len, true);
}


//
//  23.2.4.3 modifiers
//
SCL_VECTOR_TEMPL
void
SCL_VECTOR_CLASS::push_back(const Type_& value)
{
    SCL_VALIDATE(Self_, this);
    reserve_aux(size() + 1, true);
    append_aux(value);
}

SCL_VECTOR_TEMPL
void
SCL_VECTOR_CLASS::pop_back()
{
    SCL_VALIDATE(Self_, this);
    SCL_PRECONDITION( !empty() );
    erase_end(1);
}

SCL_VECTOR_TEMPL
typename SCL_VECTOR_CLASS::iterator
SCL_VECTOR_CLASS::insert(iterator iter, const Type_& value)
{
    SCL_VALIDATE(Self_, this);
    const size_type idx = iter - begin();

    if (iter == end())
        push_back(value);
    else
        insert(iter, 1, value);

    return iterator(pfirst_ + idx);
}

SCL_VECTOR_TEMPL
void
SCL_VECTOR_CLASS::insert(iterator iter, size_type len, const Type_& value)
{
    SCL_VALIDATE(Self_, this);
    SCL_PRECONDITION(in_range(iter));

    if (len == 0)
        return;

    const size_type nsize = size() + len;       // new size
    pointer         piter = iter.base();

    if (nsize <= capacity())
    {
        // Enough capacity for insertion.
        pointer     olast = plast_;             // original end()

        if (piter + len < olast)
        {
            // Insertion entirely before original end().
            _scl_uninitialized_copy(olast - len, olast, olast, alloc_);
            plast_ += len;
            scl_copy_backward(piter, olast - len, olast);
            scl_fill(piter, piter + len, value);
        }
        else
        {
            // Insertion partially before original end().
            const size_type after = len - (olast - piter);
            _scl_uninitialized_fill_n(olast, after, value, alloc_);
            plast_ += after;
            _scl_uninitialized_copy(piter, olast, piter + len, alloc_);
            plast_ += (olast - piter);
            scl_fill(piter, olast, value);
        }
    }
    else
    {
        // Not enough capacity.
        Self_   tmp(alloc_);
        tmp.reserve_aux(nsize, false);
        tmp.append_aux(begin(), iter);
        tmp.append_aux(len, value);
        tmp.append_aux(iter, end());
        tmp.swap(*this);
    }

    SCL_POSTCONDITION(size() == nsize);
}

#ifdef  SCL_HAS_MEMBER_TEMPLATES
SCL_VECTOR_TEMPL
template <class InputIterator_>
void
SCL_VECTOR_CLASS::insert(iterator iter, InputIterator_ first, InputIterator_ last)
#else
SCL_VECTOR_TEMPL
void
SCL_VECTOR_CLASS::insert(iterator iter, const_iterator first, const_iterator last)
#endif
{
    SCL_VALIDATE(Self_, this);
    SCL_PRECONDITION(in_range(iter));

    if (first == last)
        return;

    const size_type len   = scl_distance(first, last);
    const size_type nsize = size() + len;       // new size
    pointer         piter = iter.base();

    if (nsize <= capacity())
    {
        // Enough capacity for insertion.
        pointer     olast = plast_;             // original end()

        if (piter + len < olast)
        {
            // Insertion entirely before original end().
            _scl_uninitialized_copy(olast - len, olast, olast, alloc_);
            plast_ += len;
            scl_copy_backward(piter, olast - len, olast);
            scl_copy(first, last, piter);
        }
        else
        {
            // Insertion partially before original end().
            const size_type after = olast - piter;
#ifdef  SCL_HAS_MEMBER_TEMPLATES
            InputIterator_  mid = first;
#else
            const_iterator  mid = first;
#endif
            scl_advance(mid, after);
            _scl_uninitialized_copy(mid, last, olast, alloc_);
            plast_ += (len - after);
            _scl_uninitialized_copy(piter, olast, piter + len, alloc_);
            plast_ += after;
            scl_copy(first, mid, piter);
        }
    }
    else
    {
        // Not enough capacity.
        Self_   tmp(alloc_);
        tmp.reserve_aux(nsize, false);
        tmp.append_aux(begin(), iter);
        tmp.append_aux(first, last);
        tmp.append_aux(iter, end());
        tmp.swap(*this);
    }

    SCL_POSTCONDITION(size() == nsize);
}

SCL_VECTOR_TEMPL
typename SCL_VECTOR_CLASS::iterator
SCL_VECTOR_CLASS::erase(iterator iter)
{
    SCL_VALIDATE(Self_, this);
    SCL_PRECONDITION(in_range(iter) && iter != end());

    pointer     piter = iter.base();
    scl_copy(piter + 1, plast_, piter);
    erase_end(1);

    return iter;
}

SCL_VECTOR_TEMPL
typename SCL_VECTOR_CLASS::iterator
SCL_VECTOR_CLASS::erase(iterator first, iterator last)
{
    SCL_VALIDATE(Self_, this);
    SCL_PRECONDITION(in_range(first) && in_range(last));

    if (first != last)
    {
        scl_copy(last.base(), plast_, first.base());
        erase_end(last - first);
    }

    return first;
}

SCL_VECTOR_TEMPL
void
SCL_VECTOR_CLASS::clear()
{
    // Clear vector without deleting its storage.
    SCL_VALIDATE(Self_, this);
    erase_all();
}

SCL_VECTOR_TEMPL
void
SCL_VECTOR_CLASS::swap(Self_& other)
{
    SCL_VALIDATE(Self_, this);

    if (alloc_ == other.alloc_)
    {
        scl_swap(pfirst_, other.pfirst_);
        scl_swap(plast_ , other.plast_ );
        scl_swap(pfinal_, other.pfinal_);
        scl_swap(alloc_ , other.alloc_ );
    }
    else
    {
        const Self_ tmp(*this);
        assign(other.begin(), other.end());
        other.assign(tmp.begin(), tmp.end());
    }
}

SCL_VECTOR_TEMPL
bool
SCL_VECTOR_CLASS::_validate() const
{
    // Test class invariants.
    bool    okay;

    if (pfirst_ == NULL)
        okay = (plast_ == NULL && pfinal_ == NULL);
    else
        okay = (pfirst_ <= plast_ && plast_ <= pfinal_);

    return okay;
}

SCL_VECTOR_TEMPL
bool
SCL_VECTOR_CLASS::in_range(iterator iter) const
{
    pointer     piter = iter.base();
    return (piter >= pfirst_ && piter <= plast_);
}

SCL_VECTOR_TEMPL
void
SCL_VECTOR_CLASS::initialize(size_type len)
{
    // Establish class invariants.
    pfirst_ = NULL;
    plast_  = NULL;
    pfinal_ = NULL;

    if (len > 0)
        reserve_aux(len, false);
}

SCL_VECTOR_TEMPL
void
SCL_VECTOR_CLASS::reserve_aux(size_type len, bool do_copy)
{
    // Allocate storage for at least <len> elements.  If <do_copy> is true, existing
    // elements are copied to the new array.

    // In this implementation, vectors do not shrink.
    if (len <= capacity())
        return;

    // Compute the new capacity.
    const size_type ncap = IncrementSize * (len / IncrementSize + 1);
    SCL_ASSERT(ncap >= len);

    if (ncap > max_size())
        _scl_throw_length_error();

    // Allocate storage for the new array.  If the allocation fails, the state of
    // <self> is maintained.
    pointer     nfirst = alloc_.allocate(ncap);
    pointer     nlast  = nfirst;

    if (do_copy && pfirst_ != NULL)
        nlast = _scl_uninitialized_copy(pfirst_, plast_, nfirst, alloc_);

    destroy();

    pfirst_ = nfirst;
    plast_  = nlast;
    pfinal_ = nfirst + ncap;
}

SCL_VECTOR_TEMPL
void
SCL_VECTOR_CLASS::destroy()
{
    // Erase all elements of <self> and deallocate storage.
    if (pfirst_ != NULL)
    {
        _scl_destroy(pfirst_, plast_, alloc_);
        alloc_.deallocate(pfirst_, capacity());
    }

    pfirst_ = NULL;
    plast_  = NULL;
    pfinal_ = NULL;
}

SCL_VECTOR_TEMPL
void
SCL_VECTOR_CLASS::erase_all()
{
    // Erase all elements of <self>.
    if (pfirst_ != NULL)
    {
        _scl_destroy(pfirst_, plast_, alloc_);
        plast_ = pfirst_;
    }
}

SCL_VECTOR_TEMPL
void
SCL_VECTOR_CLASS::erase_end(size_type len)
{
    // Erase last <len> elements of <self>.
    SCL_PRECONDITION(len <= size());

    if (pfirst_ != NULL)
    {
        pointer first = plast_ - len;
        _scl_destroy(first, plast_, alloc_);
        plast_ = first;
    }
}

SCL_VECTOR_TEMPL
void
SCL_VECTOR_CLASS::append_aux(const Type_& value)
{
    // Append one copy of <value> to <self>.  The caller must ensure sufficient
    // space is available in the array.
    SCL_PRECONDITION(1 <= (pfinal_ - plast_));

    alloc_.construct(plast_, value);
    ++plast_;
}

SCL_VECTOR_TEMPL
void
SCL_VECTOR_CLASS::append_aux(size_type len, const Type_& value)
{
    // Append <len> copies of <value> to <self>.  The caller must ensure sufficient
    // space is available in the array.
    SCL_PRECONDITION(len <= (pfinal_ - plast_));

    _scl_uninitialized_fill_n(plast_, len, value, alloc_);
    plast_ += len;
}

#ifdef  SCL_HAS_MEMBER_TEMPLATES
SCL_VECTOR_TEMPL
template <class InputIterator_>
void
SCL_VECTOR_CLASS::append_aux(InputIterator_ first, InputIterator_ last)
{
    // Append copies of [first, last) to <self>.  The caller must ensure sufficient
    // space is available in the array.
    SCL_PRECONDITION(scl_distance(first, last) <= (pfinal_ - plast_));

    plast_ = _scl_uninitialized_copy(first, last, plast_, alloc_);
}
#else
SCL_VECTOR_TEMPL
void
SCL_VECTOR_CLASS::append_aux(const_iterator first, const_iterator last)
{
    // Append copies of [first, last) to <self>.  The caller must ensure sufficient
    // space is available in the array.
    SCL_PRECONDITION(scl_distance(first, last) <= (pfinal_ - plast_));

    plast_ = _scl_uninitialized_copy(first.base(), last.base(), plast_, alloc_);
}
#endif  /* SCL_HAS_MEMBER_TEMPLATES */


#undef  SCL_VECTOR_TEMPL
#undef  SCL_VECTOR_CLASS
#undef  SCL_VECTOR_CTOR_BEGIN
#undef  SCL_VECTOR_CTOR_END
