//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     bkh         01/19/04    Intial Implementation.
//*     bkh         03/19/04    Replaced DNB_STRING with scl_string
//*     rtl         05/10/06    Replaced long with double in _DNBGetNumericDou methods



#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNBSTRINGUTIL_H_
#define _DNBSTRINGUTIL_H_


#include <DNBSystemBase.h>
#include <wchar.h>
#include <scl_string.h>
#include <scl_stdexcept.h>
#include <DNBSystemDefs.h>

#include <CATString.h>
#include <DNBBuild.h>
#include <scl_limits.h>

enum DNBStripMode
{
    DNBStripLeading,
    DNBStripTrailing,
    DNBStripBoth,
    DNBStripAll
};


enum DNBCaseCompare
{
    DNBCaseExact,
    DNBCaseIgnore
};


/**
  * Converts string <str> to lower-case and returns <str>.
  * @param str
  *     scl_string type
  */
ExportedByDNBBuild scl_string &
DNBToLower( scl_string &str )
    DNB_THROW_SPEC_NULL;

/**
  * Converts wstring <str> to lower-case and returns <str>.
  * @param str
  *     scl_wstring type
  */
ExportedByDNBBuild scl_wstring &
DNBToLower( scl_wstring &str )
    DNB_THROW_SPEC_NULL;

/**
  * Converts CATString to string.
  * @param str
  *     scl_string type
  * @param cstr
  *    const CATString 
  */
ExportedByDNBBuild scl_string&
DNBToString( scl_string &str, const CATString &cstr )
    DNB_THROW_SPEC_NULL;

/**
  * Converts String to CATString.
  * @param cstr
  *     CATString
  * @param str
  *     const scl_string type
  */

ExportedByDNBBuild CATString&
DNBToCATString( CATString &cstr, const scl_string &str )
    DNB_THROW_SPEC_NULL;

/**
  * Converts string <str> to upper-case and returns <str>.
  * @param str
  *     scl_string type
  */
ExportedByDNBBuild scl_string &
DNBToUpper( scl_string &str )
    DNB_THROW_SPEC_NULL;

/**
  * Converts wstring <str> to upper-case and returns <str>.
  * @param str
  *     scl_wstring type
  */
ExportedByDNBBuild scl_wstring &
DNBToUpper( scl_wstring &str )
    DNB_THROW_SPEC_NULL;


/**
  * Removes character <chr> from string <str> and returns <str>.
  * @param str
  *     scl_string type
  * @param mode
  *     can be of the following 4 types.
  *      DNBStripLeading - Strips the specified char from the left hand side.
  *      DNBStripTrailing - Strips the specified char from the right hand side.
  *      DNBStripBoth - Strips the specified char from both side.
  *      DNBStripAll - Strips the specified char from the string.
  * @param chr
  *     char that is to be removed.
  */

ExportedByDNBBuild scl_string &
DNBStripChar( scl_string &str, DNBStripMode mode, char chr )
    DNB_THROW_SPEC_NULL;


/**
  * Removes character <chr> from wstring <str> and returns <str>.
  * @param str
  *     The source string of type scl_string
  * @param mode
  *     can be of the following 4 types.
  *      DNBStripLeading - Strips the specified char from the left hand side.
  *      DNBStripTrailing - Strips the specified char from the right hand side.
  *      DNBStripBoth - Strips the specified char from both side.
  *      DNBStripAll - Strips the specified char from the string.
  * @param chr
  *     wide char that is to be removed.
  */

ExportedByDNBBuild scl_wstring &
DNBStripChar( scl_wstring &str, DNBStripMode mode, wchar_t chr )
    DNB_THROW_SPEC_NULL;


/**
  * Removes white-space characters from string <str> and returns <str>
  * White-space characters are classified using isspace() and iswspace().
  * @param str
  *     scl_string type
  * @param mode
  *     can be of the following 4 types.
  *      DNBStripLeading - Strips spaces starting with the beginning of the string.
  *      DNBStripTrailing - Strips trailing spaces in the string.
  *      DNBStripBoth - Strips spaces from both side of the string.
  *      DNBStripAll - Strips spaces from all over the string.
  */
  
ExportedByDNBBuild scl_string &
DNBStripSpace( scl_string &str, DNBStripMode mode )
    DNB_THROW_SPEC_NULL;

/**
  * Removes white-space characters from wstring <str> and returns <str>
  * White-space characters are classified using isspace() and iswspace().
  * @param str
  *     scl_wstring type
  * @param mode
  *     can be of the following 4 types.
  *      DNBStripLeading - Strips spaces starting with the beginning of the string.
  *      DNBStripTrailing - Strips trailing spaces in the string.
  *      DNBStripBoth - Strips spaces from both side of the string.
  *      DNBStripAll - Strips spaces from all over the string.
  */

ExportedByDNBBuild scl_wstring &
DNBStripSpace( scl_wstring &str, DNBStripMode mode )
    DNB_THROW_SPEC_NULL;


/**
  *Compares two strings lexicographically.
  * The character sequence represented by the current String
  * is compared lexicographically to the character sequence
  * represented by the argument string.
  * @param left
  * @param right
  *   The Strings to be compared
  * @param cmp
  *   The mode of comparision can be either
  *   os_exact_case or os_ignore_case.
  *   os_exact_case is case sensitive.
  *   os_ignore_case is not case sensitive.
  * @return
  *   Comparison result.
  *   <br><b>Legal values</b>: <tt>less than 0</tt>
  *   if the value of the current object string is
  *   lexicographically less than other, or
  *   <tt>equal to 0</tt> if the value of the current object
  *   string is lexicographically equal than other.
  */

ExportedByDNBBuild int
DNBCompare( const scl_string &left, const scl_string &right, DNBCaseCompare cmp )
    DNB_THROW_SPEC_NULL;

/**
  * Compares a string to a character.
  * The character sequence represented by the current String
  * is compared lexicographically to the character.
  * @param left
  *   The String to be compared.
  * @param  right
  *     const character to be compared.
  * @param cmp
  *   The mode of comparision can be either
  *   os_exact_case or os_ignore_case.
  *   os_exact_case is case sensitive.
  *   os_ignore_case is not case sensitive.
  * @return
  *   Comparison result.
  *   <br><b>Legal values</b>: <tt>less than 0</tt>
  *   if the value of the current object string is
  *   lexicographically less than other, or
  *   <tt>equal to 0</tt> if the value of the current object
  *   string is lexicographically equal than other.
  */

ExportedByDNBBuild int
DNBCompare( const scl_string &left, const char   *right, DNBCaseCompare cmp )
    DNB_THROW_SPEC_NULL;

/**
  * Compares two wstring lexicographically.
  * The character sequence represented by the current String
  * is compared lexicographically to the character sequence
  * represented by the argument string.
  * @param left
  * @param right
  *   The Strings to be compared
  * @param cmp
  *   The mode of comparision can be either
  *   os_exact_case or os_ignore_case.
  *   os_exact_case is case sensitive.
  *   os_ignore_case is not case sensitive.
  * @return
  *   Comparison result.
  *   <br><b>Legal values</b>: <tt>less than 0</tt>
  *   if the value of the current object string is
  *   lexicographically less than other, or
  *   <tt>equal to 0</tt> if the value of the current object
  *   string is lexicographically equal than other.
  */
ExportedByDNBBuild int
DNBCompare( const scl_wstring &left, const scl_wstring &right, DNBCaseCompare cmp )
    DNB_THROW_SPEC_NULL;

/**
  * Compares wstring to a wide character const.
  * The character sequence represented by the current String
  * is compared lexicographically to the character.
  * @param left
  *   The String to be compared.
  * @param  right
  *     const character to be compared.
  * @param cmp
  *   The mode of comparision can be either
  *   os_exact_case or os_ignore_case.
  *   os_exact_case is case sensitive.
  *   os_ignore_case is not case sensitive.
  * @return
  *   Comparison result.
  *   <br><b>Legal values</b>: <tt>less than 0</tt>
  *   if the value of the current object string is
  *   lexicographically less than other, or
  *   <tt>equal to 0</tt> if the value of the current object
  *   string is lexicographically equal than other.
  */

ExportedByDNBBuild int
DNBCompare( const scl_wstring &left, const wchar_t *right, DNBCaseCompare cmp )
    DNB_THROW_SPEC_NULL;

//------------------------------------------------------------------------------
//  Private functions.
//------------------------------------------------------------------------------

scl_string::size_type
_DNBDiffPtr( const char *last, const char *first )
    DNB_THROW_SPEC_NULL;

scl_string::size_type
_DNBDiffPtr( const wchar_t *last, const wchar_t *first )
    DNB_THROW_SPEC_NULL;

template <class _CharT, class _Traits, class _Alloc>
size_t
_DNBReplace( scl_basic_string<_CharT,_Traits,_Alloc> &target,
       const scl_basic_string<_CharT,_Traits,_Alloc> &search,
       const scl_basic_string<_CharT,_Traits,_Alloc> &replace,
       scl_string::size_type tpos, size_t rcount )
    DNB_THROW_SPEC_NULL
{
    if ( tpos + search.size() > target.size() || search == replace )
        return 0;

    for ( size_t count = 0; (rcount == 0 || count < rcount); ++count )
    {
        tpos = target.find( search, tpos );
        if ( tpos == scl_string::npos )
            break;

        target.replace( tpos, search.size(), replace );
        tpos += replace.size();
    }

    return count;
}

// signed values

template <class _Type>
scl_string::size_type
_DNBGetNumericInt( const scl_string &source, _Type &value,
    scl_string::size_type spos, int base )
    DNB_THROW_SPEC((scl_range_error))
{
    value = 0;                          // Default value

    if ( spos > source.size() )
        return scl_string::npos;

    const char *first = source.c_str() + spos;
    char       *last  = NULL;
    long        vmin  = scl_numeric_limits<_Type>::min();
    long        vmax  = scl_numeric_limits<_Type>::max();
    long    result = strtol( first, &last, base );
    if ( last == NULL || last == first )
        return scl_string::npos;          // Conversion failed

    value = result;
    return _DNBDiffPtr( last, source.c_str() );

}

// Unsigned values

template <class _Type>
scl_string::size_type
_DNBGetNumericUnInt( const scl_string &source, _Type &value,
    scl_string::size_type spos, int base )
    DNB_THROW_SPEC((scl_range_error))
{
    value = 0;                          // Default value

    if ( spos > source.size() )
        return scl_string::npos;

    const char *first = source.c_str() + spos;
    char       *last  = NULL;
    long        vmin  = scl_numeric_limits<_Type>::min();
    long        vmax  = scl_numeric_limits<_Type>::max();
    long    result = strtoul( first, &last, base );
    if ( last == NULL || last == first )
        return scl_string::npos;          // Conversion failed

    value = result;
    return _DNBDiffPtr( last, source.c_str() );
}


// Real Numbers

template <class _Type>
scl_string::size_type
_DNBGetNumericDou( const scl_string &source, _Type &value,
    scl_string::size_type spos, int base )
    DNB_THROW_SPEC((scl_range_error))
{
    value = 0;                          // Default value

    if ( spos > source.size() )
        return scl_string::npos;

    const char *first = source.c_str() + spos;
    char       *last  = NULL;
    double      vmin  = scl_numeric_limits<_Type>::min();
    double      vmax  = scl_numeric_limits<_Type>::max();
    double   result = strtod( first, &last);
    if ( last == NULL || last == first )
        return scl_string::npos;          // Conversion failed

    value = result;
    return _DNBDiffPtr( last, source.c_str() );
}

// wstring 
//signed values

template <class _Type>
scl_string::size_type
_DNBGetNumericInt( const scl_wstring &source, _Type &value,
    scl_string::size_type spos, int base )
    DNB_THROW_SPEC((scl_range_error))
{
    value = 0;                          // Default value

    if ( spos > source.size() )
        return scl_string::npos;

    const wchar_t *first = source.c_str() + spos;
    wchar_t       *last  = NULL;
    long        vmin  = scl_numeric_limits<_Type>::min();
    long        vmax  = scl_numeric_limits<_Type>::max();
    long    result = wcstol( first, &last, base );
    if ( last == NULL || last == first )
        return scl_string::npos;          // Conversion failed

    value = result;
    return _DNBDiffPtr( last, source.c_str() );
}

// Unsigned values

template <class _Type>
scl_string::size_type
_DNBGetNumericUnInt( const scl_wstring &source, _Type &value,
    scl_string::size_type spos, int base )
    DNB_THROW_SPEC((scl_range_error))
{
    value = 0;                          // Default value

    if ( spos > source.size() )
        return scl_string::npos;

    const wchar_t *first = source.c_str() + spos;
    wchar_t       *last  = NULL;
    long        vmin  = scl_numeric_limits<_Type>::min();
    long        vmax  = scl_numeric_limits<_Type>::max();
    long    result = wcstoul( first, &last, base );
    if ( last == NULL || last == first )
        return scl_string::npos;          // Conversion failed

    value = result;
    return _DNBDiffPtr( last, source.c_str() );
}

// Real Numbers

template <class _Type>
scl_string::size_type
_DNBGetNumericDou( const scl_wstring &source, _Type &value,
    scl_string::size_type spos, int base )
    DNB_THROW_SPEC((scl_range_error))
{
    value = 0;                          // Default value

    if ( spos > source.size() )
        return scl_string::npos;

    const wchar_t *first = source.c_str() + spos;
    wchar_t       *last  = NULL;
    double        vmin  = scl_numeric_limits<_Type>::min();
    double        vmax  = scl_numeric_limits<_Type>::max();
    double    result = wcstod( first, &last);
    if ( last == NULL || last == first )
        return scl_string::npos;          // Conversion failed

    value = result;
    return _DNBDiffPtr( last, source.c_str() );
}



/**
 *  Starting from position <spos> in string <source>, extract an integral value
 *  and assign it to <value>.  If <base> is between 2 and 36, it is used as the
 *  base of the number.  If <base> is 0, the initial characters of the string
 *  are used to determine the base.  This function returns the position of
 *  <source> where the scan stopped.
 *  @param source
 *      The source string of type scl_string
 *  @param value
 *      type signed char
 *  @param spos
 *      Position where the scan starts
 *  @param base
 *      base between 2 and 36
 */


ExportedByDNBBuild scl_string::size_type
DNBGetNumeric( const scl_string &source, signed char &value,
    scl_string::size_type spos = 0, int base = 10 )
    DNB_THROW_SPEC((scl_range_error));

/**
 *  Starting from position <spos> in string <source>, extract a value
 *  This function returns the position of source where the scan stopped.
 *  @param source
 *      The source string of type scl_string
 *  @param value
 *      Value to be assigned of type short
 *  @param spos
 *      Position where the scan starts
 *  @param base
 *      Base between 2 and 36. If base is 0, the initial characters of the string
 *      are used to determine the base.
 */

ExportedByDNBBuild scl_string::size_type
DNBGetNumeric( const scl_string &source, short &value,
    scl_string::size_type spos = 0, int base = 10 )
    DNB_THROW_SPEC((scl_range_error));

/**
 *  Starting from position <spos> in string <source>, extract a value
 *  This function returns the position of source where the scan stopped.
 *  @param source
 *      The source string of type scl_string
 *  @param value
 *      intergral values that is assigned to value
 *  @param spos
 *      Position where the scan starts
 *  @param base
 *      Base between 2 and 36. If base is 0, the initial characters of the string
 *      are used to determine the base.
 */

ExportedByDNBBuild scl_string::size_type
DNBGetNumeric( const scl_string &source, int &value,
    scl_string::size_type spos = 0, int base = 10 )
    DNB_THROW_SPEC((scl_range_error));

/**
 *  Starting from position <spos> in string <source>, extract a value
 *  This function returns the position of source where the scan stopped.
 *  @param source
 *      The source string of type scl_string
 *  @param value
 *      intergral values that is assigned to value, type long
 *  @param spos
 *      Position where the scan starts
 *  @param base
 *      Base between 2 and 36. If base is 0, the initial characters of the string
 *      are used to determine the base.
 */

ExportedByDNBBuild scl_string::size_type
DNBGetNumeric( const scl_string &source, long &value,
    scl_string::size_type spos = 0, int base = 10 )
    DNB_THROW_SPEC((scl_range_error));

/**
 *  Starting from position <spos> in string <source>, extract a value.
 *  This function returns the position of source where the scan stopped.
 *  @param source
 *      The source string of type scl_string
 *  @param value
 *      intergral values that is assigned to value, type unsigned char
 *  @param spos
 *      Position where the scan starts
 *  @param base
 *      Base between 2 and 36. If base is 0, the initial characters of the string
 *      are used to determine the base.
 */

ExportedByDNBBuild scl_string::size_type
DNBGetNumeric( const scl_string &source, unsigned char &value,
    scl_string::size_type spos = 0, int base = 10 )
    DNB_THROW_SPEC((scl_range_error));

/**
 *  This function returns the position of source where the scan stopped.
 *  Starting from position <spos> in string <source> it extracts a value.
 *  @param source
 *      The source string of type scl_string
 *  @param value
 *      intergral values that is assigned to value, type unsigned short
 *  @param spos
 *      Position where the scan starts
 *  @param base
 *      Base between 2 and 36. If base is 0, the initial characters of the string
 *      are used to determine the base.
 */

ExportedByDNBBuild scl_string::size_type
DNBGetNumeric( const scl_string &source, unsigned short &value,
    scl_string::size_type spos = 0, int base = 10 )
    DNB_THROW_SPEC((scl_range_error));

/**
 *  This function returns the position of source where the scan stopped.
 *  Starting from position <spos> in string <source> it extracts a value
 *  @param source
 *      The source string of type scl_string
 *  @param value
 *      intergral values that is assigned to value, type unsigned int
 *  @param spos
 *      Position where the scan starts
 *  @param base
 *      Base between 2 and 36. If base is 0, the initial characters of the string
 *      are used to determine the base.
 */

ExportedByDNBBuild scl_string::size_type
DNBGetNumeric( const scl_string &source, unsigned int &value,
    scl_string::size_type spos = 0, int base = 10 )
    DNB_THROW_SPEC((scl_range_error));

/**
 *  This function returns the position in the source string where the scan stopped.
 *  Starting from position <spos> in string <source> it extracts a value
 *  @param source
 *      The source string of type scl_string
 *  @param value
 *      values that is assigned of type unsigned long
 *  @param spos
 *      Position where the scan starts
 *  @param base
 *      Base between 2 and 36. If base is 0, the initial characters of the string
 *      are used to determine the base.
 */

ExportedByDNBBuild scl_string::size_type
DNBGetNumeric( const scl_string &source, unsigned long &value,
    scl_string::size_type spos = 0, int base = 10 )
    DNB_THROW_SPEC((scl_range_error));

/**
 *  This function returns the position in the source string where the scan stopped.
 *  Starting from position <spos> in string <source> it extracts a value
 *  @param source
 *      The source string of type scl_string
 *  @param value
 *      values that is assigned of type unsigned long
 *  @param spos
 *      Position where the scan starts
 *  @param base
 *      Base between 2 and 36. If base is 0, the initial characters of the string
 *      are used to determine the base.
 */

ExportedByDNBBuild scl_string::size_type
DNBGetNumeric( const scl_string &source, float &value,
    scl_string::size_type spos = 0, int base = 10 )
    DNB_THROW_SPEC((scl_range_error));

/**
 *  This function returns the position in the source string where the scan stopped.
 *  Starting from position <spos> in string <source> it extracts a value.
 *  @param source
 *      The source string of type scl_string
 *  @param value
 *      values that is assigned of type unsigned long
 *  @param spos
 *      Position where the scan starts
 *  @param base
 *      Base between 2 and 36. If base is 0, the initial characters of the string
 *      are used to determine the base.
 */

ExportedByDNBBuild scl_string::size_type
DNBGetNumeric( const scl_string &source, double &value,
    scl_string::size_type spos = 0, int base = 10 )
    DNB_THROW_SPEC((scl_range_error));


/*************************wstring**************************/

/**
 *  This function returns the position in the wide char string where the scan stopped.
 *  Starting from position <spos> in string <source> it extracts a value.
 *  @param source
 *      The source string of type scl_wstring
 *  @param value
 *      values that is assigned of type unsigned short
 *  @param spos
 *      Position where the scan starts
 *  @param base
 *      Base between 2 and 36. If base is 0, the initial characters of the string
 *      are used to determine the base.
 */
ExportedByDNBBuild scl_string::size_type
DNBGetNumeric( const scl_wstring &source, wchar_t &value,
    scl_string::size_type spos = 0, int base = 10 )
    DNB_THROW_SPEC((scl_range_error));

/**
 *  This function returns the position in the wide char string where the scan stopped.
 *  Starting from position <spos> in string <source> it extracts a value.
 *  @param source
 *      The source string of type scl_wstring
 *  @param value
 *      values that is assigned of type short
 *  @param spos
 *      Position where the scan starts
 *  @param base
 *      Base between 2 and 36. If base is 0, the initial characters of the string
 *      are used to determine the base.
 */

ExportedByDNBBuild scl_string::size_type
DNBGetNumeric( const scl_wstring &source, short &value,
    scl_string::size_type spos = 0, int base = 10 )
    DNB_THROW_SPEC((scl_range_error));

/**
 *  This function returns the position in the wide char string where the scan stopped.
 *  Starting from position <spos> in string <source> it extracts a value.
 *  @param source
 *      The source string of type scl_wstring
 *  @param value
 *      values that is assigned of type intger
 *  @param spos
 *      Position where the scan starts
 *  @param base
 *      Base between 2 and 36. If base is 0, the initial characters of the string
 *      are used to determine the base.
 */

ExportedByDNBBuild scl_string::size_type
DNBGetNumeric( const scl_wstring &source, int &value,
    scl_string::size_type spos = 0, int base = 10 )
    DNB_THROW_SPEC((scl_range_error));

/**
 *  This function returns the position in the wide char string where the scan stopped.
 *  Starting from position <spos> in string <source> it extracts a value.
 *  @param source
 *      The source string of type scl_wstring
 *  @param value
 *      values that is assigned of type long
 *  @param spos
 *      Position where the scan starts
 *  @param base
 *      Base between 2 and 36. If base is 0, the initial characters of the string
 *      are used to determine the base.
 */

ExportedByDNBBuild scl_string::size_type
DNBGetNumeric( const scl_wstring &source, long &value,
    scl_string::size_type spos = 0, int base = 10 )
    DNB_THROW_SPEC((scl_range_error));

/**
 *  This function returns the position in the wide char string where the scan stopped.
 *  Starting from position <spos> in string <source> it extracts a value.
 *  @param source
 *      The source string of type scl_wstring
 *  @param value
 *      values that is assigned of type unsigned char
 *  @param spos
 *      Position where the scan starts
 *  @param base
 *      Base between 2 and 36. If base is 0, the initial characters of the string
 *      are used to determine the base.
 */

ExportedByDNBBuild scl_string::size_type
DNBGetNumeric( const scl_wstring &source, unsigned char &value,
    scl_string::size_type spos = 0, int base = 10 )
    DNB_THROW_SPEC((scl_range_error));

/**
 *  This function returns the position in the wide char string where the scan stopped.
 *  Starting from position <spos> in string <source> it extracts a value.
 *  @param source
 *      The source string of type scl_wstring
 *  @param value
 *      values that is assigned of type unsigned int
 *  @param spos
 *      Position where the scan starts
 *  @param base
 *      Base between 2 and 36. If base is 0, the initial characters of the string
 *      are used to determine the base.
 */

ExportedByDNBBuild scl_string::size_type
DNBGetNumeric( const scl_wstring &source, unsigned int &value,
    scl_string::size_type spos = 0, int base = 10 )
    DNB_THROW_SPEC((scl_range_error));

/**
 *  This function returns the position in the wide char string where the scan stopped.
 *  Starting from position <spos> in string <source> it extracts a value.
 *  @param source
 *      The source string of type scl_wstring
 *  @param value
 *      values that is assigned of type unsigned long
 *  @param spos
 *      Position where the scan starts
 *  @param base
 *      Base between 2 and 36. If base is 0, the initial characters of the string
 *      are used to determine the base.
 */

ExportedByDNBBuild scl_string::size_type
DNBGetNumeric( const scl_wstring &source, unsigned long &value,
    scl_string::size_type spos = 0, int base = 10 )
    DNB_THROW_SPEC((scl_range_error));

/**
 *  This function returns the position in the wide char string where the scan stopped.
 *  Starting from position <spos> in string <source> it extracts a value.
 *  @param source
 *      The source string of type scl_wstring
 *  @param value
 *      values that is assigned of type float
 *  @param spos
 *      Position where the scan starts
 *  @param base
 *      Base between 2 and 36. If base is 0, the initial characters of the string
 *      are used to determine the base.
 */

ExportedByDNBBuild scl_string::size_type
DNBGetNumeric( const scl_wstring &source, float &value,
    scl_string::size_type spos = 0, int base = 10 )
    DNB_THROW_SPEC((scl_range_error));

/**
 *  This function returns the position in the wide char string where the scan stopped.
 *  Starting from position <spos> in string <source> it extracts a value.
 *  @param source
 *      The source string of type scl_wstring
 *  @param value
 *      values that is assigned of type double
 *  @param spos
 *      Position where the scan starts
 *  @param base
 *      Base between 2 and 36. If base is 0, the initial characters of the string
 *      are used to determine the base.
 */

ExportedByDNBBuild scl_string::size_type
DNBGetNumeric( const scl_wstring &source, double &value,
    scl_string::size_type spos = 0, int base = 10 )
    DNB_THROW_SPEC((scl_range_error));


/**
 * Starting from position <tpos> in string <target>, replace no more than
 * <rcount> occurrences of substring <search> by <replace>.  If <rcount> is
 * zero, replace all occurrences of <search>.  This function returns the
 * number of substrings replaced.
 * @param target
 *      Source String that is manipulated
 * @param search
 *      substring that is searched for in the source
 * @param replace
 *      substring that is replaced
 * @param tpos
 *      starting psoition for where the search begins in the source string.
 * @param rcount
 *      the number of occurrences of the substring
 */

ExportedByDNBBuild size_t
DNBReplace( scl_string &target, const scl_string &search,
    const scl_string &replace, scl_string::size_type tpos = 0, size_t rcount = 0 )
    DNB_THROW_SPEC_NULL;

/**
 * Starting from position <tpos> in string <target>, replace no more than
 * <rcount> occurrences of substring <search> by <replace>.  If <rcount> is
 * zero, replace all occurrences of <search>.  This function returns the
 * number of substrings replaced.
 * @param target
 *      Source String that is manipulated
 * @param search
 *      substring that is searched for in the source
 * @param replace
 *      substring that is replaced
 * @param tpos
 *      starting psoition for where the search begins in the source string.
 * @param rcount
 *      the number of occurrences of the substring
 */

ExportedByDNBBuild size_t
DNBReplace( scl_wstring &target, const scl_wstring &search,
    const scl_wstring &replace, scl_string::size_type tpos = 0, size_t rcount = 0 )
    DNB_THROW_SPEC_NULL;

/**
 * Time formatting routines.
 * @param target
 *      Source String that is manipulated
 * @param format
 *      char format which is a constant, they can be %a, %A etc..
 * @param stime
 *      structure of time
 */

ExportedByDNBBuild size_t
DNBStrftime( scl_string &target, const char *format, const struct tm &stime )
    DNB_THROW_SPEC_NULL;

/**
 * Time formatting routines.
 * @param target
 *      Source String that is manipulated
 * @param format
 *      wide char format which is a constant, they can be %a, %A etc..
 * @param stime
 *      structure of time
 */

ExportedByDNBBuild size_t
DNBStrftime( scl_wstring &target, const wchar_t *format, const struct tm &stime )
    DNB_THROW_SPEC_NULL;

/** 
 * Funtion to write formatted output using a pointer to a list of arguments.
 * @param target
 *      Source String that is manipulated
 * @param format
 *      char format which is a constant
 */

ExportedByDNBBuild int
DNBSprintf( scl_string &target, const char *format, ... )
    DNB_THROW_SPEC_NULL;

/** 
 * Funtion to write formatted output using a pointer to a list of arguments.
 * @param target
 *      Source String that is manipulated
 * @param format
 *      wide char format which is a constant
 */

ExportedByDNBBuild int
DNBSprintf( scl_wstring &target, const wchar_t *format, ... )
    DNB_THROW_SPEC_NULL;

#endif  // _DNBSTRINGUTIL_H_


