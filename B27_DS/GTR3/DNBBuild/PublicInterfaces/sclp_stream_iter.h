/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: sclp_stream_iter.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Oct-2004     Initial implementation.
//
//  NOTICE:
//      This header is used internally by the Standard C++ Library.
//      It should not be included directly in application code.
//
//==============================================================================
#ifndef _SCLP_STREAM_ITER_H_
#define _SCLP_STREAM_ITER_H_


//
//  Include section
//
#ifndef  _SCLP_BASE_ITER_H_
#include <sclp_base_iter.h>             // for scl_iterator and friends
#endif

#ifndef  _SCLP_CHAR_TRAITS_H_
#include <sclp_char_traits.h>           // for scl_char_traits
#endif

#ifndef  _SCL_IOSTREAM_H_
#include <scl_iostream.h>               // for istream, ostream
#endif


//
//  24.5.1 Class template scl_istream_iterator
//  Note: This class is used only with single-byte streams.
//
template <class Type_>
class scl_istream_iterator : public
    scl_iterator<scl_input_iterator_tag, Type_, ptrdiff_t, const Type_*, const Type_&>
{
public:
    // Private types
    typedef
    scl_iterator<scl_input_iterator_tag, Type_, ptrdiff_t, const Type_*, const Type_&>
                                                Base_;
    typedef scl_istream_iterator<Type_>         Self_;

    // Public types
    typedef char                                char_type;
    typedef scl_char_traits<char>               traits_type;
    typedef istream                             istream_type;
    SCL_ITERATOR_TYPEDEFS(Base_);

    scl_istream_iterator() :
        stream_(NULL),
        value_ ()
    {
        // Nothing
    }

    scl_istream_iterator(istream_type& stream) :
        stream_(&stream),
        value_ ()
    {
        read_value();
    }

    scl_istream_iterator(const Self_& other) :
        stream_(other.stream_),
        value_ (other.value_)
    {
        // Nothing
    }

    ~scl_istream_iterator()
    {
        // Nothing
    }

    const Type_& operator*() const
    {
        return value_;
    }

    const Type_* operator->() const
    {
        return &value_;
    }

    Self_& operator++()
    {
        read_value();
        return *this;
    }

    Self_  operator++(int)
    {
        Self_   tmp(*this);
        read_value();
        return tmp;
    }

    // Internal helper method.
    bool _equal(const Self_& other) const
    {
        return (stream_ == other.stream_);
    }

private:
    void read_value()
    {
        if (stream_ == NULL)
            return;

        if (!(*stream_ >> value_))
            stream_ = NULL;
    }

    // Data members
    istream_type*   stream_;
    Type_           value_;
};


template <class Type_>
bool
operator==(const scl_istream_iterator<Type_>& lhs,
           const scl_istream_iterator<Type_>& rhs)
{
    return (lhs._equal(rhs));
}

template <class Type_>
bool
operator!=(const scl_istream_iterator<Type_>& lhs,
           const scl_istream_iterator<Type_>& rhs)
{
    return (!lhs._equal(rhs));
}


//
//  24.5.2 Class template scl_ostream_iterator
//  Note: This class is used only with single-byte streams.
//
template <class Type_>
class scl_ostream_iterator : public
    scl_iterator<scl_output_iterator_tag, void, void, void, void>
{
public:
    // Private types
    typedef
    scl_iterator<scl_output_iterator_tag, void, void, void, void>
                                                Base_;
    typedef scl_ostream_iterator<Type_>         Self_;

    // Public types
    typedef char                                char_type;
    typedef scl_char_traits<char>               traits_type;
    typedef ostream                             ostream_type;
    SCL_ITERATOR_TYPEDEFS(Base_);

    scl_ostream_iterator(ostream_type& stream) :
        stream_(&stream),
        delim_ (NULL)
    {
        // Nothing
    }

    scl_ostream_iterator(ostream_type& stream, const char_type* delim) :
        stream_(&stream),
        delim_ (delim)
    {
        // Nothing
    }

    scl_ostream_iterator(const Self_& other) :
        stream_(other.stream_),
        delim_ (other.delim_)
    {
        // Nothing
    }

    ~scl_ostream_iterator()
    {
        // Nothing
    }

    Self_& operator=(const Type_& value)
    {
        *stream_ << value;
        if (delim_ != NULL)
            *stream_ << delim_;
        return *this;
    }

    Self_& operator*()
    {
        return *this;
    }

    Self_& operator++()
    {
        return *this;
    }

    Self_& operator++(int)
    {
        return *this;
    }

private:
    ostream_type*       stream_;
    const char_type*    delim_;
};


#endif  /* _SCLP_STREAM_ITER_H_ */
