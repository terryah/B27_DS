/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 1997
//==============================================================================
//
//  FILE: DNBSystemDefs.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     23-Jun-1997     Initial implementation.
//      jod     01-Jul-2008     Simplified contents.
//
//  OVERVIEW:
//      This header file defines several constants, typedefs, macros and functions,
//      which are designed to enhance cross-platform compatibility.
//
//==============================================================================
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNBSYSTEMDEFS_H_
#define _DNBSYSTEMDEFS_H_


#ifndef _DNBSYSTEMBASE_H_
#error  "The header <DNBSystemBase.h> has not been included."
#endif


//
//  Include section
//
#ifndef  _SCL_IOSFWD_H_
#include <scl_iosfwd.h>                 // for ostream
#endif




        //************************************************************//
        //************************************************************//
        //*                                                          *//
        //*                    MANIFEST CONSTANTS                    *//
        //*                                                          *//
        //************************************************************//
        //************************************************************//


/**
 *  Return value from a successful system call.
 *
 *  @description
 *      This constant denotes the error status value that is returned when a
 *      system call succeeds.
 */
const int       DNBStatusOkay = 0;




/**
 *  Return value from a failed system call.
 *
 *  @description
 *      This constant denotes the error status value that is returned when a
 *      system call detects an error.
 */
const int       DNBStatusError = -1;




/**
 *  Terminator for single-byte character strings.
 *
 *  @description
 *      This constant denotes the end-of-string sentinel for single-byte
 *      character strings.
 */
const char      DNBEOS = '\0';




/**
 *  Terminator for wide-character strings.
 *
 *  @description
 *      This constant denotes the end-of-string sentinel for wide-character
 *      strings.
 */
const wchar_t   DNBWEOS = L'\0';




/**
 *  Null pointer value.
 *
 *  @description
 *      This symbolic constant represents the null-pointer value, which is used
 *      in many pointer operations and functions.
 */
#undef  NULL
#define NULL    0




/**
 *  File descriptor associated with standard input.
 *
 *  @description
 *      This symbolic constant specifies the file descriptor associated with the
 *      standard input device.  On most UNIX systems, this macro is defined in
 *      the header &lt;unistd.h&gt;.
 */
#ifndef STDIN_FILENO
#define STDIN_FILENO    0
#endif




/**
 *  File descriptor associated with standard output.
 *
 *  @description
 *      This symbolic constant specifies the file descriptor associated with the
 *      standard output device.  On most UNIX systems, this macro is defined in
 *      the header &lt;unistd.h&gt;.
 */
#ifndef STDOUT_FILENO
#define STDOUT_FILENO   1
#endif




/**
 *  File descriptor associated with standard error.
 *
 *  @description
 *      This symbolic constant specifies the file descriptor associated with the
 *      standard error device.  On most UNIX systems, this macro is defined in
 *      the header &lt;unistd.h&gt;.
 */
#ifndef STDERR_FILENO
#define STDERR_FILENO   2
#endif




        //************************************************************//
        //************************************************************//
        //*                                                          *//
        //*                 NUMERIC TYPE DEFINITIONS                 *//
        //*                                                          *//
        //************************************************************//
        //************************************************************//


/**
 *  Type definition for an unsigned 8-bit integer with the range
 *  0 to 255.
 */
typedef unsigned char           DNBByte;




/**
 *  Type definition for a signed 8-bit integer with the range
 *  -128 to 127.
 */
typedef signed char             DNBInteger8;




/**
 *  Type definition for a signed 16-bit integer with the range
 *  -32,768 to 32,767.
 */
typedef signed short            DNBInteger16;




/**
 *  Type definition for a signed 32-bit integer with the range
 *  -2,147,483,648 to 2,147,483,647.
 */
typedef signed int              DNBInteger32;




/**
 *  Type definition for a signed 64-bit integer with the range
 *  -9,223,372,036,854,775,808 to 9,223,372,036,854,775,807.
 */
typedef SCL_LLONG_TYPE          DNBInteger64;




/**
 *  Type definition for an unsigned 8-bit integer with the range
 *  0 to 255.
 */
typedef unsigned char           DNBUnsigned8;




/**
 *  Type definition for an unsigned 16-bit integer with the range
 *  0 to 65,535.
 */
typedef unsigned short          DNBUnsigned16;




/**
 *  Type definition for an unsigned 32-bit integer with the range
 *  0 to 4,294,967,295.
 */
typedef unsigned int            DNBUnsigned32;




/**
 *  Type definition for an unsigned 64-bit integer with the range
 *  0 to 18,446,744,073,709,551,615.
 */
typedef unsigned SCL_LLONG_TYPE DNBUnsigned64;




/**
 *  Type definition for a simulation floating-point value.
 *
 *  @description
 *      This type definition represents the default floating-point type used in the
 *      DELMIA simulation toolkit.  In general, application code should not declare
 *      variables of type float or double, unless interfacing to third-party toolkits.
 */
typedef double                  DNBReal;




        //************************************************************//
        //************************************************************//
        //*                                                          *//
        //*                     MACRO DEFINITIONS                    *//
        //*                                                          *//
        //************************************************************//
        //************************************************************//


/**
 *  Defines a condition that must hold when executing a function.
 *
 *  @description
 *      This macro provides a convenient mechanism for checking assumptions during
 *      the debugging process.  It is used primarily to check the correctness of the
 *      internal state of a function.  By using the macro, software assertions can
 *      be conditionally compiled in library code, without enclosing them in #ifdef
 *      statements.  This flexibility is achieved by using the macro @href DNB_VERIFY.
 *      If DNB_VERIFY is TRUE, calls to DNB_ASSERT are expanded in the source code.
 *      Otherwise, calls to DNB_ASSERT are removed during preprocessing.
 *      <p>
 *      If software assertions have been enabled, DNB_ASSERT evaluates the Boolean
 *      expression EXPR_.  If EXPR_ is false (0), the macro prints a diagnostic
 *      message on the standard error stream and terminates the program.  If EXPR_ is
 *      true (non-zero), the macro does nothing.
 *      <p>
 *      By convention, software assertions are disabled in production code.  For this
 *      reason, DNB_ASSERT should never be used to define an expression that contains
 *      side-effects.
 *
 *  @param EXPR_
 *      A Boolean expression that is expected to be true.
 *
 *  @example
 *  <pre>
 *      void
 *      DNBCompute( unsigned arg )
 *      {
 *          unsigned    value = arg << 1;
 *          DNB_ASSERT( value == (2 * arg) );
 *          ...
 *      }
 *  </pre>
 */
#define DNB_ASSERT(EXPR_)               _DNB_ASSERT("Assertion", EXPR_)




/**
 *  Defines a condition that must hold when entering a function.
 *
 *  @description
 *      This macro provides a convenient mechanism for checking assumptions during
 *      the debugging process.  It is used primarily to check the correctness of
 *      input arguments to a function.  By using the macro, software assertions can
 *      be conditionally compiled in library code, without enclosing them in #ifdef
 *      statements.  This flexibility is achieved by using the macro @href DNB_VERIFY.
 *      If DNB_VERIFY is TRUE, calls to DNB_PRECONDITION are expanded in the source
 *      code.  Otherwise, calls to DNB_PRECONDITION are removed during preprocessing.
 *      <p>
 *      If software assertions have been enabled, DNB_PRECONDITION evaluates the
 *      Boolean expression EXPR_.  If EXPR_ is false (0), the macro prints a diagnostic
 *      message on the standard error stream and terminates the program.  If EXPR_ is
 *      true (non-zero), the macro does nothing.
 *      <p>
 *      By convention, software assertions are disabled in production code.  For this
 *      reason, DNB_PRECONDITION should never be used to define an expression that
 *      contains side-effects.
 *
 *  @param EXPR_
 *      A Boolean expression that is expected to be true.
 *
 *  @example
 *  <pre>
 *      double
 *      Sqrt( double arg )
 *      {
 *          DNB_PRECONDITION( arg >= 0.0 );
 *          return sqrt( arg );
 *      }
 *  </pre>
 */
#define DNB_PRECONDITION(EXPR_)         _DNB_ASSERT("Precondition", EXPR_)




/**
 *  Defines a condition that must hold when exiting a function.
 *
 *  @description
 *      This macro provides a convenient mechanism for checking assumptions during
 *      the debugging process.  It is used primarily to check the correctness of
 *      output arguments of a function.  By using the macro, software assertions can
 *      be conditionally compiled in library code, without enclosing them in #ifdef
 *      statements.  This flexibility is achieved by using the macro @href DNB_VERIFY.
 *      If DNB_VERIFY is TRUE, calls to DNB_POSTCONDITION are expanded in the source
 *      code.  Otherwise, calls to DNB_POSTCONDITION are removed during preprocessing.
 *      <p>
 *      If software assertions have been enabled, DNB_POSTCONDITION evaluates the
 *      Boolean expression EXPR_.  If EXPR_ is false (0), the macro prints a diagnostic
 *      message on the standard error stream and terminates the program.  If EXPR_ is
 *      true (non-zero), the macro does nothing.
 *      <p>
 *      By convention, software assertions are disabled in production code.  For this
 *      reason, DNB_POSTCONDITION should never be used to define an expression that
 *      contains side-effects.
 *
 *  @param EXPR_
 *      A Boolean expression that is expected to be true.
 *
 *  @example
 *  <pre>
 *      unsigned
 *      DoubleIt( unsigned arg )
 *      {
 *          unsigned    result = arg << 1;
 *          DNB_POSTCONDITION( result == (2 * arg) );
 *          return result;
 *      }
 *  </pre>
 */
#define DNB_POSTCONDITION(EXPR_)        _DNB_ASSERT("Postcondition", EXPR_)




/**
 *  Specifies that an erroneous condition has occurred in a function.
 *
 *  @description
 *      This macro provides a convenient mechanism for indicating erroneous
 *      conditions in a function.  It should be called whenever a serious
 *      inconsistency has been detected during program execution.  When the
 *      macro is invoked, it prints a diagnostic message on the standard error
 *      stream and terminates the program.
 *      <p>
 *      Unlike software assertions, this macro is always enabled.  Thus, it
 *      will terminate the program in both debug and production builds.
 *
 *  @param MSG_
 *      A single-byte character string (SBCS) that specifies the nature of
 *      the error.
 *
 *  @example
 *  <pre>
 *      bool
 *      IsEven( int arg )
 *      {
 *          if ( arg % 2 == 0 )
 *              return true;
 *          else
 *              return false;
 *
 *          DNB_PROGRAM_ERROR( "Invalid condition" );
 *      }
 *  </pre>
 */
#define DNB_PROGRAM_ERROR(MSG_)         SCL_PROGRAM_ERROR(MSG_)




/**
 * @nodoc
 */
#define DNB_ASSERT_ALWAYS(EXPR_)        _SCL_DO_ASSERT("Assertion", EXPR_)




//
//  Validates a test condition in a batch ODT.
//  Note: This macro can be used only in TEST frameworks.
//
/**
 * @nodoc
 */
#define DNB_ODT_CHECK(EXPR_)            _SCL_DO_ASSERT("Test Condition", EXPR_)




/**
 * @nodoc
 */
#if     DNB_VERIFY
#define _DNB_ASSERT(MSG_, EXPR_)        _SCL_DO_ASSERT(MSG_, EXPR_)
#else
#define _DNB_ASSERT(MSG_, EXPR_)        ((void) 0)
#endif




/**
 *  Specifies a comma separator in a macro parameter.
 *
 *  @description
 *      This macro is used to embed a literal comma (,) in the parameter of a macro
 *      call.  Consider a macro that accepts only one parameter.  If the macro is
 *      called with a parameter that contains a comma, some compilers will interpret
 *      this as two macro parameters.  To remedy the situation, use DNB_COMMA in
 *      place of the literal comma separator.
 *
 *  @example
 *  <pre>
 *      #define MACRO(EXPR_)    EXPR_
 *
 *      void
 *      MyFunc()
 *      {
 *          MyClass&lt;int, double&gt;              obj1;
 *          MACRO( MyClass&lt;int DNB_COMMA double&gt; )  obj2;
 *          ...
 *      }
 *  </pre>
 */
#define DNB_COMMA   ,




        //************************************************************//
        //************************************************************//
        //*                                                          *//
        //*                    COMPATIBILITY CRUFT                   *//
        //*                                                          *//
        //************************************************************//
        //************************************************************//


/**
 *  Simulates the const_cast operator.
 *
 *  @description
 *      This macro is deprecated; use the const_cast operator directly.
 */
#define DNB_CONST_CAST(TYPE_, EXPR_)        const_cast<TYPE_ >(EXPR_)




/**
 *  Simulates the static_cast operator.
 *
 *  @description
 *      This macro is deprecated; use the static_cast operator directly.
 */
#define DNB_STATIC_CAST(TYPE_, EXPR_)       static_cast<TYPE_ >(EXPR_)




/**
 *  Simulates the reinterpret_cast operator.
 *
 *  @description
 *      This macro is deprecated; use the reinterpret_cast operator directly.
 */
#define DNB_REINTERPRET_CAST(TYPE_, EXPR_)  reinterpret_cast<TYPE_ >(EXPR_)




/**
 *  Simulates the semantics of mutable data members.
 *
 *  @description
 *      This macro is deprecated; use the mutable keyword directly.
 */
#define DNB_MODIFY_MUTABLE(TYPE_, EXPR_)    (EXPR_)




/**
 *  Specifies that a function does not throw an exception.
 *
 *  @example
 *  <pre>
 *      void
 *      MyFunc( int arg )
 *          DNB_THROW_SPEC_NULL
 *      {
 *          ...
 *      }
 *  </pre>
 */
#if     DNB_HAS_THROW_SPECS
#define DNB_THROW_SPEC_NULL         throw ()
#else
#define DNB_THROW_SPEC_NULL         /* nothing */
#endif




/**
 *  Specifies that a function can throw an exception of certain types.
 *
 *  @param EXLIST_
 *      A comma-separated list of exception classes, which are enclosed in parentheses.
 *
 *  @example
 *  <pre>
 *      void
 *      MyFunc( int arg )
 *          DNB_THROW_SPEC((int, double))
 *      {
 *          ...
 *          throw (5);
 *      }
 *  </pre>
 */
#if     DNB_HAS_THROW_SPECS
#define DNB_THROW_SPEC(EXLIST_)     throw EXLIST_
#else
#define DNB_THROW_SPEC(EXLIST_)     /* nothing */
#endif




/**
 *  Specifies that a function can throw an exception of any type.
 *
 *  @example
 *  <pre>
 *      void
 *      MyFunc( int arg )
 *          DNB_THROW_SPEC_ANY
 *      {
 *          ...
 *          throw ("Error Condition");
 *      }
 *  </pre>
 */
#define DNB_THROW_SPEC_ANY          /* nothing */




/**
 *  Specifies the allocator class used in a Standard C++ Library container.
 *
 *  @param TYPE_
 *      Type of object stored in the container class.
 *
 *  @example
 *  <pre>
 *      typedef scl_vector&lt;int, DNB_ALLOCATOR(int)&gt;   MyVector;
 *      MyVector    vec;
 *  </pre>
 */
#define DNB_ALLOCATOR(TYPE_)        scl_allocator<TYPE_ >




//
//  Defines a specialization of a class template.
//
/**
 * @nodoc
 */
#define DNB_DEFINE_CLASS_SPEC       SCL_SPECIALIZE_CLASS class




//
//  Defines an explicit instantiation of a class template.
//
/**
 * @nodoc
 */
#define DNB_DEFINE_CLASS_INST       template class




#endif  /* _DNBSYSTEMDEFS_H_ */
