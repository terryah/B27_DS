/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: sclp_pointer_iter.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Aug-2004     Initial implementation.
//
//  NOTICE:
//      This header is used internally by the Standard C++ Library.
//      It should not be included directly in application code.
//
//==============================================================================
#ifndef _SCLP_POINTER_ITER_H_
#define _SCLP_POINTER_ITER_H_


//
//  Include section
//
#ifndef  _SCLP_BASE_ITER_H_
#include <sclp_base_iter.h>
#endif


//
//  Internal class _scl_pointer_iterator
//
template <class Type_>
class _scl_pointer_iterator
{
public:
    typedef _scl_pointer_iterator<Type_>    Self_;
    typedef const Type_ *                   Pointer_;

    Pointer_ base_aux() const
    {
        return current_;
    }

#ifdef  _SUNOS_SOURCE
// Required to suppress Sun warnings about inaccessible copy constructor.
public:
#else
protected:
#endif
    _scl_pointer_iterator() :
        current_(NULL)
    {
        // Nothing
    }

    _scl_pointer_iterator(Pointer_ ptr) :
        current_(ptr)
    {
        // Nothing
    }

    _scl_pointer_iterator(const Self_& other) :
        current_(other.current_)
    {
        // Nothing
    }

    // assignment operators
    void operator=(Pointer_ ptr)
    {
        current_ = ptr;
    }

    void operator=(const Self_& other)
    {
        current_ = other.current_;
    }

    ~_scl_pointer_iterator()
    {
        current_ = NULL;
    }

    Pointer_    current_;
};


template <class Type_>
bool
operator==(const _scl_pointer_iterator<Type_>& lhs,
           const _scl_pointer_iterator<Type_>& rhs)
{
    return (lhs.base_aux() == rhs.base_aux());
}

template <class Type_>
bool
operator< (const _scl_pointer_iterator<Type_>& lhs,
           const _scl_pointer_iterator<Type_>& rhs)
{
    return (lhs.base_aux() <  rhs.base_aux());
}

template <class Type_>
bool
operator!=(const _scl_pointer_iterator<Type_>& lhs,
           const _scl_pointer_iterator<Type_>& rhs)
{
    return (lhs.base_aux() != rhs.base_aux());
}

template <class Type_>
bool
operator> (const _scl_pointer_iterator<Type_>& lhs,
           const _scl_pointer_iterator<Type_>& rhs)
{
    return (lhs.base_aux() >  rhs.base_aux());
}

template <class Type_>
bool
operator>=(const _scl_pointer_iterator<Type_>& lhs,
           const _scl_pointer_iterator<Type_>& rhs)
{
    return (lhs.base_aux() >= rhs.base_aux());
}

template <class Type_>
bool
operator<=(const _scl_pointer_iterator<Type_>& lhs,
           const _scl_pointer_iterator<Type_>& rhs)
{
    return (lhs.base_aux() <= rhs.base_aux());
}

template <class Type_>
ptrdiff_t
operator-(const _scl_pointer_iterator<Type_>& lhs,
          const _scl_pointer_iterator<Type_>& rhs)
{
    return (lhs.base_aux() - rhs.base_aux());
}


//
//  Class scl_pointer_iterator
//  This class converts a raw pointer to an iterator.
//
template <class Type_>
class scl_pointer_iterator : public _scl_pointer_iterator<Type_>
{
public:
    typedef _scl_pointer_iterator<Type_>    Base_;
    typedef scl_pointer_iterator<Type_>     Self_;

    // iterator types
    typedef scl_random_access_iterator_tag  iterator_category;
    typedef Type_                           value_type;
    typedef ptrdiff_t                       difference_type;
    typedef Type_ *                         pointer;
    typedef Type_ &                         reference;

    // construction, assignment, destruction
    scl_pointer_iterator() :
        Base_(NULL)
    {
        // Nothing
    }

    scl_pointer_iterator(pointer ptr) :
        Base_(ptr)
    {
        // Nothing
    }

    scl_pointer_iterator(const Self_& other) :
        Base_(other)
    {
        // Nothing
    }

    Self_& operator=(pointer ptr)
    {
        Base_::operator=(ptr);
        return *this;
    }

    Self_& operator=(const Self_& other)
    {
        Base_::operator=(other);
        return *this;
    }

    ~scl_pointer_iterator()
    {
        // Nothing
    }

    pointer base() const
    {
        return const_cast<Type_*>(this->current_);
    }

    // forward iterator requirements
    reference operator*() const
    {
        return *base();
    }

    pointer operator->() const
    {
        return  base();
    }

    Self_& operator++()
    {
        ++this->current_;
        return *this;
    }

    Self_ operator++(int)
    {
        Self_   tmp(*this);
        ++this->current_;
        return tmp;
    }

    // Bidirectional iterator requirements
    Self_& operator--()
    {
        --this->current_;
        return *this;
    }

    Self_ operator--(int)
    {
        Self_   tmp(*this);
        --this->current_;
        return tmp;
    }

    // Random access iterator requirements
    Self_  operator+ (difference_type rhs) const
    {
        return Self_(base() + rhs);
    }

    Self_& operator+=(difference_type rhs)
    {
        this->current_ += rhs;
        return *this;
    }

    Self_  operator- (difference_type rhs) const
    {
        return Self_(base() - rhs);
    }

    Self_& operator-=(difference_type rhs)
    {
        this->current_ -= rhs;
        return *this;
    }

    reference operator[](difference_type rhs) const
    {
        return *(base() + rhs);
    }
};


template <class Type_>
scl_pointer_iterator<Type_>
operator+(ptrdiff_t lhs, const scl_pointer_iterator<Type_>& rhs)
{
    return scl_pointer_iterator<Type_>(rhs.base() + lhs);
}


//
//  Class scl_pointer_const_iterator
//  This class converts a raw pointer to a const iterator.
//
template <class Type_>
class scl_pointer_const_iterator : public _scl_pointer_iterator<Type_>
{
public:
    typedef _scl_pointer_iterator<Type_>        Base_;
    typedef scl_pointer_const_iterator<Type_>   Self_;
    typedef scl_pointer_iterator<Type_>         Iterator_;

    // iterator types
    typedef scl_random_access_iterator_tag  iterator_category;
    typedef Type_                           value_type;
    typedef ptrdiff_t                       difference_type;
    typedef const Type_ *                   pointer;
    typedef const Type_ &                   reference;

    // construction, assignment, destruction
    scl_pointer_const_iterator() :
        Base_(NULL)
    {
        // Nothing
    }

    scl_pointer_const_iterator(pointer ptr) :
        Base_(ptr)
    {
        // Nothing
    }

    scl_pointer_const_iterator(const Self_& other) :
        Base_(other)
    {
        // Nothing
    }

    scl_pointer_const_iterator(const Iterator_& other) :
        Base_(other)
    {
        // Nothing
    }

    Self_& operator=(pointer ptr)
    {
        Base_::operator=(ptr);
        return *this;
    }

    Self_& operator=(const Self_& other)
    {
        Base_::operator=(other);
        return *this;
    }

    Self_& operator=(const Iterator_& other)
    {
        Base_::operator=(other);
        return *this;
    }

    ~scl_pointer_const_iterator()
    {
        // Nothing
    }

    pointer base() const
    {
        return this->current_;
    }

    // forward iterator requirements
    reference operator*() const
    {
        return *base();
    }

    pointer operator->() const
    {
        return  base();
    }

    Self_& operator++()
    {
        ++this->current_;
        return *this;
    }

    Self_ operator++(int)
    {
        Self_   tmp(*this);
        ++this->current_;
        return tmp;
    }

    // Bidirectional iterator requirements
    Self_& operator--()
    {
        --this->current_;
        return *this;
    }

    Self_ operator--(int)
    {
        Self_   tmp(*this);
        --this->current_;
        return tmp;
    }

    // Random access iterator requirements
    Self_  operator+ (difference_type rhs) const
    {
        return Self_(base() + rhs);
    }

    Self_& operator+=(difference_type rhs)
    {
        this->current_ += rhs;
        return *this;
    }

    Self_  operator- (difference_type rhs) const
    {
        return Self_(base() - rhs);
    }

    Self_& operator-=(difference_type rhs)
    {
        this->current_ -= rhs;
        return *this;
    }

    reference operator[](difference_type rhs) const
    {
        return *(base() + rhs);
    }
};


template <class Type_>
scl_pointer_const_iterator<Type_>
operator+(ptrdiff_t lhs, const scl_pointer_const_iterator<Type_>& rhs)
{
    return scl_pointer_const_iterator<Type_>(rhs.base() + lhs);
}


#include <sclp_pointer_iter.cc>


#endif  /* _SCLP_POINTER_ITER_H_ */
