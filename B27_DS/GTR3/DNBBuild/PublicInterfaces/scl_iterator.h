/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: scl_iterator.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Aug-2004     Initial implementation.
//
//  NOTICE:
//      This header implements functionality in the Standard C++ Library.
//      It may be included directly in application code.
//
//==============================================================================
#ifndef _SCL_ITERATOR_H_
#define _SCL_ITERATOR_H_


#ifndef  _SCL_CONFIG_H_
#include <scl_config.h>
#endif


#ifdef  SCL_USE_VENDOR_STD
#include <iterator>

#if     defined(_IRIX_SOURCE) && (_COMPILER_VERSION == 721)
#define scl_iterator_traits             iterator_traits
#define scl_iterator                    iterator
#else
#define scl_iterator_traits             SCL_VENDOR_STD::iterator_traits
#define scl_iterator                    SCL_VENDOR_STD::iterator
#endif

#define scl_input_iterator_tag          SCL_VENDOR_STD::input_iterator_tag
#define scl_output_iterator_tag         SCL_VENDOR_STD::output_iterator_tag
#define scl_forward_iterator_tag        SCL_VENDOR_STD::forward_iterator_tag
#define scl_bidirectional_iterator_tag  SCL_VENDOR_STD::bidirectional_iterator_tag
#define scl_random_access_iterator_tag  SCL_VENDOR_STD::random_access_iterator_tag
#define scl_advance                     SCL_VENDOR_STD::advance
#define scl_distance                    SCL_VENDOR_STD::distance
#define scl_reverse_iterator            SCL_VENDOR_STD::reverse_iterator
#define scl_back_insert_iterator        SCL_VENDOR_STD::back_insert_iterator
#define scl_back_inserter               SCL_VENDOR_STD::back_inserter
#define scl_front_insert_iterator       SCL_VENDOR_STD::front_insert_iterator
#define scl_front_inserter              SCL_VENDOR_STD::front_inserter
#define scl_insert_iterator             SCL_VENDOR_STD::insert_iterator
#define scl_inserter                    SCL_VENDOR_STD::inserter
#define scl_istream_iterator            SCL_VENDOR_STD::istream_iterator
#define scl_ostream_iterator            SCL_VENDOR_STD::ostream_iterator

#else
#include <sclp_base_iter.h>
#include <sclp_insert_iter.h>

#ifdef  SCL_ENABLE_IO
#include <sclp_stream_iter.h>
#endif

#include <sclp_pointer_iter.h>

#endif  /* SCL_USE_VENDOR_STD */


#endif  /* _SCL_ITERATOR_H_ */
