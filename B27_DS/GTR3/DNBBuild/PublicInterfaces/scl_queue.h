/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: scl_queue.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Aug-2004     Initial implementation.
//
//  NOTICE:
//      This header implements functionality in the Standard C++ Library.
//      It may be included directly in application code.
//
//==============================================================================
#ifndef _SCL_QUEUE_H_
#define _SCL_QUEUE_H_


#ifndef  _SCL_CONFIG_H_
#include <scl_config.h>
#endif


#ifdef  SCL_USE_VENDOR_STD
#include <queue>

#define scl_queue                       SCL_VENDOR_STD::queue
#define scl_priority_queue              SCL_VENDOR_STD::priority_queue

#else
#include <sclp_queue.h>

#endif  /* SCL_USE_VENDOR_STD */


#endif  /* _SCL_QUEUE_H_ */
