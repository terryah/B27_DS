/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: scl_climits.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Aug-2004     Initial implementation.
//
//  NOTICE:
//      This header implements functionality in the Standard C++ Library.
//      It may be included directly in application code.
//
//==============================================================================
#ifndef _SCL_CLIMITS_H_
#define _SCL_CLIMITS_H_


#ifndef  _SCL_CONFIG_H_
#include <scl_config.h>
#endif

#include <limits.h>


// Limit of `size_t' type
#ifdef  PLATEFORME_DS64
#define SCL_SIZE_MAX            SCL_ULLONG_MAX
#else
#define SCL_SIZE_MAX            UINT_MAX
#endif


// Limits of `ptrdiff_t' type
#ifdef  PLATEFORME_DS64
#define SCL_PTRDIFF_MIN         SCL_LLONG_MIN
#define SCL_PTRDIFF_MAX         SCL_LLONG_MAX
#else
#define SCL_PTRDIFF_MIN         INT_MIN
#define SCL_PTRDIFF_MAX         INT_MAX
#endif


#endif  /* _SCL_CLIMITS_H_ */
