/** @CAA2Required */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: scl_config_aix.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Oct-2004     Initial implementation.
//
//  NOTICE:
//      This header is used internally by the Standard C++ Library.
//      It should not be included directly in application code.
//
//==============================================================================


#ifndef _LONG_LONG
#error  "Intrinsic long long type is disabled."
#endif


//
//  Compiler definitions.
//
#if     (__xlC__ >= 0x0500)
// VisualAge C++ 5.0.2 or later
#define SCL_HAS_BOOL
#define SCL_HAS_DISTINCT_BOOL
#define SCL_HAS_DISTINCT_WCHAR
#define SCL_HAS_IOSTREAM_WCHAR
#define SCL_HAS_LLONG
#define SCL_HAS_EXPLICIT
#define SCL_HAS_TYPENAME
#define SCL_HAS_NAMESPACES
#define SCL_HAS_RTTI
#define SCL_HAS_EXCEPTIONS
#define SCL_HAS_THROW_SPECS
#define SCL_HAS_CLASS_USING_DECL
#define SCL_HAS_DEFAULT_TMPL_ARGS
#define SCL_HAS_NONTYPE_TMPL_ARGS
#define SCL_HAS_FUNC_TMPL_ARGS
#undef  SCL_HAS_MEMBER_TEMPLATES
#undef  SCL_HAS_PARTIAL_SPEC
#define SCL_HAS_PARTIAL_SPEC_OVERLOAD
#define SCL_HAS_SPEC_SYNTAX
#undef  SCL_HAS_STATIC_CONST_INIT
#define SCL_HAS_EXPLICIT_CLASS_INST
#define SCL_HAS_EXPLICIT_FUNC_INST
#define SCL_HAS_STD_NEW
#define SCL_HAS_STD_EXCEPTION
#define SCL_HAS_STD_TYPEINFO
#define SCL_HAS_STD_IOSTREAM
#define SCL_HAS_WSTR_FUNCTIONS

#else
#error  "Unsupported compiler"

#endif  /* Compiler definitions */


//
//  Platform configuration.
//
#undef  SCL_USE_POOL_ALLOCATOR


//
//  Platform definitions.
//
#define SCL_BYTE_ORDER          SCL_BIG_ENDIAN
#define SCL_FLOAT_SIZE          32
#define SCL_DOUBLE_SIZE         64
#ifdef  __LONGDOUBLE128
#define SCL_LONG_DOUBLE_SIZE    128
#else
#define SCL_LONG_DOUBLE_SIZE    64
#endif

#define SCL_LLONG_TYPE          long long
#define SCL_LLONG_MIN           LONGLONG_MIN
#define SCL_LLONG_MAX           LONGLONG_MAX
#define SCL_LLONG_FMT           "%lld"
#define SCL_LLONG_CONST(_X)     SCL_CONCAT2(_X,LL)

#define SCL_ULLONG_TYPE         unsigned long long
#define SCL_ULLONG_MAX          ULONGLONG_MAX
#define SCL_ULLONG_FMT          "%llu"
#define SCL_ULLONG_CONST(_X)    SCL_CONCAT2(_X,ULL)
