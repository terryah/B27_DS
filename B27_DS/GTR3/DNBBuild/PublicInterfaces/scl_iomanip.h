/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: scl_iomanip.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Oct-2004     Initial implementation.
//
//  NOTICE:
//      This header implements functionality in the Standard C++ Library.
//      It may be included directly in application code.
//
//==============================================================================
#ifndef _SCL_IOMANIP_H_
#define _SCL_IOMANIP_H_


#ifndef  _SCL_CONFIG_H_
#include <scl_config.h>
#endif

#ifndef  _SCL_IOSTREAM_H_
#include <scl_iostream.h>
#endif


#ifdef  SCL_USE_STD_IOSTREAM
#include <iomanip>

#ifdef  SCL_HAS_NAMESPACES
// from <iomanip>
using SCL_VENDOR_STD::resetiosflags;
using SCL_VENDOR_STD::setiosflags;
using SCL_VENDOR_STD::setbase;
using SCL_VENDOR_STD::setfill;
using SCL_VENDOR_STD::setprecision;
using SCL_VENDOR_STD::setw;
#endif  /* SCL_HAS_NAMESPACES */

#else
#include <iomanip.h>

#endif  /* SCL_USE_STD_IOSTREAM */


#endif  /* _SCL_IOMANIP_H_ */
