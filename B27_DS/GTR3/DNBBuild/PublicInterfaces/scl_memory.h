/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: scl_memory.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Aug-2004     Initial implementation.
//
//  NOTICE:
//      This header implements functionality in the Standard C++ Library.
//      It may be included directly in application code.
//
//==============================================================================
#ifndef _SCL_MEMORY_H_
#define _SCL_MEMORY_H_


#ifndef  _SCL_CONFIG_H_
#include <scl_config.h>
#endif


#ifdef  SCL_USE_VENDOR_STD
#include <memory>

#define scl_allocator                   SCL_VENDOR_STD::allocator
#define scl_raw_storage_iterator        SCL_VENDOR_STD::raw_storage_iterator
#define scl_get_temporary_buffer        SCL_VENDOR_STD::get_temporary_buffer
#define scl_return_temporary_buffer     SCL_VENDOR_STD::return_temporary_buffer
#define scl_uninitialized_copy          SCL_VENDOR_STD::uninitialized_copy
#define scl_uninitialized_fill          SCL_VENDOR_STD::uninitialized_fill
#define scl_uninitialized_fill_n        SCL_VENDOR_STD::uninitialized_fill_n
#define scl_auto_ptr                    SCL_VENDOR_STD::auto_ptr
#define scl_auto_ptr_ref                SCL_VENDOR_STD::auto_ptr_ref

#else
#include <sclp_allocator.h>
#include <sclp_storage_iter.h>
#include <sclp_temp_buffer.h>
#include <sclp_uninitialized.h>
#include <sclp_auto_ptr.h>

#endif  /* SCL_USE_VENDOR_STD */


#endif  /* _SCL_MEMORY_H_ */
