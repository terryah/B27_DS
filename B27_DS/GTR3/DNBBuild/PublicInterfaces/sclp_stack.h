/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: sclp_stack.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Aug-2004     Initial implementation.
//
//  NOTICE:
//      This header is used internally by the Standard C++ Library.
//      It should not be included directly in application code.
//
//==============================================================================
#ifndef _SCLP_STACK_H_
#define _SCLP_STACK_H_


//
//  Include section
//
#ifndef  _SCLP_DEQUE_H_
#include <sclp_deque.h>
#endif


//
//  23.2.3.3 Class template scl_stack
//
#ifdef  SCL_HAS_DEFAULT_TMPL_ARGS
template <class Type_, class Cntr_ = scl_deque<Type_> >
#else
template <class Type_, class Cntr_>
#endif
class scl_stack
{
public:
    typedef scl_stack<Type_, Cntr_>                 Self_;
    typedef typename Cntr_::value_type              value_type;
    typedef typename Cntr_::size_type               size_type;
    typedef          Cntr_                          container_type;

    explicit
    scl_stack(const Cntr_& cntr) : c(cntr)          { }

    scl_stack() : c()                               { }

    bool                empty() const               { return c.empty(); }
    size_type           size()  const               { return c.size();  }
    value_type&         top()                       { return c.back();  }
    const value_type&   top()   const               { return c.back();  }
    void                push(const value_type& val) { c.push_back(val); }
    void                pop()                       { c.pop_back();     }

    // Internal helper methods.
    bool    _equal(const Self_& other) const        { return (c == other.c); }
    bool    _less (const Self_& other) const        { return (c <  other.c); }

    // The following data-member name is explicitly defined by the Standard.
protected:
    Cntr_   c;
};


template <class Type_, class Cntr_>
bool
operator==(const scl_stack<Type_, Cntr_>& lhs,
           const scl_stack<Type_, Cntr_>& rhs)
{
    return lhs._equal(rhs);
}

template <class Type_, class Cntr_>
bool
operator< (const scl_stack<Type_, Cntr_>& lhs,
           const scl_stack<Type_, Cntr_>& rhs)
{
    return lhs._less(rhs);
}

template <class Type_, class Cntr_>
bool
operator!=(const scl_stack<Type_, Cntr_>& lhs,
           const scl_stack<Type_, Cntr_>& rhs)
{
    return !(lhs == rhs);
}

template <class Type_, class Cntr_>
bool
operator> (const scl_stack<Type_, Cntr_>& lhs,
           const scl_stack<Type_, Cntr_>& rhs)
{
    return (rhs < lhs);
}

template <class Type_, class Cntr_>
bool
operator<=(const scl_stack<Type_, Cntr_>& lhs,
           const scl_stack<Type_, Cntr_>& rhs)
{
    return !(rhs < lhs);
}

template <class Type_, class Cntr_>
bool
operator>=(const scl_stack<Type_, Cntr_>& lhs,
           const scl_stack<Type_, Cntr_>& rhs)
{
    return !(lhs < rhs);
}


#endif  /* _SCLP_STACK_H_ */
