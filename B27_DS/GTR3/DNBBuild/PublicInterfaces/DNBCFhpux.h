/** @CAA2Required */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 1997
//==============================================================================
//
//  FILE: DNBCFhpux.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     23-Jun-1997     Initial implementation.
//
//  OVERVIEW:
//      Attributes of HP-UX 32-bit (aC++ A.03.52).
//      Do not include this header directly in application code.
//
//==============================================================================
#define DNB_HAS_POSIX_THREADS       DNB_YES
#define DNB_HAS_WIN32_THREADS       DNB_NO
#define DNB_HAS_PRAGMA_ONCE         DNB_NO
#define DNB_HAS_BOOL                DNB_YES
#define DNB_HAS_THROW_SPECS         DNB_YES
#define DNB_HAS_STD_NAMESPACE       DNB_NO
#define DNB_HAS_FUNC_INST           DNB_YES
#define DNB_HAS_CLASS_INST          DNB_YES
#define DNB_HAS_DOUBLE_ABS          DNB_YES
#define DNB_HAS_FLOAT_MATH          DNB_NO
#define DNB_HAS_FLOAT_MATHF         DNB_YES
#define DNB_HAS_FLOAT_FMATH         DNB_NO
#define DNB_HAS_ATOMIC_LOCK         DNB_NO
#define DNB_HAS_ATOMIC_INTEGER      DNB_NO
#define DNB_HAS_FRIEND_TEMPLATE     DNB_YES


#ifndef _REENTRANT
#error  "Must specify the compiler option -D_REENTRANT."
#endif


inline  void*
operator new( size_t size, const nothrow_t& )
    throw( )
{
    return ( ::operator new( size ) );
}

inline  void*
operator new[]( size_t size, const nothrow_t& )
    throw( )
{
    return ( ::operator new[]( size ) );
}
