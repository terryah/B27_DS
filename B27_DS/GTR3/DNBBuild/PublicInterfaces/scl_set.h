/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: scl_set.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Oct-2004     Initial implementation.
//
//  NOTICE:
//      This header implements functionality in the Standard C++ Library.
//      It may be included directly in application code.
//
//==============================================================================
#ifndef _SCL_SET_H_
#define _SCL_SET_H_


#ifndef  _SCL_CONFIG_H_
#include <scl_config.h>
#endif


#ifdef  SCL_USE_VENDOR_STD
#include <set>

#define scl_set                         SCL_VENDOR_STD::set
#define scl_multiset                    SCL_VENDOR_STD::multiset
#ifndef scl_swap
#define scl_swap                        SCL_VENDOR_STD::swap
#endif

#else
#include <sclp_set.h>
#include <sclp_multiset.h>

#endif  /* SCL_USE_VENDOR_STD */


#endif  /* _SCL_SET_H_ */
