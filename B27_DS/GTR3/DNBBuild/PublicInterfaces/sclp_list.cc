/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: sclp_list.cc
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Aug-2004     Initial implementation.
//
//==============================================================================
#ifdef  SCL_HAS_MEMBER_TEMPLATES
#ifndef  _SCLP_FUNCTIONAL_H_
#include <sclp_functional.h>            // for scl_less, scl_equal_to
#endif
#endif


#define SCL_LIST_TEMPL          template <class Type_, class Alloc_>
#define SCL_LIST_CLASS          scl_list<Type_, Alloc_>
#define SCL_LIST_CTOR_BEGIN     try {
#define SCL_LIST_CTOR_END       } catch (...) { finalize(); throw; }


//
//  23.2.2.1 construct/copy/destroy
//
SCL_LIST_TEMPL
SCL_LIST_CLASS::scl_list() :
    alloc_      (),
    node_alloc_ ()
{
    SCL_VALIDATE_CTOR(Self_, this);
    initialize();
}

SCL_LIST_TEMPL
SCL_LIST_CLASS::scl_list(const Alloc_& alloc) :
    alloc_      (alloc),
    node_alloc_ (alloc)
{
    SCL_VALIDATE_CTOR(Self_, this);
    initialize();
}

SCL_LIST_TEMPL
SCL_LIST_CLASS::scl_list(size_type len) :
    alloc_      (),
    node_alloc_ ()
{
    SCL_VALIDATE_CTOR(Self_, this);
    initialize();

    SCL_LIST_CTOR_BEGIN
        Type_   value = Type_();
        insert(begin(), len, value);
    SCL_LIST_CTOR_END
}

SCL_LIST_TEMPL
SCL_LIST_CLASS::scl_list(size_type len, const Type_& value) :
    alloc_      (),
    node_alloc_ ()
{
    SCL_VALIDATE_CTOR(Self_, this);
    initialize();

    SCL_LIST_CTOR_BEGIN
        insert(begin(), len, value);
    SCL_LIST_CTOR_END
}

SCL_LIST_TEMPL
SCL_LIST_CLASS::scl_list(size_type len, const Type_& value, const Alloc_& alloc) :
    alloc_      (alloc),
    node_alloc_ (alloc)
{
    SCL_VALIDATE_CTOR(Self_, this);
    initialize();

    SCL_LIST_CTOR_BEGIN
        insert(begin(), len, value);
    SCL_LIST_CTOR_END
}

#ifdef  SCL_HAS_MEMBER_TEMPLATES
SCL_LIST_TEMPL
template <class InputIterator_>
SCL_LIST_CLASS::scl_list(InputIterator_ first, InputIterator_ last) :
    alloc_      (),
    node_alloc_ ()
{
    SCL_VALIDATE_CTOR(Self_, this);
    initialize();

    SCL_LIST_CTOR_BEGIN
        insert(begin(), first, last);
    SCL_LIST_CTOR_END
}

SCL_LIST_TEMPL
template <class InputIterator_>
SCL_LIST_CLASS::scl_list(InputIterator_ first, InputIterator_ last, const Alloc_& alloc) :
    alloc_      (alloc),
    node_alloc_ (alloc)
{
    SCL_VALIDATE_CTOR(Self_, this);
    initialize();

    SCL_LIST_CTOR_BEGIN
        insert(begin(), first, last);
    SCL_LIST_CTOR_END
}
#else
SCL_LIST_TEMPL
SCL_LIST_CLASS::scl_list(const_iterator first, const_iterator last) :
    alloc_      (),
    node_alloc_ ()
{
    SCL_VALIDATE_CTOR(Self_, this);
    initialize();

    SCL_LIST_CTOR_BEGIN
        insert(begin(), first, last);
    SCL_LIST_CTOR_END
}

SCL_LIST_TEMPL
SCL_LIST_CLASS::scl_list(const_iterator first, const_iterator last, const Alloc_& alloc) :
    alloc_      (alloc),
    node_alloc_ (alloc)
{
    SCL_VALIDATE_CTOR(Self_, this);
    initialize();

    SCL_LIST_CTOR_BEGIN
        insert(begin(), first, last);
    SCL_LIST_CTOR_END
}

SCL_LIST_TEMPL
SCL_LIST_CLASS::scl_list(const_pointer  first, const_pointer  last) :
    alloc_      (),
    node_alloc_ ()
{
    SCL_VALIDATE_CTOR(Self_, this);
    initialize();

    SCL_LIST_CTOR_BEGIN
        insert(begin(), first, last);
    SCL_LIST_CTOR_END
}

SCL_LIST_TEMPL
SCL_LIST_CLASS::scl_list(const_pointer  first, const_pointer  last, const Alloc_& alloc) :
    alloc_      (alloc),
    node_alloc_ (alloc)
{
    SCL_VALIDATE_CTOR(Self_, this);
    initialize();

    SCL_LIST_CTOR_BEGIN
        insert(begin(), first, last);
    SCL_LIST_CTOR_END
}
#endif  /* SCL_HAS_MEMBER_TEMPLATES */

SCL_LIST_TEMPL
SCL_LIST_CLASS::scl_list(const Self_& other) :
    alloc_      (other.alloc_),
    node_alloc_ (other.node_alloc_)
{
    SCL_VALIDATE_CTOR(Self_, this);
    initialize();

    SCL_LIST_CTOR_BEGIN
        insert(begin(), other.begin(), other.end());
    SCL_LIST_CTOR_END
}

SCL_LIST_TEMPL
SCL_LIST_CLASS::~scl_list()
{
    SCL_VALIDATE_DTOR(Self_, this);
    finalize();
}

SCL_LIST_TEMPL
SCL_LIST_CLASS &
SCL_LIST_CLASS::operator=(const Self_& other)
{
    SCL_VALIDATE(Self_, this);

    if (this != &other)
        assign(other.begin(), other.end());

    return *this;
}

// Global helper function that accepts any iterator type.
template <class Type_, class Alloc_, class InpIter_>
void
_scl_list_assign(SCL_LIST_CLASS* self, InpIter_ first2, InpIter_ last2)
{
    typename SCL_LIST_CLASS::iterator   first1 = self->begin();
    typename SCL_LIST_CLASS::iterator   last1  = self->end();

    for (; first1 != last1 && first2 != last2; ++first1, ++first2)
        *first1 = *first2;

    if (first2 == last2)
        self->erase(first1, last1);
    else
        self->insert(last1, first2, last2);
}

#ifdef  SCL_HAS_MEMBER_TEMPLATES
SCL_LIST_TEMPL
template <class InputIterator_>
void
SCL_LIST_CLASS::assign(InputIterator_ first, InputIterator_ last)
{
    SCL_VALIDATE(Self_, this);
    _scl_list_assign(this, first, last);
}
#else
SCL_LIST_TEMPL
void
SCL_LIST_CLASS::assign(const_iterator first, const_iterator last)
{
    SCL_VALIDATE(Self_, this);
    _scl_list_assign(this, first, last);
}

SCL_LIST_TEMPL
void
SCL_LIST_CLASS::assign(const_pointer  first, const_pointer  last)
{
    SCL_VALIDATE(Self_, this);
    _scl_list_assign(this, first, last);
}
#endif  /* SCL_HAS_MEMBER_TEMPLATES */

SCL_LIST_TEMPL
void
SCL_LIST_CLASS::assign(size_type len, const Type_& value)
{
    SCL_VALIDATE(Self_, this);

    iterator    first1 = begin();
    iterator    last1  = end();

    for (; first1 != last1 && len > 0; ++first1, --len)
        *first1 = value;

    if (len == 0)
        erase(first1, last1);
    else
        insert(last1, len, value);
}

SCL_LIST_TEMPL
typename SCL_LIST_CLASS::allocator_type
SCL_LIST_CLASS::get_allocator() const
{
    return allocator_type(node_alloc_);
}


//
//  iterators
//
SCL_LIST_TEMPL
typename SCL_LIST_CLASS::iterator
SCL_LIST_CLASS::begin()
{
    return iterator(head_->next_);
}

SCL_LIST_TEMPL
typename SCL_LIST_CLASS::const_iterator
SCL_LIST_CLASS::begin() const
{
    return const_iterator(head_->next_);
}

SCL_LIST_TEMPL
typename SCL_LIST_CLASS::iterator
SCL_LIST_CLASS::end()
{
    return iterator(head_);
}

SCL_LIST_TEMPL
typename SCL_LIST_CLASS::const_iterator
SCL_LIST_CLASS::end() const
{
    return const_iterator(head_);
}

SCL_LIST_TEMPL
typename SCL_LIST_CLASS::reverse_iterator
SCL_LIST_CLASS::rbegin()
{
    return reverse_iterator(end());
}

SCL_LIST_TEMPL
typename SCL_LIST_CLASS::const_reverse_iterator
SCL_LIST_CLASS::rbegin() const
{
    return const_reverse_iterator(end());
}

SCL_LIST_TEMPL
typename SCL_LIST_CLASS::reverse_iterator
SCL_LIST_CLASS::rend()
{
    return reverse_iterator(begin());
}

SCL_LIST_TEMPL
typename SCL_LIST_CLASS::const_reverse_iterator
SCL_LIST_CLASS::rend() const
{
    return const_reverse_iterator(begin());
}


//
//  23.2.2.2 capacity
//
SCL_LIST_TEMPL
bool
SCL_LIST_CLASS::empty() const
{
    return (length_ == 0);
}

SCL_LIST_TEMPL
typename SCL_LIST_CLASS::size_type
SCL_LIST_CLASS::size() const
{
    return length_;
}

SCL_LIST_TEMPL
typename SCL_LIST_CLASS::size_type
SCL_LIST_CLASS::max_size() const
{
    return node_alloc_.max_size();
}

SCL_LIST_TEMPL
void
SCL_LIST_CLASS::resize(size_type len)
{
    SCL_VALIDATE(Self_, this);

    Type_   value = Type_();
    resize(len, value);
}

SCL_LIST_TEMPL
void
SCL_LIST_CLASS::resize(size_type len, Type_ value)
{
    SCL_VALIDATE(Self_, this);

    if (len > length_)
    {
        insert(end(), len - length_, value);
    }
    else if (len < length_)
    {
        iterator    iter = begin();
        scl_advance(iter, len);
        erase(iter, end());
    }
}


//
//  element access
//
SCL_LIST_TEMPL
typename SCL_LIST_CLASS::reference
SCL_LIST_CLASS::front()
{
    SCL_PRECONDITION( !empty() );
    Node_*  node = head_->next_;
    return node->data_;
}

SCL_LIST_TEMPL
typename SCL_LIST_CLASS::const_reference
SCL_LIST_CLASS::front() const
{
    SCL_PRECONDITION( !empty() );
    Node_*  node = head_->next_;
    return node->data_;
}

SCL_LIST_TEMPL
typename SCL_LIST_CLASS::reference
SCL_LIST_CLASS::back()
{
    SCL_PRECONDITION( !empty() );
    Node_*  node = head_->prev_;
    return node->data_;
}

SCL_LIST_TEMPL
typename SCL_LIST_CLASS::const_reference
SCL_LIST_CLASS::back() const
{
    SCL_PRECONDITION( !empty() );
    Node_*  node = head_->prev_;
    return node->data_;
}


//
//  23.2.2.3 modifiers
//
SCL_LIST_TEMPL
void
SCL_LIST_CLASS::push_front(const Type_& value)
{
    SCL_VALIDATE(Self_, this);
    insert(begin(), value);
}

SCL_LIST_TEMPL
void
SCL_LIST_CLASS::pop_front()
{
    SCL_VALIDATE(Self_, this);
    SCL_PRECONDITION( !empty() );
    erase(begin());
}

SCL_LIST_TEMPL
void
SCL_LIST_CLASS::push_back(const Type_& value)
{
    SCL_VALIDATE(Self_, this);
    insert(end(), value);
}

SCL_LIST_TEMPL
void
SCL_LIST_CLASS::pop_back()
{
    SCL_VALIDATE(Self_, this);
    SCL_PRECONDITION( !empty() );
    iterator    iter(head_->prev_);
    erase(iter);
}

SCL_LIST_TEMPL
typename SCL_LIST_CLASS::iterator
SCL_LIST_CLASS::insert(iterator position, const Type_& value)
{
    SCL_VALIDATE(Self_, this);

    Node_*  node = create_node(value);
    node->insert(position.node_);
    ++length_;

    return iterator(node);
}

SCL_LIST_TEMPL
void
SCL_LIST_CLASS::insert(iterator position, size_type len, const Type_& value)
{
    SCL_VALIDATE(Self_, this);
    size_t  count = 0;

    try
    {
        for (; len > 0; --len, ++count)
            insert(position, value);
    }
    catch (...)
    {
        // Remove elements inserted above.
        for (; count > 0; --count)
        {
            iterator    iter = position;
            erase(--iter);
        }

        throw;
    }
}

// Global helper function that accepts any iterator type.
template <class Type_, class Alloc_, class InpIter_>
void
_scl_list_insert(SCL_LIST_CLASS* self, typename SCL_LIST_CLASS::iterator position,
    InpIter_ first, InpIter_ last)
{
    size_t  count = 0;

    try
    {
        for (; first != last; ++first, ++count)
            self->insert(position, *first);
    }
    catch (...)
    {
        // Remove elements inserted above.
        for (; count > 0; --count)
        {
            typename SCL_LIST_CLASS::iterator   iter = position;
            self->erase(--iter);
        }

        throw;
    }
}

#ifdef  SCL_HAS_MEMBER_TEMPLATES
SCL_LIST_TEMPL
template <class InputIterator_>
void
SCL_LIST_CLASS::insert(iterator position, InputIterator_ first, InputIterator_ last)
{
    SCL_VALIDATE(Self_, this);
    _scl_list_insert(this, position, first, last);
}
#else
SCL_LIST_TEMPL
void
SCL_LIST_CLASS::insert(iterator position, const_iterator first, const_iterator last)
{
    SCL_VALIDATE(Self_, this);
    _scl_list_insert(this, position, first, last);
}

SCL_LIST_TEMPL
void
SCL_LIST_CLASS::insert(iterator position, const_pointer  first, const_pointer  last)
{
    SCL_VALIDATE(Self_, this);
    _scl_list_insert(this, position, first, last);
}
#endif  /* SCL_HAS_MEMBER_TEMPLATES */

SCL_LIST_TEMPL
typename SCL_LIST_CLASS::iterator
SCL_LIST_CLASS::erase(iterator position)
{
    SCL_VALIDATE(Self_, this);

    Node_*  node = position.node_;
    Node_*  next = position.node_->next_;

    node->unlink();
    alloc_.destroy(&node->data_);
    deallocate_node(node, true);        // recycle node
    --length_;

    return iterator(next);
}

SCL_LIST_TEMPL
typename SCL_LIST_CLASS::iterator
SCL_LIST_CLASS::erase(iterator first, iterator last)
{
    SCL_VALIDATE(Self_, this);

    if (first == begin() && last == end())
    {
        // This is a common calling pattern...
        clear();
    }
    else
    {
        while (first != last)
            first = erase(first);
    }

    return last;
}

SCL_LIST_TEMPL
void
SCL_LIST_CLASS::swap(Self_& other)
{
    SCL_VALIDATE(Self_, this);

    if (node_alloc_ == other.node_alloc_)
    {
        scl_swap(length_    , other.length_);
        scl_swap(head_      , other.head_);
        scl_swap(cache_     , other.cache_);
        scl_swap(alloc_     , other.alloc_);
        scl_swap(node_alloc_, other.node_alloc_);
    }
    else
    {
        const Self_ tmp(*this);
        assign(other.begin(), other.end());
        other.assign(tmp.begin(), tmp.end());
    }
}

SCL_LIST_TEMPL
void
SCL_LIST_CLASS::clear()
{
    SCL_VALIDATE(Self_, this);
    clear_aux(true);            // recycle nodes
}


//
//  23.2.2.4 list operations
//
SCL_LIST_TEMPL
void
SCL_LIST_CLASS::splice(iterator position, Self_& other)
{
    SCL_VALIDATE(Self_, this);

    if (this == &other || other.empty())
        return;

    if (node_alloc_ == other.node_alloc_)
    {
        // Same allocators; transfer nodes from <other> to <self>.
        transfer(position, other.begin(), other.end());
        length_      += other.length_;
        other.length_ = 0;
    }
    else
    {
        // Different allocators; copy elements from <other> to <self>.
        insert(position, other.begin(), other.end());
        other.clear();
    }
}

SCL_LIST_TEMPL
void
SCL_LIST_CLASS::splice(iterator position, Self_& other, iterator first)
{
    SCL_VALIDATE(Self_, this);

    // Note: <self> and <other> may be the same object!
    iterator    last = first;
    ++last;

    if (position == first || position == last)
        return;

    if (node_alloc_ == other.node_alloc_)
    {
        // Same allocators; transfer node from <other> to <self>.
        transfer(position, first, last);
        length_       += 1;
        other.length_ -= 1;
    }
    else
    {
        // Different allocators; copy element from <other> to <self>.
        insert(position, *first);
        other.erase(first);
    }
}

SCL_LIST_TEMPL
void
SCL_LIST_CLASS::splice(iterator position, Self_& other, iterator first, iterator last)
{
    SCL_VALIDATE(Self_, this);

    // Note: <self> and <other> may be the same object!
    if (first == last)
        return;

    if (node_alloc_ == other.node_alloc_)
    {
        // Same allocators; transfer nodes from <other> to <self>.
        if (this != &other)
        {
            // Compute cardinality of [first, last) before moving nodes.
            difference_type count = scl_distance(first, last);
            length_       += count;
            other.length_ -= count;
        }

        transfer(position, first, last);
    }
    else
    {
        // Different allocators; copy elements from <other> to <self>.
        insert(position, first, last);
        other.erase(first, last);
    }
}

SCL_LIST_TEMPL
void
SCL_LIST_CLASS::remove(const Type_& value)
{
    SCL_VALIDATE(Self_, this);

    iterator    first = begin();
    iterator    last  = end();

    while (first != last)
    {
        if (*first == value)
            first = erase(first);
        else
            ++first;
    }
}

#ifdef  SCL_HAS_MEMBER_TEMPLATES
SCL_LIST_TEMPL
template <class Predicate_>
void
SCL_LIST_CLASS::remove_if(Predicate_ pred)
#else
SCL_LIST_TEMPL
void
SCL_LIST_CLASS::remove_if(bool (*pred)(const Type_&))
#endif
{
    SCL_VALIDATE(Self_, this);

    iterator    first = begin();
    iterator    last  = end();

    while (first != last)
    {
        if (pred(*first))
            first = erase(first);
        else
            ++first;
    }
}

SCL_LIST_TEMPL
void
SCL_LIST_CLASS::unique()
{
#ifdef  SCL_HAS_MEMBER_TEMPLATES
    unique(scl_equal_to<Type_>());
#else
    unique(equal_to_aux);
#endif
}

#ifdef  SCL_HAS_MEMBER_TEMPLATES
SCL_LIST_TEMPL
template <class Predicate_>
void
SCL_LIST_CLASS::unique(Predicate_ pred)
#else
SCL_LIST_TEMPL
void
SCL_LIST_CLASS::unique(bool (*pred)(const Type_&, const Type_&))
#endif
{
    SCL_VALIDATE(Self_, this);

    if (length_ < 2)
        return;

    iterator    first = begin();
    iterator    last  = end();
    iterator    next  = first;
    ++next;

    while (next != last)
    {
        if (pred(*first, *next))
            next  = erase(next);
        else
            first = next++;
    }
}

SCL_LIST_TEMPL
void
SCL_LIST_CLASS::merge(Self_& other)
{
#ifdef  SCL_HAS_MEMBER_TEMPLATES
    merge(other, scl_less<Type_>());
#else
    merge(other, less_aux);
#endif
}

#ifdef  SCL_HAS_MEMBER_TEMPLATES
SCL_LIST_TEMPL
template <class Compare_>
void
SCL_LIST_CLASS::merge(Self_& other, Compare_ comp)
#else
SCL_LIST_TEMPL
void
SCL_LIST_CLASS::merge(Self_& other, bool (*comp)(const Type_&, const Type_&))
#endif
{
    SCL_VALIDATE(Self_, this);

    if (this == &other || other.empty())
        return;

    iterator    first1 = begin();
    iterator    last1  = end();
    iterator    first2 = other.begin();
    iterator    last2  = other.end();

    const bool  same_alloc = (node_alloc_ == other.node_alloc_);

    while (first1 != last1 && first2 != last2)
    {
        if (comp(*first2, *first1))
        {
            if (same_alloc)
            {
                // Same allocators; transfer node from <other> to <self>.
                iterator    next = first2;
                transfer(first1, first2, ++next);
                first2 = next;
            }
            else
            {
                // Different allocators; copy element from <other> to <self>.
                insert(first1, *first2);
                ++first2;
            }
        }
        else
        {
            ++first1;
        }
    }

    if (first2 != last2)
    {
        if (same_alloc)
            transfer(last1, first2, last2);
        else
            insert(last1, first2, last2);
    }

    if (same_alloc)
    {
        length_      += other.length_;
        other.length_ = 0;
    }
    else
    {
        other.clear();
    }
}

SCL_LIST_TEMPL
void
SCL_LIST_CLASS::sort()
{
#ifdef  SCL_HAS_MEMBER_TEMPLATES
    sort(scl_less<Type_>());
#else
    sort(less_aux);
#endif
}

#ifdef  SCL_HAS_MEMBER_TEMPLATES
SCL_LIST_TEMPL
template <class Compare_>
void
SCL_LIST_CLASS::sort(Compare_ comp)
#else
SCL_LIST_TEMPL
void
SCL_LIST_CLASS::sort(bool (*comp)(const Type_&, const Type_&))
#endif
{
    SCL_VALIDATE(Self_, this);

    if (length_ < 2)
        return ;

    const size_t    MAX_BINS = 32;
    Self_           carry(alloc_);
    Self_           sort_bins[MAX_BINS + 1];
    size_t          num_bins = 0;               // # of bins used
    size_t          idx;

    while (!empty())
    {
        carry.splice(carry.begin(), *this, begin());

        for (idx = 0; (idx < num_bins) && !sort_bins[idx].empty(); ++idx)
        {
            sort_bins[idx].merge(carry, comp);
            sort_bins[idx].swap(carry);
        }

        if (idx == MAX_BINS)
        {
            sort_bins[idx - 1].merge(carry, comp);
        }
        else
        {
            sort_bins[idx].swap(carry);
            if (idx == num_bins)
                ++num_bins;
        }
    }

    for (idx = 1; idx < num_bins; ++idx)
        sort_bins[idx].merge(sort_bins[idx - 1], comp);

    swap(sort_bins[num_bins - 1]);
}

SCL_LIST_TEMPL
void
SCL_LIST_CLASS::reverse()
{
    SCL_VALIDATE(Self_, this);

    if (length_ < 2)
        return;

    head_->reverse();
}

SCL_LIST_TEMPL
bool
SCL_LIST_CLASS::_validate() const
{
    const size_type fsize = scl_distance( begin(),  end());
    const size_type rsize = scl_distance(rbegin(), rend());
    if (fsize != length_ || rsize != length_)
        return false;

    if (length_ == 0)
    {
        if (head_->next_ != head_ || head_->prev_ != head_)
            return false;
    }
    else
    {
        Node_*  node = head_;

        for (size_t idx = 0; idx < length_; ++idx)
        {
            node = node->next_;
            if (node->prev_->next_ != node)
                return false;
        }

        // Note: <node> is now last element
        if (head_->prev_ != node)
            return false;
    }

    return true;
}

SCL_LIST_TEMPL
void
SCL_LIST_CLASS::initialize()
{
    // Establish class invariants.
    length_ = 0;
    head_   = NULL;
    cache_  = NULL;

    // Allocate storage for dummy node.
    head_   = allocate_node();
    head_->next_ = head_;
    head_->prev_ = head_;
}

SCL_LIST_TEMPL
typename SCL_LIST_CLASS::Node_*
SCL_LIST_CLASS::allocate_node()
{
    Node_*  node;

    if (cache_ != NULL)
    {
        // Retrieve node from free list (cache).
        node   = cache_;
        cache_ = cache_->next_;
    }
    else
    {
        // Allocate storage for node.
#ifdef  SCL_USE_ALLOCATOR_REBIND
        node = node_alloc_.allocate(1);
#else
        node = (Node_*) node_alloc_.raw_allocate(1, sizeof(Node_));
#endif
    }

    return node;
}

SCL_LIST_TEMPL
void
SCL_LIST_CLASS::deallocate_node(Node_* node, bool recycle)
{
    if (recycle)
    {
        // Insert node in free list (cache).
        node->next_ = cache_;
        node->prev_ = NULL;
        cache_      = node;
    }
    else
    {
        // Deallocate storage for node.
#ifdef  SCL_USE_ALLOCATOR_REBIND
        node_alloc_.deallocate(node, 1);
#else
        node_alloc_.raw_deallocate(node, 1, sizeof(Node_));
#endif
    }
}

SCL_LIST_TEMPL
typename SCL_LIST_CLASS::Node_*
SCL_LIST_CLASS::create_node(const Type_& value)
{
    Node_*  node = allocate_node();

    try
    {
        alloc_.construct(&node->data_, value);
    }
    catch (...)
    {
        deallocate_node(node, true);    // recycle node
        throw;
    }

    return node;
}

SCL_LIST_TEMPL
void
SCL_LIST_CLASS::transfer(iterator position, iterator first, iterator last)
{
    position.node_->transfer(first.node_, last.node_);
}

SCL_LIST_TEMPL
void
SCL_LIST_CLASS::clear_aux(bool recycle)
{
    Node_*  node = head_->next_;

    while (node != head_)
    {
        Node_*  next = node->next_;
        alloc_.destroy(&node->data_);
        deallocate_node(node, recycle);
        node = next;
    }

    length_ = 0;
    head_->next_ = head_;
    head_->prev_ = head_;
}

SCL_LIST_TEMPL
void
SCL_LIST_CLASS::finalize()
{
    // Delete every node in the list and destroy its data.
    clear_aux(false);

    // Delete the head node without destroying its data.
    deallocate_node(head_, false);
    head_ = NULL;

    // Delete every node in the cache without destroying its data.
    Node_*  node = cache_;

    while (node != NULL)
    {
        Node_*  next = node->next_;
        deallocate_node(node, false);
        node = next;
    }

    cache_ = NULL;
}

SCL_LIST_TEMPL
bool
SCL_LIST_CLASS::equal_to_aux(const Type_& lhs, const Type_& rhs)
{
    return (lhs == rhs);
}

SCL_LIST_TEMPL
bool
SCL_LIST_CLASS::less_aux(const Type_& lhs, const Type_& rhs)
{
    return (lhs < rhs);
}


#undef  SCL_LIST_TEMPL
#undef  SCL_LIST_CLASS
#undef  SCL_LIST_CTOR_BEGIN
#undef  SCL_LIST_CTOR_END
