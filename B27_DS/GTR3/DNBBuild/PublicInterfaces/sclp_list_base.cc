/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: sclp_list_base.cc
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Aug-2004     Initial implementation.
//
//==============================================================================
#define SCL_LIST_NODE_TEMPL     template <class Type_>
#define SCL_LIST_NODE_CLASS     _scl_list_node<Type_>


SCL_LIST_NODE_TEMPL
void
SCL_LIST_NODE_CLASS::insert(Self_* const node)
{
    this->next_ = node;
    this->prev_ = node->prev_;
    node->prev_->next_ = this;
    node->prev_ = this;
}

SCL_LIST_NODE_TEMPL
void
SCL_LIST_NODE_CLASS::unlink()
{
    Self_* const next = this->next_;
    Self_* const prev = this->prev_;
    prev->next_ = next;
    next->prev_ = prev;
}

SCL_LIST_NODE_TEMPL
void
SCL_LIST_NODE_CLASS::reverse()
{
    Self_*  node = this;

    do
    {
        scl_swap(node->next_, node->prev_);
        node = node->prev_;
    } while (node != this);
}

SCL_LIST_NODE_TEMPL
void
SCL_LIST_NODE_CLASS::transfer(Self_* const first, Self_* const last)
{
    if (this != last)
    {
        // Remove [first, last) from its old position.
        last->prev_->next_  = this;
        first->prev_->next_ = last;
        this->prev_->next_  = first;

        // Splice [first, last) into its new position.
        Self_* const tmp = this->prev_;
        this->prev_  = last->prev_;
        last->prev_  = first->prev_;
        first->prev_ = tmp;
    }
}


#undef  SCL_LIST_NODE_TEMPL
#undef  SCL_LIST_NODE_CLASS
