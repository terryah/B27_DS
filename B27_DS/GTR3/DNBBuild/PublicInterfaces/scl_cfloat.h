/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: scl_cfloat.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Aug-2004     Initial implementation.
//
//  NOTICE:
//      This header implements functionality in the Standard C++ Library.
//      It may be included directly in application code.
//
//==============================================================================
#ifndef _SCL_CFLOAT_H_
#define _SCL_CFLOAT_H_


#ifndef  _SCL_CONFIG_H_
#include <scl_config.h>
#endif

#include <CATDataType.h>                // for CATUINT32
#include <float.h>


#ifdef  _AIX_SOURCE
#define SCL_FLT_INF     FLT_INFINITY
#define SCL_FLT_QNAN    FLT_QNAN
#define SCL_FLT_SNAN    FLT_SNAN
#define SCL_DBL_INF     DBL_INFINITY
#define SCL_DBL_QNAN    DBL_QNAN
#define SCL_DBL_SNAN    DBL_SNAN
#else
#define SCL_FLT_INF     (*(float  *) _scl_flt_inf )
#define SCL_FLT_QNAN    (*(float  *) _scl_flt_qnan)
#define SCL_FLT_SNAN    (*(float  *) _scl_flt_snan)
#define SCL_DBL_INF     (*(double *) _scl_dbl_inf )
#define SCL_DBL_QNAN    (*(double *) _scl_dbl_qnan)
#define SCL_DBL_SNAN    (*(double *) _scl_dbl_snan)
#endif  /* _AIX_SOURCE */


#define SCL_LDBL_INF    (*(long double *) _scl_ldbl_inf )
#define SCL_LDBL_QNAN   (*(long double *) _scl_ldbl_qnan)
#define SCL_LDBL_SNAN   (*(long double *) _scl_ldbl_snan)


extern SCL_STD_EXPORT CATUINT32 _scl_flt_inf[],  _scl_flt_snan[], _scl_flt_qnan[];
extern SCL_STD_EXPORT CATUINT32 _scl_dbl_inf[],  _scl_dbl_snan[], _scl_dbl_qnan[];
extern SCL_STD_EXPORT CATUINT32 _scl_ldbl_inf[], _scl_ldbl_snan[], _scl_ldbl_qnan[];


#endif  /* _SCL_CFLOAT_H_ */
