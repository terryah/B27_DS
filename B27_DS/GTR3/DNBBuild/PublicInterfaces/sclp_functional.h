/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: sclp_functional.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Dec-2004     Initial implementation.
//      rtl     24-Jun-2005     Fix UMR Errors
//
//  NOTICE:
//      This header is used internally by the Standard C++ Library.
//      It should not be included directly in application code.
//
//==============================================================================
#ifndef _SCLP_FUNCTIONAL_H_
#define _SCLP_FUNCTIONAL_H_


//
//  20.3.1 Base
//
template <class Arg_, class Result_>
struct scl_unary_function
{
    typedef Arg_        argument_type;
    typedef Result_     result_type;

    // The following methods are required to suppress UMR errors.
    scl_unary_function() { }

    scl_unary_function(const scl_unary_function&) { }

    scl_unary_function&
    operator=(const scl_unary_function&) { return *this; }
};

template <class Arg1_, class Arg2_, class Result_>
struct scl_binary_function
{
    typedef Arg1_       first_argument_type;
    typedef Arg2_       second_argument_type;
    typedef Result_     result_type;

    // The following methods are required to suppress UMR errors.
    scl_binary_function() { }

    scl_binary_function(const scl_binary_function&) { }

    scl_binary_function&
    operator=(const scl_binary_function&) { return *this; }
};


//
//  20.3.2 Arithmetic operations
//
template <class Type_>
struct scl_plus : public scl_binary_function<Type_, Type_, Type_>
{
    Type_ operator()(const Type_& x, const Type_& y) const { return (x + y); }
};

template <class Type_>
struct scl_minus : public scl_binary_function<Type_, Type_, Type_>
{
    Type_ operator()(const Type_& x, const Type_& y) const { return (x - y); }
};

template <class Type_>
struct scl_multiplies : public scl_binary_function<Type_, Type_, Type_>
{
    Type_ operator()(const Type_& x, const Type_& y) const { return (x * y); }
};

template <class Type_>
struct scl_divides : public scl_binary_function<Type_, Type_, Type_>
{
    Type_ operator()(const Type_& x, const Type_& y) const { return (x / y); }
};

template <class Type_>
struct scl_modulus : public scl_binary_function<Type_, Type_, Type_>
{
    Type_ operator()(const Type_& x, const Type_& y) const { return (x % y); }
};

template <class Type_>
struct scl_negate : public scl_unary_function<Type_, Type_>
{
    Type_ operator()(const Type_& x) const { return (-x); }
};


//
//  20.3.3 Comparisons
//
template <class Type_>
struct scl_equal_to : public scl_binary_function<Type_, Type_, bool>
{
    bool operator()(const Type_& x, const Type_& y) const { return (x == y); }
};

template <class Type_>
struct scl_not_equal_to : public scl_binary_function<Type_, Type_, bool>
{
    bool operator()(const Type_& x, const Type_& y) const { return (x != y); }
};

template <class Type_>
struct scl_greater : public scl_binary_function<Type_, Type_, bool>
{
    bool operator()(const Type_& x, const Type_& y) const { return (x >  y); }
};

template <class Type_>
struct scl_less : public scl_binary_function<Type_, Type_, bool>
{
    bool operator()(const Type_& x, const Type_& y) const { return (x <  y); }
};

template <class Type_>
struct scl_greater_equal : public scl_binary_function<Type_, Type_, bool>
{
    bool operator()(const Type_& x, const Type_& y) const { return (x >= y); }
};

template <class Type_>
struct scl_less_equal : public scl_binary_function<Type_, Type_, bool>
{
    bool operator()(const Type_& x, const Type_& y) const { return (x <= y); }
};


//
//  20.3.4 Logical operations
//
template <class Type_>
struct scl_logical_and : public scl_binary_function<Type_, Type_, bool>
{
    bool operator()(const Type_& x, const Type_& y) const { return (x && y); }
};

template <class Type_>
struct scl_logical_or : public scl_binary_function<Type_, Type_, bool>
{
    bool operator()(const Type_& x, const Type_& y) const { return (x || y); }
};

template <class Type_>
struct scl_logical_not : public scl_unary_function<Type_, bool>
{
    bool operator()(const Type_& x) const { return (!x); }
};


//
//  20.3.5 Negators
//
template <class Func_>
class scl_unary_negate : public
    scl_unary_function<typename Func_::argument_type, bool>
{
public:
    typedef typename Func_::argument_type   Arg_;

    explicit
    scl_unary_negate(const Func_& func) :
        func_(func)
    {
        // Nothing
    }

    bool operator()(const Arg_& x) const
    {
        return (!func_(x));
    }

private:
    Func_   func_;
};

template <class Func_>
scl_unary_negate<Func_>
scl_not1(const Func_& func)
{
    return scl_unary_negate<Func_>(func);
}

template <class Func_>
class scl_binary_negate : public
    scl_binary_function<typename Func_::first_argument_type,
                        typename Func_::second_argument_type,
                        bool>
{
public:
    typedef typename Func_::first_argument_type     Arg1_;
    typedef typename Func_::second_argument_type    Arg2_;

    explicit
    scl_binary_negate(const Func_& func) :
        func_(func)
    {
        // Nothing
    }

    bool operator()(const Arg1_& x, const Arg2_& y) const
    {
        return (!func_(x, y));
    }

private:
    Func_   func_;
};

template <class Func_>
scl_binary_negate<Func_>
scl_not2(const Func_& func)
{
    return scl_binary_negate<Func_>(func);
}


//
//  20.3.6 Binders
//
template <class Func_>
class scl_binder1st : public
    scl_unary_function<typename Func_::second_argument_type,
                       typename Func_::result_type>
{
public:
    typedef typename Func_::first_argument_type     Arg1_;
    typedef typename Func_::second_argument_type    Arg2_;
    typedef typename Func_::result_type             Result_;

    scl_binder1st(const Func_& func, const Arg1_& x) :
        op(func), value(x)
    {
        // Nothing
    }

    Result_ operator()(const Arg2_& y) const { return op(value, y); }
    Result_ operator()(      Arg2_& y) const { return op(value, y); }

    // The following data-member names are explicitly defined by the Standard.
protected:
    Func_   op;                 // binary functor: op(x,y)
    Arg1_   value;              // x argument of functor
};

template <class Func_, class Type_>
scl_binder1st<Func_>
scl_bind1st(const Func_& func, const Type_& x)
{
    typedef typename Func_::first_argument_type     Arg1_;
    return scl_binder1st<Func_>(func, Arg1_(x));
}

template <class Func_>
class scl_binder2nd : public
    scl_unary_function<typename Func_::first_argument_type,
                       typename Func_::result_type>
{
public:
    typedef typename Func_::first_argument_type     Arg1_;
    typedef typename Func_::second_argument_type    Arg2_;
    typedef typename Func_::result_type             Result_;

    scl_binder2nd(const Func_& func, const Arg2_& y) :
        op(func), value(y)
    {
        // Nothing
    }

    Result_ operator()(const Arg1_& x) const { return op(x, value); }
    Result_ operator()(      Arg1_& x) const { return op(x, value); }

    // The following data-member names are explicitly defined by the Standard.
protected:
    Func_   op;                 // binary functor: op(x,y)
    Arg2_   value;              // y argument of functor
};

template <class Func_, class Type_>
scl_binder2nd<Func_>
scl_bind2nd(const Func_& func, const Type_& y)
{
    typedef typename Func_::second_argument_type    Arg2_;
    return scl_binder2nd<Func_>(func, Arg2_(y));
}


//
//  20.3.7 Adaptors for pointers to functions
//
template <class Arg_, class Result_>
class scl_pointer_to_unary_function : public
    scl_unary_function<Arg_, Result_>
{
public:
    typedef Result_ (*Func_)(Arg_);

    explicit
    scl_pointer_to_unary_function(Func_ func) : func_(func) { }

    Result_ operator()(Arg_ x) const { return func_(x); }

private:
    Func_   func_;
};

template <class Arg_, class Result_>
scl_pointer_to_unary_function<Arg_, Result_>
scl_ptr_fun(Result_ (*func)(Arg_))
{
    return scl_pointer_to_unary_function<Arg_, Result_>(func);
}

template <class Arg1_, class Arg2_, class Result_>
class scl_pointer_to_binary_function : public
    scl_binary_function<Arg1_, Arg2_, Result_>
{
public:
    typedef Result_ (*Func_)(Arg1_, Arg2_);

    explicit
    scl_pointer_to_binary_function(Func_ func) : func_(func) { }

    Result_ operator()(Arg1_ x, Arg2_ y) const { return func_(x, y); }

private:
    Func_   func_;
};

template <class Arg1_, class Arg2_, class Result_>
scl_pointer_to_binary_function<Arg1_, Arg2_, Result_>
scl_ptr_fun(Result_ (*func)(Arg1_, Arg2_))
{
    return scl_pointer_to_binary_function<Arg1_, Arg2_, Result_>(func);
}


//
//  20.3.8 Adaptors for pointers to members
//
template <class Result_, class Type_>
class scl_mem_fun_t : public scl_unary_function<Type_*, Result_>
{
public:
    typedef Result_ (Type_::*Func_)();

    explicit
    scl_mem_fun_t(Func_ func) : func_(func) { }

    Result_ operator()(Type_* obj) const { return (obj->*func_)(); }

private:
    Func_   func_;
};

template <class Result_, class Type_, class Arg_>
class scl_mem_fun1_t : public scl_binary_function<Type_*, Arg_, Result_>
{
public:
    typedef Result_ (Type_::*Func_)(Arg_);

    explicit
    scl_mem_fun1_t(Func_ func) : func_(func) { }

    Result_ operator()(Type_* obj, Arg_ x) const { return (obj->*func_)(x); }

private:
    Func_   func_;
};

template <class Result_, class Type_>
scl_mem_fun_t<Result_, Type_>
scl_mem_fun(Result_ (Type_::*func)())
{
    return scl_mem_fun_t<Result_, Type_>(func);
}

template <class Result_, class Type_, class Arg_>
scl_mem_fun1_t<Result_, Type_, Arg_>
scl_mem_fun(Result_ (Type_::*func)(Arg_))
{
    return scl_mem_fun1_t<Result_, Type_, Arg_>(func);
}

template <class Result_, class Type_>
class scl_mem_fun_ref_t : public scl_unary_function<Type_, Result_>
{
public:
    typedef Result_ (Type_::*Func_)();

    explicit
    scl_mem_fun_ref_t(Func_ func) : func_(func) { }

    Result_ operator()(Type_& obj) const { return (obj.*func_)(); }

private:
    Func_   func_;
};

template <class Result_, class Type_, class Arg_>
class scl_mem_fun1_ref_t : public scl_binary_function<Type_, Arg_, Result_>
{
public:
    typedef Result_ (Type_::*Func_)(Arg_);

    explicit
    scl_mem_fun1_ref_t(Func_ func) : func_(func) { }

    Result_ operator()(Type_& obj, Arg_ x) const { return (obj.*func_)(x); }

private:
    Func_   func_;
};

template <class Result_, class Type_>
scl_mem_fun_ref_t<Result_, Type_>
scl_mem_fun_ref(Result_ (Type_::*func)())
{
    return scl_mem_fun_ref_t<Result_, Type_>(func);
}

template <class Result_, class Type_, class Arg_>
scl_mem_fun1_ref_t<Result_, Type_, Arg_>
scl_mem_fun_ref(Result_ (Type_::*func)(Arg_))
{
    return scl_mem_fun1_ref_t<Result_, Type_, Arg_>(func);
}

template <class Result_, class Type_>
class scl_const_mem_fun_t : public scl_unary_function<const Type_*, Result_>
{
public:
    typedef Result_ (Type_::*Func_)() const;

    explicit
    scl_const_mem_fun_t(Func_ func) : func_(func) { }

    Result_ operator()(const Type_* obj) const { return (obj->*func_)(); }

private:
    Func_   func_;
};

template <class Result_, class Type_, class Arg_>
class scl_const_mem_fun1_t : public scl_binary_function<const Type_*, Arg_, Result_>
{
public:
    typedef Result_ (Type_::*Func_)(Arg_) const;

    explicit
    scl_const_mem_fun1_t(Func_ func) : func_(func) { }

    Result_ operator()(const Type_* obj, Arg_ x) const { return (obj->*func_)(x); }

private:
    Func_   func_;
};

template <class Result_, class Type_>
scl_const_mem_fun_t<Result_, Type_>
scl_mem_fun(Result_ (Type_::*func)() const)
{
    return scl_const_mem_fun_t<Result_, Type_>(func);
}

template <class Result_, class Type_, class Arg_>
scl_const_mem_fun1_t<Result_, Type_, Arg_>
scl_mem_fun(Result_ (Type_::*func)(Arg_) const)
{
    return scl_const_mem_fun1_t<Result_, Type_, Arg_>(func);
}

template <class Result_, class Type_>
class scl_const_mem_fun_ref_t : public scl_unary_function<Type_, Result_>
{
public:
    typedef Result_ (Type_::*Func_)() const;

    explicit
    scl_const_mem_fun_ref_t(Func_ func) : func_(func) { }

    Result_ operator()(const Type_& obj) const { return (obj.*func_)(); }

private:
    Func_   func_;
};

template <class Result_, class Type_, class Arg_>
class scl_const_mem_fun1_ref_t : public scl_binary_function<Type_, Arg_, Result_>
{
public:
    typedef Result_ (Type_::*Func_)(Arg_) const;

    explicit
    scl_const_mem_fun1_ref_t(Func_ func) : func_(func) { }

    Result_ operator()(const Type_& obj, Arg_ x) const { return (obj.*func_)(x); }

private:
    Func_   func_;
};

template <class Result_, class Type_>
scl_const_mem_fun_ref_t<Result_, Type_>
scl_mem_fun_ref(Result_ (Type_::*func)() const)
{
    return scl_const_mem_fun_ref_t<Result_, Type_>(func);
}

template <class Result_, class Type_, class Arg_>
scl_const_mem_fun1_ref_t<Result_, Type_, Arg_>
scl_mem_fun_ref(Result_ (Type_::*func)(Arg_) const)
{
    return scl_const_mem_fun1_ref_t<Result_, Type_, Arg_>(func);
}


//
//  The following functions are not part of the C++ standard.
//
template <class Type_>
struct scl_identity : public scl_unary_function<Type_, Type_>
{
    const Type_&        operator()(const Type_& x) const { return x; }
};

template <class Pair_>
struct scl_select1st : public scl_unary_function<Pair_, typename Pair_::first_type>
{
    typedef typename Pair_::first_type  first_type;

    const first_type&   operator()(const Pair_& x) const { return x.first; }
};

template <class Pair_>
struct scl_select2nd : public scl_unary_function<Pair_, typename Pair_::second_type>
{
    typedef typename Pair_::second_type second_type;

    const second_type&  operator()(const Pair_& x) const { return x.second; }
};


#endif  /* _SCLP_FUNCTIONAL_H_ */
