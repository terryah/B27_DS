/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: scl_list.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Oct-2004     Initial implementation.
//
//  NOTICE:
//      This header implements functionality in the Standard C++ Library.
//      It may be included directly in application code.
//
//==============================================================================
#ifndef _SCL_LIST_H_
#define _SCL_LIST_H_


#ifndef  _SCL_CONFIG_H_
#include <scl_config.h>
#endif


#ifdef  SCL_USE_VENDOR_STD
#include <scl_memory.h>
#include <list>

#define scl_list                        SCL_VENDOR_STD::list
#ifndef scl_swap
#define scl_swap                        SCL_VENDOR_STD::swap
#endif

#else
#include <sclp_list.h>

#endif  /* SCL_USE_VENDOR_STD */

#define os_list scl_list

#endif  /* _SCL_LIST_H_ */
