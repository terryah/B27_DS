/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
//
//  FILE: sclp_set.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-May-2005     Initial implementation.
//
//  NOTICE:
//      This header is used internally by the Standard C++ Library.
//      It should not be included directly in application code.
//
//==============================================================================
#ifndef _SCLP_SET_H_
#define _SCLP_SET_H_


//
//  Include section
//
#ifndef  _SCLP_ALLOCATOR_H_
#include <sclp_allocator.h>             // for scl_allocator
#endif

#ifndef  _SCLP_FUNCTIONAL_H_
#include <sclp_functional.h>            // for scl_less, scl_identity
#endif

#ifndef  _SCLP_RBTREE_H_
#include <sclp_rbtree.h>                // for scl_rbtree
#endif

#ifndef  _SCLP_BASE_ALGO_H_
#include <sclp_base_algo.h>             // for scl_equal(), scl_lexicographical_compare()
#endif


//
//  Template class scl_set
//
#ifdef  SCL_HAS_DEFAULT_TMPL_ARGS
template <class Key_, class Comp_ = scl_less<Key_>, class Alloc_ = scl_allocator<Key_> >
#else
template <class Key_, class Comp_, class Alloc_>
#endif
class scl_set
{
public:
    // Private types
    typedef scl_set<Key_, Comp_, Alloc_>            Self_;
    typedef scl_rbtree<Key_, Key_,
        scl_identity<Key_>, Comp_, Alloc_>          Tree_;
    typedef typename Tree_::iterator                TreeIter_;
    enum { Multi_ = false };

    // Public types
    typedef Key_                                    key_type;
    typedef Key_                                    value_type;
    typedef Comp_                                   key_compare;
    typedef Comp_                                   value_compare;
    typedef Alloc_                                  allocator_type;
    typedef typename Alloc_::pointer                pointer;
    typedef typename Alloc_::const_pointer          const_pointer;
    typedef typename Alloc_::reference              reference;
    typedef typename Alloc_::const_reference        const_reference;
    typedef typename Alloc_::size_type              size_type;
    typedef typename Alloc_::difference_type        difference_type;

    // Note: All iterators are 'const' to prevent modification of keys.
    typedef typename Tree_::const_iterator          iterator;
    typedef typename Tree_::const_iterator          const_iterator;
    typedef scl_reverse_iterator<const_iterator>    reverse_iterator;
    typedef scl_reverse_iterator<const_iterator>    const_reverse_iterator;

    // 23.3.3.1 construct/copy/destroy
    scl_set() :
        tree_(Multi_)
    {
        // Nothing
    }

    explicit
    scl_set(const Comp_& comp) :
        tree_(Multi_, comp)
    {
        // Nothing
    }

    scl_set(const Comp_& comp, const Alloc_& alloc) :
        tree_(Multi_, comp, alloc)
    {
        // Nothing
    }

#ifdef  SCL_HAS_MEMBER_TEMPLATES
    template <class InputIterator_>
    scl_set(InputIterator_ first, InputIterator_ last) :
        tree_(Multi_)
    {
        tree_.insert(first, last);
    }

    template <class InputIterator_>
    scl_set(InputIterator_ first, InputIterator_ last, const Comp_& comp) :
        tree_(Multi_, comp)
    {
        tree_.insert(first, last);
    }

    template <class InputIterator_>
    scl_set(InputIterator_ first, InputIterator_ last, const Comp_& comp, const Alloc_& alloc) :
        tree_(Multi_, comp, alloc)
    {
        tree_.insert(first, last);
    }
#else
    scl_set(const_iterator first, const_iterator last) :
        tree_(Multi_)
    {
        tree_.insert(first, last);
    }

    scl_set(const_iterator first, const_iterator last, const Comp_& comp) :
        tree_(Multi_, comp)
    {
        tree_.insert(first, last);
    }

    scl_set(const_iterator first, const_iterator last, const Comp_& comp, const Alloc_& alloc) :
        tree_(Multi_, comp, alloc)
    {
        tree_.insert(first, last);
    }

    scl_set(const_pointer  first, const_pointer  last) :
        tree_(Multi_)
    {
        tree_.insert(first, last);
    }

    scl_set(const_pointer  first, const_pointer  last, const Comp_& comp) :
        tree_(Multi_, comp)
    {
        tree_.insert(first, last);
    }

    scl_set(const_pointer  first, const_pointer  last, const Comp_& comp, const Alloc_& alloc) :
        tree_(Multi_, comp, alloc)
    {
        tree_.insert(first, last);
    }
#endif

    scl_set(const Self_& other) :
        tree_(other.tree_)
    {
        // Nothing
    }

    ~scl_set()                                      { }
    Self_&          operator=(const Self_& other)   { tree_ = other.tree_; return *this;        }
    allocator_type  get_allocator() const           { return tree_.get_allocator();             }

    // iterators
    iterator        begin()                         { return tree_.begin();                     }
    const_iterator  begin() const                   { return tree_.begin();                     }
    iterator        end()                           { return tree_.end();                       }
    const_iterator  end() const                     { return tree_.end();                       }

    reverse_iterator        rbegin()                { return       reverse_iterator(end());     }
    const_reverse_iterator  rbegin() const          { return const_reverse_iterator(end());     }
    reverse_iterator        rend()                  { return       reverse_iterator(begin());   }
    const_reverse_iterator  rend() const            { return const_reverse_iterator(begin());   }

    // capacity
    bool            empty() const                   { return tree_.empty();                     }
    size_type       size() const                    { return tree_.size();                      }
    size_type       max_size() const                { return tree_.max_size();                  }

    // modifiers
    scl_pair<iterator, bool>
    insert(const Key_& key)
    {
        typedef scl_pair<TreeIter_, bool>   Result_;
        Result_ result = tree_.insert(key);
        return scl_pair<iterator, bool>(result.first, result.second);
    }

    iterator
    insert(iterator pos, const Key_& key)               { return tree_.insert(cast(pos), key);  }

#ifdef  SCL_HAS_MEMBER_TEMPLATES
    template <class InputIterator_>
    void
    insert(InputIterator_ first, InputIterator_ last)   { tree_.insert(first, last);            }
#else
    void
    insert(const_iterator first, const_iterator last)   { tree_.insert(first, last);            }

    void
    insert(const_pointer  first, const_pointer  last)   { tree_.insert(first, last);            }
#endif

    void            erase(iterator pos)                 { tree_.erase(cast(pos));               }
    size_type       erase(const Key_& key)              { return tree_.erase(key);              }
    void            erase(iterator first, iterator last){ tree_.erase(cast(first), cast(last)); }
    void            swap(Self_& other)                  { tree_.swap(other.tree_);              }
    void            clear()                             { tree_.clear();                        }

    // observers
    key_compare     key_comp() const                    { return tree_.key_comp();              }
    value_compare   value_comp() const                  { return tree_.key_comp();              }

    // set operations
    iterator        find(const Key_& key)               { return tree_.find(key);               }
    const_iterator  find(const Key_& key) const         { return tree_.find(key);               }
    size_type       count(const Key_& key) const        { return tree_.count(key);              }
    iterator        lower_bound(const Key_& key)        { return tree_.lower_bound(key);        }
    const_iterator  lower_bound(const Key_& key) const  { return tree_.lower_bound(key);        }
    iterator        upper_bound(const Key_& key)        { return tree_.upper_bound(key);        }
    const_iterator  upper_bound(const Key_& key) const  { return tree_.upper_bound(key);        }

    scl_pair<iterator, iterator>
    equal_range(const Key_& key)
    {
        const Tree_&    ctree = const_cast<const Tree_&>(tree_);
        return ctree.equal_range(key);
    }

    scl_pair<const_iterator, const_iterator>
    equal_range(const Key_& key) const
    {
        return tree_.equal_range(key);
    }

    // Test class invariants (nonstandard method).
    bool            _validate() const                   { return tree_._validate();             }

protected:
    TreeIter_       cast(const iterator &iter)
    {
        typedef _scl_rbtree_base_node   BNode_;
        return TreeIter_(const_cast<BNode_ *>(iter.node_));
    }

    Tree_           tree_;      // Red-black tree with unique keys.
};


template <class Key_, class Comp_, class Alloc_>
bool
operator==(const scl_set<Key_, Comp_, Alloc_>& lhs,
           const scl_set<Key_, Comp_, Alloc_>& rhs)
{
    return (lhs.size() == rhs.size()) && scl_equal(lhs.begin(), lhs.end(), rhs.begin());
}

template <class Key_, class Comp_, class Alloc_>
bool
operator< (const scl_set<Key_, Comp_, Alloc_>& lhs,
           const scl_set<Key_, Comp_, Alloc_>& rhs)
{
    return scl_lexicographical_compare(lhs.begin(), lhs.end(), rhs.begin(), rhs.end());
}

template <class Key_, class Comp_, class Alloc_>
bool
operator!=(const scl_set<Key_, Comp_, Alloc_>& lhs,
           const scl_set<Key_, Comp_, Alloc_>& rhs)
{
    return !(lhs == rhs);
}

template <class Key_, class Comp_, class Alloc_>
bool
operator<=(const scl_set<Key_, Comp_, Alloc_>& lhs,
           const scl_set<Key_, Comp_, Alloc_>& rhs)
{
    return !(rhs < lhs);
}

template <class Key_, class Comp_, class Alloc_>
bool
operator>=(const scl_set<Key_, Comp_, Alloc_>& lhs,
           const scl_set<Key_, Comp_, Alloc_>& rhs)
{
    return !(lhs < rhs);
}

template <class Key_, class Comp_, class Alloc_>
bool
operator> (const scl_set<Key_, Comp_, Alloc_>& lhs,
           const scl_set<Key_, Comp_, Alloc_>& rhs)
{
    return (rhs < lhs);
}

#ifdef  SCL_HAS_PARTIAL_SPEC_OVERLOAD
template <class Key_, class Comp_, class Alloc_>
void
scl_swap(scl_set<Key_, Comp_, Alloc_>& lhs,
         scl_set<Key_, Comp_, Alloc_>& rhs)
{
    lhs.swap(rhs);
}
#endif


#endif  /* _SCLP_SET_H_ */
