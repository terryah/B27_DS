/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: scl_stdexcept.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Aug-2004     Initial implementation.
//
//  NOTICE:
//      This header implements functionality in the Standard C++ Library.
//      It may be included directly in application code.
//
//==============================================================================
#ifndef _SCL_STDEXCEPT_H_
#define _SCL_STDEXCEPT_H_


//
//  Include section
//
#ifndef  _SCL_CONFIG_H_
#include <scl_config.h>
#endif

#ifndef  _SCL_EXCEPTION_H_
#include <scl_exception.h>
#endif

#ifndef  _SCL_STRING_H_
#include <scl_string.h>
#endif


//
//  Private class _scl_named_exception
//
class SCL_STD_EXPORT _scl_named_exception : public scl_exception
{
public:
    _scl_named_exception(const scl_string& what);
    virtual ~_scl_named_exception() SCL_NOTHROW;
    virtual const char* what() const SCL_NOTHROW;
private:
    scl_string  what_;
};


//
//  Standard logic errors
//
class SCL_STD_EXPORT scl_logic_error : public _scl_named_exception
{
public:
    scl_logic_error(const scl_string& what);
    virtual ~scl_logic_error() SCL_NOTHROW;
};

class SCL_STD_EXPORT scl_domain_error : public scl_logic_error
{
public:
    scl_domain_error(const scl_string& what);
    virtual ~scl_domain_error() SCL_NOTHROW;
};

class SCL_STD_EXPORT scl_invalid_argument : public scl_logic_error
{
public:
    scl_invalid_argument(const scl_string& what);
    virtual ~scl_invalid_argument() SCL_NOTHROW;
};

class SCL_STD_EXPORT scl_length_error : public scl_logic_error
{
public:
    scl_length_error(const scl_string& what);
    virtual ~scl_length_error() SCL_NOTHROW;
};

class SCL_STD_EXPORT scl_out_of_range : public scl_logic_error
{
public:
    scl_out_of_range(const scl_string& what);
    virtual ~scl_out_of_range() SCL_NOTHROW;
};


//
//  Standard runtime errors
//
class SCL_STD_EXPORT scl_runtime_error : public _scl_named_exception
{
public:
    scl_runtime_error(const scl_string& what);
    virtual ~scl_runtime_error() SCL_NOTHROW;
};

class SCL_STD_EXPORT scl_range_error : public scl_runtime_error
{
public:
    scl_range_error(const scl_string& what);
    virtual ~scl_range_error () SCL_NOTHROW;
};

class SCL_STD_EXPORT scl_overflow_error : public scl_runtime_error
{
public:
    scl_overflow_error(const scl_string& what);
    virtual ~scl_overflow_error () SCL_NOTHROW;
};

class SCL_STD_EXPORT scl_underflow_error : public scl_runtime_error
{
public:
    scl_underflow_error(const scl_string& what);
    virtual ~scl_underflow_error () SCL_NOTHROW;
};


//
//  Non-standard runtime errors (implementation extensions)
//
class SCL_STD_EXPORT scl_resource_limit : public scl_runtime_error
{
public:
    scl_resource_limit(const scl_string& what);
    virtual ~scl_resource_limit () SCL_NOTHROW;
};

class SCL_STD_EXPORT scl_deadlock_error : public scl_runtime_error
{
public:
    scl_deadlock_error(const scl_string& what);
    virtual ~scl_deadlock_error () SCL_NOTHROW;
};

class SCL_STD_EXPORT scl_permission_error : public scl_runtime_error
{
public:
    scl_permission_error(const scl_string& what);
    virtual ~scl_permission_error () SCL_NOTHROW;
};


#endif  /* _SCL_STDEXCEPT_H_ */
