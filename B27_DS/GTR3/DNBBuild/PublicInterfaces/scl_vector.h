/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: scl_vector.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Oct-2004     Initial implementation.
//
//  NOTICE:
//      This header implements functionality in the Standard C++ Library.
//      It may be included directly in application code.
//
//==============================================================================
#ifndef _SCL_VECTOR_H_
#define _SCL_VECTOR_H_


#ifndef  _SCL_CONFIG_H_
#include <scl_config.h>
#endif


#ifdef  SCL_USE_VENDOR_STD
#include <scl_memory.h>
#include <vector>

#define scl_vector                      SCL_VENDOR_STD::vector
#ifndef scl_swap
#define scl_swap                        SCL_VENDOR_STD::swap
#endif

#else
#include <sclp_vector.h>

#endif  /* SCL_USE_VENDOR_STD */


#endif  /* _SCL_VECTOR_H_ */
