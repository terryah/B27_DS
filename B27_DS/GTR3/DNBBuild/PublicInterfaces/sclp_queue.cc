/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: sclp_queue.cc
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Aug-2004     Initial implementation.
//
//==============================================================================
#define SCL_PQUEUE_TEMPL    template <class Type_, class Cntr_, class Comp_>
#define SCL_PQUEUE_CLASS    scl_priority_queue<Type_, Cntr_, Comp_>

SCL_PQUEUE_TEMPL
SCL_PQUEUE_CLASS::scl_priority_queue() :
    c   (),
    comp()
{
    // Nothing
}

SCL_PQUEUE_TEMPL
SCL_PQUEUE_CLASS::scl_priority_queue(const Comp_& cmpr) :
    c   (),
    comp(cmpr)
{
    // Nothing
}

SCL_PQUEUE_TEMPL
SCL_PQUEUE_CLASS::scl_priority_queue(const Comp_& cmpr, const Cntr_& cntr) :
    c   (cntr),
    comp(cmpr)
{
    scl_make_heap(c.begin(), c.end(), comp);
}

SCL_PQUEUE_TEMPL
#ifdef  SCL_HAS_MEMBER_TEMPLATES
template <class InpIter_>
#endif
SCL_PQUEUE_CLASS::scl_priority_queue(InpIter_ first, InpIter_ last) :
    c   (first, last),
    comp()
{
    scl_make_heap(c.begin(), c.end(), comp);
}

SCL_PQUEUE_TEMPL
#ifdef  SCL_HAS_MEMBER_TEMPLATES
template <class InpIter_>
#endif
SCL_PQUEUE_CLASS::scl_priority_queue(InpIter_ first, InpIter_ last, const Comp_& cmpr) :
    c   (first, last),
    comp(cmpr)
{
    scl_make_heap(c.begin(), c.end(), comp);
}

SCL_PQUEUE_TEMPL
#ifdef  SCL_HAS_MEMBER_TEMPLATES
template <class InpIter_>
#endif
SCL_PQUEUE_CLASS::scl_priority_queue(InpIter_ first, InpIter_ last, const Comp_& cmpr,
    const Cntr_& cntr) :
    c   (cntr),
    comp(cmpr)
{
    c.insert(c.end(), first, last);
    scl_make_heap(c.begin(), c.end(), comp);
}

SCL_PQUEUE_TEMPL
void
SCL_PQUEUE_CLASS::push(const value_type& val)
{
    try
    {
        c.push_back(val);
        scl_push_heap(c.begin(), c.end(), comp);
    }
    catch (...)
    {
        c.clear();
        throw;
    }
}

SCL_PQUEUE_TEMPL
void
SCL_PQUEUE_CLASS::pop()
{
    try
    {
        scl_pop_heap(c.begin(), c.end(), comp);
        c.pop_back();
    }
    catch (...)
    {
        c.clear();
        throw;
    }
}

#undef  SCL_PQUEUE_TEMPL
#undef  SCL_PQUEUE_CLASS
