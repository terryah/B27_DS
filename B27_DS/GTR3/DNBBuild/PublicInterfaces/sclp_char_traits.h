/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: sclp_char_traits.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Aug-2004     Initial implementation.
//
//  NOTICE:
//      This header is used internally by the Standard C++ Library.
//      It should not be included directly in application code.
//
//==============================================================================
#ifndef _SCLP_CHAR_TRAITS_H_
#define _SCLP_CHAR_TRAITS_H_


//
//  Include section
//
#ifndef  _SCL_CWCHAR_H_
#include <scl_cwchar.h>                 // for wchar_t, wint_t
#endif


//
//  Forward declarations
//
template <class Type_>
struct scl_char_traits;

SCL_SPECIALIZE_CLASS struct SCL_STD_EXPORT scl_char_traits<char>;
SCL_SPECIALIZE_CLASS struct SCL_STD_EXPORT scl_char_traits<wchar_t>;


//
//  21.1.1 Character traits
//
template <class Type_>
struct scl_char_traits {};


//
//  21.1.3 scl_char_traits specialization
//
SCL_SPECIALIZE_CLASS
struct SCL_STD_EXPORT scl_char_traits<char>
{
    typedef char        char_type;
    typedef int         int_type;

//  #ifdef  SCL_USE_STD_IOSTREAM
//  typedef streamoff   off_type;
//  typedef streampos   pos_type;
//  typedef mbstate_t   state_type;
//  #endif

    static void     assign(char& c1, const char& c2)   { c1 = c2; }

    static bool     eq(const char& c1, const char& c2) { return c1 == c2; }

    static bool     lt(const char& c1, const char& c2) { return c1  < c2; }

    static int      compare(const char* s1, const char* s2, size_t n);

    static size_t   length(const char* s);

    static const char*  find(const char* s, size_t n, const char& a);

    static char*    move(char* s1, const char* s2, size_t n);

    static char*    copy(char* s1, const char* s2, size_t n);

    static char*    assign(char* s, size_t n, char a);

    static int_type not_eof(const int_type& c);

    static char     to_char_type(const int_type& c);

    static int_type to_int_type(const char& c);

    static bool     eq_int_type(const int_type& c1, const int_type& c2);

    static int_type eof();

    // SCL extensions.
    static char     eos()       { return '\0'; }

    static char     newline()   { return '\n'; }
};


SCL_SPECIALIZE_CLASS
struct SCL_STD_EXPORT scl_char_traits<wchar_t>
{
    typedef wchar_t     char_type;
    typedef wint_t      int_type;

//  #ifdef  SCL_USE_STD_IOSTREAM
//  typedef streamoff   off_type;
//  typedef wstreampos  pos_type;
//  typedef mbstate_t   state_type;
//  #endif

    static void     assign(wchar_t& c1, const wchar_t& c2)   { c1 = c2; }

    static bool     eq(const wchar_t& c1, const wchar_t& c2) { return c1 == c2; }

    static bool     lt(const wchar_t& c1, const wchar_t& c2) { return c1  < c2; }

    static int      compare(const wchar_t* s1, const wchar_t* s2, size_t n);

    static size_t   length(const wchar_t* s);

    static const wchar_t*   find(const wchar_t* s, size_t n, const wchar_t& a);

    static wchar_t* move(wchar_t* s1, const wchar_t* s2, size_t n);

    static wchar_t* copy(wchar_t* s1, const wchar_t* s2, size_t n);

    static wchar_t* assign(wchar_t* s, size_t n, wchar_t a);

    static int_type not_eof(const int_type& c);

    static wchar_t  to_char_type(const int_type& c);

    static int_type to_int_type(const wchar_t& c);

    static bool     eq_int_type(const int_type& c1, const int_type& c2);

    static int_type eof();

    // SCL extensions.
    static wchar_t  eos()       { return L'\0'; }

    static wchar_t  newline()   { return L'\n'; }
};


#endif  /* _SCLP_CHAR_TRAITS_H_ */
