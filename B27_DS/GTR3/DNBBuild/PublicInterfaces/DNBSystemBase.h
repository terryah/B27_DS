/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 1997
//==============================================================================
//
//  FILE: DNBSystemBase.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     23-Jun-1997     Initial implementation.
//      jod     01-Jul-2008     Simplified contents.
//
//  OVERVIEW:
//      This header file defines the capabilities and features of the different
//      development environments currently supported by the DELMIA simulation
//      toolkit.  It essentially provides an information database characterizing
//      important attributes of the compilation environment (i.e., operating system
//      and compiler).  For each such attribute or feature of interest, a symbolic
//      constant is defined indicating that the feature is supported (DNB_YES)
//      or not available (DNB_NO).  The collection of values corresponding to a
//      particular development environment is defined by examining the space of
//      predefined identifiers.  Using this technique, the module can easily
//      accommodate multiple platforms and/or compilers.
//
//      By convention, every DELMIA simulation source file--including header files
//      and inline definition files--must include the headers <DNBSystemBase.h> and
//      <DNBSystemDefs.h>.  If the source file requires any third-party headers,
//      they must be included between <DNBSystemBase.h> and <DNBSystemDefs.h>.  To
//      be precise, the include directives for the source file should appear in the
//      following order:
//
//          #include <DNBSystemBase.h>
//          // Standard C++ headers         (e.g., <scl_vector.h>)
//          // Standard C headers           (e.g., <scl_cstdio.h>)
//          // Operating system headers     (e.g., <sys/types.h>)
//          #include <DNBSystemDefs.h>
//
//          // DELMIA headers               (e.g., <DNBxxx.h>)
//          // CATIA headers                (e.g., <CATxxx.h>)
//
//      By adhering to the above conventions, the headers <DNBSystemBase.h> and
//      <DNBSystemDefs.h> can correct any questionable constructs that appear in
//      third-party headers.
//
//==============================================================================
#ifndef _DNBSYSTEMBASE_H_
#define _DNBSYSTEMBASE_H_


#ifndef  _SCL_STDEXCEPT_H_
#include <scl_stdexcept.h>      /* for scl_exception and friends    */
#endif

#ifndef  _SCL_TYPEINFO_H_
#include <scl_typeinfo.h>       /* for scl_type_info, scl_bad_cast  */
#endif

#ifndef  _SCL_NEW_H_
#include <scl_new.h>            /* for scl_bad_alloc                */
#endif




        //************************************************************//
        //************************************************************//
        //*                                                          *//
        //*                    MANIFEST CONSTANTS                    *//
        //*                                                          *//
        //************************************************************//
        //************************************************************//


/**
 *  Specifies the Boolean value FALSE.
 *
 *  @description
 *      This macro represents the Boolean value FALSE (0).  It is typically used
 *      to specify that a particular feature is not available in the compilation
 *      environment.
 */
#define DNB_NO                  0




/**
 *  Specifies the Boolean value TRUE.
 *
 *  @description
 *      This macro represents the Boolean value TRUE (1).  It is typically used
 *      to specify that a particular feature is available in the compilation
 *      environment.
 */
#define DNB_YES                 1




/**
 *  Specifies whether run-time assertions are enabled in library code.
 *
 *  @description
 *      This macro specifies whether run-time assertions are enabled in library
 *      code.  Assertions are Boolean expressions that define the correct state
 *      of the program at particular locations in the source code.  Assertions
 *      are expressed using the macros @href DNB_ASSERT, @href DNB_PRECONDITION,
 *      and @href DNB_POSTCONDITION.  If DNB_VERIFY is TRUE, each call to an
 *      assertion macro will evaluate and test its specified condition.  Otherwise,
 *      each call is replaced by an empty statement.
 *      <p>
 *      Assertions are enabled by specifying the mkmk option "-dev".
 */
#ifdef  CNEXT_CLIENT            /* Defined during release builds. */
#undef  DNB_VERIFY
#define DNB_VERIFY              DNB_NO
#else
#ifndef DNB_VERIFY
#define DNB_VERIFY              DNB_YES
#endif
#endif  /* CNEXT_CLIENT */




/**
 *  Specifies the default trace level for localized C++ exceptions.
 *
 *  @description
 *      This macro specifies the default trace level for the class DNBException.
 *      In debug builds, all traces except for entry/exit are generated.  In
 *      release builds, all traces are disabled.
 */
#if     DNB_VERIFY
#define DNB_EXCEPT_TRACE_LEVEL  7
#else
#define DNB_EXCEPT_TRACE_LEVEL  0
#endif




/**
 *  Specifies whether the messaging facility supports UNICODE strings.
 *
 *  @description
 *      This macro specifies the character type used by the messaging facility
 *      (see class @href DNBMessage).  If the macro is TRUE, all internal messages
 *      are represented by wide-character strings (WCS).  Such strings may contain
 *      symbols from the ISO-10646 (UNICODE) character set.  If the macro is FALSE,
 *      all internal messages are represented by single-byte character strings
 *      (SBCS).  Such strings may contain symbols from any ISO-8859 character set.
 */
#undef  DNB_USE_UNICODE
#define DNB_USE_UNICODE         DNB_YES




/**
 * @nodoc
 */
#ifndef CAT_ENABLE_NATIVE_EXCEPTION
#define CAT_ENABLE_NATIVE_EXCEPTION     // Enable use of try, catch and throw
#endif




        //************************************************************//
        //************************************************************//
        //*                                                          *//
        //*              COMPILER CAPABILITIES DATABASE              *//
        //*                                                          *//
        //************************************************************//
        //************************************************************//


//
//  Load platform feature-test macros.
//
#if     defined(_WINDOWS_SOURCE)
#if     defined(PLATEFORME_DS64)
#include <DNBCFwin64.h>                 // Windows 64-bit (MSVC 8.x)
#else
#include <DNBCFwin32.h>                 // Windows 32-bit (MSVC 8.x)
#endif  /* PLATEFORME_DS64 */

#elif   defined(_IRIX_SOURCE)
#include <DNBCFsgi.h>                   // SGI IRIX 32-bit (C++ 7.2.1)

#elif   defined(_SUNOS_SOURCE)
#include <DNBCFsun.h>                   // Sun Solaris 32-bit (C++ 5.7)

#elif   defined(_AIX_SOURCE)
#include <DNBCFaix.h>                   // IBM AIX 32-bit and 64-bit (XL C++ 8.0)

#elif   defined(_HPUX_SOURCE)
#include <DNBCFhpux.h>                  // HP-UX 32-bit (aC++ A.03.52)

#else
#error  "Unsupported platform"
#endif


//
//  Make sure the current compilation environment is supported.  Check any macro
//  that is defined in the platform configuration files.
//
#ifndef DNB_HAS_POSIX_THREADS
#error  "Incomplete platform definition"
#endif




//
//  Supress exception throw specs to prevent undeclared exceptions from terminating
//  the program.
//
#undef  DNB_HAS_THROW_SPECS
#define DNB_HAS_THROW_SPECS         DNB_NO




/**
 *  Specifies a standard-conforming implementation of "operator new".
 *
 *  @example
 *  <pre>
 *      int    *num = NULL;
 *      char   *str = NULL;
 *      Object *foo = NULL;
 *      Object *bar = NULL;
 *
 *      try
 *      {
 *          num = DNB_NEW int( 5 );         // single POD
 *          str = DNB_NEW char[ 512 ];      // array of PODs
 *          foo = DNB_NEW Object( 5 );      // single class object
 *          bar = DNB_NEW Object[ 2 ];      // array of class objects
 *      }
 *      catch ( scl_bad_alloc &ex )
 *      {
 *          cerr << "Allocation failed: " << ex.what() << endl;
 *      }
 *  </pre>
 */
#define DNB_NEW         new




/**
 *  Specifies a standard-conforming implementation of "operator delete".
 *
 *  @example
 *  <pre>
 *      int    *num = DNB_NEW int( 5 );         // single POD
 *      char   *str = DNB_NEW char[ 512 ];      // array of PODs
 *      Object *foo = DNB_NEW Object( 5 );      // single class object
 *      Object *bar = DNB_NEW Object[ 2 ];      // array of class objects
 *
 *      DNB_DELETE num;
 *      DNB_DELETE [] str;
 *      DNB_DELETE foo;
 *      DNB_DELETE [] bar;
 *  </pre>
 */
#define DNB_DELETE      delete




#endif  /* _DNBSYSTEMBASE_H_ */
