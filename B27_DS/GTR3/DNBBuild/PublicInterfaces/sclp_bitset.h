/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: sclp_bitset.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Oct-2004     Initial implementation.
//
//  NOTICE:
//      This header is used internally by the Standard C++ Library.
//      It should not be included directly in application code.
//
//==============================================================================
#ifndef _SCLP_BITSET_H_
#define _SCLP_BITSET_H_


//
//  Include section
//
#ifndef  _SCL_CLIMITS_H_
#include <scl_climits.h>                // for CHAR_BIT
#endif

#ifndef  _SCL_IOSFWD_H_
#include <scl_iosfwd.h>                 // for istream, ostream
#endif

#ifndef  _SCLP_BITSET_BASE_H_
#include <sclp_bitset_base.h>           // for _scl_bitset_base
#endif


//
//  Template class scl_bitset
//
template <size_t Size_>
class scl_bitset : public _scl_bitset_base
{
public:
    // Private types
    typedef _scl_bitset_base            Base_;
    typedef scl_bitset<Size_>           Self_;

    // bit reference
    typedef _scl_bitset_reference       reference;

    // 23.3.5.1 construct/copy/destroy
                    scl_bitset();
                    scl_bitset(unsigned long value);
    explicit        scl_bitset(const scl_string& cstr);
                    scl_bitset(const scl_string& cstr, size_t cpos);
                    scl_bitset(const scl_string& cstr, size_t cpos, size_t clen);
                    scl_bitset(const Self_& other);
                    ~scl_bitset();
    Self_&          operator=(const Self_& other);

    // 23.3.5.2 scl_bitset operations
    Self_&          operator&=(const Self_& rhs);
    Self_&          operator|=(const Self_& rhs);
    Self_&          operator^=(const Self_& rhs);
    Self_&          operator<<=(size_t count);
    Self_&          operator>>=(size_t count);
    Self_&          set();
    Self_&          set(size_t pos, bool value = true);
    Self_&          reset();
    Self_&          reset(size_t pos);
    Self_           operator~() const;
    Self_&          flip();
    Self_&          flip(size_t pos);

    // element access
//  bool            operator[](size_t pos) const;
//  reference       operator[](size_t pos);

//  unsigned long   to_ulong() const;
//  scl_string      to_string() const;
//  size_t          count() const;
//  size_t          size() const;
    bool            operator==(const Self_& rhs) const;
    bool            operator!=(const Self_& rhs) const;
//  bool            test(size_t pos) const;
//  bool            any() const;
//  bool            none() const;
    Self_           operator<<(size_t count) const;
    Self_           operator>>(size_t count) const;

    // 23.3.5.3 scl_bitset operations
#ifndef SCL_HAS_NONTYPE_TMPL_ARGS
    friend Self_    operator&(const Self_& lhs, const Self_& rhs)
    {
        Self_   result(lhs);
        result &= rhs;
        return result;
    }

    friend Self_    operator|(const Self_& lhs, const Self_& rhs)
    {
        Self_   result(lhs);
        result |= rhs;
        return result;
    }

    friend Self_    operator^(const Self_& lhs, const Self_& rhs)
    {
        Self_   result(lhs);
        result ^= rhs;
        return result;
    }

#ifdef  SCL_ENABLE_IO
    friend ostream& operator<<(ostream& os, const Self_& item)
    {
        os << item.to_string();
        return os;
    }
#endif  /* SCL_ENABLE_IO             */
#endif  /* SCL_HAS_NONTYPE_TMPL_ARGS */

private:
    enum { WordBits = CHAR_BIT * sizeof(WordType) };
    enum { NumWords = (Size_ == 0) ? 1 : ((Size_ - 1) / WordBits + 1) };

    WordType        buffer_[NumWords];
};


//
//  23.3.5.3 scl_bitset operations
//
#ifdef  SCL_HAS_NONTYPE_TMPL_ARGS
template <size_t Size_>
scl_bitset<Size_>
operator&(const scl_bitset<Size_>& lhs, const scl_bitset<Size_>& rhs);

template <size_t Size_>
scl_bitset<Size_>
operator|(const scl_bitset<Size_>& lhs, const scl_bitset<Size_>& rhs);

template <size_t Size_>
scl_bitset<Size_>
operator^(const scl_bitset<Size_>& lhs, const scl_bitset<Size_>& rhs);

template <size_t Size_>
ostream&
operator<<(ostream& os, const scl_bitset<Size_>& item);
#endif  /* SCL_HAS_NONTYPE_TMPL_ARGS */


#if 0
template <class Type_, class Traits_, size_t Size_>
basic_istream<Type_, Traits_>&
operator>>(basic_istream<Type_, Traits_>& is, scl_bitset<Size_>& item);

template <class Type_, class Traits_, size_t Size_>
basic_ostream<Type_, Traits_>&
operator<<(basic_ostream<Type_, Traits_>& os, const scl_bitset<Size_>& item);
#endif


#include <sclp_bitset.cc>


#endif  /* _SCLP_BITSET_H_ */
