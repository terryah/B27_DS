/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: sclp_base_algo.cc
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Aug-2004     Initial implementation.
//      jod     06-Jun-2008     Added optimized specializations for scl_copy,
//                              scl_copy_backward, scl_fill and scl_fill_n.
//
//==============================================================================


//
//  25.1.7 scl_mismatch
//
template <class InpIter1_, class InpIter2_>
scl_pair<InpIter1_, InpIter2_>
scl_mismatch(InpIter1_ first1, InpIter1_ last1, InpIter2_ first2)
{
    for (; first1 != last1; ++first1, ++first2)
    {
        if (!(*first1 == *first2))
            break;
    }

    return scl_pair<InpIter1_, InpIter2_>(first1, first2);
}

template <class InpIter1_, class InpIter2_, class Pred_>
scl_pair<InpIter1_, InpIter2_>
scl_mismatch(InpIter1_ first1, InpIter1_ last1, InpIter2_ first2, Pred_ pred)
{
    for (; first1 != last1; ++first1, ++first2)
    {
        if (!pred(*first1, *first2))
            break;
    }

    return scl_pair<InpIter1_, InpIter2_>(first1, first2);
}


//
//  25.1.8 scl_equal
//
template <class InpIter1_, class InpIter2_>
bool
scl_equal(InpIter1_ first1, InpIter1_ last1, InpIter2_ first2)
{
    for (; first1 != last1; ++first1, ++first2)
    {
        if (!(*first1 == *first2))
            return false;
    }

    return true;
}

template <class InpIter1_, class InpIter2_, class Pred_>
bool
scl_equal(InpIter1_ first1, InpIter1_ last1, InpIter2_ first2, Pred_ pred)
{
    for (; first1 != last1; ++first1, ++first2)
    {
        if (!pred(*first1, *first2))
            return false;
    }

    return true;
}


//
//  25.2.1 scl_copy / scl_copy_backward
//  Note: These template functions are specialized below.
//
template <class InpIter_, class OutIter_>
OutIter_
scl_copy(InpIter_ first, InpIter_ last, OutIter_ result)
{
    for (; first != last; ++first, ++result)
        *result = *first;

    return result;
}

template <class BidIter1_, class BidIter2_>
BidIter2_
scl_copy_backward(BidIter1_ first, BidIter1_ last, BidIter2_ result)
{
    while (first != last)
        *--result = *--last;

    return result;
}


//
//  25.2.2 scl_swap / scl_iter_swap
//  Note: scl_swap() is defined in <scl_config.h>
//
template <class FwdIter1_, class FwdIter2_, class Type_>
void
_scl_iter_swap(FwdIter1_ lhs, FwdIter2_ rhs, const Type_*)
{
    Type_ tmp(*lhs);
    *lhs = *rhs;
    *rhs = tmp;
}

template <class FwdIter1_, class FwdIter2_>
void
scl_iter_swap(FwdIter1_ lhs, FwdIter2_ rhs)
{
    _scl_iter_swap(lhs, rhs, _scl_value_type(lhs));
}


//
//  25.2.5 scl_fill / scl_fill_n
//  Note: These template functions are specialized below.
//
template <class FwdIter_, class Type_>
void
scl_fill(FwdIter_ first, FwdIter_ last, const Type_& value)
{
    for (; first != last; ++first)
        *first = value;
}

template <class OutIter_, class Size_, class Type_>
void
scl_fill_n(OutIter_ first, Size_ len, const Type_& value)
{
    for (; len > 0; ++first, --len)
        *first = value;
}


//
//  25.3.8 scl_lexicographical_compare
//
template <class InpIter1_, class InpIter2_>
bool
scl_lexicographical_compare(InpIter1_ first1, InpIter1_ last1,
    InpIter2_ first2, InpIter2_ last2)
{
    for (; first1 != last1 && first2 != last2; ++first1, ++first2)
    {
        if (*first1 < *first2)
            return true;

        if (*first2 < *first1)
            return false;
    }

    return (first1 == last1) && (first2 != last2);
}

template <class InpIter1_, class InpIter2_, class Comp_>
bool
scl_lexicographical_compare(InpIter1_ first1, InpIter1_ last1,
    InpIter2_ first2, InpIter2_ last2, Comp_ comp)
{
    for (; first1 != last1 && first2 != last2; ++first1, ++first2)
    {
        if (comp(*first1, *first2))
            return true;

        if (comp(*first2, *first1))
            return false;
    }

    return (first1 == last1) && (first2 != last2);
}


//
//  Define optimized specializations of scl_copy, scl_copy_backward, scl_fill and
//  scl_fill_n for scalar types.
//
#define SCL_SPECIALIZE_COPY(Type_)                                          \
SCL_SPECIALIZE_FUNC inline Type_*                                           \
scl_copy(const Type_* first, const Type_* last, Type_* result)              \
{                                                                           \
    return _scl_scalar_copy(result, first, last - first);                   \
}                                                                           \
SCL_SPECIALIZE_FUNC inline Type_*                                           \
scl_copy(      Type_* first,       Type_* last, Type_* result)              \
{                                                                           \
    return _scl_scalar_copy(result, first, last - first);                   \
}                                                                           \
SCL_SPECIALIZE_FUNC inline Type_*                                           \
scl_copy_backward(const Type_* first, const Type_* last, Type_* result)     \
{                                                                           \
    return _scl_scalar_copy_backward(result, first, last - first);          \
}                                                                           \
SCL_SPECIALIZE_FUNC inline Type_*                                           \
scl_copy_backward(      Type_* first,       Type_* last, Type_* result)     \
{                                                                           \
    return _scl_scalar_copy_backward(result, first, last - first);          \
}                                                                           \
SCL_SPECIALIZE_FUNC inline void                                             \
scl_fill(Type_* first, Type_* last, const Type_& value)                     \
{                                                                           \
    _scl_scalar_fill(first, value, last - first);                           \
}                                                                           \
SCL_SPECIALIZE_FUNC inline void                                             \
scl_fill_n(Type_* first, size_t len, const Type_& value)                    \
{                                                                           \
    _scl_scalar_fill(first, value, len);                                    \
}

SCL_SPECIALIZE_COPY(char)
SCL_SPECIALIZE_COPY(signed char)
SCL_SPECIALIZE_COPY(unsigned char)

#ifdef  SCL_HAS_DISTINCT_WCHAR
SCL_SPECIALIZE_COPY(wchar_t)
#endif

SCL_SPECIALIZE_COPY(short)
SCL_SPECIALIZE_COPY(unsigned short)

SCL_SPECIALIZE_COPY(int)
SCL_SPECIALIZE_COPY(unsigned int)

SCL_SPECIALIZE_COPY(long)
SCL_SPECIALIZE_COPY(unsigned long)

#ifdef  SCL_HAS_LLONG
SCL_SPECIALIZE_COPY(SCL_LLONG_TYPE)
SCL_SPECIALIZE_COPY(unsigned SCL_LLONG_TYPE)
#endif

#ifdef  SCL_HAS_DISTINCT_BOOL
SCL_SPECIALIZE_COPY(bool)
#endif

SCL_SPECIALIZE_COPY(float)
SCL_SPECIALIZE_COPY(double)
SCL_SPECIALIZE_COPY(long double)

#undef  SCL_SPECIALIZE_COPY
