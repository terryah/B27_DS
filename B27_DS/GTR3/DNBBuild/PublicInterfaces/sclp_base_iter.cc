/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: sclp_base_iter.cc
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Oct-2004     Initial implementation.
//
//==============================================================================


//
//  24.3.1 Iterator traits
//
#ifdef  SCL_HAS_PARTIAL_SPEC

template <class Type_>
struct scl_iterator_traits<Type_*>
{
    typedef scl_random_access_iterator_tag  iterator_category;
    typedef Type_                           value_type;
    typedef ptrdiff_t                       difference_type;
    typedef Type_*                          pointer;
    typedef Type_&                          reference;
};

template <class Type_>
struct scl_iterator_traits<const Type_*>
{
    typedef scl_random_access_iterator_tag  iterator_category;
    typedef Type_                           value_type;
    typedef ptrdiff_t                       difference_type;
    typedef const Type_*                    pointer;
    typedef const Type_&                    reference;
};

#define _SCL_INTEGRAL_TRAITS(Type_)                                         \
SCL_SPECIALIZE_CLASS                                                        \
struct scl_iterator_traits<Type_> {                                         \
    typedef _scl_integral_iterator_tag  iterator_category;                  \
};

template <class Iter_>
typename scl_iterator_traits<Iter_>::iterator_category
_scl_category(const Iter_&)
{
    typedef typename scl_iterator_traits<Iter_>::iterator_category  ICat_;
    return ICat_();
}

template <class Iter_>
typename scl_iterator_traits<Iter_>::value_type*
_scl_value_type(const Iter_&)
{
    typedef typename scl_iterator_traits<Iter_>::value_type         Type_;
    return (Type_*) NULL;
}

template <class Iter_>
typename scl_iterator_traits<Iter_>::difference_type*
_scl_distance_type(const Iter_&)
{
    typedef typename scl_iterator_traits<Iter_>::difference_type    Dist_;
    return (Dist_*) NULL;
}

#else   /* SCL_HAS_PARTIAL_SPEC */

#define _SCL_INTEGRAL_TRAITS(Type_)                                         \
inline _scl_integral_iterator_tag                                           \
_scl_category(Type_) {                                                      \
    return _scl_integral_iterator_tag();                                    \
}

template <class Type_>
scl_random_access_iterator_tag
_scl_category(const Type_*)
{
    return scl_random_access_iterator_tag();
}

template <class Type_>
Type_*
_scl_value_type(const Type_*)
{
    return (Type_*) NULL;
}

template <class Type_>
ptrdiff_t*
_scl_distance_type(const Type_*)
{
    return (ptrdiff_t*) NULL;
}

template <class ICat_, class Type_, class Dist_, class TPtr_, class TRef_>
ICat_
_scl_category(const scl_iterator<ICat_, Type_, Dist_, TPtr_, TRef_>&)
{
    return ICat_();
}

template <class ICat_, class Type_, class Dist_, class TPtr_, class TRef_>
Type_*
_scl_value_type(const scl_iterator<ICat_, Type_, Dist_, TPtr_, TRef_>&)
{
    return (Type_*) NULL;
}

template <class ICat_, class Type_, class Dist_, class TPtr_, class TRef_>
Dist_*
_scl_distance_type(const scl_iterator<ICat_, Type_, Dist_, TPtr_, TRef_>&)
{
    return (Dist_*) NULL;
}

#endif  /* SCL_HAS_PARTIAL_SPEC */

// Define traits of integral types.
_SCL_INTEGRAL_TRAITS(char)
_SCL_INTEGRAL_TRAITS(signed char)
_SCL_INTEGRAL_TRAITS(unsigned char)
_SCL_INTEGRAL_TRAITS(short)
_SCL_INTEGRAL_TRAITS(unsigned short)
_SCL_INTEGRAL_TRAITS(int)
_SCL_INTEGRAL_TRAITS(unsigned int)
_SCL_INTEGRAL_TRAITS(long)
_SCL_INTEGRAL_TRAITS(unsigned long)

#ifdef  SCL_HAS_LLONG
_SCL_INTEGRAL_TRAITS(SCL_LLONG_TYPE)
_SCL_INTEGRAL_TRAITS(SCL_ULLONG_TYPE)
#endif

#ifdef  SCL_HAS_DISTINCT_BOOL
_SCL_INTEGRAL_TRAITS(bool)
#endif

#ifdef  SCL_HAS_DISTINCT_WCHAR
_SCL_INTEGRAL_TRAITS(wchar_t)
#endif

#undef  _SCL_INTEGRAL_TRAITS


//
//  24.3.2 Basic iterator
//
#define SCL_ITERATOR_TYPEDEFS(Base_)                                        \
    typedef typename Base_::iterator_category   iterator_category;          \
    typedef typename Base_::value_type          value_type;                 \
    typedef typename Base_::difference_type     difference_type;            \
    typedef typename Base_::pointer             pointer;                    \
    typedef typename Base_::reference           reference


//
//  24.3.4 Iterator operations
//
template <class Iter_, class Dist_>
void
_scl_advance(Iter_& iter, Dist_ dist, const scl_input_iterator_tag&)
{
    // Advance an input or forward iterator.
    for (; dist > 0; --dist)
        ++iter;
}

template <class Iter_, class Dist_>
void
_scl_advance(Iter_& iter, Dist_ dist, const scl_bidirectional_iterator_tag&)
{
    // Advance a bidirectional iterator.
    if (dist >= 0)
    {
        for (; dist > 0; --dist)
            ++iter;
    }
    else
    {
        for (; dist < 0; ++dist)
            --iter;
    }
}

template <class Iter_, class Dist_>
void
_scl_advance(Iter_& iter, Dist_ dist, const scl_random_access_iterator_tag&)
{
    // Advance a random access iterator.
    iter += dist;
}

template <class Iter_, class Dist_>
void
scl_advance(Iter_& iter, Dist_ dist)
{
    _scl_advance(iter, dist, _scl_category(iter));
}

template <class Iter_, class Dist_>
void
_scl_distance(Iter_ first, Iter_ last, Dist_ &dist, const scl_input_iterator_tag&)
{
    // Compute distance between two input, forward, or bidirectional iterators.
    for (; first != last; ++first)
        ++dist;
}

template <class Iter_, class Dist_>
void
_scl_distance(Iter_ first, Iter_ last, Dist_ &dist, const scl_random_access_iterator_tag&)
{
    // Compute distance between two random access iterators.
    dist += (last - first);
}

template <class Iter_>
_SCL_DIFFERENCE_TYPE(Iter_)
scl_distance(Iter_ first, Iter_ last)
{
    _SCL_DIFFERENCE_TYPE(Iter_) dist = 0;
    _scl_distance(first, last, dist, _scl_category(first));
    return dist;
}
