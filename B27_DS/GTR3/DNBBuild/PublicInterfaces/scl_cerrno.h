/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: scl_cerrno.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Aug-2004     Initial implementation.
//
//  NOTICE:
//      This header implements functionality in the Standard C++ Library.
//      It may be included directly in application code.
//
//==============================================================================
#ifndef _SCL_CERRNO_H_
#define _SCL_CERRNO_H_


#ifndef  _SCL_CONFIG_H_
#include <scl_config.h>
#endif

#include <errno.h>


#endif  /* _SCL_CERRNO_H_ */
