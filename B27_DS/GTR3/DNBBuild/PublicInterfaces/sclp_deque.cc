/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: sclp_deque.cc
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Aug-2004     Initial implementation.
//
//==============================================================================
#define SCL_DEQUE_TEMPL         template <class Type_, class Alloc_>
#define SCL_DEQUE_CLASS         scl_deque<Type_, Alloc_>


//
//  23.2.4.1 construct/copy/destroy
//
SCL_DEQUE_TEMPL
SCL_DEQUE_CLASS::scl_deque() :
    impl_()
{
    // Nothing
}

SCL_DEQUE_TEMPL
SCL_DEQUE_CLASS::scl_deque(const Alloc_& alloc) :
    impl_(alloc)
{
    // Nothing
}

SCL_DEQUE_TEMPL
SCL_DEQUE_CLASS::scl_deque(size_type len) :
    impl_(len)
{
    // Nothing
}

SCL_DEQUE_TEMPL
SCL_DEQUE_CLASS::scl_deque(size_type len, const Type_& value) :
    impl_(len, value)
{
    // Nothing
}

SCL_DEQUE_TEMPL
SCL_DEQUE_CLASS::scl_deque(size_type len, const Type_& value, const Alloc_& alloc) :
    impl_(len, value, alloc)
{
    // Nothing
}

#ifdef  SCL_HAS_MEMBER_TEMPLATES
SCL_DEQUE_TEMPL
template <class InputIterator_>
SCL_DEQUE_CLASS::scl_deque(InputIterator_ first, InputIterator_ last) :
#else
SCL_DEQUE_TEMPL
SCL_DEQUE_CLASS::scl_deque(const_iterator first, const_iterator last) :
#endif
    impl_(first, last)
{
    // Nothing
}

#ifdef  SCL_HAS_MEMBER_TEMPLATES
SCL_DEQUE_TEMPL
template <class InputIterator_>
SCL_DEQUE_CLASS::scl_deque(InputIterator_ first, InputIterator_ last, const Alloc_& alloc) :
#else
SCL_DEQUE_TEMPL
SCL_DEQUE_CLASS::scl_deque(const_iterator first, const_iterator last, const Alloc_& alloc) :
#endif
    impl_(first, last, alloc)
{
    // Nothing
}

SCL_DEQUE_TEMPL
SCL_DEQUE_CLASS::scl_deque(const Self_& other) :
    impl_(other.impl_)
{
    // Nothing
}

SCL_DEQUE_TEMPL
SCL_DEQUE_CLASS::~scl_deque()
{
    // Nothing
}

SCL_DEQUE_TEMPL
SCL_DEQUE_CLASS &
SCL_DEQUE_CLASS::operator=(const Self_& other)
{
    impl_ = other.impl_;
    return *this;
}

#ifdef  SCL_HAS_MEMBER_TEMPLATES
SCL_DEQUE_TEMPL
template <class InputIterator_>
void
SCL_DEQUE_CLASS::assign(InputIterator_ first, InputIterator_ last)
#else
SCL_DEQUE_TEMPL
void
SCL_DEQUE_CLASS::assign(const_iterator first, const_iterator last)
#endif
{
    impl_.assign(first, last);
}

SCL_DEQUE_TEMPL
void
SCL_DEQUE_CLASS::assign(size_type len, const Type_& value)
{
    impl_.assign(len, value);
}

SCL_DEQUE_TEMPL
typename SCL_DEQUE_CLASS::allocator_type
SCL_DEQUE_CLASS::get_allocator() const
{
    return impl_.get_allocator();
}


//
//  23.2.4.2 capacity
//
SCL_DEQUE_TEMPL
void
SCL_DEQUE_CLASS::resize(size_type len, const Type_& value)
{
    impl_.resize(len, value);
}


//
//  23.2.4.3 modifiers
//
SCL_DEQUE_TEMPL
void
SCL_DEQUE_CLASS::push_front(const Type_& value)
{
    impl_.insert(impl_.begin(), value);
}

SCL_DEQUE_TEMPL
void
SCL_DEQUE_CLASS::push_back(const Type_& value)
{
    impl_.push_back(value);
}

SCL_DEQUE_TEMPL
void
SCL_DEQUE_CLASS::pop_front()
{
    impl_.erase(impl_.begin());
}

SCL_DEQUE_TEMPL
void
SCL_DEQUE_CLASS::pop_back()
{
    impl_.pop_back();
}

SCL_DEQUE_TEMPL
typename SCL_DEQUE_CLASS::iterator
SCL_DEQUE_CLASS::insert(iterator iter, const Type_& value)
{
    return impl_.insert(iter, value);
}

SCL_DEQUE_TEMPL
void
SCL_DEQUE_CLASS::insert(iterator iter, size_type len, const Type_& value)
{
    impl_.insert(iter, len, value);
}

#ifdef  SCL_HAS_MEMBER_TEMPLATES
SCL_DEQUE_TEMPL
template <class InputIterator_>
void
SCL_DEQUE_CLASS::insert(iterator iter, InputIterator_ first, InputIterator_ last)
#else
SCL_DEQUE_TEMPL
void
SCL_DEQUE_CLASS::insert(iterator iter, const_iterator first, const_iterator last)
#endif
{
    impl_.insert(iter, first, last);
}

SCL_DEQUE_TEMPL
typename SCL_DEQUE_CLASS::iterator
SCL_DEQUE_CLASS::erase(iterator iter)
{
    return impl_.erase(iter);
}

SCL_DEQUE_TEMPL
typename SCL_DEQUE_CLASS::iterator
SCL_DEQUE_CLASS::erase(iterator first, iterator last)
{
    return impl_.erase(first, last);
}


#undef  SCL_DEQUE_TEMPL
#undef  SCL_DEQUE_CLASS
