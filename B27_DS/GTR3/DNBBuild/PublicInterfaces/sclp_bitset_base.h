/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: sclp_bitset_base.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Dec-2004     Initial implementation.
//
//  NOTICE:
//      This header is used internally by the Standard C++ Library.
//      It should not be included directly in application code.
//
//==============================================================================
#ifndef _SCLP_BITSET_BASE_H_
#define _SCLP_BITSET_BASE_H_


//
//  Include section
//
#ifndef  _SCLP_STRING_H_
#include <sclp_string.h>                            // for scl_string
#endif


//
//  Private class _scl_bitset_reference
//
class SCL_STD_EXPORT _scl_bitset_reference
{
public:
    typedef _scl_bitset_reference   Self_;

    ~_scl_bitset_reference();
    Self_&          operator=(bool value);          // for b[i] = value
    Self_&          operator=(const Self_& other);  // for b[i] = b[j]
    bool            operator~() const;              // flips the bit
    operator        bool() const;                   // for value = b[i]
    Self_&          flip();                         // for b[i].flip()

private:
    typedef unsigned long   WordType;               // Must match _scl_bitset_base

    _scl_bitset_reference();                        // Not implemented!
    _scl_bitset_reference(WordType* data, WordType mask);
    void            bit_set(bool value);
    bool            bit_get() const;

    // Data members.
    WordType*       data_;
    WordType        mask_;

    friend class _scl_bitset_base;
};


//
//  Private class _scl_bitset_base
//
class SCL_STD_EXPORT _scl_bitset_base
{
public:
    typedef _scl_bitset_reference   reference;

    bool            operator[](size_t pos) const;
    reference       operator[](size_t pos);

    unsigned long   to_ulong() const;
    scl_string      to_string() const;
    size_t          count() const;
    size_t          size() const;
    bool            test(size_t pos) const;
    bool            any() const;
    bool            none() const;

protected:
    typedef unsigned long   WordType;   // Must match _scl_bitset_reference

    // 23.3.5.1 construct/copy/destroy
    _scl_bitset_base();
    void            bit_init(size_t nbits, size_t nwords, WordType* data);
    void            bit_assign(unsigned long value);
    void            bit_assign(const scl_string& cstr, size_t cpos, size_t clen);
    void            bit_assign(const _scl_bitset_base& rhs);

    // 23.3.5.2 bitset operations
    void            bit_and(const _scl_bitset_base& rhs);
    void            bit_or (const _scl_bitset_base& rhs);
    void            bit_xor(const _scl_bitset_base& rhs);
    void            bit_shl(size_t count);          // shift left
    void            bit_shr(size_t count);          // shift right
    void            bit_set();
    void            bit_set(size_t pos, bool value);
    bool            bit_get(size_t pos) const;
    void            bit_reset();
    void            bit_reset(size_t pos);
    void            bit_flip();
    void            bit_flip(size_t pos);
    bool            bit_equal(const _scl_bitset_base& rhs) const;

private:
    // Disable copy construction and assignment.  Derived classes must provide
    // these services.
    _scl_bitset_base(const _scl_bitset_base&);
    void   operator=(const _scl_bitset_base&);

    // Private utilities.
    void            normalize();
    static size_t   CountBits(WordType value);
    static size_t   Min(size_t lhs, size_t rhs);

    // Data members.
    size_t          nbits_;     // Number of bits in bitset
    size_t          nwords_;    // Number of words in bitset
    WordType        umask_;     // Mask to clear unused bits
    WordType*       data_;      // Pointer to word array
};


#endif  /* _SCLP_BITSET_BASE_H_ */
