/** @CAA2Required */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2000
//
//  DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE.  IT IS REQUIRED
//  TO BUILD CAA APPLICATIONS, BUT MAY DISAPEAR AT ANY TIME.
//==============================================================================
#ifndef _DNBBUILD_H_
#define _DNBBUILD_H_


#ifdef  _WINDOWS_SOURCE

#ifdef  __DNBBuild
#define ExportedByDNBBuild      __declspec(dllexport)
#else
#define ExportedByDNBBuild      __declspec(dllimport)
#endif

#else

#define ExportedByDNBBuild      /* nothing */

#endif  /* _WINDOWS_SOURCE */


#endif  /* _DNBBUILD_H_ */
