/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: scl_iostream.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Oct-2004     Initial implementation.
//
//  NOTICE:
//      This header implements functionality in the Standard C++ Library.
//      It may be included directly in application code.
//
//==============================================================================
#ifndef _SCL_IOSTREAM_H_
#define _SCL_IOSTREAM_H_


#ifndef  _SCL_CONFIG_H_
#include <scl_config.h>
#endif

#ifndef  _SCL_IOS_H_
#include <scl_ios.h>
#endif


#ifdef  SCL_USE_STD_IOSTREAM
#include <iostream>

#ifdef  SCL_HAS_NAMESPACES
// from <ostream>
using SCL_VENDOR_STD::ostream;
using SCL_VENDOR_STD::endl;
using SCL_VENDOR_STD::ends;
using SCL_VENDOR_STD::flush;

// from <istream>
using SCL_VENDOR_STD::istream;
using SCL_VENDOR_STD::iostream;
using SCL_VENDOR_STD::ws;

// from <iostream>
using SCL_VENDOR_STD::cin;
using SCL_VENDOR_STD::cout;
using SCL_VENDOR_STD::cerr;
using SCL_VENDOR_STD::clog;
#endif  /* SCL_HAS_NAMESPACES */

#else
#include <iostream.h>

#endif  /* SCL_USE_STD_IOSTREAM */


#if     defined(SCL_USE_STD_IOSTREAM) || !defined(SCL_HAS_IOSTREAM_WCHAR)

#ifdef  SCL_HAS_DISTINCT_WCHAR
SCL_STD_EXPORT ostream&
operator<<(ostream& ostr, wchar_t item);
#endif

SCL_STD_EXPORT ostream&
operator<<(ostream& ostr, const wchar_t* item);

#endif  /* SCL_USE_STD_IOSTREAM || !SCL_HAS_IOSTREAM_WCHAR */


#endif  /* _SCL_IOSTREAM_H_ */
