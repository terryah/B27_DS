/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: scl_strstream.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Oct-2004     Initial implementation.
//
//  NOTICE:
//      This header implements functionality in the Standard C++ Library.
//      It may be included directly in application code.
//
//==============================================================================
#ifndef _SCL_STRSTREAM_H_
#define _SCL_STRSTREAM_H_


//
//  Include section
//
#ifndef  _SCL_CONFIG_H_
#include <scl_config.h>
#endif

#ifndef  _SCL_IOSTREAM_H_
#include <scl_iostream.h>
#endif


#ifdef  SCL_USE_STD_IOSTREAM

#ifdef  _WINDOWS_SOURCE
#include <strstream>
using SCL_VENDOR_STD::strstreambuf;
using SCL_VENDOR_STD::istrstream;
using SCL_VENDOR_STD::ostrstream;
using SCL_VENDOR_STD::strstream;
#else
#error  "Unsupported platform."
#endif  /* _WINDOWS_SOURCE */

#else

#ifdef  _WINDOWS_SOURCE
#include <strstrea.h>
#else
#include <strstream.h>
#endif  /* _WINDOWS_SOURCE */

#endif  /* SCL_USE_STD_IOSTREAM */


#endif  /* _SCL_STRSTREAM_H_ */
