/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: scl_stack.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Aug-2004     Initial implementation.
//
//  NOTICE:
//      This header implements functionality in the Standard C++ Library.
//      It may be included directly in application code.
//
//==============================================================================
#ifndef _SCL_STACK_H_
#define _SCL_STACK_H_


#ifndef  _SCL_CONFIG_H_
#include <scl_config.h>
#endif


#ifdef  SCL_USE_VENDOR_STD
#include <stack>

#define scl_stack                       SCL_VENDOR_STD::stack

#else
#include <sclp_stack.h>

#endif  /* SCL_USE_VENDOR_STD */


#endif  /* _SCL_STACK_H_ */
