/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: sclp_allocator.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Aug-2004     Initial implementation.
//
//  NOTICE:
//      This header is used internally by the Standard C++ Library.
//      It should not be included directly in application code.
//
//==============================================================================
#ifndef _SCLP_ALLOCATOR_H_
#define _SCLP_ALLOCATOR_H_


//
//  Include section
//
#ifndef  _SCL_CLIMITS_H_
#include <scl_climits.h>                // for SCL_SIZE_MAX
#endif

#ifndef  _SCLP_CONSTRUCT_H_
#include <sclp_construct.h>             // for _scl_construct, _scl_destroy
#endif


//
//  20.4.1 Template class scl_allocator
//
template <class Type_>
class scl_allocator;


SCL_SPECIALIZE_CLASS
class scl_allocator<void>
{
public:
    typedef size_t                      size_type;
    typedef ptrdiff_t                   difference_type;
    typedef void*                       pointer;
    typedef const void*                 const_pointer;
    typedef void                        value_type;

#ifdef  SCL_USE_ALLOCATOR_REBIND
    template <class Other_>
    struct rebind
    {
        typedef scl_allocator<Other_>   other;
    };
#endif
};


template <class Type_>
class scl_allocator
{
public:
    typedef scl_allocator<Type_>        Self_;
    typedef size_t                      size_type;
    typedef ptrdiff_t                   difference_type;
    typedef Type_*                      pointer;
    typedef const Type_*                const_pointer;
    typedef Type_&                      reference;
    typedef const Type_&                const_reference;
    typedef Type_                       value_type;

#ifdef  SCL_USE_ALLOCATOR_REBIND
    template <class Other_>
    struct rebind
    {
        typedef scl_allocator<Other_>   other;
    };
#else
    // The following methods are not part of the C++ standard.  They are used to
    // allocate memory for objects other than Type_, such as nodes in a linked list.
    void* raw_allocate(size_type count, size_type size, const void* hint = NULL)
    {
        return scl_allocate(count * size);
    }

    void raw_deallocate(void* ptr, size_type count, size_type size)
    {
        scl_deallocate(ptr, count * size);
    }
#endif

    scl_allocator()
        SCL_NOTHROW
    {
        // Nothing
    }

    scl_allocator(const Self_&)
        SCL_NOTHROW
    {
        // Nothing
    }

#ifdef  SCL_HAS_MEMBER_TEMPLATES
    template <class Other_>
    scl_allocator(const scl_allocator<Other_>&)
        SCL_NOTHROW
    {
        // Nothing
    }
#endif

    ~scl_allocator()
        SCL_NOTHROW
    {
        // Nothing
    }

    Self_&  operator=(const Self_&)
        SCL_NOTHROW
    {
        return *this;
    }

    pointer address(reference item) const
    {
        return &item;
    }

    const_pointer address(const_reference item) const
    {
        return &item;
    }

    pointer allocate(size_type count, const void* hint = NULL)
    {
        return (pointer) scl_allocate(count * sizeof(Type_));
    }

    void deallocate(pointer ptr, size_type count)
    {
        scl_deallocate(ptr, count * sizeof(Type_));
    }

    size_type max_size() const
        SCL_NOTHROW
    {
        // Return the largest N for which allocate(N) might succeed; see 20.4.1.1, p11.
        return (SCL_SIZE_MAX / sizeof(Type_));
    }

    void construct(pointer ptr, const_reference val)
    {
        _scl_construct(ptr, val);
    }

    void destroy(pointer ptr)
    {
        _scl_destroy(ptr);
    }
};


template <class Type1_, class Type2_>
bool
operator==(const scl_allocator<Type1_>&, const scl_allocator<Type2_>&)
    SCL_NOTHROW
{
    return true;
}

template <class Type1_, class Type2_>
bool
operator!=(const scl_allocator<Type1_>&, const scl_allocator<Type2_>&)
    SCL_NOTHROW
{
    return false;
}


#endif  /* _SCLP_ALLOCATOR_H_ */
