//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 */

//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     jad         08/18/97    Initial implementation.
//*     jad         07/13/99    Added conversion routines for vectors and lists.
//*     bkh         06/25/03    Added documentation.
//* 
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_STANDARDLIB_H_
#define _DNB_STANDARDLIB_H_


#include <DNBSystemBase.h>
#include <scl_memory.h>
#include <scl_string.h>
#include <scl_vector.h>
#include <scl_list.h>
#include <DNBSystemDefs.h>


#include <DNBBuild.h>




/**
  * Calls the destructor for an object.
  * 
  * <br><B>Template Parameter(s)</B><br>
  * @param T
  * The static type of the object to be destroyed.
  * @param ptr
  * A pointer to the object to be destroyed.
  * 
  * @return
  * Nothing
  * 
  * <br><B>Description</B><br>
  * This function calls the destructor for the object referenced by <tt>ptr</tt>.
  * It is used by several of the container classes in the toolkit.
  * @exception Unspecified
  * Any exception thrown by the destructor for <tt>T</tt>.
  * 
  * 
  */
template <class T>
void
DNBDestroy( T *ptr )
    DNB_THROW_SPEC_ANY;




/**
  * Deletes a sequence of heap-based objects.
  * 
  * <br><B>Template Parameter(s)</B><br>
  * @exception FwdIt
  * The forward iterator type for the container to be processed.  This
  * type must support the standard operations required by the forward
  * iterator model.
  * @param first
  * An iterator positioned at the first element in the desired range.
  * @param last
  * An iterator positioned immediately after the last element in the
  * desired range.
  * 
  * @return
  * Nothing
  * 
  * <br><B>Description</B><br>
  * This function calls @href DNB_DELETE on every item in the range (first,
  * last).  It is typically used to delete a container of heap-based
  * objects; e.g., vector<tt>int *</tt>}.  These objects are assumed to be
  * allocated using @href DNB_NEW.
  * @exception Unspecified
  * Any exception thrown by the destructor for the objects stored in the
  * container.
  * 
  * <br><B>Example</B><br>
  * <pre>
  * #include <DNBSystemBase.h>
  * #include DNB_VECTOR
  * #include <DNBSystemDefs.h>
  * 
  * #include <DNBStandardLib.h>
  * 
  * int
  * main()
  * {
  * scl_vector<int *, DNB_ALLOCATOR(int *) >    vec;
  * 
  * vec.push_back( DNB_NEW int(2) );
  * vec.push_back( DNB_NEW int(1) );
  * vec.push_back( DNB_NEW int(4) );
  * 
  * for ( size_t idx = 0; idx < vec.size(); idx++ )
  * cout << "vec[" << idx << "] = " << *(vec[idx]) << endl;
  * 
  * DNBRelease( vec.begin(), vec.end() );
  * 
  * return (0);
  * }
  * 
  * OUTPUT:
  * vec[0] = 2
  * vec[1] = 1
  * vec[2] = 4
  * </pre>
  *
  * <br><B>Tips</B><br>
  * If the container's size is specified at the time of construction, be
  * sure to initialize every element to NULL.  In this way, the container
  * can be safely deleted using DNBRelease.
  * 
  * <br><B>Warnings</B><br>
  * This function should only be called if every element in the specified
  * range is a pointer to a heap-based object.  Further, if more than one
  * element points to the same object, an error will most probably occur.
  * 
  * 
  */
template <class FwdIt>
void
DNBRelease( FwdIt first, FwdIt last )
    DNB_THROW_SPEC_ANY;




/**
  * Deletes a container of heap-based objects.
  * 
  * <br><B>Template Parameter(s)</B><br>
  * @exception Container
  * The base type of the container to be processed.  This class must
  * provide the following public member functions:
  * <UL>
  * <LI> iterator begin() }
  * <LI> iterator end() }
  * </UL>
  * @param cont
  * The container to be processed.
  * 
  * @return
  * Nothing
  * 
  * <br><B>Description</B><br>
  * This function calls @href DNB_DELETE on every item in the container
  * <tt>cont</tt>.  It is typically used to delete a container of heap-based
  * objects; e.g., vector<tt>int *</tt>}.  These objects are assumed to be
  * allocated using @href DNB_NEW.
  * @exception Unspecified
  * Any exception thrown by the destructor for the objects stored in the
  * container.
  * 
  * <br><B>Example</B><br>
  * <pre>
  * #include DNBSystemBase.h
  * #include DNB_VECTOR
  * #include DNBSystemDefs.h
  * 
  * #include DNBStandardLib.h
  * 
  * int
  * main()
  * {
  * scl_vector<int *, DNB_ALLOCATOR(int *) >    vec;
  * 
  * vec.push_back( DNB_NEW int(2) );
  * vec.push_back( DNB_NEW int(1) );
  * vec.push_back( DNB_NEW int(4) );
  * 
  * for ( size_t idx = 0; idx < vec.size(); idx++ )
  * cout << "vec[" << idx << "] = " << *(vec[idx]) << endl;
  * 
  * DNBRelease( vec );
  * 
  * return (0);
  * }
  * 
  * OUTPUT:
  * vec[0] = 2
  * vec[1] = 1
  * vec[2] = 4
  * </pre>
  *
  * <br><B>Tips</B><br>
  * If the container's size is specified at the time of construction, be
  * sure to initialize every element to NULL.  In this way, the container
  * can be safely deleted using DNBRelease.
  * 
  * <br><B>Warnings</B><br>
  * This function should only be called if every element in the container
  * is a pointer to a heap-based object.  Further, if more than one element
  * points to the same object, an error will most probably occur.
  * 
  * 
  */
template <class Container>
void
DNBRelease( Container &cont )
    DNB_THROW_SPEC_ANY;

/**
 * Converts a wstring to a string.
 */
ExportedByDNBBuild  void
DNBAssign( scl_wstring &dst, const scl_string &src )
    DNB_THROW_SPEC((scl_bad_alloc));

/**
 * Converts contents of a list to a vector.
 */
template <class Item, class Allocator>
void
DNBAssign( scl_vector<Item, Allocator> &dst, const scl_list<Item, Allocator> &src )
    DNB_THROW_SPEC((scl_bad_alloc));

/**
 * Converts contents of a vector to a list.
 */
template <class Item, class Allocator>
void
DNBAssign( scl_list<Item, Allocator> &dst, const scl_vector<Item, Allocator> &src )
    DNB_THROW_SPEC((scl_bad_alloc));


//
//  Include the public definition file.
//
#include "DNBStandardLib.cc"


#endif  /* _DNB_STANDARDLIB_H_ */
