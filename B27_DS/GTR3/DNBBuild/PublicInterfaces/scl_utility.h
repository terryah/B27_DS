/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: scl_utility.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Aug-2004     Initial implementation.
//
//  NOTICE:
//      This header implements functionality in the Standard C++ Library.
//      It may be included directly in application code.
//
//==============================================================================
#ifndef _SCL_UTILITY_H_
#define _SCL_UTILITY_H_


#ifndef  _SCL_CONFIG_H_
#include <scl_config.h>
#endif


#ifdef  SCL_USE_VENDOR_STD
#include <utility>

#define scl_pair                        SCL_VENDOR_STD::pair
#define scl_make_pair                   SCL_VENDOR_STD::make_pair

#else
#include <sclp_utility.h>

#endif  /* SCL_USE_VENDOR_STD */


#endif  /* _SCL_UTILITY_H_ */
