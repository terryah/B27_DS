/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: sclp_heap_algo.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Aug-2004     Initial implementation.
//
//  NOTICE:
//      This header is used internally by the Standard C++ Library.
//      It should not be included directly in application code.
//
//==============================================================================
#ifndef _SCLP_HEAP_ALGO_H_
#define _SCLP_HEAP_ALGO_H_


//
//  25.3.6.1 scl_push_heap
//
template <class RanIter_>
void
scl_push_heap(RanIter_ first, RanIter_ last);

template <class RanIter_, class Comp_>
void
scl_push_heap(RanIter_ first, RanIter_ last, Comp_ comp);

//
//  25.3.6.2 scl_pop_heap
//
template <class RanIter_>
void
scl_pop_heap(RanIter_ first, RanIter_ last);

template <class RanIter_, class Comp_>
void
scl_pop_heap(RanIter_ first, RanIter_ last, Comp_ comp);

//
//  25.3.6.3 scl_make_heap
//
template <class RanIter_>
void
scl_make_heap(RanIter_ first, RanIter_ last);

template <class RanIter_, class Comp_>
void
scl_make_heap(RanIter_ first, RanIter_ last, Comp_ comp);

//
//  25.3.6.4 scl_sort_heap
//
template <class RanIter_>
void
scl_sort_heap(RanIter_ first, RanIter_ last);

template <class RanIter_, class Comp_>
void
scl_sort_heap(RanIter_ first, RanIter_ last, Comp_ comp);


#include <sclp_heap_algo.cc>


#endif  /* _SCLP_HEAP_ALGO_H_ */
