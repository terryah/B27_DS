/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: sclp_deque.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Oct-2004     Initial implementation.
//
//  NOTICE:
//      This header is used internally by the Standard C++ Library.
//      It should not be included directly in application code.
//
//==============================================================================
#ifndef _SCLP_DEQUE_H_
#define _SCLP_DEQUE_H_


//
//  Include section
//
#ifndef  _SCLP_VECTOR_H_
#include <sclp_vector.h>
#endif


//
//  Template class scl_deque
//
#ifdef  SCL_HAS_DEFAULT_TMPL_ARGS
template <class Type_, class Alloc_ = scl_allocator<Type_> >
#else
template <class Type_, class Alloc_>
#endif
class scl_deque
{
public:
    // Private types
    typedef scl_deque<Type_, Alloc_>                Self_;
    typedef scl_vector<Type_, Alloc_>               Impl_;

    // Public types
    typedef Type_                                   value_type;
    typedef Alloc_                                  allocator_type;
    typedef typename Impl_::pointer                 pointer;
    typedef typename Impl_::const_pointer           const_pointer;
    typedef typename Impl_::reference               reference;
    typedef typename Impl_::const_reference         const_reference;
    typedef typename Impl_::size_type               size_type;
    typedef typename Impl_::difference_type         difference_type;

    typedef typename Impl_::iterator                iterator;
    typedef typename Impl_::const_iterator          const_iterator;
    typedef typename Impl_::reverse_iterator        reverse_iterator;
    typedef typename Impl_::const_reverse_iterator  const_reverse_iterator;

    // 23.2.4.1 construct/copy/destroy
    scl_deque();
    explicit scl_deque(const Alloc_& alloc);

    explicit scl_deque(size_type len);
    scl_deque(size_type len, const Type_& value);
    scl_deque(size_type len, const Type_& value, const Alloc_& alloc);

#ifdef  SCL_HAS_MEMBER_TEMPLATES
    template <class InputIterator_>
    scl_deque(InputIterator_ first, InputIterator_ last);

    template <class InputIterator_>
    scl_deque(InputIterator_ first, InputIterator_ last, const Alloc_& alloc);
#else
    scl_deque(const_iterator first, const_iterator last);
    scl_deque(const_iterator first, const_iterator last, const Alloc_& alloc);
#endif

    scl_deque(const Self_& other);
    ~scl_deque();

    Self_&          operator=(const Self_& other);

#ifdef  SCL_HAS_MEMBER_TEMPLATES
    template <class InputIterator_>
    void            assign(InputIterator_ first, InputIterator_ last);
#else
    void            assign(const_iterator first, const_iterator last);
#endif

    void            assign(size_type len, const Type_& value);
    allocator_type  get_allocator() const;

    // iterators
    iterator        begin()                         { return impl_.begin();     }
    const_iterator  begin() const                   { return impl_.begin();     }
    iterator        end()                           { return impl_.end();       }
    const_iterator  end() const                     { return impl_.end();       }

    reverse_iterator        rbegin()                { return impl_.rbegin();    }
    const_reverse_iterator  rbegin() const          { return impl_.rbegin();    }
    reverse_iterator        rend()                  { return impl_.rend();      }
    const_reverse_iterator  rend() const            { return impl_.rend();      }

    // 23.2.4.2 capacity
    bool            empty() const                   { return impl_.empty();     }
    size_type       size() const                    { return impl_.size();      }
    size_type       capacity() const                { return impl_.capacity();  }
    size_type       max_size() const                { return impl_.max_size();  }
    void            resize(size_type len)           { impl_.resize(len);        }
    void            resize(size_type len, const Type_& value);
    void            reserve(size_type len)          { impl_.reserve(len);       }

    // element access
    reference       operator[](size_type idx)       { return impl_[idx];        }
    const_reference operator[](size_type idx) const { return impl_[idx];        }
    reference       at(size_type idx)               { return impl_.at(idx);     }
    const_reference at(size_type idx) const         { return impl_.at(idx);     }
    reference       front()                         { return impl_.front();     }
    const_reference front() const                   { return impl_.front();     }
    reference       back()                          { return impl_.back();      }
    const_reference back() const                    { return impl_.back();      }

    // 23.2.4.3 modifiers
    void            push_front(const Type_& value);
    void            push_back (const Type_& value);
    void            pop_front();
    void            pop_back ();
    iterator        insert(iterator iter, const Type_& value);
    void            insert(iterator iter, size_type len, const Type_& value);

#ifdef  SCL_HAS_MEMBER_TEMPLATES
    template <class InputIterator_>
    void            insert(iterator iter, InputIterator_ first, InputIterator_ last);
#else
    void            insert(iterator iter, const_iterator first, const_iterator last);
#endif

    iterator        erase(iterator iter);
    iterator        erase(iterator first, iterator last);
    void            clear()                         { impl_.clear();            }
    void            swap(Self_& other)              { impl_.swap(other.impl_);  }

    // Test class invariants (nonstandard method).
    bool            _validate() const               { return impl_._validate(); }

private:
    Impl_           impl_;      // Internal container
};


template <class Type_, class Alloc_>
bool
operator==(const scl_deque<Type_, Alloc_>& lhs,
           const scl_deque<Type_, Alloc_>& rhs)
{
    return (lhs.size() == rhs.size()) && scl_equal(lhs.begin(), lhs.end(), rhs.begin());
}

template <class Type_, class Alloc_>
bool
operator< (const scl_deque<Type_, Alloc_>& lhs,
           const scl_deque<Type_, Alloc_>& rhs)
{
    return scl_lexicographical_compare(lhs.begin(), lhs.end(), rhs.begin(), rhs.end());
}

template <class Type_, class Alloc_>
bool
operator!=(const scl_deque<Type_, Alloc_>& lhs,
           const scl_deque<Type_, Alloc_>& rhs)
{
    return !(lhs == rhs);
}

template <class Type_, class Alloc_>
bool
operator>=(const scl_deque<Type_, Alloc_>& lhs,
           const scl_deque<Type_, Alloc_>& rhs)
{
    return !(lhs < rhs);
}

template <class Type_, class Alloc_>
bool
operator> (const scl_deque<Type_, Alloc_>& lhs,
           const scl_deque<Type_, Alloc_>& rhs)
{
    return (rhs < lhs);
}

template <class Type_, class Alloc_>
bool
operator<=(const scl_deque<Type_, Alloc_>& lhs,
           const scl_deque<Type_, Alloc_>& rhs)
{
    return !(rhs < lhs);
}

#ifdef  SCL_HAS_PARTIAL_SPEC_OVERLOAD
template <class Type_, class Alloc_>
void
scl_swap(scl_deque<Type_, Alloc_>& lhs,
         scl_deque<Type_, Alloc_>& rhs)
{
    lhs.swap(rhs);
}
#endif


#include <sclp_deque.cc>


#endif  /* _SCLP_DEQUE_H_ */
