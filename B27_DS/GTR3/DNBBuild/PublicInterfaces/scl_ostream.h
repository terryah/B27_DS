/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: scl_ostream.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Oct-2004     Initial implementation.
//
//  NOTICE:
//      This header implements functionality in the Standard C++ Library.
//      It may be included directly in application code.
//
//==============================================================================
#ifndef _SCL_OSTREAM_H_
#define _SCL_OSTREAM_H_


#ifndef  _SCL_IOSTREAM_H_
#include <scl_iostream.h>
#endif


#endif  /* _SCL_OSTREAM_H_ */
