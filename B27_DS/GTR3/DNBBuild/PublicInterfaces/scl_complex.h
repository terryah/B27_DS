/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: scl_complex.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Aug-2004     Initial implementation.
//
//  NOTICE:
//      This header implements functionality in the Standard C++ Library.
//      It may be included directly in application code.
//
//==============================================================================
#ifndef _SCL_COMPLEX_H_
#define _SCL_COMPLEX_H_


#ifndef  _SCL_CONFIG_H_
#include <scl_config.h>
#endif


#ifdef  SCL_USE_VENDOR_STD
#include <complex>

#define scl_complex                     SCL_VENDOR_STD::complex

#ifdef  SCL_HAS_NAMESPACES
using SCL_VENDOR_STD::real;
using SCL_VENDOR_STD::imag;
using SCL_VENDOR_STD::abs;
using SCL_VENDOR_STD::arg;
using SCL_VENDOR_STD::norm;
using SCL_VENDOR_STD::conj;
using SCL_VENDOR_STD::polar;

using SCL_VENDOR_STD::cos;
using SCL_VENDOR_STD::cosh;
using SCL_VENDOR_STD::exp;
using SCL_VENDOR_STD::log;
using SCL_VENDOR_STD::log10;
using SCL_VENDOR_STD::pow;
using SCL_VENDOR_STD::sin;
using SCL_VENDOR_STD::sinh;
using SCL_VENDOR_STD::sqrt;
using SCL_VENDOR_STD::tan;
using SCL_VENDOR_STD::tanh;
#endif  /* SCL_HAS_NAMESPACES */

#else
#include <sclp_complex.h>

#endif  /* SCL_USE_VENDOR_STD */


#endif  /* _SCL_COMPLEX_H_ */
