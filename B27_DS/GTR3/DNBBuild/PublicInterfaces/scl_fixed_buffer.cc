/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: scl_fixed_buffer.cc
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Oct-2004     Initial implementation.
//
//==============================================================================
#define SCL_FBUFFER_TEMPL   template <class Type_, size_t Size_>
#define SCL_FBUFFER_CLASS   scl_fixed_buffer<Type_, Size_>


SCL_FBUFFER_TEMPL
SCL_FBUFFER_CLASS::scl_fixed_buffer(size_type size) :
    first_  (NULL),
    size_   (0),
    align_  (0)
{
    // Allocate storage for the buffer.
    void   *ptr = (size > Size_) ? scl_allocate(size * sizeof(Type_)) : buffer_;
    first_ = reinterpret_cast<Type_ *>(ptr);
    size_  = size;

    // Initialize each element using the default constructor of <Type_>.
    Type_   value = Type_();
    scl_uninitialized_fill_n(first_, size, value);
}

SCL_FBUFFER_TEMPL
SCL_FBUFFER_CLASS::scl_fixed_buffer(size_type size, const Type_& value) :
    first_  (NULL),
    size_   (0),
    align_  (0)
{
    // Allocate storage for the buffer.
    void   *ptr = (size > Size_) ? scl_allocate(size * sizeof(Type_)) : buffer_;
    first_ = reinterpret_cast<Type_ *>(ptr);
    size_  = size;

    // Initialize each element using the specified object.
    scl_uninitialized_fill_n(first_, size, value);
}

SCL_FBUFFER_TEMPL
SCL_FBUFFER_CLASS::~scl_fixed_buffer()
{
    // Destroy each element using the destructor of <Type_>.
    _scl_destroy(first_, first_ + size_);

    // Deallocate storage for the buffer.
    if (size_ > Size_)
        scl_deallocate(first_, size_ * sizeof(Type_));
}

SCL_FBUFFER_TEMPL
typename SCL_FBUFFER_CLASS::iterator
SCL_FBUFFER_CLASS::begin()
{
    return iterator(first_);
}

SCL_FBUFFER_TEMPL
typename SCL_FBUFFER_CLASS::const_iterator
SCL_FBUFFER_CLASS::begin() const
{
    return const_iterator(first_);
}

SCL_FBUFFER_TEMPL
typename SCL_FBUFFER_CLASS::iterator
SCL_FBUFFER_CLASS::end()
{
    return iterator(first_ + size_);
}

SCL_FBUFFER_TEMPL
typename SCL_FBUFFER_CLASS::const_iterator
SCL_FBUFFER_CLASS::end() const
{
    return const_iterator(first_ + size_);
}

SCL_FBUFFER_TEMPL
typename SCL_FBUFFER_CLASS::reverse_iterator
SCL_FBUFFER_CLASS::rbegin()
{
    return reverse_iterator(end());
}

SCL_FBUFFER_TEMPL
typename SCL_FBUFFER_CLASS::const_reverse_iterator
SCL_FBUFFER_CLASS::rbegin() const
{
    return const_reverse_iterator(end());
}

SCL_FBUFFER_TEMPL
typename SCL_FBUFFER_CLASS::reverse_iterator
SCL_FBUFFER_CLASS::rend()
{
    return reverse_iterator(begin());
}

SCL_FBUFFER_TEMPL
typename SCL_FBUFFER_CLASS::const_reverse_iterator
SCL_FBUFFER_CLASS::rend() const
{
    return const_reverse_iterator(begin());
}

SCL_FBUFFER_TEMPL
typename SCL_FBUFFER_CLASS::size_type
SCL_FBUFFER_CLASS::size() const
{
    return size_;
}

SCL_FBUFFER_TEMPL
typename SCL_FBUFFER_CLASS::size_type
SCL_FBUFFER_CLASS::capacity() const
{
    return (size_ > Size_) ? size_ : Size_;
}

SCL_FBUFFER_TEMPL
typename SCL_FBUFFER_CLASS::reference
SCL_FBUFFER_CLASS::operator[](size_type idx)
{
    SCL_PRECONDITION(idx < size_);
    return *(first_ + idx);
}

SCL_FBUFFER_TEMPL
typename SCL_FBUFFER_CLASS::const_reference
SCL_FBUFFER_CLASS::operator[](size_type idx) const
{
    SCL_PRECONDITION(idx < size_);
    return *(first_ + idx);
}

SCL_FBUFFER_TEMPL
typename SCL_FBUFFER_CLASS::pointer
SCL_FBUFFER_CLASS::data()
{
    return first_;
}

SCL_FBUFFER_TEMPL
typename SCL_FBUFFER_CLASS::const_pointer
SCL_FBUFFER_CLASS::data() const
{
    return first_;
}


#undef  SCL_FBUFFER_TEMPL
#undef  SCL_FBUFFER_CLASS
