/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: sclp_vector.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Aug-2004     Initial implementation.
//
//  NOTICE:
//      This header is used internally by the Standard C++ Library.
//      It should not be included directly in application code.
//
//==============================================================================
#ifndef _SCLP_VECTOR_H_
#define _SCLP_VECTOR_H_


//
//  Include section
//
#ifndef  _SCLP_ALLOCATOR_H_
#include <sclp_allocator.h>             // for scl_allocator
#endif

#ifndef  _SCLP_BASE_ITER_H_
#include <sclp_base_iter.h>             // for scl_iterator and friends
#endif

#ifndef  _SCLP_POINTER_ITER_H_
#include <sclp_pointer_iter.h>          // for scl_pointer_iterator
#endif

#ifndef  _SCLP_UNINITIALIZED_H_
#include <sclp_uninitialized.h>         // for _scl_uninitialized_copy()
#endif

#ifndef  _SCLP_BASE_ALGO_H_
#include <sclp_base_algo.h>             // for scl_equal(), scl_lexicographical_compare()
#endif


//
//  Template class scl_vector
//
#ifdef  SCL_HAS_DEFAULT_TMPL_ARGS
template <class Type_, class Alloc_ = scl_allocator<Type_> >
#else
template <class Type_, class Alloc_>
#endif
class scl_vector
{
public:
    // Private types
    typedef scl_vector<Type_, Alloc_>               Self_;

    // Public types
    typedef Type_                                   value_type;
    typedef Alloc_                                  allocator_type;
    typedef typename Alloc_::pointer                pointer;
    typedef typename Alloc_::const_pointer          const_pointer;
    typedef typename Alloc_::reference              reference;
    typedef typename Alloc_::const_reference        const_reference;
    typedef typename Alloc_::size_type              size_type;
    typedef typename Alloc_::difference_type        difference_type;

    typedef scl_pointer_iterator<Type_>             iterator;
    typedef scl_pointer_const_iterator<Type_>       const_iterator;
    typedef scl_reverse_iterator<iterator>          reverse_iterator;
    typedef scl_reverse_iterator<const_iterator>    const_reverse_iterator;

    // 23.2.4.1 construct/copy/destroy
    scl_vector();
    explicit scl_vector(const Alloc_& alloc);

    explicit scl_vector(size_type len);
    scl_vector(size_type len, const Type_& value);
    scl_vector(size_type len, const Type_& value, const Alloc_& alloc);

#ifdef  SCL_HAS_MEMBER_TEMPLATES
    template <class InputIterator_>
    scl_vector(InputIterator_ first, InputIterator_ last);

    template <class InputIterator_>
    scl_vector(InputIterator_ first, InputIterator_ last, const Alloc_& alloc);
#else
    scl_vector(const_iterator first, const_iterator last);
    scl_vector(const_iterator first, const_iterator last, const Alloc_& alloc);
#endif

    scl_vector(const Self_& other);
    ~scl_vector();

    Self_&          operator=(const Self_& other);

#ifdef  SCL_HAS_MEMBER_TEMPLATES
    template <class InputIterator_>
    void            assign(InputIterator_ first, InputIterator_ last);
#else
    void            assign(const_iterator first, const_iterator last);
#endif

    void            assign(size_type len, const Type_& value);
    allocator_type  get_allocator() const;

    // iterators
    iterator        begin();
    const_iterator  begin() const;
    iterator        end();
    const_iterator  end() const;

    reverse_iterator        rbegin();
    const_reverse_iterator  rbegin() const;
    reverse_iterator        rend();
    const_reverse_iterator  rend() const;

    // 23.2.4.2 capacity
    bool            empty() const       { return (pfirst_ == plast_); }
    size_type       size() const        { return (plast_  - pfirst_); }
    size_type       capacity() const    { return (pfinal_ - pfirst_); }
    size_type       max_size() const    { return alloc_.max_size();   }

    void            resize(size_type len);
    void            resize(size_type len, const Type_& value);
    void            reserve(size_type len);

    // element access -- inlined for efficiency
    reference       operator[](size_type idx)
    {
        SCL_PRECONDITION(idx < size());
        return *(pfirst_ + idx);
    }

    const_reference operator[](size_type idx) const
    {
        SCL_PRECONDITION(idx < size());
        return *(pfirst_ + idx);
    }

    reference       at(size_type idx)
    {
        if (idx >= size())
            _scl_throw_out_of_range();
        return *(pfirst_ + idx);
    }

    const_reference at(size_type idx) const
    {
        if (idx >= size())
            _scl_throw_out_of_range();
        return *(pfirst_ + idx);
    }

    reference       front()
    {
        SCL_PRECONDITION( !empty() );
        return *pfirst_;
    }

    const_reference front() const
    {
        SCL_PRECONDITION( !empty() );
        return *pfirst_;
    }

    reference       back()
    {
        SCL_PRECONDITION( !empty() );
        return *(plast_ - 1);
    }

    const_reference back() const
    {
        SCL_PRECONDITION( !empty() );
        return *(plast_ - 1);
    }

    // 23.2.4.3 modifiers
    void            push_back(const Type_& value);
    void            pop_back();
    iterator        insert(iterator iter, const Type_& value);
    void            insert(iterator iter, size_type len, const Type_& value);

#ifdef  SCL_HAS_MEMBER_TEMPLATES
    template <class InputIterator_>
    void            insert(iterator iter, InputIterator_ first, InputIterator_ last);
#else
    void            insert(iterator iter, const_iterator first, const_iterator last);
#endif

    iterator        erase(iterator iter);
    iterator        erase(iterator first, iterator last);
    void            clear();
    void            swap(Self_& other);

    // Test class invariants (nonstandard method).
    bool            _validate() const;

protected:
    // Utility methods.
    bool            in_range(iterator iter) const;
    void            initialize(size_type len);
    void            reserve_aux(size_type len, bool do_copy);
    void            destroy();
    void            erase_all();
    void            erase_end(size_type len);
    void            append_aux(const Type_& value);
    void            append_aux(size_type len, const Type_& value);

#ifdef  SCL_HAS_MEMBER_TEMPLATES
    template <class InputIterator_>
    void            append_aux(InputIterator_ first, InputIterator_ last);
#else
    void            append_aux(const_iterator first, const_iterator last);
#endif

private:
    // Multiplier used to compute vector capacity.
    enum { IncrementSize = 32 };

    // Data members
    pointer         pfirst_;    // Pointer to first element
    pointer         plast_;     // Pointer just after the last active element
    pointer         pfinal_;    // Pointer just after the end of the buffer
    allocator_type  alloc_;     // Value allocator
};


template <class Type_, class Alloc_>
bool
operator==(const scl_vector<Type_, Alloc_>& lhs,
           const scl_vector<Type_, Alloc_>& rhs)
{
    return (lhs.size() == rhs.size()) && scl_equal(lhs.begin(), lhs.end(), rhs.begin());
}

template <class Type_, class Alloc_>
bool
operator< (const scl_vector<Type_, Alloc_>& lhs,
           const scl_vector<Type_, Alloc_>& rhs)
{
    return scl_lexicographical_compare(lhs.begin(), lhs.end(), rhs.begin(), rhs.end());
}

template <class Type_, class Alloc_>
bool
operator!=(const scl_vector<Type_, Alloc_>& lhs,
           const scl_vector<Type_, Alloc_>& rhs)
{
    return !(lhs == rhs);
}

template <class Type_, class Alloc_>
bool
operator>=(const scl_vector<Type_, Alloc_>& lhs,
           const scl_vector<Type_, Alloc_>& rhs)
{
    return !(lhs < rhs);
}

template <class Type_, class Alloc_>
bool
operator> (const scl_vector<Type_, Alloc_>& lhs,
           const scl_vector<Type_, Alloc_>& rhs)
{
    return (rhs < lhs);
}

template <class Type_, class Alloc_>
bool
operator<=(const scl_vector<Type_, Alloc_>& lhs,
           const scl_vector<Type_, Alloc_>& rhs)
{
    return !(rhs < lhs);
}

#ifdef  SCL_HAS_PARTIAL_SPEC_OVERLOAD
template <class Type_, class Alloc_>
void
scl_swap(scl_vector<Type_, Alloc_>& lhs,
         scl_vector<Type_, Alloc_>& rhs)
{
    lhs.swap(rhs);
}
#endif


#include <sclp_vector.cc>


#endif  /* _SCLP_VECTOR_H_ */
