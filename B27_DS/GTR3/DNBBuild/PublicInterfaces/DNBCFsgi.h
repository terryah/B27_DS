/** @CAA2Required */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 1997
//==============================================================================
//
//  FILE: DNBCFsgi.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     23-Jun-1997     Initial implementation.
//
//  OVERVIEW:
//      Attributes of SGI IRIX 32-bit (C++ 7.2.1).
//      Do not include this header directly in application code.
//
//==============================================================================
#define DNB_HAS_POSIX_THREADS       DNB_YES
#define DNB_HAS_WIN32_THREADS       DNB_NO
#define DNB_HAS_PRAGMA_ONCE         DNB_YES
#define DNB_HAS_BOOL                DNB_YES
#define DNB_HAS_THROW_SPECS         DNB_YES
#define DNB_HAS_STD_NAMESPACE       DNB_YES
#define DNB_HAS_FUNC_INST           DNB_NO              // Problems in DNBMath.
#define DNB_HAS_CLASS_INST          DNB_YES
#define DNB_HAS_DOUBLE_ABS          DNB_NO
#define DNB_HAS_FLOAT_MATH          DNB_NO
#define DNB_HAS_FLOAT_MATHF         DNB_YES
#define DNB_HAS_FLOAT_FMATH         DNB_NO
#define DNB_HAS_ATOMIC_LOCK         DNB_YES
#define DNB_HAS_ATOMIC_INTEGER      DNB_YES
#define DNB_HAS_FRIEND_TEMPLATE     DNB_YES


#define _REENTRANT


//
//  Provide a standard-conforming implementation of the allocation functions.
//  Note: MIPSPro C++ 7.2.1 does not throw an exception when the free-store
//  is exhausted; rather it returns a null pointer.
//
struct  nothrow_t   { };
const   nothrow_t   nothrow;

inline  void*
operator new( size_t size, const nothrow_t& )
    throw( )
{
    return ( ::operator new( size ) );
}

inline  void*
operator new[]( size_t size, const nothrow_t& )
    throw( )
{
    return ( ::operator new[]( size ) );
}
