/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: sclp_string.cc
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Sep-2004     Initial implementation.
//
//==============================================================================


//
//  Private class _scl_string_base.
//
#define SCL_STRBASE_TEMPL   template <class Type_, class Traits_, class Alloc_>
#define SCL_STRBASE_CLASS   _scl_string_base<Type_, Traits_, Alloc_>

SCL_STRBASE_TEMPL
bool
SCL_STRBASE_CLASS::_validate() const
{
    bool okay =
        (first_ != NULL)          &&
        (length_    <= capacity_) &&
        (BufferSize <= capacity_) &&
        (first_[length_] == Traits_::eos())    &&
        ((capacity_ + 1) % IncrementSize == 0) &&
        ((capacity_ == BufferSize) == (first_ == buffer_));

    return okay;
}

SCL_STRBASE_TEMPL
const Type_
SCL_STRBASE_CLASS::NullChar = Type_(0);

SCL_STRBASE_TEMPL
typename SCL_STRBASE_CLASS::size_type
SCL_STRBASE_CLASS::Min(size_type lhs, size_type rhs)
{
    return (lhs < rhs) ? lhs : rhs;
}

SCL_STRBASE_TEMPL
SCL_STRBASE_CLASS::_scl_string_base() :
    alloc_()
{
    initialize();
}

SCL_STRBASE_TEMPL
SCL_STRBASE_CLASS::_scl_string_base(const Alloc_& alloc) :
    alloc_(alloc)
{
    initialize();
}

SCL_STRBASE_TEMPL
SCL_STRBASE_CLASS::_scl_string_base(const Self_& cstr) :
    alloc_(cstr.alloc_)
{
    initialize();
    assign_aux(cstr.first_, cstr.length_);
}

SCL_STRBASE_TEMPL
SCL_STRBASE_CLASS::_scl_string_base(const Type_* cstr, size_type clen) :
    alloc_()
{
    SCL_PRECONDITION(cstr != NULL);
    initialize();
    assign_aux(cstr, clen);
}

SCL_STRBASE_TEMPL
SCL_STRBASE_CLASS::_scl_string_base(const Type_* cstr, size_type clen, const Alloc_& alloc) :
    alloc_(alloc)
{
    SCL_PRECONDITION(cstr != NULL);
    initialize();
    assign_aux(cstr, clen);
}

SCL_STRBASE_TEMPL
SCL_STRBASE_CLASS::_scl_string_base(size_type clen, Type_ cval) :
    alloc_()
{
    initialize();
    assign_aux(clen, cval);
}

SCL_STRBASE_TEMPL
SCL_STRBASE_CLASS::_scl_string_base(size_type clen, Type_ cval, const Alloc_& alloc) :
    alloc_(alloc)
{
    initialize();
    assign_aux(clen, cval);
}

SCL_STRBASE_TEMPL
SCL_STRBASE_CLASS::~_scl_string_base()
{
    destroy();
}

SCL_STRBASE_TEMPL
void
SCL_STRBASE_CLASS::operator=(const Self_& cstr)
{
    assign_aux(cstr.first_, cstr.length_);
}

SCL_STRBASE_TEMPL
void
SCL_STRBASE_CLASS::check_index(size_type pos) const
{
    if (pos > length_)
        _scl_throw_out_of_range();
}

SCL_STRBASE_TEMPL
void
SCL_STRBASE_CLASS::check_range(size_type pos, size_type& len) const
{
    if (pos > length_)
        _scl_throw_out_of_range();

    len = Min(len, length_ - pos);
}

SCL_STRBASE_TEMPL
void
SCL_STRBASE_CLASS::swap_aux(Self_& cstr)
{
    const Self_ temp(*this);
    assign_aux     (cstr.first_, cstr.length_);
    cstr.assign_aux(temp.first_, temp.length_);
}

SCL_STRBASE_TEMPL
void
SCL_STRBASE_CLASS::reserve_aux(size_type new_cap)
{
    // In this implementation, strings do not shrink.
    if (new_cap <= capacity_)
        return;

    Type_*  first = allocate(new_cap);          // Note: <new_cap> is modified
    Traits_::copy(first, first_, length_ + 1);  // Copy string and terminating null

    destroy();
    first_    = first;
    capacity_ = new_cap;
}

SCL_STRBASE_TEMPL
const Type_*
SCL_STRBASE_CLASS::reserve_aux(size_type new_cap, const Type_* cstr)
{
    // In this implementation, strings do not shrink.
    if (new_cap <= capacity_)
        return cstr;

    // Determine if <self> and <cstr> overlap.
    if (first_ <= cstr && cstr <= (first_ + length_))
    {
        const size_type offset = cstr - first_;
        reserve_aux(new_cap);
        cstr = first_ + offset;
    }
    else
    {
        reserve_aux(new_cap);
    }

    return cstr;
}

SCL_STRBASE_TEMPL
void
SCL_STRBASE_CLASS::resize_aux(size_type clen, Type_ cval)
{
    if (clen <= length_)
    {
        // Truncate string
        set_length(clen);
    }
    else
    {
        // Right-fill string
        append_aux(clen - length_, cval);
    }
}

SCL_STRBASE_TEMPL
void
SCL_STRBASE_CLASS::assign_aux(size_type clen, Type_ cval)
{
    reserve_aux(clen);
    Traits_::assign(first_, clen, cval);
    set_length(clen);
}

SCL_STRBASE_TEMPL
void
SCL_STRBASE_CLASS::assign_aux(const Type_* cstr, size_type clen)
{
    cstr = reserve_aux(clen, cstr);
    Traits_::move(first_, cstr, clen);          // possible overlap
    set_length(clen);
}

SCL_STRBASE_TEMPL
void
SCL_STRBASE_CLASS::append_aux(Type_ cval)
{
    const size_type new_len = length_ + 1;
    reserve_aux(new_len);
    Traits_::assign(first_[length_], cval);
    set_length(new_len);
}

SCL_STRBASE_TEMPL
void
SCL_STRBASE_CLASS::append_aux(size_type clen, Type_ cval)
{
    const size_type new_len = length_ + clen;
    reserve_aux(new_len);
    Traits_::assign(first_ + length_, clen, cval);
    set_length(new_len);
}

SCL_STRBASE_TEMPL
void
SCL_STRBASE_CLASS::append_aux(const Type_* cstr, size_type clen)
{
    const size_type new_len = length_ + clen;
    cstr = reserve_aux(new_len, cstr);
    Traits_::move(first_ + length_, cstr, clen);    // possible overlap
    set_length(new_len);
}

SCL_STRBASE_TEMPL
void
SCL_STRBASE_CLASS::replace_aux(size_type pos, size_type len, size_type clen, Type_ cval)
{
    check_range(pos, len);

    if ((length_ - len) >= (SCL_NPOS - clen))
        _scl_throw_length_error();

    const size_type new_len = length_ - len + clen;

    if (new_len <= capacity_)
    {
        Type_*  sptr = first_ + pos;

        if (len != clen)
        {
            const size_type soff = pos + len;
            Traits_::move(sptr + clen, sptr + len, length_ - soff);
        }

        Traits_::assign(sptr, clen, cval);
    }
    else
    {
        size_type new_cap = new_len;

        Type_*  first = allocate(new_cap);      // Note: <new_cap> is modified
        Type_*  sptr  = first;

        Traits_::copy(sptr, first_, pos);
        sptr += pos;

        Traits_::assign(sptr, clen, cval);
        sptr += clen;

        const size_type soff = pos + len;
        Traits_::copy(sptr, first_ + soff, length_ - soff);

        destroy();
        first_    = first;
        capacity_ = new_cap;
    }

    set_length(new_len);
}

SCL_STRBASE_TEMPL
void
SCL_STRBASE_CLASS::replace_aux(size_type pos, size_type len, const Type_* cstr, size_type clen)
{
    check_range(pos, len);

    if ((length_ - len) >= (SCL_NPOS - clen))
        _scl_throw_length_error();

    const size_type new_len = length_ - len + clen;

    if (new_len <= capacity_)
    {
        Type_*  sptr = first_ + pos;

        if (len != clen)
        {
            const size_type soff = pos + len;
            Traits_::move(sptr + clen, sptr + len, length_ - soff);
        }

        Traits_::move(sptr, cstr, clen);        // possible overlap
    }
    else
    {
        size_type new_cap = new_len;

        Type_*  first = allocate(new_cap);      // Note: <new_cap> is modified
        Type_*  sptr  = first;

        Traits_::copy(sptr, first_, pos);
        sptr += pos;

        Traits_::copy(sptr, cstr, clen);
        sptr += clen;

        const size_type soff = pos + len;
        Traits_::copy(sptr, first_ + soff, length_ - soff);

        destroy();
        first_    = first;
        capacity_ = new_cap;
    }

    set_length(new_len);
}

SCL_STRBASE_TEMPL
void
SCL_STRBASE_CLASS::initialize()
{
    // Establish class invariants.
    first_    = buffer_;
    capacity_ = BufferSize;
    set_length(0);
}

SCL_STRBASE_TEMPL
void
SCL_STRBASE_CLASS::set_length(size_type len)
{
    length_ = len;
    first_[len] = this->NullChar;
}

SCL_STRBASE_TEMPL
Type_*
SCL_STRBASE_CLASS::allocate(size_type &len)
{
    if (len > max_size())
        _scl_throw_length_error();

    const size_type inc_len = IncrementSize * (len / IncrementSize + 1) - 1;
    SCL_ASSERT(inc_len >= len);
    len = inc_len;

    Type_*  first = alloc_.allocate(len + 1);
    return first;
}

SCL_STRBASE_TEMPL
void
SCL_STRBASE_CLASS::destroy()
{
    if (first_ != buffer_)
        alloc_.deallocate(first_, capacity_ + 1);

    first_ = NULL;
}

#undef  SCL_STRBASE_TEMPL
#undef  SCL_STRBASE_CLASS


//
//  Public class scl_basic_string.
//
#define SCL_BSTRING_TEMPL   template <class Type_, class Traits_, class Alloc_>
#define SCL_BSTRING_CLASS   scl_basic_string<Type_, Traits_, Alloc_>

#ifndef SCL_HAS_STATIC_CONST_INIT
SCL_BSTRING_TEMPL
const size_t
SCL_BSTRING_CLASS::npos = SCL_NPOS;
#endif

//
//  21.3.1 construct/copy/destroy
//
SCL_BSTRING_TEMPL
SCL_BSTRING_CLASS::scl_basic_string() :
    Base_()
{
    // Nothing
}

SCL_BSTRING_TEMPL
SCL_BSTRING_CLASS::scl_basic_string(const Alloc_& alloc) :
    Base_(alloc)
{
    // Nothing
}

SCL_BSTRING_TEMPL
SCL_BSTRING_CLASS::scl_basic_string(const Self_& cstr) :
    Base_(cstr)
{
    // Nothing
}

SCL_BSTRING_TEMPL
SCL_BSTRING_CLASS::scl_basic_string(const Self_& cstr, size_type cpos, size_type clen) :
    Base_()
{
    // Note: <cpos> and <clen> are checked in 'assign' method.
    assign(cstr, cpos, clen);
}

SCL_BSTRING_TEMPL
SCL_BSTRING_CLASS::scl_basic_string(const Self_& cstr, size_type cpos, size_type clen,
    const Alloc_& alloc) :
    Base_(alloc)
{
    // Note: <cpos> and <clen> are checked in 'assign' method.
    assign(cstr, cpos, clen);
}

SCL_BSTRING_TEMPL
SCL_BSTRING_CLASS::scl_basic_string(const Type_* cstr, size_type clen) :
    Base_(cstr, clen)
{
    // Nothing
}

SCL_BSTRING_TEMPL
SCL_BSTRING_CLASS::scl_basic_string(const Type_* cstr, size_type clen, const Alloc_& alloc) :
    Base_(cstr, clen, alloc)
{
    // Nothing
}

SCL_BSTRING_TEMPL
SCL_BSTRING_CLASS::scl_basic_string(const Type_* cstr) :
    Base_(cstr, Traits_::length(cstr))
{
    // Nothing
}

SCL_BSTRING_TEMPL
SCL_BSTRING_CLASS::scl_basic_string(const Type_* cstr, const Alloc_& alloc) :
    Base_(cstr, Traits_::length(cstr), alloc)
{
    // Nothing
}

SCL_BSTRING_TEMPL
SCL_BSTRING_CLASS::scl_basic_string(size_type clen, Type_ cval) :
    Base_(clen, cval)
{
    // Nothing
}

SCL_BSTRING_TEMPL
SCL_BSTRING_CLASS::scl_basic_string(size_type clen, Type_ cval, const Alloc_& alloc) :
    Base_(clen, cval, alloc)
{
    // Nothing
}

#ifdef  SCL_HAS_MEMBER_TEMPLATES
SCL_BSTRING_TEMPL
template <class InputIterator_>
SCL_BSTRING_CLASS::scl_basic_string(InputIterator_ cfirst, InputIterator_ clast) :
#else
SCL_BSTRING_TEMPL
SCL_BSTRING_CLASS::scl_basic_string(const_iterator cfirst, const_iterator clast) :
#endif
    Base_()
{
    assign(cfirst, clast);
}

#ifdef  SCL_HAS_MEMBER_TEMPLATES
SCL_BSTRING_TEMPL
template <class InputIterator_>
SCL_BSTRING_CLASS::scl_basic_string(InputIterator_ cfirst, InputIterator_ clast,
    const Alloc_& alloc) :
#else
SCL_BSTRING_TEMPL
SCL_BSTRING_CLASS::scl_basic_string(const_iterator cfirst, const_iterator clast,
    const Alloc_& alloc) :
#endif
    Base_(alloc)
{
    assign(cfirst, clast);
}

SCL_BSTRING_TEMPL
SCL_BSTRING_CLASS::~scl_basic_string()
{
    // Nothing
}

SCL_BSTRING_TEMPL
SCL_BSTRING_CLASS &
SCL_BSTRING_CLASS::operator=(const Self_& cstr)
{
    // Equivalent to 'assign(cstr)'
    if (this != &cstr)
        assign_aux(cstr.data(), cstr.size());
    return *this;
}

SCL_BSTRING_TEMPL
SCL_BSTRING_CLASS &
SCL_BSTRING_CLASS::operator=(const Type_* cstr)
{
    // Equivalent to 'assign(cstr)'
    assign_aux(cstr, Traits_::length(cstr));
    return *this;
}

SCL_BSTRING_TEMPL
SCL_BSTRING_CLASS &
SCL_BSTRING_CLASS::operator=(Type_ cval)
{
    // Equivalent to 'assign(1, cval)'
    assign_aux(1, cval);
    return *this;
}

//
//  21.3.2 iterators
//
SCL_BSTRING_TEMPL
typename SCL_BSTRING_CLASS::iterator
SCL_BSTRING_CLASS::begin()
{
    return iterator(this->begin_aux());
}

SCL_BSTRING_TEMPL
typename SCL_BSTRING_CLASS::const_iterator
SCL_BSTRING_CLASS::begin() const
{
    return const_iterator(this->begin_aux());
}

SCL_BSTRING_TEMPL
typename SCL_BSTRING_CLASS::iterator
SCL_BSTRING_CLASS::end()
{
    return iterator(this->end_aux());
}

SCL_BSTRING_TEMPL
typename SCL_BSTRING_CLASS::const_iterator
SCL_BSTRING_CLASS::end() const
{
    return const_iterator(this->end_aux());
}

SCL_BSTRING_TEMPL
typename SCL_BSTRING_CLASS::reverse_iterator
SCL_BSTRING_CLASS::rbegin()
{
    return reverse_iterator(end());
}

SCL_BSTRING_TEMPL
typename SCL_BSTRING_CLASS::const_reverse_iterator
SCL_BSTRING_CLASS::rbegin() const
{
    return const_reverse_iterator(end());
}

SCL_BSTRING_TEMPL
typename SCL_BSTRING_CLASS::reverse_iterator
SCL_BSTRING_CLASS::rend()
{
    return reverse_iterator(begin());
}

SCL_BSTRING_TEMPL
typename SCL_BSTRING_CLASS::const_reverse_iterator
SCL_BSTRING_CLASS::rend() const
{
    return const_reverse_iterator(begin());
}

//
//  21.3.3 capacity
//
SCL_BSTRING_TEMPL
typename SCL_BSTRING_CLASS::size_type
SCL_BSTRING_CLASS::length() const
{
    return this->size();
}

SCL_BSTRING_TEMPL
void
SCL_BSTRING_CLASS::resize(size_type clen, Type_ cval)
{
    resize_aux(clen, cval);
}

SCL_BSTRING_TEMPL
void
SCL_BSTRING_CLASS::resize(size_type clen)
{
    resize_aux(clen, this->NullChar);
}

SCL_BSTRING_TEMPL
void
SCL_BSTRING_CLASS::reserve(size_type new_cap)
{
    reserve_aux(new_cap);
}

SCL_BSTRING_TEMPL
void
SCL_BSTRING_CLASS::clear()
{
    resize_aux(0, this->NullChar);
}

SCL_BSTRING_TEMPL
bool
SCL_BSTRING_CLASS::empty() const
{
    return this->size() == 0;
}

//
//  21.3.4 element access
//
SCL_BSTRING_TEMPL
typename SCL_BSTRING_CLASS::const_reference
SCL_BSTRING_CLASS::operator[](size_type pos) const
{
    // Note: In the const accessor, first_[size()] must return end-of-string.
    SCL_PRECONDITION(pos <= this->size());
    return *(this->begin_aux() + pos);
}

SCL_BSTRING_TEMPL
typename SCL_BSTRING_CLASS::reference
SCL_BSTRING_CLASS::operator[](size_type pos)
{
    // Note: In the non-const accessor, first_[size()] is undefined.
    SCL_PRECONDITION(pos <  this->size());
    return *(this->begin_aux() + pos);
}

SCL_BSTRING_TEMPL
typename SCL_BSTRING_CLASS::const_reference
SCL_BSTRING_CLASS::at(size_type pos) const
{
    if (pos >= this->size())
        _scl_throw_out_of_range();
    return *(this->begin_aux() + pos);
}

SCL_BSTRING_TEMPL
typename SCL_BSTRING_CLASS::reference
SCL_BSTRING_CLASS::at(size_type pos)
{
    if (pos >= this->size())
        _scl_throw_out_of_range();
    return *(this->begin_aux() + pos);
}

//
//  21.3.5 modifiers
//
SCL_BSTRING_TEMPL
SCL_BSTRING_CLASS &
SCL_BSTRING_CLASS::operator+=(const Self_& cstr)
{
    // Equivalent to 'append(cstr)'
    append_aux(cstr.data(), cstr.size());
    return *this;
}

SCL_BSTRING_TEMPL
SCL_BSTRING_CLASS &
SCL_BSTRING_CLASS::operator+=(const Type_* cstr)
{
    // Equivalent to 'append(cstr)'
    append_aux(cstr, Traits_::length(cstr));
    return *this;
}

SCL_BSTRING_TEMPL
SCL_BSTRING_CLASS &
SCL_BSTRING_CLASS::operator+=(Type_ cval)
{
    append_aux(cval);
    return *this;
}

SCL_BSTRING_TEMPL
SCL_BSTRING_CLASS &
SCL_BSTRING_CLASS::append(const Self_& cstr)
{
    append_aux(cstr.data(), cstr.size());
    return *this;
}

SCL_BSTRING_TEMPL
SCL_BSTRING_CLASS &
SCL_BSTRING_CLASS::append(const Self_& cstr, size_type cpos, size_type clen)
{
    cstr.check_range(cpos, clen);
    append_aux(cstr.data() + cpos, clen);
    return *this;
}

SCL_BSTRING_TEMPL
SCL_BSTRING_CLASS &
SCL_BSTRING_CLASS::append(const Type_* cstr, size_type clen)
{
    append_aux(cstr, clen);
    return *this;
}

SCL_BSTRING_TEMPL
SCL_BSTRING_CLASS &
SCL_BSTRING_CLASS::append(const Type_* cstr)
{
    append_aux(cstr, Traits_::length(cstr));
    return *this;
}

SCL_BSTRING_TEMPL
SCL_BSTRING_CLASS &
SCL_BSTRING_CLASS::append(size_type clen, Type_ cval)
{
    append_aux(clen, cval);
    return *this;
}

#ifdef  SCL_HAS_MEMBER_TEMPLATES
SCL_BSTRING_TEMPL
template <class InputIterator_>
SCL_BSTRING_CLASS &
SCL_BSTRING_CLASS::append(InputIterator_ cfirst, InputIterator_ clast)
#else
SCL_BSTRING_TEMPL
SCL_BSTRING_CLASS &
SCL_BSTRING_CLASS::append(const_iterator cfirst, const_iterator clast)
#endif
{
    const Self_     cstr(cfirst, clast);
    append_aux(cstr.data(), cstr.size());
    return *this;
}

SCL_BSTRING_TEMPL
void
SCL_BSTRING_CLASS::push_back(Type_ cval)
{
    append_aux(cval);
}

SCL_BSTRING_TEMPL
SCL_BSTRING_CLASS &
SCL_BSTRING_CLASS::assign(const Self_& cstr)
{
    if (this != &cstr)
        assign_aux(cstr.data(), cstr.size());
    return *this;
}

SCL_BSTRING_TEMPL
SCL_BSTRING_CLASS &
SCL_BSTRING_CLASS::assign(const Self_& cstr, size_type cpos, size_type clen)
{
    cstr.check_range(cpos, clen);
    assign_aux(cstr.data() + cpos, clen);
    return *this;
}

SCL_BSTRING_TEMPL
SCL_BSTRING_CLASS &
SCL_BSTRING_CLASS::assign(const Type_* cstr, size_type clen)
{
    assign_aux(cstr, clen);
    return *this;
}

SCL_BSTRING_TEMPL
SCL_BSTRING_CLASS &
SCL_BSTRING_CLASS::assign(const Type_* cstr)
{
    assign_aux(cstr, Traits_::length(cstr));
    return *this;
}

SCL_BSTRING_TEMPL
SCL_BSTRING_CLASS &
SCL_BSTRING_CLASS::assign(size_type clen, Type_ cval)
{
    assign_aux(clen, cval);
    return *this;
}

#ifdef  SCL_HAS_MEMBER_TEMPLATES
SCL_BSTRING_TEMPL
template <class InputIterator_>
SCL_BSTRING_CLASS &
SCL_BSTRING_CLASS::assign(InputIterator_ cfirst, InputIterator_ clast)
#else
SCL_BSTRING_TEMPL
SCL_BSTRING_CLASS &
SCL_BSTRING_CLASS::assign(const_iterator cfirst, const_iterator clast)
#endif
{
    const size_type new_len = scl_distance(cfirst, clast);
    clear();
    reserve_aux(new_len);

    // FIXME: Optimize assignment based on iterator type.
    for (; cfirst != clast; ++cfirst)
        append_aux(*cfirst);

    return *this;
}

SCL_BSTRING_TEMPL
SCL_BSTRING_CLASS &
SCL_BSTRING_CLASS::insert(size_type pos, const Self_& cstr)
{
    replace_aux(pos, 0, cstr.data(), cstr.size());
    return *this;
}

SCL_BSTRING_TEMPL
SCL_BSTRING_CLASS &
SCL_BSTRING_CLASS::insert(size_type pos, const Self_& cstr, size_type cpos, size_type clen)
{
    cstr.check_range(cpos, clen);
    replace_aux(pos, 0, cstr.data() + cpos, clen);
    return *this;
}

SCL_BSTRING_TEMPL
SCL_BSTRING_CLASS &
SCL_BSTRING_CLASS::insert(size_type pos, const Type_* cstr, size_type clen)
{
    replace_aux(pos, 0, cstr, clen);
    return *this;
}

SCL_BSTRING_TEMPL
SCL_BSTRING_CLASS &
SCL_BSTRING_CLASS::insert(size_type pos, const Type_* cstr)
{
    replace_aux(pos, 0, cstr, Traits_::length(cstr));
    return *this;
}

SCL_BSTRING_TEMPL
SCL_BSTRING_CLASS &
SCL_BSTRING_CLASS::insert(size_type pos, size_type clen, Type_ cval)
{
    replace_aux(pos, 0, clen, cval);
    return *this;
}

SCL_BSTRING_TEMPL
typename SCL_BSTRING_CLASS::iterator
SCL_BSTRING_CLASS::insert(iterator iter, Type_ cval)
{
    const size_type pos = iter - begin();
    replace_aux(pos, 0, 1, cval);
    return begin() + pos;
}

SCL_BSTRING_TEMPL
void
SCL_BSTRING_CLASS::insert(iterator iter, size_type clen, Type_ cval)
{
    const size_type pos = iter - begin();
    replace_aux(pos, 0, clen, cval);
}

#ifdef  SCL_HAS_MEMBER_TEMPLATES
SCL_BSTRING_TEMPL
template <class InputIterator_>
void
SCL_BSTRING_CLASS::insert(iterator iter, InputIterator_ cfirst, InputIterator_ clast)
#else
SCL_BSTRING_TEMPL
void
SCL_BSTRING_CLASS::insert(iterator iter, const_iterator cfirst, const_iterator clast)
#endif
{
    const size_type pos = iter - begin();
    const Self_     cstr(cfirst, clast);
    replace_aux(pos, 0, cstr.data(), cstr.size());
}

SCL_BSTRING_TEMPL
SCL_BSTRING_CLASS &
SCL_BSTRING_CLASS::erase(size_type pos, size_type len)
{
    replace_aux(pos, len, 0, this->NullChar);
    return *this;
}

SCL_BSTRING_TEMPL
typename SCL_BSTRING_CLASS::iterator
SCL_BSTRING_CLASS::erase(iterator iter)
{
    const size_type pos = iter - begin();
    replace_aux(pos, 1, 0, this->NullChar);
    return (begin() + pos);
}

SCL_BSTRING_TEMPL
typename SCL_BSTRING_CLASS::iterator
SCL_BSTRING_CLASS::erase(iterator first, iterator last)
{
    const size_type pos = first - begin();
    const size_type len = last  - first;
    replace_aux(pos, len, 0, this->NullChar);
    return (begin() + pos);
}

SCL_BSTRING_TEMPL
SCL_BSTRING_CLASS &
SCL_BSTRING_CLASS::replace(size_type pos, size_type len, const Self_& cstr)
{
    replace_aux(pos, len, cstr.data(), cstr.size());
    return *this;
}

SCL_BSTRING_TEMPL
SCL_BSTRING_CLASS &
SCL_BSTRING_CLASS::replace(size_type pos, size_type len, const Self_& cstr,
    size_type cpos, size_type clen)
{
    cstr.check_range(cpos, clen);
    replace_aux(pos, len, cstr.data() + cpos, clen);
    return *this;
}

SCL_BSTRING_TEMPL
SCL_BSTRING_CLASS &
SCL_BSTRING_CLASS::replace(size_type pos, size_type len, const Type_* cstr, size_type clen)
{
    replace_aux(pos, len, cstr, clen);
    return *this;
}

SCL_BSTRING_TEMPL
SCL_BSTRING_CLASS &
SCL_BSTRING_CLASS::replace(size_type pos, size_type len, const Type_* cstr)
{
    replace_aux(pos, len, cstr, Traits_::length(cstr));
    return *this;
}

SCL_BSTRING_TEMPL
SCL_BSTRING_CLASS &
SCL_BSTRING_CLASS::replace(size_type pos, size_type len, size_type clen, Type_ cval)
{
    replace_aux(pos, len, clen, cval);
    return *this;
}

SCL_BSTRING_TEMPL
SCL_BSTRING_CLASS &
SCL_BSTRING_CLASS::replace(iterator first, iterator last, const Self_& cstr)
{
    const size_type pos = first - begin();
    const size_type len = last  - first;
    replace_aux(pos, len, cstr.data(), cstr.size());
    return *this;
}

SCL_BSTRING_TEMPL
SCL_BSTRING_CLASS &
SCL_BSTRING_CLASS::replace(iterator first, iterator last, const Type_* cstr, size_type clen)
{
    const size_type pos = first - begin();
    const size_type len = last  - first;
    replace_aux(pos, len, cstr, clen);
    return *this;
}

SCL_BSTRING_TEMPL
SCL_BSTRING_CLASS &
SCL_BSTRING_CLASS::replace(iterator first, iterator last, const Type_* cstr)
{
    const size_type pos = first - begin();
    const size_type len = last  - first;
    replace_aux(pos, len, cstr, Traits_::length(cstr));
    return *this;
}

SCL_BSTRING_TEMPL
SCL_BSTRING_CLASS &
SCL_BSTRING_CLASS::replace(iterator first, iterator last, size_type clen, Type_ cval)
{
    const size_type pos = first - begin();
    const size_type len = last  - first;
    replace_aux(pos, len, clen, cval);
    return *this;
}

#ifdef  SCL_HAS_MEMBER_TEMPLATES
SCL_BSTRING_TEMPL
template <class InputIterator_>
SCL_BSTRING_CLASS &
SCL_BSTRING_CLASS::replace(iterator first, iterator last, InputIterator_ cfirst,
    InputIterator_ clast)
#else
SCL_BSTRING_TEMPL
SCL_BSTRING_CLASS &
SCL_BSTRING_CLASS::replace(iterator first, iterator last, const_iterator cfirst,
    const_iterator clast)
#endif
{
    const size_type pos = first - begin();
    const size_type len = last  - first;
    const Self_     cstr(cfirst, clast);
    replace_aux(pos, len, cstr.data(), cstr.size());
    return *this;
}

SCL_BSTRING_TEMPL
typename SCL_BSTRING_CLASS::size_type
SCL_BSTRING_CLASS::copy(Type_* cstr, size_type clen, size_type pos) const
{
    check_range(pos, clen);
    Traits_::copy(cstr, this->data() + pos, clen);
    return clen;
}

SCL_BSTRING_TEMPL
void
SCL_BSTRING_CLASS::swap(Self_& cstr)
{
    swap_aux(cstr);
}

//
//  21.3.6 string operations
//
SCL_BSTRING_TEMPL
typename SCL_BSTRING_CLASS::size_type
SCL_BSTRING_CLASS::find(const Self_& cstr, size_type pos) const
{
    return find(cstr.data(), pos, cstr.size());
}

SCL_BSTRING_TEMPL
typename SCL_BSTRING_CLASS::size_type
SCL_BSTRING_CLASS::find(const Type_* cstr, size_type pos, size_type clen) const
{
    const size_type msize = this->size();
    const Type_*    mdata = this->data();

    for (; (pos + clen) <= msize; ++pos)
    {
        if (Traits_::compare(mdata + pos, cstr, clen) == 0)
            return pos;
    }

    return npos;
}

SCL_BSTRING_TEMPL
typename SCL_BSTRING_CLASS::size_type
SCL_BSTRING_CLASS::find(const Type_* cstr, size_type pos) const
{
    return find(cstr, pos, Traits_::length(cstr));
}

SCL_BSTRING_TEMPL
typename SCL_BSTRING_CLASS::size_type
SCL_BSTRING_CLASS::find(Type_ cval, size_type pos) const
{
    return find(&cval, pos, 1);
}

SCL_BSTRING_TEMPL
typename SCL_BSTRING_CLASS::size_type
SCL_BSTRING_CLASS::rfind(const Self_& cstr, size_type pos) const
{
    return rfind(cstr.data(), pos, cstr.size());
}

SCL_BSTRING_TEMPL
typename SCL_BSTRING_CLASS::size_type
SCL_BSTRING_CLASS::rfind(const Type_* cstr, size_type pos, size_type clen) const
{
    const size_type msize = this->size();
    const Type_*    mdata = this->data();

    if (clen <= msize)
    {
        pos = Min(msize - clen, pos);
        do
        {
            if (Traits_::compare(mdata + pos, cstr, clen) == 0)
                return pos;
        }
        while (pos-- > 0);
    }

    return npos;
}

SCL_BSTRING_TEMPL
typename SCL_BSTRING_CLASS::size_type
SCL_BSTRING_CLASS::rfind(const Type_* cstr, size_type pos) const
{
    return rfind(cstr, pos, Traits_::length(cstr));
}

SCL_BSTRING_TEMPL
typename SCL_BSTRING_CLASS::size_type
SCL_BSTRING_CLASS::rfind(Type_ cval, size_type pos) const
{
    return rfind(&cval, pos, 1);
}

SCL_BSTRING_TEMPL
typename SCL_BSTRING_CLASS::size_type
SCL_BSTRING_CLASS::find_first_of(const Self_& cstr, size_type pos) const
{
    return find_first_of(cstr.data(), pos, cstr.size());
}

SCL_BSTRING_TEMPL
typename SCL_BSTRING_CLASS::size_type
SCL_BSTRING_CLASS::find_first_of(const Type_* cstr, size_type pos, size_type clen) const
{
    const size_type msize = this->size();
    const Type_*    mdata = this->data();

    if (clen > 0)
    {
        for (; pos < msize; ++pos)
        {
            if (Traits_::find(cstr, clen, mdata[pos]) != NULL)
                return pos;
        }
    }

    return npos;
}

SCL_BSTRING_TEMPL
typename SCL_BSTRING_CLASS::size_type
SCL_BSTRING_CLASS::find_first_of(const Type_* cstr, size_type pos) const
{
    return find_first_of(cstr, pos, Traits_::length(cstr));
}

SCL_BSTRING_TEMPL
typename SCL_BSTRING_CLASS::size_type
SCL_BSTRING_CLASS::find_first_of(Type_ cval, size_type pos) const
{
    return find(&cval, pos, 1);         // More efficient than find_first_of().
}

SCL_BSTRING_TEMPL
typename SCL_BSTRING_CLASS::size_type
SCL_BSTRING_CLASS::find_last_of(const Self_& cstr, size_type pos) const
{
    return find_last_of(cstr.data(), pos, cstr.size());
}

SCL_BSTRING_TEMPL
typename SCL_BSTRING_CLASS::size_type
SCL_BSTRING_CLASS::find_last_of(const Type_* cstr, size_type pos, size_type clen) const
{
    const size_type msize = this->size();
    const Type_*    mdata = this->data();

    if (clen > 0 && msize > 0)
    {
        pos = Min(msize - 1, pos);

        do
        {
            if (Traits_::find(cstr, clen, mdata[pos]) != NULL)
                return pos;
        }
        while (pos-- > 0);
    }

    return npos;
}

SCL_BSTRING_TEMPL
typename SCL_BSTRING_CLASS::size_type
SCL_BSTRING_CLASS::find_last_of(const Type_* cstr, size_type pos) const
{
    return find_last_of(cstr, pos, Traits_::length(cstr));
}

SCL_BSTRING_TEMPL
typename SCL_BSTRING_CLASS::size_type
SCL_BSTRING_CLASS::find_last_of(Type_ cval, size_type pos) const
{
    return rfind(&cval, pos, 1);        // More efficient than find_last_of().
}

SCL_BSTRING_TEMPL
typename SCL_BSTRING_CLASS::size_type
SCL_BSTRING_CLASS::find_first_not_of(const Self_& cstr, size_type pos) const
{
    return find_first_not_of(cstr.data(), pos, cstr.size());
}

SCL_BSTRING_TEMPL
typename SCL_BSTRING_CLASS::size_type
SCL_BSTRING_CLASS::find_first_not_of(const Type_* cstr, size_type pos, size_type clen) const
{
    const size_type msize = this->size();
    const Type_*    mdata = this->data();

    for (; pos < msize; ++pos)
    {
        if (Traits_::find(cstr, clen, mdata[pos]) == NULL)
            return pos;
    }

    return npos;
}

SCL_BSTRING_TEMPL
typename SCL_BSTRING_CLASS::size_type
SCL_BSTRING_CLASS::find_first_not_of(const Type_* cstr, size_type pos) const
{
    return find_first_not_of(cstr, pos, Traits_::length(cstr));
}

SCL_BSTRING_TEMPL
typename SCL_BSTRING_CLASS::size_type
SCL_BSTRING_CLASS::find_first_not_of(Type_ cval, size_type pos) const
{
    return find_first_not_of(&cval, pos, 1);
}

SCL_BSTRING_TEMPL
typename SCL_BSTRING_CLASS::size_type
SCL_BSTRING_CLASS::find_last_not_of(const Self_& cstr, size_type pos) const
{
    return find_last_not_of(cstr.data(), pos, cstr.size());
}

SCL_BSTRING_TEMPL
typename SCL_BSTRING_CLASS::size_type
SCL_BSTRING_CLASS::find_last_not_of(const Type_* cstr, size_type pos, size_type clen) const
{
    const size_type msize = this->size();
    const Type_*    mdata = this->data();

    if (msize > 0)
    {
        pos = Min(msize - 1, pos);

        do
        {
            if (Traits_::find(cstr, clen, mdata[pos]) == NULL)
                return pos;
        }
        while (pos-- > 0);
    }

    return npos;
}

SCL_BSTRING_TEMPL
typename SCL_BSTRING_CLASS::size_type
SCL_BSTRING_CLASS::find_last_not_of(const Type_* cstr, size_type pos) const
{
    return find_last_not_of(cstr, pos, Traits_::length(cstr));
}

SCL_BSTRING_TEMPL
typename SCL_BSTRING_CLASS::size_type
SCL_BSTRING_CLASS::find_last_not_of(Type_ cval, size_type pos) const
{
    return find_last_not_of(&cval, pos, 1);
}

SCL_BSTRING_TEMPL
SCL_BSTRING_CLASS
SCL_BSTRING_CLASS::substr(size_type pos, size_type len) const
{
    check_range(pos, len);
    return Self_(this->data() + pos, len);
}

SCL_BSTRING_TEMPL
int
SCL_BSTRING_CLASS::compare(const Self_& cstr) const
{
    return compare(0, this->size(), cstr.data(), cstr.size());
}

SCL_BSTRING_TEMPL
int
SCL_BSTRING_CLASS::compare(size_type pos, size_type len, const Self_& cstr) const
{
    return compare(pos, len,  cstr.data(), cstr.size());
}

SCL_BSTRING_TEMPL
int
SCL_BSTRING_CLASS::compare(size_type pos, size_type len, const Self_& cstr,
    size_type cpos, size_type clen) const
{
    cstr.check_range(cpos, clen);
    return compare(pos, len,  cstr.data() + cpos, clen);
}

SCL_BSTRING_TEMPL
int
SCL_BSTRING_CLASS::compare(const Type_* cstr) const
{
    return compare(0, this->size(), cstr, Traits_::length(cstr));
}

SCL_BSTRING_TEMPL
int
SCL_BSTRING_CLASS::compare(size_type pos, size_type len, const Type_* cstr) const
{
    return compare(pos, len,  cstr, Traits_::length(cstr));
}

SCL_BSTRING_TEMPL
int
SCL_BSTRING_CLASS::compare(size_type pos, size_type len, const Type_* cstr,
    size_type clen) const
{
    // compare [pos, pos + len) with [cstr, cstr + clen)
    check_range(pos, len);
    const size_type rlen = Min(len, clen);

    int     cres = Traits_::compare(this->data() + pos, cstr, rlen);
    if (cres == 0)
        cres = (len < clen) ? -1 : (len != clen);

    return cres;
}


#undef  SCL_BSTRING_TEMPL
#undef  SCL_BSTRING_CLASS
