/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: sclp_base_algo.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Aug-2004     Initial implementation.
//
//  NOTICE:
//      This header is used internally by the Standard C++ Library.
//      It should not be included directly in application code.
//
//==============================================================================
#ifndef _SCLP_BASE_ALGO_H_
#define _SCLP_BASE_ALGO_H_


//
//  Include section
//
#ifndef  _SCLP_BASE_ITER_H_
#include <sclp_base_iter.h>             // for _scl_value_type()
#endif

#ifndef  _SCLP_UTILITY_H_
#include <sclp_utility.h>               // for scl_pair and scl_swap()
#endif


//
//  25.1.7 scl_mismatch
//
template <class InpIter1_, class InpIter2_>
scl_pair<InpIter1_, InpIter2_>
scl_mismatch(InpIter1_ first1, InpIter1_ last1, InpIter2_ first2);

template <class InpIter1_, class InpIter2_, class Pred_>
scl_pair<InpIter1_, InpIter2_>
scl_mismatch(InpIter1_ first1, InpIter1_ last1, InpIter2_ first2, Pred_ pred);


//
//  25.1.8 scl_equal
//
template <class InpIter1_, class InpIter2_>
bool
scl_equal(InpIter1_ first1, InpIter1_ last1, InpIter2_ first2);

template <class InpIter1_, class InpIter2_, class Pred_>
bool
scl_equal(InpIter1_ first1, InpIter1_ last1, InpIter2_ first2, Pred_ pred);


//
//  25.2.1 scl_copy / scl_copy_backward
//
template <class InpIter_, class OutIter_>
OutIter_
scl_copy(InpIter_ first, InpIter_ last, OutIter_ result);

template <class BidIter1_, class BidIter2_>
BidIter2_
scl_copy_backward(BidIter1_ first, BidIter1_ last, BidIter2_ result);


//
//  25.2.2 scl_swap / scl_iter_swap
//  Note: scl_swap() is defined in <scl_config.h>
//
template <class FwdIter1_, class FwdIter2_>
void
scl_iter_swap(FwdIter1_ lhs, FwdIter2_ rhs);


//
//  25.2.5 scl_fill / scl_fill_n
//
template <class FwdIter_, class Type_>
void
scl_fill(FwdIter_ first, FwdIter_ last, const Type_& value);

template <class OutIter_, class Size_, class Type_>
void
scl_fill_n(OutIter_ first, Size_ len, const Type_& value);


//
//  25.3.7 scl_min / scl_max
//
template <class Type_>
inline
const Type_&
scl_min(const Type_& a, const Type_& b)
{
    return (b < a) ? b : a;
}

template <class Type_, class Comp_>
inline
const Type_&
scl_min(const Type_& a, const Type_& b, Comp_ comp)
{
    return comp(b, a) ? b : a;
}

template <class Type_>
inline
const Type_&
scl_max(const Type_& a, const Type_& b)
{
    return (a < b) ? b : a;
}

template <class Type_, class Comp_>
inline
const Type_&
scl_max(const Type_& a, const Type_& b, Comp_ comp)
{
    return comp(a, b) ? b : a;
}


//
//  25.3.8 scl_lexicographical_compare
//
template <class InpIter1_, class InpIter2_>
bool
scl_lexicographical_compare(InpIter1_ first1, InpIter1_ last1,
    InpIter2_ first2, InpIter2_ last2);

template <class InpIter1_, class InpIter2_, class Comp_>
bool
scl_lexicographical_compare(InpIter1_ first1, InpIter1_ last1,
    InpIter2_ first2, InpIter2_ last2, Comp_ comp);


#include <sclp_base_algo.cc>


#endif  /* _SCLP_BASE_ALGO_H_ */
