/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2008
//==============================================================================
//
//  FILE: scl_mutex.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     29-Jul-2008     Initial implementation.
//
//  NOTICE:
//      This header extends the capabilities of the Standard C++ Library.
//      It may be included directly in application code.
//
//==============================================================================
#ifndef _SCL_MUTEX_H_
#define _SCL_MUTEX_H_


//
//  Include section
//
#ifndef  _SCL_CONFIG_H_
#include <scl_config.h>
#endif

#ifndef  _SCL_STDEXCEPT_H_
#include <scl_stdexcept.h>      // for scl_resource_limit and friends
#endif

#ifndef  _SCLP_SIMPLE_MUTEX_H_
#include <sclp_simple_mutex.h>
#endif




/**
 *  Represents a simple mutual exclusion (mutex) lock.
 *
 *  @description
 *      This class implements a non-recursive mutex lock, which can be used to
 *      protect shared resources within a process.  The mutex can be acquired
 *      only once by the same thread (nested calls are not allowed).
 *      <p>
 *      In concurrent programming, two or more threads may need to access shared
 *      resource at the same time.  Examples of shared resources are global objects,
 *      reference counts, and communication queues.  A mutex grants exclusive access
 *      to the shared resource to only one thread.  If a thread acquires a mutex,
 *      other threads that attempt to acquire the mutex are suspended (blocked) until
 *      the first thread releases the mutex.
 */
class SCL_STD_EXPORT scl_mutex
{
public:
    /**
     *  Constructs a mutex lock that is dynamically allocated.
     */
    scl_mutex()
        SCL_THROWS((scl_resource_limit));

    /**
     *  Constructs a mutex lock that has static/global lifecycle.
     */
    scl_mutex(SCL_STATIC_TYPE)
        SCL_THROWS((scl_resource_limit));

    /**
     *  Destroys the mutex lock.
     */
    ~scl_mutex()
        SCL_THROWS_NONE;

    /**
     *  Attempts to acquire the mutex lock without blocking.
     *
     *  @description
     *      This method requests ownership of the mutex lock.  If the mutex is
     *      unlocked, the calling thread takes ownership of the mutex and the
     *      method returns true.  If the mutex is locked, the method immediately
     *      returns false (without blocking).  The method may be called only once
     *      by the same thread (non-recursive semantics).
     */
    bool    try_lock()
        SCL_THROWS((scl_resource_limit, scl_deadlock_error));

    /**
     *  Acquires the mutex lock while possibly blocking.
     *
     *  @description
     *      This method requests ownership of the mutex lock.  If the mutex is
     *      unlocked, the calling thread takes ownership of the mutex and the
     *      method immediately returns.  If the mutex is locked, the calling
     *      thread blocks until the mutex becomes available.  The method may be
     *      called only once by the same thread (non-recursive semantics).
     */
    void    lock()
        SCL_THROWS((scl_resource_limit, scl_deadlock_error));

    /**
     *  Releases the mutex lock.
     *
     *  @description
     *      This method relinquishes ownership of the mutex lock.  It allows a
     *      different thread to acquire the mutex, which is blocked in the lock()
     *      call.
     */
    void    unlock()
        SCL_THROWS((scl_permission_error));

private:
    // Disable copy construction and assignment.
    scl_mutex(const scl_mutex&);
    scl_mutex& operator=(const scl_mutex&);

    // Data members
    scl_simple_mutex    mutex_;         // Simple mutex object

    friend void test_scl_mutex();       // For testing purposes
};




/**
 *  Represents a guard object for a mutual exclusion (mutex) lock.
 *
 *  @description
 *      This class acquires the specified mutex lock upon creation and releases
 *      the mutex upon destruction.  It allows the safe use of mutex locks in the
 *      presence of exceptions.
 *      <p>
 *      Guard objects work in conjunction with C++ block statements to establish
 *      a critical section of code.  A guard object is typically defined at the
 *      beginning of the block, where it automatically acquires the mutex.  The
 *      mutex is automatically released when the block is exited or an exception
 *      is thrown.
 */
class SCL_STD_EXPORT scl_mutex_guard
{
public:
    /**
     *  Constructs a guard object with the given mutex lock and acquires that
     *  mutex.
     */
    scl_mutex_guard(scl_mutex& mutex)
        SCL_THROWS((scl_resource_limit, scl_deadlock_error));

    /**
     *  Destroys the guard object and releases the mutex.
     */
    ~scl_mutex_guard()
        SCL_THROWS((scl_permission_error));

private:
    // Disable copy construction and assignment.
    scl_mutex_guard(const scl_mutex_guard&);
    scl_mutex_guard& operator=(const scl_mutex_guard&);

    // Data member
    scl_mutex&          mutex_;
};


#endif  /* _SCL_MUTEX_H_ */
