/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: scl_dynamic_bitset.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      rtl     06/08/2005     Initial implementation.
//
//  NOTICE:
//      This header implements functionality in the Standard C++ Library.
//      It may be included directly in application code.
//
//==============================================================================
#ifndef _SCL_DYNAMIC_BITSET_H_
#define _SCL_DYNAMIC_BITSET_H_


#ifndef  _SCL_CONFIG_H_
#include <scl_config.h>
#endif

#include <sclp_dynamic_bitset.h>

#endif  /* _SCL_DYNAMIC_BITSET_H_ */
