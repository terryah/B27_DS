/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: scl_map.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Oct-2004     Initial implementation.
//
//  NOTICE:
//      This header implements functionality in the Standard C++ Library.
//      It may be included directly in application code.
//
//==============================================================================
#ifndef _SCL_MAP_H_
#define _SCL_MAP_H_


#ifndef  _SCL_CONFIG_H_
#include <scl_config.h>
#endif


#ifdef  SCL_USE_VENDOR_STD
#include <map>

#define scl_map                         SCL_VENDOR_STD::map
#define scl_multimap                    SCL_VENDOR_STD::multimap
#ifndef scl_swap
#define scl_swap                        SCL_VENDOR_STD::swap
#endif

#else
#include <sclp_map.h>
#include <sclp_multimap.h>

#endif  /* SCL_USE_VENDOR_STD */


#endif  /* _SCL_MAP_H_ */
