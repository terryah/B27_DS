/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: scl_fstream.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Oct-2004     Initial implementation.
//
//  NOTICE:
//      This header implements functionality in the Standard C++ Library.
//      It may be included directly in application code.
//
//==============================================================================
#ifndef _SCL_FSTREAM_H_
#define _SCL_FSTREAM_H_


#ifndef  _SCL_CONFIG_H_
#include <scl_config.h>
#endif


#ifdef  SCL_USE_STD_IOSTREAM
#include <fstream>

#ifdef  SCL_HAS_NAMESPACES
using SCL_VENDOR_STD::filebuf;
using SCL_VENDOR_STD::ifstream;
using SCL_VENDOR_STD::ofstream;
using SCL_VENDOR_STD::fstream;
using SCL_VENDOR_STD::streampos;
#endif  /* SCL_HAS_NAMESPACES */

#else
#include <fstream.h>

#endif  /* SCL_USE_STD_IOSTREAM */


#endif  /* _SCL_FSTREAM_H_ */
