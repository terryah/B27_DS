/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: sclp_string_ops.cc
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Sep-2004     Initial implementation.
//
//==============================================================================
#define SCL_BSTRING_TEMPL   template <class Type_, class Traits_, class Alloc_>
#define SCL_BSTRING_CLASS   scl_basic_string<Type_, Traits_, Alloc_>

#define SCL_BSTRING_RELOP(OPER_)                                                \
SCL_BSTRING_TEMPL                                                               \
bool                                                                            \
operator OPER_ (const SCL_BSTRING_CLASS& lhs, const SCL_BSTRING_CLASS& rhs)     \
{                                                                               \
    return (lhs.compare(rhs) OPER_ 0);                                          \
}                                                                               \
SCL_BSTRING_TEMPL                                                               \
bool                                                                            \
operator OPER_ (const Type_* lhs, const SCL_BSTRING_CLASS& rhs)                 \
{                                                                               \
    return (0 OPER_ rhs.compare(lhs));                                          \
}                                                                               \
SCL_BSTRING_TEMPL                                                               \
bool                                                                            \
operator OPER_ (const SCL_BSTRING_CLASS& lhs, const Type_* rhs)                 \
{                                                                               \
    return (lhs.compare(rhs) OPER_ 0);                                          \
}


//
//  21.3.7.1 operator+
//
SCL_BSTRING_TEMPL
SCL_BSTRING_CLASS
operator+(const SCL_BSTRING_CLASS& lhs, const SCL_BSTRING_CLASS& rhs)
{
    SCL_BSTRING_CLASS   result(lhs);
    result.append(rhs);
    return result;
}

SCL_BSTRING_TEMPL
SCL_BSTRING_CLASS
operator+(const Type_* lhs, const SCL_BSTRING_CLASS& rhs)
{
    SCL_BSTRING_CLASS   result(lhs);
    result.append(rhs);
    return result;
}

SCL_BSTRING_TEMPL
SCL_BSTRING_CLASS
operator+(Type_ lhs, const SCL_BSTRING_CLASS& rhs)
{
    SCL_BSTRING_CLASS   result(1, lhs);
    result.append(rhs);
    return result;
}

SCL_BSTRING_TEMPL
SCL_BSTRING_CLASS
operator+(const SCL_BSTRING_CLASS& lhs, const Type_* rhs)
{
    SCL_BSTRING_CLASS   result(lhs);
    result.append(rhs);
    return result;
}

SCL_BSTRING_TEMPL
SCL_BSTRING_CLASS
operator+(const SCL_BSTRING_CLASS& lhs, Type_ rhs)
{
    SCL_BSTRING_CLASS   result(lhs);
    result.append(1, rhs);
    return result;
}


//
//  21.3.7.2 operator==
//  21.3.7.3 operator!=
//  21.3.7.4 operator<
//  21.3.7.5 operator>
//  21.3.7.6 operator<=
//  21.3.7.7 operator>=
//
SCL_BSTRING_RELOP(==)
SCL_BSTRING_RELOP(!=)
SCL_BSTRING_RELOP(< )
SCL_BSTRING_RELOP(> )
SCL_BSTRING_RELOP(<=)
SCL_BSTRING_RELOP(>=)


#undef  SCL_BSTRING_TEMPL
#undef  SCL_BSTRING_CLASS
#undef  SCL_BSTRING_RELOP
