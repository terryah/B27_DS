/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: sclp_complex.cc
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Aug-2004     Initial implementation.
//
//==============================================================================


//
//  Private class _scl_complex_base
//
template <class Type_>
_scl_complex_base<Type_>::_scl_complex_base(const Type_& real, const Type_& imag) :
    real_(real),
    imag_(imag)
{
    // Nothing
}

template <class Type_>
_scl_complex_base<Type_>::_scl_complex_base(const Self_& rhs) :
    real_(rhs.real_),
    imag_(rhs.imag_)
{
    // Nothing
}

template <class Type_>
void
_scl_complex_base<Type_>::operator= (const Type_& rhs)
{
    real_ = rhs;
    imag_ = Type_(0);
}

template <class Type_>
void
_scl_complex_base<Type_>::operator+=(const Type_& rhs)
{
    real_ += rhs;
}

template <class Type_>
void
_scl_complex_base<Type_>::operator-=(const Type_& rhs)
{
    real_ -= rhs;
}

template <class Type_>
void
_scl_complex_base<Type_>::operator*=(const Type_& rhs)
{
    real_ *= rhs;
    imag_ *= rhs;
}

template <class Type_>
void
_scl_complex_base<Type_>::operator/=(const Type_& rhs)
{
    real_ /= rhs;
    imag_ /= rhs;
}

template <class Type_>
void
_scl_complex_base<Type_>::operator= (const Self_& rhs)
{
    real_ = rhs.real_;
    imag_ = rhs.imag_;
}

template <class Type_>
void
_scl_complex_base<Type_>::operator+=(const Self_& rhs)
{
    real_ += rhs.real_;
    imag_ += rhs.imag_;
}

template <class Type_>
void
_scl_complex_base<Type_>::operator-=(const Self_& rhs)
{
    real_ -= rhs.real_;
    imag_ -= rhs.imag_;
}

template <class Type_>
void
_scl_complex_base<Type_>::operator*=(const Self_& rhs)
{
    const Type_ rhs_real = rhs.real_;
    const Type_ rhs_imag = rhs.imag_;
    const Type_ real     = real_ * rhs_real - imag_ * rhs_imag;
    imag_ = real_ * rhs_imag + imag_ * rhs_real;
    real_ = real;
}

template <class Type_>
void
_scl_complex_base<Type_>::operator/=(const Self_& rhs)
{
    const Type_ rhs_real = rhs.real_;
    const Type_ rhs_imag = rhs.imag_;
    const Type_ denom    = rhs_real * rhs_real + rhs_imag * rhs_imag;
    const Type_ real     = (real_ * rhs_real + imag_ * rhs_imag) / denom;
    imag_ = (imag_ * rhs_real - real_ * rhs_imag) / denom;
    real_ = real;
}

#ifdef  SCL_HAS_MEMBER_TEMPLATES
template <class Type_>
template <class Other_>
void
_scl_complex_base<Type_>::operator= (const _scl_complex_base<Other_>& rhs)
{
    real_ = (Type_)rhs.real();
    imag_ = (Type_)rhs.imag();
}

template <class Type_>
template <class Other_>
void
_scl_complex_base<Type_>::operator+=(const _scl_complex_base<Other_>& rhs)
{
    real_ += (Type_)rhs.real();
    imag_ += (Type_)rhs.imag();
}

template <class Type_>
template <class Other_>
void
_scl_complex_base<Type_>::operator-=(const _scl_complex_base<Other_>& rhs)
{
    real_ -= (Type_)rhs.real();
    imag_ -= (Type_)rhs.imag();
}

template <class Type_>
template <class Other_>
void
_scl_complex_base<Type_>::operator*=(const _scl_complex_base<Other_>& rhs)
{
    const Type_ rhs_real = (Type_)rhs.real();
    const Type_ rhs_imag = (Type_)rhs.imag();
    const Type_ real     = real_ * rhs_real - imag_ * rhs_imag;
    imag_ = real_ * rhs_imag + imag_ * rhs_real;
    real_ = real;
}

template <class Type_>
template <class Other_>
void
_scl_complex_base<Type_>::operator/=(const _scl_complex_base<Other_>& rhs)
{
    const Type_ rhs_real = (Type_)rhs.real();
    const Type_ rhs_imag = (Type_)rhs.imag();
    const Type_ denom    = rhs_real * rhs_real + rhs_imag * rhs_imag;
    const Type_ real     = (real_ * rhs_real + imag_ * rhs_imag) / denom;
    imag_ = (imag_ * rhs_real - real_ * rhs_imag) / denom;
    real_ = real;
}
#endif  /* SCL_HAS_MEMBER_TEMPLATES */


//
//  26.2.2 template class scl_complex
//
template <class Type_>
scl_complex<Type_>::scl_complex(const Type_& real, const Type_& imag) :
    Base_(real, imag)
{
    // Nothing
}

template <class Type_>
scl_complex<Type_>::scl_complex(const Self_& rhs) :
    Base_(rhs.real_, rhs.imag_)
{
    // Nothing
}

#ifdef  SCL_HAS_MEMBER_TEMPLATES
template <class Type_>
template <class Other_>
scl_complex<Type_>::scl_complex(const scl_complex<Other_>& rhs) :
    Base_(rhs.real(), rhs.imag())
{
    // Nothing
}
#endif  /* SCL_HAS_MEMBER_TEMPLATES */

template <class Type_>
scl_complex<Type_>&
scl_complex<Type_>::operator= (const Type_& rhs)
{
    Base_::operator= (rhs);
    return *this;
}

template <class Type_>
scl_complex<Type_>&
scl_complex<Type_>::operator+=(const Type_& rhs)
{
    Base_::operator+=(rhs);
    return *this;
}

template <class Type_>
scl_complex<Type_>&
scl_complex<Type_>::operator-=(const Type_& rhs)
{
    Base_::operator-=(rhs);
    return *this;
}

template <class Type_>
scl_complex<Type_>&
scl_complex<Type_>::operator*=(const Type_& rhs)
{
    Base_::operator*=(rhs);
    return *this;
}

template <class Type_>
scl_complex<Type_>&
scl_complex<Type_>::operator/=(const Type_& rhs)
{
    Base_::operator/=(rhs);
    return *this;
}

template <class Type_>
scl_complex<Type_>&
scl_complex<Type_>::operator= (const Self_& rhs)
{
    Base_::operator= (rhs);
    return *this;
}

template <class Type_>
scl_complex<Type_>&
scl_complex<Type_>::operator+=(const Self_& rhs)
{
    Base_::operator+=(rhs);
    return *this;
}

template <class Type_>
scl_complex<Type_>&
scl_complex<Type_>::operator-=(const Self_& rhs)
{
    Base_::operator-=(rhs);
    return *this;
}

template <class Type_>
scl_complex<Type_>&
scl_complex<Type_>::operator*=(const Self_& rhs)
{
    Base_::operator*=(rhs);
    return *this;
}

template <class Type_>
scl_complex<Type_>&
scl_complex<Type_>::operator/=(const Self_& rhs)
{
    Base_::operator/=(rhs);
    return *this;
}

#ifdef  SCL_HAS_MEMBER_TEMPLATES
template <class Type_>
template <class Other_>
scl_complex<Type_>&
scl_complex<Type_>::operator= (const scl_complex<Other_>& rhs)
{
    Base_::operator= (rhs);
    return *this;
}

template <class Type_>
template <class Other_>
scl_complex<Type_>&
scl_complex<Type_>::operator+=(const scl_complex<Other_>& rhs)
{
    Base_::operator+=(rhs);
    return *this;
}

template <class Type_>
template <class Other_>
scl_complex<Type_>&
scl_complex<Type_>::operator-=(const scl_complex<Other_>& rhs)
{
    Base_::operator-=(rhs);
    return *this;
}

template <class Type_>
template <class Other_>
scl_complex<Type_>&
scl_complex<Type_>::operator*=(const scl_complex<Other_>& rhs)
{
    Base_::operator*=(rhs);
    return *this;
}

template <class Type_>
template <class Other_>
scl_complex<Type_>&
scl_complex<Type_>::operator/=(const scl_complex<Other_>& rhs)
{
    Base_::operator/=(rhs);
    return *this;
}
#endif  /* SCL_HAS_MEMBER_TEMPLATES */


//
//  26.2.6 Operators
//
template <class Type_>
scl_complex<Type_>
operator+(const scl_complex<Type_>& lhs, const scl_complex<Type_>& rhs)
{
    return scl_complex<Type_>(lhs.real() + rhs.real(), lhs.imag() + rhs.imag());
}

template <class Type_>
scl_complex<Type_>
operator+(const scl_complex<Type_>& lhs, const Type_& rhs)
{
    return scl_complex<Type_>(lhs.real() + rhs, lhs.imag());
}

template <class Type_>
scl_complex<Type_>
operator+(const Type_& lhs, const scl_complex<Type_>& rhs)
{
    return scl_complex<Type_>(lhs + rhs.real(), rhs.imag());
}

template <class Type_>
scl_complex<Type_>
operator-(const scl_complex<Type_>& lhs, const scl_complex<Type_>& rhs)
{
    return scl_complex<Type_>(lhs.real() - rhs.real(), lhs.imag() - rhs.imag());
}

template <class Type_>
scl_complex<Type_>
operator-(const scl_complex<Type_>& lhs, const Type_& rhs)
{
    return scl_complex<Type_>(lhs.real() - rhs, lhs.imag());
}

template <class Type_>
scl_complex<Type_>
operator-(const Type_& lhs, const scl_complex<Type_>& rhs)
{
    return scl_complex<Type_>(lhs - rhs.real(), -rhs.imag());
}

template <class Type_>
scl_complex<Type_>
operator*(const scl_complex<Type_>& lhs, const scl_complex<Type_>& rhs)
{
    scl_complex<Type_>  tmp(lhs);
    return (tmp *= rhs);
}

template <class Type_>
scl_complex<Type_>
operator*(const scl_complex<Type_>& lhs, const Type_& rhs)
{
    return scl_complex<Type_>(lhs.real() * rhs, lhs.imag() * rhs);
}

template <class Type_>
scl_complex<Type_>
operator*(const Type_& lhs, const scl_complex<Type_>& rhs)
{
    return scl_complex<Type_>(lhs * rhs.real(), lhs * rhs.imag());
}

template <class Type_>
scl_complex<Type_>
operator/(const scl_complex<Type_>& lhs, const scl_complex<Type_>& rhs)
{
    scl_complex<Type_>  tmp(lhs);
    return (tmp /= rhs);
}

template <class Type_>
scl_complex<Type_>
operator/(const scl_complex<Type_>& lhs, const Type_& rhs)
{
    return scl_complex<Type_>(lhs.real() / rhs, lhs.imag() / rhs);
}

template <class Type_>
scl_complex<Type_>
operator/(const Type_& lhs, const scl_complex<Type_>& rhs)
{
    scl_complex<Type_>  tmp(lhs);
    return (tmp /= rhs);
}

template <class Type_>
scl_complex<Type_>
operator+(const scl_complex<Type_>& rhs)
{
    return rhs;
}

template <class Type_>
scl_complex<Type_>
operator-(const scl_complex<Type_>& rhs)
{
    return scl_complex<Type_>(-rhs.real(), -rhs.imag());
}

template <class Type_>
bool
operator==(const scl_complex<Type_>& lhs, const scl_complex<Type_>& rhs)
{
    return (lhs.real() == rhs.real() && lhs.imag() == rhs.imag());
}

template <class Type_>
bool
operator==(const scl_complex<Type_>& lhs, const Type_& rhs)
{
    return (lhs.real() == rhs && lhs.imag() == Type_(0));
}

template <class Type_>
bool
operator==(const Type_& lhs, const scl_complex<Type_>& rhs)
{
    return (lhs == rhs.real() && Type_(0) == rhs.imag());
}

template <class Type_>
bool
operator!=(const scl_complex<Type_>& lhs, const scl_complex<Type_>& rhs)
{
    return (lhs.real() != rhs.real() || lhs.imag() != rhs.imag());
}

template <class Type_>
bool
operator!=(const scl_complex<Type_>& lhs, const Type_& rhs)
{
    return (lhs.real() != rhs || lhs.imag() != Type_(0));
}

template <class Type_>
bool
operator!=(const Type_& lhs, const scl_complex<Type_>& rhs)
{
    return (lhs != rhs.real() || Type_(0) != rhs.imag());
}

#ifdef  SCL_ENABLE_IO
template <class Type_>
istream&
operator>>(istream& is, scl_complex<Type_>& rhs)
{
    Type_   real = Type_(0);
    Type_   imag = Type_(0);
    char    ch   = '\0';

    if (!(is >> ch))
        return is;

    if (ch == '(')
    {
        is >> real >> ch;

        if (ch == ',')
        {
            is >> imag >> ch;
        }

        if (ch != ')')
        {
            is.clear(ios::failbit);
            return is;
        }
    }
    else if (is)
    {
        is.putback(ch);
        is >> real;
    }

    if (is)
        rhs = scl_complex<Type_>(real, imag);

    return is;
}

template <class Type_>
ostream&
operator<<(ostream& os, const scl_complex<Type_>& rhs)
{
    return os << "(" << rhs.real() << "," << rhs.imag() << ")";
}
#endif  /* SCL_ENABLE_IO */


//
//  26.2.7 Values
//
template <class Type_>
Type_
real(const scl_complex<Type_>& rhs)
{
    return rhs.real();
}

template <class Type_>
Type_
imag(const scl_complex<Type_>& rhs)
{
    return rhs.imag();
}

template <class Type_>
Type_
abs(const scl_complex<Type_>& rhs)
{
//  const Type_ result = ::sqrt(rhs.real() * rhs.real() + rhs.imag() * rhs.imag());
    const Type_ result = ::hypot(rhs.real(), rhs.imag());
    return result;
}

template <class Type_>
Type_
arg(const scl_complex<Type_>& rhs)
{
    return ::atan2(rhs.imag(), rhs.real());
}

template <class Type_>
Type_
norm(const scl_complex<Type_>& rhs)
{
    return (rhs.real() * rhs.real() + rhs.imag() * rhs.imag());
}

template <class Type_>
scl_complex<Type_>
conj(const scl_complex<Type_>& rhs)
{
    return scl_complex<Type_>(rhs.real(), -rhs.imag());
}

template <class Type_>
scl_complex<Type_>
polar(const Type_& rho, const Type_& theta)
{
    return scl_complex<Type_>(rho * ::cos(theta), rho * ::sin(theta));
}


//
//  26.2.8 Transcendentals
//
template <class Type_>
scl_complex<Type_>
cos(const scl_complex<Type_>& rhs)
{
    const Type_ real =  ::cos(rhs.real()) * ::cosh(rhs.imag());
    const Type_ imag = -::sin(rhs.real()) * ::sinh(rhs.imag());
    return scl_complex<Type_>(real, imag);
}

template <class Type_>
scl_complex<Type_>
cosh(const scl_complex<Type_>& rhs)
{
    const Type_ real = ::cosh(rhs.real()) * ::cos(rhs.imag());
    const Type_ imag = ::sinh(rhs.real()) * ::sin(rhs.imag());
    return scl_complex<Type_>(real, imag);
}

template <class Type_>
scl_complex<Type_>
exp(const scl_complex<Type_>& rhs)
{
    const Type_ rexp = ::exp(rhs.real());
    const Type_ real = rexp * ::cos(rhs.imag());
    const Type_ imag = rexp * ::sin(rhs.imag());
    return scl_complex<Type_>(real, imag);
}

template <class Type_>
scl_complex<Type_>
log(const scl_complex<Type_>& rhs)
{
    const Type_ real = ::log(abs(rhs));
    const Type_ imag = arg(rhs);
    return scl_complex<Type_>(real, imag);
}

template <class Type_>
scl_complex<Type_>
log10(const scl_complex<Type_>& rhs)
{
    const Type_ real = ::log10(abs(rhs));
    const Type_ imag = arg(rhs) / Type_(SCL_M_LN10);
    return scl_complex<Type_>(real, imag);
}

template <class Type_>
scl_complex<Type_>
pow(const scl_complex<Type_>& rhs, int pwr)
{
    if (rhs._is_real())
    {
        const Type_ real = ::pow(rhs.real(), Type_(pwr));
        return scl_complex<Type_>(real);
    }
    else
    {
        return exp(Type_(pwr) * log(rhs));
    }
}

template <class Type_>
scl_complex<Type_>
pow(const scl_complex<Type_>& rhs, const Type_& pwr)
{
    if (rhs._is_real() && rhs.real() > Type_(0))
    {
        const Type_ real = ::pow(rhs.real(), pwr);
        return scl_complex<Type_>(real);
    }
    else
    {
        return exp(pwr * log(rhs));
    }
}

template <class Type_>
scl_complex<Type_>
pow(const scl_complex<Type_>& rhs, const scl_complex<Type_>& pwr)
{
    if (pwr._is_real())
    {
        return pow(rhs, pwr.real());
    }
    else if (rhs._is_real())
    {
        return pow(rhs.real(), pwr);
    }
    else
    {
        return exp(pwr * log(rhs));
    }
}

template <class Type_>
scl_complex<Type_>
pow(const Type_& rhs, const scl_complex<Type_>& pwr)
{
    if (pwr._is_real() && rhs > Type_(0))
    {
        const Type_ real = ::pow(rhs, pwr.real());
        return scl_complex<Type_>(real);
    }
    else if (rhs > Type_(0))
    {
        const Type_ rlog = ::log(rhs);
        return exp(pwr * rlog);
    }
    else
    {
        const scl_complex<Type_> rcmp(rhs);
        return exp(pwr * log(rcmp));
    }
}

template <class Type_>
scl_complex<Type_>
sin(const scl_complex<Type_>& rhs)
{
    const Type_ real = ::sin(rhs.real()) * ::cosh(rhs.imag());
    const Type_ imag = ::cos(rhs.real()) * ::sinh(rhs.imag());
    return scl_complex<Type_>(real, imag);
}

template <class Type_>
scl_complex<Type_>
sinh(const scl_complex<Type_>& rhs)
{
    const Type_ real = ::sinh(rhs.real()) * ::cos(rhs.imag());
    const Type_ imag = ::cosh(rhs.real()) * ::sin(rhs.imag());
    return scl_complex<Type_>(real, imag);
}

template <class Type_>
scl_complex<Type_>
sqrt(const scl_complex<Type_>& rhs)
{
    const Type_ r_real = rhs.real();
    const Type_ r_imag = rhs.imag();
    const Type_ r_mag  = abs(rhs);
    Type_       real;
    Type_       imag;

    if (r_mag == Type_(0))
    {
        real = Type_(0);
        imag = Type_(0);
    }
    else if (r_real > Type_(0))
    {
        real = ::sqrt((r_mag + r_real) / 2);
        imag = r_imag / real / 2;
    }
    else
    {
        imag = ::sqrt((r_mag - r_real) / 2);
        if (r_imag < Type_(0))
            imag = -imag;
        real = r_imag / imag / 2;
    }

    return scl_complex<Type_>(real, imag);
}

template <class Type_>
scl_complex<Type_>
tan(const scl_complex<Type_>& rhs)
{
    const Type_ real2 = Type_(2) * rhs.real();
    const Type_ imag2 = Type_(2) * rhs.imag();
    const Type_ denom = ::cos(real2) + ::cosh(imag2);
    return scl_complex<Type_>(::sin(real2) / denom, ::sinh(imag2) / denom);
}

template <class Type_>
scl_complex<Type_>
tanh(const scl_complex<Type_>& rhs)
{
    const Type_ real2 = Type_(2) * rhs.real();
    const Type_ imag2 = Type_(2) * rhs.imag();
    const Type_ denom = ::cosh(real2) + ::cos(imag2);
    return scl_complex<Type_>(::sinh(real2) / denom, ::sin(imag2) / denom);
}


//
//  Specialization of scl_complex<float> .
//  Note: The following code is duplicated in sclp_complex.cpp .
//
#ifdef  SCL_HAS_MEMBER_TEMPLATES
SCL_SPECIALIZE_FUNC
template <class Other_>
scl_complex<float>&
scl_complex<float>::operator= (const scl_complex<Other_>& rhs)
{
    Base_::operator= (rhs);
    return *this;
}

SCL_SPECIALIZE_FUNC
template <class Other_>
scl_complex<float>&
scl_complex<float>::operator+=(const scl_complex<Other_>& rhs)
{
    Base_::operator+=(rhs);
    return *this;
}

SCL_SPECIALIZE_FUNC
template <class Other_>
scl_complex<float>&
scl_complex<float>::operator-=(const scl_complex<Other_>& rhs)
{
    Base_::operator-=(rhs);
    return *this;
}

SCL_SPECIALIZE_FUNC
template <class Other_>
scl_complex<float>&
scl_complex<float>::operator*=(const scl_complex<Other_>& rhs)
{
    Base_::operator*=(rhs);
    return *this;
}

SCL_SPECIALIZE_FUNC
template <class Other_>
scl_complex<float>&
scl_complex<float>::operator/=(const scl_complex<Other_>& rhs)
{
    Base_::operator/=(rhs);
    return *this;
}
#endif  /* SCL_HAS_MEMBER_TEMPLATES */


//
//  Specialization of scl_complex<double> .
//  Note: The following code is duplicated in sclp_complex.cpp .
//
#ifdef  SCL_HAS_MEMBER_TEMPLATES
SCL_SPECIALIZE_FUNC
template <class Other_>
scl_complex<double>&
scl_complex<double>::operator= (const scl_complex<Other_>& rhs)
{
    Base_::operator= (rhs);
    return *this;
}

SCL_SPECIALIZE_FUNC
template <class Other_>
scl_complex<double>&
scl_complex<double>::operator+=(const scl_complex<Other_>& rhs)
{
    Base_::operator+=(rhs);
    return *this;
}

SCL_SPECIALIZE_FUNC
template <class Other_>
scl_complex<double>&
scl_complex<double>::operator-=(const scl_complex<Other_>& rhs)
{
    Base_::operator-=(rhs);
    return *this;
}

SCL_SPECIALIZE_FUNC
template <class Other_>
scl_complex<double>&
scl_complex<double>::operator*=(const scl_complex<Other_>& rhs)
{
    Base_::operator*=(rhs);
    return *this;
}

SCL_SPECIALIZE_FUNC
template <class Other_>
scl_complex<double>&
scl_complex<double>::operator/=(const scl_complex<Other_>& rhs)
{
    Base_::operator/=(rhs);
    return *this;
}
#endif  /* SCL_HAS_MEMBER_TEMPLATES */


//
//  Specialization of scl_complex<long double> .
//  Note: The following code is duplicated in sclp_complex.cpp .
//
#ifdef  SCL_HAS_MEMBER_TEMPLATES
SCL_SPECIALIZE_FUNC
template <class Other_>
scl_complex<long double>&
scl_complex<long double>::operator= (const scl_complex<Other_>& rhs)
{
    Base_::operator= (rhs);
    return *this;
}

SCL_SPECIALIZE_FUNC
template <class Other_>
scl_complex<long double>&
scl_complex<long double>::operator+=(const scl_complex<Other_>& rhs)
{
    Base_::operator+=(rhs);
    return *this;
}

SCL_SPECIALIZE_FUNC
template <class Other_>
scl_complex<long double>&
scl_complex<long double>::operator-=(const scl_complex<Other_>& rhs)
{
    Base_::operator-=(rhs);
    return *this;
}

SCL_SPECIALIZE_FUNC
template <class Other_>
scl_complex<long double>&
scl_complex<long double>::operator*=(const scl_complex<Other_>& rhs)
{
    Base_::operator*=(rhs);
    return *this;
}

SCL_SPECIALIZE_FUNC
template <class Other_>
scl_complex<long double>&
scl_complex<long double>::operator/=(const scl_complex<Other_>& rhs)
{
    Base_::operator/=(rhs);
    return *this;
}
#endif  /* SCL_HAS_MEMBER_TEMPLATES */
