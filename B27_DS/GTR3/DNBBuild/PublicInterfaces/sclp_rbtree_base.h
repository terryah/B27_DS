/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
//
//  FILE: sclp_rbtree_base.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Apr-2005     Initial implementation.
//
//  NOTICE:
//      This header is used internally by the Standard C++ Library.
//      It should not be included directly in application code.
//
//==============================================================================
#ifndef _SCLP_RBTREE_BASE_H_
#define _SCLP_RBTREE_BASE_H_


//
//  Include section
//
#ifndef  _SCLP_BASE_ITER_H_
#include <sclp_base_iter.h>             // for scl_iterator and friends
#endif


//
//  Class _scl_rbtree_base_node
//  This class represents the base class for nodes in a red-black tree.  To reduce
//  code bloat, common tree operations are placed in this non-templated class.
//
class SCL_STD_EXPORT _scl_rbtree_base_node
{
public:
    typedef _scl_rbtree_base_node   Self_;
    typedef unsigned char           Byte_;

    enum color_type { Red, Black };

public:
    // Tree-manipulation routines.
    static void         Initialize(Self_* node);
    static void         DestroyNil(Self_* node);

    static       Self_* Increment(      Self_* x_node);
    static const Self_* Increment(const Self_* x_node);

    static       Self_* Decrement(      Self_* x_node);
    static const Self_* Decrement(const Self_* x_node);

    static       Self_* Minimum(      Self_* x_node, Self_* head);
    static const Self_* Minimum(const Self_* x_node, Self_* head);

    static       Self_* Maximum(      Self_* x_node, Self_* head);
    static const Self_* Maximum(const Self_* x_node, Self_* head);

    static void         RotateLeft (Self_* x_node, Self_*& root);
    static void         RotateRight(Self_* x_node, Self_*& root);

    static void         Insert(bool to_left, Self_* x_node, Self_* y_node, Self_* z_node,
        Self_* head, Self_* nil);

    static Self_*       Erase(Self_* z_node, Self_* head, const Self_* nil);

    static size_t       CountBlack(const Self_* x_node, const Self_* root, const Self_* nil);

    // Data members.
    Self_*      parent_;
    Self_*      left_;
    Self_*      right_;
    Byte_       color_;
    Byte_       is_nil_;
};


//
//  Template class _scl_rbtree_base_node
//  This class represents a node in a red-black tree.
//
template <class Value_>
class _scl_rbtree_node : public _scl_rbtree_base_node
{
public:
    typedef _scl_rbtree_node<Value_>    Self_;

    Value_      data_;

private:
    // Disable copy and assignment operations.
    _scl_rbtree_node(const Self_&)  {};
    void   operator=(const Self_&)  {};
};


//
//  Template class _scl_rbtree_iterator
//
template <class Value_>
class _scl_rbtree_iterator : public
    scl_iterator<scl_bidirectional_iterator_tag, Value_, ptrdiff_t, Value_*, Value_&>
{
public:
    // Private types
    typedef
    scl_iterator<scl_bidirectional_iterator_tag, Value_, ptrdiff_t, Value_*, Value_&>
                                                Base_;
    typedef _scl_rbtree_iterator<Value_>        Self_;
    typedef _scl_rbtree_base_node               BNode_;
    typedef _scl_rbtree_node<Value_>            VNode_;

    // Public types
    SCL_ITERATOR_TYPEDEFS(Base_);

    _scl_rbtree_iterator() :
        node_(NULL)
    {
        // Nothing
    }

    _scl_rbtree_iterator(BNode_* node) :
        node_(node)
    {
        // Nothing
    }

    reference operator*() const
    {
        return  static_cast<VNode_*>(node_)->data_;
    }

    pointer operator->() const
    {
        return &static_cast<VNode_*>(node_)->data_;
    }

    Self_& operator++()
    {
        node_ = BNode_::Increment(node_);
        return *this;
    }

    Self_  operator++(int)
    {
        Self_   tmp(*this);
        node_ = BNode_::Increment(node_);
        return tmp;
    }

    Self_& operator--()
    {
        node_ = BNode_::Decrement(node_);
        return *this;
    }

    Self_  operator--(int)
    {
        Self_   tmp(*this);
        node_ = BNode_::Decrement(node_);
        return tmp;
    }

    bool operator==(const Self_& other) const
    {
        return (node_ == other.node_);
    }

    bool operator!=(const Self_& other) const
    {
        return (node_ != other.node_);
    }

    // Note: Data member is exposed to eliminate messy friend declarations.
public:
    BNode_*     node_;
};


//
//  Template class _scl_rbtree_const_iterator
//
template <class Value_>
class _scl_rbtree_const_iterator : public
    scl_iterator<scl_bidirectional_iterator_tag, Value_, ptrdiff_t, const Value_*, const Value_&>
{
public:
    // Private types
    typedef
    scl_iterator<scl_bidirectional_iterator_tag, Value_, ptrdiff_t, const Value_*, const Value_&>
                                                Base_;
    typedef _scl_rbtree_const_iterator<Value_>  Self_;
    typedef _scl_rbtree_base_node               BNode_;
    typedef _scl_rbtree_node<Value_>            VNode_;

    // Public types
    typedef _scl_rbtree_iterator<Value_>        iterator;
    SCL_ITERATOR_TYPEDEFS(Base_);

    _scl_rbtree_const_iterator() :
        node_(NULL)
    {
        // Nothing
    }

    _scl_rbtree_const_iterator(const BNode_* node) :
        node_(node)
    {
        // Nothing
    }

    _scl_rbtree_const_iterator(const iterator& other) :
        node_(other.node_)
    {
        // Nothing
    }

    reference operator*() const
    {
        return  static_cast<const VNode_*>(node_)->data_;
    }

    pointer operator->() const
    {
        return &static_cast<const VNode_*>(node_)->data_;
    }

    Self_& operator++()
    {
        node_ = BNode_::Increment(node_);
        return *this;
    }

    Self_  operator++(int)
    {
        Self_   tmp(*this);
        node_ = BNode_::Increment(node_);
        return tmp;
    }

    Self_& operator--()
    {
        node_ = BNode_::Decrement(node_);
        return *this;
    }

    Self_  operator--(int)
    {
        Self_   tmp(*this);
        node_ = BNode_::Decrement(node_);
        return tmp;
    }

    bool operator==(const Self_& other) const
    {
        return (node_ == other.node_);
    }

    bool operator!=(const Self_& other) const
    {
        return (node_ != other.node_);
    }

    // Note: Data member is exposed to eliminate messy friend declarations.
public:
    const BNode_*   node_;
};


template <class Value_>
bool
operator==(const _scl_rbtree_iterator      <Value_>& lhs,
           const _scl_rbtree_const_iterator<Value_>& rhs)
{
    return (lhs.node_ == rhs.node_);
}

template <class Value_>
bool
operator!=(const _scl_rbtree_iterator      <Value_>& lhs,
           const _scl_rbtree_const_iterator<Value_>& rhs)
{
    return (lhs.node_ != rhs.node_);
}


#endif  /* _SCLP_RBTREE_BASE_H_ */
