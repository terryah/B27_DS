/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: sclp_construct.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Aug-2004     Initial implementation.
//
//  NOTICE:
//      This header is used internally by the Standard C++ Library.
//      It should not be included directly in application code.
//
//==============================================================================
#ifndef _SCLP_CONSTRUCT_H_
#define _SCLP_CONSTRUCT_H_


//
//  Include section
//
#ifndef  _SCL_NEW_H_
#include <scl_new.h>                    // for placement new
#endif


//
//  Private utilities
//
template <class Type_, class Other_>
inline void
_scl_construct(Type_* ptr, const Other_& val)
{
    ::new ((void *) ptr) Type_(val);
}

template <class Type_>
inline void
_scl_construct(Type_* ptr)
{
    ::new ((void *) ptr) Type_();
}

template <class Type_>
inline void
_scl_destroy(Type_* ptr)
{
    ptr->~Type_();
}

template <class FwdIter_>
void
_scl_destroy(FwdIter_ first, FwdIter_ last)
{
    for (; first != last; ++first)
        _scl_destroy(&*first);
}

template <class FwdIter_, class Alloc_>
void
_scl_destroy(FwdIter_ first, FwdIter_ last, Alloc_ &alloc)
{
    for (; first != last; ++first)
        alloc.destroy(&*first);
}


#include <sclp_construct.cc>


#endif  /* _SCLP_CONSTRUCT_H_ */
