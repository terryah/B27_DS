/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: sclp_limits.cc
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Aug-2004     Initial implementation.
//
//==============================================================================
#define SCL_LIMITS_TEMPL    template <class Type_>
#define SCL_LIMITS_CLASS    scl_numeric_limits<Type_>

SCL_LIMITS_TEMPL const bool SCL_LIMITS_CLASS::is_specialized    = false;
SCL_LIMITS_TEMPL const int  SCL_LIMITS_CLASS::digits            = 0;
SCL_LIMITS_TEMPL const int  SCL_LIMITS_CLASS::digits10          = 0;
SCL_LIMITS_TEMPL const bool SCL_LIMITS_CLASS::is_signed         = false;
SCL_LIMITS_TEMPL const bool SCL_LIMITS_CLASS::is_integer        = false;
SCL_LIMITS_TEMPL const bool SCL_LIMITS_CLASS::is_exact          = false;
SCL_LIMITS_TEMPL const int  SCL_LIMITS_CLASS::radix             = 0;
SCL_LIMITS_TEMPL const int  SCL_LIMITS_CLASS::min_exponent      = 0;
SCL_LIMITS_TEMPL const int  SCL_LIMITS_CLASS::min_exponent10    = 0;
SCL_LIMITS_TEMPL const int  SCL_LIMITS_CLASS::max_exponent      = 0;
SCL_LIMITS_TEMPL const int  SCL_LIMITS_CLASS::max_exponent10    = 0;
SCL_LIMITS_TEMPL const bool SCL_LIMITS_CLASS::has_infinity      = false;
SCL_LIMITS_TEMPL const bool SCL_LIMITS_CLASS::has_quiet_NaN     = false;
SCL_LIMITS_TEMPL const bool SCL_LIMITS_CLASS::has_signaling_NaN = false;
SCL_LIMITS_TEMPL const bool SCL_LIMITS_CLASS::has_denorm_loss   = false;
SCL_LIMITS_TEMPL const bool SCL_LIMITS_CLASS::is_iec559         = false;
SCL_LIMITS_TEMPL const bool SCL_LIMITS_CLASS::is_bounded        = false;
SCL_LIMITS_TEMPL const bool SCL_LIMITS_CLASS::is_modulo         = false;
SCL_LIMITS_TEMPL const bool SCL_LIMITS_CLASS::traps             = false;
SCL_LIMITS_TEMPL const bool SCL_LIMITS_CLASS::tinyness_before   = false;
SCL_LIMITS_TEMPL const scl_float_denorm_style
                            SCL_LIMITS_CLASS::has_denorm        = scl_denorm_absent;
SCL_LIMITS_TEMPL const scl_float_round_style
                            SCL_LIMITS_CLASS::round_style       = scl_round_toward_zero;

SCL_LIMITS_TEMPL Type_      SCL_LIMITS_CLASS::min()           SCL_NOTHROW { return Type_(0); }
SCL_LIMITS_TEMPL Type_      SCL_LIMITS_CLASS::max()           SCL_NOTHROW { return Type_(0); }
SCL_LIMITS_TEMPL Type_      SCL_LIMITS_CLASS::epsilon()       SCL_NOTHROW { return Type_(0); }
SCL_LIMITS_TEMPL Type_      SCL_LIMITS_CLASS::round_error()   SCL_NOTHROW { return Type_(0); }
SCL_LIMITS_TEMPL Type_      SCL_LIMITS_CLASS::infinity()      SCL_NOTHROW { return Type_(0); }
SCL_LIMITS_TEMPL Type_      SCL_LIMITS_CLASS::quiet_NaN()     SCL_NOTHROW { return Type_(0); }
SCL_LIMITS_TEMPL Type_      SCL_LIMITS_CLASS::signaling_NaN() SCL_NOTHROW { return Type_(0); }
SCL_LIMITS_TEMPL Type_      SCL_LIMITS_CLASS::denorm_min()    SCL_NOTHROW { return Type_(0); }

#undef  SCL_LIMITS_TEMPL
#undef  SCL_LIMITS_CLASS
