/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: sclp_complex.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Aug-2004     Initial implementation.
//
//  NOTICE:
//      This header is used internally by the Standard C++ Library.
//      It should not be included directly in application code.
//
//==============================================================================
#ifndef _SCLP_COMPLEX_H_
#define _SCLP_COMPLEX_H_


//
//  Include section
//
#ifndef  _SCL_CMATH_H_
#include <scl_cmath.h>
#endif

#ifdef  SCL_ENABLE_IO
#include <scl_iostream.h>               // for istream, ostream
#endif


//
//  Forward declarations
//
template <class Type_> class scl_complex;
SCL_SPECIALIZE_CLASS class SCL_STD_EXPORT scl_complex<float>;
SCL_SPECIALIZE_CLASS class SCL_STD_EXPORT scl_complex<double>;
SCL_SPECIALIZE_CLASS class SCL_STD_EXPORT scl_complex<long double>;


//
//  Internal class _scl_complex_base
//
template <class Type_>
class _scl_complex_base
{
public:
    typedef _scl_complex_base<Type_>    Self_;

    Type_   real() const { return real_; }
    Type_   imag() const { return imag_; }

    // Non-standard helper functions.
    bool    _is_zero() const { return (real_ == Type_(0) && imag_ == Type_(0)); }
    bool    _is_real() const { return (imag_ == Type_(0)); }

protected:
    _scl_complex_base(const Type_& real, const Type_& imag);
    _scl_complex_base(const Self_& rhs);

    void    operator= (const Type_& rhs);
    void    operator+=(const Type_& rhs);
    void    operator-=(const Type_& rhs);
    void    operator*=(const Type_& rhs);
    void    operator/=(const Type_& rhs);

    void    operator= (const Self_& rhs);
    void    operator+=(const Self_& rhs);
    void    operator-=(const Self_& rhs);
    void    operator*=(const Self_& rhs);
    void    operator/=(const Self_& rhs);

#ifdef  SCL_HAS_MEMBER_TEMPLATES
    template <class Other_> void    operator= (const _scl_complex_base<Other_>& rhs);
    template <class Other_> void    operator+=(const _scl_complex_base<Other_>& rhs);
    template <class Other_> void    operator-=(const _scl_complex_base<Other_>& rhs);
    template <class Other_> void    operator*=(const _scl_complex_base<Other_>& rhs);
    template <class Other_> void    operator/=(const _scl_complex_base<Other_>& rhs);
#endif

    // Data members
    Type_   real_;
    Type_   imag_;
};


//
//  26.2.2 template class scl_complex
//
template <class Type_>
class scl_complex : public _scl_complex_base<Type_>
{
public:
    typedef scl_complex<Type_>          Self_;
    typedef _scl_complex_base<Type_>    Base_;
    typedef Type_                       value_type;

    scl_complex(const Type_& real = Type_(0), const Type_& imag = Type_(0));
    scl_complex(const Self_& rhs);

#ifdef  SCL_HAS_MEMBER_TEMPLATES
    template <class Other_>
    scl_complex(const scl_complex<Other_>& rhs);
#endif

//  Type_   real() const;
//  Type_   imag() const;

    Self_&  operator= (const Type_& rhs);
    Self_&  operator+=(const Type_& rhs);
    Self_&  operator-=(const Type_& rhs);
    Self_&  operator*=(const Type_& rhs);
    Self_&  operator/=(const Type_& rhs);

    Self_&  operator= (const Self_& rhs);
    Self_&  operator+=(const Self_& rhs);
    Self_&  operator-=(const Self_& rhs);
    Self_&  operator*=(const Self_& rhs);
    Self_&  operator/=(const Self_& rhs);

#ifdef  SCL_HAS_MEMBER_TEMPLATES
    template <class Other_> Self_&  operator= (const scl_complex<Other_>& rhs);
    template <class Other_> Self_&  operator+=(const scl_complex<Other_>& rhs);
    template <class Other_> Self_&  operator-=(const scl_complex<Other_>& rhs);
    template <class Other_> Self_&  operator*=(const scl_complex<Other_>& rhs);
    template <class Other_> Self_&  operator/=(const scl_complex<Other_>& rhs);
#endif
};


//
//  26.2.6 Operators
//

// Addition
template <class Type_>
scl_complex<Type_>
operator+(const scl_complex<Type_>& lhs, const scl_complex<Type_>& rhs);

template <class Type_>
scl_complex<Type_>
operator+(const scl_complex<Type_>& lhs, const Type_& rhs);

template <class Type_>
scl_complex<Type_>
operator+(const Type_& lhs, const scl_complex<Type_>& rhs);

// Subtraction
template <class Type_>
scl_complex<Type_>
operator-(const scl_complex<Type_>& lhs, const scl_complex<Type_>& rhs);

template <class Type_>
scl_complex<Type_>
operator-(const scl_complex<Type_>& lhs, const Type_& rhs);

template <class Type_>
scl_complex<Type_>
operator-(const Type_& lhs, const scl_complex<Type_>& rhs);

// Multiplication
template <class Type_>
scl_complex<Type_>
operator*(const scl_complex<Type_>& lhs, const scl_complex<Type_>& rhs);

template <class Type_>
scl_complex<Type_>
operator*(const scl_complex<Type_>& lhs, const Type_& rhs);

template <class Type_>
scl_complex<Type_>
operator*(const Type_& lhs, const scl_complex<Type_>& rhs);

// Division
template <class Type_>
scl_complex<Type_>
operator/(const scl_complex<Type_>& lhs, const scl_complex<Type_>& rhs);

template <class Type_>
scl_complex<Type_>
operator/(const scl_complex<Type_>& lhs, const Type_& rhs);

template <class Type_>
scl_complex<Type_>
operator/(const Type_& lhs, const scl_complex<Type_>& rhs);

// Unary
template <class Type_>
scl_complex<Type_>
operator+(const scl_complex<Type_>& rhs);

template <class Type_>
scl_complex<Type_>
operator-(const scl_complex<Type_>& rhs);

// Equality
template <class Type_>
bool
operator==(const scl_complex<Type_>& lhs, const scl_complex<Type_>& rhs);

template <class Type_>
bool
operator==(const scl_complex<Type_>& lhs, const Type_& rhs);

template <class Type_>
bool
operator==(const Type_& lhs, const scl_complex<Type_>& rhs);

// Inequality
template <class Type_>
bool
operator!=(const scl_complex<Type_>& lhs, const scl_complex<Type_>& rhs);

template <class Type_>
bool
operator!=(const scl_complex<Type_>& lhs, const Type_& rhs);

template <class Type_>
bool
operator!=(const Type_& lhs, const scl_complex<Type_>& rhs);

// Input/Output
#ifdef  SCL_ENABLE_IO

// Read complex number with format: "real", "(real)", or "(real, imag)" .
template <class Type_>
istream&
operator>>(istream& is, scl_complex<Type_>& rhs);

// Write complex number with format: "(real, imag)" .
template <class Type_>
ostream&
operator<<(ostream& os, const scl_complex<Type_>& rhs);

#endif  /* SCL_ENABLE_IO */


//
//  26.2.7 Values
//
template <class Type_>
Type_
real(const scl_complex<Type_>& rhs);

template <class Type_>
Type_
imag(const scl_complex<Type_>& rhs);

template <class Type_>
Type_
abs(const scl_complex<Type_>& rhs);

template <class Type_>
Type_
arg(const scl_complex<Type_>& rhs);

template <class Type_>
Type_
norm(const scl_complex<Type_>& rhs);

template <class Type_>
scl_complex<Type_>
conj(const scl_complex<Type_>& rhs);

template <class Type_>
scl_complex<Type_>
polar(const Type_& rho, const Type_& theta);


//
//  26.2.8 Transcendentals
//
template <class Type_>
scl_complex<Type_>
cos(const scl_complex<Type_>& rhs);

template <class Type_>
scl_complex<Type_>
cosh(const scl_complex<Type_>& rhs);

template <class Type_>
scl_complex<Type_>
exp(const scl_complex<Type_>& rhs);

template <class Type_>
scl_complex<Type_>
log(const scl_complex<Type_>& rhs);

template <class Type_>
scl_complex<Type_>
log10(const scl_complex<Type_>& rhs);

template <class Type_>
scl_complex<Type_>
pow(const scl_complex<Type_>& rhs, int pwr);

template <class Type_>
scl_complex<Type_>
pow(const scl_complex<Type_>& rhs, const Type_& pwr);

template <class Type_>
scl_complex<Type_>
pow(const scl_complex<Type_>& rhs, const scl_complex<Type_>& pwr);

template <class Type_>
scl_complex<Type_>
pow(const Type_& rhs, const scl_complex<Type_>& pwr);

template <class Type_>
scl_complex<Type_>
sin(const scl_complex<Type_>& rhs);

template <class Type_>
scl_complex<Type_>
sinh(const scl_complex<Type_>& rhs);

template <class Type_>
scl_complex<Type_>
sqrt(const scl_complex<Type_>& rhs);

template <class Type_>
scl_complex<Type_>
tan(const scl_complex<Type_>& rhs);

template <class Type_>
scl_complex<Type_>
tanh(const scl_complex<Type_>& rhs);


//
//  26.2.3 Complex Specializations
//
SCL_SPECIALIZE_CLASS
class SCL_STD_EXPORT scl_complex<float> : public _scl_complex_base<float>
{
public:
    typedef scl_complex<float>          Self_;
    typedef _scl_complex_base<float>    Base_;
    typedef float                       value_type;

    scl_complex(float real = 0.0f, float imag = 0.0f);
    explicit scl_complex(const scl_complex<double>& rhs);
    explicit scl_complex(const scl_complex<long double>& rhs);

//  float   real() const;
//  float   imag() const;

    Self_&  operator= (float rhs);
    Self_&  operator+=(float rhs);
    Self_&  operator-=(float rhs);
    Self_&  operator*=(float rhs);
    Self_&  operator/=(float rhs);

    Self_&  operator= (const Self_& rhs);
    Self_&  operator+=(const Self_& rhs);
    Self_&  operator-=(const Self_& rhs);
    Self_&  operator*=(const Self_& rhs);
    Self_&  operator/=(const Self_& rhs);

#ifdef  SCL_HAS_MEMBER_TEMPLATES
    template <class Other_> Self_&  operator= (const scl_complex<Other_>& rhs);
    template <class Other_> Self_&  operator+=(const scl_complex<Other_>& rhs);
    template <class Other_> Self_&  operator-=(const scl_complex<Other_>& rhs);
    template <class Other_> Self_&  operator*=(const scl_complex<Other_>& rhs);
    template <class Other_> Self_&  operator/=(const scl_complex<Other_>& rhs);
#endif
};


SCL_SPECIALIZE_CLASS
class SCL_STD_EXPORT scl_complex<double> : public _scl_complex_base<double>
{
public:
    typedef scl_complex<double>         Self_;
    typedef _scl_complex_base<double>   Base_;
    typedef double                      value_type;

    scl_complex(double real = 0.0, double imag = 0.0);
    scl_complex(const scl_complex<float>& rhs);
    explicit scl_complex(const scl_complex<long double>& rhs);

//  double  real() const;
//  double  imag() const;

    Self_&  operator= (double rhs);
    Self_&  operator+=(double rhs);
    Self_&  operator-=(double rhs);
    Self_&  operator*=(double rhs);
    Self_&  operator/=(double rhs);

    Self_&  operator= (const Self_& rhs);
    Self_&  operator+=(const Self_& rhs);
    Self_&  operator-=(const Self_& rhs);
    Self_&  operator*=(const Self_& rhs);
    Self_&  operator/=(const Self_& rhs);

#ifdef  SCL_HAS_MEMBER_TEMPLATES
    template <class Other_> Self_&  operator= (const scl_complex<Other_>& rhs);
    template <class Other_> Self_&  operator+=(const scl_complex<Other_>& rhs);
    template <class Other_> Self_&  operator-=(const scl_complex<Other_>& rhs);
    template <class Other_> Self_&  operator*=(const scl_complex<Other_>& rhs);
    template <class Other_> Self_&  operator/=(const scl_complex<Other_>& rhs);
#endif
};


SCL_SPECIALIZE_CLASS
class SCL_STD_EXPORT scl_complex<long double> : public _scl_complex_base<long double>
{
public:
    typedef scl_complex<long double>        Self_;
    typedef _scl_complex_base<long double>  Base_;
    typedef long double                     value_type;

    scl_complex(long double real = 0.0L, long double imag = 0.0L);
    scl_complex(const scl_complex<float>& rhs);
    scl_complex(const scl_complex<double>& rhs);

//  long double real() const;
//  long double imag() const;

    Self_&  operator= (long double rhs);
    Self_&  operator+=(long double rhs);
    Self_&  operator-=(long double rhs);
    Self_&  operator*=(long double rhs);
    Self_&  operator/=(long double rhs);

    Self_&  operator= (const Self_& rhs);
    Self_&  operator+=(const Self_& rhs);
    Self_&  operator-=(const Self_& rhs);
    Self_&  operator*=(const Self_& rhs);
    Self_&  operator/=(const Self_& rhs);

#ifdef  SCL_HAS_MEMBER_TEMPLATES
    template <class Other_> Self_&  operator= (const scl_complex<Other_>& rhs);
    template <class Other_> Self_&  operator+=(const scl_complex<Other_>& rhs);
    template <class Other_> Self_&  operator-=(const scl_complex<Other_>& rhs);
    template <class Other_> Self_&  operator*=(const scl_complex<Other_>& rhs);
    template <class Other_> Self_&  operator/=(const scl_complex<Other_>& rhs);
#endif
};


#include <sclp_complex.cc>


#endif  /* _SCLP_COMPLEX_H_ */
