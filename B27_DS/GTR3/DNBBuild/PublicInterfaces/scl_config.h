/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: scl_config.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Oct-2004     Initial implementation.
//
//  NOTICE:
//      This header is used internally by the Standard C++ Library.
//      It should not be included directly in application code.
//
//==============================================================================
#ifndef _SCL_CONFIG_H_
#define _SCL_CONFIG_H_


#include <stddef.h>                     // for size_t, ptrdiff_t


                /**********************************************/
                /**        Compiler/Platform Features        **/
                /**********************************************/


//
//  Platform feature-test macros.
//
//  SCL_HAS_BOOL                    Type "bool" is present
//  SCL_HAS_DISTINCT_BOOL           Type "bool" is unique (built-in)
//  SCL_HAS_DISTINCT_WCHAR          Type "wchar_t" is unique (built-in)
//  SCL_HAS_IOSTREAM_WCHAR          Legacy iostream library provides inserters and
//                                  extractors for wide-character types
//  SCL_HAS_LLONG                   Type "long long" (or equivalent) is present
//  SCL_HAS_EXPLICIT                Keyword "explicit" is present
//  SCL_HAS_TYPENAME                Keyword "typename" is present
//  SCL_HAS_NAMESPACES              Namespaces are supported
//  SCL_HAS_RTTI                    Runtime type information is supported
//  SCL_HAS_EXCEPTIONS              Exception handling is enabled
//  SCL_HAS_THROW_SPECS             Throw specifications are supported
//  SCL_HAS_CLASS_USING_DECL        Class-scoped using declarations are allowed
//  SCL_HAS_DEFAULT_TMPL_ARGS       Default  template arguments are allowed
//  SCL_HAS_NONTYPE_TMPL_ARGS       Non-type template arguments are allowed
//  SCL_HAS_FUNC_TMPL_ARGS          Explicit function template arguments are allowed
//  SCL_HAS_MEMBER_TEMPLATES        Template member functions of classes are allowed
//  SCL_HAS_PARTIAL_SPEC            Partial template specialization is supported
//  SCL_HAS_PARTIAL_SPEC_OVERLOAD   Compiler resolves overloading of function template
//                                  arguments that are partial specialization
//  SCL_HAS_SPEC_SYNTAX             Template<> is required for specialization
//  SCL_HAS_STATIC_CONST_INIT       Constant-initializer is allowed in declaration
//                                  of static const data members (see section 9.4.2,
//                                  paragraph 4 of C++ Standard)
//  SCL_HAS_EXPLICIT_CLASS_INST     Explicit template class instantiation is allowed
//  SCL_HAS_EXPLICIT_FUNC_INST      Explicit template function instantiation is allowed
//  SCL_HAS_STD_NEW                 Header <new> is provided
//  SCL_HAS_STD_EXCEPTION           Header <exception> is provided
//  SCL_HAS_STD_TYPEINFO            Header <typeinfo> is provided
//  SCL_HAS_STD_IOSTREAM            New iostream library is provided (<iostream>)
//  SCL_HAS_WSTR_FUNCTIONS          Wide-character functions are provided (wmemcpy, etc)
//


//
//  Machine endian types.
//
#define SCL_LITTLE_ENDIAN   1234    // Least-significant byte first (Intel)
#define SCL_BIG_ENDIAN      4321    // Most-significant byte first (SPARC, IBM, etc)
#define SCL_PDP_ENDIAN      3412    // LSB first in word; MSW first in long (PDP)


//
//  Load platform feature-test macros.
//
#if     defined(_WINDOWS_SOURCE)
#if     defined(PLATEFORME_DS64)
#include <scl_config_win64.h>           // Windows 64-bit (MSVC 8.x)
#else
#include <scl_config_win32.h>           // Windows 32-bit (MSVC 8.x)
#endif  /* PLATEFORME_DS64 */

#elif   defined(_IRIX_SOURCE)
#include <scl_config_sgi.h>             // SGI IRIX 32-bit (C++ 7.2.1)

#elif   defined(_SUNOS_SOURCE)
#include <scl_config_sun.h>             // Sun Solaris 32-bit (C++ 5.7)

#elif   defined(_AIX_SOURCE)
#include <scl_config_aix.h>             // IBM AIX 32-bit and 64-bit (XL C++ 8.0)

#elif   defined(_HPUX_SOURCE)
#include <scl_config_hpux.h>            // HP-UX 32-bit (aC++ A.03.52)

#else
#error  "Unsupported platform"
#endif


                /**********************************************/
                /**          Library Configuration           **/
                /**********************************************/


//
//  Configure the Standard C++ Library
//
#undef  SCL_USE_STD_RELOPS

#ifdef  _CAT_ANSI_STREAMS
#define SCL_USE_STD_IOSTREAM            /* Use standard iostream library */
#else
#undef  SCL_USE_STD_IOSTREAM            /* Use legacy iostream library   */
#endif

#ifdef  SCL_HAS_MEMBER_TEMPLATES
#define SCL_USE_ALLOCATOR_REBIND
#else
#undef  SCL_USE_ALLOCATOR_REBIND
#endif

#ifndef SCL_ENABLE_IO
#define SCL_ENABLE_IO                   /* Enable I/O operations for containers */
#endif

#ifdef  CNEXT_CLIENT
#undef  SCL_DEBUG                       /* Create release build */
#else
#define SCL_DEBUG                       /* Create debug build   */
#endif


                /**********************************************/
                /**           Portability Section            **/
                /**********************************************/


//
//  Export macros.
//
#ifdef  _WINDOWS_SOURCE
#ifdef  __std12d
#define SCL_STD_EXPORT          __declspec(dllexport)
#else
#define SCL_STD_EXPORT          __declspec(dllimport)
#endif
#else
#define SCL_STD_EXPORT          /* nothing */
#endif  /* _WINDOWS_SOURCE */

#ifdef  _WINDOWS_SOURCE
#define SCL_EXPORT_TEMPLATE     extern template
#else
#define SCL_EXPORT_TEMPLATE     template
#endif


//
//  Concatenation macros.
//
#define SCL_CONCAT2(A_,B_)          _SCL_CONCAT2(A_,B_)
#define SCL_CONCAT3(A_,B_,C_)       _SCL_CONCAT3(A_,B_,C_)
#define SCL_CONCAT4(A_,B_,C_,D_)    _SCL_CONCAT4(A_,B_,C_,D_)

#define _SCL_CONCAT2(A_,B_)         A_ ## B_
#define _SCL_CONCAT3(A_,B_,C_)      A_ ## B_ ## C_
#define _SCL_CONCAT4(A_,B_,C_,D_)   A_ ## B_ ## C_ ## D_


//
//  ANSI C++ Language Support
//
#ifndef SCL_HAS_BOOL
#ifndef bool
typedef int             bool;
#endif

#ifndef true
#define true            1
#define false           0
#endif
#endif  /* SCL_HAS_BOOL */

#ifndef SCL_HAS_EXPLICIT
#undef  explicit
#define explicit                /* nothing */
#endif

#ifndef SCL_HAS_TYPENAME
#undef  typename
#define typename                /* nothing */
#endif

#ifdef  SCL_HAS_SPEC_SYNTAX
#define SCL_SPECIALIZE_CLASS    template <>
#define SCL_SPECIALIZE_FUNC     template <>
#else
#define SCL_SPECIALIZE_CLASS    /* nothing */
#define SCL_SPECIALIZE_FUNC     /* nothing */
#endif

#ifdef  SCL_HAS_THROW_SPECS
#define SCL_NOTHROW             throw ()
#define SCL_THROWS_NONE         throw ()
#define SCL_THROWS(EXLIST_)     throw EXLIST_
#define SCL_THROWS_ANY          /* nothing */
#else
#define SCL_NOTHROW             /* nothing */
#define SCL_THROWS_NONE         /* nothing */
#define SCL_THROWS(EXLIST_)     /* nothing */
#define SCL_THROWS_ANY          /* nothing */
#endif

#ifdef  SCL_HAS_DEFAULT_TMPL_ARGS
#define SCL_DEFAULT_ARG(ARG_)   = ARG_
#else
#define SCL_DEFAULT_ARG(ARG_)   /* nothing */
#endif

//
//  Define the namespace for <typeinfo>, <exception>, and <new>.  These must be
//  provided by the C++ compiler.
//
#ifdef  SCL_HAS_NAMESPACES
#define SCL_VENDOR_STD          std
#define SCL_VENDOR_STD_BEGIN    namespace SCL_VENDOR_STD {
#define SCL_VENDOR_STD_END      }
#else
#define SCL_VENDOR_STD          /* nothing */
#define SCL_VENDOR_STD_BEGIN    /* nothing */
#define SCL_VENDOR_STD_END      /* nothing */
#endif


//
//  Macros for floating-point constants.
//
#define SCL_FLT_CONST(X_)       SCL_CONCAT2(X_,f)
#define SCL_DBL_CONST(X_)       X_
#define SCL_LDBL_CONST(X_)      SCL_CONCAT2(X_,L)


//
//  Program Assertions
//
#define SCL_PROGRAM_ERROR(MSG_)         _scl_do_assert("Software", MSG_, __FILE__, __LINE__)
#define SCL_ASSERT_ALWAYS(EXPR_)        _SCL_DO_ASSERT("Assertion"    , EXPR_)
#define SCL_PRECONDITION_ALWAYS(EXPR_)  _SCL_DO_ASSERT("Precondition" , EXPR_)
#define SCL_POSTCONDITION_ALWAYS(EXPR_) _SCL_DO_ASSERT("Postcondition", EXPR_)

#ifdef  SCL_DEBUG
#define SCL_ASSERT(EXPR_)               _SCL_DO_ASSERT("Assertion"    , EXPR_)
#define SCL_PRECONDITION(EXPR_)         _SCL_DO_ASSERT("Precondition" , EXPR_)
#define SCL_POSTCONDITION(EXPR_)        _SCL_DO_ASSERT("Postcondition", EXPR_)
#else
#define SCL_ASSERT(EXPR_)               ((void) 0)
#define SCL_PRECONDITION(EXPR_)         ((void) 0)
#define SCL_POSTCONDITION(EXPR_)        ((void) 0)
#endif  /* SCL_DEBUG */


//
//  Class Invariants
//
#ifdef  SCL_DEBUG
#define SCL_VALIDATE(TYPE_, ITEM_)      _scl_validate<TYPE_ > _validate_(ITEM_, _SCL_FUNC_BOTH )
#define SCL_VALIDATE_CTOR(TYPE_, ITEM_) _scl_validate<TYPE_ > _validate_(ITEM_, _SCL_FUNC_EXIT )
#define SCL_VALIDATE_DTOR(TYPE_, ITEM_) _scl_validate<TYPE_ > _validate_(ITEM_, _SCL_FUNC_ENTRY)
#else
#define SCL_VALIDATE(TYPE_, ITEM_)      ((void) 0)
#define SCL_VALIDATE_CTOR(TYPE_, ITEM_) ((void) 0)
#define SCL_VALIDATE_DTOR(TYPE_, ITEM_) ((void) 0)
#endif  /* SCL_DEBUG */


//
//  Memory Allocation
//
const size_t    SCL_NPOS = ((size_t) -1);

SCL_STD_EXPORT void *
scl_malloc(size_t size);

SCL_STD_EXPORT void
scl_free(void* ptr);

SCL_STD_EXPORT void *
scl_allocate(size_t size);

SCL_STD_EXPORT void
scl_deallocate(void* ptr, size_t size);


//
//  Object Lifecycle Management
//
enum SCL_STATIC_TYPE { SCL_STATIC_OBJECT = 0 };


//
//  Error Codes
//
const int   SCL_STATUS_OKAY  =  0;
const int   SCL_STATUS_ERROR = -1;


//
//  25.2.2 scl_swap
//
template <class Type_>
void
scl_swap(Type_& lhs, Type_& rhs)
{
    const Type_ tmp(lhs);
    lhs = rhs;
    rhs = tmp;
}


#include <scl_config.cc>


#endif  /* _SCL_CONFIG_H_ */
