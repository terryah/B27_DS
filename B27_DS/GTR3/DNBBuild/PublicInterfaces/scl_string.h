/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: scl_string.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Oct-2004     Initial implementation.
//
//  NOTICE:
//      This header implements functionality in the Standard C++ Library.
//      It may be included directly in application code.
//
//==============================================================================
#ifndef _SCL_STRING_H_
#define _SCL_STRING_H_


#ifndef  _SCL_CONFIG_H_
#include <scl_config.h>
#endif


#ifdef  SCL_USE_VENDOR_STD
#include <scl_memory.h>
#include <string>

#if     defined(_IRIX_SOURCE) && (_COMPILER_VERSION == 721)
#define scl_char_traits                 SCL_VENDOR_STD::string_char_traits
#else
#define scl_char_traits                 SCL_VENDOR_STD::char_traits
#endif

#define scl_basic_string                SCL_VENDOR_STD::basic_string
#define scl_string                      SCL_VENDOR_STD::string
#define scl_wstring                     SCL_VENDOR_STD::wstring
#ifndef scl_swap
#define scl_swap                        SCL_VENDOR_STD::swap
#endif
#define scl_getline                     SCL_VENDOR_STD::getline

#else
#include <sclp_string.h>

#endif  /* SCL_USE_VENDOR_STD */


#endif  /* _SCL_STRING_H_ */
