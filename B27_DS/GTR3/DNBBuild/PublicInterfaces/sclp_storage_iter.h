/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: sclp_storage_iter.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Dec-2004     Initial implementation.
//
//  NOTICE:
//      This header is used internally by the Standard C++ Library.
//      It should not be included directly in application code.
//
//==============================================================================
#ifndef _SCLP_STORAGE_ITER_H_
#define _SCLP_STORAGE_ITER_H_


//
//  Include section
//
#ifndef  _SCLP_BASE_ITER_H_
#include <sclp_base_iter.h>             // for scl_iterator and friends
#endif

#ifndef  _SCLP_CONSTRUCT_H_
#include <sclp_construct.h>             // for _scl_construct
#endif


//
//  20.4.2 Template class scl_raw_storage_iterator
//
template <class OutIter_, class Type_>
class scl_raw_storage_iterator : public
    scl_iterator<scl_output_iterator_tag, void, void, void, void>
{
public:
    // Private types
    typedef
    scl_iterator<scl_output_iterator_tag, void, void, void, void>
                                                        Base_;
    typedef scl_raw_storage_iterator<OutIter_, Type_>   Self_;

    // Public types
    SCL_ITERATOR_TYPEDEFS(Base_);

    explicit
    scl_raw_storage_iterator(OutIter_ iter) :
        iter_(iter)
    {
        // Nothing
    }

    Self_& operator*()
    {
        return *this;
    }

    Self_& operator=(const Type_& val)
    {
        _scl_construct(&*iter_, val);
        return *this;
    }

    Self_& operator++()
    {
        ++iter_;
        return *this;
    }

    Self_  operator++(int)
    {
        Self_ tmp = *this;
        ++iter_;
        return tmp;
    }

private:
    OutIter_    iter_;
};


#endif  /* _SCLP_STORAGE_ITER_H_ */
