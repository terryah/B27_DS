/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: sclp_auto_ptr.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Dec-2004     Initial implementation.
//
//  NOTICE:
//      This header is used internally by the Standard C++ Library.
//      It should not be included directly in application code.
//
//==============================================================================
#ifndef _SCLP_AUTO_PTR_H_
#define _SCLP_AUTO_PTR_H_


//
//  20.4.5 Template class scl_auto_ptr
//
template <class Type_>
struct scl_auto_ptr_ref
{
    Type_*  ptr_;

    scl_auto_ptr_ref(Type_* ptr) :
        ptr_(ptr)
    {
        // Nothing
    }
};

template <class Type_>
class scl_auto_ptr
{
public:
    typedef scl_auto_ptr<Type_> Self_;
    typedef Type_               element_type;

    // constructor
    explicit
    scl_auto_ptr(Type_* ptr = NULL) SCL_NOTHROW :
        ptr_(ptr)
    {
        // Nothing
    }

    // copy constructors (with implicit conversion)
    scl_auto_ptr(Self_& other) SCL_NOTHROW :
        ptr_(other.release())
    {
        // Nothing
    }

#ifdef  SCL_HAS_MEMBER_TEMPLATES
    template <class Other_>
    scl_auto_ptr(scl_auto_ptr<Other_>& other) SCL_NOTHROW :
        ptr_(other.release())
    {
        // Nothing
    }
#endif

    // assignments (with implicit conversion)
    Self_&
    operator=(Self_& other) SCL_NOTHROW
    {
        reset(other.release());
        return *this;
    }

#ifdef  SCL_HAS_MEMBER_TEMPLATES
    template <class Other_>
    Self_&
    operator=(scl_auto_ptr<Other_>& other) SCL_NOTHROW
    {
        reset(other.release());
        return *this;
    }
#endif

    // destructor
    ~scl_auto_ptr() SCL_NOTHROW
    {
        delete ptr_;
        ptr_ = NULL;
    }

    // value access
    Type_& operator*() const SCL_NOTHROW
    {
        return *ptr_;
    }

    Type_* operator->() const SCL_NOTHROW
    {
        return ptr_;
    }

    Type_* get() const SCL_NOTHROW
    {
        return ptr_;
    }

    // release ownership
    Type_* release() SCL_NOTHROW
    {
        Type_* tmp = ptr_;
        ptr_ = NULL;
        return tmp;
    }

    // reset value
    void reset(Type_* ptr = NULL) SCL_NOTHROW
    {
        if (ptr_ != ptr)
        {
            delete ptr_;
            ptr_ = ptr;
        }
    }

    // special conversions with auxiliary type to enable copies and assignments
    scl_auto_ptr(scl_auto_ptr_ref<Type_> other) SCL_NOTHROW :
        ptr_(other.ptr_)
    {
        // Nothing
    }

    Self_&
    operator=(scl_auto_ptr_ref<Type_> other) SCL_NOTHROW
    {
        reset(other.ptr_);
        return *this;
    }

#ifdef  SCL_HAS_MEMBER_TEMPLATES
    template <class Other_>
    operator scl_auto_ptr_ref<Other_>() SCL_NOTHROW
    {
        return scl_auto_ptr_ref<Other_>(release());
    }

    template <class Other_>
    operator scl_auto_ptr<Other_>() SCL_NOTHROW
    {
        return scl_auto_ptr<Other_>(release());
    }
#endif

private:
    Type_*  ptr_;               // Refers to the actual owned object (if any).
};


#endif  /* _SCLP_AUTO_PTR_H_ */
