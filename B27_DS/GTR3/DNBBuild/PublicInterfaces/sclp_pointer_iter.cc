/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: sclp_pointer_iter.cc
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Aug-2004     Initial implementation.
//
//==============================================================================


//
//  Private utilities
//
#ifndef SCL_HAS_PARTIAL_SPEC

// Determine the category of a pointer iterator.
template <class Type_>
scl_random_access_iterator_tag
_scl_category(const scl_pointer_iterator<Type_>&)
{
    return scl_random_access_iterator_tag();
}

template <class Type_>
scl_random_access_iterator_tag
_scl_category(const scl_pointer_const_iterator<Type_>&)
{
    return scl_random_access_iterator_tag();
}

// Determine the value type of a pointer iterator.
template <class Type_>
Type_ *
_scl_value_type(const scl_pointer_iterator<Type_>&)
{
    return (Type_ *) NULL;
}

template <class Type_>
Type_ *
_scl_value_type(const scl_pointer_const_iterator<Type_>&)
{
    return (Type_ *) NULL;
}

// Determine the distance type of a pointer iterator.
template <class Type_>
ptrdiff_t *
_scl_distance_type(const scl_pointer_iterator<Type_>&)
{
    return (ptrdiff_t *) NULL;
}

template <class Type_>
ptrdiff_t *
_scl_distance_type(const scl_pointer_const_iterator<Type_>&)
{
    return (ptrdiff_t *) NULL;
}

#endif  /* SCL_HAS_PARTIAL_SPEC */
