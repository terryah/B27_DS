/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: scl_algorithm.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Dec-2004     Initial implementation.
//
//  NOTICE:
//      This header implements functionality in the Standard C++ Library.
//      It may be included directly in application code.
//
//==============================================================================
#ifndef _SCL_ALGORITHM_H_
#define _SCL_ALGORITHM_H_


#ifndef  _SCL_CONFIG_H_
#include <scl_config.h>
#endif


#ifdef  SCL_USE_VENDOR_STD
#include <algorithm>

#define scl_for_each                    SCL_VENDOR_STD::for_each
#define scl_find                        SCL_VENDOR_STD::find
#define scl_find_if                     SCL_VENDOR_STD::find_if
#define scl_find_end                    SCL_VENDOR_STD::find_end
#define scl_find_first_of               SCL_VENDOR_STD::find_first_of
#define scl_adjacent_find               SCL_VENDOR_STD::adjacent_find
#define scl_count                       SCL_VENDOR_STD::count
#define scl_count_if                    SCL_VENDOR_STD::count_if
#define scl_mismatch                    SCL_VENDOR_STD::mismatch
#define scl_equal                       SCL_VENDOR_STD::equal
#define scl_search                      SCL_VENDOR_STD::search
#define scl_search_n                    SCL_VENDOR_STD::search_n
#define scl_copy                        SCL_VENDOR_STD::copy
#define scl_copy_backward               SCL_VENDOR_STD::copy_backward
#ifndef scl_swap
#define scl_swap                        SCL_VENDOR_STD::swap
#endif
#define scl_swap_ranges                 SCL_VENDOR_STD::swap_ranges
#define scl_iter_swap                   SCL_VENDOR_STD::iter_swap
#define scl_transform                   SCL_VENDOR_STD::transform
#define scl_replace                     SCL_VENDOR_STD::replace
#define scl_replace_if                  SCL_VENDOR_STD::replace_if
#define scl_replace_copy                SCL_VENDOR_STD::replace_copy
#define scl_replace_copy_if             SCL_VENDOR_STD::replace_copy_if
#define scl_fill                        SCL_VENDOR_STD::fill
#define scl_fill_n                      SCL_VENDOR_STD::fill_n
#define scl_generate                    SCL_VENDOR_STD::generate
#define scl_generate_n                  SCL_VENDOR_STD::generate_n
#define scl_remove                      SCL_VENDOR_STD::remove
#define scl_remove_if                   SCL_VENDOR_STD::remove_if
#define scl_remove_copy                 SCL_VENDOR_STD::remove_copy
#define scl_remove_copy_if              SCL_VENDOR_STD::remove_copy_if
#define scl_unique                      SCL_VENDOR_STD::unique
#define scl_unique_copy                 SCL_VENDOR_STD::unique_copy
#define scl_reverse                     SCL_VENDOR_STD::reverse
#define scl_reverse_copy                SCL_VENDOR_STD::reverse_copy
#define scl_rotate                      SCL_VENDOR_STD::rotate
#define scl_rotate_copy                 SCL_VENDOR_STD::rotate_copy
#define scl_random_shuffle              SCL_VENDOR_STD::random_shuffle
#define scl_partition                   SCL_VENDOR_STD::partition
#define scl_stable_partition            SCL_VENDOR_STD::stable_partition
#define scl_sort                        SCL_VENDOR_STD::sort
#define scl_stable_sort                 SCL_VENDOR_STD::stable_sort
#define scl_partial_sort                SCL_VENDOR_STD::partial_sort
#define scl_partial_sort_copy           SCL_VENDOR_STD::partial_sort_copy
#define scl_nth_element                 SCL_VENDOR_STD::nth_element
#define scl_lower_bound                 SCL_VENDOR_STD::lower_bound
#define scl_upper_bound                 SCL_VENDOR_STD::upper_bound
#define scl_equal_range                 SCL_VENDOR_STD::equal_range
#define scl_binary_search               SCL_VENDOR_STD::binary_search
#define scl_merge                       SCL_VENDOR_STD::merge
#define scl_inplace_merge               SCL_VENDOR_STD::inplace_merge
#define scl_includes                    SCL_VENDOR_STD::includes
#define scl_set_union                   SCL_VENDOR_STD::set_union
#define scl_set_intersection            SCL_VENDOR_STD::set_intersection
#define scl_set_difference              SCL_VENDOR_STD::set_difference
#define scl_set_symmetric_difference    SCL_VENDOR_STD::set_symmetric_difference
#define scl_push_heap                   SCL_VENDOR_STD::push_heap
#define scl_pop_heap                    SCL_VENDOR_STD::pop_heap
#define scl_make_heap                   SCL_VENDOR_STD::make_heap
#define scl_sort_heap                   SCL_VENDOR_STD::sort_heap
#ifdef  _WINDOWS_SOURCE
#define scl_min                         SCL_VENDOR_STD::_cpp_min
#define scl_max                         SCL_VENDOR_STD::_cpp_max
#else
#define scl_min                         SCL_VENDOR_STD::min
#define scl_max                         SCL_VENDOR_STD::max
#endif
#define scl_min_element                 SCL_VENDOR_STD::min_element
#define scl_max_element                 SCL_VENDOR_STD::max_element
#define scl_lexicographical_compare     SCL_VENDOR_STD::lexicographical_compare
#define scl_next_permutation            SCL_VENDOR_STD::next_permutation
#define scl_prev_permutation            SCL_VENDOR_STD::prev_permutation

#else
#include <sclp_algorithm.h>

#endif  /* SCL_USE_VENDOR_STD */


#endif  /* _SCL_ALGORITHM_H_ */
