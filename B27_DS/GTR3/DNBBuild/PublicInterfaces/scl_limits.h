/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: scl_limits.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Oct-2004     Initial implementation.
//
//  NOTICE:
//      This header implements functionality in the Standard C++ Library.
//      It may be included directly in application code.
//
//==============================================================================
#ifndef _SCL_LIMITS_H_
#define _SCL_LIMITS_H_


#ifndef  _SCL_CONFIG_H_
#include <scl_config.h>
#endif


#ifdef  SCL_USE_VENDOR_STD
#include <limits>

#define scl_float_denorm_style          SCL_VENDOR_STD::float_denorm_style
#define scl_denorm_indeterminate        SCL_VENDOR_STD::denorm_indeterminate
#define scl_denorm_absent               SCL_VENDOR_STD::denorm_absent
#define scl_denorm_present              SCL_VENDOR_STD::denorm_present
#define scl_float_round_style           SCL_VENDOR_STD::float_round_style
#define scl_round_indeterminate         SCL_VENDOR_STD::round_indeterminate
#define scl_round_toward_zero           SCL_VENDOR_STD::round_toward_zero
#define scl_round_to_nearest            SCL_VENDOR_STD::round_to_nearest
#define scl_round_toward_infinity       SCL_VENDOR_STD::round_toward_infinity
#define scl_round_toward_neg_infinity   SCL_VENDOR_STD::round_toward_neg_infinity
#define scl_numeric_limits              SCL_VENDOR_STD::numeric_limits

#else
#include <sclp_limits.h>

#endif  /* SCL_USE_VENDOR_STD */


#endif  /* _SCL_LIMITS_H_ */
