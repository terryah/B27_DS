/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: sclp_heap_algo.cc
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Aug-2004     Initial implementation.
//
//==============================================================================


//
//  Private functions
//
template <class RanIter_, class Dist_, class Type_>
void
_scl_push_heap_aux(RanIter_ first, Dist_ hole, Dist_ top, Type_ value)
{
    Dist_   parent = (hole - 1) / 2;

    while (hole > top && *(first + parent) < value)
    {
        *(first + hole) = *(first + parent);
        hole   = parent;
        parent = (hole - 1) / 2;
    }

    *(first + hole) = value;
}

template <class RanIter_, class Comp_, class Dist_, class Type_>
void
_scl_push_heap_aux(RanIter_ first, Comp_ comp, Dist_ hole, Dist_ top, Type_ value)
{
    Dist_   parent = (hole - 1) / 2;

    while (hole > top && comp(*(first + parent), value))
    {
        *(first + hole) = *(first + parent);
        hole   = parent;
        parent = (hole - 1) / 2;
    }

    *(first + hole) = value;
}

template <class RanIter_, class Dist_, class Type_>
void
_scl_push_heap(RanIter_ first, RanIter_ last, Dist_*, Type_*)
{
    _scl_push_heap_aux(first,       Dist_((last - first) - 1), Dist_(0), Type_(*(last - 1)));
}

template <class RanIter_, class Comp_, class Dist_, class Type_>
void
_scl_push_heap(RanIter_ first, RanIter_ last, Comp_ comp, Dist_*, Type_*)
{
    _scl_push_heap_aux(first, comp, Dist_((last - first) - 1), Dist_(0), Type_(*(last - 1)));
}

template <class RanIter_, class Dist_, class Type_>
void
_scl_adjust_heap_aux(RanIter_ first, Dist_ hole, Dist_ len, Type_ value)
{
    const Dist_ top   = hole;
    Dist_       child = 2 * (hole + 1);

    while (child < len)
    {
        if (*(first + child) < *(first + (child - 1)))
            --child;
        *(first + hole) = *(first + child);
        hole  = child;
        child = 2 * (child + 1);
    }

    if (child == len)
    {
        *(first + hole) = *(first + (child - 1));
        hole = child - 1;
    }

    _scl_push_heap_aux(first,       hole, top, value);
}

template <class RanIter_, class Comp_, class Dist_, class Type_>
void
_scl_adjust_heap_aux(RanIter_ first, Comp_ comp, Dist_ hole, Dist_ len, Type_ value)
{
    const Dist_ top   = hole;
    Dist_       child = 2 * (hole + 1);

    while (child < len)
    {
        if (comp(*(first + child), *(first + (child - 1))))
            --child;
        *(first + hole) = *(first + child);
        hole  = child;
        child = 2 * (child + 1);
    }

    if (child == len)
    {
        *(first + hole) = *(first + (child - 1));
        hole = child - 1;
    }

    _scl_push_heap_aux(first, comp, hole, top, value);
}

template <class RanIter_, class Type_, class Dist_>
void
_scl_pop_heap_aux(RanIter_ first, RanIter_ last, RanIter_ result, Type_ value, Dist_*)
{
    *result = *first;
    _scl_adjust_heap_aux(first,       Dist_(0), Dist_(last - first), value);
}

template <class RanIter_, class Comp_, class Type_, class Dist_>
void
_scl_pop_heap_aux(RanIter_ first, RanIter_ last, RanIter_ result, Comp_ comp, Type_ value, Dist_*)
{
    *result = *first;
    _scl_adjust_heap_aux(first, comp, Dist_(0), Dist_(last - first), value);
}

template <class RanIter_, class Type_>
void
_scl_pop_heap(RanIter_ first, RanIter_ last, Type_*)
{
    _scl_pop_heap_aux(first, last - 1, last - 1,       Type_(*(last - 1)),
        _scl_distance_type(first));
}

template <class RanIter_, class Comp_, class Type_>
void
_scl_pop_heap(RanIter_ first, RanIter_ last, Comp_ comp, Type_*)
{
    _scl_pop_heap_aux(first, last - 1, last - 1, comp, Type_(*(last - 1)),
        _scl_distance_type(first));
}

template <class RanIter_, class Dist_, class Type_>
void
_scl_make_heap(RanIter_ first, RanIter_ last, Dist_*, Type_*)
{
    if (last - first < 2)
        return;

    const Dist_ len    = last - first;
    Dist_       parent = (len - 2) / 2;

    while (true)
    {
        _scl_adjust_heap_aux(first,       parent, len, Type_(*(first + parent)));
        if (parent == 0)
            return;
        --parent;
    }
}

template <class RanIter_, class Comp_, class Dist_, class Type_>
void
_scl_make_heap(RanIter_ first, RanIter_ last, Comp_ comp, Dist_*, Type_*)
{
    if (last - first < 2)
        return;

    const Dist_ len    = last - first;
    Dist_       parent = (len - 2) / 2;

    while (true)
    {
        _scl_adjust_heap_aux(first, comp, parent, len, Type_(*(first + parent)));
        if (parent == 0)
            return;
        --parent;
    }
}


//
//  Public functions
//
template <class RanIter_>
void
scl_push_heap(RanIter_ first, RanIter_ last)
{
    _scl_push_heap(first, last,       _scl_distance_type(first), _scl_value_type(first));
}

template <class RanIter_, class Comp_>
void
scl_push_heap(RanIter_ first, RanIter_ last, Comp_ comp)
{
    _scl_push_heap(first, last, comp, _scl_distance_type(first), _scl_value_type(first));
}

template <class RanIter_>
void
scl_pop_heap(RanIter_ first, RanIter_ last)
{
    _scl_pop_heap(first, last,       _scl_value_type(first));
}

template <class RanIter_, class Comp_>
void
scl_pop_heap(RanIter_ first, RanIter_ last, Comp_ comp)
{
    _scl_pop_heap(first, last, comp, _scl_value_type(first));
}

template <class RanIter_>
void
scl_make_heap(RanIter_ first, RanIter_ last)
{
    _scl_make_heap(first, last,       _scl_distance_type(first), _scl_value_type(first));
}

template <class RanIter_, class Comp_>
void
scl_make_heap(RanIter_ first, RanIter_ last, Comp_ comp)
{
    _scl_make_heap(first, last, comp, _scl_distance_type(first), _scl_value_type(first));
}

template <class RanIter_>
void
scl_sort_heap(RanIter_ first, RanIter_ last)
{
    while (last - first > 1)
    {
        scl_pop_heap(first, last);
        --last;
    }
}

template <class RanIter_, class Comp_>
void
scl_sort_heap(RanIter_ first, RanIter_ last, Comp_ comp)
{
    while (last - first > 1)
    {
        scl_pop_heap(first, last, comp);
        --last;
    }
}
