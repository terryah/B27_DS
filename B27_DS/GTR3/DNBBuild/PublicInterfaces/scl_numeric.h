/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: scl_numeric.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Oct-2004     Initial implementation.
//
//  NOTICE:
//      This header implements functionality in the Standard C++ Library.
//      It may be included directly in application code.
//
//==============================================================================
#ifndef _SCL_NUMERIC_H_
#define _SCL_NUMERIC_H_


#ifndef  _SCL_CONFIG_H_
#include <scl_config.h>
#endif


#ifdef  SCL_USE_VENDOR_STD
#include <numeric>

#define scl_accumulate                  SCL_VENDOR_STD::accumulate
#define scl_inner_product               SCL_VENDOR_STD::inner_product
#define scl_partial_sum                 SCL_VENDOR_STD::partial_sum
#define scl_adjacent_difference         SCL_VENDOR_STD::adjacent_difference

#else
#include <sclp_numeric.h>

#endif  /* SCL_USE_VENDOR_STD */


//
//  The following utilities are not part of the C++ standard.
//

//  Assign <value> to each element in the range [first, last), incrementing
//  <value> after each assignment.
template <class FwdIter_, class Type_>
void
scl_iota(FwdIter_ first, FwdIter_ last, Type_ value)
{
    for (; first != last; ++first, ++value)
        *first = value;
}


#endif  /* _SCL_NUMERIC_H_ */
