/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: scl_config.cc
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Oct-2004     Initial implementation.
//      jod     06-Jun-2008     Added scalar copy functions.
//
//==============================================================================

//
//  Private utilities.
//

//
//  Exception Handling
//
SCL_STD_EXPORT void
_scl_throw_bad_alloc();

SCL_STD_EXPORT void
_scl_throw_domain_error();

SCL_STD_EXPORT void
_scl_throw_invalid_argument();

SCL_STD_EXPORT void
_scl_throw_length_error();

SCL_STD_EXPORT void
_scl_throw_out_of_range();

SCL_STD_EXPORT void
_scl_throw_range_error();

SCL_STD_EXPORT void
_scl_throw_overflow_error();

SCL_STD_EXPORT void
_scl_throw_underflow_error();


//
//  Program Assertions
//
SCL_STD_EXPORT void
_scl_do_assert(const char *msg, const char *expr, const char *file, int line);

#define _SCL_DO_ASSERT(MSG_, EXPR_)     \
    ((EXPR_) ? ((void) 0) : _scl_do_assert(MSG_, #EXPR_, __FILE__, __LINE__))


//
//  Class Invariants
//
#define _SCL_FUNC_ENTRY         0x01
#define _SCL_FUNC_EXIT          0x02
#define _SCL_FUNC_BOTH          0x03

template <class Type_>
class _scl_validate
{
public:
    typedef _scl_validate<Type_>    Self_;

    _scl_validate(const Type_* item, unsigned int action) :
        item_   (item),
        action_ (action)
    {
        if (action_ & _SCL_FUNC_ENTRY)
            _SCL_DO_ASSERT( "Class Invariant", item_->_validate() );
    }

    ~_scl_validate()
    {
        if (action_ & _SCL_FUNC_EXIT)
            _SCL_DO_ASSERT( "Class Invariant", item_->_validate() );
    }

private:
    // Disable copy and assignment operations.
    _scl_validate ()                {};
    _scl_validate (const Self_&)    {};
    void operator=(const Self_&)    {};

    const Type_*    item_;
    unsigned int    action_;
};


//
//  Helpers for associative containers
//
#define SCL_PAIR(KEY_, VALUE_)  scl_pair<const KEY_, VALUE_ >


//
//  Declare optimized functions to copy and fill scalar arrays.
//  Note: The same macro logic must appear in scl_config.cpp .
//
#define SCL_DECLARE_SCALAR_COPY(Type_)                                  \
SCL_STD_EXPORT Type_*                                                   \
_scl_scalar_copy         (Type_* dst, const Type_* src, size_t len);    \
SCL_STD_EXPORT Type_*                                                   \
_scl_scalar_copy_backward(Type_* dst, const Type_* src, size_t len);    \
SCL_STD_EXPORT void                                                     \
_scl_scalar_fill         (Type_* dst,       Type_  val, size_t len);

SCL_DECLARE_SCALAR_COPY(char)
SCL_DECLARE_SCALAR_COPY(signed char)
SCL_DECLARE_SCALAR_COPY(unsigned char)

#ifdef  SCL_HAS_DISTINCT_WCHAR
SCL_DECLARE_SCALAR_COPY(wchar_t)
#endif

SCL_DECLARE_SCALAR_COPY(short)
SCL_DECLARE_SCALAR_COPY(unsigned short)

SCL_DECLARE_SCALAR_COPY(int)
SCL_DECLARE_SCALAR_COPY(unsigned int)

SCL_DECLARE_SCALAR_COPY(long)
SCL_DECLARE_SCALAR_COPY(unsigned long)

#ifdef  SCL_HAS_LLONG
SCL_DECLARE_SCALAR_COPY(SCL_LLONG_TYPE)
SCL_DECLARE_SCALAR_COPY(unsigned SCL_LLONG_TYPE)
#endif

#ifdef  SCL_HAS_DISTINCT_BOOL
SCL_DECLARE_SCALAR_COPY(bool)
#endif

SCL_DECLARE_SCALAR_COPY(float)
SCL_DECLARE_SCALAR_COPY(double)
SCL_DECLARE_SCALAR_COPY(long double)

#undef  SCL_DECLARE_SCALAR_COPY
