/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: scl_new.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Aug-2004     Initial implementation.
//
//  NOTICE:
//      This header implements functionality in the Standard C++ Library.
//      It may be included directly in application code.
//
//==============================================================================
#ifndef _SCL_NEW_H_
#define _SCL_NEW_H_


#ifndef  _SCL_CONFIG_H_
#include <scl_config.h>
#endif


#ifdef  SCL_HAS_STD_NEW
#include <new>

#else
#include <new.h>
#include <scl_exception.h>

SCL_VENDOR_STD_BEGIN

class SCL_STD_EXPORT bad_alloc : public scl_exception
{
public :
    bad_alloc() SCL_NOTHROW;
    virtual ~bad_alloc() SCL_NOTHROW;
    virtual const char* what() const SCL_NOTHROW;
};

SCL_VENDOR_STD_END

#endif  /* SCL_HAS_STD_NEW */


#define scl_bad_alloc                   SCL_VENDOR_STD::bad_alloc
#define scl_nothrow_t                   SCL_VENDOR_STD::nothrow_t
#define scl_nothrow                     SCL_VENDOR_STD::nothrow
#define scl_new_handler                 SCL_VENDOR_STD::new_handler
#define scl_set_new_handler             SCL_VENDOR_STD::set_new_handler


#endif  /* _SCL_NEW_H_ */
