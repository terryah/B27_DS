/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: scl_exception.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Aug-2004     Initial implementation.
//
//  NOTICE:
//      This header implements functionality in the Standard C++ Library.
//      It may be included directly in application code.
//
//==============================================================================
#ifndef _SCL_EXCEPTION_H_
#define _SCL_EXCEPTION_H_


#ifndef  _SCL_CONFIG_H_
#include <scl_config.h>
#endif


#ifdef  SCL_HAS_STD_EXCEPTION
#include <exception>
#define scl_exception                   SCL_VENDOR_STD::exception
#define scl_bad_exception               SCL_VENDOR_STD::bad_exception

#else

SCL_VENDOR_STD_BEGIN

class SCL_STD_EXPORT scl_exception
{
public :
    scl_exception() SCL_NOTHROW;
    virtual ~scl_exception() SCL_NOTHROW;
    virtual const char* what() const SCL_NOTHROW;
};

class SCL_STD_EXPORT scl_bad_exception : public scl_exception
{
public :
    scl_bad_exception() SCL_NOTHROW;
    virtual ~scl_bad_exception () SCL_NOTHROW;
    virtual const char* what() const SCL_NOTHROW;
};

SCL_VENDOR_STD_END

#endif  /* SCL_HAS_STD_EXCEPTION */


#endif  /* _SCL_EXCEPTION_H_ */
