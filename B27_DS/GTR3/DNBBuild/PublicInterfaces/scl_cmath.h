/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: scl_cmath.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Aug-2004     Initial implementation.
//
//  NOTICE:
//      This header implements functionality in the Standard C++ Library.
//      It may be included directly in application code.
//
//==============================================================================
#ifndef _SCL_CMATH_H_
#define _SCL_CMATH_H_


#ifndef  _SCL_CONFIG_H_
#include <scl_config.h>
#endif


//
//  If necessary, rename the C structure 'exception'.
//
#if     defined(_SUNOS_SOURCE)
#undef  exception
#define exception   math_exception
#endif

#include <math.h>

#undef  exception


//
//  Common mathematical constants.
//  Reference: http://numbers.computation.free.fr/Constants/constants.html
//
// 'double' form
#define SCL_M_E             2.7182818284590452353602874713526625    // e
#define SCL_M_LOG2E         1.4426950408889634073599246810018922    // log_2(e)
#define SCL_M_LOG10E        0.4342944819032518276511289189166051    // log_10(e)
#define SCL_M_LN2           0.6931471805599453094172321214581766    // log_e(2)
#define SCL_M_LN10          2.3025850929940456840179914546843642    // log_e(10)
#define SCL_M_LNPI          1.1447298858494001741434273513530587    // log_e(pi)
#define SCL_M_PI            3.1415926535897932384626433832795029    // pi
#define SCL_M_PI_2          1.5707963267948966192313216916397514    // pi/2
#define SCL_M_PI_4          0.7853981633974483096156608458198757    // pi/4
#define SCL_M_PI_180        0.0174532925199432957692369076848861    // pi/180
#define SCL_M_1_PI          0.3183098861837906715377675267450287    // 1/pi
#define SCL_M_2_PI          0.6366197723675813430755350534900574    // 2/pi
#define SCL_M_180_PI       57.2957795130823208767981548141051703    // 180/pi
#define SCL_M_2_SQRTPI      1.1283791670955125738961589031215452    // 2/sqrt(pi)
#define SCL_M_SQRT2         1.4142135623730950488016887242096981    // sqrt(2)
#define SCL_M_SQRT3         1.7320508075688772935274463415058724    // sqrt(3)
#define SCL_M_SQRT1_2       0.7071067811865475244008443621048490    // sqrt(1/2)
#define SCL_M_SQRTPI        1.7724538509055160272981674833411452    // sqrt(pi)

// 'float' form
#define SCL_M_Ef            SCL_FLT_CONST(SCL_M_E)
#define SCL_M_LOG2Ef        SCL_FLT_CONST(SCL_M_LOG2E)
#define SCL_M_LOG10Ef       SCL_FLT_CONST(SCL_M_LOG10E)
#define SCL_M_LN2f          SCL_FLT_CONST(SCL_M_LN2)
#define SCL_M_LN10f         SCL_FLT_CONST(SCL_M_LN10)
#define SCL_M_LNPIf         SCL_FLT_CONST(SCL_M_LNPI)
#define SCL_M_PIf           SCL_FLT_CONST(SCL_M_PI)
#define SCL_M_PI_2f         SCL_FLT_CONST(SCL_M_PI_2)
#define SCL_M_PI_4f         SCL_FLT_CONST(SCL_M_PI_4)
#define SCL_M_PI_180f       SCL_FLT_CONST(SCL_M_PI_180)
#define SCL_M_1_PIf         SCL_FLT_CONST(SCL_M_1_PI)
#define SCL_M_2_PIf         SCL_FLT_CONST(SCL_M_2_PI)
#define SCL_M_180_PIf       SCL_FLT_CONST(SCL_M_180_PI)
#define SCL_M_2_SQRTPIf     SCL_FLT_CONST(SCL_M_2_SQRTPI)
#define SCL_M_SQRT2f        SCL_FLT_CONST(SCL_M_SQRT2)
#define SCL_M_SQRT3f        SCL_FLT_CONST(SCL_M_SQRT3)
#define SCL_M_SQRT1_2f      SCL_FLT_CONST(SCL_M_SQRT1_2)
#define SCL_M_SQRTPIf       SCL_FLT_CONST(SCL_M_SQRTPI)

// 'long double' form
#define SCL_M_El            SCL_LDBL_CONST(SCL_M_E)
#define SCL_M_LOG2El        SCL_LDBL_CONST(SCL_M_LOG2E)
#define SCL_M_LOG10El       SCL_LDBL_CONST(SCL_M_LOG10E)
#define SCL_M_LN2l          SCL_LDBL_CONST(SCL_M_LN2)
#define SCL_M_LN10l         SCL_LDBL_CONST(SCL_M_LN10)
#define SCL_M_LNPIl         SCL_LDBL_CONST(SCL_M_LNPI)
#define SCL_M_PIl           SCL_LDBL_CONST(SCL_M_PI)
#define SCL_M_PI_2l         SCL_LDBL_CONST(SCL_M_PI_2)
#define SCL_M_PI_4l         SCL_LDBL_CONST(SCL_M_PI_4)
#define SCL_M_PI_180l       SCL_LDBL_CONST(SCL_M_PI_180)
#define SCL_M_1_PIl         SCL_LDBL_CONST(SCL_M_1_PI)
#define SCL_M_2_PIl         SCL_LDBL_CONST(SCL_M_2_PI)
#define SCL_M_180_PIl       SCL_LDBL_CONST(SCL_M_180_PI)
#define SCL_M_2_SQRTPIl     SCL_LDBL_CONST(SCL_M_2_SQRTPI)
#define SCL_M_SQRT2l        SCL_LDBL_CONST(SCL_M_SQRT2)
#define SCL_M_SQRT3l        SCL_LDBL_CONST(SCL_M_SQRT3)
#define SCL_M_SQRT1_2l      SCL_LDBL_CONST(SCL_M_SQRT1_2)
#define SCL_M_SQRTPIl       SCL_LDBL_CONST(SCL_M_SQRTPI)


//
//  Degree to radian conversion
//
inline float
scl_to_rad(float deg)
{
    return (deg * SCL_M_PI_180f);
}

inline double
scl_to_rad(double deg)
{
    return (deg * SCL_M_PI_180);
}

inline long double
scl_to_rad(long double deg)
{
    return (deg * SCL_M_PI_180l);
}


//
//  Radian to degree conversion
//
inline float
scl_to_deg(float rad)
{
    return (rad * SCL_M_180_PIf);
}

inline double
scl_to_deg(double rad)
{
    return (rad * SCL_M_180_PI);
}

inline long double
scl_to_deg(long double rad)
{
    return (rad * SCL_M_180_PIl);
}


//
//  Return 'sign' of scalar value.
//
template <class Type_>
inline Type_
scl_signum(const Type_ &rhs)
{
    if      (rhs > Type_(0))
        return Type_(1);
    else if (rhs < Type_(0))
        return Type_(-1);
    else
        return Type_(0);
}


#endif  /* _SCL_CMATH_H_ */
