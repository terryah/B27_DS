/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: sclp_list.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Aug-2004     Initial implementation.
//
//  NOTICE:
//      This header is used internally by the Standard C++ Library.
//      It should not be included directly in application code.
//
//==============================================================================
#ifndef _SCLP_LIST_H_
#define _SCLP_LIST_H_


//
//  Include section
//
#ifndef  _SCLP_ALLOCATOR_H_
#include <sclp_allocator.h>             // for scl_allocator
#endif

#ifndef  _SCLP_BASE_ALGO_H_
#include <sclp_base_algo.h>             // for scl_equal(), scl_lexicographical_compare()
#endif

#ifndef  _SCLP_LIST_BASE_H_
#include <sclp_list_base.h>             // for _scl_list_node and associated iterators
#endif


//
//  Template class scl_list
//
#ifdef  SCL_HAS_DEFAULT_TMPL_ARGS
template <class Type_, class Alloc_ = scl_allocator<Type_> >
#else
template <class Type_, class Alloc_>
#endif
class scl_list
{
public:
    // Private types
    typedef scl_list<Type_, Alloc_>                 Self_;
    typedef _scl_list_node<Type_>                   Node_;

    // Public types
    typedef Type_                                   value_type;
    typedef Alloc_                                  allocator_type;
    typedef typename Alloc_::pointer                pointer;
    typedef typename Alloc_::const_pointer          const_pointer;
    typedef typename Alloc_::reference              reference;
    typedef typename Alloc_::const_reference        const_reference;
    typedef typename Alloc_::size_type              size_type;
    typedef typename Alloc_::difference_type        difference_type;

    typedef _scl_list_iterator<Type_>               iterator;
    typedef _scl_list_const_iterator<Type_>         const_iterator;
    typedef scl_reverse_iterator<iterator>          reverse_iterator;
    typedef scl_reverse_iterator<const_iterator>    const_reverse_iterator;

    // 23.2.2.1 construct/copy/destroy
    scl_list();
    explicit scl_list(const Alloc_& alloc);

    explicit scl_list(size_type len);
    scl_list(size_type len, const Type_& value);
    scl_list(size_type len, const Type_& value, const Alloc_& alloc);

#ifdef  SCL_HAS_MEMBER_TEMPLATES
    template <class InputIterator_>
    scl_list(InputIterator_ first, InputIterator_ last);

    template <class InputIterator_>
    scl_list(InputIterator_ first, InputIterator_ last, const Alloc_& alloc);
#else
    scl_list(const_iterator first, const_iterator last);
    scl_list(const_iterator first, const_iterator last, const Alloc_& alloc);

    scl_list(const_pointer  first, const_pointer  last);
    scl_list(const_pointer  first, const_pointer  last, const Alloc_& alloc);
#endif

    scl_list(const Self_& other);
    ~scl_list();

    Self_&          operator=(const Self_& other);

#ifdef  SCL_HAS_MEMBER_TEMPLATES
    template <class InputIterator_>
    void            assign(InputIterator_ first, InputIterator_ last);
#else
    void            assign(const_iterator first, const_iterator last);
    void            assign(const_pointer  first, const_pointer  last);
#endif

    void            assign(size_type len, const Type_& value);
    allocator_type  get_allocator() const;

    // iterators
    iterator        begin();
    const_iterator  begin() const;
    iterator        end();
    const_iterator  end() const;

    reverse_iterator        rbegin();
    const_reverse_iterator  rbegin() const;
    reverse_iterator        rend();
    const_reverse_iterator  rend() const;

    // 23.2.2.2 capacity
    bool            empty() const;
    size_type       size() const;
    size_type       max_size() const;
    void            resize(size_type len);
    void            resize(size_type len, Type_ value);

    // element access
    reference       front();
    const_reference front() const;
    reference       back();
    const_reference back() const;

    // 23.2.2.3 modifiers
    void            push_front(const Type_& value);
    void            pop_front();
    void            push_back(const Type_& value);
    void            pop_back();

    iterator        insert(iterator position, const Type_& value);
    void            insert(iterator position, size_type len, const Type_& value);

#ifdef  SCL_HAS_MEMBER_TEMPLATES
    template <class InputIterator_>
    void            insert(iterator position, InputIterator_ first, InputIterator_ last);
#else
    void            insert(iterator position, const_iterator first, const_iterator last);
    void            insert(iterator position, const_pointer  first, const_pointer  last);
#endif

    iterator        erase(iterator position);
    iterator        erase(iterator first, iterator last);

    void            swap(Self_& other);
    void            clear();

    // 23.2.2.4 list operations
    void            splice(iterator position, Self_& other);
    void            splice(iterator position, Self_& other, iterator first);
    void            splice(iterator position, Self_& other, iterator first, iterator last);

    void            remove(const Type_& value);

#ifdef  SCL_HAS_MEMBER_TEMPLATES
    template <class Predicate_>
    void            remove_if(Predicate_ pred);
#else
    void            remove_if(bool (*pred)(const Type_&));
#endif

    void            unique();

#ifdef  SCL_HAS_MEMBER_TEMPLATES
    template <class Predicate_>
    void            unique(Predicate_ pred);
#else
    void            unique(bool (*pred)(const Type_&, const Type_&));
#endif

    void            merge(Self_& other);

#ifdef  SCL_HAS_MEMBER_TEMPLATES
    template <class Compare_>
    void            merge(Self_& other, Compare_ comp);
#else
    void            merge(Self_& other, bool (*comp)(const Type_&, const Type_&));
#endif

    void            sort();

#ifdef  SCL_HAS_MEMBER_TEMPLATES
    template <class Compare_>
    void            sort(Compare_ comp);
#else
    void            sort(bool (*comp)(const Type_&, const Type_&));
#endif

    void            reverse();

    // Test class invariants (nonstandard method).
    bool            _validate() const;

protected:
#ifdef  SCL_USE_ALLOCATOR_REBIND
    typedef typename Alloc_::template
        rebind<Node_>::other            node_allocator_type;
#else
    typedef Alloc_                      node_allocator_type;
#endif

    void            initialize();
    Node_*          allocate_node();
    void            deallocate_node(Node_* node, bool recycle);
    Node_*          create_node(const Type_& value);
    void            transfer(iterator position, iterator first, iterator last);
    void            clear_aux(bool recycle);
    void            finalize();
    static bool     equal_to_aux(const Type_& lhs, const Type_& rhs);
    static bool     less_aux    (const Type_& lhs, const Type_& rhs);

    //
    //  Note: Although a list is conceptually linear, this implementation maintains
    //  a circular linked list.  A dummy node called <head_> stores pointers to the
    //  first and last nodes in the list.  Given a list with four elements, we have:
    //      A <--> B <--> C <--> D
    //      head_->next_ : pointer to first element (A)
    //      head_->prev_ : pointer to last  element (D)
    //  Further, if the list is empty, we have:
    //      head_->next_ = head_->prev_ = head
    //
private:
    size_type           length_;        // length of list
    Node_*              head_;          // pointer to dummy head node
    Node_*              cache_;         // pointer to first node in free list
    allocator_type      alloc_;         // value allocator
    node_allocator_type node_alloc_;    // node allocator
};


template <class Type_, class Alloc_>
bool
operator==(const scl_list<Type_, Alloc_>& lhs,
           const scl_list<Type_, Alloc_>& rhs)
{
    return (lhs.size() == rhs.size()) && scl_equal(lhs.begin(), lhs.end(), rhs.begin());
}

template <class Type_, class Alloc_>
bool
operator< (const scl_list<Type_, Alloc_>& lhs,
           const scl_list<Type_, Alloc_>& rhs)
{
    return scl_lexicographical_compare(lhs.begin(), lhs.end(), rhs.begin(), rhs.end());
}

template <class Type_, class Alloc_>
bool
operator!=(const scl_list<Type_, Alloc_>& lhs,
           const scl_list<Type_, Alloc_>& rhs)
{
    return !(lhs == rhs);
}

template <class Type_, class Alloc_>
bool
operator<=(const scl_list<Type_, Alloc_>& lhs,
           const scl_list<Type_, Alloc_>& rhs)
{
    return !(rhs < lhs);
}

template <class Type_, class Alloc_>
bool
operator>=(const scl_list<Type_, Alloc_>& lhs,
           const scl_list<Type_, Alloc_>& rhs)
{
    return !(lhs < rhs);
}

template <class Type_, class Alloc_>
bool
operator> (const scl_list<Type_, Alloc_>& lhs,
           const scl_list<Type_, Alloc_>& rhs)
{
    return (rhs < lhs);
}

#ifdef  SCL_HAS_PARTIAL_SPEC_OVERLOAD
template <class Type_, class Alloc_>
void
scl_swap(scl_list<Type_, Alloc_>& lhs,
         scl_list<Type_, Alloc_>& rhs)
{
    lhs.swap(rhs);
}
#endif


#include <sclp_list.cc>


#endif  /* _SCLP_LIST_H_ */
