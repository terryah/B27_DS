/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: scl_functional.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Dec-2004     Initial implementation.
//
//  NOTICE:
//      This header implements functionality in the Standard C++ Library.
//      It may be included directly in application code.
//
//==============================================================================
#ifndef _SCL_FUNCTIONAL_H_
#define _SCL_FUNCTIONAL_H_


#ifndef  _SCL_CONFIG_H_
#include <scl_config.h>
#endif


#ifdef  SCL_USE_VENDOR_STD
#include <functional>

#define scl_unary_function              SCL_VENDOR_STD::unary_function
#define scl_binary_function             SCL_VENDOR_STD::binary_function
#define scl_plus                        SCL_VENDOR_STD::plus
#define scl_minus                       SCL_VENDOR_STD::minus
#define scl_multiplies                  SCL_VENDOR_STD::multiplies
#define scl_divides                     SCL_VENDOR_STD::divides
#define scl_modulus                     SCL_VENDOR_STD::modulus
#define scl_negate                      SCL_VENDOR_STD::negate
#define scl_equal_to                    SCL_VENDOR_STD::equal_to
#define scl_not_equal_to                SCL_VENDOR_STD::not_equal_to
#define scl_greater                     SCL_VENDOR_STD::greater
#define scl_less                        SCL_VENDOR_STD::less
#define scl_greater_equal               SCL_VENDOR_STD::greater_equal
#define scl_less_equal                  SCL_VENDOR_STD::less_equal
#define scl_logical_and                 SCL_VENDOR_STD::logical_and
#define scl_logical_or                  SCL_VENDOR_STD::logical_or
#define scl_logical_not                 SCL_VENDOR_STD::logical_not
#define scl_unary_negate                SCL_VENDOR_STD::unary_negate
#define scl_not1                        SCL_VENDOR_STD::not1
#define scl_binary_negate               SCL_VENDOR_STD::binary_negate
#define scl_not2                        SCL_VENDOR_STD::not2
#define scl_binder1st                   SCL_VENDOR_STD::binder1st
#define scl_bind1st                     SCL_VENDOR_STD::bind1st
#define scl_binder2nd                   SCL_VENDOR_STD::binder2nd
#define scl_bind2nd                     SCL_VENDOR_STD::bind2nd
#define scl_pointer_to_unary_function   SCL_VENDOR_STD::pointer_to_unary_function
#define scl_pointer_to_binary_function  SCL_VENDOR_STD::pointer_to_binary_function
#define scl_ptr_fun                     SCL_VENDOR_STD::ptr_fun
#define scl_mem_fun_t                   SCL_VENDOR_STD::mem_fun_t
#define scl_mem_fun1_t                  SCL_VENDOR_STD::mem_fun1_t
#define scl_mem_fun                     SCL_VENDOR_STD::mem_fun
#define scl_mem_fun_ref_t               SCL_VENDOR_STD::mem_fun_ref_t
#define scl_mem_fun1_ref_t              SCL_VENDOR_STD::mem_fun1_ref_t
#define scl_mem_fun_ref                 SCL_VENDOR_STD::mem_fun_ref
#define scl_const_mem_fun_t             SCL_VENDOR_STD::const_mem_fun_t
#define scl_const_mem_fun1_t            SCL_VENDOR_STD::const_mem_fun1_t
#define scl_const_mem_fun_ref_t         SCL_VENDOR_STD::const_mem_fun_ref_t
#define scl_const_mem_fun1_ref_t        SCL_VENDOR_STD::const_mem_fun1_ref_t

#else
#include <sclp_functional.h>

#endif  /* SCL_USE_VENDOR_STD */


#endif  /* _SCL_FUNCTIONAL_H_ */
