/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
//
//  FILE: sclp_rbtree.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Apr-2005     Initial implementation.
//
//  NOTICE:
//      This header is used internally by the Standard C++ Library.
//      It should not be included directly in application code.
//
//==============================================================================
#ifndef _SCLP_RBTREE_H_
#define _SCLP_RBTREE_H_


//
//  Include section
//
#ifndef  _SCLP_UTILITY_H_
#include <sclp_utility.h>               // for scl_pair
#endif

#ifndef  _SCLP_BASE_ALGO_H_
#include <sclp_base_algo.h>             // for scl_equal(), scl_lexicographical_compare()
#endif

#ifndef  _SCLP_RBTREE_BASE_H_
#include <sclp_rbtree_base.h>           // for _scl_rbtree_node and associated iterators
#endif


//
//  Template class scl_rbtree
//
template <class Key_, class Value_, class KeyVal_, class Comp_, class Alloc_>
class scl_rbtree
{
public:
    // Private types
    typedef scl_rbtree<Key_, Value_, KeyVal_, Comp_, Alloc_>    Self_;
    typedef _scl_rbtree_base_node                   BNode_;
    typedef _scl_rbtree_node<Value_>                VNode_;

    // Public types
    typedef Key_                                    key_type;
    typedef Value_                                  value_type;
    typedef Comp_                                   key_compare;
    typedef Alloc_                                  allocator_type;
    typedef typename Alloc_::pointer                pointer;
    typedef typename Alloc_::const_pointer          const_pointer;
    typedef typename Alloc_::reference              reference;
    typedef typename Alloc_::const_reference        const_reference;
    typedef typename Alloc_::size_type              size_type;
    typedef typename Alloc_::difference_type        difference_type;

    typedef _scl_rbtree_iterator<Value_>            iterator;
    typedef _scl_rbtree_const_iterator<Value_>      const_iterator;
    typedef scl_reverse_iterator<iterator>          reverse_iterator;
    typedef scl_reverse_iterator<const_iterator>    const_reverse_iterator;

    // construct/copy/destroy
    scl_rbtree(bool multi);
    scl_rbtree(bool multi, const Comp_& comp);
    scl_rbtree(bool multi, const Comp_& comp, const Alloc_& alloc);

    scl_rbtree(const Self_& other);
    ~scl_rbtree();

    Self_&          operator=(const Self_& other);
    allocator_type  get_allocator() const;
    key_compare     key_comp() const    { return key_comp_; }

    // iterators
    iterator        begin();
    const_iterator  begin() const;
    iterator        end();
    const_iterator  end() const;

    reverse_iterator        rbegin();
    const_reverse_iterator  rbegin() const;
    reverse_iterator        rend();
    const_reverse_iterator  rend() const;

    // capacity
    bool            empty() const;
    size_type       size() const;
    size_type       max_size() const;

    // modifiers
    scl_pair<iterator, bool>    insert(const Value_& value);
    iterator        insert(iterator pos, const Value_& value);

#ifdef  SCL_HAS_MEMBER_TEMPLATES
    template <class InputIterator_>
    void            insert(InputIterator_ first, InputIterator_ last);
#else
    void            insert(const_iterator first, const_iterator last);
    void            insert(const_pointer  first, const_pointer  last);
#endif

    void            erase(iterator pos);
    size_type       erase(const Key_& key);
    void            erase(iterator first, iterator last);
    void            swap(Self_& other);
    void            clear();

    // search
    iterator        find (const Key_& key);
    const_iterator  find (const Key_& key) const;
    size_type       count(const Key_& key) const;

    iterator        lower_bound(const Key_& key);
    const_iterator  lower_bound(const Key_& key) const;
    iterator        upper_bound(const Key_& key);
    const_iterator  upper_bound(const Key_& key) const;

    scl_pair<iterator, iterator>                equal_range(const Key_& key);
    scl_pair<const_iterator, const_iterator>    equal_range(const Key_& key) const;

    // Test class invariants (nonstandard method).
    bool            _validate() const;

protected:
#ifdef  SCL_USE_ALLOCATOR_REBIND
    typedef typename Alloc_::template
        rebind<VNode_>::other           node_allocator_type;
#else
    typedef Alloc_                      node_allocator_type;
#endif

    void            initialize();
    void            finalize();
    VNode_*         allocate_node();
    void            deallocate_node(VNode_* node);
    VNode_*         create_node(const Value_& value);
    void            destroy_node(VNode_* node);
    BNode_*         copy_tree(const BNode_* src_node, BNode_* dst_parent, BNode_* src_nil);
    void            copy_aux(const Self_& other);
    void            erase_tree(BNode_* node);
    void            clear_aux();
    iterator        insert_aux(BNode_* x_node, BNode_* y_node, const Value_& value);
    BNode_*&        root()                  { return head_->parent_;    }
    const BNode_*   root() const            { return head_->parent_;    }
    BNode_*&        leftmost()              { return head_->left_;      }
    const BNode_*   leftmost() const        { return head_->left_;      }
    BNode_*&        rightmost()             { return head_->right_;     }
    const BNode_*   rightmost() const       { return head_->right_;     }
    bool            kcompare(const   Key_& lkey, const BNode_* rval) const;
    bool            kcompare(const BNode_* lval, const   Key_& rkey) const;
    bool            vcompare(const Value_& lval, const BNode_* rval) const;
    bool            vcompare(const BNode_* lval, const Value_& rval) const;
    bool            vcompare(const BNode_* lval, const BNode_* rval) const;

private:
    bool                multi_;             // true if duplicate keys are allowed
    size_type           size_;              // number of nodes in tree
    VNode_*             head_;              // pointer to dummy head node
    VNode_*             nil_;               // NIL (sentinel) node
    key_compare         key_comp_;          // compare two keys
    KeyVal_             key_value_;         // extract key from value
    allocator_type      alloc_;             // value allocator
    node_allocator_type node_alloc_;        // node allocator
};


template <class Key_, class Value_, class KeyVal_, class Comp_, class Alloc_>
bool
operator==(const scl_rbtree<Key_, Value_, KeyVal_, Comp_, Alloc_>& lhs,
           const scl_rbtree<Key_, Value_, KeyVal_, Comp_, Alloc_>& rhs)
{
    return (lhs.size() == rhs.size()) && scl_equal(lhs.begin(), lhs.end(), rhs.begin());
}

template <class Key_, class Value_, class KeyVal_, class Comp_, class Alloc_>
bool
operator< (const scl_rbtree<Key_, Value_, KeyVal_, Comp_, Alloc_>& lhs,
           const scl_rbtree<Key_, Value_, KeyVal_, Comp_, Alloc_>& rhs)
{
    return scl_lexicographical_compare(lhs.begin(), lhs.end(), rhs.begin(), rhs.end());
}

template <class Key_, class Value_, class KeyVal_, class Comp_, class Alloc_>
bool
operator!=(const scl_rbtree<Key_, Value_, KeyVal_, Comp_, Alloc_>& lhs,
           const scl_rbtree<Key_, Value_, KeyVal_, Comp_, Alloc_>& rhs)
{
    return !(lhs == rhs);
}

template <class Key_, class Value_, class KeyVal_, class Comp_, class Alloc_>
bool
operator<=(const scl_rbtree<Key_, Value_, KeyVal_, Comp_, Alloc_>& lhs,
           const scl_rbtree<Key_, Value_, KeyVal_, Comp_, Alloc_>& rhs)
{
    return !(rhs < lhs);
}

template <class Key_, class Value_, class KeyVal_, class Comp_, class Alloc_>
bool
operator>=(const scl_rbtree<Key_, Value_, KeyVal_, Comp_, Alloc_>& lhs,
           const scl_rbtree<Key_, Value_, KeyVal_, Comp_, Alloc_>& rhs)
{
    return !(lhs < rhs);
}

template <class Key_, class Value_, class KeyVal_, class Comp_, class Alloc_>
bool
operator> (const scl_rbtree<Key_, Value_, KeyVal_, Comp_, Alloc_>& lhs,
           const scl_rbtree<Key_, Value_, KeyVal_, Comp_, Alloc_>& rhs)
{
    return (rhs < lhs);
}

#ifdef  SCL_HAS_PARTIAL_SPEC_OVERLOAD
template <class Key_, class Value_, class KeyVal_, class Comp_, class Alloc_>
void
scl_swap(scl_rbtree<Key_, Value_, KeyVal_, Comp_, Alloc_>& lhs,
         scl_rbtree<Key_, Value_, KeyVal_, Comp_, Alloc_>& rhs)
{
    lhs.swap(rhs);
}
#endif


#include <sclp_rbtree.cc>


#endif  /* _SCLP_RBTREE_H_ */
