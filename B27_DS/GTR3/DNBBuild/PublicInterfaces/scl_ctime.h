/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: scl_ctime.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Aug-2004     Initial implementation.
//
//  NOTICE:
//      This header implements functionality in the Standard C++ Library.
//      It may be included directly in application code.
//
//==============================================================================
#ifndef _SCL_CTIME_H_
#define _SCL_CTIME_H_


#ifndef  _SCL_CONFIG_H_
#include <scl_config.h>
#endif

#include <time.h>


//
//  Most UNIX systems provide reentrant forms of localtime() and gmtime().
//  We shall define the same on all platforms.
//
SCL_STD_EXPORT
struct tm *
scl_localtime_r(const time_t *ctime, struct tm *stime);

SCL_STD_EXPORT
struct tm *
scl_gmtime_r(const time_t *ctime, struct tm *stime);


#endif  /* _SCL_CTIME_H_ */
