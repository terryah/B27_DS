/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: sclp_numeric.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Oct-2004     Initial implementation.
//
//  NOTICE:
//      This header is used internally by the Standard C++ Library.
//      It should not be included directly in application code.
//
//==============================================================================
#ifndef _SCLP_NUMERIC_H_
#define _SCLP_NUMERIC_H_


//
//  Include section
//
#ifndef  _SCLP_BASE_ITER_H_
#include <sclp_base_iter.h>             // for _scl_value_type()
#endif


//
//  26.4 Generalized Numeric Operations
//

//
//  26.4.1 Template function scl_accumulate
//
template <class InpIter_, class Type_>
Type_
scl_accumulate(InpIter_ first, InpIter_ last, Type_ init);

template <class InpIter_, class Type_, class BinOp_>
Type_
scl_accumulate(InpIter_ first, InpIter_ last, Type_ init, BinOp_ sumOp);


//
//  26.4.2 Template function scl_inner_product
//
template <class InpIter1_, class InpIter2_, class Type_>
Type_
scl_inner_product(InpIter1_ first1, InpIter1_ last1, InpIter2_ first2, Type_ init);

template <class InpIter1_, class InpIter2_, class Type_, class BinOp1_, class BinOp2_>
Type_
scl_inner_product(InpIter1_ first1, InpIter1_ last1, InpIter2_ first2, Type_ init,
    BinOp1_ sumOp, BinOp2_ prodOp);


//
//  26.4.3 Template function scl_partial_sum
//
template <class InpIter_, class OutIter_>
OutIter_
scl_partial_sum(InpIter_ first, InpIter_ last, OutIter_ result);

template <class InpIter_, class OutIter_, class BinOp_>
OutIter_
scl_partial_sum(InpIter_ first, InpIter_ last, OutIter_ result, BinOp_ sumOp);


//
//  26.4.4 Template function scl_adjacent_difference
//
template <class InpIter_, class OutIter_>
OutIter_
scl_adjacent_difference(InpIter_ first, InpIter_ last, OutIter_ result);

template <class InpIter_, class OutIter_, class BinOp_>
OutIter_
scl_adjacent_difference(InpIter_ first, InpIter_ last, OutIter_ result, BinOp_ diffOp);


#include <sclp_numeric.cc>


#endif  /* _SCLP_NUMERIC_H_ */
