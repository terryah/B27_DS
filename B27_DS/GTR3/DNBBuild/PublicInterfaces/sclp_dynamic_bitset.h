/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: sclp_bitset.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      rtl     06/08/2005     Initial implementation.
//
//  NOTICE:
//      This header is used internally by the Standard C++ Library.
//      It should not be included directly in application code.
//
//==============================================================================
#ifndef _SCLP_DYNAMIC_BITSET_H_
#define _SCLP_DYNAMIC_BITSET_H_


//
//  Include section
//

#ifdef  SCL_ENABLE_IO
#include <scl_iostream.h>
#endif

#ifndef  _SCL_CLIMITS_H_
#include <scl_climits.h>                // for CHAR_BIT
#endif

#ifndef  _SCL_IOSFWD_H_
#include <scl_iosfwd.h>                 // for istream, ostream
#endif

#ifndef  _SCLP_BITSET_BASE_H_
#include <sclp_bitset_base.h>           // for _scl_bitset_base
#endif



class SCL_STD_EXPORT scl_dynamic_bitset : public _scl_bitset_base
{
public:
    // Some useful typedefs
    typedef _scl_bitset_base            Base_;
    typedef _scl_bitset_reference       reference;
    typedef scl_dynamic_bitset          Self_;


	//Constructors/Copy Constructor/Destructor
    scl_dynamic_bitset();
	explicit scl_dynamic_bitset(size_t size);
	scl_dynamic_bitset(size_t size, unsigned long value);
	scl_dynamic_bitset(size_t size, const scl_string& cstr);
    scl_dynamic_bitset(size_t size, const scl_string& cstr, size_t cpos);
    //scl_dynamic_bitset(size_t size, const scl_string& cstr, size_t cpos, size_t clen);
    scl_dynamic_bitset(const Self_& other);
    ~scl_dynamic_bitset();

	//Assignment operators
    Self_&          operator=(const Self_& other);
	Self_&          operator=(unsigned long value);
    Self_&          operator&=(const Self_& rhs);
    Self_&          operator|=(const Self_& rhs);
    Self_&          operator^=(const Self_& rhs);
	Self_&          operator<<=(size_t count);
    Self_&          operator>>=(size_t count);

    // element access 
    bool            operator()(size_t pos) const;
	reference       operator()(size_t pos);


	//Logical Operators
	bool            operator==(const Self_& rhs) const;
    bool            operator!=(const Self_& rhs) const;

	// Member functions

	void            resize(size_t);
	Self_&          set();
    Self_&          setBit(size_t pos);  //Sets(ie.sets to TRUE) the bit with index "pos"
	Self_&          reset();
    Self_&          resetBit(size_t pos); //Clears(ie.sets to FALSE) the bit with index "pos"
    Self_&          flip();
    Self_&          flip(size_t pos);
	Self_           operator~() const;
	Self_           operator<<(size_t count) const;
    Self_           operator>>(size_t count) const;



	// Other global methods
	friend Self_ SCL_STD_EXPORT   operator!(const Self_& other);
	friend Self_ SCL_STD_EXPORT   operator&(const Self_& lhs, const Self_& rhs);
	friend Self_ SCL_STD_EXPORT   operator|(const Self_& lhs, const Self_& rhs);
	friend Self_ SCL_STD_EXPORT   operator^(const Self_& lhs, const Self_& rhs);

#ifdef  SCL_ENABLE_IO
    friend ostream& operator<<(ostream& os, const Self_& item)
	{
		os << item.to_string();
		return os;
	}
#endif


private:

    // Data members.
	enum { WordBits = CHAR_BIT * sizeof(WordType) };
	size_t          calcNumWords(){ return (nbits_ == 0) ? 1 : ((nbits_ - 1) / WordBits + 1); }
	void            allocateBuffer();

    size_t          nbits_;     // Number of bits in bitset
    WordType*       buffer_;      // Pointer to word array
	
  
  
};

#endif  /* _SCLP_BITSET_BASE_H_ */
