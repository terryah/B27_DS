/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: sclp_numeric.cc
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Oct-2004     Initial implementation.
//
//==============================================================================

//
//  26.4.1 Template function scl_accumulate
//
template <class InpIter_, class Type_>
Type_
scl_accumulate(InpIter_ first, InpIter_ last, Type_ init)
{
    for (; first != last; ++first)
        init = init + *first;
    return init;
}

template <class InpIter_, class Type_, class BinOp_>
Type_
scl_accumulate(InpIter_ first, InpIter_ last, Type_ init, BinOp_ sumOp)
{
    for (; first != last; ++first)
        init = sumOp(init, *first);
    return init;
}


//
//  26.4.2 Template function scl_inner_product
//
template <class InpIter1_, class InpIter2_, class Type_>
Type_
scl_inner_product(InpIter1_ first1, InpIter1_ last1, InpIter2_ first2, Type_ init)
{
    for (; first1 != last1; ++first1, ++first2)
        init = init + (*first1 * *first2);
    return init;
}

template <class InpIter1_, class InpIter2_, class Type_, class BinOp1_, class BinOp2_>
Type_
scl_inner_product(InpIter1_ first1, InpIter1_ last1, InpIter2_ first2, Type_ init,
    BinOp1_ sumOp, BinOp2_ prodOp)
{
    for (; first1 != last1; ++first1, ++first2)
        init = sumOp(init, prodOp(*first1, *first2));
    return init;
}


//
//  26.4.3 Template function scl_partial_sum
//
template <class InpIter_, class OutIter_, class Type_>
OutIter_
_scl_partial_sum(InpIter_ first, InpIter_ last, OutIter_ result, Type_*)
{
    if (first == last)
        return result;

    Type_   value(*first);
    *result = value;

    for (++first, ++result; first != last; ++first, ++result)
    {
        value = value + *first;
        *result = value;
    }

    return result;
}

template <class InpIter_, class OutIter_, class BinOp_, class Type_>
OutIter_
_scl_partial_sum(InpIter_ first, InpIter_ last, OutIter_ result, BinOp_ sumOp, Type_*)
{
    if (first == last)
        return result;

    Type_   value(*first);
    *result = value;

    for (++first, ++result; first != last; ++first, ++result)
    {
        value = sumOp(value, *first);
        *result = value;
    }

    return result;
}

template <class InpIter_, class OutIter_>
OutIter_
scl_partial_sum(InpIter_ first, InpIter_ last, OutIter_ result)
{
    return _scl_partial_sum(first, last, result,        _scl_value_type(first));
}

template <class InpIter_, class OutIter_, class BinOp_>
OutIter_
scl_partial_sum(InpIter_ first, InpIter_ last, OutIter_ result, BinOp_ sumOp)
{
    return _scl_partial_sum(first, last, result, sumOp, _scl_value_type(first));
}


//
//  26.4.4 Template function scl_adjacent_difference
//
template <class InpIter_, class OutIter_, class Type_>
OutIter_
_scl_adjacent_difference(InpIter_ first, InpIter_ last, OutIter_ result, Type_*)
{
    if (first == last)
        return result;

    Type_   value(*first);
    *result = value;

    for (++first, ++result; first != last; ++first, ++result)
    {
        Type_   tmp(*first);
        *result = tmp - value;
        value = tmp;
    }

    return result;
}

template <class InpIter_, class OutIter_, class BinOp_, class Type_>
OutIter_
_scl_adjacent_difference(InpIter_ first, InpIter_ last, OutIter_ result, BinOp_ diffOp, Type_*)
{
    if (first == last)
        return result;

    Type_   value(*first);
    *result = value;

    for (++first, ++result; first != last; ++first, ++result)
    {
        Type_   tmp(*first);
        *result = diffOp(tmp, value);
        value = tmp;
    }

    return result;
}

template <class InpIter_, class OutIter_>
OutIter_
scl_adjacent_difference(InpIter_ first, InpIter_ last, OutIter_ result)
{
    return _scl_adjacent_difference(first, last, result,         _scl_value_type(first));
}

template <class InpIter_, class OutIter_, class BinOp_>
OutIter_
scl_adjacent_difference(InpIter_ first, InpIter_ last, OutIter_ result, BinOp_ diffOp)
{
    return _scl_adjacent_difference(first, last, result, diffOp, _scl_value_type(first));
}
