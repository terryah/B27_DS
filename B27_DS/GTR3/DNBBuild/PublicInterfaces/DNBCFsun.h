/** @CAA2Required */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 1997
//==============================================================================
//
//  FILE: DNBCFsun.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     23-Jun-1997     Initial implementation.
//
//  OVERVIEW:
//      Attributes of Sun Solaris 32-bit (C++ 5.7).
//      Do not include this header directly in application code.
//
//==============================================================================
#define DNB_HAS_POSIX_THREADS       DNB_YES
#define DNB_HAS_WIN32_THREADS       DNB_NO
#define DNB_HAS_PRAGMA_ONCE         DNB_NO
#define DNB_HAS_BOOL                DNB_NO
#define DNB_HAS_THROW_SPECS         DNB_YES
#define DNB_HAS_STD_NAMESPACE       DNB_NO
#define DNB_HAS_FUNC_INST           DNB_NO              // Problems in DNBMath.
#define DNB_HAS_CLASS_INST          DNB_YES
#define DNB_HAS_DOUBLE_ABS          DNB_NO
#define DNB_HAS_FLOAT_MATH          DNB_NO
#define DNB_HAS_FLOAT_MATHF         DNB_NO
#define DNB_HAS_FLOAT_FMATH         DNB_NO
#define DNB_HAS_ATOMIC_LOCK         DNB_NO
#define DNB_HAS_ATOMIC_INTEGER      DNB_YES
#define DNB_HAS_FRIEND_TEMPLATE     DNB_YES


#if     __SUNPRO_CC_COMPAT != 4
#error  "Compiler option -compat=4 has not been specified."
#endif


//
//  Provide a standard-conforming implementation of the allocation functions.
//
#include <exception.h>              /* For xalloc. */

struct  nothrow_t   { int dummy_; };
const   nothrow_t   nothrow = { 0 };

inline  void*
operator new( size_t size, const nothrow_t& )
    throw( )
{
    void   *ptr;

    try
    {
        ptr = ::operator new( size );
    }
    catch ( xalloc& )
    {
        ptr = NULL;
    }

    return (ptr);
}


extern "C"
{
    extern  int
    DNBAtomicSwap( volatile int* target, int value );
}
