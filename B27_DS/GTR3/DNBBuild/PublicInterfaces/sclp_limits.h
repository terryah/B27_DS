/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: sclp_limits.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Aug-2004     Initial implementation.
//
//  NOTICE:
//      This header is used internally by the Standard C++ Library.
//      It should not be included directly in application code.
//
//==============================================================================
#ifndef _SCLP_LIMITS_H_
#define _SCLP_LIMITS_H_


//
//  ENUM scl_float_denorm_style
//
enum scl_float_denorm_style
{
    scl_denorm_indeterminate        = -1,
    scl_denorm_absent               =  0,
    scl_denorm_present              =  1
};


//
//  ENUM scl_float_round_style
//
enum scl_float_round_style
{
    scl_round_indeterminate         = -1,
    scl_round_toward_zero           =  0,
    scl_round_to_nearest            =  1,
    scl_round_toward_infinity       =  2,
    scl_round_toward_neg_infinity   =  3
};


//
//  TEMPLATE CLASS scl_numeric_limits
//
template <class Type_>
class scl_numeric_limits
{
public:
    static const bool   is_specialized;
    static const int    digits;
    static const int    digits10;
    static const bool   is_signed;
    static const bool   is_integer;
    static const bool   is_exact;
    static const int    radix;
    static const int    min_exponent;
    static const int    min_exponent10;
    static const int    max_exponent;
    static const int    max_exponent10;
    static const bool   has_infinity;
    static const bool   has_quiet_NaN;
    static const bool   has_signaling_NaN;
    static const bool   has_denorm_loss;
    static const bool   is_iec559;
    static const bool   is_bounded;
    static const bool   is_modulo;
    static const bool   traps;
    static const bool   tinyness_before;

    static const scl_float_denorm_style has_denorm;
    static const scl_float_round_style  round_style;

    static Type_        min()           SCL_NOTHROW;
    static Type_        max()           SCL_NOTHROW;
    static Type_        epsilon()       SCL_NOTHROW;
    static Type_        round_error()   SCL_NOTHROW;
    static Type_        infinity()      SCL_NOTHROW;
    static Type_        quiet_NaN()     SCL_NOTHROW;
    static Type_        signaling_NaN() SCL_NOTHROW;
    static Type_        denorm_min()    SCL_NOTHROW;
};


//
//  MACRO SCL_DECLARE_LIMITS
//
#define SCL_DECLARE_LIMITS(Type_)                       \
SCL_SPECIALIZE_CLASS                                    \
class SCL_STD_EXPORT scl_numeric_limits<Type_>          \
{                                                       \
public:                                                 \
    static const bool   is_specialized;                 \
    static const int    digits;                         \
    static const int    digits10;                       \
    static const bool   is_signed;                      \
    static const bool   is_integer;                     \
    static const bool   is_exact;                       \
    static const int    radix;                          \
    static const int    min_exponent;                   \
    static const int    min_exponent10;                 \
    static const int    max_exponent;                   \
    static const int    max_exponent10;                 \
    static const bool   has_infinity;                   \
    static const bool   has_quiet_NaN;                  \
    static const bool   has_signaling_NaN;              \
    static const bool   has_denorm_loss;                \
    static const bool   is_iec559;                      \
    static const bool   is_bounded;                     \
    static const bool   is_modulo;                      \
    static const bool   traps;                          \
    static const bool   tinyness_before;                \
    static const scl_float_denorm_style has_denorm;     \
    static const scl_float_round_style  round_style;    \
    static Type_        min()           SCL_NOTHROW;    \
    static Type_        max()           SCL_NOTHROW;    \
    static Type_        epsilon()       SCL_NOTHROW;    \
    static Type_        round_error()   SCL_NOTHROW;    \
    static Type_        infinity()      SCL_NOTHROW;    \
    static Type_        quiet_NaN()     SCL_NOTHROW;    \
    static Type_        signaling_NaN() SCL_NOTHROW;    \
    static Type_        denorm_min()    SCL_NOTHROW;    \
}


//
//  Specialization of scl_numeric_limits<> for every built-in type.
//
SCL_DECLARE_LIMITS(char);
SCL_DECLARE_LIMITS(signed char);
SCL_DECLARE_LIMITS(unsigned char);

#ifdef  SCL_HAS_DISTINCT_WCHAR
SCL_DECLARE_LIMITS(wchar_t);
#endif

SCL_DECLARE_LIMITS(short);
SCL_DECLARE_LIMITS(unsigned short);

SCL_DECLARE_LIMITS(int);
SCL_DECLARE_LIMITS(unsigned int);

SCL_DECLARE_LIMITS(long);
SCL_DECLARE_LIMITS(unsigned long);

#ifdef  SCL_HAS_LLONG
SCL_DECLARE_LIMITS(SCL_LLONG_TYPE);
SCL_DECLARE_LIMITS(unsigned SCL_LLONG_TYPE);
#endif

#ifdef  SCL_HAS_DISTINCT_BOOL
SCL_DECLARE_LIMITS(bool);
#endif

SCL_DECLARE_LIMITS(float);
SCL_DECLARE_LIMITS(double);
SCL_DECLARE_LIMITS(long double);


#include <sclp_limits.cc>


#endif  /* _SCLP_LIMITS_H_ */
