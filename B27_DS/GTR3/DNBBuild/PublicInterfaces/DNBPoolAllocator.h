/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2003
//==============================================================================
//
//  FILE: DNBPoolAllocator.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     29-Aug-2003     Initial implementation.
//      bkh     03-Oct-2003     Added documentation.
//      jod     14-Aug-2008     Moved class to Standard C++ Library.
//
//==============================================================================
#ifndef _DNBPOOLALLOCATOR_H_
#define _DNBPOOLALLOCATOR_H_


//
//  Include section
//
#ifndef  _SCL_CONFIG_H_
#include <scl_config.h>
#endif

#ifndef  _SCL_IOSFWD_H_
#include <scl_iosfwd.h>                 // for ostream
#endif


//
//  Forward declarations
//
class DNBBlockAllocator;




class SCL_STD_EXPORT DNBPoolAllocator
{
public:
    /**
     *  Constructor.
     */
    DNBPoolAllocator( )
        SCL_THROWS((scl_bad_alloc));

    /**
     *  Destructor.
     */
    ~DNBPoolAllocator( )
        SCL_THROWS_NONE;

    /**
     *  Allocates memory of size bytes.
     */
    void *
    allocate( size_t size )
        SCL_THROWS((scl_bad_alloc));

    /**
     *  Deallocates memory of size bytes.
     */
    void
    deallocate( void *ptr, size_t size )
        SCL_THROWS_NONE;

    /**
     *  Frees unused memory in the corresponding block allocator.
     */
    void
    compact( )
        SCL_THROWS_NONE;

    /**
     *  Function to check if the pool allocator is enabled.
     */
    bool
    isEnabled( ) const
        SCL_THROWS_NONE;

    /**
     *  To the output.
     */
    void
    dump( ostream &ostr ) const
        SCL_THROWS_NONE;

    /**
     *  Returns the global pool allocator.
     */
    static DNBPoolAllocator &
    Global( )
        SCL_THROWS_NONE;

private:
    //
    //  Prohibit copy construction and assignment operations.
    //
    DNBPoolAllocator( const DNBPoolAllocator & )
        SCL_THROWS_NONE;

    void
    operator=( const DNBPoolAllocator & )
        SCL_THROWS_NONE;

    //
    //  Define the number of fixed-block allocators (or buckets) used by the
    //  pool manager.  This value is derived from the current allocation
    //  strategy.
    //
    enum { BucketCount = 128 };

    //
    //  Define the maximum block-size of a bucket.  This value is derived from
    //  the current allocation strategy.
    //
    enum { MaxBlockSize = 7680 };

    //
    //  Define some utility routines.
    //
    /**
     *  Method to find the bucket size corresponding to the index.
     *  @param idx
     *      index
     */
    static size_t
    GetBucketSize( size_t idx )
        SCL_THROWS_NONE;

    /**
     *  Obtain the index given the bucket size.
     *  @param size
     *      bucketcount
     */
    static size_t
    GetBucketIndex( size_t size )
        SCL_THROWS_NONE;

    //
    //  Data members of DNBPoolAllocator.
    //
    DNBBlockAllocator  *bucketDB_[ BucketCount ];
    bool                isEnabled_;

    friend class DNBPoolAllocatorTest;          // For testing purposes.
};




#endif  /* _DNBPOOLALLOCATOR_H_ */
