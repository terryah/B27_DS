/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: sclp_string.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Sep-2004     Initial implementation.
//
//  NOTICE:
//      This header is used internally by the Standard C++ Library.
//      It should not be included directly in application code.
//
//==============================================================================
#ifndef _SCLP_STRING_H_
#define _SCLP_STRING_H_


//
//  Include section
//
#ifndef  _SCL_IOSFWD_H_
#include <scl_iosfwd.h>                 // for istream, ostream
#endif

#ifndef  _SCLP_ALLOCATOR_H_
#include <sclp_allocator.h>             // for scl_allocator
#endif

#ifndef  _SCLP_CHAR_TRAITS_H_
#include <sclp_char_traits.h>           // for scl_char_traits
#endif

#ifndef  _SCLP_POINTER_ITER_H_
#include <sclp_pointer_iter.h>          // for scl_pointer_iterator
#endif


//
//  Internal class _scl_string_base.
//
template <class Type_, class Traits_, class Alloc_>
class _scl_string_base
{
public:
    typedef _scl_string_base<Type_, Traits_, Alloc_>    Self_;
    typedef typename Alloc_::size_type                  size_type;

    size_type       size() const            { return length_;           }
    size_type       max_size() const        { return alloc_.max_size(); }
    size_type       capacity() const        { return capacity_;         }

    const Type_*    c_str() const           { return first_;            }
    const Type_*    data()  const           { return first_;            }
    Alloc_          get_allocator() const   { return alloc_;            }

    // Test class invariants (nonstandard method).
    bool            _validate() const;

protected:
    static const Type_  NullChar;       // End-of-string delimiter.

    static size_type    Min(size_type lhs, size_type rhs);

    _scl_string_base();
    _scl_string_base(const Alloc_& alloc);

    _scl_string_base(const Self_& cstr);

    _scl_string_base(const Type_* cstr, size_type clen);
    _scl_string_base(const Type_* cstr, size_type clen, const Alloc_& alloc);

    _scl_string_base(size_type clen, Type_ cval);
    _scl_string_base(size_type clen, Type_ cval, const Alloc_& alloc);

    ~_scl_string_base();

    void            operator=(const Self_& cstr);

    void            check_index(size_type pos) const;
    void            check_range(size_type pos, size_type& len) const;

    Type_*          begin_aux()         { return first_; }
    const Type_*    begin_aux() const   { return first_; }
    Type_*          end_aux()           { return first_ + length_; }
    const Type_*    end_aux() const     { return first_ + length_; }

    void            swap_aux(Self_& cstr);

    void            reserve_aux(size_type new_cap);
    const Type_*    reserve_aux(size_type new_cap, const Type_* cstr);

    void            resize_aux(size_type clen, Type_ cval);

    void            assign_aux(size_type clen, Type_ cval);
    void            assign_aux(const Type_* cstr, size_type clen);

    void            append_aux(Type_ cval);
    void            append_aux(size_type clen, Type_ cval);
    void            append_aux(const Type_* cstr, size_type clen);

    void            replace_aux(size_type pos, size_type len, size_type clen, Type_ cval);
    void            replace_aux(size_type pos, size_type len, const Type_* cstr, size_type clen);

    // Note: HP-UX C++ A.3.31 requires BufferSize to be protected.
protected:
    // Size of buffer used to store short strings.
    enum { BufferSize = 31 };

private:
    void            initialize();
    void            set_length(size_type len);
    Type_*          allocate(size_type &len);       // Note: <len> is modified
    void            destroy();

    // Multiplier used to compute string capacity.
    enum { IncrementSize = 32 };

    // Data members
    Type_*      first_;                     // Pointer to first character
    size_type   length_;                    // String length (excluding EOS)
    size_type   capacity_;                  // String capacity (excluding EOS)
    Type_       buffer_[BufferSize + 1];    // Local buffer for short strings
    Alloc_      alloc_;                     // Value (character) allocator
};


//
//  Public class scl_basic_string.
//
template <class Type_, class Traits_, class Alloc_>
class scl_basic_string : public _scl_string_base<Type_, Traits_, Alloc_>
{
public:
    typedef _scl_string_base<Type_, Traits_, Alloc_>    Base_;
    typedef scl_basic_string<Type_, Traits_, Alloc_>    Self_;

    // 21.3 types
    typedef          Traits_                            traits_type;
    typedef typename Traits_::char_type                 value_type;
    typedef          Alloc_                             allocator_type;
    typedef typename Alloc_::size_type                  size_type;
    typedef typename Alloc_::difference_type            difference_type;

    typedef typename Alloc_::reference                  reference;
    typedef typename Alloc_::const_reference            const_reference;
    typedef typename Alloc_::pointer                    pointer;
    typedef typename Alloc_::const_pointer              const_pointer;

    typedef scl_pointer_iterator<Type_>                 iterator;
    typedef scl_pointer_const_iterator<Type_>           const_iterator;
    typedef scl_reverse_iterator<iterator>              reverse_iterator;
    typedef scl_reverse_iterator<const_iterator>        const_reverse_iterator;

#ifdef  SCL_HAS_STATIC_CONST_INIT
    static const size_type  npos = SCL_NPOS;
#else
    static const size_t     npos;
#endif

    // 21.3.1 construct/copy/destroy
    scl_basic_string();
    explicit scl_basic_string(const Alloc_& alloc);

    scl_basic_string(const Self_& cstr);

    scl_basic_string(const Self_& cstr, size_type cpos, size_type clen = npos);
    scl_basic_string(const Self_& cstr, size_type cpos, size_type clen, const Alloc_& alloc);

    scl_basic_string(const Type_* cstr, size_type clen);
    scl_basic_string(const Type_* cstr, size_type clen, const Alloc_& alloc);

    scl_basic_string(const Type_* cstr);
    scl_basic_string(const Type_* cstr, const Alloc_& alloc);

    scl_basic_string(size_type clen, Type_ cval);
    scl_basic_string(size_type clen, Type_ cval, const Alloc_& alloc);

#ifdef  SCL_HAS_MEMBER_TEMPLATES
    template <class InputIterator_>
    scl_basic_string(InputIterator_ cfirst, InputIterator_ clast);
#else
    scl_basic_string(const_iterator cfirst, const_iterator clast);
#endif

#ifdef  SCL_HAS_MEMBER_TEMPLATES
    template <class InputIterator_>
    scl_basic_string(InputIterator_ cfirst, InputIterator_ clast, const Alloc_& alloc);
#else
    scl_basic_string(const_iterator cfirst, const_iterator clast, const Alloc_& alloc);
#endif

    ~scl_basic_string();

    Self_&          operator=(const Self_& cstr);
    Self_&          operator=(const Type_* cstr);
    Self_&          operator=(Type_ cval);

    // 21.3.2 iterators
    iterator        begin();
    const_iterator  begin() const;
    iterator        end();
    const_iterator  end() const;

    reverse_iterator        rbegin();
    const_reverse_iterator  rbegin() const;
    reverse_iterator        rend();
    const_reverse_iterator  rend() const;

    // 21.3.3 capacity
//  size_type       size() const;
    size_type       length() const;
//  size_type       max_size() const;
    void            resize(size_type clen, Type_ cval);
    void            resize(size_type clen);
//  size_type       capacity() const;
    void            reserve(size_type new_cap = 0);
    void            clear();
    bool            empty() const;

    // 21.3.4 element access
    const_reference operator[](size_type pos) const;
    reference       operator[](size_type pos);
    const_reference at(size_type pos) const;
    reference       at(size_type pos);

    // 21.3.5 modifiers
    Self_&          operator+=(const Self_& cstr);
    Self_&          operator+=(const Type_* cstr);
    Self_&          operator+=(Type_ cval);

    Self_&          append(const Self_& cstr);
    Self_&          append(const Self_& cstr, size_type cpos, size_type clen);
    Self_&          append(const Type_* cstr, size_type clen);
    Self_&          append(const Type_* cstr);
    Self_&          append(size_type clen, Type_ cval);

#ifdef  SCL_HAS_MEMBER_TEMPLATES
    template <class InputIterator_>
    Self_&          append(InputIterator_ cfirst, InputIterator_ clast);
#else
    Self_&          append(const_iterator cfirst, const_iterator clast);
#endif

    void            push_back(Type_ cval);

    Self_&          assign(const Self_& cstr);
    Self_&          assign(const Self_& cstr, size_type cpos, size_type clen);
    Self_&          assign(const Type_* cstr, size_type clen);
    Self_&          assign(const Type_* cstr);
    Self_&          assign(size_type clen, Type_ cval);

#ifdef  SCL_HAS_MEMBER_TEMPLATES
    template <class InputIterator_>
    Self_&          assign(InputIterator_ cfirst, InputIterator_ clast);
#else
    Self_&          assign(const_iterator cfirst, const_iterator clast);
#endif

    Self_&          insert(size_type pos, const Self_& cstr);
    Self_&          insert(size_type pos, const Self_& cstr, size_type cpos, size_type clen);
    Self_&          insert(size_type pos, const Type_* cstr, size_type clen);
    Self_&          insert(size_type pos, const Type_* cstr);
    Self_&          insert(size_type pos, size_type clen, Type_ cval);

    iterator        insert(iterator iter, Type_ cval);
    void            insert(iterator iter, size_type clen, Type_ cval);

#ifdef  SCL_HAS_MEMBER_TEMPLATES
    template <class InputIterator_>
    void            insert(iterator iter, InputIterator_ cfirst, InputIterator_ clast);
#else
    void            insert(iterator iter, const_iterator cfirst, const_iterator clast);
#endif

    Self_&          erase(size_type pos = 0, size_type len = npos);
    iterator        erase(iterator iter);
    iterator        erase(iterator first, iterator last);

    Self_&          replace(size_type pos, size_type len, const Self_& cstr);
    Self_&          replace(size_type pos, size_type len, const Self_& cstr, size_type cpos,
                        size_type clen);
    Self_&          replace(size_type pos, size_type len, const Type_* cstr, size_type clen);
    Self_&          replace(size_type pos, size_type len, const Type_* cstr);
    Self_&          replace(size_type pos, size_type len, size_type clen, Type_ cval);

    Self_&          replace(iterator first, iterator last, const Self_& cstr);
    Self_&          replace(iterator first, iterator last, const Type_* cstr, size_type clen);
    Self_&          replace(iterator first, iterator last, const Type_* cstr);
    Self_&          replace(iterator first, iterator last, size_type clen, Type_ cval);

#ifdef  SCL_HAS_MEMBER_TEMPLATES
    template <class InputIterator_>
    Self_&          replace(iterator first, iterator last, InputIterator_ cfirst,
                        InputIterator_ clast);
#else
    Self_&          replace(iterator first, iterator last, const_iterator cfirst,
                        const_iterator clast);
#endif

    size_type       copy(Type_* cstr, size_type clen, size_type pos = 0) const;

    void            swap(Self_& cstr);

    // 21.3.6 string operations
//  const Type_*    c_str() const;
//  const Type_*    data() const;
//  Alloc_          get_allocator() const;

    size_type       find (const Self_& cstr, size_type pos = 0) const;
    size_type       find (const Type_* cstr, size_type pos, size_type clen) const;
    size_type       find (const Type_* cstr, size_type pos = 0) const;
    size_type       find (Type_        cval, size_type pos = 0) const;

    size_type       rfind(const Self_& cstr, size_type pos = npos) const;
    size_type       rfind(const Type_* cstr, size_type pos, size_type clen) const;
    size_type       rfind(const Type_* cstr, size_type pos = npos) const;
    size_type       rfind(Type_        cval, size_type pos = npos) const;

    size_type       find_first_of(const Self_& cstr, size_type pos = 0) const;
    size_type       find_first_of(const Type_* cstr, size_type pos, size_type clen) const;
    size_type       find_first_of(const Type_* cstr, size_type pos = 0) const;
    size_type       find_first_of(Type_        cval, size_type pos = 0) const;

    size_type       find_last_of (const Self_& cstr, size_type pos = npos) const;
    size_type       find_last_of (const Type_* cstr, size_type pos, size_type clen) const;
    size_type       find_last_of (const Type_* cstr, size_type pos = npos) const;
    size_type       find_last_of (Type_        cval, size_type pos = npos) const;

    size_type       find_first_not_of(const Self_& cstr, size_type pos = 0) const;
    size_type       find_first_not_of(const Type_* cstr, size_type pos, size_type clen) const;
    size_type       find_first_not_of(const Type_* cstr, size_type pos = 0) const;
    size_type       find_first_not_of(Type_        cval, size_type pos = 0) const;

    size_type       find_last_not_of (const Self_& cstr, size_type pos = npos) const;
    size_type       find_last_not_of (const Type_* cstr, size_type pos, size_type clen) const;
    size_type       find_last_not_of (const Type_* cstr, size_type pos = npos) const;
    size_type       find_last_not_of (Type_        cval, size_type pos = npos) const;

    Self_           substr(size_type pos = 0, size_type len = npos) const;

    int             compare(const Self_& cstr) const;
    int             compare(size_type pos, size_type len, const Self_& cstr) const;
    int             compare(size_type pos, size_type len, const Self_& cstr, size_type cpos,
                        size_type clen) const;
    int             compare(const Type_* cstr) const;
    int             compare(size_type pos, size_type len, const Type_* cstr) const;
    int             compare(size_type pos, size_type len, const Type_* cstr, size_type clen) const;

#ifdef  SCL_HAS_CLASS_USING_DECL
    using Base_::size;
    using Base_::max_size;
    using Base_::capacity;
    using Base_::c_str;
    using Base_::data;
    using Base_::get_allocator;
#endif  /* SCL_HAS_CLASS_USING_DECL */
};


//
//  21.3.7.1 operator+
//
template <class Type_, class Traits_, class Alloc_>
scl_basic_string<Type_, Traits_, Alloc_>
operator+(const scl_basic_string<Type_, Traits_, Alloc_>& lhs,
          const scl_basic_string<Type_, Traits_, Alloc_>& rhs);

template <class Type_, class Traits_, class Alloc_>
scl_basic_string<Type_, Traits_, Alloc_>
operator+(const Type_* lhs,
          const scl_basic_string<Type_, Traits_, Alloc_>& rhs);

template <class Type_, class Traits_, class Alloc_>
scl_basic_string<Type_, Traits_, Alloc_>
operator+(Type_ lhs,
          const scl_basic_string<Type_, Traits_, Alloc_>& rhs);

template <class Type_, class Traits_, class Alloc_>
scl_basic_string<Type_, Traits_, Alloc_>
operator+(const scl_basic_string<Type_, Traits_, Alloc_>& lhs,
          const Type_* rhs);

template <class Type_, class Traits_, class Alloc_>
scl_basic_string<Type_, Traits_, Alloc_>
operator+(const scl_basic_string<Type_, Traits_, Alloc_>& lhs,
          Type_ rhs);


//
//  21.3.7.2 operator==
//
template <class Type_, class Traits_, class Alloc_>
bool
operator==(const scl_basic_string<Type_, Traits_, Alloc_>& lhs,
           const scl_basic_string<Type_, Traits_, Alloc_>& rhs);

template <class Type_, class Traits_, class Alloc_>
bool
operator==(const Type_* lhs,
           const scl_basic_string<Type_, Traits_, Alloc_>& rhs);

template <class Type_, class Traits_, class Alloc_>
bool
operator==(const scl_basic_string<Type_, Traits_, Alloc_>& lhs,
           const Type_* rhs);


//
//  21.3.7.3 operator!=
//
template <class Type_, class Traits_, class Alloc_>
bool
operator!=(const scl_basic_string<Type_, Traits_, Alloc_>& lhs,
           const scl_basic_string<Type_, Traits_, Alloc_>& rhs);

template <class Type_, class Traits_, class Alloc_>
bool
operator!=(const Type_* lhs,
           const scl_basic_string<Type_, Traits_, Alloc_>& rhs);

template <class Type_, class Traits_, class Alloc_>
bool
operator!=(const scl_basic_string<Type_, Traits_, Alloc_>& lhs,
           const Type_* rhs);


//
//  21.3.7.4 operator<
//
template <class Type_, class Traits_, class Alloc_>
bool
operator< (const scl_basic_string<Type_, Traits_, Alloc_>& lhs,
           const scl_basic_string<Type_, Traits_, Alloc_>& rhs);

template <class Type_, class Traits_, class Alloc_>
bool
operator< (const Type_* lhs,
           const scl_basic_string<Type_, Traits_, Alloc_>& rhs);

template <class Type_, class Traits_, class Alloc_>
bool
operator< (const scl_basic_string<Type_, Traits_, Alloc_>& lhs,
           const Type_* rhs);


//
//  21.3.7.5 operator>
//
template <class Type_, class Traits_, class Alloc_>
bool
operator> (const scl_basic_string<Type_, Traits_, Alloc_>& lhs,
           const scl_basic_string<Type_, Traits_, Alloc_>& rhs);

template <class Type_, class Traits_, class Alloc_>
bool
operator> (const Type_* lhs,
           const scl_basic_string<Type_, Traits_, Alloc_>& rhs);

template <class Type_, class Traits_, class Alloc_>
bool
operator> (const scl_basic_string<Type_, Traits_, Alloc_>& lhs,
           const Type_* rhs);


//
//  21.3.7.6 operator<=
//
template <class Type_, class Traits_, class Alloc_>
bool
operator<=(const scl_basic_string<Type_, Traits_, Alloc_>& lhs,
           const scl_basic_string<Type_, Traits_, Alloc_>& rhs);

template <class Type_, class Traits_, class Alloc_>
bool
operator<=(const Type_* lhs,
           const scl_basic_string<Type_, Traits_, Alloc_>& rhs);

template <class Type_, class Traits_, class Alloc_>
bool
operator<=(const scl_basic_string<Type_, Traits_, Alloc_>& lhs,
           const Type_* rhs);


//
//  21.3.7.7 operator>=
//
template <class Type_, class Traits_, class Alloc_>
bool
operator>=(const scl_basic_string<Type_, Traits_, Alloc_>& lhs,
           const scl_basic_string<Type_, Traits_, Alloc_>& rhs);

template <class Type_, class Traits_, class Alloc_>
bool
operator>=(const Type_* lhs,
           const scl_basic_string<Type_, Traits_, Alloc_>& rhs);

template <class Type_, class Traits_, class Alloc_>
bool
operator>=(const scl_basic_string<Type_, Traits_, Alloc_>& lhs,
           const Type_* rhs);


//
//  21.3.7.8 scl_swap
//
#ifdef  SCL_HAS_PARTIAL_SPEC_OVERLOAD
template <class Type_, class Traits_, class Alloc_>
void
scl_swap(scl_basic_string<Type_, Traits_, Alloc_>& lhs,
         scl_basic_string<Type_, Traits_, Alloc_>& rhs)
{
    lhs.swap(rhs);
}
#endif


//
//  21.3.7.9 inserters and extractors
//
#if     0
template <class Type_, class Traits_, class Alloc_>
basic_istream<Type_, Traits_>&
operator>>(basic_istream<Type_, Traits_>& is,
           scl_basic_string<Type_, Traits_, Alloc_>& str);

template <class Type_, class Traits_, class Alloc_>
basic_ostream<Type_, Traits_>&
operator<<(basic_ostream<Type_, Traits_>& os,
           const scl_basic_string<Type_, Traits_, Alloc_>& str);

template <class Type_, class Traits_, class Alloc_>
basic_istream<Type_, Traits_>&
getline(basic_istream<Type_, Traits_>& is,
        scl_basic_string<Type_, Traits_, Alloc_>& str,
        Type_ delim);

template <class Type_, class Traits_, class Alloc_>
basic_istream<Type_, Traits_>&
getline(basic_istream<Type_, Traits_>& is,
        scl_basic_string<Type_, Traits_, Alloc_>& str);
#endif


//
//  Public types scl_string and scl_wstring.
//
typedef scl_basic_string<char, scl_char_traits<char>, scl_allocator<char> >
    scl_string;

typedef scl_basic_string<wchar_t, scl_char_traits<wchar_t>, scl_allocator<wchar_t> >
    scl_wstring;


//
//  Define member functions.
//
#if     defined(SCL_COMPILE_INSTANTIATE) || defined(_SCLP_STRING_CC_)
#include <sclp_string.cc>

#elif   defined(_WINDOWS_SOURCE)
#pragma warning(disable:4231)           // 'extern' before explicit instantiation
SCL_EXPORT_TEMPLATE class SCL_STD_EXPORT
_scl_string_base<char, scl_char_traits<char>, scl_allocator<char> >;

SCL_EXPORT_TEMPLATE class SCL_STD_EXPORT
scl_basic_string<char, scl_char_traits<char>, scl_allocator<char> >;

SCL_EXPORT_TEMPLATE class SCL_STD_EXPORT
_scl_string_base<wchar_t, scl_char_traits<wchar_t>, scl_allocator<wchar_t> >;

SCL_EXPORT_TEMPLATE class SCL_STD_EXPORT
scl_basic_string<wchar_t, scl_char_traits<wchar_t>, scl_allocator<wchar_t> >;
#pragma warning(default:4231)

#endif  /* SCL_COMPILE_INSTANTIATE */


//
//  Define non-member functions.
//
#include <sclp_string_ops.cc>

SCL_STD_EXPORT ostream&
operator<<(ostream& os, const scl_string& str);

SCL_STD_EXPORT ostream&
operator<<(ostream& os, const scl_wstring& str);


#endif  /* _SCLP_STRING_H_ */
