/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
//
//  FILE: sclp_rbtree.cc
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Apr-2005     Initial implementation.
//
//==============================================================================
#define SCL_RBTREE_TEMPL        \
    template <class Key_, class Value_, class KeyVal_, class Comp_, class Alloc_>
#define SCL_RBTREE_CLASS        scl_rbtree<Key_, Value_, KeyVal_, Comp_, Alloc_>
#define SCL_RBTREE_CTOR_BEGIN   try {
#define SCL_RBTREE_CTOR_END     } catch (...) { finalize(); throw; }


//
//  construct/copy/destroy
//
SCL_RBTREE_TEMPL
SCL_RBTREE_CLASS::scl_rbtree(bool multi) :
    multi_     (multi),
    key_comp_  (),
    key_value_ (),
    alloc_     (),
    node_alloc_()
{
    SCL_VALIDATE_CTOR(Self_, this);
    initialize();
}

SCL_RBTREE_TEMPL
SCL_RBTREE_CLASS::scl_rbtree(bool multi, const Comp_& comp) :
    multi_     (multi),
    key_comp_  (comp),
    key_value_ (),
    alloc_     (),
    node_alloc_()
{
    SCL_VALIDATE_CTOR(Self_, this);
    initialize();
}

SCL_RBTREE_TEMPL
SCL_RBTREE_CLASS::scl_rbtree(bool multi, const Comp_& comp, const Alloc_& alloc) :
    multi_     (multi),
    key_comp_  (comp),
    key_value_ (),
    alloc_     (alloc),
    node_alloc_(alloc)
{
    SCL_VALIDATE_CTOR(Self_, this);
    initialize();
}

SCL_RBTREE_TEMPL
SCL_RBTREE_CLASS::scl_rbtree(const Self_& other) :
    multi_     (other.multi_),
    key_comp_  (other.key_comp_),
    key_value_ (other.key_value_),
    alloc_     (other.alloc_),
    node_alloc_(other.node_alloc_)
{
    SCL_VALIDATE_CTOR(Self_, this);
    initialize();

    SCL_RBTREE_CTOR_BEGIN
        copy_aux(other);
    SCL_RBTREE_CTOR_END
}

SCL_RBTREE_TEMPL
SCL_RBTREE_CLASS::~scl_rbtree()
{
    SCL_VALIDATE_DTOR(Self_, this);
    finalize();
}

SCL_RBTREE_TEMPL
SCL_RBTREE_CLASS &
SCL_RBTREE_CLASS::operator=(const Self_& other)
{
    SCL_VALIDATE(Self_, this);

    if (this != &other)
    {
        clear_aux();
        copy_aux(other);
    }

    return *this;
}

SCL_RBTREE_TEMPL
typename SCL_RBTREE_CLASS::allocator_type
SCL_RBTREE_CLASS::get_allocator() const
{
    return allocator_type(node_alloc_);
}


//
//  iterators
//
SCL_RBTREE_TEMPL
typename SCL_RBTREE_CLASS::iterator
SCL_RBTREE_CLASS::begin()
{
    return iterator(leftmost());
}

SCL_RBTREE_TEMPL
typename SCL_RBTREE_CLASS::const_iterator
SCL_RBTREE_CLASS::begin() const
{
    return const_iterator(leftmost());
}

SCL_RBTREE_TEMPL
typename SCL_RBTREE_CLASS::iterator
SCL_RBTREE_CLASS::end()
{
    return iterator(head_);
}

SCL_RBTREE_TEMPL
typename SCL_RBTREE_CLASS::const_iterator
SCL_RBTREE_CLASS::end() const
{
    return const_iterator(head_);
}

SCL_RBTREE_TEMPL
typename SCL_RBTREE_CLASS::reverse_iterator
SCL_RBTREE_CLASS::rbegin()
{
    return reverse_iterator(end());
}

SCL_RBTREE_TEMPL
typename SCL_RBTREE_CLASS::const_reverse_iterator
SCL_RBTREE_CLASS::rbegin() const
{
    return const_reverse_iterator(end());
}

SCL_RBTREE_TEMPL
typename SCL_RBTREE_CLASS::reverse_iterator
SCL_RBTREE_CLASS::rend()
{
    return reverse_iterator(begin());
}

SCL_RBTREE_TEMPL
typename SCL_RBTREE_CLASS::const_reverse_iterator
SCL_RBTREE_CLASS::rend() const
{
    return const_reverse_iterator(begin());
}


//
//  capacity
//
SCL_RBTREE_TEMPL
bool
SCL_RBTREE_CLASS::empty() const
{
    return (size_ == 0);
}

SCL_RBTREE_TEMPL
typename SCL_RBTREE_CLASS::size_type
SCL_RBTREE_CLASS::size() const
{
    return size_;
}

SCL_RBTREE_TEMPL
typename SCL_RBTREE_CLASS::size_type
SCL_RBTREE_CLASS::max_size() const
{
    return node_alloc_.max_size();
}


//
//  modifiers
//
SCL_RBTREE_TEMPL
scl_pair<typename SCL_RBTREE_CLASS::iterator, bool>
SCL_RBTREE_CLASS::insert(const Value_& value)
{
    SCL_VALIDATE(Self_, this);
    typedef scl_pair<iterator, bool>    Result_;

    BNode_* x_node = root();
    BNode_* y_node = head_;
    bool    comp   = true;

    while (x_node != nil_)
    {
        y_node = x_node;
        comp   = vcompare(value, x_node);
        x_node = comp ? x_node->left_ : x_node->right_;
    }

    if (multi_)
    {
        iterator    pos = insert_aux(x_node, y_node, value);
        return Result_(pos, true);
    }

    iterator    iter(y_node);

    if (comp)
    {
        if (iter == begin())
        {
            iterator    pos = insert_aux(x_node, y_node, value);
            return Result_(pos, true);
        }

        --iter;
    }

    if (vcompare(iter.node_, value))
    {
        iterator    pos = insert_aux(x_node, y_node, value);
        return Result_(pos, true);
    }

    return Result_(iter, false);
}

SCL_RBTREE_TEMPL
typename SCL_RBTREE_CLASS::iterator
SCL_RBTREE_CLASS::insert(iterator pos, const Value_& value)
{
    SCL_VALIDATE(Self_, this);

    if (pos.node_ == leftmost())
    {
        // pos == begin()
        if (size_ > 0 && vcompare(value, pos.node_))
            return insert_aux(pos.node_, pos.node_, value);
        else
            return insert(value).first;
    }

    if (pos.node_ == head_)
    {
        // pos == end()
        if (vcompare(rightmost(), value))
            return insert_aux(nil_, rightmost(), value);
        else
            return insert(value).first;
    }

    iterator    before = pos;
    --before;

    if (vcompare(before.node_, value) && vcompare(value, pos.node_))
    {
        if (before.node_->right_ == nil_)
            return insert_aux(nil_, before.node_, value);
        else
            return insert_aux(pos.node_, pos.node_, value);
    }

    return insert(value).first;
}

#ifdef  SCL_HAS_MEMBER_TEMPLATES
SCL_RBTREE_TEMPL
template <class InputIterator_>
void
SCL_RBTREE_CLASS::insert(InputIterator_ first, InputIterator_ last)
{
    for (; first != last; ++first)
        insert(*first);
}
#else
SCL_RBTREE_TEMPL
void
SCL_RBTREE_CLASS::insert(const_iterator first, const_iterator last)
{
    for (; first != last; ++first)
        insert(*first);
}

SCL_RBTREE_TEMPL
void
SCL_RBTREE_CLASS::insert(const_pointer  first, const_pointer  last)
{
    for (; first != last; ++first)
        insert(*first);
}
#endif

SCL_RBTREE_TEMPL
void
SCL_RBTREE_CLASS::erase(iterator pos)
{
    SCL_VALIDATE(Self_, this);
    BNode_* y_node = BNode_::Erase(pos.node_, head_, nil_);
    destroy_node(static_cast<VNode_*>(y_node));
    --size_;
}

SCL_RBTREE_TEMPL
typename SCL_RBTREE_CLASS::size_type
SCL_RBTREE_CLASS::erase(const Key_& key)
{
    SCL_VALIDATE(Self_, this);
    scl_pair<iterator, iterator>    range = equal_range(key);
    size_type   result = scl_distance(range.first, range.second);
    erase(range.first, range.second);
    return result;
}

SCL_RBTREE_TEMPL
void
SCL_RBTREE_CLASS::erase(iterator first, iterator last)
{
    SCL_VALIDATE(Self_, this);

    if (first == begin() && last == end())
    {
        clear_aux();
    }
    else
    {
        while (first != last)
            erase(first++);
    }
}

SCL_RBTREE_TEMPL
void
SCL_RBTREE_CLASS::swap(Self_& other)
{
    SCL_VALIDATE(Self_, this);

    if (node_alloc_ == other.node_alloc_)
    {
        scl_swap(multi_     , other.multi_);
        scl_swap(size_      , other.size_);
        scl_swap(head_      , other.head_);
        scl_swap(nil_       , other.nil_);
        scl_swap(key_comp_  , other.key_comp_);
        scl_swap(key_value_ , other.key_value_);
        scl_swap(alloc_     , other.alloc_);
        scl_swap(node_alloc_, other.node_alloc_);
    }
    else
    {
        const Self_ tmp(*this);
        *this = other;
        other = tmp;
    }
}

SCL_RBTREE_TEMPL
void
SCL_RBTREE_CLASS::clear()
{
    clear_aux();
}


//
//  search
//
SCL_RBTREE_TEMPL
typename SCL_RBTREE_CLASS::iterator
SCL_RBTREE_CLASS::find(const Key_& key)
{
    SCL_VALIDATE(Self_, this);
    iterator    iter = lower_bound(key);

    if (iter == end() || kcompare(key, iter.node_))
        return end();
    else
        return iter;
}

SCL_RBTREE_TEMPL
typename SCL_RBTREE_CLASS::const_iterator
SCL_RBTREE_CLASS::find(const Key_& key) const
{
    return const_cast<Self_*>(this)->find(key);
}

SCL_RBTREE_TEMPL
typename SCL_RBTREE_CLASS::size_type
SCL_RBTREE_CLASS::count(const Key_& key) const
{
    SCL_VALIDATE(Self_, this);
    scl_pair<const_iterator, const_iterator>    range = equal_range(key);
    size_type   result = scl_distance(range.first, range.second);
    return result;
}

SCL_RBTREE_TEMPL
typename SCL_RBTREE_CLASS::iterator
SCL_RBTREE_CLASS::lower_bound(const Key_& key)
{
    SCL_VALIDATE(Self_, this);
    BNode_* x_node = root();
    BNode_* y_node = head_;

    while (x_node != nil_)
    {
        if (kcompare(x_node, key))
        {
            x_node = x_node->right_;
        }
        else
        {
            y_node = x_node;
            x_node = x_node->left_;
        }
    }

    return iterator(y_node);
}

SCL_RBTREE_TEMPL
typename SCL_RBTREE_CLASS::const_iterator
SCL_RBTREE_CLASS::lower_bound(const Key_& key) const
{
    return const_cast<Self_*>(this)->lower_bound(key);
}

SCL_RBTREE_TEMPL
typename SCL_RBTREE_CLASS::iterator
SCL_RBTREE_CLASS::upper_bound(const Key_& key)
{
    SCL_VALIDATE(Self_, this);
    BNode_* x_node = root();
    BNode_* y_node = head_;

    while (x_node != nil_)
    {
        if (kcompare(key, x_node))
        {
            y_node = x_node;
            x_node = x_node->left_;
        }
        else
        {
            x_node = x_node->right_;
        }
    }

    return iterator(y_node);
}

SCL_RBTREE_TEMPL
typename SCL_RBTREE_CLASS::const_iterator
SCL_RBTREE_CLASS::upper_bound(const Key_& key) const
{
    return const_cast<Self_*>(this)->upper_bound(key);
}

SCL_RBTREE_TEMPL
scl_pair<typename SCL_RBTREE_CLASS::iterator, typename SCL_RBTREE_CLASS::iterator>
SCL_RBTREE_CLASS::equal_range(const Key_& key)
{
    typedef scl_pair<iterator, iterator>                Result_;
    return Result_(lower_bound(key), upper_bound(key));
}

SCL_RBTREE_TEMPL
scl_pair<typename SCL_RBTREE_CLASS::const_iterator, typename SCL_RBTREE_CLASS::const_iterator>
SCL_RBTREE_CLASS::equal_range(const Key_& key) const
{
    typedef scl_pair<const_iterator, const_iterator>    Result_;
    return Result_(lower_bound(key), upper_bound(key));
}

SCL_RBTREE_TEMPL
bool
SCL_RBTREE_CLASS::_validate() const
{
    if (size_ == 0 || begin() == end())
    {
        bool    okay = (size_ == 0)     &&
            (begin() == end())          &&
            (head_->parent_ == nil_ )   &&
            (head_->left_   == head_)   &&
            (head_->right_  == head_)   &&
            (head_->color_  == BNode_::Red);

        if (!okay) return false;
    }

    size_t  depth = BNode_::CountBlack(leftmost(), root(), nil_);
    size_t  msize = 0;

    for (const_iterator iter = begin(); iter != end(); ++iter, ++msize)
    {
        const BNode_*   x_node  = iter.node_;
        const BNode_*   x_left  = x_node->left_;
        const BNode_*   x_right = x_node->right_;

        if (x_node->color_ == BNode_::Red)
        {
            if (x_left->color_ == BNode_::Red || x_right->color_ == BNode_::Red)
                return false;
        }

        if (x_left  != nil_ && vcompare(x_node, x_left))
            return false;

        if (x_right != nil_ && vcompare(x_right, x_node))
            return false;

        if (x_left == nil_ && x_right == nil_)
        {
            if (BNode_::CountBlack(x_node, root(), nil_) != depth)
                return false;
        }
    }

    if (msize != size_)
        return false;

    if (leftmost()  != BNode_::Minimum(root(), head_))
        return false;

    if (rightmost() != BNode_::Maximum(root(), head_))
        return false;

    return true;
}


//
//  protected methods
//
SCL_RBTREE_TEMPL
void
SCL_RBTREE_CLASS::initialize()
{
    // Establish class invariants.
    size_ = 0;
    head_ = NULL;
    nil_  = NULL;

    // Allocate storage for head node.
    head_ = allocate_node();
    head_->parent_ = NULL;
    head_->left_   = head_;
    head_->right_  = head_;
    head_->color_  = BNode_::Red;

    // Allocate storage for sentinel node.
    nil_  = allocate_node();
    nil_->is_nil_  = true;
    head_->parent_ = nil_;
}

SCL_RBTREE_TEMPL
void
SCL_RBTREE_CLASS::finalize()
{
    clear_aux();

    deallocate_node(head_);

    BNode_::DestroyNil(nil_);
    deallocate_node(nil_);
}

SCL_RBTREE_TEMPL
typename SCL_RBTREE_CLASS::VNode_*
SCL_RBTREE_CLASS::allocate_node()
{
    VNode_* node;

#ifdef  SCL_USE_ALLOCATOR_REBIND
    node = node_alloc_.allocate(1);
#else
    node = (VNode_*) node_alloc_.raw_allocate(1, sizeof(VNode_));
#endif

    BNode_::Initialize(node);

    return node;
}

SCL_RBTREE_TEMPL
void
SCL_RBTREE_CLASS::deallocate_node(VNode_* node)
{
#ifdef  SCL_USE_ALLOCATOR_REBIND
    node_alloc_.deallocate(node, 1);
#else
    node_alloc_.raw_deallocate(node, 1, sizeof(VNode_));
#endif
}

SCL_RBTREE_TEMPL
typename SCL_RBTREE_CLASS::VNode_*
SCL_RBTREE_CLASS::create_node(const Value_& value)
{
    VNode_* node = allocate_node();

    try
    {
        alloc_.construct(&node->data_, value);
    }
    catch (...)
    {
        deallocate_node(node);
        throw;
    }

    return node;
}

SCL_RBTREE_TEMPL
void
SCL_RBTREE_CLASS::destroy_node(VNode_* node)
{
    alloc_.destroy(&node->data_);
    deallocate_node(node);
}

SCL_RBTREE_TEMPL
typename SCL_RBTREE_CLASS::BNode_*
SCL_RBTREE_CLASS::copy_tree(const BNode_* src_node, BNode_* dst_parent, BNode_* src_nil)
{
    if (src_node == src_nil)
        return nil_;

    VNode_* dst_node  = create_node(static_cast<const VNode_*>(src_node)->data_);

    dst_node->color_  = src_node->color_;
    dst_node->left_   = copy_tree(src_node->left_ , dst_node, src_nil);
    dst_node->right_  = copy_tree(src_node->right_, dst_node, src_nil);
    dst_node->parent_ = dst_parent;

    return dst_node;
}

SCL_RBTREE_TEMPL
void
SCL_RBTREE_CLASS::copy_aux(const Self_& other)
{
    root()      = copy_tree(other.root(), head_, other.nil_);
    leftmost()  = BNode_::Minimum(root(), head_);
    rightmost() = BNode_::Maximum(root(), head_);
    size_       = other.size_;
}

SCL_RBTREE_TEMPL
void
SCL_RBTREE_CLASS::erase_tree(BNode_* node)
{
    // Erase subtree without rebalancing.
    while (node != nil_)
    {
        erase_tree(node->right_);
        BNode_* left = node->left_;
        destroy_node(static_cast<VNode_*>(node));
        node = left;
    }
}

SCL_RBTREE_TEMPL
void
SCL_RBTREE_CLASS::clear_aux()
{
    erase_tree(root());

    size_          = 0;
    head_->parent_ = nil_;
    head_->left_   = head_;
    head_->right_  = head_;
}

SCL_RBTREE_TEMPL
typename SCL_RBTREE_CLASS::iterator
SCL_RBTREE_CLASS::insert_aux(BNode_* x_node, BNode_* y_node, const Value_& value)
{
    VNode_* z_node  = create_node(value);
    bool    to_left = (y_node == head_) || (x_node != nil_) || vcompare(value, y_node);

    BNode_::Insert(to_left, x_node, y_node, z_node, head_, nil_);
    size_++;

    return iterator(z_node);
}

SCL_RBTREE_TEMPL
bool
SCL_RBTREE_CLASS::kcompare(const Key_& lkey, const BNode_* rval) const
{
    const Key_& rkey = key_value_(static_cast<const VNode_*>(rval)->data_);
    return key_comp_(lkey, rkey);
}

SCL_RBTREE_TEMPL
bool
SCL_RBTREE_CLASS::kcompare(const BNode_* lval, const Key_& rkey) const
{
    const Key_& lkey = key_value_(static_cast<const VNode_*>(lval)->data_);
    return key_comp_(lkey, rkey);
}

SCL_RBTREE_TEMPL
bool
SCL_RBTREE_CLASS::vcompare(const Value_& lval, const BNode_* rval) const
{
    const Key_& lkey = key_value_(lval);
    const Key_& rkey = key_value_(static_cast<const VNode_*>(rval)->data_);
    return key_comp_(lkey, rkey);
}

SCL_RBTREE_TEMPL
bool
SCL_RBTREE_CLASS::vcompare(const BNode_* lval, const Value_& rval) const
{
    const Key_& lkey = key_value_(static_cast<const VNode_*>(lval)->data_);
    const Key_& rkey = key_value_(rval);
    return key_comp_(lkey, rkey);
}

SCL_RBTREE_TEMPL
bool
SCL_RBTREE_CLASS::vcompare(const BNode_* lval, const BNode_* rval) const
{
    const Key_& lkey = key_value_(static_cast<const VNode_*>(lval)->data_);
    const Key_& rkey = key_value_(static_cast<const VNode_*>(rval)->data_);
    return key_comp_(lkey, rkey);
}


#undef  SCL_RBTREE_TEMPL
#undef  SCL_RBTREE_CLASS
#undef  SCL_RBTREE_CTOR_BEGIN
#undef  SCL_RBTREE_CTOR_END
