/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: sclp_queue.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Aug-2004     Initial implementation.
//
//  NOTICE:
//      This header is used internally by the Standard C++ Library.
//      It should not be included directly in application code.
//
//==============================================================================
#ifndef _SCLP_QUEUE_H_
#define _SCLP_QUEUE_H_


//
//  Include section
//
#ifndef  _SCLP_DEQUE_H_
#include <sclp_deque.h>
#endif

#ifndef  _SCLP_VECTOR_H_
#include <sclp_vector.h>
#endif

#ifndef  _SCLP_HEAP_ALGO_H_
#include <sclp_heap_algo.h>             // for scl_make_heap, scl_push_heap, scl_pop_heap
#endif

#ifndef  _SCLP_FUNCTIONAL_H_
#include <sclp_functional.h>            // for scl_less
#endif


//
//  23.2.3.1 Class template scl_queue
//
#ifdef  SCL_HAS_DEFAULT_TMPL_ARGS
template <class Type_, class Cntr_ = scl_deque<Type_> >
#else
template <class Type_, class Cntr_>
#endif
class scl_queue
{
public:
    typedef scl_queue<Type_, Cntr_>                 Self_;
    typedef typename Cntr_::value_type              value_type;
    typedef typename Cntr_::size_type               size_type;
    typedef          Cntr_                          container_type;

    scl_queue() : c()                               { }

    explicit
    scl_queue(const Cntr_& cntr) : c(cntr)          { }

    bool                empty() const               { return c.empty(); }
    size_type           size() const                { return c.size();  }
    value_type&         front()                     { return c.front(); }
    const value_type&   front() const               { return c.front(); }
    value_type&         back()                      { return c.back();  }
    const value_type&   back() const                { return c.back();  }
    void                push(const value_type& val) { c.push_back(val); }
    void                pop()                       { c.pop_front();    }

    // Internal helper methods.
    bool    _equal(const Self_& other) const        { return (c == other.c); }
    bool    _less (const Self_& other) const        { return (c <  other.c); }

    // The following data-member name is explicitly defined by the Standard.
protected:
    Cntr_   c;
};


template <class Type_, class Cntr_>
bool
operator==(const scl_queue<Type_, Cntr_>& lhs,
           const scl_queue<Type_, Cntr_>& rhs)
{
    return lhs._equal(rhs);
}

template <class Type_, class Cntr_>
bool
operator< (const scl_queue<Type_, Cntr_>& lhs,
           const scl_queue<Type_, Cntr_>& rhs)
{
    return lhs._less(rhs);
}

template <class Type_, class Cntr_>
bool
operator!=(const scl_queue<Type_, Cntr_>& lhs,
           const scl_queue<Type_, Cntr_>& rhs)
{
    return !(lhs == rhs);
}

template <class Type_, class Cntr_>
bool
operator> (const scl_queue<Type_, Cntr_>& lhs,
           const scl_queue<Type_, Cntr_>& rhs)
{
    return (rhs < lhs);
}

template <class Type_, class Cntr_>
bool
operator<=(const scl_queue<Type_, Cntr_>& lhs,
           const scl_queue<Type_, Cntr_>& rhs)
{
    return !(rhs < lhs);
}

template <class Type_, class Cntr_>
bool
operator>=(const scl_queue<Type_, Cntr_>& lhs,
           const scl_queue<Type_, Cntr_>& rhs)
{
    return !(lhs < rhs);
}


//
//  23.2.3.2 Class template scl_priority_queue
//
#ifdef  SCL_HAS_DEFAULT_TMPL_ARGS
template <class Type_,
          class Cntr_ = scl_vector<Type_>,
          class Comp_ = scl_less<typename Cntr_::value_type> >
#else
template <class Type_, class Cntr_, class Comp_>
#endif
class scl_priority_queue
{
public:
    typedef typename Cntr_::value_type              value_type;
    typedef typename Cntr_::size_type               size_type;
    typedef          Cntr_                          container_type;

#ifndef SCL_HAS_MEMBER_TEMPLATES
    typedef typename Cntr_::const_iterator          InpIter_;
#endif

    scl_priority_queue();

    explicit
    scl_priority_queue(const Comp_& cmpr);

    scl_priority_queue(const Comp_& cmpr, const Cntr_& cntr);

#ifdef  SCL_HAS_MEMBER_TEMPLATES
    template <class InpIter_>
#endif
    scl_priority_queue(InpIter_ first, InpIter_ last);

#ifdef  SCL_HAS_MEMBER_TEMPLATES
    template <class InpIter_>
#endif
    scl_priority_queue(InpIter_ first, InpIter_ last, const Comp_& cmpr);

#ifdef  SCL_HAS_MEMBER_TEMPLATES
    template <class InpIter_>
#endif
    scl_priority_queue(InpIter_ first, InpIter_ last, const Comp_& cmpr, const Cntr_& cntr);

    bool                empty() const               { return c.empty(); }
    size_type           size() const                { return c.size();  }
    const value_type&   top() const                 { return c.front(); }
    void                push(const value_type& val);
    void                pop();

    // The following data-member names are explicitly defined by the Standard.
protected:
    Cntr_   c;
    Comp_   comp;
};


#include <sclp_queue.cc>


#endif  /* _SCLP_QUEUE_H_ */
