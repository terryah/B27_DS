/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: sclp_list_base.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Aug-2004     Initial implementation.
//
//  NOTICE:
//      This header is used internally by the Standard C++ Library.
//      It should not be included directly in application code.
//
//==============================================================================
#ifndef _SCLP_LIST_BASE_H_
#define _SCLP_LIST_BASE_H_


//
//  Include section
//
#ifndef  _SCLP_BASE_ITER_H_
#include <sclp_base_iter.h>             // for scl_iterator and friends
#endif


//
//  Internal helper classes
//
template <class Type_>
class _scl_list_node
{
public:
    typedef _scl_list_node<Type_>           Self_;

    // Insert <self> before <node>.
    void insert(Self_* const node);

    // Remove <self> from linked list.
    void unlink();

    // Reverse list starting with <self>.
    void reverse();

    void transfer(Self_* const first, Self_* const last);

    // Note: Data members are exposed to eliminate messy friend declarations.
public:
    Self_*  next_;              // successor node, or first element if head
    Self_*  prev_;              // predecessor node, or last element if head
    Type_   data_;              // stored object, or unused if head

private:
    // Disable copy and assignment operations.
    _scl_list_node(const Self_&) {};
    void operator=(const Self_&) {};
};


template <class Type_>
class _scl_list_iterator : public
    scl_iterator<scl_bidirectional_iterator_tag, Type_, ptrdiff_t, Type_*, Type_&>
{
public:
    // Private types
    typedef
    scl_iterator<scl_bidirectional_iterator_tag, Type_, ptrdiff_t, Type_*, Type_&>
                                                Base_;
    typedef _scl_list_iterator<Type_>           Self_;
    typedef _scl_list_node<Type_>               Node_;

    // Public types
    SCL_ITERATOR_TYPEDEFS(Base_);

    _scl_list_iterator() :
        node_(NULL)
    {
        // Nothing
    }

    _scl_list_iterator(Node_* node) :
        node_(node)
    {
        // Nothing
    }

    reference operator*() const
    {
        return  node_->data_;
    }

    pointer operator->() const
    {
        return &node_->data_;
    }

    Self_& operator++()
    {
        node_ = node_->next_;
        return *this;
    }

    Self_  operator++(int)
    {
        Self_   tmp(*this);
        node_ = node_->next_;
        return tmp;
    }

    Self_& operator--()
    {
        node_ = node_->prev_;
        return *this;
    }

    Self_  operator--(int)
    {
        Self_   tmp(*this);
        node_ = node_->prev_;
        return tmp;
    }

    bool operator==(const Self_& other) const
    {
        return (node_ == other.node_);
    }

    bool operator!=(const Self_& other) const
    {
        return (node_ != other.node_);
    }

    // Note: Data member is exposed to eliminate messy friend declarations.
public:
    Node_*  node_;              // Pointer to list element
};


template <class Type_>
class _scl_list_const_iterator : public
    scl_iterator<scl_bidirectional_iterator_tag, Type_, ptrdiff_t, const Type_*, const Type_&>
{
public:
    // Private types
    typedef
    scl_iterator<scl_bidirectional_iterator_tag, Type_, ptrdiff_t, const Type_*, const Type_&>
                                                Base_;
    typedef _scl_list_const_iterator<Type_>     Self_;
    typedef const _scl_list_node<Type_>         Node_;

    // Public types
    typedef _scl_list_iterator<Type_>           iterator;
    SCL_ITERATOR_TYPEDEFS(Base_);

    _scl_list_const_iterator() :
        node_(NULL)
    {
        // Nothing
    }

    _scl_list_const_iterator(Node_* node) :
        node_(node)
    {
        // Nothing
    }

    _scl_list_const_iterator(const iterator& other) :
        node_(other.node_)
    {
        // Nothing
    }

    reference operator*() const
    {
        return  node_->data_;
    }

    pointer operator->() const
    {
        return &node_->data_;
    }

    Self_& operator++()
    {
        node_ = node_->next_;
        return *this;
    }

    Self_  operator++(int)
    {
        Self_   tmp(*this);
        node_ = node_->next_;
        return tmp;
    }

    Self_& operator--()
    {
        node_ = node_->prev_;
        return *this;
    }

    Self_  operator--(int)
    {
        Self_   tmp(*this);
        node_ = node_->prev_;
        return tmp;
    }

    bool operator==(const Self_& other) const
    {
        return (node_ == other.node_);
    }

    bool operator!=(const Self_& other) const
    {
        return (node_ != other.node_);
    }

    // Note: Data member is exposed to eliminate messy friend declarations.
public:
    Node_*  node_;              // Pointer to list element
};


template <class Type_>
bool
operator==(const _scl_list_iterator      <Type_>& lhs,
           const _scl_list_const_iterator<Type_>& rhs)
{
    return (lhs.node_ == rhs.node_);
}

template <class Type_>
bool
operator!=(const _scl_list_iterator      <Type_>& lhs,
           const _scl_list_const_iterator<Type_>& rhs)
{
    return (lhs.node_ != rhs.node_);
}


#include <sclp_list_base.cc>


#endif  /* _SCLP_LIST_BASE_H_ */
