/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: sclp_temp_buffer.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Dec-2004     Initial implementation.
//
//  NOTICE:
//      This header is used internally by the Standard C++ Library.
//      It should not be included directly in application code.
//
//==============================================================================
#ifndef _SCLP_TEMP_BUFFER_H_
#define _SCLP_TEMP_BUFFER_H_


//
//  Include section
//
#ifndef  _SCLP_UTILITY_H_
#include <sclp_utility.h>               // for scl_pair
#endif

#ifndef  _SCL_CLIMITS_H_
#include <scl_climits.h>                // for SCL_PTRDIFF_MAX
#endif


//
//  20.4.3 Temporary buffers
//
//  Note: The first variant of scl_get_temporary_buffer() requires the use of
//  explicit function template arguments, while the second variant does not.
//
#ifdef  SCL_HAS_FUNC_TMPL_ARGS
template <class Type_>
scl_pair<Type_*, ptrdiff_t>
scl_get_temporary_buffer(ptrdiff_t len);
#endif

template <class Type_>
scl_pair<Type_*, ptrdiff_t>
scl_get_temporary_buffer(ptrdiff_t len, Type_*);

template <class Type_>
void
scl_return_temporary_buffer(Type_* ptr);


#include <sclp_temp_buffer.cc>


#endif  /* _SCLP_TEMP_BUFFER_H_ */
