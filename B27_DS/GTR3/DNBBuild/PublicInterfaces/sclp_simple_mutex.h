/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2008
//==============================================================================
//
//  FILE: sclp_simple_mutex.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     29-Jul-2008     Initial implementation.
//
//  NOTICE:
//      This header is used internally by the Standard C++ Library.
//      It should not be included directly in application code.
//
//==============================================================================
#ifndef _SCLP_SIMPLE_MUTEX_H_
#define _SCLP_SIMPLE_MUTEX_H_


//
//  Include section
//
#ifdef  _WINDOWS_SOURCE
#ifdef  SCL_DEBUG
#include <windows.h>            // for Win32 CRITICAL_SECTION
#endif
#else
#include <pthread.h>            // for POSIX pthread_mutex_t
#endif




class SCL_STD_EXPORT scl_simple_mutex
{
public:
    scl_simple_mutex();

    scl_simple_mutex(SCL_STATIC_TYPE);

    ~scl_simple_mutex();

    int     create(bool recursive, bool adaptive);

    int     destroy();

    int     try_lock(bool& result);

    int     lock();

    int     unlock();

    bool    is_active() const;

private:
    // Disable copy construction and assignment.
    scl_simple_mutex(const scl_simple_mutex&);
    scl_simple_mutex& operator=(const scl_simple_mutex&);

    // Specify the representation of the native mutex object.
#ifdef  _WINDOWS_SOURCE

#ifdef  SCL_DEBUG
    typedef CRITICAL_SECTION    MutexRep;
#else
    // The following opaque structure must have the same size as CRITICAL_SECTION,
    // which is defined in <WinNT.h>.  By defining the opaque structure, we can
    // prevent the inclusion of <windows.h>, which generates over 250k lines of
    // compiled declarations.
    struct  MutexRep
    {
#ifdef  PLATEFORME_DS64
        __int64     dummy_[5];          // sizeof(CRITICAL_SECTION) = 40
#else
        __int32     dummy_[6];          // sizeof(CRITICAL_SECTION) = 24
#endif
    };
#endif  /* SCL_DEBUG */

#else
    typedef pthread_mutex_t     MutexRep;
#endif  /* _WINDOWS_SOURCE */

    // Initialization flag values.  Here we assume that static/global mutex objects
    // are initialized with all-bits zero.
    enum MagicValues
    {
        NotInitialized = 0,             // Magic uninitialized value
        Initialized    = 0x2487face     // Magic initialized value
    };

    // Data members
    volatile int    magic_;             // Initialization status
    MutexRep        mutex_;             // Native mutex object

    friend void     test_scl_mutex();   // For testing purposes
};


#endif  /* _SCLP_SIMPLE_MUTEX_H_ */
