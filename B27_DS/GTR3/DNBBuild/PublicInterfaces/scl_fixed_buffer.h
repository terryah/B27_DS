/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: scl_fixed_buffer.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Oct-2004     Initial implementation.
//
//  NOTICE:
//      This header extends the capabilities of the Standard C++ Library.
//      It may be included directly in application code.
//
//==============================================================================
#ifndef _SCL_FIXED_BUFFER_H_
#define _SCL_FIXED_BUFFER_H_


//
//  Include section
//
#ifndef  _SCL_CONFIG_H_
#include <scl_config.h>
#endif

#ifndef  _SCL_MEMORY_H_
#include <scl_memory.h>                 // for scl_uninitialized_fill_n
#endif

#ifndef  _SCLP_BASE_ITER_H_
#include <sclp_base_iter.h>             // for scl_reverse_iterator
#endif

#ifndef  _SCLP_POINTER_ITER_H_
#include <sclp_pointer_iter.h>          // for scl_pointer_iterator
#endif


/**
  * A fixed-size buffer that provides efficient allocation on the stack.
  *
  * <br><B>Template Parameters</B><br>
  * @param Type_
  *     The class of objects stored in the buffer.  This class must provide a
  *     default constructor, copy constructor, and destructor.
  * @param Size_
  *     The nominal capacity of the buffer.  This represents the number of
  *     elements for which memory is pre-allocated.
  *
  * <br><B>Description</B><br>
  *     This class represents a fixed-size sequence of type <tt>Type_</tt>.  It
  * has been designed to minimize the overhead incurred in creating transient
  * containers on the stack.  The template parameter <tt>Size_</tt> specifies the
  * nominal size of the buffer (which is a compile-time constant), while the
  * constructor parameter <tt>size</tt> specifies the actual size of the buffer
  * (which is a runtime constant).  If <tt>size</tt> is less than or equal to
  * <tt>Size_</tt>, an embedded array is used for data storage.  Otherwise, a
  * dynamically allocated array is used for data storage.  In either case, the
  * buffer cannot be resized after construction.  For convenience, the buffer
  * can be processed using algorithms in the Standard C++ Library.
  *
  * <br><B>Example</B><br>
  * <pre>
  * #include <scl_iostream.h>
  * #include <scl_fixed_buffer.h>
  * #include <scl_algorithm.h>
  *
  * int
  * main()
  * {
  *     typedef scl_fixed_buffer<int, 32>   Buffer;
  *
  *     size_t  size = 0;
  *     cout << "Enter size of buffer: ";
  *     cin  >> size;
  *
  *     Buffer  buf( size );
  *
  *     cout << "Capacity = " << buf.capacity() << endl;
  *     cout << "Size     = " << buf.size()     << endl;
  *
  *     size_t  idx;
  *     for (idx = 0; idx < buf.size(); ++idx)
  *         buf[idx] = idx;
  *
  *     for (idx = 0; idx < buf.size(); ++idx)
  *         cout << buf[idx] << " ";
  *     cout << endl;
  *
  *     scl_fill(buf.begin(), buf.end(), 137);
  *
  *     for (idx = 0; idx < buf.size(); ++idx)
  *         cout << buf[idx] << " ";
  *     cout << endl;
  *
  *     return 0;
  * }
  *
  * OUTPUT:
  * Enter size of buffer: 5
  * Capacity = 32
  * Size     = 5
  * 0 1 2 3 4
  * 137 137 137 137 137
  * </pre>
  *
  * <br><B>Warnings</B><br>
  *     This class can be used only in situations where the maximum size of the
  * buffer is known at the time of construction.  Unlike an STL container, this
  * class does not resize dynamically.
  *
  */
template <class Type_, size_t Size_>
class scl_fixed_buffer
{
public:
    typedef scl_fixed_buffer<Type_, Size_>  Self_;

    // types:
    typedef Type_                           value_type;
    typedef size_t                          size_type;
    typedef ptrdiff_t                       difference_type;
    typedef Type_*                          pointer;
    typedef const Type_*                    const_pointer;
    typedef Type_&                          reference;
    typedef const Type_&                    const_reference;

    typedef scl_pointer_iterator<Type_>             iterator;
    typedef scl_pointer_const_iterator<Type_>       const_iterator;
    typedef scl_reverse_iterator<iterator>          reverse_iterator;
    typedef scl_reverse_iterator<const_iterator>    const_reverse_iterator;

    // constructors, destructor:
    explicit    scl_fixed_buffer(size_type size);
                scl_fixed_buffer(size_type size, const Type_& value);
               ~scl_fixed_buffer();

    // iterators:
    iterator                begin();
    const_iterator          begin() const;
    iterator                end();
    const_iterator          end() const;
    reverse_iterator        rbegin();
    const_reverse_iterator  rbegin() const;
    reverse_iterator        rend();
    const_reverse_iterator  rend() const;

    // capacity:
    size_type               size() const;
    size_type               capacity() const;

    // element access:
    reference               operator[](size_type idx);
    const_reference         operator[](size_type idx) const;

    // storage access:
    pointer                 data();
    const_pointer           data() const;

private:
    // Disable copy and assignment operations.
    scl_fixed_buffer(const Self_&) {};
    void   operator=(const Self_&) {};

    typedef unsigned char   Byte_;

    Type_*      first_;                         // Pointer to first element
    size_type   size_;                          // Number of elements in buffer
    double      align_;                         // Included for alignment purposes
    Byte_       buffer_[Size_ * sizeof(Type_)]; // Storage for small buffers
};


typedef scl_fixed_buffer<char, 1024>        scl_char_buffer;


typedef scl_fixed_buffer<wchar_t, 1024>     scl_wchar_buffer;


#include <scl_fixed_buffer.cc>


#endif  /* _SCL_FIXED_BUFFER_H_ */
