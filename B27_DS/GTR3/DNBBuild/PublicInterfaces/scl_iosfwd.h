/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: scl_iosfwd.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Oct-2004     Initial implementation.
//
//  NOTICE:
//      This header implements functionality in the Standard C++ Library.
//      It may be included directly in application code.
//
//==============================================================================
#ifndef _SCL_IOSFWD_H_
#define _SCL_IOSFWD_H_


#ifndef  _SCL_CONFIG_H_
#include <scl_config.h>
#endif


#ifdef  SCL_USE_STD_IOSTREAM
#include <iosfwd>

#ifdef  SCL_HAS_NAMESPACES
using SCL_VENDOR_STD::ios;
using SCL_VENDOR_STD::streambuf;
using SCL_VENDOR_STD::istream;
using SCL_VENDOR_STD::ostream;
using SCL_VENDOR_STD::iostream;
using SCL_VENDOR_STD::stringbuf;
using SCL_VENDOR_STD::istringstream;
using SCL_VENDOR_STD::ostringstream;
using SCL_VENDOR_STD::stringstream;
using SCL_VENDOR_STD::filebuf;
using SCL_VENDOR_STD::ifstream;
using SCL_VENDOR_STD::ofstream;
using SCL_VENDOR_STD::fstream;
#endif  /* SCL_HAS_NAMESPACES */

#else
class ios;
class streambuf;
class istream;
class ostream;
class iostream;
class strstreambuf;             // Replaced with stringbuf
class istrstream;               // Replaced with istringstream
class ostrstream;               // Replaced with ostringstream
class strstream;                // Replaced with stringstream
class filebuf;
class ifstream;
class ofstream;
class fstream;

#endif  /* SCL_USE_STD_IOSTREAM */


#endif  /* _SCL_IOSFWD_H_ */
