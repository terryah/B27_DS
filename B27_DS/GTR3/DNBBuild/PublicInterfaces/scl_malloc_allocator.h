/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: scl_malloc_allocator.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     08-Aug-2008     Initial implementation.
//
//  NOTICE:
//      This header extends the capabilities of the Standard C++ Library.
//      It may be included directly in application code.
//
//==============================================================================
#ifndef _SCL_MALLOC_ALLOCATOR_H_
#define _SCL_MALLOC_ALLOCATOR_H_


//
//  Include section
//
#ifndef  _SCL_CLIMITS_H_
#include <scl_climits.h>                // for SCL_SIZE_MAX
#endif

#ifndef  _SCLP_CONSTRUCT_H_
#include <sclp_construct.h>             // for _scl_construct, _scl_destroy
#endif


//
//  20.4.1 Template class scl_malloc_allocator (implementation extension)
//
template <class Type_>
class scl_malloc_allocator;


SCL_SPECIALIZE_CLASS
class scl_malloc_allocator<void>
{
public:
    typedef size_t                      size_type;
    typedef ptrdiff_t                   difference_type;
    typedef void*                       pointer;
    typedef const void*                 const_pointer;
    typedef void                        value_type;

#ifdef  SCL_USE_ALLOCATOR_REBIND
    template <class Other_>
    struct rebind
    {
        typedef scl_malloc_allocator<Other_>    other;
    };
#endif
};


template <class Type_>
class scl_malloc_allocator
{
public:
    typedef scl_malloc_allocator<Type_> Self_;
    typedef size_t                      size_type;
    typedef ptrdiff_t                   difference_type;
    typedef Type_*                      pointer;
    typedef const Type_*                const_pointer;
    typedef Type_&                      reference;
    typedef const Type_&                const_reference;
    typedef Type_                       value_type;

#ifdef  SCL_USE_ALLOCATOR_REBIND
    template <class Other_>
    struct rebind
    {
        typedef scl_malloc_allocator<Other_>    other;
    };
#else
    // The following methods are not part of the C++ standard.  They are used to
    // allocate memory for objects other than Type_, such as nodes in a linked list.
    void* raw_allocate(size_type count, size_type size, const void* hint = NULL)
    {
        void*   ptr = scl_malloc(count * size);
        if (!ptr)
            _scl_throw_bad_alloc();
        return ptr;
    }

    void raw_deallocate(void* ptr, size_type count, size_type size)
    {
        scl_free(ptr);
    }
#endif

    scl_malloc_allocator()
        SCL_NOTHROW
    {
        // Nothing
    }

    scl_malloc_allocator(const Self_&)
        SCL_NOTHROW
    {
        // Nothing
    }

#ifdef  SCL_HAS_MEMBER_TEMPLATES
    template <class Other_>
    scl_malloc_allocator(const scl_malloc_allocator<Other_>&)
        SCL_NOTHROW
    {
        // Nothing
    }
#endif

    ~scl_malloc_allocator()
        SCL_NOTHROW
    {
        // Nothing
    }

    Self_&  operator=(const Self_&)
        SCL_NOTHROW
    {
        return *this;
    }

    pointer address(reference item) const
    {
        return &item;
    }

    const_pointer address(const_reference item) const
    {
        return &item;
    }

    pointer allocate(size_type count, const void* hint = NULL)
    {
        void*   ptr = scl_malloc(count * sizeof(Type_));
        if (!ptr)
            _scl_throw_bad_alloc();
        return static_cast<pointer>(ptr);
    }

    void deallocate(pointer ptr, size_type count)
    {
        scl_free(ptr);
    }

    size_type max_size() const
        SCL_NOTHROW
    {
        // Return the largest N for which allocate(N) might succeed; see 20.4.1.1, p11.
        return (SCL_SIZE_MAX / sizeof(Type_));
    }

    void construct(pointer ptr, const_reference val)
    {
        _scl_construct(ptr, val);
    }

    void destroy(pointer ptr)
    {
        _scl_destroy(ptr);
    }
};


template <class Type1_, class Type2_>
bool
operator==(const scl_malloc_allocator<Type1_>&, const scl_malloc_allocator<Type2_>&)
    SCL_NOTHROW
{
    return true;
}

template <class Type1_, class Type2_>
bool
operator!=(const scl_malloc_allocator<Type1_>&, const scl_malloc_allocator<Type2_>&)
    SCL_NOTHROW
{
    return false;
}


#endif  /* _SCL_MALLOC_ALLOCATOR_H_ */
