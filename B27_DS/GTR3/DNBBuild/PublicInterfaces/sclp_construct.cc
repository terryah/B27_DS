/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2008
//==============================================================================
//
//  FILE: sclp_construct.cc
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     26-Jul-2008     Initial implementation.
//
//==============================================================================


template <class Type_>
class scl_allocator;


//
//  Define optimized specializations of scl_destroy.
//
#define SCL_SPECIALIZE_DESTROY(Type_)                       \
SCL_SPECIALIZE_FUNC inline void                             \
_scl_destroy(Type_*) { }                                    \
SCL_SPECIALIZE_FUNC inline void                             \
_scl_destroy(Type_*, Type_*) { }                            \
SCL_SPECIALIZE_FUNC inline void                             \
_scl_destroy(Type_*, Type_*, scl_allocator<Type_>&) { }


SCL_SPECIALIZE_DESTROY(char)
SCL_SPECIALIZE_DESTROY(signed char)
SCL_SPECIALIZE_DESTROY(unsigned char)

#ifdef  SCL_HAS_DISTINCT_WCHAR
SCL_SPECIALIZE_DESTROY(wchar_t)
#endif

SCL_SPECIALIZE_DESTROY(short)
SCL_SPECIALIZE_DESTROY(unsigned short)

SCL_SPECIALIZE_DESTROY(int)
SCL_SPECIALIZE_DESTROY(unsigned int)

SCL_SPECIALIZE_DESTROY(long)
SCL_SPECIALIZE_DESTROY(unsigned long)

#ifdef  SCL_HAS_LLONG
SCL_SPECIALIZE_DESTROY(SCL_LLONG_TYPE)
SCL_SPECIALIZE_DESTROY(unsigned SCL_LLONG_TYPE)
#endif

#ifdef  SCL_HAS_DISTINCT_BOOL
SCL_SPECIALIZE_DESTROY(bool)
#endif

SCL_SPECIALIZE_DESTROY(float)
SCL_SPECIALIZE_DESTROY(double)
SCL_SPECIALIZE_DESTROY(long double)

#undef  SCL_SPECIALIZE_DESTROY
