/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: sclp_bitset.cc
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Aug-2004     Initial implementation.
//
//==============================================================================
#ifdef  SCL_ENABLE_IO
#include <scl_iostream.h>               // for istream, ostream
#endif


#define SCL_BITSET_TEMPL        template <size_t Size_>
#define SCL_BITSET_CLASS        scl_bitset<Size_>


SCL_BITSET_TEMPL
SCL_BITSET_CLASS::scl_bitset() :
    Base_()
{
    bit_init(Size_, NumWords, buffer_);
}

SCL_BITSET_TEMPL
SCL_BITSET_CLASS::scl_bitset(unsigned long value) :
    Base_()
{
    bit_init(Size_, NumWords, buffer_);
    bit_assign(value);
}

SCL_BITSET_TEMPL
SCL_BITSET_CLASS::scl_bitset(const scl_string& cstr) :
    Base_()
{
    bit_init(Size_, NumWords, buffer_);
    bit_assign(cstr, 0, scl_string::npos);
}

SCL_BITSET_TEMPL
SCL_BITSET_CLASS::scl_bitset(const scl_string& cstr, size_t cpos) :
    Base_()
{
    bit_init(Size_, NumWords, buffer_);
    bit_assign(cstr, cpos, scl_string::npos);
}

SCL_BITSET_TEMPL
SCL_BITSET_CLASS::scl_bitset(const scl_string& cstr, size_t cpos, size_t clen) :
    Base_()
{
    bit_init(Size_, NumWords, buffer_);
    bit_assign(cstr, cpos, clen);
}

SCL_BITSET_TEMPL
SCL_BITSET_CLASS::scl_bitset(const Self_& other) :
    Base_()
{
    bit_init(Size_, NumWords, buffer_);
    bit_assign(other);
}

SCL_BITSET_TEMPL
SCL_BITSET_CLASS::~scl_bitset()
{
    // Nothing
}

SCL_BITSET_TEMPL
SCL_BITSET_CLASS &
SCL_BITSET_CLASS::operator=(const Self_& other)
{
    bit_assign(other);
    return *this;
}

SCL_BITSET_TEMPL
SCL_BITSET_CLASS &
SCL_BITSET_CLASS::operator&=(const Self_& rhs)
{
    bit_and(rhs);
    return *this;
}

SCL_BITSET_TEMPL
SCL_BITSET_CLASS &
SCL_BITSET_CLASS::operator|=(const Self_& rhs)
{
    bit_or(rhs);
    return *this;
}

SCL_BITSET_TEMPL
SCL_BITSET_CLASS &
SCL_BITSET_CLASS::operator^=(const Self_& rhs)
{
    bit_xor(rhs);
    return *this;
}

SCL_BITSET_TEMPL
SCL_BITSET_CLASS &
SCL_BITSET_CLASS::operator<<=(size_t count)
{
    bit_shl(count);
    return *this;
}

SCL_BITSET_TEMPL
SCL_BITSET_CLASS &
SCL_BITSET_CLASS::operator>>=(size_t count)
{
    bit_shr(count);
    return *this;
}

SCL_BITSET_TEMPL
SCL_BITSET_CLASS &
SCL_BITSET_CLASS::set()
{
    bit_set();
    return *this;
}

SCL_BITSET_TEMPL
SCL_BITSET_CLASS &
SCL_BITSET_CLASS::set(size_t pos, bool value)
{
    bit_set(pos, value);
    return *this;
}

SCL_BITSET_TEMPL
SCL_BITSET_CLASS &
SCL_BITSET_CLASS::reset()
{
    bit_reset();
    return *this;
}

SCL_BITSET_TEMPL
SCL_BITSET_CLASS &
SCL_BITSET_CLASS::reset(size_t pos)
{
    bit_reset(pos);
    return *this;
}

SCL_BITSET_TEMPL
SCL_BITSET_CLASS
SCL_BITSET_CLASS::operator~() const
{
    Self_   result(*this);
    result.flip();
    return result;
}

SCL_BITSET_TEMPL
SCL_BITSET_CLASS &
SCL_BITSET_CLASS::flip()
{
    bit_flip();
    return *this;
}

SCL_BITSET_TEMPL
SCL_BITSET_CLASS &
SCL_BITSET_CLASS::flip(size_t pos)
{
    bit_flip(pos);
    return *this;
}

SCL_BITSET_TEMPL
bool
SCL_BITSET_CLASS::operator==(const Self_& rhs) const
{
    return bit_equal(rhs);
}

SCL_BITSET_TEMPL
bool
SCL_BITSET_CLASS::operator!=(const Self_& rhs) const
{
    return !bit_equal(rhs);
}

SCL_BITSET_TEMPL
SCL_BITSET_CLASS
SCL_BITSET_CLASS::operator<<(size_t count) const
{
    Self_   result(*this);
    result.bit_shl(count);
    return result;
}

SCL_BITSET_TEMPL
SCL_BITSET_CLASS
SCL_BITSET_CLASS::operator>>(size_t count) const
{
    Self_   result(*this);
    result.bit_shr(count);
    return result;
}

#ifdef  SCL_HAS_NONTYPE_TMPL_ARGS
SCL_BITSET_TEMPL
SCL_BITSET_CLASS
operator&(const SCL_BITSET_CLASS& lhs, const SCL_BITSET_CLASS& rhs)
{
    SCL_BITSET_CLASS    result(lhs);
    result &= rhs;
    return result;
}

SCL_BITSET_TEMPL
SCL_BITSET_CLASS
operator|(const SCL_BITSET_CLASS& lhs, const SCL_BITSET_CLASS& rhs)
{
    SCL_BITSET_CLASS    result(lhs);
    result |= rhs;
    return result;
}

SCL_BITSET_TEMPL
SCL_BITSET_CLASS
operator^(const SCL_BITSET_CLASS& lhs, const SCL_BITSET_CLASS& rhs)
{
    SCL_BITSET_CLASS    result(lhs);
    result ^= rhs;
    return result;
}

#ifdef  SCL_ENABLE_IO
SCL_BITSET_TEMPL
ostream&
operator<<(ostream& os, const SCL_BITSET_CLASS& item)
{
    os << item.to_string();
    return os;
}
#endif  /* SCL_ENABLE_IO             */
#endif  /* SCL_HAS_NONTYPE_TMPL_ARGS */


#undef  SCL_BITSET_TEMPL
#undef  SCL_BITSET_CLASS
