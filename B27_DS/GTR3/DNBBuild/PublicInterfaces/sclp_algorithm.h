/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
//  FILE: sclp_algorithm.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      jod     01-Aug-2004     Initial implementation.
//
//  NOTICE:
//      This header is used internally by the Standard C++ Library.
//      It should not be included directly in application code.
//
//==============================================================================
#ifndef _SCLP_ALGORITHM_H_
#define _SCLP_ALGORITHM_H_


//
//  Include section
//
#ifndef  _SCLP_BASE_ALGO_H_
#include <sclp_base_algo.h>
#endif

#ifndef  _SCLP_HEAP_ALGO_H_
#include <sclp_heap_algo.h>
#endif


//
//  25.1 Non-Modifying Sequence Operations
//
template <class InpIter_, class Func_>
Func_
scl_for_each(InpIter_ first, InpIter_ last, Func_ func);

template <class InpIter_, class Type_>
InpIter_
scl_find(InpIter_ first, InpIter_ last, const Type_& value);

template <class InpIter_, class Pred_>
InpIter_
scl_find_if(InpIter_ first, InpIter_ last, Pred_ pred);

template <class FwdIter_>
FwdIter_
scl_adjacent_find(FwdIter_ first, FwdIter_ last);

template <class FwdIter_, class Pred_>
FwdIter_
scl_adjacent_find(FwdIter_ first, FwdIter_ last, Pred_ pred);


//
//  25.2 Modifying Sequence Operations
//
template <class FwdIter1_, class FwdIter2_>
FwdIter2_
scl_swap_ranges(FwdIter1_ first1, FwdIter1_ last1, FwdIter2_ first2);

template <class InpIter_, class OutIter_, class UnaOp_>
OutIter_
scl_transform(InpIter_ first, InpIter_ last, OutIter_ result, UnaOp_ oper);

template <class InpIter1_, class InpIter2_, class OutIter_, class BinOp_>
OutIter_
scl_transform(InpIter1_ first1, InpIter1_ last1, InpIter2_ first2, OutIter_ result, BinOp_ oper);

template <class FwdIter_>
FwdIter_
scl_unique(FwdIter_ first, FwdIter_ last);

template <class FwdIter_, class Pred_>
FwdIter_
scl_unique(FwdIter_ first, FwdIter_ last, Pred_ pred);

template <class BidIter_>
void
scl_reverse(BidIter_ first, BidIter_ last);

template <class BidIter_, class OutIter_>
OutIter_
scl_reverse_copy(BidIter_ first, BidIter_ last, OutIter_ result);

template <class FwdIter_>
void
scl_rotate(FwdIter_ first, FwdIter_ middle, FwdIter_ last);

//
//  25.3 Sorting and Related Operations
//
template <class RanIter_>
void
scl_sort(RanIter_ first, RanIter_ last);

template <class RanIter_, class Comp_>
void
scl_sort(RanIter_ first, RanIter_ last, Comp_ comp);

template <class RanIter_>
void
scl_stable_sort(RanIter_ first, RanIter_ last);

template <class RanIter_, class Comp_>
void
scl_stable_sort(RanIter_ first, RanIter_ last, Comp_ comp);

template <class FwdIter_, class Type_>
FwdIter_
scl_lower_bound(FwdIter_ first, FwdIter_ last, const Type_& value);

template <class FwdIter_, class Type_, class Comp_>
FwdIter_
scl_lower_bound(FwdIter_ first, FwdIter_ last, const Type_& value, Comp_ comp);

template <class FwdIter_, class Type_>
FwdIter_
scl_upper_bound(FwdIter_ first, FwdIter_ last, const Type_& value);

template <class FwdIter_, class Type_, class Comp_>
FwdIter_
scl_upper_bound(FwdIter_ first, FwdIter_ last, const Type_& value, Comp_ comp);


#include <sclp_algorithm.cc>


#endif  /* _SCLP_ALGORITHM_H_ */
