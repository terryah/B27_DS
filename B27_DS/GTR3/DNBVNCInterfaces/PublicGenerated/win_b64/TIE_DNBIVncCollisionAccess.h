#ifndef __TIE_DNBIVncCollisionAccess
#define __TIE_DNBIVncCollisionAccess

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "DNBIVncCollisionAccess.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIVncCollisionAccess */
#define declare_TIE_DNBIVncCollisionAccess(classe) \
 \
 \
class TIEDNBIVncCollisionAccess##classe : public DNBIVncCollisionAccess \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIVncCollisionAccess, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall StoreCollision(int &iCollisionRank) ; \
      virtual HRESULT __stdcall ModifyRepresentations(CAT4x4Matrix & oMatrixToApply) ; \
};



#define ENVTIEdeclare_DNBIVncCollisionAccess(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall StoreCollision(int &iCollisionRank) ; \
virtual HRESULT __stdcall ModifyRepresentations(CAT4x4Matrix & oMatrixToApply) ; \


#define ENVTIEdefine_DNBIVncCollisionAccess(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::StoreCollision(int &iCollisionRank)  \
{ \
return (ENVTIECALL(DNBIVncCollisionAccess,ENVTIETypeLetter,ENVTIELetter)StoreCollision(iCollisionRank)); \
} \
HRESULT __stdcall  ENVTIEName::ModifyRepresentations(CAT4x4Matrix & oMatrixToApply)  \
{ \
return (ENVTIECALL(DNBIVncCollisionAccess,ENVTIETypeLetter,ENVTIELetter)ModifyRepresentations(oMatrixToApply)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIVncCollisionAccess(classe)    TIEDNBIVncCollisionAccess##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIVncCollisionAccess(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIVncCollisionAccess, classe) \
 \
 \
CATImplementTIEMethods(DNBIVncCollisionAccess, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIVncCollisionAccess, classe, 1) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIVncCollisionAccess, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIVncCollisionAccess, classe) \
 \
HRESULT __stdcall  TIEDNBIVncCollisionAccess##classe::StoreCollision(int &iCollisionRank)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->StoreCollision(iCollisionRank)); \
} \
HRESULT __stdcall  TIEDNBIVncCollisionAccess##classe::ModifyRepresentations(CAT4x4Matrix & oMatrixToApply)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ModifyRepresentations(oMatrixToApply)); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIVncCollisionAccess(classe) \
 \
 \
declare_TIE_DNBIVncCollisionAccess(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIVncCollisionAccess##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIVncCollisionAccess,"DNBIVncCollisionAccess",DNBIVncCollisionAccess::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIVncCollisionAccess(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIVncCollisionAccess, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIVncCollisionAccess##classe(classe::MetaObject(),DNBIVncCollisionAccess::MetaObject(),(void *)CreateTIEDNBIVncCollisionAccess##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIVncCollisionAccess(classe) \
 \
 \
declare_TIE_DNBIVncCollisionAccess(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIVncCollisionAccess##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIVncCollisionAccess,"DNBIVncCollisionAccess",DNBIVncCollisionAccess::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIVncCollisionAccess(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIVncCollisionAccess, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIVncCollisionAccess##classe(classe::MetaObject(),DNBIVncCollisionAccess::MetaObject(),(void *)CreateTIEDNBIVncCollisionAccess##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIVncCollisionAccess(classe) TIE_DNBIVncCollisionAccess(classe)
#else
#define BOA_DNBIVncCollisionAccess(classe) CATImplementBOA(DNBIVncCollisionAccess, classe)
#endif

#endif
