// COPYRIGHT Dassault Systemes 2006
//===================================================================
//
// DNBIVncIKServices.h
// Define the DNBIVncIKServices interface
//
//===================================================================
//
// Usage notes:
//   New interface: Exposes IK services to PP partners.
//
//===================================================================
//
//  OCT 2006  Creation: Code generated by the CAA wizard  KAK
//===================================================================

#ifndef DNBIVncIKServices_H
#define DNBIVncIKServices_H

/**
* @CAA2Level L0
* @CAA2Usage U3
*/
#include "DNBVNCInterfaceCPP.h"
#include "CATBaseUnknown.h"
#include "CATListOfDouble.h"

extern ExportedByDNBVNCInterfaceCPP IID IID_DNBIVncIKServices;

//------------------------------------------------------------------

/**
* Interface to expose Inverse Kinematic Services useful for RTCP instructions
* <p>
* Using this prefered syntax will enable mkdoc to document your class.
*/

class ExportedByDNBVNCInterfaceCPP DNBIVncIKServices: public CATBaseUnknown
{
	CATDeclareInterface;

public:
	
	/**
	* Returns a solution and tool orientation vector for the machine corresponding to the ToolPosition
	* while maintaining the user defined rotary joint values.
	* @param iJntValues (user input)
	*     Initial joint values
	* @param iToolPosition (user input)
	*     The tool position w.r.t. the initial work-piece mount point frame (static frame)
	* @param iRotaryJntValues (user input)
	*     The rotary joint values which are to be maintained (list size equals the number of machine DOF's)
	* @param oToolVector (output)
	*     The tool vector (IJK, array of size 3) corresponding to the rotary joint values input
	* @param oJntValues (output)
	*     A solution (list of DOF's of size equal to number of DOF) for the above input position
	*/	
	virtual HRESULT  __stdcall GetIKMachineSolution(CATListOfDouble& iJntValues,
		double* iToolPosition,
		CATListOfDouble& iRotaryJntValues,
		double* oToolVector,
		CATListOfDouble& oJntValues)=0;
	
	
	/**
	* Gets the tool position and vector w.r.t. work-piece mount point frame (static) corresponding
	*     to the joint values
	* @param iJntValues (user input)
	*     Joint values corresponding to which tool position and vector are sought
	* @param oToolPosition (output)
	*     The tool position corresponding to the above input joint values (XYZ, array of size 3)
	* @param oToolVector
	*     The tool vector corresponding to the input joint values (IJK, array of size 3)
	*/	
	virtual HRESULT  __stdcall GetIKToolPosition(CATListOfDouble& iJntValues,
		double* oToolPosition,
		double* oToolVector)=0;

	
	
	/**
	* Returns all solutions for the machine corresponding to the ToolPosition and vector (XYZIJK)
	* described w.r.t. the work-piece mount point frame taken at the intial conditions
	* @param iJntValues (user input)
	*     Joint values for the machine at which the tool vector and position are described w.r.t.
	*     work-piece mount point frame (WMP frame).
	* @param iToolPosition (user input)
	*     The position where the tool should reach w.r.t. WMP frame (XYZ)
	* @param iToolVector (user input)
	*     The vector along which the tool should reach the above position (IJK) w.r.t. WMP frame
	* @param oNoSols (output)
	*     The number of solutions returned
	* @param oSols (output value, pass NULL initialized pointer, the method does a new)
	*     The joint values (solutions) which correspond to the above tool position and vector
	*	NOTE: oSols has to be deleted with a 'delete [] oSols' by the user who calls this method.
	* Example usage:
	CATListOfDouble* solJntValues = NULL;
	// Declare/define other parameters as well and call GetIkMachineSolutions
	GetIkMachineSolutions(iJntValues, iTP, iToolVector, oNoSols, solJntValues);
	// use the returned solutions...
	delete [] solJntValues;	// user deletes the returned solutions
	*/
	virtual HRESULT  __stdcall GetIKMachineSolutions(double* iJntValues,	// initial joints position
		double* iToolPosition,
		double* iToolVector,
		unsigned int& oNoSols,
		CATListOfDouble* &oSols)=0;

};

CATDeclareHandler( DNBIVncIKServices, CATBaseUnknown ) ;
//------------------------------------------------------------------

#endif	// DNBIVncIKServices_H
