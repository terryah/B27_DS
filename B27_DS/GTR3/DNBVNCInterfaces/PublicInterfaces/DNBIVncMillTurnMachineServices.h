// COPYRIGHT Dassault Systemes 2006
//===================================================================
#ifndef DNBIVncMillTurnMachineServices_H
#define DNBIVncMillTurnMachineServices_H

/**
* @CAA2Level L1
* @CAA2Usage U3
*/

#include "DNBVNCInterfaceCPP.h"
#include "CATBaseUnknown.h"

class CATListValCATBaseUnknown_var;

extern ExportedByDNBVNCInterfaceCPP IID IID_DNBIVncMillTurnMachineServices;

//------------------------------------------------------------------

/**
* Interface representing xxx.
*
* <br><b>Role</b>: Components that implement
* DNBIVncMillTurnMachineServices are ...
* <p>
* Do not use the DNBIVncMillTurnMachineServices interface for such and such

*
* @example
*  // example is optional
*  DNBIVncMillTurnMachineServices* currentDisplay = NULL;
*  rc = window-&gt;QueryInterface(IID_DNBIVncMillTurnMachineServices,
*                                     (void**) &amp;currentDisplay);
*
* @href ClassReference, Class#MethodReference, #InternalMethod...
*/
class ExportedByDNBVNCInterfaceCPP DNBIVncMillTurnMachineServices: public CATBaseUnknown
{
	CATDeclareInterface;

public:

	/**
	* Method to get the list of all the turrets that are
	* directly aggregated under a MillTurn Machine.
	* 
	*   @return
	*      Returns a CATListValCATBaseUnknown* to all the turrets
	*      aggregated <b>directly</b> under mill-turn machine.
	*/
	virtual CATListValCATBaseUnknown_var* __stdcall GetTurrets () = 0;
	
	/**
	* Method to get the list of all the spindles that are
	* directly aggregated under a MillTurn Machine.
	* 
	*   @return
	*      Returns a CATListValCATBaseUnknown* to all the spindles
	*      aggregated <b>directly</b> under mill-turn machine.
	*/
	virtual CATListValCATBaseUnknown_var* __stdcall GetSpindles() = 0;

	/**
	* Method to get the list of all the mechanisms(other than spindles & turrets) 
	* that are directly aggregated under a MillTurn Machine.
	* 
	*   @return
	*      Returns a CATListValCATBaseUnknown* to all the mechanisms
	*      aggregated <b>directly</b> under mill-turn machine.
	*/
	virtual CATListValCATBaseUnknown_var* __stdcall GetOtherMechanisms() = 0;
	
};

CATDeclareHandler( DNBIVncMillTurnMachineServices, CATBaseUnknown ) ;
//------------------------------------------------------------------

#endif
