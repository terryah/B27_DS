/* -*-c++-*- */
//=============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2002
//=============================================================================



#ifndef DNBIVncTPModify_H
#define DNBIVncTPModify_H

/**
* @CAA2Level L0
* @CAA2Usage U3
*/

#include "DNBVNCInterfaceCPP.h"
#include "CATBaseUnknown.h"
#include "CATListOfDouble.h"

extern ExportedByDNBVNCInterfaceCPP IID IID_DNBIVncTPModify;

/**
* Interface to manage VNC Tool Path Modification
* <br><b>Role</b>: DNBIVNCTPModify has methods to 
* manage VNC Tool Path Modification
*/

class ExportedByDNBVNCInterfaceCPP DNBIVncTPModify: public CATBaseUnknown
{
	CATDeclareInterface;

	public:

	virtual HRESULT StartToolPathModifcation (int & iRank, CATBaseUnknown * piMO, CATListOfDouble & iCurPos, 
		                                      int iTeachRange = 0) = 0;

	/**
	 * Service to do Partial simulation
	 * piCurrentMP    ==> MO to be partially simulated.
	 * iStartDOFIndex ==> Starting rank for the partial simulation
	 * iEndDOFIndex   ==> Ending rank for the partial simualtion
	 */

	virtual HRESULT DoPartialSimulation (CATBaseUnknown * piCurrentMO, int iStartDOFIndex, int iEndDOFIndex) = 0;
};

CATDeclareHandler (DNBIVncTPModify, CATBaseUnknown);

#endif
