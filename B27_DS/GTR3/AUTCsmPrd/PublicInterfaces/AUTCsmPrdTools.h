// COPYRIGHT DASSAULT SYSTEMES 2005
/** @CAA2Required */
/**********************************************************************/
/* DON T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPEAR AT ANY TIME */
/**********************************************************************/
/**
 * @fullreview AZH 05:03:24
 */
#ifdef  _WINDOWS_SOURCE
#ifdef  __AUTCsmPrdTools
#define ExportedByAUTCsmPrdTools     __declspec(dllexport)
#else
#define ExportedByAUTCsmPrdTools     __declspec(dllimport)
#endif
#else
#define ExportedByAUTCsmPrdTools
#endif
