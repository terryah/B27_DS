# COPYRIGHT DASSAULT SYSTEMES 2005
#======================================================================
# Imakefile for module AUTCsmPrdImpl.m
#======================================================================
#
#  Mar 2005  Creation: Code generated by the CAA wizard  azh
#======================================================================
#
# SHARED LIBRARY 
#
BUILT_OBJECT_TYPE=SHARED LIBRARY 
 
# DO NOT EDIT :: THE CAA2 WIZARDS WILL ADD CODE HERE
WIZARD_LINK_MODULES = JS0GROUP AD0XXBAS  \
JS0FM AUTCsmInterfacesUUID AS0STARTUP \
AUTCsmIS 
# END WIZARD EDITION ZONE

LINK_WITH = $(WIZARD_LINK_MODULES) AUTCsmOptions AUTCsmItf AUTClmItf AUTCsmPrdItf AUTLciPrd AUTCmnTools AUTLciImplCpp AUTCsmPrdTools 

# System dependant variables
#
OS = AIX
BUILD=NO
#
OS = HP-UX
BUILD=NO
#
OS = IRIX
BUILD=NO
#
OS = SunOS
BUILD=NO
#
OS = Windows_NT

#
OS = win_b64
BUILD=YES
