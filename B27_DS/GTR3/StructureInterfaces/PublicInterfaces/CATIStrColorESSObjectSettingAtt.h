// COPYRIGHT Dassault Systemes 2003
//===================================================================
//
// CATIStrColorESSObjectSettingAtt.h
// Define the CATIStrColorESSObjectSettingAtt interface
//
//===================================================================
//
// Usage notes:
//   New interface: describe its use here
//
//===================================================================
//
//  Nov 2003  Creation: Code generated by the CAA wizard  jcm
//===================================================================
#ifndef CATIStrColorESSObjectSettingAtt_H
#define CATIStrColorESSObjectSettingAtt_H

/**
  * @CAA2Level L1
  * @CAA2Usage U3
  */

#include "CATBaseUnknown.h"
#include "CATStrSettingsItfCPP.h"

class CATSettingInfo;
class CATUnicodeString;

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByCATStrSettingsItfCPP IID IID_CATIStrColorESSObjectSettingAtt;
#else
extern "C" const IID IID_CATIStrColorESSObjectSettingAtt ;
#endif

//------------------------------------------------------------------

/**
 * Describe the ESSctionality of your interface here.
 * <p>
 * Using this prefered syntax will enable mkdoc to document your class.
 */
class ExportedByCATStrSettingsItfCPP CATIStrColorESSObjectSettingAtt : public CATBaseUnknown
{
  CATDeclareInterface;

  public:

    //***********************************************************************
    // Ensure consistency with the IDL interface that will delegate its work
    // Check allowed signatures in System.CATSysSettingController
    //***********************************************************************
    /**
     * Retrieves the MemberColor attribute.
     */
     virtual HRESULT GetMemberColor( unsigned int & oMemberColorR,
		                             unsigned int & oMemberColorG,
								     unsigned int & oMemberColorB ) = 0;

	/**
     * Sets the MemberColor attribute.
     */
     virtual HRESULT SetMemberColor( unsigned int iMemberColorR,
		                             unsigned int iMemberColorG,
									 unsigned int iMemberColorB ) = 0;

    /**
     * Retrieves the state of the MemberColor parameter.
     * @param oInfo
     *	Address of an object CATSettingInfo.
     * @return
     *	<b>Legal values</b>:
     *	<br><tt>S_OK :</tt>   on Success
     * 	<br><tt>E_FAIL:</tt>  on failure
     */
     virtual HRESULT GetMemberColorInfo( CATSettingInfo*    oInfo ) = 0;

    /** 
     * Locks or unlocks the MemberColor parameter.
     * <br><b>Role</b>: Locks or unlocks the MemberColor parameter if the
     * operation is allowed in the current administrated environment. In user mode 
     * this method will always return E_FAIL.
     * @param iLocked
     *	the locking operation to be performed
     *	<b>Legal values</b>:
     *	<br><tt>1 :</tt>   to lock the parameter.
     * 	<br><tt>0:</tt>   to unlock the parameter.
     * @return
     *	<b>Legal values</b>:
     *	<br><tt>S_OK :</tt>   on Success
     * 	<br><tt>E_FAIL:</tt>  on failure
     */
     virtual HRESULT SetMemberColorLock( unsigned char iLocked ) = 0;

    /**
     * Retrieves the PlateColor attribute.
     */
     virtual HRESULT GetPlateColor( unsigned int & oPlateColorR,
		                            unsigned int & oPlateColorG,
									unsigned int & oPlateColorB ) = 0;

	/**
     * Sets the PlateColor attribute.
     */
     virtual HRESULT SetPlateColor( unsigned int iPlateColorR,
		                            unsigned int iPlateColorG,
									unsigned int iPlateColorB ) = 0;

    /**
     * Retrieves the state of the PlateColor parameter.
     * @param oInfo
     *	Address of an object CATSettingInfo.
     * @return
     *	<b>Legal values</b>:
     *	<br><tt>S_OK :</tt>   on Success
     * 	<br><tt>E_FAIL:</tt>  on failure
     */
     virtual HRESULT GetPlateColorInfo( CATSettingInfo* oInfo ) = 0;

    /** 
     * Locks or unlocks the PlateColor parameter.
     * <br><b>Role</b>: Locks or unlocks the PlateColor parameter if the
     * operation is allowed in the current administrated environment. In user mode 
     * this method will always return E_FAIL.
     * @param iLocked
     *	the locking operation to be performed
     *	<b>Legal values</b>:
     *	<br><tt>1 :</tt>   to lock the parameter.
     * 	<br><tt>0:</tt>   to unlock the parameter.
     * @return
     *	<b>Legal values</b>:
     *	<br><tt>S_OK :</tt>   on Success
     * 	<br><tt>E_FAIL:</tt>  on failure
     */
     virtual HRESULT SetPlateColorLock( unsigned char iLocked ) = 0;

  // No constructors or destructors on this pure virtual base class
  // --------------------------------------------------------------
};

//------------------------------------------------------------------

#endif
