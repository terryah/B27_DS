/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAStrMember_h
#define CATIAStrMember_h

#ifndef ExportedByStructurePubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __StructurePubIDL
#define ExportedByStructurePubIDL __declspec(dllexport)
#else
#define ExportedByStructurePubIDL __declspec(dllimport)
#endif
#else
#define ExportedByStructurePubIDL
#endif
#endif

#include "CATBSTR.h"
#include "CATIAStrObject.h"
#include "CATIAStrServices.h"

class CATIADocument;
class CATIAParameter;
class CATIAReference;
class CATIAStrAnchorPoint;
class CATIAStrCutback;
class CATIAStrMemberExtremity;
class CATIAStrSection;

extern ExportedByStructurePubIDL IID IID_CATIAStrMember;

class ExportedByStructurePubIDL CATIAStrMember : public CATIAStrObject
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_Section(CATIAStrSection *& oSection)=0;

    virtual HRESULT __stdcall get_StartExtremity(CATIAStrMemberExtremity *& oExtremity)=0;

    virtual HRESULT __stdcall get_EndExtremity(CATIAStrMemberExtremity *& oExtremity)=0;

    virtual HRESULT __stdcall get_CurrentAnchorPointName(CATBSTR & oName)=0;

    virtual HRESULT __stdcall put_CurrentAnchorPointName(const CATBSTR & iName)=0;

    virtual HRESULT __stdcall get_Angle(double & oAngle)=0;

    virtual HRESULT __stdcall put_Angle(double iAngle)=0;

    virtual HRESULT __stdcall get_AngleParameter(CATIAParameter *& oAngle)=0;

    virtual HRESULT __stdcall Rotate(double iAngle)=0;

    virtual HRESULT __stdcall Flip()=0;

    virtual HRESULT __stdcall get_SurfaceReference(CATIAReference *& oSurface)=0;

    virtual HRESULT __stdcall put_SurfaceReference(CATIAReference * iSurface)=0;

    virtual HRESULT __stdcall get_Support(CATIAReference *& oSupport)=0;

    virtual HRESULT __stdcall get_InputSupport(CATIAReference *& oInputSupport)=0;

    virtual HRESULT __stdcall CreateCutback(CATIAStrMember * iMember, CatStrCutbackType iCutback, double iOffset, CATIAStrCutback *& oCutback)=0;


};

CATDeclareHandler(CATIAStrMember, CATIAStrObject);

#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIAAnalyze.h"
#include "CATIABase.h"
#include "CATIACollection.h"
#include "CATIAMove.h"
#include "CATIAParameters.h"
#include "CATIAPosition.h"
#include "CATIAProduct.h"
#include "CATIAPublications.h"
#include "CATSafeArray.h"
#include "CATVariant.h"
#include "CatFileType.h"
#include "CatProductSource.h"
#include "CatRepType.h"
#include "CatWorkModeType.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
