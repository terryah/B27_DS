/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAPPGExecLogSettingAtt_h
#define CATIAPPGExecLogSettingAtt_h

#include "CATCORBABoolean.h"
#include "CAT_VARIANT_BOOL.h"

#ifndef ExportedByStructurePubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __StructurePubIDL
#define ExportedByStructurePubIDL __declspec(dllexport)
#else
#define ExportedByStructurePubIDL __declspec(dllimport)
#endif
#else
#define ExportedByStructurePubIDL
#endif
#endif

#include "CATBSTR.h"
#include "CATIASettingController.h"

extern ExportedByStructurePubIDL IID IID_CATIAPPGExecLogSettingAtt;

class ExportedByStructurePubIDL CATIAPPGExecLogSettingAtt : public CATIASettingController
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_ExecLogPath(CATBSTR & oPath)=0;

    virtual HRESULT __stdcall get_GenExecLog(CAT_VARIANT_BOOL & oFlag)=0;

    virtual HRESULT __stdcall put_ExecLogPath(const CATBSTR & iPath)=0;

    virtual HRESULT __stdcall put_GenExecLog(CAT_VARIANT_BOOL iFlag)=0;

    virtual HRESULT __stdcall GetExecLogPathInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall GetGenExecLogInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetExecLogPathLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall SetGenExecLogLock(CAT_VARIANT_BOOL iLocked)=0;


};

CATDeclareHandler(CATIAPPGExecLogSettingAtt, CATIASettingController);

#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIABase.h"
#include "CATSafeArray.h"
#include "CATVariant.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
