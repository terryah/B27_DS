/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAStrNibblingFeature_h
#define CATIAStrNibblingFeature_h

#ifndef ExportedByStructurePubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __StructurePubIDL
#define ExportedByStructurePubIDL __declspec(dllexport)
#else
#define ExportedByStructurePubIDL __declspec(dllimport)
#endif
#else
#define ExportedByStructurePubIDL
#endif
#endif

#include "CATBSTR.h"
#include "CATIABase.h"

class CATIADocument;
class CATIAStrFoundation;
class CATIAStrMember;
class CATIAStrPlate;
class CATIAStrSection;

extern ExportedByStructurePubIDL IID IID_CATIAStrNibblingFeature;

class ExportedByStructurePubIDL CATIAStrNibblingFeature : public CATIABase
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_Type(CATBSTR & oNibblingType)=0;

    virtual HRESULT __stdcall put_Type(const CATBSTR & iNibblingType)=0;

    virtual HRESULT __stdcall get_SubType(CATBSTR & oNibblingSubType)=0;

    virtual HRESULT __stdcall put_SubType(const CATBSTR & iNibblingSubType)=0;

    virtual HRESULT __stdcall GetOffsetForExtrapolate(const CATBSTR & iOffsetForExtrapolate)=0;


};

CATDeclareHandler(CATIAStrNibblingFeature, CATIABase);

#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIAParameter.h"
#include "CATIAReference.h"
#include "CATSafeArray.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
