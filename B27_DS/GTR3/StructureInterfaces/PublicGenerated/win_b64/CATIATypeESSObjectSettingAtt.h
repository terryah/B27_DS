/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIATypeESSObjectSettingAtt_h
#define CATIATypeESSObjectSettingAtt_h

#include "CATCORBABoolean.h"
#include "CAT_VARIANT_BOOL.h"

#ifndef ExportedByStructurePubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __StructurePubIDL
#define ExportedByStructurePubIDL __declspec(dllexport)
#else
#define ExportedByStructurePubIDL __declspec(dllimport)
#endif
#else
#define ExportedByStructurePubIDL
#endif
#endif

#include "CATBSTR.h"
#include "CATIASettingController.h"

extern ExportedByStructurePubIDL IID IID_CATIATypeESSObjectSettingAtt;

class ExportedByStructurePubIDL CATIATypeESSObjectSettingAtt : public CATIASettingController
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_MemberTypes(CATBSTR & oMemberTypes)=0;

    virtual HRESULT __stdcall put_MemberTypes(const CATBSTR & iMemberTypes)=0;

    virtual HRESULT __stdcall GetMemberTypesInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetMemberTypesLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_PlateTypes(CATBSTR & oPlateTypes)=0;

    virtual HRESULT __stdcall put_PlateTypes(const CATBSTR & iPlateTypes)=0;

    virtual HRESULT __stdcall GetPlateTypesInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetPlateTypesLock(CAT_VARIANT_BOOL iLocked)=0;


};

CATDeclareHandler(CATIATypeESSObjectSettingAtt, CATIASettingController);

#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIABase.h"
#include "CATSafeArray.h"
#include "CATVariant.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
