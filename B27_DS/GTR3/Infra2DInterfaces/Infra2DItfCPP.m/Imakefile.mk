# COPYRIGHT DASSAULT SYSTEMES 2004
#======================================================================
# Imakefile for module Infra2DItfCpp.m
#
#======================================================================
#
#  Oct 2004  Creation: Code generated by the CAA wizard  sxu
#======================================================================
#

#ifdef LOCAL_DEFINITION_FOR_IID
LINK_WITH_FOR_IID = SystemUUID \
                    ObjectSpecsModelerUUID 
#else
LINK_WITH_FOR_IID =
#endif

BUILT_OBJECT_TYPE = SHARED LIBRARY

LINK_WITH = $(LINK_WITH_FOR_IID) \
            JS0GROUP \
            AD0XXBAS \ #for CATIPersistStorage
            CATObjectSpecsModeler\ #for CATISpecObject

 

#INSERTION ZONE NOT FOUND, MOVE AND APPEND THIS VARIABLE IN YOUR LINK STATEMENT
#LINK_WITH = ... $(WIZARD_LINK_MODULES) ...
# DO NOT EDIT :: THE CAA2 WIZARDS WILL ADD CODE HERE
WIZARD_LINK_MODULES =  \
JS0GROUP JS0FM JS0GROUP Infra2DInterfacesUUID 
# END WIZARD EDITION ZONE

