/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIASfmEndcutManager_h
#define CATIASfmEndcutManager_h

#ifndef ExportedByCATSfmPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __CATSfmPubIDL
#define ExportedByCATSfmPubIDL __declspec(dllexport)
#else
#define ExportedByCATSfmPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByCATSfmPubIDL
#endif
#endif

#include "CATBSTR.h"
#include "CATIABase.h"
#include "CATSafeArray.h"

class CATIASfmConnectionParameters;

extern ExportedByCATSfmPubIDL IID IID_CATIASfmEndcutManager;

class ExportedByCATSfmPubIDL CATIASfmEndcutManager : public CATIABase
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall GetAvailableEndcuts(const CATBSTR & iSectionFamily, const CATBSTR & iEndcutType, CATSafeArrayVariant *& oListEndcutNames)=0;

    virtual HRESULT __stdcall GetEndcutSpecifications(const CATBSTR & iSectionFamily, const CATBSTR & iEndcutType, const CATBSTR & iEndcutName, CATSafeArrayVariant *& oListOfContextNames, CATIASfmConnectionParameters *& oListEndCutParameters, CATSafeArrayVariant *& oListOfEndCutParamNames)=0;


};

CATDeclareHandler(CATIASfmEndcutManager, CATIABase);

#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIACollection.h"
#include "CATIAParameter.h"
#include "CATIAReference.h"
#include "CATIASfmConnectionParameters.h"
#include "CATIASfmReferences.h"
#include "CATVariant.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
