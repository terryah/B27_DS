/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIASfmSlot_h
#define CATIASfmSlot_h

#ifndef ExportedByCATSfmPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __CATSfmPubIDL
#define ExportedByCATSfmPubIDL __declspec(dllexport)
#else
#define ExportedByCATSfmPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByCATSfmPubIDL
#endif
#endif

#include "CATBSTR.h"
#include "CATIABase.h"
#include "CATSafeArray.h"

class CATIAReference;
class CATIASfmConnectionParameters;

extern ExportedByCATSfmPubIDL IID IID_CATIASfmSlot;

class ExportedByCATSfmPubIDL CATIASfmSlot : public CATIABase
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall GetMasterObject(CATIAReference *& oPenetratingObject)=0;

    virtual HRESULT __stdcall GetSlaveObject(CATIAReference *& oPenetratedObject)=0;

    virtual HRESULT __stdcall SetSlaveObject(CATIAReference * iSlaveObject)=0;

    virtual HRESULT __stdcall GetCntnDetailName(CATBSTR & oUDFName)=0;

    virtual HRESULT __stdcall GetCntnDetailParameters(CATIASfmConnectionParameters *& oListOfSlotParameters, CATSafeArrayVariant *& oListOfParameterNames)=0;

    virtual HRESULT __stdcall GetConnectionCoordinate(CATSafeArrayVariant *& oCoordinate)=0;

    virtual HRESULT __stdcall UpdateConnectionsSet()=0;


};

CATDeclareHandler(CATIASfmSlot, CATIABase);

#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIACollection.h"
#include "CATIAParameter.h"
#include "CATIAReference.h"
#include "CATIASfmConnectionParameters.h"
#include "CATIASfmReferences.h"
#include "CATVariant.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
