/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIASfmFunctionFactory_h
#define CATIASfmFunctionFactory_h

#ifndef ExportedByCATSfmPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __CATSfmPubIDL
#define ExportedByCATSfmPubIDL __declspec(dllexport)
#else
#define ExportedByCATSfmPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByCATSfmPubIDL
#endif
#endif

#include "CATBSTR.h"
#include "CATIAFactory.h"

class CATBaseDispatch;
class CATIAPart;
class CATIAReference;
class CATIASfmOpening;
class CATIASfmReferences;
class CATIASfmStandardContourParameters;
class CATIASfmStandardOpening;
class CATIASfmStandardPosStrategyParameters;

extern ExportedByCATSfmPubIDL IID IID_CATIASfmFunctionFactory;

class ExportedByCATSfmPubIDL CATIASfmFunctionFactory : public CATIAFactory
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall CreateOpening(const CATBSTR & iCategory, CATLONG iMode, CATIAReference * iIntersectingElement, CATIAReference * iSfmObject, CATIASfmOpening *& oOpening)=0;

    virtual HRESULT __stdcall CreateStandardOpening(const CATBSTR & iCategory, const CATBSTR & iContourName, CATIASfmStandardContourParameters * iListContourParams, const CATBSTR & iPosStrategyName, CATIASfmStandardPosStrategyParameters * iPositionStrategyParms, CATIAReference * ispTargetSfmObject, CATIASfmStandardOpening *& ospOpening)=0;

    virtual HRESULT __stdcall GetOpeningMgr(CATIAPart * iPrtPart, const CATBSTR & iMgrName, CATBaseDispatch *& oSfmOpeningMgr)=0;

    virtual HRESULT __stdcall get_SfmReferences(CATIASfmReferences *& oSfmReferences)=0;


};

CATDeclareHandler(CATIASfmFunctionFactory, CATIAFactory);

#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIABase.h"
#include "CATIACollection.h"
#include "CATIAParameter.h"
#include "CATIAPart.h"
#include "CATIAReference.h"
#include "CATIAReferences.h"
#include "CATIASfmOpening.h"
#include "CATIASfmOpeningContoursMgr.h"
#include "CATIASfmPositioningStrategyManager.h"
#include "CATIASfmReferences.h"
#include "CATIASfmStandardContourParameters.h"
#include "CATIASfmStandardOpening.h"
#include "CATIASfmStandardPosStrategyParameters.h"
#include "CATSafeArray.h"
#include "CATVariant.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
