/**
* @fullreview BE9 XEX 14:02:26
*/
//=============================================================================
// COPYRIGHT Dassault Systemes 2014
//=============================================================================
// CATIASfmObjectExt
//   Interface to get computed attributes on structural objects.
//=============================================================================
// Usage notes:
//=============================================================================
// Jan. 2014  Creation                                          Bhupendra MITHE
//=============================================================================

#ifndef CATIASfmObjectExt_IDL
#define CATIASfmObjectExt_IDL
/*IDLREP*/

/**
* @CAA2Level L1
* @CAA2Usage U3
*/

#include "CATIABase.idl"

/**
 * Interface to retrieve the computed attributes of SFM objects.
 * <b>Role</b>: Retrieve the computed attributes.
 */
interface CATIASfmObjectExt : CATIABase
{
  /**
  * Returns the COG X coordinate
  *<dl>
  *<dt>Example</dt>:
  *<dd>
  * This example retrieves the X coordinate of the COG of the structural object.
  * <pre>
  * Dim CoG_x As Double
  * CoG_x = SfmObjectExt.<font color="red">CoG_x</font>
  * </pre>
  * </dl> 
  */ 
  #pragma PROPERTY CoG_x
  HRESULT get_CoG_x(out /*IDLRETVAL*/ double oCoG_x);

  /**
  * Returns the COG Y coordinate
  *<dl>
  *<dt>Example</dt>:
  *<dd>
  * This example retrieves the Y coordinate of the COG of the structural object.
  * <pre>
  * Dim CoG_y As Double
  * CoG_y = SfmObjectExt.<font color="red">CoG_y</font>
  * </pre>
  * </dl> 
  */ 
  #pragma PROPERTY CoG_y
  HRESULT get_CoG_y(out /*IDLRETVAL*/ double oCoG_y);

  /**
  * Returns the COG Z coordinate
  *<dl>
  *<dt>Example</dt>:
  *<dd>
  * This example retrieves the Z coordinate of the COG of the structural object.
  * <pre>
  * Dim CoG_z As Double
  * CoG_z = SfmObjectExt.<font color="red">CoG_z</font>
  * </pre>
  * </dl> 
  */
  #pragma PROPERTY CoG_z
  HRESULT get_CoG_z(out /*IDLRETVAL*/ double oCoG_z);

  /**
  * Returns the grade of the structural object.
  *<dl>
  *<dt>Example</dt>:
  *<dd>
  * This example retrieves the grade of the structural object.
  * <pre>
  * Dim Grade As String
  * Grade = SfmObjectExt.<font color="red">Grade</font>
  * </pre>
  * </dl> 
  */
  #pragma PROPERTY Grade
  HRESULT get_Grade(inout /*IDLRETVAL*/ CATBSTR oGrade);

  /**
  * Returns whether the structural object is super object.
  *<dl>
  *<dt>Example</dt>:
  *<dd>
  * This example retrieves whether the structural object is super object.
  * <pre>
  * Dim SuperObject As Boolean
  * SuperObject = SfmObjectExt.<font color="red">IsASuperObject</font>
  * </pre>
  * </dl> 
  */
  #pragma PROPERTY IsASuperObject
  HRESULT get_IsASuperObject(out /*IDLRETVAL*/ boolean oIsSuperObject);

  /**
  * Returns the material of the structural object.
  *<dl>
  *<dt>Example</dt>:
  *<dd>
  * This example retrieves the material of the structural object.
  * <pre>
  * Dim material As String
  * material = SfmObjectExt.<font color="red">Material</font>
  * </pre>
  * </dl> 
  */
  #pragma PROPERTY Material
  HRESULT get_Material(inout /*IDLRETVAL*/ CATBSTR oMaterial);

  /**
  * Returns the PaintedArea of the structural object.
  *<dl>
  *<dt>Example</dt>:
  *<dd>
  * This example retrieves the PaintedArea of the structural object.
  * <pre>
  * Dim PaintedArea As Double
  * PaintedArea = SfmObjectExt.<font color="red">PaintedArea</font>
  * </pre>
  * </dl> 
  */
  #pragma PROPERTY PaintedArea
  HRESULT get_PaintedArea(out /*IDLRETVAL*/ double oPaintedArea);

  /**
  * Returns the ProfileLength of the profile structural object.
  *<dl>
  *<dt>Example</dt>:
  *<dd>
  * This example retrieves the ProfileLength of the structural object.
  * <pre>
  * Dim ProfileLength As Double
  * ProfileLength = SfmObjectExt.<font color="red">ProfileLength</font>
  * </pre>
  * </dl> 
  */
  #pragma PROPERTY ProfileLength
  HRESULT get_ProfileLength(out /*IDLRETVAL*/ double oProfileLength);

  /**
  * Returns the Weight of the structural object.
  *<dl>
  *<dt>Example</dt>:
  *<dd>
  * This example retrieves the Weight of the structural object.
  * <pre>
  * Dim Weight As Double
  * Weight = SfmObjectExt.<font color="red">Weight</font>
  * </pre>
  * </dl> 
  */
  #pragma PROPERTY Weight
  HRESULT get_Weight(out /*IDLRETVAL*/ double oWeight);

  /**
  * Returns the WeldingLength of the structural object.
  *<dl>
  *<dt>Example</dt>:
  *<dd>
  * This example retrieves the WeldingLength of the structural object.
  * <pre>
  * Dim WeldingLength As Double
  * WeldingLength = SfmObjectExt.<font color="red">WeldingLength</font>
  * </pre>
  * </dl> 
  */
  #pragma PROPERTY WeldingLength
  HRESULT get_WeldingLength(out /*IDLRETVAL*/ double oWeldingLength);

  /**
  * Returns the Support Name of the structural object.
  *<dl>
  *<dt>Example</dt>:
  *<dd>
  * This example retrieves the Support Name of the structural object.
  * <pre>
  * Dim SupportName As String
  * SupportName = SfmObjectExt.<font color="red">SupportName</font>
  * </pre>
  * </dl> 
  */
  #pragma PROPERTY SupportName
  HRESULT get_SupportName(inout /*IDLRETVAL*/ CATBSTR oSupportName);

  /**
  * Returns the Support Offset of the structural object.
  *<dl>
  *<dt>Example</dt>:
  *<dd>
  * This example retrieves the Support Offset of the structural object.
  * <pre>
  * Dim SupportOffset As Double
  * SupportOffset = SfmObjectExt.<font color="red">SupportOffset</font>
  * </pre>
  * </dl> 
  */
  #pragma PROPERTY SupportOffset
  HRESULT get_SupportOffset(out /*IDLRETVAL*/ double oSupportOffset);

  /**
  * Returns the Plate Perimeter of the structural object.
  *<dl>
  *<dt>Example</dt>:
  *<dd>
  * This example retrieves the Plate Perimeter of the structural object.
  * <pre>
  * Dim PlatePerimeter As Double
  * PlatePerimeter = SfmObjectExt.<font color="red">PlatePerimeter</font>
  * </pre>
  * </dl> 
  */
  #pragma PROPERTY PlatePerimeter
  HRESULT get_PlatePerimeter(out /*IDLRETVAL*/ double oPlatePerimeter);
  
  /**
  * Returns the Plate Width of the structural object.
  *<dl>
  *<dt>Example</dt>:
  *<dd>
  * This example retrieves the Plate Width of the structural object.
  * <pre>
  * Dim PlateWidth As Double
  * PlateWidth = SfmObjectExt.<font color="red">PlateWidth</font>
  * </pre>
  * </dl> 
  */
  #pragma PROPERTY PlateWidth
  HRESULT get_PlateWidth(out /*IDLRETVAL*/ double oPlateWidth);
  
  /**
  * Returns the Surface Area of the structural object.
  *<dl>
  *<dt>Example</dt>:
  *<dd>
  * This example retrieves the Surface Area of the structural object.
  * <pre>
  * Dim SurfaceArea As Double
  * SurfaceArea = SfmObjectExt.<font color="red">SurfaceArea</font>
  * </pre>
  * </dl> 
  */
  #pragma PROPERTY SurfaceArea
  HRESULT get_SurfaceArea(out /*IDLRETVAL*/ double oSurfaceArea);

  /**
  * Returns the Thickness of the structural object.
  *<dl>
  *<dt>Example</dt>:
  *<dd>
  * This example retrieves the Thickness of the structural object.
  * <pre>
  * Dim Thickness As Double
  * Thickness = SfmObjectExt.<font color="red">Thickness</font>
  * </pre>
  * </dl> 
  */
  #pragma PROPERTY Thickness
  HRESULT get_Thickness(out /*IDLRETVAL*/ double oThickness);

  /**
  * Returns the Offset of the structural object.
  *<dl>
  *<dt>Example</dt>:
  *<dd>
  * This example retrieves the Offset of the structural object.
  * <pre>
  * Dim Offset As Double
  * Offset = SfmObjectExt.<font color="red">Offset</font>
  * </pre>
  * </dl> 
  */
  #pragma PROPERTY Offset
  HRESULT get_Offset(out /*IDLRETVAL*/ double oOffset);

  /**
  * Returns the SectionName of the structural object.
  *<dl>
  *<dt>Example</dt>:
  *<dd>
  * This example retrieves the SectionName of the structural object.
  * <pre>
  * Dim SectionName As String
  * SectionName = SfmObjectExt.<font color="red">SectionName</font>
  * </pre>
  * </dl> 
  */
  #pragma PROPERTY SectionName
  HRESULT get_SectionName(inout /*IDLRETVAL*/ CATBSTR oSectionName);

  /**
  * Returns the TopZ of the structural object.
  *<dl>
  *<dt>Example</dt>:
  *<dd>
  * This example retrieves the TopZ of the structural object.
  * <pre>
  * Dim TopZ As Double
  * TopZ = SfmObjectExt.<font color="red">TopZ</font>
  * </pre>
  * </dl> 
  */
  #pragma PROPERTY TopZ
  HRESULT get_TopZ(out /*IDLRETVAL*/ double oTopZ);

  /**
  * Returns the BottomZ of the structural object.
  *<dl>
  *<dt>Example</dt>:
  *<dd>
  * This example retrieves the BottomZ of the structural object.
  * <pre>
  * Dim BottomZ As Double
  * BottomZ = SfmObjectExt.<font color="red">BottomZ</font>
  * </pre>
  * </dl> 
  */
  #pragma PROPERTY BottomZ
  HRESULT get_BottomZ(out /*IDLRETVAL*/ double oBottomZ);

  /**
  * Returns the Start EndCut name of the structural object.
  *<dl>
  *<dt>Example</dt>:
  *<dd>
  * This example retrieves the Start EndCut name of the structural object.
  * <pre>
  * Dim StartEndCutName As String
  * StartEndCutName = SfmObjectExt.<font color="red">StartEndCutName</font>
  * </pre>
  * </dl> 
  */
  #pragma PROPERTY StartEndCutName
  HRESULT get_StartEndCutName(inout /*IDLRETVAL*/ CATBSTR oStartEndCutName);

  /**
  * Returns the End EndCut name of the structural object.
  *<dl>
  *<dt>Example</dt>:
  *<dd>
  * This example retrieves the End EndCut name of the structural object.
  * <pre>
  * Dim EndEndCutName As String
  * EndEndCutName = SfmObjectExt.<font color="red">EndEndCutName</font>
  * </pre>
  * </dl> 
  */
  #pragma PROPERTY EndEndCutName
  HRESULT get_EndEndCutName(inout /*IDLRETVAL*/ CATBSTR oEndEndCutName);

  /**
  * Returns the Plate Length of the structural object.
  *<dl>
  *<dt>Example</dt>:
  *<dd>
  * This example retrieves the Plate Length of the structural object.
  * <pre>
  * Dim PlateLength As Double
  * PlateLength = SfmObjectExt.<font color="red">PlateLength</font>
  * </pre>
  * </dl> 
  */
  #pragma PROPERTY PlateLength
  HRESULT get_PlateLength(out /*IDLRETVAL*/ double oLength);

  /**
  * Returns the name of the supported plate for the structural object.
  *<dl>
  *<dt>Example</dt>:
  *<dd>
  * This example retrieves the name of the supported plate for the structural object.
  * <pre>
  * Dim SupportedPlateName As String
  * SupportedPlateName = SfmObjectExt.<font color="red">SupportedPlateName</font>
  * </pre>
  * </dl> 
  */
  #pragma PROPERTY SupportedPlateName
  HRESULT get_SupportedPlateName(inout /*IDLRETVAL*/ CATBSTR oSupportedPlateName);
};

// Interface name : CATIASfmObjectExt
#pragma ID CATIASfmObjectExt "DCE:132AF51F-57A5-49F1-9665FAD1B43BE878"
#pragma DUAL CATIASfmObjectExt

// VB object name : SfmObjectExt (Id used in Visual Basic)
#pragma ID SfmObjectExt "DCE:04C6F829-AA6F-4D7A-9E9D41D37B325656"
#pragma ALIAS CATIASfmObjectExt SfmObjectExt

#endif
