// COPYRIGHT Dassault Systemes 2007
//=============================================================================
// CATIASfmMemberCurve
//   Define the CATIASfmMemberCurve interface for managing SfmMemberCurve
//   attributes
//=============================================================================
// Usage notes:
//   Interface for automation integration
//=============================================================================
// Mar. 2007  Creation                                                S. QUINCI
//=============================================================================
#ifndef CATIASfmMemberCurve_IDL
#define CATIASfmMemberCurve_IDL
/*IDLREP*/

/**
* @CAA2Level L1
* @CAA2Usage U3
*/

#include "CATIASfmMember.idl"
#include "CATIAReference.idl"

/**
 * Interface to manage Member created with one curve and a reference surface.
 * <b>Role</b>: To manage member created with one curve and a reference surface.
 */
interface CATIASfmMemberCurve : CATIASfmMember
{
  /**
  * Returns or sets the curve.
  * <br>Sub-element(s) supported (see @href CATIABoundary object): 
  * @href CATIAEdge.
  *<dl>
  *<dt>Example</dt>:
  *<dd>
  * This example retrieves in <code>Curve</code> the supporting curve for the <code>SfmMemberCurve</code> feature.
  * <pre>
  * Dim Curve As Reference
  * Set Curve = SfmMemberCurve.<font color="red">Curve</font>
  * </pre>
  * </dl> 
  */ 
  #pragma PROPERTY Curve
  HRESULT get_Curve(out /*IDLRETVAL*/ CATIAReference oCurve);
  HRESULT put_Curve(in CATIAReference iCurve);

  /**
  * Returns or sets the reference surface.
  *<dl>
  *<dt>Example</dt>:
  *<dd>
  * This example retrieves in <code>ReferenceSurface</code> the reference surface for the <code>SfmMemberCurve</code> feature.
  * <pre>
  * Dim ReferenceSurface As Reference
  * Set ReferenceSurface = SfmMemberCurve.<font color="red">ReferenceSurface</font>
  * </pre>
  * </dl> 
  */ 
  #pragma PROPERTY ReferenceSurface
  HRESULT get_ReferenceSurface(out /*IDLRETVAL*/ CATIAReference oReferenceSurface);
  HRESULT put_ReferenceSurface(in CATIAReference iReferenceSurface);

  /**
  * Returns or sets the reference surface orientation.
  *<dl>
  *<dt>Example</dt>:
  *<dd>
  * This example retrieves in <code>ReferenceSurfaceOrient</code> the reference surface orientation for <code>SfmMemberCurve</code> feature.
  * <pre>
  * Dim ReferenceSurfaceOrient As Integer
  * Set ReferenceSurfaceOrient = SfmMemberCurve.<font color="red">ReferenceSurfaceOrientation</font>
  * </pre>
  * </dl> 
  */ 
  #pragma PROPERTY ReferenceSurfaceOrientation
  HRESULT get_ReferenceSurfaceOrientation(out /*IDLRETVAL*/ long oOrientation);
  HRESULT put_ReferenceSurfaceOrientation(in long iOrientation);

  /**
  * Inverts the reference surface orientation.
  *<dl>
  *<dt>Example</dt>:
  *<dd>
  * This example inverts the orientation of the reference surface for the <code>SfmMemberCurve</code> feature.
  * <pre>
  * SfmMemberCurve.<font color="red">InvertReferenceSurface</font>
  * </pre>
  * </dl> 
  */ 
  HRESULT InvertReferenceSurface();
};

// Interface name : CATIASfmMemberCurve
#pragma ID CATIASfmMemberCurve "DCE:eaceea07-55ab-4b97-84e7b39278db80b7"
#pragma DUAL CATIASfmMemberCurve

// VB object name : SfmMemberCurve (Id used in Visual Basic)
#pragma ID SfmMemberCurve "DCE:c2db58bc-3104-4362-962d5d64954c5c43"
#pragma ALIAS CATIASfmMemberCurve SfmMemberCurve

#endif
