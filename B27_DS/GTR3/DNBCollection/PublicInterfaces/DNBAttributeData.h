//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 */

//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     joy         02/01/99    Initial implementation.
//*     bkh         10/27/03    Changed documentation style.
//* 


#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _DNB_ATTRIBUTEDATA_H_
#define _DNB_ATTRIBUTEDATA_H_

#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>


#include <DNBCollection.h>


#include <DNBException.h>
#include <DNBAttributeBase.h> 

/**
  * The class to contain data of arbitrary type. 
  * 
  * <br><B>Template Parameter(s)</B><br>
  * @param  class Data 
  * The class of arbitrary data type. It must provide the following
  * public methods:
  * \begin{ itemize }
  * <LI> Data() }
  * <LI> Data( const Data& right ) }
  * <LI> ~Data()  }
  * <LI> Data operator=( const Data& right ) }
  * <LI> bool operator==( const Data& right ) }
  * \end{ itemize }
  * 
  * <br><B>Description</B><br>
  * This class holds data of arbitrary type. Apart from standard    
  * query functions the class provides a method to make a copy
  * of itself which can be invoked from its abstract base class.
  * 
  */
template<class Data>
class DNBAttributeData : public DNBAttributeBase
{
public:

/**
  * Default constructor. 
  * 
  * @return
  * Nothing.
  * 
  * <br><B>Description</B><br>
  * This function constructs an attribute object with default value. 
  * @exception scl_bad_alloc
  * The attribute object could not be created because of 
  * insufficient memory.
  * @exception DNBException
  * The attribute object could not be created because of an error 
  * during initialization of the attribute data. 
  * 
  */
    DNBAttributeData( )
        DNB_THROW_SPEC(( scl_bad_alloc, DNBException ));

/**
  * Constructor.
  * @param  data 
  * Data value.
  * @param  isModifiable 
  * Data modifiable?
  * 
  * @return
  * Nothing.
  * 
  * <br><B>Description</B><br>
  * This function constructs an object containing data of arbitrary  
  * type and an attribute indicating whether it is modifiable.    
  * @exception scl_bad_alloc
  * The attribute object could not be created because of 
  * insufficient memory.
  * @exception DNBException
  * The attribute object could not be created because of an error 
  * during initialization of the attribute data. 
  * 
  */
    DNBAttributeData( const Data& data, bool isModifiable = true )    
        DNB_THROW_SPEC(( scl_bad_alloc, DNBException ));

/**
  * Copy constructor.
  * @param  right 
  * The object to be copied.
  * 
  * @return
  * Nothing.
  * 
  * <br><B>Description</B><br>
  * This function creates a copy of DNBAttributeData from <tt>right</tt>.
  * @exception scl_bad_alloc
  * The attribute object could not be created because of 
  * insufficient memory.
  * @exception DNBException
  * The attribute object could not be created because of an error 
  * during initialization of the attribute data. 
  */
    DNBAttributeData( const DNBAttributeData<Data>& right )    
        DNB_THROW_SPEC(( scl_bad_alloc, DNBException ));

/**
  * Conversion constructor.
  * @param  right 
  * Reference to object to be converted.
  * 
  * @return
  * Nothing.
  * 
  * <br><B>Description</B><br>
  * This function creates a copy of DNBAttributeData from <tt>right</tt>.
  * @exception scl_bad_alloc
  * The attribute object could not be created because of
  * insufficient memory.
  * @exception DNBException
  * The attribute object could not be created because of an error 
  * during initialization of the attribute data. 
  * @exception DNBEInvalidCast
  * The attribute object could not be created because <tt>right</tt> is 
  * not of type <tt>self</tt>.
  */
    DNBAttributeData(  const DNBAttributeBase& right ) 
        DNB_THROW_SPEC(( scl_bad_alloc, DNBException, DNBEInvalidCast ));

/**
  * Destructor.
  * 
  * @return
  * Nothing.
  * 
  * <br><B>Description</B><br>
  * This function destroys <tt>self</tt>. 
  */
    virtual
    ~DNBAttributeData( )
        DNB_THROW_SPEC_NULL;

/**
  * Creates a copy of <tt>self</tt>.
  * 
  * @return
  * Clone of <tt>self</tt>.
  * 
  * <br><B>Description</B><br>
  * This function clones and returns a copy of <tt>self</tt>.
  * @exception scl_bad_alloc
  * The data element could not be cloned because of 
  * insufficient free memory.
  * @exception DNBException
  * The attribute object could not be created because of an error 
  * during initialization of the attribute data. 
  */
    DNBAttributeBase* 
    clone() const 
        DNB_THROW_SPEC((scl_bad_alloc, DNBException));

/**
  * Sets data in <tt>self</tt>.
  * @param  data 
  * The data value to be set.
  * 
  * @return
  * 
  * <br><B>Description</B><br>
  * This function sets the data value in <tt>self</tt>.
  */
    inline  void 
    setData( const Data& data ) 
        DNB_THROW_SPEC_NULL;

/**
  * Gets data from <tt>self</tt>.
  * 
  * @return
  * Data value in <tt>self</tt>. 
  * 
  * <br><B>Description</B><br>
  * This function gets the data contained in <tt>self</tt>.
  */
    inline const Data&
    getData( ) const  
        DNB_THROW_SPEC_NULL;

/**
  * Determines whether <tt>self</tt> is of type <tt>right</tt>.
  * @param  right 
  * A candidate object which might be of type <tt>self</tt>.
  * 
  * @return
  * Boolean true or false. 
  * 
  * <br><B>Description</B><br>
  * This function returns a boolean true if <tt>self</tt> is of 
  * type <tt>right</tt>. Otherwise a boolean false is returned. 
  */
    inline bool
    isInstanceOf( const DNBAttributeBase& right ) const
        DNB_THROW_SPEC_NULL;

/**
  * Assignment operator.
  * @param  right 
  * The object to be copied.
  * 
  * @return
  * Object of type <tt>self</tt>.
  * 
  * <br><B>Description</B><br>
  * This function copies <tt>right</tt> into <tt>self</tt>.
  * @exception DNBEInvalidCast
  * The object <tt>right</tt> is not of type <tt>self</tt>. 
  */
    DNBAttributeData<Data>& 
    operator=( const DNBAttributeBase& right ) 
        DNB_THROW_SPEC((DNBEInvalidCast));

/**
  * Assignment operator.
  * @param  right 
  * The object to be copied.
  * 
  * @return
  * Object of type <tt>self</tt>.
  * 
  * <br><B>Description</B><br>
  * This function copies <tt>right</tt> into <tt>self</tt>.
  */
    DNBAttributeData<Data>& 
    operator=( const DNBAttributeData<Data>& right ) 
        DNB_THROW_SPEC_NULL;

/**
  * Equality operator.
  * @param  right 
  * The object to be compared against.
  * 
  * @return
  * Boolean true or false.
  * 
  * <br><B>Description</B><br>
  * This function compares <tt>right</tt> against <tt>self</tt> and
  * returns a true if equal, else a false.
  */
    inline bool 
    operator==( const DNBAttributeBase& right ) const 
  	DNB_THROW_SPEC_NULL;


protected: 
    virtual bool 
    isEqual( const DNBAttributeBase& right ) const
	DNB_THROW_SPEC_NULL;

private:
    Data		    data_;
    const DNBAttributeData<Data>* getDerived( const DNBAttributeBase& right ) const;

};


//
//  Public definition file.
//

#include <DNBAttributeData.cc>

#endif		/* _DNB_ATTRIBUTEDATA_H_ */
