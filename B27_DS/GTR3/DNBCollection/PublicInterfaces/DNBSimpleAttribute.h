//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 */

//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     xin         02/09/00    Initial implementation.
//*     xin         03/31/00
//*     xin         05/20/00    mordify to union
//*     xin         07/09/00    mordify interface
//*     sha         11/21/02    move inline methods in a .cc file
//*     bkh         07/25/03    added documentation.
//* 
//* 
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_SIMPLEATTRIBUTE_H_
#define _DNB_SIMPLEATTRIBUTE_H_


#include <DNBSystemBase.h>
#include <scl_vector.h>
#include <DNBSystemDefs.h>


#include <DNBCollection.h>
#include <DNBAttributeBase.h>
#include <DNBVector3D.h>
#include <DNBEuler3D.h>
#include <DNBException.h>


#include <DNBMathVector.h>


//still keep this because of the bug of confusion between string and bool
#if DNB_USE_UNICODE

#define STR_TEXT( str ) ( (scl_wstring) L ## str )
#else

#define STR_TEXT( str ) ( (scl_string) str )
#endif



/**
 *  The class to contain pair data of simple type.
 *   <p>This class holds data of simple type. Apart from standard
 *      query functions the class provides a method to make a copy
 *      of itself which can be invoked from its abstract base class.
 *      It must provide the following public methods:</p>
 *    <pre>
 *      <ul><li>src Data()</li>
 *      <li>src Data( const Data& right )</li>
 *      <li>src ~Data()</li>
 *      <li>src Data operator=( const Data& right )</li>
 *      <li>src bool operator==( const Data& right )</li></ul>
 *      </pre>
 *   
 */

class ExportedByDNBCollection DNBSimpleAttribute: public DNBAttributeBase
{

public:

    typedef scl_vector<scl_wstring, DNB_ALLOCATOR(scl_wstring) >            wStringVector;
    typedef scl_vector<scl_string, DNB_ALLOCATOR(scl_string) >              stringVector;
    typedef scl_vector<DNBInteger32, DNB_ALLOCATOR(DNBInteger32) >  DNBInt32Vector;
    typedef scl_vector<DNBInteger64, DNB_ALLOCATOR(DNBInteger64) >  DNBInt64Vector;
    typedef scl_vector<bool, DNB_ALLOCATOR(bool) >                  DNBBoolVector;
    typedef scl_vector<DNBByte, DNB_ALLOCATOR(DNBByte) >            ByteVector;

    enum AttributeType
    {
        UnknownType=0,
        BooleanType,
        BooleanVectorType,
        ByteType,
        ByteVectorType,
        StringType,
        WStringType,
        StringVectorType,
        WStringVectorType,
        RealType,
        RealVectorType,
        Integer32Type,
        Integer32VectorType,
        Integer64Type,
        Integer64VectorType,
        Vector3DType,
        Euler3DType
    } ;

    AttributeType getAttributeType() const
        DNB_THROW_SPEC_NULL;

    /**
     *  This function constructs a simple attribute object with default value.
     *  @exception scl_bad_alloc
     *       The attribute object could not be created because of insufficient memory.
     *  @exception DNBException 
     *       The attribute object could not be created because of an error
     *          during initialization of the attribute data.
     */


    DNBSimpleAttribute( )
        DNB_THROW_SPEC(( scl_bad_alloc, DNBException ));

    /**
     *  This function constructs an object containing data of simple
     *     type and an attribute indicating whether it is modifiable.
     *   @param value 
     *        Data value.
     *   @param isModifiable 
     *        Data modifiable
     *   @exception scl_bad_alloc
     *        The attribute object could not be created because of
     *        insufficient memory.
     *   @exception DNBException
     *        The attribute object could not be created because of an error
     *        during initialization of the attribute data.
     *
     */
    DNBSimpleAttribute( DNBReal value, bool isModifiable = true )
        DNB_THROW_SPEC(( scl_bad_alloc, DNBException ));

    /**
     *  This function constructs an object containing data of DNBByte
     *     type and an attribute indicating whether it is modifiable.
     *  @param value 
     *        Data value.
     *  @param isModifiable 
     *        Data modifiable
     *  @exception scl_bad_alloc
     *       The attribute object could not be created because of
     *       insufficient memory.
     *  @exception DNBException
     *       The attribute object could not be created because of an error
     *        during initialization of the attribute data.
     *
     */

    DNBSimpleAttribute(  DNBByte value , bool isModifiable = true )
        DNB_THROW_SPEC(( scl_bad_alloc, DNBException ));

#if DNB_HAS_BOOL
    DNBSimpleAttribute( bool value, bool isModifiable = true )
        DNB_THROW_SPEC(( scl_bad_alloc, DNBException ));
#endif

    /**
     *  This function constructs an object containing data of DNBInteger
     *     type and an attribute indicating whether it is modifiable.
     *  @param value 
     *       Integer value.
     *  @param isModifiable 
     *        Data modifiable
     *  @exception scl_bad_alloc
     *        The attribute object could not be created because of
     *        insufficient memory.
     *  @exception DNBException
     *        The attribute object could not be created because of an error
     *        during initialization of the attribute data.
     *
     */

    DNBSimpleAttribute( DNBInteger32 value, bool isModifiable = true )
        DNB_THROW_SPEC(( scl_bad_alloc, DNBException ));

    /**
     *  This function constructs an object containing data of type DNBInteger
     *   and an attribute indicating whether it is modifiable.
     *  @param value 
     *        Integer value.
     *  @param isModifiable 
     *        Data modifiable
     *  @exception scl_bad_alloc
     *        The attribute object could not be created because of
     *         insufficient memory.
     *  @exception DNBException
     *        The attribute object could not be created because of an error
     *        during initialization of the attribute data.
     *
     */

    DNBSimpleAttribute( DNBInteger64 value, bool isModifiable = true )
        DNB_THROW_SPEC(( scl_bad_alloc, DNBException ));

    /**
     *  This function constructs an object containing data of type DNBMathVector
     *     and an attribute indicating whether it is modifiable.
     *  @param value 
     *        Math value.
     *  @param isModifiable 
     *        Data modifiable
     *  @exception scl_bad_alloc
     *        The attribute object could not be created because of
     *        insufficient memory.
     *  @exception DNBException
     *        The attribute object could not be created because of an error
     *       during initialization of the attribute data.
     *
     */

    DNBSimpleAttribute( const DNBMathVector& value, bool isModifiable = true )
        DNB_THROW_SPEC(( scl_bad_alloc, DNBException ));

    /**
     *  This function constructs an object containing data of type wStringVector
     *     and an attribute indicating whether it is modifiable.
     *  @param value 
     *        wStringVector value.
     *  @param isModifiable 
     *        Data modifiable
     *  @exception scl_bad_alloc
     *       The attribute object could not be created because of
     *       insufficient memory.
     *  @exception DNBException
     *       The attribute object could not be created because of an error
     *       during initialization of the attribute data.
     *
     */

    DNBSimpleAttribute( const wStringVector& value, bool isModifiable = true )
        DNB_THROW_SPEC(( scl_bad_alloc, DNBException ));

    /**
     *  This function constructs an object containing data of type StringVector
     *     and an attribute indicating whether it is modifiable.
     *  @param value 
     *        StringVector.
     *  @param isModifiable 
     *        Data modifiable
     *  @exception scl_bad_alloc
     *        The attribute object could not be created because of
     *         insufficient memory.
     *  @exception DNBException
     *        The attribute object could not be created because of an error
     *        during initialization of the attribute data.
     *
     */

    DNBSimpleAttribute( const stringVector& value, bool isModifiable = true )
        DNB_THROW_SPEC(( scl_bad_alloc, DNBException ));

    /**
     *  This function constructs an object containing data of type DNBInt32Vector
     *     and an attribute indicating whether it is modifiable.
     *  @param value 
     *        Integer value.
     *  @param isModifiable 
     *        Data modifiable
     *  @exception scl_bad_alloc
     *      The attribute object could not be created because of
     *       insufficient memory.
     *  @exception DNBException
     *      The attribute object could not be created because of an error
     *         during initialization of the attribute data.
     *
     */

    DNBSimpleAttribute( const DNBInt32Vector& value, bool isModifiable = true )
        DNB_THROW_SPEC(( scl_bad_alloc, DNBException ));

    /**
     *  This function constructs an object containing data of type DNBInt64Vector
     *     and an attribute indicating whether it is modifiable.
     *  @param value 
     *        Integer value.
     *  @param isModifiable 
     *        Data modifiable
     *  @exception scl_bad_alloc
     *      The attribute object could not be created because of
     *        insufficient memory.
     *  @exception DNBException
     *      The attribute object could not be created because of an error
     *        during initialization of the attribute data.
     *
     */

    DNBSimpleAttribute( const DNBInt64Vector& value, bool isModifiable = true )
        DNB_THROW_SPEC(( scl_bad_alloc, DNBException ));

    /**
     *  This function constructs an object containing data of type scl_wstring
     *     and an attribute indicating whether it is modifiable.
     *  @param value 
     *        scl_wstring value.
     *  @param isModifiable 
     *        Data modifiable
     *  @exception scl_bad_alloc
     *      The attribute object could not be created because of
     *       insufficient memory.
     *  @exception DNBException
     *      The attribute object could not be created because of an error
     *       during initialization of the attribute data.
     *
     */

    DNBSimpleAttribute( const scl_wstring& value, bool isModifiable = true )
        DNB_THROW_SPEC(( scl_bad_alloc, DNBException ));

    /**
     *  This function constructs an object containing data of type string
     *     and an attribute indicating whether it is modifiable.
     *  @param value 
     *        Data value.
     *  @param isModifiable 
     *        Data modifiable
     *  @exception scl_bad_alloc
     *      The attribute object could not be created because of
     *       insufficient memory.
     *  @exception DNBException
     *      The attribute object could not be created because of an error
     *       during initialization of the attribute data.
     *
     */

    DNBSimpleAttribute( const scl_string& value, bool isModifiable = true )
        DNB_THROW_SPEC(( scl_bad_alloc, DNBException ));

#if DNB_HAS_BOOL
    DNBSimpleAttribute( const DNBBoolVector& value, bool isModifiable = true )
        DNB_THROW_SPEC(( scl_bad_alloc, DNBException ));
#endif

    /**
     *  This function constructs an object containing data of type ByteVector
     *     and an attribute indicating whether it is modifiable.
     *  @param value 
     *        Bytevector.
     *  @param isModifiable 
     *        Data modifiable
     *  @exception scl_bad_alloc
     *       The attribute object could not be created because of
     *        insufficient memory.
     *  @exception DNBException
     *       The attribute object could not be created because of an error
     *       during initialization of the attribute data.
     *
     */

    DNBSimpleAttribute( const ByteVector& value, bool isModifiable = true )
        DNB_THROW_SPEC(( scl_bad_alloc, DNBException ));

    /**
     *  This function constructs an object containing data of type DNBVector3D
     *     and an attribute indicating whether it is modifiable.
     *  @param value 
     *        Vector3D value.
     *  @param isModifiable 
     *        Data modifiable
     *  @exception scl_bad_alloc
     *       The attribute object could not be created because of
     *       insufficient memory.
     *  @exception DNBException
     *        The attribute object could not be created because of an error
     *         during initialization of the attribute data.
     *
     */

    DNBSimpleAttribute( const DNBVector3D& value, bool isModifiable = true )
        DNB_THROW_SPEC(( scl_bad_alloc, DNBException ));

    /**
     *  This function constructs an object containing data of type DNBEuler3D
     *     and an attribute indicating whether it is modifiable.
     *  @param value 
     *        Euler3D value.
     *  @param isModifiable 
     *        Data modifiable
     *  @exception scl_bad_alloc
     *        The attribute object could not be created because of
     *        insufficient memory.
     *  @exception DNBException
     *        The attribute object could not be created because of an error
     *        during initialization of the attribute data.
     *
     */

    DNBSimpleAttribute( const DNBEuler3D& value, bool isModifiable = true )
        DNB_THROW_SPEC(( scl_bad_alloc, DNBException ));

    /**
     *   This function creates a copy of DNBSimpleAttribute from right Copy constructor.
     *  @param right 
     *       The object to be copied.
     *  @exception scl_bad_alloc
     *       The attribute object could not be created because of insufficient memory.
     *  @exception DNBException
     *      The attribute object could not be created because of an error
     *       during initialization of the attribute data.
     *
     */

    DNBSimpleAttribute( const DNBSimpleAttribute& right )
        DNB_THROW_SPEC(( scl_bad_alloc, DNBException ));

    /**
     * This function creates a copy of DNBSimpleAttribute from right.
     *  @param right 
     *      Reference to object to be converted.
     *  @exception scl_bad_alloc
     *      The attribute object could not be created because of insufficient memory.
     *  @exception DNBException
     *      The attribute object could not be created because of an error
     *      during initialization of the attribute data.
     *  @exception DNBEInvalidCast
     *      The attribute object could not be created because right is
     *      not of type self.
     *
     */

    DNBSimpleAttribute(  const DNBAttributeBase& right )
        DNB_THROW_SPEC(( scl_bad_alloc, DNBException, DNBEInvalidCast ));

    /**
     * This function destroys self.
     */
    virtual    ~DNBSimpleAttribute( )
        DNB_THROW_SPEC_NULL;

    /**
     * This function clones and returns a copy of self.
     *  @returns
     *     Clone of self.
     *  @exception scl_bad_alloc
     *      The data element could not be cloned because of insufficient free memory.
     *  @exception DNBException 
     *         The attribute object could not be created because of an error
     *         during initialization of the attribute data.
     */

    DNBAttributeBase*
    clone() const
        DNB_THROW_SPEC((scl_bad_alloc, DNBException));



    /**
     *  Determines whether self is of type right.
     *  @param right 
     *         A candidate object which might be of type self.
     *  @return
     *     Boolean true or false.
     *     This function returns a boolean true if self is of
     *     type right. Otherwise a boolean false is returned.
     */

    inline bool
    isInstanceOf( const DNBAttributeBase& right ) const
        DNB_THROW_SPEC_NULL;


    /**
     *  Conversion function to convert variable of type DNBSimpleAttribute to DNBReal.
     */
    operator DNBReal () const
        DNB_THROW_SPEC((DNBEInvalidCast));
     //  The above does not work on aix: this is the work-around/alernative method
    /**
     *  Conversion function to convert variable of type DNBSimpleAttribute to DNBReal.
     */
    DNBReal
    toDNBReal() const
        DNB_THROW_SPEC((DNBEInvalidCast));



    /**
     *  Conversion function to convert variable of type DNBSimpleAttribute to DNBByte.
     */
    operator DNBByte () const
        DNB_THROW_SPEC((DNBEInvalidCast));
    //  The above does not work on aix: this is the work-around/alernative method
    /**
     *  Conversion function to convert variable of type DNBSimpleAttribute to DNBByte.
     */
    DNBByte
    toDNBByte() const
         DNB_THROW_SPEC((DNBEInvalidCast));



#if DNB_HAS_BOOL
    operator bool () const
        DNB_THROW_SPEC((DNBEInvalidCast));

#endif


    /**
     *  Conversion function to convert variable of type DNBSimpleAttribute to DNBMathVector.
     */
    operator DNBMathVector () 
        DNB_THROW_SPEC((DNBEInvalidCast));
    //  The above does not work on aix: this is the work-around/alernative method
    /**
     *  Conversion function to convert variable of type DNBSimpleAttribute to DNBMathVector.
     */
    DNBMathVector
    toDNBMathVector() const
        DNB_THROW_SPEC((DNBEInvalidCast));


    /**
     *  Conversion function to convert variable of type DNBSimpleAttribute to wStringVector.
     */
    operator wStringVector () const
        DNB_THROW_SPEC((DNBEInvalidCast));
     //  The above does not work on aix: this is the work-around/alernative method
    /**
     *  Conversion function to convert variable of type DNBSimpleAttribute to wStringVector.
     */
     wStringVector
     towStringVector() const
        DNB_THROW_SPEC((DNBEInvalidCast));
 

    /**
     *  Conversion function to convert variable of type DNBSimpleAttribute to StringVector.
     */
    operator stringVector () const
        DNB_THROW_SPEC((DNBEInvalidCast));
     //  The above does not work on aix: this is the work-around/alernative method
    /**
     *  Conversion function to convert variable of type DNBSimpleAttribute to StringVector.
     */
    stringVector
    tostringVector() const
        DNB_THROW_SPEC((DNBEInvalidCast));
  

    /**
     *  Conversion function to convert variable of type DNBSimpleAttribute to DNBVector3D.
     */
    operator DNBVector3D () const
        DNB_THROW_SPEC((DNBEInvalidCast));
     //  The above does not work on aix: this is the work-around/alernative method
    /**
     *  Conversion function to convert variable of type DNBSimpleAttribute to DNBVector3D.
     */
    DNBVector3D
    toDNBVector3D() const
        DNB_THROW_SPEC((DNBEInvalidCast));
 

    /**
     *  Conversion function to convert variable of type DNBSimpleAttribute to DNBEuler3D.
     */
    operator DNBEuler3D () const
        DNB_THROW_SPEC((DNBEInvalidCast));
     //  The above does not work on aix: this is the work-around/alernative method
    /**
     *  Conversion function to convert variable of type DNBSimpleAttribute to DNBEuler3D.
     */
    DNBEuler3D
    toDNBEuler3D() const
        DNB_THROW_SPEC((DNBEInvalidCast));


    /**
     *  Conversion function to convert variable of type DNBSimpleAttribute to wString.
     */
    operator scl_wstring () const
        DNB_THROW_SPEC((DNBEInvalidCast));
     //  The above does not work on aix: this is the work-around/alernative method
    /**
     *  Conversion function to convert variable of type DNBSimpleAttribute to wString.
     */
     scl_wstring
    towstring() const
        DNB_THROW_SPEC((DNBEInvalidCast));



    /**
     *  Conversion function to convert variable of type DNBSimpleAttribute to String.
     */
    operator scl_string () const
        DNB_THROW_SPEC((DNBEInvalidCast));
     //  The above does not work on aix: this is the work-around/alernative method
    /**
     *  Conversion function to convert variable of type DNBSimpleAttribute to String.
     */
    scl_string
    tostring() const
        DNB_THROW_SPEC((DNBEInvalidCast));


#if DNB_HAS_BOOL
    operator DNBBoolVector () const
        DNB_THROW_SPEC((DNBEInvalidCast));
     //  The above does not work on aix: this is the work-around/alernative method
    DNBBoolVector
    toDNBBoolVector() const
        DNB_THROW_SPEC((DNBEInvalidCast));


#endif

    operator ByteVector () const
        DNB_THROW_SPEC((DNBEInvalidCast));
    //  The above does not work on aix: this is the work-around/alernative method
    ByteVector
    toByteVector() const
        DNB_THROW_SPEC((DNBEInvalidCast));


    operator DNBInteger32 () const
        DNB_THROW_SPEC((DNBEInvalidCast));
    //  The above does not work on aix: this is the work-around/alernative method
    DNBInteger32
    toDNBInteger32() const
        DNB_THROW_SPEC((DNBEInvalidCast));


    operator DNBInteger64 () const
        DNB_THROW_SPEC((DNBEInvalidCast));
     //  The above does not work on aix: this is the work-around/alernative method
    DNBInteger64
    toDNBInteger64() const
        DNB_THROW_SPEC((DNBEInvalidCast));


    operator DNBInt32Vector () const
        DNB_THROW_SPEC((DNBEInvalidCast));
     //  The above does not work on aix: this is the work-around/alernative method
    DNBInt32Vector
    toDNBInt32Vector() const
        DNB_THROW_SPEC((DNBEInvalidCast));


    operator DNBInt64Vector () const
        DNB_THROW_SPEC((DNBEInvalidCast));
     //  The above does not work on aix: this is the work-around/alernative method
    DNBInt64Vector
    toDNBInt64Vector() const
        DNB_THROW_SPEC((DNBEInvalidCast));



    /**
     * This function copies right into self.
     * @param right 
     *          The object to be copied.
     * @exception
     *     @param DNBEInvalidCast
     *     The object right is not of type self.
     */ 
    DNBSimpleAttribute&
    operator=( const DNBAttributeBase& right )
        DNB_THROW_SPEC((DNBEInvalidCast));

    /**
     *  This function copies right into self.
     *   @param right 
     *        The object to be copied.
     *   @retuns
     *        Object of type self.
     */
    DNBSimpleAttribute&
    operator=( const DNBSimpleAttribute& right )
        DNB_THROW_SPEC((DNBEInvalidCast));

    /**
     *  Conversion function to convert variable of type DNBReal to DNBSimpleAttribute.
     */
    DNBSimpleAttribute& operator =(DNBReal)
        DNB_THROW_SPEC((DNBEInvalidCast));

#if DNB_HAS_BOOL
    DNBSimpleAttribute& operator =(bool)
        DNB_THROW_SPEC((DNBEInvalidCast));
#endif

    /**
     *  Conversion function to convert variable of type DNBByte to DNBSimpleAttribute.
     */
    DNBSimpleAttribute& operator =(DNBByte)
        DNB_THROW_SPEC((DNBEInvalidCast));

    /**
     *  Conversion function to convert variable of type DNBMathVector to DNBSimpleAttribute.
     */
    DNBSimpleAttribute& operator =(const DNBMathVector&)
        DNB_THROW_SPEC((DNBEInvalidCast));

    /**
     *  Conversion function to convert variable of type wStringVector to DNBSimpleAttribute.
     */
    DNBSimpleAttribute& operator =(const wStringVector&)
        DNB_THROW_SPEC((DNBEInvalidCast));

    /**
     *  Conversion function to convert variable of type stringVector to DNBSimpleAttribute.
     */
    DNBSimpleAttribute& operator =(const stringVector&)
        DNB_THROW_SPEC((DNBEInvalidCast));

    /**
     *  Conversion function to convert variable of type DNBEuler3D to DNBSimpleAttribute.
     */
    DNBSimpleAttribute& operator =(const DNBEuler3D&)
        DNB_THROW_SPEC((DNBEInvalidCast));

    /**
     *  Conversion function to convert variable of type DNBVector3D to DNBSimpleAttribute.
     */
    DNBSimpleAttribute& operator =(const DNBVector3D&)
        DNB_THROW_SPEC((DNBEInvalidCast));

    /**
     *  Conversion function to convert variable of type scl_wstring to DNBSimpleAttribute.
     */
    DNBSimpleAttribute& operator =(const scl_wstring&)
        DNB_THROW_SPEC((DNBEInvalidCast));

    /**
     *  Conversion function to convert variable of type string to DNBSimpleAttribute.
     */
    DNBSimpleAttribute& operator =(const scl_string&)
        DNB_THROW_SPEC((DNBEInvalidCast));

#if DNB_HAS_BOOL
    DNBSimpleAttribute& operator =(const DNBBoolVector&)
        DNB_THROW_SPEC((DNBEInvalidCast));
#endif

    /**
     *  Conversion function to convert variable of type ByteVector to DNBSimpleAttribute.
     */
    DNBSimpleAttribute& operator =(const ByteVector&)
        DNB_THROW_SPEC((DNBEInvalidCast));

    /**
     *  Conversion function to convert variable of type DNBInt32Vector to DNBSimpleAttribute.
     */
    DNBSimpleAttribute& operator =(const DNBInt32Vector&)
        DNB_THROW_SPEC((DNBEInvalidCast));

    /**
     *  Conversion function to convert variable of type DNBInt64Vector to DNBSimpleAttribute.
     */
    DNBSimpleAttribute& operator =(const DNBInt64Vector&)
        DNB_THROW_SPEC((DNBEInvalidCast));

    /**
     *  Conversion function to convert variable of type DNBInteger32 to DNBSimpleAttribute.
     */
    DNBSimpleAttribute& operator =(DNBInteger32)
        DNB_THROW_SPEC((DNBEInvalidCast));

    /**
     *  Conversion function to convert variable of type DNBInteger64 to DNBSimpleAttribute.
     */
    DNBSimpleAttribute& operator =(DNBInteger64)
        DNB_THROW_SPEC((DNBEInvalidCast));

    /**
     * This function compares right against self and returns a true if equal, else a false.
     *   Equality operator.
     *   @param right
     *          The object to be compared against.
     *   @returns
     *     Boolean true or false.
     */
    inline bool
    operator==( const DNBAttributeBase& right ) const
    DNB_THROW_SPEC_NULL;

    bool
    operator==( const DNBSimpleAttribute& right ) const
    DNB_THROW_SPEC_NULL;

protected:

    virtual bool
    isEqual( const DNBAttributeBase& right ) const
    DNB_THROW_SPEC_NULL;

private:
    AttributeType  type;
    union DATA
    {
        bool            boolValue_;
        DNBByte         charValue_;
        ByteVector      *bytevecValue_;
        DNBReal         realValue_;
        DNBInteger32    int32Value_;
        DNBInteger64    int64Value_;
        DNBMathVector   *vecValue_;
        scl_string      *stringValue_;
        scl_wstring     *scl_wstringValue_;
        DNBBoolVector   *boolvecValue_;
        DNBInt32Vector  *int32vecValue_;
        DNBInt64Vector  *int64vecValue_;
        stringVector    *strvecValue_;
        wStringVector   *wstrvecValue_;
        DNBVector3D     *vec3dValue_;
        DNBEuler3D      *eulerValue_;
    } data_;

    const DNBSimpleAttribute* getDerived( const DNBAttributeBase& right ) const
        DNB_THROW_SPEC((DNBEInvalidCast));

    void releaseMemory()
            DNB_THROW_SPEC_NULL;

    void Assignment(const DNBSimpleAttribute*)
            DNB_THROW_SPEC_NULL;


    static char* Attribute_Type_Names_s[];


#if 0
    static int Attribute_Type_Sizes_s[];
#endif


};

//
//  Inline function definitions.
//

#include <DNBSimpleAttribute.cc>


#endif /*_DNB_SIMPLEATTRIBUTE_H_*/
