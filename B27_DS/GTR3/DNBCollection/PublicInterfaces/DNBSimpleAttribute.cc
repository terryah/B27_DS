//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview HRN trangara 01:08:15
 */
//*
//* FILE:
//*     DNBSimpleAttribute.cc      - principle implementation file
//*
//* MODULE:
//*     DNBSimpleAttribute         - non template class
//*
//* OVERVIEW:
//*     This module defines a class to contain data of arbitrary type.
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     sha         11/21/02    Initial implementation
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1998 - 2000 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*


inline bool
DNBSimpleAttribute::isInstanceOf(  const DNBAttributeBase& base ) const
            DNB_THROW_SPEC_NULL
{

    const DNBSimpleAttribute*   derived = dynamic_cast<
                              const DNBSimpleAttribute* >( &base );
    return ( derived != 0 );

}



inline bool
DNBSimpleAttribute::operator==( const DNBAttributeBase& right ) const
    DNB_THROW_SPEC_NULL
{
    if( this == &right )
    return true;
    return ( isEqual( right ) );
}

