//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 */

//*
//* HISTORY:
//*     Author    Date       Purpose
//*     ------    ----       -------
//*     DWB       2/28/2000  Initial implementation
//*     bkh       7/21/2003  Added documentation
//* 
//* 

#if DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _DNB_SYMBOLTABLE_H_
#define _DNB_SYMBOLTABLE_H_

#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>

#include <DNBMath.h>
#include <DNBSymbol.h>
#include <DNBVector3D.h>
#include <DNBEuler3D.h>
#include <DNBCallback.h>
#include <DNBException.h>

/**
  * This class represents a table of symbols.
  * <br><B>Description</B><br>
  * This class represents a table of symbols and methods to retrieve 
  * the symbol give a path, delimiter and attribute delimiter.
  *
  */
class DNBSymbolTable
{
  public:    
    typedef DNBSymbol< bool >                   DNBboolSymbol;
    typedef DNBSymbol< char >                   DNBcharSymbol;
    typedef DNBSymbol< wchar_t >                DNBwchar_tSymbol;
    typedef DNBSymbol< scl_string >                 DNBstringSymbol;
    typedef DNBSymbol< scl_wstring >                DNBwstringSymbol;
    typedef DNBSymbol< DNBInteger32 >           DNBInteger32Symbol;
    typedef DNBSymbol< scl_vector<DNBInteger32 , DNB_ALLOCATOR( DNBInteger32 ) > >  DNBInt32VecSymbol;
    typedef DNBSymbol< DNBInteger64 >           DNBInteger64Symbol; 
    typedef DNBSymbol< scl_vector< DNBInteger64 , DNB_ALLOCATOR( DNBInteger64 ) > > DNBInt64VecSymbol;
    typedef DNBSymbol< DNBReal >                DNBRealSymbol;
    typedef DNBSymbol< scl_vector< DNBReal, DNB_ALLOCATOR( DNBReal ) > >      DNBRealVecSymbol;
    typedef DNBSymbol< DNBVector3D >            DNBVec3DSymbol;
    typedef DNBSymbol< DNBEuler3D >             DNBEuler3DSymbol;

   /**
    * Obtain a boolean Symbol.
    * @param strPath
    *      String Path
    * @param delimiter
    *      delimiter
    * @param attrDelim
    *      delimiter
    * @return
    *      object of boolen symbol
    */
    virtual 
    DNBboolSymbol
    getboolSymbol(  const scl_wstring& strPath,
                    const wchar_t  delimiter = L'/',
                    const wchar_t  attrDelim = L':' )
        DNB_THROW_SPEC_NULL  = 0;
    
   /**
    * Obtain a character Symbol.
    * @param strPath
    *      String Path
    * @param delimiter
    *      delimiter
    * @param attrDelim
    *      delimiter
    * @return
    *      object of character symbol
    */
    virtual 
    DNBcharSymbol
    getcharSymbol( const scl_wstring& strPath,
                   const wchar_t  delimiter = L'/',
                   const wchar_t  attrDelim = L':' )
        DNB_THROW_SPEC_NULL  = 0;

   /**
    * Obtain a wchar Symbol.
    * @param strPath
    *      String Path
    * @param delimiter
    *      delimiter
    * @param attrDelim
    *      delimiter
    * @return
    *      object of wchar symbol
    */
    virtual 
    DNBwchar_tSymbol
    getwhar_tSymbol( const scl_wstring& strPath,
                     const wchar_t  delimiter = L'/',
                     const wchar_t  attrDelim = L':' )
        DNB_THROW_SPEC_NULL  = 0;

   /**
    * Obtain a string Symbol.
    * @param strPath
    *      String Path
    * @param delimiter
    *      delimiter
    * @param attrDelim
    *      delimiter
    * @return
    *      object of string symbol
    */
    virtual 
    DNBstringSymbol
    getstringSymbol( const scl_wstring& strPath,
                     const wchar_t  delimiter = L'/',
                     const wchar_t  attrDelim = L':' )
        DNB_THROW_SPEC_NULL  = 0;


   /**
    * Obtain a scl_wstring Symbol.
    * @param strPath
    *      String Path
    * @param delimiter
    *      delimiter
    * @param attrDelim
    *      delimiter
    * @return
    *      object of scl_wstring symbol
    */
    virtual 
    DNBwstringSymbol
    getwstringSymbol( const scl_wstring& strPath,
                      const wchar_t  delimiter = L'/',
                      const wchar_t  attrDelim = L':' )
        DNB_THROW_SPEC_NULL  = 0;

   /**
    * Obtain an Integer  Symbol.
    * @param strPath
    *      String Path
    * @param delimiter
    *      delimiter
    * @param attrDelim
    *      delimiter
    * @return
    *      object of integer symbol
    */
    virtual 
    DNBInteger32Symbol
    getInteger32Symbol( const scl_wstring& strPath,
                        const wchar_t  delimiter = L'/',
                        const wchar_t  attrDelim = L':' )
       DNB_THROW_SPEC_NULL  = 0;

   /**
    * Obtain an Integer  Symbol.
    * @param strPath
    *      String Path
    * @param delimiter
    *      delimiter
    * @param attrDelim
    *      delimiter
    * @return
    *      object of symbol of type DNBInt32Vec
    */
    virtual 
    DNBInt32VecSymbol
    getInt32VectSymbol( const scl_wstring& strPath,
                        const wchar_t  delimiter = L'/',
                        const wchar_t  attrDelim = L':' )
        DNB_THROW_SPEC_NULL  = 0;

   /**
    * Obtain an Integer Symbol.
    * @param strPath
    *      String Path
    * @param delimiter
    *      delimiter
    * @param attrDelim
    *      delimiter
    * @return
    *      object of integer symbol
    */
    virtual 
    DNBInteger64Symbol
    getInteger64Symbol( const scl_wstring& strPath,
                        const wchar_t  delimiter = L'/',
                        const wchar_t  attrDelim = L':' )
        DNB_THROW_SPEC_NULL  = 0;

   /**
    * Obtain an Integer Symbol.
    * @param strPath
    *      String Path
    * @param delimiter
    *      delimiter
    * @param attrDelim
    *      delimiter
    * @return
    *      object of integer symbol
    */
    virtual 
    DNBInt64VecSymbol
    getInt64VectSymbol( const scl_wstring& strPath,
                        const wchar_t  delimiter = L'/',
                        const wchar_t  attrDelim = L':' )
        DNB_THROW_SPEC_NULL  = 0;

   /**
    * Obtain a Real Symbol.
    * @param strPath
    *      String Path
    * @param delimiter
    *      delimiter
    * @param attrDelim
    *      delimiter
    * @return
    *      object of real symbol
    */
    virtual 
    DNBRealSymbol
    getRealSymbol( const scl_wstring& strPath,
                   const wchar_t  delimiter = L'/',
                   const wchar_t  attrDelim = L':' )
        DNB_THROW_SPEC_NULL  = 0;

   /**
    * Obtain a Symbol of type DNBRealVecSymbol.
    * @param strPath
    *      String Path
    * @param delimiter
    *      delimiter
    * @param attrDelim
    *      delimiter
    * @return
    *      object of type DNBRealVecSymbol symbol
    */
    virtual 
    DNBRealVecSymbol
    getRealVectSymbol( const scl_wstring& strPath,
                       const wchar_t  delimiter = L'/',
                       const wchar_t  attrDelim = L':' )
        DNB_THROW_SPEC_NULL  = 0;

   /**
    * Obtain a Symbol of type DNBVec3DSymbol.
    * @param strPath
    *      String Path
    * @param delimiter
    *      delimiter
    * @param attrDelim
    *      delimiter
    * @return
    *      object of type DNBVec3DSymbol symbol
    */
    virtual 
    DNBVec3DSymbol
    getVect3DSymbol( const scl_wstring& strPath,
                     const wchar_t  delimiter = L'/',
                     const wchar_t  attrDelim = L':' )
      DNB_THROW_SPEC_NULL  = 0;

   /**
    * Obtain a Euler symbol.
    * @param strPath
    *      String Path
    * @param delimiter
    *      delimiter
    * @param attrDelim
    *      delimiter
    * @return
    *      object of euler symbol
    */
    
    virtual 
    DNBEuler3DSymbol
    getEulerSymbol( const scl_wstring& strPath,
                    const wchar_t  delimiter = L'/',
                    const wchar_t  attrDelim = L':' )
    DNB_THROW_SPEC_NULL  = 0;

#if  DNB_HAS_BOOL
    virtual 
    void
    getSymbol( DNBboolSymbol& symbol,
               const scl_wstring& strPath,
               const wchar_t  delimiter = L'/',
               const wchar_t  attrDelim = L':' )
    DNB_THROW_SPEC_NULL  = 0;
#endif // DNB_HAS_BOOL

   /**
    * Obtain a Symbol.
    * @param symbol
    *      symbol of type DNBcharSymbol
    * @param strPath
    *      String Path
    * @param delimiter
    *      delimiter
    * @param attrDelim
    *      delimiter
    */
    virtual 
    void
    getSymbol( DNBcharSymbol& symbol,
               const scl_wstring& strPath,
               const wchar_t  delimiter = L'/',
               const wchar_t  attrDelim = L':' )
    DNB_THROW_SPEC_NULL  = 0;

   /**
    * Obtain a Symbol.
    * @param symbol
    *      symbol of type DNBwchar
    * @param strPath
    *      String Path
    * @param delimiter
    *      delimiter
    * @param attrDelim
    *      delimiter
    */
    virtual 
    void
    getSymbol( DNBwchar_tSymbol& symbol,
               const scl_wstring& strPath,
               const wchar_t  delimiter = L'/',
               const wchar_t  attrDelim = L':' )
    DNB_THROW_SPEC_NULL  = 0;

   /**
    * Obtain a Symbol.
    * @param symbol
    *      string symbol 
    * @param strPath
    *      String Path
    * @param delimiter
    *      delimiter
    * @param attrDelim
    *      delimiter
    */
    virtual 
    void
    getSymbol( DNBstringSymbol& symbol,
               const scl_wstring& strPath,
               const wchar_t  delimiter = L'/',
               const wchar_t  attrDelim = L':' )
    DNB_THROW_SPEC_NULL  = 0;

   /**
    * Obtain a Symbol.
    * @param symbol
    *      symbol of type DNBwstringSymbol
    * @param strPath
    *      String Path
    * @param delimiter
    *      delimiter
    * @param attrDelim
    *      delimiter
    */
    virtual 
    void
    getSymbol( DNBwstringSymbol& symbol,
               const scl_wstring& strPath,
               const wchar_t  delimiter = L'/',
               const wchar_t  attrDelim = L':' )
    DNB_THROW_SPEC_NULL  = 0;

   /**
    * Obtain a Symbol.
    * @param symbol
    *      symbol of type integer
    * @param strPath
    *      String Path
    * @param delimiter
    *      delimiter
    * @param attrDelim
    *      delimiter
    */
    virtual 
    void
    getSymbol( DNBInteger32Symbol& symbol,
               const scl_wstring& strPath,
               const wchar_t  delimiter = L'/',
               const wchar_t  attrDelim = L':' )
    DNB_THROW_SPEC_NULL  = 0;

   /**
    * Obtain a Symbol.
    * @param symbol
    *      symbol of type integer
    * @param strPath
    *      String Path
    * @param delimiter
    *      delimiter
    * @param attrDelim
    *      delimiter
    */
    virtual 
    void
    getSymbol( DNBInt32VecSymbol& symbol,
               const scl_wstring& strPath,
               const wchar_t  delimiter = L'/',
               const wchar_t  attrDelim = L':' )
    DNB_THROW_SPEC_NULL  = 0;

   /**
    * Obtain a Symbol.
    * @param symbol
    *      symbol of type integer
    * @param strPath
    *      String Path
    * @param delimiter
    *      delimiter
    * @param attrDelim
    *      delimiter
    */
    virtual 
    void
    getSymbol( DNBInteger64Symbol& symbol,
               const scl_wstring& strPath,
               const wchar_t  delimiter = L'/',
               const wchar_t  attrDelim = L':' )
    DNB_THROW_SPEC_NULL  = 0;

   /**
    * Obtain a Symbol.
    * @param symbol
    *      symbol of type integer
    * @param strPath
    *      String Path
    * @param delimiter
    *      delimiter
    * @param attrDelim
    *      delimiter
    */
    virtual 
    void
    getSymbol( DNBInt64VecSymbol& symbol,
               const scl_wstring& strPath,
               const wchar_t  delimiter = L'/',
               const wchar_t  attrDelim = L':' )
    DNB_THROW_SPEC_NULL  = 0;

   /**
    * Obtain a Symbol.
    * @param symbol
    *      symbol of type Real
    * @param strPath
    *      String Path
    * @param delimiter
    *      delimiter
    * @param attrDelim
    *      delimiter
    */
    virtual 
    void
    getSymbol( DNBRealSymbol& symbol,
               const scl_wstring& strPath,
               const wchar_t  delimiter = L'/',
               const wchar_t  attrDelim = L':' )
    DNB_THROW_SPEC_NULL  = 0;

   /**
    * Obtain a Symbol.
    * @param symbol
    *      symbol of type DNBRealvec
    * @param strPath
    *      String Path
    * @param delimiter
    *      delimiter
    * @param attrDelim
    *      delimiter
    */
    virtual 
    void
    getSymbol( DNBRealVecSymbol& symbol,
               const scl_wstring& strPath,
               const wchar_t  delimiter = L'/',
               const wchar_t  attrDelim = L':' )
    DNB_THROW_SPEC_NULL  = 0;

   /**
    * Obtain a Symbol.
    * @param symbol
    *      symbol of type DNBVec3DSymbol
    * @param strPath
    *      String Path
    * @param delimiter
    *      delimiter
    * @param attrDelim
    *      delimiter
    */
    virtual 
    void
    getSymbol( DNBVec3DSymbol& symbol,
               const scl_wstring& strPath,
               const wchar_t  delimiter = L'/',
               const wchar_t  attrDelim = L':' )
    DNB_THROW_SPEC_NULL  = 0;

   /**
    * To obtain a Symbol.
    * @param symbol
    *      symbol of type Euler
    * @param strPath
    *      String Path
    * @param delimiter
    *      delimiter
    * @param attrDelim
    *      delimiter
    */
    virtual 
    void
    getSymbol( DNBEuler3DSymbol& symbol,
               const scl_wstring& strPath,
               const wchar_t  delimiter = L'/',
               const wchar_t  attrDelim = L':' )
    DNB_THROW_SPEC_NULL  = 0;
};


#endif /* _DNB_SYMBOLTABLE_H_ */


