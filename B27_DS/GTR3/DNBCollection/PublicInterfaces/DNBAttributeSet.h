//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 */


//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     joy         02/01/99    Initial implementation.
//*     xin         09/05/01    Removed DNBENotFound Execption from
//*                             getAttribute( const Key& key, 
//*                                           DNBAttributeBase& data ) const
//*     bkh         10/27/03    Changed documentation style from Xenon to CAA.
//*
//* 


#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_ATTRIBUTESET_H_
#define _DNB_ATTRIBUTESET_H_

#include <DNBSystemBase.h>
#include <scl_map.h> 
#include <DNBSystemDefs.h>


#include <DNBCollection.h>
#include <DNBException.h>
#include <DNBAttributeBase.h>


/**
  * A class which contains data elements of arbitrary type.
  * 
  * <br><B>Template Parameter(s)</B><br>
  * @param  class Key 
  * The class of the key associated with each data element.
  * This class should provide the following public functions:
  * \begin{ itemize }
  * <LI>  Key() }
  * <LI>  Key( const Key& right ) }
  * <LI>  ~Key()  }
  * <LI>  Key& operator=( const Key& right ) }
  * <LI>  bool operator==( const Key& right ) }
  * <LI>  bool operator<tt>( const Key& right ) }
  * \end{ itemize }
  * #
  * <br><B>Description</B><br>
  * This class maps data of arbitrary type with an unique key.
  * The data is represented by DNBAttributeBase which is the base class 
  * of the template class DNBAttributeData which contains the arbitrary 
  * data. Apart from standard data manipulation and access functions,the 
  * class provides iterators to iterate through each data element contained
  * in it.   
  * 
  * <br><B>Example</B><br>
  * <pre>
  * Below is an example illustrating an usage as a stand-alone   
  * class. 
  * 
  * 
  * #include <DNBSystemBase.h>
  * #include <scl_iostream.h>
  * #include <scl_string.h>
  * #include <DNBSystemDefs.h>
  * 
  * #include <DNBAttributeSet.h>
  * #include <DNBAttributeData.h>
  * 
  * void
  * DNBGetValue( const DNBAttributeBase& base );
  * 
  * 
  * int
  * main()
  * {
  * 
  * typedef DNBAttributeData<int>	intData;
  * typedef DNBAttributeData<float>	fltData;
  * typedef DNBAttributeData<scl_wstring>	strData;
  * const scl_wstring CountName = L"Count";  
  * const scl_wstring CountNameNew = L"CountNew";  
  * const scl_wstring PiName = L"Pi";  
  * const scl_wstring MsgName = L"Area of circle";  
  * const float Pi = 3.141596;
  * 
  * cout << "///////////////Begin testing//////////////" << endl;
  * DNBAttributeSet<scl_wstring>			attribDB;
  * 
  * attribDB.addAttribute( CountName, intData( 0,true ) );
  * attribDB.addAttribute( PiName, fltData( Pi, false ) );
  * attribDB.addAttribute( MsgName, strData(  L"Pi x radius^2", true) );
  * 
  * DNBAttributeData<float> fltData2( Pi, true );
  * 
  * fltData fltVal = attribDB.getAttribute( PiName );
  * cout << "Attribute name = " << PiName << ", Value = " << 
  * fltVal.getData() << endl;
  * intData intVal = attribDB.getAttribute( CountName );
  * cout << "Attribute name = " << CountName << ", Value = " << 
  * intVal.getData() << endl;
  * 
  * if( intVal.isModifiable() )
  * {
  * intVal.setData( 20 );	
  * }
  * cout << "Attribute name = " << CountName << ", New value = " << 
  * intVal.getData() << endl;
  * 
  * try
  * {
  * fltVal = attribDB.getAttribute( MsgName );
  * }
  * catch( DNBEInvalidCast& error )
  * {
  * cerr << error.what() << endl;
  * 
  * const DNBAttributeBase& attr = 
  * attribDB.getAttribute( MsgName );  
  * strData strVal;
  * if( strVal.isInstanceOf( attr ) )
  * {
  * strVal = attribDB.getAttribute( MsgName );
  * } 
  * cout << "Attribute name = " << MsgName << ", Value = " << 
  * strVal.getData() << endl;
  * 
  * }
  * 
  * 
  * attribDB.addAttribute( CountNameNew, intData(0) );
  * attribDB.setAttribute( CountNameNew, intData( 36 ) );
  * intVal = attribDB.getAttribute( CountNameNew );
  * cout << "Attribute name = " << CountNameNew << ", Value = " << 
  * intVal.getData() << endl;
  * 
  * 
  * cout << "Testing const iterator" << endl;
  * DNBAttributeSet<scl_wstring>::ConstIterator iter = attribDB.begin();
  * DNBAttributeSet<scl_wstring>::ConstIterator last = attribDB.end();
  * 
  * for( ;iter != last; ++iter ) 
  * {
  * DNBAttributeBase* attrBase = ((*iter).second ); 
  * cout << "Attribute name = " << ((*iter).first) << endl;
  * DNBGetValue( *attrBase );
  * }
  * 
  * cout << "///////////////End testing/////////////////" << endl;
  * return 0;
  * }
  * 
  * 
  * void
  * DNBGetValue( const DNBAttributeBase& base ) 
  * {
  * 
  * 
  * DNBAttributeData<int> 		intData;
  * DNBAttributeData<float> 		fltData;
  * DNBAttributeData<DNBArbitraryClass> arbObject;
  * DNBAttributeData<scl_wstring> 		wstrData;
  * if( intData.isInstanceOf( base ) )
  * {
  * intData = base;  
  * int iVal = intData.getData();
  * cout << "Value = " << iVal << endl;
  * 
  * }
  * else if( fltData.isInstanceOf( base ) )
  * {
  * fltData = base;  
  * float fVal = fltData.getData();
  * cout << "Value = " << fVal << endl;
  * }
  * else if( arbObject.isInstanceOf( base ) )
  * {
  * arbObject = base;  
  * cout << "Complex data" << endl;
  * 
  * }
  * else if( wstrData.isInstanceOf( base ) )
  * {
  * wstrData = base;  
  * scl_wstring wstr = wstrData.getData();
  * cout << "Value = " << wstr << endl;
  * 
  * }
  * 
  * }
  * 
  * 
  * OUTPUT:
  * //////////////Begin testing/////////////////
  * Attribute name = Pi, Value = 3.1416
  * Attribute name = Count, Value = 0
  * Attribute name = Count, New value = 20
  * Invalid cast
  * Attribute name = Area of circle, Value = Pi x radius^2
  * Attribute name = CountNew, Value = 36
  * Testing const iterator
  * Attribute name = Area of circle
  * Value = Pi x radius^2
  * Attribute name = Count
  * Value = 0
  * Attribute name = CountNew
  * Value = 36
  * Attribute name = Pi
  * Value = 3.1416
  * ////////////////End testing////////////////////
  * </pre>
  *
  */
template< class Key >
class DNBAttributeSet
{


public:

/**
  * A map of attribute data elements.
  * 
  * <br><B>Description</B><br>
  * This type definition represents a container that associates 
  * attribute data elements with a key. 	  
  * 
  * 
  */
#if DNB_HAS_RW_STANDARD_LIB
    typedef scl_map <Key, DNBAttributeBase*, scl_less<Key>, 
		DNB_ALLOCATOR( DNBAttributeBase* ) >      Map;
#else
    typedef scl_map <Key, DNBAttributeBase*, scl_less<Key> >       Map;
#endif

/**
  * Type of object stored in map.
  * 
  * <br><B>Description</B><br>
  * Type of object stored in map.
  */

    typedef typename Map::value_type                     ValueType;


/**
  * An iterator to iterate through attribute data elements.
  * 
  * <br><B>Description</B><br>
  * This type definition represents an iterator that can be used to
  * iterate through attribute data elements.
  * 
  */
    typedef typename Map::iterator                     Iterator;

/**
  * An const iterator to iterate through attribute data elements.
  * 
  * <br><B>Description</B><br>
  * This type definition represents a const iterator that can be used to
  * iterate through attribute data elements.
  * 
  */
    typedef typename Map::const_iterator               ConstIterator;

/**
  * A reverse iterator to iterate through attribute data elements.
  * 
  * <br><B>Description</B><br>
  * This type definition represents a reverse iterator that can be 
  * used to iterate through attribute data elements.
  * 
  */
    typedef typename Map::reverse_iterator             ReverseIterator;

/**
  * An const reverse iterator to iterate through attribute data 
  * elements.
  * 
  * <br><B>Description</B><br>
  * This type definition represents a const reverse iterator that can 
  * be used to iterate through the attribute data elements.
  * 
  */
    typedef typename Map::const_reverse_iterator       ConstReverseIterator;


/**
  * Default constructor. 
  * 
  * @return
  * Nothing.
  * 
  * <br><B>Description</B><br>
  * This function constructs an empty attribute set.
  * 
  */
    DNBAttributeSet()
        DNB_THROW_SPEC_NULL;


/**
  * Copy constructor.
  * @param  right 
  * The object to be copied.
  * 
  * @return
  * Nothing.
  * 
  * <br><B>Description</B><br>
  * This function copies <tt>right</tt> onto <tt>self</tt>.
  * @exception scl_bad_alloc
  * The attribute data could not be constructed because of    
  * insufficient free memory.
  * 
  */
    DNBAttributeSet( const DNBAttributeSet<Key>& right )
        DNB_THROW_SPEC((scl_bad_alloc));


/**
  * Destructor.
  * 
  * @return
  * Nothing.
  * 
  * <br><B>Description</B><br>
  * Destructor of DNBAttributeSet. 
  * 
  */
    ~DNBAttributeSet()
            DNB_THROW_SPEC_NULL;


/**
  * This functions adds a new attribute element to <tt>self</tt>.
  * @param  key 
  * Key associated with attribute.
  * @param  value 
  * Value of the attribute.
  * 
  * @return
  * Nothing.
  * 
  * <br><B>Description</B><br>
  * This function adds a new attribute element to <tt>self</tt> based on an
  * unique key.
  * @exception scl_bad_alloc
  * The data element could not be inserted because of 
  * insufficient free memory.
  * @exception DNBEAlreadyREgistered
  * Attribute by the given key already exists.
  * 
  */
    void
    addAttribute( const Key& key, const DNBAttributeBase& value )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEAlreadyRegistered));

/**
  * This function sets the value of an attribute attribute in <tt>self</tt>.
  * @param  key 
  * Key associated with attribute.
  * @param  value 
  * Value of the attribute.
  * 
  * @return
  * Nothing.
  * 
  * <br><B>Description</B><br>
  * This function sets the value of an attribute attribute in <tt>self</tt>
  * by the given <tt>key</tt>. If an attribute by the given <tt>key</tt> is not 
  * found, an attribute with the given <tt>value</tt> and <tt>key</tt> is inserted.
  * @exception scl_bad_alloc
  * The data element could not be inserted because of 
  * insufficient free memory.
  * 
  */
    void
    setAttribute( const Key& 	            key, 
                  const DNBAttributeBase&   value )
        DNB_THROW_SPEC((scl_bad_alloc));

/**
  * Removes an attibute element from <tt>self</tt>.
  * @param  key 
  * Key associated with attribute.
  * 
  * @return
  * Nothing.
  * 
  * <br><B>Description</B><br>
  * This function removes an attribute element by given <tt>key</tt> from 
  * <tt>self</tt>.
  * @exception DNBENotFound
  * Attribute by the given <tt>name</tt> was not found. 
  * 
  */
    void
    removeAttribute( const Key& key )
        DNB_THROW_SPEC((DNBENotFound));

/**
  * Gets the base value of an attribute element of given <tt>key</tt> from 
  * <tt>self</tt>.
  * @param  key 
  * Key associated with attribute.
  * 
  * @return
  * Attribute base value.
  * 
  * <br><B>Description</B><br>
  * This function gets the base value of an attribute element 
  * associated with <tt>key</tt> in <tt>self</tt>.
  * @exception DNBENotFound
  * Attribute associated with <tt>key</tt> was not found.
  * 
  */
    const DNBAttributeBase&
    getAttribute( const Key& key, DNBAttributeBase*attr,bool *wantException ) const 
        DNB_THROW_SPEC((DNBENotFound));

/**
  * Gets the value of an attribute associated with <tt>key</tt> in <tt>self</tt>.
  * @param  key 
  * Key associated with attribute.
  * @param  data 
  * Value of attribute.
  * 
  * @return
  * Nothing.
  * 
  * <br><B>Description</B><br>
  * This function gets the value of an attribute data associated 
  * with <tt>key</tt> in <tt>self</tt>.
  * @exception DNBENotFound
  * Attribute by the given <tt>name</tt> was not found.
  * @exception DNBEInvalidCast
  * Attribute data corresponding to <tt>key</tt> does not match
  * type of <tt>data</tt>. 		
  * 
  */
//    bool 
//    getAttribute( const Key& key, DNBAttributeBase& data ) const 
//        DNB_THROW_SPEC((DNBEInvalidCast));

/**
  * Gets the number of attribute data elements in <tt>self</tt>.
  * 
  * @return
  * Number of attribute data elements in <tt>self</tt>.
  * 
  * <br><B>Description</B><br>
  * This function gets the number of attribute data elements
  * in <tt>self</tt>.
  * 
  */
    inline size_t 
    size() const 
        DNB_THROW_SPEC_NULL;

/**
  * Returns the maximum number of data elements in <tt>self</tt>. 
  * 
  * @return
  * Maximum number of attribute data elements in <tt>self</tt>.
  * 
  * <br><B>Description</B><br>
  * This function returns the maximum number of attribute data elements
  * in <tt>self</tt>.
  * 
  */
    inline size_t 
    max_size() const 
        DNB_THROW_SPEC_NULL;

/**
  * Determines whether an attribute associated with <tt>key</tt> is present in
  * <tt>self</tt>.
  * @param  Key 
  * Name of the attribute.
  * 
  * @return
  * Boolean true or false.
  * 
  * <br><B>Description</B><br>
  * This function returns a true if an attribute associated with 
  * <tt>key</tt> is present in <tt>self</tt>. Otherwise a false is returned. 
  * 
  */
    inline bool 
    isDefined( const Key& key ) const  
        DNB_THROW_SPEC_NULL;

/**
  * Determines whether an attribute associated with <tt>key</tt> is modifiable.  
  * @param  key 
  * Key value.
  * 
  * @return
  * Boolean true or false.
  * 
  * <br><B>Description</B><br>
  * This function returns a boolean true if an attribute by given
  * <tt>key</tt> is modifiable. Otherwise a boolean false is returned. 
  * @exception DNBENotFound
  * Attribute by the given <tt>key</tt> was not found.
  * 
  */
    bool 
    isModifiable( const Key& key ) const  
        DNB_THROW_SPEC((DNBENotFound));

/**
  * Retrieves an iterator pointing to the beginning of attribute data.
  * 
  * @return
  * An iterator positioned at the first element of all attribute data.  
  * 
  * <br><B>Description</B><br>
  * This function retrieves an iterator positioned at the first element
  * of all attribute data.
  * 
  */
    inline Iterator 
    begin()  
	DNB_THROW_SPEC_NULL;

/**
  * Retrieves an iterator pointing immediately after the last 
  * attribute data element.
  * 
  * @return
  * An iterator positioned immediately after the last element of all 
  * attribute data.
  * 
  * <br><B>Description</B><br>
  * This function retrieves an iterator positioned immediately after the
  * last element of all attribute data.
  * 
  */
    inline Iterator 
    end()  
	DNB_THROW_SPEC_NULL;

/**
  * Retrieves a const iterator pointing to the beginning of all 
  * attribute data elements.
  * 
  * @return
  * A const iterator positioned at the beginning of all attribute 
  * data elements.
  * 
  * <br><B>Description</B><br>
  * This function retrieves a const iterator positioned at the 
  * beginning of all attribute data elements.
  * 
  */
    inline ConstIterator 
    begin() const 
	DNB_THROW_SPEC_NULL;

/**
  * Retrieves a const iterator pointing immediately after the 
  * last attribute data element.
  * 
  * @return
  * A const iterator positioned immediately after the last attribute 
  * data element.
  * 
  * <br><B>Description</B><br>
  * This function retrieves a const iterator positioned immediately
  * after the last attribute data element.
  * 
  */
    inline ConstIterator 
    end() const 
	DNB_THROW_SPEC_NULL;

/**
  * Retrieves a reverse iterator pointing to the last element of 
  * all attribute data elements.
  * 
  * @return
  * A reverse iterator pointing to the last element of all attribute 
  * data elements. 
  * 
  * <br><B>Description</B><br>
  * This function retrieves a reverse iterator positioned at the 
  * the last element of all attribute data elements.
  * 
  */
    inline ReverseIterator
    rbegin()  
	DNB_THROW_SPEC_NULL;

/**
  * Retrieves a reverse iterator pointing immediately before the first 
  * element of all attribute data elements.
  * 
  * @return
  * A reverse iterator positioned immediately before the first element 
  * of all attribute data elements.
  * 
  * <br><B>Description</B><br>
  * This function retrieves a reverse iterator positioned 
  * immediately before the first element of all attribute data 
  * elements.
  * 
  */
    inline ReverseIterator
    rend()  
	DNB_THROW_SPEC_NULL;

/**
  * Retrieves a const reverse iterator pointing to the last element 
  * of all attribute data elements.
  * 
  * @return
  * A const reverse iterator pointing to the last element of all 
  * attribute data elements. 
  * 
  * <br><B>Description</B><br>
  * This function retrieves a const reverse iterator positioned 
  * at the last element of all attribute data elements.
  * 
  */
    inline ConstReverseIterator
    rbegin( ) const 
	DNB_THROW_SPEC_NULL;

/**
  * Retrieves a const reverse iterator pointing immediately before 
  * first element of all attribute data.
  * 
  * @return
  * A const reverse iterator positioned immediately before the first 
  * element of all attribute data.
  * 
  * <br><B>Description</B><br>
  * This function retrieves a const reverse iterator positioned 
  * immediately before the first element of all attribute data.
  * 
  */
    inline ConstReverseIterator
    rend( ) const 
	DNB_THROW_SPEC_NULL;


/**
  * Swaps contents of two sets of attributes. 
  * @param  attrib 
  * Attribute map.
  * 
  * @return
  * 
  * <br><B>Description</B><br>
  * This functions swaps contents of <tt>attrib</tt> with those of <tt>self</tt>.
  * @exception scl_bad_alloc
  * The attribute sets could not be swapped because of   
  * insufficient free memory.
  * 
  */
    inline void 
    swap( DNBAttributeSet<Key>& attrib ) 
	DNB_THROW_SPEC( (scl_bad_alloc) );

/**
  * Clears <tt>self</tt>. 
  * 
  * @return
  * 
  * <br><B>Description</B><br>
  * This functions clears <tt>self</tt>.
  * 
  */
    inline void
    clear( )
	DNB_THROW_SPEC_NULL;

/**
  * Returns whether <tt>self</tt> is empty.
  * 
  * @return
  * Boolean true or false.
  * 
  * <br><B>Description</B><br>
  * This function returns a boolean true if <tt>self</tt> is empty.
  * Otherwise a boolean false is returned. 
  * 
  */
    inline bool
    empty( ) const
	DNB_THROW_SPEC_NULL;

/**
  * Assignment operator.
  * @param  right 
  * The object to be copied.
  * 
  * @return
  * A reference to <tt>self</tt>.
  * 
  * <br><B>Description</B><br>
  * This function copies <tt>right</tt> onto <tt>self</tt>.
  * @exception scl_bad_alloc
  * The attribute data could not be assigned because of    
  * insufficient free memory.
  * 
  */
    DNBAttributeSet<Key>&
    operator=( const DNBAttributeSet<Key>& right )
        DNB_THROW_SPEC((scl_bad_alloc));

private:

    Map     map_; 		    
    inline bool
    findElement( const Key& key, ConstIterator& iter ) const
	DNB_THROW_SPEC_NULL; 

    inline bool
    findElement( const Key& key, Iterator& iter ) 
	DNB_THROW_SPEC_NULL; 
};





//
//  Public definition file.
//

#include <DNBAttributeSet.cc>

#endif		/* _DNB_ATTRIBUTESET_H_ */
