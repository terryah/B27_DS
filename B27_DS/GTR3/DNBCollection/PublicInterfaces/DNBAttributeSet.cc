//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview HRN trangara 01:08:03
 * @quickreview RTL 04:04:07
 */
//*
//* FILE:
//*     AttributeSet.cc      - principle implementation file
//*
//* MODULE:
//*     DNBAttributeSet      - single template class
//*
//* OVERVIEW:
//*     This module defines a class to contain attribute data elements
//*	of arbitrary type, each associated with an unique key. 
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     joy         02/05/99    Initial implementation
//*     xin         09/05/01    Removed DNBENotFound Execption from
//*                             getAttribute( const Key& key, 
//*                                              DNBAttributeBase& data ) const
//*     mmh         12/10/02    Delete the allocated memory when erasing an
//*                             element from the map (fix Purify MLK)
//*     sha         04/07/04    Delete the allocated memory in clear() method
//*     mmg         06/04/08    Fix for IR A0625404. Clone the attributes in copy constructor
//*                             and in the overloaded operator= method
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1998, 99 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*


template <class Key>
DNBAttributeSet<Key>::DNBAttributeSet( )
        DNB_THROW_SPEC_NULL
        : map_()
{
    // Nothing.
}



template <class Key>
DNBAttributeSet<Key>::DNBAttributeSet( const DNBAttributeSet<Key>& right )
        DNB_THROW_SPEC((scl_bad_alloc))
{
    ConstIterator iter = right.begin();
    ConstIterator last = right.end(); 
    for( ;iter != last; iter++ )
        map_[(*iter).first] = ((*iter).second)->clone();
}


template <class Key>
DNBAttributeSet<Key>::~DNBAttributeSet( )
            DNB_THROW_SPEC_NULL
{
    clear();
}



template <class Key>
void
DNBAttributeSet<Key>::addAttribute(  const Key& 	        key, 
			      	     const DNBAttributeBase&    value )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEAlreadyRegistered))
{

    Iterator  iter;
    if( findElement( key, iter ))
    {
        DNBEAlreadyRegistered    error( DNB_FORMAT("An attribute by the given \
key already exists") );
        throw error;
    }

    map_[key] = value.clone();
}



template <class Key>
void
DNBAttributeSet<Key>::setAttribute( const Key& 	               key, 
		      	            const DNBAttributeBase&    value )
        DNB_THROW_SPEC((scl_bad_alloc))
{

    Iterator  iter;
    if( findElement( key, iter ))
    {
        DNB_PRECONDITION( ((*iter).second )->isModifiable( ));
        DNB_DELETE( (*iter).second );
        map_.erase( key );
    }
    map_[key] = value.clone();

}



template <class Key >
void
DNBAttributeSet<Key>::removeAttribute( const Key& key )
        DNB_THROW_SPEC((DNBENotFound))
{


    Iterator  iter;
    if( !findElement( key, iter ))
    {
	DNBENotFound    error( DNB_FORMAT("The attribute was not found") );
	throw error;
    }

    DNB_DELETE( (*iter).second );
    map_.erase( key );

}

    
    


template <class Key>
const DNBAttributeBase& 
DNBAttributeSet<Key>::getAttribute( const Key& key, DNBAttributeBase*attr,
			bool*wantException ) const 
        DNB_THROW_SPEC((DNBENotFound))
{

    ConstIterator  iter;
    if( !findElement( key, iter ))
    {	
		if(!wantException)
		{
			DNBENotFound    error( DNB_FORMAT("The attribute was not found") );
			throw error;	
		}
		else
		{
			if(true==*wantException)
			{
				DNBENotFound    error( DNB_FORMAT("The attribute was not found") );
				throw error;	
			}
//			*wantException=false;
			return *attr;
		}
    }
    if(wantException)
		*wantException=true;
    return ( *((*iter).second) );

}


/*
template <class Key>
bool 
DNBAttributeSet<Key>::getAttribute( const Key&        key, 
				    DNBAttributeBase& data ) const 
        	      DNB_THROW_SPEC(( DNBEInvalidCast))
{

    ConstIterator  iter;
    if( !findElement( key, iter ))
    {
//	DNBENotFound    error( DNB_FORMAT("The attribute was not found") );
//	throw error;
	return false;
    }

    data = *((*iter).second);

    return true;	    

}

*/
template <class Key>
inline size_t
DNBAttributeSet<Key>::size( ) const 
	DNB_THROW_SPEC_NULL
{
    return map_.size();
}


template <class Key>
inline size_t
DNBAttributeSet<Key>::max_size( ) const 
	DNB_THROW_SPEC_NULL
{
    return map_.max_size();
}


template <class Key>
inline bool
DNBAttributeSet<Key>::isDefined( const Key& key ) const 
	DNB_THROW_SPEC_NULL
{
    ConstIterator  iter;
    return ( findElement( key, iter ) );
}


template <class Key>
bool
DNBAttributeSet<Key>::isModifiable( const Key& key ) const  
        DNB_THROW_SPEC((DNBENotFound))
{
    ConstIterator  iter;
    if( !findElement( key, iter ))
    {
	DNBENotFound    error( DNB_FORMAT("The attribute was not found") );
	throw error;
    }

    return ( ((*iter).second )->isModifiable( ));

}


template <class Key>
inline
typename
DNBAttributeSet<Key>::Iterator
DNBAttributeSet<Key>::begin( )
	DNB_THROW_SPEC_NULL
{
    return ( map_.begin( ) );
}


template <class Key>
inline
typename
DNBAttributeSet<Key>::Iterator
DNBAttributeSet<Key>::end( ) 
	DNB_THROW_SPEC_NULL
{

    return ( map_.end( ) );

}

template <class Key>
inline
typename
DNBAttributeSet<Key>::ConstIterator
DNBAttributeSet<Key>::begin( ) const 
	DNB_THROW_SPEC_NULL
{

    return ( map_.begin( ) );

}


template <class Key>
inline
typename
DNBAttributeSet<Key>::ConstIterator
DNBAttributeSet<Key>::end( ) const 
	DNB_THROW_SPEC_NULL
{

    return ( map_.end( ) );

}



template <class Key>
inline
typename
DNBAttributeSet<Key>::ReverseIterator
DNBAttributeSet<Key>::rbegin( ) 
	DNB_THROW_SPEC_NULL
{
    return ( map_.rbegin( ) );
}


template <class Key>
inline
typename
DNBAttributeSet<Key>::ReverseIterator
DNBAttributeSet<Key>::rend( ) 
	DNB_THROW_SPEC_NULL
{
    return ( map_.rend( ) );
}


template <class Key>
inline
typename
DNBAttributeSet<Key>::ConstReverseIterator
DNBAttributeSet<Key>::rbegin( ) const 
	DNB_THROW_SPEC_NULL
{
    return ( map_.rbegin( ) );
}


template <class Key>
inline
typename
DNBAttributeSet<Key>::ConstReverseIterator
DNBAttributeSet<Key>::rend( ) const 
	DNB_THROW_SPEC_NULL
{
    return ( map_.rend( ) );
}


template <class Key>
inline void
DNBAttributeSet<Key>::swap( DNBAttributeSet<Key>& attrib ) 
	DNB_THROW_SPEC( (scl_bad_alloc) )
{
    map_.swap( attrib.map_ );
}


template <class Key>
inline void
DNBAttributeSet<Key>::clear( )
	DNB_THROW_SPEC_NULL
{
    Iterator iter = begin( );
    Iterator last = end( ); 
    for( ;iter != last; iter++ )
    {
    	DNB_DELETE( (*iter).second );
    }
    map_.clear( ); 
}


template <class Key>
inline bool
DNBAttributeSet<Key>::empty( ) const 
	DNB_THROW_SPEC_NULL
{
    return ( map_.empty( ) ); 

}


template <class Key>
DNBAttributeSet<Key>&
DNBAttributeSet<Key>::operator=( const DNBAttributeSet<Key>& right )
        DNB_THROW_SPEC((scl_bad_alloc))
{
    if( this == &right )
    {
        return *this;
    }

    ConstIterator iter = right.begin();
    ConstIterator last = right.end(); 
    for( ;iter != last; iter++ )
        map_[(*iter).first] = ((*iter).second)->clone();

    return *this;
}


template <class Key>
inline bool
DNBAttributeSet<Key>::findElement( const Key& key, ConstIterator& iter ) const
	DNB_THROW_SPEC_NULL
{
    iter = map_.find( key );
    return ( iter != map_.end() );

}


template <class Key>
inline bool
DNBAttributeSet<Key>::findElement( const Key& key, Iterator& iter ) 
	DNB_THROW_SPEC_NULL
{
    iter = map_.find( key );
    return ( iter != map_.end() );

}

