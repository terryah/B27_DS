//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview HRN trangara 01:08:03
 */
// --------------------------------------------------------
//*
//* FILE:
//*     AttributeData.cc      - principle implementation file
//*
//* MODULE:
//*     DNBAttributeData      - single template class
//*
//* OVERVIEW:
//*     This module defines a class to contain data of arbitrary type. 
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     joy         02/05/99    Initial implementation
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1998, 99 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*

template <class Data>
DNBAttributeData<Data>::DNBAttributeData( )
        DNB_THROW_SPEC( (scl_bad_alloc, DNBException) )
        : DNBAttributeBase( true ), data_()
{
    // Nothing.
}


template <class Data>
DNBAttributeData<Data>::DNBAttributeData( const Data& data, 
					  bool isModifiable ) 
        DNB_THROW_SPEC( (scl_bad_alloc, DNBException) )
	: DNBAttributeBase( isModifiable ), 
	  data_( data ) 
{
    // Nothing.
}


template <class Data>
DNBAttributeData<Data>::DNBAttributeData( const DNBAttributeData<Data>& right )
        DNB_THROW_SPEC( (scl_bad_alloc, DNBException) )
       	: DNBAttributeBase( right ), 
       	  data_( right.data_ )
{
    // Nothing.
}


template<class Data>
DNBAttributeData<Data>::DNBAttributeData( const DNBAttributeBase& right ) 
        DNB_THROW_SPEC( (scl_bad_alloc, DNBException, DNBEInvalidCast) )
	: DNBAttributeBase( right )
{
    const DNBAttributeData<Data>* derived = getDerived( right ); 
    data_ = derived->data_; 
}


template <class Data>
DNBAttributeData<Data>::~DNBAttributeData( )
            DNB_THROW_SPEC_NULL
{
    // Nothing.
}


template <class Data>
DNBAttributeBase*
DNBAttributeData<Data>::clone( ) const 
        DNB_THROW_SPEC((scl_bad_alloc, DNBException))
{
    return DNB_NEW DNBAttributeData<Data> (*this);

}


template <class Data>
inline void 
DNBAttributeData<Data>::setData( const Data& data )
            DNB_THROW_SPEC_NULL
{

    DNB_PRECONDITION( DNBAttributeBase::isModifiable() ); 
    data_ = data;

}


template <class Data>
inline const Data&
DNBAttributeData<Data>::getData( ) const 
            DNB_THROW_SPEC_NULL
{
    return data_;

}


template <class Data>
inline bool
DNBAttributeData<Data>::isInstanceOf(  const DNBAttributeBase& base ) const 
            DNB_THROW_SPEC_NULL
{

    const DNBAttributeData<Data>*	derived = dynamic_cast< 
					          const DNBAttributeData<Data>* >
				  		( &base );
    return ( derived != 0 );

}



template<class Data>
DNBAttributeData<Data>& 
DNBAttributeData<Data>::operator=(  const DNBAttributeBase& right ) 
        DNB_THROW_SPEC((DNBEInvalidCast))
{
    if( this == &right )
	return *this;
    const DNBAttributeData<Data>* derived = getDerived( right ); 
    setData( derived->data_ );
    DNBAttributeBase::operator=( right );
    return *this;

}


template<class Data>
DNBAttributeData<Data>& 
DNBAttributeData<Data>::operator=( const DNBAttributeData<Data>& right ) 
        DNB_THROW_SPEC_NULL
{
    if( this == &right )
	return *this;

    setData( right.data_ );
    DNBAttributeBase::operator=( right );
    return *this;
}


template<class Data>
inline bool 
DNBAttributeData<Data>::operator==( const DNBAttributeBase& right ) const 
	DNB_THROW_SPEC_NULL
{
    if( this == &right )
	return true;
    return ( isEqual( right ) );
}


template<class Data>
bool
DNBAttributeData<Data>::isEqual( const DNBAttributeBase& right ) const
	DNB_THROW_SPEC_NULL
{

    const DNBAttributeData<Data>* derived = dynamic_cast< const DNBAttributeData<Data>* >
				  		( &right );
    if( derived == 0 )
    {
	return false;
    }

    return ( data_ == derived->data_ );
}


template<class Data>
const DNBAttributeData<Data>*
DNBAttributeData<Data>::getDerived( const DNBAttributeBase& right ) const
{

    const DNBAttributeData<Data>* derived = dynamic_cast< const DNBAttributeData<Data>* >
				  		( &right );
    if ( derived == 0 )
    {
        DNBEInvalidCast    error( DNB_FORMAT("Invalid cast") );
        throw error;
    }

    return derived;
}
