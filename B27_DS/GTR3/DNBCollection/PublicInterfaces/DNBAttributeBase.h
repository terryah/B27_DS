//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 */

//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     joy         02/01/99    Initial implementation.
//*     bkh         07/01/03    Added documentation.
//* 

#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif



#ifndef _DNB_ATTRIBUTEBASE_H_
#define _DNB_ATTRIBUTEBASE_H_

#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>


#include <DNBCollection.h>

#include <DNBException.h>

/**
  * The abstract base class for data elements of arbitrary type.
  * 
  * <br><B>Description</B><br>
  * This class defines the base class for data elements of arbitrary 
  * type with the following characteristics:
  * <UL>
  * <LI> It stores whether the data is modifiable.
  * <LI> It provides virtual functions @href DNBAttributeBase::clone, 
  * @href DNBAttributeBase::assign and 
  * @href DNBAttributeBase::isModifiable.
  * </UL>
  */
class ExportedByDNBCollection DNBAttributeBase
{
public:
/**
  * Constructor.
  * @param isModifiable
  * Is data modifiable?
  * 
  * @return
  * Nothing
  * 
  * <br><B>Description</B><br>
  * This function constructs a DNBAttributeBase with the 
  * isModifiable attribute. The attribute data is not modifiable
  * after construction if <tt>isModifiable</tt> is false.
  */
    DNBAttributeBase( bool isModifiable = true )
        DNB_THROW_SPEC_NULL;

/**
  * Copy constructor.
  * @param right
  * Object to be copied. 
  * 
  * @return
  * Nothing
  * 
  * <br><B>Description</B><br>
  * This function constructs an object of type DNBAttributeBase 
  * from <tt>right</tt>. 
  */
    DNBAttributeBase( const DNBAttributeBase& right )
        DNB_THROW_SPEC_NULL;

/**
  * Destructor.
  * 
  * @return
  * Nothing
  * 
  * <br><B>Description</B><br>
  * This function destroys <tt>self</tt>. 
  */
    virtual
    ~DNBAttributeBase( )
            DNB_THROW_SPEC_NULL;
/**
  * Creates a copy of <tt>self</tt>.
  * 
  * @return
  * Copy of <tt>self</tt>.
  * 
  * <br><B>Description</B><br>
  * Virtual function to clone and return a copy of <tt>self</tt>.
  * @exception scl_bad_alloc
  * The data element could not be cloned because of 
  * insufficient memory.
  * @exception DNBException
  * The data element could not be cloned because of an 
  * error in the creation of the attribute data.
  */
    virtual DNBAttributeBase*
    clone( ) const 
	DNB_THROW_SPEC((scl_bad_alloc, DNBException)) = 0 ;

/**
  * Is data modifiable?
  * 
  * @return
  * True or false. 
  * 
  * <br><B>Description</B><br>
  * This function returns a true if the parameterized data
  * is modifiable. Otherwise a false is returned. 
  */
    inline bool
    isModifiable( ) const
        DNB_THROW_SPEC_NULL;

/**
  * Assignment operator.
  * @param  right 
  * The object to be copied.
  * 
  * @return
  * Instance of <tt>self</tt>.
  * 
  * <br><B>Description</B><br>
  * This function copies <tt>right</tt> into <tt>self</tt>.
  */
    DNBAttributeBase& 
    operator=( const DNBAttributeBase& right ) 
	DNB_THROW_SPEC_NULL;

/**
  * Equality operator.
  * @param  right 
  * The object to be compared against.
  * 
  * @return
  * True if <tt>right</tt> is equal to <tt>self</tt>, otherwise false.
  * 
  * <br><B>Description</B><br>
  * This function compares <tt>right</tt> against <tt>self</tt> and
  * returns a true if equal, otherwise a false. 
  */
    inline bool 
    operator==( const DNBAttributeBase& right ) const 
	DNB_THROW_SPEC_NULL;


protected:
    bool
    virtual isEqual( const DNBAttributeBase& right ) const  
	DNB_THROW_SPEC_NULL = 0;

private:

    bool	isModifiable_;

};


//
//  Inline function definitions.
//

#include <DNBAttributeBase.cc>

#endif		/* _DNB_ATTRIBUTEBASE_H_ */
