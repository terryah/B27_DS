//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 */


#if DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _DNB_SYMBOL_H_
#define _DNB_SYMBOL_H_

#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>

#include <DNBCollection.h>
#include <DNBCallback.h>
#include <DNBException.h>

#include <DNBVector3D.h>
#include <DNBEuler3D.h>

/**
 * DNBSymbolBase
 */


class ExportedByDNBCollection DNBSymbolBase
{
  public:
   /**
    * Specifies the symbolType.
    * <p><b>Role</b><br>
    *    This enumeration type specifies the type to be used.
    *     It may be boolean, character etc..</p>
    */
      
      enum symbolType {NOTYPE,
                     boolType, 
                     charType, 
                     wcharType, 
                     stringType,
                     scl_wstringType,
                     Int32Type,
                     Int32VecType,
                     Int64Type,
                     Int64VecType, 
                     RealType, 
                     RealVecType, 
                     Vec3DType, 
                     Euler3DType };

   
	DNBSymbolBase()
        DNB_THROW_SPEC_NULL;
    /**
     *  Copy constructor
     */
	DNBSymbolBase( const DNBSymbolBase& rhs )
        DNB_THROW_SPEC_NULL;

    virtual ~DNBSymbolBase()
        DNB_THROW_SPEC_NULL;

    /**
     * To set the Symbol Type.
     *  @param type
     *      symbol type
     */
    void
    setSymbolType( const symbolType& type )
        DNB_THROW_SPEC_NULL;

    /**
     * To set the Symbol name.
     *  @param wstrName
     *      scl_wstring type
     */
    void
    setSymbolName( const scl_wstring& wstrName )
        DNB_THROW_SPEC_NULL;

    /**
     * To get the Symbol Type.
     */
    const symbolType&
    getSymbolType()
        DNB_THROW_SPEC_NULL;

    /**
     * To get the Symbol name.
     */
    const scl_wstring&
    getSymbolName()
        DNB_THROW_SPEC_NULL;

    const DNBSymbolBase&
    operator=(const DNBSymbolBase& rhs )
        DNB_THROW_SPEC_NULL;


  private:
    scl_wstring             wstrSymbolName_;
    symbolType          symType_;
};


//
//
//  DNBSymbol
//
//

template < class T >
class DNBSymbol : public DNBSymbolBase
{
  public:
    DNBSymbol()
        DNB_THROW_SPEC_NULL;

    DNBSymbol( const DNBSymbol<T>& rhs )
        DNB_THROW_SPEC_NULL;
     
    ~DNBSymbol()
        DNB_THROW_SPEC_NULL;

    const T
    getValue() 
        DNB_THROW_SPEC_NULL;

    void
    setValue( const T& value ) 
        DNB_THROW_SPEC_NULL;

    void
    addSetFunctor( DNBFunctor1<T>& functor )
        DNB_THROW_SPEC_NULL;

    void
    addGetFunctor( DNBFunctor0wRet<T>& functor )
        DNB_THROW_SPEC_NULL;

    operator bool() const
        DNB_THROW_SPEC_NULL;

    const DNBSymbol<T>&
    operator=(const DNBSymbol<T>& rhs )
        DNB_THROW_SPEC_NULL;


  private:
    DNBFunctor1<T>      setFunctor_;
    DNBFunctor0wRet<T>  getFunctor_;

};


#include "DNBSymbol.cc"  //Implementation file


#endif /*_DNB_SYMBOL_H_ */

