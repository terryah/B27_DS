//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/** @CAA2Required */
/*---------------------------------------------------------------------*/
/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */
/*---------------------------------------------------------------------*/
//
//    This Header file is included by all the Module.h header files in
//    PublicInterfaces
//

#ifndef DNBCOLLECTION_H

#define DNBCOLLECTION_H DNBCollection

#ifdef _WINDOWS_SOURCE
#if defined(__DNBCollection)
#define ExportedByDNBCollection __declspec(dllexport)
#else
#define ExportedByDNBCollection __declspec(dllimport)
#endif
#else
#define ExportedByDNBCollection
#endif

#endif /* DNBCOLLECTION_H */
