//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview HRN trangara 01:08:03
 */
// --------------------------------------------------------

template < class T >
DNBSymbol<T>::DNBSymbol()
    DNB_THROW_SPEC_NULL
{
}    
 
template < class T >
DNBSymbol<T>::DNBSymbol( const DNBSymbol<T>& rhs )
    DNB_THROW_SPEC_NULL : DNBSymbolBase( rhs )
{
    setFunctor_ = rhs.setFunctor_;
    getFunctor_ = rhs.getFunctor_;
}    

template < class T >
const DNBSymbol<T>&
DNBSymbol<T>::operator=(const DNBSymbol<T>& rhs)
    DNB_THROW_SPEC_NULL
{
	DNBSymbolBase::operator=( rhs );
    setFunctor_		= rhs.setFunctor_;
    getFunctor_		= rhs.getFunctor_;
    return *this;
}    


template < class T >
DNBSymbol<T>::~DNBSymbol()
    DNB_THROW_SPEC_NULL
{
}    

template < class T >
const T
DNBSymbol<T>::getValue() 
    DNB_THROW_SPEC_NULL
{
//    DNBAssert( getFunctor_ );
    
    return getFunctor_();
}

template < class T >
void
DNBSymbol<T>::setValue( const T& value ) 
    DNB_THROW_SPEC_NULL
{
//    DNBAssert( setFunctor_ );
    
    setFunctor_( value );
}

template < class T >
void
DNBSymbol<T>::addSetFunctor( DNBFunctor1<T>& functor )
    DNB_THROW_SPEC_NULL
{
//  DNBAssert( functor );

    setFunctor_ = functor;
}

template < class T >
void
DNBSymbol<T>::addGetFunctor( DNBFunctor0wRet<T>& functor )
    DNB_THROW_SPEC_NULL
{
    DNB_ASSERT( functor );

    getFunctor_ = functor;
}

template < class T >
DNBSymbol<T>::operator bool() const
    DNB_THROW_SPEC_NULL
{
    return ( getFunctor_ && setFunctor_ );
}    

