
// COPYRIGHT DASSAULT SYSTEMES 1999
/** @CAA2Required */
/**********************************************************************/
/* DON T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPEAR AT ANY TIME */
/**********************************************************************/

/**
 * @nodoc 
 * These #undef statements clean-up the preprocessor so that
 * the hash table declaration macros can be reused at some other
 * point in the source code to declare a hash table on some other
 * type of objects.
 */
#undef	CATHDICO_Retrieve
#undef	CATHDICO_Locate
#undef	CATHDICO_LocatePosition
#undef	CATHDICO_Next
#undef	CATHDICO_NextPosition
#undef	CATHDICO_Remove
#undef	CATHDICO_RemovePosition
#undef	CATHDICO_RemoveAll
#undef	CATHDICO_Dimension
#undef	CATHDICO_Collisions
#undef	CATHDICO_Unused
#undef	CATHDICO_PrintStats
#undef	CATHDICO_PrintAddr
#undef	CATHDICO_ApplyMemberFunct
#undef	CATHDICO_ApplyMemberFunctConst
#undef	CATHDICO_ApplyGlobalFunct
#undef	CATHDICO_ApplyDelete
#undef	CATHDICO_ostreamOP

#undef	CATCOLLEC_ExportedBy
#define	CATCOLLEC_ExportedBy
