#ifndef CAT_VARIANT_BOOL_h
#define CAT_VARIANT_BOOL_h
/** @CAA2Required */
/**********************************************************************/
/* DON T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPEAR AT ANY TIME */
/**********************************************************************/

// COPYRIGHT DASSAULT SYSTEMES 2001

/**
 * This is a temporary definition for the Automation VARIANT_BOOL type.
 * Once all the implementations which use booleans have migrated to
 * CAT_VARIANT_BOOL, CAT_VARIANT_TRUE, and CAT_VARIANT_FALSE,
 * the real definition for them will be substituted.
 * typedef short CAT_VARIANT_BOOL;
 * #define CAT_VARIANT_TRUE  -1 (0xFFFF)
 * #define CAT_VARIANT_FALSE 0x0000
 */
typedef unsigned char CAT_VARIANT_BOOL;
//typedef short CAT_VARIANT_BOOL;

#ifndef CAT_VARIANT_TRUE
#define CAT_VARIANT_TRUE 1
#endif // CAT_VARIANT_TRUE

//#ifndef CAT_VARIANT_TRUE
//#define CAT_VARIANT_TRUE -1
//#endif // CAT_VARIANT_TRUE

#ifndef CAT_VARIANT_FALSE
#define CAT_VARIANT_FALSE 0
#endif // CAT_VARIANT_FALSE

#endif // CAT_VARIANT_BOOL_h

