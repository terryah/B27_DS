BUILT_OBJECT_TYPE = ARCHIVE
ENCODING_KEY = System
DELIVERABLE = NO
BUILD = YES
CPPFLAGS = -DPRA_SUPPORT\
           -U_ASM_ALLOWED
LOCAL_CFLAGS = $(CPPFLAGS)
LOCAL_CCFLAGS = $(CPPFLAGS)

OS = AIX
LOCAL_CFLAGS = $(CPPFLAGS) -D_AIX_SOURCE
LOCAL_CCFLAGS = $(LOCAL_CFLAGS)

OS = HP-UX
LOCAL_CFLAGS = $(CPPFLAGS)
LOCAL_CCFLAGS = $(LOCAL_CFLAGS)

OS = IRIX
LOCAL_CFLAGS = $(CPPFLAGS)
LOCAL_CCFLAGS = $(LOCAL_CFLAGS)

OS = SunOS
LOCAL_CFLAGS = $(CPPFLAGS)
LOCAL_CCFLAGS = $(LOCAL_CFLAGS)

OS = Windows_NT
LOCAL_CFLAGS = $(CPPFLAGS)\
               -D_X86_SOURCE
LOCAL_CCFLAGS = $(LOCAL_CFLAGS)
STATIC_LIBS = YES    

OS = Linux
LOCAL_CFLAGS = $(CPPFLAGS)\
               -D_X86_SOURCE -D_M_IX86
LOCAL_CCFLAGS = $(LOCAL_CFLAGS)
