#
#ifdef LOCAL_DEFINITION_FOR_IID
LINK_WITH_FOR_IID = \
MechanicalCommandsUUID\
SystemUUID 
#else
LINK_WITH_FOR_IID =
#endif 
#
BUILT_OBJECT_TYPE=SHARED LIBRARY

INCLUDED_MODULES = CATMmlSoftModifications CATMmlSys

LINK_WITH=$(LINK_WITH_FOR_IID)    CATMecModLiveInterfaces \
            JS0GROUP JS0FM \
            ObjectModeler \
            CATObjectSpecsModeler \
            AC0SPBAS \
            CATMecModInterfaces \
            SystemUUID CATMecModLiveUseItf\
			ObjectModelerSystem CATMathStream 
