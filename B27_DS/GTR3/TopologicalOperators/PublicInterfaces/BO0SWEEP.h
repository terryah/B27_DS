#include "CATIACGMLevel.h"
#ifdef CATIACGMR420CAA
#ifdef _STATIC_SOURCE
#define	ExportedByBO0SWEEP
#elif defined __BO0SWEEP


// COPYRIGHT DASSAULT SYSTEMES 1999

/** @CAA2Required */
#define ExportedByBO0SWEEP DSYExport
#else
#define ExportedByBO0SWEEP DSYImport
#endif
#include "DSYExport.h"
#else
#ifdef _STATIC_SOURCE
#define	ExportedByBO0SWEEP
#elif defined _WINDOWS_SOURCE
#ifdef	__BO0SWEEP

// COPYRIGHT DASSAULT SYSTEMES 1999
/** @CAA2Required */
#define	ExportedByBO0SWEEP	__declspec(dllexport)
#else
#define	ExportedByBO0SWEEP	__declspec(dllimport)
#endif
#else
#define	ExportedByBO0SWEEP
#endif
#endif
#include "TopologicalOperatorsCommonDec.h"
