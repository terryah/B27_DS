// COPYRIGHT DASSAULT SYSTEMES 2003

/** @CAA2Required */
/*---------------------------------------------------------------------*/
/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */
/*---------------------------------------------------------------------*/

#include "CATIACGMLevel.h"
#ifndef __CATTopologicalOperators_h__
#define __CATTopologicalOperators_h__

#ifdef CATIACGMR420CAA
#ifdef _STATIC_SOURCE
#define	ExportedByCATTopologicalOperators
#elif defined(__CATTopologicalOperators)
#define ExportedByCATTopologicalOperators DSYExport
#else
#define ExportedByCATTopologicalOperators DSYImport
#endif
#include "DSYExport.h"
#else
#ifdef _STATIC_SOURCE
#define	ExportedByCATTopologicalOperators
#elif defined _WINDOWS_SOURCE
  #ifdef	__CATTopologicalOperators
    #define	ExportedByCATTopologicalOperators	__declspec(dllexport)
  #else
    #define	ExportedByCATTopologicalOperators	__declspec(dllimport)
  #endif
#else
  #define	ExportedByCATTopologicalOperators
#endif
#endif
#include "TopologicalOperatorsCommonDec.h"

#endif
