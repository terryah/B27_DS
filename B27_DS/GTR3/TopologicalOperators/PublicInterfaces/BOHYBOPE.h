#include "CATIACGMLevel.h"
#ifdef CATIACGMR420CAA
#ifdef _STATIC_SOURCE
#define	ExportedByBOHYBOPE
#elif defined __BOHYBOPE


// COPYRIGHT DASSAULT SYSTEMES 1999

/** @CAA2Required */
#define ExportedByBOHYBOPE DSYExport
#else
#define ExportedByBOHYBOPE DSYImport
#endif
#include "DSYExport.h"
#else
#ifdef _STATIC_SOURCE
#define	ExportedByBOHYBOPE
#elif defined _WINDOWS_SOURCE
#ifdef	__BOHYBOPE

// COPYRIGHT DASSAULT SYSTEMES 1999
/** @CAA2Required */
#define	ExportedByBOHYBOPE	__declspec(dllexport)
#else
#define	ExportedByBOHYBOPE	__declspec(dllimport)
#endif
#else
#define	ExportedByBOHYBOPE
#endif
#endif
#include "TopologicalOperatorsCommonDec.h"
