#include "CATIACGMLevel.h"
#ifdef CATIACGMR420CAA
#ifdef _STATIC_SOURCE
#define	ExportedByParallel
#elif defined __Parallel


// COPYRIGHT DASSAULT SYSTEMES 1999

/** @CAA2Required */
#define ExportedByParallel DSYExport
#else
#define ExportedByParallel DSYImport
#endif
#include "DSYExport.h"
#else
#ifdef _STATIC_SOURCE
#define	ExportedByParallel
#elif defined _WINDOWS_SOURCE
#ifdef	__Parallel

// COPYRIGHT DASSAULT SYSTEMES 1999
/** @CAA2Required */

#define	ExportedByParallel	__declspec(dllexport)
#else
#define	ExportedByParallel	__declspec(dllimport)
#endif
#else
#define	ExportedByParallel
#endif
#endif
#include "TopologicalOperatorsCommonDec.h"
