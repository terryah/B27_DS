#include "CATIACGMLevel.h"
#ifdef CATIACGMR420CAA
#ifdef _STATIC_SOURCE
#define	ExportedByWireExtrapol
#elif defined __WireExtrapol


/** @CAA2Required */

// COPYRIGHT DASSAULT SYSTEMES 1999
#define ExportedByWireExtrapol DSYExport
#else
#define ExportedByWireExtrapol DSYImport
#endif
#include "DSYExport.h"
#else
#ifdef _STATIC_SOURCE
#define	ExportedByWireExtrapol
#elif defined _WINDOWS_SOURCE
#ifdef	__WireExtrapol
/** @CAA2Required */
// COPYRIGHT DASSAULT SYSTEMES 1999
 
#define	ExportedByWireExtrapol 	__declspec(dllexport)
#else
#define	ExportedByWireExtrapol 	__declspec(dllimport)
#endif
#else
#define	ExportedByWireExtrapol
#endif
#endif
#include "TopologicalOperatorsCommonDec.h"
