#include "CATIACGMLevel.h"
#ifndef Primitives_h
#define Primitives_h

// COPYRIGHT DASSAULT SYSTEMES 1999
/** @CAA2Required */
/*---------------------------------------------------------------------*/
/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */
/*---------------------------------------------------------------------*/
#ifdef CATIACGMR420CAA
#ifdef _STATIC_SOURCE
#define	ExportedByPrimitives
#elif defined(__Primitives)
#define ExportedByPrimitives DSYExport
#else
#define ExportedByPrimitives DSYImport
#endif
#include "DSYExport.h"
#else
#ifdef _STATIC_SOURCE
#define	ExportedByPrimitives
#elif defined _WINDOWS_SOURCE
#ifdef	__Primitives
#define	ExportedByPrimitives	__declspec(dllexport)
#else
#define	ExportedByPrimitives	__declspec(dllimport)
#endif
#else
#define	ExportedByPrimitives
#endif
#endif
#include "TopologicalOperatorsCommonDec.h"
#endif
