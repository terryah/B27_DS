#include "CATIACGMLevel.h"
#ifdef CATIACGMR420CAA
#ifdef _STATIC_SOURCE
#define	ExportedByBODYNOPE
#elif defined __BODYNOPE


// COPYRIGHT DASSAULT SYSTEMES 1999

/** @CAA2Required */
#define ExportedByBODYNOPE DSYExport
#else
#define ExportedByBODYNOPE DSYImport
#endif
#include "DSYExport.h"
#else
#ifdef _STATIC_SOURCE
#define	ExportedByBODYNOPE
#elif defined _WINDOWS_SOURCE
#ifdef	__BODYNOPE

// COPYRIGHT DASSAULT SYSTEMES 1999
/** @CAA2Required */
#define	ExportedByBODYNOPE	__declspec(dllexport)
#else
#define	ExportedByBODYNOPE	__declspec(dllimport)
#endif
#else
#define	ExportedByBODYNOPE
#endif
#endif
#include "TopologicalOperatorsCommonDec.h"
