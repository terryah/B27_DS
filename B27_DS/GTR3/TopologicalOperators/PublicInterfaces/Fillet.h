#include "CATIACGMLevel.h"
#ifdef CATIACGMR420CAA
#ifdef _STATIC_SOURCE
#define	ExportedByFillet
#elif	__Fillet
// COPYRIGHT DASSAULT SYSTEMES 1999
/** @CAA2Required */
#define ExportedByFillet DSYExport
#else
#define	ExportedByFillet	DSYImport
#endif
#include "DSYExport.h"
#else
#ifdef _STATIC_SOURCE
#define	ExportedByFillet
#elif defined _WINDOWS_SOURCE
#ifdef	__Fillet

// COPYRIGHT DASSAULT SYSTEMES 1999
/** @CAA2Required */

#define	ExportedByFillet	__declspec(dllexport)
#else
#define	ExportedByFillet	__declspec(dllimport)
#endif
#else
#define	ExportedByFillet
#endif
#endif
#include "TopologicalOperatorsCommonDec.h"
