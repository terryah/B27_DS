#include "CATIACGMLevel.h"
#ifdef CATIACGMR420CAA
#ifdef _STATIC_SOURCE
#define	ExportedByAdvTopoSketch
#elif defined __AdvTopoSketch


// COPYRIGHT DASSAULT SYSTEMES 1999

/** @CAA2Required */

/*---------------------------------------------------------------------*/

/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */

/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */

/*---------------------------------------------------------------------*/
#define ExportedByAdvTopoSketch DSYExport
#else
#define ExportedByAdvTopoSketch DSYImport
#endif
#include "DSYExport.h"
#else
#ifdef _STATIC_SOURCE
#define	ExportedByAdvTopoSketch
#elif defined _WINDOWS_SOURCE
// COPYRIGHT DASSAULT SYSTEMES 1999
/** @CAA2Required */
/*---------------------------------------------------------------------*/
/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */
/*---------------------------------------------------------------------*/
#ifdef	__AdvTopoSketch
#define	ExportedByAdvTopoSketch	__declspec(dllexport)
#else
#define	ExportedByAdvTopoSketch	__declspec(dllimport)
#endif
#else
#define	ExportedByAdvTopoSketch
#endif
#endif
#include <TopologicalOperatorsCommonDec.h>
