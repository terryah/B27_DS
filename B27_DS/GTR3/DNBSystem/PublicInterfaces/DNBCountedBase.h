//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//
//  Notes:
//    + None
//
//  TODO:
//    + Nothing
//
// SMW: moved from the framework DNBObjectModel to DNBSystem
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_COUNTEDBASE_H_
#define _DNB_COUNTEDBASE_H_


#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>


#include <DNBSystem.h>




//
//  Provide the necessary forward declarations.
//
class DNBCountedObject;




//
//  This class represents the base class for reference-counting smart pointers.
//  It is provided to facilitate the copy-construction and assignment of
//  compatible types of smart pointers.
//
class ExportedByDNBSystem DNBCountedBase
{
public:
    //
    //  This method returns true if <self> is bound to an object, and false
    //  otherwise.
    //
    inline
    operator bool( ) const
        DNB_THROW_SPEC_NULL;

    //
    //  This method returns true if <self> is bound to an object, and false
    //  otherwise.
    //
    inline bool
    isBound( ) const
        DNB_THROW_SPEC_NULL;

    //
    //  This method returns true if <self> references the same object as the
    //  argument <object>, and false otherwise.
    //
    inline bool
    operator==( const DNBCountedObject *object ) const
        DNB_THROW_SPEC_NULL;

    //
    //  This method returns true if <self> references the same object as
    //  <right>, and false otherwise.
    //
    inline bool
    operator==( const DNBCountedBase &right ) const
        DNB_THROW_SPEC_NULL;

    //
    //  This method returns true if <self> references a different object than
    //  the argument <object>, and false otherwise.
    //
    inline bool
    operator!=( const DNBCountedObject *object ) const
        DNB_THROW_SPEC_NULL;

    //
    //  This method returns true if <self> references a different object than
    //  <right>, and false otherwise.
    //
    inline bool
    operator!=( const DNBCountedBase &right ) const
        DNB_THROW_SPEC_NULL;

    //
    //  This method implements a trivial less-than operator that compares the
    //  addresses of <self>'s object and <right>'s object.  By providing such a
    //  method, smart pointers can be stored in STL containers.  Note: If a
    //  different ordering scheme is desired, the method can be overriden in a
    //  derived class.
    //
    inline bool
    operator< ( const DNBCountedBase &right ) const
        DNB_THROW_SPEC_NULL;

    //
    //  This method returns the raw pointer to the referent.  It should not be
    //  called in client code.
    //
    inline DNBCountedObject *
    _getReferent( ) const
        DNB_THROW_SPEC_NULL;

protected:
    //
    //  By defining the following methods in the protected section, this class
    //  can only be used as a base class.
    //
    DNBCountedBase( const DNBCountedObject *object )
        DNB_THROW_SPEC_NULL;

    DNBCountedBase( const DNBCountedBase &right )
        DNB_THROW_SPEC_NULL;

    ~DNBCountedBase( )
        DNB_THROW_SPEC_NULL;

    DNBCountedBase &
    operator=( const DNBCountedObject *object )
        DNB_THROW_SPEC_NULL;

    DNBCountedBase &
    operator=( const DNBCountedBase &right )
        DNB_THROW_SPEC_NULL;

    //
    //  Define some utility methods.
    //
    inline DNBCountedObject *
    getReferent( ) const
        DNB_THROW_SPEC_NULL;

    void
    detach( )
        DNB_THROW_SPEC_NULL;

private:
    void
    transfer( DNBCountedObject *object )
        DNB_THROW_SPEC_NULL;

    DNBCountedObject   *object_;
};




//
//  Include the public definition file.
//
#include "DNBCountedBase.cc"




#endif  /* _DNB_COUNTEDBASE_H_ */
