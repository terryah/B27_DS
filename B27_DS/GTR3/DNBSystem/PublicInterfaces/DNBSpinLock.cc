//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview RTL trangara 01:09:19
 */
// --------------------------------------------------------
inline  void
DNBSpinLock::acquireRead( )
    DNB_THROW_SPEC_NULL
{
    acquire( );
}


inline  void
DNBSpinLock::acquireWrite( )
    DNB_THROW_SPEC_NULL
{
    acquire( );
}


inline  void
DNBSpinLock::release( )
    DNB_THROW_SPEC_NULL
{
    lock_.release( );
}
