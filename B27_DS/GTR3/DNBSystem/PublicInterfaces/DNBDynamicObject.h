//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//
//  Notes:
//    + The method orphanObject() disassociates <self> from all other objects,
//      thereby making it an "orphan".  The resulting object can then be
//      scheduled for deletion.
//
//  TODO:
//    + Implement a memory allocation manager for dynamic objects using the
//      class-specific versions of operator new() and operator delete().
//
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_DYNAMICOBJECT_H_
#define _DNB_DYNAMICOBJECT_H_


#include <DNBSystemBase.h>
#include <scl_string.h>
#include <DNBSystemDefs.h>


#include <DNBSystem.h>


#include <DNBException.h>
#include <DNBClassInfo.h>


//
//  Provide the necessary forward declarations.
//
template <class Object>
class _DNBDynamicTester;


#ifdef  _WINDOWS_SOURCE
#pragma warning(disable:4291)
#endif




//
//  This section defines the macros required to support the run-time type
//  identification (RTTI) facility.  The macro DNB_DECLARE_DYNAMIC_RTTI must be
//  included in the definition of every RTTI-enabled class.
//
#define _DNB_DECLARE_DYNAMIC_METHODS                                        \
public:                                                                     \
    static  scl_string                                                      \
    ClassName( )                                                            \
        DNB_THROW_SPEC_NULL                                                 \
    {                                                                       \
        return ClassInfo_.getClassName( );                                  \
    }                                                                       \
    virtual scl_string                                                      \
    getClassName( ) const                                                   \
        DNB_THROW_SPEC_NULL                                                 \
    {                                                                       \
        return ClassInfo_.getClassName( );                                  \
    }                                                                       \
    static  const DNBClassInfo &                                            \
    ClassInfo( )                                                            \
        DNB_THROW_SPEC_NULL                                                 \
    {                                                                       \
        return ClassInfo_;                                                  \
    }                                                                       \
    virtual const DNBClassInfo &                                            \
    getClassInfo( ) const                                                   \
        DNB_THROW_SPEC_NULL                                                 \
    {                                                                       \
        return ClassInfo_;                                                  \
    }                                                                       \
    virtual bool                                                            \
    isInstanceOf( const DNBClassInfo &other ) const                         \
        DNB_THROW_SPEC_NULL                                                 \
    {                                                                       \
        return ( ClassInfo_.isInstanceOf( other ) );                        \
    }                                                                       \
    virtual bool                                                            \
    isDerivedFrom( const DNBClassInfo &other ) const                        \
        DNB_THROW_SPEC_NULL                                                 \
    {                                                                       \
        return ( ClassInfo_.isDerivedFrom( other ) );                       \
    }                                                                       \
protected:                                                                  \
    virtual void                                                            \
    orphanObject( )                                                         \
        DNB_THROW_SPEC_NULL;                                                \
private:                                                                    \
    void                                                                    \
    orphanMembers( )                                                        \
        DNB_THROW_SPEC_NULL


#define DNB_DECLARE_DYNAMIC_RTTI(_CLASS)                                    \
    _DNB_DECLARE_DYNAMIC_METHODS;                                           \
private:                                                                    \
    typedef      _DNBDynamicTester<_CLASS > DynamicTester;                  \
    friend class _DNBDynamicTester<_CLASS >;                                \
    static const  DNBClassInfo  ClassInfo_


//
//  One of the following macros must be included in the implementation file for
//  each RTTI-enabled non-template class.
//
#define DNB_DEFINE_DYNAMIC_RTTI_B0(_CLASS)                                  \
    void                                                                    \
    _CLASS::orphanObject( )                                                 \
        DNB_THROW_SPEC_NULL                                                 \
    {                                                                       \
        if ( isOrphaned() )                                                 \
            return;                                                         \
        _CLASS::orphanMembers( );                                           \
    }                                                                       \
    const DNBClassInfo                                                      \
    _CLASS::ClassInfo_ = DNBClassInfo( typeid(_CLASS *), sizeof(_CLASS),    \
        NULL, NULL, NULL )


#define DNB_DEFINE_DYNAMIC_RTTI_B1(_CLASS, _BASE1)                          \
    void                                                                    \
    _CLASS::orphanObject( )                                                 \
        DNB_THROW_SPEC_NULL                                                 \
    {                                                                       \
        if ( isOrphaned() )                                                 \
            return;                                                         \
        _CLASS::orphanMembers( );                                           \
        _BASE1::orphanObject( );                                            \
    }                                                                       \
    const DNBClassInfo                                                      \
    _CLASS::ClassInfo_ = DNBClassInfo( typeid(_CLASS *), sizeof(_CLASS),    \
        NULL, &_BASE1::ClassInfo(), NULL )


#define DNB_DEFINE_DYNAMIC_RTTI_B2(_CLASS, _BASE1, _BASE2)                  \
    void                                                                    \
    _CLASS::orphanObject( )                                                 \
        DNB_THROW_SPEC_NULL                                                 \
    {                                                                       \
        if ( isOrphaned() )                                                 \
            return;                                                         \
        _CLASS::orphanMembers( );                                           \
        _BASE1::orphanObject( );                                            \
        _BASE2::orphanObject( );                                            \
    }                                                                       \
    const DNBClassInfo                                                      \
    _CLASS::ClassInfo_ = DNBClassInfo( typeid(_CLASS *), sizeof(_CLASS),    \
        NULL, &_BASE1::ClassInfo(), &_BASE2::ClassInfo() )


//
//  One of the following macros must be included in the public definition file
//  for each RTTI-enabled template class.
//
#define DNB_DEFINE_DYNAMIC_RTTI_TMPL_B0(_CLASS, _PARAMS)                    \
    template _PARAMS                                                        \
    void                                                                    \
    _CLASS::orphanObject( )                                                 \
        DNB_THROW_SPEC_NULL                                                 \
    {                                                                       \
        if ( isOrphaned() )                                                 \
            return;                                                         \
        _CLASS::orphanMembers( );                                           \
    }                                                                       \
    template _PARAMS                                                        \
    const DNBClassInfo                                                      \
    _CLASS::ClassInfo_ = DNBClassInfo( typeid(_CLASS *), sizeof(_CLASS),    \
        NULL, NULL, NULL )


#define DNB_DEFINE_DYNAMIC_RTTI_TMPL_B1(_CLASS, _PARAMS, _BASE1)            \
    template _PARAMS                                                        \
    void                                                                    \
    _CLASS::orphanObject( )                                                 \
        DNB_THROW_SPEC_NULL                                                 \
    {                                                                       \
        if ( isOrphaned() )                                                 \
            return;                                                         \
        _CLASS::orphanMembers( );                                           \
        _BASE1::orphanObject( );                                            \
    }                                                                       \
    template _PARAMS                                                        \
    const DNBClassInfo                                                      \
    _CLASS::ClassInfo_ = DNBClassInfo( typeid(_CLASS *), sizeof(_CLASS),    \
        NULL, &_BASE1::ClassInfo(), NULL )


#define DNB_DEFINE_DYNAMIC_RTTI_TMPL_B2(_CLASS, _PARAMS, _BASE1, _BASE2)    \
    template _PARAMS                                                        \
    void                                                                    \
    _CLASS::orphanObject( )                                                 \
        DNB_THROW_SPEC_NULL                                                 \
    {                                                                       \
        if ( isOrphaned() )                                                 \
            return;                                                         \
        _CLASS::orphanMembers( );                                           \
        _BASE1::orphanObject( );                                            \
        _BASE2::orphanObject( );                                            \
    }                                                                       \
    template _PARAMS                                                        \
    const DNBClassInfo                                                      \
    _CLASS::ClassInfo_ = DNBClassInfo( typeid(_CLASS *), sizeof(_CLASS),    \
        NULL, &_BASE1::ClassInfo(), &_BASE2::ClassInfo() )




//
//  This section defines the macros required to support the dynamic construction
//  of objects.  The macro DNB_DECLARE_DYNAMIC_CREATE must be included in the
//  definition of every dynamically constructable class.
//
#define DNB_DECLARE_DYNAMIC_CREATE(_CLASS)                                  \
    _DNB_DECLARE_DYNAMIC_METHODS;                                           \
private:                                                                    \
    static  DNBDynamicObject *                                              \
    _CreateObject( )                                                        \
        DNB_THROW_SPEC((scl_bad_alloc, DNBException))                           \
    {                                                                       \
        return DNB_NEW _CLASS( );                                           \
    }                                                                       \
    virtual DNBDynamicObject *                                              \
    _replicateObject( ) const                                               \
        DNB_THROW_SPEC((scl_bad_alloc, DNBException))                           \
    {                                                                       \
        return DNB_NEW _CLASS( );                                           \
    }                                                                       \
    virtual DNBDynamicObject *                                              \
    _duplicateObject( CopyMode mode ) const                                 \
        DNB_THROW_SPEC((scl_bad_alloc, DNBException))                           \
    {                                                                       \
        return DNB_NEW _CLASS( *this, mode );                               \
    }                                                                       \
    typedef      _DNBDynamicTester<_CLASS > DynamicTester;                  \
    friend class _DNBDynamicTester<_CLASS >;                                \
    static const  DNBClassInfo  ClassInfo_


//
//  One of the following macros must be included in the implementation file for
//  each dynamically constructable non-template class.
//
#define DNB_DEFINE_DYNAMIC_CREATE_B0(_CLASS)                                \
    void                                                                    \
    _CLASS::orphanObject( )                                                 \
        DNB_THROW_SPEC_NULL                                                 \
    {                                                                       \
        if ( isOrphaned() )                                                 \
            return;                                                         \
        _CLASS::orphanMembers( );                                           \
    }                                                                       \
    const DNBClassInfo                                                      \
    _CLASS::ClassInfo_ = DNBClassInfo( typeid(_CLASS *), sizeof(_CLASS),    \
        _CLASS::_CreateObject, NULL, NULL )


#define DNB_DEFINE_DYNAMIC_CREATE_B1(_CLASS, _BASE1)                        \
    void                                                                    \
    _CLASS::orphanObject( )                                                 \
        DNB_THROW_SPEC_NULL                                                 \
    {                                                                       \
        if ( isOrphaned() )                                                 \
            return;                                                         \
        _CLASS::orphanMembers( );                                           \
        _BASE1::orphanObject( );                                            \
    }                                                                       \
    const DNBClassInfo                                                      \
    _CLASS::ClassInfo_ = DNBClassInfo( typeid(_CLASS *), sizeof(_CLASS),    \
        _CLASS::_CreateObject, &_BASE1::ClassInfo(), NULL )


#define DNB_DEFINE_DYNAMIC_CREATE_B2(_CLASS, _BASE1, _BASE2)                \
    void                                                                    \
    _CLASS::orphanObject( )                                                 \
        DNB_THROW_SPEC_NULL                                                 \
    {                                                                       \
        if ( isOrphaned() )                                                 \
            return;                                                         \
        _CLASS::orphanMembers( );                                           \
        _BASE1::orphanObject( );                                            \
        _BASE2::orphanObject( );                                            \
    }                                                                       \
    const DNBClassInfo                                                      \
    _CLASS::ClassInfo_ = DNBClassInfo( typeid(_CLASS *), sizeof(_CLASS),    \
        _CLASS::_CreateObject, &_BASE1::ClassInfo(), &_BASE2::ClassInfo() )


//
//  One of the following macros must be included in the public definition file
//  for each dynamically constructable template class.
//
#define DNB_DEFINE_DYNAMIC_CREATE_TMPL_B0(_CLASS, _PARAMS)                  \
    template _PARAMS                                                        \
    void                                                                    \
    _CLASS::orphanObject( )                                                 \
        DNB_THROW_SPEC_NULL                                                 \
    {                                                                       \
        if ( isOrphaned() )                                                 \
            return;                                                         \
        _CLASS::orphanMembers( );                                           \
    }                                                                       \
    template _PARAMS                                                        \
    const DNBClassInfo                                                      \
    _CLASS::ClassInfo_ = DNBClassInfo( typeid(_CLASS *), sizeof(_CLASS),    \
        _CLASS::_CreateObject, NULL, NULL )


#define DNB_DEFINE_DYNAMIC_CREATE_TMPL_B1(_CLASS, _PARAMS, _BASE1)          \
    template _PARAMS                                                        \
    void                                                                    \
    _CLASS::orphanObject( )                                                 \
        DNB_THROW_SPEC_NULL                                                 \
    {                                                                       \
        if ( isOrphaned() )                                                 \
            return;                                                         \
        _CLASS::orphanMembers( );                                           \
        _BASE1::orphanObject( );                                            \
    }                                                                       \
    template _PARAMS                                                        \
    const DNBClassInfo                                                      \
    _CLASS::ClassInfo_ = DNBClassInfo( typeid(_CLASS *), sizeof(_CLASS),    \
        _CLASS::_CreateObject, &_BASE1::ClassInfo(), NULL )


#define DNB_DEFINE_DYNAMIC_CREATE_TMPL_B2(_CLASS, _PARAMS, _BASE1, _BASE2)  \
    template _PARAMS                                                        \
    void                                                                    \
    _CLASS::orphanObject( )                                                 \
        DNB_THROW_SPEC_NULL                                                 \
    {                                                                       \
        if ( isOrphaned() )                                                 \
            return;                                                         \
        _CLASS::orphanMembers( );                                           \
        _BASE1::orphanObject( );                                            \
        _BASE2::orphanObject( );                                            \
    }                                                                       \
    template _PARAMS                                                        \
    const DNBClassInfo                                                      \
    _CLASS::ClassInfo_ = DNBClassInfo( typeid(_CLASS *), sizeof(_CLASS),    \
        _CLASS::_CreateObject, &_BASE1::ClassInfo(), &_BASE2::ClassInfo() )




//
//  The following macros are used to test the invariant of a class.  They are
//  typically included at the beginning of every non-const method (including
//  constructors and destructors).  In the debug version of the library, the
//  macros automatically invoke the validateObject( ) method on <self> at the
//  appropriate time.  In particular, validateObject( ) is called upon exiting
//  class constructors and other non-const methods, and upon entering the class
//  destructor.  To handle these cases, the developer should use the macros as
//  indicated below:
//
//    o DNB_VALIDATE_CTOR() in the class constructors,
//    o DNB_VALIDATE_DTOR() in the class destructor,
//    o DNB_VALIDATE_FUNC() in all other non-const methods.
//
#if     DNB_VERIFY

#define DNB_VALIDATE_CTOR()     \
    DynamicTester   _DNBTester( this, DynamicTester::Constructor )
#define DNB_VALIDATE_DTOR()     \
    DynamicTester   _DNBTester( this, DynamicTester::Destructor  )
#define DNB_VALIDATE_FUNC()     \
    DynamicTester   _DNBTester( this, DynamicTester::Function    )

#else

#define DNB_VALIDATE_CTOR()     ((void) 0)
#define DNB_VALIDATE_DTOR()     ((void) 0)
#define DNB_VALIDATE_FUNC()     ((void) 0)

#endif  /* DNB_VERIFY */




//
//  This class represents the principal base class of the Xenon toolkit.  It
//  provides basic services for run-time type identification (RTTI), dynamic
//  construction, and object diagnostic testing.
//
class ExportedByDNBSystem DNBDynamicObject
{
public:
    enum CopyMode
    {
        ShallowCopy,
        DeepCopy,
        BranchCopy
    };


    /*
     * Overloaded versions of new and delete 
     */
    static void*
    operator new( size_t size )
    DNB_THROW_SPEC((scl_bad_alloc));

    static void
    operator delete(void* ptr, size_t size);

    static void*
    operator new[]( size_t size)
    DNB_THROW_SPEC((scl_bad_alloc));

    static void
    operator delete[](void* ptr, size_t size);


    DNB_DECLARE_DYNAMIC_CREATE( DNBDynamicObject );

protected:
    //
    //  By defining the following methods in the protected section, this class
    //  can only be used as a base class.
    //
    DNBDynamicObject( )
        DNB_THROW_SPEC_NULL;

    DNBDynamicObject( const DNBDynamicObject &right, CopyMode mode )
        DNB_THROW_SPEC_NULL;

    virtual
    ~DNBDynamicObject( )
        DNB_THROW_SPEC_ANY;

    DNBDynamicObject &
    operator=( const DNBDynamicObject &right )
        DNB_THROW_SPEC_NULL;

    inline bool
    isOrphaned( ) const
        DNB_THROW_SPEC_NULL;

    virtual DNBDynamicObject *
    replicateObject( ) const
        DNB_THROW_SPEC((scl_bad_alloc, DNBException));

    virtual DNBDynamicObject *
    duplicateObject( CopyMode mode ) const
        DNB_THROW_SPEC((scl_bad_alloc, DNBException));

private:
    //
    //  This method performs a validity check on <self> by checking the class
    //  invariant.  The class invariant is the collection of conditions that
    //  apply during the lifetime of the object; i.e., the period between the
    //  end of the constructor call and the beginning of the destructor call.
    //  The class invariant might temporarily lapse within a member or friend
    //  function of the class, but the member or friend function must restore
    //  the invariant before returning to the caller.
    //
    //  When implementing a new class, the developer should define a private
    //  method called validateObject() which checks the internal state of the
    //  object.  This method should test the individual data members of <self>
    //  using the macro DNB_ASSERT().  In the debug version of the library, the
    //  validateObject() method may terminate the program with a message that
    //  indicates the line number and filename where the assertion failed.
    //
#if 0
    void
    validateObject( ) const
        DNB_THROW_SPEC_NULL;
#endif

    bool        isOrphaned_;            // Orphan status
};




//
//  The following template class simulates the dynamic_cast() operator for
//  classes that are derived from DNBDynamicObject.
//
template <class Target>
class DNBDynamicCast
{
public:
    DNBDynamicCast( const DNBDynamicObject *source )
        DNB_THROW_SPEC_NULL;

    ~DNBDynamicCast( )
        DNB_THROW_SPEC_NULL;

    operator Target( ) const
        DNB_THROW_SPEC_NULL;

private:
    DNBDynamicCast( const DNBDynamicCast<Target> &right )
        DNB_THROW_SPEC_NULL;

    DNBDynamicCast<Target> &
    operator=( const DNBDynamicCast<Target> &right )
        DNB_THROW_SPEC_NULL;

    const DNBDynamicObject *source_;
};




//
//  Include the public definition file.
//
#include <DNBDynamicObject.cc>




#endif  /* _DNB_DYNAMICOBJECT_H_ */
