//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview RTL trangara 01:09:19
 */
// --------------------------------------------------------
//*
//* FILE:
//*     MsgData.cc    - public definition file
//*
//* MODULE:
//*     DNBMsgData    - single non-template class
//*
//* OVERVIEW:
//*     This module defines the abstract base class for localized data elements.
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     jad         08/18/97    Initial implementation.
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1997 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*
inline  DNBMsgBase::SizeType
DNBMsgData::getWidth( ) const DNB_THROW_SPEC_NULL
{
    return width_;
}


inline  DNBMsgBase::CharType
DNBMsgData::getFill( ) const DNB_THROW_SPEC_NULL
{
    return fill_;
}


inline  DNBMsgBase::AlignmentType
DNBMsgData::getAlignment( ) const DNB_THROW_SPEC_NULL
{
    return alignment_;
}


inline  DNBMsgBase::SizeType
DNBMsgData::getPrecision( ) const DNB_THROW_SPEC_NULL
{
    return precision_;
}


inline  DNBMsgBase::SymbolType
DNBMsgData::getSymbol( ) const DNB_THROW_SPEC_NULL
{
    return symbol_;
}


inline  DNBMsgBase::FormatType
DNBMsgData::getFormat( ) const DNB_THROW_SPEC_NULL
{
    return format_;
}


inline  void
DNBMsgData::setWidth( SizeType width ) DNB_THROW_SPEC_NULL
{
    width_ = width;
}


inline  void
DNBMsgData::setFill( CharType fill ) DNB_THROW_SPEC_NULL
{
    fill_ = fill;
}


inline  void
DNBMsgData::setAlignment( AlignmentType alignment ) DNB_THROW_SPEC_NULL
{
    alignment_ = alignment;
}


inline  void
DNBMsgData::setPrecision( SizeType precision ) DNB_THROW_SPEC_NULL
{
    precision_ = precision;
}


inline  void
DNBMsgData::setSymbol( SymbolType symbol ) DNB_THROW_SPEC_NULL
{
    symbol_ = symbol;
}


inline  void
DNBMsgData::setFormat( FormatType format ) DNB_THROW_SPEC_NULL
{
    format_ = format;
}


template <class T>
_DNBMsgManip<T>::_DNBMsgManip( func_t func, T value ) DNB_THROW_SPEC_NULL :
    func_   (func),
    value_  (value)
{
    // Nothing
}


template <class T>
_DNBMsgManip<T>::~_DNBMsgManip( ) DNB_THROW_SPEC_NULL
{
    // Nothing
}


template <class T>
void
_DNBMsgManip<T>::invoke( DNBMsgData &data ) const DNB_THROW_SPEC_NULL
{
    (func_)( data, value_ );
}
