/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2003
//==============================================================================
//
//  FILE: DNBSysFastMutexLock.h
//
//  HISTORY:
//      Author  Date            Comments
//      ------  -----------     --------
//      smw     20-Dec-2003     Initial implementation.
//      jod     09-Aug-2008     Reimplemented using scl_simple_mutex.
//
//==============================================================================
#ifndef _DNBSYSFASTMUTEXLOCK_H_
#define _DNBSYSFASTMUTEXLOCK_H_


#include <DNBSystemBase.h>
#include <sclp_simple_mutex.h>  // for scl_simple_mutex
#include <DNBSystemDefs.h>

#include <DNBSystem.h>          // for ExportedByDNBSystem
#include <DNBException.h>       // for DNBEResourceLimit and friends
#include <DNBSysThreadDefs.h>   // for DNBStaticObject




/**
 *  Represents a simple mutual exclusion (mutex) lock.
 *
 *  @description
 *      This class implements a recursive mutex lock, which can be used to
 *      protect shared resources within a process.  The mutex can be acquired
 *      multiple times by the same thread (nested calls are allowed).
 *      <p>
 *      In concurrent programming, two or more threads may need to access shared
 *      resource at the same time.  Examples of shared resources are global objects,
 *      reference counts, and communication queues.  A mutex grants exclusive access
 *      to the shared resource to only one thread.  If a thread acquires a mutex,
 *      other threads that attempt to acquire the mutex are suspended (blocked) until
 *      the first thread releases the mutex.
 */
class ExportedByDNBSystem DNBSysFastMutexLock
{
public:
    /**
     *  Constructs a mutex lock that is dynamically allocated.
     */
    DNBSysFastMutexLock( )
        DNB_THROW_SPEC((DNBEResourceLimit));

    /**
     *  Constructs a mutex lock that has static/global lifecycle.
     */
    DNBSysFastMutexLock( DNBStaticObject )
        DNB_THROW_SPEC((DNBEResourceLimit));

    /**
     *  Destroys the mutex lock.
     */
    ~DNBSysFastMutexLock( )
        DNB_THROW_SPEC_NULL;

    /**
     *  Attempts to acquire the mutex lock without blocking.
     *
     *  @description
     *      This method requests ownership of the mutex lock.  If the mutex is
     *      unlocked, the calling thread takes ownership of the mutex and the
     *      method returns true.  If the mutex is locked, the method immediately
     *      returns false (without blocking).  The method may be called multiple
     *      times by the same thread (recursive semantics).
     */
    bool    tryAcquire( )
        DNB_THROW_SPEC((DNBEResourceLimit, DNBELockViolation));

    /**
     *  Acquires the mutex lock while possibly blocking.
     *
     *  @description
     *      This method requests ownership of the mutex lock.  If the mutex is
     *      unlocked, the calling thread takes ownership of the mutex and the
     *      method immediately returns.  If the mutex is locked, the calling
     *      thread blocks until the mutex becomes available.  The method may be
     *      called multiple times by the same thread (recursive semantics).
     */
    void    acquire( )
        DNB_THROW_SPEC((DNBEResourceLimit, DNBELockViolation));

    /**
     *  Releases the mutex lock.
     *
     *  @description
     *      This method relinquishes ownership of the mutex lock.  It allows a
     *      different thread to acquire the mutex, which is blocked in the acquire()
     *      call.
     */
    void    release( )
        DNB_THROW_SPEC((DNBELockViolation));

    /**
     *  Represents a guard object for a mutual exclusion (mutex) lock.
     *
     *  @description
     *      This class acquires the specified mutex lock upon creation and releases
     *      the mutex upon destruction.  It allows the safe use of mutex locks in the
     *      presence of exceptions.
     *      <p>
     *      Guard objects work in conjunction with C++ block statements to establish
     *      a critical section of code.  A guard object is typically defined at the
     *      beginning of the block, where it automatically acquires the mutex.  The
     *      mutex is automatically released when the block is exited or an exception
     *      is thrown.
     */
    class ExportedByDNBSystem LockGuard
    {
    public:
        LockGuard( DNBSysFastMutexLock& mutexLock )
            DNB_THROW_SPEC((DNBEResourceLimit, DNBELockViolation));

        ~LockGuard( )
            DNB_THROW_SPEC((DNBELockViolation));

        void    acquire( )
            DNB_THROW_SPEC((DNBEResourceLimit, DNBELockViolation));

        void    release( )
            DNB_THROW_SPEC((DNBELockViolation));

        bool    isAcquired( ) const
            DNB_THROW_SPEC_NULL;

    private:
        // Prohibit copy construction and assignment.
        LockGuard( const LockGuard& );
        LockGuard& operator=( const LockGuard& );

        // Data members
        DNBSysFastMutexLock&    mutexLock_;
        bool                    isAcquired_;
    };


    class ExportedByDNBSystem UnlockGuard
    {
    public:
        UnlockGuard( DNBSysFastMutexLock& mutexLock )
            DNB_THROW_SPEC((DNBELockViolation));

        ~UnlockGuard( )
            DNB_THROW_SPEC((DNBEResourceLimit, DNBELockViolation));

        void    acquire( )
            DNB_THROW_SPEC((DNBEResourceLimit, DNBELockViolation));

        void    release( )
            DNB_THROW_SPEC((DNBELockViolation));

        bool    isAcquired( ) const
            DNB_THROW_SPEC_NULL;

    private:
        // Prohibit copy construction and assignment.
        UnlockGuard( const UnlockGuard& );
        UnlockGuard& operator=( const UnlockGuard& );

        // Data members
        DNBSysFastMutexLock&    mutexLock_;
        bool                    isAcquired_;
    };

private:
    // Prohibit copy construction and assignment operations
    DNBSysFastMutexLock( const DNBSysFastMutexLock& );
    DNBSysFastMutexLock& operator=( const DNBSysFastMutexLock& );

    // Data members
    scl_simple_mutex    mutex_;
};


#endif  /* _DNBSYSFASTMUTEXLOCK_H_ */
