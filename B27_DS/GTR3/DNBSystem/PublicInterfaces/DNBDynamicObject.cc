//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview RTL trangara 01:11:15
 * @quickReview SHA 03:08:06
 */
// --------------------------------------------------------

//------------------------------------------------------------------------------
// Implements DNBDynamicObject::isOrphaned( )
//------------------------------------------------------------------------------
inline bool
DNBDynamicObject::isOrphaned( ) const
    DNB_THROW_SPEC_NULL
{
    return isOrphaned_;
}

//
//  The following template class is used to test the invariant of a class that
//  is derived from DNBDynamicObject.  It should not be accessed directly in
//  client code.
//
template <class Object>
class _DNBDynamicTester
{
public:
    enum MethodType
    {
        Constructor,
        Destructor,
        Function
    };

    _DNBDynamicTester( const Object *object, MethodType methodType )
        DNB_THROW_SPEC_NULL :

        object_     ( object ),
        methodType_ ( methodType )
    {
        if ( methodType_ == Destructor )
            object_->validateObject( );
    }

    ~_DNBDynamicTester( )
        DNB_THROW_SPEC_NULL
    {
        if ( methodType_ != Destructor )
            object_->validateObject( );
    }

private:
    //
    //  Prohibit the following operations.
    //
    _DNBDynamicTester( const _DNBDynamicTester<Object> &right )
        DNB_THROW_SPEC_NULL;

    _DNBDynamicTester<Object> &
    operator=( const _DNBDynamicTester<Object> &right )
        DNB_THROW_SPEC_NULL;

    const Object   *object_;
    MethodType      methodType_;
};




//------------------------------------------------------------------------------
// Implements _DNBGetClassInfo( const Target * )
//------------------------------------------------------------------------------
template <class Target>
const DNBClassInfo &
_DNBGetClassInfo( const Target * )
    DNB_THROW_SPEC_NULL
{
    return Target::ClassInfo( );
}


//------------------------------------------------------------------------------
// Implements DNBDynamicCast<Target>::DNBDynamicCast
//------------------------------------------------------------------------------
template <class Target>
DNBDynamicCast<Target>::DNBDynamicCast( const DNBDynamicObject *source )
    DNB_THROW_SPEC_NULL :
    source_ ( source )
{
    // Nothing
}


//------------------------------------------------------------------------------
// Implements DNBDynamicCast<Target>::~DNBDynamicCast( )
//------------------------------------------------------------------------------
template <class Target>
DNBDynamicCast<Target>::~DNBDynamicCast( )
    DNB_THROW_SPEC_NULL
{
    // Nothing
}


//------------------------------------------------------------------------------
// Implements DNBDynamicCast<Target>::operator Target( )
//------------------------------------------------------------------------------
template <class Target>
DNBDynamicCast<Target>::operator Target( ) const
    DNB_THROW_SPEC_NULL
{
    if ( source_ == NULL )
        return NULL;

    const DNBClassInfo &sourceInfo = source_->getClassInfo( );
    const DNBClassInfo &targetInfo = _DNBGetClassInfo( (Target) NULL );

    if ( sourceInfo.isDerivedFrom( targetInfo ) )
        return (Target) source_;

    return NULL;
}
