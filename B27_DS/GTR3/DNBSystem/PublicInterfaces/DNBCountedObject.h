//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//
//  TODO:
//    + Nothing
//
// SMW: moved from DNBObjectModel to DNBSystem
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNBCountedObject_H_
#define _DNBCountedObject_H_


#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>


#include <DNBSystem.h>
#include <DNBDynamicObject.h>
#include <DNBAtomicCounter.h>




//
//  This class represents the base class for reference-counted objects.  Its
//  interface is compatible with the class RWCountingBody.
//
class ExportedByDNBSystem DNBCountedObject : public DNBDynamicObject
{
    DNB_DECLARE_DYNAMIC_CREATE( DNBCountedObject );

public:
    DNBUnsigned32
    addReference( )
        DNB_THROW_SPEC_NULL;

    DNBUnsigned32
    removeReference( )
        DNB_THROW_SPEC_NULL;

    DNBUnsigned32
    getReferences( ) const
        DNB_THROW_SPEC_NULL;

    bool
    isReferenced( ) const
        DNB_THROW_SPEC_NULL;

    bool
    isShared( ) const
        DNB_THROW_SPEC_NULL;

protected:
    //
    //  By defining the following methods in the protected section, this class
    //  may be used only as a base class.
    //
    DNBCountedObject( DNBUnsigned32 initCount = 0 )
        DNB_THROW_SPEC((DNBEResourceLimit));

    DNBCountedObject( const DNBCountedObject &right, CopyMode mode )
        DNB_THROW_SPEC((DNBEResourceLimit));

    virtual
    ~DNBCountedObject( )
        DNB_THROW_SPEC_NULL;

    DNBCountedObject &
    operator=( const DNBCountedObject &right )
        DNB_THROW_SPEC_NULL;

private:
    DNBAtomicCounter    refCount_;      // Number of external references

    friend class DNBCountedObjectTest;  // For testing purposes only.
};




#endif  /* _DNBCountedObject_H_ */
