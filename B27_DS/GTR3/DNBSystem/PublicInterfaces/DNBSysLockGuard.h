//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
#ifndef DNBSysLockGuard_H
#define DNBSysLockGuard_H


#include <DNBSystem.h>
#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>
#include <DNBSysThreadDefs.h>

class DNBSysIMutexLock;


class ExportedByDNBSystem DNBSysLockGuard
{
public:
    DNBSysLockGuard( DNBSysIMutexLock &mutexLock );

    ~DNBSysLockGuard( );

    void
    acquire( );

    void
    release( );

    bool
    isAcquired( ) const;

private:
    //
    //  Prohibit copy construction and assignment operations.
    //
    DNBSysLockGuard( const DNBSysLockGuard & );

    DNBSysLockGuard &
    operator=( const DNBSysLockGuard & );

    //
    //  Data members.
    //
    DNBSysIMutexLock   &mutexLock_;
    bool                isAcquired_;
};




#endif  // DNBSysLockGuard_H
