//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 */

//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     jad         08/18/97    Initial implementation.
//*     rtl         07/16/02    Added an 'atexit' handler (onExit) to clean up
//*                             the global locale
//*     bkh         10/31/03    Implementation of new documentation style.
//*     smw         05/19/05    Removed use of Rogue Wave; In lack of real loclaization
//*								functionality, this becomes a dummy class
//* 
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_LOCALE_H_
#define _DNB_LOCALE_H_


#include <DNBSystemBase.h>
#include <scl_cfloat.h>
#include <scl_ctime.h>
//#include <rw/locale.h>
//#include <rw/zone.h>
#include <DNBSystemDefs.h>

#include <DNBSystem.h>
#include <DNBMsgBase.h>
#include <DNBSysDate.h>
#include <DNBSysTime.h>
#include <DNBSysDateTime.h>

/**
  * The formatting conventions and time zone properties of a geographic
  * region.
  * 
  * <br><B>Description</B><br>
  * This class encapsulates the formatting conventions and time zone
  * properties of a geographic region or cultural boundary.  It defines an
  * interface for formatting numbers, dates, times, and currency.  The class
  * currently uses the localization and time zone facilities provided by
  * the Rogue Wave Tools.h++ Library (i.e., RWLocale and RWZone).  However,
  * the implementation can be easily modified to use the locale facilities
  * provided by the Standard C++ Library, when such support becomes widely
  * available.  This class is used primarily by the class @href DNBMessage,
  * to generate the localized representation of each data element.
  * 
  * <br><B>Example</B><br>
  * <pre>
  * #include DNBSystemBase.h
  * #include <scl_iostream.h>
  * #include rw/rwtime.h
  * #include DNBSystemDefs.h
  * #include DNBMessage.h
  * 
  * #ifdef  _MSC_VER
  * #define GERMAN_LOC      "german"                // Windows 95/NT
  * #else
  * #define GERMAN_LOC      "de"                    // UNIX
  * #endif
  * 
  * int
  * main()
  * {
  * DNBLocale   GmLocale( GERMAN_LOC );
  * RWTime      rwtime;
  * 
  * DNBMessage  msg( DNB_FORMAT("The current date and time is: %1") );
  * msg  << DNBSetFormat('C') << rwtime;
  * cout << msg.scribe( GmLocale ) << endl;
  * 
  * return (0);
  * }
  * 
  * OUTPUT:
  * The current date and time is: Donnerstag, 25. September 1997 16:21:32
  * </pre>
  *
  * <br><B>Warnings</B><br>
  * The current implementation does not encapsulate character set,
  * collation, or message information.  In addition, the class does not
  * provide thread-safe semantics while the function <tt>setlocale</tt> is being
  * called.  For this reason, there should be precisely one <tt>DNBLocale</tt>
  * object defined within the program.  This restriction will be eliminated
  * when the module begins to use the standard <tt>locale</tt> class.
  * 
  */


class ExportedByDNBSystem DNBLocale : public DNBMsgBase
{
public:
/**
  * Constructs a composite locale object.
  * @param localeName
  * The name of the desired locale (e.g., "de").  The list of
  * possible names is documented in the <tt>setlocale</tt> manual page.
  * @param zone
  * An object which represents the desired time zone, based on the
  * abstract interface specified by the class RWZone.  This object
  * must be allocated on the free-store; it will be deleted when the
  * locale object is destroyed.
  * 
  * @return
  * Nothing
  * 
  * <br><B>Description</B><br>
  * This function constructs a locale object which characterizes the
  * formatting conventions specified by <tt>localeName</tt> and the time zone
  * properties specified by <tt>zone</tt>.  If <tt>localeName</tt> denotes the empty
  * string (""), the function determines the formatting conventions for
  * the user's chosen locale (usually specified by the environment
  * variable <tt>LANG</tt>).  Similarly, if <tt>zone</tt> is set to NULL, the function
  * determines the properties of the local time zone (usually specified
  * by the environment variable <tt>TZ</tt>).
  * @exception scl_runtime_error
  * The parameter <tt>localeName</tt> does not represent a valid locale.
  * @exception scl_bad_alloc
  * The object could not be created because of insufficient free
  * memory.
  * 
  * <br><B>Tips</B><br>
  * By creating multiple instances of this class, a program can have
  * more than one locale active at the same time
  * 
  * <br><B>Warnings</B><br>
  * Despite the existing standard for locale names, many operating
  * system vendors provide variant naming schemes.  If you wish to
  * format values for a specific locale, you should consult the
  * vendor's documentation for the precise name of the locale.  On the
  * UNIX and Windows 95/NT platforms, this information is provided in
  * the <tt>setlocale</tt> manual page.
  * 
  * 
  */
    DNBLocale( const char *localeName = "" /*, const RWZone *zone = NULL */ )
        DNB_THROW_SPEC((scl_runtime_error, scl_bad_alloc));


/**
  * Destroys a composite locale object.
  * 
  * @return
  * Nothing
  * 
  * <br><B>Description</B><br>
  * This function destroys a composite locale object.
  * 
  * 
  */
    virtual
    ~DNBLocale( ) DNB_THROW_SPEC_NULL;


/**
  * Computes the number of characters in a multibyte character string.
  * @param str
  * A pointer to the first element in a multibyte character string.
  * The string must be null-terminated and must begin in the
  * "initial" shift state.
  * @param strLength
  * The number of single-byte characters in <tt>str</tt>, excluding the
  * terminating null character.
  * 
  * @return
  * The number of characters in <tt>str</tt>.
  * 
  * <br><B>Description</B><br>
  * This function returns the number of characters in the multibyte
  * character string (MBCS) referenced by <tt>str</tt>.  The string is assumed
  * to be encoded using the multibyte character set associated with
  * <tt>self</tt>.
  * @exception scl_runtime_error
  * The string <tt>str</tt> contains one or more invalid multibyte
  * characters.
  * 
  * 
  */
    virtual size_t
    length( const char *str, size_t strLength ) const
        DNB_THROW_SPEC((scl_runtime_error));


/**
  * Computes the number of characters in a wide character string.
  * @param str
  * A pointer to the first element in a wide character string.  The
  * string must be null-terminated.
  * 
  * @return
  * The number of characters in <tt>str</tt>.
  * 
  * <br><B>Description</B><br>
  * This function returns the number of characters in the wide character
  * string (WCS) referenced by <tt>str</tt>.  The string is assumed to be
  * encoded using the wide character set associated with <tt>self</tt>.
  * @exception scl_runtime_error
  * The string <tt>str</tt> contains one or more invalid wide characters.
  * 
  * 
  */
    virtual size_t
    length( const wchar_t *str ) const DNB_THROW_SPEC((scl_runtime_error));


/**
  * Converts a multibyte character string to a standard message string.
  * @param str
  * The output string.
  * @param item
  * The multibyte character string to be converted.  This string
  * must be null-terminated and must begin in the "initial" shift
  * state.
  * @param itemLength
  * The number of single-byte characters in <tt>item</tt>, excluding the
  * terminating null character.
  * 
  * @return
  * Nothing
  * 
  * <br><B>Description</B><br>
  * This function converts the multibyte character string (MBCS)
  * referenced by <tt>item</tt> into a form that is compatible with the message
  * library.  The string <tt>item</tt> is assumed to be encoded using the
  * multibyte character set associated with <tt>self</tt>.  The results of the
  * conversion are written to <tt>str</tt>.
  * @exception scl_bad_alloc
  * The conversion could not be performed because of insufficient
  * free memory.
  * @exception scl_runtime_error
  * The string <tt>item</tt> contains one or more invalid multibyte
  * characters.
  * 
  * 
  */
    virtual void
    put( StringType &str, const char *item, size_t itemLength ) const
        DNB_THROW_SPEC((scl_bad_alloc, scl_runtime_error));


/**
  * Converts a wide character string to a standard message string.
  * @param str
  * The output string.
  * @param item
  * The wide character string to be converted.  This string must be
  * null-terminated.
  * @param itemLength
  * The number of wide characters in <tt>item</tt>, excluding the
  * terminating null character.
  * 
  * @return
  * Nothing
  * 
  * <br><B>Description</B><br>
  * This function converts the wide character string (WCS) referenced by
  * <tt>item</tt> into a form that is compatible with the message library.  The
  * string <tt>item</tt> is assumed to be encoded using the wide character set
  * associated with <tt>self</tt>.  The results of the conversion are written
  * to <tt>str</tt>.
  * @exception scl_bad_alloc
  * The conversion could not be performed because of insufficient
  * free memory.
  * @exception scl_runtime_error
  * The string <tt>item</tt> contains one or more invalid wide characters.
  * 
  * 
  */
    virtual void
    put( StringType &str, const wchar_t *item, size_t itemLength ) const
        DNB_THROW_SPEC((scl_bad_alloc, scl_runtime_error));


/**
  * Converts a standard message string to a multibyte character string.
  * @param str
  * The input string.
  * @param item
  * The multibyte character string to be extracted.  If the routine
  * fails, the specified object is untouched.
  * @param itemSize
  * The maximum number of single-byte characters that can be stored
  * in <tt>item</tt> (including the terminating null character).
  * 
  * @return
  * Nothing
  * 
  * <br><B>Description</B><br>
  * This function converts the message string <tt>str</tt> to the multibyte
  * character string (MBCS) referenced by <tt>item</tt>.  The string <tt>str</tt> is
  * assumed to be encoded using the single-byte or wide character set
  * associated with <tt>self</tt>.  Further, the string <tt>item</tt> will be encoded
  * using the multibyte character set associated with <tt>self</tt>.  If the
  * conversion is successful, the routine stores a terminating null
  * character in <tt>item</tt>.
  * @exception scl_bad_alloc
  * The conversion could not be performed because of insufficient
  * free memory.
  * @exception scl_runtime_error
  * The string <tt>str</tt> contains one or more invalid characters.
  * @exception length_error
  * The buffer <tt>item</tt> is of insufficient size to accommodate the
  * output string.
  * 
  * 
  */
    virtual void
    get( const StringType &str, char *item, size_t itemSize ) const
        DNB_THROW_SPEC((scl_bad_alloc, scl_runtime_error, scl_length_error));


/**
  * Converts a standard message string to a wide character string.
  * @param str
  * The input string.
  * @param item
  * The wide character string to be extracted.  If the routine
  * fails, the specified object is untouched.
  * @param itemSize
  * The maximum number of wide characters that can be stored in
  * <tt>item</tt> (including the terminating null character).
  * 
  * @return
  * Nothing
  * 
  * <br><B>Description</B><br>
  * This function converts the message string <tt>str</tt> to the wide
  * character string (WCS) referenced by <tt>item</tt>.  The string <tt>str</tt> is
  * assumed to be encoded using the single-byte or wide character set
  * associated with <tt>self</tt>.  Further, the string <tt>item</tt> will be encoded
  * using the wide character set associated with <tt>self</tt>.  If the
  * conversion is successful, the routine stores a terminating null
  * character in <tt>item</tt>.
  * @exception scl_bad_alloc
  * The conversion could not be performed because of insufficient
  * free memory.
  * @exception scl_runtime_error
  * The string <tt>str</tt> contains one or more invalid characters.
  * @exception length_error
  * The buffer <tt>item</tt> is of insufficient size to accommodate the
  * output string.
  * 
  * 
  */
    virtual void
    get( const StringType &str, wchar_t *item, size_t itemSize ) const
        DNB_THROW_SPEC((scl_bad_alloc, scl_runtime_error, scl_length_error));


/**
  * Converts a signed integer to a string.
  * @param str
  * The output string.
  * @param item
  * The signed integer to be converted.
  * 
  * @return
  * Nothing
  * 
  * <br><B>Description</B><br>
  * This function converts the signed integer <tt>item</tt> to a string, using
  * the formatting conventions specified in <tt>self</tt>.  The results of the
  * conversion are written to <tt>str</tt>.
  * @exception scl_bad_alloc
  * The conversion could not be performed because of insufficient
  * free memory.
  * 
  * 
  */
    virtual void
    put( StringType &str, long item ) const DNB_THROW_SPEC((scl_bad_alloc));


/**
  * Converts a string to a signed integer.
  * @param str
  * The input string.
  * @param item
  * The signed integer to be extracted.  If the routine fails, the
  * specified object is untouched.
  * 
  * @return
  * The Boolean value <tt>true</tt> for a valid integral string, and <tt>false</tt>
  * otherwise.
  * 
  * <br><B>Description</B><br>
  * This function converts the string <tt>str</tt> to a signed integer, using
  * the formatting conventions specified in <tt>self</tt>.  The results of the
  * conversion are written to <tt>item</tt>.  The string may contain spaces
  * before and after the (optional) sign, and at the end.  In addition,
  * it may contain digit group separators.  The function will accept all
  * valid numeric strings, and reject all others.  The following are
  * examples of valid integral strings in an English-speaking locale:
  * 
  * "1"       & " -02. "      & "+ 1,234" \\
  * "1234567" & "1,234,567"   &
  * 
  * 
  * @exception scl_bad_alloc
  * The conversion could not be performed because of insufficient
  * free memory.
  * 
  * 
  */
//    virtual bool
//    get( const StringType &str, long &item ) const DNB_THROW_SPEC((scl_bad_alloc));


/**
  * Converts an unsigned integer to a string.
  * @param str
  * The output string.
  * @param item
  * The unsigned integer to be converted.
  * 
  * @return
  * Nothing
  * 
  * <br><B>Description</B><br>
  * This function converts the unsigned integer <tt>item</tt> to a string,
  * using the formatting conventions specified in <tt>self</tt>.  The results
  * of the conversion are written to <tt>str</tt>.
  * @exception scl_bad_alloc
  * The conversion could not be performed because of insufficient
  * free memory.
  * 
  * 
  */
    virtual void
    put( StringType &str, unsigned long item ) const
        DNB_THROW_SPEC((scl_bad_alloc));


/**
  * Converts a floating-point number to a string.
  * @param str
  * The output string.
  * @param item
  * The floating-point number to be converted.
  * @param precision
  * The number of digits to place after the decimal separator.
  * 
  * @return
  * Nothing
  * 
  * <br><B>Description</B><br>
  * This function converts the floating-point number <tt>item</tt> to a string,
  * using the formatting conventions specified in <tt>self</tt>.  The results
  * of the conversion are written to <tt>str</tt>.
  * @exception scl_bad_alloc
  * The conversion could not be performed because of insufficient
  * free memory.
  * 
  * 
  */
    virtual void
    put( StringType &str, double item, SizeType precision = FLT_DIG ) const
        DNB_THROW_SPEC((scl_bad_alloc));


/**
  * Converts a standard time object to a string.
  * @param str
  * The output string.
  * @param item
  * The <tt>struct tm</tt> object to be converted.  The time component is
  * assumed to be expressed in Greenwich mean time (GMT).  Further,
  * the following fields of <tt>item</tt> must be set consistently:
  * <tt>tm_year</tt>, <tt>tm_mon</tt>, <tt>tm_mday</tt>, <tt>tm_hour</tt>, <tt>tm_min</tt>, and
  * <tt>tm_sec</tt>.
  * @param format
  * A character which specifies the desired output format.  The
  * meanings assigned to the format character are identical to
  * those used in the Standard C Library routine <tt>strftime(3c)</tt>.
  * 
  * @return
  * Nothing
  * 
  * <br><B>Description</B><br>
  * This function converts the <tt>struct tm</tt> object <tt>item</tt> to a string,
  * using the formatting conventions and time zone properties specified
  * in <tt>self</tt>.  The results of the conversion are written to <tt>str</tt>.  The
  * output format is controlled by the parameter <tt>format</tt>, which must
  * specify one of the characters listed below.
  * 
  * <table>
  * <tr align=left>
  * <th>Code</th>       <th> Meaning</th>                       <th> Example </th> </td></tr>
  * <tr>
  * <td>a</td>          <td> Abbreviated weekday name</td>      <td> Wed </td></tr>
  * <tr>
  * <td>A</td>          <td>Full weekday name </td>                 <td> Wednesday </td></tr>
  * <tr><td>b</td>      <td>Abbreviated month name</td>             <td> Feb </td></tr>
  * <tr><td>B</td>      <td>Full month name</td>                    <td>February</td></tr>
  * <tr><td>c</td>      <td>Date and time</td>                      <td>02/29/84 02:34:56 </td></tr>
  * <tr><td>C</td>      <td>Date and time (long form)</td>          <td>Feb 29 14:34:56 1984 </td></tr>
  * <tr><td>d</td>      <td> Day of the month (01-31)</td>          <td>29 </td></tr>
  * <tr><td>H</td>      <td> Hour of the 24-hour day (00-23)</td>   <td>14 </td></tr>
  * <tr><td>I</td>      <td> Hour of the 12-hour day (01-12)</td>   <td>02</td></tr>
  * <tr><td>j</td>      <td> Day of the year (001-366)</td>          <td> 060 </td> </tr>
  * <tr><td>m</td>      <td> Month of the year (01-12)</td>          <td> 02 </td> </tr>
  * <tr><td>M</td>      <td> Minutes after the hour (00-59)</td>     <td> 34 </td> </tr>
  * <tr><td>p</td>      <td> AM/PM indicator, if any </td>           <td> PM </td> </tr>
  * <tr><td>S </td>     <td> Seconds after the minute (00-61)</td>   <td> 56 </td> </tr>
  * <tr><td>U</td>      <td> Sunday week of the year (00-53)</td>    <td> 09 </td> </tr>
  * <tr><td>w</td>      <td> Day of the week (0-6, Sunday=0)</td>    <td> 3 </td> </tr>
  * <tr><td>W </td>     <td> Monday week of the year (00-53)</td>    <td> 09 </td> </tr>
  * <tr><td>x </td>     <td> Date</td>                               <td> 02/29/84 </td> </tr>
  * <tr><td>X </td>     <td> Time</td>                               <td> 02:34:56 </td> </tr>
  * <tr><td>y</td>      <td> Year of the century (00-99)</td>        <td> 84 </td> </tr>
  * <tr><td>Y </td>     <td> Year with the century </td>             <td> 1984 </td> </tr>
  * <tr><td>Z </td>     <td> Time zone name  </td>                   <td> EST </td> </tr>
  * </table >
  * 
  * The third column of this table is the output corresponding to the
  * time and date:
  * 
  * \begin{center}
  * Wednesday, February 29, 1984 02:34:56 EST
  * \end{center}
  * @exception scl_bad_alloc
  * The conversion could not be performed because of insufficient
  * free memory.
  * 
  * 
  */
    virtual void
    put( StringType &str, const struct tm &item, FormatType format ) const
        DNB_THROW_SPEC((scl_bad_alloc));


/**
  * Converts a Rogue Wave time object to a string.
  * @param str
  * The output string.
  * @param item
  * The <tt>RWTime</tt> object to be converted.
  * @param format
  * A character which specifies the desired output format.  The
  * meanings assigned to the format character are identical to
  * those used in the Standard C Library routine <tt>strftime(3c)</tt>.
  * 
  * @return
  * Nothing
  * 
  * <br><B>Description</B><br>
  * This function converts the <tt>RWTime</tt> object <tt>item</tt> to a string, using
  * the formatting conventions and time zone properties specified in
  * <tt>self</tt>.  The results of the conversion are written to <tt>str</tt>.  The
  * output format is controlled by the parameter <tt>format</tt>, which must
  * specify one of the characters listed below.
  * 
  * <table>
  * <tr align=left>
  * <th>Code</th>       <th> Meaning</th>                       <th> Example </th> </td></tr>
  * <tr>
  * <td>a</td>          <td> Abbreviated weekday name</td>      <td> Wed </td></tr>
  * <tr>
  * <td>A</td>          <td>Full weekday name </td>                 <td> Wednesday </td></tr>
  * <tr><td>b</td>      <td>Abbreviated month name</td>             <td> Feb </td></tr>
  * <tr><td>B</td>      <td>Full month name</td>                    <td>February</td></tr>
  * <tr><td>c</td>      <td>Date and time</td>                      <td>02/29/84 02:34:56 </td></tr>
  * <tr><td>C</td>      <td>Date and time (long form)</td>          <td>Feb 29 14:34:56 1984 </td></tr>
  * <tr><td>d</td>      <td> Day of the month (01-31)</td>          <td>29 </td></tr>
  * <tr><td>H</td>      <td> Hour of the 24-hour day (00-23)</td>   <td>14 </td></tr>
  * <tr><td>I</td>      <td> Hour of the 12-hour day (01-12)</td>   <td>02</td></tr>
  * <tr><td>j</td>      <td> Day of the year (001-366)</td>          <td> 060 </td> </tr>
  * <tr><td>m</td>      <td> Month of the year (01-12)</td>          <td> 02 </td> </tr>
  * <tr><td>M</td>      <td> Minutes after the hour (00-59)</td>     <td> 34 </td> </tr>
  * <tr><td>p</td>      <td> AM/PM indicator, if any </td>           <td> PM </td> </tr>
  * <tr><td>S </td>     <td> Seconds after the minute (00-61)</td>   <td> 56 </td> </tr>
  * <tr><td>U</td>      <td> Sunday week of the year (00-53)</td>    <td> 09 </td> </tr>
  * <tr><td>w</td>      <td> Day of the week (0-6, Sunday=0)</td>    <td> 3 </td> </tr>
  * <tr><td>W </td>     <td> Monday week of the year (00-53)</td>    <td> 09 </td> </tr>
  * <tr><td>x </td>     <td> Date</td>                               <td> 02/29/84 </td> </tr>
  * <tr><td>X </td>     <td> Time</td>                               <td> 02:34:56 </td> </tr>
  * <tr><td>y</td>      <td> Year of the century (00-99)</td>        <td> 84 </td> </tr>
  * <tr><td>Y </td>     <td> Year with the century </td>             <td> 1984 </td> </tr>
  * <tr><td>Z </td>     <td> Time zone name  </td>                   <td> EST </td> </tr>
  * </table >
  * 
  * The third column of this table is the output corresponding to the
  * time and date:
  * 
  * \begin{center}
  * Wednesday, February 29, 1984 02:34:56 EST
  * \end{center}
  * @exception scl_bad_alloc
  * The conversion could not be performed because of insufficient
  * free memory.
  * 
  * 
  */
    virtual void
//  put( StringType &str, const RWTime &item, FormatType format ) const
    put( StringType &str, const DNBSysDateTime &item, FormatType format ) const
        DNB_THROW_SPEC((scl_bad_alloc));


/**
  * Converts a DNBSysTime object to a string.
  */
    virtual void
    put( StringType &str, const DNBSysTime &item, FormatType format ) const
        DNB_THROW_SPEC((scl_bad_alloc));


/**
  * Converts a Rogue Wave date object to a string.
  * @param str
  * The output string.
  * @param item
  * The <tt>RWDate</tt> object to be converted.
  * @param format
  * A character which specifies the desired output format.  The
  * meanings assigned to the format character are identical to
  * those used in the Standard C Library routine <tt>strftime(3c)</tt>.
  * 
  * @return
  * Nothing
  * 
  * <br><B>Description</B><br>
  * This function converts the <tt>RWDate</tt> object <tt>item</tt> to a string, using
  * the formatting conventions and time zone properties specified in
  * <tt>self</tt>.  The results of the conversion are written to <tt>str</tt>.  The
  * output format is controlled by the parameter <tt>format</tt>, which must
  * specify one of the characters listed below.
  * 
  ** <table>
  * <tr align=left>
  * <th>Code</th>       <th> Meaning</th>                       <th> Example </th> </td></tr>
  * <tr>
  * <td>a</td>          <td> Abbreviated weekday name</td>      <td> Wed </td></tr>
  * <tr>
  * <td>A</td>          <td>Full weekday name </td>                 <td> Wednesday </td></tr>
  * <tr><td>b</td>      <td>Abbreviated month name</td>             <td> Feb </td></tr>
  * <tr><td>B</td>      <td>Full month name</td>                    <td>February</td></tr>
  * <tr><td>d</td>      <td> Day of the month (01-31)</td>          <td>29 </td></tr>
  * <tr><td>j</td>      <td> Day of the year (001-366)</td>          <td> 060 </td> </tr>
  * <tr><td>m</td>      <td> Month of the year (01-12)</td>          <td> 02 </td> </tr>
  * <tr><td>U</td>      <td> Sunday week of the year (00-53)</td>    <td> 09 </td> </tr>
  * <tr><td>w</td>      <td> Day of the week (0-6, Sunday=0)</td>    <td> 3 </td> </tr>
  * <tr><td>W </td>     <td> Monday week of the year (00-53)</td>    <td> 09 </td> </tr>
  * <tr><td>x </td>     <td> Date</td>                               <td> 02/29/84 </td> </tr>
  * <tr><td>X </td>     <td> Time</td>                               <td> 02:34:56 </td> </tr>
  * <tr><td>y</td>      <td> Year of the century (00-99)</td>        <td> 84 </td> </tr>
  * <tr><td>Y </td>     <td> Year with the century </td>             <td> 1984 </td> </tr>
  * </table >
  * The third column of this table is the output corresponding to the
  * date:
  * 
  * \begin{center}
  * Wednesday, February 29, 1984
  * \end{center}
  * @exception scl_bad_alloc
  * The conversion could not be performed because of insufficient
  * free memory.
  * 
  * 
  */
    virtual void
//    put( StringType &str, const RWDate &item, FormatType format ) const
      put( StringType &str, const DNBSysDate &item, FormatType format ) const
        DNB_THROW_SPEC((scl_bad_alloc));


/**
  * Converts a currency value to a string.
  * @param str
  * The output string.
  * @param item
  * The currency value to be converted.  This parameter is assumed
  * to contain an integer representing the number of units of
  * currency.  For example, in a US locale, 1000 units of currency
  * is equivalent to $10.00.
  * @param symbol
  * Specifies the currency symbol which is used to format <tt>item</tt>.
  * 
  * @return
  * Nothing
  * 
  * <br><B>Description</B><br>
  * This function converts the currency value <tt>item</tt> to a string, using
  * the formatting conventions specified in <tt>self</tt>.  The results of the
  * conversion are written to <tt>str</tt>.  The parameter <tt>symbol</tt> determines
  * whether the local (e.g., "$") or international (e.g., "USD ")
  * currency symbol is applied, or none.
  * @exception scl_bad_alloc
  * The conversion could not be performed because of insufficient
  * free memory.
  * 
  * 
  */
    virtual void
    putMoney( StringType &str, double item, SymbolType symbol = LocalSymbol )
        const DNB_THROW_SPEC((scl_bad_alloc));


/**
  * Determines whether the runtime system supports a named locale.
  * @param localeName
  * The name of the locale to be tested.
  * 
  * @return
  * The Boolean value <tt>true</tt> if the named locale exists, and <tt>false</tt>
  * otherwise.
  * 
  * <br><B>Description</B><br>
  * This static function determines whether the runtime system supports
  * the locale specified by <tt>localeName</tt>.  It is generally called before
  * attempting to create an instance of the @href DNBLocale class.  In
  * this way, the instance can be defined outside of a try block.
  * 
  * 
  */
    static bool
    exists( const char *localeName ) DNB_THROW_SPEC_NULL;


/**
  * Returns a reference to the global locale.
  * 
  * @return
  * A reference to the global locale.
  * 
  * <br><B>Description</B><br>
  * This static function returns a reference to the global locale.  This
  * object is used by several routines as the default locale.  It is
  * logically equivalent to <tt>DNBLocale( "" )</tt>.  That is, the formatting
  * conventions correspond to the user's chosen locale and the time zone
  * properties characterize the local time zone.  The global locale
  * object is created the first time this function is called.
  * @exception scl_bad_alloc
  * The global locale object could not be created because of
  * insufficient free memory.
  * 
  * 
  */
    static const DNBLocale &
    global( ) DNB_THROW_SPEC((scl_bad_alloc));


/**
  * 'atexit' handler which destroys the global locale object
  * 
  * @return
  * 
  * <br><B>Description</B><br>
  * This static function deletes the global locale object. While creating
  * a new global locale , this function is registered with the atexit function.
  * During program termination, this function is executed and deletes the global
  * locale object
  * 
  * 
  */
    static void
    onExit(void) DNB_THROW_SPEC_NULL;


private:
//    const RWZone &
//    getRWZone() const DNB_THROW_SPEC_NULL;

    //
    //  To maintain compatibility with the Standard C++ Library, we shall
    //  disable copy and assignment operations for the current class.
    //
    DNBLocale( const DNBLocale & ) DNB_THROW_SPEC_NULL;

    void
    operator=( const DNBLocale & ) DNB_THROW_SPEC_NULL;

 //   const RWLocale *rwLocale_;
 //   const RWZone   *rwZone_;
};


#endif  /* _DNB_LOCALE_H_ */
