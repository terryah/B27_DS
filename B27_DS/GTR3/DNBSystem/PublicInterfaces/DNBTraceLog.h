/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 */
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//* FILE:
//*     TraceLog.h
//*
//*
//* OVERVIEW:
//*
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     BKH        10/02/03    Implementation
//      rtl        11/07/03    Modify
//*     
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1997-99 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*


#ifndef _DNB_TRACELOG_H_
#define _DNB_TRACELOG_H_


#include <DNBSystemBase.h>
#include <scl_iosfwd.h>
//#include DNB_FSTREAM
#include <scl_strstream.h>
#include <scl_string.h>
#include <DNBSystemDefs.h>

#include <DNBTraceLevel.h>
#include <DNBSystem.h>
#include <DNBSysFastMutexLock.h>
// #include <DNBSysMutexLock.h>
// #include <DNBSysLockGuard.h>

//  User defined environment variables.
//      setenv  DNBTraceDefaultLevel = Debug
//      setenv  DNBF1TraceLevel      = Info
//  For a trace called "DNBFoo", the user may define the following environment
//  variables:
//      export DNBFooTraceLevel  = Debug
//      export DNBFooTraceStream = "c:/temp/foo.txt"
//      export DNBFooTraceFormat = "%n(%p) %f(%l) %m>> "


class ExportedByDNBSystem DNBTraceLog
{
public:
   
   /**
    *  The Tracelog facility
    * <b>Role<b>
    *   The Trace Log facility which displays the trace messages, 
    *   either the standard output or directs it to a V5 log
    * @param name 
    *   The name of the log file
    * @param level
    *   The trace level by default is Unspecified.
    */

    DNBTraceLog( const scl_string &name,DNBTraceLevel level=DNBTraceLevelUnspec,const scl_string format="DefaultFormat")
    DNB_THROW_SPEC_NULL;

   /**
    *   Destructor.
    */
    virtual
    ~DNBTraceLog()
     DNB_THROW_SPEC_NULL;

   /**
    * Method to obtain the trace name.
    */
    scl_string
    getName( ) const;

   /**
    * Method to set the Trace level.
    */ 
    void
    setLevel( DNBTraceLevel level )
    DNB_THROW_SPEC_ANY;

  /**
   * Method to obtain boolean value to display 
   * Fatal/Error/Warning messages when Trace is set to "NONE" at any level.
   */
    bool
    detLevel(DNBTraceLevel lev);
    
   /**
    * Method to obtain the Trace level.
    */ 
    DNBTraceLevel
    getLevel( ) const;

    //
    //  Trace stream.
    //  FIXME: To support trace-specific streams, handle-body semantics are
    //  required on streams/files.  In this way, multiple traces can be directed
    //  to the same file.
    //
#if 0
    void
    setStream( ostream &stream );

    int
    setStream( const scl_string &file );

    ostream &
    getStream( ) const;
#endif

   /**
    * To set the Format
    * Trace format characters (like printf).
    *  %f  : print file name without leading directory
    *  %F  : print file name with leading directory
    *  %l  : print line number
    *  %m  : print level name
    *  %n  : print trace nameq
    *  %p  : print current thread ID
    *  %P  : print current process ID
    *  %t  : print timestamp in hour:minute:sec:usec format
    *  %T  : print timestamp in month/day/year hour:minute:sec:usec format
    *  %%  : print single percent sign, '%'
    *  c   : print literal character
    *  Default Format: "%n(%p) %f(%l) %m: "
    */
    void
    SetFormat( const scl_string &format )
    DNB_THROW_SPEC_NULL;

   /**
    * To obtain the Format.
    */
    scl_string
    GetFormat( )
    DNB_THROW_SPEC_NULL;


   /**
    *  Recursive mutual exclusion lock on trace.
    */
    void
    acquireLock( )
    DNB_THROW_SPEC_NULL;

   /**
    * To Unlock  a trace (output stream).
    */
    void
    releaseLock( )
    DNB_THROW_SPEC_NULL;

    /**
     *  Write prefix string to output stream.
     *  @param level
     *      The DNBTraceLevel- can be any of the 7 levels.
     *  @param msg
     *      The message to be displayed 
     */
    ostream &
    write( DNBTraceLevel level, const char *msg )
    DNB_THROW_SPEC_NULL;

    /**
     *  Write prefix string to file.
     *  @param level
     *      The DNBTraceLevel- can be any of the 7 levels.
     *  @param file
     *      The file name 
     *  @param line
     *      The line number
     */
    ostream &
    write( DNBTraceLevel level, const char *file, int line )
    DNB_THROW_SPEC_NULL;

    //
    // Default trace parameters (static methods).
    //
   /**
    * To set the Default level, by deafult it is set 
    *   to Unspecified - DNBTraceLevelUnspec;
    */
    static void
    SetDefaultLevel( DNBTraceLevel level )
    DNB_THROW_SPEC_NULL;

   /**
    * To obtain the default level
    */
    static DNBTraceLevel
    GetDefaultLevel( )
    DNB_THROW_SPEC_NULL;

   /**
    * To set the deafult stream: it is set to the standard output by default.
    */
    static void
    SetDefaultStream( ostream &stream )
    DNB_THROW_SPEC_NULL;

   /**
    * To set the default stream as the V5 log file.
    */
    static int
    SetDefaultStream( const scl_string &file )
    DNB_THROW_SPEC_NULL;

   /**
    * To obtain the stream.
    */
    static ostream &
    GetDefaultStream( )
    DNB_THROW_SPEC_NULL;

   /**
    * To set the Defaultformat
    */
    static void
    SetDefaultFormat( const scl_string &format )
    DNB_THROW_SPEC_NULL;

   /**
    * To obtain the Defaultformat.
    */
    static scl_string
    GetDefaultFormat( )
    DNB_THROW_SPEC_NULL;

  
    /**
     * Shorten the lock guard type name.
     */
    typedef DNBSysFastMutexLock::LockGuard LockGuard;

private:
    scl_string          name_;
    DNBTraceLevel   level_;
    scl_string          format_;

    ostrstream      Buffer_;
    DNBTraceLevel   currentLevel_;
    DNBSysFastMutexLock mutex_;


/*
#if 0
    ostream        *stream_;
#endif
*/
    static DNBTraceLevel    DefaultLevel;
    static ostream         *DefaultStream;
    static scl_string           DefaultFormat;
    static DNBSysFastMutexLock ClassMutex;

};


class ExportedByDNBSystem DNBTraceCall
{
public:
   /**
    * The call to display the entry and exit functionality. 
    */
    DNBTraceCall( DNBTraceLog &log, const char *msg )
    DNB_THROW_SPEC_NULL;

    ~DNBTraceCall( )
     DNB_THROW_SPEC_NULL;

    DNBTraceLog &log_;
    char *msg_;
};

/**
 * The Fatal, Error and Warning messages are always directed to the V5log file.
 */
//
//  The following macros are used to report unrecoverable errors.
//
#define DNB_TRACE_FATAL(_LOG, _MSG)                                         \
    _DNB_TRACE_IMPL0(_LOG, _MSG, DNBTraceLevelFatal)

/**
 * The Fatal message which accepts the log and the message as input.
 */

#define DNB_TRACE_FATAL0(_LOG, _MSG)                                        \
    _DNB_TRACE_IMPL0(_LOG, _MSG, DNBTraceLevelFatal)

/**
 * The Fatal message which takes in the log, message and an object. 
 */
#define DNB_TRACE_FATAL1(_LOG, _MSG, _A1)                                   \
    _DNB_TRACE_IMPL1(_LOG, _MSG, _A1, DNBTraceLevelFatal)

/**
 * The Fatal trace which takes in the log, message and 2 object.
 */
#define DNB_TRACE_FATAL2(_LOG, _MSG, _A1, _A2)                              \
    _DNB_TRACE_IMPL2(_LOG, _MSG, _A1, _A2, DNBTraceLevelFatal)

/**
 * The Fatal message which takes in the log, message and 3 object.
 */
#define DNB_TRACE_FATAL3(_LOG, _MSG, _A1, _A2, _A3)                         \
    _DNB_TRACE_IMPL3(_LOG, _MSG, _A1, _A2, _A3, DNBTraceLevelFatal)

/**
 * The Fatal message which takes in the log, message and 4 object.
 */
#define DNB_TRACE_FATAL4(_LOG, _MSG, _A1, _A2, _A3, _A4)                    \
    _DNB_TRACE_IMPL4(_LOG, _MSG, _A1, _A2, _A3, _A4, DNBTraceLevelFatal)

//
//  The following macros are used to report recoverable errors.
//
#define DNB_TRACE_ERROR(_LOG, _MSG)                                         \
    _DNB_TRACE_IMPL0(_LOG, _MSG, DNBTraceLevelError)

/**
 * The Error message which accepts the log and the message as input.
 */
#define DNB_TRACE_ERROR0(_LOG, _MSG)                                        \
    _DNB_TRACE_IMPL0(_LOG, _MSG, DNBTraceLevelError)

/**
 * The Error message which takes in the log, message and 1 object.
 */

#define DNB_TRACE_ERROR1(_LOG, _MSG, _A1)                                   \
    _DNB_TRACE_IMPL1(_LOG, _MSG, _A1, DNBTraceLevelError)

/**
 * The Error message which takes in the log, message and 2 object.
 */
#define DNB_TRACE_ERROR2(_LOG, _MSG, _A1, _A2)                              \
    _DNB_TRACE_IMPL2(_LOG, _MSG, _A1, _A2, DNBTraceLevelError)

/**
 * The Error message which takes in the log, message and 3 object.
 */

#define DNB_TRACE_ERROR3(_LOG, _MSG, _A1, _A2, _A3)                         \
    _DNB_TRACE_IMPL3(_LOG, _MSG, _A1, _A2, _A3, DNBTraceLevelError)

/**
 * The Error message which takes in the log, message and 4 object.
 */

#define DNB_TRACE_ERROR4(_LOG, _MSG, _A1, _A2, _A3, _A4)                    \
    _DNB_TRACE_IMPL4(_LOG, _MSG, _A1, _A2, _A3, _A4, DNBTraceLevelError)


//
//  The following macros are used to report insignificant/potential errors.
//
#define DNB_TRACE_WARNING(_LOG, _MSG)                                       \
    _DNB_TRACE_IMPL0(_LOG, _MSG, DNBTraceLevelWarning)

/**
 * The Warning message which accepts the log and the message as input.
 */

#define DNB_TRACE_WARNING0(_LOG, _MSG)                                      \
    _DNB_TRACE_IMPL0(_LOG, _MSG, DNBTraceLevelWarning)

/**
 * The Warning message which accepts the log, message and 1 object as input.
 */

#define DNB_TRACE_WARNING1(_LOG, _MSG, _A1)                                 \
    _DNB_TRACE_IMPL1(_LOG, _MSG, _A1, DNBTraceLevelWarning)

/**
 * The Warning message which accepts the log, message and 2 objects as input.
 */

#define DNB_TRACE_WARNING2(_LOG, _MSG, _A1, _A2)                            \
    _DNB_TRACE_IMPL2(_LOG, _MSG, _A1, _A2, DNBTraceLevelWarning)

/**
 * The Warning message which accepts the log, message and 3 objects as input.
 */

#define DNB_TRACE_WARNING3(_LOG, _MSG, _A1, _A2, _A3)                       \
    _DNB_TRACE_IMPL3(_LOG, _MSG, _A1, _A2, _A3, DNBTraceLevelWarning)

/**
 * The Warning message which accepts the log,message and 4 objects as input.
 */

#define DNB_TRACE_WARNING4(_LOG, _MSG, _A1, _A2, _A3, _A4)                  \
    _DNB_TRACE_IMPL4(_LOG, _MSG, _A1, _A2, _A3, _A4, DNBTraceLevelWarning)


//
//  The following macros are used to output informational messages.
//
#if DNB_VERIFY

#define DNB_TRACE_INFO(_LOG, _MSG)                                          \
    _DNB_TRACE_IMPL0(_LOG, _MSG, DNBTraceLevelInfo)

/**
 * Macro to display the Information which takes in the log and message.
 */

#define DNB_TRACE_INFO0(_LOG, _MSG)                                         \
    _DNB_TRACE_IMPL0(_LOG, _MSG, DNBTraceLevelInfo)

/**
 * The Informational macro which takes in the log, message and an object.
 */

#define DNB_TRACE_INFO1(_LOG, _MSG, _A1)                                    \
    _DNB_TRACE_IMPL1(_LOG, _MSG, _A1, DNBTraceLevelInfo)

/**
 * Macro which takes in the log, message and 2 objects.
 */

#define DNB_TRACE_INFO2(_LOG, _MSG, _A1, _A2)                               \
    _DNB_TRACE_IMPL2(_LOG, _MSG, _A1, _A2, DNBTraceLevelInfo)

/**
 * Macro which takes in the log, message and 3 objects.
 */

#define DNB_TRACE_INFO3(_LOG, _MSG, _A1, _A2, _A3)                          \
    _DNB_TRACE_IMPL3(_LOG, _MSG, _A1, _A2, _A3, DNBTraceLevelInfo)

/**
 * Macro which takes in the log, message and 4 object.
 */

#define DNB_TRACE_INFO4(_LOG, _MSG, _A1, _A2, _A3, _A4)                     \
    _DNB_TRACE_IMPL4(_LOG, _MSG, _A1, _A2, _A3, _A4, DNBTraceLevelInfo)

#else

#define DNB_TRACE_INFO(_LOG, _MSG)                          ((void) 0)

#define DNB_TRACE_INFO0(_LOG, _MSG)                         ((void) 0)

#define DNB_TRACE_INFO1(_LOG, _MSG, _A1)                    ((void) 0)

#define DNB_TRACE_INFO2(_LOG, _MSG, _A1, _A2)               ((void) 0)

#define DNB_TRACE_INFO3(_LOG, _MSG, _A1, _A2, _A3)          ((void) 0)

#define DNB_TRACE_INFO4(_LOG, _MSG, _A1, _A2, _A3, _A4)     ((void) 0)

#endif  /* DNB_VERIFY */




//
//  The following macros are used to output debug messages.
//
#if DNB_VERIFY

/**
 * Macro to output the debug information.
 */

#define DNB_TRACE_DEBUG(_LOG, _MSG)                                         \
    _DNB_TRACE_IMPL0(_LOG, _MSG, DNBTraceLevelDebug)

/**
 * The Debug message which accepts the log and a message.
 */

#define DNB_TRACE_DEBUG0(_LOG, _MSG)                                        \
    _DNB_TRACE_IMPL0(_LOG, _MSG, DNBTraceLevelDebug)

/**
 * The Debug message which accepts the log, msg and 1 object as input.
 */

#define DNB_TRACE_DEBUG1(_LOG, _MSG, _A1)                                   \
    _DNB_TRACE_IMPL1(_LOG, _MSG, _A1, DNBTraceLevelDebug)

/**
 * The Warning message which accepts the log, msg and 2 objects as input.
 */

#define DNB_TRACE_DEBUG2(_LOG, _MSG, _A1, _A2)                              \
    _DNB_TRACE_IMPL2(_LOG, _MSG, _A1, _A2, DNBTraceLevelDebug)

/**
 * The Warning message which accepts the log, msg and 3 objects as input.
 */

#define DNB_TRACE_DEBUG3(_LOG, _MSG, _A1, _A2, _A3)                         \
    _DNB_TRACE_IMPL3(_LOG, _MSG, _A1, _A2, _A3, DNBTraceLevelDebug)

/**
 * The Warning message which accepts the log, msg and 4 objects as input.
 */

#define DNB_TRACE_DEBUG4(_LOG, _MSG, _A1, _A2, _A3, _A4)                    \
    _DNB_TRACE_IMPL4(_LOG, _MSG, _A1, _A2, _A3, _A4, DNBTraceLevelDebug)

#else

#define DNB_TRACE_DEBUG(_LOG, _MSG)                         ((void) 0)

#define DNB_TRACE_DEBUG0(_LOG, _MSG)                        ((void) 0)

#define DNB_TRACE_DEBUG1(_LOG, _MSG, _A1)                   ((void) 0)

#define DNB_TRACE_DEBUG2(_LOG, _MSG, _A1, _A2)              ((void) 0)

#define DNB_TRACE_DEBUG3(_LOG, _MSG, _A1, _A2, _A3)         ((void) 0)

#define DNB_TRACE_DEBUG4(_LOG, _MSG, _A1, _A2, _A3, _A4)    ((void) 0)

#endif  /* DNB_VERIFY */



/**
 * The following macro is used to output entry/exit messages.
 */
#if DNB_VERIFY

#define DNB_TRACE_CALL(_LOG, _MSG)                                          \
    DNBTraceCall _DNBTraceCallDummy ## __LINE__ (_LOG, _MSG)

#else

#define DNB_TRACE_CALL(_LOG, _MSG)                          ((void) 0)

#endif  /* DNB_VERIFY */



#include <DNBTraceLog.cc>

#endif  /* _DNB_TRACELOG_H_*/
