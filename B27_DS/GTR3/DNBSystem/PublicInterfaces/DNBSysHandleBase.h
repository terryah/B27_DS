//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
//
// DNBSysHandleBase.h
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     rtl        05/05/2005   Initial Implementation

// NOTES:
// TBD
//==============================================================================
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_SYSHANDLE_BASE_H_
#define _DNB_SYSHANDLE_BASE_H_


#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>


#include <DNBSystem.h>
#include <DNBSysBodyBase.h>
#include <DNBCountedPointer.h>


class ExportedByDNBSystem DNBSysHandleBase
{
public:

	bool
    operator==(const DNBSysHandleBase& rhs) const
    DNB_THROW_SPEC_ANY;

	bool
    operator<(const DNBSysHandleBase& rhs) const
    DNB_THROW_SPEC_ANY;

	bool
    operator!=(const DNBSysHandleBase& rhs) const
    DNB_THROW_SPEC_ANY;

	bool
    isValid(void) const
        DNB_THROW_SPEC_ANY;
   
   protected:

	DNBSysHandleBase()
        DNB_THROW_SPEC_ANY;

	DNBSysHandleBase(DNBSysBodyBase* body)
        DNB_THROW_SPEC_ANY;

	DNBSysHandleBase(const DNBSysHandleBase& rhs)
        DNB_THROW_SPEC_ANY;

	~DNBSysHandleBase(void)
        DNB_THROW_SPEC_ANY;

	DNBSysHandleBase&
      operator=(const DNBSysHandleBase& rhs)
        DNB_THROW_SPEC_ANY;      


	DNBSysBodyBase&
      body(void) const
      DNB_THROW_SPEC_ANY;

protected:
      
    DNBCountedPointer<DNBSysBodyBase> body_;

};




//
//  Include the public definition file.
//
//#include "DNBSysHandleBase.cc"




#endif  /* _DNB_SYSHANDLE_BASE_H_ */
