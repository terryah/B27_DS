//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
//
// DNBSysThreadAttribute.h
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     smw        01/26/2005   Initial Implementation
//
//==============================================================================


#ifndef DNBSysThreadAtribute_H
#define DNBSysThreadAttribute_H


#include <DNBSystem.h>
#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>
#include <DNBSysThreadDefs.h>
#include <DNBSysLockGuard.h>	
#include <DNBSysFastMutexLock.h>
#include <DNBSystemTrace.h>
#include <DNBException.h>


/**
  * The DNBSysThreadAttribute class groups all the different attributes of a thread.  
  * A thread is created with an instance of this class; If no instance is specified
  * during creation of the thread, the default instance is used.  Once a thread is
  * created, changing the thread attribute structure does not change the thread's 
  * behavour.  However, it is possible to change a thread's priority by a direct method
  * call to the thread itself.
  *	Currently, there are significant differences between Unix's pthreads
  * thread attributes and win32 thread attributes.  In fact, the intersection between 
  * the two sets of thread attributes is empty!
  * In Summary:
  *	Win32 threads
  * -------------
  * have the following attributes which can be specified at thread 
  * creation time: 
  *		initial thread "committed" stack size ( note: this is not the thread stack size )
  *		security attributes:
  *		createSuspended: boolean flag specifying wether the thread is to run immediately 
  *     after creation or to be in suspended mode until resumeThread is called.
  *		Thread priority is not a parameter that can be given upon creation, but a call 
  *     can be made on the thread itself to change its priority after creation
  *   
  *
  * pthreads
  * ---------
  * The pthreads pthread_attr_t is well documented and provides for the following
  * thread attributes: detach state; stack address; stack size; contention scope;
  * scheduling policy; scheduling parameters ( mainly priority ); and inheritance of 
  * scheduling parameters;
  * 
  * Therefore, the only attribute that can be made to work on both is thread priority
  *
  */



/*****************************************************************************************/
/* BEGIN CLASS DNBSysThreadAttribute													 */
/*****************************************************************************************/


class ExportedByDNBSystem DNBSysThreadAttribute
{

public:


/*****************************************************************************************/
/*  Singleton instance of default DNBSysThreadAttribute									 */
/*****************************************************************************************/

// FIXME: This singleton instance doesn't realy serve any puporse, since each thread is created 
// with a copy of a DNBSysThreadAttribute instance.  We could have DNBSysThreadAttribute 
// be reference counted instead.  Then each thread would be created with a handle to the
// singleton default attribute object.  If a thread wishes to change its thread attributes,  then it
// would need to create a new thread attributes object and set its handle to itself
// With the current implementation, you can create a thread with default thread attributes 
// and then just call the appropriate methods to change the attribute values
	static
	DNBSysThreadAttribute*
	Default() 
		DNB_THROW_SPEC ( ( DNBEResourceLimit ) );

/*****************************************************************************************/
/*  Constructor and destructor															 */
/*****************************************************************************************/


	/* 
	 * Constructor
	 * Constructs a DNBSysThreadAttribute with default attribute values:
	 */
	DNBSysThreadAttribute()  
		DNB_THROW_SPEC ( ( DNBEResourceLimit ) ) ;


/*****************************************************************************************/

	/* 
	 * Copy constructor
	 */
	DNBSysThreadAttribute( const DNBSysThreadAttribute& another )  
		DNB_THROW_SPEC ( ( DNBEResourceLimit ) );


/*****************************************************************************************/
	/* 
	 * Assignment Operator
	 */
	void
	operator= ( const DNBSysThreadAttribute& another )  
		DNB_THROW_SPEC_NULL ;


/*****************************************************************************************/


	/*
	 * Destructor
	 */
	~DNBSysThreadAttribute()
		DNB_THROW_SPEC_NULL;


/*****************************************************************************************
*  Getter and setter methods; add getter and setter nethods for default values?
******************************************************************************************
* These methods either succeed ( if the operating system supports the functionality )
* or throw an exception
*****************************************************************************************/


	DNBSysDetachState 
	getDetachState() const
		DNB_THROW_SPEC ( ( DNBEInternalError ) );



/*****************************************************************************************/


	void
	setDetachState( DNBSysDetachState state )
		DNB_THROW_SPEC_NULL;


/*****************************************************************************************/

	void*
	getStackAddress() const 
		DNB_THROW_SPEC ( ( DNBEInternalError, DNBENotSupported ) ) ;


/*****************************************************************************************/

	void 
	setStackAddress( void* address )
		DNB_THROW_SPEC ( ( DNBEInternalError, DNBENotSupported ) ) ;


/*****************************************************************************************/
/* Getter and setter for thread stack size
/* Note: stack size parameter can be given at thread creation time on Win32.
/* However, in that context, that parametr controls the initial "committed"
/* thread stack size, and this grows as needed ( by committing more pages from
/* reserved memory ) up to the limit of 1MB default ( changeable only by editing 
/* STACKSIZE element in the .DEF file or using a linker option.)
/* Whereas the pthreads stack size attribute is the actual thread stack size
/* **************************************************************************************/
	size_t
	getStackSize() const
		DNB_THROW_SPEC ( ( DNBEInternalError, DNBENotSupported ) ) ;


/*****************************************************************************************/


	void
	setStackSize( size_t size )
		DNB_THROW_SPEC ( ( DNBEInternalError, DNBENotSupported, DNBERangeError ) ) ;


/*****************************************************************************************/

	
	DNBSysInheritScheduling
	getInheritScheduling() const
		DNB_THROW_SPEC ( ( DNBEInternalError, DNBENotSupported ) ) ;


/*****************************************************************************************/
	

	void
	setInheritScheduling( DNBSysInheritScheduling inheritSched )
		DNB_THROW_SPEC ( ( DNBEInternalError, DNBENotSupported ) ) ;


/*****************************************************************************************/


    DNBSysThreadPriority
    getPriority () const 
		DNB_THROW_SPEC_NULL;


/*****************************************************************************************/


    DNBSysThreadPriority
    getMaximumPriority () const 
		DNB_THROW_SPEC ( ( DNBENotSupported, DNBEInternalError ) ) ;


/*****************************************************************************************/


    DNBSysThreadPriority
    getMinimumPriority () const 
		DNB_THROW_SPEC ( ( DNBENotSupported , DNBEInternalError ) ) ;


/*****************************************************************************************/


	void 
	setPriority( DNBSysThreadPriority priority )
		DNB_THROW_SPEC ( ( DNBEInternalError, DNBENotSupported ) ) ;


/*****************************************************************************************/


	DNBSysSchedulingPolicy
	getSchedulingPolicy() const
		DNB_THROW_SPEC ( ( DNBEInternalError, DNBENotSupported ) ) ;


/*****************************************************************************************/


	void
	setSchedulingPolicy( DNBSysSchedulingPolicy policy )
		DNB_THROW_SPEC ( ( DNBEInternalError, DNBENotSupported ) ) ;


/*****************************************************************************************/


	DNBSysContentionScope 
	getContentionScope() const
		DNB_THROW_SPEC ( ( DNBEInternalError, DNBENotSupported ) );


/*****************************************************************************************/


	void
	setContentionScope( DNBSysContentionScope scope )
		DNB_THROW_SPEC ( ( DNBEInternalError, DNBENotSupported ) ) ;


#if DNB_HAS_WIN32_THREADS


/*****************************************************************************************/


	LPSECURITY_ATTRIBUTES 
	getSecurityAttributes() const
		DNB_THROW_SPEC_NULL;
  
	
/*****************************************************************************************/


	void
	setSecurityAttributes ( LPSECURITY_ATTRIBUTES securityAttributes )
		DNB_THROW_SPEC_NULL ;


/*****************************************************************************************/

	
	size_t
    getInitialCommittedStackSize() const
		DNB_THROW_SPEC_NULL;


/*****************************************************************************************/


	void
	setInitialCommittedStackSize( size_t initialCommittedStackSize )
		DNB_THROW_SPEC_NULL;


/*****************************************************************************************/


	DWORD
    getCreateSuspendedFlag() const
		DNB_THROW_SPEC_NULL;


/*****************************************************************************************/


	void
	setCreateSuspendedFlag( DWORD createSuspended )
		DNB_THROW_SPEC_NULL;


/*****************************************************************************************/


#endif // DNB_HAS_WIN32_THREADS




/*****************************************************************************************/
/* Functionality specific to POSIX
/*****************************************************************************************/

#	if DNB_HAS_POSIX_THREADS

	pthread_attr_t*
	getPthread_attr_t()
		DNB_THROW_SPEC_NULL;


	sched_param*
	getSchedParam ()
		DNB_THROW_SPEC_NULL;

#	endif




private:


#	if DNB_HAS_POSIX_THREADS

		pthread_attr_t			attribute_;
		sched_param			sched_param_;

#	elif DNB_HAS_WIN32_THREADS

		DWORD					initialCommittedStackSize_;
		LPSECURITY_ATTRIBUTES	securityAttributes_;
		DWORD					createSuspended_;
		DNBSysThreadPriority	priority_;
		DNBSysDetachState		detachState_;

#	else

#		error Unsupported environment

#	endif

		// pointer to singleton instance of default DNBSysThreadAttribute 
		static DNBSysThreadAttribute * defaultAttribute; 

		// Mutex to protect the creation of the singleton
		static DNBSysFastMutexLock mutex_;


/*****************************************************************************************/
/* END OF CLASS DNBSysThreadAttribute												 	 */
/*****************************************************************************************/

};


#endif /* DNBSysThreadAttribute_H */
