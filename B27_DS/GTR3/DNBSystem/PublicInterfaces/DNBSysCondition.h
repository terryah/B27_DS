//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
//
// DNBSysCondition.h
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     smw        12/20/2003   Initial Implementation

// NOTES:
// The implemenation of condition variables on Unix is straightforward.
// The impelementation on Windows is more complex, as explained below.  
// There are two alternative implementations on Windows, both are adapted from the 
// following refrence:
// Douglas C. Schmidt and Irfan Pyarali, "Strategies for Implementing POSIX Condition 
//                  Variables on Win32", C++ Report, SIGS, Vol. 10, No. 5, June, 1998. 
// The first approach is what is termed the SetEvent solution in that paper.  This is
// the more efficient but not perfectly fair solution.  The second approach is the 
// SignalObjectAndWait solution, which is less efficient but more fair.
// One or the other approch can be enables through the definition of the macro
// DNB_CONDITION_MORE_FAIR to be 1 or 0

//==============================================================================
#ifndef DNBSysCondition_H
#define DNBSysCondition_H


#include <DNBSystem.h>
#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>
#include <DNBSysThreadDefs.h>
#include <DNBSysICondition.h>
#include <DNBSysMutexLock.h>

#define DNB_CONDITION_MORE_FAIR 1

#if DNB_HAS_WIN32_THREADS

#if DNB_CONDITION_MORE_FAIR   // The SignalObjectAndWait approach
// There is currently an issue with this approach.  SignalObjectAndWait is used 
// in the wait() method to atomically: 1) release the "external" mutex, a necessary 
// step in implementing condition variables and 2) wait for a semaphore.
// The mutex which is used in DNBSysCondition is a DNBSysMutexLock, which internally 
// has a win 32 mutex ( i.e. type HANDLE).
// However, SignalObjectAndWait releases the internal mutex (i.e. the HANDLE) 
// associated with the DNBSysMutexLock, but not
// the actual DNBSysMutexLock. In other words, the release() method of the 
// DNBSysMutexLock is never called, which is necessary to allow others to re-acquire it.
// Therefore the ODT hangs in a deadlock.  This approach is currently disabled.
// ==> Change the implementation of DNBSysMutexLock so that it acquires the internal
// lock only in non-recursive mode, for error checking.
// Leave the implementation as is for DNBCriticalSection.
// The only problem with this solution is that a DNBSysCondition based on the more fair approach
// requires the associated external mutex to be recursive.

typedef struct
{
    HANDLE waiters_done_;                // An auto-reset event used by the broadcasting/
                                        // signaling thread to wait for all the waiting 
                                        // thread(s) to wake up and be released from the 
                                        // semaphore. 

    size_t waiters_count_;            // Count of the number of waiters on this condition
    CRITICAL_SECTION waiters_count_lock_; // serialize access to above count

    HANDLE sema_;                       // Semaphore used to queue up threads waiting for 
                                        // the condition to become signaled. 
    size_t was_broadcast_;              //  Keeps track of whether we were broadcasting or 
                                        // signaling.  This allows us to optimize the code 
                                        // if we're just signaling.

} NativeConditionRep;

typedef DNBSysMutexLock MutexLockRep;  // FIXME: probably not needed; here and in other cases, it is the same

#else // The SetEvent approach

typedef struct
{
    enum { SIGNAL = 0,                  // for auto reset event
           BROADCAST = 1,               // for manual reset event
           EVENT_TYPES = 2
    };
    HANDLE events_[EVENT_TYPES];        // One manual reset event and one auto-reset event
    size_t waiters_count_;            // Count of the number of waiters on this condition
    CRITICAL_SECTION waiters_count_lock_; // serialize access to above count

} NativeConditionRep;

typedef DNBSysMutexLock MutexLockRep;


#endif

//-------------------------------------------------------------------------------------------

#else  // i.e.  DNB_HAS_POSIX_THREADS

typedef pthread_cond_t NativeConditionRep;

typedef DNBSysMutexLock MutexLockRep;


#endif

//------------------------------------------------------------------------------------------
// COMMON IMPLEMENTATION
//------------------------------------------------------------------------------------------
class ExportedByDNBSystem DNBSysCondition: public DNBSysICondition
{
public:
     DNBSysCondition(MutexLockRep& lock ); 

    ~DNBSysCondition( );

    void 
    signal(void);

    void 
    signalAll(void);

    void
    wait(void);

    DNBSysIMutexLock * getLock() { return lock_;};

protected:

    DNBSysCondition(DNBSysCondition&);
    DNBSysCondition& operator=(DNBSysCondition&);

private:

    NativeConditionRep condition_;
    MutexLockRep * 	lock_;      

};


#endif  // DNBSysCondition_H
