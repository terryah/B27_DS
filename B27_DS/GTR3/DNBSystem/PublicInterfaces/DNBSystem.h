//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/** @CAA2Required */
/*---------------------------------------------------------------------*/
/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */
/*---------------------------------------------------------------------*/
//
//    This Header file is included by all the Module.h header files in 
//    PublicInterfaces
//

#ifndef DNBSYSTEM_H

#define DNBSYSTEM_H DNBSystem

#ifdef _WINDOWS_SOURCE
#if defined(__DNBSystem)
#define ExportedByDNBSystem __declspec(dllexport)
#else
#define ExportedByDNBSystem __declspec(dllimport)
#endif
#else
#define ExportedByDNBSystem
#endif

#endif /* DNBSYSTEM_H */
