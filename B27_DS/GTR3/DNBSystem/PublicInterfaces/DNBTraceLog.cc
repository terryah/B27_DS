/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview rtl bkh 03:11:01
 * @quickReview BKH 03:11:07
 * @quickReview SHA 03:11:24
 */
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//*
//* FILE:
//*     TraceLog.cc    - private macros to output trace message of specified level.
//*                     -not to be used in client code.
//*
//* MODULE:
//*     TraceLog       - 
//*
//* OVERVIEW:
//*    Private macros to output trace message of specified level.
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     BKH        10/02/03    Implementation
//*     RTL        11/24/03    Fix compilation errors(due to inclusion of
//*                            DNBStandardLib.h )
//*     
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1997-99 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*

//#include <DNBStandardLib.h>
#include <DNBTraceLog.h>

#define _DNB_TRACE_IMPL0(_LOG, _MSG, _LVL) do {                             \
    if ( (_LOG).detLevel(_LVL)) {                                    \
        (_LOG).acquireLock();                                                \
        (_LOG).write((_LVL), __FILE__, __LINE__) << (_MSG) << endl;         \
        (_LOG).releaseLock();                                              \
    }                                                                       \
} while (0)

#define _DNB_TRACE_IMPL1(_LOG, _MSG, _A1, _LVL) do {                        \
    if ( (_LOG).detLevel(_LVL)) {                                    \
        (_LOG).acquireLock();                                                \
        (_LOG).write((_LVL), __FILE__, __LINE__) << (_MSG)                  \
            << " " << (_A1) << endl;                                        \
        (_LOG).releaseLock();                                              \
    }                                                                       \
} while (0)

#define _DNB_TRACE_IMPL2(_LOG, _MSG, _A1, _A2, _LVL) do {                   \
    if ( (_LOG).detLevel(_LVL)) {                                    \
        (_LOG).acquireLock();                                                \
        (_LOG).write((_LVL), __FILE__, __LINE__) << (_MSG)                  \
            << " " << (_A1) << " " << (_A2) << endl;                        \
        (_LOG).releaseLock();                                              \
    }                                                                       \
} while (0)

#define _DNB_TRACE_IMPL3(_LOG, _MSG, _A1, _A2, _A3, _LVL) do {              \
    if ( (_LOG).detLevel(_LVL)) {                                    \
        (_LOG).acquireLock();                                                \
        (_LOG).write((_LVL), __FILE__, __LINE__) << (_MSG)                  \
            << " " << (_A1) << " " << (_A2) <<" " << (_A3) <<  endl;        \
        (_LOG).releaseLock();                                              \
    }                                                                       \
} while (0)

#define _DNB_TRACE_IMPL4(_LOG, _MSG, _A1, _A2, _A3, _A4, _LVL) do {         \
    if ( (_LOG).detLevel(_LVL)) {                                    \
        (_LOG).acquireLock();                                                \
        (_LOG).write((_LVL), __FILE__, __LINE__) << (_MSG)                  \
            << " " << (_A1) << " " << (_A2) <<" " << (_A3)                  \
            << " " << (_A4) <<  endl;                                       \
        (_LOG).releaseLock();                                              \
    }                                                                       \
} while (0)

