//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/*********************************************************************************************/
//
// DNBSysThread.h
//
/*********************************************************************************************/
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     smw        07/13/2004   Initial Implementation
//
/*********************************************************************************************/


#ifndef DNBSysThread_H
#define DNBSysThread_H


#include <DNBSystem.h>
#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>
#include <DNBSysThreadDefs.h>
#include <DNBSysIRunnable.h>
#include <DNBSysThreadId.h>
#include <DNBSysMonitor.h>
#include <DNBSysLockGuard.h>
#include <DNBSysUnlockGuard.h>
#include <DNBSysFastMutexLock.h>
#include <DNBCountedObject.h>
#include <DNBCountedPointer.h>
#include <DNBSysThreadAttribute.h>
#include <DNBSysRunnableFunctor.h> // for convenience "make" functions 
#include <DNBException.h>



#if DNB_HAS_WIN32_THREADS

    typedef HANDLE         DNBNativeThreadRep;

#elif DNB_HAS_POSIX_THREADS

    typedef pthread_t     DNBNativeThreadRep;

#else

#	error "Unsupported / undefined threading environment"

#endif // DNB_HAS_POSIX_THREADS



/*****************************************************************************************/
// Class DNBSysThreadBody
/*****************************************************************************************/


class ExportedByDNBSystem DNBSysThreadBody : public DNBSysMonitor, public DNBCountedObject
{
 
public:
    

/*****************************************************************************************/
// Constructors, destructor, assigment operator
/*****************************************************************************************/


    /*
     * Constructs a DNBSysThread object with the given runnable.  No native thread is created
     * until the start method is issued.  The default thread attibutes will be used.
	 * For information on the default values, see the DNBSysThreadAttribute class.
     */
 
    DNBSysThreadBody ( DNBSysIRunnable runnable ) 
					   DNB_THROW_SPEC_NULL;


	/*
	 * Constructor; same as above but uses the specified thread attribute object.
	 */
    DNBSysThreadBody ( DNBSysIRunnable runnable, 
					   const DNBSysThreadAttribute & attribute ) 
					   DNB_THROW_SPEC_NULL;



    /* Destructor; 
	 * Unless this is a detached thread on Unix, then we have to wait until the native thread 
	 * is finished doing its job before destructing this object
	 */
    ~DNBSysThreadBody()
		DNB_THROW_SPEC_NULL ;  
// cannot throw  exceptions because aix complains about that it is less restrtictive throw spec
// then DNBCountedObject
  
    
/*****************************************************************************************/    
// Getters and setters
// Should consider using ReadLockGuards for simple reads  ( ie getters )
/*****************************************************************************************/


    const DNBNativeThreadRep 
    getNativeThreadRep() const
		DNB_THROW_SPEC_NULL;
  
	
/*****************************************************************************************/


    DNBSysIRunnable 
    getRunnable() const
		DNB_THROW_SPEC_NULL;


/*****************************************************************************************/


    bool 
    setRunnable ( DNBSysIRunnable aRunnable)
		DNB_THROW_SPEC_NULL;


/*****************************************************************************************/


    DNBSysThreadPriority 
    getPriority () const
		DNB_THROW_SPEC_NULL;


/*****************************************************************************************/


	/*
	 *  Maximum Priority on Unix is specific to the scheduling policy assigned to the thread.
	 *  This is why this method is not static
	 */
    DNBSysThreadPriority 
    getMaximumPriority () const
		DNB_THROW_SPEC_NULL;


/*****************************************************************************************/

	/*
	 *  Minimum priority on Unix is specific to the scheduling policy assigned to the thread.
	 *  This is why this method is not static
	 */
    DNBSysThreadPriority 
    getMinimumPriority () const
		DNB_THROW_SPEC_NULL;


/*****************************************************************************************/


    void 
    setPriority ( DNBSysThreadPriority priority )
		DNB_THROW_SPEC_NULL;


/*****************************************************************************************/


    DNBSysExecutionState
	getState() const
		DNB_THROW_SPEC_NULL;



/*****************************************************************************************/


    const DNBSysThreadAttribute&
    getAttribute ()
		DNB_THROW_SPEC_NULL;


/*****************************************************************************************/


    void
    setAttribute ( const DNBSysThreadAttribute& attribute )
		DNB_THROW_SPEC_NULL;


/*****************************************************************************************/


    const DNBSysThreadId& 
    getThreadId() const
		DNB_THROW_SPEC_NULL;


/*****************************************************************************************/
// For convenience, interrogate any of all possible stated a DNBSysThread object ( not the
//	native os thread ) can be in
/*****************************************************************************************/

/* Here is the state diagram of a DNBSysThread, in the form state1 -> ( event ) -> state2
   Upon creation, a DNBSysThread is Idle.
   FIXME: may not be comprehensive; finish and double check code handles all transitions

  Idle -> ( start ) -> Running
  Idle -> ( cancel ) -> Idle
  Running -> ( suspend ) -> Suspended
  Running -> ( cancel ) -> Idle
  Running -> ( native os thread returns ) -> Idle
  Suspended -> ( cancel ) -> Idle
  Suspended -> ( resume ) -> Running

*/

/*****************************************************************************************/

	
	bool
	isRunning() const
		DNB_THROW_SPEC_NULL;


/*****************************************************************************************/


	bool
	isIdle() const
		DNB_THROW_SPEC_NULL;



/*****************************************************************************************/


	bool
	isSuspended() const
		DNB_THROW_SPEC_NULL;



/*****************************************************************************************/
// Lifecycle methods
/*****************************************************************************************/

	/* 
	 * This method actually creates the native os thread
	 */

    void
    start( ) 
		DNB_THROW_SPEC ( ( DNBEResourceLimit, DNBEInternalError ) );



/*****************************************************************************************/


    // POSIX does not provide a suspend. But may be available in vendor's debugging interface
    // ( e.g. libthread_db.so for Solaris ).
    // " (Behaviour of Suspend ) is often emulated through signals.  Typically SIGUSR1 will be used for this 
    // purpose in Common C++ applications, depending in the target platform.   On solaris, the
    // Solaris thread library supports suspend and resume directly." ( extract from comments by
    // Open Source Telecom Corporation.

    bool 
    suspend() 
		DNB_THROW_SPEC ( (DNBELogicError ) );


/*****************************************************************************************/


    void 
    resume () 
		DNB_THROW_SPEC ((DNBELogicError));



/*****************************************************************************************/


    bool 
    join ()
		DNB_THROW_SPEC( ( DNBEDeadlock, DNBEInternalError ) );

// FIXME: consider having another join method which returns, through a pointer argument,
// the thread's exit value ( no others have done this );
// Also, consider a join with a timeout value


/*****************************************************************************************/


    bool
    cancel ()
		DNB_THROW_SPEC_NULL;


/*****************************************************************************************/

    
private:
    
    DNBNativeThreadRep 		thread_;
    DNBSysThreadId			threadId_;
    DNBSysIRunnable 		runnable_;  // The alternative to caching the runnable is to pass it as
										// an argument to start() method.  In this case, it will also 
										// have to be the argument to the os_thread_function
	DNBSysThreadAttribute	attribute_;
    DNBSysExecutionState	state_;



	// Prohibit copy construction and assignment operator
	/* 
	 * Copy constructor
	 */
	DNBSysThreadBody ( DNBSysThreadBody& another );


	/*
	 * Assignment operator
	 */
    void
    operator=( DNBSysThreadBody& second );   

};


/*****************************************************************************************/
// end of class DNBSysThreadBody
/*****************************************************************************************/




/*****************************************************************************************/
// Typedef for handle body idiom
/*****************************************************************************************/

typedef DNBCountedPointer<DNBSysThreadBody> DNBSysThread;



/*****************************************************************************************/
// Global functions
/*****************************************************************************************/
    


// Global function wrapping the system sleep call for platform independence
void 
ExportedByDNBSystem
DNBSysThreadSleep ( unsigned long milliseconds );


// Global function yield
void 
ExportedByDNBSystem
DNBSysThreadYield ();



// Note: These methods are on the counted pointer ( i.e. handle instantiation over 
// DNBSysThreadBody
bool
ExportedByDNBSystem
operator==( DNBSysThread first, DNBSysThread second );



bool
ExportedByDNBSystem
operator<( DNBSysThread first, DNBSysThread second ) ;







/*****************************************************************************************/
// Make function for more straightforward thread creation
/*****************************************************************************************/

DNBSysThread
ExportedByDNBSystem
DNBMakeThread ( DNBFunctor0 functor );

// Make a thread from a global function with no return and no args
template < class functionType >
DNBSysThread
DNBMakeThreadG ( functionType function );


// Make a thread from a global function with no return and takes 1 arg
template < class functionType, class arg1Type >
DNBSysThread
DNBMakeThreadG ( functionType function, arg1Type arg1 );


// Make a thread from a global function with no return and takes 2 arg
template < class functionType, class arg1Type , class arg2Type >
DNBSysThread
DNBMakeThreadG ( functionType function, arg1Type arg1, arg2Type arg2 );

// etc..

// Make threads from a class member functions

template< class Client, class Member >
DNBSysThread
DNBMakeThreadM ( Client* client, Member member );



// include the definition of the above templated functions only 

#include <DNBSysThread.cc>


#endif  /* DNBSysThread_H */

