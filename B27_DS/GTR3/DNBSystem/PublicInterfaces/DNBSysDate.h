//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//
//
//
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNBSYSDATE_H_
#define _DNBSYSDATE_H_


#include <DNBSystemBase.h>
#include <scl_iosfwd.h>                 // for ostream
#include <scl_string.h>                 // for scl_string, scl_wstring
#include <scl_ctime.h>                  // for time_t, struct tm
#include <DNBSystemDefs.h>


#include <DNBSystem.h>




//
//  This class represents an absolute date without reference to a particular
//  calendar system.  Internally, the date is expressed as a Julian day number,
//  which characterizes the number of days elapsed since January 1, 4713 BC.
//  In accordance with common practice, this class provides an interface to set
//  and extract date information using the Gregorian calendar notations of day,
//  month, and year.  The class can be used to store dates in other calendar
//  systems, but those systems will have to convert to/from the internal
//  representation.
//
//  Date range: Jan 01 4713 BC to Dec 31 9999 AD.
//
//  The class allows the conversion to/from:
//    o integer number of days from some epoch
//    o day, month, year, day of year, and weekday components
//    o ANSI C 'time_t' and 'struct tm' objects
//
//  Note: Years AD are positive, while years BC are negative.  There is no year
//  zero (0); it goes from 1 BC to 1 AD.
//
class ExportedByDNBSystem DNBSysDate
{
public:
    //
    //  Enumerated list of Gregorian weekdays.
    //
    enum DayOfWeekEnum
    {
        Sunday = 1,
        Monday,
        Tuesday,
        Wednesday,
        Thursday,
        Friday,
        Saturday
    };

    //
    //  Enumerated list of Gregorian months.
    //
    enum MonthEnum
    {
        January = 1,
        February,
        March,
        April,
        May,
        June,
        July,
        August,
        September,
        October,
        November,
        December
    };

    //
    //  Enumerated list of Calendar eras.
    //
    enum EraEnum
    {
        // Secular form
        BCE   = 0,                      // Before Common Era
        CE    = 1,                      // Common Era

        // Christian form
        BC    = 0,                      // Before Christ (alias of BCE)
        AD    = 1                       // Anno Domini   (alias of CE)
    };

    //
    //  Date field limits.
    //
    enum
    {
        JulianDayMin  =        0,       // Jan 01 4713 BC
        JulianDayMax  =  5373484,       // Dec 31 9999 AD
        DayOfYearMin  =        1,       // Jan 01
        DayOfYearMax  =      366,       // Dec 31 (leap year)
        DayOfWeekMin  =   Sunday,       // First day of week
        DayOfWeekMax  = Saturday,       // Last day of week
        DayOfMonthMin =        1,       // First day of month
        DayOfMonthMax =       31,       // Maximum number of days
        MonthMin      =  January,       // First month
        MonthMax      = December,       // Last month
        YearMin       =    -4713,       // Earliest year is 4713 BC
        YearMax       =     9999,       // Latest year is 9999 AD
        EraMin        =      BCE,       // Before Common Era
        EraMax        =       CE,       // Common Era
        InvalidDate   =        0        // Invalid day, month, year
    };

    struct DateInfo
    {
        int     JulianDay;              // Days since Jan 01 4713 BC
        int     DayOfYear;              // 1 to 365; 366 if leap year
        int     DayOfWeek;              // 1 to 7; see DayOfWeekEnum
        int     DayOfMonth;             // 1 to 31
        int     Month;                  // 1 to 12; see MonthEnum
        int     Year;                   // -4713 to 9999; 0 is invalid
        int     Era;                    // BCE or CE; see EraEnum
        bool    LeapYear;               // True if leap year
    };

    enum DateFormat
    {
        MMDDYYYY,
        DDMMYYYY,
        YYYYMMDD
    };

    //
    //  Constructors, assignment operators, and destructor.
    //

    // Default constructor.  Constructs an invalid date.
    DNBSysDate( )
        DNB_THROW_SPEC_NULL;

    // Copy constructor.
    DNBSysDate( const DNBSysDate &right )
        DNB_THROW_SPEC_NULL;

    // Constructs <self> given a Julian day number.
    explicit
    DNBSysDate( int jday )
        DNB_THROW_SPEC_NULL;

    // Constructs <self> given a day of year and year number.
    DNBSysDate( int yday, int year )
        DNB_THROW_SPEC_NULL;

    // Constructs <self> given a day of month, month number, and year number.
    DNBSysDate( int mday, int month, int year )
        DNB_THROW_SPEC_NULL;

    // Assignment operator.
    DNBSysDate &
    operator=( const DNBSysDate &right )
        DNB_THROW_SPEC_NULL;

    // Destructor.
    ~DNBSysDate( )
        DNB_THROW_SPEC_NULL;

    // Clears the value of <self> resulting in an invalid date.
    void
    clear( )
        DNB_THROW_SPEC_NULL;

    //
    //  Mutator methods.
    //

    // Initializes <self> given a Julian day number.
    bool
    setDate( int jday )
        DNB_THROW_SPEC_NULL;

    // Initializes <self> given a day of year and year number.
    bool
    setDate( int yday, int year )
        DNB_THROW_SPEC_NULL;

    // Initializes <self> given a day of month, month number, and year
    // number.
    bool
    setDate( int mday, int month, int year )
        DNB_THROW_SPEC_NULL;

    // Initializes <self> given a day of month, full or abbreviated month
    // name, and year number.
#if 0
    bool
    setDate( int mday, const scl_string &month, int year )
        DNB_THROW_SPEC_NULL;

    bool
    setDate( int mday, const scl_wstring &month, int year )
        DNB_THROW_SPEC_NULL;
#endif

    // Initializes <self> from an ANSI C time object, using the fields tm_mday,
    // tm_mon, and tm_year.
    bool
    setCTime( const struct tm &stime )
        DNB_THROW_SPEC_NULL;

    // Initializes <self> from an ANSI C time value.
    bool
    setCTime( const time_t &ctime )
        DNB_THROW_SPEC_NULL;

    //
    //  Accessor methods.
    //

    // Returns the Julian day number corresponding to <self>.
    void
    getDate( int &jday ) const
        DNB_THROW_SPEC_NULL;

    // Returns the day of year and year number corresponding to <self>.
    void
    getDate( int &yday, int &year ) const
        DNB_THROW_SPEC_NULL;

    // Returns the day of month, month number, and year number corresponding
    // to <self>.
    void
    getDate( int &mday, int &month, int &year ) const
        DNB_THROW_SPEC_NULL;

    // Returns the day of month, full month name, and year number corresponding
    // to <self>.
    void
    getDate( int &mday, scl_string &month, int &year ) const
        DNB_THROW_SPEC_NULL;

    void
    getDate( int &mday, scl_wstring &month, int &year ) const
        DNB_THROW_SPEC_NULL;

    // Returns the ANSI C time object corresponding to <self>.  The time fields
    // specify the local time corresponding to midnight UTC.
    void
    getCTime( struct tm &stime ) const
        DNB_THROW_SPEC_NULL;

    // Returns the ANSI C time value corresponding to <self>.
    void
    getCTime( time_t &ctime ) const
        DNB_THROW_SPEC_NULL;

    // Returns the date parameters corresponding to <self>.
    void
    getDate( DateInfo &info ) const
        DNB_THROW_SPEC_NULL;

    //
    //  Simple accessor methods.
    //

    // Returns the Julian day number corresponding to <self>.
    int
    getJulianDay( ) const
        DNB_THROW_SPEC_NULL;

    // Returns the day of year corresponding to <self>.
    int
    getDayOfYear( ) const
        DNB_THROW_SPEC_NULL;

    // Returns the day of week corresponding to <self>.
    int
    getDayOfWeek( ) const
        DNB_THROW_SPEC_NULL;

    // Returns the day of month corresponding to <self>.
    int
    getDayOfMonth( ) const
        DNB_THROW_SPEC_NULL;

    // Returns the month corresponding to <self>.
    int
    getMonth( ) const
        DNB_THROW_SPEC_NULL;

    // Returns the year corresponding to <self>.
    int
    getYear( ) const
        DNB_THROW_SPEC_NULL;

    // Returns the era (BCE, CE) corresponding to <self>.
    int
    getEra( ) const
        DNB_THROW_SPEC_NULL;

    // Returns the week number corresponding to <self>, where <wday> is the
    // first day of the week.
    int
    getWeekOfYear( int wday = Sunday ) const
        DNB_THROW_SPEC_NULL;

    // Returns true if <self> is a valid date.
    bool
    isValid( ) const
        DNB_THROW_SPEC_NULL;

    // Returns true if the year corresponding to <self> is a leap year.
    bool
    isLeapYear( ) const
        DNB_THROW_SPEC_NULL;

    scl_string
    toString( char format = 'x' ) const
        DNB_THROW_SPEC_NULL;

    scl_wstring
    toStringW( wchar_t format = L'x' ) const
        DNB_THROW_SPEC_NULL;

    //
    //  Relational operators.
    //
    bool
    operator==( const DNBSysDate &right ) const
        DNB_THROW_SPEC_NULL;

    bool
    operator!=( const DNBSysDate &right ) const
        DNB_THROW_SPEC_NULL;

    bool
    operator< ( const DNBSysDate &right ) const
        DNB_THROW_SPEC_NULL;

    bool
    operator<=( const DNBSysDate &right ) const
        DNB_THROW_SPEC_NULL;

    bool
    operator> ( const DNBSysDate &right ) const
        DNB_THROW_SPEC_NULL;

    bool
    operator>=( const DNBSysDate &right ) const
        DNB_THROW_SPEC_NULL;

    //
    //  Arithmetic operators.
    //

    // Adds one day to <self> and returns the new date.
    DNBSysDate &
    operator++( )
        DNB_THROW_SPEC_NULL;

    // Subtracts one day from <self> and returns the new date.
    DNBSysDate &
    operator--( )
        DNB_THROW_SPEC_NULL;

    // Adds one day to <self> and returns the original date.
    DNBSysDate
    operator++( int )
        DNB_THROW_SPEC_NULL;

    // Subtracts one day from <self> and returns the original date.
    DNBSysDate
    operator--( int )
        DNB_THROW_SPEC_NULL;

    // Adds <count> days to <self> and returns the result.
    DNBSysDate &
    operator+=( int count )
        DNB_THROW_SPEC_NULL;

    // Adds <count> days to <self> and returns the result.
    DNBSysDate &
    addDays( int count )
    DNB_THROW_SPEC_NULL { return operator+=(count); };


    // Subtracts <count> days from <self> and returns the result.
    DNBSysDate &
    operator-=( int count )
        DNB_THROW_SPEC_NULL;

    // Adds <count> weeks to <self> and returns the result.
    DNBSysDate &
    addWeeks( int count )
        DNB_THROW_SPEC_NULL;

    // Adds <count> months to <self> and returns the result.
    DNBSysDate &
    addMonths( int count )
        DNB_THROW_SPEC_NULL;

    // Adds <count> years to <self> and returns the result.
    DNBSysDate &
    addYears( int count )
        DNB_THROW_SPEC_NULL;

    // Returns the next date that falls on weekday <wday>.
    DNBSysDate
    nextDate( int wday ) const
        DNB_THROW_SPEC_NULL;

    // Returns the previous date that falls on weekday <wday>.
    DNBSysDate
    prevDate( int wday ) const
        DNB_THROW_SPEC_NULL;

    //
    //  Static methods.
    //

    // Returns a date object with today's date.
    static  DNBSysDate
    Today( )
        DNB_THROW_SPEC_NULL;

    // Returns the minimum representable date.
    static  DNBSysDate
    DateMin( )
        DNB_THROW_SPEC_NULL;

    // Returns the maximum representable date.
    static  DNBSysDate
    DateMax( )
        DNB_THROW_SPEC_NULL;

    static  scl_string
    MonthName( int month, bool full = true )
        DNB_THROW_SPEC_NULL;

    static  scl_wstring
    MonthNameW( int month, bool full = true )
        DNB_THROW_SPEC_NULL;

    static  scl_string
    DayOfWeekName( int wday, bool full = true )
        DNB_THROW_SPEC_NULL;

    static  scl_wstring
    DayOfWeekNameW( int wday, bool full = true )
        DNB_THROW_SPEC_NULL;

    static  scl_string
    EraName( int era, bool secular = false )
        DNB_THROW_SPEC_NULL;

    static  scl_wstring
    EraNameW( int era, bool secular = true )
        DNB_THROW_SPEC_NULL;

    // Returns the number of days in the specified month and year.
    static  int
    DaysInMonth( int month, int year )
        DNB_THROW_SPEC_NULL;

    // Returns the number of days in the specified year.
    static  int
    DaysInYear( int year )
        DNB_THROW_SPEC_NULL;

    // Returns true if <year> is a leap year.
    static  bool
    IsLeapYear( int year )
        DNB_THROW_SPEC_NULL;

    // Returns true if <jday> is a valid Julian day.
    static  bool
    IsJulianDay( int jday )
        DNB_THROW_SPEC_NULL;

    // Returns true if <yday> is a valid day in <year>.
    static  bool
    IsDayInYear( int yday, int year )
        DNB_THROW_SPEC_NULL;

    // Returns true if <wday> is a valid weekday.
    static  bool
    IsDayInWeek( int wday )
        DNB_THROW_SPEC_NULL;

    // Returns true if <mday> is a valid day in <month> of <year>.
    static  bool
    IsDayInMonth( int mday, int month, int year )
        DNB_THROW_SPEC_NULL;

    // Returns true if <month> is a valid month number.
    static  bool
    IsMonth( int month )
        DNB_THROW_SPEC_NULL;

    // Returns true if <year> is a valid year number.
    static  bool
    IsYear( int year )
        DNB_THROW_SPEC_NULL;

    // Returns true if <era> is a valid calendar era.
    static  bool
    IsEra( int era )
        DNB_THROW_SPEC_NULL;


    // Note that all internal calculations
    //  are performed using 32-bit arithemtic.
    //
    typedef DNBInteger32    DateRep;  // had to be public for Unix to compile


protected:
    //
    //  Define several helper functions.  

    enum
    {
        InvalidJDay   =      -1,        // Invalid Julian day
        UnixEpochJDay = 2440588,        // 01 Jan 1970 AD
        GregorianJDay = 2299161,        // 15 Oct 1582 AD
        CommonEraJDay = 1721424         // 01 Jan 0001 AD
    };

    struct DateType
    {
        DateRep     mday;
        DateRep     month;
        DateRep     year;
        DateRep     yday;
    };

    static  int
    DaysBeforeMonth( int month, int year )
        DNB_THROW_SPEC_NULL;

    static  DateRep
    CalcJulian( DateRep mday, DateRep month, DateRep year )
        DNB_THROW_SPEC_NULL;

    static  DateRep
    CalcJulian( DateRep yday, DateRep year )
        DNB_THROW_SPEC_NULL;

    static  void
    CalcGregorian( DateType &gdate, DateRep jday )
        DNB_THROW_SPEC_NULL;

private:
    DateRep     jday_;                  // Days since Jan 01 4713 BC
};




//
//  Binary arithmetic operators
//
ExportedByDNBSystem
DNBSysDate
operator+( const DNBSysDate &left, int right )
    DNB_THROW_SPEC_NULL;


ExportedByDNBSystem
DNBSysDate
operator+( int left, const DNBSysDate &right )
    DNB_THROW_SPEC_NULL;


ExportedByDNBSystem
DNBSysDate
operator-( const DNBSysDate &left, int right )
    DNB_THROW_SPEC_NULL;


ExportedByDNBSystem
int
operator-( const DNBSysDate &left, const DNBSysDate &right )
    DNB_THROW_SPEC_NULL;


//
//  Misc. global functions
//
ExportedByDNBSystem
ostream &
operator<<( ostream &stream, const DNBSysDate &date )
    DNB_THROW_SPEC_NULL;




#endif  /* _DNBSYSDATE_H_ */
