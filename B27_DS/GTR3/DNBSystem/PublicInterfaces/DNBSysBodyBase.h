//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
//
// DNBSysBodyBase.h
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     rtl        05/05/2005   Initial Implementation

// NOTES:
// TBD
//==============================================================================
#ifndef _DNB_SYSBODY_BASE_H_
#define _DNB_SYSBODY_BASE_H_


#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>


#include <DNBSystem.h>
#include <DNBCountedObject.h>
#include <DNBSysMonitor.h>


class ExportedByDNBSystem DNBSysBodyBase : public DNBSysMonitor, public DNBCountedObject
{


public:
	DNBSysBodyBase(void)
        DNB_THROW_SPEC_ANY;

	DNBSysBodyBase(const DNBSysBodyBase& rhs)
        DNB_THROW_SPEC_ANY;

	virtual
    ~DNBSysBodyBase(void)
        DNB_THROW_SPEC_NULL;

	DNBSysBodyBase&
    operator=(const DNBSysBodyBase& rhs)
        DNB_THROW_SPEC_ANY;

};




//
//  Include the public definition file.
//
//#include "DNBSysBodyBase.cc"




#endif  /* _DNB_SYSBODY_BASE_H_ */
