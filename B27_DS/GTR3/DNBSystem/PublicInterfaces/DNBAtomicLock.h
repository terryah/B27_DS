//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 */

//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*    
//*     bkh         10/02/03    Added documentation
//*
//* 

#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNBAtomicLock_H_
#define _DNBAtomicLock_H_


#include <DNBSystemBase.h>


#include <DNBSystemDefs.h>


#include <DNBSystem.h>
#include <DNBException.h>

#if     !DNB_HAS_ATOMIC_LOCK
#include <DNBSysMutexLock.h>
#endif


/**
  *  This class represents a thread-safe integral value that is suitable for use
  *   in locking applications.
  */
class ExportedByDNBSystem DNBAtomicLock
{
public:
    /**
      * Constructor.
      */
    DNBAtomicLock( )
        DNB_THROW_SPEC((DNBEResourceLimit));

    /**
      * Destructor.
      */
    ~DNBAtomicLock( )
        DNB_THROW_SPEC_NULL;

    /**
     *  Mutual exclusive lock.
     */
    bool
    acquire( )
        DNB_THROW_SPEC_NULL;

    /**
     *  Release of lock.
     */
    void
    release( )
        DNB_THROW_SPEC_NULL;

    /**
     *  Checking for locked state and setting it to locked.
     */
    bool
    isLocked( ) const
        DNB_THROW_SPEC_NULL;

protected:
    enum { UNLOCKED = 0, LOCKED = 1 };

#if     defined(_WINDOWS_SOURCE)
typedef long    AtomicType;             // Formal type is LONG.
#else
typedef int     AtomicType;             // Valid for all Unix platforms.
#endif

#if     DNB_HAS_ATOMIC_LOCK
    volatile AtomicType     state_;     // Current state of lock.
#else
    mutable  DNBSysMutexLock    mutex_;     // Actual mutex lock.
    volatile AtomicType     state_;     // Current state of lock.
#endif

private:
    //
    //  Prohibit copy construction and assigment of DNBAtomicLock objects.
    //
    DNBAtomicLock( const DNBAtomicLock &right );

    DNBAtomicLock &
    operator=( const DNBAtomicLock &right );

    friend class DNBAtomicLockTest;     // For testing purposes only.
};




#endif  /* _DNBAtomicLock_H_ */
