//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 */

//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     jad         08/18/97    Initial implementation.
//*     jad         06/20/99    Added an inserter for a DNBMessage object.
//*     rtl         03/19/02    Code change for reading message from catalog :)
//*     bkh         10/31/03    Implementation of new documentation style.
//*     rtl         04/28/05    Undo reading message from catalog :(
//*     rtl         05/20/05    Provide accessor method for formatStr_
//*
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_MESSAGE_H_
#define _DNB_MESSAGE_H_


#include <DNBSystemBase.h>
#include <scl_vector.h>
#include <scl_iosfwd.h>
//#include <rw/rwdate.h>
//#include <rw/rwtime.h>
#include <DNBSystemDefs.h>


#include <DNBSystem.h>
#include <DNBMsgData.h>




/**
  * Defines the default format string for a localized message.
  * @param str
  * A string literal which specifies the format string for the default
  * locale.  Note: In all Deneb applications, the default locale is
  * "English (USA)".  When developing code for such applications, the
  * format string must consist entirely of ASCII characters.
  *
  * @return
  * The single-byte or wide-character representation of the literal <tt>str</tt>.
  *
  * <br><B>Description</B><br>
  * This macro converts the string literal <tt>str</tt> to a representation that
  * is compatible with the message library.  The macro must appear in the
  * constructor of every message object (see class @href DNBMessage).  By
  * adhering to this convention, developers can build single-byte and
  * wide-character applications from the same source code.
  *
  * This macro operates as follows.  If the macro @href DNB_USE_UNICODE
  * returns TRUE, the parameter <tt>str</tt> is converted to its equivalent
  * wide-character representation.  Otherwise, <tt>str</tt> is returned in its
  * (default) single-byte form.
  *
  * <br><B>Example</B><br>
  * <pre>
  * RWTime      rwtime;
  * DNBMessage  msg( DNB_FORMAT("The current date and time is: %1") );
  * msg  << DNBSetFormat('c') << rwtime;
  * </pre>
  *
  * <br><B>Tips</B><br>
  * If a format string is too long to fit conveniently on one line, it can
  * be written on multiple lines using the ANSI C continuation facility.
  * This technique is illustrated below:
  * <pre>
  * DNBMessage  msg( DNB_FORMAT("Now is the time for all good men to come \
  * to the aid of their country.") );
  * </pre>
  *
  * The continuation mechanism is the only supported method of expressing
  * multi-line format strings.  Although you may be tempted to break a long
  * format string into separate (adjacent) string constants, the resulting
  * definition will generate a compilation error if the application supports
  * wide-character messages.
  *
  * <br><B>Warnings</B><br>
  * This macro must be used to define the format string for every localized
  * message.  It can only appear within the constructor of a message object.
  * If you need to insert a character literal or string literal into a
  * message, you should define the literal with the macro @href DNB_TEXT.
  * By adhering to this policy, an external program can be written to
  * extract the complete set of format strings defined within a library.
  *
  *
  */
#define DNB_FORMAT(str)     DNB_TEXT(str), __FILE__, __LINE__



/**
  * A discrete unit of text which can be formatted using different locales.
  *
  * <br><B>Description</B><br>
  * This class represents a discrete unit of text which can be formatted
  * using different locales (i.e., the set of conventions for nationality,
  * culture, and language).  For our purposes, a localized message consists
  * of a (default) format string and an arbitrary number of data elements.
  * The format string is specified at the time of construction in the form
  * of a string literal (see macro @href DNB_FORMAT).  It contains text to
  * be copied verbatim and zero or more conversion specifications of the
  * form <tt>%i</tt>, where <tt>i</tt> denotes the index of a particular data element
  * (numbered consecutively from one).  Data elements are added to the
  * message object using the inserter operator, <tt><<</tt>.  Each such element
  * may represent a number, date, time, currency, or other user-defined
  * type.  The programmer can set various attributes of the data element
  * (such as its field width and alignment mode) using a predefined set of
  * manipulators.
  *
  * Once the message has been initialized, it can be converted to a string
  * using the member function @href DNBMessage::scribe.  This function
  * formats the message using the supplied @href DNBLocale object.
  *
  * <br><B>Example</B><br>
  * <pre>
  * #include DNBSystemBase.h
  * #include <scl_iostream.h>
  * #include rw/rwtime.h
  * #include DNBSystemDefs.h
  * #include DNBMessage.h
  *
  * #ifdef  _MSC_VER
  * #define GERMAN_LOC      "german"                // Windows 95/NT
  * #else
  * #define GERMAN_LOC      "de"                    // UNIX
  * #endif
  *
  * int
  * main()
  * {
  * DNBLocale   GmLocale( GERMAN_LOC );
  * RWTime      rwtime;
  *
  * DNBMessage  msg( DNB_FORMAT("The current date and time is: %1") );
  * msg  << DNBSetFormat('C') << rwtime;
  * cout << msg.scribe( GmLocale ) << endl;
  *
  * return (0);
  * }
  *
  * OUTPUT:
  * The current date and time is: Donnerstag, 25. September 1997 16:21:32
  * </pre>
  *
  * <br><B>Tips</B><br>
  * To display a literal percent character ('<tt>%</tt>') in a localized message,
  * you must include the character sequence "<tt>%%</tt>" in its format string.
  *
  *
  */
class ExportedByDNBSystem DNBMessage : public DNBMsgData
{
public:
/**
  * Constructs a localized message.
  * @param formatStr
  * A string literal which specifies the format string for the
  * default locale.  This literal must be defined with the macro
  * @href DNB_FORMAT.  Note: In all Deneb applications, the default
  * locale is "English (USA)".  When developing code for such
  * applications, the format string must consist entirely of ASCII
  * characters.
  * @param srcFile
  * The name of the source file which contains the definition of the
  * current message.  This parameter is automatically supplied by
  * the macro @href DNB_FORMAT.  It is ignored in the current
  * implementation.
  * @param srcLine
  * The line number of the source file which contains the definition
  * of the current message.  This parameter is automatically
  * supplied by the macro @href DNB_FORMAT.  It is ignored in the
  * current implementation.
  *
  * @return
  * Nothing
  *
  * <br><B>Description</B><br>
  * This function constructs a localized message with the format string
  * initialized to <tt>formatStr</tt>.  The resulting object has zero data
  * elements.
  *
  *
  */
    DNBMessage( const CharType *formatStr, const char *srcFile, int srcLine )
        DNB_THROW_SPEC_NULL;


/**
  * Constructs a localized message using copy semantics.
  * @param right
  * The localized message to be copied.
  *
  * @return
  * Nothing
  *
  * <br><B>Description</B><br>
  * This function copies the format string and data elements of <tt>right</tt>
  * to <tt>self</tt>.  The function employs deep-copy semantics.
  * @exception scl_bad_alloc
  * The object could not be created because of insufficient free
  * memory.
  *
  *
  */
    DNBMessage( const DNBMessage &right ) DNB_THROW_SPEC((scl_bad_alloc));


/**
  * Destroys a localized message.
  *
  * @return
  * Nothing
  *
  * <br><B>Description</B><br>
  * This function destroys a localized message.
  *
  *
  */
    virtual
    ~DNBMessage( ) DNB_THROW_SPEC_NULL;


/**
  * Copies a localized message to <tt>self</tt>.
  * @param right
  * The localized message to be copied.
  *
  * @return
  * Nothing
  *
  * <br><B>Description</B><br>
  * This function copies the format string and data elements of <tt>right</tt>
  * to <tt>self</tt>.  The function employs deep-copy semantics.
  * @exception scl_bad_alloc
  * The object could not be modified because of insufficient free
  * memory.
  *
  *
  */
    void
    operator=( const DNBMessage &right ) DNB_THROW_SPEC((scl_bad_alloc));


/**
  * Inserts a localized data element into <tt>self</tt>.
  * @param data
  * The localized data element to be inserted.
  *
  * @return
  * Nothing
  *
  * <br><B>Description</B><br>
  * This function inserts the localized data element <tt>data</tt> into the
  * data list associated with <tt>self</tt>.  It is used exclusively by the
  * inserter operator (<tt><<</tt>) for user-defined types.
  * @exception scl_bad_alloc
  * The object could not be inserted because of insufficient free
  * memory.
  *
  * <br><B>Warnings</B><br>
  * This function should never be called explicitly by client code.
  *
  *
  */
    virtual void
    append( const DNBMsgData &data ) DNB_THROW_SPEC((scl_bad_alloc));


/**
  * Creates a copy of <tt>self</tt> on the free store.
  *
  * @return
  * A pointer to the newly created object.
  *
  * <br><B>Description</B><br>
  * This function implements a virtual copy constructor.  It is used by
  * the message library to create copies of localized messages.
  * @exception scl_bad_alloc
  * The object could not be created because of insufficient free
  * memory.
  *
  *
  */
    virtual DNBMsgData *
    clone( ) const DNB_THROW_SPEC((scl_bad_alloc));


/**
  * Creates the localized representation of <tt>self</tt>. This version of scribe
  * is used for localizing messages read from the message catalog
  * The global locale is used to format the message.
  *
  * @return
  * The localized representation of <tt>self</tt>.
  *
  * <br><B>Description</B><br>
  * This function returns <tt>self</tt> as a string, formatted by the global
  * locale.  It is used to create the printable representation of a
  * localized message.  The function performs the following operations:
  *
  * <UL>
  * <LI> The default format string is replaced by its foreign-language
  * equivalent.  If this operation fails, the default format
  * string is used instead.  (Note: This step is currently not
  * implemented.).
  * If the format string was from the message catalog, then we already
  * have the localized version. All we need to do now is to replace
  * the positional parameters(/pn or /Pn) with the localized representation
  * of the corresponding data element
  * <LI> The resulting format string is scanned to detect conversion
  * specifications and literal text.
  * <LI> For each conversion specification, the localized
  * representation of the corresponding data element is copied
  * to the output buffer.
  * <LI> All other characters are copied verbatim to the output buffer.
  * <LI> The output buffer is returned to the caller.
  * </UL>
  * @exception scl_bad_alloc
  * An internal buffer could not be allocated because of
  * insufficient free memory.
  * @exception scl_runtime_error
  * A localized data element could not be expressed as a string.
  *
  *
  */
    virtual StringType
    scribe( )
        DNB_THROW_SPEC((scl_bad_alloc, scl_runtime_error));

/**
  * Creates the localized representation of <tt>self</tt>.
  * @param locale
  * The locale object which is used to format the message.
  *
  * @return
  * The localized representation of <tt>self</tt>.
  *
  * <br><B>Description</B><br>
  * This function returns <tt>self</tt> as a string, formatted by the <tt>locale</tt>
  * argument.  It is used to create the printable representation of a
  * localized message.  The function performs the following operations:
  *
  * <UL>
  * <LI> The default format string is replaced by its foreign-language
  * equivalent.  If this operation fails, the default format
  * string is used instead.  (Note: This step is currently not
  * implemented.).
  * If the format string was from the message catalog, then we already
  * have the localized version. All we need to do now is to replace
  * the positional parameters(/pn or /Pn) with the localized representation
  * of the corresponding data element
  * <LI> The resulting format string is scanned to detect conversion
  * specifications and literal text.
  * <LI> For each conversion specification, the localized
  * representation of the corresponding data element is copied
  * to the output buffer.
  * <LI> All other characters are copied verbatim to the output buffer.
  * <LI> The output buffer is returned to the caller.
  * </UL>
  * @exception scl_bad_alloc
  * An internal buffer could not be allocated because of
  * insufficient free memory.
  * @exception scl_runtime_error
  * A localized data element could not be expressed as a string.
  *
  *
  */
    virtual StringType
    scribe( const DNBLocale &locale ) const
        DNB_THROW_SPEC((scl_bad_alloc, scl_runtime_error));

/**
  * Returns the formatString <tt>self</tt>.
  *
  * @return
  * formatString <tt>formatStr_</tt>.
  *
  * <br><B>Description</B><br>
  * This function returns the formatString with which the
  * object was created. Typically used on DNBException objects
  * If you want a localized version of the formatStr_ with
  * all parameters replaced, use the scribe() method.
  */	
	StringType
	getFormatStr()
	DNB_THROW_SPEC_ANY;

private:

    typedef scl_vector<DNBMsgData *, DNB_ALLOCATOR(DNBMsgData *) >  DataList;

    static void
    cloneData( DataList &dst, const DataList &src ) DNB_THROW_SPEC((scl_bad_alloc));

    StringType      formatStr_;         // Format string.
    DataList        dataList_;          // List of data elements.

};




//
//  The following operator sets the formatting attributes of the next data
//  element inserted into a message.  This operator will not appear in the
//  external documentation.
//
template<class T>
DNBMessage &
operator<<( DNBMessage &msg, const _DNBMsgManip<T> &manip ) DNB_THROW_SPEC_NULL;




/**
  * Inserts a Boolean value into a localized message.
  * @param msg
  * The message that is to receive the data element.
  * @param item
  * The data element to be inserted.
  *
  * @return
  * A reference to <tt>msg</tt>.
  *
  * <br><B>Description</B><br>
  * This function inserts the Boolean value <tt>item</tt> into the message <tt>msg</tt>.
  * When the message is scribed, the data element will be converted to the
  * locale-specific representation of "true" or "false".  The following
  * manipulators may be used to set the formatting attributes of <tt>item</tt>.
  *
  * <UL>
  * <LI> @href DNBSetWidth
  * <LI> @href DNBSetFill
  * <LI> @href DNBSetAlignment
  * </UL>
  * @exception scl_bad_alloc
  * The data element could not be inserted because of insufficient free
  * memory.
  *
  *
  */
#if     DNB_HAS_BOOL
ExportedByDNBSystem  DNBMessage &
operator<<( DNBMessage &msg, bool item ) DNB_THROW_SPEC((scl_bad_alloc));
#endif




/**
  * Inserts a character into a localized message.
  * @param msg
  * The message that is to receive the data element.
  * @param item
  * The data element to be inserted.
  *
  * @return
  * A reference to <tt>msg</tt>.
  *
  * <br><B>Description</B><br>
  * This function inserts the character <tt>item</tt> into the message <tt>msg</tt>.  The
  * following manipulators may be used to set the formatting attributes of
  * <tt>item</tt>.
  *
  * <UL>
  * <LI> @href DNBSetWidth
  * <LI> @href DNBSetFill
  * <LI> @href DNBSetAlignment
  * </UL>
  * @exception scl_bad_alloc
  * The data element could not be inserted because of insufficient free
  * memory.
  *
  *
  */
ExportedByDNBSystem  DNBMessage &
operator<<( DNBMessage &msg, char item ) DNB_THROW_SPEC((scl_bad_alloc));




/**
  * Inserts a signed character into a localized message.
  * @param msg
  * The message that is to receive the data element.
  * @param item
  * The data element to be inserted.
  *
  * @return
  * A reference to <tt>msg</tt>.
  *
  * <br><B>Description</B><br>
  * This function inserts the signed character <tt>item</tt> into the message
  * <tt>msg</tt>.  The following manipulators may be used to set the formatting
  * attributes of <tt>item</tt>.
  *
  * <UL>
  * <LI> @href DNBSetWidth
  * <LI> @href DNBSetFill
  * <LI> @href DNBSetAlignment
  * </UL>
  * @exception scl_bad_alloc
  * The data element could not be inserted because of insufficient free
  * memory.
  *
  *
  */
ExportedByDNBSystem  DNBMessage &
operator<<( DNBMessage &msg, signed char item ) DNB_THROW_SPEC((scl_bad_alloc));




/**
  * Inserts an unsigned character into a localized message.
  * @param msg
  * The message that is to receive the data element.
  * @param item
  * The data element to be inserted.
  *
  * @return
  * A reference to <tt>msg</tt>.
  *
  * <br><B>Description</B><br>
  * This function inserts the unsigned character <tt>item</tt> into the message
  * <tt>msg</tt>.  The following manipulators may be used to set the formatting
  * attributes of <tt>item</tt>.
  *
  * <UL>
  * <LI> @href DNBSetWidth
  * <LI> @href DNBSetFill
  * <LI> @href DNBSetAlignment
  * </UL>
  * @exception scl_bad_alloc
  * The data element could not be inserted because of insufficient free
  * memory.
  *
  *
  */
ExportedByDNBSystem  DNBMessage &
operator<<( DNBMessage &msg, unsigned char item ) DNB_THROW_SPEC((scl_bad_alloc));




/**
  * Inserts a wide character into a localized message.
  * @param msg
  * The message that is to receive the data element.
  * @param item
  * The data element to be inserted.
  *
  * @return
  * A reference to <tt>msg</tt>.
  *
  * <br><B>Description</B><br>
  * This function inserts the wide character <tt>item</tt> into the message <tt>msg</tt>.
  * The following manipulators may be used to set the formatting attributes
  * of <tt>item</tt>.
  *
  * <UL>
  * <LI> @href DNBSetWidth
  * <LI> @href DNBSetFill
  * <LI> @href DNBSetAlignment
  * </UL>
  * @exception scl_bad_alloc
  * The data element could not be inserted because of insufficient free
  * memory.
  *
  *
  */
#ifdef  SCL_HAS_DISTINCT_WCHAR
ExportedByDNBSystem  DNBMessage &
operator<<( DNBMessage &msg, wchar_t item ) DNB_THROW_SPEC((scl_bad_alloc));
#endif




/**
  * Inserts a short integer into a localized message.
  * @param msg
  * The message that is to receive the data element.
  * @param item
  * The data element to be inserted.
  *
  * @return
  * A reference to <tt>msg</tt>.
  *
  * <br><B>Description</B><br>
  * This function inserts the short integer <tt>item</tt> into the message <tt>msg</tt>.
  * The following manipulators may be used to set the formatting attributes
  * of <tt>item</tt>.
  *
  * <UL>
  * <LI> @href DNBSetWidth
  * <LI> @href DNBSetFill
  * <LI> @href DNBSetAlignment
  * </UL>
  * @exception scl_bad_alloc
  * The data element could not be inserted because of insufficient free
  * memory.
  *
  *
  */
ExportedByDNBSystem  DNBMessage &
operator<<( DNBMessage &msg, short item ) DNB_THROW_SPEC((scl_bad_alloc));




/**
  * Inserts an unsigned short integer into a localized message.
  * @param msg
  * The message that is to receive the data element.
  * @param item
  * The data element to be inserted.
  *
  * @return
  * A reference to <tt>msg</tt>.
  *
  * <br><B>Description</B><br>
  * This function inserts the unsigned short integer <tt>item</tt> into the message
  * <tt>msg</tt>.  The following manipulators may be used to set the formatting
  * attributes of <tt>item</tt>.
  *
  * <UL>
  * <LI> @href DNBSetWidth
  * <LI> @href DNBSetFill
  * <LI> @href DNBSetAlignment
  * </UL>
  * @exception scl_bad_alloc
  * The data element could not be inserted because of insufficient free
  * memory.
  *
  *
  */
ExportedByDNBSystem  DNBMessage &
operator<<( DNBMessage &msg, unsigned short item ) DNB_THROW_SPEC((scl_bad_alloc));




/**
  * Inserts an integer into a localized message.
  * @param msg
  * The message that is to receive the data element.
  * @param item
  * The data element to be inserted.
  *
  * @return
  * A reference to <tt>msg</tt>.
  *
  * <br><B>Description</B><br>
  * This function inserts the integer <tt>item</tt> into the message <tt>msg</tt>.  The
  * following manipulators may be used to set the formatting attributes of
  * <tt>item</tt>.
  *
  * <UL>
  * <LI> @href DNBSetWidth
  * <LI> @href DNBSetFill
  * <LI> @href DNBSetAlignment
  * </UL>
  * @exception scl_bad_alloc
  * The data element could not be inserted because of insufficient free
  * memory.
  *
  *
  */
ExportedByDNBSystem  DNBMessage &
operator<<( DNBMessage &msg, int item ) DNB_THROW_SPEC((scl_bad_alloc));




/**
  * Inserts an unsigned integer into a localized message.
  * @param msg
  * The message that is to receive the data element.
  * @param item
  * The data element to be inserted.
  *
  * @return
  * A reference to <tt>msg</tt>.
  *
  * <br><B>Description</B><br>
  * This function inserts the unsigned integer <tt>item</tt> into the message
  * <tt>msg</tt>.  The following manipulators may be used to set the formatting
  * attributes of <tt>item</tt>.
  *
  * <UL>
  * <LI> @href DNBSetWidth
  * <LI> @href DNBSetFill
  * <LI> @href DNBSetAlignment
  * </UL>
  * @exception scl_bad_alloc
  * The data element could not be inserted because of insufficient free
  * memory.
  *
  *
  */
ExportedByDNBSystem  DNBMessage &
operator<<( DNBMessage &msg, unsigned int item ) DNB_THROW_SPEC((scl_bad_alloc));




/**
  * Inserts a long integer into a localized message.
  * @param msg
  * The message that is to receive the data element.
  * @param item
  * The data element to be inserted.
  *
  * @return
  * A reference to <tt>msg</tt>.
  *
  * <br><B>Description</B><br>
  * This function inserts the long integer <tt>item</tt> into the message <tt>msg</tt>.
  * The following manipulators may be used to set the formatting attributes
  * of <tt>item</tt>.
  *
  * <UL>
  * <LI> @href DNBSetWidth
  * <LI> @href DNBSetFill
  * <LI> @href DNBSetAlignment
  * </UL>
  * @exception scl_bad_alloc
  * The data element could not be inserted because of insufficient free
  * memory.
  *
  *
  */
ExportedByDNBSystem  DNBMessage &
operator<<( DNBMessage &msg, long item ) DNB_THROW_SPEC((scl_bad_alloc));




/**
  * Inserts an unsigned long integer into a localized message.
  * @param msg
  * The message that is to receive the data element.
  * @param item
  * The data element to be inserted.
  *
  * @return
  * A reference to <tt>msg</tt>.
  *
  * <br><B>Description</B><br>
  * This function inserts the unsigned long integer <tt>item</tt> into the message
  * <tt>msg</tt>.  The following manipulators may be used to set the formatting
  * attributes of <tt>item</tt>.
  *
  * <UL>
  * <LI> @href DNBSetWidth
  * <LI> @href DNBSetFill
  * <LI> @href DNBSetAlignment
  * </UL>
  * @exception scl_bad_alloc
  * The data element could not be inserted because of insufficient free
  * memory.
  *
  *
  */
ExportedByDNBSystem  DNBMessage &
operator<<( DNBMessage &msg, unsigned long item ) DNB_THROW_SPEC((scl_bad_alloc));




/**
  * Inserts a single-precision floating-point number into a localized
  * message.
  * @param msg
  * The message that is to receive the data element.
  * @param item
  * The data element to be inserted.
  *
  * @return
  * A reference to <tt>msg</tt>.
  *
  * <br><B>Description</B><br>
  * This function inserts the single-precision floating-point number <tt>item</tt>
  * into the message <tt>msg</tt>.  The following manipulators may be used to set
  * the formatting attributes of <tt>item</tt>.
  *
  * <UL>
  * <LI> @href DNBSetWidth
  * <LI> @href DNBSetFill
  * <LI> @href DNBSetAlignment
  * <LI> @href DNBSetPrecision
  * </UL>
  * @exception scl_bad_alloc
  * The data element could not be inserted because of insufficient free
  * memory.
  *
  *
  */
ExportedByDNBSystem  DNBMessage &
operator<<( DNBMessage &msg, float item ) DNB_THROW_SPEC((scl_bad_alloc));




/**
  * Inserts a double-precision floating-point number into a localized
  * message.
  * @param msg
  * The message that is to receive the data element.
  * @param item
  * The data element to be inserted.
  *
  * @return
  * A reference to <tt>msg</tt>.
  *
  * <br><B>Description</B><br>
  * This function inserts the double-precision floating-point number <tt>item</tt>
  * into the message <tt>msg</tt>.  The following manipulators may be used to set
  * the formatting attributes of <tt>item</tt>.
  *
  * <UL>
  * <LI> @href DNBSetWidth
  * <LI> @href DNBSetFill
  * <LI> @href DNBSetAlignment
  * <LI> @href DNBSetPrecision
  * </UL>
  * @exception scl_bad_alloc
  * The data element could not be inserted because of insufficient free
  * memory.
  *
  *
  */
ExportedByDNBSystem  DNBMessage &
operator<<( DNBMessage &msg, double item ) DNB_THROW_SPEC((scl_bad_alloc));




/**
  * Inserts an extended-precision floating-point number into a localized
  * message.
  * @param msg
  * The message that is to receive the data element.
  * @param item
  * The data element to be inserted.
  *
  * @return
  * A reference to <tt>msg</tt>.
  *
  * <br><B>Description</B><br>
  * This function inserts the extended-precision floating-point number
  * <tt>item</tt> into the message <tt>msg</tt>.  The following manipulators may be used
  * to set the formatting attributes of <tt>item</tt>.
  *
  * <UL>
  * <LI> @href DNBSetWidth
  * <LI> @href DNBSetFill
  * <LI> @href DNBSetAlignment
  * <LI> @href DNBSetPrecision
  * </UL>
  * @exception scl_bad_alloc
  * The data element could not be inserted because of insufficient free
  * memory.
  *
  *
  */
ExportedByDNBSystem  DNBMessage &
operator<<( DNBMessage &msg, long double item ) DNB_THROW_SPEC((scl_bad_alloc));




/**
  * Inserts a multibyte character string into a localized message.
  * @param msg
  * The message that is to receive the data element.
  * @param item
  * The multibyte character string to be inserted.  This string must be
  * null-terminated and must begin in the "initial" shift state.
  *
  * @return
  * A reference to <tt>msg</tt>.
  *
  * <br><B>Description</B><br>
  * This function inserts the multibyte character string (MBCS) referenced
  * by <tt>item</tt> into the message <tt>msg</tt>.  The following manipulators may be
  * used to set the formatting attributes of <tt>item</tt>.
  *
  * <UL>
  * <LI> @href DNBSetWidth
  * <LI> @href DNBSetFill
  * <LI> @href DNBSetAlignment
  * </UL>
  * @exception scl_bad_alloc
  * The data element could not be inserted because of insufficient free
  * memory.
  *
  *
  */
ExportedByDNBSystem  DNBMessage &
operator<<( DNBMessage &msg, const char *item ) DNB_THROW_SPEC((scl_bad_alloc));




/**
  * Inserts a single-byte character string into a localized message.
  * @param msg
  * The message that is to receive the data element.
  * @param item
  * The single-byte character string to be inserted.
  *
  * @return
  * A reference to <tt>msg</tt>.
  *
  * <br><B>Description</B><br>
  * This function inserts the single-byte character string (SBCS) referenced
  * by <tt>item</tt> into the message <tt>msg</tt>.  The following manipulators may be
  * used to set the formatting attributes of <tt>item</tt>.
  *
  * <UL>
  * <LI> @href DNBSetWidth
  * <LI> @href DNBSetFill
  * <LI> @href DNBSetAlignment
  * </UL>
  * @exception scl_bad_alloc
  * The data element could not be inserted because of insufficient free
  * memory.
  *
  *
  */
ExportedByDNBSystem  DNBMessage &
operator<<( DNBMessage &msg, const scl_string &item ) DNB_THROW_SPEC((scl_bad_alloc));




/**
  * Inserts a wide character string into a localized message.
  * @param msg
  * The message that is to receive the data element.
  * @param item
  * The wide character string to be inserted.  This string must be
  * null-terminated.
  *
  * @return
  * A reference to <tt>msg</tt>.
  *
  * <br><B>Description</B><br>
  * This function inserts the wide character string (WCS) referenced by
  * <tt>item</tt> into the message <tt>msg</tt>.  The following manipulators may be used
  * to set the formatting attributes of <tt>item</tt>.
  *
  * <UL>
  * <LI> @href DNBSetWidth
  * <LI> @href DNBSetFill
  * <LI> @href DNBSetAlignment
  * </UL>
  * @exception scl_bad_alloc
  * The data element could not be inserted because of insufficient free
  * memory.
  *
  *
  */
ExportedByDNBSystem  DNBMessage &
operator<<( DNBMessage &msg, const wchar_t *item ) DNB_THROW_SPEC((scl_bad_alloc));




/**
  * Inserts a wide character string into a localized message.
  * @param msg
  * The message that is to receive the data element.
  * @param item
  * The wide character string to be inserted.
  *
  * @return
  * A reference to <tt>msg</tt>.
  *
  * <br><B>Description</B><br>
  * This function inserts the wide character string (WCS) denoted by <tt>item</tt>
  * into the message <tt>msg</tt>.  The following manipulators may be used to set
  * the formatting attributes of <tt>item</tt>.
  *
  * <UL>
  * <LI> @href DNBSetWidth
  * <LI> @href DNBSetFill
  * <LI> @href DNBSetAlignment
  * </UL>
  * @exception scl_bad_alloc
  * The data element could not be inserted because of insufficient free
  * memory.
  *
  *
  */
ExportedByDNBSystem  DNBMessage &
operator<<( DNBMessage &msg, const scl_wstring &item ) DNB_THROW_SPEC((scl_bad_alloc));




/**
  * Inserts a localized message into a localized message.
  * @param msg
  * The message that is to receive the data element.
  * @param item
  * The localized message to be inserted.
  *
  * @return
  * A reference to <tt>msg</tt>.
  *
  * <br><B>Description</B><br>
  * This function inserts the localized message denoted by <tt>item</tt> into the
  * message <tt>msg</tt>.  The following manipulators may be used to set the
  * formatting attributes of <tt>item</tt>.
  *
  * <UL>
  * <LI> @href DNBSetWidth
  * <LI> @href DNBSetFill
  * <LI> @href DNBSetAlignment
  * </UL>
  * @exception scl_bad_alloc
  * The data element could not be inserted because of insufficient free
  * memory.
  *
  *
  */
ExportedByDNBSystem  DNBMessage &
operator<<( DNBMessage &msg, const DNBMessage &item )
    DNB_THROW_SPEC((scl_bad_alloc));




/**
  * A monetary value for a particular locale.
  *
  * <br><B>Description</B><br>
  * This class represents a single monetary value for a particular locale
  * (i.e., geographic region or nationality).  It uses the convention that
  * such values represent an integral number of the smallest unit of
  * currency in use.  For example, in the United States, $10.00 can be
  * represented as:
  * <pre>
  * DNBCurrency     ten( 1000.0 );      // 1000 cents
  * </pre>
  *
  * This class is used primarily by the message library (see class
  * @href DNBMessage) to represent currency in a standard way.  As such, it
  * provides only a minimal interface.
  *
  *
  */
class ExportedByDNBSystem DNBCurrency
{
public:
/**
  * Constructs a currency object.
  * @param value
  * The desired monetary value.  This parameter is expressed as an
  * integral number of the smallest unit of currency in use.
  *
  * @return
  * Nothing
  *
  * <br><B>Description</B><br>
  * This function constructs a currency object with the monetary value
  * of <tt>value</tt>.
  *
  *
  */
    inline  explicit
    DNBCurrency( double value ) DNB_THROW_SPEC_NULL;


/**
  * Sets the monetary value of <tt>self</tt>.
  * @param value
  * The desired monetary value.  This parameter is expressed as an
  * integral number of the smallest unit of currency in use.
  *
  * @return
  * Nothing
  *
  * <br><B>Description</B><br>
  * This function sets the monetary value of <tt>self</tt> to <tt>value</tt>.
  *
  *
  */
    inline  void
    setValue( double value ) DNB_THROW_SPEC_NULL;


/**
  * Retrieves the monetary value of <tt>self</tt>.
  *
  * @return
  * The monetary value of <tt>self</tt>.
  *
  * <br><B>Description</B><br>
  * This function returns the monetary value of <tt>self</tt>.
  *
  *
  */
    inline  double
    getValue( ) const DNB_THROW_SPEC_NULL;

private:
    double      value_;
};




/**
  * Inserts a monetary value into a localized message.
  * @param msg
  * The message that is to receive the data element.
  * @param item
  * The data element to be inserted.
  *
  * @return
  * A reference to <tt>msg</tt>.
  *
  * <br><B>Description</B><br>
  * This function inserts the monetary value <tt>item</tt> into the message <tt>msg</tt>.
  * The following manipulators may be used to set the formatting attributes
  * of <tt>item</tt>.
  *
  * <UL>
  * <LI> @href DNBSetWidth
  * <LI> @href DNBSetFill
  * <LI> @href DNBSetAlignment
  * <LI> @href DNBSetSymbol
  * </UL>
  * @exception scl_bad_alloc
  * The data element could not be inserted because of insufficient free
  * memory.
  *
  *
  */
ExportedByDNBSystem  DNBMessage &
operator<<( DNBMessage &msg, const DNBCurrency &item )
    DNB_THROW_SPEC((scl_bad_alloc));




/**
  * Inserts a standard time object into a localized message.
  * @param msg
  * The message that is to receive the data element.
  * @param item
  * The <tt>struct tm</tt> object to be inserted.  The time component is
  * assumed to be expressed in Greenwich mean time (GMT).  Further, the
  * following fields of <tt>item</tt> must be set consistently: <tt>tm_year</tt>,
  * <tt>tm_mon</tt>, <tt>tm_mday</tt>, <tt>tm_hour</tt>, <tt>tm_min</tt>, and <tt>tm_sec</tt>.
  *
  * @return
  * A reference to <tt>msg</tt>.
  *
  * <br><B>Description</B><br>
  * This function inserts the <tt>struct tm</tt> object <tt>item</tt> into the message
  * <tt>msg</tt>.  The following manipulators may be used to set the formatting
  * attributes of <tt>item</tt>.
  *
  * <UL>
  * <LI> @href DNBSetWidth
  * <LI> @href DNBSetFill
  * <LI> @href DNBSetAlignment
  * <LI> @href DNBSetFormat
  * </UL>
  *
  * The default format character for <tt>struct tm</tt> objects is
  * @href DNBMsgBase::DateTime, which is equivalent to the character 'c'.
  * @exception scl_bad_alloc
  * The data element could not be inserted because of insufficient free
  * memory.
  *
  *
  */
ExportedByDNBSystem  DNBMessage &
operator<<( DNBMessage &msg, const struct tm &item )
    DNB_THROW_SPEC((scl_bad_alloc));




/**
  * Inserts a Rogue Wave date object into a localized message.
  * @param msg
  * The message that is to receive the data element.
  * @param item
  * The data element to be inserted.
  *
  * @return
  * A reference to <tt>msg</tt>.
  *
  * <br><B>Description</B><br>
  * This function inserts the <tt>RWDate</tt> object <tt>item</tt> into the message <tt>msg</tt>.
  * The following manipulators may be used to set the formatting attributes
  * of <tt>item</tt>.
  *
  * <UL>
  * <LI> @href DNBSetWidth
  * <LI> @href DNBSetFill
  * <LI> @href DNBSetAlignment
  * <LI> @href DNBSetFormat
  * </UL>
  *
  * The default format character for <tt>RWDate</tt> objects is
  * @href DNBMsgBase::DateOnly, which is equivalent to the character 'x'.
  * @exception scl_bad_alloc
  * The data element could not be inserted because of insufficient free
  * memory.
  *
  *
  */
ExportedByDNBSystem  DNBMessage &
//operator<<( DNBMessage &msg, const RWDate &item ) DNB_THROW_SPEC((scl_bad_alloc));
operator<<( DNBMessage &msg, const DNBSysDate &item ) DNB_THROW_SPEC((scl_bad_alloc));


/**
  * Inserts a DNBSysTime object into a localized message.
  * @param msg
  * The message that is to receive the data element.
  * @param item
  * The data element to be inserted.
  *
  * @return
  * A reference to <tt>msg</tt>.
  */
ExportedByDNBSystem  DNBMessage &
operator<<( DNBMessage &msg, const DNBSysTime &item ) DNB_THROW_SPEC((scl_bad_alloc));



/**
  * Inserts a Rogue Wave time object into a localized message.
  * @param msg
  * The message that is to receive the data element.
  * @param item
  * The data element to be inserted.
  *
  * @return
  * A reference to <tt>msg</tt>.
  *
  * <br><B>Description</B><br>
  * This function inserts the <tt>RWTime</tt> object <tt>item</tt> into the message <tt>msg</tt>.
  * The following manipulators may be used to set the formatting attributes
  * of <tt>item</tt>.
  *
  * <UL>
  * <LI> @href DNBSetWidth
  * <LI> @href DNBSetFill
  * <LI> @href DNBSetAlignment
  * <LI> @href DNBSetFormat
  * </UL>
  *
  * The default format character for <tt>RWTime</tt> objects is
  * @href DNBMsgBase::DateTime, which is equivalent to the character 'c'.
  * @exception scl_bad_alloc
  * The data element could not be inserted because of insufficient free
  * memory.
  *
  *
  */
ExportedByDNBSystem  DNBMessage &
//operator<<( DNBMessage &msg, const RWTime &item ) DNB_THROW_SPEC((scl_bad_alloc));
operator<<( DNBMessage &msg, const DNBSysDateTime &item ) DNB_THROW_SPEC((scl_bad_alloc));



/**
  * Writes a localized message to an output stream using the global locale.
  * @param ostr
  * The output stream that is to receive the localized message.
  * @param msg
  * The localized message to be inserted.
  *
  * @return
  * A reference to <tt>ostr</tt>.
  *
  * <br><B>Description</B><br>
  * This function writes the localized message <tt>msg</tt> to the output stream
  * <tt>ostr</tt> using the global locale.  It is equivalent to the following code
  * fragment:
  * <pre>
  * ostr << msg.scribe( DNBLocale::global() );
  * </pre>
  *
  * The function is used primarily in test programs which support only a
  * single locale.
  * @exception scl_bad_alloc
  * An internal buffer could not be allocated because of insufficient
  * free memory.
  * @exception scl_runtime_error
  * A localized data element could not be expressed as a string.
  *
  */
ExportedByDNBSystem  ostream &
operator<<( ostream &ostr, const DNBMessage &msg )
    DNB_THROW_SPEC((scl_bad_alloc, scl_runtime_error));




//
//  Include the public definition file.
//
#include "DNBMessage.cc"


#endif  /* _DNB_MESSAGE_H_ */
