//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
//
// DNBSysMutexLock.h
//
// ==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     smw        12/20/2003   Initial Implementation
//
// ==============================================================================


#ifndef DNBSysMutexLock_H
#define DNBSysMutexLock_H

#include <DNBSystem.h>
#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>
#include <DNBException.h>                 // for exception class declarations
#include <DNBSysThreadDefs.h>
#include <DNBSysFastMutexLock.h>
#include <DNBSysIMutexLock.h>
#include <DNBSysThreadId.h>


/**
  * This class implements a recursive/non-recursive mutex.  Do not use this class to protect
  * global / static objects; Use DNBSysFastMutexLock instead.
  */
class ExportedByDNBSystem DNBSysMutexLock : public DNBSysIMutexLock
{
public:

    /**
      * Construct a DNBSysMutexLock object.  If recursive is true, the object will be recursive
      * i.e. a thread who already owns the mutex will succeed at acquiring it again.  This is the
      * default behavior if recursive is not specified.
      */
    DNBSysMutexLock( bool recursive = true)
        DNB_THROW_SPEC ((DNBELockViolation, DNBEResourceLimit, DNBEInternalError));

    /**
      * Destructor
      */
    virtual ~DNBSysMutexLock( )
        DNB_THROW_SPEC ((DNBELockViolation));


    /*
     * Locks the section of code following the call to acquire() so that only one thread at a 
     * time can enter that section, until a call to release() is issued.  If this mutex lock
     * is recursive, the <\it same> thread can issue another call to acquire the same mutex
     * which it already owns.  
     */   
    virtual void acquire( ) DNB_THROW_SPEC ((DNBELockViolation));

    

    /**
     * Release the mutex so other threads waiting on the mutex can acquire it
     */
    virtual void release( ) DNB_THROW_SPEC ((DNBENotOwner));


#   if DNB_HAS_WIN32_THREADS

        typedef HANDLE              NativeLockRep;

#   elif DNB_HAS_POSIX_THREADS

        typedef pthread_mutex_t     NativeLockRep;

#   endif


    /**
      * Returns the internal operating system lock used
      */
    const NativeLockRep * getNativeLock( ) const;


protected:

    void initialize( ) DNB_THROW_SPEC ((DNBELockViolation, DNBEResourceLimit, DNBEInternalError));

    void finalize( ) DNB_THROW_SPEC ((DNBELockViolation));


private:
    //
    //  Prohibit copy construction and assignment operations.
    //
    DNBSysMutexLock( const DNBSysMutexLock & );

    DNBSysMutexLock & operator=( const DNBSysMutexLock & );

    //
    //  Data members.
    //
    NativeLockRep       nativeLock_;
    DNBSysFastMutexLock internalLock_;
    DNBSysThreadId      owningThread_;
    bool                recursive_;

};


#endif  // DNBSysMutexLock_H
