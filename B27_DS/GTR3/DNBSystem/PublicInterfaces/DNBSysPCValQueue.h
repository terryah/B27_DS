//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
//
// DNBSysPCValQueue.h
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     smw        12/20/2003   Initial Implementation
//	   smw        09/20/2004   changed the internal container type to be a templated parameter
//							   Changed the method names to follow STL convention (e.g. push instead of read, etc..)
/**
 * @fullreview //smw 03:12:23    // not yet reviewed
 * @quickreview //jod 04:02:02   // not yet reviewed
 */
//==============================================================================

#ifndef DNB_SYS_PC_VAL_QUEUE_H
#define DNB_SYS_PC_VAL_QUEUE_H

#include <DNBSystemBase.h>
#include <scl_deque.h>
#include <DNBSystemDefs.h>
#include <DNBSystem.h>

#include <DNBSysPCBuffer.h>
#include <DNBException.h>

template < class Type, class Sequence = scl_deque< Type>  >
class DNBSysPCValQueue: public DNBSysPCBuffer
{
public:

    /** 
      * Constuctor
      */
    DNBSysPCValQueue ( size_t maxSize = 0 );



    /**
      * Destructor
      */
    ~DNBSysPCValQueue();

 
    
    
    /**
      * Returns a copy of the next available element in the queue, if any,
      * and if the queue is open.  
      * Returns NULL otherwise
      */
    // Note: definition of peek differs from that of RW;  I would like to simply not provide a peek function
    Type
    peek() const
    DNB_THROW_SPEC_NULL;





    /**
      * If the queue is open and there is an available element in the queue, returns TRUE and copies the element 
      * to the element argument passed by reference.  Otherwise returns FALSE.
      */
    bool
    tryPeek( Type& element ) const
    DNB_THROW_SPEC_NULL;




    /**
      * If the queue is open and there is an available element in the queue, returns true and removes 
      * the next element from the queue and copies into the instance passed as a reference.  
      * If the queue is empty, the calling thread will block until data becomes available 
      * on the queue ( as a result of another thread writing to it ). Returns false if the read operation 
      * was unsuccessful due to the queue being closed. If a thread is blocked waiting to read from
      * an empty queue and then the queue is closed, the thread will unblock and return false.
      */
    bool
    read( Type& element )
    DNB_THROW_SPEC_NULL;





    /**
      * If the queue is open and contains a value, this function removes that value from the queue, 
      * copies it into the instance passed as a reference, and returns TRUE to indicate that the read 
      * succeeded. If the queue is empty or closed, this function immediately returns FALSE 
      * to indicate that the read was unsuccessful. 
      */
    bool
    tryRead( Type& element )
    DNB_THROW_SPEC_NULL;




    /**
      * If the queue is open and has available space, returns true and Inserts the supplied element at 
      * the next position in the queue.
      * If the queue is full, the calling thread blocks until the queue 
      * has available space ( as a result of other thread(s) reading from it).
      * Returns false if the write operation was unsuccessful due to the queue being closed.  
      * 
      */
    bool
    write( Type element )
    DNB_THROW_SPEC_NULL ;




    /**
      * Inserts a value at the next position in the queue, but only if the value may be written 
      * immediately; in other words, only if there is sufficient free capacity. 
      * In this case, the method returns a value of TRUE to indicate that the write succeeded. 
      * If the queue is full, this function immediately returns a value of FALSE to indicate that 
      * the write was unsuccessful. 
      */
    bool
    tryWrite( Type element )
    DNB_THROW_SPEC_NULL;



    // Inherited pure virtual functions needed to be implemented here

    bool 
    canRead() const
    DNB_THROW_SPEC_NULL;



    bool 
    canWrite() const 
    DNB_THROW_SPEC_NULL;




    /**
      * Returns the number of entries in the queue
      */
    size_t 
    entries() const
    DNB_THROW_SPEC_NULL;



    /** 
      * Removes all entries currently stored in the buffer. If the buffer is full, and there are 
      * threads waiting to write to the buffer, this function signals these threads that they may 
      * now attempt to write.
      */
	// FIXME: actually, we signal in all cases: there does not seem to be any harm in signaling
	// when no one is waiting; the next waiter that comes will not immediately get the previous
	// signal.
    void
    flush()  
    DNB_THROW_SPEC_NULL;



    // could be moved to DNBSysPCBuffer class; or move everything from that class to here and delete that class

    void
    close();


    void
    open();
 

    bool
    isClosed();


private:

    Sequence		queue_;
    bool            isClosed_;  

};


// Include the public definition file

#include <DNBSysPCValQueue.cc>

#endif // DNB_SYS_PC_VAL_QUEUE_H
