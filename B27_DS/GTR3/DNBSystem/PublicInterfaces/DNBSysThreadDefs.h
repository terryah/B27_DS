//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/

/**
 */
//==============================================================================
//
// DNBSysThreadDefs.h
//
// ==============================================================================
//                                                         
// DESCRIPTION:
//   Definitions related to Multi-threading                
//                                                         
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     smw        12/20/2003   Initial Implementation
//
// ==============================================================================



#ifndef DNB_SYS_THREAD_DEFS_H
#define DNB_SYS_THREAD_DEFS_H
//

//
//  Windows 2000/XP configuration.
//
#ifdef  _WINDOWS_SOURCE
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#undef  Yield
#endif  // _WINDOWS_SOURCE


//
//  IRIX 6.x configuration.
//
#ifdef  _IRIX_SOURCE
#include <pthread.h>
#include <errno.h>
#include <unistd.h> // for sleep
#include <sys/timespec.h> // for timespec required for timed wait on pthread_cond_timedwait
#endif  // _IRIX_SOURCE


//
//  SunOS 5.x configuration.
//
#ifdef  _SUNOS_SOURCE
#include <pthread.h>
#include <errno.h>
#include <unistd.h>
#include <time.h>  // for timespec required for timed wait on pthread_cond_timedwait
#endif  // _SUNOS_SOURCE


//
//  AIX 5.x configuration.
//
#ifdef  _AIX_SOURCE
#include <pthread.h>
#include <errno.h>
#include <unistd.h>  // for sleep
#include <time.h> // for timespec required for timed wait on pthread_cond_timedwait
#endif  // _AIX_SOURCE


//
//  HP-UX 11.x configuration.
//
#ifdef  _HPUX_SOURCE
#include <pthread.h>
#include <errno.h>
#include <unistd.h> // for sleep
#include <sys/time.h> // for timespec required for timed wait on pthread_cond_timedwait
#endif  // _HPUX_SOURCE


//
//  Unsupported configuration.
//
#if     !(DNB_HAS_WIN32_THREADS) && !(DNB_HAS_POSIX_THREADS)
#error  "Multithreading is unsupported on this Platform"
#endif


//
//  Debug macro.
//
#ifdef  CNEXT_CLIENT                    // Defined during release builds.

#ifdef  DNBSYS_THREAD_DEBUG             // FIXME: check that this is the macro name used for debugging
#undef  DNBSYS_THREAD_DEBUG             // Disable debug code.
#endif

#else

#ifndef DNBSYS_THREAD_DEBUG
#define DNBSYS_THREAD_DEBUG             // Enable debug code.
#endif

#endif  // CNEXT_CLIENT


//
//  This type represents a timeout value in milliseconds.
//
#if     DNB_HAS_WIN32_THREADS
typedef DWORD       DNBSysTimeout;

#elif   DNB_HAS_POSIX_THREADS
typedef unsigned    DNBSysTimeout;

#endif


enum DNBStaticObject { DNB_STATIC_OBJECT };  // This is really not specific to mutltithreading. It provides a 
                                             // solution for the general problem of global / static initialization.


/***********************************************************************************/
// Enumerations related to thread attributes ( DNBThreadAttribute objects )
/***********************************************************************************/

enum DNBSysDetachState {
	DNB_SYS_DETACH_STATE_DETACHED,
	DNB_SYS_DETACH_STATE_JOINABLE
};


enum DNBSysInheritScheduling {
	DNB_SYS_INHERIT_SCHEDULING_YES,
	DNB_SYS_INHERIT_SCHEDULING_NO,
};


enum DNBSysSchedulingPolicy {
	DNB_SYS_SCHEDULING_POLICY_FIFO,
	DNB_SYS_SCHEDULING_POLICY_RR,
	DNB_SYS_SCHEDULING_POLICY_OTHER,
};


enum DNBSysContentionScope {
	DNB_SYS_CONTENTION_SCOPE_SYSTEM,
	DNB_SYS_CONTENTION_SCOPE_PROCESS 
};




typedef int DNBSysThreadPriority;



/***********************************************************************************/
// enums related to thread states
/***********************************************************************************/

enum DNBSysExecutionState {
//   DNB_EXEC_INITIAL        = 0x0001,  // Constructed; waiting for start
   DNB_SYS_IDLE           = 0x0002,  // Finished running its runnable; waiting for restart
   DNB_SYS_RUNNING        = 0x0004,  // Active, executing
   DNB_SYS_SUSPENDED      = 0x0010,  // blocked; Waiting for resume()
};


/***********************************************************************************/
/**
  * This is the type of the function that can be passed as a parameter to native thread creation
  * functions
  */
/***********************************************************************************/

#   if DNB_HAS_WIN32_THREADS
        typedef  unsigned long (__stdcall *os_thread_function_type) (void *);
        typedef unsigned long os_thread_function_return_type;  // FIXME : currently not being used; delete
#   elif DNB_HAS_POSIX_THREADS
        typedef void* (*os_thread_function_type) ( void* );
        typedef void* os_thread_function_return_type;
#   endif


#endif // DNB_SYS_THREAD_DEFS_H
