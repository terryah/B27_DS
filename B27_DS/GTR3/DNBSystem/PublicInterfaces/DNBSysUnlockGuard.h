//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
#ifndef DNBSysUnlockGuard_H
#define DNBSysUnlockGuard_H


#include <DNBSystem.h>
#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>
#include <DNBSysThreadDefs.h>

class DNBSysIMutexLock;


class ExportedByDNBSystem DNBSysUnlockGuard
{
public:
    DNBSysUnlockGuard( DNBSysIMutexLock &mutexLock );

    ~DNBSysUnlockGuard( );

    void
    acquire( );

    void
    release( );

    bool isAcquired( );

private:
    //
    //  Prohibit copy construction and assignment operations.
    //
    DNBSysUnlockGuard( const DNBSysUnlockGuard & );

    DNBSysUnlockGuard &
    operator=( const DNBSysUnlockGuard & );

    //
    //  Data members.
    //
    DNBSysIMutexLock   &mutexLock_;
    bool		isAcquired_;
};




#endif  // DNBSysUnlockGuard_H
