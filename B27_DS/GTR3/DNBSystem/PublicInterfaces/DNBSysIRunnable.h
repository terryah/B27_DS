//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
//
// DNBSysIRunnable.h
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     smw        07/13/2004   Initial Implementation
//
//==============================================================================


#ifndef DNBSysIRunnable_H
#define DNBSysIRunnable_H


#include <DNBSystem.h>
#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>
#include <DNBCountedObject.h>
#include <DNBCountedPointer.h>

/**
  * Pure virtual base class to be implemented by anything that wishes to run on a 
  * separate thread.  A DNBSysThread object is created with ( and therefore is bound to )
  * a DNBSysIRunnable.
  * DNBSysIRunnable needs to use the handle-body mechanism because otherwise:
  *		1) A DNBSysThread has to be created with a reference or a pointer to a DNBSysIRunnable
  *			 ( not an instance of it, because you cannot create an instance of a pure virtual 
  *				class )
  *		2) It is possible for the DNBSysRunnable object to go out of scope and therefore be deleted
  *           before the thread which uses it.  For example, when an array of threads are declared
  *           then bound to runnables inside a for loop. Because the runnables are also created
  *			  inside the for loop, they are destoyed upon loop exit.
  * For more information on the handle-Body mechanism, see DNBCountedPointer.
  */

class /*ExportedByDNBSystem*/ DNBSysIRunnableBody : public DNBCountedObject
// activate ExportedBy macro after I have a cpp file
{

public:

	virtual 
	void     
	initialize() = 0;  // FIXME: not neeeded so far; remove


/* return values was unsigned int; win32 thread functions return a DWORD ( unsigned 32 bit int ); 
   pthreads functions return a void *; On     window, we can then just retrun the address of the usnigned int 
  cast to a void * 
*/
   virtual 
   void*    
   run(void) = 0; 
   

   virtual 
	   void     
   finalize() = 0;		// FIXME: not needed so far; remove

};


typedef DNBCountedPointer< DNBSysIRunnableBody > DNBSysIRunnable;

#endif /* DNBSysIRunnable_H */
