//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//
//  Notes:
//    + None
//
//  TODO:
//    + Nothing
//
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_CLASSINFO_H_
#define _DNB_CLASSINFO_H_


#include <DNBSystemBase.h>
#include <scl_iosfwd.h>
#include <scl_string.h>
#include <scl_vector.h>
#include <scl_typeinfo.h>
#include <DNBSystemDefs.h>


#include <DNBSystem.h>


#include <DNBException.h>


//
//  Provide the necessary forward declarations.
//
class DNBDynamicObject;




//
//  This class stores the runtime information of a class that is derived from
//  DNBDynamicObject.  The class can be used to obtain information about an
//  object or its base class(es) at run time.  The ability to determine the
//  class of an object at run time is useful when extra type checking is needed,
//  or when a developer must write special-purpose code based on the class of
//  an object.  Run-time class information is not supported directly by the C++
//  language.
//
class ExportedByDNBSystem DNBClassInfo
{
public:
    typedef DNBDynamicObject *  CreateMethod( void );

    DNBClassInfo( const scl_type_info &classID, size_t objectSize,
        CreateMethod *createMethod, const DNBClassInfo *base1Info,
        const DNBClassInfo *base2Info )
        DNB_THROW_SPEC((scl_bad_alloc));

    DNBClassInfo( const DNBClassInfo &right )
        DNB_THROW_SPEC((scl_bad_alloc));

    ~DNBClassInfo( )
        DNB_THROW_SPEC_NULL;

    DNBClassInfo &
    operator=( const DNBClassInfo &right )
        DNB_THROW_SPEC((scl_bad_alloc));

    inline bool
    operator==( const DNBClassInfo &right ) const
        DNB_THROW_SPEC_NULL;

    inline bool
    operator!=( const DNBClassInfo &right ) const
        DNB_THROW_SPEC_NULL;

    inline bool
    operator< ( const DNBClassInfo &right ) const
        DNB_THROW_SPEC_NULL;

    inline bool
    isIdentifiable( ) const
        DNB_THROW_SPEC_NULL;

    inline scl_string
    getClassName( ) const
        DNB_THROW_SPEC_NULL;

    inline size_t
    getObjectSize( ) const
        DNB_THROW_SPEC_NULL;

    inline size_t
    getBaseCount( ) const
        DNB_THROW_SPEC_NULL;

    inline const DNBClassInfo &
    getBaseInfo( size_t idx ) const
        DNB_THROW_SPEC_NULL;

    bool
    isInstanceOf( const DNBClassInfo &other ) const
        DNB_THROW_SPEC_NULL;

    bool
    isDerivedFrom( const DNBClassInfo &other ) const
        DNB_THROW_SPEC_NULL;

    inline bool
    isConstructable( ) const
        DNB_THROW_SPEC_NULL;

    DNBDynamicObject *
    createObject( ) const
        DNB_THROW_SPEC((scl_bad_alloc, DNBException));

    friend
    ExportedByDNBSystem ostream &
    operator<<( ostream &ostr, const DNBClassInfo &item )
        DNB_THROW_SPEC_NULL;

private:
    //
    //  Define some utility routines.
    //
    static  scl_string
    FilterClassName( const char *className )
        DNB_THROW_SPEC((scl_bad_alloc));

    typedef scl_vector<const DNBClassInfo *, DNB_ALLOCATOR(const DNBClassInfo *) >
        BaseInfoDB;

    typedef BaseInfoDB::iterator        BaseIterator;

    typedef BaseInfoDB::const_iterator  BaseConstIterator;

    const scl_type_info    *classID_;
    scl_string              className_;
    size_t              objectSize_;
    CreateMethod       *createMethod_;
    BaseInfoDB          baseInfoDB_;
};




//
//  Include the public definition file.
//
#include "DNBClassInfo.cc"




#endif  /* _DNB_CLASSINFO_H_ */
