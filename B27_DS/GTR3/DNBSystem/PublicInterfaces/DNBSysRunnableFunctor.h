//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
//
// DNBSysRunnableFunctor.h
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     smw        07/13/2004   Initial Implementation
//
//==============================================================================


#ifndef DNBSysRunnableFunctor_H
#define DNBSysRunnableFunctor_H


#include <DNBSystem.h>
#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>
#include <DNBCallback.h>
#include <DNBSysIRunnable.h>
#include <DNBException.h>

/**
* This class is used to define the work to be done by a DNBSysThread, when the work to be done is a 
* function which takes no arguments and returns no value.  This function type is DNBFunctor0.
* ( Class should be renamed DNBSysRunnableFunctor0 ? )
*/
class ExportedByDNBSystem DNBSysRunnableFunctorBody: public DNBSysIRunnableBody
{
	
public:

    
    // Constructors
	
	/**
	* Constructs a DNBSysRunnableFunctor with a default DNBFunctor0, which is a handle to a NULL body.
	*/ 
	DNBSysRunnableFunctorBody ( );
	  

	 /**
	 * Constructs a DNBSysRunnableFunctorBody by copying the DNBFunctor0 given as an argument.
	 * Since DNBFunctor0 is a reference-counted object, this simply increments its count
	 * NOT: DNBFunctors are strange: they look like they are supposed to be reference counted,
	 * but they are not: copy constructor actually clones the body, and destructor deletes it???
	 * Yes now I remember: it is that way by design, according to JOD, to allow some kind 
	 * of interesting type of inheritance and specialization??
	 */ 
	 DNBSysRunnableFunctorBody ( DNBFunctor0 functor ) ; 
	  
	  
	 // Destructor 
	 ~DNBSysRunnableFunctorBody() 
		DNB_THROW_SPEC_NULL  ;
	  
	  	  
	  
	  // Member Functions
	  
	  void *
	  run()
		DNB_THROW_SPEC ( ( DNBEInternalError ) );
	  

	  void initialize() ;  // so far not needed; delete
	  
	  void finalize() ; // so far not needed; delete
	  

	  // Global functions are friends
	  friend 
	  ExportedByDNBSystem
	  bool
      operator==( DNBSysRunnableFunctorBody& rf1, DNBSysRunnableFunctorBody& rf2 );

	  friend
	  ExportedByDNBSystem
	  bool
	  operator<( DNBSysRunnableFunctorBody& rf1, DNBSysRunnableFunctorBody& rf2 );



	  
private:
    
    DNBFunctor0         functor_;   
	

	// Prohibit copy construction and assignment operator; Remove implementation once approved

	// Copy constructor
	DNBSysRunnableFunctorBody ( DNBSysRunnableFunctorBody& second )
	{
		functor_ = second.functor_;
	};

	
	// Assignment operator
	 void
	 operator=(const DNBSysRunnableFunctorBody& second)
	 {
		  functor_ = second.functor_;
	 };


	 void *
	 internal_run()
	 DNB_THROW_SPEC ( ( DNBEInternalError ) );
    
};




// Global Functions using DNBSysRunnableFunctorBody

/**
  * Equality operator
  */

ExportedByDNBSystem
bool
operator==( DNBSysRunnableFunctorBody& rf1,  DNBSysRunnableFunctorBody& rf2 );


/**
  * Inequality operator; allows for having ordered sets of DNBSysRunnableFunctions
  */
ExportedByDNBSystem
bool
operator<( DNBSysRunnableFunctorBody& rf1, DNBSysRunnableFunctorBody& rf2 )  ;



// Global Make Functions for convenience of creating handles to runnables from global and member functions

template < class functionType >
DNBSysIRunnable 
DNBMakeRunnableFunctorG( functionType function );


template < class functionType, class arg1Type >
DNBSysIRunnable 
DNBMakeRunnableFunctorG( functionType function, arg1Type arg1 );


template < class functionType, class arg1Type, class arg2Type>
DNBSysIRunnable 
DNBMakeRunnableFunctorG( functionType function, arg1Type arg1 , arg2Type arg2 );


template < class functionType, class arg1Type, class arg2Typpe, class arg3Type >
DNBSysIRunnable 
DNBMakeRunnableFunctorG( functionType function, arg1Type arg1, arg2Typpe arg2, arg3Type arg3 );


template < class functionType, class arg1Type, class arg2Typpe, class arg3Type, class arg4Type >
DNBSysIRunnable 
DNBMakeRunnableFunctorG( functionType function, arg1Type arg1, arg2Typpe arg2, arg3Type arg3, arg4Type arg4 );


template < class functionType, class arg1Type, class arg2Typpe, class arg3Type, \
			class arg4Type,
			class arg5Type >
DNBSysIRunnable
DNBMakeRunnableFunctorG( functionType function, arg1Type arg1, arg2Typpe arg2, arg3Type arg3, \
						arg4Type arg4,
						arg5Type arg5 );


// FIXME - Repeat for member functions


// inlcude the definition of the above templated funtions

#include <DNBSysRunnableFunctor.cc>


#endif /* DNBSysRunnableFunctor */
