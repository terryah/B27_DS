//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
//
// DNBSysPCBuffer.h
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     smw        12/20/2003   Initial Implementation
/**
 * @fullreview //smw 03:12:23
 * @quickreview //jod 04:02:02
 */
//==============================================================================

#ifndef DNB_SYS_PC_QUEUE_H
#define DNB_SYS_PC_QUEUE_H

#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>
#include <DNBSystem.h>
#include <DNBSysMutexLock.h>
#include <DNBSysCondition.h>
#include <DNBSysLockGuard.h>
#include <DNBSysMonitor.h>

class ExportedByDNBSystem DNBSysPCBuffer: public DNBSysMonitor
{
public:

    /*
     * Returns the capacity of the buffer
     */
    size_t
    getCapacity() const
    DNB_THROW_SPEC_NULL;


    /*
     * Sets the capacity of the buffer
     */
    void
    setCapacity( size_t aSize )
    DNB_THROW_SPEC_NULL;


    /*
     * Closes the buffer to prohibit future data transfers. Any attempt to write to a closed 
     * buffer, or to read from a closed buffer that is already empty, produces an exception. 
     * Any entries already stored within the buffer at the time of its closing may still be 
     * read from without producing an exception. 
     * If the buffer is empty and there are threads waiting to peek or to read from the buffer,
     * this function unblocks these threads and each exits its peek or read function with an 
     * exception. If the buffer is full and there are threads waiting to write to the buffer, 
     * this function unblocks these threads and each exits its write function with an 
     * exception.
     */

    /*void
    close()
    DNB_THROW_SPEC_NULL;


    void
    open()
    DNB_THROW_SPEC_NULL;


    bool
    isOpen() const
    DNB_THROW_SPEC_NULL;*/



    // pure virtual functions
    virtual bool 
    canRead() const
    DNB_THROW_SPEC_NULL = 0;


    virtual bool 
    canWrite() const 
    DNB_THROW_SPEC_NULL = 0;


    virtual size_t 
    entries() const
    DNB_THROW_SPEC_NULL = 0;


    virtual void
    flush()  
    DNB_THROW_SPEC_NULL = 0;


    // callback-related methods not yet implemented ( apparently not being used anyway ).
    // Need a DNBSysCallBack/functor class or implement using the RWFunctors


    virtual
    ~DNBSysPCBuffer ()
    {
    };

protected:

    DNBSysPCBuffer ( size_t maxSize = 0/*, bool isOpen = true*/ );  // maxSize of 0 means unlimited size

    DNBSysCondition    notEmpty_;
    DNBSysCondition    notFull_;

    //bool closed_;
    size_t maxEntries_;
};

#endif // DNB_SYS_PC_QUEUE_H
