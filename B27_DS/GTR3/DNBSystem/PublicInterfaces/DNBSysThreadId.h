//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
//
// DNBSysThreadId.h
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     smw        12/20/2003   Initial Implementation
//
//==============================================================================


#ifndef DNBSysThreadId_H
#define DNBSysThreadId_H


#include <DNBSystem.h>
#include <scl_iosfwd.h>                 // for ostream
#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>
#include <DNBSysThreadDefs.h>

class ExportedByDNBSystem DNBSysThreadId
{


public:

#if     DNB_HAS_WIN32_THREADS
    typedef DWORD         NativeThreadIdRep;
#elif   DNB_HAS_POSIX_THREADS
    typedef pthread_t     NativeThreadIdRep;
#endif

// Constructors
DNBSysThreadId();

DNBSysThreadId(const NativeThreadIdRep& threadId);


// Destructor 
~DNBSysThreadId() {};


// Member Operators
DNBSysThreadId&
operator=(const DNBSysThreadId& second);

const NativeThreadIdRep &
getNativeThreadIdRep() const;

// FIXME: consider moving these to be global functions
bool
operator==(const DNBSysThreadId& second) const;

bool
operator!=(const DNBSysThreadId& second) const;

bool
operator<(const DNBSysThreadId& second) const;

// Member Functions
void
clear(void);

unsigned
hash() const;

bool
isValid() const;

void
setValid(bool val);



private:

    NativeThreadIdRep threadId_;
    bool                isValid_;
    
};

// Globals
// platform independent function to return the running thread id

DNBSysThreadId ExportedByDNBSystem
DNBSysGetThreadId();

// Streaming 
ExportedByDNBSystem
ostream&
operator<<(ostream& str,DNBSysThreadId aThreadId);

#endif  // DNBSysThreadId_H
