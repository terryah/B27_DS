//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 */

//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     jad         08/18/97    Initial implementation.
//*     bkh         10/31/03    Implementation of new documentation style.
//* 
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_MSGBASE_H_
#define _DNB_MSGBASE_H_


#include <DNBSystemBase.h>
#include <scl_string.h>
#include <DNBSystemDefs.h>


#include <DNBSystem.h>




/**
  * Defines literal text for a localized message.
  * @param str
  * The character or string literal which is inserted into a message.
  * 
  * @return
  * The single-byte or wide-character representation of the literal <tt>str</tt>.
  * 
  * <br><B>Description</B><br>
  * This macro converts the specified character or string literal to a
  * representation that is compatible with the message library.  It is
  * typically used to initialize objects of type @href DNBMsgBase::CharType
  * and @href DNBMsgBase::StringType.  By using this macro, developers can
  * build single-byte and wide-character applications from the same source
  * code.
  * 
  * This macro operates as follows: If the macro @href DNB_USE_UNICODE
  * returns TRUE, the parameter <tt>str</tt> is converted to its equivalent
  * wide-character representation.  Otherwise, <tt>str</tt> is returned in its
  * (default) single-byte form.
  * 
  * <br><B>Example</B><br>
  * <pre>
  * DNBMsgBase::CharType    ch = DNB_TEXT('c');
  * DNBMsgBase::CharType   *s1 = DNB_TEXT("hello");
  * DNBMsgBase::StringType  s2( DNB_TEXT("hello") );
  * 
  * </pre>
  *
  */
#if     DNB_USE_UNICODE
#define DNB_TEXT(str)       L ## str
#else
#define DNB_TEXT(str)       str
#endif




/**
  * The base class for the message facility.
  * 
  * <br><B>Description</B><br>
  * This class defines various characteristics (or traits) of the message
  * library.  It represents the common base class for localized messages of
  * type @href DNBMessage, localized data elements of type @href DNBMsgData,
  * and locale objects of type @href DNBLocale.  The @href DNB_USE_UNICODE
  * macro specifies the character type used by the message library.  If the
  * macro returns TRUE, messages are represented using wide-character
  * strings (WCS).  Such strings may contain symbols from the ISO-10646
  * (UNICODE) character set.  If the macro returns FALSE, messages are
  * represented using single-byte character strings (SBCS).  Such strings
  * may contain symbols from any ISO-8859 character set.
  * 
  * Note: Instances of this class are generally not defined in client code.
  * 
  * 
  */
class ExportedByDNBSystem DNBMsgBase
{
public:
/**
  * The character type used by the message library.
  * 
  * <br><B>Description</B><br>
  * This type definition specifies the character type used by the
  * message library.  If the macro @href DNB_USE_UNICODE returns TRUE,
  * the base type is <tt>wchar_t</tt>.  Otherwise, the base type is <tt>char</tt>.
  * 
  * 
  */
#if DNB_USE_UNICODE
    typedef wchar_t     CharType;
#else
    typedef char        CharType;
#endif


/**
  * The string type used by the message library.
  * 
  * <br><B>Description</B><br>
  * This type definition specifies the string type used by the message
  * library.  If the macro @href DNB_USE_UNICODE returns TRUE, the base
  * type is <tt>scl_wstring</tt>.  Otherwise, the base type is <tt>string</tt>.
  * 
  * 
  */
#if DNB_USE_UNICODE
    typedef scl_wstring     StringType;
#else
    typedef scl_string      StringType;
#endif


/**
  * The size type used by the message library.
  * 
  * <br><B>Description</B><br>
  * This type definition specifies the base type used by the message
  * library to represent length parameters (e.g., field widths and
  * floating-point precisions).
  * 
  * 
  */
    typedef unsigned    SizeType;


/**
  * The alignment modes supported by the message library.
  * 
  * <br><B>Description</B><br>
  * This enumeration type specifies the various alignment schemes that
  * may be used to format an arbitrary data element.
  * 
  * 
  */
    enum AlignmentType
    {
/**
  * Indicates the data element should be left-justified within the
  * field.
  * 
  * 
  */
        Left,

/**
  * Indicates the data element should be centered within the field.
  * 
  * 
  */
        Center,

/**
  * Indicates the data element should be right-justified within the
  * field.
  * 
  * 
  */
        Right
    };


/**
  * The format symbols supported by the message library.
  * 
  * <br><B>Description</B><br>
  * This enumeration type specifies the various symbols that may be used
  * to format a numeric value (such as currency).
  * 
  * 
  */
    enum SymbolType
    {
/**
  * Indicates that no symbol should be used to format the numeric
  * value.
  * 
  * <br><B>Description</B><br>
  * This enumeration constant specifies that no symbol should be
  * applied to the localized representation of the numeric value.
  * For example, in the US locale, $10.00 is represented as "10.00".
  * 
  * 
  */
        NoSymbol = 0,

/**
  * Indicates the local symbol should be used to format the numeric
  * value.
  * 
  * <br><B>Description</B><br>
  * This enumeration constant specifies that the local symbol should
  * be applied to the localized representation of the numeric value.
  * For example, in the US locale, $10.00 is represented as
  * "$10.00".
  * 
  * 
  */
        LocalSymbol,

/**
  * Indicates the international symbol should be used to format the
  * numeric value.
  * 
  * <br><B>Description</B><br>
  * This enumeration constant specifies that the international
  * symbol should be applied to the localized representation of the
  * numeric value.  For example, in the US locale, $10.00 is
  * represented as "USD 10.00".
  * 
  * 
  */
        IntlSymbol
    };


/**
  * The format type used by the message library.
  * 
  * <br><B>Description</B><br>
  * This type definition specifies the base type used by the message
  * library to represent format characters.  Such characters are used
  * to format date and time quantities.  The meanings assigned to the
  * format characters are identical to those used in the Standard C
  * Library function <tt>strftime()</tt>.
  * 
  * 
  */
    typedef char        FormatType;


/**
  * The format character for the locale-specific date.
  * 
  * <br><B>Description</B><br>
  * This object specifies the format character which produces the
  * locale-specific date.  It is equivalent to the format character 'x'.
  * 
  * 
  */
    static const FormatType DateOnly;


/**
  * The format character for the locale-specific time.
  * 
  * <br><B>Description</B><br>
  * This object specifies the format character which produces the
  * locale-specific time.  It is equivalent to the format character 'X'.
  * 
  * 
  */
    static const FormatType TimeOnly;


/**
  * The format character for the locale-specific date and time.
  * 
  * <br><B>Description</B><br>
  * This object specifies the format character which produces the
  * locale-specific date and time.  It is equivalent to the format
  * character 'c'.
  * 
  * 
  */
    static const FormatType DateTime;


/**
  * Constructs a basic message.
  * 
  * @return
  * Nothing
  * 
  * <br><B>Description</B><br>
  * This function constructs a basic message.
  * 
  * 
  */
    DNBMsgBase( ) DNB_THROW_SPEC_NULL;


/**
  * Constructs a basic message using copy semantics.
  * @param right
  * The basic message to be copied.
  * 
  * @return
  * Nothing
  * 
  * <br><B>Description</B><br>
  * This function copies the attributes of <tt>right</tt> to <tt>self</tt>.
  * 
  * 
  */
    DNBMsgBase( const DNBMsgBase &right ) DNB_THROW_SPEC_NULL;


/**
  * Destroys a basic message.
  * 
  * @return
  * Nothing
  * 
  * <br><B>Description</B><br>
  * This function destroys a basic message.
  * 
  * 
  */
    virtual
    ~DNBMsgBase( ) DNB_THROW_SPEC_NULL;


/**
  * Copies a basic message to <tt>self</tt>.
  * @param right
  * The basic message to be copied.
  * 
  * @return
  * Nothing
  * 
  * <br><B>Description</B><br>
  * This function copies the attributes of <tt>right</tt> to <tt>self</tt>.
  * 
  * 
  */
    void
    operator=( const DNBMsgBase &right ) DNB_THROW_SPEC_NULL;
};


#endif  /* _DNB_MSGBASE_H_ */
