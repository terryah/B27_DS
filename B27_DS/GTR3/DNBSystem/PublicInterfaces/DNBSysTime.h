//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
//
// DNBSysTime.h
//
// ==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     smw        02/27/2004   Initial Implementation
//
// =============================================================================
/**
 * @fullreview SMW JOD 04:03:31
 */
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNBSYSTIME_H_
#define _DNBSYSTIME_H_


#include <DNBSystemBase.h>
#include <scl_iosfwd.h>                 // for ostream
#include <scl_string.h>                 // for scl_string, scl_wstring
#include <time.h>                       // for time_t, struct tm
#include <sys/timeb.h>                  // for struct timeb
#include <DNBSystemDefs.h>


#include <DNBSystem.h>




class ExportedByDNBSystem DNBSysTime
{
public:
   
    enum
    {
        //
        // Global constants
        //
        HoursInDay =                    24,
        MinutesInHour =                 60,
        SecondsInMinute =               60,
        MillisecondsInSecond =        1000,
        MillisecondsInMinute =       60000,
        MillisecondsInHour   =     3600000,
        MillisecondsInDay =       86400000,   // hoursInDay * minutesInHour * secondsInMinute * millisecondsInSecond

        //
        //  time field limits.
        //
        HourFieldMin       =        0,
        MinuteFieldMin     =        0,
        SecondFieldMin     =        0,
        MillisecondFieldMin=        0,

        HourFieldMax       =       HoursInDay - 1,
        MinuteFieldMax     =       MinutesInHour - 1,
        SecondFieldMax     =       SecondsInMinute - 1,
        MillisecondFieldMax=       MillisecondsInSecond - 1,

        //
        // Miscellaneous 
        //
        MillisecondsMax       =       MillisecondsInDay - 1,
        InvalidTime   =       -1

    };

    /**
      * A structure to contain the broken-down time fields.
      * It is used as a more efficient way to get all the time fields at once
      * in the method getTime()
      */
    struct TimeInfo
    {
        int     HourField;              // Hours since midnight
        int     MinuteField;            // Minutes since the hour
        int     SecondField;            // Seconds since the minute
        int     MillisecondField;       // Milliseconds since the second
    };



    //
    //  Constructors, assignment operator, and destructor.
    //

    /**
      * Default constructor.  Constructs an invalid time.
      */
    DNBSysTime( )
        DNB_THROW_SPEC_NULL;


    /**
      * Copy constructor.
      */
    DNBSysTime( const DNBSysTime &right )
        DNB_THROW_SPEC_NULL;


    /**
      * Constructs <self> given number of milliseconds since midnight
      */
    explicit
    DNBSysTime( int milliseconds )
        DNB_THROW_SPEC_NULL; 


    /**
      * Constructs <self> given the hour, minute, second and millesond fields since midnight
      */
    DNBSysTime( int hourF, 
                int minuteF, 
                int secondF, 
                int millisecondF = 0) 
        DNB_THROW_SPEC_NULL ;


    /*
     * Constructs <self> given an ANSI C struct tm time object.  This time object is assumed to contain
     * local time
     */
    DNBSysTime ( const tm &tmstruct, int millisecondF = 0) 
        DNB_THROW_SPEC_NULL;


    /*   
     * Constructs <self> from an ANSI C time_t time value, which contains the number of seconds since 
     * the Unix Epoch, UTC.  The constructed time will be the corresponding local time.
     */
    DNBSysTime (const time_t &tt, int millisecondF )   // Could not default the millisecondF to 0 because on aix , 
						       // a time_t is an int and therefore cannot resolve this constructor
						      //  from the one that takes an int
        DNB_THROW_SPEC_NULL;


    /*
     * Assignment operator.
     */
    DNBSysTime &
    operator=( const DNBSysTime &right )
        DNB_THROW_SPEC_NULL;


    /*
     * Destructor.
     */
    virtual ~DNBSysTime( )
        DNB_THROW_SPEC_NULL;


    /*
     * Clears the value of <self> resulting in an invalid Time.
     */
    void
    clear( )
        DNB_THROW_SPEC_NULL;


    //
    //  Mutator methods.
    //

    /*
     * Initializes <self> given the number of milliseconds since midnight
     */
    bool
    setTime( int milliseconds )
        DNB_THROW_SPEC_NULL;


    /** 
      * Initializes <self> given the hour, minute, second and millisecond fields since midnight
      */
    bool
    setTime( int hourF, int minuteF, int secondF, int millisecondF = 0 )
        DNB_THROW_SPEC_NULL;

 
    /*
     * Initializes <self> from an ANSI C struct tm time object. This time object is assumed to contain
     * local time
     */
    bool
    setCTime( const tm &stime, int millisecondF =0 )
        DNB_THROW_SPEC_NULL;


    /*
     * Initializes <self> from an ANSI C time_t time value, which contains the number of seconds 
     * since the Unix Epoch, UTC.
     * The local time which corresponds to the time_t value will be set.
     */
    bool
    setCTime( const time_t &tt, int millisecondF =0 )
        DNB_THROW_SPEC_NULL;



    //
    //  Accessor methods.
    //


    /*
     * Returns <self> as number of milliseconds since midnight
     */
    void
    getTime ( int& milliseconds )
        DNB_THROW_SPEC_NULL;

    /*
     * Returns <self> as number of hours, minutes, seconds and milliseconds since midnight
     */
    void
    getTime ( int& hourField, int& minuteField, int& secondField, int& millisecondField )
        DNB_THROW_SPEC_NULL;



    /* Returns the ANSI C time object corresponding to <self>, with the date
     * field set to today and tm_isdst set to -1.
     */
    void
    getCTime( tm &stime ) const
        DNB_THROW_SPEC_NULL;


    /*
     * Returns the ANSI C time value corresponding to the time value in <self> on today.
     */
    void
    getCTime( time_t &tt) const
        DNB_THROW_SPEC_NULL;



    /*
     * Returns the time as a TimeInfo structure
     */
    void
    getTime ( TimeInfo& info )
        DNB_THROW_SPEC_NULL;



    /*
     * Returns the hour field of <self>
     */
    int
    getHourField( ) const
        DNB_THROW_SPEC_NULL;



    /*
     * Returns the minute field of <self>
     */
    int
    getMinuteField( ) const
        DNB_THROW_SPEC_NULL;


    /*
     * Returns the second field of <self>
     */
    int
    getSecondField( ) const
        DNB_THROW_SPEC_NULL;

    /*
     * Returns the millisecond field of <self>
     */
    int
    getMillisecondField( ) const
        DNB_THROW_SPEC_NULL;



    /**
     * Returns self as number of milliseonds since midnight.  Same as getTime ( int & ).
     */
    int
    getMilliseconds() const DNB_THROW_SPEC_NULL { return msec_; };




    /**
     * Returns true if <self> is a valid Time.
     */
    bool
    isValid( ) const
        DNB_THROW_SPEC_NULL;




    /**
     * Returns true if <self> is an afternoon time
     */
    bool
    isPM ()
        DNB_THROW_SPEC_NULL
        { return ( getHourField() >= 12 ); }



    /**
     * Returns true if <self> is a morning time
     */
    bool
    isAM ()
        DNB_THROW_SPEC_NULL
        { return ( ! isPM() ); }


    /**
     * Returns <self> formatted into a string using a format specified by the argument
     * <format>.  The format convention follows that of the standard C strftime function
     * A default format is used if none is specified.
     */
    scl_string
    toString( const char *format = "%H:%M:%S" ) const
        DNB_THROW_SPEC_NULL;

    // FIXME: we currently do not format milliseconds, because the code to do this type of formatting
    // belongs in a DNBLocal object to be completed in a future project.


    /**
     * Returns <self> formatted into a wide string using a format specified by the argument
     * <format>.  The format convention follows that of the standard C strftime function
     * A default format is used if none is specified.
     */
    scl_wstring
    toWString( const wchar_t *format ) const
        DNB_THROW_SPEC_NULL;

    //
    //  Relational operators.
    //

    /**
      * Returns true if <self> is equal to <right>
      */
    bool
    operator==( const DNBSysTime &right ) const
        DNB_THROW_SPEC_NULL;


    /**
      * Returns true if <self> is not equal to <right>
      */
    bool
    operator!=( const DNBSysTime &right ) const
        DNB_THROW_SPEC_NULL;


    /**
      * Returns true if <self> is strictly less than <right>
      */
    bool
    operator< ( const DNBSysTime &right ) const
        DNB_THROW_SPEC_NULL;


    /**
      * Returns true if <self> is less than or equal to <right>
      */
    bool
    operator<=( const DNBSysTime &right ) const
        DNB_THROW_SPEC_NULL;

    /**
      * Returns true if <self> is strictly greater than <right>
      */
    bool
    operator> ( const DNBSysTime &right ) const
        DNB_THROW_SPEC_NULL;


    /**
      * Returns true if <self> is greater than or equal to <right>
      */
    bool
    operator>=( const DNBSysTime &right ) const
        DNB_THROW_SPEC_NULL;


    //
    //  Arithmetic 
    //

    /**
      * Adds <count> hours to <self> and returns the result.
      */
    DNBSysTime &
    addHours( int count )
        DNB_THROW_SPEC_NULL;


    /**
      * Adds <count> minutes to <self> and returns the result.
      */
    DNBSysTime &
    addMinutes( int count )
        DNB_THROW_SPEC_NULL;


    /** 
      * Adds <count> seconds to <self> and returns the result.
      */
    DNBSysTime &
    addSeconds( int count )
        DNB_THROW_SPEC_NULL;


    /**
      * Adds <count> milliseconds to <self> and returns the result.
      */
    DNBSysTime &
    addMilliseconds( int count )
        DNB_THROW_SPEC_NULL;


    //
    //  Static methods.
    //

    /**
      * Returns a time object with the current local time.
      */
    static  DNBSysTime
    Now( )
        DNB_THROW_SPEC_NULL;

    /**
      * Return true if the given argument is a valid hour field
      */
    static bool 
    IsHourField ( int hour ) 
        DNB_THROW_SPEC_NULL 
        { return hour <= HourFieldMax ; };


     /**
     * Return true if the given argument is a valid minute field
     */
    static bool 
    IsMinuteField ( int minute ) 
        DNB_THROW_SPEC_NULL    
        { return minute <= MinuteFieldMax; };


    /**
      * Return true if the given argument is a valid second field
      */
    static bool 
    IsSecondField ( int second ) 
        DNB_THROW_SPEC_NULL 
        { return second <= SecondFieldMax; };


   /**
    * Return true if the given argument is a valid millisecond field
    */
    static bool 
    IsMillisecondField ( int millisecond)
        DNB_THROW_SPEC_NULL
        { return millisecond <= MillisecondFieldMax; }; 


    /*
     * Returns the maximum possible time in a day    
     */
    static DNBSysTime
    MaxTime() 
        DNB_THROW_SPEC_NULL;


    /*
     * Returns the minimum possible time in a day
     */
    static DNBSysTime
    MinTime() 
        DNB_THROW_SPEC_NULL;


    friend class DNBSysTimeTest;  // for testing protected methods


// protected: // No longer protected since DNBSysDateTime needs these and it no longer derives from DNBSysTime

    typedef DNBInteger32 TimeRep;

    // Below are versions of public counterparts, that return the number of days rolled over, 
    // after the arithmetic was performed.  The return value is also contained in the second 
    // argument <ndays>.
    // These are only needed and used by the DateTime class.

    // Adds <count> hours to <self> and returns the number of days rolled over.
    int
    addHours_( int count )
        DNB_THROW_SPEC_NULL;

    // Adds <count> minutes to <self> and returns the number of days rolled over.
    int 
    addMinutes_( int count )
        DNB_THROW_SPEC_NULL;

    // Adds <count> seconds to <self> and returns the number of days rolled over. 
    int
    addSeconds_( int count )
        DNB_THROW_SPEC_NULL;

    // Adds <count> milliseconds to <self> and returns the number of days rolled over.
    int
    addMilliseconds_( int count )
        DNB_THROW_SPEC_NULL;



private:
    TimeRep    msec_;
    int wrap( DNBInteger64 result);      // wraps the time around and return number of days 
                                         // rolled over, positive number means rolled forward, 
                                         // negative number means rolled back
                                         // The return value is used and needed by the DateTime class.
    
};


//
//  Binary arithmetic operators
//

// deleted due to ambuiguity of meaning
// Will reinstate once we have a timePeriod object

//
//  Misc. global functions
//
ExportedByDNBSystem
ostream &
operator<<( ostream &stream, const DNBSysTime &time )
    DNB_THROW_SPEC_NULL;


#endif  /* _DNBSYSTime_H_ */
