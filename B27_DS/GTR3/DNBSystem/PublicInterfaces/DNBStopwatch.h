/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBStopwatch.h
//      Definition file for DNBStopwatch
//
//==============================================================================
//
// Usage notes:
//
//  This class measures program execution time (in real time) and returns the
//  result in floating-point seconds.  The class provides start(), stop(), and
//  reset() methods, similar to a mechanical stopwatch.
//
//  Example:
//      DNBStopwatch    watch;
//      watch.start();
//      /* computation of interest */
//      watch.stop();
//      cout << "Elapsed time: " << watch.read() << endl;
//.
//
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     arawat      07/01/2003   Initial Implementation
//
//==============================================================================

#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNBSTOPWATCH_H_
#define _DNBSTOPWATCH_H_


#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>

#include <DNBSystem.h>


class ExportedByDNBSystem DNBStopwatch
{
public:
    /**
     * Set the elapsed time to zero and the timing mode to stopped.
     */
    DNBStopwatch( )
        DNB_THROW_SPEC_NULL;

    /**
     * Destroy the stopwatch.
     */
    ~DNBStopwatch( )
        DNB_THROW_SPEC_NULL;

    /**
     * Return true if the stopwatch is running, or false if stopped.
     */
    bool
    isRunning() const
        DNB_THROW_SPEC_NULL;

    /**
     * Begin measurement of execution time and set the timing mode to running.
     * If the stopwatch is initially running, the call has no effect.
     */
    void
    start( )
        DNB_THROW_SPEC_NULL;

    /** Halt measurement of execution time and set the timing mode to stopped.
     *  If the stopwatch is initially stopped, the call has no effect.
     */
    void
    stop( )
        DNB_THROW_SPEC_NULL;

    /**
     * Reset the elapsed time to zero and set the timing mode to stopped.
     */
    void
    reset( )
        DNB_THROW_SPEC_NULL;

    /** 
     * Return the elapsed time in floating-point seconds.
     */
    double
    read( ) const
        DNB_THROW_SPEC_NULL;

    /**
     * Return the elapsed time in integral microseconds (usec).
     */
    DNBInteger64
    readMicro( ) const
        DNB_THROW_SPEC_NULL;

    /**
     * Return the resolution of measured time in floating-point seconds. 
     * A typical value is 1/60 second.
     */
    static double
    GetResolution( )
        DNB_THROW_SPEC_NULL;

    /**
     * Return the resolution of measured time in integral microseconds (usec).
     */
    static DNBInteger32
    GetResolutionMicro( )
        DNB_THROW_SPEC_NULL;

private:

#ifdef _WINDOWS_SOURCE

    /**
     * Start time of Stopwatch
     */
	LARGE_INTEGER			_StartTime;

    /**
     * Stop time of Stopwatch
     */
	LARGE_INTEGER			_StopTime; 

    /**
     * Frequency of the internal Clock. 
     */
	static LARGE_INTEGER	_Frequency; 

    /**
     * Flag to determine whether we are using GetTickCount or not
     */
	static bool				_UsingGetTickCount;
    
#else

	/**
     * Start and Stop time for unix OS
     */
	signed long long 		_StartTime;
	signed long long	 	_StopTime;

#endif

    /** 
     * Flag to check if the clock is running or not
     */

	bool					_IsRunning;  
	
    /**
     * Prohibit copy construction and assignment.
     */
    DNBStopwatch( const DNBStopwatch & )
        DNB_THROW_SPEC_NULL;

    DNBStopwatch &
    operator=( const DNBStopwatch & )
        DNB_THROW_SPEC_NULL;
};




#endif  /* _DNBSTOPWATCH_H_ */
