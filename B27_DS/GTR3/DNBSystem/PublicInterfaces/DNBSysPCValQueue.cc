//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
//
// DNBSysPCValQueue.cc
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     smw        04/12/2004   Initial Implementation
//	   smw        09/20/2004   changed the internal container type to be a templated parameter
//							   Changed the method names to follow STL convention (e.g. push instead of read, etc..)

/**
 * @fullreview smw 03:12:23    // not yet reviewed
 * @quickreview jod 04:02:02   // not yet reviewed
 */
//==============================================================================


#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>
#include <DNBSystem.h>

#include <DNBSysPCBuffer.h>

template < class Type, class Sequence >
DNBSysPCValQueue< Type, Sequence >::DNBSysPCValQueue (  size_t maxSize ): 
    DNBSysPCBuffer( maxSize ),
    queue_ (),
	isClosed_ ( false )
{

}



template < class Type, class Sequence >
DNBSysPCValQueue<Type, Sequence >::~DNBSysPCValQueue()
{

}


/**
  * Returns a copy of the next available element in the queue, if any.  
  * Returns NULL otherwise
  */
template < class Type, class Sequence >
Type    
DNBSysPCValQueue<Type, Sequence >::peek() const
DNB_THROW_SPEC_NULL
{
    DNBSysLockGuard guard ( monitor() );
	if ( ! isClosed_ )
	{
		if ( canRead() ) 
			return queue_.front();
		else
			return NULL;
	}
	else
		return NULL;

}




/**
  * If there is an available element in the queue, returns TRUE and copies the element 
  * to the element argument passed by reference.  Otherwise returns FALSE.
  */
template < class Type, class Sequence >
bool
DNBSysPCValQueue<Type, Sequence >::tryPeek( Type& element ) const
DNB_THROW_SPEC_NULL
{
    DNBSysLockGuard guard ( monitor() );
	if ( ! isClosed_ )
	{ 
	    if ( canRead() ) 
		{
			element = queue_.front();
			return true;
		}
		else
			return false;
	}
	else
		return false;
}




/**
  * Removes the next element from the queue and returns it.  If the queue is empty, the 
  * calling thread will block until data becomes available on the queue ( as a result of
  * another thread writing to it
  */
template < class Type, class Sequence >
bool
DNBSysPCValQueue<Type, Sequence >::read( Type& element )
DNB_THROW_SPEC_NULL
{
    DNBSysLockGuard guard ( monitor() );
	if ( ! isClosed_ )
	{ 
		while ( ( queue_.size() == 0 ) && ( ! isClosed_ ) ) notEmpty_.wait();
		if ( ! isClosed_ )  // thread woken up by a stop call or by a real not empty signal?
		{
			element = queue_.front();
			queue_.pop_front();
			notFull_.signal();
			return true;
		}
		else // it must have been woken up by a stop call; queue is still empty
		{
			return false;
			// DNBEInternalError e ( DNB_FORMAT( " trying to read from a stopped empty queue " ) );
			// throw ( e );
			// Note: there is a bug with throwing exceptions and pthreads: when two threads are waiting on a
			// condition, then the condition becomes true, the first thread wakes up, throws an exception and exits
			// the second thread may never wake up.  This is why the approach of throwing excpetions has been dismissed
		}
	}
	else
	{
		return false;
		//DNBEInternalError e ( DNB_FORMAT( " Trying to read from a stopped queue " ) );
		//throw ( e );
	}
}



/**
  * If the queue contains a value, this function removes that value from the queue, 
  * copies it into the instance passed as a reference, and returns TRUE to indicate that the read 
  * succeeded. If the buffer is empty, this function immediately returns FALSE 
  * to indicate that the read was unsuccessful. 
  */
template < class Type, class Sequence >
bool
DNBSysPCValQueue<Type, Sequence >::tryRead( Type& element )
DNB_THROW_SPEC_NULL
{
    DNBSysLockGuard guard ( monitor() );
	if ( ! isClosed_ )
	{  
		if ( queue_.size() == 0 ) return false;
		element = queue_.front();
		queue_.pop_front();
		notFull_.signal();
		return true;
	}
	else
		return false;
}




/**
  * Returns true and inserts the supplied element at the next position in the queue,
  * if the queue is open and has available space.
  * If the queue is full, the calling thread blocks until the queue 
  * has available space ( as a result of other thread(s) reading from it.
  * Returns false if the queue is closed.
  * If the queue is closed while a thread is blocked trying to write to it,
  * the thread unblocks and exits returning false.
  */
template < class Type, class Sequence >
bool
DNBSysPCValQueue<Type, Sequence >::write( Type element )
DNB_THROW_SPEC_NULL
{
    DNBSysLockGuard guard ( monitor() );
	if ( ! isClosed_ )
	{ 
		while ( ( maxEntries_ != 0 ) && ( queue_.size() == maxEntries_ ) &&  ( ! isClosed_ ) ) 
			notFull_.wait();
		if ( ! isClosed_ )
		{
			queue_.push_back ( element );
			notEmpty_.signal();
			return true;
		}
		else // must have been woken up by a stop call while queue is full;
		{
			return false;
			//DNBEInternalError e ( DNB_FORMAT( " Trying to write to a stopped full queue " ) );
			//throw ( e );
		}
	}
	else
	{
		return false;
		//DNBEInternalError e ( DNB_FORMAT( " Trying to write to a stopped queue " ) );
		//throw ( e );
	}
}




/**
  * Returns TRUE  and inserts the supplied element at the next position 
  * in the queue, if the queue is open and has available space.
  * Returns FALSE if the queue is full or closed.
  */
template < class Type, class Sequence >
bool
DNBSysPCValQueue<Type, Sequence >::tryWrite( Type element )
DNB_THROW_SPEC_NULL
{
    DNBSysLockGuard guard ( monitor() );
	if ( ! isClosed_ )
	{ 
	    if ( maxEntries_ != 0 && queue_.size() == maxEntries_ ) return false;
		queue_.push_back ( element );
		notEmpty_.signal();
		return true;
	}
	else
		return false;
}



// Inherited pure virtual functions needed to be implemented here
template < class Type, class Sequence >
bool 
DNBSysPCValQueue<Type, Sequence >::canRead() const
DNB_THROW_SPEC_NULL
{
    DNBSysLockGuard guard ( monitor() ); 
	if ( ! isClosed_ )
	{ 
		return ( queue_.size() != 0 );
	}
	else
		return false;
}


template < class Type, class Sequence >
bool 
DNBSysPCValQueue<Type, Sequence >::canWrite() const 
DNB_THROW_SPEC_NULL
{
    DNBSysLockGuard guard ( monitor() ); 
	if ( ! isClosed_ )
	{ 
		return ( maxEntries_ == 0 || queue_.size() < maxEntries_ );
	}
	else
		return false;
}




template < class Type, class Sequence >
size_t 
DNBSysPCValQueue<Type, Sequence >::entries() const
DNB_THROW_SPEC_NULL
{
    DNBSysLockGuard guard ( monitor() ); 
    return queue_.size();
}




template < class Type, class Sequence >
void
DNBSysPCValQueue<Type, Sequence >::flush()  
DNB_THROW_SPEC_NULL
{
    DNBSysLockGuard guard ( monitor() ); 
    while ( ! queue_.empty() ) queue_.pop_front();
	notFull_.signal();
}



// FIXME methods below could be moved to DNBSysPCBuffer class; or delete that class

template < class Type, class Sequence >
void
DNBSysPCValQueue<Type, Sequence >::close()
    {
        DNBSysLockGuard guard ( monitor() );
        if ( ! isClosed_ )
	{
            isClosed_ = true;
		notEmpty_.signalAll();  // wake up any threads blocked waiting for something to read
		notFull_.signalAll();  // wakup all threads blocked waiting for something to write
		// when the threads wake up. they check again if they can really read / write or not.
		// if not, that means the queue is stopped and they throw an exception.
		// FIXME: Is it necessary to maintain a count of the number of readers
		// and writers like RogueWave does?: signaling when no one is actually wating does not seem to be harmful 
		// ( on Windows at least; check Unix )
	}
    };


template < class Type, class Sequence >
void
DNBSysPCValQueue<Type, Sequence >::open()
    {
        DNBSysLockGuard guard ( monitor() );
        if ( isClosed_ )
            isClosed_ = false;
		//  No need to signal; no threads blocked on a stopped queue.
    };
 

template < class Type, class Sequence >
bool
DNBSysPCValQueue<Type, Sequence >::isClosed()
    {
        DNBSysLockGuard guard ( monitor() );
        return isClosed_;
    };
