//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview RTL trangara 01:11:15
 */
// --------------------------------------------------------


#if defined(__HP_aCC)
#include <DNBDynamicObject.h>
#endif

template <class Object>
inline
DNBCountedPointer<Object>::DNBCountedPointer( const Object *object )
    DNB_THROW_SPEC_NULL :

    DNBCountedBase  ( object )
{
    // Nothing
}


template <class Object>
inline
DNBCountedPointer<Object>::DNBCountedPointer( const DNBCountedBase &right )
    DNB_THROW_SPEC((DNBEInvalidCast)) :

    DNBCountedBase  ( downcast(right) )
{
    // Nothing
}


template <class Object>
inline
DNBCountedPointer<Object>::DNBCountedPointer(
    const DNBCountedPointer<Object> &right )
    DNB_THROW_SPEC_NULL :

    DNBCountedBase  ( right )
{
    // Nothing
}


template <class Object>
inline
DNBCountedPointer<Object>::~DNBCountedPointer( )
    DNB_THROW_SPEC_NULL
{
    // Nothing
}


template <class Object>
inline DNBCountedPointer<Object> &
DNBCountedPointer<Object>::operator=( const Object *object )
    DNB_THROW_SPEC_NULL
{
    DNBCountedBase::operator=( object );

    return *this;
}


template <class Object>
inline DNBCountedPointer<Object> &
DNBCountedPointer<Object>::operator=( const DNBCountedBase &right )
    DNB_THROW_SPEC((DNBEInvalidCast))
{
    //
    //  Note: The following method checks for self-assignment.
    //
    DNBCountedBase::operator=( downcast(right) );

    return *this;
}


template <class Object>
inline DNBCountedPointer<Object> &
DNBCountedPointer<Object>::operator=( const DNBCountedPointer<Object> &right )
    DNB_THROW_SPEC_NULL
{
    //
    //  Note: The following method checks for self-assignment.
    //
    DNBCountedBase::operator=( right );

    return *this;
}


template <class Object>
inline void
DNBCountedPointer<Object>::clear( )
    DNB_THROW_SPEC_NULL
{
    detach( );
}


template <class Object>
inline Object &
DNBCountedPointer<Object>::operator*( ) const
    DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION( isBound() );
    return *DNB_STATIC_CAST( Object *, getReferent() );
}


template <class Object>
inline Object *
DNBCountedPointer<Object>::operator->( ) const
    DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION( isBound() );
    return  DNB_STATIC_CAST( Object *, getReferent() );
}


template <class Object>
inline Object *
DNBCountedPointer<Object>::_getObject( ) const
    DNB_THROW_SPEC_NULL
{
    return DNB_STATIC_CAST( Object *, getReferent() );
}


template <class Object>
inline Object *
DNBCountedPointer<Object>::getObject( ) const
    DNB_THROW_SPEC_NULL
{
    return DNB_STATIC_CAST( Object *, getReferent() );
}


template <class Object>
Object *
DNBCountedPointer<Object>::downcast( const DNBCountedBase &right )
    DNB_THROW_SPEC((DNBEInvalidCast))
{
    DNBCountedObject   *source = right._getReferent( );
    if ( source == NULL )
        return NULL;

#if defined (__HP_aCC)
    Object     *target = DNBDynamicCast<Object *>( source );
#else
    Object     *target = dynamic_cast<Object *>( source );
#endif


    if ( target == NULL )
    {
        DNBEInvalidCast     error( DNB_FORMAT("The source and target pointers \
have incompatible referent types") );
        throw error;
    }

    return target;
}
