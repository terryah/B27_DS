//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
//
// DNBSysDateTime.h
//
// ==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     smw        02/27/2004   Initial Implementation
//     smw        04/26/2004   Changed the class so that it contains a DNBSysDate and a DNBSysTime
//                             instead of deriving from them.  This will avoid the following
//                             confusion: 
//                              1) Previously, the following call returned the GMT time:
//                                 DNBSysDateTime dt ( aDate, aTime );
//                                 int h1 = dt.getHourField();  // this returned the GMT hour, 
//                                                              // which is probably not the intent
//                                 int h2 = dt.getTime().getHourField(); // this is how you get the local hour
//                              2) Currently, dt.getHourField() will not compile.  You have to go through the getTime.
//                              OR we could only expose the methods that make sense from DNBSysDate and DNBSysTime
//                              DNBSysDateTime dt ( aDate, aTime );
//                              int h1 = dt.getHourField();  // this returns the expected local hour, 
//                             
//
// =============================================================================

#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef __DNBSYSDATETIME_H__
#define __DNBSYSDATETIME_H__

#include <DNBSystemBase.h>
#include <scl_string.h>
#include <sys/timeb.h>          // for struct timeb needed to get milliseconds
#if _IRIX_SOURCE
#include <sys/time.h>           // for gettimeofday, needed to get time zone info // FIXME Did not work; try getenv (TIMEZONE)
#endif
#include <scl_ctime.h>
#include <DNBSystemDefs.h>

#include <DNBSystem.h>
#include <DNBSysDate.h>
#include <DNBSysTime.h>
#include <DNBStringUtil.h>



class ExportedByDNBSystem DNBSysDateTime
{

public:


  // Static members

  /**
    * Return a DNBSysDateTime object containing the local date and time
    */
  static DNBSysDateTime Now() DNB_THROW_SPEC_NULL;          





  // constructors

  /* 
   * Constructs an invalid date time object
   */
  DNBSysDateTime() 
      DNB_THROW_SPEC_NULL;


  /*
   * Construct a DateTime object from given Date and Time objects
   * The Date and Time objects interpreted as the local date and time.
   */
  DNBSysDateTime( const DNBSysDate& date, 
                  const DNBSysTime& time )
      DNB_THROW_SPEC_NULL ;


  /*
   * Construct a DateTime object from given struct tm object and optional number of milliseconds field.
   * The <tmstruct> is interpreted as containing local date and time.
   */
  DNBSysDateTime ( const struct tm& tmstruct, int msecF = 0 ) 
      DNB_THROW_SPEC_NULL;


  /*
   * Construct a DateTime object from given time_t value and optional number of milliseconds field.
   * The constructed DateTime are the local date and time which correspond to the time_t value tt
   */
  DNBSysDateTime ( time_t& tt, int msecF = 0 ) 
      DNB_THROW_SPEC_NULL;


  /*
   * copy constructor
   */
  DNBSysDateTime(const DNBSysDateTime& t) 
      DNB_THROW_SPEC_NULL;


  /* 
   * Destructor
   */
  ~DNBSysDateTime() DNB_THROW_SPEC_NULL;


  /* 
   * assignment operator 
   */
  DNBSysDateTime& operator=(const DNBSysDateTime& t) 
  DNB_THROW_SPEC_NULL;


  // Accessor Methods


  /*
   * Returns an instance of DNBSysTime which is a copy of the time component of <self> .
   * The DNBSysTime object contains local time.
   */
  DNBSysTime   
  getTime()  const 
  DNB_THROW_SPEC_NULL;


  /*
   * Returns an instance of DNBSysTime which is a copy of the time component of <self> .
   * The DNBSysTime object contains GMT time.
   */
  DNBSysTime   
  getTimeGMT() const 
  DNB_THROW_SPEC_NULL;



  /*
   * Returns an instance of DNBSysDate which is a copy of the date component of <self> .
   * The DNBSysDate object contains the local date.
   */
  DNBSysDate   
  getDate() const 
  DNB_THROW_SPEC_NULL;



  /*
   * Returns an instance of DNBSysDate which is a copy of the date component of <self> .
   * The DNBSysDate object contains the GMT date.
   */
  DNBSysDate
  getDateGMT() const 
  DNB_THROW_SPEC_NULL;


  
  void
  getCTimeGMT( struct tm & structtm) const 
  DNB_THROW_SPEC_NULL;


  void
  getCTimeLocal( struct tm & structtm) const 
  DNB_THROW_SPEC_NULL;


  /**
    * Returns the time_t value corresponding to self.  Time_t is defined as the number of seconds
    * since the Unix epoch ( Jan 1, 1970 ) UTC.
    */
  void
  getCTime( time_t& tt) const 
  DNB_THROW_SPEC_NULL;




  scl_string        
  toString(const char dateFormat = 'x', const char * timeFormat = "%H:%M:%S %p" ) const
  DNB_THROW_SPEC_NULL;


  scl_wstring        
  toWString(const wchar_t dateFormat = L'x', const wchar_t * timeFormat = L"%H:%M:%S %p") const
  DNB_THROW_SPEC_NULL; 



  /*
   * Returns TRUE if self is during Daylight-Saving Time in the local time zone.
   */
  /*bool 
  isDST() const 
  DNB_THROW_SPEC_NULL;*/ // not possible to do this, on all platforms, without going through 
                         // a struct tm object, which would work only for UNIX times (i.e from 
                         // Jan 1 1970 and till 2039 ?)


  bool          
  isValid() const 
  DNB_THROW_SPEC_NULL
    {  return ( date_.isValid()) && time_.isValid(); }




  // Mutator Methods

  bool
  setDateTime ( const DNBSysDate & d, const DNBSysTime& t )
  DNB_THROW_SPEC_NULL;


  bool
  setCTime( const struct tm & structtm, int msecF = 0 ) 
  DNB_THROW_SPEC_NULL;



  bool
  setCTime( time_t& tt, int msecF = 0 ) 
  DNB_THROW_SPEC_NULL;




  // Arithmetic operators removed due to ambiguity of meaning



  // Arithmetic methods


  // Adds <value> days to <self> and returns the result.
  DNBSysDateTime &
  addDays( int value )
        DNB_THROW_SPEC_NULL { date_.operator+=(value); return ( *this ); };


  // Adds <value> weeks to <self> and returns the result.
  DNBSysDateTime &
  addWeeks( int value )
        DNB_THROW_SPEC_NULL { date_.addWeeks( value ); return ( *this); };

  // Adds <value> months to <self> and returns the result.
  DNBSysDateTime &
  addMonths( int value )
      DNB_THROW_SPEC_NULL { date_.addMonths( value ); return ( *this); };

  // Adds <value> years to <self> and returns the result.
  DNBSysDateTime &
  addYears( int value )
      DNB_THROW_SPEC_NULL { date_.addYears( value ); return ( *this); };


  DNBSysDateTime&        
  addHours( int value ) 
      DNB_THROW_SPEC_NULL;


  DNBSysDateTime&        
  addMinutes( int value ) 
      DNB_THROW_SPEC_NULL;        


  DNBSysDateTime&
  addSeconds( int value ) 
      DNB_THROW_SPEC_NULL;   
  

  DNBSysDateTime&        
  addMilliseconds( int value ) 
      DNB_THROW_SPEC_NULL;



  // Relational operators.


  bool 
  operator< (const DNBSysDateTime& right) const
    DNB_THROW_SPEC_NULL;


  bool 
  operator==(const DNBSysDateTime& right) const
    DNB_THROW_SPEC_NULL;


  bool 
  operator<=(const DNBSysDateTime& right) const 
    DNB_THROW_SPEC_NULL;


  bool 
  operator> (const DNBSysDateTime& right) const 
    DNB_THROW_SPEC_NULL;



  bool 
  operator>=(const DNBSysDateTime& right) const 
    DNB_THROW_SPEC_NULL;



  bool 
  operator!=(const DNBSysDateTime& right) const
    DNB_THROW_SPEC_NULL;


  DNBSysDateTime    
  max(const DNBSysDateTime& dt) const 
    DNB_THROW_SPEC_NULL;


  DNBSysDateTime    
  min(const DNBSysDateTime& dt) const 
    DNB_THROW_SPEC_NULL;


private:

    DNBSysDate date_;     // The GMT date corresponding to this
    DNBSysTime time_;     // The GMT time corresponding to this

   // Helper methods
   void convertToGMT_() DNB_THROW_SPEC_NULL;
   DNBSysDateTime convertToLocal_copy_() const DNB_THROW_SPEC_NULL;
};



// Global functions for binary arithmetic
// Removed due to ambuiguity of meaning

/*
DNBSysDateTime operator+(const DNBSysDateTime& dt1, const DNBSysDateTime& dt2);


DNBSysDateTime operator+(const DNBSysDateTime& dt, int value);


DNBSysDateTime operator-(const DNBSysDateTime& dt1, const DNBSysDateTime& dt2);


DNBSysDateTime operator-(const DNBSysDateTime& t, int value);
*/

// Global Misc. functions

ExportedByDNBSystem 
ostream& 
operator<<(ostream&, const DNBSysDateTime&) 
DNB_THROW_SPEC_NULL;


#endif  /* __DNBSYSDATETIME_H__ */
