//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
#ifndef DNBSysICondition_H
#define DNBSysICondition_H


#include <DNBSystem.h>
#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>
#include <DNBSysThreadDefs.h>
#include <DNBSysIMutexLock.h>



class ExportedByDNBSystem DNBSysICondition
{
public:

    virtual void 
    signal(void) = 0;

    virtual void 
    signalAll(void) = 0;

    virtual void
    wait(void) = 0;

    virtual DNBSysIMutexLock * getLock() = 0;

};


#endif  // DNBSysICondition_H
