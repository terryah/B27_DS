//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 */

//*
//*  HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*      BKH        10/02/03    Intial Implementation
//*      BKH        11/14/03    Replaced DNB_IOSTREAM with os_iosfwd.h      
//*

#include <DNBSystemBase.h>
#include <scl_iosfwd.h>
#include <DNBSystemDefs.h>
#include <DNBSystem.h>

enum DNBTraceLevel
{
    DNBTraceLevelNone     = 0,  // Supress trace messages except Fatal, Error and Warning.
    DNBTraceLevelFatal    = 1,  // Report unrecoverable errors
    DNBTraceLevelError    = 2,  // Report recoverable errors
    DNBTraceLevelWarning  = 3,  // Report insignificant/potential errors
    DNBTraceLevelInfo     = 4,  // Output informational messages
    DNBTraceLevelDebug    = 5,  // Output debug messages
    DNBTraceLevelCall     = 6,  // Output entry/exit messages
    DNBTraceLevelUnspec   = 7   // Trace level unspecified
};

ExportedByDNBSystem DNBTraceLevel
getTraceLevel(const char *level);

ExportedByDNBSystem scl_string
getTraceName(DNBTraceLevel level);

ExportedByDNBSystem ostream &
operator<<( ostream &stream, const DNBTraceLevel &level );



