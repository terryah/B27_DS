//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/

/*********************************************************************************************/
//
// DNBSysThread.cc
//
/*********************************************************************************************/
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     smw        07/13/2004   Initial Implementation
//
/*********************************************************************************************/



/*****************************************************************************************/
// Make function for more straightforward thread creation
/*****************************************************************************************/



// Make a thread from a global function with no return and no args
template < class functionType >
DNBSysThread
DNBMakeThreadG ( functionType function )
{
	// first create the functor
	DNBFunctor0 functor = DNBMakeFunctorG( (DNBFunctor0 *) 0, function );
	// then create the thread
	return DNBMakeThread ( functor );
}


// Make a thread from a global function with no return and takes 1 arg
template < class functionType, class arg1Type >
DNBSysThread
DNBMakeThreadG ( functionType function, arg1Type arg1 )
{
	// first create the functor
	DNBFunctor0 functor = DNBMakeFunctorG( (DNBFunctor0 *) 0, function, arg1 );
	// then create the thread
	return DNBMakeThread ( functor );
}


// Make a thread from a global function with no return and takes 2 arg
template < class functionType, class arg1Type , class arg2Type >
DNBSysThread
DNBMakeThreadG ( functionType function, arg1Type arg1, arg2Type arg2 )
{
	// first create the functor
	DNBFunctor0 functor = DNBMakeFunctorG( (DNBFunctor0 *) 0, function, arg1 , arg2 );
	// then create the thread
	return ( DNBMakeThread ( functor ) );
}

// etc..

// Make threads from a class member functions

template< class Client, class Member >
DNBSysThread
DNBMakeThreadM ( Client* client, Member member )
{
	DNBFunctor0 functor = DNBMakeFunctorM( (DNBFunctor0 *) 0, client, member );
	return ( DNBMakeThread( functor ) );
}


