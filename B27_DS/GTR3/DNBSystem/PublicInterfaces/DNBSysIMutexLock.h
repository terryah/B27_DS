//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
#ifndef DNBSysIMutexLock_H
#define DNBSysIMutexLock_H


#include <DNBSystem.h>
#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>




class ExportedByDNBSystem DNBSysIMutexLock
{
public:
    virtual void
    acquire( ) = 0;

    virtual void
    release( ) = 0;


protected:  // FIXME: delete this ; not needed 

        enum InitState
    {
        NotInitialized = 0,             // Assume zero value in static objects.
        Initialized    = 0xface0001     // Assign magic number in constructor.
    };

};




#endif  // DNBSysIMutexLock_H
