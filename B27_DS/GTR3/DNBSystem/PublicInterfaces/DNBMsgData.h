//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 */

//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     jad         08/18/97    Initial implementation.
//*     bkh         11/04/03    Implementation of new documentation style.
//* 
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_MSGDATA_H_
#define _DNB_MSGDATA_H_


#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>


#include <DNBSystem.h>


#include <DNBLocale.h>
#include <DNBMsgBase.h>




/**
  * The abstract base class for localized data elements.
  * 
  * <br><B>Description</B><br>
  * This class defines the standard attributes and methods of a localized
  * data element.  For each such element, a private class is defined which
  * manages the localized representation of the data element.  This private
  * class has the following properties:
  * 
  * <UL>
  * <LI> It is derived from @href DNBMsgData.
  * <LI> It stores the current value of the data element.
  * <LI> It provides implementations of the @href DNBMsgData::clone and
  * @href DNBMsgData::scribe methods.
  * </UL>
  * 
  * By adhering to these conventions, the data elements associated with a
  * message can be manipulated in a uniform manner.
  * 
  * 
  */
class ExportedByDNBSystem DNBMsgData : public DNBMsgBase
{
public:
/**
  * The default field width for a data element.
  * 
  * <br><B>Description</B><br>
  * This object specifies the default field width for an arbitrary data
  * element.  It is equivalent to zero.
  * 
  * 
  */
    static const SizeType   defaultWidth;


/**
  * The default fill character for a data element.
  * 
  * <br><B>Description</B><br>
  * This object specifies the default fill character for an arbitrary
  * data element.  It is equivalent to a single space character.
  * 
  * 
  */
    static const CharType   defaultFill;


/**
  * The default alignment mode for a data element.
  * 
  * <br><B>Description</B><br>
  * This object specifies the default alignment mode for an arbitrary
  * data element.  It is equivalent to @href DNBMsgBase::Right.
  * 
  * 
  */
    static const AlignmentType  defaultAlignment;


/**
  * The default precision for a data element.
  * 
  * <br><B>Description</B><br>
  * This object specifies the default precision for a floating-point
  * data element.  It is equivalent to <tt>FLT_DIG</tt>, which is defined in
  * <tt><tt>float.h</tt></tt>.
  * 
  * 
  */
    static const SizeType   defaultPrecision;


/**
  * The default format symbol for a data element.
  * 
  * <br><B>Description</B><br>
  * This object specifies the default format symbol for a numeric data
  * element.  It is equivalent to @href DNBMsgBase::LocalSymbol.
  * 
  * 
  */
    static const SymbolType defaultSymbol;


/**
  * The default format character for a data element.
  * 
  * <br><B>Description</B><br>
  * This object specifies the default format character for a date/time
  * data element.  It is equivalent to @href DNBMsgBase::DateTime.
  * 
  * 
  */
    static const FormatType defaultFormat;


/**
  * Constructs a localized data element with the default formatting
  * attributes.
  * 
  * @return
  * Nothing
  * 
  * <br><B>Description</B><br>
  * This function constructs a localized data element with the default
  * formatting attributes.
  * 
  * 
  */
    DNBMsgData( ) DNB_THROW_SPEC_NULL;


/**
  * Constructs a localized data element using copy semantics.
  * @param right
  * The localized data element to be copied.
  * 
  * @return
  * Nothing
  * 
  * <br><B>Description</B><br>
  * This function copies the formatting attributes of <tt>right</tt> to <tt>self</tt>.
  * 
  * 
  */
    DNBMsgData( const DNBMsgData &right ) DNB_THROW_SPEC_NULL;


/**
  * Destroys a localized data element.
  * 
  * @return
  * Nothing
  * 
  * <br><B>Description</B><br>
  * This function destroys a localized data element.
  * 
  * 
  */
    virtual
    ~DNBMsgData( ) DNB_THROW_SPEC_NULL;


/**
  * Retrieves the minimum field width of <tt>self</tt>.
  * 
  * @return
  * The minimum field width of <tt>self</tt>.
  * 
  * <br><B>Description</B><br>
  * This function retrieves the minimum field width of the current data
  * element.
  * 
  * 
  */
    inline  SizeType
    getWidth( ) const DNB_THROW_SPEC_NULL;


/**
  * Retrieves the fill character of <tt>self</tt>.
  * 
  * @return
  * The fill character of <tt>self</tt>.
  * 
  * <br><B>Description</B><br>
  * This function retrieves the fill character of the current data
  * element.
  * 
  * 
  */
    inline  CharType
    getFill( ) const DNB_THROW_SPEC_NULL;


/**
  * Retrieves the alignment mode of <tt>self</tt>.
  * 
  * @return
  * The alignment mode of <tt>self</tt>.
  * 
  * <br><B>Description</B><br>
  * This function retrieves the alignment mode of the current data
  * element.
  * 
  * 
  */
    inline  AlignmentType
    getAlignment( ) const DNB_THROW_SPEC_NULL;


/**
  * Retrieves the floating-point precision of <tt>self</tt>.
  * 
  * @return
  * The floating-point precision of <tt>self</tt>.
  * 
  * <br><B>Description</B><br>
  * This function retrieves the floating-point precision of the current
  * data element.
  * 
  * 
  */
    inline  SizeType
    getPrecision( ) const DNB_THROW_SPEC_NULL;


/**
  * Retrieves the format symbol of <tt>self</tt>.
  * 
  * @return
  * The format symbol of <tt>self</tt>.
  * 
  * <br><B>Description</B><br>
  * This function retrieves the format symbol of the current data
  * element.
  * 
  * 
  */
    inline  SymbolType
    getSymbol( ) const DNB_THROW_SPEC_NULL;


/**
  * Retrieves the format character of <tt>self</tt>.
  * 
  * @return
  * The format character of <tt>self</tt>.
  * 
  * <br><B>Description</B><br>
  * This function retrieves the format character of the current data
  * element.
  * 
  * 
  */
    inline  FormatType
    getFormat( ) const DNB_THROW_SPEC_NULL;


/**
  * Sets the minimum field width of <tt>self</tt>.
  * @param width
  * The desired minimum field width.
  * 
  * @return
  * Nothing
  * 
  * <br><B>Description</B><br>
  * This function sets the minimum field width of the current data
  * element.
  * 
  * 
  */
    inline  void
    setWidth( SizeType width ) DNB_THROW_SPEC_NULL;


/**
  * Sets the fill character of <tt>self</tt>.
  * @param fill
  * The desired fill character.
  * 
  * @return
  * Nothing
  * 
  * <br><B>Description</B><br>
  * This function sets the fill character of the current data element.
  * 
  * 
  */
    inline  void
    setFill( CharType fill ) DNB_THROW_SPEC_NULL;


/**
  * Sets the alignment mode of <tt>self</tt>.
  * @param alignment
  * The desired alignment mode.
  * 
  * @return
  * Nothing
  * 
  * <br><B>Description</B><br>
  * This function sets the alignment mode of the current data element.
  * 
  * 
  */
    inline  void
    setAlignment( AlignmentType alignment ) DNB_THROW_SPEC_NULL;


/**
  * Sets the floating-point precision of <tt>self</tt>.
  * @param precision
  * The desired floating-point precision.
  * 
  * @return
  * Nothing
  * 
  * <br><B>Description</B><br>
  * This function sets the floating-point precision of the current data
  * element.
  * 
  * 
  */
    inline  void
    setPrecision( SizeType precision ) DNB_THROW_SPEC_NULL;


/**
  * Sets the format symbol of <tt>self</tt>.
  * @param symbol
  * The desired format symbol.
  * 
  * @return
  * Nothing
  * 
  * <br><B>Description</B><br>
  * This function sets the format symbol of the current data element.
  * 
  * 
  */
    inline  void
    setSymbol( SymbolType symbol ) DNB_THROW_SPEC_NULL;


/**
  * Sets the format character of <tt>self</tt>.
  * @param format
  * The desired format character.
  * 
  * @return
  * Nothing
  * 
  * <br><B>Description</B><br>
  * This function sets the format character of the current data element.
  * 
  * 
  */
    inline  void
    setFormat( FormatType format ) DNB_THROW_SPEC_NULL;


/**
  * Creates a copy of <tt>self</tt> on the free store.
  * 
  * @return
  * A pointer to the newly created object.
  * 
  * <br><B>Description</B><br>
  * This function implements a virtual copy constructor.  It is used by
  * the message library to create copies of localized data elements.
  * The method must be defined in every class that is derived from
  * @href DNBMsgData.  If <tt>DNBFooMsg</tt> is one such class, its <tt>clone</tt>
  * method is implemented as follows:
  * <pre>
  * DNBMsgData *
  * DNBFooMsg::clone( ) const DNB_THROW_SPEC((scl_bad_alloc))
  * {
  * return DNB_NEW DNBFooMsg( *this );
  * }
  * </pre>
  * 
  * @exception scl_bad_alloc
  * The object could not be created because of insufficient free
  * memory.
  * 
  * 
  */
    virtual DNBMsgData *
    clone( ) const DNB_THROW_SPEC((scl_bad_alloc)) = 0;


/**
  * Creates the localized representation of <tt>self</tt>.
  * @param locale
  * The locale object which is used to format the data element.
  * 
  * @return
  * The localized representation of <tt>self</tt>.
  * 
  * <br><B>Description</B><br>
  * This function returns <tt>self</tt> as a string, formatted by the <tt>locale</tt>
  * argument.  It is used by the message library to create the printable
  * representation of a localized data element.  The method must be
  * defined in every class that is derived from @href DNBMsgData.
  * @exception scl_bad_alloc
  * An internal buffer could not be allocated because of
  * insufficient free memory.
  * @exception scl_runtime_error
  * The localized data element could not be expressed as a string.
  * 
  * 
  */
    virtual StringType
    scribe( const DNBLocale &locale ) const
        DNB_THROW_SPEC((scl_bad_alloc, scl_runtime_error)) = 0;

protected:
/**
  * Copies a localized data element to <tt>self</tt>.
  * @param right
  * The localized data element to be copied.
  * 
  * @return
  * Nothing
  * 
  * <br><B>Description</B><br>
  * This function copies the formatting attributes of <tt>right</tt> to <tt>self</tt>.
  * 
  * 
  */
    void
    operator=( const DNBMsgData &right ) DNB_THROW_SPEC_NULL;


/**
  * Justifies a string using the formatting attributes of <tt>self</tt>.
  * @param message
  * The string to be justified.
  * 
  * @return
  * Nothing
  * 
  * <br><B>Description</B><br>
  * This function justifies the parameter <tt>message</tt> using the formatting
  * attributes of <tt>self</tt>.  If <tt>message</tt> has fewer characters than the
  * current field width, it will be padded with the current fill
  * character to the field width given.  The current alignment mode
  * specifies whether the data element is left-justified, centered, or
  * right-justified within the field.
  * 
  * 
  */
    void
    justifyMsg( StringType &message ) const DNB_THROW_SPEC_NULL;

    SizeType        width_;
    CharType        fill_;
    AlignmentType   alignment_;
    SizeType        precision_;
    SymbolType      symbol_;
    FormatType      format_;
};




//
//  The following template class represents a simplified "function object".
//  When an instance of this class is inserted into a message object, it sets
//  the formatting attributes of the next data element.  This class is used
//  exclusively by the manipulators defined below.
//
template <class T>
class _DNBMsgManip
{
public:
    typedef void (*func_t)( DNBMsgData &data, T value );

    _DNBMsgManip( func_t func, T value ) DNB_THROW_SPEC_NULL;

    ~_DNBMsgManip( ) DNB_THROW_SPEC_NULL;

    void
    invoke( DNBMsgData &data ) const DNB_THROW_SPEC_NULL;

private:
    func_t      func_;
    T           value_;
};




/**
  * Sets the minimum field width of the next data element inserted into a
  * localized message.
  * @param width
  * The desired minimum field width.
  * 
  * @return
  * A function object which can be inserted into a message.
  * 
  * <br><B>Description</B><br>
  * This function sets the minimum field width of the next data element
  * inserted into a message.  If the converted value has fewer characters
  * than the field width, it will be padded with the fill character to the
  * field width given.  The alignment mode specifies whether the data
  * element is left-justified, centered, or right-justified within the
  * field.  The default field width is zero.
  * 
  * <br><B>Example</B><br>
  * <pre>
  * int         item = 123456;
  * DNBMessage  msg( DNB_FORMAT("#%1#") );
  * msg  << DNBSetWidth( 10 ) << item;
  * cout << msg << endl;
  * 
  * OUTPUT:
  * #   123,456#
  * 
  * </pre>
  *
  */
ExportedByDNBSystem  _DNBMsgManip<DNBMsgBase::SizeType>
DNBSetWidth( DNBMsgBase::SizeType width ) DNB_THROW_SPEC_NULL;




/**
  * Sets the fill character of the next data element inserted into a
  * localized message.
  * @param fill
  * The desired fill character.  This parameter must be specified with
  * the macro @href DNB_TEXT.
  * 
  * @return
  * A function object which can be inserted into a message.
  * 
  * <br><B>Description</B><br>
  * This function sets the fill character of the next data element inserted
  * into a message.  If the converted value has fewer characters than the
  * field width, it will be padded with the fill character to the field
  * width given.  The alignment mode specifies whether the data element is
  * left-justified, centered, or right-justified within the field.  The
  * default fill character is a single space.
  * 
  * <br><B>Example</B><br>
  * <pre>
  * int         item = 123456;
  * DNBMessage  msg( DNB_FORMAT("#%1#") );
  * msg  << DNBSetWidth( 10 ) << DNBSetFill( DNB_TEXT('^') ) << item;
  * cout << msg << endl;
  * 
  * OUTPUT:
  * #^^^123,456#
  * 
  * </pre>
  *
  */
ExportedByDNBSystem  _DNBMsgManip<DNBMsgBase::CharType>
DNBSetFill( DNBMsgBase::CharType fill ) DNB_THROW_SPEC_NULL;




/**
  * Sets the alignment mode of the next data element inserted into a
  * localized message.
  * @param alignment
  * The desired alignment mode.
  * 
  * @return
  * A function object which can be inserted into a message.
  * 
  * <br><B>Description</B><br>
  * This function sets the alignment mode of the next data element inserted
  * into a message.  If the converted value has fewer characters than the
  * field width, it will be padded with the fill character to the field
  * width given.  The alignment mode specifies whether the data element is
  * left-justified, centered, or right-justified within the field.  The
  * default alignment mode is @href DNBMsgBase::Right.
  * 
  * <br><B>Example</B><br>
  * <pre>
  * int         item = 123456;
  * DNBMessage  msg( DNB_FORMAT("#%1#\n#%2#\n#%3#") );
  * msg  << DNBSetWidth( 12 ) << DNBSetAlignment( DNBMsgBase::Left )
  * << item
  * << DNBSetWidth( 12 ) << DNBSetAlignment( DNBMsgBase::Center )
  * << item
  * << DNBSetWidth( 12 ) << DNBSetAlignment( DNBMsgBase::Right )
  * << item;
  * cout << msg << endl;
  * 
  * OUTPUT:
  * #123,456     #
  * #  123,456   #
  * #     123,456#
  * 
  * </pre>
  *
  */
ExportedByDNBSystem  _DNBMsgManip<DNBMsgBase::AlignmentType>
DNBSetAlignment( DNBMsgBase::AlignmentType alignment ) DNB_THROW_SPEC_NULL;




/**
  * Sets the floating-point precision of the next data element inserted into
  * a localized message.
  * @param precision
  * The desired floating-point precision.
  * 
  * @return
  * A function object which can be inserted into a message.
  * 
  * <br><B>Description</B><br>
  * This function sets the floating-point precision of the next data element
  * inserted into a message.  The precision attribute specifies the number
  * of digits to place after the decimal separator.  The default precision
  * is equivalent to <tt>FLT_DIG</tt>, which is defined in <tt><tt>float.h</tt></tt>.
  * 
  * <br><B>Example</B><br>
  * <pre>
  * float       item = 1234.56789;
  * DNBMessage  msg( DNB_FORMAT("#%1#") );
  * msg << DNBSetWidth( 12 ) << DNBSetPrecision( 3 ) << item;
  * cout << msg << endl;
  * 
  * OUTPUT:
  * #   1,234.568#
  * 
  * </pre>
  *
  */
ExportedByDNBSystem  _DNBMsgManip<DNBMsgBase::SizeType>
DNBSetPrecision( DNBMsgBase::SizeType precision ) DNB_THROW_SPEC_NULL;




/**
  * Sets the format symbol of the next data element inserted into a
  * localized message.
  * @param symbol
  * The desired format symbol.
  * 
  * @return
  * A function object which can be inserted into a message.
  * 
  * <br><B>Description</B><br>
  * This function sets the format symbol of the next data element inserted
  * into a message.  The format symbol specifies the characters that are
  * used to format a numeric value (such as currency).  The default symbol
  * is equivalent to @href DNBMsgBase::LocalSymbol.
  * 
  * <br><B>Example</B><br>
  * <pre>
  * double      item = 123456.0;
  * DNBMessage  msg( DNB_FORMAT("#%1#\n#%2#\n#%3#") );
  * msg  << DNBSetWidth( 12 ) << DNBSetSymbol( DNBMsgBase::NoSymbol )
  * << DNBCurrency( item )
  * << DNBSetWidth( 12 ) << DNBSetSymbol( DNBMsgBase::LocalSymbol )
  * << DNBCurrency( item )
  * << DNBSetWidth( 12 ) << DNBSetSymbol( DNBMsgBase::IntlSymbol )
  * << DNBCurrency( item );
  * cout << msg << endl;
  * 
  * OUTPUT:
  * #    1,234.56#
  * #   $1,234.56#
  * # USD1,234.56#
  * 
  * </pre>
  *
  */
ExportedByDNBSystem  _DNBMsgManip<DNBMsgBase::SymbolType>
DNBSetSymbol( DNBMsgBase::SymbolType symbol ) DNB_THROW_SPEC_NULL;




/**
  * Sets the format character of the next data element inserted into a
  * localized message.
  * @param format
  * The desired format character.  This parameter must specify one of
  * the ASCII characters listed below.
  * 
  * @return
  * A function object which can be inserted into a message.
  * 
  * <br><B>Description</B><br>
  * This function sets the format character of the next data element
  * inserted into a message.  Such characters are used to format date and
  * time quantities.  The meanings assigned to the format characters are
  * identical to those used in the Standard C Library function <tt>strftime()</tt>.
  * These assignments are listed below.
  * 
  * \begin{center}
  * \begin{tabular}{lll}
  * Code  & Meaning                           & Example \\
  * a     & Abbreviated weekday name          & Wed \\
  * A     & Full weekday name                 & Wednesday \\
  * b     & Abbreviated month name            & Feb \\
  * B     & Full month name                   & February \\
  * c     & Date and time                     & 02/29/84 02:34:56 \\
  * C     & Date and time (long form)         & Feb 29 14:34:56 1984 \\
  * d     & Day of the month (01-31)          & 29 \\
  * H     & Hour of the 24-hour day (00-23)   & 14 \\
  * I     & Hour of the 12-hour day (01-12)   & 02 \\
  * j     & Day of the year (001-366)         & 060 \\
  * m     & Month of the year (01-12)         & 02 \\
  * M     & Minutes after the hour (00-59)    & 34 \\
  * p     & AM/PM indicator, if any           & PM \\
  * S     & Seconds after the minute (00-61)  & 56 \\
  * U     & Sunday week of the year (00-53)   & 09 \\
  * w     & Day of the week (0-6, Sunday=0)   & 3 \\
  * W     & Monday week of the year (00-53)   & 09 \\
  * x     & Date                              & 02/29/84 \\
  * X     & Time                              & 02:34:56 \\
  * y     & Year of the century (00-99)       & 84 \\
  * Y     & Year with the century             & 1984 \\
  * Z     & Time zone name                    & EST
  * \end{tabular}
  * \end{center}
  * 
  * The third column of this table is the output corresponding to the
  * time and date:
  * 
  * \begin{center}
  * Wednesday, February 29, 1984 02:34:56 EST
  * \end{center}
  * 
  * The default format character is @href DNBMsgBase::DateTime, which is
  * equivalent to the character 'c'.
  * 
  * <br><B>Example</B><br>
  * <pre>
  * RWTime      item;
  * DNBMessage  msg( DNB_FORMAT("#%1#\n#%2#\n#%3#\n#%4#") );
  * msg  << DNBSetWidth( 20 ) << DNBSetFormat( 'B' ) << item
  * << DNBSetWidth( 20 ) << DNBSetFormat( 'x' ) << item
  * << DNBSetWidth( 20 ) << DNBSetFormat( 'X' ) << item
  * << DNBSetWidth( 20 ) << DNBSetFormat( 'c' ) << item;
  * cout << msg << endl;
  * 
  * OUTPUT:
  * #             October#
  * #            10/03/97#
  * #            10:50:24#
  * #   10/03/97 10:50:24#
  * 
  * </pre>
  *
  */
ExportedByDNBSystem  _DNBMsgManip<DNBMsgBase::FormatType>
DNBSetFormat( DNBMsgBase::FormatType format ) DNB_THROW_SPEC_NULL;




//
//  Include the public definition file.
//
#include "DNBMsgData.cc"


#endif  /* _DNB_MSGDATA_H_ */
