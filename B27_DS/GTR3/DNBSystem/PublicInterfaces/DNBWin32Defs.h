//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef DNBWin32Defs_H
#define DNBWin32Defs_H


#include <IUnknown.h>                   // Defined in System framework.


#ifdef  _WINDOWS_SOURCE
#pragma comment(lib, "uuid")
#endif


#endif  /* DNBWin32Defs_H */
