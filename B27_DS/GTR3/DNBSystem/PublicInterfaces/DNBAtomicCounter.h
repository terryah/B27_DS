//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 */


//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*    
//*     bkh         10/02/03    Added documentation
//*
//* 

#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNBAtomicCounter_H_
#define _DNBAtomicCounter_H_


#include <DNBSystemBase.h>

#if     !DNB_HAS_ATOMIC_INTEGER
#include <DNBSysMutexLock.h>
#include <DNBSysLockGuard.h>
#endif

#include <DNBSystemDefs.h>


#include <DNBSystem.h>
#include <DNBException.h>




/**
 *  This class represents a thread-safe integral counter that is suitable for
 *  use in reference counting applications.
 */
class ExportedByDNBSystem DNBAtomicCounter
{
public:

#if     defined(_WINDOWS_SOURCE)
typedef long    AtomicType;             // Formal type is LONG.
#else
typedef int     AtomicType;             // Valid for all Unix platforms.
#endif

    DNBAtomicCounter( AtomicType initCount = 0 )
        DNB_THROW_SPEC((DNBEResourceLimit));
/**
 *  Copy constructor
 */
    DNBAtomicCounter( const DNBAtomicCounter &right )
        DNB_THROW_SPEC((DNBEResourceLimit));

    ~DNBAtomicCounter( )
        DNB_THROW_SPEC_NULL;

    //
    //  Atomic load, store, read-modify-write operations.
    //
    /**
     *  Function to set the atomic value
     */
    void
    set( AtomicType value )
        DNB_THROW_SPEC_NULL;

    /**
     *  Function to get the atomic value
     */

    AtomicType
    get( ) const
        DNB_THROW_SPEC_NULL;

    /**
     *  Atomic increment.
     */
    AtomicType
    increment( )
        DNB_THROW_SPEC_NULL;

    /**
     *  Atomic decrement.
     */
    AtomicType
    decrement( )
        DNB_THROW_SPEC_NULL;

    //
    //  Standard integral operations.
    //
    /**
     *  Assign atomic value
     */
    void
    operator=( AtomicType value )
        DNB_THROW_SPEC_NULL
    {
        set( value );
    }

    /**
     *  Overload assignment operator.
     */
    void
    operator=( const DNBAtomicCounter &right )
        DNB_THROW_SPEC_NULL
    {
        set( right.get() );
    }

    /**
     *  Increment operator for the atomic value
     */
    AtomicType
    operator++( )
        DNB_THROW_SPEC_NULL
    {
        return increment( );
    }

    /**
     *  Decrement atomic value.
     */
    AtomicType
    operator--( )
        DNB_THROW_SPEC_NULL
    {
        return decrement( );
    }

    /**
     *  Conversion function.
     */
    operator AtomicType( ) const
        DNB_THROW_SPEC_NULL
    {
        return get( );
    }

protected:

#if     DNB_HAS_ATOMIC_INTEGER
    volatile AtomicType     count_;     // Current value of counter.
#else
    typedef DNBSysMutexLock               MonitorType;   // should be "MutexType"
    typedef DNBSysLockGuard               MonitorGuard;  // should be "LockGuard"


    /**
     *  Mutex lock
     */
    MonitorType &
    getMonitor( ) const
        DNB_THROW_SPEC_NULL
    {
        return DNB_MODIFY_MUTABLE(DNBAtomicCounter, monitor_);
    }

    mutable  MonitorType    monitor_;   // Mutex for atomic method calls
    volatile AtomicType     count_;     // Current value of counter.
#endif

    friend class DNBAtomicCounterTest;  // For testing purposes only.
};




#endif  /* _DNBAtomicCounter_H_ */
