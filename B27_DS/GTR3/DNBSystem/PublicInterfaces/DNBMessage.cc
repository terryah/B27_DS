//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview RTL trangara 01:09:19
 */
// --------------------------------------------------------
//*
//* FILE:
//*     Message.cc    - public definition file
//*
//* MODULE:
//*     DNBMessage    - single non-template class
//*
//* OVERVIEW:
//*     This module defines a framework for managing localized messages.
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     jad         08/18/97    Initial implementation.
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1997 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*
template<class T>
DNBMessage &
operator<<( DNBMessage &msg, const _DNBMsgManip<T> &manip ) DNB_THROW_SPEC_NULL
{
    manip.invoke( msg );
    return msg;
}


inline
DNBCurrency::DNBCurrency( double value ) DNB_THROW_SPEC_NULL :
    value_  (value)
{
    // Nothing
}


inline  void
DNBCurrency::setValue( double value ) DNB_THROW_SPEC_NULL
{
    value_ = value;
}


inline  double
DNBCurrency::getValue( ) const DNB_THROW_SPEC_NULL
{
    return value_;
}
