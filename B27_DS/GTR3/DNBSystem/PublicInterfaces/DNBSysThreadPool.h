//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
//
// DNBSysThreadPool.h
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     smw        08/11/2004   Initial Implementation
//
//==============================================================================


#ifndef DNBSysThreadPool_H
#define DNBSysThreadPool_H


#include <DNBSystem.h>
#include <DNBSystemBase.h>
#include <scl_vector.h>
#include <DNBSystemDefs.h>
#include <DNBSysThreadDefs.h>
#include <DNBSysIRunnable.h>
#include <DNBSysThread.h>
#include <DNBSysThreadId.h>
#include <DNBSysMonitor.h>
#include <DNBSysLockGuard.h>
#include <DNBSysFastMutexLock.h>
#include <DNBSysPCValQueue.h>


class ExportedByDNBSystem DNBSysThreadPool : public DNBSysMonitor
{
public:


	typedef DNBSysPCValQueue < DNBSysIRunnable >	TaskQueueType;  
	typedef scl_vector< DNBSysThread >              ThreadCollectionType; 


    // Constructor
    DNBSysThreadPool( int numThreads )
		DNB_THROW_SPEC_NULL;
 



    // Destructor 
    ~DNBSysThreadPool( ) 
		DNB_THROW_SPEC_NULL;
  
    

    bool 
    enqueue( DNBSysIRunnable aRunnable ) 
		DNB_THROW_SPEC_NULL;




    void 
    start() 
		DNB_THROW_SPEC_NULL;



/**
  * This method will cause threads in the pool to finish their task if already started 
  * then simply terminate.  Threads blocked due to an empty or a full task queue 
  * simply wake-up and terminate.
  * Any enqueued tasks remain in the queue.  Subsequent attempts to enqueue tasks will fail.
  * The thread pool can be restarted;  This is however expensive since the
  * corresponding operating system threads get deleted upon closing of the queue then recreated.
  */
    void
    stop() 
		DNB_THROW_SPEC_NULL;



    void 
    enlarge( size_t newSize ) 
		DNB_THROW_SPEC_NULL;
    
    

	size_t
	size() const 
		DNB_THROW_SPEC_NULL ;



	size_t
	numberTasksPending () const
		DNB_THROW_SPEC_NULL;


private:

    size_t                      size_;				// number of threads in pool
    ThreadCollectionType        threadCollection_;
    TaskQueueType               taskQueue_;

	/*
  	 * every thread in the pool is running this method
 	*/
	void internalRun () DNB_THROW_SPEC_NULL ;


	// Prohibit copy construction and assignment operator
	    // Assignment operator
    DNBSysThreadPool&
    operator=( const DNBSysThreadPool& second )
		DNB_THROW_SPEC_NULL;

	DNBSysThreadPool ( DNBSysThreadPool & another )
		DNB_THROW_SPEC_NULL;

    
};



#endif /* DNBSysThreadPool */
