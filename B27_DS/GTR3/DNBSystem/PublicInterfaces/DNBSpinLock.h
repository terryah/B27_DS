//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 */

//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*    
//*     bkh         10/03/03    Added documentation
//*

#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNBSpinLock_H_
#define _DNBSpinLock_H_


#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>


#include <DNBSystem.h>
#include <DNBAtomicLock.h>

#include <DNBSysIMutexLock.h>
#include <DNBSysLockGuard.h>
#include <DNBSysUnlockGuard.h>



/**
  * Class for exclusive synchronization locks.
  * <br><B>Description</B><br> 
  * This class represents a non-recursive, exclusive synchronization lock that
  *  is designed to protect short critical sections of code.  The lock uses an
  *  adaptive spin-yield algorithm that is extremely efficient in the contention-
  *  free case.  The class provides an API similiar to DNBSysMutexLock.
  */
class ExportedByDNBSystem DNBSpinLock : public DNBSysIMutexLock
{
public:
    DNBSpinLock( )
        DNB_THROW_SPEC((DNBEResourceLimit));

    ~DNBSpinLock( )
        DNB_THROW_SPEC_NULL;

    /**
     *  Function to acquire the lock
     */
    void
    acquire( )
        DNB_THROW_SPEC_NULL;

    /**
     *  Function to acquire the read lock
     */
    inline  void
    acquireRead( )
        DNB_THROW_SPEC_NULL;

    /**
     *  Function to acquire the write lock
     */
    inline  void
    acquireWrite( )
        DNB_THROW_SPEC_NULL;

    /**
     *  Function to release the lock
     */
    inline  void
    release( )
        DNB_THROW_SPEC_NULL;

    //typedef RWTLockGuard<DNBSpinLock>   LockGuard;
    typedef DNBSysLockGuard   LockGuard;
    //typedef RWTUnlockGuard<DNBSpinLock> UnlockGuard;
    typedef DNBSysUnlockGuard   UnlockGuard;


protected:
    enum
    {
        YieldLimit = 100000             // Dead-lock limit.
    };

    enum
    {
        SpinLimitUP = 1,                // Uniprocessor spin limit.
        SpinLimitMP = 4000              // Multiprocessor spin limit.
    };

    /**
     *  Function to obtain the number of processors involved.
     */
    static  size_t
    GetNumProcessors( )
        DNB_THROW_SPEC_NULL;

    /**
     *  Function to obtain the spin limit
     */
    static  size_t
    GetSpinLimit( )
        DNB_THROW_SPEC_NULL;

    static  size_t  SpinLimit;

private:
    //
    //  Prohibit copy construction and assigment of DNBSpinLock objects.
    //
    DNBSpinLock( const DNBSpinLock &right );

    DNBSpinLock &
    operator=( const DNBSpinLock &right );

    DNBAtomicLock   lock_;

    friend class DNBSpinLockTest;       // For testing purposes only.
};




//
//  Include the public definition file.
//
#include "DNBSpinLock.cc"




#endif  /* _DNBSpinLock_H_ */
