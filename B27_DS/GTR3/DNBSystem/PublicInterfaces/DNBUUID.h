//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 */

//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*    
//*     bkh         10/03/03    Added documentation
//*

#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_UUID_H_
#define _DNB_UUID_H_


#include <DNBSystemBase.h>
#include <scl_iosfwd.h>
#include <DNBSystemDefs.h>


#include <DNBSystem.h>


#include <DNBWin32Defs.h>
#include <DNBException.h>


/**
 *  This class represents a Universally Unique IDentifier (UUID)
 */
class ExportedByDNBSystem DNBUUID
{
public:
    DNBUUID( )
        DNB_THROW_SPEC((DNBENetworkError));

    /**
     *  To check the right UUID version.
     *  @param right
     *      constant GUID
     */
    DNBUUID( const GUID &right )
        DNB_THROW_SPEC((DNBEInvalidFormat));

    /**
     *  Function to check the string for invalid UUID version.
     *  @param str
     *      constant character
     */
    DNBUUID( const char *str )
        DNB_THROW_SPEC((DNBEInvalidFormat));

   /**
    *   Constructor.
    */
    DNBUUID( const DNBUUID &right )
        DNB_THROW_SPEC_NULL;

   /**
    *   Destructor.
    */
    ~DNBUUID( )
        DNB_THROW_SPEC_NULL;

    /**
     *  To assign GUID's.
     */
    void
    operator=( const GUID &right )
        DNB_THROW_SPEC((DNBEInvalidFormat));

    /**
     *  Operator to assign UUID's.
     */
    void
    operator=( const DNBUUID &right )
        DNB_THROW_SPEC_NULL;

    /**
     *  Function to generate UUID.
     */
    void
    generate( )
        DNB_THROW_SPEC((DNBENetworkError));

    /**
     *  Function to convert to type GUID.
     */
    operator GUID( ) const
        DNB_THROW_SPEC_NULL;

    /**
     *  Operator to find if UUID is not empty/zero.
     */
    operator bool( ) const
        DNB_THROW_SPEC_NULL;

    /**
     *  Function to find if UUID is all zeros.
     */
    bool
    isNil( ) const
        DNB_THROW_SPEC_NULL;

    /**
     *  Function to compare two UUID's.
     */
    int
    compareTo( const DNBUUID &right ) const
        DNB_THROW_SPEC_NULL;

    /**
     *  Function to convert UUID to string type.
     */
    scl_string
    toString( ) const
        DNB_THROW_SPEC_NULL;

    static const DNBUUID    Nil;

private:
    enum { StringLength = 36 };

    enum { StringFields = 11 };

    enum NilType { NilIdent };

    DNBUUID( NilType )
        DNB_THROW_SPEC_NULL;

    bool
    isValid( ) const
        DNB_THROW_SPEC_NULL;

    void
    extract( DNBUnsigned64 &high, DNBUnsigned64 &low ) const
        DNB_THROW_SPEC_NULL;

    DNBUnsigned32   data1_;
    DNBUnsigned16   data2_;
    DNBUnsigned16   data3_;
    DNBUnsigned8    data4_[8];
};



/**
 *  Comparision operator.
 *  @param  left
 *      constant BUUID
 *  @param right
 *      constant BUUID
 */
ExportedByDNBSystem  bool
operator==( const DNBUUID& left, const DNBUUID& right )
    DNB_THROW_SPEC_NULL;

/**
 *  Comparing to find if left is not equal to right.
 *  @param  left
 *      constant BUUID
 *  @param right
 *      constant BUUID
 */
ExportedByDNBSystem  bool
operator!=( const DNBUUID& left, const DNBUUID& right )
    DNB_THROW_SPEC_NULL;

/**
 *  Comparision operator.
 *  @param  left
 *      constant BUUID
 *  @param right
 *      constant BUUID
 */
ExportedByDNBSystem  bool
operator< ( const DNBUUID& left, const DNBUUID& right )
    DNB_THROW_SPEC_NULL;

/**
 *  Comparision operator.
 *  @param  left
 *      constant BUUID
 *  @param right
 *      constant BUUID
 */
ExportedByDNBSystem  bool
operator<=( const DNBUUID& left, const DNBUUID& right )
    DNB_THROW_SPEC_NULL;

/**
 *  Comparision operator.
 *  @param  left
 *      constant BUUID
 *  @param right
 *      constant BUUID
 */
ExportedByDNBSystem  bool
operator> ( const DNBUUID& left, const DNBUUID& right )
    DNB_THROW_SPEC_NULL;

/**
 *  Comparision operator.
 *  @param  left
 *      constant BUUID
 *  @param right
 *      constant BUUID
 */
ExportedByDNBSystem  bool
operator>=( const DNBUUID& left, const DNBUUID& right )
    DNB_THROW_SPEC_NULL;

/**
 *  Insertion operator.
 *  @param ostr
 *      Of type ostream
 *  @param item
 *      constant BUUID       
 */
ExportedByDNBSystem  ostream &
operator<<( ostream &ostr, const DNBUUID &item )
    DNB_THROW_SPEC_NULL;




typedef DNBUUID     DNBClassID;




#endif  /* _DNB_UUID_H_ */
