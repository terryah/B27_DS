//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
//
// DNBSysMonitor.h
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     smw        04/27/2004   Initial Implementation
/**
 * @fullreview //smw 03:12:23
 * @quickreview //jod 04:02:02
 */
//==============================================================================

#if DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef DNB_SYS_MONITOR_H
#define DNB_SYS_MONITOR_H

#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>
#include <DNBSystem.h>
#include <DNBSysMutexLock.h>
#include <DNBSysLockGuard.h>
#include <DNBSysUnlockGuard.h>

class ExportedByDNBSystem DNBSysMonitor : public DNBSysIMutexLock    // so that we get the lock guard functionality
{

public:


  typedef DNBSysMutexLock MutexType;  // FIXME: If there is a need to monitor with another type of mutex,
                                      // can either template the mutex type, or have additional
                                      // argument to constructor of type DNBSysIMutexLock.

  typedef DNBSysLockGuard LockGuard;
  typedef DNBSysUnlockGuard UnlockGuard;


protected:

    MutexType          mutex_;

    /**
     * Construct a default instance (initializes the mutex)
     */
    DNBSysMonitor(void);


    /**
     * Define copy-constructor so derived classes can copy-construct, 
     * but initialize the mutex; don't copy it...
     * (mutexes cannot be copied)
     */
//    DNBSysMonitor(const DNBSysMonitor second);  What happens if we don't have one?

    /**
     * Define an assignment operator so derived classes can do assignments, 
     * but don't assign the mutex, leave it alone...
     * (mutexes cannot be assigned)
     */
//    DNBSysMonitor& operator=(const DNBSysMonitor &); What happens if we don't have one?

    /**
     * Lock the monitor; acquires the monitor's mutex.
     */
    void acquire(void) { mutex_.acquire(); };


    /**
     * Determine whether calling thread currently owns the monitor (mutex)
     * Primarily intended for use in precondition assertions.
     */
    //bool isAcquired(void) const { return mutex_.isAcquired(); };  // if needed, then need to add 
                                                                    // isAcquired to DNBSysMutexLock


    /**
     * Cast away const on this and return a reference. Used to lock a 
     * monitor via a guard. This is necessary to avoid having to 
     * explicitly cast away const on the this pointer in every const 
     * member function that needs to lock the monitor.
     */
    DNBSysMonitor& monitor(void) const { return const_cast<DNBSysMonitor&> (*this); };

	DNBSysMonitor& monitor_temp(void) const { return monitor(); };  // for smoother migration; 
 
    /**
     * Accessor function for the monitor's mutex.
     */
    MutexType& mutex(void) { return mutex_;} ;

	MutexType& mutex_temp(void) { return mutex_;} ;		// for smoother migration; 


    /**
     * Unlock the monitor; releases the monitor's mutex.
     */
    void release(void) { mutex_.release(); };

    /**
     * Conditionally lock the monitor, but only if it can be locked 
     * without blocking.
     */
    // bool tryAcquire(void) { return mutex_.tryAcquire(); }; // not sure if needed; if so, derive from
                                                              // DNBSysITimedMutexLock instead and change
                                                              // the mutex type
};

#endif // DNB_SYS_MONITOR_H
