//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//*
//* FILE:
//*     Callback.h     - public header file
//*
//* MODULE:
//*     DNBFunctorX        - template class for non-return functors
//*
//*     DNBFunctorXwRet    - template class for function returning functors
//*
//* OVERVIEW:
//*     This module defines several functors. A functor is a callback mechanism.
//*     It allows the containment of a method and the passing around of this method
//*     for the purpose of propagating event handling. The functors here can represent
//*     either standard C functions or member functions of classes.
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     DJB         10/01/99    Initial implementation.
//      rtl/mmg     06/23/03    Provide virtual destructor in 
//                              _DNBBaseFunctorBody
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1998, 99  Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*
//*
//* EXAMPLE:
//*
//*    DNBFunctor0      //A function with no arguments passed & no return values
//*    DNBFunctor1      //A function with 1 arguments passed & no return values
//*    DNBFunctor2      //A function with 2 arguments passed & no return values
//*    DNBFunctor3      //A function with 3 arguments passed & no return values
//*    DNBFunctor4      //A function with 4 arguments passed & no return values
//*    DNBFunctor0wRet  //A function with 0 arguments passed & no return values
//*    DNBFunctor1wRet  //A function with 1 arguments passed & 1 return value
//*    DNBFunctor2wRet  //A function with 2 arguments passed & 2 return value
//*    DNBFunctor3wRet  //A function with 3 arguments passed & 3 return value
//*    DNBFunctor4wRet  //A function with 4 arguments passed & 4 return value
//*
//*    int foo( int x, long y, Handle z );
//*
//*    class newclass
//*    {
//*      public:
//*          int bar( int a, long b, Handle c );
//*    };
//*
//*    try
//*    {
//*        newclass obj;
//*        DNBFunctor3wRet functor1 = DNBMakeFunctor( (DNBFunctor3wRet*) 0,
//*                                                  &foo );
//*        DNBFunctor3wRet functor2 = DNBMakeFunctor( (DNBFunctor3wRet*) 0,
//*                                                    &obj, &newclass::bar );
//*    }
//*    catch ( scl_bad_alloc )
//*    {
//*    }
//*
//*


#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_CALLBACK_H_
#define _DNB_CALLBACK_H_


#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>


#include <DNBSystem.h>


#include <DNBException.h>

//----------------------------------------------------------------------
//  The software for the copyright below has been significantly modified 
//  and is not a complete representation of the code for the copyright 
//  below.
//
// Copyright (c) 1996 Paul Jakubik 
// 
// Permission to use, copy, modify, distribute and sell this software
// and its documentation for any purpose is hereby granted without fee,
// provided that the above copyright notice appear in all copies and
// that both that copyright notice and this permission notice appear
// in supporting documentation.  Paul Jakubik makes no representations 
// about the suitability of this software for any purpose.  It is 
// provided "as is" without express or implied warranty.
//----------------------------------------------------------------------


class ExportedByDNBSystem _DNBBaseFunctorBody
{
public:
    virtual 
    void* 
    getCallee() const
        DNB_THROW_SPEC_ANY    = 0;    

    virtual 
    void* 
    getFunc() const
        DNB_THROW_SPEC_ANY    = 0;    

    virtual 
    bool 
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY    = 0;    

    virtual 
    bool 
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY    = 0;
    
    virtual
    ~_DNBBaseFunctorBody()
    DNB_THROW_SPEC_ANY
    {}
};

class ExportedByDNBSystem DNBBaseFunctor
{
public:
    DNBBaseFunctor()
        DNB_THROW_SPEC_ANY;

    virtual ~DNBBaseFunctor()
        DNB_THROW_SPEC_ANY;

    bool 
    operator==(const DNBBaseFunctor& rhs ) const
        DNB_THROW_SPEC_ANY;

    bool 
    operator<(const DNBBaseFunctor& rhs ) const
        DNB_THROW_SPEC_ANY;
    
    operator bool() const
        DNB_THROW_SPEC_ANY;

    void* 
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctorBody*
    getBody() const
        DNB_THROW_SPEC_ANY;

    bool
    isValid()
        DNB_THROW_SPEC_ANY;
};

//
// Includes the declarations of all the functors 
//
#include <DNBCallbackDec.h>

//
// Include definition file
//
#include <DNBCallback.cc>


#endif /* CALLBACK_H */
