//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
#ifndef DNBSysITimedMutexLock_H
#define DNBSysITimedMutexLock_H


#include <DNBSystem.h>
#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>
#include <DNBSysThreadDefs.h>


#include <DNBSysIMutexLock.h>

class ExportedByDNBSystem DNBSysITimedMutexLock //: public DNBSysIMutexLock
{
public:

    virtual bool
    tryAcquire( /*DNBSysTimeout timeout */) = 0;

};

#endif //DNBSysITimedMutexLock_H

