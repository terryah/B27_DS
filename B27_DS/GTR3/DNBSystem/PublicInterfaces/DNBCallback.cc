//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview RTL trangara 01:09:19
 */
// --------------------------------------------------------
/////////////////////////////////////////////////////////
//
// 0 Passed,  0 Returned 
//
/////////////////////////////////////////////////////////

template< class Function >
_DNBFunction0Body< Function>::_DNBFunction0Body()
    DNB_THROW_SPEC_ANY
{
}


template< class Function >
_DNBFunction0Body< Function>::_DNBFunction0Body(const Function& f)
    DNB_THROW_SPEC_ANY:
    f_(f) 
{
    DNB_PRECONDITION( f_ );
}

template< class Function >
_DNBFunction0Body< Function>::_DNBFunction0Body(const _DNBFunction0Body< Function>& functionFunctor)
    DNB_THROW_SPEC_ANY:
     f_(functionFunctor.f_)
{
    DNB_PRECONDITION( f_ );
}

template< class Function >
_DNBFunction0Body< Function>::~_DNBFunction0Body()    
DNB_THROW_SPEC_ANY
{
}

template< class Function >
void
_DNBFunction0Body< Function>::operator()(  )
    DNB_THROW_SPEC_ANY
{
    f_();
}

template< class Function >
void*
_DNBFunction0Body< Function>::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return NULL;
}

template< class Function >
void*
_DNBFunction0Body< Function>::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&f_;
}   

template< class Function >
_DNBBaseFunctor0Body*
_DNBFunction0Body< Function>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return DNB_NEW _DNBFunction0Body< Function>(*this);
}

template< class Function >
bool 
_DNBFunction0Body< Function >::IsEqual(_DNBBaseFunctorBody& rhs) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction0Body< Function >* rhsBody = dynamic_cast< _DNBFunction0Body< Function > *  > (&rhs);

    if (rhsBody != NULL)
    {
        return( f_ == rhsBody->f_ );
    }
    return false;
}

template< class Function >
bool 
_DNBFunction0Body< Function >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction0Body< Function >* rhsBody = dynamic_cast< _DNBFunction0Body< Function > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        return ( f_ < rhsBody->f_ );
    }
    else
        return ( (void *)this < (void *)&rhs );
}

template< class Function, class M1 >
_DNBFunction0M1Body< Function, M1 >::_DNBFunction0M1Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class Function, class M1 >
_DNBFunction0M1Body< Function, M1 >::_DNBFunction0M1Body(const Function& f, M1 m1)
    DNB_THROW_SPEC_ANY :
    f_(f),
    m1_(m1)
{
    DNB_PRECONDITION( f_ );
}

template< class Function, class M1 >
_DNBFunction0M1Body< Function, M1 >::_DNBFunction0M1Body(const _DNBFunction0M1Body< Function, M1 >& functionFunctor)
    DNB_THROW_SPEC_ANY :
    f_(functionFunctor.f_),
    m1_(functionFunctor.m1_)
{
    DNB_PRECONDITION( f_ );
}

template< class Function, class M1 >
_DNBFunction0M1Body< Function, M1 >::~_DNBFunction0M1Body()
    DNB_THROW_SPEC_ANY
{
}

template< class Function, class M1>
void 
_DNBFunction0M1Body< Function, M1 >::operator()(  ) 
    DNB_THROW_SPEC_ANY
{ 
    f_( m1_ );
}

template< class Function, class M1 >
void*
_DNBFunction0M1Body< Function, M1 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return NULL;
}    
template< class Function, class M1 >
void*
_DNBFunction0M1Body< Function, M1 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&f_;
}   

template< class Function, class M1 >
_DNBBaseFunctor0Body*
_DNBFunction0M1Body< Function, M1>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBFunction0M1Body< Function, M1 >( *this );
}

template< class Function, class M1 >
bool 
_DNBFunction0M1Body< Function, M1 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction0M1Body< Function, M1 >* rhsBody = dynamic_cast< _DNBFunction0M1Body< Function, M1 > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
            return ( m1_ == rhsBody->m1_ );

    }
    
    return false;
}

template< class Function, class M1 >
bool 
_DNBFunction0M1Body< Function, M1 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction0M1Body< Function, M1 >* rhsBody = dynamic_cast< _DNBFunction0M1Body< Function, M1 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
        {
            return ( m1_ < rhsBody->m1_ );

        }
        else
            return ( f_ < rhsBody->f_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class Function, class M1, class M2 >
_DNBFunction0M2Body< Function, M1, M2 >::_DNBFunction0M2Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class Function, class M1, class M2 >
_DNBFunction0M2Body< Function, M1, M2 >::_DNBFunction0M2Body(const Function& f, M1 m1, M2 m2)
    DNB_THROW_SPEC_ANY :
    f_(f),
    m1_(m1),
    m2_(m2)
{
    DNB_PRECONDITION( f_ );
}

template< class Function, class M1, class M2 >
_DNBFunction0M2Body< Function, M1, M2 >::_DNBFunction0M2Body(const _DNBFunction0M2Body< Function, M1, M2 >& functionFunctor)
    DNB_THROW_SPEC_ANY :
    f_(functionFunctor.f_),
    m1_(functionFunctor.m1_),
    m2_(functionFunctor.m2_)
{
    DNB_PRECONDITION( f_ );
}

template< class Function, class M1, class M2 >
_DNBFunction0M2Body< Function, M1, M2 >::~_DNBFunction0M2Body()
    DNB_THROW_SPEC_ANY
{
}

template< class Function, class M1, class M2>
void 
_DNBFunction0M2Body< Function, M1, M2 >::operator()(  ) 
    DNB_THROW_SPEC_ANY
{ 
    f_( m1_, m2_ );
}

template< class Function, class M1, class M2 >
void*
_DNBFunction0M2Body< Function, M1, M2 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return NULL;
}    
template< class Function, class M1, class M2 >
void*
_DNBFunction0M2Body< Function, M1, M2 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&f_;
}   

template< class Function, class M1, class M2 >
_DNBBaseFunctor0Body*
_DNBFunction0M2Body< Function, M1, M2>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBFunction0M2Body< Function, M1, M2 >( *this );
}

template< class Function, class M1, class M2 >
bool 
_DNBFunction0M2Body< Function, M1, M2 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction0M2Body< Function, M1, M2 >* rhsBody = dynamic_cast< _DNBFunction0M2Body< Function, M1, M2 > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
            if ( m1_ == rhsBody->m1_ )
                return ( m2_ == rhsBody->m2_ );

    }
    
    return false;
}

template< class Function, class M1, class M2 >
bool 
_DNBFunction0M2Body< Function, M1, M2 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction0M2Body< Function, M1, M2 >* rhsBody = dynamic_cast< _DNBFunction0M2Body< Function, M1, M2 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
        {
            if ( m1_ == rhsBody->m1_ )
            {
                return ( m2_ < rhsBody->m2_ );
            }
            else
                return ( m1_ < rhsBody->m1_ );

        }
        else
            return ( f_ < rhsBody->f_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class Function, class M1, class M2, class M3 >
_DNBFunction0M3Body< Function, M1, M2, M3 >::_DNBFunction0M3Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class Function, class M1, class M2, class M3 >
_DNBFunction0M3Body< Function, M1, M2, M3 >::_DNBFunction0M3Body(const Function& f, M1 m1, M2 m2, M3 m3)
    DNB_THROW_SPEC_ANY :
    f_(f),
    m1_(m1),
    m2_(m2),
    m3_(m3)
{
    DNB_PRECONDITION( f_ );
}

template< class Function, class M1, class M2, class M3 >
_DNBFunction0M3Body< Function, M1, M2, M3 >::_DNBFunction0M3Body(const _DNBFunction0M3Body< Function, M1, M2, M3 >& functionFunctor)
    DNB_THROW_SPEC_ANY :
    f_(functionFunctor.f_),
    m1_(functionFunctor.m1_),
    m2_(functionFunctor.m2_),
    m3_(functionFunctor.m3_)
{
    DNB_PRECONDITION( f_ );
}

template< class Function, class M1, class M2, class M3 >
_DNBFunction0M3Body< Function, M1, M2, M3 >::~_DNBFunction0M3Body()
    DNB_THROW_SPEC_ANY
{
}

template< class Function, class M1, class M2, class M3>
void 
_DNBFunction0M3Body< Function, M1, M2, M3 >::operator()(  ) 
    DNB_THROW_SPEC_ANY
{ 
    f_( m1_, m2_, m3_ );
}

template< class Function, class M1, class M2, class M3 >
void*
_DNBFunction0M3Body< Function, M1, M2, M3 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return NULL;
}    
template< class Function, class M1, class M2, class M3 >
void*
_DNBFunction0M3Body< Function, M1, M2, M3 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&f_;
}   

template< class Function, class M1, class M2, class M3 >
_DNBBaseFunctor0Body*
_DNBFunction0M3Body< Function, M1, M2, M3>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBFunction0M3Body< Function, M1, M2, M3 >( *this );
}

template< class Function, class M1, class M2, class M3 >
bool 
_DNBFunction0M3Body< Function, M1, M2, M3 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction0M3Body< Function, M1, M2, M3 >* rhsBody = dynamic_cast< _DNBFunction0M3Body< Function, M1, M2, M3 > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
            if ( m1_ == rhsBody->m1_ )
                if ( m2_ == rhsBody->m2_ )
                    return ( m3_ == rhsBody->m3_ );

    }
    
    return false;
}

template< class Function, class M1, class M2, class M3 >
bool 
_DNBFunction0M3Body< Function, M1, M2, M3 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction0M3Body< Function, M1, M2, M3 >* rhsBody = dynamic_cast< _DNBFunction0M3Body< Function, M1, M2, M3 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
        {
            if ( m1_ == rhsBody->m1_ )
            {
                if ( m2_ == rhsBody->m2_ )
                {
                    return ( m3_ < rhsBody->m3_ );
                }
                else
                    return ( m2_ < rhsBody->m2_ );
            }
            else
                return ( m1_ < rhsBody->m1_ );

        }
        else
            return ( f_ < rhsBody->f_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class Function, class M1, class M2, class M3, class M4 >
_DNBFunction0M4Body< Function, M1, M2, M3, M4 >::_DNBFunction0M4Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class Function, class M1, class M2, class M3, class M4 >
_DNBFunction0M4Body< Function, M1, M2, M3, M4 >::_DNBFunction0M4Body(const Function& f, M1 m1, M2 m2, M3 m3, M4 m4)
    DNB_THROW_SPEC_ANY :
    f_(f),
    m1_(m1),
    m2_(m2),
    m3_(m3),
    m4_(m4)
{
    DNB_PRECONDITION( f_ );
}

template< class Function, class M1, class M2, class M3, class M4 >
_DNBFunction0M4Body< Function, M1, M2, M3, M4 >::_DNBFunction0M4Body(const _DNBFunction0M4Body< Function, M1, M2, M3, M4 >& functionFunctor)
    DNB_THROW_SPEC_ANY :
    f_(functionFunctor.f_),
    m1_(functionFunctor.m1_),
    m2_(functionFunctor.m2_),
    m3_(functionFunctor.m3_),
    m4_(functionFunctor.m4_)
{
    DNB_PRECONDITION( f_ );
}

template< class Function, class M1, class M2, class M3, class M4 >
_DNBFunction0M4Body< Function, M1, M2, M3, M4 >::~_DNBFunction0M4Body()
    DNB_THROW_SPEC_ANY
{
}

template< class Function, class M1, class M2, class M3, class M4>
void 
_DNBFunction0M4Body< Function, M1, M2, M3, M4 >::operator()(  ) 
    DNB_THROW_SPEC_ANY
{ 
    f_( m1_, m2_, m3_, m4_ );
}

template< class Function, class M1, class M2, class M3, class M4 >
void*
_DNBFunction0M4Body< Function, M1, M2, M3, M4 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return NULL;
}    
template< class Function, class M1, class M2, class M3, class M4 >
void*
_DNBFunction0M4Body< Function, M1, M2, M3, M4 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&f_;
}   

template< class Function, class M1, class M2, class M3, class M4 >
_DNBBaseFunctor0Body*
_DNBFunction0M4Body< Function, M1, M2, M3, M4>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBFunction0M4Body< Function, M1, M2, M3, M4 >( *this );
}

template< class Function, class M1, class M2, class M3, class M4 >
bool 
_DNBFunction0M4Body< Function, M1, M2, M3, M4 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction0M4Body< Function, M1, M2, M3, M4 >* rhsBody = dynamic_cast< _DNBFunction0M4Body< Function, M1, M2, M3, M4 > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
            if ( m1_ == rhsBody->m1_ )
                if ( m2_ == rhsBody->m2_ )
                    if ( m3_ == rhsBody->m3_ )
                        return ( m4_ == rhsBody->m4_ );

    }
    
    return false;
}

template< class Function, class M1, class M2, class M3, class M4 >
bool 
_DNBFunction0M4Body< Function, M1, M2, M3, M4 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction0M4Body< Function, M1, M2, M3, M4 >* rhsBody = dynamic_cast< _DNBFunction0M4Body< Function, M1, M2, M3, M4 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
        {
            if ( m1_ == rhsBody->m1_ )
            {
                if ( m2_ == rhsBody->m2_ )
                {
                    if ( m3_ == rhsBody->m3_ )
                    {
                        return ( m4_ < rhsBody->m4_ );
                    }
                    else
                        return ( m3_ < rhsBody->m3_ );
                }
                else
                    return ( m2_ < rhsBody->m2_ );
            }
            else
                return ( m1_ < rhsBody->m1_ );

        }
        else
            return ( f_ < rhsBody->f_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class Function, class M1, class M2, class M3, class M4, class M5 >
_DNBFunction0M5Body< Function, M1, M2, M3, M4, M5 >::_DNBFunction0M5Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class Function, class M1, class M2, class M3, class M4, class M5 >
_DNBFunction0M5Body< Function, M1, M2, M3, M4, M5 >::_DNBFunction0M5Body(const Function& f, M1 m1, M2 m2, M3 m3, M4 m4, M5 m5)
    DNB_THROW_SPEC_ANY :
    f_(f),
    m1_(m1),
    m2_(m2),
    m3_(m3),
    m4_(m4),
    m5_(m5)
{
    DNB_PRECONDITION( f_ );
}

template< class Function, class M1, class M2, class M3, class M4, class M5 >
_DNBFunction0M5Body< Function, M1, M2, M3, M4, M5 >::_DNBFunction0M5Body(const _DNBFunction0M5Body< Function, M1, M2, M3, M4, M5 >& functionFunctor)
    DNB_THROW_SPEC_ANY :
    f_(functionFunctor.f_),
    m1_(functionFunctor.m1_),
    m2_(functionFunctor.m2_),
    m3_(functionFunctor.m3_),
    m4_(functionFunctor.m4_),
    m5_(functionFunctor.m5_)
{
    DNB_PRECONDITION( f_ );
}

template< class Function, class M1, class M2, class M3, class M4, class M5 >
_DNBFunction0M5Body< Function, M1, M2, M3, M4, M5 >::~_DNBFunction0M5Body()
    DNB_THROW_SPEC_ANY
{
}

template< class Function, class M1, class M2, class M3, class M4, class M5>
void 
_DNBFunction0M5Body< Function, M1, M2, M3, M4, M5 >::operator()(  ) 
    DNB_THROW_SPEC_ANY
{ 
    f_( m1_, m2_, m3_, m4_, m5_ );
}

template< class Function, class M1, class M2, class M3, class M4, class M5 >
void*
_DNBFunction0M5Body< Function, M1, M2, M3, M4, M5 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return NULL;
}    
template< class Function, class M1, class M2, class M3, class M4, class M5 >
void*
_DNBFunction0M5Body< Function, M1, M2, M3, M4, M5 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&f_;
}   

template< class Function, class M1, class M2, class M3, class M4, class M5 >
_DNBBaseFunctor0Body*
_DNBFunction0M5Body< Function, M1, M2, M3, M4, M5>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBFunction0M5Body< Function, M1, M2, M3, M4, M5 >( *this );
}

template< class Function, class M1, class M2, class M3, class M4, class M5 >
bool 
_DNBFunction0M5Body< Function, M1, M2, M3, M4, M5 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction0M5Body< Function, M1, M2, M3, M4, M5 >* rhsBody = dynamic_cast< _DNBFunction0M5Body< Function, M1, M2, M3, M4, M5 > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
            if ( m1_ == rhsBody->m1_ )
                if ( m2_ == rhsBody->m2_ )
                    if ( m3_ == rhsBody->m3_ )
                        if ( m4_ == rhsBody->m4_ )
                            return ( m5_ == rhsBody->m5_ );

    }
    
    return false;
}

template< class Function, class M1, class M2, class M3, class M4, class M5 >
bool 
_DNBFunction0M5Body< Function, M1, M2, M3, M4, M5 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction0M5Body< Function, M1, M2, M3, M4, M5 >* rhsBody = dynamic_cast< _DNBFunction0M5Body< Function, M1, M2, M3, M4, M5 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
        {
            if ( m1_ == rhsBody->m1_ )
            {
                if ( m2_ == rhsBody->m2_ )
                {
                    if ( m3_ == rhsBody->m3_ )
                    {
                        if ( m4_ == rhsBody->m4_ )
                        {
                            return ( m5_ < rhsBody->m5_ );
                        }
                        else
                            return ( m4_ < rhsBody->m4_ );
                    }
                    else
                        return ( m3_ < rhsBody->m3_ );
                }
                else
                    return ( m2_ < rhsBody->m2_ );
            }
            else
                return ( m1_ < rhsBody->m1_ );

        }
        else
            return ( f_ < rhsBody->f_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class Client, class Member >
_DNBMemberFunctor0Body< Client, Member >::_DNBMemberFunctor0Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class Client, class Member >
_DNBMemberFunctor0Body< Client, Member >::_DNBMemberFunctor0Body( const _DNBMemberFunctor0Body< Client, Member >& memberFunctor )
    DNB_THROW_SPEC_ANY
{
    client_ = memberFunctor.client_;
    member_ = memberFunctor.member_;
}        

template< class Client, class Member >
_DNBMemberFunctor0Body< Client, Member >::_DNBMemberFunctor0Body(Client* client, Member member)
    DNB_THROW_SPEC_ANY:
    client_(client), 
    member_(member)
{
    DNB_PRECONDITION( client_ );
    DNB_PRECONDITION( member_ );
}

template< class Client, class Member >
_DNBMemberFunctor0Body< Client, Member >::~_DNBMemberFunctor0Body()
    DNB_THROW_SPEC_ANY
{
}

template< class Client, class Member >
void
_DNBMemberFunctor0Body< Client, Member >::operator()(  )
    DNB_THROW_SPEC_ANY
{
    (client_->*member_)(  );
}

template< class Client, class Member >
void *
_DNBMemberFunctor0Body< Client, Member >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return (void *)client_;
}

template< class Client, class Member >
void *
_DNBMemberFunctor0Body< Client, Member >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&member_;
}

template< class Client, class Member >
_DNBBaseFunctor0Body* 
_DNBMemberFunctor0Body< Client, Member >::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return DNB_NEW _DNBMemberFunctor0Body< Client, Member >( *this );
}

template< class Client, class Member >
bool 
_DNBMemberFunctor0Body< Client, Member >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor0Body< Client, Member >* rhsBody = dynamic_cast< _DNBMemberFunctor0Body< Client, Member > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        return ( ( client_ == rhsBody->client_ ) && ( member_ == rhsBody->member_ ) );
    }

    return false;
}

template< class Client, class Member >
bool 
_DNBMemberFunctor0Body< Client, Member >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor0Body< Client, Member >* rhsBody = dynamic_cast< _DNBMemberFunctor0Body< Client, Member > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
            return ( (void *)&member_ < (void *)&(rhsBody->member_) );
        else
            return ( client_ < rhsBody->client_ );
    }
    else
        return ( (void *)this < (void *)&rhs );
}
template< class Client, class Member, class M1 >
_DNBMemberFunctor0M1Body< Client, Member, M1 >::_DNBMemberFunctor0M1Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class Client, class Member, class M1 >
_DNBMemberFunctor0M1Body< Client, Member, M1 >::_DNBMemberFunctor0M1Body(const _DNBMemberFunctor0M1Body< Client, Member, M1 >& memberFunctor)
    DNB_THROW_SPEC_ANY: 
m1_(memberFunctor.m1_)
{
   client_ = memberFunctor.client_;
    member_ = memberFunctor.member_;
}

template< class Client, class Member, class M1 >
_DNBMemberFunctor0M1Body< Client, Member, M1 >::_DNBMemberFunctor0M1Body(Client* client, Member member, M1 m1)
    DNB_THROW_SPEC_ANY:
client_(client), 
    member_(member),
    m1_(m1)
{
    DNB_PRECONDITION( client_ );
    DNB_PRECONDITION( member_ );
}

template< class Client, class Member, class M1 >
_DNBMemberFunctor0M1Body< Client, Member, M1 >::~_DNBMemberFunctor0M1Body()
    DNB_THROW_SPEC_ANY
{
}

template< class Client, class Member, class M1>
void 
_DNBMemberFunctor0M1Body< Client, Member, M1 >::operator()(  ) 
    DNB_THROW_SPEC_ANY
{ 
    ( client_->*member_ )( m1_ );
}

template< class Client, class Member, class M1 >
void*
_DNBMemberFunctor0M1Body< Client, Member, M1 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return (void *)client_;
}    
template< class Client, class Member, class M1 >
void*
_DNBMemberFunctor0M1Body< Client, Member, M1 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&member_;
}   

template< class Client, class Member, class M1 >
_DNBBaseFunctor0Body*
_DNBMemberFunctor0M1Body< Client, Member, M1>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBMemberFunctor0M1Body< Client, Member, M1 >( *this );
}

template< class Client, class Member, class M1 >
bool 
_DNBMemberFunctor0M1Body< Client, Member, M1 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor0M1Body< Client, Member, M1 >* rhsBody = dynamic_cast< _DNBMemberFunctor0M1Body< Client, Member, M1 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
            if ( member_ == rhsBody->member_ )
                return ( m1_ == rhsBody->m1_ );
    }
    
    return false;
}

template< class Client, class Member, class M1 >
bool 
_DNBMemberFunctor0M1Body< Client, Member, M1 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor0M1Body< Client, Member, M1 >* rhsBody = dynamic_cast< _DNBMemberFunctor0M1Body< Client, Member, M1 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
        {
            if ( member_ == rhsBody->member_ )
            {
                return ( m1_ < rhsBody->m1_ );
            }
            else
                return ( (void *)&member_ < (void *)&(rhsBody->member_) );
        }
        else
            return ( client_ < rhsBody->client_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class Client, class Member, class M1, class M2 >
_DNBMemberFunctor0M2Body< Client, Member, M1, M2 >::_DNBMemberFunctor0M2Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class Client, class Member, class M1, class M2 >
_DNBMemberFunctor0M2Body< Client, Member, M1, M2 >::_DNBMemberFunctor0M2Body(const _DNBMemberFunctor0M2Body< Client, Member, M1, M2 >& memberFunctor)
    DNB_THROW_SPEC_ANY: 
m1_(memberFunctor.m1_),
    m2_(memberFunctor.m2_)
{
   client_ = memberFunctor.client_;
    member_ = memberFunctor.member_;
}

template< class Client, class Member, class M1, class M2 >
_DNBMemberFunctor0M2Body< Client, Member, M1, M2 >::_DNBMemberFunctor0M2Body(Client* client, Member member, M1 m1, M2 m2)
    DNB_THROW_SPEC_ANY:
client_(client), 
    member_(member),
    m1_(m1),
    m2_(m2)
{
    DNB_PRECONDITION( client_ );
    DNB_PRECONDITION( member_ );
}

template< class Client, class Member, class M1, class M2 >
_DNBMemberFunctor0M2Body< Client, Member, M1, M2 >::~_DNBMemberFunctor0M2Body()
    DNB_THROW_SPEC_ANY
{
}

template< class Client, class Member, class M1, class M2>
void 
_DNBMemberFunctor0M2Body< Client, Member, M1, M2 >::operator()(  ) 
    DNB_THROW_SPEC_ANY
{ 
    ( client_->*member_ )( m1_, m2_ );
}

template< class Client, class Member, class M1, class M2 >
void*
_DNBMemberFunctor0M2Body< Client, Member, M1, M2 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return (void *)client_;
}    
template< class Client, class Member, class M1, class M2 >
void*
_DNBMemberFunctor0M2Body< Client, Member, M1, M2 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&member_;
}   

template< class Client, class Member, class M1, class M2 >
_DNBBaseFunctor0Body*
_DNBMemberFunctor0M2Body< Client, Member, M1, M2>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBMemberFunctor0M2Body< Client, Member, M1, M2 >( *this );
}

template< class Client, class Member, class M1, class M2 >
bool 
_DNBMemberFunctor0M2Body< Client, Member, M1, M2 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor0M2Body< Client, Member, M1, M2 >* rhsBody = dynamic_cast< _DNBMemberFunctor0M2Body< Client, Member, M1, M2 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
            if ( member_ == rhsBody->member_ )
                if ( m1_ == rhsBody->m1_ )
                    return ( m2_ == rhsBody->m2_ );
    }
    
    return false;
}

template< class Client, class Member, class M1, class M2 >
bool 
_DNBMemberFunctor0M2Body< Client, Member, M1, M2 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor0M2Body< Client, Member, M1, M2 >* rhsBody = dynamic_cast< _DNBMemberFunctor0M2Body< Client, Member, M1, M2 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
        {
            if ( member_ == rhsBody->member_ )
            {
                if ( m1_ == rhsBody->m1_ )
                {
                    return ( m2_ < rhsBody->m2_ );
                }
                else
                    return ( m1_ < rhsBody->m1_ );
            }
            else
                return ( (void *)&member_ < (void *)&(rhsBody->member_) );
        }
        else
            return ( client_ < rhsBody->client_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class Client, class Member, class M1, class M2, class M3 >
_DNBMemberFunctor0M3Body< Client, Member, M1, M2, M3 >::_DNBMemberFunctor0M3Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class Client, class Member, class M1, class M2, class M3 >
_DNBMemberFunctor0M3Body< Client, Member, M1, M2, M3 >::_DNBMemberFunctor0M3Body(const _DNBMemberFunctor0M3Body< Client, Member, M1, M2, M3 >& memberFunctor)
    DNB_THROW_SPEC_ANY: 
m1_(memberFunctor.m1_),
    m2_(memberFunctor.m2_),
    m3_(memberFunctor.m3_)
{
   client_ = memberFunctor.client_;
    member_ = memberFunctor.member_;
}

template< class Client, class Member, class M1, class M2, class M3 >
_DNBMemberFunctor0M3Body< Client, Member, M1, M2, M3 >::_DNBMemberFunctor0M3Body(Client* client, Member member, M1 m1, M2 m2, M3 m3)
    DNB_THROW_SPEC_ANY:
client_(client), 
    member_(member),
    m1_(m1),
    m2_(m2),
    m3_(m3)
{
    DNB_PRECONDITION( client_ );
    DNB_PRECONDITION( member_ );
}

template< class Client, class Member, class M1, class M2, class M3 >
_DNBMemberFunctor0M3Body< Client, Member, M1, M2, M3 >::~_DNBMemberFunctor0M3Body()
    DNB_THROW_SPEC_ANY
{
}

template< class Client, class Member, class M1, class M2, class M3>
void 
_DNBMemberFunctor0M3Body< Client, Member, M1, M2, M3 >::operator()(  ) 
    DNB_THROW_SPEC_ANY
{ 
    ( client_->*member_ )( m1_, m2_, m3_ );
}

template< class Client, class Member, class M1, class M2, class M3 >
void*
_DNBMemberFunctor0M3Body< Client, Member, M1, M2, M3 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return (void *)client_;
}    
template< class Client, class Member, class M1, class M2, class M3 >
void*
_DNBMemberFunctor0M3Body< Client, Member, M1, M2, M3 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&member_;
}   

template< class Client, class Member, class M1, class M2, class M3 >
_DNBBaseFunctor0Body*
_DNBMemberFunctor0M3Body< Client, Member, M1, M2, M3>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBMemberFunctor0M3Body< Client, Member, M1, M2, M3 >( *this );
}

template< class Client, class Member, class M1, class M2, class M3 >
bool 
_DNBMemberFunctor0M3Body< Client, Member, M1, M2, M3 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor0M3Body< Client, Member, M1, M2, M3 >* rhsBody = dynamic_cast< _DNBMemberFunctor0M3Body< Client, Member, M1, M2, M3 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
            if ( member_ == rhsBody->member_ )
                if ( m1_ == rhsBody->m1_ )
                    if ( m2_ == rhsBody->m2_ )
                        return ( m3_ == rhsBody->m3_ );
    }
    
    return false;
}

template< class Client, class Member, class M1, class M2, class M3 >
bool 
_DNBMemberFunctor0M3Body< Client, Member, M1, M2, M3 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor0M3Body< Client, Member, M1, M2, M3 >* rhsBody = dynamic_cast< _DNBMemberFunctor0M3Body< Client, Member, M1, M2, M3 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
        {
            if ( member_ == rhsBody->member_ )
            {
                if ( m1_ == rhsBody->m1_ )
                {
                    if ( m2_ == rhsBody->m2_ )
                    {
                        return ( m3_ < rhsBody->m3_ );
                    }
                    else
                        return ( m2_ < rhsBody->m2_ );
                }
                else
                    return ( m1_ < rhsBody->m1_ );
            }
            else
                return ( (void *)&member_ < (void *)&(rhsBody->member_) );
        }
        else
            return ( client_ < rhsBody->client_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class Client, class Member, class M1, class M2, class M3, class M4 >
_DNBMemberFunctor0M4Body< Client, Member, M1, M2, M3, M4 >::_DNBMemberFunctor0M4Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class Client, class Member, class M1, class M2, class M3, class M4 >
_DNBMemberFunctor0M4Body< Client, Member, M1, M2, M3, M4 >::_DNBMemberFunctor0M4Body(const _DNBMemberFunctor0M4Body< Client, Member, M1, M2, M3, M4 >& memberFunctor)
    DNB_THROW_SPEC_ANY: 
m1_(memberFunctor.m1_),
    m2_(memberFunctor.m2_),
    m3_(memberFunctor.m3_),
    m4_(memberFunctor.m4_)
{
   client_ = memberFunctor.client_;
    member_ = memberFunctor.member_;
}

template< class Client, class Member, class M1, class M2, class M3, class M4 >
_DNBMemberFunctor0M4Body< Client, Member, M1, M2, M3, M4 >::_DNBMemberFunctor0M4Body(Client* client, Member member, M1 m1, M2 m2, M3 m3, M4 m4)
    DNB_THROW_SPEC_ANY:
client_(client), 
    member_(member),
    m1_(m1),
    m2_(m2),
    m3_(m3),
    m4_(m4)
{
    DNB_PRECONDITION( client_ );
    DNB_PRECONDITION( member_ );
}

template< class Client, class Member, class M1, class M2, class M3, class M4 >
_DNBMemberFunctor0M4Body< Client, Member, M1, M2, M3, M4 >::~_DNBMemberFunctor0M4Body()
    DNB_THROW_SPEC_ANY
{
}

template< class Client, class Member, class M1, class M2, class M3, class M4>
void 
_DNBMemberFunctor0M4Body< Client, Member, M1, M2, M3, M4 >::operator()(  ) 
    DNB_THROW_SPEC_ANY
{ 
    ( client_->*member_ )( m1_, m2_, m3_, m4_ );
}

template< class Client, class Member, class M1, class M2, class M3, class M4 >
void*
_DNBMemberFunctor0M4Body< Client, Member, M1, M2, M3, M4 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return (void *)client_;
}    
template< class Client, class Member, class M1, class M2, class M3, class M4 >
void*
_DNBMemberFunctor0M4Body< Client, Member, M1, M2, M3, M4 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&member_;
}   

template< class Client, class Member, class M1, class M2, class M3, class M4 >
_DNBBaseFunctor0Body*
_DNBMemberFunctor0M4Body< Client, Member, M1, M2, M3, M4>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBMemberFunctor0M4Body< Client, Member, M1, M2, M3, M4 >( *this );
}

template< class Client, class Member, class M1, class M2, class M3, class M4 >
bool 
_DNBMemberFunctor0M4Body< Client, Member, M1, M2, M3, M4 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor0M4Body< Client, Member, M1, M2, M3, M4 >* rhsBody = dynamic_cast< _DNBMemberFunctor0M4Body< Client, Member, M1, M2, M3, M4 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
            if ( member_ == rhsBody->member_ )
                if ( m1_ == rhsBody->m1_ )
                    if ( m2_ == rhsBody->m2_ )
                        if ( m3_ == rhsBody->m3_ )
                            return ( m4_ == rhsBody->m4_ );
    }
    
    return false;
}

template< class Client, class Member, class M1, class M2, class M3, class M4 >
bool 
_DNBMemberFunctor0M4Body< Client, Member, M1, M2, M3, M4 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor0M4Body< Client, Member, M1, M2, M3, M4 >* rhsBody = dynamic_cast< _DNBMemberFunctor0M4Body< Client, Member, M1, M2, M3, M4 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
        {
            if ( member_ == rhsBody->member_ )
            {
                if ( m1_ == rhsBody->m1_ )
                {
                    if ( m2_ == rhsBody->m2_ )
                    {
                        if ( m3_ == rhsBody->m3_ )
                        {
                            return ( m4_ < rhsBody->m4_ );
                        }
                        else
                            return ( m3_ < rhsBody->m3_ );
                    }
                    else
                        return ( m2_ < rhsBody->m2_ );
                }
                else
                    return ( m1_ < rhsBody->m1_ );
            }
            else
                return ( (void *)&member_ < (void *)&(rhsBody->member_) );
        }
        else
            return ( client_ < rhsBody->client_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class Client, class Member, class M1, class M2, class M3, class M4, class M5 >
_DNBMemberFunctor0M5Body< Client, Member, M1, M2, M3, M4, M5 >::_DNBMemberFunctor0M5Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class Client, class Member, class M1, class M2, class M3, class M4, class M5 >
_DNBMemberFunctor0M5Body< Client, Member, M1, M2, M3, M4, M5 >::_DNBMemberFunctor0M5Body(const _DNBMemberFunctor0M5Body< Client, Member, M1, M2, M3, M4, M5 >& memberFunctor)
    DNB_THROW_SPEC_ANY: 
m1_(memberFunctor.m1_),
    m2_(memberFunctor.m2_),
    m3_(memberFunctor.m3_),
    m4_(memberFunctor.m4_),
    m5_(memberFunctor.m5_)
{
   client_ = memberFunctor.client_;
    member_ = memberFunctor.member_;
}

template< class Client, class Member, class M1, class M2, class M3, class M4, class M5 >
_DNBMemberFunctor0M5Body< Client, Member, M1, M2, M3, M4, M5 >::_DNBMemberFunctor0M5Body(Client* client, Member member, M1 m1, M2 m2, M3 m3, M4 m4, M5 m5)
    DNB_THROW_SPEC_ANY:
client_(client), 
    member_(member),
    m1_(m1),
    m2_(m2),
    m3_(m3),
    m4_(m4),
    m5_(m5)
{
    DNB_PRECONDITION( client_ );
    DNB_PRECONDITION( member_ );
}

template< class Client, class Member, class M1, class M2, class M3, class M4, class M5 >
_DNBMemberFunctor0M5Body< Client, Member, M1, M2, M3, M4, M5 >::~_DNBMemberFunctor0M5Body()
    DNB_THROW_SPEC_ANY
{
}

template< class Client, class Member, class M1, class M2, class M3, class M4, class M5>
void 
_DNBMemberFunctor0M5Body< Client, Member, M1, M2, M3, M4, M5 >::operator()(  ) 
    DNB_THROW_SPEC_ANY
{ 
    ( client_->*member_ )( m1_, m2_, m3_, m4_, m5_ );
}

template< class Client, class Member, class M1, class M2, class M3, class M4, class M5 >
void*
_DNBMemberFunctor0M5Body< Client, Member, M1, M2, M3, M4, M5 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return (void *)client_;
}    
template< class Client, class Member, class M1, class M2, class M3, class M4, class M5 >
void*
_DNBMemberFunctor0M5Body< Client, Member, M1, M2, M3, M4, M5 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&member_;
}   

template< class Client, class Member, class M1, class M2, class M3, class M4, class M5 >
_DNBBaseFunctor0Body*
_DNBMemberFunctor0M5Body< Client, Member, M1, M2, M3, M4, M5>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBMemberFunctor0M5Body< Client, Member, M1, M2, M3, M4, M5 >( *this );
}

template< class Client, class Member, class M1, class M2, class M3, class M4, class M5 >
bool 
_DNBMemberFunctor0M5Body< Client, Member, M1, M2, M3, M4, M5 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor0M5Body< Client, Member, M1, M2, M3, M4, M5 >* rhsBody = dynamic_cast< _DNBMemberFunctor0M5Body< Client, Member, M1, M2, M3, M4, M5 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
            if ( member_ == rhsBody->member_ )
                if ( m1_ == rhsBody->m1_ )
                    if ( m2_ == rhsBody->m2_ )
                        if ( m3_ == rhsBody->m3_ )
                            if ( m4_ == rhsBody->m4_ )
                                return ( m5_ == rhsBody->m5_ );
    }
    
    return false;
}

template< class Client, class Member, class M1, class M2, class M3, class M4, class M5 >
bool 
_DNBMemberFunctor0M5Body< Client, Member, M1, M2, M3, M4, M5 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor0M5Body< Client, Member, M1, M2, M3, M4, M5 >* rhsBody = dynamic_cast< _DNBMemberFunctor0M5Body< Client, Member, M1, M2, M3, M4, M5 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
        {
            if ( member_ == rhsBody->member_ )
            {
                if ( m1_ == rhsBody->m1_ )
                {
                    if ( m2_ == rhsBody->m2_ )
                    {
                        if ( m3_ == rhsBody->m3_ )
                        {
                            if ( m4_ == rhsBody->m4_ )
                            {
                                return ( m5_ < rhsBody->m5_ );
                            }
                            else
                                return ( m4_ < rhsBody->m4_ );
                        }
                        else
                            return ( m3_ < rhsBody->m3_ );
                    }
                    else
                        return ( m2_ < rhsBody->m2_ );
                }
                else
                    return ( m1_ < rhsBody->m1_ );
            }
            else
                return ( (void *)&member_ < (void *)&(rhsBody->member_) );
        }
        else
            return ( client_ < rhsBody->client_ );
    }

    return ( (void *)this < (void *)&rhs );
}
/////////////////////////////////////////////////////////
//
// 1 Passed,  0 Returned 
//
/////////////////////////////////////////////////////////

template< class A1, class Function >
_DNBFunction1Body< A1, Function>::_DNBFunction1Body()
    DNB_THROW_SPEC_ANY
{
}


template< class A1, class Function >
_DNBFunction1Body< A1, Function>::_DNBFunction1Body(const Function& f)
    DNB_THROW_SPEC_ANY:
    f_(f) 
{
    DNB_PRECONDITION( f_ );
}

template< class A1, class Function >
_DNBFunction1Body< A1, Function>::_DNBFunction1Body(const _DNBFunction1Body< A1, Function>& functionFunctor)
    DNB_THROW_SPEC_ANY:
     f_(functionFunctor.f_)
{
    DNB_PRECONDITION( f_ );
}

template< class A1, class Function >
_DNBFunction1Body< A1, Function>::~_DNBFunction1Body()    
DNB_THROW_SPEC_ANY
{
}

template< class A1, class Function >
void
_DNBFunction1Body< A1, Function>::operator()( A1 a1 )
    DNB_THROW_SPEC_ANY
{
    f_(a1);
}

template< class A1, class Function >
void*
_DNBFunction1Body< A1, Function>::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return NULL;
}

template< class A1, class Function >
void*
_DNBFunction1Body< A1, Function>::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&f_;
}   

template< class A1, class Function >
_DNBBaseFunctor1Body< A1 >*
_DNBFunction1Body< A1, Function>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return DNB_NEW _DNBFunction1Body< A1, Function>(*this);
}

template< class A1, class Function >
bool 
_DNBFunction1Body< A1, Function >::IsEqual(_DNBBaseFunctorBody& rhs) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction1Body< A1, Function >* rhsBody = dynamic_cast< _DNBFunction1Body< A1, Function > *  > (&rhs);

    if (rhsBody != NULL)
    {
        return( f_ == rhsBody->f_ );
    }
    return false;
}

template< class A1, class Function >
bool 
_DNBFunction1Body< A1, Function >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction1Body< A1, Function >* rhsBody = dynamic_cast< _DNBFunction1Body< A1, Function > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        return ( f_ < rhsBody->f_ );
    }
    else
        return ( (void *)this < (void *)&rhs );
}

template< class A1, class Function, class M1 >
_DNBFunction1M1Body< A1, Function, M1 >::_DNBFunction1M1Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class A1, class Function, class M1 >
_DNBFunction1M1Body< A1, Function, M1 >::_DNBFunction1M1Body(const Function& f, M1 m1)
    DNB_THROW_SPEC_ANY :
    f_(f),
    m1_(m1)
{
    DNB_PRECONDITION( f_ );
}

template< class A1, class Function, class M1 >
_DNBFunction1M1Body< A1, Function, M1 >::_DNBFunction1M1Body(const _DNBFunction1M1Body< A1, Function, M1 >& functionFunctor)
    DNB_THROW_SPEC_ANY :
    f_(functionFunctor.f_),
    m1_(functionFunctor.m1_)
{
    DNB_PRECONDITION( f_ );
}

template< class A1, class Function, class M1 >
_DNBFunction1M1Body< A1, Function, M1 >::~_DNBFunction1M1Body()
    DNB_THROW_SPEC_ANY
{
}

template< class A1, class Function, class M1>
void 
_DNBFunction1M1Body< A1, Function, M1 >::operator()( A1 a1 ) 
    DNB_THROW_SPEC_ANY
{ 
    f_( m1_, a1 );
}

template< class A1, class Function, class M1 >
void*
_DNBFunction1M1Body< A1, Function, M1 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return NULL;
}    
template< class A1, class Function, class M1 >
void*
_DNBFunction1M1Body< A1, Function, M1 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&f_;
}   

template< class A1, class Function, class M1 >
_DNBBaseFunctor1Body< A1 >*
_DNBFunction1M1Body< A1, Function, M1>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBFunction1M1Body< A1, Function, M1 >( *this );
}

template< class A1, class Function, class M1 >
bool 
_DNBFunction1M1Body< A1, Function, M1 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction1M1Body< A1, Function, M1 >* rhsBody = dynamic_cast< _DNBFunction1M1Body< A1, Function, M1 > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
            return ( m1_ == rhsBody->m1_ );

    }
    
    return false;
}

template< class A1, class Function, class M1 >
bool 
_DNBFunction1M1Body< A1, Function, M1 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction1M1Body< A1, Function, M1 >* rhsBody = dynamic_cast< _DNBFunction1M1Body< A1, Function, M1 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
        {
            return ( m1_ < rhsBody->m1_ );

        }
        else
            return ( f_ < rhsBody->f_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class A1, class Function, class M1, class M2 >
_DNBFunction1M2Body< A1, Function, M1, M2 >::_DNBFunction1M2Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class A1, class Function, class M1, class M2 >
_DNBFunction1M2Body< A1, Function, M1, M2 >::_DNBFunction1M2Body(const Function& f, M1 m1, M2 m2)
    DNB_THROW_SPEC_ANY :
    f_(f),
    m1_(m1),
    m2_(m2)
{
    DNB_PRECONDITION( f_ );
}

template< class A1, class Function, class M1, class M2 >
_DNBFunction1M2Body< A1, Function, M1, M2 >::_DNBFunction1M2Body(const _DNBFunction1M2Body< A1, Function, M1, M2 >& functionFunctor)
    DNB_THROW_SPEC_ANY :
    f_(functionFunctor.f_),
    m1_(functionFunctor.m1_),
    m2_(functionFunctor.m2_)
{
    DNB_PRECONDITION( f_ );
}

template< class A1, class Function, class M1, class M2 >
_DNBFunction1M2Body< A1, Function, M1, M2 >::~_DNBFunction1M2Body()
    DNB_THROW_SPEC_ANY
{
}

template< class A1, class Function, class M1, class M2>
void 
_DNBFunction1M2Body< A1, Function, M1, M2 >::operator()( A1 a1 ) 
    DNB_THROW_SPEC_ANY
{ 
    f_( a1, m1_, m2_ );
}

template< class A1, class Function, class M1, class M2 >
void*
_DNBFunction1M2Body< A1, Function, M1, M2 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return NULL;
}    
template< class A1, class Function, class M1, class M2 >
void*
_DNBFunction1M2Body< A1, Function, M1, M2 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&f_;
}   

template< class A1, class Function, class M1, class M2 >
_DNBBaseFunctor1Body< A1 >*
_DNBFunction1M2Body< A1, Function, M1, M2>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBFunction1M2Body< A1, Function, M1, M2 >( *this );
}

template< class A1, class Function, class M1, class M2 >
bool 
_DNBFunction1M2Body< A1, Function, M1, M2 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction1M2Body< A1, Function, M1, M2 >* rhsBody = dynamic_cast< _DNBFunction1M2Body< A1, Function, M1, M2 > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
            if ( m1_ == rhsBody->m1_ )
                return ( m2_ == rhsBody->m2_ );

    }
    
    return false;
}

template< class A1, class Function, class M1, class M2 >
bool 
_DNBFunction1M2Body< A1, Function, M1, M2 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction1M2Body< A1, Function, M1, M2 >* rhsBody = dynamic_cast< _DNBFunction1M2Body< A1, Function, M1, M2 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
        {
            if ( m1_ == rhsBody->m1_ )
            {
                return ( m2_ < rhsBody->m2_ );
            }
            else
                return ( m1_ < rhsBody->m1_ );

        }
        else
            return ( f_ < rhsBody->f_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class A1, class Function, class M1, class M2, class M3 >
_DNBFunction1M3Body< A1, Function, M1, M2, M3 >::_DNBFunction1M3Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class A1, class Function, class M1, class M2, class M3 >
_DNBFunction1M3Body< A1, Function, M1, M2, M3 >::_DNBFunction1M3Body(const Function& f, M1 m1, M2 m2, M3 m3)
    DNB_THROW_SPEC_ANY :
    f_(f),
    m1_(m1),
    m2_(m2),
    m3_(m3)
{
    DNB_PRECONDITION( f_ );
}

template< class A1, class Function, class M1, class M2, class M3 >
_DNBFunction1M3Body< A1, Function, M1, M2, M3 >::_DNBFunction1M3Body(const _DNBFunction1M3Body< A1, Function, M1, M2, M3 >& functionFunctor)
    DNB_THROW_SPEC_ANY :
    f_(functionFunctor.f_),
    m1_(functionFunctor.m1_),
    m2_(functionFunctor.m2_),
    m3_(functionFunctor.m3_)
{
    DNB_PRECONDITION( f_ );
}

template< class A1, class Function, class M1, class M2, class M3 >
_DNBFunction1M3Body< A1, Function, M1, M2, M3 >::~_DNBFunction1M3Body()
    DNB_THROW_SPEC_ANY
{
}

template< class A1, class Function, class M1, class M2, class M3>
void 
_DNBFunction1M3Body< A1, Function, M1, M2, M3 >::operator()( A1 a1 ) 
    DNB_THROW_SPEC_ANY
{ 
    f_( a1, m1_, m2_, m3_ );
}

template< class A1, class Function, class M1, class M2, class M3 >
void*
_DNBFunction1M3Body< A1, Function, M1, M2, M3 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return NULL;
}    
template< class A1, class Function, class M1, class M2, class M3 >
void*
_DNBFunction1M3Body< A1, Function, M1, M2, M3 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&f_;
}   

template< class A1, class Function, class M1, class M2, class M3 >
_DNBBaseFunctor1Body< A1 >*
_DNBFunction1M3Body< A1, Function, M1, M2, M3>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBFunction1M3Body< A1, Function, M1, M2, M3 >( *this );
}

template< class A1, class Function, class M1, class M2, class M3 >
bool 
_DNBFunction1M3Body< A1, Function, M1, M2, M3 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction1M3Body< A1, Function, M1, M2, M3 >* rhsBody = dynamic_cast< _DNBFunction1M3Body< A1, Function, M1, M2, M3 > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
            if ( m1_ == rhsBody->m1_ )
                if ( m2_ == rhsBody->m2_ )
                    return ( m3_ == rhsBody->m3_ );

    }
    
    return false;
}

template< class A1, class Function, class M1, class M2, class M3 >
bool 
_DNBFunction1M3Body< A1, Function, M1, M2, M3 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction1M3Body< A1, Function, M1, M2, M3 >* rhsBody = dynamic_cast< _DNBFunction1M3Body< A1, Function, M1, M2, M3 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
        {
            if ( m1_ == rhsBody->m1_ )
            {
                if ( m2_ == rhsBody->m2_ )
                {
                    return ( m3_ < rhsBody->m3_ );
                }
                else
                    return ( m2_ < rhsBody->m2_ );
            }
            else
                return ( m1_ < rhsBody->m1_ );

        }
        else
            return ( f_ < rhsBody->f_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class A1, class Function, class M1, class M2, class M3, class M4 >
_DNBFunction1M4Body< A1, Function, M1, M2, M3, M4 >::_DNBFunction1M4Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class A1, class Function, class M1, class M2, class M3, class M4 >
_DNBFunction1M4Body< A1, Function, M1, M2, M3, M4 >::_DNBFunction1M4Body(const Function& f, M1 m1, M2 m2, M3 m3, M4 m4)
    DNB_THROW_SPEC_ANY :
    f_(f),
    m1_(m1),
    m2_(m2),
    m3_(m3),
    m4_(m4)
{
    DNB_PRECONDITION( f_ );
}

template< class A1, class Function, class M1, class M2, class M3, class M4 >
_DNBFunction1M4Body< A1, Function, M1, M2, M3, M4 >::_DNBFunction1M4Body(const _DNBFunction1M4Body< A1, Function, M1, M2, M3, M4 >& functionFunctor)
    DNB_THROW_SPEC_ANY :
    f_(functionFunctor.f_),
    m1_(functionFunctor.m1_),
    m2_(functionFunctor.m2_),
    m3_(functionFunctor.m3_),
    m4_(functionFunctor.m4_)
{
    DNB_PRECONDITION( f_ );
}

template< class A1, class Function, class M1, class M2, class M3, class M4 >
_DNBFunction1M4Body< A1, Function, M1, M2, M3, M4 >::~_DNBFunction1M4Body()
    DNB_THROW_SPEC_ANY
{
}

template< class A1, class Function, class M1, class M2, class M3, class M4>
void 
_DNBFunction1M4Body< A1, Function, M1, M2, M3, M4 >::operator()( A1 a1 ) 
    DNB_THROW_SPEC_ANY
{ 
    f_( a1, m1_, m2_, m3_, m4_ );
}

template< class A1, class Function, class M1, class M2, class M3, class M4 >
void*
_DNBFunction1M4Body< A1, Function, M1, M2, M3, M4 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return NULL;
}    
template< class A1, class Function, class M1, class M2, class M3, class M4 >
void*
_DNBFunction1M4Body< A1, Function, M1, M2, M3, M4 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&f_;
}   

template< class A1, class Function, class M1, class M2, class M3, class M4 >
_DNBBaseFunctor1Body< A1 >*
_DNBFunction1M4Body< A1, Function, M1, M2, M3, M4>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBFunction1M4Body< A1, Function, M1, M2, M3, M4 >( *this );
}

template< class A1, class Function, class M1, class M2, class M3, class M4 >
bool 
_DNBFunction1M4Body< A1, Function, M1, M2, M3, M4 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction1M4Body< A1, Function, M1, M2, M3, M4 >* rhsBody = dynamic_cast< _DNBFunction1M4Body< A1, Function, M1, M2, M3, M4 > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
            if ( m1_ == rhsBody->m1_ )
                if ( m2_ == rhsBody->m2_ )
                    if ( m3_ == rhsBody->m3_ )
                        return ( m4_ == rhsBody->m4_ );

    }
    
    return false;
}

template< class A1, class Function, class M1, class M2, class M3, class M4 >
bool 
_DNBFunction1M4Body< A1, Function, M1, M2, M3, M4 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction1M4Body< A1, Function, M1, M2, M3, M4 >* rhsBody = dynamic_cast< _DNBFunction1M4Body< A1, Function, M1, M2, M3, M4 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
        {
            if ( m1_ == rhsBody->m1_ )
            {
                if ( m2_ == rhsBody->m2_ )
                {
                    if ( m3_ == rhsBody->m3_ )
                    {
                        return ( m4_ < rhsBody->m4_ );
                    }
                    else
                        return ( m3_ < rhsBody->m3_ );
                }
                else
                    return ( m2_ < rhsBody->m2_ );
            }
            else
                return ( m1_ < rhsBody->m1_ );

        }
        else
            return ( f_ < rhsBody->f_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class A1, class Client, class Member >
_DNBMemberFunctor1Body< A1, Client, Member >::_DNBMemberFunctor1Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class A1, class Client, class Member >
_DNBMemberFunctor1Body< A1, Client, Member >::_DNBMemberFunctor1Body( const _DNBMemberFunctor1Body< A1, Client, Member >& memberFunctor )
    DNB_THROW_SPEC_ANY
{
    client_ = memberFunctor.client_;
    member_ = memberFunctor.member_;
}        

template< class A1, class Client, class Member >
_DNBMemberFunctor1Body< A1, Client, Member >::_DNBMemberFunctor1Body(Client* client, Member member)
    DNB_THROW_SPEC_ANY:
    client_(client), 
    member_(member)
{
    DNB_PRECONDITION( client_ );
    DNB_PRECONDITION( member_ );
}

template< class A1, class Client, class Member >
_DNBMemberFunctor1Body< A1, Client, Member >::~_DNBMemberFunctor1Body()
    DNB_THROW_SPEC_ANY
{
}

template< class A1, class Client, class Member >
void
_DNBMemberFunctor1Body< A1, Client, Member >::operator()( A1 a1 )
    DNB_THROW_SPEC_ANY
{
    (client_->*member_)( a1 );
}

template< class A1, class Client, class Member >
void *
_DNBMemberFunctor1Body< A1, Client, Member >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return (void *)client_;
}

template< class A1, class Client, class Member >
void *
_DNBMemberFunctor1Body< A1, Client, Member >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&member_;
}

template< class A1, class Client, class Member >
_DNBBaseFunctor1Body< A1 >* 
_DNBMemberFunctor1Body< A1, Client, Member >::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return DNB_NEW _DNBMemberFunctor1Body< A1, Client, Member >( *this );
}

template< class A1, class Client, class Member >
bool 
_DNBMemberFunctor1Body< A1, Client, Member >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor1Body< A1, Client, Member >* rhsBody = dynamic_cast< _DNBMemberFunctor1Body< A1, Client, Member > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        return ( ( client_ == rhsBody->client_ ) && ( member_ == rhsBody->member_ ) );
    }

    return false;
}

template< class A1, class Client, class Member >
bool 
_DNBMemberFunctor1Body< A1, Client, Member >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor1Body< A1, Client, Member >* rhsBody = dynamic_cast< _DNBMemberFunctor1Body< A1, Client, Member > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
            return ( (void *)&member_ < (void *)&(rhsBody->member_) );
        else
            return ( client_ < rhsBody->client_ );
    }
    else
        return ( (void *)this < (void *)&rhs );
}
template< class A1, class Client, class Member, class M1 >
_DNBMemberFunctor1M1Body< A1, Client, Member, M1 >::_DNBMemberFunctor1M1Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class A1, class Client, class Member, class M1 >
_DNBMemberFunctor1M1Body< A1, Client, Member, M1 >::_DNBMemberFunctor1M1Body(const _DNBMemberFunctor1M1Body< A1, Client, Member, M1 >& memberFunctor)
    DNB_THROW_SPEC_ANY: 
m1_(memberFunctor.m1_)
{
   client_ = memberFunctor.client_;
    member_ = memberFunctor.member_;
}

template< class A1, class Client, class Member, class M1 >
_DNBMemberFunctor1M1Body< A1, Client, Member, M1 >::_DNBMemberFunctor1M1Body(Client* client, Member member, M1 m1)
    DNB_THROW_SPEC_ANY:
client_(client), 
    member_(member),
    m1_(m1)
{
    DNB_PRECONDITION( client_ );
    DNB_PRECONDITION( member_ );
}

template< class A1, class Client, class Member, class M1 >
_DNBMemberFunctor1M1Body< A1, Client, Member, M1 >::~_DNBMemberFunctor1M1Body()
    DNB_THROW_SPEC_ANY
{
}

template< class A1, class Client, class Member, class M1>
void 
_DNBMemberFunctor1M1Body< A1, Client, Member, M1 >::operator()( A1 a1 ) 
    DNB_THROW_SPEC_ANY
{ 
    ( client_->*member_ )( a1, m1_ );
}

template< class A1, class Client, class Member, class M1 >
void*
_DNBMemberFunctor1M1Body< A1, Client, Member, M1 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return (void *)client_;
}    
template< class A1, class Client, class Member, class M1 >
void*
_DNBMemberFunctor1M1Body< A1, Client, Member, M1 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&member_;
}   

template< class A1, class Client, class Member, class M1 >
_DNBBaseFunctor1Body< A1 >*
_DNBMemberFunctor1M1Body< A1, Client, Member, M1>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBMemberFunctor1M1Body< A1, Client, Member, M1 >( *this );
}

template< class A1, class Client, class Member, class M1 >
bool 
_DNBMemberFunctor1M1Body< A1, Client, Member, M1 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor1M1Body< A1, Client, Member, M1 >* rhsBody = dynamic_cast< _DNBMemberFunctor1M1Body< A1, Client, Member, M1 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
            if ( member_ == rhsBody->member_ )
                return ( m1_ == rhsBody->m1_ );
    }
    
    return false;
}

template< class A1, class Client, class Member, class M1 >
bool 
_DNBMemberFunctor1M1Body< A1, Client, Member, M1 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor1M1Body< A1, Client, Member, M1 >* rhsBody = dynamic_cast< _DNBMemberFunctor1M1Body< A1, Client, Member, M1 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
        {
            if ( member_ == rhsBody->member_ )
            {
                return ( m1_ < rhsBody->m1_ );
            }
            else
                return ( (void *)&member_ < (void *)&(rhsBody->member_) );
        }
        else
            return ( client_ < rhsBody->client_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class A1, class Client, class Member, class M1, class M2 >
_DNBMemberFunctor1M2Body< A1, Client, Member, M1, M2 >::_DNBMemberFunctor1M2Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class A1, class Client, class Member, class M1, class M2 >
_DNBMemberFunctor1M2Body< A1, Client, Member, M1, M2 >::_DNBMemberFunctor1M2Body(const _DNBMemberFunctor1M2Body< A1, Client, Member, M1, M2 >& memberFunctor)
    DNB_THROW_SPEC_ANY: 
m1_(memberFunctor.m1_),
    m2_(memberFunctor.m2_)
{
   client_ = memberFunctor.client_;
    member_ = memberFunctor.member_;
}

template< class A1, class Client, class Member, class M1, class M2 >
_DNBMemberFunctor1M2Body< A1, Client, Member, M1, M2 >::_DNBMemberFunctor1M2Body(Client* client, Member member, M1 m1, M2 m2)
    DNB_THROW_SPEC_ANY:
client_(client), 
    member_(member),
    m1_(m1),
    m2_(m2)
{
    DNB_PRECONDITION( client_ );
    DNB_PRECONDITION( member_ );
}

template< class A1, class Client, class Member, class M1, class M2 >
_DNBMemberFunctor1M2Body< A1, Client, Member, M1, M2 >::~_DNBMemberFunctor1M2Body()
    DNB_THROW_SPEC_ANY
{
}

template< class A1, class Client, class Member, class M1, class M2>
void 
_DNBMemberFunctor1M2Body< A1, Client, Member, M1, M2 >::operator()( A1 a1 ) 
    DNB_THROW_SPEC_ANY
{ 
    ( client_->*member_ )( a1, m1_, m2_ );
}

template< class A1, class Client, class Member, class M1, class M2 >
void*
_DNBMemberFunctor1M2Body< A1, Client, Member, M1, M2 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return (void *)client_;
}    
template< class A1, class Client, class Member, class M1, class M2 >
void*
_DNBMemberFunctor1M2Body< A1, Client, Member, M1, M2 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&member_;
}   

template< class A1, class Client, class Member, class M1, class M2 >
_DNBBaseFunctor1Body< A1 >*
_DNBMemberFunctor1M2Body< A1, Client, Member, M1, M2>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBMemberFunctor1M2Body< A1, Client, Member, M1, M2 >( *this );
}

template< class A1, class Client, class Member, class M1, class M2 >
bool 
_DNBMemberFunctor1M2Body< A1, Client, Member, M1, M2 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor1M2Body< A1, Client, Member, M1, M2 >* rhsBody = dynamic_cast< _DNBMemberFunctor1M2Body< A1, Client, Member, M1, M2 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
            if ( member_ == rhsBody->member_ )
                if ( m1_ == rhsBody->m1_ )
                    return ( m2_ == rhsBody->m2_ );
    }
    
    return false;
}

template< class A1, class Client, class Member, class M1, class M2 >
bool 
_DNBMemberFunctor1M2Body< A1, Client, Member, M1, M2 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor1M2Body< A1, Client, Member, M1, M2 >* rhsBody = dynamic_cast< _DNBMemberFunctor1M2Body< A1, Client, Member, M1, M2 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
        {
            if ( member_ == rhsBody->member_ )
            {
                if ( m1_ == rhsBody->m1_ )
                {
                    return ( m2_ < rhsBody->m2_ );
                }
                else
                    return ( m1_ < rhsBody->m1_ );
            }
            else
                return ( (void *)&member_ < (void *)&(rhsBody->member_) );
        }
        else
            return ( client_ < rhsBody->client_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class A1, class Client, class Member, class M1, class M2, class M3 >
_DNBMemberFunctor1M3Body< A1, Client, Member, M1, M2, M3 >::_DNBMemberFunctor1M3Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class A1, class Client, class Member, class M1, class M2, class M3 >
_DNBMemberFunctor1M3Body< A1, Client, Member, M1, M2, M3 >::_DNBMemberFunctor1M3Body(const _DNBMemberFunctor1M3Body< A1, Client, Member, M1, M2, M3 >& memberFunctor)
    DNB_THROW_SPEC_ANY: 
m1_(memberFunctor.m1_),
    m2_(memberFunctor.m2_),
    m3_(memberFunctor.m3_)
{
   client_ = memberFunctor.client_;
    member_ = memberFunctor.member_;
}

template< class A1, class Client, class Member, class M1, class M2, class M3 >
_DNBMemberFunctor1M3Body< A1, Client, Member, M1, M2, M3 >::_DNBMemberFunctor1M3Body(Client* client, Member member, M1 m1, M2 m2, M3 m3)
    DNB_THROW_SPEC_ANY:
client_(client), 
    member_(member),
    m1_(m1),
    m2_(m2),
    m3_(m3)
{
    DNB_PRECONDITION( client_ );
    DNB_PRECONDITION( member_ );
}

template< class A1, class Client, class Member, class M1, class M2, class M3 >
_DNBMemberFunctor1M3Body< A1, Client, Member, M1, M2, M3 >::~_DNBMemberFunctor1M3Body()
    DNB_THROW_SPEC_ANY
{
}

template< class A1, class Client, class Member, class M1, class M2, class M3>
void 
_DNBMemberFunctor1M3Body< A1, Client, Member, M1, M2, M3 >::operator()( A1 a1 ) 
    DNB_THROW_SPEC_ANY
{ 
    ( client_->*member_ )( a1, m1_, m2_, m3_ );
}

template< class A1, class Client, class Member, class M1, class M2, class M3 >
void*
_DNBMemberFunctor1M3Body< A1, Client, Member, M1, M2, M3 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return (void *)client_;
}    
template< class A1, class Client, class Member, class M1, class M2, class M3 >
void*
_DNBMemberFunctor1M3Body< A1, Client, Member, M1, M2, M3 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&member_;
}   

template< class A1, class Client, class Member, class M1, class M2, class M3 >
_DNBBaseFunctor1Body< A1 >*
_DNBMemberFunctor1M3Body< A1, Client, Member, M1, M2, M3>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBMemberFunctor1M3Body< A1, Client, Member, M1, M2, M3 >( *this );
}

template< class A1, class Client, class Member, class M1, class M2, class M3 >
bool 
_DNBMemberFunctor1M3Body< A1, Client, Member, M1, M2, M3 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor1M3Body< A1, Client, Member, M1, M2, M3 >* rhsBody = dynamic_cast< _DNBMemberFunctor1M3Body< A1, Client, Member, M1, M2, M3 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
            if ( member_ == rhsBody->member_ )
                if ( m1_ == rhsBody->m1_ )
                    if ( m2_ == rhsBody->m2_ )
                        return ( m3_ == rhsBody->m3_ );
    }
    
    return false;
}

template< class A1, class Client, class Member, class M1, class M2, class M3 >
bool 
_DNBMemberFunctor1M3Body< A1, Client, Member, M1, M2, M3 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor1M3Body< A1, Client, Member, M1, M2, M3 >* rhsBody = dynamic_cast< _DNBMemberFunctor1M3Body< A1, Client, Member, M1, M2, M3 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
        {
            if ( member_ == rhsBody->member_ )
            {
                if ( m1_ == rhsBody->m1_ )
                {
                    if ( m2_ == rhsBody->m2_ )
                    {
                        return ( m3_ < rhsBody->m3_ );
                    }
                    else
                        return ( m2_ < rhsBody->m2_ );
                }
                else
                    return ( m1_ < rhsBody->m1_ );
            }
            else
                return ( (void *)&member_ < (void *)&(rhsBody->member_) );
        }
        else
            return ( client_ < rhsBody->client_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class A1, class Client, class Member, class M1, class M2, class M3, class M4 >
_DNBMemberFunctor1M4Body< A1, Client, Member, M1, M2, M3, M4 >::_DNBMemberFunctor1M4Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class A1, class Client, class Member, class M1, class M2, class M3, class M4 >
_DNBMemberFunctor1M4Body< A1, Client, Member, M1, M2, M3, M4 >::_DNBMemberFunctor1M4Body(const _DNBMemberFunctor1M4Body< A1, Client, Member, M1, M2, M3, M4 >& memberFunctor)
    DNB_THROW_SPEC_ANY: 
m1_(memberFunctor.m1_),
    m2_(memberFunctor.m2_),
    m3_(memberFunctor.m3_),
    m4_(memberFunctor.m4_)
{
   client_ = memberFunctor.client_;
    member_ = memberFunctor.member_;
}

template< class A1, class Client, class Member, class M1, class M2, class M3, class M4 >
_DNBMemberFunctor1M4Body< A1, Client, Member, M1, M2, M3, M4 >::_DNBMemberFunctor1M4Body(Client* client, Member member, M1 m1, M2 m2, M3 m3, M4 m4)
    DNB_THROW_SPEC_ANY:
client_(client), 
    member_(member),
    m1_(m1),
    m2_(m2),
    m3_(m3),
    m4_(m4)
{
    DNB_PRECONDITION( client_ );
    DNB_PRECONDITION( member_ );
}

template< class A1, class Client, class Member, class M1, class M2, class M3, class M4 >
_DNBMemberFunctor1M4Body< A1, Client, Member, M1, M2, M3, M4 >::~_DNBMemberFunctor1M4Body()
    DNB_THROW_SPEC_ANY
{
}

template< class A1, class Client, class Member, class M1, class M2, class M3, class M4>
void 
_DNBMemberFunctor1M4Body< A1, Client, Member, M1, M2, M3, M4 >::operator()( A1 a1 ) 
    DNB_THROW_SPEC_ANY
{ 
    ( client_->*member_ )( a1, m1_, m2_, m3_, m4_ );
}

template< class A1, class Client, class Member, class M1, class M2, class M3, class M4 >
void*
_DNBMemberFunctor1M4Body< A1, Client, Member, M1, M2, M3, M4 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return (void *)client_;
}    
template< class A1, class Client, class Member, class M1, class M2, class M3, class M4 >
void*
_DNBMemberFunctor1M4Body< A1, Client, Member, M1, M2, M3, M4 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&member_;
}   

template< class A1, class Client, class Member, class M1, class M2, class M3, class M4 >
_DNBBaseFunctor1Body< A1 >*
_DNBMemberFunctor1M4Body< A1, Client, Member, M1, M2, M3, M4>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBMemberFunctor1M4Body< A1, Client, Member, M1, M2, M3, M4 >( *this );
}

template< class A1, class Client, class Member, class M1, class M2, class M3, class M4 >
bool 
_DNBMemberFunctor1M4Body< A1, Client, Member, M1, M2, M3, M4 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor1M4Body< A1, Client, Member, M1, M2, M3, M4 >* rhsBody = dynamic_cast< _DNBMemberFunctor1M4Body< A1, Client, Member, M1, M2, M3, M4 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
            if ( member_ == rhsBody->member_ )
                if ( m1_ == rhsBody->m1_ )
                    if ( m2_ == rhsBody->m2_ )
                        if ( m3_ == rhsBody->m3_ )
                            return ( m4_ == rhsBody->m4_ );
    }
    
    return false;
}

template< class A1, class Client, class Member, class M1, class M2, class M3, class M4 >
bool 
_DNBMemberFunctor1M4Body< A1, Client, Member, M1, M2, M3, M4 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor1M4Body< A1, Client, Member, M1, M2, M3, M4 >* rhsBody = dynamic_cast< _DNBMemberFunctor1M4Body< A1, Client, Member, M1, M2, M3, M4 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
        {
            if ( member_ == rhsBody->member_ )
            {
                if ( m1_ == rhsBody->m1_ )
                {
                    if ( m2_ == rhsBody->m2_ )
                    {
                        if ( m3_ == rhsBody->m3_ )
                        {
                            return ( m4_ < rhsBody->m4_ );
                        }
                        else
                            return ( m3_ < rhsBody->m3_ );
                    }
                    else
                        return ( m2_ < rhsBody->m2_ );
                }
                else
                    return ( m1_ < rhsBody->m1_ );
            }
            else
                return ( (void *)&member_ < (void *)&(rhsBody->member_) );
        }
        else
            return ( client_ < rhsBody->client_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class A1 >
DNBFunctor1< A1 >::DNBFunctor1(_DNBBaseFunctor1Body< A1 >* body)
    DNB_THROW_SPEC_ANY: 
    body_(body) 
{
}

template< class A1 >
DNBFunctor1< A1 >::DNBFunctor1(const DNBFunctor1< A1 >& DNBFunctor)
    DNB_THROW_SPEC_ANY
{
    if ( DNBFunctor.body_) 
        body_ = (DNBFunctor.body_)->clone(); 
    else
        body_ = NULL;
}

template< class A1 >
DNBFunctor1< A1 >::~DNBFunctor1() 
    DNB_THROW_SPEC_ANY
{
    if ( body_)
        DNB_DELETE body_;
}

template< class A1 >
_DNBBaseFunctorBody*
DNBFunctor1< A1 >::getBody() const
    DNB_THROW_SPEC_ANY
{
    return (_DNBBaseFunctorBody*)body_;
}

template< class A1 >
void
DNBFunctor1< A1 >::operator()( A1 a1 ) 
    DNB_THROW_SPEC_ANY
{ 
    // full name used instead of operator shorthand for Visual C++
    DNB_PRECONDITION( body_ );
     body_->operator()( a1 ); 
} 

template< class A1 >
DNBFunctor1< A1 >& 
DNBFunctor1< A1 >::operator=(const DNBFunctor1< A1 >& rhs)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    if ( &rhs == this )
        return *this;

    if (body_)
    {
        DNB_DELETE body_;
        body_ = NULL;
    }

    if (rhs.body_)
        body_ = (rhs.body_)->clone();
    else
        body_ = NULL;

    return *this;
}
/////////////////////////////////////////////////////////
//
// 2 Passed,  0 Returned 
//
/////////////////////////////////////////////////////////

template< class A1, class A2, class Function >
_DNBFunction2Body< A1, A2, Function>::_DNBFunction2Body()
    DNB_THROW_SPEC_ANY
{
}


template< class A1, class A2, class Function >
_DNBFunction2Body< A1, A2, Function>::_DNBFunction2Body(const Function& f)
    DNB_THROW_SPEC_ANY:
    f_(f) 
{
    DNB_PRECONDITION( f_ );
}

template< class A1, class A2, class Function >
_DNBFunction2Body< A1, A2, Function>::_DNBFunction2Body(const _DNBFunction2Body< A1, A2, Function>& functionFunctor)
    DNB_THROW_SPEC_ANY:
     f_(functionFunctor.f_)
{
    DNB_PRECONDITION( f_ );
}

template< class A1, class A2, class Function >
_DNBFunction2Body< A1, A2, Function>::~_DNBFunction2Body()    
DNB_THROW_SPEC_ANY
{
}

template< class A1, class A2, class Function >
void
_DNBFunction2Body< A1, A2, Function>::operator()( A1 a1, A2 a2 )
    DNB_THROW_SPEC_ANY
{
    f_(a1, a2 );
}

template< class A1, class A2, class Function >
void*
_DNBFunction2Body< A1, A2, Function>::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return NULL;
}

template< class A1, class A2, class Function >
void*
_DNBFunction2Body< A1, A2, Function>::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&f_;
}   

template< class A1, class A2, class Function >
_DNBBaseFunctor2Body< A1, A2 >*
_DNBFunction2Body< A1, A2, Function>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return DNB_NEW _DNBFunction2Body< A1, A2, Function>(*this);
}

template< class A1, class A2, class Function >
bool 
_DNBFunction2Body< A1, A2, Function >::IsEqual(_DNBBaseFunctorBody& rhs) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction2Body< A1, A2, Function >* rhsBody = dynamic_cast< _DNBFunction2Body< A1, A2, Function > *  > (&rhs);

    if (rhsBody != NULL)
    {
        return( f_ == rhsBody->f_ );
    }
    return false;
}

template< class A1, class A2, class Function >
bool 
_DNBFunction2Body< A1, A2, Function >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction2Body< A1, A2, Function >* rhsBody = dynamic_cast< _DNBFunction2Body< A1, A2, Function > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        return ( f_ < rhsBody->f_ );
    }
    else
        return ( (void *)this < (void *)&rhs );
}

template< class A1, class A2, class Function, class M1 >
_DNBFunction2M1Body< A1, A2, Function, M1 >::_DNBFunction2M1Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class A1, class A2, class Function, class M1 >
_DNBFunction2M1Body< A1, A2, Function, M1 >::_DNBFunction2M1Body(const Function& f, M1 m1)
    DNB_THROW_SPEC_ANY :
    f_(f),
    m1_(m1)
{
    DNB_PRECONDITION( f_ );
}

template< class A1, class A2, class Function, class M1 >
_DNBFunction2M1Body< A1, A2, Function, M1 >::_DNBFunction2M1Body(const _DNBFunction2M1Body< A1, A2, Function, M1 >& functionFunctor)
    DNB_THROW_SPEC_ANY :
    f_(functionFunctor.f_),
    m1_(functionFunctor.m1_)
{
    DNB_PRECONDITION( f_ );
}

template< class A1, class A2, class Function, class M1 >
_DNBFunction2M1Body< A1, A2, Function, M1 >::~_DNBFunction2M1Body()
    DNB_THROW_SPEC_ANY
{
}

template< class A1, class A2, class Function, class M1>
void 
_DNBFunction2M1Body< A1, A2, Function, M1 >::operator()( A1 a1, A2 a2 ) 
    DNB_THROW_SPEC_ANY
{ 
    f_( a1, a2, m1_  );
}

template< class A1, class A2, class Function, class M1 >
void*
_DNBFunction2M1Body< A1, A2, Function, M1 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return NULL;
}    
template< class A1, class A2, class Function, class M1 >
void*
_DNBFunction2M1Body< A1, A2, Function, M1 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&f_;
}   

template< class A1, class A2, class Function, class M1 >
_DNBBaseFunctor2Body< A1, A2 >*
_DNBFunction2M1Body< A1, A2, Function, M1>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBFunction2M1Body< A1, A2, Function, M1 >( *this );
}

template< class A1, class A2, class Function, class M1 >
bool 
_DNBFunction2M1Body< A1, A2, Function, M1 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction2M1Body< A1, A2, Function, M1 >* rhsBody = dynamic_cast< _DNBFunction2M1Body< A1, A2, Function, M1 > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
            return ( m1_ == rhsBody->m1_ );

    }
    
    return false;
}

template< class A1, class A2, class Function, class M1 >
bool 
_DNBFunction2M1Body< A1, A2, Function, M1 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction2M1Body< A1, A2, Function, M1 >* rhsBody = dynamic_cast< _DNBFunction2M1Body< A1, A2, Function, M1 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
        {
            return ( m1_ < rhsBody->m1_ );

        }
        else
            return ( f_ < rhsBody->f_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class A1, class A2, class Function, class M1, class M2 >
_DNBFunction2M2Body< A1, A2, Function, M1, M2 >::_DNBFunction2M2Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class A1, class A2, class Function, class M1, class M2 >
_DNBFunction2M2Body< A1, A2, Function, M1, M2 >::_DNBFunction2M2Body(const Function& f, M1 m1, M2 m2)
    DNB_THROW_SPEC_ANY :
    f_(f),
    m1_(m1),
    m2_(m2)
{
    DNB_PRECONDITION( f_ );
}

template< class A1, class A2, class Function, class M1, class M2 >
_DNBFunction2M2Body< A1, A2, Function, M1, M2 >::_DNBFunction2M2Body(const _DNBFunction2M2Body< A1, A2, Function, M1, M2 >& functionFunctor)
    DNB_THROW_SPEC_ANY :
    f_(functionFunctor.f_),
    m1_(functionFunctor.m1_),
    m2_(functionFunctor.m2_)
{
    DNB_PRECONDITION( f_ );
}

template< class A1, class A2, class Function, class M1, class M2 >
_DNBFunction2M2Body< A1, A2, Function, M1, M2 >::~_DNBFunction2M2Body()
    DNB_THROW_SPEC_ANY
{
}

template< class A1, class A2, class Function, class M1, class M2>
void 
_DNBFunction2M2Body< A1, A2, Function, M1, M2 >::operator()( A1 a1, A2 a2 ) 
    DNB_THROW_SPEC_ANY
{ 
    f_( a1, a2, m1_, m2_  );
}

template< class A1, class A2, class Function, class M1, class M2 >
void*
_DNBFunction2M2Body< A1, A2, Function, M1, M2 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return NULL;
}    
template< class A1, class A2, class Function, class M1, class M2 >
void*
_DNBFunction2M2Body< A1, A2, Function, M1, M2 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&f_;
}   

template< class A1, class A2, class Function, class M1, class M2 >
_DNBBaseFunctor2Body< A1, A2 >*
_DNBFunction2M2Body< A1, A2, Function, M1, M2>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBFunction2M2Body< A1, A2, Function, M1, M2 >( *this );
}

template< class A1, class A2, class Function, class M1, class M2 >
bool 
_DNBFunction2M2Body< A1, A2, Function, M1, M2 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction2M2Body< A1, A2, Function, M1, M2 >* rhsBody = dynamic_cast< _DNBFunction2M2Body< A1, A2, Function, M1, M2 > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
            if ( m1_ == rhsBody->m1_ )
                return ( m2_ == rhsBody->m2_ );

    }
    
    return false;
}

template< class A1, class A2, class Function, class M1, class M2 >
bool 
_DNBFunction2M2Body< A1, A2, Function, M1, M2 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction2M2Body< A1, A2, Function, M1, M2 >* rhsBody = dynamic_cast< _DNBFunction2M2Body< A1, A2, Function, M1, M2 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
        {
            if ( m1_ == rhsBody->m1_ )
            {
                return ( m2_ < rhsBody->m2_ );
            }
            else
                return ( m1_ < rhsBody->m1_ );

        }
        else
            return ( f_ < rhsBody->f_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class A1, class A2, class Function, class M1, class M2, class M3 >
_DNBFunction2M3Body< A1, A2, Function, M1, M2, M3 >::_DNBFunction2M3Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class A1, class A2, class Function, class M1, class M2, class M3 >
_DNBFunction2M3Body< A1, A2, Function, M1, M2, M3 >::_DNBFunction2M3Body(const Function& f, M1 m1, M2 m2, M3 m3)
    DNB_THROW_SPEC_ANY :
    f_(f),
    m1_(m1),
    m2_(m2),
    m3_(m3)
{
    DNB_PRECONDITION( f_ );
}

template< class A1, class A2, class Function, class M1, class M2, class M3 >
_DNBFunction2M3Body< A1, A2, Function, M1, M2, M3 >::_DNBFunction2M3Body(const _DNBFunction2M3Body< A1, A2, Function, M1, M2, M3 >& functionFunctor)
    DNB_THROW_SPEC_ANY :
    f_(functionFunctor.f_),
    m1_(functionFunctor.m1_),
    m2_(functionFunctor.m2_),
    m3_(functionFunctor.m3_)
{
    DNB_PRECONDITION( f_ );
}

template< class A1, class A2, class Function, class M1, class M2, class M3 >
_DNBFunction2M3Body< A1, A2, Function, M1, M2, M3 >::~_DNBFunction2M3Body()
    DNB_THROW_SPEC_ANY
{
}

template< class A1, class A2, class Function, class M1, class M2, class M3>
void 
_DNBFunction2M3Body< A1, A2, Function, M1, M2, M3 >::operator()( A1 a1, A2 a2 ) 
    DNB_THROW_SPEC_ANY
{ 
    f_( a1, a2, m1_, m2_, m3_  );
}

template< class A1, class A2, class Function, class M1, class M2, class M3 >
void*
_DNBFunction2M3Body< A1, A2, Function, M1, M2, M3 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return NULL;
}    
template< class A1, class A2, class Function, class M1, class M2, class M3 >
void*
_DNBFunction2M3Body< A1, A2, Function, M1, M2, M3 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&f_;
}   

template< class A1, class A2, class Function, class M1, class M2, class M3 >
_DNBBaseFunctor2Body< A1, A2 >*
_DNBFunction2M3Body< A1, A2, Function, M1, M2, M3>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBFunction2M3Body< A1, A2, Function, M1, M2, M3 >( *this );
}

template< class A1, class A2, class Function, class M1, class M2, class M3 >
bool 
_DNBFunction2M3Body< A1, A2, Function, M1, M2, M3 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction2M3Body< A1, A2, Function, M1, M2, M3 >* rhsBody = dynamic_cast< _DNBFunction2M3Body< A1, A2, Function, M1, M2, M3 > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
            if ( m1_ == rhsBody->m1_ )
                if ( m2_ == rhsBody->m2_ )
                    return ( m3_ == rhsBody->m3_ );

    }
    
    return false;
}

template< class A1, class A2, class Function, class M1, class M2, class M3 >
bool 
_DNBFunction2M3Body< A1, A2, Function, M1, M2, M3 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction2M3Body< A1, A2, Function, M1, M2, M3 >* rhsBody = dynamic_cast< _DNBFunction2M3Body< A1, A2, Function, M1, M2, M3 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
        {
            if ( m1_ == rhsBody->m1_ )
            {
                if ( m2_ == rhsBody->m2_ )
                {
                    return ( m3_ < rhsBody->m3_ );
                }
                else
                    return ( m2_ < rhsBody->m2_ );
            }
            else
                return ( m1_ < rhsBody->m1_ );

        }
        else
            return ( f_ < rhsBody->f_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class A1, class A2, class Client, class Member >
_DNBMemberFunctor2Body< A1, A2, Client, Member >::_DNBMemberFunctor2Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class A1, class A2, class Client, class Member >
_DNBMemberFunctor2Body< A1, A2, Client, Member >::_DNBMemberFunctor2Body( const _DNBMemberFunctor2Body< A1, A2, Client, Member >& memberFunctor )
    DNB_THROW_SPEC_ANY
{
    client_ = memberFunctor.client_;
    member_ = memberFunctor.member_;
}        

template< class A1, class A2, class Client, class Member >
_DNBMemberFunctor2Body< A1, A2, Client, Member >::_DNBMemberFunctor2Body(Client* client, Member member)
    DNB_THROW_SPEC_ANY:
    client_(client), 
    member_(member)
{
    DNB_PRECONDITION( client_ );
    DNB_PRECONDITION( member_ );
}

template< class A1, class A2, class Client, class Member >
_DNBMemberFunctor2Body< A1, A2, Client, Member >::~_DNBMemberFunctor2Body()
    DNB_THROW_SPEC_ANY
{
}

template< class A1, class A2, class Client, class Member >
void
_DNBMemberFunctor2Body< A1, A2, Client, Member >::operator()( A1 a1, A2 a2 )
    DNB_THROW_SPEC_ANY
{
    (client_->*member_)( a1, a2  );
}

template< class A1, class A2, class Client, class Member >
void *
_DNBMemberFunctor2Body< A1, A2, Client, Member >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return (void *)client_;
}

template< class A1, class A2, class Client, class Member >
void *
_DNBMemberFunctor2Body< A1, A2, Client, Member >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&member_;
}

template< class A1, class A2, class Client, class Member >
_DNBBaseFunctor2Body< A1, A2 >* 
_DNBMemberFunctor2Body< A1, A2, Client, Member >::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return DNB_NEW _DNBMemberFunctor2Body< A1, A2, Client, Member >( *this );
}

template< class A1, class A2, class Client, class Member >
bool 
_DNBMemberFunctor2Body< A1, A2, Client, Member >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor2Body< A1, A2, Client, Member >* rhsBody = dynamic_cast< _DNBMemberFunctor2Body< A1, A2, Client, Member > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        return ( ( client_ == rhsBody->client_ ) && ( member_ == rhsBody->member_ ) );
    }

    return false;
}

template< class A1, class A2, class Client, class Member >
bool 
_DNBMemberFunctor2Body< A1, A2, Client, Member >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor2Body< A1, A2, Client, Member >* rhsBody = dynamic_cast< _DNBMemberFunctor2Body< A1, A2, Client, Member > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
            return ( (void *)&member_ < (void *)&(rhsBody->member_) );
        else
            return ( client_ < rhsBody->client_ );
    }
    else
        return ( (void *)this < (void *)&rhs );
}
template< class A1, class A2, class Client, class Member, class M1 >
_DNBMemberFunctor2M1Body< A1, A2, Client, Member, M1 >::_DNBMemberFunctor2M1Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class A1, class A2, class Client, class Member, class M1 >
_DNBMemberFunctor2M1Body< A1, A2, Client, Member, M1 >::_DNBMemberFunctor2M1Body(const _DNBMemberFunctor2M1Body< A1, A2, Client, Member, M1 >& memberFunctor)
    DNB_THROW_SPEC_ANY: 
m1_(memberFunctor.m1_)
{
   client_ = memberFunctor.client_;
    member_ = memberFunctor.member_;
}

template< class A1, class A2, class Client, class Member, class M1 >
_DNBMemberFunctor2M1Body< A1, A2, Client, Member, M1 >::_DNBMemberFunctor2M1Body(Client* client, Member member, M1 m1)
    DNB_THROW_SPEC_ANY:
client_(client), 
    member_(member),
    m1_(m1)
{
    DNB_PRECONDITION( client_ );
    DNB_PRECONDITION( member_ );
}

template< class A1, class A2, class Client, class Member, class M1 >
_DNBMemberFunctor2M1Body< A1, A2, Client, Member, M1 >::~_DNBMemberFunctor2M1Body()
    DNB_THROW_SPEC_ANY
{
}

template< class A1, class A2, class Client, class Member, class M1>
void 
_DNBMemberFunctor2M1Body< A1, A2, Client, Member, M1 >::operator()( A1 a1, A2 a2 ) 
    DNB_THROW_SPEC_ANY
{ 
    ( client_->*member_ )( a1, a2, m1_  );
}

template< class A1, class A2, class Client, class Member, class M1 >
void*
_DNBMemberFunctor2M1Body< A1, A2, Client, Member, M1 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return (void *)client_;
}    
template< class A1, class A2, class Client, class Member, class M1 >
void*
_DNBMemberFunctor2M1Body< A1, A2, Client, Member, M1 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&member_;
}   

template< class A1, class A2, class Client, class Member, class M1 >
_DNBBaseFunctor2Body< A1, A2 >*
_DNBMemberFunctor2M1Body< A1, A2, Client, Member, M1>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBMemberFunctor2M1Body< A1, A2, Client, Member, M1 >( *this );
}

template< class A1, class A2, class Client, class Member, class M1 >
bool 
_DNBMemberFunctor2M1Body< A1, A2, Client, Member, M1 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor2M1Body< A1, A2, Client, Member, M1 >* rhsBody = dynamic_cast< _DNBMemberFunctor2M1Body< A1, A2, Client, Member, M1 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
            if ( member_ == rhsBody->member_ )
                return ( m1_ == rhsBody->m1_ );
    }
    
    return false;
}

template< class A1, class A2, class Client, class Member, class M1 >
bool 
_DNBMemberFunctor2M1Body< A1, A2, Client, Member, M1 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor2M1Body< A1, A2, Client, Member, M1 >* rhsBody = dynamic_cast< _DNBMemberFunctor2M1Body< A1, A2, Client, Member, M1 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
        {
            if ( member_ == rhsBody->member_ )
            {
                return ( m1_ < rhsBody->m1_ );
            }
            else
                return ( (void *)&member_ < (void *)&(rhsBody->member_) );
        }
        else
            return ( client_ < rhsBody->client_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class A1, class A2, class Client, class Member, class M1, class M2 >
_DNBMemberFunctor2M2Body< A1, A2, Client, Member, M1, M2 >::_DNBMemberFunctor2M2Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class A1, class A2, class Client, class Member, class M1, class M2 >
_DNBMemberFunctor2M2Body< A1, A2, Client, Member, M1, M2 >::_DNBMemberFunctor2M2Body(const _DNBMemberFunctor2M2Body< A1, A2, Client, Member, M1, M2 >& memberFunctor)
    DNB_THROW_SPEC_ANY: 
m1_(memberFunctor.m1_),
    m2_(memberFunctor.m2_)
{
   client_ = memberFunctor.client_;
    member_ = memberFunctor.member_;
}

template< class A1, class A2, class Client, class Member, class M1, class M2 >
_DNBMemberFunctor2M2Body< A1, A2, Client, Member, M1, M2 >::_DNBMemberFunctor2M2Body(Client* client, Member member, M1 m1, M2 m2)
    DNB_THROW_SPEC_ANY:
client_(client), 
    member_(member),
    m1_(m1),
    m2_(m2)
{
    DNB_PRECONDITION( client_ );
    DNB_PRECONDITION( member_ );
}

template< class A1, class A2, class Client, class Member, class M1, class M2 >
_DNBMemberFunctor2M2Body< A1, A2, Client, Member, M1, M2 >::~_DNBMemberFunctor2M2Body()
    DNB_THROW_SPEC_ANY
{
}

template< class A1, class A2, class Client, class Member, class M1, class M2>
void 
_DNBMemberFunctor2M2Body< A1, A2, Client, Member, M1, M2 >::operator()( A1 a1, A2 a2 ) 
    DNB_THROW_SPEC_ANY
{ 
    ( client_->*member_ )( a1, a2, m1_, m2_  );
}

template< class A1, class A2, class Client, class Member, class M1, class M2 >
void*
_DNBMemberFunctor2M2Body< A1, A2, Client, Member, M1, M2 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return (void *)client_;
}    
template< class A1, class A2, class Client, class Member, class M1, class M2 >
void*
_DNBMemberFunctor2M2Body< A1, A2, Client, Member, M1, M2 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&member_;
}   

template< class A1, class A2, class Client, class Member, class M1, class M2 >
_DNBBaseFunctor2Body< A1, A2 >*
_DNBMemberFunctor2M2Body< A1, A2, Client, Member, M1, M2>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBMemberFunctor2M2Body< A1, A2, Client, Member, M1, M2 >( *this );
}

template< class A1, class A2, class Client, class Member, class M1, class M2 >
bool 
_DNBMemberFunctor2M2Body< A1, A2, Client, Member, M1, M2 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor2M2Body< A1, A2, Client, Member, M1, M2 >* rhsBody = dynamic_cast< _DNBMemberFunctor2M2Body< A1, A2, Client, Member, M1, M2 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
            if ( member_ == rhsBody->member_ )
                if ( m1_ == rhsBody->m1_ )
                    return ( m2_ == rhsBody->m2_ );
    }
    
    return false;
}

template< class A1, class A2, class Client, class Member, class M1, class M2 >
bool 
_DNBMemberFunctor2M2Body< A1, A2, Client, Member, M1, M2 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor2M2Body< A1, A2, Client, Member, M1, M2 >* rhsBody = dynamic_cast< _DNBMemberFunctor2M2Body< A1, A2, Client, Member, M1, M2 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
        {
            if ( member_ == rhsBody->member_ )
            {
                if ( m1_ == rhsBody->m1_ )
                {
                    return ( m2_ < rhsBody->m2_ );
                }
                else
                    return ( m1_ < rhsBody->m1_ );
            }
            else
                return ( (void *)&member_ < (void *)&(rhsBody->member_) );
        }
        else
            return ( client_ < rhsBody->client_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class A1, class A2, class Client, class Member, class M1, class M2, class M3 >
_DNBMemberFunctor2M3Body< A1, A2, Client, Member, M1, M2, M3 >::_DNBMemberFunctor2M3Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class A1, class A2, class Client, class Member, class M1, class M2, class M3 >
_DNBMemberFunctor2M3Body< A1, A2, Client, Member, M1, M2, M3 >::_DNBMemberFunctor2M3Body(const _DNBMemberFunctor2M3Body< A1, A2, Client, Member, M1, M2, M3 >& memberFunctor)
    DNB_THROW_SPEC_ANY: 
m1_(memberFunctor.m1_),
    m2_(memberFunctor.m2_),
    m3_(memberFunctor.m3_)
{
   client_ = memberFunctor.client_;
    member_ = memberFunctor.member_;
}

template< class A1, class A2, class Client, class Member, class M1, class M2, class M3 >
_DNBMemberFunctor2M3Body< A1, A2, Client, Member, M1, M2, M3 >::_DNBMemberFunctor2M3Body(Client* client, Member member, M1 m1, M2 m2, M3 m3)
    DNB_THROW_SPEC_ANY:
client_(client), 
    member_(member),
    m1_(m1),
    m2_(m2),
    m3_(m3)
{
    DNB_PRECONDITION( client_ );
    DNB_PRECONDITION( member_ );
}

template< class A1, class A2, class Client, class Member, class M1, class M2, class M3 >
_DNBMemberFunctor2M3Body< A1, A2, Client, Member, M1, M2, M3 >::~_DNBMemberFunctor2M3Body()
    DNB_THROW_SPEC_ANY
{
}

template< class A1, class A2, class Client, class Member, class M1, class M2, class M3>
void 
_DNBMemberFunctor2M3Body< A1, A2, Client, Member, M1, M2, M3 >::operator()( A1 a1, A2 a2 ) 
    DNB_THROW_SPEC_ANY
{ 
    ( client_->*member_ )( a1, a2, m1_, m2_, m3_  );
}

template< class A1, class A2, class Client, class Member, class M1, class M2, class M3 >
void*
_DNBMemberFunctor2M3Body< A1, A2, Client, Member, M1, M2, M3 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return (void *)client_;
}    
template< class A1, class A2, class Client, class Member, class M1, class M2, class M3 >
void*
_DNBMemberFunctor2M3Body< A1, A2, Client, Member, M1, M2, M3 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&member_;
}   

template< class A1, class A2, class Client, class Member, class M1, class M2, class M3 >
_DNBBaseFunctor2Body< A1, A2 >*
_DNBMemberFunctor2M3Body< A1, A2, Client, Member, M1, M2, M3>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBMemberFunctor2M3Body< A1, A2, Client, Member, M1, M2, M3 >( *this );
}

template< class A1, class A2, class Client, class Member, class M1, class M2, class M3 >
bool 
_DNBMemberFunctor2M3Body< A1, A2, Client, Member, M1, M2, M3 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor2M3Body< A1, A2, Client, Member, M1, M2, M3 >* rhsBody = dynamic_cast< _DNBMemberFunctor2M3Body< A1, A2, Client, Member, M1, M2, M3 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
            if ( member_ == rhsBody->member_ )
                if ( m1_ == rhsBody->m1_ )
                    if ( m2_ == rhsBody->m2_ )
                        return ( m3_ == rhsBody->m3_ );
    }
    
    return false;
}

template< class A1, class A2, class Client, class Member, class M1, class M2, class M3 >
bool 
_DNBMemberFunctor2M3Body< A1, A2, Client, Member, M1, M2, M3 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor2M3Body< A1, A2, Client, Member, M1, M2, M3 >* rhsBody = dynamic_cast< _DNBMemberFunctor2M3Body< A1, A2, Client, Member, M1, M2, M3 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
        {
            if ( member_ == rhsBody->member_ )
            {
                if ( m1_ == rhsBody->m1_ )
                {
                    if ( m2_ == rhsBody->m2_ )
                    {
                        return ( m3_ < rhsBody->m3_ );
                    }
                    else
                        return ( m2_ < rhsBody->m2_ );
                }
                else
                    return ( m1_ < rhsBody->m1_ );
            }
            else
                return ( (void *)&member_ < (void *)&(rhsBody->member_) );
        }
        else
            return ( client_ < rhsBody->client_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class A1, class A2 >
DNBFunctor2< A1, A2 >::DNBFunctor2(_DNBBaseFunctor2Body< A1, A2 >* body)
    DNB_THROW_SPEC_ANY: 
    body_(body) 
{
}

template< class A1, class A2 >
DNBFunctor2< A1, A2 >::DNBFunctor2(const DNBFunctor2< A1, A2 >& DNBFunctor)
    DNB_THROW_SPEC_ANY
{
    if ( DNBFunctor.body_) 
        body_ = (DNBFunctor.body_)->clone(); 
    else
        body_ = NULL;
}

template< class A1, class A2 >
DNBFunctor2< A1, A2 >::~DNBFunctor2() 
    DNB_THROW_SPEC_ANY
{
    if ( body_)
        DNB_DELETE body_;
}

template< class A1, class A2 >
_DNBBaseFunctorBody*
DNBFunctor2< A1, A2 >::getBody() const
    DNB_THROW_SPEC_ANY
{
    return (_DNBBaseFunctorBody*)body_;
}

template< class A1, class A2 >
void
DNBFunctor2< A1, A2 >::operator()( A1 a1, A2 a2 ) 
    DNB_THROW_SPEC_ANY
{ 
    // full name used instead of operator shorthand for Visual C++
    DNB_PRECONDITION( body_ );
     body_->operator()( a1, a2  ); 
} 

template< class A1, class A2 >
DNBFunctor2< A1, A2 >& 
DNBFunctor2< A1, A2 >::operator=(const DNBFunctor2< A1, A2 >& rhs)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    if ( &rhs == this )
        return *this;

    if (body_)
    {
        DNB_DELETE body_;
        body_ = NULL;
    }

    if (rhs.body_)
        body_ = (rhs.body_)->clone();
    else
        body_ = NULL;

    return *this;
}
/////////////////////////////////////////////////////////
//
// 3 Passed,  0 Returned 
//
/////////////////////////////////////////////////////////

template< class A1, class A2, class A3, class Function >
_DNBFunction3Body< A1, A2, A3, Function>::_DNBFunction3Body()
    DNB_THROW_SPEC_ANY
{
}


template< class A1, class A2, class A3, class Function >
_DNBFunction3Body< A1, A2, A3, Function>::_DNBFunction3Body(const Function& f)
    DNB_THROW_SPEC_ANY:
    f_(f) 
{
    DNB_PRECONDITION( f_ );
}

template< class A1, class A2, class A3, class Function >
_DNBFunction3Body< A1, A2, A3, Function>::_DNBFunction3Body(const _DNBFunction3Body< A1, A2, A3, Function>& functionFunctor)
    DNB_THROW_SPEC_ANY:
     f_(functionFunctor.f_)
{
    DNB_PRECONDITION( f_ );
}

template< class A1, class A2, class A3, class Function >
_DNBFunction3Body< A1, A2, A3, Function>::~_DNBFunction3Body()    
DNB_THROW_SPEC_ANY
{
}

template< class A1, class A2, class A3, class Function >
void
_DNBFunction3Body< A1, A2, A3, Function>::operator()( A1 a1, A2 a2, A3 a3 )
    DNB_THROW_SPEC_ANY
{
    f_(a1, a2 , a3 );
}

template< class A1, class A2, class A3, class Function >
void*
_DNBFunction3Body< A1, A2, A3, Function>::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return NULL;
}

template< class A1, class A2, class A3, class Function >
void*
_DNBFunction3Body< A1, A2, A3, Function>::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&f_;
}   

template< class A1, class A2, class A3, class Function >
_DNBBaseFunctor3Body< A1, A2, A3 >*
_DNBFunction3Body< A1, A2, A3, Function>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return DNB_NEW _DNBFunction3Body< A1, A2, A3, Function>(*this);
}

template< class A1, class A2, class A3, class Function >
bool 
_DNBFunction3Body< A1, A2, A3, Function >::IsEqual(_DNBBaseFunctorBody& rhs) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction3Body< A1, A2, A3, Function >* rhsBody = dynamic_cast< _DNBFunction3Body< A1, A2, A3, Function > *  > (&rhs);

    if (rhsBody != NULL)
    {
        return( f_ == rhsBody->f_ );
    }
    return false;
}

template< class A1, class A2, class A3, class Function >
bool 
_DNBFunction3Body< A1, A2, A3, Function >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction3Body< A1, A2, A3, Function >* rhsBody = dynamic_cast< _DNBFunction3Body< A1, A2, A3, Function > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        return ( f_ < rhsBody->f_ );
    }
    else
        return ( (void *)this < (void *)&rhs );
}

template< class A1, class A2, class A3, class Function, class M1 >
_DNBFunction3M1Body< A1, A2, A3, Function, M1 >::_DNBFunction3M1Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class A1, class A2, class A3, class Function, class M1 >
_DNBFunction3M1Body< A1, A2, A3, Function, M1 >::_DNBFunction3M1Body(const Function& f, M1 m1)
    DNB_THROW_SPEC_ANY :
    f_(f),
    m1_(m1)
{
    DNB_PRECONDITION( f_ );
}

template< class A1, class A2, class A3, class Function, class M1 >
_DNBFunction3M1Body< A1, A2, A3, Function, M1 >::_DNBFunction3M1Body(const _DNBFunction3M1Body< A1, A2, A3, Function, M1 >& functionFunctor)
    DNB_THROW_SPEC_ANY :
    f_(functionFunctor.f_),
    m1_(functionFunctor.m1_)
{
    DNB_PRECONDITION( f_ );
}

template< class A1, class A2, class A3, class Function, class M1 >
_DNBFunction3M1Body< A1, A2, A3, Function, M1 >::~_DNBFunction3M1Body()
    DNB_THROW_SPEC_ANY
{
}

template< class A1, class A2, class A3, class Function, class M1>
void 
_DNBFunction3M1Body< A1, A2, A3, Function, M1 >::operator()( A1 a1, A2 a2, A3 a3 ) 
    DNB_THROW_SPEC_ANY
{ 
    f_( a1, a2 , a3, m1_  );
}

template< class A1, class A2, class A3, class Function, class M1 >
void*
_DNBFunction3M1Body< A1, A2, A3, Function, M1 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return NULL;
}    
template< class A1, class A2, class A3, class Function, class M1 >
void*
_DNBFunction3M1Body< A1, A2, A3, Function, M1 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&f_;
}   

template< class A1, class A2, class A3, class Function, class M1 >
_DNBBaseFunctor3Body< A1, A2, A3 >*
_DNBFunction3M1Body< A1, A2, A3, Function, M1>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBFunction3M1Body< A1, A2, A3, Function, M1 >( *this );
}

template< class A1, class A2, class A3, class Function, class M1 >
bool 
_DNBFunction3M1Body< A1, A2, A3, Function, M1 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction3M1Body< A1, A2, A3, Function, M1 >* rhsBody = dynamic_cast< _DNBFunction3M1Body< A1, A2, A3, Function, M1 > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
            return ( m1_ == rhsBody->m1_ );

    }
    
    return false;
}

template< class A1, class A2, class A3, class Function, class M1 >
bool 
_DNBFunction3M1Body< A1, A2, A3, Function, M1 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction3M1Body< A1, A2, A3, Function, M1 >* rhsBody = dynamic_cast< _DNBFunction3M1Body< A1, A2, A3, Function, M1 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
        {
            return ( m1_ < rhsBody->m1_ );

        }
        else
            return ( f_ < rhsBody->f_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class A1, class A2, class A3, class Function, class M1, class M2 >
_DNBFunction3M2Body< A1, A2, A3, Function, M1, M2 >::_DNBFunction3M2Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class A1, class A2, class A3, class Function, class M1, class M2 >
_DNBFunction3M2Body< A1, A2, A3, Function, M1, M2 >::_DNBFunction3M2Body(const Function& f, M1 m1, M2 m2)
    DNB_THROW_SPEC_ANY :
    f_(f),
    m1_(m1),
    m2_(m2)
{
    DNB_PRECONDITION( f_ );
}

template< class A1, class A2, class A3, class Function, class M1, class M2 >
_DNBFunction3M2Body< A1, A2, A3, Function, M1, M2 >::_DNBFunction3M2Body(const _DNBFunction3M2Body< A1, A2, A3, Function, M1, M2 >& functionFunctor)
    DNB_THROW_SPEC_ANY :
    f_(functionFunctor.f_),
    m1_(functionFunctor.m1_),
    m2_(functionFunctor.m2_)
{
    DNB_PRECONDITION( f_ );
}

template< class A1, class A2, class A3, class Function, class M1, class M2 >
_DNBFunction3M2Body< A1, A2, A3, Function, M1, M2 >::~_DNBFunction3M2Body()
    DNB_THROW_SPEC_ANY
{
}

template< class A1, class A2, class A3, class Function, class M1, class M2>
void 
_DNBFunction3M2Body< A1, A2, A3, Function, M1, M2 >::operator()( A1 a1, A2 a2, A3 a3 ) 
    DNB_THROW_SPEC_ANY
{ 
    f_( a1, a2 , a3, m1_, m2_  );
}

template< class A1, class A2, class A3, class Function, class M1, class M2 >
void*
_DNBFunction3M2Body< A1, A2, A3, Function, M1, M2 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return NULL;
}    
template< class A1, class A2, class A3, class Function, class M1, class M2 >
void*
_DNBFunction3M2Body< A1, A2, A3, Function, M1, M2 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&f_;
}   

template< class A1, class A2, class A3, class Function, class M1, class M2 >
_DNBBaseFunctor3Body< A1, A2, A3 >*
_DNBFunction3M2Body< A1, A2, A3, Function, M1, M2>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBFunction3M2Body< A1, A2, A3, Function, M1, M2 >( *this );
}

template< class A1, class A2, class A3, class Function, class M1, class M2 >
bool 
_DNBFunction3M2Body< A1, A2, A3, Function, M1, M2 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction3M2Body< A1, A2, A3, Function, M1, M2 >* rhsBody = dynamic_cast< _DNBFunction3M2Body< A1, A2, A3, Function, M1, M2 > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
            if ( m1_ == rhsBody->m1_ )
                return ( m2_ == rhsBody->m2_ );

    }
    
    return false;
}

template< class A1, class A2, class A3, class Function, class M1, class M2 >
bool 
_DNBFunction3M2Body< A1, A2, A3, Function, M1, M2 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction3M2Body< A1, A2, A3, Function, M1, M2 >* rhsBody = dynamic_cast< _DNBFunction3M2Body< A1, A2, A3, Function, M1, M2 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
        {
            if ( m1_ == rhsBody->m1_ )
            {
                return ( m2_ < rhsBody->m2_ );
            }
            else
                return ( m1_ < rhsBody->m1_ );

        }
        else
            return ( f_ < rhsBody->f_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class A1, class A2, class A3, class Client, class Member >
_DNBMemberFunctor3Body< A1, A2, A3, Client, Member >::_DNBMemberFunctor3Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class A1, class A2, class A3, class Client, class Member >
_DNBMemberFunctor3Body< A1, A2, A3, Client, Member >::_DNBMemberFunctor3Body( const _DNBMemberFunctor3Body< A1, A2, A3, Client, Member >& memberFunctor )
    DNB_THROW_SPEC_ANY
{
    client_ = memberFunctor.client_;
    member_ = memberFunctor.member_;
}        

template< class A1, class A2, class A3, class Client, class Member >
_DNBMemberFunctor3Body< A1, A2, A3, Client, Member >::_DNBMemberFunctor3Body(Client* client, Member member)
    DNB_THROW_SPEC_ANY:
    client_(client), 
    member_(member)
{
    DNB_PRECONDITION( client_ );
    DNB_PRECONDITION( member_ );
}

template< class A1, class A2, class A3, class Client, class Member >
_DNBMemberFunctor3Body< A1, A2, A3, Client, Member >::~_DNBMemberFunctor3Body()
    DNB_THROW_SPEC_ANY
{
}

template< class A1, class A2, class A3, class Client, class Member >
void
_DNBMemberFunctor3Body< A1, A2, A3, Client, Member >::operator()( A1 a1, A2 a2, A3 a3 )
    DNB_THROW_SPEC_ANY
{
    (client_->*member_)( a1, a2 , a3  );
}

template< class A1, class A2, class A3, class Client, class Member >
void *
_DNBMemberFunctor3Body< A1, A2, A3, Client, Member >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return (void *)client_;
}

template< class A1, class A2, class A3, class Client, class Member >
void *
_DNBMemberFunctor3Body< A1, A2, A3, Client, Member >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&member_;
}

template< class A1, class A2, class A3, class Client, class Member >
_DNBBaseFunctor3Body< A1, A2, A3 >* 
_DNBMemberFunctor3Body< A1, A2, A3, Client, Member >::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return DNB_NEW _DNBMemberFunctor3Body< A1, A2, A3, Client, Member >( *this );
}

template< class A1, class A2, class A3, class Client, class Member >
bool 
_DNBMemberFunctor3Body< A1, A2, A3, Client, Member >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor3Body< A1, A2, A3, Client, Member >* rhsBody = dynamic_cast< _DNBMemberFunctor3Body< A1, A2, A3, Client, Member > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        return ( ( client_ == rhsBody->client_ ) && ( member_ == rhsBody->member_ ) );
    }

    return false;
}

template< class A1, class A2, class A3, class Client, class Member >
bool 
_DNBMemberFunctor3Body< A1, A2, A3, Client, Member >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor3Body< A1, A2, A3, Client, Member >* rhsBody = dynamic_cast< _DNBMemberFunctor3Body< A1, A2, A3, Client, Member > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
            return ( (void *)&member_ < (void *)&(rhsBody->member_) );
        else
            return ( client_ < rhsBody->client_ );
    }
    else
        return ( (void *)this < (void *)&rhs );
}
template< class A1, class A2, class A3, class Client, class Member, class M1 >
_DNBMemberFunctor3M1Body< A1, A2, A3, Client, Member, M1 >::_DNBMemberFunctor3M1Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class A1, class A2, class A3, class Client, class Member, class M1 >
_DNBMemberFunctor3M1Body< A1, A2, A3, Client, Member, M1 >::_DNBMemberFunctor3M1Body(const _DNBMemberFunctor3M1Body< A1, A2, A3, Client, Member, M1 >& memberFunctor)
    DNB_THROW_SPEC_ANY: 
m1_(memberFunctor.m1_)
{
   client_ = memberFunctor.client_;
    member_ = memberFunctor.member_;
}

template< class A1, class A2, class A3, class Client, class Member, class M1 >
_DNBMemberFunctor3M1Body< A1, A2, A3, Client, Member, M1 >::_DNBMemberFunctor3M1Body(Client* client, Member member, M1 m1)
    DNB_THROW_SPEC_ANY:
client_(client), 
    member_(member),
    m1_(m1)
{
    DNB_PRECONDITION( client_ );
    DNB_PRECONDITION( member_ );
}

template< class A1, class A2, class A3, class Client, class Member, class M1 >
_DNBMemberFunctor3M1Body< A1, A2, A3, Client, Member, M1 >::~_DNBMemberFunctor3M1Body()
    DNB_THROW_SPEC_ANY
{
}

template< class A1, class A2, class A3, class Client, class Member, class M1>
void 
_DNBMemberFunctor3M1Body< A1, A2, A3, Client, Member, M1 >::operator()( A1 a1, A2 a2, A3 a3 ) 
    DNB_THROW_SPEC_ANY
{ 
    ( client_->*member_ )( a1, a2 , a3, m1_  );
}

template< class A1, class A2, class A3, class Client, class Member, class M1 >
void*
_DNBMemberFunctor3M1Body< A1, A2, A3, Client, Member, M1 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return (void *)client_;
}    
template< class A1, class A2, class A3, class Client, class Member, class M1 >
void*
_DNBMemberFunctor3M1Body< A1, A2, A3, Client, Member, M1 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&member_;
}   

template< class A1, class A2, class A3, class Client, class Member, class M1 >
_DNBBaseFunctor3Body< A1, A2, A3 >*
_DNBMemberFunctor3M1Body< A1, A2, A3, Client, Member, M1>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBMemberFunctor3M1Body< A1, A2, A3, Client, Member, M1 >( *this );
}

template< class A1, class A2, class A3, class Client, class Member, class M1 >
bool 
_DNBMemberFunctor3M1Body< A1, A2, A3, Client, Member, M1 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor3M1Body< A1, A2, A3, Client, Member, M1 >* rhsBody = dynamic_cast< _DNBMemberFunctor3M1Body< A1, A2, A3, Client, Member, M1 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
            if ( member_ == rhsBody->member_ )
                return ( m1_ == rhsBody->m1_ );
    }
    
    return false;
}

template< class A1, class A2, class A3, class Client, class Member, class M1 >
bool 
_DNBMemberFunctor3M1Body< A1, A2, A3, Client, Member, M1 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor3M1Body< A1, A2, A3, Client, Member, M1 >* rhsBody = dynamic_cast< _DNBMemberFunctor3M1Body< A1, A2, A3, Client, Member, M1 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
        {
            if ( member_ == rhsBody->member_ )
            {
                return ( m1_ < rhsBody->m1_ );
            }
            else
                return ( (void *)&member_ < (void *)&(rhsBody->member_) );
        }
        else
            return ( client_ < rhsBody->client_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class A1, class A2, class A3, class Client, class Member, class M1, class M2 >
_DNBMemberFunctor3M2Body< A1, A2, A3, Client, Member, M1, M2 >::_DNBMemberFunctor3M2Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class A1, class A2, class A3, class Client, class Member, class M1, class M2 >
_DNBMemberFunctor3M2Body< A1, A2, A3, Client, Member, M1, M2 >::_DNBMemberFunctor3M2Body(const _DNBMemberFunctor3M2Body< A1, A2, A3, Client, Member, M1, M2 >& memberFunctor)
    DNB_THROW_SPEC_ANY: 
m1_(memberFunctor.m1_),
    m2_(memberFunctor.m2_)
{
   client_ = memberFunctor.client_;
    member_ = memberFunctor.member_;
}

template< class A1, class A2, class A3, class Client, class Member, class M1, class M2 >
_DNBMemberFunctor3M2Body< A1, A2, A3, Client, Member, M1, M2 >::_DNBMemberFunctor3M2Body(Client* client, Member member, M1 m1, M2 m2)
    DNB_THROW_SPEC_ANY:
client_(client), 
    member_(member),
    m1_(m1),
    m2_(m2)
{
    DNB_PRECONDITION( client_ );
    DNB_PRECONDITION( member_ );
}

template< class A1, class A2, class A3, class Client, class Member, class M1, class M2 >
_DNBMemberFunctor3M2Body< A1, A2, A3, Client, Member, M1, M2 >::~_DNBMemberFunctor3M2Body()
    DNB_THROW_SPEC_ANY
{
}

template< class A1, class A2, class A3, class Client, class Member, class M1, class M2>
void 
_DNBMemberFunctor3M2Body< A1, A2, A3, Client, Member, M1, M2 >::operator()( A1 a1, A2 a2, A3 a3 ) 
    DNB_THROW_SPEC_ANY
{ 
    ( client_->*member_ )( a1, a2 , a3, m1_, m2_  );
}

template< class A1, class A2, class A3, class Client, class Member, class M1, class M2 >
void*
_DNBMemberFunctor3M2Body< A1, A2, A3, Client, Member, M1, M2 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return (void *)client_;
}    
template< class A1, class A2, class A3, class Client, class Member, class M1, class M2 >
void*
_DNBMemberFunctor3M2Body< A1, A2, A3, Client, Member, M1, M2 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&member_;
}   

template< class A1, class A2, class A3, class Client, class Member, class M1, class M2 >
_DNBBaseFunctor3Body< A1, A2, A3 >*
_DNBMemberFunctor3M2Body< A1, A2, A3, Client, Member, M1, M2>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBMemberFunctor3M2Body< A1, A2, A3, Client, Member, M1, M2 >( *this );
}

template< class A1, class A2, class A3, class Client, class Member, class M1, class M2 >
bool 
_DNBMemberFunctor3M2Body< A1, A2, A3, Client, Member, M1, M2 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor3M2Body< A1, A2, A3, Client, Member, M1, M2 >* rhsBody = dynamic_cast< _DNBMemberFunctor3M2Body< A1, A2, A3, Client, Member, M1, M2 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
            if ( member_ == rhsBody->member_ )
                if ( m1_ == rhsBody->m1_ )
                    return ( m2_ == rhsBody->m2_ );
    }
    
    return false;
}

template< class A1, class A2, class A3, class Client, class Member, class M1, class M2 >
bool 
_DNBMemberFunctor3M2Body< A1, A2, A3, Client, Member, M1, M2 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor3M2Body< A1, A2, A3, Client, Member, M1, M2 >* rhsBody = dynamic_cast< _DNBMemberFunctor3M2Body< A1, A2, A3, Client, Member, M1, M2 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
        {
            if ( member_ == rhsBody->member_ )
            {
                if ( m1_ == rhsBody->m1_ )
                {
                    return ( m2_ < rhsBody->m2_ );
                }
                else
                    return ( m1_ < rhsBody->m1_ );
            }
            else
                return ( (void *)&member_ < (void *)&(rhsBody->member_) );
        }
        else
            return ( client_ < rhsBody->client_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class A1, class A2, class A3 >
DNBFunctor3< A1, A2, A3 >::DNBFunctor3(_DNBBaseFunctor3Body< A1, A2, A3 >* body)
    DNB_THROW_SPEC_ANY: 
    body_(body) 
{
}

template< class A1, class A2, class A3 >
DNBFunctor3< A1, A2, A3 >::DNBFunctor3(const DNBFunctor3< A1, A2, A3 >& DNBFunctor)
    DNB_THROW_SPEC_ANY
{
    if ( DNBFunctor.body_) 
        body_ = (DNBFunctor.body_)->clone(); 
    else
        body_ = NULL;
}

template< class A1, class A2, class A3 >
DNBFunctor3< A1, A2, A3 >::~DNBFunctor3() 
    DNB_THROW_SPEC_ANY
{
    if ( body_)
        DNB_DELETE body_;
}

template< class A1, class A2, class A3 >
_DNBBaseFunctorBody*
DNBFunctor3< A1, A2, A3 >::getBody() const
    DNB_THROW_SPEC_ANY
{
    return (_DNBBaseFunctorBody*)body_;
}

template< class A1, class A2, class A3 >
void
DNBFunctor3< A1, A2, A3 >::operator()( A1 a1, A2 a2, A3 a3 ) 
    DNB_THROW_SPEC_ANY
{ 
    // full name used instead of operator shorthand for Visual C++
    DNB_PRECONDITION( body_ );
     body_->operator()( a1, a2 , a3  ); 
} 

template< class A1, class A2, class A3 >
DNBFunctor3< A1, A2, A3 >& 
DNBFunctor3< A1, A2, A3 >::operator=(const DNBFunctor3< A1, A2, A3 >& rhs)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    if ( &rhs == this )
        return *this;

    if (body_)
    {
        DNB_DELETE body_;
        body_ = NULL;
    }

    if (rhs.body_)
        body_ = (rhs.body_)->clone();
    else
        body_ = NULL;

    return *this;
}
/////////////////////////////////////////////////////////
//
// 4 Passed,  0 Returned 
//
/////////////////////////////////////////////////////////

template< class A1, class A2, class A3, class A4, class Function >
_DNBFunction4Body< A1, A2, A3, A4, Function>::_DNBFunction4Body()
    DNB_THROW_SPEC_ANY
{
}


template< class A1, class A2, class A3, class A4, class Function >
_DNBFunction4Body< A1, A2, A3, A4, Function>::_DNBFunction4Body(const Function& f)
    DNB_THROW_SPEC_ANY:
    f_(f) 
{
    DNB_PRECONDITION( f_ );
}

template< class A1, class A2, class A3, class A4, class Function >
_DNBFunction4Body< A1, A2, A3, A4, Function>::_DNBFunction4Body(const _DNBFunction4Body< A1, A2, A3, A4, Function>& functionFunctor)
    DNB_THROW_SPEC_ANY:
     f_(functionFunctor.f_)
{
    DNB_PRECONDITION( f_ );
}

template< class A1, class A2, class A3, class A4, class Function >
_DNBFunction4Body< A1, A2, A3, A4, Function>::~_DNBFunction4Body()    
DNB_THROW_SPEC_ANY
{
}

template< class A1, class A2, class A3, class A4, class Function >
void
_DNBFunction4Body< A1, A2, A3, A4, Function>::operator()( A1 a1, A2 a2, A3 a3, A4 a4 )
    DNB_THROW_SPEC_ANY
{
    f_(a1, a2 , a3 , a4 );
}

template< class A1, class A2, class A3, class A4, class Function >
void*
_DNBFunction4Body< A1, A2, A3, A4, Function>::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return NULL;
}

template< class A1, class A2, class A3, class A4, class Function >
void*
_DNBFunction4Body< A1, A2, A3, A4, Function>::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&f_;
}   

template< class A1, class A2, class A3, class A4, class Function >
_DNBBaseFunctor4Body< A1, A2, A3, A4 >*
_DNBFunction4Body< A1, A2, A3, A4, Function>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return DNB_NEW _DNBFunction4Body< A1, A2, A3, A4, Function>(*this);
}

template< class A1, class A2, class A3, class A4, class Function >
bool 
_DNBFunction4Body< A1, A2, A3, A4, Function >::IsEqual(_DNBBaseFunctorBody& rhs) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction4Body< A1, A2, A3, A4, Function >* rhsBody = dynamic_cast< _DNBFunction4Body< A1, A2, A3, A4, Function > *  > (&rhs);

    if (rhsBody != NULL)
    {
        return( f_ == rhsBody->f_ );
    }
    return false;
}

template< class A1, class A2, class A3, class A4, class Function >
bool 
_DNBFunction4Body< A1, A2, A3, A4, Function >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction4Body< A1, A2, A3, A4, Function >* rhsBody = dynamic_cast< _DNBFunction4Body< A1, A2, A3, A4, Function > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        return ( f_ < rhsBody->f_ );
    }
    else
        return ( (void *)this < (void *)&rhs );
}

template< class A1, class A2, class A3, class A4, class Function, class M1 >
_DNBFunction4M1Body< A1, A2, A3, A4, Function, M1 >::_DNBFunction4M1Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class A1, class A2, class A3, class A4, class Function, class M1 >
_DNBFunction4M1Body< A1, A2, A3, A4, Function, M1 >::_DNBFunction4M1Body(const Function& f, M1 m1)
    DNB_THROW_SPEC_ANY :
    f_(f),
    m1_(m1)
{
    DNB_PRECONDITION( f_ );
}

template< class A1, class A2, class A3, class A4, class Function, class M1 >
_DNBFunction4M1Body< A1, A2, A3, A4, Function, M1 >::_DNBFunction4M1Body(const _DNBFunction4M1Body< A1, A2, A3, A4, Function, M1 >& functionFunctor)
    DNB_THROW_SPEC_ANY :
    f_(functionFunctor.f_),
    m1_(functionFunctor.m1_)
{
    DNB_PRECONDITION( f_ );
}

template< class A1, class A2, class A3, class A4, class Function, class M1 >
_DNBFunction4M1Body< A1, A2, A3, A4, Function, M1 >::~_DNBFunction4M1Body()
    DNB_THROW_SPEC_ANY
{
}

template< class A1, class A2, class A3, class A4, class Function, class M1>
void 
_DNBFunction4M1Body< A1, A2, A3, A4, Function, M1 >::operator()( A1 a1, A2 a2, A3 a3, A4 a4 ) 
    DNB_THROW_SPEC_ANY
{ 
    f_( a1, a2 , a3 , a4, m1_  );
}

template< class A1, class A2, class A3, class A4, class Function, class M1 >
void*
_DNBFunction4M1Body< A1, A2, A3, A4, Function, M1 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return NULL;
}    
template< class A1, class A2, class A3, class A4, class Function, class M1 >
void*
_DNBFunction4M1Body< A1, A2, A3, A4, Function, M1 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&f_;
}   

template< class A1, class A2, class A3, class A4, class Function, class M1 >
_DNBBaseFunctor4Body< A1, A2, A3, A4 >*
_DNBFunction4M1Body< A1, A2, A3, A4, Function, M1>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBFunction4M1Body< A1, A2, A3, A4, Function, M1 >( *this );
}

template< class A1, class A2, class A3, class A4, class Function, class M1 >
bool 
_DNBFunction4M1Body< A1, A2, A3, A4, Function, M1 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction4M1Body< A1, A2, A3, A4, Function, M1 >* rhsBody = dynamic_cast< _DNBFunction4M1Body< A1, A2, A3, A4, Function, M1 > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
            return ( m1_ == rhsBody->m1_ );

    }
    
    return false;
}

template< class A1, class A2, class A3, class A4, class Function, class M1 >
bool 
_DNBFunction4M1Body< A1, A2, A3, A4, Function, M1 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction4M1Body< A1, A2, A3, A4, Function, M1 >* rhsBody = dynamic_cast< _DNBFunction4M1Body< A1, A2, A3, A4, Function, M1 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
        {
            return ( m1_ < rhsBody->m1_ );

        }
        else
            return ( f_ < rhsBody->f_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class A1, class A2, class A3, class A4, class Client, class Member >
_DNBMemberFunctor4Body< A1, A2, A3, A4, Client, Member >::_DNBMemberFunctor4Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class A1, class A2, class A3, class A4, class Client, class Member >
_DNBMemberFunctor4Body< A1, A2, A3, A4, Client, Member >::_DNBMemberFunctor4Body( const _DNBMemberFunctor4Body< A1, A2, A3, A4, Client, Member >& memberFunctor )
    DNB_THROW_SPEC_ANY
{
    client_ = memberFunctor.client_;
    member_ = memberFunctor.member_;
}        

template< class A1, class A2, class A3, class A4, class Client, class Member >
_DNBMemberFunctor4Body< A1, A2, A3, A4, Client, Member >::_DNBMemberFunctor4Body(Client* client, Member member)
    DNB_THROW_SPEC_ANY:
    client_(client), 
    member_(member)
{
    DNB_PRECONDITION( client_ );
    DNB_PRECONDITION( member_ );
}

template< class A1, class A2, class A3, class A4, class Client, class Member >
_DNBMemberFunctor4Body< A1, A2, A3, A4, Client, Member >::~_DNBMemberFunctor4Body()
    DNB_THROW_SPEC_ANY
{
}

template< class A1, class A2, class A3, class A4, class Client, class Member >
void
_DNBMemberFunctor4Body< A1, A2, A3, A4, Client, Member >::operator()( A1 a1, A2 a2, A3 a3, A4 a4 )
    DNB_THROW_SPEC_ANY
{
    (client_->*member_)( a1, a2 , a3 , a4  );
}

template< class A1, class A2, class A3, class A4, class Client, class Member >
void *
_DNBMemberFunctor4Body< A1, A2, A3, A4, Client, Member >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return (void *)client_;
}

template< class A1, class A2, class A3, class A4, class Client, class Member >
void *
_DNBMemberFunctor4Body< A1, A2, A3, A4, Client, Member >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&member_;
}

template< class A1, class A2, class A3, class A4, class Client, class Member >
_DNBBaseFunctor4Body< A1, A2, A3, A4 >* 
_DNBMemberFunctor4Body< A1, A2, A3, A4, Client, Member >::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return DNB_NEW _DNBMemberFunctor4Body< A1, A2, A3, A4, Client, Member >( *this );
}

template< class A1, class A2, class A3, class A4, class Client, class Member >
bool 
_DNBMemberFunctor4Body< A1, A2, A3, A4, Client, Member >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor4Body< A1, A2, A3, A4, Client, Member >* rhsBody = dynamic_cast< _DNBMemberFunctor4Body< A1, A2, A3, A4, Client, Member > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        return ( ( client_ == rhsBody->client_ ) && ( member_ == rhsBody->member_ ) );
    }

    return false;
}

template< class A1, class A2, class A3, class A4, class Client, class Member >
bool 
_DNBMemberFunctor4Body< A1, A2, A3, A4, Client, Member >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor4Body< A1, A2, A3, A4, Client, Member >* rhsBody = dynamic_cast< _DNBMemberFunctor4Body< A1, A2, A3, A4, Client, Member > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
            return ( (void *)&member_ < (void *)&(rhsBody->member_) );
        else
            return ( client_ < rhsBody->client_ );
    }
    else
        return ( (void *)this < (void *)&rhs );
}
template< class A1, class A2, class A3, class A4, class Client, class Member, class M1 >
_DNBMemberFunctor4M1Body< A1, A2, A3, A4, Client, Member, M1 >::_DNBMemberFunctor4M1Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class A1, class A2, class A3, class A4, class Client, class Member, class M1 >
_DNBMemberFunctor4M1Body< A1, A2, A3, A4, Client, Member, M1 >::_DNBMemberFunctor4M1Body(const _DNBMemberFunctor4M1Body< A1, A2, A3, A4, Client, Member, M1 >& memberFunctor)
    DNB_THROW_SPEC_ANY: 
m1_(memberFunctor.m1_)
{
   client_ = memberFunctor.client_;
    member_ = memberFunctor.member_;
}

template< class A1, class A2, class A3, class A4, class Client, class Member, class M1 >
_DNBMemberFunctor4M1Body< A1, A2, A3, A4, Client, Member, M1 >::_DNBMemberFunctor4M1Body(Client* client, Member member, M1 m1)
    DNB_THROW_SPEC_ANY:
client_(client), 
    member_(member),
    m1_(m1)
{
    DNB_PRECONDITION( client_ );
    DNB_PRECONDITION( member_ );
}

template< class A1, class A2, class A3, class A4, class Client, class Member, class M1 >
_DNBMemberFunctor4M1Body< A1, A2, A3, A4, Client, Member, M1 >::~_DNBMemberFunctor4M1Body()
    DNB_THROW_SPEC_ANY
{
}

template< class A1, class A2, class A3, class A4, class Client, class Member, class M1>
void 
_DNBMemberFunctor4M1Body< A1, A2, A3, A4, Client, Member, M1 >::operator()( A1 a1, A2 a2, A3 a3, A4 a4 ) 
    DNB_THROW_SPEC_ANY
{ 
    ( client_->*member_ )( a1, a2 , a3 , a4, m1_  );
}

template< class A1, class A2, class A3, class A4, class Client, class Member, class M1 >
void*
_DNBMemberFunctor4M1Body< A1, A2, A3, A4, Client, Member, M1 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return (void *)client_;
}    
template< class A1, class A2, class A3, class A4, class Client, class Member, class M1 >
void*
_DNBMemberFunctor4M1Body< A1, A2, A3, A4, Client, Member, M1 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&member_;
}   

template< class A1, class A2, class A3, class A4, class Client, class Member, class M1 >
_DNBBaseFunctor4Body< A1, A2, A3, A4 >*
_DNBMemberFunctor4M1Body< A1, A2, A3, A4, Client, Member, M1>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBMemberFunctor4M1Body< A1, A2, A3, A4, Client, Member, M1 >( *this );
}

template< class A1, class A2, class A3, class A4, class Client, class Member, class M1 >
bool 
_DNBMemberFunctor4M1Body< A1, A2, A3, A4, Client, Member, M1 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor4M1Body< A1, A2, A3, A4, Client, Member, M1 >* rhsBody = dynamic_cast< _DNBMemberFunctor4M1Body< A1, A2, A3, A4, Client, Member, M1 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
            if ( member_ == rhsBody->member_ )
                return ( m1_ == rhsBody->m1_ );
    }
    
    return false;
}

template< class A1, class A2, class A3, class A4, class Client, class Member, class M1 >
bool 
_DNBMemberFunctor4M1Body< A1, A2, A3, A4, Client, Member, M1 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor4M1Body< A1, A2, A3, A4, Client, Member, M1 >* rhsBody = dynamic_cast< _DNBMemberFunctor4M1Body< A1, A2, A3, A4, Client, Member, M1 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
        {
            if ( member_ == rhsBody->member_ )
            {
                return ( m1_ < rhsBody->m1_ );
            }
            else
                return ( (void *)&member_ < (void *)&(rhsBody->member_) );
        }
        else
            return ( client_ < rhsBody->client_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class A1, class A2, class A3, class A4 >
DNBFunctor4< A1, A2, A3, A4 >::DNBFunctor4(_DNBBaseFunctor4Body< A1, A2, A3, A4 >* body)
    DNB_THROW_SPEC_ANY: 
    body_(body) 
{
}

template< class A1, class A2, class A3, class A4 >
DNBFunctor4< A1, A2, A3, A4 >::DNBFunctor4(const DNBFunctor4< A1, A2, A3, A4 >& DNBFunctor)
    DNB_THROW_SPEC_ANY
{
    if ( DNBFunctor.body_) 
        body_ = (DNBFunctor.body_)->clone(); 
    else
        body_ = NULL;
}

template< class A1, class A2, class A3, class A4 >
DNBFunctor4< A1, A2, A3, A4 >::~DNBFunctor4() 
    DNB_THROW_SPEC_ANY
{
    if ( body_)
        DNB_DELETE body_;
}

template< class A1, class A2, class A3, class A4 >
_DNBBaseFunctorBody*
DNBFunctor4< A1, A2, A3, A4 >::getBody() const
    DNB_THROW_SPEC_ANY
{
    return (_DNBBaseFunctorBody*)body_;
}

template< class A1, class A2, class A3, class A4 >
void
DNBFunctor4< A1, A2, A3, A4 >::operator()( A1 a1, A2 a2, A3 a3, A4 a4 ) 
    DNB_THROW_SPEC_ANY
{ 
    // full name used instead of operator shorthand for Visual C++
    DNB_PRECONDITION( body_ );
     body_->operator()( a1, a2 , a3 , a4  ); 
} 

template< class A1, class A2, class A3, class A4 >
DNBFunctor4< A1, A2, A3, A4 >& 
DNBFunctor4< A1, A2, A3, A4 >::operator=(const DNBFunctor4< A1, A2, A3, A4 >& rhs)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    if ( &rhs == this )
        return *this;

    if (body_)
    {
        DNB_DELETE body_;
        body_ = NULL;
    }

    if (rhs.body_)
        body_ = (rhs.body_)->clone();
    else
        body_ = NULL;

    return *this;
}
/////////////////////////////////////////////////////////
//
// 5 Passed,  0 Returned 
//
/////////////////////////////////////////////////////////

template< class A1, class A2, class A3, class A4, class A5, class Function >
_DNBFunction5Body< A1, A2, A3, A4, A5, Function>::_DNBFunction5Body()
    DNB_THROW_SPEC_ANY
{
}


template< class A1, class A2, class A3, class A4, class A5, class Function >
_DNBFunction5Body< A1, A2, A3, A4, A5, Function>::_DNBFunction5Body(const Function& f)
    DNB_THROW_SPEC_ANY:
    f_(f) 
{
    DNB_PRECONDITION( f_ );
}

template< class A1, class A2, class A3, class A4, class A5, class Function >
_DNBFunction5Body< A1, A2, A3, A4, A5, Function>::_DNBFunction5Body(const _DNBFunction5Body< A1, A2, A3, A4, A5, Function>& functionFunctor)
    DNB_THROW_SPEC_ANY:
     f_(functionFunctor.f_)
{
    DNB_PRECONDITION( f_ );
}

template< class A1, class A2, class A3, class A4, class A5, class Function >
_DNBFunction5Body< A1, A2, A3, A4, A5, Function>::~_DNBFunction5Body()    
DNB_THROW_SPEC_ANY
{
}

template< class A1, class A2, class A3, class A4, class A5, class Function >
void
_DNBFunction5Body< A1, A2, A3, A4, A5, Function>::operator()( A1 a1, A2 a2, A3 a3, A4 a4, A5 a5 )
    DNB_THROW_SPEC_ANY
{
    f_(a1, a2 , a3 , a4 , a5 );
}

template< class A1, class A2, class A3, class A4, class A5, class Function >
void*
_DNBFunction5Body< A1, A2, A3, A4, A5, Function>::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return NULL;
}

template< class A1, class A2, class A3, class A4, class A5, class Function >
void*
_DNBFunction5Body< A1, A2, A3, A4, A5, Function>::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&f_;
}   

template< class A1, class A2, class A3, class A4, class A5, class Function >
_DNBBaseFunctor5Body< A1, A2, A3, A4, A5 >*
_DNBFunction5Body< A1, A2, A3, A4, A5, Function>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return DNB_NEW _DNBFunction5Body< A1, A2, A3, A4, A5, Function>(*this);
}

template< class A1, class A2, class A3, class A4, class A5, class Function >
bool 
_DNBFunction5Body< A1, A2, A3, A4, A5, Function >::IsEqual(_DNBBaseFunctorBody& rhs) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction5Body< A1, A2, A3, A4, A5, Function >* rhsBody = dynamic_cast< _DNBFunction5Body< A1, A2, A3, A4, A5, Function > *  > (&rhs);

    if (rhsBody != NULL)
    {
        return( f_ == rhsBody->f_ );
    }
    return false;
}

template< class A1, class A2, class A3, class A4, class A5, class Function >
bool 
_DNBFunction5Body< A1, A2, A3, A4, A5, Function >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction5Body< A1, A2, A3, A4, A5, Function >* rhsBody = dynamic_cast< _DNBFunction5Body< A1, A2, A3, A4, A5, Function > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        return ( f_ < rhsBody->f_ );
    }
    else
        return ( (void *)this < (void *)&rhs );
}

template< class A1, class A2, class A3, class A4, class A5, class Client, class Member >
_DNBMemberFunctor5Body< A1, A2, A3, A4, A5, Client, Member >::_DNBMemberFunctor5Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class A1, class A2, class A3, class A4, class A5, class Client, class Member >
_DNBMemberFunctor5Body< A1, A2, A3, A4, A5, Client, Member >::_DNBMemberFunctor5Body( const _DNBMemberFunctor5Body< A1, A2, A3, A4, A5, Client, Member >& memberFunctor )
    DNB_THROW_SPEC_ANY
{
    client_ = memberFunctor.client_;
    member_ = memberFunctor.member_;
}        

template< class A1, class A2, class A3, class A4, class A5, class Client, class Member >
_DNBMemberFunctor5Body< A1, A2, A3, A4, A5, Client, Member >::_DNBMemberFunctor5Body(Client* client, Member member)
    DNB_THROW_SPEC_ANY:
    client_(client), 
    member_(member)
{
    DNB_PRECONDITION( client_ );
    DNB_PRECONDITION( member_ );
}

template< class A1, class A2, class A3, class A4, class A5, class Client, class Member >
_DNBMemberFunctor5Body< A1, A2, A3, A4, A5, Client, Member >::~_DNBMemberFunctor5Body()
    DNB_THROW_SPEC_ANY
{
}

template< class A1, class A2, class A3, class A4, class A5, class Client, class Member >
void
_DNBMemberFunctor5Body< A1, A2, A3, A4, A5, Client, Member >::operator()( A1 a1, A2 a2, A3 a3, A4 a4, A5 a5 )
    DNB_THROW_SPEC_ANY
{
    (client_->*member_)( a1, a2 , a3 , a4 , a5  );
}

template< class A1, class A2, class A3, class A4, class A5, class Client, class Member >
void *
_DNBMemberFunctor5Body< A1, A2, A3, A4, A5, Client, Member >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return (void *)client_;
}

template< class A1, class A2, class A3, class A4, class A5, class Client, class Member >
void *
_DNBMemberFunctor5Body< A1, A2, A3, A4, A5, Client, Member >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&member_;
}

template< class A1, class A2, class A3, class A4, class A5, class Client, class Member >
_DNBBaseFunctor5Body< A1, A2, A3, A4, A5 >* 
_DNBMemberFunctor5Body< A1, A2, A3, A4, A5, Client, Member >::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return DNB_NEW _DNBMemberFunctor5Body< A1, A2, A3, A4, A5, Client, Member >( *this );
}

template< class A1, class A2, class A3, class A4, class A5, class Client, class Member >
bool 
_DNBMemberFunctor5Body< A1, A2, A3, A4, A5, Client, Member >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor5Body< A1, A2, A3, A4, A5, Client, Member >* rhsBody = dynamic_cast< _DNBMemberFunctor5Body< A1, A2, A3, A4, A5, Client, Member > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        return ( ( client_ == rhsBody->client_ ) && ( member_ == rhsBody->member_ ) );
    }

    return false;
}

template< class A1, class A2, class A3, class A4, class A5, class Client, class Member >
bool 
_DNBMemberFunctor5Body< A1, A2, A3, A4, A5, Client, Member >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor5Body< A1, A2, A3, A4, A5, Client, Member >* rhsBody = dynamic_cast< _DNBMemberFunctor5Body< A1, A2, A3, A4, A5, Client, Member > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
            return ( (void *)&member_ < (void *)&(rhsBody->member_) );
        else
            return ( client_ < rhsBody->client_ );
    }
    else
        return ( (void *)this < (void *)&rhs );
}
template< class A1, class A2, class A3, class A4, class A5 >
DNBFunctor5< A1, A2, A3, A4, A5 >::DNBFunctor5(_DNBBaseFunctor5Body< A1, A2, A3, A4, A5 >* body)
    DNB_THROW_SPEC_ANY: 
    body_(body) 
{
}

template< class A1, class A2, class A3, class A4, class A5 >
DNBFunctor5< A1, A2, A3, A4, A5 >::DNBFunctor5(const DNBFunctor5< A1, A2, A3, A4, A5 >& DNBFunctor)
    DNB_THROW_SPEC_ANY
{
    if ( DNBFunctor.body_) 
        body_ = (DNBFunctor.body_)->clone(); 
    else
        body_ = NULL;
}

template< class A1, class A2, class A3, class A4, class A5 >
DNBFunctor5< A1, A2, A3, A4, A5 >::~DNBFunctor5() 
    DNB_THROW_SPEC_ANY
{
    if ( body_)
        DNB_DELETE body_;
}

template< class A1, class A2, class A3, class A4, class A5 >
_DNBBaseFunctorBody*
DNBFunctor5< A1, A2, A3, A4, A5 >::getBody() const
    DNB_THROW_SPEC_ANY
{
    return (_DNBBaseFunctorBody*)body_;
}

template< class A1, class A2, class A3, class A4, class A5 >
void
DNBFunctor5< A1, A2, A3, A4, A5 >::operator()( A1 a1, A2 a2, A3 a3, A4 a4, A5 a5 ) 
    DNB_THROW_SPEC_ANY
{ 
    // full name used instead of operator shorthand for Visual C++
    DNB_PRECONDITION( body_ );
     body_->operator()( a1, a2 , a3 , a4 , a5  ); 
} 

template< class A1, class A2, class A3, class A4, class A5 >
DNBFunctor5< A1, A2, A3, A4, A5 >& 
DNBFunctor5< A1, A2, A3, A4, A5 >::operator=(const DNBFunctor5< A1, A2, A3, A4, A5 >& rhs)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    if ( &rhs == this )
        return *this;

    if (body_)
    {
        DNB_DELETE body_;
        body_ = NULL;
    }

    if (rhs.body_)
        body_ = (rhs.body_)->clone();
    else
        body_ = NULL;

    return *this;
}
/////////////////////////////////////////////////////////
//
// 0 Passed,  1 Returned 
//
/////////////////////////////////////////////////////////

template< class R, class Function >
_DNBFunction0wRetBody< R, Function>::_DNBFunction0wRetBody()
    DNB_THROW_SPEC_ANY
{
}


template< class R, class Function >
_DNBFunction0wRetBody< R, Function>::_DNBFunction0wRetBody(const Function& f)
    DNB_THROW_SPEC_ANY:
    f_(f) 
{
    DNB_PRECONDITION( f_ );
}

template< class R, class Function >
_DNBFunction0wRetBody< R, Function>::_DNBFunction0wRetBody(const _DNBFunction0wRetBody< R, Function>& functionFunctor)
    DNB_THROW_SPEC_ANY:
     f_(functionFunctor.f_)
{
    DNB_PRECONDITION( f_ );
}

template< class R, class Function >
_DNBFunction0wRetBody< R, Function>::~_DNBFunction0wRetBody()    
DNB_THROW_SPEC_ANY
{
}

template< class R, class Function >
R
_DNBFunction0wRetBody< R, Function>::operator()(  )
    DNB_THROW_SPEC_ANY
{
    return f_();
}

template< class R, class Function >
void*
_DNBFunction0wRetBody< R, Function>::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return NULL;
}

template< class R, class Function >
void*
_DNBFunction0wRetBody< R, Function>::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&f_;
}   

template< class R, class Function >
_DNBBaseFunctor0wRetBody< R >*
_DNBFunction0wRetBody< R, Function>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return DNB_NEW _DNBFunction0wRetBody< R, Function>( *this );
}

template< class R, class Function >
bool 
_DNBFunction0wRetBody< R, Function >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction0wRetBody< R, Function >* rhsBody = dynamic_cast< _DNBFunction0wRetBody< R, Function > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        return ( f_ == rhsBody->f_ );
    }

    return false;
}

template< class R, class Function >
bool 
_DNBFunction0wRetBody< R, Function >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction0wRetBody< R, Function >* rhsBody = dynamic_cast< _DNBFunction0wRetBody< R, Function > *  > ( &rhs );

    if ( rhsBody != NULL )
        return ( f_ < rhsBody-> f_ );
    else
        return ( (void *)this < (void *)&rhs );
}

template< class R, class Function, class M1 >
_DNBFunction0wRetM1Body< R, Function, M1 >::_DNBFunction0wRetM1Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class R, class Function, class M1 >
_DNBFunction0wRetM1Body< R, Function, M1 >::_DNBFunction0wRetM1Body(const Function& f, M1 m1)
    DNB_THROW_SPEC_ANY :
    f_(f),
    m1_(m1)
{
    DNB_PRECONDITION( f_ );
}

template< class R, class Function, class M1 >
_DNBFunction0wRetM1Body< R, Function, M1 >::_DNBFunction0wRetM1Body(const _DNBFunction0wRetM1Body< R, Function, M1 >& functionFunctor)
    DNB_THROW_SPEC_ANY :
    f_(functionFunctor.f_),
    m1_(functionFunctor.m1_)
{
    DNB_PRECONDITION( f_ );
}

template< class R, class Function, class M1 >
_DNBFunction0wRetM1Body< R, Function, M1 >::~_DNBFunction0wRetM1Body()
    DNB_THROW_SPEC_ANY
{
}

template< class R, class Function, class M1>
R 
_DNBFunction0wRetM1Body< R, Function, M1 >::operator()(  ) 
    DNB_THROW_SPEC_ANY
{ 
    return f_( m1_ );
}

template< class R, class Function, class M1 >
void*
_DNBFunction0wRetM1Body< R, Function, M1 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return NULL;
}    
template< class R, class Function, class M1 >
void*
_DNBFunction0wRetM1Body< R, Function, M1 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&f_;
}   

template< class R, class Function, class M1 >
_DNBBaseFunctor0wRetBody< R >*
_DNBFunction0wRetM1Body< R, Function, M1>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBFunction0wRetM1Body< R, Function, M1 >( *this );
}

template< class R, class Function, class M1 >
bool 
_DNBFunction0wRetM1Body< R, Function, M1 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction0wRetM1Body< R, Function, M1 >* rhsBody = dynamic_cast< _DNBFunction0wRetM1Body< R, Function, M1 > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
            return ( m1_ == rhsBody->m1_ );

    }
    
    return false;
}

template< class R, class Function, class M1 >
bool 
_DNBFunction0wRetM1Body< R, Function, M1 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction0wRetM1Body< R, Function, M1 >* rhsBody = dynamic_cast< _DNBFunction0wRetM1Body< R, Function, M1 > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
        {
            return ( m1_ < rhsBody->m1_ );

        }
        else
            return ( f_ < rhsBody->f_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class R, class Function, class M1, class M2 >
_DNBFunction0wRetM2Body< R, Function, M1, M2 >::_DNBFunction0wRetM2Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class R, class Function, class M1, class M2 >
_DNBFunction0wRetM2Body< R, Function, M1, M2 >::_DNBFunction0wRetM2Body(const Function& f, M1 m1, M2 m2)
    DNB_THROW_SPEC_ANY :
    f_(f),
    m1_(m1),
    m2_(m2)
{
    DNB_PRECONDITION( f_ );
}

template< class R, class Function, class M1, class M2 >
_DNBFunction0wRetM2Body< R, Function, M1, M2 >::_DNBFunction0wRetM2Body(const _DNBFunction0wRetM2Body< R, Function, M1, M2 >& functionFunctor)
    DNB_THROW_SPEC_ANY :
    f_(functionFunctor.f_),
    m1_(functionFunctor.m1_),
    m2_(functionFunctor.m2_)
{
    DNB_PRECONDITION( f_ );
}

template< class R, class Function, class M1, class M2 >
_DNBFunction0wRetM2Body< R, Function, M1, M2 >::~_DNBFunction0wRetM2Body()
    DNB_THROW_SPEC_ANY
{
}

template< class R, class Function, class M1, class M2>
R 
_DNBFunction0wRetM2Body< R, Function, M1, M2 >::operator()(  ) 
    DNB_THROW_SPEC_ANY
{ 
    return f_( m1_, m2_ );
}

template< class R, class Function, class M1, class M2 >
void*
_DNBFunction0wRetM2Body< R, Function, M1, M2 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return NULL;
}    
template< class R, class Function, class M1, class M2 >
void*
_DNBFunction0wRetM2Body< R, Function, M1, M2 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&f_;
}   

template< class R, class Function, class M1, class M2 >
_DNBBaseFunctor0wRetBody< R >*
_DNBFunction0wRetM2Body< R, Function, M1, M2>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBFunction0wRetM2Body< R, Function, M1, M2 >( *this );
}

template< class R, class Function, class M1, class M2 >
bool 
_DNBFunction0wRetM2Body< R, Function, M1, M2 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction0wRetM2Body< R, Function, M1, M2 >* rhsBody = dynamic_cast< _DNBFunction0wRetM2Body< R, Function, M1, M2 > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
            if ( m1_ == rhsBody->m1_ )
                return ( m2_ == rhsBody->m2_ );

    }
    
    return false;
}

template< class R, class Function, class M1, class M2 >
bool 
_DNBFunction0wRetM2Body< R, Function, M1, M2 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction0wRetM2Body< R, Function, M1, M2 >* rhsBody = dynamic_cast< _DNBFunction0wRetM2Body< R, Function, M1, M2 > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
        {
            if ( m1_ == rhsBody->m1_ )
            {
                return ( m2_ < rhsBody->m2_ );
            }
            else
                return ( m1_ < rhsBody->m1_ );

        }
        else
            return ( f_ < rhsBody->f_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class R, class Function, class M1, class M2, class M3 >
_DNBFunction0wRetM3Body< R, Function, M1, M2, M3 >::_DNBFunction0wRetM3Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class R, class Function, class M1, class M2, class M3 >
_DNBFunction0wRetM3Body< R, Function, M1, M2, M3 >::_DNBFunction0wRetM3Body(const Function& f, M1 m1, M2 m2, M3 m3)
    DNB_THROW_SPEC_ANY :
    f_(f),
    m1_(m1),
    m2_(m2),
    m3_(m3)
{
    DNB_PRECONDITION( f_ );
}

template< class R, class Function, class M1, class M2, class M3 >
_DNBFunction0wRetM3Body< R, Function, M1, M2, M3 >::_DNBFunction0wRetM3Body(const _DNBFunction0wRetM3Body< R, Function, M1, M2, M3 >& functionFunctor)
    DNB_THROW_SPEC_ANY :
    f_(functionFunctor.f_),
    m1_(functionFunctor.m1_),
    m2_(functionFunctor.m2_),
    m3_(functionFunctor.m3_)
{
    DNB_PRECONDITION( f_ );
}

template< class R, class Function, class M1, class M2, class M3 >
_DNBFunction0wRetM3Body< R, Function, M1, M2, M3 >::~_DNBFunction0wRetM3Body()
    DNB_THROW_SPEC_ANY
{
}

template< class R, class Function, class M1, class M2, class M3>
R 
_DNBFunction0wRetM3Body< R, Function, M1, M2, M3 >::operator()(  ) 
    DNB_THROW_SPEC_ANY
{ 
    return f_( m1_, m2_, m3_ );
}

template< class R, class Function, class M1, class M2, class M3 >
void*
_DNBFunction0wRetM3Body< R, Function, M1, M2, M3 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return NULL;
}    
template< class R, class Function, class M1, class M2, class M3 >
void*
_DNBFunction0wRetM3Body< R, Function, M1, M2, M3 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&f_;
}   

template< class R, class Function, class M1, class M2, class M3 >
_DNBBaseFunctor0wRetBody< R >*
_DNBFunction0wRetM3Body< R, Function, M1, M2, M3>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBFunction0wRetM3Body< R, Function, M1, M2, M3 >( *this );
}

template< class R, class Function, class M1, class M2, class M3 >
bool 
_DNBFunction0wRetM3Body< R, Function, M1, M2, M3 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction0wRetM3Body< R, Function, M1, M2, M3 >* rhsBody = dynamic_cast< _DNBFunction0wRetM3Body< R, Function, M1, M2, M3 > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
            if ( m1_ == rhsBody->m1_ )
                if ( m2_ == rhsBody->m2_ )
                    return ( m3_ == rhsBody->m3_ );

    }
    
    return false;
}

template< class R, class Function, class M1, class M2, class M3 >
bool 
_DNBFunction0wRetM3Body< R, Function, M1, M2, M3 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction0wRetM3Body< R, Function, M1, M2, M3 >* rhsBody = dynamic_cast< _DNBFunction0wRetM3Body< R, Function, M1, M2, M3 > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
        {
            if ( m1_ == rhsBody->m1_ )
            {
                if ( m2_ == rhsBody->m2_ )
                {
                    return ( m3_ < rhsBody->m3_ );
                }
                else
                    return ( m2_ < rhsBody->m2_ );
            }
            else
                return ( m1_ < rhsBody->m1_ );

        }
        else
            return ( f_ < rhsBody->f_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class R, class Function, class M1, class M2, class M3, class M4 >
_DNBFunction0wRetM4Body< R, Function, M1, M2, M3, M4 >::_DNBFunction0wRetM4Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class R, class Function, class M1, class M2, class M3, class M4 >
_DNBFunction0wRetM4Body< R, Function, M1, M2, M3, M4 >::_DNBFunction0wRetM4Body(const Function& f, M1 m1, M2 m2, M3 m3, M4 m4)
    DNB_THROW_SPEC_ANY :
    f_(f),
    m1_(m1),
    m2_(m2),
    m3_(m3),
    m4_(m4)
{
    DNB_PRECONDITION( f_ );
}

template< class R, class Function, class M1, class M2, class M3, class M4 >
_DNBFunction0wRetM4Body< R, Function, M1, M2, M3, M4 >::_DNBFunction0wRetM4Body(const _DNBFunction0wRetM4Body< R, Function, M1, M2, M3, M4 >& functionFunctor)
    DNB_THROW_SPEC_ANY :
    f_(functionFunctor.f_),
    m1_(functionFunctor.m1_),
    m2_(functionFunctor.m2_),
    m3_(functionFunctor.m3_),
    m4_(functionFunctor.m4_)
{
    DNB_PRECONDITION( f_ );
}

template< class R, class Function, class M1, class M2, class M3, class M4 >
_DNBFunction0wRetM4Body< R, Function, M1, M2, M3, M4 >::~_DNBFunction0wRetM4Body()
    DNB_THROW_SPEC_ANY
{
}

template< class R, class Function, class M1, class M2, class M3, class M4>
R 
_DNBFunction0wRetM4Body< R, Function, M1, M2, M3, M4 >::operator()(  ) 
    DNB_THROW_SPEC_ANY
{ 
    return f_( m1_, m2_, m3_, m4_ );
}

template< class R, class Function, class M1, class M2, class M3, class M4 >
void*
_DNBFunction0wRetM4Body< R, Function, M1, M2, M3, M4 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return NULL;
}    
template< class R, class Function, class M1, class M2, class M3, class M4 >
void*
_DNBFunction0wRetM4Body< R, Function, M1, M2, M3, M4 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&f_;
}   

template< class R, class Function, class M1, class M2, class M3, class M4 >
_DNBBaseFunctor0wRetBody< R >*
_DNBFunction0wRetM4Body< R, Function, M1, M2, M3, M4>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBFunction0wRetM4Body< R, Function, M1, M2, M3, M4 >( *this );
}

template< class R, class Function, class M1, class M2, class M3, class M4 >
bool 
_DNBFunction0wRetM4Body< R, Function, M1, M2, M3, M4 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction0wRetM4Body< R, Function, M1, M2, M3, M4 >* rhsBody = dynamic_cast< _DNBFunction0wRetM4Body< R, Function, M1, M2, M3, M4 > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
            if ( m1_ == rhsBody->m1_ )
                if ( m2_ == rhsBody->m2_ )
                    if ( m3_ == rhsBody->m3_ )
                        return ( m4_ == rhsBody->m4_ );

    }
    
    return false;
}

template< class R, class Function, class M1, class M2, class M3, class M4 >
bool 
_DNBFunction0wRetM4Body< R, Function, M1, M2, M3, M4 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction0wRetM4Body< R, Function, M1, M2, M3, M4 >* rhsBody = dynamic_cast< _DNBFunction0wRetM4Body< R, Function, M1, M2, M3, M4 > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
        {
            if ( m1_ == rhsBody->m1_ )
            {
                if ( m2_ == rhsBody->m2_ )
                {
                    if ( m3_ == rhsBody->m3_ )
                    {
                        return ( m4_ < rhsBody->m4_ );
                    }
                    else
                        return ( m3_ < rhsBody->m3_ );
                }
                else
                    return ( m2_ < rhsBody->m2_ );
            }
            else
                return ( m1_ < rhsBody->m1_ );

        }
        else
            return ( f_ < rhsBody->f_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class R, class Function, class M1, class M2, class M3, class M4, class M5 >
_DNBFunction0wRetM5Body< R, Function, M1, M2, M3, M4, M5 >::_DNBFunction0wRetM5Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class R, class Function, class M1, class M2, class M3, class M4, class M5 >
_DNBFunction0wRetM5Body< R, Function, M1, M2, M3, M4, M5 >::_DNBFunction0wRetM5Body(const Function& f, M1 m1, M2 m2, M3 m3, M4 m4, M5 m5)
    DNB_THROW_SPEC_ANY :
    f_(f),
    m1_(m1),
    m2_(m2),
    m3_(m3),
    m4_(m4),
    m5_(m5)
{
    DNB_PRECONDITION( f_ );
}

template< class R, class Function, class M1, class M2, class M3, class M4, class M5 >
_DNBFunction0wRetM5Body< R, Function, M1, M2, M3, M4, M5 >::_DNBFunction0wRetM5Body(const _DNBFunction0wRetM5Body< R, Function, M1, M2, M3, M4, M5 >& functionFunctor)
    DNB_THROW_SPEC_ANY :
    f_(functionFunctor.f_),
    m1_(functionFunctor.m1_),
    m2_(functionFunctor.m2_),
    m3_(functionFunctor.m3_),
    m4_(functionFunctor.m4_),
    m5_(functionFunctor.m5_)
{
    DNB_PRECONDITION( f_ );
}

template< class R, class Function, class M1, class M2, class M3, class M4, class M5 >
_DNBFunction0wRetM5Body< R, Function, M1, M2, M3, M4, M5 >::~_DNBFunction0wRetM5Body()
    DNB_THROW_SPEC_ANY
{
}

template< class R, class Function, class M1, class M2, class M3, class M4, class M5>
R 
_DNBFunction0wRetM5Body< R, Function, M1, M2, M3, M4, M5 >::operator()(  ) 
    DNB_THROW_SPEC_ANY
{ 
    return f_( m1_, m2_, m3_, m4_, m5_ );
}

template< class R, class Function, class M1, class M2, class M3, class M4, class M5 >
void*
_DNBFunction0wRetM5Body< R, Function, M1, M2, M3, M4, M5 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return NULL;
}    
template< class R, class Function, class M1, class M2, class M3, class M4, class M5 >
void*
_DNBFunction0wRetM5Body< R, Function, M1, M2, M3, M4, M5 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&f_;
}   

template< class R, class Function, class M1, class M2, class M3, class M4, class M5 >
_DNBBaseFunctor0wRetBody< R >*
_DNBFunction0wRetM5Body< R, Function, M1, M2, M3, M4, M5>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBFunction0wRetM5Body< R, Function, M1, M2, M3, M4, M5 >( *this );
}

template< class R, class Function, class M1, class M2, class M3, class M4, class M5 >
bool 
_DNBFunction0wRetM5Body< R, Function, M1, M2, M3, M4, M5 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction0wRetM5Body< R, Function, M1, M2, M3, M4, M5 >* rhsBody = dynamic_cast< _DNBFunction0wRetM5Body< R, Function, M1, M2, M3, M4, M5 > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
            if ( m1_ == rhsBody->m1_ )
                if ( m2_ == rhsBody->m2_ )
                    if ( m3_ == rhsBody->m3_ )
                        if ( m4_ == rhsBody->m4_ )
                            return ( m5_ == rhsBody->m5_ );

    }
    
    return false;
}

template< class R, class Function, class M1, class M2, class M3, class M4, class M5 >
bool 
_DNBFunction0wRetM5Body< R, Function, M1, M2, M3, M4, M5 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction0wRetM5Body< R, Function, M1, M2, M3, M4, M5 >* rhsBody = dynamic_cast< _DNBFunction0wRetM5Body< R, Function, M1, M2, M3, M4, M5 > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
        {
            if ( m1_ == rhsBody->m1_ )
            {
                if ( m2_ == rhsBody->m2_ )
                {
                    if ( m3_ == rhsBody->m3_ )
                    {
                        if ( m4_ == rhsBody->m4_ )
                        {
                            return ( m5_ < rhsBody->m5_ );
                        }
                        else
                            return ( m4_ < rhsBody->m4_ );
                    }
                    else
                        return ( m3_ < rhsBody->m3_ );
                }
                else
                    return ( m2_ < rhsBody->m2_ );
            }
            else
                return ( m1_ < rhsBody->m1_ );

        }
        else
            return ( f_ < rhsBody->f_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class R, class Client, class Member >
_DNBMemberFunctor0wRetBody< R, Client, Member >::_DNBMemberFunctor0wRetBody() 
    DNB_THROW_SPEC_ANY
{
}

template< class R, class Client, class Member >
_DNBMemberFunctor0wRetBody< R, Client, Member >::_DNBMemberFunctor0wRetBody( const _DNBMemberFunctor0wRetBody< R, Client, Member >& memberFunctor )
    DNB_THROW_SPEC_ANY
{
    client_ = memberFunctor.client_;
    member_ = memberFunctor.member_;
}        

template< class R, class Client, class Member >
_DNBMemberFunctor0wRetBody< R, Client, Member >::_DNBMemberFunctor0wRetBody(Client* client, Member member)
    DNB_THROW_SPEC_ANY:
    client_(client), 
    member_(member)
{
    DNB_PRECONDITION( client_ );
    DNB_PRECONDITION( member_ );
}

template< class R, class Client, class Member >
_DNBMemberFunctor0wRetBody< R, Client, Member >::~_DNBMemberFunctor0wRetBody()
    DNB_THROW_SPEC_ANY
{
}

template< class R, class Client, class Member >
R
_DNBMemberFunctor0wRetBody< R, Client, Member >::operator()(  )
    DNB_THROW_SPEC_ANY
{
    return (client_->*member_)(  );
}

template< class R, class Client, class Member >
void *
_DNBMemberFunctor0wRetBody< R, Client, Member >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return (void *)client_;
}

template< class R, class Client, class Member >
void *
_DNBMemberFunctor0wRetBody< R, Client, Member >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&member_;
}

template< class R, class Client, class Member >
_DNBBaseFunctor0wRetBody< R >* 
_DNBMemberFunctor0wRetBody< R, Client, Member >::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return DNB_NEW _DNBMemberFunctor0wRetBody< R, Client, Member >( *this );
}

template< class R, class Client, class Member >
bool 
_DNBMemberFunctor0wRetBody< R, Client, Member >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor0wRetBody< R, Client, Member >* rhsBody = dynamic_cast< _DNBMemberFunctor0wRetBody< R, Client, Member > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
         return ( ( client_ == rhsBody->client_ ) && ( member_ == rhsBody->member_ ) );
    }

    return false;
}

template< class R, class Client, class Member >
bool 
_DNBMemberFunctor0wRetBody< R, Client, Member >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor0wRetBody< R, Client, Member >* rhsBody = dynamic_cast< _DNBMemberFunctor0wRetBody< R, Client, Member > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
         if ( client_ == client_ )
             return ( (void *)&member_ < (void *)&(rhsBody->member_) );
         else
             return ( client_ < rhsBody->client_ );
    }
    else
        return ( (void *)this < (void *)&rhs );
}
template< class R, class Client, class Member, class M1 >
_DNBMemberFunctor0wRetM1Body< R, Client, Member, M1 >::_DNBMemberFunctor0wRetM1Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class R, class Client, class Member, class M1 >
_DNBMemberFunctor0wRetM1Body< R, Client, Member, M1 >::_DNBMemberFunctor0wRetM1Body(const _DNBMemberFunctor0wRetM1Body< R, Client, Member, M1 >& memberFunctor)
    DNB_THROW_SPEC_ANY: 
m1_(memberFunctor.m1_)
{
   client_ = memberFunctor.client_;
    member_ = memberFunctor.member_;
}

template< class R, class Client, class Member, class M1 >
_DNBMemberFunctor0wRetM1Body< R, Client, Member, M1 >::_DNBMemberFunctor0wRetM1Body(Client* client, Member member, M1 m1)
    DNB_THROW_SPEC_ANY:
client_(client), 
    member_(member),
    m1_(m1)
{
    DNB_PRECONDITION( client_ );
    DNB_PRECONDITION( member_ );
}

template< class R, class Client, class Member, class M1 >
_DNBMemberFunctor0wRetM1Body< R, Client, Member, M1 >::~_DNBMemberFunctor0wRetM1Body()
    DNB_THROW_SPEC_ANY
{
}

template< class R, class Client, class Member, class M1>
R 
_DNBMemberFunctor0wRetM1Body< R, Client, Member, M1 >::operator()(  ) 
    DNB_THROW_SPEC_ANY
{ 
    return ( client_->*member_ )( m1_ );
}

template< class R, class Client, class Member, class M1 >
void*
_DNBMemberFunctor0wRetM1Body< R, Client, Member, M1 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return (void *)client_;
}    
template< class R, class Client, class Member, class M1 >
void*
_DNBMemberFunctor0wRetM1Body< R, Client, Member, M1 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&member_;
}   

template< class R, class Client, class Member, class M1 >
_DNBBaseFunctor0wRetBody< R >*
_DNBMemberFunctor0wRetM1Body< R, Client, Member, M1>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBMemberFunctor0wRetM1Body< R, Client, Member, M1 >( *this );
}

template< class R, class Client, class Member, class M1 >
bool 
_DNBMemberFunctor0wRetM1Body< R, Client, Member, M1 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor0wRetM1Body< R, Client, Member, M1 >* rhsBody = dynamic_cast< _DNBMemberFunctor0wRetM1Body< R, Client, Member, M1 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
            if ( member_ == rhsBody->member_ )
                return ( m1_ == rhsBody->m1_ );

    }

    return false;
}

template< class R, class Client, class Member, class M1 >
bool 
_DNBMemberFunctor0wRetM1Body< R, Client, Member, M1 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor0wRetM1Body< R, Client, Member, M1 >* rhsBody = dynamic_cast< _DNBMemberFunctor0wRetM1Body< R, Client, Member, M1 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
        {
            if ( member_ == rhsBody->member_ )
            {
                return ( m1_ < rhsBody->m1_ );

            }
            else
                return ( (void *)&member_ < (void *)&(rhsBody->member_) );
        }
        else
            return ( client_ < rhsBody->client_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class R, class Client, class Member, class M1, class M2 >
_DNBMemberFunctor0wRetM2Body< R, Client, Member, M1, M2 >::_DNBMemberFunctor0wRetM2Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class R, class Client, class Member, class M1, class M2 >
_DNBMemberFunctor0wRetM2Body< R, Client, Member, M1, M2 >::_DNBMemberFunctor0wRetM2Body(const _DNBMemberFunctor0wRetM2Body< R, Client, Member, M1, M2 >& memberFunctor)
    DNB_THROW_SPEC_ANY: 
m1_(memberFunctor.m1_),
    m2_(memberFunctor.m2_)
{
   client_ = memberFunctor.client_;
    member_ = memberFunctor.member_;
}

template< class R, class Client, class Member, class M1, class M2 >
_DNBMemberFunctor0wRetM2Body< R, Client, Member, M1, M2 >::_DNBMemberFunctor0wRetM2Body(Client* client, Member member, M1 m1, M2 m2)
    DNB_THROW_SPEC_ANY:
client_(client), 
    member_(member),
    m1_(m1),
    m2_(m2)
{
    DNB_PRECONDITION( client_ );
    DNB_PRECONDITION( member_ );
}

template< class R, class Client, class Member, class M1, class M2 >
_DNBMemberFunctor0wRetM2Body< R, Client, Member, M1, M2 >::~_DNBMemberFunctor0wRetM2Body()
    DNB_THROW_SPEC_ANY
{
}

template< class R, class Client, class Member, class M1, class M2>
R 
_DNBMemberFunctor0wRetM2Body< R, Client, Member, M1, M2 >::operator()(  ) 
    DNB_THROW_SPEC_ANY
{ 
    return ( client_->*member_ )( m1_, m2_ );
}

template< class R, class Client, class Member, class M1, class M2 >
void*
_DNBMemberFunctor0wRetM2Body< R, Client, Member, M1, M2 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return (void *)client_;
}    
template< class R, class Client, class Member, class M1, class M2 >
void*
_DNBMemberFunctor0wRetM2Body< R, Client, Member, M1, M2 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&member_;
}   

template< class R, class Client, class Member, class M1, class M2 >
_DNBBaseFunctor0wRetBody< R >*
_DNBMemberFunctor0wRetM2Body< R, Client, Member, M1, M2>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBMemberFunctor0wRetM2Body< R, Client, Member, M1, M2 >( *this );
}

template< class R, class Client, class Member, class M1, class M2 >
bool 
_DNBMemberFunctor0wRetM2Body< R, Client, Member, M1, M2 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor0wRetM2Body< R, Client, Member, M1, M2 >* rhsBody = dynamic_cast< _DNBMemberFunctor0wRetM2Body< R, Client, Member, M1, M2 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
            if ( member_ == rhsBody->member_ )
                if ( m1_ == rhsBody->m1_ )
                    return ( m2_ == rhsBody->m2_ );

    }

    return false;
}

template< class R, class Client, class Member, class M1, class M2 >
bool 
_DNBMemberFunctor0wRetM2Body< R, Client, Member, M1, M2 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor0wRetM2Body< R, Client, Member, M1, M2 >* rhsBody = dynamic_cast< _DNBMemberFunctor0wRetM2Body< R, Client, Member, M1, M2 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
        {
            if ( member_ == rhsBody->member_ )
            {
                if ( m1_ == rhsBody->m1_ )
                {
                    return ( m2_ < rhsBody->m2_ );
                }
                else
                    return ( m1_ < rhsBody->m1_ );

            }
            else
                return ( (void *)&member_ < (void *)&(rhsBody->member_) );
        }
        else
            return ( client_ < rhsBody->client_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class R, class Client, class Member, class M1, class M2, class M3 >
_DNBMemberFunctor0wRetM3Body< R, Client, Member, M1, M2, M3 >::_DNBMemberFunctor0wRetM3Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class R, class Client, class Member, class M1, class M2, class M3 >
_DNBMemberFunctor0wRetM3Body< R, Client, Member, M1, M2, M3 >::_DNBMemberFunctor0wRetM3Body(const _DNBMemberFunctor0wRetM3Body< R, Client, Member, M1, M2, M3 >& memberFunctor)
    DNB_THROW_SPEC_ANY: 
m1_(memberFunctor.m1_),
    m2_(memberFunctor.m2_),
    m3_(memberFunctor.m3_)
{
   client_ = memberFunctor.client_;
    member_ = memberFunctor.member_;
}

template< class R, class Client, class Member, class M1, class M2, class M3 >
_DNBMemberFunctor0wRetM3Body< R, Client, Member, M1, M2, M3 >::_DNBMemberFunctor0wRetM3Body(Client* client, Member member, M1 m1, M2 m2, M3 m3)
    DNB_THROW_SPEC_ANY:
client_(client), 
    member_(member),
    m1_(m1),
    m2_(m2),
    m3_(m3)
{
    DNB_PRECONDITION( client_ );
    DNB_PRECONDITION( member_ );
}

template< class R, class Client, class Member, class M1, class M2, class M3 >
_DNBMemberFunctor0wRetM3Body< R, Client, Member, M1, M2, M3 >::~_DNBMemberFunctor0wRetM3Body()
    DNB_THROW_SPEC_ANY
{
}

template< class R, class Client, class Member, class M1, class M2, class M3>
R 
_DNBMemberFunctor0wRetM3Body< R, Client, Member, M1, M2, M3 >::operator()(  ) 
    DNB_THROW_SPEC_ANY
{ 
    return ( client_->*member_ )( m1_, m2_, m3_ );
}

template< class R, class Client, class Member, class M1, class M2, class M3 >
void*
_DNBMemberFunctor0wRetM3Body< R, Client, Member, M1, M2, M3 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return (void *)client_;
}    
template< class R, class Client, class Member, class M1, class M2, class M3 >
void*
_DNBMemberFunctor0wRetM3Body< R, Client, Member, M1, M2, M3 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&member_;
}   

template< class R, class Client, class Member, class M1, class M2, class M3 >
_DNBBaseFunctor0wRetBody< R >*
_DNBMemberFunctor0wRetM3Body< R, Client, Member, M1, M2, M3>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBMemberFunctor0wRetM3Body< R, Client, Member, M1, M2, M3 >( *this );
}

template< class R, class Client, class Member, class M1, class M2, class M3 >
bool 
_DNBMemberFunctor0wRetM3Body< R, Client, Member, M1, M2, M3 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor0wRetM3Body< R, Client, Member, M1, M2, M3 >* rhsBody = dynamic_cast< _DNBMemberFunctor0wRetM3Body< R, Client, Member, M1, M2, M3 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
            if ( member_ == rhsBody->member_ )
                if ( m1_ == rhsBody->m1_ )
                    if ( m2_ == rhsBody->m2_ )
                        return ( m3_ == rhsBody->m3_ );

    }

    return false;
}

template< class R, class Client, class Member, class M1, class M2, class M3 >
bool 
_DNBMemberFunctor0wRetM3Body< R, Client, Member, M1, M2, M3 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor0wRetM3Body< R, Client, Member, M1, M2, M3 >* rhsBody = dynamic_cast< _DNBMemberFunctor0wRetM3Body< R, Client, Member, M1, M2, M3 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
        {
            if ( member_ == rhsBody->member_ )
            {
                if ( m1_ == rhsBody->m1_ )
                {
                    if ( m2_ == rhsBody->m2_ )
                    {
                        return ( m3_ < rhsBody->m3_ );
                    }
                    else
                        return ( m2_ < rhsBody->m2_ );
                }
                else
                    return ( m1_ < rhsBody->m1_ );

            }
            else
                return ( (void *)&member_ < (void *)&(rhsBody->member_) );
        }
        else
            return ( client_ < rhsBody->client_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class R, class Client, class Member, class M1, class M2, class M3, class M4 >
_DNBMemberFunctor0wRetM4Body< R, Client, Member, M1, M2, M3, M4 >::_DNBMemberFunctor0wRetM4Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class R, class Client, class Member, class M1, class M2, class M3, class M4 >
_DNBMemberFunctor0wRetM4Body< R, Client, Member, M1, M2, M3, M4 >::_DNBMemberFunctor0wRetM4Body(const _DNBMemberFunctor0wRetM4Body< R, Client, Member, M1, M2, M3, M4 >& memberFunctor)
    DNB_THROW_SPEC_ANY: 
m1_(memberFunctor.m1_),
    m2_(memberFunctor.m2_),
    m3_(memberFunctor.m3_),
    m4_(memberFunctor.m4_)
{
   client_ = memberFunctor.client_;
    member_ = memberFunctor.member_;
}

template< class R, class Client, class Member, class M1, class M2, class M3, class M4 >
_DNBMemberFunctor0wRetM4Body< R, Client, Member, M1, M2, M3, M4 >::_DNBMemberFunctor0wRetM4Body(Client* client, Member member, M1 m1, M2 m2, M3 m3, M4 m4)
    DNB_THROW_SPEC_ANY:
client_(client), 
    member_(member),
    m1_(m1),
    m2_(m2),
    m3_(m3),
    m4_(m4)
{
    DNB_PRECONDITION( client_ );
    DNB_PRECONDITION( member_ );
}

template< class R, class Client, class Member, class M1, class M2, class M3, class M4 >
_DNBMemberFunctor0wRetM4Body< R, Client, Member, M1, M2, M3, M4 >::~_DNBMemberFunctor0wRetM4Body()
    DNB_THROW_SPEC_ANY
{
}

template< class R, class Client, class Member, class M1, class M2, class M3, class M4>
R 
_DNBMemberFunctor0wRetM4Body< R, Client, Member, M1, M2, M3, M4 >::operator()(  ) 
    DNB_THROW_SPEC_ANY
{ 
    return ( client_->*member_ )( m1_, m2_, m3_, m4_ );
}

template< class R, class Client, class Member, class M1, class M2, class M3, class M4 >
void*
_DNBMemberFunctor0wRetM4Body< R, Client, Member, M1, M2, M3, M4 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return (void *)client_;
}    
template< class R, class Client, class Member, class M1, class M2, class M3, class M4 >
void*
_DNBMemberFunctor0wRetM4Body< R, Client, Member, M1, M2, M3, M4 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&member_;
}   

template< class R, class Client, class Member, class M1, class M2, class M3, class M4 >
_DNBBaseFunctor0wRetBody< R >*
_DNBMemberFunctor0wRetM4Body< R, Client, Member, M1, M2, M3, M4>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBMemberFunctor0wRetM4Body< R, Client, Member, M1, M2, M3, M4 >( *this );
}

template< class R, class Client, class Member, class M1, class M2, class M3, class M4 >
bool 
_DNBMemberFunctor0wRetM4Body< R, Client, Member, M1, M2, M3, M4 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor0wRetM4Body< R, Client, Member, M1, M2, M3, M4 >* rhsBody = dynamic_cast< _DNBMemberFunctor0wRetM4Body< R, Client, Member, M1, M2, M3, M4 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
            if ( member_ == rhsBody->member_ )
                if ( m1_ == rhsBody->m1_ )
                    if ( m2_ == rhsBody->m2_ )
                        if ( m3_ == rhsBody->m3_ )
                            return ( m4_ == rhsBody->m4_ );

    }

    return false;
}

template< class R, class Client, class Member, class M1, class M2, class M3, class M4 >
bool 
_DNBMemberFunctor0wRetM4Body< R, Client, Member, M1, M2, M3, M4 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor0wRetM4Body< R, Client, Member, M1, M2, M3, M4 >* rhsBody = dynamic_cast< _DNBMemberFunctor0wRetM4Body< R, Client, Member, M1, M2, M3, M4 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
        {
            if ( member_ == rhsBody->member_ )
            {
                if ( m1_ == rhsBody->m1_ )
                {
                    if ( m2_ == rhsBody->m2_ )
                    {
                        if ( m3_ == rhsBody->m3_ )
                        {
                            return ( m4_ < rhsBody->m4_ );
                        }
                        else
                            return ( m3_ < rhsBody->m3_ );
                    }
                    else
                        return ( m2_ < rhsBody->m2_ );
                }
                else
                    return ( m1_ < rhsBody->m1_ );

            }
            else
                return ( (void *)&member_ < (void *)&(rhsBody->member_) );
        }
        else
            return ( client_ < rhsBody->client_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class R, class Client, class Member, class M1, class M2, class M3, class M4, class M5 >
_DNBMemberFunctor0wRetM5Body< R, Client, Member, M1, M2, M3, M4, M5 >::_DNBMemberFunctor0wRetM5Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class R, class Client, class Member, class M1, class M2, class M3, class M4, class M5 >
_DNBMemberFunctor0wRetM5Body< R, Client, Member, M1, M2, M3, M4, M5 >::_DNBMemberFunctor0wRetM5Body(const _DNBMemberFunctor0wRetM5Body< R, Client, Member, M1, M2, M3, M4, M5 >& memberFunctor)
    DNB_THROW_SPEC_ANY: 
m1_(memberFunctor.m1_),
    m2_(memberFunctor.m2_),
    m3_(memberFunctor.m3_),
    m4_(memberFunctor.m4_),
    m5_(memberFunctor.m5_)
{
   client_ = memberFunctor.client_;
    member_ = memberFunctor.member_;
}

template< class R, class Client, class Member, class M1, class M2, class M3, class M4, class M5 >
_DNBMemberFunctor0wRetM5Body< R, Client, Member, M1, M2, M3, M4, M5 >::_DNBMemberFunctor0wRetM5Body(Client* client, Member member, M1 m1, M2 m2, M3 m3, M4 m4, M5 m5)
    DNB_THROW_SPEC_ANY:
client_(client), 
    member_(member),
    m1_(m1),
    m2_(m2),
    m3_(m3),
    m4_(m4),
    m5_(m5)
{
    DNB_PRECONDITION( client_ );
    DNB_PRECONDITION( member_ );
}

template< class R, class Client, class Member, class M1, class M2, class M3, class M4, class M5 >
_DNBMemberFunctor0wRetM5Body< R, Client, Member, M1, M2, M3, M4, M5 >::~_DNBMemberFunctor0wRetM5Body()
    DNB_THROW_SPEC_ANY
{
}

template< class R, class Client, class Member, class M1, class M2, class M3, class M4, class M5>
R 
_DNBMemberFunctor0wRetM5Body< R, Client, Member, M1, M2, M3, M4, M5 >::operator()(  ) 
    DNB_THROW_SPEC_ANY
{ 
    return ( client_->*member_ )( m1_, m2_, m3_, m4_, m5_ );
}

template< class R, class Client, class Member, class M1, class M2, class M3, class M4, class M5 >
void*
_DNBMemberFunctor0wRetM5Body< R, Client, Member, M1, M2, M3, M4, M5 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return (void *)client_;
}    
template< class R, class Client, class Member, class M1, class M2, class M3, class M4, class M5 >
void*
_DNBMemberFunctor0wRetM5Body< R, Client, Member, M1, M2, M3, M4, M5 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&member_;
}   

template< class R, class Client, class Member, class M1, class M2, class M3, class M4, class M5 >
_DNBBaseFunctor0wRetBody< R >*
_DNBMemberFunctor0wRetM5Body< R, Client, Member, M1, M2, M3, M4, M5>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBMemberFunctor0wRetM5Body< R, Client, Member, M1, M2, M3, M4, M5 >( *this );
}

template< class R, class Client, class Member, class M1, class M2, class M3, class M4, class M5 >
bool 
_DNBMemberFunctor0wRetM5Body< R, Client, Member, M1, M2, M3, M4, M5 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor0wRetM5Body< R, Client, Member, M1, M2, M3, M4, M5 >* rhsBody = dynamic_cast< _DNBMemberFunctor0wRetM5Body< R, Client, Member, M1, M2, M3, M4, M5 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
            if ( member_ == rhsBody->member_ )
                if ( m1_ == rhsBody->m1_ )
                    if ( m2_ == rhsBody->m2_ )
                        if ( m3_ == rhsBody->m3_ )
                            if ( m4_ == rhsBody->m4_ )
                                return ( m5_ == rhsBody->m5_ );

    }

    return false;
}

template< class R, class Client, class Member, class M1, class M2, class M3, class M4, class M5 >
bool 
_DNBMemberFunctor0wRetM5Body< R, Client, Member, M1, M2, M3, M4, M5 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor0wRetM5Body< R, Client, Member, M1, M2, M3, M4, M5 >* rhsBody = dynamic_cast< _DNBMemberFunctor0wRetM5Body< R, Client, Member, M1, M2, M3, M4, M5 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
        {
            if ( member_ == rhsBody->member_ )
            {
                if ( m1_ == rhsBody->m1_ )
                {
                    if ( m2_ == rhsBody->m2_ )
                    {
                        if ( m3_ == rhsBody->m3_ )
                        {
                            if ( m4_ == rhsBody->m4_ )
                            {
                                return ( m5_ < rhsBody->m5_ );
                            }
                            else
                                return ( m4_ < rhsBody->m4_ );
                        }
                        else
                            return ( m3_ < rhsBody->m3_ );
                    }
                    else
                        return ( m2_ < rhsBody->m2_ );
                }
                else
                    return ( m1_ < rhsBody->m1_ );

            }
            else
                return ( (void *)&member_ < (void *)&(rhsBody->member_) );
        }
        else
            return ( client_ < rhsBody->client_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class R >
DNBFunctor0wRet< R >::DNBFunctor0wRet(_DNBBaseFunctor0wRetBody< R >* body)
    DNB_THROW_SPEC_ANY: 
    body_(body) 
{
}

template< class R >
DNBFunctor0wRet< R >::DNBFunctor0wRet(const DNBFunctor0wRet< R >& DNBFunctor)
    DNB_THROW_SPEC_ANY
{
    if ( DNBFunctor.body_) 
        body_ = (DNBFunctor.body_)->clone(); 
    else
        body_ = NULL;
}

template< class R >
DNBFunctor0wRet< R >::~DNBFunctor0wRet() 
    DNB_THROW_SPEC_ANY
{
    if ( body_)
        DNB_DELETE body_;
}

template< class R >
_DNBBaseFunctorBody*
DNBFunctor0wRet< R >::getBody() const
    DNB_THROW_SPEC_ANY
{
    return (_DNBBaseFunctorBody*)body_;
}

template< class R >
R
DNBFunctor0wRet< R >::operator()(  ) 
    DNB_THROW_SPEC_ANY
{ 
    // full name used instead of operator shorthand for Visual C++
    DNB_PRECONDITION( body_ );
     return body_->operator()(  ); 
} 

template< class R >
DNBFunctor0wRet< R >& 
DNBFunctor0wRet< R >::operator=(const DNBFunctor0wRet< R >& rhs)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    if ( &rhs == this )
        return *this;

    if (body_)
    {
        DNB_DELETE body_;
        body_ = NULL;
    }

    if (rhs.body_)
        body_ = (rhs.body_)->clone();
    else
        body_ = NULL;

    return *this;
}
/////////////////////////////////////////////////////////
//
// 1 Passed,  1 Returned 
//
/////////////////////////////////////////////////////////

template< class R, class A1, class Function >
_DNBFunction1wRetBody< R, A1, Function>::_DNBFunction1wRetBody()
    DNB_THROW_SPEC_ANY
{
}


template< class R, class A1, class Function >
_DNBFunction1wRetBody< R, A1, Function>::_DNBFunction1wRetBody(const Function& f)
    DNB_THROW_SPEC_ANY:
    f_(f) 
{
    DNB_PRECONDITION( f_ );
}

template< class R, class A1, class Function >
_DNBFunction1wRetBody< R, A1, Function>::_DNBFunction1wRetBody(const _DNBFunction1wRetBody< R, A1, Function>& functionFunctor)
    DNB_THROW_SPEC_ANY:
     f_(functionFunctor.f_)
{
    DNB_PRECONDITION( f_ );
}

template< class R, class A1, class Function >
_DNBFunction1wRetBody< R, A1, Function>::~_DNBFunction1wRetBody()    
DNB_THROW_SPEC_ANY
{
}

template< class R, class A1, class Function >
R
_DNBFunction1wRetBody< R, A1, Function>::operator()( A1 a1 )
    DNB_THROW_SPEC_ANY
{
    return f_(a1);
}

template< class R, class A1, class Function >
void*
_DNBFunction1wRetBody< R, A1, Function>::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return NULL;
}

template< class R, class A1, class Function >
void*
_DNBFunction1wRetBody< R, A1, Function>::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&f_;
}   

template< class R, class A1, class Function >
_DNBBaseFunctor1wRetBody< R, A1 >*
_DNBFunction1wRetBody< R, A1, Function>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return DNB_NEW _DNBFunction1wRetBody< R, A1, Function>( *this );
}

template< class R, class A1, class Function >
bool 
_DNBFunction1wRetBody< R, A1, Function >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction1wRetBody< R, A1, Function >* rhsBody = dynamic_cast< _DNBFunction1wRetBody< R, A1, Function > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        return ( f_ == rhsBody->f_ );
    }

    return false;
}

template< class R, class A1, class Function >
bool 
_DNBFunction1wRetBody< R, A1, Function >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction1wRetBody< R, A1, Function >* rhsBody = dynamic_cast< _DNBFunction1wRetBody< R, A1, Function > *  > ( &rhs );

    if ( rhsBody != NULL )
        return ( f_ < rhsBody-> f_ );
    else
        return ( (void *)this < (void *)&rhs );
}

template< class R, class A1, class Function, class M1 >
_DNBFunction1wRetM1Body< R, A1, Function, M1 >::_DNBFunction1wRetM1Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class R, class A1, class Function, class M1 >
_DNBFunction1wRetM1Body< R, A1, Function, M1 >::_DNBFunction1wRetM1Body(const Function& f, M1 m1)
    DNB_THROW_SPEC_ANY :
    f_(f),
    m1_(m1)
{
    DNB_PRECONDITION( f_ );
}

template< class R, class A1, class Function, class M1 >
_DNBFunction1wRetM1Body< R, A1, Function, M1 >::_DNBFunction1wRetM1Body(const _DNBFunction1wRetM1Body< R, A1, Function, M1 >& functionFunctor)
    DNB_THROW_SPEC_ANY :
    f_(functionFunctor.f_),
    m1_(functionFunctor.m1_)
{
    DNB_PRECONDITION( f_ );
}

template< class R, class A1, class Function, class M1 >
_DNBFunction1wRetM1Body< R, A1, Function, M1 >::~_DNBFunction1wRetM1Body()
    DNB_THROW_SPEC_ANY
{
}

template< class R, class A1, class Function, class M1>
R 
_DNBFunction1wRetM1Body< R, A1, Function, M1 >::operator()( A1 a1 ) 
    DNB_THROW_SPEC_ANY
{ 
    return f_( a1, m1_ );
}

template< class R, class A1, class Function, class M1 >
void*
_DNBFunction1wRetM1Body< R, A1, Function, M1 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return NULL;
}    
template< class R, class A1, class Function, class M1 >
void*
_DNBFunction1wRetM1Body< R, A1, Function, M1 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&f_;
}   

template< class R, class A1, class Function, class M1 >
_DNBBaseFunctor1wRetBody< R, A1 >*
_DNBFunction1wRetM1Body< R, A1, Function, M1>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBFunction1wRetM1Body< R, A1, Function, M1 >( *this );
}

template< class R, class A1, class Function, class M1 >
bool 
_DNBFunction1wRetM1Body< R, A1, Function, M1 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction1wRetM1Body< R, A1, Function, M1 >* rhsBody = dynamic_cast< _DNBFunction1wRetM1Body< R, A1, Function, M1 > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
            return ( m1_ == rhsBody->m1_ );

    }
    
    return false;
}

template< class R, class A1, class Function, class M1 >
bool 
_DNBFunction1wRetM1Body< R, A1, Function, M1 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction1wRetM1Body< R, A1, Function, M1 >* rhsBody = dynamic_cast< _DNBFunction1wRetM1Body< R, A1, Function, M1 > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
        {
            return ( m1_ < rhsBody->m1_ );

        }
        else
            return ( f_ < rhsBody->f_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class R, class A1, class Function, class M1, class M2 >
_DNBFunction1wRetM2Body< R, A1, Function, M1, M2 >::_DNBFunction1wRetM2Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class R, class A1, class Function, class M1, class M2 >
_DNBFunction1wRetM2Body< R, A1, Function, M1, M2 >::_DNBFunction1wRetM2Body(const Function& f, M1 m1, M2 m2)
    DNB_THROW_SPEC_ANY :
    f_(f),
    m1_(m1),
    m2_(m2)
{
    DNB_PRECONDITION( f_ );
}

template< class R, class A1, class Function, class M1, class M2 >
_DNBFunction1wRetM2Body< R, A1, Function, M1, M2 >::_DNBFunction1wRetM2Body(const _DNBFunction1wRetM2Body< R, A1, Function, M1, M2 >& functionFunctor)
    DNB_THROW_SPEC_ANY :
    f_(functionFunctor.f_),
    m1_(functionFunctor.m1_),
    m2_(functionFunctor.m2_)
{
    DNB_PRECONDITION( f_ );
}

template< class R, class A1, class Function, class M1, class M2 >
_DNBFunction1wRetM2Body< R, A1, Function, M1, M2 >::~_DNBFunction1wRetM2Body()
    DNB_THROW_SPEC_ANY
{
}

template< class R, class A1, class Function, class M1, class M2>
R 
_DNBFunction1wRetM2Body< R, A1, Function, M1, M2 >::operator()( A1 a1 ) 
    DNB_THROW_SPEC_ANY
{ 
    return f_( a1, m1_, m2_ );
}

template< class R, class A1, class Function, class M1, class M2 >
void*
_DNBFunction1wRetM2Body< R, A1, Function, M1, M2 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return NULL;
}    
template< class R, class A1, class Function, class M1, class M2 >
void*
_DNBFunction1wRetM2Body< R, A1, Function, M1, M2 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&f_;
}   

template< class R, class A1, class Function, class M1, class M2 >
_DNBBaseFunctor1wRetBody< R, A1 >*
_DNBFunction1wRetM2Body< R, A1, Function, M1, M2>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBFunction1wRetM2Body< R, A1, Function, M1, M2 >( *this );
}

template< class R, class A1, class Function, class M1, class M2 >
bool 
_DNBFunction1wRetM2Body< R, A1, Function, M1, M2 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction1wRetM2Body< R, A1, Function, M1, M2 >* rhsBody = dynamic_cast< _DNBFunction1wRetM2Body< R, A1, Function, M1, M2 > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
            if ( m1_ == rhsBody->m1_ )
                return ( m2_ == rhsBody->m2_ );

    }
    
    return false;
}

template< class R, class A1, class Function, class M1, class M2 >
bool 
_DNBFunction1wRetM2Body< R, A1, Function, M1, M2 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction1wRetM2Body< R, A1, Function, M1, M2 >* rhsBody = dynamic_cast< _DNBFunction1wRetM2Body< R, A1, Function, M1, M2 > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
        {
            if ( m1_ == rhsBody->m1_ )
            {
                return ( m2_ < rhsBody->m2_ );
            }
            else
                return ( m1_ < rhsBody->m1_ );

        }
        else
            return ( f_ < rhsBody->f_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class R, class A1, class Function, class M1, class M2, class M3 >
_DNBFunction1wRetM3Body< R, A1, Function, M1, M2, M3 >::_DNBFunction1wRetM3Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class R, class A1, class Function, class M1, class M2, class M3 >
_DNBFunction1wRetM3Body< R, A1, Function, M1, M2, M3 >::_DNBFunction1wRetM3Body(const Function& f, M1 m1, M2 m2, M3 m3)
    DNB_THROW_SPEC_ANY :
    f_(f),
    m1_(m1),
    m2_(m2),
    m3_(m3)
{
    DNB_PRECONDITION( f_ );
}

template< class R, class A1, class Function, class M1, class M2, class M3 >
_DNBFunction1wRetM3Body< R, A1, Function, M1, M2, M3 >::_DNBFunction1wRetM3Body(const _DNBFunction1wRetM3Body< R, A1, Function, M1, M2, M3 >& functionFunctor)
    DNB_THROW_SPEC_ANY :
    f_(functionFunctor.f_),
    m1_(functionFunctor.m1_),
    m2_(functionFunctor.m2_),
    m3_(functionFunctor.m3_)
{
    DNB_PRECONDITION( f_ );
}

template< class R, class A1, class Function, class M1, class M2, class M3 >
_DNBFunction1wRetM3Body< R, A1, Function, M1, M2, M3 >::~_DNBFunction1wRetM3Body()
    DNB_THROW_SPEC_ANY
{
}

template< class R, class A1, class Function, class M1, class M2, class M3>
R 
_DNBFunction1wRetM3Body< R, A1, Function, M1, M2, M3 >::operator()( A1 a1 ) 
    DNB_THROW_SPEC_ANY
{ 
    return f_( a1, m1_, m2_, m3_ );
}

template< class R, class A1, class Function, class M1, class M2, class M3 >
void*
_DNBFunction1wRetM3Body< R, A1, Function, M1, M2, M3 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return NULL;
}    
template< class R, class A1, class Function, class M1, class M2, class M3 >
void*
_DNBFunction1wRetM3Body< R, A1, Function, M1, M2, M3 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&f_;
}   

template< class R, class A1, class Function, class M1, class M2, class M3 >
_DNBBaseFunctor1wRetBody< R, A1 >*
_DNBFunction1wRetM3Body< R, A1, Function, M1, M2, M3>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBFunction1wRetM3Body< R, A1, Function, M1, M2, M3 >( *this );
}

template< class R, class A1, class Function, class M1, class M2, class M3 >
bool 
_DNBFunction1wRetM3Body< R, A1, Function, M1, M2, M3 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction1wRetM3Body< R, A1, Function, M1, M2, M3 >* rhsBody = dynamic_cast< _DNBFunction1wRetM3Body< R, A1, Function, M1, M2, M3 > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
            if ( m1_ == rhsBody->m1_ )
                if ( m2_ == rhsBody->m2_ )
                    return ( m3_ == rhsBody->m3_ );

    }
    
    return false;
}

template< class R, class A1, class Function, class M1, class M2, class M3 >
bool 
_DNBFunction1wRetM3Body< R, A1, Function, M1, M2, M3 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction1wRetM3Body< R, A1, Function, M1, M2, M3 >* rhsBody = dynamic_cast< _DNBFunction1wRetM3Body< R, A1, Function, M1, M2, M3 > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
        {
            if ( m1_ == rhsBody->m1_ )
            {
                if ( m2_ == rhsBody->m2_ )
                {
                    return ( m3_ < rhsBody->m3_ );
                }
                else
                    return ( m2_ < rhsBody->m2_ );
            }
            else
                return ( m1_ < rhsBody->m1_ );

        }
        else
            return ( f_ < rhsBody->f_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class R, class A1, class Function, class M1, class M2, class M3, class M4 >
_DNBFunction1wRetM4Body< R, A1, Function, M1, M2, M3, M4 >::_DNBFunction1wRetM4Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class R, class A1, class Function, class M1, class M2, class M3, class M4 >
_DNBFunction1wRetM4Body< R, A1, Function, M1, M2, M3, M4 >::_DNBFunction1wRetM4Body(const Function& f, M1 m1, M2 m2, M3 m3, M4 m4)
    DNB_THROW_SPEC_ANY :
    f_(f),
    m1_(m1),
    m2_(m2),
    m3_(m3),
    m4_(m4)
{
    DNB_PRECONDITION( f_ );
}

template< class R, class A1, class Function, class M1, class M2, class M3, class M4 >
_DNBFunction1wRetM4Body< R, A1, Function, M1, M2, M3, M4 >::_DNBFunction1wRetM4Body(const _DNBFunction1wRetM4Body< R, A1, Function, M1, M2, M3, M4 >& functionFunctor)
    DNB_THROW_SPEC_ANY :
    f_(functionFunctor.f_),
    m1_(functionFunctor.m1_),
    m2_(functionFunctor.m2_),
    m3_(functionFunctor.m3_),
    m4_(functionFunctor.m4_)
{
    DNB_PRECONDITION( f_ );
}

template< class R, class A1, class Function, class M1, class M2, class M3, class M4 >
_DNBFunction1wRetM4Body< R, A1, Function, M1, M2, M3, M4 >::~_DNBFunction1wRetM4Body()
    DNB_THROW_SPEC_ANY
{
}

template< class R, class A1, class Function, class M1, class M2, class M3, class M4>
R 
_DNBFunction1wRetM4Body< R, A1, Function, M1, M2, M3, M4 >::operator()( A1 a1 ) 
    DNB_THROW_SPEC_ANY
{ 
    return f_( a1, m1_, m2_, m3_, m4_ );
}

template< class R, class A1, class Function, class M1, class M2, class M3, class M4 >
void*
_DNBFunction1wRetM4Body< R, A1, Function, M1, M2, M3, M4 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return NULL;
}    
template< class R, class A1, class Function, class M1, class M2, class M3, class M4 >
void*
_DNBFunction1wRetM4Body< R, A1, Function, M1, M2, M3, M4 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&f_;
}   

template< class R, class A1, class Function, class M1, class M2, class M3, class M4 >
_DNBBaseFunctor1wRetBody< R, A1 >*
_DNBFunction1wRetM4Body< R, A1, Function, M1, M2, M3, M4>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBFunction1wRetM4Body< R, A1, Function, M1, M2, M3, M4 >( *this );
}

template< class R, class A1, class Function, class M1, class M2, class M3, class M4 >
bool 
_DNBFunction1wRetM4Body< R, A1, Function, M1, M2, M3, M4 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction1wRetM4Body< R, A1, Function, M1, M2, M3, M4 >* rhsBody = dynamic_cast< _DNBFunction1wRetM4Body< R, A1, Function, M1, M2, M3, M4 > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
            if ( m1_ == rhsBody->m1_ )
                if ( m2_ == rhsBody->m2_ )
                    if ( m3_ == rhsBody->m3_ )
                        return ( m4_ == rhsBody->m4_ );

    }
    
    return false;
}

template< class R, class A1, class Function, class M1, class M2, class M3, class M4 >
bool 
_DNBFunction1wRetM4Body< R, A1, Function, M1, M2, M3, M4 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction1wRetM4Body< R, A1, Function, M1, M2, M3, M4 >* rhsBody = dynamic_cast< _DNBFunction1wRetM4Body< R, A1, Function, M1, M2, M3, M4 > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
        {
            if ( m1_ == rhsBody->m1_ )
            {
                if ( m2_ == rhsBody->m2_ )
                {
                    if ( m3_ == rhsBody->m3_ )
                    {
                        return ( m4_ < rhsBody->m4_ );
                    }
                    else
                        return ( m3_ < rhsBody->m3_ );
                }
                else
                    return ( m2_ < rhsBody->m2_ );
            }
            else
                return ( m1_ < rhsBody->m1_ );

        }
        else
            return ( f_ < rhsBody->f_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class R, class A1, class Client, class Member >
_DNBMemberFunctor1wRetBody< R, A1, Client, Member >::_DNBMemberFunctor1wRetBody() 
    DNB_THROW_SPEC_ANY
{
}

template< class R, class A1, class Client, class Member >
_DNBMemberFunctor1wRetBody< R, A1, Client, Member >::_DNBMemberFunctor1wRetBody( const _DNBMemberFunctor1wRetBody< R, A1, Client, Member >& memberFunctor )
    DNB_THROW_SPEC_ANY
{
    client_ = memberFunctor.client_;
    member_ = memberFunctor.member_;
}        

template< class R, class A1, class Client, class Member >
_DNBMemberFunctor1wRetBody< R, A1, Client, Member >::_DNBMemberFunctor1wRetBody(Client* client, Member member)
    DNB_THROW_SPEC_ANY:
    client_(client), 
    member_(member)
{
    DNB_PRECONDITION( client_ );
    DNB_PRECONDITION( member_ );
}

template< class R, class A1, class Client, class Member >
_DNBMemberFunctor1wRetBody< R, A1, Client, Member >::~_DNBMemberFunctor1wRetBody()
    DNB_THROW_SPEC_ANY
{
}

template< class R, class A1, class Client, class Member >
R
_DNBMemberFunctor1wRetBody< R, A1, Client, Member >::operator()( A1 a1 )
    DNB_THROW_SPEC_ANY
{
    return (client_->*member_)( a1 );
}

template< class R, class A1, class Client, class Member >
void *
_DNBMemberFunctor1wRetBody< R, A1, Client, Member >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return (void *)client_;
}

template< class R, class A1, class Client, class Member >
void *
_DNBMemberFunctor1wRetBody< R, A1, Client, Member >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&member_;
}

template< class R, class A1, class Client, class Member >
_DNBBaseFunctor1wRetBody< R, A1 >* 
_DNBMemberFunctor1wRetBody< R, A1, Client, Member >::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return DNB_NEW _DNBMemberFunctor1wRetBody< R, A1, Client, Member >( *this );
}

template< class R, class A1, class Client, class Member >
bool 
_DNBMemberFunctor1wRetBody< R, A1, Client, Member >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor1wRetBody< R, A1, Client, Member >* rhsBody = dynamic_cast< _DNBMemberFunctor1wRetBody< R, A1, Client, Member > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
         return ( ( client_ == rhsBody->client_ ) && ( member_ == rhsBody->member_ ) );
    }

    return false;
}

template< class R, class A1, class Client, class Member >
bool 
_DNBMemberFunctor1wRetBody< R, A1, Client, Member >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor1wRetBody< R, A1, Client, Member >* rhsBody = dynamic_cast< _DNBMemberFunctor1wRetBody< R, A1, Client, Member > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
         if ( client_ == client_ )
             return ( (void *)&member_ < (void *)&(rhsBody->member_) );
         else
             return ( client_ < rhsBody->client_ );
    }
    else
        return ( (void *)this < (void *)&rhs );
}
template< class R, class A1, class Client, class Member, class M1 >
_DNBMemberFunctor1wRetM1Body< R, A1, Client, Member, M1 >::_DNBMemberFunctor1wRetM1Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class R, class A1, class Client, class Member, class M1 >
_DNBMemberFunctor1wRetM1Body< R, A1, Client, Member, M1 >::_DNBMemberFunctor1wRetM1Body(const _DNBMemberFunctor1wRetM1Body< R, A1, Client, Member, M1 >& memberFunctor)
    DNB_THROW_SPEC_ANY: 
m1_(memberFunctor.m1_)
{
   client_ = memberFunctor.client_;
    member_ = memberFunctor.member_;
}

template< class R, class A1, class Client, class Member, class M1 >
_DNBMemberFunctor1wRetM1Body< R, A1, Client, Member, M1 >::_DNBMemberFunctor1wRetM1Body(Client* client, Member member, M1 m1)
    DNB_THROW_SPEC_ANY:
client_(client), 
    member_(member),
    m1_(m1)
{
    DNB_PRECONDITION( client_ );
    DNB_PRECONDITION( member_ );
}

template< class R, class A1, class Client, class Member, class M1 >
_DNBMemberFunctor1wRetM1Body< R, A1, Client, Member, M1 >::~_DNBMemberFunctor1wRetM1Body()
    DNB_THROW_SPEC_ANY
{
}

template< class R, class A1, class Client, class Member, class M1>
R 
_DNBMemberFunctor1wRetM1Body< R, A1, Client, Member, M1 >::operator()( A1 a1 ) 
    DNB_THROW_SPEC_ANY
{ 
    return ( client_->*member_ )( a1, m1_ );
}

template< class R, class A1, class Client, class Member, class M1 >
void*
_DNBMemberFunctor1wRetM1Body< R, A1, Client, Member, M1 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return (void *)client_;
}    
template< class R, class A1, class Client, class Member, class M1 >
void*
_DNBMemberFunctor1wRetM1Body< R, A1, Client, Member, M1 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&member_;
}   

template< class R, class A1, class Client, class Member, class M1 >
_DNBBaseFunctor1wRetBody< R, A1 >*
_DNBMemberFunctor1wRetM1Body< R, A1, Client, Member, M1>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBMemberFunctor1wRetM1Body< R, A1, Client, Member, M1 >( *this );
}

template< class R, class A1, class Client, class Member, class M1 >
bool 
_DNBMemberFunctor1wRetM1Body< R, A1, Client, Member, M1 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor1wRetM1Body< R, A1, Client, Member, M1 >* rhsBody = dynamic_cast< _DNBMemberFunctor1wRetM1Body< R, A1, Client, Member, M1 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
            if ( member_ == rhsBody->member_ )
                return ( m1_ == rhsBody->m1_ );

    }

    return false;
}

template< class R, class A1, class Client, class Member, class M1 >
bool 
_DNBMemberFunctor1wRetM1Body< R, A1, Client, Member, M1 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor1wRetM1Body< R, A1, Client, Member, M1 >* rhsBody = dynamic_cast< _DNBMemberFunctor1wRetM1Body< R, A1, Client, Member, M1 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
        {
            if ( member_ == rhsBody->member_ )
            {
                return ( m1_ < rhsBody->m1_ );

            }
            else
                return ( (void *)&member_ < (void *)&(rhsBody->member_) );
        }
        else
            return ( client_ < rhsBody->client_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class R, class A1, class Client, class Member, class M1, class M2 >
_DNBMemberFunctor1wRetM2Body< R, A1, Client, Member, M1, M2 >::_DNBMemberFunctor1wRetM2Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class R, class A1, class Client, class Member, class M1, class M2 >
_DNBMemberFunctor1wRetM2Body< R, A1, Client, Member, M1, M2 >::_DNBMemberFunctor1wRetM2Body(const _DNBMemberFunctor1wRetM2Body< R, A1, Client, Member, M1, M2 >& memberFunctor)
    DNB_THROW_SPEC_ANY: 
m1_(memberFunctor.m1_),
    m2_(memberFunctor.m2_)
{
   client_ = memberFunctor.client_;
    member_ = memberFunctor.member_;
}

template< class R, class A1, class Client, class Member, class M1, class M2 >
_DNBMemberFunctor1wRetM2Body< R, A1, Client, Member, M1, M2 >::_DNBMemberFunctor1wRetM2Body(Client* client, Member member, M1 m1, M2 m2)
    DNB_THROW_SPEC_ANY:
client_(client), 
    member_(member),
    m1_(m1),
    m2_(m2)
{
    DNB_PRECONDITION( client_ );
    DNB_PRECONDITION( member_ );
}

template< class R, class A1, class Client, class Member, class M1, class M2 >
_DNBMemberFunctor1wRetM2Body< R, A1, Client, Member, M1, M2 >::~_DNBMemberFunctor1wRetM2Body()
    DNB_THROW_SPEC_ANY
{
}

template< class R, class A1, class Client, class Member, class M1, class M2>
R 
_DNBMemberFunctor1wRetM2Body< R, A1, Client, Member, M1, M2 >::operator()( A1 a1 ) 
    DNB_THROW_SPEC_ANY
{ 
    return ( client_->*member_ )( a1, m1_, m2_ );
}

template< class R, class A1, class Client, class Member, class M1, class M2 >
void*
_DNBMemberFunctor1wRetM2Body< R, A1, Client, Member, M1, M2 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return (void *)client_;
}    
template< class R, class A1, class Client, class Member, class M1, class M2 >
void*
_DNBMemberFunctor1wRetM2Body< R, A1, Client, Member, M1, M2 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&member_;
}   

template< class R, class A1, class Client, class Member, class M1, class M2 >
_DNBBaseFunctor1wRetBody< R, A1 >*
_DNBMemberFunctor1wRetM2Body< R, A1, Client, Member, M1, M2>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBMemberFunctor1wRetM2Body< R, A1, Client, Member, M1, M2 >( *this );
}

template< class R, class A1, class Client, class Member, class M1, class M2 >
bool 
_DNBMemberFunctor1wRetM2Body< R, A1, Client, Member, M1, M2 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor1wRetM2Body< R, A1, Client, Member, M1, M2 >* rhsBody = dynamic_cast< _DNBMemberFunctor1wRetM2Body< R, A1, Client, Member, M1, M2 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
            if ( member_ == rhsBody->member_ )
                if ( m1_ == rhsBody->m1_ )
                    return ( m2_ == rhsBody->m2_ );

    }

    return false;
}

template< class R, class A1, class Client, class Member, class M1, class M2 >
bool 
_DNBMemberFunctor1wRetM2Body< R, A1, Client, Member, M1, M2 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor1wRetM2Body< R, A1, Client, Member, M1, M2 >* rhsBody = dynamic_cast< _DNBMemberFunctor1wRetM2Body< R, A1, Client, Member, M1, M2 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
        {
            if ( member_ == rhsBody->member_ )
            {
                if ( m1_ == rhsBody->m1_ )
                {
                    return ( m2_ < rhsBody->m2_ );
                }
                else
                    return ( m1_ < rhsBody->m1_ );

            }
            else
                return ( (void *)&member_ < (void *)&(rhsBody->member_) );
        }
        else
            return ( client_ < rhsBody->client_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class R, class A1, class Client, class Member, class M1, class M2, class M3 >
_DNBMemberFunctor1wRetM3Body< R, A1, Client, Member, M1, M2, M3 >::_DNBMemberFunctor1wRetM3Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class R, class A1, class Client, class Member, class M1, class M2, class M3 >
_DNBMemberFunctor1wRetM3Body< R, A1, Client, Member, M1, M2, M3 >::_DNBMemberFunctor1wRetM3Body(const _DNBMemberFunctor1wRetM3Body< R, A1, Client, Member, M1, M2, M3 >& memberFunctor)
    DNB_THROW_SPEC_ANY: 
m1_(memberFunctor.m1_),
    m2_(memberFunctor.m2_),
    m3_(memberFunctor.m3_)
{
   client_ = memberFunctor.client_;
    member_ = memberFunctor.member_;
}

template< class R, class A1, class Client, class Member, class M1, class M2, class M3 >
_DNBMemberFunctor1wRetM3Body< R, A1, Client, Member, M1, M2, M3 >::_DNBMemberFunctor1wRetM3Body(Client* client, Member member, M1 m1, M2 m2, M3 m3)
    DNB_THROW_SPEC_ANY:
client_(client), 
    member_(member),
    m1_(m1),
    m2_(m2),
    m3_(m3)
{
    DNB_PRECONDITION( client_ );
    DNB_PRECONDITION( member_ );
}

template< class R, class A1, class Client, class Member, class M1, class M2, class M3 >
_DNBMemberFunctor1wRetM3Body< R, A1, Client, Member, M1, M2, M3 >::~_DNBMemberFunctor1wRetM3Body()
    DNB_THROW_SPEC_ANY
{
}

template< class R, class A1, class Client, class Member, class M1, class M2, class M3>
R 
_DNBMemberFunctor1wRetM3Body< R, A1, Client, Member, M1, M2, M3 >::operator()( A1 a1 ) 
    DNB_THROW_SPEC_ANY
{ 
    return ( client_->*member_ )( a1, m1_, m2_, m3_ );
}

template< class R, class A1, class Client, class Member, class M1, class M2, class M3 >
void*
_DNBMemberFunctor1wRetM3Body< R, A1, Client, Member, M1, M2, M3 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return (void *)client_;
}    
template< class R, class A1, class Client, class Member, class M1, class M2, class M3 >
void*
_DNBMemberFunctor1wRetM3Body< R, A1, Client, Member, M1, M2, M3 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&member_;
}   

template< class R, class A1, class Client, class Member, class M1, class M2, class M3 >
_DNBBaseFunctor1wRetBody< R, A1 >*
_DNBMemberFunctor1wRetM3Body< R, A1, Client, Member, M1, M2, M3>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBMemberFunctor1wRetM3Body< R, A1, Client, Member, M1, M2, M3 >( *this );
}

template< class R, class A1, class Client, class Member, class M1, class M2, class M3 >
bool 
_DNBMemberFunctor1wRetM3Body< R, A1, Client, Member, M1, M2, M3 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor1wRetM3Body< R, A1, Client, Member, M1, M2, M3 >* rhsBody = dynamic_cast< _DNBMemberFunctor1wRetM3Body< R, A1, Client, Member, M1, M2, M3 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
            if ( member_ == rhsBody->member_ )
                if ( m1_ == rhsBody->m1_ )
                    if ( m2_ == rhsBody->m2_ )
                        return ( m3_ == rhsBody->m3_ );

    }

    return false;
}

template< class R, class A1, class Client, class Member, class M1, class M2, class M3 >
bool 
_DNBMemberFunctor1wRetM3Body< R, A1, Client, Member, M1, M2, M3 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor1wRetM3Body< R, A1, Client, Member, M1, M2, M3 >* rhsBody = dynamic_cast< _DNBMemberFunctor1wRetM3Body< R, A1, Client, Member, M1, M2, M3 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
        {
            if ( member_ == rhsBody->member_ )
            {
                if ( m1_ == rhsBody->m1_ )
                {
                    if ( m2_ == rhsBody->m2_ )
                    {
                        return ( m3_ < rhsBody->m3_ );
                    }
                    else
                        return ( m2_ < rhsBody->m2_ );
                }
                else
                    return ( m1_ < rhsBody->m1_ );

            }
            else
                return ( (void *)&member_ < (void *)&(rhsBody->member_) );
        }
        else
            return ( client_ < rhsBody->client_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class R, class A1, class Client, class Member, class M1, class M2, class M3, class M4 >
_DNBMemberFunctor1wRetM4Body< R, A1, Client, Member, M1, M2, M3, M4 >::_DNBMemberFunctor1wRetM4Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class R, class A1, class Client, class Member, class M1, class M2, class M3, class M4 >
_DNBMemberFunctor1wRetM4Body< R, A1, Client, Member, M1, M2, M3, M4 >::_DNBMemberFunctor1wRetM4Body(const _DNBMemberFunctor1wRetM4Body< R, A1, Client, Member, M1, M2, M3, M4 >& memberFunctor)
    DNB_THROW_SPEC_ANY: 
m1_(memberFunctor.m1_),
    m2_(memberFunctor.m2_),
    m3_(memberFunctor.m3_),
    m4_(memberFunctor.m4_)
{
   client_ = memberFunctor.client_;
    member_ = memberFunctor.member_;
}

template< class R, class A1, class Client, class Member, class M1, class M2, class M3, class M4 >
_DNBMemberFunctor1wRetM4Body< R, A1, Client, Member, M1, M2, M3, M4 >::_DNBMemberFunctor1wRetM4Body(Client* client, Member member, M1 m1, M2 m2, M3 m3, M4 m4)
    DNB_THROW_SPEC_ANY:
client_(client), 
    member_(member),
    m1_(m1),
    m2_(m2),
    m3_(m3),
    m4_(m4)
{
    DNB_PRECONDITION( client_ );
    DNB_PRECONDITION( member_ );
}

template< class R, class A1, class Client, class Member, class M1, class M2, class M3, class M4 >
_DNBMemberFunctor1wRetM4Body< R, A1, Client, Member, M1, M2, M3, M4 >::~_DNBMemberFunctor1wRetM4Body()
    DNB_THROW_SPEC_ANY
{
}

template< class R, class A1, class Client, class Member, class M1, class M2, class M3, class M4>
R 
_DNBMemberFunctor1wRetM4Body< R, A1, Client, Member, M1, M2, M3, M4 >::operator()( A1 a1 ) 
    DNB_THROW_SPEC_ANY
{ 
    return ( client_->*member_ )( a1, m1_, m2_, m3_, m4_ );
}

template< class R, class A1, class Client, class Member, class M1, class M2, class M3, class M4 >
void*
_DNBMemberFunctor1wRetM4Body< R, A1, Client, Member, M1, M2, M3, M4 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return (void *)client_;
}    
template< class R, class A1, class Client, class Member, class M1, class M2, class M3, class M4 >
void*
_DNBMemberFunctor1wRetM4Body< R, A1, Client, Member, M1, M2, M3, M4 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&member_;
}   

template< class R, class A1, class Client, class Member, class M1, class M2, class M3, class M4 >
_DNBBaseFunctor1wRetBody< R, A1 >*
_DNBMemberFunctor1wRetM4Body< R, A1, Client, Member, M1, M2, M3, M4>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBMemberFunctor1wRetM4Body< R, A1, Client, Member, M1, M2, M3, M4 >( *this );
}

template< class R, class A1, class Client, class Member, class M1, class M2, class M3, class M4 >
bool 
_DNBMemberFunctor1wRetM4Body< R, A1, Client, Member, M1, M2, M3, M4 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor1wRetM4Body< R, A1, Client, Member, M1, M2, M3, M4 >* rhsBody = dynamic_cast< _DNBMemberFunctor1wRetM4Body< R, A1, Client, Member, M1, M2, M3, M4 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
            if ( member_ == rhsBody->member_ )
                if ( m1_ == rhsBody->m1_ )
                    if ( m2_ == rhsBody->m2_ )
                        if ( m3_ == rhsBody->m3_ )
                            return ( m4_ == rhsBody->m4_ );

    }

    return false;
}

template< class R, class A1, class Client, class Member, class M1, class M2, class M3, class M4 >
bool 
_DNBMemberFunctor1wRetM4Body< R, A1, Client, Member, M1, M2, M3, M4 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor1wRetM4Body< R, A1, Client, Member, M1, M2, M3, M4 >* rhsBody = dynamic_cast< _DNBMemberFunctor1wRetM4Body< R, A1, Client, Member, M1, M2, M3, M4 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
        {
            if ( member_ == rhsBody->member_ )
            {
                if ( m1_ == rhsBody->m1_ )
                {
                    if ( m2_ == rhsBody->m2_ )
                    {
                        if ( m3_ == rhsBody->m3_ )
                        {
                            return ( m4_ < rhsBody->m4_ );
                        }
                        else
                            return ( m3_ < rhsBody->m3_ );
                    }
                    else
                        return ( m2_ < rhsBody->m2_ );
                }
                else
                    return ( m1_ < rhsBody->m1_ );

            }
            else
                return ( (void *)&member_ < (void *)&(rhsBody->member_) );
        }
        else
            return ( client_ < rhsBody->client_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class R, class A1 >
DNBFunctor1wRet< R, A1 >::DNBFunctor1wRet(_DNBBaseFunctor1wRetBody< R, A1 >* body)
    DNB_THROW_SPEC_ANY: 
    body_(body) 
{
}

template< class R, class A1 >
DNBFunctor1wRet< R, A1 >::DNBFunctor1wRet(const DNBFunctor1wRet< R, A1 >& DNBFunctor)
    DNB_THROW_SPEC_ANY
{
    if ( DNBFunctor.body_) 
        body_ = (DNBFunctor.body_)->clone(); 
    else
        body_ = NULL;
}

template< class R, class A1 >
DNBFunctor1wRet< R, A1 >::~DNBFunctor1wRet() 
    DNB_THROW_SPEC_ANY
{
    if ( body_)
        DNB_DELETE body_;
}

template< class R, class A1 >
_DNBBaseFunctorBody*
DNBFunctor1wRet< R, A1 >::getBody() const
    DNB_THROW_SPEC_ANY
{
    return (_DNBBaseFunctorBody*)body_;
}

template< class R, class A1 >
R
DNBFunctor1wRet< R, A1 >::operator()( A1 a1 ) 
    DNB_THROW_SPEC_ANY
{ 
    // full name used instead of operator shorthand for Visual C++
    DNB_PRECONDITION( body_ );
     return body_->operator()( a1 ); 
} 

template< class R, class A1 >
DNBFunctor1wRet< R, A1 >& 
DNBFunctor1wRet< R, A1 >::operator=(const DNBFunctor1wRet< R, A1 >& rhs)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    if ( &rhs == this )
        return *this;

    if (body_)
    {
        DNB_DELETE body_;
        body_ = NULL;
    }

    if (rhs.body_)
        body_ = (rhs.body_)->clone();
    else
        body_ = NULL;

    return *this;
}
/////////////////////////////////////////////////////////
//
// 2 Passed,  1 Returned 
//
/////////////////////////////////////////////////////////

template< class R, class A1, class A2, class Function >
_DNBFunction2wRetBody< R, A1, A2, Function>::_DNBFunction2wRetBody()
    DNB_THROW_SPEC_ANY
{
}


template< class R, class A1, class A2, class Function >
_DNBFunction2wRetBody< R, A1, A2, Function>::_DNBFunction2wRetBody(const Function& f)
    DNB_THROW_SPEC_ANY:
    f_(f) 
{
    DNB_PRECONDITION( f_ );
}

template< class R, class A1, class A2, class Function >
_DNBFunction2wRetBody< R, A1, A2, Function>::_DNBFunction2wRetBody(const _DNBFunction2wRetBody< R, A1, A2, Function>& functionFunctor)
    DNB_THROW_SPEC_ANY:
     f_(functionFunctor.f_)
{
    DNB_PRECONDITION( f_ );
}

template< class R, class A1, class A2, class Function >
_DNBFunction2wRetBody< R, A1, A2, Function>::~_DNBFunction2wRetBody()    
DNB_THROW_SPEC_ANY
{
}

template< class R, class A1, class A2, class Function >
R
_DNBFunction2wRetBody< R, A1, A2, Function>::operator()( A1 a1, A2 a2 )
    DNB_THROW_SPEC_ANY
{
    return f_(a1, a2 );
}

template< class R, class A1, class A2, class Function >
void*
_DNBFunction2wRetBody< R, A1, A2, Function>::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return NULL;
}

template< class R, class A1, class A2, class Function >
void*
_DNBFunction2wRetBody< R, A1, A2, Function>::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&f_;
}   

template< class R, class A1, class A2, class Function >
_DNBBaseFunctor2wRetBody< R, A1, A2 >*
_DNBFunction2wRetBody< R, A1, A2, Function>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return DNB_NEW _DNBFunction2wRetBody< R, A1, A2, Function>( *this );
}

template< class R, class A1, class A2, class Function >
bool 
_DNBFunction2wRetBody< R, A1, A2, Function >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction2wRetBody< R, A1, A2, Function >* rhsBody = dynamic_cast< _DNBFunction2wRetBody< R, A1, A2, Function > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        return ( f_ == rhsBody->f_ );
    }

    return false;
}

template< class R, class A1, class A2, class Function >
bool 
_DNBFunction2wRetBody< R, A1, A2, Function >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction2wRetBody< R, A1, A2, Function >* rhsBody = dynamic_cast< _DNBFunction2wRetBody< R, A1, A2, Function > *  > ( &rhs );

    if ( rhsBody != NULL )
        return ( f_ < rhsBody-> f_ );
    else
        return ( (void *)this < (void *)&rhs );
}

template< class R, class A1, class A2, class Function, class M1 >
_DNBFunction2wRetM1Body< R, A1, A2, Function, M1 >::_DNBFunction2wRetM1Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class R, class A1, class A2, class Function, class M1 >
_DNBFunction2wRetM1Body< R, A1, A2, Function, M1 >::_DNBFunction2wRetM1Body(const Function& f, M1 m1)
    DNB_THROW_SPEC_ANY :
    f_(f),
    m1_(m1)
{
    DNB_PRECONDITION( f_ );
}

template< class R, class A1, class A2, class Function, class M1 >
_DNBFunction2wRetM1Body< R, A1, A2, Function, M1 >::_DNBFunction2wRetM1Body(const _DNBFunction2wRetM1Body< R, A1, A2, Function, M1 >& functionFunctor)
    DNB_THROW_SPEC_ANY :
    f_(functionFunctor.f_),
    m1_(functionFunctor.m1_)
{
    DNB_PRECONDITION( f_ );
}

template< class R, class A1, class A2, class Function, class M1 >
_DNBFunction2wRetM1Body< R, A1, A2, Function, M1 >::~_DNBFunction2wRetM1Body()
    DNB_THROW_SPEC_ANY
{
}

template< class R, class A1, class A2, class Function, class M1>
R 
_DNBFunction2wRetM1Body< R, A1, A2, Function, M1 >::operator()( A1 a1, A2 a2 ) 
    DNB_THROW_SPEC_ANY
{ 
    return f_( a1, a2, m1_  );
}

template< class R, class A1, class A2, class Function, class M1 >
void*
_DNBFunction2wRetM1Body< R, A1, A2, Function, M1 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return NULL;
}    
template< class R, class A1, class A2, class Function, class M1 >
void*
_DNBFunction2wRetM1Body< R, A1, A2, Function, M1 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&f_;
}   

template< class R, class A1, class A2, class Function, class M1 >
_DNBBaseFunctor2wRetBody< R, A1, A2 >*
_DNBFunction2wRetM1Body< R, A1, A2, Function, M1>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBFunction2wRetM1Body< R, A1, A2, Function, M1 >( *this );
}

template< class R, class A1, class A2, class Function, class M1 >
bool 
_DNBFunction2wRetM1Body< R, A1, A2, Function, M1 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction2wRetM1Body< R, A1, A2, Function, M1 >* rhsBody = dynamic_cast< _DNBFunction2wRetM1Body< R, A1, A2, Function, M1 > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
            return ( m1_ == rhsBody->m1_ );

    }
    
    return false;
}

template< class R, class A1, class A2, class Function, class M1 >
bool 
_DNBFunction2wRetM1Body< R, A1, A2, Function, M1 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction2wRetM1Body< R, A1, A2, Function, M1 >* rhsBody = dynamic_cast< _DNBFunction2wRetM1Body< R, A1, A2, Function, M1 > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
        {
            return ( m1_ < rhsBody->m1_ );

        }
        else
            return ( f_ < rhsBody->f_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class R, class A1, class A2, class Function, class M1, class M2 >
_DNBFunction2wRetM2Body< R, A1, A2, Function, M1, M2 >::_DNBFunction2wRetM2Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class R, class A1, class A2, class Function, class M1, class M2 >
_DNBFunction2wRetM2Body< R, A1, A2, Function, M1, M2 >::_DNBFunction2wRetM2Body(const Function& f, M1 m1, M2 m2)
    DNB_THROW_SPEC_ANY :
    f_(f),
    m1_(m1),
    m2_(m2)
{
    DNB_PRECONDITION( f_ );
}

template< class R, class A1, class A2, class Function, class M1, class M2 >
_DNBFunction2wRetM2Body< R, A1, A2, Function, M1, M2 >::_DNBFunction2wRetM2Body(const _DNBFunction2wRetM2Body< R, A1, A2, Function, M1, M2 >& functionFunctor)
    DNB_THROW_SPEC_ANY :
    f_(functionFunctor.f_),
    m1_(functionFunctor.m1_),
    m2_(functionFunctor.m2_)
{
    DNB_PRECONDITION( f_ );
}

template< class R, class A1, class A2, class Function, class M1, class M2 >
_DNBFunction2wRetM2Body< R, A1, A2, Function, M1, M2 >::~_DNBFunction2wRetM2Body()
    DNB_THROW_SPEC_ANY
{
}

template< class R, class A1, class A2, class Function, class M1, class M2>
R 
_DNBFunction2wRetM2Body< R, A1, A2, Function, M1, M2 >::operator()( A1 a1, A2 a2 ) 
    DNB_THROW_SPEC_ANY
{ 
    return f_( a1, a2, m1_, m2_  );
}

template< class R, class A1, class A2, class Function, class M1, class M2 >
void*
_DNBFunction2wRetM2Body< R, A1, A2, Function, M1, M2 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return NULL;
}    
template< class R, class A1, class A2, class Function, class M1, class M2 >
void*
_DNBFunction2wRetM2Body< R, A1, A2, Function, M1, M2 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&f_;
}   

template< class R, class A1, class A2, class Function, class M1, class M2 >
_DNBBaseFunctor2wRetBody< R, A1, A2 >*
_DNBFunction2wRetM2Body< R, A1, A2, Function, M1, M2>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBFunction2wRetM2Body< R, A1, A2, Function, M1, M2 >( *this );
}

template< class R, class A1, class A2, class Function, class M1, class M2 >
bool 
_DNBFunction2wRetM2Body< R, A1, A2, Function, M1, M2 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction2wRetM2Body< R, A1, A2, Function, M1, M2 >* rhsBody = dynamic_cast< _DNBFunction2wRetM2Body< R, A1, A2, Function, M1, M2 > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
            if ( m1_ == rhsBody->m1_ )
                return ( m2_ == rhsBody->m2_ );

    }
    
    return false;
}

template< class R, class A1, class A2, class Function, class M1, class M2 >
bool 
_DNBFunction2wRetM2Body< R, A1, A2, Function, M1, M2 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction2wRetM2Body< R, A1, A2, Function, M1, M2 >* rhsBody = dynamic_cast< _DNBFunction2wRetM2Body< R, A1, A2, Function, M1, M2 > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
        {
            if ( m1_ == rhsBody->m1_ )
            {
                return ( m2_ < rhsBody->m2_ );
            }
            else
                return ( m1_ < rhsBody->m1_ );

        }
        else
            return ( f_ < rhsBody->f_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class R, class A1, class A2, class Function, class M1, class M2, class M3 >
_DNBFunction2wRetM3Body< R, A1, A2, Function, M1, M2, M3 >::_DNBFunction2wRetM3Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class R, class A1, class A2, class Function, class M1, class M2, class M3 >
_DNBFunction2wRetM3Body< R, A1, A2, Function, M1, M2, M3 >::_DNBFunction2wRetM3Body(const Function& f, M1 m1, M2 m2, M3 m3)
    DNB_THROW_SPEC_ANY :
    f_(f),
    m1_(m1),
    m2_(m2),
    m3_(m3)
{
    DNB_PRECONDITION( f_ );
}

template< class R, class A1, class A2, class Function, class M1, class M2, class M3 >
_DNBFunction2wRetM3Body< R, A1, A2, Function, M1, M2, M3 >::_DNBFunction2wRetM3Body(const _DNBFunction2wRetM3Body< R, A1, A2, Function, M1, M2, M3 >& functionFunctor)
    DNB_THROW_SPEC_ANY :
    f_(functionFunctor.f_),
    m1_(functionFunctor.m1_),
    m2_(functionFunctor.m2_),
    m3_(functionFunctor.m3_)
{
    DNB_PRECONDITION( f_ );
}

template< class R, class A1, class A2, class Function, class M1, class M2, class M3 >
_DNBFunction2wRetM3Body< R, A1, A2, Function, M1, M2, M3 >::~_DNBFunction2wRetM3Body()
    DNB_THROW_SPEC_ANY
{
}

template< class R, class A1, class A2, class Function, class M1, class M2, class M3>
R 
_DNBFunction2wRetM3Body< R, A1, A2, Function, M1, M2, M3 >::operator()( A1 a1, A2 a2 ) 
    DNB_THROW_SPEC_ANY
{ 
    return f_( a1, a2, m1_, m2_, m3_ );
}

template< class R, class A1, class A2, class Function, class M1, class M2, class M3 >
void*
_DNBFunction2wRetM3Body< R, A1, A2, Function, M1, M2, M3 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return NULL;
}    
template< class R, class A1, class A2, class Function, class M1, class M2, class M3 >
void*
_DNBFunction2wRetM3Body< R, A1, A2, Function, M1, M2, M3 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&f_;
}   

template< class R, class A1, class A2, class Function, class M1, class M2, class M3 >
_DNBBaseFunctor2wRetBody< R, A1, A2 >*
_DNBFunction2wRetM3Body< R, A1, A2, Function, M1, M2, M3>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBFunction2wRetM3Body< R, A1, A2, Function, M1, M2, M3 >( *this );
}

template< class R, class A1, class A2, class Function, class M1, class M2, class M3 >
bool 
_DNBFunction2wRetM3Body< R, A1, A2, Function, M1, M2, M3 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction2wRetM3Body< R, A1, A2, Function, M1, M2, M3 >* rhsBody = dynamic_cast< _DNBFunction2wRetM3Body< R, A1, A2, Function, M1, M2, M3 > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
            if ( m1_ == rhsBody->m1_ )
                if ( m2_ == rhsBody->m2_ )
                    return ( m3_ == rhsBody->m3_ );

    }
    
    return false;
}

template< class R, class A1, class A2, class Function, class M1, class M2, class M3 >
bool 
_DNBFunction2wRetM3Body< R, A1, A2, Function, M1, M2, M3 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction2wRetM3Body< R, A1, A2, Function, M1, M2, M3 >* rhsBody = dynamic_cast< _DNBFunction2wRetM3Body< R, A1, A2, Function, M1, M2, M3 > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
        {
            if ( m1_ == rhsBody->m1_ )
            {
                if ( m2_ == rhsBody->m2_ )
                {
                    return ( m3_ < rhsBody->m3_ );
                }
                else
                    return ( m2_ < rhsBody->m2_ );
            }
            else
                return ( m1_ < rhsBody->m1_ );

        }
        else
            return ( f_ < rhsBody->f_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class R, class A1, class A2, class Client, class Member >
_DNBMemberFunctor2wRetBody< R, A1, A2, Client, Member >::_DNBMemberFunctor2wRetBody() 
    DNB_THROW_SPEC_ANY
{
}

template< class R, class A1, class A2, class Client, class Member >
_DNBMemberFunctor2wRetBody< R, A1, A2, Client, Member >::_DNBMemberFunctor2wRetBody( const _DNBMemberFunctor2wRetBody< R, A1, A2, Client, Member >& memberFunctor )
    DNB_THROW_SPEC_ANY
{
    client_ = memberFunctor.client_;
    member_ = memberFunctor.member_;
}        

template< class R, class A1, class A2, class Client, class Member >
_DNBMemberFunctor2wRetBody< R, A1, A2, Client, Member >::_DNBMemberFunctor2wRetBody(Client* client, Member member)
    DNB_THROW_SPEC_ANY:
    client_(client), 
    member_(member)
{
    DNB_PRECONDITION( client_ );
    DNB_PRECONDITION( member_ );
}

template< class R, class A1, class A2, class Client, class Member >
_DNBMemberFunctor2wRetBody< R, A1, A2, Client, Member >::~_DNBMemberFunctor2wRetBody()
    DNB_THROW_SPEC_ANY
{
}

template< class R, class A1, class A2, class Client, class Member >
R
_DNBMemberFunctor2wRetBody< R, A1, A2, Client, Member >::operator()( A1 a1, A2 a2 )
    DNB_THROW_SPEC_ANY
{
    return (client_->*member_)( a1, a2  );
}

template< class R, class A1, class A2, class Client, class Member >
void *
_DNBMemberFunctor2wRetBody< R, A1, A2, Client, Member >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return (void *)client_;
}

template< class R, class A1, class A2, class Client, class Member >
void *
_DNBMemberFunctor2wRetBody< R, A1, A2, Client, Member >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&member_;
}

template< class R, class A1, class A2, class Client, class Member >
_DNBBaseFunctor2wRetBody< R, A1, A2 >* 
_DNBMemberFunctor2wRetBody< R, A1, A2, Client, Member >::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return DNB_NEW _DNBMemberFunctor2wRetBody< R, A1, A2, Client, Member >( *this );
}

template< class R, class A1, class A2, class Client, class Member >
bool 
_DNBMemberFunctor2wRetBody< R, A1, A2, Client, Member >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor2wRetBody< R, A1, A2, Client, Member >* rhsBody = dynamic_cast< _DNBMemberFunctor2wRetBody< R, A1, A2, Client, Member > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
         return ( ( client_ == rhsBody->client_ ) && ( member_ == rhsBody->member_ ) );
    }

    return false;
}

template< class R, class A1, class A2, class Client, class Member >
bool 
_DNBMemberFunctor2wRetBody< R, A1, A2, Client, Member >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor2wRetBody< R, A1, A2, Client, Member >* rhsBody = dynamic_cast< _DNBMemberFunctor2wRetBody< R, A1, A2, Client, Member > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
         if ( client_ == client_ )
             return ( (void *)&member_ < (void *)&(rhsBody->member_) );
         else
             return ( client_ < rhsBody->client_ );
    }
    else
        return ( (void *)this < (void *)&rhs );
}
template< class R, class A1, class A2, class Client, class Member, class M1 >
_DNBMemberFunctor2wRetM1Body< R, A1, A2, Client, Member, M1 >::_DNBMemberFunctor2wRetM1Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class R, class A1, class A2, class Client, class Member, class M1 >
_DNBMemberFunctor2wRetM1Body< R, A1, A2, Client, Member, M1 >::_DNBMemberFunctor2wRetM1Body(const _DNBMemberFunctor2wRetM1Body< R, A1, A2, Client, Member, M1 >& memberFunctor)
    DNB_THROW_SPEC_ANY: 
m1_(memberFunctor.m1_)
{
   client_ = memberFunctor.client_;
    member_ = memberFunctor.member_;
}

template< class R, class A1, class A2, class Client, class Member, class M1 >
_DNBMemberFunctor2wRetM1Body< R, A1, A2, Client, Member, M1 >::_DNBMemberFunctor2wRetM1Body(Client* client, Member member, M1 m1)
    DNB_THROW_SPEC_ANY:
client_(client), 
    member_(member),
    m1_(m1)
{
    DNB_PRECONDITION( client_ );
    DNB_PRECONDITION( member_ );
}

template< class R, class A1, class A2, class Client, class Member, class M1 >
_DNBMemberFunctor2wRetM1Body< R, A1, A2, Client, Member, M1 >::~_DNBMemberFunctor2wRetM1Body()
    DNB_THROW_SPEC_ANY
{
}

template< class R, class A1, class A2, class Client, class Member, class M1>
R 
_DNBMemberFunctor2wRetM1Body< R, A1, A2, Client, Member, M1 >::operator()( A1 a1, A2 a2 ) 
    DNB_THROW_SPEC_ANY
{ 
    return ( client_->*member_ )( a1, a2, m1_  );
}

template< class R, class A1, class A2, class Client, class Member, class M1 >
void*
_DNBMemberFunctor2wRetM1Body< R, A1, A2, Client, Member, M1 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return (void *)client_;
}    
template< class R, class A1, class A2, class Client, class Member, class M1 >
void*
_DNBMemberFunctor2wRetM1Body< R, A1, A2, Client, Member, M1 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&member_;
}   

template< class R, class A1, class A2, class Client, class Member, class M1 >
_DNBBaseFunctor2wRetBody< R, A1, A2 >*
_DNBMemberFunctor2wRetM1Body< R, A1, A2, Client, Member, M1>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBMemberFunctor2wRetM1Body< R, A1, A2, Client, Member, M1 >( *this );
}

template< class R, class A1, class A2, class Client, class Member, class M1 >
bool 
_DNBMemberFunctor2wRetM1Body< R, A1, A2, Client, Member, M1 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor2wRetM1Body< R, A1, A2, Client, Member, M1 >* rhsBody = dynamic_cast< _DNBMemberFunctor2wRetM1Body< R, A1, A2, Client, Member, M1 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
            if ( member_ == rhsBody->member_ )
                return ( m1_ == rhsBody->m1_ );

    }

    return false;
}

template< class R, class A1, class A2, class Client, class Member, class M1 >
bool 
_DNBMemberFunctor2wRetM1Body< R, A1, A2, Client, Member, M1 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor2wRetM1Body< R, A1, A2, Client, Member, M1 >* rhsBody = dynamic_cast< _DNBMemberFunctor2wRetM1Body< R, A1, A2, Client, Member, M1 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
        {
            if ( member_ == rhsBody->member_ )
            {
                return ( m1_ < rhsBody->m1_ );

            }
            else
                return ( (void *)&member_ < (void *)&(rhsBody->member_) );
        }
        else
            return ( client_ < rhsBody->client_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class R, class A1, class A2, class Client, class Member, class M1, class M2 >
_DNBMemberFunctor2wRetM2Body< R, A1, A2, Client, Member, M1, M2 >::_DNBMemberFunctor2wRetM2Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class R, class A1, class A2, class Client, class Member, class M1, class M2 >
_DNBMemberFunctor2wRetM2Body< R, A1, A2, Client, Member, M1, M2 >::_DNBMemberFunctor2wRetM2Body(const _DNBMemberFunctor2wRetM2Body< R, A1, A2, Client, Member, M1, M2 >& memberFunctor)
    DNB_THROW_SPEC_ANY: 
m1_(memberFunctor.m1_),
    m2_(memberFunctor.m2_)
{
   client_ = memberFunctor.client_;
    member_ = memberFunctor.member_;
}

template< class R, class A1, class A2, class Client, class Member, class M1, class M2 >
_DNBMemberFunctor2wRetM2Body< R, A1, A2, Client, Member, M1, M2 >::_DNBMemberFunctor2wRetM2Body(Client* client, Member member, M1 m1, M2 m2)
    DNB_THROW_SPEC_ANY:
client_(client), 
    member_(member),
    m1_(m1),
    m2_(m2)
{
    DNB_PRECONDITION( client_ );
    DNB_PRECONDITION( member_ );
}

template< class R, class A1, class A2, class Client, class Member, class M1, class M2 >
_DNBMemberFunctor2wRetM2Body< R, A1, A2, Client, Member, M1, M2 >::~_DNBMemberFunctor2wRetM2Body()
    DNB_THROW_SPEC_ANY
{
}

template< class R, class A1, class A2, class Client, class Member, class M1, class M2>
R 
_DNBMemberFunctor2wRetM2Body< R, A1, A2, Client, Member, M1, M2 >::operator()( A1 a1, A2 a2 ) 
    DNB_THROW_SPEC_ANY
{ 
    return ( client_->*member_ )( a1, a2, m1_, m2_  );
}

template< class R, class A1, class A2, class Client, class Member, class M1, class M2 >
void*
_DNBMemberFunctor2wRetM2Body< R, A1, A2, Client, Member, M1, M2 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return (void *)client_;
}    
template< class R, class A1, class A2, class Client, class Member, class M1, class M2 >
void*
_DNBMemberFunctor2wRetM2Body< R, A1, A2, Client, Member, M1, M2 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&member_;
}   

template< class R, class A1, class A2, class Client, class Member, class M1, class M2 >
_DNBBaseFunctor2wRetBody< R, A1, A2 >*
_DNBMemberFunctor2wRetM2Body< R, A1, A2, Client, Member, M1, M2>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBMemberFunctor2wRetM2Body< R, A1, A2, Client, Member, M1, M2 >( *this );
}

template< class R, class A1, class A2, class Client, class Member, class M1, class M2 >
bool 
_DNBMemberFunctor2wRetM2Body< R, A1, A2, Client, Member, M1, M2 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor2wRetM2Body< R, A1, A2, Client, Member, M1, M2 >* rhsBody = dynamic_cast< _DNBMemberFunctor2wRetM2Body< R, A1, A2, Client, Member, M1, M2 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
            if ( member_ == rhsBody->member_ )
                if ( m1_ == rhsBody->m1_ )
                    return ( m2_ == rhsBody->m2_ );

    }

    return false;
}

template< class R, class A1, class A2, class Client, class Member, class M1, class M2 >
bool 
_DNBMemberFunctor2wRetM2Body< R, A1, A2, Client, Member, M1, M2 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor2wRetM2Body< R, A1, A2, Client, Member, M1, M2 >* rhsBody = dynamic_cast< _DNBMemberFunctor2wRetM2Body< R, A1, A2, Client, Member, M1, M2 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
        {
            if ( member_ == rhsBody->member_ )
            {
                if ( m1_ == rhsBody->m1_ )
                {
                    return ( m2_ < rhsBody->m2_ );
                }
                else
                    return ( m1_ < rhsBody->m1_ );

            }
            else
                return ( (void *)&member_ < (void *)&(rhsBody->member_) );
        }
        else
            return ( client_ < rhsBody->client_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class R, class A1, class A2, class Client, class Member, class M1, class M2, class M3 >
_DNBMemberFunctor2wRetM3Body< R, A1, A2, Client, Member, M1, M2, M3 >::_DNBMemberFunctor2wRetM3Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class R, class A1, class A2, class Client, class Member, class M1, class M2, class M3 >
_DNBMemberFunctor2wRetM3Body< R, A1, A2, Client, Member, M1, M2, M3 >::_DNBMemberFunctor2wRetM3Body(const _DNBMemberFunctor2wRetM3Body< R, A1, A2, Client, Member, M1, M2, M3 >& memberFunctor)
    DNB_THROW_SPEC_ANY: 
m1_(memberFunctor.m1_),
    m2_(memberFunctor.m2_),
    m3_(memberFunctor.m3_)
{
   client_ = memberFunctor.client_;
    member_ = memberFunctor.member_;
}

template< class R, class A1, class A2, class Client, class Member, class M1, class M2, class M3 >
_DNBMemberFunctor2wRetM3Body< R, A1, A2, Client, Member, M1, M2, M3 >::_DNBMemberFunctor2wRetM3Body(Client* client, Member member, M1 m1, M2 m2, M3 m3)
    DNB_THROW_SPEC_ANY:
client_(client), 
    member_(member),
    m1_(m1),
    m2_(m2),
    m3_(m3)
{
    DNB_PRECONDITION( client_ );
    DNB_PRECONDITION( member_ );
}

template< class R, class A1, class A2, class Client, class Member, class M1, class M2, class M3 >
_DNBMemberFunctor2wRetM3Body< R, A1, A2, Client, Member, M1, M2, M3 >::~_DNBMemberFunctor2wRetM3Body()
    DNB_THROW_SPEC_ANY
{
}

template< class R, class A1, class A2, class Client, class Member, class M1, class M2, class M3>
R 
_DNBMemberFunctor2wRetM3Body< R, A1, A2, Client, Member, M1, M2, M3 >::operator()( A1 a1, A2 a2 ) 
    DNB_THROW_SPEC_ANY
{ 
    return ( client_->*member_ )( a1, a2, m1_, m2_, m3_  );
}

template< class R, class A1, class A2, class Client, class Member, class M1, class M2, class M3 >
void*
_DNBMemberFunctor2wRetM3Body< R, A1, A2, Client, Member, M1, M2, M3 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return (void *)client_;
}    
template< class R, class A1, class A2, class Client, class Member, class M1, class M2, class M3 >
void*
_DNBMemberFunctor2wRetM3Body< R, A1, A2, Client, Member, M1, M2, M3 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&member_;
}   

template< class R, class A1, class A2, class Client, class Member, class M1, class M2, class M3 >
_DNBBaseFunctor2wRetBody< R, A1, A2 >*
_DNBMemberFunctor2wRetM3Body< R, A1, A2, Client, Member, M1, M2, M3>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBMemberFunctor2wRetM3Body< R, A1, A2, Client, Member, M1, M2, M3 >( *this );
}

template< class R, class A1, class A2, class Client, class Member, class M1, class M2, class M3 >
bool 
_DNBMemberFunctor2wRetM3Body< R, A1, A2, Client, Member, M1, M2, M3 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor2wRetM3Body< R, A1, A2, Client, Member, M1, M2, M3 >* rhsBody = dynamic_cast< _DNBMemberFunctor2wRetM3Body< R, A1, A2, Client, Member, M1, M2, M3 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
            if ( member_ == rhsBody->member_ )
                if ( m1_ == rhsBody->m1_ )
                    if ( m2_ == rhsBody->m2_ )
                        return ( m3_ == rhsBody->m3_ );

    }

    return false;
}

template< class R, class A1, class A2, class Client, class Member, class M1, class M2, class M3 >
bool 
_DNBMemberFunctor2wRetM3Body< R, A1, A2, Client, Member, M1, M2, M3 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor2wRetM3Body< R, A1, A2, Client, Member, M1, M2, M3 >* rhsBody = dynamic_cast< _DNBMemberFunctor2wRetM3Body< R, A1, A2, Client, Member, M1, M2, M3 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
        {
            if ( member_ == rhsBody->member_ )
            {
                if ( m1_ == rhsBody->m1_ )
                {
                    if ( m2_ == rhsBody->m2_ )
                    {
                        return ( m3_ < rhsBody->m3_ );
                    }
                    else
                        return ( m2_ < rhsBody->m2_ );
                }
                else
                    return ( m1_ < rhsBody->m1_ );

            }
            else
                return ( (void *)&member_ < (void *)&(rhsBody->member_) );
        }
        else
            return ( client_ < rhsBody->client_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class R, class A1, class A2 >
DNBFunctor2wRet< R, A1, A2 >::DNBFunctor2wRet(_DNBBaseFunctor2wRetBody< R, A1, A2 >* body)
    DNB_THROW_SPEC_ANY: 
    body_(body) 
{
}

template< class R, class A1, class A2 >
DNBFunctor2wRet< R, A1, A2 >::DNBFunctor2wRet(const DNBFunctor2wRet< R, A1, A2 >& DNBFunctor)
    DNB_THROW_SPEC_ANY
{
    if ( DNBFunctor.body_) 
        body_ = (DNBFunctor.body_)->clone(); 
    else
        body_ = NULL;
}

template< class R, class A1, class A2 >
DNBFunctor2wRet< R, A1, A2 >::~DNBFunctor2wRet() 
    DNB_THROW_SPEC_ANY
{
    if ( body_)
        DNB_DELETE body_;
}

template< class R, class A1, class A2 >
_DNBBaseFunctorBody*
DNBFunctor2wRet< R, A1, A2 >::getBody() const
    DNB_THROW_SPEC_ANY
{
    return (_DNBBaseFunctorBody*)body_;
}

template< class R, class A1, class A2 >
R
DNBFunctor2wRet< R, A1, A2 >::operator()( A1 a1, A2 a2 ) 
    DNB_THROW_SPEC_ANY
{ 
    // full name used instead of operator shorthand for Visual C++
    DNB_PRECONDITION( body_ );
     return body_->operator()( a1, a2  ); 
} 

template< class R, class A1, class A2 >
DNBFunctor2wRet< R, A1, A2 >& 
DNBFunctor2wRet< R, A1, A2 >::operator=(const DNBFunctor2wRet< R, A1, A2 >& rhs)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    if ( &rhs == this )
        return *this;

    if (body_)
    {
        DNB_DELETE body_;
        body_ = NULL;
    }

    if (rhs.body_)
        body_ = (rhs.body_)->clone();
    else
        body_ = NULL;

    return *this;
}
/////////////////////////////////////////////////////////
//
// 3 Passed,  1 Returned 
//
/////////////////////////////////////////////////////////

template< class R, class A1, class A2, class A3, class Function >
_DNBFunction3wRetBody< R, A1, A2, A3, Function>::_DNBFunction3wRetBody()
    DNB_THROW_SPEC_ANY
{
}


template< class R, class A1, class A2, class A3, class Function >
_DNBFunction3wRetBody< R, A1, A2, A3, Function>::_DNBFunction3wRetBody(const Function& f)
    DNB_THROW_SPEC_ANY:
    f_(f) 
{
    DNB_PRECONDITION( f_ );
}

template< class R, class A1, class A2, class A3, class Function >
_DNBFunction3wRetBody< R, A1, A2, A3, Function>::_DNBFunction3wRetBody(const _DNBFunction3wRetBody< R, A1, A2, A3, Function>& functionFunctor)
    DNB_THROW_SPEC_ANY:
     f_(functionFunctor.f_)
{
    DNB_PRECONDITION( f_ );
}

template< class R, class A1, class A2, class A3, class Function >
_DNBFunction3wRetBody< R, A1, A2, A3, Function>::~_DNBFunction3wRetBody()    
DNB_THROW_SPEC_ANY
{
}

template< class R, class A1, class A2, class A3, class Function >
R
_DNBFunction3wRetBody< R, A1, A2, A3, Function>::operator()( A1 a1, A2 a2, A3 a3 )
    DNB_THROW_SPEC_ANY
{
    return f_(a1, a2 , a3 );
}

template< class R, class A1, class A2, class A3, class Function >
void*
_DNBFunction3wRetBody< R, A1, A2, A3, Function>::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return NULL;
}

template< class R, class A1, class A2, class A3, class Function >
void*
_DNBFunction3wRetBody< R, A1, A2, A3, Function>::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&f_;
}   

template< class R, class A1, class A2, class A3, class Function >
_DNBBaseFunctor3wRetBody< R, A1, A2, A3 >*
_DNBFunction3wRetBody< R, A1, A2, A3, Function>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return DNB_NEW _DNBFunction3wRetBody< R, A1, A2, A3, Function>( *this );
}

template< class R, class A1, class A2, class A3, class Function >
bool 
_DNBFunction3wRetBody< R, A1, A2, A3, Function >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction3wRetBody< R, A1, A2, A3, Function >* rhsBody = dynamic_cast< _DNBFunction3wRetBody< R, A1, A2, A3, Function > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        return ( f_ == rhsBody->f_ );
    }

    return false;
}

template< class R, class A1, class A2, class A3, class Function >
bool 
_DNBFunction3wRetBody< R, A1, A2, A3, Function >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction3wRetBody< R, A1, A2, A3, Function >* rhsBody = dynamic_cast< _DNBFunction3wRetBody< R, A1, A2, A3, Function > *  > ( &rhs );

    if ( rhsBody != NULL )
        return ( f_ < rhsBody-> f_ );
    else
        return ( (void *)this < (void *)&rhs );
}

template< class R, class A1, class A2, class A3, class Function, class M1 >
_DNBFunction3wRetM1Body< R, A1, A2, A3, Function, M1 >::_DNBFunction3wRetM1Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class R, class A1, class A2, class A3, class Function, class M1 >
_DNBFunction3wRetM1Body< R, A1, A2, A3, Function, M1 >::_DNBFunction3wRetM1Body(const Function& f, M1 m1)
    DNB_THROW_SPEC_ANY :
    f_(f),
    m1_(m1)
{
    DNB_PRECONDITION( f_ );
}

template< class R, class A1, class A2, class A3, class Function, class M1 >
_DNBFunction3wRetM1Body< R, A1, A2, A3, Function, M1 >::_DNBFunction3wRetM1Body(const _DNBFunction3wRetM1Body< R, A1, A2, A3, Function, M1 >& functionFunctor)
    DNB_THROW_SPEC_ANY :
    f_(functionFunctor.f_),
    m1_(functionFunctor.m1_)
{
    DNB_PRECONDITION( f_ );
}

template< class R, class A1, class A2, class A3, class Function, class M1 >
_DNBFunction3wRetM1Body< R, A1, A2, A3, Function, M1 >::~_DNBFunction3wRetM1Body()
    DNB_THROW_SPEC_ANY
{
}

template< class R, class A1, class A2, class A3, class Function, class M1>
R 
_DNBFunction3wRetM1Body< R, A1, A2, A3, Function, M1 >::operator()( A1 a1, A2 a2, A3 a3 ) 
    DNB_THROW_SPEC_ANY
{ 
    return f_( a1, a2 , a3, m1_  );
}

template< class R, class A1, class A2, class A3, class Function, class M1 >
void*
_DNBFunction3wRetM1Body< R, A1, A2, A3, Function, M1 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return NULL;
}    
template< class R, class A1, class A2, class A3, class Function, class M1 >
void*
_DNBFunction3wRetM1Body< R, A1, A2, A3, Function, M1 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&f_;
}   

template< class R, class A1, class A2, class A3, class Function, class M1 >
_DNBBaseFunctor3wRetBody< R, A1, A2, A3 >*
_DNBFunction3wRetM1Body< R, A1, A2, A3, Function, M1>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBFunction3wRetM1Body< R, A1, A2, A3, Function, M1 >( *this );
}

template< class R, class A1, class A2, class A3, class Function, class M1 >
bool 
_DNBFunction3wRetM1Body< R, A1, A2, A3, Function, M1 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction3wRetM1Body< R, A1, A2, A3, Function, M1 >* rhsBody = dynamic_cast< _DNBFunction3wRetM1Body< R, A1, A2, A3, Function, M1 > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
            return ( m1_ == rhsBody->m1_ );

    }
    
    return false;
}

template< class R, class A1, class A2, class A3, class Function, class M1 >
bool 
_DNBFunction3wRetM1Body< R, A1, A2, A3, Function, M1 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction3wRetM1Body< R, A1, A2, A3, Function, M1 >* rhsBody = dynamic_cast< _DNBFunction3wRetM1Body< R, A1, A2, A3, Function, M1 > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
        {
            return ( m1_ < rhsBody->m1_ );

        }
        else
            return ( f_ < rhsBody->f_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class R, class A1, class A2, class A3, class Function, class M1, class M2 >
_DNBFunction3wRetM2Body< R, A1, A2, A3, Function, M1, M2 >::_DNBFunction3wRetM2Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class R, class A1, class A2, class A3, class Function, class M1, class M2 >
_DNBFunction3wRetM2Body< R, A1, A2, A3, Function, M1, M2 >::_DNBFunction3wRetM2Body(const Function& f, M1 m1, M2 m2)
    DNB_THROW_SPEC_ANY :
    f_(f),
    m1_(m1),
    m2_(m2)
{
    DNB_PRECONDITION( f_ );
}

template< class R, class A1, class A2, class A3, class Function, class M1, class M2 >
_DNBFunction3wRetM2Body< R, A1, A2, A3, Function, M1, M2 >::_DNBFunction3wRetM2Body(const _DNBFunction3wRetM2Body< R, A1, A2, A3, Function, M1, M2 >& functionFunctor)
    DNB_THROW_SPEC_ANY :
    f_(functionFunctor.f_),
    m1_(functionFunctor.m1_),
    m2_(functionFunctor.m2_)
{
    DNB_PRECONDITION( f_ );
}

template< class R, class A1, class A2, class A3, class Function, class M1, class M2 >
_DNBFunction3wRetM2Body< R, A1, A2, A3, Function, M1, M2 >::~_DNBFunction3wRetM2Body()
    DNB_THROW_SPEC_ANY
{
}

template< class R, class A1, class A2, class A3, class Function, class M1, class M2>
R 
_DNBFunction3wRetM2Body< R, A1, A2, A3, Function, M1, M2 >::operator()( A1 a1, A2 a2, A3 a3 ) 
    DNB_THROW_SPEC_ANY
{ 
    return f_( a1, a2 , a3, m1_, m2_  );
}

template< class R, class A1, class A2, class A3, class Function, class M1, class M2 >
void*
_DNBFunction3wRetM2Body< R, A1, A2, A3, Function, M1, M2 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return NULL;
}    
template< class R, class A1, class A2, class A3, class Function, class M1, class M2 >
void*
_DNBFunction3wRetM2Body< R, A1, A2, A3, Function, M1, M2 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&f_;
}   

template< class R, class A1, class A2, class A3, class Function, class M1, class M2 >
_DNBBaseFunctor3wRetBody< R, A1, A2, A3 >*
_DNBFunction3wRetM2Body< R, A1, A2, A3, Function, M1, M2>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBFunction3wRetM2Body< R, A1, A2, A3, Function, M1, M2 >( *this );
}

template< class R, class A1, class A2, class A3, class Function, class M1, class M2 >
bool 
_DNBFunction3wRetM2Body< R, A1, A2, A3, Function, M1, M2 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction3wRetM2Body< R, A1, A2, A3, Function, M1, M2 >* rhsBody = dynamic_cast< _DNBFunction3wRetM2Body< R, A1, A2, A3, Function, M1, M2 > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
            if ( m1_ == rhsBody->m1_ )
                return ( m2_ == rhsBody->m2_ );

    }
    
    return false;
}

template< class R, class A1, class A2, class A3, class Function, class M1, class M2 >
bool 
_DNBFunction3wRetM2Body< R, A1, A2, A3, Function, M1, M2 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction3wRetM2Body< R, A1, A2, A3, Function, M1, M2 >* rhsBody = dynamic_cast< _DNBFunction3wRetM2Body< R, A1, A2, A3, Function, M1, M2 > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
        {
            if ( m1_ == rhsBody->m1_ )
            {
                return ( m2_ < rhsBody->m2_ );
            }
            else
                return ( m1_ < rhsBody->m1_ );

        }
        else
            return ( f_ < rhsBody->f_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class R, class A1, class A2, class A3, class Client, class Member >
_DNBMemberFunctor3wRetBody< R, A1, A2, A3, Client, Member >::_DNBMemberFunctor3wRetBody() 
    DNB_THROW_SPEC_ANY
{
}

template< class R, class A1, class A2, class A3, class Client, class Member >
_DNBMemberFunctor3wRetBody< R, A1, A2, A3, Client, Member >::_DNBMemberFunctor3wRetBody( const _DNBMemberFunctor3wRetBody< R, A1, A2, A3, Client, Member >& memberFunctor )
    DNB_THROW_SPEC_ANY
{
    client_ = memberFunctor.client_;
    member_ = memberFunctor.member_;
}        

template< class R, class A1, class A2, class A3, class Client, class Member >
_DNBMemberFunctor3wRetBody< R, A1, A2, A3, Client, Member >::_DNBMemberFunctor3wRetBody(Client* client, Member member)
    DNB_THROW_SPEC_ANY:
    client_(client), 
    member_(member)
{
    DNB_PRECONDITION( client_ );
    DNB_PRECONDITION( member_ );
}

template< class R, class A1, class A2, class A3, class Client, class Member >
_DNBMemberFunctor3wRetBody< R, A1, A2, A3, Client, Member >::~_DNBMemberFunctor3wRetBody()
    DNB_THROW_SPEC_ANY
{
}

template< class R, class A1, class A2, class A3, class Client, class Member >
R
_DNBMemberFunctor3wRetBody< R, A1, A2, A3, Client, Member >::operator()( A1 a1, A2 a2, A3 a3 )
    DNB_THROW_SPEC_ANY
{
    return (client_->*member_)( a1, a2 , a3  );
}

template< class R, class A1, class A2, class A3, class Client, class Member >
void *
_DNBMemberFunctor3wRetBody< R, A1, A2, A3, Client, Member >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return (void *)client_;
}

template< class R, class A1, class A2, class A3, class Client, class Member >
void *
_DNBMemberFunctor3wRetBody< R, A1, A2, A3, Client, Member >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&member_;
}

template< class R, class A1, class A2, class A3, class Client, class Member >
_DNBBaseFunctor3wRetBody< R, A1, A2, A3 >* 
_DNBMemberFunctor3wRetBody< R, A1, A2, A3, Client, Member >::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return DNB_NEW _DNBMemberFunctor3wRetBody< R, A1, A2, A3, Client, Member >( *this );
}

template< class R, class A1, class A2, class A3, class Client, class Member >
bool 
_DNBMemberFunctor3wRetBody< R, A1, A2, A3, Client, Member >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor3wRetBody< R, A1, A2, A3, Client, Member >* rhsBody = dynamic_cast< _DNBMemberFunctor3wRetBody< R, A1, A2, A3, Client, Member > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
         return ( ( client_ == rhsBody->client_ ) && ( member_ == rhsBody->member_ ) );
    }

    return false;
}

template< class R, class A1, class A2, class A3, class Client, class Member >
bool 
_DNBMemberFunctor3wRetBody< R, A1, A2, A3, Client, Member >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor3wRetBody< R, A1, A2, A3, Client, Member >* rhsBody = dynamic_cast< _DNBMemberFunctor3wRetBody< R, A1, A2, A3, Client, Member > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
         if ( client_ == client_ )
             return ( (void *)&member_ < (void *)&(rhsBody->member_) );
         else
             return ( client_ < rhsBody->client_ );
    }
    else
        return ( (void *)this < (void *)&rhs );
}
template< class R, class A1, class A2, class A3, class Client, class Member, class M1 >
_DNBMemberFunctor3wRetM1Body< R, A1, A2, A3, Client, Member, M1 >::_DNBMemberFunctor3wRetM1Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class R, class A1, class A2, class A3, class Client, class Member, class M1 >
_DNBMemberFunctor3wRetM1Body< R, A1, A2, A3, Client, Member, M1 >::_DNBMemberFunctor3wRetM1Body(const _DNBMemberFunctor3wRetM1Body< R, A1, A2, A3, Client, Member, M1 >& memberFunctor)
    DNB_THROW_SPEC_ANY: 
m1_(memberFunctor.m1_)
{
   client_ = memberFunctor.client_;
    member_ = memberFunctor.member_;
}

template< class R, class A1, class A2, class A3, class Client, class Member, class M1 >
_DNBMemberFunctor3wRetM1Body< R, A1, A2, A3, Client, Member, M1 >::_DNBMemberFunctor3wRetM1Body(Client* client, Member member, M1 m1)
    DNB_THROW_SPEC_ANY:
client_(client), 
    member_(member),
    m1_(m1)
{
    DNB_PRECONDITION( client_ );
    DNB_PRECONDITION( member_ );
}

template< class R, class A1, class A2, class A3, class Client, class Member, class M1 >
_DNBMemberFunctor3wRetM1Body< R, A1, A2, A3, Client, Member, M1 >::~_DNBMemberFunctor3wRetM1Body()
    DNB_THROW_SPEC_ANY
{
}

template< class R, class A1, class A2, class A3, class Client, class Member, class M1>
R 
_DNBMemberFunctor3wRetM1Body< R, A1, A2, A3, Client, Member, M1 >::operator()( A1 a1, A2 a2, A3 a3 ) 
    DNB_THROW_SPEC_ANY
{ 
    return ( client_->*member_ )( a1, a2 , a3, m1_  );
}

template< class R, class A1, class A2, class A3, class Client, class Member, class M1 >
void*
_DNBMemberFunctor3wRetM1Body< R, A1, A2, A3, Client, Member, M1 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return (void *)client_;
}    
template< class R, class A1, class A2, class A3, class Client, class Member, class M1 >
void*
_DNBMemberFunctor3wRetM1Body< R, A1, A2, A3, Client, Member, M1 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&member_;
}   

template< class R, class A1, class A2, class A3, class Client, class Member, class M1 >
_DNBBaseFunctor3wRetBody< R, A1, A2, A3 >*
_DNBMemberFunctor3wRetM1Body< R, A1, A2, A3, Client, Member, M1>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBMemberFunctor3wRetM1Body< R, A1, A2, A3, Client, Member, M1 >( *this );
}

template< class R, class A1, class A2, class A3, class Client, class Member, class M1 >
bool 
_DNBMemberFunctor3wRetM1Body< R, A1, A2, A3, Client, Member, M1 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor3wRetM1Body< R, A1, A2, A3, Client, Member, M1 >* rhsBody = dynamic_cast< _DNBMemberFunctor3wRetM1Body< R, A1, A2, A3, Client, Member, M1 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
            if ( member_ == rhsBody->member_ )
                return ( m1_ == rhsBody->m1_ );

    }

    return false;
}

template< class R, class A1, class A2, class A3, class Client, class Member, class M1 >
bool 
_DNBMemberFunctor3wRetM1Body< R, A1, A2, A3, Client, Member, M1 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor3wRetM1Body< R, A1, A2, A3, Client, Member, M1 >* rhsBody = dynamic_cast< _DNBMemberFunctor3wRetM1Body< R, A1, A2, A3, Client, Member, M1 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
        {
            if ( member_ == rhsBody->member_ )
            {
                return ( m1_ < rhsBody->m1_ );

            }
            else
                return ( (void *)&member_ < (void *)&(rhsBody->member_) );
        }
        else
            return ( client_ < rhsBody->client_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class R, class A1, class A2, class A3, class Client, class Member, class M1, class M2 >
_DNBMemberFunctor3wRetM2Body< R, A1, A2, A3, Client, Member, M1, M2 >::_DNBMemberFunctor3wRetM2Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class R, class A1, class A2, class A3, class Client, class Member, class M1, class M2 >
_DNBMemberFunctor3wRetM2Body< R, A1, A2, A3, Client, Member, M1, M2 >::_DNBMemberFunctor3wRetM2Body(const _DNBMemberFunctor3wRetM2Body< R, A1, A2, A3, Client, Member, M1, M2 >& memberFunctor)
    DNB_THROW_SPEC_ANY: 
m1_(memberFunctor.m1_),
    m2_(memberFunctor.m2_)
{
   client_ = memberFunctor.client_;
    member_ = memberFunctor.member_;
}

template< class R, class A1, class A2, class A3, class Client, class Member, class M1, class M2 >
_DNBMemberFunctor3wRetM2Body< R, A1, A2, A3, Client, Member, M1, M2 >::_DNBMemberFunctor3wRetM2Body(Client* client, Member member, M1 m1, M2 m2)
    DNB_THROW_SPEC_ANY:
client_(client), 
    member_(member),
    m1_(m1),
    m2_(m2)
{
    DNB_PRECONDITION( client_ );
    DNB_PRECONDITION( member_ );
}

template< class R, class A1, class A2, class A3, class Client, class Member, class M1, class M2 >
_DNBMemberFunctor3wRetM2Body< R, A1, A2, A3, Client, Member, M1, M2 >::~_DNBMemberFunctor3wRetM2Body()
    DNB_THROW_SPEC_ANY
{
}

template< class R, class A1, class A2, class A3, class Client, class Member, class M1, class M2>
R 
_DNBMemberFunctor3wRetM2Body< R, A1, A2, A3, Client, Member, M1, M2 >::operator()( A1 a1, A2 a2, A3 a3 ) 
    DNB_THROW_SPEC_ANY
{ 
    return ( client_->*member_ )( a1, a2 , a3, m1_, m2_  );
}

template< class R, class A1, class A2, class A3, class Client, class Member, class M1, class M2 >
void*
_DNBMemberFunctor3wRetM2Body< R, A1, A2, A3, Client, Member, M1, M2 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return (void *)client_;
}    
template< class R, class A1, class A2, class A3, class Client, class Member, class M1, class M2 >
void*
_DNBMemberFunctor3wRetM2Body< R, A1, A2, A3, Client, Member, M1, M2 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&member_;
}   

template< class R, class A1, class A2, class A3, class Client, class Member, class M1, class M2 >
_DNBBaseFunctor3wRetBody< R, A1, A2, A3 >*
_DNBMemberFunctor3wRetM2Body< R, A1, A2, A3, Client, Member, M1, M2>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBMemberFunctor3wRetM2Body< R, A1, A2, A3, Client, Member, M1, M2 >( *this );
}

template< class R, class A1, class A2, class A3, class Client, class Member, class M1, class M2 >
bool 
_DNBMemberFunctor3wRetM2Body< R, A1, A2, A3, Client, Member, M1, M2 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor3wRetM2Body< R, A1, A2, A3, Client, Member, M1, M2 >* rhsBody = dynamic_cast< _DNBMemberFunctor3wRetM2Body< R, A1, A2, A3, Client, Member, M1, M2 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
            if ( member_ == rhsBody->member_ )
                if ( m1_ == rhsBody->m1_ )
                    return ( m2_ == rhsBody->m2_ );

    }

    return false;
}

template< class R, class A1, class A2, class A3, class Client, class Member, class M1, class M2 >
bool 
_DNBMemberFunctor3wRetM2Body< R, A1, A2, A3, Client, Member, M1, M2 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor3wRetM2Body< R, A1, A2, A3, Client, Member, M1, M2 >* rhsBody = dynamic_cast< _DNBMemberFunctor3wRetM2Body< R, A1, A2, A3, Client, Member, M1, M2 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
        {
            if ( member_ == rhsBody->member_ )
            {
                if ( m1_ == rhsBody->m1_ )
                {
                    return ( m2_ < rhsBody->m2_ );
                }
                else
                    return ( m1_ < rhsBody->m1_ );

            }
            else
                return ( (void *)&member_ < (void *)&(rhsBody->member_) );
        }
        else
            return ( client_ < rhsBody->client_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class R, class A1, class A2, class A3 >
DNBFunctor3wRet< R, A1, A2, A3 >::DNBFunctor3wRet(_DNBBaseFunctor3wRetBody< R, A1, A2, A3 >* body)
    DNB_THROW_SPEC_ANY: 
    body_(body) 
{
}

template< class R, class A1, class A2, class A3 >
DNBFunctor3wRet< R, A1, A2, A3 >::DNBFunctor3wRet(const DNBFunctor3wRet< R, A1, A2, A3 >& DNBFunctor)
    DNB_THROW_SPEC_ANY
{
    if ( DNBFunctor.body_) 
        body_ = (DNBFunctor.body_)->clone(); 
    else
        body_ = NULL;
}

template< class R, class A1, class A2, class A3 >
DNBFunctor3wRet< R, A1, A2, A3 >::~DNBFunctor3wRet() 
    DNB_THROW_SPEC_ANY
{
    if ( body_)
        DNB_DELETE body_;
}

template< class R, class A1, class A2, class A3 >
_DNBBaseFunctorBody*
DNBFunctor3wRet< R, A1, A2, A3 >::getBody() const
    DNB_THROW_SPEC_ANY
{
    return (_DNBBaseFunctorBody*)body_;
}

template< class R, class A1, class A2, class A3 >
R
DNBFunctor3wRet< R, A1, A2, A3 >::operator()( A1 a1, A2 a2, A3 a3 ) 
    DNB_THROW_SPEC_ANY
{ 
    // full name used instead of operator shorthand for Visual C++
    DNB_PRECONDITION( body_ );
     return body_->operator()( a1, a2 , a3  ); 
} 

template< class R, class A1, class A2, class A3 >
DNBFunctor3wRet< R, A1, A2, A3 >& 
DNBFunctor3wRet< R, A1, A2, A3 >::operator=(const DNBFunctor3wRet< R, A1, A2, A3 >& rhs)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    if ( &rhs == this )
        return *this;

    if (body_)
    {
        DNB_DELETE body_;
        body_ = NULL;
    }

    if (rhs.body_)
        body_ = (rhs.body_)->clone();
    else
        body_ = NULL;

    return *this;
}
/////////////////////////////////////////////////////////
//
// 4 Passed,  1 Returned 
//
/////////////////////////////////////////////////////////

template< class R, class A1, class A2, class A3, class A4, class Function >
_DNBFunction4wRetBody< R, A1, A2, A3, A4, Function>::_DNBFunction4wRetBody()
    DNB_THROW_SPEC_ANY
{
}


template< class R, class A1, class A2, class A3, class A4, class Function >
_DNBFunction4wRetBody< R, A1, A2, A3, A4, Function>::_DNBFunction4wRetBody(const Function& f)
    DNB_THROW_SPEC_ANY:
    f_(f) 
{
    DNB_PRECONDITION( f_ );
}

template< class R, class A1, class A2, class A3, class A4, class Function >
_DNBFunction4wRetBody< R, A1, A2, A3, A4, Function>::_DNBFunction4wRetBody(const _DNBFunction4wRetBody< R, A1, A2, A3, A4, Function>& functionFunctor)
    DNB_THROW_SPEC_ANY:
     f_(functionFunctor.f_)
{
    DNB_PRECONDITION( f_ );
}

template< class R, class A1, class A2, class A3, class A4, class Function >
_DNBFunction4wRetBody< R, A1, A2, A3, A4, Function>::~_DNBFunction4wRetBody()    
DNB_THROW_SPEC_ANY
{
}

template< class R, class A1, class A2, class A3, class A4, class Function >
R
_DNBFunction4wRetBody< R, A1, A2, A3, A4, Function>::operator()( A1 a1, A2 a2, A3 a3, A4 a4 )
    DNB_THROW_SPEC_ANY
{
    return f_(a1, a2 , a3 , a4 );
}

template< class R, class A1, class A2, class A3, class A4, class Function >
void*
_DNBFunction4wRetBody< R, A1, A2, A3, A4, Function>::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return NULL;
}

template< class R, class A1, class A2, class A3, class A4, class Function >
void*
_DNBFunction4wRetBody< R, A1, A2, A3, A4, Function>::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&f_;
}   

template< class R, class A1, class A2, class A3, class A4, class Function >
_DNBBaseFunctor4wRetBody< R, A1, A2, A3, A4 >*
_DNBFunction4wRetBody< R, A1, A2, A3, A4, Function>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return DNB_NEW _DNBFunction4wRetBody< R, A1, A2, A3, A4, Function>( *this );
}

template< class R, class A1, class A2, class A3, class A4, class Function >
bool 
_DNBFunction4wRetBody< R, A1, A2, A3, A4, Function >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction4wRetBody< R, A1, A2, A3, A4, Function >* rhsBody = dynamic_cast< _DNBFunction4wRetBody< R, A1, A2, A3, A4, Function > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        return ( f_ == rhsBody->f_ );
    }

    return false;
}

template< class R, class A1, class A2, class A3, class A4, class Function >
bool 
_DNBFunction4wRetBody< R, A1, A2, A3, A4, Function >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction4wRetBody< R, A1, A2, A3, A4, Function >* rhsBody = dynamic_cast< _DNBFunction4wRetBody< R, A1, A2, A3, A4, Function > *  > ( &rhs );

    if ( rhsBody != NULL )
        return ( f_ < rhsBody-> f_ );
    else
        return ( (void *)this < (void *)&rhs );
}

template< class R, class A1, class A2, class A3, class A4, class Function, class M1 >
_DNBFunction4wRetM1Body< R, A1, A2, A3, A4, Function, M1 >::_DNBFunction4wRetM1Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class R, class A1, class A2, class A3, class A4, class Function, class M1 >
_DNBFunction4wRetM1Body< R, A1, A2, A3, A4, Function, M1 >::_DNBFunction4wRetM1Body(const Function& f, M1 m1)
    DNB_THROW_SPEC_ANY :
    f_(f),
    m1_(m1)
{
    DNB_PRECONDITION( f_ );
}

template< class R, class A1, class A2, class A3, class A4, class Function, class M1 >
_DNBFunction4wRetM1Body< R, A1, A2, A3, A4, Function, M1 >::_DNBFunction4wRetM1Body(const _DNBFunction4wRetM1Body< R, A1, A2, A3, A4, Function, M1 >& functionFunctor)
    DNB_THROW_SPEC_ANY :
    f_(functionFunctor.f_),
    m1_(functionFunctor.m1_)
{
    DNB_PRECONDITION( f_ );
}

template< class R, class A1, class A2, class A3, class A4, class Function, class M1 >
_DNBFunction4wRetM1Body< R, A1, A2, A3, A4, Function, M1 >::~_DNBFunction4wRetM1Body()
    DNB_THROW_SPEC_ANY
{
}

template< class R, class A1, class A2, class A3, class A4, class Function, class M1>
R 
_DNBFunction4wRetM1Body< R, A1, A2, A3, A4, Function, M1 >::operator()( A1 a1, A2 a2, A3 a3, A4 a4 ) 
    DNB_THROW_SPEC_ANY
{ 
    return f_( a1, a2 , a3 , a4, m1_  );
}

template< class R, class A1, class A2, class A3, class A4, class Function, class M1 >
void*
_DNBFunction4wRetM1Body< R, A1, A2, A3, A4, Function, M1 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return NULL;
}    
template< class R, class A1, class A2, class A3, class A4, class Function, class M1 >
void*
_DNBFunction4wRetM1Body< R, A1, A2, A3, A4, Function, M1 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&f_;
}   

template< class R, class A1, class A2, class A3, class A4, class Function, class M1 >
_DNBBaseFunctor4wRetBody< R, A1, A2, A3, A4 >*
_DNBFunction4wRetM1Body< R, A1, A2, A3, A4, Function, M1>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBFunction4wRetM1Body< R, A1, A2, A3, A4, Function, M1 >( *this );
}

template< class R, class A1, class A2, class A3, class A4, class Function, class M1 >
bool 
_DNBFunction4wRetM1Body< R, A1, A2, A3, A4, Function, M1 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction4wRetM1Body< R, A1, A2, A3, A4, Function, M1 >* rhsBody = dynamic_cast< _DNBFunction4wRetM1Body< R, A1, A2, A3, A4, Function, M1 > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
            return ( m1_ == rhsBody->m1_ );

    }
    
    return false;
}

template< class R, class A1, class A2, class A3, class A4, class Function, class M1 >
bool 
_DNBFunction4wRetM1Body< R, A1, A2, A3, A4, Function, M1 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction4wRetM1Body< R, A1, A2, A3, A4, Function, M1 >* rhsBody = dynamic_cast< _DNBFunction4wRetM1Body< R, A1, A2, A3, A4, Function, M1 > *  > ( &rhs ); 

    if ( rhsBody != NULL )
    {
        if ( f_ == rhsBody->f_ )
        {
            return ( m1_ < rhsBody->m1_ );

        }
        else
            return ( f_ < rhsBody->f_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class R, class A1, class A2, class A3, class A4, class Client, class Member >
_DNBMemberFunctor4wRetBody< R, A1, A2, A3, A4, Client, Member >::_DNBMemberFunctor4wRetBody() 
    DNB_THROW_SPEC_ANY
{
}

template< class R, class A1, class A2, class A3, class A4, class Client, class Member >
_DNBMemberFunctor4wRetBody< R, A1, A2, A3, A4, Client, Member >::_DNBMemberFunctor4wRetBody( const _DNBMemberFunctor4wRetBody< R, A1, A2, A3, A4, Client, Member >& memberFunctor )
    DNB_THROW_SPEC_ANY
{
    client_ = memberFunctor.client_;
    member_ = memberFunctor.member_;
}        

template< class R, class A1, class A2, class A3, class A4, class Client, class Member >
_DNBMemberFunctor4wRetBody< R, A1, A2, A3, A4, Client, Member >::_DNBMemberFunctor4wRetBody(Client* client, Member member)
    DNB_THROW_SPEC_ANY:
    client_(client), 
    member_(member)
{
    DNB_PRECONDITION( client_ );
    DNB_PRECONDITION( member_ );
}

template< class R, class A1, class A2, class A3, class A4, class Client, class Member >
_DNBMemberFunctor4wRetBody< R, A1, A2, A3, A4, Client, Member >::~_DNBMemberFunctor4wRetBody()
    DNB_THROW_SPEC_ANY
{
}

template< class R, class A1, class A2, class A3, class A4, class Client, class Member >
R
_DNBMemberFunctor4wRetBody< R, A1, A2, A3, A4, Client, Member >::operator()( A1 a1, A2 a2, A3 a3, A4 a4 )
    DNB_THROW_SPEC_ANY
{
    return (client_->*member_)( a1, a2 , a3 , a4  );
}

template< class R, class A1, class A2, class A3, class A4, class Client, class Member >
void *
_DNBMemberFunctor4wRetBody< R, A1, A2, A3, A4, Client, Member >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return (void *)client_;
}

template< class R, class A1, class A2, class A3, class A4, class Client, class Member >
void *
_DNBMemberFunctor4wRetBody< R, A1, A2, A3, A4, Client, Member >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&member_;
}

template< class R, class A1, class A2, class A3, class A4, class Client, class Member >
_DNBBaseFunctor4wRetBody< R, A1, A2, A3, A4 >* 
_DNBMemberFunctor4wRetBody< R, A1, A2, A3, A4, Client, Member >::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return DNB_NEW _DNBMemberFunctor4wRetBody< R, A1, A2, A3, A4, Client, Member >( *this );
}

template< class R, class A1, class A2, class A3, class A4, class Client, class Member >
bool 
_DNBMemberFunctor4wRetBody< R, A1, A2, A3, A4, Client, Member >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor4wRetBody< R, A1, A2, A3, A4, Client, Member >* rhsBody = dynamic_cast< _DNBMemberFunctor4wRetBody< R, A1, A2, A3, A4, Client, Member > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
         return ( ( client_ == rhsBody->client_ ) && ( member_ == rhsBody->member_ ) );
    }

    return false;
}

template< class R, class A1, class A2, class A3, class A4, class Client, class Member >
bool 
_DNBMemberFunctor4wRetBody< R, A1, A2, A3, A4, Client, Member >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor4wRetBody< R, A1, A2, A3, A4, Client, Member >* rhsBody = dynamic_cast< _DNBMemberFunctor4wRetBody< R, A1, A2, A3, A4, Client, Member > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
         if ( client_ == client_ )
             return ( (void *)&member_ < (void *)&(rhsBody->member_) );
         else
             return ( client_ < rhsBody->client_ );
    }
    else
        return ( (void *)this < (void *)&rhs );
}
template< class R, class A1, class A2, class A3, class A4, class Client, class Member, class M1 >
_DNBMemberFunctor4wRetM1Body< R, A1, A2, A3, A4, Client, Member, M1 >::_DNBMemberFunctor4wRetM1Body() 
    DNB_THROW_SPEC_ANY
{
}

template< class R, class A1, class A2, class A3, class A4, class Client, class Member, class M1 >
_DNBMemberFunctor4wRetM1Body< R, A1, A2, A3, A4, Client, Member, M1 >::_DNBMemberFunctor4wRetM1Body(const _DNBMemberFunctor4wRetM1Body< R, A1, A2, A3, A4, Client, Member, M1 >& memberFunctor)
    DNB_THROW_SPEC_ANY: 
m1_(memberFunctor.m1_)
{
   client_ = memberFunctor.client_;
    member_ = memberFunctor.member_;
}

template< class R, class A1, class A2, class A3, class A4, class Client, class Member, class M1 >
_DNBMemberFunctor4wRetM1Body< R, A1, A2, A3, A4, Client, Member, M1 >::_DNBMemberFunctor4wRetM1Body(Client* client, Member member, M1 m1)
    DNB_THROW_SPEC_ANY:
client_(client), 
    member_(member),
    m1_(m1)
{
    DNB_PRECONDITION( client_ );
    DNB_PRECONDITION( member_ );
}

template< class R, class A1, class A2, class A3, class A4, class Client, class Member, class M1 >
_DNBMemberFunctor4wRetM1Body< R, A1, A2, A3, A4, Client, Member, M1 >::~_DNBMemberFunctor4wRetM1Body()
    DNB_THROW_SPEC_ANY
{
}

template< class R, class A1, class A2, class A3, class A4, class Client, class Member, class M1>
R 
_DNBMemberFunctor4wRetM1Body< R, A1, A2, A3, A4, Client, Member, M1 >::operator()( A1 a1, A2 a2, A3 a3, A4 a4 ) 
    DNB_THROW_SPEC_ANY
{ 
    return ( client_->*member_ )( a1, a2 , a3 , a4, m1_  );
}

template< class R, class A1, class A2, class A3, class A4, class Client, class Member, class M1 >
void*
_DNBMemberFunctor4wRetM1Body< R, A1, A2, A3, A4, Client, Member, M1 >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return (void *)client_;
}    
template< class R, class A1, class A2, class A3, class A4, class Client, class Member, class M1 >
void*
_DNBMemberFunctor4wRetM1Body< R, A1, A2, A3, A4, Client, Member, M1 >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&member_;
}   

template< class R, class A1, class A2, class A3, class A4, class Client, class Member, class M1 >
_DNBBaseFunctor4wRetBody< R, A1, A2, A3, A4 >*
_DNBMemberFunctor4wRetM1Body< R, A1, A2, A3, A4, Client, Member, M1>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return new _DNBMemberFunctor4wRetM1Body< R, A1, A2, A3, A4, Client, Member, M1 >( *this );
}

template< class R, class A1, class A2, class A3, class A4, class Client, class Member, class M1 >
bool 
_DNBMemberFunctor4wRetM1Body< R, A1, A2, A3, A4, Client, Member, M1 >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor4wRetM1Body< R, A1, A2, A3, A4, Client, Member, M1 >* rhsBody = dynamic_cast< _DNBMemberFunctor4wRetM1Body< R, A1, A2, A3, A4, Client, Member, M1 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
            if ( member_ == rhsBody->member_ )
                return ( m1_ == rhsBody->m1_ );

    }

    return false;
}

template< class R, class A1, class A2, class A3, class A4, class Client, class Member, class M1 >
bool 
_DNBMemberFunctor4wRetM1Body< R, A1, A2, A3, A4, Client, Member, M1 >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor4wRetM1Body< R, A1, A2, A3, A4, Client, Member, M1 >* rhsBody = dynamic_cast< _DNBMemberFunctor4wRetM1Body< R, A1, A2, A3, A4, Client, Member, M1 > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        if ( client_ == rhsBody->client_ )
        {
            if ( member_ == rhsBody->member_ )
            {
                return ( m1_ < rhsBody->m1_ );

            }
            else
                return ( (void *)&member_ < (void *)&(rhsBody->member_) );
        }
        else
            return ( client_ < rhsBody->client_ );
    }

    return ( (void *)this < (void *)&rhs );
}

template< class R, class A1, class A2, class A3, class A4 >
DNBFunctor4wRet< R, A1, A2, A3, A4 >::DNBFunctor4wRet(_DNBBaseFunctor4wRetBody< R, A1, A2, A3, A4 >* body)
    DNB_THROW_SPEC_ANY: 
    body_(body) 
{
}

template< class R, class A1, class A2, class A3, class A4 >
DNBFunctor4wRet< R, A1, A2, A3, A4 >::DNBFunctor4wRet(const DNBFunctor4wRet< R, A1, A2, A3, A4 >& DNBFunctor)
    DNB_THROW_SPEC_ANY
{
    if ( DNBFunctor.body_) 
        body_ = (DNBFunctor.body_)->clone(); 
    else
        body_ = NULL;
}

template< class R, class A1, class A2, class A3, class A4 >
DNBFunctor4wRet< R, A1, A2, A3, A4 >::~DNBFunctor4wRet() 
    DNB_THROW_SPEC_ANY
{
    if ( body_)
        DNB_DELETE body_;
}

template< class R, class A1, class A2, class A3, class A4 >
_DNBBaseFunctorBody*
DNBFunctor4wRet< R, A1, A2, A3, A4 >::getBody() const
    DNB_THROW_SPEC_ANY
{
    return (_DNBBaseFunctorBody*)body_;
}

template< class R, class A1, class A2, class A3, class A4 >
R
DNBFunctor4wRet< R, A1, A2, A3, A4 >::operator()( A1 a1, A2 a2, A3 a3, A4 a4 ) 
    DNB_THROW_SPEC_ANY
{ 
    // full name used instead of operator shorthand for Visual C++
    DNB_PRECONDITION( body_ );
     return body_->operator()( a1, a2 , a3 , a4  ); 
} 

template< class R, class A1, class A2, class A3, class A4 >
DNBFunctor4wRet< R, A1, A2, A3, A4 >& 
DNBFunctor4wRet< R, A1, A2, A3, A4 >::operator=(const DNBFunctor4wRet< R, A1, A2, A3, A4 >& rhs)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    if ( &rhs == this )
        return *this;

    if (body_)
    {
        DNB_DELETE body_;
        body_ = NULL;
    }

    if (rhs.body_)
        body_ = (rhs.body_)->clone();
    else
        body_ = NULL;

    return *this;
}
/////////////////////////////////////////////////////////
//
// 5 Passed,  1 Returned 
//
/////////////////////////////////////////////////////////

template< class R, class A1, class A2, class A3, class A4, class A5, class Function >
_DNBFunction5wRetBody< R, A1, A2, A3, A4, A5, Function>::_DNBFunction5wRetBody()
    DNB_THROW_SPEC_ANY
{
}


template< class R, class A1, class A2, class A3, class A4, class A5, class Function >
_DNBFunction5wRetBody< R, A1, A2, A3, A4, A5, Function>::_DNBFunction5wRetBody(const Function& f)
    DNB_THROW_SPEC_ANY:
    f_(f) 
{
    DNB_PRECONDITION( f_ );
}

template< class R, class A1, class A2, class A3, class A4, class A5, class Function >
_DNBFunction5wRetBody< R, A1, A2, A3, A4, A5, Function>::_DNBFunction5wRetBody(const _DNBFunction5wRetBody< R, A1, A2, A3, A4, A5, Function>& functionFunctor)
    DNB_THROW_SPEC_ANY:
     f_(functionFunctor.f_)
{
    DNB_PRECONDITION( f_ );
}

template< class R, class A1, class A2, class A3, class A4, class A5, class Function >
_DNBFunction5wRetBody< R, A1, A2, A3, A4, A5, Function>::~_DNBFunction5wRetBody()    
DNB_THROW_SPEC_ANY
{
}

template< class R, class A1, class A2, class A3, class A4, class A5, class Function >
R
_DNBFunction5wRetBody< R, A1, A2, A3, A4, A5, Function>::operator()( A1 a1, A2 a2, A3 a3, A4 a4, A5 a5 )
    DNB_THROW_SPEC_ANY
{
    return f_(a1, a2 , a3 , a4 , a5 );
}

template< class R, class A1, class A2, class A3, class A4, class A5, class Function >
void*
_DNBFunction5wRetBody< R, A1, A2, A3, A4, A5, Function>::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return NULL;
}

template< class R, class A1, class A2, class A3, class A4, class A5, class Function >
void*
_DNBFunction5wRetBody< R, A1, A2, A3, A4, A5, Function>::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&f_;
}   

template< class R, class A1, class A2, class A3, class A4, class A5, class Function >
_DNBBaseFunctor5wRetBody< R, A1, A2, A3, A4, A5 >*
_DNBFunction5wRetBody< R, A1, A2, A3, A4, A5, Function>::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return DNB_NEW _DNBFunction5wRetBody< R, A1, A2, A3, A4, A5, Function>( *this );
}

template< class R, class A1, class A2, class A3, class A4, class A5, class Function >
bool 
_DNBFunction5wRetBody< R, A1, A2, A3, A4, A5, Function >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction5wRetBody< R, A1, A2, A3, A4, A5, Function >* rhsBody = dynamic_cast< _DNBFunction5wRetBody< R, A1, A2, A3, A4, A5, Function > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
        return ( f_ == rhsBody->f_ );
    }

    return false;
}

template< class R, class A1, class A2, class A3, class A4, class A5, class Function >
bool 
_DNBFunction5wRetBody< R, A1, A2, A3, A4, A5, Function >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBFunction5wRetBody< R, A1, A2, A3, A4, A5, Function >* rhsBody = dynamic_cast< _DNBFunction5wRetBody< R, A1, A2, A3, A4, A5, Function > *  > ( &rhs );

    if ( rhsBody != NULL )
        return ( f_ < rhsBody-> f_ );
    else
        return ( (void *)this < (void *)&rhs );
}

template< class R, class A1, class A2, class A3, class A4, class A5, class Client, class Member >
_DNBMemberFunctor5wRetBody< R, A1, A2, A3, A4, A5, Client, Member >::_DNBMemberFunctor5wRetBody() 
    DNB_THROW_SPEC_ANY
{
}

template< class R, class A1, class A2, class A3, class A4, class A5, class Client, class Member >
_DNBMemberFunctor5wRetBody< R, A1, A2, A3, A4, A5, Client, Member >::_DNBMemberFunctor5wRetBody( const _DNBMemberFunctor5wRetBody< R, A1, A2, A3, A4, A5, Client, Member >& memberFunctor )
    DNB_THROW_SPEC_ANY
{
    client_ = memberFunctor.client_;
    member_ = memberFunctor.member_;
}        

template< class R, class A1, class A2, class A3, class A4, class A5, class Client, class Member >
_DNBMemberFunctor5wRetBody< R, A1, A2, A3, A4, A5, Client, Member >::_DNBMemberFunctor5wRetBody(Client* client, Member member)
    DNB_THROW_SPEC_ANY:
    client_(client), 
    member_(member)
{
    DNB_PRECONDITION( client_ );
    DNB_PRECONDITION( member_ );
}

template< class R, class A1, class A2, class A3, class A4, class A5, class Client, class Member >
_DNBMemberFunctor5wRetBody< R, A1, A2, A3, A4, A5, Client, Member >::~_DNBMemberFunctor5wRetBody()
    DNB_THROW_SPEC_ANY
{
}

template< class R, class A1, class A2, class A3, class A4, class A5, class Client, class Member >
R
_DNBMemberFunctor5wRetBody< R, A1, A2, A3, A4, A5, Client, Member >::operator()( A1 a1, A2 a2, A3 a3, A4 a4, A5 a5 )
    DNB_THROW_SPEC_ANY
{
    return (client_->*member_)( a1, a2 , a3 , a4 , a5  );
}

template< class R, class A1, class A2, class A3, class A4, class A5, class Client, class Member >
void *
_DNBMemberFunctor5wRetBody< R, A1, A2, A3, A4, A5, Client, Member >::getCallee() const
    DNB_THROW_SPEC_ANY
{
    return (void *)client_;
}

template< class R, class A1, class A2, class A3, class A4, class A5, class Client, class Member >
void *
_DNBMemberFunctor5wRetBody< R, A1, A2, A3, A4, A5, Client, Member >::getFunc() const
    DNB_THROW_SPEC_ANY
{
    return (void *)&member_;
}

template< class R, class A1, class A2, class A3, class A4, class A5, class Client, class Member >
_DNBBaseFunctor5wRetBody< R, A1, A2, A3, A4, A5 >* 
_DNBMemberFunctor5wRetBody< R, A1, A2, A3, A4, A5, Client, Member >::clone()
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    return DNB_NEW _DNBMemberFunctor5wRetBody< R, A1, A2, A3, A4, A5, Client, Member >( *this );
}

template< class R, class A1, class A2, class A3, class A4, class A5, class Client, class Member >
bool 
_DNBMemberFunctor5wRetBody< R, A1, A2, A3, A4, A5, Client, Member >::IsEqual( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor5wRetBody< R, A1, A2, A3, A4, A5, Client, Member >* rhsBody = dynamic_cast< _DNBMemberFunctor5wRetBody< R, A1, A2, A3, A4, A5, Client, Member > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
         return ( ( client_ == rhsBody->client_ ) && ( member_ == rhsBody->member_ ) );
    }

    return false;
}

template< class R, class A1, class A2, class A3, class A4, class A5, class Client, class Member >
bool 
_DNBMemberFunctor5wRetBody< R, A1, A2, A3, A4, A5, Client, Member >::IsLessThan( _DNBBaseFunctorBody& rhs ) const
    DNB_THROW_SPEC_ANY
{
    _DNBMemberFunctor5wRetBody< R, A1, A2, A3, A4, A5, Client, Member >* rhsBody = dynamic_cast< _DNBMemberFunctor5wRetBody< R, A1, A2, A3, A4, A5, Client, Member > *  > ( &rhs );

    if ( rhsBody != NULL )
    {
         if ( client_ == client_ )
             return ( (void *)&member_ < (void *)&(rhsBody->member_) );
         else
             return ( client_ < rhsBody->client_ );
    }
    else
        return ( (void *)this < (void *)&rhs );
}
template< class R, class A1, class A2, class A3, class A4, class A5 >
DNBFunctor5wRet< R, A1, A2, A3, A4, A5 >::DNBFunctor5wRet(_DNBBaseFunctor5wRetBody< R, A1, A2, A3, A4, A5 >* body)
    DNB_THROW_SPEC_ANY: 
    body_(body) 
{
}

template< class R, class A1, class A2, class A3, class A4, class A5 >
DNBFunctor5wRet< R, A1, A2, A3, A4, A5 >::DNBFunctor5wRet(const DNBFunctor5wRet< R, A1, A2, A3, A4, A5 >& DNBFunctor)
    DNB_THROW_SPEC_ANY
{
    if ( DNBFunctor.body_) 
        body_ = (DNBFunctor.body_)->clone(); 
    else
        body_ = NULL;
}

template< class R, class A1, class A2, class A3, class A4, class A5 >
DNBFunctor5wRet< R, A1, A2, A3, A4, A5 >::~DNBFunctor5wRet() 
    DNB_THROW_SPEC_ANY
{
    if ( body_)
        DNB_DELETE body_;
}

template< class R, class A1, class A2, class A3, class A4, class A5 >
_DNBBaseFunctorBody*
DNBFunctor5wRet< R, A1, A2, A3, A4, A5 >::getBody() const
    DNB_THROW_SPEC_ANY
{
    return (_DNBBaseFunctorBody*)body_;
}

template< class R, class A1, class A2, class A3, class A4, class A5 >
R
DNBFunctor5wRet< R, A1, A2, A3, A4, A5 >::operator()( A1 a1, A2 a2, A3 a3, A4 a4, A5 a5 ) 
    DNB_THROW_SPEC_ANY
{ 
    // full name used instead of operator shorthand for Visual C++
    DNB_PRECONDITION( body_ );
     return body_->operator()( a1, a2 , a3 , a4 , a5  ); 
} 

template< class R, class A1, class A2, class A3, class A4, class A5 >
DNBFunctor5wRet< R, A1, A2, A3, A4, A5 >& 
DNBFunctor5wRet< R, A1, A2, A3, A4, A5 >::operator=(const DNBFunctor5wRet< R, A1, A2, A3, A4, A5 >& rhs)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    if ( &rhs == this )
        return *this;

    if (body_)
    {
        DNB_DELETE body_;
        body_ = NULL;
    }

    if (rhs.body_)
        body_ = (rhs.body_)->clone();
    else
        body_ = NULL;

    return *this;
}
