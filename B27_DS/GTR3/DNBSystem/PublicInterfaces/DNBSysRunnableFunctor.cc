//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/


//==============================================================================
//
// DNBSysRunnableFunctor.cc
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     smw        07/13/2004   Initial Implementation
//
//==============================================================================


// Global Make Functions for convenience of creating handles to runnables from global and member functions

template < class functionType >
DNBSysIRunnable 
DNBMakeRunnableFunctorG( functionType function )
{
	DNBFunctor0 functor = DNBMakeFunctorG ( (DNBFunctor0 *)0, function );
	DNBSysIRunnable runnable ( new DNBSysRunnableFunctorBody ( functor ) );
	return ( runnable );
};


template < class functionType, class arg1Type >
DNBSysIRunnable 
DNBMakeRunnableFunctorG( functionType function, arg1Type arg1 )
{
	DNBFunctor0 functor = DNBMakeFunctorG ( (DNBFunctor0 *)0, function, arg1 );
	DNBSysIRunnable runnable ( new DNBSysRunnableFunctorBody ( functor ) );
	return ( runnable );
};


template < class functionType, class arg1Type, class arg2Type>
DNBSysIRunnable 
DNBMakeRunnableFunctorG( functionType function, arg1Type arg1 , arg2Type arg2 )
{
	// FIXME 
};


template < class functionType, class arg1Type, class arg2Typpe, class arg3Type >
DNBSysIRunnable 
DNBMakeRunnableFunctorG( functionType function, arg1Type arg1, arg2Typpe arg2, arg3Type arg3 )
{
	// FIXME
};


template < class functionType, class arg1Type, class arg2Typpe, class arg3Type, class arg4Type >
DNBSysIRunnable 
DNBMakeRunnableFunctorG( functionType function, arg1Type arg1, arg2Typpe arg2, arg3Type arg3, arg4Type arg4 )
{
	// FIXME
};


template < class functionType, class arg1Type, class arg2Typpe, class arg3Type, \
			class arg4Type,
			class arg5Type >
DNBSysIRunnable
DNBMakeRunnableFunctorG( functionType function, arg1Type arg1, arg2Typpe arg2, arg3Type arg3, \
						arg4Type arg4,
						arg5Type arg5 )
{
	// FIXME
};


// FIXME - Repeat for member functions

