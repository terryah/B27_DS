//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 */

//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     JOD         08/18/1997  Initial implementation.
//*     JOD         05/12/1998  Introduced a new naming scheme for exception
//*                             classes (i.e., "DNBExxx").
//*     JOD         08/04/1998  Introduced a mechanism to define and declare
//*                             exception classes.
//*     JOD         12/10/1998  Changed the exception specifications on
//*                             DNBException::what() and DNBException::where().
//*     BPL         01/05/2001  Added RW Tracing
//*     JOD         05/25/2001  Revised several comments.
//*     SHA         01/24/2002  Replaced DNB_THROW_SPEC_NULL with throw() 
//*                             on destructor/what() to comply with 
//*                             std::exception declaration.
//*     RTL         03/19/2002  Code change for reading exception messages from catalog :)
//*     BKH         10/30/2003  Implementation of new documentation style.
//*     RTL         11/06/2003  Code change for using the new Trace facility
//*     RTL         04/28/2005  Undo reading exception messages from catalog :(
//*     RTL         05/19/2005  Provide clone() and raise() methods on all DNBException
//*                             objects
//*
//* 
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNBException_H_
#define _DNBException_H_


#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>


#include <DNBSystem.h>
#include <DNBMessage.h>
// #include <DNBSystemTrace.h>  // smw; caa; try if really needed
#include <scl_exception.h>



//
//  Instrumentation of exception classes
//
//      The DELMIA exception classes are instrumented using the DNBTraceLog class
//  DNBSystemTrace.  
//  Any exception class that is created using the macros DNB_DECLARE_EXCEPTION and
//  DNB_DEFINE_EXCEPTION will output diagnostic information to the reporting stream.
//  Trace messages are generated at a single level:
//  An INFO level message is generated when an exception has been
//  thrown.  Moreover, two other INFO level messages are used to report the values
//  of the what() and where() strings.
//
//  Example: Trace level 7
//  INFO|<file>:<line>|DNBEInvalidFormat|0x2ff1c8f0> An exception was thrown
//  DEBUG|<file>:<line>|DNBEInvalidFormat|0x2ff1c8f0> what = The left hand side
//      of an equals sign evaluates to something other than a symbol
//  DEBUG|<file>:<line>:DNBEInvalidFormat|0x2ff1c8f0> where = <file>(<line>)
//
//  Control:
//      The trace level of compiled code is controlled by the environment variable
//  DNB_EXCEPT_TRACE_LEVEL, which is defined in DNBSystemBase.h.  In a debug
//  build, this macro is set to level 7 (DEBUG).  In a release build, it is set
//  to 0 (no trace).
//
//




/**
  * Declares a new localized exception class.
  * @param _LIB
  * The symbolic name of the library that exports the exception class
  * (e.g., DNBSystem).
  * @param _NAME
  * The name of the exception class (e.g., DNBENumericError).
  * @param _BASE
  * The base class of the exception class (e.g., DNBERuntimeError).
  * 
  * <br><B>Description</B><br>
  * This macro provides the class definition for a new localized exception
  * class.  It is generally used in a public header file, such as
  * <tt><tt>DNBException.h</tt></tt>.  The name of the exception class is given by
  * <tt>_NAME</tt>, while its base class is given by <tt>_BASE</tt>.  The parameter <tt>_LIB</tt>
  * specifies the library in which the exception class is defined.
  * 
  * 
  */
#define DNB_DECLARE_EXCEPTION(_LIB, _NAME, _BASE)                              \
    class ExportedBy ## _LIB _NAME : public _BASE                              \
    {                                                                          \
    public:                                                                    \
        _NAME( const CharType *formatStr, const char *srcFile, int srcLine )   \
            DNB_THROW_SPEC_NULL;                                               \
			                                                                   \
		virtual DNBMsgData *                                                   \
		clone( ) const                                                         \
		DNB_THROW_SPEC( (scl_bad_alloc) );                                     \
		                                                                       \
	    virtual void                                                           \
		raise() const;                                                         \
    }


/**
  * Defines a new localized exception class.
  * @param _LIB
  * The symbolic name of the library that exports the exception class
  * (e.g., DNBSystem).
  * @param _NAME
  * The name of the exception class (e.g., DNBENumericError).
  * @param _BASE
  * The base class of the exception class (e.g., DNBERuntimeError).
  * 
  * <br><B>Description</B><br>
  * This macro provides the implementation of a new localized exception
  * class.  It is generally used in a principal implementation file, such
  * as <tt><tt>DNBException.cpp</tt></tt>.  The name of the exception class is given by
  * <tt>_NAME</tt>, while its base class is given by <tt>_BASE</tt>.  The parameter <tt>_LIB</tt>
  * specifies the library in which the exception class is defined.
  * 
  * 
  */
#define DNB_DEFINE_EXCEPTION(_LIB, _NAME, _BASE)                                       \
    _NAME::_NAME( const CharType *formatStr, const char *srcFile, int srcLine )        \
        DNB_THROW_SPEC_NULL : _BASE( formatStr, srcFile, srcLine )                     \
    {                                                                                  \
        DNBSystemTrace.SetFormat(scl_string("%m(%p) %F:%l"));                          \
        DNB_TRACE_INFO2(DNBSystemTrace,"\n\tException ", #_NAME," has been created."); \
        DNBSystemTrace.SetFormat(scl_string(" "));                                     \
        DNB_TRACE_INFO1(DNBSystemTrace,"\tReason : ",what());                          \
        DNB_TRACE_INFO1(DNBSystemTrace, "\tLocation: ",where() );                      \
	}                                                                                  \
		                                                                               \
	DNBMsgData *                                                                       \
	_NAME::clone( ) const                                                              \
	DNB_THROW_SPEC( (scl_bad_alloc) )                                                  \
	{                                                                                  \
		return DNB_NEW _NAME( *this );                                                 \
	}                                                                                  \
                                                                                       \
	void                                                                               \
	_NAME::raise() const                                                               \
	{                                                                                  \
		throw _NAME(*this);                                                            \
	}


/**
  * The base class of all localized exceptions.
  * 
  * <br><B>Description</B><br>
  * This class represents the base class of the DELMIA exception hierarchy.
  * It defines the primary interface for the types of objects thrown as
  * exceptions by the DELMIA toolkit.  Such objects are used to report error
  * conditions that are detected during program execution.  As with standard
  * exceptions, the object's type indicates the general nature of the error
  * (e.g., domain error), while an embedded message describes the specific
  * cause of the error (e.g., "negative argument in sqrt").  The reporting
  * mechanism uses the localized message facility implemented in the class
  * @href DNBMessage.  It should be emphasized that the set of exception
  * classes is not complete; additional classes will be defined as the need
  * arises.  To ensure some level of consistency in the DELMIA exception
  * hierarchy, please submit requests for new exception classes to the
  * @href dionise@delmia.com
  * 
  * By convention, every exception that can be thrown directly or indirectly
  * by a DELMIA function must be derived from the class @href DNBException.
  * The only exception to this rule is that a function may throw an object
  * of class <tt>scl_bad_alloc</tt>, if insufficient memory is available.  A localized
  * exception cannot be used to report a memory allocation error, because it
  * requires dynamic storage for proper construction.
  * 
  * The following list describes the basic procedure for creating and
  * throwing a localized exception.  These steps must be performed whenever
  * an error condition is detected in a DELMIA function.
  * 
  * <UL>
  * <LI> Decide whether the error condition represents a logic error or a
  * runtime error.  In general, logic errors are due to errors in the
  * internal logic of the program, while runtime errors are due to
  * events beyond the scope of the program.
  * <LI> Select the localized exception class which characterizes the
  * general nature of the error condition.  If the error condition
  * represents a logic error, the exception class must be derived from
  * @href DNBELogicError.  If the error condition represents a runtime
  * error, the exception class must be derived from
  * @href DNBERuntimeError.
  * <LI> Define an instance of the exception class selected above.  If
  * possible, the exception object should be named <tt>error</tt>.
  * <LI> Define a message which describes the specific cause of the error.
  * The message must consist of a default format string and a set of
  * data elements.  When composing the message, it is important to
  * keep in mind that logic errors are typically caught and handled in
  * the calling code, while runtime errors are typically propagated to
  * the graphical user interface.  For this reason, runtime errors
  * should be described in a style and format that is appropriate for
  * the end-user.
  * <LI> Specify the format string in the constructor of the exception
  * class using the macro @href DNB_FORMAT.
  * <LI> Add the data elements to the exception object using the inserter
  * operator, <tt><<</tt>.
  * <LI> Throw the exception object using the keyword <tt>throw</tt>.
  * </UL>
  * 
  * Once a localized exception is thrown, it can be caught using a typical
  * catch clause:
  * <pre>
  * catch ( DNBException &error )
  * {
  * cerr << error.scribe( locale ) << endl;
  * }
  * </pre>
  * 
  * In the above code fragment, the error message is formatted using the
  * specified locale, and then directed to standard error.  To maintain
  * compatibility with the Standard C++ Library, all localized exceptions
  * are derived from the class <tt>exception</tt>.  This allows the Standard C++
  * Library to catch any exceptions that may be thrown from the DELMIA
  * toolkit.
  * 
  * <br><B>Example</B><br>
  * <pre>
  * double
  * DNBSqrt( double arg )
  * DNB_THROW_SPEC((DNBEDomainError))
  * {
  * if ( arg < 0.0 )
  * {
  * DNBEDomainError error( DNB_FORMAT("Negative Argument: %1") );
  * error << DNBSetPrecision( 6 ) << arg;
  * throw error;
  * }
  * 
  * return sqrt( arg );
  * }
  * 
  * </pre>
  *
  */
class ExportedByDNBSystem DNBException : public scl_exception, public DNBMessage
{
public:
/**
  * Constructs a localized exception.
  * @param formatStr
  * A string literal which specifies the format string for the
  * default locale.  This literal must be defined with the macro
  * @href DNB_FORMAT.  Note: In all Deneb applications, the default
  * locale is "English (USA)".  When developing code for such
  * applications, the format string must consist entirely of ASCII
  * characters.
  * @param srcFile
  * The name of the source file which contains the definition of the
  * current exception.  This parameter is automatically supplied by
  * the macro @href DNB_FORMAT.
  * @param srcLine
  * The line number of the source file which contains the definition
  * of the current exception.  This parameter is automatically
  * supplied by the macro @href DNB_FORMAT.
  * 
  * @return
  * Nothing
  * 
  * <br><B>Description</B><br>
  * This function constructs a localized exception with the format
  * string initialized to <tt>formatStr</tt>.  The resulting object has zero
  * data elements.  The parameters <tt>srcFile</tt> and <tt>srcLine</tt> specify the
  * location at which the error condition was detected.
  * 
  * 
  */
    DNBException( const CharType *formatStr, const char *srcFile, int srcLine )
        DNB_THROW_SPEC_NULL;


/**
  * Constructs a localized exception using copy semantics.
  * @param right
  * The localized exception to be copied.
  * 
  * @return
  * Nothing
  * 
  * <br><B>Description</B><br>
  * This function copies the format string and data elements of <tt>right</tt>
  * to <tt>self</tt>.  The function employs deep-copy semantics.
  * @exception scl_bad_alloc
  * The object could not be created because of insufficient free
  * memory.
  * 
  * 
  */
    DNBException( const DNBException &right )
        DNB_THROW_SPEC((scl_bad_alloc));


/**
  * Destroys a localized exception.
  * 
  * @return
  * Nothing
  * 
  * <br><B>Description</B><br>
  * This function destroys a localized exception.
  * 
  * 
  */
    virtual
    ~DNBException( )
        throw();

/**
  * Copies a localized exception to <tt>self</tt>.
  * @param right
  * The localized exception to be copied.
  * 
  * @return
  * Nothing
  * 
  * <br><B>Description</B><br>
  * This function copies the format string and data elements of <tt>right</tt>
  * to <tt>self</tt>.  The function employs deep-copy semantics.
  * @exception scl_bad_alloc
  * The object could not be modified because of insufficient free
  * memory.
  * 
  * 
  */
    void
    operator=( const DNBException &right )
        DNB_THROW_SPEC((scl_bad_alloc));


/**
  * Creates a copy of <tt>self</tt> on the free store.
  * 
  * @return
  * A pointer to the newly created object.
  * 
  * <br><B>Description</B><br>
  * This function implements a virtual copy constructor.  It is used by
  * the message library to create copies of localized messages.
  * @exception scl_bad_alloc
  * The object could not be created because of insufficient free
  * memory.
  * 
  * 
  */
    virtual DNBMsgData *
    clone( ) const
        DNB_THROW_SPEC((scl_bad_alloc));

/**
  * Raises the exception by thworwing <tt>self</tt> 
  * 
  * <br><B>Description</B><br>
  *	Raises the exception by thworwing <tt>self</tt> 
  *
  */	
	virtual void
		raise( ) const;


/**
  * Retrieves the diagnostic message associated with <tt>self</tt>.
  * 
  * @return
  * The pointer to a single-byte character string which specifies the
  * cause of the error condition.
  * 
  * <br><B>Description</B><br>
  * This function converts the localized message embedded in <tt>self</tt> to a
  * string, using the formatting conventions of the global locale (see
  * @href DNBLocale::global).  It is used to obtain the diagnostic
  * message associated with a localized exception.
  * 
  * 
  */
    virtual const char *
    what( ) const
        throw();


/**
  * Retrieves the location of the error condition associated with
  * <tt>self</tt>.
  * 
  * @return
  * The pointer to a single-byte character string which specifies the
  * location of the error condition.
  * 
  * <br><B>Description</B><br>
  * This function returns the source file and line number of the source
  * code statement that detected the error condition associated with
  * <tt>self</tt>.  The location is expressed in the form "filename(lineno)".
  * 
  * 
  */
    virtual const char *
    where( ) const
        DNB_THROW_SPEC_NULL;

/**
  * Returns the srcFile of the error condition associated with 
  * <tt>self</tt>.
  * 
  * @return
  * The pointer to a single-byte character string which specifies the
  * location of the error condition.
  * 
  * <br><B>Description</B><br>
  * This function returns the source file of the source
  * code statement that detected the error condition associated with
  * <tt>self</tt>.  
  * 
  * 
  */	
	const char *
	getSrcFile()
		DNB_THROW_SPEC_ANY;
	
/**
  * Returns the line number of the source code the error condition 
  * occured
  * 
  * @return
  * Line number of the source code
  * 
  * <br><B>Description</B><br>
  * This function returns the line number of the source
  * code statement that detected the error condition associated with
  * <tt>self</tt>.  
  * 
  * 
  */	
	int
	getSrcLine()
		DNB_THROW_SPEC_ANY;


private:
    const char     *srcFile_;
    int             srcLine_;
    mutable scl_string  whatStr_;
    mutable scl_string  whereStr_;
};




/**
  * The localized exception class which describes a logic error.
  * 
  * <br><B>Description</B><br>
  * This class is used to report logic errors in the DELMIA toolkit.  The
  * distinguishing characteristic of logic errors is that they are due to
  * errors in the internal logic of the program.  In theory, such errors
  * are preventable.
  * 
  * The following table specifies the exception classes that are associated
  * with logic errors.  As expected, each class is derived from
  * @href DNBELogicError.
  *  
  * <LI>[DNBEDomainError]
  * An input parameter violates a domain constraint.
  * <LI>[DNBEInvalidArgument]
  * An input parameter is invalid.
  * <LI>[DNBELengthError]
  * An attempt is made to produce an object whose length exceeds the
  * maximum allowable size.
  * <LI>[DNBEOutOfRange]
  * An input parameter violates a range constraint.
  * <LI>[DNBENullPointer]
  * An input parameter specifies a null pointer.
  * 
  * 
  * 
  */
class ExportedByDNBSystem DNBELogicError : public DNBException
{
public:
/**
  * Constructs a localized exception.
  * @param formatStr
  * A string literal which specifies the format string for the
  * default locale.  This literal must be defined with the macro
  * @href DNB_FORMAT.
  * @param srcFile
  * The name of the source file which contains the definition of the
  * current exception.  This parameter is automatically supplied by
  * the macro @href DNB_FORMAT.
  * @param srcLine
  * The line number of the source file which contains the definition
  * of the current exception.  This parameter is automatically
  * supplied by the macro @href DNB_FORMAT.
  * 
  * @return
  * Nothing
  * 
  * <br><B>Description</B><br>
  * This function constructs a localized exception with the format
  * string initialized to <tt>formatStr</tt>.  The resulting object has zero
  * data elements.  The parameters <tt>srcFile</tt> and <tt>srcLine</tt> specify the
  * location at which the error condition was detected.
  * 
  * 
  */
    DNBELogicError( const CharType *formatStr, const char *srcFile,
        int srcLine )
        DNB_THROW_SPEC_NULL;

/**
  * Creates a copy of <tt>self</tt> on the free store.
  * 
  * @return
  * A pointer to the newly created object.
  * 
  * <br><B>Description</B><br>
  * This function implements a virtual copy constructor.  It is used by
  * the message library to create copies of localized messages.
  * @exception scl_bad_alloc
  * The object could not be created because of insufficient free
  * memory.
  * 
  * 
  */
    virtual DNBMsgData *
    clone( ) const
        DNB_THROW_SPEC((scl_bad_alloc));

/**
  * Raises the exception by thworwing <tt>self</tt> 
  * 
  * <br><B>Description</B><br>
  *	Raises the exception by thworwing <tt>self</tt> 
  *
  */	
	virtual void
		raise( ) const;
};




//
//  Define the exception classes that are used to report logic errors.  Note:
//  These classes must be documented in the comment block for DNBELogicError.
//
DNB_DECLARE_EXCEPTION( DNBSystem, DNBEDomainError    , DNBELogicError );
DNB_DECLARE_EXCEPTION( DNBSystem, DNBEInvalidArgument, DNBELogicError );
DNB_DECLARE_EXCEPTION( DNBSystem, DNBELengthError    , DNBELogicError );
DNB_DECLARE_EXCEPTION( DNBSystem, DNBEOutOfRange     , DNBELogicError );
DNB_DECLARE_EXCEPTION( DNBSystem, DNBENullPointer    , DNBELogicError );




/**
  * The localized exception class which describes a runtime error.
  * 
  * <br><B>Description</B><br>
  * This class is used to report runtime errors in the DELMIA toolkit.  The
  * distinguishing characteristic of runtime errors is that they are due to
  * events beyond the scope of the program.  In general, such errors cannot
  * be predicted in advance.
  * 
  * The following tables specify exception classes that are associated
  * with runtime errors.  As expected, each class is derived from
  * @href DNBERuntimeError.
  * 
  * {\large 1.0 General Errors}
  * 
  * <LI>[DNBEInternalError]
  * An internal inconsistency has been detected.
  * <LI>[DNBEInvalidName]
  * The specified name is invalid.
  * <LI>[DNBEDuplicateName]
  * The specified name is already in use.
  * <LI>[DNBEInvalidFormat]
  * The syntax of the specified string is incorrect.
  * <LI>[DNBEInvalidRelation]
  * An invalid relationship exists between two or more entities.
  * <LI>[DNBEDoesNotExist]
  * The specified entity does not exist (or is no longer available).
  * <LI>[DNBEDisabled]
  * The specified entity has been marked for deletion.
  * <LI>[DNBEAlreadyExists]
  * The specified entity already exists.
  * <LI>[DNBENotFound]
  * The specified entity cannot be found (i.e., search failed).
  * <LI>[DNBENotSupported]
  * The specified request is not supported.
  * <LI>[DNBENotImplemented]
  * The specified method is not implemented.
  * <LI>[DNBEBadType]
  * The specified type is incorrect.
  * <LI>[DNBECannotCreate]
  * An entity cannot be created.
  * <LI>[DNBEAlreadyRegistered]
  * The specified service is already registered.
  * <LI>[DNBEAlreadyInUse]
  * The specified entity is already in use.
  * <LI>[DNBEResourceLimit]
  * Insufficient system resources exist to complete the requested
  * service.
  * <LI>[DNBEInvalidCast]
  * The specified cast is invalid.
  * 
  * {\large 2.0 Numeric Errors}
  * 
  * <LI>[DNBERangeError]
  * An intermediate result violates a range constraint.
  * <LI>[DNBEOverflowError]
  * An overflow condition has been encountered during an internal
  * computation.
  * <LI>[DNBEUnderflowError]
  * An underflow condition has been encountered during an internal
  * computation.
  * <LI>[DNBENumericError]
  * A numeric operation cannot deliver the correct result within the
  * declared error bounds.
  * <LI>[DNBEZeroDivide]
  * The application attempted a division by zero.
  * <LI>[DNBEZeroVector]
  * The specified vector is the zero vector.
  * <LI>[DNBESingularMatrix]
  * The specified matrix is singular.
  * <LI>[DNBEIllegalMatrix]
  * The specified matrix is illegal.
  *  
  * {\large 3.0 Threading Errors}
  * 
  * <LI>[DNBEInvalidLock]
  * The thread cannot acquire a write lock within a read-only
  * transaction.
  * <LI>[DNBELockViolation]
  * The thread cannot access the specified entity because another thread
  * has locked the entity.
  * <LI>[DNBELockFailed]
  * The specified entity cannot be locked.
  * <LI>[DNBENotLocked]
  * The specified entity is already unlocked.
  * <LI>[DNBENotOwner]
  * The client does not own the specified entity.
  * <LI>[DNBETimeout]
  * The specified timeout period has expired.
  * <LI>[DNBEOperationAborted]
  * An operation has been aborted because of either a thread exit or an
  * application request.
  * <LI>[DNBEDeadlock]
  * A potential deadlock condition has been detected.
  *  
  * {\large 4.0 I/O Errors}
  * 
  * <LI>[DNBEFileNotFound]
  * The operating system cannot find the specified file.
  * <LI>[DNBEPathNotFound]
  * The operating system cannot find the specified path.
  * <LI>[DNBEAccessDenied]
  * The specified file cannot be accessed.
  * <LI>[DNBEOpenFailed]
  * The operating system cannot open the specified file.
  * <LI>[DNBEFileExists]
  * The specified file already exists.
  * <LI>[DNBEWriteProtect]
  * The specified device is write protected.
  * <LI>[DNBEWriteFault]
  * The system cannot write to the specified device.
  * <LI>[DNBEReadFault]
  * The system cannot read from the specified device.
  * <LI>[DNBEDiskFull]
  * There is not enough space on the disk.
  * <LI>[DNBEIOError]
  * The request could not be performed because of an I/O device error.
  * <LI>[DNBENetworkError]
  * The request could not be performed because of a network error.
  * <LI>[DNBENoNetwork]
  * The network is not present or is not started.
  *  
  */
class ExportedByDNBSystem DNBERuntimeError : public DNBException
{
public:
/**
  * Constructs a localized exception.
  * @param formatStr
  * A string literal which specifies the format string for the
  * default locale.  This literal must be defined with the macro
  * @href DNB_FORMAT.
  * @param srcFile
  * The name of the source file which contains the definition of the
  * current exception.  This parameter is automatically supplied by
  * the macro @href DNB_FORMAT.
  * @param srcLine
  * The line number of the source file which contains the definition
  * of the current exception.  This parameter is automatically
  * supplied by the macro @href DNB_FORMAT.
  * 
  * @return
  * Nothing
  * 
  * <br><B>Description</B><br>
  * This function constructs a localized exception with the format
  * string initialized to <tt>formatStr</tt>.  The resulting object has zero
  * data elements.  The parameters <tt>srcFile</tt> and <tt>srcLine</tt> specify the
  * location at which the error condition was detected.
  * 
  * 
  */
    DNBERuntimeError( const CharType *formatStr, const char *srcFile,
        int srcLine )
        DNB_THROW_SPEC_NULL;

/**
  * Creates a copy of <tt>self</tt> on the free store.
  * 
  * @return
  * A pointer to the newly created object.
  * 
  * <br><B>Description</B><br>
  * This function implements a virtual copy constructor.  It is used by
  * the message library to create copies of localized messages.
  * @exception scl_bad_alloc
  * The object could not be created because of insufficient free
  * memory.
  * 
  * 
  */
    virtual DNBMsgData *
    clone( ) const
        DNB_THROW_SPEC((scl_bad_alloc));

/**
  * Raises the exception by thworwing <tt>self</tt> 
  * 
  * <br><B>Description</B><br>
  *	Raises the exception by thworwing <tt>self</tt> 
  *
  */	
	virtual void
		raise( ) const;
};




//
//  Define the exception classes that are used to report runtime errors.  Note:
//  These classes must be documented in the comment block for DNBERuntimeError.
//
DNB_DECLARE_EXCEPTION( DNBSystem, DNBEInternalError    , DNBERuntimeError );
DNB_DECLARE_EXCEPTION( DNBSystem, DNBEInvalidName      , DNBERuntimeError );
DNB_DECLARE_EXCEPTION( DNBSystem, DNBEDuplicateName    , DNBERuntimeError );
DNB_DECLARE_EXCEPTION( DNBSystem, DNBEInvalidFormat    , DNBERuntimeError );
DNB_DECLARE_EXCEPTION( DNBSystem, DNBEInvalidRelation  , DNBERuntimeError );
DNB_DECLARE_EXCEPTION( DNBSystem, DNBEDoesNotExist     , DNBERuntimeError );
DNB_DECLARE_EXCEPTION( DNBSystem, DNBEDisabled         , DNBERuntimeError );
DNB_DECLARE_EXCEPTION( DNBSystem, DNBEAlreadyExists    , DNBERuntimeError );
DNB_DECLARE_EXCEPTION( DNBSystem, DNBENotFound         , DNBERuntimeError );
DNB_DECLARE_EXCEPTION( DNBSystem, DNBENotSupported     , DNBERuntimeError );
DNB_DECLARE_EXCEPTION( DNBSystem, DNBENotImplemented   , DNBERuntimeError );
DNB_DECLARE_EXCEPTION( DNBSystem, DNBEBadType          , DNBERuntimeError );
DNB_DECLARE_EXCEPTION( DNBSystem, DNBECannotCreate     , DNBERuntimeError );
DNB_DECLARE_EXCEPTION( DNBSystem, DNBEAlreadyRegistered, DNBERuntimeError );
DNB_DECLARE_EXCEPTION( DNBSystem, DNBEAlreadyInUse     , DNBERuntimeError );
DNB_DECLARE_EXCEPTION( DNBSystem, DNBEResourceLimit    , DNBERuntimeError );
DNB_DECLARE_EXCEPTION( DNBSystem, DNBEInvalidCast      , DNBERuntimeError );


DNB_DECLARE_EXCEPTION( DNBSystem, DNBERangeError       , DNBERuntimeError );
DNB_DECLARE_EXCEPTION( DNBSystem, DNBEOverflowError    , DNBERuntimeError );
DNB_DECLARE_EXCEPTION( DNBSystem, DNBEUnderflowError   , DNBERuntimeError );
DNB_DECLARE_EXCEPTION( DNBSystem, DNBENumericError     , DNBERuntimeError );
DNB_DECLARE_EXCEPTION( DNBSystem, DNBEZeroDivide       , DNBERuntimeError );
DNB_DECLARE_EXCEPTION( DNBSystem, DNBEZeroVector       , DNBERuntimeError );
DNB_DECLARE_EXCEPTION( DNBSystem, DNBESingularMatrix   , DNBERuntimeError );
DNB_DECLARE_EXCEPTION( DNBSystem, DNBEIllegalMatrix    , DNBERuntimeError );


DNB_DECLARE_EXCEPTION( DNBSystem, DNBEInvalidLock      , DNBERuntimeError );
DNB_DECLARE_EXCEPTION( DNBSystem, DNBELockViolation    , DNBERuntimeError );
DNB_DECLARE_EXCEPTION( DNBSystem, DNBELockFailed       , DNBERuntimeError );
DNB_DECLARE_EXCEPTION( DNBSystem, DNBENotLocked        , DNBERuntimeError );
DNB_DECLARE_EXCEPTION( DNBSystem, DNBENotOwner         , DNBERuntimeError );
DNB_DECLARE_EXCEPTION( DNBSystem, DNBETimeout          , DNBERuntimeError );
DNB_DECLARE_EXCEPTION( DNBSystem, DNBEOperationAborted , DNBERuntimeError );
DNB_DECLARE_EXCEPTION( DNBSystem, DNBEDeadlock         , DNBERuntimeError );


DNB_DECLARE_EXCEPTION( DNBSystem, DNBEFileNotFound     , DNBERuntimeError );
DNB_DECLARE_EXCEPTION( DNBSystem, DNBEPathNotFound     , DNBERuntimeError );
DNB_DECLARE_EXCEPTION( DNBSystem, DNBEAccessDenied     , DNBERuntimeError );
DNB_DECLARE_EXCEPTION( DNBSystem, DNBEOpenFailed       , DNBERuntimeError );
DNB_DECLARE_EXCEPTION( DNBSystem, DNBEFileExists       , DNBERuntimeError );
DNB_DECLARE_EXCEPTION( DNBSystem, DNBEWriteProtect     , DNBERuntimeError );
DNB_DECLARE_EXCEPTION( DNBSystem, DNBEWriteFault       , DNBERuntimeError );
DNB_DECLARE_EXCEPTION( DNBSystem, DNBEReadFault        , DNBERuntimeError );
DNB_DECLARE_EXCEPTION( DNBSystem, DNBEDiskFull         , DNBERuntimeError );
DNB_DECLARE_EXCEPTION( DNBSystem, DNBEIOError          , DNBERuntimeError );
DNB_DECLARE_EXCEPTION( DNBSystem, DNBENetworkError     , DNBERuntimeError );
DNB_DECLARE_EXCEPTION( DNBSystem, DNBENoNetwork        , DNBERuntimeError );




#endif  /* _DNBException_H_ */
