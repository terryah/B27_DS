//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview RTL trangara 01:11:15
 */
// --------------------------------------------------------

inline
DNBCountedBase::operator bool( ) const
    DNB_THROW_SPEC_NULL
{
    return ( object_ != NULL );
}


inline bool
DNBCountedBase::isBound( ) const
    DNB_THROW_SPEC_NULL
{
    return ( object_ != NULL );
}


inline bool
DNBCountedBase::operator==( const DNBCountedObject *object ) const
    DNB_THROW_SPEC_NULL
{
    return ( object_ == object );
}


inline bool
DNBCountedBase::operator==( const DNBCountedBase &right ) const
    DNB_THROW_SPEC_NULL
{
    return ( object_ == right.object_ );
}


inline bool
DNBCountedBase::operator!=( const DNBCountedObject *object ) const
    DNB_THROW_SPEC_NULL
{
    return ( object_ != object );
}


inline bool
DNBCountedBase::operator!=( const DNBCountedBase &right ) const
    DNB_THROW_SPEC_NULL
{
    return ( object_ != right.object_ );
}


inline bool
DNBCountedBase::operator< ( const DNBCountedBase &right ) const
    DNB_THROW_SPEC_NULL
{
    return ( object_ < right.object_ );
}


inline DNBCountedObject *
DNBCountedBase::_getReferent( ) const
    DNB_THROW_SPEC_NULL
{
    return object_;
}


inline DNBCountedObject *
DNBCountedBase::getReferent( ) const
    DNB_THROW_SPEC_NULL
{
    return object_;
}
