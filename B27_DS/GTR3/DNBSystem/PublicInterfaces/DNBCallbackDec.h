//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/////////////////////////////////////////////////////////
//
// 0 Passed,  0 Returned 
//
/////////////////////////////////////////////////////////

class ExportedByDNBSystem _DNBBaseFunctor0Body : public _DNBBaseFunctorBody
{
public:
    virtual 
    void 
    operator()(  )
        DNB_THROW_SPEC_ANY    = 0;

    virtual
    _DNBBaseFunctor0Body*
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ))    = 0;
};

template< class Function >
class _DNBFunction0Body : public _DNBBaseFunctor0Body
{
public:
    _DNBFunction0Body(const _DNBFunction0Body< Function>& functionFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBFunction0Body(const Function& f)
        DNB_THROW_SPEC_ANY;

    virtual
    ~_DNBFunction0Body()
        DNB_THROW_SPEC_ANY;

    virtual
    void
    operator()(  )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const

        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor0Body* 
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBFunction0Body()
        DNB_THROW_SPEC_ANY;

    Function f_;
};

template< class Function, class M1 >
class _DNBFunction0M1Body : public _DNBBaseFunctor0Body
{
public:
    _DNBFunction0M1Body(const _DNBFunction0M1Body< Function, M1 >& functionFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBFunction0M1Body(const Function& f, M1 m1 )
        DNB_THROW_SPEC_ANY;

    virtual
    ~_DNBFunction0M1Body()
        DNB_THROW_SPEC_ANY;

    virtual
    void
    operator()(  )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor0Body*
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBFunction0M1Body()
        DNB_THROW_SPEC_ANY;

    Function f_;
    M1       m1_;

};

template< class Function, class M1, class M2 >
class _DNBFunction0M2Body : public _DNBBaseFunctor0Body
{
public:
    _DNBFunction0M2Body(const _DNBFunction0M2Body< Function, M1, M2 >& functionFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBFunction0M2Body(const Function& f, M1 m1, M2 m2 )
        DNB_THROW_SPEC_ANY;

    virtual
    ~_DNBFunction0M2Body()
        DNB_THROW_SPEC_ANY;

    virtual
    void
    operator()(  )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor0Body*
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBFunction0M2Body()
        DNB_THROW_SPEC_ANY;

    Function f_;
    M1       m1_;
    M2       m2_;

};

template< class Function, class M1, class M2, class M3 >
class _DNBFunction0M3Body : public _DNBBaseFunctor0Body
{
public:
    _DNBFunction0M3Body(const _DNBFunction0M3Body< Function, M1, M2, M3 >& functionFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBFunction0M3Body(const Function& f, M1 m1, M2 m2, M3 m3 )
        DNB_THROW_SPEC_ANY;

    virtual
    ~_DNBFunction0M3Body()
        DNB_THROW_SPEC_ANY;

    virtual
    void
    operator()(  )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor0Body*
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBFunction0M3Body()
        DNB_THROW_SPEC_ANY;

    Function f_;
    M1       m1_;
    M2       m2_;
    M3       m3_;

};

template< class Function, class M1, class M2, class M3, class M4 >
class _DNBFunction0M4Body : public _DNBBaseFunctor0Body
{
public:
    _DNBFunction0M4Body(const _DNBFunction0M4Body< Function, M1, M2, M3, M4 >& functionFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBFunction0M4Body(const Function& f, M1 m1, M2 m2, M3 m3, M4 m4 )
        DNB_THROW_SPEC_ANY;

    virtual
    ~_DNBFunction0M4Body()
        DNB_THROW_SPEC_ANY;

    virtual
    void
    operator()(  )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor0Body*
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBFunction0M4Body()
        DNB_THROW_SPEC_ANY;

    Function f_;
    M1       m1_;
    M2       m2_;
    M3       m3_;
    M4       m4_;

};

template< class Function, class M1, class M2, class M3, class M4, class M5 >
class _DNBFunction0M5Body : public _DNBBaseFunctor0Body
{
public:
    _DNBFunction0M5Body(const _DNBFunction0M5Body< Function, M1, M2, M3, M4, M5 >& functionFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBFunction0M5Body(const Function& f, M1 m1, M2 m2, M3 m3, M4 m4, M5 m5 )
        DNB_THROW_SPEC_ANY;

    virtual
    ~_DNBFunction0M5Body()
        DNB_THROW_SPEC_ANY;

    virtual
    void
    operator()(  )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor0Body*
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBFunction0M5Body()
        DNB_THROW_SPEC_ANY;

    Function f_;
    M1       m1_;
    M2       m2_;
    M3       m3_;
    M4       m4_;
    M5       m5_;

};

template< class Client, class Member >
class _DNBMemberFunctor0Body:
public _DNBBaseFunctor0Body
{
public:
    _DNBMemberFunctor0Body( const _DNBMemberFunctor0Body< Client, Member >& memberFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBMemberFunctor0Body(Client* client, Member member)
        DNB_THROW_SPEC_ANY;

    virtual ~_DNBMemberFunctor0Body()
        DNB_THROW_SPEC_ANY;

    virtual
    void
    operator()(  )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor0Body*
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBMemberFunctor0Body()
        DNB_THROW_SPEC_ANY;

    Client* client_;
    Member member_;
};

template< class Client, class Member, class M1 >
class _DNBMemberFunctor0M1Body:
public _DNBBaseFunctor0Body
{
public:
    _DNBMemberFunctor0M1Body( const _DNBMemberFunctor0M1Body< Client, Member, M1 >& memberFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBMemberFunctor0M1Body(Client* client, Member member, M1 m1 )
        DNB_THROW_SPEC_ANY;
        
    virtual ~_DNBMemberFunctor0M1Body()
        DNB_THROW_SPEC_ANY;

    virtual
    void
    operator()(  )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor0Body* 
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBMemberFunctor0M1Body()
        DNB_THROW_SPEC_ANY;

    Client* client_;
    Member member_;

   M1       m1_;

};

template< class Client, class Member, class M1, class M2 >
class _DNBMemberFunctor0M2Body:
public _DNBBaseFunctor0Body
{
public:
    _DNBMemberFunctor0M2Body( const _DNBMemberFunctor0M2Body< Client, Member, M1, M2 >& memberFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBMemberFunctor0M2Body(Client* client, Member member, M1 m1, M2 m2 )
        DNB_THROW_SPEC_ANY;
        
    virtual ~_DNBMemberFunctor0M2Body()
        DNB_THROW_SPEC_ANY;

    virtual
    void
    operator()(  )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor0Body* 
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBMemberFunctor0M2Body()
        DNB_THROW_SPEC_ANY;

    Client* client_;
    Member member_;

   M1       m1_;
    M2       m2_;

};

template< class Client, class Member, class M1, class M2, class M3 >
class _DNBMemberFunctor0M3Body:
public _DNBBaseFunctor0Body
{
public:
    _DNBMemberFunctor0M3Body( const _DNBMemberFunctor0M3Body< Client, Member, M1, M2, M3 >& memberFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBMemberFunctor0M3Body(Client* client, Member member, M1 m1, M2 m2, M3 m3 )
        DNB_THROW_SPEC_ANY;
        
    virtual ~_DNBMemberFunctor0M3Body()
        DNB_THROW_SPEC_ANY;

    virtual
    void
    operator()(  )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor0Body* 
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBMemberFunctor0M3Body()
        DNB_THROW_SPEC_ANY;

    Client* client_;
    Member member_;

   M1       m1_;
    M2       m2_;
    M3       m3_;

};

template< class Client, class Member, class M1, class M2, class M3, class M4 >
class _DNBMemberFunctor0M4Body:
public _DNBBaseFunctor0Body
{
public:
    _DNBMemberFunctor0M4Body( const _DNBMemberFunctor0M4Body< Client, Member, M1, M2, M3, M4 >& memberFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBMemberFunctor0M4Body(Client* client, Member member, M1 m1, M2 m2, M3 m3, M4 m4 )
        DNB_THROW_SPEC_ANY;
        
    virtual ~_DNBMemberFunctor0M4Body()
        DNB_THROW_SPEC_ANY;

    virtual
    void
    operator()(  )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor0Body* 
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBMemberFunctor0M4Body()
        DNB_THROW_SPEC_ANY;

    Client* client_;
    Member member_;

   M1       m1_;
    M2       m2_;
    M3       m3_;
    M4       m4_;

};

template< class Client, class Member, class M1, class M2, class M3, class M4, class M5 >
class _DNBMemberFunctor0M5Body:
public _DNBBaseFunctor0Body
{
public:
    _DNBMemberFunctor0M5Body( const _DNBMemberFunctor0M5Body< Client, Member, M1, M2, M3, M4, M5 >& memberFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBMemberFunctor0M5Body(Client* client, Member member, M1 m1, M2 m2, M3 m3, M4 m4, M5 m5 )
        DNB_THROW_SPEC_ANY;
        
    virtual ~_DNBMemberFunctor0M5Body()
        DNB_THROW_SPEC_ANY;

    virtual
    void
    operator()(  )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor0Body* 
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBMemberFunctor0M5Body()
        DNB_THROW_SPEC_ANY;

    Client* client_;
    Member member_;

   M1       m1_;
    M2       m2_;
    M3       m3_;
    M4       m4_;
    M5       m5_;

};

class ExportedByDNBSystem DNBFunctor0  : public DNBBaseFunctor
{
public:
    DNBFunctor0(_DNBBaseFunctor0Body* body = NULL)
        DNB_THROW_SPEC_ANY;

    DNBFunctor0(const DNBFunctor0& DNBFunctor)
        DNB_THROW_SPEC_ANY;

    virtual
    ~DNBFunctor0()
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctorBody*
    getBody() const
        DNB_THROW_SPEC_ANY;

    DNBFunctor0&
    operator=(const DNBFunctor0& DNBFunctor)
        DNB_THROW_SPEC(( scl_bad_alloc ));

    void
    operator()(  )
        DNB_THROW_SPEC_ANY;

private:
    _DNBBaseFunctor0Body* body_;
};

template< class Function >
DNBFunctor0
DNBMakeFunctorG(DNBFunctor0*, Function f)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBFunction0Body< Function>* body =
        DNB_NEW _DNBFunction0Body< Function>(f);
    return DNBFunctor0(body);
}

template< class Function, class M1 >
DNBFunctor0
DNBMakeFunctorG(DNBFunctor0*, Function f, M1 m1 )
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBFunction0M1Body< Function, M1 >* body =
        DNB_NEW _DNBFunction0M1Body< Function, M1 >(f, m1);
    return DNBFunctor0(body);
}

template< class Function, class M1, class M2 >
DNBFunctor0
DNBMakeFunctorG(DNBFunctor0*, Function f, M1 m1, M2 m2 )
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBFunction0M2Body< Function, M1, M2 >* body =
        DNB_NEW _DNBFunction0M2Body< Function, M1, M2 >(f, m1, m2);
    return DNBFunctor0(body);
}

template< class Function, class M1, class M2, class M3 >
DNBFunctor0
DNBMakeFunctorG(DNBFunctor0*, Function f, M1 m1, M2 m2, M3 m3 )
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBFunction0M3Body< Function, M1, M2, M3 >* body =
        DNB_NEW _DNBFunction0M3Body< Function, M1, M2, M3 >(f, m1, m2, m3);
    return DNBFunctor0(body);
}

template< class Function, class M1, class M2, class M3, class M4 >
DNBFunctor0
DNBMakeFunctorG(DNBFunctor0*, Function f, M1 m1, M2 m2, M3 m3, M4 m4 )
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBFunction0M4Body< Function, M1, M2, M3, M4 >* body =
        DNB_NEW _DNBFunction0M4Body< Function, M1, M2, M3, M4 >(f, m1, m2, m3, m4);
    return DNBFunctor0(body);
}

template< class Function, class M1, class M2, class M3, class M4, class M5 >
DNBFunctor0
DNBMakeFunctorG(DNBFunctor0*, Function f, M1 m1, M2 m2, M3 m3, M4 m4, M5 m5 )
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBFunction0M5Body< Function, M1, M2, M3, M4, M5 >* body =
        DNB_NEW _DNBFunction0M5Body< Function, M1, M2, M3, M4, M5 >(f, m1, m2, m3, m4, m5);
    return DNBFunctor0(body);
}

template< class Client, class Member >
DNBFunctor0
DNBMakeFunctorM(DNBFunctor0*,Client* client, Member member)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBMemberFunctor0Body< Client, Member >* body =
        DNB_NEW _DNBMemberFunctor0Body< Client, Member >(client,member);
    return DNBFunctor0(body);
}

template< class Client, class Member, class M1>
DNBFunctor0
DNBMakeFunctorM(DNBFunctor0 *, Client* client, Member member, M1 m1)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBMemberFunctor0M1Body< Client, Member, M1 >* body =
        DNB_NEW _DNBMemberFunctor0M1Body< Client, Member, M1 >( client, member, m1 );
    return DNBFunctor0 (body);
}

template< class Client, class Member, class M1, class M2>
DNBFunctor0
DNBMakeFunctorM(DNBFunctor0 *, Client* client, Member member, M1 m1, M2 m2)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBMemberFunctor0M2Body< Client, Member, M1, M2 >* body =
        DNB_NEW _DNBMemberFunctor0M2Body< Client, Member, M1, M2 >( client, member, m1, m2 );
    return DNBFunctor0 (body);
}

template< class Client, class Member, class M1, class M2, class M3>
DNBFunctor0
DNBMakeFunctorM(DNBFunctor0 *, Client* client, Member member, M1 m1, M2 m2, M3 m3)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBMemberFunctor0M3Body< Client, Member, M1, M2, M3 >* body =
        DNB_NEW _DNBMemberFunctor0M3Body< Client, Member, M1, M2, M3 >( client, member, m1, m2, m3 );
    return DNBFunctor0 (body);
}

template< class Client, class Member, class M1, class M2, class M3, class M4>
DNBFunctor0
DNBMakeFunctorM(DNBFunctor0 *, Client* client, Member member, M1 m1, M2 m2, M3 m3, M4 m4)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBMemberFunctor0M4Body< Client, Member, M1, M2, M3, M4 >* body =
        DNB_NEW _DNBMemberFunctor0M4Body< Client, Member, M1, M2, M3, M4 >( client, member, m1, m2, m3, m4 );
    return DNBFunctor0 (body);
}

template< class Client, class Member, class M1, class M2, class M3, class M4, class M5>
DNBFunctor0
DNBMakeFunctorM(DNBFunctor0 *, Client* client, Member member, M1 m1, M2 m2, M3 m3, M4 m4, M5 m5)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBMemberFunctor0M5Body< Client, Member, M1, M2, M3, M4, M5 >* body =
        DNB_NEW _DNBMemberFunctor0M5Body< Client, Member, M1, M2, M3, M4, M5 >( client, member, m1, m2, m3, m4, m5 );
    return DNBFunctor0 (body);
}

/////////////////////////////////////////////////////////
//
// 1 Passed,  0 Returned 
//
/////////////////////////////////////////////////////////

template< class A1 >
class  _DNBBaseFunctor1Body : public _DNBBaseFunctorBody
{
public:
    virtual 
    void 
    operator()( A1 a1 )
        DNB_THROW_SPEC_ANY    = 0;

    virtual
    _DNBBaseFunctor1Body< A1 >*
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ))    = 0;
};

template< class A1, class Function >
class _DNBFunction1Body : public _DNBBaseFunctor1Body< A1 >
{
public:
    _DNBFunction1Body(const _DNBFunction1Body< A1, Function>& functionFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBFunction1Body(const Function& f)
        DNB_THROW_SPEC_ANY;

    virtual
    ~_DNBFunction1Body()
        DNB_THROW_SPEC_ANY;

    virtual
    void
    operator()( A1 a1 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const

        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor1Body< A1 >* 
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBFunction1Body()
        DNB_THROW_SPEC_ANY;

    Function f_;
};

template< class A1, class Function, class M1 >
class _DNBFunction1M1Body : public _DNBBaseFunctor1Body< A1 >
{
public:
    _DNBFunction1M1Body(const _DNBFunction1M1Body< A1, Function, M1 >& functionFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBFunction1M1Body(const Function& f, M1 m1 )
        DNB_THROW_SPEC_ANY;

    virtual
    ~_DNBFunction1M1Body()
        DNB_THROW_SPEC_ANY;

    virtual
    void
    operator()( A1 a1 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor1Body< A1 >*
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBFunction1M1Body()
        DNB_THROW_SPEC_ANY;

    Function f_;
    M1       m1_;

};

template< class A1, class Function, class M1, class M2 >
class _DNBFunction1M2Body : public _DNBBaseFunctor1Body< A1 >
{
public:
    _DNBFunction1M2Body(const _DNBFunction1M2Body< A1, Function, M1, M2 >& functionFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBFunction1M2Body(const Function& f, M1 m1, M2 m2 )
        DNB_THROW_SPEC_ANY;

    virtual
    ~_DNBFunction1M2Body()
        DNB_THROW_SPEC_ANY;

    virtual
    void
    operator()( A1 a1 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor1Body< A1 >*
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBFunction1M2Body()
        DNB_THROW_SPEC_ANY;

    Function f_;
    M1       m1_;
    M2       m2_;

};

template< class A1, class Function, class M1, class M2, class M3 >
class _DNBFunction1M3Body : public _DNBBaseFunctor1Body< A1 >
{
public:
    _DNBFunction1M3Body(const _DNBFunction1M3Body< A1, Function, M1, M2, M3 >& functionFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBFunction1M3Body(const Function& f, M1 m1, M2 m2, M3 m3 )
        DNB_THROW_SPEC_ANY;

    virtual
    ~_DNBFunction1M3Body()
        DNB_THROW_SPEC_ANY;

    virtual
    void
    operator()( A1 a1 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor1Body< A1 >*
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBFunction1M3Body()
        DNB_THROW_SPEC_ANY;

    Function f_;
    M1       m1_;
    M2       m2_;
    M3       m3_;

};

template< class A1, class Function, class M1, class M2, class M3, class M4 >
class _DNBFunction1M4Body : public _DNBBaseFunctor1Body< A1 >
{
public:
    _DNBFunction1M4Body(const _DNBFunction1M4Body< A1, Function, M1, M2, M3, M4 >& functionFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBFunction1M4Body(const Function& f, M1 m1, M2 m2, M3 m3, M4 m4 )
        DNB_THROW_SPEC_ANY;

    virtual
    ~_DNBFunction1M4Body()
        DNB_THROW_SPEC_ANY;

    virtual
    void
    operator()( A1 a1 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor1Body< A1 >*
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBFunction1M4Body()
        DNB_THROW_SPEC_ANY;

    Function f_;
    M1       m1_;
    M2       m2_;
    M3       m3_;
    M4       m4_;

};

template< class A1, class Client, class Member >
class _DNBMemberFunctor1Body:
public _DNBBaseFunctor1Body< A1 >
{
public:
    _DNBMemberFunctor1Body( const _DNBMemberFunctor1Body< A1, Client, Member >& memberFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBMemberFunctor1Body(Client* client, Member member)
        DNB_THROW_SPEC_ANY;

    virtual ~_DNBMemberFunctor1Body()
        DNB_THROW_SPEC_ANY;

    virtual
    void
    operator()( A1 a1 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor1Body< A1 >*
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBMemberFunctor1Body()
        DNB_THROW_SPEC_ANY;

    Client* client_;
    Member member_;
};

template< class A1, class Client, class Member, class M1 >
class _DNBMemberFunctor1M1Body:
public _DNBBaseFunctor1Body< A1 >
{
public:
    _DNBMemberFunctor1M1Body( const _DNBMemberFunctor1M1Body< A1, Client, Member, M1 >& memberFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBMemberFunctor1M1Body(Client* client, Member member, M1 m1 )
        DNB_THROW_SPEC_ANY;
        
    virtual ~_DNBMemberFunctor1M1Body()
        DNB_THROW_SPEC_ANY;

    virtual
    void
    operator()( A1 a1 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor1Body< A1 >* 
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBMemberFunctor1M1Body()
        DNB_THROW_SPEC_ANY;

    Client* client_;
    Member member_;

   M1       m1_;

};

template< class A1, class Client, class Member, class M1, class M2 >
class _DNBMemberFunctor1M2Body:
public _DNBBaseFunctor1Body< A1 >
{
public:
    _DNBMemberFunctor1M2Body( const _DNBMemberFunctor1M2Body< A1, Client, Member, M1, M2 >& memberFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBMemberFunctor1M2Body(Client* client, Member member, M1 m1, M2 m2 )
        DNB_THROW_SPEC_ANY;
        
    virtual ~_DNBMemberFunctor1M2Body()
        DNB_THROW_SPEC_ANY;

    virtual
    void
    operator()( A1 a1 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor1Body< A1 >* 
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBMemberFunctor1M2Body()
        DNB_THROW_SPEC_ANY;

    Client* client_;
    Member member_;

   M1       m1_;
    M2       m2_;

};

template< class A1, class Client, class Member, class M1, class M2, class M3 >
class _DNBMemberFunctor1M3Body:
public _DNBBaseFunctor1Body< A1 >
{
public:
    _DNBMemberFunctor1M3Body( const _DNBMemberFunctor1M3Body< A1, Client, Member, M1, M2, M3 >& memberFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBMemberFunctor1M3Body(Client* client, Member member, M1 m1, M2 m2, M3 m3 )
        DNB_THROW_SPEC_ANY;
        
    virtual ~_DNBMemberFunctor1M3Body()
        DNB_THROW_SPEC_ANY;

    virtual
    void
    operator()( A1 a1 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor1Body< A1 >* 
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBMemberFunctor1M3Body()
        DNB_THROW_SPEC_ANY;

    Client* client_;
    Member member_;

   M1       m1_;
    M2       m2_;
    M3       m3_;

};

template< class A1, class Client, class Member, class M1, class M2, class M3, class M4 >
class _DNBMemberFunctor1M4Body:
public _DNBBaseFunctor1Body< A1 >
{
public:
    _DNBMemberFunctor1M4Body( const _DNBMemberFunctor1M4Body< A1, Client, Member, M1, M2, M3, M4 >& memberFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBMemberFunctor1M4Body(Client* client, Member member, M1 m1, M2 m2, M3 m3, M4 m4 )
        DNB_THROW_SPEC_ANY;
        
    virtual ~_DNBMemberFunctor1M4Body()
        DNB_THROW_SPEC_ANY;

    virtual
    void
    operator()( A1 a1 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor1Body< A1 >* 
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBMemberFunctor1M4Body()
        DNB_THROW_SPEC_ANY;

    Client* client_;
    Member member_;

   M1       m1_;
    M2       m2_;
    M3       m3_;
    M4       m4_;

};

template< class A1 >
class  DNBFunctor1  : public DNBBaseFunctor
{
public:
    DNBFunctor1(_DNBBaseFunctor1Body< A1 >* body = NULL)
        DNB_THROW_SPEC_ANY;

    DNBFunctor1(const DNBFunctor1< A1 >& DNBFunctor)
        DNB_THROW_SPEC_ANY;

    virtual
    ~DNBFunctor1()
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctorBody*
    getBody() const
        DNB_THROW_SPEC_ANY;

    DNBFunctor1< A1 >&
    operator=(const DNBFunctor1< A1 >& DNBFunctor)
        DNB_THROW_SPEC(( scl_bad_alloc ));

    void
    operator()( A1 a1 )
        DNB_THROW_SPEC_ANY;

private:
    _DNBBaseFunctor1Body< A1 >* body_;
};

template< class A1, class Function >
DNBFunctor1< A1 >
DNBMakeFunctorG(DNBFunctor1< A1 >*, Function f)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBFunction1Body< A1, Function>* body =
        DNB_NEW _DNBFunction1Body< A1, Function>(f);
    return DNBFunctor1< A1 >(body);
}

template< class A1, class Function, class M1 >
DNBFunctor1< A1 >
DNBMakeFunctorG(DNBFunctor1< A1 >*, Function f, M1 m1 )
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBFunction1M1Body< A1, Function, M1 >* body =
        DNB_NEW _DNBFunction1M1Body< A1, Function, M1 >(f, m1);
    return DNBFunctor1< A1 >(body);
}

template< class A1, class Function, class M1, class M2 >
DNBFunctor1< A1 >
DNBMakeFunctorG(DNBFunctor1< A1 >*, Function f, M1 m1, M2 m2 )
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBFunction1M2Body< A1, Function, M1, M2 >* body =
        DNB_NEW _DNBFunction1M2Body< A1, Function, M1, M2 >(f, m1, m2);
    return DNBFunctor1< A1 >(body);
}

template< class A1, class Function, class M1, class M2, class M3 >
DNBFunctor1< A1 >
DNBMakeFunctorG(DNBFunctor1< A1 >*, Function f, M1 m1, M2 m2, M3 m3 )
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBFunction1M3Body< A1, Function, M1, M2, M3 >* body =
        DNB_NEW _DNBFunction1M3Body< A1, Function, M1, M2, M3 >(f, m1, m2, m3);
    return DNBFunctor1< A1 >(body);
}

template< class A1, class Function, class M1, class M2, class M3, class M4 >
DNBFunctor1< A1 >
DNBMakeFunctorG(DNBFunctor1< A1 >*, Function f, M1 m1, M2 m2, M3 m3, M4 m4 )
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBFunction1M4Body< A1, Function, M1, M2, M3, M4 >* body =
        DNB_NEW _DNBFunction1M4Body< A1, Function, M1, M2, M3, M4 >(f, m1, m2, m3, m4);
    return DNBFunctor1< A1 >(body);
}

template< class A1, class Client, class Member >
DNBFunctor1< A1 >
DNBMakeFunctorM(DNBFunctor1< A1 >*,Client* client, Member member)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBMemberFunctor1Body< A1, Client, Member >* body =
        DNB_NEW _DNBMemberFunctor1Body< A1, Client, Member >(client,member);
    return DNBFunctor1< A1 >(body);
}

template< class A1, class Client, class Member, class M1>
DNBFunctor1< A1 >
DNBMakeFunctorM(DNBFunctor1< A1 > *, Client* client, Member member, M1 m1)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBMemberFunctor1M1Body< A1, Client, Member, M1 >* body =
        DNB_NEW _DNBMemberFunctor1M1Body< A1, Client, Member, M1 >( client, member, m1 );
    return DNBFunctor1< A1 > (body);
}

template< class A1, class Client, class Member, class M1, class M2>
DNBFunctor1< A1 >
DNBMakeFunctorM(DNBFunctor1< A1 > *, Client* client, Member member, M1 m1, M2 m2)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBMemberFunctor1M2Body< A1, Client, Member, M1, M2 >* body =
        DNB_NEW _DNBMemberFunctor1M2Body< A1, Client, Member, M1, M2 >( client, member, m1, m2 );
    return DNBFunctor1< A1 > (body);
}

template< class A1, class Client, class Member, class M1, class M2, class M3>
DNBFunctor1< A1 >
DNBMakeFunctorM(DNBFunctor1< A1 > *, Client* client, Member member, M1 m1, M2 m2, M3 m3)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBMemberFunctor1M3Body< A1, Client, Member, M1, M2, M3 >* body =
        DNB_NEW _DNBMemberFunctor1M3Body< A1, Client, Member, M1, M2, M3 >( client, member, m1, m2, m3 );
    return DNBFunctor1< A1 > (body);
}

template< class A1, class Client, class Member, class M1, class M2, class M3, class M4>
DNBFunctor1< A1 >
DNBMakeFunctorM(DNBFunctor1< A1 > *, Client* client, Member member, M1 m1, M2 m2, M3 m3, M4 m4)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBMemberFunctor1M4Body< A1, Client, Member, M1, M2, M3, M4 >* body =
        DNB_NEW _DNBMemberFunctor1M4Body< A1, Client, Member, M1, M2, M3, M4 >( client, member, m1, m2, m3, m4 );
    return DNBFunctor1< A1 > (body);
}

/////////////////////////////////////////////////////////
//
// 2 Passed,  0 Returned 
//
/////////////////////////////////////////////////////////

template< class A1, class A2 >
class  _DNBBaseFunctor2Body : public _DNBBaseFunctorBody
{
public:
    virtual 
    void 
    operator()( A1 a1, A2 a2 )
        DNB_THROW_SPEC_ANY    = 0;

    virtual
    _DNBBaseFunctor2Body< A1, A2 >*
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ))    = 0;
};

template< class A1, class A2, class Function >
class _DNBFunction2Body : public _DNBBaseFunctor2Body< A1, A2 >
{
public:
    _DNBFunction2Body(const _DNBFunction2Body< A1, A2, Function>& functionFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBFunction2Body(const Function& f)
        DNB_THROW_SPEC_ANY;

    virtual
    ~_DNBFunction2Body()
        DNB_THROW_SPEC_ANY;

    virtual
    void
    operator()( A1 a1, A2 a2 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const

        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor2Body< A1, A2 >* 
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBFunction2Body()
        DNB_THROW_SPEC_ANY;

    Function f_;
};

template< class A1, class A2, class Function, class M1 >
class _DNBFunction2M1Body : public _DNBBaseFunctor2Body< A1, A2 >
{
public:
    _DNBFunction2M1Body(const _DNBFunction2M1Body< A1, A2, Function, M1 >& functionFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBFunction2M1Body(const Function& f, M1 m1 )
        DNB_THROW_SPEC_ANY;

    virtual
    ~_DNBFunction2M1Body()
        DNB_THROW_SPEC_ANY;

    virtual
    void
    operator()( A1 a1, A2 a2 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor2Body< A1, A2 >*
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBFunction2M1Body()
        DNB_THROW_SPEC_ANY;

    Function f_;
    M1       m1_;

};

template< class A1, class A2, class Function, class M1, class M2 >
class _DNBFunction2M2Body : public _DNBBaseFunctor2Body< A1, A2 >
{
public:
    _DNBFunction2M2Body(const _DNBFunction2M2Body< A1, A2, Function, M1, M2 >& functionFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBFunction2M2Body(const Function& f, M1 m1, M2 m2 )
        DNB_THROW_SPEC_ANY;

    virtual
    ~_DNBFunction2M2Body()
        DNB_THROW_SPEC_ANY;

    virtual
    void
    operator()( A1 a1, A2 a2 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor2Body< A1, A2 >*
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBFunction2M2Body()
        DNB_THROW_SPEC_ANY;

    Function f_;
    M1       m1_;
    M2       m2_;

};

template< class A1, class A2, class Function, class M1, class M2, class M3 >
class _DNBFunction2M3Body : public _DNBBaseFunctor2Body< A1, A2 >
{
public:
    _DNBFunction2M3Body(const _DNBFunction2M3Body< A1, A2, Function, M1, M2, M3 >& functionFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBFunction2M3Body(const Function& f, M1 m1, M2 m2, M3 m3 )
        DNB_THROW_SPEC_ANY;

    virtual
    ~_DNBFunction2M3Body()
        DNB_THROW_SPEC_ANY;

    virtual
    void
    operator()( A1 a1, A2 a2 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor2Body< A1, A2 >*
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBFunction2M3Body()
        DNB_THROW_SPEC_ANY;

    Function f_;
    M1       m1_;
    M2       m2_;
    M3       m3_;

};

template< class A1, class A2, class Client, class Member >
class _DNBMemberFunctor2Body:
public _DNBBaseFunctor2Body< A1, A2 >
{
public:
    _DNBMemberFunctor2Body( const _DNBMemberFunctor2Body< A1, A2, Client, Member >& memberFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBMemberFunctor2Body(Client* client, Member member)
        DNB_THROW_SPEC_ANY;

    virtual ~_DNBMemberFunctor2Body()
        DNB_THROW_SPEC_ANY;

    virtual
    void
    operator()( A1 a1, A2 a2 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor2Body< A1, A2 >*
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBMemberFunctor2Body()
        DNB_THROW_SPEC_ANY;

    Client* client_;
    Member member_;
};

template< class A1, class A2, class Client, class Member, class M1 >
class _DNBMemberFunctor2M1Body:
public _DNBBaseFunctor2Body< A1, A2 >
{
public:
    _DNBMemberFunctor2M1Body( const _DNBMemberFunctor2M1Body< A1, A2, Client, Member, M1 >& memberFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBMemberFunctor2M1Body(Client* client, Member member, M1 m1 )
        DNB_THROW_SPEC_ANY;
        
    virtual ~_DNBMemberFunctor2M1Body()
        DNB_THROW_SPEC_ANY;

    virtual
    void
    operator()( A1 a1, A2 a2 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor2Body< A1, A2 >* 
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBMemberFunctor2M1Body()
        DNB_THROW_SPEC_ANY;

    Client* client_;
    Member member_;

   M1       m1_;

};

template< class A1, class A2, class Client, class Member, class M1, class M2 >
class _DNBMemberFunctor2M2Body:
public _DNBBaseFunctor2Body< A1, A2 >
{
public:
    _DNBMemberFunctor2M2Body( const _DNBMemberFunctor2M2Body< A1, A2, Client, Member, M1, M2 >& memberFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBMemberFunctor2M2Body(Client* client, Member member, M1 m1, M2 m2 )
        DNB_THROW_SPEC_ANY;
        
    virtual ~_DNBMemberFunctor2M2Body()
        DNB_THROW_SPEC_ANY;

    virtual
    void
    operator()( A1 a1, A2 a2 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor2Body< A1, A2 >* 
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBMemberFunctor2M2Body()
        DNB_THROW_SPEC_ANY;

    Client* client_;
    Member member_;

   M1       m1_;
    M2       m2_;

};

template< class A1, class A2, class Client, class Member, class M1, class M2, class M3 >
class _DNBMemberFunctor2M3Body:
public _DNBBaseFunctor2Body< A1, A2 >
{
public:
    _DNBMemberFunctor2M3Body( const _DNBMemberFunctor2M3Body< A1, A2, Client, Member, M1, M2, M3 >& memberFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBMemberFunctor2M3Body(Client* client, Member member, M1 m1, M2 m2, M3 m3 )
        DNB_THROW_SPEC_ANY;
        
    virtual ~_DNBMemberFunctor2M3Body()
        DNB_THROW_SPEC_ANY;

    virtual
    void
    operator()( A1 a1, A2 a2 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor2Body< A1, A2 >* 
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBMemberFunctor2M3Body()
        DNB_THROW_SPEC_ANY;

    Client* client_;
    Member member_;

   M1       m1_;
    M2       m2_;
    M3       m3_;

};

template< class A1, class A2 >
class  DNBFunctor2  : public DNBBaseFunctor
{
public:
    DNBFunctor2(_DNBBaseFunctor2Body< A1, A2 >* body = NULL)
        DNB_THROW_SPEC_ANY;

    DNBFunctor2(const DNBFunctor2< A1, A2 >& DNBFunctor)
        DNB_THROW_SPEC_ANY;

    virtual
    ~DNBFunctor2()
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctorBody*
    getBody() const
        DNB_THROW_SPEC_ANY;

    DNBFunctor2< A1, A2 >&
    operator=(const DNBFunctor2< A1, A2 >& DNBFunctor)
        DNB_THROW_SPEC(( scl_bad_alloc ));

    void
    operator()( A1 a1, A2 a2 )
        DNB_THROW_SPEC_ANY;

private:
    _DNBBaseFunctor2Body< A1, A2 >* body_;
};

template< class A1, class A2, class Function >
DNBFunctor2< A1, A2 >
DNBMakeFunctorG(DNBFunctor2< A1, A2 >*, Function f)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBFunction2Body< A1, A2, Function>* body =
        DNB_NEW _DNBFunction2Body< A1, A2, Function>(f);
    return DNBFunctor2< A1, A2 >(body);
}

template< class A1, class A2, class Function, class M1 >
DNBFunctor2< A1, A2 >
DNBMakeFunctorG(DNBFunctor2< A1, A2 >*, Function f, M1 m1 )
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBFunction2M1Body< A1, A2, Function, M1 >* body =
        DNB_NEW _DNBFunction2M1Body< A1, A2, Function, M1 >(f, m1);
    return DNBFunctor2< A1, A2 >(body);
}

template< class A1, class A2, class Function, class M1, class M2 >
DNBFunctor2< A1, A2 >
DNBMakeFunctorG(DNBFunctor2< A1, A2 >*, Function f, M1 m1, M2 m2 )
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBFunction2M2Body< A1, A2, Function, M1, M2 >* body =
        DNB_NEW _DNBFunction2M2Body< A1, A2, Function, M1, M2 >(f, m1, m2);
    return DNBFunctor2< A1, A2 >(body);
}

template< class A1, class A2, class Function, class M1, class M2, class M3 >
DNBFunctor2< A1, A2 >
DNBMakeFunctorG(DNBFunctor2< A1, A2 >*, Function f, M1 m1, M2 m2, M3 m3 )
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBFunction2M3Body< A1, A2, Function, M1, M2, M3 >* body =
        DNB_NEW _DNBFunction2M3Body< A1, A2, Function, M1, M2, M3 >(f, m1, m2, m3);
    return DNBFunctor2< A1, A2 >(body);
}

template< class A1, class A2, class Client, class Member >
DNBFunctor2< A1, A2 >
DNBMakeFunctorM(DNBFunctor2< A1, A2 >*,Client* client, Member member)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBMemberFunctor2Body< A1, A2, Client, Member >* body =
        DNB_NEW _DNBMemberFunctor2Body< A1, A2, Client, Member >(client,member);
    return DNBFunctor2< A1, A2 >(body);
}

template< class A1, class A2, class Client, class Member, class M1>
DNBFunctor2< A1, A2 >
DNBMakeFunctorM(DNBFunctor2< A1, A2 > *, Client* client, Member member, M1 m1)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBMemberFunctor2M1Body< A1, A2, Client, Member, M1 >* body =
        DNB_NEW _DNBMemberFunctor2M1Body< A1, A2, Client, Member, M1 >( client, member, m1 );
    return DNBFunctor2< A1, A2 > (body);
}

template< class A1, class A2, class Client, class Member, class M1, class M2>
DNBFunctor2< A1, A2 >
DNBMakeFunctorM(DNBFunctor2< A1, A2 > *, Client* client, Member member, M1 m1, M2 m2)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBMemberFunctor2M2Body< A1, A2, Client, Member, M1, M2 >* body =
        DNB_NEW _DNBMemberFunctor2M2Body< A1, A2, Client, Member, M1, M2 >( client, member, m1, m2 );
    return DNBFunctor2< A1, A2 > (body);
}

template< class A1, class A2, class Client, class Member, class M1, class M2, class M3>
DNBFunctor2< A1, A2 >
DNBMakeFunctorM(DNBFunctor2< A1, A2 > *, Client* client, Member member, M1 m1, M2 m2, M3 m3)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBMemberFunctor2M3Body< A1, A2, Client, Member, M1, M2, M3 >* body =
        DNB_NEW _DNBMemberFunctor2M3Body< A1, A2, Client, Member, M1, M2, M3 >( client, member, m1, m2, m3 );
    return DNBFunctor2< A1, A2 > (body);
}

/////////////////////////////////////////////////////////
//
// 3 Passed,  0 Returned 
//
/////////////////////////////////////////////////////////

template< class A1, class A2, class A3 >
class  _DNBBaseFunctor3Body : public _DNBBaseFunctorBody
{
public:
    virtual 
    void 
    operator()( A1 a1, A2 a2, A3 a3 )
        DNB_THROW_SPEC_ANY    = 0;

    virtual
    _DNBBaseFunctor3Body< A1, A2, A3 >*
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ))    = 0;
};

template< class A1, class A2, class A3, class Function >
class _DNBFunction3Body : public _DNBBaseFunctor3Body< A1, A2, A3 >
{
public:
    _DNBFunction3Body(const _DNBFunction3Body< A1, A2, A3, Function>& functionFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBFunction3Body(const Function& f)
        DNB_THROW_SPEC_ANY;

    virtual
    ~_DNBFunction3Body()
        DNB_THROW_SPEC_ANY;

    virtual
    void
    operator()( A1 a1, A2 a2, A3 a3 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const

        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor3Body< A1, A2, A3 >* 
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBFunction3Body()
        DNB_THROW_SPEC_ANY;

    Function f_;
};

template< class A1, class A2, class A3, class Function, class M1 >
class _DNBFunction3M1Body : public _DNBBaseFunctor3Body< A1, A2, A3 >
{
public:
    _DNBFunction3M1Body(const _DNBFunction3M1Body< A1, A2, A3, Function, M1 >& functionFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBFunction3M1Body(const Function& f, M1 m1 )
        DNB_THROW_SPEC_ANY;

    virtual
    ~_DNBFunction3M1Body()
        DNB_THROW_SPEC_ANY;

    virtual
    void
    operator()( A1 a1, A2 a2, A3 a3 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor3Body< A1, A2, A3 >*
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBFunction3M1Body()
        DNB_THROW_SPEC_ANY;

    Function f_;
    M1       m1_;

};

template< class A1, class A2, class A3, class Function, class M1, class M2 >
class _DNBFunction3M2Body : public _DNBBaseFunctor3Body< A1, A2, A3 >
{
public:
    _DNBFunction3M2Body(const _DNBFunction3M2Body< A1, A2, A3, Function, M1, M2 >& functionFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBFunction3M2Body(const Function& f, M1 m1, M2 m2 )
        DNB_THROW_SPEC_ANY;

    virtual
    ~_DNBFunction3M2Body()
        DNB_THROW_SPEC_ANY;

    virtual
    void
    operator()( A1 a1, A2 a2, A3 a3 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor3Body< A1, A2, A3 >*
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBFunction3M2Body()
        DNB_THROW_SPEC_ANY;

    Function f_;
    M1       m1_;
    M2       m2_;

};

template< class A1, class A2, class A3, class Client, class Member >
class _DNBMemberFunctor3Body:
public _DNBBaseFunctor3Body< A1, A2, A3 >
{
public:
    _DNBMemberFunctor3Body( const _DNBMemberFunctor3Body< A1, A2, A3, Client, Member >& memberFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBMemberFunctor3Body(Client* client, Member member)
        DNB_THROW_SPEC_ANY;

    virtual ~_DNBMemberFunctor3Body()
        DNB_THROW_SPEC_ANY;

    virtual
    void
    operator()( A1 a1, A2 a2, A3 a3 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor3Body< A1, A2, A3 >*
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBMemberFunctor3Body()
        DNB_THROW_SPEC_ANY;

    Client* client_;
    Member member_;
};

template< class A1, class A2, class A3, class Client, class Member, class M1 >
class _DNBMemberFunctor3M1Body:
public _DNBBaseFunctor3Body< A1, A2, A3 >
{
public:
    _DNBMemberFunctor3M1Body( const _DNBMemberFunctor3M1Body< A1, A2, A3, Client, Member, M1 >& memberFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBMemberFunctor3M1Body(Client* client, Member member, M1 m1 )
        DNB_THROW_SPEC_ANY;
        
    virtual ~_DNBMemberFunctor3M1Body()
        DNB_THROW_SPEC_ANY;

    virtual
    void
    operator()( A1 a1, A2 a2, A3 a3 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor3Body< A1, A2, A3 >* 
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBMemberFunctor3M1Body()
        DNB_THROW_SPEC_ANY;

    Client* client_;
    Member member_;

   M1       m1_;

};

template< class A1, class A2, class A3, class Client, class Member, class M1, class M2 >
class _DNBMemberFunctor3M2Body:
public _DNBBaseFunctor3Body< A1, A2, A3 >
{
public:
    _DNBMemberFunctor3M2Body( const _DNBMemberFunctor3M2Body< A1, A2, A3, Client, Member, M1, M2 >& memberFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBMemberFunctor3M2Body(Client* client, Member member, M1 m1, M2 m2 )
        DNB_THROW_SPEC_ANY;
        
    virtual ~_DNBMemberFunctor3M2Body()
        DNB_THROW_SPEC_ANY;

    virtual
    void
    operator()( A1 a1, A2 a2, A3 a3 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor3Body< A1, A2, A3 >* 
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBMemberFunctor3M2Body()
        DNB_THROW_SPEC_ANY;

    Client* client_;
    Member member_;

   M1       m1_;
    M2       m2_;

};

template< class A1, class A2, class A3 >
class  DNBFunctor3  : public DNBBaseFunctor
{
public:
    DNBFunctor3(_DNBBaseFunctor3Body< A1, A2, A3 >* body = NULL)
        DNB_THROW_SPEC_ANY;

    DNBFunctor3(const DNBFunctor3< A1, A2, A3 >& DNBFunctor)
        DNB_THROW_SPEC_ANY;

    virtual
    ~DNBFunctor3()
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctorBody*
    getBody() const
        DNB_THROW_SPEC_ANY;

    DNBFunctor3< A1, A2, A3 >&
    operator=(const DNBFunctor3< A1, A2, A3 >& DNBFunctor)
        DNB_THROW_SPEC(( scl_bad_alloc ));

    void
    operator()( A1 a1, A2 a2, A3 a3 )
        DNB_THROW_SPEC_ANY;

private:
    _DNBBaseFunctor3Body< A1, A2, A3 >* body_;
};

template< class A1, class A2, class A3, class Function >
DNBFunctor3< A1, A2, A3 >
DNBMakeFunctorG(DNBFunctor3< A1, A2, A3 >*, Function f)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBFunction3Body< A1, A2, A3, Function>* body =
        DNB_NEW _DNBFunction3Body< A1, A2, A3, Function>(f);
    return DNBFunctor3< A1, A2, A3 >(body);
}

template< class A1, class A2, class A3, class Function, class M1 >
DNBFunctor3< A1, A2, A3 >
DNBMakeFunctorG(DNBFunctor3< A1, A2, A3 >*, Function f, M1 m1 )
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBFunction3M1Body< A1, A2, A3, Function, M1 >* body =
        DNB_NEW _DNBFunction3M1Body< A1, A2, A3, Function, M1 >(f, m1);
    return DNBFunctor3< A1, A2, A3 >(body);
}

template< class A1, class A2, class A3, class Function, class M1, class M2 >
DNBFunctor3< A1, A2, A3 >
DNBMakeFunctorG(DNBFunctor3< A1, A2, A3 >*, Function f, M1 m1, M2 m2 )
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBFunction3M2Body< A1, A2, A3, Function, M1, M2 >* body =
        DNB_NEW _DNBFunction3M2Body< A1, A2, A3, Function, M1, M2 >(f, m1, m2);
    return DNBFunctor3< A1, A2, A3 >(body);
}

template< class A1, class A2, class A3, class Client, class Member >
DNBFunctor3< A1, A2, A3 >
DNBMakeFunctorM(DNBFunctor3< A1, A2, A3 >*,Client* client, Member member)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBMemberFunctor3Body< A1, A2, A3, Client, Member >* body =
        DNB_NEW _DNBMemberFunctor3Body< A1, A2, A3, Client, Member >(client,member);
    return DNBFunctor3< A1, A2, A3 >(body);
}

template< class A1, class A2, class A3, class Client, class Member, class M1>
DNBFunctor3< A1, A2, A3 >
DNBMakeFunctorM(DNBFunctor3< A1, A2, A3 > *, Client* client, Member member, M1 m1)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBMemberFunctor3M1Body< A1, A2, A3, Client, Member, M1 >* body =
        DNB_NEW _DNBMemberFunctor3M1Body< A1, A2, A3, Client, Member, M1 >( client, member, m1 );
    return DNBFunctor3< A1, A2, A3 > (body);
}

template< class A1, class A2, class A3, class Client, class Member, class M1, class M2>
DNBFunctor3< A1, A2, A3 >
DNBMakeFunctorM(DNBFunctor3< A1, A2, A3 > *, Client* client, Member member, M1 m1, M2 m2)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBMemberFunctor3M2Body< A1, A2, A3, Client, Member, M1, M2 >* body =
        DNB_NEW _DNBMemberFunctor3M2Body< A1, A2, A3, Client, Member, M1, M2 >( client, member, m1, m2 );
    return DNBFunctor3< A1, A2, A3 > (body);
}

/////////////////////////////////////////////////////////
//
// 4 Passed,  0 Returned 
//
/////////////////////////////////////////////////////////

template< class A1, class A2, class A3, class A4 >
class  _DNBBaseFunctor4Body : public _DNBBaseFunctorBody
{
public:
    virtual 
    void 
    operator()( A1 a1, A2 a2, A3 a3, A4 a4 )
        DNB_THROW_SPEC_ANY    = 0;

    virtual
    _DNBBaseFunctor4Body< A1, A2, A3, A4 >*
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ))    = 0;
};

template< class A1, class A2, class A3, class A4, class Function >
class _DNBFunction4Body : public _DNBBaseFunctor4Body< A1, A2, A3, A4 >
{
public:
    _DNBFunction4Body(const _DNBFunction4Body< A1, A2, A3, A4, Function>& functionFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBFunction4Body(const Function& f)
        DNB_THROW_SPEC_ANY;

    virtual
    ~_DNBFunction4Body()
        DNB_THROW_SPEC_ANY;

    virtual
    void
    operator()( A1 a1, A2 a2, A3 a3, A4 a4 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const

        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor4Body< A1, A2, A3, A4 >* 
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBFunction4Body()
        DNB_THROW_SPEC_ANY;

    Function f_;
};

template< class A1, class A2, class A3, class A4, class Function, class M1 >
class _DNBFunction4M1Body : public _DNBBaseFunctor4Body< A1, A2, A3, A4 >
{
public:
    _DNBFunction4M1Body(const _DNBFunction4M1Body< A1, A2, A3, A4, Function, M1 >& functionFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBFunction4M1Body(const Function& f, M1 m1 )
        DNB_THROW_SPEC_ANY;

    virtual
    ~_DNBFunction4M1Body()
        DNB_THROW_SPEC_ANY;

    virtual
    void
    operator()( A1 a1, A2 a2, A3 a3, A4 a4 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor4Body< A1, A2, A3, A4 >*
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBFunction4M1Body()
        DNB_THROW_SPEC_ANY;

    Function f_;
    M1       m1_;

};

template< class A1, class A2, class A3, class A4, class Client, class Member >
class _DNBMemberFunctor4Body:
public _DNBBaseFunctor4Body< A1, A2, A3, A4 >
{
public:
    _DNBMemberFunctor4Body( const _DNBMemberFunctor4Body< A1, A2, A3, A4, Client, Member >& memberFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBMemberFunctor4Body(Client* client, Member member)
        DNB_THROW_SPEC_ANY;

    virtual ~_DNBMemberFunctor4Body()
        DNB_THROW_SPEC_ANY;

    virtual
    void
    operator()( A1 a1, A2 a2, A3 a3, A4 a4 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor4Body< A1, A2, A3, A4 >*
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBMemberFunctor4Body()
        DNB_THROW_SPEC_ANY;

    Client* client_;
    Member member_;
};

template< class A1, class A2, class A3, class A4, class Client, class Member, class M1 >
class _DNBMemberFunctor4M1Body:
public _DNBBaseFunctor4Body< A1, A2, A3, A4 >
{
public:
    _DNBMemberFunctor4M1Body( const _DNBMemberFunctor4M1Body< A1, A2, A3, A4, Client, Member, M1 >& memberFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBMemberFunctor4M1Body(Client* client, Member member, M1 m1 )
        DNB_THROW_SPEC_ANY;
        
    virtual ~_DNBMemberFunctor4M1Body()
        DNB_THROW_SPEC_ANY;

    virtual
    void
    operator()( A1 a1, A2 a2, A3 a3, A4 a4 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor4Body< A1, A2, A3, A4 >* 
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBMemberFunctor4M1Body()
        DNB_THROW_SPEC_ANY;

    Client* client_;
    Member member_;

   M1       m1_;

};

template< class A1, class A2, class A3, class A4 >
class  DNBFunctor4  : public DNBBaseFunctor
{
public:
    DNBFunctor4(_DNBBaseFunctor4Body< A1, A2, A3, A4 >* body = NULL)
        DNB_THROW_SPEC_ANY;

    DNBFunctor4(const DNBFunctor4< A1, A2, A3, A4 >& DNBFunctor)
        DNB_THROW_SPEC_ANY;

    virtual
    ~DNBFunctor4()
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctorBody*
    getBody() const
        DNB_THROW_SPEC_ANY;

    DNBFunctor4< A1, A2, A3, A4 >&
    operator=(const DNBFunctor4< A1, A2, A3, A4 >& DNBFunctor)
        DNB_THROW_SPEC(( scl_bad_alloc ));

    void
    operator()( A1 a1, A2 a2, A3 a3, A4 a4 )
        DNB_THROW_SPEC_ANY;

private:
    _DNBBaseFunctor4Body< A1, A2, A3, A4 >* body_;
};

template< class A1, class A2, class A3, class A4, class Function >
DNBFunctor4< A1, A2, A3, A4 >
DNBMakeFunctorG(DNBFunctor4< A1, A2, A3, A4 >*, Function f)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBFunction4Body< A1, A2, A3, A4, Function>* body =
        DNB_NEW _DNBFunction4Body< A1, A2, A3, A4, Function>(f);
    return DNBFunctor4< A1, A2, A3, A4 >(body);
}

template< class A1, class A2, class A3, class A4, class Function, class M1 >
DNBFunctor4< A1, A2, A3, A4 >
DNBMakeFunctorG(DNBFunctor4< A1, A2, A3, A4 >*, Function f, M1 m1 )
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBFunction4M1Body< A1, A2, A3, A4, Function, M1 >* body =
        DNB_NEW _DNBFunction4M1Body< A1, A2, A3, A4, Function, M1 >(f, m1);
    return DNBFunctor4< A1, A2, A3, A4 >(body);
}

template< class A1, class A2, class A3, class A4, class Client, class Member >
DNBFunctor4< A1, A2, A3, A4 >
DNBMakeFunctorM(DNBFunctor4< A1, A2, A3, A4 >*,Client* client, Member member)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBMemberFunctor4Body< A1, A2, A3, A4, Client, Member >* body =
        DNB_NEW _DNBMemberFunctor4Body< A1, A2, A3, A4, Client, Member >(client,member);
    return DNBFunctor4< A1, A2, A3, A4 >(body);
}

template< class A1, class A2, class A3, class A4, class Client, class Member, class M1>
DNBFunctor4< A1, A2, A3, A4 >
DNBMakeFunctorM(DNBFunctor4< A1, A2, A3, A4 > *, Client* client, Member member, M1 m1)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBMemberFunctor4M1Body< A1, A2, A3, A4, Client, Member, M1 >* body =
        DNB_NEW _DNBMemberFunctor4M1Body< A1, A2, A3, A4, Client, Member, M1 >( client, member, m1 );
    return DNBFunctor4< A1, A2, A3, A4 > (body);
}

/////////////////////////////////////////////////////////
//
// 5 Passed,  0 Returned 
//
/////////////////////////////////////////////////////////

template< class A1, class A2, class A3, class A4, class A5 >
class  _DNBBaseFunctor5Body : public _DNBBaseFunctorBody
{
public:
    virtual 
    void 
    operator()( A1 a1, A2 a2, A3 a3, A4 a4, A5 a5 )
        DNB_THROW_SPEC_ANY    = 0;

    virtual
    _DNBBaseFunctor5Body< A1, A2, A3, A4, A5 >*
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ))    = 0;
};

template< class A1, class A2, class A3, class A4, class A5, class Function >
class _DNBFunction5Body : public _DNBBaseFunctor5Body< A1, A2, A3, A4, A5 >
{
public:
    _DNBFunction5Body(const _DNBFunction5Body< A1, A2, A3, A4, A5, Function>& functionFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBFunction5Body(const Function& f)
        DNB_THROW_SPEC_ANY;

    virtual
    ~_DNBFunction5Body()
        DNB_THROW_SPEC_ANY;

    virtual
    void
    operator()( A1 a1, A2 a2, A3 a3, A4 a4, A5 a5 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const

        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor5Body< A1, A2, A3, A4, A5 >* 
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBFunction5Body()
        DNB_THROW_SPEC_ANY;

    Function f_;
};

template< class A1, class A2, class A3, class A4, class A5, class Client, class Member >
class _DNBMemberFunctor5Body:
public _DNBBaseFunctor5Body< A1, A2, A3, A4, A5 >
{
public:
    _DNBMemberFunctor5Body( const _DNBMemberFunctor5Body< A1, A2, A3, A4, A5, Client, Member >& memberFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBMemberFunctor5Body(Client* client, Member member)
        DNB_THROW_SPEC_ANY;

    virtual ~_DNBMemberFunctor5Body()
        DNB_THROW_SPEC_ANY;

    virtual
    void
    operator()( A1 a1, A2 a2, A3 a3, A4 a4, A5 a5 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor5Body< A1, A2, A3, A4, A5 >*
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBMemberFunctor5Body()
        DNB_THROW_SPEC_ANY;

    Client* client_;
    Member member_;
};

template< class A1, class A2, class A3, class A4, class A5 >
class  DNBFunctor5  : public DNBBaseFunctor
{
public:
    DNBFunctor5(_DNBBaseFunctor5Body< A1, A2, A3, A4, A5 >* body = NULL)
        DNB_THROW_SPEC_ANY;

    DNBFunctor5(const DNBFunctor5< A1, A2, A3, A4, A5 >& DNBFunctor)
        DNB_THROW_SPEC_ANY;

    virtual
    ~DNBFunctor5()
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctorBody*
    getBody() const
        DNB_THROW_SPEC_ANY;

    DNBFunctor5< A1, A2, A3, A4, A5 >&
    operator=(const DNBFunctor5< A1, A2, A3, A4, A5 >& DNBFunctor)
        DNB_THROW_SPEC(( scl_bad_alloc ));

    void
    operator()( A1 a1, A2 a2, A3 a3, A4 a4, A5 a5 )
        DNB_THROW_SPEC_ANY;

private:
    _DNBBaseFunctor5Body< A1, A2, A3, A4, A5 >* body_;
};

template< class A1, class A2, class A3, class A4, class A5, class Function >
DNBFunctor5< A1, A2, A3, A4, A5 >
DNBMakeFunctorG(DNBFunctor5< A1, A2, A3, A4, A5 >*, Function f)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBFunction5Body< A1, A2, A3, A4, A5, Function>* body =
        DNB_NEW _DNBFunction5Body< A1, A2, A3, A4, A5, Function>(f);
    return DNBFunctor5< A1, A2, A3, A4, A5 >(body);
}

template< class A1, class A2, class A3, class A4, class A5, class Client, class Member >
DNBFunctor5< A1, A2, A3, A4, A5 >
DNBMakeFunctorM(DNBFunctor5< A1, A2, A3, A4, A5 >*,Client* client, Member member)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBMemberFunctor5Body< A1, A2, A3, A4, A5, Client, Member >* body =
        DNB_NEW _DNBMemberFunctor5Body< A1, A2, A3, A4, A5, Client, Member >(client,member);
    return DNBFunctor5< A1, A2, A3, A4, A5 >(body);
}

/////////////////////////////////////////////////////////
//
// 0 Passed,  1 Returned 
//
/////////////////////////////////////////////////////////

template< class R >
class  _DNBBaseFunctor0wRetBody : public _DNBBaseFunctorBody
{
public:
    virtual 
    R 
    operator()(  )
        DNB_THROW_SPEC_ANY    = 0;

    virtual
    _DNBBaseFunctor0wRetBody< R >*
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ))    = 0;
};

template< class R, class Function >
class _DNBFunction0wRetBody : public _DNBBaseFunctor0wRetBody< R >
{
public:
    _DNBFunction0wRetBody(const _DNBFunction0wRetBody< R, Function>& functionFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBFunction0wRetBody(const Function& f)
        DNB_THROW_SPEC_ANY;

    virtual
    ~_DNBFunction0wRetBody()
        DNB_THROW_SPEC_ANY;

    virtual
    R
    operator()(  )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const

        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor0wRetBody< R >* 
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBFunction0wRetBody()
        DNB_THROW_SPEC_ANY;

    Function f_;
};

template< class R, class Function, class M1 >
class _DNBFunction0wRetM1Body : public _DNBBaseFunctor0wRetBody< R >
{
public:
    _DNBFunction0wRetM1Body(const _DNBFunction0wRetM1Body< R, Function, M1 >& functionFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBFunction0wRetM1Body(const Function& f, M1 m1 )
        DNB_THROW_SPEC_ANY;

    virtual
    ~_DNBFunction0wRetM1Body()
        DNB_THROW_SPEC_ANY;

    virtual
    R
    operator()(  )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor0wRetBody< R >*
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBFunction0wRetM1Body()
        DNB_THROW_SPEC_ANY;

    Function f_;
    M1       m1_;

};

template< class R, class Function, class M1, class M2 >
class _DNBFunction0wRetM2Body : public _DNBBaseFunctor0wRetBody< R >
{
public:
    _DNBFunction0wRetM2Body(const _DNBFunction0wRetM2Body< R, Function, M1, M2 >& functionFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBFunction0wRetM2Body(const Function& f, M1 m1, M2 m2 )
        DNB_THROW_SPEC_ANY;

    virtual
    ~_DNBFunction0wRetM2Body()
        DNB_THROW_SPEC_ANY;

    virtual
    R
    operator()(  )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor0wRetBody< R >*
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBFunction0wRetM2Body()
        DNB_THROW_SPEC_ANY;

    Function f_;
    M1       m1_;
    M2       m2_;

};

template< class R, class Function, class M1, class M2, class M3 >
class _DNBFunction0wRetM3Body : public _DNBBaseFunctor0wRetBody< R >
{
public:
    _DNBFunction0wRetM3Body(const _DNBFunction0wRetM3Body< R, Function, M1, M2, M3 >& functionFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBFunction0wRetM3Body(const Function& f, M1 m1, M2 m2, M3 m3 )
        DNB_THROW_SPEC_ANY;

    virtual
    ~_DNBFunction0wRetM3Body()
        DNB_THROW_SPEC_ANY;

    virtual
    R
    operator()(  )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor0wRetBody< R >*
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBFunction0wRetM3Body()
        DNB_THROW_SPEC_ANY;

    Function f_;
    M1       m1_;
    M2       m2_;
    M3       m3_;

};

template< class R, class Function, class M1, class M2, class M3, class M4 >
class _DNBFunction0wRetM4Body : public _DNBBaseFunctor0wRetBody< R >
{
public:
    _DNBFunction0wRetM4Body(const _DNBFunction0wRetM4Body< R, Function, M1, M2, M3, M4 >& functionFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBFunction0wRetM4Body(const Function& f, M1 m1, M2 m2, M3 m3, M4 m4 )
        DNB_THROW_SPEC_ANY;

    virtual
    ~_DNBFunction0wRetM4Body()
        DNB_THROW_SPEC_ANY;

    virtual
    R
    operator()(  )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor0wRetBody< R >*
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBFunction0wRetM4Body()
        DNB_THROW_SPEC_ANY;

    Function f_;
    M1       m1_;
    M2       m2_;
    M3       m3_;
    M4       m4_;

};

template< class R, class Function, class M1, class M2, class M3, class M4, class M5 >
class _DNBFunction0wRetM5Body : public _DNBBaseFunctor0wRetBody< R >
{
public:
    _DNBFunction0wRetM5Body(const _DNBFunction0wRetM5Body< R, Function, M1, M2, M3, M4, M5 >& functionFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBFunction0wRetM5Body(const Function& f, M1 m1, M2 m2, M3 m3, M4 m4, M5 m5 )
        DNB_THROW_SPEC_ANY;

    virtual
    ~_DNBFunction0wRetM5Body()
        DNB_THROW_SPEC_ANY;

    virtual
    R
    operator()(  )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor0wRetBody< R >*
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBFunction0wRetM5Body()
        DNB_THROW_SPEC_ANY;

    Function f_;
    M1       m1_;
    M2       m2_;
    M3       m3_;
    M4       m4_;
    M5       m5_;

};

template< class R, class Client, class Member >
class _DNBMemberFunctor0wRetBody:
public _DNBBaseFunctor0wRetBody< R >
{
public:
    _DNBMemberFunctor0wRetBody( const _DNBMemberFunctor0wRetBody< R, Client, Member >& memberFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBMemberFunctor0wRetBody(Client* client, Member member)
        DNB_THROW_SPEC_ANY;

    virtual ~_DNBMemberFunctor0wRetBody()
        DNB_THROW_SPEC_ANY;

    virtual
    R
    operator()(  )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor0wRetBody< R >*
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBMemberFunctor0wRetBody()
        DNB_THROW_SPEC_ANY;

    Client* client_;
    Member member_;
};

template< class R, class Client, class Member, class M1 >
class _DNBMemberFunctor0wRetM1Body:
public _DNBBaseFunctor0wRetBody< R >
{
public:
    _DNBMemberFunctor0wRetM1Body( const _DNBMemberFunctor0wRetM1Body< R, Client, Member, M1 >& memberFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBMemberFunctor0wRetM1Body(Client* client, Member member, M1 m1 )
        DNB_THROW_SPEC_ANY;
        
    virtual ~_DNBMemberFunctor0wRetM1Body()
        DNB_THROW_SPEC_ANY;

    virtual
    R
    operator()(  )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor0wRetBody< R >* 
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBMemberFunctor0wRetM1Body()
        DNB_THROW_SPEC_ANY;

    Client* client_;
    Member member_;

   M1       m1_;

};

template< class R, class Client, class Member, class M1, class M2 >
class _DNBMemberFunctor0wRetM2Body:
public _DNBBaseFunctor0wRetBody< R >
{
public:
    _DNBMemberFunctor0wRetM2Body( const _DNBMemberFunctor0wRetM2Body< R, Client, Member, M1, M2 >& memberFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBMemberFunctor0wRetM2Body(Client* client, Member member, M1 m1, M2 m2 )
        DNB_THROW_SPEC_ANY;
        
    virtual ~_DNBMemberFunctor0wRetM2Body()
        DNB_THROW_SPEC_ANY;

    virtual
    R
    operator()(  )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor0wRetBody< R >* 
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBMemberFunctor0wRetM2Body()
        DNB_THROW_SPEC_ANY;

    Client* client_;
    Member member_;

   M1       m1_;
    M2       m2_;

};

template< class R, class Client, class Member, class M1, class M2, class M3 >
class _DNBMemberFunctor0wRetM3Body:
public _DNBBaseFunctor0wRetBody< R >
{
public:
    _DNBMemberFunctor0wRetM3Body( const _DNBMemberFunctor0wRetM3Body< R, Client, Member, M1, M2, M3 >& memberFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBMemberFunctor0wRetM3Body(Client* client, Member member, M1 m1, M2 m2, M3 m3 )
        DNB_THROW_SPEC_ANY;
        
    virtual ~_DNBMemberFunctor0wRetM3Body()
        DNB_THROW_SPEC_ANY;

    virtual
    R
    operator()(  )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor0wRetBody< R >* 
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBMemberFunctor0wRetM3Body()
        DNB_THROW_SPEC_ANY;

    Client* client_;
    Member member_;

   M1       m1_;
    M2       m2_;
    M3       m3_;

};

template< class R, class Client, class Member, class M1, class M2, class M3, class M4 >
class _DNBMemberFunctor0wRetM4Body:
public _DNBBaseFunctor0wRetBody< R >
{
public:
    _DNBMemberFunctor0wRetM4Body( const _DNBMemberFunctor0wRetM4Body< R, Client, Member, M1, M2, M3, M4 >& memberFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBMemberFunctor0wRetM4Body(Client* client, Member member, M1 m1, M2 m2, M3 m3, M4 m4 )
        DNB_THROW_SPEC_ANY;
        
    virtual ~_DNBMemberFunctor0wRetM4Body()
        DNB_THROW_SPEC_ANY;

    virtual
    R
    operator()(  )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor0wRetBody< R >* 
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBMemberFunctor0wRetM4Body()
        DNB_THROW_SPEC_ANY;

    Client* client_;
    Member member_;

   M1       m1_;
    M2       m2_;
    M3       m3_;
    M4       m4_;

};

template< class R, class Client, class Member, class M1, class M2, class M3, class M4, class M5 >
class _DNBMemberFunctor0wRetM5Body:
public _DNBBaseFunctor0wRetBody< R >
{
public:
    _DNBMemberFunctor0wRetM5Body( const _DNBMemberFunctor0wRetM5Body< R, Client, Member, M1, M2, M3, M4, M5 >& memberFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBMemberFunctor0wRetM5Body(Client* client, Member member, M1 m1, M2 m2, M3 m3, M4 m4, M5 m5 )
        DNB_THROW_SPEC_ANY;
        
    virtual ~_DNBMemberFunctor0wRetM5Body()
        DNB_THROW_SPEC_ANY;

    virtual
    R
    operator()(  )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor0wRetBody< R >* 
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBMemberFunctor0wRetM5Body()
        DNB_THROW_SPEC_ANY;

    Client* client_;
    Member member_;

   M1       m1_;
    M2       m2_;
    M3       m3_;
    M4       m4_;
    M5       m5_;

};

template< class R >
class  DNBFunctor0wRet  : public DNBBaseFunctor
{
public:
    DNBFunctor0wRet(_DNBBaseFunctor0wRetBody< R >* body = NULL)
        DNB_THROW_SPEC_ANY;

    DNBFunctor0wRet(const DNBFunctor0wRet< R >& DNBFunctor)
        DNB_THROW_SPEC_ANY;

    virtual
    ~DNBFunctor0wRet()
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctorBody*
    getBody() const
        DNB_THROW_SPEC_ANY;

    DNBFunctor0wRet< R >&
    operator=(const DNBFunctor0wRet< R >& DNBFunctor)
        DNB_THROW_SPEC(( scl_bad_alloc ));

    R
    operator()(  )
        DNB_THROW_SPEC_ANY;

private:
    _DNBBaseFunctor0wRetBody< R >* body_;
};

template< class R, class Function >
DNBFunctor0wRet< R >
DNBMakeFunctorG(DNBFunctor0wRet< R >*, Function f)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBFunction0wRetBody< R, Function>* body =
        DNB_NEW _DNBFunction0wRetBody< R, Function>(f);
    return DNBFunctor0wRet< R >(body);
}

template< class R, class Function, class M1 >
DNBFunctor0wRet< R >
DNBMakeFunctorG(DNBFunctor0wRet< R >*, Function f, M1 m1 )
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBFunction0wRetM1Body< R, Function, M1 >* body =
        DNB_NEW _DNBFunction0wRetM1Body< R, Function, M1 >(f, m1);
    return DNBFunctor0wRet< R >(body);
}

template< class R, class Function, class M1, class M2 >
DNBFunctor0wRet< R >
DNBMakeFunctorG(DNBFunctor0wRet< R >*, Function f, M1 m1, M2 m2 )
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBFunction0wRetM2Body< R, Function, M1, M2 >* body =
        DNB_NEW _DNBFunction0wRetM2Body< R, Function, M1, M2 >(f, m1, m2);
    return DNBFunctor0wRet< R >(body);
}

template< class R, class Function, class M1, class M2, class M3 >
DNBFunctor0wRet< R >
DNBMakeFunctorG(DNBFunctor0wRet< R >*, Function f, M1 m1, M2 m2, M3 m3 )
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBFunction0wRetM3Body< R, Function, M1, M2, M3 >* body =
        DNB_NEW _DNBFunction0wRetM3Body< R, Function, M1, M2, M3 >(f, m1, m2, m3);
    return DNBFunctor0wRet< R >(body);
}

template< class R, class Function, class M1, class M2, class M3, class M4 >
DNBFunctor0wRet< R >
DNBMakeFunctorG(DNBFunctor0wRet< R >*, Function f, M1 m1, M2 m2, M3 m3, M4 m4 )
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBFunction0wRetM4Body< R, Function, M1, M2, M3, M4 >* body =
        DNB_NEW _DNBFunction0wRetM4Body< R, Function, M1, M2, M3, M4 >(f, m1, m2, m3, m4);
    return DNBFunctor0wRet< R >(body);
}

template< class R, class Function, class M1, class M2, class M3, class M4, class M5 >
DNBFunctor0wRet< R >
DNBMakeFunctorG(DNBFunctor0wRet< R >*, Function f, M1 m1, M2 m2, M3 m3, M4 m4, M5 m5 )
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBFunction0wRetM5Body< R, Function, M1, M2, M3, M4, M5 >* body =
        DNB_NEW _DNBFunction0wRetM5Body< R, Function, M1, M2, M3, M4, M5 >(f, m1, m2, m3, m4, m5);
    return DNBFunctor0wRet< R >(body);
}

template< class R, class Client, class Member >
DNBFunctor0wRet< R >
DNBMakeFunctorM(DNBFunctor0wRet< R >*,Client* client, Member member)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBMemberFunctor0wRetBody< R, Client, Member >* body =
        DNB_NEW _DNBMemberFunctor0wRetBody< R, Client, Member >(client,member);
    return DNBFunctor0wRet< R >(body);
}

template< class R, class Client, class Member, class M1>
DNBFunctor0wRet< R >
DNBMakeFunctorM(DNBFunctor0wRet< R > *, Client* client, Member member, M1 m1)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBMemberFunctor0wRetM1Body< R, Client, Member, M1 >* body =
        DNB_NEW _DNBMemberFunctor0wRetM1Body< R, Client, Member, M1 >( client, member, m1 );
    return DNBFunctor0wRet< R > (body);
}

template< class R, class Client, class Member, class M1, class M2>
DNBFunctor0wRet< R >
DNBMakeFunctorM(DNBFunctor0wRet< R > *, Client* client, Member member, M1 m1, M2 m2)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBMemberFunctor0wRetM2Body< R, Client, Member, M1, M2 >* body =
        DNB_NEW _DNBMemberFunctor0wRetM2Body< R, Client, Member, M1, M2 >( client, member, m1, m2 );
    return DNBFunctor0wRet< R > (body);
}

template< class R, class Client, class Member, class M1, class M2, class M3>
DNBFunctor0wRet< R >
DNBMakeFunctorM(DNBFunctor0wRet< R > *, Client* client, Member member, M1 m1, M2 m2, M3 m3)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBMemberFunctor0wRetM3Body< R, Client, Member, M1, M2, M3 >* body =
        DNB_NEW _DNBMemberFunctor0wRetM3Body< R, Client, Member, M1, M2, M3 >( client, member, m1, m2, m3 );
    return DNBFunctor0wRet< R > (body);
}

template< class R, class Client, class Member, class M1, class M2, class M3, class M4>
DNBFunctor0wRet< R >
DNBMakeFunctorM(DNBFunctor0wRet< R > *, Client* client, Member member, M1 m1, M2 m2, M3 m3, M4 m4)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBMemberFunctor0wRetM4Body< R, Client, Member, M1, M2, M3, M4 >* body =
        DNB_NEW _DNBMemberFunctor0wRetM4Body< R, Client, Member, M1, M2, M3, M4 >( client, member, m1, m2, m3, m4 );
    return DNBFunctor0wRet< R > (body);
}

template< class R, class Client, class Member, class M1, class M2, class M3, class M4, class M5>
DNBFunctor0wRet< R >
DNBMakeFunctorM(DNBFunctor0wRet< R > *, Client* client, Member member, M1 m1, M2 m2, M3 m3, M4 m4, M5 m5)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBMemberFunctor0wRetM5Body< R, Client, Member, M1, M2, M3, M4, M5 >* body =
        DNB_NEW _DNBMemberFunctor0wRetM5Body< R, Client, Member, M1, M2, M3, M4, M5 >( client, member, m1, m2, m3, m4, m5 );
    return DNBFunctor0wRet< R > (body);
}

/////////////////////////////////////////////////////////
//
// 1 Passed,  1 Returned 
//
/////////////////////////////////////////////////////////

template< class R, class A1  >
class  _DNBBaseFunctor1wRetBody : public _DNBBaseFunctorBody
{
public:
    virtual 
    R 
    operator()( A1 a1 )
        DNB_THROW_SPEC_ANY    = 0;

    virtual
    _DNBBaseFunctor1wRetBody< R, A1 >*
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ))    = 0;
};

template< class R, class A1, class Function >
class _DNBFunction1wRetBody : public _DNBBaseFunctor1wRetBody< R, A1 >
{
public:
    _DNBFunction1wRetBody(const _DNBFunction1wRetBody< R, A1, Function>& functionFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBFunction1wRetBody(const Function& f)
        DNB_THROW_SPEC_ANY;

    virtual
    ~_DNBFunction1wRetBody()
        DNB_THROW_SPEC_ANY;

    virtual
    R
    operator()( A1 a1 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const

        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor1wRetBody< R, A1 >* 
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBFunction1wRetBody()
        DNB_THROW_SPEC_ANY;

    Function f_;
};

template< class R, class A1, class Function, class M1 >
class _DNBFunction1wRetM1Body : public _DNBBaseFunctor1wRetBody< R, A1 >
{
public:
    _DNBFunction1wRetM1Body(const _DNBFunction1wRetM1Body< R, A1, Function, M1 >& functionFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBFunction1wRetM1Body(const Function& f, M1 m1 )
        DNB_THROW_SPEC_ANY;

    virtual
    ~_DNBFunction1wRetM1Body()
        DNB_THROW_SPEC_ANY;

    virtual
    R
    operator()( A1 a1 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor1wRetBody< R, A1 >*
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBFunction1wRetM1Body()
        DNB_THROW_SPEC_ANY;

    Function f_;
    M1       m1_;

};

template< class R, class A1, class Function, class M1, class M2 >
class _DNBFunction1wRetM2Body : public _DNBBaseFunctor1wRetBody< R, A1 >
{
public:
    _DNBFunction1wRetM2Body(const _DNBFunction1wRetM2Body< R, A1, Function, M1, M2 >& functionFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBFunction1wRetM2Body(const Function& f, M1 m1, M2 m2 )
        DNB_THROW_SPEC_ANY;

    virtual
    ~_DNBFunction1wRetM2Body()
        DNB_THROW_SPEC_ANY;

    virtual
    R
    operator()( A1 a1 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor1wRetBody< R, A1 >*
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBFunction1wRetM2Body()
        DNB_THROW_SPEC_ANY;

    Function f_;
    M1       m1_;
    M2       m2_;

};

template< class R, class A1, class Function, class M1, class M2, class M3 >
class _DNBFunction1wRetM3Body : public _DNBBaseFunctor1wRetBody< R, A1 >
{
public:
    _DNBFunction1wRetM3Body(const _DNBFunction1wRetM3Body< R, A1, Function, M1, M2, M3 >& functionFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBFunction1wRetM3Body(const Function& f, M1 m1, M2 m2, M3 m3 )
        DNB_THROW_SPEC_ANY;

    virtual
    ~_DNBFunction1wRetM3Body()
        DNB_THROW_SPEC_ANY;

    virtual
    R
    operator()( A1 a1 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor1wRetBody< R, A1 >*
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBFunction1wRetM3Body()
        DNB_THROW_SPEC_ANY;

    Function f_;
    M1       m1_;
    M2       m2_;
    M3       m3_;

};

template< class R, class A1, class Function, class M1, class M2, class M3, class M4 >
class _DNBFunction1wRetM4Body : public _DNBBaseFunctor1wRetBody< R, A1 >
{
public:
    _DNBFunction1wRetM4Body(const _DNBFunction1wRetM4Body< R, A1, Function, M1, M2, M3, M4 >& functionFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBFunction1wRetM4Body(const Function& f, M1 m1, M2 m2, M3 m3, M4 m4 )
        DNB_THROW_SPEC_ANY;

    virtual
    ~_DNBFunction1wRetM4Body()
        DNB_THROW_SPEC_ANY;

    virtual
    R
    operator()( A1 a1 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor1wRetBody< R, A1 >*
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBFunction1wRetM4Body()
        DNB_THROW_SPEC_ANY;

    Function f_;
    M1       m1_;
    M2       m2_;
    M3       m3_;
    M4       m4_;

};

template< class R, class A1, class Client, class Member >
class _DNBMemberFunctor1wRetBody:
public _DNBBaseFunctor1wRetBody< R, A1 >
{
public:
    _DNBMemberFunctor1wRetBody( const _DNBMemberFunctor1wRetBody< R, A1, Client, Member >& memberFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBMemberFunctor1wRetBody(Client* client, Member member)
        DNB_THROW_SPEC_ANY;

    virtual ~_DNBMemberFunctor1wRetBody()
        DNB_THROW_SPEC_ANY;

    virtual
    R
    operator()( A1 a1 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor1wRetBody< R, A1 >*
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBMemberFunctor1wRetBody()
        DNB_THROW_SPEC_ANY;

    Client* client_;
    Member member_;
};

template< class R, class A1, class Client, class Member, class M1 >
class _DNBMemberFunctor1wRetM1Body:
public _DNBBaseFunctor1wRetBody< R, A1 >
{
public:
    _DNBMemberFunctor1wRetM1Body( const _DNBMemberFunctor1wRetM1Body< R, A1, Client, Member, M1 >& memberFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBMemberFunctor1wRetM1Body(Client* client, Member member, M1 m1 )
        DNB_THROW_SPEC_ANY;
        
    virtual ~_DNBMemberFunctor1wRetM1Body()
        DNB_THROW_SPEC_ANY;

    virtual
    R
    operator()( A1 a1 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor1wRetBody< R, A1 >* 
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBMemberFunctor1wRetM1Body()
        DNB_THROW_SPEC_ANY;

    Client* client_;
    Member member_;

   M1       m1_;

};

template< class R, class A1, class Client, class Member, class M1, class M2 >
class _DNBMemberFunctor1wRetM2Body:
public _DNBBaseFunctor1wRetBody< R, A1 >
{
public:
    _DNBMemberFunctor1wRetM2Body( const _DNBMemberFunctor1wRetM2Body< R, A1, Client, Member, M1, M2 >& memberFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBMemberFunctor1wRetM2Body(Client* client, Member member, M1 m1, M2 m2 )
        DNB_THROW_SPEC_ANY;
        
    virtual ~_DNBMemberFunctor1wRetM2Body()
        DNB_THROW_SPEC_ANY;

    virtual
    R
    operator()( A1 a1 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor1wRetBody< R, A1 >* 
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBMemberFunctor1wRetM2Body()
        DNB_THROW_SPEC_ANY;

    Client* client_;
    Member member_;

   M1       m1_;
    M2       m2_;

};

template< class R, class A1, class Client, class Member, class M1, class M2, class M3 >
class _DNBMemberFunctor1wRetM3Body:
public _DNBBaseFunctor1wRetBody< R, A1 >
{
public:
    _DNBMemberFunctor1wRetM3Body( const _DNBMemberFunctor1wRetM3Body< R, A1, Client, Member, M1, M2, M3 >& memberFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBMemberFunctor1wRetM3Body(Client* client, Member member, M1 m1, M2 m2, M3 m3 )
        DNB_THROW_SPEC_ANY;
        
    virtual ~_DNBMemberFunctor1wRetM3Body()
        DNB_THROW_SPEC_ANY;

    virtual
    R
    operator()( A1 a1 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor1wRetBody< R, A1 >* 
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBMemberFunctor1wRetM3Body()
        DNB_THROW_SPEC_ANY;

    Client* client_;
    Member member_;

   M1       m1_;
    M2       m2_;
    M3       m3_;

};

template< class R, class A1, class Client, class Member, class M1, class M2, class M3, class M4 >
class _DNBMemberFunctor1wRetM4Body:
public _DNBBaseFunctor1wRetBody< R, A1 >
{
public:
    _DNBMemberFunctor1wRetM4Body( const _DNBMemberFunctor1wRetM4Body< R, A1, Client, Member, M1, M2, M3, M4 >& memberFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBMemberFunctor1wRetM4Body(Client* client, Member member, M1 m1, M2 m2, M3 m3, M4 m4 )
        DNB_THROW_SPEC_ANY;
        
    virtual ~_DNBMemberFunctor1wRetM4Body()
        DNB_THROW_SPEC_ANY;

    virtual
    R
    operator()( A1 a1 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor1wRetBody< R, A1 >* 
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBMemberFunctor1wRetM4Body()
        DNB_THROW_SPEC_ANY;

    Client* client_;
    Member member_;

   M1       m1_;
    M2       m2_;
    M3       m3_;
    M4       m4_;

};

template< class R, class A1  >
class  DNBFunctor1wRet  : public DNBBaseFunctor
{
public:
    DNBFunctor1wRet(_DNBBaseFunctor1wRetBody< R, A1 >* body = NULL)
        DNB_THROW_SPEC_ANY;

    DNBFunctor1wRet(const DNBFunctor1wRet< R, A1 >& DNBFunctor)
        DNB_THROW_SPEC_ANY;

    virtual
    ~DNBFunctor1wRet()
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctorBody*
    getBody() const
        DNB_THROW_SPEC_ANY;

    DNBFunctor1wRet< R, A1 >&
    operator=(const DNBFunctor1wRet< R, A1 >& DNBFunctor)
        DNB_THROW_SPEC(( scl_bad_alloc ));

    R
    operator()( A1 a1 )
        DNB_THROW_SPEC_ANY;

private:
    _DNBBaseFunctor1wRetBody< R, A1 >* body_;
};

template< class R, class A1, class Function >
DNBFunctor1wRet< R, A1 >
DNBMakeFunctorG(DNBFunctor1wRet< R, A1 >*, Function f)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBFunction1wRetBody< R, A1, Function>* body =
        DNB_NEW _DNBFunction1wRetBody< R, A1, Function>(f);
    return DNBFunctor1wRet< R, A1 >(body);
}

template< class R, class A1, class Function, class M1 >
DNBFunctor1wRet< R, A1 >
DNBMakeFunctorG(DNBFunctor1wRet< R, A1 >*, Function f, M1 m1 )
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBFunction1wRetM1Body< R, A1, Function, M1 >* body =
        DNB_NEW _DNBFunction1wRetM1Body< R, A1, Function, M1 >(f, m1);
    return DNBFunctor1wRet< R, A1 >(body);
}

template< class R, class A1, class Function, class M1, class M2 >
DNBFunctor1wRet< R, A1 >
DNBMakeFunctorG(DNBFunctor1wRet< R, A1 >*, Function f, M1 m1, M2 m2 )
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBFunction1wRetM2Body< R, A1, Function, M1, M2 >* body =
        DNB_NEW _DNBFunction1wRetM2Body< R, A1, Function, M1, M2 >(f, m1, m2);
    return DNBFunctor1wRet< R, A1 >(body);
}

template< class R, class A1, class Function, class M1, class M2, class M3 >
DNBFunctor1wRet< R, A1 >
DNBMakeFunctorG(DNBFunctor1wRet< R, A1 >*, Function f, M1 m1, M2 m2, M3 m3 )
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBFunction1wRetM3Body< R, A1, Function, M1, M2, M3 >* body =
        DNB_NEW _DNBFunction1wRetM3Body< R, A1, Function, M1, M2, M3 >(f, m1, m2, m3);
    return DNBFunctor1wRet< R, A1 >(body);
}

template< class R, class A1, class Function, class M1, class M2, class M3, class M4 >
DNBFunctor1wRet< R, A1 >
DNBMakeFunctorG(DNBFunctor1wRet< R, A1 >*, Function f, M1 m1, M2 m2, M3 m3, M4 m4 )
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBFunction1wRetM4Body< R, A1, Function, M1, M2, M3, M4 >* body =
        DNB_NEW _DNBFunction1wRetM4Body< R, A1, Function, M1, M2, M3, M4 >(f, m1, m2, m3, m4);
    return DNBFunctor1wRet< R, A1 >(body);
}

template< class R, class A1, class Client, class Member >
DNBFunctor1wRet< R, A1 >
DNBMakeFunctorM(DNBFunctor1wRet< R, A1 >*,Client* client, Member member)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBMemberFunctor1wRetBody< R, A1, Client, Member >* body =
        DNB_NEW _DNBMemberFunctor1wRetBody< R, A1, Client, Member >(client,member);
    return DNBFunctor1wRet< R, A1 >(body);
}

template< class R, class A1, class Client, class Member, class M1>
DNBFunctor1wRet< R, A1 >
DNBMakeFunctorM(DNBFunctor1wRet< R, A1 > *, Client* client, Member member, M1 m1)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBMemberFunctor1wRetM1Body< R, A1, Client, Member, M1 >* body =
        DNB_NEW _DNBMemberFunctor1wRetM1Body< R, A1, Client, Member, M1 >( client, member, m1 );
    return DNBFunctor1wRet< R, A1 > (body);
}

template< class R, class A1, class Client, class Member, class M1, class M2>
DNBFunctor1wRet< R, A1 >
DNBMakeFunctorM(DNBFunctor1wRet< R, A1 > *, Client* client, Member member, M1 m1, M2 m2)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBMemberFunctor1wRetM2Body< R, A1, Client, Member, M1, M2 >* body =
        DNB_NEW _DNBMemberFunctor1wRetM2Body< R, A1, Client, Member, M1, M2 >( client, member, m1, m2 );
    return DNBFunctor1wRet< R, A1 > (body);
}

template< class R, class A1, class Client, class Member, class M1, class M2, class M3>
DNBFunctor1wRet< R, A1 >
DNBMakeFunctorM(DNBFunctor1wRet< R, A1 > *, Client* client, Member member, M1 m1, M2 m2, M3 m3)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBMemberFunctor1wRetM3Body< R, A1, Client, Member, M1, M2, M3 >* body =
        DNB_NEW _DNBMemberFunctor1wRetM3Body< R, A1, Client, Member, M1, M2, M3 >( client, member, m1, m2, m3 );
    return DNBFunctor1wRet< R, A1 > (body);
}

template< class R, class A1, class Client, class Member, class M1, class M2, class M3, class M4>
DNBFunctor1wRet< R, A1 >
DNBMakeFunctorM(DNBFunctor1wRet< R, A1 > *, Client* client, Member member, M1 m1, M2 m2, M3 m3, M4 m4)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBMemberFunctor1wRetM4Body< R, A1, Client, Member, M1, M2, M3, M4 >* body =
        DNB_NEW _DNBMemberFunctor1wRetM4Body< R, A1, Client, Member, M1, M2, M3, M4 >( client, member, m1, m2, m3, m4 );
    return DNBFunctor1wRet< R, A1 > (body);
}

/////////////////////////////////////////////////////////
//
// 2 Passed,  1 Returned 
//
/////////////////////////////////////////////////////////

template< class R, class A1, class A2  >
class  _DNBBaseFunctor2wRetBody : public _DNBBaseFunctorBody
{
public:
    virtual 
    R 
    operator()( A1 a1, A2 a2 )
        DNB_THROW_SPEC_ANY    = 0;

    virtual
    _DNBBaseFunctor2wRetBody< R, A1, A2 >*
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ))    = 0;
};

template< class R, class A1, class A2, class Function >
class _DNBFunction2wRetBody : public _DNBBaseFunctor2wRetBody< R, A1, A2 >
{
public:
    _DNBFunction2wRetBody(const _DNBFunction2wRetBody< R, A1, A2, Function>& functionFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBFunction2wRetBody(const Function& f)
        DNB_THROW_SPEC_ANY;

    virtual
    ~_DNBFunction2wRetBody()
        DNB_THROW_SPEC_ANY;

    virtual
    R
    operator()( A1 a1, A2 a2 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const

        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor2wRetBody< R, A1, A2 >* 
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBFunction2wRetBody()
        DNB_THROW_SPEC_ANY;

    Function f_;
};

template< class R, class A1, class A2, class Function, class M1 >
class _DNBFunction2wRetM1Body : public _DNBBaseFunctor2wRetBody< R, A1, A2 >
{
public:
    _DNBFunction2wRetM1Body(const _DNBFunction2wRetM1Body< R, A1, A2, Function, M1 >& functionFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBFunction2wRetM1Body(const Function& f, M1 m1 )
        DNB_THROW_SPEC_ANY;

    virtual
    ~_DNBFunction2wRetM1Body()
        DNB_THROW_SPEC_ANY;

    virtual
    R
    operator()( A1 a1, A2 a2 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor2wRetBody< R, A1, A2 >*
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBFunction2wRetM1Body()
        DNB_THROW_SPEC_ANY;

    Function f_;
    M1       m1_;

};

template< class R, class A1, class A2, class Function, class M1, class M2 >
class _DNBFunction2wRetM2Body : public _DNBBaseFunctor2wRetBody< R, A1, A2 >
{
public:
    _DNBFunction2wRetM2Body(const _DNBFunction2wRetM2Body< R, A1, A2, Function, M1, M2 >& functionFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBFunction2wRetM2Body(const Function& f, M1 m1, M2 m2 )
        DNB_THROW_SPEC_ANY;

    virtual
    ~_DNBFunction2wRetM2Body()
        DNB_THROW_SPEC_ANY;

    virtual
    R
    operator()( A1 a1, A2 a2 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor2wRetBody< R, A1, A2 >*
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBFunction2wRetM2Body()
        DNB_THROW_SPEC_ANY;

    Function f_;
    M1       m1_;
    M2       m2_;

};

template< class R, class A1, class A2, class Function, class M1, class M2, class M3 >
class _DNBFunction2wRetM3Body : public _DNBBaseFunctor2wRetBody< R, A1, A2 >
{
public:
    _DNBFunction2wRetM3Body(const _DNBFunction2wRetM3Body< R, A1, A2, Function, M1, M2, M3 >& functionFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBFunction2wRetM3Body(const Function& f, M1 m1, M2 m2, M3 m3 )
        DNB_THROW_SPEC_ANY;

    virtual
    ~_DNBFunction2wRetM3Body()
        DNB_THROW_SPEC_ANY;

    virtual
    R
    operator()( A1 a1, A2 a2 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor2wRetBody< R, A1, A2 >*
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBFunction2wRetM3Body()
        DNB_THROW_SPEC_ANY;

    Function f_;
    M1       m1_;
    M2       m2_;
    M3       m3_;

};

template< class R, class A1, class A2, class Client, class Member >
class _DNBMemberFunctor2wRetBody:
public _DNBBaseFunctor2wRetBody< R, A1, A2 >
{
public:
    _DNBMemberFunctor2wRetBody( const _DNBMemberFunctor2wRetBody< R, A1, A2, Client, Member >& memberFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBMemberFunctor2wRetBody(Client* client, Member member)
        DNB_THROW_SPEC_ANY;

    virtual ~_DNBMemberFunctor2wRetBody()
        DNB_THROW_SPEC_ANY;

    virtual
    R
    operator()( A1 a1, A2 a2 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor2wRetBody< R, A1, A2 >*
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBMemberFunctor2wRetBody()
        DNB_THROW_SPEC_ANY;

    Client* client_;
    Member member_;
};

template< class R, class A1, class A2, class Client, class Member, class M1 >
class _DNBMemberFunctor2wRetM1Body:
public _DNBBaseFunctor2wRetBody< R, A1, A2 >
{
public:
    _DNBMemberFunctor2wRetM1Body( const _DNBMemberFunctor2wRetM1Body< R, A1, A2, Client, Member, M1 >& memberFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBMemberFunctor2wRetM1Body(Client* client, Member member, M1 m1 )
        DNB_THROW_SPEC_ANY;
        
    virtual ~_DNBMemberFunctor2wRetM1Body()
        DNB_THROW_SPEC_ANY;

    virtual
    R
    operator()( A1 a1, A2 a2 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor2wRetBody< R, A1, A2 >* 
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBMemberFunctor2wRetM1Body()
        DNB_THROW_SPEC_ANY;

    Client* client_;
    Member member_;

   M1       m1_;

};

template< class R, class A1, class A2, class Client, class Member, class M1, class M2 >
class _DNBMemberFunctor2wRetM2Body:
public _DNBBaseFunctor2wRetBody< R, A1, A2 >
{
public:
    _DNBMemberFunctor2wRetM2Body( const _DNBMemberFunctor2wRetM2Body< R, A1, A2, Client, Member, M1, M2 >& memberFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBMemberFunctor2wRetM2Body(Client* client, Member member, M1 m1, M2 m2 )
        DNB_THROW_SPEC_ANY;
        
    virtual ~_DNBMemberFunctor2wRetM2Body()
        DNB_THROW_SPEC_ANY;

    virtual
    R
    operator()( A1 a1, A2 a2 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor2wRetBody< R, A1, A2 >* 
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBMemberFunctor2wRetM2Body()
        DNB_THROW_SPEC_ANY;

    Client* client_;
    Member member_;

   M1       m1_;
    M2       m2_;

};

template< class R, class A1, class A2, class Client, class Member, class M1, class M2, class M3 >
class _DNBMemberFunctor2wRetM3Body:
public _DNBBaseFunctor2wRetBody< R, A1, A2 >
{
public:
    _DNBMemberFunctor2wRetM3Body( const _DNBMemberFunctor2wRetM3Body< R, A1, A2, Client, Member, M1, M2, M3 >& memberFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBMemberFunctor2wRetM3Body(Client* client, Member member, M1 m1, M2 m2, M3 m3 )
        DNB_THROW_SPEC_ANY;
        
    virtual ~_DNBMemberFunctor2wRetM3Body()
        DNB_THROW_SPEC_ANY;

    virtual
    R
    operator()( A1 a1, A2 a2 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor2wRetBody< R, A1, A2 >* 
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBMemberFunctor2wRetM3Body()
        DNB_THROW_SPEC_ANY;

    Client* client_;
    Member member_;

   M1       m1_;
    M2       m2_;
    M3       m3_;

};

template< class R, class A1, class A2  >
class  DNBFunctor2wRet  : public DNBBaseFunctor
{
public:
    DNBFunctor2wRet(_DNBBaseFunctor2wRetBody< R, A1, A2 >* body = NULL)
        DNB_THROW_SPEC_ANY;

    DNBFunctor2wRet(const DNBFunctor2wRet< R, A1, A2 >& DNBFunctor)
        DNB_THROW_SPEC_ANY;

    virtual
    ~DNBFunctor2wRet()
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctorBody*
    getBody() const
        DNB_THROW_SPEC_ANY;

    DNBFunctor2wRet< R, A1, A2 >&
    operator=(const DNBFunctor2wRet< R, A1, A2 >& DNBFunctor)
        DNB_THROW_SPEC(( scl_bad_alloc ));

    R
    operator()( A1 a1, A2 a2 )
        DNB_THROW_SPEC_ANY;

private:
    _DNBBaseFunctor2wRetBody< R, A1, A2 >* body_;
};

template< class R, class A1, class A2, class Function >
DNBFunctor2wRet< R, A1, A2 >
DNBMakeFunctorG(DNBFunctor2wRet< R, A1, A2 >*, Function f)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBFunction2wRetBody< R, A1, A2, Function>* body =
        DNB_NEW _DNBFunction2wRetBody< R, A1, A2, Function>(f);
    return DNBFunctor2wRet< R, A1, A2 >(body);
}

template< class R, class A1, class A2, class Function, class M1 >
DNBFunctor2wRet< R, A1, A2 >
DNBMakeFunctorG(DNBFunctor2wRet< R, A1, A2 >*, Function f, M1 m1 )
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBFunction2wRetM1Body< R, A1, A2, Function, M1 >* body =
        DNB_NEW _DNBFunction2wRetM1Body< R, A1, A2, Function, M1 >(f, m1);
    return DNBFunctor2wRet< R, A1, A2 >(body);
}

template< class R, class A1, class A2, class Function, class M1, class M2 >
DNBFunctor2wRet< R, A1, A2 >
DNBMakeFunctorG(DNBFunctor2wRet< R, A1, A2 >*, Function f, M1 m1, M2 m2 )
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBFunction2wRetM2Body< R, A1, A2, Function, M1, M2 >* body =
        DNB_NEW _DNBFunction2wRetM2Body< R, A1, A2, Function, M1, M2 >(f, m1, m2);
    return DNBFunctor2wRet< R, A1, A2 >(body);
}

template< class R, class A1, class A2, class Function, class M1, class M2, class M3 >
DNBFunctor2wRet< R, A1, A2 >
DNBMakeFunctorG(DNBFunctor2wRet< R, A1, A2 >*, Function f, M1 m1, M2 m2, M3 m3 )
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBFunction2wRetM3Body< R, A1, A2, Function, M1, M2, M3 >* body =
        DNB_NEW _DNBFunction2wRetM3Body< R, A1, A2, Function, M1, M2, M3 >(f, m1, m2, m3);
    return DNBFunctor2wRet< R, A1, A2 >(body);
}

template< class R, class A1, class A2, class Client, class Member >
DNBFunctor2wRet< R, A1, A2 >
DNBMakeFunctorM(DNBFunctor2wRet< R, A1, A2 >*,Client* client, Member member)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBMemberFunctor2wRetBody< R, A1, A2, Client, Member >* body =
        DNB_NEW _DNBMemberFunctor2wRetBody< R, A1, A2, Client, Member >(client,member);
    return DNBFunctor2wRet< R, A1, A2 >(body);
}

template< class R, class A1, class A2, class Client, class Member, class M1>
DNBFunctor2wRet< R, A1, A2 >
DNBMakeFunctorM(DNBFunctor2wRet< R, A1, A2 > *, Client* client, Member member, M1 m1)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBMemberFunctor2wRetM1Body< R, A1, A2, Client, Member, M1 >* body =
        DNB_NEW _DNBMemberFunctor2wRetM1Body< R, A1, A2, Client, Member, M1 >( client, member, m1 );
    return DNBFunctor2wRet< R, A1, A2 > (body);
}

template< class R, class A1, class A2, class Client, class Member, class M1, class M2>
DNBFunctor2wRet< R, A1, A2 >
DNBMakeFunctorM(DNBFunctor2wRet< R, A1, A2 > *, Client* client, Member member, M1 m1, M2 m2)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBMemberFunctor2wRetM2Body< R, A1, A2, Client, Member, M1, M2 >* body =
        DNB_NEW _DNBMemberFunctor2wRetM2Body< R, A1, A2, Client, Member, M1, M2 >( client, member, m1, m2 );
    return DNBFunctor2wRet< R, A1, A2 > (body);
}

template< class R, class A1, class A2, class Client, class Member, class M1, class M2, class M3>
DNBFunctor2wRet< R, A1, A2 >
DNBMakeFunctorM(DNBFunctor2wRet< R, A1, A2 > *, Client* client, Member member, M1 m1, M2 m2, M3 m3)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBMemberFunctor2wRetM3Body< R, A1, A2, Client, Member, M1, M2, M3 >* body =
        DNB_NEW _DNBMemberFunctor2wRetM3Body< R, A1, A2, Client, Member, M1, M2, M3 >( client, member, m1, m2, m3 );
    return DNBFunctor2wRet< R, A1, A2 > (body);
}

/////////////////////////////////////////////////////////
//
// 3 Passed,  1 Returned 
//
/////////////////////////////////////////////////////////

template< class R, class A1, class A2, class A3  >
class  _DNBBaseFunctor3wRetBody : public _DNBBaseFunctorBody
{
public:
    virtual 
    R 
    operator()( A1 a1, A2 a2, A3 a3 )
        DNB_THROW_SPEC_ANY    = 0;

    virtual
    _DNBBaseFunctor3wRetBody< R, A1, A2, A3 >*
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ))    = 0;
};

template< class R, class A1, class A2, class A3, class Function >
class _DNBFunction3wRetBody : public _DNBBaseFunctor3wRetBody< R, A1, A2, A3 >
{
public:
    _DNBFunction3wRetBody(const _DNBFunction3wRetBody< R, A1, A2, A3, Function>& functionFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBFunction3wRetBody(const Function& f)
        DNB_THROW_SPEC_ANY;

    virtual
    ~_DNBFunction3wRetBody()
        DNB_THROW_SPEC_ANY;

    virtual
    R
    operator()( A1 a1, A2 a2, A3 a3 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const

        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor3wRetBody< R, A1, A2, A3 >* 
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBFunction3wRetBody()
        DNB_THROW_SPEC_ANY;

    Function f_;
};

template< class R, class A1, class A2, class A3, class Function, class M1 >
class _DNBFunction3wRetM1Body : public _DNBBaseFunctor3wRetBody< R, A1, A2, A3 >
{
public:
    _DNBFunction3wRetM1Body(const _DNBFunction3wRetM1Body< R, A1, A2, A3, Function, M1 >& functionFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBFunction3wRetM1Body(const Function& f, M1 m1 )
        DNB_THROW_SPEC_ANY;

    virtual
    ~_DNBFunction3wRetM1Body()
        DNB_THROW_SPEC_ANY;

    virtual
    R
    operator()( A1 a1, A2 a2, A3 a3 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor3wRetBody< R, A1, A2, A3 >*
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBFunction3wRetM1Body()
        DNB_THROW_SPEC_ANY;

    Function f_;
    M1       m1_;

};

template< class R, class A1, class A2, class A3, class Function, class M1, class M2 >
class _DNBFunction3wRetM2Body : public _DNBBaseFunctor3wRetBody< R, A1, A2, A3 >
{
public:
    _DNBFunction3wRetM2Body(const _DNBFunction3wRetM2Body< R, A1, A2, A3, Function, M1, M2 >& functionFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBFunction3wRetM2Body(const Function& f, M1 m1, M2 m2 )
        DNB_THROW_SPEC_ANY;

    virtual
    ~_DNBFunction3wRetM2Body()
        DNB_THROW_SPEC_ANY;

    virtual
    R
    operator()( A1 a1, A2 a2, A3 a3 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor3wRetBody< R, A1, A2, A3 >*
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBFunction3wRetM2Body()
        DNB_THROW_SPEC_ANY;

    Function f_;
    M1       m1_;
    M2       m2_;

};

template< class R, class A1, class A2, class A3, class Client, class Member >
class _DNBMemberFunctor3wRetBody:
public _DNBBaseFunctor3wRetBody< R, A1, A2, A3 >
{
public:
    _DNBMemberFunctor3wRetBody( const _DNBMemberFunctor3wRetBody< R, A1, A2, A3, Client, Member >& memberFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBMemberFunctor3wRetBody(Client* client, Member member)
        DNB_THROW_SPEC_ANY;

    virtual ~_DNBMemberFunctor3wRetBody()
        DNB_THROW_SPEC_ANY;

    virtual
    R
    operator()( A1 a1, A2 a2, A3 a3 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor3wRetBody< R, A1, A2, A3 >*
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBMemberFunctor3wRetBody()
        DNB_THROW_SPEC_ANY;

    Client* client_;
    Member member_;
};

template< class R, class A1, class A2, class A3, class Client, class Member, class M1 >
class _DNBMemberFunctor3wRetM1Body:
public _DNBBaseFunctor3wRetBody< R, A1, A2, A3 >
{
public:
    _DNBMemberFunctor3wRetM1Body( const _DNBMemberFunctor3wRetM1Body< R, A1, A2, A3, Client, Member, M1 >& memberFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBMemberFunctor3wRetM1Body(Client* client, Member member, M1 m1 )
        DNB_THROW_SPEC_ANY;
        
    virtual ~_DNBMemberFunctor3wRetM1Body()
        DNB_THROW_SPEC_ANY;

    virtual
    R
    operator()( A1 a1, A2 a2, A3 a3 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor3wRetBody< R, A1, A2, A3 >* 
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBMemberFunctor3wRetM1Body()
        DNB_THROW_SPEC_ANY;

    Client* client_;
    Member member_;

   M1       m1_;

};

template< class R, class A1, class A2, class A3, class Client, class Member, class M1, class M2 >
class _DNBMemberFunctor3wRetM2Body:
public _DNBBaseFunctor3wRetBody< R, A1, A2, A3 >
{
public:
    _DNBMemberFunctor3wRetM2Body( const _DNBMemberFunctor3wRetM2Body< R, A1, A2, A3, Client, Member, M1, M2 >& memberFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBMemberFunctor3wRetM2Body(Client* client, Member member, M1 m1, M2 m2 )
        DNB_THROW_SPEC_ANY;
        
    virtual ~_DNBMemberFunctor3wRetM2Body()
        DNB_THROW_SPEC_ANY;

    virtual
    R
    operator()( A1 a1, A2 a2, A3 a3 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor3wRetBody< R, A1, A2, A3 >* 
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBMemberFunctor3wRetM2Body()
        DNB_THROW_SPEC_ANY;

    Client* client_;
    Member member_;

   M1       m1_;
    M2       m2_;

};

template< class R, class A1, class A2, class A3  >
class  DNBFunctor3wRet  : public DNBBaseFunctor
{
public:
    DNBFunctor3wRet(_DNBBaseFunctor3wRetBody< R, A1, A2, A3 >* body = NULL)
        DNB_THROW_SPEC_ANY;

    DNBFunctor3wRet(const DNBFunctor3wRet< R, A1, A2, A3 >& DNBFunctor)
        DNB_THROW_SPEC_ANY;

    virtual
    ~DNBFunctor3wRet()
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctorBody*
    getBody() const
        DNB_THROW_SPEC_ANY;

    DNBFunctor3wRet< R, A1, A2, A3 >&
    operator=(const DNBFunctor3wRet< R, A1, A2, A3 >& DNBFunctor)
        DNB_THROW_SPEC(( scl_bad_alloc ));

    R
    operator()( A1 a1, A2 a2, A3 a3 )
        DNB_THROW_SPEC_ANY;

private:
    _DNBBaseFunctor3wRetBody< R, A1, A2, A3 >* body_;
};

template< class R, class A1, class A2, class A3, class Function >
DNBFunctor3wRet< R, A1, A2, A3 >
DNBMakeFunctorG(DNBFunctor3wRet< R, A1, A2, A3 >*, Function f)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBFunction3wRetBody< R, A1, A2, A3, Function>* body =
        DNB_NEW _DNBFunction3wRetBody< R, A1, A2, A3, Function>(f);
    return DNBFunctor3wRet< R, A1, A2, A3 >(body);
}

template< class R, class A1, class A2, class A3, class Function, class M1 >
DNBFunctor3wRet< R, A1, A2, A3 >
DNBMakeFunctorG(DNBFunctor3wRet< R, A1, A2, A3 >*, Function f, M1 m1 )
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBFunction3wRetM1Body< R, A1, A2, A3, Function, M1 >* body =
        DNB_NEW _DNBFunction3wRetM1Body< R, A1, A2, A3, Function, M1 >(f, m1);
    return DNBFunctor3wRet< R, A1, A2, A3 >(body);
}

template< class R, class A1, class A2, class A3, class Function, class M1, class M2 >
DNBFunctor3wRet< R, A1, A2, A3 >
DNBMakeFunctorG(DNBFunctor3wRet< R, A1, A2, A3 >*, Function f, M1 m1, M2 m2 )
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBFunction3wRetM2Body< R, A1, A2, A3, Function, M1, M2 >* body =
        DNB_NEW _DNBFunction3wRetM2Body< R, A1, A2, A3, Function, M1, M2 >(f, m1, m2);
    return DNBFunctor3wRet< R, A1, A2, A3 >(body);
}

template< class R, class A1, class A2, class A3, class Client, class Member >
DNBFunctor3wRet< R, A1, A2, A3 >
DNBMakeFunctorM(DNBFunctor3wRet< R, A1, A2, A3 >*,Client* client, Member member)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBMemberFunctor3wRetBody< R, A1, A2, A3, Client, Member >* body =
        DNB_NEW _DNBMemberFunctor3wRetBody< R, A1, A2, A3, Client, Member >(client,member);
    return DNBFunctor3wRet< R, A1, A2, A3 >(body);
}

template< class R, class A1, class A2, class A3, class Client, class Member, class M1>
DNBFunctor3wRet< R, A1, A2, A3 >
DNBMakeFunctorM(DNBFunctor3wRet< R, A1, A2, A3 > *, Client* client, Member member, M1 m1)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBMemberFunctor3wRetM1Body< R, A1, A2, A3, Client, Member, M1 >* body =
        DNB_NEW _DNBMemberFunctor3wRetM1Body< R, A1, A2, A3, Client, Member, M1 >( client, member, m1 );
    return DNBFunctor3wRet< R, A1, A2, A3 > (body);
}

template< class R, class A1, class A2, class A3, class Client, class Member, class M1, class M2>
DNBFunctor3wRet< R, A1, A2, A3 >
DNBMakeFunctorM(DNBFunctor3wRet< R, A1, A2, A3 > *, Client* client, Member member, M1 m1, M2 m2)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBMemberFunctor3wRetM2Body< R, A1, A2, A3, Client, Member, M1, M2 >* body =
        DNB_NEW _DNBMemberFunctor3wRetM2Body< R, A1, A2, A3, Client, Member, M1, M2 >( client, member, m1, m2 );
    return DNBFunctor3wRet< R, A1, A2, A3 > (body);
}

/////////////////////////////////////////////////////////
//
// 4 Passed,  1 Returned 
//
/////////////////////////////////////////////////////////

template< class R, class A1, class A2, class A3, class A4  >
class  _DNBBaseFunctor4wRetBody : public _DNBBaseFunctorBody
{
public:
    virtual 
    R 
    operator()( A1 a1, A2 a2, A3 a3, A4 a4 )
        DNB_THROW_SPEC_ANY    = 0;

    virtual
    _DNBBaseFunctor4wRetBody< R, A1, A2, A3, A4 >*
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ))    = 0;
};

template< class R, class A1, class A2, class A3, class A4, class Function >
class _DNBFunction4wRetBody : public _DNBBaseFunctor4wRetBody< R, A1, A2, A3, A4 >
{
public:
    _DNBFunction4wRetBody(const _DNBFunction4wRetBody< R, A1, A2, A3, A4, Function>& functionFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBFunction4wRetBody(const Function& f)
        DNB_THROW_SPEC_ANY;

    virtual
    ~_DNBFunction4wRetBody()
        DNB_THROW_SPEC_ANY;

    virtual
    R
    operator()( A1 a1, A2 a2, A3 a3, A4 a4 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const

        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor4wRetBody< R, A1, A2, A3, A4 >* 
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBFunction4wRetBody()
        DNB_THROW_SPEC_ANY;

    Function f_;
};

template< class R, class A1, class A2, class A3, class A4, class Function, class M1 >
class _DNBFunction4wRetM1Body : public _DNBBaseFunctor4wRetBody< R, A1, A2, A3, A4 >
{
public:
    _DNBFunction4wRetM1Body(const _DNBFunction4wRetM1Body< R, A1, A2, A3, A4, Function, M1 >& functionFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBFunction4wRetM1Body(const Function& f, M1 m1 )
        DNB_THROW_SPEC_ANY;

    virtual
    ~_DNBFunction4wRetM1Body()
        DNB_THROW_SPEC_ANY;

    virtual
    R
    operator()( A1 a1, A2 a2, A3 a3, A4 a4 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor4wRetBody< R, A1, A2, A3, A4 >*
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBFunction4wRetM1Body()
        DNB_THROW_SPEC_ANY;

    Function f_;
    M1       m1_;

};

template< class R, class A1, class A2, class A3, class A4, class Client, class Member >
class _DNBMemberFunctor4wRetBody:
public _DNBBaseFunctor4wRetBody< R, A1, A2, A3, A4 >
{
public:
    _DNBMemberFunctor4wRetBody( const _DNBMemberFunctor4wRetBody< R, A1, A2, A3, A4, Client, Member >& memberFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBMemberFunctor4wRetBody(Client* client, Member member)
        DNB_THROW_SPEC_ANY;

    virtual ~_DNBMemberFunctor4wRetBody()
        DNB_THROW_SPEC_ANY;

    virtual
    R
    operator()( A1 a1, A2 a2, A3 a3, A4 a4 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor4wRetBody< R, A1, A2, A3, A4 >*
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBMemberFunctor4wRetBody()
        DNB_THROW_SPEC_ANY;

    Client* client_;
    Member member_;
};

template< class R, class A1, class A2, class A3, class A4, class Client, class Member, class M1 >
class _DNBMemberFunctor4wRetM1Body:
public _DNBBaseFunctor4wRetBody< R, A1, A2, A3, A4 >
{
public:
    _DNBMemberFunctor4wRetM1Body( const _DNBMemberFunctor4wRetM1Body< R, A1, A2, A3, A4, Client, Member, M1 >& memberFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBMemberFunctor4wRetM1Body(Client* client, Member member, M1 m1 )
        DNB_THROW_SPEC_ANY;
        
    virtual ~_DNBMemberFunctor4wRetM1Body()
        DNB_THROW_SPEC_ANY;

    virtual
    R
    operator()( A1 a1, A2 a2, A3 a3, A4 a4 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor4wRetBody< R, A1, A2, A3, A4 >* 
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBMemberFunctor4wRetM1Body()
        DNB_THROW_SPEC_ANY;

    Client* client_;
    Member member_;

   M1       m1_;

};

template< class R, class A1, class A2, class A3, class A4  >
class  DNBFunctor4wRet  : public DNBBaseFunctor
{
public:
    DNBFunctor4wRet(_DNBBaseFunctor4wRetBody< R, A1, A2, A3, A4 >* body = NULL)
        DNB_THROW_SPEC_ANY;

    DNBFunctor4wRet(const DNBFunctor4wRet< R, A1, A2, A3, A4 >& DNBFunctor)
        DNB_THROW_SPEC_ANY;

    virtual
    ~DNBFunctor4wRet()
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctorBody*
    getBody() const
        DNB_THROW_SPEC_ANY;

    DNBFunctor4wRet< R, A1, A2, A3, A4 >&
    operator=(const DNBFunctor4wRet< R, A1, A2, A3, A4 >& DNBFunctor)
        DNB_THROW_SPEC(( scl_bad_alloc ));

    R
    operator()( A1 a1, A2 a2, A3 a3, A4 a4 )
        DNB_THROW_SPEC_ANY;

private:
    _DNBBaseFunctor4wRetBody< R, A1, A2, A3, A4 >* body_;
};

template< class R, class A1, class A2, class A3, class A4, class Function >
DNBFunctor4wRet< R, A1, A2, A3, A4 >
DNBMakeFunctorG(DNBFunctor4wRet< R, A1, A2, A3, A4 >*, Function f)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBFunction4wRetBody< R, A1, A2, A3, A4, Function>* body =
        DNB_NEW _DNBFunction4wRetBody< R, A1, A2, A3, A4, Function>(f);
    return DNBFunctor4wRet< R, A1, A2, A3, A4 >(body);
}

template< class R, class A1, class A2, class A3, class A4, class Function, class M1 >
DNBFunctor4wRet< R, A1, A2, A3, A4 >
DNBMakeFunctorG(DNBFunctor4wRet< R, A1, A2, A3, A4 >*, Function f, M1 m1 )
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBFunction4wRetM1Body< R, A1, A2, A3, A4, Function, M1 >* body =
        DNB_NEW _DNBFunction4wRetM1Body< R, A1, A2, A3, A4, Function, M1 >(f, m1);
    return DNBFunctor4wRet< R, A1, A2, A3, A4 >(body);
}

template< class R, class A1, class A2, class A3, class A4, class Client, class Member >
DNBFunctor4wRet< R, A1, A2, A3, A4 >
DNBMakeFunctorM(DNBFunctor4wRet< R, A1, A2, A3, A4 >*,Client* client, Member member)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBMemberFunctor4wRetBody< R, A1, A2, A3, A4, Client, Member >* body =
        DNB_NEW _DNBMemberFunctor4wRetBody< R, A1, A2, A3, A4, Client, Member >(client,member);
    return DNBFunctor4wRet< R, A1, A2, A3, A4 >(body);
}

template< class R, class A1, class A2, class A3, class A4, class Client, class Member, class M1>
DNBFunctor4wRet< R, A1, A2, A3, A4 >
DNBMakeFunctorM(DNBFunctor4wRet< R, A1, A2, A3, A4 > *, Client* client, Member member, M1 m1)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBMemberFunctor4wRetM1Body< R, A1, A2, A3, A4, Client, Member, M1 >* body =
        DNB_NEW _DNBMemberFunctor4wRetM1Body< R, A1, A2, A3, A4, Client, Member, M1 >( client, member, m1 );
    return DNBFunctor4wRet< R, A1, A2, A3, A4 > (body);
}

/////////////////////////////////////////////////////////
//
// 5 Passed,  1 Returned 
//
/////////////////////////////////////////////////////////

template< class R, class A1, class A2, class A3, class A4, class A5  >
class  _DNBBaseFunctor5wRetBody : public _DNBBaseFunctorBody
{
public:
    virtual 
    R 
    operator()( A1 a1, A2 a2, A3 a3, A4 a4, A5 a5 )
        DNB_THROW_SPEC_ANY    = 0;

    virtual
    _DNBBaseFunctor5wRetBody< R, A1, A2, A3, A4, A5 >*
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ))    = 0;
};

template< class R, class A1, class A2, class A3, class A4, class A5, class Function >
class _DNBFunction5wRetBody : public _DNBBaseFunctor5wRetBody< R, A1, A2, A3, A4, A5 >
{
public:
    _DNBFunction5wRetBody(const _DNBFunction5wRetBody< R, A1, A2, A3, A4, A5, Function>& functionFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBFunction5wRetBody(const Function& f)
        DNB_THROW_SPEC_ANY;

    virtual
    ~_DNBFunction5wRetBody()
        DNB_THROW_SPEC_ANY;

    virtual
    R
    operator()( A1 a1, A2 a2, A3 a3, A4 a4, A5 a5 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const

        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor5wRetBody< R, A1, A2, A3, A4, A5 >* 
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBFunction5wRetBody()
        DNB_THROW_SPEC_ANY;

    Function f_;
};

template< class R, class A1, class A2, class A3, class A4, class A5, class Client, class Member >
class _DNBMemberFunctor5wRetBody:
public _DNBBaseFunctor5wRetBody< R, A1, A2, A3, A4, A5 >
{
public:
    _DNBMemberFunctor5wRetBody( const _DNBMemberFunctor5wRetBody< R, A1, A2, A3, A4, A5, Client, Member >& memberFunctor )
        DNB_THROW_SPEC_ANY;

    _DNBMemberFunctor5wRetBody(Client* client, Member member)
        DNB_THROW_SPEC_ANY;

    virtual ~_DNBMemberFunctor5wRetBody()
        DNB_THROW_SPEC_ANY;

    virtual
    R
    operator()( A1 a1, A2 a2, A3 a3, A4 a4, A5 a5 )
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getCallee() const
        DNB_THROW_SPEC_ANY;

    virtual
    void*
    getFunc() const
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctor5wRetBody< R, A1, A2, A3, A4, A5 >*
    clone()
        DNB_THROW_SPEC(( scl_bad_alloc ));

    virtual
    bool
    IsEqual( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

    virtual
    bool
    IsLessThan( _DNBBaseFunctorBody& rhs ) const
        DNB_THROW_SPEC_ANY;

private:
    _DNBMemberFunctor5wRetBody()
        DNB_THROW_SPEC_ANY;

    Client* client_;
    Member member_;
};

template< class R, class A1, class A2, class A3, class A4, class A5  >
class  DNBFunctor5wRet  : public DNBBaseFunctor
{
public:
    DNBFunctor5wRet(_DNBBaseFunctor5wRetBody< R, A1, A2, A3, A4, A5 >* body = NULL)
        DNB_THROW_SPEC_ANY;

    DNBFunctor5wRet(const DNBFunctor5wRet< R, A1, A2, A3, A4, A5 >& DNBFunctor)
        DNB_THROW_SPEC_ANY;

    virtual
    ~DNBFunctor5wRet()
        DNB_THROW_SPEC_ANY;

    virtual
    _DNBBaseFunctorBody*
    getBody() const
        DNB_THROW_SPEC_ANY;

    DNBFunctor5wRet< R, A1, A2, A3, A4, A5 >&
    operator=(const DNBFunctor5wRet< R, A1, A2, A3, A4, A5 >& DNBFunctor)
        DNB_THROW_SPEC(( scl_bad_alloc ));

    R
    operator()( A1 a1, A2 a2, A3 a3, A4 a4, A5 a5 )
        DNB_THROW_SPEC_ANY;

private:
    _DNBBaseFunctor5wRetBody< R, A1, A2, A3, A4, A5 >* body_;
};

template< class R, class A1, class A2, class A3, class A4, class A5, class Function >
DNBFunctor5wRet< R, A1, A2, A3, A4, A5 >
DNBMakeFunctorG(DNBFunctor5wRet< R, A1, A2, A3, A4, A5 >*, Function f)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBFunction5wRetBody< R, A1, A2, A3, A4, A5, Function>* body =
        DNB_NEW _DNBFunction5wRetBody< R, A1, A2, A3, A4, A5, Function>(f);
    return DNBFunctor5wRet< R, A1, A2, A3, A4, A5 >(body);
}

template< class R, class A1, class A2, class A3, class A4, class A5, class Client, class Member >
DNBFunctor5wRet< R, A1, A2, A3, A4, A5 >
DNBMakeFunctorM(DNBFunctor5wRet< R, A1, A2, A3, A4, A5 >*,Client* client, Member member)
    DNB_THROW_SPEC(( scl_bad_alloc ))
{
    _DNBMemberFunctor5wRetBody< R, A1, A2, A3, A4, A5, Client, Member >* body =
        DNB_NEW _DNBMemberFunctor5wRetBody< R, A1, A2, A3, A4, A5, Client, Member >(client,member);
    return DNBFunctor5wRet< R, A1, A2, A3, A4, A5 >(body);
}

