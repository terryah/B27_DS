//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview RTL trangara 01:11:15
 */
// --------------------------------------------------------

inline bool
DNBClassInfo::operator==( const DNBClassInfo &right ) const
    DNB_THROW_SPEC_NULL
{
#if     defined(__SUNPRO_CC)
    return ( className_ == right.className_ );
#else
    return ( *classID_ == *right.classID_ );
#endif
}


inline bool
DNBClassInfo::operator!=( const DNBClassInfo &right ) const
    DNB_THROW_SPEC_NULL
{
#if     defined(__SUNPRO_CC)
    return ( className_ != right.className_ );
#else
    return ( *classID_ != *right.classID_ );
#endif
}


inline bool
DNBClassInfo::operator< ( const DNBClassInfo &right ) const
    DNB_THROW_SPEC_NULL
{
#if     defined(__SUNPRO_CC)
    return ( className_ < right.className_ );
#else
    return ( (*classID_).before( *right.classID_ ) );
#endif
}


inline bool
DNBClassInfo::isIdentifiable( ) const
    DNB_THROW_SPEC_NULL
{
    return true;
}


inline scl_string
DNBClassInfo::getClassName( ) const
    DNB_THROW_SPEC_NULL
{
    return className_;
}


inline size_t
DNBClassInfo::getObjectSize( ) const
    DNB_THROW_SPEC_NULL
{
    return objectSize_;
}


inline size_t
DNBClassInfo::getBaseCount( ) const
    DNB_THROW_SPEC_NULL
{
    return baseInfoDB_.size( );
}


inline const DNBClassInfo &
DNBClassInfo::getBaseInfo( size_t idx ) const
    DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION( idx < baseInfoDB_.size() );
    return *baseInfoDB_[ idx ];
}


inline bool
DNBClassInfo::isConstructable( ) const
    DNB_THROW_SPEC_NULL
{
    return ( createMethod_ != NULL );
}
