// --------------------------------------------------------
// COPYRIGHT Dassault Systemes 2006
//===================================================================

#ifndef CATIImmNavAddin_h 
#define CATIImmNavAddin_h 

/***
* @CAA2Level L1
* @CAA2Usage U5
*/

#ifdef  _WINDOWS_SOURCE
#ifdef  __CATENOVIAProviderItf
#define ExportedByCATENOVIAProviderItf     __declspec(dllexport)
#else
#define ExportedByCATENOVIAProviderItf     __declspec(dllimport)
#endif
#else
#define ExportedByCATENOVIAProviderItf
#endif

#include "CATIWorkbenchAddin.h"

extern ExportedByCATENOVIAProviderItf IID IID_CATIImmNavAddin;

class ExportedByCATENOVIAProviderItf CATIImmNavAddin : public CATIWorkbenchAddin 
{ 
  CATDeclareInterface; 
  
public: 

}; 

CATDeclareHandler(CATIImmNavAddin, CATIWorkbenchAddin); 

#endif 
