/***
 COPYRIGHT Dassault Systemes 2005
*/


#ifndef CATImmIdentifierAcquisitionAgent_H
#define CATImmIdentifierAcquisitionAgent_H

/***
* @CAA2Level L1
* @CAA2Usage U1
*/

#ifdef  _WINDOWS_SOURCE
#ifdef  __CATImmIdAcqAgent
#define ExportedByCATImmIdAcqAgent     __declspec(dllexport)
#else
#define ExportedByCATImmIdAcqAgent     __declspec(dllimport)
#endif
#else
#define ExportedByCATImmIdAcqAgent
#endif

#include "CATPathElementAgent.h"

class CATPathElement;
class CATImmIdentifierAcquisitionAgentPrivate;
class CATIPLMIdentificator;

/**
* Aquisition Agent for VPMNav views.
*
*  <b>Role</b> Class providing MVC CAA control to go from VPMNav presentation views to ENOVIA model information.
*  <p>
*/
class ExportedByCATImmIdAcqAgent CATImmIdentifierAcquisitionAgent: public CATPathElementAgent{

  CATDeclareClass;

  public:
	  
  /*** constructor
   * @param iBehavior
   * iBehavior can be CATDlgEngSimpleValuation and CATDlgEngWithPrevaluation. 
   * Default is CATDlgEngSimpleValuation. 
  ***/
	CATImmIdentifierAcquisitionAgent(CATDlgEngBehavior iBehavior=NULL);
	
  virtual ~CATImmIdentifierAcquisitionAgent();

  /***
   * Retreive the domain & type of the ENOVIAvpm Object acquired.
   * Role:Once Acquisition is Valuated, returns the domain & type of the acquired information. 
   * With the current behavior, only VPMPartVersion & VPMDocumentRevision or 
   * one of a customer derived class can be returned as type
   * Notice that other inherited method from DialogAgent like GetValue, GetElementValue should not be used.
   * @return the type of the ENOVIAvpm object
   * e.g.: For VPMPartVersion class <code>"PRODUCT/VPMPartVersion"</code> string should be returned
  ***/
  CATUnicodeString GetType();

  /***
   * Retreive the identifiers of the ENOVIAvpm Object acquired. 
   * Role : Identifiers are Unique key attributes by which one can retrieve by query a single one object 
   * in providing the key attributes value. 
   * @param  oNames
   * The names of the identifier attributes
   * e.g.: For VPMPartVersion <code>"V_id"</code> or <code> "V_version"</code> will compose the list
   * @param  oValues
   * The values of the identifier attributes. 
   * For each identifier attribute in the list oNames correspond a value (at same rank) in the oValues list. 
   * @return S_OK when identifiers can be all retrieved otherwise E_FAIL.
  ***/
  HRESULT GetIdentifiers(CATListOfCATUnicodeString& oNames, CATListOfCATUnicodeString& oValues);

 /***
   * Retreive the identificator of the ENOVIAvpm Object acquired. 
   * Role : Identificator are used to interoperate between various element of the architecture 
   *<p>Identificator can be used 
   *<ul>
   *<li>to Run a service on the ENOVIA server,</li>
   *<li>to retreive if the object is currently loadded in the CATIA session,</li>
   *<li>to compare two navigation objects</li>
   *</ul>
   *<p>
   * @param  opId
   * The ENOVIA navigation model identifier
   * @return S_OK when identifiers can be all retrieved otherwise E_FAIL.
  ***/
 HRESULT GetIdentificator(CATIPLMIdentificator** opId);

  private:

  /***
  * Overridden function to capture information about selected object.
  * Role : Analyse the type of object in CATPathElement object and validate against supported types.
  * @param  PathToCheck
  * The Path identifier of the object under the mouse seletion.
  * @param  ElementTypeListPosition
  * Position of the captured element.
  * @return validated CATPathElement of object under mouse selection otherwise NULL.
  */
  CATPathElement* CheckPath (CATPathElement* PathToCheck, int ElementTypeListPosition);

  CATImmIdentifierAcquisitionAgent(CATImmIdentifierAcquisitionAgent &);
  CATImmIdentifierAcquisitionAgent& operator=(CATImmIdentifierAcquisitionAgent&);

  CATImmIdentifierAcquisitionAgentPrivate* _pData;

};

#endif
