//COPYRIGHT Dassault Systemes 2006


#ifndef CATImmCAAServices_H
#define CATImmCAAServices_H

/**
* @CAA2Level L1
* @CAA2Usage U1
*/

#ifdef  _WINDOWS_SOURCE
#ifdef  __CATImmCAAServices
#define ExportedByCATImmCAAServices     __declspec(dllexport)
#else
#define ExportedByCATImmCAAServices     __declspec(dllimport)
#endif
#else
#define ExportedByCATImmCAAServices
#endif

#include "CATUnicodeString.h"
#include "CATCollec.h"

/**
 * This class allows filtering the explore windows of a PRC by a LCA configuration Handler.
 * <b>Role</b>:
 * This class allows filtering the explore windows of a PRC by a LCA configuration Handler.
*/

class  ExportedByCATImmCAAServices CATImmCAAServices 
{
public:



   /**
   * Retreive the current root identifier from the current VPM navigator Window.  
   * <br><b>Role:</b></br> Root identifier may be used for purpose of knowing the root ids & is further used for setting its filter.  
   * @param  oIdentifierNames
   * The identification of the root object (if any)  
   * <br>e.g.: For an �PRODUCT� object of VPMProductRootClass class it contains only one identifier <code>�V_ID�</code>. 
   * @param  oIdentifierValues
   * The identifier of the root object.   
   * <br>e.g.: For an �PRODUCT� object of VPMProductRootClass class there is only one string whose value is the <code>�V_ID�</code> value of the object.  
   * @return 
   * <ul>
   * <li><tt>S_OK</tt>  : if the filter capability is enabled and if such a root object exists.
   * <li><tt>E_FAIL</tt>: returned in case of many errors, comprising current activated window is not VPMNav window, as well no connection with server.  
   * </ul>
   */


   static HRESULT GetCurrentRootIdentifiers(CATListOfCATUnicodeString& oIdentifierNames, CATListOfCATUnicodeString& oIdentifierValues,  CATUnicodeString& oRootType); 

   /**
   * Retreive the current root type from the current VPM navigator Window.  
   * <br><b>Role:</b></br> Root type may be used to interpret identifiers.  
   * @param  oRootType
   * The type of the root identifier
   * <br> 
   * The Filter Type is basically the type of the object it is tied upon. The string is returned that way: 
   * <br>e.g. For a filter on a VPMProductRootClass object of the PRODUCT domain:  <code>�CustomDomain/ENOVIA_VPMRootProductClass�</code> where first part of the string is the customization domain (here �CustomDomain�) and could be PRODUCT, the second part of the string is the product modeler type. 
   * @return
   * <ul>
   * <li><tt>S_OK</tt>  : if the filter capability is enabled and if such a root object exists. 
   * <li><tt>E_FAIL</tt>: returned in case of many errors, comprising current activated window is not VPMNav window, but as well no connection with server. 
   * </ul>
   */
   
   
   static HRESULT GetCurrentRootType(CATUnicodeString& oRootType);
   
   /**
   * @deprecated V5R18 CATImmCAAServices#GetPRCConfigFilter
   * Use @href CATImmCAAServices#GetAllPRCConfigHandlers instead.
   *
   * Read the Configuration Handlers that may then be used for filtering. 
   * <br><b>Role:</b></br> The way to designate the filter, is the way to designate the object it refers to.  Configuration Handlers are designated by a identifier (<code>�ID�</code>).  
   * @param  iPRCIdentifiersNames
   * The identifier name of the object on which to apply filter
   * <br>This is a PRC whose identifier attribute is only V_ID.
   * @param  iPRCIdentifiersValues
   * The identifier value of the object on which to apply filter
   * @param  oCHIdsNames
   * The identifier names of the config handlers associated with the PRC. 
   * <br>The first identifier (ID) will be returned. 
   * @param  oCHIdsValues
   * The identifier values of the config handler.
   * @return 
   * <ul>
   * <li><tt>S_OK</tt>  : when identifiers can be retrieved (even if NULL string).
   * <li><tt>E_FAIL</tt>: returned in case of an error. 
   * </ul>
   */
  
  
  static HRESULT GetPRCConfigFilter(const CATListOfCATUnicodeString& iPRCIdentifiersNames, const CATListOfCATUnicodeString& iPRCIdentifiersValues, CATListOfCATUnicodeString& oCHdsNames, CATListOfCATUnicodeString& oCHdsValues);
  
  /**
   * @deprecated V5R18 CATImmCAAServices#SetPRCConfigFilter
   * Use @href CATImmCAAServices#SetPRCConfigHandler instead.
   *
   * Set the Configuration Handler used for filtering.
   * <br><b>Role:</b></br> The way to designate the filter, is the way to designate the object it refers to.  Configuration Handlers are designated by an identifier (<code>�ID�</code>). 
   * @param  iPRCIdentifiersNames
   * The identifier of the object on which to apply filter
   * <br>This is a PRC whose identifier attribute is only V_ID.
   * @param  iPRCIdentifiersValues
   * The identifier value of the PRC on which to apply filter
   * @param  iCHIdsNames
   * The identifier names of the config handler to be currently set. 
   * <br>The first identifier (ID) is used in the list to designate the config handler.First identifier may be a NULL string, then it means that Config Filter is set to NULL. 
   * @param  iCHIdsValues
   * The identifier values of the config handler to be currently set.
   * @return
   * <ul>
   * <li><tt>S_OK</tt>  : when identifiers can be retrieved and associated to such Filter object (if NULL string). 
   * <li><tt>E_FAIL</tt>: returned in case of many errors, current activated window is not VPMNav window, but as well no connection with server or more than one config handler being set on a PRC. 
   * </ul>
   */
   
   
   static HRESULT SetPRCConfigFilter(const CATListOfCATUnicodeString& iPRCIdentifiersNames, const CATListOfCATUnicodeString& iPRCIdentifiersValues, const CATListOfCATUnicodeString& iCHIdsNames, const CATListOfCATUnicodeString& iCHIdsValues);
   
   /**
   * Open a search result window. 
   * <br><b>Role:</b></br> The way to retrieve Objects saved in Enovia through a Search Result... 
   * @param  iType
   * The object type follows this format {CustomDomain}/{Object type}. 
   * e.g. For a VPMProductRootClass object of the PRODUCT domain:  <code>�CustomDomain/ENOVIA_VPMRootProductClass�</code> where first part of the string is the customization domain (here �CustomDomain�) and could be PRODUCT, the second part of the string is the product modeler type. 
   * @param  iAttrNameList
   * List of the attribute names describing an object.For example, a DR is described with the attributes V_Id and V_version 
   * @param  iNb
   * Number of attribute sets.
   * @param  iAttrValueList
   * The dimension of this array is iNb. In each CATListOfCATUnicodeString, you define the list of values for the set of 
   * attributes defined in iAttrNameList. The number of values in this list is equal to the number of attributes in iAttrNameList.  
   * @return
   * <ul>
   * <li><tt>S_OK</tt>  : When everything is OK
   * <li><tt>E_FAIL</tt>: 
   * </ul>
   */

   static HRESULT OpenSearchResultWindow(const CATUnicodeString& iType,const CATListOfCATUnicodeString& iAttrNameList,const int iNb,const CATListOfCATUnicodeString* iArrayValueList);

   // NFO - Highlight
   /**
   * Set a ConfigHandler and specify the Expand level.
   * <br><b>Role:</b></br> Set the Configuration Handler on the PRC and expand the product assembly up to given expand level. This method also checks whether Configuration Handler is actually attached to PRC based on check flag passed to the method.
   * @param  iPRCName
   * The name of the object (PRC) on which to apply filter
   * @param  iCHName
   * The name of the config handler to be currently set. 
   * @param  iExpandLevel
   * Level up to which to expand PS after applying CH. Default is 0 which means collapsed.
   * @param  iCheckFlag
   * Flag to check whether CH was applied on the PRC. Default in No check (for better performance)
   * @return
   * <ul>
   * <li><tt>S_OK</tt>  : Successful execution
   * <li><tt>E_FAIL</tt>: If iCheckFlag equals 1 and the ConfigHandler is not actually associated to the PRC
   * <li><tt>E_FAIL</tt>: Returned in case of many errors, current activated window is not VPMNav window, but as well no connection with server or config handler not set on a PRC. 
   * </ul>
   */
   static HRESULT SetPRCConfigHandler(const CATUnicodeString& iPRCName, const CATUnicodeString& iCHName, const int& iExpandLevel=0, const int& iCheckFlag=0);
   
   /**
   * Return all ConfigHandlers associated with the PRC.
   * <br><b>Role:</b></br> Read all the Configuration Handlers associated to a given PRC and that may be used for filtering the PRC. At a time there may be many Configuration Handlers associated with PRC. This API will return all of them.
   * @param  iPRCName
   * The name of the object (PRC) on which to apply filter
   * @param  oCHNames
   * The names of the config handlers associated with the PRC. 
   * @return 
   * <ul>
   * <li><tt>S_OK</tt>  : When identifiers can be retrieved.
   * <li><tt>E_FAIL</tt>: Returned in case of an error. 
   * </ul>
   */
   static HRESULT GetAllPRCConfigHandlers(const CATUnicodeString& iPRCName, CATListOfCATUnicodeString& oCHNames);

   /**
   * Return the current ConfigHandler applied to the PRC.
   * <br><b>Role:</b></br> Read the Current Configuration Handler that is currently set for the PRC. At a time there could be more than one Configuration Handlers associated with PRC but only one is active. This API will return the same.
   * @param  iPRCName
   * The name of the object (PRC) on which to apply filter.
   * @param  oCHName
   * The name of the current config handler applied to the PRC. 
   * @return 
   * <ul>
   * <li><tt>S_OK</tt>  : When config handler can be retrieved.
   * <li><tt>E_FAIL</tt>: Returned in case of an error.
   * </ul>
   */
   static HRESULT GetPRCConfigHandler(const CATUnicodeString& iPRCName, CATUnicodeString& oCHName);
   
};

#endif
