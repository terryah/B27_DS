#ifndef __TIE_CATIImmNavAddin
#define __TIE_CATIImmNavAddin

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATIImmNavAddin.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface CATIImmNavAddin */
#define declare_TIE_CATIImmNavAddin(classe) \
 \
 \
class TIECATIImmNavAddin##classe : public CATIImmNavAddin \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(CATIImmNavAddin, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual void              CreateCommands () ; \
      virtual CATCmdContainer * CreateToolbars () ; \
};



#define ENVTIEdeclare_CATIImmNavAddin(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual void              CreateCommands () ; \
virtual CATCmdContainer * CreateToolbars () ; \


#define ENVTIEdefine_CATIImmNavAddin(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
void               ENVTIEName::CreateCommands ()  \
{ \
 (ENVTIECALL(CATIImmNavAddin,ENVTIETypeLetter,ENVTIELetter)CreateCommands ()); \
} \
CATCmdContainer *  ENVTIEName::CreateToolbars ()  \
{ \
return (ENVTIECALL(CATIImmNavAddin,ENVTIETypeLetter,ENVTIELetter)CreateToolbars ()); \
} \


/* Name of the TIE class */
#define class_TIE_CATIImmNavAddin(classe)    TIECATIImmNavAddin##classe


/* Common methods inside a TIE */
#define common_TIE_CATIImmNavAddin(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(CATIImmNavAddin, classe) \
 \
 \
CATImplementTIEMethods(CATIImmNavAddin, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(CATIImmNavAddin, classe, 1) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(CATIImmNavAddin, classe) \
CATImplementCATBaseUnknownMethodsForTIE(CATIImmNavAddin, classe) \
 \
void               TIECATIImmNavAddin##classe::CreateCommands ()  \
{ \
   ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateCommands (); \
} \
CATCmdContainer *  TIECATIImmNavAddin##classe::CreateToolbars ()  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateToolbars ()); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_CATIImmNavAddin(classe) \
 \
 \
declare_TIE_CATIImmNavAddin(classe) \
 \
 \
CATMetaClass * __stdcall TIECATIImmNavAddin##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_CATIImmNavAddin,"CATIImmNavAddin",CATIImmNavAddin::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_CATIImmNavAddin(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(CATIImmNavAddin, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicCATIImmNavAddin##classe(classe::MetaObject(),CATIImmNavAddin::MetaObject(),(void *)CreateTIECATIImmNavAddin##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_CATIImmNavAddin(classe) \
 \
 \
declare_TIE_CATIImmNavAddin(classe) \
 \
 \
CATMetaClass * __stdcall TIECATIImmNavAddin##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_CATIImmNavAddin,"CATIImmNavAddin",CATIImmNavAddin::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_CATIImmNavAddin(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(CATIImmNavAddin, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicCATIImmNavAddin##classe(classe::MetaObject(),CATIImmNavAddin::MetaObject(),(void *)CreateTIECATIImmNavAddin##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_CATIImmNavAddin(classe) TIE_CATIImmNavAddin(classe)
#else
#define BOA_CATIImmNavAddin(classe) CATImplementBOA(CATIImmNavAddin, classe)
#endif

#endif
