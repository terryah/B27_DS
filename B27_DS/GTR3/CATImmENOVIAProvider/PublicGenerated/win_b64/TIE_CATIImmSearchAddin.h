#ifndef __TIE_CATIImmSearchAddin
#define __TIE_CATIImmSearchAddin

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATIImmSearchAddin.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface CATIImmSearchAddin */
#define declare_TIE_CATIImmSearchAddin(classe) \
 \
 \
class TIECATIImmSearchAddin##classe : public CATIImmSearchAddin \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(CATIImmSearchAddin, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual void              CreateCommands () ; \
      virtual CATCmdContainer * CreateToolbars () ; \
};



#define ENVTIEdeclare_CATIImmSearchAddin(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual void              CreateCommands () ; \
virtual CATCmdContainer * CreateToolbars () ; \


#define ENVTIEdefine_CATIImmSearchAddin(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
void               ENVTIEName::CreateCommands ()  \
{ \
 (ENVTIECALL(CATIImmSearchAddin,ENVTIETypeLetter,ENVTIELetter)CreateCommands ()); \
} \
CATCmdContainer *  ENVTIEName::CreateToolbars ()  \
{ \
return (ENVTIECALL(CATIImmSearchAddin,ENVTIETypeLetter,ENVTIELetter)CreateToolbars ()); \
} \


/* Name of the TIE class */
#define class_TIE_CATIImmSearchAddin(classe)    TIECATIImmSearchAddin##classe


/* Common methods inside a TIE */
#define common_TIE_CATIImmSearchAddin(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(CATIImmSearchAddin, classe) \
 \
 \
CATImplementTIEMethods(CATIImmSearchAddin, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(CATIImmSearchAddin, classe, 1) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(CATIImmSearchAddin, classe) \
CATImplementCATBaseUnknownMethodsForTIE(CATIImmSearchAddin, classe) \
 \
void               TIECATIImmSearchAddin##classe::CreateCommands ()  \
{ \
   ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateCommands (); \
} \
CATCmdContainer *  TIECATIImmSearchAddin##classe::CreateToolbars ()  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateToolbars ()); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_CATIImmSearchAddin(classe) \
 \
 \
declare_TIE_CATIImmSearchAddin(classe) \
 \
 \
CATMetaClass * __stdcall TIECATIImmSearchAddin##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_CATIImmSearchAddin,"CATIImmSearchAddin",CATIImmSearchAddin::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_CATIImmSearchAddin(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(CATIImmSearchAddin, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicCATIImmSearchAddin##classe(classe::MetaObject(),CATIImmSearchAddin::MetaObject(),(void *)CreateTIECATIImmSearchAddin##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_CATIImmSearchAddin(classe) \
 \
 \
declare_TIE_CATIImmSearchAddin(classe) \
 \
 \
CATMetaClass * __stdcall TIECATIImmSearchAddin##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_CATIImmSearchAddin,"CATIImmSearchAddin",CATIImmSearchAddin::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_CATIImmSearchAddin(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(CATIImmSearchAddin, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicCATIImmSearchAddin##classe(classe::MetaObject(),CATIImmSearchAddin::MetaObject(),(void *)CreateTIECATIImmSearchAddin##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_CATIImmSearchAddin(classe) TIE_CATIImmSearchAddin(classe)
#else
#define BOA_CATIImmSearchAddin(classe) CATImplementBOA(CATIImmSearchAddin, classe)
#endif

#endif
