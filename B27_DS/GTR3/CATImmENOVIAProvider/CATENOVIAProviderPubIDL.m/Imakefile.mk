# COPYRIGHT DASSAULT SYSTEMES 2003
#======================================================================
# Imakefile for module CATImmPubIdl.m
# Module for compilation of the public IDL interfaces
#======================================================================
#
#  Dec 2003  Creation: Code generated by the CAA wizard  mnr
#======================================================================
#
# NO BUILD           
#

BUILT_OBJECT_TYPE=NONE

SOURCES_PATH=PublicInterfaces
COMPILATION_IDL=YES

