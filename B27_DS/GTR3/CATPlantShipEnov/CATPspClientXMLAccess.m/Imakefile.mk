# COPYRIGHT DASSAULT SYSTEMES 2005
#======================================================================
# Imakefile for module CATPenModXMLAccess.m
#======================================================================
#
#  Aug 2004  Creation: Code generated by the CAA wizard  AGQ
#======================================================================
#
# SHARED LIBRARY 
#
BUILT_OBJECT_TYPE=SHARED LIBRARY 
 
# DO NOT EDIT :: THE CAA2 WIZARDS WILL ADD CODE HERE
WIZARD_LINK_MODULES = JS0GROUP \
CATObjectModelerBase \
CATPDMBase \
CATXMLParserItf 


# END WIZARD EDITION ZONE

LINK_WITH = $(WIZARD_LINK_MODULES)

CXX_EXCEPTION =

# System dependant variables
#
OS = AIX
#
OS = HP-UX
#
OS = IRIX
#
OS = SunOS
#
OS = Windows_NT
