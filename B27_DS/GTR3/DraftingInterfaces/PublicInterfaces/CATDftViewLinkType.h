#ifndef __CATDftViewLinkType_h__
#define __CATDftViewLinkType_h__
// COPYRIGHT DASSAULT SYSTEMES 1999

/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */


/**
  * Positionning link type definition.
  * @Param AUTOMATIC
  *   The positionning is managed regarding of the views
  *   initial positionning.
  */
enum CATDftViewLinkType {	AUTOMATIC=0,DFT_PERPENDICULAR,DFT_ALIGNED,DFT_NOTPOSITIONED};

#endif
