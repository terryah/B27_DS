// COPYRIGHT Dassault Systemes 2008
//===================================================================
//
// CATIDftElementInSystem.h
// Define the CATIDftElementInSystem interface
//
//===================================================================
//
// Usage notes:
//   New interface: describe its use here
//
//===================================================================
//
//  Jan 2008  Creation: Code generated by the CAA wizard  SMR
//===================================================================
#ifndef CATIDftElementInSystem_H
#define CATIDftElementInSystem_H

#include "DraftingItfCPP.h"
#include "CATBaseUnknown.h"

/**
* @CAA2Level L1
* @CAA2Usage U3
*/

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByDraftingItfCPP IID IID_CATIDftElementInSystem;
#else
extern "C" const IID IID_CATIDftElementInSystem ;
#endif

//------------------------------------------------------------------

/**
*
* Interface used to get the system on which the element is linked.
* <b>Role</b>: By using this interface on a dimension,
* it is possible to retrieve the dimension system containing the dimension.
* @href CATIDrwDimSystem 
*/
class ExportedByDraftingItfCPP CATIDftElementInSystem: public CATBaseUnknown
{
  CATDeclareInterface;

  public:

  /*
  * Retrieves the system linked to this element.
  * @param oSystem [out, CATBaseUnknown#Release]
  *      Returned system.
  * @return
  * <dt><tt>S_OK</tt> <dd>The system is retrieved.
  * <dt><tt>S_FALSE</tt> <dd>There is no system linked to this element.
  * <dt><tt>E_FAIL</tt> <dd>if the operation failed.
  * </dl>
  */
  virtual HRESULT GetSytemFromElement(CATBaseUnknown ** pSystem) =0;

  // No constructors or destructors on this pure virtual base class
  // --------------------------------------------------------------
};
CATDeclareHandler( CATIDftElementInSystem, CATBaseUnknown ) ;
//------------------------------------------------------------------

#endif
