// COPYRIGHT DASSAULT SYSTEMES 1997
#ifndef CATIADrawingWeldings_IDL
#define CATIADrawingWeldings_IDL
/*IDLREP*/


#include "CATIADrawingWelding.idl"
#include "CATIACollection.idl"
#include "CATVariant.idl"

/**
* @CAA2Level L1
* @CAA2Usage U3
*/

 /**
  * A collection of all the drawing weldings currently managed by a
  * drawing view of drawing sheet in a drawing document.
  */
interface CATIADrawingWeldings : CATIACollection
{
 /**
  * Creates a drawing welding and adds it to the drawing weldings collection.
  * This drawing welding becomes the active one.
  * @param iSymbol
  *   The drawing welding symbol to assign to the drawing welding
  * @param iPositionX,iPositionY
  *   The drawing welding x and y coordinates, expressed in millimeters,
  *   and expressed with respect to the view coordinate system
  * @return The created drawing welding
  * <!-- @sample -->
  * </dl>
  * <dt><b>Example:</b>
  * <dd>
  * The following example creates a drawing welding, 
  * retrieved in <tt>MyWelding</tt>, in the <tt>MyView</tt> drawing view.
  * This view belongs to the drawing view collection of the drawing sheet.
  * <pre>
  * Dim MyView As DrawingView
  * Set MyView = MySheet.Views.ActiveView
  * Dim MyWelding As DrawingWelding
  * Set MyWelding = 
  *    MyView.Weldings.<font color="red">Add</font>(catSquareWelding, 0., 0.)
  * </pre>
  * </dl>
  */
  HRESULT Add(in CatWeldingSymbol iSymbol, in double iPositionX, in double iPositionY,
              out /*IDLRETVAL*/ CATIADrawingWelding oNewDrawingWelding);


 /**
  * Removes a drawing welding from the drawing weldings collection.
  * @param iIndex
  *   The index of the drawing welding to remove from
  *   the collection of drawing weldings.
  *   As a numerics, this index is the rank of the drawing text
  *   in the collection.
  *   The index of the first drawing welding in the collection is 1, and
  *   the index of the last drawing welding is Count.
  * <!-- @sample -->
  * </dl>
  * <dt><b>Example:</b>
  * <dd>
  * The following example removes the third drawing welding 
  * from the drawing welding collection of the active view
  * of the active document, supposed to be a drawing document.
  * <pre>
  * Dim MyView As DrawingView
  * Set MyView  = MySheet.Views.ActiveView
  * MyView.Drawing.<font color="red">Remove</font>(3)
  * </pre>
  * </dl>
  */
  HRESULT Remove(in long iIndex);

/**
  * Returns a drawing welding using its index from the drawing weldings collection.
  * @param iIndex
  *   The index of the drawing welding to retrieve from the collection of drawing weldings.
  *   As a numerics, this index is the rank of the drawing welding in the collection.
  *   The index of the first drawing welding in the collection is 1, and
  *   the index of the last drawing welding is Count.
  * @return The retrieved drawing welding
  * <!-- @sample -->
  * </dl>
  * <dt><b>Example:</b>
  * <dd>
  * This example retrieves in <tt>ThisDrawingWelding</tt> the second drawing welding,
  * in the drawing welding collection of the active view
  * in the active sheet, in the active document supposed to be a drawing document.
  * <pre>
  * Dim MyView  As DrawingView
  * Set MyView  = MySheet.Views.ActiveView
  * Dim ThisDrawingWelding As DrawingWelding
  * Set ThisDrawingWelding = MyView.Weldings.<font color="red">Item</font>(2)
  * </pre>
  * </dl>
  */
  HRESULT Item(in long iIndex, out /*IDLRETVAL*/ CATIADrawingWelding oItem);
			
};

// Interface name : CATIADrawingWeldings
#pragma ID CATIADrawingWeldings "DCE:187945cc-3ec1-11d3-ad46006094eb80b5"
#pragma DUAL CATIADrawingWeldings

// VB object name : DrawingTexts
#pragma ID DrawingWeldings "DCE:6fbf7a4a-3ec1-11d3-ad46006094eb80b5"
#pragma ALIAS CATIADrawingWeldings DrawingWeldings

#endif

