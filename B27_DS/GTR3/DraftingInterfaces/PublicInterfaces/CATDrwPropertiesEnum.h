#ifndef CATDrwPropertiesEnum_H
#define CATDrwPropertiesEnum_H

// COPYRIGHT DASSAULT SYSTEMES 1999

/**
* @CAA2Level L1
* @CAA2Usage U1
*/

/**
* Context in which the refresh of an object is asked after modifying its properties. 
* @param CATDrwRefreshNone
* No special context has been defined yet.
* @see CATIDrwProperties
*/
enum CATDrwRefreshContext{ CATDrwRefreshNone = 0, CATDrwRefreshVisu = 1 };

#endif
