// COPYRIGHT DASSAULT SYSTEMES 1999
//===================================================================
//
// CATIDftGenView.h
// Sheet feature type interface.
//
//===================================================================
//
// Usage notes:
//
//===================================================================
//
//  Mar 1999  Creation: Code generated by the CAA wizard  MMR
//===================================================================

/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */

#ifndef CATIDftGenView_H
#define CATIDftGenView_H

// Drafting
#include "CATDftFilletRep.h"
#include "CATDftRepresentationMode.h"

// System
#include "IUnknown.h"
#include "CATMacForIUnknown.h"
#include "CATBooleanDef.h"

#include "CATDraftingInterfaces.h"

// Mathematics
class CATMathPlane;
class CATMathPoint;

class CATIDftGenViewLinks;

#include "DraftingItfCPP.h"
#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByDraftingItfCPP IID IID_CATIDftGenView ;
#else
extern "C" const IID IID_CATIDftGenView ;
#endif

#include "CATDftGenBox3DDefs.h"

/**
 * Interface to retrieve attributs dedicated to generative drawing views.
 * <b>Role</b>: This interface to retrieve informations dedicated to generative drawing views.
 */

class ExportedByDraftingItfCPP CATIDftGenView : public IUnknown
{
public:


   /**
   * Used to know if the view is generative.
   * @param oGenStatus
   *   <br>  TRUE = A result is generated from 3D
   *   <br>  FALSE = The view isn't generative
   */
   virtual HRESULT IsGenerative(boolean *oGenStatus) const = 0;


   /**
   * Gets the generative view hidden line representation.
   * @param oShowHidden
   *   <br> TRUE = Hidden lines are show 
   *   <br>  FALSE = Hidden lines are not show
   */  
   virtual HRESULT IsShowHidden(boolean *oShowHidden) const = 0;

   /**
   * Sets the generative view hidden line representation.
   * @param iShowHidden
   *   <br>  TRUE = Hidden lines are show 
   *   <br>  FALSE = Hidden lines are not show
   */  
   virtual HRESULT SetShowHidden(const boolean iShowHidden) = 0;
    
   /**
   * Gets the generative view smooth edges representation.
   * @param oShowSmooth
   *   <br> TRUE = Smooth edges are show
   *   <br> FALSE = Smooth edges are not show
   */  
   virtual HRESULT IsShowSmooth(boolean *oShowSmooth) const = 0;

   /**
   * Sets the generative view smooth edges representation.
   * @param iShowSmooth
   *   <br>  TRUE = Smooth edges are show 
   *   <br>  FALSE = Smooth edges are not show
   */  
   virtual HRESULT SetShowSmooth(const boolean iShowSmooth) = 0;
   
   /**
   * Gets the generative view fillets representation.
   * @param oFilletRep
   *   The different modes are NONE, BOUNDARY, ORIGINALEDGE
   */
   /** @nodoc */
   virtual HRESULT GetFilletRep (CATDftFilletRep *oFilletRep) const =0;

   /**
   * Sets the generative view fillets representation.
   * @param iFilletRep
   *   The different modes are NONE, BOUNDARY, ORIGINALEDGE
   */
   /** @nodoc */
   virtual HRESULT SetFilletRep (const CATDftFilletRep iFilletRep) =0;
   
   /**
   * Used to know if symbolic annotations will be generated from design.
   * @param iType
   *   The type of annotation
   * @param oDressGen
   *   TRUE if generated, FALSE if not
   */
   /** @nodoc */
   virtual HRESULT GetDressUpGeneration (const CATIdent iType, boolean *oDressGen) const =0;

   /**
   * Sets the kind of annotation which have to be generated from design.
   * @param iType
   *   The type of annotation
   * @param iDressGen
   *   TRUE if generated, FALSE if not
   */
   /** @nodoc */
   virtual HRESULT SetDressUpGeneration (const CATIdent iType, const boolean iDressGen) =0;
   
   /**
   * Returns the projection plane on the Generative View.
   * @param oPlane
   *    the CATMathPlane wich describes the projection plane of the view
   */
   virtual HRESULT GetProjectionPlane(CATMathPlane ** oPlane) const = 0;
   
   /**
   * Sets the projection plane on the Generative View.
   * @param iPlane
   *    the CATMathPlane wich describes the projection plane of the view
   */
   virtual HRESULT SetProjectionPlane(CATMathPlane * iPlane) = 0;

   /**
   * Returns the Links Manager of the generative view.
   * @param <tt>CATIDftGenViewLinks ** oLinks</tt>
   * [out] The Links Manager
   * @return
   * Un <tt>HRESULT</tt>
   * <dl>
   * <dt> <tt>S_OK</tt>     <dd> Succeeded
   * <dt> <tt>E_UNEXPECTED</tt>   <dd> Internal Error
   * </dl>
   */
   /** @nodoc */
   virtual HRESULT GetViewLinks(CATIDftGenViewLinks ** oLinks) const = 0; 
   
   /**
   * Returns the object that uses this GenView. In the Drafting Context : the View.
   * @param <tt>const IID & iIID</tt>
   * [in] the required interface
   * @param <tt>IUnknown ** oObject</tt>
   * [out] the required object
   * @return
   * A <tt>HRESULT</tt>
   * <dl>
   * <dt> <tt>S_OK</tt>     <dd> Succeeded
   * <dt> <tt>E_FAIL</tt>   <dd> Invalid IID or no ExtendedObject
   * </dl>
   */
   /** @nodoc */
   virtual HRESULT GetExtendedObject(const IID & iIID, IUnknown ** oObject) = 0;

   /**
   * Gets the generative view representation mode.
   * @param oRepMode
   *   The different modes are EXACT, POLYHEDRIC
   * @return
   * <dt><tt>S_OK</tt> <dd>if operation succeeded.
   * <dt><tt>E_FAIL</tt> <dd>if an unspecified failure has occurred.
   * </dl>
   */
   /** @nodoc */
   virtual HRESULT GetRepresentationMode (CATDftRepresentationMode *oRepMode) const =0;

   /**
   * Sets the generative view representation mode.
   * @param iRepMode
   *   The different modes are EXACT, POLYHEDRIC
   * @return
   * <dt><tt>S_OK</tt> <dd>if operation succeeded.
   * <dt><tt>E_FAIL</tt> <dd>if the drawing view owns a detail, section or breakout 
   * specification, or if an unspecified failure has occurred.
   * </dl>
   */
   /** @nodoc */
   virtual HRESULT SetRepresentationMode (const CATDftRepresentationMode iRepMode) =0;

   /**
   * Retrieves box mathematical definition.
   *   @param oBox
   *      The box as xmin, xmax, ymin, ymax, zmin, zmax
   */
   virtual HRESULT GetBox3D (CATMathPoint* oPointA, CATMathPoint* oPointB, CATMathPoint* oPointC, CATMathPoint* oPointD) = 0;

   /**
   * Sets or create a clipping box (not available if a DMUBox already exists on the view).
   * The view may own only one box
   *  @param iBox
   *      The box as xmin, xmax, ymin, ymax, zmin, zmax
   * @return
   * <dt><tt>S_OK</tt> <dd>if operation succeeded.
   * <dt><tt>E_FAIL</tt> <dd>if A DMUBox exists on the view
   */
   virtual HRESULT SetBox3D (const CATMathPoint* iPointA,const CATMathPoint* iPointB,const CATMathPoint* iPointC,const CATMathPoint* iPointD) = 0;

   /**
   * Sets parameters on a box operator.
   *   @param iClippingType (box, slice or back clipping plane)
   *   @param iClippingMode ** NOT IMPLEMENTED ** 
   * @return
   * <dt><tt>S_OK</tt> <dd>if operation succeeded.
   * <dt><tt>E_FAIL</tt> <dd>if a box does not exist on the view
   */
   virtual HRESULT SetBox3DOptions(const CATDrwGenBox3DType iClippingType=ClippingByBox, 
								   const CATDrwGenBox3DMode iClippingMode=Relimit) = 0;

    /**
   * Gets parameters on a box operator.
   *   @param oClippingType (box, slice or back clipping plane)
   *   @param oClippingMode ** NOT IMPLEMENTED ** 
   * @return
   * <dt><tt>S_OK</tt> <dd>if operation succeeded.
   * <dt><tt>E_FAIL</tt> <dd>if a box does not exist on the view
   */
   virtual HRESULT GetBox3DOptions(CATDrwGenBox3DType &oClippingType, 
								   CATDrwGenBox3DMode &oClippingMode) = 0;

   /**
   * Used to know if the Box3D definition comes from an external object (i.e. DMUBOX).
   * @param oBoxModifiable
   *   TRUE  = standalone box
   *   FALSE = box created with an external link (i.e. DMUBOX)
   */
   virtual HRESULT IsBox3DModifiable (CATBoolean *oBoxModifiable) =0;

   /**
   * Removes a box3D operator in the view.
   */
   virtual HRESULT RemoveBox3D () =0;

private :
    
    CATDeclareInterface;
    
};
#endif








