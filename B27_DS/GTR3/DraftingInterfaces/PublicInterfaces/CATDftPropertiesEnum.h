#ifndef CATDftPropertiesEnum_H
#define CATDftPropertiesEnum_H
//==============================================================================
//	COPYRIGHT DASSAULT SYSTEMES 2002
//==============================================================================

/**
* @CAA2Level L1
* @CAA2Usage U1
*/

/**
* Enum used to specified the refresh context.
* <br><i>DftRefreshAll is the only value available today. Other values will be 
* created in the future.</i>
* @see CATIDftProperties
*/
enum CATDftRefreshContext{ DftRefreshAll = 0 };

#endif
