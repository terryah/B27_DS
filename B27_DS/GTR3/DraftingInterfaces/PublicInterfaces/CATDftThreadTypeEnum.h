#ifndef CATDftThreadTypeEnum_H
#define CATDftThreadTypeEnum_H
//	COPYRIGHT DASSAULT SYSTEMES 2006
/**
* @CAA2Level L1
* @CAA2Usage U1
*/

/**
* The thread type.
*   @param CATDftThreaded
*    The feature is threaded.
*   @param CATDftTaped 
*    The feature is taped.
*/
enum CATDftThreadTypeEnum{ CATDftThreaded, CATDftTaped };

#endif
