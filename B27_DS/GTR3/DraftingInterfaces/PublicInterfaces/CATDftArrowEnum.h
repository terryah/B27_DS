#ifndef CATIDftArrowEnum_H
#define CATIDftArrowEnum_H

// COPYRIGHT DASSAULT SYSTEMES 2001

/**
* @CAA2Level L1
* @CAA2Usage U1
*/

/**
* Arrow extrimity.
* @param CATDftArrowTail
*   Tail of arrow.
* @param CATDftArrowHead
*   Head of Arrow.
*/
enum CATDftArrowExtremity { CATDftArrowTail = 0, CATDftArrowHead = 1 };

#endif

