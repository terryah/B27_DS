#ifndef CATDftTextParameters_H
#define CATDftTextParameters_H
//==============================================================================
//                                   CATIA - Version 5
//	COPYRIGHT DASSAULT SYSTEMES 1999
//------------------------------------------------------------------------------
// FILENAME    :    CATDftTextParameters.h
// LOCATION    :    DraftingInterfaces\PublicInterfaces
// AUTHOR      :    XJT
// DATE        :    April 1999
//------------------------------------------------------------------------------
// DESCRIPTION :    Enums for Text
//
//------------------------------------------------------------------------------
// COMMENTS    :    
//                  
//------------------------------------------------------------------------------
// MODIFICATIONS    user       date      purpose
//------------------------------------------------------------------------------

/**
* @CAA2Level L1
* @CAA2Usage U1
*/

/**
* Global text parameters.
* Enum used to specified global text parameters.
* @see CATIDftText
*/
enum DftTextSetting { 
  DftBorderMode  = 0  , // INTEGER
  DftAutoFlip    = 1  , // INTEGER
  DftLineSpacing = 2  , // DOUBLE
  DftCharSlant   = 3  , // DOUBLE
  DftCharSpacing = 4 }; // DOUBLE 

/**
* Wrapping mode.
* Enum used to specified wrapping behaviour.
* @see CATIDftText
*/
enum DftWrappingMode { 
  DftWrappingOff  = 0 ,
  DftWrappingOn   = 1 ,
  DftWrappingAuto = 2};

/**
* Justification type.
* <br>Enum used to specified text justification.
* @see CATIDftTextProperties
*/
enum DftJustification {  
  DftLeft = 1, 
  DftCenter = 2, 
  DftRight = 3, 
  DftJustified = 4 };

/**
* Anchor point positions. 
* <br>Enum used to specified text anchor point.
* @see CATIDftTextProperties
*/
enum DftAnchorPoint{ 
  DftTopLeft      = 1, 
  DftMiddleLeft   = 2, 
  DftBottomLeft   = 3, 
  DftTopCenter    = 4, 
  DftMiddleCenter = 5, 
  DftBottomCenter = 6, 
  DftTopRight     = 7,   
  DftMiddleRight  = 8,   
  DftBottomRight  = 9 };

/**
* Frame type. 
* Enum used to specified text frame type.
* @see CATIDftTextProperties
*/
enum DftFrameType{ 
  DftNone         = 0, 
  DftRectangle    = 1, 
  DftSquare       = 2, 
  DftCircle       = 3,
  DftScoredCircle = 4, 
  DftDiamond      = 5, 
  DftTriangle     = 6, 
  DftRightFlag    = 7, 
  DftLeftFlag     = 8, 
  DftBothFlag     = 9, 
  DftOblong       = 10, 
  DftEllipse      = 11,
  DftFixRectangle    = 51, 
  DftFixSquare       = 52, 
  DftFixCircle       = 53,
  DftFixScoredCircle = 54, 
  DftFixDiamond      = 55, 
  DftFixTriangle     = 56, 
  DftFixRightFlag    = 57, 
  DftFixLeftFlag     = 58, 
  DftFixBothFlag     = 59, 
  DftFixOblong       = 60, 
  DftFixEllipse      = 61,
  DftCustom       = 255 };  

/** 
* GDT tolerance symbols.
* @see CATIDftGDT
*/
enum DftGDTSymbol { 
  DFT_GDT_NOSYMBOL ,
  DFT_GDT_STRAIGHTNESS ,
  DFT_GDT_FLATNESS ,
  DFT_GDT_CIRCULARITY ,
  DFT_GDT_CYLINDRICITY ,
  DFT_GDT_LINEPROFILE ,
  DFT_GDT_SURFACEPROFILE ,
  DFT_GDT_ANGULARITY ,
  DFT_GDT_PERPENDICULARITY ,
  DFT_GDT_PARALLELISM ,
  DFT_GDT_POSITION ,
  DFT_GDT_CONCENTRICITY ,
  DFT_GDT_SYMMETRY ,
  DFT_GDT_CIRCULARRUNOUT ,
  DFT_GDT_TOTALRUNOUT };					

/** 
* GDT modification symbols.
* @see CATIDftGDT
*/
enum DftGDTModifier { 
  DFT_GDT_NOMODIFIER ,
  DFT_GDT_MAX ,
  DFT_GDT_MIN ,
  DFT_GDT_PROJ ,
  DFT_GDT_FREE ,
  DFT_GDT_TFU ,
  DFT_GDT_TANGENT,
  DFT_GDT_ST,
  DFT_GDT_ARROW };

/** 
* GDT modification symbols.
* @see CATIDftGDT
*/
enum DftGDTModifiers { 
  Dft_None = -1,
  Dft_Diameter = 0,
  Dft_Free = 24,
  Dft_Least = 25,
  Dft_Max = 26,
  Dft_Proj = 27,
  Dft_Nas = 28,
  Dft_Tangent = 29,
  Dft_ST = 37,
  Dft_Arrow = 38,
  Dft_Unequally = 39
};

/** 
* GDT value types.
* @see CATIDftGDT
*/
enum DftGDTValueType {
  DFT_GDT_NOVALUETYPE ,
  DFT_GDT_DIAMETER ,
  DFT_GDT_RADIUS };

/**
* Defines behaviour of an associativity.
* Override  : Overrides the associativity to match the new position.
* Recompute : Keep the delta information of the associativity (the position given is only an indication).
* Delete    : Deletes the associativity.
* @see CATIDftAnnotation
*/
enum DftAssociativityMode {
  Override  = 0,
  Recompute = 1,
  Delete    = 2};

/** Define the possible orientation reference of an annotation.
* Sheet   : the angle of the annotation is given in the sheet.<BR>
* View    : the angle of the annotation is given in its view.<BR>
* Element : the angle of the annotation is given associatively to an element.
* @see CATIDftAnnotation
*/
enum DftOrientationReference {
  DftOrientSheet  = 0,
  DftOrientView   = 1,
  DftOrientElement= 2};
  
/**
*                    4
*             7   /-------
*             8  /    3
*            \  / 6   1
*           5 \/      2.
*/
enum DftRoughnessText
{
	Rough_FirstRequirement   = 1,
  Rough_SecondRequirement  = 2,
	Rough_OtherRequirement   = 3,
  Rough_ProductionMethod   = 4,
	Rough_MachiningAllowance = 5,
	Rough_CutOff             = 6,
	Rough_Max                = 7,
  Rough_Min                = 8
};

/**
* Kind of roughness specification for machining process.
*/
enum DftRoughType
{
	Rough_Basic,
	Rough_RemovalRequired,
	Rough_RemovalProhibited
};

/**
* Kind of roughness specification for surface lay.
*/
enum DftRoughSurfaceLay
{
	RoughMode_None,
	RoughMode_Parallel,
	RoughMode_Perpendicular,
	RoughMode_Crossed,
	RoughMode_Multidirection,
	RoughMode_Circular,
	RoughMode_Radial,
	RoughMode_Particulate
};

/** 
* Thread types.
*/
enum DftThreadType { 
  DftThreaded, 
  DftTaped 
};

#endif
