#ifndef CatDraftingSettingAtt_H
#define CatDraftingSettingAtt_H

/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */

// COPYRIGHT DASSAULT SYSTEMES 2003
//--------------------------------------------------------------------------
// Enums for drafting settings
//--------------------------------------------------------------------------

/**
* Enum for the way of new sheet creation.
* @param CATDrwFirstSheet
*   Creation from the first sheet
* @param CATDrwStyle
*   Creation from sheet style
*/
enum CatDrwNewSheetFrom { catDrwFirstSheet = 0, catDrwStyle = 1};

#endif
