#ifndef CatDimLineRep_IDL
#define CatDimLineRep_IDL
/*IDLREP*/

// COPYRIGHT DASSAULT SYSTEMES 
//--------------------------------------------------------------------------
// 
// ept - 10/98 Extracted from CATIADrawingDimension.idl
//--------------------------------------------------------------------------

/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */
 

/**
 * Dimension line representation.
 * <b>Role</b>: CatDimLineRep is used to specify dimension line representation.
 * @param catDimUndef
 *   Undefined. 
 * @param catDimHoriz,
 *   Horizontal.
 * @param catDimVert,
 *   Vertical. 
 * @param catDimAuto,
 *   Automatic. 
 * @param catDimUserDefined,
 *   User defined. 
 * @param catDimTrueDim, 
 *   True dimension. 
 * @param catDimParallel,
 *   Parallel (for curvilinear dimensions only).
 * @param catDimOffset,
 *   Offset (for curvilinear dimensions only).
 */
enum CatDimLineRep
{ 
	catDimUndef,
	catDimHoriz,
	catDimVert,
	catDimAuto,
	catDimUserDefined,
	catDimTrueDim,
    catDimParallel,
    catDimOffset
};

	                     
#endif
