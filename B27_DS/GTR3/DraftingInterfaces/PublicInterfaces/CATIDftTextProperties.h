#ifndef CATIDftTextProperties_H
#define CATIDftTextProperties_H
//==============================================================================
//                                   CATIA - Version 5
//	COPYRIGHT DASSAULT SYSTEMES 1999
/**
* @CAA2Level L1
* @CAA2Usage U3
*/
//==============================================================================

// Inheritance
#include "CATIDftProperties.h"

// Parameters
#include "CATCORBABoolean.h"

// System
#include "CATBooleanDef.h"

// Forward definition for method SetValue
class CATIDftTextProperties;
class CATUnicodeString;

// Enums
#include "CATDftTextParameters.h"

// ExportedBy
#include "CATDraftingInterfaces.h"
#include "DraftingItfCPP.h"
#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByDraftingItfCPP IID IID_CATIDftTextProperties ;
#else
extern "C" const IID IID_CATIDftTextProperties;
#endif

/**
* Behaviour interface to modify elements which contains Texts.
*/
class ExportedByDraftingItfCPP CATIDftTextProperties : public CATIDftProperties
{

  public:

   /**
   * Gets the short name of the font. 
   * @param oFontName
   * @return 
   * S_OK : Method correctly executed. 
   * <br>No restriction.
   * <br>S_READONLY : Method correctly executed.
   * <br>Returned value cannot be modified, it's read-only.
   * <br><i>Can be usefull when the property is fixed by a standard, etc.</i>
   * <br>S_UNDEFINED : Method correctly executed.
   * <br>Returned value is undefined.
   * <br><i>Case of an object having several subparts supporting the same property with a different value.</i>
   * <br>E_FAIL : Method execution failed.
   * <br>Reasons of the failure are not given.
   * <br>E_NOTIMPL : No implementation available for this method.
   * @see CATFont
   */
   virtual HRESULT GetFontName( wchar_t **oFontName ) = 0;

   /**
   * Sets the short name of the font.
   * @param iFontName
   * @return 
   * S_OK : Method correctly executed. 
   * <br>No restriction.
   * <br>E_FAIL : Method execution failed.
   * <br>Reasons of the failure are not given.
   * <br>E_NOTIMPL : No implementation available for this method.
   * @see CATFont
   */
   virtual HRESULT SetFontName( const wchar_t *iFontName ) = 0;

   /**
   * Gets the size of the font in millimeter (mm).
   * @param oFontSize
   * @return 
   * S_OK : Method correctly executed. 
   * <br>No restriction.
   * <br>S_READONLY : Method correctly executed.
   * <br>Returned value cannot be modified, it's read-only.
   * <br><i>Can be usefull when the property is fixed by a standard, etc.</i>
   * <br>S_UNDEFINED : Method correctly executed.
   * <br>Returned value is undefined.
   * <br><i>Case of an object having several subparts supporting the same property with a different value.</i>
   * <br>E_FAIL : Method execution failed.
   * <br>Reasons of the failure are not given.
   * <br>E_NOTIMPL : No implementation available for this method.
   */
   virtual HRESULT GetFontSize( double *oFontSize ) = 0;

   /**
   * Sets the size of the font in millimeter (mm).
   * @param iFontSize
   * @return 
   * S_OK : Method correctly executed. 
   * <br>No restriction.
   * <br>E_FAIL : Method execution failed.
   * <br>Reasons of the failure are not given.
   * <br>E_NOTIMPL : No implementation available for this method.
   */
   virtual HRESULT SetFontSize( const double iFontSize ) = 0;

   /**
   * Gets the Bold style of the font.
   * @param oBold
   * @return 
   * S_OK : Method correctly executed. 
   * <br>No restriction.
   * <br>S_READONLY : Method correctly executed.
   * <br>Returned value cannot be modified, it's read-only.
   * <br><i>Can be usefull when the property is fixed by a standard, etc.</i>
   * <br>S_UNDEFINED : Method correctly executed.
   * <br>Returned value is undefined.
   * <br><i>Case of an object having several subparts supporting the same property with a different value.</i>
   * <br>E_FAIL : Method execution failed.
   * <br>Reasons of the failure are not given.
   * <br>E_NOTIMPL : No implementation available for this method.
   */
   virtual HRESULT GetBold( boolean *oBold ) = 0;

   /**
   * Sets the Bold style of the font.
   * @param iBold
   * @return 
   * S_OK : Method correctly executed. 
   * <br>No restriction.
   * <br>E_FAIL : Method execution failed.
   * <br>Reasons of the failure are not given.
   * <br>E_NOTIMPL : No implementation available for this method.
   */
   virtual HRESULT SetBold( const boolean iBold ) = 0;

   /**
   * Gets the Italic style of the font.
   * @param oItalic
   * @return 
   * S_OK : Method correctly executed. 
   * <br>No restriction.
   * <br>S_READONLY : Method correctly executed.
   * <br>Returned value cannot be modified, it's read-only.
   * <br><i>Can be usefull when the property is fixed by a standard, etc.</i>
   * <br>S_UNDEFINED : Method correctly executed.
   * <br>Returned value is undefined.
   * <br><i>Case of an object having several subparts supporting the same property with a different value.</i>
   * <br>E_FAIL : Method execution failed.
   * <br>Reasons of the failure are not given.
   * <br>E_NOTIMPL : No implementation available for this method.
   */
   virtual HRESULT GetItalic( boolean *oItalic ) = 0;

   /**
   * Sets the Italic style of the font.
   * @param iItalic
   * @return 
   * S_OK : Method correctly executed. 
   * <br>No restriction.
   * <br>E_FAIL : Method execution failed.
   * <br>Reasons of the failure are not given.
   * <br>E_NOTIMPL : No implementation available for this method.
   */
   virtual HRESULT SetItalic( const boolean iItalic ) = 0;

   /**
   * Gets the Underline style of the font.
   * <br><i>Underline and Overline styles are usualy mutualy exclusive.</i>
   * @param oUnderline
   * @return 
   * S_OK : Method correctly executed. 
   * <br>No restriction.
   * <br>S_READONLY : Method correctly executed.
   * <br>Returned value cannot be modified, it's read-only.
   * <br><i>Can be usefull when the property is fixed by a standard, etc.</i>
   * <br>S_UNDEFINED : Method correctly executed.
   * <br>Returned value is undefined.
   * <br><i>Case of an object having several subparts supporting the same property with a different value.</i>
   * <br>E_FAIL : Method execution failed.
   * <br>Reasons of the failure are not given.
   * <br>E_NOTIMPL : No implementation available for this method.
   */
   virtual HRESULT GetUnderline( boolean *oUnderline ) = 0;

   /**
   * Sets the Underline style of the font.
   * <br><i>Underline and Overline styles are usualy mutualy exclusive.</i>
   * @param iUnderline
   * @return 
   * S_OK : Method correctly executed. 
   * <br>No restriction.
   * <br>E_FAIL : Method execution failed.
   * <br>Reasons of the failure are not given.
   * <br>E_NOTIMPL : No implementation available for this method.
   */
   virtual HRESULT SetUnderline( const boolean iUnderline ) = 0;

   /**
   * Gets the Overline style of the font.
   * <br><i>Underline and Overline styles are usualy mutualy exclusive.</i>
   * @param oOverline
   * @return 
   * S_OK : Method correctly executed. 
   * <br>No restriction.
   * <br>S_READONLY : Method correctly executed.
   * <br>Returned value cannot be modified, it's read-only.
   * <br><i>Can be usefull when the property is fixed by a standard, etc.</i>
   * <br>S_UNDEFINED : Method correctly executed.
   * <br>Returned value is undefined.
   * <br><i>Case of an object having several subparts supporting the same property with a different value.</i>
   * <br>E_FAIL : Method execution failed.
   * <br>Reasons of the failure are not given.
   * <br>E_NOTIMPL : No implementation available for this method.
   */
   virtual HRESULT GetOverline( boolean *oOverline ) = 0;

   /**
   * Sets the Overline style of the font.
   * <br><i>Underline and Overline styles are usualy mutualy exclusive.</i>
   * @param iOverline
   * @return 
   * S_OK : Method correctly executed. 
   * <br>No restriction.
   * <br>E_FAIL : Method execution failed.
   * <br>Reasons of the failure are not given.
   * <br>E_NOTIMPL : No implementation available for this method.
   */
   virtual HRESULT SetOverline( const boolean iOverline ) = 0;

   /**
   * Gets the StrikeThru style of the font.
   * @param oStrikeThru
   * @return 
   * S_OK : Method correctly executed. 
   * <br>No restriction.
   * <br>S_READONLY : Method correctly executed.
   * <br>Returned value cannot be modified, it's read-only.
   * <br><i>Can be usefull when the property is fixed by a standard, etc.</i>
   * <br>S_UNDEFINED : Method correctly executed.
   * <br>Returned value is undefined.
   * <br><i>Case of an object having several subparts supporting the same property with a different value.</i>
   * <br>E_FAIL : Method execution failed.
   * <br>Reasons of the failure are not given.
   * <br>E_NOTIMPL : No implementation available for this method.
   */
   virtual HRESULT GetStrikeThru( boolean *oStrikeThru ) = 0;

   /**
   * Sets the StrikeThru style of the font.
   * @param iStrikeThru
   * @return 
   * S_OK : Method correctly executed. 
   * <br>No restriction.
   * <br>E_FAIL : Method execution failed.
   * <br>Reasons of the failure are not given.
   * <br>E_NOTIMPL : No implementation available for this method.
   */
   virtual HRESULT SetStrikeThru( const boolean iStrikeThru ) = 0;

   /**
   * Gets the Superscript style of the font.
   * <br><i>Superscript and Subscript styles are mutualy exclusive.</i>
   * @param oSuperscript
   * @return 
   * S_OK : Method correctly executed. 
   * <br>No restriction.
   * <br>S_READONLY : Method correctly executed.
   * <br>Returned value cannot be modified, it's read-only.
   * <br><i>Can be usefull when the property is fixed by a standard, etc.</i>
   * <br>S_UNDEFINED : Method correctly executed.
   * <br>Returned value is undefined.
   * <br><i>Case of an object having several subparts supporting the same property with a different value.</i>
   * <br>E_FAIL : Method execution failed.
   * <br>Reasons of the failure are not given.
   * <br>E_NOTIMPL : No implementation available for this method.
   */
   virtual HRESULT GetSuperscript( boolean *oSuperscript ) = 0;

   /**
   * Sets the Superscript style of the font.
   * <br><i>Superscript and Subscript styles are mutualy exclusive.</i>
   * @param iSuperscript
   * @return 
   * S_OK : Method correctly executed. 
   * <br>No restriction.
   * <br>E_FAIL : Method execution failed.
   * <br>Reasons of the failure are not given.
   * <br>E_NOTIMPL : No implementation available for this method.
   */
   virtual HRESULT SetSuperscript( const boolean iSuperscript ) = 0;

   /**
   * Gets the Subscript style of the font.
   * <br><i>Superscript and Subscript styles are mutualy exclusive.</i>
   * @param oSubscript
   * @return 
   * S_OK : Method correctly executed. 
   * <br>No restriction.
   * <br>S_READONLY : Method correctly executed.
   * <br>Returned value cannot be modified, it's read-only.
   * <br><i>Can be usefull when the property is fixed by a standard, etc.</i>
   * <br>S_UNDEFINED : Method correctly executed.
   * <br>Returned value is undefined.
   * <br><i>Case of an object having several subparts supporting the same property with a different value.</i>
   * <br>E_FAIL : Method execution failed.
   * <br>Reasons of the failure are not given.
   * <br>E_NOTIMPL : No implementation available for this method.
   */
   virtual HRESULT GetSubscript( boolean *oSubscript ) = 0;

   /**
   * Sets the Subscript style of the font.
   * <br><i>Superscript and Subscript styles are mutualy exclusive.</i>
   * @param iSubscript
   * @return 
   * S_OK : Method correctly executed. 
   * <br>No restriction.
   * <br>E_FAIL : Method execution failed.
   * <br>Reasons of the failure are not given.
   * <br>E_NOTIMPL : No implementation available for this method.
   */
   virtual HRESULT SetSubscript( const boolean iSubscript ) = 0;

   /**
   * Gets the Justification of the text inside of its box.
   * @param oJustification
   * @return 
   * S_OK : Method correctly executed. 
   * <br>No restriction.
   * <br>S_READONLY : Method correctly executed.
   * <br>Returned value cannot be modified, it's read-only.
   * <br><i>Can be usefull when the property is fixed by a standard, etc.</i>
   * <br>S_UNDEFINED : Method correctly executed.
   * <br>Returned value is undefined.
   * <br><i>Case of an object having several subparts supporting the same property with a different value.</i>
   * <br>E_FAIL : Method execution failed.
   * <br>Reasons of the failure are not given.
   * <br>E_NOTIMPL : No implementation available for this method.
   * @see DftJustification
   */
   virtual HRESULT GetJustification( DftJustification *oJustification ) = 0;

   /**
   * Sets the Justification of the text inside of its box.
   * @param iJustification
   * @return 
   * S_OK : Method correctly executed. 
   * <br>No restriction.
   * <br>E_FAIL : Method execution failed.
   * <br>Reasons of the failure are not given.
   * <br>E_NOTIMPL : No implementation available for this method.
   * @see DftJustification
   */
   virtual HRESULT SetJustification( const DftJustification iJustification ) = 0;

   /**
   * Gets the Kerning style of the font.
   * @param oKerning
   * @return 
   * S_OK : Method correctly executed. 
   * <br>No restriction.
   * <br>S_READONLY : Method correctly executed.
   * <br>Returned value cannot be modified, it's read-only.
   * <br><i>Can be usefull when the property is fixed by a standard, etc.</i>
   * <br>S_UNDEFINED : Method correctly executed.
   * <br>Returned value is undefined.
   * <br><i>Case of an object having several subparts supporting the same property with a different value.</i>
   * <br>E_FAIL : Method execution failed.
   * <br>Reasons of the failure are not given.
   * <br>E_NOTIMPL : No implementation available for this method.
   */
   virtual HRESULT GetKerning( boolean *oKerning ) = 0;

   /**
   * Sets the Kerning style of the font.
   * @param iKerning
   * @return 
   * S_OK : Method correctly executed. 
   * <br>No restriction.
   * <br>E_FAIL : Method execution failed.
   * <br>Reasons of the failure are not given.
   * <br>E_NOTIMPL : No implementation available for this method.
   */
   virtual HRESULT SetKerning( const boolean iKerning ) = 0;

   /**
   * Gets the AnchorPoint of the text box.
   * @param oAnchorPoint
   * @return 
   * S_OK : Method correctly executed. 
   * <br>No restriction.
   * <br>S_READONLY : Method correctly executed.
   * <br>Returned value cannot be modified, it's read-only.
   * <br><i>Can be usefull when the property is fixed by a standard, etc.</i>
   * <br>S_UNDEFINED : Method correctly executed.
   * <br>Returned value is undefined.
   * <br><i>Case of an object having several subparts supporting the same property with a different value.</i>
   * <br>E_FAIL : Method execution failed.
   * <br>Reasons of the failure are not given.
   * <br>E_NOTIMPL : No implementation available for this method.
   * @see DftAnchorPoint
   */
   virtual HRESULT GetAnchorPoint( DftAnchorPoint *oAnchorPoint ) = 0;

   /**
   * Sets the AnchorPoint of the text box.
   * @param iAnchorPoint
   * @return 
   * S_OK : Method correctly executed. 
   * <br>No restriction.
   * <br>E_FAIL : Method execution failed.
   * <br>Reasons of the failure are not given.
   * <br>E_NOTIMPL : No implementation available for this method.
   * @see DftAnchorPoint
   */
   virtual HRESULT SetAnchorPoint( const DftAnchorPoint iAnchorPoint ) = 0;

   /**
   * Gets the Type of the Frame surrounding the text.
   * @param oFrameType
   * @return 
   * S_OK : Method correctly executed. 
   * <br>No restriction.
   * <br>S_READONLY : Method correctly executed.
   * <br>Returned value cannot be modified, it's read-only.
   * <br><i>Can be usefull when the property is fixed by a standard, etc.</i>
   * <br>S_UNDEFINED : Method correctly executed.
   * <br>Returned value is undefined.
   * <br><i>Case of an object having several subparts supporting the same property with a different value.</i>
   * <br>E_FAIL : Method execution failed.
   * <br>Reasons of the failure are not given.
   * <br>E_NOTIMPL : No implementation available for this method.
   * @see DftFrameType
   */
   virtual HRESULT GetFrameType( DftFrameType *oFrameType ) = 0;

   /**
   * Sets the Type of the Frame surrounding the text.
   * @param iFrameType
   * @return 
   * S_OK : Method correctly executed. 
   * <br>No restriction.
   * <br>E_FAIL : Method execution failed.
   * <br>Reasons of the failure are not given.
   * <br>E_NOTIMPL : No implementation available for this method.
   * @see DftFrameType
   */
   virtual HRESULT SetFrameType( const DftFrameType iFrameType ) = 0;

   /**
   * @nodoc Not implemented yet
   * Gets the name of the frame surrounding the text.
   * @param oFrameName
   * @return 
   * S_OK : Method correctly executed. 
   * <br>No restriction.
   * <br>S_READONLY : Method correctly executed.
   * <br>Returned value cannot be modified, it's read-only.
   * <br><i>Can be usefull when the property is fixed by a standard, etc.</i>
   * <br>S_UNDEFINED : Method correctly executed.
   * <br>Returned value is undefined.
   * <br><i>Case of an object having several subparts supporting the same property with a different value.</i>
   * <br>E_FAIL : Method execution failed.
   * <br>Reasons of the failure are not given.
   * <br>E_NOTIMPL : No implementation available for this method.
   * @see DftFrameType
   */
   virtual HRESULT GetFrameName(CATUnicodeString &oFrameName) = 0;

   /**
   * @nodoc Not implemented yet
   * Sets the name of the frame surrounding the text.
   * @param iFrameName
   * @return 
   * S_OK : Method correctly executed. 
   * <br>No restriction.
   * <br>E_FAIL : Method execution failed.
   * <br>Reasons of the failure are not given.
   * <br>E_NOTIMPL : No implementation available for this method.
   * @see DftFrameType
   */
   virtual HRESULT SetFrameName(const CATUnicodeString &iFrameName) = 0;

   /**
   * Gets the Color of the text.
   * @param oColor
   * @return 
   * S_OK : Method correctly executed. 
   * <br>No restriction.
   * <br>S_READONLY : Method correctly executed.
   * <br>Returned value cannot be modified, it's read-only.
   * <br><i>Can be usefull when the property is fixed by a standard, etc.</i>
   * <br>S_UNDEFINED : Method correctly executed.
   * <br>Returned value is undefined.
   * <br><i>Case of an object having several subparts supporting the same property with a different value.</i>
   * <br>E_FAIL : Method execution failed.
   * <br>Reasons of the failure are not given.
   * <br>E_NOTIMPL : No implementation available for this method.
   */
   virtual HRESULT GetColor( unsigned int *oColor) = 0;

   /**
   * Sets the Color of the text.
   * @param iColor
   * @return 
   * S_OK : Method correctly executed. 
   * <br>No restriction.
   * <br>E_FAIL : Method execution failed.
   * <br>Reasons of the failure are not given.
   * <br>E_NOTIMPL : No implementation available for this method.
   */
   virtual HRESULT SetColor(const unsigned int iColor) = 0;

   /**
   * Gets the engineering symbol attached to the text.
   * @param oSymbol
   * @return 
   * S_OK : Method correctly executed. 
   * <br>No restriction.
   * <br>S_READONLY : Method correctly executed.
   * <br>Returned value cannot be modified, it's read-only.
   * <br><i>Can be usefull when the property is fixed by a standard, etc.</i>
   * <br>S_UNDEFINED : Method correctly executed.
   * <br>Returned value is undefined.
   * <br><i>Case of an object having several subparts supporting the same property with a different value.</i>
   * <br>E_FAIL : Method execution failed.
   * <br>Reasons of the failure are not given.
   * <br>E_NOTIMPL : No implementation available for this method.
   * @see DftFrameType
   */
   virtual HRESULT GetSymbol( DftGDTSymbol *oSymbol ) = 0;

   /**
   * Sets the engineering symbol attached to the text.
   * @param iSymbol
   * @return 
   * S_OK : Method correctly executed. 
   * <br>No restriction.
   * <br>E_FAIL : Method execution failed.
   * <br>Reasons of the failure are not given.
   * <br>E_NOTIMPL : No implementation available for this method.
   * @see DftFrameType
   */
   virtual HRESULT SetSymbol( const DftGDTSymbol iSymbol ) = 0;

   /**
   * Gets the FontRatio value of the text.
   * @param oFontRatio
   *    The font ratio (width/height) value of the text.
   * @return An HRESULT value.
   *    <br><b>Legal values</b>:
   *    <dl>
   *    <dt>S_OK
   *    <dd>Method correctly executed. 
   *    <dd>No restriction.
   *    <dt>S_READONLY
   *    <dd>Method correctly executed.
   *    <dd>Returned value cannot be modified, it's read-only.
   *    <dd><i>Can be usefull when the property is fixed by a standard, etc.</i>
   *    <dt>S_UNDEFINED
   *    <dd>Method correctly executed.
   *    <dd>Returned value is undefined.
   *    <dd><i>Case of an object having several subparts supporting the same property with a different value.</i>
   *    <dt>E_FAIL
   *    <dd>Method execution failed.
   *    <dd>Reasons of the failure are not given.
   *    <dt>E_IMPL
   *    <dd>No implementation available for this method.
   *    </dl>
   */
   virtual HRESULT GetFontRatio( double *oFontRatio ) = 0;

   /**
   * Sets the FontRatio value of the text.
   * @param iFontRatio
   *    The font ratio (width/height) value of the text.
   * @return An HRESULT value.
   *    <br><b>Legal values</b>:
   *    <dl>
   *    <dt>S_OK
   *    <dd>Method correctly executed. 
   *    <dd>No restriction.
   *    <dt>S_READONLY
   *    <dd>Method correctly executed.
   *    <dd>Returned value cannot be modified, it's read-only.
   *    <dd><i>Can be usefull when the property is fixed by a standard, etc.</i>
   *    <dt>S_UNDEFINED
   *    <dd>Method correctly executed.
   *    <dd>Returned value is undefined.
   *    <dd><i>Case of an object having several subparts supporting the same property with a different value.</i>
   *    <dt>E_FAIL
   *    <dd>Method execution failed.
   *    <dd>Reasons of the failure are not given.
   *    <dt>E_IMPL
   *    <dd>No implementation available for this method.
   *    </dl>
   */
   virtual HRESULT SetFontRatio( const double iFontRatio ) = 0;

   /**
   * Gets the character spacing of the font.
   * @param oCharSpacing
   *    The space between two character.
   * @return An HRESULT value.
   *    <br><b>Legal values</b>:
   *    <dl>
   *    <dt>S_OK
   *    <dd>Method correctly executed. 
   *    <dd>No restriction.
   *    <dt>S_READONLY
   *    <dd>Method correctly executed.
   *    <dd>Returned value cannot be modified, it's read-only.
   *    <dd><i>Can be usefull when the property is fixed by a standard, etc.</i>
   *    <dt>S_UNDEFINED
   *    <dd>Method correctly executed.
   *    <dd>Returned value is undefined.
   *    <dd><i>Case of an object having several subparts supporting the same property with a different value.</i>
   *    <dt>E_FAIL
   *    <dd>Method execution failed.
   *    <dd>Reasons of the failure are not given.
   *    <dt>E_IMPL
   *    <dd>No implementation available for this method.
   *    </dl>
   */
   virtual HRESULT GetCharacterSpacing( double *oCharSpacing ) = 0;

   /**
   * Sets the character spacing of the font.
   * @param iCharSpacing
   *    The space between two character.
   * @return An HRESULT value.
   *    <br><b>Legal values</b>:
   *    <dl>
   *    <dt>S_OK
   *    <dd>Method correctly executed. 
   *    <dd>No restriction.
   *    <dt>S_READONLY
   *    <dd>Method correctly executed.
   *    <dd>Returned value cannot be modified, it's read-only.
   *    <dd><i>Can be usefull when the property is fixed by a standard, etc.</i>
   *    <dt>S_UNDEFINED
   *    <dd>Method correctly executed.
   *    <dd>Returned value is undefined.
   *    <dd><i>Case of an object having several subparts supporting the same property with a different value.</i>
   *    <dt>E_FAIL
   *    <dd>Method execution failed.
   *    <dd>Reasons of the failure are not given.
   *    <dt>E_IMPL
   *    <dd>No implementation available for this method.
   *    </dl>
   */
   virtual HRESULT SetCharacterSpacing( const double iCharSpacing ) = 0;
};

#endif
