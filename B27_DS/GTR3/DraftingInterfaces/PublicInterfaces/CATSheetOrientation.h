#ifndef CATSheetOrientation_H
#define CATSheetOrientation_H

// COPYRIGHT Dassault Systemes 2003
//===================================================================
//
// CATSheetOrientation.h
//
//===================================================================

/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */

/**
 * Sheet orientation:
 *
 *   - SheetPortrait: portrait orientation.
 *
 *   - SheetLandscape: landscape orientation.
 */
enum CATSheetOrientation { SheetPortrait, SheetLandscape };

#endif
