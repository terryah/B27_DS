#ifndef CatSheetGenViewsPosMode_IDL
#define CatSheetGenViewsPosMode_IDL
/*IDLREP*/

// COPYRIGHT DASSAULT SYSTEMES 2003

/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */

/**
 * Positioning mode values for generative views inside the sheet.
 * The positioning mode for a given sheet determines the generative views position behavior when an update is requested.
 * Notice: 
 * the position of generative views is usually the position of the image of the center of gravity of the 3D data.
 * <dl>
 * <dt> <tt>catFixedCG</tt>     <dd> the position of the image of the center of gravity of the 3D data remains the same after an update.
 * <dt> <tt>catFixedAxis</tt>   <dd> the position of the image of the center of gravity is modified so that existing geometries don't move after an update.
 * </dl>
 */
enum CatSheetGenViewsPosMode { catFixedCG, catFixedAxis };

#endif

