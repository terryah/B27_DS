/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIA2DPrintArea_h
#define CATIA2DPrintArea_h

#include "CATCORBABoolean.h"
#include "CAT_VARIANT_BOOL.h"

#ifndef ExportedByDraftingPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __DraftingPubIDL
#define ExportedByDraftingPubIDL __declspec(dllexport)
#else
#define ExportedByDraftingPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByDraftingPubIDL
#endif
#endif

#include "CATIABase.h"

extern ExportedByDraftingPubIDL IID IID_CATIA2DPrintArea;

class ExportedByDraftingPubIDL CATIA2DPrintArea : public CATIABase
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall SetArea(double iX, double iY, double iWidth, double iHeigth)=0;

    virtual HRESULT __stdcall GetArea(double & oX, double & oY, double & oWidth, double & oHeigth, CAT_VARIANT_BOOL & oActivated)=0;

    virtual HRESULT __stdcall put_AreaLowX(double iX)=0;

    virtual HRESULT __stdcall get_AreaLowX(double & oX)=0;

    virtual HRESULT __stdcall put_AreaLowY(double iY)=0;

    virtual HRESULT __stdcall get_AreaLowY(double & oY)=0;

    virtual HRESULT __stdcall put_AreaWidth(double iWidth)=0;

    virtual HRESULT __stdcall get_AreaWidth(double & oWidth)=0;

    virtual HRESULT __stdcall put_AreaHeigth(double iHeigth)=0;

    virtual HRESULT __stdcall get_AreaHeigth(double & oHeigth)=0;

    virtual HRESULT __stdcall put_ActivationState(CAT_VARIANT_BOOL iActivated)=0;

    virtual HRESULT __stdcall get_ActivationState(CAT_VARIANT_BOOL & oActivated)=0;


};

CATDeclareHandler(CATIA2DPrintArea, CATIABase);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
