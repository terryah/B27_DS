#ifndef __TIE_CATIADrawingTextTemporary
#define __TIE_CATIADrawingTextTemporary

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATIADrawingTextTemporary.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface CATIADrawingTextTemporary */
#define declare_TIE_CATIADrawingTextTemporary(classe) \
 \
 \
class TIECATIADrawingTextTemporary##classe : public CATIADrawingTextTemporary \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(CATIADrawingTextTemporary, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall get_NbLink(CATLONG & oNbLink); \
      virtual HRESULT __stdcall GetParameterLink(CATLONG iIndex, CATBaseDispatch *& oPointedParm); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_CATIADrawingTextTemporary(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall get_NbLink(CATLONG & oNbLink); \
virtual HRESULT __stdcall GetParameterLink(CATLONG iIndex, CATBaseDispatch *& oPointedParm); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_CATIADrawingTextTemporary(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::get_NbLink(CATLONG & oNbLink) \
{ \
return (ENVTIECALL(CATIADrawingTextTemporary,ENVTIETypeLetter,ENVTIELetter)get_NbLink(oNbLink)); \
} \
HRESULT __stdcall  ENVTIEName::GetParameterLink(CATLONG iIndex, CATBaseDispatch *& oPointedParm) \
{ \
return (ENVTIECALL(CATIADrawingTextTemporary,ENVTIETypeLetter,ENVTIELetter)GetParameterLink(iIndex,oPointedParm)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(CATIADrawingTextTemporary,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(CATIADrawingTextTemporary,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(CATIADrawingTextTemporary,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(CATIADrawingTextTemporary,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(CATIADrawingTextTemporary,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_CATIADrawingTextTemporary(classe)    TIECATIADrawingTextTemporary##classe


/* Common methods inside a TIE */
#define common_TIE_CATIADrawingTextTemporary(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(CATIADrawingTextTemporary, classe) \
 \
 \
CATImplementTIEMethods(CATIADrawingTextTemporary, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(CATIADrawingTextTemporary, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(CATIADrawingTextTemporary, classe) \
CATImplementCATBaseUnknownMethodsForTIE(CATIADrawingTextTemporary, classe) \
 \
HRESULT __stdcall  TIECATIADrawingTextTemporary##classe::get_NbLink(CATLONG & oNbLink) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_NbLink(oNbLink)); \
} \
HRESULT __stdcall  TIECATIADrawingTextTemporary##classe::GetParameterLink(CATLONG iIndex, CATBaseDispatch *& oPointedParm) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetParameterLink(iIndex,oPointedParm)); \
} \
HRESULT  __stdcall  TIECATIADrawingTextTemporary##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication)); \
} \
HRESULT  __stdcall  TIECATIADrawingTextTemporary##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent)); \
} \
HRESULT  __stdcall  TIECATIADrawingTextTemporary##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  TIECATIADrawingTextTemporary##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  TIECATIADrawingTextTemporary##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj)); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_CATIADrawingTextTemporary(classe) \
 \
 \
declare_TIE_CATIADrawingTextTemporary(classe) \
 \
 \
CATMetaClass * __stdcall TIECATIADrawingTextTemporary##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_CATIADrawingTextTemporary,"CATIADrawingTextTemporary",CATIADrawingTextTemporary::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_CATIADrawingTextTemporary(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(CATIADrawingTextTemporary, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicCATIADrawingTextTemporary##classe(classe::MetaObject(),CATIADrawingTextTemporary::MetaObject(),(void *)CreateTIECATIADrawingTextTemporary##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_CATIADrawingTextTemporary(classe) \
 \
 \
declare_TIE_CATIADrawingTextTemporary(classe) \
 \
 \
CATMetaClass * __stdcall TIECATIADrawingTextTemporary##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_CATIADrawingTextTemporary,"CATIADrawingTextTemporary",CATIADrawingTextTemporary::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_CATIADrawingTextTemporary(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(CATIADrawingTextTemporary, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicCATIADrawingTextTemporary##classe(classe::MetaObject(),CATIADrawingTextTemporary::MetaObject(),(void *)CreateTIECATIADrawingTextTemporary##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_CATIADrawingTextTemporary(classe) TIE_CATIADrawingTextTemporary(classe)
#else
#define BOA_CATIADrawingTextTemporary(classe) CATImplementBOA(CATIADrawingTextTemporary, classe)
#endif

#endif
