/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CatSymbolParameters_h
#define CatSymbolParameters_h

enum CatSymbolType {
        catNotUsed,
        catCross,
        catPlus,
        catConcentric,
        catCoincident,
        catFullCircle,
        catFullSquare,
        catStar,
        catDot,
        catSmallDot,
        catMisc1,
        catMisc2,
        catFullCircle2,
        catFullSquare2,
        catOpenArrow,
        catUnfilledArrow,
        catBlankedArrow,
        catFilledArrow,
        catUnfilledCircle,
        catBlankedCircle,
        catFilledCircle,
        catCrossedCircle,
        catBlankedSquare,
        catFilledSquare,
        catBlankedTriangle,
        catFilledTriangle,
        catManipulatorSquare,
        catMamipulatorDiamond,
        catManipulatorCircle,
        catManipulatorTriangle,
        catDoubleOpenArrow,
        catWave
};

#endif
