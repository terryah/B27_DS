/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIADrawingArrows_h
#define CATIADrawingArrows_h

#ifndef ExportedByDraftingPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __DraftingPubIDL
#define ExportedByDraftingPubIDL __declspec(dllexport)
#else
#define ExportedByDraftingPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByDraftingPubIDL
#endif
#endif

#include "CATIACollection.h"

class CATIADrawingArrow;

extern ExportedByDraftingPubIDL IID IID_CATIADrawingArrows;

class ExportedByDraftingPubIDL CATIADrawingArrows : public CATIACollection
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall Add(double iHeadPointX, double iHeadPointY, double iTailPointX, double iTailPointY, CATIADrawingArrow *& oNewDrawingArrow)=0;

    virtual HRESULT __stdcall Remove(CATLONG iIndex)=0;

    virtual HRESULT __stdcall Item(CATLONG iIndex, CATIADrawingArrow *& oItem)=0;


};

CATDeclareHandler(CATIADrawingArrows, CATIACollection);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIABase.h"
#include "CATIADrawingArrow.h"
#include "CATSafeArray.h"
#include "CatSymbolParameters.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
