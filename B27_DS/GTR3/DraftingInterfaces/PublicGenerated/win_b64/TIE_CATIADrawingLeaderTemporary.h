#ifndef __TIE_CATIADrawingLeaderTemporary
#define __TIE_CATIADrawingLeaderTemporary

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATIADrawingLeaderTemporary.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface CATIADrawingLeaderTemporary */
#define declare_TIE_CATIADrawingLeaderTemporary(classe) \
 \
 \
class TIECATIADrawingLeaderTemporary##classe : public CATIADrawingLeaderTemporary \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(CATIADrawingLeaderTemporary, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall get_AnchorSymbol(CATLONG & oAnchorSymbol); \
      virtual HRESULT __stdcall put_AnchorSymbol(CATLONG iAnchorSymbol); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_CATIADrawingLeaderTemporary(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall get_AnchorSymbol(CATLONG & oAnchorSymbol); \
virtual HRESULT __stdcall put_AnchorSymbol(CATLONG iAnchorSymbol); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_CATIADrawingLeaderTemporary(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::get_AnchorSymbol(CATLONG & oAnchorSymbol) \
{ \
return (ENVTIECALL(CATIADrawingLeaderTemporary,ENVTIETypeLetter,ENVTIELetter)get_AnchorSymbol(oAnchorSymbol)); \
} \
HRESULT __stdcall  ENVTIEName::put_AnchorSymbol(CATLONG iAnchorSymbol) \
{ \
return (ENVTIECALL(CATIADrawingLeaderTemporary,ENVTIETypeLetter,ENVTIELetter)put_AnchorSymbol(iAnchorSymbol)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(CATIADrawingLeaderTemporary,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(CATIADrawingLeaderTemporary,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(CATIADrawingLeaderTemporary,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(CATIADrawingLeaderTemporary,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(CATIADrawingLeaderTemporary,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_CATIADrawingLeaderTemporary(classe)    TIECATIADrawingLeaderTemporary##classe


/* Common methods inside a TIE */
#define common_TIE_CATIADrawingLeaderTemporary(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(CATIADrawingLeaderTemporary, classe) \
 \
 \
CATImplementTIEMethods(CATIADrawingLeaderTemporary, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(CATIADrawingLeaderTemporary, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(CATIADrawingLeaderTemporary, classe) \
CATImplementCATBaseUnknownMethodsForTIE(CATIADrawingLeaderTemporary, classe) \
 \
HRESULT __stdcall  TIECATIADrawingLeaderTemporary##classe::get_AnchorSymbol(CATLONG & oAnchorSymbol) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_AnchorSymbol(oAnchorSymbol)); \
} \
HRESULT __stdcall  TIECATIADrawingLeaderTemporary##classe::put_AnchorSymbol(CATLONG iAnchorSymbol) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_AnchorSymbol(iAnchorSymbol)); \
} \
HRESULT  __stdcall  TIECATIADrawingLeaderTemporary##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication)); \
} \
HRESULT  __stdcall  TIECATIADrawingLeaderTemporary##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent)); \
} \
HRESULT  __stdcall  TIECATIADrawingLeaderTemporary##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  TIECATIADrawingLeaderTemporary##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  TIECATIADrawingLeaderTemporary##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj)); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_CATIADrawingLeaderTemporary(classe) \
 \
 \
declare_TIE_CATIADrawingLeaderTemporary(classe) \
 \
 \
CATMetaClass * __stdcall TIECATIADrawingLeaderTemporary##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_CATIADrawingLeaderTemporary,"CATIADrawingLeaderTemporary",CATIADrawingLeaderTemporary::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_CATIADrawingLeaderTemporary(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(CATIADrawingLeaderTemporary, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicCATIADrawingLeaderTemporary##classe(classe::MetaObject(),CATIADrawingLeaderTemporary::MetaObject(),(void *)CreateTIECATIADrawingLeaderTemporary##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_CATIADrawingLeaderTemporary(classe) \
 \
 \
declare_TIE_CATIADrawingLeaderTemporary(classe) \
 \
 \
CATMetaClass * __stdcall TIECATIADrawingLeaderTemporary##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_CATIADrawingLeaderTemporary,"CATIADrawingLeaderTemporary",CATIADrawingLeaderTemporary::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_CATIADrawingLeaderTemporary(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(CATIADrawingLeaderTemporary, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicCATIADrawingLeaderTemporary##classe(classe::MetaObject(),CATIADrawingLeaderTemporary::MetaObject(),(void *)CreateTIECATIADrawingLeaderTemporary##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_CATIADrawingLeaderTemporary(classe) TIE_CATIADrawingLeaderTemporary(classe)
#else
#define BOA_CATIADrawingLeaderTemporary(classe) CATImplementBOA(CATIADrawingLeaderTemporary, classe)
#endif

#endif
