// COPYRIGHT Dassault Systemes 2004
//=============================================================================
//
// CATICustoIconProduct.h
// Define the CATICustoIconProduct interface
//
//=============================================================================
//
// Usage notes:
//   Customization of product structure tree icons
//
//=============================================================================
//
//  Apr 2004  Creation: Code generated by the CAA wizard  lcf
//=============================================================================

#ifndef CATICustoIconProduct_H
#define CATICustoIconProduct_H

/**
* @CAA2Level L1
* @CAA2Usage U5
*/

#include "CATProductStructureInterfaces.h"
#include "CATBaseUnknown.h"
#include "CATUnicodeString.h"


#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByCATProductStructureInterfaces IID IID_CATICustoIconProduct;
#else
extern "C" const IID IID_CATICustoIconProduct ;
#endif

//------------------------------------------------------------------

/**
 * Interface to provide icons for the product structure tree.
 * <br><b>Role:</b> By implementing this interface on a feature extension of Product
 * its own icons to be displayed in the product structure tree. It can also
 * indicates whether various information masks can be overlaid on the icons or not.
 */
class ExportedByCATProductStructureInterfaces CATICustoIconProduct: public CATBaseUnknown
{
	CATDeclareInterface;
	
public:
	/**
	 * Returns the icon to be displayed for a product.
	 * <br><b>Role:</b> The icon returned will be used to display an
	 * external product in a product structure tree.
	 *   @param oIcon
	 *      The name of the icon that should be used for a product
	 *   @return
	 *      E_FAIL  internal error
	 *	S_FALSE no icon provided, use the default
	 *      S_OK	use the returned icon
	 */
	virtual HRESULT GetIconForProduct( CATUnicodeString &  oIcon ) = 0;

	/**
	 * Returns the icon to be displayed for a component.
	 * <br><b>Role:</b> The icon returned will be used to display an
	 * internal component in a product structure tree.
	 *   @param oIcon
	 *      The name of the icon that should be used for a component
	 *   @return
	 *      E_FAIL  internal error
	 *	S_FALSE no icon provided, use the default
	 *      S_OK	use the returned icon
	 */
	virtual HRESULT GetIconForComponent( CATUnicodeString &  oIcon ) = 0;

	/**
	 * Indicates whether document information mask can be added to the icon.
	 * <br><b>Role:</b> When an icon is provided, this method is called to
	 * indicate whether document information mask can be added to the icon
	 * on its top right corner.
	 *   @return
	 *      E_FAIL  internal error
	 *	S_FALSE no mask should be added
	 *	S_OK    document information mask can be added
	 */
	virtual HRESULT IsPossibleToAddDocMask() = 0;

	/**
	 * Indicates whether representation information mask can be added to the icon.
	 * <br><b>Role:</b> When an icon is provided, this method is called to
	 * indicate whether representation information mask can be added to the
	 * icon on its bottom right corner.
	 *   @return
	 *      E_FAIL  internal error
	 *      S_FALSE no mask should be added
	 *      S_OK    representation information mask can be added
	 */
	virtual HRESULT IsPossibleToAddRepMask() = 0;

	/**
	 * Indicates whether central information mask can be added to the icon.
	 * <br><b>Role:</b> When an icon is provided, this method is called to
	 * indicate whether central information mask can be added to the icon.
	 *   @return
	 *      E_FAIL  internal error
	 *      S_FALSE no mask should be added
	 *	S_OK	central information mask can be added
	 */
	virtual HRESULT IsPossibleToAddCentralMask() = 0;

	/**
	 * Indicates whether gears mask can be added to the icon.
	 * <br><b>Role:</b> When an icon is provided, this method is called to
	 * indicate whether gears mask can be added to the icon its bottom left
	 * corner.
	 *   @return
	 *      E_FAIL  internal error
	 *      S_FALSE no mask should be added
	 *      S_OK    gears mask can be added
	 */
	virtual HRESULT IsPossibleToAddGearsMask() = 0;

	/**
	 * Indicates whether contextuality information mask can be added to the icon.
	 * <br><b>Role:</b> When an icon is provided without a gears mask,
	 * this method is called to indicate whether contextuality information
	 * mask can be added to the icon.
	 *   @return
	 *      E_FAIL  internal error
	 *      S_FALSE no mask should be added
	 *	S_OK	contextuality information mask can be added
	 */
	virtual HRESULT IsPossibleToAddContextDesignMask() = 0;

	/**
	 * Indicates whether flexible design information mask can be added to the icon.
	 * <br><b>Role:</b> When an icon is provided without a gears mask,
	 * this method is called to indicate whether flexible design information
	 * mask can be added to the icon.
	 *   @return
	 *      E_FAIL  internal error
	 *      S_FALSE no mask should be added
	 *	S_OK	flexible design information mask can be added
	 */
	virtual HRESULT IsPossibleToAddFlexibleDesignMask() = 0;

	
	// No constructor or destructor on this pure virtual base class
	// --------------------------------------------------------------
};

//------------------------------------------------------------------

#endif
