// COPYRIGHT Dassault Systemes 2002
//===================================================================
//
// CATIMfgWarmStartMgt.h
// Define the CATIMfgWarmStartMgt interface
//
//===================================================================
//
// Usage notes:
//   New interface: describe its use here
//
//===================================================================
//
//  May 2002  Creation: Code generated by the CAA wizard  VDX
//===================================================================
#ifndef CATIMfgWarmStartMgt_H
#define CATIMfgWarmStartMgt_H

/**
 * @CAA2Level L0
 * @CAA2Usage U3
*/

#include "CATMfgSimulationItfCPP.h"
// System
#include "CATBaseUnknown.h"

// ObjectSpecsModeler
#include "CATISpecObject.h"

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByCATMfgSimulationItfCPP IID IID_CATIMfgWarmStartMgt;
#else
extern "C" const IID IID_CATIMfgWarmStartMgt ;
#endif

//------------------------------------------------------------------

/**
 * Describe the functionality of your interface here
 * <p>
 * Using this prefered syntax will enable mkdoc to document your class.
 */
class ExportedByCATMfgSimulationItfCPP CATIMfgWarmStartMgt: public CATBaseUnknown
{
  CATDeclareInterface;

  public:

    /**
     * Remember that interfaces define only virtual pure methods.
     * Dont forget to document your methods.
     * <dl>
     * <dt><b>Example:</b>
     * <pre>
     *       
           *   MyFunction does this and that
           *   @param Arg1
           *      The first argument of MyFunction.
           *   @return
           *      Error code of function.
           * 
     * virtual int MyFunction (int Arg1) = 0;
     * </pre>
     * </dl>
     */
	virtual HRESULT SetWarmStartData(const CATISpecObject_var &ispSpecObjectData) = 0;

	/*
	 * Remember that interfaces define only virtual pure methods.
     * Dont forget to document your methods.
     * <dl>
     * <dt><b>Example:</b>
     * <pre>
     *       
           *   MyFunction does this and that
           *   @param Arg1
           *      The first argument of MyFunction.
           *   @return
           *      Error code of function.
           * 
     * virtual int MyFunction (int Arg1) = 0;
     * </pre>
     * </dl>
     */
	virtual HRESULT GetWarmStartData(CATISpecObject_var & ospSpecObjectData) = 0;


	/*
	  * Remember that interfaces define only virtual pure methods.
     * Dont forget to document your methods.
     * <dl>
     * <dt><b>Example:</b>
     * <pre>
     *       
           *   MyFunction does this and that
           *   @param Arg1
           *      The first argument of MyFunction.
           *   @return
           *      Error code of function.
           * 
     * virtual int MyFunction (int Arg1) = 0;
     * </pre>
     * </dl>
     */
	virtual HRESULT FlushWarmStartData() = 0;



  // No constructors or destructors on this pure virtual base class
  // --------------------------------------------------------------
};

CATDeclareHandler( CATIMfgWarmStartMgt, CATBaseUnknown) ;
//------------------------------------------------------------------

#endif
