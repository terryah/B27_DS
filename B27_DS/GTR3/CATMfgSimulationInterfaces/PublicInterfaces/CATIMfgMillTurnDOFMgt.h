// COPYRIGHT Dassault Systemes 2008
//===================================================================
//
// CATIMfgMillTurnDOFMgt.h
// Define the CATIMfgMillTurnDOFMgt interface
//
//===================================================================
//
// Usage notes:
//   New interface: describe its use here
//
//===================================================================
//
//  Jan 2008  Creation: Code generated by the CAA wizard  USH
//===================================================================
#ifndef CATIMfgMillTurnDOFMgt_H
#define CATIMfgMillTurnDOFMgt_H

/**
 * @CAA2Level L0
 * @CAA2Usage U3
*/

// System
#include "CATBaseUnknown.h"
#include "CATListOfDouble.h"

// Mathematics 
#include "CATMathTransformation.h"

// ManufacturingInterfaces
#include "CATIMfgTPSynchro.h"

// CATMfgSimulationInterfaces
#include "CATMfgSimulationItfCPP.h"


#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByCATMfgSimulationItfCPP IID IID_CATIMfgMillTurnDOFMgt;
#else
extern "C" const IID IID_CATIMfgMillTurnDOFMgt;
#endif

//------------------------------------------------------------------

/**
 * Describe the functionality of your interface here
 * <p>
 * Using this prefered syntax will enable mkdoc to document your class.
 */
class ExportedByCATMfgSimulationItfCPP CATIMfgMillTurnDOFMgt: public CATBaseUnknown
{
  CATDeclareInterface;

  public:


	  enum MfgTimingMode {Duration=1, Velocity=2 };

	  enum MfgInsertionMode {Default=0, Modified=1, New=2, Removed=3 };

	  enum MfgEditionMode {NO=0, YES=1};

	  enum MfgIndexingMode {ToolPath=0, ISOData=1};

	  enum MfgFeedMode {Value=0, RAPID=1};



	/**
     * Returns the size of DOF table
	 * @param osize 
	 * size of DOF table
     */
	virtual HRESULT GetSize(int &osize) = 0;

	/**
     * Method to add DOF target in the DOF table
	 * @param ipDOF
	 * concatenated list of DOF values 
	 * @param CATIMfgMillTurnDOFMgt::MfgTimingMode 
	 * timing mode for the target 
	 * @param iValue
	 * list of 
	 * @param iIndex
	 * List of ISO indexes
	 * @param iMainID
	 * List of MainPanel ID
	 * @param iSubID
	 * List of SubProgramPanelID
	 * @param iSubIndex
	 * List of subISOpanelIndex
     */
	virtual HRESULT AddDOFTarget (CATListOfDouble * &ipDOF, CATIMfgMillTurnDOFMgt::MfgTimingMode & iMode,CATListOfDouble * &iValue,
		                          CATListOfInt * &iIndex,CATListOfInt * &iMainID,CATListOfInt * &iSubID,CATListOfInt * &iSubIndex) = 0;


	/**
     * Overloaded Method to add DOF target in the DOF table
	 * @param ipDOF
	 * concatenated list of DOF values 
	 * @param CATIMfgMillTurnDOFMgt::MfgTimingMode 
	 * timing mode for the target 
	 * @param iFeedMode
	 * list of values of feed type
	 * @param iValue
	 * list of values of feed rate
	 * @param iIndex
	 * List of ISO indexes
	 * @param iMainID
	 * List of MainPanel ID
	 * @param iSubID
	 * List of SubProgramPanelID
	 * @param iSubIndex
	 * List of subISOpanelIndex
     */
	virtual HRESULT AddDOFTarget (CATListOfDouble *&                ipDOF, 
	                              CATIMfgMillTurnDOFMgt::MfgTimingMode iTimingMode,
		                          CATListOfInt *&					iFeedMode,  //CATIMfgMillTurnDOFMgt::MfgFeedMode   iFeedMode,  
		                          double                            iValue,
								  CATListOfInt *&                   iIndex,
								  CATListOfInt *&                   iMainID,
								  CATListOfInt *&                   iSubID,
								  CATListOfInt *&                   iSubIndex) = 0;

	/**
     * overloaded Method to add dof target
     */
	virtual HRESULT AddDOFTarget () = 0;


	/**
     * method to remove DOF target
	 * @param iRank
	 * DOF rank to delete
     */
	virtual HRESULT RemoveDOFTarget (int &iRank) = 0;


	/**
     * method to set dof values on current DOF rank
	 * @ param ipDOF
	 * dof values to set
     */
	virtual HRESULT SetDOFDisplacementOnCurrentDOFTarget (CATListOfDouble *&ipDOF) = 0;


	/**
     * method to set DOF values on a particular Rank
	 * @ param iRank
	 * DOf Rank on which dofvalues to set
	 * @ param ipDOF
	 * dof values to set
     */
	virtual HRESULT SetDOFDisplacementDOFTarget (int &iRank, CATListOfDouble *&ipDOF) = 0;


	/**
     * method to get DOF values on a particular Rank
	 * @ param iRank
	 * DOf Rank on which dofvalues to get
	 * @ param ipDOF
	 * dof values on that rank
     */
	virtual HRESULT GetDOFDisplacementDOFTarget (int &iRank, CATListOfDouble *&opDOF) = 0;


	/**
     * method to get Timing mode on a particular Rank
	 * @ param iRank
	 * DOf Rank on which Timing mode to get
	 * @ parm oMode
	 * timing mode set on that DOF Rank
     */
	virtual HRESULT GetMfgTimingModeDOFTarget (int &iRank, CATIMfgMillTurnDOFMgt::MfgTimingMode &oMode) = 0;

	/**
     ** method to get Feed mode on a particular Rank
	 * @ param iRank
	 * DOf Rank on which Feed mode to get
	 * @ parm oMode
	 * feed mode set on that DOF Rank
     */
	virtual HRESULT GetMfgFeedModeDOFTarget (int            iRank,
		                                     CATListOfInt *&oMode) = 0; //CATIMfgMillTurnDOFMgt::MfgFeedMode &oMode) = 0;

	/**
     ** method to set Feed mode on a particular Rank
	 * @ param iRank
	 * DOf Rank on which Feed mode to set
	 * @ parm oMode
	 * feed mode set on that DOF Rank
     */

	virtual HRESULT SetMfgFeedModeDOFTarget (int           iRank,
		                                     CATListOfInt *&oMode) = 0; //CATIMfgMillTurnDOFMgt::MfgFeedMode iMode) = 0;
	/**
     ** method to set velocity on current DOF Rank
	 * @ parm iVelocity
	 * list of velocities to set on that DOF Rank
     */
	virtual HRESULT SetVelocityOnCurrentDOFTarget (CATListOfDouble *& iVelocity) = 0;

	/**
     ** method to get velocity on particular DOF Rank
	 * @ param iRank
	 * DOf Rank on which velocity to retrieve
	 * @ parm oVelocity
	 * list of velocities set on that DOF Rank
     */

	virtual HRESULT GetVelocityDOFTarget (int &iRank, CATListOfDouble *& oVelocity) = 0;

	/**
     ** method to set duration on current DOF Rank
	 * @ parm iDuration
	 * list of durations to set on that DOF Rank
     */
	virtual HRESULT SetDurationOnCurrentDOFTarget (CATListOfDouble *& iDuration) = 0;
	/**
     ** method to get durations on particular DOF Rank
	 * @ param iRank
	 * DOf Rank on which durations to retrieve
	 * @ parm oDuration
	 * list of durations set on that DOF Rank
     */
	virtual HRESULT GetDurationDOFTarget (int &iRank, CATListOfDouble *& oDuration) = 0;
	/**
	  * method to sset index, mainprogram ID, subProgramID and subISOIndex on current DOF rank
	  * @ param iIndex index of main ISO program
	  * @ param iMainID id of main program 
	  * @ param iSubID id of subProgam
	  * @ param iSubIndex index of sub ISO program 
     */
	virtual HRESULT SetIndexOnCurrentDOFTarget  (CATListOfInt *& iIndex,CATListOfInt *& iMainID,CATListOfInt *& iSubID,CATListOfInt *& iSubIndex) = 0;
	/**
     * returns list of main ISO Program index for any particular DOF rank
	 * @  param iRank
	 * Rank on which ISO indexes to retrieve
	 @ param oIndex
	 * List of ISO index for that particular rank
     */
	virtual HRESULT GetIndexDOFTarget (const int & iRank, CATListOfInt *& oIndex) = 0;
	/**
     * sets list of main ISO Program index for any particular DOF rank
	 * @  param iRank
	 * Rank on which ISO indexes to set
	 @ param oIndex
	 * List of ISO index for that particular rank
     */
	virtual HRESULT SetIndexDOFTarget (int &iRank, CATListOfInt *& iIndex) = 0;

  /**
    * To insert a new DOF Target at a Rank
    *   @param iRank
    *      DOF Rank at which a new DOF has to be added.
    *   @param iMode
    *      Mode of insertion. Can be any one of Modified, New, Removed
    */

	virtual HRESULT InsertAfterDOFTarget (int &iRank, CATIMfgMillTurnDOFMgt::MfgInsertionMode &iMode) = 0;
  /**
    * To set Insertion mode for a DOF Target
    *   @param iRank
    *      DOF Rank at which a new Insertion mode has to be set.
    *   @param oMode
    *      Mode of insertion. Acceptable values : YES/NO
    */

	virtual HRESULT SetInsertionModeDOFTarget (int &iRank, CATIMfgMillTurnDOFMgt::MfgInsertionMode &iMode) = 0;

  /**
    * To get the Insertion mode for a DOF Target
    *   @param iRank
    *      DOF Rank for which a Insertion mode has to be queried.
    *   @param iMode
    *      Mode of insertion. Acceptable values : YES/NO
    */

	virtual HRESULT GetInsertionModeDOFTarget (int &iRank, CATIMfgMillTurnDOFMgt::MfgInsertionMode &oMode) = 0;
	
   /**
    * To set the Edition mode for a DOF Target
    *   @param iRank
    *      DOF Rank for which a new Edition mode has to be set.
    *   @param iMode
    *      Mode of edition. Acceptable values : YES/NO
    */

	virtual HRESULT SetEditionModeDOFTarget (int &iRank, CATIMfgMillTurnDOFMgt::MfgEditionMode &iMode) = 0;
	
	/**
	 * To get the Edition mode for a DOF Target
	 * @param iRank
	 * DOF Rank for which Edition mode has to be set.
     * @param oMode
     * Mode of edition. Acceptable values : YES/NO
    */

	virtual HRESULT GetEditionModeDOFTarget (int &iRank, CATIMfgMillTurnDOFMgt::MfgEditionMode &oMode) = 0;

	/**
	 * method to get the target informations for a particular DOF rank
	 * @param opDOF
	 * concatenated list of DOF values 
	 * @param CATIMfgMillTurnDOFMgt::MfgTimingMode oTimingMode
	 * timing mode for the target 
	 * @param oMfgFeedMode
	 * list of values of feed type
	 * @param oValue
	 * list of values of feed rate
	 * @param oIndex
	 * List of ISO indexes
	 * @param oInsertMode
	 * insert mode for DOF table
	 * @param oTransfo
	 * List of tcp transformation
	*/
	virtual HRESULT GetDOFTargetInfos (int &iRank, CATListOfDouble *&opDOF, CATIMfgMillTurnDOFMgt::MfgTimingMode &oTimingMode, double &oValue, int &oIndex, CATIMfgMillTurnDOFMgt::MfgInsertionMode &oInsertMode, CATMathTransformation &oTransfo) = 0;

	/**
	 * overloaded method to get the target informations for a particular DOF rank
	 * @param opDOF
	 * concatenated list of DOF values 
	 * @param CATIMfgMillTurnDOFMgt::MfgTimingMode oTimingMode
	 * timing mode for the target 
	 * @param oMfgFeedMode
	 * list of values of feed type
	 * @param oValue
	 * list of values of feed rate
	 * @param oIndex
	 * List of ISO indexes
	 * @param oInsertMode
	 * insert mode for DOF table
	 * @param oTransfo
	 * List of tcp transformation
	*/
	virtual HRESULT GetDOFTargetInfos (int iRank, 
		                               CATListOfDouble *&                     opDOF,
									   CATIMfgMillTurnDOFMgt::MfgTimingMode &    oTimingMode,
									   CATListOfInt	*&						  oMfgFeedMode,//CATIMfgMillTurnDOFMgt::MfgFeedMode &      oMfgFeedMode,
									   CATListOfDouble *&                     oValue,
									   CATListOfInt *&                        oIndex,
									   CATIMfgMillTurnDOFMgt::MfgInsertionMode & oInsertMode,
									   CATMathTransformation &                oTransfo) = 0;
	/**
	* method to get the tpsynchro mode for the dof table
	* @ param iCircleMode
	* TPsynchromode::circle mode 
	* @ param iCycleMode
	* TPsynchromode:: cycle mode 
	*/

	virtual HRESULT SetDOFTableSynchroModes (CATIMfgTPSynchro::CircleMode &iCircleMode, CATIMfgTPSynchro::CycleMode &iCycleMode) = 0;
	
	/**
	* method to get the tpsynchro mode for the dof table
	* @ param oCircleMode
	* TPsynchromode::circle mode 
	* @ param oCycleMode
	* TPsynchromode:: cycle mode 
	*/
	virtual HRESULT GetDOFTableSynchroModes (CATIMfgTPSynchro::CircleMode &oCircleMode, CATIMfgTPSynchro::CycleMode &oCycleMode) = 0;


	/**
	* method to get the indexing mode for the DOF table
	* @ param oIndexingMode
	* indexing mode
	*/
	virtual HRESULT GetIndexingMode (CATIMfgMillTurnDOFMgt::MfgIndexingMode &oIndexingMode) = 0;

	/**
	* method to set the indexing mode for the dof table
	* @ param iIndexingMode
	* indexing mode 
	*/
	virtual HRESULT SetIndexingMode (CATIMfgMillTurnDOFMgt::MfgIndexingMode iIndexingMode) = 0;

	/**
     * sets tcptransformation for any particular DOF rank
	 * @  param iRank
	 * Rank on which ISO indexes to set
	 * @ param iTransfo
	 * transformation to set
     */

	virtual HRESULT SetTCPForDOFTarget(int &iRank, CATMathTransformation &iTransfo) = 0;


	/**
     * returns tcptransformation for any particular DOF rank
	 * @  param iRank
	 * Rank on which ISO indexes to retrieve
	 * @ param oTransfo
	 * transformation to return
     */
	virtual HRESULT GetTCPForDOFTarget (int &iRank, CATMathTransformation &oTransfo) = 0;
	
	/**
     * Retrieves the SubProgramID corresponding to a rank.<br>
	 *   @param iRank
	 *      The rank of the DOF
	 *   @param oSubID
	 *      The value of the subprogram ID	 
     */
	virtual HRESULT GetSubProgramID (const int & iRank, CATListOfInt *& oSubID) = 0;
	
	/**
     * Retrieves the ISO Index corresponding to a rank.<br>
	 *   @param iRank
	 *      The rank of the DOF
	 *   @param oSubID
	 *      The value of the subprogram ISO Index	 
     */
	virtual HRESULT GetSubIndexDOFTarget (const int & iRank, CATListOfInt *& oSubIndex) = 0;


  // No constructors or destructors on this pure virtual base class
  // --------------------------------------------------------------
};

CATDeclareHandler( CATIMfgMillTurnDOFMgt, CATBaseUnknown) ;
//------------------------------------------------------------------

#endif

