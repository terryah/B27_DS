#ifndef __TIE_IDNBIgpWorkbenchAddin
#define __TIE_IDNBIgpWorkbenchAddin

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "IDNBIgpWorkbenchAddin.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface IDNBIgpWorkbenchAddin */
#define declare_TIE_IDNBIgpWorkbenchAddin(classe) \
 \
 \
class TIEIDNBIgpWorkbenchAddin##classe : public IDNBIgpWorkbenchAddin \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(IDNBIgpWorkbenchAddin, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual void              CreateCommands () ; \
      virtual CATCmdContainer * CreateToolbars () ; \
};



#define ENVTIEdeclare_IDNBIgpWorkbenchAddin(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual void              CreateCommands () ; \
virtual CATCmdContainer * CreateToolbars () ; \


#define ENVTIEdefine_IDNBIgpWorkbenchAddin(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
void               ENVTIEName::CreateCommands ()  \
{ \
 (ENVTIECALL(IDNBIgpWorkbenchAddin,ENVTIETypeLetter,ENVTIELetter)CreateCommands ()); \
} \
CATCmdContainer *  ENVTIEName::CreateToolbars ()  \
{ \
return (ENVTIECALL(IDNBIgpWorkbenchAddin,ENVTIETypeLetter,ENVTIELetter)CreateToolbars ()); \
} \


/* Name of the TIE class */
#define class_TIE_IDNBIgpWorkbenchAddin(classe)    TIEIDNBIgpWorkbenchAddin##classe


/* Common methods inside a TIE */
#define common_TIE_IDNBIgpWorkbenchAddin(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(IDNBIgpWorkbenchAddin, classe) \
 \
 \
CATImplementTIEMethods(IDNBIgpWorkbenchAddin, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(IDNBIgpWorkbenchAddin, classe, 1) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(IDNBIgpWorkbenchAddin, classe) \
CATImplementCATBaseUnknownMethodsForTIE(IDNBIgpWorkbenchAddin, classe) \
 \
void               TIEIDNBIgpWorkbenchAddin##classe::CreateCommands ()  \
{ \
   ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateCommands (); \
} \
CATCmdContainer *  TIEIDNBIgpWorkbenchAddin##classe::CreateToolbars ()  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateToolbars ()); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_IDNBIgpWorkbenchAddin(classe) \
 \
 \
declare_TIE_IDNBIgpWorkbenchAddin(classe) \
 \
 \
CATMetaClass * __stdcall TIEIDNBIgpWorkbenchAddin##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_IDNBIgpWorkbenchAddin,"IDNBIgpWorkbenchAddin",IDNBIgpWorkbenchAddin::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_IDNBIgpWorkbenchAddin(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(IDNBIgpWorkbenchAddin, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicIDNBIgpWorkbenchAddin##classe(classe::MetaObject(),IDNBIgpWorkbenchAddin::MetaObject(),(void *)CreateTIEIDNBIgpWorkbenchAddin##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_IDNBIgpWorkbenchAddin(classe) \
 \
 \
declare_TIE_IDNBIgpWorkbenchAddin(classe) \
 \
 \
CATMetaClass * __stdcall TIEIDNBIgpWorkbenchAddin##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_IDNBIgpWorkbenchAddin,"IDNBIgpWorkbenchAddin",IDNBIgpWorkbenchAddin::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_IDNBIgpWorkbenchAddin(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(IDNBIgpWorkbenchAddin, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicIDNBIgpWorkbenchAddin##classe(classe::MetaObject(),IDNBIgpWorkbenchAddin::MetaObject(),(void *)CreateTIEIDNBIgpWorkbenchAddin##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_IDNBIgpWorkbenchAddin(classe) TIE_IDNBIgpWorkbenchAddin(classe)
#else
#define BOA_IDNBIgpWorkbenchAddin(classe) CATImplementBOA(IDNBIgpWorkbenchAddin, classe)
#endif

#endif
