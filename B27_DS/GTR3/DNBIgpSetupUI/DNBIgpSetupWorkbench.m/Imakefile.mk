# COPYRIGHT DELMIA CORP. 2002
#======================================================================
#  Imakefile for module DNBIgpSetupWorkbench
#======================================================================
#
# SHARED LIBRARY
#

BUILT_OBJECT_TYPE=SHARED LIBRARY
INCLUDED_MODULES = DNBIgpUserActions

# DO NOT EDIT :: THE CAA2 WIZARDS WILL ADD CODE HERE
WIZARD_LINK_MODULES = JS0GROUP \
                      AD0XXBAS \
                      AC0SPBAS\
                      CATObjectModelerBase \
                      KnowledgeItf \
                      WSPROCESS  \
                      CD0FRAME \
                      ProcessInterfaces \
                      JS0FM \
	                   CATMathematics \
                      CATMathStream\
                      DI0PANV2 \
                      DNBIgripSimBase \
                      DNBIgpSetupModel \
                      DNBIgpSetupInterfaces \
					       DpmFastenerInf \
                      SELECT   \
                      AS0STARTUP \
                      RouRoutableInterfaces \
                      CATArrWkbConfiguration \
                      CATViz \
                      DNBProductUI \
                      DNBIgpMultiWorkbench \
                      DNBIgripSimItfCPP

# END WIZARD EDITION ZONE

LINK_WITH = $(WIZARD_LINK_MODULES) DNBSimulationUI DNBProcessUI DNBSimulationItf


