# COPYRIGHT DASSAULT SYSTEMES 2004
#======================================================================
# Imakefile for module DNBIgpTagTaskReconcile.m
#======================================================================
#
#  Aug 2004  Creation: Code generated by the CAA wizard  bqs
#======================================================================
#
# NONE 
#
BUILT_OBJECT_TYPE=NONE 
 
# DO NOT EDIT :: THE CAA2 WIZARDS WILL ADD CODE HERE
WIZARD_LINK_MODULES = JS0GROUP \
JS0GROUP JS0FM AD0XXBAS DNBIgpSetupUIUUID  \
OM0EDPRO  \
DI0PANV2  \
JS0SETT  \
CATInfInterfaces DNBIgpSetupPubIDL DNBIgpSetupInterfacesUUID  \
DNBIgpSetupItfCPP 
# END WIZARD EDITION ZONE

LINK_WITH = $(WIZARD_LINK_MODULES)

# System dependant variables
#
OS = AIX
#
OS = HP-UX
#
OS = IRIX
#
OS = SunOS
#
OS = Windows_NT
