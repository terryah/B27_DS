// COPYRIGHT Dassault Systemes 2005
//===================================================================
//
// AUTICsmExtBlockManager.h
// Define the AUTICsmExtBlockManager interface
//
//===================================================================
//
// Usage notes:
//   New interface: describe its use here
//
//===================================================================
//
//  Oct 2005  Creation: Code generated by the CAA wizard  azh
//===================================================================
/**
* @CAA2Level L0
* @CAA2Usage U5
*/

/**
 * @fullreview AZH BPZ 05:10:04
 * @quickreview AZG 06:04:14
 */

#ifndef AUTICsmExtBlockManager_H
#define AUTICsmExtBlockManager_H

// Local (AUTCsmInterfaces) framework
#include "AUTCoSimGtwItfCPP.h"
#include "AUTICsmExtBlock.h"

// Dialog framework
class CATDlgDialog;
class CATDlgEditor;

// System framework
class CATUnicodeString;
#include "CATBaseUnknown.h"
#include "CATBoolean.h"
#include "CATLISTHand_Clean.h"
#include "CATLISTHand_AllFunct.h"
#include "CATLISTHand_Declare.h"

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByAUTCoSimGtwItfCPP IID IID_AUTICsmExtBlockManager;
#else
extern "C" const IID IID_AUTICsmExtBlockManager ;
#endif

//------------------------------------------------------------------

/**
* Interface dedicated to manage one external block.
*
* <br><b>Role</b>: manages information on external block.
*	
*/

class ExportedByAUTCoSimGtwItfCPP AUTICsmExtBlockManager : public CATBaseUnknown
{
	CATDeclareInterface;
public:

	/** 
	* Use for enable or diseable parameter generator's button in GUI. 
	*
	* @param oEnable [out]
	*   TRUE if implementation has parameter generator.
	*
	* @return
	*   <code>S_OK</code> if everything ran ok, otherwise <code>E_FAIL</code>
	*
	*/

	virtual HRESULT EnableInitParamGenerator(AUTCsmCATBoolean &oEnable) = 0 ; 

	/** 
	* Build parameter generator dialog (if need). 
	*
	* @param ipFather [in]
	*  Father dialog of the parameter generator dialog.
	*
	* @param ipParamEditor [in]
	*  CATDlgEditor is used for get/set parameter.
	*
	* @return
	*   <code>S_OK</code> if everything ran ok, otherwise <code>E_FAIL</code>
	*
	*/

	virtual HRESULT BuildParamGenerator(CATDlgDialog * ipFather, CATDlgEditor * ipParamEditor) = 0 ; 

	/** 
	* Start initialization parameter generator. 
	*
	* @return
	*   <code>S_OK</code> if everything ran ok, otherwise <code>E_FAIL</code>
	*
	*/

	virtual HRESULT StartParamGenerator() = 0 ; 

	/** 
	* Initializes external block
	*
	* <br><b>Role</b>: This method is used to prepare and configure the external block.
	*
	* @param iParam [in]
	*   The parameter which enable to initialize the external block.
	*
	* @return
	*   <code>S_OK</code> if everything ran ok, otherwise <code>E_FAIL</code>
	*
	*/

	virtual HRESULT Initialization(const AUTCsmCATUnicodeString &iParam) = 0 ; 

	/** 
	* Gets implementation's description.
	*
	* @param oDescription [out]
	* Short implementation's description (less than 10 words).
	*
	* @return
	*   <code>S_OK</code> if everything ran ok, otherwise <code>E_FAIL</code>
	*/

	virtual HRESULT GetDescription(AUTCsmCATUnicodeString &oDescription) = 0 ;

	/** 
	* Gets block's items number (inputs and outputs).
	*
	* @param oNb [out]
	* Items number.
	*
	* @return
	*   <code>S_OK</code> if everything ran ok, otherwise <code>E_FAIL</code>
	*/

	virtual HRESULT GetItemsNumber(int &Number) = 0 ;

	/** 
	* Gets item by index (inputs and outputs).
	* Warning 0 is the first indice
	*
	* @param iIndex [in]
	* Item's index.
	*
	* @param oName [out]
	* Item's name.
	*
	* @param oType [out]
	* Item's type. 
	* @see AUTICsmExtBlock.
	*
	* @param oDir [out]
	* Item's direction.
	* @see AUTICsmExtBlock.
	*
	* @return
	*   <code>S_OK</code> if everything ran ok, otherwise <code>E_FAIL</code>
	*/

	virtual HRESULT GetItem(int iIndex, AUTCsmCATUnicodeString &oName, AUTICsmExtBlock::ItemType &oType, AUTICsmExtBlock::ItemDir &oDir) = 0 ;
	virtual HRESULT GetInitialValue(int iIndex, AUTCsmCATUnicodeString &oInitialValue) = 0 ;

	/** 
	* Gets block's parameters.
	*
	* @param oAutoStart [out]
	* TRUE if external block must be launched when simulation start, otherwise it will be launched by the start signal.
	*
	* @param oHasGUI [out]
	* TRUE if external block has graphic interface, otherwise FALSE.
	*
	* @return
	*   <code>S_OK</code> if everything ran ok, otherwise <code>E_FAIL</code>
	*/

	virtual HRESULT GetParameters(CATBaseUnknown *iBlock,AUTCsmCATBoolean &oAutoStart, AUTCsmCATBoolean &oHasGUI) = 0 ;

	/** 
	* Gets block's default graphic parameters.
	*
	* @param oTitle [out]
	* Default Title.
	*
	* @param oX [out]
	* Default X position.
	*
	* @param oY [out]
	* Default Y position.
	*
	* @param oZoom [out]
	* Default zoom value.
	*
	* @return
	*   <code>S_OK</code> if everything ran ok, otherwise <code>E_FAIL</code>
	*/

	virtual HRESULT GetGraphicParameters(CATBaseUnknown *iBlock,AUTCsmCATUnicodeString &oTitle,int &oX, int &oY, double &oZoom) = 0 ;
	/** 
	* Sets AutoStart when modified by user interface.
	*
	* @param iAutoStart [in]
	* TRUE if external block must be launched when simulation start, otherwise it will be launched by the start signal.
	*
	* 
	* @return
	*   <code>S_OK</code> if everything ran ok, otherwise <code>E_FAIL</code>
	*/

	virtual HRESULT SetAutoStart(CATBaseUnknown *iBlock,const AUTCsmCATBoolean iAutoStart) = 0 ;

	/** 
	* Sets block's default graphic parameters when user modify it.
	*
	* @param iTitle [in]
	* Default Title.
	*
	* @param iX [in]
	* Default X position.
	*
	* @param iY [in]
	* Default Y position.
	*
	* @param iZoom [in]
	* Default zoom value.
	*
	* @return
	*   <code>S_OK</code> if everything ran ok, otherwise <code>E_FAIL</code>
	*/

	virtual HRESULT SetGraphicParameters(CATBaseUnknown *iBlock,const AUTCsmCATUnicodeString &iTitle,const int iX,const int iY, const double iZoom) = 0 ;


	/** 
	* Instantiate external block for simulation.
	*
	* @param oppExtBlock [out]
	* Instantiate external block. It will be used during simulation for data exchange between simulator and external world.
	*
	* @return
	*   <code>S_OK</code> if everything ran ok, otherwise <code>E_FAIL</code>
	*/

	virtual HRESULT InstantiateForSimulation(AUTICsmExtBlock ** oppExtBlock) = 0 ;
};

CATDeclareHandler(AUTICsmExtBlockManager, CATBaseUnknown);
//------------------------------------------------------------------

#undef	CATCOLLEC_ExportedBy
#define	CATCOLLEC_ExportedBy	ExportedByAUTCoSimGtwItfCPP
CATLISTHand_DECLARE(AUTICsmExtBlockManager_var)

#endif
