// COPYRIGHT DASSAULT SYSTEMES 2005
/** @CAA2Required */
/**********************************************************************/
/* DON T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPEAR AT ANY TIME */
/**********************************************************************/
/**
 * @fullreview AZH 05:03:24
 * @fullreview AZH IME 05:04:08
 */
#ifdef  _WINDOWS_SOURCE
#ifdef  __AUTCoSimGtwItfCPP
#define ExportedByAUTCoSimGtwItfCPP     __declspec(dllexport)
#else
#define ExportedByAUTCoSimGtwItfCPP     __declspec(dllimport)
#endif
#else
#define ExportedByAUTCoSimGtwItfCPP
#endif
