#ifndef __TIE_AUTICsmExSyncBlock
#define __TIE_AUTICsmExSyncBlock

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "AUTICsmExSyncBlock.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface AUTICsmExSyncBlock */
#define declare_TIE_AUTICsmExSyncBlock(classe) \
 \
 \
class TIEAUTICsmExSyncBlock##classe : public AUTICsmExSyncBlock \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(AUTICsmExSyncBlock, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT Init(void **oFirstContext); \
      virtual HRESULT Step(void *iContext,void **oNextContext); \
      virtual HRESULT Close() ; \
      virtual HRESULT GetState(void *iContext,int iIndex, AUTCsmCATBoolean &oState) ; \
      virtual HRESULT SetState(void *iContext,int iIndex, AUTCsmCATBoolean iState) ; \
      virtual HRESULT GetValue(void *iContext,int iIndex, AUTCsmCATBoolean &oValue) ; \
      virtual HRESULT GetValue(void *iContext,int iIndex, int &oValue) ; \
      virtual HRESULT GetValue(void *iContext,int iIndex, float &oValue) ; \
      virtual HRESULT GetValue(void *iContext,int iIndex, double &oValue) ; \
      virtual HRESULT GetValue(void *iContext,int iIndex, AUTCsmCATUnicodeString &oValue) ; \
      virtual HRESULT SetValue(void *iContext,int iIndex, AUTCsmCATBoolean iValue) ; \
      virtual HRESULT SetValue(void *iContext,int iIndex, int iValue) ; \
      virtual HRESULT SetValue(void *iContext,int iIndex, float iValue) ; \
      virtual HRESULT SetValue(void *iContext,int iIndex, double iValue) ; \
      virtual HRESULT SetValue(void *iContext,int iIndex, AUTCsmCATUnicodeString &iValue) ; \
};



#define ENVTIEdeclare_AUTICsmExSyncBlock(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT Init(void **oFirstContext); \
virtual HRESULT Step(void *iContext,void **oNextContext); \
virtual HRESULT Close() ; \
virtual HRESULT GetState(void *iContext,int iIndex, AUTCsmCATBoolean &oState) ; \
virtual HRESULT SetState(void *iContext,int iIndex, AUTCsmCATBoolean iState) ; \
virtual HRESULT GetValue(void *iContext,int iIndex, AUTCsmCATBoolean &oValue) ; \
virtual HRESULT GetValue(void *iContext,int iIndex, int &oValue) ; \
virtual HRESULT GetValue(void *iContext,int iIndex, float &oValue) ; \
virtual HRESULT GetValue(void *iContext,int iIndex, double &oValue) ; \
virtual HRESULT GetValue(void *iContext,int iIndex, AUTCsmCATUnicodeString &oValue) ; \
virtual HRESULT SetValue(void *iContext,int iIndex, AUTCsmCATBoolean iValue) ; \
virtual HRESULT SetValue(void *iContext,int iIndex, int iValue) ; \
virtual HRESULT SetValue(void *iContext,int iIndex, float iValue) ; \
virtual HRESULT SetValue(void *iContext,int iIndex, double iValue) ; \
virtual HRESULT SetValue(void *iContext,int iIndex, AUTCsmCATUnicodeString &iValue) ; \


#define ENVTIEdefine_AUTICsmExSyncBlock(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT  ENVTIEName::Init(void **oFirstContext) \
{ \
return (ENVTIECALL(AUTICsmExSyncBlock,ENVTIETypeLetter,ENVTIELetter)Init(oFirstContext)); \
} \
HRESULT  ENVTIEName::Step(void *iContext,void **oNextContext) \
{ \
return (ENVTIECALL(AUTICsmExSyncBlock,ENVTIETypeLetter,ENVTIELetter)Step(iContext,oNextContext)); \
} \
HRESULT  ENVTIEName::Close()  \
{ \
return (ENVTIECALL(AUTICsmExSyncBlock,ENVTIETypeLetter,ENVTIELetter)Close()); \
} \
HRESULT  ENVTIEName::GetState(void *iContext,int iIndex, AUTCsmCATBoolean &oState)  \
{ \
return (ENVTIECALL(AUTICsmExSyncBlock,ENVTIETypeLetter,ENVTIELetter)GetState(iContext,iIndex,oState)); \
} \
HRESULT  ENVTIEName::SetState(void *iContext,int iIndex, AUTCsmCATBoolean iState)  \
{ \
return (ENVTIECALL(AUTICsmExSyncBlock,ENVTIETypeLetter,ENVTIELetter)SetState(iContext,iIndex,iState)); \
} \
HRESULT  ENVTIEName::GetValue(void *iContext,int iIndex, AUTCsmCATBoolean &oValue)  \
{ \
return (ENVTIECALL(AUTICsmExSyncBlock,ENVTIETypeLetter,ENVTIELetter)GetValue(iContext,iIndex,oValue)); \
} \
HRESULT  ENVTIEName::GetValue(void *iContext,int iIndex, int &oValue)  \
{ \
return (ENVTIECALL(AUTICsmExSyncBlock,ENVTIETypeLetter,ENVTIELetter)GetValue(iContext,iIndex,oValue)); \
} \
HRESULT  ENVTIEName::GetValue(void *iContext,int iIndex, float &oValue)  \
{ \
return (ENVTIECALL(AUTICsmExSyncBlock,ENVTIETypeLetter,ENVTIELetter)GetValue(iContext,iIndex,oValue)); \
} \
HRESULT  ENVTIEName::GetValue(void *iContext,int iIndex, double &oValue)  \
{ \
return (ENVTIECALL(AUTICsmExSyncBlock,ENVTIETypeLetter,ENVTIELetter)GetValue(iContext,iIndex,oValue)); \
} \
HRESULT  ENVTIEName::GetValue(void *iContext,int iIndex, AUTCsmCATUnicodeString &oValue)  \
{ \
return (ENVTIECALL(AUTICsmExSyncBlock,ENVTIETypeLetter,ENVTIELetter)GetValue(iContext,iIndex,oValue)); \
} \
HRESULT  ENVTIEName::SetValue(void *iContext,int iIndex, AUTCsmCATBoolean iValue)  \
{ \
return (ENVTIECALL(AUTICsmExSyncBlock,ENVTIETypeLetter,ENVTIELetter)SetValue(iContext,iIndex,iValue)); \
} \
HRESULT  ENVTIEName::SetValue(void *iContext,int iIndex, int iValue)  \
{ \
return (ENVTIECALL(AUTICsmExSyncBlock,ENVTIETypeLetter,ENVTIELetter)SetValue(iContext,iIndex,iValue)); \
} \
HRESULT  ENVTIEName::SetValue(void *iContext,int iIndex, float iValue)  \
{ \
return (ENVTIECALL(AUTICsmExSyncBlock,ENVTIETypeLetter,ENVTIELetter)SetValue(iContext,iIndex,iValue)); \
} \
HRESULT  ENVTIEName::SetValue(void *iContext,int iIndex, double iValue)  \
{ \
return (ENVTIECALL(AUTICsmExSyncBlock,ENVTIETypeLetter,ENVTIELetter)SetValue(iContext,iIndex,iValue)); \
} \
HRESULT  ENVTIEName::SetValue(void *iContext,int iIndex, AUTCsmCATUnicodeString &iValue)  \
{ \
return (ENVTIECALL(AUTICsmExSyncBlock,ENVTIETypeLetter,ENVTIELetter)SetValue(iContext,iIndex,iValue)); \
} \


/* Name of the TIE class */
#define class_TIE_AUTICsmExSyncBlock(classe)    TIEAUTICsmExSyncBlock##classe


/* Common methods inside a TIE */
#define common_TIE_AUTICsmExSyncBlock(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(AUTICsmExSyncBlock, classe) \
 \
 \
CATImplementTIEMethods(AUTICsmExSyncBlock, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(AUTICsmExSyncBlock, classe, 1) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(AUTICsmExSyncBlock, classe) \
CATImplementCATBaseUnknownMethodsForTIE(AUTICsmExSyncBlock, classe) \
 \
HRESULT  TIEAUTICsmExSyncBlock##classe::Init(void **oFirstContext) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Init(oFirstContext)); \
} \
HRESULT  TIEAUTICsmExSyncBlock##classe::Step(void *iContext,void **oNextContext) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Step(iContext,oNextContext)); \
} \
HRESULT  TIEAUTICsmExSyncBlock##classe::Close()  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Close()); \
} \
HRESULT  TIEAUTICsmExSyncBlock##classe::GetState(void *iContext,int iIndex, AUTCsmCATBoolean &oState)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetState(iContext,iIndex,oState)); \
} \
HRESULT  TIEAUTICsmExSyncBlock##classe::SetState(void *iContext,int iIndex, AUTCsmCATBoolean iState)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetState(iContext,iIndex,iState)); \
} \
HRESULT  TIEAUTICsmExSyncBlock##classe::GetValue(void *iContext,int iIndex, AUTCsmCATBoolean &oValue)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetValue(iContext,iIndex,oValue)); \
} \
HRESULT  TIEAUTICsmExSyncBlock##classe::GetValue(void *iContext,int iIndex, int &oValue)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetValue(iContext,iIndex,oValue)); \
} \
HRESULT  TIEAUTICsmExSyncBlock##classe::GetValue(void *iContext,int iIndex, float &oValue)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetValue(iContext,iIndex,oValue)); \
} \
HRESULT  TIEAUTICsmExSyncBlock##classe::GetValue(void *iContext,int iIndex, double &oValue)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetValue(iContext,iIndex,oValue)); \
} \
HRESULT  TIEAUTICsmExSyncBlock##classe::GetValue(void *iContext,int iIndex, AUTCsmCATUnicodeString &oValue)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetValue(iContext,iIndex,oValue)); \
} \
HRESULT  TIEAUTICsmExSyncBlock##classe::SetValue(void *iContext,int iIndex, AUTCsmCATBoolean iValue)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetValue(iContext,iIndex,iValue)); \
} \
HRESULT  TIEAUTICsmExSyncBlock##classe::SetValue(void *iContext,int iIndex, int iValue)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetValue(iContext,iIndex,iValue)); \
} \
HRESULT  TIEAUTICsmExSyncBlock##classe::SetValue(void *iContext,int iIndex, float iValue)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetValue(iContext,iIndex,iValue)); \
} \
HRESULT  TIEAUTICsmExSyncBlock##classe::SetValue(void *iContext,int iIndex, double iValue)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetValue(iContext,iIndex,iValue)); \
} \
HRESULT  TIEAUTICsmExSyncBlock##classe::SetValue(void *iContext,int iIndex, AUTCsmCATUnicodeString &iValue)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetValue(iContext,iIndex,iValue)); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_AUTICsmExSyncBlock(classe) \
 \
 \
declare_TIE_AUTICsmExSyncBlock(classe) \
 \
 \
CATMetaClass * __stdcall TIEAUTICsmExSyncBlock##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_AUTICsmExSyncBlock,"AUTICsmExSyncBlock",AUTICsmExSyncBlock::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_AUTICsmExSyncBlock(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(AUTICsmExSyncBlock, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicAUTICsmExSyncBlock##classe(classe::MetaObject(),AUTICsmExSyncBlock::MetaObject(),(void *)CreateTIEAUTICsmExSyncBlock##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_AUTICsmExSyncBlock(classe) \
 \
 \
declare_TIE_AUTICsmExSyncBlock(classe) \
 \
 \
CATMetaClass * __stdcall TIEAUTICsmExSyncBlock##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_AUTICsmExSyncBlock,"AUTICsmExSyncBlock",AUTICsmExSyncBlock::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_AUTICsmExSyncBlock(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(AUTICsmExSyncBlock, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicAUTICsmExSyncBlock##classe(classe::MetaObject(),AUTICsmExSyncBlock::MetaObject(),(void *)CreateTIEAUTICsmExSyncBlock##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_AUTICsmExSyncBlock(classe) TIE_AUTICsmExSyncBlock(classe)
#else
#define BOA_AUTICsmExSyncBlock(classe) CATImplementBOA(AUTICsmExSyncBlock, classe)
#endif

#endif
