#ifndef __TIE_AUTICsmExSyncBlockManager
#define __TIE_AUTICsmExSyncBlockManager

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "AUTICsmExSyncBlockManager.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface AUTICsmExSyncBlockManager */
#define declare_TIE_AUTICsmExSyncBlockManager(classe) \
 \
 \
class TIEAUTICsmExSyncBlockManager##classe : public AUTICsmExSyncBlockManager \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(AUTICsmExSyncBlockManager, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT EnableInitParamGenerator(AUTCsmCATBoolean &oEnable) ; \
      virtual HRESULT BuildParamGenerator(CATDlgDialog * ipFather, CATDlgEditor * ipParamEditor) ; \
      virtual HRESULT StartParamGenerator() ; \
      virtual HRESULT Initialization(const AUTCsmCATUnicodeString &iParam) ; \
      virtual HRESULT GetDescription(AUTCsmCATUnicodeString &oDescription) ; \
      virtual HRESULT GetItemsNumber(const AUTCsmCATUnicodeString &iParam,int &NumberIn,int &NumberOut) ; \
      virtual HRESULT GetInput(const AUTCsmCATUnicodeString &iParam,int iIndex, AUTCsmCATUnicodeString &oName, AUTICsmExSyncBlock::ItemType &oType, AUTCsmCATUnicodeString &oInitialValue) ; \
      virtual HRESULT GetOutput(const AUTCsmCATUnicodeString &iParam,int iIndex, AUTCsmCATUnicodeString &oName, AUTICsmExSyncBlock::ItemType &oType, AUTCsmCATUnicodeString &oInitialValue) ; \
      virtual HRESULT Instantiate(const AUTCsmCATUnicodeString &iParam,AUTICsmExSyncBlock ** oppExtBlock) ; \
};



#define ENVTIEdeclare_AUTICsmExSyncBlockManager(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT EnableInitParamGenerator(AUTCsmCATBoolean &oEnable) ; \
virtual HRESULT BuildParamGenerator(CATDlgDialog * ipFather, CATDlgEditor * ipParamEditor) ; \
virtual HRESULT StartParamGenerator() ; \
virtual HRESULT Initialization(const AUTCsmCATUnicodeString &iParam) ; \
virtual HRESULT GetDescription(AUTCsmCATUnicodeString &oDescription) ; \
virtual HRESULT GetItemsNumber(const AUTCsmCATUnicodeString &iParam,int &NumberIn,int &NumberOut) ; \
virtual HRESULT GetInput(const AUTCsmCATUnicodeString &iParam,int iIndex, AUTCsmCATUnicodeString &oName, AUTICsmExSyncBlock::ItemType &oType, AUTCsmCATUnicodeString &oInitialValue) ; \
virtual HRESULT GetOutput(const AUTCsmCATUnicodeString &iParam,int iIndex, AUTCsmCATUnicodeString &oName, AUTICsmExSyncBlock::ItemType &oType, AUTCsmCATUnicodeString &oInitialValue) ; \
virtual HRESULT Instantiate(const AUTCsmCATUnicodeString &iParam,AUTICsmExSyncBlock ** oppExtBlock) ; \


#define ENVTIEdefine_AUTICsmExSyncBlockManager(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT  ENVTIEName::EnableInitParamGenerator(AUTCsmCATBoolean &oEnable)  \
{ \
return (ENVTIECALL(AUTICsmExSyncBlockManager,ENVTIETypeLetter,ENVTIELetter)EnableInitParamGenerator(oEnable)); \
} \
HRESULT  ENVTIEName::BuildParamGenerator(CATDlgDialog * ipFather, CATDlgEditor * ipParamEditor)  \
{ \
return (ENVTIECALL(AUTICsmExSyncBlockManager,ENVTIETypeLetter,ENVTIELetter)BuildParamGenerator(ipFather,ipParamEditor)); \
} \
HRESULT  ENVTIEName::StartParamGenerator()  \
{ \
return (ENVTIECALL(AUTICsmExSyncBlockManager,ENVTIETypeLetter,ENVTIELetter)StartParamGenerator()); \
} \
HRESULT  ENVTIEName::Initialization(const AUTCsmCATUnicodeString &iParam)  \
{ \
return (ENVTIECALL(AUTICsmExSyncBlockManager,ENVTIETypeLetter,ENVTIELetter)Initialization(iParam)); \
} \
HRESULT  ENVTIEName::GetDescription(AUTCsmCATUnicodeString &oDescription)  \
{ \
return (ENVTIECALL(AUTICsmExSyncBlockManager,ENVTIETypeLetter,ENVTIELetter)GetDescription(oDescription)); \
} \
HRESULT  ENVTIEName::GetItemsNumber(const AUTCsmCATUnicodeString &iParam,int &NumberIn,int &NumberOut)  \
{ \
return (ENVTIECALL(AUTICsmExSyncBlockManager,ENVTIETypeLetter,ENVTIELetter)GetItemsNumber(iParam,NumberIn,NumberOut)); \
} \
HRESULT  ENVTIEName::GetInput(const AUTCsmCATUnicodeString &iParam,int iIndex, AUTCsmCATUnicodeString &oName, AUTICsmExSyncBlock::ItemType &oType, AUTCsmCATUnicodeString &oInitialValue)  \
{ \
return (ENVTIECALL(AUTICsmExSyncBlockManager,ENVTIETypeLetter,ENVTIELetter)GetInput(iParam,iIndex,oName,oType,oInitialValue)); \
} \
HRESULT  ENVTIEName::GetOutput(const AUTCsmCATUnicodeString &iParam,int iIndex, AUTCsmCATUnicodeString &oName, AUTICsmExSyncBlock::ItemType &oType, AUTCsmCATUnicodeString &oInitialValue)  \
{ \
return (ENVTIECALL(AUTICsmExSyncBlockManager,ENVTIETypeLetter,ENVTIELetter)GetOutput(iParam,iIndex,oName,oType,oInitialValue)); \
} \
HRESULT  ENVTIEName::Instantiate(const AUTCsmCATUnicodeString &iParam,AUTICsmExSyncBlock ** oppExtBlock)  \
{ \
return (ENVTIECALL(AUTICsmExSyncBlockManager,ENVTIETypeLetter,ENVTIELetter)Instantiate(iParam,oppExtBlock)); \
} \


/* Name of the TIE class */
#define class_TIE_AUTICsmExSyncBlockManager(classe)    TIEAUTICsmExSyncBlockManager##classe


/* Common methods inside a TIE */
#define common_TIE_AUTICsmExSyncBlockManager(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(AUTICsmExSyncBlockManager, classe) \
 \
 \
CATImplementTIEMethods(AUTICsmExSyncBlockManager, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(AUTICsmExSyncBlockManager, classe, 1) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(AUTICsmExSyncBlockManager, classe) \
CATImplementCATBaseUnknownMethodsForTIE(AUTICsmExSyncBlockManager, classe) \
 \
HRESULT  TIEAUTICsmExSyncBlockManager##classe::EnableInitParamGenerator(AUTCsmCATBoolean &oEnable)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->EnableInitParamGenerator(oEnable)); \
} \
HRESULT  TIEAUTICsmExSyncBlockManager##classe::BuildParamGenerator(CATDlgDialog * ipFather, CATDlgEditor * ipParamEditor)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->BuildParamGenerator(ipFather,ipParamEditor)); \
} \
HRESULT  TIEAUTICsmExSyncBlockManager##classe::StartParamGenerator()  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->StartParamGenerator()); \
} \
HRESULT  TIEAUTICsmExSyncBlockManager##classe::Initialization(const AUTCsmCATUnicodeString &iParam)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Initialization(iParam)); \
} \
HRESULT  TIEAUTICsmExSyncBlockManager##classe::GetDescription(AUTCsmCATUnicodeString &oDescription)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetDescription(oDescription)); \
} \
HRESULT  TIEAUTICsmExSyncBlockManager##classe::GetItemsNumber(const AUTCsmCATUnicodeString &iParam,int &NumberIn,int &NumberOut)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItemsNumber(iParam,NumberIn,NumberOut)); \
} \
HRESULT  TIEAUTICsmExSyncBlockManager##classe::GetInput(const AUTCsmCATUnicodeString &iParam,int iIndex, AUTCsmCATUnicodeString &oName, AUTICsmExSyncBlock::ItemType &oType, AUTCsmCATUnicodeString &oInitialValue)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetInput(iParam,iIndex,oName,oType,oInitialValue)); \
} \
HRESULT  TIEAUTICsmExSyncBlockManager##classe::GetOutput(const AUTCsmCATUnicodeString &iParam,int iIndex, AUTCsmCATUnicodeString &oName, AUTICsmExSyncBlock::ItemType &oType, AUTCsmCATUnicodeString &oInitialValue)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetOutput(iParam,iIndex,oName,oType,oInitialValue)); \
} \
HRESULT  TIEAUTICsmExSyncBlockManager##classe::Instantiate(const AUTCsmCATUnicodeString &iParam,AUTICsmExSyncBlock ** oppExtBlock)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Instantiate(iParam,oppExtBlock)); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_AUTICsmExSyncBlockManager(classe) \
 \
 \
declare_TIE_AUTICsmExSyncBlockManager(classe) \
 \
 \
CATMetaClass * __stdcall TIEAUTICsmExSyncBlockManager##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_AUTICsmExSyncBlockManager,"AUTICsmExSyncBlockManager",AUTICsmExSyncBlockManager::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_AUTICsmExSyncBlockManager(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(AUTICsmExSyncBlockManager, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicAUTICsmExSyncBlockManager##classe(classe::MetaObject(),AUTICsmExSyncBlockManager::MetaObject(),(void *)CreateTIEAUTICsmExSyncBlockManager##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_AUTICsmExSyncBlockManager(classe) \
 \
 \
declare_TIE_AUTICsmExSyncBlockManager(classe) \
 \
 \
CATMetaClass * __stdcall TIEAUTICsmExSyncBlockManager##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_AUTICsmExSyncBlockManager,"AUTICsmExSyncBlockManager",AUTICsmExSyncBlockManager::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_AUTICsmExSyncBlockManager(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(AUTICsmExSyncBlockManager, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicAUTICsmExSyncBlockManager##classe(classe::MetaObject(),AUTICsmExSyncBlockManager::MetaObject(),(void *)CreateTIEAUTICsmExSyncBlockManager##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_AUTICsmExSyncBlockManager(classe) TIE_AUTICsmExSyncBlockManager(classe)
#else
#define BOA_AUTICsmExSyncBlockManager(classe) CATImplementBOA(AUTICsmExSyncBlockManager, classe)
#endif

#endif
