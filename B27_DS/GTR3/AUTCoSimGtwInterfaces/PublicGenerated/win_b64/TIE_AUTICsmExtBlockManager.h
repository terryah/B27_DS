#ifndef __TIE_AUTICsmExtBlockManager
#define __TIE_AUTICsmExtBlockManager

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "AUTICsmExtBlockManager.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface AUTICsmExtBlockManager */
#define declare_TIE_AUTICsmExtBlockManager(classe) \
 \
 \
class TIEAUTICsmExtBlockManager##classe : public AUTICsmExtBlockManager \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(AUTICsmExtBlockManager, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT EnableInitParamGenerator(AUTCsmCATBoolean &oEnable) ; \
      virtual HRESULT BuildParamGenerator(CATDlgDialog * ipFather, CATDlgEditor * ipParamEditor) ; \
      virtual HRESULT StartParamGenerator() ; \
      virtual HRESULT Initialization(const AUTCsmCATUnicodeString &iParam) ; \
      virtual HRESULT GetDescription(AUTCsmCATUnicodeString &oDescription) ; \
      virtual HRESULT GetItemsNumber(int &Number) ; \
      virtual HRESULT GetItem(int iIndex, AUTCsmCATUnicodeString &oName, AUTICsmExtBlock::ItemType &oType, AUTICsmExtBlock::ItemDir &oDir) ; \
      virtual HRESULT GetInitialValue(int iIndex, AUTCsmCATUnicodeString &oInitialValue) ; \
      virtual HRESULT GetParameters(CATBaseUnknown *iBlock,AUTCsmCATBoolean &oAutoStart, AUTCsmCATBoolean &oHasGUI) ; \
      virtual HRESULT GetGraphicParameters(CATBaseUnknown *iBlock,AUTCsmCATUnicodeString &oTitle,int &oX, int &oY, double &oZoom) ; \
      virtual HRESULT SetAutoStart(CATBaseUnknown *iBlock,const AUTCsmCATBoolean iAutoStart) ; \
      virtual HRESULT SetGraphicParameters(CATBaseUnknown *iBlock,const AUTCsmCATUnicodeString &iTitle,const int iX,const int iY, const double iZoom) ; \
      virtual HRESULT InstantiateForSimulation(AUTICsmExtBlock ** oppExtBlock) ; \
};



#define ENVTIEdeclare_AUTICsmExtBlockManager(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT EnableInitParamGenerator(AUTCsmCATBoolean &oEnable) ; \
virtual HRESULT BuildParamGenerator(CATDlgDialog * ipFather, CATDlgEditor * ipParamEditor) ; \
virtual HRESULT StartParamGenerator() ; \
virtual HRESULT Initialization(const AUTCsmCATUnicodeString &iParam) ; \
virtual HRESULT GetDescription(AUTCsmCATUnicodeString &oDescription) ; \
virtual HRESULT GetItemsNumber(int &Number) ; \
virtual HRESULT GetItem(int iIndex, AUTCsmCATUnicodeString &oName, AUTICsmExtBlock::ItemType &oType, AUTICsmExtBlock::ItemDir &oDir) ; \
virtual HRESULT GetInitialValue(int iIndex, AUTCsmCATUnicodeString &oInitialValue) ; \
virtual HRESULT GetParameters(CATBaseUnknown *iBlock,AUTCsmCATBoolean &oAutoStart, AUTCsmCATBoolean &oHasGUI) ; \
virtual HRESULT GetGraphicParameters(CATBaseUnknown *iBlock,AUTCsmCATUnicodeString &oTitle,int &oX, int &oY, double &oZoom) ; \
virtual HRESULT SetAutoStart(CATBaseUnknown *iBlock,const AUTCsmCATBoolean iAutoStart) ; \
virtual HRESULT SetGraphicParameters(CATBaseUnknown *iBlock,const AUTCsmCATUnicodeString &iTitle,const int iX,const int iY, const double iZoom) ; \
virtual HRESULT InstantiateForSimulation(AUTICsmExtBlock ** oppExtBlock) ; \


#define ENVTIEdefine_AUTICsmExtBlockManager(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT  ENVTIEName::EnableInitParamGenerator(AUTCsmCATBoolean &oEnable)  \
{ \
return (ENVTIECALL(AUTICsmExtBlockManager,ENVTIETypeLetter,ENVTIELetter)EnableInitParamGenerator(oEnable)); \
} \
HRESULT  ENVTIEName::BuildParamGenerator(CATDlgDialog * ipFather, CATDlgEditor * ipParamEditor)  \
{ \
return (ENVTIECALL(AUTICsmExtBlockManager,ENVTIETypeLetter,ENVTIELetter)BuildParamGenerator(ipFather,ipParamEditor)); \
} \
HRESULT  ENVTIEName::StartParamGenerator()  \
{ \
return (ENVTIECALL(AUTICsmExtBlockManager,ENVTIETypeLetter,ENVTIELetter)StartParamGenerator()); \
} \
HRESULT  ENVTIEName::Initialization(const AUTCsmCATUnicodeString &iParam)  \
{ \
return (ENVTIECALL(AUTICsmExtBlockManager,ENVTIETypeLetter,ENVTIELetter)Initialization(iParam)); \
} \
HRESULT  ENVTIEName::GetDescription(AUTCsmCATUnicodeString &oDescription)  \
{ \
return (ENVTIECALL(AUTICsmExtBlockManager,ENVTIETypeLetter,ENVTIELetter)GetDescription(oDescription)); \
} \
HRESULT  ENVTIEName::GetItemsNumber(int &Number)  \
{ \
return (ENVTIECALL(AUTICsmExtBlockManager,ENVTIETypeLetter,ENVTIELetter)GetItemsNumber(Number)); \
} \
HRESULT  ENVTIEName::GetItem(int iIndex, AUTCsmCATUnicodeString &oName, AUTICsmExtBlock::ItemType &oType, AUTICsmExtBlock::ItemDir &oDir)  \
{ \
return (ENVTIECALL(AUTICsmExtBlockManager,ENVTIETypeLetter,ENVTIELetter)GetItem(iIndex,oName,oType,oDir)); \
} \
HRESULT  ENVTIEName::GetInitialValue(int iIndex, AUTCsmCATUnicodeString &oInitialValue)  \
{ \
return (ENVTIECALL(AUTICsmExtBlockManager,ENVTIETypeLetter,ENVTIELetter)GetInitialValue(iIndex,oInitialValue)); \
} \
HRESULT  ENVTIEName::GetParameters(CATBaseUnknown *iBlock,AUTCsmCATBoolean &oAutoStart, AUTCsmCATBoolean &oHasGUI)  \
{ \
return (ENVTIECALL(AUTICsmExtBlockManager,ENVTIETypeLetter,ENVTIELetter)GetParameters(iBlock,oAutoStart,oHasGUI)); \
} \
HRESULT  ENVTIEName::GetGraphicParameters(CATBaseUnknown *iBlock,AUTCsmCATUnicodeString &oTitle,int &oX, int &oY, double &oZoom)  \
{ \
return (ENVTIECALL(AUTICsmExtBlockManager,ENVTIETypeLetter,ENVTIELetter)GetGraphicParameters(iBlock,oTitle,oX,oY,oZoom)); \
} \
HRESULT  ENVTIEName::SetAutoStart(CATBaseUnknown *iBlock,const AUTCsmCATBoolean iAutoStart)  \
{ \
return (ENVTIECALL(AUTICsmExtBlockManager,ENVTIETypeLetter,ENVTIELetter)SetAutoStart(iBlock,iAutoStart)); \
} \
HRESULT  ENVTIEName::SetGraphicParameters(CATBaseUnknown *iBlock,const AUTCsmCATUnicodeString &iTitle,const int iX,const int iY, const double iZoom)  \
{ \
return (ENVTIECALL(AUTICsmExtBlockManager,ENVTIETypeLetter,ENVTIELetter)SetGraphicParameters(iBlock,iTitle,iX,iY,iZoom)); \
} \
HRESULT  ENVTIEName::InstantiateForSimulation(AUTICsmExtBlock ** oppExtBlock)  \
{ \
return (ENVTIECALL(AUTICsmExtBlockManager,ENVTIETypeLetter,ENVTIELetter)InstantiateForSimulation(oppExtBlock)); \
} \


/* Name of the TIE class */
#define class_TIE_AUTICsmExtBlockManager(classe)    TIEAUTICsmExtBlockManager##classe


/* Common methods inside a TIE */
#define common_TIE_AUTICsmExtBlockManager(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(AUTICsmExtBlockManager, classe) \
 \
 \
CATImplementTIEMethods(AUTICsmExtBlockManager, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(AUTICsmExtBlockManager, classe, 1) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(AUTICsmExtBlockManager, classe) \
CATImplementCATBaseUnknownMethodsForTIE(AUTICsmExtBlockManager, classe) \
 \
HRESULT  TIEAUTICsmExtBlockManager##classe::EnableInitParamGenerator(AUTCsmCATBoolean &oEnable)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->EnableInitParamGenerator(oEnable)); \
} \
HRESULT  TIEAUTICsmExtBlockManager##classe::BuildParamGenerator(CATDlgDialog * ipFather, CATDlgEditor * ipParamEditor)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->BuildParamGenerator(ipFather,ipParamEditor)); \
} \
HRESULT  TIEAUTICsmExtBlockManager##classe::StartParamGenerator()  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->StartParamGenerator()); \
} \
HRESULT  TIEAUTICsmExtBlockManager##classe::Initialization(const AUTCsmCATUnicodeString &iParam)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Initialization(iParam)); \
} \
HRESULT  TIEAUTICsmExtBlockManager##classe::GetDescription(AUTCsmCATUnicodeString &oDescription)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetDescription(oDescription)); \
} \
HRESULT  TIEAUTICsmExtBlockManager##classe::GetItemsNumber(int &Number)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItemsNumber(Number)); \
} \
HRESULT  TIEAUTICsmExtBlockManager##classe::GetItem(int iIndex, AUTCsmCATUnicodeString &oName, AUTICsmExtBlock::ItemType &oType, AUTICsmExtBlock::ItemDir &oDir)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(iIndex,oName,oType,oDir)); \
} \
HRESULT  TIEAUTICsmExtBlockManager##classe::GetInitialValue(int iIndex, AUTCsmCATUnicodeString &oInitialValue)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetInitialValue(iIndex,oInitialValue)); \
} \
HRESULT  TIEAUTICsmExtBlockManager##classe::GetParameters(CATBaseUnknown *iBlock,AUTCsmCATBoolean &oAutoStart, AUTCsmCATBoolean &oHasGUI)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetParameters(iBlock,oAutoStart,oHasGUI)); \
} \
HRESULT  TIEAUTICsmExtBlockManager##classe::GetGraphicParameters(CATBaseUnknown *iBlock,AUTCsmCATUnicodeString &oTitle,int &oX, int &oY, double &oZoom)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetGraphicParameters(iBlock,oTitle,oX,oY,oZoom)); \
} \
HRESULT  TIEAUTICsmExtBlockManager##classe::SetAutoStart(CATBaseUnknown *iBlock,const AUTCsmCATBoolean iAutoStart)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetAutoStart(iBlock,iAutoStart)); \
} \
HRESULT  TIEAUTICsmExtBlockManager##classe::SetGraphicParameters(CATBaseUnknown *iBlock,const AUTCsmCATUnicodeString &iTitle,const int iX,const int iY, const double iZoom)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetGraphicParameters(iBlock,iTitle,iX,iY,iZoom)); \
} \
HRESULT  TIEAUTICsmExtBlockManager##classe::InstantiateForSimulation(AUTICsmExtBlock ** oppExtBlock)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->InstantiateForSimulation(oppExtBlock)); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_AUTICsmExtBlockManager(classe) \
 \
 \
declare_TIE_AUTICsmExtBlockManager(classe) \
 \
 \
CATMetaClass * __stdcall TIEAUTICsmExtBlockManager##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_AUTICsmExtBlockManager,"AUTICsmExtBlockManager",AUTICsmExtBlockManager::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_AUTICsmExtBlockManager(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(AUTICsmExtBlockManager, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicAUTICsmExtBlockManager##classe(classe::MetaObject(),AUTICsmExtBlockManager::MetaObject(),(void *)CreateTIEAUTICsmExtBlockManager##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_AUTICsmExtBlockManager(classe) \
 \
 \
declare_TIE_AUTICsmExtBlockManager(classe) \
 \
 \
CATMetaClass * __stdcall TIEAUTICsmExtBlockManager##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_AUTICsmExtBlockManager,"AUTICsmExtBlockManager",AUTICsmExtBlockManager::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_AUTICsmExtBlockManager(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(AUTICsmExtBlockManager, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicAUTICsmExtBlockManager##classe(classe::MetaObject(),AUTICsmExtBlockManager::MetaObject(),(void *)CreateTIEAUTICsmExtBlockManager##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_AUTICsmExtBlockManager(classe) TIE_AUTICsmExtBlockManager(classe)
#else
#define BOA_AUTICsmExtBlockManager(classe) CATImplementBOA(AUTICsmExtBlockManager, classe)
#endif

#endif
