#ifndef __TIE_AUTICsmExtBlock
#define __TIE_AUTICsmExtBlock

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "AUTICsmExtBlock.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface AUTICsmExtBlock */
#define declare_TIE_AUTICsmExtBlock(classe) \
 \
 \
class TIEAUTICsmExtBlock##classe : public AUTICsmExtBlock \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(AUTICsmExtBlock, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT Open(AUTCsmCATUnicodeString &iTitle, void * ipData, int oX, int oY, double oZoom) ; \
      virtual HRESULT IsOpened(AUTCsmCATBoolean &oIsOpened); \
      virtual HRESULT Update(); \
      virtual HRESULT Close(const AUTCsmCATBoolean iForReset = FALSE) ; \
      virtual HRESULT GetState(int iIndex, AUTCsmCATBoolean &oState, AUTCsmCATBoolean iForUse = TRUE) ; \
      virtual HRESULT SetState(int iIndex, AUTCsmCATBoolean iState) ; \
      virtual HRESULT GetValue(int iIndex, AUTCsmCATBoolean &oValue) ; \
      virtual HRESULT GetValue(int iIndex, int &oValue) ; \
      virtual HRESULT GetValue(int iIndex, float &oValue) ; \
      virtual HRESULT GetValue(int iIndex, double &oValue) ; \
      virtual HRESULT GetValue(int iIndex, AUTCsmCATUnicodeString &oValue) ; \
      virtual HRESULT SetValue(int iIndex, AUTCsmCATBoolean iValue) ; \
      virtual HRESULT SetValue(int iIndex, int iValue) ; \
      virtual HRESULT SetValue(int iIndex, float iValue) ; \
      virtual HRESULT SetValue(int iIndex, double iValue) ; \
      virtual HRESULT SetValue(int iIndex, AUTCsmCATUnicodeString &iValue) ; \
      virtual HRESULT SetValue(char * ipKey, int iValue) ; \
      virtual HRESULT SetValue(char * ipKey, AUTCsmCATBoolean iValue) ; \
      virtual HRESULT SetValue(char * ipKey, AUTCsmCATUnicodeString &iValue) ; \
      virtual HRESULT SetValue(char * ipKey, float iValue) ; \
      virtual HRESULT SetValue(char * ipKey, double iValue) ; \
      virtual HRESULT SetPosition(int iX,int iY,double iZoom) ; \
      virtual HRESULT GetPosition(int &oX, int &oY, double &oZoom) ; \
      virtual HRESULT SetTitle(AUTCsmCATUnicodeString &iTitle) ; \
      virtual HRESULT GetTitle(AUTCsmCATUnicodeString &oTitle) ; \
      virtual HRESULT GetBlockManager(AUTICsmExtBlockManager_var &ospManager) ; \
};



#define ENVTIEdeclare_AUTICsmExtBlock(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT Open(AUTCsmCATUnicodeString &iTitle, void * ipData, int oX, int oY, double oZoom) ; \
virtual HRESULT IsOpened(AUTCsmCATBoolean &oIsOpened); \
virtual HRESULT Update(); \
virtual HRESULT Close(const AUTCsmCATBoolean iForReset = FALSE) ; \
virtual HRESULT GetState(int iIndex, AUTCsmCATBoolean &oState, AUTCsmCATBoolean iForUse = TRUE) ; \
virtual HRESULT SetState(int iIndex, AUTCsmCATBoolean iState) ; \
virtual HRESULT GetValue(int iIndex, AUTCsmCATBoolean &oValue) ; \
virtual HRESULT GetValue(int iIndex, int &oValue) ; \
virtual HRESULT GetValue(int iIndex, float &oValue) ; \
virtual HRESULT GetValue(int iIndex, double &oValue) ; \
virtual HRESULT GetValue(int iIndex, AUTCsmCATUnicodeString &oValue) ; \
virtual HRESULT SetValue(int iIndex, AUTCsmCATBoolean iValue) ; \
virtual HRESULT SetValue(int iIndex, int iValue) ; \
virtual HRESULT SetValue(int iIndex, float iValue) ; \
virtual HRESULT SetValue(int iIndex, double iValue) ; \
virtual HRESULT SetValue(int iIndex, AUTCsmCATUnicodeString &iValue) ; \
virtual HRESULT SetValue(char * ipKey, int iValue) ; \
virtual HRESULT SetValue(char * ipKey, AUTCsmCATBoolean iValue) ; \
virtual HRESULT SetValue(char * ipKey, AUTCsmCATUnicodeString &iValue) ; \
virtual HRESULT SetValue(char * ipKey, float iValue) ; \
virtual HRESULT SetValue(char * ipKey, double iValue) ; \
virtual HRESULT SetPosition(int iX,int iY,double iZoom) ; \
virtual HRESULT GetPosition(int &oX, int &oY, double &oZoom) ; \
virtual HRESULT SetTitle(AUTCsmCATUnicodeString &iTitle) ; \
virtual HRESULT GetTitle(AUTCsmCATUnicodeString &oTitle) ; \
virtual HRESULT GetBlockManager(AUTICsmExtBlockManager_var &ospManager) ; \


#define ENVTIEdefine_AUTICsmExtBlock(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT  ENVTIEName::Open(AUTCsmCATUnicodeString &iTitle, void * ipData, int oX, int oY, double oZoom)  \
{ \
return (ENVTIECALL(AUTICsmExtBlock,ENVTIETypeLetter,ENVTIELetter)Open(iTitle,ipData,oX,oY,oZoom)); \
} \
HRESULT  ENVTIEName::IsOpened(AUTCsmCATBoolean &oIsOpened) \
{ \
return (ENVTIECALL(AUTICsmExtBlock,ENVTIETypeLetter,ENVTIELetter)IsOpened(oIsOpened)); \
} \
HRESULT  ENVTIEName::Update() \
{ \
return (ENVTIECALL(AUTICsmExtBlock,ENVTIETypeLetter,ENVTIELetter)Update()); \
} \
HRESULT  ENVTIEName::Close(const AUTCsmCATBoolean iForReset )  \
{ \
return (ENVTIECALL(AUTICsmExtBlock,ENVTIETypeLetter,ENVTIELetter)Close(iForReset )); \
} \
HRESULT  ENVTIEName::GetState(int iIndex, AUTCsmCATBoolean &oState, AUTCsmCATBoolean iForUse )  \
{ \
return (ENVTIECALL(AUTICsmExtBlock,ENVTIETypeLetter,ENVTIELetter)GetState(iIndex,oState,iForUse )); \
} \
HRESULT  ENVTIEName::SetState(int iIndex, AUTCsmCATBoolean iState)  \
{ \
return (ENVTIECALL(AUTICsmExtBlock,ENVTIETypeLetter,ENVTIELetter)SetState(iIndex,iState)); \
} \
HRESULT  ENVTIEName::GetValue(int iIndex, AUTCsmCATBoolean &oValue)  \
{ \
return (ENVTIECALL(AUTICsmExtBlock,ENVTIETypeLetter,ENVTIELetter)GetValue(iIndex,oValue)); \
} \
HRESULT  ENVTIEName::GetValue(int iIndex, int &oValue)  \
{ \
return (ENVTIECALL(AUTICsmExtBlock,ENVTIETypeLetter,ENVTIELetter)GetValue(iIndex,oValue)); \
} \
HRESULT  ENVTIEName::GetValue(int iIndex, float &oValue)  \
{ \
return (ENVTIECALL(AUTICsmExtBlock,ENVTIETypeLetter,ENVTIELetter)GetValue(iIndex,oValue)); \
} \
HRESULT  ENVTIEName::GetValue(int iIndex, double &oValue)  \
{ \
return (ENVTIECALL(AUTICsmExtBlock,ENVTIETypeLetter,ENVTIELetter)GetValue(iIndex,oValue)); \
} \
HRESULT  ENVTIEName::GetValue(int iIndex, AUTCsmCATUnicodeString &oValue)  \
{ \
return (ENVTIECALL(AUTICsmExtBlock,ENVTIETypeLetter,ENVTIELetter)GetValue(iIndex,oValue)); \
} \
HRESULT  ENVTIEName::SetValue(int iIndex, AUTCsmCATBoolean iValue)  \
{ \
return (ENVTIECALL(AUTICsmExtBlock,ENVTIETypeLetter,ENVTIELetter)SetValue(iIndex,iValue)); \
} \
HRESULT  ENVTIEName::SetValue(int iIndex, int iValue)  \
{ \
return (ENVTIECALL(AUTICsmExtBlock,ENVTIETypeLetter,ENVTIELetter)SetValue(iIndex,iValue)); \
} \
HRESULT  ENVTIEName::SetValue(int iIndex, float iValue)  \
{ \
return (ENVTIECALL(AUTICsmExtBlock,ENVTIETypeLetter,ENVTIELetter)SetValue(iIndex,iValue)); \
} \
HRESULT  ENVTIEName::SetValue(int iIndex, double iValue)  \
{ \
return (ENVTIECALL(AUTICsmExtBlock,ENVTIETypeLetter,ENVTIELetter)SetValue(iIndex,iValue)); \
} \
HRESULT  ENVTIEName::SetValue(int iIndex, AUTCsmCATUnicodeString &iValue)  \
{ \
return (ENVTIECALL(AUTICsmExtBlock,ENVTIETypeLetter,ENVTIELetter)SetValue(iIndex,iValue)); \
} \
HRESULT  ENVTIEName::SetValue(char * ipKey, int iValue)  \
{ \
return (ENVTIECALL(AUTICsmExtBlock,ENVTIETypeLetter,ENVTIELetter)SetValue(ipKey,iValue)); \
} \
HRESULT  ENVTIEName::SetValue(char * ipKey, AUTCsmCATBoolean iValue)  \
{ \
return (ENVTIECALL(AUTICsmExtBlock,ENVTIETypeLetter,ENVTIELetter)SetValue(ipKey,iValue)); \
} \
HRESULT  ENVTIEName::SetValue(char * ipKey, AUTCsmCATUnicodeString &iValue)  \
{ \
return (ENVTIECALL(AUTICsmExtBlock,ENVTIETypeLetter,ENVTIELetter)SetValue(ipKey,iValue)); \
} \
HRESULT  ENVTIEName::SetValue(char * ipKey, float iValue)  \
{ \
return (ENVTIECALL(AUTICsmExtBlock,ENVTIETypeLetter,ENVTIELetter)SetValue(ipKey,iValue)); \
} \
HRESULT  ENVTIEName::SetValue(char * ipKey, double iValue)  \
{ \
return (ENVTIECALL(AUTICsmExtBlock,ENVTIETypeLetter,ENVTIELetter)SetValue(ipKey,iValue)); \
} \
HRESULT  ENVTIEName::SetPosition(int iX,int iY,double iZoom)  \
{ \
return (ENVTIECALL(AUTICsmExtBlock,ENVTIETypeLetter,ENVTIELetter)SetPosition(iX,iY,iZoom)); \
} \
HRESULT  ENVTIEName::GetPosition(int &oX, int &oY, double &oZoom)  \
{ \
return (ENVTIECALL(AUTICsmExtBlock,ENVTIETypeLetter,ENVTIELetter)GetPosition(oX,oY,oZoom)); \
} \
HRESULT  ENVTIEName::SetTitle(AUTCsmCATUnicodeString &iTitle)  \
{ \
return (ENVTIECALL(AUTICsmExtBlock,ENVTIETypeLetter,ENVTIELetter)SetTitle(iTitle)); \
} \
HRESULT  ENVTIEName::GetTitle(AUTCsmCATUnicodeString &oTitle)  \
{ \
return (ENVTIECALL(AUTICsmExtBlock,ENVTIETypeLetter,ENVTIELetter)GetTitle(oTitle)); \
} \
HRESULT  ENVTIEName::GetBlockManager(AUTICsmExtBlockManager_var &ospManager)  \
{ \
return (ENVTIECALL(AUTICsmExtBlock,ENVTIETypeLetter,ENVTIELetter)GetBlockManager(ospManager)); \
} \


/* Name of the TIE class */
#define class_TIE_AUTICsmExtBlock(classe)    TIEAUTICsmExtBlock##classe


/* Common methods inside a TIE */
#define common_TIE_AUTICsmExtBlock(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(AUTICsmExtBlock, classe) \
 \
 \
CATImplementTIEMethods(AUTICsmExtBlock, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(AUTICsmExtBlock, classe, 1) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(AUTICsmExtBlock, classe) \
CATImplementCATBaseUnknownMethodsForTIE(AUTICsmExtBlock, classe) \
 \
HRESULT  TIEAUTICsmExtBlock##classe::Open(AUTCsmCATUnicodeString &iTitle, void * ipData, int oX, int oY, double oZoom)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Open(iTitle,ipData,oX,oY,oZoom)); \
} \
HRESULT  TIEAUTICsmExtBlock##classe::IsOpened(AUTCsmCATBoolean &oIsOpened) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->IsOpened(oIsOpened)); \
} \
HRESULT  TIEAUTICsmExtBlock##classe::Update() \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Update()); \
} \
HRESULT  TIEAUTICsmExtBlock##classe::Close(const AUTCsmCATBoolean iForReset )  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Close(iForReset )); \
} \
HRESULT  TIEAUTICsmExtBlock##classe::GetState(int iIndex, AUTCsmCATBoolean &oState, AUTCsmCATBoolean iForUse )  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetState(iIndex,oState,iForUse )); \
} \
HRESULT  TIEAUTICsmExtBlock##classe::SetState(int iIndex, AUTCsmCATBoolean iState)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetState(iIndex,iState)); \
} \
HRESULT  TIEAUTICsmExtBlock##classe::GetValue(int iIndex, AUTCsmCATBoolean &oValue)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetValue(iIndex,oValue)); \
} \
HRESULT  TIEAUTICsmExtBlock##classe::GetValue(int iIndex, int &oValue)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetValue(iIndex,oValue)); \
} \
HRESULT  TIEAUTICsmExtBlock##classe::GetValue(int iIndex, float &oValue)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetValue(iIndex,oValue)); \
} \
HRESULT  TIEAUTICsmExtBlock##classe::GetValue(int iIndex, double &oValue)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetValue(iIndex,oValue)); \
} \
HRESULT  TIEAUTICsmExtBlock##classe::GetValue(int iIndex, AUTCsmCATUnicodeString &oValue)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetValue(iIndex,oValue)); \
} \
HRESULT  TIEAUTICsmExtBlock##classe::SetValue(int iIndex, AUTCsmCATBoolean iValue)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetValue(iIndex,iValue)); \
} \
HRESULT  TIEAUTICsmExtBlock##classe::SetValue(int iIndex, int iValue)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetValue(iIndex,iValue)); \
} \
HRESULT  TIEAUTICsmExtBlock##classe::SetValue(int iIndex, float iValue)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetValue(iIndex,iValue)); \
} \
HRESULT  TIEAUTICsmExtBlock##classe::SetValue(int iIndex, double iValue)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetValue(iIndex,iValue)); \
} \
HRESULT  TIEAUTICsmExtBlock##classe::SetValue(int iIndex, AUTCsmCATUnicodeString &iValue)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetValue(iIndex,iValue)); \
} \
HRESULT  TIEAUTICsmExtBlock##classe::SetValue(char * ipKey, int iValue)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetValue(ipKey,iValue)); \
} \
HRESULT  TIEAUTICsmExtBlock##classe::SetValue(char * ipKey, AUTCsmCATBoolean iValue)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetValue(ipKey,iValue)); \
} \
HRESULT  TIEAUTICsmExtBlock##classe::SetValue(char * ipKey, AUTCsmCATUnicodeString &iValue)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetValue(ipKey,iValue)); \
} \
HRESULT  TIEAUTICsmExtBlock##classe::SetValue(char * ipKey, float iValue)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetValue(ipKey,iValue)); \
} \
HRESULT  TIEAUTICsmExtBlock##classe::SetValue(char * ipKey, double iValue)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetValue(ipKey,iValue)); \
} \
HRESULT  TIEAUTICsmExtBlock##classe::SetPosition(int iX,int iY,double iZoom)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetPosition(iX,iY,iZoom)); \
} \
HRESULT  TIEAUTICsmExtBlock##classe::GetPosition(int &oX, int &oY, double &oZoom)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetPosition(oX,oY,oZoom)); \
} \
HRESULT  TIEAUTICsmExtBlock##classe::SetTitle(AUTCsmCATUnicodeString &iTitle)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetTitle(iTitle)); \
} \
HRESULT  TIEAUTICsmExtBlock##classe::GetTitle(AUTCsmCATUnicodeString &oTitle)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetTitle(oTitle)); \
} \
HRESULT  TIEAUTICsmExtBlock##classe::GetBlockManager(AUTICsmExtBlockManager_var &ospManager)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetBlockManager(ospManager)); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_AUTICsmExtBlock(classe) \
 \
 \
declare_TIE_AUTICsmExtBlock(classe) \
 \
 \
CATMetaClass * __stdcall TIEAUTICsmExtBlock##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_AUTICsmExtBlock,"AUTICsmExtBlock",AUTICsmExtBlock::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_AUTICsmExtBlock(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(AUTICsmExtBlock, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicAUTICsmExtBlock##classe(classe::MetaObject(),AUTICsmExtBlock::MetaObject(),(void *)CreateTIEAUTICsmExtBlock##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_AUTICsmExtBlock(classe) \
 \
 \
declare_TIE_AUTICsmExtBlock(classe) \
 \
 \
CATMetaClass * __stdcall TIEAUTICsmExtBlock##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_AUTICsmExtBlock,"AUTICsmExtBlock",AUTICsmExtBlock::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_AUTICsmExtBlock(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(AUTICsmExtBlock, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicAUTICsmExtBlock##classe(classe::MetaObject(),AUTICsmExtBlock::MetaObject(),(void *)CreateTIEAUTICsmExtBlock##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_AUTICsmExtBlock(classe) TIE_AUTICsmExtBlock(classe)
#else
#define BOA_AUTICsmExtBlock(classe) CATImplementBOA(AUTICsmExtBlock, classe)
#endif

#endif
