// COPYRIGHT Dassault Systemes 2000

/**
  * @CAA2Level L0
  * @CAA2Usage U3
  */
//===================================================================
//
// CATIMldSettingPlate.h
// Define the CATIMldSettingPlate interface
//
//===================================================================
//
// Usage notes:
//   New interface: describe its use here
//
//===================================================================
//
//  Sep 2000  Creation: Code generated by the CAA wizard  pic
//===================================================================
#ifndef CATIMldSettingPlate_H
#define CATIMldSettingPlate_H

#include "CATMoldItfCPP.h"
#include "CATIMldPlate.h"

extern ExportedByCATMoldItfCPP IID IID_CATIMldSettingPlate ;

//------------------------------------------------------------------

/**
 * Describe the functionality of your interface here
 * <p>
 * Using this prefered syntax will enable mkdoc to document your class.
 */
class ExportedByCATMoldItfCPP CATIMldSettingPlate: public CATIMldPlate
{
  CATDeclareInterface;

  public:

    /**
     * Remember that interfaces define only virtual pure methods.
     * Dont forget to document your methods.
     * <dl>
     * <dt><b>Example:</b>
     * <pre>
     *       
           *   MyFunction does this and that
           *   @param Arg1
           *      The first argument of MyFunction.
           *   @return
           *      Error code of function.
           * 
     * virtual int MyFunction (int Arg1) = 0;
     * </pre>
     * </dl>
     */

  // No constructors or destructors on this pure virtual base class
  // --------------------------------------------------------------
};

//------------------------------------------------------------------

CATDeclareHandler( CATIMldSettingPlate, CATIMldPlate );

#endif
