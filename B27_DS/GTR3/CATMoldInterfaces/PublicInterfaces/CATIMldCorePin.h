// COPYRIGHT Dassault Systemes 2000

/**
  * @CAA2Level L0
  * @CAA2Usage U3
  */

//===================================================================
//
// CATIMldCorePin.h
// Define the CATIMldCorePin interface
//
//===================================================================
//
// Usage notes:
//   New interface: describe its use here
//
//===================================================================
//
//  Oct 2000  Creation: Code generated by the CAA wizard  lch
//===================================================================
#ifndef CATIMldCorePin_H
#define CATIMldCorePin_H

#include "CATMoldItfCPP.h"
#include "CATIMldComponent.h"

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByCATMoldItfCPP IID IID_CATIMldCorePin ;
#else
extern "C" const IID IID_CATIMldCorePin ;
#endif

//------------------------------------------------------------------

/**
 * Describe the functionality of your interface here
 * <p>
 * Using this prefered syntax will enable mkdoc to document your class.
 */
class ExportedByCATMoldItfCPP CATIMldCorePin: public CATIMldComponent
{
  CATDeclareInterface;

  public:

    /**
     * Remember that interfaces define only virtual pure methods.
     * Dont forget to document your methods.
     * <dl>
     * <dt><b>Example:</b>
     * <pre>
     *       
           *   MyFunction does this and that
           *   @param Arg1
           *      The first argument of MyFunction.
           *   @return
           *      Error code of function.
           * 
     * virtual int MyFunction (int Arg1) = 0;
     * </pre>
     * </dl>
     */

  // No constructors or destructors on this pure virtual base class
  // --------------------------------------------------------------
};

//------------------------------------------------------------------

CATDeclareHandler( CATIMldCorePin, CATIMldComponent );

#endif
