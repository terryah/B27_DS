#=====================================================================================
#                                     CNEXT - CXR4+
#                          COPYRIGHT DASSAULT SYSTEMES 2000+ 
#-------------------------------------------------------------------------------------
# MODULE      :    CATRpmReporterTypeLib
# FRAMEWORK   :    CATRpmReporterInterfaces
# AUTHOR      :    VSO
# DATE        :    9.2001
#-------------------------------------------------------------------------------------
# DESCRIPTION :    
#-------------------------------------------------------------------------------------
# COMMENTS    :    
#-------------------------------------------------------------------------------------
# MODIFICATIONS     user  date        purpose
#    HISTORY        ----  ----        -------
#
#=====================================================================================


BUILT_OBJECT_TYPE=TYPELIB
# no more BUILD_PRIORITY=50
LINK_WITH = InfTypeLib
#BUILD_PRIORITY must be a bigger number than InfItf(0)

