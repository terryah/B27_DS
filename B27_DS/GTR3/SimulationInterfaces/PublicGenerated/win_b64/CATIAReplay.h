/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAReplay_h
#define CATIAReplay_h

#ifndef ExportedBySimulationPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __SimulationPubIDL
#define ExportedBySimulationPubIDL __declspec(dllexport)
#else
#define ExportedBySimulationPubIDL __declspec(dllimport)
#endif
#else
#define ExportedBySimulationPubIDL
#endif
#endif

#include "CATIABase.h"
#include "CATSafeArray.h"

class CATIAProduct;

extern ExportedBySimulationPubIDL IID IID_CATIAReplay;

class ExportedBySimulationPubIDL CATIAReplay : public CATIABase
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall AddProductMotion(CATIAProduct * iProduct, CATLONG & oChannel)=0;

    virtual HRESULT __stdcall AddSample(CATLONG iChannel, double iCurrentTime, const CATSafeArrayVariant & iPosition)=0;

    virtual HRESULT __stdcall GetNbProductMotion(CATLONG & oNbChannel)=0;

    virtual HRESULT __stdcall GetProduct(CATLONG iChannel, CATIAProduct *& oProduct)=0;

    virtual HRESULT __stdcall GetNbSample(CATLONG iChannel, CATLONG & oNbSample)=0;

    virtual HRESULT __stdcall GetSampleTime(CATLONG iChannel, CATLONG iSample, double & oTime)=0;

    virtual HRESULT __stdcall GetSamplePosition(CATLONG iChannel, CATLONG iSample, CATSafeArrayVariant & oPosition)=0;

    virtual HRESULT __stdcall RemoveSample(CATLONG iChannel, CATLONG iSample)=0;


};

CATDeclareHandler(CATIAReplay, CATIABase);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIAAnalyze.h"
#include "CATIACollection.h"
#include "CATIAMove.h"
#include "CATIAParameters.h"
#include "CATIAPosition.h"
#include "CATIAProduct.h"
#include "CATIAPublications.h"
#include "CATVariant.h"
#include "CatFileType.h"
#include "CatProductSource.h"
#include "CatRepType.h"
#include "CatWorkModeType.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
