#include "CATIACGMLevel.h"
#ifdef CATIACGMR420CAA
#ifdef _STATIC_SOURCE
#define	ExportedByY30C3XGG
#elif defined __Y30C3XGG


// COPYRIGHT DASSAULT SYSTEMES 1999

/** @CAA2Required */

/*---------------------------------------------------------------------*/

/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */

/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */

/*---------------------------------------------------------------------*/
#define ExportedByY30C3XGG DSYExport
#else
#define ExportedByY30C3XGG DSYImport
#endif
#include "DSYExport.h"
#else
#ifdef _STATIC_SOURCE
#define	ExportedByY30C3XGG
#elif defined _WINDOWS_SOURCE
#ifdef	__Y30C3XGG

// COPYRIGHT DASSAULT SYSTEMES 1999
/** @CAA2Required */
/*---------------------------------------------------------------------*/
/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */
/*---------------------------------------------------------------------*/
#define	ExportedByY30C3XGG	__declspec(dllexport)
#else
#define	ExportedByY30C3XGG	__declspec(dllimport)
#endif
#else
#define	ExportedByY30C3XGG
#endif
#endif
#include <GeometricOperatorsCommonDec.h>
