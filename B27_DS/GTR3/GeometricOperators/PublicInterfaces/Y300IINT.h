#include "CATIACGMLevel.h"
#ifdef CATIACGMR420CAA
#ifdef _STATIC_SOURCE
#define	ExportedByY300IINT
#elif defined __Y300IINT


// COPYRIGHT DASSAULT SYSTEMES 1999

/** @CAA2Required */
#define ExportedByY300IINT DSYExport
#else
#define ExportedByY300IINT DSYImport
#endif
#include "DSYExport.h"
#else
#ifdef _STATIC_SOURCE
#define	ExportedByY300IINT
#elif defined _WINDOWS_SOURCE
#ifdef	__Y300IINT

// COPYRIGHT DASSAULT SYSTEMES 1999
/** @CAA2Required */
#define	ExportedByY300IINT	__declspec(dllexport)
#else
#define	ExportedByY300IINT	__declspec(dllimport)
#endif
#else
#define	ExportedByY300IINT
#endif
#endif
#include <GeometricOperatorsCommonDec.h>
