#include "CATIACGMLevel.h"
// COPYRIGHT DASSAULT SYSTEMES 2003
#ifdef CATIACGMR420CAA
#ifdef _STATIC_SOURCE
#define	ExportedByGeoNurbsTools
#elif defined __GeoNurbsTools


/** @CAA2Required */

//**********************************************************************

//* DON T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS *

//* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPEAR AT ANY TIME *

//**********************************************************************
#define ExportedByGeoNurbsTools DSYExport
#else
#define ExportedByGeoNurbsTools DSYImport
#endif
#include "DSYExport.h"
#else
#ifdef _STATIC_SOURCE
#define	ExportedByGeoNurbsTools
#elif defined _WINDOWS_SOURCE
#ifdef	__GeoNurbsTools
#define	ExportedByGeoNurbsTools	__declspec(dllexport)
/** @CAA2Required */
//**********************************************************************
//* DON T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS *
//* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPEAR AT ANY TIME *
//**********************************************************************
#else
#define	ExportedByGeoNurbsTools	__declspec(dllimport)
#endif
#else
#define	ExportedByGeoNurbsTools
#endif
#endif
#include <GeometricOperatorsCommonDec.h>
