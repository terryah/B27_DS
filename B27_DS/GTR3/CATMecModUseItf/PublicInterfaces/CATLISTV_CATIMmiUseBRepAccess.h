
#ifndef CATLISTV_CATIMmiUseBRepAccess_H 
#define CATLISTV_CATIMmiUseBRepAccess_H

// COPYRIGHT DASSAULT SYSTEMES 2009

/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */

/**
 * @collection CATLISTV(CATIMmiUseBRepAccess_var)
 * Collection class for specobjects.
 * All the methods of handlers collection classes are available.
 * Refer to the articles dealing with collections in the encyclopedia.
 */

#include "CATLISTHand_Clean.h"

#include "CATLISTHand_AllFunct.h"

#include "CATLISTHand_Declare.h"

#undef  CATCOLLEC_ExportedBy
#define	CATCOLLEC_ExportedBy ExportedByCATMecModUseItf

#include "CATIMmiUseBRepAccess.h"
CATLISTHand_DECLARE(CATIMmiUseBRepAccess_var)

#endif // CATLISTV_CATIMmiUseBRepAccess_H
