#ifndef CATListOfCATIMmiUseBRepAccess_h 
#define CATListOfCATIMmiUseBRepAccess_h

// COPYRIGHT DASSAULT SYSTEMES 2009

/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */
 
#include "CATLISTP_Clean.h"
#include "CATLISTP_PublicInterface.h"
#include "CATLISTP_Declare.h"
#include "CATMecModUseItf.h"

/**
 * @collection CATLISTP(CATIMmiUseBRepAccess)
 * Collection class for pointer to CATIMmiUseBRepAccess interface.
 * All the methods of pointer collection classes are available.<br>
 * Refer to the articles dealing with collections in the encyclopedia.
 */
class CATIMmiUseBRepAccess ;

#undef	CATCOLLEC_ExportedBy
#define	CATCOLLEC_ExportedBy	ExportedByCATMecModUseItf

CATLISTP_DECLARE( CATIMmiUseBRepAccess )

#endif


