// COPYRIGHT DASSAULT SYSTEMES  2009
/** @CAA2Required */
//**********************************************************************
//* DON T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS *
//* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPEAR AT ANY TIME *
//**********************************************************************

#ifndef __CATMecModUseItf_h__ 
#define __CATMecModUseItf_h__
#ifdef _WINDOWS_SOURCE
#ifdef  __CATMecModUseItf
#define ExportedByCATMecModUseItf    __declspec(dllexport)
#else
#define ExportedByCATMecModUseItf    __declspec(dllimport)
#endif
#else
#define ExportedByCATMecModUseItf
#endif

#endif

