/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2006
//==============================================================================

#if DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef DNBIgpMoveEvent_H
#define DNBIgpMoveEvent_H

//DNBSimulationControl
#include <DNBTimelet.h>
//DNBSimulationBase
#include <DNBMoveEvent.h>
//DNBRobotModel
#include <DNBBasicRobot3D.h>
//DNBWorldModel
#include <DNBBasicTagPoint3D.h>
//System
#include "CATListOfInt.h"
#include "CATListOfDouble.h"
#include <CATUnicodeString.h>
//DNBIgripSimBase
#include <DNBIgripSimBase.h>

// Event for device move

class ExportedByDNBIgripSimBase
DNBIgpMoveEvent : public DNBMoveEvent
{
    DNB_DECLARE_SIMEVENT( DNBIgpMoveEvent);

public:
    DNBIgpMoveEvent();
    DNBIgpMoveEvent( const DNBIgpMoveEvent &right, CopyMode mode );

    virtual ~DNBIgpMoveEvent()
    DNB_THROW_SPEC_NULL;

/**
    * Get rounding value.
    * @return double
    *   value used for rounding
    */
    double
    getRounding()
    DNB_THROW_SPEC_NULL;

    /**
    * Get speed for the move.
    * @return double
    *   speed value
    */
    double
    getSpeed()
    DNB_THROW_SPEC_NULL;

    /**
    * Get acceleration for the move.
    * @return double
    *   acceleration value
    */
    double
    getAccel()
    DNB_THROW_SPEC_NULL;

    		
    /**
    * Get motion type(JOINT/STRAIGHT/CIRCULAR/CIRCULARVIA/SLEW).
    * @return int
    *   enum representing the motion type
    */
    int
    getMotionType()
    DNB_THROW_SPEC_NULL;

    /**
    * Get Configuration(or posture)
    * @return int
    */
    int
    getConfig()
    DNB_THROW_SPEC_NULL;

    /**
    * Get turn mode ( DNBTurnNumber or DNBTurnSign or DNBSolutionAngle )
    * @return DNBBasicRobot3D::TurnMode
    */
    DNBBasicRobot3D::TurnMode
    getTurnMode()
    DNB_THROW_SPEC_NULL;

    /**
    * Get turn numbers
    * @return CATListOfInt
    */
    CATListOfInt
    getTurnNumbers()
    DNB_THROW_SPEC_NULL;
    
    /**
    * Get turn signs
    * @return CATListOfInt
    */
    CATListOfInt
    getTurnSign()
    DNB_THROW_SPEC_NULL;

    /**
    * Get motion basis (MOTION_ABSOLUTE,MOTION_PERCENT,MOTION_TIME)
    * @return int
    *   enum representing the Motion Basis.
    */
    int
    getMotionBasis()
    DNB_THROW_SPEC_NULL;

    /**
    * Get WDM handle to the tag
    * @return DNBBasicTagPoint3D::Handle
    *   tag WDM handle
    */
    DNBBasicTagPoint3D::Handle
    getTag()
    DNB_THROW_SPEC_NULL;

    /**
    * Get WDM handle to device
    * @return DNBBasicDevice3D::Handle
    *   device  WDM handle
    */
    DNBBasicDevice3D::Handle
    getDevice()
    DNB_THROW_SPEC_NULL;

    /**
    * Get target type
    * @return int
    */
    int
    getTargetType()
    DNB_THROW_SPEC_NULL;

    /**
    * Set rounding value.
    * @param double
    *   double value for rounding.
    */
    void
    setRounding(double rounding)
    DNB_THROW_SPEC_NULL;

    /**
    * Set speed for the motion.
    * @param double
    *   double value for speed.
    */
    void
    setSpeed(double speed)
    DNB_THROW_SPEC_NULL;

    /**
    * Set acceleration  for the motion.
    * @param double
    *   double value for speed.
    */
    void
    setAccel(double accel)
    DNB_THROW_SPEC_NULL;

    /**
    * Set motion type(JOINT/STRAIGHT/CIRCULAR/CIRCULARVIA/SLEW).
    * @param int
    *   representing the Motion Type.
    */
    void
    setMotionType(int motionType)
    DNB_THROW_SPEC_NULL;

    /**
    * Set configuration(or posture)
    * @param int
    */
    void
    setConfig(int config)
    DNB_THROW_SPEC_NULL;

    /**
    * Set turn mode ( DNBTurnNumber or DNBTurnSign or DNBSolutionAngle )
    * @param DNBBasicRobot3D::TurnMode
    */
    void
    setTurnMode(const DNBBasicRobot3D::TurnMode &iTurnMode)
    DNB_THROW_SPEC_NULL;

    /**
    * Set turn numbers
    * @param CATListOfInt
    */
    void
    setTurnNumbers(const CATListOfInt & iTurnNumbers)
    DNB_THROW_SPEC_NULL;

    /**
    * Set turn signs
    * @param CATListOfInt
    */
    void
    setTurnSign(const CATListOfInt & iTurnSign)
    DNB_THROW_SPEC_NULL;

    /**
    * Set motion basis (MOTION_ABSOLUTE,MOTION_PERCENT,MOTION_TIME)
    * @param int
    *   representing the Motion Basis.
    */
    void
    setMotionBasis(int motionBasis)
    DNB_THROW_SPEC_NULL;

    /**
    * Set tag
    * @param DNBBasicTagPoint3D::Handle
    *   tag WDM handle
    */
    void
    setTag(DNBBasicTagPoint3D::Handle tag)
    DNB_THROW_SPEC_NULL;

    /**
    * Set device
    * @param DNBBasicDevice3D::Handle
    *   device WDM handle
    */
    void
    setDevice(DNBBasicDevice3D::Handle hObj)
    DNB_THROW_SPEC_NULL;

    /**
    * Set target type
    * @param int
    */
    void setTargetType(int targetType)
    DNB_THROW_SPEC_NULL;

    /** 
    * @nodoc
    */
    CATUnicodeString getTagReference()
    DNB_THROW_SPEC_NULL;

    /** 
    * @nodoc
    */
    CATListOfDouble getRef2PartXYZWPR()
    DNB_THROW_SPEC_NULL;

    /** 
    * @nodoc
    */
    void setTagReference( CATUnicodeString tagReference )
    DNB_THROW_SPEC_NULL;

    /** 
    * @nodoc
    */
    void setRef2PartXYZWPR( CATListOfDouble XYZWPR )
    DNB_THROW_SPEC_NULL;

private:
    double						_rounding;
    double						_speed;
    double						_accel;
    int							_motype;
    int							_config;
    int							_motionBasis;
    int                         _targetType;
    DNBBasicTagPoint3D::Handle	_hTag;
    DNBBasicDevice3D::Handle    _hDevice;
    CATUnicodeString            _tagReference;
    CATListOfDouble             _XYZWPR;
    CATListOfInt                _turnNumber;
    CATListOfInt                _turnSign;
    DNBBasicRobot3D::TurnMode   _turnMode;
};


#endif
