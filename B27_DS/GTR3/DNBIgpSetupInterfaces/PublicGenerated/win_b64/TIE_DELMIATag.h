#ifndef __TIE_DELMIATag
#define __TIE_DELMIATag

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DELMIATag.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DELMIATag */
#define declare_TIE_DELMIATag(classe) \
 \
 \
class TIEDELMIATag##classe : public DELMIATag \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DELMIATag, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall GetName(CATBSTR & oTagName); \
      virtual HRESULT __stdcall SetName(const CATBSTR & iTagName); \
      virtual HRESULT __stdcall GetType(CATBSTR & oFrameName); \
      virtual HRESULT __stdcall SetType(const CATBSTR & iFrameName); \
      virtual HRESULT __stdcall SetXYZ(double iX, double iY, double iZ); \
      virtual HRESULT __stdcall GetXYZ(CATSafeArrayVariant & ioXYZ); \
      virtual HRESULT __stdcall SetYPR(double iY, double iP, double iR); \
      virtual HRESULT __stdcall GetYPR(CATSafeArrayVariant & ioYPR); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DELMIATag(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall GetName(CATBSTR & oTagName); \
virtual HRESULT __stdcall SetName(const CATBSTR & iTagName); \
virtual HRESULT __stdcall GetType(CATBSTR & oFrameName); \
virtual HRESULT __stdcall SetType(const CATBSTR & iFrameName); \
virtual HRESULT __stdcall SetXYZ(double iX, double iY, double iZ); \
virtual HRESULT __stdcall GetXYZ(CATSafeArrayVariant & ioXYZ); \
virtual HRESULT __stdcall SetYPR(double iY, double iP, double iR); \
virtual HRESULT __stdcall GetYPR(CATSafeArrayVariant & ioYPR); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DELMIATag(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::GetName(CATBSTR & oTagName) \
{ \
return (ENVTIECALL(DELMIATag,ENVTIETypeLetter,ENVTIELetter)GetName(oTagName)); \
} \
HRESULT __stdcall  ENVTIEName::SetName(const CATBSTR & iTagName) \
{ \
return (ENVTIECALL(DELMIATag,ENVTIETypeLetter,ENVTIELetter)SetName(iTagName)); \
} \
HRESULT __stdcall  ENVTIEName::GetType(CATBSTR & oFrameName) \
{ \
return (ENVTIECALL(DELMIATag,ENVTIETypeLetter,ENVTIELetter)GetType(oFrameName)); \
} \
HRESULT __stdcall  ENVTIEName::SetType(const CATBSTR & iFrameName) \
{ \
return (ENVTIECALL(DELMIATag,ENVTIETypeLetter,ENVTIELetter)SetType(iFrameName)); \
} \
HRESULT __stdcall  ENVTIEName::SetXYZ(double iX, double iY, double iZ) \
{ \
return (ENVTIECALL(DELMIATag,ENVTIETypeLetter,ENVTIELetter)SetXYZ(iX,iY,iZ)); \
} \
HRESULT __stdcall  ENVTIEName::GetXYZ(CATSafeArrayVariant & ioXYZ) \
{ \
return (ENVTIECALL(DELMIATag,ENVTIETypeLetter,ENVTIELetter)GetXYZ(ioXYZ)); \
} \
HRESULT __stdcall  ENVTIEName::SetYPR(double iY, double iP, double iR) \
{ \
return (ENVTIECALL(DELMIATag,ENVTIETypeLetter,ENVTIELetter)SetYPR(iY,iP,iR)); \
} \
HRESULT __stdcall  ENVTIEName::GetYPR(CATSafeArrayVariant & ioYPR) \
{ \
return (ENVTIECALL(DELMIATag,ENVTIETypeLetter,ENVTIELetter)GetYPR(ioYPR)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DELMIATag,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DELMIATag,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DELMIATag,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DELMIATag,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DELMIATag,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DELMIATag(classe)    TIEDELMIATag##classe


/* Common methods inside a TIE */
#define common_TIE_DELMIATag(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DELMIATag, classe) \
 \
 \
CATImplementTIEMethods(DELMIATag, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DELMIATag, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DELMIATag, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DELMIATag, classe) \
 \
HRESULT __stdcall  TIEDELMIATag##classe::GetName(CATBSTR & oTagName) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&oTagName); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetName(oTagName); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&oTagName); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIATag##classe::SetName(const CATBSTR & iTagName) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&iTagName); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetName(iTagName); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&iTagName); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIATag##classe::GetType(CATBSTR & oFrameName) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&oFrameName); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetType(oFrameName); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&oFrameName); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIATag##classe::SetType(const CATBSTR & iFrameName) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&iFrameName); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetType(iFrameName); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&iFrameName); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIATag##classe::SetXYZ(double iX, double iY, double iZ) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&iX,&iY,&iZ); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetXYZ(iX,iY,iZ); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&iX,&iY,&iZ); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIATag##classe::GetXYZ(CATSafeArrayVariant & ioXYZ) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&ioXYZ); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetXYZ(ioXYZ); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&ioXYZ); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIATag##classe::SetYPR(double iY, double iP, double iR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&iY,&iP,&iR); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetYPR(iY,iP,iR); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&iY,&iP,&iR); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIATag##classe::GetYPR(CATSafeArrayVariant & ioYPR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&ioYPR); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetYPR(ioYPR); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&ioYPR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIATag##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIATag##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIATag##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,11,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,11,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIATag##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,12,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,12,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIATag##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,13,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,13,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DELMIATag(classe) \
 \
 \
declare_TIE_DELMIATag(classe) \
 \
 \
CATMetaClass * __stdcall TIEDELMIATag##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DELMIATag,"DELMIATag",DELMIATag::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DELMIATag(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DELMIATag, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDELMIATag##classe(classe::MetaObject(),DELMIATag::MetaObject(),(void *)CreateTIEDELMIATag##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DELMIATag(classe) \
 \
 \
declare_TIE_DELMIATag(classe) \
 \
 \
CATMetaClass * __stdcall TIEDELMIATag##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DELMIATag,"DELMIATag",DELMIATag::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DELMIATag(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DELMIATag, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDELMIATag##classe(classe::MetaObject(),DELMIATag::MetaObject(),(void *)CreateTIEDELMIATag##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DELMIATag(classe) TIE_DELMIATag(classe)
#else
#define BOA_DELMIATag(classe) CATImplementBOA(DELMIATag, classe)
#endif

#endif
