#ifndef __TIE_DELMIADeviceTaskFactory
#define __TIE_DELMIADeviceTaskFactory

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DELMIADeviceTaskFactory.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DELMIADeviceTaskFactory */
#define declare_TIE_DELMIADeviceTaskFactory(classe) \
 \
 \
class TIEDELMIADeviceTaskFactory##classe : public DELMIADeviceTaskFactory \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DELMIADeviceTaskFactory, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall CreateDeviceTask(const CATBSTR & iName, DELMIADeviceTask *& oDeviceTask); \
      virtual HRESULT __stdcall GetAllDeviceTasks(CATSafeArrayVariant & oRobotTaskList); \
      virtual HRESULT __stdcall DeleteDeviceTask(DELMIADeviceTask * iDeviceTask); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DELMIADeviceTaskFactory(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall CreateDeviceTask(const CATBSTR & iName, DELMIADeviceTask *& oDeviceTask); \
virtual HRESULT __stdcall GetAllDeviceTasks(CATSafeArrayVariant & oRobotTaskList); \
virtual HRESULT __stdcall DeleteDeviceTask(DELMIADeviceTask * iDeviceTask); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DELMIADeviceTaskFactory(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::CreateDeviceTask(const CATBSTR & iName, DELMIADeviceTask *& oDeviceTask) \
{ \
return (ENVTIECALL(DELMIADeviceTaskFactory,ENVTIETypeLetter,ENVTIELetter)CreateDeviceTask(iName,oDeviceTask)); \
} \
HRESULT __stdcall  ENVTIEName::GetAllDeviceTasks(CATSafeArrayVariant & oRobotTaskList) \
{ \
return (ENVTIECALL(DELMIADeviceTaskFactory,ENVTIETypeLetter,ENVTIELetter)GetAllDeviceTasks(oRobotTaskList)); \
} \
HRESULT __stdcall  ENVTIEName::DeleteDeviceTask(DELMIADeviceTask * iDeviceTask) \
{ \
return (ENVTIECALL(DELMIADeviceTaskFactory,ENVTIETypeLetter,ENVTIELetter)DeleteDeviceTask(iDeviceTask)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DELMIADeviceTaskFactory,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DELMIADeviceTaskFactory,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DELMIADeviceTaskFactory,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DELMIADeviceTaskFactory,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DELMIADeviceTaskFactory,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DELMIADeviceTaskFactory(classe)    TIEDELMIADeviceTaskFactory##classe


/* Common methods inside a TIE */
#define common_TIE_DELMIADeviceTaskFactory(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DELMIADeviceTaskFactory, classe) \
 \
 \
CATImplementTIEMethods(DELMIADeviceTaskFactory, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DELMIADeviceTaskFactory, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DELMIADeviceTaskFactory, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DELMIADeviceTaskFactory, classe) \
 \
HRESULT __stdcall  TIEDELMIADeviceTaskFactory##classe::CreateDeviceTask(const CATBSTR & iName, DELMIADeviceTask *& oDeviceTask) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&iName,&oDeviceTask); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateDeviceTask(iName,oDeviceTask); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&iName,&oDeviceTask); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIADeviceTaskFactory##classe::GetAllDeviceTasks(CATSafeArrayVariant & oRobotTaskList) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&oRobotTaskList); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAllDeviceTasks(oRobotTaskList); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&oRobotTaskList); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIADeviceTaskFactory##classe::DeleteDeviceTask(DELMIADeviceTask * iDeviceTask) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&iDeviceTask); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->DeleteDeviceTask(iDeviceTask); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&iDeviceTask); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIADeviceTaskFactory##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIADeviceTaskFactory##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIADeviceTaskFactory##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIADeviceTaskFactory##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIADeviceTaskFactory##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DELMIADeviceTaskFactory(classe) \
 \
 \
declare_TIE_DELMIADeviceTaskFactory(classe) \
 \
 \
CATMetaClass * __stdcall TIEDELMIADeviceTaskFactory##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DELMIADeviceTaskFactory,"DELMIADeviceTaskFactory",DELMIADeviceTaskFactory::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DELMIADeviceTaskFactory(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DELMIADeviceTaskFactory, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDELMIADeviceTaskFactory##classe(classe::MetaObject(),DELMIADeviceTaskFactory::MetaObject(),(void *)CreateTIEDELMIADeviceTaskFactory##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DELMIADeviceTaskFactory(classe) \
 \
 \
declare_TIE_DELMIADeviceTaskFactory(classe) \
 \
 \
CATMetaClass * __stdcall TIEDELMIADeviceTaskFactory##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DELMIADeviceTaskFactory,"DELMIADeviceTaskFactory",DELMIADeviceTaskFactory::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DELMIADeviceTaskFactory(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DELMIADeviceTaskFactory, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDELMIADeviceTaskFactory##classe(classe::MetaObject(),DELMIADeviceTaskFactory::MetaObject(),(void *)CreateTIEDELMIADeviceTaskFactory##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DELMIADeviceTaskFactory(classe) TIE_DELMIADeviceTaskFactory(classe)
#else
#define BOA_DELMIADeviceTaskFactory(classe) CATImplementBOA(DELMIADeviceTaskFactory, classe)
#endif

#endif
