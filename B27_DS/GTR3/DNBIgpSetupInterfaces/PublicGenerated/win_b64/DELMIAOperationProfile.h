/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef DELMIAOperationProfile_h
#define DELMIAOperationProfile_h

#ifndef ExportedByDNBIgpSetupPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __DNBIgpSetupPubIDL
#define ExportedByDNBIgpSetupPubIDL __declspec(dllexport)
#else
#define ExportedByDNBIgpSetupPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByDNBIgpSetupPubIDL
#endif
#endif

#include "CATBSTR.h"
#include "CATIAActivity.h"
#include "CATSafeArray.h"

extern ExportedByDNBIgpSetupPubIDL IID IID_DELMIAOperationProfile;

class ExportedByDNBIgpSetupPubIDL DELMIAOperationProfile : public CATIAActivity
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall GetUserProfilesList(const CATBSTR & iProfileType, CATSafeArrayVariant *& oProfiles)=0;

    virtual HRESULT __stdcall SetUserProfilesList(const CATSafeArrayVariant & iUserProfiles)=0;


};

CATDeclareHandler(DELMIAOperationProfile, CATIAActivity);

#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIABase.h"
#include "CATIACollection.h"
#include "CATIAItems.h"
#include "CATIAOutputs.h"
#include "CATIARelations.h"
#include "CATSPPDeclarations.h"
#include "CATVariant.h"
#include "DNBItemAssignmentType.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
