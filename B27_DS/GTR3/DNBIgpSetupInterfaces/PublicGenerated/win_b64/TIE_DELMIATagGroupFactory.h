#ifndef __TIE_DELMIATagGroupFactory
#define __TIE_DELMIATagGroupFactory

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DELMIATagGroupFactory.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DELMIATagGroupFactory */
#define declare_TIE_DELMIATagGroupFactory(classe) \
 \
 \
class TIEDELMIATagGroupFactory##classe : public DELMIATagGroupFactory \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DELMIATagGroupFactory, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall CreateTagGroup(const CATBSTR & iTagGroupName, CAT_VARIANT_BOOL iModifyReference, CATIAProduct * iProduct, DELMIATagGroup *& oTagGroup); \
      virtual HRESULT __stdcall CreateTagGroupInDoc(const CATBSTR & iTagGroupName, DELMIATagGroup *& oTagGroup); \
      virtual HRESULT __stdcall DeleteTagGroup(DELMIATagGroup * iTagGroup); \
      virtual HRESULT __stdcall ReparentTagGroup(DELMIATagGroup * iTagGroup, CATIAProduct *& ioProduct, CAT_VARIANT_BOOL iModifyReference); \
      virtual HRESULT __stdcall CopyTagGroup(DELMIATagGroup * iTagGroup, DELMIATagGroup *& ioTagGroup); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DELMIATagGroupFactory(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall CreateTagGroup(const CATBSTR & iTagGroupName, CAT_VARIANT_BOOL iModifyReference, CATIAProduct * iProduct, DELMIATagGroup *& oTagGroup); \
virtual HRESULT __stdcall CreateTagGroupInDoc(const CATBSTR & iTagGroupName, DELMIATagGroup *& oTagGroup); \
virtual HRESULT __stdcall DeleteTagGroup(DELMIATagGroup * iTagGroup); \
virtual HRESULT __stdcall ReparentTagGroup(DELMIATagGroup * iTagGroup, CATIAProduct *& ioProduct, CAT_VARIANT_BOOL iModifyReference); \
virtual HRESULT __stdcall CopyTagGroup(DELMIATagGroup * iTagGroup, DELMIATagGroup *& ioTagGroup); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DELMIATagGroupFactory(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::CreateTagGroup(const CATBSTR & iTagGroupName, CAT_VARIANT_BOOL iModifyReference, CATIAProduct * iProduct, DELMIATagGroup *& oTagGroup) \
{ \
return (ENVTIECALL(DELMIATagGroupFactory,ENVTIETypeLetter,ENVTIELetter)CreateTagGroup(iTagGroupName,iModifyReference,iProduct,oTagGroup)); \
} \
HRESULT __stdcall  ENVTIEName::CreateTagGroupInDoc(const CATBSTR & iTagGroupName, DELMIATagGroup *& oTagGroup) \
{ \
return (ENVTIECALL(DELMIATagGroupFactory,ENVTIETypeLetter,ENVTIELetter)CreateTagGroupInDoc(iTagGroupName,oTagGroup)); \
} \
HRESULT __stdcall  ENVTIEName::DeleteTagGroup(DELMIATagGroup * iTagGroup) \
{ \
return (ENVTIECALL(DELMIATagGroupFactory,ENVTIETypeLetter,ENVTIELetter)DeleteTagGroup(iTagGroup)); \
} \
HRESULT __stdcall  ENVTIEName::ReparentTagGroup(DELMIATagGroup * iTagGroup, CATIAProduct *& ioProduct, CAT_VARIANT_BOOL iModifyReference) \
{ \
return (ENVTIECALL(DELMIATagGroupFactory,ENVTIETypeLetter,ENVTIELetter)ReparentTagGroup(iTagGroup,ioProduct,iModifyReference)); \
} \
HRESULT __stdcall  ENVTIEName::CopyTagGroup(DELMIATagGroup * iTagGroup, DELMIATagGroup *& ioTagGroup) \
{ \
return (ENVTIECALL(DELMIATagGroupFactory,ENVTIETypeLetter,ENVTIELetter)CopyTagGroup(iTagGroup,ioTagGroup)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DELMIATagGroupFactory,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DELMIATagGroupFactory,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DELMIATagGroupFactory,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DELMIATagGroupFactory,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DELMIATagGroupFactory,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DELMIATagGroupFactory(classe)    TIEDELMIATagGroupFactory##classe


/* Common methods inside a TIE */
#define common_TIE_DELMIATagGroupFactory(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DELMIATagGroupFactory, classe) \
 \
 \
CATImplementTIEMethods(DELMIATagGroupFactory, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DELMIATagGroupFactory, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DELMIATagGroupFactory, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DELMIATagGroupFactory, classe) \
 \
HRESULT __stdcall  TIEDELMIATagGroupFactory##classe::CreateTagGroup(const CATBSTR & iTagGroupName, CAT_VARIANT_BOOL iModifyReference, CATIAProduct * iProduct, DELMIATagGroup *& oTagGroup) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&iTagGroupName,&iModifyReference,&iProduct,&oTagGroup); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateTagGroup(iTagGroupName,iModifyReference,iProduct,oTagGroup); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&iTagGroupName,&iModifyReference,&iProduct,&oTagGroup); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIATagGroupFactory##classe::CreateTagGroupInDoc(const CATBSTR & iTagGroupName, DELMIATagGroup *& oTagGroup) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&iTagGroupName,&oTagGroup); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateTagGroupInDoc(iTagGroupName,oTagGroup); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&iTagGroupName,&oTagGroup); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIATagGroupFactory##classe::DeleteTagGroup(DELMIATagGroup * iTagGroup) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&iTagGroup); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->DeleteTagGroup(iTagGroup); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&iTagGroup); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIATagGroupFactory##classe::ReparentTagGroup(DELMIATagGroup * iTagGroup, CATIAProduct *& ioProduct, CAT_VARIANT_BOOL iModifyReference) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&iTagGroup,&ioProduct,&iModifyReference); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ReparentTagGroup(iTagGroup,ioProduct,iModifyReference); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&iTagGroup,&ioProduct,&iModifyReference); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIATagGroupFactory##classe::CopyTagGroup(DELMIATagGroup * iTagGroup, DELMIATagGroup *& ioTagGroup) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&iTagGroup,&ioTagGroup); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CopyTagGroup(iTagGroup,ioTagGroup); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&iTagGroup,&ioTagGroup); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIATagGroupFactory##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIATagGroupFactory##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIATagGroupFactory##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIATagGroupFactory##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIATagGroupFactory##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DELMIATagGroupFactory(classe) \
 \
 \
declare_TIE_DELMIATagGroupFactory(classe) \
 \
 \
CATMetaClass * __stdcall TIEDELMIATagGroupFactory##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DELMIATagGroupFactory,"DELMIATagGroupFactory",DELMIATagGroupFactory::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DELMIATagGroupFactory(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DELMIATagGroupFactory, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDELMIATagGroupFactory##classe(classe::MetaObject(),DELMIATagGroupFactory::MetaObject(),(void *)CreateTIEDELMIATagGroupFactory##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DELMIATagGroupFactory(classe) \
 \
 \
declare_TIE_DELMIATagGroupFactory(classe) \
 \
 \
CATMetaClass * __stdcall TIEDELMIATagGroupFactory##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DELMIATagGroupFactory,"DELMIATagGroupFactory",DELMIATagGroupFactory::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DELMIATagGroupFactory(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DELMIATagGroupFactory, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDELMIATagGroupFactory##classe(classe::MetaObject(),DELMIATagGroupFactory::MetaObject(),(void *)CreateTIEDELMIATagGroupFactory##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DELMIATagGroupFactory(classe) TIE_DELMIATagGroupFactory(classe)
#else
#define BOA_DELMIATagGroupFactory(classe) CATImplementBOA(DELMIATagGroupFactory, classe)
#endif

#endif
