#ifndef __TIE_DELMIAMountManager
#define __TIE_DELMIAMountManager

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DELMIAMountManager.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DELMIAMountManager */
#define declare_TIE_DELMIAMountManager(classe) \
 \
 \
class TIEDELMIAMountManager##classe : public DELMIAMountManager \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DELMIAMountManager, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall MountDevice(CATIAProduct * iDevice, const CATBSTR & iToolProfileName, const CATSafeArrayVariant & iMountOffset, CAT_VARIANT_BOOL iToolMobility, DELMIATag * iToolTag, DELMIATag * iBaseTag); \
      virtual HRESULT __stdcall UnMountDevice(CATIAProduct * iDevice); \
      virtual HRESULT __stdcall ReMountDevice(CATIAProduct * iDevice); \
      virtual HRESULT __stdcall UnSetDevice(CATIAProduct * iDevice); \
      virtual HRESULT __stdcall GetMountedDevices(CATSafeArrayVariant & oDeviceList); \
      virtual HRESULT __stdcall IsDeviceMounted(CATIAProduct * iDevice, CAT_VARIANT_BOOL & iIsMounted); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DELMIAMountManager(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall MountDevice(CATIAProduct * iDevice, const CATBSTR & iToolProfileName, const CATSafeArrayVariant & iMountOffset, CAT_VARIANT_BOOL iToolMobility, DELMIATag * iToolTag, DELMIATag * iBaseTag); \
virtual HRESULT __stdcall UnMountDevice(CATIAProduct * iDevice); \
virtual HRESULT __stdcall ReMountDevice(CATIAProduct * iDevice); \
virtual HRESULT __stdcall UnSetDevice(CATIAProduct * iDevice); \
virtual HRESULT __stdcall GetMountedDevices(CATSafeArrayVariant & oDeviceList); \
virtual HRESULT __stdcall IsDeviceMounted(CATIAProduct * iDevice, CAT_VARIANT_BOOL & iIsMounted); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DELMIAMountManager(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::MountDevice(CATIAProduct * iDevice, const CATBSTR & iToolProfileName, const CATSafeArrayVariant & iMountOffset, CAT_VARIANT_BOOL iToolMobility, DELMIATag * iToolTag, DELMIATag * iBaseTag) \
{ \
return (ENVTIECALL(DELMIAMountManager,ENVTIETypeLetter,ENVTIELetter)MountDevice(iDevice,iToolProfileName,iMountOffset,iToolMobility,iToolTag,iBaseTag)); \
} \
HRESULT __stdcall  ENVTIEName::UnMountDevice(CATIAProduct * iDevice) \
{ \
return (ENVTIECALL(DELMIAMountManager,ENVTIETypeLetter,ENVTIELetter)UnMountDevice(iDevice)); \
} \
HRESULT __stdcall  ENVTIEName::ReMountDevice(CATIAProduct * iDevice) \
{ \
return (ENVTIECALL(DELMIAMountManager,ENVTIETypeLetter,ENVTIELetter)ReMountDevice(iDevice)); \
} \
HRESULT __stdcall  ENVTIEName::UnSetDevice(CATIAProduct * iDevice) \
{ \
return (ENVTIECALL(DELMIAMountManager,ENVTIETypeLetter,ENVTIELetter)UnSetDevice(iDevice)); \
} \
HRESULT __stdcall  ENVTIEName::GetMountedDevices(CATSafeArrayVariant & oDeviceList) \
{ \
return (ENVTIECALL(DELMIAMountManager,ENVTIETypeLetter,ENVTIELetter)GetMountedDevices(oDeviceList)); \
} \
HRESULT __stdcall  ENVTIEName::IsDeviceMounted(CATIAProduct * iDevice, CAT_VARIANT_BOOL & iIsMounted) \
{ \
return (ENVTIECALL(DELMIAMountManager,ENVTIETypeLetter,ENVTIELetter)IsDeviceMounted(iDevice,iIsMounted)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DELMIAMountManager,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DELMIAMountManager,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DELMIAMountManager,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DELMIAMountManager,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DELMIAMountManager,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DELMIAMountManager(classe)    TIEDELMIAMountManager##classe


/* Common methods inside a TIE */
#define common_TIE_DELMIAMountManager(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DELMIAMountManager, classe) \
 \
 \
CATImplementTIEMethods(DELMIAMountManager, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DELMIAMountManager, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DELMIAMountManager, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DELMIAMountManager, classe) \
 \
HRESULT __stdcall  TIEDELMIAMountManager##classe::MountDevice(CATIAProduct * iDevice, const CATBSTR & iToolProfileName, const CATSafeArrayVariant & iMountOffset, CAT_VARIANT_BOOL iToolMobility, DELMIATag * iToolTag, DELMIATag * iBaseTag) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&iDevice,&iToolProfileName,&iMountOffset,&iToolMobility,&iToolTag,&iBaseTag); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->MountDevice(iDevice,iToolProfileName,iMountOffset,iToolMobility,iToolTag,iBaseTag); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&iDevice,&iToolProfileName,&iMountOffset,&iToolMobility,&iToolTag,&iBaseTag); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAMountManager##classe::UnMountDevice(CATIAProduct * iDevice) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&iDevice); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->UnMountDevice(iDevice); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&iDevice); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAMountManager##classe::ReMountDevice(CATIAProduct * iDevice) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&iDevice); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ReMountDevice(iDevice); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&iDevice); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAMountManager##classe::UnSetDevice(CATIAProduct * iDevice) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&iDevice); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->UnSetDevice(iDevice); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&iDevice); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAMountManager##classe::GetMountedDevices(CATSafeArrayVariant & oDeviceList) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&oDeviceList); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetMountedDevices(oDeviceList); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&oDeviceList); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAMountManager##classe::IsDeviceMounted(CATIAProduct * iDevice, CAT_VARIANT_BOOL & iIsMounted) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&iDevice,&iIsMounted); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->IsDeviceMounted(iDevice,iIsMounted); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&iDevice,&iIsMounted); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAMountManager##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAMountManager##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAMountManager##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAMountManager##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAMountManager##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,11,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,11,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DELMIAMountManager(classe) \
 \
 \
declare_TIE_DELMIAMountManager(classe) \
 \
 \
CATMetaClass * __stdcall TIEDELMIAMountManager##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DELMIAMountManager,"DELMIAMountManager",DELMIAMountManager::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DELMIAMountManager(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DELMIAMountManager, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDELMIAMountManager##classe(classe::MetaObject(),DELMIAMountManager::MetaObject(),(void *)CreateTIEDELMIAMountManager##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DELMIAMountManager(classe) \
 \
 \
declare_TIE_DELMIAMountManager(classe) \
 \
 \
CATMetaClass * __stdcall TIEDELMIAMountManager##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DELMIAMountManager,"DELMIAMountManager",DELMIAMountManager::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DELMIAMountManager(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DELMIAMountManager, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDELMIAMountManager##classe(classe::MetaObject(),DELMIAMountManager::MetaObject(),(void *)CreateTIEDELMIAMountManager##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DELMIAMountManager(classe) TIE_DELMIAMountManager(classe)
#else
#define BOA_DELMIAMountManager(classe) CATImplementBOA(DELMIAMountManager, classe)
#endif

#endif
