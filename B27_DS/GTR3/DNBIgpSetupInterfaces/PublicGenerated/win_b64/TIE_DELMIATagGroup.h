#ifndef __TIE_DELMIATagGroup
#define __TIE_DELMIATagGroup

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DELMIATagGroup.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DELMIATagGroup */
#define declare_TIE_DELMIATagGroup(classe) \
 \
 \
class TIEDELMIATagGroup##classe : public DELMIATagGroup \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DELMIATagGroup, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall GetTag(short index, DELMIATag *& oTag); \
      virtual HRESULT __stdcall CreateTag(DELMIATag *& ioTag); \
      virtual HRESULT __stdcall GetTagList(CATSafeArrayVariant & ioTagList); \
      virtual HRESULT __stdcall GetOwner(CATBSTR & ioParentProduct); \
      virtual HRESULT __stdcall DeleteTag(DELMIATag * iTag); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & iIDName, CATBaseDispatch *& oObject); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oName); \
      virtual HRESULT  __stdcall get_Count(CATLONG & oNbItems); \
      virtual HRESULT  __stdcall get__NewEnum(IUnknown *& oEnumIter); \
};



#define ENVTIEdeclare_DELMIATagGroup(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall GetTag(short index, DELMIATag *& oTag); \
virtual HRESULT __stdcall CreateTag(DELMIATag *& ioTag); \
virtual HRESULT __stdcall GetTagList(CATSafeArrayVariant & ioTagList); \
virtual HRESULT __stdcall GetOwner(CATBSTR & ioParentProduct); \
virtual HRESULT __stdcall DeleteTag(DELMIATag * iTag); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & iIDName, CATBaseDispatch *& oObject); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oName); \
virtual HRESULT  __stdcall get_Count(CATLONG & oNbItems); \
virtual HRESULT  __stdcall get__NewEnum(IUnknown *& oEnumIter); \


#define ENVTIEdefine_DELMIATagGroup(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::GetTag(short index, DELMIATag *& oTag) \
{ \
return (ENVTIECALL(DELMIATagGroup,ENVTIETypeLetter,ENVTIELetter)GetTag(index,oTag)); \
} \
HRESULT __stdcall  ENVTIEName::CreateTag(DELMIATag *& ioTag) \
{ \
return (ENVTIECALL(DELMIATagGroup,ENVTIETypeLetter,ENVTIELetter)CreateTag(ioTag)); \
} \
HRESULT __stdcall  ENVTIEName::GetTagList(CATSafeArrayVariant & ioTagList) \
{ \
return (ENVTIECALL(DELMIATagGroup,ENVTIETypeLetter,ENVTIELetter)GetTagList(ioTagList)); \
} \
HRESULT __stdcall  ENVTIEName::GetOwner(CATBSTR & ioParentProduct) \
{ \
return (ENVTIECALL(DELMIATagGroup,ENVTIETypeLetter,ENVTIELetter)GetOwner(ioParentProduct)); \
} \
HRESULT __stdcall  ENVTIEName::DeleteTag(DELMIATag * iTag) \
{ \
return (ENVTIECALL(DELMIATagGroup,ENVTIETypeLetter,ENVTIELetter)DeleteTag(iTag)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DELMIATagGroup,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DELMIATagGroup,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & iIDName, CATBaseDispatch *& oObject) \
{ \
return (ENVTIECALL(DELMIATagGroup,ENVTIETypeLetter,ENVTIELetter)GetItem(iIDName,oObject)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oName) \
{ \
return (ENVTIECALL(DELMIATagGroup,ENVTIETypeLetter,ENVTIELetter)get_Name(oName)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Count(CATLONG & oNbItems) \
{ \
return (ENVTIECALL(DELMIATagGroup,ENVTIETypeLetter,ENVTIELetter)get_Count(oNbItems)); \
} \
HRESULT  __stdcall  ENVTIEName::get__NewEnum(IUnknown *& oEnumIter) \
{ \
return (ENVTIECALL(DELMIATagGroup,ENVTIETypeLetter,ENVTIELetter)get__NewEnum(oEnumIter)); \
} \


/* Name of the TIE class */
#define class_TIE_DELMIATagGroup(classe)    TIEDELMIATagGroup##classe


/* Common methods inside a TIE */
#define common_TIE_DELMIATagGroup(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DELMIATagGroup, classe) \
 \
 \
CATImplementTIEMethods(DELMIATagGroup, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DELMIATagGroup, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DELMIATagGroup, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DELMIATagGroup, classe) \
 \
HRESULT __stdcall  TIEDELMIATagGroup##classe::GetTag(short index, DELMIATag *& oTag) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&index,&oTag); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetTag(index,oTag); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&index,&oTag); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIATagGroup##classe::CreateTag(DELMIATag *& ioTag) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&ioTag); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateTag(ioTag); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&ioTag); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIATagGroup##classe::GetTagList(CATSafeArrayVariant & ioTagList) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&ioTagList); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetTagList(ioTagList); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&ioTagList); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIATagGroup##classe::GetOwner(CATBSTR & ioParentProduct) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&ioParentProduct); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetOwner(ioParentProduct); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&ioParentProduct); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIATagGroup##classe::DeleteTag(DELMIATag * iTag) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&iTag); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->DeleteTag(iTag); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&iTag); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIATagGroup##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIATagGroup##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIATagGroup##classe::GetItem(const CATBSTR & iIDName, CATBaseDispatch *& oObject) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&iIDName,&oObject); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(iIDName,oObject); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&iIDName,&oObject); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIATagGroup##classe::get_Name(CATBSTR & oName) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&oName); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oName); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&oName); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIATagGroup##classe::get_Count(CATLONG & oNbItems) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&oNbItems); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Count(oNbItems); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&oNbItems); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIATagGroup##classe::get__NewEnum(IUnknown *& oEnumIter) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,11,&_Trac2,&oEnumIter); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get__NewEnum(oEnumIter); \
   ExitAfterCall(this,11,_Trac2,&_ret_arg,&oEnumIter); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DELMIATagGroup(classe) \
 \
 \
declare_TIE_DELMIATagGroup(classe) \
 \
 \
CATMetaClass * __stdcall TIEDELMIATagGroup##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DELMIATagGroup,"DELMIATagGroup",DELMIATagGroup::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DELMIATagGroup(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DELMIATagGroup, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDELMIATagGroup##classe(classe::MetaObject(),DELMIATagGroup::MetaObject(),(void *)CreateTIEDELMIATagGroup##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DELMIATagGroup(classe) \
 \
 \
declare_TIE_DELMIATagGroup(classe) \
 \
 \
CATMetaClass * __stdcall TIEDELMIATagGroup##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DELMIATagGroup,"DELMIATagGroup",DELMIATagGroup::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DELMIATagGroup(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DELMIATagGroup, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDELMIATagGroup##classe(classe::MetaObject(),DELMIATagGroup::MetaObject(),(void *)CreateTIEDELMIATagGroup##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DELMIATagGroup(classe) TIE_DELMIATagGroup(classe)
#else
#define BOA_DELMIATagGroup(classe) CATImplementBOA(DELMIATagGroup, classe)
#endif

#endif
