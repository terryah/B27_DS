#ifndef __TIE_DELMIARobotTaskFactory
#define __TIE_DELMIARobotTaskFactory

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DELMIARobotTaskFactory.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DELMIARobotTaskFactory */
#define declare_TIE_DELMIARobotTaskFactory(classe) \
 \
 \
class TIEDELMIARobotTaskFactory##classe : public DELMIARobotTaskFactory \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DELMIARobotTaskFactory, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall CreateRobotTask(const CATBSTR & iName, DELMIARobotTask *& oRobotTask); \
      virtual HRESULT __stdcall GetAllRobotTasks(CATSafeArrayVariant & oRobotTaskList); \
      virtual HRESULT __stdcall DeleteRobotTask(DELMIARobotTask * iRobotTask); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DELMIARobotTaskFactory(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall CreateRobotTask(const CATBSTR & iName, DELMIARobotTask *& oRobotTask); \
virtual HRESULT __stdcall GetAllRobotTasks(CATSafeArrayVariant & oRobotTaskList); \
virtual HRESULT __stdcall DeleteRobotTask(DELMIARobotTask * iRobotTask); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DELMIARobotTaskFactory(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::CreateRobotTask(const CATBSTR & iName, DELMIARobotTask *& oRobotTask) \
{ \
return (ENVTIECALL(DELMIARobotTaskFactory,ENVTIETypeLetter,ENVTIELetter)CreateRobotTask(iName,oRobotTask)); \
} \
HRESULT __stdcall  ENVTIEName::GetAllRobotTasks(CATSafeArrayVariant & oRobotTaskList) \
{ \
return (ENVTIECALL(DELMIARobotTaskFactory,ENVTIETypeLetter,ENVTIELetter)GetAllRobotTasks(oRobotTaskList)); \
} \
HRESULT __stdcall  ENVTIEName::DeleteRobotTask(DELMIARobotTask * iRobotTask) \
{ \
return (ENVTIECALL(DELMIARobotTaskFactory,ENVTIETypeLetter,ENVTIELetter)DeleteRobotTask(iRobotTask)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DELMIARobotTaskFactory,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DELMIARobotTaskFactory,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DELMIARobotTaskFactory,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DELMIARobotTaskFactory,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DELMIARobotTaskFactory,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DELMIARobotTaskFactory(classe)    TIEDELMIARobotTaskFactory##classe


/* Common methods inside a TIE */
#define common_TIE_DELMIARobotTaskFactory(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DELMIARobotTaskFactory, classe) \
 \
 \
CATImplementTIEMethods(DELMIARobotTaskFactory, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DELMIARobotTaskFactory, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DELMIARobotTaskFactory, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DELMIARobotTaskFactory, classe) \
 \
HRESULT __stdcall  TIEDELMIARobotTaskFactory##classe::CreateRobotTask(const CATBSTR & iName, DELMIARobotTask *& oRobotTask) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&iName,&oRobotTask); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateRobotTask(iName,oRobotTask); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&iName,&oRobotTask); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotTaskFactory##classe::GetAllRobotTasks(CATSafeArrayVariant & oRobotTaskList) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&oRobotTaskList); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAllRobotTasks(oRobotTaskList); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&oRobotTaskList); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotTaskFactory##classe::DeleteRobotTask(DELMIARobotTask * iRobotTask) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&iRobotTask); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->DeleteRobotTask(iRobotTask); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&iRobotTask); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIARobotTaskFactory##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIARobotTaskFactory##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIARobotTaskFactory##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIARobotTaskFactory##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIARobotTaskFactory##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DELMIARobotTaskFactory(classe) \
 \
 \
declare_TIE_DELMIARobotTaskFactory(classe) \
 \
 \
CATMetaClass * __stdcall TIEDELMIARobotTaskFactory##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DELMIARobotTaskFactory,"DELMIARobotTaskFactory",DELMIARobotTaskFactory::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DELMIARobotTaskFactory(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DELMIARobotTaskFactory, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDELMIARobotTaskFactory##classe(classe::MetaObject(),DELMIARobotTaskFactory::MetaObject(),(void *)CreateTIEDELMIARobotTaskFactory##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DELMIARobotTaskFactory(classe) \
 \
 \
declare_TIE_DELMIARobotTaskFactory(classe) \
 \
 \
CATMetaClass * __stdcall TIEDELMIARobotTaskFactory##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DELMIARobotTaskFactory,"DELMIARobotTaskFactory",DELMIARobotTaskFactory::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DELMIARobotTaskFactory(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DELMIARobotTaskFactory, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDELMIARobotTaskFactory##classe(classe::MetaObject(),DELMIARobotTaskFactory::MetaObject(),(void *)CreateTIEDELMIARobotTaskFactory##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DELMIARobotTaskFactory(classe) TIE_DELMIARobotTaskFactory(classe)
#else
#define BOA_DELMIARobotTaskFactory(classe) CATImplementBOA(DELMIARobotTaskFactory, classe)
#endif

#endif
