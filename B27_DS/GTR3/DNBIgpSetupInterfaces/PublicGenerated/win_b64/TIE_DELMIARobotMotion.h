#ifndef __TIE_DELMIARobotMotion
#define __TIE_DELMIARobotMotion

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DELMIARobotMotion.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DELMIARobotMotion */
#define declare_TIE_DELMIARobotMotion(classe) \
 \
 \
class TIEDELMIARobotMotion##classe : public DELMIARobotMotion \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DELMIARobotMotion, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall SetMotionType(short index); \
      virtual HRESULT __stdcall GetMotionType(short & index); \
      virtual HRESULT __stdcall GetTargetType(short & index); \
      virtual HRESULT __stdcall SetCartesianTarget(const CATSafeArrayVariant & iCartesianTarget); \
      virtual HRESULT __stdcall GetCartesianTarget(CATSafeArrayVariant & oCartesianTarget); \
      virtual HRESULT __stdcall SetJointTarget(const CATSafeArrayVariant & iJointTarget); \
      virtual HRESULT __stdcall GetJointTarget(CATSafeArrayVariant & oJointTarget); \
      virtual HRESULT __stdcall SetTagTarget(DELMIATag * iTag); \
      virtual HRESULT __stdcall GetTagTarget(DELMIATag *& oTag); \
      virtual HRESULT __stdcall SetTurnNumbers(const CATSafeArrayVariant & iTurnNumbers); \
      virtual HRESULT __stdcall GetTurnNumbers(CATSafeArrayVariant & oTurnNumbers); \
      virtual HRESULT __stdcall SetAuxillaryAxisValues(CATIABase * iDevice, const CATBSTR & iAuxillaryAxis, const CATSafeArrayVariant & iAuxillaryAxisValues); \
      virtual HRESULT __stdcall GetAuxillaryAxisValues(CATIABase * iDevice, const CATBSTR & iAuxillaryAxis, CATSafeArrayVariant & oAuxillaryAxisValues); \
      virtual HRESULT __stdcall GetAuxillaryAxisHome(CATIABase * iDevice, const CATBSTR & iAuxillaryAxis, CATBSTR & oHomeName); \
      virtual HRESULT __stdcall SetAuxillaryAxisHome(CATIABase * iDevice, const CATBSTR & iAuxillaryAxis, const CATBSTR & iHomeName); \
      virtual HRESULT __stdcall SetConfig(short iConfigNumber); \
      virtual HRESULT __stdcall GetConfig(short & oConfigNumber); \
      virtual HRESULT __stdcall SetOrientationMode(short index); \
      virtual HRESULT __stdcall GetOrientationMode(short & index); \
      virtual HRESULT __stdcall GetToolProfile(CATIABase *& oToolProfile); \
      virtual HRESULT __stdcall SetToolProfile(CATIABase * iToolProfile); \
      virtual HRESULT __stdcall GetMotionProfile(CATIABase *& oMotionProfile); \
      virtual HRESULT __stdcall SetMotionProfile(CATIABase * iMotionProfile); \
      virtual HRESULT __stdcall GetObjectProfile(CATIABase *& oObjProfile); \
      virtual HRESULT __stdcall SetObjectProfile(CATIABase * iObjProfile); \
      virtual HRESULT __stdcall GetAccuracyProfile(CATIABase *& oAccuracyProfile); \
      virtual HRESULT __stdcall SetAccuracyProfile(CATIABase * iAccuracyProfile); \
      virtual HRESULT __stdcall IsSubTypeOf(const CATBSTR & iName, CAT_VARIANT_BOOL & oVal); \
      virtual HRESULT __stdcall AttrValue(const CATVariant & iIndex, CATVariant & oAttVal); \
      virtual HRESULT __stdcall AttrName(CATLONG iIndex, CATBSTR & oName); \
      virtual HRESULT __stdcall get_Type(CATBSTR & oType); \
      virtual HRESULT __stdcall get_Description(CATBSTR & oDescriptionBSTR); \
      virtual HRESULT __stdcall put_Description(const CATBSTR & iDescriptionBSTR); \
      virtual HRESULT __stdcall get_CycleTime(double & oCT); \
      virtual HRESULT __stdcall put_CycleTime(double iCT); \
      virtual HRESULT __stdcall get_CalculatedCycleTime(double & oCCT); \
      virtual HRESULT __stdcall get_BeginningDate(double & oBegin); \
      virtual HRESULT __stdcall put_BeginningDate(double iSBT); \
      virtual HRESULT __stdcall get_EndDate(double & oEnd); \
      virtual HRESULT __stdcall get_ChildrenActivities(CATIAActivities *& oChildren); \
      virtual HRESULT __stdcall CreateChild(const CATBSTR & iTypeOfChild, CATIAActivity *& oCreatedChild); \
      virtual HRESULT __stdcall CreateLink(CATIAActivity * iSecondActivity); \
      virtual HRESULT __stdcall RemoveLink(CATIAActivity * iSecondActivity); \
      virtual HRESULT __stdcall get_NextCFActivities(CATIAActivities *& oNextCF); \
      virtual HRESULT __stdcall get_PreviousCFActivities(CATIAActivities *& oPreviousCF); \
      virtual HRESULT __stdcall get_NextPRFActivities(CATIAActivities *& oNextPRF); \
      virtual HRESULT __stdcall get_PreviousPRFActivities(CATIAActivities *& oPreviousPRF); \
      virtual HRESULT __stdcall get_AttrCount(CATLONG & oNbAttr); \
      virtual HRESULT __stdcall get_Items(CATIAItems *& oItems); \
      virtual HRESULT __stdcall get_Outputs(CATIAOutputs *& oOutputs); \
      virtual HRESULT __stdcall get_Resources(CATIAResources *& oResources); \
      virtual HRESULT __stdcall get_Relations(CATIARelations *& oRelations); \
      virtual HRESULT __stdcall get_Parameters(CATIAParameters *& oParameters); \
      virtual HRESULT __stdcall GetTechnologicalObject(const CATBSTR & iApplicationType, CATBaseDispatch *& oApplicativeObj); \
      virtual HRESULT __stdcall get_PrecedenceActivities(CATIAActivities *& oActivities); \
      virtual HRESULT __stdcall get_PossiblePrecedenceActivities(CATIAActivities *& oActivities); \
      virtual HRESULT __stdcall get_ProcessID(CATBSTR & oProcessID); \
      virtual HRESULT __stdcall SetProcessID(const CATBSTR & iProcessID, CAT_VARIANT_BOOL iCheckUnique); \
      virtual HRESULT __stdcall get_CalculatedBeginTime(double & oCBT); \
      virtual HRESULT __stdcall AddAttr(const CATBSTR & iAttributeName, SPPProcessAttributeType AttrType, const CATBSTR & iAttributePromptName); \
      virtual HRESULT __stdcall RemoveAttr(const CATBSTR & iAttributeName); \
      virtual HRESULT __stdcall AddActivityConstraint(CATIAActivity * iActivity, SPPProcessConstraintType iConstraintType); \
      virtual HRESULT __stdcall RemoveActivityConstraint(CATIAActivity * iActivity, SPPProcessConstraintType iConstraintType); \
      virtual HRESULT __stdcall GetActivityConstraints(SPPProcessConstraintType iConstraintType, CATIAActivities *& oConstrtList); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DELMIARobotMotion(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall SetMotionType(short index); \
virtual HRESULT __stdcall GetMotionType(short & index); \
virtual HRESULT __stdcall GetTargetType(short & index); \
virtual HRESULT __stdcall SetCartesianTarget(const CATSafeArrayVariant & iCartesianTarget); \
virtual HRESULT __stdcall GetCartesianTarget(CATSafeArrayVariant & oCartesianTarget); \
virtual HRESULT __stdcall SetJointTarget(const CATSafeArrayVariant & iJointTarget); \
virtual HRESULT __stdcall GetJointTarget(CATSafeArrayVariant & oJointTarget); \
virtual HRESULT __stdcall SetTagTarget(DELMIATag * iTag); \
virtual HRESULT __stdcall GetTagTarget(DELMIATag *& oTag); \
virtual HRESULT __stdcall SetTurnNumbers(const CATSafeArrayVariant & iTurnNumbers); \
virtual HRESULT __stdcall GetTurnNumbers(CATSafeArrayVariant & oTurnNumbers); \
virtual HRESULT __stdcall SetAuxillaryAxisValues(CATIABase * iDevice, const CATBSTR & iAuxillaryAxis, const CATSafeArrayVariant & iAuxillaryAxisValues); \
virtual HRESULT __stdcall GetAuxillaryAxisValues(CATIABase * iDevice, const CATBSTR & iAuxillaryAxis, CATSafeArrayVariant & oAuxillaryAxisValues); \
virtual HRESULT __stdcall GetAuxillaryAxisHome(CATIABase * iDevice, const CATBSTR & iAuxillaryAxis, CATBSTR & oHomeName); \
virtual HRESULT __stdcall SetAuxillaryAxisHome(CATIABase * iDevice, const CATBSTR & iAuxillaryAxis, const CATBSTR & iHomeName); \
virtual HRESULT __stdcall SetConfig(short iConfigNumber); \
virtual HRESULT __stdcall GetConfig(short & oConfigNumber); \
virtual HRESULT __stdcall SetOrientationMode(short index); \
virtual HRESULT __stdcall GetOrientationMode(short & index); \
virtual HRESULT __stdcall GetToolProfile(CATIABase *& oToolProfile); \
virtual HRESULT __stdcall SetToolProfile(CATIABase * iToolProfile); \
virtual HRESULT __stdcall GetMotionProfile(CATIABase *& oMotionProfile); \
virtual HRESULT __stdcall SetMotionProfile(CATIABase * iMotionProfile); \
virtual HRESULT __stdcall GetObjectProfile(CATIABase *& oObjProfile); \
virtual HRESULT __stdcall SetObjectProfile(CATIABase * iObjProfile); \
virtual HRESULT __stdcall GetAccuracyProfile(CATIABase *& oAccuracyProfile); \
virtual HRESULT __stdcall SetAccuracyProfile(CATIABase * iAccuracyProfile); \
virtual HRESULT __stdcall IsSubTypeOf(const CATBSTR & iName, CAT_VARIANT_BOOL & oVal); \
virtual HRESULT __stdcall AttrValue(const CATVariant & iIndex, CATVariant & oAttVal); \
virtual HRESULT __stdcall AttrName(CATLONG iIndex, CATBSTR & oName); \
virtual HRESULT __stdcall get_Type(CATBSTR & oType); \
virtual HRESULT __stdcall get_Description(CATBSTR & oDescriptionBSTR); \
virtual HRESULT __stdcall put_Description(const CATBSTR & iDescriptionBSTR); \
virtual HRESULT __stdcall get_CycleTime(double & oCT); \
virtual HRESULT __stdcall put_CycleTime(double iCT); \
virtual HRESULT __stdcall get_CalculatedCycleTime(double & oCCT); \
virtual HRESULT __stdcall get_BeginningDate(double & oBegin); \
virtual HRESULT __stdcall put_BeginningDate(double iSBT); \
virtual HRESULT __stdcall get_EndDate(double & oEnd); \
virtual HRESULT __stdcall get_ChildrenActivities(CATIAActivities *& oChildren); \
virtual HRESULT __stdcall CreateChild(const CATBSTR & iTypeOfChild, CATIAActivity *& oCreatedChild); \
virtual HRESULT __stdcall CreateLink(CATIAActivity * iSecondActivity); \
virtual HRESULT __stdcall RemoveLink(CATIAActivity * iSecondActivity); \
virtual HRESULT __stdcall get_NextCFActivities(CATIAActivities *& oNextCF); \
virtual HRESULT __stdcall get_PreviousCFActivities(CATIAActivities *& oPreviousCF); \
virtual HRESULT __stdcall get_NextPRFActivities(CATIAActivities *& oNextPRF); \
virtual HRESULT __stdcall get_PreviousPRFActivities(CATIAActivities *& oPreviousPRF); \
virtual HRESULT __stdcall get_AttrCount(CATLONG & oNbAttr); \
virtual HRESULT __stdcall get_Items(CATIAItems *& oItems); \
virtual HRESULT __stdcall get_Outputs(CATIAOutputs *& oOutputs); \
virtual HRESULT __stdcall get_Resources(CATIAResources *& oResources); \
virtual HRESULT __stdcall get_Relations(CATIARelations *& oRelations); \
virtual HRESULT __stdcall get_Parameters(CATIAParameters *& oParameters); \
virtual HRESULT __stdcall GetTechnologicalObject(const CATBSTR & iApplicationType, CATBaseDispatch *& oApplicativeObj); \
virtual HRESULT __stdcall get_PrecedenceActivities(CATIAActivities *& oActivities); \
virtual HRESULT __stdcall get_PossiblePrecedenceActivities(CATIAActivities *& oActivities); \
virtual HRESULT __stdcall get_ProcessID(CATBSTR & oProcessID); \
virtual HRESULT __stdcall SetProcessID(const CATBSTR & iProcessID, CAT_VARIANT_BOOL iCheckUnique); \
virtual HRESULT __stdcall get_CalculatedBeginTime(double & oCBT); \
virtual HRESULT __stdcall AddAttr(const CATBSTR & iAttributeName, SPPProcessAttributeType AttrType, const CATBSTR & iAttributePromptName); \
virtual HRESULT __stdcall RemoveAttr(const CATBSTR & iAttributeName); \
virtual HRESULT __stdcall AddActivityConstraint(CATIAActivity * iActivity, SPPProcessConstraintType iConstraintType); \
virtual HRESULT __stdcall RemoveActivityConstraint(CATIAActivity * iActivity, SPPProcessConstraintType iConstraintType); \
virtual HRESULT __stdcall GetActivityConstraints(SPPProcessConstraintType iConstraintType, CATIAActivities *& oConstrtList); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DELMIARobotMotion(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::SetMotionType(short index) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)SetMotionType(index)); \
} \
HRESULT __stdcall  ENVTIEName::GetMotionType(short & index) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)GetMotionType(index)); \
} \
HRESULT __stdcall  ENVTIEName::GetTargetType(short & index) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)GetTargetType(index)); \
} \
HRESULT __stdcall  ENVTIEName::SetCartesianTarget(const CATSafeArrayVariant & iCartesianTarget) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)SetCartesianTarget(iCartesianTarget)); \
} \
HRESULT __stdcall  ENVTIEName::GetCartesianTarget(CATSafeArrayVariant & oCartesianTarget) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)GetCartesianTarget(oCartesianTarget)); \
} \
HRESULT __stdcall  ENVTIEName::SetJointTarget(const CATSafeArrayVariant & iJointTarget) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)SetJointTarget(iJointTarget)); \
} \
HRESULT __stdcall  ENVTIEName::GetJointTarget(CATSafeArrayVariant & oJointTarget) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)GetJointTarget(oJointTarget)); \
} \
HRESULT __stdcall  ENVTIEName::SetTagTarget(DELMIATag * iTag) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)SetTagTarget(iTag)); \
} \
HRESULT __stdcall  ENVTIEName::GetTagTarget(DELMIATag *& oTag) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)GetTagTarget(oTag)); \
} \
HRESULT __stdcall  ENVTIEName::SetTurnNumbers(const CATSafeArrayVariant & iTurnNumbers) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)SetTurnNumbers(iTurnNumbers)); \
} \
HRESULT __stdcall  ENVTIEName::GetTurnNumbers(CATSafeArrayVariant & oTurnNumbers) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)GetTurnNumbers(oTurnNumbers)); \
} \
HRESULT __stdcall  ENVTIEName::SetAuxillaryAxisValues(CATIABase * iDevice, const CATBSTR & iAuxillaryAxis, const CATSafeArrayVariant & iAuxillaryAxisValues) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)SetAuxillaryAxisValues(iDevice,iAuxillaryAxis,iAuxillaryAxisValues)); \
} \
HRESULT __stdcall  ENVTIEName::GetAuxillaryAxisValues(CATIABase * iDevice, const CATBSTR & iAuxillaryAxis, CATSafeArrayVariant & oAuxillaryAxisValues) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)GetAuxillaryAxisValues(iDevice,iAuxillaryAxis,oAuxillaryAxisValues)); \
} \
HRESULT __stdcall  ENVTIEName::GetAuxillaryAxisHome(CATIABase * iDevice, const CATBSTR & iAuxillaryAxis, CATBSTR & oHomeName) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)GetAuxillaryAxisHome(iDevice,iAuxillaryAxis,oHomeName)); \
} \
HRESULT __stdcall  ENVTIEName::SetAuxillaryAxisHome(CATIABase * iDevice, const CATBSTR & iAuxillaryAxis, const CATBSTR & iHomeName) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)SetAuxillaryAxisHome(iDevice,iAuxillaryAxis,iHomeName)); \
} \
HRESULT __stdcall  ENVTIEName::SetConfig(short iConfigNumber) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)SetConfig(iConfigNumber)); \
} \
HRESULT __stdcall  ENVTIEName::GetConfig(short & oConfigNumber) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)GetConfig(oConfigNumber)); \
} \
HRESULT __stdcall  ENVTIEName::SetOrientationMode(short index) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)SetOrientationMode(index)); \
} \
HRESULT __stdcall  ENVTIEName::GetOrientationMode(short & index) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)GetOrientationMode(index)); \
} \
HRESULT __stdcall  ENVTIEName::GetToolProfile(CATIABase *& oToolProfile) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)GetToolProfile(oToolProfile)); \
} \
HRESULT __stdcall  ENVTIEName::SetToolProfile(CATIABase * iToolProfile) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)SetToolProfile(iToolProfile)); \
} \
HRESULT __stdcall  ENVTIEName::GetMotionProfile(CATIABase *& oMotionProfile) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)GetMotionProfile(oMotionProfile)); \
} \
HRESULT __stdcall  ENVTIEName::SetMotionProfile(CATIABase * iMotionProfile) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)SetMotionProfile(iMotionProfile)); \
} \
HRESULT __stdcall  ENVTIEName::GetObjectProfile(CATIABase *& oObjProfile) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)GetObjectProfile(oObjProfile)); \
} \
HRESULT __stdcall  ENVTIEName::SetObjectProfile(CATIABase * iObjProfile) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)SetObjectProfile(iObjProfile)); \
} \
HRESULT __stdcall  ENVTIEName::GetAccuracyProfile(CATIABase *& oAccuracyProfile) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)GetAccuracyProfile(oAccuracyProfile)); \
} \
HRESULT __stdcall  ENVTIEName::SetAccuracyProfile(CATIABase * iAccuracyProfile) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)SetAccuracyProfile(iAccuracyProfile)); \
} \
HRESULT __stdcall  ENVTIEName::IsSubTypeOf(const CATBSTR & iName, CAT_VARIANT_BOOL & oVal) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)IsSubTypeOf(iName,oVal)); \
} \
HRESULT __stdcall  ENVTIEName::AttrValue(const CATVariant & iIndex, CATVariant & oAttVal) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)AttrValue(iIndex,oAttVal)); \
} \
HRESULT __stdcall  ENVTIEName::AttrName(CATLONG iIndex, CATBSTR & oName) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)AttrName(iIndex,oName)); \
} \
HRESULT __stdcall  ENVTIEName::get_Type(CATBSTR & oType) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)get_Type(oType)); \
} \
HRESULT __stdcall  ENVTIEName::get_Description(CATBSTR & oDescriptionBSTR) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)get_Description(oDescriptionBSTR)); \
} \
HRESULT __stdcall  ENVTIEName::put_Description(const CATBSTR & iDescriptionBSTR) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)put_Description(iDescriptionBSTR)); \
} \
HRESULT __stdcall  ENVTIEName::get_CycleTime(double & oCT) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)get_CycleTime(oCT)); \
} \
HRESULT __stdcall  ENVTIEName::put_CycleTime(double iCT) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)put_CycleTime(iCT)); \
} \
HRESULT __stdcall  ENVTIEName::get_CalculatedCycleTime(double & oCCT) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)get_CalculatedCycleTime(oCCT)); \
} \
HRESULT __stdcall  ENVTIEName::get_BeginningDate(double & oBegin) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)get_BeginningDate(oBegin)); \
} \
HRESULT __stdcall  ENVTIEName::put_BeginningDate(double iSBT) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)put_BeginningDate(iSBT)); \
} \
HRESULT __stdcall  ENVTIEName::get_EndDate(double & oEnd) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)get_EndDate(oEnd)); \
} \
HRESULT __stdcall  ENVTIEName::get_ChildrenActivities(CATIAActivities *& oChildren) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)get_ChildrenActivities(oChildren)); \
} \
HRESULT __stdcall  ENVTIEName::CreateChild(const CATBSTR & iTypeOfChild, CATIAActivity *& oCreatedChild) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)CreateChild(iTypeOfChild,oCreatedChild)); \
} \
HRESULT __stdcall  ENVTIEName::CreateLink(CATIAActivity * iSecondActivity) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)CreateLink(iSecondActivity)); \
} \
HRESULT __stdcall  ENVTIEName::RemoveLink(CATIAActivity * iSecondActivity) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)RemoveLink(iSecondActivity)); \
} \
HRESULT __stdcall  ENVTIEName::get_NextCFActivities(CATIAActivities *& oNextCF) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)get_NextCFActivities(oNextCF)); \
} \
HRESULT __stdcall  ENVTIEName::get_PreviousCFActivities(CATIAActivities *& oPreviousCF) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)get_PreviousCFActivities(oPreviousCF)); \
} \
HRESULT __stdcall  ENVTIEName::get_NextPRFActivities(CATIAActivities *& oNextPRF) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)get_NextPRFActivities(oNextPRF)); \
} \
HRESULT __stdcall  ENVTIEName::get_PreviousPRFActivities(CATIAActivities *& oPreviousPRF) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)get_PreviousPRFActivities(oPreviousPRF)); \
} \
HRESULT __stdcall  ENVTIEName::get_AttrCount(CATLONG & oNbAttr) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)get_AttrCount(oNbAttr)); \
} \
HRESULT __stdcall  ENVTIEName::get_Items(CATIAItems *& oItems) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)get_Items(oItems)); \
} \
HRESULT __stdcall  ENVTIEName::get_Outputs(CATIAOutputs *& oOutputs) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)get_Outputs(oOutputs)); \
} \
HRESULT __stdcall  ENVTIEName::get_Resources(CATIAResources *& oResources) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)get_Resources(oResources)); \
} \
HRESULT __stdcall  ENVTIEName::get_Relations(CATIARelations *& oRelations) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)get_Relations(oRelations)); \
} \
HRESULT __stdcall  ENVTIEName::get_Parameters(CATIAParameters *& oParameters) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)get_Parameters(oParameters)); \
} \
HRESULT __stdcall  ENVTIEName::GetTechnologicalObject(const CATBSTR & iApplicationType, CATBaseDispatch *& oApplicativeObj) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)GetTechnologicalObject(iApplicationType,oApplicativeObj)); \
} \
HRESULT __stdcall  ENVTIEName::get_PrecedenceActivities(CATIAActivities *& oActivities) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)get_PrecedenceActivities(oActivities)); \
} \
HRESULT __stdcall  ENVTIEName::get_PossiblePrecedenceActivities(CATIAActivities *& oActivities) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)get_PossiblePrecedenceActivities(oActivities)); \
} \
HRESULT __stdcall  ENVTIEName::get_ProcessID(CATBSTR & oProcessID) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)get_ProcessID(oProcessID)); \
} \
HRESULT __stdcall  ENVTIEName::SetProcessID(const CATBSTR & iProcessID, CAT_VARIANT_BOOL iCheckUnique) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)SetProcessID(iProcessID,iCheckUnique)); \
} \
HRESULT __stdcall  ENVTIEName::get_CalculatedBeginTime(double & oCBT) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)get_CalculatedBeginTime(oCBT)); \
} \
HRESULT __stdcall  ENVTIEName::AddAttr(const CATBSTR & iAttributeName, SPPProcessAttributeType AttrType, const CATBSTR & iAttributePromptName) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)AddAttr(iAttributeName,AttrType,iAttributePromptName)); \
} \
HRESULT __stdcall  ENVTIEName::RemoveAttr(const CATBSTR & iAttributeName) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)RemoveAttr(iAttributeName)); \
} \
HRESULT __stdcall  ENVTIEName::AddActivityConstraint(CATIAActivity * iActivity, SPPProcessConstraintType iConstraintType) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)AddActivityConstraint(iActivity,iConstraintType)); \
} \
HRESULT __stdcall  ENVTIEName::RemoveActivityConstraint(CATIAActivity * iActivity, SPPProcessConstraintType iConstraintType) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)RemoveActivityConstraint(iActivity,iConstraintType)); \
} \
HRESULT __stdcall  ENVTIEName::GetActivityConstraints(SPPProcessConstraintType iConstraintType, CATIAActivities *& oConstrtList) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)GetActivityConstraints(iConstraintType,oConstrtList)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DELMIARobotMotion,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DELMIARobotMotion(classe)    TIEDELMIARobotMotion##classe


/* Common methods inside a TIE */
#define common_TIE_DELMIARobotMotion(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DELMIARobotMotion, classe) \
 \
 \
CATImplementTIEMethods(DELMIARobotMotion, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DELMIARobotMotion, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DELMIARobotMotion, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DELMIARobotMotion, classe) \
 \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::SetMotionType(short index) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&index); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetMotionType(index); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&index); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::GetMotionType(short & index) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&index); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetMotionType(index); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&index); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::GetTargetType(short & index) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&index); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetTargetType(index); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&index); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::SetCartesianTarget(const CATSafeArrayVariant & iCartesianTarget) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&iCartesianTarget); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetCartesianTarget(iCartesianTarget); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&iCartesianTarget); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::GetCartesianTarget(CATSafeArrayVariant & oCartesianTarget) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&oCartesianTarget); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetCartesianTarget(oCartesianTarget); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&oCartesianTarget); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::SetJointTarget(const CATSafeArrayVariant & iJointTarget) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&iJointTarget); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetJointTarget(iJointTarget); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&iJointTarget); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::GetJointTarget(CATSafeArrayVariant & oJointTarget) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&oJointTarget); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetJointTarget(oJointTarget); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&oJointTarget); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::SetTagTarget(DELMIATag * iTag) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&iTag); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetTagTarget(iTag); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&iTag); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::GetTagTarget(DELMIATag *& oTag) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&oTag); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetTagTarget(oTag); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&oTag); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::SetTurnNumbers(const CATSafeArrayVariant & iTurnNumbers) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&iTurnNumbers); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetTurnNumbers(iTurnNumbers); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&iTurnNumbers); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::GetTurnNumbers(CATSafeArrayVariant & oTurnNumbers) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,11,&_Trac2,&oTurnNumbers); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetTurnNumbers(oTurnNumbers); \
   ExitAfterCall(this,11,_Trac2,&_ret_arg,&oTurnNumbers); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::SetAuxillaryAxisValues(CATIABase * iDevice, const CATBSTR & iAuxillaryAxis, const CATSafeArrayVariant & iAuxillaryAxisValues) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,12,&_Trac2,&iDevice,&iAuxillaryAxis,&iAuxillaryAxisValues); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetAuxillaryAxisValues(iDevice,iAuxillaryAxis,iAuxillaryAxisValues); \
   ExitAfterCall(this,12,_Trac2,&_ret_arg,&iDevice,&iAuxillaryAxis,&iAuxillaryAxisValues); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::GetAuxillaryAxisValues(CATIABase * iDevice, const CATBSTR & iAuxillaryAxis, CATSafeArrayVariant & oAuxillaryAxisValues) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,13,&_Trac2,&iDevice,&iAuxillaryAxis,&oAuxillaryAxisValues); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAuxillaryAxisValues(iDevice,iAuxillaryAxis,oAuxillaryAxisValues); \
   ExitAfterCall(this,13,_Trac2,&_ret_arg,&iDevice,&iAuxillaryAxis,&oAuxillaryAxisValues); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::GetAuxillaryAxisHome(CATIABase * iDevice, const CATBSTR & iAuxillaryAxis, CATBSTR & oHomeName) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,14,&_Trac2,&iDevice,&iAuxillaryAxis,&oHomeName); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAuxillaryAxisHome(iDevice,iAuxillaryAxis,oHomeName); \
   ExitAfterCall(this,14,_Trac2,&_ret_arg,&iDevice,&iAuxillaryAxis,&oHomeName); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::SetAuxillaryAxisHome(CATIABase * iDevice, const CATBSTR & iAuxillaryAxis, const CATBSTR & iHomeName) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,15,&_Trac2,&iDevice,&iAuxillaryAxis,&iHomeName); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetAuxillaryAxisHome(iDevice,iAuxillaryAxis,iHomeName); \
   ExitAfterCall(this,15,_Trac2,&_ret_arg,&iDevice,&iAuxillaryAxis,&iHomeName); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::SetConfig(short iConfigNumber) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,16,&_Trac2,&iConfigNumber); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetConfig(iConfigNumber); \
   ExitAfterCall(this,16,_Trac2,&_ret_arg,&iConfigNumber); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::GetConfig(short & oConfigNumber) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,17,&_Trac2,&oConfigNumber); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetConfig(oConfigNumber); \
   ExitAfterCall(this,17,_Trac2,&_ret_arg,&oConfigNumber); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::SetOrientationMode(short index) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,18,&_Trac2,&index); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetOrientationMode(index); \
   ExitAfterCall(this,18,_Trac2,&_ret_arg,&index); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::GetOrientationMode(short & index) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,19,&_Trac2,&index); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetOrientationMode(index); \
   ExitAfterCall(this,19,_Trac2,&_ret_arg,&index); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::GetToolProfile(CATIABase *& oToolProfile) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,20,&_Trac2,&oToolProfile); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetToolProfile(oToolProfile); \
   ExitAfterCall(this,20,_Trac2,&_ret_arg,&oToolProfile); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::SetToolProfile(CATIABase * iToolProfile) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,21,&_Trac2,&iToolProfile); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetToolProfile(iToolProfile); \
   ExitAfterCall(this,21,_Trac2,&_ret_arg,&iToolProfile); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::GetMotionProfile(CATIABase *& oMotionProfile) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,22,&_Trac2,&oMotionProfile); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetMotionProfile(oMotionProfile); \
   ExitAfterCall(this,22,_Trac2,&_ret_arg,&oMotionProfile); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::SetMotionProfile(CATIABase * iMotionProfile) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,23,&_Trac2,&iMotionProfile); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetMotionProfile(iMotionProfile); \
   ExitAfterCall(this,23,_Trac2,&_ret_arg,&iMotionProfile); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::GetObjectProfile(CATIABase *& oObjProfile) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,24,&_Trac2,&oObjProfile); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetObjectProfile(oObjProfile); \
   ExitAfterCall(this,24,_Trac2,&_ret_arg,&oObjProfile); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::SetObjectProfile(CATIABase * iObjProfile) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,25,&_Trac2,&iObjProfile); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetObjectProfile(iObjProfile); \
   ExitAfterCall(this,25,_Trac2,&_ret_arg,&iObjProfile); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::GetAccuracyProfile(CATIABase *& oAccuracyProfile) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,26,&_Trac2,&oAccuracyProfile); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAccuracyProfile(oAccuracyProfile); \
   ExitAfterCall(this,26,_Trac2,&_ret_arg,&oAccuracyProfile); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::SetAccuracyProfile(CATIABase * iAccuracyProfile) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,27,&_Trac2,&iAccuracyProfile); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetAccuracyProfile(iAccuracyProfile); \
   ExitAfterCall(this,27,_Trac2,&_ret_arg,&iAccuracyProfile); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::IsSubTypeOf(const CATBSTR & iName, CAT_VARIANT_BOOL & oVal) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,28,&_Trac2,&iName,&oVal); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->IsSubTypeOf(iName,oVal); \
   ExitAfterCall(this,28,_Trac2,&_ret_arg,&iName,&oVal); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::AttrValue(const CATVariant & iIndex, CATVariant & oAttVal) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,29,&_Trac2,&iIndex,&oAttVal); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->AttrValue(iIndex,oAttVal); \
   ExitAfterCall(this,29,_Trac2,&_ret_arg,&iIndex,&oAttVal); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::AttrName(CATLONG iIndex, CATBSTR & oName) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,30,&_Trac2,&iIndex,&oName); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->AttrName(iIndex,oName); \
   ExitAfterCall(this,30,_Trac2,&_ret_arg,&iIndex,&oName); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::get_Type(CATBSTR & oType) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,31,&_Trac2,&oType); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Type(oType); \
   ExitAfterCall(this,31,_Trac2,&_ret_arg,&oType); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::get_Description(CATBSTR & oDescriptionBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,32,&_Trac2,&oDescriptionBSTR); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Description(oDescriptionBSTR); \
   ExitAfterCall(this,32,_Trac2,&_ret_arg,&oDescriptionBSTR); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::put_Description(const CATBSTR & iDescriptionBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,33,&_Trac2,&iDescriptionBSTR); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Description(iDescriptionBSTR); \
   ExitAfterCall(this,33,_Trac2,&_ret_arg,&iDescriptionBSTR); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::get_CycleTime(double & oCT) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,34,&_Trac2,&oCT); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_CycleTime(oCT); \
   ExitAfterCall(this,34,_Trac2,&_ret_arg,&oCT); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::put_CycleTime(double iCT) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,35,&_Trac2,&iCT); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_CycleTime(iCT); \
   ExitAfterCall(this,35,_Trac2,&_ret_arg,&iCT); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::get_CalculatedCycleTime(double & oCCT) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,36,&_Trac2,&oCCT); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_CalculatedCycleTime(oCCT); \
   ExitAfterCall(this,36,_Trac2,&_ret_arg,&oCCT); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::get_BeginningDate(double & oBegin) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,37,&_Trac2,&oBegin); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_BeginningDate(oBegin); \
   ExitAfterCall(this,37,_Trac2,&_ret_arg,&oBegin); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::put_BeginningDate(double iSBT) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,38,&_Trac2,&iSBT); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_BeginningDate(iSBT); \
   ExitAfterCall(this,38,_Trac2,&_ret_arg,&iSBT); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::get_EndDate(double & oEnd) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,39,&_Trac2,&oEnd); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_EndDate(oEnd); \
   ExitAfterCall(this,39,_Trac2,&_ret_arg,&oEnd); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::get_ChildrenActivities(CATIAActivities *& oChildren) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,40,&_Trac2,&oChildren); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_ChildrenActivities(oChildren); \
   ExitAfterCall(this,40,_Trac2,&_ret_arg,&oChildren); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::CreateChild(const CATBSTR & iTypeOfChild, CATIAActivity *& oCreatedChild) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,41,&_Trac2,&iTypeOfChild,&oCreatedChild); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateChild(iTypeOfChild,oCreatedChild); \
   ExitAfterCall(this,41,_Trac2,&_ret_arg,&iTypeOfChild,&oCreatedChild); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::CreateLink(CATIAActivity * iSecondActivity) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,42,&_Trac2,&iSecondActivity); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateLink(iSecondActivity); \
   ExitAfterCall(this,42,_Trac2,&_ret_arg,&iSecondActivity); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::RemoveLink(CATIAActivity * iSecondActivity) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,43,&_Trac2,&iSecondActivity); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->RemoveLink(iSecondActivity); \
   ExitAfterCall(this,43,_Trac2,&_ret_arg,&iSecondActivity); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::get_NextCFActivities(CATIAActivities *& oNextCF) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,44,&_Trac2,&oNextCF); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_NextCFActivities(oNextCF); \
   ExitAfterCall(this,44,_Trac2,&_ret_arg,&oNextCF); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::get_PreviousCFActivities(CATIAActivities *& oPreviousCF) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,45,&_Trac2,&oPreviousCF); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_PreviousCFActivities(oPreviousCF); \
   ExitAfterCall(this,45,_Trac2,&_ret_arg,&oPreviousCF); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::get_NextPRFActivities(CATIAActivities *& oNextPRF) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,46,&_Trac2,&oNextPRF); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_NextPRFActivities(oNextPRF); \
   ExitAfterCall(this,46,_Trac2,&_ret_arg,&oNextPRF); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::get_PreviousPRFActivities(CATIAActivities *& oPreviousPRF) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,47,&_Trac2,&oPreviousPRF); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_PreviousPRFActivities(oPreviousPRF); \
   ExitAfterCall(this,47,_Trac2,&_ret_arg,&oPreviousPRF); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::get_AttrCount(CATLONG & oNbAttr) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,48,&_Trac2,&oNbAttr); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_AttrCount(oNbAttr); \
   ExitAfterCall(this,48,_Trac2,&_ret_arg,&oNbAttr); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::get_Items(CATIAItems *& oItems) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,49,&_Trac2,&oItems); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Items(oItems); \
   ExitAfterCall(this,49,_Trac2,&_ret_arg,&oItems); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::get_Outputs(CATIAOutputs *& oOutputs) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,50,&_Trac2,&oOutputs); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Outputs(oOutputs); \
   ExitAfterCall(this,50,_Trac2,&_ret_arg,&oOutputs); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::get_Resources(CATIAResources *& oResources) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,51,&_Trac2,&oResources); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Resources(oResources); \
   ExitAfterCall(this,51,_Trac2,&_ret_arg,&oResources); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::get_Relations(CATIARelations *& oRelations) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,52,&_Trac2,&oRelations); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Relations(oRelations); \
   ExitAfterCall(this,52,_Trac2,&_ret_arg,&oRelations); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::get_Parameters(CATIAParameters *& oParameters) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,53,&_Trac2,&oParameters); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parameters(oParameters); \
   ExitAfterCall(this,53,_Trac2,&_ret_arg,&oParameters); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::GetTechnologicalObject(const CATBSTR & iApplicationType, CATBaseDispatch *& oApplicativeObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,54,&_Trac2,&iApplicationType,&oApplicativeObj); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetTechnologicalObject(iApplicationType,oApplicativeObj); \
   ExitAfterCall(this,54,_Trac2,&_ret_arg,&iApplicationType,&oApplicativeObj); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::get_PrecedenceActivities(CATIAActivities *& oActivities) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,55,&_Trac2,&oActivities); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_PrecedenceActivities(oActivities); \
   ExitAfterCall(this,55,_Trac2,&_ret_arg,&oActivities); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::get_PossiblePrecedenceActivities(CATIAActivities *& oActivities) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,56,&_Trac2,&oActivities); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_PossiblePrecedenceActivities(oActivities); \
   ExitAfterCall(this,56,_Trac2,&_ret_arg,&oActivities); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::get_ProcessID(CATBSTR & oProcessID) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,57,&_Trac2,&oProcessID); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_ProcessID(oProcessID); \
   ExitAfterCall(this,57,_Trac2,&_ret_arg,&oProcessID); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::SetProcessID(const CATBSTR & iProcessID, CAT_VARIANT_BOOL iCheckUnique) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,58,&_Trac2,&iProcessID,&iCheckUnique); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetProcessID(iProcessID,iCheckUnique); \
   ExitAfterCall(this,58,_Trac2,&_ret_arg,&iProcessID,&iCheckUnique); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::get_CalculatedBeginTime(double & oCBT) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,59,&_Trac2,&oCBT); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_CalculatedBeginTime(oCBT); \
   ExitAfterCall(this,59,_Trac2,&_ret_arg,&oCBT); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::AddAttr(const CATBSTR & iAttributeName, SPPProcessAttributeType AttrType, const CATBSTR & iAttributePromptName) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,60,&_Trac2,&iAttributeName,&AttrType,&iAttributePromptName); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->AddAttr(iAttributeName,AttrType,iAttributePromptName); \
   ExitAfterCall(this,60,_Trac2,&_ret_arg,&iAttributeName,&AttrType,&iAttributePromptName); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::RemoveAttr(const CATBSTR & iAttributeName) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,61,&_Trac2,&iAttributeName); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->RemoveAttr(iAttributeName); \
   ExitAfterCall(this,61,_Trac2,&_ret_arg,&iAttributeName); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::AddActivityConstraint(CATIAActivity * iActivity, SPPProcessConstraintType iConstraintType) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,62,&_Trac2,&iActivity,&iConstraintType); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->AddActivityConstraint(iActivity,iConstraintType); \
   ExitAfterCall(this,62,_Trac2,&_ret_arg,&iActivity,&iConstraintType); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::RemoveActivityConstraint(CATIAActivity * iActivity, SPPProcessConstraintType iConstraintType) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,63,&_Trac2,&iActivity,&iConstraintType); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->RemoveActivityConstraint(iActivity,iConstraintType); \
   ExitAfterCall(this,63,_Trac2,&_ret_arg,&iActivity,&iConstraintType); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotMotion##classe::GetActivityConstraints(SPPProcessConstraintType iConstraintType, CATIAActivities *& oConstrtList) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,64,&_Trac2,&iConstraintType,&oConstrtList); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetActivityConstraints(iConstraintType,oConstrtList); \
   ExitAfterCall(this,64,_Trac2,&_ret_arg,&iConstraintType,&oConstrtList); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIARobotMotion##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,65,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,65,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIARobotMotion##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,66,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,66,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIARobotMotion##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,67,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,67,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIARobotMotion##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,68,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,68,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIARobotMotion##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,69,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,69,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DELMIARobotMotion(classe) \
 \
 \
declare_TIE_DELMIARobotMotion(classe) \
 \
 \
CATMetaClass * __stdcall TIEDELMIARobotMotion##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DELMIARobotMotion,"DELMIARobotMotion",DELMIARobotMotion::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DELMIARobotMotion(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DELMIARobotMotion, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDELMIARobotMotion##classe(classe::MetaObject(),DELMIARobotMotion::MetaObject(),(void *)CreateTIEDELMIARobotMotion##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DELMIARobotMotion(classe) \
 \
 \
declare_TIE_DELMIARobotMotion(classe) \
 \
 \
CATMetaClass * __stdcall TIEDELMIARobotMotion##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DELMIARobotMotion,"DELMIARobotMotion",DELMIARobotMotion::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DELMIARobotMotion(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DELMIARobotMotion, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDELMIARobotMotion##classe(classe::MetaObject(),DELMIARobotMotion::MetaObject(),(void *)CreateTIEDELMIARobotMotion##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DELMIARobotMotion(classe) TIE_DELMIARobotMotion(classe)
#else
#define BOA_DELMIARobotMotion(classe) CATImplementBOA(DELMIARobotMotion, classe)
#endif

#endif
