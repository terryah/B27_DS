/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef DELMIADeviceTask_h
#define DELMIADeviceTask_h

#ifndef ExportedByDNBIgpSetupPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __DNBIgpSetupPubIDL
#define ExportedByDNBIgpSetupPubIDL __declspec(dllexport)
#else
#define ExportedByDNBIgpSetupPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByDNBIgpSetupPubIDL
#endif
#endif

#include "CATIAActivity.h"

class CATIABase;

extern ExportedByDNBIgpSetupPubIDL IID IID_DELMIADeviceTask;

class ExportedByDNBIgpSetupPubIDL DELMIADeviceTask : public CATIAActivity
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall CreateMoveHomeActivity(CATIABase * ispFather, short position, CATIAActivity *& oMoveHomeActy)=0;

    virtual HRESULT __stdcall CreateMoveJointsActivity(CATIABase * ispFather, short position, CATIAActivity *& oMoveJointsActy)=0;

    virtual HRESULT __stdcall CreateDelayActivity(CATIABase * ispFather, short position, double delay_time, CATIAActivity *& oDelayActy)=0;


};

CATDeclareHandler(DELMIADeviceTask, CATIAActivity);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIABase.h"
#include "CATIACollection.h"
#include "CATIAItems.h"
#include "CATIAOutputs.h"
#include "CATIARelations.h"
#include "CATSPPDeclarations.h"
#include "CATSafeArray.h"
#include "CATVariant.h"
#include "DNBItemAssignmentType.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
