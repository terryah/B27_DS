#ifndef __TIE_DELMIARobotTaskCloneFactory
#define __TIE_DELMIARobotTaskCloneFactory

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DELMIARobotTaskCloneFactory.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DELMIARobotTaskCloneFactory */
#define declare_TIE_DELMIARobotTaskCloneFactory(classe) \
 \
 \
class TIEDELMIARobotTaskCloneFactory##classe : public DELMIARobotTaskCloneFactory \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DELMIARobotTaskCloneFactory, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall CloneTaskInSameRobot(DELMIARobotTask * iSrcRobotTask, const CATBSTR & iTargetTaskName, const CATSafeArrayVariant & iTransOnTarget, DELMIARobotTask *& oClonedRobotTask); \
      virtual HRESULT __stdcall CloneTaskInAnotherRobot(DELMIARobotTask * iSrcRobotTask, CATIABase * iDestRobot, const CATBSTR & iTargetTaskName, const CATSafeArrayVariant & iTransOnTarget, DELMIARobotTask *& oClonedRobotTask); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DELMIARobotTaskCloneFactory(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall CloneTaskInSameRobot(DELMIARobotTask * iSrcRobotTask, const CATBSTR & iTargetTaskName, const CATSafeArrayVariant & iTransOnTarget, DELMIARobotTask *& oClonedRobotTask); \
virtual HRESULT __stdcall CloneTaskInAnotherRobot(DELMIARobotTask * iSrcRobotTask, CATIABase * iDestRobot, const CATBSTR & iTargetTaskName, const CATSafeArrayVariant & iTransOnTarget, DELMIARobotTask *& oClonedRobotTask); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DELMIARobotTaskCloneFactory(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::CloneTaskInSameRobot(DELMIARobotTask * iSrcRobotTask, const CATBSTR & iTargetTaskName, const CATSafeArrayVariant & iTransOnTarget, DELMIARobotTask *& oClonedRobotTask) \
{ \
return (ENVTIECALL(DELMIARobotTaskCloneFactory,ENVTIETypeLetter,ENVTIELetter)CloneTaskInSameRobot(iSrcRobotTask,iTargetTaskName,iTransOnTarget,oClonedRobotTask)); \
} \
HRESULT __stdcall  ENVTIEName::CloneTaskInAnotherRobot(DELMIARobotTask * iSrcRobotTask, CATIABase * iDestRobot, const CATBSTR & iTargetTaskName, const CATSafeArrayVariant & iTransOnTarget, DELMIARobotTask *& oClonedRobotTask) \
{ \
return (ENVTIECALL(DELMIARobotTaskCloneFactory,ENVTIETypeLetter,ENVTIELetter)CloneTaskInAnotherRobot(iSrcRobotTask,iDestRobot,iTargetTaskName,iTransOnTarget,oClonedRobotTask)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DELMIARobotTaskCloneFactory,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DELMIARobotTaskCloneFactory,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DELMIARobotTaskCloneFactory,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DELMIARobotTaskCloneFactory,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DELMIARobotTaskCloneFactory,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DELMIARobotTaskCloneFactory(classe)    TIEDELMIARobotTaskCloneFactory##classe


/* Common methods inside a TIE */
#define common_TIE_DELMIARobotTaskCloneFactory(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DELMIARobotTaskCloneFactory, classe) \
 \
 \
CATImplementTIEMethods(DELMIARobotTaskCloneFactory, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DELMIARobotTaskCloneFactory, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DELMIARobotTaskCloneFactory, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DELMIARobotTaskCloneFactory, classe) \
 \
HRESULT __stdcall  TIEDELMIARobotTaskCloneFactory##classe::CloneTaskInSameRobot(DELMIARobotTask * iSrcRobotTask, const CATBSTR & iTargetTaskName, const CATSafeArrayVariant & iTransOnTarget, DELMIARobotTask *& oClonedRobotTask) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&iSrcRobotTask,&iTargetTaskName,&iTransOnTarget,&oClonedRobotTask); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CloneTaskInSameRobot(iSrcRobotTask,iTargetTaskName,iTransOnTarget,oClonedRobotTask); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&iSrcRobotTask,&iTargetTaskName,&iTransOnTarget,&oClonedRobotTask); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIARobotTaskCloneFactory##classe::CloneTaskInAnotherRobot(DELMIARobotTask * iSrcRobotTask, CATIABase * iDestRobot, const CATBSTR & iTargetTaskName, const CATSafeArrayVariant & iTransOnTarget, DELMIARobotTask *& oClonedRobotTask) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&iSrcRobotTask,&iDestRobot,&iTargetTaskName,&iTransOnTarget,&oClonedRobotTask); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CloneTaskInAnotherRobot(iSrcRobotTask,iDestRobot,iTargetTaskName,iTransOnTarget,oClonedRobotTask); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&iSrcRobotTask,&iDestRobot,&iTargetTaskName,&iTransOnTarget,&oClonedRobotTask); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIARobotTaskCloneFactory##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIARobotTaskCloneFactory##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIARobotTaskCloneFactory##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIARobotTaskCloneFactory##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIARobotTaskCloneFactory##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DELMIARobotTaskCloneFactory(classe) \
 \
 \
declare_TIE_DELMIARobotTaskCloneFactory(classe) \
 \
 \
CATMetaClass * __stdcall TIEDELMIARobotTaskCloneFactory##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DELMIARobotTaskCloneFactory,"DELMIARobotTaskCloneFactory",DELMIARobotTaskCloneFactory::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DELMIARobotTaskCloneFactory(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DELMIARobotTaskCloneFactory, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDELMIARobotTaskCloneFactory##classe(classe::MetaObject(),DELMIARobotTaskCloneFactory::MetaObject(),(void *)CreateTIEDELMIARobotTaskCloneFactory##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DELMIARobotTaskCloneFactory(classe) \
 \
 \
declare_TIE_DELMIARobotTaskCloneFactory(classe) \
 \
 \
CATMetaClass * __stdcall TIEDELMIARobotTaskCloneFactory##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DELMIARobotTaskCloneFactory,"DELMIARobotTaskCloneFactory",DELMIARobotTaskCloneFactory::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DELMIARobotTaskCloneFactory(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DELMIARobotTaskCloneFactory, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDELMIARobotTaskCloneFactory##classe(classe::MetaObject(),DELMIARobotTaskCloneFactory::MetaObject(),(void *)CreateTIEDELMIARobotTaskCloneFactory##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DELMIARobotTaskCloneFactory(classe) TIE_DELMIARobotTaskCloneFactory(classe)
#else
#define BOA_DELMIARobotTaskCloneFactory(classe) CATImplementBOA(DELMIARobotTaskCloneFactory, classe)
#endif

#endif
