// COPYRIGHT DASSAULT SYSTEMES 2013
//===================================================================
//
// DNBIBuildSplineMoveRep.h
// Define the DNBIBuildSplineMoveRep interface.
//
//===================================================================
//
// Usage notes:
//   DNBIBuildSplineMoveRep: This Interface provides way to get a  customized (spline) visu for Robot task.
//
//===================================================================

#ifndef DNBIBuildSplineMoveRep_H
#define DNBIBuildSplineMoveRep_H

class CATRep;

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

#include "DNBIgpSetupItfCPP.h"

// System
#include "CATBaseUnknown.h"
#include "CATLISTV_CATBaseUnknown.h"

extern ExportedByDNBIgpSetupItfCPP IID IID_DNBIBuildSplineMoveRep ;

//------------------------------------------------------------------

/**
 * <p>
 * <b>Role</b>:
 * This Interface provides a way to get a  customized (spline) visu for Robot task.
 */

class ExportedByDNBIgpSetupItfCPP DNBIBuildSplineMoveRep: public CATBaseUnknown
{
  /**
   * @nodoc
   */
   CATDeclareInterface;

public:

    /**
      * Gives the customized spline Representation.
      * @param iListOfNodes
      *   the list of Robot motions which falls in the spline block
      * @return 
      * A pointer to the graphical representation.      
      */ 
     virtual CATRep* GetSplineRep(const CATListValCATBaseUnknown_var iListOfNodes) = 0;
};

/**
 * @nodoc
 */
CATDeclareHandler( DNBIBuildSplineMoveRep, CATBaseUnknown );

#endif
