// COPYRIGHT DASSAULT SYSTEMES 2002
//===================================================================
//
// DNBOperationType.h
// Define the DNBOperationType.h
//
//===================================================================
//
// Usage notes:
//   DNBOperationType is used to specify the Operation Types.
//
//===================================================================
//
//  Jan 2002  Creation:                                           GJA
//===================================================================
#ifndef DNBOperationType_H
#define DNBOperationType_H

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

/**
 * Operation Types.
 * @param DNBIgpUntyped
 *   Normal operation.
 * @param DNBIgpViaPoint
 *   Via point operation.
 */

enum DNBOperationType
{ 
   DNBIgpUntyped = 0,
   DNBIgpViaPoint
};

#endif


