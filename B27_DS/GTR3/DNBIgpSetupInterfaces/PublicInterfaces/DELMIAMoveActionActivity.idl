// COPYRIGHT Dassault Systemes 2007
//===================================================================
//
// DELMIAMoveActionActivity.idl
// Automation interface for the MoveActionActivity element 
//
//===================================================================
//
// Usage notes:
//
//===================================================================
//
//  Dec 2007  Creation:                                      NPO
//===================================================================
#ifndef DELMIAMoveActionActivity_IDL
#define DELMIAMoveActionActivity_IDL
/*IDLREP*/

/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */

#include "CATIAActivity.idl"
#include "CATSafeArray.idl"
#include "CATBSTR.idl"

/**
 * Interface representing a Move Activity inside a generic Action (Close Gun/Open Gun etc).
 *
 * <br><b>Role</b>: This interface is used to retieve/assign the value of motion targets/attributes 
 * for the Move Activity.
 * 
 * <br>The following code snippet can be used to obtain a MoveActionActivity from a selected Activity 
 * <pre>
 *   Dim oSelectAct As Activity
 *   set oSelectAct = CATIA.ActiveDocument.Selection.FindObject("CATIAActivity")
 *   Dim objMoveAct As MoveActionActivity
 *   set objMoveAct = oSelectAct.GetTechnologicalObject("MoveActionActivity")
 * </pre>
 */

interface DELMIAMoveActionActivity : CATIAActivity 
{
   /**
   * Retreives Target Type depending on the defined target("HOME" / "JOINT")
   * @return oTargetType
   *     The Target Type for the Move.
   * @sample
   *   <pre>
   *   Dim objMoveAct As MoveActionActivity
   *          ......
   *   Dim  TgtType As String
   *   TgtType=objMoveAct.TargetType
   *   </pre>
   */
#pragma PROPERTY TargetType   
   HRESULT get_TargetType(inout  /*IDLRETVAL*/ CATBSTR oTargetType); 

   /**
   * This property returns and sets the joint values for the Move activity.
   * @return oJointVal
   *     The joint values for the Move activity.
   * @param iJointVal
   *     The specified joint values for the move activity.
   * @sample
   *   <pre>
   *   Dim objMoveAct As MoveActionActivity
   *          ......
   *   Dim  ListOfJointValues() 
   *   set ListOfJointValues=objMoveAct.JointValues
   *   ..
   *   For i = 0 to ubound (ListOfJointValues)
   *      ...
   *   Next
   *   objMoveAct.JointValues=ListOfJointValues
   *   </pre>
   */
#pragma PROPERTY JointValues   
   HRESULT get_JointValues(out  /*IDLRETVAL*/ CATSafeArrayVariant oJointVal); 
   HRESULT put_JointValues(in CATSafeArrayVariant iJointVal);

   /**
   *  Sets and Retreives Home Name corresponding to the activity target
   * @return oHomeName
   *     Home Name stored as the target.
   * @param iHomeName
   *     The specified Home Name as the activity target
   * @sample
   *   <pre>
   *   Dim objMoveAct As MoveActionActivity
   *          ......
   *   Dim  HomeName as string
   *   set HomeName=objMoveAct.HomeName
   *   HomeName  = "Home.2"
   *   objMoveAct.HomeName=HomeName
   *   </pre>
   */
#pragma PROPERTY HomeName   
   HRESULT get_HomeName(inout  /*IDLRETVAL*/ CATBSTR oHomeName); 
   HRESULT put_HomeName(in CATBSTR iHomeName); 
};

// Interface name : DELMIAMoveActionActivity
#pragma ID DELMIAMoveActionActivity "DCE:62aeb4ad-ecb2-4d4e-925ac7b39f1830de"
#pragma DUAL DELMIAMoveActionActivity

// VB object name : MoveActionActivity (Id used in Visual Basic)
#pragma ID MoveActionActivity "DCE:81a990df-5fa8-4221-b8c599aa53288306"
#pragma ALIAS DELMIAMoveActionActivity MoveActionActivity

#endif
