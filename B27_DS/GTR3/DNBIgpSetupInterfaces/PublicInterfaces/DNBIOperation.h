#ifndef DNBIOperation_H
#define DNBIOperation_H

// COPYRIGHT DASSAULT SYSTEMES 2003

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

#include "DNBIgpSetupItfCPP.h"
#include "DNBOperationType.h"
#include "CATBaseUnknown.h"
#include "CATBooleanDef.h"
#include "CATUnicodeString.h"
#include "CATLISTV_CATBaseUnknown.h"
#include "CATLISTV_CATISpecObject.h"

extern ExportedByDNBIgpSetupItfCPP IID IID_DNBIOperation;
class DNBIRobotTargetMotion;
/**
 * Interface to access Operations.
 * <b>Role</b>: 
 * The Operation has Target and it tells if the Target is of Type Via
 * @see DNBIOperationFactory
 */
class ExportedByDNBIgpSetupItfCPP DNBIOperation: public CATBaseUnknown
{
   /**
    * @nodoc
    */
   CATDeclareInterface;

  public:
    enum DNBViaMode { UnTyped = 0 , ViaPoint };
         
   /**
    * Tells if the Target is a Via Point
    * @param oViaMode
    *   ViaPoint if it is a Via Point
    *   UnTyped  if it is not a Via Point
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl>
    */ 
   virtual HRESULT GetViaMode( DNBIOperation::DNBViaMode * oViaMode) = 0;
   /**
    * Sets it is a Via Point or as not a Via Point
    * @param iViaMode
    *   ViaPoint if to set the Target to be a Via Point
    *   UnTyped  if to set it the Target to be not a Via Point
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl>
    */ 
   virtual HRESULT SetViaMode( const DNBIOperation::DNBViaMode &  iViaMode) =0;
   /**
    * Retrieves the Type of Operation.
    * @param iOpType
    *   The Type of the operation.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl>
    */ 
   virtual HRESULT GetType( CATUnicodeString &oType) = 0;

   /**
    * Retrieves the RobotTargetMotion
    * @param oRobotTargetMotion
    *   Returns the RobotMotion
    * @return 
    *   An HRESULT
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl>
    */ 
   virtual HRESULT GetRobotTargetMotion( DNBIRobotTargetMotion** oRobotTargetMotion) = 0;
 
   /**
    * Retrieves all the Childrens
    * @param oChildrenList
    *   Returns the Pre-Actions, RobotMotion,  Post-Actions
    * @return 
    *   An HRESULT
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
     *   </dl>
    */ 
 
   virtual HRESULT GetAllChildren(CATListValCATBaseUnknown_var* oChildrenList)=0;
   /**
    * Tells if the Target has UserProfilesList saved
    * @param oObjList
    *   list of UserProfilesList if saved before
    * @param iType
    *   if iType specified, only a list of that type inside UserProfilesList will be returned
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>Nothing saved</dd>
    *   </dl>
    */ 
   virtual HRESULT GetUserProfilesList( CATListValCATISpecObject_var * oObjList, CATUnicodeString iType = "" ) = 0;
   /**
    * Sets UserProfilesList
    * @param iObjList
    *   list of UserProfilesList to be set
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *     </dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was failed to be created</dd>
     *   </dl>
    */ 
   virtual HRESULT SetUserProfilesList( const CATListValCATISpecObject_var & iObjList ) = 0;
};

/**
 * @nodoc
 */
CATDeclareHandler( DNBIOperation, CATBaseUnknown );

#endif
