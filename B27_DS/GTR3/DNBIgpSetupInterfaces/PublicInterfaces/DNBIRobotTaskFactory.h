#ifndef DNBIRobotTaskFactory_H
#define DNBIRobotTaskFactory_H

// COPYRIGHT DASSAULT SYSTEMES 2003

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

#include "DNBIgpSetupItfCPP.h"
#include "CATBaseUnknown.h"
#include "DNBListOfRobotTask.h"

class CATUnicodeString;
class DNBIRobotTask;

extern ExportedByDNBIgpSetupItfCPP IID IID_DNBIRobotTaskFactory;

/**
 * Interface to create and access RobotTask.
 * <b>Role:</b>
 * This Interface provides methods for Creating/Getting/Removing Robot Tasks.
 */
class ExportedByDNBIgpSetupItfCPP DNBIRobotTaskFactory: public CATBaseUnknown
{
  /**
   * @nodoc
   */
   CATDeclareInterface;
   
public:
   
   /**
    * Creates a Robot Task
    * @param iName
    *   The Robot Task Nmae.
    * @param oRobotTask
    *   The Created Robot Task.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
    *   </dl> 
    */
   virtual HRESULT CreateRobotTask(const CATUnicodeString & iName,
                                         DNBIRobotTask** oRobotTask)=0;

   /**
    * Retrieves the Underlying List of Robot Tasks.
    * @param oRobotTaskList
    *   The Robot Task List.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
    *   </dl> 
    */
   virtual HRESULT GetAllRobotTasks(DNBListOfRobotTask & oRobotTaskList)=0;
   
   /**
    * Removes the required Robot Task.
    * @param iRobotTask
    *   The Robot Task to be Removed.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
    *   </dl> 
    */
   virtual HRESULT RemoveRobotTask( DNBIRobotTask* iRobotTask)=0;
};

/**
 * @nodoc
 */
CATDeclareHandler( DNBIRobotTaskFactory, CATBaseUnknown );

#endif
