// COPYRIGHT Dassault Systemes 2007
//===================================================================
//
// DNBIRobotContinuousPathMotionAttr.h
// Define the DNBIRobotContinuousPathMotionAttr interface
//
//===================================================================
//
// Usage notes:
//   Interface to access RobotContinuousPathMotion attributes
//
//===================================================================
//
//  Aug 2007  Creation: Code generated by the CAA wizard  ATJ
//===================================================================
#ifndef DNBIRobotContinuousPathMotionAttr_H
#define DNBIRobotContinuousPathMotionAttr_H

#include "DNBIgpSetupItfCPP.h"
#include "CATBaseUnknown.h"
#include "CATListOfDouble.h"

class CATUnicodeString;
class CATListValCATBaseUnknown_var;
class CATIProduct;

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */
extern ExportedByDNBIgpSetupItfCPP IID IID_DNBIRobotContinuousPathMotionAttr;
//------------------------------------------------------------------

/**
 * Interface to access RobotContinuousPathMotion attributes.
 * <b>Role:</b>
 * This interface provides methods to Set/retrieve motion attributes 
 * related to RobotContinuousPathMotion.
 */

class ExportedByDNBIgpSetupItfCPP DNBIRobotContinuousPathMotionAttr: public CATBaseUnknown
{
  CATDeclareInterface;

  public:

   /**
    * TCP Orient Mode
    */
   enum DNBCPTCPOrientMode
   {
      TCPOrientOneAxis   = 0,
      TCPOrientTwoAxis   = 1,
      TCPOrientThreeAxis = 2,
      TCPOrientWristAxis = 3
   };
   
   /**
    * Motion Type
    */
   enum DNBCPMotionType 
   {
        JOINTMOVE,     // Interpolated movement of joints
        LINEARMOVE,    // The TCP moves in a linear fashion
        PREDEFINED,    // PREDEFINED MOVE, JOINT Interpolated
        CIRCULAR,      // The TCP moves in a circular fashion
        CIRCULARVIA,   // via point in the TCP moves in a circular fashionon
        SLEW
     
   };

   /**
    * Sets the turn number values for all the targets.
    * @param iTurnNumbers
    *   List of turn number values to be set for all the targets 
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>Turn numbers are successfully set</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>Turn numbers could not be set</dd>
    *   </dl> 
    */
   virtual HRESULT SetTurnNumbers(const CATListOfInt & iTurnNumbers)=0;
   
   /**
    * Gets the turn number values for all the targets.
    * @param oTurnNumbers
    *   List of turn number values to be retrieved from all the targets 
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>Turn numbers are successfully retrieved</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>Turn numbers could not be retrieved successfully</dd>
    *   </dl> 
    */
   virtual HRESULT GetTurnNumbers(CATListOfInt & oTurnNumbers) const =0;

	/**
    * Sets the turn sign values for all the targets.
    * @param iTurnSigns
    *   List of turn sign values to be set for all the targets 
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>Turn sign values are successfully set</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>Turn sign values could not be set</dd>
    *   </dl> 
    */
   virtual HRESULT SetTurnSigns(const CATListOfInt & iTurnSigns)=0;
   
   /**
    * Gets the turn sign values for all the targets.
    * @param oTurnSigns
    *   List of turn sign values to be retrieved from all the targets 
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>Turn sign values are successfully retrieved</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>Turn sign values could not be retrieved successfully</dd>
    *   </dl> 
    */
   virtual HRESULT GetTurnSigns(CATListOfInt & oTurnSigns) const =0;

   /**
    * Sets the robot configuration values for all the targets.
    * @param iConfig
    *   List of robot configuration values to be set for all the targets 
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>Robot configuration values are successfully set</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>Robot configuration values could not be set</dd>
    *   </dl> 
    */
   virtual HRESULT SetRobotConfiguration(const CATListOfInt & iConfig)=0;
   
   /**
    * Gets the robot configuration values for all the targets.
    * @param oConfigNumber
    *   List of robot configuration values to be retrieved from all the targets 
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>Robot configuration values are successfully retrieved</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>Robot configuration values could not be retrieved successfully</dd>
    *   </dl> 
    */
   virtual HRESULT GetRobotConfiguration(CATListOfInt &oConfigNumber)const =0;
   		
   /**
    * Gets the tool profiles for all the targets.
    * @param oToolProfiles
    *   List of tool profiles to be retrieved from all the targets 
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>Tool profiles are successfully retrieved</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>Tool profiles could not be retrieved successfully</dd>
    *   </dl> 
    */
   virtual HRESULT GetToolProfile(CATListValCATBaseUnknown_var * oToolProfiles)const = 0;

   /**
    * Gets the motion profiles for all the targets.
    * @param oMotionProfiles
    *   List of motion profiles to be retrieved from all the targets 
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>Motion profiles are successfully retrieved</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>Motion profiles could not be retrieved successfully</dd>
    *   </dl> 
    */
   virtual HRESULT GetMotionProfile(CATListValCATBaseUnknown_var *oMotionProfiles)const = 0;

   /**
    * Gets the object profiles for all the targets.
    * @param oObjProfiles
    *   List of object profiles to be retrieved from all the targets 
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>Object profiles are successfully retrieved</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>Object profiles could not be retrieved successfully</dd>
    *   </dl> 
    */
   virtual HRESULT GetObjectProfile(CATListValCATBaseUnknown_var * oObjProfiles)const = 0;

   /**
    * Gets the accuracy profiles for all the targets.
    * @param oAccuracyProfiles
    *   List of accuracy profiles to be retrieved from all the targets 
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>Accuracy profiles are successfully retrieved</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>Accuracy profiles could not be retrieved successfully</dd>
    *   </dl> 
    */
   virtual HRESULT GetAccuracyProfile(CATListValCATBaseUnknown_var * oAccuracyProfiles)const = 0;
 
   /**
    * Sets the root product.
    * @param piDeviceRoot
    *   The root product 
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>Root product successfully set</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>Root product could not be set</dd>
    *   </dl> 
    */
   virtual HRESULT SetRootProduct(CATIProduct* piDeviceRoot) = 0;

   /**
    * Gets the root product.
    * @param poDeviceRoot
    *   The root product
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>Root product successfully retrieved</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>Root product could not be retrieved successfully</dd>
    *   </dl> 
    */
   virtual HRESULT GetRootProduct(CATIProduct ** poDeviceRoot)const = 0;

   /**
    * Sets the motion type.
    * @param motType
    *   The motion type
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>Motion type successfully set</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>Motion type could not be set</dd>
    *   </dl> 
    */
   virtual HRESULT SetMotionType( const DNBIRobotContinuousPathMotionAttr::DNBCPMotionType motType ) = 0;

  /**
    * Gets the motion type.
    * @param motType
    *   The motion type
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>Motion type successfully retrieved</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>Motion type could not be retrieved successfully</dd>
    *   </dl> 
    */
   virtual HRESULT GetMotionType( DNBIRobotContinuousPathMotionAttr::DNBCPMotionType &motType )const = 0;

   /**
    * Sets the mechanism.
    * @param piMech
    *   The mechanism
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>Mechanism successfully set</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>Mechanism could not be set</dd>
    *   </dl> 
    */
   virtual HRESULT SetMechanism( CATBaseUnknown* piMech ) = 0;

   /**
    * Gets the mechanism.
    * @param poMech
    *   The mechanism
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>Mechanism successfully retrieved</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>Mechanism could not be retrieved successfully</dd>
    *   </dl> 
    */
   virtual HRESULT GetMechanism( CATBaseUnknown** poMech ) const = 0;

   /**
    * Sets the auxiliary axis lock status for the auxiliary device.
    * @param ipAuxDevice
    *   The auxiliary device
    * @param iLock
    *   The auxiliary axis lock status
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>Status successfully set</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>Status could not be set</dd>
    *   </dl> 
    */
   virtual HRESULT SetAuxAxisLockStatus( CATBaseUnknown* ipAuxDevice, const CATBoolean &iLock) = 0;

   /**
    * Gets the auxiliary axis lock status for the auxiliary device.
    * @param ipAuxDevice
    *   The auxiliary device
    * @param oLock
    *   The auxiliary axis lock status
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>Status successfully retrieved</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>Status could not be retrieved successfully</dd>
    *   </dl> 
    */
   virtual HRESULT GetAuxAxisLockStatus( CATBaseUnknown* ipAuxDevice, CATBoolean &oLock ) const = 0;

  // No constructors or destructors on this pure virtual base class
  // --------------------------------------------------------------
};
CATDeclareHandler( DNBIRobotContinuousPathMotionAttr, CATBaseUnknown );

//------------------------------------------------------------------

#endif
