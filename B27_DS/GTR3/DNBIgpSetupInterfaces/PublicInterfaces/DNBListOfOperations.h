// COPYRIGHT DASSAULT SYSTEMES 2003
//===================================================================
//
// DNBListOfOperation
//   This interface allows the management of a list of DNBIOperation
//
//===================================================================
//
// 17/06/2003 Creation                                        NPJ
//===================================================================
#ifndef DNBListOfOperation_H
#define DNBListOfOperation_H

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

/**
 * @collection DNBListOfOperation
 * Collection class for pointers to DNBIOperation.
 * Only the following methods of pointer collection classes are available:
 * <ul>
 * <li><tt>Append</tt></li>
 * <li><tt>Size</tt></li>
 * <li><tt>Operator[]</tt></li>
 * <li><tt>RemovePosition</tt></li>
 * <li><tt>RemoveAll</tt></li>
 * </ul>
 * Refer to the articles dealing with collections in the encyclopedia.
 */

#include "DNBIgpSetupItfCPP.h"
class DNBIOperation;

// clean previous functions requests
#include  <CATLISTP_Clean.h>

// require needed functions
#define CATLISTP_Append
#define CATLISTP_RemovePosition
#define CATLISTP_RemoveAll
#define CATLISTP_Swap
// get macros
#include  <CATLISTP_Declare.h>

// generate interface of collection-class
// (functions declarations)
#undef  CATCOLLEC_ExportedBy
#define CATCOLLEC_ExportedBy    ExportedByDNBIgpSetupItfCPP

/**
 * @nodoc
 */
CATLISTP_DECLARE(DNBIOperation)
typedef CATLISTP(DNBIOperation) DNBListOfOperations ;
#endif

