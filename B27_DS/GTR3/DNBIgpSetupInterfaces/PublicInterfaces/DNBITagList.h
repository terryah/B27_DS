// COPYRIGHT DASSAULT SYSTEMES 2000
//===================================================================
//
// DNBITagList.h
//   This interface allows the management of a list of Tags.
//
//===================================================================
//
//  Dec 2000  Creation: SAR
//  Feb 2002  Documentation                                       GJA
//===================================================================
#ifndef DNBITagList_H
#define DNBITagList_H

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

/**
 * @collection DNBITagList
 * Collection class for pointers to DNBITag.
 * Only the following methods of pointer collection classes are available:
 * <ul>
 * <li><tt>Append</tt></li>
 * <li><tt>Size</tt></li>
 * <li><tt>Operator[]</tt></li>
 * <li><tt>RemovePosition</tt></li>
 * <li><tt>RemoveAll</tt></li>
 * </ul>
 * Refer to the articles dealing with collections in the encyclopedia.
 */

#include "DNBIgpSetupItfCPP.h"
class DNBITag;

// clean previous functions requests
#include  <CATLISTP_Clean.h>

// require needed functions
#define CATLISTP_Append
#define CATLISTP_RemovePosition
#define CATLISTP_RemoveAll

// get macros
#include  <CATLISTP_Declare.h>

// generate interface of collection-class
// (functions declarations)
#undef  CATCOLLEC_ExportedBy
#define CATCOLLEC_ExportedBy    ExportedByDNBIgpSetupItfCPP

/**
 * @nodoc
 */
CATLISTP_DECLARE(DNBITag)
typedef CATLISTP(DNBITag) DNBITagList ;
#endif

