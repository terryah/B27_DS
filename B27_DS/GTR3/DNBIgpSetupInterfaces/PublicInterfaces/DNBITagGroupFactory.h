// COPYRIGHT Dassault Systemes 2002
//===================================================================
//
// DNBITagGroupFactory.h
// Define the DNBITagGroupFactory interface
//
//===================================================================
//
// Usage notes:
//   DNBITagGroupFactory interface: Factory to Create Tag Groups. Tag 
//   Groups are used to contain Tags.
//  
//===================================================================
//
//  Feb 2002  Creation and Documentation                          GJA
//  03/03/2002 Modified for CATBSTR to CATUnicodeString           RMY
//  July 2003  Added New method to account for Modify Reference   NPJ
//	APR  2005  Added the DeleteTagGroup/ReparentTagGroup, 
//               CopyTagGroup  methods							         NPO
//
//===================================================================
#ifndef DNBITagGroupFactory_H
#define DNBITagGroupFactory_H

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

#include "DNBIgpSetupItfCPP.h"

// System
#include "CATBaseUnknown.h"
#include "CATBooleanDef.h"

// ProductStructure
class CATIProduct;

// DNBIgpSetupInterfaces
class DNBITagGroup;

// System
class CATUnicodeString;

extern ExportedByDNBIgpSetupItfCPP IID IID_DNBITagGroupFactory ;

/**
* Interface to create a Tag Group.
* <b>Role:</b>
* This interface provides methods for creating tag groups. 
*/
class ExportedByDNBIgpSetupItfCPP DNBITagGroupFactory: public CATBaseUnknown
{
   /**
    * @nodoc
    */
   CATDeclareInterface;

public:
    
   /**
    * Creates a Tag Group.
    * @param iTagGroupName
    *   Name of the TagGroup to be created.
    * @param iParentProd
    *   The Parent Product of the TagGroup.
    * @param oTagGroup
    *   The Newly created TagGroup.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
    *   </dl>
    */
   virtual HRESULT     CreateTagGroup( CATUnicodeString & iTagGroupName,
                                       CATIProduct    * iParentProd,
                                       DNBITagGroup  ** oTagGroup ) = 0;

   /**
     * Creates a Tag Group. Each Tag Group is associated with a Name, option to
	 * to modify reference or modify locally and the reference product.
     * @param iTagGroupName
     *     Name of the Tag Group to be created.
     * @param iModifyReference
     *     Modify Reference or modify locally.
     * @param iProduct
     *     Reference Product.
	 * @param oTagGroup
     *     Created Tag Group.
     * @return
     * an HRESULT value.
     * <br><b>Legal values</b>:
     * <ul>
     * <li>S_OK if the operation succeeds </li>
     * <li>E_FAIL otherwise </li>.
     * </ul>
     */
   virtual HRESULT     CreateTagGroup(CATUnicodeString &iTagGroupName,
                           boolean iModifyReference,
                           CATIProduct *iProduct,
                           DNBITagGroup ** oTagGroup) = 0;
   /**
     * Deletes a Tag Group. 
	 * @param iTagGroup
     *      Tag Group.to be deleted
     * @return
     * an HRESULT value.
     * <br><b>Legal values</b>:
     * <ul>
     * <li>S_OK if the operation succeeds </li>
     * <li>E_FAIL otherwise </li>.
     * </ul>
     */
   virtual HRESULT     DeleteTagGroup(DNBITagGroup * iTagGroup) = 0;
      
   /**
     * Reparents a Tag Group. 
	 * @param iTagGroup
     *      Tag Group.to be reparented
	 * @param iModifyReference
     *     Modify Reference or modify locally.
     * @param iProduct
     *     New Product to which the tag group has to be reparented
     * @return
     * an HRESULT value.
     * <br><b>Legal values</b>:
     * <ul>
     * <li>S_OK if the operation succeeds </li>
     * <li>E_FAIL otherwise </li>.
     * </ul>
     */

   virtual HRESULT  ReparentTagGroup(DNBITagGroup * iTagGroup,CATIProduct *iProduct,boolean iModifyReference)=0;
         
   /**
     * Copies a Tag Group to new a CATIproduct
	 * @param iTagGroup
     *      Tag Group.to be copied
     * @param iProduct
     *     New TagGroup to which the tag group has to be copied
     * @return
     * an HRESULT value.
     * <br><b>Legal values</b>:
     * <ul>
     * <li>S_OK if the operation succeeds </li>
     * <li>E_FAIL otherwise </li>.
     * </ul>
     */

   virtual HRESULT  CopyTagGroup(DNBITagGroup * iTagGroup,DNBITagGroup *iProduct)=0;

};

/**
 * @nodoc
 */
CATDeclareHandler( DNBITagGroupFactory, CATBaseUnknown );
#endif

