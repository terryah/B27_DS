// COPYRIGHT DASSAULT SYSTEMES 2000
//===================================================================
//
// DNBFrameType.h
// Define the DNBFrameType.h
//
//===================================================================
//
// Usage notes:
//   DNBFrameType is used to specify the Frame Types.
//
//===================================================================
//
//  Nov 2000  Creation: Code generated by the CAA wizard  vmap
//  Feb 2002  Documentation                                       GJA
//===================================================================
#ifndef DNBFrameType_H
#define DNBFrameType_H

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

/**
 * Frame Type.
 * @param DNBManufFrame
 *   Manufacturing Frame: Frames used during the manufacturing process
 *   as operation targets.
 * @param DNBDesignFrame
 *   Design Frame: Frames used in the design context. It is normally 
 *   located on a part and can be reused in a joint creation dialog 
 *   to create joints between parts. This feature is used to create 
 *   joints between multiCAD parts.
 * @param DNBToolFrame
 *   Tool Frame: Used to define the tool control point.
 * @param DNBBaseFrame
 *   Base Frame: Used to define the base of the device.
 * @param DNBStackPointFrame
 *   Base Frame: Used to define the Stack Point of the Resources.
 * @param DNBCustomFrame
 *   Custom Frame: For custom user needs.
 */

enum DNBFrameType
{ 
   DNBManufFrame=0,
   DNBDesignFrame=1,
   DNBToolFrame=2,
   DNBBaseFrame=3,
   DNBStackPointFrame=4,
   DNBCustomFrame=10
};

#endif
