// COPYRIGHT DASSAULT SYSTEMES 2003
//===================================================================
//
// DNBListOfRobotTask
//   This interface allows the management of a list of DNBIRobotTask
//
//===================================================================
//
// 17/06/2003 Creation                                        NPJ
//===================================================================
#ifndef DNBListOfRobotTask_H
#define DNBListOfRobotTask_H

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

/**
 * @collection DNBListOfRobotTask
 * Collection class for pointers to DNBIRobotTask.
 * Only the following methods of pointer collection classes are available:
 * <ul>
 * <li><tt>Append</tt></li>
 * <li><tt>Size</tt></li>
 * <li><tt>Operator[]</tt></li>
 * <li><tt>RemovePosition</tt></li>
 * <li><tt>RemoveAll</tt></li>
 * </ul>
 * Refer to the articles dealing with collections in the encyclopedia.
 */

#include "DNBIgpSetupItfCPP.h"
class DNBIRobotTask;

// clean previous functions requests
#include  <CATLISTP_Clean.h>

// require needed functions
#define CATLISTP_Append
#define CATLISTP_RemovePosition
#define CATLISTP_RemoveAll
#define CATLISTP_Swap
// get macros
#include  <CATLISTP_Declare.h>

// generate interface of collection-class
// (functions declarations)
#undef  CATCOLLEC_ExportedBy
#define CATCOLLEC_ExportedBy    ExportedByDNBIgpSetupItfCPP

/**
 * @nodoc
 */
CATLISTP_DECLARE(DNBIRobotTask)
typedef CATLISTP(DNBIRobotTask) DNBListOfRobotTask ;
#endif

