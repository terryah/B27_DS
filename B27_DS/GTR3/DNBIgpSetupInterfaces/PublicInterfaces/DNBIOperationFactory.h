// COPYRIGHT Dassault Systemes 2002
//===================================================================
//
// DNBIOperationFactory.h
// Define the DNBIOperationFactory interface
//
//===================================================================
//
// Usage notes:
//   DNBIOperationFactory: Interface to create Operations.
//
//===================================================================
//
//  Jan 2002  Creation:                                           GJA
//===================================================================
#ifndef DNBIOperationFactory_H
#define DNBIOperationFactory_H

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

#include "DNBIgpSetupItfCPP.h"
#include "CATBaseUnknown.h"

// DNBIgpSetupInterfaces
class DNBIOperation;

// DMAPSInterfaces
class CATISPPChildManagement;
class CATISPPFlowMgt;

extern ExportedByDNBIgpSetupItfCPP IID IID_DNBIOperationFactory;

//------------------------------------------------------------------

/**
 * Interface to create Operations.
 * <b>Role</b>: The Operation can be created as a child of a Robot 
 * Sequence or as a successor of an other Operation. 
 * @see DNBIOperation
 */

class ExportedByDNBIgpSetupItfCPP DNBIOperationFactory: public CATBaseUnknown
{
   /**
    * @nodoc
    */
   CATDeclareInterface;

  public:

   /**
    * Creates a Operation as a child of an other activity.
    * @param iFather
    *   The father of the new Operation.
    * @param oAct
    *   The new Operation.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
    *   </dl>
    */ 
   virtual HRESULT CreateChildOperation(CATISPPChildManagement *iFather, 
                                        DNBIOperation **oAct) const = 0;

   /**
    * Creates a Operation as a successor of an other activity.
    * @param iBefore
    *   The predecessor of the new Operation.
    * @param oAct
    *   The new Operation.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
    *   </dl>
    */ 
   virtual HRESULT CreateAfterOperation(CATISPPFlowMgt *iBefore, 
                                        DNBIOperation **oAct) const = 0;

};

/**
 * @nodoc
 */
CATDeclareHandler( DNBIOperationFactory, CATBaseUnknown );

//------------------------------------------------------------------

#endif
