// COPYRIGHT Dassault Systemes 2006
//===================================================================
//
// DNBIUserActionsToolbar.h
// Define the DNBIUserActionsToolbar interface
//
//===================================================================
//
// Usage notes:
//
// This Interface helps the clients instantiate and expose user defined commannds
// coressponding to actions defined in a particular ".act" file in clients workbench.
//
//===================================================================
//
//  Apr 2006  creation                 npj
//===================================================================
#ifndef DNBIUserActionsToolbar_H
#define DNBIUserActionsToolbar_H

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

#include "CATBaseUnknown.h"
#include "DNBIgpSetupItfCPP.h"

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByDNBIgpSetupItfCPP IID IID_DNBIUserActionsToolbar;
#else
extern "C" const IID IID_DNBIUserActionsToolbar ;
#endif

//------------------------------------------------------------------

class CATListValCATUnicodeString;
class CATUnicodeString;

class ExportedByDNBIgpSetupItfCPP DNBIUserActionsToolbar: public CATBaseUnknown
{
   CATDeclareInterface;
   
public:

      /**
       * This method returns the headers of action commands
       * for the passed in ".act" file as a List of CATUnicodeString. 
       * Usage Notes
       * Calling funtion has to pass a NULL CATListValCATUnicodeString and subsequently 
       * delete the list.
       * @param iLibraryPath
       *   Complete Path of the act file.
       * @param oUserActionCommands
       *   List of User action commands which needs to be appended
       *   to the ToolBar by the caller in their workbench code.
       * @return
       *   An HRESULT.
       *   <br><b>Legal values</b>:
       *   <dl>
       *     <dt>S_OK</dt>
       *     <dd>Command creation is successfull.
       *     <dt>E_FAIL </dt>
       *     <dd>Command creation failed for some reason.
       *   </dl> 
       */
      virtual HRESULT CreateUserActionCommands(CATUnicodeString &iLibraryPath, CATListValCATUnicodeString*& oUserActionCommands) = 0;

};

/**
 * @nodoc
 */
CATDeclareHandler(DNBIUserActionsToolbar,CATBaseUnknown);
//------------------------------------------------------------------

#endif
