#ifndef DNBIRobotTask_H
#define DNBIRobotTask_H

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

// COPYRIGHT DASSAULT SYSTEMES 2003

#include "DNBIgpSetupItfCPP.h"
#include "CATBaseUnknown.h"
#include "DNBListOfOperations.h"
#include "DNBITag.h"
#include "DNBIOperation.h"

class DNBIOperation;
class DNBITag;
class CATUnicodeString;
extern ExportedByDNBIgpSetupItfCPP IID IID_DNBIRobotTask;


/**
 * Interface to access Robot Task.
 * <b>Role:</b>
 * This Interface provides methods to Create/Delete Operation on 
 * the Robot Task.
 */
class ExportedByDNBIgpSetupItfCPP DNBIRobotTask: public CATBaseUnknown
{
  /**
   * @nodoc
   */
   CATDeclareInterface;
   
public:
   
   /**
    * Creates Operation as a child of Robot Task.
    * @param iReferenceOperation
    *   The Reference Operation.
    * @param iAfterOperation
    *   The Operation after which the required operation
    *   is to be created.
    * @param oCreatedOperation
    *   The Created Operation.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
    *   </dl> 
    */
   virtual HRESULT CreateOperation( DNBIOperation* iReferenceOperation,
                                    DNBIOperation* iAfterOperation,
                                    DNBIOperation** oCreatedOperation)=0;
 
      
   /**
    * Creates Operation as a child of Robot Task.
    * @param iOperation
    *   The Operation after which the required operation
    *   is to be created.
    * @iWhere
    *  1->before 0->After the iOperation
    * @param iViaMode
    *   ViaPoint if to set the Target to be a Via Point
    *   UnTyped  if to set it the Target to be not a Via Point
    * @param oCreatedOperation
    *   The Created Operation.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
    *   </dl> 
    */
   virtual HRESULT CreateOperation( DNBIOperation* iOperation,int iWhere,
       const DNBIOperation::DNBViaMode &  iViaMode,DNBIOperation** oCreatedOperation)=0;

   /**
    * Creates Operation as a child of Robot Task.
	* @param iRobotTask
    *   The task to be called in the current robot task
    * @param iAfterOperation
    *   The Operation after which the required operation
    *   is to be created..
	* @param iWhere
	* The postion of where the task needs to be inserted Before(0) After(1)
	* @param oCreatedOperation
	* The created operation
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
    *   </dl> 
    */
   virtual HRESULT CreateCallTask( DNBIRobotTask* iRobotTask, DNBIOperation* iAfterOperation,boolean iWhere,CATBaseUnknown **oCreatedCallTask)=0;
      
   /**
    * Deletes the required Operation.
    * @param iOperation
    *   The Operation to be deleted.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
    *   </dl> 
    */
   virtual HRESULT DeleteOperation( DNBIOperation* iOperation)=0;

   /**
    * Creates Operation as a child of Robot Task.
    * @param iChildType
    *   The Child Type
    * @param oCreatedOperation
    *   The Created Operation.
    * @param iAfterOperation
    *   After the Operation, if NULL then at the Start of the RobotTask
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
    *   </dl> 
    */
   virtual HRESULT CreateChild(const CATUnicodeString & iChildType,
                               DNBIOperation* iAfterOperation,
                               DNBIOperation ** oCreatedOperation)=0;
   /**
    * Retrieves the List of Operations of Robot Task.
    * @param oOperationList
    *   The list of Operations 
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
    *   </dl> 
    */

   virtual HRESULT GetAllOperations(DNBListOfOperations & oOperationList)=0;
   

   /** 
     * Gets the Robot Handle that owns this RobotTask
     * @param oRobot
     *    The Robot Handle
     * @return
     *   An HRESULT.
     *   <br><b>Legal values</b>:
     *   <dl>
     *     <dt>S_OK</dt>
     *     <dd>The component is successfully created
     *         and the interface pointer is successfully returned</dd>
     *     <dt>E_FAIL </dt>
     *     <dd>The component was successfully created,
     *         but the interface query failed</dd>
     *     <dt>E_NOINTERFACE </dt>
     *     <dd>The component was successfully created,
     *         but the it doesn't implement the requested interface</dd>
     *     <dt>E_OUTOFMEMORY </dt>
     *     <dd><dd>The component allocation failed</dd>
     *   </dl>
     */
     // virtual HRESULT GetOwningRobot(DNBIBasicRobot ** oRobot)=0;

};

/**
 * @nodoc
 */
CATDeclareHandler( DNBIRobotTask, CATBaseUnknown );

#endif
