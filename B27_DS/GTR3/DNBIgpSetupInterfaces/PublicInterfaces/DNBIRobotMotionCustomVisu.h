// COPYRIGHT DASSAULT SYSTEMES 2013
//===================================================================
//
// DNBIRobotMotionCustomVisu.h
// Define the DNBIRobotMotionCustomVisu interface.
//
//===================================================================
//
// Usage notes:
//   DNBIRobotMotionCustomVisu: This Interface provides way to get a customized visu for Robot Motion activity.
// 
//===================================================================

#ifndef DNBIRobotMotionCustomVisu_H
#define DNBIRobotMotionCustomVisu_H

class CATRep;

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

#include "DNBIgpSetupItfCPP.h"

// System
#include "CATBaseUnknown.h"
#include "CATLISTV_CATBaseUnknown.h"

extern ExportedByDNBIgpSetupItfCPP IID IID_DNBIRobotMotionCustomVisu ;

//------------------------------------------------------------------

/**
 * <p>
 * <b>Role</b>:
 * This Interface provides a way to get a customized visu for Robot Motion activity.
 */

class ExportedByDNBIgpSetupItfCPP DNBIRobotMotionCustomVisu: public CATBaseUnknown
{
  /**
   * @nodoc
   */
   CATDeclareInterface;

public:

    /**
      * Gives the customized visual representation for given Robot Motion activity.
      * @param iRobotMotion
      *   the input Robot Motion activity for which custom visu is required
      * @return 
      * A pointer to the graphical representation.      
      */ 
     virtual CATRep* GetCustomRep(const CATBaseUnknown_var &iRobotMotion) = 0;


    /**
      * To decide about showing the default visual representation of Robot Motion activity.
      * @param iRobotMotion
      *   the input Robot Motion activity
      * @return 
      * TRUE if default representation is required and FALSE if not required
      */ 
     virtual boolean ShowDefaultMotionRep(const CATBaseUnknown_var &iRobotMotionActy) = 0;
};

/**
 * @nodoc
 */
CATDeclareHandler( DNBIRobotMotionCustomVisu, CATBaseUnknown );

#endif
