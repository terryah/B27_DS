#ifndef __TIE_DNBIASimTraceSettingAtt
#define __TIE_DNBIASimTraceSettingAtt

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIASimTraceSettingAtt.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIASimTraceSettingAtt */
#define declare_TIE_DNBIASimTraceSettingAtt(classe) \
 \
 \
class TIEDNBIASimTraceSettingAtt##classe : public DNBIASimTraceSettingAtt \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIASimTraceSettingAtt, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall get_TracePointVisu(CAT_VARIANT_BOOL & oVisu); \
      virtual HRESULT __stdcall put_TracePointVisu(CAT_VARIANT_BOOL iVisu); \
      virtual HRESULT __stdcall GetTracePointVisuInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetTracePointVisuLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_TracePointColor(CATSafeArrayVariant *& oColor); \
      virtual HRESULT __stdcall put_TracePointColor(const CATSafeArrayVariant & iColor); \
      virtual HRESULT __stdcall GetTracePointColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetTracePointColorLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_TracePointSymbol(CATLONG & oType); \
      virtual HRESULT __stdcall put_TracePointSymbol(CATLONG iType); \
      virtual HRESULT __stdcall GetTracePointSymbolInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetTracePointSymbolLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_TraceLineVisu(CAT_VARIANT_BOOL & oVisu); \
      virtual HRESULT __stdcall put_TraceLineVisu(CAT_VARIANT_BOOL iVisu); \
      virtual HRESULT __stdcall GetTraceLineVisuInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetTraceLineVisuLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_TraceLineColor(CATSafeArrayVariant *& oColor); \
      virtual HRESULT __stdcall put_TraceLineColor(const CATSafeArrayVariant & iColor); \
      virtual HRESULT __stdcall GetTraceLineColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetTraceLineColorLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_TraceLineType(CATLONG & oType); \
      virtual HRESULT __stdcall put_TraceLineType(CATLONG iType); \
      virtual HRESULT __stdcall GetTraceLineTypeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetTraceLineTypeLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_TraceLineThick(CATLONG & oType); \
      virtual HRESULT __stdcall put_TraceLineThick(CATLONG iType); \
      virtual HRESULT __stdcall GetTraceLineThickInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetTraceLineThickLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_TraceAxisVisu(CAT_VARIANT_BOOL & oVisu); \
      virtual HRESULT __stdcall put_TraceAxisVisu(CAT_VARIANT_BOOL iVisu); \
      virtual HRESULT __stdcall GetTraceAxisVisuInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetTraceAxisVisuLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_TraceAxisColor(CATSafeArrayVariant *& oColor); \
      virtual HRESULT __stdcall put_TraceAxisColor(const CATSafeArrayVariant & iColor); \
      virtual HRESULT __stdcall GetTraceAxisColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetTraceAxisColorLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_TraceAxisType(CATLONG & oType); \
      virtual HRESULT __stdcall put_TraceAxisType(CATLONG iType); \
      virtual HRESULT __stdcall GetTraceAxisTypeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetTraceAxisTypeLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_TraceAxisThick(CATLONG & oType); \
      virtual HRESULT __stdcall put_TraceAxisThick(CATLONG iType); \
      virtual HRESULT __stdcall GetTraceAxisThickInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetTraceAxisThickLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_TraceDeletePath(CAT_VARIANT_BOOL & oDelete); \
      virtual HRESULT __stdcall put_TraceDeletePath(CAT_VARIANT_BOOL iDelete); \
      virtual HRESULT __stdcall GetTraceDeletePathInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetTraceDeletePathLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_TraceLegend(CATSafeArrayVariant *& oLegend); \
      virtual HRESULT __stdcall put_TraceLegend(const CATSafeArrayVariant & iLegend); \
      virtual HRESULT __stdcall GetTraceLegendInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetTraceLegendLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall Commit(); \
      virtual HRESULT __stdcall Rollback(); \
      virtual HRESULT __stdcall ResetToAdminValues(); \
      virtual HRESULT __stdcall ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList); \
      virtual HRESULT __stdcall SaveRepository(); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIASimTraceSettingAtt(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall get_TracePointVisu(CAT_VARIANT_BOOL & oVisu); \
virtual HRESULT __stdcall put_TracePointVisu(CAT_VARIANT_BOOL iVisu); \
virtual HRESULT __stdcall GetTracePointVisuInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetTracePointVisuLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_TracePointColor(CATSafeArrayVariant *& oColor); \
virtual HRESULT __stdcall put_TracePointColor(const CATSafeArrayVariant & iColor); \
virtual HRESULT __stdcall GetTracePointColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetTracePointColorLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_TracePointSymbol(CATLONG & oType); \
virtual HRESULT __stdcall put_TracePointSymbol(CATLONG iType); \
virtual HRESULT __stdcall GetTracePointSymbolInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetTracePointSymbolLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_TraceLineVisu(CAT_VARIANT_BOOL & oVisu); \
virtual HRESULT __stdcall put_TraceLineVisu(CAT_VARIANT_BOOL iVisu); \
virtual HRESULT __stdcall GetTraceLineVisuInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetTraceLineVisuLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_TraceLineColor(CATSafeArrayVariant *& oColor); \
virtual HRESULT __stdcall put_TraceLineColor(const CATSafeArrayVariant & iColor); \
virtual HRESULT __stdcall GetTraceLineColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetTraceLineColorLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_TraceLineType(CATLONG & oType); \
virtual HRESULT __stdcall put_TraceLineType(CATLONG iType); \
virtual HRESULT __stdcall GetTraceLineTypeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetTraceLineTypeLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_TraceLineThick(CATLONG & oType); \
virtual HRESULT __stdcall put_TraceLineThick(CATLONG iType); \
virtual HRESULT __stdcall GetTraceLineThickInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetTraceLineThickLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_TraceAxisVisu(CAT_VARIANT_BOOL & oVisu); \
virtual HRESULT __stdcall put_TraceAxisVisu(CAT_VARIANT_BOOL iVisu); \
virtual HRESULT __stdcall GetTraceAxisVisuInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetTraceAxisVisuLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_TraceAxisColor(CATSafeArrayVariant *& oColor); \
virtual HRESULT __stdcall put_TraceAxisColor(const CATSafeArrayVariant & iColor); \
virtual HRESULT __stdcall GetTraceAxisColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetTraceAxisColorLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_TraceAxisType(CATLONG & oType); \
virtual HRESULT __stdcall put_TraceAxisType(CATLONG iType); \
virtual HRESULT __stdcall GetTraceAxisTypeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetTraceAxisTypeLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_TraceAxisThick(CATLONG & oType); \
virtual HRESULT __stdcall put_TraceAxisThick(CATLONG iType); \
virtual HRESULT __stdcall GetTraceAxisThickInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetTraceAxisThickLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_TraceDeletePath(CAT_VARIANT_BOOL & oDelete); \
virtual HRESULT __stdcall put_TraceDeletePath(CAT_VARIANT_BOOL iDelete); \
virtual HRESULT __stdcall GetTraceDeletePathInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetTraceDeletePathLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_TraceLegend(CATSafeArrayVariant *& oLegend); \
virtual HRESULT __stdcall put_TraceLegend(const CATSafeArrayVariant & iLegend); \
virtual HRESULT __stdcall GetTraceLegendInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetTraceLegendLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall Commit(); \
virtual HRESULT __stdcall Rollback(); \
virtual HRESULT __stdcall ResetToAdminValues(); \
virtual HRESULT __stdcall ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList); \
virtual HRESULT __stdcall SaveRepository(); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIASimTraceSettingAtt(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::get_TracePointVisu(CAT_VARIANT_BOOL & oVisu) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_TracePointVisu(oVisu)); \
} \
HRESULT __stdcall  ENVTIEName::put_TracePointVisu(CAT_VARIANT_BOOL iVisu) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_TracePointVisu(iVisu)); \
} \
HRESULT __stdcall  ENVTIEName::GetTracePointVisuInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetTracePointVisuInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetTracePointVisuLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetTracePointVisuLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_TracePointColor(CATSafeArrayVariant *& oColor) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_TracePointColor(oColor)); \
} \
HRESULT __stdcall  ENVTIEName::put_TracePointColor(const CATSafeArrayVariant & iColor) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_TracePointColor(iColor)); \
} \
HRESULT __stdcall  ENVTIEName::GetTracePointColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetTracePointColorInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetTracePointColorLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetTracePointColorLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_TracePointSymbol(CATLONG & oType) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_TracePointSymbol(oType)); \
} \
HRESULT __stdcall  ENVTIEName::put_TracePointSymbol(CATLONG iType) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_TracePointSymbol(iType)); \
} \
HRESULT __stdcall  ENVTIEName::GetTracePointSymbolInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetTracePointSymbolInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetTracePointSymbolLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetTracePointSymbolLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_TraceLineVisu(CAT_VARIANT_BOOL & oVisu) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_TraceLineVisu(oVisu)); \
} \
HRESULT __stdcall  ENVTIEName::put_TraceLineVisu(CAT_VARIANT_BOOL iVisu) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_TraceLineVisu(iVisu)); \
} \
HRESULT __stdcall  ENVTIEName::GetTraceLineVisuInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetTraceLineVisuInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetTraceLineVisuLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetTraceLineVisuLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_TraceLineColor(CATSafeArrayVariant *& oColor) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_TraceLineColor(oColor)); \
} \
HRESULT __stdcall  ENVTIEName::put_TraceLineColor(const CATSafeArrayVariant & iColor) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_TraceLineColor(iColor)); \
} \
HRESULT __stdcall  ENVTIEName::GetTraceLineColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetTraceLineColorInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetTraceLineColorLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetTraceLineColorLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_TraceLineType(CATLONG & oType) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_TraceLineType(oType)); \
} \
HRESULT __stdcall  ENVTIEName::put_TraceLineType(CATLONG iType) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_TraceLineType(iType)); \
} \
HRESULT __stdcall  ENVTIEName::GetTraceLineTypeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetTraceLineTypeInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetTraceLineTypeLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetTraceLineTypeLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_TraceLineThick(CATLONG & oType) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_TraceLineThick(oType)); \
} \
HRESULT __stdcall  ENVTIEName::put_TraceLineThick(CATLONG iType) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_TraceLineThick(iType)); \
} \
HRESULT __stdcall  ENVTIEName::GetTraceLineThickInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetTraceLineThickInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetTraceLineThickLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetTraceLineThickLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_TraceAxisVisu(CAT_VARIANT_BOOL & oVisu) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_TraceAxisVisu(oVisu)); \
} \
HRESULT __stdcall  ENVTIEName::put_TraceAxisVisu(CAT_VARIANT_BOOL iVisu) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_TraceAxisVisu(iVisu)); \
} \
HRESULT __stdcall  ENVTIEName::GetTraceAxisVisuInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetTraceAxisVisuInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetTraceAxisVisuLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetTraceAxisVisuLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_TraceAxisColor(CATSafeArrayVariant *& oColor) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_TraceAxisColor(oColor)); \
} \
HRESULT __stdcall  ENVTIEName::put_TraceAxisColor(const CATSafeArrayVariant & iColor) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_TraceAxisColor(iColor)); \
} \
HRESULT __stdcall  ENVTIEName::GetTraceAxisColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetTraceAxisColorInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetTraceAxisColorLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetTraceAxisColorLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_TraceAxisType(CATLONG & oType) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_TraceAxisType(oType)); \
} \
HRESULT __stdcall  ENVTIEName::put_TraceAxisType(CATLONG iType) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_TraceAxisType(iType)); \
} \
HRESULT __stdcall  ENVTIEName::GetTraceAxisTypeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetTraceAxisTypeInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetTraceAxisTypeLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetTraceAxisTypeLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_TraceAxisThick(CATLONG & oType) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_TraceAxisThick(oType)); \
} \
HRESULT __stdcall  ENVTIEName::put_TraceAxisThick(CATLONG iType) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_TraceAxisThick(iType)); \
} \
HRESULT __stdcall  ENVTIEName::GetTraceAxisThickInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetTraceAxisThickInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetTraceAxisThickLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetTraceAxisThickLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_TraceDeletePath(CAT_VARIANT_BOOL & oDelete) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_TraceDeletePath(oDelete)); \
} \
HRESULT __stdcall  ENVTIEName::put_TraceDeletePath(CAT_VARIANT_BOOL iDelete) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_TraceDeletePath(iDelete)); \
} \
HRESULT __stdcall  ENVTIEName::GetTraceDeletePathInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetTraceDeletePathInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetTraceDeletePathLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetTraceDeletePathLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_TraceLegend(CATSafeArrayVariant *& oLegend) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_TraceLegend(oLegend)); \
} \
HRESULT __stdcall  ENVTIEName::put_TraceLegend(const CATSafeArrayVariant & iLegend) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_TraceLegend(iLegend)); \
} \
HRESULT __stdcall  ENVTIEName::GetTraceLegendInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetTraceLegendInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetTraceLegendLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetTraceLegendLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::Commit() \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)Commit()); \
} \
HRESULT __stdcall  ENVTIEName::Rollback() \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)Rollback()); \
} \
HRESULT __stdcall  ENVTIEName::ResetToAdminValues() \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)ResetToAdminValues()); \
} \
HRESULT __stdcall  ENVTIEName::ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)ResetToAdminValuesByName(iAttList)); \
} \
HRESULT __stdcall  ENVTIEName::SaveRepository() \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)SaveRepository()); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIASimTraceSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIASimTraceSettingAtt(classe)    TIEDNBIASimTraceSettingAtt##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIASimTraceSettingAtt(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIASimTraceSettingAtt, classe) \
 \
 \
CATImplementTIEMethods(DNBIASimTraceSettingAtt, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIASimTraceSettingAtt, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIASimTraceSettingAtt, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIASimTraceSettingAtt, classe) \
 \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::get_TracePointVisu(CAT_VARIANT_BOOL & oVisu) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&oVisu); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_TracePointVisu(oVisu); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&oVisu); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::put_TracePointVisu(CAT_VARIANT_BOOL iVisu) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&iVisu); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_TracePointVisu(iVisu); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&iVisu); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::GetTracePointVisuInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetTracePointVisuInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::SetTracePointVisuLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetTracePointVisuLock(iLocked); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::get_TracePointColor(CATSafeArrayVariant *& oColor) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&oColor); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_TracePointColor(oColor); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&oColor); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::put_TracePointColor(const CATSafeArrayVariant & iColor) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&iColor); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_TracePointColor(iColor); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&iColor); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::GetTracePointColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetTracePointColorInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::SetTracePointColorLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetTracePointColorLock(iLocked); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::get_TracePointSymbol(CATLONG & oType) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&oType); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_TracePointSymbol(oType); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&oType); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::put_TracePointSymbol(CATLONG iType) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&iType); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_TracePointSymbol(iType); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&iType); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::GetTracePointSymbolInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,11,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetTracePointSymbolInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,11,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::SetTracePointSymbolLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,12,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetTracePointSymbolLock(iLocked); \
   ExitAfterCall(this,12,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::get_TraceLineVisu(CAT_VARIANT_BOOL & oVisu) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,13,&_Trac2,&oVisu); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_TraceLineVisu(oVisu); \
   ExitAfterCall(this,13,_Trac2,&_ret_arg,&oVisu); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::put_TraceLineVisu(CAT_VARIANT_BOOL iVisu) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,14,&_Trac2,&iVisu); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_TraceLineVisu(iVisu); \
   ExitAfterCall(this,14,_Trac2,&_ret_arg,&iVisu); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::GetTraceLineVisuInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,15,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetTraceLineVisuInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,15,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::SetTraceLineVisuLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,16,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetTraceLineVisuLock(iLocked); \
   ExitAfterCall(this,16,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::get_TraceLineColor(CATSafeArrayVariant *& oColor) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,17,&_Trac2,&oColor); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_TraceLineColor(oColor); \
   ExitAfterCall(this,17,_Trac2,&_ret_arg,&oColor); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::put_TraceLineColor(const CATSafeArrayVariant & iColor) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,18,&_Trac2,&iColor); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_TraceLineColor(iColor); \
   ExitAfterCall(this,18,_Trac2,&_ret_arg,&iColor); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::GetTraceLineColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,19,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetTraceLineColorInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,19,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::SetTraceLineColorLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,20,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetTraceLineColorLock(iLocked); \
   ExitAfterCall(this,20,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::get_TraceLineType(CATLONG & oType) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,21,&_Trac2,&oType); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_TraceLineType(oType); \
   ExitAfterCall(this,21,_Trac2,&_ret_arg,&oType); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::put_TraceLineType(CATLONG iType) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,22,&_Trac2,&iType); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_TraceLineType(iType); \
   ExitAfterCall(this,22,_Trac2,&_ret_arg,&iType); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::GetTraceLineTypeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,23,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetTraceLineTypeInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,23,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::SetTraceLineTypeLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,24,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetTraceLineTypeLock(iLocked); \
   ExitAfterCall(this,24,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::get_TraceLineThick(CATLONG & oType) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,25,&_Trac2,&oType); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_TraceLineThick(oType); \
   ExitAfterCall(this,25,_Trac2,&_ret_arg,&oType); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::put_TraceLineThick(CATLONG iType) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,26,&_Trac2,&iType); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_TraceLineThick(iType); \
   ExitAfterCall(this,26,_Trac2,&_ret_arg,&iType); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::GetTraceLineThickInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,27,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetTraceLineThickInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,27,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::SetTraceLineThickLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,28,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetTraceLineThickLock(iLocked); \
   ExitAfterCall(this,28,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::get_TraceAxisVisu(CAT_VARIANT_BOOL & oVisu) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,29,&_Trac2,&oVisu); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_TraceAxisVisu(oVisu); \
   ExitAfterCall(this,29,_Trac2,&_ret_arg,&oVisu); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::put_TraceAxisVisu(CAT_VARIANT_BOOL iVisu) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,30,&_Trac2,&iVisu); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_TraceAxisVisu(iVisu); \
   ExitAfterCall(this,30,_Trac2,&_ret_arg,&iVisu); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::GetTraceAxisVisuInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,31,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetTraceAxisVisuInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,31,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::SetTraceAxisVisuLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,32,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetTraceAxisVisuLock(iLocked); \
   ExitAfterCall(this,32,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::get_TraceAxisColor(CATSafeArrayVariant *& oColor) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,33,&_Trac2,&oColor); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_TraceAxisColor(oColor); \
   ExitAfterCall(this,33,_Trac2,&_ret_arg,&oColor); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::put_TraceAxisColor(const CATSafeArrayVariant & iColor) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,34,&_Trac2,&iColor); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_TraceAxisColor(iColor); \
   ExitAfterCall(this,34,_Trac2,&_ret_arg,&iColor); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::GetTraceAxisColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,35,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetTraceAxisColorInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,35,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::SetTraceAxisColorLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,36,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetTraceAxisColorLock(iLocked); \
   ExitAfterCall(this,36,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::get_TraceAxisType(CATLONG & oType) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,37,&_Trac2,&oType); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_TraceAxisType(oType); \
   ExitAfterCall(this,37,_Trac2,&_ret_arg,&oType); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::put_TraceAxisType(CATLONG iType) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,38,&_Trac2,&iType); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_TraceAxisType(iType); \
   ExitAfterCall(this,38,_Trac2,&_ret_arg,&iType); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::GetTraceAxisTypeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,39,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetTraceAxisTypeInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,39,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::SetTraceAxisTypeLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,40,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetTraceAxisTypeLock(iLocked); \
   ExitAfterCall(this,40,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::get_TraceAxisThick(CATLONG & oType) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,41,&_Trac2,&oType); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_TraceAxisThick(oType); \
   ExitAfterCall(this,41,_Trac2,&_ret_arg,&oType); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::put_TraceAxisThick(CATLONG iType) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,42,&_Trac2,&iType); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_TraceAxisThick(iType); \
   ExitAfterCall(this,42,_Trac2,&_ret_arg,&iType); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::GetTraceAxisThickInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,43,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetTraceAxisThickInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,43,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::SetTraceAxisThickLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,44,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetTraceAxisThickLock(iLocked); \
   ExitAfterCall(this,44,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::get_TraceDeletePath(CAT_VARIANT_BOOL & oDelete) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,45,&_Trac2,&oDelete); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_TraceDeletePath(oDelete); \
   ExitAfterCall(this,45,_Trac2,&_ret_arg,&oDelete); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::put_TraceDeletePath(CAT_VARIANT_BOOL iDelete) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,46,&_Trac2,&iDelete); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_TraceDeletePath(iDelete); \
   ExitAfterCall(this,46,_Trac2,&_ret_arg,&iDelete); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::GetTraceDeletePathInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,47,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetTraceDeletePathInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,47,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::SetTraceDeletePathLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,48,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetTraceDeletePathLock(iLocked); \
   ExitAfterCall(this,48,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::get_TraceLegend(CATSafeArrayVariant *& oLegend) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,49,&_Trac2,&oLegend); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_TraceLegend(oLegend); \
   ExitAfterCall(this,49,_Trac2,&_ret_arg,&oLegend); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::put_TraceLegend(const CATSafeArrayVariant & iLegend) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,50,&_Trac2,&iLegend); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_TraceLegend(iLegend); \
   ExitAfterCall(this,50,_Trac2,&_ret_arg,&iLegend); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::GetTraceLegendInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,51,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetTraceLegendInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,51,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::SetTraceLegendLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,52,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetTraceLegendLock(iLocked); \
   ExitAfterCall(this,52,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::Commit() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,53,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Commit(); \
   ExitAfterCall(this,53,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::Rollback() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,54,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Rollback(); \
   ExitAfterCall(this,54,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::ResetToAdminValues() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,55,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ResetToAdminValues(); \
   ExitAfterCall(this,55,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,56,&_Trac2,&iAttList); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ResetToAdminValuesByName(iAttList); \
   ExitAfterCall(this,56,_Trac2,&_ret_arg,&iAttList); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimTraceSettingAtt##classe::SaveRepository() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,57,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SaveRepository(); \
   ExitAfterCall(this,57,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIASimTraceSettingAtt##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,58,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,58,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIASimTraceSettingAtt##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,59,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,59,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIASimTraceSettingAtt##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,60,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,60,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIASimTraceSettingAtt##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,61,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,61,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIASimTraceSettingAtt##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,62,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,62,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIASimTraceSettingAtt(classe) \
 \
 \
declare_TIE_DNBIASimTraceSettingAtt(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIASimTraceSettingAtt##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIASimTraceSettingAtt,"DNBIASimTraceSettingAtt",DNBIASimTraceSettingAtt::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIASimTraceSettingAtt(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIASimTraceSettingAtt, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIASimTraceSettingAtt##classe(classe::MetaObject(),DNBIASimTraceSettingAtt::MetaObject(),(void *)CreateTIEDNBIASimTraceSettingAtt##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIASimTraceSettingAtt(classe) \
 \
 \
declare_TIE_DNBIASimTraceSettingAtt(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIASimTraceSettingAtt##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIASimTraceSettingAtt,"DNBIASimTraceSettingAtt",DNBIASimTraceSettingAtt::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIASimTraceSettingAtt(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIASimTraceSettingAtt, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIASimTraceSettingAtt##classe(classe::MetaObject(),DNBIASimTraceSettingAtt::MetaObject(),(void *)CreateTIEDNBIASimTraceSettingAtt##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIASimTraceSettingAtt(classe) TIE_DNBIASimTraceSettingAtt(classe)
#else
#define BOA_DNBIASimTraceSettingAtt(classe) CATImplementBOA(DNBIASimTraceSettingAtt, classe)
#endif

#endif
