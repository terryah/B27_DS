#ifndef __TIE_DNBIASimulationSettingAtt
#define __TIE_DNBIASimulationSettingAtt

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIASimulationSettingAtt.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIASimulationSettingAtt */
#define declare_TIE_DNBIASimulationSettingAtt(classe) \
 \
 \
class TIEDNBIASimulationSettingAtt##classe : public DNBIASimulationSettingAtt \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIASimulationSettingAtt, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall get_AthStateMgt(CAT_VARIANT_BOOL & oAthStateMgt); \
      virtual HRESULT __stdcall put_AthStateMgt(CAT_VARIANT_BOOL iAthStateMgt); \
      virtual HRESULT __stdcall GetAthStateMgtInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetAthStateMgtLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_AthEndCondition(CAT_VARIANT_BOOL & oAthEndCondition); \
      virtual HRESULT __stdcall put_AthEndCondition(CAT_VARIANT_BOOL iAthEndCondition); \
      virtual HRESULT __stdcall GetAthEndConditionInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetAthEndConditionLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_AthTextMsg(CAT_VARIANT_BOOL & oAthTextMsg); \
      virtual HRESULT __stdcall put_AthTextMsg(CAT_VARIANT_BOOL iAthTextMsg); \
      virtual HRESULT __stdcall GetAthTextMsgInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetAthTextMsgLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_AthViewpoint(CAT_VARIANT_BOOL & oAthViewpoint); \
      virtual HRESULT __stdcall put_AthViewpoint(CAT_VARIANT_BOOL iAthViewpoint); \
      virtual HRESULT __stdcall GetAthViewpointInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetAthViewpointLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_AthHyperlink(CAT_VARIANT_BOOL & oAthHyperlink); \
      virtual HRESULT __stdcall put_AthHyperlink(CAT_VARIANT_BOOL iAthHyperlink); \
      virtual HRESULT __stdcall GetAthHyperlinkInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetAthHyperlinkLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_AthAnnotation(CAT_VARIANT_BOOL & oAthAnnotation); \
      virtual HRESULT __stdcall put_AthAnnotation(CAT_VARIANT_BOOL iAthAnnotation); \
      virtual HRESULT __stdcall GetAthAnnotationInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetAthAnnotationLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_AthDisableSim(CAT_VARIANT_BOOL & oAthDisableSim); \
      virtual HRESULT __stdcall put_AthDisableSim(CAT_VARIANT_BOOL iAthDisableSim); \
      virtual HRESULT __stdcall GetAthDisableSimInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetAthDisableSimLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_AthSelAgentDlg(CAT_VARIANT_BOOL & oAthSelAgentDlg); \
      virtual HRESULT __stdcall put_AthSelAgentDlg(CAT_VARIANT_BOOL iAthSelAgentDlg); \
      virtual HRESULT __stdcall GetAthSelAgentDlgInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetAthSelAgentDlgLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_RunStateMgt(CAT_VARIANT_BOOL & oRunStateMgt); \
      virtual HRESULT __stdcall put_RunStateMgt(CAT_VARIANT_BOOL iRunStateMgt); \
      virtual HRESULT __stdcall GetRunStateMgtInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetRunStateMgtLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_RunEndCondition(CAT_VARIANT_BOOL & oRunEndCondition); \
      virtual HRESULT __stdcall put_RunEndCondition(CAT_VARIANT_BOOL iRunEndCondition); \
      virtual HRESULT __stdcall GetRunEndConditionInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetRunEndConditionLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_RunTextMsg(CAT_VARIANT_BOOL & oRunTextMsg); \
      virtual HRESULT __stdcall put_RunTextMsg(CAT_VARIANT_BOOL iRunTextMsg); \
      virtual HRESULT __stdcall GetRunTextMsgInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetRunTextMsgLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_RunViewpoint(CAT_VARIANT_BOOL & oRunViewpoint); \
      virtual HRESULT __stdcall put_RunViewpoint(CAT_VARIANT_BOOL iRunViewpoint); \
      virtual HRESULT __stdcall GetRunViewpointInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetRunViewpointLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_RunHyperlink(CAT_VARIANT_BOOL & oRunHyperlink); \
      virtual HRESULT __stdcall put_RunHyperlink(CAT_VARIANT_BOOL iRunHyperlink); \
      virtual HRESULT __stdcall GetRunHyperlinkInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetRunHyperlinkLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_RunAnnotation(CAT_VARIANT_BOOL & oRunAnnotation); \
      virtual HRESULT __stdcall put_RunAnnotation(CAT_VARIANT_BOOL iRunAnnotation); \
      virtual HRESULT __stdcall GetRunAnnotationInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetRunAnnotationLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_RunPause(CAT_VARIANT_BOOL & oRunPause); \
      virtual HRESULT __stdcall put_RunPause(CAT_VARIANT_BOOL iRunPause); \
      virtual HRESULT __stdcall GetRunPauseInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetRunPauseLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_RunStepSize(float & oRunStepSize); \
      virtual HRESULT __stdcall put_RunStepSize(float iRunStepSize); \
      virtual HRESULT __stdcall GetRunStepSizeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetRunStepSizeLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_PSDynClashMode(DNBVisualizationMode & oPSDynClashMode); \
      virtual HRESULT __stdcall put_PSDynClashMode(DNBVisualizationMode iPSDynClashMode); \
      virtual HRESULT __stdcall GetPSDynClashModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetPSDynClashModeLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_PSCycleTime(CAT_VARIANT_BOOL & oPSCycleTime); \
      virtual HRESULT __stdcall put_PSCycleTime(CAT_VARIANT_BOOL iPSCycleTime); \
      virtual HRESULT __stdcall GetPSCycleTimeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetPSCycleTimeLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_PVStateMgt(CAT_VARIANT_BOOL & oPVStateMgt); \
      virtual HRESULT __stdcall put_PVStateMgt(CAT_VARIANT_BOOL iPVStateMgt); \
      virtual HRESULT __stdcall GetPVStateMgtInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetPVStateMgtLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_PVEndCondition(CAT_VARIANT_BOOL & oPVEndCondition); \
      virtual HRESULT __stdcall put_PVEndCondition(CAT_VARIANT_BOOL iPVEndCondition); \
      virtual HRESULT __stdcall GetPVEndConditionInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetPVEndConditionLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_ASNavigationMode(DNBSimNavigationMode & oASNavigationMode); \
      virtual HRESULT __stdcall put_ASNavigationMode(DNBSimNavigationMode iASNavigationMode); \
      virtual HRESULT __stdcall GetASNavigationModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetASNavigationModeLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_ASStepSize(float & oASStepSize); \
      virtual HRESULT __stdcall put_ASStepSize(float iASStepSize); \
      virtual HRESULT __stdcall GetASStepSizeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetASStepSizeLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_RunTextBehavior(DNBActBehaviorType & oRunTextBehavior); \
      virtual HRESULT __stdcall put_RunTextBehavior(DNBActBehaviorType iRunTextBehavior); \
      virtual HRESULT __stdcall GetRunTextBehaviorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetRunTextBehaviorLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_RunHlnkBehavior(DNBHlnkBehaviorType & oRunHlnkBehavior); \
      virtual HRESULT __stdcall put_RunHlnkBehavior(DNBHlnkBehaviorType iRunHlnkBehavior); \
      virtual HRESULT __stdcall GetRunHlnkBehaviorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetRunHlnkBehaviorLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_RunAnnotBehavior(DNBActBehaviorType & oRunAnnotBehavior); \
      virtual HRESULT __stdcall put_RunAnnotBehavior(DNBActBehaviorType iRunAnnotBehavior); \
      virtual HRESULT __stdcall GetRunAnnotBehaviorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetRunAnnotBehaviorLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_RunVisBehavior(DNBActBehaviorType & oRunVisBehavior); \
      virtual HRESULT __stdcall put_RunVisBehavior(DNBActBehaviorType iRunVisBehavior); \
      virtual HRESULT __stdcall GetRunVisBehaviorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetRunVisBehaviorLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_PSGraphicUpdate(DNBSimGraphUpdateMode & oPSGraphicUpdate); \
      virtual HRESULT __stdcall put_PSGraphicUpdate(DNBSimGraphUpdateMode iPSGraphicUpdate); \
      virtual HRESULT __stdcall GetPSGraphicUpdateInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetPSGraphicUpdateLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_PSGraphicSimStep(CATLONG & oPSGraphicSimStep); \
      virtual HRESULT __stdcall put_PSGraphicSimStep(CATLONG iPSGraphicSimStep); \
      virtual HRESULT __stdcall GetPSGraphicSimStepInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetPSGraphicSimStepLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall Commit(); \
      virtual HRESULT __stdcall Rollback(); \
      virtual HRESULT __stdcall ResetToAdminValues(); \
      virtual HRESULT __stdcall ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList); \
      virtual HRESULT __stdcall SaveRepository(); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIASimulationSettingAtt(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall get_AthStateMgt(CAT_VARIANT_BOOL & oAthStateMgt); \
virtual HRESULT __stdcall put_AthStateMgt(CAT_VARIANT_BOOL iAthStateMgt); \
virtual HRESULT __stdcall GetAthStateMgtInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetAthStateMgtLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_AthEndCondition(CAT_VARIANT_BOOL & oAthEndCondition); \
virtual HRESULT __stdcall put_AthEndCondition(CAT_VARIANT_BOOL iAthEndCondition); \
virtual HRESULT __stdcall GetAthEndConditionInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetAthEndConditionLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_AthTextMsg(CAT_VARIANT_BOOL & oAthTextMsg); \
virtual HRESULT __stdcall put_AthTextMsg(CAT_VARIANT_BOOL iAthTextMsg); \
virtual HRESULT __stdcall GetAthTextMsgInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetAthTextMsgLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_AthViewpoint(CAT_VARIANT_BOOL & oAthViewpoint); \
virtual HRESULT __stdcall put_AthViewpoint(CAT_VARIANT_BOOL iAthViewpoint); \
virtual HRESULT __stdcall GetAthViewpointInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetAthViewpointLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_AthHyperlink(CAT_VARIANT_BOOL & oAthHyperlink); \
virtual HRESULT __stdcall put_AthHyperlink(CAT_VARIANT_BOOL iAthHyperlink); \
virtual HRESULT __stdcall GetAthHyperlinkInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetAthHyperlinkLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_AthAnnotation(CAT_VARIANT_BOOL & oAthAnnotation); \
virtual HRESULT __stdcall put_AthAnnotation(CAT_VARIANT_BOOL iAthAnnotation); \
virtual HRESULT __stdcall GetAthAnnotationInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetAthAnnotationLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_AthDisableSim(CAT_VARIANT_BOOL & oAthDisableSim); \
virtual HRESULT __stdcall put_AthDisableSim(CAT_VARIANT_BOOL iAthDisableSim); \
virtual HRESULT __stdcall GetAthDisableSimInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetAthDisableSimLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_AthSelAgentDlg(CAT_VARIANT_BOOL & oAthSelAgentDlg); \
virtual HRESULT __stdcall put_AthSelAgentDlg(CAT_VARIANT_BOOL iAthSelAgentDlg); \
virtual HRESULT __stdcall GetAthSelAgentDlgInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetAthSelAgentDlgLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_RunStateMgt(CAT_VARIANT_BOOL & oRunStateMgt); \
virtual HRESULT __stdcall put_RunStateMgt(CAT_VARIANT_BOOL iRunStateMgt); \
virtual HRESULT __stdcall GetRunStateMgtInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetRunStateMgtLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_RunEndCondition(CAT_VARIANT_BOOL & oRunEndCondition); \
virtual HRESULT __stdcall put_RunEndCondition(CAT_VARIANT_BOOL iRunEndCondition); \
virtual HRESULT __stdcall GetRunEndConditionInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetRunEndConditionLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_RunTextMsg(CAT_VARIANT_BOOL & oRunTextMsg); \
virtual HRESULT __stdcall put_RunTextMsg(CAT_VARIANT_BOOL iRunTextMsg); \
virtual HRESULT __stdcall GetRunTextMsgInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetRunTextMsgLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_RunViewpoint(CAT_VARIANT_BOOL & oRunViewpoint); \
virtual HRESULT __stdcall put_RunViewpoint(CAT_VARIANT_BOOL iRunViewpoint); \
virtual HRESULT __stdcall GetRunViewpointInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetRunViewpointLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_RunHyperlink(CAT_VARIANT_BOOL & oRunHyperlink); \
virtual HRESULT __stdcall put_RunHyperlink(CAT_VARIANT_BOOL iRunHyperlink); \
virtual HRESULT __stdcall GetRunHyperlinkInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetRunHyperlinkLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_RunAnnotation(CAT_VARIANT_BOOL & oRunAnnotation); \
virtual HRESULT __stdcall put_RunAnnotation(CAT_VARIANT_BOOL iRunAnnotation); \
virtual HRESULT __stdcall GetRunAnnotationInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetRunAnnotationLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_RunPause(CAT_VARIANT_BOOL & oRunPause); \
virtual HRESULT __stdcall put_RunPause(CAT_VARIANT_BOOL iRunPause); \
virtual HRESULT __stdcall GetRunPauseInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetRunPauseLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_RunStepSize(float & oRunStepSize); \
virtual HRESULT __stdcall put_RunStepSize(float iRunStepSize); \
virtual HRESULT __stdcall GetRunStepSizeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetRunStepSizeLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_PSDynClashMode(DNBVisualizationMode & oPSDynClashMode); \
virtual HRESULT __stdcall put_PSDynClashMode(DNBVisualizationMode iPSDynClashMode); \
virtual HRESULT __stdcall GetPSDynClashModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetPSDynClashModeLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_PSCycleTime(CAT_VARIANT_BOOL & oPSCycleTime); \
virtual HRESULT __stdcall put_PSCycleTime(CAT_VARIANT_BOOL iPSCycleTime); \
virtual HRESULT __stdcall GetPSCycleTimeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetPSCycleTimeLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_PVStateMgt(CAT_VARIANT_BOOL & oPVStateMgt); \
virtual HRESULT __stdcall put_PVStateMgt(CAT_VARIANT_BOOL iPVStateMgt); \
virtual HRESULT __stdcall GetPVStateMgtInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetPVStateMgtLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_PVEndCondition(CAT_VARIANT_BOOL & oPVEndCondition); \
virtual HRESULT __stdcall put_PVEndCondition(CAT_VARIANT_BOOL iPVEndCondition); \
virtual HRESULT __stdcall GetPVEndConditionInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetPVEndConditionLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_ASNavigationMode(DNBSimNavigationMode & oASNavigationMode); \
virtual HRESULT __stdcall put_ASNavigationMode(DNBSimNavigationMode iASNavigationMode); \
virtual HRESULT __stdcall GetASNavigationModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetASNavigationModeLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_ASStepSize(float & oASStepSize); \
virtual HRESULT __stdcall put_ASStepSize(float iASStepSize); \
virtual HRESULT __stdcall GetASStepSizeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetASStepSizeLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_RunTextBehavior(DNBActBehaviorType & oRunTextBehavior); \
virtual HRESULT __stdcall put_RunTextBehavior(DNBActBehaviorType iRunTextBehavior); \
virtual HRESULT __stdcall GetRunTextBehaviorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetRunTextBehaviorLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_RunHlnkBehavior(DNBHlnkBehaviorType & oRunHlnkBehavior); \
virtual HRESULT __stdcall put_RunHlnkBehavior(DNBHlnkBehaviorType iRunHlnkBehavior); \
virtual HRESULT __stdcall GetRunHlnkBehaviorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetRunHlnkBehaviorLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_RunAnnotBehavior(DNBActBehaviorType & oRunAnnotBehavior); \
virtual HRESULT __stdcall put_RunAnnotBehavior(DNBActBehaviorType iRunAnnotBehavior); \
virtual HRESULT __stdcall GetRunAnnotBehaviorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetRunAnnotBehaviorLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_RunVisBehavior(DNBActBehaviorType & oRunVisBehavior); \
virtual HRESULT __stdcall put_RunVisBehavior(DNBActBehaviorType iRunVisBehavior); \
virtual HRESULT __stdcall GetRunVisBehaviorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetRunVisBehaviorLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_PSGraphicUpdate(DNBSimGraphUpdateMode & oPSGraphicUpdate); \
virtual HRESULT __stdcall put_PSGraphicUpdate(DNBSimGraphUpdateMode iPSGraphicUpdate); \
virtual HRESULT __stdcall GetPSGraphicUpdateInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetPSGraphicUpdateLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_PSGraphicSimStep(CATLONG & oPSGraphicSimStep); \
virtual HRESULT __stdcall put_PSGraphicSimStep(CATLONG iPSGraphicSimStep); \
virtual HRESULT __stdcall GetPSGraphicSimStepInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetPSGraphicSimStepLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall Commit(); \
virtual HRESULT __stdcall Rollback(); \
virtual HRESULT __stdcall ResetToAdminValues(); \
virtual HRESULT __stdcall ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList); \
virtual HRESULT __stdcall SaveRepository(); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIASimulationSettingAtt(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::get_AthStateMgt(CAT_VARIANT_BOOL & oAthStateMgt) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_AthStateMgt(oAthStateMgt)); \
} \
HRESULT __stdcall  ENVTIEName::put_AthStateMgt(CAT_VARIANT_BOOL iAthStateMgt) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_AthStateMgt(iAthStateMgt)); \
} \
HRESULT __stdcall  ENVTIEName::GetAthStateMgtInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetAthStateMgtInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetAthStateMgtLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetAthStateMgtLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_AthEndCondition(CAT_VARIANT_BOOL & oAthEndCondition) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_AthEndCondition(oAthEndCondition)); \
} \
HRESULT __stdcall  ENVTIEName::put_AthEndCondition(CAT_VARIANT_BOOL iAthEndCondition) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_AthEndCondition(iAthEndCondition)); \
} \
HRESULT __stdcall  ENVTIEName::GetAthEndConditionInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetAthEndConditionInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetAthEndConditionLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetAthEndConditionLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_AthTextMsg(CAT_VARIANT_BOOL & oAthTextMsg) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_AthTextMsg(oAthTextMsg)); \
} \
HRESULT __stdcall  ENVTIEName::put_AthTextMsg(CAT_VARIANT_BOOL iAthTextMsg) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_AthTextMsg(iAthTextMsg)); \
} \
HRESULT __stdcall  ENVTIEName::GetAthTextMsgInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetAthTextMsgInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetAthTextMsgLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetAthTextMsgLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_AthViewpoint(CAT_VARIANT_BOOL & oAthViewpoint) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_AthViewpoint(oAthViewpoint)); \
} \
HRESULT __stdcall  ENVTIEName::put_AthViewpoint(CAT_VARIANT_BOOL iAthViewpoint) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_AthViewpoint(iAthViewpoint)); \
} \
HRESULT __stdcall  ENVTIEName::GetAthViewpointInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetAthViewpointInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetAthViewpointLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetAthViewpointLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_AthHyperlink(CAT_VARIANT_BOOL & oAthHyperlink) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_AthHyperlink(oAthHyperlink)); \
} \
HRESULT __stdcall  ENVTIEName::put_AthHyperlink(CAT_VARIANT_BOOL iAthHyperlink) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_AthHyperlink(iAthHyperlink)); \
} \
HRESULT __stdcall  ENVTIEName::GetAthHyperlinkInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetAthHyperlinkInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetAthHyperlinkLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetAthHyperlinkLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_AthAnnotation(CAT_VARIANT_BOOL & oAthAnnotation) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_AthAnnotation(oAthAnnotation)); \
} \
HRESULT __stdcall  ENVTIEName::put_AthAnnotation(CAT_VARIANT_BOOL iAthAnnotation) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_AthAnnotation(iAthAnnotation)); \
} \
HRESULT __stdcall  ENVTIEName::GetAthAnnotationInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetAthAnnotationInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetAthAnnotationLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetAthAnnotationLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_AthDisableSim(CAT_VARIANT_BOOL & oAthDisableSim) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_AthDisableSim(oAthDisableSim)); \
} \
HRESULT __stdcall  ENVTIEName::put_AthDisableSim(CAT_VARIANT_BOOL iAthDisableSim) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_AthDisableSim(iAthDisableSim)); \
} \
HRESULT __stdcall  ENVTIEName::GetAthDisableSimInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetAthDisableSimInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetAthDisableSimLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetAthDisableSimLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_AthSelAgentDlg(CAT_VARIANT_BOOL & oAthSelAgentDlg) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_AthSelAgentDlg(oAthSelAgentDlg)); \
} \
HRESULT __stdcall  ENVTIEName::put_AthSelAgentDlg(CAT_VARIANT_BOOL iAthSelAgentDlg) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_AthSelAgentDlg(iAthSelAgentDlg)); \
} \
HRESULT __stdcall  ENVTIEName::GetAthSelAgentDlgInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetAthSelAgentDlgInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetAthSelAgentDlgLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetAthSelAgentDlgLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_RunStateMgt(CAT_VARIANT_BOOL & oRunStateMgt) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_RunStateMgt(oRunStateMgt)); \
} \
HRESULT __stdcall  ENVTIEName::put_RunStateMgt(CAT_VARIANT_BOOL iRunStateMgt) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_RunStateMgt(iRunStateMgt)); \
} \
HRESULT __stdcall  ENVTIEName::GetRunStateMgtInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetRunStateMgtInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetRunStateMgtLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetRunStateMgtLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_RunEndCondition(CAT_VARIANT_BOOL & oRunEndCondition) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_RunEndCondition(oRunEndCondition)); \
} \
HRESULT __stdcall  ENVTIEName::put_RunEndCondition(CAT_VARIANT_BOOL iRunEndCondition) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_RunEndCondition(iRunEndCondition)); \
} \
HRESULT __stdcall  ENVTIEName::GetRunEndConditionInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetRunEndConditionInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetRunEndConditionLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetRunEndConditionLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_RunTextMsg(CAT_VARIANT_BOOL & oRunTextMsg) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_RunTextMsg(oRunTextMsg)); \
} \
HRESULT __stdcall  ENVTIEName::put_RunTextMsg(CAT_VARIANT_BOOL iRunTextMsg) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_RunTextMsg(iRunTextMsg)); \
} \
HRESULT __stdcall  ENVTIEName::GetRunTextMsgInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetRunTextMsgInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetRunTextMsgLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetRunTextMsgLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_RunViewpoint(CAT_VARIANT_BOOL & oRunViewpoint) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_RunViewpoint(oRunViewpoint)); \
} \
HRESULT __stdcall  ENVTIEName::put_RunViewpoint(CAT_VARIANT_BOOL iRunViewpoint) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_RunViewpoint(iRunViewpoint)); \
} \
HRESULT __stdcall  ENVTIEName::GetRunViewpointInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetRunViewpointInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetRunViewpointLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetRunViewpointLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_RunHyperlink(CAT_VARIANT_BOOL & oRunHyperlink) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_RunHyperlink(oRunHyperlink)); \
} \
HRESULT __stdcall  ENVTIEName::put_RunHyperlink(CAT_VARIANT_BOOL iRunHyperlink) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_RunHyperlink(iRunHyperlink)); \
} \
HRESULT __stdcall  ENVTIEName::GetRunHyperlinkInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetRunHyperlinkInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetRunHyperlinkLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetRunHyperlinkLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_RunAnnotation(CAT_VARIANT_BOOL & oRunAnnotation) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_RunAnnotation(oRunAnnotation)); \
} \
HRESULT __stdcall  ENVTIEName::put_RunAnnotation(CAT_VARIANT_BOOL iRunAnnotation) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_RunAnnotation(iRunAnnotation)); \
} \
HRESULT __stdcall  ENVTIEName::GetRunAnnotationInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetRunAnnotationInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetRunAnnotationLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetRunAnnotationLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_RunPause(CAT_VARIANT_BOOL & oRunPause) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_RunPause(oRunPause)); \
} \
HRESULT __stdcall  ENVTIEName::put_RunPause(CAT_VARIANT_BOOL iRunPause) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_RunPause(iRunPause)); \
} \
HRESULT __stdcall  ENVTIEName::GetRunPauseInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetRunPauseInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetRunPauseLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetRunPauseLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_RunStepSize(float & oRunStepSize) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_RunStepSize(oRunStepSize)); \
} \
HRESULT __stdcall  ENVTIEName::put_RunStepSize(float iRunStepSize) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_RunStepSize(iRunStepSize)); \
} \
HRESULT __stdcall  ENVTIEName::GetRunStepSizeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetRunStepSizeInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetRunStepSizeLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetRunStepSizeLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_PSDynClashMode(DNBVisualizationMode & oPSDynClashMode) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_PSDynClashMode(oPSDynClashMode)); \
} \
HRESULT __stdcall  ENVTIEName::put_PSDynClashMode(DNBVisualizationMode iPSDynClashMode) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_PSDynClashMode(iPSDynClashMode)); \
} \
HRESULT __stdcall  ENVTIEName::GetPSDynClashModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetPSDynClashModeInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetPSDynClashModeLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetPSDynClashModeLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_PSCycleTime(CAT_VARIANT_BOOL & oPSCycleTime) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_PSCycleTime(oPSCycleTime)); \
} \
HRESULT __stdcall  ENVTIEName::put_PSCycleTime(CAT_VARIANT_BOOL iPSCycleTime) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_PSCycleTime(iPSCycleTime)); \
} \
HRESULT __stdcall  ENVTIEName::GetPSCycleTimeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetPSCycleTimeInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetPSCycleTimeLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetPSCycleTimeLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_PVStateMgt(CAT_VARIANT_BOOL & oPVStateMgt) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_PVStateMgt(oPVStateMgt)); \
} \
HRESULT __stdcall  ENVTIEName::put_PVStateMgt(CAT_VARIANT_BOOL iPVStateMgt) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_PVStateMgt(iPVStateMgt)); \
} \
HRESULT __stdcall  ENVTIEName::GetPVStateMgtInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetPVStateMgtInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetPVStateMgtLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetPVStateMgtLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_PVEndCondition(CAT_VARIANT_BOOL & oPVEndCondition) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_PVEndCondition(oPVEndCondition)); \
} \
HRESULT __stdcall  ENVTIEName::put_PVEndCondition(CAT_VARIANT_BOOL iPVEndCondition) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_PVEndCondition(iPVEndCondition)); \
} \
HRESULT __stdcall  ENVTIEName::GetPVEndConditionInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetPVEndConditionInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetPVEndConditionLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetPVEndConditionLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_ASNavigationMode(DNBSimNavigationMode & oASNavigationMode) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_ASNavigationMode(oASNavigationMode)); \
} \
HRESULT __stdcall  ENVTIEName::put_ASNavigationMode(DNBSimNavigationMode iASNavigationMode) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_ASNavigationMode(iASNavigationMode)); \
} \
HRESULT __stdcall  ENVTIEName::GetASNavigationModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetASNavigationModeInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetASNavigationModeLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetASNavigationModeLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_ASStepSize(float & oASStepSize) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_ASStepSize(oASStepSize)); \
} \
HRESULT __stdcall  ENVTIEName::put_ASStepSize(float iASStepSize) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_ASStepSize(iASStepSize)); \
} \
HRESULT __stdcall  ENVTIEName::GetASStepSizeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetASStepSizeInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetASStepSizeLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetASStepSizeLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_RunTextBehavior(DNBActBehaviorType & oRunTextBehavior) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_RunTextBehavior(oRunTextBehavior)); \
} \
HRESULT __stdcall  ENVTIEName::put_RunTextBehavior(DNBActBehaviorType iRunTextBehavior) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_RunTextBehavior(iRunTextBehavior)); \
} \
HRESULT __stdcall  ENVTIEName::GetRunTextBehaviorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetRunTextBehaviorInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetRunTextBehaviorLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetRunTextBehaviorLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_RunHlnkBehavior(DNBHlnkBehaviorType & oRunHlnkBehavior) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_RunHlnkBehavior(oRunHlnkBehavior)); \
} \
HRESULT __stdcall  ENVTIEName::put_RunHlnkBehavior(DNBHlnkBehaviorType iRunHlnkBehavior) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_RunHlnkBehavior(iRunHlnkBehavior)); \
} \
HRESULT __stdcall  ENVTIEName::GetRunHlnkBehaviorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetRunHlnkBehaviorInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetRunHlnkBehaviorLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetRunHlnkBehaviorLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_RunAnnotBehavior(DNBActBehaviorType & oRunAnnotBehavior) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_RunAnnotBehavior(oRunAnnotBehavior)); \
} \
HRESULT __stdcall  ENVTIEName::put_RunAnnotBehavior(DNBActBehaviorType iRunAnnotBehavior) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_RunAnnotBehavior(iRunAnnotBehavior)); \
} \
HRESULT __stdcall  ENVTIEName::GetRunAnnotBehaviorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetRunAnnotBehaviorInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetRunAnnotBehaviorLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetRunAnnotBehaviorLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_RunVisBehavior(DNBActBehaviorType & oRunVisBehavior) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_RunVisBehavior(oRunVisBehavior)); \
} \
HRESULT __stdcall  ENVTIEName::put_RunVisBehavior(DNBActBehaviorType iRunVisBehavior) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_RunVisBehavior(iRunVisBehavior)); \
} \
HRESULT __stdcall  ENVTIEName::GetRunVisBehaviorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetRunVisBehaviorInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetRunVisBehaviorLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetRunVisBehaviorLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_PSGraphicUpdate(DNBSimGraphUpdateMode & oPSGraphicUpdate) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_PSGraphicUpdate(oPSGraphicUpdate)); \
} \
HRESULT __stdcall  ENVTIEName::put_PSGraphicUpdate(DNBSimGraphUpdateMode iPSGraphicUpdate) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_PSGraphicUpdate(iPSGraphicUpdate)); \
} \
HRESULT __stdcall  ENVTIEName::GetPSGraphicUpdateInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetPSGraphicUpdateInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetPSGraphicUpdateLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetPSGraphicUpdateLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_PSGraphicSimStep(CATLONG & oPSGraphicSimStep) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_PSGraphicSimStep(oPSGraphicSimStep)); \
} \
HRESULT __stdcall  ENVTIEName::put_PSGraphicSimStep(CATLONG iPSGraphicSimStep) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_PSGraphicSimStep(iPSGraphicSimStep)); \
} \
HRESULT __stdcall  ENVTIEName::GetPSGraphicSimStepInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetPSGraphicSimStepInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetPSGraphicSimStepLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetPSGraphicSimStepLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::Commit() \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)Commit()); \
} \
HRESULT __stdcall  ENVTIEName::Rollback() \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)Rollback()); \
} \
HRESULT __stdcall  ENVTIEName::ResetToAdminValues() \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)ResetToAdminValues()); \
} \
HRESULT __stdcall  ENVTIEName::ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)ResetToAdminValuesByName(iAttList)); \
} \
HRESULT __stdcall  ENVTIEName::SaveRepository() \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)SaveRepository()); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIASimulationSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIASimulationSettingAtt(classe)    TIEDNBIASimulationSettingAtt##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIASimulationSettingAtt(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIASimulationSettingAtt, classe) \
 \
 \
CATImplementTIEMethods(DNBIASimulationSettingAtt, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIASimulationSettingAtt, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIASimulationSettingAtt, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIASimulationSettingAtt, classe) \
 \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::get_AthStateMgt(CAT_VARIANT_BOOL & oAthStateMgt) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&oAthStateMgt); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_AthStateMgt(oAthStateMgt); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&oAthStateMgt); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::put_AthStateMgt(CAT_VARIANT_BOOL iAthStateMgt) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&iAthStateMgt); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_AthStateMgt(iAthStateMgt); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&iAthStateMgt); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::GetAthStateMgtInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAthStateMgtInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::SetAthStateMgtLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetAthStateMgtLock(iLocked); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::get_AthEndCondition(CAT_VARIANT_BOOL & oAthEndCondition) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&oAthEndCondition); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_AthEndCondition(oAthEndCondition); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&oAthEndCondition); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::put_AthEndCondition(CAT_VARIANT_BOOL iAthEndCondition) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&iAthEndCondition); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_AthEndCondition(iAthEndCondition); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&iAthEndCondition); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::GetAthEndConditionInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAthEndConditionInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::SetAthEndConditionLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetAthEndConditionLock(iLocked); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::get_AthTextMsg(CAT_VARIANT_BOOL & oAthTextMsg) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&oAthTextMsg); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_AthTextMsg(oAthTextMsg); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&oAthTextMsg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::put_AthTextMsg(CAT_VARIANT_BOOL iAthTextMsg) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&iAthTextMsg); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_AthTextMsg(iAthTextMsg); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&iAthTextMsg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::GetAthTextMsgInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,11,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAthTextMsgInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,11,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::SetAthTextMsgLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,12,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetAthTextMsgLock(iLocked); \
   ExitAfterCall(this,12,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::get_AthViewpoint(CAT_VARIANT_BOOL & oAthViewpoint) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,13,&_Trac2,&oAthViewpoint); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_AthViewpoint(oAthViewpoint); \
   ExitAfterCall(this,13,_Trac2,&_ret_arg,&oAthViewpoint); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::put_AthViewpoint(CAT_VARIANT_BOOL iAthViewpoint) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,14,&_Trac2,&iAthViewpoint); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_AthViewpoint(iAthViewpoint); \
   ExitAfterCall(this,14,_Trac2,&_ret_arg,&iAthViewpoint); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::GetAthViewpointInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,15,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAthViewpointInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,15,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::SetAthViewpointLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,16,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetAthViewpointLock(iLocked); \
   ExitAfterCall(this,16,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::get_AthHyperlink(CAT_VARIANT_BOOL & oAthHyperlink) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,17,&_Trac2,&oAthHyperlink); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_AthHyperlink(oAthHyperlink); \
   ExitAfterCall(this,17,_Trac2,&_ret_arg,&oAthHyperlink); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::put_AthHyperlink(CAT_VARIANT_BOOL iAthHyperlink) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,18,&_Trac2,&iAthHyperlink); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_AthHyperlink(iAthHyperlink); \
   ExitAfterCall(this,18,_Trac2,&_ret_arg,&iAthHyperlink); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::GetAthHyperlinkInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,19,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAthHyperlinkInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,19,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::SetAthHyperlinkLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,20,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetAthHyperlinkLock(iLocked); \
   ExitAfterCall(this,20,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::get_AthAnnotation(CAT_VARIANT_BOOL & oAthAnnotation) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,21,&_Trac2,&oAthAnnotation); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_AthAnnotation(oAthAnnotation); \
   ExitAfterCall(this,21,_Trac2,&_ret_arg,&oAthAnnotation); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::put_AthAnnotation(CAT_VARIANT_BOOL iAthAnnotation) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,22,&_Trac2,&iAthAnnotation); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_AthAnnotation(iAthAnnotation); \
   ExitAfterCall(this,22,_Trac2,&_ret_arg,&iAthAnnotation); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::GetAthAnnotationInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,23,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAthAnnotationInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,23,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::SetAthAnnotationLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,24,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetAthAnnotationLock(iLocked); \
   ExitAfterCall(this,24,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::get_AthDisableSim(CAT_VARIANT_BOOL & oAthDisableSim) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,25,&_Trac2,&oAthDisableSim); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_AthDisableSim(oAthDisableSim); \
   ExitAfterCall(this,25,_Trac2,&_ret_arg,&oAthDisableSim); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::put_AthDisableSim(CAT_VARIANT_BOOL iAthDisableSim) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,26,&_Trac2,&iAthDisableSim); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_AthDisableSim(iAthDisableSim); \
   ExitAfterCall(this,26,_Trac2,&_ret_arg,&iAthDisableSim); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::GetAthDisableSimInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,27,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAthDisableSimInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,27,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::SetAthDisableSimLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,28,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetAthDisableSimLock(iLocked); \
   ExitAfterCall(this,28,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::get_AthSelAgentDlg(CAT_VARIANT_BOOL & oAthSelAgentDlg) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,29,&_Trac2,&oAthSelAgentDlg); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_AthSelAgentDlg(oAthSelAgentDlg); \
   ExitAfterCall(this,29,_Trac2,&_ret_arg,&oAthSelAgentDlg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::put_AthSelAgentDlg(CAT_VARIANT_BOOL iAthSelAgentDlg) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,30,&_Trac2,&iAthSelAgentDlg); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_AthSelAgentDlg(iAthSelAgentDlg); \
   ExitAfterCall(this,30,_Trac2,&_ret_arg,&iAthSelAgentDlg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::GetAthSelAgentDlgInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,31,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAthSelAgentDlgInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,31,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::SetAthSelAgentDlgLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,32,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetAthSelAgentDlgLock(iLocked); \
   ExitAfterCall(this,32,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::get_RunStateMgt(CAT_VARIANT_BOOL & oRunStateMgt) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,33,&_Trac2,&oRunStateMgt); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_RunStateMgt(oRunStateMgt); \
   ExitAfterCall(this,33,_Trac2,&_ret_arg,&oRunStateMgt); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::put_RunStateMgt(CAT_VARIANT_BOOL iRunStateMgt) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,34,&_Trac2,&iRunStateMgt); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_RunStateMgt(iRunStateMgt); \
   ExitAfterCall(this,34,_Trac2,&_ret_arg,&iRunStateMgt); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::GetRunStateMgtInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,35,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetRunStateMgtInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,35,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::SetRunStateMgtLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,36,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetRunStateMgtLock(iLocked); \
   ExitAfterCall(this,36,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::get_RunEndCondition(CAT_VARIANT_BOOL & oRunEndCondition) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,37,&_Trac2,&oRunEndCondition); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_RunEndCondition(oRunEndCondition); \
   ExitAfterCall(this,37,_Trac2,&_ret_arg,&oRunEndCondition); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::put_RunEndCondition(CAT_VARIANT_BOOL iRunEndCondition) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,38,&_Trac2,&iRunEndCondition); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_RunEndCondition(iRunEndCondition); \
   ExitAfterCall(this,38,_Trac2,&_ret_arg,&iRunEndCondition); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::GetRunEndConditionInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,39,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetRunEndConditionInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,39,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::SetRunEndConditionLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,40,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetRunEndConditionLock(iLocked); \
   ExitAfterCall(this,40,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::get_RunTextMsg(CAT_VARIANT_BOOL & oRunTextMsg) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,41,&_Trac2,&oRunTextMsg); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_RunTextMsg(oRunTextMsg); \
   ExitAfterCall(this,41,_Trac2,&_ret_arg,&oRunTextMsg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::put_RunTextMsg(CAT_VARIANT_BOOL iRunTextMsg) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,42,&_Trac2,&iRunTextMsg); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_RunTextMsg(iRunTextMsg); \
   ExitAfterCall(this,42,_Trac2,&_ret_arg,&iRunTextMsg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::GetRunTextMsgInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,43,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetRunTextMsgInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,43,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::SetRunTextMsgLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,44,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetRunTextMsgLock(iLocked); \
   ExitAfterCall(this,44,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::get_RunViewpoint(CAT_VARIANT_BOOL & oRunViewpoint) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,45,&_Trac2,&oRunViewpoint); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_RunViewpoint(oRunViewpoint); \
   ExitAfterCall(this,45,_Trac2,&_ret_arg,&oRunViewpoint); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::put_RunViewpoint(CAT_VARIANT_BOOL iRunViewpoint) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,46,&_Trac2,&iRunViewpoint); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_RunViewpoint(iRunViewpoint); \
   ExitAfterCall(this,46,_Trac2,&_ret_arg,&iRunViewpoint); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::GetRunViewpointInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,47,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetRunViewpointInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,47,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::SetRunViewpointLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,48,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetRunViewpointLock(iLocked); \
   ExitAfterCall(this,48,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::get_RunHyperlink(CAT_VARIANT_BOOL & oRunHyperlink) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,49,&_Trac2,&oRunHyperlink); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_RunHyperlink(oRunHyperlink); \
   ExitAfterCall(this,49,_Trac2,&_ret_arg,&oRunHyperlink); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::put_RunHyperlink(CAT_VARIANT_BOOL iRunHyperlink) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,50,&_Trac2,&iRunHyperlink); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_RunHyperlink(iRunHyperlink); \
   ExitAfterCall(this,50,_Trac2,&_ret_arg,&iRunHyperlink); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::GetRunHyperlinkInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,51,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetRunHyperlinkInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,51,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::SetRunHyperlinkLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,52,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetRunHyperlinkLock(iLocked); \
   ExitAfterCall(this,52,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::get_RunAnnotation(CAT_VARIANT_BOOL & oRunAnnotation) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,53,&_Trac2,&oRunAnnotation); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_RunAnnotation(oRunAnnotation); \
   ExitAfterCall(this,53,_Trac2,&_ret_arg,&oRunAnnotation); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::put_RunAnnotation(CAT_VARIANT_BOOL iRunAnnotation) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,54,&_Trac2,&iRunAnnotation); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_RunAnnotation(iRunAnnotation); \
   ExitAfterCall(this,54,_Trac2,&_ret_arg,&iRunAnnotation); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::GetRunAnnotationInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,55,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetRunAnnotationInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,55,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::SetRunAnnotationLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,56,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetRunAnnotationLock(iLocked); \
   ExitAfterCall(this,56,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::get_RunPause(CAT_VARIANT_BOOL & oRunPause) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,57,&_Trac2,&oRunPause); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_RunPause(oRunPause); \
   ExitAfterCall(this,57,_Trac2,&_ret_arg,&oRunPause); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::put_RunPause(CAT_VARIANT_BOOL iRunPause) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,58,&_Trac2,&iRunPause); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_RunPause(iRunPause); \
   ExitAfterCall(this,58,_Trac2,&_ret_arg,&iRunPause); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::GetRunPauseInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,59,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetRunPauseInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,59,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::SetRunPauseLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,60,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetRunPauseLock(iLocked); \
   ExitAfterCall(this,60,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::get_RunStepSize(float & oRunStepSize) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,61,&_Trac2,&oRunStepSize); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_RunStepSize(oRunStepSize); \
   ExitAfterCall(this,61,_Trac2,&_ret_arg,&oRunStepSize); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::put_RunStepSize(float iRunStepSize) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,62,&_Trac2,&iRunStepSize); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_RunStepSize(iRunStepSize); \
   ExitAfterCall(this,62,_Trac2,&_ret_arg,&iRunStepSize); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::GetRunStepSizeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,63,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetRunStepSizeInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,63,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::SetRunStepSizeLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,64,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetRunStepSizeLock(iLocked); \
   ExitAfterCall(this,64,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::get_PSDynClashMode(DNBVisualizationMode & oPSDynClashMode) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,65,&_Trac2,&oPSDynClashMode); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_PSDynClashMode(oPSDynClashMode); \
   ExitAfterCall(this,65,_Trac2,&_ret_arg,&oPSDynClashMode); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::put_PSDynClashMode(DNBVisualizationMode iPSDynClashMode) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,66,&_Trac2,&iPSDynClashMode); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_PSDynClashMode(iPSDynClashMode); \
   ExitAfterCall(this,66,_Trac2,&_ret_arg,&iPSDynClashMode); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::GetPSDynClashModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,67,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetPSDynClashModeInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,67,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::SetPSDynClashModeLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,68,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetPSDynClashModeLock(iLocked); \
   ExitAfterCall(this,68,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::get_PSCycleTime(CAT_VARIANT_BOOL & oPSCycleTime) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,69,&_Trac2,&oPSCycleTime); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_PSCycleTime(oPSCycleTime); \
   ExitAfterCall(this,69,_Trac2,&_ret_arg,&oPSCycleTime); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::put_PSCycleTime(CAT_VARIANT_BOOL iPSCycleTime) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,70,&_Trac2,&iPSCycleTime); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_PSCycleTime(iPSCycleTime); \
   ExitAfterCall(this,70,_Trac2,&_ret_arg,&iPSCycleTime); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::GetPSCycleTimeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,71,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetPSCycleTimeInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,71,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::SetPSCycleTimeLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,72,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetPSCycleTimeLock(iLocked); \
   ExitAfterCall(this,72,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::get_PVStateMgt(CAT_VARIANT_BOOL & oPVStateMgt) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,73,&_Trac2,&oPVStateMgt); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_PVStateMgt(oPVStateMgt); \
   ExitAfterCall(this,73,_Trac2,&_ret_arg,&oPVStateMgt); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::put_PVStateMgt(CAT_VARIANT_BOOL iPVStateMgt) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,74,&_Trac2,&iPVStateMgt); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_PVStateMgt(iPVStateMgt); \
   ExitAfterCall(this,74,_Trac2,&_ret_arg,&iPVStateMgt); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::GetPVStateMgtInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,75,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetPVStateMgtInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,75,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::SetPVStateMgtLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,76,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetPVStateMgtLock(iLocked); \
   ExitAfterCall(this,76,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::get_PVEndCondition(CAT_VARIANT_BOOL & oPVEndCondition) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,77,&_Trac2,&oPVEndCondition); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_PVEndCondition(oPVEndCondition); \
   ExitAfterCall(this,77,_Trac2,&_ret_arg,&oPVEndCondition); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::put_PVEndCondition(CAT_VARIANT_BOOL iPVEndCondition) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,78,&_Trac2,&iPVEndCondition); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_PVEndCondition(iPVEndCondition); \
   ExitAfterCall(this,78,_Trac2,&_ret_arg,&iPVEndCondition); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::GetPVEndConditionInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,79,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetPVEndConditionInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,79,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::SetPVEndConditionLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,80,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetPVEndConditionLock(iLocked); \
   ExitAfterCall(this,80,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::get_ASNavigationMode(DNBSimNavigationMode & oASNavigationMode) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,81,&_Trac2,&oASNavigationMode); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_ASNavigationMode(oASNavigationMode); \
   ExitAfterCall(this,81,_Trac2,&_ret_arg,&oASNavigationMode); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::put_ASNavigationMode(DNBSimNavigationMode iASNavigationMode) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,82,&_Trac2,&iASNavigationMode); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_ASNavigationMode(iASNavigationMode); \
   ExitAfterCall(this,82,_Trac2,&_ret_arg,&iASNavigationMode); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::GetASNavigationModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,83,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetASNavigationModeInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,83,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::SetASNavigationModeLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,84,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetASNavigationModeLock(iLocked); \
   ExitAfterCall(this,84,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::get_ASStepSize(float & oASStepSize) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,85,&_Trac2,&oASStepSize); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_ASStepSize(oASStepSize); \
   ExitAfterCall(this,85,_Trac2,&_ret_arg,&oASStepSize); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::put_ASStepSize(float iASStepSize) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,86,&_Trac2,&iASStepSize); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_ASStepSize(iASStepSize); \
   ExitAfterCall(this,86,_Trac2,&_ret_arg,&iASStepSize); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::GetASStepSizeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,87,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetASStepSizeInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,87,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::SetASStepSizeLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,88,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetASStepSizeLock(iLocked); \
   ExitAfterCall(this,88,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::get_RunTextBehavior(DNBActBehaviorType & oRunTextBehavior) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,89,&_Trac2,&oRunTextBehavior); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_RunTextBehavior(oRunTextBehavior); \
   ExitAfterCall(this,89,_Trac2,&_ret_arg,&oRunTextBehavior); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::put_RunTextBehavior(DNBActBehaviorType iRunTextBehavior) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,90,&_Trac2,&iRunTextBehavior); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_RunTextBehavior(iRunTextBehavior); \
   ExitAfterCall(this,90,_Trac2,&_ret_arg,&iRunTextBehavior); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::GetRunTextBehaviorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,91,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetRunTextBehaviorInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,91,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::SetRunTextBehaviorLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,92,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetRunTextBehaviorLock(iLocked); \
   ExitAfterCall(this,92,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::get_RunHlnkBehavior(DNBHlnkBehaviorType & oRunHlnkBehavior) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,93,&_Trac2,&oRunHlnkBehavior); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_RunHlnkBehavior(oRunHlnkBehavior); \
   ExitAfterCall(this,93,_Trac2,&_ret_arg,&oRunHlnkBehavior); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::put_RunHlnkBehavior(DNBHlnkBehaviorType iRunHlnkBehavior) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,94,&_Trac2,&iRunHlnkBehavior); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_RunHlnkBehavior(iRunHlnkBehavior); \
   ExitAfterCall(this,94,_Trac2,&_ret_arg,&iRunHlnkBehavior); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::GetRunHlnkBehaviorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,95,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetRunHlnkBehaviorInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,95,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::SetRunHlnkBehaviorLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,96,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetRunHlnkBehaviorLock(iLocked); \
   ExitAfterCall(this,96,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::get_RunAnnotBehavior(DNBActBehaviorType & oRunAnnotBehavior) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,97,&_Trac2,&oRunAnnotBehavior); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_RunAnnotBehavior(oRunAnnotBehavior); \
   ExitAfterCall(this,97,_Trac2,&_ret_arg,&oRunAnnotBehavior); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::put_RunAnnotBehavior(DNBActBehaviorType iRunAnnotBehavior) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,98,&_Trac2,&iRunAnnotBehavior); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_RunAnnotBehavior(iRunAnnotBehavior); \
   ExitAfterCall(this,98,_Trac2,&_ret_arg,&iRunAnnotBehavior); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::GetRunAnnotBehaviorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,99,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetRunAnnotBehaviorInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,99,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::SetRunAnnotBehaviorLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,100,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetRunAnnotBehaviorLock(iLocked); \
   ExitAfterCall(this,100,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::get_RunVisBehavior(DNBActBehaviorType & oRunVisBehavior) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,101,&_Trac2,&oRunVisBehavior); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_RunVisBehavior(oRunVisBehavior); \
   ExitAfterCall(this,101,_Trac2,&_ret_arg,&oRunVisBehavior); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::put_RunVisBehavior(DNBActBehaviorType iRunVisBehavior) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,102,&_Trac2,&iRunVisBehavior); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_RunVisBehavior(iRunVisBehavior); \
   ExitAfterCall(this,102,_Trac2,&_ret_arg,&iRunVisBehavior); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::GetRunVisBehaviorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,103,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetRunVisBehaviorInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,103,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::SetRunVisBehaviorLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,104,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetRunVisBehaviorLock(iLocked); \
   ExitAfterCall(this,104,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::get_PSGraphicUpdate(DNBSimGraphUpdateMode & oPSGraphicUpdate) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,105,&_Trac2,&oPSGraphicUpdate); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_PSGraphicUpdate(oPSGraphicUpdate); \
   ExitAfterCall(this,105,_Trac2,&_ret_arg,&oPSGraphicUpdate); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::put_PSGraphicUpdate(DNBSimGraphUpdateMode iPSGraphicUpdate) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,106,&_Trac2,&iPSGraphicUpdate); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_PSGraphicUpdate(iPSGraphicUpdate); \
   ExitAfterCall(this,106,_Trac2,&_ret_arg,&iPSGraphicUpdate); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::GetPSGraphicUpdateInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,107,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetPSGraphicUpdateInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,107,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::SetPSGraphicUpdateLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,108,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetPSGraphicUpdateLock(iLocked); \
   ExitAfterCall(this,108,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::get_PSGraphicSimStep(CATLONG & oPSGraphicSimStep) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,109,&_Trac2,&oPSGraphicSimStep); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_PSGraphicSimStep(oPSGraphicSimStep); \
   ExitAfterCall(this,109,_Trac2,&_ret_arg,&oPSGraphicSimStep); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::put_PSGraphicSimStep(CATLONG iPSGraphicSimStep) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,110,&_Trac2,&iPSGraphicSimStep); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_PSGraphicSimStep(iPSGraphicSimStep); \
   ExitAfterCall(this,110,_Trac2,&_ret_arg,&iPSGraphicSimStep); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::GetPSGraphicSimStepInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,111,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetPSGraphicSimStepInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,111,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::SetPSGraphicSimStepLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,112,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetPSGraphicSimStepLock(iLocked); \
   ExitAfterCall(this,112,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::Commit() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,113,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Commit(); \
   ExitAfterCall(this,113,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::Rollback() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,114,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Rollback(); \
   ExitAfterCall(this,114,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::ResetToAdminValues() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,115,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ResetToAdminValues(); \
   ExitAfterCall(this,115,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,116,&_Trac2,&iAttList); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ResetToAdminValuesByName(iAttList); \
   ExitAfterCall(this,116,_Trac2,&_ret_arg,&iAttList); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationSettingAtt##classe::SaveRepository() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,117,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SaveRepository(); \
   ExitAfterCall(this,117,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIASimulationSettingAtt##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,118,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,118,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIASimulationSettingAtt##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,119,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,119,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIASimulationSettingAtt##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,120,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,120,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIASimulationSettingAtt##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,121,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,121,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIASimulationSettingAtt##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,122,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,122,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIASimulationSettingAtt(classe) \
 \
 \
declare_TIE_DNBIASimulationSettingAtt(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIASimulationSettingAtt##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIASimulationSettingAtt,"DNBIASimulationSettingAtt",DNBIASimulationSettingAtt::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIASimulationSettingAtt(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIASimulationSettingAtt, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIASimulationSettingAtt##classe(classe::MetaObject(),DNBIASimulationSettingAtt::MetaObject(),(void *)CreateTIEDNBIASimulationSettingAtt##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIASimulationSettingAtt(classe) \
 \
 \
declare_TIE_DNBIASimulationSettingAtt(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIASimulationSettingAtt##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIASimulationSettingAtt,"DNBIASimulationSettingAtt",DNBIASimulationSettingAtt::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIASimulationSettingAtt(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIASimulationSettingAtt, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIASimulationSettingAtt##classe(classe::MetaObject(),DNBIASimulationSettingAtt::MetaObject(),(void *)CreateTIEDNBIASimulationSettingAtt##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIASimulationSettingAtt(classe) TIE_DNBIASimulationSettingAtt(classe)
#else
#define BOA_DNBIASimulationSettingAtt(classe) CATImplementBOA(DNBIASimulationSettingAtt, classe)
#endif

#endif
