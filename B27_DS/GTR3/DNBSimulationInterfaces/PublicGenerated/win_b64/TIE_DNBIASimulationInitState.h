#ifndef __TIE_DNBIASimulationInitState
#define __TIE_DNBIASimulationInitState

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIASimulationInitState.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIASimulationInitState */
#define declare_TIE_DNBIASimulationInitState(classe) \
 \
 \
class TIEDNBIASimulationInitState##classe : public DNBIASimulationInitState \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIASimulationInitState, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall SaveInitialState(DNBSimInitStateAttr iAttrMask); \
      virtual HRESULT __stdcall RestoreInitialState(DNBSimInitStateAttr iAttrMask); \
      virtual HRESULT __stdcall SaveInitialStateList(const CATSafeArrayVariant & iProductLists, DNBSimInitStateAttr iAttrMask); \
      virtual HRESULT __stdcall RestoreInitialStateList(const CATSafeArrayVariant & iProductLists, DNBSimInitStateAttr iAttrMask); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIASimulationInitState(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall SaveInitialState(DNBSimInitStateAttr iAttrMask); \
virtual HRESULT __stdcall RestoreInitialState(DNBSimInitStateAttr iAttrMask); \
virtual HRESULT __stdcall SaveInitialStateList(const CATSafeArrayVariant & iProductLists, DNBSimInitStateAttr iAttrMask); \
virtual HRESULT __stdcall RestoreInitialStateList(const CATSafeArrayVariant & iProductLists, DNBSimInitStateAttr iAttrMask); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIASimulationInitState(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::SaveInitialState(DNBSimInitStateAttr iAttrMask) \
{ \
return (ENVTIECALL(DNBIASimulationInitState,ENVTIETypeLetter,ENVTIELetter)SaveInitialState(iAttrMask)); \
} \
HRESULT __stdcall  ENVTIEName::RestoreInitialState(DNBSimInitStateAttr iAttrMask) \
{ \
return (ENVTIECALL(DNBIASimulationInitState,ENVTIETypeLetter,ENVTIELetter)RestoreInitialState(iAttrMask)); \
} \
HRESULT __stdcall  ENVTIEName::SaveInitialStateList(const CATSafeArrayVariant & iProductLists, DNBSimInitStateAttr iAttrMask) \
{ \
return (ENVTIECALL(DNBIASimulationInitState,ENVTIETypeLetter,ENVTIELetter)SaveInitialStateList(iProductLists,iAttrMask)); \
} \
HRESULT __stdcall  ENVTIEName::RestoreInitialStateList(const CATSafeArrayVariant & iProductLists, DNBSimInitStateAttr iAttrMask) \
{ \
return (ENVTIECALL(DNBIASimulationInitState,ENVTIETypeLetter,ENVTIELetter)RestoreInitialStateList(iProductLists,iAttrMask)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIASimulationInitState,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIASimulationInitState,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIASimulationInitState,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIASimulationInitState,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIASimulationInitState,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIASimulationInitState(classe)    TIEDNBIASimulationInitState##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIASimulationInitState(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIASimulationInitState, classe) \
 \
 \
CATImplementTIEMethods(DNBIASimulationInitState, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIASimulationInitState, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIASimulationInitState, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIASimulationInitState, classe) \
 \
HRESULT __stdcall  TIEDNBIASimulationInitState##classe::SaveInitialState(DNBSimInitStateAttr iAttrMask) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&iAttrMask); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SaveInitialState(iAttrMask); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&iAttrMask); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationInitState##classe::RestoreInitialState(DNBSimInitStateAttr iAttrMask) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&iAttrMask); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->RestoreInitialState(iAttrMask); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&iAttrMask); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationInitState##classe::SaveInitialStateList(const CATSafeArrayVariant & iProductLists, DNBSimInitStateAttr iAttrMask) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&iProductLists,&iAttrMask); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SaveInitialStateList(iProductLists,iAttrMask); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&iProductLists,&iAttrMask); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASimulationInitState##classe::RestoreInitialStateList(const CATSafeArrayVariant & iProductLists, DNBSimInitStateAttr iAttrMask) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&iProductLists,&iAttrMask); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->RestoreInitialStateList(iProductLists,iAttrMask); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&iProductLists,&iAttrMask); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIASimulationInitState##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIASimulationInitState##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIASimulationInitState##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIASimulationInitState##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIASimulationInitState##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIASimulationInitState(classe) \
 \
 \
declare_TIE_DNBIASimulationInitState(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIASimulationInitState##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIASimulationInitState,"DNBIASimulationInitState",DNBIASimulationInitState::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIASimulationInitState(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIASimulationInitState, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIASimulationInitState##classe(classe::MetaObject(),DNBIASimulationInitState::MetaObject(),(void *)CreateTIEDNBIASimulationInitState##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIASimulationInitState(classe) \
 \
 \
declare_TIE_DNBIASimulationInitState(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIASimulationInitState##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIASimulationInitState,"DNBIASimulationInitState",DNBIASimulationInitState::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIASimulationInitState(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIASimulationInitState, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIASimulationInitState##classe(classe::MetaObject(),DNBIASimulationInitState::MetaObject(),(void *)CreateTIEDNBIASimulationInitState##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIASimulationInitState(classe) TIE_DNBIASimulationInitState(classe)
#else
#define BOA_DNBIASimulationInitState(classe) CATImplementBOA(DNBIASimulationInitState, classe)
#endif

#endif
