#ifndef __TIE_DNBIAAnalysisSettingAtt
#define __TIE_DNBIAAnalysisSettingAtt

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIAAnalysisSettingAtt.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIAAnalysisSettingAtt */
#define declare_TIE_DNBIAAnalysisSettingAtt(classe) \
 \
 \
class TIEDNBIAAnalysisSettingAtt##classe : public DNBIAAnalysisSettingAtt \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIAAnalysisSettingAtt, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall get_BeepMode(CAT_VARIANT_BOOL & oBeepMode); \
      virtual HRESULT __stdcall put_BeepMode(CAT_VARIANT_BOOL iBeepMode); \
      virtual HRESULT __stdcall GetBeepModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetBeepModeLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_AskAnlMode(CAT_VARIANT_BOOL & oAskAnlMode); \
      virtual HRESULT __stdcall put_AskAnlMode(CAT_VARIANT_BOOL iAskAnlMode); \
      virtual HRESULT __stdcall GetAskAnlModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetAskAnlModeLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_EnableAnlMode(CAT_VARIANT_BOOL & oEnableAnlMode); \
      virtual HRESULT __stdcall put_EnableAnlMode(CAT_VARIANT_BOOL iEnableAnlMode); \
      virtual HRESULT __stdcall GetEnableAnlModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetEnableAnlModeLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_AnalysisLevel(DNBAnalysisLevel & oAnalysisLevel); \
      virtual HRESULT __stdcall put_AnalysisLevel(DNBAnalysisLevel iAnalysisLevel); \
      virtual HRESULT __stdcall GetAnalysisLevelInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetAnalysisLevelLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_VisualizationMode(DNBVisualizationMode & oVisualizationMode); \
      virtual HRESULT __stdcall put_VisualizationMode(DNBVisualizationMode iVisualizationMode); \
      virtual HRESULT __stdcall GetVisualizationModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetVisualizationModeLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_VoxelStaticAnl(CAT_VARIANT_BOOL & oVoxelStaticAnl); \
      virtual HRESULT __stdcall put_VoxelStaticAnl(CAT_VARIANT_BOOL iVoxelStaticAnl); \
      virtual HRESULT __stdcall GetVoxelStaticAnlInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetVoxelStaticAnlLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_AnlIntDist(CAT_VARIANT_BOOL & oAnlIntDist); \
      virtual HRESULT __stdcall put_AnlIntDist(CAT_VARIANT_BOOL iAnlIntDist); \
      virtual HRESULT __stdcall GetAnlIntDistInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetAnlIntDistLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_AnlTravelLimit(CAT_VARIANT_BOOL & oAnlTravelLimit); \
      virtual HRESULT __stdcall put_AnlTravelLimit(CAT_VARIANT_BOOL iAnlTravelLimit); \
      virtual HRESULT __stdcall GetAnlTravelLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetAnlTravelLimitLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_AnlVelocityLimit(CAT_VARIANT_BOOL & oAnlVelocityLimit); \
      virtual HRESULT __stdcall put_AnlVelocityLimit(CAT_VARIANT_BOOL iAnlVelocityLimit); \
      virtual HRESULT __stdcall GetAnlVelocityLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetAnlVelocityLimitLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_AnlAccelLimit(CAT_VARIANT_BOOL & oAnlVelocityLimit); \
      virtual HRESULT __stdcall put_AnlAccelLimit(CAT_VARIANT_BOOL iAnlVelocityLimit); \
      virtual HRESULT __stdcall GetAnlAccelLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetAnlAccelLimitLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_AnlCautionZone(CAT_VARIANT_BOOL & oAnlCautionZone); \
      virtual HRESULT __stdcall put_AnlCautionZone(CAT_VARIANT_BOOL iAnlCautionZone); \
      virtual HRESULT __stdcall GetAnlCautionZoneInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetAnlCautionZoneLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_AnlLinSpeedLimit(CAT_VARIANT_BOOL & oAnlLinSpeedLimit); \
      virtual HRESULT __stdcall put_AnlLinSpeedLimit(CAT_VARIANT_BOOL iAnlLinSpeedLimit); \
      virtual HRESULT __stdcall GetAnlLinSpeedLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetAnlLinSpeedLimitLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_AnlRotSpeedLimit(CAT_VARIANT_BOOL & oAnlRotSpeedLimit); \
      virtual HRESULT __stdcall put_AnlRotSpeedLimit(CAT_VARIANT_BOOL iAnlRotSpeedLimit); \
      virtual HRESULT __stdcall GetAnlRotSpeedLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetAnlRotSpeedLimitLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_AnlLinAccelLimit(CAT_VARIANT_BOOL & oAnlLinAccelLimit); \
      virtual HRESULT __stdcall put_AnlLinAccelLimit(CAT_VARIANT_BOOL iAnlLinAccelLimit); \
      virtual HRESULT __stdcall GetAnlLinAccelLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetAnlLinAccelLimitLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_AnlRotAccelLimit(CAT_VARIANT_BOOL & oAnlRotAccelLimit); \
      virtual HRESULT __stdcall put_AnlRotAccelLimit(CAT_VARIANT_BOOL iAnlRotAccelLimit); \
      virtual HRESULT __stdcall GetAnlRotAccelLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetAnlRotAccelLimitLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_AnlIOAnalysis(CAT_VARIANT_BOOL & oAnlIOAnalysis); \
      virtual HRESULT __stdcall put_AnlIOAnalysis(CAT_VARIANT_BOOL iAnlIOAnalysis); \
      virtual HRESULT __stdcall GetAnlIOAnalysisInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetAnlIOAnalysisLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_DisplayAnlStatus(CAT_VARIANT_BOOL & oDisplayAnlStatus); \
      virtual HRESULT __stdcall put_DisplayAnlStatus(CAT_VARIANT_BOOL iDisplayAnlStatus); \
      virtual HRESULT __stdcall GetDisplayAnlStatusInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetDisplayAnlStatusLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_AnlIntf(CAT_VARIANT_BOOL & oAnlIntf); \
      virtual HRESULT __stdcall put_AnlIntf(CAT_VARIANT_BOOL iAnlIntf); \
      virtual HRESULT __stdcall GetAnlIntfInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetAnlIntfLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_AnlMeasure(CAT_VARIANT_BOOL & oAnlMeasure); \
      virtual HRESULT __stdcall put_AnlMeasure(CAT_VARIANT_BOOL iAnlMeasure); \
      virtual HRESULT __stdcall GetAnlMeasureInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetAnlMeasureLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_SyncAnlSpecs(CAT_VARIANT_BOOL & oSyncAnlSpecs); \
      virtual HRESULT __stdcall put_SyncAnlSpecs(CAT_VARIANT_BOOL iSyncAnlSpecs); \
      virtual HRESULT __stdcall GetSyncAnlSpecsInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetSyncAnlSpecsLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_ColorMode(CAT_VARIANT_BOOL & oColorMode); \
      virtual HRESULT __stdcall put_ColorMode(CAT_VARIANT_BOOL iColorMode); \
      virtual HRESULT __stdcall GetColorModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetColorModeLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_WhiteMode(CAT_VARIANT_BOOL & oWhiteMode); \
      virtual HRESULT __stdcall put_WhiteMode(CAT_VARIANT_BOOL iWhiteMode); \
      virtual HRESULT __stdcall GetWhiteModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetWhiteModeLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_ClashColor(CATSafeArrayVariant *& oColor); \
      virtual HRESULT __stdcall put_ClashColor(const CATSafeArrayVariant & iColor); \
      virtual HRESULT __stdcall GetClashColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetClashColorLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_ClearanceColor(CATSafeArrayVariant *& oColor); \
      virtual HRESULT __stdcall put_ClearanceColor(const CATSafeArrayVariant & iColor); \
      virtual HRESULT __stdcall GetClearanceColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetClearanceColorLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall Commit(); \
      virtual HRESULT __stdcall Rollback(); \
      virtual HRESULT __stdcall ResetToAdminValues(); \
      virtual HRESULT __stdcall ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList); \
      virtual HRESULT __stdcall SaveRepository(); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIAAnalysisSettingAtt(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall get_BeepMode(CAT_VARIANT_BOOL & oBeepMode); \
virtual HRESULT __stdcall put_BeepMode(CAT_VARIANT_BOOL iBeepMode); \
virtual HRESULT __stdcall GetBeepModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetBeepModeLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_AskAnlMode(CAT_VARIANT_BOOL & oAskAnlMode); \
virtual HRESULT __stdcall put_AskAnlMode(CAT_VARIANT_BOOL iAskAnlMode); \
virtual HRESULT __stdcall GetAskAnlModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetAskAnlModeLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_EnableAnlMode(CAT_VARIANT_BOOL & oEnableAnlMode); \
virtual HRESULT __stdcall put_EnableAnlMode(CAT_VARIANT_BOOL iEnableAnlMode); \
virtual HRESULT __stdcall GetEnableAnlModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetEnableAnlModeLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_AnalysisLevel(DNBAnalysisLevel & oAnalysisLevel); \
virtual HRESULT __stdcall put_AnalysisLevel(DNBAnalysisLevel iAnalysisLevel); \
virtual HRESULT __stdcall GetAnalysisLevelInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetAnalysisLevelLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_VisualizationMode(DNBVisualizationMode & oVisualizationMode); \
virtual HRESULT __stdcall put_VisualizationMode(DNBVisualizationMode iVisualizationMode); \
virtual HRESULT __stdcall GetVisualizationModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetVisualizationModeLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_VoxelStaticAnl(CAT_VARIANT_BOOL & oVoxelStaticAnl); \
virtual HRESULT __stdcall put_VoxelStaticAnl(CAT_VARIANT_BOOL iVoxelStaticAnl); \
virtual HRESULT __stdcall GetVoxelStaticAnlInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetVoxelStaticAnlLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_AnlIntDist(CAT_VARIANT_BOOL & oAnlIntDist); \
virtual HRESULT __stdcall put_AnlIntDist(CAT_VARIANT_BOOL iAnlIntDist); \
virtual HRESULT __stdcall GetAnlIntDistInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetAnlIntDistLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_AnlTravelLimit(CAT_VARIANT_BOOL & oAnlTravelLimit); \
virtual HRESULT __stdcall put_AnlTravelLimit(CAT_VARIANT_BOOL iAnlTravelLimit); \
virtual HRESULT __stdcall GetAnlTravelLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetAnlTravelLimitLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_AnlVelocityLimit(CAT_VARIANT_BOOL & oAnlVelocityLimit); \
virtual HRESULT __stdcall put_AnlVelocityLimit(CAT_VARIANT_BOOL iAnlVelocityLimit); \
virtual HRESULT __stdcall GetAnlVelocityLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetAnlVelocityLimitLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_AnlAccelLimit(CAT_VARIANT_BOOL & oAnlVelocityLimit); \
virtual HRESULT __stdcall put_AnlAccelLimit(CAT_VARIANT_BOOL iAnlVelocityLimit); \
virtual HRESULT __stdcall GetAnlAccelLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetAnlAccelLimitLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_AnlCautionZone(CAT_VARIANT_BOOL & oAnlCautionZone); \
virtual HRESULT __stdcall put_AnlCautionZone(CAT_VARIANT_BOOL iAnlCautionZone); \
virtual HRESULT __stdcall GetAnlCautionZoneInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetAnlCautionZoneLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_AnlLinSpeedLimit(CAT_VARIANT_BOOL & oAnlLinSpeedLimit); \
virtual HRESULT __stdcall put_AnlLinSpeedLimit(CAT_VARIANT_BOOL iAnlLinSpeedLimit); \
virtual HRESULT __stdcall GetAnlLinSpeedLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetAnlLinSpeedLimitLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_AnlRotSpeedLimit(CAT_VARIANT_BOOL & oAnlRotSpeedLimit); \
virtual HRESULT __stdcall put_AnlRotSpeedLimit(CAT_VARIANT_BOOL iAnlRotSpeedLimit); \
virtual HRESULT __stdcall GetAnlRotSpeedLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetAnlRotSpeedLimitLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_AnlLinAccelLimit(CAT_VARIANT_BOOL & oAnlLinAccelLimit); \
virtual HRESULT __stdcall put_AnlLinAccelLimit(CAT_VARIANT_BOOL iAnlLinAccelLimit); \
virtual HRESULT __stdcall GetAnlLinAccelLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetAnlLinAccelLimitLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_AnlRotAccelLimit(CAT_VARIANT_BOOL & oAnlRotAccelLimit); \
virtual HRESULT __stdcall put_AnlRotAccelLimit(CAT_VARIANT_BOOL iAnlRotAccelLimit); \
virtual HRESULT __stdcall GetAnlRotAccelLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetAnlRotAccelLimitLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_AnlIOAnalysis(CAT_VARIANT_BOOL & oAnlIOAnalysis); \
virtual HRESULT __stdcall put_AnlIOAnalysis(CAT_VARIANT_BOOL iAnlIOAnalysis); \
virtual HRESULT __stdcall GetAnlIOAnalysisInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetAnlIOAnalysisLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_DisplayAnlStatus(CAT_VARIANT_BOOL & oDisplayAnlStatus); \
virtual HRESULT __stdcall put_DisplayAnlStatus(CAT_VARIANT_BOOL iDisplayAnlStatus); \
virtual HRESULT __stdcall GetDisplayAnlStatusInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetDisplayAnlStatusLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_AnlIntf(CAT_VARIANT_BOOL & oAnlIntf); \
virtual HRESULT __stdcall put_AnlIntf(CAT_VARIANT_BOOL iAnlIntf); \
virtual HRESULT __stdcall GetAnlIntfInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetAnlIntfLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_AnlMeasure(CAT_VARIANT_BOOL & oAnlMeasure); \
virtual HRESULT __stdcall put_AnlMeasure(CAT_VARIANT_BOOL iAnlMeasure); \
virtual HRESULT __stdcall GetAnlMeasureInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetAnlMeasureLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_SyncAnlSpecs(CAT_VARIANT_BOOL & oSyncAnlSpecs); \
virtual HRESULT __stdcall put_SyncAnlSpecs(CAT_VARIANT_BOOL iSyncAnlSpecs); \
virtual HRESULT __stdcall GetSyncAnlSpecsInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetSyncAnlSpecsLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_ColorMode(CAT_VARIANT_BOOL & oColorMode); \
virtual HRESULT __stdcall put_ColorMode(CAT_VARIANT_BOOL iColorMode); \
virtual HRESULT __stdcall GetColorModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetColorModeLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_WhiteMode(CAT_VARIANT_BOOL & oWhiteMode); \
virtual HRESULT __stdcall put_WhiteMode(CAT_VARIANT_BOOL iWhiteMode); \
virtual HRESULT __stdcall GetWhiteModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetWhiteModeLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_ClashColor(CATSafeArrayVariant *& oColor); \
virtual HRESULT __stdcall put_ClashColor(const CATSafeArrayVariant & iColor); \
virtual HRESULT __stdcall GetClashColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetClashColorLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_ClearanceColor(CATSafeArrayVariant *& oColor); \
virtual HRESULT __stdcall put_ClearanceColor(const CATSafeArrayVariant & iColor); \
virtual HRESULT __stdcall GetClearanceColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetClearanceColorLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall Commit(); \
virtual HRESULT __stdcall Rollback(); \
virtual HRESULT __stdcall ResetToAdminValues(); \
virtual HRESULT __stdcall ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList); \
virtual HRESULT __stdcall SaveRepository(); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIAAnalysisSettingAtt(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::get_BeepMode(CAT_VARIANT_BOOL & oBeepMode) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_BeepMode(oBeepMode)); \
} \
HRESULT __stdcall  ENVTIEName::put_BeepMode(CAT_VARIANT_BOOL iBeepMode) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_BeepMode(iBeepMode)); \
} \
HRESULT __stdcall  ENVTIEName::GetBeepModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetBeepModeInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetBeepModeLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetBeepModeLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_AskAnlMode(CAT_VARIANT_BOOL & oAskAnlMode) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_AskAnlMode(oAskAnlMode)); \
} \
HRESULT __stdcall  ENVTIEName::put_AskAnlMode(CAT_VARIANT_BOOL iAskAnlMode) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_AskAnlMode(iAskAnlMode)); \
} \
HRESULT __stdcall  ENVTIEName::GetAskAnlModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetAskAnlModeInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetAskAnlModeLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetAskAnlModeLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_EnableAnlMode(CAT_VARIANT_BOOL & oEnableAnlMode) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_EnableAnlMode(oEnableAnlMode)); \
} \
HRESULT __stdcall  ENVTIEName::put_EnableAnlMode(CAT_VARIANT_BOOL iEnableAnlMode) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_EnableAnlMode(iEnableAnlMode)); \
} \
HRESULT __stdcall  ENVTIEName::GetEnableAnlModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetEnableAnlModeInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetEnableAnlModeLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetEnableAnlModeLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_AnalysisLevel(DNBAnalysisLevel & oAnalysisLevel) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_AnalysisLevel(oAnalysisLevel)); \
} \
HRESULT __stdcall  ENVTIEName::put_AnalysisLevel(DNBAnalysisLevel iAnalysisLevel) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_AnalysisLevel(iAnalysisLevel)); \
} \
HRESULT __stdcall  ENVTIEName::GetAnalysisLevelInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetAnalysisLevelInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetAnalysisLevelLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetAnalysisLevelLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_VisualizationMode(DNBVisualizationMode & oVisualizationMode) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_VisualizationMode(oVisualizationMode)); \
} \
HRESULT __stdcall  ENVTIEName::put_VisualizationMode(DNBVisualizationMode iVisualizationMode) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_VisualizationMode(iVisualizationMode)); \
} \
HRESULT __stdcall  ENVTIEName::GetVisualizationModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetVisualizationModeInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetVisualizationModeLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetVisualizationModeLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_VoxelStaticAnl(CAT_VARIANT_BOOL & oVoxelStaticAnl) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_VoxelStaticAnl(oVoxelStaticAnl)); \
} \
HRESULT __stdcall  ENVTIEName::put_VoxelStaticAnl(CAT_VARIANT_BOOL iVoxelStaticAnl) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_VoxelStaticAnl(iVoxelStaticAnl)); \
} \
HRESULT __stdcall  ENVTIEName::GetVoxelStaticAnlInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetVoxelStaticAnlInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetVoxelStaticAnlLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetVoxelStaticAnlLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_AnlIntDist(CAT_VARIANT_BOOL & oAnlIntDist) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_AnlIntDist(oAnlIntDist)); \
} \
HRESULT __stdcall  ENVTIEName::put_AnlIntDist(CAT_VARIANT_BOOL iAnlIntDist) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_AnlIntDist(iAnlIntDist)); \
} \
HRESULT __stdcall  ENVTIEName::GetAnlIntDistInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetAnlIntDistInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetAnlIntDistLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetAnlIntDistLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_AnlTravelLimit(CAT_VARIANT_BOOL & oAnlTravelLimit) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_AnlTravelLimit(oAnlTravelLimit)); \
} \
HRESULT __stdcall  ENVTIEName::put_AnlTravelLimit(CAT_VARIANT_BOOL iAnlTravelLimit) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_AnlTravelLimit(iAnlTravelLimit)); \
} \
HRESULT __stdcall  ENVTIEName::GetAnlTravelLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetAnlTravelLimitInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetAnlTravelLimitLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetAnlTravelLimitLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_AnlVelocityLimit(CAT_VARIANT_BOOL & oAnlVelocityLimit) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_AnlVelocityLimit(oAnlVelocityLimit)); \
} \
HRESULT __stdcall  ENVTIEName::put_AnlVelocityLimit(CAT_VARIANT_BOOL iAnlVelocityLimit) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_AnlVelocityLimit(iAnlVelocityLimit)); \
} \
HRESULT __stdcall  ENVTIEName::GetAnlVelocityLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetAnlVelocityLimitInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetAnlVelocityLimitLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetAnlVelocityLimitLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_AnlAccelLimit(CAT_VARIANT_BOOL & oAnlVelocityLimit) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_AnlAccelLimit(oAnlVelocityLimit)); \
} \
HRESULT __stdcall  ENVTIEName::put_AnlAccelLimit(CAT_VARIANT_BOOL iAnlVelocityLimit) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_AnlAccelLimit(iAnlVelocityLimit)); \
} \
HRESULT __stdcall  ENVTIEName::GetAnlAccelLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetAnlAccelLimitInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetAnlAccelLimitLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetAnlAccelLimitLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_AnlCautionZone(CAT_VARIANT_BOOL & oAnlCautionZone) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_AnlCautionZone(oAnlCautionZone)); \
} \
HRESULT __stdcall  ENVTIEName::put_AnlCautionZone(CAT_VARIANT_BOOL iAnlCautionZone) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_AnlCautionZone(iAnlCautionZone)); \
} \
HRESULT __stdcall  ENVTIEName::GetAnlCautionZoneInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetAnlCautionZoneInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetAnlCautionZoneLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetAnlCautionZoneLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_AnlLinSpeedLimit(CAT_VARIANT_BOOL & oAnlLinSpeedLimit) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_AnlLinSpeedLimit(oAnlLinSpeedLimit)); \
} \
HRESULT __stdcall  ENVTIEName::put_AnlLinSpeedLimit(CAT_VARIANT_BOOL iAnlLinSpeedLimit) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_AnlLinSpeedLimit(iAnlLinSpeedLimit)); \
} \
HRESULT __stdcall  ENVTIEName::GetAnlLinSpeedLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetAnlLinSpeedLimitInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetAnlLinSpeedLimitLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetAnlLinSpeedLimitLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_AnlRotSpeedLimit(CAT_VARIANT_BOOL & oAnlRotSpeedLimit) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_AnlRotSpeedLimit(oAnlRotSpeedLimit)); \
} \
HRESULT __stdcall  ENVTIEName::put_AnlRotSpeedLimit(CAT_VARIANT_BOOL iAnlRotSpeedLimit) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_AnlRotSpeedLimit(iAnlRotSpeedLimit)); \
} \
HRESULT __stdcall  ENVTIEName::GetAnlRotSpeedLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetAnlRotSpeedLimitInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetAnlRotSpeedLimitLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetAnlRotSpeedLimitLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_AnlLinAccelLimit(CAT_VARIANT_BOOL & oAnlLinAccelLimit) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_AnlLinAccelLimit(oAnlLinAccelLimit)); \
} \
HRESULT __stdcall  ENVTIEName::put_AnlLinAccelLimit(CAT_VARIANT_BOOL iAnlLinAccelLimit) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_AnlLinAccelLimit(iAnlLinAccelLimit)); \
} \
HRESULT __stdcall  ENVTIEName::GetAnlLinAccelLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetAnlLinAccelLimitInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetAnlLinAccelLimitLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetAnlLinAccelLimitLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_AnlRotAccelLimit(CAT_VARIANT_BOOL & oAnlRotAccelLimit) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_AnlRotAccelLimit(oAnlRotAccelLimit)); \
} \
HRESULT __stdcall  ENVTIEName::put_AnlRotAccelLimit(CAT_VARIANT_BOOL iAnlRotAccelLimit) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_AnlRotAccelLimit(iAnlRotAccelLimit)); \
} \
HRESULT __stdcall  ENVTIEName::GetAnlRotAccelLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetAnlRotAccelLimitInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetAnlRotAccelLimitLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetAnlRotAccelLimitLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_AnlIOAnalysis(CAT_VARIANT_BOOL & oAnlIOAnalysis) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_AnlIOAnalysis(oAnlIOAnalysis)); \
} \
HRESULT __stdcall  ENVTIEName::put_AnlIOAnalysis(CAT_VARIANT_BOOL iAnlIOAnalysis) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_AnlIOAnalysis(iAnlIOAnalysis)); \
} \
HRESULT __stdcall  ENVTIEName::GetAnlIOAnalysisInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetAnlIOAnalysisInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetAnlIOAnalysisLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetAnlIOAnalysisLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_DisplayAnlStatus(CAT_VARIANT_BOOL & oDisplayAnlStatus) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_DisplayAnlStatus(oDisplayAnlStatus)); \
} \
HRESULT __stdcall  ENVTIEName::put_DisplayAnlStatus(CAT_VARIANT_BOOL iDisplayAnlStatus) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_DisplayAnlStatus(iDisplayAnlStatus)); \
} \
HRESULT __stdcall  ENVTIEName::GetDisplayAnlStatusInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetDisplayAnlStatusInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetDisplayAnlStatusLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetDisplayAnlStatusLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_AnlIntf(CAT_VARIANT_BOOL & oAnlIntf) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_AnlIntf(oAnlIntf)); \
} \
HRESULT __stdcall  ENVTIEName::put_AnlIntf(CAT_VARIANT_BOOL iAnlIntf) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_AnlIntf(iAnlIntf)); \
} \
HRESULT __stdcall  ENVTIEName::GetAnlIntfInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetAnlIntfInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetAnlIntfLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetAnlIntfLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_AnlMeasure(CAT_VARIANT_BOOL & oAnlMeasure) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_AnlMeasure(oAnlMeasure)); \
} \
HRESULT __stdcall  ENVTIEName::put_AnlMeasure(CAT_VARIANT_BOOL iAnlMeasure) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_AnlMeasure(iAnlMeasure)); \
} \
HRESULT __stdcall  ENVTIEName::GetAnlMeasureInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetAnlMeasureInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetAnlMeasureLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetAnlMeasureLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_SyncAnlSpecs(CAT_VARIANT_BOOL & oSyncAnlSpecs) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_SyncAnlSpecs(oSyncAnlSpecs)); \
} \
HRESULT __stdcall  ENVTIEName::put_SyncAnlSpecs(CAT_VARIANT_BOOL iSyncAnlSpecs) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_SyncAnlSpecs(iSyncAnlSpecs)); \
} \
HRESULT __stdcall  ENVTIEName::GetSyncAnlSpecsInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetSyncAnlSpecsInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetSyncAnlSpecsLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetSyncAnlSpecsLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_ColorMode(CAT_VARIANT_BOOL & oColorMode) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_ColorMode(oColorMode)); \
} \
HRESULT __stdcall  ENVTIEName::put_ColorMode(CAT_VARIANT_BOOL iColorMode) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_ColorMode(iColorMode)); \
} \
HRESULT __stdcall  ENVTIEName::GetColorModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetColorModeInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetColorModeLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetColorModeLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_WhiteMode(CAT_VARIANT_BOOL & oWhiteMode) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_WhiteMode(oWhiteMode)); \
} \
HRESULT __stdcall  ENVTIEName::put_WhiteMode(CAT_VARIANT_BOOL iWhiteMode) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_WhiteMode(iWhiteMode)); \
} \
HRESULT __stdcall  ENVTIEName::GetWhiteModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetWhiteModeInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetWhiteModeLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetWhiteModeLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_ClashColor(CATSafeArrayVariant *& oColor) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_ClashColor(oColor)); \
} \
HRESULT __stdcall  ENVTIEName::put_ClashColor(const CATSafeArrayVariant & iColor) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_ClashColor(iColor)); \
} \
HRESULT __stdcall  ENVTIEName::GetClashColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetClashColorInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetClashColorLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetClashColorLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_ClearanceColor(CATSafeArrayVariant *& oColor) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_ClearanceColor(oColor)); \
} \
HRESULT __stdcall  ENVTIEName::put_ClearanceColor(const CATSafeArrayVariant & iColor) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_ClearanceColor(iColor)); \
} \
HRESULT __stdcall  ENVTIEName::GetClearanceColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetClearanceColorInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetClearanceColorLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetClearanceColorLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::Commit() \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)Commit()); \
} \
HRESULT __stdcall  ENVTIEName::Rollback() \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)Rollback()); \
} \
HRESULT __stdcall  ENVTIEName::ResetToAdminValues() \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)ResetToAdminValues()); \
} \
HRESULT __stdcall  ENVTIEName::ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)ResetToAdminValuesByName(iAttList)); \
} \
HRESULT __stdcall  ENVTIEName::SaveRepository() \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)SaveRepository()); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIAAnalysisSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIAAnalysisSettingAtt(classe)    TIEDNBIAAnalysisSettingAtt##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIAAnalysisSettingAtt(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIAAnalysisSettingAtt, classe) \
 \
 \
CATImplementTIEMethods(DNBIAAnalysisSettingAtt, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIAAnalysisSettingAtt, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIAAnalysisSettingAtt, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIAAnalysisSettingAtt, classe) \
 \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::get_BeepMode(CAT_VARIANT_BOOL & oBeepMode) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&oBeepMode); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_BeepMode(oBeepMode); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&oBeepMode); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::put_BeepMode(CAT_VARIANT_BOOL iBeepMode) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&iBeepMode); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_BeepMode(iBeepMode); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&iBeepMode); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::GetBeepModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetBeepModeInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::SetBeepModeLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetBeepModeLock(iLocked); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::get_AskAnlMode(CAT_VARIANT_BOOL & oAskAnlMode) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&oAskAnlMode); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_AskAnlMode(oAskAnlMode); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&oAskAnlMode); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::put_AskAnlMode(CAT_VARIANT_BOOL iAskAnlMode) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&iAskAnlMode); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_AskAnlMode(iAskAnlMode); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&iAskAnlMode); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::GetAskAnlModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAskAnlModeInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::SetAskAnlModeLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetAskAnlModeLock(iLocked); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::get_EnableAnlMode(CAT_VARIANT_BOOL & oEnableAnlMode) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&oEnableAnlMode); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_EnableAnlMode(oEnableAnlMode); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&oEnableAnlMode); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::put_EnableAnlMode(CAT_VARIANT_BOOL iEnableAnlMode) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&iEnableAnlMode); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_EnableAnlMode(iEnableAnlMode); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&iEnableAnlMode); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::GetEnableAnlModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,11,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetEnableAnlModeInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,11,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::SetEnableAnlModeLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,12,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetEnableAnlModeLock(iLocked); \
   ExitAfterCall(this,12,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::get_AnalysisLevel(DNBAnalysisLevel & oAnalysisLevel) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,13,&_Trac2,&oAnalysisLevel); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_AnalysisLevel(oAnalysisLevel); \
   ExitAfterCall(this,13,_Trac2,&_ret_arg,&oAnalysisLevel); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::put_AnalysisLevel(DNBAnalysisLevel iAnalysisLevel) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,14,&_Trac2,&iAnalysisLevel); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_AnalysisLevel(iAnalysisLevel); \
   ExitAfterCall(this,14,_Trac2,&_ret_arg,&iAnalysisLevel); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::GetAnalysisLevelInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,15,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAnalysisLevelInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,15,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::SetAnalysisLevelLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,16,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetAnalysisLevelLock(iLocked); \
   ExitAfterCall(this,16,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::get_VisualizationMode(DNBVisualizationMode & oVisualizationMode) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,17,&_Trac2,&oVisualizationMode); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_VisualizationMode(oVisualizationMode); \
   ExitAfterCall(this,17,_Trac2,&_ret_arg,&oVisualizationMode); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::put_VisualizationMode(DNBVisualizationMode iVisualizationMode) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,18,&_Trac2,&iVisualizationMode); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_VisualizationMode(iVisualizationMode); \
   ExitAfterCall(this,18,_Trac2,&_ret_arg,&iVisualizationMode); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::GetVisualizationModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,19,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetVisualizationModeInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,19,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::SetVisualizationModeLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,20,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetVisualizationModeLock(iLocked); \
   ExitAfterCall(this,20,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::get_VoxelStaticAnl(CAT_VARIANT_BOOL & oVoxelStaticAnl) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,21,&_Trac2,&oVoxelStaticAnl); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_VoxelStaticAnl(oVoxelStaticAnl); \
   ExitAfterCall(this,21,_Trac2,&_ret_arg,&oVoxelStaticAnl); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::put_VoxelStaticAnl(CAT_VARIANT_BOOL iVoxelStaticAnl) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,22,&_Trac2,&iVoxelStaticAnl); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_VoxelStaticAnl(iVoxelStaticAnl); \
   ExitAfterCall(this,22,_Trac2,&_ret_arg,&iVoxelStaticAnl); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::GetVoxelStaticAnlInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,23,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetVoxelStaticAnlInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,23,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::SetVoxelStaticAnlLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,24,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetVoxelStaticAnlLock(iLocked); \
   ExitAfterCall(this,24,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::get_AnlIntDist(CAT_VARIANT_BOOL & oAnlIntDist) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,25,&_Trac2,&oAnlIntDist); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_AnlIntDist(oAnlIntDist); \
   ExitAfterCall(this,25,_Trac2,&_ret_arg,&oAnlIntDist); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::put_AnlIntDist(CAT_VARIANT_BOOL iAnlIntDist) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,26,&_Trac2,&iAnlIntDist); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_AnlIntDist(iAnlIntDist); \
   ExitAfterCall(this,26,_Trac2,&_ret_arg,&iAnlIntDist); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::GetAnlIntDistInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,27,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAnlIntDistInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,27,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::SetAnlIntDistLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,28,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetAnlIntDistLock(iLocked); \
   ExitAfterCall(this,28,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::get_AnlTravelLimit(CAT_VARIANT_BOOL & oAnlTravelLimit) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,29,&_Trac2,&oAnlTravelLimit); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_AnlTravelLimit(oAnlTravelLimit); \
   ExitAfterCall(this,29,_Trac2,&_ret_arg,&oAnlTravelLimit); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::put_AnlTravelLimit(CAT_VARIANT_BOOL iAnlTravelLimit) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,30,&_Trac2,&iAnlTravelLimit); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_AnlTravelLimit(iAnlTravelLimit); \
   ExitAfterCall(this,30,_Trac2,&_ret_arg,&iAnlTravelLimit); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::GetAnlTravelLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,31,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAnlTravelLimitInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,31,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::SetAnlTravelLimitLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,32,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetAnlTravelLimitLock(iLocked); \
   ExitAfterCall(this,32,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::get_AnlVelocityLimit(CAT_VARIANT_BOOL & oAnlVelocityLimit) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,33,&_Trac2,&oAnlVelocityLimit); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_AnlVelocityLimit(oAnlVelocityLimit); \
   ExitAfterCall(this,33,_Trac2,&_ret_arg,&oAnlVelocityLimit); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::put_AnlVelocityLimit(CAT_VARIANT_BOOL iAnlVelocityLimit) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,34,&_Trac2,&iAnlVelocityLimit); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_AnlVelocityLimit(iAnlVelocityLimit); \
   ExitAfterCall(this,34,_Trac2,&_ret_arg,&iAnlVelocityLimit); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::GetAnlVelocityLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,35,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAnlVelocityLimitInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,35,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::SetAnlVelocityLimitLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,36,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetAnlVelocityLimitLock(iLocked); \
   ExitAfterCall(this,36,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::get_AnlAccelLimit(CAT_VARIANT_BOOL & oAnlVelocityLimit) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,37,&_Trac2,&oAnlVelocityLimit); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_AnlAccelLimit(oAnlVelocityLimit); \
   ExitAfterCall(this,37,_Trac2,&_ret_arg,&oAnlVelocityLimit); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::put_AnlAccelLimit(CAT_VARIANT_BOOL iAnlVelocityLimit) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,38,&_Trac2,&iAnlVelocityLimit); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_AnlAccelLimit(iAnlVelocityLimit); \
   ExitAfterCall(this,38,_Trac2,&_ret_arg,&iAnlVelocityLimit); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::GetAnlAccelLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,39,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAnlAccelLimitInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,39,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::SetAnlAccelLimitLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,40,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetAnlAccelLimitLock(iLocked); \
   ExitAfterCall(this,40,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::get_AnlCautionZone(CAT_VARIANT_BOOL & oAnlCautionZone) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,41,&_Trac2,&oAnlCautionZone); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_AnlCautionZone(oAnlCautionZone); \
   ExitAfterCall(this,41,_Trac2,&_ret_arg,&oAnlCautionZone); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::put_AnlCautionZone(CAT_VARIANT_BOOL iAnlCautionZone) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,42,&_Trac2,&iAnlCautionZone); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_AnlCautionZone(iAnlCautionZone); \
   ExitAfterCall(this,42,_Trac2,&_ret_arg,&iAnlCautionZone); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::GetAnlCautionZoneInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,43,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAnlCautionZoneInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,43,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::SetAnlCautionZoneLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,44,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetAnlCautionZoneLock(iLocked); \
   ExitAfterCall(this,44,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::get_AnlLinSpeedLimit(CAT_VARIANT_BOOL & oAnlLinSpeedLimit) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,45,&_Trac2,&oAnlLinSpeedLimit); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_AnlLinSpeedLimit(oAnlLinSpeedLimit); \
   ExitAfterCall(this,45,_Trac2,&_ret_arg,&oAnlLinSpeedLimit); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::put_AnlLinSpeedLimit(CAT_VARIANT_BOOL iAnlLinSpeedLimit) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,46,&_Trac2,&iAnlLinSpeedLimit); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_AnlLinSpeedLimit(iAnlLinSpeedLimit); \
   ExitAfterCall(this,46,_Trac2,&_ret_arg,&iAnlLinSpeedLimit); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::GetAnlLinSpeedLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,47,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAnlLinSpeedLimitInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,47,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::SetAnlLinSpeedLimitLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,48,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetAnlLinSpeedLimitLock(iLocked); \
   ExitAfterCall(this,48,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::get_AnlRotSpeedLimit(CAT_VARIANT_BOOL & oAnlRotSpeedLimit) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,49,&_Trac2,&oAnlRotSpeedLimit); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_AnlRotSpeedLimit(oAnlRotSpeedLimit); \
   ExitAfterCall(this,49,_Trac2,&_ret_arg,&oAnlRotSpeedLimit); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::put_AnlRotSpeedLimit(CAT_VARIANT_BOOL iAnlRotSpeedLimit) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,50,&_Trac2,&iAnlRotSpeedLimit); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_AnlRotSpeedLimit(iAnlRotSpeedLimit); \
   ExitAfterCall(this,50,_Trac2,&_ret_arg,&iAnlRotSpeedLimit); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::GetAnlRotSpeedLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,51,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAnlRotSpeedLimitInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,51,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::SetAnlRotSpeedLimitLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,52,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetAnlRotSpeedLimitLock(iLocked); \
   ExitAfterCall(this,52,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::get_AnlLinAccelLimit(CAT_VARIANT_BOOL & oAnlLinAccelLimit) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,53,&_Trac2,&oAnlLinAccelLimit); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_AnlLinAccelLimit(oAnlLinAccelLimit); \
   ExitAfterCall(this,53,_Trac2,&_ret_arg,&oAnlLinAccelLimit); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::put_AnlLinAccelLimit(CAT_VARIANT_BOOL iAnlLinAccelLimit) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,54,&_Trac2,&iAnlLinAccelLimit); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_AnlLinAccelLimit(iAnlLinAccelLimit); \
   ExitAfterCall(this,54,_Trac2,&_ret_arg,&iAnlLinAccelLimit); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::GetAnlLinAccelLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,55,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAnlLinAccelLimitInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,55,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::SetAnlLinAccelLimitLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,56,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetAnlLinAccelLimitLock(iLocked); \
   ExitAfterCall(this,56,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::get_AnlRotAccelLimit(CAT_VARIANT_BOOL & oAnlRotAccelLimit) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,57,&_Trac2,&oAnlRotAccelLimit); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_AnlRotAccelLimit(oAnlRotAccelLimit); \
   ExitAfterCall(this,57,_Trac2,&_ret_arg,&oAnlRotAccelLimit); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::put_AnlRotAccelLimit(CAT_VARIANT_BOOL iAnlRotAccelLimit) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,58,&_Trac2,&iAnlRotAccelLimit); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_AnlRotAccelLimit(iAnlRotAccelLimit); \
   ExitAfterCall(this,58,_Trac2,&_ret_arg,&iAnlRotAccelLimit); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::GetAnlRotAccelLimitInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,59,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAnlRotAccelLimitInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,59,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::SetAnlRotAccelLimitLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,60,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetAnlRotAccelLimitLock(iLocked); \
   ExitAfterCall(this,60,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::get_AnlIOAnalysis(CAT_VARIANT_BOOL & oAnlIOAnalysis) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,61,&_Trac2,&oAnlIOAnalysis); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_AnlIOAnalysis(oAnlIOAnalysis); \
   ExitAfterCall(this,61,_Trac2,&_ret_arg,&oAnlIOAnalysis); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::put_AnlIOAnalysis(CAT_VARIANT_BOOL iAnlIOAnalysis) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,62,&_Trac2,&iAnlIOAnalysis); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_AnlIOAnalysis(iAnlIOAnalysis); \
   ExitAfterCall(this,62,_Trac2,&_ret_arg,&iAnlIOAnalysis); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::GetAnlIOAnalysisInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,63,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAnlIOAnalysisInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,63,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::SetAnlIOAnalysisLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,64,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetAnlIOAnalysisLock(iLocked); \
   ExitAfterCall(this,64,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::get_DisplayAnlStatus(CAT_VARIANT_BOOL & oDisplayAnlStatus) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,65,&_Trac2,&oDisplayAnlStatus); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_DisplayAnlStatus(oDisplayAnlStatus); \
   ExitAfterCall(this,65,_Trac2,&_ret_arg,&oDisplayAnlStatus); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::put_DisplayAnlStatus(CAT_VARIANT_BOOL iDisplayAnlStatus) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,66,&_Trac2,&iDisplayAnlStatus); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_DisplayAnlStatus(iDisplayAnlStatus); \
   ExitAfterCall(this,66,_Trac2,&_ret_arg,&iDisplayAnlStatus); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::GetDisplayAnlStatusInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,67,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetDisplayAnlStatusInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,67,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::SetDisplayAnlStatusLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,68,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetDisplayAnlStatusLock(iLocked); \
   ExitAfterCall(this,68,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::get_AnlIntf(CAT_VARIANT_BOOL & oAnlIntf) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,69,&_Trac2,&oAnlIntf); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_AnlIntf(oAnlIntf); \
   ExitAfterCall(this,69,_Trac2,&_ret_arg,&oAnlIntf); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::put_AnlIntf(CAT_VARIANT_BOOL iAnlIntf) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,70,&_Trac2,&iAnlIntf); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_AnlIntf(iAnlIntf); \
   ExitAfterCall(this,70,_Trac2,&_ret_arg,&iAnlIntf); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::GetAnlIntfInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,71,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAnlIntfInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,71,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::SetAnlIntfLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,72,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetAnlIntfLock(iLocked); \
   ExitAfterCall(this,72,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::get_AnlMeasure(CAT_VARIANT_BOOL & oAnlMeasure) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,73,&_Trac2,&oAnlMeasure); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_AnlMeasure(oAnlMeasure); \
   ExitAfterCall(this,73,_Trac2,&_ret_arg,&oAnlMeasure); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::put_AnlMeasure(CAT_VARIANT_BOOL iAnlMeasure) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,74,&_Trac2,&iAnlMeasure); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_AnlMeasure(iAnlMeasure); \
   ExitAfterCall(this,74,_Trac2,&_ret_arg,&iAnlMeasure); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::GetAnlMeasureInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,75,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAnlMeasureInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,75,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::SetAnlMeasureLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,76,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetAnlMeasureLock(iLocked); \
   ExitAfterCall(this,76,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::get_SyncAnlSpecs(CAT_VARIANT_BOOL & oSyncAnlSpecs) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,77,&_Trac2,&oSyncAnlSpecs); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_SyncAnlSpecs(oSyncAnlSpecs); \
   ExitAfterCall(this,77,_Trac2,&_ret_arg,&oSyncAnlSpecs); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::put_SyncAnlSpecs(CAT_VARIANT_BOOL iSyncAnlSpecs) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,78,&_Trac2,&iSyncAnlSpecs); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_SyncAnlSpecs(iSyncAnlSpecs); \
   ExitAfterCall(this,78,_Trac2,&_ret_arg,&iSyncAnlSpecs); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::GetSyncAnlSpecsInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,79,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetSyncAnlSpecsInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,79,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::SetSyncAnlSpecsLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,80,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetSyncAnlSpecsLock(iLocked); \
   ExitAfterCall(this,80,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::get_ColorMode(CAT_VARIANT_BOOL & oColorMode) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,81,&_Trac2,&oColorMode); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_ColorMode(oColorMode); \
   ExitAfterCall(this,81,_Trac2,&_ret_arg,&oColorMode); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::put_ColorMode(CAT_VARIANT_BOOL iColorMode) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,82,&_Trac2,&iColorMode); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_ColorMode(iColorMode); \
   ExitAfterCall(this,82,_Trac2,&_ret_arg,&iColorMode); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::GetColorModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,83,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetColorModeInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,83,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::SetColorModeLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,84,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetColorModeLock(iLocked); \
   ExitAfterCall(this,84,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::get_WhiteMode(CAT_VARIANT_BOOL & oWhiteMode) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,85,&_Trac2,&oWhiteMode); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_WhiteMode(oWhiteMode); \
   ExitAfterCall(this,85,_Trac2,&_ret_arg,&oWhiteMode); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::put_WhiteMode(CAT_VARIANT_BOOL iWhiteMode) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,86,&_Trac2,&iWhiteMode); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_WhiteMode(iWhiteMode); \
   ExitAfterCall(this,86,_Trac2,&_ret_arg,&iWhiteMode); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::GetWhiteModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,87,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetWhiteModeInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,87,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::SetWhiteModeLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,88,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetWhiteModeLock(iLocked); \
   ExitAfterCall(this,88,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::get_ClashColor(CATSafeArrayVariant *& oColor) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,89,&_Trac2,&oColor); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_ClashColor(oColor); \
   ExitAfterCall(this,89,_Trac2,&_ret_arg,&oColor); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::put_ClashColor(const CATSafeArrayVariant & iColor) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,90,&_Trac2,&iColor); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_ClashColor(iColor); \
   ExitAfterCall(this,90,_Trac2,&_ret_arg,&iColor); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::GetClashColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,91,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetClashColorInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,91,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::SetClashColorLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,92,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetClashColorLock(iLocked); \
   ExitAfterCall(this,92,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::get_ClearanceColor(CATSafeArrayVariant *& oColor) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,93,&_Trac2,&oColor); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_ClearanceColor(oColor); \
   ExitAfterCall(this,93,_Trac2,&_ret_arg,&oColor); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::put_ClearanceColor(const CATSafeArrayVariant & iColor) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,94,&_Trac2,&iColor); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_ClearanceColor(iColor); \
   ExitAfterCall(this,94,_Trac2,&_ret_arg,&iColor); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::GetClearanceColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,95,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetClearanceColorInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,95,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::SetClearanceColorLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,96,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetClearanceColorLock(iLocked); \
   ExitAfterCall(this,96,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::Commit() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,97,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Commit(); \
   ExitAfterCall(this,97,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::Rollback() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,98,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Rollback(); \
   ExitAfterCall(this,98,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::ResetToAdminValues() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,99,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ResetToAdminValues(); \
   ExitAfterCall(this,99,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,100,&_Trac2,&iAttList); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ResetToAdminValuesByName(iAttList); \
   ExitAfterCall(this,100,_Trac2,&_ret_arg,&iAttList); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnalysisSettingAtt##classe::SaveRepository() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,101,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SaveRepository(); \
   ExitAfterCall(this,101,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAAnalysisSettingAtt##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,102,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,102,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAAnalysisSettingAtt##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,103,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,103,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAAnalysisSettingAtt##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,104,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,104,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAAnalysisSettingAtt##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,105,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,105,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAAnalysisSettingAtt##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,106,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,106,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIAAnalysisSettingAtt(classe) \
 \
 \
declare_TIE_DNBIAAnalysisSettingAtt(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAAnalysisSettingAtt##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAAnalysisSettingAtt,"DNBIAAnalysisSettingAtt",DNBIAAnalysisSettingAtt::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAAnalysisSettingAtt(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIAAnalysisSettingAtt, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAAnalysisSettingAtt##classe(classe::MetaObject(),DNBIAAnalysisSettingAtt::MetaObject(),(void *)CreateTIEDNBIAAnalysisSettingAtt##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIAAnalysisSettingAtt(classe) \
 \
 \
declare_TIE_DNBIAAnalysisSettingAtt(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAAnalysisSettingAtt##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAAnalysisSettingAtt,"DNBIAAnalysisSettingAtt",DNBIAAnalysisSettingAtt::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAAnalysisSettingAtt(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIAAnalysisSettingAtt, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAAnalysisSettingAtt##classe(classe::MetaObject(),DNBIAAnalysisSettingAtt::MetaObject(),(void *)CreateTIEDNBIAAnalysisSettingAtt##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIAAnalysisSettingAtt(classe) TIE_DNBIAAnalysisSettingAtt(classe)
#else
#define BOA_DNBIAAnalysisSettingAtt(classe) CATImplementBOA(DNBIAAnalysisSettingAtt, classe)
#endif

#endif
