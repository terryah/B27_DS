#ifndef DNBHlnkBehaviorType_H
#define DNBHlnkBehaviorType_H

/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */
// COPYRIGHT DASSAULT SYSTEMES 2005
//--------------------------------------------------------------------------
// DNBHlnkBehaviorType Enum
//--------------------------------------------------------------------------

    /**
     * The Hyperlink Behavior setting attribute range of values.
     * @param DNBBehaviorContinue
     *   The behavior for hyperlink activities is: Continue.
     * @param DNBBehaviorPause
     *   The behavior for hyperlink activities is: Pause.
     */
enum DNBHlnkBehaviorType {
  DNBBehaviorContinue,
  DNBBehaviorPause
};
#endif
