/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBISimEnv.h
//      Define the DNBISimEnv interface.
//
//==============================================================================
//
// Usage notes: 
//      This interface provides access to different simulation entities.
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     sra          11/01/2001   Initial implementation.
//     sha          02/25/2002   Added new methods: intervalTimer/currentCommand
//     mmh          03/14/2002   Change name to DNBISimEnv, add new methods
//     sha          05/13/2002   Added new method reset() and a new argument to
//                               existing getCurrentCamWindow() method
//     sha          05/09/2002   Added new methods: set/get ProcessTraverser
//     sha          05/16/2002   Added new methods: set/get IOEnableStatus
//     mmh          05/20/2002   Add set/get/reset CurrentReplay() methods
//     mmh          06/26/2002   Added set/get DestroyProcessTraverser methods
//     mmh          08/22/2002   Add method for functor management
//     sha          10/07/2002   Added new methods addSimActivity() and
//                               getSimActivityCount() to hold the # of sim acts
//     mmh          10/15/2002   Add new methods to manage the simulation 
//                               environment
//     sar          10/31/2002   Add methods to control the build of tasks
//     mmh          11/06/2002   Add methods for simulation settings
//     mmh          11/19/2002   Add method to set the restore proclet
//     sha          03/24/2003   Add methods to implement precedence constraints
//     mmh          04/09/2003   Add support for progress bar
//     mmh          04/11/2003   Add get/reset methods for the Analysis object
//     sha          09/22/2003   Removed getWorld() method, use DNBIWDMExt(Doc)
//     sha          04/21/2004   Provide "jog" support outside a sim command
//     mmh          02/14/2006   Add support for time constraints
//     mmh          05/15/2006   Add support for Resource Simulation
//     mmh          11/07/2006   Add support for PSY reporting to allow them to
//                               register/unregister their own process visitor;
//                               add methods to manage activity creation when 
//                               Process Simulation command is started with a
//                               pre-selected activity
//
//==============================================================================
#ifndef DNBISimEnv_H
#define DNBISimEnv_H

#include <CATBaseUnknown.h>
#include <CATBooleanDef.h>
#include <CATISPPChildManagement.h>
#include <DNBSimulationItfCPP.h>
#include <CATUnicodeString.h>
#include <CATString.h>

/**
 * Simulation Context values.
 */
#define DNB_UNDEF_CTX       0x00000000
#define DNB_PROCESS_CTX     0x00000001
#define DNB_RESOURCE_CTX    0x00000002

class DNBProcessController;
class DNBStepletController;
class DNBTimeletClock;
class DNBProcessTraverser;
class DNBProductTraverser;
class DNBCATListTraverser;
class DNBIntervalTimer;
class CATStateCommand;
class CATFrmWindow;
class CATIASiExWReplay;
class DNBMsgDlg;
class DNBTimeProclet;
class DNBRestoreInitProclet;
class CATIProgressTaskUI;
class DNBAnalysis;
class DNBBasicTHRMessage;
class DNBGroupMessage;
class DNBTraversalVisitor;

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByDNBSimulationItfCPP  IID IID_DNBISimEnv ;
#else
extern "C" const IID IID_DNBISimEnv ;
#endif

//------------------------------------------------------------------

/**
 * This interface provides access to different simulation entities.
 */
class ExportedByDNBSimulationItfCPP DNBISimEnv: public CATBaseUnknown
{
    CATDeclareInterface;
    
public:
    
    /**
     * Sets the current process controller.
     */
    virtual HRESULT setProcessController( DNBProcessController* prcCntrl ) = 0;
    /**
     * Gets the current process controller.
     */
    virtual DNBProcessController* getProcessController( boolean force = FALSE ) = 0;
    /**
     * Resets the current process controller.
     */
    virtual HRESULT resetProcessController() = 0;
    
    /**
     * Gets the current controller, creates it if necessary.
     */
    virtual DNBTimeletClock* getController() = 0;
    /**
     * Resets the current controller.
     */
    virtual HRESULT resetController() = 0;

    /**
     * Resets the current controller.
     */
    virtual HRESULT resetStepController() = 0;
    /**
     * Gets the current controller, creates it if necessary.
     */
    virtual DNBStepletController* getStepController( boolean force = FALSE ) = 0;

    /**
     * Gets the current time proclet, creates it if necessary.
     */
    virtual DNBTimeProclet* getTimeProclet() = 0;
    /**
     * Resets the current time proclet.
     */
    virtual HRESULT resetTimeProclet() = 0;

    /**
     * Sets the current restore initial state proclet.
     */
    virtual HRESULT setRestoreProclet( DNBRestoreInitProclet* rProclet ) = 0;
    /**
     * Gets the current restore initial state proclet, creates it if necessary.
     */
    virtual DNBRestoreInitProclet* getRestoreProclet() = 0;
    /**
     * Resets the current restore initial state proclet.
     */
    virtual HRESULT resetRestoreProclet() = 0;

    /**
     * Resets the handle to the world.
     */
    virtual HRESULT resetWorld() = 0;

    /**
     * Sets the current process.
     */
    virtual HRESULT setCurrentProcess( CATISPPChildManagement_var& crtProc ) = 0;
    /**
     * Gets the current process (if not set, returns the root process).
     */
    virtual CATISPPChildManagement_var getCurrentProcess( boolean force = FALSE ) = 0;
    /**
     * Resets the current process.
     */
    virtual HRESULT resetCurrentProcess() = 0;

    /**
     * Sets the current build process traverser.
     */
    virtual HRESULT setProcessTraverser( DNBProcessTraverser* prcTrav ) = 0;
    /**
     * Gets the current build process traverser.
     */
    virtual DNBProcessTraverser* getProcessTraverser( boolean force = FALSE ) = 0;
    /**
     * Resets the current build process traverser.
     */
    virtual HRESULT resetProcessTraverser() = 0;
    
    /**
     * Sets the current destroy process traverser.
     */
    virtual HRESULT setDestroyProcessTraverser( DNBProcessTraverser* pTrv ) = 0;
    /**
     * Gets the current destroy process traverser.
     */
    virtual DNBProcessTraverser* getDestroyProcessTraverser( boolean force = FALSE ) = 0;
    /**
     * Resets the current destroy process traverser.
     */
    virtual HRESULT resetDestroyProcessTraverser() = 0;
    
    /**
     * Gets the current build product traverser.
     */
    virtual DNBProductTraverser* getProductTraverser() = 0;
    /**
     * Resets the current build product traverser.
     */
    virtual HRESULT resetProductTraverser() = 0;
    
    /**
     * Gets the current applicative data traverser.
     */
    virtual DNBCATListTraverser* getApplicTraverser() = 0;
    /**
     * Resets the current applicative data traverser.
     */
    virtual HRESULT resetApplicTraverser() = 0;
    
    /**
     * Sets the current interval timer.
     */
    virtual HRESULT setIntervalTimer( DNBIntervalTimer* intervalTimer ) = 0;
    /**
     * Gets the current interval timer.
     */
    virtual DNBIntervalTimer* getIntervalTimer( boolean force = FALSE ) = 0;
    /**
     * Resets the current interval timer.
     */
    virtual HRESULT resetIntervalTimer() = 0;
    
    /**
     * Sets the current command.
     */
    virtual HRESULT setCurrentCommand( CATStateCommand* crtCmd ) = 0;
    /**
     * Gets the current command.
     */
    virtual CATStateCommand* getCurrentCommand() = 0;
    /**
     * Resets the current command.
     */
    virtual HRESULT resetCurrentCommand() = 0;
    
    /**
     * Resets the current 3D window that has camera choreography.
     */
    virtual HRESULT resetCurrentCamWindow() = 0;
    /**
     * Gets the current 3D window that has camera choreography.
     */
    virtual CATFrmWindow* getCurrentCamWindow( boolean force = FALSE ) = 0;
    
    /**
     * Sets the message dialog used by the current text activity.
     */
    virtual HRESULT setCurrentMsgDlg( DNBMsgDlg* msgDlg ) = 0;
    /**
     * Gets the message dialog used by the current text activity.
     */
    virtual DNBMsgDlg* getCurrentMsgDlg() = 0;

    /**
     * Sets the IO enable status.
     */
    virtual HRESULT setIOEnableStatus( boolean enableIO ) = 0;
    /**
     * Gets the IO enable status.
     */
    virtual boolean getIOEnableStatus() = 0;
    
    /**
     * Increases the simulation activity counter.
     */
    virtual HRESULT addSimActivity() = 0;
    /**
     * Gets the simulation activity counter.
     */
    virtual int getSimActivityCount() = 0;

    /**
     * Resets the interface to the default values.
     */
    virtual HRESULT reset() = 0;

    /**
     * Destroys the internally created objects.
     */
    virtual HRESULT destroyObjects() = 0;

    /**
     * Sets the current replay to be created.
     */
    virtual HRESULT setCurrentReplay( CATIASiExWReplay* iReplay ) = 0;
    /**
     * Gets the current replay to be created.
     */
    virtual CATIASiExWReplay* getCurrentReplay() = 0;
    /**
     * Resets the current replay.
     */
    virtual HRESULT resetCurrentReplay() = 0;

    /**
     * Sets the functor management status.
     */
    virtual HRESULT setFunctorMgt( const boolean iEnable ) = 0;
    /**
     * Gets the functor management status.
     */
    virtual boolean getFunctorMgt() = 0;

    /**
     * Sets the Tasks enable status.
     */
    virtual HRESULT setTaskEnableStatus( boolean enableTask ) = 0;
    /**
     * Gets the Tasks enable status.
     */
    virtual boolean getTaskEnableStatus() = 0;

    /**
     * Sets the simulation settings.
     */
    virtual HRESULT setSimSettings( int iSettings ) = 0;
    /**
     * Gets the simulation settings.
     */
    virtual int getSimSettings() = 0;

    /**
     * Append activity with precedence constraints.
     */
    virtual HRESULT appendActPrecedence( CATBaseUnknown_var act ) = 0;
    /**
     * Link activities with precedence constraints.
     */
    virtual HRESULT buildActPrecedence() = 0;
    /**
     * Reset list of activities with precedence constraints.
     */
    virtual HRESULT resetActPrecedence() = 0;

    /**
     * Sets the information needed for the progress bar.
     */
    virtual HRESULT setProgressTaskUI( CATIProgressTaskUI* pUI ) = 0;
    /**
     * Gets the information needed for the progress bar.
     */
    virtual CATIProgressTaskUI* getProgressTaskUI() = 0;
    /**
     * Sets the range for the progress bar.
     */
    virtual HRESULT setRangeProgressTaskUI( long iMin = 0, long iMax = 100 ) = 0;
    /**
     * Updates the progress bar with the given value.
     * @param iVal
     *  The value to be set (default 1).
     * @param bSet
     *  Indicates if the given value will be set or added to the current value
     *  (defaul FALSE - the value will be added to the current one).
     */
    virtual HRESULT updateProgressTaskUI( long iVal = 1, boolean bSet = FALSE ) = 0;

    /**
     * Gets the analysis object, creates it if necessary.
     */
    virtual DNBAnalysis* getAnalysis( boolean bArg = FALSE, boolean force = FALSE ) = 0;
    /**
     * Resets the analysis object.
     */
    virtual HRESULT resetAnalysis() = 0;

    /**
     * Inits the simulation environment (create the World and the root assembly).
     */
    virtual HRESULT Init() = 0;
    /**
     * Destroys the simulation environment (the World).
     */
    virtual HRESULT Destroy() = 0;
    /**
     * Issues a graphic update (by flusing the Observer List).
     */
    virtual HRESULT UpdateWorld() = 0;
    /**
     * Adds a DNBBasicTHRMessage to be used during simulation
     */
    virtual HRESULT addPCQMessage( const CATUnicodeString &name, DNBBasicTHRMessage *msg ) = 0;
    /**
     * Removes a DNBBasicTHRMessage to be used during simulation
     */
    virtual DNBBasicTHRMessage *removePCQMessage( const CATUnicodeString &name ) = 0;
    /**
     * Gets a DNBBasicTHRMessage to be used during simulation
     */
    virtual DNBBasicTHRMessage *getPCQMessage( const CATUnicodeString &name ) = 0;
    /**
     * Fills a DNBGroupMessage with DNBBasicTHRMessages
     */
    virtual HRESULT appendPCQMessages( DNBGroupMessage *msg ) = 0;
    /**
     * Deletes the DNBBasicTHRMessages to be used during simulation
     */
    virtual HRESULT resetPCQMessages() = 0;
    /**
     * Link activities with precedence and/or time constraints, based on given argument.
     */
    virtual HRESULT buildActConstraints( const CATUnicodeString &iType ) = 0;

    /**
     * Sets the current build resource traverser.
     */
    virtual HRESULT setBuildResourceTraverser( DNBProductTraverser* pTrv ) = 0;
    /**
     * Gets the current build resource traverser.
     */
    virtual DNBProductTraverser* getBuildResourceTraverser( boolean force = FALSE ) = 0;
    /**
     * Resets the current build resource traverser.
     */
    virtual HRESULT resetBuildResourceTraverser() = 0;
    
    /**
     * Sets the current destroy resource traverser.
     */
    virtual HRESULT setDestroyResourceTraverser( DNBProductTraverser* pTrv ) = 0;
    /**
     * Gets the current destroy resource traverser.
     */
    virtual DNBProductTraverser* getDestroyResourceTraverser( boolean force = FALSE ) = 0;
    /**
     * Resets the current destroy resource traverser.
     */
    virtual HRESULT resetDestroyResourceTraverser() = 0;
    
    /**
     * Sets the resource context flag.
     */
    virtual HRESULT setResourceContextFlag( int iCtx ) = 0;
    /**
     * Gets the resource context flag.
     */
    virtual int getResourceContextFlag() = 0;
    /**
     * Returns TRUE if the resource context flag is set to DNB_RESOURCE_CTX.
     */
    virtual boolean isResourceContext() = 0;

    /**
     * Registers a visitor to be used while building the process.
     */
    virtual HRESULT registerBuildProcVisitor( DNBTraversalVisitor* iBuildVis ) = 0;
    /**
     * Registers a visitor to be used while destroying the process.
     */
    virtual HRESULT registerDestroyProcVisitor( DNBTraversalVisitor* iDestroyVis ) = 0;
    /**
     * Removes a previously registered build process visitor, based on its class name.
     */
    virtual HRESULT removeBuildProcVisitor( const CATString &iClassName ) = 0;
    /**
     * Removes a previously registered destroy process visitor, based on its class name.
     */
    virtual HRESULT removeDestroyProcVisitor( const CATString &iClassName ) = 0;
    /**
     * Appends the registered build process visitors to the given process traverser.
     */
    virtual HRESULT appendBuildProcVisitors( DNBProcessTraverser* pBuildTrav ) = 0;
    /**
     * Appends the registered destroy process visitors to the given process traverser.
     */
    virtual HRESULT appendDestroyProcVisitors( DNBProcessTraverser* pDestroyTrav ) = 0;

    /**
     * Sets the currently pre-selected activity.
     */
    virtual HRESULT setCrtSelActivity( CATISPPChildManagement_var& crtSelAct ) = 0;
    /**
     * Gets the currently pre-selected activity, if any.
     */
    virtual CATISPPChildManagement_var getCrtSelActivity() = 0;
    /**
     * Resets the currently pre-selected activity.
     */
    virtual HRESULT resetCrtSelActivity() = 0;
};

//------------------------------------------------------------------
CATDeclareHandler( DNBISimEnv, CATBaseUnknown );

#endif
