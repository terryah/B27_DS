/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2002
//==============================================================================
//
// DNBActCreationOpt.h
//      Defines options for the simulation activities.
//
//==============================================================================
//
// Usage notes: 
//      Use these values to specify different options for the simulation 
//      activities.
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     mmh          09/24/2002   Initial implementation
//     mmh          08/13/2003   Add option for State Management
//     mmh          03/16/2004   Add options for Simulation Control Tools tlb.
//     mmh          06/16/2004   Add options for Simulation Player tlb.
//     smw          10/06/2004   Added option for annotation activities
//     mmh          04/19/2005   Added option for simulation commands type to 
//                               support behavior options (R16DMR003)
//     rtl          04/28/2005   Added option for End Conditions
//==============================================================================
#ifndef DNBSimActOpt_H
#define DNBSimActOpt_H

/**
 * Activity Creation Options.
 */
#define DNB_CREATE_DEFAULT  0x00000000
#define DNB_CREATE_BEFORE   0x00000001
#define DNB_CREATE_AFTER    0x00000002
#define DNB_CREATE_CHILD    0x00000004
#define DNB_CREATE_ALL      0xffffffff

/**
 * Simulation Activity Mode.
 */
#define DNB_UNDEFINED       0x00000000
#define DNB_BUILD_TIME      0x00000001
#define DNB_RUN_TIME        0x00000002
#define DNB_RUN_SPEC        0x00000004

/**
 * Simulation Settings.
 */
#define DNB_ENABLE_TEXT     0x00000001
#define DNB_ENABLE_VIEW     0x00000002
#define DNB_ENABLE_HLNK     0x00000004
#define DNB_ENABLE_PAUSE    0x00000008
#define DNB_ENABLE_STATE_MGT    0x00000010
#define DNB_ENABLE_ANNOTATION	0x00000020   // smw
#define DNB_ENABLE_ENDCONDITION	0x00000040   

/**
 * Simulation Control Tools toolbar.
 */
#define DNB_SCT_SIM_OPT     0x00000001
#define DNB_SCT_SIM_ACT     0x00000002
#define DNB_SCT_ACT_OPT     0x00000004
#define DNB_SCT_REAL_TIME   0x00000008
#define DNB_SCT_VIS_OPT     0x00000010
#define DNB_SCT_IREP_OPT    0x00000020
#define DNB_SCT_ALL         0xffffffff

/**
 * Simulation Player toolbar.
 */
#define DNB_SP_JUMP_START   0x00000001
#define DNB_SP_PLAY_BACK    0x00000002
#define DNB_SP_STEP_BACK    0x00000004
#define DNB_SP_PAUSE        0x00000008
#define DNB_SP_STEP_FWD     0x00000010
#define DNB_SP_PLAY_FWD     0x00000020
#define DNB_SP_JUMP_END     0x00000040
#define DNB_SP_SPINNER      0x00000080
#define DNB_SP_EDITOR       0x00000100
#define DNB_SP_LOOP         0x00000200
#define DNB_SP_CLOSE        0x00000400
#define DNB_SP_ALL          0xffffffff

#endif //DNBSimActOpt_H

