#ifndef DNBAnalysisLevel_H
#define DNBAnalysisLevel_H

/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */
// COPYRIGHT DASSAULT SYSTEMES 2004
//--------------------------------------------------------------------------
// DNBAnalysisLevel Enum
//--------------------------------------------------------------------------

    /**
     * The Analysis Level setting attribute range of values.
     * @param DNBAnalysisLevelOff
     *   The Analysis Level is Off
     * @param DNBAnalysisLevelHighlight
     *   The Analysis Level is Highlight (highlight is used to notify the user).
     * @param DNBAnalysisLevelVerbose
     *   The Analysis Level is Verbose (highlight and messages are used to 
     *   notify the user).
     * @param DNBAnalysisLevelInterrupt
     *   The Analysis Level is Interrupt (highlight and messages are used to 
     *   notify the user and the simulation will be stopped).
     */
enum DNBAnalysisLevel {
  DNBAnalysisLevelOff,
  DNBAnalysisLevelHighlight,
  DNBAnalysisLevelVerbose,
  DNBAnalysisLevelInterrupt
};
#endif
