/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2000
//==============================================================================
//
// DNBIAnalysisConfiguration.h
//   Define the DNBIAnalysisConfiguration interface
//
//==============================================================================
//
// Usage notes: 
//   Used to access the clash analysis specifications.
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     mmr          11/01/2000   Initial implementation
//     ymo          03/01/2001   Added new get/set methods for Velocity and
//                               Acceleration status
//     mmh          08/20/2001   Add new set/get methods for linear/rotation
//                               velocity/acceleration limits check.
//     mmh          06/14/2002   Add methods for ModelUpdates
//     mmh          06/17/2002   Add levels for the analysis objects.
//     mmh          07/01/2002   Add methods to get/set color for device/robot 
//                               analysis
//     mmg          03/18/2003   add methods to get/set highlight mode
//     sra          04/05/2003   Added methods to Set & Get the Waiting Resource
//                               color code
//     sra          05/09/2003   Add methods to Get & Set the IO Analysis Status
//     mmh          07/22/2003   Add methods for Soft Limits (per GSX request)
//     rtl          09/20/2004   Change method signatures for setting individual
//                               reporting levels on analysis objects
//     mmh          12/01/2004   Add methods/change method signatures to monitor
//                               analysis objects (R15)
//     mmh          01/28/2008   Add methods for ForceAnlVerbose status
//
//==============================================================================
#ifndef DNBIAnalysisConfiguration_H
#define DNBIAnalysisConfiguration_H

#include <DNBSimulationItfCPP.h>
#include <DNBAnalysisMode.h>
#include <CATBaseUnknown.h>
#include <CATISiAnalysisList.h>
#include <CATBooleanDef.h>

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByDNBSimulationItfCPP  IID IID_DNBIAnalysisConfiguration ;
#else
extern "C" const IID IID_DNBIAnalysisConfiguration ;
#endif

//------------------------------------------------------------------------------

/**
 * <p>
 * This interface is used to access the clash analysis specifications.
 */
class ExportedByDNBSimulationItfCPP DNBIAnalysisConfiguration : 
                                                        public CATBaseUnknown
{
    CATDeclareInterface;

public:

    /**
     * Outputs the status of the Clash Analysis.
     *   @param oOnOffStop
     *      output parameter containing the status
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT GetStatus(DNBAnalysisMode &oOnOffStop) = 0;

    /**
     * Sets the status of the Clash Analysis.
     *   @param iOnOffStop
     *      input parameter containing the status
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT SetStatus(DNBAnalysisMode iOnOffStop)  = 0;

    /**
     * Gets the analysis specs list.
     *   @param oList
     *      output parameter containing the analysis specs list
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT GetAnlList(CATISiAnalysisList &oList) = 0; 

    /**
     * Gets the active analysis specs list.
     *   @param oList
     *      output parameter containing the analysis active specs list
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT GetActiveAnlList(CATISiAnalysisList &oList) = 0; 

    /**
     * Gets the active analysis specs list.
     *   @param oList
     *      output parameter containing the active analysis specs list.
     *   @param oLevelList
     *      output parameter containing the analysis level list for the active 
     *      analysis specs.
     *   @param oMonitorList
     *      output parameter containing the monitor list for the active analysis 
     *      specs.
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT GetActiveAnlList( CATISiAnalysisList &oList, 
                CATListOfInt &oLevelList, CATListOfInt &oMonitorList ) = 0; 

    /**
     * Sets the active analysis specs list.
     *   @param iList
     *      input parameter containing the analysis active specs list.
     *   @param iLevelList
     *      input parameter containing the analysis level list for the active 
     *      analysis specs.
     *   @param iMonitorList
     *      input parameter containing the monitor list for the active analysis 
     *      specs.
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT SetActiveAnlList( CATISiAnalysisList &iList, 
                        CATListOfInt *iLevelList = NULL,
                        CATListOfInt *iMonitorList = NULL) = 0; 

    /**
     * Updates the active analysis specs list.
     *   @param iAnalysis
     *      input parameter containing the analysis
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT UpdateActiveAnlList(CATISiAnalysis_var &iAnalysis) = 0; 

    /**
     * Updates the active analysis specs list.
     *   @param iList
     *      input parameter containing the analysis active specs list
     *      to be added to current analysis active list
     *   @param iLevelList
     *      input parameter containing the analysis level list for the given 
     *      active analysis specs.
     *   @param iMonitorList
     *      input parameter containing the monitor list for the given active 
     *      analysis specs.
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT UpdateActiveAnlList( CATISiAnalysisList &iList, 
                        CATListOfInt *iLevelList = NULL,
                        CATListOfInt *iMonitorList = NULL) = 0; 

    /**
     * Gets the current version number.
     *   @param oVersion
     *      output parameter contains the current version number
     *   @return HRESULT
     *      Error code of function.
     */

    virtual HRESULT GetCurrentVersion (unsigned int &oVersion) = 0;

    /**
     * Gets the status of the Analysis objects (interference, distance).
     *   @param oStatus
     *      output parameter contains the status of the analysis objects.
     *   @param iLevelChangeStatus
     *      input parameter.
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT GetAnalysisStatus( int &oStatus, 
                        boolean iLevelChangeStatus = FALSE ) = 0;
    /**
     * Sets the status of the Analysis objects (interference, distance).
     *   @param iStatus
     *      input parameter sets the status of the analysis objects
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT SetAnalysisStatus( int iStatus ) = 0;

    /**
     * Gets the monitor values of the Analysis objects.
     *   @param oMonitor
     *      output parameter contains the monitor value of the analysis objects.
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT GetAnalysisMonitor( int &oMonitor ) = 0;
    /**
     * Sets the monitor value for the Analysis objects.
     *   @param iMonitor
     *      input parameter sets the monitor value for the analysis objects
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT SetAnalysisMonitor( int iMonitor ) = 0;

    /**
     * Gets the status of the Travel Limit analysis for all devices
     *   @param oStatus
     *      output parameter contains the status of the travel limit analysis
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT GetTravelLimitStatus( int &oStatus ) = 0;
    /**
     * Sets the status of the travel limit analysis for all devices
     *   @param iStatus
     *      input parameter sets the status of the travel limit analysis
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT SetTravelLimitStatus( int iStatus ) = 0;

      
    /**
     * Gets the status of the Velocity Limit analysis for all devices
     *   @param oStatus
     *      output parameter contains the status of velocity limit analysis 
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT GetVelocityLimitStatus( int &oStatus ) = 0;
    /**
     * Sets the status of the Velocity Limit analysis for all devices
     *   @param iStatus
     *      input parameter sets the status of the velocity limit analysis
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT SetVelocityLimitStatus( int iStatus ) = 0;

    /**
     * Gets the status of the Acceleration Limit analysis for all devices
     *   @param oStatus
     *      output parameter contains the status of acceleration limit analysis
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT GetAccelerationLimitStatus( int &oStatus ) = 0; 
    /**
     * Sets the status of the Acceleration Limit analysis for all devices
     *   @param iStatus
     *      input parameter sets the status of acceleration limit analysis
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT SetAccelerationLimitStatus( int iStatus ) = 0;

    /**
     * Gets the status of the Linear Velocity Limit analysis for all robots
     *   @param oStatus
     *      output parameter contains status of linear velocity limit analysis
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT GetLinVelLimitStatus( int &oStatus ) = 0;
    /**
     * Sets the status of the Linear Velocity Limit analysis for all robots
     *   @param iStatus
     *      input parameter sets the status of linear velocity limit analysis 
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT SetLinVelLimitStatus( int iStatus ) = 0;

    /**
     * Gets the status of the Rotation Velocity Limit analysis for all robots
     *   @param oStatus
     *      output parameter contains status of rotation velocity limit analysis
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT GetRotVelLimitStatus( int &oStatus ) = 0;
    /**
     * Sets the status of the Rotation Velocity Limit analysis for all robots
     *   @param iStatus
     *      input parameter sets the status of rotation velocity limits analysis
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT SetRotVelLimitStatus( int iStatus ) = 0;

    /**
     * Gets the status of the Linear Acceleration Limit analysis for all robots
     *   @param oStatus
     *      output parameter contains status of linear acceleration limit analysis
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT GetLinAccLimitStatus( int &oStatus ) = 0;
    /**
     * Sets the status of the Linear Acceleration Limit analysis for all robots
     *   @param iStatus
     *      input parameter sets the status of linear acceleration limits analysis
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT SetLinAccLimitStatus( int iStatus ) = 0;

    /**
     * Gets the status of the Rotation Acceleration Limit analysis for all robots
     *   @param oStatus
     *      output parameter contains status of rotation acceleration limits analysis
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT GetRotAccLimitStatus( int &oStatus ) = 0;
    /**
     * Sets the status of the Rotation Acceleration Limit analysis for all robots
     *   @param iStatus
     *      input parameter sets the status of rotation acceleration limits analysis
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT SetRotAccLimitStatus( int iStatus ) = 0;

    /**
     * Gets the color used by the Travel Limit analysis for all devices
     *   @param oR
     *      output parameter contains the color(red component) used by the 
     *      travel limit analysis
     *   @param oG
     *      output parameter contains the color(green component) used by the 
     *      travel limit analysis
     *   @param oB
     *      output parameter contains the color(blue component) used by the 
     *      travel limit analysis
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT GetTravelLimitColor( unsigned int &oR, unsigned int &oG, 
                    unsigned int &oB ) = 0;
    /**
     * Sets the color used by the travel limit analysis for all devices
     *   @param iR
     *      input parameter sets the color(red component) used by the travel 
     *      limit analysis
     *   @param iG
     *      input parameter sets the color(green component) used by the travel 
     *      limit analysis
     *   @param iB
     *      input parameter sets the color(blue component) used by the travel 
     *      limit analysis
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT SetTravelLimitColor( unsigned int iR, unsigned int iG, 
                    unsigned int iB ) = 0;

    /**
     * Gets the color used by the Velocity Limit analysis for all devices
     *   @param oR
     *      output parameter contains the color(red component) used by the
     *      velocity limit analysis 
     *   @param oG
     *      output parameter contains the color(green component) used by the
     *      velocity limit analysis 
     *   @param oB
     *      output parameter contains the color(blue component) used by the
     *      velocity limit analysis 
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT GetVelocityLimitColor( unsigned int &oR, unsigned int &oG, 
                    unsigned int &oB ) = 0;
    /**
     * Sets the color used by the Velocity Limit analysis for all devices
     *   @param iR
     *      input parameter sets the color(red component) used by the velocity
     *      limit analysis
     *   @param iG
     *      input parameter sets the color(green component) used by the velocity
     *      limit analysis
     *   @param iB
     *      input parameter sets the color(blue component) used by the velocity
     *      limit analysis
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT SetVelocityLimitColor( unsigned int iR, unsigned int iG, 
                    unsigned int iB ) = 0;

    /**
     * Gets the color used by the Acceleration Limit analysis for all devices
     *   @param oR
     *      output parameter contains the color(red component) used by 
     *      acceleration limit analysis
     *   @param oG
     *      output parameter contains the color(green component) used by 
     *      acceleration limit analysis
     *   @param oB
     *      output parameter contains the color(blue component) used by 
     *      acceleration limit analysis
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT GetAccelLimitColor( unsigned int &oR, unsigned int &oG, 
                    unsigned int &oB ) = 0; 
    /**
     * Sets the color used by the Acceleration Limit analysis for all devices
     *   @param iR
     *      input parameter sets the color(red component) used by acceleration
     *      limit analysis
     *   @param iG
     *      input parameter sets the color(green component) used by acceleration
     *      limit analysis
     *   @param iB
     *      input parameter sets the color(blue component) used by acceleration
     *      limit analysis
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT SetAccelLimitColor( unsigned int iR, unsigned int iG, 
                    unsigned int iB ) = 0;

    /**
     * Gets the color used by the Linear Velocity Limit analysis for all robots
     *   @param oR
     *      output parameter contains color(red component) used by linear 
     *      velocity limit analysis
     *   @param oG
     *      output parameter contains color(green component) used by linear 
     *      velocity limit analysis
     *   @param oB
     *      output parameter contains color(blue component) used by linear 
     *      velocity limit analysis
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT GetLinVelLimitColor( unsigned int &oR, unsigned int &oG, 
                    unsigned int &oB ) = 0;
    /**
     * Sets the color used by the Linear Velocity Limit analysis for all robots
     *   @param iR
     *      input parameter sets the color(red component) used by linear 
     *      velocity limit analysis 
     *   @param iG
     *      input parameter sets the color(green component) used by linear 
     *      velocity limit analysis 
     *   @param iB
     *      input parameter sets the color(blue component) used by linear 
     *      velocity limit analysis 
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT SetLinVelLimitColor( unsigned int iR, unsigned int iG, 
                    unsigned int iB ) = 0;

    /**
     * Gets the color used by the Rotation Velocity Limit analysis for all 
     *  robots
     *   @param oR
     *      output parameter contains color(red component) used by rotation 
     *      velocity limit analysis
     *   @param oG
     *      output parameter contains color(green component) used by rotation 
     *      velocity limit analysis
     *   @param oB
     *      output parameter contains color(blue component) used by rotation 
     *      velocity limit analysis
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT GetRotVelLimitColor( unsigned int &oR, unsigned int &oG, 
                    unsigned int &oB ) = 0;
    /**
     * Sets the color used by the Rotation Velocity Limit analysis for all 
     *  robots
     *   @param iR
     *      input parameter sets the color(red component) used by rotation 
     *      velocity limits analysis
     *   @param iG
     *      input parameter sets the color(green component) used by rotation 
     *      velocity limits analysis
     *   @param iB
     *      input parameter sets the color(blue component) used by rotation 
     *      velocity limits analysis
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT SetRotVelLimitColor( unsigned int iR, unsigned int iG, 
                    unsigned int iB ) = 0;

    /**
     * Gets the color used by the Linear Acceleration Limit analysis for all 
     *  robots
     *   @param oR
     *      output parameter contains color(red component) used by linear 
     *      acceleration limit analysis
     *   @param oG
     *      output parameter contains color(green component) used by linear 
     *      acceleration limit analysis
     *   @param oB
     *      output parameter contains color(blue component) used by linear 
     *      acceleration limit analysis
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT GetLinAccLimitColor( unsigned int &oR, unsigned int &oG, 
                    unsigned int &oB ) = 0;
    /**
     * Sets the color used by the Linear Acceleration Limit analysis for all 
     *  robots
     *   @param iR
     *      input parameter sets the color(red component) used by linear 
     *      acceleration limits analysis
     *   @param iG
     *      input parameter sets the color(green component) used by linear 
     *      acceleration limits analysis
     *   @param iB
     *      input parameter sets the color(blue component) used by linear 
     *      acceleration limits analysis
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT SetLinAccLimitColor( unsigned int iR, unsigned int iG, 
                    unsigned int iB ) = 0;

    /**
     * Gets the color used by the Rotation Acceleration Limit analysis for all 
     *  robots
     *   @param oR
     *      output parameter contains color(red component) used by rotation 
     *      acceleration limits analysis
     *   @param oG
     *      output parameter contains color(green component) used by rotation 
     *      acceleration limits analysis
     *   @param oB
     *      output parameter contains color(blue component) used by rotation 
     *      acceleration limits analysis
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT GetRotAccLimitColor( unsigned int &oR, unsigned int &oG, 
                    unsigned int &oB ) = 0;
    /**
     * Sets the color used by the Rotation Acceleration Limit analysis for all 
     *  robots
     *   @param iR
     *      input parameter sets the color(red component) used by rotation 
     *      acceleration limits analysis
     *   @param iG
     *      input parameter sets the color(green component) used by rotation 
     *      acceleration limits analysis
     *   @param iB
     *      input parameter sets the color(blue component) used by rotation 
     *      acceleration limits analysis
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT SetRotAccLimitColor( unsigned int iR, unsigned int iG, 
                    unsigned int iB ) = 0;

    /**
     * Gets the color used by the Reachability analysis for all robots
     *   @param oR
     *      output parameter contains color(red component) used by reachability 
     *      analysis
     *   @param oG
     *      output parameter contains color(green component) used by reachability 
     *      analysis
     *   @param oB
     *      output parameter contains color(blue component) used by reachability 
     *      analysis
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT GetReachabilityColor( unsigned int &oR, unsigned int &oG, 
                    unsigned int &oB ) = 0;
    /**
     * Sets the color used by the Reachability analysis for all robots
     *   @param iR
     *      input parameter sets the color(red component) used by reachability 
     *      analysis
     *   @param iG
     *      input parameter sets the color(green component) used by reachability 
     *      analysis
     *   @param iB
     *      input parameter sets the color(blue component) used by reachability 
     *      analysis
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT SetReachabilityColor( unsigned int iR, unsigned int iG, 
                    unsigned int iB ) = 0;

    /**
     * Gets the color used by the Singularity analysis for all robots
     *   @param oR
     *      output parameter contains color(red component) used by singularity 
     *      analysis
     *   @param oG
     *      output parameter contains color(green component) used by singularity 
     *      analysis
     *   @param oB
     *      output parameter contains color(blue component) used by singularity 
     *      analysis
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT GetSingularityColor( unsigned int &oR, unsigned int &oG, 
                    unsigned int &oB ) = 0;
    /**
     * Sets the color used by the Singularity analysis for all robots
     *   @param iR
     *      input parameter sets the color(red component) used by singularity 
     *      analysis
     *   @param iG
     *      input parameter sets the color(green component) used by singularity 
     *      analysis
     *   @param iB
     *      input parameter sets the color(blue component) used by singularity 
     *      analysis
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT SetSingularityColor( unsigned int iR, unsigned int iG, 
                    unsigned int iB ) = 0;

    /**
     * Outputs the status of the Design Model Updates.
     *   @param oValue
     *      output parameter containing the status
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT GetModelUpdates(int &oValue) = 0;

    /**
     * Sets the status of the Design Model Updates.
     *   @param iValue
     *      input parameter containing the status
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT SetModelUpdates(int iValue) = 0;

    /**
     * Sets the Highlighting mode for the Analysis objects 
     *   @param iValue
     *      input parameter containing the mode
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT SetHighlightMode(int iValue) = 0;

    /**
     * Gets the Highlighting mode for the Analysis objects 
     *   @param oValue
     *      onput parameter containing the status
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT GetHighlightMode(int &oValue) = 0;

    /**
     * Sets the Wait IO color code for the Analysis objects 
     *   @param iValue iR, iG, iB
     *      input color code parameters 
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT SetWaitingResourceColor(unsigned int iR, unsigned int iG,  unsigned int iB) = 0;

    /**
     * Gets the Wait IO color code for the Analysis objects 
     *   @param oValue iR, iG, iB
     *      output color code parameters
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT GetWaitingResourceColor(unsigned int& iR, unsigned int& iG, unsigned int& iB) = 0;

    /**
     * Gets the Wait IO color code Analysis Status 
     *   @param oValue
     *      output parameter containing the IO Analysis status
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT GetIOAnalysisStatus( int &status ) = 0;

    /**
     * Sets the Wait IO color code Analysis Status 
     *   @param iValue Status
     *      input parameter containing the IO Analysis status
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT SetIOAnalysisStatus( int status ) = 0;

    /**
     * Gets the status of the Caution zone analysis for all devices
     *   @param oStatus
     *      output parameter contains the status of the Soft limit analysis
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT GetCautionZoneStatus( int &oStatus ) = 0;

    /**
     * Sets the status of the Caution zone analysis for all devices
     *   @param iStatus
     *      input parameter sets the status of the Soft limit analysis
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT SetCautionZoneStatus( int iStatus ) = 0;

    /**
     * Gets the color used by the Caution zone analysis for all devices
     *   @param oR
     *      output parameter contains the color(red component) used by the 
     *      Soft limit analysis
     *   @param oG
     *      output parameter contains the color(green component) used by the 
     *      Soft limit analysis
     *   @param oB
     *      output parameter contains the color(blue component) used by the 
     *      Soft limit analysis
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT GetCautionZoneColor( unsigned int &oR, unsigned int &oG, 
                    unsigned int &oB ) = 0;
    /**
     * Sets the color used by the Caution zone analysis for all devices
     *   @param iR
     *      input parameter sets the color(red component) used by the Soft 
     *      limit analysis
     *   @param iG
     *      input parameter sets the color(green component) used by the Soft 
     *      limit analysis
     *   @param iB
     *      input parameter sets the color(blue component) used by the Soft 
     *      limit analysis
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT SetCautionZoneColor( unsigned int iR, unsigned int iG, 
                    unsigned int iB ) = 0;

    /**
     * Gets the scope values of the Analysis objects.
     *   @param oScope
     *      output parameter contains the scope value of the analysis objects.
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT GetAnalysisScope( int &oScope ) = 0;
    /**
     * Sets the scope value for the Analysis objects.
     *   @param iScope
     *      input parameter sets the scope value for the analysis objects
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT SetAnalysisScope( int iScope ) = 0;


    /**
     * Gets the typemask value of the active Analysis objects.
     *   @param oTypeMask
     *      output parameter contains the mask of active Analysis types.
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT GetAnalysisTypeMask( int &oTypeMask ) = 0;
    /**
     * Sets the typemask value of the active Analysis objects.
     *   @param iTypeMask
     *      input parameter sets the typemask value of the active analyses
     *   @return HRESULT
     *      Error code of function.
     */
    virtual HRESULT SetAnalysisTypeMask( int iTypeMask ) = 0;

    /**
     * <dl>
     * Outputs the status of the Force Analysis Verbose.
     *   @param oValue
     *      output parameter containing the status
     *   @return HRESULT
     *      Error code of function.
     * 
     * </pre>
     * </dl>
     */
    virtual HRESULT GetAnlVerboseStatus(int &oValue) = 0;

    /**
     * <dl>
     * Sets the status of the Force Analysis Verbose.
     *   @param iValue
     *      input parameter containing the status
     *   @return HRESULT
     *      Error code of function.
     * 
     * </pre>
     * </dl>
     */
    virtual HRESULT SetAnlVerboseStatus(int iValue) = 0;


    // No constructors or destructors on this pure virtual base class
    // --------------------------------------------------------------
};

//------------------------------------------------------------------------------

CATDeclareHandler( DNBIAnalysisConfiguration, CATBaseUnknown );

#endif //DNBIAnalysisConfiguration_H
