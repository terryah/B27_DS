#ifndef DNBSimGraphUpdateMode_H
#define DNBSimGraphUpdateMode_H

/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */

// COPYRIGHT DASSAULT SYSTEMES 2008
//--------------------------------------------------------------------------
// DNBSimGraphUpdateMode Enum
//--------------------------------------------------------------------------

    /**
     * The PSGraphicUpdate setting attribute range of values.
     * @param DNBSimGraphUpdateDisabled
     *   The Simulation Graphical Updates are disabled.
     * @param DNBSimGraphUpdateEnabled
     *   The Simulation Graphical Updates are enabled.
     */
enum DNBSimGraphUpdateMode {
  DNBSimGraphUpdateDisabled,
  DNBSimGraphUpdateEnabled
};
#endif
