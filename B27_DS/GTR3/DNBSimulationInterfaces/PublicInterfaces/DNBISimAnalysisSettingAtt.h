// COPYRIGHT Dassault Systemes 2004
//===================================================================
//
// DNBISimAnalysisSettingAtt.h
// Define the DNBISimAnalysisSettingAtt interface
//
//===================================================================
//
// Usage notes:
//   New interface: describe its use here
//
//===================================================================
//
//  Jan 2004  Creation: Code generated by the CAA wizard  sha
//  Aug 2004  Modified: Add "voxel in static analysis"    mmh
//  Apr 2006  Modified: Add "synchronize analysis specs"  mmh
//===================================================================
#ifndef DNBISimAnalysisSettingAtt_H
#define DNBISimAnalysisSettingAtt_H

/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */

#include "DNBSimulationItfCPP.h"
#include "CATBaseUnknown.h"
#include "CATBooleanDef.h"
#include "DNBAnalysisLevel.h"
#include "DNBVisualizationMode.h"

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByDNBSimulationItfCPP IID IID_DNBISimAnalysisSettingAtt;
#else
extern "C" const IID IID_DNBISimAnalysisSettingAtt ;
#endif

class CATSettingInfo;
class CATUnicodeString;

//------------------------------------------------------------------

/**  
 * Interface to handle parameters of DELMIA Analysis Tools Options Tab page
 * <b>Role</b>: This interface is implemented by a component which 
 * represents the controller of Analysis Tools Options parameter settings.
 * <ul>
 * <li>Methods to set value of each parameter</li>
 * <li>Methods to get value of each parameter</li>
 * <li>Methods to get information on each parameter</li>
 * <li>Methods to lock/unlock value of each parameter</li>
 * </ul>
 */
class ExportedByDNBSimulationItfCPP DNBISimAnalysisSettingAtt: public CATBaseUnknown
{
  CATDeclareInterface;

  public:

    //***********************************************************************
    // Ensure consistency with the IDL interface that will delegate its work
    // Check allowed signatures in System.CATSysSettingController
    //***********************************************************************

    //-------------------------------------------------------------------------
    //  BeepMode setting attribute
    //-------------------------------------------------------------------------
    /**
     * Retrieves the BeepMode attribute.
     */
     virtual HRESULT GetBeepMode( CATBoolean& ioBeepMode ) = 0;

    /**
     * Sets the BeepMode attribute.
     */
     virtual HRESULT SetBeepMode( const CATBoolean iBeepMode ) = 0;

    /**
     * Retrieves the state of the BeepMode parameter.
     * @param oInfo
     *    Address of an object CATSettingInfo.
     * @return
     *    <b>Legal values</b>:
     *    <br><tt>S_OK :</tt>   on Success
     *    <br><tt>E_FAIL:</tt>  on failure
     */
     virtual HRESULT GetBeepModeInfo( CATSettingInfo* oInfo ) = 0;

    /** 
     * Locks or unlocks the BeepMode parameter.
     * <br><b>Role</b>: Locks or unlocks the BeepMode parameter if the
     * operation is allowed in the current administrated environment. In user mode 
     * this method will always return E_FAIL.
     * @param iLocked
     *    the locking operation to be performed
     *    <b>Legal values</b>:
     *    <br><tt>1:</tt>   to lock the parameter.
     *    <br><tt>0:</tt>   to unlock the parameter.
     * @return
     *    <b>Legal values</b>:
     *    <br><tt>S_OK :</tt>   on Success
     *    <br><tt>E_FAIL:</tt>  on failure
     */
     virtual HRESULT SetBeepModeLock( unsigned char iLocked ) = 0;

    //-------------------------------------------------------------------------
    //  AskAnlMode setting attribute
    //-------------------------------------------------------------------------
    /**
     * Retrieves the AskAnlMode attribute.
     */
     virtual HRESULT GetAskAnlMode( CATBoolean& ioAskAnlMode ) = 0;

    /**
     * Sets the AskAnlMode attribute.
     */
     virtual HRESULT SetAskAnlMode( const CATBoolean iAskAnlMode ) = 0;

    /**
     * Retrieves the state of the AskAnlMode parameter.
     * @param oInfo
     *    Address of an object CATSettingInfo.
     * @return
     *    <b>Legal values</b>:
     *    <br><tt>S_OK :</tt>   on Success
     *    <br><tt>E_FAIL:</tt>  on failure
     */
     virtual HRESULT GetAskAnlModeInfo( CATSettingInfo* oInfo ) = 0;

    /** 
     * Locks or unlocks the AskAnlMode parameter.
     * <br><b>Role</b>: Locks or unlocks the AskAnlMode parameter if the
     * operation is allowed in the current administrated environment. In user mode 
     * this method will always return E_FAIL.
     * @param iLocked
     *    the locking operation to be performed
     *    <b>Legal values</b>:
     *    <br><tt>1:</tt>   to lock the parameter.
     *    <br><tt>0:</tt>   to unlock the parameter.
     * @return
     *    <b>Legal values</b>:
     *    <br><tt>S_OK :</tt>   on Success
     *    <br><tt>E_FAIL:</tt>  on failure
     */
     virtual HRESULT SetAskAnlModeLock( unsigned char iLocked ) = 0;

    //-------------------------------------------------------------------------
    //  EnableAnlMode setting attribute
    //-------------------------------------------------------------------------
    /**
     * Retrieves the EnableAnlMode attribute.
     */
     virtual HRESULT GetEnableAnlMode( CATBoolean& ioEnableAnlMode ) = 0;

    /**
     * Sets the EnableAnlMode attribute.
     */
     virtual HRESULT SetEnableAnlMode( const CATBoolean iEnableAnlMode ) = 0;

    /**
     * Retrieves the state of the EnableAnlMode parameter.
     * @param oInfo
     *    Address of an object CATSettingInfo.
     * @return
     *    <b>Legal values</b>:
     *    <br><tt>S_OK :</tt>   on Success
     *    <br><tt>E_FAIL:</tt>  on failure
     */
     virtual HRESULT GetEnableAnlModeInfo( CATSettingInfo* oInfo ) = 0;

    /** 
     * Locks or unlocks the EnableAnlMode parameter.
     * <br><b>Role</b>: Locks or unlocks the EnableAnlMode parameter if the
     * operation is allowed in the current administrated environment. In user mode 
     * this method will always return E_FAIL.
     * @param iLocked
     *    the locking operation to be performed
     *    <b>Legal values</b>:
     *    <br><tt>1:</tt>   to lock the parameter.
     *    <br><tt>0:</tt>   to unlock the parameter.
     * @return
     *    <b>Legal values</b>:
     *    <br><tt>S_OK :</tt>   on Success
     *    <br><tt>E_FAIL:</tt>  on failure
     */
     virtual HRESULT SetEnableAnlModeLock( unsigned char iLocked ) = 0;

    //-------------------------------------------------------------------------
    //  AnalysisLevel setting attribute
    //-------------------------------------------------------------------------
    /**
     * Retrieves the AnalysisLevel attribute.
     */
     virtual HRESULT GetAnalysisLevel( DNBAnalysisLevel& ioAnalysisLevel ) = 0;

    /**
     * Sets the AnalysisLevel attribute.
     */
     virtual HRESULT SetAnalysisLevel( const DNBAnalysisLevel iAnalysisLevel ) = 0;

    /**
     * Retrieves the state of the AnalysisLevel parameter.
     * @param oInfo
     *    Address of an object CATSettingInfo.
     * @return
     *    <b>Legal values</b>:
     *    <br><tt>S_OK :</tt>   on Success
     *    <br><tt>E_FAIL:</tt>  on failure
     */
     virtual HRESULT GetAnalysisLevelInfo( CATSettingInfo* oInfo ) = 0;

    /** 
     * Locks or unlocks the AnalysisLevel parameter.
     * <br><b>Role</b>: Locks or unlocks the AnalysisLevel parameter if the
     * operation is allowed in the current administrated environment. In user mode 
     * this method will always return E_FAIL.
     * @param iLocked
     *    the locking operation to be performed
     *    <b>Legal values</b>:
     *    <br><tt>1:</tt>   to lock the parameter.
     *    <br><tt>0:</tt>   to unlock the parameter.
     * @return
     *    <b>Legal values</b>:
     *    <br><tt>S_OK :</tt>   on Success
     *    <br><tt>E_FAIL:</tt>  on failure
     */
     virtual HRESULT SetAnalysisLevelLock( unsigned char iLocked ) = 0;

    //-------------------------------------------------------------------------
    //  VisualizationMode setting attribute
    //-------------------------------------------------------------------------
    /**
     * Retrieves the VisualizationMode attribute.
     */
     virtual HRESULT GetVisualizationMode( DNBVisualizationMode& ioVisualizationMode ) = 0;

    /**
     * Sets the VisualizationMode attribute.
     */
     virtual HRESULT SetVisualizationMode( const DNBVisualizationMode iVisualizationMode ) = 0;

    /**
     * Retrieves the state of the VisualizationMode parameter.
     * @param oInfo
     *    Address of an object CATSettingInfo.
     * @return
     *    <b>Legal values</b>:
     *    <br><tt>S_OK :</tt>   on Success
     *    <br><tt>E_FAIL:</tt>  on failure
     */
     virtual HRESULT GetVisualizationModeInfo( CATSettingInfo* oInfo ) = 0;

    /** 
     * Locks or unlocks the VisualizationMode parameter.
     * <br><b>Role</b>: Locks or unlocks the VisualizationMode parameter if the
     * operation is allowed in the current administrated environment. In user mode 
     * this method will always return E_FAIL.
     * @param iLocked
     *    the locking operation to be performed
     *    <b>Legal values</b>:
     *    <br><tt>1:</tt>   to lock the parameter.
     *    <br><tt>0:</tt>   to unlock the parameter.
     * @return
     *    <b>Legal values</b>:
     *    <br><tt>S_OK :</tt>   on Success
     *    <br><tt>E_FAIL:</tt>  on failure
     */
     virtual HRESULT SetVisualizationModeLock( unsigned char iLocked ) = 0;

    //-------------------------------------------------------------------------
    //  VoxelStaticAnl setting attribute
    //-------------------------------------------------------------------------
    /**
     * Retrieves the VoxelStaticAnl attribute.
     */
     virtual HRESULT GetVoxelStaticAnl( CATBoolean& ioVoxelStaticAnl ) = 0;

    /**
     * Sets the VoxelStaticAnl attribute.
     */
     virtual HRESULT SetVoxelStaticAnl( const CATBoolean iVoxelStaticAnl ) = 0;

    /**
     * Retrieves the state of the VoxelStaticAnl parameter.
     * @param oInfo
     *    Address of an object CATSettingInfo.
     * @return
     *    <b>Legal values</b>:
     *    <br><tt>S_OK :</tt>   on Success
     *    <br><tt>E_FAIL:</tt>  on failure
     */
     virtual HRESULT GetVoxelStaticAnlInfo( CATSettingInfo* oInfo ) = 0;

    /** 
     * Locks or unlocks the VoxelStaticAnl parameter.
     * <br><b>Role</b>: Locks or unlocks the VoxelStaticAnl parameter if the
     * operation is allowed in the current administrated environment. In user mode 
     * this method will always return E_FAIL.
     * @param iLocked
     *    the locking operation to be performed
     *    <b>Legal values</b>:
     *    <br><tt>1:</tt>   to lock the parameter.
     *    <br><tt>0:</tt>   to unlock the parameter.
     * @return
     *    <b>Legal values</b>:
     *    <br><tt>S_OK :</tt>   on Success
     *    <br><tt>E_FAIL:</tt>  on failure
     */
     virtual HRESULT SetVoxelStaticAnlLock( unsigned char iLocked ) = 0;

    //-------------------------------------------------------------------------
    //  AnlIntDist setting attribute
    //-------------------------------------------------------------------------
    /**
     * Retrieves the AnlIntDist attribute.
     */
     virtual HRESULT GetAnlIntDist( CATBoolean& ioAnlIntDist ) = 0;

    /**
     * Sets the AnlIntDist attribute.
     */
     virtual HRESULT SetAnlIntDist( const CATBoolean iAnlIntDist ) = 0;

    /**
     * Retrieves the state of the AnlIntDist parameter.
     * @param oInfo
     *    Address of an object CATSettingInfo.
     * @return
     *    <b>Legal values</b>:
     *    <br><tt>S_OK :</tt>   on Success
     *    <br><tt>E_FAIL:</tt>  on failure
     */
     virtual HRESULT GetAnlIntDistInfo( CATSettingInfo* oInfo ) = 0;

    /** 
     * Locks or unlocks the AnlIntDist parameter.
     * <br><b>Role</b>: Locks or unlocks the AnlIntDist parameter if the
     * operation is allowed in the current administrated environment. In user mode 
     * this method will always return E_FAIL.
     * @param iLocked
     *    the locking operation to be performed
     *    <b>Legal values</b>:
     *    <br><tt>1:</tt>   to lock the parameter.
     *    <br><tt>0:</tt>   to unlock the parameter.
     * @return
     *    <b>Legal values</b>:
     *    <br><tt>S_OK :</tt>   on Success
     *    <br><tt>E_FAIL:</tt>  on failure
     */
     virtual HRESULT SetAnlIntDistLock( unsigned char iLocked ) = 0;

    //-------------------------------------------------------------------------
    //  AnlTravelLimit setting attribute
    //-------------------------------------------------------------------------
    /**
     * Retrieves the AnlTravelLimit attribute.
     */
     virtual HRESULT GetAnlTravelLimit( CATBoolean& ioAnlTravelLimit ) = 0;

    /**
     * Sets the AnlTravelLimit attribute.
     */
     virtual HRESULT SetAnlTravelLimit( const CATBoolean iAnlTravelLimit ) = 0;

    /**
     * Retrieves the state of the AnlTravelLimit parameter.
     * @param oInfo
     *    Address of an object CATSettingInfo.
     * @return
     *    <b>Legal values</b>:
     *    <br><tt>S_OK :</tt>   on Success
     *    <br><tt>E_FAIL:</tt>  on failure
     */
     virtual HRESULT GetAnlTravelLimitInfo( CATSettingInfo* oInfo ) = 0;

    /** 
     * Locks or unlocks the AnlTravelLimit parameter.
     * <br><b>Role</b>: Locks or unlocks the AnlTravelLimit parameter if the
     * operation is allowed in the current administrated environment. In user mode 
     * this method will always return E_FAIL.
     * @param iLocked
     *    the locking operation to be performed
     *    <b>Legal values</b>:
     *    <br><tt>1:</tt>   to lock the parameter.
     *    <br><tt>0:</tt>   to unlock the parameter.
     * @return
     *    <b>Legal values</b>:
     *    <br><tt>S_OK :</tt>   on Success
     *    <br><tt>E_FAIL:</tt>  on failure
     */
     virtual HRESULT SetAnlTravelLimitLock( unsigned char iLocked ) = 0;

    //-------------------------------------------------------------------------
    //  AnlVelocityLimit setting attribute
    //-------------------------------------------------------------------------
    /**
     * Retrieves the AnlVelocityLimit attribute.
     */
     virtual HRESULT GetAnlVelocityLimit( CATBoolean& ioAnlVelocityLimit ) = 0;

    /**
     * Sets the AnlVelocityLimit attribute.
     */
     virtual HRESULT SetAnlVelocityLimit( const CATBoolean iAnlVelocityLimit ) = 0;

    /**
     * Retrieves the state of the AnlVelocityLimit parameter.
     * @param oInfo
     *    Address of an object CATSettingInfo.
     * @return
     *    <b>Legal values</b>:
     *    <br><tt>S_OK :</tt>   on Success
     *    <br><tt>E_FAIL:</tt>  on failure
     */
     virtual HRESULT GetAnlVelocityLimitInfo( CATSettingInfo* oInfo ) = 0;

    /** 
     * Locks or unlocks the AnlVelocityLimit parameter.
     * <br><b>Role</b>: Locks or unlocks the AnlVelocityLimit parameter if the
     * operation is allowed in the current administrated environment. In user mode 
     * this method will always return E_FAIL.
     * @param iLocked
     *    the locking operation to be performed
     *    <b>Legal values</b>:
     *    <br><tt>1:</tt>   to lock the parameter.
     *    <br><tt>0:</tt>   to unlock the parameter.
     * @return
     *    <b>Legal values</b>:
     *    <br><tt>S_OK :</tt>   on Success
     *    <br><tt>E_FAIL:</tt>  on failure
     */
     virtual HRESULT SetAnlVelocityLimitLock( unsigned char iLocked ) = 0;

    //-------------------------------------------------------------------------
    //  AnlAccelLimit setting attribute
    //-------------------------------------------------------------------------
    /**
     * Retrieves the AnlAccelLimit attribute.
     */
     virtual HRESULT GetAnlAccelLimit( CATBoolean& ioAnlAccelLimit ) = 0;

    /**
     * Sets the AnlAccelLimit attribute.
     */
     virtual HRESULT SetAnlAccelLimit( const CATBoolean iAnlAccelLimit ) = 0;

    /**
     * Retrieves the state of the AnlAccelLimit parameter.
     * @param oInfo
     *    Address of an object CATSettingInfo.
     * @return
     *    <b>Legal values</b>:
     *    <br><tt>S_OK :</tt>   on Success
     *    <br><tt>E_FAIL:</tt>  on failure
     */
     virtual HRESULT GetAnlAccelLimitInfo( CATSettingInfo* oInfo ) = 0;

    /** 
     * Locks or unlocks the AnlAccelLimit parameter.
     * <br><b>Role</b>: Locks or unlocks the AnlAccelLimit parameter if the
     * operation is allowed in the current administrated environment. In user mode 
     * this method will always return E_FAIL.
     * @param iLocked
     *    the locking operation to be performed
     *    <b>Legal values</b>:
     *    <br><tt>1:</tt>   to lock the parameter.
     *    <br><tt>0:</tt>   to unlock the parameter.
     * @return
     *    <b>Legal values</b>:
     *    <br><tt>S_OK :</tt>   on Success
     *    <br><tt>E_FAIL:</tt>  on failure
     */
     virtual HRESULT SetAnlAccelLimitLock( unsigned char iLocked ) = 0;

    //-------------------------------------------------------------------------
    //  AnlCautionZone setting attribute
    //-------------------------------------------------------------------------
    /**
     * Retrieves the AnlCautionZone attribute.
     */
     virtual HRESULT GetAnlCautionZone( CATBoolean& ioAnlCautionZone ) = 0;

    /**
     * Sets the AnlCautionZone attribute.
     */
     virtual HRESULT SetAnlCautionZone( const CATBoolean iAnlCautionZone ) = 0;

    /**
     * Retrieves the state of the AnlCautionZone parameter.
     * @param oInfo
     *    Address of an object CATSettingInfo.
     * @return
     *    <b>Legal values</b>:
     *    <br><tt>S_OK :</tt>   on Success
     *    <br><tt>E_FAIL:</tt>  on failure
     */
     virtual HRESULT GetAnlCautionZoneInfo( CATSettingInfo* oInfo ) = 0;

    /** 
     * Locks or unlocks the AnlCautionZone parameter.
     * <br><b>Role</b>: Locks or unlocks the AnlCautionZone parameter if the
     * operation is allowed in the current administrated environment. In user mode 
     * this method will always return E_FAIL.
     * @param iLocked
     *    the locking operation to be performed
     *    <b>Legal values</b>:
     *    <br><tt>1:</tt>   to lock the parameter.
     *    <br><tt>0:</tt>   to unlock the parameter.
     * @return
     *    <b>Legal values</b>:
     *    <br><tt>S_OK :</tt>   on Success
     *    <br><tt>E_FAIL:</tt>  on failure
     */
     virtual HRESULT SetAnlCautionZoneLock( unsigned char iLocked ) = 0;

    //-------------------------------------------------------------------------
    //  AnlLinSpeedLimit setting attribute
    //-------------------------------------------------------------------------
    /**
     * Retrieves the AnlLinSpeedLimit attribute.
     */
     virtual HRESULT GetAnlLinSpeedLimit( CATBoolean& ioAnlLinSpeedLimit ) = 0;

    /**
     * Sets the AnlLinSpeedLimit attribute.
     */
     virtual HRESULT SetAnlLinSpeedLimit( const CATBoolean iAnlLinSpeedLimit ) = 0;

    /**
     * Retrieves the state of the AnlLinSpeedLimit parameter.
     * @param oInfo
     *    Address of an object CATSettingInfo.
     * @return
     *    <b>Legal values</b>:
     *    <br><tt>S_OK :</tt>   on Success
     *    <br><tt>E_FAIL:</tt>  on failure
     */
     virtual HRESULT GetAnlLinSpeedLimitInfo( CATSettingInfo* oInfo ) = 0;

    /** 
     * Locks or unlocks the AnlLinSpeedLimit parameter.
     * <br><b>Role</b>: Locks or unlocks the AnlLinSpeedLimit parameter if the
     * operation is allowed in the current administrated environment. In user mode 
     * this method will always return E_FAIL.
     * @param iLocked
     *    the locking operation to be performed
     *    <b>Legal values</b>:
     *    <br><tt>1:</tt>   to lock the parameter.
     *    <br><tt>0:</tt>   to unlock the parameter.
     * @return
     *    <b>Legal values</b>:
     *    <br><tt>S_OK :</tt>   on Success
     *    <br><tt>E_FAIL:</tt>  on failure
     */
     virtual HRESULT SetAnlLinSpeedLimitLock( unsigned char iLocked ) = 0;

    //-------------------------------------------------------------------------
    //  AnlRotSpeedLimit setting attribute
    //-------------------------------------------------------------------------
    /**
     * Retrieves the AnlRotSpeedLimit attribute.
     */
     virtual HRESULT GetAnlRotSpeedLimit( CATBoolean& ioAnlRotSpeedLimit ) = 0;

    /**
     * Sets the AnlLinSpeedLimit attribute.
     */
     virtual HRESULT SetAnlRotSpeedLimit( const CATBoolean iAnlRotSpeedLimit ) = 0;

    /**
     * Retrieves the state of the AnlRotSpeedLimit parameter.
     * @param oInfo
     *    Address of an object CATSettingInfo.
     * @return
     *    <b>Legal values</b>:
     *    <br><tt>S_OK :</tt>   on Success
     *    <br><tt>E_FAIL:</tt>  on failure
     */
     virtual HRESULT GetAnlRotSpeedLimitInfo( CATSettingInfo* oInfo ) = 0;

    /** 
     * Locks or unlocks the AnlRotSpeedLimit parameter.
     * <br><b>Role</b>: Locks or unlocks the AnlRotSpeedLimit parameter if the
     * operation is allowed in the current administrated environment. In user mode 
     * this method will always return E_FAIL.
     * @param iLocked
     *    the locking operation to be performed
     *    <b>Legal values</b>:
     *    <br><tt>1:</tt>   to lock the parameter.
     *    <br><tt>0:</tt>   to unlock the parameter.
     * @return
     *    <b>Legal values</b>:
     *    <br><tt>S_OK :</tt>   on Success
     *    <br><tt>E_FAIL:</tt>  on failure
     */
     virtual HRESULT SetAnlRotSpeedLimitLock( unsigned char iLocked ) = 0;

    //-------------------------------------------------------------------------
    //  AnlLinAccelLimit setting attribute
    //-------------------------------------------------------------------------
    /**
     * Retrieves the AnlLinAccelLimit attribute.
     */
     virtual HRESULT GetAnlLinAccelLimit( CATBoolean& ioAnlLinAccelLimit ) = 0;

    /**
     * Sets the AnlLinAccelLimit attribute.
     */
     virtual HRESULT SetAnlLinAccelLimit( const CATBoolean iAnlLinAccelLimit ) = 0;

    /**
     * Retrieves the state of the AnlLinAccelLimit parameter.
     * @param oInfo
     *    Address of an object CATSettingInfo.
     * @return
     *    <b>Legal values</b>:
     *    <br><tt>S_OK :</tt>   on Success
     *    <br><tt>E_FAIL:</tt>  on failure
     */
     virtual HRESULT GetAnlLinAccelLimitInfo( CATSettingInfo* oInfo ) = 0;

    /** 
     * Locks or unlocks the AnlLinAccelLimit parameter.
     * <br><b>Role</b>: Locks or unlocks the AnlLinAccelLimit parameter if the
     * operation is allowed in the current administrated environment. In user mode 
     * this method will always return E_FAIL.
     * @param iLocked
     *    the locking operation to be performed
     *    <b>Legal values</b>:
     *    <br><tt>1:</tt>   to lock the parameter.
     *    <br><tt>0:</tt>   to unlock the parameter.
     * @return
     *    <b>Legal values</b>:
     *    <br><tt>S_OK :</tt>   on Success
     *    <br><tt>E_FAIL:</tt>  on failure
     */
     virtual HRESULT SetAnlLinAccelLimitLock( unsigned char iLocked ) = 0;

    //-------------------------------------------------------------------------
    //  AnlRotAccelLimit setting attribute
    //-------------------------------------------------------------------------
    /**
     * Retrieves the AnlRotAccelLimit attribute.
     */
     virtual HRESULT GetAnlRotAccelLimit( CATBoolean& ioAnlRotAccelLimit ) = 0;

    /**
     * Sets the AnlRotAccelLimit attribute.
     */
     virtual HRESULT SetAnlRotAccelLimit( const CATBoolean iAnlRotAccelLimit ) = 0;

    /**
     * Retrieves the state of the AnlRotAccelLimit parameter.
     * @param oInfo
     *    Address of an object CATSettingInfo.
     * @return
     *    <b>Legal values</b>:
     *    <br><tt>S_OK :</tt>   on Success
     *    <br><tt>E_FAIL:</tt>  on failure
     */
     virtual HRESULT GetAnlRotAccelLimitInfo( CATSettingInfo* oInfo ) = 0;

    /** 
     * Locks or unlocks the AnlRotAccelLimit parameter.
     * <br><b>Role</b>: Locks or unlocks the AnlRotAccelLimit parameter if the
     * operation is allowed in the current administrated environment. In user mode 
     * this method will always return E_FAIL.
     * @param iLocked
     *    the locking operation to be performed
     *    <b>Legal values</b>:
     *    <br><tt>1:</tt>   to lock the parameter.
     *    <br><tt>0:</tt>   to unlock the parameter.
     * @return
     *    <b>Legal values</b>:
     *    <br><tt>S_OK :</tt>   on Success
     *    <br><tt>E_FAIL:</tt>  on failure
     */
     virtual HRESULT SetAnlRotAccelLimitLock( unsigned char iLocked ) = 0;

    //-------------------------------------------------------------------------
    //  AnlIOAnalysis setting attribute
    //-------------------------------------------------------------------------
    /**
     * Retrieves the AnlIOAnalysis attribute.
     */
     virtual HRESULT GetAnlIOAnalysis( CATBoolean& ioAnlIOAnalysis ) = 0;

    /**
     * Sets the AnlIOAnalysis attribute.
     */
     virtual HRESULT SetAnlIOAnalysis( const CATBoolean iAnlIOAnalysis ) = 0;

    /**
     * Retrieves the state of the AnlIOAnalysis parameter.
     * @param oInfo
     *    Address of an object CATSettingInfo.
     * @return
     *    <b>Legal values</b>:
     *    <br><tt>S_OK :</tt>   on Success
     *    <br><tt>E_FAIL:</tt>  on failure
     */
     virtual HRESULT GetAnlIOAnalysisInfo( CATSettingInfo* oInfo ) = 0;

    /** 
     * Locks or unlocks the AnlIOAnalysis parameter.
     * <br><b>Role</b>: Locks or unlocks the AnlIOAnalysis parameter if the
     * operation is allowed in the current administrated environment. In user mode 
     * this method will always return E_FAIL.
     * @param iLocked
     *    the locking operation to be performed
     *    <b>Legal values</b>:
     *    <br><tt>1:</tt>   to lock the parameter.
     *    <br><tt>0:</tt>   to unlock the parameter.
     * @return
     *    <b>Legal values</b>:
     *    <br><tt>S_OK :</tt>   on Success
     *    <br><tt>E_FAIL:</tt>  on failure
     */
     virtual HRESULT SetAnlIOAnalysisLock( unsigned char iLocked ) = 0;

    //-------------------------------------------------------------------------
    //  DisplayAnlStatus setting attribute
    //-------------------------------------------------------------------------
    /**
     * Retrieves the DisplayAnlStatus attribute.
     */
     virtual HRESULT GetDisplayAnlStatus( CATBoolean& ioDisplayAnlStatus ) = 0;

    /**
     * Sets the DisplayAnlStatus attribute.
     */
     virtual HRESULT SetDisplayAnlStatus( const CATBoolean iDisplayAnlStatus ) = 0;

    /**
     * Retrieves the state of the DisplayAnlStatus parameter.
     * @param oInfo
     *    Address of an object CATSettingInfo.
     * @return
     *    <b>Legal values</b>:
     *    <br><tt>S_OK :</tt>   on Success
     *    <br><tt>E_FAIL:</tt>  on failure
     */
     virtual HRESULT GetDisplayAnlStatusInfo( CATSettingInfo* oInfo ) = 0;

    /** 
     * Locks or unlocks the AskAnlMode parameter.
     * <br><b>Role</b>: Locks or unlocks the AskAnlMode parameter if the
     * operation is allowed in the current administrated environment. In user mode 
     * this method will always return E_FAIL.
     * @param iLocked
     *    the locking operation to be performed
     *    <b>Legal values</b>:
     *    <br><tt>1:</tt>   to lock the parameter.
     *    <br><tt>0:</tt>   to unlock the parameter.
     * @return
     *    <b>Legal values</b>:
     *    <br><tt>S_OK :</tt>   on Success
     *    <br><tt>E_FAIL:</tt>  on failure
     */
     virtual HRESULT SetDisplayAnlStatusLock( unsigned char iLocked ) = 0;

    //-------------------------------------------------------------------------
    //  AnlIntf setting attribute
    //-------------------------------------------------------------------------
    /**
     * Retrieves the AnlIntf attribute.
     */
     virtual HRESULT GetAnlIntf( CATBoolean& ioAnlIntf ) = 0;

    /**
     * Sets the AnlIntf attribute.
     */
     virtual HRESULT SetAnlIntf( const CATBoolean iAnlIntf ) = 0;

    /**
     * Retrieves the state of the AnlIntf parameter.
     * @param oInfo
     *    Address of an object CATSettingInfo.
     * @return
     *    <b>Legal values</b>:
     *    <br><tt>S_OK :</tt>   on Success
     *    <br><tt>E_FAIL:</tt>  on failure
     */
     virtual HRESULT GetAnlIntfInfo( CATSettingInfo* oInfo ) = 0;

    /** 
     * Locks or unlocks the AnlIntf parameter.
     * <br><b>Role</b>: Locks or unlocks the AnlIntf parameter if the
     * operation is allowed in the current administrated environment. In user mode 
     * this method will always return E_FAIL.
     * @param iLocked
     *    the locking operation to be performed
     *    <b>Legal values</b>:
     *    <br><tt>1:</tt>   to lock the parameter.
     *    <br><tt>0:</tt>   to unlock the parameter.
     * @return
     *    <b>Legal values</b>:
     *    <br><tt>S_OK :</tt>   on Success
     *    <br><tt>E_FAIL:</tt>  on failure
     */
     virtual HRESULT SetAnlIntfLock( unsigned char iLocked ) = 0;

    //-------------------------------------------------------------------------
    //  AnlMeasure setting attribute
    //-------------------------------------------------------------------------
    /**
     * Retrieves the AnlMeasure attribute.
     */
     virtual HRESULT GetAnlMeasure( CATBoolean& ioAnlMeasure ) = 0;

    /**
     * Sets the AnlMeasure attribute.
     */
     virtual HRESULT SetAnlMeasure( const CATBoolean iAnlMeasure ) = 0;

    /**
     * Retrieves the state of the AnlMeasure parameter.
     * @param oInfo
     *    Address of an object CATSettingInfo.
     * @return
     *    <b>Legal values</b>:
     *    <br><tt>S_OK :</tt>   on Success
     *    <br><tt>E_FAIL:</tt>  on failure
     */
     virtual HRESULT GetAnlMeasureInfo( CATSettingInfo* oInfo ) = 0;

    /** 
     * Locks or unlocks the AnlMeasure parameter.
     * <br><b>Role</b>: Locks or unlocks the AnlMeasure parameter if the
     * operation is allowed in the current administrated environment. In user mode 
     * this method will always return E_FAIL.
     * @param iLocked
     *    the locking operation to be performed
     *    <b>Legal values</b>:
     *    <br><tt>1:</tt>   to lock the parameter.
     *    <br><tt>0:</tt>   to unlock the parameter.
     * @return
     *    <b>Legal values</b>:
     *    <br><tt>S_OK :</tt>   on Success
     *    <br><tt>E_FAIL:</tt>  on failure
     */
     virtual HRESULT SetAnlMeasureLock( unsigned char iLocked ) = 0;

    //-------------------------------------------------------------------------
    //  SyncAnlSpecs setting attribute
    //-------------------------------------------------------------------------
    /**
     * Retrieves the SyncAnlSpecs attribute.
     */
     virtual HRESULT GetSyncAnlSpecs( CATBoolean& ioSyncAnlSpecs ) = 0;

    /**
     * Sets the SyncAnlSpecs attribute.
     */
     virtual HRESULT SetSyncAnlSpecs( const CATBoolean iSyncAnlSpecs ) = 0;

    /**
     * Retrieves the state of the SyncAnlSpecs parameter.
     * @param oInfo
     *    Address of an object CATSettingInfo.
     * @return
     *    <b>Legal values</b>:
     *    <br><tt>S_OK :</tt>   on Success
     *    <br><tt>E_FAIL:</tt>  on failure
     */
     virtual HRESULT GetSyncAnlSpecsInfo( CATSettingInfo* oInfo ) = 0;

    /** 
     * Locks or unlocks the SyncAnlSpecs parameter.
     * <br><b>Role</b>: Locks or unlocks the SyncAnlSpecs parameter if the
     * operation is allowed in the current administrated environment. In user mode 
     * this method will always return E_FAIL.
     * @param iLocked
     *    the locking operation to be performed
     *    <b>Legal values</b>:
     *    <br><tt>1:</tt>   to lock the parameter.
     *    <br><tt>0:</tt>   to unlock the parameter.
     * @return
     *    <b>Legal values</b>:
     *    <br><tt>S_OK :</tt>   on Success
     *    <br><tt>E_FAIL:</tt>  on failure
     */
     virtual HRESULT SetSyncAnlSpecsLock( unsigned char iLocked ) = 0;

    //-------------------------------------------------------------------------
    //  ColorMode setting attribute
    //-------------------------------------------------------------------------
    /**
     * Retrieves the ColorMode attribute.
     */
     virtual HRESULT GetColorMode( CATBoolean& ioColorMode ) = 0;

    /**
     * Sets the ColorMode attribute.
     */
     virtual HRESULT SetColorMode( const CATBoolean iColorMode ) = 0;

    /**
     * Retrieves the state of the ColorMode parameter.
     * @param oInfo
     *    Address of an object CATSettingInfo.
     * @return
     *    <b>Legal values</b>:
     *    <br><tt>S_OK :</tt>   on Success
     *    <br><tt>E_FAIL:</tt>  on failure
     */
     virtual HRESULT GetColorModeInfo( CATSettingInfo* oInfo ) = 0;

    /** 
     * Locks or unlocks the ColorMode parameter.
     * <br><b>Role</b>: Locks or unlocks the ColorMode parameter if the
     * operation is allowed in the current administrated environment. In user mode 
     * this method will always return E_FAIL.
     * @param iLocked
     *    the locking operation to be performed
     *    <b>Legal values</b>:
     *    <br><tt>1:</tt>   to lock the parameter.
     *    <br><tt>0:</tt>   to unlock the parameter.
     * @return
     *    <b>Legal values</b>:
     *    <br><tt>S_OK :</tt>   on Success
     *    <br><tt>E_FAIL:</tt>  on failure
     */
     virtual HRESULT SetColorModeLock( unsigned char iLocked ) = 0;

    //-------------------------------------------------------------------------
    //  WhiteMode setting attribute
    //-------------------------------------------------------------------------
    /**
     * Retrieves the WhiteMode attribute.
     */
     virtual HRESULT GetWhiteMode( CATBoolean& ioWhiteMode ) = 0;

    /**
     * Sets the WhiteMode attribute.
     */
     virtual HRESULT SetWhiteMode( const CATBoolean iWhiteMode ) = 0;

    /**
     * Retrieves the state of the WhiteMode parameter.
     * @param oInfo
     *    Address of an object CATSettingInfo.
     * @return
     *    <b>Legal values</b>:
     *    <br><tt>S_OK :</tt>   on Success
     *    <br><tt>E_FAIL:</tt>  on failure
     */
     virtual HRESULT GetWhiteModeInfo( CATSettingInfo* oInfo ) = 0;

    /** 
     * Locks or unlocks the WhiteMode parameter.
     * <br><b>Role</b>: Locks or unlocks the WhiteMode parameter if the
     * operation is allowed in the current administrated environment. In user mode 
     * this method will always return E_FAIL.
     * @param iLocked
     *    the locking operation to be performed
     *    <b>Legal values</b>:
     *    <br><tt>1:</tt>   to lock the parameter.
     *    <br><tt>0:</tt>   to unlock the parameter.
     * @return
     *    <b>Legal values</b>:
     *    <br><tt>S_OK :</tt>   on Success
     *    <br><tt>E_FAIL:</tt>  on failure
     */
     virtual HRESULT SetWhiteModeLock( unsigned char iLocked ) = 0;

    //-------------------------------------------------------------------------
    //  ClashColor setting attribute
    //-------------------------------------------------------------------------
    /**
     * Retrieves the ClashColor attribute.
     */
    virtual HRESULT GetClashColor( unsigned int* ioColor, CATLONG32 iMaxLength, CATLONG32* oReadLength=NULL ) = 0;
    /**
     * Sets the ClashColor attribute.
     */
    virtual HRESULT SetClashColor( const unsigned int* iColor, CATLONG32 iLength=1 ) = 0;
    /**
     * Retrieves the state of the ClashColor parameter.
     * @param oInfo
     *  Address of an object CATSettingInfo.
     * @return
     *  <b>Legal values</b>:
     *  <br><tt>S_OK :</tt>   on Success
     *  <br><tt>E_FAIL:</tt>  on failure
     */
    virtual HRESULT GetClashColorInfo( CATSettingInfo* oInfo ) = 0;
    /** 
     * Locks or unlocks the ClashColor parameter.
     * <br><b>Role</b>: Locks or unlocks the ClashColor parameter if the
     * operation is allowed in the current administrated environment. In user mode 
     * this method will always return E_FAIL.
     * @param iLocked
     *  the locking operation to be performed
     *  <b>Legal values</b>:
     *  <br><tt>1 :</tt>   to lock the parameter.
     *  <br><tt>0:</tt>   to unlock the parameter.
     * @return
     *  <b>Legal values</b>:
     *  <br><tt>S_OK :</tt>   on Success
     *  <br><tt>E_FAIL:</tt>  on failure
     */
    virtual HRESULT SetClashColorLock( unsigned char iLocked ) = 0; 

    //-------------------------------------------------------------------------
    //  ClearanceColor setting attribute
    //-------------------------------------------------------------------------
    /**
     * Retrieves the ClearanceColor attribute.
     */
    virtual HRESULT GetClearanceColor( unsigned int* ioColor, CATLONG32 iMaxLength, CATLONG32* oReadLength=NULL ) = 0;
    /**
     * Sets the ClearanceColor attribute.
     */
    virtual HRESULT SetClearanceColor( const unsigned int* iColor, CATLONG32 iLength=1 ) = 0;
    /**
     * Retrieves the state of the ClearanceColor parameter.
     * @param oInfo
     *  Address of an object CATSettingInfo.
     * @return
     *  <b>Legal values</b>:
     *  <br><tt>S_OK :</tt>   on Success
     *  <br><tt>E_FAIL:</tt>  on failure
     */
    virtual HRESULT GetClearanceColorInfo( CATSettingInfo* oInfo ) = 0;
    /** 
     * Locks or unlocks the ClearanceColor parameter.
     * <br><b>Role</b>: Locks or unlocks the ClearanceColor parameter if the
     * operation is allowed in the current administrated environment. In user mode 
     * this method will always return E_FAIL.
     * @param iLocked
     *  the locking operation to be performed
     *  <b>Legal values</b>:
     *  <br><tt>1 :</tt>   to lock the parameter.
     *  <br><tt>0:</tt>   to unlock the parameter.
     * @return
     *  <b>Legal values</b>:
     *  <br><tt>S_OK :</tt>   on Success
     *  <br><tt>E_FAIL:</tt>  on failure
     */
    virtual HRESULT SetClearanceColorLock( unsigned char iLocked ) = 0; 

  // No constructors or destructors on this pure virtual base class
  // --------------------------------------------------------------
};

//------------------------------------------------------------------
CATDeclareHandler( DNBISimAnalysisSettingAtt, CATBaseUnknown) ;

#endif
