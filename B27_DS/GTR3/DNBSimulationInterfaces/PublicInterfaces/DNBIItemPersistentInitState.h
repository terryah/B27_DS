/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 1999
//==============================================================================
//
// DNBIItemPersistentInitState.h
//   Define the DNBIItemPersistentInitState interface
//
//==============================================================================
//
// Usage notes:
//   Implement this interface for all objects which need to have a simulation
//   initial state.
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     hrn          11/01/1999   Initial implementation
//     mmg          12/20/2002   added support for local restore masks
//     mmh          05/08/2003   Add attribute for Transparency
//     sha          07/22/2004   Modify InitializeInitState() method to create
//                               the state object if required
//     mmh          03/07/2005   D0485038: add new method to return mask of 
//                               existing attributes
//     mmh          06/01/2005   A0497083: modify DNB_ALL_INIT_STATE and add
//                               new value to force application of init state
//     mmh          03/15/2007   Add support for feat file migration
//
//==============================================================================
#ifndef DNBIItemPersistentInitState_H
#define DNBIItemPersistentInitState_H

#include "DNBSimulationItfCPP.h"
#include "CATBaseUnknown.h"
#include "CATBooleanDef.h"
#include "CATUnicodeString.h"

class CATISpecObject;

#define DNB_VISIBILITY_INIT_STATE 0x00000001
#define DNB_POSITION_INIT_STATE   0x00000002
#define DNB_COLOR_INIT_STATE      0x00000004
#define DNB_TRANSP_INIT_STATE     0x00000008
#define DNB_ALL_INIT_STATE        0x0fffffff
#define DNB_FORCE_INIT_STATE      0x10000000

enum DNBRestoreDestination{
    DNBRestoreToModel = 0,
    DNBRestoreToWDM   = 1
};

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByDNBSimulationItfCPP IID IID_DNBIItemPersistentInitState ;
#else
extern "C" const IID IID_DNBIItemPersistentInitState ;
#endif

//------------------------------------------------------------------------------

/**
 * All objects that adhere to this interface will have a persistent initial
 * state of simulation. The implementation for this interface will determine
 * all the properties that need to be saved and retrieved.
 */
class ExportedByDNBSimulationItfCPP DNBIItemPersistentInitState: public CATBaseUnknown
{
  CATDeclareInterface;

  public:

    /** 
     * This is the interface to save the initial state of a given 
     * entity present in the process.
     */
    virtual void SaveInitState( const boolean refresh = FALSE,
                                 const int mask = DNB_ALL_INIT_STATE ) = 0;

    /** 
     * This is the interface to restore the saved init state of a given
     * entity in the process.
     */
    virtual void RestoreInitState( const int mask = DNB_ALL_INIT_STATE,
                                    const DNBRestoreDestination where = DNBRestoreToWDM ) = 0;

    /**
     * Returns the status of the initial state for the querried object.
     */
    virtual boolean HasInitState() = 0;

    /**
     * Initializes the object to point to its initial state. Creates the
     * state object if required.
     */
    virtual void InitializeInitState( CATISpecObject *obj = NULL, 
                                      const boolean create = FALSE ) = 0;

    /**
     * Sets the local mask defining which data to restore.
     */
    virtual void SetLocalRestoreMask( const int mask ) = 0;

    /**
     * Gets the local mask defining which data to restore.
     */
    virtual int GetLocalRestoreMask() = 0;

    /**
     * Gets the mask defining the existing attributes.
     */
    virtual int GetExistingAttrMask() = 0;

    /**
     * Migrates the initial state object to the new container schema, returns 
     * TRUE if the migration succeeded, FALSE otherwise.
     */
    virtual boolean MigrateInitState( CATBaseUnknown* ipBUOld, boolean bExt = FALSE ) = 0;

    /**
     * Returns start-up information: type, name, file.
     */
    virtual HRESULT GetSUInfo( CATUnicodeString& sStartUpType,
                               CATUnicodeString& sStartUpName,
                               CATUnicodeString& sStartUpFeat ) = 0;
    
    // No constructors or destructors on this pure virtual base class
    // --------------------------------------------------------------
};

//------------------------------------------------------------------------------

CATDeclareHandler( DNBIItemPersistentInitState, CATBaseUnknown );

#endif //DNBIItemPersistentInitState_H
