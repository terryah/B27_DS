/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBIMsgWndExt.h
//      Defines the DNBIMsgWndExt interface.
//
//==============================================================================
//
// Usage notes: 
//      Interface used for managing the message windows owned by a V5 object.
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     rtl          03/11/2013   IR-222672V5-6R2012 Fix. CAA Exposition.
//
//==============================================================================
#ifndef DNBI_MSGWNDEXT_H
#define DNBI_MSGWNDEXT_H

#include <CATBaseUnknown.h>
#include <IUnknown.h>
#include <CATUnicodeString.h>

#include <DNBSimulationItfCPP.h>

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByDNBSimulationItfCPP  IID IID_DNBIMsgWndExt ;
#else
extern "C" const IID IID_DNBIMsgWndExt ;
#endif

class DNBBasicMsgWnd;
class CATListPtrDNBBasicMsgWnd;
class CATNotification;

//------------------------------------------------------------------------------

/**
 * Interface used for managing the message windows owned by a V5 object.
 */
class ExportedByDNBSimulationItfCPP DNBIMsgWndExt : public CATBaseUnknown
{
    CATDeclareInterface; 

public:
    /**
     * Adds a new message window.
     */
    virtual HRESULT
    AddMsgWnd( DNBBasicMsgWnd *iWnd ) = 0;
    /**
     * Removes the specified message window.
     */
    virtual HRESULT
    RemoveMsgWnd( DNBBasicMsgWnd *iWnd ) = 0;
    /**
     * Gets a message window with a specified title.
     */
    virtual DNBBasicMsgWnd*
    GetMsgWnd( CATUnicodeString &iTitle ) = 0;
    /**
     * Gets a message window with a specified index.
     */
    virtual DNBBasicMsgWnd*
    GetMsgWndInd( int iIndex ) = 0;
    /**
     * Displays all the message windows.
     */
    virtual HRESULT
    ShowWindows() = 0;
    /**
     * Hides all the message windows.
     */
    virtual HRESULT
    HideWindows() = 0;
    /**
     * Clears all the message windows. If forceClear is not null, it will force
     * all the windows to have the contents erased.
     */
    virtual HRESULT
    ClearWindows( int forceClear = 0 ) = 0;
    /**
     * Deletes all the message windows.
     */
    virtual HRESULT
    DeleteWindows() = 0;
    /**
     * Clean-ups the list of message windows.
     */
    virtual HRESULT
    CleanUpWindows() = 0;
    /**
     * Minimizes all the message windows.
     */
    virtual HRESULT
    MinimizeWindows() = 0;
    /**
     * Maximizes all the message windows.
     */
    virtual HRESULT
    MaximizeWindows() = 0;
    /**
     * Restores all the message windows.
     */
    virtual HRESULT
    RestoreWindows() = 0;
    /**
     * Retrieves a list of all the message windows.
     */
    virtual HRESULT
    GetMsgWndList( CATListPtrDNBBasicMsgWnd *&oList ) = 0;
    /**
     * Retrieves a list of all the message windows of given type.
     */
    virtual HRESULT
    GetMsgWndList( CATListPtrDNBBasicMsgWnd *&oList, const char *iName ) = 0;
    /**
     * Returns the number of message windows.
     */
    virtual int
    GetMsgWndListSize() = 0;
    /**
     * Binds the functors for the visible windows.
     */
    virtual HRESULT
    BindFunctors() = 0;
    /**
     * Notifies the windows that a simulation command was activated.
     */
    virtual HRESULT
    ActivateSimCmd( CATNotification *iNotif ) = 0;
    /**
     * Notifies the windows that a simulation command was deactivated.
     */
    virtual HRESULT
    DeactivateSimCmd( CATNotification *iNotif ) = 0;
    /**
     * Notifies the windows that a simulation command was canceled.
     */
    virtual HRESULT
    CancelSimCmd( CATNotification *iNotif ) = 0;
    /**
     * Exports all the message windows.
     */
    virtual HRESULT
    ExportWindows( CATUnicodeString iFileName, int iSheetType ) = 0;
};

//------------------------------------------------------------------------------

CATDeclareHandler( DNBIMsgWndExt, CATBaseUnknown );

#endif //DNBI_MSGWNDEXT_H
