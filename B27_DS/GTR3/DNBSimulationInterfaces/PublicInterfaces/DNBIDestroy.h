/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//=============================================================================
// COPYRIGHT Dassault Systemes 2001
//=============================================================================
//
// DNBIDestroy.h
// Defines the DNBIDestroy interface
//
//=============================================================================
//
// Usage notes:
//   This generic interface is used to destroy the transient model for V5 activities
//   (proclets). Implement this interface for any V5 object that needs a specific
//   action while it is being destroyed.
//
//=============================================================================
#ifndef DNBIDestroy_H
#define DNBIDestroy_H

#include "DNBSimulationItfCPP.h"
#include "CATBaseUnknown.h"

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByDNBSimulationItfCPP IID IID_DNBIDestroy ;
#else
extern "C" const IID IID_DNBIDestroy ;
#endif

//------------------------------------------------------------------

/**
 * Generic destroy protocol.
 */
class ExportedByDNBSimulationItfCPP DNBIDestroy: public CATBaseUnknown
{
  CATDeclareInterface;

  public:

     /**
     * Called before any of the given object's descendents are called.
     */
    virtual void 
    PreOrderDestroy( const CATBaseUnknown_var father ) =0;

    /**
     * Called after all of the given object's descendents have been called.
     */
    virtual void 
    PostOrderDestroy( const CATBaseUnknown_var father ) =0;


  // No constructors or destructors on this pure virtual base class
  // --------------------------------------------------------------
};
CATDeclareHandler( DNBIDestroy, CATBaseUnknown );
//------------------------------------------------------------------

#endif
