#ifndef DNBActBehaviorType_H
#define DNBActBehaviorType_H

/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */
// COPYRIGHT DASSAULT SYSTEMES 2005
//--------------------------------------------------------------------------
// DNBActBehaviorType Enum
//--------------------------------------------------------------------------

    /**
     * The Behavior setting attribute range of values.
     * @param DNBBehaviorProcess
     *   The behavior for specific activities is: Process.
     * @param DNBBehaviorParent
     *   The behavior for specific activities is: Parent.
     */
enum DNBActBehaviorType {
  DNBBehaviorProcess,
  DNBBehaviorParent
};
#endif
