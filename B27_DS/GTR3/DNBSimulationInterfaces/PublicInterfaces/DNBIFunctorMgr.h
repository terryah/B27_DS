/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBIFunctorMgr.h
//      Defines the DNBIFunctorMgr interface.
//
//==============================================================================
//
// Usage notes: 
//      Interface used for managing the functors and the observers/recorders 
//      added to different objects.
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     mmh          03/08/2002   Initial implementation
//     mmh          05/01/2003   Add methods to get/reset functor mask
//
//==============================================================================
#ifndef DNBI_FUNCTORMGR_H
#define DNBI_FUNCTORMGR_H

#include <CATBaseUnknown.h>

#include <DNBSimulationItfCPP.h>

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByDNBSimulationItfCPP  IID IID_DNBIFunctorMgr ;
#else
extern "C" const IID IID_DNBIFunctorMgr ;
#endif

//------------------------------------------------------------------------------

/**
 * Interface used for managing the functors added to different objects.
 */
class ExportedByDNBSimulationItfCPP DNBIFunctorMgr : public CATBaseUnknown
{
    CATDeclareInterface; 

public:
    /**
     * Binds the functors specified in the mask to the given object.
     */
    virtual HRESULT
    BindFunctors( unsigned int iMask, const CATBaseUnknown_var &iObj ) = 0;
    /**
     * Enables/disables the observers/recorders specified in the mask for the 
     * given object.
     */
    virtual HRESULT
    SetEnableState( unsigned int iMask, const CATBaseUnknown_var &iObj,
                    int iState ) = 0;
    /**
     * Forces observation for the observers/recorders specified in the mask for 
     * the given object.
     */
    virtual HRESULT
    ForceObservation( unsigned int iMask, const CATBaseUnknown_var &iObj ) = 0;
    /**
     * Gets the mask corresponding to the functors that need to be added, based 
     * on the given mask.
     * @param iMask
     *   Mask corresponding to the functors that the client needs.
     * @param oMask
     *   Mask corresponding to the functors that are not created.
     */
    virtual HRESULT
    GetFunctorMask( unsigned int iMask, unsigned int& oMask ) = 0;
    /**
     * Resets the functor mask.
     */
    virtual HRESULT
    ResetFunctorMask() = 0;
};

//------------------------------------------------------------------------------

CATDeclareHandler( DNBIFunctorMgr, CATBaseUnknown );

#endif //DNBI_FUNCTORMGR_H
