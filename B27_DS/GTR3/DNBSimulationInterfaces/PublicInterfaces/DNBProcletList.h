/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2003
//==============================================================================
//
// DNBProcletList.h
//      Implementation of a list of pointers of DNBProclet.
//
//==============================================================================
//
// Usage notes: 
//      Collection class for pointers of proclet elements. Only the following
//      functions are defined:
//          0 Append
//          0 RemovePosition
//          0 RemoveList
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     sha          09/22/2003   Initial implementation
//
//==============================================================================
#ifndef DNB_PROCLET_LIST_H
#define DNB_PROCLET_LIST_H

#include <DNBSimulationItfCPP.h>

class DNBProclet;

//------------------------------------------------------------------------------
// CATLISTP
//------------------------------------------------------------------------------

#include <CATLISTP_Clean.h>

//#include <CATLISTP_AllFunct.h>
#define	CATLISTP_Append
#define	CATLISTP_RemovePosition
#define	CATLISTP_RemoveList

#include <CATLISTP_Declare.h>

#ifdef CATCOLLEC_ExportedBy
#undef CATCOLLEC_ExportedBy
#endif
#define CATCOLLEC_ExportedBy ExportedByDNBSimulationItfCPP

CATLISTP_DECLARE( DNBProclet )

typedef CATLISTP( DNBProclet ) DNBProcletListP;

#endif  // DNB_PROCLET_LIST_H
