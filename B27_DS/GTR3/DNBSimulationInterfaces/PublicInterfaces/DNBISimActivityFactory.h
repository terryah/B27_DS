/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2002
//==============================================================================
//
// DNBISimActivityFactory.h
//      Interface used as a factory for the creation of simulation activities.
//
//==============================================================================
//
// Usage notes: 
//      Interface used as a factory for the creation of simulation activities.
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     mmh          10/25/2002   Initial implementation
//
//==============================================================================
#ifndef DNBISimActivityFactory_H
#define DNBISimActivityFactory_H

#include <CATBaseUnknown.h>
#include <DNBSimulationItfCPP.h>

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByDNBSimulationItfCPP  IID IID_DNBISimActivityFactory ;
#else
extern "C" const IID IID_DNBISimActivityFactory ;
#endif

class CATISPPChildManagement_var;
class CATISPPFlowMgt_var;
class CATUnicodeString;

/**
 * Interface used as a factory for the creation of simulation activities.
 */
class ExportedByDNBSimulationItfCPP DNBISimActivityFactory :
                                                          public CATBaseUnknown
{
    CATDeclareInterface;

public :

    /**
     * Sets the type of the activity to be created.
     */
    virtual HRESULT SetActivityType( const CATUnicodeString& iType ) = 0;

    /**
     * Create the simulation activity as a child of the given activity.
     */
    virtual CATBaseUnknown_var CreateChildSimActivity
                        ( const CATISPPChildManagement_var &iFather ) const = 0;

    /**
     * Create the simulation activity after the given activity.
     */
    virtual CATBaseUnknown_var CreateAfterSimActivity
                        ( const CATISPPFlowMgt_var &iBefore ) const = 0;

    /**
     * Create the simulation activity before the given activity.
     */
    virtual CATBaseUnknown_var CreateBeforeSimActivity
                        ( const CATISPPFlowMgt_var &iAfter ) const = 0;

};

CATDeclareHandler(DNBISimActivityFactory,CATBaseUnknown);

#endif // DNBISimActivityFactory_H

