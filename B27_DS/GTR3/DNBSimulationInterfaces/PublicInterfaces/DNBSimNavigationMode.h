#ifndef DNBSimNavigationMode_H
#define DNBSimNavigationMode_H

/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */
// COPYRIGHT DASSAULT SYSTEMES 2004
//--------------------------------------------------------------------------
// DNBSimNavigationMode Enum
//--------------------------------------------------------------------------

    /**
     * The analysis SimNavigation Mode setting attribute range of values.
     * @param DNBSimNavigationModeStep
     *   The Simulation Navigation Mode is Step.
     * @param DNBSimNavigationModeAnimate
     *   The Simulation Navigation Mode is Animation.
     */
enum DNBSimNavigationMode {
  DNBSimNavigationModeStep,
  DNBSimNavigationModeAnimate
};
#endif
