/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2000
//==============================================================================
//
// DNBISimulationActivity.h
//   Define the DNBISimulationActivity interface
//
//==============================================================================
//
// Usage notes:
//   This interface is used to differenciate the simulation activities from the
//   other activities and to license the simulation activities, as well.
//
//==============================================================================
//
//      cre     sha     01/01/2000      Original implementation.
//      mod     sha     12/16/2003      Moved from DNBSimActivityInterfaces
//==============================================================================
#ifndef DNBISimulationActivity_H
#define DNBISimulationActivity_H

#include "DNBSimulationItfCPP.h"
#include "CATBooleanDef.h"
#include "CATBaseUnknown.h"
#include "CATString.h"

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByDNBSimulationItfCPP IID IID_DNBISimulationActivity;
#else
extern "C" const IID IID_DNBISimulationActivity;
#endif


//------------------------------------------------------------------

/**
 * This interface is used to differenciate the simulation activities from the
 * other activities and to license the simulation activities, as well.
 */
class ExportedByDNBSimulationItfCPP DNBISimulationActivity: 
                                                        public CATBaseUnknown
{
    CATDeclareInterface;
    
public:
    
    enum DNBSimulationType
    {
        Choreography,       // DNBSimulateChoreography
        DeviceActivity,     // DNBSimulateDevice
        Assembly,           // DNBSimulateAssembly
        Human,              // DNBSimulateHuman
        Igrip,              // DNBSimulateIgrip
        Inspect,            // DNBSimulateInspect
        Machining,          // DNBSimulateMachining
        BIW,                // DNBSimulateBIW
        Other               // DNBSimulateOther
    };

    /**
     * Returns TRUE if this activity is autorized to be simulated, 
     * otherwise returns FALSE.
     */
    virtual boolean 
    IsSimulationAuthorized() = 0;

    /**
     * Returns the simulation type of the activity.
     */
    virtual CATString
    GetSimulationType() = 0;
        
    // No constructors or destructors on this pure virtual base class
    // --------------------------------------------------------------
};

//------------------------------------------------------------------

CATDeclareHandler( DNBISimulationActivity, CATBaseUnknown );

#endif
