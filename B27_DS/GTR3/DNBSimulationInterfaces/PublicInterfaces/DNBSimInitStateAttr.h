#ifndef DNBSimInitStateAttr_H
#define DNBSimInitStateAttr_H

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

// COPYRIGHT DASSAULT SYSTEMES 2007
//--------------------------------------------------------------------------
// DNBSimInitStateAttr Enum
//--------------------------------------------------------------------------

    /**
     * Specifies which Initial State attributes will be managed.
     * @param DNBVisInitStateAttr
     *   The value to be used when Visibility attributes are managed.
     * @param DNBPosInitStateAttr
     *   The value to be used when Position attributes are managed.
     * @param DNBColInitStateAttr
     *   The value to be used when Color attributes are managed.
     * @param DNBOpacInitStateAttr
     *   The value to be used when Transparency attributes are managed.
     * @param DNBVisPosInitStateAttr
     *   The value to be used when Visibility and Position attributes are managed.
     * @param DNBVisColInitStateAttr
     *   The value to be used when Visibility and Color attributes are managed.
     * @param DNBVisOpacInitStateAttr
     *   The value to be used when Visibility and Transparency attributes are managed.
     * @param DNBVisPosColInitStateAttr
     *   The value to be used when Visibility, Position and Color attributes are managed.
     * @param DNBVisPosOpacInitStateAttr
     *   The value to be used when Visibility, Position and Transparency attributes are managed.
     * @param DNBVisColOpacInitStateAttr
     *   The value to be used when Visibility, Color and Transparency attributes are managed.
     * @param DNBPosColInitStateAttr
     *   The value to be used when Position and Color attributes are managed.
     * @param DNBPosOpacInitStateAttr
     *   The value to be used when Position and Transparency attributes are managed.
     * @param DNBPosColOpacInitStateAttr
     *   The value to be used when Position, Color and Transparency attributes are managed.
     * @param DNBColOpacInitStateAttr
     *   The value to be used when Color and Transparency attributes are managed.
     * @param DNBAllInitStateAttr
     *   The value to be used when all attributes are managed.
     */
enum DNBSimInitStateAttr {
  DNBVisInitStateAttr,
  DNBPosInitStateAttr,
  DNBColInitStateAttr,
  DNBOpacInitStateAttr,
  DNBVisPosInitStateAttr,
  DNBVisColInitStateAttr,
  DNBVisOpacInitStateAttr,
  DNBVisPosColInitStateAttr,
  DNBVisPosOpacInitStateAttr,
  DNBVisColOpacInitStateAttr,
  DNBPosColInitStateAttr,
  DNBPosOpacInitStateAttr,
  DNBPosColOpacInitStateAttr,
  DNBColOpacInitStateAttr,
  DNBAllInitStateAttr
};

#endif

