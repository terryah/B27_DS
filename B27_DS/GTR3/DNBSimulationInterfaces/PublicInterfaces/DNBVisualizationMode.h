#ifndef DNBVisualizationMode_H
#define DNBVisualizationMode_H

/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */
// COPYRIGHT DASSAULT SYSTEMES 2004
//--------------------------------------------------------------------------
// DNBVisualizationMode Enum
//--------------------------------------------------------------------------

    /**
     * The analysis Visualization Mode setting attribute range of values.
     * @param DNBVisualizationModeHighlight
     *   The Visualization Mode is Highlight (objects in collision will be 
     *   highlighted).
     * @param DNBVisualizationModeCurves
     *   The Visualization Mode is Curves (collision curves will be displayed).
     */
enum DNBVisualizationMode {
  DNBVisualizationModeHighlight,
  DNBVisualizationModeCurves
};
#endif
