/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBIProcletExt.h
//   Defines the DNBIProcletExt interface.
//
//==============================================================================
//
// Usage notes: 
//   This interface is used to manage the simulation entities (proclets) that
//   are associated with an activity during the simulation setup phase.
//   Currently, each activity can have a linear list of proclets associated
//   with it. The order in the list is important, since it specifies the
//   precedence relationship and is used in the simulation.
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     sha          01/01/1999   Initial implementation
//     mmh          01/08/2001   Added some extra methods used by the cycle
//                               time proclet implementation
//     sha          05/01/2002   Restructured the interface
//     mmh          09/19/2002   Change return value for getProcletListSize()
//                               
//     awn          03/13/2003   Removal of const
//     rtl          05/05/2003   Fix for IR A0390437 - add extra methods to
//                               be used by process verification command
//     rtl          05/08/2003   Further removal of const
//     bkh          05/19/2003   Added getName Method.
//     sha          09/22/2003   Eliminate the use of the STL list container
//     mmh          01/16/2004   Provide lists of start/stop proclets, methods
//                               to add start/stop proclets
//     mmh          02/17/2004   Add methods for GetChildren/GetChildrenCount
//     sha          02/20/2004   Add methods for get[Start/Stop]Proclets lists
//
//==============================================================================
#ifndef DNBI_ProcletEXT_H
#define DNBI_ProcletEXT_H

#include <CATBaseUnknown.h>
#include <CATBooleanDef.h>
#include <CATUnicodeString.h>
#include <CATLISTV_CATBaseUnknown.h>
#include <DNBProcletList.h>
#include <DNBSimulationItfCPP.h>

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByDNBSimulationItfCPP  IID IID_DNBIProcletExt ;
#else
extern "C" const IID IID_DNBIProcletExt ;
#endif

class DNBProclet;

typedef DNBProcletListP DNBProcletList;

//------------------------------------------------------------------------------

/**
 * This interface is used to manage the simulation entities (proclets) that
 * are associated with an activity during the simulation setup phase.
 */
class ExportedByDNBSimulationItfCPP DNBIProcletExt : public CATBaseUnknown
{
    CATDeclareInterface;

public:
    
    /**
     * Returns the start proclet.
     * @param iFirst
     *  Specifies if the first or last start proclet is returned. By default
     *  the first start proclet is returned (iFirst = TRUE).
     */
    virtual DNBProclet * GetStart( boolean iFirst = TRUE ) =0;

    /**
     * Returns the stop proclet.
     * @param iFirst
     *  Specifies if the first or last stop proclet is returned. By default
     *  the last stop proclet is returned (iFirst = FALSE).
     */
    virtual DNBProclet * GetStop( boolean iFirst = FALSE ) =0;
    
    /**
     * Appends a proclet to the start list.
     */
    virtual HRESULT addStartProclet( DNBProclet *iProclet ) =0;

    /**
     * Appends a proclet to the stop list.
     */
    virtual HRESULT addStopProclet( DNBProclet *iProclet ) =0;

    /**
     * Returns the next activity's start proclet.
     */
    virtual DNBProclet * GetNextActStart() =0;
    
    /**
     * Returns the previous activity's stop proclet.
     */
    virtual DNBProclet * GetPrevActStop() =0;

    /**
     * Get the nth proclet from the list.
     */
    virtual DNBProclet * GetProclet( int index = 0 ) =0;

    /**
     * Sets the first proclet in the list.
     */
    virtual HRESULT SetProclet( DNBProclet *proclet, boolean autoLink = FALSE  ) =0;
    
    /**
     * Appends a proclet to the list.
     */
    virtual HRESULT appendProclet( DNBProclet *proclet, boolean autoLink = FALSE  ) =0;

    /**
     * Prepends a proclet to the list.
     */
    virtual HRESULT prependProclet( DNBProclet *proclet, boolean autoLink = FALSE  ) =0;
    
    /**
     * Inserts a proclet to the list at the given index.
     */
    virtual HRESULT insertProclet( DNBProclet *proclet,
                                   int index, boolean autoLink = FALSE  ) =0;

    /**
     * Removes the given proclet.
     */
    virtual HRESULT removeProclet( DNBProclet *proclet, boolean autoUnlink = FALSE  ) =0;

    /**
     * Removes the proclet at the given index.
     */
    virtual HRESULT removeProclet( int index, boolean autoUnlink = FALSE  ) =0;

    /**
     * Returns the proclet list size.
     */
    virtual int getProcletListSize() =0;

    /**
     * Retrieves the list of proclets.
     */
    virtual HRESULT getProclets( DNBProcletList *proclist ) =0;

    /**
     * Returns the start proclet list size.
     */
    virtual int getStartProcletListSize() =0;

    /**
     * Retrieves the start list of proclets.
     */
    virtual HRESULT getStartProclets( DNBProcletList *proclist ) =0;

    /**
     * Returns the stop proclet list size.
     */
    virtual int getStopProcletListSize() =0;

    /**
     * Retrieves the stop list of proclets.
     */
    virtual HRESULT getStopProclets( DNBProcletList *proclist ) =0;

    /**
     * Clears out the list of proclets.
     */
    virtual HRESULT clearProclets() =0;

    /**
     * Disable the proclets (stores them in a temp list to be removed).
     */
    virtual HRESULT disableProclets() =0;

    /**
     * Retrieves the list of disabled proclets.
     */
    virtual HRESULT getDisabledProclets( DNBProcletList *proclist ) =0;

    /**
     * Clears out the list of disabled proclets.
     */
    virtual HRESULT clearDisabledProclets() =0;

    virtual HRESULT linkProclets() =0;

    virtual HRESULT setRecurseStatus( boolean recurse ) =0;
    virtual boolean getRecurseStatus() =0;

    virtual HRESULT setBuildStatus( boolean built ) =0;
    virtual boolean getBuildStatus() =0;

    virtual HRESULT reset() =0;

    /**
     * Returns the resource proclet list size
     */
    virtual int getResourceProcletListSize() =0;

    /**
     * Retrieves the list of resource proclets
     */
    virtual HRESULT getResourceProclets( DNBProcletList *proclist ) =0;

    /**
     * Insert the proclet in the resource proclet list
     */
    virtual HRESULT insertResourceProclet(DNBProclet *rPlet) =0;

    /**
     * Returns the name of the object.
     */
    virtual HRESULT getName(CATUnicodeString& Label) =0;

    /**
     * Returns number of children from an Activity, with the possibility to 
     * search for specific Activity types (Feeder/Deliver children are ignored).
     * @param  iType
     *  <tt> Name of the Activity type used for the search, by default, equals 
     *  to Activity (Physical) </tt>
     * @return
     *  Number of Activities.
     **/ 
    virtual int GetChildrenCount( const char * iType = "" ) =0;

    /**
     * Returns the children from an Activity, with the possibility to search for
     * specific Activity types (Feeder/Deliver children are ignored).
     * @param oChildren
     *  <tt> List of Interface Handlers for the Activities as Children. </tt>
     * @param iType
     *  <tt> Name of the Activity type used for the search, by default, equals 
     *  to Activity (Physical). </tt>
     * @return
     *   E_FAIL if there was an error, S_OK otherwise.
     **/ 
    virtual HRESULT GetChildren( CATListValCATBaseUnknown_var& oChildren, 
                            const char * iType = "" ) =0;
};

//------------------------------------------------------------------------------

CATDeclareHandler( DNBIProcletExt, CATBaseUnknown );

#endif //DNBI_ProcletEXT_H
