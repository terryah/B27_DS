/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBIBuild.h
//   Defines DNBIBuild interface.
//
//==============================================================================
//
// Usage notes: 
//   This generic interface is used to build V5 objects (activities, products)
//   into the transient model. Implement this interface for any V5 object that 
//   needs to be simulated.
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     mmg          01/01/1999   Initial implementation
//     mmh          11/29/2001   Add new value for DNBBuildType
//     awn          03/13/2003   Removal of const
//
//==============================================================================
#ifndef DNBI_BUILD_H
#define DNBI_BUILD_H

#include <CATBaseUnknown.h>
#include <CATBooleanDef.h>
#include <DNBSimulationItfCPP.h>

enum DNBBuildType { DefaultBuild, UpdateBuild, BranchBuild };

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByDNBSimulationItfCPP  IID IID_DNBIBuild;
#else
extern "C" const IID IID_DNBIBuild;
#endif

//------------------------------------------------------------------------------

/**
 * Generic build protocol.
 */
class ExportedByDNBSimulationItfCPP  DNBIBuild : public CATBaseUnknown
{
    public:
	CATDeclareInterface; 

    /**
     * Called before any of the given object's descendents are called.
     */
	virtual void 
	PreOrderBuild( const CATBaseUnknown_var father, 
                   DNBBuildType buildType=DefaultBuild ) =0;

    /**
     * Called after all of the given object's descendents have been called.
     */
	virtual void 
	PostOrderBuild( const CATBaseUnknown_var father, 
                    DNBBuildType buildType=DefaultBuild ) =0;
};

//------------------------------------------------------------------------------

CATDeclareHandler( DNBIBuild, CATBaseUnknown );

#endif // DNBI_BUILD_H
