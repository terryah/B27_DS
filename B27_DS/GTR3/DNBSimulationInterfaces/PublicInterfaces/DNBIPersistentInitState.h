/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBIPersistentInitState.h
//      Define the DNBIPersistentInitState interface.
//
//==============================================================================
//
// Usage notes: 
//      This interface will save/restore the start condition of all the items 
//      that adhere to the interface DNBIItemPersistentInitState.
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     hrn          11/01/1999   Initial implementation.
//     mmh          05/24/2001   Remove the argument from RemoveStartCondition.
//     mmh          07/11/2001   Add 2 methods: save/restore for a list of items
//     sha          09/28/2001   Add 3 methods: register/unregister items
//
//==============================================================================

#ifndef DNBIPersistentInitState_H
#define DNBIPersistentInitState_H

#include "CATBaseUnknown.h"
#include "CATBooleanDef.h"
#include "DNBSimulationItfCPP.h"
#include "DNBIItemPersistentInitState.h"

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByDNBSimulationItfCPP IID IID_DNBIPersistentInitState ;
#else
extern "C" const IID IID_DNBIPersistentInitState ;
#endif

class CATISpecObject;
class CATILinkableObject;
class CATListValCATBaseUnknown_var;

//------------------------------------------------------------------------------

/**
 * This interface will save/restore the start condition of all the items that 
 * adhere to the interface DNBIItemPersistentInitState.
 */
class ExportedByDNBSimulationItfCPP DNBIPersistentInitState: 
        public CATBaseUnknown
{
    CATDeclareInterface;
    
public:
    
    /**
     * This interface will save the start condition of all the items
     * that adhere to the interface DNBIItemPersistentInitState
     */
    virtual void SaveStartCondition( 
            const boolean refresh = FALSE, 
            const int mask = DNB_ALL_INIT_STATE ) = 0;
    
    /**
     * This interface will restore the start condition for all the
     * items for which the start condition has been saved.
     */
    virtual void RestoreStartCondition( 
            const int mask = DNB_ALL_INIT_STATE,
            const DNBRestoreDestination where = DNBRestoreToWDM ) = 0;
    
    /**
     * This interface will update the Start condition and remove
     * references to redundant items.
     */
    virtual void UpdateStartCondition( 
            const int mask = DNB_ALL_INIT_STATE ) = 0;
    
    /**
     * This interface saves the start condition of the selected
     * item if it adheres to the DNBIItemPersistentInitState interface.
     */
    virtual void SaveItemStartCondition( 
            const CATBaseUnknown_var &item,
            const boolean refresh = FALSE,
            const int mask = DNB_ALL_INIT_STATE ) = 0;
    
    /**
     * This function restores the start condition for the selected
     * item if it has been saved.
     */
    virtual void RestoreItemStartCondition( 
            const CATBaseUnknown_var &item,
            const int mask = DNB_ALL_INIT_STATE,
            const DNBRestoreDestination where = DNBRestoreToWDM ) = 0;
    
    /**
     * This method saves the start condition for each item in the given list,
     * if it adheres to the DNBIItemPersistentInitState interface.
     */
    virtual void SaveListStartCondition( 
            const CATListValCATBaseUnknown_var *List,
            const boolean refresh = FALSE,
            const int mask = DNB_ALL_INIT_STATE ) = 0;
    
    /**
     * This method restores the start condition for each item in the given
     * list, if it has been saved.
     */
    virtual void RestoreListStartCondition( 
            const CATListValCATBaseUnknown_var *List,
            const int mask = DNB_ALL_INIT_STATE,
            const DNBRestoreDestination where = DNBRestoreToWDM ) = 0;
    
    /**
     * Adds a persistent state object for the given object.
     */
    virtual CATISpecObject *AddInitStateObject(
            CATILinkableObject *itemLinkObj )= 0;
    
    /**
     * Removes the start condition for the given object.
     */
    virtual void RemoveStartCondition() = 0;

    /**
     * Returns the state of the simulation state condition.
     */
    virtual boolean HasInitialState() = 0;

    /**
     * Registers an item for each save/restore initial state is desired.
     */
    virtual HRESULT RegisterItem( const CATBaseUnknown_var &item ) = 0;
    /**
     * Unegisters the given item.
     */
    virtual HRESULT UnregisterItem( const CATBaseUnknown_var &item ) = 0;
    /**
     * Unegisters all items.
     */
    virtual HRESULT UnregisterItems() = 0;

    // No constructors or destructors on this pure virtual base class
    // --------------------------------------------------------------
};

//------------------------------------------------------------------------------

CATDeclareHandler( DNBIPersistentInitState, CATBaseUnknown );

#endif //DNBIPersistentInitState_H
