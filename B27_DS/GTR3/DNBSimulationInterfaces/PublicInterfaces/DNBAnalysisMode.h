/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2000
//==============================================================================
//
// DNBAnalysisMode.h
//   Define the DNBAnalysisMode.h
//
//==============================================================================
//
// Usage notes: 
//   DNBAnalysisMode is used to specify the Clash Analysis Modes.
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     mmr          01/01/2000   Initial implementation
//     mmh          06/17/2002   Add values for AnalysisLevel
//     mmh          08/25/2004   Add values for DNBICustomAnlConfigDlg
//
//==============================================================================
#ifndef DNBAnalysisMode_H
#define DNBAnalysisMode_H

/**
 * Analysis Configuration  mode.
 * @param AnlOff
 *   The Anlysis mode is OFF .
 *
 * @param AnlOn
 *   The Analysis Mode is ON.
 *
 */
enum DNBAnalysisMode
{ 
   AnlOff=0,
   AnlOn=1
};

/**
 * Analysis Level.
 * @param AnOff
 *   The Analysis Level is OFF .
 *
 * @param AnHighlight
 *   The Analysis Level is Highlight (highlight is used to notify the user).
 *
 * @param AnVerbose
 *   The Analysis Level is Verbose (highlight and messages are used to notify
 *   the user).
 *
 * @param AnInterrupt
 *   The Analysis Level is Interrupt (highlight and messages are used to notify
 *   the user and the simulation will be stopped).
 *
 */

#define DNB_ANALYSIS_OFF            0
#define DNB_ANALYSIS_HIGHLIGHT      1
#define DNB_ANALYSIS_VERBOSE        2
#define DNB_ANALYSIS_INTERRUPT      3

#define DNB_ANALYSIS_SCOPE_GLOBAL   0
#define DNB_ANALYSIS_SCOPE_LOCAL    1
#define DNB_ANALYSIS_SCOPE_ALL      2

//enum DNBAnalysisLevel
//{
//    AnOff=0,
//    AnHighlight=1,
//    AnVerbose=2,
//    AnInterrupt=3
//};

/**
 * Values used to customize tab-pages displayed by AnalysisConfiguration dialog.
 */

#define DNB_AC_ANL_PAGE    0x00000001
#define DNB_AC_DEV_PAGE    0x00000002
#define DNB_AC_ROB_PAGE    0x00000004
#define DNB_AC_IO_PAGE     0x00000008
#define DNB_AC_ALL_PAGE    0xffffffff

#endif //DNBAnalysisMode_H

