# COPYRIGHT DASSAULT SYSTEMES 2004
#======================================================================
# Imakefile for module CAT3DXmlParser.m
#======================================================================
#
#  Aug 2004  Creation: Code generated by the CAA wizard  rnh
#  11/01/2005; Claude Guinamard : Desactivation temporaire de la fonctionalite
#                                 "CGR dans le ventre" sur plateformes 64 bits
#======================================================================
#
# SHARED LIBRARY 
#
BUILT_OBJECT_TYPE=SHARED LIBRARY 
 
COMON_LINK_WITH  = JS0GROUP \
                   CATLightXml \
                   JS0ZLIB \
                   CATViz \
                   CAT3DXmlLightBaseServices \
                   CAT3DXmlInterfaces  
                                    
# System dependant variables
#
OS = AIX
LINK_WITH = $(COMON_LINK_WITH) 
#
OS = aix_a64
LINK_WITH = $(COMON_LINK_WITH)
#
OS = HP-UX
LINK_WITH = $(COMON_LINK_WITH) 
#
OS  =  IRIX
#BUILT_OBJECT_TYPE=NONE
LINK_WITH = $(COMON_LINK_WITH) 
LOCAL_CCFLAGS=-DLT_CPU_SPARC  -DLT_OS_IRIX  -DLT_V5_XVL  -DLT_V5_CAA  $(NOT_MKMK_DEBUG:+"-DNDEBUG")
CXX_EXCEPTION=
#
OS  =  SunOS
#BUILT_OBJECT_TYPE=NONE
LINK_WITH = $(COMON_LINK_WITH) 
LOCAL_CCFLAGS=-DLT_CPU_SPARC  -DLT_OS_SOLARIS  -DLT_V5_XVL  -DLT_V5_CAA  $(NOT_MKMK_DEBUG:+"-DNDEBUG")
CXX_EXCEPTION=
#
OS = Windows_NT
LINK_WITH = $(COMON_LINK_WITH) 
LOCAL_CCFLAGS = /D "LT_CPU_PENTIUM" /D "LT_OS_WINDOWS" \
		/D "LT_V5_XVL" /D "LT_V5_CAA" \
		$(NOT_MKMK_DEBUG:+"/D NDEBUG")
CXX_EXCEPTION=
#
OS = intel_a64
LINK_WITH = $(COMON_LINK_WITH)
