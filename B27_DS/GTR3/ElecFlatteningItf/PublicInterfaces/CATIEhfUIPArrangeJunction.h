// COPYRIGHT Dassault Systemes 2006
//===================================================================
//
// CATIEhfUIPArrangeJunction.h
// Define the CATIEhfUIPArrangeJunction interface
//
//===================================================================
//
// Usage notes:
//   New interface: describe its use here
//
//===================================================================
//
//  Feb 2006  Creation: Code generated by the CAA wizard  AKL
//===================================================================

//Specify CAA Level and CAA Usage for interface
/**
  * @CAA2Level L1
  * @CAA2Usage U5
  */

#ifndef CATIEhfUIPArrangeJunction_h
#define CATIEhfUIPArrangeJunction_h

#include "ExportedByCATEhfInterfaces.h"
#include "CATBaseUnknown.h"

// --- ObjectModelerBase Framework ---
#include "sequence_CATBaseUnknown_ptr.h"

// --- Mathematics Framework ---
#include "CATLISTP_CATMathVector.h"

class CATMathPlane ;

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByCATEhfInterfaces IID IID_CATIEhfUIPArrangeJunction;
#else
extern "C" const IID IID_CATIEhfUIPArrangeJunction ;
#endif

//------------------------------------------------------------------

/**
 * This Interface CATIEhfUIPArrangeJunction allows �Arrange Junction � User Pattern� command to be implemented by user for the definition of his own branch arrangement rules.
 * Interface must be implemented on component CAAEhfArrangeJunction by extending the component.
 */

class ExportedByCATEhfInterfaces CATIEhfUIPArrangeJunction: public CATBaseUnknown
{
  CATDeclareInterface;

  public:

    /**
     *       
     *   User has to provide the implementation for this method when he implements this interface on CAAEhfArrangeJunction component.
     *   This method takes input as connected bundle segment extremities at the junction and tangent of bundle segment selected to remain fixed by user.
     *   The input bundle segment extremities list is ordered in clock-wise angular fashion with respect to the normal to Flatten Plane selected in Flattening Parameters.
     *   In case of user has selected fixed bundle segment having a branch point as the selected junction, tangents of both bundle segments of the branch will be input.
     *   Please refer PES of "Arrange Junction" command for further details.
     *   Location of header files for argument types in frameworks:
     *   For CATListPtrCATBaseUnknown type: Application\PublicInterfaces.
     *   For CATMathPlane, CATMathVector and CATLISTP_CATMathVector types: Mathematics\PublicInterfaces.
     *   @param iListOfBundleSegmentExtremity
     *      (Input): List of CATBaseUnknown pointers of bundle segment extremities at the selected junction
     *   @param iFlatteningPlane
     *      (Input): Active plane in Flattening Parameters. User is supposed to have flattened the model on this plane. Programmer can use this plane to calculate the rotated vectors
     *   @param ioListOfDirections
     *      (Input and Output): List of CATMathVector pointers. If the vector is valuated in Input, then it is considered as the main direction. For other vector pointers, the implementation must put a value as Output
     *   @return
     *   An HRESULT value
     *   <br><b>Legal values</b>:
     *   <dl>
     *     <dt>S_OK</dt>
     *     <dd>Method is successful</dd>
     *     <dt>E_FAIL </dt>
     *     <dd>Method Failed</dd>
     *   </dl>
     * 
     */
  virtual HRESULT RetrieveUserDirection(const CATListPtrCATBaseUnknown & iListOfBundleSegmentExtremity, 
                                        const CATMathPlane             & iFlatteningPlane,
                                        CATLISTP_CATMathVector         & ioListOfDirections) = 0 ;

  // No constructors or destructors on this pure virtual base class
  // --------------------------------------------------------------
};

//------------------------------------------------------------------

#endif


