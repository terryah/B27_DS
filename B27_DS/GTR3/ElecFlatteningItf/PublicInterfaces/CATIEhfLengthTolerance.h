// COPYRIGHT Dassault Systemes 2006
//===================================================================
//
// CATIEhfLengthTolerance.h
// Define the CATIEhfLengthTolerance interface
//
//===================================================================
//
// Usage notes:
//   New interface: describe its use here
//
//===================================================================
//
//  Nov 2006  Creation: Code generated by the CAA wizard  DJH
//===================================================================

//Specify CAA Level and CAA Usage for interface
/**
  * @CAA2Level L1
  * @CAA2Usage U3
  */

#ifndef CATIEhfLengthTolerance_h
#define CATIEhfLengthTolerance_h

#include "ExportedByCATEhfInterfaces.h"
#include "CATBaseUnknown.h"

class CATUnicodeString ;
class CATICkeInst ;
class CATIProduct ;



#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByCATEhfInterfaces IID IID_CATIEhfLengthTolerance;
#else
extern "C" const IID IID_CATIEhfLengthTolerance;
#endif

//------------------------------------------------------------------

/**
 * This Interface CATIEhfLengthTolerance allows to manage the Length Tolerance object.
 * Interface CATIEhfLengthTolerance is implemented on the Length Tolerance feature and should be used by user.
 */

class ExportedByCATEhfInterfaces CATIEhfLengthTolerance : public CATBaseUnknown
{
  CATDeclareInterface;

  public:

/**
 * Retrieves the name of the Length Tolerance.
 * @param oLengthToleranceName
 *     (Output): CATUnicodeString which contains the Name of Length Tolerance.
 * @return
 *   An HRESULT value
 *   <br><b>Legal values</b>:
 *   <dl>
 *     <dt>S_OK</dt>
 *     <dd>The attribute is successfully retrieved</dd>
 *     <dt>E_FAIL</dt>
 *     <dd>An error occurred while retrieving the attribute</dd>
 *   </dl>
 */
  
  virtual HRESULT GetName( CATUnicodeString & oLengthToleranceName ) = 0;

/**
 * Retrieves the Fix extremity of the Length Tolerance.
 * @param opIUnkFixExtremity
 *     (Output): Pointer on the Fix extremity of the Length Tolerance.
 * @return
 *   An HRESULT value
 *   <br><b>Legal values</b>:
 *   <dl>
 *     <dt>S_OK</dt>
 *     <dd>The attribute is successfully retrieved</dd>
 *     <dt>E_FAIL</dt>
 *     <dd>An error occurred while retrieving the attribute</dd>
 *   </dl>
 */
  virtual HRESULT GetFixExtremity( CATBaseUnknown *& opIUnkFixExtremity ) = 0;

/**
 * Retrieves the Move extremity of the Length Tolerance.
 * @param opIUnkMoveExtremity
 *     (Output): Pointer on the Move extremity of the Length Tolerance.
 * @return
 *   An HRESULT value
 *   <br><b>Legal values</b>:
 *   <dl>
 *     <dt>S_OK</dt>
 *     <dd>The attribute is successfully retrieved</dd>
 *     <dt>E_FAIL</dt>
 *     <dd>An error occurred while retrieving the attribute</dd>
 *   </dl>
 */

  virtual HRESULT GetMoveExtremity( CATBaseUnknown *& opIUnkMoveExtremity ) = 0;

/**
 * Retrieves the value of applied Length Tolerance.
 * @param opICkeInstToleranceValue
 *     (Output): Pointer on CATICkeInst which contains the value of the Length Tolerance applied between the two extremities.
 * @return
 *   An HRESULT value
 *   <br><b>Legal values</b>:
 *   <dl>
 *     <dt>S_OK</dt>
 *     <dd>The attribute is successfully retrieved</dd>
 *     <dt>E_FAIL</dt>
 *     <dd>An error occurred while retrieving the attribute</dd>
 *   </dl>
 */

  virtual HRESULT GetToleranceValue( CATICkeInst *& opICkeInstToleranceValue ) = 0;

/**
 * Retrieves the length of the segment portion where the Length Tolerance was applied.
 * @param opICkeInstPortionLength
 *     (Output): Pointer on CATICkeInst which contains the curvilinear length between the two extremities.
 * @return
 *   An HRESULT value
 *   <br><b>Legal values</b>:
 *   <dl>
 *     <dt>S_OK</dt>
 *     <dd>The attribute is successfully retrieved</dd>
 *     <dt>E_FAIL</dt>
 *     <dd>An error occurred while retrieving the attribute</dd>
 *   </dl>
 */

  virtual HRESULT GetPortionLength( CATICkeInst *& opICkeInstPortionLength ) = 0;

  /**
 * to set the value of Length Tolerance.
 * @param ipICkeInstToleranceValue
 *     (Input): Pointer on CATICkeInst which contains the value of the Length Tolerance to be applied between the two defined extremities
 * @param ipIProdRoot
 *     (Input): Pointer on the "Root" Product of Flattened document.
 * @return
 *   An HRESULT value
 *   <br><b>Legal values</b>:
 *   <dl>
 *     <dt>S_OK</dt>
 *     <dd>The attribute is successfully retrieved</dd>
 *     <dt>E_FAIL</dt>
 *     <dd>An error occurred while retrieving the attribute</dd>
 *   </dl>
 */

  virtual HRESULT SetToleranceValue( const CATICkeInst * ipICkeInstToleranceValue,
                                     CATIProduct       * ipIProdRoot ) = 0;
  // No constructors or destructors on this pure virtual base class
  // --------------------------------------------------------------
};

//------------------------------------------------------------------

#endif

