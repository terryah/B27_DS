// COPYRIGHT Dassault Systemes 2003

/** 
 * @CAA2Level L1
 * @CAA2Usage U3
 */

//===================================================================
//
// CATIEhfEnvironment.h
// Define the CATIEhfEnvironment interface
//
//===================================================================
//
// Usage notes:
//   New interface: To allow the user to create and retrieve 
//   the flattening parameters in/from the Product.
//
//===================================================================
//
//  Aug 2003  Creation: Code generated by the CAA wizard  suw
//===================================================================
#ifndef CATIEhfEnvironment_H
#define CATIEhfEnvironment_H

#include "CATBaseUnknown.h"
#include "ExportedByCATEhfInterfaces.h"

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByCATEhfInterfaces IID IID_CATIEhfEnvironment;
#else
extern "C" const IID IID_CATIEhfEnvironment;
#endif

//------------------------------------------------------------------

/**
 * This Interface allows you to find the Flattening Parameters
 * used in " Electrical Harness Flattening " WorkBench.
 */

class CATListValCATBaseUnknown_var;
class CATIEhfFlatteningParameters;

class ExportedByCATEhfInterfaces CATIEhfEnvironment: public CATBaseUnknown
{
  CATDeclareInterface;

  public:


/**
 * Creates the flattening parameters.
 * @param opIFlatteningParameters
 *     Pointer to created flattening parameters
 * @return
 *   An HRESULT value
 *   <br><b>Legal values</b>:
 *   <dl>
 *     <dt>S_OK</dt>
 *     <dd>Flattening parameters are successfully created</dd>
 *     <dt>E_FAIL </dt>
 *     <dd>An error occurred while creating flattening parameters</dd>
 *   </dl>
 */
        
  virtual HRESULT CreateFlatteningParameters ( CATIEhfFlatteningParameters*& opIFlatteningParameters ) = 0;


/**
 * Retrieves the flattening parameters stored inside the product.
 * @param oListParameters [out]
 *     The list of flattening parameters
 * @return
 *   An HRESULT value
 *   <br><b>Legal values</b>:
 *   <dl>
 *     <dt>S_OK</dt>
 *     <dd>Flattening parameters are successfully retrieved</dd>
 *     <dt>E_FAIL </dt>
 *     <dd>An error occurred while searching flattening parameters</dd>
 *   </dl>
 */
        
  virtual HRESULT GetFlatteningParameters ( CATListValCATBaseUnknown_var** oListParameters ) = 0;


  // No constructors or destructors on this pure virtual base class
  // --------------------------------------------------------------
};

//------------------------------------------------------------------

#endif

