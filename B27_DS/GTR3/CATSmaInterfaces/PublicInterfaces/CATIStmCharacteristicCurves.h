#ifndef CATIStmCharacteristicCurves_H
#define CATIStmCharacteristicCurves_H

// COPYRIGHT DASSAULT SYSTEMES  1999

/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */

#include "CATBaseUnknown.h"
#include "ExportedByCATSmaInterfaces.h"
#include "CATListOfCATUnicodeString.h"

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByCATSmaInterfaces IID IID_CATIStmCharacteristicCurves;
#else
extern "C" const IID IID_CATIStmCharacteristicCurves;
#endif

class CATBody;
class CATUnicodeString;

/**
 * Interface to retrieve an aerospace sheet metal <b>characteristic curve</b> body.
 * <b>Role</b>: Provides the basic methods to access data of a sheet metal
 * <b>characteristic curve</b> body.
 *
 * <br>A <b>characteristic curve</b> can be accessed by :
 *      <ul>
 *      <li> <b>View</b> : <b>MfDefault3DView</b> or <b>MfUnfoldedView</b>,</li>
 *      <li> <b>Type</b> : the <b>characteristic curve</b> type</li>
 *      </ul>
 */
class ExportedByCATSmaInterfaces CATIStmCharacteristicCurves : public CATBaseUnknown
  { 
  CATDeclareInterface;
	
  public: 
    
    /**
     *  <br><b>Role</b>: This method retrieves all the <b>characteristic curve</b> types available for <tt>this</tt> feature.
     *
     *  @param  iView
     *    A reference on <tt>CATUnicodeString</tt> instance.
     *      <br><b>Legal values</b>: 
     *      <ul>
     *      <li><b>"MfDefault3DView"</b>: for the default 3D view,</li>
     *      <li><b>"MfUnfoldedView"</b> : for an unfolded view.</li>
     *      </ul>
     *  @param  oListCurveTypes
     *    The list of the available <b>characteristic curve</b> types.
     *      <br><b>Legal values</b>: 
     *      <ul>
     *      <li><b>"OML"</b>:  for the outer mold line,</li>
     *      <li><b>"OML2"</b>: for the second outer mold line,</li>
     *      <li><b>"IML"</b>:  for inner mold line,</li>
     *      <li><b>"BTLF"</b>: for the bend tangent line belonging to this feature,</li>
     *      <li><b>"BTLB"</b>: for the bend tangent line belonging to this base feature.</li>
	 *      <li><b>"CLB"</b>: for the bend tangent line belonging to this base feature.</li>
     *      </ul>
     */
     virtual HRESULT __stdcall GetAvailableCharacteristicCurveTypes (const CATUnicodeString &    iView,
                                                                     CATListOfCATUnicodeString & oListCurveTypes) const = 0;
    
    /**
     *  Retrieves a characteristic curve associated to <tt>this</tt> feature.
     *    @param  iView
     *      A reference on <tt>CATUnicodeString</tt> instance.
     *      <br><b>Legal values</b>: 
     *      <ul>
     *      <li><b>"MfDefault3DView"</b>: for the default 3D view,</li>
     *      <li><b>"MfUnfoldedView"</b> : for an unfolded view.</li>
     *      </ul>
     *    @param  iIdentifier
     *      A reference on <tt>CATUnicodeString</tt> instance.
     *      <br><b>Legal values</b>: 
     *      <ul>
     *      <li><b>"OML"</b>:  for the outer mold line,</li>
     *      <li><b>"OML2"</b>: for the second outer mold line (in unfolded view only),</li>
     *      <li><b>"IML"</b>:  for inner mold line,</li>
     *      <li><b>"BTLF"</b>: for the bend tangent line belonging to this feature,</li>
     *      <li><b>"BTLB"</b>: for the bend tangent line belonging to this base feature.</li>
	 *      <li><b>"CLB"</b>: for the bend tangent line belonging to this base feature.</li>
     *      </ul>
     *    @param  opiCharacteristicCurve
     *      An interface pointer to the topological body including the characteristic curve.
     *    @return <tt>S_OK</tt>.
     *      The topological body is successfully created.
     *    @return <tt>E_FAIL</tt>.
     *      The topological body cannot be created.. Use CATError::CATGetLastError()
     *      to retrieve the error by the <tt>HRESULT</tt> value. @see CATError.
     */
    virtual HRESULT __stdcall GetCharacteristicCurve (const CATUnicodeString & iView,
                		                                  const CATUnicodeString & iIdentifier,
											                                CATBody **               opiCharacteristicCurve) const = 0;
 
  };

CATDeclareHandler( CATIStmCharacteristicCurves, CATBaseUnknown );

#endif

