/* -*-c++-*- */
#ifndef CATIStmJoggle_H
#define CATIStmJoggle_H

// COPYRIGHT DASSAULT SYSTEMES  1999

/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */ 
#include "CATBaseUnknown.h"
#include "CATISpecObject.h"
#include "ExportedByCATSmaInterfaces.h"
#include "CATTopDefine.h"
#include "CATICkeParm.h"

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByCATSmaInterfaces IID IID_CATIStmJoggle;
#else
extern "C" const IID IID_CATIStmJoggle;
#endif

/**
 * Interface to manage the aerospace sheet metal <b>joggle</b> feature.
 * <b>Role</b>: Provides the basic methods to access data of a sheet metal
 * <b>joggle</b> feature.
 *
 * <br>A <b>joggle</b> is a local deformation of a <b>surfacic flange</b> or a <b>Web</b>.
 *      <br><b>A <b>joggle</b> is defined by</b> : 
 *      <ul>
 *      <li> <b>support</b>      : a <b>surfacic flange</b> or the <b>web</b>,</li>
 *      <li> <b>plane</b>        : a <b>CATPlane</b> or a planar face,</li>
 *      <li> <b>depth</b>        : the offset value of the joggle support surface,</li>
 *      <li> <b>runout</b>       : the length of the joggle,</li>
 *      <li> <b>start radius</b> : the value of the start fillet radius of the joggle,</li>
 *      <li> <b>end radius</b>   : the value of the end fillet radius of the joggle.</li>
 *      </ul>
 */

class ExportedByCATSmaInterfaces CATIStmJoggle : public CATBaseUnknown
  {
	CATDeclareInterface;
	
  public: 
    
    /**
     * <br><b>Role</b>: This method retrieves the support of the joggle.
     *
     * @param opiSupportSpec 
     *   Specification of the support : a surfacic flange or a Web.
     *
     */ 
    virtual HRESULT __stdcall GetSupport (CATISpecObject ** opiSupportSpec) = 0;
    
    /**
     * <br><b>Role</b>: This method sets the support of the joggle.
     *
     * @param ipiSupportSpec 
     *   Specification of the support : a surfacic flange or a Web.
     *
     */ 
    virtual HRESULT __stdcall SetSupport (const CATISpecObject * ipiSupportSpec) = 0;
    
    /**
     * <b>Role</b>: Method to manage Joggle internal specifications.
     *
     *              A call to this method is mandatory BEFORE updating the Joggle.
     *                 - if the Support has been modified : 
     *                   the Joggle will be removed from its Previous Support (if any).
     *                 - if the Joggle Plane has been modified : 
     *                   the Joggle will be inserted on the Support according with other Joggles (if any).
     *                 - a new OFFSET Surface of the Support will be created if necessary.
     *
     * @param ipiPrtPartSpec 
     *   Part Feature
     * @param ipiPrevSupportSpec 
     *   Suppport of the Joggle BEFORE any Joggle modification ( is NULL if NEW Joggle ).
     */ 
    virtual HRESULT __stdcall ManageOnSupport (const CATISpecObject * ipiPrtPartSpec,
                                               const CATISpecObject * ipiPrevSupportSpec) = 0;
    
    /**
     * <br><b>Role</b>: This method retrieves the start plane of the joggle.
     *
     * @param opiPlaneSpec 
     *   a plane or a planar face.
     *
     */ 
    virtual HRESULT __stdcall GetPlane (CATISpecObject ** opiPlaneSpec) = 0;    
       
    /**
     * <br><b>Role</b>: This method sets the start plane of the joggle.
     *
     * @param ipiPlaneSpec 
     *   a plane or a planar face.
     *
     */ 
    virtual HRESULT __stdcall SetPlane (const CATISpecObject * ipiPlaneSpec) = 0;

    /**
     * <br><b>Role</b>: This method retrieves the runout definition type of the joggle.
     *                  This definition is set by the <b>sheet metal parameters</b> for all the
     *                  joggles created in the current Part.
     *
     * @param RunoutType
     *   an integer 
     *      <br><b>Legal values</b>: 
     *      <ul>
     *      <li> <b>1</b> : the runout length includes the joggle fillets,</li>
     *      <li> <b>2</b> : the runout length excludes partially the joggle fillets.</li>
     *      </ul>
     */ 
    virtual HRESULT __stdcall GetRunoutType (int & RunoutType) = 0;    
    
    /**
     * <br><b>Role</b>: This method retrieves the runout length parameter of the joggle.
     *
     * @param Runout 
     *   a CATICkeParm : use the method <b>Valuate</b> to modify the runout length. @see CATICkeParm.
     */ 
    virtual HRESULT __stdcall GetRunout (CATICkeParm ** opiRunoutParam) = 0;      
    
    /**
     * <br><b>Role</b>: This method retrieves the relative orientation of the runout compared to the joggle plane.
     * @param oRunoutOrient 
     *      A reference to a <b>CATOrientation</b> instance.
     *      <br><b>Legal values</b>: 
     *      <ul>
     *      <li> <b>CATOrientationNegative</b>: The orientations are opposite,</li>
     *      <li> <b>CATOrientationPositive</b>: The orientations are the same,</li>
     *      <li> <b>CATOrientationUnknown</b> : The orientation is unknown.</li>
     *      </ul>
     */
    virtual HRESULT __stdcall GetRunoutOrient (CATOrientation & oRunoutOrient) = 0;
    
    /**
     * <br><b>Role</b>: This method sets the relative orientation of the runout compared to the joggle plane.
     * @param iRunoutOrient 
     *      A reference to a <b>CATOrientation</b> instance.
     *      <br><b>Legal values</b>: 
     *      <ul>
     *      <li> <b>CATOrientationNegative</b>: The orientations are opposite,</li>
     *      <li> <b>CATOrientationPositive</b>: The orientations are the same,</li>
     *      <li> <b>CATOrientationUnknown</b> : The orientation is unknown.</li>
     *      </ul>
     */
    virtual HRESULT __stdcall SetRunoutOrient (const CATOrientation & iRunoutOrient) = 0;

    /**
     * <br><b>Role</b>: This method retrieves the depth length parameter of to the joggle.
     *
     * @param opiDepth 
     *   a CATICkeParm : use the method <b>Valuate</b> to modify the depth length. @see CATICkeParm.
     */ 
    virtual HRESULT __stdcall GetDepth (CATICkeParm ** opiDepthParam) = 0;
    
    /**
     * <br><b>Role</b>: This method retrieves the relative orientation of the depth compared to the joggle support.
     * @param oDepthOrient 
     *      A reference to a <b>CATOrientation</b> instance.
     *      <br><b>Legal values</b>: 
     *      <ul>
     *      <li> <b>CATOrientationNegative</b>: The orientations are opposite,</li>
     *      <li> <b>CATOrientationPositive</b>: The orientations are the same,</li>
     *      <li> <b>CATOrientationUnknown</b> : The orientation is unknown.</li>
     *      </ul>
     */
    virtual HRESULT __stdcall GetDepthOrient (CATOrientation & oDepthOrient) = 0;
    
    /**
     * <br><b>Role</b>: This method sets the relative orientation of the depth compared to the joggle support.
     * @param iDepthOrient 
     *      A reference to a <b>CATOrientation</b> instance.
     *      <br><b>Legal values</b>: 
     *      <ul>
     *      <li> <b>CATOrientationNegative</b>: The orientations are opposite,</li>
     *      <li> <b>CATOrientationPositive</b>: The orientations are the same,</li>
     *      <li> <b>CATOrientationUnknown</b> : The orientation is unknown.</li>
     *      </ul>
     */
    virtual HRESULT __stdcall SetDepthOrient (const CATOrientation & iDepthOrient) = 0;

    /**
     * <br><b>Role</b>: This method retrieves the radius of the start fillet of the Joggle.
     *
     * @param opiRadiusParam 
     *   a CATICkeParm : use the method <b>Valuate</b> to modify the radius length. @see CATICkeParm.
     */ 
    virtual HRESULT __stdcall GetStartRadius (CATICkeParm ** opiRadiusParam) = 0;

    /**
     * <br><b>Role</b>: This method retrieves the radius of the end fillet of the Joggle.
     *
     * @param opiRadiusParam 
     *   a CATICkeParm : use the method <b>Valuate</b> to modify the radius length. @see CATICkeParm.
     */ 
    virtual HRESULT __stdcall GetEndRadius (CATICkeParm ** opiRadiusParam) = 0;

    /**
     * <br><b>Role</b>: This method retrieves the clearance length of the Joggle.
     *
     * @param opiClearanceParam 
     *   a CATICkeParm : use the method <b>Valuate</b> to modify the Clearance length. @see CATICkeParm.
     */ 
    virtual HRESULT __stdcall GetClearance (CATICkeParm ** opiClearanceParam) = 0;

  };

CATDeclareHandler(CATIStmJoggle, CATBaseUnknown);

#endif
