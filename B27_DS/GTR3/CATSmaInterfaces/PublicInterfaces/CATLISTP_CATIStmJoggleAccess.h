// COPYRIGHT Dassault Systemes 2006

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

#ifndef CATLISTP_CATIStmJoggleAccess_H
#define CATLISTP_CATIStmJoggleAccess_H

/**
 * <br><b>Role</b>: Collection class for pointers to CATIStmJoggleAccess objects.
 */
#include "CATLISTP_Clean.h"
#include "CATLISTP_PublicInterface.h"

#include "ExportedByCATSmaInterfaces.h"

#undef	CATCOLLEC_ExportedBy
#define CATCOLLEC_ExportedBy ExportedByCATSmaInterfaces

class CATIStmJoggleAccess;

#include "CATLISTP_Declare.h"
CATLISTP_DECLARE(CATIStmJoggleAccess);

#endif


