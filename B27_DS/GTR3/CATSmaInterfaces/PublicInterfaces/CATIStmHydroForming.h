#ifndef CATIStmHydroForming_H
#define CATIStmHydroForming_H

// COPYRIGHT Dassault Systemes 2006

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

#include "ExportedByCATSmaInterfaces.h"
#include "CATIStmTechnologicalProcess.h"
#include "CATLISTP_CATIStmJoggleAccess.h"

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByCATSmaInterfaces IID IID_CATIStmHydroForming;
#else
extern "C" const IID IID_CATIStmHydroForming ;
#endif

class CATUnicodeString;

/**
 * <br><b>Role</b>: The HydroForming technological process extracts the following data 
 *                  included in a CATPart :
 *
 *      <br><b>Data extracted</b>: 
 *      <ul>
 *      <li> <b>Bending Faces</b>,</li>
 *      <li> <b>Joggle Runout Faces</b>,</li>
 *      <li> <b>UnJoggled Faces</b>,</li>
 *      </ul>
 */
class ExportedByCATSmaInterfaces CATIStmHydroForming : public CATIStmTechnologicalProcess
  { 
  CATDeclareInterface;

  public:

    /**
     * <br><b>Role</b>: This method retrieves the Bending faces associated to this Hydro Forming process.
     *
     * @param iTypeView
     *      A reference on <tt>CATUnicodeString</tt> instance.
     *      <br><b>Legal values</b>: 
     *      <ul>
     *      <li><b>"MfDefault3DView"</b>: the faces extracted from the default 3D view,</li>
     *      <li><b>"MfUnfoldedView"</b>: the faces extracted from the unfolded view,</li>
     *      </ul>
     * @param   iExtractSkinType
     *   an integer 
     *      <br><b>Legal values</b>: 
     *      <ul>
     *      <li> <b>1</b> : the faces extracted from the INTERNAL skin (related to the Bending orientation),</li>
     *      <li> <b>2</b> : the faces extracted from the EXTERNAL skin (related to the Bending orientation).</li>
     *      </ul>
     * @return <tt>S_OK</tt>.
     *      The list of cells is successfully valuated.
     * @return <tt>E_FAIL</tt>.
     *      The list of cells is empty.
     */
    virtual HRESULT __stdcall GetBendingFaces (const CATUnicodeString & iTypeView,
                                               const int &              iExtractSkinOption, 
                                               CATLISTP(CATCell) &      oCells) const = 0;
    /**
     * <br><b>Role</b>: This method retrieves the list of joggles associated to this Hydro Forming process.
     *
     * Call the <b>CATIStmJoggleAccess</b> interface methods on each member of the list to retrieve the Joggle data.
     *
	   * @param   olJoggles
     *      The list of pointers on CATIStmJoggleAccess.
     * @return <tt>S_OK</tt>.
     *      The list of Joggles is successfully valuated.
     * @return <tt>E_FAIL</tt>.
     *      The list of Joggles is empty.
     */
    virtual HRESULT __stdcall GetJoggles (CATLISTP(CATIStmJoggleAccess) & olJoggles) const = 0;    
    
    /**
     * <br><b>Role</b>: This method retrieves the Support faces associated to this Hydro Forming process.
     *                  Support faces include all faces except Bending Faces and Joggle Faces.
     *
     * @param iTypeView
     *      A reference on <tt>CATUnicodeString</tt> instance.
     *      <br><b>Legal values</b>: 
     *      <ul>
     *      <li><b>"MfDefault3DView"</b>: the faces extracted from the default 3D view,</li>
     *      <li><b>"MfUnfoldedView"</b>: the faces extracted from the unfolded view,</li>
     *      </ul>
     * @param   iExtractSkinType
     *   an integer 
     *      <br><b>Legal values</b>: 
     *      <ul>
     *      <li> <b>1</b> : the faces extracted from the INTERNAL skin (related to the Bending orientation),</li>
     *      <li> <b>2</b> : the faces extracted from the EXTERNAL skin (related to the Bending orientation).</li>
     *      </ul>
     * @return <tt>S_OK</tt>.
     *      The list of cells is successfully valuated.
     * @return <tt>E_FAIL</tt>.
     *      The list of cells is empty.
     */
    virtual HRESULT __stdcall GetSupportFaces (const CATUnicodeString & iTypeView,
                                               const int &              iExtractSkinOption, 
                                               CATLISTP(CATCell) &      oCells) const = 0;

  };

CATDeclareHandler(CATIStmHydroForming, CATIStmTechnologicalProcess);

#endif
