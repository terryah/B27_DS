/* -*-c++-*- */
#ifndef CATIAerospaceSheetMetalFactory_H
#define CATIAerospaceSheetMetalFactory_H
 
// COPYRIGHT DASSAULT SYSTEMES  1999

/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */
#include "CATBaseUnknown.h"
#include "CATISpecObject.h"
#include "ExportedByCATSmaInterfaces.h"

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByCATSmaInterfaces IID IID_CATIAerospaceSheetMetalFactory;
#else
extern "C" const IID IID_CATIAerospaceSheetMetalFactory;
#endif

/**
 * Interface to create Aerospace Sheet Metal Features. 
 * <b>Role</b>: Provides the public basic methods to create Aerospace Sheet Metal Features.
 */

class ExportedByCATSmaInterfaces CATIAerospaceSheetMetalFactory : public CATBaseUnknown
  { 
	CATDeclareInterface;

  public: 
    
    /**
     * <br><b>Role</b>: This method creates a Web Feature.
     *
     * @param ipiPartSpec
     *   the Part Feature in which the Web will be inserted.
     * @param opiWebSpec
     *   the created Web Feature.
     *
     */   
	   virtual HRESULT __stdcall CreateWeb (const CATISpecObject * ipiPartSpec,
                                          CATISpecObject **      opiWebSpec) = 0;
	
    /**
     * <br><b>Role</b>: This method creates a Surfacic Flange Feature.
     *
     * @param ipiPartSpec
     *   the Part Feature in which the Surfacic Flange will be inserted.
     * @param opiSurfacicFlangeSpec
     *   the created Surfacic Flange Feature.
     *
     */   
	   virtual HRESULT __stdcall CreateSurfacicFlange (const CATISpecObject * ipiPartSpec,
                                                     CATISpecObject **      opiSurfacicFlangeSpec) = 0;
  
    /**
     * <br><b>Role</b>: This method creates a Joggle Feature.
     *
     * @param ipiPartSpec
     *   the Part Feature in which the Joggle will be inserted.
     * @param opiJoggleSpec
     *   the created Joggle Feature.
     *
     */   
	   virtual HRESULT __stdcall CreateJoggle (const CATISpecObject * ipiPartSpec,
                                             CATISpecObject **      opiJoggleSpec) = 0;
  
  };

CATDeclareHandler(CATIAerospaceSheetMetalFactory, CATBaseUnknown);

#endif 

