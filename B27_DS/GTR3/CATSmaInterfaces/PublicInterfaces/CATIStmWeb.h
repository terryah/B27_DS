/* -*-c++-*- */
#ifndef CATIStmWeb_H
#define CATIStmWeb_H

// COPYRIGHT DASSAULT SYSTEMES  1999

/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */ 
#include "CATBaseUnknown.h"
#include "ExportedByCATSmaInterfaces.h"
#include "CATISpecObject.h"
#include "CATListPtrCATISpecObject.h"
#include "CATListOfInt.h"
#include "CATTopDefine.h"

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByCATSmaInterfaces IID IID_CATIStmWeb;
#else
extern "C" const IID IID_CATIStmWeb;
#endif

/**
 * Interface to manage the aerospace sheet metal <b>web</b> feature. 
 * <b>Role</b>: Provides the basic methods to access data of a sheet metal <b>web</b> feature.
 *
 *<br>A <b>web</b> is defined by :
 *      <ul>
 *      <li> <b>support</b>  : a <b>plane</b> or a planar face,</li>
 *      <li> <b>boundary</b> : one closed sketch or a set of opened sketchs or curves or surfaces.</li>
 *      </ul> 
 */

class ExportedByCATSmaInterfaces CATIStmWeb : public CATBaseUnknown
  {
	CATDeclareInterface;
	
  public: 
        
    /**
     * <br><b>Role</b>: This method retrieves the web support (designed in folded view).
     *
     * @param opSupportSpec 
     *   Specification of the support : a plane or a planar face or a closed sketch.
     */    
    virtual HRESULT __stdcall GetSupport (CATISpecObject ** opiSupportSpec) = 0;
            
    /**
     * <br><b>Role</b>: This method sets the web support (designed in folded view).
     *
     * @param ipiSupportSpec 
     *   Specification of support : a plane or a planar Face or a closed sketch.
     *                              (in case of a closed sketch the web boundary will be 
     *                               automatically filled with it).
     */    
    virtual HRESULT __stdcall SetSupport (const CATISpecObject * ipiSupportSpec) = 0;

    /**
	   * <br><b>Role</b>: This method retrieves the web material orientation.
	   *
	   * @param oMaterialOrient 
	   *   The returned web material orientation
     *      <br><b>Legal values</b>: 
     *      <ul>
     *      <li> <b>CATOrientationPositive</b>: if oriented like the support surface normale,</li>
     *      <li> <b>CATOrientationNegative</b>: else if the orientations are opposite,</li>
     *      <li> <b>CATOrientationUnknown</b> : if the computation failed.</li>
     *      </ul>
     */ 
    virtual HRESULT __stdcall GetMaterialOrient (CATOrientation & oMaterialOrient) = 0;            
    
    /**
	   * <br><b>Role</b>: This method sets the web material orientation.
	   *
	   * @param iMaterialOrient 
	   *   The web material orientation value
     *      <br><b>Legal values</b>: 
     *      <ul>
     *      <li> <b>CATOrientationPositive</b>: if oriented like the support surface normale,</li>
     *      <li> <b>CATOrientationNegative</b>: else if the orientations are opposite,</li>
     *      <li> <b>CATOrientationUnknown</b> : if the computation failed.</li>
     *      </ul>
     */ 
    virtual HRESULT __stdcall SetMaterialOrient (const CATOrientation & iMaterialOrient) = 0;

    /**
     * <br><b>Role</b>: This method retrieves the web boundary limit(s).
     *
     * @param oBoundaryElementsList 
     *   List of specifications : one closed sketch or opened sketchs or curves or surfaces.
     * @param oViewTypeList 
     *   List of int : the type of the view in which a web limit specification has been designed.
     *      <br><b>Legal values</b>: 
     *      <ul>
     *      <li> a set of <b>sketchs</b> or <b>curves</b> or <b>surfaces</b> designed in <b>folded</b> view,</li>
     *      <li> a set of <b>sketchs</b> or <b>curves</b> or <b>surfaces</b> designed in <b>unfolded</b> view (Not yet implemented).</li>
     *      </ul>
     */ 
    virtual HRESULT __stdcall GetBoundaryElements (CATLISTP(CATISpecObject) & oBoundaryElementsList,
                                                   CATListOfInt             & oViewTypeList) = 0;    
    
    /**
     * <br><b>Role</b>: This method sets the web boundary limit(s).
     *
     * @param iBoundaryElementsList 
     *   List of specifications : one closed sketch or a set of opened sketchs or curves or surfaces.
     *                            Sketchs and curves will be projected on the web support.
     *                            Surfaces will be intersected with the web support.
     *                            !!! if more than 1 limit : the limits must be set in order to define
     *                                a closed boundary on the web support.
     *                            If a previous boundary exists it will be removed 
     *                            before setting the new ones.
     * @param iViewTypeList 
     *   List of int : the type of the view in which a web limit specification has been designed.
     *      <br><b>Legal values</b>: 
     *      <ul>
     *      <li> a set of <b>sketchs</b> or <b>curves</b> or <b>surfaces</b> designed in <b>folded</b> view,</li>
     *      <li> a set of <b>sketchs</b> or <b>curves</b> or <b>surfaces</b> designed in <b>unfolded</b> view (Not yet implemented).</li>
     *      </ul>
     */  
    virtual HRESULT __stdcall SetBoundaryElements (const CATLISTP(CATISpecObject) & iBoundaryElementsList,
                                                   const CATListOfInt             & iViewTypeList) = 0;

    /**
     * <br><b>Role</b>: This methode retrieves the list of joggles of web.
     * @param oJogglesList 
     *   List of specifications : <b>Joggle</b>(s).
	   *
     */ 
	  virtual HRESULT __stdcall GetJoggles (CATLISTP(CATISpecObject) & oJogglesList) = 0;

  };

CATDeclareHandler(CATIStmWeb, CATBaseUnknown);

#endif
