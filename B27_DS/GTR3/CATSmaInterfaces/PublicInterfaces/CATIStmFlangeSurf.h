/* -*-c++-*- */
#ifndef CATIStmFlangeSurf_H
#define CATIStmFlangeSurf_H

// COPYRIGHT DASSAULT SYSTEMES  1999

/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */ 
#include "CATBaseUnknown.h"
#include "ExportedByCATSmaInterfaces.h"
#include "CATBaseUnknown.h"
#include "CATISpecObject.h"
#include "CATListPtrCATISpecObject.h"
#include "CATListOfInt.h"
#include "CATBody.h"
#include "CATUnicodeString.h"
#include "CATListOfCATUnicodeString.h"
#include "CATICkeParm.h"

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByCATSmaInterfaces IID IID_CATIStmFlangeSurf;
#else
extern "C" const IID IID_CATIStmFlangeSurf;
#endif

/**
 * Interface to manage the aerospace sheet metal <b>surfacic flange</b> feature.
 * <b>Role</b>: Provides the basic methods to access data of a sheet metal
 * <b>surfacic flange</b> feature.
 *
 * <br>A <b>surfacic flange</b> is defined by :
 *      <ul>
 *      <li> <b>base feature</b>              : the <b>web</b> or another <b>surfacic flange</b>,</li>
 *      <li> <b>support</b>                   : a surface or a curve,</li>
 *      <li> <b>fillet radius</b>             : a length,</li>
 *      <li> <b>edge of part</b>              : a curve or a length,</li>
 *      <li> <b>first side</b>                : a curve or a plane,</li>
 *      <li> <b>first side corner radius</b>  : a length,</li>
 *      <li> <b>second side</b>               : a curve or a plane,</li>
 *      <li> <b>second side corner radius</b> : a length,</li>
 *      <li> <b>manufacturing process</b>     : a parameter,</li>
 *      <li> <b>joggle compensation</b>       : a parameter,</li>
 *      <li> <b>first side compensation</b>   : a parameter,</li>
 *      <li> <b>second side compensation</b>  : a parameter.</li>
 *      </ul>
 */

class ExportedByCATSmaInterfaces CATIStmFlangeSurf : public CATBaseUnknown
  { 
  CATDeclareInterface;
   
  public: 
    
    /**
     * <br><b>Role</b>: This method retrieves the base feature of the surfacic flange.
     *
     * @param opiBaseFeatureSpec 
     *   Specification of the base feature  
     *      <br><b>Legal values</b>: 
     *      <ul>
     *      <li> the <b>web</b>,</li>
     *      <li> another <b>surfacic flange</b>.</li>
     *      </ul>
     */ 
    virtual HRESULT __stdcall GetBaseFeature (CATISpecObject ** opiBaseFeatureSpec) = 0;

    /**
     * <br><b>Role</b>: This method sets the base feature of the surfacic flange.
     *
     * @param ipiBaseFeatureSpec 
     *   Specification of the base feature  
     *      <br><b>Legal values</b>: 
     *      <ul>
     *      <li> the <b>web</b>,</li>
     *      <li> another <b>surfacic flange</b>.</li>
     *      </ul>
     */ 
    virtual HRESULT __stdcall SetBaseFeature (const CATISpecObject * ipiBaseFeatureSpec) = 0;

    /**
     * <br><b>Role</b>: This method retrieves the bend radius type of the surfacic flange.
     * @param oBendRadiusType 
     *   a CATUnicodeString  
     *      <br><b>Legal values</b>: 
     *      <ul>
     *      <li> "<b>Constant</b>".</li>
     *      </ul>
     */ 
    virtual HRESULT __stdcall GetBendRadiusType (CATUnicodeString & oBendRadiusType) = 0;
	    
    /**
     * <br><b>Role</b>: This method sets the bend radius type of the surfacic flange.
     * @param iBendRadiusType 
     *   a CATUnicodeString  
     *      <br><b>Legal values</b>: 
     *      <ul>
     *      <li> "<b>Constant</b>".</li>
     *      </ul>
     */ 
    virtual HRESULT __stdcall SetBendRadiusType (const CATUnicodeString & iBendRadiusType) = 0;

    /**
     * <br><b>Role</b>: This method retrieves the bend radius value of the surfacic flange.
     *
     * @param opiBendRadiusValueParam  
     *   a CATICkeParm : use the method <b>Valuate</b> to modify the bend radius value. @see CATICkeParm.
     */ 
    virtual HRESULT __stdcall GetBendRadiusValue (CATICkeParm ** opiBendRadiusValueParam) = 0;

	  /**
     * <br><b>Role</b>: This method retrieves the support type of the surfacic flange.
     *
     * @param oSupportType 
     *   a CATUnicodeString  
     *      <br><b>Legal values</b>: 
     *      <ul>
     *      <li> "<b>Exact</b>",</li>
     *      <li> "<b>Approximation</b>",</li>
     *      <li> "<b>Angle</b>".</li>
     *      </ul>
     */ 
    virtual HRESULT __stdcall GetSupportType (CATUnicodeString & oSupportType) = 0;
	  
    /**
     * <br><b>Role</b>: This method sets the support type of the surfacic flange.
     *
     * @param iSupportType
     *   a CATUnicodeString  
     *      <br><b>Legal values</b>: 
     *      <ul>
     *      <li> "<b>Exact</b>",</li>
     *      <li> "<b>Approximation</b>",</li>
     *      <li> "<b>Angle</b>".</li>
     *      </ul>
     */ 
    virtual HRESULT __stdcall SetSupportType (const CATUnicodeString & iSupportType) = 0;

    /**
     * <br><b>Role</b>: This method retrieves the support of the surfacic flange.
     * @param opiSupportSpec
     *   Specification of the support  
     *      <br><b>Legal values</b>: 
     *      <ul>
     *      <li> a <b>surface</b> (if support type is different from "<b>Angle</b>"),</li>
     *      <li> a <b>curve</b>   (if support type is equal to "<b>Angle</b>").</li>
     *      </ul>
     */ 
    virtual HRESULT __stdcall GetSupport (CATISpecObject ** opiSupportSpec) = 0;

    /**
     * <br><b>Role</b>: This method sets the support of the surfacic flange.
     * @param ipiSupportSpec
     *   Specification of the support  
     *      <br><b>Legal values</b>: 
     *      <ul>
     *      <li> a <b>surface</b> (if support type is different from "<b>Angle</b>"),</li>
     *      <li> a <b>curve</b>   (if support type is equal to "<b>Angle</b>").</li>
     *      </ul>
     */ 
    virtual HRESULT __stdcall SetSupport (const CATISpecObject * ipiSupportSpec) = 0;

    /**
     * <br><b>Role</b>: This method is usefull to limit the support of the surfacic flange
     *                  if the support type is "Angle" or "Exact".
     * @param opiLengthParam  
     *   a CATICkeParm : use the method <b>Valuate</b> to modify the support length value. @see CATICkeParm.
     */ 
    virtual HRESULT __stdcall GetSupportLength (CATICkeParm ** opiLengthParam) = 0;
    
    /**
     * <br><b>Role</b>: This method retrieves the approximate length of the surfacic flange 
     *                  if <b>Support type</b> is equal to <b>Approximation</b>.
     * @param opiApproxLengthParam  
     *   a CATICkeParm : Do not modify this value.
     */ 
    virtual HRESULT __stdcall GetSupportApproxLength	(CATICkeParm ** opiApproxLengthParam) = 0;
    
    /**
     * <br><b>Role</b>: This method retrieves the Angle of the support and the base feature support
     *                  if <b>Support type</b> is equal to <b>Angle</b>.
     * @param opiSupportAngleParam  
     *   a CATICkeParm : use the method <b>Valuate</b> to modify the bend radius value. @see CATICkeParm.
     */ 
  	virtual HRESULT __stdcall GetSupportAngle (CATICkeParm ** opiSupportAngleParam) = 0;

 	  /**
     * <br><b>Role</b>: This method retrieves the base feature orientation of the Surfacic Flange,
     *                  it defines the base feature side to keep compared to the flange support orientation.
     *
     * @param oBaseFeatureOrient  
     *      A reference to a <b>CATOrientation</b> instance.
     *      <br><b>Legal values</b>: 
     *      <ul>
     *      <li> <b>CATOrientationNegative</b>: The base feature side to keep is opposite to the flange support orientation,</li>
     *      <li> <b>CATOrientationPositive</b>: The base feature side to keep is same as the flange support orientation,</li>
     *      <li> <b>CATOrientationUnknown</b> : The orientation is unknown.</li>
     *      </ul>
     */ 
    virtual HRESULT __stdcall GetBaseFeatureOrient (CATOrientation & oBaseFeatureOrient) = 0;
 	  
    /**
     * <br><b>Role</b>: This method sets the base feature orientation of the Surfacic Flange,
     *                  it defines the base feature side to keep compared to the flange support orientation.
     *
     * @param iBaseFeatureOrient  
     *      A reference to a <b>CATOrientation</b> instance.
     *      <br><b>Legal values</b>: 
     *      <ul>
     *      <li> <b>CATOrientationNegative</b>: The base feature side to keep is opposite to the flange support orientation,</li>
     *      <li> <b>CATOrientationPositive</b>: The base feature side to keep is same as the flange support orientation,</li>
     *      <li> <b>CATOrientationUnknown</b> : The orientation is unknown.</li>
     *      </ul>
     */ 
    virtual HRESULT __stdcall SetBaseFeatureOrient (const CATOrientation & iBaseFeatureOrient) = 0;

    /**
     * <br><b>Role</b>: This method retrieves the flange orientation of the Surfacic Flange,
     *                  it defines the fillet orientation compared to the base feature support orientation.
     * @param oFlangeOrient  
     *      A reference to a <b>CATOrientation</b> instance.
     *      <br><b>Legal values</b>: 
     *      <ul>
     *      <li> <b>CATOrientationNegative</b>: The fillet and the base feature support orientations are opposite,</li>
     *      <li> <b>CATOrientationPositive</b>: The fillet and the base feature support orientations are the same,</li>
     *      <li> <b>CATOrientationUnknown</b> : The orientation is unknown.</li>
     *      </ul>
     */ 
    virtual HRESULT __stdcall GetFlangeOrient (CATOrientation & oFlangeOrient) = 0;
    
    /**
     * <br><b>Role</b>: This method sets the flange orientation of the Surfacic Flange,
     *                  it defines the fillet orientation compared to the base feature support orientation.
     * @param iFlangeOrient  
     *      A reference to a <b>CATOrientation</b> instance.
     *      <br><b>Legal values</b>: 
     *      <ul>
     *      <li> <b>CATOrientationNegative</b>: The fillet and the base feature support orientations are opposite,</li>
     *      <li> <b>CATOrientationPositive</b>: The fillet and the base feature support orientations are the same,</li>
     *      <li> <b>CATOrientationUnknown</b> : The orientation is unknown.</li>
     *      </ul>
     */ 
    virtual HRESULT __stdcall SetFlangeOrient (const CATOrientation & iFlangeOrient) = 0;

    /**
     * <br><b>Role</b>: This method retrieves the material orientation of the surfacic flange.
     *                  It defines the thickness direction compared to the surfacic flange support orientation.
     * @param oMaterialOrient  
     *      A reference to a <b>CATOrientation</b> instance.
     *      <br><b>Legal values</b>: 
     *      <ul>
     *      <li> <b>CATOrientationNegative</b>: The thickness and the surfacic flange support orientations are opposite,</li>
     *      <li> <b>CATOrientationPositive</b>: The thickness and the surfacic flange support orientations are the same,</li>
     *      <li> <b>CATOrientationUnknown</b> : The orientation is unknown.</li>
     *      </ul>
     */ 
    virtual HRESULT __stdcall GetMaterialOrient (CATOrientation & oMaterialOrient) = 0;

    /**
     * <br><b>Role</b>: This method sets the material orientation of the surfacic flange.
     *                  It defines the thickness direction compared to the surfacic flange support orientation.
     * @param iMaterialOrient  
     *      A reference to a <b>CATOrientation</b> instance.
     *      <br><b>Legal values</b>: 
     *      <ul>
     *      <li> <b>CATOrientationNegative</b>: The thickness and the surfacic flange support orientations are opposite,</li>
     *      <li> <b>CATOrientationPositive</b>: The thickness and the surfacic flange support orientations are the same,</li>
     *      <li> <b>CATOrientationUnknown</b> : The orientation is unknown.</li>
     *      </ul>
     */ 
    virtual HRESULT __stdcall SetMaterialOrient (const CATOrientation & iMaterialOrient) = 0;

	  /**
     * <br><b>Role</b>: This method retrieves the <b>edge of part</b> type of the surfacic flange.
     * @param oEOPType
     *   a CATUnicodeString  
     *      <br><b>Legal values</b>: 
     *      <ul>
     *      <li> "<b>Length From OML</b>",</li>
     *      <li> "<b>Element FD</b>",</li>
     *      <li> "<b>Element FP</b>".</li>
     *      </ul>
     */ 
    virtual HRESULT __stdcall GetEOPType (CATUnicodeString & oEOPType) = 0;
	  
    /**
     * <br><b>Role</b>: This method sets the <b>edge of part</b> type of the surfacic flange.
     * @param iEOPType
     *   a CATUnicodeString  
     *      <br><b>Legal values</b>: 
     *      <ul>
     *      <li> "<b>Length From OML</b>",</li>
     *      <li> "<b>Element FD</b>",</li>
     *      <li> "<b>Element FP</b>".</li>
     *      </ul>
     */ 
    virtual HRESULT __stdcall SetEOPType (const CATUnicodeString & iEOPType) = 0;
    
    /**
     * <br><b>Role</b>: This methode retrieves the length of the edge of part of the surfacic flange
     *                  if the edge of part type is equal to <b>Length from OML</b>.
     *
     * @param opiEOPLengthParam  
     *   a CATICkeParm : use the method <b>Valuate</b> to modify the Length From OML value. @see CATICkeParm.
     */ 
    virtual HRESULT __stdcall GetEOPLength (CATICkeParm ** opiEOPLengthParam) = 0;

    /**
     * <br><b>Role</b>: This methode retrieves the list of elements of the edge of part 
     *                  if the edge of part type is equal to <b>Element FD</b> or <b>Element FP</b>.
     * @param oEOPElementsList  
     *   List of specifications : curves or opened sketchs.
	   *
     */ 
	  virtual HRESULT __stdcall GetEOPElements (CATLISTP(CATISpecObject) & oEOPElementsList) = 0;
    
    /**
     * <br><b>Role</b>: This methode sets the list of elements of the edge of part 
     *                  if the edge of part type is equal to <b>Element FD</b> or <b>Element FP</b>.
     * @param iEOPElementsList  
     *   List of specifications : curves or opened sketchs.
	   *
     */ 
	  virtual HRESULT __stdcall SetEOPElements (const CATLISTP(CATISpecObject) & iEOPElementsList) = 0;

	  /**
     * <br><b>Role</b>: This method retrieves the first side type of the surfacic flange.
     * @param oSideType
     *   a CATUnicodeString  
     *      <br><b>Legal values</b>: 
     *      <ul>
     *      <li> "<b>Standard</b>",</li>
     *      <li> "<b>None</b>",</li>
     *      <li> "<b>Element FD</b>",</li>
     *      <li> "<b>Element FP</b>".</li>
     *      </ul>
     */ 
    virtual HRESULT __stdcall GetSide1Type (CATUnicodeString & oSideType) = 0;
	  
    /**
     * <br><b>Role</b>: This method sets the first side type of the surfacic flange.
     * @param iSideType
     *   a CATUnicodeString  
     *      <br><b>Legal values</b>: 
     *      <ul>
     *      <li> "<b>Standard</b>",</li>
     *      <li> "<b>None</b>",</li>
     *      <li> "<b>Element FD</b>",</li>
     *      <li> "<b>Element FP</b>".</li>
     *      </ul>
     */ 
    virtual HRESULT __stdcall SetSide1Type (const CATUnicodeString & iSideType) = 0;

    /**
     * <br><b>Role</b>: This method retrieves the first side elements of the surfacic flange
     *                  if it's type is equal to <b>Element FD</b> or <b>Element FD</b>.
     * @param oSideElementsList  
     *   List of specifications : curve(s) or opened sketch(s) or plane(s).
     */ 
    virtual HRESULT __stdcall GetSide1Elements (CATLISTP(CATISpecObject) & oSideElementsList) = 0;

    /**
     * <br><b>Role</b>: This method sets the first side elements of the surfacic flange
     *                  if it's type is equal to <b>Element FD</b> or <b>Element FD</b>.
     * @param iSideElementsList  
     *   List of specifications : curve(s) or opened sketch(s) or plane(s).
     */ 
    virtual HRESULT __stdcall SetSide1Elements (const CATLISTP(CATISpecObject) & iSideElementsList) = 0;

	 /**
     * <br><b>Role</b>: This method retrieves the first side Dress-Up type of the surfacic flange.
     * @param oDressUpType
     *   a CATUnicodeString  
     *      <br><b>Legal values</b>: 
     *      <ul>
     *      <li> "<b>None</b>",</li>
     *      <li> "<b>Corner</b>".</li>
     *      </ul>
     */ 
    virtual HRESULT __stdcall GetSide1DressUpType (CATUnicodeString & oDressUpType) = 0;
	 
    /**
     * <br><b>Role</b>: This method sets the first side Dress-Up type of the surfacic flange.
     * @param iDressUpType
     *   a CATUnicodeString  
     *      <br><b>Legal values</b>: 
     *      <ul>
     *      <li> "<b>None</b>",</li>
     *      <li> "<b>Corner</b>".</li>
     *      </ul>
     */ 
    virtual HRESULT __stdcall SetSide1DressUpType (const CATUnicodeString & iDressUpType) = 0;
    
    /**
     * <br><b>Role</b>: This method retrieves the first side corner value of the surfacic flange 
     *                  if its DressUp type is equal to <b>Corner</b>.
     * @param opiCornerRadiusParam  
     *   a CATICkeParm : use the method <b>Valuate</b> to modify the first side corner radius value. @see CATICkeParm.
     */ 
    virtual HRESULT __stdcall GetSide1CornerRadius (CATICkeParm ** opiCornerRadiusParam) = 0;

	  /**
     * <br><b>Role</b>: This method retrieves the second side type of the surfacic flange.
     * @param oSideType
     *   a CATUnicodeString  
     *      <br><b>Legal values</b>: 
     *      <ul>
     *      <li> "<b>Standard</b>",</li>
     *      <li> "<b>None</b>",</li>
     *      <li> "<b>Element FD</b>",</li>
     *      <li> "<b>Element FP</b>".</li>
     *      </ul>
     */ 
    virtual HRESULT __stdcall GetSide2Type (CATUnicodeString & oSideType) = 0;
	  
    /**
     * <br><b>Role</b>: This method sets the second side type of the surfacic flange.
     * @param iSideType
     *   a CATUnicodeString  
     *      <br><b>Legal values</b>: 
     *      <ul>
     *      <li> "<b>Standard</b>",</li>
     *      <li> "<b>None</b>",</li>
     *      <li> "<b>Element FD</b>",</li>
     *      <li> "<b>Element FP</b>".</li>
     *      </ul>
     */ 
    virtual HRESULT __stdcall SetSide2Type (const CATUnicodeString & iSideType) = 0;

    /**
     * <br><b>Role</b>: This method retrieves the second side elements of the surfacic flange
     *                  if it's type is equal to <b>Element FD</b> or <b>Element FD</b>.
     * @param oSideElementsList  
     *   List of specifications : curve(s) or opened sketch(s) or plane(s).
     */ 
    virtual HRESULT __stdcall GetSide2Elements (CATLISTP(CATISpecObject) & oSideElementsList) = 0;

    /**
     * <br><b>Role</b>: This method sets the second side elements of the surfacic flange
     *                  if it's type is equal to <b>Element FD</b> or <b>Element FD</b>.
     * @param iSideElementsList  
     *   List of specifications : curve(s) or opened sketch(s) or plane(s).
     */ 
    virtual HRESULT __stdcall SetSide2Elements (const CATLISTP(CATISpecObject) & iSideElementsList) = 0;

	  /**
     * <br><b>Role</b>: This method retrieves the second side Dress-Up type of the surfacic flange.
     * @param oDressUpType
     *   a CATUnicodeString  
     *      <br><b>Legal values</b>: 
     *      <ul>
     *      <li> "<b>None</b>",</li>
     *      <li> "<b>Corner</b>".</li>
     *      </ul>
     */ 
    virtual HRESULT __stdcall GetSide2DressUpType (CATUnicodeString & oDressUpType) = 0;
    
    /**
     * <br><b>Role</b>: This method sets the second side Dress-Up type of the surfacic flange.
     * @param iDressUpType
     *   a CATUnicodeString  
     *      <br><b>Legal values</b>: 
     *      <ul>
     *      <li> "<b>None</b>",</li>
     *      <li> "<b>Corner</b>".</li>
     *      </ul>
     */ 
    virtual HRESULT __stdcall SetSide2DressUpType (const CATUnicodeString & iDressUpType) = 0;
    
    /**
     * <br><b>Role</b>: This method retrieves the second side corner value of the surfacic flange 
     *                  if its Dress-Up type is equal to <b>Corner</b>.
     * @param opiCornerRadiusParam  
     *   a CATICkeParm : use the method <b>Valuate</b> to modify the first side corner radius value. @see CATICkeParm.
     */ 
    virtual HRESULT __stdcall GetSide2CornerRadius (CATICkeParm ** opiCornerRadiusParam) = 0;
    
    /**
     * <br><b>Role</b>: This method retrieves the K_Factor associated to the fillet of the surfacic flange.
     * @param opiKFactorParam  
     *   a CATICkeParm : use the method <b>Valuate</b> to modify the K_Factor value. @see CATICkeParm.
     */ 
    virtual HRESULT __stdcall GetKFactor (CATICkeParm ** opiKFactorParam) = 0;

	  /**
     * <br><b>Role</b>: This method retrieves the manufacturing process type of the surfacic flange.
     * @param oManufProcType
     *   a CATUnicodeString  
     *      <br><b>Legal values</b>: 
     *      <ul>
     *      <li> "<b>Hydropressed</b>",</li>
     *      <li> "<b>BreakFormed</b>".</li>
     *      </ul>
     */ 
    virtual HRESULT __stdcall GetManufacturingProcessType (CATUnicodeString & oManufProcType) = 0;

	  /**
     * <br><b>Role</b>: This method sets the manufacturing process type of the surfacic flange.
     * @param iManufProcType
     *   a CATUnicodeString  
     *      <br><b>Legal values</b>: 
     *      <ul>
     *      <li> "<b>Hydropressed</b>",</li>
     *      <li> "<b>BreakFormed</b>".</li>
     *      </ul>
     */ 
    virtual HRESULT __stdcall SetManufacturingProcessType (const CATUnicodeString & iManufProcType) = 0;

    /**
     * <br><b>Role</b>: This methode retrieves the list of joggles of the surfacic flange.
     * @param oJogglesList  
     *   List of specifications : <b>Joggle</b>(s).
	   *
     */ 
	  virtual HRESULT __stdcall GetJoggles (CATLISTP(CATISpecObject) & oJogglesList) = 0;

    /**
     * <br><b>Role</b>: This method retrieves if the joggle compensation is set on the surfacic flange.
     *
     * @param oCompensation
     *   a CATUnicodeString  
     *      <br><b>Legal values</b>: 
     *      <ul>
     *      <li> "<b>None</b>",</li>
     *      <li> "<b>Apply</b>",</li>
     *      </ul>
     */ 
    virtual HRESULT __stdcall GetJoggleCompensation (CATUnicodeString & oCompensation) = 0;

	  /**
     * <br><b>Role</b>: This method sets the joggle compensation of the surfacic flange.
     *
     *                  !!! The joggle compensation method is defined on the sheet metal parameters.
     *
     * @param ipiPrtPartSpec
     *   The Part Feature
     * @param iCompensation
     *   a CATUnicodeString  
     *      <br><b>Legal values</b>: 
     *      <ul>
     *      <li> "<b>None</b>",</li>
     *      <li> "<b>Apply</b>",</li>
     *      </ul>
     */ 
    virtual HRESULT __stdcall SetJoggleCompensation (const CATISpecObject *   ipiPrtPartSpec,
                                                     const CATUnicodeString & iCompensation) = 0;

	  /**
     * <br><b>Role</b>: This method retrieves the first side compensation of the surfacic flange.
     * @param oCompensation
     *   a CATUnicodeString  
     *      <br><b>Legal values</b>: 
     *      <ul>
     *      <li> "<b>None</b>",</li>
     *      <li> "<b>Automatic</b>",</li>
     *      <li> "<b>Angle</b>",</li>
     *      <li> "<b>Length</b>".</li>
     *      </ul>
     */ 
    virtual HRESULT __stdcall GetSide1CompensationType (CATUnicodeString & oCompensation) = 0;
	  
    /**
     * <br><b>Role</b>: This method retrieves the first side compensation value of the surfacic flange.
     * @param oCompensationValue
     *   a double 
     *
     */ 
    virtual HRESULT __stdcall GetSide1CompensationValue (double & oCompensation) = 0;
	      
    /**
     * <br><b>Role</b>: This method valuate the first side compensation value of the surfacic flange.
     * @param iCompensationValue
     *   a double 
     *
     */ 
    virtual HRESULT __stdcall SetSide1CompensationValue (const double & iCompensation) = 0;
    
    /**
     * <br><b>Role</b>: This method sets the first side compensation of the surfacic flange.
     * @param iCompensation
     *   a CATUnicodeString  
     *      <br><b>Legal values</b>: 
     *      <ul>
     *      <li> "<b>None</b>",</li>
     *      <li> "<b>Automatic</b>",</li>
     *      <li> "<b>Angle</b>",</li>
     *      <li> "<b>Length</b>".</li>
     *      </ul>
     */ 
    virtual HRESULT __stdcall SetSide1CompensationType (const CATUnicodeString & iCompensation) = 0;

	  /**
     * <br><b>Role</b>: This method retrieves the second side compensation of the surfacic flange.
     * @param oCompensation
     *   a CATUnicodeString  
     *      <br><b>Legal values</b>: 
     *      <ul>
     *      <li> "<b>None</b>",</li>
     *      <li> "<b>Automatic</b>",</li>
     *      <li> "<b>Angle</b>",</li>
     *      <li> "<b>Length</b>".</li>
     *      </ul>
     */ 
    virtual HRESULT __stdcall GetSide2CompensationType (CATUnicodeString & oCompensation) = 0;

	  /**
     * <br><b>Role</b>: This method sets the second side compensation of the surfacic flange.
     * @param iCompensation
     *   a CATUnicodeString  
     *      <br><b>Legal values</b>: 
     *      <ul>
     *      <li> "<b>None</b>",</li>
     *      <li> "<b>Automatic</b>",</li>
     *      <li> "<b>Angle</b>",</li>
     *      <li> "<b>Length</b>".</li>
     *      </ul>
     */ 
    virtual HRESULT __stdcall SetSide2CompensationType (const CATUnicodeString & iCompensation) = 0;
    
    /**
     * <br><b>Role</b>: This method retrieves the second side compensation value of the surfacic flange.
     * @param oCompensationValue
     *   a double 
     *
     */ 
    virtual HRESULT __stdcall GetSide2CompensationValue (double & oCompensation) = 0;
	      
    /**
     * <br><b>Role</b>: This method valuate the second side compensation value of the surfacic flange.
     * @param iCompensationValue
     *   a double 
     *
     */ 
    virtual HRESULT __stdcall SetSide2CompensationValue (const double & iCompensation) = 0;

  };

CATDeclareHandler (CATIStmFlangeSurf, CATBaseUnknown);

#endif
