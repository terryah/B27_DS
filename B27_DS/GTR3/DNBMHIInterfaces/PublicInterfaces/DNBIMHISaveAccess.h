/**
 * @CAA2Level L1
 * @CAA2Usage U3 
 */
// COPYRIGHT Dassault Systemes 2006
//===================================================================
//
// DNBIMHISaveAccess.h
// Define the DNBIMHISaveAccess interface
//
//===================================================================
//
// Usage notes:
//   New interface: describe its use here
//
//===================================================================
//
//  Mar 2006  Creation: Code generated by the CAA wizard  yms
//===================================================================
#ifndef DNBIMHISaveAccess_H
#define DNBIMHISaveAccess_H

#include "CAADNBPDMItfCPP.h"
#include "CATBaseUnknown.h"

// System Framework
#include "CATBooleanDef.h"
#include "CATListOfCATUnicodeString.h"
class CATUnicodeString;

// DNBMHIInterfaces Framework
class DNBIMHILoadParameters;

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByCAADNBPDMItfCPP IID IID_DNBIMHISaveAccess;
#else
extern "C" const IID IID_DNBIMHISaveAccess ;
#endif

//------------------------------------------------------------------

/**
* Interface representing a means to 
*  (1) Save data to the Hub
*  (2) Retrieve certain information on the loaded document
*
* <p> 
* DNBIMHISaveAccess is implemented on CATDocument.
* Applications and CAA partners should <b>NOT</b> implement this interface.
*
* @example
*  <pre>
*
*  // *********************************
*  // Example: Saving a Loaded Document
*  // *********************************
*
*  CATDocument * pLoadedDocument = ... ;
*  if (pLoadedDocument)
*  {
*     DNBIMHISaveAccess * pSaveAccess = NULL;
*     HRESULT RC = pLoadedDocument-&gt;QueryInterface(IID_DNBIMHISaveAccess,
*                                     (void**) &amp;pSaveAccess);
*     if (SUCCEEDED(RC) && NULL != pSaveAccess)
*     {
*        CATUnicodeString uDetailingName = "Detailing for Workplan A";
*        CATBoolean bOverwriteDetailing = TRUE;
*        CATListOfCATUnicodeString ListErrorMessages;
*        RC = pSaveAccess-&gt;SaveToPPRHub( uDetailingName, 
*                        bOverwriteDetailing, ListErrorMessages );
*     
*        pSaveAccess-&gt;Release();
*        pSaveAccess = NULL;
*      
*     } // if (SUCCEEDED(RC) && NULL != pSaveAccess)
*
*  } // if (pLoadedDocument)  
*  </pre>
*
*/
class ExportedByCAADNBPDMItfCPP DNBIMHISaveAccess: public CATBaseUnknown
{
   CATDeclareInterface;

public:

   /** 
   * This method saves a document from V5 to the PPR Hub
   * <br><b>Note:</b> All Tools->Options settings will be respected during the save
   * @param iDetailingName [in]
   *   The name of the detailing to save
   *   <br><b>Note:</b> If the Tools->Options 'Save Without Detailing' is checked ON, 
   *   then NO detailing will be saved. This argument will be ignored and only the
   *   exposed data will be saved to the Hub
   * @param iOverwriteDetailing [in]
   *   Option to indicate if a detailing with the same name needs to overwritten
   *   If this argument is FALSE and a detailing with the same name exists, then
   *   the save will be aborted (Neither detailing nor exposed data will be saved)
   * @param oListErrorMessages [out]
   *   List of error messages about invalid or incorrect arguments (for example 
   *   Tools->Options settings conflicting with input arguments to method). 
   *   <br><b>Note:</b> This will <b>not</b> contain the error messages that occured during 
   *   the save (for e.g., failure to save an object/relation in the database)
   * @return
   *   <code>S_OK</code> if everything ran OK
   *   <code>E_FAIL</code> on error
   *
   */
   virtual HRESULT SaveToPPRHub( const CATUnicodeString & iDetailingName, 
      const CATBoolean & iOverwriteDetailing, 
      CATListOfCATUnicodeString & oListErrorMessages ) = 0;

   /** 
   * Gets the interface pointed to the Load Parameters object that contains
   * all the information pertaining to the load of the document
   * @param oLoadParameters [out]
   *   Interface pointer to the Load Parameters object.  This must be
   *   released after use (As per normal interface lifecycle rules)
   * @return
   *   <code>S_OK</code> if everything ran OK
   *   <code>E_FAIL</code> on error
   *
   */
   virtual HRESULT GetLoadParameters( DNBIMHILoadParameters *& oLoadParameters ) = 0;

   /** 
   * Gets the name of the currently loaded detailing and the names
   * of all the other detailings that exist for the loaded object
   * @param oCurrentlyLoadedDetailing [out]
   *   The name of the currently loaded detailing. 
   *   This will be empty if no detailing was loaded
   * @param oListDetailingNames [out]
   *   The names of all the detailings that exist on the loaded object
   * @return
   *   <code>S_OK</code> if names returned OK
   *   <code>S_FALSE</code> if no detailings exist for the loaded object
   *   <code>E_FAIL</code> on error
   */
   virtual HRESULT GetDetailingNames( CATUnicodeString & oCurrentlyLoadedDetailing,
      CATListOfCATUnicodeString & oListDetailingNames ) = 0;

};

//------------------------------------------------------------------
CATDeclareHandler( DNBIMHISaveAccess, CATBaseUnknown );

#endif
