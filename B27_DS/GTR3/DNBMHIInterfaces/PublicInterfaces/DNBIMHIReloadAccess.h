// COPYRIGHT Dassault Systemes 2008
//===================================================================
//
// DNBIMHIReloadAccess.h
// Define the DNBIMHIReloadAccess interface
//
//===================================================================
//
// Usage notes:
//   Interface to reload the already loaded document with the same
//   set of filters or different set of filters.
//   If different set of filters are to be applied at the time of
//   reload, the user has to call SetFilters() method prior to calling
//   ReloadFromPPRHub().
//
//===================================================================
//
//  Feb 2008  Creation: Code generated by the CAA wizard  BCM
//===================================================================
#ifndef DNBIMHIReloadAccess_H
#define DNBIMHIReloadAccess_H

#include "CAADNBPDMItfCPP.h"
#include "CATBaseUnknown.h"
#include "CATListOfCATUnicodeString.h"

class CATUnicodeString;

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByCAADNBPDMItfCPP IID IID_DNBIMHIReloadAccess;
#else
extern "C" const IID IID_DNBIMHIReloadAccess ;
#endif

//------------------------------------------------------------------

/**
* Interface to reload the already loaded document with same or different filters
*
* <b>Note:</b> If different set of filters are to be applied at the time of
*  reload, the user has to call SetFilters() method prior to calling
*  ReloadFromPPRHub().
* 
*/
class ExportedByCAADNBPDMItfCPP DNBIMHIReloadAccess: public CATBaseUnknown
{
  CATDeclareInterface;

  public:

    /**
    * This method set the filters to the currently loaded document
    *
    * @param iFilters [in]
    *  <pre>
    *  The filters to be set. This is a list of values separated by XML type tags
    *  The following tags are supported 
    *     &lt;GlobalFilterSetName&gt;FilterSetName for Global&lt;/GlobalFilterSetName&gt;
    *     &lt;ProductFilterSetName&gt;FilterSetName for Product&lt;/ProductFilterSetName&gt;  
    *     &lt;ProcessFilterSetName&gt;FilterSetName for Process&lt;/ProcessFilterSetName&gt;
    *     &lt;ResourceFilterSetName&gt;FilterSetName for Resource&lt;/ResourceFilterSetName&gt;
    *     &lt;ModStatementFilterID&gt;ID of Mod&lt;/ModStatementFilterID&gt;
    *     &lt;PlanningStateOwnerFilterID&gt;ID of owner planning state&lt;/PlanningStateOwnerFilterID&gt;
    *     &lt;PlanningStateOtherFilterID&gt;ID of others planning state&lt;/PlanningStateOtherFilterID&gt;
    *     &lt;ApplyImplicitFilter&gt;TRUE or FALSE&lt;/ApplyImplicitFilter&gt;
    *     &lt;ProductFilterByCO&gt;
    *             &lt;PreFilter&gt;ID of SB1 &lt;/PreFilter&gt;
    *             &lt;PostFilter&gt;ID of SB2&lt;/PostFilter&gt;
    *             &lt;PrePostFilter&gt;ID of SB3 &lt;/PrePostFilter&gt;
    *     &lt;/ProductFilterByCO&gt; 
    *     (ProductFilterByCO is valid only if ProductFilterSetName is set)
    *
    * @param oErrorMessages [out]
    *   The list of message about the failures of the method
    *
    * @return
    *     <code>S_OK</code> if all OK
    *     <code>E_INVALIDARG</code> if incorrect input format
    */
   virtual HRESULT SetFilters( const CATUnicodeString &iFilters,
                               CATListOfCATUnicodeString &oErrorMessages ) = 0;

   /**
 	 * This method reloads the document checking for the validity of the loaded branch.
    *
    * NOTE 1: If SetFilters() is NOT called prior to this method, then the reload will
    * honor the same filters which with the document was originally loaded. Incase the
    * loaded branch is invalid then the reload will fail.
    *
    * NOTE 2: If SetFilters() is called prior to this method, then the filters specified
    * through SetFilters will be considered for the reload. In case the loaded branch is
    * invalid for the newly set filters then the reload will fail and the originally 
    * applied filter (with which the document was opened) will be set.
    *
    * @param oErrorMessages [out]
    *   The list of message about the failures of the method
    *
    * @return
    *     <code>S_OK</code> if all OK
    *     <code>E_FAIL</code> if reload failed
 	 */
	virtual HRESULT ReloadFromPPRHub( CATListOfCATUnicodeString &oErrorMessages ) = 0;
};

//------------------------------------------------------------------
CATDeclareHandler( DNBIMHIReloadAccess, CATBaseUnknown );

#endif
