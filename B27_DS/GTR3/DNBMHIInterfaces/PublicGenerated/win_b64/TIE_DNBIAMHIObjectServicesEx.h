#ifndef __TIE_DNBIAMHIObjectServicesEx
#define __TIE_DNBIAMHIObjectServicesEx

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIAMHIObjectServicesEx.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIAMHIObjectServicesEx */
#define declare_TIE_DNBIAMHIObjectServicesEx(classe) \
 \
 \
class TIEDNBIAMHIObjectServicesEx##classe : public DNBIAMHIObjectServicesEx \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIAMHIObjectServicesEx, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall Add(const CATSafeArrayVariant & iListObjectIDs, DNBMHIObjType iObjectType, DNBMHIInsertType iInsertType, CATSafeArrayVariant *& oListErrorMessages); \
      virtual HRESULT __stdcall Remove(const CATSafeArrayVariant & iListObjectIDs, DNBMHIObjType iObjectType, DNBMHIInsertType iInsertType, CATSafeArrayVariant *& oListErrorMessages); \
      virtual HRESULT __stdcall Replace(const CATSafeArrayVariant & iRemoveListObjectIDs, const CATSafeArrayVariant & iAddListObjectIDs, DNBMHIObjType iObjectType, DNBMHIInsertType iInsertType, CATSafeArrayVariant *& oListErrorMessages); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIAMHIObjectServicesEx(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall Add(const CATSafeArrayVariant & iListObjectIDs, DNBMHIObjType iObjectType, DNBMHIInsertType iInsertType, CATSafeArrayVariant *& oListErrorMessages); \
virtual HRESULT __stdcall Remove(const CATSafeArrayVariant & iListObjectIDs, DNBMHIObjType iObjectType, DNBMHIInsertType iInsertType, CATSafeArrayVariant *& oListErrorMessages); \
virtual HRESULT __stdcall Replace(const CATSafeArrayVariant & iRemoveListObjectIDs, const CATSafeArrayVariant & iAddListObjectIDs, DNBMHIObjType iObjectType, DNBMHIInsertType iInsertType, CATSafeArrayVariant *& oListErrorMessages); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIAMHIObjectServicesEx(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::Add(const CATSafeArrayVariant & iListObjectIDs, DNBMHIObjType iObjectType, DNBMHIInsertType iInsertType, CATSafeArrayVariant *& oListErrorMessages) \
{ \
return (ENVTIECALL(DNBIAMHIObjectServicesEx,ENVTIETypeLetter,ENVTIELetter)Add(iListObjectIDs,iObjectType,iInsertType,oListErrorMessages)); \
} \
HRESULT __stdcall  ENVTIEName::Remove(const CATSafeArrayVariant & iListObjectIDs, DNBMHIObjType iObjectType, DNBMHIInsertType iInsertType, CATSafeArrayVariant *& oListErrorMessages) \
{ \
return (ENVTIECALL(DNBIAMHIObjectServicesEx,ENVTIETypeLetter,ENVTIELetter)Remove(iListObjectIDs,iObjectType,iInsertType,oListErrorMessages)); \
} \
HRESULT __stdcall  ENVTIEName::Replace(const CATSafeArrayVariant & iRemoveListObjectIDs, const CATSafeArrayVariant & iAddListObjectIDs, DNBMHIObjType iObjectType, DNBMHIInsertType iInsertType, CATSafeArrayVariant *& oListErrorMessages) \
{ \
return (ENVTIECALL(DNBIAMHIObjectServicesEx,ENVTIETypeLetter,ENVTIELetter)Replace(iRemoveListObjectIDs,iAddListObjectIDs,iObjectType,iInsertType,oListErrorMessages)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIAMHIObjectServicesEx,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIAMHIObjectServicesEx,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIAMHIObjectServicesEx,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIAMHIObjectServicesEx,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIAMHIObjectServicesEx,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIAMHIObjectServicesEx(classe)    TIEDNBIAMHIObjectServicesEx##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIAMHIObjectServicesEx(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIAMHIObjectServicesEx, classe) \
 \
 \
CATImplementTIEMethods(DNBIAMHIObjectServicesEx, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIAMHIObjectServicesEx, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIAMHIObjectServicesEx, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIAMHIObjectServicesEx, classe) \
 \
HRESULT __stdcall  TIEDNBIAMHIObjectServicesEx##classe::Add(const CATSafeArrayVariant & iListObjectIDs, DNBMHIObjType iObjectType, DNBMHIInsertType iInsertType, CATSafeArrayVariant *& oListErrorMessages) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&iListObjectIDs,&iObjectType,&iInsertType,&oListErrorMessages); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Add(iListObjectIDs,iObjectType,iInsertType,oListErrorMessages); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&iListObjectIDs,&iObjectType,&iInsertType,&oListErrorMessages); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIObjectServicesEx##classe::Remove(const CATSafeArrayVariant & iListObjectIDs, DNBMHIObjType iObjectType, DNBMHIInsertType iInsertType, CATSafeArrayVariant *& oListErrorMessages) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&iListObjectIDs,&iObjectType,&iInsertType,&oListErrorMessages); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Remove(iListObjectIDs,iObjectType,iInsertType,oListErrorMessages); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&iListObjectIDs,&iObjectType,&iInsertType,&oListErrorMessages); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIObjectServicesEx##classe::Replace(const CATSafeArrayVariant & iRemoveListObjectIDs, const CATSafeArrayVariant & iAddListObjectIDs, DNBMHIObjType iObjectType, DNBMHIInsertType iInsertType, CATSafeArrayVariant *& oListErrorMessages) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&iRemoveListObjectIDs,&iAddListObjectIDs,&iObjectType,&iInsertType,&oListErrorMessages); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Replace(iRemoveListObjectIDs,iAddListObjectIDs,iObjectType,iInsertType,oListErrorMessages); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&iRemoveListObjectIDs,&iAddListObjectIDs,&iObjectType,&iInsertType,&oListErrorMessages); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIObjectServicesEx##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIObjectServicesEx##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIObjectServicesEx##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIObjectServicesEx##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIObjectServicesEx##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIAMHIObjectServicesEx(classe) \
 \
 \
declare_TIE_DNBIAMHIObjectServicesEx(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAMHIObjectServicesEx##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAMHIObjectServicesEx,"DNBIAMHIObjectServicesEx",DNBIAMHIObjectServicesEx::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAMHIObjectServicesEx(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIAMHIObjectServicesEx, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAMHIObjectServicesEx##classe(classe::MetaObject(),DNBIAMHIObjectServicesEx::MetaObject(),(void *)CreateTIEDNBIAMHIObjectServicesEx##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIAMHIObjectServicesEx(classe) \
 \
 \
declare_TIE_DNBIAMHIObjectServicesEx(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAMHIObjectServicesEx##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAMHIObjectServicesEx,"DNBIAMHIObjectServicesEx",DNBIAMHIObjectServicesEx::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAMHIObjectServicesEx(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIAMHIObjectServicesEx, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAMHIObjectServicesEx##classe(classe::MetaObject(),DNBIAMHIObjectServicesEx::MetaObject(),(void *)CreateTIEDNBIAMHIObjectServicesEx##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIAMHIObjectServicesEx(classe) TIE_DNBIAMHIObjectServicesEx(classe)
#else
#define BOA_DNBIAMHIObjectServicesEx(classe) CATImplementBOA(DNBIAMHIObjectServicesEx, classe)
#endif

#endif
