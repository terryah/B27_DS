#ifndef __TIE_DNBIAMHIQueryServices
#define __TIE_DNBIAMHIQueryServices

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIAMHIQueryServices.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIAMHIQueryServices */
#define declare_TIE_DNBIAMHIQueryServices(classe) \
 \
 \
class TIEDNBIAMHIQueryServices##classe : public DNBIAMHIQueryServices \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIAMHIQueryServices, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall Find(DNBMHIObjType iObjectType, const CATSafeArrayVariant & iAttributeNameList, const CATSafeArrayVariant & iAttributeValueList, CATSafeArrayVariant *& oListObjectIDs); \
      virtual HRESULT __stdcall GetRelatedObjects(const CATBSTR & iObjectID, const CATBSTR & iRelationName, CATSafeArrayVariant *& oListRelatedObjectIDs); \
      virtual HRESULT __stdcall GetType(const CATSafeArrayVariant & iListIDs, CATSafeArrayVariant *& oListTypes); \
      virtual HRESULT __stdcall GetV5ObjectFromID(const CATBSTR & iObjectID, CATIAItem *& oV5Object); \
      virtual HRESULT __stdcall GetRelationAndRelatedObjects(const CATBSTR & iObjectID, const CATBSTR & iRelationName, CATSafeArrayVariant *& oListRelatedObjectIDs, CATSafeArrayVariant *& oListRelationObjectIDs); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIAMHIQueryServices(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall Find(DNBMHIObjType iObjectType, const CATSafeArrayVariant & iAttributeNameList, const CATSafeArrayVariant & iAttributeValueList, CATSafeArrayVariant *& oListObjectIDs); \
virtual HRESULT __stdcall GetRelatedObjects(const CATBSTR & iObjectID, const CATBSTR & iRelationName, CATSafeArrayVariant *& oListRelatedObjectIDs); \
virtual HRESULT __stdcall GetType(const CATSafeArrayVariant & iListIDs, CATSafeArrayVariant *& oListTypes); \
virtual HRESULT __stdcall GetV5ObjectFromID(const CATBSTR & iObjectID, CATIAItem *& oV5Object); \
virtual HRESULT __stdcall GetRelationAndRelatedObjects(const CATBSTR & iObjectID, const CATBSTR & iRelationName, CATSafeArrayVariant *& oListRelatedObjectIDs, CATSafeArrayVariant *& oListRelationObjectIDs); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIAMHIQueryServices(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::Find(DNBMHIObjType iObjectType, const CATSafeArrayVariant & iAttributeNameList, const CATSafeArrayVariant & iAttributeValueList, CATSafeArrayVariant *& oListObjectIDs) \
{ \
return (ENVTIECALL(DNBIAMHIQueryServices,ENVTIETypeLetter,ENVTIELetter)Find(iObjectType,iAttributeNameList,iAttributeValueList,oListObjectIDs)); \
} \
HRESULT __stdcall  ENVTIEName::GetRelatedObjects(const CATBSTR & iObjectID, const CATBSTR & iRelationName, CATSafeArrayVariant *& oListRelatedObjectIDs) \
{ \
return (ENVTIECALL(DNBIAMHIQueryServices,ENVTIETypeLetter,ENVTIELetter)GetRelatedObjects(iObjectID,iRelationName,oListRelatedObjectIDs)); \
} \
HRESULT __stdcall  ENVTIEName::GetType(const CATSafeArrayVariant & iListIDs, CATSafeArrayVariant *& oListTypes) \
{ \
return (ENVTIECALL(DNBIAMHIQueryServices,ENVTIETypeLetter,ENVTIELetter)GetType(iListIDs,oListTypes)); \
} \
HRESULT __stdcall  ENVTIEName::GetV5ObjectFromID(const CATBSTR & iObjectID, CATIAItem *& oV5Object) \
{ \
return (ENVTIECALL(DNBIAMHIQueryServices,ENVTIETypeLetter,ENVTIELetter)GetV5ObjectFromID(iObjectID,oV5Object)); \
} \
HRESULT __stdcall  ENVTIEName::GetRelationAndRelatedObjects(const CATBSTR & iObjectID, const CATBSTR & iRelationName, CATSafeArrayVariant *& oListRelatedObjectIDs, CATSafeArrayVariant *& oListRelationObjectIDs) \
{ \
return (ENVTIECALL(DNBIAMHIQueryServices,ENVTIETypeLetter,ENVTIELetter)GetRelationAndRelatedObjects(iObjectID,iRelationName,oListRelatedObjectIDs,oListRelationObjectIDs)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIAMHIQueryServices,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIAMHIQueryServices,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIAMHIQueryServices,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIAMHIQueryServices,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIAMHIQueryServices,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIAMHIQueryServices(classe)    TIEDNBIAMHIQueryServices##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIAMHIQueryServices(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIAMHIQueryServices, classe) \
 \
 \
CATImplementTIEMethods(DNBIAMHIQueryServices, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIAMHIQueryServices, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIAMHIQueryServices, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIAMHIQueryServices, classe) \
 \
HRESULT __stdcall  TIEDNBIAMHIQueryServices##classe::Find(DNBMHIObjType iObjectType, const CATSafeArrayVariant & iAttributeNameList, const CATSafeArrayVariant & iAttributeValueList, CATSafeArrayVariant *& oListObjectIDs) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&iObjectType,&iAttributeNameList,&iAttributeValueList,&oListObjectIDs); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Find(iObjectType,iAttributeNameList,iAttributeValueList,oListObjectIDs); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&iObjectType,&iAttributeNameList,&iAttributeValueList,&oListObjectIDs); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIQueryServices##classe::GetRelatedObjects(const CATBSTR & iObjectID, const CATBSTR & iRelationName, CATSafeArrayVariant *& oListRelatedObjectIDs) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&iObjectID,&iRelationName,&oListRelatedObjectIDs); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetRelatedObjects(iObjectID,iRelationName,oListRelatedObjectIDs); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&iObjectID,&iRelationName,&oListRelatedObjectIDs); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIQueryServices##classe::GetType(const CATSafeArrayVariant & iListIDs, CATSafeArrayVariant *& oListTypes) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&iListIDs,&oListTypes); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetType(iListIDs,oListTypes); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&iListIDs,&oListTypes); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIQueryServices##classe::GetV5ObjectFromID(const CATBSTR & iObjectID, CATIAItem *& oV5Object) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&iObjectID,&oV5Object); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetV5ObjectFromID(iObjectID,oV5Object); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&iObjectID,&oV5Object); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIQueryServices##classe::GetRelationAndRelatedObjects(const CATBSTR & iObjectID, const CATBSTR & iRelationName, CATSafeArrayVariant *& oListRelatedObjectIDs, CATSafeArrayVariant *& oListRelationObjectIDs) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&iObjectID,&iRelationName,&oListRelatedObjectIDs,&oListRelationObjectIDs); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetRelationAndRelatedObjects(iObjectID,iRelationName,oListRelatedObjectIDs,oListRelationObjectIDs); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&iObjectID,&iRelationName,&oListRelatedObjectIDs,&oListRelationObjectIDs); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIQueryServices##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIQueryServices##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIQueryServices##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIQueryServices##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIQueryServices##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIAMHIQueryServices(classe) \
 \
 \
declare_TIE_DNBIAMHIQueryServices(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAMHIQueryServices##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAMHIQueryServices,"DNBIAMHIQueryServices",DNBIAMHIQueryServices::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAMHIQueryServices(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIAMHIQueryServices, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAMHIQueryServices##classe(classe::MetaObject(),DNBIAMHIQueryServices::MetaObject(),(void *)CreateTIEDNBIAMHIQueryServices##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIAMHIQueryServices(classe) \
 \
 \
declare_TIE_DNBIAMHIQueryServices(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAMHIQueryServices##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAMHIQueryServices,"DNBIAMHIQueryServices",DNBIAMHIQueryServices::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAMHIQueryServices(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIAMHIQueryServices, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAMHIQueryServices##classe(classe::MetaObject(),DNBIAMHIQueryServices::MetaObject(),(void *)CreateTIEDNBIAMHIQueryServices##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIAMHIQueryServices(classe) TIE_DNBIAMHIQueryServices(classe)
#else
#define BOA_DNBIAMHIQueryServices(classe) CATImplementBOA(DNBIAMHIQueryServices, classe)
#endif

#endif
