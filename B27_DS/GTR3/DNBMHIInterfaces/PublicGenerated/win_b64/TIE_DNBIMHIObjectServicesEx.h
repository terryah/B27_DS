#ifndef __TIE_DNBIMHIObjectServicesEx
#define __TIE_DNBIMHIObjectServicesEx

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "DNBIMHIObjectServicesEx.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIMHIObjectServicesEx */
#define declare_TIE_DNBIMHIObjectServicesEx(classe) \
 \
 \
class TIEDNBIMHIObjectServicesEx##classe : public DNBIMHIObjectServicesEx \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIMHIObjectServicesEx, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT Add(  CATListOfCATUnicodeString & iListObjectIDs, const DNBMHIObjType & iObjectType, const DNBMHIInsertType & iInsertType, CATListOfCATUnicodeString & oListErrorMessages ) ; \
      virtual HRESULT Remove(  CATListOfCATUnicodeString & iListObjectIDs, const DNBMHIObjType & iObjectType, const DNBMHIInsertType & iInsertType, CATListOfCATUnicodeString & oListErrorMessages ) ; \
      virtual HRESULT Replace(  CATListOfCATUnicodeString & iRemoveListObjectIDs, CATListOfCATUnicodeString & iAddListObjectIDs, const DNBMHIObjType & iObjectType, const DNBMHIInsertType & iInsertType, CATListOfCATUnicodeString & oListErrorMessages ) ; \
};



#define ENVTIEdeclare_DNBIMHIObjectServicesEx(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT Add(  CATListOfCATUnicodeString & iListObjectIDs, const DNBMHIObjType & iObjectType, const DNBMHIInsertType & iInsertType, CATListOfCATUnicodeString & oListErrorMessages ) ; \
virtual HRESULT Remove(  CATListOfCATUnicodeString & iListObjectIDs, const DNBMHIObjType & iObjectType, const DNBMHIInsertType & iInsertType, CATListOfCATUnicodeString & oListErrorMessages ) ; \
virtual HRESULT Replace(  CATListOfCATUnicodeString & iRemoveListObjectIDs, CATListOfCATUnicodeString & iAddListObjectIDs, const DNBMHIObjType & iObjectType, const DNBMHIInsertType & iInsertType, CATListOfCATUnicodeString & oListErrorMessages ) ; \


#define ENVTIEdefine_DNBIMHIObjectServicesEx(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT  ENVTIEName::Add(  CATListOfCATUnicodeString & iListObjectIDs, const DNBMHIObjType & iObjectType, const DNBMHIInsertType & iInsertType, CATListOfCATUnicodeString & oListErrorMessages )  \
{ \
return (ENVTIECALL(DNBIMHIObjectServicesEx,ENVTIETypeLetter,ENVTIELetter)Add(iListObjectIDs,iObjectType,iInsertType,oListErrorMessages )); \
} \
HRESULT  ENVTIEName::Remove(  CATListOfCATUnicodeString & iListObjectIDs, const DNBMHIObjType & iObjectType, const DNBMHIInsertType & iInsertType, CATListOfCATUnicodeString & oListErrorMessages )  \
{ \
return (ENVTIECALL(DNBIMHIObjectServicesEx,ENVTIETypeLetter,ENVTIELetter)Remove(iListObjectIDs,iObjectType,iInsertType,oListErrorMessages )); \
} \
HRESULT  ENVTIEName::Replace(  CATListOfCATUnicodeString & iRemoveListObjectIDs, CATListOfCATUnicodeString & iAddListObjectIDs, const DNBMHIObjType & iObjectType, const DNBMHIInsertType & iInsertType, CATListOfCATUnicodeString & oListErrorMessages )  \
{ \
return (ENVTIECALL(DNBIMHIObjectServicesEx,ENVTIETypeLetter,ENVTIELetter)Replace(iRemoveListObjectIDs,iAddListObjectIDs,iObjectType,iInsertType,oListErrorMessages )); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIMHIObjectServicesEx(classe)    TIEDNBIMHIObjectServicesEx##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIMHIObjectServicesEx(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIMHIObjectServicesEx, classe) \
 \
 \
CATImplementTIEMethods(DNBIMHIObjectServicesEx, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIMHIObjectServicesEx, classe, 1) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIMHIObjectServicesEx, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIMHIObjectServicesEx, classe) \
 \
HRESULT  TIEDNBIMHIObjectServicesEx##classe::Add(  CATListOfCATUnicodeString & iListObjectIDs, const DNBMHIObjType & iObjectType, const DNBMHIInsertType & iInsertType, CATListOfCATUnicodeString & oListErrorMessages )  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Add(iListObjectIDs,iObjectType,iInsertType,oListErrorMessages )); \
} \
HRESULT  TIEDNBIMHIObjectServicesEx##classe::Remove(  CATListOfCATUnicodeString & iListObjectIDs, const DNBMHIObjType & iObjectType, const DNBMHIInsertType & iInsertType, CATListOfCATUnicodeString & oListErrorMessages )  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Remove(iListObjectIDs,iObjectType,iInsertType,oListErrorMessages )); \
} \
HRESULT  TIEDNBIMHIObjectServicesEx##classe::Replace(  CATListOfCATUnicodeString & iRemoveListObjectIDs, CATListOfCATUnicodeString & iAddListObjectIDs, const DNBMHIObjType & iObjectType, const DNBMHIInsertType & iInsertType, CATListOfCATUnicodeString & oListErrorMessages )  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Replace(iRemoveListObjectIDs,iAddListObjectIDs,iObjectType,iInsertType,oListErrorMessages )); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIMHIObjectServicesEx(classe) \
 \
 \
declare_TIE_DNBIMHIObjectServicesEx(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIMHIObjectServicesEx##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIMHIObjectServicesEx,"DNBIMHIObjectServicesEx",DNBIMHIObjectServicesEx::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIMHIObjectServicesEx(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIMHIObjectServicesEx, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIMHIObjectServicesEx##classe(classe::MetaObject(),DNBIMHIObjectServicesEx::MetaObject(),(void *)CreateTIEDNBIMHIObjectServicesEx##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIMHIObjectServicesEx(classe) \
 \
 \
declare_TIE_DNBIMHIObjectServicesEx(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIMHIObjectServicesEx##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIMHIObjectServicesEx,"DNBIMHIObjectServicesEx",DNBIMHIObjectServicesEx::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIMHIObjectServicesEx(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIMHIObjectServicesEx, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIMHIObjectServicesEx##classe(classe::MetaObject(),DNBIMHIObjectServicesEx::MetaObject(),(void *)CreateTIEDNBIMHIObjectServicesEx##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIMHIObjectServicesEx(classe) TIE_DNBIMHIObjectServicesEx(classe)
#else
#define BOA_DNBIMHIObjectServicesEx(classe) CATImplementBOA(DNBIMHIObjectServicesEx, classe)
#endif

#endif
