#ifndef __TIE_DNBIAMHISaveAccess
#define __TIE_DNBIAMHISaveAccess

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIAMHISaveAccess.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIAMHISaveAccess */
#define declare_TIE_DNBIAMHISaveAccess(classe) \
 \
 \
class TIEDNBIAMHISaveAccess##classe : public DNBIAMHISaveAccess \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIAMHISaveAccess, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall SaveToPPRHub(const CATBSTR & iDetailingName, CAT_VARIANT_BOOL iOverwriteDetailing, CATSafeArrayVariant *& oListErrorMessages); \
      virtual HRESULT __stdcall GetLoadParameters(DNBIAMHILoadParameters *& oLoadParameters); \
      virtual HRESULT __stdcall GetDetailingNames(CATBSTR & oCurrentlyLoadedDetailing, CATSafeArrayVariant *& oListDetailingNames); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIAMHISaveAccess(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall SaveToPPRHub(const CATBSTR & iDetailingName, CAT_VARIANT_BOOL iOverwriteDetailing, CATSafeArrayVariant *& oListErrorMessages); \
virtual HRESULT __stdcall GetLoadParameters(DNBIAMHILoadParameters *& oLoadParameters); \
virtual HRESULT __stdcall GetDetailingNames(CATBSTR & oCurrentlyLoadedDetailing, CATSafeArrayVariant *& oListDetailingNames); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIAMHISaveAccess(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::SaveToPPRHub(const CATBSTR & iDetailingName, CAT_VARIANT_BOOL iOverwriteDetailing, CATSafeArrayVariant *& oListErrorMessages) \
{ \
return (ENVTIECALL(DNBIAMHISaveAccess,ENVTIETypeLetter,ENVTIELetter)SaveToPPRHub(iDetailingName,iOverwriteDetailing,oListErrorMessages)); \
} \
HRESULT __stdcall  ENVTIEName::GetLoadParameters(DNBIAMHILoadParameters *& oLoadParameters) \
{ \
return (ENVTIECALL(DNBIAMHISaveAccess,ENVTIETypeLetter,ENVTIELetter)GetLoadParameters(oLoadParameters)); \
} \
HRESULT __stdcall  ENVTIEName::GetDetailingNames(CATBSTR & oCurrentlyLoadedDetailing, CATSafeArrayVariant *& oListDetailingNames) \
{ \
return (ENVTIECALL(DNBIAMHISaveAccess,ENVTIETypeLetter,ENVTIELetter)GetDetailingNames(oCurrentlyLoadedDetailing,oListDetailingNames)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIAMHISaveAccess,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIAMHISaveAccess,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIAMHISaveAccess,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIAMHISaveAccess,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIAMHISaveAccess,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIAMHISaveAccess(classe)    TIEDNBIAMHISaveAccess##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIAMHISaveAccess(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIAMHISaveAccess, classe) \
 \
 \
CATImplementTIEMethods(DNBIAMHISaveAccess, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIAMHISaveAccess, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIAMHISaveAccess, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIAMHISaveAccess, classe) \
 \
HRESULT __stdcall  TIEDNBIAMHISaveAccess##classe::SaveToPPRHub(const CATBSTR & iDetailingName, CAT_VARIANT_BOOL iOverwriteDetailing, CATSafeArrayVariant *& oListErrorMessages) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&iDetailingName,&iOverwriteDetailing,&oListErrorMessages); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SaveToPPRHub(iDetailingName,iOverwriteDetailing,oListErrorMessages); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&iDetailingName,&iOverwriteDetailing,&oListErrorMessages); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHISaveAccess##classe::GetLoadParameters(DNBIAMHILoadParameters *& oLoadParameters) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&oLoadParameters); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetLoadParameters(oLoadParameters); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&oLoadParameters); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHISaveAccess##classe::GetDetailingNames(CATBSTR & oCurrentlyLoadedDetailing, CATSafeArrayVariant *& oListDetailingNames) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&oCurrentlyLoadedDetailing,&oListDetailingNames); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetDetailingNames(oCurrentlyLoadedDetailing,oListDetailingNames); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&oCurrentlyLoadedDetailing,&oListDetailingNames); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHISaveAccess##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHISaveAccess##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHISaveAccess##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHISaveAccess##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHISaveAccess##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIAMHISaveAccess(classe) \
 \
 \
declare_TIE_DNBIAMHISaveAccess(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAMHISaveAccess##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAMHISaveAccess,"DNBIAMHISaveAccess",DNBIAMHISaveAccess::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAMHISaveAccess(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIAMHISaveAccess, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAMHISaveAccess##classe(classe::MetaObject(),DNBIAMHISaveAccess::MetaObject(),(void *)CreateTIEDNBIAMHISaveAccess##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIAMHISaveAccess(classe) \
 \
 \
declare_TIE_DNBIAMHISaveAccess(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAMHISaveAccess##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAMHISaveAccess,"DNBIAMHISaveAccess",DNBIAMHISaveAccess::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAMHISaveAccess(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIAMHISaveAccess, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAMHISaveAccess##classe(classe::MetaObject(),DNBIAMHISaveAccess::MetaObject(),(void *)CreateTIEDNBIAMHISaveAccess##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIAMHISaveAccess(classe) TIE_DNBIAMHISaveAccess(classe)
#else
#define BOA_DNBIAMHISaveAccess(classe) CATImplementBOA(DNBIAMHISaveAccess, classe)
#endif

#endif
