#ifndef __TIE_DNBIAMHIReloadAccess
#define __TIE_DNBIAMHIReloadAccess

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIAMHIReloadAccess.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIAMHIReloadAccess */
#define declare_TIE_DNBIAMHIReloadAccess(classe) \
 \
 \
class TIEDNBIAMHIReloadAccess##classe : public DNBIAMHIReloadAccess \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIAMHIReloadAccess, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall SetFilters(const CATBSTR & iFilters, CATSafeArrayVariant *& oListErrorMessages); \
      virtual HRESULT __stdcall ReloadFromPPRHub(CATSafeArrayVariant *& oListErrorMessages); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIAMHIReloadAccess(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall SetFilters(const CATBSTR & iFilters, CATSafeArrayVariant *& oListErrorMessages); \
virtual HRESULT __stdcall ReloadFromPPRHub(CATSafeArrayVariant *& oListErrorMessages); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIAMHIReloadAccess(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::SetFilters(const CATBSTR & iFilters, CATSafeArrayVariant *& oListErrorMessages) \
{ \
return (ENVTIECALL(DNBIAMHIReloadAccess,ENVTIETypeLetter,ENVTIELetter)SetFilters(iFilters,oListErrorMessages)); \
} \
HRESULT __stdcall  ENVTIEName::ReloadFromPPRHub(CATSafeArrayVariant *& oListErrorMessages) \
{ \
return (ENVTIECALL(DNBIAMHIReloadAccess,ENVTIETypeLetter,ENVTIELetter)ReloadFromPPRHub(oListErrorMessages)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIAMHIReloadAccess,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIAMHIReloadAccess,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIAMHIReloadAccess,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIAMHIReloadAccess,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIAMHIReloadAccess,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIAMHIReloadAccess(classe)    TIEDNBIAMHIReloadAccess##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIAMHIReloadAccess(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIAMHIReloadAccess, classe) \
 \
 \
CATImplementTIEMethods(DNBIAMHIReloadAccess, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIAMHIReloadAccess, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIAMHIReloadAccess, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIAMHIReloadAccess, classe) \
 \
HRESULT __stdcall  TIEDNBIAMHIReloadAccess##classe::SetFilters(const CATBSTR & iFilters, CATSafeArrayVariant *& oListErrorMessages) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&iFilters,&oListErrorMessages); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetFilters(iFilters,oListErrorMessages); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&iFilters,&oListErrorMessages); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIReloadAccess##classe::ReloadFromPPRHub(CATSafeArrayVariant *& oListErrorMessages) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&oListErrorMessages); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ReloadFromPPRHub(oListErrorMessages); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&oListErrorMessages); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIReloadAccess##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIReloadAccess##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIReloadAccess##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIReloadAccess##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIReloadAccess##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIAMHIReloadAccess(classe) \
 \
 \
declare_TIE_DNBIAMHIReloadAccess(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAMHIReloadAccess##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAMHIReloadAccess,"DNBIAMHIReloadAccess",DNBIAMHIReloadAccess::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAMHIReloadAccess(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIAMHIReloadAccess, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAMHIReloadAccess##classe(classe::MetaObject(),DNBIAMHIReloadAccess::MetaObject(),(void *)CreateTIEDNBIAMHIReloadAccess##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIAMHIReloadAccess(classe) \
 \
 \
declare_TIE_DNBIAMHIReloadAccess(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAMHIReloadAccess##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAMHIReloadAccess,"DNBIAMHIReloadAccess",DNBIAMHIReloadAccess::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAMHIReloadAccess(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIAMHIReloadAccess, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAMHIReloadAccess##classe(classe::MetaObject(),DNBIAMHIReloadAccess::MetaObject(),(void *)CreateTIEDNBIAMHIReloadAccess##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIAMHIReloadAccess(classe) TIE_DNBIAMHIReloadAccess(classe)
#else
#define BOA_DNBIAMHIReloadAccess(classe) CATImplementBOA(DNBIAMHIReloadAccess, classe)
#endif

#endif
