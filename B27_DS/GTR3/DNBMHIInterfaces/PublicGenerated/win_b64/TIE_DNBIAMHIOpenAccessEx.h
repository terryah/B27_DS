#ifndef __TIE_DNBIAMHIOpenAccessEx
#define __TIE_DNBIAMHIOpenAccessEx

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIAMHIOpenAccessEx.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIAMHIOpenAccessEx */
#define declare_TIE_DNBIAMHIOpenAccessEx(classe) \
 \
 \
class TIEDNBIAMHIOpenAccessEx##classe : public DNBIAMHIOpenAccessEx \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIAMHIOpenAccessEx, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall ConnectToPPRHub(const CATBSTR & iUsername, const CATBSTR & iPassword); \
      virtual HRESULT __stdcall CreateLoadParameters(DNBIAMHILoadParameters *& oLoadParameters); \
      virtual HRESULT __stdcall LoadFromPPRHubEx(DNBIAMHILoadParameters * iLoadParameters, CAT_VARIANT_BOOL iCreateDefaultWindow, CAT_VARIANT_BOOL iIsReadOnly, CAT_VARIANT_BOOL iIsPartialReadOnly, CATSafeArrayVariant *& oErrorMessages, CATIADocument *& oLoadedDoc); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIAMHIOpenAccessEx(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall ConnectToPPRHub(const CATBSTR & iUsername, const CATBSTR & iPassword); \
virtual HRESULT __stdcall CreateLoadParameters(DNBIAMHILoadParameters *& oLoadParameters); \
virtual HRESULT __stdcall LoadFromPPRHubEx(DNBIAMHILoadParameters * iLoadParameters, CAT_VARIANT_BOOL iCreateDefaultWindow, CAT_VARIANT_BOOL iIsReadOnly, CAT_VARIANT_BOOL iIsPartialReadOnly, CATSafeArrayVariant *& oErrorMessages, CATIADocument *& oLoadedDoc); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIAMHIOpenAccessEx(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::ConnectToPPRHub(const CATBSTR & iUsername, const CATBSTR & iPassword) \
{ \
return (ENVTIECALL(DNBIAMHIOpenAccessEx,ENVTIETypeLetter,ENVTIELetter)ConnectToPPRHub(iUsername,iPassword)); \
} \
HRESULT __stdcall  ENVTIEName::CreateLoadParameters(DNBIAMHILoadParameters *& oLoadParameters) \
{ \
return (ENVTIECALL(DNBIAMHIOpenAccessEx,ENVTIETypeLetter,ENVTIELetter)CreateLoadParameters(oLoadParameters)); \
} \
HRESULT __stdcall  ENVTIEName::LoadFromPPRHubEx(DNBIAMHILoadParameters * iLoadParameters, CAT_VARIANT_BOOL iCreateDefaultWindow, CAT_VARIANT_BOOL iIsReadOnly, CAT_VARIANT_BOOL iIsPartialReadOnly, CATSafeArrayVariant *& oErrorMessages, CATIADocument *& oLoadedDoc) \
{ \
return (ENVTIECALL(DNBIAMHIOpenAccessEx,ENVTIETypeLetter,ENVTIELetter)LoadFromPPRHubEx(iLoadParameters,iCreateDefaultWindow,iIsReadOnly,iIsPartialReadOnly,oErrorMessages,oLoadedDoc)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIAMHIOpenAccessEx,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIAMHIOpenAccessEx,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIAMHIOpenAccessEx,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIAMHIOpenAccessEx,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIAMHIOpenAccessEx,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIAMHIOpenAccessEx(classe)    TIEDNBIAMHIOpenAccessEx##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIAMHIOpenAccessEx(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIAMHIOpenAccessEx, classe) \
 \
 \
CATImplementTIEMethods(DNBIAMHIOpenAccessEx, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIAMHIOpenAccessEx, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIAMHIOpenAccessEx, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIAMHIOpenAccessEx, classe) \
 \
HRESULT __stdcall  TIEDNBIAMHIOpenAccessEx##classe::ConnectToPPRHub(const CATBSTR & iUsername, const CATBSTR & iPassword) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&iUsername,&iPassword); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ConnectToPPRHub(iUsername,iPassword); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&iUsername,&iPassword); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIOpenAccessEx##classe::CreateLoadParameters(DNBIAMHILoadParameters *& oLoadParameters) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&oLoadParameters); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateLoadParameters(oLoadParameters); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&oLoadParameters); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIOpenAccessEx##classe::LoadFromPPRHubEx(DNBIAMHILoadParameters * iLoadParameters, CAT_VARIANT_BOOL iCreateDefaultWindow, CAT_VARIANT_BOOL iIsReadOnly, CAT_VARIANT_BOOL iIsPartialReadOnly, CATSafeArrayVariant *& oErrorMessages, CATIADocument *& oLoadedDoc) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&iLoadParameters,&iCreateDefaultWindow,&iIsReadOnly,&iIsPartialReadOnly,&oErrorMessages,&oLoadedDoc); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->LoadFromPPRHubEx(iLoadParameters,iCreateDefaultWindow,iIsReadOnly,iIsPartialReadOnly,oErrorMessages,oLoadedDoc); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&iLoadParameters,&iCreateDefaultWindow,&iIsReadOnly,&iIsPartialReadOnly,&oErrorMessages,&oLoadedDoc); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIOpenAccessEx##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIOpenAccessEx##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIOpenAccessEx##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIOpenAccessEx##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIOpenAccessEx##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIAMHIOpenAccessEx(classe) \
 \
 \
declare_TIE_DNBIAMHIOpenAccessEx(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAMHIOpenAccessEx##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAMHIOpenAccessEx,"DNBIAMHIOpenAccessEx",DNBIAMHIOpenAccessEx::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAMHIOpenAccessEx(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIAMHIOpenAccessEx, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAMHIOpenAccessEx##classe(classe::MetaObject(),DNBIAMHIOpenAccessEx::MetaObject(),(void *)CreateTIEDNBIAMHIOpenAccessEx##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIAMHIOpenAccessEx(classe) \
 \
 \
declare_TIE_DNBIAMHIOpenAccessEx(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAMHIOpenAccessEx##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAMHIOpenAccessEx,"DNBIAMHIOpenAccessEx",DNBIAMHIOpenAccessEx::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAMHIOpenAccessEx(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIAMHIOpenAccessEx, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAMHIOpenAccessEx##classe(classe::MetaObject(),DNBIAMHIOpenAccessEx::MetaObject(),(void *)CreateTIEDNBIAMHIOpenAccessEx##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIAMHIOpenAccessEx(classe) TIE_DNBIAMHIOpenAccessEx(classe)
#else
#define BOA_DNBIAMHIOpenAccessEx(classe) CATImplementBOA(DNBIAMHIOpenAccessEx, classe)
#endif

#endif
