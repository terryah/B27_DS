#ifndef __TIE_DNBIAMHILoadParameters
#define __TIE_DNBIAMHILoadParameters

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIAMHILoadParameters.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIAMHILoadParameters */
#define declare_TIE_DNBIAMHILoadParameters(classe) \
 \
 \
class TIEDNBIAMHILoadParameters##classe : public DNBIAMHILoadParameters \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIAMHILoadParameters, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall GetSelectedObjectID(CATBSTR & oSelectedObjectID); \
      virtual HRESULT __stdcall GetBranchIDs(CATSafeArrayVariant *& oListBranchIDs); \
      virtual HRESULT __stdcall GetDetailingID(CATBSTR & oDetailingID); \
      virtual HRESULT __stdcall GetProductBOMID(CATBSTR & oProductBOMID); \
      virtual HRESULT __stdcall GetResourceTreeID(CATBSTR & oResourceTreeID); \
      virtual HRESULT __stdcall GetProcessTreeID(CATBSTR & oProcessTreeID); \
      virtual HRESULT __stdcall SetSelectedObjectID(const CATBSTR & iSelectedObjectID); \
      virtual HRESULT __stdcall SetBranchIDs(const CATSafeArrayVariant & iListBranchIDs); \
      virtual HRESULT __stdcall SetDetailingID(const CATBSTR & iDetailingID); \
      virtual HRESULT __stdcall SetProductBOMID(const CATBSTR & iProductBOMID); \
      virtual HRESULT __stdcall SetResourceTreeID(const CATBSTR & iResourceTreeID); \
      virtual HRESULT __stdcall SetProcessTreeID(const CATBSTR & iProcessTreeID); \
      virtual HRESULT __stdcall SetFilters(const CATBSTR & iFilters); \
      virtual HRESULT __stdcall GetFilters(CATBSTR & oFilters); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIAMHILoadParameters(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall GetSelectedObjectID(CATBSTR & oSelectedObjectID); \
virtual HRESULT __stdcall GetBranchIDs(CATSafeArrayVariant *& oListBranchIDs); \
virtual HRESULT __stdcall GetDetailingID(CATBSTR & oDetailingID); \
virtual HRESULT __stdcall GetProductBOMID(CATBSTR & oProductBOMID); \
virtual HRESULT __stdcall GetResourceTreeID(CATBSTR & oResourceTreeID); \
virtual HRESULT __stdcall GetProcessTreeID(CATBSTR & oProcessTreeID); \
virtual HRESULT __stdcall SetSelectedObjectID(const CATBSTR & iSelectedObjectID); \
virtual HRESULT __stdcall SetBranchIDs(const CATSafeArrayVariant & iListBranchIDs); \
virtual HRESULT __stdcall SetDetailingID(const CATBSTR & iDetailingID); \
virtual HRESULT __stdcall SetProductBOMID(const CATBSTR & iProductBOMID); \
virtual HRESULT __stdcall SetResourceTreeID(const CATBSTR & iResourceTreeID); \
virtual HRESULT __stdcall SetProcessTreeID(const CATBSTR & iProcessTreeID); \
virtual HRESULT __stdcall SetFilters(const CATBSTR & iFilters); \
virtual HRESULT __stdcall GetFilters(CATBSTR & oFilters); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIAMHILoadParameters(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::GetSelectedObjectID(CATBSTR & oSelectedObjectID) \
{ \
return (ENVTIECALL(DNBIAMHILoadParameters,ENVTIETypeLetter,ENVTIELetter)GetSelectedObjectID(oSelectedObjectID)); \
} \
HRESULT __stdcall  ENVTIEName::GetBranchIDs(CATSafeArrayVariant *& oListBranchIDs) \
{ \
return (ENVTIECALL(DNBIAMHILoadParameters,ENVTIETypeLetter,ENVTIELetter)GetBranchIDs(oListBranchIDs)); \
} \
HRESULT __stdcall  ENVTIEName::GetDetailingID(CATBSTR & oDetailingID) \
{ \
return (ENVTIECALL(DNBIAMHILoadParameters,ENVTIETypeLetter,ENVTIELetter)GetDetailingID(oDetailingID)); \
} \
HRESULT __stdcall  ENVTIEName::GetProductBOMID(CATBSTR & oProductBOMID) \
{ \
return (ENVTIECALL(DNBIAMHILoadParameters,ENVTIETypeLetter,ENVTIELetter)GetProductBOMID(oProductBOMID)); \
} \
HRESULT __stdcall  ENVTIEName::GetResourceTreeID(CATBSTR & oResourceTreeID) \
{ \
return (ENVTIECALL(DNBIAMHILoadParameters,ENVTIETypeLetter,ENVTIELetter)GetResourceTreeID(oResourceTreeID)); \
} \
HRESULT __stdcall  ENVTIEName::GetProcessTreeID(CATBSTR & oProcessTreeID) \
{ \
return (ENVTIECALL(DNBIAMHILoadParameters,ENVTIETypeLetter,ENVTIELetter)GetProcessTreeID(oProcessTreeID)); \
} \
HRESULT __stdcall  ENVTIEName::SetSelectedObjectID(const CATBSTR & iSelectedObjectID) \
{ \
return (ENVTIECALL(DNBIAMHILoadParameters,ENVTIETypeLetter,ENVTIELetter)SetSelectedObjectID(iSelectedObjectID)); \
} \
HRESULT __stdcall  ENVTIEName::SetBranchIDs(const CATSafeArrayVariant & iListBranchIDs) \
{ \
return (ENVTIECALL(DNBIAMHILoadParameters,ENVTIETypeLetter,ENVTIELetter)SetBranchIDs(iListBranchIDs)); \
} \
HRESULT __stdcall  ENVTIEName::SetDetailingID(const CATBSTR & iDetailingID) \
{ \
return (ENVTIECALL(DNBIAMHILoadParameters,ENVTIETypeLetter,ENVTIELetter)SetDetailingID(iDetailingID)); \
} \
HRESULT __stdcall  ENVTIEName::SetProductBOMID(const CATBSTR & iProductBOMID) \
{ \
return (ENVTIECALL(DNBIAMHILoadParameters,ENVTIETypeLetter,ENVTIELetter)SetProductBOMID(iProductBOMID)); \
} \
HRESULT __stdcall  ENVTIEName::SetResourceTreeID(const CATBSTR & iResourceTreeID) \
{ \
return (ENVTIECALL(DNBIAMHILoadParameters,ENVTIETypeLetter,ENVTIELetter)SetResourceTreeID(iResourceTreeID)); \
} \
HRESULT __stdcall  ENVTIEName::SetProcessTreeID(const CATBSTR & iProcessTreeID) \
{ \
return (ENVTIECALL(DNBIAMHILoadParameters,ENVTIETypeLetter,ENVTIELetter)SetProcessTreeID(iProcessTreeID)); \
} \
HRESULT __stdcall  ENVTIEName::SetFilters(const CATBSTR & iFilters) \
{ \
return (ENVTIECALL(DNBIAMHILoadParameters,ENVTIETypeLetter,ENVTIELetter)SetFilters(iFilters)); \
} \
HRESULT __stdcall  ENVTIEName::GetFilters(CATBSTR & oFilters) \
{ \
return (ENVTIECALL(DNBIAMHILoadParameters,ENVTIETypeLetter,ENVTIELetter)GetFilters(oFilters)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIAMHILoadParameters,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIAMHILoadParameters,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIAMHILoadParameters,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIAMHILoadParameters,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIAMHILoadParameters,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIAMHILoadParameters(classe)    TIEDNBIAMHILoadParameters##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIAMHILoadParameters(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIAMHILoadParameters, classe) \
 \
 \
CATImplementTIEMethods(DNBIAMHILoadParameters, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIAMHILoadParameters, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIAMHILoadParameters, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIAMHILoadParameters, classe) \
 \
HRESULT __stdcall  TIEDNBIAMHILoadParameters##classe::GetSelectedObjectID(CATBSTR & oSelectedObjectID) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&oSelectedObjectID); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetSelectedObjectID(oSelectedObjectID); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&oSelectedObjectID); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHILoadParameters##classe::GetBranchIDs(CATSafeArrayVariant *& oListBranchIDs) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&oListBranchIDs); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetBranchIDs(oListBranchIDs); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&oListBranchIDs); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHILoadParameters##classe::GetDetailingID(CATBSTR & oDetailingID) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&oDetailingID); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetDetailingID(oDetailingID); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&oDetailingID); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHILoadParameters##classe::GetProductBOMID(CATBSTR & oProductBOMID) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&oProductBOMID); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetProductBOMID(oProductBOMID); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&oProductBOMID); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHILoadParameters##classe::GetResourceTreeID(CATBSTR & oResourceTreeID) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&oResourceTreeID); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetResourceTreeID(oResourceTreeID); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&oResourceTreeID); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHILoadParameters##classe::GetProcessTreeID(CATBSTR & oProcessTreeID) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&oProcessTreeID); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetProcessTreeID(oProcessTreeID); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&oProcessTreeID); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHILoadParameters##classe::SetSelectedObjectID(const CATBSTR & iSelectedObjectID) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&iSelectedObjectID); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetSelectedObjectID(iSelectedObjectID); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&iSelectedObjectID); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHILoadParameters##classe::SetBranchIDs(const CATSafeArrayVariant & iListBranchIDs) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&iListBranchIDs); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetBranchIDs(iListBranchIDs); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&iListBranchIDs); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHILoadParameters##classe::SetDetailingID(const CATBSTR & iDetailingID) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&iDetailingID); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetDetailingID(iDetailingID); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&iDetailingID); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHILoadParameters##classe::SetProductBOMID(const CATBSTR & iProductBOMID) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&iProductBOMID); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetProductBOMID(iProductBOMID); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&iProductBOMID); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHILoadParameters##classe::SetResourceTreeID(const CATBSTR & iResourceTreeID) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,11,&_Trac2,&iResourceTreeID); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetResourceTreeID(iResourceTreeID); \
   ExitAfterCall(this,11,_Trac2,&_ret_arg,&iResourceTreeID); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHILoadParameters##classe::SetProcessTreeID(const CATBSTR & iProcessTreeID) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,12,&_Trac2,&iProcessTreeID); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetProcessTreeID(iProcessTreeID); \
   ExitAfterCall(this,12,_Trac2,&_ret_arg,&iProcessTreeID); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHILoadParameters##classe::SetFilters(const CATBSTR & iFilters) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,13,&_Trac2,&iFilters); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetFilters(iFilters); \
   ExitAfterCall(this,13,_Trac2,&_ret_arg,&iFilters); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHILoadParameters##classe::GetFilters(CATBSTR & oFilters) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,14,&_Trac2,&oFilters); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetFilters(oFilters); \
   ExitAfterCall(this,14,_Trac2,&_ret_arg,&oFilters); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHILoadParameters##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,15,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,15,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHILoadParameters##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,16,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,16,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHILoadParameters##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,17,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,17,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHILoadParameters##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,18,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,18,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHILoadParameters##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,19,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,19,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIAMHILoadParameters(classe) \
 \
 \
declare_TIE_DNBIAMHILoadParameters(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAMHILoadParameters##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAMHILoadParameters,"DNBIAMHILoadParameters",DNBIAMHILoadParameters::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAMHILoadParameters(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIAMHILoadParameters, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAMHILoadParameters##classe(classe::MetaObject(),DNBIAMHILoadParameters::MetaObject(),(void *)CreateTIEDNBIAMHILoadParameters##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIAMHILoadParameters(classe) \
 \
 \
declare_TIE_DNBIAMHILoadParameters(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAMHILoadParameters##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAMHILoadParameters,"DNBIAMHILoadParameters",DNBIAMHILoadParameters::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAMHILoadParameters(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIAMHILoadParameters, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAMHILoadParameters##classe(classe::MetaObject(),DNBIAMHILoadParameters::MetaObject(),(void *)CreateTIEDNBIAMHILoadParameters##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIAMHILoadParameters(classe) TIE_DNBIAMHILoadParameters(classe)
#else
#define BOA_DNBIAMHILoadParameters(classe) CATImplementBOA(DNBIAMHILoadParameters, classe)
#endif

#endif
