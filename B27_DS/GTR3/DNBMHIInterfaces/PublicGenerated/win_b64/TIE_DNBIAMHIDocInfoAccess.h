#ifndef __TIE_DNBIAMHIDocInfoAccess
#define __TIE_DNBIAMHIDocInfoAccess

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIAMHIDocInfoAccess.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIAMHIDocInfoAccess */
#define declare_TIE_DNBIAMHIDocInfoAccess(classe) \
 \
 \
class TIEDNBIAMHIDocInfoAccess##classe : public DNBIAMHIDocInfoAccess \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIAMHIDocInfoAccess, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall GetDocumentInfo(CATSafeArrayVariant *& oListDocDetails, CATSafeArrayVariant *& oListIDs); \
      virtual HRESULT __stdcall GetFilterInfo(CATSafeArrayVariant *& oListFilterDetails, CATSafeArrayVariant *& oListFilterIDs); \
      virtual HRESULT __stdcall GetBeginDate(const CATBSTR & iFilterSetName, CATBSTR & oBeginDate); \
      virtual HRESULT __stdcall GetEndDate(const CATBSTR & iFilterSetName, CATBSTR & oEndDate); \
      virtual HRESULT __stdcall GetLabel(const CATBSTR & iFilterSetName, CATBSTR & oLabel); \
      virtual HRESULT __stdcall GetLineNumber(const CATBSTR & iFilterSetName, CATBSTR & oLineNumber); \
      virtual HRESULT __stdcall GetExtEffectivityFilter(const CATBSTR & iFilterSetName, CATBSTR & oExtEffFilter, CATBSTR & oExtEffType); \
      virtual HRESULT __stdcall GetExtEffAddnlFilter(const CATBSTR & iFilterSetName, CATBSTR & oExtEffAddnl); \
      virtual HRESULT __stdcall GetComponentFilter(const CATBSTR & iFilterSetName, CATBSTR & oCompFilter, CATBSTR & oCompFltrType); \
      virtual HRESULT __stdcall GetAttributeFilters(const CATBSTR & iFilterSetName, CATSafeArrayVariant *& oListAttrFilterSpec); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIAMHIDocInfoAccess(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall GetDocumentInfo(CATSafeArrayVariant *& oListDocDetails, CATSafeArrayVariant *& oListIDs); \
virtual HRESULT __stdcall GetFilterInfo(CATSafeArrayVariant *& oListFilterDetails, CATSafeArrayVariant *& oListFilterIDs); \
virtual HRESULT __stdcall GetBeginDate(const CATBSTR & iFilterSetName, CATBSTR & oBeginDate); \
virtual HRESULT __stdcall GetEndDate(const CATBSTR & iFilterSetName, CATBSTR & oEndDate); \
virtual HRESULT __stdcall GetLabel(const CATBSTR & iFilterSetName, CATBSTR & oLabel); \
virtual HRESULT __stdcall GetLineNumber(const CATBSTR & iFilterSetName, CATBSTR & oLineNumber); \
virtual HRESULT __stdcall GetExtEffectivityFilter(const CATBSTR & iFilterSetName, CATBSTR & oExtEffFilter, CATBSTR & oExtEffType); \
virtual HRESULT __stdcall GetExtEffAddnlFilter(const CATBSTR & iFilterSetName, CATBSTR & oExtEffAddnl); \
virtual HRESULT __stdcall GetComponentFilter(const CATBSTR & iFilterSetName, CATBSTR & oCompFilter, CATBSTR & oCompFltrType); \
virtual HRESULT __stdcall GetAttributeFilters(const CATBSTR & iFilterSetName, CATSafeArrayVariant *& oListAttrFilterSpec); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIAMHIDocInfoAccess(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::GetDocumentInfo(CATSafeArrayVariant *& oListDocDetails, CATSafeArrayVariant *& oListIDs) \
{ \
return (ENVTIECALL(DNBIAMHIDocInfoAccess,ENVTIETypeLetter,ENVTIELetter)GetDocumentInfo(oListDocDetails,oListIDs)); \
} \
HRESULT __stdcall  ENVTIEName::GetFilterInfo(CATSafeArrayVariant *& oListFilterDetails, CATSafeArrayVariant *& oListFilterIDs) \
{ \
return (ENVTIECALL(DNBIAMHIDocInfoAccess,ENVTIETypeLetter,ENVTIELetter)GetFilterInfo(oListFilterDetails,oListFilterIDs)); \
} \
HRESULT __stdcall  ENVTIEName::GetBeginDate(const CATBSTR & iFilterSetName, CATBSTR & oBeginDate) \
{ \
return (ENVTIECALL(DNBIAMHIDocInfoAccess,ENVTIETypeLetter,ENVTIELetter)GetBeginDate(iFilterSetName,oBeginDate)); \
} \
HRESULT __stdcall  ENVTIEName::GetEndDate(const CATBSTR & iFilterSetName, CATBSTR & oEndDate) \
{ \
return (ENVTIECALL(DNBIAMHIDocInfoAccess,ENVTIETypeLetter,ENVTIELetter)GetEndDate(iFilterSetName,oEndDate)); \
} \
HRESULT __stdcall  ENVTIEName::GetLabel(const CATBSTR & iFilterSetName, CATBSTR & oLabel) \
{ \
return (ENVTIECALL(DNBIAMHIDocInfoAccess,ENVTIETypeLetter,ENVTIELetter)GetLabel(iFilterSetName,oLabel)); \
} \
HRESULT __stdcall  ENVTIEName::GetLineNumber(const CATBSTR & iFilterSetName, CATBSTR & oLineNumber) \
{ \
return (ENVTIECALL(DNBIAMHIDocInfoAccess,ENVTIETypeLetter,ENVTIELetter)GetLineNumber(iFilterSetName,oLineNumber)); \
} \
HRESULT __stdcall  ENVTIEName::GetExtEffectivityFilter(const CATBSTR & iFilterSetName, CATBSTR & oExtEffFilter, CATBSTR & oExtEffType) \
{ \
return (ENVTIECALL(DNBIAMHIDocInfoAccess,ENVTIETypeLetter,ENVTIELetter)GetExtEffectivityFilter(iFilterSetName,oExtEffFilter,oExtEffType)); \
} \
HRESULT __stdcall  ENVTIEName::GetExtEffAddnlFilter(const CATBSTR & iFilterSetName, CATBSTR & oExtEffAddnl) \
{ \
return (ENVTIECALL(DNBIAMHIDocInfoAccess,ENVTIETypeLetter,ENVTIELetter)GetExtEffAddnlFilter(iFilterSetName,oExtEffAddnl)); \
} \
HRESULT __stdcall  ENVTIEName::GetComponentFilter(const CATBSTR & iFilterSetName, CATBSTR & oCompFilter, CATBSTR & oCompFltrType) \
{ \
return (ENVTIECALL(DNBIAMHIDocInfoAccess,ENVTIETypeLetter,ENVTIELetter)GetComponentFilter(iFilterSetName,oCompFilter,oCompFltrType)); \
} \
HRESULT __stdcall  ENVTIEName::GetAttributeFilters(const CATBSTR & iFilterSetName, CATSafeArrayVariant *& oListAttrFilterSpec) \
{ \
return (ENVTIECALL(DNBIAMHIDocInfoAccess,ENVTIETypeLetter,ENVTIELetter)GetAttributeFilters(iFilterSetName,oListAttrFilterSpec)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIAMHIDocInfoAccess,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIAMHIDocInfoAccess,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIAMHIDocInfoAccess,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIAMHIDocInfoAccess,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIAMHIDocInfoAccess,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIAMHIDocInfoAccess(classe)    TIEDNBIAMHIDocInfoAccess##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIAMHIDocInfoAccess(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIAMHIDocInfoAccess, classe) \
 \
 \
CATImplementTIEMethods(DNBIAMHIDocInfoAccess, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIAMHIDocInfoAccess, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIAMHIDocInfoAccess, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIAMHIDocInfoAccess, classe) \
 \
HRESULT __stdcall  TIEDNBIAMHIDocInfoAccess##classe::GetDocumentInfo(CATSafeArrayVariant *& oListDocDetails, CATSafeArrayVariant *& oListIDs) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&oListDocDetails,&oListIDs); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetDocumentInfo(oListDocDetails,oListIDs); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&oListDocDetails,&oListIDs); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIDocInfoAccess##classe::GetFilterInfo(CATSafeArrayVariant *& oListFilterDetails, CATSafeArrayVariant *& oListFilterIDs) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&oListFilterDetails,&oListFilterIDs); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetFilterInfo(oListFilterDetails,oListFilterIDs); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&oListFilterDetails,&oListFilterIDs); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIDocInfoAccess##classe::GetBeginDate(const CATBSTR & iFilterSetName, CATBSTR & oBeginDate) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&iFilterSetName,&oBeginDate); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetBeginDate(iFilterSetName,oBeginDate); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&iFilterSetName,&oBeginDate); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIDocInfoAccess##classe::GetEndDate(const CATBSTR & iFilterSetName, CATBSTR & oEndDate) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&iFilterSetName,&oEndDate); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetEndDate(iFilterSetName,oEndDate); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&iFilterSetName,&oEndDate); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIDocInfoAccess##classe::GetLabel(const CATBSTR & iFilterSetName, CATBSTR & oLabel) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&iFilterSetName,&oLabel); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetLabel(iFilterSetName,oLabel); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&iFilterSetName,&oLabel); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIDocInfoAccess##classe::GetLineNumber(const CATBSTR & iFilterSetName, CATBSTR & oLineNumber) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&iFilterSetName,&oLineNumber); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetLineNumber(iFilterSetName,oLineNumber); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&iFilterSetName,&oLineNumber); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIDocInfoAccess##classe::GetExtEffectivityFilter(const CATBSTR & iFilterSetName, CATBSTR & oExtEffFilter, CATBSTR & oExtEffType) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&iFilterSetName,&oExtEffFilter,&oExtEffType); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetExtEffectivityFilter(iFilterSetName,oExtEffFilter,oExtEffType); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&iFilterSetName,&oExtEffFilter,&oExtEffType); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIDocInfoAccess##classe::GetExtEffAddnlFilter(const CATBSTR & iFilterSetName, CATBSTR & oExtEffAddnl) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&iFilterSetName,&oExtEffAddnl); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetExtEffAddnlFilter(iFilterSetName,oExtEffAddnl); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&iFilterSetName,&oExtEffAddnl); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIDocInfoAccess##classe::GetComponentFilter(const CATBSTR & iFilterSetName, CATBSTR & oCompFilter, CATBSTR & oCompFltrType) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&iFilterSetName,&oCompFilter,&oCompFltrType); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetComponentFilter(iFilterSetName,oCompFilter,oCompFltrType); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&iFilterSetName,&oCompFilter,&oCompFltrType); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIDocInfoAccess##classe::GetAttributeFilters(const CATBSTR & iFilterSetName, CATSafeArrayVariant *& oListAttrFilterSpec) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&iFilterSetName,&oListAttrFilterSpec); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAttributeFilters(iFilterSetName,oListAttrFilterSpec); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&iFilterSetName,&oListAttrFilterSpec); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIDocInfoAccess##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,11,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,11,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIDocInfoAccess##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,12,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,12,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIDocInfoAccess##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,13,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,13,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIDocInfoAccess##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,14,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,14,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIDocInfoAccess##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,15,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,15,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIAMHIDocInfoAccess(classe) \
 \
 \
declare_TIE_DNBIAMHIDocInfoAccess(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAMHIDocInfoAccess##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAMHIDocInfoAccess,"DNBIAMHIDocInfoAccess",DNBIAMHIDocInfoAccess::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAMHIDocInfoAccess(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIAMHIDocInfoAccess, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAMHIDocInfoAccess##classe(classe::MetaObject(),DNBIAMHIDocInfoAccess::MetaObject(),(void *)CreateTIEDNBIAMHIDocInfoAccess##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIAMHIDocInfoAccess(classe) \
 \
 \
declare_TIE_DNBIAMHIDocInfoAccess(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAMHIDocInfoAccess##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAMHIDocInfoAccess,"DNBIAMHIDocInfoAccess",DNBIAMHIDocInfoAccess::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAMHIDocInfoAccess(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIAMHIDocInfoAccess, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAMHIDocInfoAccess##classe(classe::MetaObject(),DNBIAMHIDocInfoAccess::MetaObject(),(void *)CreateTIEDNBIAMHIDocInfoAccess##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIAMHIDocInfoAccess(classe) TIE_DNBIAMHIDocInfoAccess(classe)
#else
#define BOA_DNBIAMHIDocInfoAccess(classe) CATImplementBOA(DNBIAMHIDocInfoAccess, classe)
#endif

#endif
