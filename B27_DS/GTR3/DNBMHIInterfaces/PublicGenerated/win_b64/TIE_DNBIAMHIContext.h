#ifndef __TIE_DNBIAMHIContext
#define __TIE_DNBIAMHIContext

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIAMHIContext.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIAMHIContext */
#define declare_TIE_DNBIAMHIContext(classe) \
 \
 \
class TIEDNBIAMHIContext##classe : public DNBIAMHIContext \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIAMHIContext, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall LoadContext(const CATBSTR & iContextName, DNBMHIContextLoadOption iLoadOption, CATSafeArrayVariant *& oListErrorMessages); \
      virtual HRESULT __stdcall CreateUserContext(const CATBSTR & iRefProcessID, const CATBSTR & iContextName, const CATBSTR & iUserCtxDescription, const CATSafeArrayVariant & iListContextObjectIDs, CATSafeArrayVariant *& oListErrorMessages); \
      virtual HRESULT __stdcall SetNewDocumentLoadMode(CAT_VARIANT_BOOL iNewDocLoadMode); \
      virtual HRESULT __stdcall SetLoadParameters(DNBIAMHILoadParameters * iLoadParameters); \
      virtual HRESULT __stdcall IsContextLoadedFromParent(CAT_VARIANT_BOOL & oLoadedFromParent); \
      virtual HRESULT __stdcall DisableSaveOfContext(CAT_VARIANT_BOOL iDisableContextSave); \
      virtual HRESULT __stdcall CreateContextDoc(CATSafeArrayVariant *& oErrorMessages, CATIADocument *& oLoadedDoc); \
      virtual HRESULT __stdcall AddToUserContext(const CATSafeArrayVariant & iListContextObjectIDs, CATSafeArrayVariant *& oListErrorMessages); \
      virtual HRESULT __stdcall RemoveFromUserContext(const CATSafeArrayVariant & iListContextObjectIDs, CATSafeArrayVariant *& oListErrorMessages); \
      virtual HRESULT __stdcall UpdateUserContext(const CATSafeArrayVariant & iListOfOldContextObjectIDs, const CATSafeArrayVariant & iListOfNewContextObjectIDs, CATSafeArrayVariant *& oListErrorMessages); \
      virtual HRESULT __stdcall DeleteUserContext(const CATBSTR & iRefProcessID, const CATBSTR & iContextName, CATSafeArrayVariant *& oListErrorMessages); \
      virtual HRESULT __stdcall GetUserContextList(const CATBSTR & iRefProcessID, CATSafeArrayVariant *& oListOfUserContextNames, CATSafeArrayVariant *& oListErrorMessages); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIAMHIContext(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall LoadContext(const CATBSTR & iContextName, DNBMHIContextLoadOption iLoadOption, CATSafeArrayVariant *& oListErrorMessages); \
virtual HRESULT __stdcall CreateUserContext(const CATBSTR & iRefProcessID, const CATBSTR & iContextName, const CATBSTR & iUserCtxDescription, const CATSafeArrayVariant & iListContextObjectIDs, CATSafeArrayVariant *& oListErrorMessages); \
virtual HRESULT __stdcall SetNewDocumentLoadMode(CAT_VARIANT_BOOL iNewDocLoadMode); \
virtual HRESULT __stdcall SetLoadParameters(DNBIAMHILoadParameters * iLoadParameters); \
virtual HRESULT __stdcall IsContextLoadedFromParent(CAT_VARIANT_BOOL & oLoadedFromParent); \
virtual HRESULT __stdcall DisableSaveOfContext(CAT_VARIANT_BOOL iDisableContextSave); \
virtual HRESULT __stdcall CreateContextDoc(CATSafeArrayVariant *& oErrorMessages, CATIADocument *& oLoadedDoc); \
virtual HRESULT __stdcall AddToUserContext(const CATSafeArrayVariant & iListContextObjectIDs, CATSafeArrayVariant *& oListErrorMessages); \
virtual HRESULT __stdcall RemoveFromUserContext(const CATSafeArrayVariant & iListContextObjectIDs, CATSafeArrayVariant *& oListErrorMessages); \
virtual HRESULT __stdcall UpdateUserContext(const CATSafeArrayVariant & iListOfOldContextObjectIDs, const CATSafeArrayVariant & iListOfNewContextObjectIDs, CATSafeArrayVariant *& oListErrorMessages); \
virtual HRESULT __stdcall DeleteUserContext(const CATBSTR & iRefProcessID, const CATBSTR & iContextName, CATSafeArrayVariant *& oListErrorMessages); \
virtual HRESULT __stdcall GetUserContextList(const CATBSTR & iRefProcessID, CATSafeArrayVariant *& oListOfUserContextNames, CATSafeArrayVariant *& oListErrorMessages); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIAMHIContext(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::LoadContext(const CATBSTR & iContextName, DNBMHIContextLoadOption iLoadOption, CATSafeArrayVariant *& oListErrorMessages) \
{ \
return (ENVTIECALL(DNBIAMHIContext,ENVTIETypeLetter,ENVTIELetter)LoadContext(iContextName,iLoadOption,oListErrorMessages)); \
} \
HRESULT __stdcall  ENVTIEName::CreateUserContext(const CATBSTR & iRefProcessID, const CATBSTR & iContextName, const CATBSTR & iUserCtxDescription, const CATSafeArrayVariant & iListContextObjectIDs, CATSafeArrayVariant *& oListErrorMessages) \
{ \
return (ENVTIECALL(DNBIAMHIContext,ENVTIETypeLetter,ENVTIELetter)CreateUserContext(iRefProcessID,iContextName,iUserCtxDescription,iListContextObjectIDs,oListErrorMessages)); \
} \
HRESULT __stdcall  ENVTIEName::SetNewDocumentLoadMode(CAT_VARIANT_BOOL iNewDocLoadMode) \
{ \
return (ENVTIECALL(DNBIAMHIContext,ENVTIETypeLetter,ENVTIELetter)SetNewDocumentLoadMode(iNewDocLoadMode)); \
} \
HRESULT __stdcall  ENVTIEName::SetLoadParameters(DNBIAMHILoadParameters * iLoadParameters) \
{ \
return (ENVTIECALL(DNBIAMHIContext,ENVTIETypeLetter,ENVTIELetter)SetLoadParameters(iLoadParameters)); \
} \
HRESULT __stdcall  ENVTIEName::IsContextLoadedFromParent(CAT_VARIANT_BOOL & oLoadedFromParent) \
{ \
return (ENVTIECALL(DNBIAMHIContext,ENVTIETypeLetter,ENVTIELetter)IsContextLoadedFromParent(oLoadedFromParent)); \
} \
HRESULT __stdcall  ENVTIEName::DisableSaveOfContext(CAT_VARIANT_BOOL iDisableContextSave) \
{ \
return (ENVTIECALL(DNBIAMHIContext,ENVTIETypeLetter,ENVTIELetter)DisableSaveOfContext(iDisableContextSave)); \
} \
HRESULT __stdcall  ENVTIEName::CreateContextDoc(CATSafeArrayVariant *& oErrorMessages, CATIADocument *& oLoadedDoc) \
{ \
return (ENVTIECALL(DNBIAMHIContext,ENVTIETypeLetter,ENVTIELetter)CreateContextDoc(oErrorMessages,oLoadedDoc)); \
} \
HRESULT __stdcall  ENVTIEName::AddToUserContext(const CATSafeArrayVariant & iListContextObjectIDs, CATSafeArrayVariant *& oListErrorMessages) \
{ \
return (ENVTIECALL(DNBIAMHIContext,ENVTIETypeLetter,ENVTIELetter)AddToUserContext(iListContextObjectIDs,oListErrorMessages)); \
} \
HRESULT __stdcall  ENVTIEName::RemoveFromUserContext(const CATSafeArrayVariant & iListContextObjectIDs, CATSafeArrayVariant *& oListErrorMessages) \
{ \
return (ENVTIECALL(DNBIAMHIContext,ENVTIETypeLetter,ENVTIELetter)RemoveFromUserContext(iListContextObjectIDs,oListErrorMessages)); \
} \
HRESULT __stdcall  ENVTIEName::UpdateUserContext(const CATSafeArrayVariant & iListOfOldContextObjectIDs, const CATSafeArrayVariant & iListOfNewContextObjectIDs, CATSafeArrayVariant *& oListErrorMessages) \
{ \
return (ENVTIECALL(DNBIAMHIContext,ENVTIETypeLetter,ENVTIELetter)UpdateUserContext(iListOfOldContextObjectIDs,iListOfNewContextObjectIDs,oListErrorMessages)); \
} \
HRESULT __stdcall  ENVTIEName::DeleteUserContext(const CATBSTR & iRefProcessID, const CATBSTR & iContextName, CATSafeArrayVariant *& oListErrorMessages) \
{ \
return (ENVTIECALL(DNBIAMHIContext,ENVTIETypeLetter,ENVTIELetter)DeleteUserContext(iRefProcessID,iContextName,oListErrorMessages)); \
} \
HRESULT __stdcall  ENVTIEName::GetUserContextList(const CATBSTR & iRefProcessID, CATSafeArrayVariant *& oListOfUserContextNames, CATSafeArrayVariant *& oListErrorMessages) \
{ \
return (ENVTIECALL(DNBIAMHIContext,ENVTIETypeLetter,ENVTIELetter)GetUserContextList(iRefProcessID,oListOfUserContextNames,oListErrorMessages)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIAMHIContext,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIAMHIContext,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIAMHIContext,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIAMHIContext,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIAMHIContext,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIAMHIContext(classe)    TIEDNBIAMHIContext##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIAMHIContext(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIAMHIContext, classe) \
 \
 \
CATImplementTIEMethods(DNBIAMHIContext, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIAMHIContext, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIAMHIContext, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIAMHIContext, classe) \
 \
HRESULT __stdcall  TIEDNBIAMHIContext##classe::LoadContext(const CATBSTR & iContextName, DNBMHIContextLoadOption iLoadOption, CATSafeArrayVariant *& oListErrorMessages) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&iContextName,&iLoadOption,&oListErrorMessages); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->LoadContext(iContextName,iLoadOption,oListErrorMessages); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&iContextName,&iLoadOption,&oListErrorMessages); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIContext##classe::CreateUserContext(const CATBSTR & iRefProcessID, const CATBSTR & iContextName, const CATBSTR & iUserCtxDescription, const CATSafeArrayVariant & iListContextObjectIDs, CATSafeArrayVariant *& oListErrorMessages) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&iRefProcessID,&iContextName,&iUserCtxDescription,&iListContextObjectIDs,&oListErrorMessages); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateUserContext(iRefProcessID,iContextName,iUserCtxDescription,iListContextObjectIDs,oListErrorMessages); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&iRefProcessID,&iContextName,&iUserCtxDescription,&iListContextObjectIDs,&oListErrorMessages); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIContext##classe::SetNewDocumentLoadMode(CAT_VARIANT_BOOL iNewDocLoadMode) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&iNewDocLoadMode); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetNewDocumentLoadMode(iNewDocLoadMode); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&iNewDocLoadMode); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIContext##classe::SetLoadParameters(DNBIAMHILoadParameters * iLoadParameters) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&iLoadParameters); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetLoadParameters(iLoadParameters); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&iLoadParameters); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIContext##classe::IsContextLoadedFromParent(CAT_VARIANT_BOOL & oLoadedFromParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&oLoadedFromParent); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->IsContextLoadedFromParent(oLoadedFromParent); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&oLoadedFromParent); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIContext##classe::DisableSaveOfContext(CAT_VARIANT_BOOL iDisableContextSave) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&iDisableContextSave); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->DisableSaveOfContext(iDisableContextSave); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&iDisableContextSave); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIContext##classe::CreateContextDoc(CATSafeArrayVariant *& oErrorMessages, CATIADocument *& oLoadedDoc) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&oErrorMessages,&oLoadedDoc); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateContextDoc(oErrorMessages,oLoadedDoc); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&oErrorMessages,&oLoadedDoc); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIContext##classe::AddToUserContext(const CATSafeArrayVariant & iListContextObjectIDs, CATSafeArrayVariant *& oListErrorMessages) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&iListContextObjectIDs,&oListErrorMessages); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->AddToUserContext(iListContextObjectIDs,oListErrorMessages); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&iListContextObjectIDs,&oListErrorMessages); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIContext##classe::RemoveFromUserContext(const CATSafeArrayVariant & iListContextObjectIDs, CATSafeArrayVariant *& oListErrorMessages) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&iListContextObjectIDs,&oListErrorMessages); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->RemoveFromUserContext(iListContextObjectIDs,oListErrorMessages); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&iListContextObjectIDs,&oListErrorMessages); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIContext##classe::UpdateUserContext(const CATSafeArrayVariant & iListOfOldContextObjectIDs, const CATSafeArrayVariant & iListOfNewContextObjectIDs, CATSafeArrayVariant *& oListErrorMessages) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&iListOfOldContextObjectIDs,&iListOfNewContextObjectIDs,&oListErrorMessages); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->UpdateUserContext(iListOfOldContextObjectIDs,iListOfNewContextObjectIDs,oListErrorMessages); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&iListOfOldContextObjectIDs,&iListOfNewContextObjectIDs,&oListErrorMessages); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIContext##classe::DeleteUserContext(const CATBSTR & iRefProcessID, const CATBSTR & iContextName, CATSafeArrayVariant *& oListErrorMessages) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,11,&_Trac2,&iRefProcessID,&iContextName,&oListErrorMessages); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->DeleteUserContext(iRefProcessID,iContextName,oListErrorMessages); \
   ExitAfterCall(this,11,_Trac2,&_ret_arg,&iRefProcessID,&iContextName,&oListErrorMessages); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIContext##classe::GetUserContextList(const CATBSTR & iRefProcessID, CATSafeArrayVariant *& oListOfUserContextNames, CATSafeArrayVariant *& oListErrorMessages) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,12,&_Trac2,&iRefProcessID,&oListOfUserContextNames,&oListErrorMessages); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetUserContextList(iRefProcessID,oListOfUserContextNames,oListErrorMessages); \
   ExitAfterCall(this,12,_Trac2,&_ret_arg,&iRefProcessID,&oListOfUserContextNames,&oListErrorMessages); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIContext##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,13,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,13,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIContext##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,14,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,14,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIContext##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,15,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,15,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIContext##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,16,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,16,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIContext##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,17,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,17,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIAMHIContext(classe) \
 \
 \
declare_TIE_DNBIAMHIContext(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAMHIContext##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAMHIContext,"DNBIAMHIContext",DNBIAMHIContext::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAMHIContext(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIAMHIContext, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAMHIContext##classe(classe::MetaObject(),DNBIAMHIContext::MetaObject(),(void *)CreateTIEDNBIAMHIContext##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIAMHIContext(classe) \
 \
 \
declare_TIE_DNBIAMHIContext(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAMHIContext##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAMHIContext,"DNBIAMHIContext",DNBIAMHIContext::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAMHIContext(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIAMHIContext, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAMHIContext##classe(classe::MetaObject(),DNBIAMHIContext::MetaObject(),(void *)CreateTIEDNBIAMHIContext##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIAMHIContext(classe) TIE_DNBIAMHIContext(classe)
#else
#define BOA_DNBIAMHIContext(classe) CATImplementBOA(DNBIAMHIContext, classe)
#endif

#endif
