#ifndef __TIE_DNBIAMHIPlanningContext
#define __TIE_DNBIAMHIPlanningContext

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIAMHIPlanningContext.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIAMHIPlanningContext */
#define declare_TIE_DNBIAMHIPlanningContext(classe) \
 \
 \
class TIEDNBIAMHIPlanningContext##classe : public DNBIAMHIPlanningContext \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIAMHIPlanningContext, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall LoadPlanningContext(DNBMHIPlanningContextLoadOption iLoadOption, CATSafeArrayVariant *& oListErrorMessages); \
      virtual HRESULT __stdcall SpecifyLoadVolume(DNBMHIVolumeBoundaryOperator iOperator, double iClearence, double iXmin, double iXmax, double iYmin, double iYmax, double iZmin, double iZmax); \
      virtual HRESULT __stdcall SpecifyReferenceObjects(const CATSafeArrayVariant & iReferenceObjectIDs); \
      virtual HRESULT __stdcall SetNewDocumentLoadMode(CAT_VARIANT_BOOL iNewDocLoadMode); \
      virtual HRESULT __stdcall SetLoadParameters(DNBIAMHILoadParameters * iLoadParameters); \
      virtual HRESULT __stdcall IsContextLoadedFromParent(CAT_VARIANT_BOOL & oLoadedFromParent); \
      virtual HRESULT __stdcall DisableSaveOfContext(CAT_VARIANT_BOOL iDisableContextSave); \
      virtual HRESULT __stdcall CreateContextDoc(CATSafeArrayVariant *& oErrorMessages, CATIADocument *& oLoadedDoc); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIAMHIPlanningContext(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall LoadPlanningContext(DNBMHIPlanningContextLoadOption iLoadOption, CATSafeArrayVariant *& oListErrorMessages); \
virtual HRESULT __stdcall SpecifyLoadVolume(DNBMHIVolumeBoundaryOperator iOperator, double iClearence, double iXmin, double iXmax, double iYmin, double iYmax, double iZmin, double iZmax); \
virtual HRESULT __stdcall SpecifyReferenceObjects(const CATSafeArrayVariant & iReferenceObjectIDs); \
virtual HRESULT __stdcall SetNewDocumentLoadMode(CAT_VARIANT_BOOL iNewDocLoadMode); \
virtual HRESULT __stdcall SetLoadParameters(DNBIAMHILoadParameters * iLoadParameters); \
virtual HRESULT __stdcall IsContextLoadedFromParent(CAT_VARIANT_BOOL & oLoadedFromParent); \
virtual HRESULT __stdcall DisableSaveOfContext(CAT_VARIANT_BOOL iDisableContextSave); \
virtual HRESULT __stdcall CreateContextDoc(CATSafeArrayVariant *& oErrorMessages, CATIADocument *& oLoadedDoc); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIAMHIPlanningContext(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::LoadPlanningContext(DNBMHIPlanningContextLoadOption iLoadOption, CATSafeArrayVariant *& oListErrorMessages) \
{ \
return (ENVTIECALL(DNBIAMHIPlanningContext,ENVTIETypeLetter,ENVTIELetter)LoadPlanningContext(iLoadOption,oListErrorMessages)); \
} \
HRESULT __stdcall  ENVTIEName::SpecifyLoadVolume(DNBMHIVolumeBoundaryOperator iOperator, double iClearence, double iXmin, double iXmax, double iYmin, double iYmax, double iZmin, double iZmax) \
{ \
return (ENVTIECALL(DNBIAMHIPlanningContext,ENVTIETypeLetter,ENVTIELetter)SpecifyLoadVolume(iOperator,iClearence,iXmin,iXmax,iYmin,iYmax,iZmin,iZmax)); \
} \
HRESULT __stdcall  ENVTIEName::SpecifyReferenceObjects(const CATSafeArrayVariant & iReferenceObjectIDs) \
{ \
return (ENVTIECALL(DNBIAMHIPlanningContext,ENVTIETypeLetter,ENVTIELetter)SpecifyReferenceObjects(iReferenceObjectIDs)); \
} \
HRESULT __stdcall  ENVTIEName::SetNewDocumentLoadMode(CAT_VARIANT_BOOL iNewDocLoadMode) \
{ \
return (ENVTIECALL(DNBIAMHIPlanningContext,ENVTIETypeLetter,ENVTIELetter)SetNewDocumentLoadMode(iNewDocLoadMode)); \
} \
HRESULT __stdcall  ENVTIEName::SetLoadParameters(DNBIAMHILoadParameters * iLoadParameters) \
{ \
return (ENVTIECALL(DNBIAMHIPlanningContext,ENVTIETypeLetter,ENVTIELetter)SetLoadParameters(iLoadParameters)); \
} \
HRESULT __stdcall  ENVTIEName::IsContextLoadedFromParent(CAT_VARIANT_BOOL & oLoadedFromParent) \
{ \
return (ENVTIECALL(DNBIAMHIPlanningContext,ENVTIETypeLetter,ENVTIELetter)IsContextLoadedFromParent(oLoadedFromParent)); \
} \
HRESULT __stdcall  ENVTIEName::DisableSaveOfContext(CAT_VARIANT_BOOL iDisableContextSave) \
{ \
return (ENVTIECALL(DNBIAMHIPlanningContext,ENVTIETypeLetter,ENVTIELetter)DisableSaveOfContext(iDisableContextSave)); \
} \
HRESULT __stdcall  ENVTIEName::CreateContextDoc(CATSafeArrayVariant *& oErrorMessages, CATIADocument *& oLoadedDoc) \
{ \
return (ENVTIECALL(DNBIAMHIPlanningContext,ENVTIETypeLetter,ENVTIELetter)CreateContextDoc(oErrorMessages,oLoadedDoc)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIAMHIPlanningContext,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIAMHIPlanningContext,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIAMHIPlanningContext,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIAMHIPlanningContext,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIAMHIPlanningContext,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIAMHIPlanningContext(classe)    TIEDNBIAMHIPlanningContext##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIAMHIPlanningContext(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIAMHIPlanningContext, classe) \
 \
 \
CATImplementTIEMethods(DNBIAMHIPlanningContext, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIAMHIPlanningContext, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIAMHIPlanningContext, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIAMHIPlanningContext, classe) \
 \
HRESULT __stdcall  TIEDNBIAMHIPlanningContext##classe::LoadPlanningContext(DNBMHIPlanningContextLoadOption iLoadOption, CATSafeArrayVariant *& oListErrorMessages) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&iLoadOption,&oListErrorMessages); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->LoadPlanningContext(iLoadOption,oListErrorMessages); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&iLoadOption,&oListErrorMessages); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIPlanningContext##classe::SpecifyLoadVolume(DNBMHIVolumeBoundaryOperator iOperator, double iClearence, double iXmin, double iXmax, double iYmin, double iYmax, double iZmin, double iZmax) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&iOperator,&iClearence,&iXmin,&iXmax,&iYmin,&iYmax,&iZmin,&iZmax); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SpecifyLoadVolume(iOperator,iClearence,iXmin,iXmax,iYmin,iYmax,iZmin,iZmax); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&iOperator,&iClearence,&iXmin,&iXmax,&iYmin,&iYmax,&iZmin,&iZmax); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIPlanningContext##classe::SpecifyReferenceObjects(const CATSafeArrayVariant & iReferenceObjectIDs) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&iReferenceObjectIDs); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SpecifyReferenceObjects(iReferenceObjectIDs); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&iReferenceObjectIDs); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIPlanningContext##classe::SetNewDocumentLoadMode(CAT_VARIANT_BOOL iNewDocLoadMode) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&iNewDocLoadMode); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetNewDocumentLoadMode(iNewDocLoadMode); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&iNewDocLoadMode); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIPlanningContext##classe::SetLoadParameters(DNBIAMHILoadParameters * iLoadParameters) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&iLoadParameters); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetLoadParameters(iLoadParameters); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&iLoadParameters); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIPlanningContext##classe::IsContextLoadedFromParent(CAT_VARIANT_BOOL & oLoadedFromParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&oLoadedFromParent); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->IsContextLoadedFromParent(oLoadedFromParent); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&oLoadedFromParent); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIPlanningContext##classe::DisableSaveOfContext(CAT_VARIANT_BOOL iDisableContextSave) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&iDisableContextSave); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->DisableSaveOfContext(iDisableContextSave); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&iDisableContextSave); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIPlanningContext##classe::CreateContextDoc(CATSafeArrayVariant *& oErrorMessages, CATIADocument *& oLoadedDoc) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&oErrorMessages,&oLoadedDoc); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateContextDoc(oErrorMessages,oLoadedDoc); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&oErrorMessages,&oLoadedDoc); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIPlanningContext##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIPlanningContext##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIPlanningContext##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,11,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,11,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIPlanningContext##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,12,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,12,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIPlanningContext##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,13,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,13,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIAMHIPlanningContext(classe) \
 \
 \
declare_TIE_DNBIAMHIPlanningContext(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAMHIPlanningContext##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAMHIPlanningContext,"DNBIAMHIPlanningContext",DNBIAMHIPlanningContext::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAMHIPlanningContext(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIAMHIPlanningContext, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAMHIPlanningContext##classe(classe::MetaObject(),DNBIAMHIPlanningContext::MetaObject(),(void *)CreateTIEDNBIAMHIPlanningContext##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIAMHIPlanningContext(classe) \
 \
 \
declare_TIE_DNBIAMHIPlanningContext(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAMHIPlanningContext##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAMHIPlanningContext,"DNBIAMHIPlanningContext",DNBIAMHIPlanningContext::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAMHIPlanningContext(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIAMHIPlanningContext, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAMHIPlanningContext##classe(classe::MetaObject(),DNBIAMHIPlanningContext::MetaObject(),(void *)CreateTIEDNBIAMHIPlanningContext##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIAMHIPlanningContext(classe) TIE_DNBIAMHIPlanningContext(classe)
#else
#define BOA_DNBIAMHIPlanningContext(classe) CATImplementBOA(DNBIAMHIPlanningContext, classe)
#endif

#endif
