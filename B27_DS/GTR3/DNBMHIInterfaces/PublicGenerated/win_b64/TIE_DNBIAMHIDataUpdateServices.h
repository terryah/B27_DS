#ifndef __TIE_DNBIAMHIDataUpdateServices
#define __TIE_DNBIAMHIDataUpdateServices

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIAMHIDataUpdateServices.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIAMHIDataUpdateServices */
#define declare_TIE_DNBIAMHIDataUpdateServices(classe) \
 \
 \
class TIEDNBIAMHIDataUpdateServices##classe : public DNBIAMHIDataUpdateServices \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIAMHIDataUpdateServices, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall UpdateConfiguredName(CATIAItem * iItem); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIAMHIDataUpdateServices(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall UpdateConfiguredName(CATIAItem * iItem); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIAMHIDataUpdateServices(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::UpdateConfiguredName(CATIAItem * iItem) \
{ \
return (ENVTIECALL(DNBIAMHIDataUpdateServices,ENVTIETypeLetter,ENVTIELetter)UpdateConfiguredName(iItem)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIAMHIDataUpdateServices,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIAMHIDataUpdateServices,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIAMHIDataUpdateServices,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIAMHIDataUpdateServices,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIAMHIDataUpdateServices,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIAMHIDataUpdateServices(classe)    TIEDNBIAMHIDataUpdateServices##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIAMHIDataUpdateServices(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIAMHIDataUpdateServices, classe) \
 \
 \
CATImplementTIEMethods(DNBIAMHIDataUpdateServices, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIAMHIDataUpdateServices, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIAMHIDataUpdateServices, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIAMHIDataUpdateServices, classe) \
 \
HRESULT __stdcall  TIEDNBIAMHIDataUpdateServices##classe::UpdateConfiguredName(CATIAItem * iItem) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&iItem); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->UpdateConfiguredName(iItem); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&iItem); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIDataUpdateServices##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIDataUpdateServices##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIDataUpdateServices##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIDataUpdateServices##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIDataUpdateServices##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIAMHIDataUpdateServices(classe) \
 \
 \
declare_TIE_DNBIAMHIDataUpdateServices(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAMHIDataUpdateServices##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAMHIDataUpdateServices,"DNBIAMHIDataUpdateServices",DNBIAMHIDataUpdateServices::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAMHIDataUpdateServices(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIAMHIDataUpdateServices, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAMHIDataUpdateServices##classe(classe::MetaObject(),DNBIAMHIDataUpdateServices::MetaObject(),(void *)CreateTIEDNBIAMHIDataUpdateServices##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIAMHIDataUpdateServices(classe) \
 \
 \
declare_TIE_DNBIAMHIDataUpdateServices(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAMHIDataUpdateServices##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAMHIDataUpdateServices,"DNBIAMHIDataUpdateServices",DNBIAMHIDataUpdateServices::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAMHIDataUpdateServices(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIAMHIDataUpdateServices, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAMHIDataUpdateServices##classe(classe::MetaObject(),DNBIAMHIDataUpdateServices::MetaObject(),(void *)CreateTIEDNBIAMHIDataUpdateServices##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIAMHIDataUpdateServices(classe) TIE_DNBIAMHIDataUpdateServices(classe)
#else
#define BOA_DNBIAMHIDataUpdateServices(classe) CATImplementBOA(DNBIAMHIDataUpdateServices, classe)
#endif

#endif
