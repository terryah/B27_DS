#ifndef __TIE_DNBIAMHIOpenAccess
#define __TIE_DNBIAMHIOpenAccess

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIAMHIOpenAccess.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIAMHIOpenAccess */
#define declare_TIE_DNBIAMHIOpenAccess(classe) \
 \
 \
class TIEDNBIAMHIOpenAccess##classe : public DNBIAMHIOpenAccess \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIAMHIOpenAccess, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall ConnectToPPRHub(const CATBSTR & iUsername, const CATBSTR & iPassword); \
      virtual HRESULT __stdcall CreateLoadParameters(DNBIAMHILoadParameters *& oLoadParameters); \
      virtual HRESULT __stdcall LoadFromPPRHub(DNBIAMHILoadParameters * iLoadParameters, CAT_VARIANT_BOOL iCreateDefaultWindow, CAT_VARIANT_BOOL iIsReadOnly, CATSafeArrayVariant *& oErrorMessages, CATIADocument *& oLoadedDoc); \
      virtual HRESULT __stdcall SetEnvironmentForVBSLaunch(const CATBSTR & iObjId, const CATBSTR & iFilter, const CATBSTR & iParentId, const CATBSTR & iProjectId, const CATBSTR & iUserName, const CATBSTR & iPwd, CATSafeArrayVariant *& oErrorMessages); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIAMHIOpenAccess(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall ConnectToPPRHub(const CATBSTR & iUsername, const CATBSTR & iPassword); \
virtual HRESULT __stdcall CreateLoadParameters(DNBIAMHILoadParameters *& oLoadParameters); \
virtual HRESULT __stdcall LoadFromPPRHub(DNBIAMHILoadParameters * iLoadParameters, CAT_VARIANT_BOOL iCreateDefaultWindow, CAT_VARIANT_BOOL iIsReadOnly, CATSafeArrayVariant *& oErrorMessages, CATIADocument *& oLoadedDoc); \
virtual HRESULT __stdcall SetEnvironmentForVBSLaunch(const CATBSTR & iObjId, const CATBSTR & iFilter, const CATBSTR & iParentId, const CATBSTR & iProjectId, const CATBSTR & iUserName, const CATBSTR & iPwd, CATSafeArrayVariant *& oErrorMessages); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIAMHIOpenAccess(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::ConnectToPPRHub(const CATBSTR & iUsername, const CATBSTR & iPassword) \
{ \
return (ENVTIECALL(DNBIAMHIOpenAccess,ENVTIETypeLetter,ENVTIELetter)ConnectToPPRHub(iUsername,iPassword)); \
} \
HRESULT __stdcall  ENVTIEName::CreateLoadParameters(DNBIAMHILoadParameters *& oLoadParameters) \
{ \
return (ENVTIECALL(DNBIAMHIOpenAccess,ENVTIETypeLetter,ENVTIELetter)CreateLoadParameters(oLoadParameters)); \
} \
HRESULT __stdcall  ENVTIEName::LoadFromPPRHub(DNBIAMHILoadParameters * iLoadParameters, CAT_VARIANT_BOOL iCreateDefaultWindow, CAT_VARIANT_BOOL iIsReadOnly, CATSafeArrayVariant *& oErrorMessages, CATIADocument *& oLoadedDoc) \
{ \
return (ENVTIECALL(DNBIAMHIOpenAccess,ENVTIETypeLetter,ENVTIELetter)LoadFromPPRHub(iLoadParameters,iCreateDefaultWindow,iIsReadOnly,oErrorMessages,oLoadedDoc)); \
} \
HRESULT __stdcall  ENVTIEName::SetEnvironmentForVBSLaunch(const CATBSTR & iObjId, const CATBSTR & iFilter, const CATBSTR & iParentId, const CATBSTR & iProjectId, const CATBSTR & iUserName, const CATBSTR & iPwd, CATSafeArrayVariant *& oErrorMessages) \
{ \
return (ENVTIECALL(DNBIAMHIOpenAccess,ENVTIETypeLetter,ENVTIELetter)SetEnvironmentForVBSLaunch(iObjId,iFilter,iParentId,iProjectId,iUserName,iPwd,oErrorMessages)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIAMHIOpenAccess,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIAMHIOpenAccess,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIAMHIOpenAccess,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIAMHIOpenAccess,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIAMHIOpenAccess,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIAMHIOpenAccess(classe)    TIEDNBIAMHIOpenAccess##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIAMHIOpenAccess(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIAMHIOpenAccess, classe) \
 \
 \
CATImplementTIEMethods(DNBIAMHIOpenAccess, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIAMHIOpenAccess, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIAMHIOpenAccess, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIAMHIOpenAccess, classe) \
 \
HRESULT __stdcall  TIEDNBIAMHIOpenAccess##classe::ConnectToPPRHub(const CATBSTR & iUsername, const CATBSTR & iPassword) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&iUsername,&iPassword); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ConnectToPPRHub(iUsername,iPassword); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&iUsername,&iPassword); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIOpenAccess##classe::CreateLoadParameters(DNBIAMHILoadParameters *& oLoadParameters) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&oLoadParameters); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateLoadParameters(oLoadParameters); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&oLoadParameters); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIOpenAccess##classe::LoadFromPPRHub(DNBIAMHILoadParameters * iLoadParameters, CAT_VARIANT_BOOL iCreateDefaultWindow, CAT_VARIANT_BOOL iIsReadOnly, CATSafeArrayVariant *& oErrorMessages, CATIADocument *& oLoadedDoc) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&iLoadParameters,&iCreateDefaultWindow,&iIsReadOnly,&oErrorMessages,&oLoadedDoc); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->LoadFromPPRHub(iLoadParameters,iCreateDefaultWindow,iIsReadOnly,oErrorMessages,oLoadedDoc); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&iLoadParameters,&iCreateDefaultWindow,&iIsReadOnly,&oErrorMessages,&oLoadedDoc); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIOpenAccess##classe::SetEnvironmentForVBSLaunch(const CATBSTR & iObjId, const CATBSTR & iFilter, const CATBSTR & iParentId, const CATBSTR & iProjectId, const CATBSTR & iUserName, const CATBSTR & iPwd, CATSafeArrayVariant *& oErrorMessages) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&iObjId,&iFilter,&iParentId,&iProjectId,&iUserName,&iPwd,&oErrorMessages); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetEnvironmentForVBSLaunch(iObjId,iFilter,iParentId,iProjectId,iUserName,iPwd,oErrorMessages); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&iObjId,&iFilter,&iParentId,&iProjectId,&iUserName,&iPwd,&oErrorMessages); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIOpenAccess##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIOpenAccess##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIOpenAccess##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIOpenAccess##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIOpenAccess##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIAMHIOpenAccess(classe) \
 \
 \
declare_TIE_DNBIAMHIOpenAccess(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAMHIOpenAccess##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAMHIOpenAccess,"DNBIAMHIOpenAccess",DNBIAMHIOpenAccess::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAMHIOpenAccess(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIAMHIOpenAccess, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAMHIOpenAccess##classe(classe::MetaObject(),DNBIAMHIOpenAccess::MetaObject(),(void *)CreateTIEDNBIAMHIOpenAccess##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIAMHIOpenAccess(classe) \
 \
 \
declare_TIE_DNBIAMHIOpenAccess(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAMHIOpenAccess##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAMHIOpenAccess,"DNBIAMHIOpenAccess",DNBIAMHIOpenAccess::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAMHIOpenAccess(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIAMHIOpenAccess, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAMHIOpenAccess##classe(classe::MetaObject(),DNBIAMHIOpenAccess::MetaObject(),(void *)CreateTIEDNBIAMHIOpenAccess##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIAMHIOpenAccess(classe) TIE_DNBIAMHIOpenAccess(classe)
#else
#define BOA_DNBIAMHIOpenAccess(classe) CATImplementBOA(DNBIAMHIOpenAccess, classe)
#endif

#endif
