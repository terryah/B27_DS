#ifndef __TIE_DNBIAMHIObjectServices
#define __TIE_DNBIAMHIObjectServices

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIAMHIObjectServices.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIAMHIObjectServices */
#define declare_TIE_DNBIAMHIObjectServices(classe) \
 \
 \
class TIEDNBIAMHIObjectServices##classe : public DNBIAMHIObjectServices \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIAMHIObjectServices, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall Insert(const CATSafeArrayVariant & iListObjectIDs, DNBMHIObjType iObjectType, DNBMHIInsertType iInsertType, CATSafeArrayVariant *& oListErrorMessages); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIAMHIObjectServices(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall Insert(const CATSafeArrayVariant & iListObjectIDs, DNBMHIObjType iObjectType, DNBMHIInsertType iInsertType, CATSafeArrayVariant *& oListErrorMessages); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIAMHIObjectServices(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::Insert(const CATSafeArrayVariant & iListObjectIDs, DNBMHIObjType iObjectType, DNBMHIInsertType iInsertType, CATSafeArrayVariant *& oListErrorMessages) \
{ \
return (ENVTIECALL(DNBIAMHIObjectServices,ENVTIETypeLetter,ENVTIELetter)Insert(iListObjectIDs,iObjectType,iInsertType,oListErrorMessages)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIAMHIObjectServices,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIAMHIObjectServices,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIAMHIObjectServices,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIAMHIObjectServices,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIAMHIObjectServices,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIAMHIObjectServices(classe)    TIEDNBIAMHIObjectServices##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIAMHIObjectServices(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIAMHIObjectServices, classe) \
 \
 \
CATImplementTIEMethods(DNBIAMHIObjectServices, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIAMHIObjectServices, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIAMHIObjectServices, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIAMHIObjectServices, classe) \
 \
HRESULT __stdcall  TIEDNBIAMHIObjectServices##classe::Insert(const CATSafeArrayVariant & iListObjectIDs, DNBMHIObjType iObjectType, DNBMHIInsertType iInsertType, CATSafeArrayVariant *& oListErrorMessages) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&iListObjectIDs,&iObjectType,&iInsertType,&oListErrorMessages); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Insert(iListObjectIDs,iObjectType,iInsertType,oListErrorMessages); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&iListObjectIDs,&iObjectType,&iInsertType,&oListErrorMessages); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIObjectServices##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIObjectServices##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIObjectServices##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIObjectServices##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIObjectServices##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIAMHIObjectServices(classe) \
 \
 \
declare_TIE_DNBIAMHIObjectServices(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAMHIObjectServices##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAMHIObjectServices,"DNBIAMHIObjectServices",DNBIAMHIObjectServices::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAMHIObjectServices(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIAMHIObjectServices, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAMHIObjectServices##classe(classe::MetaObject(),DNBIAMHIObjectServices::MetaObject(),(void *)CreateTIEDNBIAMHIObjectServices##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIAMHIObjectServices(classe) \
 \
 \
declare_TIE_DNBIAMHIObjectServices(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAMHIObjectServices##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAMHIObjectServices,"DNBIAMHIObjectServices",DNBIAMHIObjectServices::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAMHIObjectServices(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIAMHIObjectServices, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAMHIObjectServices##classe(classe::MetaObject(),DNBIAMHIObjectServices::MetaObject(),(void *)CreateTIEDNBIAMHIObjectServices##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIAMHIObjectServices(classe) TIE_DNBIAMHIObjectServices(classe)
#else
#define BOA_DNBIAMHIObjectServices(classe) CATImplementBOA(DNBIAMHIObjectServices, classe)
#endif

#endif
