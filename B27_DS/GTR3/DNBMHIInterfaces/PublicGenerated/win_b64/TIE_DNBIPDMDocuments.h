#ifndef __TIE_DNBIPDMDocuments
#define __TIE_DNBIPDMDocuments

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "DNBIPDMDocuments.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIPDMDocuments */
#define declare_TIE_DNBIPDMDocuments(classe) \
 \
 \
class TIEDNBIPDMDocuments##classe : public DNBIPDMDocuments \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIPDMDocuments, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT GetPDMDocuments(const CATUnicodeString & iUserID, const CATUnicodeString & iProjectNameShort, CATListValCATUnicodeString &iDocIds, CATListValCATUnicodeString &iTargetFileNames, CATListValCATUnicodeString &oDocFilePaths, CATListValCATUnicodeString &oErrorMessages) ; \
};



#define ENVTIEdeclare_DNBIPDMDocuments(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT GetPDMDocuments(const CATUnicodeString & iUserID, const CATUnicodeString & iProjectNameShort, CATListValCATUnicodeString &iDocIds, CATListValCATUnicodeString &iTargetFileNames, CATListValCATUnicodeString &oDocFilePaths, CATListValCATUnicodeString &oErrorMessages) ; \


#define ENVTIEdefine_DNBIPDMDocuments(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT  ENVTIEName::GetPDMDocuments(const CATUnicodeString & iUserID, const CATUnicodeString & iProjectNameShort, CATListValCATUnicodeString &iDocIds, CATListValCATUnicodeString &iTargetFileNames, CATListValCATUnicodeString &oDocFilePaths, CATListValCATUnicodeString &oErrorMessages)  \
{ \
return (ENVTIECALL(DNBIPDMDocuments,ENVTIETypeLetter,ENVTIELetter)GetPDMDocuments(iUserID,iProjectNameShort,iDocIds,iTargetFileNames,oDocFilePaths,oErrorMessages)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIPDMDocuments(classe)    TIEDNBIPDMDocuments##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIPDMDocuments(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIPDMDocuments, classe) \
 \
 \
CATImplementTIEMethods(DNBIPDMDocuments, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIPDMDocuments, classe, 1) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIPDMDocuments, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIPDMDocuments, classe) \
 \
HRESULT  TIEDNBIPDMDocuments##classe::GetPDMDocuments(const CATUnicodeString & iUserID, const CATUnicodeString & iProjectNameShort, CATListValCATUnicodeString &iDocIds, CATListValCATUnicodeString &iTargetFileNames, CATListValCATUnicodeString &oDocFilePaths, CATListValCATUnicodeString &oErrorMessages)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetPDMDocuments(iUserID,iProjectNameShort,iDocIds,iTargetFileNames,oDocFilePaths,oErrorMessages)); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIPDMDocuments(classe) \
 \
 \
declare_TIE_DNBIPDMDocuments(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIPDMDocuments##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIPDMDocuments,"DNBIPDMDocuments",DNBIPDMDocuments::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIPDMDocuments(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIPDMDocuments, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIPDMDocuments##classe(classe::MetaObject(),DNBIPDMDocuments::MetaObject(),(void *)CreateTIEDNBIPDMDocuments##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIPDMDocuments(classe) \
 \
 \
declare_TIE_DNBIPDMDocuments(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIPDMDocuments##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIPDMDocuments,"DNBIPDMDocuments",DNBIPDMDocuments::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIPDMDocuments(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIPDMDocuments, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIPDMDocuments##classe(classe::MetaObject(),DNBIPDMDocuments::MetaObject(),(void *)CreateTIEDNBIPDMDocuments##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIPDMDocuments(classe) TIE_DNBIPDMDocuments(classe)
#else
#define BOA_DNBIPDMDocuments(classe) CATImplementBOA(DNBIPDMDocuments, classe)
#endif

#endif
