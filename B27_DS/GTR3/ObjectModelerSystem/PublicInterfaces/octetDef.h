/* -*-c++-*- */
// COPYRIGHT DASSAULT SYSTEMES 2000
/** @CAA2Required */
//**********************************************************************
//* DON T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS *
//* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPEAR AT ANY TIME *
//**********************************************************************

#ifndef octetDef_h
#define octetDef_h

// Migration on System FrameWork definition from octet
// ======================================================
#include "CATCORBATypes.h"
// ======================================================
//typedef unsigned char  	octet ;

#endif
