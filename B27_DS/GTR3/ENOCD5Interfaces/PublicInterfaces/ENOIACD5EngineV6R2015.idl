// COPYRIGHT Dassault Systemes 2012
//===================================================================
//
// ENOIACD5EngineV6R2015.idl
// Automation interface for the CD5EngineV6R2015 element 
//
//===================================================================
//
// Usage notes:
//     Dim oTheseCD5Engine As CD5Engine
//     Set oTheseCD5Engine = CATIA.GetItem("CD5EngineV6R2015") 
//
//===================================================================

#ifndef ENOIACD5EngineV6R2015_IDL
#define ENOIACD5EngineV6R2015_IDL
/*IDLREP*/

/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */

#include "CATIABase.idl"
#include "ENOIACD5EngineV6R2014x.idl"
interface CATIABase;
interface IDispatch;
interface CATIADocument;
interface ENOIACD5TemplateTypes;
interface ENOIACD5Template;
interface ENOIACD5ID;
interface ENOIACD5IDs;
interface ENOIACD5Properties;

/**
 * Represents the ENOVIA V6 Integration Engine, that is to say the entry point to the CATIA/ENOVIA V6 Integration.
 * <br>It allows end user to realize the various operations like ENOVIA New
 * <br>Note that all operations performed from this interface are the same as operations available in the  
 * ENOVIA V6 menu in CATIA, unless most of them are executed without panel.
 *
 * @sample
 * The following example indicates how to retrieve the ENOVIA V6 Integration Engine.
 * <pre>
 * <font color="blue">Dim</font> oCD5Engine <font color="blue">As</font> CD5EngineV6R2015
 * <font color="blue">Set</font> oCD5Engine = CATIA.GetItem(<font color="red">"CD5EngineV6R2015"</font>)
 * </pre>
 * 
 */

interface ENOIACD5EngineV6R2015 : ENOIACD5EngineV6R2014x
{

	 /**
    * Returns (gets) the list of all "Template Types".
    * 
    * @sample
    * The following example gets the list of Template Types.  
	* <pre>
	* <font color="blue">Dim</font> oTemplateTypes <font color="blue">As</font> ENOIACD5TemplateTypes
    * <font color="blue">Set</font> oTemplateTypes = oCD5Engine.<font color="red">TemplateTypes</font>
    * </pre>
    */
#pragma PROPERTY TemplateTypes
    HRESULT get_TemplateTypes (out /*IDLRETVAL*/ ENOIACD5TemplateTypes oValue); 

	/**
    * Creates a new object from given template. 
    * 
    * @param iCD5Template
    * The Template object from which we want to create a new object.
    * @param iName
    * The name for the new object.
	* @param iType
    * The ENOVIA type for the new object.
    * @return
    * The created Document.
    * @exception
    * <dl>
    * <dt>-1697450280 : CATIA is not connected to ENOVIA V6.</dt>
	* <dt>-1782306828 : Template object is assigned more than one file.</dt>
	* <dt>-1774688310 : Template object is not assigned any file.</dt>
	* <dt>-1866082326 : File of same name as that of file attached to Template object is present on checkout directory.</dt>
	* <dt>-1739257732 : The name for the new object contains unsupported characters.</dt>
	* <dt>-1706631745 : An object with the same name as that of new object is already present in ENOVIA.</dt>
	* <dt>-1844336441 : File of same name as that of new object is already present on checkout directory.</dt>
    * </dl>
    *
    * @sample
    * The following example creates a new object to be loaded in the CATIA session:
    * <ul>
    * <li>iCD5Template : template.</li>
    * <li>iName : "NewPart".</li>
    * </ul>
    * <pre>
	* <font color="blue">Dim</font> types <font color="blue">As</font> Array
	* types = oTemplateType.PossibleTypes
    * <font color="blue">Dim</font> document <font color="blue">As</font> CATIADocument
    * <font color="blue">Set</font> document = oCD5Engine.<font color="red">NewFrom</font>(template, "NewPart", types(0))
    * </pre>
    */
    HRESULT NewFrom (in ENOIACD5Template iCD5Template, in CATBSTR iName, in CATBSTR iType, out /*IDLRETVAL*/ CATIADocument oDocument);
    /**
    * Returns CD5ID  of the object for the given PhysicalID.
    * 
    * @param iPhysicalID
    * Physical ID of the object
    * @return
    * The created CD5ID of the object
    * @exception
    * <dl>
    * <dt>-1697450280 : CATIA is not connected to ENOVIA V6.</dt>
    * </dl>
    *
    * @sample
    * The following example returns the ENOIACD5ID of the object:
    * <ul>
    * <li>iPhysicalID : "6EFB8D2E00008A445257E36100000DF7".</li>
    * </ul>
    * <pre>
	* <font color="blue">Dim</font> ID <font color="blue">As</font> CD5ID
    * <font color="blue">Set</font> ID = oCD5Engine.<font color="red">GetIDFromPhysicalID</font>("6EFB8D2E00008A445257E36100000DF7")
    * </pre>
    */
    HRESULT GetIDFromPhysicalID(in CATBSTR iPhysicalID, out /*IDLRETVAL*/ ENOIACD5ID oID);
      /**
    * Returns CD5IDs  of the objects for the given PhysicalIDs.
    * 
    * @param iPhysicalIDs
    * Physical IDs of the objects
    * @return
    * The created CD5ID array of the objects
    * @exception
    * <dl>
    * <dt>-1697450280 : CATIA is not connected to ENOVIA V6.</dt>
    * </dl>
    *
    * @sample
    * The following example returns the ENOIACD5IDs of the object:
    * <ul>
    * <li>iPhysicalIDs : "6EFB8D2E00008A445257E36100000DF7,6EFB8D2E00008A445257E3610000090".</li>
    * </ul>
    * <pre>
	* <font color="blue">Dim</font> IDs <font color="blue">As</font> CD5IDs
    * <font color="blue">Set</font> IDs = oCD5Engine.<font color="red">GetIDsFromPhysicalIDs</font>(iPhysicalIDs)
    * </pre>
    */

    HRESULT GetIDsFromPhysicalIDs(in CATSafeArrayVariant iPhysicalIDs, out /*IDLRETVAL*/ ENOIACD5IDs oIDs);

	/**
    * Returns a collection of all the ENOVIA V6 properties for a given ENOVIA object.
    * 
    * @param iCATIADocument
    * The document representing ENOVIA V6 Object which is to be explored.
	* @return
    * The collection of properties of the ENOVIA object passes as iCATIADocument
    * @exception
    * <dl>
    * <dt>-1697450280 : CATIA is not connected to ENOVIA V6.</dt>
    * </dl>
	*
    * @sample
    * The following example retrieves all the ENOVIA V6 properties for a given ENOVIA object.
    * <pre>
    * <font color="blue">Dim</font> docCD5ID <font color="blue">As</font> CD5ID
    * <font color="blue">Set</font> docCD5ID = oCD5Engine.<font color="blue">GetIDFromTNR</font>("CATProduct For Team", "MyProduct", "---")
	* <font color="blue">Dim</font> objDocument <font color="blue">As</font> Document
	* <font color="blue">Set</font> objDocument = oCD5Engine.<font color="blue">Open</font>(docCD5ID)
    * <font color="blue">Dim</font> oCD5Properties <font color="blue">As</font> CD5Properties
    * <font color="blue">Set</font> oCD5Properties = oCD5Engine.<font color="red">GetPropertiesOfDocument</font>(objDocument)
    * </pre>
    */
	HRESULT GetPropertiesOfDocument (in CATIADocument iCATIADocument, out /*IDLRETVAL*/ ENOIACD5Properties oCD5Properties);

	/**
    * Returns a collection of all the ENOVIA V6 properties for a given Embedded Component.
    * 
    * @param iCATIADocument
    * The document representing ENOVIA V6 Object in which the target embedded component resides.
	* @param iEmbeddedComponentName
    * Name of the embedded component to be explored.
	* @return
    * The collection of properties of the embedded component passed as input
    * @exception
    * <dl>
    * <dt>-1697450280 : CATIA is not connected to ENOVIA V6.</dt>
    * </dl>
	*
    * @sample
    * The following example retrieves all the ENOVIA V6 properties for a given Embedded Component.
    * <pre>
    * <font color="blue">Dim</font> docCD5ID <font color="blue">As</font> CD5ID
    * <font color="blue">Set</font> docCD5ID = oCD5Engine.<font color="blue">GetIDFromTNR</font>("CATProduct For Team", "MyProduct", "---")
	* <font color="blue">Dim</font> objDocument <font color="blue">As</font> Document
	* <font color="blue">Set</font> objDocument = oCD5Engine.<font color="blue">Open</font>(docCD5ID)
    * <font color="blue">Dim</font> oCD5Properties <font color="blue">As</font> CD5Properties
    * <font color="blue">Set</font> oCD5Properties = oCD5Engine.<font color="red">GetPropertiesOfEmbeddedComponent</font>(objDocument, "Embedded_Component_Name")
    * </pre>
    */
	HRESULT GetPropertiesOfEmbeddedComponent 
		(in CATIADocument iCATIADocument, in CATBSTR iEmbeddedComponentName, out /*IDLRETVAL*/ ENOIACD5Properties oCD5Properties);

	/**
    * Returns a collection of all the ENOVIA V6 properties for a given Object defined by Type, Name, Revision & Version.
    * 
    * @param iENOIACD5ID
    * The ENOIACD5ID object id representing ENOVIA V6 Object which is to be explored.
	* @return
    * The collection of properties of the ENOVIA object passes as iENOIACD5ID
    * @exception
    * <dl>
    * <dt>-1697450280 : CATIA is not connected to ENOVIA V6.</dt>
	* <dt>-1691273589 : Invalid object type</dt>
	* <dt>-1857112104 : Name or Type Empty</dt>
    * </dl>
	*
    * @sample
    * The following example retrieves all the ENOVIA V6 properties from a given ENOIACD5ID.
    * <pre>
    * <font color="blue">Dim</font> ObjID <font color="blue">As</font> CD5ID
    * <font color="blue">Set</font> ObjID = oCD5Engine.<font color="blue">GetIDFromTNRV</font>("Versioned CATPart", "Part_Name", "A", "0")
    * <font color="blue">Dim</font> oCD5Properties <font color="blue">As</font> CD5Properties
    * <font color="blue">Set</font> oCD5Properties = oCD5Engine.<font color="red">GetProperties</font>(ObjID)
    * </pre>
    */
	HRESULT GetProperties (in ENOIACD5ID iENOIACD5ID, out /*IDLRETVAL*/ ENOIACD5Properties oCD5Properties);


};

// Interface name : ENOIACD5EngineV6R2015
#pragma ID ENOIACD5EngineV6R2015 "DCE:FF51F18E-7435-4d46-B3F3111BB012A0DB"
#pragma DUAL ENOIACD5EngineV6R2015

// VB object name : CD5EngineV6R2015 (Id used in Visual Basic)
#pragma ID CD5EngineV6R2015 "DCE:0B343A19-9590-472b-9D785AC72EDAEE8C"
#pragma ALIAS ENOIACD5EngineV6R2015 CD5EngineV6R2015

#endif
