# COPYRIGHT DASSAULT SYSTEMES 2012
#======================================================================
# Imakefile for module ENOCD5IntegPubIDL
# Module for compilation of the Public IDL interfaces
#======================================================================
#
#  Jan 2012  Creation: Code generated by the CAA wizard  CJD
#======================================================================
#
# NO BUILD             
#

BUILT_OBJECT_TYPE=SHARED LIBRARY

LINK_WITH = JS0FM JS0GROUP CATObjectModelerBase

SOURCES_PATH=PublicInterfaces
COMPILATION_IDL=YES

