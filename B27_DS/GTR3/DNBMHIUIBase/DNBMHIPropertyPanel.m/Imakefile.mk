#==============================================================================
# COPYRIGHT DASSAULT SYSTEMES 2006
#==============================================================================
# Imakefile for module DNBMHIPropertyPanel.m
#==============================================================================
# Usage notes:
#
# BUILD_OBJECT_TYPE = SHARED LIBRARY (DLL) or LOAD MODULE (EXE) or NONE  
# INCLUDED_MODULES  = <MODULE_1_NAME> <MODULE_2_NAME> ... 
# LINK_WITH         = <DLL_1_NAME>  \  # <FW_1_NAME>
#                     <DLL_2_NAME>  \  # <FW_2_NAME>
#
#==============================================================================
#==============================================================================

BUILT_OBJECT_TYPE = SHARED LIBRARY 

WIZARD_LINK_MODULES = JS0GROUP CATObjectModelerBase \
JS0GROUP JS0FM \
AD0XXBAS  \
ProcessInterfaces DMAPSInterfacesUUID 

LINK_WITH = $(WIZARD_LINK_MODULES) ProcessInterfaces			 \	  # DMAPSInterfaces
			CATIPDAdapterItf			\   # CATIPDAdapterInterfaces
			CATIPDAdapter               \   # CATIPDAdapterImpl
			CATObjectModelerBase		\   # ObjectModelerBase
			CATApplicationFrame			\	# ApplicationFrame		
			CATIAApplicationFrame		\	# CATIAApplicationFrame	
			DI0BUILD					\	# Dialog
			DI0PANV2					\	# Dialog
			CATDialogEngine				\	# DialogEngine			
			CATProductStructure1		\	# ProductStructure		
			JS0GROUP 				    \	# System				
			JS0FM       				\	# System				
			CATVisualization			\	# Visualization			
			CATViz                      \   # VisualizationBase
			KnowledgeItf                \   # KnowledgeInterfaces
			CK0FEAT                     \   # LiteralFeatures
			CATFileMenu                 \   # FileMenu
			CATLifDictionary			\   # LiteralFeatures
			DNBPLMItf                   \   # DNBPLMInterfaces
			CATImmItf                   \   # CATImmWtpInterfaces
			DNBMHIBase                  \   # DNBMHIBase
			DNBMHIUIBase                \   # DNBMHIUIBase
			CATDlgHtml           \   #  for CATDlgHtmlView


# System dependant variables
#
OS = AIX
BUILD = NO
#
OS = HP-UX
BUILD = NO
#
OS = IRIX
BUILD = NO
#
OS = SunOS
BUILD = NO
#
OS = Windows_NT

 \
JS0GROUP JS0FM JS0GROUP DNBMHIUIBaseUUID 
# END WIZARD EDITION ZONE
