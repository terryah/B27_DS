# COPYRIGHT DASSAULT SYSTEMES 1999
#======================================================================
#
# Imakefile for module CATTTRSModel.m
#
#======================================================================
#  Jan 1999  Creation                                      
#======================================================================

BUILT_OBJECT_TYPE = SHARED LIBRARY

INCLUDED_MODULES = CATTTRSTeckPack      \
				   CATTTRSSetManagement \
				   CATTTRSAssocitivityManagement \				   

LINK_WITH=  JS0GROUP                    \ # System
            SystemUUID                  \ # System
            CATMathematics              \
            CATVisualization            \
            CATViz                      \
            CATVisUUID                  \
            CATApplicationFrame         \
            CATIAApplicationFrame       \
            CATGeometricObjects         \
            CATGeometricOperators       \
            CATNewTopologicalObjects    \
            YI00IMPL                    \ # NewTopologicalObjects
            CATObjectModelerBase        \
            ObjectModelerBaseUUID       \
            CATObjectSpecsModeler       \
            ObjectSpecsModelerUUID      \
            KnowledgeItf                \ # KnowledgeInterfaces
            CATLiteralFeatures          \
            CATProductStructure1        \
            ProductStructureUUID        \
            CATTTRSItf                  \
            CATTTRSUUID                 \
            CATMecModInterfaces         \
            CATMmiUUID                  \ # MecModInterfaces
            CATMechanicalModeler        \
            CATSketcherInterfaces       \
            CATGitInterfaces            \
            CATSaiSpaceAnalysisItf      \ # SpaceAnalysisInterfaces
            CATUdfInterfaces            \
            CATMathStream               \
            CATCGMGeoMath               \
            CATConstraintModeler        \
            CATConstraintModelerItf     \
            TECHNLNK                       \ # Cluster
            CATProductStructureInterfaces  \ # Cluster
            CSASOLID                       \ # Cluster
            CATIAEntity                    \ # Cluster
            AC0CATPL                       \ # Cluster
            DraftingItfCPP                 \ # DraftingInterfaces
            CATPrsScene \ # ProductStructure
            #CATTPSItfCPP \
            CATGMOperatorsInterfaces  \ # GMOperatorsInterfaces          CATGMOperatorsInterfaces
