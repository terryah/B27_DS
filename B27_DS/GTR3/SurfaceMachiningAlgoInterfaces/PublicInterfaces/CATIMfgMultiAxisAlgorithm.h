//===================================================================
// COPYRIGHT DASSAULT SYSTEMES 2006
//===================================================================
//
// CATIMfgMultiAxisAlgorithm.h
//
//===================================================================

#ifndef CATIMfgMultiAxisAlgorithm_H
#define CATIMfgMultiAxisAlgorithm_H

/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */

#include "CATSMGAlgoItfEnv.h"
#include "CATMfgMultiAxisAlgoDefs.h"

#include "CATBaseUnknown.h"
#include "CATString.h"

#include "CATListOfCATCurves.h"
#include "ListPOfCATFace.h"

class CATMathVector;
class CATMathPlane;
class CATIContainer_var;

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByCATSMGAlgoItfEnv IID IID_CATIMfgMultiAxisAlgorithm;
#else
extern "C" const IID IID_CATIMfgMultiAxisAlgorithm ;
#endif

/**
 * Interface for Multi-axis Machining Algorithms. 
 * Use CATInstantiateComponent to instanciate it with : 
 *<ul>
 * <li>MultiAxisSweepingInstanceName class name for multi-axis sweeping algorithm.
 * <li>MultiAxisContourDrivenInstanceName class name for multi-axis contour-driven algorithm.
 *</ul> 
 */  
class ExportedByCATSMGAlgoItfEnv CATIMfgMultiAxisAlgorithm: public CATBaseUnknown
{
  CATDeclareInterface;

  public:

/**
 * Sets a real parameter.
 * @param iAttributeName
 *   One of the following keys :
 *    <dl>
 *      <dt><tt>MfgAlgMachiningTolerance</tt>    <dd>Machining tolerance (>0.)
 *      <dt><tt>MfgAlgMaxDiscretizationStep</tt>  <dd>Maximum discretization step (>0.)
 *      <dt><tt>MfgAlgMaxDiscretizationAngle</tt>  <dd>Maximum discretization angle (>0.)
 *      <dt><tt>MfgAlgMaxDistance</tt>  <dd>StepOver - Distance on part (>0.)
 *      <dt><tt>MfgAlgOffsetOnGuide1</tt>  <dd>Offset on guide 1 (for multi-axis contour-driven only)
 *      <dt><tt>MfgAlgOffsetOnGuide2</tt>  <dd>Offset on guide 2 (for multi-axis contour-driven only)
 *    </dl>
 *
 * @param iAttributeValue
 *   The defined value of the parameter (in millimeter for lengths and degree for angles).
 *
 * @return
 *    <dl>
 *      <dt><tt>S_OK</tt>    <dd>if the method succeeds
 *      <dt><tt>E_FAIL</tt>  <dd>if iAttributeName is not valid or if iAttributeValue is out of range
 *    </dl>
 */    
    virtual HRESULT SetValue (const CATString & iAttributeName, const double iAttributeValue)=0;


/**
 * Sets a integer parameter.
 * @param iAttributeName
 *   One of the following keys :
 *    <dl>
 *      <dt><tt>MfgAlgMachiningMode</tt>    <dd>Tool path style (1:Zigzag / 2:Oneway)
 *      <dt><tt>MfgAlgStepoverSide</tt>  <dd>Stepover side (1:left / -1:right)
 *      <dt><tt>MfgAlgContouringMode</tt>  <dd>Guiding strategy (1:BetweenContour / 2:ParallelContour) ) (for multi-axis contour-driven only)
 *      <dt><tt>MfgAlgFromToContour</tt>  <dd>Contour direction (1:FromContour / 2: ToContour) (for multi-axis contour-driven only)
 *      <dt><tt>MfgAlgPositionOnGuide1</tt>  <dd>Position on Guide1 (1:In / 2: Out / 3:On) (for multi-axis contour-driven only)
 *      <dt><tt>MfgAlgPositionOnGuide2</tt>  <dd>Position on Guide2 (1:In / 2: Out / 3:On) (for multi-axis contour-driven only)
 *    </dl>
 *
 * @param iAttributeValue
 *   The defined value of the parameter.
 *
 * @return
 *    <dl>
 *      <dt><tt>S_OK</tt>    <dd>if the method succeeds
 *      <dt><tt>E_FAIL</tt>  <dd>if iAttributeName is not valid 
 *    </dl>
 */    
    virtual HRESULT SetValue (const CATString & iAttributeName, const int iAttributeValue)=0;

    
/**
 * Sets a direction parameter.
 * @param iDirectionName 
 *   One of the following keys :
 *    <dl>
 *      <dt><tt>MfgAlgViewDirection</tt>    <dd>View direction
 *      <dt><tt>MfgAlgStartDirection</tt>  <dd>Start direction
 *    </dl>
 *
 * @param iDirection
 *   The direction in global axis system .
 *
 * @return
 *    <dl>
 *      <dt><tt>S_OK</tt>    <dd>if the method succeeds
 *      <dt><tt>E_FAIL</tt>  <dd>if iDirectionName is not valid
 *    </dl>
 */    
    virtual HRESULT SetDirection (const CATString & iDirectionName, const CATMathVector & iDirection)=0;


/**
 * Sets a 2D geometry.
 * @param iGeometryType 
 *   One of the following keys :
 *    <dl>
 *      <dt><tt>MfgAlgParts</tt>    <dd>Part
 *    </dl>
 *
 * @param iListOfFaces
 *   The list of faces defining the geometry.
 *
 * @return
 *    <dl>
 *      <dt><tt>S_OK</tt>    <dd>if the method succeeds
 *      <dt><tt>E_FAIL</tt>  <dd>if iGeometryType is not valid or if iListOfFaces is empty
 *    </dl>
 */    
    virtual HRESULT SetSurfacicGeometry (const CATString & iGeometryType, const CATLISTP (CATFace) & iListOfFaces)=0;

/**
 * Sets a 1D geometry.
 * @param iGeometryType 
 *   One of the following keys :
 *    <dl>
 *      <dt><tt>MfgAlgLimitLine</tt>    <dd>Limiting contour
 *      <dt><tt>MfgAlgGuide1</tt>    <dd>First guide (for multi-axis contour-driven only)
 *      <dt><tt>MfgAlgGuide2</tt>    <dd>Second guide (for multi-axis contour-driven only)
 *      <dt><tt>MfgAlgStop1</tt>    <dd>First stop (for multi-axis contour-driven only)
 *      <dt><tt>MfgAlgStop2</tt>    <dd>Second stop (for multi-axis contour-driven only)
 *    </dl>
 *
 * @param iListOfCurves 
 *   The list of curves defining the geometry.
 *
 * @return
 *    <dl>
 *      <dt><tt>S_OK</tt>    <dd>if the method succeeds
 *      <dt><tt>E_FAIL</tt>  <dd>if iGeometryType is not valid
 *    </dl>
 */    
    virtual HRESULT SetWireFrameGeometry (const CATString & iGeometryType, const CATLISTP (CATCurve) & iListOfCurves)=0;

/**
 * Sets a specific tool.
 * If this method is not called, ball end mill tool of 10mm diameter will be taken into account during computation.
 * @param ihTool 
 *   The manufacturing tool (CATIMfgTool) 
 */    
    virtual HRESULT SetTool (const CATBaseUnknown_var &ihTool)=0;

 /**
 * Adds an axial motion at the end of a macro.
 * @param iMacro 
 *   Where motion is added 1:Approach / 2:Retract / 3:LinkingApproach / 4:LinkingRetract / 5:ReturnInALevelApproach / 6:ReturnInALevelRetract
 *
 * @param iDistance 
 *   The length of the motion.
 */    
    virtual HRESULT AddMacroAxialMotion (const int iMacro, const double iDistance =10.)=0;

/**
 * Adds a tangent motion at the end of a macro.
 * @param iMacro 
 *   Where motion is added 1:Approach / 2:Retract / 3:LinkingApproach / 4:LinkingRetract / 5:ReturnInALevelApproach / 6:ReturnInALevelRetract
 *
 * @param iDistance 
 *   The length of the motion.
 *
 * @param iVerticalAngle 
 *   The vertical angle of the motion.
 *
 * @param iHorizontalAngle 
 *   The horizontal angle of the motion.
 *
 * @return
 *    <dl>
 *      <dt><tt>S_OK</tt>    <dd>if the method succeeds
 *      <dt><tt>E_FAIL</tt>  <dd>if motion is added after an axial motion
 *    </dl>
 */    
    virtual HRESULT AddMacroTangentMotion (const int iMacro, const double iDistance =10., const double iVerticalAngle =0., const double iHorizontalAngle =0.)=0;

/**
 * Adds a ramping motion at the end of a macro.
 * Ths motion is not available on retract motions.
 * @param iMacro 
 *   Where motion is added 1:Approach / 3:LinkingApproach / 5:ReturnInALevelApproach
 *
 * @param iHorizontalSafetyDistance
 *   The horizontal safety distance of the motion
 *
 * @param iVerticalSafetyDistance
 *   The vertical safety distance of the motion
 *
 * @param iRampingAngle
 *   The ramping angle of the motion
 *
 * @return
 *    <dl>
 *      <dt><tt>S_OK</tt>    <dd>if the method succeeds
 *      <dt><tt>E_FAIL</tt>  <dd>if motion is added after an axial motion or on a retract macro
 *    </dl>
 */    
    virtual HRESULT AddMacroRampingMotion (const int iMacro, const double iHorizontalSafetyDistance=10., const double iVerticalSafetyDistance=10., const double iRampingAngle=20.)=0;

/**
 * Adds a circular motion at the end of a macro.
 * @param iMacro 
 *   Where motion is added 1:Approach / 2:Retract / 3:LinkingApproach / 4:LinkingRetract / 5:ReturnInALevelApproach / 6:ReturnInALevelRetract
 *
 * @param iAngularSector
 *   The angular sector of the motion
 *
 * @param iAngularOrientation 
 *   The angular orientation of the motion
 *
 * @param iRadius 
 *   The radius of the motion (> 0.)
 *
 * @return
 *    <dl>
 *      <dt><tt>S_OK</tt>    <dd>if the method succeeds
 *      <dt><tt>E_FAIL</tt>  <dd>if motion is added after an axial motion
 *    </dl>
 */    
    virtual HRESULT AddMacroCircularMotion (const int iMacro, const double iAngularSector=90., const double iAngularOrientation=0., const double iRadius=10.)=0;

/**
 * Adds a motion perpendicular to a plane at the end of a macro.
 * @param iMacro 
 *   Where motion is added 1:Approach / 2:Retract / 3:LinkingApproach / 4:LinkingRetract / 5:ReturnInALevelApproach / 6:ReturnInALevelRetract
 *
 * @param iPlane
 *   The plane of the motion
 *
 * @return
 *    <dl>
 *      <dt><tt>S_OK</tt>    <dd>if the method succeeds
 *      <dt><tt>E_FAIL</tt>  <dd>if motion is added after an axial motion
 *    </dl>
 */    
    virtual HRESULT AddMacroToAPlaneMotion (const int iMacro, const CATMathPlane & iPlane)=0;

 /**
 * Adds a motion along a line at the end of a macro.
 * @param iMacro 
 *   Where motion is added 1:Approach / 2:Retract / 3:LinkingApproach / 4:LinkingRetract / 5:ReturnInALevelApproach / 6:ReturnInALevelRetract
 *
 * @param iDistance
 *   The length of the motion
 *
 * @param iLineDirection 
 *   The direction of the motion
 */    
   virtual HRESULT AddMacroAlongALineMotion (const int iMacro, const double iDistance, const CATMathVector & iLineDirection)=0;

 /**
 * Adds a user syntax at the end of a macro.
 * @param iMacro 
 *   Where motion is added 1:Approach / 2:Retract / 3:LinkingApproach / 4:LinkingRetract / 5:ReturnInALevelApproach / 6:ReturnInALevelRetract
 *
 * @param iStatement
 *   The word to add
 */    
   virtual HRESULT AddMacroSyntax (const int iMacro, const CATUnicodeString & iStatement)=0;

 /**
 * Runs algorithm.
 * @param ihContainer  
 *   The tool path container of the process where the result is created
 *
 * @param ohToolPath
 *   The machining tool path computed
 *
 * @return
 *    <dl>
 *      <dt><tt>S_OK</tt>    <dd>if the method succeeds
 *      <dt><tt>E_FAIL</tt>  <dd>if computation fails
 *    </dl>
 */    
    virtual HRESULT ComputeToolPath (const CATIContainer_var & ihContainer, CATBaseUnknown_var & ohToolPath)=0;

 /**
 * Permits to control the activation and deactivation of the collisions checking. (By default no collisions checking is made)
 * @param iColl
 * if TRUE -> Activate collisions checking, FALSE -> Deactivate collisions checking.
 * @return
 *    <dl>
 *      <dt><tt>S_OK</tt>    <dd>if the method succeeds
 *      <dt><tt>E_FAIL</tt>  <dd>if computation fails
 *    </dl>
 */ 
	virtual HRESULT ActivateCollisionChecking(const CATBoolean iColl)=0;

 /**
 * Add a default clearance with distance mode, distance = 3mm. (By default no clearance is added)
 * @param iClear
 * if TRUE -> Add a clearance, FALSE -> No clearance will be added.
 * @return
 *    <dl>
 *      <dt><tt>S_OK</tt>    <dd>if the method succeeds
 *      <dt><tt>E_FAIL</tt>  <dd>if computation fails
 *    </dl>
 */ 
	virtual HRESULT AddClearance(const CATBoolean iClear)=0;

 /**
 * Unsets all parameters and geometry, already set.
 **/    
    virtual HRESULT UnsetData ()=0;
};

CATDeclareHandler(CATIMfgMultiAxisAlgorithm, CATBaseUnknown) ;

#endif
