#ifndef __CATMfgMultiAxisAlgoDefs_h__
#define __CATMfgMultiAxisAlgoDefs_h__
//=================================================================================
// COPYRIGHT DASSAULT SYSTEMES 2006
//=================================================================================

/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */

#include "CATSMGAlgoItfEnv.h"
class CATUnicodeString;
class CATString;

// Instance name
ExportedByCATSMGAlgoItfEnv extern const CATUnicodeString MultiAxisSweepingInstanceName;
ExportedByCATSMGAlgoItfEnv extern const CATUnicodeString MultiAxisContourDrivenInstanceName;

// Real parameter
ExportedByCATSMGAlgoItfEnv extern const CATString MfgAlgMachiningTolerance;
ExportedByCATSMGAlgoItfEnv extern const CATString MfgAlgMaxDiscretizationStep;
ExportedByCATSMGAlgoItfEnv extern const CATString MfgAlgMaxDiscretizationAngle;
ExportedByCATSMGAlgoItfEnv extern const CATString MfgAlgMaxDistance;
ExportedByCATSMGAlgoItfEnv extern const CATString MfgAlgOffsetOnGuide1;
ExportedByCATSMGAlgoItfEnv extern const CATString MfgAlgOffsetOnGuide2;

// Integer parameter
ExportedByCATSMGAlgoItfEnv extern const CATString MfgAlgMachiningMode;
ExportedByCATSMGAlgoItfEnv extern const CATString MfgAlgStepoverSide;
ExportedByCATSMGAlgoItfEnv extern const CATString MfgAlgContouringMode;
ExportedByCATSMGAlgoItfEnv extern const CATString MfgAlgFromToContour;
ExportedByCATSMGAlgoItfEnv extern const CATString MfgAlgPositionOnGuide1;
ExportedByCATSMGAlgoItfEnv extern const CATString MfgAlgPositionOnGuide2;

// Direction
ExportedByCATSMGAlgoItfEnv extern const CATString MfgAlgViewDirection;
ExportedByCATSMGAlgoItfEnv extern const CATString MfgAlgStartDirection;

// Geometry
ExportedByCATSMGAlgoItfEnv extern const CATString MfgAlgParts;
ExportedByCATSMGAlgoItfEnv extern const CATString MfgAlgLimitLine;
ExportedByCATSMGAlgoItfEnv extern const CATString MfgAlgGuide1;
ExportedByCATSMGAlgoItfEnv extern const CATString MfgAlgGuide2;
ExportedByCATSMGAlgoItfEnv extern const CATString MfgAlgStop1;
ExportedByCATSMGAlgoItfEnv extern const CATString MfgAlgStop2;

#endif

