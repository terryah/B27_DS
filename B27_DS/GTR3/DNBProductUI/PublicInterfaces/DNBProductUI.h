//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/** @CAA2Required */
/*---------------------------------------------------------------------*/
/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */
/*---------------------------------------------------------------------*/
//
//    This Header file is included by all the Module.h header files in 
//    PublicInterfaces
//

#ifndef DNBPRODUCTUI_H

#define DNBPRODUCTUI_H DNBProductUI

#ifdef _WINDOWS_SOURCE
#if defined(__DNBProductUI)
#define ExportedByDNBProductUI __declspec(dllexport)
#else
#define ExportedByDNBProductUI __declspec(dllimport)
#endif
#else
#define ExportedByDNBProductUI
#endif

#endif /* DNBPRODUCTUI_H */
