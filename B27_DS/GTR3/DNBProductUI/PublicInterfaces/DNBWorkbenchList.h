/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBWorkbenchList.h
//      Implementation of a list of values of DNBWorkbench.
//
//==============================================================================
//
// Usage notes: 
//      Collection class for values of access elements. All functions defined
//      on a list are available.
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     sha          10/16/2001   Initial implementation
//     sha          05/20/2002   Removed useless code (for improving the Code 
//                               Coverage)
//
//==============================================================================
#ifndef DNB_WORKBENCH_LIST_H
#define DNB_WORKBENCH_LIST_H

#include <DNBProductUI.h>

class DNBWorkbench;

#include <CATLISTP_Clean.h>

//#include <CATLISTP_AllFunct.h>
#define	CATLISTP_CtorFromArrayPtrs
#define	CATLISTP_Append
#define	CATLISTP_Compare
#define	CATLISTP_RemoveDuplicates
#define	CATLISTP_RemoveValue
#define	CATLISTP_RemoveAll

#include <CATLISTP_Declare.h>

#ifdef CATCOLLEC_ExportedBy
#undef CATCOLLEC_ExportedBy
#endif
#define CATCOLLEC_ExportedBy ExportedByDNBProductUI

CATLISTP_DECLARE( DNBWorkbench )

typedef CATLISTP( DNBWorkbench ) DNBWorkbenchListP;

#endif  // DNB_WORKBENCH_LIST_H
