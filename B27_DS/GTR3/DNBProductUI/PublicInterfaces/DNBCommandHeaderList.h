/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBCommandHeaderList.h
//      Implementation of a list of values of DNBCommandHeader.
//
//==============================================================================
//
// Usage notes: 
//      Collection class for values of access elements. All functions defined
//      on a list are available.
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     sha          10/16/2001   Initial implementation
//     sha          01/18/2002   Include ExportBy clause
//     sha          05/20/2002   Removed useless code (for improving the Code 
//                               Coverage)
//
//==============================================================================
#ifndef DNB_COMMAND_HEADER_LIST_H
#define DNB_COMMAND_HEADER_LIST_H

#include <DNBProductUI.h>

class DNBCommandHeader;

//------------------------------------------------------------------------------
// CATLISTP
//------------------------------------------------------------------------------

#include <CATLISTP_Clean.h>

//#include <CATLISTP_AllFunct.h>
#define	CATLISTP_Append
#define	CATLISTP_RemovePosition
#define	CATLISTP_RemoveList

#include <CATLISTP_Declare.h>

#ifdef CATCOLLEC_ExportedBy
#undef CATCOLLEC_ExportedBy
#endif
#define CATCOLLEC_ExportedBy ExportedByDNBProductUI

CATLISTP_DECLARE( DNBCommandHeader )

typedef CATLISTP( DNBCommandHeader ) DNBCommandHeaderListP;

#endif  // DNB_COMMAND_HEADER_LIST_H
