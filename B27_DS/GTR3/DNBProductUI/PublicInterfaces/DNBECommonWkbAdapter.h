/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2004
//==============================================================================
//
// DNBECommonWkbAdapter.h
//      Common workbench adapter.
//
//==============================================================================
//
// Usage notes: 
//      This is the base class for all workbench.
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     sha          08/20/2004   Initial implementation
//     mmh          06/06/2007   Fix 586205: Declare __workbenches as public
//
//==============================================================================
#ifndef DNBECommonWkbAdapter_H
#define DNBECommonWkbAdapter_H

#include <CATBaseUnknown.h>
#include <CATListPV.h>
#include <CATString.h>
#include <CATCmdContainer.h>
#include <DNBProductUI.h>

#include <DNBCommandHeaderList.h>
#include <DNBWorkbenchList.h>

class DNBAccess;
class DNBCommandHeader;
class DNBWorkbench;

class CATCmdWorkbench;

//------------------------------------------------------------------------------

/**
 * Common workbench adapter.
 * <b>Role</b>: The workbench is the object seen by the end user in place of the
 * workshop. As in workshops, tools are grouped in toolbars and menus. 
 * These tools are commands which are handled by workshop and its workbenches by
 * means of command headers. 
 * <br><br>
 * The workshop dedicated to this workbench features a specific workbench 
 * interface, see @href CATIWorkshop#GetWorkbenchInterface, which derives from 
 * <tt>CATIWorkbench</tt>.Each workbench of this workshop is an implementation 
 * of this specific workbench interface. 
 * <br><br>
 * To enable the instanciation of the workbench you must:
 * <ul>
 * <li> Create a workbench factory interface which derives from @href 
 * CATIGenericFactory. </li>
 * <li> Implement this factory interface with the help of these two macros: 
 * @href CATDeclareConfigurationFactory and 
 * @href CATImplementConfigurationFactory. </li>
 * </ul>
 * @see CATIWorkshop, CATIWorkbenchAddin
 */
class ExportedByDNBProductUI DNBECommonWkbAdapter : 
    public CATBaseUnknown
{
    CATDeclareClass;

public:

    /*
	 * Standard constructor for an implementation class
	 */
    DNBECommonWkbAdapter( const CATString &str = "", const int level = 2 );

	/*
	 * Standard destructor for an implementation class
	 */
    virtual ~DNBECommonWkbAdapter();

    /**
     * Instantiates the command headers.  
     * <br><b>Role</b>: Each command you want to make available in your workshop
     * or workbench must have a command header. The command header holds the 
     * necessary information to load the command, such as the name of the shared
     * library in which the command's executable code is located, the name of 
     * the command class, and the data to pass to the command's code when this 
     * command becomes the current one. The command header has resources for 
     * each command to display such as the command name shown to the end user, 
     * its ToolTip displayed in a balloon, its help message, and its icon.
     * <br>Each instance of header is deleted by V5.
     * @see CATCommandHeader
     */
    void CreateCommands();
    
    /**
     * Creates the workbench accesses.
     * <br><b>Role</b>: Creates the containers for the workbench, the menubar, 
     * the menus, and the toolbars, the starters for the commands, and arranges
     * the commands in the menus and toolbars.
     * With the @href NewAccess macro you have explanations to create  each 
     * component of the workbench and how to arrange it.
     * <br><b>Warning</b>:
     * The workbench's name must be the same as the class implementing this 
     * interface.
     * @return 
     *   The workbench's container . 
     */
    CATCmdWorkbench * CreateWorkbench();
    
    /**
     * Returns the name of the interface exposed by the workbench to create 
     * add-ins.
     * <br><b>Role</b>: To enable client applications of your workbench to add 
     * their own addins, you should provide an interface that the client 
     * application will implement. This interface should derive from the @href 
     * CATIWorkbenchAddin interface. 
     * <br><b>Warning</b>: The name of this interface must be 
     * pppIxxx<b>Addin</b>, where ppp is your application prefix (such as CAT 
     * for CATIA) and xxx is the name of your workbench.
     */
    CATClassId GetAddinInterface();

    /**
     * Replaces specifics interfaces by customized interfaces.
     */
    void GetCustomInterfaces(CATListPV* defaultIIDList,
                             CATListPV* customIIDList);

    struct DNBWorkbenches
    {
        DNBWorkbenches();
        ~DNBWorkbenches();

        DNBWorkbenchListP _wkbs;

        void CleanUpList();
    };

    static DNBWorkbenches           __workbenches;

protected:

    /**
     * Override this method in order to define your application's workbench
     * <br><b>Role</b>: Defines the command headers as well as the toolbars/menus
     * of your application.
     */
    virtual void DefineWorkbench() = 0;

    /**
     * Use this method to append a new toolbar to your workbench.
     */
    void AppendToolbar( const CATString& toolbarID,
                        const CATString& fatherID="",
                        int visibility = 1,
                        CATCmdContainer::ComponentPosition position = CATCmdContainer::Right );
    /**
     * Use this method to append a new menubar to your workbench.
     */
    void AppendMenubar( const CATString& menubarID,
                        const CATString& fatherID="" );
    /**
     * Use this method to append a new command header to a toolbar and/or a menubar
     * (by providing a pointer to an existing command header)
     */
    void AppendCmdHdr( DNBCommandHeader *cmdHdr,
                       const CATString& toolbarID,
                       const CATString& menubarID );
    /**
     * Use this method to append a new command header to a toolbar and/or a menubar.
     * (by providing the ID of an existing command header)
     */
    void AppendCmdHdr( const CATString& headerID,
                       const CATString& toolbarID,
                       const CATString& menubarID,
                       boolean force = FALSE );
    /**
     * Use this method to append a new separator to a toolbar and/or a menubar.
     */
    void AppendSep( const CATString& toolbarID,
                    const CATString& menubarID );

    /**
     * Use this method to remove a toolbar from your workbench.
     */
    void RemoveToolbar( const CATString& toolbarID,
                        boolean destroyHeaders = TRUE );
    /**
     * Use this method to remove a menubar from your workbench.
     */
    void RemoveMenubar( const CATString& menubarID,
                        boolean destroyHeaders = TRUE );
    /**
     * Use this method to remove a command header from a toolbar and/or a menubar.
     * (by providing the ID of an existing command header)
     */
    void RemoveCmdHdr( const CATString& headerID,
                       boolean destroyHeader = TRUE,
                       boolean force = FALSE );
    /**
     * Use this method to remove an existing separator from a toolbar and/or a menubar.
     */
    void RemoveSep( const CATString& headerID );
    /**
     * Use this method to retrieve a command header from command header list.
     * (by providing the ID of an existing command header)
     */
    DNBCommandHeader* LocateCmdHdr( const CATString& headerID );

protected:
    //
    // THIS SECTION FOR INTERNAL USAGE ONLY
    // DO NOT USE IN CLIENT CODE
    //
    void AppendSep( const CATString& headerID,
                    DNBAccess* toolbar,
                    DNBAccess* menubar );
    
    static DNBWorkbench* FindWorkbench( const CATString &workbenchID );

    void RetrieveCrtWorkbench();
    void SetupCrtWorkbench();
    void DeleteCrtWorkbench();

    CATString                       _workbenchID;
    DNBWorkbench*                   _workbench;
    DNBAccess*                      _toolbar;
    DNBAccess*                      _menubar;
    DNBCommandHeaderListP*          _commandHeaderList;
    int                             _level;

private:
    // The copy constructor and the equal operator must not be implemented
    // -------------------------------------------------------------------
    DNBECommonWkbAdapter( DNBECommonWkbAdapter& );
    DNBECommonWkbAdapter& operator=( DNBECommonWkbAdapter& );
};

//------------------------------------------------------------------------------

#endif // DNBECommonWkbAdapter_H
