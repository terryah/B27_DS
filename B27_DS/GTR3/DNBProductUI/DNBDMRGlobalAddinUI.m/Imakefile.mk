#
#   Imakefile.mk for DNBDMRGlobalAddinUI
#   Copyright (C) DELMIA Corp., 2012
#
BUILT_OBJECT_TYPE   = SHARED LIBRARY

COMMON_LINK_WITH    = JS0GROUP                  \ # System
                      JS0FM                     \ # System
                      CATApplicationFrame       \ # ApplicationFrame

LINK_WITH           = $(COMMON_LINK_WITH)

INCLUDED_MODULES    = 

#
# Define the build options for the current module.
#
OS      = Windows_NT
BUILD   = YES

OS      = IRIX
BUILD   = YES

OS      = SunOS
BUILD   = YES

OS      = AIX
BUILD   = YES

OS      = HP-UX
BUILD   = YES

OS      = win_a
BUILD   = NO
