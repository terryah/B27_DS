// COPYRIGHT Dassault Systemes 2003

/**
* @CAA2Level L1
* @CAA2Usage U3
*/

//===================================================================
//
// CATIAToleranceSheetSettingAtt.idl
// Automation interface for the ToleranceSheetSettingAtt element 
//
//===================================================================
//
// Usage notes:
//   New interface: describe its use here
//
//===================================================================
//
//  Dec 2003  Creation: Code generated by the CAA wizard  sob
//===================================================================
#ifndef CATIAToleranceSheetSettingAtt_IDL
#define CATIAToleranceSheetSettingAtt_IDL
/*IDLREP*/

#include "CATIASettingController.idl"
#include "CATVariant.idl"
#include "CATSafeArray.idl"
#include "CATBSTR.idl"

/**  
* The interface to access a CATIAToleranceSheetSettingAtt.
* This interface may be used to read or modify in the CATIA\Tools\Option the settings values
* of Tolerance sheet.
*/
interface CATIAToleranceSheetSettingAtt : CATIASettingController 
{

#pragma PROPERTY DefaultTolerance
    /**
     * Returns or sets the DefaultTolerance parameter.
     * <br><b>Role</b>:Return or Set the DefaultTolerance parameter if it is possible
	 * in the current administrative context. In user mode this method will always
	 * return E_FAIL.
	 * @param oDefaultTolerance
	 *	<b>Legal values</b>:
	 *	<br><tt>0 :</tt>   to not accept a default tolerance
	 * 	<br><tt>1 :</tt>   to accept a default tolerance.
     */
     HRESULT get_DefaultTolerance(out /*IDLRETVAL*/ short		oDefaultTolerance);
     HRESULT put_DefaultTolerance( in short	iDefaultTolerance );

	 /** 
	 * Retrieves environment informations for the DefaultTolerance parameter.
	 * <br><b>Role</b>:Retrieves the state of the DefaultTolerance parameter 
	 * in the current environment. 
	 * @param ioAdminLevel
	 *       <br>If the parameter is locked, AdminLevel gives the administration
	 *       level that imposes the value of the parameter.
	 *	 <br>If the parameter is not locked, AdminLevel gives the administration
	 *       level that will give the value of the parameter after a reset.
	 * @param ioLocked
	 *      Indicates if the parameter has been locked.
	 * @return 
	 *      Indicates if the parameter has been explicitly modified or remain
	 *      to the administrated value.
     */
     HRESULT GetDefaultToleranceInfo(inout		CATBSTR		ioAdminLevel
						, inout					CATBSTR		ioLocked
						, out  /*IDLRETVAL*/	boolean		oModified
						);

    /**
     * Locks or unlocks the DefaultTolerance parameter.
	 * <br><b>Role</b>:Locks or unlocks the DefaultTolerance parameter if it is possible
	 * in the current administrative context. In user mode this method will always
	 * return E_FAIL.
	 * @param iLocked
	 *	the locking operation to be performed
	 *	<b>Legal values</b>:
	 *	<br><tt>TRUE :</tt>   to lock the parameter.
	 * 	<br><tt>FALSE:</tt>   to unlock the parameter.
     */
     HRESULT SetDefaultToleranceLock( in  boolean iLocked );


#pragma PROPERTY LengthMaxTolerance
    /**
     * Returns or sets the LengthMaxTolerance parameter.
     * <br><b>Role</b>:Return or Set the LengthMaxTolerance parameter if it is possible
	 * in the current administrative context. In user mode this method will always
	 * return E_FAIL.
	 * @param oLengthMaxTolerance
     *    The length maximum tolerance value.
     */
     HRESULT get_LengthMaxTolerance(out /*IDLRETVAL*/ double		oLengthMaxTolerance);
     HRESULT put_LengthMaxTolerance(in double	iLengthMaxTolerance );

	 /** 
	 * Retrieves environment informations for the LengthMaxTolerance parameter.
	 * <br><b>Role</b>:Retrieves the state of the LengthMaxTolerance parameter 
	 * in the current environment. 
	 * @param ioAdminLevel
	 *       <br>If the parameter is locked, AdminLevel gives the administration
	 *       level that imposes the value of the parameter.
	 *	 <br>If the parameter is not locked, AdminLevel gives the administration
	 *       level that will give the value of the parameter after a reset.
	 * @param ioLocked
	 *      Indicates if the parameter has been locked.
	 * @return 
	 *      Indicates if the parameter has been explicitly modified or remain
	 *      to the administrated value.
     */
     HRESULT GetLengthMaxToleranceInfo	( inout					CATBSTR		ioAdminLevel
						, inout					CATBSTR		ioLocked
						, out  /*IDLRETVAL*/	boolean		oModified
						);

    /**
     * Locks or unlocks the LengthMaxTolerance parameter.
	 * <br><b>Role</b>:Locks or unlocks the LengthMaxTolerance parameter if it is possible
	 * in the current administrative context. In user mode this method will always
	 * return E_FAIL.
	 * @param iLocked
	 *	the locking operation to be performed
	 *	<b>Legal values</b>:
	 *	<br><tt>TRUE :</tt>   to lock the parameter.
	 * 	<br><tt>FALSE:</tt>   to unlock the parameter.
     */
     HRESULT SetLengthMaxToleranceLock( in  boolean iLocked );

#pragma PROPERTY LengthMinTolerance
    /**
     * Returns or sets the LengthMinTolerance parameter.
     * <br><b>Role</b>:Return or Set the LengthMinTolerance parameter if it is possible
	 * in the current administrative context. In user mode this method will always
	 * return E_FAIL.
	 * @param oLengthMinTolerance
     *    The length minimum tolerance value.
     */
     HRESULT get_LengthMinTolerance(out /*IDLRETVAL*/ double		oLengthMinTolerance);
     HRESULT put_LengthMinTolerance(in double	iLengthMinTolerance );

	 /** 
	 * Retrieves environment informations for the LengthMinTolerance parameter.
	 * <br><b>Role</b>:Retrieves the state of the LengthMinTolerance parameter 
	 * in the current environment. 
	 * @param ioAdminLevel
	 *       <br>If the parameter is locked, AdminLevel gives the administration
	 *       level that imposes the value of the parameter.
	 *	 <br>If the parameter is not locked, AdminLevel gives the administration
	 *       level that will give the value of the parameter after a reset.
	 * @param ioLocked
	 *      Indicates if the parameter has been locked.
	 * @return 
	 *      Indicates if the parameter has been explicitly modified or remain
	 *      to the administrated value.
     */
     HRESULT GetLengthMinToleranceInfo	( inout					CATBSTR		ioAdminLevel
						, inout					CATBSTR		ioLocked
						, out  /*IDLRETVAL*/	boolean		oModified
						);

    /**
     * Locks or unlocks the LengthMinTolerance parameter.
	 * <br><b>Role</b>:Locks or unlocks the LengthMinTolerance parameter if it is possible
	 * in the current administrative context. In user mode this method will always
	 * return E_FAIL.
	 * @param iLocked
	 *	the locking operation to be performed
	 *	<b>Legal values</b>:
	 *	<br><tt>TRUE :</tt>   to lock the parameter.
	 * 	<br><tt>FALSE:</tt>   to unlock the parameter.
     */
     HRESULT SetLengthMinToleranceLock( in  boolean iLocked );

#pragma PROPERTY AngleMaxTolerance
    /**
     * Returns or sets the AngleMaxTolerance parameter.
     * <br><b>Role</b>:Return or Set the AngleMaxTolerance parameter if it is possible
	 * in the current administrative context. In user mode this method will always
	 * return E_FAIL.
	 * @param oAngleMaxTolerance
     *    The angle maximum tolerance value.
     */
     HRESULT get_AngleMaxTolerance(out /*IDLRETVAL*/ double		oAngleMaxTolerance);
     HRESULT put_AngleMaxTolerance(in double	iAngleMaxTolerance );

	 /** 
	 * Retrieves environment informations for the AngleMaxTolerance parameter.
	 * <br><b>Role</b>:Retrieves the state of the AngleMaxTolerance parameter 
	 * in the current environment. 
	 * @param ioAdminLevel
	 *       <br>If the parameter is locked, AdminLevel gives the administration
	 *       level that imposes the value of the parameter.
	 *	 <br>If the parameter is not locked, AdminLevel gives the administration
	 *       level that will give the value of the parameter after a reset.
	 * @param ioLocked
	 *      Indicates if the parameter has been locked.
	 * @return 
	 *      Indicates if the parameter has been explicitly modified or remain
	 *      to the administrated value.
     */
     HRESULT GetAngleMaxToleranceInfo	( inout					CATBSTR		ioAdminLevel
						, inout					CATBSTR		ioLocked
						, out  /*IDLRETVAL*/	boolean		oModified
						);

    /**
     * Locks or unlocks the AngleMaxTolerance parameter.
	 * <br><b>Role</b>:Locks or unlocks the AngleMaxTolerance parameter if it is possible
	 * in the current administrative context. In user mode this method will always
	 * return E_FAIL.
	 * @param iLocked
	 *	the locking operation to be performed
	 *	<b>Legal values</b>:
	 *	<br><tt>TRUE :</tt>   to lock the parameter.
	 * 	<br><tt>FALSE:</tt>   to unlock the parameter.
     */
     HRESULT SetAngleMaxToleranceLock( in  boolean iLocked );

#pragma PROPERTY AngleMinTolerance
    /**
     * Returns or sets the AngleMinTolerance parameter.
     * <br><b>Role</b>:Return or Set the AngleMinTolerance parameter if it is possible
	 * in the current administrative context. In user mode this method will always
	 * return E_FAIL.
	 * @param oAngleMinTolerance
     *    The angle minimum tolerance value.
     */
     HRESULT get_AngleMinTolerance(out /*IDLRETVAL*/ double		oAngleMinTolerance);
     HRESULT put_AngleMinTolerance(in double	iAngleMinTolerance );

	 /** 
	 * Retrieves environment informations for the AngleMinTolerance parameter.
	 * <br><b>Role</b>:Retrieves the state of the AngleMinTolerance parameter 
	 * in the current environment. 
	 * @param ioAdminLevel
	 *       <br>If the parameter is locked, AdminLevel gives the administration
	 *       level that imposes the value of the parameter.
	 *	 <br>If the parameter is not locked, AdminLevel gives the administration
	 *       level that will give the value of the parameter after a reset.
	 * @param ioLocked
	 *      Indicates if the parameter has been locked.
	 * @return 
	 *      Indicates if the parameter has been explicitly modified or remain
	 *      to the administrated value.
     */
     HRESULT GetAngleMinToleranceInfo	( inout					CATBSTR		ioAdminLevel
						, inout					CATBSTR		ioLocked
						, out  /*IDLRETVAL*/	boolean		oModified
						);

    /**
     * Locks or unlocks the AngleMinTolerance parameter.
	 * <br><b>Role</b>:Locks or unlocks the AngleMinTolerance parameter if it is possible
	 * in the current administrative context. In user mode this method will always
	 * return E_FAIL.
	 * @param iLocked
	 *	the locking operation to be performed
	 *	<b>Legal values</b>:
	 *	<br><tt>TRUE :</tt>   to lock the parameter.
	 * 	<br><tt>FALSE:</tt>   to unlock the parameter.
     */
     HRESULT SetAngleMinToleranceLock( in  boolean iLocked );
};

// Interface name : CATIAToleranceSheetSettingAtt
#pragma ID CATIAToleranceSheetSettingAtt "DCE:4dfda8fc-ebe7-4c49-96c9efce7d86f6c4"
#pragma DUAL CATIAToleranceSheetSettingAtt

// VB object name : ToleranceSheetSettingAtt (Id used in Visual Basic)
#pragma ID ToleranceSheetSettingAtt "DCE:f97df34d-1101-4698-82a573a99d981e2a"
#pragma ALIAS CATIAToleranceSheetSettingAtt ToleranceSheetSettingAtt

#endif
