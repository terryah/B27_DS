/* -*-c++-*- */
#ifndef CATListOfCATIType_h
#define CATListOfCATIType_h

// COPYRIGHT DASSAULT SYSTEMES 2000

/** 
 * @CAA2Level L1
 * @CAA2Usage U1 
 */

/*  CAA2Reviewed */


#include "CATLifSpecs.h"

#include "CATIType.h"

#include "CATLISTHand_Clean.h"

#include "CATLISTHand_AllFunct.h"

#include "CATLISTHand_Declare.h"

#undef	CATCOLLEC_ExportedBy
#define	CATCOLLEC_ExportedBy	ExportedByCATLifSpecs

CATLISTHand_DECLARE( CATIType_var );

#endif


