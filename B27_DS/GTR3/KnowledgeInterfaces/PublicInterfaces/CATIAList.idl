// COPYRIGHT Dassault Systemes 2002

/** 
 * @CAA2Level L1
 * @CAA2Usage U3
 */

//===================================================================
//
// CATIAList.idl
// Automation interface for the List element 
//
#ifndef CATIAList_IDL
#define CATIAList_IDL
/*IDLREP*/

#include "CATIACollection.idl"
#include "CATVariant.idl"

interface CATIAFeatureGenerator;

/**  
 * Represents a CATIAList.  
 */
interface CATIAList : CATIACollection 
{
	/**
     * Retrieves a Feature using its index or its name from the
     * Features collection.
     * @param iIndex
     *   The index or the name of the Feature to retrieve from
     *   the collection of Features.
     *   As a numerics, this index is the rank of the Feature
     *   in the collection.
     *   The index of the first Feature in the collection is 1, and
     *   the index of the last Feature is Count.
     *   As a string, it is the name you assigned to the Feature using
     *   the @href CATIABase#Name property or when creating the Feature.
     * @return The retrieved Feature
     * <! @sample >
     * <dt><b>Example:</b>
     * <dd>
     * This example retrieves the last Feature in the <tt>Features</tt>
     * collection. 
     * <pre>
     * Dim lastFeature As CATIABase
     * Set lastFeature = Features.<font color="red">Item</font>(Features.Count)
     * </pre>
     * </dl>
     */
    HRESULT     Item
                ( in                CATVariant            iIndex
                , out /*IDLRETVAL*/ CATIABase			oFeature
                );

	/**
	 * Sets an item in the list at a position. Does an AddRef on the item.
	 * Returns E_FAIL if the object type is not correct or the index is
	 * out of bounds.
	 * Returns E_FAIL if trying to set an already existing
	 * element while IsDuplicateElementsAllowed is false.
	 * @arg index Starts at 1.
	 */
    HRESULT     Replace
                ( in                CATVariant            iIndex
                , in				CATIABase			iItemValue
                );


	/**
	 * Adds an item at the end of the list. Does an AddRef on the item.
	 * Returns E_FAIL if the object type is not correct.
	 * Will return E_FAIL if trying to set an already existing
	 * element while IsDuplicateElementsAllowed is false.
	 */
    HRESULT     Add
                ( in				CATIABase			iItemValue
                );


	/**
	 * Reorders an element by moving it from the current position to the target position.
	 * Doesn't change the list if either position is out of the list.
	 * Return E_FAIL if cannot reorder.
	 * @arg iCurrent current position of the item to reorder.
	 * @arg iTarget target position.
	 */
	HRESULT		Reorder
                ( in                CATVariant            iIndexCurrent
                , in				CATVariant			  iIndexTarget
                );


    /**
     * Removes a Feature from the Features collection.
     * @param iIndex
     *   The index or the name of the Feature to retrieve from
     *   the collection of Features.
     *   As a numerics, this index is the rank of the Feature
     *   in the collection.
     *   The index of the first Feature in the collection is 1, and
     *   the index of the last Feature is Count.
     *   As a string, it is the name you assigned to the Feature using
     *   the @href CATIABase#Name property or when creating the Feature.
     * </dl>
     * <dt><b>Example:</b>
     * <dd>
     * This example removes the Feature named <tt>density</tt> from
     * the <tt>Features</tt> collection.
     * <pre>
     * Features.<font color="red">Remove</font>("density")
     * </pre>
     * </dl>
      */
     HRESULT   Remove(in CATVariant iIndex);

	/**
    	* @nodoc
	*/
#pragma PROPERTY FeatureGenerator
	/**
    	* @nodoc
	* Returns the feature generator.
	*/
	HRESULT get_FeatureGenerator(inout /*IDLRETVAL*/ CATIAFeatureGenerator oGenerator);
	
};

// Interface name : CATIAList
#pragma ID CATIAList "DCE:6e155dc8-7916-11d6-bea80002b349a513"
#pragma DUAL CATIAList

// VB object name : List (Id used in Visual Basic)
#pragma ID List "DCE:6e155dc9-7916-11d6-bea80002b349a513"
#pragma ALIAS CATIAList List

#endif
