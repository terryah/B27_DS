// COPYRIGHT DASSAULT SYSTEMES 2000


#ifndef CATIForwardsDecl_h
#define CATIForwardsDecl_h


/** 
 * @CAA2Level L1
 * @CAA2Usage U1 
 */

class CATIInstanceListener;
class CATIType;

class CATIValue_var;
class CATIType_var;
class CATIAttributesDescription_var;
class CATIInstance_var;
class CATIInstanceListener_var;
class CATIInstanciation_var;
class CATITypeDictionary_var;
class CATIInstance;
class CATIType;
class CATIValue;
class CATIInstanceAttributes;
class CATIInstanceListener;
//class CATIInstanciationListener;
#endif
