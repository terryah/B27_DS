/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAParameterSet_h
#define CATIAParameterSet_h

#ifndef ExportedByKnowledgePubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __KnowledgePubIDL
#define ExportedByKnowledgePubIDL __declspec(dllexport)
#else
#define ExportedByKnowledgePubIDL __declspec(dllimport)
#endif
#else
#define ExportedByKnowledgePubIDL
#endif
#endif

#include "CATIABase.h"

class CATIAParameterSets;
class CATIAParameters;

extern ExportedByKnowledgePubIDL IID IID_CATIAParameterSet;

class ExportedByKnowledgePubIDL CATIAParameterSet : public CATIABase
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_ParameterSets(CATIAParameterSets *& oParameterSets)=0;

    virtual HRESULT __stdcall get_DirectParameters(CATIAParameters *& oParameters)=0;

    virtual HRESULT __stdcall get_AllParameters(CATIAParameters *& oParameters)=0;


};

CATDeclareHandler(CATIAParameterSet, CATIABase);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
