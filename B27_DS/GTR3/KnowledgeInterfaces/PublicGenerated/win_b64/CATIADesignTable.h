/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIADesignTable_h
#define CATIADesignTable_h

#include "CATCORBABoolean.h"
#include "CAT_VARIANT_BOOL.h"

#ifndef ExportedByKnowledgePubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __KnowledgePubIDL
#define ExportedByKnowledgePubIDL __declspec(dllexport)
#else
#define ExportedByKnowledgePubIDL __declspec(dllimport)
#endif
#else
#define ExportedByKnowledgePubIDL
#endif
#endif

#include "CATBSTR.h"
#include "CATIARelation.h"

class CATIAParameter;

extern ExportedByKnowledgePubIDL IID IID_CATIADesignTable;

class ExportedByKnowledgePubIDL CATIADesignTable : public CATIARelation
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall AddAssociation(CATIAParameter * iParameter, const CATBSTR & iSheetColumn)=0;

    virtual HRESULT __stdcall RemoveAssociation(const CATBSTR & iSheetColumn)=0;

    virtual HRESULT __stdcall AddNewRow()=0;

    virtual HRESULT __stdcall get_CopyMode(CAT_VARIANT_BOOL & oMode)=0;

    virtual HRESULT __stdcall put_CopyMode(CAT_VARIANT_BOOL iMode)=0;

    virtual HRESULT __stdcall get_Configuration(short & oLineNb)=0;

    virtual HRESULT __stdcall put_Configuration(short iLineNb)=0;

    virtual HRESULT __stdcall get_ConfigurationsNb(short & oConfigurationsNb)=0;

    virtual HRESULT __stdcall get_FilePath(CATBSTR & oFilePath)=0;

    virtual HRESULT __stdcall put_FilePath(const CATBSTR & iFilePath)=0;

    virtual HRESULT __stdcall get_ColumnsNb(short & oColumns)=0;

    virtual HRESULT __stdcall CellAsString(short iRow, short iColumn, CATBSTR & oValue)=0;

    virtual HRESULT __stdcall Synchronize()=0;


};

CATDeclareHandler(CATIADesignTable, CATIARelation);

#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIABase.h"
#include "CATIAKnowledgeActivableObject.h"
#include "CATIAKnowledgeObject.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
