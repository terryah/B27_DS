/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAOptimizations_h
#define CATIAOptimizations_h

#ifndef ExportedByKnowledgePubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __KnowledgePubIDL
#define ExportedByKnowledgePubIDL __declspec(dllexport)
#else
#define ExportedByKnowledgePubIDL __declspec(dllimport)
#endif
#else
#define ExportedByKnowledgePubIDL
#endif
#endif

#include "CATBSTR.h"
#include "CATIACollection.h"
#include "CATVariant.h"

class CATIABase;
class CATIAOptimization;
class CATIASetOfEquation;

extern ExportedByKnowledgePubIDL IID IID_CATIAOptimizations;

class ExportedByKnowledgePubIDL CATIAOptimizations : public CATIACollection
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall Item(const CATVariant & iIndex, CATIABase *& oOptimization)=0;

    virtual HRESULT __stdcall CreateOptimization(CATIAOptimization *& oOptimization)=0;

    virtual HRESULT __stdcall CreateConstraintsSatisfaction(const CATBSTR & iName, const CATBSTR & iComment, const CATBSTR & iFormulaBody, CATIASetOfEquation *& oConstraintsSatisfaction)=0;


};

CATDeclareHandler(CATIAOptimizations, CATIACollection);

#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
