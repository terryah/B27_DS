/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIALength_h
#define CATIALength_h

#ifndef ExportedByKnowledgePubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __KnowledgePubIDL
#define ExportedByKnowledgePubIDL __declspec(dllexport)
#else
#define ExportedByKnowledgePubIDL __declspec(dllimport)
#endif
#else
#define ExportedByKnowledgePubIDL
#endif
#endif

#include "CATIADimension.h"

extern ExportedByKnowledgePubIDL IID IID_CATIALength;

class ExportedByKnowledgePubIDL CATIALength : public CATIADimension
{
    CATDeclareInterface;

public:

};

CATDeclareHandler(CATIALength, CATIADimension);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIABase.h"
#include "CATIAParameter.h"
#include "CATIARealParam.h"
#include "CATSafeArray.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
