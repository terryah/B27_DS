/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAParameters_h
#define CATIAParameters_h

#include "CATCORBABoolean.h"
#include "CAT_VARIANT_BOOL.h"

#ifndef ExportedByKnowledgePubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __KnowledgePubIDL
#define ExportedByKnowledgePubIDL __declspec(dllexport)
#else
#define ExportedByKnowledgePubIDL __declspec(dllimport)
#endif
#else
#define ExportedByKnowledgePubIDL
#endif
#endif

#include "CATBSTR.h"
#include "CATIACollection.h"
#include "CATVariant.h"

class CATIABase;
class CATIABoolParam;
class CATIADimension;
class CATIAIntParam;
class CATIAListParameter;
class CATIAParameter;
class CATIAParameterSet;
class CATIARealParam;
class CATIAStrParam;
class CATIAUnits;

extern ExportedByKnowledgePubIDL IID IID_CATIAParameters;

class ExportedByKnowledgePubIDL CATIAParameters : public CATIACollection
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall CreateString(const CATBSTR & iName, const CATBSTR & iValue, CATIAStrParam *& oParameter)=0;

    virtual HRESULT __stdcall CreateReal(const CATBSTR & iName, double iValue, CATIARealParam *& oParameter)=0;

    virtual HRESULT __stdcall CreateInteger(const CATBSTR & iName, CATLONG iValue, CATIAIntParam *& oParameter)=0;

    virtual HRESULT __stdcall CreateBoolean(const CATBSTR & iName, CAT_VARIANT_BOOL iValue, CATIABoolParam *& oParameter)=0;

    virtual HRESULT __stdcall CreateDimension(const CATBSTR & iName, const CATBSTR & iMagnitude, double iValue, CATIADimension *& oParameter)=0;

    virtual HRESULT __stdcall CreateList(const CATBSTR & iName, CATIAListParameter *& oParameter)=0;

    virtual HRESULT __stdcall Item(const CATVariant & iIndex, CATIAParameter *& oParameter)=0;

    virtual HRESULT __stdcall Remove(const CATVariant & iIndex)=0;

    virtual HRESULT __stdcall SubList(CATIABase * iObject, CAT_VARIANT_BOOL iRecursively, CATIAParameters *& oListParameters)=0;

    virtual HRESULT __stdcall get_Units(CATIAUnits *& oUnits)=0;

    virtual HRESULT __stdcall GetNameToUseInRelation(CATIABase * iObject, CATBSTR & oName)=0;

    virtual HRESULT __stdcall CreateSetOfParameters(CATIABase * iFather)=0;

    virtual HRESULT __stdcall get_RootParameterSet(CATIAParameterSet *& oParameterSet)=0;


};

CATDeclareHandler(CATIAParameters, CATIACollection);

#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
