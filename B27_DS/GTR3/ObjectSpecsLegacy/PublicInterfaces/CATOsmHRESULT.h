#ifndef __CATOsmHRESULT_h__
#define __CATOsmHRESULT_h__
/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */
/*
 * COPYRIGHT DASSAULT SYSTEMES 2008
 */

#include "CATErrorDef.h"
/** There is no local value associated to this key. */
#define E_NOLOCALVALUE    MAKE_HRESULT(SEVERITY_ERROR, FACILITY_ITF, 0xAF00 | 0x0001)
/** Key does not exist for this item. */
#define E_KEYNOTFOUND     MAKE_HRESULT(SEVERITY_ERROR, FACILITY_ITF, 0xAF00 | 0x0002)
/** Access outside the bound of the list. */
#define E_OUTOFBOUND      MAKE_HRESULT(SEVERITY_ERROR, FACILITY_ITF, 0xAF00 | 0x0003)
/** Invalid list operation. */
#define E_NOTALIST MAKE_HRESULT(SEVERITY_ERROR, FACILITY_ITF, 0xAF00 | 0x0004)
/** Incorrect type for this operation. */
#define E_TYPEMISMATCH MAKE_HRESULT(SEVERITY_ERROR, FACILITY_ITF, 0xAF00 | 0x0005)
/** The target cannot be loaded. */
#define E_CANNOTLOAD MAKE_HRESULT(SEVERITY_ERROR, FACILITY_ITF, 0xAF00 | 0x0006)
/**
 * An intermediate attribute link cannot be solved.
 * Intermediate feature cannot be loaded or intermediate key does not exist.
 */
#define  E_ATTRLINK	MAKE_HRESULT(SEVERITY_ERROR, FACILITY_ITF, 0xAF00 | 0x0007 )
/** The target is not loaded. */
#define  E_NOTLOADED	MAKE_HRESULT(SEVERITY_ERROR, FACILITY_ITF, 0xAF00 | 0x0008)
/**
 * An attribute or feature facet cannot be found.
 * The method seems to be S_OK but
 */
#define E_FACETNOTFOUND	 MAKE_HRESULT(SEVERITY_ERROR, FACILITY_ITF, 0xAF00 |0x0009 )

/**
 * We cannot give a correct status for this method.
 */
 
#define S_NOSTATUS ((HRESULT)0x00000002L)



#endif
