
#ifndef CATLISTV_CATISPECOBJECT_H
#define CATLISTV_CATISPECOBJECT_H

// COPYRIGHT DASSAULT SYSTEMES 1999

/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */

/**
 * @collection CATLISTV(CATISpecObject_var)
 * Collection class for specobjects.
 * All the methods of handlers collection classes are available.
 * Refer to the articles dealing with collections in the encyclopedia.
 */

#include "CATLISTHand_Clean.h"

#include "CATLISTHand_AllFunct.h"

#include "CATLISTHand_Declare.h"

#include "AC0SPBAS.h"
#undef  CATCOLLEC_ExportedBy
#define	CATCOLLEC_ExportedBy ExportedByAC0SPBAS

#include "CATISpecObject.h"
CATLISTHand_DECLARE(CATISpecObject_var)

#endif // CATLISTV_CATISPECOBJECT_H
