
#ifndef CATLISTV_CATISPECATTRIBUTE_H
#define CATLISTV_CATISPECATTRIBUTE_H

// COPYRIGHT DASSAULT SYSTEMES 1999

/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */

/**
 * @collection CATLISTV(CATISpecAttribute_var)
 * Collection class for attributes.
 * All the methods of handlers collection classes are available.
 * Refer to the articles dealing with collections in the encyclopedia.
 */

#include "CATLISTHand_Clean.h"

#include "CATLISTHand_AllFunct.h"

#include "CATLISTHand_Declare.h"

#include "AC0SPBAS.h"
#undef  CATCOLLEC_ExportedBy
#define	CATCOLLEC_ExportedBy ExportedByAC0SPBAS

#include "CATISpecAttribute.h"
CATLISTHand_DECLARE(CATISpecAttribute_var)

#endif // CATLISTV_CATISPECATTRIBUTE_H


