// COPYRIGHT DASSAULT SYSTEMES 2000
/** @CAA2Required */
//**********************************************************************
//* DON T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS *
//* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPEAR AT ANY TIME *
//**********************************************************************

#ifndef CP0SPEC_H
#define CP0SPEC_H

#ifndef ExportedByCP0SPEC
#ifdef _WINDOWS_SOURCE
#ifdef __CP0SPEC
#define ExportedByCP0SPEC __declspec(dllexport)
#else
#define ExportedByCP0SPEC __declspec(dllimport)
#endif
#else
#define ExportedByCP0SPEC
#endif
#endif

#endif

