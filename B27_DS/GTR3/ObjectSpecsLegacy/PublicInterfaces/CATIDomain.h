
#ifndef __CATIDomain_h__
#define __CATIDomain_h__


/**
 * @CAA2Required                  
 */
// COPYRIGHT DASSAULT SYSTEMES 1998

#include "CATBaseUnknown.h"
#include "CATCORBABoolean.h"
#include "CATBaseUnknown_var.h"

#ifndef CATIDomain_var
#define CATIDomain CATBaseUnknown
#define CATIDomain_var CATBaseUnknown_var
#endif // CATIDomain_var


#endif // __CATIDomain_h__
