# COPYRIGHT DASSAULT SYSTEMES 2002
#==============================================================================
# Imakefile for module DNBDpmBase.m
#==============================================================================
#
# SHARED LIBRARY 
#==============================================================================
#  mod  lhg  07/16/2003  Added DNBMfgAssyDataModel to INCLUDED_MODULES
#==============================================================================
#
BUILT_OBJECT_TYPE=SHARED LIBRARY
 
INCLUDED_MODULES=   DNBDpmAdapters \
                    DNBMfgAssyDataModel \
                    DNBDpmCompTool \
                    DNBDpmAutomation

LINK_WITH = CATApplicationFrame		\   # ApplicationFrame
            CATProcessInterfaces	\   # DMAPSInterfaces
            DNBDPMExecutionServices \   # DNBDpmExecutionServices
            DNBDPMItf               \
			DNBProcessUI			\
			DNBState                \
			Mathematics             \
            DNBGantt DNBGanttItf	\   # DNBGanttChart
            DNBGanttSettings        \   # DNBGanttChart
            DNBGraphEditor          \   # DNBGraphEditor
            AC0SPBAS                \   # ObjectSpecsModeler
            ProcessModelerBase      \   # ProcessPlatformBase   
            JS0GROUP JS0FM          \   # System
            ObjectModeler           \
            CATVisualization        \   # Visualization
            CATViz                  \   # For CATVisualizationBase FW
            DNBResourceProgramItf   \   # For DNBResourceProgramInterfaces
            AS0STARTUP              \   # CATIProduct
			CATIPDAdapterItf        \   # Hub Interfaces 
			DNBMHIBase				\   # DNBPLMServices
			CATPDMBase				\   # CATPDMLog
			CATPDMBaseItfCPP		\   # CATIPDMLogObject
			CATConstraintModelerUI  \
			CATIAApplicationFrame   \
			CATArrangementItfCPP   \
			CATVisUUID \						# nvo 2005/10/25: for CATIVisProperties (DNBMfgAssyDataModel)
			CATGraphicProperties \	# nvo 2005/10/25: for CATExtIVisProperties (DNBMfgAssyDataModel)
			CATIPDAdapterItf		\   # DNBIUnifiedPropertyAccessor
			CATInteractiveInterfaces InteractiveInterfacesUUID \   # For CATIIcon
			KnowledgeItf \
			SimulationProIDL \			# nvo 2005/10/25: for Simulation
			SimulationInterfacesUUID \
			CATDynClash \ # plk 16-JAN-2007: IR A0564430: clash detection was not implemented for MAs
            DPMSettingsItf \
			CK0FEAT \
			CATDMUGroup \
			DNBFastenerInterfaces \
			DNBResourceBehaviorItf \
			DNBDpmBaseUUID \
			DNBDpmUtils \
			DNBPLMItf \
			CATImmItf \
			DNBStateInterfacesUUID \
			DNBStateItfCPP \
			DNBStateItf \
			CATNavigatorItf \ # plk 16-JAN-2007: IR A0564430: clash detection was not implemented for MAs
			ResourceItf \
			CATLifRelations \
			SB0COMF \
			CATProductStructureInterfaces \
			ExecutionItfCPP \ 
			

# Useless Link_With 03/27/2002 YMS            

# GE0MAIN

# System dependant variables
#
OS = AIX
#
OS = HP-UX
#
OS = IRIX
#
OS = SunOS
#
OS = Windows_NT
#LOCAL_CCFLAGS=-FR

