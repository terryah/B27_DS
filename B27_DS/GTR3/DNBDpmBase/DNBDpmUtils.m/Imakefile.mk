# COPYRIGHT DASSAULT SYSTEMES 2005
#======================================================================
# Imakefile for module DELMSDTools.m
#======================================================================
#
#  Nov 2005  Creation: Code generated maw
#======================================================================
#
# SHARED LIBRARY 
#
BUILT_OBJECT_TYPE=SHARED LIBRARY 
 
# DO NOT EDIT :: THE CAA2 WIZARDS WILL ADD CODE HERE
WIZARD_LINK_MODULES = JS0GROUP JS0FM CATMathematics   \
JS0GROUP JS0FM CATApplicationFrame 
# END WIZARD EDITION ZONE

COMMON_LINK_WITH = $(WIZARD_LINK_MODULES) \
ObjectModeler            \
CATMathStream            \
CATApplicationFrame      \
CATVisualization         \ 
CATViz                   \
ProcessItfCPP            \
SP0BBOSM                 \
SP0BBDOC                 \
AS0STARTUP               \
CATIPDAdapterItf         \
CATIPDAdapterItfCPP      \
ExecutionItfCPP          \
DNBPLMItf                \
AC0SPEXT                 \  # CATIExtendable
CATProductStructureInterfaces \
DNBResourceBehaviorItf   \
DNBMHIServices           \  # For DNBPLMOpenObjectServices
CATImmItf                \  # For CATImmRelation
DNBState                 \  # For DNBReparentResourceService (DNBFactoryOnState)
DNBStateItfCPP           \  # same
DNBMfgAssyDataModel		 \  # For DNBEMfgAssemblyFactory
ResourceModule DNBDpmPertVisu ResourceItf KnowledgeItf	CATPDMItfOld GE0ITFS		

WINDOWS_LINK_WITH = DNBMHIOpenEngine         \  # Access e.g. to Plantype attributes
                    DNBMHIEngineServices        # For DNBImmTypeUtilities


# System dependant variables
#
OS = COMMON
LINK_WITH = $(COMMON_LINK_WITH)
#
OS = Windows_NT
LINK_WITH = $(COMMON_LINK_WITH) \
            $(WINDOWS_LINK_WITH)
