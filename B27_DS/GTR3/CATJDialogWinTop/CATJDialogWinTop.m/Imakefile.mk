# COPYRIGHT DASSAULT SYSTEMES 2001
#

BUILT_OBJECT_TYPE=SHARED LIBRARY 
ALIASES_ON_IMPORT=CATJDialogWinTop CATJdwBase

LINK_WITH = CATJdwBase JS0GROUP JS0FM JS0MT DI0PANV2 VE0GRPH2 ON0FRAME ON0MAIN ON0GREXT AC0XXLNK AC0ITEMS CATInteractiveInterfaces CATVisualization CATViz CATPrt CATPrtBase CATPrint CATSysMainThreadMQ

OS=aix_a
SYS_LIBS = -lXm -lpthread -lXt -lxkbfile -lXmu -lXi -lX11 -lm

OS=aix_a64
SYS_LIBS = -lXm -lpthread -lXt -lxkbfile -lXmu -lXi -lX11 -lm

OS=IRIX
SYS_LIBS= -lpthread -lX11 -lXt -lXm

OS=intel_a
SYS_LIBS= ole32.lib rpcrt4.lib ws2_32.lib Wininet.lib atl.lib

OS = win_a
BUILD = NO
