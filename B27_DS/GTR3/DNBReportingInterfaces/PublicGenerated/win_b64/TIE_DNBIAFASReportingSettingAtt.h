#ifndef __TIE_DNBIAFASReportingSettingAtt
#define __TIE_DNBIAFASReportingSettingAtt

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIAFASReportingSettingAtt.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIAFASReportingSettingAtt */
#define declare_TIE_DNBIAFASReportingSettingAtt(classe) \
 \
 \
class TIEDNBIAFASReportingSettingAtt##classe : public DNBIAFASReportingSettingAtt \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIAFASReportingSettingAtt, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall get_JavaExe(CATBSTR & oJavaExe); \
      virtual HRESULT __stdcall put_JavaExe(const CATBSTR & iJavaExe); \
      virtual HRESULT __stdcall GetJavaExeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetJavaExeLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_JavaClassPath(CATBSTR & oJavaClassPath); \
      virtual HRESULT __stdcall put_JavaClassPath(const CATBSTR & iJavaClassPath); \
      virtual HRESULT __stdcall GetJavaClassPathInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetJavaClassPathLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_StyleSheetDir(CATBSTR & oStyleSheetDir); \
      virtual HRESULT __stdcall put_StyleSheetDir(const CATBSTR & iStyleSheetDir); \
      virtual HRESULT __stdcall GetStyleSheetDirInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetStyleSheetDirLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_DefaultStyleSheet(CATBSTR & oRobotProgramDir); \
      virtual HRESULT __stdcall put_DefaultStyleSheet(const CATBSTR & iRobotProgramDir); \
      virtual HRESULT __stdcall GetDefaultStyleSheetInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetDefaultStyleSheetLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_ResultOutputDir(CATBSTR & oResultOutputDir); \
      virtual HRESULT __stdcall put_ResultOutputDir(const CATBSTR & iResultOutputDir); \
      virtual HRESULT __stdcall GetResultOutputDirInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetResultOutputDirLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_SVGViewer(CATLONG & oSVGViewer); \
      virtual HRESULT __stdcall put_SVGViewer(CATLONG iSVGViewer); \
      virtual HRESULT __stdcall GetSVGViewerInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetSVGViewerLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall Commit(); \
      virtual HRESULT __stdcall Rollback(); \
      virtual HRESULT __stdcall ResetToAdminValues(); \
      virtual HRESULT __stdcall ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList); \
      virtual HRESULT __stdcall SaveRepository(); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIAFASReportingSettingAtt(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall get_JavaExe(CATBSTR & oJavaExe); \
virtual HRESULT __stdcall put_JavaExe(const CATBSTR & iJavaExe); \
virtual HRESULT __stdcall GetJavaExeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetJavaExeLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_JavaClassPath(CATBSTR & oJavaClassPath); \
virtual HRESULT __stdcall put_JavaClassPath(const CATBSTR & iJavaClassPath); \
virtual HRESULT __stdcall GetJavaClassPathInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetJavaClassPathLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_StyleSheetDir(CATBSTR & oStyleSheetDir); \
virtual HRESULT __stdcall put_StyleSheetDir(const CATBSTR & iStyleSheetDir); \
virtual HRESULT __stdcall GetStyleSheetDirInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetStyleSheetDirLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_DefaultStyleSheet(CATBSTR & oRobotProgramDir); \
virtual HRESULT __stdcall put_DefaultStyleSheet(const CATBSTR & iRobotProgramDir); \
virtual HRESULT __stdcall GetDefaultStyleSheetInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetDefaultStyleSheetLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_ResultOutputDir(CATBSTR & oResultOutputDir); \
virtual HRESULT __stdcall put_ResultOutputDir(const CATBSTR & iResultOutputDir); \
virtual HRESULT __stdcall GetResultOutputDirInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetResultOutputDirLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_SVGViewer(CATLONG & oSVGViewer); \
virtual HRESULT __stdcall put_SVGViewer(CATLONG iSVGViewer); \
virtual HRESULT __stdcall GetSVGViewerInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetSVGViewerLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall Commit(); \
virtual HRESULT __stdcall Rollback(); \
virtual HRESULT __stdcall ResetToAdminValues(); \
virtual HRESULT __stdcall ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList); \
virtual HRESULT __stdcall SaveRepository(); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIAFASReportingSettingAtt(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::get_JavaExe(CATBSTR & oJavaExe) \
{ \
return (ENVTIECALL(DNBIAFASReportingSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_JavaExe(oJavaExe)); \
} \
HRESULT __stdcall  ENVTIEName::put_JavaExe(const CATBSTR & iJavaExe) \
{ \
return (ENVTIECALL(DNBIAFASReportingSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_JavaExe(iJavaExe)); \
} \
HRESULT __stdcall  ENVTIEName::GetJavaExeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAFASReportingSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetJavaExeInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetJavaExeLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAFASReportingSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetJavaExeLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_JavaClassPath(CATBSTR & oJavaClassPath) \
{ \
return (ENVTIECALL(DNBIAFASReportingSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_JavaClassPath(oJavaClassPath)); \
} \
HRESULT __stdcall  ENVTIEName::put_JavaClassPath(const CATBSTR & iJavaClassPath) \
{ \
return (ENVTIECALL(DNBIAFASReportingSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_JavaClassPath(iJavaClassPath)); \
} \
HRESULT __stdcall  ENVTIEName::GetJavaClassPathInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAFASReportingSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetJavaClassPathInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetJavaClassPathLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAFASReportingSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetJavaClassPathLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_StyleSheetDir(CATBSTR & oStyleSheetDir) \
{ \
return (ENVTIECALL(DNBIAFASReportingSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_StyleSheetDir(oStyleSheetDir)); \
} \
HRESULT __stdcall  ENVTIEName::put_StyleSheetDir(const CATBSTR & iStyleSheetDir) \
{ \
return (ENVTIECALL(DNBIAFASReportingSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_StyleSheetDir(iStyleSheetDir)); \
} \
HRESULT __stdcall  ENVTIEName::GetStyleSheetDirInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAFASReportingSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetStyleSheetDirInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetStyleSheetDirLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAFASReportingSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetStyleSheetDirLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_DefaultStyleSheet(CATBSTR & oRobotProgramDir) \
{ \
return (ENVTIECALL(DNBIAFASReportingSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_DefaultStyleSheet(oRobotProgramDir)); \
} \
HRESULT __stdcall  ENVTIEName::put_DefaultStyleSheet(const CATBSTR & iRobotProgramDir) \
{ \
return (ENVTIECALL(DNBIAFASReportingSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_DefaultStyleSheet(iRobotProgramDir)); \
} \
HRESULT __stdcall  ENVTIEName::GetDefaultStyleSheetInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAFASReportingSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetDefaultStyleSheetInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetDefaultStyleSheetLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAFASReportingSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetDefaultStyleSheetLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_ResultOutputDir(CATBSTR & oResultOutputDir) \
{ \
return (ENVTIECALL(DNBIAFASReportingSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_ResultOutputDir(oResultOutputDir)); \
} \
HRESULT __stdcall  ENVTIEName::put_ResultOutputDir(const CATBSTR & iResultOutputDir) \
{ \
return (ENVTIECALL(DNBIAFASReportingSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_ResultOutputDir(iResultOutputDir)); \
} \
HRESULT __stdcall  ENVTIEName::GetResultOutputDirInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAFASReportingSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetResultOutputDirInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetResultOutputDirLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAFASReportingSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetResultOutputDirLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_SVGViewer(CATLONG & oSVGViewer) \
{ \
return (ENVTIECALL(DNBIAFASReportingSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_SVGViewer(oSVGViewer)); \
} \
HRESULT __stdcall  ENVTIEName::put_SVGViewer(CATLONG iSVGViewer) \
{ \
return (ENVTIECALL(DNBIAFASReportingSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_SVGViewer(iSVGViewer)); \
} \
HRESULT __stdcall  ENVTIEName::GetSVGViewerInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAFASReportingSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetSVGViewerInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetSVGViewerLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAFASReportingSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetSVGViewerLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::Commit() \
{ \
return (ENVTIECALL(DNBIAFASReportingSettingAtt,ENVTIETypeLetter,ENVTIELetter)Commit()); \
} \
HRESULT __stdcall  ENVTIEName::Rollback() \
{ \
return (ENVTIECALL(DNBIAFASReportingSettingAtt,ENVTIETypeLetter,ENVTIELetter)Rollback()); \
} \
HRESULT __stdcall  ENVTIEName::ResetToAdminValues() \
{ \
return (ENVTIECALL(DNBIAFASReportingSettingAtt,ENVTIETypeLetter,ENVTIELetter)ResetToAdminValues()); \
} \
HRESULT __stdcall  ENVTIEName::ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList) \
{ \
return (ENVTIECALL(DNBIAFASReportingSettingAtt,ENVTIETypeLetter,ENVTIELetter)ResetToAdminValuesByName(iAttList)); \
} \
HRESULT __stdcall  ENVTIEName::SaveRepository() \
{ \
return (ENVTIECALL(DNBIAFASReportingSettingAtt,ENVTIETypeLetter,ENVTIELetter)SaveRepository()); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIAFASReportingSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIAFASReportingSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIAFASReportingSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIAFASReportingSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIAFASReportingSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIAFASReportingSettingAtt(classe)    TIEDNBIAFASReportingSettingAtt##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIAFASReportingSettingAtt(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIAFASReportingSettingAtt, classe) \
 \
 \
CATImplementTIEMethods(DNBIAFASReportingSettingAtt, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIAFASReportingSettingAtt, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIAFASReportingSettingAtt, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIAFASReportingSettingAtt, classe) \
 \
HRESULT __stdcall  TIEDNBIAFASReportingSettingAtt##classe::get_JavaExe(CATBSTR & oJavaExe) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&oJavaExe); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_JavaExe(oJavaExe); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&oJavaExe); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAFASReportingSettingAtt##classe::put_JavaExe(const CATBSTR & iJavaExe) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&iJavaExe); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_JavaExe(iJavaExe); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&iJavaExe); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAFASReportingSettingAtt##classe::GetJavaExeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetJavaExeInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAFASReportingSettingAtt##classe::SetJavaExeLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetJavaExeLock(iLocked); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAFASReportingSettingAtt##classe::get_JavaClassPath(CATBSTR & oJavaClassPath) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&oJavaClassPath); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_JavaClassPath(oJavaClassPath); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&oJavaClassPath); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAFASReportingSettingAtt##classe::put_JavaClassPath(const CATBSTR & iJavaClassPath) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&iJavaClassPath); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_JavaClassPath(iJavaClassPath); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&iJavaClassPath); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAFASReportingSettingAtt##classe::GetJavaClassPathInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetJavaClassPathInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAFASReportingSettingAtt##classe::SetJavaClassPathLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetJavaClassPathLock(iLocked); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAFASReportingSettingAtt##classe::get_StyleSheetDir(CATBSTR & oStyleSheetDir) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&oStyleSheetDir); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_StyleSheetDir(oStyleSheetDir); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&oStyleSheetDir); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAFASReportingSettingAtt##classe::put_StyleSheetDir(const CATBSTR & iStyleSheetDir) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&iStyleSheetDir); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_StyleSheetDir(iStyleSheetDir); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&iStyleSheetDir); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAFASReportingSettingAtt##classe::GetStyleSheetDirInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,11,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetStyleSheetDirInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,11,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAFASReportingSettingAtt##classe::SetStyleSheetDirLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,12,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetStyleSheetDirLock(iLocked); \
   ExitAfterCall(this,12,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAFASReportingSettingAtt##classe::get_DefaultStyleSheet(CATBSTR & oRobotProgramDir) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,13,&_Trac2,&oRobotProgramDir); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_DefaultStyleSheet(oRobotProgramDir); \
   ExitAfterCall(this,13,_Trac2,&_ret_arg,&oRobotProgramDir); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAFASReportingSettingAtt##classe::put_DefaultStyleSheet(const CATBSTR & iRobotProgramDir) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,14,&_Trac2,&iRobotProgramDir); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_DefaultStyleSheet(iRobotProgramDir); \
   ExitAfterCall(this,14,_Trac2,&_ret_arg,&iRobotProgramDir); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAFASReportingSettingAtt##classe::GetDefaultStyleSheetInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,15,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetDefaultStyleSheetInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,15,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAFASReportingSettingAtt##classe::SetDefaultStyleSheetLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,16,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetDefaultStyleSheetLock(iLocked); \
   ExitAfterCall(this,16,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAFASReportingSettingAtt##classe::get_ResultOutputDir(CATBSTR & oResultOutputDir) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,17,&_Trac2,&oResultOutputDir); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_ResultOutputDir(oResultOutputDir); \
   ExitAfterCall(this,17,_Trac2,&_ret_arg,&oResultOutputDir); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAFASReportingSettingAtt##classe::put_ResultOutputDir(const CATBSTR & iResultOutputDir) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,18,&_Trac2,&iResultOutputDir); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_ResultOutputDir(iResultOutputDir); \
   ExitAfterCall(this,18,_Trac2,&_ret_arg,&iResultOutputDir); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAFASReportingSettingAtt##classe::GetResultOutputDirInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,19,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetResultOutputDirInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,19,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAFASReportingSettingAtt##classe::SetResultOutputDirLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,20,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetResultOutputDirLock(iLocked); \
   ExitAfterCall(this,20,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAFASReportingSettingAtt##classe::get_SVGViewer(CATLONG & oSVGViewer) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,21,&_Trac2,&oSVGViewer); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_SVGViewer(oSVGViewer); \
   ExitAfterCall(this,21,_Trac2,&_ret_arg,&oSVGViewer); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAFASReportingSettingAtt##classe::put_SVGViewer(CATLONG iSVGViewer) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,22,&_Trac2,&iSVGViewer); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_SVGViewer(iSVGViewer); \
   ExitAfterCall(this,22,_Trac2,&_ret_arg,&iSVGViewer); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAFASReportingSettingAtt##classe::GetSVGViewerInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,23,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetSVGViewerInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,23,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAFASReportingSettingAtt##classe::SetSVGViewerLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,24,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetSVGViewerLock(iLocked); \
   ExitAfterCall(this,24,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAFASReportingSettingAtt##classe::Commit() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,25,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Commit(); \
   ExitAfterCall(this,25,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAFASReportingSettingAtt##classe::Rollback() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,26,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Rollback(); \
   ExitAfterCall(this,26,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAFASReportingSettingAtt##classe::ResetToAdminValues() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,27,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ResetToAdminValues(); \
   ExitAfterCall(this,27,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAFASReportingSettingAtt##classe::ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,28,&_Trac2,&iAttList); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ResetToAdminValuesByName(iAttList); \
   ExitAfterCall(this,28,_Trac2,&_ret_arg,&iAttList); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAFASReportingSettingAtt##classe::SaveRepository() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,29,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SaveRepository(); \
   ExitAfterCall(this,29,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAFASReportingSettingAtt##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,30,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,30,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAFASReportingSettingAtt##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,31,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,31,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAFASReportingSettingAtt##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,32,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,32,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAFASReportingSettingAtt##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,33,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,33,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAFASReportingSettingAtt##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,34,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,34,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIAFASReportingSettingAtt(classe) \
 \
 \
declare_TIE_DNBIAFASReportingSettingAtt(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAFASReportingSettingAtt##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAFASReportingSettingAtt,"DNBIAFASReportingSettingAtt",DNBIAFASReportingSettingAtt::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAFASReportingSettingAtt(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIAFASReportingSettingAtt, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAFASReportingSettingAtt##classe(classe::MetaObject(),DNBIAFASReportingSettingAtt::MetaObject(),(void *)CreateTIEDNBIAFASReportingSettingAtt##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIAFASReportingSettingAtt(classe) \
 \
 \
declare_TIE_DNBIAFASReportingSettingAtt(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAFASReportingSettingAtt##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAFASReportingSettingAtt,"DNBIAFASReportingSettingAtt",DNBIAFASReportingSettingAtt::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAFASReportingSettingAtt(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIAFASReportingSettingAtt, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAFASReportingSettingAtt##classe(classe::MetaObject(),DNBIAFASReportingSettingAtt::MetaObject(),(void *)CreateTIEDNBIAFASReportingSettingAtt##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIAFASReportingSettingAtt(classe) TIE_DNBIAFASReportingSettingAtt(classe)
#else
#define BOA_DNBIAFASReportingSettingAtt(classe) CATImplementBOA(DNBIAFASReportingSettingAtt, classe)
#endif

#endif
