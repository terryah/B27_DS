/* -*-c++-*- */
#ifndef CAT2DCstPointDefs_h
#define CAT2DCstPointDefs_h
/** @CAA2Required */
/**********************************************************************/
/* DON T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPEAR AT ANY TIME */
/**********************************************************************/

// COPYRIGHT DASSAULT SYSTEMES 2003

enum CAT2DCstPointDefinitionMode
{
  CstPointDefinitionMode_Explicit  = 1,
  CstPointDefinitionMode_FromCurve = 2
};

#endif
