#ifndef CatGeometricType_IDL
#define CatGeometricType_IDL
/*IDLREP*/

// COPYRIGHT DASSAULT SYSTEMES 1999

/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */

// CatGeometricType:						   
/**
 * Enumeration for all types of possible geometrical elements.
 *
 *   @param catGeoTypeUnknown
 *     The element does not fit in any of the following types.
 *
 *   @param catGeoTypeAxis2D
 *     The geometrical element is a 2D axis.
 *
 *   @param catGeoTypePoint2D
 *     The geometrical element is a 2D point.
 *
 *   @param catGeoTypeLine2D
 *     The geometrical element is a 2D line.
 *
 *   @param catGeoTypeControlPoint2D
 *     The geometrical element is a 2D spline control point.
 *
 *   @param catGeoTypeCircle2D
 *     The geometrical element is a 2D circle.
 *
 *   @param catGeoTypeHyperbola2D
 *     The geometrical element is a 2D hyperbola.
 *
 *   @param catGeoTypeParabola2D
 *     The geometrical element is a 2D parabola
 *
 *   @param catGeoTypeEllipse2D
 *     The geometrical element is a 2D ellipse.
 *
 *   @param catGeoTypeSpline2D
 *     The geometrical element is a 2D spline.
 *
 *   @param catGeoTypePoint
 *     The geometrical element is a 3D point.
 *
 *   @param catGeoTypeLine
 *     The geometrical element is a 3D line.
 *
 *   @param catGeoTypePlane
 *     The geometrical element is a 3D plane.
 */

enum CatGeometricType
{
	catGeoTypeUnknown,
	catGeoTypeAxis2D,
	catGeoTypePoint2D,
	catGeoTypeLine2D,
	catGeoTypeControlPoint2D,
	catGeoTypeCircle2D,
	catGeoTypeHyperbola2D,
	catGeoTypeParabola2D,
	catGeoTypeEllipse2D,
	catGeoTypeSpline2D,
	catGeoTypePoint,
	catGeoTypeLine,
	catGeoTypePlane
};

#endif // CatGeometricType_IDL









