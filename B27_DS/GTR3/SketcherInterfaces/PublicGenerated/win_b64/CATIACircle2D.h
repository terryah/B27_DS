/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIACircle2D_h
#define CATIACircle2D_h

#ifndef ExportedBySketcherPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __SketcherPubIDL
#define ExportedBySketcherPubIDL __declspec(dllexport)
#else
#define ExportedBySketcherPubIDL __declspec(dllimport)
#endif
#else
#define ExportedBySketcherPubIDL
#endif
#endif

#include "CATIACurve2D.h"
#include "CATSafeArray.h"

class CATIAPoint2D;

extern ExportedBySketcherPubIDL IID IID_CATIACircle2D;

class ExportedBySketcherPubIDL CATIACircle2D : public CATIACurve2D
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_CenterPoint(CATIAPoint2D *& oCenterPoint)=0;

    virtual HRESULT __stdcall put_CenterPoint(CATIAPoint2D * iCenterPoint)=0;

    virtual HRESULT __stdcall get_Radius(double & oRadius)=0;

    virtual HRESULT __stdcall SetData(double iCenterX, double iCenterY, double iRadius)=0;

    virtual HRESULT __stdcall GetCenter(CATSafeArrayVariant & oData)=0;


};

CATDeclareHandler(CATIACircle2D, CATIACurve2D);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATGeometricType.h"
#include "CATIABase.h"
#include "CATIAGeometricElement.h"
#include "CATIAGeometry2D.h"
#include "CATIAPoint2D.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
