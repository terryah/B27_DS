/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIASketch_h
#define CATIASketch_h

#ifndef ExportedBySketcherPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __SketcherPubIDL
#define ExportedBySketcherPubIDL __declspec(dllexport)
#else
#define ExportedBySketcherPubIDL __declspec(dllimport)
#endif
#else
#define ExportedBySketcherPubIDL
#endif
#endif

#include "CATIABase.h"
#include "CATSafeArray.h"

class CATIAAxis2D;
class CATIACircle;
class CATIAConstraints;
class CATIAFactory2D;
class CATIAGeometricElements;
class CATIALine2D;
class CATIARectangle;

extern ExportedBySketcherPubIDL IID IID_CATIASketch;

class ExportedBySketcherPubIDL CATIASketch : public CATIABase
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_GeometricElements(CATIAGeometricElements *& oGeometricElements)=0;

    virtual HRESULT __stdcall get_Constraints(CATIAConstraints *& oConstraints)=0;

    virtual HRESULT __stdcall get_Factory2D(CATIAFactory2D *& oFactory)=0;

    virtual HRESULT __stdcall get_CenterLine(CATIALine2D *& oLine)=0;

    virtual HRESULT __stdcall put_CenterLine(CATIALine2D * iLine)=0;

    virtual HRESULT __stdcall get_AbsoluteAxis(CATIAAxis2D *& oAxis)=0;

    virtual HRESULT __stdcall GetAbsoluteAxisData(CATSafeArrayVariant & oAxisData)=0;

    virtual HRESULT __stdcall SetAbsoluteAxisData(const CATSafeArrayVariant & iAxisData)=0;

    virtual HRESULT __stdcall OpenEdition(CATIAFactory2D *& oFactory)=0;

    virtual HRESULT __stdcall CloseEdition()=0;

    virtual HRESULT __stdcall InverseOrientation()=0;

    virtual HRESULT __stdcall Evaluate()=0;


};

CATDeclareHandler(CATIASketch, CATIABase);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
