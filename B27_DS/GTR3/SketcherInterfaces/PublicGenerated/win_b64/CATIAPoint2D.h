/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAPoint2D_h
#define CATIAPoint2D_h

#ifndef ExportedBySketcherPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __SketcherPubIDL
#define ExportedBySketcherPubIDL __declspec(dllexport)
#else
#define ExportedBySketcherPubIDL __declspec(dllimport)
#endif
#else
#define ExportedBySketcherPubIDL
#endif
#endif

#include "CATIAGeometry2D.h"
#include "CATSafeArray.h"

extern ExportedBySketcherPubIDL IID IID_CATIAPoint2D;

class ExportedBySketcherPubIDL CATIAPoint2D : public CATIAGeometry2D
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall SetData(double iX, double iY)=0;

    virtual HRESULT __stdcall GetCoordinates(CATSafeArrayVariant & oPoint)=0;


};

CATDeclareHandler(CATIAPoint2D, CATIAGeometry2D);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATGeometricType.h"
#include "CATIABase.h"
#include "CATIAGeometricElement.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
