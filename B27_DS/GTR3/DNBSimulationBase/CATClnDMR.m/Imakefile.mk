#ifdef LOCAL_DEFINITION_FOR_IID
LINK_WITH_FOR_IID = \
ObjectSpecsModelerUUID
#else
LINK_WITH_FOR_IID =
#endif

BUILT_OBJECT_TYPE=SHARED LIBRARY

LINK_WITH=$(LINK_WITH_FOR_IID)          \
                CATClnBase              \ # CATDataCompatibilityInfra
                CATClnSpecs             \ # CATDataCompatibilityInfra
                CATObjectModelerBase    \ # ObjectModelerBase
                CATObjectSpecsModeler   \ # ObjectSpecsModeler
                AC0SPBAS                \ # ObjectSpecsModeler
                JS0GROUP                \ # System
                DNBSimulationItfCPP     \ # DNBSimulationInterfaces
                DNBSimulationBase       \ # DNBSimulationBase
                ArrUtility              \ # CATArrangementUI

