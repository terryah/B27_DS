/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview JYV MMH 01:08:30
 * @quickReview MMH 02:10:15
 * @quickReview MMH 03:02:24
 * @quickReview SHA 03:03:21
 * @quickReview RTL 03:09:12
 */
// --------------------------------------------------------
     
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBObserver.cc
//   Templated class for all observers.
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     mmg          01/01/1999   Initial implementation
//     sha          01/01/2000   Fix a bug in buffer_changes() method
//     mmh          08/30/2001   Fixes - full code review
//     mmg          09/23/2002   added support for Collect functors
//     mmg          02/17/2003   added support for adaptive flush to model
//     mmg          03/21/2003   pass _object to apply_event() method
//     mmg          09/05/2003   support lifecycle observation and clone
//
//==============================================================================
#ifndef TMPL_DEFINE_DYNAMIC_RTTI
#define TMPL_DEFINE_DYNAMIC_RTTI
DNB_DEFINE_DYNAMIC_RTTI_TMPL_B1( DNBObserver<Adaptor DNB_COMMA Obs>,
    <class Adaptor DNB_COMMA class Obs>, DNBObserverBase );
#endif


DNB_DEFINE_SHARED_OBJECT_TMPL( DNBObserver<Adaptor DNB_COMMA Obs>,
    <class Adaptor DNB_COMMA class Obs> );


#if 0
DNB_DEFINE_EXTENSION_FACTORY_TMPL( DNBObserver<Adaptor DNB_COMMA Obs>,
    <class Adaptor DNB_COMMA class Obs>, DNBBasicEntity );
#endif


//------------------------------------------------------------------------------
// Implements DNBObserver<Adaptor,Obs>::create
//
// creates an observer and binds it to a Basic object type
// and sets the entity lifecycle that this observer is observing
//------------------------------------------------------------------------------
template < class Adaptor, class Obs >
typename DNBObserver<Adaptor,Obs>::Handle
DNBObserver<Adaptor,Obs>::create( const DNBSharedHandle<Obs> hObj, 
                                                DNBBasicEntity::LifeCycle lCyc )
    DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit))
{
    DNBObserver<Adaptor,Obs> *pTarget = 
                                DNB_NEW DNBObserver<Adaptor,Obs>(hObj, lCyc );
    if( !pTarget )
        return NULL;

    typename DNBObserver<Adaptor,Obs>::Handle hTarget( pTarget );

    // check to see if the object being observered is an extension
    if( hObj.isDerivedFrom( DNBExtensionObject::ClassInfo() ) )
    {
        // observing an extension, bind the observer to the Basic object
        DNBExtensionObject::Pointer pObj( hObj );
        pTarget->bindExtension( pObj->getExtendible() );
    }
    else
    {
        // observing a Basic object
        pTarget->bindExtension( hObj );
    }
    return hTarget;
}

//------------------------------------------------------------------------------
// Implements DNBObserver<Adaptor,Obs>::duplicateObject
//
// calls the copy constructor for an observer
//------------------------------------------------------------------------------
template < class Adaptor, class Obs >
DNBDynamicObject *
DNBObserver<Adaptor,Obs>::duplicateObject( 
                    CopyMode mode, DNBNotificationAgent::Handle hBasic ) const
    DNB_THROW_SPEC((scl_bad_alloc, DNBException)) 
{
    return( DNB_NEW DNBObserver<Adaptor,Obs>( *this, mode, hBasic ) ); 
}

//------------------------------------------------------------------------------
// Implements DNBObserver<Adaptor,Obs>::DNBObserver 
//------------------------------------------------------------------------------
template < class Adaptor, class Obs >
DNBObserver<Adaptor,Obs>::DNBObserver( DNBSharedHandle<Obs> obj, DNBBasicEntity::LifeCycle lCyc )
    DNB_THROW_SPEC((DNBEResourceLimit)) :
    _object( obj ), _functormap(), DNBObserverBase( lCyc )
{}

//------------------------------------------------------------------------------
// Implements DNBObserver<Adaptor,Obs>::DNBObserver copy constructor
//------------------------------------------------------------------------------
template < class Adaptor, class Obs >
DNBObserver<Adaptor,Obs>::DNBObserver(
        const DNBObserver<Adaptor,Obs> &right, CopyMode mode,
                                    DNBNotificationAgent::Handle hBasic )
    DNB_THROW_SPEC((DNBEResourceLimit)) : 
    _functormap( right._functormap ), DNBObserverBase( right, mode )
{
    // check to see if the object being observered is an extension
    if( right._object.isDerivedFrom( DNBExtensionObject::ClassInfo() ) )
    {
        // observing an extension, cache the handle to the extension of the
        // same type on the cloned base object
        DNBExtendibleObject::ConstPointer pBasic( hBasic );
        _object = pBasic->getExtension( 
                                right._object._getObject()->getClassID() );
    }
    else
    {
        // observing an entity, cache the handle to the cloned entity
        _object = hBasic;
    }
}

//------------------------------------------------------------------------------
// Implements DNBObserver<Adaptor,Obs>::~DNBObserver
//------------------------------------------------------------------------------
template < class Adaptor, class Obs >
DNBObserver<Adaptor,Obs>::~DNBObserver( )
    DNB_THROW_SPEC_NULL
{
    DNBObserver<Adaptor,Obs>::orphanObject( );
}

//------------------------------------------------------------------------------
// Implements DNBObserver<Adaptor,Obs>::orphanMembers
//------------------------------------------------------------------------------
template < class Adaptor, class Obs >
void
DNBObserver<Adaptor,Obs>::orphanMembers()
    DNB_THROW_SPEC_NULL
{
    remove_functors();
}

//------------------------------------------------------------------------------
// Implements DNBObserver<Adaptor,Obs>::bindMembers
//------------------------------------------------------------------------------
template < class Adaptor, class Obs >
void
DNBObserver<Adaptor,Obs>::bindMembers()
    DNB_THROW_SPEC_NULL
{
    _data[_read_buffer].init( _object );
    _data[_write_buffer].init( _object );
    DNBObserverBase::bindMembers();
    if( !_functormap.empty() )
        subscribe();
}

//------------------------------------------------------------------------------
// Implements DNBObserver<Adaptor,Obs>::add_functor
//------------------------------------------------------------------------------
template < class Adaptor, class Obs >
void
DNBObserver<Adaptor,Obs>::add_functor( scl_wstring key, 
                                       Functor func, FunctorType functype )
    DNB_THROW_SPEC_NULL
{
    // if this is the first functor, have the observer subscribe to events
    // on the object it is observing
    if( _functormap.empty() )
        subscribe();

    // only add a functor once to the functor list
    if( _functormap.find( key ) == _functormap.end() )
    {
        _functormap[key] = func;
        add_functor_data( key, functype );
    }
}

//------------------------------------------------------------------------------
// Implements DNBObserver<Adaptor,Obs>::remove_functor
//
// remove a functor by name
//------------------------------------------------------------------------------
template < class Adaptor, class Obs >
void
DNBObserver<Adaptor,Obs>::remove_functor( scl_wstring key )
    DNB_THROW_SPEC_NULL
{
    if( _functormap.empty() )
        return;

    _functormap.erase( key );
    _basefunctormap.erase( key );

    // if this is the last functor, have the observer unsubscribe from events
    // on the object it is observing
    if( _functormap.empty() )
        unsubscribe();
}

//------------------------------------------------------------------------------
// Implements DNBObserver<Adaptor,Obs>::remove_functor
//
// remove a functor by object
//------------------------------------------------------------------------------
template < class Adaptor, class Obs >
void
DNBObserver<Adaptor,Obs>::remove_functor( Functor func )
    DNB_THROW_SPEC_NULL
{
    if( _functormap.empty() )
        return;

    FunctorIter iter;
    FunctorIter next = _functormap.begin();
    while( next != _functormap.end() )
    {
        iter = next++;
        if( (*iter).second == func )
        {
            _functormap.erase( iter );
            _basefunctormap.erase( (*iter).first );
            break;
        }
    }

    // if this is the last functor, have the observer unsubscribe from events
    // on the object it is observing
    if( _functormap.empty() )
        unsubscribe();
}

//------------------------------------------------------------------------------
// Implements DNBObserver<Adaptor,Obs>::remove_functors
//------------------------------------------------------------------------------
template < class Adaptor, class Obs >
void
DNBObserver<Adaptor,Obs>::remove_functors()
    DNB_THROW_SPEC_NULL
{
    if( !_functormap.empty() )
    {
        _functormap.clear();
        _basefunctormap.clear();
        unsubscribe();
    }
}

//------------------------------------------------------------------------------
// Implements DNBObserver<Adaptor,Obs>::subscribe
//
// have the observer subscribe to events from the object it is observing
//------------------------------------------------------------------------------
template < class Adaptor, class Obs >
void
DNBObserver<Adaptor,Obs>::subscribe()
    DNB_THROW_SPEC_NULL
{
    DNBNotificationAgent::Pointer pObj( 
                        _data[_write_buffer].publisher( _object ) );
    if( pObj )
    {
        EventFunctor functor =
            DNBMakeEventFunctor( this, &DNBObserver<Adaptor,Obs>::on_event );
        pObj->addSubscriber( functor, _data[_write_buffer].mask( _object ) );
    }
}

//------------------------------------------------------------------------------
// Implements DNBObserver<Adaptor,Obs>::unsubscribe
//
// have the observer unsubscribe from events from the object it is observing
//------------------------------------------------------------------------------
template < class Adaptor, class Obs >
void
DNBObserver<Adaptor,Obs>::unsubscribe()
    DNB_THROW_SPEC_NULL
{
    DNBNotificationAgent::Pointer pObj( 
            _data[_write_buffer].publisher( _object ) );
    if( pObj )
    {
        EventFunctor functor =
            DNBMakeEventFunctor( this, &DNBObserver<Adaptor,Obs>::on_event );
        try
        {
            pObj->removeSubscriber( functor );
        }
        catch( ... )
        {
        }
    }
}

//------------------------------------------------------------------------------
// Implements DNBObserver<Adaptor,Obs>::apply_changes
//------------------------------------------------------------------------------
template < class Adaptor, class Obs >
void
DNBObserver<Adaptor,Obs>::apply_changes()
    DNB_THROW_SPEC_NULL
{
    if( _events[_read_buffer].any() )
    {
        //
        //  call any Apply functors
        //
        FunctorIter iter;
        BaseFunctorIter baseiter;
        for ( iter = _functormap.begin(), baseiter = _basefunctormap.begin();
              baseiter != _basefunctormap.end(); ++baseiter, ++iter )
        {
            if( (*baseiter).second.functype != Apply )
                continue;

            if( !(*baseiter).second.enabled )
                continue;

            ((*iter).second)( _events[_read_buffer], _data[_read_buffer]);
        }
    }
    DNBObserverBase::apply_changes();
}

//------------------------------------------------------------------------------
// Implements DNBObserver<Adaptor,Obs>::buffer_changes
//------------------------------------------------------------------------------
template< class Adaptor, class Obs >
bool
DNBObserver<Adaptor,Obs>::buffer_changes()
    DNB_THROW_SPEC_NULL
{
    bool ret = TRUE;
    
    // if this observer is being deleted, do not buffer data and do not
    // add to observerlist
    if( isOrphaned() ) 
    {
        ret = FALSE;
    }
    else if( _events[_write_buffer].any() )
    {
        //
        // SHA: fixed MMG's changes 
        //
        if( _enable_state == Enabled ||
               ( _enable_state == SwapDisabled && updated() ) ) 
        {
            _data[_write_buffer].collect( _object );

            //
            //  call any Collect functors
            //
            FunctorIter iter;
            BaseFunctorIter baseiter;
            for ( iter = _functormap.begin(), baseiter =_basefunctormap.begin();
                  baseiter != _basefunctormap.end(); ++baseiter, ++iter )
            {
                if( (*baseiter).second.functype != Collect )
                    continue;

                if( !(*baseiter).second.enabled )
                    continue;

                // only call functors if events have occurred
                ((*iter).second)( _events[_write_buffer], _data[_write_buffer]);
            }
        }
    }
    DNBObserverBase::buffer_changes();

    return( ret );
}

//------------------------------------------------------------------------------
// Implements DNBObserver<Adaptor,Obs>::on_event
//
// method called when an observed object generates and event
//------------------------------------------------------------------------------
template< class Adaptor, class Obs >
void
DNBObserver<Adaptor,Obs>::on_event(
        const DNBNotificationAgent::ConstPointer &ent, const EventSet &events )
    DNB_THROW_SPEC_NULL
{
    if( events.any() )
    {
        EventSet curr( _events[_write_buffer] );
        bool override = _data[_write_buffer].apply_event( 
                                                curr, events, _object, FALSE );
        _events[_write_buffer] = curr;

        if( add_to_observerlist( override ) )
            _data[_write_buffer].apply_event( _flushEvents, events, 
                                                                _object, TRUE );
    }
}

//------------------------------------------------------------------------------
// Implements DNBObserver<Adaptor,Obs>::force_observation w/events
//------------------------------------------------------------------------------
template< class Adaptor, class Obs >
void
DNBObserver<Adaptor,Obs>::force_observation( const EventSet &events, 
        bool collect )
    DNB_THROW_SPEC_NULL
{
    on_event( (const DNBNotificationAgent::ConstPointer)NULL, events );
    if( !collect )
        DNBObserverBase::buffer_changes();
}

//------------------------------------------------------------------------------
// Implements DNBObserver<Adaptor,Obs>::force_observation adaptive
//------------------------------------------------------------------------------
template< class Adaptor, class Obs >
void
DNBObserver<Adaptor,Obs>::force_observation( bool collect )
    DNB_THROW_SPEC_NULL
{
    if( _flushEvents.any() )
    {
        force_observation( _flushEvents, collect );
        DNBObserverBase::force_observation( collect );
    }
}
