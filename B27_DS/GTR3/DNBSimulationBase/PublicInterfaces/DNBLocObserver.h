/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBLocObserver.h
//   Location observer for DNBBasicEntity3D objects
//
//==============================================================================
//
// Usage notes: 
//   Use this class to observe location change events during the simulation,
//   published by DNBBasicEntity3D objects.
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     mmg          01/01/1999   Initial implementation
//     rtl          02/14/2002   Ported to HP
//     mmg          08/04/2003   added entity backpointer
//     mmh          03/04/2004   add modelId to be used in the functors
//
//==============================================================================
#if DNB_HAS_PRAGMA_ONCE
#pragma once
#endif
 
#ifndef _DNB_LOCOBSERVERBASE_H
#define _DNB_LOCOBSERVERBASE_H
 
#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>
#include <DNBSimTime.h>
#include <DNBObserver.h>
#include <DNBBasicEntity3D.h>
#include <DNBXform3D.h>

#include <DNBSimulationBase.h>

//------------------------------------------------------------------------------

/**
 * Observer for DNBBasicEntity3D objects
 */
class ExportedByDNBSimulationBase DNBObserveLocation
{
    DNB_DECLARE_OBSERVER_ADAPTOR( DNBBasicEntity3D );

public:
    /**
     * Constructs a DNBObserveLocation object.
     */
    DNBObserveLocation()
        DNB_THROW_SPEC_NULL;

    /**
     * Destructs a DNBObserveLocation object.
     */
    ~DNBObserveLocation()
        DNB_THROW_SPEC_NULL;

    /**
     * Returns the observed base location of the object.
     */
    DNBXform3D &
    getLocation()
        DNB_THROW_SPEC_NULL;

    /**
     * Returns the entity being observed.
     */
    DNBBasicEntity3D::Handle 
    getEntity()
        DNB_THROW_SPEC_NULL;

    /**
     * Returns the model id.
     */
    void* 
    getModelId()
        DNB_THROW_SPEC_NULL;

    /**
     * Returns the observed time of the object.
     */
    DNBSimTime getTime()
        DNB_THROW_SPEC_NULL;

    /**
     * Returns the observed base location of the object.
     */
    DNBXform3D &
    getAbsLocation()
        DNB_THROW_SPEC_NULL;
private:
    DNBXform3D               _Rellocation;
    DNBXform3D               _Abslocation;
    DNBBasicEntity3D::Handle _hParent;
    DNBBasicEntity3D::Handle _hEnt;
    void                    *_modelId;
    DNBSimTime               _time;
};

//------------------------------------------------------------------------------

typedef DNBObserver< DNBObserveLocation, DNBBasicEntity3D > DNBLocObserver;

DNB_DECLARE_SHARED_OBJECT_INST(DNBLocObserver,"be6df0ce-d500-11d3-a436-0004ac960d10");

#endif // _DNB_LOCOBSERVERBASE_H
