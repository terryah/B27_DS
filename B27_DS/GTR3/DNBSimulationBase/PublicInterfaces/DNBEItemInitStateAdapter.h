/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBEItemInitStateAdapter.h
//   Adapter for DNBIItemPersistentInitState interface implementation
//
//==============================================================================
//
// Usage notes: 
//   Derive your data extension from DNBEItemInitStateAdapter and use the 
//   default implementation for all/some of the methods provided by the 
//   interface. 
//   Also use the utility functions like _GetVisibility()/_SetVisibility() ...
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     sha          04/2001      Initial implementation
//     mmh          08/05/2002   Implement ChangeComponentState method - fix MLK
//     mmg          12/20/2002   added local mask capability for restore
//     mmh          05/08/2003   Add Transparency attribute
//     sha          07/22/2004   Modify InitializeInitState() method to create
//                               the state object if required
//     mmh          03/07/2005   D0485038: add new method to return mask of 
//                               existing attributes
//     mmh          06/01/2005   A0497083: add data member to force application
//                               of init state
//     mmh          03/12/2007   Add support for feat file implementation
//
//==============================================================================
#ifndef DNBEItemInitStateAdapter_H
#define DNBEItemInitStateAdapter_H

#include <DNBSimulationBase.h>
#include <CATBaseUnknown.h>
#include <CATBooleanDef.h>
#include <CATISpecObject.h>
#include <CATUnicodeString.h>
#include <DNBIItemPersistentInitState.h>

//------------------------------------------------------------------------------

/**
* Class Object modeler implementation class.
* <br>
* Default implementation (adapter) for:
*  <ol>
*  <li>@see DNBSimulationInterfaces.DNBIItemPersistentInitState
*  </ol>
* Using this prefered syntax will enable mkdoc to document your class.
*/
class ExportedByDNBSimulationBase DNBEItemInitStateAdapter: public DNBIItemPersistentInitState
{
    CATDeclareClass;
    
public:
    
    // Standard constructors and destructors for an implementation class
    // -----------------------------------------------------------------
    DNBEItemInitStateAdapter ();
    virtual ~DNBEItemInitStateAdapter ();
    
    /**
    * Default implementation for:
    * @see DNBSimulationInterfaces.DNBIItemPersistentInitState#SaveInitState
    */
    virtual void SaveInitState( const boolean refresh,
                                const int mask );
    
    /**
    * Default implementation for:
    * @see DNBSimulationInterfaces.DNBIItemPersistentInitState#RestoreInitState
    */
    virtual void RestoreInitState( const int mask,
                                   const DNBRestoreDestination where );
    
    /**
    * Default implementation for:
    * @see DNBSimulationInterfaces.DNBIItemPersistentInitState#HasInitState
    */
    virtual boolean HasInitState();
    
    /**
    * Default implementation for:
    * @see DNBSimulationInterfaces.DNBIItemPersistentInitState#InitializeInitState
    */
    virtual void InitializeInitState( CATISpecObject *obj, const boolean create );
    
    /**
     * Sets the local mask defining which data to restore.
     */
    virtual void SetLocalRestoreMask( const int mask );

    /**
     * Gets the local mask defining which data to restore.
     */
    virtual int GetLocalRestoreMask();

    /**
     * @nodoc
     */
    HRESULT ChangeComponentState( ComponentState iFromState,
                ComponentState iToState,
                const CATSysChangeComponentStateContext * iContext);

    /**
     * Default implementation for:
     * @see DNBSimulationInterfaces.DNBIItemPersistentInitState#GetExistingAttrMask
     */
    virtual int GetExistingAttrMask();

    /**
     * Default implementation for:
     * @see DNBSimulationInterfaces.DNBIItemPersistentInitState#MigrateInitState
     */
    virtual boolean MigrateInitState( CATBaseUnknown* ipBUOld, boolean bExt );

    /**
     * Default implementation for:
     * @see DNBSimulationInterfaces.DNBIItemPersistentInitState#GetSUInfo
     */
    virtual HRESULT GetSUInfo( CATUnicodeString& sStartUpType,
                               CATUnicodeString& sStartUpName,
                               CATUnicodeString& sStartUpFeat );

private:
    // The copy constructor and the equal operator must not be implemented
    // -------------------------------------------------------------------
    DNBEItemInitStateAdapter (DNBEItemInitStateAdapter &);
    DNBEItemInitStateAdapter& operator=(DNBEItemInitStateAdapter&);

protected:

    void _SetVisibility(int vis, const DNBRestoreDestination where );
    int  _GetVisibility();

    void _SetColor(int *colorVal, const DNBRestoreDestination where, 
            const int mask = DNB_COLOR_INIT_STATE | DNB_TRANSP_INIT_STATE );
    void _GetColor(int *colorVal, 
            const int mask = DNB_COLOR_INIT_STATE | DNB_TRANSP_INIT_STATE );

    void _SetPosition(double *posVal, const DNBRestoreDestination where);
    int  _GetPosition(double *posVal);
    
    CATISpecObject_var _state_obj;
    int _localRestoreMask;
    int _attrMask;
    int _forceInitState;

    CATUnicodeString    _sSUName;   // name of start-up
    CATUnicodeString    _sSUType;   // type of start-up
    CATUnicodeString    _sSUFile;   // file where start-up is defined
};

//------------------------------------------------------------------------------

#endif
