/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBEntityAttrExt.h
//   WDM extension of DNBBasicEntity3D object
//
//==============================================================================
//
// Usage notes: 
//   This extension should be created when building WDM (use the provided 
//   adapter DNBEBuildPrdAdapter when impementing the DNBIBuild extension). 
//   Use this extension during the simultaion to modify the graphical attributes
//   of the extended object.
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     mmg          01/01/1999   Initial implementation
//     mmh          01/18/2001   Added support for color/transparency
//     mmh          03/29/2001   Further enhanced the transparency support
//     mmh          10/01/2002   Add methods for highlight and restore color, 
//                               opacity
//     mmh          11/12/2002   Remove the EventColorResetMessage event
//     mmg          12/20/2002   Added support for propagation of color and
//                               visiblity in the WDM
//     mmh          06/05/2003   Add flag to force visibility for some objects
//     mmg          05/11/2004   Add flag to disable propagation 
//     mmg          08/17/2004   modify propagation override to be a static
//
//==============================================================================
#if DNB_HAS_PRAGMA_ONCE
#pragma once
#endif
 
#ifndef _DNB_ENTITYATTREXT_H
#define _DNB_ENTITYATTREXT_H
 
#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>
#include <DNBExtensionObject.h>
#include <DNBBasicEntity3D.h>

#include <DNBSimulationBase.h>

//------------------------------------------------------------------------------

/**
 * Class to extend a DNBBasicEntity3D derived object with graphical attributes.
 */
class ExportedByDNBSimulationBase DNBEntityAttrExt : public DNBExtensionObject
{
    DNB_DECLARE_DYNAMIC_CREATE( DNBEntityAttrExt );

    DNB_DECLARE_SHARED_OBJECT( DNBEntityAttrExt );
 
    DNB_DECLARE_EXTENSION_OBJECT( DNBEntityAttrExt, DNBBasicEntity3D );

    DNB_DECLARE_EXTENSION_FACTORY( DNBEntityAttrExt, DNBBasicEntity3D );

public:

    enum Events
    {
        EventFirst = DNBExtensionObject::EventLast - 1,
        EventVisibilityMessage,             // show/noshow 
        EventPropagateVisibilityMessage,    // show/noshow w/propagation
        EventColorModifiedMessage,          // color changed
        EventPropagateColorModifiedMessage, // color changed w/propagation
        EventHighlightMessage,              // highlight
        EventHighlightResetMessage,         // reset highlight
        EventLast
    };

    enum VisibilityMode { SHOW=0, HIDE };

    /**
     * Gets the visibility status.
     */
    VisibilityMode 
    getVisibilityMode() const 
        DNB_THROW_SPEC_NULL;

    /**
     * Sets the visibility status.
     */
    void 
    setVisibilityMode( VisibilityMode visibilityMode = SHOW, 
                       bool propagate = true )
        DNB_THROW_SPEC_NULL;

    /**
     * Gets the color attributes.
     */
    DNBVector3D
    getColor() const 
        DNB_THROW_SPEC_NULL;

    /**
     * Sets the color attributes.
     */
    void 
    setColor( const DNBVector3D &color, int opacityValue,
              int iColor = 1, int iOpacity = 1, bool propagate = true )
        DNB_THROW_SPEC_NULL;

    /**
     * Resets (clears) the color.
     */
    void 
    resetColor( bool propagate = true )
        DNB_THROW_SPEC_NULL;

    /**
     * Restores the original color.
     */
    void 
    restoreColor()
        DNB_THROW_SPEC_NULL;

    /**
     *  Gets the transparency attribut.
     */
    int
    getOpacityValue() const
        DNB_THROW_SPEC_NULL;

    /**
     * Gets the color inheritance.
     */
    int
    getIsColor() const
        DNB_THROW_SPEC_NULL;

    /**
     * Gets the transparency inheritance.
     */
    int
    getIsOpacity() const
        DNB_THROW_SPEC_NULL;

    /**
     * Gets the highlight attributes.
     */
    DNBVector3D
    getHighlight() const 
        DNB_THROW_SPEC_NULL;

    /**
     * Sets the highlight attributes.
     */
    void
    setHighlight( const DNBVector3D &highlight, 
                  int iHighlight = 1 )
        DNB_THROW_SPEC_NULL;

    /**
     * Gets the highlight inheritance.
     */
    int
    getIsHighlight() const
        DNB_THROW_SPEC_NULL;

    /**
     * Gets the apply visibility flag.
     */
    int
    getApplyVisibility() const
        DNB_THROW_SPEC_NULL;

    /**
     * Sets the apply visibility flag.
     */
    void 
    setApplyVisibility( int iApply )
        DNB_THROW_SPEC_NULL;

    /**
     * Override the propagation override flag 
     */
    static void 
    _setPropagationMode( bool iPropagate )
        DNB_THROW_SPEC_NULL;

protected:

    //  By defining the following methods in the protected section, application
    //  programmers must use the static factory methods to create and destroy
    //  instances of this class.
    // ------------------------------------------------------------------------
    DNBEntityAttrExt() 
        DNB_THROW_SPEC((DNBEResourceLimit));

    DNBEntityAttrExt( const DNBEntityAttrExt &right, CopyMode mode )
        DNB_THROW_SPEC((DNBEResourceLimit));
 
    virtual void
    propagateAttributes( ) const
        DNB_THROW_SPEC_NULL;

    virtual
    ~DNBEntityAttrExt( )
        DNB_THROW_SPEC_NULL;

private:

    // cache the visibility state for fewer changes.
    VisibilityMode  _visibilityMode;
    DNBVector3D     _color;
    int             _opacityValue;
    // 0 - do not apply, 1 - apply color/opacity
    int             _isColor;
    int             _isOpacity;
    // highlight
    DNBVector3D     _highlight;
    int             _isHighlight;
    int             _applyVis;
    static bool     _propagate;
};

//------------------------------------------------------------------------------

#endif // _DNB_ENTITYATTREXT_H
