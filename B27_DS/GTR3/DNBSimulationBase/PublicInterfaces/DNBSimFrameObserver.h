/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBSimFrameObserver.h
//   This class implements a DNBBasicEntity observer
//
//==============================================================================
//
// Usage notes: 
//   Use this class to observe simulation time (e.g. display the simulation
//   time in a dialog, update the Gantt chart time bar)
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     mmg          01/01/1999   Initial implementation
//     rtl          02/14/2002   Ported to HP
//
//==============================================================================
#if DNB_HAS_PRAGMA_ONCE
#pragma once
#endif
 
#ifndef _DNB_SIMFRAMEOBSERVER_H
#define _DNB_SIMFRAMEOBSERVER_H
 
#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>
#include <DNBObserver.h>
#include <DNBSimTime.h>
#include <DNBBasicWorld.h>

#include <DNBSimulationBase.h>

//------------------------------------------------------------------------------

/**
 * This class implements a DNBBasicEntity observer.
 */
class ExportedByDNBSimulationBase DNBObserveSimFrame
{
    DNB_DECLARE_OBSERVER_ADAPTOR( DNBBasicWorld );

public:
    /**
     * Constructs a DNBObserveSimFrame object.
     */
    DNBObserveSimFrame()
        DNB_THROW_SPEC_NULL;

    /**
     * Destructs a DNBObserveSimFrame object.
     */
    ~DNBObserveSimFrame()
        DNB_THROW_SPEC_NULL;

    /**
     * Returns the current simulation time.
     */
    DNBSimTime
    getTime() const
        DNB_THROW_SPEC_NULL;

    /**
     * Returns the current simulation step (replay).
     */
    DNBReal
    getStepVal() const
        DNB_THROW_SPEC_NULL;

private:
    DNBSimTime _time;
    DNBReal    _stepval;
};

//------------------------------------------------------------------------------

typedef DNBObserver< DNBObserveSimFrame, DNBBasicWorld > DNBSimFrameObserver;

DNB_DECLARE_SHARED_OBJECT_INST( DNBSimFrameObserver,"e1f05790-d4ff-11d3-a436-0004ac960d10");

#endif // _DNB_SIMFRAMEOBSERVER_H
