/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBAttrObserver.h
//      Observer for DNBEntityAttrExt extension.
//
//==============================================================================
//
// Usage notes: 
//      Use this class to observe events published by DNBEntityAttrExt.
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     mmg          01/01/1999   Initial implementation
//     mmh          01/18/2001   Added support for color/transparency
//     mmh          03/29/2001   Further enhanced the transparency support
//     rtl          02/14/2002   Ported to HP
//     mmh          10/02/2002   Add methods for highlight
//     mmg          08/04/2003   added entity backpointer
//     mmh          03/04/2004   add modelId to be used in the functors
//
//==============================================================================
#if DNB_HAS_PRAGMA_ONCE
#pragma once
#endif
 
#ifndef _DNB_ATTROBSERVER_H
#define _DNB_ATTROBSERVER_H
 
#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>
#include <DNBSimTime.h>
#include <DNBObserver.h>
#include <DNBEntityAttrExt.h>
#include <DNBVector3D.h>

#include <DNBSimulationBase.h>

//------------------------------------------------------------------------------

/**
 * Observer for DNBEntityAttrExt extension.
 */
class ExportedByDNBSimulationBase DNBObserveVisuAttribs
{
    DNB_DECLARE_OBSERVER_ADAPTOR( DNBEntityAttrExt );

public:
    /**
     * Constructs a DNBObserveVisuAttribs object.
     */
    DNBObserveVisuAttribs()
        DNB_THROW_SPEC_NULL;

    /**
     * Copies a DNBObserveVisuAttribs object.
     */
    DNBObserveVisuAttribs( const DNBObserveVisuAttribs &right )
        DNB_THROW_SPEC_NULL;

    /**
     * Destructs a DNBObserveVisuAttribs object.
     */
    ~DNBObserveVisuAttribs()
        DNB_THROW_SPEC_NULL;

    /**
     * Gets the color attributes.
     */
    DNBVector3D &
    getColor()
        DNB_THROW_SPEC_NULL;

    /**
     * Gets the visibility mode.
     */
    DNBEntityAttrExt::VisibilityMode
    getVisibility()
        DNB_THROW_SPEC_NULL;

    /**
     * Gets the transparency (opacity) attribute.
     */
    int
    getOpacity()
        DNB_THROW_SPEC_NULL;

    /**
     * Returns the color inheritance status.
     */
    int
    getIsColor()
        DNB_THROW_SPEC_NULL;

    /**
     * Returns the transparency inheritance status.
     */
    int
    getIsOpacity()
        DNB_THROW_SPEC_NULL;

    /**
     * Gets the highlight attributes.
     */
    DNBVector3D &
    getHighlight()
        DNB_THROW_SPEC_NULL;

    /**
     * Returns the highlight status.
     */
    int
    getIsHighlight()
        DNB_THROW_SPEC_NULL;

    /**
     * Returns the entity being observed.
     */
    DNBEntityAttrExt::Handle 
    getEntity()
        DNB_THROW_SPEC_NULL;

    /**
     * Returns the model id.
     */
    void* 
    getModelId()
        DNB_THROW_SPEC_NULL;

    /**
     * Returns the observed time of the object.
     */
    DNBSimTime 
    getTime()
        DNB_THROW_SPEC_NULL;

    /**
     * Sets the color attributes.
     */
    void
    _setColor( int isit, DNBVector3D &color )
        DNB_THROW_SPEC_NULL;

    /**
     * Sets the opacity
     */
    void
    _setOpacity( int isit, int opacity )
        DNB_THROW_SPEC_NULL;

private:
    DNBEntityAttrExt::VisibilityMode _visibility;
    DNBVector3D                      _color;
    int                              _opacity;
    int                              _isColor;   // 0 if no color
    int                              _isOpacity; // 0 if no opacity
    DNBVector3D                      _highlight;
    int                              _isHighlight;   // 0 if no highlight
    DNBEntityAttrExt::Handle         _hEnt;
    void                            *_modelId;
    DNBSimTime                       _time;
};

//------------------------------------------------------------------------------

typedef DNBObserver< DNBObserveVisuAttribs, DNBEntityAttrExt > DNBAttrObserver;

DNB_DECLARE_SHARED_OBJECT_INST(DNBAttrObserver,"88e80660-d500-11d3-a436-0004ac960d10");

#endif // _DNB_ATTROBSERVER_H
