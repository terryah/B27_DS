/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBCATTraversers.h
//   This file provides the basic implementation for the traverser/visitor
//   mechanism:
//      DNBTraversalVisitor         : base class for the visitor
//      DNBCATTraverser             : base class for the traverser (descendent)
//      DNBProcessTraverser         : traverser for process trees (descendent)
//      DNBProductTraverser         : traverser for product trees (descendent)
//      DNBAscendentProductTraverser: traverser for product trees (ascendent)
//      DNBCATListTraverser         : traverser for lists
//
//==============================================================================
//
// Usage notes: 
//   Derive your visitor from DNBTraversalVisitor base class and override
//   PreOrder()/PostOrder() methods. 
//   Also use one of the existing traversers if traversing trees/lists or
//   derive your own implementation from DNBCATTraverser and override
//   Traverse() and GetChildren().
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     mmg          01/01/1999   Initial implementation
//     sha          01/01/2000   Added the ascendent traverser
//     mmh          11/30/2001   Change parameters from reference to pointer
//     mmh          07/18/2002   Add Pre/Post Traverse for DNBCATListTraverser
//     mmh          11/14/2003   Add RTTI info and enable flag in the visitor, 
//                               add new methods for the basic traverser to 
//                               support the changes in visitor
//     mmh          12/10/2003   Add new method to retrieve the enable/disable
//                               status for the visitors
//     mmh          01/21/2004   Add method to retrieve a specific visitor
//     mmh          11/08/2006   Add flag in DNBTraversalVisitor to prevent its
//                               deletion by the traverser
//
//==============================================================================
#ifndef DNBCATTraversers_H
#define DNBCATTraversers_H

#include <DNBSystemBase.h>
#include <scl_list.h>
#include <DNBSystemDefs.h>

#include <DNBDynamicObject.h>

#include <DNBCATBase.h>
#include <CATBaseUnknown.h>
#include <CATIProduct.h>
#include <DNBCATDefs.h>

#include <DNBSimulationBase.h>

//------------------------------------------------------------------------------

/**
 * MACROS
 */

#define _DNB_DECLARE_METHOD(_CLASS)                                      \
public:                                                                  \
    _CLASS( const _CLASS &right, CopyMode mode )                         \
        DNB_THROW_SPEC((DNBEResourceLimit));


#define _DNB_DEFINE_METHOD(_CLASS,_BASE)                                 \
_CLASS::_CLASS( const _CLASS &right, CopyMode mode )                     \
DNB_THROW_SPEC((DNBEResourceLimit))                                      \
:_BASE(right, mode)                                                      \
{}                                                                       \
void _CLASS::orphanMembers()                                             \
DNB_THROW_SPEC_NULL                                                      \
{}


/**
 * The macro DNB_DECLARE_VISITOR must be included in the header 
 * file of every class derived from DNBTraversalVisitor that 
 * doesn't have a default constructor
 * DNB_DECLARE_VISITOR( classname )
 */
#define DNB_DECLARE_VISITOR(_CLASS)                                     \
    _DNB_DECLARE_METHOD(_CLASS)                                         \
protected:                                                              \
    _CLASS()                                                            \
       DNB_THROW_SPEC_NULL                                              \
       {}                                                               \
    DNB_DECLARE_DYNAMIC_CREATE(_CLASS) 


/**
 * The macro DNB_DECLARE_VISITOR_NOCTR must be included in the
 * header file of every class derived from DNBTraversalVisitor that 
 * has a default constructor.
 * DNB_DECLARE_VISITOR_NOCTR( classname ).
 */
#define DNB_DECLARE_VISITOR_NOCTR(_CLASS)                               \
    _DNB_DECLARE_METHOD(_CLASS)                                         \
    DNB_DECLARE_DYNAMIC_CREATE(_CLASS)


/**
 * The macro DNB_DEFINE_VISITOR must be included in the
 * .cpp file of every class derived from DNBTraversalVisitor
 * DNB_DEFINE_VISITOR( classname, baseclassname )
 */
#define DNB_DEFINE_VISITOR(_CLASS,_BASE)                               \
    DNB_DEFINE_DYNAMIC_CREATE_B1(_CLASS,_BASE);                        \
    _DNB_DEFINE_METHOD(_CLASS,_BASE)

//------------------------------------------------------------------------------

/**
 * Base class for the visitor.
 * <b>Role</b>: Provided as a base class for deriving new visitors. 
 * Derive your visitor from DNBTraversalVisitor base class and override
 * PreOrder()/PostOrder() methods. 
 */
class ExportedByDNBSimulationBase DNBTraversalVisitor : public DNBDynamicObject
{

    DNB_DECLARE_VISITOR_NOCTR(DNBTraversalVisitor);

public:
    enum TraversalMode{ NoAction, 
                        ContinueTraversal, 
                        BreakTraversalAtPreorder,  // do not traverse my children, but let my parent proceed with its remaining children and its postorder
                        BreakTraversal,            // do not let my parent continue traversing its children list after me, do not let my parent to do a postorder
                        StopTraversal };
    /**
     * Constructs a DNBTraversalVisitor object.
     * @param iAlways
     *   The visitor's mode/
     *   <br><b>Legal values</b>: The default mode expressed by FALSE specifies
     *   that the visitor will be appended at the end of the list of visitors 
     *   held by the traverser. A value of TRUE will force the visitor to be
     *   inserted at the begining of the traverser's list.
     */
    DNBTraversalVisitor( boolean iAlways = FALSE, boolean iDeletable = TRUE );

    /**
     * Destructs a DNBTraversalVisitor object.
     */
    virtual ~DNBTraversalVisitor();
    
    /**
     * Called by the traverser before any of the given object's children have 
     * been visited.
     */
    virtual TraversalMode
    PreOrder( const CATBaseUnknown_var & );
    
    /**
     * Called by the traverser after all of the given object's children have 
     * been visited.
     */
    virtual TraversalMode
    PostOrder( const CATBaseUnknown_var & );
    
    /**
     * Returns the mode of the visitor.
     */
    boolean
    IsAlways();
    
    /**
     * Enables/disables the visitor.
     */
    HRESULT
    EnableVisitor( boolean iEnable );
    
    /**
     * Returns the enable/disable status of the visitor.
     */
    boolean
    IsEnabled();
    
    /**
     * Returns the deletion mode of the visitor.
     */
    boolean
    IsDeletable();
    
private:
    boolean             _always;
    boolean             _enabled;
    boolean             _deletable;
};

//------------------------------------------------------------------------------

/**
 * Base class for all traversers.
 */
class ExportedByDNBSimulationBase DNBCATTraverser 
{
public:
    typedef DNBTraversalVisitor::TraversalMode TraversalMode;

    /**
     * Constructs a DNBCATTraverser object.
     */
    DNBCATTraverser();

    /**
     * Constructs a DNBCATTraverser object with a given visitor.
     */
    DNBCATTraverser( DNBTraversalVisitor * );

    /**
     * Destructs a DNBCATTraverser object.
     */
    virtual ~DNBCATTraverser();
    
    /**
     * Generic traversal algorithm, stating from the given node.
     */
    virtual TraversalMode
    Traverse( const CATBaseUnknown_var & );
    
    /**
     * Visits the given node by applying all the visitors.
     */
    virtual TraversalMode
    Visit( const CATBaseUnknown_var & );
    
    /**
     * Returns the stop node.
     */
    CATBaseUnknown_var & 
    GetStop();
    
    /**
     * Initializes the traverser with the given visitor.
     */
    HRESULT 
    SetVisitor( DNBTraversalVisitor * );
    
    /**
     * Appends a new visitor to the list.
     */
    HRESULT 
    AppendVisitor( DNBTraversalVisitor * );
    
    /**
     * Prepends a new visitor to the list.
     */
    HRESULT 
    PrependVisitor( DNBTraversalVisitor * );
    
    /**
     * Removes the given visitor from the list.
     */
    HRESULT 
    RemoveVisitor( DNBTraversalVisitor * );
    
    /**
     * Clears all visitors.
     */
    HRESULT 
    ClearVisitors();
    
    /**
     * Enables/disables the visitor with the specified class name.
     */
    HRESULT 
    EnableVisitor( scl_string iClassName, boolean iEnable );

    /**
     * Retrieves the enable/disable status for the visitor with the 
     * specified class name.
     */
    HRESULT 
    GetEnableStatus( scl_string iClassName, boolean &oEnable );
    
    /**
     * Retrieves the visitor with the specified class name.
     */
    DNBTraversalVisitor * 
    GetVisitor( scl_string iClassName );
    
protected:
    /**
     * Generic method, needs to be overriden by the derived clases.
     */
    virtual CATListValCATBaseUnknown_var*
    GetChildren( const CATBaseUnknown_var & );
    
    /**
     * Called by the Traverse(), before any of the given object's children 
     * have been visited. Applies the list of visitors to the given
     * node.
     */
    TraversalMode
    PreOrder( const CATBaseUnknown_var & );
    
    /**
     * Called by the Traverse(), after all of the given object's children 
     * have been visited. Applies the list of visitors to the given
     * node.
     */
    TraversalMode
    PostOrder( const CATBaseUnknown_var & );
    
private:
    typedef scl_list<DNBTraversalVisitor *,
        DNB_ALLOCATOR(DNBTraversalVisitor *) > Vislist;
    typedef scl_list<DNBTraversalVisitor *,
        DNB_ALLOCATOR(DNBTraversalVisitor *) >::iterator Visiter;
    
    Vislist            _visitors;
    CATBaseUnknown_var _stopObj;
    
};

//------------------------------------------------------------------------------

/**
 * Process based specific traverser, derived from DNBCATTraverser.
 */
class ExportedByDNBSimulationBase DNBProcessTraverser : public DNBCATTraverser
{
public:
    /**
     * Constructs a DNBProcessTraverser object.
     */
    DNBProcessTraverser();

    /**
     * Constructs a DNBProcessTraverser object with a given visitor.
     */
    DNBProcessTraverser( DNBTraversalVisitor * );

    /**
     * Destructs a DNBProcessTraverser object.
     */
    ~DNBProcessTraverser();
    
    /**
     * Specific process based implementation, returning the list of children
     * for the given object.
     */
    CATListValCATBaseUnknown_var*
    GetChildren( const CATBaseUnknown_var & );
};

//------------------------------------------------------------------------------

/**
 * Product based specific traverser, derived from DNBCATTraverser.
 */
class ExportedByDNBSimulationBase DNBProductTraverser : public DNBCATTraverser
{
public:
    /**
     * Constructs a DNBProductTraverser object.
     */
    DNBProductTraverser();

    /**
     * Constructs a DNBProductTraverser object with a given visitor.
     */
    DNBProductTraverser( DNBTraversalVisitor * );

    /**
     * Destructs a DNBProductTraverser object.
     */
    ~DNBProductTraverser();

    
    /**
     * Specific product based implementation, returning the list of children
     * for the given object.
     */
    CATListValCATBaseUnknown_var*
    GetChildren( const CATBaseUnknown_var & );
};

//------------------------------------------------------------------------------

/**
 * Product based specific traverser, derived from DNBCATTraverser, used for
 * assending traversal.
 */
class ExportedByDNBSimulationBase DNBAscendentProductTraverser : public DNBCATTraverser
{
public:
    typedef DNBTraversalVisitor::TraversalMode TraversalMode;

    /**
     * Constructs a DNBAscendentProductTraverser object.
     */
    DNBAscendentProductTraverser();

    /**
     * Constructs a DNBAscendentProductTraverser object with a given visitor.
     */
    DNBAscendentProductTraverser( DNBTraversalVisitor * );

    /**
     * Destructs a DNBAscendentProductTraverser object.
     */
    ~DNBAscendentProductTraverser();
    
    /**
     * Overrides the default implementation in order to perform an ascendent
     * traversal.
     */
    virtual TraversalMode
    Traverse( const CATBaseUnknown_var & );

    /**
     * Returns the father of the given object.
     */
    CATBaseUnknown_var
    GetFather( const CATBaseUnknown_var & );
};

//------------------------------------------------------------------------------

/**
 * Generic list based traverser, derived from DNBCATTraverser.
 */
class ExportedByDNBSimulationBase DNBCATListTraverser : public DNBCATTraverser
{
public:
    typedef DNBTraversalVisitor::TraversalMode TraversalMode;

    /**
     * Constructs a DNBCATListTraverser object.
     */
    DNBCATListTraverser();

    /**
     * Constructs a DNBCATListTraverser object with a given visitor.
     */
    DNBCATListTraverser( DNBTraversalVisitor * );

    /**
     * Destructs a DNBCATListTraverser object.
     */
    ~DNBCATListTraverser();
    
    /**
     * Traversal implementation for a CATListValCATBaseUnknown_var list.
     */
    TraversalMode
    Traverse( const CATListValCATBaseUnknown_var* );
    
    /**
     * Traversal implementation for a CATListPtrCATBaseUnknown list.
     */
    TraversalMode
    Traverse( const CATListPtrCATBaseUnknown * );
    
    /**
     * Traversal implementation for a CATListValCATBaseUnknown_var list 
     * (pre-order).
     */
    TraversalMode
    PreTraverse( const CATListValCATBaseUnknown_var* );
    
    /**
     * Traversal implementation for a CATListPtrCATBaseUnknown list(pre-order).
     */
    TraversalMode
    PreTraverse( const CATListPtrCATBaseUnknown * );
    
    /**
     * Traversal implementation for a CATListValCATBaseUnknown_var list 
     * (post-order).
     */
    TraversalMode
    PostTraverse( const CATListValCATBaseUnknown_var* );
    
    /**
     * Traversal implementation for a CATListPtrCATBaseUnknown list(post-order).
     */
    TraversalMode
    PostTraverse( const CATListPtrCATBaseUnknown * );
    
    /**
     * Returns the stop index.
     */
    int
    GetStopIndex();
    
private:
    int _stopIndex;
};

//------------------------------------------------------------------------------

#endif // DNBCATTraversers_H
