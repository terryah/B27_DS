/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBStartFinishObserver.h
//   Observer for DNBStartFinishExt extension.
//
//==============================================================================
//
// Usage notes: 
//   Use this clase as an observer for DNBStartFinishExt objects.
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     mmh          06/07/2001   Initial implementation
//     rtl          02/14/2002   Ported to HP
//
//==============================================================================
#if DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _DNB_STARTFINISHOBSERVER_H
#define _DNB_STARTFINISHOBSERVER_H

#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>

#include <DNBObserver.h>
#include <DNBSimulationBase.h>
#include <DNBStartFinishExt.h>

//------------------------------------------------------------------------------

/**
 * Observer for DNBStartFinishExt extension.
 */
class ExportedByDNBSimulationBase DNBObserveStartFinish
{
    DNB_DECLARE_OBSERVER_ADAPTOR( DNBStartFinishExt );
    
public:
    /**
     * Constructs a DNBObserveStartFinish entity.
     */
    DNBObserveStartFinish()
        DNB_THROW_SPEC_NULL;
    ~DNBObserveStartFinish()
        DNB_THROW_SPEC_NULL;
    
    /**
     * Gets the current status.
     */
    int getStatus() 
        DNB_THROW_SPEC_NULL;
    
private:
    int    _started;
};

//------------------------------------------------------------------------------

typedef DNBObserver< DNBObserveStartFinish, DNBStartFinishExt > DNBStartFinishObserver;

DNB_DECLARE_SHARED_OBJECT_INST( DNBStartFinishObserver,"e3fc574e-5b47-11d5-b948-00b0d078dba8");

#endif // _DNB_STARTFINISHOBSERVER_H
