/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBMsgWndExt.h
//      Extension providing support for the messaging system.
//
//==============================================================================
//
// Usage notes: 
//      This extension of a basic entity is providing support for the messaging 
//      system.
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     mmh          05/29/2001   Initial implementation
//     mmh          07/31/2001   Store the index in the DNBMsgWndInfo, add 
//                               updateMsgWnd().
//     mmh          10/08/2001   Add flag for auto clean up.
//     mmh          10/03/2001   Add methods that accept regular char* strings.
//     mmh          02/08/2002   Add priority level for messages, modify the 
//                               nextMsg(), startMsg() methods
//     mmh          12/04/2007   Add simulation time for messages
//
//==============================================================================
#if DNB_HAS_PRAGMA_ONCE
#pragma once
#endif
 
#ifndef _DNB_MSGWNDEXT_H
#define _DNB_MSGWNDEXT_H
 
#include <DNBSystemBase.h>
#include <scl_vector.h>
#include <DNBSystemDefs.h>

#include <DNBExtensionObject.h>
#include <DNBBasicEntity.h>
#include <DNBSimTime.h>

#include <DNBSimulationBase.h>

#include <CATBooleanDef.h>
#include <DNBMsgPriorityLevel.h>

class DNBMessage;

//------------------------------------------------------------------------------

/**
 * Extension providing support for the messaging system.
 */
class ExportedByDNBSimulationBase DNBMsgWndExt : public DNBExtensionObject
{
    DNB_DECLARE_DYNAMIC_CREATE( DNBMsgWndExt );

    DNB_DECLARE_SHARED_OBJECT( DNBMsgWndExt );
 
    DNB_DECLARE_EXTENSION_OBJECT( DNBMsgWndExt, DNBBasicEntity );

    DNB_DECLARE_EXTENSION_FACTORY( DNBMsgWndExt, DNBBasicEntity );

public:

    enum Events
    {
        EventFirst = DNBExtensionObject::EventLast - 1,
        EventClearMsg,
        EventNextMsg,
        EventCreateMsgWnd,
        EventShowMsgWnd,
        EventHideMsgWnd,
        EventDeleteMsgWnd,
        EventSetSizeMsgWnd,
        EventSetPositionMsgWnd,
        EventLast
    };

    enum StatusMode { SAME=0, CHANGED, DELETED };
    typedef scl_vector< scl_wstring, DNB_ALLOCATOR(scl_wstring) >   DNBMsgQueue;
    typedef scl_vector< int, DNB_ALLOCATOR(int) >           DNBPLevelQueue;
    typedef scl_vector< Events, DNB_ALLOCATOR(Events) >     DNBEventQueue;
    typedef scl_vector< double, DNB_ALLOCATOR(double) >     DNBSimTimeQueue;

    struct DNBMsgWndInfo
    {
        scl_wstring         _title;
        int             _index;
        DNBMsgQueue     _msgQueue;
        DNBPLevelQueue  _PLvlQueue;
        DNBSimTimeQueue _simTimeQueue;
        int             _x;
        int             _y;
        int             _h;
        int             _w;
        StatusMode      _status;
        boolean         _wasDeleted;
        DNBEventQueue   _evQueue;
        boolean         _autoClean;
        
        DNBMsgWndInfo( scl_wstring title = L"", int index = 0,
                       boolean autoClean = TRUE,
                       int x = 0, int y = 0, int h = 0, int w = 0,
                       StatusMode status = SAME ) :
            _title( title ), _index( index ),
            _autoClean( autoClean ),
            _x( x ), _y( y ), _h( h ), _w( w ),
            _status( status ), _wasDeleted( FALSE )
        {}
    };

    typedef scl_vector< DNBMsgWndInfo, DNB_ALLOCATOR(DNBMsgWndInfo) > 
                                                        DNBMsgWndInfoVector;

    // message handling interface
    /**
     * Creates a new message in the message window with the specified ID. 
     *  The previous message will be deleted.
     */
    void startMsg( const scl_wstring &msg, int ID=0, int iPLvl = DNB_MSG_INFO );
    /**
     * Creates a new message in the message window with the specified ID. 
     *  The previous message will be deleted.
     */
    void startMsg( const DNBMessage &msg, int ID=0, int iPLvl = DNB_MSG_INFO );
    /**
     * Creates a new message in the message window with the specified ID. 
     *  The previous message will be deleted.
     */
    void startMsg( const char *msg, int ID=0, int iPLvl = DNB_MSG_INFO );
    /**
     * Appends the given message to the message window with the specified ID.
     */
    void nextMsg( const scl_wstring &msg, int ID=0, int iPLvl = DNB_MSG_INFO );
    /**
     * Appends the given message to the message window with the specified ID.
     */
    void nextMsg( const DNBMessage &msg, int ID=0, int iPLvl = DNB_MSG_INFO );
    /**
     * Appends the given message to the message window with the specified ID.
     */
    void nextMsg( const char *msg, int ID=0, int iPLvl = DNB_MSG_INFO );
    /**
     * Clears all messages from the message window with the specified ID.
     */
    void clearMsg( int ID=0 );

    // message window handling interface
    /**
     * Creates a window with the given title, position and size.
     * @return The ID of the new created window.
     */
    int createMsgWnd( const scl_wstring &title, boolean autoClean = TRUE,
        int x=0, int y=0, int h=0, int w=0 );
    /**
     * Creates a window with the given title, position and size.
     * @return The ID of the new created window.
     */
    int createMsgWnd( const DNBMessage &title, boolean autoClean = TRUE,
        int x=0, int y=0, int h=0, int w=0 );
    /**
     * Displays the message window with the specified ID.
     */
    void showMsgWnd( int ID );
    /**
     * Hides the message window with the specified ID.
     */
    void hideMsgWnd( int ID );
    /**
     * Deletes the message window with the specified ID.
     */
    void deleteMsgWnd( int ID );
    /**
     * Sets the position of the message window with the specified ID.
     */
    void setMsgWndPosition( int ID, int x, int y );
    /**
     * Gets the position of the message window with the specified ID.
     */
    void getMsgWndPosition( int ID, int &x, int &y );
    /**
     * Sets the size of the message window with the specified ID.
     */
    void setMsgWndSize( int ID, int h, int w );
    /**
     * Gets the size of the message window with the specified ID.
     */
    void getMsgWndSize( int ID, int &h, int &w );

    /**
     * Returns 1 if the ID belongs to a valid window.
     */
    int isValidID( int ID );
    /**
     * Returns the vector containing information about the message windows.
     */
    DNBMsgWndInfoVector& GetMsgWndInfoVector();
    /**
     * Data clean-up for the changed message windows.
     */
    void dataCleanUp();
    /**
     * Adds a message window with the given title, at the specified position.
     */
    void updateMsgWnd( const scl_wstring& title, const int index, const int x=0, 
        const int y=0, const int h=0, const int w=0 );
    /**
     * Sets the current simulation time, to be used for displayed messages.
     */
    void setCrtSimTime( const DNBSimTime &iTime );

protected:
    //  By defining the following methods in the protected section, application
    //  programmers must use the static factory methods to create and destroy
    //  instances of this class.
    // ------------------------------------------------------------------------
    /**
     * Constructs a DNBMsgWndExt object.
     */
    DNBMsgWndExt() 
        DNB_THROW_SPEC((DNBEResourceLimit));
    /**
     * Copy constructor.
     */
    DNBMsgWndExt( const DNBMsgWndExt &right, CopyMode mode )
        DNB_THROW_SPEC((DNBEResourceLimit));
    virtual
    ~DNBMsgWndExt( )
        DNB_THROW_SPEC_NULL;

private:
    DNBMsgWndInfoVector                 _msgWndInfoVector;
    boolean                             _firstWndCreated;
    DNBSimTime                          _crtSimTime;
};

//------------------------------------------------------------------------------

#endif // _DNB_MSGWNDEXT_H
