/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBParametricCamera3D.h
//  This class represents an abstract body with extent.  It will eventually
//  include support for tag points and paths.
//
//==============================================================================
//
// Usage notes: 
//   Use this class to model modify the camara attributes during simulation.
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     mmg          01/01/1999   Initial implementation
//     sha          09/07/2001   Added new method cancelEventPropagation()
//     mmh          06/27/2003   Add constructor for DNBCameraParameters
//
//==============================================================================
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_PARAMETRICCAMERA3D_H_
#define _DNB_PARAMETRICCAMERA3D_H_

#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>

#include <DNBBasicCamera3D.h>

#include <DNBSimulationBase.h>

//------------------------------------------------------------------------------

/**
 * This class represents an abstract body with extent.  It will eventually
 * include support for tag points and paths.
 */
class ExportedByDNBSimulationBase DNBParametricCamera3D : 
                    public DNBBasicCamera3D
{
    DNB_DECLARE_DYNAMIC_CREATE( DNBParametricCamera3D );

    DNB_DECLARE_SHARED_OBJECT( DNBParametricCamera3D );

    DNB_DECLARE_EXTENDIBLE_OBJECT( DNBParametricCamera3D );
    
    DNB_DECLARE_EXTENDIBLE_FACTORY( DNBParametricCamera3D );

public:

    enum DNBCameraType{ Conic, Cylindric, Unknown };

    typedef struct DNBCamParam
    {
        DNBVector3D   origin;
        DNBVector3D   target;
        DNBVector3D   direction;
        DNBVector3D   updirection;
        DNBReal       nearplane;
        DNBReal       farplane;
        DNBReal       fieldofview;
        DNBCameraType type;
        

        DNBCamParam()
            : origin(), target(), direction(), updirection(), nearplane(0.0),
            farplane(0.0), fieldofview(0.0), type(Unknown)
        {}

    } DNBCameraParameters;

    /**
     * Returns the type name of the entity.
     */
    virtual DNBMessage
    getEntityTypeName( ) const
        DNB_THROW_SPEC_NULL;

    /**
     * Returns the origin of the camera.
     */
    virtual DNBVector3D &
    getOrigin() 
        DNB_THROW_SPEC_NULL;

    /**
     * Returns the direction of the camera.
     */
    virtual DNBVector3D &
    getDirection() 
        DNB_THROW_SPEC_NULL;

    /**
     * Returns the up direction of the camera.
     */
    virtual DNBVector3D &
    getUpDirection() 
        DNB_THROW_SPEC_NULL;

    /**
     * Returns the target.
     */
    virtual DNBVector3D &
    getTarget() 
        DNB_THROW_SPEC_NULL;

    /**
     * Returns the near plane.
     */
    virtual DNBReal  
    getNearPlane() const
        DNB_THROW_SPEC_NULL;

    /**
     * Returns the far plane.
     */
    virtual DNBReal  
    getFarPlane() const
        DNB_THROW_SPEC_NULL;

    /**
     * Returns the field of view.
     */
    virtual DNBReal  
    getFieldOfView() const
        DNB_THROW_SPEC_NULL;

    /**
     * Returns the camera type.
     */
    virtual DNBCameraType  
    getType() const
        DNB_THROW_SPEC_NULL;

    /**
     * Sets the origin of the camera.
     */
    virtual void         
    setOrigin( DNBVector3D &origin) 
        DNB_THROW_SPEC_NULL;

    /**
     * Sets the direction of the camera.
     */
    virtual void         
    setDirection( DNBVector3D &direction) 
        DNB_THROW_SPEC_NULL;

    /**
     * Sets the up direction of the camera.
     */
    virtual void         
    setUpDirection( DNBVector3D &updir )
        DNB_THROW_SPEC_NULL;

    /**
     * Sets the target (DNBReal).
     */
    virtual void         
    setTarget( DNBReal targetDistance )
        DNB_THROW_SPEC_NULL;

    /**
     * Sets the target (DNBVector3D).
     */
    virtual void         
    setTarget( DNBVector3D &target )
        DNB_THROW_SPEC_NULL;

    /**
     * Sets the near plane.
     */
    virtual void         
    setNearPlane( DNBReal nearplane )
        DNB_THROW_SPEC_NULL;

    /**
     * Sets the far plane.
     */
    virtual void         
    setFarPlane( DNBReal farplane )
        DNB_THROW_SPEC_NULL;

    /**
     * Sets the field of view.
     */
    virtual void  
    setFieldOfView( DNBReal fov ) 
        DNB_THROW_SPEC_NULL;

    /**
     * Sets the camera type.
     */
    virtual void  
    setType( DNBCameraType type ) 
        DNB_THROW_SPEC_NULL;

    /**
     * Sets all camera parameters.
     */
    virtual void  
    setParameters( DNBCameraParameters &params ) 
        DNB_THROW_SPEC_NULL;

    /**
     * Returns all camera parameters.
     */
    virtual DNBCameraParameters &
    getParameters() 
        DNB_THROW_SPEC_NULL;
    
    /**
     * Prevents the events from being dispatched to the subscribers.
     */
    void
    cancelEventPropagation()
        DNB_THROW_SPEC_NULL;

	/**
	 * Sets the relative object 
	 */
	void
	setRelativeObject(DNBBasicEntity3D::Handle relObj )
		DNB_THROW_SPEC_NULL;

	/**
	 * Returns the relative object
	 */
	DNBBasicEntity3D::Handle
	getRelativeObject()
		DNB_THROW_SPEC_NULL;


    enum Events
    {
        EventFirst = DNBBasicCamera3D::EventLast - 1,
        EventNearChanged,
        EventFarChanged,
        EventFOVChanged,
        EventTypeChanged,
        EventLast
    };

protected:
    //  By defining the following methods in the protected section, application
    //  programmers must use the static factory methods to create and destroy
    //  instances of this class.
    // ------------------------------------------------------------------------
    DNBParametricCamera3D( )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit, DNBENetworkError));

    DNBParametricCamera3D( const DNBParametricCamera3D &right, CopyMode mode )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit, DNBENetworkError));

    virtual
    ~DNBParametricCamera3D( )
        DNB_THROW_SPEC_NULL;

    void
    orthogonalize()
        DNB_THROW_SPEC_NULL;

private:
    DNBCameraParameters  _params;
    DNBVector3D          _stable_updirection;
    DNBReal              _targetDistance;
	DNBBasicEntity3D::Handle _relativeObj;
};

//------------------------------------------------------------------------------

#endif  // _DNB_PARAMETRICCAMERA3D_H_
