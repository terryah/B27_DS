/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBBasicProclet.h
//      Base class for the proclets used during the time based simulation.
//
//==============================================================================
//
// Usage notes: 
//      Use this base class to derive a new proclet to be used during the 
//      time based simulation.
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     mmg          01/01/1999   Initial implementation
//     sha          08/31/2001   Added new set/get methods for MODEL ID.
//     sha          02/21/2002   Modify to allow controlling the proclet
//                               (static method/data member)
//     sha          05/03/2002   Move the entire DNBProcessProcletBase content
//                               in this new class
//     mmh          05/30/2002   Add thread safe methods to get/set 
//                               _procletEnabled
//     mmh          08/13/2002   Add new cleanUp() method
//     rtl          10/24/2002   Provide RTTI facility
//     rtl          04/15/2003   Change RTTI macro to the one with no ctor
//     mmh          03/17/2004   Add virtual method to replace the postEnd phase
//                               callback used in PV (A0418680)
//     mmh          04/14/2005   add methods for behavior options (R16DMR003)
//     mmg          05/15/2007   add behavior to allow tracking of all 
//                               activities, not just leaf ones
//
//==============================================================================

#if DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _DNB_BASIC_PROCLET_H
#define _DNB_BASIC_PROCLET_H

#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>
#include <DNBBasicEntity.h>
#include <DNBProclet.h>
#include <DNBSysFastMutexLock.h>
#include <DNBSimulationBase.h>

#define DNB_PARENT_BEHAVIOR_TEXT    0x00000001
#define DNB_PARENT_BEHAVIOR_ANNOT   0x00000002
#define DNB_PARENT_BEHAVIOR_VIS     0x00000004
#define DNB_PAUSE_BEHAVIOR_HLNK     0x00000008
#define DNB_PARENT_BEHAVIOR_ACTIVE  0x00000010

//------------------------------------------------------------------------------

/**
 * Base class for the proclets used during the time based simulation.
 * <b>Role</b>: 
 */


class ExportedByDNBSimulationBase DNBBasicProclet : public DNBProclet
{

    DNB_DECLARE_SIMLET_NOCTR(DNBBasicProclet);

public:

    /**
     * Constructs a DNBBasicProclet object.
     */
    DNBBasicProclet( const Time &startTime=0, 
                     const Time &duration=0,
                     void *model_id = NULL,
                     DNBBasicEntity::Handle hObj = NULL,
                     const Time &updateFrequency = 0 )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Destructs a DNBBasicProclet object.
     */
    ~DNBBasicProclet()
        DNB_THROW_SPEC_ANY;

    /**
     * Sets the MODEL ID.
     */
    void
    SetModelID( void * );

    /**
     * Returns the MODEL ID.
     */
    void *
    GetModelID() const;

    /**
     * Performs the clean-up of the proclet.
     */
    void
    cleanUp()
        DNB_THROW_SPEC_ANY;

    /**
     * Contains actions to be executed later.
     */
    virtual void
    postAct()
        DNB_THROW_SPEC_ANY;

    /**
     * Contains actions to be executed when the Parent ends (if Behavior is set 
     * to Parent).
     */
    virtual void
    resetEffect( int bFlag )
        DNB_THROW_SPEC_ANY;

    /**
     * Sets the enabled/disabled status of all the proclets.
     */
    static void
    setEnabledStatus( bool flag = true )
        DNB_THROW_SPEC_NULL;

    /**
     * Returns the status of all the proclets (true for enabled).
     */
    static bool
    getEnabledStatus()
        DNB_THROW_SPEC_NULL;

    /**
     * Sets the Continue/Pause option for the hyperlink activities
     * (true for Continue).
     */
    static void
    setHlnkBehavior( bool flag = true )
        DNB_THROW_SPEC_NULL;

    /**
     * Sets the Process/Parent option for the text activities
     * (true for Process).
     */
    static void
    setTextBehavior( bool flag = true )
        DNB_THROW_SPEC_NULL;

    /**
     * Sets the Process/Parent option for the annotation activities
     * (true for Process).
     */
    static void
    setAnnotBehavior( bool flag = true )
        DNB_THROW_SPEC_NULL;

    /**
     * Sets the Process/Parent option for the visibility activities
     * (true for Process).
     */
    static void
    setVisBehavior( bool flag = true )
        DNB_THROW_SPEC_NULL;

    /**
     * Returns the Behavior option.
     */
    static int
    getBehavior()
        DNB_THROW_SPEC_NULL;

    /**
     * Sets the Behavior option.
     */
    static void
    setBehavior( int iBehavior = 0 )
        DNB_THROW_SPEC_NULL;

//  typedef RWRecursiveLock<RWMutexLock>    DNBEnableLock;
	typedef DNBSysFastMutexLock				DNBEnableLock;

protected:
    void                   *_model_id;
    DNBBasicEntity::Handle _hObj;

    static bool            _procletEnabled;
    static DNBEnableLock   _enableLock;

    static int             _behaviorAct;
    static DNBEnableLock   _behaviorLock;
};

//------------------------------------------------------------------------------

#endif //_DNB_BASIC_PROCLET_H
