//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/** @CAA2Required */
/*---------------------------------------------------------------------*/
/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */
/*---------------------------------------------------------------------*/
//
//    This Header file is included by all the Module.h header files in 
//    PublicInterfaces
//

#ifndef DNBSIMULATIONBASE_H

#define DNBSIMULATIONBASE_H DNBSimulationBase

#ifdef _WINDOWS_SOURCE
#if defined(__DNBSimulationBase)
#define ExportedByDNBSimulationBase __declspec(dllexport)
#else
#define ExportedByDNBSimulationBase __declspec(dllimport)
#endif
#else
#define ExportedByDNBSimulationBase
#endif

#endif /* DNBSIMULATIONBASE_H */
