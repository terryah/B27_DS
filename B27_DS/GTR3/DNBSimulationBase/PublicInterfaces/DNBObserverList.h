/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBObserverList.h
//   This class implements a WDM extension of a DNBBasicEntity object.
//
//==============================================================================
//
// Usage notes: 
//   This class is used to store the buffered obesrvers to be send to the main
//   thread.
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     mmg          01/01/1999   Initial implementation
//     mmh          08/07/2002   Add methods: remove_from_observerlist
//     mmg          09/03/2002   removed method: signal_buffer_applied
//     mmg          08/04/2003   changed single doublebuffered observerlist into
//                               3 doublebuffered observerlists to support
//                               lifecycle observations
//     mmg          02/25/2004   added capability to manage synchronous updates
//     sha          05/17/2004   replace list with map (performance enhancement)
//
//==============================================================================
#if DNB_HAS_PRAGMA_ONCE
#pragma once
#endif
 
#ifndef _DNB_OBSERVERLIST_H
#define _DNB_OBSERVERLIST_H
 
#include <DNBSystemBase.h>
//#include <rw/thr/mutex.h>
//#include <rw/thr/condtion.h>
#include <DNBSysMutexLock.h>
#include <DNBSysCondition.h>
#include <DNBSysLockGuard.h>
//#include <rw/thr/func0.h>
#include <DNBSystemDefs.h>
#include <DNBException.h>
#include <DNBExtensionObject.h>
#include <DNBBasicEntity.h>
#include <DNBBasicEntity3D.h>
#include <DNBObserverBase.h>

#include <DNBSimulationBase.h>

//------------------------------------------------------------------------------

/**
 * This class implements a WDM extension of a DNBBasicEntity object.
 */
class ExportedByDNBSimulationBase DNBObserverList : public DNBExtensionObject
{
    DNB_DECLARE_DYNAMIC_CREATE( DNBObserverList );

    DNB_DECLARE_SHARED_OBJECT( DNBObserverList );
 
    DNB_DECLARE_EXTENSION_OBJECT( DNBObserverList, DNBBasicEntity );

    DNB_DECLARE_EXTENSION_FACTORY( DNBObserverList, DNBBasicEntity );

public:

    /**
     * Returns the read buffer.
     */
    int 
    read_buffer()
        DNB_THROW_SPEC_NULL;

    /**
     * Returns the write buffer.
     */
    int 
    write_buffer()
        DNB_THROW_SPEC_NULL;

    /**
     * Applies changes buffers into observers.
     */
    virtual void 
    apply_changes()
        DNB_THROW_SPEC_NULL;

    /**
     * Buffers the changes into observers.
     */
    virtual void 
    buffer_changes()
        DNB_THROW_SPEC_NULL;

    /**
     * Swaps the read/write buffers.
     */
    virtual bool 
    swap_buffers()
        DNB_THROW_SPEC_NULL;

    /**
     * Wait logic on the applied buffer.
     */
    virtual void 
    wait_on_buffer_applied()
        DNB_THROW_SPEC_NULL;

    /**
     * Wait logic on the processed buffer.
     */
    virtual void 
    wait_on_buffer_processed()
        DNB_THROW_SPEC_NULL;

    /**
     * Returns the status of the buffer.
     */
    virtual int 
    buffer_applied()
        DNB_THROW_SPEC_NULL;

    /**
     * Returns the status of the buffer.
     */
    virtual int 
    buffer_processed()
        DNB_THROW_SPEC_NULL;

    /**
     * Signals that the buffer has been processed.
     */
    virtual void
    signal_buffer_processed()
        DNB_THROW_SPEC_NULL;

    /**
     * Flushes the processors.
     */
    virtual void
    flush_processors()
        DNB_THROW_SPEC_NULL;

    /**
     * Increments the number of processors.
     */
    virtual void
    increment_num_processors()
        DNB_THROW_SPEC_NULL;

    /**
     * Decrements the number of processors.
     */
    virtual void
    decrement_num_processors()
        DNB_THROW_SPEC_NULL;

    /**
     * Sets the number of processors.
     */
    virtual void
    set_num_processors( int )
        DNB_THROW_SPEC_NULL;

    /**
     * Adds the given observer to the list (by Handle).
     */
    void 
    add_to_observerlist( DNBObserverBase::Handle )
        DNB_THROW_SPEC_NULL;

    /**
     * Adds the given observer to the list (by Pointer).
     */
    void 
    add_to_observerlist( DNBObserverBase * )
        DNB_THROW_SPEC_NULL;

    /**
     * Removes the given observer from the list (by Handle).
     */
    void 
    remove_from_observerlist( DNBObserverBase::Handle )
        DNB_THROW_SPEC_NULL;

    /**
     * Removes the given observer from the list (by Pointer).
     */
    void 
    remove_from_observerlist( DNBObserverBase * )
        DNB_THROW_SPEC_NULL;

    /**
     * Clears up the list of observers.
     */
    void
    clear_observerlist( int buffer, bool reset = false )
        DNB_THROW_SPEC_NULL;

    void
    request_swap()
        DNB_THROW_SPEC_NULL;

    bool
    swap_requested()
        DNB_THROW_SPEC_NULL;

    void
    request_sync()
        DNB_THROW_SPEC_NULL;

    bool
    sync_requested()
        DNB_THROW_SPEC_NULL;

    bool
    do_not_redraw_requested()
        DNB_THROW_SPEC_NULL;

    void
    request_swap( bool do_not_redraw )
        DNB_THROW_SPEC_NULL;

protected:
    //  By defining the following methods in the protected section, application
    //  programmers must use the static factory methods to create and destroy
    //  instances of this class.
    // ------------------------------------------------------------------------
    DNBObserverList()
        DNB_THROW_SPEC((DNBEResourceLimit));

    DNBObserverList( const DNBObserverList &right, CopyMode mode )
        DNB_THROW_SPEC((DNBEResourceLimit));
 
    virtual
    ~DNBObserverList( )
        DNB_THROW_SPEC_NULL;
 
private:
    typedef scl_map <DNBObserverBase::Handle,int,scl_less<DNBObserverBase::Handle> > ObjectIndexMap;
    typedef scl_map <int,DNBObserverBase::Handle,scl_less<int> > IndexObjectMap;

    // _obs_idx_map[0][x] is map of create entity observers
    // _obs_idx_map[1][x] is map of modify entity observers
    // _obs_idx_map[2][x] is map of destroy entity observers
    ObjectIndexMap          _obs_idx_map[3][2];
    unsigned long           _index;

    int                     _write_buffer;
    int                     _read_buffer;
    bool                    _do_swap;
	bool                    _do_not_redraw;
    bool                    _synchronous;

    protected:

    int                     _buffer_applied;
    int                     _buffer_processed;

//  mutable RWMutexLock     _monitor;
	mutable DNBSysMutexLock _monitor;
//  RWCondition             _stall;
	DNBSysCondition			_stall;
    int                     _num_processors;
};

//------------------------------------------------------------------------------

#endif // _DNB_OBSERVERLIST_H
