/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBObserverBase.h
//   Observer base class.
//
//==============================================================================
//
// Usage notes: 
//   Used by the templated DNBObserver class. Do not derive from this class.
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     mmg          01/01/1999   Initial implementation
//     mmg          09/23/2002   added support for functor type
//     sha          11/01/2002   added support for selective model flush
//     mmg          12/20/2002   added has_functors method
//     mmg          01/17/2003   added set_flushable_state
//     mmg          02/17/2003   added support for adaptive flush to model
//     mmg          08/04/2003   support lifecycle observation
//     mmh          07/27/2004   add method is_flushable() - used when cleaning
//                               the observerlist with reset
//
//==============================================================================
#if DNB_HAS_PRAGMA_ONCE
#pragma once
#endif
 
#ifndef _DNB_OBSERVERBASE_H
#define _DNB_OBSERVERBASE_H
 
#include <DNBSystemBase.h>
//#include <rw/thr/funcr0.h>
#include <scl_map.h>
#include <DNBSystemDefs.h>
#include <DNBBasicEntity.h>
#include <DNBExtensionObject.h>
#include <DNBAttributeExt.h>

#include <DNBSimulationBase.h>

class DNBObserverList;

//------------------------------------------------------------------------------

/**
 * Observer base class.
 */
class ExportedByDNBSimulationBase DNBObserverBase : public DNBExtensionObject
{
    DNB_DECLARE_DYNAMIC_RTTI( DNBObserverBase );

    DNB_DECLARE_SHARED_OBJECT( DNBObserverBase );
 
    DNB_DECLARE_EXTENSION_OBJECT( DNBObserverBase, DNBBasicEntity );

public:

    //
    //  Disabled - do not collect data and do not add to observerlist
    //  SwapDisabled - collect the data but do not add to observerlist
    //  Enabled - collect data and add to observerlist
    //
    enum EnableState{ Disabled, SwapDisabled, Enabled };

    //
    //  Apply - call the functor during the apply phase on the main thread
    //  Collect - call the functor immediately after the collect call on the
    //            sim thread
    //
    enum FunctorType{ Apply, Collect };

    //typedef RWFunctorR0<int> CheckFunctor;
    typedef DNBFunctor0wRet<int> CheckFunctor;

    /**
     * Adds itself to the observer list.
     */
    bool 
    add_to_observerlist( bool override )
        DNB_THROW_SPEC_NULL;

    /**
     * Returns the state of the observer.
     */
    EnableState
    get_enable_state()
        DNB_THROW_SPEC_NULL;

    /**
     * Sets the state of the observer.
     */
    void
    set_enable_state( EnableState state )
        DNB_THROW_SPEC_NULL;

    /**
     * Applies the buffered changes.
     */
    virtual void 
    apply_changes() 
        DNB_THROW_SPEC_NULL;

    /**
     * Buffers the changes into observer.
     */
    virtual bool 
    buffer_changes() 
        DNB_THROW_SPEC_NULL;

    /**
     * Returns the current status of the observer.
     */
    bool 
    updated() 
        DNB_THROW_SPEC_NULL;

    /**
     * Returns whether any functors have been added to the observer
     */
    bool 
    has_functors() 
        DNB_THROW_SPEC_NULL;

    bool
    has_functor( scl_wstring key )
        DNB_THROW_SPEC_NULL;

    void
    enable_functor( scl_wstring key, bool state )
        DNB_THROW_SPEC_NULL;

    bool
    functor_enabled( scl_wstring key )
        DNB_THROW_SPEC_NULL;


    DNBBasicEntity::LifeCycle
    get_lifecycle()
        DNB_THROW_SPEC_NULL;

    /**
     * set FlushModel attribute
     */
    void 
    reset_flushable_mask() 
        DNB_THROW_SPEC_NULL;

    virtual void
    force_observation( bool collect = FALSE )
        DNB_THROW_SPEC_NULL;

    /**
     * Returns true if any flag has been set in the FlushModel attribute, 
     * otherwise returns false.
     */
    bool 
    is_flushable() 
        DNB_THROW_SPEC_NULL;

protected:
    //  By defining the following methods in the protected section, application
    //  programmers must use the static factory methods to create and destroy
    //  instances of this class.
    // ------------------------------------------------------------------------
    DNBObserverBase( DNBBasicEntity::LifeCycle lCyc = 
                                        DNBBasicEntity::EntityModified )
        DNB_THROW_SPEC((DNBEResourceLimit));

    DNBObserverBase( const DNBObserverBase &right, CopyMode mode )
        DNB_THROW_SPEC((DNBEResourceLimit));
 
    virtual
    ~DNBObserverBase( )
        DNB_THROW_SPEC_NULL;
 
protected:
    void
    swap_buffers()
        DNB_THROW_SPEC_NULL;

    void 
    add_functor_data( scl_wstring &key, FunctorType functype = Apply ) 
	DNB_THROW_SPEC_NULL;
    
    struct Basefunctor_data
    {
        Basefunctor_data()
            :enabled(true), functype(Apply){}

        bool
        operator==( const Basefunctor_data &rhs )
            DNB_THROW_SPEC_NULL
        { return this == &rhs; }
        
        bool
        operator<( const Basefunctor_data &rhs )
            DNB_THROW_SPEC_NULL
        { return this < &rhs; }
        
        bool         enabled;
        FunctorType  functype;
    };

    typedef scl_map <scl_wstring,Basefunctor_data,scl_less<scl_wstring> > BaseFunctorMap;
    typedef BaseFunctorMap::iterator BaseFunctorIter;
    typedef BaseFunctorMap::const_iterator BaseFunctorConstIter;

    BaseFunctorMap            _basefunctormap;
    EnableState               _enable_state;
    bool                      _updated[2];
    int                       _write_buffer;
    int                       _read_buffer;
    bool                      _swap;
    EventSet                  _events[2];
    EventSet                  _flushEvents;

private:
    DNBObserverList          *_obslist;
    bool                      _inlist[2];
    DNBBasicEntity::LifeCycle _lCyc;
    
    friend class DNBObserverList;
};

//------------------------------------------------------------------------------

#endif // _DNB_OBSERVERBASE_H
