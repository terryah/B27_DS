/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBStartFinishExt.h
//      WDM extension used to store the simulation status.
//
//==============================================================================
//
// Usage notes: 
//      Implements a WDM extension used to remember if the simulation is started
//      or finished for visualization purposes.
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     mmh          06/06/2001   Initial implementation
//
//==============================================================================
#if DNB_HAS_PRAGMA_ONCE
#pragma once
#endif
 
#ifndef _DNB_STARTFINISHEXT_H
#define _DNB_STARTFINISHEXT_H
 
#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>
#include <DNBExtensionObject.h>
#include <DNBBasicEntity.h>

#include <DNBSimulationBase.h>

//------------------------------------------------------------------------------

/**
 * Class to store the simulation status (started or not) for visualization.
 * <b>Role</b>: Implements a WDM extension to a DNBBasicEntity.
 */
class ExportedByDNBSimulationBase DNBStartFinishExt : public DNBExtensionObject
{
    DNB_DECLARE_DYNAMIC_CREATE( DNBStartFinishExt );

    DNB_DECLARE_SHARED_OBJECT( DNBStartFinishExt );
 
    DNB_DECLARE_EXTENSION_OBJECT( DNBStartFinishExt, DNBBasicEntity );

    DNB_DECLARE_EXTENSION_FACTORY( DNBStartFinishExt, DNBBasicEntity );

public:
    enum Events
    {
        EventFirst = DNBExtensionObject::EventLast - 1,
        EventModifyStatus,       // Status modified 
        EventLast,
        EventSimReset
    };

    /**
     * Called when the simulation is started.
     */
    void startSim()
        DNB_THROW_SPEC_NULL;
    /**
     * Called when the simulation is finished.
     */
    void finishSim()
        DNB_THROW_SPEC_NULL;
    /**
     * Returns the status of the simulation.
     */
    int getStatus()
        DNB_THROW_SPEC_NULL;
    /**
     * Called when the simulation is Reset.
     */
    void  resetSim()
        DNB_THROW_SPEC_NULL;

protected:
    //  By defining the following methods in the protected section, application
    //  programmers must use the static factory methods to create and destroy
    //  instances of this class.
    // ------------------------------------------------------------------------
    /**
     * Constructs a DNBStartFinishExt object.
     */
    DNBStartFinishExt() 
        DNB_THROW_SPEC((DNBEResourceLimit));

    /**
     * Copy constructor.
     */
    DNBStartFinishExt( const DNBStartFinishExt &right, CopyMode mode )
        DNB_THROW_SPEC((DNBEResourceLimit));
 
    virtual
    ~DNBStartFinishExt( )
        DNB_THROW_SPEC_NULL;

private:
    int     _started;
};

//------------------------------------------------------------------------------

#endif // _DNB_STARTFINISHEXT_H
