/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBEItemInitStateContAdapter.h
//    Adapter for DNBIItemPersistentInitState extension
//
//==============================================================================
//
// Usage notes: 
//   Derive your data extension from DNBEItemInitStateContAdapter and use the 
//   default implementation for all of the methods provided by the interface.
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     sha          04/2001      Initial implementation
//     mmg          12/20/2002   derive from DNBEItemInitStateAdapter
//
//==============================================================================
#ifndef DNBEItemInitStateContAdapter_H
#define DNBEItemInitStateContAdapter_H

#include <DNBSimulationBase.h>
#include <CATBaseUnknown.h>
#include <CATBooleanDef.h>
#include <DNBEItemInitStateAdapter.h>
#include <DNBIItemPersistentInitState.h>

class CATISpecObject;

//------------------------------------------------------------------------------

/**
* Class Object modeler implementation class.
* <br>
* Default implementation (adapter) for:
*  <ol>
*  <li>@see DNBSimulationInterfaces.DNBIItemPersistentInitState
*  </ol>
* Using this prefered syntax will enable mkdoc to document your class.
*/
class ExportedByDNBSimulationBase DNBEItemInitStateContAdapter: public DNBEItemInitStateAdapter
{
    CATDeclareClass;
    
public:
    
    // Standard constructors and destructors for an implementation class
    // -----------------------------------------------------------------
    DNBEItemInitStateContAdapter ();
    virtual ~DNBEItemInitStateContAdapter ();
    
    /**
    * Default implementation for:
    * @see DNBSimulationInterfaces.DNBIItemPersistentInitState#SaveInitState
    */
    virtual void SaveInitState( const boolean refresh,
                                const int mask );
    
    /**
    * Default implementation for:
    * @see DNBSimulationInterfaces.DNBIItemPersistentInitState#RestoreInitState
    */
    virtual void RestoreInitState( const int mask,
                                   const DNBRestoreDestination where );
    
    /**
    * Default implementation for:
    * @see DNBSimulationInterfaces.DNBIItemPersistentInitState#HasInitState
    */
    virtual boolean HasInitState();
    
    /**
    * Default implementation for:
    * @see DNBSimulationInterfaces.DNBIItemPersistentInitState#InitializeInitState
    */
    virtual void InitializeInitState( CATISpecObject *obj );
    
private:
    // The copy constructor and the equal operator must not be implemented
    // -------------------------------------------------------------------
    DNBEItemInitStateContAdapter (DNBEItemInitStateContAdapter &);
    DNBEItemInitStateContAdapter& operator=(DNBEItemInitStateContAdapter&);
};

//------------------------------------------------------------------------------

#endif
