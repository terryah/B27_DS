/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBMoveEvent.h
//      The Move Event class. All application-specific simulation events
//      will be derived from this class.  
//      Subscribers subscribe to type of events.
//      Publishers publish instance of events. 
//      Event-Notifier notifies the subscribers of the events as and when 
//      they are  published.
//
//==============================================================================
//
// Usage notes: 
//      All Subscribers subscribe to type of events.
//      Publishers publish instance of events. 
//      Event-Notifier notifies the subscribers of the events as and when 
//      they are  published.
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     rtl          01/20/2001   Initial Development
//
//==============================================================================
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _DNB_DNBMoveEvent_H_
#define _DNB_DNBMoveEvent_H_

#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>

#include <DNBSimulationBase.h>
#include <DNBSimEvent.h>


class ExportedByDNBSimulationBase DNBMoveEvent : public DNBSimEvent
{
    
    DNB_DECLARE_SIMEVENT( DNBMoveEvent);
public:
    
    DNBMoveEvent()
        DNB_THROW_SPEC_NULL;
    
    /**
     *Copy constructor. 
     *All derived classes will have to provide a copy constructor.
     */
    DNBMoveEvent( const DNBMoveEvent &right, CopyMode mode )
        DNB_THROW_SPEC((DNBEResourceLimit));
    
    ~DNBMoveEvent()
        DNB_THROW_SPEC_NULL;
    
};

#endif //_INCLUDE_DNB_DNBMoveEvent_H_

