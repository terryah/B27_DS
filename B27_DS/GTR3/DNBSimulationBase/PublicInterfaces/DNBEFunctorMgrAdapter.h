/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBEFunctorMgrAdapter.h
//      Adapter for the DNBIFunctorMgr extension.
//
//==============================================================================
//
// Usage notes: 
//      Base class for the DNBIFunctorMgr implementation. Do use CodeExtension
//  when doing a BOA extension.
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     mmh          03/11/2002   Initial implementation
//     mmh          05/09/2002   Add methods for recorders
//     mmg          10/10/2002   added method to log entity movement
//     awn          03/13/2003   BOA modification
//     mmh          05/01/2003   Add methods to get/reset functor mask
//     mmh          10/05/2004   Provide all methods from interface
//
//==============================================================================

#ifndef DNB_E_FUNCTORMGRADAPTER_H
#define DNB_E_FUNCTORMGRADAPTER_H

#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>

#include <DNBNotificationAgent.h>
#include <DNBBasicEntity.h>
// #include <DNBLocObserver.h>      // smw; caa 
class DNBObserveLocation;           // smw; caa
#include <DNBAttrObserver.h>
// #include <DNBStartFinishObserver.h>  // smw; caa; not used?
// #include <DNBCameraObserver.h>   // smw; caa
class DNBObserveCamera;             // smw; caa
//#include <DNBMessageObserver.h>   // smw; caa
class DNBObserveMessage;            // smw; caa
//#include <DNBMessageRecorder.h>   // smw; caa
class DNBRecordMessage;             // smw; caa
#include <DNBCATBase.h>
#include <DNBIFunctorMgr.h>
#include <DNBIWDMExt.h>
#include <CATBooleanDef.h>
#include <DNBCATDefs.h>

#include <DNBSimulationBase.h>

class CATFrmLayout;
class CATFrmEditor;
class CATFrmWindow;
class CATIASiExWReplay;
class DNBMsgDlg;

//------------------------------------------------------------------------------

/**
 * Adapter for the DNBIFunctorMgr extension.
 */
class ExportedByDNBSimulationBase DNBEFunctorMgrAdapter : public DNBIFunctorMgr
{
    CATDeclareClass;

public:
    /**
     * Constructs a DNBEFunctorMgrAdapter object.
     */
    DNBEFunctorMgrAdapter();
    /**
     * Destructs a DNBEFunctorMgrAdapter object.
     */
    virtual ~DNBEFunctorMgrAdapter();
    /**
     * Gets the mask corresponding to the functors that need to be added, based 
     * on the given mask.
     * @param iMask
     *   Mask corresponding to the functors that the client needs.
     * @param oMask
     *   Mask corresponding to the functors that are not created.
     */
    virtual HRESULT
    GetFunctorMask( unsigned int iMask, unsigned int& oMask );
    /**
     * Resets the functor mask.
     */
    virtual HRESULT
    ResetFunctorMask();
    /**
     * Binds the functors specified in the mask to the given object.
     */
    virtual HRESULT
    BindFunctors( unsigned int iMask, const CATBaseUnknown_var &iObj );
    /**
     * Enables/disables the observers/recorders specified in the mask for the 
     * given object.
     */
    virtual HRESULT
    SetEnableState( unsigned int iMask, const CATBaseUnknown_var &iObj,
                    int iState );
    /**
     * Forces observation for the observers/recorders specified in the mask for 
     * the given object.
     */
    virtual HRESULT
    ForceObservation( unsigned int iMask, const CATBaseUnknown_var &iObj );

protected:
    /**
     * Applies the position to the bagrep or to the model.
     */
    virtual void DNBApplyPositionObs( DNBNotificationAgent::EventSet &, 
                    DNBObserveLocation & );

    /**
     * Applies visibility and color to the bagrep or to the model.
     */
    virtual void DNBApplyVisibilityObs( DNBNotificationAgent::EventSet &, 
                    DNBObserveVisuAttribs & );

    /**
     * Applies the viewpoint changes.
     */
    virtual void DNBApplyViewpointObs( DNBNotificationAgent::EventSet &, 
                    DNBObserveCamera &, CATFrmWindow * );

    /**
     * Displays information in a text dialog.
     */
    virtual void DNBApplyTextObs( DNBNotificationAgent::EventSet &, 
                    DNBObserveMessage &, DNBMsgDlg * );

    /**
     * 
     */
    virtual void DNBApplyPositionRec( DNBNotificationAgent::EventSet &, 
                    DNBObserveLocation &, CATIASiExWReplay * );

    /**
     * 
     */
    virtual void DNBApplyVisibilityRec( DNBNotificationAgent::EventSet &, 
                    DNBObserveVisuAttribs &, CATIASiExWReplay * );

    /**
     * 
     */
    virtual void DNBApplyColorRec( DNBNotificationAgent::EventSet &, 
                    DNBObserveVisuAttribs &, CATIASiExWReplay * );

    /**
     * 
     */
    virtual void DNBApplyOpacityRec( DNBNotificationAgent::EventSet &, 
                    DNBObserveVisuAttribs &, CATIASiExWReplay * );

    /**
     * 
     */
    virtual void DNBApplyViewpointRec( DNBNotificationAgent::EventSet &, 
                    DNBObserveCamera &, CATIASiExWReplay * );

    /**
     * 
     */
    virtual void DNBApplyTextRec( DNBNotificationAgent::EventSet &, 
                    DNBRecordMessage &, CATIASiExWReplay *, scl_wstring );
    /**
     * 
     */
    virtual void DNBLogMovement( DNBNotificationAgent::EventSet &, 
                    DNBObserveLocation & );

    unsigned int            _fMask;
    unsigned int            _supportedMask;
};

//------------------------------------------------------------------------------

#endif //DNB_E_FUNCTORMGRADAPTER_H
