/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2006
//==============================================================================
//
// DNBSimEvent.h
//      The Move Event class. All application-specific simulation events
//      will be derived from this class.  
//      Subscribers subscribe to type of events.
//      Publishers publish instance of events. 
//      Event-Notifier notifies the subscribers of the events as and when 
//      they are  published.
//
//==============================================================================
//
// Usage notes: 
//      All Subscribers subscribe to type of events.
//      Publishers publish instance of events. 
//      Event-Notifier notifies the subscribers of the events as and when 
//      they are  published.
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     rtl          01/10/2006   Initial implementation
//     rtl          06/12/2006   Fix for IR A0537077. Force observation of the event
//
//==============================================================================
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _DNB_DNBSimEvent_H_
#define _DNB_DNBSimEvent_H_

#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>

#include <DNBSimulationBase.h>
#include <DNBBasicSimEvent.h>

#include <DNBTimelet.h>
#include <CATModelIdentificator.h>
#include <CATUnicodeString.h>



class ExportedByDNBSimulationBase DNBSimEvent : public DNBBasicSimEvent
{
    
    DNB_DECLARE_SIMEVENT( DNBSimEvent);
public:
    
    DNBSimEvent()
        DNB_THROW_SPEC_NULL;
    
    /**
     *Copy constructor. 
     *All derived classes will have to provide a copy constructor.
     */
    DNBSimEvent( const DNBSimEvent &right, CopyMode mode )
        DNB_THROW_SPEC((DNBEResourceLimit));
    
    ~DNBSimEvent()
        DNB_THROW_SPEC_NULL;

   virtual void
   setStartTime(DNBTimelet::Time startTime)
   DNB_THROW_SPEC_NULL;

   virtual DNBTimelet::Time
   getStartTime()
   DNB_THROW_SPEC_NULL;

   virtual void
   getStartTime(DNBTimelet::Time &oStartTime)
   DNB_THROW_SPEC_NULL;

   virtual void
   setEndTime(DNBTimelet::Time endTime)
   DNB_THROW_SPEC_NULL;

   virtual DNBTimelet::Time
   getEndTime()
   DNB_THROW_SPEC_NULL;

   virtual void
   getEndTime(DNBTimelet::Time &oEndTime)
   DNB_THROW_SPEC_NULL;

   virtual void
   setModelId(CATModelIdentificator& modelId)
   DNB_THROW_SPEC_NULL;

   virtual CATModelIdentificator
   getModelId()
   DNB_THROW_SPEC_NULL;

   virtual void
   getModelId(CATModelIdentificator &oModelId)
   DNB_THROW_SPEC_NULL;

   virtual void
   setName(const CATUnicodeString& name)
   DNB_THROW_SPEC_NULL;

   virtual CATUnicodeString
   getName()
   DNB_THROW_SPEC_NULL;

   virtual void
   getName(CATUnicodeString &oName)
   DNB_THROW_SPEC_NULL;

   virtual void
   setForceMode( bool forceMode )
   DNB_THROW_SPEC_NULL;

   virtual bool
   getForceMode()
   DNB_THROW_SPEC_NULL;

   virtual void
   publish()
   DNB_THROW_SPEC_NULL;


protected:

    DNBTimelet::Time			_StartTime;
    DNBTimelet::Time			_EndTime;
    CATModelIdentificator       _ModelId;
    CATUnicodeString            _Name;
	bool                        _force;
    
};

#endif //_INCLUDE_DNB_DNBSimEvent_H_

