/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBObserver.h
//   Templated class for all observers.
//
//==============================================================================
//
// Usage notes: 
//   Use this class to create new observers, by implementing the observer
//   adapter and than specialize this template with your new adapter.
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     mmg          01/01/1999   Initial implementation
//     mmg          09/23/2002   added functor type info to add_functor method
//     mmg          02/17/2003   added support for adaptive flush to model
//     mmg          03/21/2003   added argument to apply event and changed
//                               DEFAULT_APPLY_EVENT macro
//     mmg          09/05/2003   fixed to manage clone correctly
//
//==============================================================================
#define HAS_DNB_FUNCTOR 1

#if DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _DNB_OBSERVER_H
#define _DNB_OBSERVER_H

#include <DNBSystemBase.h>

#include <scl_map.h>
#include <DNBSystemDefs.h>

#include <DNBNotificationAgent.h>
#include <DNBExtensionObject.h>
#include <DNBBasicEntity.h>
#include <DNBObserverBase.h>

#include <DNBSimulationBase.h>

//------------------------------------------------------------------------------
/**
 * Templated class for all observers.
 */
template < class Adaptor, class Obs >
class DNBObserver : public DNBObserverBase
{
    typedef DNBObserver<Adaptor, Obs> _DNBObservertype;

    DNB_DECLARE_DYNAMIC_RTTI( _DNBObservertype );

    DNB_DECLARE_SHARED_OBJECT_TMPL( _DNBObservertype );

    DNB_DECLARE_EXTENSION_OBJECT_TMPL( _DNBObservertype, DNBBasicEntity );


#if 0
    DNB_DECLARE_EXTENSION_FACTORY( _DNBObservertype, DNBBasicEntity );
#endif

public:

    typedef void (*signature)( EventSet &, Adaptor & );

    typedef DNBFunctor2< EventSet &, Adaptor & > Functor;

    /**
     * Static factory method to create a DNBObserver instance.
     */
    static typename DNBObserver<Adaptor,Obs>::Handle
    create( const DNBSharedHandle<Obs> hObj, 
            DNBBasicEntity::LifeCycle lCyc = DNBBasicEntity::EntityModified )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit));

    /**
     * Adds a new functor to the list.
     * Apply type will be called on main thread during apply phase
     * Collect type will be called on sim thread after collect phase
     */
    void
    add_functor( scl_wstring key, Functor func, FunctorType functype = Apply )
        DNB_THROW_SPEC_NULL;

    /**
     * Removes the given functor (by pointer).
     */
    void
    remove_functor( Functor func )
        DNB_THROW_SPEC_NULL;

    /**
     * Removes the given functor (by name).
     */
    void
    remove_functor( scl_wstring key )
        DNB_THROW_SPEC_NULL;

    /**
     * Clears up the entire list of functors.
     */
    void
    remove_functors()
        DNB_THROW_SPEC_NULL;

    /**
     * Forces an observation with the given event set.
     */
    void
    force_observation( const EventSet &, bool collect = FALSE )
        DNB_THROW_SPEC_NULL;

    /**
     * Forces an observation with the buffered event set;
     */
    void
    force_observation( bool collect = FALSE )
        DNB_THROW_SPEC_NULL;

    /**
     * Applies the changes stored in the observer.
     */
    void
    apply_changes()
        DNB_THROW_SPEC_NULL;

    /**
     * Buffer changes to the observer.
     */
    bool
    buffer_changes()
        DNB_THROW_SPEC_NULL;

protected:
    //  By defining the following methods in the protected section, application
    //  programmers must use the static factory methods to create and destroy
    //  instances of this class.
    // ------------------------------------------------------------------------
    DNBObserver( DNBSharedHandle<Obs>, 
            DNBBasicEntity::LifeCycle lCyc = DNBBasicEntity::EntityModified )
        DNB_THROW_SPEC((DNBEResourceLimit));

    DNBObserver( const DNBObserver<Adaptor,Obs> &, CopyMode, 
                                        DNBNotificationAgent::Handle )
        DNB_THROW_SPEC((DNBEResourceLimit));

    virtual
    ~DNBObserver()
        DNB_THROW_SPEC_NULL;

    /**
     * implementation of duplicateObject which takes the handle to the new
     * object which will have this extension bound to it 
     */
    virtual DNBDynamicObject *
    duplicateObject( CopyMode mode, DNBNotificationAgent::Handle hBasic ) const
        DNB_THROW_SPEC((scl_bad_alloc, DNBException));

private:

    void
    on_event( const DNBNotificationAgent::ConstPointer &, const EventSet & )
        DNB_THROW_SPEC_NULL;

    void
    subscribe()
        DNB_THROW_SPEC_NULL;

    void
    unsubscribe()
        DNB_THROW_SPEC_NULL;

    Adaptor              _data[2];

    typedef scl_map < scl_wstring, Functor,scl_less< scl_wstring > > FunctorMap;
    typedef typename FunctorMap::iterator FunctorIter;
    typedef typename FunctorMap::const_iterator FunctorConstIter;

    FunctorMap          _functormap;

    DNBSharedHandle<Obs> _object;

};

//------------------------------------------------------------------------------

#include "DNBObserver.cc"

#define DNB_DECLARE_OBSERVER_ADAPTOR(_HANDLE)                       \
public:                                                             \
    void                                                            \
    init( DNBSharedHandle<_HANDLE> )                                \
	    DNB_THROW_SPEC_NULL;                                        \
                                                                    \
    DNBNotificationAgent::EventSet                                  \
    mask( DNBSharedHandle<_HANDLE> )                                \
	    DNB_THROW_SPEC_NULL;                                        \
                                                                    \
    DNBNotificationAgent::Handle                                    \
    publisher( DNBSharedHandle<_HANDLE> )                           \
	    DNB_THROW_SPEC_NULL;                                        \
                                                                    \
    bool                                                            \
    apply_event( DNBNotificationAgent::EventSet &,                  \
		 const DNBNotificationAgent::EventSet &,                    \
         DNBSharedHandle<_HANDLE>,                                  \
         bool flush )                                               \
	    DNB_THROW_SPEC_NULL;                                        \
                                                                    \
    void                                                            \
    collect( DNBSharedHandle<_HANDLE> )                             \
	    DNB_THROW_SPEC_NULL

#define DNB_DEFINE_DEFAULT_OBSERVER_PUBLISHER(_CLASS,_HANDLE)       \
    DNBNotificationAgent::Handle                                    \
    _CLASS::publisher( DNBSharedHandle<_HANDLE> hEnt )              \
	    DNB_THROW_SPEC_NULL                                         \
    {                                                               \
        return( hEnt );                                             \
    }
								
#define DNB_DEFINE_DEFAULT_OBSERVER_APPLY_EVENT(_CLASS,_HANDLE)     \
    bool                                                            \
    _CLASS::apply_event( DNBNotificationAgent::EventSet &current,   \
		   const DNBNotificationAgent::EventSet &event,             \
           DNBSharedHandle<_HANDLE>,                                \
           bool flush )                                             \
	    DNB_THROW_SPEC_NULL                                         \
    {                                                               \
        if( !flush )                                                \
            current |= event;                                       \
        return( FALSE );                                            \
    }

#define DNB_DEFINE_DEFAULT_OBSERVER_INIT(_CLASS,_HANDLE)            \
    void                                                            \
    _CLASS::init( DNBSharedHandle<_HANDLE> hEnt )                   \
	    DNB_THROW_SPEC_NULL                                         \
    {}				

#endif // _DNB_OBSERVER_H
