/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBWDMExtAdapters.h
//   This class implements several adapters for DNBIWDMExt extension.
//
//==============================================================================
//
// Usage notes: 
//   Use this adapters to implement DNBIWDMExt extensions:
//      - DNBWDMExtAdapter: generic adapter
//      - DNBWDMProductExtAdapter: product based adapter which implements the
//                                 selective build mechanism
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     sha          03/01/2001   Initial implementation
//     sha          04/01/2001   Modified GetHandle() method to take an extra
//                               argument (disableDescendingRecursion)
//     mmh          03/07/2002   Add method BuildHandle() to deal with functors.
//     mmh          08/14/2002   Add new AddFunctors() method to add functors 
//                               after the entity was built
//     awn          03/13/2003   BOA modification (do not use CodeExtension)
//
//==============================================================================
#ifndef DNB_WDMEXTADAPTERS_H
#define DNB_WDMEXTADAPTERS_H

#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>
#include <DNBBasicEntity.h>

#include <DNBCATBase.h>
#include <DNBIWDMExt.h>
#include <CATBooleanDef.h>
#include <DNBCATDefs.h>

#include <DNBSimulationBase.h>

//------------------------------------------------------------------------------

/**
 * Generic adapter for implementing DNBIWDMExt extensions.
 */
class ExportedByDNBSimulationBase DNBWDMExtAdapter : public DNBIWDMExt
{
    CATDeclareClass;

public:
    /**
     * Constructs a DNBWDMExtAdapter object.
     */
    DNBWDMExtAdapter();

    /**
     * Destructs a DNBWDMExtAdapter object.
     */
    virtual ~DNBWDMExtAdapter();
    
    /**
     * Returns the WDM handle for the extended V5 object.
     * @param force_build
     *   Not used.
     * @param father
     *   Not used.
     * @param disableDescendingRecursion
     *   Not used.
     */
    virtual DNBBasicEntity::Handle GetHandle( 
        boolean force_build = FALSE,
        const CATBaseUnknown_var &father = NULL_var,
        boolean disableDescendingRecursion = FALSE );

    /**
     * Returns the WDM handle for the extended V5 object.
     * @param iMask
     *   Not used.
     * @param father
     *   Not used.
     * @param disableDescendingRecursion
     *   Not used.
     */
    virtual DNBBasicEntity::Handle BuildHandle( 
        unsigned int iMask = 0,
        const CATBaseUnknown_var &father = NULL_var,
        boolean disableDescendingRecursion = FALSE );

    /**
     * Applies the correct functors based on the mask.
     * @param iMask
     *   Indicated the functors to be applied.
     * @param father
     *   The father of the current object.
     */
    virtual HRESULT AddFunctors( unsigned int iMask = 0,
        const CATBaseUnknown_var &father = NULL_var );

    /**
     * Sets the extended V5 object with the WDM handle.
     * @param handle
     *    WDM handle to be stored in the extension.
     */
    virtual void SetHandle( const DNBBasicEntity::Handle &handle );

    /**
     * Returns the build mode required for this extension.
     * <br>Legal values<b></b>:
     * <tt>TRUE</tt> when all descendents of the object have to be build
     * <tt>FALSE</tt> when not all descendents of the object are required to
     * be build
     */
    virtual boolean BuildBranch();

    /**
     * Returns the state of the object.
     */
    virtual boolean IsBuilt();
    
protected:
    DNBBasicEntity::Handle  _wdm_handle;
};

//------------------------------------------------------------------------------

/**
 * Product based adapter which implements the selective build mechanism for
 * implementing DNBIWDMExt extensions.
 */
class ExportedByDNBSimulationBase DNBWDMProductExtAdapter : 
    public DNBWDMExtAdapter
{
    CATDeclareClass;
    
public:
    /**
     * Constructs a DNBWDMProductExtAdapter object.
     */
    DNBWDMProductExtAdapter();

    /**
     * Destructs a DNBWDMProductExtAdapter object.
     */
    virtual ~DNBWDMProductExtAdapter();
    
    /**
     * Returns the WDM handle for the extended V5 object.
     * @param force_build
     *   <br>Legal values<b></b>:
     *   <tt>TRUE</tt> forces the object, all its ascendents and all its
     *                 descendents to be build (selective build protocol)
     *   <tt>FALSE</tt> just returns the stored handle
     * @param father
     *   Not used.
     * @param disableDescendingRecursion
     *   <br>Legal values<b></b>:
     *   <tt>TRUE</tt> disable the descending recursion, used by the replay
     *                 command only!
     *   <tt>FALSE</tt> no effect on the selective build protocol
     */
    virtual DNBBasicEntity::Handle GetHandle( 
        boolean force_build = FALSE,
        const CATBaseUnknown_var &father = NULL_var,
        boolean disableDescendingRecursion = FALSE );
    
    /**
     * Returns the WDM handle for the extended V5 object.
     * @param iMask
     *   Indicates which functor should be added for the built objects 
     * @param father
     *   Not used.
     * @param disableDescendingRecursion
     *   <br>Legal values<b></b>:
     *   <tt>TRUE</tt> disable the descending recursion, used by the replay
     *                 command only!
     *   <tt>FALSE</tt> no effect on the selective build protocol
     */
    virtual DNBBasicEntity::Handle BuildHandle( 
        unsigned int iMask = 0,
        const CATBaseUnknown_var &iFather = NULL_var,
        boolean disableDescendingRecursion = FALSE );
};

//------------------------------------------------------------------------------

#endif //DNB_WDMEXTADAPTERS_H
