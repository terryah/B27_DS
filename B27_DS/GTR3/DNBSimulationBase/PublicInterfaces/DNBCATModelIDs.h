/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBCATModelIDs.h
//      This class implements and an extension of a DNBBasicEntity.
//
//==============================================================================
//
// Usage notes: 
//      Use this class to store the V5 counterpart of a WDM object.
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     mmg          01/01/1999   Initial implementation
//     mmh          06/29/2001   Add resetHandle parameter in "create" method, 
//                               _resetHandle data member to be checked in 
//                               orphanMember - don't reset world's handle.
//
//==============================================================================
#if DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _DNB_CATMODELIDS_H
#define _DNB_CATMODELIDS_H

#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>
#include <DNBExtensionObject.h>
#include <DNBBasicEntity.h>

#include <DNBSimulationBase.h>

//------------------------------------------------------------------------------

/**
 * This class implements and an extension of a DNBBasicEntity.
 */
class ExportedByDNBSimulationBase DNBCATModelIDs : public DNBExtensionObject
{
    DNB_DECLARE_DYNAMIC_CREATE( DNBCATModelIDs );
    
    DNB_DECLARE_SHARED_OBJECT( DNBCATModelIDs );
    
    DNB_DECLARE_EXTENSION_OBJECT( DNBCATModelIDs, DNBBasicEntity );
    
    DNB_DECLARE_EXTENSION_FACTORY( DNBCATModelIDs, DNBBasicEntity );
    
public:
    /**
     * Static factory methods.
     */
    static DNBCATModelIDs::Handle
    create( const DNBBasicEntity::Handle hBasicObject, 
            void *persistent_id = NULL, void *transient_id = NULL,
            bool resetHandle = TRUE )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit));
    
    /**
     * Gets the persistent ID.
     */
    void *
    getPersistentID() const
        DNB_THROW_SPEC_NULL;
    
    /**
     * Gets the transient ID.
     */
    void *
    getTransientID() const
        DNB_THROW_SPEC_NULL;
    
    /**
     * Sets the persistent ID.
     */
    void 
    setPersistentID( void *id )
        DNB_THROW_SPEC_NULL;
    
    /**
     * Sets the transient ID.
     */
    void 
    setTransientID( void *id ) 
        DNB_THROW_SPEC_NULL;
    
protected:
    //  By defining the following methods in the protected section, application
    //  programmers must use the static factory methods to create and destroy
    //  instances of this class.
    // ------------------------------------------------------------------------
    DNBCATModelIDs( void *persistent_id = NULL, void *transient_id = NULL,
                    bool resetHandle = TRUE )
        DNB_THROW_SPEC((DNBEResourceLimit));
    
    DNBCATModelIDs( const DNBCATModelIDs &right, CopyMode mode )
        DNB_THROW_SPEC((DNBEResourceLimit));
    
    virtual
        ~DNBCATModelIDs( )
        DNB_THROW_SPEC_NULL;
    
private:
    
    void *_persistent_id;
    void *_transient_id;
    bool _resetHandle;
};

//------------------------------------------------------------------------------

#endif // _DNB_CATMODELIDS_H
