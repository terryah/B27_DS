/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBFunctorUtilities.h
//      Contains methods used for the management of the functors.
//
//==============================================================================
//
// Usage notes: 
//      These methods are used in some of the functors and commands to decide 
//      if the changes should be applied to the reps or to the model.
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     mmg          01/01/1999   Initial implementation
//     mmh          03/11/2002   Move to a different file.
//     sha          06/17/2002   Provide a default argument in DNBSetApplyToReps
//                               to force a check in DNBIAnalysisConfiguration
//     mmh          10/18/2002   Add method to determine if a simulation 
//                               command should be shared or exclusive
//     mmg          07/16/2004   DNBApplyToReps new returns an int
//
//==============================================================================
#if DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _DNB_FUNCTORUTILITIES_H
#define _DNB_FUNCTORUTILITIES_H

#include <DNBSimulationBase.h>
#include <CATCommand.h>

#define ApplyChangesToModel   0
#define ApplyChangesToReps    1
#define ApplyChangesToNothing 2
//------------------------------------------------------------------------------

/**
 * Returns the value of the flag which indicates if the changes should be 
 * applied to the bagrep or to the model.
 */
ExportedByDNBSimulationBase 
int
DNBApplyToReps();

//------------------------------------------------------------------------------

/**
 * Forces the changes to be applied to the bagrep.
 */
ExportedByDNBSimulationBase 
void
DNBSetApplyToReps( int analysisCheck = 0 );

/**
 * Forces the changes to be applied to nothing
 */
ExportedByDNBSimulationBase 
void
DNBSetApplyToNothing();
//------------------------------------------------------------------------------

/**
 * Forces the changes to be applied to the model.
 */
ExportedByDNBSimulationBase 
void
DNBResetApplyToReps();

//------------------------------------------------------------------------------
/**
 * Returns the value of the flag which indicates if the viewpoint was changed
 * from the simulation or by the user.
 */
ExportedByDNBSimulationBase 
bool
DNBSimViewpointModified();

//------------------------------------------------------------------------------

/**
 * Indicates that the viewpoint was changed from the simulation.
 */
ExportedByDNBSimulationBase 
void
DNBSetSimViewpointModified();

//------------------------------------------------------------------------------

/**
 * Indicates that the viewpoint was changed by the user.
 */
ExportedByDNBSimulationBase 
void
DNBResetSimViewpointModified();

//------------------------------------------------------------------------------

/**
 * Indicates if a simulation command should be shared or exclusive.
 */
ExportedByDNBSimulationBase 
CATCommandMode
DNBGetSimCommandMode();

//------------------------------------------------------------------------------

#endif // _DNB_FUNCTORUTILITIES_H
