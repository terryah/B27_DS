/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBUtilities.h
//      WDM utility functions.
//
//==============================================================================
//
// Usage notes: 
//      WDM utilities used to get the logical descendents of the given object.
//      Based on the needs, one can get a list and/or a vector of descendents.
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     mmg          01/01/1999   Initial implementation
//     mmh          08/28/2001   Added getSpatialDescendents methods.
//     sha          11/19/2002   Added dumpSpatialChildren()/dumpProductTree()
//     mmg          12/11/2002   Added flushWorldSpatialState() method
//     mmg          02/17/2003   Added flushObservers()
//     rtl          05/16/2005   Added lessthan operator ( operator < )
//                               for smart pointers
//
//==============================================================================
#ifndef DNBUTILITIES_H
#define DNBUTILITIES_H

#include <DNBSystemBase.h>
#include <scl_list.h>
#include <DNBSystemDefs.h>
#include <DNBBasicEntity.h>
#include <DNBBasicWorld.h>
#include <DNBBasicEntity3D.h>
#include <CATBooleanDef.h>

#include <DNBSimulationBase.h>

/**
 * Returns a list of descendents for the given object.
 */
ExportedByDNBSimulationBase void
getLogicalDescendents( DNBBasicEntity::Handle, DNBBasicEntity::List & )
    DNB_THROW_SPEC_NULL;

/**
 * Returns a vector of descendents for the given object.
 */
ExportedByDNBSimulationBase void
getLogicalDescendents( DNBBasicEntity::Handle, DNBBasicEntity::Vector & )
    DNB_THROW_SPEC_NULL;

/**
 * Returns a list of spatial descendents for the given object.
 */
ExportedByDNBSimulationBase void
getSpatialDescendents( DNBBasicEntity3D::Handle, DNBBasicEntity3D::List & )
    DNB_THROW_SPEC_NULL;

/**
 * Returns a vector of spatial descendents for the given object.
 */
ExportedByDNBSimulationBase void
getSpatialDescendents( DNBBasicEntity3D::Handle, DNBBasicEntity3D::Vector & )
    DNB_THROW_SPEC_NULL;

/**
 * Dumps all spatial descendents for the given object (recursivly).
 */
ExportedByDNBSimulationBase void
dumpSpatialChildren( DNBBasicEntity3D::Handle )
    DNB_THROW_SPEC_NULL;

/**
 * Dumps the logical children for the given object and all spatial descendents
 * of the world's root assembly.
 */
ExportedByDNBSimulationBase void
dumpProductTree( DNBBasicEntity::Handle )
    DNB_THROW_SPEC_NULL;

/**
 * Propagates full xforms for the entire world
 */
ExportedByDNBSimulationBase void
flushWorldSpatialState( DNBBasicWorld::Handle )
    DNB_THROW_SPEC_NULL;

/**
 * Force observation on all data that needs to be flushed to CNEXT model
 */
ExportedByDNBSimulationBase void
flushObservers( DNBBasicEntity::Handle )
    DNB_THROW_SPEC_NULL;

/**
 * Force observation on all data that needs to be flushed to CNEXT model
 */
ExportedByDNBSimulationBase void
flushWorldObservers( DNBBasicWorld::Handle )
    DNB_THROW_SPEC_NULL;

/**
 * Sets the flag to manage the build of V5 attachments.
 * @param iBuildAll
 *  if TRUE, enables the build for all V5 attachments, otherwise it 
 *  enables the selective V5 attachment build.
 */
ExportedByDNBSimulationBase void
SetV5AttachmentBuildFlag( boolean iBuildAll )
    DNB_THROW_SPEC_NULL;

// SMW: moved to DNBInfrastructure/DNBCatConversions.h
/*
// Define < operator for CATISiLink_var so that the maps build into the
// DNBGraph can function properly. mmg 12/20/2002
//
ExportedByDNBSimulationBase bool
operator<(const CATBaseUnknown_var &left, const CATBaseUnknown_var &right )
    DNB_THROW_SPEC_NULL; */

#endif // DNBUTILITIES_H
