/* COPYRIGHT DASSAULT SYSTEMES 2003 */
#ifndef CATPPRHubAttrOption_H
#define CATPPRHubAttrOption_H

/**
  * @CAA2Level L0
  * @CAA2Usage U1
**/

enum  CATPPRHubAttrOption {
	All,
	BusinessLogic,
	Provider,
	Site,
	Object
   };

#endif
