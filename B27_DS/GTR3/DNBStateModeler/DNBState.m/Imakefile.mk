# COPYRIGHT DASSAULT SYSTEMES 2003
#======================================================================
# Imakefile for module DNBState.m
#======================================================================
#
#  Apr 2003  Creation: Code generated by the CAA wizard  plk
#======================================================================
#
# SHARED LIBRARY 
#
BUILT_OBJECT_TYPE=SHARED LIBRARY 

INCLUDED_MODULES=	DNBState \
                    DNBStateAutomation
 
LINK_WITH =  JS0GROUP				\		# System
			 JS0FM					\		# System
			 JS0CORBA  \
			 DNBStateItf		\
			 ObjectModeler			\		# ObjectModelerBase
			 AC0SPBAS				\		# ObjectSpecModeler
			 CATApplicationFrame  \
			 CATPDMBaseItf \ 
			 CATVisualization        \   # Visualization
			 KnowledgeItf			\	 # Knowledge
			 ExecutionItfCPP		\	# DNBDpmInterfaces
             ProcessInterfaces			\	# DMAPSInterfaces
             CATProductStructure1			\	# ProductStructure		## AS0STARTUP
			 CATProductStructureInterfaces	\	# ProductStructureInterfaces
			 CATMathematics				\	# Mathematics
			 	
			 
              
		

# System dependant variables
#
OS = AIX
#BUILD = NO

OS = HP-UX
#BUILD = NO

OS = IRIX
#BUILD = NO

OS = SunOS
#BUILD = NO

OS = Windows_NT
#BUILD = NO

