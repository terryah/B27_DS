// COPYRIGHT Dassault Systemes 2005
/////////////////////////////////////////////////////////////////////////
//						                       //
//   TODO 12/31/01:					               //
//	1. Retire setTCPLocation(), etc.	       		       //	
//      2. Retire setTCPDisplacement(), etc.                           //
//      3. Retire doinverse(), doTCPInverse(), etc.		       //
//   TODO 02/28/02:						       //
//      1. Comment new functions.				       //
//
//      mmg     11/05/2003      added support for TCP tracking (TJOG)
//					                               
/////////////////////////////////////////////////////////////////////////	


#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_BASICROBOT3D_H_
#define _DNB_BASICROBOT3D_H_

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>

#include <DNBBasicDevice3D.h>
#include <DNBInverseKinSolver.h>

#include <DNBRobotModel.h>

#ifndef MAX_KIN_CHAINS
#define MAX_KIN_CHAINS 32
#endif

class DNBInverseKinSolverNumeric;
class DNBRobConfiguration;
class DNBKinStatus;


class ExportedByDNBRobotModel DNBBasicRobot3D : public DNBBasicDevice3D
{
    DNB_DECLARE_DYNAMIC_CREATE( DNBBasicRobot3D );

    DNB_DECLARE_SHARED_OBJECT( DNBBasicRobot3D );

    DNB_DECLARE_EXTENDIBLE_OBJECT( DNBBasicRobot3D )

    DNB_DECLARE_EXTENDIBLE_FACTORY( DNBBasicRobot3D );

public:



    typedef DNBInverseKinSolver::SolutionState    SolutionState;

    typedef DNBInverseKinSolver::Solutions        Solutions;
    typedef DNBInverseKinSolver::SolStates        SolStates;


    typedef size_t	Config;
    //
    // The concept of 'Posture' is equivalent to the D5 concept
    // of configuration. This should be used in place of 'Config'
    // for future developments. Another class ( DNBRobConfiguration ) 
    // has been defined separately, which is an aggregate of 1) Posture 
    // and 2) Turn number(s) or Turn sign(s). This defines the kinematic 
    // configuration of a Robot. 
    // jyv, 06/20/2001 
    //
    typedef size_t	Posture;
    typedef size_t	TurnBits;

    typedef scl_vector<scl_wstring, DNB_ALLOCATOR(scl_wstring) > DNBForwardKinMap;
	
	typedef scl_vector< DNBXform3D, DNB_ALLOCATOR(DNBXform3D) > AlphaNodeHomeDB;
	
	typedef scl_vector < int, DNB_ALLOCATOR( int ) > DevDOFIdxDB;

	typedef scl_vector <scl_wstring, DNB_ALLOCATOR(scl_wstring) > ToolProfileNamesDB;
	// The significance of DevDOFIdxDB
	//--------------------------------
	// The kinematic chains will have a series of alpha map nodes indexed 
	// serially from 0 to n for each chain. This serial index is different 
	// than the actual DOF indices of each part of the mechanism. In order 
	// to correspond to the right, actual serial index of any 
	// DNBBasicBody3D::Handle of the mechanism, given any particular kinematic 
	// chain and the index of the alpha Map Node (DNBBasicBody3D::Handle), 
	// the public methods such as getDeviceDOFIndex and getKinematicDOFIndex 
	// use this vector variable. These above vector variables declared are to 
	// be built with the right data when the WDM is built (in the function 
	// DNBRobot::buildRobot by calling the routine 
	// pRobot->createAlphaMapNodeToMechanismDOFMap();
	// pjr, 06:02:09

    
    enum TurnSign { DNBSignNegativeTurn = INT_MIN, DNBSignSolutionAngle = 0, DNBSignPositiveTurn = INT_MAX }; 
    enum TurnMode { DNBSolutionAngle, DNBTurnNumber, DNBTurnSign };

    enum TCPType{ RobotTCP, FixedTCP };

    virtual DNBMessage
    getEntityTypeName( ) const
        DNB_THROW_SPEC_NULL;

    inline void
    setMaxTCPCartesianSpeed( DNBReal speed ) 
	DNB_THROW_SPEC_NULL;

    inline DNBReal 
    getMaxTCPCartesianSpeed( ) const 
	DNB_THROW_SPEC_NULL;

    inline void
    setMaxTCPJointSpeed( DNBReal speed ) 
	DNB_THROW_SPEC_NULL;

    inline DNBReal 
    getMaxTCPJointSpeed( ) const 
	DNB_THROW_SPEC_NULL;

    inline void
    setMaxTCPAngularSpeedS1( DNBReal speed ) 
	DNB_THROW_SPEC_NULL;

    inline DNBReal 
    getMaxTCPAngularSpeedS1( ) const 
	DNB_THROW_SPEC_NULL;

    inline void
    setMaxTCPAngularSpeedS2( DNBReal speed ) 
	DNB_THROW_SPEC_NULL;

    inline DNBReal 
    getMaxTCPAngularSpeedS2( ) const 
	DNB_THROW_SPEC_NULL;

    inline void
    setMaxTCPAngularSpeedS3( DNBReal speed ) 
	DNB_THROW_SPEC_NULL;

    inline DNBReal 
    getMaxTCPAngularSpeedS3( ) const 
	DNB_THROW_SPEC_NULL;

    inline void
    setMaxTCPCartesianAccel( DNBReal accel ) 
	DNB_THROW_SPEC_NULL;

    inline DNBReal 
    getMaxTCPCartesianAccel( ) const
	DNB_THROW_SPEC_NULL;

    inline void
    setMaxTCPJointAccel( DNBReal accel ) 
	DNB_THROW_SPEC_NULL;

    inline DNBReal 
    getMaxTCPJointAccel( ) const 
	DNB_THROW_SPEC_NULL;

    inline void
    setMaxTCPAngularAccelA1( DNBReal speed ) 
	DNB_THROW_SPEC_NULL;

    inline DNBReal 
    getMaxTCPAngularAccelA1( ) const 
	DNB_THROW_SPEC_NULL;

    inline void
    setMaxTCPAngularAccelA2( DNBReal speed ) 
	DNB_THROW_SPEC_NULL;

    inline DNBReal 
    getMaxTCPAngularAccelA2( ) const 
	DNB_THROW_SPEC_NULL;

    inline void
    setMaxTCPAngularAccelA3( DNBReal speed ) 
	DNB_THROW_SPEC_NULL;

    inline DNBReal 
    getMaxTCPAngularAccelA3( ) const 
	DNB_THROW_SPEC_NULL;

    inline void
    setDefaultApproachAxis( DNBMath3D::AxisType axis )
	DNB_THROW_SPEC_NULL;

    inline void
    setDefaultSingularityTolerance( DNBReal tol ) 
	DNB_THROW_SPEC((DNBERangeError));

    inline DNBReal 
    getDefaultSingularityTolerance( ) const 
	DNB_THROW_SPEC_NULL;



    inline void
    setCurrentSingularityTolerance( DNBReal tol ) 
	DNB_THROW_SPEC((DNBERangeError));

    inline DNBReal 
    getCurrentSingularityTolerance( ) const 
	DNB_THROW_SPEC_NULL;

    inline void
    resetCurrentSingularityTolerance( ) 
	DNB_THROW_SPEC_NULL;

    inline void
    setCurrentToolProfileName( const scl_wstring &name )
	DNB_THROW_SPEC_NULL;

	// To add the tool profile name(s) for the active kin chain
	// Call setActiveKinChainIndex(int) to set active kin chain before
	// calling the below function
	inline void
	addChainProfileName( const scl_wstring &profileName )
	DNB_THROW_SPEC_NULL;

    inline scl_wstring
    getCurrentToolProfileName( ) const
	DNB_THROW_SPEC_NULL;

    void
    setInverseKinSolver( const DNBInverseKinSolver *pInvSolver )
	DNB_THROW_SPEC((DNBEAlreadyRegistered));

    inline DNBInverseKinSolver *
    getInverseKinSolver( ) const
	DNB_THROW_SPEC_NULL;

    void
    clearInverseKinSolver( )
	DNB_THROW_SPEC_NULL;

    void
    setRRSInverseKinSolver( const DNBInverseKinSolver *pInvSolver )
	DNB_THROW_SPEC((DNBEAlreadyRegistered));

    void
    clearRRSInverseKinSolver( )
	DNB_THROW_SPEC_NULL;

    void
    setInvKinSolverNumeric( const DNBInverseKinSolverNumeric *pInvSolver )
	DNB_THROW_SPEC((DNBEAlreadyRegistered));

    inline DNBInverseKinSolverNumeric *
    getInvKinSolverNumeric() const
	DNB_THROW_SPEC_NULL;

    void
    clearInvKinSolverNumeric()
	DNB_THROW_SPEC_NULL;

    bool
    activateRRSInvKinSolver( )
	DNB_THROW_SPEC_NULL;

    bool
    activateDefaultInvKinSolver( )
	DNB_THROW_SPEC_NULL;

    inline bool 
    isRRSInvKinSolverActive () const
	DNB_THROW_SPEC_NULL;

    inline bool
    hasInverseKinSolver( ) const
	DNB_THROW_SPEC_NULL;

    inline virtual size_t
    getSolutionCount( ) const
	DNB_THROW_SPEC_NULL;

    void
    setForwardKinMap( const DNBForwardKinMap &kinMap )
        DNB_THROW_SPEC(( scl_bad_alloc, DNBEInvalidFormat, DNBEDoesNotExist ));

    void
    getForwardKinMap( DNBForwardKinMap &kinMap ) const
	DNB_THROW_SPEC_NULL;

    void
    clearForwardKinMap( )
	DNB_THROW_SPEC_NULL;

    typedef scl_pair < scl_wstring, bool >			 aPosture;
    typedef scl_vector< aPosture, DNB_ALLOCATOR(aPosture) >    PostureDB;
    typedef scl_vector< scl_wstring, DNB_ALLOCATOR(scl_wstring) >    PostureNameDB;
    typedef scl_vector< scl_wstring, DNB_ALLOCATOR(scl_wstring) >    ConfigNameDB;

    void
    addPosture( const Posture& posture, const scl_wstring& name, 
	       bool feasible = true ) 
	DNB_THROW_SPEC_NULL;

    void
    setPostureName( const Posture& posture, const scl_wstring& name ) 
	DNB_THROW_SPEC_NULL;

    void
    setPostureNames( const PostureNameDB& names ) 
	DNB_THROW_SPEC_NULL;

    scl_wstring  
    getPostureName( const Posture& posture ) const
	DNB_THROW_SPEC_NULL;

    void  
    getPostureNames( PostureNameDB& names ) const
	DNB_THROW_SPEC((scl_bad_alloc));

    void 
    getPostures( PostureDB& postures ) const
	DNB_THROW_SPEC((scl_bad_alloc));

    bool 
    hasPosture( const scl_wstring& name, Posture& posture ) const
	DNB_THROW_SPEC((scl_bad_alloc));

	DNBReal
	getCurrentTCPSpeed ( ) const
	DNB_THROW_SPEC_NULL;

    void
    setPostureFeasibility( const Posture& posture, bool feasible ) 
	DNB_THROW_SPEC_NULL;

    bool 
    isPostureFeasible( const Posture& posture ) const 
	DNB_THROW_SPEC_NULL;

    inline void
    setDefaultPosture( Posture posture )
	DNB_THROW_SPEC_NULL;

    inline Posture
    getDefaultPosture( ) const
	DNB_THROW_SPEC_NULL;

    //
    // Returns a bool 'true' if it is successful in determining 
    // a posture based on current DOF values. A bool 'false' 
    // is returned in case the posture is:
    //			    1) Singular
    //			    2) Unreachable
    //			    3) Infeasible 
    //
    bool 
    getCurrentPosture( Posture& posture )
	DNB_THROW_SPEC((scl_bad_alloc, DNBException));


    //////////////////////////////////////////////////////////////////////
    //                                                                  //
    // Set TCP type: RobotTCP or FixedTCP.                              //
    //                                                                  //
    //////////////////////////////////////////////////////////////////////
    inline void
    setTCPType( TCPType type )
	DNB_THROW_SPEC_NULL;


    inline TCPType
    getTCPType( ) const
	DNB_THROW_SPEC_NULL;

    enum RobotType
    {
	RobotType_Serial,
	RobotType_Bifurcated,
	RobotType_Other
    };

    RobotType
    getRobotType() const
	DNB_THROW_SPEC_NULL;

    ///////////////////////////////////////////////////////////////////////
    //                                                                   //
    // Set uframe offset : 				                 //
    //		1) if tool is on Robot ( RobotTCP ) this offset is with  //
    // respect to the world.                                             //
    // 2) If tool is fixed, this offset is with respect to the           //
    // mount flange ( mount frame ).                                     // 
    // Note: hEntity not used currently.				 //
    //                                                                   //
    ///////////////////////////////////////////////////////////////////////
    void
    setUFrameOffset( const DNBXform3D       &offset,
		     DNBBasicEntity::Handle hEntity = NULL )
	DNB_THROW_SPEC_NULL;

    DNBXform3D 
    getUFrameOffset( DNBBasicEntity::Handle hEntity = NULL )
	DNB_THROW_SPEC_NULL;


    ////////////////////////////////////////////////////////////////////////
    // 									  //
    // States 0,2,5 used to maintain compatiblity with D5 solver error    // 
    // states.								  //
    //									  //
    ////////////////////////////////////////////////////////////////////////
    enum ArmState { 
           	    ArmSolutionGood = 0,
           	    ArmJointLimitsExceeded = 1,
	   	    ArmTargetUnreachable = 2,
	   	    ArmSolutionSingular = 5,
           	    ArmPostureInvalid = 6,
	   	    ArmNoSolution = 7,	     // not in use - 12/27/01
	   	    ArmSolverError = 8 	     // not in use - 12/27/01
		  };

    typedef scl_vector< ArmState, DNB_ALLOCATOR(ArmState) >    KinStates;


    ////////////////////////////////////////////////////////////////////////
    //                                                                    //
    // Performs inverse kinematics so that the target frame specified by  //
    // tgtXform is coincident with the tool frame if tool is fixed or     //
    // tool frame is coincident with the target frame if the tool is      //
    // attached to the Robot.                                             // 
    // If hEntity is NULL, the target is specified with respect to the    //
    // UFrame.                                                            //
    // Applies the default posture to the Robot and returns the           //
    // RequestStatus of the request along with it's kinematic status.     //
    //								          //
    ////////////////////////////////////////////////////////////////////////
    virtual RequestStatus
    setTargetLocation(  const DNBXform3D    &tgtXform, 
			DNBKinStatus	    &state,
			DNBBasicEntity3D::Handle  hEntity = NULL  ) 
	DNB_THROW_SPEC((scl_bad_alloc, DNBException));

    ////////////////////////////////////////////////////////////////////////
    //                                                                    //
    // Performs inverse kinematics so that the target frame specified by  //
    // tgtXform is coincident with the tool frame if tool is fixed or     //
    // tool frame is coincident with the target frame if the tool is      //
    // attached to the Robot.                                             // 
    // If hEntity is NULL, the target is specified with respect to the    //
    // UFrame.                                                            //
    // Applies the posture specified to the Robot and returns the         //
    // RequestStatus of the request along with it's kinematic status.     //
    //								          //
    ////////////////////////////////////////////////////////////////////////
    virtual RequestStatus
    setTargetLocation(  const DNBXform3D    &tgtXform,
			Posture	            posture,
			DNBKinStatus	    &state, 
			DNBBasicEntity3D::Handle  hEntity = NULL  ) 
	DNB_THROW_SPEC((scl_bad_alloc, DNBException));

    ////////////////////////////////////////////////////////////////////////
    //                                                                    //
    // Performs inverse kinematics so that the target frame specified by  //
    // tgtXform is coincident with the tool frame if tool is fixed or     //
    // tool frame is coincident with the target frame if the tool is      //
    // attached to the Robot.                                             // 
    // If hEntity is NULL, the target is specified with respect to the    //
    // UFrame.                                                            //
    // Applies the configuration specified to the Robot and returns the   //    
    // RequestStatus of the request along with it's kinematic status.     //
    //								          //
    ////////////////////////////////////////////////////////////////////////
    virtual RequestStatus
    setTargetLocation( const DNBXform3D   		&tgtXform,
		       const DNBRobConfiguration	&config,  
		       DNBKinStatus	    		&state,  
		       DNBBasicEntity3D::Handle         hEntity = NULL ) 
	DNB_THROW_SPEC((scl_bad_alloc, DNBException));


    ////////////////////////////////////////////////////////////////////////
    //                                                                    //
    // Performs inverse kinematics so that the target frame specified by  //
    // tagPoint is coincident with the tool frame if the tool is fixed or //
    // the tool frame is coincident with tagPoint if the tool is attached //
    // to  the Robot. The tag point is assumed to be spatially parented   //
    // to an entity that is fixed with respect to the mount frame.        // 
    // Uses posture and turn information on tagPoint if available         //
    // otherwise uses the default posture.   		                  // 
    // Returns RequestStatus and kinematic status of the request.         //
    //								          //
    ////////////////////////////////////////////////////////////////////////
    virtual RequestStatus
    setTargetLocation( DNBBasicTagPoint3D::Handle     tagPoint, 
		       DNBKinStatus	              &state  ) 
	DNB_THROW_SPEC((scl_bad_alloc, DNBException));

    ////////////////////////////////////////////////////////////////////////
    //                                                                    //
    // Performs inverse kinematics so that the target frame specified by  //
    // tagPoint is coincident with the tool frame if the tool is fixed or //
    // the tool frame is coincident with tagPoint if the tool is attached //
    // to  the Robot. The tag point is assumed to be spatially parented   //
    // to an entity that is fixed with respect to the mount frame.        // 
    // Uses posture specified.                                            //
    // otherwise uses the default posture.   		                  // 
    // Returns RequestStatus and kinematic status of the request.         //
    //								          //
    ////////////////////////////////////////////////////////////////////////
    virtual RequestStatus
    setTargetLocation( DNBBasicTagPoint3D::Handle	tagPoint, 
		       Posture	            		posture,
		       DNBKinStatus	    		&state  ) 
	DNB_THROW_SPEC((scl_bad_alloc, DNBException));

    ////////////////////////////////////////////////////////////////////////
    //                                                                    //
    // Performs inverse kinematics so that the target frame specified by  //
    // tagPoint is coincident with the tool frame if the tool is fixed or //
    // the tool frame is coincident with tagPoint if the tool is attached //
    // to  the Robot. The tag point is assumed to be spatially parented   //
    // to an entity that is fixed with respect to the mount frame.        // 
    // Uses configuration specified.                                      //
    // otherwise uses the default posture.   		                  // 
    // Returns RequestStatus and kinematic status of the request.         //
    //								          //
    ////////////////////////////////////////////////////////////////////////
    virtual RequestStatus
    setTargetLocation( DNBBasicTagPoint3D::Handle	tagPoint,
		       const DNBRobConfiguration	&config,  
		       DNBKinStatus	    		&state ) 
	DNB_THROW_SPEC((scl_bad_alloc, DNBException));


    ////////////////////////////////////////////////////////////////////////
    //                                                                    //
    // Returns location of target wrt to entity if specified, else wrt    // 
    // UFrame. Assumes mount part is set.                                 //
    //									  //
    ////////////////////////////////////////////////////////////////////////  
    DNBXform3D 
    getTargetLocation( DNBBasicEntity3D::Handle hEntity = NULL ) const
	DNB_THROW_SPEC_NULL;


    ////////////////////////////////////////////////////////////////////////
    //                                                                    //
    // Performs inverse kinematics so that the target frame specified by  //
    // tgtXform is coincident with the UFrame if tool is fixed or the     //
    // mount frame is coincident with the target frame if the tool is     //
    // attached to the Robot.                                             // 
    // If hEntity is NULL, the target is specified with respect to the    //
    // UFrame.                                                            //
    // Applies the default posture to the Robot and returns the           //
    // RequestStatus of the request along with it's kinematic status.     //
    //								          //
    ////////////////////////////////////////////////////////////////////////
    virtual RequestStatus
    setArmLocation(  const DNBXform3D    &tgtXform, 
			DNBKinStatus	    &state,
			DNBBasicEntity3D::Handle  hEntity = NULL  ) 
	DNB_THROW_SPEC((scl_bad_alloc, DNBException));

    ////////////////////////////////////////////////////////////////////////
    //                                                                    //
    // Performs inverse kinematics so that the target frame specified by  //
    // tgtXform is coincident with the UFrame if tool is fixed or the     //
    // mount frame is coincident with the target frame if the tool is     //
    // attached to the Robot.                                             // 
    // If hEntity is NULL, the target is specified with respect to the    //
    // UFrame.                                                            //
    // Applies the posture specified.                                     //
    // Returns RequestStatus along with it's kinematic status.            //
    //								          //
    ////////////////////////////////////////////////////////////////////////
    virtual RequestStatus
    setArmLocation(  const DNBXform3D    &tgtXform,
			Posture	            posture,
			DNBKinStatus	    &state, 
			DNBBasicEntity3D::Handle  hEntity = NULL  ) 
	DNB_THROW_SPEC((scl_bad_alloc, DNBException));

    ////////////////////////////////////////////////////////////////////////
    //                                                                    //
    // Performs inverse kinematics so that the target frame specified by  //
    // tgtXform is coincident with the UFrame if tool is fixed or the     //
    // mount frame is coincident with the target frame if the tool is     //
    // attached to the Robot.                                             // 
    // If hEntity is NULL, the target is specified with respect to the    //
    // UFrame.                                                            //
    // Applies the configuration specified.                               //
    // Returns RequestStatus along with it's kinematic status.            //
    //								          //
    ////////////////////////////////////////////////////////////////////////
    virtual RequestStatus
    setArmLocation( const DNBXform3D   		&tgtXform,
		       const DNBRobConfiguration	&config,  
		       DNBKinStatus	    		&state,  
		       DNBBasicEntity3D::Handle         hEntity = NULL ) 
	DNB_THROW_SPEC((scl_bad_alloc, DNBException));


    ////////////////////////////////////////////////////////////////////////
    //                                                                    //
    // Performs inverse kinematics so that the target frame specified by  //
    // tagPoint is coincident with the UFrame if the tool is fixed or     //
    // the mount frame is coincident with tagPoint if the tool is         //
    // attached to the Robot. The tag point is assumed to be spatially    //
    // parented to an entity that is fixed with respect to the mount      //
    // frame.                                                             // 
    // Uses posture and turn information on tagPoint if available         //
    // otherwise uses the default posture.   		                  // 
    // Returns RequestStatus and kinematic status of the request.         //
    //								          //
    ////////////////////////////////////////////////////////////////////////
    virtual RequestStatus
    setArmLocation( DNBBasicTagPoint3D::Handle     tagPoint, 
		       DNBKinStatus	              &state  ) 
	DNB_THROW_SPEC((scl_bad_alloc, DNBException));

    ////////////////////////////////////////////////////////////////////////
    //                                                                    //
    // Performs inverse kinematics so that the target frame specified by  //
    // tagPoint is coincident with the UFrame if the tool is fixed or     //
    // the mount frame is coincident with tagPoint if the tool is         //
    // attached to the Robot. The tag point is assumed to be spatially    //
    // parented to an entity that is fixed with respect to the mount      //
    // frame.                                                             // 
    // Uses the posture specified.                                        // 
    // Returns RequestStatus and kinematic status of the request.         //
    //								          //
    ////////////////////////////////////////////////////////////////////////
    virtual RequestStatus
    setArmLocation( DNBBasicTagPoint3D::Handle	tagPoint, 
		       Posture	            		posture,
		       DNBKinStatus	    		&state  ) 
	DNB_THROW_SPEC((scl_bad_alloc, DNBException));

    ////////////////////////////////////////////////////////////////////////
    //                                                                    //
    // Performs inverse kinematics so that the target frame specified by  //
    // tagPoint is coincident with the UFrame if the tool is fixed or     //
    // the mount frame is coincident with tagPoint if the tool is         //
    // attached to the Robot. The tag point is assumed to be spatially    //
    // parented to an entity that is fixed with respect to the mount      //
    // frame.                                                             // 
    // Uses the configuration specified.                                  // 
    // Returns RequestStatus and kinematic status of the request.         //
    //								          //
    ////////////////////////////////////////////////////////////////////////
    virtual RequestStatus
    setArmLocation( DNBBasicTagPoint3D::Handle	tagPoint,
		       const DNBRobConfiguration	&config,  
		       DNBKinStatus	    		&state ) 
	DNB_THROW_SPEC((scl_bad_alloc, DNBException));

    /////////////////////////////////////////////////////////////////////
    //
    // Returns mount frame with respect world if entity NULL  else with
    // respect to entity if non-NULL for RobotTCP; returns UFrame 
    // with respect to world if entity NULL else with respect to entity
    // if non-NULL for FixedTCP. Assumes mount part is set.
    //
    ///////////////////////////////////////////////////////////////////// 
    virtual DNBXform3D 
    getArmLocation( DNBBasicEntity3D::Handle hEntity = NULL ) const
	DNB_THROW_SPEC_NULL;


    ////////////////////////////////////////////////////////////////////////
    //									  //
    //  Analytic form of setTargetLocation.                               //
    //								          //
    ////////////////////////////////////////////////////////////////////////
    virtual void 
    calcDOFDisplacementsTarget( const DNBXform3D              &tgtXform,
                          Solutions			&sols,
		          KinStates			&states,
			  DNBBasicEntity3D::Handle      hEntity = NULL ) 
	DNB_THROW_SPEC((scl_bad_alloc, DNBException));

    ////////////////////////////////////////////////////////////////////////
    //									  //
    //  Analytic form of setTargetLocation.                               // 
    //								          //
    ////////////////////////////////////////////////////////////////////////
    virtual void 
    calcDOFDisplacementsTarget( const DNBXform3D          &tgtXform,
		          Posture		    &posture,
                          DNBMathVector		    &solution,
		          DNBKinStatus		    &state, 
			  DNBBasicEntity3D::Handle  hEntity = NULL  ) 
	DNB_THROW_SPEC((scl_bad_alloc, DNBException));

    ////////////////////////////////////////////////////////////////////////
    //									  //
    //  Analytic form of setTargetLocation.                               // 
    //								          //
    ////////////////////////////////////////////////////////////////////////
    virtual void 
    calcDOFDisplacementsTarget( const DNBXform3D              &tgtXform,
		          const DNBRobConfiguration	&config,
                          DNBMathVector		        &solution,
		          DNBKinStatus		        &state, 
			  DNBBasicEntity3D::Handle      hEntity = NULL  ) 
	DNB_THROW_SPEC((scl_bad_alloc, DNBException));

    ////////////////////////////////////////////////////////////////////////
    //									  //
    //  Analytic form of setTargetLocation.                               // 
    //								          //
    ////////////////////////////////////////////////////////////////////////
    virtual void 
    calcDOFDisplacementsTarget( DNBBasicTagPoint3D::Handle	tagPoint,
                          Solutions			&sols,
		          KinStates			&states )
	DNB_THROW_SPEC((scl_bad_alloc, DNBException));

    ////////////////////////////////////////////////////////////////////////
    //									  //
    //  Analytic form of setTargetLocation.                               // 
    //								          //
    ////////////////////////////////////////////////////////////////////////
    virtual void 
    calcDOFDisplacementsTarget( DNBBasicTagPoint3D::Handle    tagPoint,
		          Posture			posture,
                          DNBMathVector			&solution,
		          DNBKinStatus			&state )
	DNB_THROW_SPEC((scl_bad_alloc, DNBException));

    ////////////////////////////////////////////////////////////////////////
    //									  //
    //  Analytic form of setTargetLocation.                               // 
    //								          //
    ////////////////////////////////////////////////////////////////////////
    virtual void 
    calcDOFDisplacementsTarget( DNBBasicTagPoint3D::Handle	tagPoint,
		          const DNBRobConfiguration	&config,
                          DNBMathVector		        &solution,
		          DNBKinStatus		        &state )
	DNB_THROW_SPEC((scl_bad_alloc, DNBException));

    //  Analytic form of setArmLocation. 

    virtual void 
    calcDOFDisplacementsArm( const DNBXform3D              &tgtXform,
                          Solutions			&sols,
		          KinStates			&states,
			  DNBBasicEntity3D::Handle      hEntity = NULL ) 
	DNB_THROW_SPEC((scl_bad_alloc, DNBException));

    //  Analytic form of setArmLocation. 
    virtual void 
    calcDOFDisplacementsArm( const DNBXform3D          &tgtXform,
		          Posture		    &posture,
                          DNBMathVector		    &solution,
		          DNBKinStatus		    &state, 
			  DNBBasicEntity3D::Handle  hEntity = NULL  ) 
	DNB_THROW_SPEC((scl_bad_alloc, DNBException));

    //  Analytic form of setArmLocation. 
    virtual void 
    calcDOFDisplacementsArm( const DNBXform3D              &tgtXform,
		          const DNBRobConfiguration	&config,
                          DNBMathVector		        &solution,
		          DNBKinStatus		        &state, 
			  DNBBasicEntity3D::Handle      hEntity = NULL  ) 
	DNB_THROW_SPEC((scl_bad_alloc, DNBException));


    //  Analytic form of setArmLocation. 
    virtual void 
    calcDOFDisplacementsArm( DNBBasicTagPoint3D::Handle	tagPoint,
                          Solutions			&sols,
		          KinStates			&states )
	DNB_THROW_SPEC((scl_bad_alloc, DNBException));


    //  Analytic form of setArmLocation. 
    virtual void 
    calcDOFDisplacementsArm( DNBBasicTagPoint3D::Handle    tagPoint,
		          Posture			posture,
                          DNBMathVector			&solution,
		          DNBKinStatus			&state )
	DNB_THROW_SPEC((scl_bad_alloc, DNBException));

    //  Analytic form of setArmLocation. 
    virtual void 
    calcDOFDisplacementsArm( DNBBasicTagPoint3D::Handle	tagPoint,
		          const DNBRobConfiguration	&config,
                          DNBMathVector		        &solution,
		          DNBKinStatus		        &state )
	DNB_THROW_SPEC((scl_bad_alloc, DNBException));



    ///////////////////////////////////////////////////////////////////////
    //							                 //
    // Returns all solutions and corresponding states for given T6.      //
    //								         //
    ///////////////////////////////////////////////////////////////////////  
    void
    doInverseKinematics( const DNBXform3D &T6,
                         Solutions        &sols, 
			 KinStates        &kinStates ) const
    	DNB_THROW_SPEC((scl_bad_alloc, DNBException));

    void
    doArmInverseKinematics( const DNBXform3D &T6,
	                    DNBMathVector    &dofValues,
                            Solutions        &sols, 
			    KinStates        &kinStates ) const
    	DNB_THROW_SPEC((scl_bad_alloc, DNBException));

    void
    calcArmDOFDisplacementsTarget( const DNBXform3D          &tgtXform,
 				   const DNBRobConfiguration &hConfig,
 				   DNBMathVector             &solution,
 				   DNBKinStatus              &state,
				   DNBBasicEntity3D::Handle  hEntity = NULL ) const
        DNB_THROW_SPEC((scl_bad_alloc, DNBException));



////////////////////////////////////////////////////////////////////////
   //  
   // Remove xxxConfigxxx() functions after notifying clients.
   // 


    void
    setConfigName( const Posture& posture, const scl_wstring& name ) 
	DNB_THROW_SPEC_NULL;

    void
    setConfigNames( const PostureNameDB& names ) 
	DNB_THROW_SPEC_NULL;

    scl_wstring  
    getConfigName( const Posture& posture ) const
	DNB_THROW_SPEC_NULL;

    void  
    getConfigNames( PostureNameDB& names ) const
	DNB_THROW_SPEC((scl_bad_alloc));

    void 
    getConfigs( PostureDB& postures ) const
	DNB_THROW_SPEC((scl_bad_alloc));

    void
    setConfigFeasibility( const Posture& posture, bool feasible ) 
	DNB_THROW_SPEC_NULL;

    bool 
    isConfigFeasible( const Posture& posture ) const 
	DNB_THROW_SPEC_NULL;

    inline void
    setDefaultConfig( Posture posture )
	DNB_THROW_SPEC_NULL;

    inline Posture
    getDefaultConfig( ) const
	DNB_THROW_SPEC_NULL;

    //
    // Returns a bool 'true' if it is successful in determining 
    // a posture based on current DOF values. A bool 'false' 
    // is returned in case the posture is:
    //			    1) Singular
    //			    2) Unreachable
    //			    3) Infeasible 
    //
    bool 
    getCurrentConfig( Posture& posture )
	DNB_THROW_SPEC((scl_bad_alloc, DNBException));

///////////////////////////////////////////////////////////////////////

    inline void
    setCurrentUTool( const DNBXform3D &uToolOffset )
	DNB_THROW_SPEC_NULL;

    inline DNBXform3D
    getCurrentUTool( ) const
	DNB_THROW_SPEC_NULL;


    //
    //tcpLoc is relative to hEntity which defaults to the world (NULL)
    //

    ///////////////////////////////////////////////////////////////
    // 	jyv 	10/10/2001        
    // To be deprecated.					 //
    ///////////////////////////////////////////////////////////////

    inline RequestStatus
    setTCPLocation( const DNBXform3D         &tcpLoc, 
                    DNBBasicEntity3D::Handle hEntity = NULL )
	DNB_THROW_SPEC((scl_bad_alloc, DNBException));

    // To be deprecated.					 //
    inline RequestStatus
    setTCPLocation( const DNBXform3D &tcpLoc, Posture posture,
                    DNBBasicEntity3D::Handle hEntity = NULL )
	DNB_THROW_SPEC((scl_bad_alloc, DNBException));

    /////////////////////////////////////////////////////////////
    // 	jyv 	10/10/2001        
    // To be deprecated.					 //
    /////////////////////////////////////////////////////////////
    RequestStatus
    setTCPLocation( const DNBXform3D           &tcpLoc, 
		    const DNBRobConfiguration  &hConfig,
		    DNBBasicEntity3D::Handle   hEntity = NULL )
	DNB_THROW_SPEC((scl_bad_alloc, DNBException));

    // To be deprecated.					 //
    inline RequestStatus
    setMountLocation( const DNBXform3D &mountLoc, Posture posture,
                      DNBBasicEntity3D::Handle hEntity = NULL )
	DNB_THROW_SPEC((scl_bad_alloc, DNBException));

    // To be deprecated.					 //
    inline RequestStatus
    setMountPartLocation( const DNBXform3D &mountPartLoc, Posture posture,
                          DNBBasicEntity3D::Handle hEntity = NULL )
	DNB_THROW_SPEC((scl_bad_alloc, DNBException));

    //
    //tcpDisplacement is relative to the base part
    //
    // To be deprecated.					 //
    inline RequestStatus
    setTCPDisplacement( const DNBXform3D &tcpDisplacement, Posture posture )
	DNB_THROW_SPEC((scl_bad_alloc, DNBException));

    // To be deprecated.					 //
    inline RequestStatus
    setMountDisplacement( const DNBXform3D &mountDisplacement, Posture posture )
	DNB_THROW_SPEC((scl_bad_alloc, DNBException));

    // To be deprecated.					 //
    RequestStatus
    setMountPartDisplacement( const DNBXform3D &target, Posture posture )
	DNB_THROW_SPEC((scl_bad_alloc, DNBException));

    // To be deprecated.					 //
    inline RequestStatus
    setTCPDisplacement( const DNBXform3D &tcpDisplacement )
	DNB_THROW_SPEC((scl_bad_alloc, DNBException));

    // To be deprecated.					 //
    inline RequestStatus
    setMountDisplacement( const DNBXform3D &mountDisplacement )
	DNB_THROW_SPEC((scl_bad_alloc, DNBException));

    // To be deprecated.					 //
    inline RequestStatus
    setMountPartDisplacement( const DNBXform3D &target )
	DNB_THROW_SPEC((scl_bad_alloc, DNBException));

    /////////////////////////////////////////////////////////////
    // 	jyv 	10/10/2001        
    // To be deprecated.					 //
    /////////////////////////////////////////////////////////////
    RequestStatus
    setTCPDisplacement( const DNBXform3D          &tcpDisplacement, 
		        const DNBRobConfiguration &hConfig )
	DNB_THROW_SPEC((scl_bad_alloc, DNBException));

    //
    //results of the following three methods are relative to hEntity
    //which defaults to the world (NULL)
    //
    inline DNBXform3D
    getTCPLocation( DNBBasicEntity3D::Handle hEntity = NULL ) const
	DNB_THROW_SPEC_NULL;
    //
    //results of the following three methods are relative to the base part
    //
    inline DNBXform3D
    getTCPDisplacement( ) const
	DNB_THROW_SPEC_NULL;
    //
    //location is the mount part location, it is relative to the base part
    //
    // To be deprecated.					 //
    virtual void
    doInverse( const DNBXform3D &location,
               Solutions &sols, SolStates &states ) const
	DNB_THROW_SPEC((scl_bad_alloc, DNBException));


    //
    //location is the TCP location, it is relative to hEntity which
    //defaults to the world (NULL)
    //
    // To be deprecated.					 //
    virtual void
    doTCPInverse( const DNBXform3D &location,
                  Solutions &sols, SolStates &states,
	          DNBBasicEntity3D::Handle hEntity = NULL ) const
	DNB_THROW_SPEC((scl_bad_alloc, DNBException));


    // To be deprecated.					 //
    virtual void
    doTCPInverse( const DNBXform3D&		location,
		  const DNBRobConfiguration&	hsConfig,
                  Solutions&			sols,
		  SolStates&			states,
	          DNBBasicEntity3D::Handle 	hEntity = NULL ) const
	DNB_THROW_SPEC((scl_bad_alloc, DNBException));

	void setLinearVelocityStatus(int,unsigned int red=-1,
        unsigned int green=-1,unsigned int blue=-1)
	        DNB_THROW_SPEC_NULL;

	void setRotateVelocityStatus(int,unsigned int red=-1,
        unsigned int green=-1,unsigned int blue=-1)
	        DNB_THROW_SPEC_NULL;
	
	void setLinearAccelerationStatus(int,unsigned int red=-1,
        unsigned int green=-1,unsigned int blue=-1)
	        DNB_THROW_SPEC_NULL;

	void setRotateAccelerationStatus(int,unsigned int red=-1,
        unsigned int green=-1,unsigned int blue=-1)
	        DNB_THROW_SPEC_NULL;

    void resetLimitStatus()
         DNB_THROW_SPEC_NULL;

	void setReachabilityMode(unsigned int,unsigned int,unsigned int)
	        DNB_THROW_SPEC_NULL;

	void setSingularityMode(unsigned int,unsigned int,unsigned int)
	        DNB_THROW_SPEC_NULL;

        //
        // Deprecate.
        //
	void hardErrorFeedback(SolutionState state)
        DNB_THROW_SPEC_NULL;


	RequestStatus
	checkLinearVelocity(DNBReal tcp_vel)
        DNB_THROW_SPEC_NULL;
	
	RequestStatus
	checkLinearAcceleration(DNBReal tcp_accel)
        DNB_THROW_SPEC_NULL;
	
	RequestStatus
	checkRotateVelocity(DNBReal tcp_avel)
        DNB_THROW_SPEC_NULL;

	RequestStatus
	checkRotateAcceleration(DNBReal tcp_aaccel)
        DNB_THROW_SPEC_NULL;

	void resetLimitFlags()
        DNB_THROW_SPEC_NULL;

    //
    // Creates DNBRobConfiguration with default posture.
    //
    DNBRobConfiguration
    createRobConfiguration()
	DNB_THROW_SPEC((scl_bad_alloc));

    //
    // Computes bitvector corresponding to turn number or turn sign
    // as the case may, based on joint limits of each DOF.
    // This function should be called after the DOF limits are set.
    // 
    TurnBits 
    evaluateTurnBits( ) const
	DNB_THROW_SPEC_NULL;

    //
    // Returns turn number limits if DOF is angular, irrespective
    // of whether turn number mode is enabled or not. 
    // Returns bool 'false' if DOF is not angular, else 'true'.
    //
    bool 
    getTurnNumberLimits( size_t dofIndex, int& lower, int& upper ) const
        DNB_THROW_SPEC_NULL;

    //
    // Returns turn number ( 0 for [-Pi,Pi) ) if DOF is angular, 
    // irrespective of whether turn number mode is enabled or not. 
    // Returns bool 'false' if DOF is not angular, else 'true'.
    //
    bool 
    getTurnNumber( size_t dofIndex, int& turnNumber ) const
        DNB_THROW_SPEC_NULL;

    //
    // Returns turn sign if DOF is angular, irrespective
    // of whether turn sign mode is enabled or not. 
    // Returns bool 'false' if DOF is not angular, else 'true'.
    //
    bool 
    getTurnSign( size_t dofIndex, TurnSign& turnSign ) const
        DNB_THROW_SPEC_NULL;

    //
    // Set turn mode ( DNBTurnNumber or DNBTurnSign or DNBSolutionAngle ).
    // Internally resets turn bits to zero.
    //
    inline void 
    setTurnMode(  DNBBasicRobot3D::TurnMode turnMode )
        DNB_THROW_SPEC_NULL;

    //
    // Returns turn mode ( DNBTurnNumber or DNBTurnSign or DNBSolutionAngle ).
    //
    inline TurnMode 
    getTurnMode( ) const
        DNB_THROW_SPEC_NULL;

    //
    // Disable support for turn number or turn sign mode  
    // whichever is active. 
    // Internally resets turn bits to zero.
    //
    inline void
    disableTurnMode( )
        DNB_THROW_SPEC_NULL;


    //
    // Enables turn number support for given DOF.
    // Throws exception DNBERangeError if dofIndex > DOF count. 
    // Throws exception DNBENotSupported if turn number mode not enabled.
    //
    void 
    enableTurnNumber( size_t dofIndex )
        DNB_THROW_SPEC(( DNBERangeError, DNBENotSupported ));

    //
    // Enable turn number for DOF's based on value and position 
    // of bit corresponding to given DOF.
    // Throws exception DNBERangeError if turnBits > 2*( 1 << DOFCount ). 
    // Throws exception DNBENotSupported if turn number mode not enabled.
    // 
    // Example: enable turn number for DOF's 4,5 & 6 
    //		TurnBits turnBits = 0;
    //	        turnBits |= ( 1 << 3 );
    //	        turnBits |= ( 1 << 4 );
    //	        turnBits |= ( 1 << 5 );
    //	        pRobot->enableTurnNumbers( turnBits );
    // 
    void 
    enableTurnNumbers( TurnBits turnBits )
        DNB_THROW_SPEC(( DNBERangeError, DNBENotSupported ));

    //
    // Returns 'false' if turn number mode is not enabled.
    // Returns 'true' if turn number enabled for given DOF.
    //
    inline bool 
    isTurnNumberEnabled( size_t dofIndex ) const
        DNB_THROW_SPEC_NULL;


    //
    // Enables turn sign support for given DOF.
    // Throws exception DNBERangeError if dofIndex > DOF count. 
    // Throws exception DNBENotSupported if turn sign mode not enabled.
    //
    void 
    enableTurnSign( size_t dofIndex )
        DNB_THROW_SPEC(( DNBERangeError, DNBENotSupported ));

    //
    // Enable turn sign for DOF's based on value and position 
    // of bit corresponding to given DOF.
    // Throws exception DNBERangeError if turnBits > 2*( 1 << DOFCount ). 
    // Throws exception DNBENotSupported if turn sign mode not enabled.
    // 
    // Example: enable turn sign for DOF's 4,5 & 6 
    //		TurnBits turnBits = 0;
    //	        turnBits |= ( 1 << 3 );
    //	        turnBits |= ( 1 << 4 );
    //	        turnBits |= ( 1 << 5 );
    //	        pRobot->enableTurnSigns( turnBits );
    // 
    void 
    enableTurnSigns( TurnBits turnBits )
        DNB_THROW_SPEC(( DNBERangeError, DNBENotSupported ));

    //
    // Returns 'false' if turn sign mode is not enabled.
    // Returns 'true' if turn sign enabled for given DOF.
    //
    inline bool 
    isTurnSignEnabled( size_t dofIndex ) const
        DNB_THROW_SPEC_NULL;

    void
    setAuxLengths( const DNBMathVector &auxLengths )
        DNB_THROW_SPEC_NULL;

    void
    getAuxLengths( DNBMathVector &auxLengths ) const
        DNB_THROW_SPEC_NULL;

    enum SignatureCalib
    {
	SignatureCalib_None = 0,
	SignatureCalib_JointOffsets = 1,
	SignatureCalib_Vectors = 3
    };

    bool
    setSigType( SignatureCalib sigType )
        DNB_THROW_SPEC_NULL;

    inline SignatureCalib
    getSigType() const
        DNB_THROW_SPEC_NULL;

    void
    setKinParamsJointOffsets( const DNBMathVector &jointOffsets )
        DNB_THROW_SPEC_NULL;

    void
    getKinParamsJointOffsets( DNBMathVector &jointOffsets ) const
        DNB_THROW_SPEC_NULL;

    typedef scl_vector< DNBMathVector, DNB_ALLOCATOR( DNBMathVector ) > VectorCalibData;
    void
    setKinParamsVectors( const VectorCalibData &data )
        DNB_THROW_SPEC_NULL;

    void
    getKinParamsVectors( VectorCalibData &data ) const
        DNB_THROW_SPEC_NULL;

    int
    thetasToAlphas( const DNBMathVector &thetas, DNBMathVector &alphas ) const
        DNB_THROW_SPEC_NULL;

    int
    alphasToThetas( const DNBMathVector &alphas, DNBMathVector &thetas ) const
        DNB_THROW_SPEC_NULL;

    typedef scl_vector< int, DNB_ALLOCATOR(int) > AlphaMapAxes;

    void
    setAlphaMapAxes( const AlphaMapAxes &axes )
        DNB_THROW_SPEC((scl_bad_alloc));

    void
    getAlphaMapAxes( AlphaMapAxes &axes ) const
        DNB_THROW_SPEC((scl_bad_alloc));

    typedef scl_map <DNBBasicBody3D::Handle, scl_wstring, scl_less <DNBBasicBody3D::Handle> > PartJointExprDB;

    typedef scl_map <DNBBasicBody3D::Handle, int, scl_less <DNBBasicBody3D::Handle> > PartJointDOFIdxDB;

    void
    setPartJointExprDB( const PartJointExprDB &data )
        DNB_THROW_SPEC((scl_bad_alloc));

    void
    setPartJointDOFIdxDB( const PartJointDOFIdxDB &data )
        DNB_THROW_SPEC((scl_bad_alloc));

    int
    kin_inq_alphamap( ) const
        DNB_THROW_SPEC((DNBEDoesNotExist));

    inline void
    setArmDOFCount( size_t armDOFCount )
        DNB_THROW_SPEC_NULL;

    inline size_t
    getArmDOFCount() const
        DNB_THROW_SPEC_NULL;

    inline void
    disableErrorFeedback (void)
        DNB_THROW_SPEC_NULL;

    inline void
    enableErrorFeedback (void)
        DNB_THROW_SPEC_NULL;

    void
    enableTCPTracking( DNBBasicEntity3D::Handle )
        DNB_THROW_SPEC_NULL;

    void
    disableTCPTracking()
        DNB_THROW_SPEC_NULL;

	DNBXform3D
	AnalyzeDOFDisplacements(const DNBMathVector &dofs)
	        DNB_THROW_SPEC_NULL;

	DNBXform3D
	AnalyzeSystemDOFDisplacements(const DNBMathVector &dofs)
	        DNB_THROW_SPEC_NULL;
    void
    setKinMode( DNBInverseKinSolver::KinMode kinMode )
        DNB_THROW_SPEC_NULL;

	//To set the active kin chain given its index
	void
	setActiveKinChain( const int &iIndex )
		DNB_THROW_SPEC_NULL;

	//To retrieve active kin chain index
	int
	getActiveKinChainIndex() const
		DNB_THROW_SPEC_NULL;

	// To create a map between the actual serial index (DOF index) of the mechanism
	// with any particular serial index of any kinematic chain. Should be called 
	// when building the WDM.
	bool
	createAlphaMapNodeToMechanismDOFMap()
		DNB_THROW_SPEC_NULL;

	// Retrieves the corresponding device DOF given any serial index (kin chain DOF) of any
	// kinematic chain using the implicit variable activeKinChainIndex_
	int
	getDeviceDOFIndex( const int &alphaMapNodeDOFIdx ) const //Given AlphaMapNodes DOF index
		DNB_THROW_SPEC_NULL;

	// Retrieves the corresponding Kin Chain DOF Index given any device DOF Index
	// for the current active Kinematic Chain (activeKinChainIndex_)
	int
	getKinematicDOFIndex( const int &mechanismDOFIdx ) const//Given the Mechanism DOF index
		DNB_THROW_SPEC_NULL;

	// When inverse solvers yield a solution with num dofs of a kin chain 
	// lesser than the total dofs of the device, (happens when having 
	// multiple kin chains), this new solution
	// is merged with the DOFDisplacements of the entire mechanism.
	bool
	mergeInvKinSolution( DNBMathVector &invKinsolution, DNBMathVector &deviceDOFDisplacement )
		DNB_THROW_SPEC_NULL;

	// To set total no of chains
	void
	setKinChainsCount( const int &kinChainsCount )
		DNB_THROW_SPEC_NULL;
	
	//To retrieve total no of chains
	int
	getKinChainsCount() const
		DNB_THROW_SPEC_NULL;

    ////////////////////////////////////////////////////////////////////////
    //                                                                    //
    // Adds a conveyor to the robot.                                      //
    //								                                      //
    ////////////////////////////////////////////////////////////////////////
    void
    addConveyor (const DNBBasicDevice3D::Handle &hConveyor)
        DNB_THROW_SPEC_NULL;

    void
    getConveyor (const DNBInteger32 Index, DNBBasicDevice3D::Handle &hConveyor)
        DNB_THROW_SPEC_NULL;

    //Returns the travel distance (joint value)of the currently active conveyor
    DNBReal
    getConveyorTravelDistance (void)
        DNB_THROW_SPEC_NULL;

    //Sets an active conveyor
    void
    setActiveConveyor(const DNBInteger32 Index)
        DNB_THROW_SPEC_NULL;

    //Returns an index of an active conveyor
    DNBInteger32
    getActiveConveyor (void)
        DNB_THROW_SPEC_NULL;

    //Gives back the number of conveyors....
    DNBInteger32
    getConveyorCount(void)
        DNB_THROW_SPEC_NULL;

    /**
        R18 Highlights
    **/
    //For now, identification of conveyors is done based on ID, which they
    //will get assigned automatically during creation.
    struct Conveyor
    {
        DNBInteger32                Index_;
        DNBBasicDevice3D::Handle    Handle_;
    };   

    typedef scl_vector <Conveyor, DNB_ALLOCATOR (Conveyor)> Conveyors;

    typedef struct ConveyorList
    {
        DNBReal     StartDistance_;     //joint value at the start of the tracking process
        Conveyors   Conveyors_;         //a list of conveyors
        struct Conveyor*   ActiveConveyor_;    //currently active conveyor
    } ;
    // Occasionally we need to move the robot to evaluate certain conditions.
    // These two functions are used enable/disable the updating of certain state
    // variables and color.
    bool GetUpdateMode ();
    void SetUpdateMode (bool bUpdateMode);


protected:
    DNBBasicRobot3D( )
	DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit, DNBENetworkError));

    DNBBasicRobot3D( const DNBBasicRobot3D &other, CopyMode mode )
	DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit, DNBENetworkError));

    virtual
    ~DNBBasicRobot3D( )
	DNB_THROW_SPEC_NULL;

    virtual void
    resetEntity( )
	DNB_THROW_SPEC_NULL;
#if 0
    virtual RequestStatus
    updateDOFDisplacements( DNBMathVector &displacements )
        DNB_THROW_SPEC_NULL;
#endif

    void
    applyTurnNumber( DNBMathVector& solution, const DNBRobConfiguration& hPosture ) const
        DNB_THROW_SPEC_NULL;

    void
    applyTurnSign( DNBMathVector& solution, const DNBRobConfiguration& hPosture ) const
        DNB_THROW_SPEC_NULL;

    static DNBXform3D
    getEntityLocation( DNBBasicEntity3D::Handle hEntity )
	DNB_THROW_SPEC_NULL;

    DNBRobConfiguration
    extractRobConfig(DNBBasicTagPoint3D::Handle hTagPt )
        DNB_THROW_SPEC_NULL;

	virtual void setColor(size_t joint=-1)//(unsigned int red,unsigned int green,unsigned int blue, int joint)
        DNB_THROW_SPEC_NULL;

    void 
    hardErrorFeedback( ArmState state )
        DNB_THROW_SPEC_NULL;

    TurnMode		   turnMode_;
    TurnBits		   turnBits_;
    
    //put them in public ?
    void 
    setHighlightStatus(const bool);
    bool 
    getHighlightStatus() const;
    
private:    



    ConveyorList    ConveyorList_;

    /**
        End of R18 Highlights
    **/

    typedef PostureDB::iterator		PostureIter;
    typedef PostureDB::const_iterator	PostureConstIter;

    typedef scl_vector<DNBExpressionSolver<DNBReal>::solverPointer,
                 DNB_ALLOCATOR(DNBExpressionSolver<DNBReal>::solverPointer) >
		 DNBExprSolverDB;

    void
    on_event( const DNBNotificationAgent::ConstPointer &, const EventSet & )
        DNB_THROW_SPEC_NULL;

    RequestStatus
    moveRobot( DNBMathVector &dofDisplacements )
	DNB_THROW_SPEC_NULL;

    void
    doForwardKinMapping( DNBMathVector &dofVals ) const
        DNB_THROW_SPEC(( scl_bad_alloc, DNBEDoesNotExist, DNBEZeroDivide,
	                 DNBEOverflowError, DNBEUnderflowError ));
	void
	initForwardKinMap()
		DNB_THROW_SPEC(( scl_bad_alloc, DNBEInvalidFormat, DNBEDoesNotExist ));
#if 0
    static DNBXform3D
    getEntityLocation( DNBBasicEntity3D::Handle hEntity )
	DNB_THROW_SPEC_NULL;
#endif
    bool 
    whichSolutionMatch( const DNBMathVector& tcpDisp, 
			const Solutions& solutions,
			const SolStates& states,
			Posture&		 posture ) const
	DNB_THROW_SPEC_NULL;


    Posture
    closestSolution( const DNBMathVector &currentDOF,
                     const Solutions     &solutions,
                     const KinStates     &states ) const
    DNB_THROW_SPEC_NULL;

    DNBReal
    vectorDiffSqr( const DNBMathVector& currentDOF,
                   const DNBMathVector& aSolution ) const
        DNB_THROW_SPEC_NULL;

    bool
    vectorsEqual( const DNBMathVector& currentDOF,
                  const DNBMathVector& aSolution,
                  DNBReal tolerance = 1.0e-5 ) const
        DNB_THROW_SPEC_NULL;

    //
    // hint: 1 D5 to WDM unit
    //       0 WDM to D5 unit
    //
    void
    applyUnitConversionFactor( DNBMathVector& vec, int hint = 1 ) const
        DNB_THROW_SPEC_NULL;


	void checkVerbose();

#if 0
    void
    applyTurnNumber( DNBMathVector& solution, const DNBRobConfiguration& hPosture ) const
        DNB_THROW_SPEC_NULL;

    void
    applyTurnSign( DNBMathVector& solution, const DNBRobConfiguration& hPosture ) const
        DNB_THROW_SPEC_NULL;
#endif
    void
    normalizeAngularDOF( DNBMathVector& solution  ) const
        DNB_THROW_SPEC_NULL;


    int
    computeTurnNumber( DNBReal angle ) const
	DNB_THROW_SPEC_NULL;

    void
    doTCPDispInverse( const DNBXform3D            &disp,
                      const DNBRobConfiguration   &hConfig,
                      Solutions                   &solution,
                      SolStates                   &states
                     ) const
    DNB_THROW_SPEC((scl_bad_alloc, DNBException));


    RequestStatus 
    applyT6( const DNBXform3D            &T6,
	     Posture		         posture,
	     DNBKinStatus		 &state ) 
    DNB_THROW_SPEC((scl_bad_alloc, DNBException));

    RequestStatus 
    applyT6( const DNBXform3D            &T6,
	     const DNBRobConfiguration   &config, 
	     DNBKinStatus		 &state ) 
    DNB_THROW_SPEC((scl_bad_alloc, DNBException));

#if 0
    DNBRobConfiguration
    extractRobConfig(DNBBasicTagPoint3D::Handle hTagPt )
        DNB_THROW_SPEC_NULL;
#endif

    DNBXform3D
    calculateT6Target( const DNBXform3D         &tgtXform,
                       DNBBasicEntity3D::Handle hEntity ) const
        DNB_THROW_SPEC_NULL;

    DNBXform3D
    calculateT6Target( DNBBasicTagPoint3D::Handle  hTagPoint ) const
        DNB_THROW_SPEC_NULL;

    DNBXform3D
    calculateT6Arm( const DNBXform3D         &tgtXform,
                    DNBBasicEntity3D::Handle hEntity ) const
        DNB_THROW_SPEC_NULL;

    DNBXform3D
    calculateT6Arm( DNBBasicTagPoint3D::Handle  hTagPoint ) const
        DNB_THROW_SPEC_NULL;

    DNBXform3D
    getT6( ) const
        DNB_THROW_SPEC_NULL;

    DNBReal
    evalExpr( DNBBasicBody3D::Handle node, const DNBMathVector &thetas ) const
        DNB_THROW_SPEC_NULL;

    int
    kin_do_arm_inverse_kin( DNBReal *link_lengths,
	                    DNBReal *link_offsets,
			    DNBReal (*T6)[4],
			    Solutions &solutions,
			    SolStates &warnings,
			    size_t in_num_dofs,
			    size_t in_num_solutions, 
			    size_t in_num_arm_dofs,
			    size_t in_max_arm_iterations,
			    DNBMathVector &curr_thetas );

    bool
    doWristFwdKin( DNBXform3D &wrist_to_mp, const DNBMathVector &curr_thetas )
        DNB_THROW_SPEC_NULL;

    bool
    sig_save_home_xforms()
        DNB_THROW_SPEC_NULL;

    void
    sig_restore_home_xforms()
        DNB_THROW_SPEC_NULL;

    bool
    sig_joffsets_to_home_xforms()
        DNB_THROW_SPEC_NULL;

    bool
    sig_vectors_to_home_xforms()
        DNB_THROW_SPEC_NULL;

    void
    doSignatureCalib( DNBXform3D &t6, Solutions &sols, KinStates &states ) const
        DNB_THROW_SPEC_NULL;

    void
    doJntOffsetCalib( Solutions &sols, KinStates &states ) const
        DNB_THROW_SPEC_NULL;

    void
    doVectorCalib( DNBXform3D &t6, Solutions &sols, KinStates &states ) const
        DNB_THROW_SPEC_NULL;

	struct KinChain
	{
		KinChain();
		KinChain( const KinChain &other );
		~KinChain();
		void operator=(const KinChain &other);

		Posture						defaultPosture_;
		Posture						previousPosture_;
		TCPType						TCPType_;
		DNBXform3D					uToolOffset_;
		DNBXform3D					uToolOffsetInv_;
		DNBXform3D					UFrameOffset_;
		DNBXform3D					UFrameOffsetInv_;
		bool						TCPOverSingularLimit_;
		DNBInverseKinSolver			*pInvSolver_;
		DNBInverseKinSolver			*pDefaultInvSolver_;
		DNBInverseKinSolver			*pRRSInvSolver_;
		DNBInverseKinSolverNumeric	*pInvSolverNumeric_;
		DNBMathVector				auxLengths_;
		SignatureCalib				kinParams_sig_type_;
		DNBMathVector				kinParams_joint_offsets_;
		VectorCalibData				vectorCalibData_;
		AlphaMapAxes				alphaMapAxes_;
		AlphaNodeHomeDB				alphaNodeHomeDB_;
		DNBBasicJoint3D::Vector		alphaNodeJointDB_;
		DNBMath3D::AxisType			defaultApproachAxis_;
		size_t						armDOFCount_;
		size_t						PostureCount_;
		PostureDB					PostureDB_;

		DNBReal						MaxTCPCartesianSpeed_;
		DNBReal						MaxTCPJointSpeed_;
		DNBReal						MaxTCPAngularSpeedS1_;
		DNBReal						MaxTCPAngularSpeedS2_;
		DNBReal						MaxTCPAngularSpeedS3_;
		DNBReal						MaxTCPCartesianAccel_;
		DNBReal						MaxTCPJointAccel_;
		DNBReal						MaxTCPAngularAccelA1_;
		DNBReal						MaxTCPAngularAccelA2_;
		DNBReal						MaxTCPAngularAccelA3_;

		Posture						CurrentPostureNumber_;
		
		DevDOFIdxDB					devDOFIdxDB_;
		ToolProfileNamesDB			toolProfileNamesDB_;

	};
	typedef scl_vector < KinChain, DNB_ALLOCATOR( KinChain ) > KinChainDB;
	KinChainDB kinChainDB_;


	//To set the active kin chain given the chain
	void
	setActiveKinChain( const KinChain* kinChain )
		DNB_THROW_SPEC_NULL;

	KinChain	*currChain;
	DNBReal						DefaultSingularityTolerance_;
	DNBReal						CurrentSingularityTolerance_;
	

    typedef scl_map <DNBBasicBody3D::Handle,
                DNBExpressionSolver<DNBReal>::solverPointer,
                scl_less <DNBBasicBody3D::Handle> >
            PartJointExprSolverDB;

    PartJointExprDB                partJointExprDB_;
    PartJointDOFIdxDB              partJointDOFIdxDB_;
    PartJointExprSolverDB          partJointExprSolverDB_;

    scl_wstring                currentToolProfileName_;
	DNBExprSolverDB				exprSolvers_;
	bool						_bFKinMapInitilaized;
	DNBForwardKinMap			fwdKinMap_;
    //////////////////////feedback settings
	int TCPRotateVelocityLimitStatus_, TCPLinearVelocityLimitStatus_,
			  TCPLinearAccelerationLimitStatus_,TCPRotateAccelerationLimitStatus_,
     iniTCPRotateVelocityLimitStatus_, iniTCPLinearVelocityLimitStatus_,
        iniTCPLinearAccelerationLimitStatus_,iniTCPRotateAccelerationLimitStatus_;



	unsigned int TCPLinearVelocityRed_,TCPLinearVelocityGreen_,TCPLinearVelocityBlue_,
		TCPRotateVelocityRed_,TCPRotateVelocityGreen_,TCPRotateVelocityBlue_,
		TCPLinearAccelerationRed_,TCPLinearAccelerationGreen_,TCPLinearAccelerationBlue_,
		TCPRotateAccelerationRed_,TCPRotateAccelerationGreen_,TCPRotateAccelerationBlue_,
		TCPReachRed_,TCPReachGreen_,TCPReachBlue_,
		TCPSingularRed_,TCPSingularGreen_,TCPSingularBlue_;

	bool TCPOverRotateVelocityLimit_, 
		TCPOverLinearVelocityLimit_,
		TCPOverLinearAccelerationLimit_,
		TCPOverRotateAccelerationLimit_;

	bool errorFeedbackEnabled_;

    DNBBasicEntity3D::Handle trackEntity_;	

    //0578001.highlight status flag. 
    //To improve the performance,restore color only when need to do that.
    bool robotHighlightON_;//put it in protected ?
	
};


#include "DNBBasicRobot3D.cc"


#endif /* _DNB_BASICROBOT3D_H_ */
