// COPYRIGHT Dassault Systemes 2005
//*
//* FILE:
//*     DNBCartesianPlannerBase.h      - public header file
//*
//* MODULE:
//*     DNBCartesianPlannerBase  - single non-template class
//*
//* OVERVIEW:
//*     This module defines an extension class which is base for all
//*     motion planner classes.
//*	 
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*		xin			12/06/00
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
// COPYRIGHT Dassault Systemes 2005
//*
//* COPYRIGHT:
//*     Copyright (C) 1998 - 2000 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_CARTESIANPLANNERBASE_H_
#define _DNB_CARTESIANPLANNERBASE_H_


/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */


#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>

#include <DNBMotionPlannerBase.h>
#include <DNBBasicPath3D.h>
#include <DNBBasicRobot3D.h>
#include <DNBSimpleAttribute.h>
#include <DNBAttributeExt.h>


class ExportedByDNBRobotModel DNBCartesianPlannerBase : virtual public DNBMotionPlannerBase
{

    DNB_DECLARE_DYNAMIC_RTTI( DNBCartesianPlannerBase );

public:
	void postEvent(size_t);


    enum TCPJointMode 
		{ 
		      TCPJointShortestAngles,
		      TCPJointSolutionAngles,
			  TCPJointTurnNumbers,
		      TCPJointShortestAnglesIgnoreLimits,
			  TCPJointTurnSigns 
		};


	virtual
	void addTagPoint(DNBBasicTagPoint3D::Handle hTag)
    DNB_THROW_SPEC((DNBEDomainError)); 

	virtual
	void addPath(DNBBasicPath3D::Handle hPath)
    DNB_THROW_SPEC((DNBEDomainError)); 

	virtual 
	DNBReal getCurrentTCPSpeed (void) const = 0;

    virtual
	void clearPath( )
	DNB_THROW_SPEC_NULL;


    virtual
    ~DNBCartesianPlannerBase( )
	DNB_THROW_SPEC_NULL;


	inline DNBBasicPath3D::Handle
		getPath()
			DNB_THROW_SPEC_NULL;

    inline void
    setTCPPlannerPeriod( DNBSimTime period ) 
	DNB_THROW_SPEC((DNBERangeError));

    inline DNBSimTime
    getTCPPlannerPeriod( ) const
	DNB_THROW_SPEC_NULL;

    inline void
    setDefaultTCPSpeed( DNBReal speed )
	DNB_THROW_SPEC_NULL;

    inline DNBReal 
    getDefaultTCPSpeed( ) const
	DNB_THROW_SPEC_NULL;

    inline void
    setDefaultTCPAccel( DNBReal accel )
	DNB_THROW_SPEC_NULL;

    inline DNBReal 
    getDefaultTCPAccel( ) const
	DNB_THROW_SPEC_NULL;

	
protected:
    DNBCartesianPlannerBase( )
	DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit));

    DNBCartesianPlannerBase( const scl_wstring&,DNBBasicRobot3D::Handle)
	DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit));

    DNBCartesianPlannerBase( const DNBCartesianPlannerBase &other,
			    CopyMode mode )
	DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit));

	TCPOrientMode getDefaultTCPOrientMode()
	  	DNB_THROW_SPEC_NULL;
  
	TCPJointMode  getDefaultTCPJointMode( )
	DNB_THROW_SPEC_NULL;

	DNBBasicRobot3D::Handle hRobot;

	DNBBasicPath3D::Handle hPath;	

private:
    DNBSimTime		   TCPPlannerPeriod_;
    DNBReal 		   DefaultTCPSpeed_;
    DNBReal				DefaultTCPAccel_;

};

#include "DNBCartesianPlannerBase.cc"


#endif  /* _DNB_CARTESIANPLANNERBASE_H_ */
