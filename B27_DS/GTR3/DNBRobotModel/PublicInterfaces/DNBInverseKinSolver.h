// COPYRIGHT Dassault Systemes 2005
//08:09:19 A0638548 When RRS2 is connected, CONFIG of 'Out of Limits' cannot be selected. 
//         Fix (adding new enum SolutionState_JointLimitsExceeded) provided by AKP.  -GJA
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_INVERSEKINSOLVER_H_
#define _DNB_INVERSEKINSOLVER_H_

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */


#include <DNBSystemBase.h>
#include <scl_vector.h>
#include <scl_string.h>
#include <DNBSystemDefs.h>

#include <DNBDynamicObject.h>
#include <DNBMathVector.h>
#include <DNBXform3D.h>
#include <DNBSharedHandle.h>
#include <DNBException.h>

#include <DNBRobotModel.h>


class DNBBasicRobot3D;


class ExportedByDNBRobotModel DNBInverseKinSolver : public DNBDynamicObject
{
    DNB_DECLARE_DYNAMIC_RTTI( DNBInverseKinSolver );

public:
    //
    //Avoid circular inclusions
    //
    typedef DNBSharedHandle<DNBBasicRobot3D> RobotHandle;


    enum SolutionState
	 {
	     SolutionState_Good        = 0,
	     SolutionState_JointLimitsExceeded = 1,
	     SolutionState_Unreachable = 2,
	     SolutionState_Singular    = 5
	 };

    typedef scl_vector<DNBMathVector, DNB_ALLOCATOR(DNBMathVector) >    Solutions;
    typedef scl_vector<SolutionState, DNB_ALLOCATOR(SolutionState) >    SolStates;


    virtual scl_wstring
    getInverseKinSolverName( ) const
        DNB_THROW_SPEC_NULL = 0;

    RobotHandle
    getRobot( ) const
        DNB_THROW_SPEC_NULL;

    bool
    isBound( ) const
        DNB_THROW_SPEC_NULL;

    virtual size_t
    getSolutionCount( ) const
        DNB_THROW_SPEC_NULL = 0;

    virtual size_t
    getKinDOFCount( ) const
        DNB_THROW_SPEC_NULL = 0;

    virtual void
    doInverse( const DNBXform3D &target,
               Solutions &sols, SolStates &states ) const
        DNB_THROW_SPEC((scl_bad_alloc, DNBException)) = 0;

    virtual bool
    doWristFwdKin( DNBXform3D &wrist_to_mp, const DNBMathVector &curr_thetas )
        DNB_THROW_SPEC_NULL;

    virtual size_t
    getArmDOFCount( ) const
        DNB_THROW_SPEC_NULL;

    enum KinMode
    {
	KinMode_Normal = 0,
	KinMode_TrackTCP,
	KinMode_Other
    };

    void
    setKinMode( KinMode mode )
        DNB_THROW_SPEC_NULL;

    KinMode
    getKinMode() const
        DNB_THROW_SPEC_NULL;

    DNBInverseKinSolver()
        DNB_THROW_SPEC_NULL;

    DNBInverseKinSolver( const DNBInverseKinSolver &other, CopyMode mode )
        DNB_THROW_SPEC_NULL;

    virtual
    ~DNBInverseKinSolver()
        DNB_THROW_SPEC_NULL;

protected:
    virtual void
    init()
	    DNB_THROW_SPEC_ANY;

    virtual void
    deinit()
	    DNB_THROW_SPEC_ANY;

    KinMode kinMode_;

private:
    void
    bindRobot( RobotHandle hRobot )
        DNB_THROW_SPEC((DNBEAlreadyRegistered));

    void
    clearRobot( )
	    DNB_THROW_SPEC_NULL;


    RobotHandle    hRobot_;

    friend class DNBBasicRobot3D;
};


#endif /* _DNB_INVERSEKINSOLVER_H_ */
