// COPYRIGHT Dassault Systemes 2005
/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

/**
 * @fullreview RTL/czo 02:01:02
 */


inline DNBBasicPath3D::Handle
DNBCartesianPlannerBase::getPath()
	DNB_THROW_SPEC_NULL
{
	return hPath;
}


inline void
DNBCartesianPlannerBase::setTCPPlannerPeriod( DNBSimTime period ) 
	DNB_THROW_SPEC((DNBERangeError))
{
     if( period < 0.0 )
    {
        DNBERangeError    error( DNB_FORMAT("Negative TCP motion planner period"));
        throw error;
    }

	DNBInteger32 nPeriod	= DNB_STATIC_CAST (DNBInteger32, period.seconds() * 1000.0); 
	DNBReal		 dPeriod	= period.seconds();
	
	//This will ensure that values such as 0.300000 get rounded to 0.300000
	//and values such as 0.2999999 get rounded to 0.3000000, too.
	if (fabs((nPeriod/1000.0) - dPeriod) > 0.000001)
	{		
		nPeriod++;
	}
	
	TCPPlannerPeriod_ = nPeriod/1000.0;
}

inline DNBSimTime
DNBCartesianPlannerBase::getTCPPlannerPeriod( ) const
	DNB_THROW_SPEC_NULL
{ 
    //NAV(Mar/20/2006): I am getting values that make no sense. For example, 
	//50ms is returned as 0.05000001s. Need to round the value returned as it
	//creates problems in other spots.

	DNBInteger32	dIntValue = DNB_STATIC_CAST (DNBInteger32, TCPPlannerPeriod_.seconds() * 1000.0);

	return dIntValue/1000.0;

//    return TCPPlannerPeriod_; 
}

inline void
DNBCartesianPlannerBase::setDefaultTCPSpeed( DNBReal speed )
	DNB_THROW_SPEC_NULL
{
    DefaultTCPSpeed_ = speed;
}

inline DNBReal 
DNBCartesianPlannerBase::getDefaultTCPSpeed( ) const
	DNB_THROW_SPEC_NULL
{
    return DefaultTCPSpeed_;
}


inline void
DNBCartesianPlannerBase::setDefaultTCPAccel( DNBReal accel )
	DNB_THROW_SPEC_NULL
{
    DefaultTCPAccel_= accel;
}

inline DNBReal 
DNBCartesianPlannerBase::getDefaultTCPAccel( ) const
	DNB_THROW_SPEC_NULL
{
    return DefaultTCPAccel_; 
}
