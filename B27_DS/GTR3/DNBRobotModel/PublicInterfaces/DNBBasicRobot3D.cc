// COPYRIGHT Dassault Systemes 2005
/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

/**
 * @fullreview RTL/czo 02:01:02
 *
 * @quickReview CZO 03:02:11 for AKP update
 * @quickreview KPR 06:02:15
 * @quickreview kpr 06:02:17
 * @quickreview kpr 06:03:03
 */


#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>



inline void
DNBBasicRobot3D::setMaxTCPCartesianSpeed( DNBReal speed ) 
	DNB_THROW_SPEC_NULL
{
    if ( !currChain )
		return;
	currChain->MaxTCPCartesianSpeed_ = speed;
}

inline DNBReal 
DNBBasicRobot3D::getMaxTCPCartesianSpeed( ) const 
	DNB_THROW_SPEC_NULL
{
    if ( !currChain )
		return 0.0;
	return currChain->MaxTCPCartesianSpeed_;
}

inline void
DNBBasicRobot3D::setMaxTCPJointSpeed( DNBReal speed ) 
	DNB_THROW_SPEC_NULL
{
   if ( !currChain )
		return;
	currChain->MaxTCPJointSpeed_ = speed;
}

inline DNBReal 
DNBBasicRobot3D::getMaxTCPJointSpeed( ) const 
	DNB_THROW_SPEC_NULL
{
    if ( !currChain )
		return 0.0;
	return currChain->MaxTCPJointSpeed_;
}

inline void
DNBBasicRobot3D::setMaxTCPAngularSpeedS1( DNBReal speed ) 
	DNB_THROW_SPEC_NULL
{
    if ( !currChain )
		return;
	currChain->MaxTCPAngularSpeedS1_ = speed;
}

inline DNBReal 
DNBBasicRobot3D::getMaxTCPAngularSpeedS1( ) const 
	DNB_THROW_SPEC_NULL
{
    if ( !currChain )
		return 0.0;
	return currChain->MaxTCPAngularSpeedS1_;
}

inline void
DNBBasicRobot3D::setMaxTCPAngularSpeedS2( DNBReal speed ) 
	DNB_THROW_SPEC_NULL
{
    if ( !currChain )
		return;
	currChain->MaxTCPAngularSpeedS2_ = speed;
}

inline DNBReal 
DNBBasicRobot3D::getMaxTCPAngularSpeedS2( ) const 
	DNB_THROW_SPEC_NULL
{
    if ( !currChain )
		return 0.0;
	return currChain->MaxTCPAngularSpeedS2_;
}

inline void
DNBBasicRobot3D::setMaxTCPAngularSpeedS3( DNBReal speed ) 
	DNB_THROW_SPEC_NULL
{
    if ( !currChain )
		return;
    currChain->MaxTCPAngularSpeedS3_ = speed;
}

inline DNBReal 
DNBBasicRobot3D::getMaxTCPAngularSpeedS3( ) const 
	DNB_THROW_SPEC_NULL
{
    if ( !currChain )
		return 0.0;
	return currChain->MaxTCPAngularSpeedS3_;
}

inline void
DNBBasicRobot3D::setMaxTCPCartesianAccel( DNBReal accel ) 
	DNB_THROW_SPEC_NULL
{
    if ( !currChain )
		return;
	currChain->MaxTCPCartesianAccel_= accel;
}

inline DNBReal 
DNBBasicRobot3D::getMaxTCPCartesianAccel( ) const
	DNB_THROW_SPEC_NULL
{
    if ( !currChain )
		return 0.0;
	return currChain->MaxTCPCartesianAccel_;
}

inline void
DNBBasicRobot3D::setMaxTCPJointAccel( DNBReal accel ) 
	DNB_THROW_SPEC_NULL
{
    if ( !currChain )
		return;
	currChain->MaxTCPJointAccel_= accel;
}

inline DNBReal 
DNBBasicRobot3D::getMaxTCPJointAccel( ) const 
	DNB_THROW_SPEC_NULL
{
    if ( !currChain )
		return 0.0;
	return currChain->MaxTCPJointAccel_;
}

inline void
DNBBasicRobot3D::setMaxTCPAngularAccelA1( DNBReal accel ) 
	DNB_THROW_SPEC_NULL
{
    if ( !currChain )
		return;
	currChain->MaxTCPAngularAccelA1_= accel; 
}

inline DNBReal 
DNBBasicRobot3D::getMaxTCPAngularAccelA1( ) const 
	DNB_THROW_SPEC_NULL
{
    if ( !currChain )
		return 0.0;
	return currChain->MaxTCPAngularAccelA1_;
}

inline void
DNBBasicRobot3D::setMaxTCPAngularAccelA2( DNBReal accel ) 
	DNB_THROW_SPEC_NULL
{
	if ( !currChain )
		return;
    currChain->MaxTCPAngularAccelA2_= accel; 
}

inline DNBReal 
DNBBasicRobot3D::getMaxTCPAngularAccelA2( ) const 
	DNB_THROW_SPEC_NULL
{
    if ( !currChain )
		return 0.0;
	return currChain->MaxTCPAngularAccelA2_;
}

inline void
DNBBasicRobot3D::setMaxTCPAngularAccelA3( DNBReal accel ) 
	DNB_THROW_SPEC_NULL
{
    if ( !currChain )
		return;
	currChain->MaxTCPAngularAccelA3_= accel; 
}

inline DNBReal 
DNBBasicRobot3D::getMaxTCPAngularAccelA3( ) const 
	DNB_THROW_SPEC_NULL
{
    if ( !currChain )
		return 0.0;
	return currChain->MaxTCPAngularAccelA3_;
}

inline void
DNBBasicRobot3D::setDefaultApproachAxis( DNBMath3D::AxisType axis )
    DNB_THROW_SPEC_NULL
{
    if ( !currChain )
		return;
	currChain->defaultApproachAxis_ = axis;
}



inline void
DNBBasicRobot3D::setDefaultSingularityTolerance( DNBReal tol ) 
	DNB_THROW_SPEC((DNBERangeError))
{
    if ( !currChain )
		return;
	if( tol < 0.0 )
    {

        DNBERangeError    error( DNB_FORMAT("Negative singularity tolerance."));
        throw error;

    }
    DefaultSingularityTolerance_ = tol;
}

inline DNBReal 
DNBBasicRobot3D::getDefaultSingularityTolerance( ) const 
	DNB_THROW_SPEC_NULL
{
    if ( !currChain )
		return 0.0;
	return DefaultSingularityTolerance_;
}



inline void
DNBBasicRobot3D::setCurrentSingularityTolerance( DNBReal tol ) 
	DNB_THROW_SPEC((DNBERangeError))

{
   if ( !currChain )
		return;
	if( tol < 0.0 )
   {

        DNBERangeError    error( DNB_FORMAT("Negative singularity tolerance."));
        throw error;

   }
   CurrentSingularityTolerance_ = tol; 
}

inline DNBReal 
DNBBasicRobot3D::getCurrentSingularityTolerance( ) const 
	DNB_THROW_SPEC_NULL

{
    if ( !currChain )
		return 0.0;
	return CurrentSingularityTolerance_;
}

inline void
DNBBasicRobot3D::resetCurrentSingularityTolerance( ) 
	DNB_THROW_SPEC_NULL

{
    if ( !currChain )
		return;
	CurrentSingularityTolerance_ = DefaultSingularityTolerance_;
}




inline DNBInverseKinSolver *
DNBBasicRobot3D::getInverseKinSolver( ) const
    DNB_THROW_SPEC_NULL
{
    if ( !currChain )
		return NULL;
	return currChain->pInvSolver_;
}


inline DNBInverseKinSolverNumeric *
DNBBasicRobot3D::getInvKinSolverNumeric() const
    DNB_THROW_SPEC_NULL
{
    if ( !currChain )
		return NULL;
	return currChain->pInvSolverNumeric_;
}

inline bool
DNBBasicRobot3D::hasInverseKinSolver( ) const
    DNB_THROW_SPEC_NULL
{
    if ( !currChain )
		return false;
	return (currChain->pInvSolver_ != NULL);
}


inline size_t
DNBBasicRobot3D::getSolutionCount( ) const
    DNB_THROW_SPEC_NULL
{
    if ( !currChain )
		return 0;
	DNB_ASSERT( currChain->pInvSolver_ );

    return currChain->pInvSolver_->getSolutionCount( );
}


inline void
DNBBasicRobot3D::setDefaultPosture( Posture posture )
    DNB_THROW_SPEC_NULL
{
    if ( !currChain )
		return;
	DNB_ASSERT( posture < getSolutionCount() );

    currChain->previousPosture_ = currChain->defaultPosture_ = posture;
}


inline DNBBasicRobot3D::Posture
DNBBasicRobot3D::getDefaultPosture( ) const
    DNB_THROW_SPEC_NULL
{
    if ( !currChain )
		return 0;
	return currChain->defaultPosture_;
}


inline void
DNBBasicRobot3D::setCurrentUTool( const DNBXform3D &uToolOffset )
    DNB_THROW_SPEC_NULL
{
    if ( !currChain )
		return;
	currChain->uToolOffsetInv_ = currChain->uToolOffset_ = uToolOffset;
    (currChain->uToolOffsetInv_).invert();
}


inline DNBXform3D
DNBBasicRobot3D::getCurrentUTool( ) const
    DNB_THROW_SPEC_NULL
{
    if ( !currChain )
		return DNBXform3D::Identity;
	return currChain->uToolOffset_;
}

inline DNBBasicRobot3D::RequestStatus
DNBBasicRobot3D::setTCPLocation( const DNBXform3D &tcpLoc,
				 DNBBasicEntity3D::Handle hEntity )
    DNB_THROW_SPEC((scl_bad_alloc, DNBException))
{
#ifndef CNEXT_CLIENT 
    cerr << "DNBBasicRobot3D::setTCPLocation will be deprecated; use setArmDisplacement." << endl;
#endif
    
	if ( !currChain )
		return RequestRejected;
	if( currChain->TCPType_ == FixedTCP )
	return RequestRejected;
    DNBXform3D uToolInv( getCurrentUTool() );
    uToolInv.invert();

    return setMountLocation( tcpLoc * uToolInv, currChain->defaultPosture_, hEntity );
}


inline DNBBasicRobot3D::RequestStatus
DNBBasicRobot3D::setTCPLocation( const DNBXform3D &tcpLoc,
                                 Posture posture,
				 DNBBasicEntity3D::Handle hEntity )
    DNB_THROW_SPEC((scl_bad_alloc, DNBException))
{
#ifndef CNEXT_CLIENT 
    cerr << "DNBBasicRobot3D::setTCPLocation will be deprecated; use setArmDisplacement." << endl;
#endif
    if ( !currChain )
		return RequestRejected;
	if( currChain->TCPType_ == FixedTCP )
	return RequestRejected;
    DNBXform3D uToolInv( getCurrentUTool() );
    uToolInv.invert();

    return setMountLocation( tcpLoc * uToolInv, posture, hEntity );
}


inline DNBBasicRobot3D::RequestStatus
DNBBasicRobot3D::setMountLocation( const DNBXform3D &mountLoc,
                                   Posture posture,
				   DNBBasicEntity3D::Handle hEntity )
    DNB_THROW_SPEC((scl_bad_alloc, DNBException))
{
    DNBXform3D mountInv( getMountOffset() );
    mountInv.invert();

    return setMountPartLocation( mountLoc * mountInv, posture, hEntity );
}


inline DNBBasicRobot3D::RequestStatus
DNBBasicRobot3D::setMountPartLocation( const DNBXform3D &mountPartLoc,
                                       Posture posture,
				       DNBBasicEntity3D::Handle hEntity )
    DNB_THROW_SPEC((scl_bad_alloc, DNBException))
{
    DNBXform3D xform( getEntityLocation( getBasePart() ) );
    xform.invert();
    xform.postMultiply( getEntityLocation( hEntity ) );
    xform.postMultiply( mountPartLoc );

    return setMountPartDisplacement( xform, posture );
}


inline DNBBasicRobot3D::RequestStatus
DNBBasicRobot3D::setTCPDisplacement( const DNBXform3D &tcpDisplacement,
                                     Posture posture )
    DNB_THROW_SPEC((scl_bad_alloc, DNBException))
{
#ifndef CNEXT_CLIENT 
    cerr << "DNBBasicRobot3D::setTCPDisplacement will be deprecated; use setArmDisplacement." << endl;
#endif
    if ( !currChain )
		return RequestRejected;
	if( currChain->TCPType_ == FixedTCP )
	return RequestRejected;
    DNBXform3D uTool_inv( currChain->uToolOffset_ );
    uTool_inv.invert( );

    return setMountDisplacement( tcpDisplacement * uTool_inv, posture );
}


inline DNBBasicRobot3D::RequestStatus
DNBBasicRobot3D::setMountDisplacement( const DNBXform3D &mountDisplacement,
                                       Posture posture )
    DNB_THROW_SPEC((scl_bad_alloc, DNBException))
{
    DNBXform3D mount_inv( currDeviceKinChain->mountOffset_ );
    mount_inv.invert( );

    return setMountPartDisplacement( mountDisplacement * mount_inv, posture );
}


inline DNBBasicRobot3D::RequestStatus
DNBBasicRobot3D::setTCPDisplacement( const DNBXform3D &tcpDisplacement )
    DNB_THROW_SPEC((scl_bad_alloc, DNBException))
{
#ifndef CNEXT_CLIENT 
    cerr << "DNBBasicRobot3D::setTCPDisplacement will be deprecated; use setArmDisplacement." << endl;
#endif
    if ( !currChain )
		return RequestRejected;
	if( currChain->TCPType_ == FixedTCP )
	return RequestRejected;
    return setTCPDisplacement( tcpDisplacement, currChain->defaultPosture_ );
}


inline DNBBasicRobot3D::RequestStatus
DNBBasicRobot3D::setMountDisplacement( const DNBXform3D &mountDisplacement )
    DNB_THROW_SPEC((scl_bad_alloc, DNBException))
{
    if ( !currChain )
		return RequestRejected;
	return setMountDisplacement( mountDisplacement, currChain->defaultPosture_ );
}


inline DNBBasicRobot3D::RequestStatus
DNBBasicRobot3D::setMountPartDisplacement( const DNBXform3D &target )
    DNB_THROW_SPEC((scl_bad_alloc, DNBException))
{
    if ( !currChain )
		return RequestRejected;
	return setMountPartDisplacement( target, currChain->defaultPosture_ );
}


inline DNBXform3D
DNBBasicRobot3D::getTCPLocation( DNBBasicEntity3D::Handle hEntity ) const
    DNB_THROW_SPEC_NULL
{
    if ( !currChain )
		return DNBXform3D::Identity;
	if( currChain->TCPType_ == FixedTCP )
    { 
	if( !hEntity )
	    return currChain->uToolOffset_;
	else
	{
            DNBXform3D xform( getEntityLocation( hEntity ) );
            xform.invert();
	    return xform*(currChain->uToolOffset_);
	}


    }
    return getMountLocation( hEntity ) * getCurrentUTool();
}

inline DNBXform3D
DNBBasicRobot3D::getTCPDisplacement( ) const
    DNB_THROW_SPEC_NULL
{
    if ( !currChain )
		return DNBXform3D::Identity;
	if( currChain->TCPType_ == FixedTCP )
    {
        DNBXform3D xform( getEntityLocation( currDeviceKinChain->hBasePart_ ) );
        xform.invert();
	return xform*(currChain->uToolOffset_);
    }
    return getMountDisplacement() * getCurrentUTool();
}


inline void 
DNBBasicRobot3D::setTurnMode( DNBBasicRobot3D::TurnMode turnMode )  
    DNB_THROW_SPEC_NULL
{
    turnMode_ = turnMode;
    turnBits_ = 0;
}


inline DNBBasicRobot3D::TurnMode 
DNBBasicRobot3D::getTurnMode( ) const 
    DNB_THROW_SPEC_NULL
{
    return turnMode_;
}

inline  void
DNBBasicRobot3D::disableTurnMode( ) 
    DNB_THROW_SPEC_NULL
{
    turnMode_ = DNBSolutionAngle;
    turnBits_ = 0; 
}



inline bool 
DNBBasicRobot3D::isTurnNumberEnabled( size_t dofIndex ) const
    DNB_THROW_SPEC_NULL
{
    return ( turnMode_ != DNBTurnNumber ? false : turnBits_  & ( 1 << dofIndex ) );
}


inline bool 
DNBBasicRobot3D::isTurnSignEnabled( size_t dofIndex ) const
    DNB_THROW_SPEC_NULL
{
    return ( turnMode_ != DNBTurnSign ? false : turnBits_  & ( 1 << dofIndex ) );
}


inline    void
DNBBasicRobot3D::setConfigName( const Posture& posture, const scl_wstring& name ) 
	DNB_THROW_SPEC_NULL
{
    setPostureName( posture, name );
}

inline    void
DNBBasicRobot3D::setConfigNames( const PostureNameDB& names ) 
	DNB_THROW_SPEC_NULL
{
    setPostureNames( names );
}

inline  scl_wstring  
DNBBasicRobot3D::getConfigName( const Posture& posture ) const
	DNB_THROW_SPEC_NULL
{
    return getPostureName( posture );
}

inline    void  
DNBBasicRobot3D::getConfigNames( PostureNameDB& names ) const
	DNB_THROW_SPEC((scl_bad_alloc))
{
    getPostureNames( names );
}

inline    void 
DNBBasicRobot3D::getConfigs( PostureDB& postures ) const
	DNB_THROW_SPEC((scl_bad_alloc))
{
    getPostures( postures );
}

inline    void
DNBBasicRobot3D::setConfigFeasibility( const Posture& posture, bool feasible ) 
	DNB_THROW_SPEC_NULL
{
    setPostureFeasibility( posture, feasible );
}

inline    bool 
DNBBasicRobot3D::isConfigFeasible( const Posture& posture ) const 
	DNB_THROW_SPEC_NULL
{
    return isPostureFeasible( posture );
}

inline void
DNBBasicRobot3D::setDefaultConfig( Posture posture )
	DNB_THROW_SPEC_NULL
{
    setDefaultPosture( posture );

}

inline DNBBasicRobot3D::Posture
DNBBasicRobot3D::getDefaultConfig( ) const
	DNB_THROW_SPEC_NULL
{
    return getDefaultPosture();

}

inline bool
DNBBasicRobot3D::getCurrentConfig( Posture& posture )
	DNB_THROW_SPEC((scl_bad_alloc, DNBException))
{
    return getCurrentPosture( posture );
}

inline void
DNBBasicRobot3D::setTCPType( TCPType type )
     DNB_THROW_SPEC_NULL
{
    if ( !currChain )
		return;
	currChain->TCPType_ = type;

}

inline DNBBasicRobot3D::TCPType
DNBBasicRobot3D::getTCPType( ) const
     DNB_THROW_SPEC_NULL
{
    if ( !currChain )
		return RobotTCP;
	return currChain->TCPType_;
}


inline void
DNBBasicRobot3D::setArmDOFCount( size_t armDOFCount )
    DNB_THROW_SPEC_NULL
{
    if ( !currChain )
		return;
	currChain->armDOFCount_ = armDOFCount;
}


inline size_t
DNBBasicRobot3D::getArmDOFCount() const
    DNB_THROW_SPEC_NULL
{
    if ( !currChain )
		return 0;
	return currChain->armDOFCount_;
}


inline void
DNBBasicRobot3D::disableErrorFeedback ()
    DNB_THROW_SPEC_NULL
{
    errorFeedbackEnabled_ = false;
}


inline void
DNBBasicRobot3D::enableErrorFeedback ()
    DNB_THROW_SPEC_NULL
{
    errorFeedbackEnabled_ = true;
}


inline bool
DNBBasicRobot3D::isRRSInvKinSolverActive() const
    DNB_THROW_SPEC_NULL
{
    if ( !currChain )
		return false;
	return (currChain->pInvSolver_ == currChain->pRRSInvSolver_);
}


inline DNBBasicRobot3D::SignatureCalib
DNBBasicRobot3D::getSigType() const
    DNB_THROW_SPEC_NULL
{
	if ( !currChain )
        return SignatureCalib_None;
	return currChain->kinParams_sig_type_;
}

inline void
DNBBasicRobot3D::setCurrentToolProfileName( const scl_wstring &name )
    DNB_THROW_SPEC_NULL
{
    int zz=0;
	for ( zz=0; zz<kinChainsCount_; zz++ )
	{
		ToolProfileNamesDB::iterator iter = kinChainDB_[zz].toolProfileNamesDB_.begin();
		ToolProfileNamesDB::iterator end = kinChainDB_[zz].toolProfileNamesDB_.end();

		for ( ; iter!= end; iter++ )
		{
			if ( (*iter) == name )
			{
				currentToolProfileName_ = name;
				setActiveKinChain( &kinChainDB_[zz] );
				return;
			}
		}		
	}
} 

inline void
DNBBasicRobot3D::addChainProfileName( const scl_wstring &wname )
	DNB_THROW_SPEC_NULL
{
	//This sets the tool profile name for the active kin chain
	//Call setActiveKinChain(int) to set active kin chain before
	currChain->toolProfileNamesDB_.push_back( wname );	
}

inline scl_wstring
DNBBasicRobot3D::getCurrentToolProfileName() const
    DNB_THROW_SPEC_NULL
{
    return currentToolProfileName_;
}
