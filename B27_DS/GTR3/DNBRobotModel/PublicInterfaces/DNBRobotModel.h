// COPYRIGHT Dassault Systemes 2005
/** @CAA2Required */
/*---------------------------------------------------------------------*/
/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */
/*---------------------------------------------------------------------*/
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_ROBOTMODEL_H_
#define _DNB_ROBOTMODEL_H_


#ifdef  _WINDOWS_SOURCE
#ifdef  __DNBRobotModel
#define ExportedByDNBRobotModel    __declspec(dllexport)
#else
#define ExportedByDNBRobotModel    __declspec(dllimport)
#endif
#else
#define ExportedByDNBRobotModel    /* nothing */
#endif


#endif  /* _DNB_ROBOTMODEL_H_ */
