// COPYRIGHT Dassault Systemes 2005
/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

/**
 * @fullreview RTL/czo 02:01:02
 */



inline void 
DNBCartesianPlannerExt::addTagPoint(DNBBasicTagPoint3D::Handle hTagPoint)
    DNB_THROW_SPEC((DNBEDomainError)) 
{


	currentCartesianPlanner->addTagPoint(hTagPoint);
}


inline void 
DNBCartesianPlannerExt::addPath(DNBBasicPath3D::Handle hPath_)
    DNB_THROW_SPEC((DNBEDomainError)) 
{
	

	currentCartesianPlanner->addPath(hPath_);
}


inline void DNBCartesianPlannerExt::clearPath( )
	DNB_THROW_SPEC_NULL
{
	
	currentCartesianPlanner->clearPath();
}

inline DNBBasicPath3D::Handle
DNBCartesianPlannerExt::getPath()
	DNB_THROW_SPEC_NULL
{
	return currentCartesianPlanner->getPath();
}

inline void 
DNBCartesianPlannerExt::addDOFTarget(DNBBasicDOFTarget::Handle hTarget)
    DNB_THROW_SPEC((DNBEDomainError))
{
	currentCartesianPlanner->addDOFTarget(hTarget);
}

inline void 
DNBCartesianPlannerExt::clearSetTargets( )
	DNB_THROW_SPEC_NULL
{
	currentCartesianPlanner->clearPath();
}


inline void 
DNBCartesianPlannerExt::reset()
		DNB_THROW_SPEC_NULL
{
	currentCartesianPlanner->reset();
}


inline void 
DNBCartesianPlannerExt::resetProcess()
	DNB_THROW_SPEC_NULL
{
	currentCartesianPlanner->resetProcess();
}


inline DNBSimTime 
DNBCartesianPlannerExt::getSegmentTime(size_t i)
		DNB_THROW_SPEC_NULL
{
	return currentCartesianPlanner->getSegmentTime(i);
}

inline void 
DNBCartesianPlannerExt::setSamplePeriod(DNBSimTime step)
	DNB_THROW_SPEC_NULL
{
	currentCartesianPlanner->setSamplePeriod(step);
}

inline void 
DNBCartesianPlannerExt::addCartesianPlanner(DNBCartesianPlannerBase* newCartesianPlanner)
	DNB_THROW_SPEC_NULL
{
	if(newCartesianPlanner)
		cartesianPlanners_.push_back(newCartesianPlanner);
}

inline bool 
DNBCartesianPlannerExt::reachedEndOfMove ()
	DNB_THROW_SPEC_NULL
{
	return currentCartesianPlanner->reachedEndOfMove();
}

inline void 
DNBCartesianPlannerExt::getCurrentPlannerName(scl_wstring&name)
	DNB_THROW_SPEC_NULL
{
	name=currentCartesianPlanner->getPlannerName();
}


inline void
DNBCartesianPlannerExt::setTCPPlannerPeriod( DNBSimTime period ) 
	DNB_THROW_SPEC((DNBERangeError))
{
	currentCartesianPlanner->setTCPPlannerPeriod( period );
}

inline DNBSimTime
DNBCartesianPlannerExt::getTCPPlannerPeriod( ) const
	DNB_THROW_SPEC_NULL
{ 
    return currentCartesianPlanner->getTCPPlannerPeriod( ); 
}

inline void
DNBCartesianPlannerExt::setTCPMinMotionTime( DNBSimTime time ) 
	DNB_THROW_SPEC((DNBERangeError))
{ 

    if( time < 0.0 )
    {

        DNBERangeError    error( DNB_FORMAT("Negative TCP minimum motion planner period "));
        throw error;

    }

    TCPMinMotionTime_ = time;
}

inline DNBSimTime
DNBCartesianPlannerExt::getTCPMinMotionTime( ) const
	DNB_THROW_SPEC_NULL
{
    return TCPMinMotionTime_;
}

inline void
DNBCartesianPlannerExt::setDefaultTCPMotionType( MotionType TCPMotion )
	DNB_THROW_SPEC_NULL
{
    DefaultTCPMotionType_ = TCPMotion;
}

inline DNBCartesianPlannerExt::MotionType
DNBCartesianPlannerExt::getDefaultTCPMotionType( ) const
	DNB_THROW_SPEC_NULL
{
    return DefaultTCPMotionType_;
}

inline void
DNBCartesianPlannerExt::setCurrentTCPMotionType( MotionType TCPMotion )
	DNB_THROW_SPEC_NULL

{
   CurrentTCPMotionType_ = TCPMotion;
}

inline DNBCartesianPlannerExt::MotionType
DNBCartesianPlannerExt::getCurrentTCPMotionType( ) const
	DNB_THROW_SPEC_NULL

{
    return CurrentTCPMotionType_;
}

inline void
DNBCartesianPlannerExt::resetCurrentTCPMotionType( )
	DNB_THROW_SPEC_NULL

{
    CurrentTCPMotionType_ = DefaultTCPMotionType_;

}

inline void
DNBCartesianPlannerExt::setDefaultTCPSpeedMode( TCPSpeedMode speedMode )
	DNB_THROW_SPEC_NULL
{
    DefaultTCPSpeedMode_ = speedMode;
}

inline DNBCartesianPlannerExt::TCPSpeedMode 
DNBCartesianPlannerExt::getDefaultTCPSpeedMode( ) const
	DNB_THROW_SPEC_NULL
{
    return DefaultTCPSpeedMode_;

}

inline void
DNBCartesianPlannerExt::setCurrentTCPSpeedMode( TCPSpeedMode speedMode )
	DNB_THROW_SPEC_NULL

{
    CurrentTCPSpeedMode_ = speedMode;
}

inline DNBCartesianPlannerExt::TCPSpeedMode 
DNBCartesianPlannerExt::getCurrentTCPSpeedMode( ) const
	DNB_THROW_SPEC_NULL

{
    return CurrentTCPSpeedMode_;
}

inline void
DNBCartesianPlannerExt::resetCurrentTCPSpeedMode( )
	DNB_THROW_SPEC_NULL

{
    CurrentTCPSpeedMode_ = DefaultTCPSpeedMode_;
}



inline void
DNBCartesianPlannerExt::setDefaultTCPSpeed( DNBReal speed )
	DNB_THROW_SPEC_NULL
{
    currentCartesianPlanner->setDefaultTCPSpeed( speed);
}

inline DNBReal 
DNBCartesianPlannerExt::getDefaultTCPSpeed( ) const
	DNB_THROW_SPEC_NULL
{
    return currentCartesianPlanner->getDefaultTCPSpeed();
}

inline void
DNBCartesianPlannerExt::setCurrentTCPSpeed( DNBReal speed )
	DNB_THROW_SPEC_NULL

{
    CurrentTCPSpeed_ = speed;
}

inline DNBReal 
DNBCartesianPlannerExt::getCurrentTCPSpeed( ) const
	DNB_THROW_SPEC_NULL

{
    return currentCartesianPlanner->getCurrentTCPSpeed();
}

inline void
DNBCartesianPlannerExt::resetCurrentTCPSpeed( )
	DNB_THROW_SPEC_NULL

{
    CurrentTCPSpeed_ = currentCartesianPlanner->getDefaultTCPSpeed();
}

inline void
DNBCartesianPlannerExt::setDefaultTCPAccelMode( TCPAccelMode accelMode )
	DNB_THROW_SPEC_NULL
{
    DefaultTCPAccelMode_ = accelMode;
}

inline DNBCartesianPlannerExt::TCPAccelMode 
DNBCartesianPlannerExt::getDefaultTCPAccelMode( ) const
	DNB_THROW_SPEC_NULL
{
    return DefaultTCPAccelMode_;
}

inline void
DNBCartesianPlannerExt::setCurrentTCPAccelMode( TCPAccelMode accelMode )
	DNB_THROW_SPEC_NULL

{
    CurrentTCPAccelMode_ = accelMode;
}

inline DNBCartesianPlannerExt::TCPAccelMode 
DNBCartesianPlannerExt::getCurrentTCPAccelMode( ) const
	DNB_THROW_SPEC_NULL

{
    return CurrentTCPAccelMode_;
}

inline void
DNBCartesianPlannerExt::resetCurrentTCPAccelMode( )
	DNB_THROW_SPEC_NULL

{
    CurrentTCPAccelMode_= DefaultTCPAccelMode_;
}



inline void
DNBCartesianPlannerExt::setDefaultTCPAccel( DNBReal accel )
	DNB_THROW_SPEC_NULL
{
    currentCartesianPlanner-> setDefaultTCPAccel(accel);
}

inline DNBReal 
DNBCartesianPlannerExt::getDefaultTCPAccel( ) const
	DNB_THROW_SPEC_NULL
{
    return currentCartesianPlanner->getDefaultTCPAccel(); 
}



inline void
DNBCartesianPlannerExt::setCurrentTCPAccel( DNBReal accel )
	DNB_THROW_SPEC_NULL

{
    CurrentTCPAccel_ = accel;
}

inline DNBReal 
DNBCartesianPlannerExt::getCurrentTCPAccel( ) const
	DNB_THROW_SPEC_NULL

{
    return CurrentTCPAccel_;
}

inline void
DNBCartesianPlannerExt::resetCurrenttTCPAccel( )
	DNB_THROW_SPEC_NULL

{
    CurrentTCPAccel_ = currentCartesianPlanner->getDefaultTCPAccel();
}



inline void
DNBCartesianPlannerExt::setDefaultTCPTermType( TCPTermType termType )
	DNB_THROW_SPEC_NULL
{
    DefaultTCPTermType_= termType;

}

inline DNBCartesianPlannerExt::TCPTermType 
DNBCartesianPlannerExt::getDefaultTCPTermType( ) const
	DNB_THROW_SPEC_NULL
{
    return DefaultTCPTermType_;
}

inline void
DNBCartesianPlannerExt::setDefaultTCPCornerMode( TCPCornerMode cornerMode )
	DNB_THROW_SPEC_NULL
{
    DefaultTCPCornerMode_ = cornerMode;
}

inline DNBCartesianPlannerExt::TCPCornerMode
DNBCartesianPlannerExt::getDefaultTCPCornerMode( ) const
	DNB_THROW_SPEC_NULL
{
    return DefaultTCPCornerMode_;
}

inline void
DNBCartesianPlannerExt::setDefaultTCPRounding( DNBReal TCPRounding )
	DNB_THROW_SPEC((DNBERangeError))
{
    if( TCPRounding < 0.0 || TCPRounding > 1.0 )
    {
        DNBERangeError    error( DNB_FORMAT("Invalid TCP rounding."));
        throw error;

    }
    DefaultTCPRounding_ = TCPRounding;
}

inline DNBReal 
DNBCartesianPlannerExt::getDefaultTCPRounding( ) const
	DNB_THROW_SPEC_NULL
{
    return DefaultTCPRounding_;
}



inline void
DNBCartesianPlannerExt::setCurrentTCPTermType( TCPTermType termType )
	DNB_THROW_SPEC_NULL

{
    CurrentTCPTermType_= termType;
}

inline DNBCartesianPlannerExt::TCPTermType 
DNBCartesianPlannerExt::getCurrentTCPTermType( ) const
	DNB_THROW_SPEC_NULL

{
    return CurrentTCPTermType_;
}

inline void
DNBCartesianPlannerExt::resetCurrentTCPTermType( )
	DNB_THROW_SPEC_NULL

{
    CurrentTCPTermType_= DefaultTCPTermType_;
}

inline void
DNBCartesianPlannerExt::setCurrentTCPCornerMode( TCPCornerMode cornerMode )
	DNB_THROW_SPEC_NULL

{
    CurrentTCPCornerMode_ = cornerMode;
}

inline DNBCartesianPlannerExt::TCPCornerMode
DNBCartesianPlannerExt::getCurrentTCPCornerMode( ) const
	DNB_THROW_SPEC_NULL

{
    return CurrentTCPCornerMode_;
}

inline void
DNBCartesianPlannerExt::resetCurrentTCPCornerMode( )
	DNB_THROW_SPEC_NULL

{
    CurrentTCPCornerMode_ = DefaultTCPCornerMode_;
}

inline void
DNBCartesianPlannerExt::setCurrentTCPRounding( DNBReal TCPRounding )
	DNB_THROW_SPEC((DNBERangeError))

{
   if( TCPRounding < 0.0 || TCPRounding > 1.0 )
   {
       DNBERangeError    error( DNB_FORMAT("Invalid TCP rounding."));
       throw error;

   }
   CurrentTCPRounding_ = TCPRounding; 
}

inline DNBReal 
DNBCartesianPlannerExt::getCurrentTCPRounding( ) const
	DNB_THROW_SPEC_NULL

{
    return CurrentTCPRounding_;
}

inline void
DNBCartesianPlannerExt::resetCurrentTCPRounding( )
	DNB_THROW_SPEC_NULL

{
    CurrentTCPRounding_ = DefaultTCPRounding_;
}

inline void
DNBCartesianPlannerExt::setDefaultApproachAxis( DNBMath3D::AxisType axis )
	DNB_THROW_SPEC_NULL
{
    DefaultApproachAxis_ = axis;
}

inline DNBMath3D::AxisType 
DNBCartesianPlannerExt::getDefaultApproachAxis( ) const
	DNB_THROW_SPEC_NULL
{
    return DefaultApproachAxis_;
}

inline void
DNBCartesianPlannerExt::setDefaultApproachDirection( ApproachDirection approach )
	DNB_THROW_SPEC_NULL
{
    DefaultApproachDirection_ = approach;
}


inline DNBCartesianPlannerExt::ApproachDirection 
DNBCartesianPlannerExt::getDefaultApproachDirection( ) const
	DNB_THROW_SPEC_NULL
{
    return DefaultApproachDirection_;
}

inline void
DNBCartesianPlannerExt::setDefaultTCPDistance( DNBReal TCPDistance )
	DNB_THROW_SPEC_NULL
{
    DefaultTCPDistance_ = TCPDistance;
}

inline DNBReal 
DNBCartesianPlannerExt::getDefaultTCPDistance( ) const
	DNB_THROW_SPEC_NULL
{
    return DefaultTCPDistance_;
}

inline void
DNBCartesianPlannerExt::setDefaultTCPOrientMode( TCPOrientMode orientMode )
	DNB_THROW_SPEC_NULL
{
    DefaultTCPOrientMode_ = orientMode;
}

inline DNBCartesianPlannerExt::TCPOrientMode 
DNBCartesianPlannerExt::getDefaultTCPOrientMode( ) const
	DNB_THROW_SPEC_NULL
{
    return DefaultTCPOrientMode_;
}

inline void
DNBCartesianPlannerExt::setCurrentTCPApproachAxis( DNBMath3D::AxisType axis )
	DNB_THROW_SPEC_NULL

{
    CurrentApproachAxis_ = axis;
}

inline DNBMath3D::AxisType 
DNBCartesianPlannerExt::getCurrentTCPApproachAxis( ) const
	DNB_THROW_SPEC_NULL

{
    return CurrentApproachAxis_;
}
inline void
DNBCartesianPlannerExt::resetCurrentTCPApproachAxis( )
	DNB_THROW_SPEC_NULL

{
    CurrentApproachAxis_ = DefaultApproachAxis_; 
}



inline void
DNBCartesianPlannerExt::setCurrentTCPApproachDirection( ApproachDirection approach )
	DNB_THROW_SPEC_NULL

{
    CurrentApproachDirection_ = approach;
}

inline DNBCartesianPlannerExt::ApproachDirection 
DNBCartesianPlannerExt::getCurrentTCPApproachDirection( ) const
	DNB_THROW_SPEC_NULL

{
    return CurrentApproachDirection_;
}

inline void
DNBCartesianPlannerExt::resetCurrentTCPApproachDirection( )
	DNB_THROW_SPEC_NULL

{
    CurrentApproachDirection_ = DefaultApproachDirection_;
}


inline void
DNBCartesianPlannerExt::setCurrentTCPDistance( DNBReal TCPDistance )
	DNB_THROW_SPEC_NULL

{
    CurrentTCPDistance_ = TCPDistance;
}

inline DNBReal 
DNBCartesianPlannerExt::getCurrentTCPDistance( ) const
	DNB_THROW_SPEC_NULL
    
{
    return CurrentTCPDistance_;
}

inline void
DNBCartesianPlannerExt::resetCurrentTCPDistance( )
	DNB_THROW_SPEC_NULL

{
    CurrentTCPDistance_ = DefaultTCPDistance_;
}

inline void
DNBCartesianPlannerExt::setCurrentTCPOrientMode( TCPOrientMode orientMode )
	DNB_THROW_SPEC_NULL

{
    CurrentTCPOrientMode_ = orientMode;
}

inline DNBCartesianPlannerBase::TCPOrientMode 
DNBCartesianPlannerExt::getCurrentTCPOrientMode( ) const
	DNB_THROW_SPEC_NULL

{
    return CurrentTCPOrientMode_;
}

inline void
DNBCartesianPlannerExt::resetCurrentTCPOrientMode( )
	DNB_THROW_SPEC_NULL

{
    CurrentTCPOrientMode_ = DefaultTCPOrientMode_ ;
}


inline void
DNBCartesianPlannerExt::setDefaultTCPJointMode( TCPJointMode jointMode )
	DNB_THROW_SPEC_NULL
{
    DefaultTCPJointMode_ = jointMode;
}

inline DNBCartesianPlannerBase::TCPJointMode 
DNBCartesianPlannerExt::getDefaultTCPJointMode( )
	DNB_THROW_SPEC_NULL
{
    return DefaultTCPJointMode_;
}




inline void
DNBCartesianPlannerExt::setCurrentTCPJointMode( TCPJointMode jointMode )
	DNB_THROW_SPEC_NULL

{
    CurrentTCPJointMode_= jointMode;
}

inline DNBCartesianPlannerBase::TCPJointMode 
DNBCartesianPlannerExt::getCurrentTCPJointMode( )
	DNB_THROW_SPEC_NULL

{
    return CurrentTCPJointMode_; 
}

inline void
DNBCartesianPlannerExt::resetCurrentTCPJointMode( )
	DNB_THROW_SPEC_NULL

{
    CurrentTCPJointMode_ = DefaultTCPJointMode_;
}
