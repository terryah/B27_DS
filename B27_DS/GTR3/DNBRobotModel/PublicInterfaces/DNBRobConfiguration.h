// COPYRIGHT Dassault Systemes 2005
////////////////////////////////////////////////////////////////////////////
//
//
//    Example:
//
//	 // Somewhere in the motion planner code.
//
//
//        DNBBasicRobot3D::Pointer pRobot(hRobot);^M
//	  pRobot->setTurnMode( DNBBasicRobot3D::DNBTurnNumber );
//	  //
//	  // Enable turn numbers for all DOF's.
//	  //
//        pRobot->enableTurnNumbers( 63 );
//	  // 
//	  // create configuration with default posture.
//	  //
//        DNBRobConfiguration hConfig = pRobot->createRobConfiguration();
//	  DNBBasicRobot3D::Posture posture = 5;
//	  //
//	  // set configuration posture.
//	  //
//        hConfig.setPosture( posture );
//
//        hConfig.setTurnNumber( 0, 2 );
//        hConfig.setTurnNumber( 1, 3 );
//        hConfig.setTurnNumber( 2, 4 );
//        hConfig.setTurnNumber( 3, 5 );
//        hConfig.setTurnNumber( 4, 6 );
//        hConfig.setTurnNumber( 5, 7 );
//
//        pRobot->doTCPInverse( p2, hConfig, sols,  states );^M
//
////////////////////////////////////////////////////////////////////////////  



#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNBROBCONFIGURATION_H_
#define _DNBROBCONFIGURATION_H_

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */


#include <DNBSystemBase.h>
#include <scl_vector.h>
#include <scl_climits.h>
#include <DNBSystemDefs.h>



#include <DNBRobotModel.h>

#include <DNBDynamicObject.h>
#include <DNBBasicRobot3D.h>


class ExportedByDNBRobotModel DNBRobConfiguration : public DNBDynamicObject
{
    DNB_DECLARE_DYNAMIC_RTTI( DNBRobConfiguration );


public:

    typedef DNBBasicRobot3D::Posture			       Posture;
    typedef DNBBasicRobot3D::TurnSign			       TurnSign;
    typedef scl_pair < size_t, int >			 	       TurnValue;
    typedef scl_vector< TurnValue, DNB_ALLOCATOR( TurnValue ) >    TurnValueDB;
  

    DNBBasicRobot3D::Handle 
    getRobot( ) const
        DNB_THROW_SPEC_NULL;

    //
    // throws exception if Robot does not support given posture
    //
    void
    setPosture( Posture posture )
        DNB_THROW_SPEC(( DNBENotSupported ));

    void
    getPosture( Posture& posture ) const
        DNB_THROW_SPEC_NULL;

    //
    // throws exception if Robot does not support posture 
    // by given name
    //
    void
    setPosture( const scl_wstring& name )
        DNB_THROW_SPEC(( DNBENotSupported ));

    void
    getPosture( scl_wstring& name ) const
        DNB_THROW_SPEC_NULL;

    //
    // throws exception if turn number not supported by Robot. 
    // throws exception if turn number for given DOF not supported.
    //
    void
    setTurnNumber( size_t dofIndex, int turnNumber )
        DNB_THROW_SPEC(( DNBENotSupported ));

    //
    // returns turn number if turn number supported. 
    // returns value 0 if turn number not enabled. 
    //
    int 
    getTurnNumber( size_t dofIndex ) const
        DNB_THROW_SPEC_NULL;

    //
    // throws exception if turn sign not supported by Robot 
    // throws exception if turn sign for given DOF not supported.
    //
    void
    setTurnSign( size_t dofIndex, TurnSign turnSign )
        DNB_THROW_SPEC(( DNBENotSupported ));

    //
    // returns turn sign if turn sign supported. 
    // returns DNBSignSolutionAngle if turn sign not enabled. 
    //
    TurnSign 
    getTurnSign( size_t dofIndex ) const
        DNB_THROW_SPEC_NULL;

    void
    getTurnValues( TurnValueDB& gTurn ) const
        DNB_THROW_SPEC((scl_bad_alloc));

    DNBBasicRobot3D::TurnMode
    getTurnMode( ) const
        DNB_THROW_SPEC_NULL;

    //
    // copy constructor
    //
    DNBRobConfiguration( const DNBRobConfiguration& other, CopyMode mode )
	DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit, DNBENetworkError));

    virtual
    ~DNBRobConfiguration( )
	DNB_THROW_SPEC_NULL;


    bool 
    operator==( const DNBRobConfiguration &right ) const
	DNB_THROW_SPEC_NULL;

private:

    DNBRobConfiguration( DNBBasicRobot3D::Handle hRobot )
	DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit, DNBENetworkError));
    

    bool
    findTurn( size_t dofIndex, size_t& idx  )
        DNB_THROW_SPEC_NULL;

    bool
    findTurn( size_t dofIndex, size_t& idx  ) const
        DNB_THROW_SPEC_NULL;

    DNBBasicRobot3D::Handle 			hRobot_;
    Posture					posture_;
    scl_wstring					postureName_;
    TurnValueDB					turnValueDB_;
    friend 	   		class		DNBBasicRobot3D;    

};




#endif /* _DNBROBCONFIGURATION_H_ */
