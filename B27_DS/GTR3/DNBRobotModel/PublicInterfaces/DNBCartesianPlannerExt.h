// COPYRIGHT Dassault Systemes 2005
//*
//* FILE:
//*     DNBCartesianPlannerExt.h      - public header file
//*
//* MODULE:
//*     DNBCartesianPlannerExt  - single non-template class
//*
//* OVERVIEW:
//*     This module defines an extension class which is base for all
//*     motion planner classes.
//*	 
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     xin         06/05/00    Initial implementation
//*		xin			12/06/00
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1998 - 2000 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*
//  Jun 2006  Change micro from DNB_THROW_SPEC_NULL to DNB_THROW_SPEC_ANY LYN 
//*
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_CARTESIANPLANNEREXT_H_
#define _DNB_CARTESIANPLANNEREXT_H_

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */


#include <DNBSystemBase.h>
#include <scl_vector.h>
#include <DNBSystemDefs.h>


#include <DNBMotionPlannerExt.h>

#include <DNBCartesianPlannerBase.h>

class ExportedByDNBRobotModel DNBCartesianPlannerExt : public DNBMotionPlannerExt
{

    DNB_DECLARE_DYNAMIC_CREATE( DNBCartesianPlannerExt );

    DNB_DECLARE_SHARED_OBJECT( DNBCartesianPlannerExt );

    DNB_DECLARE_EXTENSION_OBJECT( DNBCartesianPlannerExt, DNBBasicRobot3D );

    DNB_DECLARE_EXTENSION_FACTORY( DNBCartesianPlannerExt, DNBBasicRobot3D );


public:

	inline void 
	addTagPoint(DNBBasicTagPoint3D::Handle hTagPoint)
    DNB_THROW_SPEC((DNBEDomainError)); 

	inline void 
	addPath(DNBBasicPath3D::Handle hPath)
    DNB_THROW_SPEC((DNBEDomainError)); 

    inline void 
	clearPath( )
	DNB_THROW_SPEC_NULL;

	inline void 
	addDOFTarget(DNBBasicDOFTarget::Handle hTarget)
    DNB_THROW_SPEC((DNBEDomainError)); 

    inline void clearSetTargets( )
	DNB_THROW_SPEC_NULL;

	DNBMathVector 
	evaluate( const DNBSimTime &currentTime )
	DNB_THROW_SPEC_ANY;

	DNBMathVector 
	evaluate( )
	DNB_THROW_SPEC_ANY;
  
	inline void 
	reset()
		DNB_THROW_SPEC_NULL;

	DNBSimTime 
	getMoveTime()
		DNB_THROW_SPEC_ANY;
	inline DNBSimTime 
	getSegmentTime(size_t i)
		DNB_THROW_SPEC_NULL;

	inline void 
	setSamplePeriod(DNBSimTime step)
	DNB_THROW_SPEC_NULL;

	
	inline void 
	resetProcess(void)
	DNB_THROW_SPEC_NULL;

	inline void 
	addCartesianPlanner(DNBCartesianPlannerBase* newCartesianPlanner)
	DNB_THROW_SPEC_NULL;

	void 
	removeCartesianPlanner(const scl_wstring& name)
	DNB_THROW_SPEC_NULL;

	bool setCurrentPlanner(const scl_wstring&)
	DNB_THROW_SPEC_NULL;

	inline void getCurrentPlannerName (scl_wstring& name)
	DNB_THROW_SPEC_NULL;

	inline DNBBasicPath3D::Handle
	getPath()
	DNB_THROW_SPEC_NULL;

	inline bool reachedEndOfMove ()
	DNB_THROW_SPEC_NULL;


    enum TCPSpeedMode { 
		          TCPSpeedAbsolute,
			  TCPSpeedPercent 
		      };
    enum TCPAccelMode { 
		          TCPAccelAbsolute,
			  TCPAccelPercent 
		      };
    enum TCPTermType { 
		         TCPTermDecel, 
		         TCPTermDecelRounding 
		     };
    enum TCPCornerMode { 
		           TCPCornerVelocity,
			   TCPCornerDistance 
		       };
    enum ApproachDirection { 
		               ApproachIn, 
			       ApproachOut 
			   };

	typedef DNBCartesianPlannerBase::TCPOrientMode  TCPOrientMode;
	typedef DNBCartesianPlannerBase::TCPJointMode	TCPJointMode;
//	typedef DNBMotionPlannerBase::MotionType MotionType;
	enum MotionType
	{
		JOINT,
		STRAIGHT,
		CIRCULAR, 
		CIRCULARVIA,
		SLEW
	};


    inline void
    setTCPPlannerPeriod( DNBSimTime period ) 
	DNB_THROW_SPEC((DNBERangeError));

    inline DNBSimTime
    getTCPPlannerPeriod( ) const
	DNB_THROW_SPEC_NULL;

    inline void
    setTCPMinMotionTime( DNBSimTime time ) 
	DNB_THROW_SPEC((DNBERangeError));

    inline DNBSimTime
    getTCPMinMotionTime( ) const
	DNB_THROW_SPEC_NULL;

    inline void
    setDefaultTCPMotionType( MotionType TCPmotion )
	DNB_THROW_SPEC_NULL;

    inline MotionType
    getDefaultTCPMotionType( ) const
	DNB_THROW_SPEC_NULL;

    inline void
    setCurrentTCPMotionType( MotionType TCPmotion )
	DNB_THROW_SPEC_NULL;

    inline MotionType
    getCurrentTCPMotionType( ) const
	DNB_THROW_SPEC_NULL;

    inline void
    resetCurrentTCPMotionType( )
	DNB_THROW_SPEC_NULL;

    inline void
    setDefaultTCPSpeedMode( TCPSpeedMode speedMode )
	DNB_THROW_SPEC_NULL;

    inline TCPSpeedMode 
    getDefaultTCPSpeedMode( ) const
	DNB_THROW_SPEC_NULL;

	inline void
    setCurrentTCPSpeedMode( TCPSpeedMode speedMode )
	DNB_THROW_SPEC_NULL;

    inline TCPSpeedMode 
    getCurrentTCPSpeedMode( ) const
	DNB_THROW_SPEC_NULL;

    inline void
    resetCurrentTCPSpeedMode( )
	DNB_THROW_SPEC_NULL;

    inline void
    setDefaultTCPSpeed( DNBReal speed )
	DNB_THROW_SPEC_NULL;

    inline DNBReal 
    getDefaultTCPSpeed( ) const
	DNB_THROW_SPEC_NULL;

    inline void
    setCurrentTCPSpeed( DNBReal speed )
	DNB_THROW_SPEC_NULL;

    inline DNBReal 
    getCurrentTCPSpeed( ) const
	DNB_THROW_SPEC_NULL;

    inline void
    resetCurrentTCPSpeed( )
	DNB_THROW_SPEC_NULL;


    inline void
    setDefaultTCPAccelMode( TCPAccelMode accelMode )
	DNB_THROW_SPEC_NULL;

    inline TCPAccelMode 
    getDefaultTCPAccelMode( ) const
	DNB_THROW_SPEC_NULL;

    inline void
    setCurrentTCPAccelMode( TCPAccelMode accelMode )
	DNB_THROW_SPEC_NULL;

    inline TCPAccelMode 
    getCurrentTCPAccelMode( ) const
	DNB_THROW_SPEC_NULL;

    inline void
    resetCurrentTCPAccelMode( )
	DNB_THROW_SPEC_NULL;

    inline void
    setDefaultTCPAccel( DNBReal accel )
	DNB_THROW_SPEC_NULL;

    inline DNBReal 
    getDefaultTCPAccel( ) const
	DNB_THROW_SPEC_NULL;

    inline void
    setCurrentTCPAccel( DNBReal accel )
	DNB_THROW_SPEC_NULL;

    inline DNBReal 
    getCurrentTCPAccel( ) const
	DNB_THROW_SPEC_NULL;

    inline void
    resetCurrenttTCPAccel( )
	DNB_THROW_SPEC_NULL;


    inline void
    setDefaultTCPTermType( TCPTermType termType )
	DNB_THROW_SPEC_NULL;

    inline TCPTermType 
    getDefaultTCPTermType( ) const
	DNB_THROW_SPEC_NULL;

    inline void
    setDefaultTCPCornerMode( TCPCornerMode cornerMode )
	DNB_THROW_SPEC_NULL;

    inline TCPCornerMode
    getDefaultTCPCornerMode( ) const
	DNB_THROW_SPEC_NULL;

    inline void
    setDefaultTCPRounding( DNBReal TCProunding )
	DNB_THROW_SPEC((DNBERangeError));

    inline DNBReal 
    getDefaultTCPRounding( ) const
	DNB_THROW_SPEC_NULL;
 

    inline void
    setCurrentTCPTermType( TCPTermType termType )
	DNB_THROW_SPEC_NULL;

    inline TCPTermType 
    getCurrentTCPTermType( ) const
	DNB_THROW_SPEC_NULL;

    inline void
    resetCurrentTCPTermType( )
	DNB_THROW_SPEC_NULL;

    inline void
    setCurrentTCPCornerMode( TCPCornerMode cornerMode )
	DNB_THROW_SPEC_NULL;

    inline TCPCornerMode
    getCurrentTCPCornerMode( ) const
	DNB_THROW_SPEC_NULL;

    inline void
    resetCurrentTCPCornerMode( )
	DNB_THROW_SPEC_NULL;

    inline void
    setCurrentTCPRounding( DNBReal TCProunding )
	DNB_THROW_SPEC((DNBERangeError));

    inline DNBReal 
    getCurrentTCPRounding( ) const
	DNB_THROW_SPEC_NULL;

    inline void
    resetCurrentTCPRounding( )
	DNB_THROW_SPEC_NULL;

    inline void
    setDefaultApproachAxis( DNBMath3D::AxisType axis )
	DNB_THROW_SPEC_NULL;

    inline  DNBMath3D::AxisType 
    getDefaultApproachAxis( ) const
	DNB_THROW_SPEC_NULL;

    inline void
    setDefaultApproachDirection( ApproachDirection approach )
	DNB_THROW_SPEC_NULL;

    inline ApproachDirection 
    getDefaultApproachDirection( ) const
	DNB_THROW_SPEC_NULL;

    inline void
    setDefaultTCPDistance( DNBReal TCPDistance )
	DNB_THROW_SPEC_NULL;

    inline DNBReal 
    getDefaultTCPDistance( ) const
	DNB_THROW_SPEC_NULL;
    
    inline void
    setDefaultTCPOrientMode( TCPOrientMode orientMode )
	DNB_THROW_SPEC_NULL;

    inline TCPOrientMode 
    getDefaultTCPOrientMode( ) const
	DNB_THROW_SPEC_NULL;

    inline void
    setCurrentTCPApproachAxis( DNBMath3D::AxisType axis )
	DNB_THROW_SPEC_NULL;

    inline DNBMath3D::AxisType 
    getCurrentTCPApproachAxis( ) const
	DNB_THROW_SPEC_NULL;

    inline void
    resetCurrentTCPApproachAxis( )
	DNB_THROW_SPEC_NULL;

    inline void
    setCurrentTCPApproachDirection( ApproachDirection approach )
	DNB_THROW_SPEC_NULL;

    inline ApproachDirection 
    getCurrentTCPApproachDirection( ) const
	DNB_THROW_SPEC_NULL;

    inline void
    resetCurrentTCPApproachDirection( )
	DNB_THROW_SPEC_NULL;

	inline void
    setCurrentTCPDistance( DNBReal TCPDistance )
	DNB_THROW_SPEC_NULL;

    inline DNBReal 
    getCurrentTCPDistance( ) const
	DNB_THROW_SPEC_NULL;
    
    inline void
    resetCurrentTCPDistance( )
	DNB_THROW_SPEC_NULL;

    inline void
    setCurrentTCPOrientMode( TCPOrientMode orientMode )
	DNB_THROW_SPEC_NULL;

    inline TCPOrientMode 
    getCurrentTCPOrientMode( ) const
	DNB_THROW_SPEC_NULL;

    inline void
    resetCurrentTCPOrientMode( )
	DNB_THROW_SPEC_NULL;

    inline void
    setDefaultTCPJointMode( TCPJointMode jointMode )
	DNB_THROW_SPEC_NULL;

    inline TCPJointMode 
    getDefaultTCPJointMode( )
	DNB_THROW_SPEC_NULL;

    inline void
    setCurrentTCPJointMode( TCPJointMode jointMode )
	DNB_THROW_SPEC_NULL;

    inline TCPJointMode 
    getCurrentTCPJointMode( )
	DNB_THROW_SPEC_NULL;

    inline void
    resetCurrentTCPJointMode( )
	DNB_THROW_SPEC_NULL;

protected:
    DNBCartesianPlannerExt( )
	DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit));

    DNBCartesianPlannerExt( const DNBCartesianPlannerExt &other,
			    CopyMode mode )
	DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit));

    virtual
    ~DNBCartesianPlannerExt( )
	DNB_THROW_SPEC_NULL;

	typedef scl_vector< DNBCartesianPlannerBase*,
        DNB_ALLOCATOR(DNBCartesianPlannerBase*) >    DNBCartesianPlannerDB;

private:

	DNBBasicRobot3D::Handle hRobot_;
	DNBCartesianPlannerBase* currentCartesianPlanner;
	DNBCartesianPlannerDB   cartesianPlanners_;
    DNBSimTime		   TCPMinMotionTime_;
    MotionType	   DefaultTCPMotionType_;
    MotionType	   CurrentTCPMotionType_;
    TCPSpeedMode	   DefaultTCPSpeedMode_;
    TCPSpeedMode	   CurrentTCPSpeedMode_;
    DNBReal		   CurrentTCPSpeed_;
    TCPAccelMode	   DefaultTCPAccelMode_;
    TCPAccelMode	   CurrentTCPAccelMode_;
    DNBReal		   CurrentTCPAccel_;
    TCPTermType		   DefaultTCPTermType_;
    TCPCornerMode	   DefaultTCPCornerMode_;
    DNBReal	   	   DefaultTCPRounding_;
    TCPTermType		   CurrentTCPTermType_;
    TCPCornerMode	   CurrentTCPCornerMode_;
    DNBReal		   CurrentTCPRounding_;
    DNBMath3D::AxisType	   DefaultApproachAxis_;
    ApproachDirection	   DefaultApproachDirection_;
    DNBReal 		   DefaultTCPDistance_;
    TCPOrientMode 	   DefaultTCPOrientMode_;
    DNBMath3D::AxisType	   CurrentApproachAxis_;
    ApproachDirection	   CurrentApproachDirection_;
    DNBReal		   CurrentTCPDistance_;
    TCPOrientMode	   CurrentTCPOrientMode_;
    TCPJointMode 	   DefaultTCPJointMode_;
    TCPJointMode	   CurrentTCPJointMode_;

};


#include "DNBCartesianPlannerExt.cc"

#endif  /* _DNB_CARTESIANPLANNEREXT_H_ */
