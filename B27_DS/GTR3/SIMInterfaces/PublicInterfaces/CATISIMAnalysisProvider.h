// COPYRIGHT Dassault Systemes 2001
//===================================================================
//
// CATISIMAnalysisProvider.h
// Define the CATISIMAnalysisProvider interface
//
//===================================================================
//
// Usage notes:
//   New interface: describe its use here
//
//===================================================================
//
//  May 2001  Creation: Code generated by the CAA wizard  pba
//===================================================================
/**
 * @CAA2Level L0
 * @CAA2Usage U3
 */
#ifndef CATISIMAnalysisProvider_H
#define CATISIMAnalysisProvider_H

#include "SIMItfCPP.h"
#include "CATBaseUnknown.h"
#include "CATListOfCATUnicodeString.h"
#include "CATListOfInt.h"
#include "CATLISTV_CATBaseUnknown.h"


extern ExportedBySIMItfCPP IID IID_CATISIMAnalysisProvider ;

//------------------------------------------------------------------

/**
 * Describe the functionality of your interface here
 * <p>
 * Using this prefered syntax will enable mkdoc to document your class.
 */
class ExportedBySIMItfCPP CATISIMAnalysisProvider: public CATBaseUnknown
{
  CATDeclareInterface;

  public:


  // No constructors or destructors on this pure virtual base class
  // --------------------------------------------------------------
    // The Analysis Provider is an alternate vision of the Agent interface
    // that permits the control the computation for N Analysis attached to it

    // Get a reconditionned Alias of the Analyses by the Agent
//USELESS    virtual HRESULT GetTitle(CATListValCATUnicodeString& oListTitle) = 0;

    // Get the associated Analyses (CATISiAnalysis)
    virtual HRESULT GetAnalysis(CATListValCATBaseUnknown_var& oListAnalysis) = 0;

    // Get the Status of the Analyses
    // 0  = Inactive
    // >0 = Active
    virtual HRESULT GetActivationStatus(CATRawCollint& oListStatus) = 0;

    // Set the Status for Analyses 
    // When ListStatus has one element Status is applied to all Analyses
    virtual HRESULT SetActivationStatus(CATRawCollint& iListStatus) = 0;

};

/** @nodoc */
CATDeclareHandler(CATISIMAnalysisProvider,CATBaseUnknown);


//------------------------------------------------------------------

#endif
