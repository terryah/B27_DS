// COPYRIGHT Dassault Systemes 2002
//===================================================================
//
// CATSIMPlayContext.h
// Header definition of CATSIMPlayContext
//
//===================================================================
// Usage notes: Class ONLY used in the Agent<->Player communication 
// protocol.
// Object designed to contain the set of information required to
//    load the default state of the object on init.
//    It is also used to save the current state of the agent unloaded.
//===================================================================
//
//  Apr 2002  Creation: Code generated by the CAA wizard  STV
//===================================================================
#ifndef CATSIMPlayContext_H
#define CATSIMPlayContext_H

/**
  * @CAA2Level L0
  * @CAA2Usage U3
  */

#include "SIMItfCPP.h"
#include "CatSIMLoopMode.h"

//-----------------------------------------------------------------------

/** @nodoc */
class ExportedBySIMItfCPP CATSIMPlayContext
{
public:
    
    // members
    int                 _DefaultParameterIndex;
    double              _DefaultClockValue;
    double              _DefaultStepValue;
    double              _DefaultTempoValue;
    CatSIMLoopMode      _DefaultLoopMode;
    
    // Standard constructors and destructors
    // -------------------------------------
    CATSIMPlayContext ();
    virtual ~CATSIMPlayContext ();
    
    // Copy constructor and equal operator
    // -----------------------------------
    CATSIMPlayContext (CATSIMPlayContext &);
    CATSIMPlayContext& operator=(CATSIMPlayContext&);
    
};

//-----------------------------------------------------------------------

#endif
