#ifndef CatSIMValidationStatusRequest_H
#define CatSIMValidationStatusRequest_H
// COPYRIGHT DASSAULT SYSTEMES 2001

/**
 * @CAA2Level L0
 * @CAA2Usage U3
 */

//   This header declaration enumerates values of interface parameter.
//===================================================================

/** @nodoc */
enum CatSIMValidationStatusRequest // definition to be completed
{
  catSIMValidationOK
 ,catSIMValidationKOResetAndGo
 ,catSIMValidationKOContinue
 ,catSIMValidationKOStop
 ,catSIMValidationKOStopAndReset
};

#endif
