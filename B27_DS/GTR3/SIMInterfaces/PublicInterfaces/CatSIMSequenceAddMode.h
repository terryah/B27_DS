#ifndef CatSIMSequenceAddMode_H
#define CatSIMSequenceAddMode_H
// COPYRIGHT DASSAULT SYSTEMES 2001

/**
 * @CAA2Level L0
 * @CAA2Usage U3
 */

//   This header declaration enumerates values of interface parameter.
//===================================================================

/** @nodoc */
enum CatSIMSequenceAddMode // definition to be completed
{
  catSIMSequenceAddModeInsertWithoutScheduling
 ,catSIMSequenceAddModeInsertAfterStartBeforeStop
 ,catSIMSequenceAddModeInsertAfterLastBeforeStop
};

#endif
