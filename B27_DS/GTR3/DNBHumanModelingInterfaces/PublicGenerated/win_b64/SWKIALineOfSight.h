/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef SWKIALineOfSight_h
#define SWKIALineOfSight_h

#ifndef ExportedBySWKHumanModelingPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __SWKHumanModelingPubIDL
#define ExportedBySWKHumanModelingPubIDL __declspec(dllexport)
#else
#define ExportedBySWKHumanModelingPubIDL __declspec(dllimport)
#endif
#else
#define ExportedBySWKHumanModelingPubIDL
#endif
#endif

#include "CATSafeArray.h"
#include "SWKIABodyElement.h"

class SWKIADOF;

extern ExportedBySWKHumanModelingPubIDL IID IID_SWKIALineOfSight;

class ExportedBySWKHumanModelingPubIDL SWKIALineOfSight : public SWKIABodyElement
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_NbDOFs(CATLONG & poNbDOFs)=0;

    virtual HRESULT __stdcall GetDOF(CATLONG piDOFNumber, SWKIADOF *& poDOF)=0;

    virtual HRESULT __stdcall get_Length(double & poLength)=0;

    virtual HRESULT __stdcall SetPosition(const CATSafeArrayVariant & piNewPosition)=0;

    virtual HRESULT __stdcall ApplyPosition(const CATSafeArrayVariant & piPositionIncrement)=0;


};

CATDeclareHandler(SWKIALineOfSight, SWKIABodyElement);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIABase.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
