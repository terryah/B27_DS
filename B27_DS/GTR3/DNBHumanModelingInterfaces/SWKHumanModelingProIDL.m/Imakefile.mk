# COPYRIGHT DASSAULT SYSTEMES 2000
#======================================================================
# Imakefile for module SWKHumanModelingProIDL
# Module for compilation of the protected IDL interfaces
#======================================================================
#
#  Feb 2000  Creation: Code generated by the CAA wizard  augustin
#======================================================================
#
# NO BUILD             
#

BUILT_OBJECT_TYPE=NONE

SOURCES_PATH=ProtectedInterfaces
COMPILATION_IDL=YES

