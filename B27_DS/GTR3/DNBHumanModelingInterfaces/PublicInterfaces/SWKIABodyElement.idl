// COPYRIGHT DASSAULT SYSTEMES 2008
#ifndef SWKIABodyElement_IDL
#define SWKIABodyElement_IDL

/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */

/*IDLREP*/

// ****************************************************************************
// Framework DNBHumanModelingInterfaces
// Copyright Safework Inc.
// ****************************************************************************
//  Abstract:
//  ---------
//
//  Interfaces for Journalling
//
// ****************************************************************************
//  Usage:
//  ------
//
// ****************************************************************************
//  Inheritance:
//  ------------
//
//  CATIABase
//
// ****************************************************************************
//  Main Methods:
//  -------------
//
//   get_FullName
//
//   GetGlobalPosition
//   get_PositionX
//   get_PositionY
//   get_PositionZ
//
//   get_Manikin    (ATTN: This is different from CATIABase::GetParent)
//
//   get_RefreshDisplay
//   put_RefreshDisplay
//
//   Refresh3D
// ****************************************************************************
//  History
//  -------
//
//  Author: Jean-Guy AUGUSTIN
//  Date  : 26/01/00
//  Goal  : Creation
// ****************************************************************************
//
// --- DNBHumanModelingInterfaces
#include "CATIABase.idl"
//
// --- System
#include "CATSafeArray.idl"
//
// --- DNBHumanModelingInterfaces
interface SWKIAManikin;



/**  
 * This interface is used to access a body element (segment, ellipse, ...)
 * It provides common services for all body elements of the manikin.
 */
interface SWKIABodyElement : CATIABase
{
 // ===========================================================================
 //  get_FullName
 //  ------------
 //
    /**
      * Returns the full name of the body element.<br>
      * This property is different from the property<br>
      * <code>Name</code> of <code>AnyObject</code>,<br>
      * which gives the short name or abbreviated name<br>
      * of the body element.<br>
      * <br>
      * For instance, if the body element in question is<br>
      * the left leg segment, then property <code>Name</code><br>
      * yields "<font color=green>LSLeLe</font>", whereas<br>
      * property <code>FullName</code> yields the character<br>
      * string "<font color=green>Left Leg</font>".
      **/
#pragma PROPERTY FullName
    HRESULT get_FullName(inout /*IDLRETVAL*/ CATBSTR pioFullName);

 // ===========================================================================
 //  GetGlobalPosition
 //  -----------------
 //
    /**
      * Returns the global position ofthe body element.<br>
      * If this body element is the body, then the position returned is<br>
      * the global position of the manikin.  If the<br>
      * body element is a segment or a line of sight, the position<br>
      * returned is the global position of the beginning of that segment or<br>
      * line of sight.  If the body element is an ellipse, the global position<br>
      * of the center of that ellipse is returned.
      **/
    HRESULT GetGlobalPosition(inout CATSafeArrayVariant poGlobalPosition);

 // ===========================================================================
 //  get_PositionX
 //  -------------
 //
    /**
      * Returns the x coordinate of the position of the body element.<br>
      * If this body element is the body, then the position returned is<br>
      * the x-coordinate of global position of the manikin.  If the<br>
      * body element is a segment or a line of sight, the position<br>
      * returned is the x-coordinate of the beginning of that segment or<br>
      * line of sight.  If the body element is an ellipse, the x-coordiante<br>
      * of the center of that ellipse is returned.
      **/
#pragma PROPERTY PositionX
    HRESULT get_PositionX (out /*IDLRETVAL*/ double poPositionX);


 // ===========================================================================
 //  get_PositionY
 //  -------------
 //
    /**
      * Returns the y coordinate of the position of the body element.<br>
      * If this body element is the body, then the position returned is<br>
      * the y-coordinate of global position of the manikin.  If the<br>
      * body element is a segment or a line of sight, the position<br>
      * returned is the y-coordinate of the beginning of that segment or<br>
      * line of sight.  If the body element is an ellipse, the y-coordiante<br>
      * of the center is returned.
      **/
#pragma PROPERTY PositionY
    HRESULT get_PositionY (out /*IDLRETVAL*/ double poPositionY);


 // ===========================================================================
 //  get_PositionZ
 //  -------------
 //
    /**
      * Returns the z coordinate of the position of the body element.<br>
      * If this body element is the body, then the position returned is<br>
      * the z-coordinate of global position of the manikin.  If the<br>
      * body element is a segment or a line of sight, the position<br>
      * returned is the z-coordinate of the beginning of that segment or<br>
      * line of sight.  If the body element is an ellipse, the z-coordiante<br>
      * of the center is returned.
      **/
#pragma PROPERTY PositionZ
    HRESULT get_PositionZ (out /*IDLRETVAL*/ double poPositionZ);


 // ===========================================================================
 //  get_Manikin
 //  -----------
 //
    /**
      *  Returns the manikin which owns this body element.
      **/
#pragma PROPERTY Manikin
    HRESULT get_Manikin(out /*IDLRETVAL*/ SWKIAManikin poManikin);


 // ===========================================================================
 //  get_RefreshDisplay
 //  ------------------
 //  put_RefreshDisplay
 //  ------------------
 //
    /**
      * Enables or disables the update of the display during the script<br>
      * replay.<br>
      * To improve performance, this update can be temporarely disabled <br>
      * by setting this property to <b>False</b> in the script.<br>
      * <br>
      * The property is <b>True</b> if the elements's display is refreshed
      * after each posture change (value set by default).<br>
      * <! @sample >
      * <dl>
      * <dt><b>Example:</b>
      * <dd>
      * This example makes the update of a manikin display disabled during a portion
      * of the script replay.
      * <pre>
      * myManikin.Body.<font color="red">RefreshDisplay</font> = False
      * </pre>
      * </dl>
      **/
#pragma PROPERTY RefreshDisplay
    HRESULT get_RefreshDisplay(out /*IDLRETVAL*/ boolean poRefreshIsEnabled);
    HRESULT put_RefreshDisplay(in boolean piRefreshIsEnabled);


 // ===========================================================================
 //  Refresh3D
 //  ---------
 //
    /**
      *  Refreshes the 3D representation of the current element.
      **/
    HRESULT Refresh3D();
};

// Interface name : SWKIABodyElement
#pragma ID SWKIABodyElement "DCE:12c1daa2-d402-11d3-8092005004d3fe74"
#pragma DUAL SWKIABodyElement

// VB object name : SWKBodyElement (Id used in Visual Basic)
#pragma ID SWKBodyElement "DCE:12c1daa3-d402-11d3-8092005004d3fe74"
#pragma ALIAS SWKIABodyElement SWKBodyElement

#endif
