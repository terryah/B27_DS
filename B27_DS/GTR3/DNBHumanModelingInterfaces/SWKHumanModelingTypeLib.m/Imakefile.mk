# COPYRIGHT DASSAULT SYSTEMES 2000
#======================================================================
# Imakefile for module SWKHumanModelingTypeLib.m
# Module for compilation of the typelib
#======================================================================
#
#  Feb 2000  Creation: Code generated by the CAA wizard  augustin
#======================================================================
#
# TYPELIB          
#

BUILT_OBJECT_TYPE=TYPELIB
# no more BUILD_PRIORITY=30

LINK_WITH = InfTypeLib PSTypeLib

#OS      = IRIX
#BUILD   = NO

#OS      = SunOS
#BUILD   = NO

#OS      = AIX
#BUILD   = NO

#OS      = HP-UX
#BUILD   = NO

#OS      = win_a
#BUILD=NO
