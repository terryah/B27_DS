// COPYRIGHT DASSAULT SYSTEMES 2000
//===================================================================
//
// CatMarker2DType.idl
//   This automation declaration enumerates values of interface parameter.
//
//===================================================================
//
//  Jul 2000  Creation: LJE
//
//===================================================================
#ifndef CatMarker2DType_IDL
#define CatMarker2DType_IDL
/*IDLREP*/

/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */

/**
 * The different types of Marker2Ds
 * @see CATIAMarker2D.
 * @param catMarker2DTypeLine
 *   The marker is a line between two points.
 * @param catMarker2DTypeArrow
 *   The marker is an arrow between two points.
 * @param catMarker2DTypeRectangle
 *   The marker is a rectangle between two points.
 * @param catMarker2DTypeCircle
 *   The marker is a circle centered on a first point and lying on a second point.
 * @param catMarker2DTypeFreeHand
 *   The marker is a curve along a series of points.
 * @param catMarker2DTypeText
 *   The marker is a text on a point.
 * @param catMarker2DTypePicture
 *   The marker is a picture between two points.
 */
enum CatMarker2DType
{
  catMarker2DTypeLine,
  catMarker2DTypeArrow,
  catMarker2DTypeRectangle,
  catMarker2DTypeCircle,
  catMarker2DTypeFreeHand,
  catMarker2DTypeText,
  catMarker2DTypePicture
};

#endif
