/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAAnnotatedView_h
#define CATIAAnnotatedView_h

#ifndef ExportedByNavigatorPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __NavigatorPubIDL
#define ExportedByNavigatorPubIDL __declspec(dllexport)
#else
#define ExportedByNavigatorPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByNavigatorPubIDL
#endif
#endif

#include "CATBSTR.h"
#include "CATIABase.h"
#include "CATSafeArray.h"
#include "CatAnnotatedViewBehavior.h"
#include "CatProjectionMode.h"

class CATIAMarker2Ds;

extern ExportedByNavigatorPubIDL IID IID_CATIAAnnotatedView;

class ExportedByNavigatorPubIDL CATIAAnnotatedView : public CATIABase
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall GetOrigin(CATSafeArrayVariant & oOrigin)=0;

    virtual HRESULT __stdcall GetSightDirection(CATSafeArrayVariant & oSight)=0;

    virtual HRESULT __stdcall GetUpDirection(CATSafeArrayVariant & oUp)=0;

    virtual HRESULT __stdcall get_ProjectionMode(CatProjectionMode & oProjectionMode)=0;

    virtual HRESULT __stdcall get_Zoom(double & oZoom)=0;

    virtual HRESULT __stdcall get_FieldOfView(double & oFieldOfView)=0;

    virtual HRESULT __stdcall get_Comment(CATBSTR & oText)=0;

    virtual HRESULT __stdcall put_Comment(const CATBSTR & iText)=0;

    virtual HRESULT __stdcall get_Sound(CATBSTR & oPath)=0;

    virtual HRESULT __stdcall put_Sound(const CATBSTR & iPath)=0;

    virtual HRESULT __stdcall get_Marker2Ds(CATIAMarker2Ds *& oMarker2Ds)=0;

    virtual HRESULT __stdcall Update()=0;

    virtual HRESULT __stdcall get_BehaviorMode(CatAnnotatedViewBehavior & oBehaviorMode)=0;

    virtual HRESULT __stdcall put_BehaviorMode(CatAnnotatedViewBehavior iBehaviorMode)=0;


};

CATDeclareHandler(CATIAAnnotatedView, CATIABase);

#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIACollection.h"
#include "CATIAMarker2D.h"
#include "CATIAMarker2Ds.h"
#include "CATIAViewer.h"
#include "CATIAViewer3D.h"
#include "CATVariant.h"
#include "CatCaptureFormat.h"
#include "CatClippingMode.h"
#include "CatLightingMode.h"
#include "CatMarker2DType.h"
#include "CatMarkerTextOrientation.h"
#include "CatNavigationStyle.h"
#include "CatRenderingMode.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
