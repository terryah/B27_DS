/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAMarkerSettingAtt_h
#define CATIAMarkerSettingAtt_h

#include "CATCORBABoolean.h"
#include "CAT_VARIANT_BOOL.h"

#ifndef ExportedByNavigatorPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __NavigatorPubIDL
#define ExportedByNavigatorPubIDL __declspec(dllexport)
#else
#define ExportedByNavigatorPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByNavigatorPubIDL
#endif
#endif

#include "CATBSTR.h"
#include "CATIASettingController.h"

extern ExportedByNavigatorPubIDL IID IID_CATIAMarkerSettingAtt;

class ExportedByNavigatorPubIDL CATIAMarkerSettingAtt : public CATIASettingController
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall GetMarkerDefaultsColor(CATLONG & oRed, CATLONG & oGreen, CATLONG & oBlue)=0;

    virtual HRESULT __stdcall SetMarkerDefaultsColor(CATLONG iRed, CATLONG iGreen, CATLONG iBlue)=0;

    virtual HRESULT __stdcall GetMarkerDefaultsColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetMarkerDefaultsColorLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_MarkerDefaultsWeight(CATLONG & oValue)=0;

    virtual HRESULT __stdcall put_MarkerDefaultsWeight(CATLONG iValue)=0;

    virtual HRESULT __stdcall GetMarkerDefaultsWeightInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetMarkerDefaultsWeightLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_MarkerDefaultsDashed(CATLONG & oValue)=0;

    virtual HRESULT __stdcall put_MarkerDefaultsDashed(CATLONG iValue)=0;

    virtual HRESULT __stdcall GetMarkerDefaultsDashedInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetMarkerDefaultsDashedLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_Marker2DAutoNaming(CAT_VARIANT_BOOL & oIsActive)=0;

    virtual HRESULT __stdcall put_Marker2DAutoNaming(CAT_VARIANT_BOOL iIsActive)=0;

    virtual HRESULT __stdcall GetMarker2DAutoNamingInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetMarker2DAutoNamingLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_Marker3DAutoNaming(CAT_VARIANT_BOOL & oIsActive)=0;

    virtual HRESULT __stdcall put_Marker3DAutoNaming(CAT_VARIANT_BOOL iIsActive)=0;

    virtual HRESULT __stdcall GetMarker3DAutoNamingInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetMarker3DAutoNamingLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall GetMarkerTextColor2D(CATLONG & oRed, CATLONG & oGreen, CATLONG & oBlue)=0;

    virtual HRESULT __stdcall SetMarkerTextColor2D(CATLONG iRed, CATLONG iGreen, CATLONG iBlue)=0;

    virtual HRESULT __stdcall GetMarkerTextColor2DInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetMarkerTextColor2DLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall GetMarkerTextColor3D(CATLONG & oRed, CATLONG & oGreen, CATLONG & oBlue)=0;

    virtual HRESULT __stdcall SetMarkerTextColor3D(CATLONG iRed, CATLONG iGreen, CATLONG iBlue)=0;

    virtual HRESULT __stdcall GetMarkerTextColor3DInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetMarkerTextColor3DLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_MarkerTextWeight2D(CATLONG & oValue)=0;

    virtual HRESULT __stdcall put_MarkerTextWeight2D(CATLONG iValue)=0;

    virtual HRESULT __stdcall GetMarkerTextWeight2DInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetMarkerTextWeight2DLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall put_MarkerTextWeight3D(CATLONG iValue)=0;

    virtual HRESULT __stdcall get_MarkerTextWeight3D(CATLONG & oValue)=0;

    virtual HRESULT __stdcall GetMarkerTextWeight3DInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetMarkerTextWeight3DLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_MarkerTextDashed2D(CATLONG & oValue)=0;

    virtual HRESULT __stdcall put_MarkerTextDashed2D(CATLONG iValue)=0;

    virtual HRESULT __stdcall GetMarkerTextDashed2DInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetMarkerTextDashed2DLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_MarkerTextDashed3D(CATLONG & oValue)=0;

    virtual HRESULT __stdcall put_MarkerTextDashed3D(CATLONG iValue)=0;

    virtual HRESULT __stdcall GetMarkerTextDashed3DInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetMarkerTextDashed3DLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_MarkerTextDefaultsFont2D(CATBSTR & oValue)=0;

    virtual HRESULT __stdcall put_MarkerTextDefaultsFont2D(const CATBSTR & iValue)=0;

    virtual HRESULT __stdcall GetMarkerTextDefaultsFont2DInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetMarkerTextDefaultsFont2DLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall put_MarkerTextDefaultsFont3D(const CATBSTR & iValue)=0;

    virtual HRESULT __stdcall get_MarkerTextDefaultsFont3D(CATBSTR & oValue)=0;

    virtual HRESULT __stdcall GetMarkerTextDefaultsFont3DInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetMarkerTextDefaultsFont3DLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_MarkerTextDefaultsSize2D(CATLONG & oValue)=0;

    virtual HRESULT __stdcall put_MarkerTextDefaultsSize2D(CATLONG iValue)=0;

    virtual HRESULT __stdcall GetMarkerTextDefaultsSize2DInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetMarkerTextDefaultsSize2DLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall put_MarkerTextDefaultsSize3D(CATLONG iValue)=0;

    virtual HRESULT __stdcall get_MarkerTextDefaultsSize3D(CATLONG & oValue)=0;

    virtual HRESULT __stdcall GetMarkerTextDefaultsSize3DInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetMarkerTextDefaultsSize3DLock(CAT_VARIANT_BOOL iLocked)=0;


};

CATDeclareHandler(CATIAMarkerSettingAtt, CATIASettingController);

#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIABase.h"
#include "CATSafeArray.h"
#include "CATVariant.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
