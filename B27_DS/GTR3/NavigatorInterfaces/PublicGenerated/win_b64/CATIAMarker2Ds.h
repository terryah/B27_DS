/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAMarker2Ds_h
#define CATIAMarker2Ds_h

#ifndef ExportedByNavigatorPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __NavigatorPubIDL
#define ExportedByNavigatorPubIDL __declspec(dllexport)
#else
#define ExportedByNavigatorPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByNavigatorPubIDL
#endif
#endif

#include "CATBSTR.h"
#include "CATIACollection.h"
#include "CATSafeArray.h"
#include "CATVariant.h"

class CATIAMarker2D;

extern ExportedByNavigatorPubIDL IID IID_CATIAMarker2Ds;

class ExportedByNavigatorPubIDL CATIAMarker2Ds : public CATIACollection
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall Add2DLine(const CATSafeArrayVariant & iCoordinates, CATIAMarker2D *& oMarker2D)=0;

    virtual HRESULT __stdcall Add2DArrow(const CATSafeArrayVariant & iCoordinates, CATIAMarker2D *& oMarker2D)=0;

    virtual HRESULT __stdcall Add2DRectangle(const CATSafeArrayVariant & iCoordinates, CATLONG iFillStatus, CATIAMarker2D *& oMarker2D)=0;

    virtual HRESULT __stdcall Add2DCircle(const CATSafeArrayVariant & iCoordinates, CATLONG iFillStatus, CATIAMarker2D *& oMarker2D)=0;

    virtual HRESULT __stdcall Add2DFreeHand(const CATSafeArrayVariant & iCoordinates, CATIAMarker2D *& oMarker2D)=0;

    virtual HRESULT __stdcall Add2DText(const CATSafeArrayVariant & iCoordinates, const CATBSTR & iText, CATIAMarker2D *& oMarker2D)=0;

    virtual HRESULT __stdcall Add2DPicture(const CATSafeArrayVariant & iCoordinates, const CATBSTR & iPath, CATIAMarker2D *& oMarker2D)=0;

    virtual HRESULT __stdcall Item(const CATVariant & iIndex, CATIAMarker2D *& oMarker2D)=0;

    virtual HRESULT __stdcall Remove(const CATVariant & iIndex)=0;


};

CATDeclareHandler(CATIAMarker2Ds, CATIACollection);

#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIABase.h"
#include "CATIAMarker2D.h"
#include "CatMarker2DType.h"
#include "CatMarkerTextOrientation.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
