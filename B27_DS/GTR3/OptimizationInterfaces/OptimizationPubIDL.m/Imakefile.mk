# COPYRIGHT DASSAULT SYSTEMES 2000
#======================================================================
# Imakefile for module OptimizationPubIdl.m
# Module for compilation of the public IDL interfaces
#======================================================================
#
#  Feb 2000  Creation: Code generated by the CAA wizard  TVQ
#======================================================================
#
# NO BUILD           
#

BUILT_OBJECT_TYPE=NONE

SOURCES_PATH=PublicInterfaces
COMPILATION_IDL=YES

