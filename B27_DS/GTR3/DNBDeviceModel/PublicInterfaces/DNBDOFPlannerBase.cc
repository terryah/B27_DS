// COPYRIGHT Dassault Systemes 2005
/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

inline DNBBasicDOFTargetSet::Handle
DNBDOFPlannerBase::getDOFTargetSet()
	DNB_THROW_SPEC_NULL
{
	return hTargetSet;
}

inline void
DNBDOFPlannerBase::setDOFPlannerPeriod( DNBSimTime period )
	DNB_THROW_SPEC((DNBERangeError))
{
    if( period < 0.0 )
    {

	DNBERangeError    error( DNB_FORMAT("Negative DOF motion planner period"));
	throw error;


    }
    DOFPlannerPeriod_ = period;

}


inline DNBSimTime
DNBDOFPlannerBase::getDOFPlannerPeriod( ) const
	DNB_THROW_SPEC_NULL
{
    return DOFPlannerPeriod_;

}

inline void
DNBDOFPlannerBase::setDefaultDOFSpeed( DNBReal speed )
    DNB_THROW_SPEC_NULL
{
    DefaultDOFSpeed_ = speed;

}


inline DNBReal
DNBDOFPlannerBase::getDefaultDOFSpeed( ) const
    DNB_THROW_SPEC_NULL
{
    return DefaultDOFSpeed_;

}

inline DNBReal
DNBDOFPlannerBase::PrepareMove (DNBBasicDOFTarget::Handle hTarget)
	DNB_THROW_SPEC_NULL
{
	return 0.0;
}

#if 0
inline DNBMathVector
DNBDOFPlannerBase::ExecuteMove (const DNBSimTime Time)
	DNB_THROW_SPEC_NULL
{
	DNBMathVector tempVec (0, 0.0);

	return tempVec;
}
#endif
