// COPYRIGHT Dassault Systemes 2005
/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

/**
 * @fullreview rtl xyuan 01:08:16
 */
// --------------------------------------------------------

inline void 
DNBMotionPlannerBase::setSamplePeriod(DNBSimTime step)
	DNB_THROW_SPEC_NULL
{
	timeStep=step;
}

inline scl_wstring& 
DNBMotionPlannerBase::getPlannerName() 
	DNB_THROW_SPEC_NULL
{
	return plannerName;
}

