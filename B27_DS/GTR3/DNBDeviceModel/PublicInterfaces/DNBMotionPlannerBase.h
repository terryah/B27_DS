// COPYRIGHT Dassault Systemes 2005
//*
//* FILE:
//*     DNBMotionPlannerBase.h      - public header file
//*
//* MODULE:
//*     DNBMotionPlannerBase  - single non-template class
//*
//* OVERVIEW:
//*     This module defines an extension class which is base for all
//*     motion planner classes.
//*	 
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*		xin			11/29/00	initial
//*		
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1998 - 2000 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*
//  Jun 2006  Change micro from DNB_THROW_SPEC_NULL to DNB_THROW_SPEC_ANY LYN 
//*
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_MOTIONPLANNERBASE_H_
#define _DNB_MOTIONPLANNERBASE_H_

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */



#include <DNBSystemBase.h>
#include <scl_vector.h>
#include <DNBSystemDefs.h>
#include <DNBDynamicObject.h>


////

#include <DNBSimTime.h>
#include <DNBBasicDevice3D.h>
#include <DNBSimletException.h>


///////////////////////////////////////////////



class ExportedByDNBDeviceModel DNBMotionPlannerBase : public DNBDynamicObject
{

    DNB_DECLARE_DYNAMIC_RTTI( DNBMotionPlannerBase );

public:

    typedef enum MTM	//motion time mode
	{
		EXACT,			//no speed/acceleration limits taken into account
		CONSTRAINED		//constrained by speeds and accelerations
	}MotionTimeMode;

	typedef enum ST
	{
		VARIABLE_ACCELERATION = 0,
		CONSTANT_ACCELERATION
		
	}ScalingType;

	enum RoundType
	{
		DISTANCE,
		VELOCITY
	};
	enum MotionType
	{
		JOINT,
		STRAIGHT,
		CIRCULAR, 
		CIRCULARVIA,
		SLEW
	};

	enum TCPOrientMode 
		{ 
			TCPOrientOneAxis, 
			TCPOrientTwoAxis,
			TCPOrientThreeAxis,
			TCPOrientWristAxis 
		};
	enum InterpolationMode
	{	
		NEARMODE,
		KEEPCFG_KEEPTURN,
		KEEPCFG_SETTURN,
		SETCFG_KEEPTURN,
		SETCFG_SETTURN
	};

	enum MotionBasis
	{
		MOTION_ABSOLUTE,
		MOTION_PERCENT,
		MOTION_TIME
	};

	DNBSimTime getSegmentTime(size_t i)
		DNB_THROW_SPEC_NULL;

	inline scl_wstring& getPlannerName()
	DNB_THROW_SPEC_NULL;

	inline void setSamplePeriod(DNBSimTime step)
	DNB_THROW_SPEC_NULL;

	virtual void 
	addDOFTarget(DNBBasicDOFTarget::Handle hTarget)
    DNB_THROW_SPEC((DNBEDomainError))=0; 


	
	virtual 
		void resetProcess()
	DNB_THROW_SPEC_NULL=0;

	virtual DNBMathVector 
	evaluate( const DNBSimTime &currentTime )
	DNB_THROW_SPEC_ANY=0;

	virtual DNBMathVector 
	evaluate( )
	DNB_THROW_SPEC_ANY=0;

	virtual void reset()
		DNB_THROW_SPEC_NULL=0;

	virtual DNBSimTime 
	getMoveTime()
			DNB_THROW_SPEC_ANY=0;

	virtual bool reachedEndOfMove()
			DNB_THROW_SPEC_NULL=0;

protected:



	DNBMotionPlannerBase( )
	DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit));

	DNBMotionPlannerBase( const scl_wstring&)
	DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit));

    DNBMotionPlannerBase( const DNBMotionPlannerBase &other,
			    CopyMode mode )
	DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit));

    virtual
    ~DNBMotionPlannerBase( )
	DNB_THROW_SPEC_NULL;

    typedef scl_vector< DNBSimTime, DNB_ALLOCATOR(DNBSimTime) > SegmentTimeDB;

	SegmentTimeDB segTime_;
	DNBSimTime  total_move_time,timeStep;

	struct MotionAttributes
	{
		MotionType						motionType_;	//JOINT, STRAIGHT, CIRCULAR, CIRCULARVIA
		RoundType						roundType_;		//VELOCITY, DISTANCE
		TCPOrientMode					intplStrategy_;	//one,two,three-axis or wrist
		InterpolationMode				intplMode_;		//SETCONFIG_SETTURN, etc
//		DNBBasicTagPoint3D::TimingMode	timeMode_;		//DURATION, VELOCITY
		DNBReal							speedortime;	//
		DNBReal							accel;			//
		DNBReal							roundValue_;	//
		MotionBasis						motionBasis_;	//MOTION_ABSOLUTE, MOTION_PERCENT, MOTION_TIME
	};

private:

	scl_wstring plannerName;
};

#include "DNBMotionPlannerBase.cc"


#endif  /* _DNB_MOTIONPLANNERBASE_H_ */
