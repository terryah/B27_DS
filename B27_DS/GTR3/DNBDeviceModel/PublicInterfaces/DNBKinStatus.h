// COPYRIGHT Dassault Systemes 2005

#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_KINSTATUS_H_
#define _DNB_KINSTATUS_H_

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */


#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>


#include <DNBDeviceModel.h>



////////////////////////////////////////////////////////////////////
//							          //
// This class contains a 32 bit unsigned integer that can contain //
// information about the kinematic status of a forward kinematic  //
// or inverse kinamatic function call as the case may be, using   //
// the appropriate Device or Robot interfaces available.          //
// The status may be Device specific DOFStatus or Robot specific  //
// ArmState. Each status corresponds to one bit in the 32 bit     //
// unsigned integer. These states are:				  //
//		0 - ArmSolutionGood, no error                     //  	
//              1 - DOFSoftErrorExceeded
//              2 - ArmTargetUnreachable 
//              3 - DOFHardErrorExceeded
//              5 - ArmSolutionSingular 
//              6 - ArmPostureInvalid 
//              7 - ArmNoSolution 
//              8 - ArmSolverError 
//
///////////////////////////////////////////////////////////////////////
          									   




class ExportedByDNBDeviceModel DNBKinStatus 
{
public:


    DNBKinStatus( )
        DNB_THROW_SPEC((scl_bad_alloc));

    DNBKinStatus( const DNBKinStatus &right )
        DNB_THROW_SPEC((scl_bad_alloc));

    virtual
    ~DNBKinStatus( )
        DNB_THROW_SPEC_NULL;

    DNBKinStatus& 
    operator=( const DNBUnsigned32 &status )
	DNB_THROW_SPEC_NULL;

    bool
    operator==( const DNBUnsigned32 &status ) const 
	DNB_THROW_SPEC_NULL;

    /////////////////////////////////////////////////////////////////
    //                                                             //
    // Return kinematic state enumerated in DOFStatus and ArmState // 
    // corresponding to the first non-zero bit of status_.         //               //                                                             //
    /////////////////////////////////////////////////////////////////     

    DNBUnsigned32
    what() const
        DNB_THROW_SPEC_NULL;

private:

    DNBUnsigned32    status_;


};







#endif  /* _DNB_KINSTATUS_H_ */
