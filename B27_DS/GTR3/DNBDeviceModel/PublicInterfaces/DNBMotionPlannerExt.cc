// COPYRIGHT Dassault Systemes 2005
/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */


inline void
DNBMotionPlannerExt::postEvent(size_t eve)
{
	DNBNotificationAgent::postEvent(eve);
}
