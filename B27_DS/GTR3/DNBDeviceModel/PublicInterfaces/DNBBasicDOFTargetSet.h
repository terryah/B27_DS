// COPYRIGHT Dassault Systemes 2005
//
//*
//* FILE:
//*     DNBBasicDOFTargetSet.h      - public header file
//*
//* MODULE:
//*     DNBBasicDOFTargetSet  - 
//*
//* OVERVIEW:
//*     This 
//*     
//*	 
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     xin         03/07/00    Initial implementation
//*					07/12/00	modify interface
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1998 - 2000 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_BASICDOFTARGETSET_H_
#define _DNB_BASICDOFTARGETSET_H_



/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */



#include <DNBSystemBase.h>
#include <scl_vector.h>
#include <DNBSystemDefs.h>


#include <DNBDeviceModel.h>
#include <DNBBasicEntity.h>
#include <DNBBasicDOFTarget.h>


//
//
//  Note:
//         It is possible to have single tag points in 
//         multiple paths. When inserting a tag point in
//         to a path, if the tag point is NOT a child of
//         any other 3D entity then that path will take 
//         that tag point to be its child. If there is a
//         parent for that tag point then the path will not
//         alter it.
//
//         This is important to remember! If a tag point
//         has a spatial parent that goes away, the tag point
//         in the path will keep that point around until the
//         point is removed from the path.
//
//         If you wish to avoid conflicts with this and DO NOT
//         NEED single points in multiple paths then never set
//         the spatial or logical parent of a tag point that 
//         is entered into a path.
//
class ExportedByDNBDeviceModel DNBBasicDOFTargetSet : public DNBBasicEntity
{

    DNB_DECLARE_DYNAMIC_CREATE( DNBBasicDOFTargetSet );
    DNB_DECLARE_SHARED_OBJECT( DNBBasicDOFTargetSet );
    DNB_DECLARE_EXTENDIBLE_OBJECT( DNBBasicDOFTargetSet );
    DNB_DECLARE_EXTENDIBLE_FACTORY( DNBBasicDOFTargetSet );

public:

    virtual DNBMessage
    getEntityTypeName( ) const
        DNB_THROW_SPEC_NULL;


    size_t
    getDOFTargetCount() const
        DNB_THROW_SPEC_NULL;

    //
    // NOTE: all indices are 0 based
    //
    DNBBasicDOFTarget::Handle
    getDOFTarget( const size_t index) const
        DNB_THROW_SPEC((DNBERangeError));

    void
    getDOFTargets( DNBBasicDOFTarget::List& listDOFTargets ) const
        DNB_THROW_SPEC_NULL;

    void
    getDOFTargets( DNBBasicDOFTarget::Vector& vectorDOFTargets ) const
        DNB_THROW_SPEC_NULL;

    void
    setDOFTargets( const DNBBasicDOFTarget::List& listDOFTargets )
        DNB_THROW_SPEC_NULL;

    void
    setDOFTargets( const DNBBasicDOFTarget::Vector& vectorDOFTargets )
        DNB_THROW_SPEC_NULL;

    

    //
    // insert moves the point into the requested position
    // and moves current occupied point and the rest up 1 position 
    //
    // Note that duplicate points can exist in a path.
    //
    bool
    insert( size_t index, DNBBasicDOFTarget::Handle hDOFTarget )
        DNB_THROW_SPEC_NULL;

    //
    // addDOFTarget adds passed DOF target to the end ( push's to end of vector )
    //
    void
    append( DNBBasicDOFTarget::Handle hDOFTarget )
    DNB_THROW_SPEC_NULL;
 
    void
    prepend( DNBBasicDOFTarget::Handle hDOFTarget )
    DNB_THROW_SPEC_NULL;

    bool
    remove( DNBBasicDOFTarget::Handle hDOFTarget )
        DNB_THROW_SPEC_NULL;

    bool
    remove( size_t index )
        DNB_THROW_SPEC_NULL;

    void 
    clear( )
        DNB_THROW_SPEC_NULL;

    size_t
    getIndex( DNBBasicDOFTarget::Handle hDOFTarget ) const
        DNB_THROW_SPEC_NULL;

    bool 
    swap( size_t fromIndex, size_t toIndex ) 
        DNB_THROW_SPEC_NULL;

protected:

    DNBBasicDOFTargetSet()
        DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit, DNBENetworkError));

    DNBBasicDOFTargetSet( const DNBBasicDOFTargetSet &right, CopyMode mode )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit, DNBENetworkError));

    virtual
    ~DNBBasicDOFTargetSet( )
        DNB_THROW_SPEC_NULL;

private:
	DNBBasicDOFTarget::Vector TargetSet;

};




#endif  /* _DNB_BASICDOFTARGETSET_H_ */

