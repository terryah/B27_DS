//
// COPYRIGHT DELMIA CORP. 2007
//===================================================================
//
// DNBConveyorTrackingEnums.h
// 
//
//===================================================================
//
// Usage notes:
// 
/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef DNB_CONVEYOR_TRACKING_ENUMS_H_
#define DNB_CONVEYOR_TRACKING_ENUMS_H_

typedef enum TPT    //Conveyor tracking position
    {
        TRACK_START = 0,
        TRACK_TARGET
    } PositionType;

    typedef enum CTM    //Conveyor Tracking Mode
    {
        TRACKING_OFF = 0,
        TRACKING_ON
    } TrackingMode;


#endif

