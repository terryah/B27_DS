// COPYRIGHT Dassault Systemes 2005
//
//*
//* FILE:
//*     DNBBasicDOFTarget.h      - public header file
//*
//* MODULE:
//*     DNBBasicDOFTarget  - 
//*
//* OVERVIEW:
//*     This 
//*     
//*	 
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     xin         03/07/00    Initial implementation
//*					07/13/00	modify
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1998 - 2000 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*

#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_BASICDOFTARGET_H_
#define _DNB_BASICDOFTARGET_H_

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */



#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>


#include <DNBDeviceModel.h>
#include <DNBSimTime.h>
#include <DNBBasicEntity.h>
#include <DNBMathVector.h>


//
//  This class represents an abstract body with extent.  It will eventually
//  include support for tag points and paths.
//
class ExportedByDNBDeviceModel DNBBasicDOFTarget : public DNBBasicEntity
{
    DNB_DECLARE_DYNAMIC_CREATE( DNBBasicDOFTarget );
    DNB_DECLARE_SHARED_OBJECT( DNBBasicDOFTarget );
    DNB_DECLARE_EXTENDIBLE_OBJECT( DNBBasicDOFTarget );
    DNB_DECLARE_EXTENDIBLE_FACTORY( DNBBasicDOFTarget );


public:

   enum TimingMode
    {	
		DURATION,
		VELOCITY    
	};

	void setDOFDisplacement(const DNBMathVector&newDOFvalue)
        DNB_THROW_SPEC((DNBEOutOfRange,DNBEInvalidArgument));

	void setDuration(const DNBSimTime& value)
        DNB_THROW_SPEC_NULL;

	//in %
	void setVelocity(DNBReal value)
        DNB_THROW_SPEC_NULL;

 	TimingMode getTimingMode() const
        DNB_THROW_SPEC_NULL;

	void getDOFDisplacement(DNBMathVector&newDOF  ) const
        DNB_THROW_SPEC_NULL;

	size_t getDOFCount() const
        DNB_THROW_SPEC_NULL;

	DNBSimTime getDuration() const
        DNB_THROW_SPEC_NULL;
	
	DNBReal getVelocity() const
        DNB_THROW_SPEC_NULL;

    virtual DNBMessage
    getEntityTypeName( ) const
        DNB_THROW_SPEC_NULL;

protected:
    DNBBasicDOFTarget( )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit, DNBENetworkError));

    DNBBasicDOFTarget( const DNBBasicDOFTarget &right, CopyMode mode )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit, DNBENetworkError));

    virtual
    ~DNBBasicDOFTarget( )
        DNB_THROW_SPEC_NULL;

private:
	DNBMathVector  DOFvalue_;
	TimingMode  timingMode_;
	DNBReal		velocity_;			//0-1
	DNBSimTime	duration_;
};




#endif  /* _DNB_BASICDOFTARGET_H_ */

