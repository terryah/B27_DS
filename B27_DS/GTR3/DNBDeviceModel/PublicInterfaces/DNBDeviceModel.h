// COPYRIGHT Dassault Systemes 2005
/** @CAA2Required */
/*---------------------------------------------------------------------*/
/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */
/*---------------------------------------------------------------------*/

#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_DEVICEMODEL_H_
#define _DNB_DEVICEMODEL_H_


#ifdef  _WINDOWS_SOURCE
#ifdef  __DNBDeviceModel
#define ExportedByDNBDeviceModel    __declspec(dllexport)
#else
#define ExportedByDNBDeviceModel    __declspec(dllimport)
#endif
#else
#define ExportedByDNBDeviceModel    /* nothing */
#endif


#endif  /* _DNB_DEVICEMODEL_H_ */
