// COPYRIGHT Dassault Systemes 2006

#ifndef _DNB_EXPRUTILS_H_
#define _DNB_EXPRUTILS_H_

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */


#include <DNBSystemBase.h>
#include <scl_string.h>
#include <scl_list.h>
#include <DNBSystemDefs.h>

#include <DNBException.h>
#include "DNBDeviceModel.h"

class CATUnicodeString;


//
// caller must call removeSpaces() before call any other method in this class
//
class ExportedByDNBDeviceModel DNBExprUtils
{
public:
    typedef scl_list<scl_wstring, DNB_ALLOCATOR(scl_wstring) > Symbols;

    DNBExprUtils( )
        DNB_THROW_SPEC_NULL;

    ~DNBExprUtils( )
        DNB_THROW_SPEC_NULL;

    static void
    transformExpr( scl_wstring &expr,
                   const Symbols &symbols, wchar_t symbolDelimiter = L'\"' )
        DNB_THROW_SPEC((DNBEInvalidFormat));

    static void
    removeSpaces( scl_wstring &str )
        DNB_THROW_SPEC_NULL;

    static void
    replaceToken( scl_wstring &str, const scl_wstring &token, const scl_wstring &value );

    // ConvertDecimalToLocale()
    //   This routine expects a string argument that contains only a number, and which may contain
    // a decimal character '.' to represent the decimal.  This routine
    // will replace the decimal with the appropriate locale-defined character.
    //   If the first character is an equals sign, then the string is an IGCALC expression
    // and nothing is done to it.
    // @args ioNumericString - on input, a numerical string that may contain a decimal '.'
    //                       - on output, a numerical string with the locale-defined decimal character
    // e.g.
    // If the locale is defined such that a decimal character is a comma,
    // 3.5 -> 3,5
    // If the locale is defined such that a decimal character is a decimal,
    // 3.5 -> 3.5
    static void ConvertDecimalToLocale(CATUnicodeString &ioNumericString);

    // ConvertLocaleToDecimal()
    //   This routine expects a string that contains only a number, and which may contain
    // a locale-defined decimal character to represent the decimal.  This routine
    // will replace the locale-defined character with a '.'.
    //   If the first character is an equals sign, then the string is an IGCALC expression
    // and nothing is done to it.
    // @args - ioNumericString - on input, a numerical string that may contain a locale-defined
    //                           decimal character
    //                         - on output, a numerical string with the locale-defined decimal
    //                           replaced with a '.' character
    // e.g.
    // If the locale is defined such that a decimal character is a comma,
    // 3,5 -> 3.5
    // If the locale is defined such that a decimal character is a decimal,
    // 3.5 -> 3.5
    static void ConvertLocaleToDecimal(CATUnicodeString &ioNumericString);
    static void ConvertLocaleToDecimal(scl_wstring &ioNumericString);

private:
    static void
    transformTrigFunctions( scl_wstring &in )
        DNB_THROW_SPEC((DNBEInvalidFormat));

    /*
     * Function: replaceConstants()
     *           Searches for constant placeholders "RAD", "DEG" and "PI" in the input
     *           string, and replaces them with a numerical string.  For example, the
     *           string "RAD" will be replaced with the string "0.0174533".
     * @param scl_wstring &ioIn - On input, the string containing the constant variables
     *           to be replaced.  On output, the string with the variables replaced
     *           with their corresponding numerical strings.
     * @param wchar_t symbolDelimiter - A delimiter character used in the input string
     *           indicating sections of the input string that are not to be touched.
     * @return void
     */
    static void
    replaceConstants( scl_wstring &ioIn, wchar_t symbolDelimiter );

    static void
    delimitTemporarySymbols( scl_wstring &in, wchar_t symbolDelimiter )
        DNB_THROW_SPEC((DNBEInvalidFormat));

    static scl_wstring
    advanceToEndOfAlphaNum( const scl_wstring &in, scl_wstring::size_type &pos )
        DNB_THROW_SPEC((DNBEInvalidFormat));

    static void
    advanceToNextDelimiter( const scl_wstring &in, scl_wstring::size_type &pos,
			    wchar_t symbolDelimiter )
        DNB_THROW_SPEC((DNBEInvalidFormat));

    static bool
    isKnownWord( const scl_wstring &foundWord )
        DNB_THROW_SPEC_NULL;

    static bool
    isScientificNotation( const scl_wstring &expr, scl_wstring::size_type pos )
        DNB_THROW_SPEC_NULL;

    static scl_wstring::size_type
    advanceToEndOfNum( const scl_wstring &expr, scl_wstring::size_type pos )
        DNB_THROW_SPEC_NULL;

    static void
    delimit( scl_wstring &in, scl_wstring::size_type from, scl_wstring::size_type to,
	     wchar_t delimiter )
        DNB_THROW_SPEC_NULL;
};


#endif /* _DNB_EXPRUTILS_H_ */
