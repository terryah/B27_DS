// COPYRIGHT Dassault Systemes 2005
//*
//* FILE:
//*     DNBDOFPlannerExt.h      - public header file
//*
//* MODULE:
//*     DNBDOFPlannerExt  - single non-template class
//*
//* OVERVIEW:
//*     This module defines an extension class which is base for all
//*     motion planner classes.
//*	 
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     xin         03/20/00    Initial implementation
//*		xin			11/30/00
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1998 - 2000 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_DOFPLANNEREXT_H_
#define _DNB_DOFPLANNEREXT_H_


/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */



#include <DNBSystemBase.h>
#include <scl_vector.h>
#include <DNBSystemDefs.h>


#include <DNBMotionPlannerExt.h>
#include <DNBBasicDOFTargetSet.h>
#include <DNBDOFPlannerBase.h>
#include <DNBConveyorTrackingEnums.h>


class ExportedByDNBDeviceModel DNBDOFPlannerExt : public DNBMotionPlannerExt
{

    DNB_DECLARE_DYNAMIC_CREATE( DNBDOFPlannerExt );

    DNB_DECLARE_SHARED_OBJECT( DNBDOFPlannerExt );

    DNB_DECLARE_EXTENSION_OBJECT( DNBDOFPlannerExt, DNBBasicDevice3D );

    DNB_DECLARE_EXTENSION_FACTORY( DNBDOFPlannerExt, DNBBasicDevice3D );
    
public:
	inline void addDOFTarget(DNBBasicDOFTarget::Handle hTarget)
    DNB_THROW_SPEC((DNBEDomainError)); 

	inline void addDOFTargetSet(DNBBasicDOFTargetSet::Handle hTargetSet)
    DNB_THROW_SPEC((DNBEDomainError)); 

    inline void clearSetTargets( )
	DNB_THROW_SPEC_NULL;

	inline DNBMathVector 
	evaluate( const DNBSimTime &currentTime )
	DNB_THROW_SPEC((DNBEZeroVector, DNBEZeroDivide, DNBEStop, DNBEHalt,
                        DNBEOverflowError/*, RWTHRInvalidPointer*/));

	inline DNBMathVector 
	evaluate( )
	DNB_THROW_SPEC((DNBEZeroVector, DNBEZeroDivide, DNBEStop, DNBEHalt,
                        DNBEOverflowError/*, RWTHRInvalidPointer*/));

	inline void reset()
		DNB_THROW_SPEC_NULL;

	inline DNBSimTime getMoveTime()
			DNB_THROW_SPEC_NULL;

	inline DNBSimTime getSegmentTime(size_t i)
		DNB_THROW_SPEC_NULL;

	inline void setSamplePeriod(DNBSimTime step)
	DNB_THROW_SPEC_NULL;

	
	inline void resetProcess(void)
	DNB_THROW_SPEC_NULL;

	inline void addDOFPlanner(DNBDOFPlannerBase* newDOFPlanner)
	DNB_THROW_SPEC_NULL;

	void removeDOFPlanner(const scl_wstring& name)
	DNB_THROW_SPEC_NULL;

	bool setCurrentPlanner(const scl_wstring&)
	DNB_THROW_SPEC_NULL;

	inline void getCurrentPlannerName (scl_wstring& name)
	DNB_THROW_SPEC_NULL;

	inline void setInitDOFPos (	const DNBMathVector& )
	DNB_THROW_SPEC_NULL;

	//in absolute value
	inline void setInitDOFSpeed(const DNBMathVector& )
	DNB_THROW_SPEC_NULL;

	inline DNBMathVector 
	getDOFSpeed ()
	DNB_THROW_SPEC_NULL;

	inline DNBMathVector 
	getDOFAccel ()
	DNB_THROW_SPEC_NULL;


	inline bool 
	reachedEndOfMove ()
	DNB_THROW_SPEC_NULL;

    inline void
    setDefaultDOFRounding( DNBReal rounding )
	DNB_THROW_SPEC((DNBERangeError));

    inline DNBReal 
    getDefaultDOFRounding( ) const
	DNB_THROW_SPEC_NULL;

    inline void
    setCurrentDOFRounding( DNBReal rounding )
	DNB_THROW_SPEC((DNBERangeError));

    inline DNBReal 
    getCurrentDOFRounding( ) const
	DNB_THROW_SPEC_NULL;

    inline void
    resetCurrentDOFRounding( )
	DNB_THROW_SPEC_NULL;

    inline void
    setDOFPlannerPeriod( DNBSimTime period )
	DNB_THROW_SPEC((DNBERangeError));

    inline DNBSimTime 
    getDOFPlannerPeriod( ) const
	DNB_THROW_SPEC_NULL;

    inline void
    setDOFMinMotionTime( DNBSimTime time )
	DNB_THROW_SPEC((DNBERangeError));

    inline DNBSimTime 
    getDOFMinMotionTime( ) const
	DNB_THROW_SPEC_NULL;

    inline void
    setDefaultDOFSpeed( DNBReal speed ) 
	DNB_THROW_SPEC_NULL;

    inline DNBReal 
    getDefaultDOFSpeed( ) const
	DNB_THROW_SPEC_NULL;

	inline DNBReal
	PrepareMove(DNBBasicDOFTarget::Handle)
	DNB_THROW_SPEC_NULL;

	inline void
	ScaleMove (const DNBSimTime)
	DNB_THROW_SPEC_NULL;

	inline DNBMathVector
	ExecuteMove (const DNBSimTime)
	DNB_THROW_SPEC_NULL;

    //R18 Highlight implementation
    inline void
    setTrackingMode (const TrackingMode Mode)
    DNB_THROW_SPEC_NULL;

    inline void
    UpdatePosition (const PositionType Type, const DNBMathVector &Position)
    DNB_THROW_SPEC_NULL;

    inline void
    getMoveMaxSpeed (const  DNBInteger32    Index,
                            DNBMathVector   &Speed)
    DNB_THROW_SPEC_NULL;

    inline void
    MoveMaxSpeedCalculation   (const bool Enable = false)
    DNB_THROW_SPEC_NULL;


#if 0
    inline void
    setCurrentDOFSpeed( DNBReal speed ) 
	DNB_THROW_SPEC_NULL;

    inline DNBReal 
    getCurrentDOFSpeed( ) const
	DNB_THROW_SPEC_NULL;

    inline void
    resetCurrentDOFSpeed( )
	DNB_THROW_SPEC_NULL;
#endif

protected:
    DNBDOFPlannerExt( )
	DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit));

    DNBDOFPlannerExt( const DNBDOFPlannerExt &other,
			    CopyMode mode )
	DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit));

    virtual
    ~DNBDOFPlannerExt( )
	DNB_THROW_SPEC_NULL;



	typedef scl_vector< DNBDOFPlannerBase*, DNB_ALLOCATOR(DNBDOFPlannerBase*) >
        DNBDOFPlannerDB;

private:
    DNBReal	    DefaultDOFRounding_;
    DNBReal	    CurrentDOFRounding_;
    DNBSimTime 	    DOFMinMotionTime_;

	DNBDOFPlannerBase* currentDOFPlanner;
	DNBDOFPlannerDB   DOFPlanners_;
};


#include "DNBDOFPlannerExt.cc"


#endif  /* _DNB_DOFPLANNEREXT_H_ */
