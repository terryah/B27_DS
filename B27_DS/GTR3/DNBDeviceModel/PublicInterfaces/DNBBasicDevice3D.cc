// COPYRIGHT Dassault Systemes 2005
/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

/**
 * @quickReview SHA 03:03:21
 *
 * @quickReview CZO 03:08:12
 * @quickReview KPR 05:12:22
 * @quickreview KPR 06:02:17
 */
//==============================================================================
//
//     mmg      02/17/2003   fixed problem with casting in calls to 
//                           getEntityLocation()
//     gsx      07/11/2003   Modified to support soft limits
//     gsx	08/28/03     Modifications for soft limit margin
//     pjr      12/22/2005   To Fix C0510630: A tolerance of One Micron is included
//                           for joint value w.r.t the joint limits.
//==============================================================================

#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>

#include <DNBExpressionSolver.h>
#include <DNBExprUtils.h>
#include <DNBSIUnit.h>

#ifndef ONE_MICRON
#define ONE_MICRON 0.000001
#endif


inline void
DNBBasicDevice3D::setMountOffset( const DNBXform3D &mountOffset )
    DNB_THROW_SPEC_NULL
{
	DNB_ASSERT( currDeviceKinChain );
	currDeviceKinChain->mountOffsetInv_ = currDeviceKinChain->mountOffset_ = mountOffset;
    currDeviceKinChain->mountOffsetInv_.invert();
}


inline DNBXform3D
DNBBasicDevice3D::getMountOffset( ) const
    DNB_THROW_SPEC_NULL
{
    DNB_ASSERT( currDeviceKinChain );
	return currDeviceKinChain->mountOffset_;
}

inline DNBXform3D
DNBBasicDevice3D::getMountLocation( DNBBasicEntity3D::Handle hEntity ) const
    DNB_THROW_SPEC_NULL
{
    return getMountPartLocation( hEntity ) * getMountOffset();
}


inline DNBXform3D
DNBBasicDevice3D::getMountPartLocation( DNBBasicEntity3D::Handle hEntity ) const
    DNB_THROW_SPEC_NULL
{
    DNB_ASSERT( currDeviceKinChain );
	DNBXform3D xform( getEntityLocation( hEntity ) );
    xform.invert();
    DNBBasicEntity3D::Handle tmp( currDeviceKinChain->hMountPart_ );
    return xform * getEntityLocation( tmp );
}

inline DNBXform3D
DNBBasicDevice3D::getMountDisplacement( ) const
    DNB_THROW_SPEC_NULL
{
    return getMountPartDisplacement() * getMountOffset();
}

inline DNBXform3D
DNBBasicDevice3D::getMountPartDisplacement( ) const
    DNB_THROW_SPEC_NULL
{
    DNB_ASSERT( currDeviceKinChain );
	DNBBasicEntity3D::Handle tmp( currDeviceKinChain->hBasePart_ );
    DNBXform3D xform( getEntityLocation( tmp ));
    xform.invert();
    DNBBasicEntity3D::Handle tmp2( currDeviceKinChain->hMountPart_ );
    return xform * getEntityLocation( tmp2 );
}

inline void
DNBBasicDevice3D::setMountPart( DNBBasicBody3D::Handle hMountPart )
    DNB_THROW_SPEC_NULL
{
    DNB_ASSERT( currDeviceKinChain );
	DNB_ASSERT( hMountPart );
    currDeviceKinChain->hMountPart_ = hMountPart;
}


inline DNBBasicBody3D::Handle
DNBBasicDevice3D::getMountPart( ) const
    DNB_THROW_SPEC_NULL
{
    DNB_ASSERT( currDeviceKinChain );
	return currDeviceKinChain->hMountPart_;
}

inline void
DNBBasicDevice3D::setBasePart( DNBBasicBody3D::Handle hBasePart )
    DNB_THROW_SPEC_NULL
{
    DNB_ASSERT( hBasePart );
    currDeviceKinChain->hBasePart_ = hBasePart;
}



inline DNBBasicBody3D::Handle
DNBBasicDevice3D::getBasePart( ) const
    DNB_THROW_SPEC_NULL
{
    return currDeviceKinChain->hBasePart_;
}



inline void
DNBBasicDevice3D::setDOFDisplacementsDefault( const DNBMathVector& DOFValues )
    DNB_THROW_SPEC_NULL
{

    DNB_PRECONDITION( DOFValues.length() == DOFCount_ );
    DOFDisplacementsDefault_ = DOFValues;

}


inline void
DNBBasicDevice3D::getDOFDisplacementsDefault( DNBMathVector& DOFValues ) const
    DNB_THROW_SPEC_NULL
{
    DOFValues = DOFDisplacementsDefault_; 

}



inline void
DNBBasicDevice3D::addDOFDisplacementLimits( const DNBReal&  lower, const DNBReal& upper, size_t index ) 
	DNB_THROW_SPEC_NULL
{
    DNB_ASSERT( index < DOFCount_ );


    DOFLimits_[ index ].displacementLower_ = lower < upper ? lower : upper; 
    DOFLimits_[ index ].displacementUpper_ = lower < upper ? upper : lower; 
    DOFLimits_[ index ].isDisConst_ = true;
}

inline void
DNBBasicDevice3D::addDOFDisplacementCautionZone( const DNBReal&  marginValue, 
			bool isAbsoluteValue, size_t index )
	DNB_THROW_SPEC_NULL
{
    DNB_ASSERT( index < DOFCount_ );

    DOFLimits_[ index ].marginValue_ = marginValue;
    DOFLimits_[ index ].isSoftLimitPresent_ = true;
    DOFLimits_[ index ].isAbsoluteValue_ = isAbsoluteValue;
}


inline void
DNBBasicDevice3D::addDOFDisplacementLimits( const scl_wstring &lower,
                                            const scl_wstring &upper,
                                            size_t idx )
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEInvalidFormat, DNBEDoesNotExist ))
{
    DNB_ASSERT( idx < DOFCount_ );

    DOFLimits_[ idx ].disLowerExpr_ = lower;
    DOFLimits_[ idx ].disUpperExpr_ = upper;

    DNBWDMSymbolTable   symbolTable = getSymbolTable( );
    scl_wstring tmp( lower );
    DNBExprUtils::ConvertLocaleToDecimal(tmp);

    DNBExprUtils::removeSpaces( tmp );
    DNBExprUtils::transformExpr( tmp, DNBExprUtils::Symbols() );
    DOFLimits_[ idx ].solverLower_ = 
            DNB_NEW DNBExpressionSolver<DNBReal>( tmp, symbolTable );

    tmp = upper;
    DNBExprUtils::ConvertLocaleToDecimal(tmp);
    DNBExprUtils::removeSpaces( tmp );
    DNBExprUtils::transformExpr( tmp, DNBExprUtils::Symbols() );
    DOFLimits_[ idx ].solverUpper_ = 
            DNB_NEW DNBExpressionSolver<DNBReal>( tmp, symbolTable );

    DOFLimits_[ idx ].isDisConst_ = false;
}


inline DNBReal
DNBBasicDevice3D::getDOFDisplacementLimitLower( size_t idx ) const 
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEDoesNotExist, DNBEZeroDivide,
                     DNBEOverflowError, DNBEUnderflowError ))
{
    DNB_ASSERT( idx < DOFCount_ );

    if( DOFLimits_[idx].isDisConst_ )
    {
        return DOFLimits_[idx].displacementLower_;
    }
    else
    {
	if( getDOFType( idx ) == Linear )
            return DOFLimits_[idx].solverLower_->getValue() * DNBSIUnit::Milli;
        else
            return DOFLimits_[idx].solverLower_->getValue();
    }
}


inline DNBReal
DNBBasicDevice3D::getDOFDisplacementLimitUpper( size_t idx ) const 
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEDoesNotExist, DNBEZeroDivide,
                     DNBEOverflowError, DNBEUnderflowError ))
{
    DNB_ASSERT( idx < DOFCount_ );

    if( DOFLimits_[idx].isDisConst_ )
    {
        return DOFLimits_[idx].displacementUpper_;
    }
    else
    {
	if( getDOFType( idx ) == Linear )
            return DOFLimits_[idx].solverUpper_->getValue() * DNBSIUnit::Milli;
	else
            return DOFLimits_[idx].solverUpper_->getValue();
    }
}

inline DNBReal
DNBBasicDevice3D::getDOFDisplacementCautionZoneLower( size_t idx ) const 
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEDoesNotExist, DNBEZeroDivide,
                     DNBEOverflowError, DNBEUnderflowError ))
{
    DNB_ASSERT( idx < DOFCount_ );

    DNBReal upper = getDOFDisplacementLimitUpper( idx );
    DNBReal lower = getDOFDisplacementLimitLower( idx );
    DNBReal range = upper - lower;
    DNBReal lowerSoft;
    if ( DOFLimits_[idx].isAbsoluteValue_ == TRUE )
	lowerSoft = lower + DOFLimits_[idx].marginValue_;
    else
	lowerSoft = lower + 
	    (range * DOFLimits_[idx].marginValue_);

    return lowerSoft;
}

inline DNBReal
DNBBasicDevice3D::getDOFDisplacementCautionZoneUpper( size_t idx ) const 
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEDoesNotExist, DNBEZeroDivide,
                     DNBEOverflowError, DNBEUnderflowError ))
{
    DNB_ASSERT( idx < DOFCount_ );

    DNBReal upper = getDOFDisplacementLimitUpper( idx );
    DNBReal lower = getDOFDisplacementLimitLower( idx );
    DNBReal range = upper - lower;
    DNBReal upperSoft;
    if ( DOFLimits_[idx].isAbsoluteValue_ == TRUE )
	upperSoft = upper - DOFLimits_[idx].marginValue_;
    else
	upperSoft = upper - 
		(range * DOFLimits_[idx].marginValue_);

    return upperSoft;
}


inline bool
DNBBasicDevice3D::isDOFDisplacementWithinLimits( const DNBReal& displacement, 
						 size_t index ) const
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEDoesNotExist, DNBEZeroDivide,
                     DNBEOverflowError, DNBEUnderflowError ))
{
    DNB_ASSERT( index < DOFCount_ );
    return ( displacement >= getDOFDisplacementLimitLower( index )-ONE_MICRON &&  
             displacement <= getDOFDisplacementLimitUpper( index )+ONE_MICRON ?
	     true : false ); 
}

inline bool
DNBBasicDevice3D::isDOFDisplacementValid( const DNBReal& 
	    displacement, size_t index, DNBKinStatus& kinStatus ) const
	DNB_THROW_SPEC(( scl_bad_alloc, DNBEDoesNotExist, DNBEZeroDivide,
	                 DNBEOverflowError, DNBEUnderflowError ))
{
    DNB_ASSERT( index < DOFCount_ );
    if ( (DOFLimits_[index].isSoftLimitPresent_ == true) &&
		  (DOFSoftLimitStatus_ != DNB_ANALYSIS_OFF) )
    {
        if( !isDOFDisplacementWithinCautionZone(displacement,index))
        {
	    // Check whether the displacement is within hard limits or not
	    // If displacement is outside hard limits, then automatically
	    // it will be outside caution zone. So to set the kinStatus
	    // properly make hard limits check also
	    if( !isDOFDisplacementWithinLimits(displacement,index))
	        kinStatus = DOFHardLimitExceeded;
	    else
	        kinStatus = DOFCautionZoneExceeded;
	    return false;
	}
    }
    else 
    {
        if( !isDOFDisplacementWithinLimits( displacement, index ))
        {
	    kinStatus = DOFHardLimitExceeded;
	    return false;
        }
    }
    return true;
}

inline bool
DNBBasicDevice3D::isDOFDisplacementWithinCautionZone( const DNBReal& displacement,size_t index ) const
    DNB_THROW_SPEC(( scl_bad_alloc, DNBEDoesNotExist, DNBEZeroDivide,
                     DNBEOverflowError, DNBEUnderflowError ))
{
    DNB_ASSERT( index < DOFCount_ );
    // If no caution zone is defined, check for the hard limits
    if ( DOFLimits_[index].isSoftLimitPresent_ == false )
    {
	return ( isDOFDisplacementWithinLimits( displacement, index ) );
    }

    return ( displacement >= getDOFDisplacementCautionZoneLower( index )-ONE_MICRON &&  
             displacement <= getDOFDisplacementCautionZoneUpper( index )+ONE_MICRON ?
	     true : false ); 
}


inline void
DNBBasicDevice3D::addDOFVelocityLimits( const DNBReal&  lower, const DNBReal& upper, size_t index ) 
	DNB_THROW_SPEC_NULL
{
    DNB_ASSERT( index < DOFCount_ );

    DOFLimits_[ index ].velocityLower_ = lower < upper ? lower : upper; 
    DOFLimits_[ index ].velocityUpper_ = lower < upper ? upper : lower; 

}


inline DNBReal
DNBBasicDevice3D::getDOFVelocityLimitLower( size_t index ) const 
	DNB_THROW_SPEC_NULL
{
    DNB_ASSERT( index < DOFCount_ );

    return DOFLimits_[index].velocityLower_;

}


inline DNBReal
DNBBasicDevice3D::getDOFVelocityLimitUpper( size_t index ) const 
	DNB_THROW_SPEC_NULL
{
    DNB_ASSERT( index < DOFCount_ );

    return DOFLimits_[index].velocityUpper_;

}


inline bool
DNBBasicDevice3D::isDOFVelocityWithinLimits( const DNBReal& velocity, 
					     size_t index ) const
	DNB_THROW_SPEC_NULL 
{
    DNB_ASSERT( index < DOFCount_ );
    return ( fabs(velocity) >= DOFLimits_[ index ].velocityLower_  &&  
             fabs(velocity) <= DOFLimits_[ index ].velocityUpper_ ?
	     true : false ); 

}


inline void
DNBBasicDevice3D::addDOFAccelerationLimits( const DNBReal&  lower, const DNBReal& upper, size_t index ) 
	DNB_THROW_SPEC_NULL
{
    DNB_ASSERT( index < DOFCount_ );

    DOFLimits_[ index ].accelerationLower_ = lower < upper ? lower : upper; 
    DOFLimits_[ index ].accelerationUpper_ = lower < upper ? upper : lower; 

}


inline DNBReal
DNBBasicDevice3D::getDOFAccelerationLimitLower( size_t index ) const 
	DNB_THROW_SPEC_NULL
{
    DNB_ASSERT( index < DOFCount_ );

    return DOFLimits_[index].accelerationLower_;

}


inline DNBReal
DNBBasicDevice3D::getDOFAccelerationLimitUpper( size_t index ) const 
	DNB_THROW_SPEC_NULL
{
    DNB_ASSERT( index < DOFCount_ );

    return DOFLimits_[index].accelerationUpper_;

}


inline bool
DNBBasicDevice3D::isDOFAccelerationWithinLimits( const DNBReal& acceleration, 
					         size_t index ) const
	DNB_THROW_SPEC_NULL 
{
    DNB_ASSERT( index < DOFCount_ );
    return ( fabs(acceleration) >= DOFLimits_[ index ].accelerationLower_  &&  
             fabs(acceleration) <= DOFLimits_[ index ].accelerationUpper_ ?
	     true : false ); 

}




inline DNBWDMSymbolTable
DNBBasicDevice3D::getSymbolTable( ) const
    DNB_THROW_SPEC_NULL
{
    return symbTable_;

}



#if 0

inline void
DNBBasicDevice3D::dump( ostream& oStr ) const 
        DNB_THROW_SPEC_NULL
{
    oStr << "Device dump" << endl;
    DNBBasicMfgAssembly3D::dump( oStr );

}

#endif
