// COPYRIGHT Dassault Systemes 2005
//
//  Notes:
//    + None
//
//  TODO:
//    + Define the class DNBBasicDOFProfile to model the complete state of a DOF
//      vector.  This class includes the type, name, displacement, velocity, and
//      acceleration of each degree-of-freedom.
//
//    + Add the methods setDOFProfile() and getDOFProfile().
//
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_BASICCONSTRAINT3D_H_
#define _DNB_BASICCONSTRAINT3D_H_

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */


#include <DNBSystemBase.h>
#include <scl_vector.h>
#include <scl_string.h>  // SMW july 5
#include <DNBSystemDefs.h>

#include <DNBDeviceModel.h>
#include <DNBBasicConnection3D.h>
#include <DNBMathVector.h>


//
//  This class represents an abstract constraint between a pair of bodies.  In
//  general, constraints define restrictions on system configuration imposed by
//  mechanical joints and other connections.  They limit one or more degrees of
//  freedom between the frame of one part and the frame of another part.
//
class ExportedByDNBDeviceModel DNBBasicJoint3D : public DNBBasicConnection3D
{
    DNB_DECLARE_DYNAMIC_RTTI( DNBBasicJoint3D );

    DNB_DECLARE_SHARED_OBJECT( DNBBasicJoint3D );

    DNB_DECLARE_EXTENDIBLE_OBJECT( DNBBasicJoint3D );

public:
    virtual DNBMessage
    getEntityTypeName( ) const
        DNB_THROW_SPEC_NULL;

    //
    //  DOF count
    //
    size_t
    getDOFCount( ) const
        DNB_THROW_SPEC_NULL;

    //
    //  DOF types
    //
    enum DOFType
    {                                   // Units:
        Linear,                         // meters
        Angular,                        // radians
        Unitless                        // none
    };

    typedef scl_vector<DOFType, DNB_ALLOCATOR(DOFType) >    DOFTypeDB;

    DOFType
    getDOFType( size_t index ) const
        DNB_THROW_SPEC_NULL;

    void
    getDOFTypes( DOFTypeDB &types ) const
        DNB_THROW_SPEC_NULL;

    //
    //  DOF names
    //
    typedef scl_vector<scl_wstring, DNB_ALLOCATOR(scl_wstring) >    DOFNameDB;

    void
    setDOFName( size_t index, const scl_wstring &name )
        DNB_THROW_SPEC_NULL;

    const scl_wstring &
    getDOFName( size_t index ) const
        DNB_THROW_SPEC_NULL;

    void
    setDOFNames( const DOFNameDB &names )
        DNB_THROW_SPEC_NULL;

    void
    getDOFNames( DOFNameDB &names ) const
        DNB_THROW_SPEC_NULL;

    //
    //  DOF displacements
    //
    RequestStatus
    setDOFDisplacement( size_t index, DNBReal &displacement )
        DNB_THROW_SPEC_NULL;

    DNBReal
    getDOFDisplacement( size_t index ) const
        DNB_THROW_SPEC_NULL;

    RequestStatus
    setDOFDisplacements( DNBMathVector &displacements )
        DNB_THROW_SPEC_NULL;

    void
    getDOFDisplacements( DNBMathVector &displacements ) const
        DNB_THROW_SPEC_NULL;

    virtual DNBXform3D
    calcVariableLocation( DNBMathVector &displacements ) const
        DNB_THROW_SPEC_NULL = 0;

    enum Events
    {
        EventFirst = DNBBasicConnection3D::EventLast - 1,
        EventDOFCountModified,
        EventDOFTypeModified,
        EventDOFNameModified,
        EventDOFDisplacementModified,   // Instantaneous change in displacement
        EventDOFProfileModified,        // Complete profile modified
        EventLast
    };

protected:
    //
    //  By defining the following methods in the protected section, this class
    //  can only be used as a base class.
    //
    DNBBasicJoint3D( )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit, DNBENetworkError));

    DNBBasicJoint3D( const DNBBasicJoint3D &right, CopyMode mode )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit, DNBENetworkError));

    virtual
    ~DNBBasicJoint3D( )
        DNB_THROW_SPEC_NULL;

    //
    //  The following methods are used to construct an actual constraint.
    //
    void
    setDOFCount( size_t count )
        DNB_THROW_SPEC((scl_bad_alloc));

    void
    setDOFType( size_t index, DOFType type )
        DNB_THROW_SPEC_NULL;

    void
    setDOFTypes( const DOFTypeDB &types )
        DNB_THROW_SPEC_NULL;

    //
    //  The following method is used to get the variable (or joint) location
    //  of the outer frame with respect to the inner frame.  Each derived class
    //  must provide an implementation of this method.
    //
    virtual DNBXform3D
    getVariableLocation( ) const
        DNB_THROW_SPEC_NULL = 0;

    //
    //  The following method is used to compute the relative location of the
    //  outer body with respect to the inner body, given the specified DOF
    //  displacements.  Each derived class must provide an implementation of
    //  this method.
    //
    virtual RequestStatus
    updateDOFDisplacements( DNBMathVector &displacements )
        DNB_THROW_SPEC_NULL = 0;

private:
    size_t          DOFCount_;          // TODO: Store in DOFProfile_.
    DOFTypeDB       DOFTypeDB_;         // TODO: Store in DOFProfile_.
    DOFNameDB       DOFNameDB_;         // TODO: Store in DOFProfile_.
    DNBMathVector   DOFProfile_;        // TODO: Change type to DNBDOFProfile.

    friend 	    class 		DNBForwardKinSolverDirect;
};




//
//  Include the public definition file.
//
#include "DNBBasicJoint3D.cc"




#endif  /* _DNB_BASICCONSTRAINT3D_H_ */
