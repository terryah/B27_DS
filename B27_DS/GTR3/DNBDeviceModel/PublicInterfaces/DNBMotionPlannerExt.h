// COPYRIGHT Dassault Systemes 2005
//*
//* FILE:
//*     DNBMotionPlannerExt.h      - public header file
//*
//* MODULE:
//*     DNBMotionPlannerExt  - single non-template class
//*
//* OVERVIEW:
//*     This module defines an extension class which is base for all
//*     motion planner classes.
//*	 
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     xin         03/05/00    Initial implementation
//*		xin			11/30/00
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1998 - 2000 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_MOTIONPLANNEREXT_H_
#define _DNB_MOTIONPLANNEREXT_H_

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */



#include <DNBSystemBase.h>
#include <scl_vector.h>
#include <DNBSystemDefs.h>


#include <DNBDeviceModel.h>
#include <DNBExtensionObject.h>
////

#include <DNBSimTime.h>
#include <DNBSimletException.h>
#include <DNBBasicDevice3D.h>


class ExportedByDNBDeviceModel DNBMotionPlannerExt : public DNBExtensionObject
{

    DNB_DECLARE_DYNAMIC_RTTI( DNBMotionPlannerExt );

    DNB_DECLARE_SHARED_OBJECT( DNBMotionPlannerExt );

    DNB_DECLARE_EXTENSION_OBJECT( DNBMotionPlannerExt, DNBBasicDevice3D );



public:
	bool acquirePlanner()
	DNB_THROW_SPEC_NULL;

	bool releasePlanner()
	DNB_THROW_SPEC_NULL;

   	enum Events
    {
        EventFirst = DNBExtensionObject::EventLast - 1,
		EventNextLocationReached,
		EndLocationReached,
        EventLast
    };

	inline void postEvent(size_t);
protected:

    

	enum State
	{
		ACTIVE,
		INACTIVE
	};



	DNBMotionPlannerExt( )
	DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit));

    DNBMotionPlannerExt( const DNBMotionPlannerExt &other,
			    CopyMode mode )
	DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit));

    virtual
    ~DNBMotionPlannerExt( )
	DNB_THROW_SPEC_NULL;


private:

	State state_;
};


#include "DNBMotionPlannerExt.cc"

#endif  /* _DNB_MOTIONPLANNEREXT_H_ */
