// COPYRIGHT Dassault Systemes 2005
//
//  Notes:
//    + None
//
//    mmg   01/24/03    added isHomePosition()
//    gsx   07/11/03    Modified for supporting soft limits
//    gsx   08/28/03    Modifications for soft limits margin value
//
//  TODO:
//    + Use-case where a body added is a parent - propagation deadlock.
//	At present assert failure in propagateBaseLocation.
// 
//
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_BASICDEVICE3D_H_
#define _DNB_BASICDEVICE3D_H_

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

#include <DNBSystemBase.h>
#include <scl_vector.h>
#include <scl_map.h>
#include <scl_utility.h>
#include <DNBSystemDefs.h>

#include <DNBDeviceModel.h>
#include <DNBBasicMfgAssembly3D.h>
#include <DNBBasicBody3D.h>
#include <DNBBasicPart3D.h>
#include <DNBBasicJoint3D.h>
#include <DNBMathVector.h>
#include <DNBBasicDOFTarget.h>
#include <DNBBasicDOFTargetSet.h>
#include <DNBWDMSymbolTable.h>
#include <DNBExpressionSolver.h>
#include <DNBAuxDeviceType.h>
#include <DNBAnalysisMode.h>
#include <DNBKinStatus.h>


class DNBForwardKinSolverGraph;
class DNBForwardKinSolver;
//class DNBKinStatus;


//
//  This class represents an articulated device.
//
class ExportedByDNBDeviceModel DNBBasicDevice3D : public DNBBasicMfgAssembly3D
{
    DNB_DECLARE_DYNAMIC_CREATE( DNBBasicDevice3D );

    DNB_DECLARE_SHARED_OBJECT( DNBBasicDevice3D );

    DNB_DECLARE_EXTENDIBLE_OBJECT( DNBBasicDevice3D );

    DNB_DECLARE_EXTENDIBLE_FACTORY( DNBBasicDevice3D );

public:
    virtual DNBMessage
    getEntityTypeName( ) const
        DNB_THROW_SPEC_NULL;

    void
    setForwardKinSolver( DNBForwardKinSolver* pFKin )
        DNB_THROW_SPEC((DNBEAlreadyRegistered));

    DNBForwardKinSolver* 
    getForwardKinSolver( ) const
        DNB_THROW_SPEC_NULL;

    void 
    clearForwardKinSolver( ) 
        DNB_THROW_SPEC_NULL;

    //
    //  DOF count
    //
    void
    setDOFCount( size_t count )
        DNB_THROW_SPEC((scl_bad_alloc));

    size_t
    getDOFCount( ) const
        DNB_THROW_SPEC_NULL;

    //
    //  DOF types
    //
    enum DOFType
    {                                   // Units:
        Linear,                         // meters
        Angular,                        // radians
        Unitless                        // none
    };

    typedef scl_vector<DOFType, DNB_ALLOCATOR(DOFType) >    DOFTypeDB;
	typedef scl_vector<bool, DNB_ALLOCATOR(bool) > DNBBoolVector;

    void
    setDOFType( size_t index, DOFType type )
        DNB_THROW_SPEC_NULL;

    DOFType
    getDOFType( size_t index ) const
        DNB_THROW_SPEC_NULL;

    void
    setDOFTypes( const DOFTypeDB &types )
        DNB_THROW_SPEC_NULL;

    void
    getDOFTypes( DOFTypeDB &types ) const
        DNB_THROW_SPEC_NULL;

    //
    //  DOF names
    //
    typedef scl_vector<scl_wstring, DNB_ALLOCATOR(scl_wstring) >    DOFNameDB;

    void
    setDOFName( size_t index, const scl_wstring &name )
        DNB_THROW_SPEC_NULL;

    const scl_wstring &
    getDOFName( size_t index ) const
        DNB_THROW_SPEC_NULL;

    void
    setDOFNames( const DOFNameDB &names )
        DNB_THROW_SPEC_NULL;

    void
    getDOFNames( DOFNameDB &names ) const
        DNB_THROW_SPEC_NULL;
    
    bool
    addAuxiliaryDevice( DNBBasicDevice3D::Handle hAuxDev, 
                        DNBAuxDeviceType type )
	DNB_THROW_SPEC_NULL;

    bool
    removeAuxiliaryDevice( DNBBasicDevice3D::Handle hAuxDev )
	DNB_THROW_SPEC_NULL;

    size_t
    getAuxiliaryDeviceCount() const
	DNB_THROW_SPEC_NULL;

    void
    getAuxiliaryDevices( DNBBasicDevice3D::Vector &auxDevices,
	                 DNBAuxDeviceType type ) const
	DNB_THROW_SPEC_NULL;

    void
    getAuxiliaryDevices( DNBBasicDevice3D::Vector &auxDevices ) const
	DNB_THROW_SPEC_NULL;

    //
    // Returns auxiliary device corresponding to Device Systems 
    // DOF index DOFIndex.
    //
    DNBBasicDevice3D::Handle
    getAuxiliaryDevice( size_t DOFIndex ) const
        DNB_THROW_SPEC_NULL;

    //
    //The first portion of dofDisplacements is always the robot's dof values,
    //the rest is determined by this call. Otherwise the order that the aux
    //devices are added is assumed.
    //
    bool
    setAuxiliaryDeviceOrder( const DNBBasicDevice3D::Vector &auxDevs )
        DNB_THROW_SPEC_NULL;

    size_t
    getSystemDOFCount() const
        DNB_THROW_SPEC_NULL;

    void
    getSystemDOFDisplacements( DNBMathVector &displacements ) const
        DNB_THROW_SPEC_NULL;

    RequestStatus
    setSystemDOFDisplacements( DNBMathVector &displacements )
        DNB_THROW_SPEC_NULL;

    DNBBasicBody3D::Handle
    getSystemFixedPart( ) const
        DNB_THROW_SPEC_NULL;

    RequestStatus
    calcSystemFwdKin( const DNBMathVector &displacements ) const
        DNB_THROW_SPEC_NULL;


    struct  DeviceSubEntry 
    {
        DNBXform3D                      absXform_;
        DNBXform3D                      relXform_;
        DNBXform3D                      relXformFixedPart_;
    };

    typedef scl_map < DNBBasicBody3D::Handle, DeviceSubEntry, scl_less <DNBBasicBody3D::Handle> > 	DeviceSubEntryDB;


    //
    // Line of Action related interface
    //
    struct ExportedByDNBDeviceModel DirectDrive
    {
	size_t 		          	 deviceDOF_; // device dof
	DNBBasicJoint3D::DOFType         DOFType_;   // angular, etc.	
	DNBBasicBody3D::Handle           hInnerBody_;  
	DNBBasicBody3D::Handle    	 hOuterBody_;
	DNBVector3D			 positionLOA_;     // position of LOA
	DNBVector3D			 directionLOA_; // direction of LOA 
	DNBMath3D::AxisType              axis_;	
	
	// copy constructor needed to fix UMR errors due to "holes" in struct ( copy constructor is
	// called when pushing instances of the struct into an STL container )
	DirectDrive( const DirectDrive & another );
	// Because added copy construcor, now need default constructor and assignment operator
	DirectDrive();
	void operator=( const DirectDrive & another );

    };
    typedef scl_vector< DirectDrive, DNB_ALLOCATOR( DirectDrive ) >	DirectDriveDB;

    void
    getDirectDrives( DirectDriveDB& drives ) const
        DNB_THROW_SPEC((scl_bad_alloc));


    DNBBasicDevice3D::RequestStatus
    doForwardKinematics( const DNBMathVector &displacements )
        DNB_THROW_SPEC_NULL;

    //
    //  DOF displacements
    //

    enum DOFState { 
	   DOFCautionZoneExceeded = 1,
	   DOFHardLimitExceeded = 3 };

    RequestStatus
    setDOFDisplacement( size_t index, DNBReal &displacement )
        DNB_THROW_SPEC_NULL;

    DNBReal
    getDOFDisplacement( size_t index ) const
        DNB_THROW_SPEC_NULL;


    RequestStatus
    setDOFDisplacements( DNBMathVector &displacements )
        DNB_THROW_SPEC_NULL;

    ////////////////////////////////////////////////////////////////////
    //                                                                //
    // status is set to DOFHardLimitExceeded and DOFSoftLimitExceeded //
    // if DOF hard limit is exceeded and DOFSoftLimitExceeded if soft //
    // limit is exceeded.                                             //
    // Note: as of 01/14/02, soft limits are not defined.             //
    //                                                                //
    //////////////////////////////////////////////////////////////////// 
    RequestStatus
    setDOFDisplacements( DNBMathVector &displacements,
			 DNBKinStatus  &status )
        DNB_THROW_SPEC_NULL;

    void
    getDOFDisplacements( DNBMathVector &displacements ) const
        DNB_THROW_SPEC_NULL;
 
    //////////////////////////////////////////////////////////////////////
    //                                                                  // 
    // Returns bool 'true' if all DOF's are within limits; returns bool //
    // 'false' if any DOF is not within limits.                         //
    //                                                                  //
    //////////////////////////////////////////////////////////////////////
    bool
    areDOFDisplacementsWithinLimits( const DNBMathVector& displacement ) const
	DNB_THROW_SPEC(( scl_bad_alloc ));

    //////////////////////////////////////////////////////////////////////
    //                                                                  //
    // Returns bool 'true' if all DOF's are within limits; returns bool //
    // 'false' if any DOF is not within limits; also returns those DOF's// 
    // in the empty vector 'dofs' provided as an input.                 //
    //                                                                  //
    //////////////////////////////////////////////////////////////////////
    bool
    areDOFDisplacementsWithinLimits( 
			   const DNBMathVector                    &displacement,
			   scl_vector<size_t, DNB_ALLOCATOR(size_t) > &dofs,
			   bool basedOnGiven = false ) const
	DNB_THROW_SPEC(( scl_bad_alloc )); 

    // gsx
    //////////////////////////////////////////////////////////////////////
    //                                                                  // 
    // Returns bool 'true' if all DOF's are within soft limits; returns //
    // bool 'false' if any DOF is not within soft limits.               //
    //                                                                  //
    //////////////////////////////////////////////////////////////////////
    bool
    areDOFDisplacementsWithinCautionZone( const DNBMathVector& displacement ) 
		    const DNB_THROW_SPEC(( scl_bad_alloc ));

    //////////////////////////////////////////////////////////////////////
    //                                                                  // 
    // This will depend on the caution zone setting and caution zone    //
    // analysis state settings. 					//
    //                                                                  //
    //////////////////////////////////////////////////////////////////////
    bool
    areDOFDisplacementsValid( const DNBMathVector& displacement,
				DNBKinStatus& kinStatus ,bool basedOnGiven = false) 
		    const DNB_THROW_SPEC(( scl_bad_alloc ));
    
     inline bool
    isDOFDisplacementValid( const DNBReal& displacement,
                     size_t index, DNBKinStatus& kinStatus ) const
	DNB_THROW_SPEC(( scl_bad_alloc, DNBEDoesNotExist, DNBEZeroDivide,
	                 DNBEOverflowError, DNBEUnderflowError ));
     
    void
    setFixedPart( DNBBasicBody3D::Handle hBody )
        DNB_THROW_SPEC_NULL;

    DNBBasicBody3D::Handle
    getFixedPart( ) const
        DNB_THROW_SPEC_NULL;

    // To set the mount offset for the active kin chain
	inline void
    setMountOffset( const DNBXform3D &mountOffset )
        DNB_THROW_SPEC_NULL;

    // To get the mount offset of the current active kin chain
	inline DNBXform3D
    getMountOffset( ) const
        DNB_THROW_SPEC_NULL;

    // To set the mount part for the current active kin chain
	inline void
    setMountPart( DNBBasicBody3D::Handle hMountPart )
        DNB_THROW_SPEC_NULL;

    // To get the mount part of the current active kin chain
	inline DNBBasicBody3D::Handle
    getMountPart( ) const
        DNB_THROW_SPEC_NULL;
    
    inline void
    setBasePart( DNBBasicBody3D::Handle hBasePart )
        DNB_THROW_SPEC_NULL;

    inline DNBBasicBody3D::Handle
    getBasePart( ) const
        DNB_THROW_SPEC_NULL;


    inline DNBXform3D
    getMountLocation( DNBBasicEntity3D::Handle hEntity = NULL ) const
        DNB_THROW_SPEC_NULL;

    //
    // To be deprecated; 01/14/02
    // ( To get Mount Part location for the current active kin chain )
    inline DNBXform3D
    getMountPartLocation( DNBBasicEntity3D::Handle hEntity = NULL ) const
        DNB_THROW_SPEC_NULL;

    inline DNBXform3D
    getMountDisplacement( ) const
        DNB_THROW_SPEC_NULL;

    //
    // To be deprecated; 01/14/02
    // ( To get Mount Part displacement for the current active kin chain )
    inline DNBXform3D
    getMountPartDisplacement( ) const
        DNB_THROW_SPEC_NULL;

    void
    addJoint( DNBBasicConnection3D::Handle hJoint )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEAlreadyRegistered));

    bool
    hasJoint( DNBBasicJoint3D::Handle hJoint ) const
        DNB_THROW_SPEC_NULL;


    DNBBasicJoint3D::Handle
    getJoint( const scl_wstring& baseName ) const 
	DNB_THROW_SPEC_NULL;

    void
    getJoints( DNBBasicJoint3D::Vector &gJoints ) const
        DNB_THROW_SPEC((scl_bad_alloc));

    void
    getJoints( DNBBasicJoint3D::List &gJoints ) const
        DNB_THROW_SPEC((scl_bad_alloc));


    size_t
    getJointCount( ) const
        DNB_THROW_SPEC_NULL;

    void
    destroyJoint( DNBBasicConnection3D::Handle hJoint )
        DNB_THROW_SPEC((DNBENotFound));

    void
    removeJoint( DNBBasicConnection3D::Handle hJoint )
    DNB_THROW_SPEC((DNBENotFound));

    void
    replaceJoint( DNBBasicConnection3D::Handle hJoint1,
                  DNBBasicConnection3D::Handle hJoint2 )
    	DNB_THROW_SPEC((scl_bad_alloc, DNBEAlreadyRegistered, DNBEInvalidRelation, DNBENotFound));

    DNBBasicBody3D::Handle
    getInnerBody( DNBBasicBody3D::Handle hBody ) const
        DNB_THROW_SPEC_NULL;

    void
    getMovableBodies( DNBBasicBody3D::Vector& gVector ) const
	DNB_THROW_SPEC((scl_bad_alloc));

    void
    getMovableBodies( DNBBasicBody3D::List& gList ) const
	DNB_THROW_SPEC((scl_bad_alloc));

    bool
    hasMovableBody( DNBBasicBody3D::Handle hBody ) const
        DNB_THROW_SPEC_NULL;

    DNBXform3D
    getMovableBodyBaseLocation( DNBBasicBody3D::Handle hBody ) const
	DNB_THROW_SPEC((DNBENotFound));

    inline void
    setDOFDisplacementsDefault( const DNBMathVector& DOFValues )
        DNB_THROW_SPEC_NULL;


    inline void
    getDOFDisplacementsDefault( DNBMathVector& DOFValues ) const
        DNB_THROW_SPEC_NULL;


    RequestStatus 
    applyDOFDisplacementsDefault( ) 
        DNB_THROW_SPEC_NULL;

    //
    // DOF interface. 
    //

    inline void
    addDOFDisplacementLimits( const DNBReal& lower, const DNBReal& upper, 
			  size_t index )
	DNB_THROW_SPEC_NULL; 

    void
    addDOFDisplacementLimits( const scl_wstring &lower, const scl_wstring &upper,
                              size_t idx )
	DNB_THROW_SPEC(( scl_bad_alloc, DNBEInvalidFormat, DNBEDoesNotExist ));
  
    inline DNBReal
    getDOFDisplacementLimitLower( size_t index ) const
	DNB_THROW_SPEC(( scl_bad_alloc, DNBEDoesNotExist, DNBEZeroDivide,
	                 DNBEOverflowError, DNBEUnderflowError ));

    inline DNBReal
    getDOFDisplacementLimitUpper( size_t index ) const
	DNB_THROW_SPEC(( scl_bad_alloc, DNBEDoesNotExist, DNBEZeroDivide,
	                 DNBEOverflowError, DNBEUnderflowError ));

    inline bool
    isDOFDisplacementWithinLimits( const DNBReal& displacement,
                                   size_t index ) const
	DNB_THROW_SPEC(( scl_bad_alloc, DNBEDoesNotExist, DNBEZeroDivide,
	                 DNBEOverflowError, DNBEUnderflowError ));

    inline void
    addDOFVelocityLimits( const DNBReal& lower, const DNBReal& upper, 
			  size_t index )
	DNB_THROW_SPEC_NULL; 
  
    inline void
    addDOFVelocityLimits( const scl_wstring &lower, const scl_wstring &upper, 
			  size_t index )
	DNB_THROW_SPEC_NULL; 
  
    inline DNBReal
    getDOFVelocityLimitLower( size_t index ) const
	DNB_THROW_SPEC_NULL; 

    inline DNBReal
    getDOFVelocityLimitUpper( size_t index ) const
	DNB_THROW_SPEC_NULL; 

    inline bool
    isDOFVelocityWithinLimits( const DNBReal& velocity, size_t index ) const
	DNB_THROW_SPEC_NULL; 

    inline void
    addDOFAccelerationLimits( const DNBReal& lower, const DNBReal& upper, 
			  size_t index )
	DNB_THROW_SPEC_NULL; 
  
    inline DNBReal
    getDOFAccelerationLimitLower( size_t index ) const
	DNB_THROW_SPEC_NULL; 

    inline DNBReal
    getDOFAccelerationLimitUpper( size_t index ) const
	DNB_THROW_SPEC_NULL; 

    inline bool
    isDOFAccelerationWithinLimits( const DNBReal& acceleration, size_t index ) const
	DNB_THROW_SPEC_NULL; 

    // gsx
    inline void
    addDOFDisplacementCautionZone(const DNBReal& marginValue, 
	    bool isAbsoluteValue, size_t index)
	DNB_THROW_SPEC_NULL; 

	// gsx
	inline bool
    isDOFDisplacementWithinCautionZone( const DNBReal& displacement,
                                   size_t index ) const
	DNB_THROW_SPEC(( scl_bad_alloc, DNBEDoesNotExist, DNBEZeroDivide,
	                 DNBEOverflowError, DNBEUnderflowError ));

	// gsx
	inline DNBReal
    getDOFDisplacementCautionZoneLower( size_t index ) const
	DNB_THROW_SPEC(( scl_bad_alloc, DNBEDoesNotExist, DNBEZeroDivide,
	                 DNBEOverflowError, DNBEUnderflowError ));
	// gsx
    inline DNBReal
    getDOFDisplacementCautionZoneUpper( size_t index ) const
	DNB_THROW_SPEC(( scl_bad_alloc, DNBEDoesNotExist, DNBEZeroDivide,
	                 DNBEOverflowError, DNBEUnderflowError ));


    //
    // Home position related interface
    //

 
    void
    addHomePosition( DNBBasicDOFTarget::Handle hHome ) 
        DNB_THROW_SPEC((scl_bad_alloc, DNBEAlreadyRegistered));

    DNBBasicDOFTarget::Handle 
    getHomePosition( const scl_wstring& name ) const
	DNB_THROW_SPEC_NULL;

    DNBBasicDOFTarget::Handle 
    isHomePosition() const
	DNB_THROW_SPEC_NULL;

    void
    getHomePositions( DNBBasicDOFTarget::Vector& gHome ) const
	DNB_THROW_SPEC_NULL;

    void
    getHomePositions( DNBBasicDOFTarget::List& gHome ) const
	DNB_THROW_SPEC_NULL;

    void
    getHomePositions( DNBBasicDOFTargetSet::Handle hHomeSet ) const
	DNB_THROW_SPEC_NULL;

    bool 
    removeHomePosition(  const scl_wstring& name )
	DNB_THROW_SPEC_NULL;

    bool 
    removeHomePosition( DNBBasicDOFTarget::Handle hHome ) 
	DNB_THROW_SPEC_NULL;
	
    void
    removeHomePositions() 
	DNB_THROW_SPEC_NULL;

    bool 
    destroyHomePosition( const scl_wstring& name )
	DNB_THROW_SPEC_NULL;

    bool 
    destroyHomePosition( DNBBasicDOFTarget::Handle hHome )
	DNB_THROW_SPEC_NULL;

    void
    destroyHomePositions( ) 
	DNB_THROW_SPEC_NULL;

    size_t
    getHomePositionCount( ) const
	DNB_THROW_SPEC_NULL;

    
    struct moveSchedule
    {
	DNBBasicDOFTarget::Handle   hTarget1_;
	DNBBasicDOFTarget::Handle   hTarget2_;
	DNBReal			    time_;
    };  

    typedef scl_vector< moveSchedule, DNB_ALLOCATOR( moveSchedule ) >  moveScheduleVector; 

    //
    // Note this overwrites the 'time' value of a move schedule
    // if it already exits.
    //
    
    bool 
    addMoveSchedule( DNBBasicDOFTarget::Handle hTarget1, 
		     DNBBasicDOFTarget::Handle hTarget2, 
		     DNBReal time )   
        DNB_THROW_SPEC((scl_bad_alloc));


    bool 
    getMoveSchedule( const scl_wstring& target1, 
		     const scl_wstring& target2, 
		     DNBReal& time ) const   
        DNB_THROW_SPEC_NULL;

    bool 
    getMoveSchedule( DNBBasicDOFTarget::Handle hTarget1, 
		     DNBBasicDOFTarget::Handle hTarget2, 
		     DNBReal& time )  const 
        DNB_THROW_SPEC_NULL;


    void
    getMoveSchedule( moveScheduleVector& vMove ) const 
        DNB_THROW_SPEC((scl_bad_alloc));
   

    bool 
    removeMoveSchedule( const scl_wstring& target1, 
		        const scl_wstring& target2 )   
        DNB_THROW_SPEC_NULL;

    bool 
    removeMoveSchedule( DNBBasicDOFTarget::Handle hTarget1, 
		        DNBBasicDOFTarget::Handle htarget2 )   
        DNB_THROW_SPEC_NULL;


    inline DNBWDMSymbolTable
    getSymbolTable( ) const
        DNB_THROW_SPEC_NULL;



#if 0
 
    void
    dump( ostream& oStr ) const
        DNB_THROW_SPEC_NULL;

#endif

    enum Events
    {
        EventFirst = DNBBasicBody3D::EventLast - 1,
        EventDOFCountModified,
        EventDOFTypeModified,
        EventDOFNameModified,
        EventDOFDisplacementModified,   // Instantaneous change in displacement
        EventDOFProfileModified,        // Complete profile modified
        EventLast
    };

    void setTravelLimitStatus(int,unsigned int red=-1,
        unsigned int green=-1,unsigned int blue =-1)
            DNB_THROW_SPEC_NULL;
    // gsx
    void setCautionZoneStatus(int,unsigned int red=-1,
	unsigned int green=-1,unsigned int blue =-1)
	    DNB_THROW_SPEC_NULL;
    void setVelocityLimitStatus(int,unsigned int red =-1,
        unsigned int green=-1,unsigned int blue=-1)
            DNB_THROW_SPEC_NULL;
    void setAccelerationLimitStatus(int,unsigned int red=-1,
        unsigned int green=-1,unsigned int blue=-1)
            DNB_THROW_SPEC_NULL;
    void resetLimitStatus()
            DNB_THROW_SPEC_NULL;

	RequestStatus
	checkDOFTravelLimit(DNBMathVector &displacements,DNBKinStatus *kinStatus=NULL,
		            bool updateColor = true)
        DNB_THROW_SPEC_NULL;
	
	RequestStatus
	checkDOFVelocityLimit(DNBMathVector &velocities)
        DNB_THROW_SPEC_NULL;

	RequestStatus
	checkDOFAccelerationLimit(DNBMathVector &accels)
        DNB_THROW_SPEC_NULL;
	
	void getTravelLimitStatus( int&, unsigned int&, unsigned int&, unsigned int& ) const
        DNB_THROW_SPEC_NULL;

	// gsx
	RequestStatus
	checkDOFTravelCautionZone(DNBMathVector &displacements,
				    DNBKinStatus *kinStatus=NULL, bool updateColor = true)
        DNB_THROW_SPEC_NULL;


   void setWaitingResourceColor( int iAnlMode, unsigned int red=-1,
        unsigned int green=-1,unsigned int blue =-1)
            DNB_THROW_SPEC_NULL;

   void getWaitingResourceColor( unsigned int& red,
        unsigned int& green,unsigned int& blue )
            DNB_THROW_SPEC_NULL;

   bool
   getDeviceWaitStatus() 
    DNB_THROW_SPEC_NULL;

   void
   setDeviceWaitStatus( bool isDeviceWaiting ) 
    DNB_THROW_SPEC_NULL;

   int
   getIOAnalysisMode()
    DNB_THROW_SPEC_NULL;

   void 
   deviceIOStatusVerboseInfo( DNBMessage& dnb_msg ) const
     DNB_THROW_SPEC_NULL;

    //////////////////////////////////////////////////////////////////////
    //
    // Gets all parts associated with given DOF. If primary is true, 
    // returns only those parts for which given DOF is primary.
    //
    //////////////////////////////////////////////////////////////////////
    void
    getParts( size_t DOFIndex, DNBBasicBody3D::Vector &gParts, bool primary = false ) const
        DNB_THROW_SPEC((scl_bad_alloc));

    typedef scl_vector< int, DNB_ALLOCATOR(int) > AlphaMapDOFs;

    // To set the DOF index of each alphaMapNode ( each part in the active kin
	// chain ). If there is only one chain, the DOF indices are the indices of the 
	// parts in the given mechanism
	void
    setAlphaMapDOFs( const AlphaMapDOFs &alphaMapDOFs )
        DNB_THROW_SPEC_NULL;

    // To get the DOF index of each alphaMapNode ( each part in the active kin
	// chain ).
	void
    getAlphaMapDOFs( AlphaMapDOFs &alphaMapDOFs ) const
        DNB_THROW_SPEC_NULL;

    // To set all the parts in an active Kin Chain indexed by activeKinChainIndex_.
	// If there is only one chain, these parts happen to be the ones which are 
	// associated as outer parts in the command joints
	void
    setAlphaMapNodes( const DNBBasicBody3D::Vector &alphaMapNodes )
        DNB_THROW_SPEC((scl_bad_alloc));

    // To get all the parts in an active Kin Chain indexed by activeKinChainIndex_.
	void
    getAlphaMapNodes( DNBBasicBody3D::Vector &alphaMapNodes ) const
        DNB_THROW_SPEC((scl_bad_alloc));

    RequestStatus
    setBaseLocation( DNBXform3D &baseLocation )
        DNB_THROW_SPEC_NULL;

    RequestStatus
    setBaseLocation( const DNBXform3D &relativeLocation,
        DNBBasicEntity3D::Handle hOther )
        DNB_THROW_SPEC_NULL;

protected:
    //
    //  By defining the following methods in the protected section, application
    //  programmers must use the static factory methods to create and destroy
    //  instances of this class.
    //
    DNBBasicDevice3D( )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit, DNBENetworkError));

    DNBBasicDevice3D( const DNBBasicDevice3D &right, CopyMode mode )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit, DNBENetworkError));

    virtual
    ~DNBBasicDevice3D( )
        DNB_THROW_SPEC_NULL;

    //
    //  The following method is used to compute the location of each constraint
    //  (or joint) on the device, given the specified DOF displacements.
    //  This function is Kin Chain dependent for TCPOverReachLimit_
    virtual RequestStatus
    updateDOFDisplacements( DNBMathVector &displacements )
        DNB_THROW_SPEC_NULL;

    void
    setDOFValues( const DNBMathVector &displacements ) const
        DNB_THROW_SPEC_NULL;

    void
    getDOFValues( DNBMathVector &dofValues ) const
        DNB_THROW_SPEC_NULL;

    virtual void
    propagateBaseLocation( ) const
        DNB_THROW_SPEC_NULL;

    //(unsigned int red,unsigned int green,unsigned int blue, int joint=-1)
    virtual void
    setColor(size_t joint=-1)
        DNB_THROW_SPEC_NULL;

    void
    checkVerbose();


    DNBForwardKinSolver*       pForwardKin_;
    
	
	class ExportedByDNBDeviceModel DeviceKinChain
	{
	public:
		DeviceKinChain();
		DeviceKinChain( const DeviceKinChain &other );
		~DeviceKinChain();
		void operator=(const DeviceKinChain &other);

		bool						TCPOverReachLimit_;
		DNBBasicBody3D::Handle		hBasePart_;
		DNBBasicBody3D::Handle		hMountPart_;
		DNBXform3D					mountOffset_;
		DNBXform3D					mountOffsetInv_;
		AlphaMapDOFs				alphaMapDOFs_;
		DNBBasicBody3D::Vector		alphaMapNodes_;
	};
	
	typedef scl_vector < DeviceKinChain, DNB_ALLOCATOR( DeviceKinChain ) > DeviceKinChainDB;
	DeviceKinChainDB deviceKinChainDB_;
	DeviceKinChain *currDeviceKinChain;

	int			kinChainsCount_; // Set using the function "setKinChainsCount" provided in
								//DNBBasicRobot3D. The default is always one.	

    // Occasionally we need to move the device to evaluate certain conditions.
    // This variable is used to enable/disable the updating of certain state
    // variables and color.
    bool   _bUpdateMode;

    //Check if there is any highlight existing or needRefreshColor
    // false means setColor() will do nothing but call checkVerbose() only.
    bool  isChangingColor();


private:

    //
    // In Device or Joint?
    //
    class DOFLimitGroup
    {
    public:
	DOFLimitGroup( )
            DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit, DNBENetworkError));

	DOFLimitGroup( DNBReal dl, DNBReal du, DNBReal vl, DNBReal vu, 
		       DNBReal al, DNBReal au  ) 
            DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit, DNBENetworkError));

	~DOFLimitGroup( )
            DNB_THROW_SPEC_NULL;
        void
	operator=( const DOFLimitGroup& right )
	    DNB_THROW_SPEC_NULL;
	bool 
	operator==( const DOFLimitGroup& right ) const
	    DNB_THROW_SPEC_NULL;
	bool 
	operator<( const DOFLimitGroup& right ) const
	    DNB_THROW_SPEC_NULL;

	DNBReal		displacementLower_;
	DNBReal		displacementUpper_;
        scl_wstring         disLowerExpr_;
        scl_wstring         disUpperExpr_;
	DNBExpressionSolver<DNBReal>::solverPointer solverLower_;
	DNBExpressionSolver<DNBReal>::solverPointer solverUpper_;
        bool            isDisConst_;

	DNBReal		velocityLower_;
	DNBReal		velocityUpper_;
	DNBReal		accelerationLower_;
	DNBReal		accelerationUpper_;
	bool		isSoftLimitPresent_;
	DNBReal		marginValue_;
	bool		isAbsoluteValue_;
    };




    typedef scl_vector<DOFLimitGroup, DNB_ALLOCATOR( DOFLimitGroup ) >   DOFLimits;

    class homePosition 
    {
    public:
        typedef scl_pair < DNBBasicDOFTarget::Handle, DNBReal >  aPair;

        typedef scl_vector< aPair, DNB_ALLOCATOR( aPair ) > moveScheduleDB;

        typedef moveScheduleDB::iterator                moveScheduleIter;

        typedef moveScheduleDB::const_iterator          moveScheduleConstIter;

	homePosition( )
            DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit, DNBENetworkError));

	homePosition( DNBBasicDOFTarget::Handle hTarget ) 
            DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit, DNBENetworkError));

	homePosition( const homePosition& right ) 
            DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit, DNBENetworkError));

	~homePosition( )
            DNB_THROW_SPEC_NULL;
        void
	operator=( const homePosition& right )
	    DNB_THROW_SPEC_NULL;
	bool 
	operator==( const homePosition& right ) const
	    DNB_THROW_SPEC_NULL;
	bool 
	operator<( const homePosition& right ) const
	    DNB_THROW_SPEC_NULL;

 	moveScheduleIter getMoveSchedule( 
					DNBBasicDOFTarget::Handle hTarget ) 
	    DNB_THROW_SPEC_NULL;
 	moveScheduleConstIter getMoveSchedule( 
					DNBBasicDOFTarget::Handle hTarget ) const
	    DNB_THROW_SPEC_NULL;

	DNBBasicDOFTarget::Handle 	homePosition_;
 	moveScheduleDB		        moveScheduleDB_; 

    };

    typedef scl_vector<homePosition, DNB_ALLOCATOR( homePosition ) >   homePositionDB;

    typedef DOFLimits::iterator		      	      DOFLimitIterator;
    typedef DOFLimits::const_iterator		      DOFLimitConstIterator;

    typedef homePositionDB::iterator		      homePositionIterator;
    typedef homePositionDB::const_iterator	      homePositionConstIterator;

    typedef DNBBasicEntity::Vector::const_iterator    ChildEntityConstIterator;
    

    typedef DeviceSubEntryDB::iterator		 DeviceSubEntryIterator;
    typedef DeviceSubEntryDB::const_iterator	 DeviceSubEntryConstIterator;

    bool 
    hasHomePosition( const scl_wstring& name  )
	DNB_THROW_SPEC_NULL;

    bool 
    hasHomePosition( const scl_wstring& name ) const  
	DNB_THROW_SPEC_NULL;

    bool 
    hasHomePosition( DNBBasicDOFTarget::Handle hTarget )  
	DNB_THROW_SPEC_NULL;

    bool 
    hasHomePosition( DNBBasicDOFTarget::Handle hTarget ) const
	DNB_THROW_SPEC_NULL;

    DNBBasicDevice3D::homePositionIterator
    findHomePosition( const scl_wstring& baseName )
 	DNB_THROW_SPEC_NULL;
    
    DNBBasicDevice3D::homePositionConstIterator
    findHomePosition( const scl_wstring& baseName ) const
 	DNB_THROW_SPEC_NULL;

    DNBBasicDevice3D::homePositionIterator
    findHomePosition( DNBBasicDOFTarget::Handle hTarget )
 	DNB_THROW_SPEC_NULL;

    DNBBasicDevice3D::homePositionConstIterator
    findHomePosition( DNBBasicDOFTarget::Handle hTarget ) const
 	DNB_THROW_SPEC_NULL;

    void
    removeMoveScheduleHomePosition( DNBBasicDOFTarget::Handle hHome )
        DNB_THROW_SPEC_NULL;


    ChildEntityConstIterator
    findChildEntity( const DNBBasicEntity::Vector& gChildren, 
	             const scl_wstring& name ) const
        DNB_THROW_SPEC_NULL;

    void
    onNotification( )
        DNB_THROW_SPEC_NULL;

    void
    registerDOFSymbols( )
        DNB_THROW_SPEC_NULL;

    void
    destroyDOFSymbols( )
        DNB_THROW_SPEC_NULL;

    void
    setDOFValueLC( DNBReal value, size_t index )
        DNB_THROW_SPEC_NULL;

    DNBReal
    getDOFValueLC( size_t index )
        DNB_THROW_SPEC_NULL;

    void
    setDOFValueUC( DNBReal value, size_t index )
        DNB_THROW_SPEC_NULL;

    DNBReal
    getDOFValueUC( size_t index )
        DNB_THROW_SPEC_NULL;

    void
    applyDOFDisplacements( )
        DNB_THROW_SPEC_NULL;

    void
    updateRelativeXform() 
        DNB_THROW_SPEC_NULL;

	bool needRefreshColor();

    size_t          DOFCount_;          // TODO: Store in DOFProfile_.
    DOFTypeDB       DOFTypeDB_;         // TODO: Store in DOFProfile_.
    DOFNameDB       DOFNameDB_;         // TODO: Store in DOFProfile_.
    DNBMathVector   DOFProfile_;        // TODO: Change type to DNBDOFProfile.
    DOFLimits	    DOFLimits_;
    mutable 	DNBMathVector   DOFValue_;       
    mutable 	DNBMathVector   DOFValueTemp_;       
    mutable 	DNBMathVector   DOFDisplacementsDefault_;       
    homePositionDB  		homePositionDB_;
    DNBWDMSymbolTable  		symbTable_;
    DeviceSubEntryDB		DeviceSubEntryDB_;
    DNBBasicBody3D::Handle      hFixedPart_;
    DNBXform3D                  fixedPartRelative_;

    bool                 isDeviceWaiting_;

	//////////////////////feedback settings
	// gsx. Added feedback settings for soft limits
	int DOFTravelLimitStatus_, DOFVelocityLimitStatus_, DOFAccelerationLimitStatus_, DOFSoftLimitStatus_,
	    iniDOFTravelLimitStatus_, iniDOFVelocityLimitStatus_, iniDOFAccelerationLimitStatus_, iniDOFSoftLimitStatus_;
	// gsx. Added feedback setting for soft limits
	unsigned int DOFTravelRed_,DOFTravelGreen_,DOFTravelBlue_,
	        DOFSoftLimRed_,DOFSoftLimGreen_,DOFSoftLimBlue_,
		DOFVelocityRed_,DOFVelocityGreen_,DOFVelocityBlue_,
		DOFAccelerationRed_,DOFAccelerationGreen_,DOFAccelerationBlue_;

   int IOAnalysisStatus_, iniIOAnalysisStatus_;
   unsigned int WAITResourceColorRed_, WAITResourceColorGreen_, WAITResourceColorBlue_;

   // gsx
	DNBBoolVector DOFTravelLimits_,DOFVelocityLimits_,DOFAccelerationLimits_, DOFSoftLimits_, oldDOFTravelLimits_,oldDOFVelocityLimits_,oldDOFAccelerationLimits_, oldDOFSoftLimits_;

    struct AuxDeviceEntry
    {
	DNBBasicEntity3D::Handle auxDevice;
	DNBAuxDeviceType            type;

	AuxDeviceEntry( );
	AuxDeviceEntry( const AuxDeviceEntry &other );

	AuxDeviceEntry &
	operator=( const AuxDeviceEntry &other );

        bool
        operator==( const AuxDeviceEntry &other ) const;

        bool
        operator< ( const AuxDeviceEntry &other ) const;
    };

    typedef scl_list<AuxDeviceEntry, DNB_ALLOCATOR(AuxDeviceEntry) > AuxDeviceDB;
    AuxDeviceDB auxDeviceDB_;

    friend class DNBForwardKinSolverGraph;
};


#include "DNBBasicDevice3D.cc"


#endif  /* _DNB_BASICDEVICE3D_H_ */
