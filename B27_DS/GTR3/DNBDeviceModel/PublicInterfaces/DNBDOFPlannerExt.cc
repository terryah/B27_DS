// COPYRIGHT Dassault Systemes 2005
/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

/**
 * @fullreview rtl xyuan 01:08:16
 * @error UNP N err_1 Check if newDOFPlanner is not NULL
 */
// --------------------------------------------------------
/** @anchor err_1 Check if newDOFPlanner is not NULL */ 

inline void
DNBDOFPlannerExt::addDOFTarget(DNBBasicDOFTarget::Handle hTarget)
    DNB_THROW_SPEC((DNBEDomainError)) 
{


	currentDOFPlanner->addDOFTarget(hTarget);
}

inline void 
DNBDOFPlannerExt::addDOFTargetSet(DNBBasicDOFTargetSet::Handle hTargetSet_)
    DNB_THROW_SPEC((DNBEDomainError)) 
{



	currentDOFPlanner->addDOFTargetSet(hTargetSet_);
}
	
inline void 
DNBDOFPlannerExt::clearSetTargets( )
	DNB_THROW_SPEC_NULL
{


	currentDOFPlanner->clearSetTargets();
}

inline DNBMathVector 
DNBDOFPlannerExt::evaluate( const DNBSimTime &currentTime )
	DNB_THROW_SPEC((DNBEZeroVector, DNBEZeroDivide, DNBEStop, DNBEHalt,
                    DNBEOverflowError/*, RWTHRInvalidPointer*/))
{

	return currentDOFPlanner->evaluate(currentTime);
}

inline DNBMathVector 
DNBDOFPlannerExt::evaluate( )
	DNB_THROW_SPEC((DNBEZeroVector, DNBEZeroDivide, DNBEStop, DNBEHalt,
                    DNBEOverflowError/*, RWTHRInvalidPointer*/))
{

	return currentDOFPlanner->evaluate();
}

inline void 
DNBDOFPlannerExt::reset()
		DNB_THROW_SPEC_NULL
{
	currentDOFPlanner->reset();
}


inline void 
DNBDOFPlannerExt::resetProcess()
		DNB_THROW_SPEC_NULL
{
	currentDOFPlanner->resetProcess();
}

inline DNBSimTime 
DNBDOFPlannerExt::getMoveTime()
			DNB_THROW_SPEC_NULL
{
	return currentDOFPlanner->getMoveTime();
}

inline DNBSimTime 
DNBDOFPlannerExt::getSegmentTime(size_t i)
		DNB_THROW_SPEC_NULL
{
	return currentDOFPlanner->getSegmentTime(i);
}

inline void 
DNBDOFPlannerExt::setSamplePeriod(DNBSimTime step)
	DNB_THROW_SPEC_NULL
{
	currentDOFPlanner->setSamplePeriod(step);
}

inline void 
DNBDOFPlannerExt::addDOFPlanner(DNBDOFPlannerBase* newDOFPlanner)
		DNB_THROW_SPEC_NULL
{
	if(newDOFPlanner)
		DOFPlanners_.push_back(newDOFPlanner);
}

inline void 
DNBDOFPlannerExt::getCurrentPlannerName (scl_wstring& name)
	DNB_THROW_SPEC_NULL
{
	name = currentDOFPlanner->getPlannerName ();
}

inline void 
DNBDOFPlannerExt::setInitDOFPos (	const DNBMathVector& newDOF)
		DNB_THROW_SPEC_NULL
{
	currentDOFPlanner->setInitDOFPos(newDOF);
}
	//in absolute value
inline void 
DNBDOFPlannerExt::setInitDOFSpeed(const DNBMathVector& newSpeed)
		DNB_THROW_SPEC_NULL
{
	currentDOFPlanner->setInitDOFSpeed(newSpeed);
}

inline DNBMathVector 
DNBDOFPlannerExt::getDOFSpeed ()
		DNB_THROW_SPEC_NULL
{
	return currentDOFPlanner->getDOFSpeed();
}

inline DNBMathVector 
DNBDOFPlannerExt::getDOFAccel ()
		DNB_THROW_SPEC_NULL
{
	return currentDOFPlanner->getDOFAccel();
}


inline bool 
DNBDOFPlannerExt::reachedEndOfMove ()
		DNB_THROW_SPEC_NULL
{
	return currentDOFPlanner->reachedEndOfMove();
}


inline void 
DNBDOFPlannerExt::setDefaultDOFRounding( DNBReal rounding )
    DNB_THROW_SPEC((DNBERangeError))
{
    if( rounding < 0.0 || rounding > 1.0 )
    {
	DNBERangeError    error( DNB_FORMAT("Rounding out of range [0,1]"));
	throw error;
    }
    DefaultDOFRounding_ = rounding;

}


inline DNBReal 
DNBDOFPlannerExt::getDefaultDOFRounding( ) const
    DNB_THROW_SPEC_NULL
{
    return DefaultDOFRounding_;

}



inline void 
DNBDOFPlannerExt::setCurrentDOFRounding( DNBReal rounding )
    DNB_THROW_SPEC((DNBERangeError))
{
    if( rounding < 0.0 || rounding > 1.0 )
    {
		DNBERangeError    error( DNB_FORMAT("Rounding out of range [0,1]"));
		throw error;
    }
    CurrentDOFRounding_ = rounding;

}


inline DNBReal 
DNBDOFPlannerExt::getCurrentDOFRounding( ) const
    DNB_THROW_SPEC_NULL
{
    return CurrentDOFRounding_;

}


inline void 
DNBDOFPlannerExt::resetCurrentDOFRounding( )
    DNB_THROW_SPEC_NULL
{
    CurrentDOFRounding_ = DefaultDOFRounding_;

}

inline void
DNBDOFPlannerExt::setDOFPlannerPeriod( DNBSimTime period )
	DNB_THROW_SPEC((DNBERangeError))
{
	currentDOFPlanner->setDOFPlannerPeriod(period);
}

inline DNBSimTime 
DNBDOFPlannerExt::getDOFPlannerPeriod( ) const
	DNB_THROW_SPEC_NULL
{
	return currentDOFPlanner->getDOFPlannerPeriod();
}


inline void
DNBDOFPlannerExt::setDOFMinMotionTime( DNBSimTime time )
	DNB_THROW_SPEC((DNBERangeError))
{
    if( time < 0.0 )
    {

        DNBERangeError    error( DNB_FORMAT("Negative DOF minimum motion planner period"));
        throw error;

    }
    DOFMinMotionTime_ = time;
}

inline DNBSimTime 
DNBDOFPlannerExt::getDOFMinMotionTime( ) const
	DNB_THROW_SPEC_NULL
{
    return DOFMinMotionTime_;
}


inline void
DNBDOFPlannerExt::setDefaultDOFSpeed( DNBReal speed )
    DNB_THROW_SPEC_NULL
{    
	currentDOFPlanner->setDefaultDOFSpeed(  speed );
}


inline DNBReal
DNBDOFPlannerExt::getDefaultDOFSpeed( ) const
    DNB_THROW_SPEC_NULL
{
    return currentDOFPlanner->getDefaultDOFSpeed(  );

}

inline DNBReal
DNBDOFPlannerExt::PrepareMove(DNBBasicDOFTarget::Handle hTarget)
	DNB_THROW_SPEC_NULL
{
	return currentDOFPlanner->PrepareMove (hTarget);
}
inline void
DNBDOFPlannerExt::ScaleMove (const DNBSimTime Time)
DNB_THROW_SPEC_NULL
{
	currentDOFPlanner->ScaleMove (Time);
}

inline DNBMathVector
DNBDOFPlannerExt::ExecuteMove (const DNBSimTime Time)
DNB_THROW_SPEC_NULL
{
	return currentDOFPlanner->ExecuteMove (Time);
}

inline void
DNBDOFPlannerExt::UpdatePosition (const PositionType Type, const DNBMathVector &Position)
    DNB_THROW_SPEC_NULL
{
    currentDOFPlanner->UpdatePosition (Type, Position);
}

inline void
DNBDOFPlannerExt::getMoveMaxSpeed (const DNBInteger32   Index,
                                         DNBMathVector  &Speed)
    DNB_THROW_SPEC_NULL
{
    currentDOFPlanner->getMoveMaxSpeed (Index, Speed);
}

inline void
DNBDOFPlannerExt::MoveMaxSpeedCalculation (const bool Enabled)
    DNB_THROW_SPEC_NULL
{
    currentDOFPlanner->MoveMaxSpeedCalculation (Enabled);
}



#if 0
inline void
DNBDOFPlannerExt::setCurrentDOFSpeed( DNBReal speed )
    DNB_THROW_SPEC_NULL
{
    CurrentDOFSpeed_ = speed;

}


inline DNBReal
DNBDOFPlannerExt::getCurrentDOFSpeed( ) const
    DNB_THROW_SPEC_NULL
{
    return CurrentDOFSpeed_;

}


inline void
DNBDOFPlannerExt::resetCurrentDOFSpeed( )
    DNB_THROW_SPEC_NULL
{
    CurrentDOFSpeed_ = currentDOFPlanner->getDefaultDOFSpeed( );

}

#endif
