// COPYRIGHT Dassault Systemes 2005
//*
//* FILE:
//*     DNBDOFPlannerBase.h      - public header file
//*
//* MODULE:
//*     DNBDOFPlannerBase  - single non-template class
//*
//* OVERVIEW:
//*     This module defines an extension class which is base for all
//*     motion planner classes.
//*	 
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*		xin			11/29/00
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1998 - 2000 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_DOFPLANNERBASE_H_
#define _DNB_DOFPLANNERBASE_H_

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

#include <DNBSystemBase.h>
#include <scl_vector.h>
#include <DNBSystemDefs.h>


#include <DNBMotionPlannerBase.h>
#include <DNBBasicDOFTargetSet.h>
#include <DNBConveyorTrackingEnums.h>

class ExportedByDNBDeviceModel DNBDOFPlannerBase : virtual public DNBMotionPlannerBase
{


    DNB_DECLARE_DYNAMIC_RTTI( DNBDOFPlannerBase );

public:

	virtual void 
	addDOFTargetSet(DNBBasicDOFTargetSet::Handle hTargetSet)
    DNB_THROW_SPEC((DNBEDomainError)); 

    void clearSetTargets( )
	DNB_THROW_SPEC_NULL;



    virtual
    ~DNBDOFPlannerBase( )
	DNB_THROW_SPEC_NULL;



	virtual void setInitDOFPos(const DNBMathVector&)
			DNB_THROW_SPEC_NULL=0;

	//in absolute value
	virtual void setInitDOFSpeed(const DNBMathVector& vel)
			DNB_THROW_SPEC_NULL=0;

	virtual DNBMathVector getDOFSpeed()
			DNB_THROW_SPEC_NULL=0;

	virtual DNBMathVector getDOFAccel()
			DNB_THROW_SPEC_NULL=0;

	virtual bool reachedEndOfMove()
			DNB_THROW_SPEC_NULL=0;

    inline void
    setDOFPlannerPeriod( DNBSimTime period )
	DNB_THROW_SPEC((DNBERangeError));

    inline DNBSimTime 
    getDOFPlannerPeriod( ) const
	DNB_THROW_SPEC_NULL;

    inline void
    setDefaultDOFSpeed( DNBReal speed ) 
	DNB_THROW_SPEC_NULL;

    inline DNBReal 
    getDefaultDOFSpeed( ) const
	DNB_THROW_SPEC_NULL;

	inline DNBReal
	PrepareMove (DNBBasicDOFTarget::Handle hTarget)
	DNB_THROW_SPEC_NULL;

	inline virtual void
	ScaleMove (const DNBSimTime)
	DNB_THROW_SPEC_NULL =0;

	inline virtual DNBMathVector
	ExecuteMove (const DNBSimTime)
	DNB_THROW_SPEC_NULL = 0;

    virtual void
    UpdatePosition (const PositionType Type, const DNBMathVector &Position)
    DNB_THROW_SPEC_NULL = 0;

    virtual void
    getMoveMaxSpeed (const  DNBInteger32 Index,
                            DNBMathVector &Speed)
    DNB_THROW_SPEC_NULL = 0;

    virtual void
    MoveMaxSpeedCalculation (const bool Enable = false)
    DNB_THROW_SPEC_NULL = 0;

protected:
    DNBDOFPlannerBase( )
	DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit));

    DNBDOFPlannerBase( const scl_wstring&,DNBBasicDevice3D::Handle)
	DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit));

    DNBDOFPlannerBase( const DNBDOFPlannerBase &other,
			    CopyMode mode )
	DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit));

	void postEvent(size_t);

	inline DNBBasicDOFTargetSet::Handle
		getDOFTargetSet()
	DNB_THROW_SPEC_NULL;

    //
    // workaround on aix 64 bit platform
    // implementation of addDOFTarget() in this class could not get called
    // in aix 64 bit mode
    //
    void 
    addDOFTarget_(DNBBasicDOFTarget::Handle hTarget)
        DNB_THROW_SPEC((DNBEDomainError));


	DNBBasicDevice3D::Handle hDevice;

	DNBBasicDOFTargetSet::Handle hTargetSet;
	size_t KinDOFCount_;
private:
    DNBSimTime 	    DOFPlannerPeriod_;
    DNBReal	    DefaultDOFSpeed_;

};

#include "DNBDOFPlannerBase.cc"


#endif  /* _DNB_DOFPLANNERBASE_H_ */
