//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//
//  Notes:
//    + Implement a facility to convert non-const pointers to const pointers.
//      Consider deriving the class DNBSharedPointer from DNBSharedConstPointer.
//
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_SHAREDCONSTPOINTER_H_
#define _DNB_SHAREDCONSTPOINTER_H_


#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>


#include <DNBObjectModel.h>


#include <DNBSharedBase.h>
#include <DNBSharedHandle.h>




//
//  This class represents a const pointer to a shared object that is derived
//  from DNBSharedObject.  Each time an instance of this class is bound to a
//  shared object, it increments the object's reference count and acquires a
//  read lock on the object.  Further, each time it detaches from the shared
//  object, it releases the read lock on the object and decrements the object's
//  reference count.  When the reference count reaches zero, the object is
//  automatically deleted from the system.  The locking mechanism provides
//  synchronized access to shared objects from multiple threads, even in the
//  presence of C++ exceptions.  By convention, every shared pointer must be
//  allocated on the program stack (automatic storage).
//
template <class Object>
class DNBSharedConstPointer : public DNBSharedBase
{
public:
    typedef DNBSharedHandle<Object>     Handle;

    inline explicit
    DNBSharedConstPointer( const Object *object )
        DNB_THROW_SPEC_NULL;

    //
    //  This method creates a shared pointer to <handle>'s object, and then
    //  acquires a read lock on the object.  The resulting pointer can be used
    //  to access the object within a block statement (or context).
    //
    inline explicit
    DNBSharedConstPointer( const Handle &handle )
        DNB_THROW_SPEC_NULL;

    inline
    ~DNBSharedConstPointer( )
        DNB_THROW_SPEC_NULL;

    inline const Object &
    operator*( ) const
        DNB_THROW_SPEC_NULL;

    inline const Object *
    operator->( ) const
        DNB_THROW_SPEC_NULL;

    inline void
    release( )
        DNB_THROW_SPEC_NULL;

    inline void
    clear( )
        DNB_THROW_SPEC_NULL;

    //
    //  This method returns the raw pointer to the referent.  It should not be
    //  used in client code.
    //
    inline const Object *
    _getObject( ) const
        DNB_THROW_SPEC_NULL;

protected:
    inline const Object *
    getObject( ) const
        DNB_THROW_SPEC_NULL;

private:
    //
    //  Prohibit the following operations on shared pointers.
    //
    DNBSharedConstPointer( const DNBSharedConstPointer<Object> &right )
        DNB_THROW_SPEC_NULL;

    DNBSharedConstPointer<Object> &
    operator=( const DNBSharedConstPointer<Object> &right )
        DNB_THROW_SPEC_NULL;

    DNBSharedConstPointer<Object> *
    operator&( ) const
        DNB_THROW_SPEC_NULL;

    #if ! defined(_IRIX_SOURCE) 
    void *
    operator new( size_t size )
        DNB_THROW_SPEC_NULL;

    void
    operator delete( void *ptr )
        DNB_THROW_SPEC_NULL;
    #endif
};




//
//  Include the public definition file.
//
#include "DNBSharedConstPointer.cc"




#endif  /* _DNB_SHAREDCONSTPOINTER_H_ */
