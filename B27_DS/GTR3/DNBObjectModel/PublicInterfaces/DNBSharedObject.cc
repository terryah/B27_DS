//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview RTL trangara 01:11:15
 */
// --------------------------------------------------------

inline  void
DNBSharedObject::acquireReadLock( )
    DNB_THROW_SPEC_NULL
{
    txnLock_->acquireRead( );
}


inline  void
DNBSharedObject::acquireWriteLock( )
    DNB_THROW_SPEC_NULL
{
    txnLock_->acquireWrite( );
}


inline  void
DNBSharedObject::releaseLock( )
    DNB_THROW_SPEC_NULL
{
    txnLock_->release( );
}


inline  bool
DNBSharedObject::isLocked( ) const
    DNB_THROW_SPEC_NULL
{
    return txnLock_->isLocked( );
}


inline  bool
DNBSharedObject::isReadLocked( ) const
    DNB_THROW_SPEC_NULL
{
    return txnLock_->isReadLocked( );
}


inline  bool
DNBSharedObject::isWriteLocked( ) const
    DNB_THROW_SPEC_NULL
{
    return txnLock_->isWriteLocked( );
}


inline  DNBInteger32
DNBSharedObject::getNestingLevel( ) const
    DNB_THROW_SPEC_NULL
{
    return txnLock_->getNestingLevel( );
}

