//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//
//  Notes:
//    + None
//
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_SHAREDVECTOR_H_
#define _DNB_SHAREDVECTOR_H_


#include <DNBSystemBase.h>
#include <scl_vector.h>
#include <DNBSystemDefs.h>


#include <DNBObjectModel.h>


#include <DNBSharedHandle.h>


//
//  Provide the necessary forward declarations.
//
template <class Object>
class DNBSharedList;

template <class Object>
class DNBSharedSet;




//
//  This class represents a container for a collection of shared objects that
//  are derived from DNBSharedObject.  It is typically used to specify a set of
//  entities in the simulation model (e.g., all parts in the world).  Each time
//  a bound handle is added to a shared vector, the reference count of the
//  corresponding object is incremented.  Further, each time a bound handle is
//  removed from a shared vector, the reference count of the corresponding
//  object is decremented.  When the reference count reaches zero, the shared
//  object is automatically deleted from the system.  The reference-counting
//  facility helps to eliminate potential memory resource errors and leaks,
//  especially in the presence of exceptions and multiple threads.  In general,
//  shared vectors can be stored in global memory (static storage), in
//  dynamically allocated memory (dynamic storage), or on the program stack
//  (automatic storage).
//
//  By convention, application programmers refer to the various entities in the
//  simulation model using shared handles--as opposed to raw pointers.  These
//  handles must be converted to shared pointers before calling any method on
//  the entity.  This procedure is explained in the header file for the class
//  DNBSharedPointer.
//
template <class Object>
class DNBSharedVector : public scl_vector< DNBSharedHandle<Object>,
    DNB_ALLOCATOR(DNBSharedHandle<Object>) >
{
public:
    DNBSharedVector( )
        DNB_THROW_SPEC_NULL;

    DNBSharedVector( const DNBSharedVector<Object> &right )
        DNB_THROW_SPEC((scl_bad_alloc));

    ~DNBSharedVector( )
        DNB_THROW_SPEC_NULL;

    DNBSharedVector<Object> &
    operator=( const DNBSharedVector<Object> &right )
        DNB_THROW_SPEC_NULL;

    DNBSharedVector<Object> &
    operator=( const DNBSharedList<Object> &right )
        DNB_THROW_SPEC_NULL;

    DNBSharedVector<Object> &
    operator=( const DNBSharedSet<Object> &right )
        DNB_THROW_SPEC_NULL;

protected:
    typedef DNBSharedHandle<Object>                 Handle;

    typedef scl_vector< Handle, DNB_ALLOCATOR(Handle) > Base;
};




template <class Target, class Source>
void
DNBSharedAssign( DNBSharedVector<Target> &target,
    const DNBSharedVector<Source> &source )
    DNB_THROW_SPEC((scl_bad_alloc));




//
//  Include the public definition file.
//
#include "DNBSharedVector.cc"




#endif  /* _DNB_SHAREDVECTOR_H_ */
