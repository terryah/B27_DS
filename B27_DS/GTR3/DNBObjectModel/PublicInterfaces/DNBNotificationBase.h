//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 */

//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     joy         05/05/99    Initial implementation.
//*     bkh         11/05/03    Implementation of new documentation style.
//* 
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif



#ifndef _DNB_NOTIFICATIONBASE_H_
#define _DNB_NOTIFICATIONBASE_H_

#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>


#include <DNBObjectModel.h>

#include <DNBException.h>
#include <DNBDynamicObject.h>

/**
  * The abstract base class for notification elements of arbitrary type.
  * 
  * <br><B>Description</B><br>
  * This class defines the base class for notification elements of 
  * arbitrary type. 
  * 
  * 
  * 
  * 
  */
class ExportedByDNBObjectModel DNBNotificationBase : public DNBDynamicObject 
{

    DNB_DECLARE_DYNAMIC_CREATE( DNBNotificationBase );

public:

    virtual DNBNotificationBase*
    clone() const 
        DNB_THROW_SPEC((scl_bad_alloc, DNBException));

protected:
 
/**
  * Constructor.
  * 
  * @return
  * Nothing
  * 
  * <br><B>Description</B><br>
  * This function constructs a DNBNotificationBase. 
  * 
  * 
  */
    DNBNotificationBase( )
        DNB_THROW_SPEC_NULL;

/**
  * Copy constructor.
  * @param right
  * Object to be copied. 
  * 
  * @return
  * Nothing
  * 
  * <br><B>Description</B><br>
  * This function constructs an object of type DNBNotificationBase 
  * from <tt>right</tt>. 
  * 
  * 
  */
    DNBNotificationBase( const DNBNotificationBase& right,
			 CopyMode		    mode )
        DNB_THROW_SPEC_NULL;

public:

/**
  * Destructor.
  * 
  * @return
  * Nothing
  * 
  * <br><B>Description</B><br>
  * This function destroys <tt>self</tt>. 
  * 
  * 
  */
    virtual
    ~DNBNotificationBase( )
            DNB_THROW_SPEC_NULL;

};


#endif		/* _DNB_NOTIFICATIONBASE_H_ */
