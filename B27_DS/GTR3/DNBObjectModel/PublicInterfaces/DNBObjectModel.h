//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/** @CAA2Required */
/*---------------------------------------------------------------------*/
/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */
/*---------------------------------------------------------------------*/
//
//    This Header file is included by all the Module.h header files in 
//    PublicInterfaces
//

#ifndef DNBOBJECTMODEL_H

#define DNBOBJECTMODEL_H DNBObjectModel

#ifdef _WINDOWS_SOURCE
#if defined(__DNBObjectModel)
#define ExportedByDNBObjectModel __declspec(dllexport)
#else
#define ExportedByDNBObjectModel __declspec(dllimport)
#endif
#else
#define ExportedByDNBObjectModel
#endif

#endif /* DNBOBJECTMODEL_H */
