//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//
//  Notes:
//    + An extendible object is both an event publisher and an event subscriber.
//
//    + For a particular entity, the extendible object and extension objects
//      share the same transaction lock.
//
//  TODO:
//    + Add the following notifications:
//        o NotifExtensionAdded         (specify extension handle)
//        o NotifExtensionRemoved       (specify extension handle)
//        o NotifInterfaceAdded         (specify interface pointer)
//        o NotifInterfaceRemoved       (specify interface pointer)
//
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_EXTENDIBLEOBJECT_H_
#define _DNB_EXTENDIBLEOBJECT_H_


#include <DNBSystemBase.h>
#include <scl_map.h>
#include <DNBSystemDefs.h>


#include <DNBObjectModel.h>


#include <DNBNotificationAgent.h>
#include <DNBExtensionObject.h>
#include <DNBCOMObject.h>


#define DNB_DECLARE_EXTENDIBLE_OBJECT(_CLASS)                               \
public:                                                                     \
    static  _CLASS::Handle                                                  \
    clone( _CLASS::Handle hSource, CopyMode mode )                          \
        DNB_THROW_SPEC((scl_bad_alloc, DNBException))                           \
    {                                                                       \
        return CloneObject( hSource, mode );                                \
    }                                                                       \
    static  void                                                            \
    destroy( _CLASS::Handle &hTarget )                                      \
        DNB_THROW_SPEC_NULL                                                 \
    {                                                                       \
        DestroyObject( hTarget );                                           \
        hTarget.clear( );                                                   \
    }                                                                       \
protected:                                                                  \
    static  _CLASS::Handle                                                  \
    CloneObject( _CLASS::Handle hSource, CopyMode mode )                    \
        DNB_THROW_SPEC((scl_bad_alloc, DNBException))                           \
    {                                                                       \
        _CLASS::ConstPointer    pSource( hSource );                         \
        _CLASS                 *rTarget = DNB_STATIC_CAST( _CLASS *,        \
            pSource->duplicateObject( mode ) );                             \
        _CLASS::Handle          hTarget( rTarget );                         \
        return hTarget;                                                     \
    }                                                                       \
    static  void                                                            \
    DestroyObject( _CLASS::Handle hTarget )                                 \
        DNB_THROW_SPEC_NULL                                                 \
    {                                                                       \
        _CLASS::Pointer         pTarget( hTarget );                         \
        pTarget->orphanObject( );                                           \
    }


#define DNB_DEFINE_EXTENDIBLE_OBJECT(_CLASS)    /* nothing */


#define DNB_DECLARE_EXTENDIBLE_FACTORY(_CLASS)                              \
public:                                                                     \
    static  _CLASS::Handle                                                  \
    create( const scl_wstring &name = L"" )                                     \
        DNB_THROW_SPEC((scl_bad_alloc, DNBException))                           \
    {                                                                       \
        return CreateObject( name );                                        \
    }                                                                       \
protected:                                                                  \
    static  _CLASS::Handle                                                  \
    CreateObject( const scl_wstring &name )                                     \
        DNB_THROW_SPEC((scl_bad_alloc, DNBException))                           \
    {                                                                       \
        _CLASS     *rTarget = DNB_NEW _CLASS( );                            \
        rTarget->setEntityName( name );                                     \
        return rTarget;                                                     \
    }


#define DNB_DEFINE_EXTENDIBLE_FACTORY(_CLASS)   /* nothing */




//
//  This class represents an object that can support multiple class extensions
//  and COM interfaces.
//
class ExportedByDNBObjectModel DNBExtendibleObject : public DNBNotificationAgent
{
    DNB_DECLARE_DYNAMIC_CREATE( DNBExtendibleObject );

    DNB_DECLARE_SHARED_OBJECT( DNBExtendibleObject );

    DNB_DECLARE_EXTENDIBLE_OBJECT( DNBExtendibleObject );

public:
    //
    //  TODO: Move this enumeration to DNBAssociationObject.
    //
    enum SetOrdering
    {
        NaturalOrder,
        DescendingOrder,
        AscendingOrder
    };

    //
    //  Extension facility.
    //
    bool
    hasExtension( const DNBClassID &classID ) const
        DNB_THROW_SPEC_NULL;

    DNBExtensionObject::Handle
    getExtension( const DNBClassID &classID ) const
        DNB_THROW_SPEC_NULL;

    void
    getExtensions( DNBExtensionObject::Vector &gExtension ) const
        DNB_THROW_SPEC((scl_bad_alloc));

    void
    getExtensions( DNBExtensionObject::List   &gExtension ) const
        DNB_THROW_SPEC((scl_bad_alloc));

    void
    removeExtension( const DNBClassID &classID )
        DNB_THROW_SPEC_NULL;

    void
    removeExtensions( )
        DNB_THROW_SPEC_NULL;

    //
    //  Interface facility.
    //
    typedef DNBCOMPointer<IUnknown>     IUnknownPointer;

    bool
    hasInterface( const DNBClassID &classID ) const
        DNB_THROW_SPEC_NULL;

    IUnknownPointer
    getInterface( const DNBClassID &classID ) const
        DNB_THROW_SPEC_NULL;

    IUnknown *
    queryInterface( const DNBClassID &classID ) const
        DNB_THROW_SPEC_NULL;

    void
    removeInterface( const DNBClassID &classID )
        DNB_THROW_SPEC_NULL;

    void
    removeInterfaces( )
        DNB_THROW_SPEC_NULL;

    enum Events
    {
        EventFirst = DNBNotificationAgent::EventLast - 1,
        EventLast
    };

protected:
    //
    //  By defining the following methods in the protected section, this class
    //  can only be used as a base class.
    //
    DNBExtendibleObject( )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit));

    DNBExtendibleObject( const DNBExtendibleObject &right, CopyMode mode )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit));

    virtual
    ~DNBExtendibleObject( )
        DNB_THROW_SPEC_NULL;

    virtual DNBDynamicObject *
    duplicateObject( CopyMode mode ) const
        DNB_THROW_SPEC((scl_bad_alloc, DNBException));

private:
    //
    //  Extension facility.
    //
#if DNB_HAS_RW_STANDARD_LIB
    typedef scl_map <DNBClassID, DNBExtensionObject::Handle, scl_less<DNBClassID> > ExtensionDB;
#else
    typedef scl_map <DNBClassID, DNBExtensionObject::Handle, scl_less<DNBClassID> > ExtensionDB;
#endif

    typedef ExtensionDB::iterator       ExtensionDBIterator;

    typedef ExtensionDB::const_iterator ExtensionDBConstIterator;

    typedef ExtensionDB::value_type     ExtensionDBValueType;

    typedef DNBExtensionObject::List    ExtensionSQ;

    typedef ExtensionSQ::iterator       ExtensionSQIterator;

    typedef ExtensionSQ::const_iterator ExtensionSQConstIterator;

    void
    addExtension( const DNBClassID &classID,
        DNBExtensionObject::Handle hExtension )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEAlreadyRegistered));

    void
    removeExtension( const DNBClassID &classID,
        DNBExtensionObject::Handle hExtension )
        DNB_THROW_SPEC_NULL;

    void
    cloneExtensions( const DNBExtendibleObject &right, CopyMode mode )
        DNB_THROW_SPEC((scl_bad_alloc, DNBException));

    //
    //  Interface facility.
    //
#if DNB_HAS_RW_STANDARD_LIB
    typedef scl_map <DNBClassID, DNBCOMObject::Pointer, scl_less<DNBClassID> >      InterfaceDB;
#else
    typedef scl_map <DNBClassID, DNBCOMObject::Pointer, scl_less<DNBClassID> > InterfaceDB;
#endif

    typedef InterfaceDB::iterator       InterfaceDBIterator;

    typedef InterfaceDB::const_iterator InterfaceDBConstIterator;

    typedef InterfaceDB::value_type     InterfaceDBValueType;

    typedef DNBCOMObject::List          InterfaceSQ;

    typedef InterfaceSQ::iterator       InterfaceSQIterator;

    typedef InterfaceSQ::const_iterator InterfaceSQConstIterator;

    void
    addInterface( const DNBClassID &classID,
        DNBCOMObject::Pointer pInterface )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEAlreadyRegistered));

    void
    removeInterface( const DNBClassID &classID,
        DNBCOMObject::Pointer pInterface )
        DNB_THROW_SPEC_NULL;

    void
    cloneInterfaces( const DNBExtendibleObject &right, CopyMode mode )
        DNB_THROW_SPEC((scl_bad_alloc, DNBException));

    //
    //  Define the data members for the class.
    //
    ExtensionDB     extensionDB_;       // Database of extension objects
    ExtensionSQ     extensionSQ_;       // Sequence of extension objects
    InterfaceDB     interfaceDB_;       // Database of interface objects
    InterfaceSQ     interfaceSQ_;       // Sequence of interface objects

    //
    //  Allow the class DNBExtensionObject to call the private methods
    //  addExtension() and removeExtension().
    //
    friend class DNBExtensionObject;

    //
    //  Allow the class DNBCOMObject to call the private methods addInterface()
    //  and removeInterface().
    //
    friend class DNBCOMObject;
};




#endif  /* _DNB_EXTENDIBLEOBJECT_H_ */
