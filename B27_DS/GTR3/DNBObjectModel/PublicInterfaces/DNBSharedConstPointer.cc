//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview RTL trangara 01:11:15
 */
// --------------------------------------------------------

template <class Object>
inline
DNBSharedConstPointer<Object>::DNBSharedConstPointer( const Object *object )
    DNB_THROW_SPEC_NULL :

    DNBSharedBase   ( object, ReadLock )
{
    // Nothing
}


template <class Object>
inline
DNBSharedConstPointer<Object>::DNBSharedConstPointer( const Handle &handle )
    DNB_THROW_SPEC_NULL :

    DNBSharedBase   ( handle, ReadLock )
{
    // Nothing
}


template <class Object>
inline
DNBSharedConstPointer<Object>::~DNBSharedConstPointer( )
    DNB_THROW_SPEC_NULL
{
    // Nothing
}


template <class Object>
inline const Object &
DNBSharedConstPointer<Object>::operator*( ) const
    DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION( isAcquired() );
    return *DNB_STATIC_CAST( Object *, getReferent() );
}


template <class Object>
inline const Object *
DNBSharedConstPointer<Object>::operator->( ) const
    DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION( isAcquired() );
    return  DNB_STATIC_CAST( Object *, getReferent() );
}


template <class Object>
inline void
DNBSharedConstPointer<Object>::release( )
    DNB_THROW_SPEC_NULL
{
    unlock( );
}


template <class Object>
inline void
DNBSharedConstPointer<Object>::clear( )
    DNB_THROW_SPEC_NULL
{
    detach( );
}


template <class Object>
inline const Object *
DNBSharedConstPointer<Object>::_getObject( ) const
    DNB_THROW_SPEC_NULL
{
    return DNB_STATIC_CAST( Object *, getReferent() );
}


template <class Object>
inline const Object *
DNBSharedConstPointer<Object>::getObject( ) const
    DNB_THROW_SPEC_NULL
{
    return DNB_STATIC_CAST( Object *, getReferent() );
}

#if ! defined(_IRIX_SOURCE)
template <class Object>
void *
DNBSharedConstPointer<Object>::operator new( size_t size )
    DNB_THROW_SPEC_NULL
{
    DNB_PROGRAM_ERROR( "Cannot create a shared pointer" );
    return NULL;
}


template <class Object>
void
DNBSharedConstPointer<Object>::operator delete( void *ptr )
    DNB_THROW_SPEC_NULL
{
    DNB_PROGRAM_ERROR( "Cannot delete a shared pointer" );
}
#endif
