//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//
//  TODO:
//    + Test the implementation of this class.
//
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNBSharedMutex_H_
#define _DNBSharedMutex_H_


#include <DNBSystemBase.h>
#include <scl_vector.h>
#include <DNBSystemDefs.h>


#include <DNBObjectModel.h>
#include <DNBCountedObject.h>
#include <DNBCountedPointer.h>
#include <DNBSpinLock.h>
#include <DNBSysFastMutexLock.h>
#include <DNBSysThreadId.h>

//
//  Provide the necessary forward declarations.
//
class DNBSharedObject;




//
//  This class represents a recursive mutual-exclusion lock (or mutex) that may
//  be shared among multiple objects.  It provides an interface that is roughly
//  compatible with the template class RWTRecursiveLock.  The semantics of the
//  lock are single-reader, one-writer (SROW).
//
class ExportedByDNBObjectModel DNBSharedMutex : public DNBCountedObject
{
    DNB_DECLARE_DYNAMIC_RTTI( DNBSharedMutex );

public:
    typedef DNBCountedPointer<DNBSharedMutex>   Pointer;


    DNBSharedMutex( )
        DNB_THROW_SPEC((DNBEResourceLimit));

    virtual
    ~DNBSharedMutex( )
        DNB_THROW_SPEC_NULL;

    void
    acquire( )
        DNB_THROW_SPEC_NULL;

    void
    acquireRead( )
        DNB_THROW_SPEC_NULL;

    void
    acquireWrite( )
        DNB_THROW_SPEC_NULL;

    void
    release( )
        DNB_THROW_SPEC_NULL;

    bool
    isLocked( ) const
        DNB_THROW_SPEC_NULL;

    bool
    isReadLocked( ) const
        DNB_THROW_SPEC_NULL;

    bool
    isWriteLocked( ) const
        DNB_THROW_SPEC_NULL;

    DNBInteger32
    getNestingLevel( ) const
        DNB_THROW_SPEC_NULL;

    void
    registerClient( DNBSharedObject *object )
        DNB_THROW_SPEC((scl_bad_alloc));

    void
    unregisterClient( DNBSharedObject *object )
        DNB_THROW_SPEC_NULL;

private:
    //
    //  Prohibit the following operations.
    //
    DNBSharedMutex( const DNBSharedMutex &right )
        DNB_THROW_SPEC_NULL;

    DNBSharedMutex &
    operator=( const DNBSharedMutex &right )
        DNB_THROW_SPEC_NULL;

    //
    //  Define some utility routines.
    //
    typedef DNBSysFastMutexLock     MutexType;

    typedef DNBSpinLock             MonitorType;

    typedef DNBSpinLock::LockGuard  MonitorGuard;

    inline  MonitorType &
    getOwnerLock( ) const
        DNB_THROW_SPEC_NULL
    {
        return DNB_MODIFY_MUTABLE(DNBSharedMutex, ownerLock_);
    }

    inline  MonitorType &
    getClientLock( ) const
        DNB_THROW_SPEC_NULL
    {
        return DNB_MODIFY_MUTABLE(DNBSharedMutex, clientLock_);
    }

    typedef scl_vector<DNBSharedObject *, DNB_ALLOCATOR(DNBSharedObject *) >
        ClientDB;

    typedef ClientDB::iterator      ClientDBIterator;

    void
    beginWriteTxn( )
        DNB_THROW_SPEC_NULL;

    void
    endWriteTxn( )
        DNB_THROW_SPEC_NULL;

    //
    //  The data member <numLevels_> specifies the nesting level of the shared
    //  mutex.  If <numLevels_> is positive, it represents the number of nested
    //  read locks.  If <numLevels_> is negative, it represents the number of
    //  nested write locks.  If a particular thread acquires a write lock and
    //  then a read lock, the latter is automatically converted to a write lock.
    //
    MutexType           mutexLock_;     // Recursive mutual exclusion lock.
    mutable MonitorType ownerLock_;     // Lock for threadID_ and numLevels_.
    DNBSysThreadId      threadID_;      // Owner of the shared mutex.
    DNBInteger32        numLevels_;     // Nesting level of the shared mutex.
    mutable MonitorType clientLock_;    // Lock for clientDB_.
    ClientDB            clientDB_;      // Set of shared objects using mutex.
};




#endif  /* _DNBSharedMutex_H_ */
