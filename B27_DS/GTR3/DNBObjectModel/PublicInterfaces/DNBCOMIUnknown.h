//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_COMIUNKNOWN_H_
#define _DNB_COMIUNKNOWN_H_


#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>


#include <DNBObjectModel.h>


#include <DNBWin32Defs.h>



#include "IUnknown.h"


#endif  /* _DNB_COMIUNKNOWN_H_ */
