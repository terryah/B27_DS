//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview RTL trangara 01:11:15
 */
// --------------------------------------------------------

#if defined (__HP_aCC) || defined (PLATEFORME_DS64)
#include <DNBDynamicObject.h>
#endif

template <class Object>
inline
DNBSharedHandle<Object>::DNBSharedHandle( const Object *object )
    DNB_THROW_SPEC_NULL :

    DNBSharedBase   ( object, NoLock )
{
    // Nothing
}


template <class Object>
inline
DNBSharedHandle<Object>::DNBSharedHandle( const DNBSharedBase &right )
    DNB_THROW_SPEC((DNBEInvalidCast)) :

    DNBSharedBase   ( downcast(right), NoLock )
{
    // Nothing
}


template <class Object>
inline
DNBSharedHandle<Object>::DNBSharedHandle( const DNBSharedHandle<Object> &right )
    DNB_THROW_SPEC_NULL :

    DNBSharedBase   ( right, NoLock )
{
    // Nothing
}


template <class Object>
inline
DNBSharedHandle<Object>::~DNBSharedHandle( )
    DNB_THROW_SPEC_NULL
{
    // Nothing
}


template <class Object>
inline DNBSharedHandle<Object> &
DNBSharedHandle<Object>::operator=( const Object *object )
    DNB_THROW_SPEC_NULL
{
    DNBSharedBase::operator=( object );

    return *this;
}


template <class Object>
inline DNBSharedHandle<Object> &
DNBSharedHandle<Object>::operator=( const DNBSharedBase &right )
    DNB_THROW_SPEC((DNBEInvalidCast))
{
    //
    //  Note: The following method checks for self-assignment.
    //
    DNBSharedBase::operator=( downcast(right) );

    return *this;
}


template <class Object>
inline DNBSharedHandle<Object> &
DNBSharedHandle<Object>::operator=( const DNBSharedHandle<Object> &right )
    DNB_THROW_SPEC_NULL
{
    //
    //  Note: The following method checks for self-assignment.
    //
    DNBSharedBase::operator=( right );

    return *this;
}


template <class Object>
inline void
DNBSharedHandle<Object>::clear( )
    DNB_THROW_SPEC_NULL
{
    detach( );
}


template <class Object>
inline Object *
DNBSharedHandle<Object>::_getObject( ) const
    DNB_THROW_SPEC_NULL
{
    return DNB_STATIC_CAST( Object *, getReferent() );
}


template <class Object>
inline Object *
DNBSharedHandle<Object>::getObject( ) const
    DNB_THROW_SPEC_NULL
{
    return DNB_STATIC_CAST( Object *, getReferent() );
}


template <class Object>
Object *
DNBSharedHandle<Object>::downcast( const DNBSharedBase &right )
    DNB_THROW_SPEC((DNBEInvalidCast))
{
    DNBSharedObject    *source = right._getReferent( );
    if ( source == NULL )
        return NULL;

#if     defined(__SUNPRO_CC) || defined(__HP_aCC) || defined(PLATEFORME_DS64)
    Object     *target = DNBDynamicCast<Object *>( source );
#else
    Object     *target = dynamic_cast<Object *>( source );
#endif

    if ( target == NULL )
    {
        DNBEInvalidCast     error( DNB_FORMAT("The source and target handles \
have incompatible referent types") );
        throw error;
    }

    return target;
}
