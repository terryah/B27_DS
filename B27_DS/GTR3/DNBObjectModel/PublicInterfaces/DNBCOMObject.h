//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//
//  Notes:
//    + None
//
//  TODO:
//    + Nothing
//
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_COMOBJECT_H_
#define _DNB_COMOBJECT_H_


#include <DNBSystemBase.h>
#include <scl_vector.h>
#include <scl_list.h>
#include <DNBSystemDefs.h>


#include <DNBObjectModel.h>


#include <DNBUUID.h>
#include <DNBCOMPointer.h>
#include <DNBSharedHandle.h>
#include <DNBCOMIUnknown.h>
#include <DNBCountedObject.h>


//
//  Provide the necessary forward declarations.
//
class DNBExtendibleObject;




#define DNB_DECLARE_COM_OBJECT(_CLASS, _IFACE, _BASIC)                      \
public:                                                                     \
    typedef DNBCOMPointer<_CLASS >                      Pointer;            \
    typedef scl_vector<Pointer, DNB_ALLOCATOR(Pointer) >    Vector;             \
    typedef scl_list  <Pointer, DNB_ALLOCATOR(Pointer) > List;               \
    static  const DNBClassID &                                              \
    ClassID( )                                                              \
        DNB_THROW_SPEC_NULL                                                 \
    {                                                                       \
        return ClassID_;                                                    \
    }                                                                       \
    virtual const DNBClassID &                                              \
    getClassID( ) const                                                     \
        DNB_THROW_SPEC_NULL                                                 \
    {                                                                       \
        return ClassID_;                                                    \
    }                                                                       \
    static  DNBCOMPointer<_IFACE>                                           \
    clone( _CLASS::Pointer pSource, CopyMode mode,                          \
        DNBSharedHandle<_BASIC> hBasic )                                    \
        DNB_THROW_SPEC((scl_bad_alloc, DNBException))                           \
    {                                                                       \
        return CloneObject( pSource, mode, hBasic );                        \
    }                                                                       \
    static  void                                                            \
    destroy( _CLASS::Pointer &pTarget )                                     \
        DNB_THROW_SPEC_NULL                                                 \
    {                                                                       \
        DestroyObject( pTarget );                                           \
        pTarget.clear( );                                                   \
    }                                                                       \
protected:                                                                  \
    static  DNBCOMPointer<_IFACE>                                           \
    CloneObject( _CLASS::Pointer pSource, CopyMode mode,                    \
        DNBSharedHandle<_BASIC> hBasic )                                    \
        DNB_THROW_SPEC((scl_bad_alloc, DNBException))                           \
    {                                                                       \
        _CLASS                 *rTarget = DNB_STATIC_CAST( _CLASS *,        \
            pSource->duplicateObject( mode ) );                             \
        _CLASS::Pointer         pTarget( rTarget );                         \
        rTarget->bindInterface( hBasic );                                   \
        return pTarget;                                                     \
    }                                                                       \
    static  void                                                            \
    DestroyObject( _CLASS::Pointer pTarget )                                \
        DNB_THROW_SPEC_NULL                                                 \
    {                                                                       \
        pTarget->orphanObject( );                                           \
    }                                                                       \
    virtual void                                                            \
    bindMembers( )                                                          \
        DNB_THROW_SPEC_NULL;                                                \
private:                                                                    \
    _CLASS( const _CLASS &right )                                           \
        DNB_THROW_SPEC_NULL;                                                \
    _CLASS &                                                                \
    operator=( const _CLASS &right )                                        \
        DNB_THROW_SPEC_NULL;                                                \
    static const DNBClassID     ClassID_


#define DNB_DEFINE_COM_OBJECT(_CLASS, _IFACE, _BASIC, _CLSID)               \
    const DNBClassID                                                        \
    _CLASS::ClassID_ = DNBClassID( _CLSID )




#define DNB_DECLARE_COM_FACTORY(_CLASS, _IFACE, _BASIC)                     \
public:                                                                     \
    static  DNBCOMPointer<_IFACE>                                           \
    create( DNBSharedHandle<_BASIC> hBasic )                                \
        DNB_THROW_SPEC((scl_bad_alloc, DNBException))                           \
    {                                                                       \
        return CreateObject( hBasic );                                      \
    }                                                                       \
protected:                                                                  \
    static  DNBCOMPointer<_IFACE>                                           \
    CreateObject( DNBSharedHandle<_BASIC> hBasic )                          \
        DNB_THROW_SPEC((scl_bad_alloc, DNBException))                           \
    {                                                                       \
        _CLASS                 *rTarget = DNB_NEW _CLASS( );                \
        _CLASS::Pointer         pTarget( rTarget );                         \
        rTarget->bindInterface( hBasic );                                   \
        return pTarget;                                                     \
    }


#define DNB_DEFINE_COM_FACTORY(_CLASS, _IFACE, _BASIC)  /* nothing */




//
//  This class represents the base class for all COM-based implementation
//  objects.
//
class ExportedByDNBObjectModel DNBCOMObject : public virtual IUnknown,
    public DNBCountedObject
{
    DNB_DECLARE_DYNAMIC_CREATE( DNBCOMObject );

    DNB_DECLARE_COM_OBJECT( DNBCOMObject, IUnknown, DNBExtendibleObject );

public:
    //
    //  The following typedef is equivalent to DNBExtendibleObject::Handle.  The
    //  latter form cannot be used here, to avoid circular dependencies between
    //  this header and <DNB/ExtendibleObject.h>.
    //
    typedef DNBSharedHandle<DNBExtendibleObject>    ExtendibleHandle;

    //
    //  This method returns a handle to the entity's extendible object.
    //
    ExtendibleHandle
    getExtendible( ) const
        DNB_THROW_SPEC_NULL;

    //
    //  The following three methods are inherited from IUnknown.
    //
    virtual HRESULT __stdcall
    QueryInterface( REFIID riid, void **ppvObject )
        DNB_THROW_SPEC_ANY;

    virtual ULONG __stdcall
    AddRef( )
        DNB_THROW_SPEC_ANY;

    virtual ULONG __stdcall
    Release( )
        DNB_THROW_SPEC_ANY;

protected:
    //
    //  By defining the following methods in the protected section, this class
    //  can only be used as a base class.
    //
    DNBCOMObject( )
        DNB_THROW_SPEC((DNBEResourceLimit));

    DNBCOMObject( const DNBCOMObject &right, CopyMode mode )
        DNB_THROW_SPEC((DNBEResourceLimit));

    virtual
    ~DNBCOMObject( )
        DNB_THROW_SPEC_NULL;

    void
    bindInterface( ExtendibleHandle hExtendible )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEAlreadyRegistered));

private:
    ExtendibleHandle    hExtendible_;   // Handle to the extendible object.

    //
    //  Allow the class DNBExtendibleObject to call the protected methods
    //  DestroyObject() and CloneObject().
    //
    friend class DNBExtendibleObject;
};




#endif  /* _DNB_COMOBJECT_H_ */
