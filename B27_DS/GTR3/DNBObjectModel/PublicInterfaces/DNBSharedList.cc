//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview RTL trangara 01:11:15
 */
// --------------------------------------------------------

#include <DNBSharedVector.h>
#include <DNBSharedSet.h>


template <class Object>
DNBSharedList<Object>::DNBSharedList( )
    DNB_THROW_SPEC_NULL :

    Base( )
{
    // Nothing
}


template <class Object>
DNBSharedList<Object>::DNBSharedList( const DNBSharedList<Object> &right )
    DNB_THROW_SPEC((scl_bad_alloc)) :

    Base( right )
{
    // Nothing
}


template <class Object>
DNBSharedList<Object>::~DNBSharedList( )
    DNB_THROW_SPEC_NULL
{
    // Nothing
}


template <class Object>
DNBSharedList<Object> &
DNBSharedList<Object>::operator=( const DNBSharedList<Object> &right )
    DNB_THROW_SPEC_NULL
{
    if ( this != &right )               // Prevent self-assignment.
        Base::operator=( right );

    return *this;
}


template <class Object>
DNBSharedList<Object> &
DNBSharedList<Object>::operator=( const DNBSharedVector<Object> &right )
    DNB_THROW_SPEC_NULL
{
    this->clear();

    typename DNBSharedVector<Object>::const_iterator iter = right.begin( );
    typename DNBSharedVector<Object>::const_iterator last = right.end( );

    for ( ; iter != last; ++iter )
        push_back( *iter );

    return *this;
}

template <class Object>
DNBSharedList<Object> &
DNBSharedList<Object>::operator=( const DNBSharedSet<Object> &right )
    DNB_THROW_SPEC_NULL
{
    this->clear();

    typename DNBSharedSet<Object>::const_iterator iter = right.begin( );
    typename DNBSharedSet<Object>::const_iterator last = right.end( );

    for ( ; iter != last; ++iter )
        push_back( *iter );

    return *this;
}


template <class Target, class Source>
void
DNBSharedAssign( DNBSharedList<Target> &target,
    const DNBSharedList<Source> &source )
    DNB_THROW_SPEC((scl_bad_alloc))
{
    target.clear( );

    typename DNBSharedList<Source>::const_iterator   iter = source.begin( );
    typename DNBSharedList<Source>::const_iterator   last = source.end( );

    for ( ; iter != last; ++iter )
    {
        Source     *rSource = (*iter)._getObject( );
        DNB_ASSERT( rSource->isDerivedFrom( Target::ClassInfo() ) );

        target.push_back( DNB_STATIC_CAST(Target *, rSource) );
    }
}
