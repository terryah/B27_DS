//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview RTL trangara 01:11:15
 */
// --------------------------------------------------------

inline  void
DNBNotificationAgent::postEvent( EventID id )
    DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION( id < EventCapacity );
    (void) postedEvents_.set( id );
}


inline  void
DNBNotificationAgent::cancelEvent( EventID id )
    DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION( id < EventCapacity );
    (void) postedEvents_.reset( id );
}


inline  bool
DNBNotificationAgent::testEvent( EventID id ) const
    DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION( id < EventCapacity );
    return postedEvents_.test( id );
}


inline  void
DNBNotificationAgent::cancelEvents( )
    DNB_THROW_SPEC_NULL
{
    (void) postedEvents_.reset( );
}


inline  DNBNotificationAgent::EventSet
DNBNotificationAgent::getEvents( ) const
    DNB_THROW_SPEC_NULL
{
    return postedEvents_;
}


inline  void
DNBNotificationAgent::postNotification( const DNBNotificationBase &notif )
    DNB_THROW_SPEC((scl_bad_alloc))
{
    DNBNotificationBase    *notifPtr  = notif.clone( );
    const DNBClassInfo     *classInfo = &notif.getClassInfo( );

    NotifDB::value_type     item( classInfo, notifPtr );

    postedNotifs_.insert( item );
}


inline  void
DNBNotificationAgent::cancelNotification( const DNBClassInfo &classInfo )
    DNB_THROW_SPEC_NULL
{
    const DNBNotificationBase *notifPtr = NULL;
    NotifConstIterator iter = postedNotifs_.find( &classInfo );
    if( iter != postedNotifs_.end() )
        notifPtr = (*iter).second;

    postedNotifs_.erase( &classInfo );

    if( notifPtr )
        delete notifPtr;
}


inline  bool
DNBNotificationAgent::testNotification( const DNBClassInfo &classInfo ) const
    DNB_THROW_SPEC_NULL
{
    NotifConstIterator  iter = postedNotifs_.find( &classInfo );
    return ( iter != postedNotifs_.end() );
}


inline  void
DNBNotificationAgent::cancelNotifications( )
    DNB_THROW_SPEC_NULL
{
    // clean-up notifications
    NotifIterator cleanIter = postedNotifs_.begin();
    NotifIterator cleanIterEnd = postedNotifs_.end();
    for( ; cleanIter != cleanIterEnd; cleanIter++ )
    {
        const DNBNotificationBase *notifBase = (*cleanIter).second;
        if( notifBase )
        {
            delete notifBase;
        }
    }

    postedNotifs_.clear( );
}
