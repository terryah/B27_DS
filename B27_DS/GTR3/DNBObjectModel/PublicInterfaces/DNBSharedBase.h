//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//
//  Notes:
//    + None
//
//  TODO:
//    + Nothing
//
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_SHAREDBASE_H_
#define _DNB_SHAREDBASE_H_


#include <DNBSystemBase.h>
#include <scl_string.h>
#include <DNBSystemDefs.h>


#include <DNBObjectModel.h>


#include <DNBClassInfo.h>


//
//  Provide the necessary forward declarations.
//
class DNBSharedObject;



//
//  This class represents the base class for transaction-locking smart pointers.
//  It is provided to facilitate the copy-construction and assignment of
//  compatible types of smart pointers.
//
class ExportedByDNBObjectModel DNBSharedBase
{
public:
    //
    //  This method returns true if <self> is bound to an object, and false
    //  otherwise.
    //
    inline
    operator bool( ) const
        DNB_THROW_SPEC_NULL;

    //
    //  This method returns true if <self> is bound to an object, and false
    //  otherwise.
    //
    inline bool
    isBound( ) const
        DNB_THROW_SPEC_NULL;

    //
    //  This method returns true if <self> references the same object as the
    //  argument <object>, and false otherwise.
    //
    inline bool
    operator==( const DNBSharedObject *object ) const
        DNB_THROW_SPEC_NULL;

    //
    //  This method returns true if <self> references the same object as
    //  <right>, and false otherwise.
    //
    inline bool
    operator==( const DNBSharedBase &right ) const
        DNB_THROW_SPEC_NULL;

    //
    //  This method returns true if <self> references a different object than
    //  the argument <object>, and false otherwise.
    //
    inline bool
    operator!=( const DNBSharedObject *object ) const
        DNB_THROW_SPEC_NULL;

    //
    //  This method returns true if <self> references a different object than
    //  <right>, and false otherwise.
    //
    inline bool
    operator!=( const DNBSharedBase &right ) const
        DNB_THROW_SPEC_NULL;

    //
    //  This method implements a trivial less-than operator that compares the
    //  addresses of <self>'s object and <right>'s object.  By providing such a
    //  method, smart pointers can be stored in STL containers.  Note: If a
    //  different ordering scheme is desired, the method can be overriden in a
    //  derived class.
    //
    inline bool
    operator< ( const DNBSharedBase &right ) const
        DNB_THROW_SPEC_NULL;

    //
    //  This method returns true if <self> has acquired a lock on its shared
    //  object, and false otherwise.
    //
    inline bool
    isAcquired( ) const
        DNB_THROW_SPEC_NULL;

    //
    //  The following methods provide RTTI information on the referent.
    //
    scl_string
    getClassName( ) const
        DNB_THROW_SPEC_NULL;

    const DNBClassInfo &
    getClassInfo( ) const
        DNB_THROW_SPEC_NULL;

    bool
    isInstanceOf( const DNBClassInfo &other ) const
        DNB_THROW_SPEC_NULL;

    bool
    isDerivedFrom( const DNBClassInfo &other ) const
        DNB_THROW_SPEC_NULL;

    //
    //  This method returns the raw pointer to the referent.  It should not be
    //  called in client code.
    //
    inline DNBSharedObject *
    _getReferent( ) const
        DNB_THROW_SPEC_NULL;

protected:
    //
    //  By defining the following methods in the protected section, this class
    //  can only be used as a base class.
    //
    enum LockMode
    {
        NoLock,
        ReadLock,
        WriteLock
    };

    DNBSharedBase( const DNBSharedObject *object, LockMode mode = NoLock )
        DNB_THROW_SPEC_NULL;

    DNBSharedBase( const DNBSharedBase &right, LockMode mode = NoLock )
        DNB_THROW_SPEC_NULL;

    ~DNBSharedBase( )
        DNB_THROW_SPEC_NULL;

    DNBSharedBase &
    operator=( const DNBSharedObject *object )
        DNB_THROW_SPEC_NULL;

    DNBSharedBase &
    operator=( const DNBSharedBase &right )
        DNB_THROW_SPEC_NULL;

    //
    //  Define some utility methods.
    //
    inline DNBSharedObject *
    getReferent( ) const
        DNB_THROW_SPEC_NULL;

    void
    unlock( )
        DNB_THROW_SPEC_NULL;

    void
    transfer( DNBSharedObject *object )
        DNB_THROW_SPEC_NULL;

    void
    detach( )
        DNB_THROW_SPEC_NULL;

private:
    void
    attach( LockMode mode )
        DNB_THROW_SPEC_NULL;

    DNBSharedObject    *object_;
    bool                isAcquired_;
};




//
//  Include the public definition file.
//
#include "DNBSharedBase.cc"




#endif  /* _DNB_SHAREDBASE_H_ */
