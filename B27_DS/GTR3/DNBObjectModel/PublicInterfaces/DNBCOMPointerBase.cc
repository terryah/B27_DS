//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview RTL trangara 01:11:15
 */
// --------------------------------------------------------

inline
DNBCOMPointerBase::operator bool( ) const
    DNB_THROW_SPEC_NULL
{
    return ( iface_ != NULL );
}


inline bool
DNBCOMPointerBase::isBound( ) const
    DNB_THROW_SPEC_NULL
{
    return ( iface_ != NULL );
}


inline bool
DNBCOMPointerBase::operator==( const IUnknown *iface ) const
    DNB_THROW_SPEC_NULL
{
    return ( iface_ == iface );
}


inline bool
DNBCOMPointerBase::operator==( const DNBCOMPointerBase &right ) const
    DNB_THROW_SPEC_NULL
{
    return ( iface_ == right.iface_ );
}


inline bool
DNBCOMPointerBase::operator!=( const IUnknown *iface ) const
    DNB_THROW_SPEC_NULL
{
    return ( iface_ != iface );
}


inline bool
DNBCOMPointerBase::operator!=( const DNBCOMPointerBase &right ) const
    DNB_THROW_SPEC_NULL
{
    return ( iface_ != right.iface_ );
}


inline bool
DNBCOMPointerBase::operator< ( const DNBCOMPointerBase &right ) const
    DNB_THROW_SPEC_NULL
{
    return ( iface_ < right.iface_ );
}


inline IUnknown *
DNBCOMPointerBase::_getReferent( ) const
    DNB_THROW_SPEC_NULL
{
    return iface_;
}


inline IUnknown *
DNBCOMPointerBase::getReferent( ) const
    DNB_THROW_SPEC_NULL
{
    return iface_;
}
