//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview RTL trangara 01:11:15
 */
// --------------------------------------------------------


template <class Object>
DNBSharedSet<Object>::DNBSharedSet( )
    DNB_THROW_SPEC_NULL :

    Base( )
{
    // Nothing
}


template <class Object>
DNBSharedSet<Object>::DNBSharedSet( const DNBSharedSet<Object> &right )
    DNB_THROW_SPEC((scl_bad_alloc)) :

    Base( right )
{
    // Nothing
}


template <class Object>
DNBSharedSet<Object>::~DNBSharedSet( )
    DNB_THROW_SPEC_NULL
{
    // Nothing
}


template <class Object>
DNBSharedSet<Object> &
DNBSharedSet<Object>::operator=( const DNBSharedSet<Object> &right )
    DNB_THROW_SPEC_NULL
{
    if ( this != &right )               // Prevent self-assignment.
        Base::operator=( right );

    return *this;
}

template <class Target, class Source>
void
DNBSharedAssign( DNBSharedSet<Target> &target,
    const DNBSharedSet<Source> &source )
    DNB_THROW_SPEC((scl_bad_alloc))
{
    target.clear( );

    typename DNBSharedSet<Source>::const_iterator   iter = source.begin( );
    typename DNBSharedSet<Source>::const_iterator   last = source.end( );

    for ( ; iter != last; ++iter )
    {
        Source     *rSource = (*iter)._getObject( );
        DNB_ASSERT( rSource->isDerivedFrom( Target::ClassInfo() ) );

        target.push_back( DNB_STATIC_CAST(Target *, rSource) );
    }
}
