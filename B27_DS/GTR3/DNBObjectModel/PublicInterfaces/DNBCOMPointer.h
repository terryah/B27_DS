//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//
//  Notes:
//   +  None
//
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_COMPOINTER_H_
#define _DNB_COMPOINTER_H_


#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>


#include <DNBObjectModel.h>


#include <DNBCOMPointerBase.h>
#include <DNBException.h>




//
//  This class represents a reference-counting smart pointer to a COM interface
//  that is derived from IUnknown.  Each time an instance of this class is bound
//  to the interface, it increments the interface's reference count.  Further,
//  each time it detaches from the interface, it decrements the interface's
//  reference count.  When the reference count reaches zero, the interface is
//  automatically deleted from the system.  The reference-counting facility
//  helps to eliminate potential memory resource errors and leaks, especially
//  in the presence of C++ exceptions and multiple threads.  In general, counted
//  pointers can be stored in global memory (static storage), in dynamically
//  allocated memory (dynamic storage), or on the program stack (automatic
//  storage).
//
template <class Iface>
class DNBCOMPointer : public DNBCOMPointerBase
{
public:
    //
    //  This method creates a smart pointer which is bound to <iface> and
    //  increments the reference count on the interface.
    //
    inline
    DNBCOMPointer( const Iface *iface = NULL )
        DNB_THROW_SPEC_NULL;

    //
    //  This method creates a smart pointer which is bound to <right>'s
    //  interface and increments the reference count on the interface.
    //
    inline
    DNBCOMPointer( const DNBCOMPointerBase &right )
        DNB_THROW_SPEC((DNBEInvalidCast));

    //
    //  This method creates a smart pointer which is bound to <right>'s
    //  interface and increments the reference count on the interface.
    //
    inline
    DNBCOMPointer( const DNBCOMPointer<Iface> &right )
        DNB_THROW_SPEC_NULL;

    //
    //  This method detaches <self> from the current interface (if any),
    //  decrements the interface's reference count, and deletes the interface
    //  if there are no other references to it.
    //
    inline
    ~DNBCOMPointer( )
        DNB_THROW_SPEC_NULL;

    //
    //  This method detaches <self> from the current interface (if any),
    //  decrements the interface's reference count, and deletes the interface
    //  if there are no other references to it.  The method then binds <self>
    //  to <iface> and increments the reference count on the interface.
    //
    inline DNBCOMPointer<Iface> &
    operator=( const Iface *iface )
        DNB_THROW_SPEC_NULL;

    //
    //  This method detaches <self> from the current interface (if any),
    //  decrements the interface's reference count, and deletes the interface
    //  if there are no other references to it.  The method then binds <self>
    //  to <right>'s interface and increments the reference count on the
    //  interface.
    //
    inline DNBCOMPointer<Iface> &
    operator=( const DNBCOMPointerBase &right )
        DNB_THROW_SPEC((DNBEInvalidCast));

    //
    //  This method detaches <self> from the current interface (if any),
    //  decrements the interface's reference count, and deletes the interface
    //  if there are no other references to it.  The method then binds <self>
    //  to <right>'s interface and increments the reference count on the
    //  interface.
    //
    inline DNBCOMPointer<Iface> &
    operator=( const DNBCOMPointer<Iface> &right )
        DNB_THROW_SPEC_NULL;

    //
    //  This method detaches <self> from the current interface (if any),
    //  decrements the interface's reference count, and deletes the interface
    //  if there are no other references to it.
    //
    inline void
    clear( )
        DNB_THROW_SPEC_NULL;

    inline Iface &
    operator*( ) const
        DNB_THROW_SPEC_NULL;

    inline Iface *
    operator->( ) const
        DNB_THROW_SPEC_NULL;

    //
    //  This method returns the raw pointer to the referent.  It should not be
    //  used in client code.
    //
    inline Iface *
    _getInterface( ) const
        DNB_THROW_SPEC_NULL;

protected:
    inline Iface *
    getInterface( ) const
        DNB_THROW_SPEC_NULL;

private:
    static Iface *
    downcast( const DNBCOMPointerBase &right )
        DNB_THROW_SPEC((DNBEInvalidCast));
};




//
//  Include the public definition file.
//
#include "DNBCOMPointer.cc"




#endif  /* _DNB_COMPOINTER_H_ */
