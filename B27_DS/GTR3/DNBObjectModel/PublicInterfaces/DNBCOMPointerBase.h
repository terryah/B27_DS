//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//
//  Notes:
//    + None
//
//  TODO:
//    + Nothing
//
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_COMPOINTERBASE_H_
#define _DNB_COMPOINTERBASE_H_


#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>


#include <DNBObjectModel.h>




//
//  Provide the necessary forward declarations.
//
struct IUnknown;




//
//  This class represents the base class for reference-counting smart pointers
//  to COM interfaces.  It is provided to facilitate the copy-construction and
//  assignment of compatible types of smart pointers.
//
class ExportedByDNBObjectModel DNBCOMPointerBase
{
public:
    //
    //  This method returns true if <self> is bound to an interface, and false
    //  otherwise.
    //
    inline
    operator bool( ) const
        DNB_THROW_SPEC_NULL;

    //
    //  This method returns true if <self> is bound to an interface, and false
    //  otherwise.
    //
    inline bool
    isBound( ) const
        DNB_THROW_SPEC_NULL;

    //
    //  This method returns true if <self> references the same interface as the
    //  argument <iface>, and false otherwise.
    //
    inline bool
    operator==( const IUnknown *iface ) const
        DNB_THROW_SPEC_NULL;

    //
    //  This method returns true if <self> references the same interface as
    //  <right>, and false otherwise.
    //
    inline bool
    operator==( const DNBCOMPointerBase &right ) const
        DNB_THROW_SPEC_NULL;

    //
    //  This method returns true if <self> references a different interface than
    //  the argument <iface>, and false otherwise.
    //
    inline bool
    operator!=( const IUnknown *iface ) const
        DNB_THROW_SPEC_NULL;

    //
    //  This method returns true if <self> references a different interface than
    //  <right>, and false otherwise.
    //
    inline bool
    operator!=( const DNBCOMPointerBase &right ) const
        DNB_THROW_SPEC_NULL;

    //
    //  This method implements a trivial less-than operator that compares the
    //  addresses of <self>'s interface and <right>'s interface.  By providing
    //  such a method, smart pointers can be stored in STL containers.  Note:
    //  If a different ordering scheme is desired, the method can be overriden
    //  in a derived class.
    //
    inline bool
    operator< ( const DNBCOMPointerBase &right ) const
        DNB_THROW_SPEC_NULL;

    //
    //  This method returns the raw pointer to the referent.  It should not be
    //  called in client code.
    //
    inline IUnknown *
    _getReferent( ) const
        DNB_THROW_SPEC_NULL;

protected:
    //
    //  By defining the following methods in the protected section, this class
    //  can only be used as a base class.
    //
    DNBCOMPointerBase( const IUnknown *iface )
        DNB_THROW_SPEC_NULL;

    DNBCOMPointerBase( const DNBCOMPointerBase &right )
        DNB_THROW_SPEC_NULL;

    ~DNBCOMPointerBase( )
        DNB_THROW_SPEC_NULL;

    DNBCOMPointerBase &
    operator=( const IUnknown *iface )
        DNB_THROW_SPEC_NULL;

    DNBCOMPointerBase &
    operator=( const DNBCOMPointerBase &right )
        DNB_THROW_SPEC_NULL;

    //
    //  Define some utility methods.
    //
    inline IUnknown *
    getReferent( ) const
        DNB_THROW_SPEC_NULL;

    void
    detach( )
        DNB_THROW_SPEC_NULL;

private:
    void
    transfer( IUnknown *iface )
        DNB_THROW_SPEC_NULL;

    IUnknown   *iface_;
};




//
//  Include the public definition file.
//
#include "DNBCOMPointerBase.cc"




#endif  /* _DNB_COMPOINTERBASE_H_ */
