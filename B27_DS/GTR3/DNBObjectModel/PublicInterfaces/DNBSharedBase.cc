//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview RTL trangara 01:11:15
 */
// --------------------------------------------------------

inline
DNBSharedBase::operator bool( ) const
    DNB_THROW_SPEC_NULL
{
    return ( object_ != NULL );
}


inline bool
DNBSharedBase::isBound( ) const
    DNB_THROW_SPEC_NULL
{
    return ( object_ != NULL );
}


inline bool
DNBSharedBase::operator==( const DNBSharedObject *object ) const
    DNB_THROW_SPEC_NULL
{
    return ( object_ == object );
}


inline bool
DNBSharedBase::operator==( const DNBSharedBase &right ) const
    DNB_THROW_SPEC_NULL
{
    return ( object_ == right.object_ );
}


inline bool
DNBSharedBase::operator!=( const DNBSharedObject *object ) const
    DNB_THROW_SPEC_NULL
{
    return ( object_ != object );
}


inline bool
DNBSharedBase::operator!=( const DNBSharedBase &right ) const
    DNB_THROW_SPEC_NULL
{
    return ( object_ != right.object_ );
}


inline bool
DNBSharedBase::operator< ( const DNBSharedBase &right ) const
    DNB_THROW_SPEC_NULL
{
    return ( object_ < right.object_ );
}


inline bool
DNBSharedBase::isAcquired( ) const
    DNB_THROW_SPEC_NULL
{
    return isAcquired_;
}


inline DNBSharedObject *
DNBSharedBase::_getReferent( ) const
    DNB_THROW_SPEC_NULL
{
    return object_;
}


inline DNBSharedObject *
DNBSharedBase::getReferent( ) const
    DNB_THROW_SPEC_NULL
{
    return object_;
}
