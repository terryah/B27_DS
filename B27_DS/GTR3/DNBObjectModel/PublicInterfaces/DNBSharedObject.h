//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//
//  TODO:
//    + Consider declaring the synchronization and reference-counting methods in
//      the protected section of the class.  In this way, they may be accessed
//      only by derived classes and the appropriate "handle" classes.
//
// rtl : Ported to HP (02/14/2002)

#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNBSharedObject_H_
#define _DNBSharedObject_H_


#include <DNBSystemBase.h>
#include <scl_vector.h>
#include <DNBSystemDefs.h>


#include <DNBSharedHandle.h>
#include <DNBSharedVector.h>
#include <DNBSharedSet.h>
#include <DNBSharedList.h>
#include <DNBSharedPointer.h>
#include <DNBSharedConstPointer.h>
#include <DNBUUID.h>                    // For DNBClassID
#include <DNBObjectModel.h>
#include <DNBCountedObject.h>
#include <DNBSpinLock.h>
#include <DNBCallback.h>
#include <DNBSharedMutex.h>




#define DNB_DECLARE_SHARED_OBJECT(_CLASS)                                   \
public:                                                                     \
    typedef DNBSharedHandle<_CLASS >        Handle;                         \
    typedef DNBSharedVector<_CLASS >        Vector;                         \
    typedef DNBSharedList<_CLASS >          List;                           \
    typedef DNBSharedSet<_CLASS >           Set;                            \
    typedef DNBSharedPointer<_CLASS >       Pointer;                        \
    typedef DNBSharedConstPointer<_CLASS >  ConstPointer;                   \
    static  const DNBClassID &                                              \
    ClassID( )                                                              \
        DNB_THROW_SPEC_NULL                                                 \
    {                                                                       \
        return ClassID_;                                                    \
    }                                                                       \
    virtual const DNBClassID &                                              \
    getClassID( ) const                                                     \
        DNB_THROW_SPEC_NULL                                                 \
    {                                                                       \
        return ClassID_;                                                    \
    }                                                                       \
private:                                                                    \
    _CLASS( const _CLASS &right )                                           \
        DNB_THROW_SPEC_NULL;                                                \
    _CLASS &                                                                \
    operator=( const _CLASS &right )                                        \
        DNB_THROW_SPEC_NULL;                                                \
    static const DNBClassID     ClassID_


#define DNB_DEFINE_SHARED_OBJECT(_CLASS, _CLSID)                            \
    const DNBClassID                                                        \
    _CLASS::ClassID_ = DNBClassID( _CLSID )


#define DNB_DECLARE_SHARED_OBJECT_TMPL(_CLASS)                              \
public:                                                                     \
    typedef DNBSharedHandle<_CLASS >        Handle;                         \
    typedef DNBSharedVector<_CLASS >        Vector;                         \
    typedef DNBSharedList<_CLASS >          List;                           \
    typedef DNBSharedSet<_CLASS >           Set;                            \
    typedef DNBSharedPointer<_CLASS >       Pointer;                        \
    typedef DNBSharedConstPointer<_CLASS >  ConstPointer;                   \
    static  const DNBClassID &                                              \
    ClassID( )                                                              \
        DNB_THROW_SPEC_NULL                                                 \
    {                                                                       \
        return ClassID_;                                                    \
    }                                                                       \
    virtual const DNBClassID &                                              \
    getClassID( ) const                                                     \
        DNB_THROW_SPEC_NULL                                                 \
    {                                                                       \
        return ClassID_;                                                    \
    }                                                                       \
private:                                                                    \
    static const DNBClassID   ClassID_


#ifndef RWSTD_TEMPLATE
#define RWSTD_TEMPLATE template <>
#endif


#if defined(_WIN32) || defined(_WIN64)
#define DNB_DEFINE_SHARED_OBJECT_TMPL(_CLASS, _PARAMS)                      \
    template _PARAMS                                                        \
    const DNBClassID                                                        \
    _CLASS::ClassID_ = DNBClassID::Nil

#define DNB_DECLARE_SHARED_OBJECT_INST(_CLASS,_CLSID)                       \
    const  DNBClassID                                                      \
    _CLASS::ClassID_ = DNBClassID( _CLSID )

#define DNB_DEFINE_SHARED_OBJECT_INST(_CLASS, _CLSID)
#else
#define DNB_DEFINE_SHARED_OBJECT_TMPL(_CLASS, _PARAMS)

#define DNB_DECLARE_SHARED_OBJECT_INST(_CLASS, _CLSID)

#define DNB_DEFINE_SHARED_OBJECT_INST(_CLASS, _CLSID)                       \
     RWSTD_TEMPLATE  const DNBClassID                                       \
    _CLASS::ClassID_ = DNBClassID( _CLSID )
#endif


//
//  This class represents the base class for all shared objects.  It includes
//  facilities for run-time type identification (RTTI), dynamic construction,
//  object diagnostic testing, reference counting, and transaction-based locks.
//
class ExportedByDNBObjectModel DNBSharedObject : public DNBCountedObject
{
    DNB_DECLARE_DYNAMIC_CREATE( DNBSharedObject );

    DNB_DECLARE_SHARED_OBJECT( DNBSharedObject );

public:
    inline  void
    acquireReadLock( )
        DNB_THROW_SPEC_NULL;

    inline  void
    acquireWriteLock( )
        DNB_THROW_SPEC_NULL;

    inline  void
    releaseLock( )
        DNB_THROW_SPEC_NULL;

    inline  bool
    isLocked( ) const
        DNB_THROW_SPEC_NULL;

    inline  bool
    isReadLocked( ) const
        DNB_THROW_SPEC_NULL;

    inline  bool
    isWriteLocked( ) const
        DNB_THROW_SPEC_NULL;

    inline  DNBInteger32
    getNestingLevel( ) const
        DNB_THROW_SPEC_NULL;

    bool
    sharesLockWith( DNBSharedObject::Handle hOther ) const
        DNB_THROW_SPEC_NULL;

protected:
    //
    //  By defining the following methods in the protected section, this class
    //  may be used only as a base class.
    //
    DNBSharedObject( bool createLock = true )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit));

    DNBSharedObject( const DNBSharedObject &right, CopyMode mode )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit));

    virtual
    ~DNBSharedObject( )
        DNB_THROW_SPEC_NULL;

    //
    //  Declare some utility methods.
    //
    typedef DNBSpinLock             MonitorType;

    typedef DNBSpinLock::LockGuard  MonitorGuard;

    inline  MonitorType &
    getMonitor( ) const
        DNB_THROW_SPEC_NULL
    {
        return DNB_MODIFY_MUTABLE(DNBSharedObject, monitor_);
    }

    void
    assignLock( DNBSharedObject::Handle hOther )
        DNB_THROW_SPEC((scl_bad_alloc));

    enum TransactionTrigger
    {
        BeginWriteTxn,
        EndWriteTxn
    };

    typedef void (DNBSharedObject::*TransactionHookFunc)(TransactionTrigger);

    typedef DNBFunctor1<TransactionTrigger> TransactionHook;

    static  TransactionHook *               TransactionHookNil;

    void
    addTransactionHook( const TransactionHook &hook )
        DNB_THROW_SPEC((scl_bad_alloc));

    void
    removeTransactionHook( const TransactionHook &hook )
        DNB_THROW_SPEC_NULL;

private:
    //
    //  Define some utility routines.
    //
    typedef scl_vector<TransactionHook, DNB_ALLOCATOR(TransactionHook) >
        TransactionHookDB;

    typedef TransactionHookDB::iterator         TransactionHookIterator;

    typedef TransactionHookDB::reverse_iterator TransactionHookRevIterator;

    void
    beginWriteTxn( )
        DNB_THROW_SPEC_NULL;

    void
    endWriteTxn( )
        DNB_THROW_SPEC_NULL;

    //
    //  Define the data members.
    //
    bool                    createLock_;    // True if a lock was created.
    mutable MonitorType     monitor_;       // Mutex for atomic method calls
    DNBSharedMutex::Pointer txnLock_;       // Transaction lock for the object
    TransactionHookDB       txnHookDB_;     // Set of transaction hooks

    //
    //  Allow the class DNBSharedMutex to call the methods beginWriteTxn() and
    //  endWriteTxn().
    //
    friend class DNBSharedMutex;
};




//
//  Include the public definition file.
//
#include "DNBSharedObject.cc"




#endif  /* _DNBSharedObject_H_ */
