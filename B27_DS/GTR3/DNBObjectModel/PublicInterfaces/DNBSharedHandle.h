//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//
//  Notes:
//   +  None
//
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_SHAREDHANDLE_H_
#define _DNB_SHAREDHANDLE_H_


#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>


#include <DNBObjectModel.h>


#include <DNBSharedBase.h>
#include <DNBException.h>



//
//  This class represents a handle to a shared object that is derived from
//  DNBSharedObject.  Each time an instance of this class is bound to the
//  object, it increments the object's reference count.  Further, each time it
//  detaches from the object, it decrements the object's reference count.  When
//  the reference count reaches zero, the object is automatically deleted from
//  the system.  The reference-counting facility helps to eliminate potential
//  memory resource errors and leaks, especially in the presence of C++
//  exceptions and multiple threads.  In general, handles can be stored in
//  global memory (static storage), in dynamically allocated memory (dynamic
//  storage), or on the program stack (automatic storage).
//
//  By convention, application programmers refer to the various entities in the
//  simulation model using shared handles--as opposed to raw pointers.  These
//  handles must be converted to shared pointers before calling any method on
//  the entity.  This procedure is explained in the header file for the class
//  DNBSharedPointer.
//
template <class Object>
class DNBSharedHandle : public DNBSharedBase
{
public:
    //
    //  This method creates a handle which is bound to <object> and increments
    //  the reference count on the object.
    //
    inline
    DNBSharedHandle( const Object *object = NULL )
        DNB_THROW_SPEC_NULL;

    //
    //  This method creates a handle which is bound to <right>'s object and
    //  increments the reference count on the object.
    //
    inline
    DNBSharedHandle( const DNBSharedBase &right )
        DNB_THROW_SPEC((DNBEInvalidCast));

    //
    //  This method creates a handle which is bound to <right>'s object and
    //  increments the reference count on the object.
    //
    inline
    DNBSharedHandle( const DNBSharedHandle<Object> &right )
        DNB_THROW_SPEC_NULL;

    //
    //  This method detaches <self> from the current object (if any), decrements
    //  the object's reference count, and deletes the object if there are no
    //  other references to it.
    //
    inline
    ~DNBSharedHandle( )
        DNB_THROW_SPEC_NULL;

    //
    //  This method detaches <self> from the current object (if any), decrements
    //  the object's reference count, and deletes the object if there are no
    //  other references to it.  The method then binds <self> to <object> and
    //  increments the reference count on the object.
    //
    inline DNBSharedHandle<Object> &
    operator=( const Object *object )
        DNB_THROW_SPEC_NULL;

    //
    //  This method detaches <self> from the current object (if any), decrements
    //  the object's reference count, and deletes the object if there are no
    //  other references to it.  The method then binds <self> to <right>'s
    //  object and increments the reference count on the object.
    //
    inline DNBSharedHandle<Object> &
    operator=( const DNBSharedBase &right )
        DNB_THROW_SPEC((DNBEInvalidCast));

    //
    //  This method detaches <self> from the current object (if any), decrements
    //  the object's reference count, and deletes the object if there are no
    //  other references to it.  The method then binds <self> to <right>'s
    //  object and increments the reference count on the object.
    //
    inline DNBSharedHandle<Object> &
    operator=( const DNBSharedHandle<Object> &right )
        DNB_THROW_SPEC_NULL;

    //
    //  This method detaches <self> from the current object (if any), decrements
    //  the object's reference count, and deletes the object if there are no
    //  other references to it.
    //
    inline void
    clear( )
        DNB_THROW_SPEC_NULL;

    //
    //  This method returns the raw pointer to the referent.  It should not be
    //  used in client code.
    //
    inline Object *
    _getObject( ) const
        DNB_THROW_SPEC_NULL;

protected:
    inline Object *
    getObject( ) const
        DNB_THROW_SPEC_NULL;

private:
    static Object *
    downcast( const DNBSharedBase &right )
        DNB_THROW_SPEC((DNBEInvalidCast));
};




//
//  Include the public definition file.
//
#include "DNBSharedHandle.cc"




#endif  /* _DNB_SHAREDHANDLE_H_ */
