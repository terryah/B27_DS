//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//
//  Notes:
//    + An extension object is both an event publisher and an event subscriber.
//
//    + For a particular entity, the extendible object and extension objects
//      share the same transaction lock.
//
//  TODO:
//    + Nothing
//
// rtl : Ported to HP (02/14/2002)
// mmg  08/29/03    modified how DNBExtensionObject handles cloning so that
//                  it passes a handle to the object being extended to the
//                  duplicateObject method
//
//

#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_EXTENSIONOBJECT_H_
#define _DNB_EXTENSIONOBJECT_H_


#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>


#include <DNBObjectModel.h>


#include <DNBNotificationAgent.h>


//
//  Provide the necessary forward declarations.
//
class DNBExtendibleObject;




#define DNB_DECLARE_EXTENSION_OBJECT(_CLASS, _BASIC)                        \
public:                                                                     \
    static  _CLASS::Handle                                                  \
    clone( _CLASS::Handle hSource, CopyMode mode,                           \
        DNBSharedHandle<_BASIC> hBasic )                                    \
        DNB_THROW_SPEC((scl_bad_alloc, DNBException))                           \
    {                                                                       \
        return CloneObject( hSource, mode, hBasic );                        \
    }                                                                       \
    static  void                                                            \
    destroy( _CLASS::Handle &hTarget )                                      \
        DNB_THROW_SPEC_NULL                                                 \
    {                                                                       \
        DestroyObject( hTarget );                                           \
        hTarget.clear( );                                                   \
    }                                                                       \
protected:                                                                  \
    static  _CLASS::Handle                                                  \
    CloneObject( _CLASS::Handle hSource, CopyMode mode,                     \
        DNBSharedHandle<_BASIC> hBasic )                                    \
        DNB_THROW_SPEC((scl_bad_alloc, DNBException))                           \
    {                                                                       \
        _CLASS::ConstPointer    pSource( hSource );                         \
        _CLASS                 *rTarget = DNB_STATIC_CAST( _CLASS *,        \
            pSource->duplicateObject( mode, hBasic ) );                     \
        _CLASS::Handle          hTarget( rTarget );                         \
        rTarget->bindExtension( hBasic );                                   \
        return hTarget;                                                     \
    }                                                                       \
    static  void                                                            \
    DestroyObject( _CLASS::Handle hTarget )                                 \
        DNB_THROW_SPEC_NULL                                                 \
    {                                                                       \
        _CLASS::Pointer         pTarget( hTarget );                         \
        pTarget->orphanObject( );                                           \
    }                                                                       \
    virtual void                                                            \
    bindMembers( )                                                          \
        DNB_THROW_SPEC_NULL


#define DNB_DEFINE_EXTENSION_OBJECT(_CLASS, _BASIC)     /* nothing */


#define DNB_DECLARE_EXTENSION_OBJECT_TMPL(_CLASS, _BASIC)                   \
public:                                                                     \
    static typename  _CLASS::Handle                                         \
    clone( typename _CLASS::Handle hSource, CopyMode mode,                  \
        DNBSharedHandle<_BASIC> hBasic )                                    \
        DNB_THROW_SPEC((scl_bad_alloc, DNBException))                           \
    {                                                                       \
        return CloneObject( hSource, mode, hBasic );                        \
    }                                                                       \
    static  void                                                            \
    destroy( typename _CLASS::Handle &hTarget )                             \
        DNB_THROW_SPEC_NULL                                                 \
    {                                                                       \
        DestroyObject( hTarget );                                           \
        hTarget.clear( );                                                   \
    }                                                                       \
protected:                                                                  \
    static  typename _CLASS::Handle                                         \
    CloneObject( typename _CLASS::Handle hSource, CopyMode mode,            \
        DNBSharedHandle<_BASIC> hBasic )                                    \
        DNB_THROW_SPEC((scl_bad_alloc, DNBException))                           \
    {                                                                       \
        typename _CLASS::ConstPointer    pSource( hSource );                \
        _CLASS                 *rTarget = DNB_STATIC_CAST( _CLASS *,        \
            pSource->duplicateObject( mode, hBasic ) );                     \
        typename _CLASS::Handle          hTarget( rTarget );                \
        rTarget->bindExtension( hBasic );                                   \
        return hTarget;                                                     \
    }                                                                       \
    static  void                                                            \
    DestroyObject( typename _CLASS::Handle hTarget )                        \
        DNB_THROW_SPEC_NULL                                                 \
    {                                                                       \
        typename _CLASS::Pointer         pTarget( hTarget );                \
        pTarget->orphanObject( );                                           \
    }                                                                       \
    virtual void                                                            \
    bindMembers( )                                                          \
        DNB_THROW_SPEC_NULL


#define DNB_DEFINE_EXTENSION_OBJECT_TMPL(_CLASS, _BASIC)     /* nothing */


#define DNB_DECLARE_EXTENSION_FACTORY(_CLASS, _BASIC)                       \
public:                                                                     \
    static  _CLASS::Handle                                                  \
    create( DNBSharedHandle<_BASIC> hBasic )                                \
        DNB_THROW_SPEC((scl_bad_alloc, DNBException))                           \
    {                                                                       \
        return CreateObject( hBasic );                                      \
    }                                                                       \
protected:                                                                  \
    static  _CLASS::Handle                                                  \
    CreateObject( DNBSharedHandle<_BASIC> hBasic )                          \
        DNB_THROW_SPEC((scl_bad_alloc, DNBException))                           \
    {                                                                       \
        _CLASS     *rTarget = DNB_NEW _CLASS( );                            \
        rTarget->bindExtension( hBasic );                                   \
        return rTarget;                                                     \
    }


#define DNB_DEFINE_EXTENSION_FACTORY(_CLASS, _BASIC)    /* nothing */




//
//  This class represents the base class for all extension classes.
//
class ExportedByDNBObjectModel DNBExtensionObject : public DNBNotificationAgent
{
    DNB_DECLARE_DYNAMIC_CREATE( DNBExtensionObject );

    DNB_DECLARE_SHARED_OBJECT( DNBExtensionObject );

    DNB_DECLARE_EXTENSION_OBJECT( DNBExtensionObject, DNBExtendibleObject );

public:
    //
    //  The following typedef is equivalent to DNBExtendibleObject::Handle.  The
    //  latter identifier cannot be used here because it would create a circular
    //  dependency between this header and <DNB/ExtendibleObject.h>.
    //
    typedef DNBSharedHandle<DNBExtendibleObject>    ExtendibleHandle;

    //
    //  TODO: Move this enumeration to DNBAssociationObject.
    //
    enum SetOrdering
    {
        NaturalOrder,
        DescendingOrder,
        AscendingOrder
    };

    //
    //  This method returns a handle to the entity's extendible object.
    //\V5Inf\DNBObjectModel\ProtectedInterfaces\DNBExtensionObject.h
    ExtendibleHandle
    getExtendible( ) const
        DNB_THROW_SPEC_NULL;

    //
    //  TODO:  The following method is deprecated.
    //
    ExtendibleHandle
    getBasicObject( ) const
        DNB_THROW_SPEC_NULL;

    //
    //  The following methods can be used to navigate to other extensions.
    //
    bool
    hasExtension( const DNBClassID &classID ) const
        DNB_THROW_SPEC_NULL;

    DNBExtensionObject::Handle
    getExtension( const DNBClassID &classID ) const
        DNB_THROW_SPEC_NULL;

    void
    getExtensions( DNBExtensionObject::Vector &gExtension ) const
        DNB_THROW_SPEC((scl_bad_alloc));

    void
    getExtensions( DNBExtensionObject::List   &gExtension ) const
        DNB_THROW_SPEC((scl_bad_alloc));

    enum Events
    {
        EventFirst = DNBNotificationAgent::EventLast - 1,
        EventLast
    };

protected:
    //
    //  By defining the following methods in the protected section, this class
    //  can only be used as a base class.
    //
    DNBExtensionObject( )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit));

    DNBExtensionObject( const DNBExtensionObject &right, CopyMode mode )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit));

    virtual
    ~DNBExtensionObject( )
        DNB_THROW_SPEC_NULL;

    void
    bindExtension( ExtendibleHandle hExtendible )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEAlreadyRegistered));

    virtual DNBDynamicObject *
    duplicateObject( CopyMode mode, DNBNotificationAgent::Handle basic ) const
        DNB_THROW_SPEC((scl_bad_alloc, DNBException));

private:
    ExtendibleHandle    hExtendible_;   // Handle to the extendible object.

    //
    //  Allow the class DNBExtendibleObject to call the protected methods
    //  DestroyObject() and CloneObject().
    //
    friend class DNBExtendibleObject;
};




#endif  /* _DNB_EXTENSIONOBJECT_H_ */
