//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//
//  Notes:
//    + None
//
//  TODO:
//    + In the event handler, consider passing a raw pointer to the publisher.
//      This should eliminate the need for DNBMakeEventFunctor.
//
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNBNotificationAgent_H_
#define _DNBNotificationAgent_H_


#include <DNBSystemBase.h>
#include <scl_bitset.h>
#include <scl_vector.h>
#include <scl_map.h>
#include <DNBSystemDefs.h>


#include <DNBObjectModel.h>
#include <DNBSharedObject.h>
#include <DNBCallback.h>
#include <DNBNotificationBase.h>




#define DNB_DECLARE_NOTIFICATION(_NOTIF, _DATA)                             \
private:                                                                    \
    static int  _NOTIF ## ID;                                               \
public:                                                                     \
    typedef DNBNotificationGeneric<_DATA, &_NOTIF ## ID> _NOTIF




//
//  This class represents a notification agent--an object that is capable of
//  both publishing events and subscribing to events.
//
class ExportedByDNBObjectModel DNBNotificationAgent : public DNBSharedObject
{
    DNB_DECLARE_DYNAMIC_CREATE( DNBNotificationAgent );

    DNB_DECLARE_SHARED_OBJECT( DNBNotificationAgent );

public:
    //
    //  Type definitions for event management.
    //
    typedef size_t                  EventID;

    enum { EventCapacity = 64 };

    typedef scl_bitset <EventCapacity>   EventSet;

    //
    //  This type definition specifies the functor class for an event handler,
    //  which is a special method used to communicate state changes between a
    //  publisher object and a subscriber object.  By convention, an event
    //  handler is defined in the private section of the subscriber's class,
    //  with the following signature:
    //
    //  void
    //  DNBSubscriber::onEvent( const DNBPublisher::ConstPointer &pPublisher,
    //      const EventSet &events )
    //      DNB_THROW_SPEC_NULL;
    //
    //  The functor is defined using the global function DNBMakeEventFunctor
    //  (defined below).
    //
    typedef DNBFunctor2<const DNBSharedConstPointer<DNBNotificationAgent> &,
        const EventSet &>           EventFunctor;

    void
    addSubscriber( const EventFunctor &functor, 
                   const EventSet &mask, int count = -1 )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEAlreadyRegistered));

    void
    removeSubscriber( const EventFunctor &functor )
        DNB_THROW_SPEC((DNBENotFound));

    void
    setEventMask( const EventFunctor &functor, const EventSet &mask )
        DNB_THROW_SPEC((DNBENotFound));

    void
    getEventMask( const EventFunctor &functor, EventSet &mask ) const
        DNB_THROW_SPEC((DNBENotFound));

    //
    //  Type definitions for notification management.
    //
    typedef DNBFunctor2<const DNBSharedConstPointer<DNBNotificationAgent> &,
        const DNBNotificationBase *>    NotifFunctor;

    void
    addSubscriber( const NotifFunctor &functor, 
                   const DNBClassInfo &classInfo, int count = -1 )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEAlreadyRegistered));

    void
    removeSubscriber( const NotifFunctor &functor,
        const DNBClassInfo &classInfo )
        DNB_THROW_SPEC((DNBENotFound));

    //
    //  The following method is used to dispatch events and notifications to all
    //  subscriber objects before the end of the transaction.
    //
    void
    notifySubscribers( )
        DNB_THROW_SPEC_NULL;

    enum Events
    {
        EventLast = 0
    };

protected:
    //
    //  By defining the following methods in the protected section, this class
    //  may be used only as a base class.
    //
    DNBNotificationAgent( bool createLock = true )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit));

    DNBNotificationAgent( const DNBNotificationAgent &right, CopyMode mode )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit));

    virtual
    ~DNBNotificationAgent( )
        DNB_THROW_SPEC_NULL;

    //
    //  Declare some utility methods for managing events.
    //
    inline  void
    postEvent( EventID id )
        DNB_THROW_SPEC_NULL;

    inline  void
    cancelEvent( EventID id )
        DNB_THROW_SPEC_NULL;

    inline  bool
    testEvent( EventID id ) const
        DNB_THROW_SPEC_NULL;

    inline  void
    cancelEvents( )
        DNB_THROW_SPEC_NULL;

    inline  EventSet
    getEvents( ) const
        DNB_THROW_SPEC_NULL;

    void
    dispatchEvents( )
        DNB_THROW_SPEC_NULL;

    //
    //  Declare some utility methods for managing notifications.
    //
    inline  void
    postNotification( const DNBNotificationBase &notif )
        DNB_THROW_SPEC((scl_bad_alloc));

    inline  void
    cancelNotification( const DNBClassInfo &classInfo )
        DNB_THROW_SPEC_NULL;

    inline  bool
    testNotification( const DNBClassInfo &classInfo ) const
        DNB_THROW_SPEC_NULL;

    inline  void
    cancelNotifications( )
        DNB_THROW_SPEC_NULL;

    void
    dispatchNotifications( )
        DNB_THROW_SPEC_NULL;

    //
    //  Declare some utility methods for managing notification hooks.  A
    //  notification hook is a special method defined in a derived class that
    //  can be used to process existing events/notifications and even post new
    //  events/notifications.  It is called immediately before the dispatch
    //  phase.
    //
    typedef void (DNBNotificationAgent::*NotificationHookFunc)();

    typedef DNBFunctor0         NotificationHook;

    static  NotificationHook *  NotificationHookNil;

    void
    addNotificationHook( const NotificationHook &hook )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEAlreadyRegistered));

    void
    removeNotificationHook( const NotificationHook &hook )
        DNB_THROW_SPEC((DNBENotFound));

    virtual void
    onNotificationTxn( TransactionTrigger trigger )
        DNB_THROW_SPEC_NULL;

private:
    //
    //  Define the database of event subscribers.
    //
    class EventSubEntry
    {
    public:
        EventSubEntry( )
            DNB_THROW_SPEC_NULL;

        EventSubEntry( const EventFunctor &functor,
                       int count = -1 )
            DNB_THROW_SPEC_NULL;

        EventSubEntry( const EventFunctor &functor, 
                       const EventSet &mask, 
                       int count = -1 )
            DNB_THROW_SPEC_NULL;

        EventSubEntry( const EventSubEntry &right )
            DNB_THROW_SPEC_NULL;

        ~EventSubEntry( )
            DNB_THROW_SPEC_NULL;

        void
        operator=( const EventSubEntry &right )
            DNB_THROW_SPEC_NULL;

        bool
        operator==( const EventSubEntry &right ) const
            DNB_THROW_SPEC_NULL;

        void
        setPublisher( DNBNotificationAgent *pPublisher )
            DNB_THROW_SPEC_NULL;

        Handle          hSubscriber_;
        EventFunctor    functor_;
        EventSet        mask_;
        bool            sharesLock_;
        int             count_;
    };

    typedef scl_vector<EventSubEntry, DNB_ALLOCATOR(EventSubEntry) >    EventSubDB;

    typedef EventSubDB::iterator        EventSubIterator;

    typedef EventSubDB::const_iterator  EventSubConstIterator;

    EventSubIterator
    findSubscriber( const EventFunctor &functor )
        DNB_THROW_SPEC((DNBENotFound));

    EventSubConstIterator
    findSubscriber( const EventFunctor &functor ) const
        DNB_THROW_SPEC((DNBENotFound));

    //
    //  Define the database of notification subscribers.
    //
    class NotifSubEntry
    {
    public:
        NotifSubEntry( )
            DNB_THROW_SPEC_NULL;

        NotifSubEntry( const NotifFunctor &functor,
                       int count = -1 )
            DNB_THROW_SPEC_NULL;

        NotifSubEntry( const NotifFunctor &functor,
                       const DNBClassInfo &classInfo, 
                       int count = -1 )
            DNB_THROW_SPEC_NULL;

        NotifSubEntry( const NotifSubEntry &right )
            DNB_THROW_SPEC_NULL;

        ~NotifSubEntry( )
            DNB_THROW_SPEC_NULL;

        void
        operator=( const NotifSubEntry &right )
            DNB_THROW_SPEC_NULL;

        bool
        operator==( const NotifSubEntry &right ) const
            DNB_THROW_SPEC_NULL;

        Handle              hSubscriber_;
        NotifFunctor        functor_;
        const DNBClassInfo *classInfo_;
        int                 count_;
    };

    typedef scl_vector<NotifSubEntry, DNB_ALLOCATOR(NotifSubEntry) >    NotifSubDB;

#if DNB_HAS_RW_STANDARD_LIB
    typedef scl_multimap <const DNBClassInfo *, const DNBNotificationBase *,
        scl_less<const DNBClassInfo *> > NotifDB;
#else
    typedef scl_multimap <const DNBClassInfo *, const DNBNotificationBase *,
        scl_less<const DNBClassInfo *> >    NotifDB;
#endif

    typedef NotifSubDB::iterator        NotifSubIterator;

    typedef NotifSubDB::const_iterator  NotifSubConstIterator;

    typedef NotifDB::iterator           NotifIterator;

    typedef NotifDB::const_iterator     NotifConstIterator;

    NotifSubIterator
    findSubscriber( const NotifFunctor &functor, const DNBClassInfo &classInfo )
        DNB_THROW_SPEC((DNBENotFound));

    NotifSubConstIterator
    findSubscriber( const NotifFunctor &functor,
        const DNBClassInfo &classInfo ) const
        DNB_THROW_SPEC((DNBENotFound));

    //
    //  Define the database of notification hooks.
    //
    typedef scl_vector<NotificationHook, DNB_ALLOCATOR(NotificationHook) >
        NotificationHookDB;

    typedef NotificationHookDB::iterator    NotificationHookIterator;

    void
    notifyPublisher( )
        DNB_THROW_SPEC_NULL;

    EventSubDB          eventSubDB_;    // Database of event subscribers
    EventSet            postedEvents_;  // Set of pending events
    NotificationHookDB  notifHookDB_;   // Set of notification hooks
    NotifSubDB          notifSubDB_;    // Database of notification subscribers
    NotifDB             postedNotifs_;  // Set of pending notifications
};




//
//  This template function creates an event functor for the subscriber object
//  specified by the pointer <pSubscriber> and the event handler specified by
//  the pointer-to-member-function <method>.
//
template <class Publisher, class Subscriber, class CallType>
DNBFunctor2<const DNBNotificationAgent::ConstPointer &,
    const DNBNotificationAgent::EventSet &>
DNBMakeEventFunctor( Subscriber *pSubscriber, void (CallType::*method)(
    const DNBSharedConstPointer<Publisher> &,
    const DNBNotificationAgent::EventSet &) )
    DNB_THROW_SPEC((scl_bad_alloc))
{
    typedef const DNBNotificationAgent::ConstPointer   &P1;
    typedef const DNBNotificationAgent::EventSet       &P2;
    typedef void (CallType::*MemFunc)(P1, P2);

    DNB_PRECONDITION( Subscriber::ClassInfo().isDerivedFrom(
        DNBNotificationAgent::ClassInfo()) );

    DNB_PRECONDITION( Publisher ::ClassInfo().isDerivedFrom(
        DNBNotificationAgent::ClassInfo()) );

    _DNBMemberFunctor2Body<P1, P2, Subscriber, MemFunc>    *body =
        DNB_NEW _DNBMemberFunctor2Body<P1, P2, Subscriber, MemFunc>(
        pSubscriber, DNB_REINTERPRET_CAST(MemFunc, method) );

    return DNBFunctor2<P1, P2>( body );
}


template <class Publisher, class Subscriber, class CallType>
DNBFunctor2<const DNBNotificationAgent::ConstPointer &,
    const DNBNotificationBase *>
DNBMakeNotifFunctor( Subscriber *pSubscriber, void (CallType::*method)(
    const DNBSharedConstPointer<Publisher> &, const DNBNotificationBase *) )
    DNB_THROW_SPEC((scl_bad_alloc))
{
    typedef const DNBNotificationAgent::ConstPointer   &P1;
    typedef const DNBNotificationBase                  *P2;
    typedef void (CallType::*MemFunc)(P1, P2);

    DNB_PRECONDITION( Subscriber::ClassInfo().isDerivedFrom(
        DNBNotificationAgent::ClassInfo()) );

    DNB_PRECONDITION( Publisher ::ClassInfo().isDerivedFrom(
        DNBNotificationAgent::ClassInfo()) );

    _DNBMemberFunctor2Body<P1, P2, Subscriber, MemFunc>    *body =
        DNB_NEW _DNBMemberFunctor2Body<P1, P2, Subscriber, MemFunc>(
        pSubscriber, DNB_REINTERPRET_CAST(MemFunc, method) );

    return DNBFunctor2<P1, P2>( body );
}




//
//  Include the public definition file.
//
#include "DNBNotificationAgent.cc"




#endif  /* _DNBNotificationAgent_H_ */
