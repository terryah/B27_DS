//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview RTL trangara 01:11:15
 */
// --------------------------------------------------------

template <class Iface>
inline
DNBCOMPointer<Iface>::DNBCOMPointer( const Iface *iface )
    DNB_THROW_SPEC_NULL :

    DNBCOMPointerBase   ( iface )
{
    // Nothing
}


template <class Iface>
inline
DNBCOMPointer<Iface>::DNBCOMPointer( const DNBCOMPointerBase &right )
    DNB_THROW_SPEC((DNBEInvalidCast)) :

    DNBCOMPointerBase   ( downcast(right) )
{
    // Nothing
}


template <class Iface>
inline
DNBCOMPointer<Iface>::DNBCOMPointer( const DNBCOMPointer<Iface> &right )
    DNB_THROW_SPEC_NULL :

    DNBCOMPointerBase   ( right )
{
    // Nothing
}


template <class Iface>
inline
DNBCOMPointer<Iface>::~DNBCOMPointer( )
    DNB_THROW_SPEC_NULL
{
    // Nothing
}


template <class Iface>
inline DNBCOMPointer<Iface> &
DNBCOMPointer<Iface>::operator=( const Iface *iface )
    DNB_THROW_SPEC_NULL
{
    DNBCOMPointerBase::operator=( iface );

    return *this;
}


template <class Iface>
inline DNBCOMPointer<Iface> &
DNBCOMPointer<Iface>::operator=( const DNBCOMPointerBase &right )
    DNB_THROW_SPEC((DNBEInvalidCast))
{
    //
    //  Note: The following method checks for self-assignment.
    //
    DNBCOMPointerBase::operator=( downcast(right) );

    return *this;
}


template <class Iface>
inline DNBCOMPointer<Iface> &
DNBCOMPointer<Iface>::operator=( const DNBCOMPointer<Iface> &right )
    DNB_THROW_SPEC_NULL
{
    //
    //  Note: The following method checks for self-assignment.
    //
    DNBCOMPointerBase::operator=( right );

    return *this;
}


template <class Iface>
inline void
DNBCOMPointer<Iface>::clear( )
    DNB_THROW_SPEC_NULL
{
    detach( );
}


template <class Iface>
inline Iface &
DNBCOMPointer<Iface>::operator*( ) const
    DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION( isBound() );
    return *dynamic_cast<Iface *>( getReferent() );
}


template <class Iface>
inline Iface *
DNBCOMPointer<Iface>::operator->( ) const
    DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION( isBound() );
    return  dynamic_cast<Iface *>( getReferent() );
}


template <class Iface>
inline Iface *
DNBCOMPointer<Iface>::_getInterface( ) const
    DNB_THROW_SPEC_NULL
{
    return dynamic_cast<Iface *>( getReferent() );
}


template <class Iface>
inline Iface *
DNBCOMPointer<Iface>::getInterface( ) const
    DNB_THROW_SPEC_NULL
{
    return dynamic_cast<Iface *>( getReferent() );
}


template <class Iface>
Iface *
DNBCOMPointer<Iface>::downcast( const DNBCOMPointerBase &right )
    DNB_THROW_SPEC((DNBEInvalidCast))
{
    IUnknown   *source = right._getReferent( );
    if ( source == NULL )
        return NULL;

    Iface      *target = dynamic_cast<Iface *>( source );
    if ( target == NULL )
    {
        DNBEInvalidCast     error( DNB_FORMAT("The source and target pointers \
have incompatible referent types") );
        throw error;
    }

    return target;
}
