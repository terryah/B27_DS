// Declarations communes tous modules FreeFormOperators.
// COPYRIGHT DASSAULT SYSTEMES 2007
/** @CAA2Required */
//**********************************************************************
//* DON T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS *
//* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPEAR AT ANY TIME *
//**********************************************************************
// 21/02/07 NLD Creation
// 29/03/07 NLD Remplacement de FrFTrackNSTOL par CATTrackNSTOL
// 19/04/11 NLD Ajout         CATIACGMLevel  
//#include <FrFTrackNSTOL.h>
#include <CATTrackNSTOL.h>
#include <CATIACGMLevel.h>

