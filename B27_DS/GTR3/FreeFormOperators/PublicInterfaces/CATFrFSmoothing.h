#include "CATIACGMLevel.h"
#ifdef CATIACGMR420CAA
#ifdef _STATIC_SOURCE
#define	ExportedByCATFrFSmoothing
#elif defined __CATFrFSmoothing
#define ExportedByCATFrFSmoothing DSYExport
#else
#define ExportedByCATFrFSmoothing DSYImport
#endif
#include "DSYExport.h"
#else
#ifdef _STATIC_SOURCE
#define	ExportedByCATFrFSmoothing
#elif defined _WINDOWS_SOURCE
#ifdef	__CATFrFSmoothing

// COPYRIGHT DASSAULT SYSTEMES PROVENCE 2010
/** @CAA2Required */

#define	ExportedByCATFrFSmoothing	__declspec(dllexport)
#else
#define	ExportedByCATFrFSmoothing  __declspec(dllimport)
#endif
#else
#define	ExportedByCATFrFSmoothing
#endif
#endif
#include <FrFCommonDec.h>
