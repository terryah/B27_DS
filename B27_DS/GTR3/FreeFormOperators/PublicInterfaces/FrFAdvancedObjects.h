#include "CATIACGMLevel.h"
#ifdef CATIACGMR420CAA
#ifdef _STATIC_SOURCE
#define	ExportedByFrFAdvancedObjects
#elif defined __FrFAdvancedObjects


// COPYRIGHT DASSAULT SYSTEMES 1999

/** @CAA2Required */

//**********************************************************************

//* DON T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS *

//* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPEAR AT ANY TIME *

//**********************************************************************
#define ExportedByFrFAdvancedObjects DSYExport
#else
#define ExportedByFrFAdvancedObjects DSYImport
#endif
#include "DSYExport.h"
#else
#ifdef _STATIC_SOURCE
#define	ExportedByFrFAdvancedObjects
#elif defined _WINDOWS_SOURCE
// COPYRIGHT DASSAULT SYSTEMES 1999
/** @CAA2Required */
//**********************************************************************
//* DON T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS *
//* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPEAR AT ANY TIME *
//**********************************************************************

#ifdef	__FrFAdvancedObjects
#define	ExportedByFrFAdvancedObjects	__declspec(dllexport)
#else
#define	ExportedByFrFAdvancedObjects	__declspec(dllimport)
#endif
#else
#define	ExportedByFrFAdvancedObjects
#endif
#endif
#include <FrFCommonDec.h>
