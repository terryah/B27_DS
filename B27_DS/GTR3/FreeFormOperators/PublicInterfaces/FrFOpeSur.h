#include "CATIACGMLevel.h"
#ifdef CATIACGMR420CAA
#ifdef _STATIC_SOURCE
#define	ExportedByFrFOpeSur
#elif defined __FrFOpeSur
#define ExportedByFrFOpeSur DSYExport
#else
#define ExportedByFrFOpeSur DSYImport
#endif
#include "DSYExport.h"
#else
#ifdef _STATIC_SOURCE
#define	ExportedByFrFOpeSur
#elif defined _WINDOWS_SOURCE
#ifdef	__FrFOpeSur

// COPYRIGHT DASSAULT SYSTEMES 1999
/** @CAA2Required */

#define	ExportedByFrFOpeSur	__declspec(dllexport)
#else
#define	ExportedByFrFOpeSur	__declspec(dllimport)
#endif
#else
#define	ExportedByFrFOpeSur
#endif
#endif
#include <FrFCommonDec.h>
