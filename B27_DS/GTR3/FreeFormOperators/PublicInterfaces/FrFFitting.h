#include "CATIACGMLevel.h"
#ifdef CATIACGMR420CAA
#ifdef _STATIC_SOURCE
#define	ExportedByFrFFitting
#elif defined __FrFFitting
#define ExportedByFrFFitting DSYExport
#else
#define ExportedByFrFFitting DSYImport
#endif
#include "DSYExport.h"
#else
#ifdef _STATIC_SOURCE
#define	ExportedByFrFFitting
#elif defined _WINDOWS_SOURCE
#ifdef	__FrFFitting

// COPYRIGHT DASSAULT SYSTEMES 1999
/** @CAA2Required */

#define	ExportedByFrFFitting	__declspec(dllexport)
#else
#define	ExportedByFrFFitting	__declspec(dllimport)
#endif
#else
#define	ExportedByFrFFitting
#endif
#endif
#include <FrFCommonDec.h>
