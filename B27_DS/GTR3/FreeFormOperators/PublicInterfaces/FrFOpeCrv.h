#include "CATIACGMLevel.h"
#ifdef CATIACGMR420CAA
#ifdef _STATIC_SOURCE
#define	ExportedByFrFOpeCrv
#elif defined __FrFOpeCrv
#define ExportedByFrFOpeCrv DSYExport
#else
#define ExportedByFrFOpeCrv DSYImport
#endif
#include "DSYExport.h"
#else
#ifdef _STATIC_SOURCE
#define	ExportedByFrFOpeCrv
#elif defined _WINDOWS_SOURCE
#ifdef	__FrFOpeCrv

// COPYRIGHT DASSAULT SYSTEMES 1999
/** @CAA2Required */

#define	ExportedByFrFOpeCrv	__declspec(dllexport)
#else
#define	ExportedByFrFOpeCrv	__declspec(dllimport)
#endif
#else
#define	ExportedByFrFOpeCrv
#endif
#endif
#include <FrFCommonDec.h>
