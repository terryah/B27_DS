	// COPYRIGHT Dassault Systemes 2003
//===================================================================
//
// CATIColMergeContextTable.h
// Define the CATIColMergeContextTable interface
//
//===================================================================
//
// Usage notes:
//   New interface: describe its use here
//
//===================================================================
//
//  Jul 2003  Creation: Code generated by the CAA wizard  ank
//===================================================================
#ifndef CATIColMergeContextTable_H
#define CATIColMergeContextTable_H
/**
* @CAA2Level L1
* @CAA2Usage U3
*/

#include "CATInstantCollabDesignItf.h"
#include "CATBaseUnknown.h"
#include "CATListOfCATUnicodeString.h"

class CATUnicodeString;

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByCATInstantCollabDesignItf IID IID_CATIColMergeContextTable;
#else
extern "C" const IID IID_CATIColMergeContextTable ;
#endif

/**
*	NOT USED : default is CATCol_OPTIONAL
*/
typedef enum
{
   CATCol_OPTIONAL 
  ,CATCol_MANDATORY
  ,CATCol_ASK_MUSER
} 
CATIColMergeContextFlag;

/**
*	The collaborative semantic :
*	<li>CATCol_IGNORED_AGGREGATOR : agregator feature is not embeded inside the briefcase. It must be created
*	on the merge side</li>
*	<li>CATCol_PROTECTED_AGGREGATOR : agregator is embeded but always discarded (i.e not replaced)</lI>
*	<li>CATCol_FORCED_AGGREGATOR : agregator is embeded and always replaced (i.e replaced)
*	<u>Example</u><br>
*	When an internal sketch's feature is selected, the entire sketch is embeded thanks to this role
*	</li>
*	<li>CATCol_INTERNAL_AGGREGATED : aggregated feature which is mandatory for the feature</li>	
*	<li>CATCol_AGGREGATED : aggregated feature which is not mandatory for the feature
*		<u>Example</u><br>
*		A geomtrical set defines all the GSD features as agregated. It means that the geometrical set can be shared<br>
*		without its children.
*	</li>
*	<li>CATCol_PROTECTED_DEPENDANCY : the pointed features is embeded is not replaced if it exists in the destination part<br>
*		<u>Example</u><br>
*		The input surface of a GSD offset is always protected. This semantic allows advanced concurrent design scenarios </br>
*	</li>	
*	<li>CATCol_FORCED_DEPENDANCY : the pointed features is embeded is ALWAYS replaced if it exists in the destination part<br>
*		<u>Example</u><br>
*		The input profile of a solid feature is always replaced.
*	</li>
*/
typedef enum
{
    CATCol_IGNORED_AGGREGATOR
   ,CATCol_PROTECTED_AGGREGATOR
   ,CATCol_FORCED_AGGREGATOR
   ,CATCol_INTERNAL_AGGREGATED
   ,CATCol_AGGREGATED
   ,CATCol_PROTECTED_DEPENDANCY
   ,CATCol_FORCED_DEPENDANCY
   ,CATCol_IGNORED
   ,CATCol_UNDEF
}
CATIColMergeContextRole;

/**
*	This interface defines the semantics used to represent all the linked feature of a collaborative feature.<br>
*	The table contains a set of rows, each of them describing ONE link :<br>
*	<u>Example : context table for a pad aggregated under an hybrid body</u><br><br>
*	----------------------------------------------------------------------------<br>
*	ROLE NAME | Pointed Object | ContextRole                  | ContextFlag		<br>
*	----------------------------------------------------------------------------<br>
*	Myfather  | HybridBody	   | CATCol_PROTECTED_AGGREGATOR  | CATCol_OPTIONAL<br>
*	Myprofile | sketch   	   | CATCol_FORCED_DEPENDENCY	  | CATCol_OPTIONAL<br>
*	<br>
*	<li>RoleName : string chosen by the developer to identify a link</li>
*	<li>Pointed object : the feature </li>
*	<li>Context role : the collaborative semantic</li>
*	<li>ContextFlag : not used .Default is CATCol_OPTIONAL</li>
*	<br>
*	The table is built during the share step by a low level adapter which guarantees the feature model integrity.<br>
*	The goal is to have a closed set of features.<br>
*	The folowing methods provide access to the rows and the columns of the table and the possibility to add string property<br>
*	inside the table. These properties are streamed whith the table.
*/
class ExportedByCATInstantCollabDesignItf CATIColMergeContextTable: public CATBaseUnknown
{
	CATDeclareInterface;
	
public:
	
	/**
	*	Add a new entry inside the table.
	*	@return
	*		The index inside the table of the new entry
	*/
	virtual HRESULT CreateLink    (CATIColMergeContextRole iRole,CATUnicodeString & iRoleName,
									CATBaseUnknown * iValue,CATIColMergeContextFlag iFlag,
									int & oIndex)=0;

	/**
	*	Remove an entry from the table
	*/
	virtual HRESULT RemoveLink    (int iIndex)=0;

	/**
	*	Get the number of entries inside the table
	*/
	virtual HRESULT GetRoleNumber (int &oNumber)=0;

	/**
	*	Valuate the ContextRole column of the row iIndex
	*/
	virtual HRESULT SetContextRole(int iIndex, CATIColMergeContextRole iCtxRole)=0;
	/**
	*	Valuate the rolename column of the row iIndex
	*/
	virtual HRESULT SetRoleName   (int iIndex, CATUnicodeString & iName)=0;
	/**
	*	Valuate the object column of the row iIndex. If an object was already set, it is released
	*/
	virtual HRESULT SetObject     (int iIndex, CATBaseUnknown * iValue)=0;
	/**
	*	NOT USED :Valuate the context flag column of the row iIndex
	*/
	virtual HRESULT SetContextFlag(int iIndex, CATIColMergeContextFlag iFlag)=0;
	
	/**
	*	Get the entire entry iIndex from the table
	*/
	virtual HRESULT GetLink       (int iIndex, CATIColMergeContextRole &oRole,CATUnicodeString &oRoleName,CATBaseUnknown *& oValue,CATIColMergeContextFlag &oFlag)=0;	
	/**
	*	Get the context role column value of the row iIndex
	*/
	virtual HRESULT GetContextRole(int iIndex, CATIColMergeContextRole & oCtxRole)=0;
	/**
	*	Get the role name column value of the row iIndex
	*/
	virtual HRESULT GetRoleName   (int iIndex, CATUnicodeString & oName)=0;
	/**
	*	Get the object column value of the row iIndex
	*
	* @param iIndex [in]
	*   The parameter role...
	* @param oValue [out, CATBaseUnknown#Release]
	*   The parameter role...
	*
	* @return
	*   <code>S_OK</code> if everything ran ok, otherwise...
	*
	*/
	virtual HRESULT GetObject     (int iIndex, CATBaseUnknown *& oValue)=0;
	/**
	*	NOT USED : Get the ContextFlag column value of the row iIndex
	*/
	virtual HRESULT GetContextFlag(int iIndex, CATIColMergeContextFlag & oFlag)=0;

	/**
	*	Find an entry inside the table from an input object
	*	@param iObject
	*		feature to find inside the table
	*	@return
	*		the index inside the table of the link which contains iObject or -1 if not found
	*/
	virtual int     LocateObject  (const CATBaseUnknown * iObject)=0;

	/**
	*	Add a string property (key,value) inside the table
	*/
	virtual HRESULT CreateProperty(const CATUnicodeString & ikey,const CATUnicodeString & value)=0;
	/**
	*	List all the properties key
	*/
	virtual HRESULT ListPropertyKeys(CATListOfCATUnicodeString & list) = 0;
	/**
	*	Return the value of a property from a key
	*/
	virtual HRESULT GetProperty   (const CATUnicodeString & ikey, CATUnicodeString & ovalue)=0;
	/**
	*	Remove a property from a key
	*/
	virtual HRESULT RemoveProperty(const CATUnicodeString & ikey)=0;
};
CATDeclareHandler(CATIColMergeContextTable, CATBaseUnknown);

//------------------------------------------------------------------

#endif
