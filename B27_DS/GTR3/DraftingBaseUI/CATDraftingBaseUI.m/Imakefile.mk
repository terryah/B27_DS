BUILT_OBJECT_TYPE = SHARED LIBRARY

LINK_WITH= JS0GROUP \
           JS0FM \
		   DI0PANV2 \
		   CATViz \
		   CATVisualization \
		   CATMathematics \
		   CATApplicationFrame \
		   CATIAApplicationFrame \
		   CATObjectSpecsModeler \
		   DraftingItfCPP \
		   CATDraftingBaseInfra \
		   CATDraftingBaseInfraUI \
		   CATAnnotationModeler \
		   CATDraftingAnnotationModeler \
		   CATInteractiveInterfaces \
		   CATMecModInterfaces \
		   CATProductStructure1 \
		   CATSketcherInterfaces \
		   CATObjectModelerBase \
#
INCLUDED_MODULES = CATDuiServices \
                   CATDbuCtxMenu \
