#
# SHARED LIBRARY
#
BUILT_OBJECT_TYPE=SHARED LIBRARY

INCLUDED_MODULES=SkinWire VarSweepDriver KinUtilities WBxUtilities 

LINK_WITH_V6=
#if defined(CATIAR201) 
LINK_WITH_V6=CATGMOperatorsInterfaces
#endif
 
LINK_WITH_V5R23_V6R214=
#if defined(CATIAV5R23) || defined(CATIAR214)
LINK_WITH_V5R23_V6R214=CATWhiteBoxSolver
#endif

LINK_WITH_V5R26_V6R418=
#if defined(CATIAV5R26) || defined(CATIAR418)
LINK_WITH_V5R26_V6R418=CATWBx
#endif

LINK_WITH= \
  $(LINK_WITH_V6) \
  $(LINK_WITH_V5R23_V6R214) \
  $(LINK_WITH_V5R26_V6R418) \
  JS0GROUP CATSysTS \
  CATCDS \
  CATCDSUtilities \
  CATMathematics \
  CATMathStream \
  CATAdvancedMathematics \
  CATTopologicalOperators \
  CATCGMGeoMath \
  CATGeometricObjects \
  CATGeometricOperators \
  CATFreeFormOperators  \
  CATGMModelInterfaces
 
LOCAL_CCFLAGS_ASSERT=-DNOT_CDS_ASSERT $(MKMK_DEBUG:+"-DCDS_ASSERT")

#if os Windows
LOCAL_CCFLAGS=-D_HAS_EXCEPTIONS=0 $(LOCAL_CCFLAGS_ASSERT)
#elif os Linux
LOCAL_CCFLAGS=-std=c++0x -DCPP11_AVALAIBLE $(LOCAL_CCFLAGS_ASSERT)
#else
LOCAL_CCFLAGS=$(LOCAL_CCFLAGS_ASSERT)
#endif
