// COPYRIGHT Dassault Systemes 2007
//===================================================================
//
// CATIExchangeUserProperty.h
// Define the CATIExchangeUserProperty interface
//
//===================================================================
//
// Usage notes:
//   New interface: describe its use here
//
//===================================================================
//
//  Apr 2007  Creation: Code generated by the CAA wizard  MAX
// 2013-11-05: DFB: This API is deprecated in R24 & R216; use CATIExchangeUserProperties
//===================================================================
/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

#ifndef CATIExchangeUserProperty_H
#define CATIExchangeUserProperty_H

#include "DataExchangeKernelItfCPP.h"
#include "CATBaseUnknown.h"
#include "CATListOfCATUnicodeString.h"

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByDataExchangeKernelItfCPP IID IID_CATIExchangeUserProperty;
#else
extern "C" const IID IID_CATIExchangeUserProperty ;
#endif

//------------------------------------------------------------------

/* @deprecated R24 CATIExchangeUserProperties */

/**
 * Interface to exchange user properties, just like User Defined Attributes.
 * <b>Role</b>: This interface is dedicated to exchange user defined properties.
 * <br>Available from CATIA V5R18.
 */ 
class ExportedByDataExchangeKernelItfCPP CATIExchangeUserProperty: public CATBaseUnknown
{
  CATDeclareInterface;

  public:

	/*
	* Retrieves 3 lists of same length. At each index you can get a different attribute :
	* @param oAttributesNames
	*		The list of attributes' names.
	* @param oAttributesValues
	*		The list of attributes' values.
	* @param oAttributesTypes
	*		The list of attributes' types.
	*		Types can take the following values : string, real, integer, boolean
	*/
	virtual HRESULT GetUserAttributes(CATListOfCATUnicodeString& oAttributesNames,
									CATListOfCATUnicodeString& oAttributesValues,
									CATListOfCATUnicodeString& oAttributesTypes) const = 0;

  // No constructors or destructors on this pure virtual base class
  // --------------------------------------------------------------
};
CATDeclareHandler(CATIExchangeUserProperty,CATBaseUnknown);

//------------------------------------------------------------------

#endif
