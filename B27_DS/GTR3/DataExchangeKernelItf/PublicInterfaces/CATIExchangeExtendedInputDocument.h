/* -*-c++-*- */
// COPYRIGHT Dassault Systemes Provence 2005-2010
//===================================================================
//
// CATIExchangeExtendedInputDocument.h
// Define the CATIExchangeExtendedInputDocument interface
//
//===================================================================
//
// Usage notes:
//   Interface to manage advanced function on input documents.
//
//===================================================================
//
//  Sep 2005  Creation: Code generated by the CAA wizard  dfb
//  17/04/2007; C. Guinamard : Gestion des "Invalid Integrity"
//                             Ajout de la valeur STATUS_INVALID_INTEGRITY
//                             a l'enum XCADVersionStatus.
//  22/05/2008; C. Guinamard : Gestion des problemes "Authoring"
//                             Ajout de la valeur STATUS_STATUS_AUTHORING
//                             a l'enum XCADVersionStatus.
//  19/03/2010: JRX/DFB: Add new method for Input Doc Properties - since V5R20SP02 & V6R2011 
//  20/09/2012: DFB: CAA L0 XCAD Interfaces since V5R23
//===================================================================
#ifndef CATIExchangeExtendedInputDocument_H
#define CATIExchangeExtendedInputDocument_H

/**
 * @CAA2Level L0
 * @CAA2Usage U0 
 */

#include "DataExchangeKernelItfCPP.h"
#include "CATBaseUnknown.h"
#include "CATUnicodeString.h"

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByDataExchangeKernelItfCPP IID IID_CATIExchangeExtendedInputDocument;
#else
extern "C" const IID IID_CATIExchangeExtendedInputDocument ;
#endif

//------------------------------------------------------------------

/**
 * Interface to manage advanced function on input documents.
 * <b>Role</b>: This interface describes the XCAD input document.
 * This interface gives access to the document version status.
 * <br>Available in CATIA V5R20SP02.
 * @deprecated V6R2011 CATIExchangePLMObject
 * @see CATIExchangeInputDocument
 */

// HERE ARE THE DIFFERENT RETURN CODES AVAILABLE FOR METHOD GetStatusVersion
typedef enum {
    STATUS_VERSION_UNKNOWN = 1                  // The version of the input document is unknown
    , STATUS_VERSION_SUPPORTED                  // The version of the input document is fully supported
    , STATUS_VERSION_PARTIALLY_SUPPORTED        // The version of the input document is partially supported (document too recent)
    , STATUS_VERSION_NOT_SUPPORTED              // The version of the input document is not supported (document too recent)
    , STATUS_INVALID_INTEGRITY                  // The 3DXML document has Invalid Integrity
    , STATUS_AUTHORING                          // The document has "Authoring" problem (in fact the 3DXML doc. has no public view)
} XCADVersionStatus;

class ExportedByDataExchangeKernelItfCPP CATIExchangeExtendedInputDocument: public CATBaseUnknown
{
    CATDeclareInterface;

public:

    /**
     * Get a status on a XCAD document version.
     * @param oVersionStatus
     *                          The status version of the input document as defined in the enum XCADVersionStatus.
     */
    virtual HRESULT GetVersionStatus(XCADVersionStatus& oVersionStatus) = 0;

    /**
    * Get the native system software of the document which created the native format file used to generate this exchange file
    * @param oNativeSystemID
    *                          The name of the system and the product ID, version number of the native system software of this document.
    * @param oBuildDate
    *                          The release date of the native system software of this document.

    * <br>(for example : CATIA document since R10: &lt Version &gt 5/ &lt Version &gt &lt Release &gt 10/ &lt Release&gt &lt ServicePack&gt 1/&lt ServicePack &gt &lt BuildDate &gt mm-dd-yyyy.hh.mm/ &lt BuildDate &gt).
    */
    virtual HRESULT GetNativeSystemID(CATUnicodeString& oNativeSystemID, CATUnicodeString& oBuildDate) = 0;

    /**
    * Get the Author of the document.
    * @param oDocumentAuthor
    *                          The name of the person who has created this document.
    */
    virtual HRESULT GetDocumentAuthor(CATUnicodeString& oDocumentAuthor) = 0;

    /**
    * Get the Organization of the document.
    * @param oDocumentOrganization
    *                          The name of the organization or group with whom the author is associated.
    */
    virtual HRESULT GetDocumentOrganization(CATUnicodeString& oDocumentOrganization) = 0;

    /**
    * Get the Authorization of the document.
    * @param oDocumentAuthorization
    *                          The authorization or restriction specifications related to this document.
    */
    virtual HRESULT GetDocumentAuthorization(CATUnicodeString& oDocumentAuthorization) = 0;

    /**
    * Get the Status of the document.
    * @param oDocumentStatus
    *                          Lifecycle status of this document: draft, in work, published, obsolete, ...
    */
    virtual HRESULT GetDocumentStatus(CATUnicodeString& oDocumentStatus) = 0;
	
	/**
    * Get the Revision number or name of the document.
    * @param oDocumentStatus
    *                          Lifecycle revision name or number of this document (or date).
    */
    virtual HRESULT GetDocumentRevision(CATUnicodeString& oDocumentRevision) = 0;

    // No constructors or destructors on this pure virtual base class
    // --------------------------------------------------------------
};
CATDeclareHandler(CATIExchangeExtendedInputDocument,CATBaseUnknown);
//------------------------------------------------------------------

#endif
