// COPYRIGHT DASSAULT SYSTEMES 2003
/** @CAA2Required */
/*---------------------------------------------------------------------*/
/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */
/*---------------------------------------------------------------------*/
#include "CATIACGMLevel.h"
#ifndef V5ToV4Tools_h
#define V5ToV4Tools_h

#ifdef CATIACGMR420CAA
#ifdef __V5ToV4Tools

//#ifdef __YP0TOPO
#define ExportedByV5ToV4Tools DSYExport
#else
#define ExportedByV5ToV4Tools DSYImport
#endif
#include "DSYExport.h"
#else
#ifdef _WINDOWS_SOURCE
//#ifdef __YP0TOPO
#ifdef __V5ToV4Tools
#define ExportedByV5ToV4Tools __declspec(dllexport)
#else
#define ExportedByV5ToV4Tools __declspec(dllimport)
#endif
#else
#define ExportedByV5ToV4Tools
#endif
#endif

#ifndef NULL
#define NULL 0
#endif
#ifndef FALSE
#define FALSE 0
#endif
#ifndef TRUE
#define TRUE 1
#endif
#endif
