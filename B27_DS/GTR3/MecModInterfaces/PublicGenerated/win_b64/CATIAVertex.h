/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAVertex_h
#define CATIAVertex_h

#ifndef ExportedByMecModPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __MecModPubIDL
#define ExportedByMecModPubIDL __declspec(dllexport)
#else
#define ExportedByMecModPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByMecModPubIDL
#endif
#endif

#include "CATIABoundary.h"

extern ExportedByMecModPubIDL IID IID_CATIAVertex;

class ExportedByMecModPubIDL CATIAVertex : public CATIABoundary
{
    CATDeclareInterface;

public:

};

CATDeclareHandler(CATIAVertex, CATIABoundary);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIABase.h"
#include "CATIAReference.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
