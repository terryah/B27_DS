/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAInstanceFactory_h
#define CATIAInstanceFactory_h

#ifndef ExportedByMecModPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __MecModPubIDL
#define ExportedByMecModPubIDL __declspec(dllexport)
#else
#define ExportedByMecModPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByMecModPubIDL
#endif
#endif

#include "CATBSTR.h"
#include "CATIAFactory.h"

class CATBaseDispatch;
class CATIABase;

extern ExportedByMecModPubIDL IID IID_CATIAInstanceFactory;

class ExportedByMecModPubIDL CATIAInstanceFactory : public CATIAFactory
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall AddInstance(CATIABase * iReference, CATIABase *& oInstance)=0;

    virtual HRESULT __stdcall BeginInstanceFactory(const CATBSTR & iNameOfReference, const CATBSTR & iNameOfDocument)=0;

    virtual HRESULT __stdcall put_InstantiationMode(const CATBSTR & iInstantiationModeBSTR)=0;

    virtual HRESULT __stdcall BeginInstantiate()=0;

    virtual HRESULT __stdcall PutInputData(const CATBSTR & iName, CATBaseDispatch * iInput)=0;

    virtual HRESULT __stdcall GetParameter(const CATBSTR & iName, CATIABase *& oParameter)=0;

    virtual HRESULT __stdcall Instantiate(CATIABase *& oInstance)=0;

    virtual HRESULT __stdcall EndInstantiate()=0;

    virtual HRESULT __stdcall EndInstanceFactory()=0;


};

CATDeclareHandler(CATIAInstanceFactory, CATIAFactory);

#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIABase.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
