/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAOrderedGeometricalSet_h
#define CATIAOrderedGeometricalSet_h

#ifndef ExportedByMecModPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __MecModPubIDL
#define ExportedByMecModPubIDL __declspec(dllexport)
#else
#define ExportedByMecModPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByMecModPubIDL
#endif
#endif

#include "CATIABase.h"

class CATIABodies;
class CATIAGeometricElements;
class CATIAHybridBodies;
class CATIAHybridShape;
class CATIAHybridShapes;
class CATIAOrderedGeometricalSets;
class CATIASketches;

extern ExportedByMecModPubIDL IID IID_CATIAOrderedGeometricalSet;

class ExportedByMecModPubIDL CATIAOrderedGeometricalSet : public CATIABase
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_HybridShapes(CATIAHybridShapes *& oHybridShapes)=0;

    virtual HRESULT __stdcall get_Bodies(CATIABodies *& oBodies)=0;

    virtual HRESULT __stdcall get_OrderedGeometricalSets(CATIAOrderedGeometricalSets *& oOrderedGeometricalSets)=0;

    virtual HRESULT __stdcall get_OrderedSketches(CATIASketches *& oSketches)=0;

    virtual HRESULT __stdcall InsertHybridShape(CATIAHybridShape * iHybridShape)=0;


};

CATDeclareHandler(CATIAOrderedGeometricalSet, CATIABase);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
