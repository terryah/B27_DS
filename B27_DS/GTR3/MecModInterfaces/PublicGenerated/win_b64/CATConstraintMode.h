/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATConstraintMode_h
#define CATConstraintMode_h

enum CatConstraintMode {
        catCstModeDrivingDimension,
        catCstModeDrivenDimension
};

#endif
