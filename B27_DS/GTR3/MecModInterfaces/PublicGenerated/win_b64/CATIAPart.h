/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAPart_h
#define CATIAPart_h

#include "CATCORBABoolean.h"
#include "CAT_VARIANT_BOOL.h"

#ifndef ExportedByMecModPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __MecModPubIDL
#define ExportedByMecModPubIDL __declspec(dllexport)
#else
#define ExportedByMecModPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByMecModPubIDL
#endif
#endif

#include "CATBSTR.h"
#include "CATIABase.h"

class CATIAAxisSystems;
class CATIABodies;
class CATIABody;
class CATIACollection;
class CATIAConstraints;
class CATIAFactory;
class CATIAGeometricElement;
class CATIAGeometricElements;
class CATIAHybridBodies;
class CATIAOrderedGeometricalSets;
class CATIAOriginElements;
class CATIAParameters;
class CATIAReference;
class CATIARelations;

extern ExportedByMecModPubIDL IID IID_CATIAPart;

class ExportedByMecModPubIDL CATIAPart : public CATIABase
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_MainBody(CATIABody *& oMainBody)=0;

    virtual HRESULT __stdcall put_MainBody(CATIABody * iNewMainBody)=0;

    virtual HRESULT __stdcall get_InWorkObject(CATIABase *& oInWorkObject)=0;

    virtual HRESULT __stdcall put_InWorkObject(CATIABase * iInWorkObject)=0;

    virtual HRESULT __stdcall get_Bodies(CATIABodies *& oBodies)=0;

    virtual HRESULT __stdcall get_HybridBodies(CATIAHybridBodies *& oHybridBodies)=0;

    virtual HRESULT __stdcall get_ShapeFactory(CATIAFactory *& oShapeFactory)=0;

    virtual HRESULT __stdcall get_HybridShapeFactory(CATIAFactory *& oHybridShapeFactory)=0;

    virtual HRESULT __stdcall get_SheetMetalFactory(CATIAFactory *& oSheetMetalFactory)=0;

    virtual HRESULT __stdcall get_SheetMetalParameters(CATIABase *& oSheetMetalParameters)=0;

    virtual HRESULT __stdcall get_GeometricElements(CATIAGeometricElements *& oGeometricElements)=0;

    virtual HRESULT __stdcall get_Constraints(CATIAConstraints *& oConstraints)=0;

    virtual HRESULT __stdcall get_Relations(CATIARelations *& oRelations)=0;

    virtual HRESULT __stdcall get_Parameters(CATIAParameters *& oParameters)=0;

    virtual HRESULT __stdcall get_AnnotationSets(CATIACollection *& oAnnotationSets)=0;

    virtual HRESULT __stdcall get_UserSurfaces(CATIACollection *& oUserSurfaces)=0;

    virtual HRESULT __stdcall get_AxisSystems(CATIAAxisSystems *& oAxisSystems)=0;

    virtual HRESULT __stdcall get_OriginElements(CATIAOriginElements *& oOrigin)=0;

    virtual HRESULT __stdcall get_Density(double & oDensity)=0;

    virtual HRESULT __stdcall CreateReferenceFromName(const CATBSTR & iLabel, CATIAReference *& oRef)=0;

    virtual HRESULT __stdcall CreateReferenceFromGeometry(CATIABase * iObject, CATIAReference *& oRef)=0;

    virtual HRESULT __stdcall CreateReferenceFromObject(CATIABase * iObject, CATIAReference *& oRef)=0;

    virtual HRESULT __stdcall FindObjectByName(const CATBSTR & iObjName, CATIABase *& oObject)=0;

    virtual HRESULT __stdcall Update()=0;

    virtual HRESULT __stdcall UpdateObject(CATIABase * iObject)=0;

    virtual HRESULT __stdcall IsUpToDate(CATIABase * iObject, CAT_VARIANT_BOOL & oValue)=0;

    virtual HRESULT __stdcall Inactivate(CATIABase * iObject)=0;

    virtual HRESULT __stdcall Activate(CATIABase * iObject)=0;

    virtual HRESULT __stdcall IsInactive(CATIABase * iObject, CAT_VARIANT_BOOL & oValue)=0;

    virtual HRESULT __stdcall CreateReferenceFromBRepName(const CATBSTR & iLabel, CATIABase * iObjectContext, CATIAReference *& oRef)=0;

    virtual HRESULT __stdcall GetCustomerFactory(const CATBSTR & iFactoryIID, CATIAFactory *& oCustomerFactory)=0;

    virtual HRESULT __stdcall get_OrderedGeometricalSets(CATIAOrderedGeometricalSets *& oOrderedGeometricalSets)=0;


};

CATDeclareHandler(CATIAPart, CATIABase);

#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
