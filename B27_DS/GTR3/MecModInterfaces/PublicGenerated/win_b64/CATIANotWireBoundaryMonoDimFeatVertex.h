/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIANotWireBoundaryMonoDimFeatVertex_h
#define CATIANotWireBoundaryMonoDimFeatVertex_h

#ifndef ExportedByMecModPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __MecModPubIDL
#define ExportedByMecModPubIDL __declspec(dllexport)
#else
#define ExportedByMecModPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByMecModPubIDL
#endif
#endif

#include "CATIAVertex.h"

extern ExportedByMecModPubIDL IID IID_CATIANotWireBoundaryMonoDimFeatVertex;

class ExportedByMecModPubIDL CATIANotWireBoundaryMonoDimFeatVertex : public CATIAVertex
{
    CATDeclareInterface;

public:

};

CATDeclareHandler(CATIANotWireBoundaryMonoDimFeatVertex, CATIAVertex);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIABase.h"
#include "CATIABoundary.h"
#include "CATIAReference.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
