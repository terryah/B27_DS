/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIATriDimFeatEdge_h
#define CATIATriDimFeatEdge_h

#ifndef ExportedByMecModPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __MecModPubIDL
#define ExportedByMecModPubIDL __declspec(dllexport)
#else
#define ExportedByMecModPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByMecModPubIDL
#endif
#endif

#include "CATIAEdge.h"

extern ExportedByMecModPubIDL IID IID_CATIATriDimFeatEdge;

class ExportedByMecModPubIDL CATIATriDimFeatEdge : public CATIAEdge
{
    CATDeclareInterface;

public:

};

CATDeclareHandler(CATIATriDimFeatEdge, CATIAEdge);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIABase.h"
#include "CATIABoundary.h"
#include "CATIAReference.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
