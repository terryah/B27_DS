/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAShapeInstance_h
#define CATIAShapeInstance_h

#ifndef ExportedByMecModPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __MecModPubIDL
#define ExportedByMecModPubIDL __declspec(dllexport)
#else
#define ExportedByMecModPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByMecModPubIDL
#endif
#endif

#include "CATBSTR.h"
#include "CATIAShape.h"

class CATBaseDispatch;
class CATIABase;

extern ExportedByMecModPubIDL IID IID_CATIAShapeInstance;

class ExportedByMecModPubIDL CATIAShapeInstance : public CATIAShape
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall GetInput(const CATBSTR & iName, CATIABase *& oInput)=0;

    virtual HRESULT __stdcall GetInputData(const CATBSTR & iName, CATBaseDispatch *& oInput)=0;

    virtual HRESULT __stdcall PutInput(const CATBSTR & iName, CATIABase * iInput)=0;

    virtual HRESULT __stdcall PutInputData(const CATBSTR & iName, CATBaseDispatch * iInput)=0;

    virtual HRESULT __stdcall get_InputsCount(CATLONG & oNbInputs)=0;

    virtual HRESULT __stdcall GetInputFromPosition(CATLONG iPosition, CATIABase *& oInput)=0;

    virtual HRESULT __stdcall GetInputDataFromPosition(CATLONG iPosition, CATBaseDispatch *& oInput)=0;

    virtual HRESULT __stdcall GetParameter(const CATBSTR & iName, CATIABase *& oParameter)=0;

    virtual HRESULT __stdcall get_ParametersCount(CATLONG & oNbParameters)=0;

    virtual HRESULT __stdcall GetParameterFromPosition(CATLONG iPosition, CATIABase *& oParameter)=0;

    virtual HRESULT __stdcall GetOutput(const CATBSTR & iName, CATIABase *& oOutput)=0;

    virtual HRESULT __stdcall get_OutputsCount(CATLONG & oNbOutputs)=0;

    virtual HRESULT __stdcall GetOutputFromPosition(CATLONG iPosition, CATIABase *& oOutput)=0;


};

CATDeclareHandler(CATIAShapeInstance, CATIAShape);

#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIABase.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
