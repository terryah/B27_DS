#ifndef CATListOfCATIBRepAccess_h
#define CATListOfCATIBRepAccess_h

// COPYRIGHT DASSAULT SYSTEMES 1999

/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */


#include "CATLISTP_Clean.h"
#include "CATLISTP_PublicInterface.h"
#include "CATLISTP_Declare.h"
#include "MecModItfCPP.h"

/**
 * @collection CATLISTP(CATIBRepAccess)
 * Collection class for pointer to CATIBRepAccess interface.
 * All the methods of pointer collection classes are available.<br>
 * Refer to the articles dealing with collections in the encyclopedia.
 */
class CATIBRepAccess ;

#undef	CATCOLLEC_ExportedBy
#define	CATCOLLEC_ExportedBy	ExportedByMecModItfCPP

CATLISTP_DECLARE( CATIBRepAccess )

#endif


