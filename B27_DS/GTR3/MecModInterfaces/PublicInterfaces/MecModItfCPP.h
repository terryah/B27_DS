// COPYRIGHT DASSAULT SYSTEMES  2000
/** @CAA2Required */
//**********************************************************************
//* DON T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS *
//* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPEAR AT ANY TIME *
//**********************************************************************

#ifndef __MecModItfCPP_h__
#define __MecModItfCPP_h__
#ifdef _WINDOWS_SOURCE
#ifdef  __MecModItfCPP
#define ExportedByMecModItfCPP    __declspec(dllexport)
#else
#define ExportedByMecModItfCPP    __declspec(dllimport)
#endif
#else
#define ExportedByMecModItfCPP
#endif

#endif

