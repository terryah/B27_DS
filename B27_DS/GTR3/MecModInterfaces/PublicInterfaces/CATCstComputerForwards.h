#ifndef CATCstComputerForwards_h
#define CATCstComputerForwards_h

// COPYRIGHT DASSAULT SYSTEMES 1999

/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */

#include "MecModItfCPP.h"
class CATMathPlane;
class CATMathLine;
class CATMathPoint;
class CATI2DPoint;
class CATI2DPoint_var;
class CATI2DCircle;
class CATI2DCircle_var;
class CATBaseUnknown;
class CATBaseUnknown_var;
class CATI2DWFGeometry;
class CATI2DWFGeometry_var;
//class CATCstComputer;
class ExportedByMecModItfCPP CATDimMath;
class ExportedByMecModItfCPP CATDimTempCst;
#endif
