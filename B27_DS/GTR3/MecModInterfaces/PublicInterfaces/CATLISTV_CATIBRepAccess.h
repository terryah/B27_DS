
#ifndef CATLISTV_CATIBRepAccess_H
#define CATLISTV_CATIBRepAccess_H

// COPYRIGHT DASSAULT SYSTEMES 2008

/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */

/**
 * @collection CATLISTV(CATIBRepAccess_var)
 * Collection class for specobjects.
 * All the methods of handlers collection classes are available.
 * Refer to the articles dealing with collections in the encyclopedia.
 */

#include "CATLISTHand_Clean.h"

#include "CATLISTHand_AllFunct.h"

#include "CATLISTHand_Declare.h"

#include "AC0SPBAS.h"
#undef  CATCOLLEC_ExportedBy
#define	CATCOLLEC_ExportedBy ExportedByMecModItfCPP

#include "CATIBRepAccess.h"
CATLISTHand_DECLARE(CATIBRepAccess_var)

#endif // CATLISTV_CATIBRepAccess_H
