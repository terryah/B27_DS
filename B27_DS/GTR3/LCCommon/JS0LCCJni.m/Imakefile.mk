BUILT_OBJECT_TYPE = SHARED LIBRARY
BUILD = NO
DELIVERABLE = NO
MKMK_USEPRECOMPIL=
COMMON_LINK_WITH = LCCArch DSLSArch SSLArch

OS = AIX
BUILD = YES
LOCAL_CCFLAGS = $(COMMON_CCFLAGS) 
LINK_WITH = $(COMMON_LINK_WITH) 
SYS_INCPATH =
SYS_LIBPATH =
SYS_LIBS = -lodm -lcfg -lpthreads


OS = Linux
BUILD = YES
LOCAL_CCFLAGS = $(COMMON_CCFLAGS) 
LINK_WITH = $(COMMON_LINK_WITH) 
SYS_LIBS = -lpthread

OS = SunOS
BUILD = YES
LOCAL_CCFLAGS = $(COMMON_CCFLAGS) 
LINK_WITH = $(COMMON_LINK_WITH) 
SYS_LIBS= -lpthread -lsocket -lnsl

OS = Darwin
BUILD = YES
LOCAL_CCFLAGS = $(COMMON_CCFLAGS) 
LINK_WITH = $(COMMON_LINK_WITH)
SYS_LIBS=-framework CoreServices -framework CoreFoundation -framework IOKit

OS = Windows_NT
BUILD = YES
LOCAL_CCFLAGS = $(COMMON_CCFLAGS)
LINK_WITH = $(COMMON_LINK_WITH)
SYS_LIBS =  wsock32.lib advapi32.lib dnsapi.lib wininet.lib Ws2_32.lib
LOCAL_LDFLAGS = /NODEFAULTLIB:msvcirt.lib /NODEFAULTLIB:libcmt.lib 
