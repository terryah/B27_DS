BUILT_OBJECT_TYPE = LOAD MODULE
ENCODING_KEY = System
LINK_WITH = LCCArch DSLSArch SSLArch 

OS = Linux
SYS_LIBS= -lpthread

OS = SunOS
SYS_LIBS= -lpthread -lsocket -lnsl
SUNMATH_LIBS=
FORTRAN_LIBS=

OS = Darwin
SYS_LIBS=-framework CoreServices -framework CoreFoundation -framework IOKit \
 -framework SecurityFoundation -framework Security -framework SecurityInterface -framework AppKit \
 -framework Foundation

OS= iOS
BUILD= NO


OS = Windows_NT
MKMK_USEPRECOMPIL=
LINK_WITH=DSLSArchMT  LCCArchMT SSLArchMT
LOCAL_LDFLAGS = /NODEFAULTLIB:MSVCRT.lib /NODEFAULTLIB:uafxcw.lib
SYS_LIBS =  wsock32.lib advapi32.lib dnsapi.lib wininet.lib Ws2_32.lib Iphlpapi.lib Shlwapi.lib Secur32.lib Netapi32.lib libcmt.lib
STATIC_LIBS =YES

