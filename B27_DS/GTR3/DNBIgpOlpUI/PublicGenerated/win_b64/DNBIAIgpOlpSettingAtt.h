/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef DNBIAIgpOlpSettingAtt_h
#define DNBIAIgpOlpSettingAtt_h

#include "CATCORBABoolean.h"
#include "CAT_VARIANT_BOOL.h"

#ifndef ExportedByDNBIgpOlpPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __DNBIgpOlpPubIDL
#define ExportedByDNBIgpOlpPubIDL __declspec(dllexport)
#else
#define ExportedByDNBIgpOlpPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByDNBIgpOlpPubIDL
#endif
#endif

#include "CATBSTR.h"
#include "CATIASettingController.h"

extern ExportedByDNBIgpOlpPubIDL IID IID_DNBIAIgpOlpSettingAtt;

class ExportedByDNBIgpOlpPubIDL DNBIAIgpOlpSettingAtt : public CATIASettingController
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_DownloaderDir(CATBSTR & oDnldDir)=0;

    virtual HRESULT __stdcall put_DownloaderDir(const CATBSTR & iDnldDir)=0;

    virtual HRESULT __stdcall SetDownloaderDirLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall GetDownloaderDirInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall get_Downloader(CATBSTR & oDownloader)=0;

    virtual HRESULT __stdcall put_Downloader(const CATBSTR & iDownloader)=0;

    virtual HRESULT __stdcall SetDownloaderLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall GetDownloaderInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall get_UploaderDir(CATBSTR & oUpldDir)=0;

    virtual HRESULT __stdcall put_UploaderDir(const CATBSTR & iUpldDir)=0;

    virtual HRESULT __stdcall SetUploaderDirLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall GetUploaderDirInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall get_Uploader(CATBSTR & oUploader)=0;

    virtual HRESULT __stdcall put_Uploader(const CATBSTR & iUploader)=0;

    virtual HRESULT __stdcall SetUploaderLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall GetUploaderInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall get_XMLDir(CATBSTR & oXMLDir)=0;

    virtual HRESULT __stdcall put_XMLDir(const CATBSTR & iXMLDir)=0;

    virtual HRESULT __stdcall SetXMLDirLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall GetXMLDirInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall get_XMLSchema(CATBSTR & oXMLSchema)=0;

    virtual HRESULT __stdcall put_XMLSchema(const CATBSTR & iXMLSchema)=0;

    virtual HRESULT __stdcall SetXMLSchemaLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall GetXMLSchemaInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall get_RobotProgramDir(CATBSTR & oRobotProgramDir)=0;

    virtual HRESULT __stdcall put_RobotProgramDir(const CATBSTR & iRobotProgramDir)=0;

    virtual HRESULT __stdcall SetRobotProgramDirLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall GetRobotProgramDirInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall get_NRLDir(CATBSTR & oNRLDir)=0;

    virtual HRESULT __stdcall put_NRLDir(const CATBSTR & iNRLDir)=0;

    virtual HRESULT __stdcall SetNRLDirLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall GetNRLDirInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall get_JavaExe(CATBSTR & oJavaExe)=0;

    virtual HRESULT __stdcall put_JavaExe(const CATBSTR & iJavaExe)=0;

    virtual HRESULT __stdcall SetJavaExeLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall GetJavaExeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall get_JavaClassPath(CATBSTR & oJavaClassPath)=0;

    virtual HRESULT __stdcall put_JavaClassPath(const CATBSTR & iJavaClassPath)=0;

    virtual HRESULT __stdcall SetJavaClassPathLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall GetJavaClassPathInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall get_ExistingTaskTreatment(CATLONG & oVerdict)=0;

    virtual HRESULT __stdcall put_ExistingTaskTreatment(CATLONG iVerdict)=0;

    virtual HRESULT __stdcall SetExistingTaskTreatmentLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall GetExistingTaskTreatmentInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall get_NRLTeachDialogDisplayMode(CATLONG & oIsHidden)=0;

    virtual HRESULT __stdcall put_NRLTeachDialogDisplayMode(CATLONG iIsHidden)=0;

    virtual HRESULT __stdcall SetNRLTeachDialogDisplayModeLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall GetNRLTeachDialogDisplayModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;


};

CATDeclareHandler(DNBIAIgpOlpSettingAtt, CATIASettingController);

#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIABase.h"
#include "CATSafeArray.h"
#include "CATVariant.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
