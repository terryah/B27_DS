#ifndef __TIE_DNBIAIgpOlpSettingAtt
#define __TIE_DNBIAIgpOlpSettingAtt

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIAIgpOlpSettingAtt.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIAIgpOlpSettingAtt */
#define declare_TIE_DNBIAIgpOlpSettingAtt(classe) \
 \
 \
class TIEDNBIAIgpOlpSettingAtt##classe : public DNBIAIgpOlpSettingAtt \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIAIgpOlpSettingAtt, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall get_DownloaderDir(CATBSTR & oDnldDir); \
      virtual HRESULT __stdcall put_DownloaderDir(const CATBSTR & iDnldDir); \
      virtual HRESULT __stdcall SetDownloaderDirLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall GetDownloaderDirInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall get_Downloader(CATBSTR & oDownloader); \
      virtual HRESULT __stdcall put_Downloader(const CATBSTR & iDownloader); \
      virtual HRESULT __stdcall SetDownloaderLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall GetDownloaderInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall get_UploaderDir(CATBSTR & oUpldDir); \
      virtual HRESULT __stdcall put_UploaderDir(const CATBSTR & iUpldDir); \
      virtual HRESULT __stdcall SetUploaderDirLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall GetUploaderDirInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall get_Uploader(CATBSTR & oUploader); \
      virtual HRESULT __stdcall put_Uploader(const CATBSTR & iUploader); \
      virtual HRESULT __stdcall SetUploaderLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall GetUploaderInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall get_XMLDir(CATBSTR & oXMLDir); \
      virtual HRESULT __stdcall put_XMLDir(const CATBSTR & iXMLDir); \
      virtual HRESULT __stdcall SetXMLDirLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall GetXMLDirInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall get_XMLSchema(CATBSTR & oXMLSchema); \
      virtual HRESULT __stdcall put_XMLSchema(const CATBSTR & iXMLSchema); \
      virtual HRESULT __stdcall SetXMLSchemaLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall GetXMLSchemaInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall get_RobotProgramDir(CATBSTR & oRobotProgramDir); \
      virtual HRESULT __stdcall put_RobotProgramDir(const CATBSTR & iRobotProgramDir); \
      virtual HRESULT __stdcall SetRobotProgramDirLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall GetRobotProgramDirInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall get_NRLDir(CATBSTR & oNRLDir); \
      virtual HRESULT __stdcall put_NRLDir(const CATBSTR & iNRLDir); \
      virtual HRESULT __stdcall SetNRLDirLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall GetNRLDirInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall get_JavaExe(CATBSTR & oJavaExe); \
      virtual HRESULT __stdcall put_JavaExe(const CATBSTR & iJavaExe); \
      virtual HRESULT __stdcall SetJavaExeLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall GetJavaExeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall get_JavaClassPath(CATBSTR & oJavaClassPath); \
      virtual HRESULT __stdcall put_JavaClassPath(const CATBSTR & iJavaClassPath); \
      virtual HRESULT __stdcall SetJavaClassPathLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall GetJavaClassPathInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall get_ExistingTaskTreatment(CATLONG & oVerdict); \
      virtual HRESULT __stdcall put_ExistingTaskTreatment(CATLONG iVerdict); \
      virtual HRESULT __stdcall SetExistingTaskTreatmentLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall GetExistingTaskTreatmentInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall get_NRLTeachDialogDisplayMode(CATLONG & oIsHidden); \
      virtual HRESULT __stdcall put_NRLTeachDialogDisplayMode(CATLONG iIsHidden); \
      virtual HRESULT __stdcall SetNRLTeachDialogDisplayModeLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall GetNRLTeachDialogDisplayModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall Commit(); \
      virtual HRESULT __stdcall Rollback(); \
      virtual HRESULT __stdcall ResetToAdminValues(); \
      virtual HRESULT __stdcall ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList); \
      virtual HRESULT __stdcall SaveRepository(); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIAIgpOlpSettingAtt(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall get_DownloaderDir(CATBSTR & oDnldDir); \
virtual HRESULT __stdcall put_DownloaderDir(const CATBSTR & iDnldDir); \
virtual HRESULT __stdcall SetDownloaderDirLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall GetDownloaderDirInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall get_Downloader(CATBSTR & oDownloader); \
virtual HRESULT __stdcall put_Downloader(const CATBSTR & iDownloader); \
virtual HRESULT __stdcall SetDownloaderLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall GetDownloaderInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall get_UploaderDir(CATBSTR & oUpldDir); \
virtual HRESULT __stdcall put_UploaderDir(const CATBSTR & iUpldDir); \
virtual HRESULT __stdcall SetUploaderDirLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall GetUploaderDirInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall get_Uploader(CATBSTR & oUploader); \
virtual HRESULT __stdcall put_Uploader(const CATBSTR & iUploader); \
virtual HRESULT __stdcall SetUploaderLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall GetUploaderInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall get_XMLDir(CATBSTR & oXMLDir); \
virtual HRESULT __stdcall put_XMLDir(const CATBSTR & iXMLDir); \
virtual HRESULT __stdcall SetXMLDirLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall GetXMLDirInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall get_XMLSchema(CATBSTR & oXMLSchema); \
virtual HRESULT __stdcall put_XMLSchema(const CATBSTR & iXMLSchema); \
virtual HRESULT __stdcall SetXMLSchemaLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall GetXMLSchemaInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall get_RobotProgramDir(CATBSTR & oRobotProgramDir); \
virtual HRESULT __stdcall put_RobotProgramDir(const CATBSTR & iRobotProgramDir); \
virtual HRESULT __stdcall SetRobotProgramDirLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall GetRobotProgramDirInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall get_NRLDir(CATBSTR & oNRLDir); \
virtual HRESULT __stdcall put_NRLDir(const CATBSTR & iNRLDir); \
virtual HRESULT __stdcall SetNRLDirLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall GetNRLDirInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall get_JavaExe(CATBSTR & oJavaExe); \
virtual HRESULT __stdcall put_JavaExe(const CATBSTR & iJavaExe); \
virtual HRESULT __stdcall SetJavaExeLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall GetJavaExeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall get_JavaClassPath(CATBSTR & oJavaClassPath); \
virtual HRESULT __stdcall put_JavaClassPath(const CATBSTR & iJavaClassPath); \
virtual HRESULT __stdcall SetJavaClassPathLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall GetJavaClassPathInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall get_ExistingTaskTreatment(CATLONG & oVerdict); \
virtual HRESULT __stdcall put_ExistingTaskTreatment(CATLONG iVerdict); \
virtual HRESULT __stdcall SetExistingTaskTreatmentLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall GetExistingTaskTreatmentInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall get_NRLTeachDialogDisplayMode(CATLONG & oIsHidden); \
virtual HRESULT __stdcall put_NRLTeachDialogDisplayMode(CATLONG iIsHidden); \
virtual HRESULT __stdcall SetNRLTeachDialogDisplayModeLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall GetNRLTeachDialogDisplayModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall Commit(); \
virtual HRESULT __stdcall Rollback(); \
virtual HRESULT __stdcall ResetToAdminValues(); \
virtual HRESULT __stdcall ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList); \
virtual HRESULT __stdcall SaveRepository(); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIAIgpOlpSettingAtt(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::get_DownloaderDir(CATBSTR & oDnldDir) \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_DownloaderDir(oDnldDir)); \
} \
HRESULT __stdcall  ENVTIEName::put_DownloaderDir(const CATBSTR & iDnldDir) \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_DownloaderDir(iDnldDir)); \
} \
HRESULT __stdcall  ENVTIEName::SetDownloaderDirLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetDownloaderDirLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::GetDownloaderDirInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetDownloaderDirInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::get_Downloader(CATBSTR & oDownloader) \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_Downloader(oDownloader)); \
} \
HRESULT __stdcall  ENVTIEName::put_Downloader(const CATBSTR & iDownloader) \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_Downloader(iDownloader)); \
} \
HRESULT __stdcall  ENVTIEName::SetDownloaderLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetDownloaderLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::GetDownloaderInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetDownloaderInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::get_UploaderDir(CATBSTR & oUpldDir) \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_UploaderDir(oUpldDir)); \
} \
HRESULT __stdcall  ENVTIEName::put_UploaderDir(const CATBSTR & iUpldDir) \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_UploaderDir(iUpldDir)); \
} \
HRESULT __stdcall  ENVTIEName::SetUploaderDirLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetUploaderDirLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::GetUploaderDirInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetUploaderDirInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::get_Uploader(CATBSTR & oUploader) \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_Uploader(oUploader)); \
} \
HRESULT __stdcall  ENVTIEName::put_Uploader(const CATBSTR & iUploader) \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_Uploader(iUploader)); \
} \
HRESULT __stdcall  ENVTIEName::SetUploaderLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetUploaderLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::GetUploaderInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetUploaderInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::get_XMLDir(CATBSTR & oXMLDir) \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_XMLDir(oXMLDir)); \
} \
HRESULT __stdcall  ENVTIEName::put_XMLDir(const CATBSTR & iXMLDir) \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_XMLDir(iXMLDir)); \
} \
HRESULT __stdcall  ENVTIEName::SetXMLDirLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetXMLDirLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::GetXMLDirInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetXMLDirInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::get_XMLSchema(CATBSTR & oXMLSchema) \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_XMLSchema(oXMLSchema)); \
} \
HRESULT __stdcall  ENVTIEName::put_XMLSchema(const CATBSTR & iXMLSchema) \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_XMLSchema(iXMLSchema)); \
} \
HRESULT __stdcall  ENVTIEName::SetXMLSchemaLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetXMLSchemaLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::GetXMLSchemaInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetXMLSchemaInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::get_RobotProgramDir(CATBSTR & oRobotProgramDir) \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_RobotProgramDir(oRobotProgramDir)); \
} \
HRESULT __stdcall  ENVTIEName::put_RobotProgramDir(const CATBSTR & iRobotProgramDir) \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_RobotProgramDir(iRobotProgramDir)); \
} \
HRESULT __stdcall  ENVTIEName::SetRobotProgramDirLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetRobotProgramDirLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::GetRobotProgramDirInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetRobotProgramDirInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::get_NRLDir(CATBSTR & oNRLDir) \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_NRLDir(oNRLDir)); \
} \
HRESULT __stdcall  ENVTIEName::put_NRLDir(const CATBSTR & iNRLDir) \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_NRLDir(iNRLDir)); \
} \
HRESULT __stdcall  ENVTIEName::SetNRLDirLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetNRLDirLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::GetNRLDirInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetNRLDirInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::get_JavaExe(CATBSTR & oJavaExe) \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_JavaExe(oJavaExe)); \
} \
HRESULT __stdcall  ENVTIEName::put_JavaExe(const CATBSTR & iJavaExe) \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_JavaExe(iJavaExe)); \
} \
HRESULT __stdcall  ENVTIEName::SetJavaExeLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetJavaExeLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::GetJavaExeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetJavaExeInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::get_JavaClassPath(CATBSTR & oJavaClassPath) \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_JavaClassPath(oJavaClassPath)); \
} \
HRESULT __stdcall  ENVTIEName::put_JavaClassPath(const CATBSTR & iJavaClassPath) \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_JavaClassPath(iJavaClassPath)); \
} \
HRESULT __stdcall  ENVTIEName::SetJavaClassPathLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetJavaClassPathLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::GetJavaClassPathInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetJavaClassPathInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::get_ExistingTaskTreatment(CATLONG & oVerdict) \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_ExistingTaskTreatment(oVerdict)); \
} \
HRESULT __stdcall  ENVTIEName::put_ExistingTaskTreatment(CATLONG iVerdict) \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_ExistingTaskTreatment(iVerdict)); \
} \
HRESULT __stdcall  ENVTIEName::SetExistingTaskTreatmentLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetExistingTaskTreatmentLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::GetExistingTaskTreatmentInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetExistingTaskTreatmentInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::get_NRLTeachDialogDisplayMode(CATLONG & oIsHidden) \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_NRLTeachDialogDisplayMode(oIsHidden)); \
} \
HRESULT __stdcall  ENVTIEName::put_NRLTeachDialogDisplayMode(CATLONG iIsHidden) \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_NRLTeachDialogDisplayMode(iIsHidden)); \
} \
HRESULT __stdcall  ENVTIEName::SetNRLTeachDialogDisplayModeLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetNRLTeachDialogDisplayModeLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::GetNRLTeachDialogDisplayModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetNRLTeachDialogDisplayModeInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::Commit() \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)Commit()); \
} \
HRESULT __stdcall  ENVTIEName::Rollback() \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)Rollback()); \
} \
HRESULT __stdcall  ENVTIEName::ResetToAdminValues() \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)ResetToAdminValues()); \
} \
HRESULT __stdcall  ENVTIEName::ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList) \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)ResetToAdminValuesByName(iAttList)); \
} \
HRESULT __stdcall  ENVTIEName::SaveRepository() \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)SaveRepository()); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIAIgpOlpSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIAIgpOlpSettingAtt(classe)    TIEDNBIAIgpOlpSettingAtt##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIAIgpOlpSettingAtt(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIAIgpOlpSettingAtt, classe) \
 \
 \
CATImplementTIEMethods(DNBIAIgpOlpSettingAtt, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIAIgpOlpSettingAtt, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIAIgpOlpSettingAtt, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIAIgpOlpSettingAtt, classe) \
 \
HRESULT __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::get_DownloaderDir(CATBSTR & oDnldDir) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&oDnldDir); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_DownloaderDir(oDnldDir); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&oDnldDir); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::put_DownloaderDir(const CATBSTR & iDnldDir) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&iDnldDir); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_DownloaderDir(iDnldDir); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&iDnldDir); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::SetDownloaderDirLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetDownloaderDirLock(iLocked); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::GetDownloaderDirInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetDownloaderDirInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::get_Downloader(CATBSTR & oDownloader) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&oDownloader); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Downloader(oDownloader); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&oDownloader); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::put_Downloader(const CATBSTR & iDownloader) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&iDownloader); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Downloader(iDownloader); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&iDownloader); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::SetDownloaderLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetDownloaderLock(iLocked); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::GetDownloaderInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetDownloaderInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::get_UploaderDir(CATBSTR & oUpldDir) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&oUpldDir); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_UploaderDir(oUpldDir); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&oUpldDir); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::put_UploaderDir(const CATBSTR & iUpldDir) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&iUpldDir); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_UploaderDir(iUpldDir); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&iUpldDir); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::SetUploaderDirLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,11,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetUploaderDirLock(iLocked); \
   ExitAfterCall(this,11,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::GetUploaderDirInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,12,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetUploaderDirInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,12,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::get_Uploader(CATBSTR & oUploader) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,13,&_Trac2,&oUploader); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Uploader(oUploader); \
   ExitAfterCall(this,13,_Trac2,&_ret_arg,&oUploader); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::put_Uploader(const CATBSTR & iUploader) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,14,&_Trac2,&iUploader); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Uploader(iUploader); \
   ExitAfterCall(this,14,_Trac2,&_ret_arg,&iUploader); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::SetUploaderLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,15,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetUploaderLock(iLocked); \
   ExitAfterCall(this,15,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::GetUploaderInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,16,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetUploaderInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,16,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::get_XMLDir(CATBSTR & oXMLDir) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,17,&_Trac2,&oXMLDir); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_XMLDir(oXMLDir); \
   ExitAfterCall(this,17,_Trac2,&_ret_arg,&oXMLDir); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::put_XMLDir(const CATBSTR & iXMLDir) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,18,&_Trac2,&iXMLDir); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_XMLDir(iXMLDir); \
   ExitAfterCall(this,18,_Trac2,&_ret_arg,&iXMLDir); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::SetXMLDirLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,19,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetXMLDirLock(iLocked); \
   ExitAfterCall(this,19,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::GetXMLDirInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,20,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetXMLDirInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,20,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::get_XMLSchema(CATBSTR & oXMLSchema) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,21,&_Trac2,&oXMLSchema); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_XMLSchema(oXMLSchema); \
   ExitAfterCall(this,21,_Trac2,&_ret_arg,&oXMLSchema); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::put_XMLSchema(const CATBSTR & iXMLSchema) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,22,&_Trac2,&iXMLSchema); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_XMLSchema(iXMLSchema); \
   ExitAfterCall(this,22,_Trac2,&_ret_arg,&iXMLSchema); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::SetXMLSchemaLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,23,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetXMLSchemaLock(iLocked); \
   ExitAfterCall(this,23,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::GetXMLSchemaInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,24,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetXMLSchemaInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,24,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::get_RobotProgramDir(CATBSTR & oRobotProgramDir) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,25,&_Trac2,&oRobotProgramDir); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_RobotProgramDir(oRobotProgramDir); \
   ExitAfterCall(this,25,_Trac2,&_ret_arg,&oRobotProgramDir); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::put_RobotProgramDir(const CATBSTR & iRobotProgramDir) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,26,&_Trac2,&iRobotProgramDir); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_RobotProgramDir(iRobotProgramDir); \
   ExitAfterCall(this,26,_Trac2,&_ret_arg,&iRobotProgramDir); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::SetRobotProgramDirLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,27,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetRobotProgramDirLock(iLocked); \
   ExitAfterCall(this,27,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::GetRobotProgramDirInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,28,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetRobotProgramDirInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,28,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::get_NRLDir(CATBSTR & oNRLDir) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,29,&_Trac2,&oNRLDir); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_NRLDir(oNRLDir); \
   ExitAfterCall(this,29,_Trac2,&_ret_arg,&oNRLDir); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::put_NRLDir(const CATBSTR & iNRLDir) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,30,&_Trac2,&iNRLDir); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_NRLDir(iNRLDir); \
   ExitAfterCall(this,30,_Trac2,&_ret_arg,&iNRLDir); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::SetNRLDirLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,31,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetNRLDirLock(iLocked); \
   ExitAfterCall(this,31,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::GetNRLDirInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,32,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetNRLDirInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,32,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::get_JavaExe(CATBSTR & oJavaExe) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,33,&_Trac2,&oJavaExe); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_JavaExe(oJavaExe); \
   ExitAfterCall(this,33,_Trac2,&_ret_arg,&oJavaExe); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::put_JavaExe(const CATBSTR & iJavaExe) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,34,&_Trac2,&iJavaExe); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_JavaExe(iJavaExe); \
   ExitAfterCall(this,34,_Trac2,&_ret_arg,&iJavaExe); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::SetJavaExeLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,35,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetJavaExeLock(iLocked); \
   ExitAfterCall(this,35,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::GetJavaExeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,36,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetJavaExeInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,36,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::get_JavaClassPath(CATBSTR & oJavaClassPath) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,37,&_Trac2,&oJavaClassPath); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_JavaClassPath(oJavaClassPath); \
   ExitAfterCall(this,37,_Trac2,&_ret_arg,&oJavaClassPath); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::put_JavaClassPath(const CATBSTR & iJavaClassPath) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,38,&_Trac2,&iJavaClassPath); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_JavaClassPath(iJavaClassPath); \
   ExitAfterCall(this,38,_Trac2,&_ret_arg,&iJavaClassPath); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::SetJavaClassPathLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,39,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetJavaClassPathLock(iLocked); \
   ExitAfterCall(this,39,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::GetJavaClassPathInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,40,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetJavaClassPathInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,40,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::get_ExistingTaskTreatment(CATLONG & oVerdict) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,41,&_Trac2,&oVerdict); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_ExistingTaskTreatment(oVerdict); \
   ExitAfterCall(this,41,_Trac2,&_ret_arg,&oVerdict); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::put_ExistingTaskTreatment(CATLONG iVerdict) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,42,&_Trac2,&iVerdict); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_ExistingTaskTreatment(iVerdict); \
   ExitAfterCall(this,42,_Trac2,&_ret_arg,&iVerdict); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::SetExistingTaskTreatmentLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,43,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetExistingTaskTreatmentLock(iLocked); \
   ExitAfterCall(this,43,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::GetExistingTaskTreatmentInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,44,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetExistingTaskTreatmentInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,44,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::get_NRLTeachDialogDisplayMode(CATLONG & oIsHidden) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,45,&_Trac2,&oIsHidden); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_NRLTeachDialogDisplayMode(oIsHidden); \
   ExitAfterCall(this,45,_Trac2,&_ret_arg,&oIsHidden); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::put_NRLTeachDialogDisplayMode(CATLONG iIsHidden) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,46,&_Trac2,&iIsHidden); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_NRLTeachDialogDisplayMode(iIsHidden); \
   ExitAfterCall(this,46,_Trac2,&_ret_arg,&iIsHidden); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::SetNRLTeachDialogDisplayModeLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,47,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetNRLTeachDialogDisplayModeLock(iLocked); \
   ExitAfterCall(this,47,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::GetNRLTeachDialogDisplayModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,48,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetNRLTeachDialogDisplayModeInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,48,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::Commit() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,49,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Commit(); \
   ExitAfterCall(this,49,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::Rollback() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,50,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Rollback(); \
   ExitAfterCall(this,50,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::ResetToAdminValues() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,51,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ResetToAdminValues(); \
   ExitAfterCall(this,51,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,52,&_Trac2,&iAttList); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ResetToAdminValuesByName(iAttList); \
   ExitAfterCall(this,52,_Trac2,&_ret_arg,&iAttList); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::SaveRepository() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,53,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SaveRepository(); \
   ExitAfterCall(this,53,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,54,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,54,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,55,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,55,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,56,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,56,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,57,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,57,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAIgpOlpSettingAtt##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,58,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,58,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIAIgpOlpSettingAtt(classe) \
 \
 \
declare_TIE_DNBIAIgpOlpSettingAtt(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAIgpOlpSettingAtt##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAIgpOlpSettingAtt,"DNBIAIgpOlpSettingAtt",DNBIAIgpOlpSettingAtt::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAIgpOlpSettingAtt(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIAIgpOlpSettingAtt, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAIgpOlpSettingAtt##classe(classe::MetaObject(),DNBIAIgpOlpSettingAtt::MetaObject(),(void *)CreateTIEDNBIAIgpOlpSettingAtt##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIAIgpOlpSettingAtt(classe) \
 \
 \
declare_TIE_DNBIAIgpOlpSettingAtt(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAIgpOlpSettingAtt##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAIgpOlpSettingAtt,"DNBIAIgpOlpSettingAtt",DNBIAIgpOlpSettingAtt::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAIgpOlpSettingAtt(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIAIgpOlpSettingAtt, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAIgpOlpSettingAtt##classe(classe::MetaObject(),DNBIAIgpOlpSettingAtt::MetaObject(),(void *)CreateTIEDNBIAIgpOlpSettingAtt##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIAIgpOlpSettingAtt(classe) TIE_DNBIAIgpOlpSettingAtt(classe)
#else
#define BOA_DNBIAIgpOlpSettingAtt(classe) CATImplementBOA(DNBIAIgpOlpSettingAtt, classe)
#endif

#endif
