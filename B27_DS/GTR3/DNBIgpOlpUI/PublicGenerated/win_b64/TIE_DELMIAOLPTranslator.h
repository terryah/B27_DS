#ifndef __TIE_DELMIAOLPTranslator
#define __TIE_DELMIAOLPTranslator

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DELMIAOLPTranslator.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DELMIAOLPTranslator */
#define declare_TIE_DELMIAOLPTranslator(classe) \
 \
 \
class TIEDELMIAOLPTranslator##classe : public DELMIAOLPTranslator \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DELMIAOLPTranslator, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall DownloadTasks(const CATSafeArrayVariant & lListOfTasks, const CATBSTR & sPathToXSLTTranslator, const CATBSTR & sContext, CAT_VARIANT_BOOL bIsPartCoordinates, CAT_VARIANT_BOOL bIsSubroutine, const CATBSTR & sLogFileName); \
      virtual HRESULT __stdcall GetRobotProgramDirectory(CATBSTR & sRobotProgramDir); \
      virtual HRESULT __stdcall UploadRobotProgram(CATIAProduct * spISelectedRobot, const CATBSTR & sPathToRobotProgramFile, const CATBSTR & sPathToUploaderFile, CAT_VARIANT_BOOL bIsUploadInPartCoords); \
      virtual HRESULT __stdcall GetXMLFileDirectory(CATBSTR & sXMLFileDir); \
      virtual HRESULT __stdcall DownloadAsXML(DELMIARobotTask * iRobotTask, const CATBSTR & oXMLFileName, CAT_VARIANT_BOOL iIsPartCoordinates, CAT_VARIANT_BOOL iIsSimulated, CAT_VARIANT_BOOL iIsSubRoutines); \
      virtual HRESULT __stdcall ConvertXMLToNRL(const CATBSTR & iXMLFileName, const CATBSTR & iXSLTFileName, const CATBSTR & oNRLTargetFolder); \
      virtual HRESULT __stdcall UploadFromXML(DELMIARobotTask * iRobotTask, const CATBSTR & iXMLFileName, CAT_VARIANT_BOOL iIsPartCoordinates, CATIAProduct * iPart); \
      virtual HRESULT __stdcall ConvertNRLToXML(const CATBSTR & iNRLFileName, const CATBSTR & iParserName, const CATBSTR & oXMLFileName); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DELMIAOLPTranslator(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall DownloadTasks(const CATSafeArrayVariant & lListOfTasks, const CATBSTR & sPathToXSLTTranslator, const CATBSTR & sContext, CAT_VARIANT_BOOL bIsPartCoordinates, CAT_VARIANT_BOOL bIsSubroutine, const CATBSTR & sLogFileName); \
virtual HRESULT __stdcall GetRobotProgramDirectory(CATBSTR & sRobotProgramDir); \
virtual HRESULT __stdcall UploadRobotProgram(CATIAProduct * spISelectedRobot, const CATBSTR & sPathToRobotProgramFile, const CATBSTR & sPathToUploaderFile, CAT_VARIANT_BOOL bIsUploadInPartCoords); \
virtual HRESULT __stdcall GetXMLFileDirectory(CATBSTR & sXMLFileDir); \
virtual HRESULT __stdcall DownloadAsXML(DELMIARobotTask * iRobotTask, const CATBSTR & oXMLFileName, CAT_VARIANT_BOOL iIsPartCoordinates, CAT_VARIANT_BOOL iIsSimulated, CAT_VARIANT_BOOL iIsSubRoutines); \
virtual HRESULT __stdcall ConvertXMLToNRL(const CATBSTR & iXMLFileName, const CATBSTR & iXSLTFileName, const CATBSTR & oNRLTargetFolder); \
virtual HRESULT __stdcall UploadFromXML(DELMIARobotTask * iRobotTask, const CATBSTR & iXMLFileName, CAT_VARIANT_BOOL iIsPartCoordinates, CATIAProduct * iPart); \
virtual HRESULT __stdcall ConvertNRLToXML(const CATBSTR & iNRLFileName, const CATBSTR & iParserName, const CATBSTR & oXMLFileName); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DELMIAOLPTranslator(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::DownloadTasks(const CATSafeArrayVariant & lListOfTasks, const CATBSTR & sPathToXSLTTranslator, const CATBSTR & sContext, CAT_VARIANT_BOOL bIsPartCoordinates, CAT_VARIANT_BOOL bIsSubroutine, const CATBSTR & sLogFileName) \
{ \
return (ENVTIECALL(DELMIAOLPTranslator,ENVTIETypeLetter,ENVTIELetter)DownloadTasks(lListOfTasks,sPathToXSLTTranslator,sContext,bIsPartCoordinates,bIsSubroutine,sLogFileName)); \
} \
HRESULT __stdcall  ENVTIEName::GetRobotProgramDirectory(CATBSTR & sRobotProgramDir) \
{ \
return (ENVTIECALL(DELMIAOLPTranslator,ENVTIETypeLetter,ENVTIELetter)GetRobotProgramDirectory(sRobotProgramDir)); \
} \
HRESULT __stdcall  ENVTIEName::UploadRobotProgram(CATIAProduct * spISelectedRobot, const CATBSTR & sPathToRobotProgramFile, const CATBSTR & sPathToUploaderFile, CAT_VARIANT_BOOL bIsUploadInPartCoords) \
{ \
return (ENVTIECALL(DELMIAOLPTranslator,ENVTIETypeLetter,ENVTIELetter)UploadRobotProgram(spISelectedRobot,sPathToRobotProgramFile,sPathToUploaderFile,bIsUploadInPartCoords)); \
} \
HRESULT __stdcall  ENVTIEName::GetXMLFileDirectory(CATBSTR & sXMLFileDir) \
{ \
return (ENVTIECALL(DELMIAOLPTranslator,ENVTIETypeLetter,ENVTIELetter)GetXMLFileDirectory(sXMLFileDir)); \
} \
HRESULT __stdcall  ENVTIEName::DownloadAsXML(DELMIARobotTask * iRobotTask, const CATBSTR & oXMLFileName, CAT_VARIANT_BOOL iIsPartCoordinates, CAT_VARIANT_BOOL iIsSimulated, CAT_VARIANT_BOOL iIsSubRoutines) \
{ \
return (ENVTIECALL(DELMIAOLPTranslator,ENVTIETypeLetter,ENVTIELetter)DownloadAsXML(iRobotTask,oXMLFileName,iIsPartCoordinates,iIsSimulated,iIsSubRoutines)); \
} \
HRESULT __stdcall  ENVTIEName::ConvertXMLToNRL(const CATBSTR & iXMLFileName, const CATBSTR & iXSLTFileName, const CATBSTR & oNRLTargetFolder) \
{ \
return (ENVTIECALL(DELMIAOLPTranslator,ENVTIETypeLetter,ENVTIELetter)ConvertXMLToNRL(iXMLFileName,iXSLTFileName,oNRLTargetFolder)); \
} \
HRESULT __stdcall  ENVTIEName::UploadFromXML(DELMIARobotTask * iRobotTask, const CATBSTR & iXMLFileName, CAT_VARIANT_BOOL iIsPartCoordinates, CATIAProduct * iPart) \
{ \
return (ENVTIECALL(DELMIAOLPTranslator,ENVTIETypeLetter,ENVTIELetter)UploadFromXML(iRobotTask,iXMLFileName,iIsPartCoordinates,iPart)); \
} \
HRESULT __stdcall  ENVTIEName::ConvertNRLToXML(const CATBSTR & iNRLFileName, const CATBSTR & iParserName, const CATBSTR & oXMLFileName) \
{ \
return (ENVTIECALL(DELMIAOLPTranslator,ENVTIETypeLetter,ENVTIELetter)ConvertNRLToXML(iNRLFileName,iParserName,oXMLFileName)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DELMIAOLPTranslator,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DELMIAOLPTranslator,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DELMIAOLPTranslator,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DELMIAOLPTranslator,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DELMIAOLPTranslator,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DELMIAOLPTranslator(classe)    TIEDELMIAOLPTranslator##classe


/* Common methods inside a TIE */
#define common_TIE_DELMIAOLPTranslator(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DELMIAOLPTranslator, classe) \
 \
 \
CATImplementTIEMethods(DELMIAOLPTranslator, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DELMIAOLPTranslator, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DELMIAOLPTranslator, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DELMIAOLPTranslator, classe) \
 \
HRESULT __stdcall  TIEDELMIAOLPTranslator##classe::DownloadTasks(const CATSafeArrayVariant & lListOfTasks, const CATBSTR & sPathToXSLTTranslator, const CATBSTR & sContext, CAT_VARIANT_BOOL bIsPartCoordinates, CAT_VARIANT_BOOL bIsSubroutine, const CATBSTR & sLogFileName) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&lListOfTasks,&sPathToXSLTTranslator,&sContext,&bIsPartCoordinates,&bIsSubroutine,&sLogFileName); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->DownloadTasks(lListOfTasks,sPathToXSLTTranslator,sContext,bIsPartCoordinates,bIsSubroutine,sLogFileName); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&lListOfTasks,&sPathToXSLTTranslator,&sContext,&bIsPartCoordinates,&bIsSubroutine,&sLogFileName); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAOLPTranslator##classe::GetRobotProgramDirectory(CATBSTR & sRobotProgramDir) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&sRobotProgramDir); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetRobotProgramDirectory(sRobotProgramDir); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&sRobotProgramDir); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAOLPTranslator##classe::UploadRobotProgram(CATIAProduct * spISelectedRobot, const CATBSTR & sPathToRobotProgramFile, const CATBSTR & sPathToUploaderFile, CAT_VARIANT_BOOL bIsUploadInPartCoords) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&spISelectedRobot,&sPathToRobotProgramFile,&sPathToUploaderFile,&bIsUploadInPartCoords); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->UploadRobotProgram(spISelectedRobot,sPathToRobotProgramFile,sPathToUploaderFile,bIsUploadInPartCoords); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&spISelectedRobot,&sPathToRobotProgramFile,&sPathToUploaderFile,&bIsUploadInPartCoords); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAOLPTranslator##classe::GetXMLFileDirectory(CATBSTR & sXMLFileDir) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&sXMLFileDir); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetXMLFileDirectory(sXMLFileDir); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&sXMLFileDir); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAOLPTranslator##classe::DownloadAsXML(DELMIARobotTask * iRobotTask, const CATBSTR & oXMLFileName, CAT_VARIANT_BOOL iIsPartCoordinates, CAT_VARIANT_BOOL iIsSimulated, CAT_VARIANT_BOOL iIsSubRoutines) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&iRobotTask,&oXMLFileName,&iIsPartCoordinates,&iIsSimulated,&iIsSubRoutines); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->DownloadAsXML(iRobotTask,oXMLFileName,iIsPartCoordinates,iIsSimulated,iIsSubRoutines); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&iRobotTask,&oXMLFileName,&iIsPartCoordinates,&iIsSimulated,&iIsSubRoutines); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAOLPTranslator##classe::ConvertXMLToNRL(const CATBSTR & iXMLFileName, const CATBSTR & iXSLTFileName, const CATBSTR & oNRLTargetFolder) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&iXMLFileName,&iXSLTFileName,&oNRLTargetFolder); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ConvertXMLToNRL(iXMLFileName,iXSLTFileName,oNRLTargetFolder); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&iXMLFileName,&iXSLTFileName,&oNRLTargetFolder); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAOLPTranslator##classe::UploadFromXML(DELMIARobotTask * iRobotTask, const CATBSTR & iXMLFileName, CAT_VARIANT_BOOL iIsPartCoordinates, CATIAProduct * iPart) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&iRobotTask,&iXMLFileName,&iIsPartCoordinates,&iPart); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->UploadFromXML(iRobotTask,iXMLFileName,iIsPartCoordinates,iPart); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&iRobotTask,&iXMLFileName,&iIsPartCoordinates,&iPart); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAOLPTranslator##classe::ConvertNRLToXML(const CATBSTR & iNRLFileName, const CATBSTR & iParserName, const CATBSTR & oXMLFileName) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&iNRLFileName,&iParserName,&oXMLFileName); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ConvertNRLToXML(iNRLFileName,iParserName,oXMLFileName); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&iNRLFileName,&iParserName,&oXMLFileName); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAOLPTranslator##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAOLPTranslator##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAOLPTranslator##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,11,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,11,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAOLPTranslator##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,12,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,12,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAOLPTranslator##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,13,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,13,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DELMIAOLPTranslator(classe) \
 \
 \
declare_TIE_DELMIAOLPTranslator(classe) \
 \
 \
CATMetaClass * __stdcall TIEDELMIAOLPTranslator##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DELMIAOLPTranslator,"DELMIAOLPTranslator",DELMIAOLPTranslator::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DELMIAOLPTranslator(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DELMIAOLPTranslator, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDELMIAOLPTranslator##classe(classe::MetaObject(),DELMIAOLPTranslator::MetaObject(),(void *)CreateTIEDELMIAOLPTranslator##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DELMIAOLPTranslator(classe) \
 \
 \
declare_TIE_DELMIAOLPTranslator(classe) \
 \
 \
CATMetaClass * __stdcall TIEDELMIAOLPTranslator##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DELMIAOLPTranslator,"DELMIAOLPTranslator",DELMIAOLPTranslator::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DELMIAOLPTranslator(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DELMIAOLPTranslator, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDELMIAOLPTranslator##classe(classe::MetaObject(),DELMIAOLPTranslator::MetaObject(),(void *)CreateTIEDELMIAOLPTranslator##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DELMIAOLPTranslator(classe) TIE_DELMIAOLPTranslator(classe)
#else
#define BOA_DELMIAOLPTranslator(classe) CATImplementBOA(DELMIAOLPTranslator, classe)
#endif

#endif
