#ifdef LOCAL_DEFINITION_FOR_IID
LINK_WITH_FOR_IID = \
MechanicalModelerUUID \
ObjectSpecsModelerUUID
#else
LINK_WITH_FOR_IID =
#endif
# COPYRIGHT DASSAULT SYSTEMES 2000
#======================================================================
# Imakefile for module CATClnMeasure.m
#======================================================================
#
#  Avr. 2002  Creation: LFI
#======================================================================
#
# SHARED LIBRARY 
#
BUILT_OBJECT_TYPE=SHARED LIBRARY 
 
LINK_WITH=$(LINK_WITH_FOR_IID)   JS0GROUP CATObjectSpecsModeler AC0SPCHECK AC0SPBAS \
             CATClnBase CATClnSpecs CATObjectModelerBase  SpaceAnalysisItf \
             CATSpaMeasure CATViz CATSpaInertia JS0CORBA SB0COMF
