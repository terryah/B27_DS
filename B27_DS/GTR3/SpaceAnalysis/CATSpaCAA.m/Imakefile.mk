#ifdef LOCAL_DEFINITION_FOR_IID
LINK_WITH_FOR_IID = \
CATAfrUUID \
SystemUUID
#else
LINK_WITH_FOR_IID =
#endif

BUILT_OBJECT_TYPE=SHARED LIBRARY
INCLUDED_MODULES = CATSpaAutomation

LINK_WITH=$(LINK_WITH_FOR_IID) \
            JS0GROUP \
            VE0BASE \
            Mathematics \
            CATAdvancedMathematics \
            CATGeometricOperators \
            CATGeometricObjects \
            CATAxisBody \
            O20COLLI \
            SpaceAnalysisItf \
            O20MEASU \
            CATMeasureGeometryInterfaces \  #CATIMeasurable
			AD0XXBAS                     \  #CATIAlias
			MecModItfCPP                 \ 
			SB0COMF                 \ 
            Collections \
            NS0S3STR \
            SimulationItfCPP \
            CATPrdIntegration \
            AC0SPBAS \
            CATProductStructureInterfaces\
            CATNavigatorItf\
            CD0WIN\
            CATInfInterfaces\
			CATSpaSectioning\
			CATViz CATMathStream CATCGMGeoMath \
            YI00IMPL\
			CD0AUTO\
            CATMeasureVisuBase\
			CATScriptReplayInteractions\
			CATSpaInertia\
			CATInteractiveInterfaces\      #CATIBuildPath
            #BODYNOPE\
		  	#CATIAEntity\                   #CATIAStandard.h
            #CATObjectModelerCATIA\         #CATImplementationCATIA.h

# System dependant variables
#
OS = AIX
#
OS = HP-UX
#
OS = IRIX
#
OS = SunOS
#
OS = Windows_NT
