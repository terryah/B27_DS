#ifdef LOCAL_DEFINITION_FOR_IID
LINK_WITH_FOR_IID = \
CATAfrUUID \
NavigatorInterfacesUUID \
ObjectModelerBaseUUID \
SimulationInterfacesUUID
#else
LINK_WITH_FOR_IID =
#endif
#
# SHARED LIBRARY
#
BUILT_OBJECT_TYPE=SHARED LIBRARY
INCLUDED_MODULES = OE0MEASU CATSpaDic

LINK_WITH_BASE=$(LINK_WITH_FOR_IID)  \
            CD0FRAME \
            CD0WIN CD0SHOW\
            CK0FEAT \
            CK0UNIT \
            Collections \
            DI0PANV2 \
            JS0FM \
            JS0GROUP \
            VE0BASE \
            DI0STATE \
            Mathematics \
            O20MEASU \
            O20COLLI \
            YP00IMPL \
            SpecsModeler \
            ObjectModeler \
            CATMechanicalModelerUI \
            SpaceAnalysisItf \
            CATSimulationBase CATSimulationInterfaces \
            CATNewTopologicalObjects \
            Y300IINT \
            CATTopologicalOperators\
            CATProductStructure1\
            CATNavigatorItf \
            CATSketcherInterfaces\
			CATSpaInertia\
			CATMecModInterfaces\
			CATObjectModelerBase\
			CATKnowledgeModeler\
			CATViz CATMathStream KnowledgeItf CATCGMGeoMath \
			CATGraphicProperties \
            AnalysisTools \
			CATNavigator2Itf \
 			YN000FUN\ #CATMathSetOfPoints
			TessAPI\ #CATTesselate
            CATMeasureGeometryInterfaces \  #CATIMeasurable
            SIMItf\   #Simulation #VisuMeasurableServices \
			CATLightXml CAT3DXmlInterfaces CAT3DXmlBaseServices \
			CAT3DXmlLightBaseServices XMLUtils CATBinaryXml XMLParserItf CAT3DXmlInterfacesUUID CATNavigator2Itf CATSPA3DXMLUtilities\
			CATAxisBody \
			MF0GEOM \
			BasicTopology\
			ProcessInterfaces\
			CATPrt \
			CATPartInterfaces \
			AC0XXLNK
					
#ifdef CATIAV5R11
LINK_WITH=$(LINK_WITH_BASE) CATDMUBase CATGitInterfaces CATInteractiveInterfaces
#else
LINK_WITH=$(LINK_WITH_BASE)
#endif

#			ToolsVisu \
#			CATMechanicalModeler\
#			CATBasicTopologicalOpe\
#   		CATDMUModel\
#			CATGitInterfaces\
#			CATMmrAxisSystem\
#           AC0SPBAS \
#       AD0XXBAS \
#       AS0STARTUP \
#       AC0XXLNK \

#    O20COLLI 
#    ObjectModeler
#    SketcherItf 
#    CD0SHOW 
#    VI0REPV4


#DI0PANV2 : Dialog
#DI0STATE : DialogEngine
#SimulationBase : for DMU settings, to be removed when DMU moved to 4DNavBase
#O20COLLI O20MEASU : Interference
#YP00IMPL: CATGeometry (CATLine, CATCurve etc.)
#SketcherItf : CATI2DWFGeometry
#SubGeoLnk : CATVisuSolVertex
#VI0REPV4 : CATISolidComponent

#
OS = AIX

#
OS = HP-UX


#
OS = IRIX

#
OS = SunOS

#
OS = Windows_NT

