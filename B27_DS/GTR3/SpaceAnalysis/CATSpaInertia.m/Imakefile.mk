
#ifdef LOCAL_DEFINITION_FOR_IID
LINK_WITH_FOR_IID = \
InfInterfacesUUID \
MechanicalModelerUUID \
NavigatorInterfacesUUID \
ObjectModelerBaseUUID \
ObjectSpecsModelerUUID \
ProductStructureInterfacesUUID \
SimulationInterfacesUUID \
SystemUUID
#else
LINK_WITH_FOR_IID =
#endif

#
# SHARED LIBRARY
#
BUILT_OBJECT_TYPE=SHARED LIBRARY

LINK_WITH_BASE=$(LINK_WITH_FOR_IID)  \
            CD0FRAME \
            CD0WIN CD0SHOW\
            CK0FEAT \
            Collections \
            DI0PANV2 \
            JS0FM \
            JS0GROUP \
            VE0BASE \
            DI0STATE \
            Mathematics \
            O20MEASU \
            O20COLLI \
            SpecsModeler \
            ObjectModeler \
            SpaceAnalysisItf \
            CATSimulationBase \
            CATSimulationInterfaces \
            CATNewTopologicalObjects \
			CATElfiniServicesItfCPP \
            CATTopologicalOperators\
            CATProductStructure1\
            CATNavigatorItf\
            ProcessInterfaces\
	        CK0PARAM\
			AC0CATPL\
	        CATInteractiveInterfaces\
	        MecModItfCPP\
		  	CATLifRelations\
            CATGitInterfaces\
            CATNavigator2Itf\
            CATMechanicalModelerUI\
		  	CATCdbEntity\                  #CATIAStandard.h
            CATObjectModelerCATIA\         #CATImplementationCATIA.h
            CATMeasureGeometryInterfaces\   #CATIMeasurable
            AC0SPEXT\                      #CATIUpdateProvider
            CATVisualization\              #CATIPathAccess
            OSMInterfacesItfCPP\           #CATINewSceneController
            ProductStructureInterfacesUUID CATProductStructureInterfaces \
			CATViz CATMathStream KnowledgeItf CATGraphicProperties\
			MF0GEOM\
			CATDMUWind\

	        #CATMechanicalModeler\
			#CATMmrAxisSystem\
			#CATKnowledgeModeler\
            #YP00IMPL \
			#CATAxisBody\                   #CATCoordinateSystem 
            #CATGitInterfaces\              #CATIGSMFactory 
		    #CATMatItfCPP\                 #CATIMaterialFeature
			#CATMmrAxisSystem\              #CATIMf3DAxisSystem        
            #CATProductStructureInterfaces\
            #CATInfInterfaces\
		  	#CATIAEntity\                   #CATIAStandard.h

#ifdef CATIAV5R11
LINK_WITH=$(LINK_WITH_BASE) CATDMUBase

#else
LINK_WITH=$(LINK_WITH_BASE)
#endif

#
OS = AIX

#
OS = HP-UX


#
OS = IRIX

#
OS = SunOS

#
OS = Windows_NT

