# COPYRIGHT 2004 DASSAULT SYSTEMES
# Big Jar module which agregates V5 Java class loaders and V5 java starters, for backward compatibility

BUILT_OBJECT_TYPE=PACK
TYPE=COMMON
LINK_WITH=V5JsyLoader V5JsyStarter V5JsyLegacyStarter

PACK_TYPE=BIGJAR


