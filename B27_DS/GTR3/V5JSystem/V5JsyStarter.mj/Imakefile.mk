# COPYRIGHT 2004 DASSAULT SYSTEMES
# Module: V5JsyStarter
# Description: V5 Java starter class

BUILT_OBJECT_TYPE=JAVA

TYPE=CLIENT  # Intentionally made for client application, cannot be TYPE=COMMON or SERVER.

LINK_WITH=V5JsyLoader
