#======================================================================
# COPYRIGHT DASSAULT SYSTEMES PROVENCE 2000
#======================================================================
#----------------------------------------------------#
#--                                                --#
#--  Attention il ne sert a rien, si vous voulez   --#
#-- ajouter un link le mettre dans le Imakefile.mk --#
#--         de CATSdeStepToAssembly.m              --#
#--                                                --#
#----------------------------------------------------#
BUILT_OBJECT_TYPE = NONE

# Compilation conditionelle pour support des step avec 3dxml.
# LOCAL_CCFLAGS = -DIMPEXPCGRTO3DXML
