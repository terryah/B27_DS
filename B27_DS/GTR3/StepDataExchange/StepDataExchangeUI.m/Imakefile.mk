# COPYRIGHT 1999-2001 DASSAULT SYSTEMES PROVENCE
#======================================================================
# Imakefile for module StepDataExchangeUI.m
#======================================================================
#
# Oct 2000  Creation: Code generated by the CAA wizard  yfz
# 15/01/2001: JHH: Suppression des #ifdef CAT..V5Ri i<=4 meme en commentaires (mkCheckSource)
# 23/08/2001: JHH: Suppression des LINK_WITH inutiles
#======================================================================
#
# SHARED LIBRARY 
#
BUILT_OBJECT_TYPE=SHARED LIBRARY 
 
LINK_WITH_COMMON =	JS0FM  \	# System\JS0FM.dll
			JS0INF \	# System\JS0GROUP.dll
			AC0XXLNK \	# ObjectModelerBase\CATObjectModelerBase.dll
			DI0PANV2 \	# Dialog\DI0PANV2.dll
			OM0EDPRO \	# CATIAApplicationFrame\CATIAApplicationFrame.dll for User Settings Repository
			STEPExchangeEnv  KS0SIMPL \
			JS0GROUP CATSdeSettingItfCPP CATSdeSettingInterfacesUUID \
			CK0UNIT KnowledgeItf \ #Pour la mise en place des unitees dans les edit box

LINK_WITH = $(LINK_WITH_COMMON) CATDlgStandard


# System dependant variables
#
OS = AIX
#
OS = HP-UX
#
OS = IRIX
#
OS = SunOS
#
OS = Windows_NT
