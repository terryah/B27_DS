BUILT_OBJECT_TYPE=LOAD MODULE

#ifdef MK_ORBIXCPP33
BUILD=YES

# delivered through an CNext.specifics_hpux
OS=aix_a
PROGRAM_NAME=libITtls.3.3.va50.so

OS=aix_a64
PROGRAM_NAME=libITtls.3.3.va60.so

OS=hpux_b
PROGRAM_NAME=libITtls.3.3.aCC.1

OS=solaris_a
PROGRAM_NAME=libITtls.3.3.forte8.so.1

OS=intel_a
BUILD=NO

OS=win_b64
BUILD=NO

#else
BUILD=NO
#endif
