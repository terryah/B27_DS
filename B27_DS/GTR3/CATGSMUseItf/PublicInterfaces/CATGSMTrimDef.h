/* -*-c++-*- */

#ifndef CATGSMTrimDef_H
#define CATGSMTrimDef_H

// COPYRIGHT DASSAULT SYSTEMES 1999

/**
  * @CAA2Level L1
  * @CAA2Usage U1
  */

/**
 * Types for trim of support.
 * @param CATGSMNoTrim
 *  no trim of support
 * @param CATGSMTrim
 *  trim of support
 * @see CATIGSMBlend#SetTrimSupport
 */ 

enum CATGSMTrimSupportMode {  CATGSMNoTrimSupport = 1 ,
                                     CATGSMTrimSupport };
#endif
