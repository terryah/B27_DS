/* -*-C++-*- */
#ifndef CATGSMMinMax_H
#define CATGSMMinMax_H

// 
// COPYRIGHT DASSAULT SYSTEMES 1999

/**
  * @CAA2Level L1
  * @CAA2Usage U3
  */

/**
 * Types for extremum definition.
 * <b>Role</b>: Defines the different type of extremum 
 * @param GSMMin
 *          minimum type 
 * @param GSMMax
 *          maximum type
 * @see CATIGSMExtremum, CATIGSMFactory
 */

	enum GSMMinMax {GSMMin=0, GSMMax};

#endif
