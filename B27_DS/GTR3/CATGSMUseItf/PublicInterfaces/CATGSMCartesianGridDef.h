/* -*-c++-*- */
#ifndef CATGSMCartesianGridDef_H
#define CATGSMCartesianGridDef_H

// COPYRIGHT DASSAULT SYSTEMES 2009

/**
* @CAA2Level L1
* @CAA2Usage U3
*/

/** 
 * Types for position Label CAT3DCartesianGridLabel. 
 * @param NoLabel 
 *   No Label visible 
 * @param LeftBottom 
 *    Label is positionned always on Left Bottom 
 * @param FullScreen
 *  Label is always visible, depends on the location (Top,Bottom,Left,Right) 
 */
enum CATGSMCartesianGridLabel { NoLabel = 0 , LeftBottom = 1 , FullScreen = 2 };

#endif
