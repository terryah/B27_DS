#ifndef __TIE_DNBIAOrderCreation
#define __TIE_DNBIAOrderCreation

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIAOrderCreation.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIAOrderCreation */
#define declare_TIE_DNBIAOrderCreation(classe) \
 \
 \
class TIEDNBIAOrderCreation##classe : public DNBIAOrderCreation \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIAOrderCreation, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall CreateShopOrder(short & oErrCode); \
      virtual HRESULT __stdcall set_TransactionID(const CATBSTR & iTransactionID); \
      virtual HRESULT __stdcall set_RequestType(const CATBSTR & iRequsetType); \
      virtual HRESULT __stdcall set_DPELoginInfo(const CATBSTR & iDPELoginId, const CATBSTR & iDPEPassword); \
      virtual HRESULT __stdcall set_RequestedNodeDetails(const CATBSTR & iProjID, const CATBSTR & iRequestedNodeID, const CATBSTR & iEffectivity); \
      virtual HRESULT __stdcall set_FTPServerInformation(const CATBSTR & iftpHostName, const CATBSTR & iftpLoginName, const CATBSTR & iftpPassword, const CATBSTR & iftpOutputlocation); \
      virtual HRESULT __stdcall set_ProcessName(const CATBSTR & iprocessName); \
      virtual HRESULT __stdcall set_FilterInputString(const CATBSTR & ifilterInputString); \
      virtual HRESULT __stdcall set_SetEncryptionMode(CAT_VARIANT_BOOL iEncryptmode); \
      virtual HRESULT __stdcall set_GeneratePackNGo(CAT_VARIANT_BOOL iRequired); \
      virtual HRESULT __stdcall set_ExportJobXML(CAT_VARIANT_BOOL iRequired); \
      virtual HRESULT __stdcall set_ExportPrecXML(CAT_VARIANT_BOOL iRequired); \
      virtual HRESULT __stdcall set_UpdatereleaseTable(CAT_VARIANT_BOOL iRequired); \
      virtual HRESULT __stdcall set_ExportImageCapture(CAT_VARIANT_BOOL iRequired); \
      virtual HRESULT __stdcall set_Export3DXML(CAT_VARIANT_BOOL iRequired); \
      virtual HRESULT __stdcall set_Zip(CAT_VARIANT_BOOL iRequired); \
      virtual HRESULT __stdcall set_FTP(CAT_VARIANT_BOOL iRequired); \
      virtual HRESULT __stdcall set_LitePackNGo(CAT_VARIANT_BOOL iRequired); \
      virtual HRESULT __stdcall set_ManufacturingContext(CAT_VARIANT_BOOL iRequired); \
      virtual HRESULT __stdcall set_LoadContext(const CATBSTR & iRequired); \
      virtual HRESULT __stdcall set_LoadPositions(const CATBSTR & iRequired); \
      virtual HRESULT __stdcall set_GenerateSMGXML(CAT_VARIANT_BOOL iRequired); \
      virtual HRESULT __stdcall set_GenerateSMGXmlExePath(const CATBSTR & iRequired); \
      virtual HRESULT __stdcall set_GenerateSMGMappingXML(CAT_VARIANT_BOOL iRequired); \
      virtual HRESULT __stdcall GetErrorDescription(short ErrorCode, CATBSTR & ErrDescription); \
      virtual HRESULT __stdcall CreateShopOrder2(const CATBSTR & iXMLInput); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIAOrderCreation(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall CreateShopOrder(short & oErrCode); \
virtual HRESULT __stdcall set_TransactionID(const CATBSTR & iTransactionID); \
virtual HRESULT __stdcall set_RequestType(const CATBSTR & iRequsetType); \
virtual HRESULT __stdcall set_DPELoginInfo(const CATBSTR & iDPELoginId, const CATBSTR & iDPEPassword); \
virtual HRESULT __stdcall set_RequestedNodeDetails(const CATBSTR & iProjID, const CATBSTR & iRequestedNodeID, const CATBSTR & iEffectivity); \
virtual HRESULT __stdcall set_FTPServerInformation(const CATBSTR & iftpHostName, const CATBSTR & iftpLoginName, const CATBSTR & iftpPassword, const CATBSTR & iftpOutputlocation); \
virtual HRESULT __stdcall set_ProcessName(const CATBSTR & iprocessName); \
virtual HRESULT __stdcall set_FilterInputString(const CATBSTR & ifilterInputString); \
virtual HRESULT __stdcall set_SetEncryptionMode(CAT_VARIANT_BOOL iEncryptmode); \
virtual HRESULT __stdcall set_GeneratePackNGo(CAT_VARIANT_BOOL iRequired); \
virtual HRESULT __stdcall set_ExportJobXML(CAT_VARIANT_BOOL iRequired); \
virtual HRESULT __stdcall set_ExportPrecXML(CAT_VARIANT_BOOL iRequired); \
virtual HRESULT __stdcall set_UpdatereleaseTable(CAT_VARIANT_BOOL iRequired); \
virtual HRESULT __stdcall set_ExportImageCapture(CAT_VARIANT_BOOL iRequired); \
virtual HRESULT __stdcall set_Export3DXML(CAT_VARIANT_BOOL iRequired); \
virtual HRESULT __stdcall set_Zip(CAT_VARIANT_BOOL iRequired); \
virtual HRESULT __stdcall set_FTP(CAT_VARIANT_BOOL iRequired); \
virtual HRESULT __stdcall set_LitePackNGo(CAT_VARIANT_BOOL iRequired); \
virtual HRESULT __stdcall set_ManufacturingContext(CAT_VARIANT_BOOL iRequired); \
virtual HRESULT __stdcall set_LoadContext(const CATBSTR & iRequired); \
virtual HRESULT __stdcall set_LoadPositions(const CATBSTR & iRequired); \
virtual HRESULT __stdcall set_GenerateSMGXML(CAT_VARIANT_BOOL iRequired); \
virtual HRESULT __stdcall set_GenerateSMGXmlExePath(const CATBSTR & iRequired); \
virtual HRESULT __stdcall set_GenerateSMGMappingXML(CAT_VARIANT_BOOL iRequired); \
virtual HRESULT __stdcall GetErrorDescription(short ErrorCode, CATBSTR & ErrDescription); \
virtual HRESULT __stdcall CreateShopOrder2(const CATBSTR & iXMLInput); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIAOrderCreation(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::CreateShopOrder(short & oErrCode) \
{ \
return (ENVTIECALL(DNBIAOrderCreation,ENVTIETypeLetter,ENVTIELetter)CreateShopOrder(oErrCode)); \
} \
HRESULT __stdcall  ENVTIEName::set_TransactionID(const CATBSTR & iTransactionID) \
{ \
return (ENVTIECALL(DNBIAOrderCreation,ENVTIETypeLetter,ENVTIELetter)set_TransactionID(iTransactionID)); \
} \
HRESULT __stdcall  ENVTIEName::set_RequestType(const CATBSTR & iRequsetType) \
{ \
return (ENVTIECALL(DNBIAOrderCreation,ENVTIETypeLetter,ENVTIELetter)set_RequestType(iRequsetType)); \
} \
HRESULT __stdcall  ENVTIEName::set_DPELoginInfo(const CATBSTR & iDPELoginId, const CATBSTR & iDPEPassword) \
{ \
return (ENVTIECALL(DNBIAOrderCreation,ENVTIETypeLetter,ENVTIELetter)set_DPELoginInfo(iDPELoginId,iDPEPassword)); \
} \
HRESULT __stdcall  ENVTIEName::set_RequestedNodeDetails(const CATBSTR & iProjID, const CATBSTR & iRequestedNodeID, const CATBSTR & iEffectivity) \
{ \
return (ENVTIECALL(DNBIAOrderCreation,ENVTIETypeLetter,ENVTIELetter)set_RequestedNodeDetails(iProjID,iRequestedNodeID,iEffectivity)); \
} \
HRESULT __stdcall  ENVTIEName::set_FTPServerInformation(const CATBSTR & iftpHostName, const CATBSTR & iftpLoginName, const CATBSTR & iftpPassword, const CATBSTR & iftpOutputlocation) \
{ \
return (ENVTIECALL(DNBIAOrderCreation,ENVTIETypeLetter,ENVTIELetter)set_FTPServerInformation(iftpHostName,iftpLoginName,iftpPassword,iftpOutputlocation)); \
} \
HRESULT __stdcall  ENVTIEName::set_ProcessName(const CATBSTR & iprocessName) \
{ \
return (ENVTIECALL(DNBIAOrderCreation,ENVTIETypeLetter,ENVTIELetter)set_ProcessName(iprocessName)); \
} \
HRESULT __stdcall  ENVTIEName::set_FilterInputString(const CATBSTR & ifilterInputString) \
{ \
return (ENVTIECALL(DNBIAOrderCreation,ENVTIETypeLetter,ENVTIELetter)set_FilterInputString(ifilterInputString)); \
} \
HRESULT __stdcall  ENVTIEName::set_SetEncryptionMode(CAT_VARIANT_BOOL iEncryptmode) \
{ \
return (ENVTIECALL(DNBIAOrderCreation,ENVTIETypeLetter,ENVTIELetter)set_SetEncryptionMode(iEncryptmode)); \
} \
HRESULT __stdcall  ENVTIEName::set_GeneratePackNGo(CAT_VARIANT_BOOL iRequired) \
{ \
return (ENVTIECALL(DNBIAOrderCreation,ENVTIETypeLetter,ENVTIELetter)set_GeneratePackNGo(iRequired)); \
} \
HRESULT __stdcall  ENVTIEName::set_ExportJobXML(CAT_VARIANT_BOOL iRequired) \
{ \
return (ENVTIECALL(DNBIAOrderCreation,ENVTIETypeLetter,ENVTIELetter)set_ExportJobXML(iRequired)); \
} \
HRESULT __stdcall  ENVTIEName::set_ExportPrecXML(CAT_VARIANT_BOOL iRequired) \
{ \
return (ENVTIECALL(DNBIAOrderCreation,ENVTIETypeLetter,ENVTIELetter)set_ExportPrecXML(iRequired)); \
} \
HRESULT __stdcall  ENVTIEName::set_UpdatereleaseTable(CAT_VARIANT_BOOL iRequired) \
{ \
return (ENVTIECALL(DNBIAOrderCreation,ENVTIETypeLetter,ENVTIELetter)set_UpdatereleaseTable(iRequired)); \
} \
HRESULT __stdcall  ENVTIEName::set_ExportImageCapture(CAT_VARIANT_BOOL iRequired) \
{ \
return (ENVTIECALL(DNBIAOrderCreation,ENVTIETypeLetter,ENVTIELetter)set_ExportImageCapture(iRequired)); \
} \
HRESULT __stdcall  ENVTIEName::set_Export3DXML(CAT_VARIANT_BOOL iRequired) \
{ \
return (ENVTIECALL(DNBIAOrderCreation,ENVTIETypeLetter,ENVTIELetter)set_Export3DXML(iRequired)); \
} \
HRESULT __stdcall  ENVTIEName::set_Zip(CAT_VARIANT_BOOL iRequired) \
{ \
return (ENVTIECALL(DNBIAOrderCreation,ENVTIETypeLetter,ENVTIELetter)set_Zip(iRequired)); \
} \
HRESULT __stdcall  ENVTIEName::set_FTP(CAT_VARIANT_BOOL iRequired) \
{ \
return (ENVTIECALL(DNBIAOrderCreation,ENVTIETypeLetter,ENVTIELetter)set_FTP(iRequired)); \
} \
HRESULT __stdcall  ENVTIEName::set_LitePackNGo(CAT_VARIANT_BOOL iRequired) \
{ \
return (ENVTIECALL(DNBIAOrderCreation,ENVTIETypeLetter,ENVTIELetter)set_LitePackNGo(iRequired)); \
} \
HRESULT __stdcall  ENVTIEName::set_ManufacturingContext(CAT_VARIANT_BOOL iRequired) \
{ \
return (ENVTIECALL(DNBIAOrderCreation,ENVTIETypeLetter,ENVTIELetter)set_ManufacturingContext(iRequired)); \
} \
HRESULT __stdcall  ENVTIEName::set_LoadContext(const CATBSTR & iRequired) \
{ \
return (ENVTIECALL(DNBIAOrderCreation,ENVTIETypeLetter,ENVTIELetter)set_LoadContext(iRequired)); \
} \
HRESULT __stdcall  ENVTIEName::set_LoadPositions(const CATBSTR & iRequired) \
{ \
return (ENVTIECALL(DNBIAOrderCreation,ENVTIETypeLetter,ENVTIELetter)set_LoadPositions(iRequired)); \
} \
HRESULT __stdcall  ENVTIEName::set_GenerateSMGXML(CAT_VARIANT_BOOL iRequired) \
{ \
return (ENVTIECALL(DNBIAOrderCreation,ENVTIETypeLetter,ENVTIELetter)set_GenerateSMGXML(iRequired)); \
} \
HRESULT __stdcall  ENVTIEName::set_GenerateSMGXmlExePath(const CATBSTR & iRequired) \
{ \
return (ENVTIECALL(DNBIAOrderCreation,ENVTIETypeLetter,ENVTIELetter)set_GenerateSMGXmlExePath(iRequired)); \
} \
HRESULT __stdcall  ENVTIEName::set_GenerateSMGMappingXML(CAT_VARIANT_BOOL iRequired) \
{ \
return (ENVTIECALL(DNBIAOrderCreation,ENVTIETypeLetter,ENVTIELetter)set_GenerateSMGMappingXML(iRequired)); \
} \
HRESULT __stdcall  ENVTIEName::GetErrorDescription(short ErrorCode, CATBSTR & ErrDescription) \
{ \
return (ENVTIECALL(DNBIAOrderCreation,ENVTIETypeLetter,ENVTIELetter)GetErrorDescription(ErrorCode,ErrDescription)); \
} \
HRESULT __stdcall  ENVTIEName::CreateShopOrder2(const CATBSTR & iXMLInput) \
{ \
return (ENVTIECALL(DNBIAOrderCreation,ENVTIETypeLetter,ENVTIELetter)CreateShopOrder2(iXMLInput)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIAOrderCreation,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIAOrderCreation,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIAOrderCreation,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIAOrderCreation,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIAOrderCreation,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIAOrderCreation(classe)    TIEDNBIAOrderCreation##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIAOrderCreation(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIAOrderCreation, classe) \
 \
 \
CATImplementTIEMethods(DNBIAOrderCreation, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIAOrderCreation, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIAOrderCreation, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIAOrderCreation, classe) \
 \
HRESULT __stdcall  TIEDNBIAOrderCreation##classe::CreateShopOrder(short & oErrCode) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&oErrCode); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateShopOrder(oErrCode); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&oErrCode); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAOrderCreation##classe::set_TransactionID(const CATBSTR & iTransactionID) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&iTransactionID); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->set_TransactionID(iTransactionID); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&iTransactionID); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAOrderCreation##classe::set_RequestType(const CATBSTR & iRequsetType) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&iRequsetType); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->set_RequestType(iRequsetType); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&iRequsetType); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAOrderCreation##classe::set_DPELoginInfo(const CATBSTR & iDPELoginId, const CATBSTR & iDPEPassword) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&iDPELoginId,&iDPEPassword); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->set_DPELoginInfo(iDPELoginId,iDPEPassword); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&iDPELoginId,&iDPEPassword); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAOrderCreation##classe::set_RequestedNodeDetails(const CATBSTR & iProjID, const CATBSTR & iRequestedNodeID, const CATBSTR & iEffectivity) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&iProjID,&iRequestedNodeID,&iEffectivity); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->set_RequestedNodeDetails(iProjID,iRequestedNodeID,iEffectivity); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&iProjID,&iRequestedNodeID,&iEffectivity); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAOrderCreation##classe::set_FTPServerInformation(const CATBSTR & iftpHostName, const CATBSTR & iftpLoginName, const CATBSTR & iftpPassword, const CATBSTR & iftpOutputlocation) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&iftpHostName,&iftpLoginName,&iftpPassword,&iftpOutputlocation); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->set_FTPServerInformation(iftpHostName,iftpLoginName,iftpPassword,iftpOutputlocation); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&iftpHostName,&iftpLoginName,&iftpPassword,&iftpOutputlocation); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAOrderCreation##classe::set_ProcessName(const CATBSTR & iprocessName) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&iprocessName); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->set_ProcessName(iprocessName); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&iprocessName); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAOrderCreation##classe::set_FilterInputString(const CATBSTR & ifilterInputString) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&ifilterInputString); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->set_FilterInputString(ifilterInputString); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&ifilterInputString); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAOrderCreation##classe::set_SetEncryptionMode(CAT_VARIANT_BOOL iEncryptmode) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&iEncryptmode); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->set_SetEncryptionMode(iEncryptmode); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&iEncryptmode); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAOrderCreation##classe::set_GeneratePackNGo(CAT_VARIANT_BOOL iRequired) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&iRequired); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->set_GeneratePackNGo(iRequired); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&iRequired); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAOrderCreation##classe::set_ExportJobXML(CAT_VARIANT_BOOL iRequired) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,11,&_Trac2,&iRequired); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->set_ExportJobXML(iRequired); \
   ExitAfterCall(this,11,_Trac2,&_ret_arg,&iRequired); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAOrderCreation##classe::set_ExportPrecXML(CAT_VARIANT_BOOL iRequired) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,12,&_Trac2,&iRequired); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->set_ExportPrecXML(iRequired); \
   ExitAfterCall(this,12,_Trac2,&_ret_arg,&iRequired); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAOrderCreation##classe::set_UpdatereleaseTable(CAT_VARIANT_BOOL iRequired) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,13,&_Trac2,&iRequired); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->set_UpdatereleaseTable(iRequired); \
   ExitAfterCall(this,13,_Trac2,&_ret_arg,&iRequired); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAOrderCreation##classe::set_ExportImageCapture(CAT_VARIANT_BOOL iRequired) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,14,&_Trac2,&iRequired); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->set_ExportImageCapture(iRequired); \
   ExitAfterCall(this,14,_Trac2,&_ret_arg,&iRequired); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAOrderCreation##classe::set_Export3DXML(CAT_VARIANT_BOOL iRequired) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,15,&_Trac2,&iRequired); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->set_Export3DXML(iRequired); \
   ExitAfterCall(this,15,_Trac2,&_ret_arg,&iRequired); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAOrderCreation##classe::set_Zip(CAT_VARIANT_BOOL iRequired) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,16,&_Trac2,&iRequired); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->set_Zip(iRequired); \
   ExitAfterCall(this,16,_Trac2,&_ret_arg,&iRequired); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAOrderCreation##classe::set_FTP(CAT_VARIANT_BOOL iRequired) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,17,&_Trac2,&iRequired); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->set_FTP(iRequired); \
   ExitAfterCall(this,17,_Trac2,&_ret_arg,&iRequired); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAOrderCreation##classe::set_LitePackNGo(CAT_VARIANT_BOOL iRequired) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,18,&_Trac2,&iRequired); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->set_LitePackNGo(iRequired); \
   ExitAfterCall(this,18,_Trac2,&_ret_arg,&iRequired); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAOrderCreation##classe::set_ManufacturingContext(CAT_VARIANT_BOOL iRequired) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,19,&_Trac2,&iRequired); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->set_ManufacturingContext(iRequired); \
   ExitAfterCall(this,19,_Trac2,&_ret_arg,&iRequired); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAOrderCreation##classe::set_LoadContext(const CATBSTR & iRequired) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,20,&_Trac2,&iRequired); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->set_LoadContext(iRequired); \
   ExitAfterCall(this,20,_Trac2,&_ret_arg,&iRequired); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAOrderCreation##classe::set_LoadPositions(const CATBSTR & iRequired) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,21,&_Trac2,&iRequired); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->set_LoadPositions(iRequired); \
   ExitAfterCall(this,21,_Trac2,&_ret_arg,&iRequired); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAOrderCreation##classe::set_GenerateSMGXML(CAT_VARIANT_BOOL iRequired) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,22,&_Trac2,&iRequired); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->set_GenerateSMGXML(iRequired); \
   ExitAfterCall(this,22,_Trac2,&_ret_arg,&iRequired); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAOrderCreation##classe::set_GenerateSMGXmlExePath(const CATBSTR & iRequired) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,23,&_Trac2,&iRequired); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->set_GenerateSMGXmlExePath(iRequired); \
   ExitAfterCall(this,23,_Trac2,&_ret_arg,&iRequired); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAOrderCreation##classe::set_GenerateSMGMappingXML(CAT_VARIANT_BOOL iRequired) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,24,&_Trac2,&iRequired); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->set_GenerateSMGMappingXML(iRequired); \
   ExitAfterCall(this,24,_Trac2,&_ret_arg,&iRequired); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAOrderCreation##classe::GetErrorDescription(short ErrorCode, CATBSTR & ErrDescription) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,25,&_Trac2,&ErrorCode,&ErrDescription); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetErrorDescription(ErrorCode,ErrDescription); \
   ExitAfterCall(this,25,_Trac2,&_ret_arg,&ErrorCode,&ErrDescription); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAOrderCreation##classe::CreateShopOrder2(const CATBSTR & iXMLInput) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,26,&_Trac2,&iXMLInput); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateShopOrder2(iXMLInput); \
   ExitAfterCall(this,26,_Trac2,&_ret_arg,&iXMLInput); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAOrderCreation##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,27,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,27,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAOrderCreation##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,28,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,28,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAOrderCreation##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,29,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,29,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAOrderCreation##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,30,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,30,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAOrderCreation##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,31,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,31,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIAOrderCreation(classe) \
 \
 \
declare_TIE_DNBIAOrderCreation(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAOrderCreation##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAOrderCreation,"DNBIAOrderCreation",DNBIAOrderCreation::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAOrderCreation(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIAOrderCreation, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAOrderCreation##classe(classe::MetaObject(),DNBIAOrderCreation::MetaObject(),(void *)CreateTIEDNBIAOrderCreation##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIAOrderCreation(classe) \
 \
 \
declare_TIE_DNBIAOrderCreation(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAOrderCreation##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAOrderCreation,"DNBIAOrderCreation",DNBIAOrderCreation::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAOrderCreation(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIAOrderCreation, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAOrderCreation##classe(classe::MetaObject(),DNBIAOrderCreation::MetaObject(),(void *)CreateTIEDNBIAOrderCreation##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIAOrderCreation(classe) TIE_DNBIAOrderCreation(classe)
#else
#define BOA_DNBIAOrderCreation(classe) CATImplementBOA(DNBIAOrderCreation, classe)
#endif

#endif
