#
#   Imakefile.mk for DNBSORItf.m
#   Copyright (C) DASSAULT SYSTEMES, 2002
#

BUILT_OBJECT_TYPE = SHARED LIBRARY

INCLUDED_MODULES =  DNBSORItfCPP \  
					DNBSORPubIDL \
                    DNBSORProIDL \

COMMON_LINK_WITH =  JS0GROUP                  \ # System
                    JS0CORBA                  \ # System
                    JS0FM                     \
                    JS0GROUP                  \
                    DNBSORInterfacesUUID      \

LINK_WITH = $(COMMON_LINK_WITH)

#
# Define the build options for the current module.
#
OS      = Windows_NT
BUILD   = YES

OS      = IRIX
BUILD   = NO

OS      = SunOS
BUILD   = NO

OS      = AIX
BUILD   = NO

OS      = HP-UX
BUILD   = NO

OS      = win_a
BUILD   = NO
