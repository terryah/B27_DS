//COPYRIGHT DASSAULT SYSTEMES 2007
/** @CAA2Required */
/*---------------------------------------------------------------------*/
/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */
/*---------------------------------------------------------------------*/

#ifdef  _WINDOWS_SOURCE
#ifdef  __DNBSORItfCPP
#define ExportedByDNBSORItfCPP        __declspec(dllexport)
#else
#define ExportedByDNBSORItfCPP        __declspec(dllimport)
#endif
#else
#define ExportedByDNBSORItfCPP
#endif
