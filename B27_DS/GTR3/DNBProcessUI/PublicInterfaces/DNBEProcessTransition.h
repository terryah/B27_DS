/**
* @CAA2Level L0
* @CAA2Usage U0
*/
#ifndef DNBEProcessTransition_H
#define DNBEProcessTransition_H

/* -*-c++-*- */
//======================================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2001
//======================================================================================
//
//  Class DNBEProcessTransition.h:
//    Provide implementation to interface CATIWorkbenchTransition
//
//======================================================================================
//  Usage:
//      This class may be used as follows...
//
//  Modification History:
//     
//      cre       sha   02/05/2002          original implementation
//      mod     sha     02/12/2002      add init()/dispose() for enabling PPR
//                                      Hub commands
//      mod     sra     07/16/2003      Add method to Activate program & 
//                                      IO related extensions
//======================================================================================

#include <DNBProcessUI.h>
#include <CATEProcessTransition.h>

class CATICompoundContainer_var;

class ExportedByDNBProcessUI DNBEProcessTransition: public CATEProcessTransition
{
public:
    
    // -----------------------------------------------------------------
    // Standard constructors and destructors for an implementation class
    // -----------------------------------------------------------------
/**
 * @nodoc
 */
    DNBEProcessTransition ();
/**
 * @nodoc
 */
    virtual ~DNBEProcessTransition ();
/**
 * @nodoc
 */

    virtual void Init();
/**
 * @nodoc
 */
    virtual void Dispose();
/**
 * @nodoc
 */
    
protected:
    void AppendLibraries(const CATListOfCATString& libList);

    void ActivateExtensions( const CATICompoundContainer_var& spICompCont, 
                             const CATUnicodeString& extName );
private:

    // -------------------------------------------------------------------
    // The copy constructor and the equal operator must not be implemented
    // -------------------------------------------------------------------
    DNBEProcessTransition (DNBEProcessTransition &);
    DNBEProcessTransition& operator=(DNBEProcessTransition&);
    
};

#endif // DNBEProcessTransition_H
