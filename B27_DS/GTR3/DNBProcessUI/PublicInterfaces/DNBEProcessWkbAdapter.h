/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBEProcessWkbAdapter.h
//      Process based workbench adapter.
//
//==============================================================================
//
// Usage notes: 
//      Derive your product workbench (for process based products only) from
//      this adapter.
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     sha          10/10/2001   Initial implementation
//     sha          11/08/2001   Changed all methods to be non-virtual
//     sha          04/23/2002   Fix MLK errors in the common workbench infr
//                               and update the comments
//     sha          07/03/2002   Modify RemoveCmdHdr() method and add new method
//                               LocateCmdHdr()
//     sha          01/31/2003   Fix IR 375355: RemoveSep() method does nothing
//     sha          11/12/2003   Fix IR 418298: fix for shareable products
//
//==============================================================================
#ifndef DNBEProcessWkbAdapter_H
#define DNBEProcessWkbAdapter_H

#include <CATString.h>
#include <DNBECommonWkbAdapter.h>
#include <DNBProcessUI.h>

class DNBAccess;
class DNBCommandHeader;
class DNBWorkbench;

class CATCmdWorkbench;

//------------------------------------------------------------------------------

class ExportedByDNBProcessUI DNBEProcessWkbAdapter : public DNBECommonWkbAdapter
{
    CATDeclareClass;

public:

    // Standard constructors and destructors for an implementation class
    // -----------------------------------------------------------------
/**
 * @nodoc
 */
    DNBEProcessWkbAdapter( const CATString &str = "", const int level = 2 );
/**
 * @nodoc
 */
    virtual ~DNBEProcessWkbAdapter();

/**
 * @nodoc
 */
    void GetCustomInterfaces(CATListPV* defaultIIDList,
                             CATListPV* customIIDList);

protected:

    void SetupCrtWorkbench();
    void ValidateUILevel();

private:
    // The copy constructor and the equal operator must not be implemented
    // -------------------------------------------------------------------
    DNBEProcessWkbAdapter( DNBEProcessWkbAdapter& );
    DNBEProcessWkbAdapter& operator=( DNBEProcessWkbAdapter& );

    int     _localLevel;
};

//------------------------------------------------------------------------------

#endif // DNBEProcessWkbAdapter_H
