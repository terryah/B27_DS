//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/** @CAA2Required */
/*---------------------------------------------------------------------*/
/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */
/*---------------------------------------------------------------------*/
//
//    This Header file is included by all the Module.h header files in 
//    PublicInterfaces
//

#ifndef DNBPROCESSUI_H

#define DNBPROCESSUI_H DNBProcessUI

#ifdef _WINDOWS_SOURCE
#if defined(__DNBProcessUI)
#define ExportedByDNBProcessUI __declspec(dllexport)
#else
#define ExportedByDNBProcessUI __declspec(dllimport)
#endif
#else
#define ExportedByDNBProcessUI
#endif

#endif /* DNBPROCESSUI_H */
