#
#   Imakefile.mk for DNBProcessUI
#   Copyright (C) DELMIA Corp., 2002
#
BUILT_OBJECT_TYPE   = SHARED LIBRARY

COMMON_LINK_WITH    = JS0GROUP                  \ # System
                      JS0FM                     \ # System
                      CATVisualization          \ # Visualization
                      CATViz                    \ # VisualizationBase
                      CATPrdIntegration         \ # ProductStructure
                      CATApplicationFrame       \ # ApplicationFrame
                      CATObjectModelerBase      \ # ObjectModelerBase
                      AC0SPEXT                  \ # ObjectSpecsModeler
                      AC0SPBAS                  \ # ObjectSpecsModeler
                      CATMathematics            \ # Mathematics
                      CATMathStream             \ # Mathematics
                      ProcessModelerBase        \ # ProcessModelerVisu
                      ProcessModelerVisu        \ # ProcessModelerVisu
                      CATProcessInterfaces      \ # DMAPSInterfaces
                      DNBStateItf               \ # DNBStateInterfaces
                      DNBState                  \ # DNBStateModeler
                      DNBProductUI              \ # DNBProductUI
                      DNBProcessInterfaces      \ # DNBProcessInterfaces
                      DNBProcessBase            \ # DNBProcessBase
                      DNBIPDHubEnableServices   \ # LOCAL
                      DNBResourceBehaviorItf    \ # DNBResourceBehaviorInterfaces
                      SELECT                    \ # CATIAApplicationFrame
                      CATArrangementItf         \ # CATArrangementInterfaces
                      ArrUtility                \ # CATArrangementUI
                      ResourceItf				\ # DNBResourceBehaviorInterfaces

LINK_WITH           = $(COMMON_LINK_WITH)

INCLUDED_MODULES    = DNBStateNavigVisu

ALIASES_ON_IMPORT=DNBProcessUI DNBProductUI

#
# Define the build options for the current module.
#
OS      = Windows_NT
BUILD   = YES

OS      = IRIX
BUILD   = YES

OS      = SunOS
BUILD   = YES

OS      = AIX
BUILD   = YES

OS      = HP-UX
BUILD   = YES

OS      = win_a
BUILD   = NO
