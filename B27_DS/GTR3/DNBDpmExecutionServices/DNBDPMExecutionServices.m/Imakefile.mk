# COPYRIGHT DASSAULT SYSTEMES 2001
#==============================================================================
# Imakefile for module DNBDPMExecutionServices
#==============================================================================
#
#  Feb 2001  Creation:                                                      PAR
#  Oct 2001  Added dpmgantt and timsolver modules as include                PAR
#  Feb 2001	 Added CATVisualization for linking								KDR
#==============================================================================
#
# SHARED LIBRARY 
#
BUILT_OBJECT_TYPE=SHARED LIBRARY

INCLUDED_MODULES= DNBTimeSolver DNBDpmNotificatons

# DO NOT EDIT :: THE CAA2 WIZARDS WILL ADD CODE HERE
WIZARD_LINK_MODULES =  \
# END WIZARD EDITION ZONE

LINK_WITH =    JS0FM \
               JS0GROUP  \
			   DNBDPMItf \
			   AD0XXBAS  \
			   DI0PANV2 \
			   CATApplicationFrame \
			   DNBResourceProgramItf \
			   CATVisualization

OS = Windows_NT
#LOCAL_CCFLAGS=-FR


