#
#   Imakefile.mk for DNBAnalysisSettings
#   Copyright (C) DELMIA Corp., 2003
#
BUILT_OBJECT_TYPE   = SHARED LIBRARY

# DO NOT EDIT :: THE CAA2 WIZARDS WILL ADD CODE HERE
WIZARD_LINK_MODULES = \
JS0GROUP CATInfInterfaces DNBSimulationPubIDL DNBSimulationInterfacesUUID  \
DNBSimulationItfCPP 
# END WIZARD EDITION ZONE

LINK_WITH           = $(WIZARD_LINK_MODULES)    \
                      JS0FM                     \ # System
                      OM0EDPRO                  \ # CATIAApplicationFrame
                      AD0XXBAS                  \ # ObjectModelerBase
                      DI0PANV2                  \ # Dialog
                      CATDlgStandard            \ # Dialog

# System dependant variables
#
OS = AIX
#
OS = HP-UX
#
OS = IRIX
#
OS = SunOS
#
OS = Windows_NT
