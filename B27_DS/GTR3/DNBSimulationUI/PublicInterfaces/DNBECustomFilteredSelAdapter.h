/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2003
//==============================================================================
//
// DNBECustomFilteredSelAdapter.h
//      Adapter that implements methods from CATICustomFilteredSelection 
//      interface.
//
//==============================================================================
//
// Usage notes: 
//      Use this adapter as a base class for your implementation of the 
//      CATICustomFilteredSelection interface for WorkbenchName+"_selection".
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     mmh          04/01/2003   Initial implementation
//     mmh          06/09/2003   Make data member protected
//
//==============================================================================
#ifndef DNB_E_CUSTOM_FILTERED_SEL_ADAPTER_H
#define DNB_E_CUSTOM_FILTERED_SEL_ADAPTER_H

#include <CATBaseUnknown.h>
#include <DNBSimulationUI.h>

class CATPathElement;

/**
 * Adapter that implements methods from CATICustomFilteredSelection interface.
 */
class ExportedByDNBSimulationUI DNBECustomFilteredSelAdapter : 
                                                        public CATBaseUnknown
{
    CATDeclareClass;
    
public:
/**
 * @nodoc
 */
    DNBECustomFilteredSelAdapter();
/**
 * @nodoc
 */
    ~DNBECustomFilteredSelAdapter();
    
/**
 * @nodoc
 */
	virtual CATPathElement* PrecheckPath( CATPathElement *PathToCheck );

/**
 * @nodoc
 */
    virtual CATPathElement* CheckPath( CATPathElement *PathToCheck,
                                            int ElementTypeListPosition );
    
protected:
    CATPathElement          *_originalPath;
};

#endif      // DNB_E_CUSTOM_FILTERED_SEL_ADAPTER_H

