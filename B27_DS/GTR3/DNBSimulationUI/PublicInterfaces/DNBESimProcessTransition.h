/**
* @CAA2Level L0
* @CAA2Usage U0
*/
#ifndef DNBESimProcessTransition_H
#define DNBESimProcessTransition_H

/* -*-c++-*- */
//======================================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2001
//======================================================================================
//
//  Class DNBESimProcessTransition.h:
//    Provide implementation to interface CATIWorkbenchTransition
//
//======================================================================================
//  Usage:
//      This class may be used as follows...
//
//  Modification History:
//     
//      cre       sha   02/05/2002          original implementation
//      mod       mmh   06/07/2002          implement Init, Dispose methods
//======================================================================================

#include <DNBSimulationUI.h>
#include <DNBEProcessTransition.h>

class ExportedByDNBSimulationUI DNBESimProcessTransition: public DNBEProcessTransition
{
public:
    
    // -----------------------------------------------------------------
    // Standard constructors and destructors for an implementation class
    // -----------------------------------------------------------------

/**
 * @nodoc
*/
    DNBESimProcessTransition ();

/**
 * @nodoc
 */
    virtual ~DNBESimProcessTransition ();
    
 /**
 * @nodoc
 */
	virtual void Init();

/**
 * @nodoc
 */
    virtual void Dispose();

private:
    // -------------------------------------------------------------------
    // The copy constructor and the equal operator must not be implemented
    // -------------------------------------------------------------------
    DNBESimProcessTransition (DNBESimProcessTransition &);
    DNBESimProcessTransition& operator=(DNBESimProcessTransition&);
    
};

#endif // DNBESimProcessTransition_H
