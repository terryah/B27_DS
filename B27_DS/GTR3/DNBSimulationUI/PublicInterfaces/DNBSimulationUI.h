//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/** @CAA2Required */
/*---------------------------------------------------------------------*/
/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */
/*---------------------------------------------------------------------*/
//
//    This Header file is included by all the Module.h header files in 
//    PublicInterfaces
//

#ifndef DNBSIMULATIONUI_H

#define DNBSIMULATIONUI_H DNBSimulationUI

#ifdef _WINDOWS_SOURCE
#if defined(__DNBSimulationUI)
#define ExportedByDNBSimulationUI __declspec(dllexport)
#else
#define ExportedByDNBSimulationUI __declspec(dllimport)
#endif
#else
#define ExportedByDNBSimulationUI
#endif

#endif /* DNBSIMULATIONUI_H */
