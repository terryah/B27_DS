/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBESimProcessWkbAdapter.h
//      Process simulation based workbench adapter.
//
//==============================================================================
//
// Usage notes: 
//      Derive your product workbench (for process based products only) from
//      this adapter.
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     sha          10/10/2001   Initial implementation
//     sha          11/08/2001   Changed all methods to be non-virtual
//
//==============================================================================
#ifndef DNBESimProcessWkbAdapter_H
#define DNBESimProcessWkbAdapter_H

#include <CATBaseUnknown.h>
#include <CATListPV.h>
#include <CATString.h>
#include <CATCmdContainer.h>
#include <DNBSimulationUI.h>

#include <DNBEProcessWkbAdapter.h>

class CATCmdWorkbench;

//------------------------------------------------------------------------------

class ExportedByDNBSimulationUI DNBESimProcessWkbAdapter : 
    public DNBEProcessWkbAdapter
{
    CATDeclareClass;

public:

    // Standard constructors and destructors for an implementation class
    // -----------------------------------------------------------------
/**
 * @nodoc
 */
    DNBESimProcessWkbAdapter( const CATString &str = "", const int level = 2 );

/**
 * @nodoc
 */
    virtual ~DNBESimProcessWkbAdapter();

/**
 * @nodoc
 */
	void GetCustomInterfaces(CATListPV* defaultIIDList,
                             CATListPV* customIIDList);

protected:

    void SetupCrtWorkbench();
    void ValidateUILevel();

private:
    // The copy constructor and the equal operator must not be implemented
    // -------------------------------------------------------------------
    DNBESimProcessWkbAdapter( DNBESimProcessWkbAdapter& );
    DNBESimProcessWkbAdapter& operator=( DNBESimProcessWkbAdapter& );

    int     _localLevel;
};

//------------------------------------------------------------------------------

#endif // DNBESimProcessWkbAdapter_H
