#
#   Imakefile.mk for DNBSimulationUI
#   Copyright (C) DELMIA Corp., 2001
#
BUILT_OBJECT_TYPE   = SHARED LIBRARY

LINK_WITH           = JS0GROUP                  \ # System
                      JS0FM                     \ # System
                      CATApplicationFrame       \ # ApplicationFrame
                      SELECT                    \ # CATIAApplicationFrame
                      CATMathematics            \ # Mathematics
                      CATMathStream             \ # Mathematics
                      ProcessModelerVisu        \ # ProcessPlatformVisu
                      CATProductStructure1      \ # ProductStructure
                      CATPrsWksPRDWorkshop      \ # ProductStructureUI
                      CATObjectModelerBase      \ # ObjectModelerBase
                      CATArrangementItf         \ # CATArrangementInterfaces
                      ArrUIInterfaces           \ # CATArrangementUI
                      CATArrWkbConfiguration    \ # CATArrangementUI
                      ArrUtility                \ # CATArrangementUI
                      CATDMUWorkBench           \ # Navigator4DUI
                      CATViz                    \ # VisualizationBase
                      CATVisualization          \ # Visualization
                      ProcessInterfaces         \ # DMAPSInterfaces
                      DNBSimulationItf          \ # DNBSimulationInterfaces
                      DNBDeviceItf              \ # DNBDeviceInterfaces
                      DNBProductUI              \ # DNBProductUI
                      DNBProcessUI              \ # DNBProcessUI
                      DNBProcessBase            \ # DNBProcessBase
                      DNBProcessInterfaces      \ # DNBProcessInterfaces
                      DNBInfrastructure         \ # DNBInfrastructure


INCLUDED_MODULES    = 

#
# Define the build options for the current module.
#
OS      = Windows_NT
BUILD   = YES

OS      = IRIX
BUILD   = YES

OS      = SunOS
BUILD   = YES

OS      = AIX
BUILD   = YES

OS      = HP-UX
BUILD   = YES

OS      = win_a
BUILD   = NO

