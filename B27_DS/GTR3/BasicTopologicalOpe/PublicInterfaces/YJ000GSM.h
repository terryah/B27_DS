#include "CATIACGMLevel.h"
#ifdef CATIACGMR420CAA
#ifdef	__YJ000GSM


// COPYRIGHT DASSAULT SYSTEMES 1999

/** @required */

/*---------------------------------------------------------------------*/

/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */

/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */

/*---------------------------------------------------------------------*/
#define ExportedByYJ000GSM DSYExport
#else
#define ExportedByYJ000GSM DSYImport
#endif
#include "DSYExport.h"
#else
#ifdef	_WINDOWS_SOURCE
#ifdef	__YJ000GSM
// COPYRIGHT DASSAULT SYSTEMES 1999
/** @CAA2Required */
/*---------------------------------------------------------------------*/
/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */
/*---------------------------------------------------------------------*/
#define	ExportedByYJ000GSM	__declspec(dllexport)
#else
#define	ExportedByYJ000GSM	__declspec(dllimport)
#endif
#else
#define	ExportedByYJ000GSM
#endif
#endif
