#include "CATIACGMLevel.h"
#ifdef CATIACGMR420CAA
#ifdef	__BasicTopology


// COPYRIGHT DASSAULT SYSTEMES 1999

/** @required */

/*---------------------------------------------------------------------*/

/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */

/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */

/*---------------------------------------------------------------------*/
#define ExportedByBasicTopology DSYExport
#else
#define ExportedByBasicTopology DSYImport
#endif
#include "DSYExport.h"
#else
#ifdef	_WINDOWS_SOURCE
#ifdef	__BasicTopology
// COPYRIGHT DASSAULT SYSTEMES 1999
/** @CAA2Required */
/*---------------------------------------------------------------------*/
/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */
/*---------------------------------------------------------------------*/
#define	ExportedByBasicTopology	__declspec(dllexport)
#else
#define	ExportedByBasicTopology	__declspec(dllimport)
#endif
#else
#define	ExportedByBasicTopology
#endif
#endif
