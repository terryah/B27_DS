// COPYRIGHT Dassault Systemes 2003
//===================================================================
//
// CATIStmFlangedHole.h
// Define the CATIStmFlangedHole interface
//
//===================================================================
//
// Usage notes:
//   New interface: describe its use here
//
//===================================================================
//
//  Sep 2003  Creation: Code generated by the CAA wizard  JHI
//===================================================================
#ifndef CATIStmFlangedHole_H
#define CATIStmFlangedHole_H

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

// CATSmInterfaces Module :
#include "ExportedByCATSmInterfaces.h"
#include "CATBaseUnknown.h"
#include "CATMathPoint.h"
#include "CATMathDirection.h"
#include "CATICkeParm.h"
#include "CATISpecObject.h"


#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByCATSmInterfaces IID IID_CATIStmFlangedHole;
#else
extern "C" const IID IID_CATIStmFlangedHole ;
#endif

//------------------------------------------------------------------

/**
 * Interface dedicated to Sheet Metal Application.
 * <b>Role</b>: Objects that implement this interface is a FlangedHole.
 */
class ExportedByCATSmInterfaces CATIStmFlangedHole : public CATBaseUnknown
{
    CATDeclareInterface;

public:

  	/**
     * Retrieves the Center point of the Flanged Hole.
     *
     * @param oCenterPoint
     *   CATMathPoint.
     *
     */    
	virtual HRESULT GetCenterPoint(CATMathPoint & oCenterPoint)=0;	

	/**
	 * Retrieves the Stamping direction.
     *
     * @param oMathDirection
     *   CATMathDirection.
     *
     */    
	virtual HRESULT GetDirection(CATMathDirection & oDirection)=0; 

	/**
	 * Retrieves the Radius of the Flanged Hole
     *
     * @param oRadius
     *   CATICkeParm.
     *
     */    
	virtual HRESULT GetRadius(CATICkeParm_var & oRadius)=0;

	/**
	 * Retrieves the Angle of the Flanged Hole
     *
     * @param oAngle
     *   CATICkeParm.
     *
     */    
	virtual HRESULT GetAngle(CATICkeParm_var & oAngle)=0;

	/**
	 * Retrieves the inner smallest diameter of the Flanged Hole
     *
     * @param oDiameter
     *   CATICkeParm.
     *
     */    
	virtual HRESULT GetDiameter(CATICkeParm_var & oDiameter)=0;

	/**
	 * Retrieves the Height of the Flanged Hole
     *
     * @param oHeight
     *   CATICkeParm.
     *
     */    
	virtual HRESULT GetHeight(CATICkeParm_var & oHeight)=0;

    /**
    * Retrieves the characteristic curves of the Flanged Hole.
    * @param iView : view
    *   <br><tt>1</tt> Folded
    *   <br><tt>2</tt> Flattened
    * @param iCurve : type of characteristic curve
    *   <br><tt>1</tt> OML
    *   <br><tt>2</tt> IML (FBL)
    *   <br><tt>4</tt> BTL Base Feature
    * @param hCharacteristicCurve
    *
    */ 
	virtual HRESULT GetCharacteristicCurve (const int & iView,
                		                    const int & iCurve,
											CATISpecObject_var & hCharacteristicCurve) = 0;



};

CATDeclareHandler( CATIStmFlangedHole, CATBaseUnknown );

//------------------------------------------------------------------

#endif
