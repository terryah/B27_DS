// COPYRIGHT Dassault Systemes 2003
//===================================================================
//
// CATIStmBead.h
// Define the CATIStmBead interface
//
//===================================================================
//
// Usage notes:
//   New interface: describe its use here
//
//===================================================================
//
//  Sep 2003  Creation: Code generated by the CAA wizard  JHI
//===================================================================
#ifndef CATIStmBead_H
#define CATIStmBead_H

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

// CATSmInterfaces Module :
#include "ExportedByCATSmInterfaces.h"
#include "CATBaseUnknown.h"
#include "CATICkeParm.h"
#include "CATMathDirection.h"

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByCATSmInterfaces IID IID_CATIStmBead;
#else
extern "C" const IID IID_CATIStmBead ;
#endif

//------------------------------------------------------------------

/**
 * Interface dedicated to Sheet Metal Application.
 * <b>Role</b>: Objects that implement this interface is a Flanged Hole.
 */
class ExportedByCATSmInterfaces CATIStmBead : public CATBaseUnknown
{
    CATDeclareInterface;


	
	/**
	 * Retrieves the center profile of the Bead
     *
     * @param oProfile
     *   CATISpecObject.
     *
     */    
	virtual HRESULT GetProfile(CATISpecObject_var & oProfile)=0;

	
	/**
	 * Retrieves the Stamping direction.
     *
     * @param oMathDirection
     *   CATMathDirection.
     *
     */    
	virtual HRESULT GetDirection(CATMathDirection & oDirection)=0; 

	/**
	 * Retrieves the Fillet Radius of the Bead
     *
     * @param oFilletRadius
     *   CATICkeParm.
     *
     */    
	virtual HRESULT GetFilletRadius(CATICkeParm_var & oFilletRadius)=0;


	/**
	 * Retrieves the Section Radius of the Bead
     *
     * @param oSectionRadius
     *   CATICkeParm.
     *
     */    
	virtual HRESULT GetCrossSectionRadius(CATICkeParm_var & oSectionRadius)=0;

	/**
	 * Retrieves the End Radius of the Bead
     *
     * @param oEndRadius
     *   CATICkeParm.
     *
     */    
	virtual HRESULT GetEndRadius(CATICkeParm_var & oEndRadius)=0;

	/**
	 * Retrieves the Height of the Bead
     *
     * @param oHeight
     *   CATICkeParm.
     *
     */    
	virtual HRESULT GetHeight(CATICkeParm_var & oHeight)=0;


    /**
    * Retrieves the characteristic curves of the Bead.
    * @param iView : view
    *   <br><tt>1</tt> Folded
    *   <br><tt>2</tt> Flattened
    * @param iCurve : type of characteristic curve
    *   <br><tt>4</tt> BTL Base Feature
    * @param hCharacteristicCurve
    *
    */ 
	virtual HRESULT GetCharacteristicCurve (const int & iView,
                		                    const int & iCurve,
											CATISpecObject_var & hCharacteristicCurve) = 0;

public:

};

CATDeclareHandler( CATIStmBead, CATBaseUnknown );

//------------------------------------------------------------------

#endif
