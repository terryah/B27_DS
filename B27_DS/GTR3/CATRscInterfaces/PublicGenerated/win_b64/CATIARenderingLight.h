/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIARenderingLight_h
#define CATIARenderingLight_h

#ifndef ExportedByCATRscPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __CATRscPubIDL
#define ExportedByCATRscPubIDL __declspec(dllexport)
#else
#define ExportedByCATRscPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByCATRscPubIDL
#endif
#endif

#include "CATIABase.h"
#include "CATSafeArray.h"

extern ExportedByCATRscPubIDL IID IID_CATIARenderingLight;

class ExportedByCATRscPubIDL CATIARenderingLight : public CATIABase
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_Type(short & oType)=0;

    virtual HRESULT __stdcall put_Type(short iType)=0;

    virtual HRESULT __stdcall get_Mode(short & oMode)=0;

    virtual HRESULT __stdcall put_Mode(short iMode)=0;

    virtual HRESULT __stdcall GetOrigin(CATSafeArrayVariant & oOrigin)=0;

    virtual HRESULT __stdcall PutOrigin(const CATSafeArrayVariant & iOrigin)=0;

    virtual HRESULT __stdcall GetTarget(CATSafeArrayVariant & oTarget)=0;

    virtual HRESULT __stdcall PutTarget(const CATSafeArrayVariant & iTarget)=0;

    virtual HRESULT __stdcall GetColor(CATSafeArrayVariant & oColor)=0;

    virtual HRESULT __stdcall PutColor(const CATSafeArrayVariant & iColor)=0;

    virtual HRESULT __stdcall get_Intensity(double & oIntensity)=0;

    virtual HRESULT __stdcall put_Intensity(double iIntensity)=0;

    virtual HRESULT __stdcall get_Ambient(double & oAmbient)=0;

    virtual HRESULT __stdcall put_Ambient(double iAmbient)=0;

    virtual HRESULT __stdcall get_Diffuse(double & oDiffuse)=0;

    virtual HRESULT __stdcall put_Diffuse(double iDiffuse)=0;

    virtual HRESULT __stdcall get_Specular(double & oSpecular)=0;

    virtual HRESULT __stdcall put_Specular(double iSpecular)=0;

    virtual HRESULT __stdcall get_Angle(double & oAngle)=0;

    virtual HRESULT __stdcall put_Angle(double iAngle)=0;

    virtual HRESULT __stdcall get_EndDistance(double & oEndDistance)=0;

    virtual HRESULT __stdcall put_EndDistance(double iEndDistance)=0;

    virtual HRESULT __stdcall get_AttenuationStartRatio(double & oAttenuationStartRatio)=0;

    virtual HRESULT __stdcall put_AttenuationStartRatio(double iAttenuationStartRatio)=0;

    virtual HRESULT __stdcall get_AttenuationAngleRatio(double & oAttenuationAngleRatio)=0;

    virtual HRESULT __stdcall put_AttenuationAngleRatio(double iAttenuationAngleRatio)=0;

    virtual HRESULT __stdcall get_ShadowStatus(short & oShadowStatus)=0;

    virtual HRESULT __stdcall put_ShadowStatus(short iShadowStatus)=0;

    virtual HRESULT __stdcall get_HardwareShadowStatus(short & oHardwareShadowStatus)=0;

    virtual HRESULT __stdcall put_HardwareShadowStatus(short iHardwareShadowStatus)=0;

    virtual HRESULT __stdcall get_HardwareShadowSmoothing(short & oHardwareShadowSmoothing)=0;

    virtual HRESULT __stdcall put_HardwareShadowSmoothing(short iHardwareShadowSmoothing)=0;

    virtual HRESULT __stdcall GetShadowColor(CATSafeArrayVariant & oShadowColor)=0;

    virtual HRESULT __stdcall PutShadowColor(const CATSafeArrayVariant & iShadowColor)=0;

    virtual HRESULT __stdcall get_HardwareShadowTransparency(double & oHardwareShadowTransparency)=0;

    virtual HRESULT __stdcall put_HardwareShadowTransparency(double iHardwareShadowTransparency)=0;

    virtual HRESULT __stdcall get_ShadowObjectStatus(short & oShadowObjectStatus)=0;

    virtual HRESULT __stdcall put_ShadowObjectStatus(short iShadowObjectStatus)=0;

    virtual HRESULT __stdcall get_ShadowMapSize(short & oShadowMapSize)=0;

    virtual HRESULT __stdcall put_ShadowMapSize(short iShadowMapSize)=0;

    virtual HRESULT __stdcall get_ShadowFittingMode(short & oShadowFittingMode)=0;

    virtual HRESULT __stdcall put_ShadowFittingMode(short iShadowFittingMode)=0;

    virtual HRESULT __stdcall get_ActiveStatus(short & oActiveStatus)=0;

    virtual HRESULT __stdcall put_ActiveStatus(short iActiveStatus)=0;

    virtual HRESULT __stdcall get_LightAreaType(short & oLightAreaType)=0;

    virtual HRESULT __stdcall put_LightAreaType(short iLightAreaType)=0;

    virtual HRESULT __stdcall get_AreaStatus(short & oAreaStatus)=0;

    virtual HRESULT __stdcall put_AreaStatus(short iAreaStatus)=0;

    virtual HRESULT __stdcall get_RectangleLightLength(double & oRectangleLightLength)=0;

    virtual HRESULT __stdcall put_RectangleLightLength(double iRectangleLightLength)=0;

    virtual HRESULT __stdcall get_RectangleLightWidth(double & oRectangleLightWidth)=0;

    virtual HRESULT __stdcall put_RectangleLightWidth(double iRectangleLightWidth)=0;

    virtual HRESULT __stdcall get_DiskLightRadius(double & oDiskLightRadius)=0;

    virtual HRESULT __stdcall put_DiskLightRadius(double iDiskLightRadius)=0;

    virtual HRESULT __stdcall get_SphereLightRadius(double & oSphereLightRadius)=0;

    virtual HRESULT __stdcall put_SphereLightRadius(double iSphereLightRadius)=0;

    virtual HRESULT __stdcall get_CylinderLightRadius(double & oCylinderLightRadius)=0;

    virtual HRESULT __stdcall put_CylinderLightRadius(double iCylinderLightRadius)=0;

    virtual HRESULT __stdcall get_CylinderLightHeight(double & oCylinderLightHeight)=0;

    virtual HRESULT __stdcall put_CylinderLightHeight(double iCylinderLightHeight)=0;

    virtual HRESULT __stdcall get_AreaSamplesU(short & oAreaSamplesU)=0;

    virtual HRESULT __stdcall put_AreaSamplesU(short iAreaSamplesU)=0;

    virtual HRESULT __stdcall get_AreaSamplesV(short & oAreaSamplesV)=0;

    virtual HRESULT __stdcall put_AreaSamplesV(short iAreaSamplesV)=0;

    virtual HRESULT __stdcall get_IlluminationStatus(short & oIlluminationStatus)=0;

    virtual HRESULT __stdcall put_IlluminationStatus(short iIlluminationStatus)=0;

    virtual HRESULT __stdcall get_FalloffExponent(short & oFalloffExponent)=0;

    virtual HRESULT __stdcall put_FalloffExponent(short iFalloffExponent)=0;

    virtual HRESULT __stdcall get_EnergyFactor(double & oEnergyFactor)=0;

    virtual HRESULT __stdcall put_EnergyFactor(double iEnergyFactor)=0;

    virtual HRESULT __stdcall get_CausticPhotonsNumber(CATLONG & oCausticPhotonsNumber)=0;

    virtual HRESULT __stdcall put_CausticPhotonsNumber(CATLONG iCausticPhotonsNumber)=0;

    virtual HRESULT __stdcall get_GlobalPhotonsNumber(CATLONG & oGlobalPhotonsNumber)=0;

    virtual HRESULT __stdcall put_GlobalPhotonsNumber(CATLONG iGlobalPhotonsNumber)=0;


};

CATDeclareHandler(CATIARenderingLight, CATIABase);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
