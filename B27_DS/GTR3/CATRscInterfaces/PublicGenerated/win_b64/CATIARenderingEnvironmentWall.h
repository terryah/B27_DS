/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIARenderingEnvironmentWall_h
#define CATIARenderingEnvironmentWall_h

#ifndef ExportedByCATRscPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __CATRscPubIDL
#define ExportedByCATRscPubIDL __declspec(dllexport)
#else
#define ExportedByCATRscPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByCATRscPubIDL
#endif
#endif

#include "CATBSTR.h"
#include "CATIABase.h"

extern ExportedByCATRscPubIDL IID IID_CATIARenderingEnvironmentWall;

class ExportedByCATRscPubIDL CATIARenderingEnvironmentWall : public CATIABase
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_ActiveStatus(short & oActiveStatus)=0;

    virtual HRESULT __stdcall put_ActiveStatus(short iActiveStatus)=0;

    virtual HRESULT __stdcall get_ShadowsStatus(short & oShadowsStatus)=0;

    virtual HRESULT __stdcall put_ShadowsStatus(short iShadowsStatus)=0;

    virtual HRESULT __stdcall get_Texture(CATBSTR & oTextureName)=0;

    virtual HRESULT __stdcall put_Texture(const CATBSTR & iTextureName)=0;

    virtual HRESULT __stdcall get_AutoScaleStatus(short & oAutoScaleStatus)=0;

    virtual HRESULT __stdcall put_AutoScaleStatus(short iAutoScaleStatus)=0;

    virtual HRESULT __stdcall FitAllInWall()=0;

    virtual HRESULT __stdcall get_LinkedScaleStatus(short & oLinkedScaleStatus)=0;

    virtual HRESULT __stdcall put_LinkedScaleStatus(short iLinkedScaleStatus)=0;

    virtual HRESULT __stdcall get_ScaleU(double & oScaleU)=0;

    virtual HRESULT __stdcall put_ScaleU(double iScaleU)=0;

    virtual HRESULT __stdcall get_ScaleV(double & oScaleV)=0;

    virtual HRESULT __stdcall put_ScaleV(double iScaleV)=0;

    virtual HRESULT __stdcall get_TranslationU(double & oTranslationU)=0;

    virtual HRESULT __stdcall put_TranslationU(double iTranslationU)=0;

    virtual HRESULT __stdcall get_TranslationV(double & oTranslationV)=0;

    virtual HRESULT __stdcall put_TranslationV(double iTranslationV)=0;

    virtual HRESULT __stdcall get_Rotation(double & oRotation)=0;

    virtual HRESULT __stdcall put_Rotation(double iRotation)=0;

    virtual HRESULT __stdcall get_FlipUStatus(short & oFlipUStatus)=0;

    virtual HRESULT __stdcall put_FlipUStatus(short iFlipUStatus)=0;

    virtual HRESULT __stdcall get_FlipVStatus(short & oFlipVStatus)=0;

    virtual HRESULT __stdcall put_FlipVStatus(short iFlipVStatus)=0;


};

CATDeclareHandler(CATIARenderingEnvironmentWall, CATIABase);

#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
