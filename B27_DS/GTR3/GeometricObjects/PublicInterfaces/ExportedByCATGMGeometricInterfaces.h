// COPYRIGHT DASSAULT SYSTEMES 1999
/** @CAA2Required */
/*---------------------------------------------------------------------*/
/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */
/*---------------------------------------------------------------------*/
#include "CATIACGMLevel.h"
#ifdef CATIACGMR420CAA
#if defined ( _STATIC_SOURCE ) 
#define	ExportedByCATGMGeometricInterfaces
#elif defined __CATGMGeometricInterfaces

#define ExportedByCATGMGeometricInterfaces DSYExport
#else
#define ExportedByCATGMGeometricInterfaces DSYImport
#endif
#include "DSYExport.h"
#else
#if defined ( _STATIC_SOURCE ) 
#define	ExportedByCATGMGeometricInterfaces
#elif defined (_WINDOWS_SOURCE)  
#if defined ( __CATGMGeometricInterfaces )   	
#define	ExportedByCATGMGeometricInterfaces	__declspec(dllexport)
#else
#define	ExportedByCATGMGeometricInterfaces	__declspec(dllimport)
#endif
#else
#define	ExportedByCATGMGeometricInterfaces
#endif
#endif


