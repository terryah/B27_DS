//=============================================================================
// COPYRIGHT DASSAULT SYSTEMES  1999
//=============================================================================
/** @CAA2Required */
/*---------------------------------------------------------------------*/
/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */
/*---------------------------------------------------------------------*/
#include "CATIACGMLevel.h"
#ifdef CATIACGMR420CAA
#ifdef _STATIC_SOURCE
#define	ExportedByCATGeometricObjects
#elif defined __CATGeometricObjects

#define ExportedByCATGeometricObjects DSYExport
#else
#define ExportedByCATGeometricObjects DSYImport
#endif
#include "DSYExport.h"
#else
#ifdef _STATIC_SOURCE
#define	ExportedByCATGeometricObjects
#elif defined _WINDOWS_SOURCE
#ifdef	__CATGeometricObjects
#define	ExportedByCATGeometricObjects	__declspec(dllexport)
#else
#define	ExportedByCATGeometricObjects	__declspec(dllimport)
#endif
#else
#define	ExportedByCATGeometricObjects
#endif
#endif
#include "GeometricObjectsCommonDec.h"
