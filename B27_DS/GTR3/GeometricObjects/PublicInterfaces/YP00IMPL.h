// COPYRIGHT DASSAULT SYSTEMES 1999
/** @CAA2Required */
/*---------------------------------------------------------------------*/
/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */
/*---------------------------------------------------------------------*/
#include "CATIACGMLevel.h"
#ifdef CATIACGMR420CAA
#ifdef _STATIC_SOURCE
#define	ExportedByYP00IMPL
#elif defined __YP00IMPL

#define ExportedByYP00IMPL DSYExport
#else
#define ExportedByYP00IMPL DSYImport
#endif
#include "DSYExport.h"
#else
#ifdef _STATIC_SOURCE
#define	ExportedByYP00IMPL
#elif defined _WINDOWS_SOURCE
#ifdef	__YP00IMPL
#define	ExportedByYP00IMPL	__declspec(dllexport)
#else
#define	ExportedByYP00IMPL	__declspec(dllimport)
#endif
#else
#define	ExportedByYP00IMPL
#endif
#endif
