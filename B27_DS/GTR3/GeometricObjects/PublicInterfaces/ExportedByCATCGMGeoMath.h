// COPYRIGHT DASSAULT SYSTEMES 1999
/** @CAA2Required */
#include "CATIACGMLevel.h"
#ifdef CATIACGMR420CAA
#ifdef _STATIC_SOURCE
#define	ExportedByCATCGMGeoMath
#elif defined __CATCGMGeoMath

#define ExportedByCATCGMGeoMath DSYExport
#else
#define ExportedByCATCGMGeoMath DSYImport
#endif
#include "DSYExport.h"
#else
#ifdef _STATIC_SOURCE
#define	ExportedByCATCGMGeoMath
#elif defined _WINDOWS_SOURCE
#ifdef	__CATCGMGeoMath
#define	ExportedByCATCGMGeoMath	__declspec(dllexport)
#else
#define	ExportedByCATCGMGeoMath	__declspec(dllimport)
#endif
#else
#define	ExportedByCATCGMGeoMath
#endif
#endif
#include "GeometricObjectsCommonDec.h"
