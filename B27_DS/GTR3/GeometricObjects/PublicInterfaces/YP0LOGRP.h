#include "CATIACGMLevel.h"
#ifdef CATIACGMR420CAA
#ifdef _STATIC_SOURCE
#define	ExportedByYP0LOGRP
#elif defined __YP0LOGRP


// COPYRIGHT DASSAULT SYSTEMES 1999

/** @CAA2Required */

/*---------------------------------------------------------------------*/

/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */

/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */

/*---------------------------------------------------------------------*/
#define ExportedByYP0LOGRP DSYExport
#else
#define ExportedByYP0LOGRP DSYImport
#endif
#include "DSYExport.h"
#else
#ifdef _STATIC_SOURCE
#define	ExportedByYP0LOGRP
#elif defined _WINDOWS_SOURCE
#ifdef	__YP0LOGRP

// COPYRIGHT DASSAULT SYSTEMES 1999
/** @CAA2Required */
/*---------------------------------------------------------------------*/
/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */
/*---------------------------------------------------------------------*/
#define	ExportedByYP0LOGRP	__declspec(dllexport)
#else
#define	ExportedByYP0LOGRP	__declspec(dllimport)
#endif
#else
#define	ExportedByYP0LOGRP
#endif
#endif
#include "GeometricObjectsCommonDec.h"
