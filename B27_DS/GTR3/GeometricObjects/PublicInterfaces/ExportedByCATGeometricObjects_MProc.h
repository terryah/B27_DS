// COPYRIGHT DASSAULT SYSTEMES 1999
/** @CAA2Required */
#include "CATIACGMLevel.h"
#ifdef CATIACGMR420CAA
#ifdef _STATIC_SOURCE
#define	ExportedByCATGeometricObjects_MProc
#elif defined __CATGeometricObjects_MProc

#define ExportedByCATGeometricObjects_MProc DSYExport
#else
#define ExportedByCATGeometricObjects_MProc DSYImport
#endif
#include "DSYExport.h"
#else
#ifdef _STATIC_SOURCE
#define	ExportedByCATGeometricObjects_MProc
#elif defined _WINDOWS_SOURCE
#ifdef	__CATGeometricObjects_MProc
#define	ExportedByCATGeometricObjects_MProc	__declspec(dllexport)
#else
#define	ExportedByCATGeometricObjects_MProc	__declspec(dllimport)
#endif
#else
#define	ExportedByCATGeometricObjects_MProc
#endif
#endif
