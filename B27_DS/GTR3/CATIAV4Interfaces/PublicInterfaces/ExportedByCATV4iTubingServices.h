/* -*-c++-*- */
/* ExportedByCATV4iTubingServices.h : COPYRIGHT DASSAULT SYSTEMES 2009 */

/** @CAA2Required */
/*---------------------------------------------------------------------*/
/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */
/*---------------------------------------------------------------------*/

#ifndef  ExportedByCATV4iTubingServices_h
#define  ExportedByCATV4iTubingServices_h

#ifdef _WINDOWS_SOURCE
#ifdef __CATV4iTubingServices
#define ExportedByCATV4iTubingServices  __declspec(dllexport)
#else
#define ExportedByCATV4iTubingServices  __declspec(dllimport)
#endif
#else
#define ExportedByCATV4iTubingServices
#endif

#endif   /* ExportedByCATV4iTubingServices_h */
