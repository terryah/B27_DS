/* -*-c++-*- */
/* ExportedByCATV4iServices.h : COPYRIGHT DASSAULT SYSTEMES 2003 */

/** @CAA2Required */
/*---------------------------------------------------------------------*/
/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */
/*---------------------------------------------------------------------*/

#ifndef  ExportedByCATV4iCpV4ToV5_h
#define  ExportedByCATV4iCpV4ToV5_h

#ifdef _WINDOWS_SOURCE
#ifdef __CATV4iCpV4ToV5
#define ExportedByCATV4iCpV4ToV5  __declspec(dllexport)
#else
#define ExportedByCATV4iCpV4ToV5  __declspec(dllimport)
#endif
#else
#define ExportedByCATV4iCpV4ToV5
#endif

#endif   /* ExportedByCATV4iInterface_h */

