// COPYRIGHT DASSAULT SYSTEMES 2001
/** @CAA2Required */
//**********************************************************************
//* DON T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS *
//* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPEAR AT ANY TIME *
//**********************************************************************

#ifdef	_WINDOWS_SOURCE
#ifdef	__CATMechanicalModeler
#define	ExportedByCATMechanicalModeler	__declspec(dllexport)
#else
#define	ExportedByCATMechanicalModeler	__declspec(dllimport)
#endif
#else
#define	ExportedByCATMechanicalModeler
#endif

#ifndef NULL
#define NULL 0
#endif

