#ifdef LOCAL_DEFINITION_FOR_IID
LINK_WITH_FOR_IID = \
MechanicalModelerUUID \
ObjectSpecsModelerUUID
#else
LINK_WITH_FOR_IID =
#endif
# COPYRIGHT DASSAULT SYSTEMES 2000
#======================================================================
# Imakefile for module CATMfCheckAndClean.m
#======================================================================
#
#  Avr. 2000  Creation: PPB
#======================================================================
#
# SHARED LIBRARY 
#
BUILT_OBJECT_TYPE=SHARED LIBRARY 
 
LINK_WITH=$(LINK_WITH_FOR_IID)   JS0GROUP CATObjectSpecsModeler AC0SPCHECK AC0SPBAS CATUdfInterfaces\
             MF0STARTUP CATClnBase CATClnSpecs CATObjectModelerBase CATMecModInterfaces CATProductStructure1 \
             CATMathematics CATGitInterfaces CATTopologicalObjects CATVisualization CATGngGraph \ 
			 CATMechanicalModelerLive \
             CATInteractiveInterfaces CATViz CATGeometricObjects CATApplicationFrame PartItf CATMmrSys CATGeomReroute \
             CATMmlSys CATGngFeatures CATClnGenericNaming
