# COPYRIGHT DASSAULT SYSTEMES 2003
#======================================================================
# Imakefile for module CATMmrEmbeddedGeom.m
#======================================================================
#
#  Jan 2003  Creation: Code generated by the CAA wizard  llv
#======================================================================
#
# SHARED LIBRARY 
#
BUILT_OBJECT_TYPE=SHARED LIBRARY 
# coverage R14
BUILD=NO
 
# DO NOT EDIT :: THE CAA2 WIZARDS WILL ADD CODE HERE
WIZARD_LINK_MODULES =  \
JS0FM JS0GROUP MechanicalModelerUUID  \
CATMmrEmbeddedGeom\ 
  CATObjectModelerCATIA\
            CATObjectModelerBase\
            CATLiteralFeatures \
            CATSysFile\
            CATIAApplicationFrame\
            JS0GROUP\
            Mathematics\
            PartItf\
            SpecsModeler\
            CATMechanicalModeler\
            MecModItf\
			CATMMiUUID\
			MMUInterfaces\
			CATGitInterfaces\
			CATMecModInterfaces\
			AS0STARTUP\
			CATVisualization
			
			# END WIZARD EDITION ZONE

LINK_WITH= CATObjectModelerCATIA\
            CATObjectModelerBase\
            CATLiteralFeatures \
            CATSysFile\
            CATIAApplicationFrame\
            JS0GROUP\
            Mathematics\
            PartItf\
            SpecsModeler\
            CATMechanicalModeler\
            MecModItf\
			CATMmiUUID\
			CATGitInterfaces\
			CATMecModInterfaces\
			AS0STARTUP\
			CATVisualization


# System dependant variables
#
OS = AIX
#
OS = HP-UX
#
OS = IRIX
#
OS = SunOS
#
OS = Windows_NT
