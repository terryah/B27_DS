BUILT_OBJECT_TYPE=SHARED LIBRARY

LINK_WITH = AC0SPBAS \
			AD0XXBAS \
			CATLayersAndFilters \
			CATMecModLiveInterfaces \
		    CATMecModUseItf \ 
			CATMmlBRepModel \
			CATVisualization \
			CATSketcherInterfaces \
			JS0CORBA \
			MF0ROOT \
			MecModItfCPP \
			PartItf #CATIPrtBooleanOperation \
		    CATInteractiveInterfaces #CATUILevelManager \
			GN0GRAPH \ 
			CATTopologicalOperators \
			CATMecModLiveUseItf \
						CATMmrSys \
			CATMmlBrepAttributes \
			CATGitInterfaces \
			CATConstraintModelerItf \
			CATMmlRefPlane
