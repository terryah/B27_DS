//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/** @CAA2Required */
/*---------------------------------------------------------------------*/
/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */
/*---------------------------------------------------------------------*/
//
//    This Header file is included by all the Module.h header files in 
//    PublicInterfaces
//

#ifndef DNBINFRASTRUCTURE_H

#define DNBINFRASTRUCTURE_H DNBInfrastructure

#ifdef _WINDOWS_SOURCE
#if defined(__DNBInfrastructure)
#define ExportedByDNBInfrastructure __declspec(dllexport)
#else
#define ExportedByDNBInfrastructure __declspec(dllimport)
#endif
#else
#define ExportedByDNBInfrastructure
#endif

#endif /* DNBINFRASTRUCTURE_H */
