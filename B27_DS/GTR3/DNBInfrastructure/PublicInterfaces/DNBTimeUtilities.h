/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBTimeUtilities.h
//      Time utility functions.
//
//==============================================================================
//
// Usage notes: 
//      Time utilities used to get the current time in milliseconds.
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     mmh          06/13/2001   Initial implementation
//     sha          08/10/2001   Added new utility: GetONIDLETimeOut() 
//     sha          11/07/2002   Added new utility: DNBEnablePerfCheck() 
//     sha          12/11/2002   Provided new performance trace macros
//
//==============================================================================
#ifndef DNBTIMEUTILITIES_H
#define DNBTIMEUTILITIES_H

#include <DNBSystemBase.h>
#include <scl_iosfwd.h>
#include <scl_iomanip.h>
#include <DNBSystemDefs.h>

#include <DNBInfrastructure.h>

//------------------------------------------------------------------------------

/**
 * Returns the current time in milliseconds.
 */
ExportedByDNBInfrastructure unsigned long 
GetCrtTimeInMilliseconds();

/**
 * Returns the OnIdle time out value (milliseconds) from the environment
 * variable DNB_ONIDLE_TIMEOUT.
 */
ExportedByDNBInfrastructure int 
GetONIDLETimeOut();

/**
 * Returns 1 if the DNB_ENABLE_PERF environment variable is set.
 */
ExportedByDNBInfrastructure int 
DNBEnablePerfCheck();

//------------------------------------------------------------------------------
// Macros

#define _DNB_DUMP_TIME(msg,time)                            \
    cout << resetiosflags(ios::adjustfield)                 \
         << setiosflags(ios::right)                         \
         << setw(35) << setfill('+')                        \
         << msg << ":"                                      \
         << setw(10) << setfill(' ')                        \
         << time << " s" << endl                            \
         << resetiosflags(ios::adjustfield);

#define DNB_PERF_TRACE(msg)                                 \
    if( DNBEnablePerfCheck() )                              \
    {                                                       \
        cout << msg << endl;                                \
    }

#define DNB_BEGIN_PERF_TRACE(msg)                           \
    if( DNBEnablePerfCheck() )                              \
    {                                                       \
        cout << resetiosflags(ios::adjustfield)             \
             << setiosflags(ios::right)                     \
             << setw(15) << setfill('+')                    \
             << "BEGIN "                                    \
             << resetiosflags(ios::adjustfield)             \
             << setiosflags(ios::left)                      \
             << setw(40) << setfill('+')                    \
             << msg << endl                                 \
             << resetiosflags(ios::adjustfield);            \
    }
    
#define DNB_END_PERF_TRACE(msg)                             \
    if( DNBEnablePerfCheck() )                              \
    {                                                       \
        cout << resetiosflags(ios::adjustfield)             \
             << setiosflags(ios::right)                     \
             << setw(15) << setfill('+')                    \
             << "END "                                      \
             << resetiosflags(ios::adjustfield)             \
             << setiosflags(ios::left)                      \
             << setw(40) << setfill('+')                    \
             << msg << endl                                 \
             << resetiosflags(ios::adjustfield);            \
    }

#define DNB_DECLARE_CLASS_PERF_TRACE                        \
private:                                                    \
    unsigned long       _perfStartTime;                     \
    unsigned long       _perfStopTime;
    
#define DNB_INIT_CLASS_PERF_TRACE                           \
    _perfStartTime = 0;                                     \
    _perfStopTime = 0;

#define DNB_START_CLASS_CLOCK                               \
    _perfStartTime = 0;                                     \
    if( DNBEnablePerfCheck() )                              \
    {                                                       \
        _perfStartTime =                                    \
            GetCrtTimeInMilliseconds();                     \
    }

#define DNB_STOP_CLASS_CLOCK(msg)                           \
    if( _perfStartTime )                                    \
    {                                                       \
        _perfStopTime =                                     \
            GetCrtTimeInMilliseconds();                     \
        float time =                                        \
            ( _perfStopTime - _perfStartTime ) / 1000.0;    \
        _DNB_DUMP_TIME(msg,time);                           \
        DNB_INIT_CLASS_PERF_TRACE;                          \
    }

#define DNB_START_CLOCK                                     \
{                                                           \
    unsigned long startTime = 0;                            \
    if( DNBEnablePerfCheck() )                              \
    {                                                       \
        startTime = GetCrtTimeInMilliseconds();             \
    }

#define DNB_SAMPLE_CLOCK(msg)                               \
    if( startTime )                                         \
    {                                                       \
        unsigned long stopTime =                            \
            GetCrtTimeInMilliseconds();                     \
        float time =                                        \
            ( stopTime - startTime ) / 1000.0;              \
        _DNB_DUMP_TIME(msg,time);                           \
    }                                                        
    
#define DNB_STOP_CLOCK(msg)                                 \
    if( startTime )                                         \
    {                                                       \
        unsigned long stopTime =                            \
            GetCrtTimeInMilliseconds();                     \
        float time =                                        \
            ( stopTime - startTime ) / 1000.0;              \
        _DNB_DUMP_TIME(msg,time);                           \
    }                                                       \
}

//------------------------------------------------------------------------------

#endif // DNBTIMEUTILITIES_H
