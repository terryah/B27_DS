/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBBasicMsgWnd.h
//      Base class for message windows.
//
//==============================================================================
//
// Usage notes: 
//      Used as a base class for all the windows that display device specific
//      messages or data readout during simulation.
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     rtl          03/11/2013   IR-222672V5-6R2012 Fix. CAA Exposition
//
//==============================================================================
#ifndef DNB_BASICMSGWND_H
#define DNB_BASICMSGWND_H

#include <DNBInfrastructure.h>

#include <CATDlgDialog.h>
#include <CATBooleanDef.h>
#include <CATListOfCATUnicodeString.h>

class CATUnicodeString;

//------------------------------------------------------------------------------

/**
 * Base class for message windows.
 * <b>Role</b>: Used as a base class for all the windows that display device 
 * specific messages or data read-out during simulation.
 */
class ExportedByDNBInfrastructure DNBBasicMsgWnd : public CATDlgDialog
{
    CATDeclareClass;

public:
    /**
     * Constructs a DNBBasicMsgWnd object.
     */
    DNBBasicMsgWnd( const CATBaseUnknown_var &iOwner, const CATString &iTitle,
        CATDlgStyle iStyle = CATDlgWndAutoResize|
                             CATDlgWndNoButton|CATDlgGridLayout);
    virtual ~DNBBasicMsgWnd();

    /**
     * Displays the message window.
     */
    virtual void ShowWindow();
    /**
     * Hides the message window.
     */
    virtual void HideWindow();
    /**
     * Clears the message window. It can be implemented by the derived classes.
     */
    virtual void ClearWindow( boolean hideEmpty=TRUE );
    /**
     * Deletes the message window.
     */
    void DeleteWindow();
    /**
     * Returns TRUE if the message window is visible.
     */
    boolean IsVisible();
    /**
     * Returns TRUE if the message window is hidden.
     */
    boolean IsHidden();
    /**
     * Pure virtual method - binds the functor for the message window.
     * It has to be implemented by all derived classes.
     */
    virtual void BindFunctor() = 0;
    /**
     * Contains actions to be executed when a simulation command is activated.
     */
    virtual void ActivateSimCmd( CATNotification *iNotif );
    /**
     * Contains actions to be executed when a simulation command is deactivated.
     */
    virtual void DeactivateSimCmd( CATNotification *iNotif );
    /**
     * Contains actions to be executed when a simulation command is canceled.
     */
    virtual void CancelSimCmd( CATNotification *iNotif );
    /**
     * Activated when the window is closing.
     */
    virtual void OnWindCloseNotif( CATCommand *, CATNotification* , 
            CATCommandClientData data);
    /**
     * Activated when the window is restored.
     */
    virtual void OnWindRestoreNotif( CATCommand *, CATNotification * ,
            CATCommandClientData data);
    /**
     * Activated when the window is minimized.
     */
    virtual void OnWindMinimizeNotif( CATCommand *, CATNotification * ,
            CATCommandClientData data);
    /**
     * Activated when the window is maximized.
     */
    virtual void OnWindMaximizeNotif( CATCommand *, CATNotification * , 
            CATCommandClientData data);
    /**
     * Sets the index value.
     */
    void SetIndex( const int index );
    /**
     * Gets the index value.
     */
    int GetIndex() const;
    /**
     * Returns TRUE if the window is automatically cleaned up.
     */
    boolean IsAutoCleanEnabled() const;
    /**
     * Sets the flag to enable the automatic clean up.
     */
    void EnableAutoClean( const boolean autoClean );
    /**
     * Sets the priority filter.
     */
    virtual void SetFilter( int iFilter );
    /**
     * Sets the owner of the window.
     * @param iOwner
     *  The value to be set as owner.
     * @param iDelete
     *  If iOwner is NULL, if TRUE, delete the window, otherwise remove window
     *  from the DNBIMsgWndMgr extension.
     */
    void SetOwner( const CATBaseUnknown_var &iOwner, 
        boolean iDelete );
    /**
     * Gets the owner of the window.
     */
    CATBaseUnknown_var GetOwner();
    /**
     * Adds a callback to clean internal objects.
     */
    void AddCleanCB();
    /**
     * Sets the flag to control the functor binding.
     */
    virtual void SetBindFunctorFlag( const boolean bEnable );
    /**
     * Gets the flag that controls the functor binding.
     */
    boolean GetBindFunctorFlag();
    /**
     * Returns the internal title of the window.
     */
    CATUnicodeString& GetInternalTitle();
    /**
     * Returns the number of columns to be exported to an external file.
     * @param bExportAll
     *  If TRUE, method is called while exporting all message windows (default FALSE).
     */
    virtual int GetNbColumnExport( const boolean bExportAll = FALSE );
    /**
     * Exports data to an external file.
     * @param oExportList
     *  The list to be populated with exported data (do not empty this list).
     * @param iNbColumn
     *  The number of columns to be generated.
     * @param bExportAll
     *  If TRUE, method is called while exporting all message windows (default FALSE).
     */
    virtual void ExportData( CATListValCATUnicodeString& oExportList, int iNbColumn,
        const boolean bExportAll = FALSE );

protected:
    /**
     * Cleans the internal objects.
     */
    virtual void CleanObjects( CATCallbackEvent iPublishedEvent = NULL,
                        void * iPublishingObject = NULL,
                        CATNotification * iNotif = NULL,
                        CATSubscriberData iClientData = NULL,
                        CATCallback iCallback = 0 );
    /**
     * Generates the external file with given name/type and exports data into it.
     */
    virtual HRESULT GenerateFileForExport( CATUnicodeString iFileName, int iSheetType );

    CATBaseUnknown_var  _owner;
    int                 _index;
    boolean             _autoClean;
    boolean             _bindFunctor;
    CATUnicodeString    _internalTitle;

};

//------------------------------------------------------------------------------

#endif
