/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBCATConversions.h
//      Provides a series of V5<->WDM conversions, by ovrriding DNBAssign global
//      function.
//
//==============================================================================
//
// Usage notes: 
//      Use the desired DNBAssign function, by passing first the destination and
//      then the source of the conversion.
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     mmg          01/01/1999   Initial implementation
//     sha          06/04/2002   Added some more conversions
//
//==============================================================================
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef DNBCATCONVERSIONS_H
#define DNBCATCONVERSIONS_H

#include <DNBSystemBase.h>
#include <scl_list.h>
#include <DNBSystemDefs.h>

#include <DNBMathVector.h>
// smw; caa; #include <DNBXform3D.h>
template < class T> class DNBBasicXform3D;  // smw; caa
typedef DNBBasicXform3D<DNBReal>    DNBXform3D; // smw; caa
template < class T> class DNBBasicVector3D;  // smw; caa
typedef DNBBasicVector3D<DNBReal>    DNBVector3D; // smw; caa
#include <DNBCATBase.h>
#include <CATListOfDouble.h>
#include <CATMathTransformation.h>
#include <CAT4x4Matrix.h>
#include <CATUnicodeString.h>
#include <CATBaseUnknown.h>
#include <CATLISTV_CATBaseUnknown.h>
#include <sequence_CATBaseUnknown_ptr.h>
#include <CATMathAxis.h>
#include <CATMathPoint.h>
#include <CATMathVector.h>
#include <DNBCATDefs.h>

#include <DNBInfrastructure.h>

//------------------------------------------------------------------------------
/**
 * Converts an array of 12 doubles to a DNBXform3D.
 */
ExportedByDNBInfrastructure void
DNBAssign( DNBXform3D &dst, const double *src );

/**
 * Converts a DNBXform3D to an array of 12 doubles.
 */
ExportedByDNBInfrastructure void
DNBAssign( double *dst, const DNBXform3D &src );
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
/**
 * Converts a variable length CATListOfDouble to a DNBMathVector.
 */
ExportedByDNBInfrastructure void
DNBAssign( DNBMathVector &dst, const CATListOfDouble &src );

/**
 * Converts a variable length DNBMathVector to a CATListOfDouble.
 */
ExportedByDNBInfrastructure void
DNBAssign( CATListOfDouble &dst, const DNBMathVector &src );
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
/**
 * Converts a three-dimensional homogeneous transformation matrix 
 * (CATMathTransformation) to its WDM counterpart (DNBXform3D).
 */
ExportedByDNBInfrastructure void
DNBAssign( DNBXform3D &dst, const CATMathTransformation &src );

/**
 * Converts a three-dimensional homogeneous transformation matrix 
 * (DNBXform3D) to its V5 counterpart (CATMathTransformation).
 */
ExportedByDNBInfrastructure void
DNBAssign( CATMathTransformation &dst, const DNBXform3D &src );
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
/**
 * Converts a three-dimensional homogeneous transformation matrix 
 * (CAT4x4Matrix) to its WDM counterpart (DNBXform3D).
 */
ExportedByDNBInfrastructure void
DNBAssign( DNBXform3D& dst, const CAT4x4Matrix &src );

/**
 * Converts a three-dimensional homogeneous transformation matrix 
 * (DNBXform3D) to its V5 counterpart (CAT4x4Matrix).
 */
ExportedByDNBInfrastructure void
DNBAssign( CAT4x4Matrix &dst, const DNBXform3D &src );
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
/**
 * Converts a wide string to a V5 CATUnicodeString.
 */
ExportedByDNBInfrastructure void
DNBAssign( CATUnicodeString &dst, const scl_wstring &src );

/**
 * Converts a V5 CATUnicodeString to a wide string.
 */
ExportedByDNBInfrastructure void
DNBAssign( scl_wstring &dst, const CATUnicodeString &src );
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
/**
 * Converts a STL scl_list <void *> to a V5 SEQUENCE.
 */
ExportedByDNBInfrastructure void
DNBAssign( SEQUENCE(CATBaseUnknown_ptr) &dst,
                const scl_list<void *, DNB_ALLOCATOR(void *) > &src );
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
/**
 * Converts a STL scl_list <void *> to a V5 CATLISTV.
 */
ExportedByDNBInfrastructure void
DNBAssign( CATLISTV(CATBaseUnknown_var) &dst,
                const scl_list<void *, DNB_ALLOCATOR(void *) > &src );
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
/**
 * Converts a V5 CATMathVector to a three-dimensional point vector (DNBVector3D)
 */
ExportedByDNBInfrastructure void
DNBAssign( DNBVector3D& dst, const CATMathVector &src );

/**
 * Converts a three-dimensional point vector (DNBVector3D) to a V5 CATMathVector 
 */
ExportedByDNBInfrastructure void
DNBAssign( CATMathVector& dst, const DNBVector3D& src );
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
/**
 * Converts a V5 CATMathPoint to a three-dimensional point vector (DNBVector3D)
 */
ExportedByDNBInfrastructure void
DNBAssign( DNBVector3D& dst, const CATMathPoint &src );

/**
 * Converts a three-dimensional point vector (DNBVector3D) to a V5 CATMathPoint
 */
ExportedByDNBInfrastructure void
DNBAssign( CATMathPoint& dst, const DNBVector3D& src );
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
/**
 * Converts a three-dimensional homogeneous transformation matrix 
 * (CATMathAxis) to its WDM counterpart (DNBXform3D).
 */
ExportedByDNBInfrastructure void
DNBAssign( DNBXform3D& xform, const CATMathAxis &src );

/**
 * Converts a three-dimensional homogeneous transformation matrix 
 * (DNBXform3D) to its V5 counterpart (CATMathAxis).
 */
ExportedByDNBInfrastructure void
DNBAssign( CATMathAxis& xform, const DNBXform3D& src );
//------------------------------------------------------------------------------


// Define < operator for CATISiLink_var so that the maps build into the
// DNBGraph can function properly. mmg 12/20/2002
//
ExportedByDNBInfrastructure bool
operator<(const CATBaseUnknown_var &left, const CATBaseUnknown_var &right )
    DNB_THROW_SPEC_NULL;

#endif  /* DNBCATCONVERSIONS_H */
