/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBBasicTHRMessage.h
//      Base class for all PCQueue (threaded) messages.
//
//==============================================================================
//
// Usage notes: 
//      This class is virtual pure, so it cannot be instatiated directly.
//      Derive your threaded message from this class and override evaluate()
//      method to accommodate your needs.
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     mmg          01/01/1999   Initial implementation
//     mmg          09/03/2002   added method: reset()
//     mmg          09/13/2002   added method: posted()
//     mmg          09/22/2002   changed reset() method to prepost()
//
//==============================================================================
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _DNB_BASICTHRMESSAGE_H_
#define _DNB_BASICTHRMESSAGE_H_

#include <DNBSystemBase.h>
//#include <rw/thr/mutex.h>
//#include <rw/thr/condtion.h>
#include <DNBSysMutexLock.h>
#include <DNBSysCondition.h>
// #include <DNBSysLockGuard.h> // smw; caa 
#include <DNBSystemDefs.h>

#include <DNBInfrastructure.h>

//------------------------------------------------------------------------------

/**
 * Base class for all PCQueue (threaded) messages.
 */
class ExportedByDNBInfrastructure DNBBasicTHRMessage
{
public:
    /**
     * Constructs a DNBBasicTHRMessage object.
     */
    DNBBasicTHRMessage( int complexity = 1 )
        DNB_THROW_SPEC_NULL;
    
    /**
     * Copy constructs a DNBBasicTHRMessage object.
     */
    DNBBasicTHRMessage( const DNBBasicTHRMessage & )
        DNB_THROW_SPEC_NULL;
    
    /**
     * Assigns a DNBBasicTHRMessage object.
     */
    virtual void 
    operator=( const DNBBasicTHRMessage & )
        DNB_THROW_SPEC_NULL;
    
    /**
     * Destructs a DNBBasicTHRMessage object.
     */
    virtual ~DNBBasicTHRMessage()
        DNB_THROW_SPEC_NULL;
    
    /**
     * Posts itself on the PCQueue.
     * If blocking is TRUE, the method will wait for the evaluation of the
     * message and returns thereafter.
     */
    virtual void 
    post( bool blocking = TRUE, bool instant = FALSE )
        DNB_THROW_SPEC_NULL;
    
    /**
     * Waits for the evaluation of the message.
     */
    void 
    wait()
        DNB_THROW_SPEC_NULL;
    
    /**
     * Signals the evaluation of the message.
     */
    void 
    signal()
        DNB_THROW_SPEC_NULL;
    
    /**
     * Specifies the complexity of the message.
     */
    int 
    complexity()
        DNB_THROW_SPEC_NULL;

    /**
     * Specifies whether a message has been posted to the PCQueue
     */
    bool 
    posted()
        DNB_THROW_SPEC_NULL;

    /**
     * allows for logic to be executed just prior to posting the message
     */
    virtual bool 
    prepost()
        DNB_THROW_SPEC_ANY;
    
    /**
     * Evaluates the message.
     * Pure virtual function which must be overridden.
     */
    virtual bool 
    evaluate()
        DNB_THROW_SPEC_ANY = 0;

    /**
     * Evaluates the message.
     * if flushing is true, always return true
     */
    virtual bool 
    evaluate( bool flushing )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Specifies whether a message has been posted as blocking to the PCQueue
     */
    bool 
    blocking()
        DNB_THROW_SPEC_NULL;
    //
    //  The following relational operators are required to pacify the AIX C++
    //  compiler.
    //

    /**
     * Equality operator.
     */
    bool
    operator==( const DNBBasicTHRMessage &right ) const
        DNB_THROW_SPEC_NULL;
    
    /**
     * Less than operator.
     */
    bool
    operator <( const DNBBasicTHRMessage &right ) const
        DNB_THROW_SPEC_NULL;
    
protected:
    int                     _read_buffer;
    int                     _write_buffer;

private:
    bool                    _evaluated;
    mutable DNBSysMutexLock _monitor;
    DNBSysCondition         _cond;
    int                     _complexity;
    bool                    _blocking;
};

//------------------------------------------------------------------------------

#endif  // _DNB_BASICTHRMESSAGE_H_
