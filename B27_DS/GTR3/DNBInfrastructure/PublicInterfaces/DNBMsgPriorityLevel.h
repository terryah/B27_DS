/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBMsgPriorityLevel.h
//      Defines the priority levels used as filter by the messaging system.
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     mmh          02/08/2002   Initial implementation
//     mmh          02/14/2002   Add value for NO_PREFIX
//
//==============================================================================
#ifndef DNBMsgPriorityLevel_H
#define DNBMsgPriorityLevel_H

#define DNB_MSG_INFO            0x00000001
#define DNB_MSG_WARNING         0x00000002
#define DNB_MSG_ERROR           0x00000004
#define DNB_MSG_FATAL_ERROR     0x00000008

#define DNB_NO_PREFIX           0x10000000

#endif //DNBMsgPriorityLevel_H
