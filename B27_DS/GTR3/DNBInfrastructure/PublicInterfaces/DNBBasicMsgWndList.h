/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBBasicMsgWndList.h
//      Implementation of a list of pointers of DNBBasicMsgWnd objects.
//
//==============================================================================
//
// Usage notes: 
//      Collection class for pointers of basic message windows. All functions 
//      defined on a list are available.
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     rtl          03/11/2013   IR-222672V5-6R2012 Fix. CAA Exposition.
//
//==============================================================================
#ifndef DNB_BASIC_MSG_WND_LIST_H
#define DNB_BASIC_MSG_WND_LIST_H

#include <DNBInfrastructure.h>

class DNBBasicMsgWnd;

//------------------------------------------------------------------------------
// CATLISTP
//------------------------------------------------------------------------------

#include <CATLISTP_Clean.h>

//#include <CATLISTP_AllFunct.h>
#define CATLISTP_Append

#include <CATLISTP_Declare.h>

#ifdef CATCOLLEC_ExportedBy
#undef CATCOLLEC_ExportedBy
#endif
#define CATCOLLEC_ExportedBy ExportedByDNBInfrastructure

CATLISTP_DECLARE( DNBBasicMsgWnd )

typedef CATLISTP( DNBBasicMsgWnd ) DNBBasicMsgWndListP;

#endif  // DNB_BASIC_MSG_WND_LIST_H
