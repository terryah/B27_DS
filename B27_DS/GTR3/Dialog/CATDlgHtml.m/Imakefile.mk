#======================================================================
# Imakefile for module CATDlgHtml.m
#======================================================================
#
#======================================================================
# Shared Library
#======================================================================

BUILT_OBJECT_TYPE = SHARED LIBRARY

LINK_WITH = JS0GROUP CATSysTS JS0FM DI0PANV2

#
OS = Windows
# For official CATIAV5PrecompiledHeader.h to include afxwin.h
MKMFC_DEPENDENCY = yes

#
OS = Darwin
LOCAL_LDFLAGS = -framework Cocoa -framework WebKit

#
# On iOS, use CATDlgCocoaDrv
OS = iOS
BUILD = NO

#
# On Android, use CATDlgAndroidDrv
OS = Android
ALIASES_ON_IMPORT = CATDlgAndroidDrv
BUILD = NO
