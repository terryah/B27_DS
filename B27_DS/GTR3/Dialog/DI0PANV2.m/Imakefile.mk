#======================================================================
# Imakefile for module DI0PANV2.m
#======================================================================
#
#======================================================================
# Shared Library
#======================================================================

BUILT_OBJECT_TYPE = SHARED LIBRARY

COMMON_LINK_WITH = \
  CATSysFile CATSysMultiThreading CATSysRunBrw JS0FM JS0GROUP \ # System
  CATVBAInfra VBAIntegration \ # VBA

#
OS = UNIX
LINK_WITH = $(COMMON_LINK_WITH) CATDlgBitmap
SYS_LIBS = -lXm -lXmu -lXt -lxkbfile -lX11

#
OS = Windows
LINK_WITH = $(COMMON_LINK_WITH)
LOCAL_CCFLAGS = /D"OEMRESOURCE"
MKMFC_DEPENDENCY = YES
