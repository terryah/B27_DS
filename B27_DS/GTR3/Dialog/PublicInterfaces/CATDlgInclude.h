#ifndef CATDlgPANEL
#define CATDlgPANEL

// COPYRIGHT DASSAULT SYSTEMES 1999

/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */

/**
 * @nodoc 
 * List of included files for the Dialog framework.
 */
#include "CATDlgUtility.h"
#include "CATDlgBarMenu.h"
#include "CATDlgBox.h"
#include "CATDlgCheckButton.h"
#include "CATDlgCheckItem.h"
#include "CATDlgCombo.h"
#include "CATDlgContainer.h"
#include "CATDlgControl.h"
#include "CATDlgContextualMenu.h"
#include "CATDlgBlackBox.h"
#include "CATDlgDialog.h"
#include "CATDlgDocument.h"
#include "CATDlgRadioButton.h"
#include "CATDlgEditor.h"
#include "CATDlgRadioItem.h"
#include "CATDlgFile.h"
#include "CATDlgFrame.h"
#include "CATDlgMenuItem.h"
#include "CATDlgLabel.h"
#include "CATDlgSelectorList.h"
#include "CATDlgMenu.h"
#include "CATDlgPushItem.h"
#include "CATDlgNotify.h"
#include "CATDlgPushButton.h"
#include "CATDialog.h"
#include "CATDlgSeparator.h"
#include "CATDlgSeparatorItem.h"
#include "CATDlgSlider.h"
#include "CATDlgSpinner.h"
#include "CATDlgStatusBar.h"
#include "CATDlgSubMenu.h"
#include "CATDlgToolBar.h"
#include "CATDlgWindow.h"
#include "CATDlgMultiList.h"
#include "CATDlgTabContainer.h"
#include "CATDlgTabPage.h"
#include "CATDlgSplitter.h"
#include "CATDlgIconBox.h"
#include "CATDlgProgress.h"
#include "CATDlgScrollBar.h"
#endif
