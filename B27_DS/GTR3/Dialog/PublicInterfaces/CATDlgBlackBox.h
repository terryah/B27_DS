#ifndef CATDLGBLACKBOX_H
#define CATDLGBLACKBOX_H

// COPYRIGHT DASSAULT SYSTEMES 2004

/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */

#include "CATDialog.h"
/**
 * Base class for CATDlgWindows (for Windows platforms) or CATDlgMotif (for Unix platforms).
 *
 * You cannot construct a CATDlgWindows directly: it can be constructed 
 * as the base class of one of its derived class only. 
 */
//--------------------------------------------------------------------------
class ExportedByDI0PANV2 CATDlgBlackBox : public CATDialog
{

//--------------------------------------------------------------------------
 public:
        CATDeclareClass ;

 protected:

        CATDlgBlackBox( CATDialog *pParent, const CATString& rObjectName, CATDlgStyle nStyle=NULL);

        virtual ~CATDlgBlackBox();
};
#endif
