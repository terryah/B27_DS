
// COPYRIGHT DASSAULT SYSTEMES 2000
/** @CAA2Required */
/**********************************************************************/
/* DON T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPEAR AT ANY TIME */
/**********************************************************************/

#ifndef ExportedByDI0PANV2

#ifdef _WINDOWS_SOURCE

#define DRIVER_WINDOWS

#if defined(__DI0PANV2)
#define ExportedByDI0PANV2 __declspec(dllexport)
#else
#define ExportedByDI0PANV2 __declspec(dllimport)
#endif

#else
#define DRIVER_MOTIF 

/* For MAINWIN version comment the previous statement
   and uncomment the following block */
/*
#include "CATPreMainwin.h"
#include "CATMainwin.h"

#define DRIVER_WINDOWS
#define DRIVER_MAINWIN
*/

#define ExportedByDI0PANV2

#endif

#endif
