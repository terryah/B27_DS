#ifndef __TIE_DNBIAMHIVersionManagement
#define __TIE_DNBIAMHIVersionManagement

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIAMHIVersionManagement.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIAMHIVersionManagement */
#define declare_TIE_DNBIAMHIVersionManagement(classe) \
 \
 \
class TIEDNBIAMHIVersionManagement##classe : public DNBIAMHIVersionManagement \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIAMHIVersionManagement, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall GetAllVersions(CATBaseDispatch * iObject, CATSafeArrayVariant *& oVersionIDs); \
      virtual HRESULT __stdcall GetAllVersionsFromID(const CATBSTR & iObjectID, CATSafeArrayVariant *& oVersionIDs); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIAMHIVersionManagement(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall GetAllVersions(CATBaseDispatch * iObject, CATSafeArrayVariant *& oVersionIDs); \
virtual HRESULT __stdcall GetAllVersionsFromID(const CATBSTR & iObjectID, CATSafeArrayVariant *& oVersionIDs); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIAMHIVersionManagement(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::GetAllVersions(CATBaseDispatch * iObject, CATSafeArrayVariant *& oVersionIDs) \
{ \
return (ENVTIECALL(DNBIAMHIVersionManagement,ENVTIETypeLetter,ENVTIELetter)GetAllVersions(iObject,oVersionIDs)); \
} \
HRESULT __stdcall  ENVTIEName::GetAllVersionsFromID(const CATBSTR & iObjectID, CATSafeArrayVariant *& oVersionIDs) \
{ \
return (ENVTIECALL(DNBIAMHIVersionManagement,ENVTIETypeLetter,ENVTIELetter)GetAllVersionsFromID(iObjectID,oVersionIDs)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIAMHIVersionManagement,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIAMHIVersionManagement,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIAMHIVersionManagement,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIAMHIVersionManagement,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIAMHIVersionManagement,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIAMHIVersionManagement(classe)    TIEDNBIAMHIVersionManagement##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIAMHIVersionManagement(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIAMHIVersionManagement, classe) \
 \
 \
CATImplementTIEMethods(DNBIAMHIVersionManagement, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIAMHIVersionManagement, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIAMHIVersionManagement, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIAMHIVersionManagement, classe) \
 \
HRESULT __stdcall  TIEDNBIAMHIVersionManagement##classe::GetAllVersions(CATBaseDispatch * iObject, CATSafeArrayVariant *& oVersionIDs) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&iObject,&oVersionIDs); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAllVersions(iObject,oVersionIDs); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&iObject,&oVersionIDs); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIVersionManagement##classe::GetAllVersionsFromID(const CATBSTR & iObjectID, CATSafeArrayVariant *& oVersionIDs) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&iObjectID,&oVersionIDs); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAllVersionsFromID(iObjectID,oVersionIDs); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&iObjectID,&oVersionIDs); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIVersionManagement##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIVersionManagement##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIVersionManagement##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIVersionManagement##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIVersionManagement##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIAMHIVersionManagement(classe) \
 \
 \
declare_TIE_DNBIAMHIVersionManagement(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAMHIVersionManagement##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAMHIVersionManagement,"DNBIAMHIVersionManagement",DNBIAMHIVersionManagement::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAMHIVersionManagement(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIAMHIVersionManagement, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAMHIVersionManagement##classe(classe::MetaObject(),DNBIAMHIVersionManagement::MetaObject(),(void *)CreateTIEDNBIAMHIVersionManagement##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIAMHIVersionManagement(classe) \
 \
 \
declare_TIE_DNBIAMHIVersionManagement(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAMHIVersionManagement##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAMHIVersionManagement,"DNBIAMHIVersionManagement",DNBIAMHIVersionManagement::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAMHIVersionManagement(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIAMHIVersionManagement, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAMHIVersionManagement##classe(classe::MetaObject(),DNBIAMHIVersionManagement::MetaObject(),(void *)CreateTIEDNBIAMHIVersionManagement##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIAMHIVersionManagement(classe) TIE_DNBIAMHIVersionManagement(classe)
#else
#define BOA_DNBIAMHIVersionManagement(classe) CATImplementBOA(DNBIAMHIVersionManagement, classe)
#endif

#endif
