#ifndef __TIE_DNBIAMHIRelationServices
#define __TIE_DNBIAMHIRelationServices

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIAMHIRelationServices.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIAMHIRelationServices */
#define declare_TIE_DNBIAMHIRelationServices(classe) \
 \
 \
class TIEDNBIAMHIRelationServices##classe : public DNBIAMHIRelationServices \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIAMHIRelationServices, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall GetRelation(const CATBSTR & iRelationSource, const CATBSTR & iRelationTarget, const CATBSTR & iRelationName, CATBSTR & oRelationObjectID); \
      virtual HRESULT __stdcall GetRelations(const CATSafeArrayVariant & iRelationSources, const CATSafeArrayVariant & iRelationTargets, const CATBSTR & iRelationName, CATSafeArrayVariant *& oListRelationObjectIDs); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIAMHIRelationServices(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall GetRelation(const CATBSTR & iRelationSource, const CATBSTR & iRelationTarget, const CATBSTR & iRelationName, CATBSTR & oRelationObjectID); \
virtual HRESULT __stdcall GetRelations(const CATSafeArrayVariant & iRelationSources, const CATSafeArrayVariant & iRelationTargets, const CATBSTR & iRelationName, CATSafeArrayVariant *& oListRelationObjectIDs); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIAMHIRelationServices(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::GetRelation(const CATBSTR & iRelationSource, const CATBSTR & iRelationTarget, const CATBSTR & iRelationName, CATBSTR & oRelationObjectID) \
{ \
return (ENVTIECALL(DNBIAMHIRelationServices,ENVTIETypeLetter,ENVTIELetter)GetRelation(iRelationSource,iRelationTarget,iRelationName,oRelationObjectID)); \
} \
HRESULT __stdcall  ENVTIEName::GetRelations(const CATSafeArrayVariant & iRelationSources, const CATSafeArrayVariant & iRelationTargets, const CATBSTR & iRelationName, CATSafeArrayVariant *& oListRelationObjectIDs) \
{ \
return (ENVTIECALL(DNBIAMHIRelationServices,ENVTIETypeLetter,ENVTIELetter)GetRelations(iRelationSources,iRelationTargets,iRelationName,oListRelationObjectIDs)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIAMHIRelationServices,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIAMHIRelationServices,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIAMHIRelationServices,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIAMHIRelationServices,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIAMHIRelationServices,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIAMHIRelationServices(classe)    TIEDNBIAMHIRelationServices##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIAMHIRelationServices(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIAMHIRelationServices, classe) \
 \
 \
CATImplementTIEMethods(DNBIAMHIRelationServices, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIAMHIRelationServices, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIAMHIRelationServices, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIAMHIRelationServices, classe) \
 \
HRESULT __stdcall  TIEDNBIAMHIRelationServices##classe::GetRelation(const CATBSTR & iRelationSource, const CATBSTR & iRelationTarget, const CATBSTR & iRelationName, CATBSTR & oRelationObjectID) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&iRelationSource,&iRelationTarget,&iRelationName,&oRelationObjectID); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetRelation(iRelationSource,iRelationTarget,iRelationName,oRelationObjectID); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&iRelationSource,&iRelationTarget,&iRelationName,&oRelationObjectID); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIRelationServices##classe::GetRelations(const CATSafeArrayVariant & iRelationSources, const CATSafeArrayVariant & iRelationTargets, const CATBSTR & iRelationName, CATSafeArrayVariant *& oListRelationObjectIDs) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&iRelationSources,&iRelationTargets,&iRelationName,&oListRelationObjectIDs); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetRelations(iRelationSources,iRelationTargets,iRelationName,oListRelationObjectIDs); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&iRelationSources,&iRelationTargets,&iRelationName,&oListRelationObjectIDs); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIRelationServices##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIRelationServices##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIRelationServices##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIRelationServices##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIRelationServices##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIAMHIRelationServices(classe) \
 \
 \
declare_TIE_DNBIAMHIRelationServices(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAMHIRelationServices##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAMHIRelationServices,"DNBIAMHIRelationServices",DNBIAMHIRelationServices::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAMHIRelationServices(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIAMHIRelationServices, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAMHIRelationServices##classe(classe::MetaObject(),DNBIAMHIRelationServices::MetaObject(),(void *)CreateTIEDNBIAMHIRelationServices##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIAMHIRelationServices(classe) \
 \
 \
declare_TIE_DNBIAMHIRelationServices(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAMHIRelationServices##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAMHIRelationServices,"DNBIAMHIRelationServices",DNBIAMHIRelationServices::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAMHIRelationServices(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIAMHIRelationServices, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAMHIRelationServices##classe(classe::MetaObject(),DNBIAMHIRelationServices::MetaObject(),(void *)CreateTIEDNBIAMHIRelationServices##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIAMHIRelationServices(classe) TIE_DNBIAMHIRelationServices(classe)
#else
#define BOA_DNBIAMHIRelationServices(classe) CATImplementBOA(DNBIAMHIRelationServices, classe)
#endif

#endif
