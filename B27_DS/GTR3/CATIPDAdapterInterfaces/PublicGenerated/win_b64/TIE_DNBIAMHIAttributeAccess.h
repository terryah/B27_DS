#ifndef __TIE_DNBIAMHIAttributeAccess
#define __TIE_DNBIAMHIAttributeAccess

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIAMHIAttributeAccess.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIAMHIAttributeAccess */
#define declare_TIE_DNBIAMHIAttributeAccess(classe) \
 \
 \
class TIEDNBIAMHIAttributeAccess##classe : public DNBIAMHIAttributeAccess \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIAMHIAttributeAccess, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall SetStringAttribute(const CATBSTR & iObjId, const CATBSTR & iAttrName, const CATBSTR & iAttrValue); \
      virtual HRESULT __stdcall SetLongAttribute(const CATBSTR & iObjId, const CATBSTR & iAttrName, CATLONG iAttrValue); \
      virtual HRESULT __stdcall SetDoubleAttribute(const CATBSTR & iObjId, const CATBSTR & iAttrName, double iAttrValue); \
      virtual HRESULT __stdcall SetBooleanAttribute(const CATBSTR & iObjId, const CATBSTR & iAttrName, CAT_VARIANT_BOOL iAttrValue); \
      virtual HRESULT __stdcall GetStringAttribute(const CATBSTR & iObjId, const CATBSTR & iAttrName, CATBSTR & oAttrValue); \
      virtual HRESULT __stdcall GetLongAttribute(const CATBSTR & iObjId, const CATBSTR & iAttrName, CATLONG & oAttrValue); \
      virtual HRESULT __stdcall GetDoubleAttribute(const CATBSTR & iObjId, const CATBSTR & iAttrName, double & oAttrValue); \
      virtual HRESULT __stdcall GetBooleanAttribute(const CATBSTR & iObjId, const CATBSTR & iAttrName, CAT_VARIANT_BOOL & oAttrValue); \
      virtual HRESULT __stdcall SetQueryObjectClass(const CATBSTR & iQueryObjectClass); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIAMHIAttributeAccess(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall SetStringAttribute(const CATBSTR & iObjId, const CATBSTR & iAttrName, const CATBSTR & iAttrValue); \
virtual HRESULT __stdcall SetLongAttribute(const CATBSTR & iObjId, const CATBSTR & iAttrName, CATLONG iAttrValue); \
virtual HRESULT __stdcall SetDoubleAttribute(const CATBSTR & iObjId, const CATBSTR & iAttrName, double iAttrValue); \
virtual HRESULT __stdcall SetBooleanAttribute(const CATBSTR & iObjId, const CATBSTR & iAttrName, CAT_VARIANT_BOOL iAttrValue); \
virtual HRESULT __stdcall GetStringAttribute(const CATBSTR & iObjId, const CATBSTR & iAttrName, CATBSTR & oAttrValue); \
virtual HRESULT __stdcall GetLongAttribute(const CATBSTR & iObjId, const CATBSTR & iAttrName, CATLONG & oAttrValue); \
virtual HRESULT __stdcall GetDoubleAttribute(const CATBSTR & iObjId, const CATBSTR & iAttrName, double & oAttrValue); \
virtual HRESULT __stdcall GetBooleanAttribute(const CATBSTR & iObjId, const CATBSTR & iAttrName, CAT_VARIANT_BOOL & oAttrValue); \
virtual HRESULT __stdcall SetQueryObjectClass(const CATBSTR & iQueryObjectClass); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIAMHIAttributeAccess(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::SetStringAttribute(const CATBSTR & iObjId, const CATBSTR & iAttrName, const CATBSTR & iAttrValue) \
{ \
return (ENVTIECALL(DNBIAMHIAttributeAccess,ENVTIETypeLetter,ENVTIELetter)SetStringAttribute(iObjId,iAttrName,iAttrValue)); \
} \
HRESULT __stdcall  ENVTIEName::SetLongAttribute(const CATBSTR & iObjId, const CATBSTR & iAttrName, CATLONG iAttrValue) \
{ \
return (ENVTIECALL(DNBIAMHIAttributeAccess,ENVTIETypeLetter,ENVTIELetter)SetLongAttribute(iObjId,iAttrName,iAttrValue)); \
} \
HRESULT __stdcall  ENVTIEName::SetDoubleAttribute(const CATBSTR & iObjId, const CATBSTR & iAttrName, double iAttrValue) \
{ \
return (ENVTIECALL(DNBIAMHIAttributeAccess,ENVTIETypeLetter,ENVTIELetter)SetDoubleAttribute(iObjId,iAttrName,iAttrValue)); \
} \
HRESULT __stdcall  ENVTIEName::SetBooleanAttribute(const CATBSTR & iObjId, const CATBSTR & iAttrName, CAT_VARIANT_BOOL iAttrValue) \
{ \
return (ENVTIECALL(DNBIAMHIAttributeAccess,ENVTIETypeLetter,ENVTIELetter)SetBooleanAttribute(iObjId,iAttrName,iAttrValue)); \
} \
HRESULT __stdcall  ENVTIEName::GetStringAttribute(const CATBSTR & iObjId, const CATBSTR & iAttrName, CATBSTR & oAttrValue) \
{ \
return (ENVTIECALL(DNBIAMHIAttributeAccess,ENVTIETypeLetter,ENVTIELetter)GetStringAttribute(iObjId,iAttrName,oAttrValue)); \
} \
HRESULT __stdcall  ENVTIEName::GetLongAttribute(const CATBSTR & iObjId, const CATBSTR & iAttrName, CATLONG & oAttrValue) \
{ \
return (ENVTIECALL(DNBIAMHIAttributeAccess,ENVTIETypeLetter,ENVTIELetter)GetLongAttribute(iObjId,iAttrName,oAttrValue)); \
} \
HRESULT __stdcall  ENVTIEName::GetDoubleAttribute(const CATBSTR & iObjId, const CATBSTR & iAttrName, double & oAttrValue) \
{ \
return (ENVTIECALL(DNBIAMHIAttributeAccess,ENVTIETypeLetter,ENVTIELetter)GetDoubleAttribute(iObjId,iAttrName,oAttrValue)); \
} \
HRESULT __stdcall  ENVTIEName::GetBooleanAttribute(const CATBSTR & iObjId, const CATBSTR & iAttrName, CAT_VARIANT_BOOL & oAttrValue) \
{ \
return (ENVTIECALL(DNBIAMHIAttributeAccess,ENVTIETypeLetter,ENVTIELetter)GetBooleanAttribute(iObjId,iAttrName,oAttrValue)); \
} \
HRESULT __stdcall  ENVTIEName::SetQueryObjectClass(const CATBSTR & iQueryObjectClass) \
{ \
return (ENVTIECALL(DNBIAMHIAttributeAccess,ENVTIETypeLetter,ENVTIELetter)SetQueryObjectClass(iQueryObjectClass)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIAMHIAttributeAccess,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIAMHIAttributeAccess,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIAMHIAttributeAccess,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIAMHIAttributeAccess,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIAMHIAttributeAccess,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIAMHIAttributeAccess(classe)    TIEDNBIAMHIAttributeAccess##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIAMHIAttributeAccess(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIAMHIAttributeAccess, classe) \
 \
 \
CATImplementTIEMethods(DNBIAMHIAttributeAccess, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIAMHIAttributeAccess, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIAMHIAttributeAccess, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIAMHIAttributeAccess, classe) \
 \
HRESULT __stdcall  TIEDNBIAMHIAttributeAccess##classe::SetStringAttribute(const CATBSTR & iObjId, const CATBSTR & iAttrName, const CATBSTR & iAttrValue) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&iObjId,&iAttrName,&iAttrValue); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetStringAttribute(iObjId,iAttrName,iAttrValue); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&iObjId,&iAttrName,&iAttrValue); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIAttributeAccess##classe::SetLongAttribute(const CATBSTR & iObjId, const CATBSTR & iAttrName, CATLONG iAttrValue) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&iObjId,&iAttrName,&iAttrValue); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetLongAttribute(iObjId,iAttrName,iAttrValue); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&iObjId,&iAttrName,&iAttrValue); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIAttributeAccess##classe::SetDoubleAttribute(const CATBSTR & iObjId, const CATBSTR & iAttrName, double iAttrValue) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&iObjId,&iAttrName,&iAttrValue); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetDoubleAttribute(iObjId,iAttrName,iAttrValue); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&iObjId,&iAttrName,&iAttrValue); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIAttributeAccess##classe::SetBooleanAttribute(const CATBSTR & iObjId, const CATBSTR & iAttrName, CAT_VARIANT_BOOL iAttrValue) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&iObjId,&iAttrName,&iAttrValue); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetBooleanAttribute(iObjId,iAttrName,iAttrValue); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&iObjId,&iAttrName,&iAttrValue); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIAttributeAccess##classe::GetStringAttribute(const CATBSTR & iObjId, const CATBSTR & iAttrName, CATBSTR & oAttrValue) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&iObjId,&iAttrName,&oAttrValue); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetStringAttribute(iObjId,iAttrName,oAttrValue); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&iObjId,&iAttrName,&oAttrValue); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIAttributeAccess##classe::GetLongAttribute(const CATBSTR & iObjId, const CATBSTR & iAttrName, CATLONG & oAttrValue) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&iObjId,&iAttrName,&oAttrValue); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetLongAttribute(iObjId,iAttrName,oAttrValue); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&iObjId,&iAttrName,&oAttrValue); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIAttributeAccess##classe::GetDoubleAttribute(const CATBSTR & iObjId, const CATBSTR & iAttrName, double & oAttrValue) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&iObjId,&iAttrName,&oAttrValue); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetDoubleAttribute(iObjId,iAttrName,oAttrValue); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&iObjId,&iAttrName,&oAttrValue); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIAttributeAccess##classe::GetBooleanAttribute(const CATBSTR & iObjId, const CATBSTR & iAttrName, CAT_VARIANT_BOOL & oAttrValue) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&iObjId,&iAttrName,&oAttrValue); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetBooleanAttribute(iObjId,iAttrName,oAttrValue); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&iObjId,&iAttrName,&oAttrValue); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIAttributeAccess##classe::SetQueryObjectClass(const CATBSTR & iQueryObjectClass) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&iQueryObjectClass); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetQueryObjectClass(iQueryObjectClass); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&iQueryObjectClass); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIAttributeAccess##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIAttributeAccess##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,11,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,11,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIAttributeAccess##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,12,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,12,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIAttributeAccess##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,13,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,13,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIAttributeAccess##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,14,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,14,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIAMHIAttributeAccess(classe) \
 \
 \
declare_TIE_DNBIAMHIAttributeAccess(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAMHIAttributeAccess##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAMHIAttributeAccess,"DNBIAMHIAttributeAccess",DNBIAMHIAttributeAccess::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAMHIAttributeAccess(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIAMHIAttributeAccess, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAMHIAttributeAccess##classe(classe::MetaObject(),DNBIAMHIAttributeAccess::MetaObject(),(void *)CreateTIEDNBIAMHIAttributeAccess##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIAMHIAttributeAccess(classe) \
 \
 \
declare_TIE_DNBIAMHIAttributeAccess(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAMHIAttributeAccess##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAMHIAttributeAccess,"DNBIAMHIAttributeAccess",DNBIAMHIAttributeAccess::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAMHIAttributeAccess(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIAMHIAttributeAccess, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAMHIAttributeAccess##classe(classe::MetaObject(),DNBIAMHIAttributeAccess::MetaObject(),(void *)CreateTIEDNBIAMHIAttributeAccess##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIAMHIAttributeAccess(classe) TIE_DNBIAMHIAttributeAccess(classe)
#else
#define BOA_DNBIAMHIAttributeAccess(classe) CATImplementBOA(DNBIAMHIAttributeAccess, classe)
#endif

#endif
