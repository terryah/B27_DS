#ifndef __TIE_DELMIAIPDPDProperty
#define __TIE_DELMIAIPDPDProperty

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DELMIAIPDPDProperty.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DELMIAIPDPDProperty */
#define declare_TIE_DELMIAIPDPDProperty(classe) \
 \
 \
class TIEDELMIAIPDPDProperty##classe : public DELMIAIPDPDProperty \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DELMIAIPDPDProperty, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall SetStringAttribute(const CATBSTR & iAttrName, const CATBSTR & iAttrValue); \
      virtual HRESULT __stdcall GetStringAttribute(const CATBSTR & iAttrName, CATBSTR & oAttrValue); \
      virtual HRESULT __stdcall SetLongAttribute(const CATBSTR & iAttrName, CATLONG iAttrValue); \
      virtual HRESULT __stdcall GetLongAttribute(const CATBSTR & iAttrName, CATLONG & oAttrValue); \
      virtual HRESULT __stdcall SetDoubleAttribute(const CATBSTR & iAttrName, double iAttrValue); \
      virtual HRESULT __stdcall GetDoubleAttribute(const CATBSTR & iAttrName, double & oAttrValue); \
      virtual HRESULT __stdcall SetBooleanAttribute(const CATBSTR & iAttrName, CAT_VARIANT_BOOL iAttrValue); \
      virtual HRESULT __stdcall GetBooleanAttribute(const CATBSTR & iAttrName, CAT_VARIANT_BOOL & oAttrValue); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DELMIAIPDPDProperty(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall SetStringAttribute(const CATBSTR & iAttrName, const CATBSTR & iAttrValue); \
virtual HRESULT __stdcall GetStringAttribute(const CATBSTR & iAttrName, CATBSTR & oAttrValue); \
virtual HRESULT __stdcall SetLongAttribute(const CATBSTR & iAttrName, CATLONG iAttrValue); \
virtual HRESULT __stdcall GetLongAttribute(const CATBSTR & iAttrName, CATLONG & oAttrValue); \
virtual HRESULT __stdcall SetDoubleAttribute(const CATBSTR & iAttrName, double iAttrValue); \
virtual HRESULT __stdcall GetDoubleAttribute(const CATBSTR & iAttrName, double & oAttrValue); \
virtual HRESULT __stdcall SetBooleanAttribute(const CATBSTR & iAttrName, CAT_VARIANT_BOOL iAttrValue); \
virtual HRESULT __stdcall GetBooleanAttribute(const CATBSTR & iAttrName, CAT_VARIANT_BOOL & oAttrValue); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DELMIAIPDPDProperty(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::SetStringAttribute(const CATBSTR & iAttrName, const CATBSTR & iAttrValue) \
{ \
return (ENVTIECALL(DELMIAIPDPDProperty,ENVTIETypeLetter,ENVTIELetter)SetStringAttribute(iAttrName,iAttrValue)); \
} \
HRESULT __stdcall  ENVTIEName::GetStringAttribute(const CATBSTR & iAttrName, CATBSTR & oAttrValue) \
{ \
return (ENVTIECALL(DELMIAIPDPDProperty,ENVTIETypeLetter,ENVTIELetter)GetStringAttribute(iAttrName,oAttrValue)); \
} \
HRESULT __stdcall  ENVTIEName::SetLongAttribute(const CATBSTR & iAttrName, CATLONG iAttrValue) \
{ \
return (ENVTIECALL(DELMIAIPDPDProperty,ENVTIETypeLetter,ENVTIELetter)SetLongAttribute(iAttrName,iAttrValue)); \
} \
HRESULT __stdcall  ENVTIEName::GetLongAttribute(const CATBSTR & iAttrName, CATLONG & oAttrValue) \
{ \
return (ENVTIECALL(DELMIAIPDPDProperty,ENVTIETypeLetter,ENVTIELetter)GetLongAttribute(iAttrName,oAttrValue)); \
} \
HRESULT __stdcall  ENVTIEName::SetDoubleAttribute(const CATBSTR & iAttrName, double iAttrValue) \
{ \
return (ENVTIECALL(DELMIAIPDPDProperty,ENVTIETypeLetter,ENVTIELetter)SetDoubleAttribute(iAttrName,iAttrValue)); \
} \
HRESULT __stdcall  ENVTIEName::GetDoubleAttribute(const CATBSTR & iAttrName, double & oAttrValue) \
{ \
return (ENVTIECALL(DELMIAIPDPDProperty,ENVTIETypeLetter,ENVTIELetter)GetDoubleAttribute(iAttrName,oAttrValue)); \
} \
HRESULT __stdcall  ENVTIEName::SetBooleanAttribute(const CATBSTR & iAttrName, CAT_VARIANT_BOOL iAttrValue) \
{ \
return (ENVTIECALL(DELMIAIPDPDProperty,ENVTIETypeLetter,ENVTIELetter)SetBooleanAttribute(iAttrName,iAttrValue)); \
} \
HRESULT __stdcall  ENVTIEName::GetBooleanAttribute(const CATBSTR & iAttrName, CAT_VARIANT_BOOL & oAttrValue) \
{ \
return (ENVTIECALL(DELMIAIPDPDProperty,ENVTIETypeLetter,ENVTIELetter)GetBooleanAttribute(iAttrName,oAttrValue)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DELMIAIPDPDProperty,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DELMIAIPDPDProperty,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DELMIAIPDPDProperty,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DELMIAIPDPDProperty,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DELMIAIPDPDProperty,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DELMIAIPDPDProperty(classe)    TIEDELMIAIPDPDProperty##classe


/* Common methods inside a TIE */
#define common_TIE_DELMIAIPDPDProperty(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DELMIAIPDPDProperty, classe) \
 \
 \
CATImplementTIEMethods(DELMIAIPDPDProperty, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DELMIAIPDPDProperty, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DELMIAIPDPDProperty, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DELMIAIPDPDProperty, classe) \
 \
HRESULT __stdcall  TIEDELMIAIPDPDProperty##classe::SetStringAttribute(const CATBSTR & iAttrName, const CATBSTR & iAttrValue) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&iAttrName,&iAttrValue); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetStringAttribute(iAttrName,iAttrValue); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&iAttrName,&iAttrValue); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAIPDPDProperty##classe::GetStringAttribute(const CATBSTR & iAttrName, CATBSTR & oAttrValue) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&iAttrName,&oAttrValue); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetStringAttribute(iAttrName,oAttrValue); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&iAttrName,&oAttrValue); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAIPDPDProperty##classe::SetLongAttribute(const CATBSTR & iAttrName, CATLONG iAttrValue) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&iAttrName,&iAttrValue); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetLongAttribute(iAttrName,iAttrValue); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&iAttrName,&iAttrValue); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAIPDPDProperty##classe::GetLongAttribute(const CATBSTR & iAttrName, CATLONG & oAttrValue) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&iAttrName,&oAttrValue); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetLongAttribute(iAttrName,oAttrValue); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&iAttrName,&oAttrValue); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAIPDPDProperty##classe::SetDoubleAttribute(const CATBSTR & iAttrName, double iAttrValue) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&iAttrName,&iAttrValue); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetDoubleAttribute(iAttrName,iAttrValue); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&iAttrName,&iAttrValue); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAIPDPDProperty##classe::GetDoubleAttribute(const CATBSTR & iAttrName, double & oAttrValue) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&iAttrName,&oAttrValue); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetDoubleAttribute(iAttrName,oAttrValue); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&iAttrName,&oAttrValue); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAIPDPDProperty##classe::SetBooleanAttribute(const CATBSTR & iAttrName, CAT_VARIANT_BOOL iAttrValue) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&iAttrName,&iAttrValue); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetBooleanAttribute(iAttrName,iAttrValue); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&iAttrName,&iAttrValue); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAIPDPDProperty##classe::GetBooleanAttribute(const CATBSTR & iAttrName, CAT_VARIANT_BOOL & oAttrValue) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&iAttrName,&oAttrValue); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetBooleanAttribute(iAttrName,oAttrValue); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&iAttrName,&oAttrValue); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAIPDPDProperty##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAIPDPDProperty##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAIPDPDProperty##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,11,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,11,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAIPDPDProperty##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,12,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,12,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAIPDPDProperty##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,13,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,13,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DELMIAIPDPDProperty(classe) \
 \
 \
declare_TIE_DELMIAIPDPDProperty(classe) \
 \
 \
CATMetaClass * __stdcall TIEDELMIAIPDPDProperty##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DELMIAIPDPDProperty,"DELMIAIPDPDProperty",DELMIAIPDPDProperty::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DELMIAIPDPDProperty(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DELMIAIPDPDProperty, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDELMIAIPDPDProperty##classe(classe::MetaObject(),DELMIAIPDPDProperty::MetaObject(),(void *)CreateTIEDELMIAIPDPDProperty##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DELMIAIPDPDProperty(classe) \
 \
 \
declare_TIE_DELMIAIPDPDProperty(classe) \
 \
 \
CATMetaClass * __stdcall TIEDELMIAIPDPDProperty##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DELMIAIPDPDProperty,"DELMIAIPDPDProperty",DELMIAIPDPDProperty::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DELMIAIPDPDProperty(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DELMIAIPDPDProperty, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDELMIAIPDPDProperty##classe(classe::MetaObject(),DELMIAIPDPDProperty::MetaObject(),(void *)CreateTIEDELMIAIPDPDProperty##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DELMIAIPDPDProperty(classe) TIE_DELMIAIPDPDProperty(classe)
#else
#define BOA_DELMIAIPDPDProperty(classe) CATImplementBOA(DELMIAIPDPDProperty, classe)
#endif

#endif
