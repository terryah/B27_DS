#ifndef __TIE_DNBIAIPDTemplateProperty
#define __TIE_DNBIAIPDTemplateProperty

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIAIPDTemplateProperty.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIAIPDTemplateProperty */
#define declare_TIE_DNBIAIPDTemplateProperty(classe) \
 \
 \
class TIEDNBIAIPDTemplateProperty##classe : public DNBIAIPDTemplateProperty \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIAIPDTemplateProperty, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall GetTemplateStringAttribute(const CATBSTR & iAttrName, CATBSTR & oAttrValue); \
      virtual HRESULT __stdcall GetTemplateLongAttribute(const CATBSTR & iAttrName, CATLONG & oAttrValue); \
      virtual HRESULT __stdcall GetTemplateDoubleAttribute(const CATBSTR & iAttrName, double & oAttrValue); \
      virtual HRESULT __stdcall GetTemplateBooleanAttribute(const CATBSTR & iAttrName, CAT_VARIANT_BOOL & oAttrValue); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIAIPDTemplateProperty(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall GetTemplateStringAttribute(const CATBSTR & iAttrName, CATBSTR & oAttrValue); \
virtual HRESULT __stdcall GetTemplateLongAttribute(const CATBSTR & iAttrName, CATLONG & oAttrValue); \
virtual HRESULT __stdcall GetTemplateDoubleAttribute(const CATBSTR & iAttrName, double & oAttrValue); \
virtual HRESULT __stdcall GetTemplateBooleanAttribute(const CATBSTR & iAttrName, CAT_VARIANT_BOOL & oAttrValue); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIAIPDTemplateProperty(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::GetTemplateStringAttribute(const CATBSTR & iAttrName, CATBSTR & oAttrValue) \
{ \
return (ENVTIECALL(DNBIAIPDTemplateProperty,ENVTIETypeLetter,ENVTIELetter)GetTemplateStringAttribute(iAttrName,oAttrValue)); \
} \
HRESULT __stdcall  ENVTIEName::GetTemplateLongAttribute(const CATBSTR & iAttrName, CATLONG & oAttrValue) \
{ \
return (ENVTIECALL(DNBIAIPDTemplateProperty,ENVTIETypeLetter,ENVTIELetter)GetTemplateLongAttribute(iAttrName,oAttrValue)); \
} \
HRESULT __stdcall  ENVTIEName::GetTemplateDoubleAttribute(const CATBSTR & iAttrName, double & oAttrValue) \
{ \
return (ENVTIECALL(DNBIAIPDTemplateProperty,ENVTIETypeLetter,ENVTIELetter)GetTemplateDoubleAttribute(iAttrName,oAttrValue)); \
} \
HRESULT __stdcall  ENVTIEName::GetTemplateBooleanAttribute(const CATBSTR & iAttrName, CAT_VARIANT_BOOL & oAttrValue) \
{ \
return (ENVTIECALL(DNBIAIPDTemplateProperty,ENVTIETypeLetter,ENVTIELetter)GetTemplateBooleanAttribute(iAttrName,oAttrValue)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIAIPDTemplateProperty,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIAIPDTemplateProperty,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIAIPDTemplateProperty,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIAIPDTemplateProperty,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIAIPDTemplateProperty,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIAIPDTemplateProperty(classe)    TIEDNBIAIPDTemplateProperty##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIAIPDTemplateProperty(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIAIPDTemplateProperty, classe) \
 \
 \
CATImplementTIEMethods(DNBIAIPDTemplateProperty, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIAIPDTemplateProperty, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIAIPDTemplateProperty, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIAIPDTemplateProperty, classe) \
 \
HRESULT __stdcall  TIEDNBIAIPDTemplateProperty##classe::GetTemplateStringAttribute(const CATBSTR & iAttrName, CATBSTR & oAttrValue) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&iAttrName,&oAttrValue); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetTemplateStringAttribute(iAttrName,oAttrValue); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&iAttrName,&oAttrValue); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAIPDTemplateProperty##classe::GetTemplateLongAttribute(const CATBSTR & iAttrName, CATLONG & oAttrValue) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&iAttrName,&oAttrValue); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetTemplateLongAttribute(iAttrName,oAttrValue); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&iAttrName,&oAttrValue); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAIPDTemplateProperty##classe::GetTemplateDoubleAttribute(const CATBSTR & iAttrName, double & oAttrValue) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&iAttrName,&oAttrValue); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetTemplateDoubleAttribute(iAttrName,oAttrValue); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&iAttrName,&oAttrValue); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAIPDTemplateProperty##classe::GetTemplateBooleanAttribute(const CATBSTR & iAttrName, CAT_VARIANT_BOOL & oAttrValue) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&iAttrName,&oAttrValue); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetTemplateBooleanAttribute(iAttrName,oAttrValue); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&iAttrName,&oAttrValue); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAIPDTemplateProperty##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAIPDTemplateProperty##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAIPDTemplateProperty##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAIPDTemplateProperty##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAIPDTemplateProperty##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIAIPDTemplateProperty(classe) \
 \
 \
declare_TIE_DNBIAIPDTemplateProperty(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAIPDTemplateProperty##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAIPDTemplateProperty,"DNBIAIPDTemplateProperty",DNBIAIPDTemplateProperty::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAIPDTemplateProperty(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIAIPDTemplateProperty, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAIPDTemplateProperty##classe(classe::MetaObject(),DNBIAIPDTemplateProperty::MetaObject(),(void *)CreateTIEDNBIAIPDTemplateProperty##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIAIPDTemplateProperty(classe) \
 \
 \
declare_TIE_DNBIAIPDTemplateProperty(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAIPDTemplateProperty##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAIPDTemplateProperty,"DNBIAIPDTemplateProperty",DNBIAIPDTemplateProperty::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAIPDTemplateProperty(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIAIPDTemplateProperty, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAIPDTemplateProperty##classe(classe::MetaObject(),DNBIAIPDTemplateProperty::MetaObject(),(void *)CreateTIEDNBIAIPDTemplateProperty##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIAIPDTemplateProperty(classe) TIE_DNBIAIPDTemplateProperty(classe)
#else
#define BOA_DNBIAIPDTemplateProperty(classe) CATImplementBOA(DNBIAIPDTemplateProperty, classe)
#endif

#endif
