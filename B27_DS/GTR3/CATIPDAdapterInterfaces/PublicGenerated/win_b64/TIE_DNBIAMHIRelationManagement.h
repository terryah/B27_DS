#ifndef __TIE_DNBIAMHIRelationManagement
#define __TIE_DNBIAMHIRelationManagement

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIAMHIRelationManagement.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIAMHIRelationManagement */
#define declare_TIE_DNBIAMHIRelationManagement(classe) \
 \
 \
class TIEDNBIAMHIRelationManagement##classe : public DNBIAMHIRelationManagement \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIAMHIRelationManagement, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall GetRelationObject(const CATBSTR & iRelationName, CATBaseDispatch * iAssignedObject, CATBaseDispatch *& oRelationObjAttrProvider); \
      virtual HRESULT __stdcall GetSupportedRelations(CATSafeArrayVariant *& oListSupportedRelations); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIAMHIRelationManagement(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall GetRelationObject(const CATBSTR & iRelationName, CATBaseDispatch * iAssignedObject, CATBaseDispatch *& oRelationObjAttrProvider); \
virtual HRESULT __stdcall GetSupportedRelations(CATSafeArrayVariant *& oListSupportedRelations); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIAMHIRelationManagement(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::GetRelationObject(const CATBSTR & iRelationName, CATBaseDispatch * iAssignedObject, CATBaseDispatch *& oRelationObjAttrProvider) \
{ \
return (ENVTIECALL(DNBIAMHIRelationManagement,ENVTIETypeLetter,ENVTIELetter)GetRelationObject(iRelationName,iAssignedObject,oRelationObjAttrProvider)); \
} \
HRESULT __stdcall  ENVTIEName::GetSupportedRelations(CATSafeArrayVariant *& oListSupportedRelations) \
{ \
return (ENVTIECALL(DNBIAMHIRelationManagement,ENVTIETypeLetter,ENVTIELetter)GetSupportedRelations(oListSupportedRelations)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIAMHIRelationManagement,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIAMHIRelationManagement,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIAMHIRelationManagement,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIAMHIRelationManagement,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIAMHIRelationManagement,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIAMHIRelationManagement(classe)    TIEDNBIAMHIRelationManagement##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIAMHIRelationManagement(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIAMHIRelationManagement, classe) \
 \
 \
CATImplementTIEMethods(DNBIAMHIRelationManagement, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIAMHIRelationManagement, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIAMHIRelationManagement, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIAMHIRelationManagement, classe) \
 \
HRESULT __stdcall  TIEDNBIAMHIRelationManagement##classe::GetRelationObject(const CATBSTR & iRelationName, CATBaseDispatch * iAssignedObject, CATBaseDispatch *& oRelationObjAttrProvider) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&iRelationName,&iAssignedObject,&oRelationObjAttrProvider); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetRelationObject(iRelationName,iAssignedObject,oRelationObjAttrProvider); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&iRelationName,&iAssignedObject,&oRelationObjAttrProvider); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIRelationManagement##classe::GetSupportedRelations(CATSafeArrayVariant *& oListSupportedRelations) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&oListSupportedRelations); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetSupportedRelations(oListSupportedRelations); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&oListSupportedRelations); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIRelationManagement##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIRelationManagement##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIRelationManagement##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIRelationManagement##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIRelationManagement##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIAMHIRelationManagement(classe) \
 \
 \
declare_TIE_DNBIAMHIRelationManagement(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAMHIRelationManagement##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAMHIRelationManagement,"DNBIAMHIRelationManagement",DNBIAMHIRelationManagement::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAMHIRelationManagement(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIAMHIRelationManagement, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAMHIRelationManagement##classe(classe::MetaObject(),DNBIAMHIRelationManagement::MetaObject(),(void *)CreateTIEDNBIAMHIRelationManagement##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIAMHIRelationManagement(classe) \
 \
 \
declare_TIE_DNBIAMHIRelationManagement(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAMHIRelationManagement##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAMHIRelationManagement,"DNBIAMHIRelationManagement",DNBIAMHIRelationManagement::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAMHIRelationManagement(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIAMHIRelationManagement, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAMHIRelationManagement##classe(classe::MetaObject(),DNBIAMHIRelationManagement::MetaObject(),(void *)CreateTIEDNBIAMHIRelationManagement##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIAMHIRelationManagement(classe) TIE_DNBIAMHIRelationManagement(classe)
#else
#define BOA_DNBIAMHIRelationManagement(classe) CATImplementBOA(DNBIAMHIRelationManagement, classe)
#endif

#endif
