#ifndef __TIE_DNBIAMHIPropertyAccess
#define __TIE_DNBIAMHIPropertyAccess

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIAMHIPropertyAccess.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIAMHIPropertyAccess */
#define declare_TIE_DNBIAMHIPropertyAccess(classe) \
 \
 \
class TIEDNBIAMHIPropertyAccess##classe : public DNBIAMHIPropertyAccess \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIAMHIPropertyAccess, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall SetStringAttribute(const CATBSTR & iAttrName, const CATBSTR & iAttrValue); \
      virtual HRESULT __stdcall GetStringAttribute(const CATBSTR & iAttrName, CATBSTR & oAttrValue); \
      virtual HRESULT __stdcall SetLongAttribute(const CATBSTR & iAttrName, CATLONG iAttrValue); \
      virtual HRESULT __stdcall GetLongAttribute(const CATBSTR & iAttrName, CATLONG & oAttrValue); \
      virtual HRESULT __stdcall SetDoubleAttribute(const CATBSTR & iAttrName, double iAttrValue); \
      virtual HRESULT __stdcall GetDoubleAttribute(const CATBSTR & iAttrName, double & oAttrValue); \
      virtual HRESULT __stdcall SetBooleanAttribute(const CATBSTR & iAttrName, CAT_VARIANT_BOOL iAttrValue); \
      virtual HRESULT __stdcall GetBooleanAttribute(const CATBSTR & iAttrName, CAT_VARIANT_BOOL & oAttrValue); \
      virtual HRESULT __stdcall SetQueryObjectClass(const CATBSTR & iQueryObjectClass); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIAMHIPropertyAccess(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall SetStringAttribute(const CATBSTR & iAttrName, const CATBSTR & iAttrValue); \
virtual HRESULT __stdcall GetStringAttribute(const CATBSTR & iAttrName, CATBSTR & oAttrValue); \
virtual HRESULT __stdcall SetLongAttribute(const CATBSTR & iAttrName, CATLONG iAttrValue); \
virtual HRESULT __stdcall GetLongAttribute(const CATBSTR & iAttrName, CATLONG & oAttrValue); \
virtual HRESULT __stdcall SetDoubleAttribute(const CATBSTR & iAttrName, double iAttrValue); \
virtual HRESULT __stdcall GetDoubleAttribute(const CATBSTR & iAttrName, double & oAttrValue); \
virtual HRESULT __stdcall SetBooleanAttribute(const CATBSTR & iAttrName, CAT_VARIANT_BOOL iAttrValue); \
virtual HRESULT __stdcall GetBooleanAttribute(const CATBSTR & iAttrName, CAT_VARIANT_BOOL & oAttrValue); \
virtual HRESULT __stdcall SetQueryObjectClass(const CATBSTR & iQueryObjectClass); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIAMHIPropertyAccess(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::SetStringAttribute(const CATBSTR & iAttrName, const CATBSTR & iAttrValue) \
{ \
return (ENVTIECALL(DNBIAMHIPropertyAccess,ENVTIETypeLetter,ENVTIELetter)SetStringAttribute(iAttrName,iAttrValue)); \
} \
HRESULT __stdcall  ENVTIEName::GetStringAttribute(const CATBSTR & iAttrName, CATBSTR & oAttrValue) \
{ \
return (ENVTIECALL(DNBIAMHIPropertyAccess,ENVTIETypeLetter,ENVTIELetter)GetStringAttribute(iAttrName,oAttrValue)); \
} \
HRESULT __stdcall  ENVTIEName::SetLongAttribute(const CATBSTR & iAttrName, CATLONG iAttrValue) \
{ \
return (ENVTIECALL(DNBIAMHIPropertyAccess,ENVTIETypeLetter,ENVTIELetter)SetLongAttribute(iAttrName,iAttrValue)); \
} \
HRESULT __stdcall  ENVTIEName::GetLongAttribute(const CATBSTR & iAttrName, CATLONG & oAttrValue) \
{ \
return (ENVTIECALL(DNBIAMHIPropertyAccess,ENVTIETypeLetter,ENVTIELetter)GetLongAttribute(iAttrName,oAttrValue)); \
} \
HRESULT __stdcall  ENVTIEName::SetDoubleAttribute(const CATBSTR & iAttrName, double iAttrValue) \
{ \
return (ENVTIECALL(DNBIAMHIPropertyAccess,ENVTIETypeLetter,ENVTIELetter)SetDoubleAttribute(iAttrName,iAttrValue)); \
} \
HRESULT __stdcall  ENVTIEName::GetDoubleAttribute(const CATBSTR & iAttrName, double & oAttrValue) \
{ \
return (ENVTIECALL(DNBIAMHIPropertyAccess,ENVTIETypeLetter,ENVTIELetter)GetDoubleAttribute(iAttrName,oAttrValue)); \
} \
HRESULT __stdcall  ENVTIEName::SetBooleanAttribute(const CATBSTR & iAttrName, CAT_VARIANT_BOOL iAttrValue) \
{ \
return (ENVTIECALL(DNBIAMHIPropertyAccess,ENVTIETypeLetter,ENVTIELetter)SetBooleanAttribute(iAttrName,iAttrValue)); \
} \
HRESULT __stdcall  ENVTIEName::GetBooleanAttribute(const CATBSTR & iAttrName, CAT_VARIANT_BOOL & oAttrValue) \
{ \
return (ENVTIECALL(DNBIAMHIPropertyAccess,ENVTIETypeLetter,ENVTIELetter)GetBooleanAttribute(iAttrName,oAttrValue)); \
} \
HRESULT __stdcall  ENVTIEName::SetQueryObjectClass(const CATBSTR & iQueryObjectClass) \
{ \
return (ENVTIECALL(DNBIAMHIPropertyAccess,ENVTIETypeLetter,ENVTIELetter)SetQueryObjectClass(iQueryObjectClass)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIAMHIPropertyAccess,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIAMHIPropertyAccess,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIAMHIPropertyAccess,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIAMHIPropertyAccess,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIAMHIPropertyAccess,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIAMHIPropertyAccess(classe)    TIEDNBIAMHIPropertyAccess##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIAMHIPropertyAccess(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIAMHIPropertyAccess, classe) \
 \
 \
CATImplementTIEMethods(DNBIAMHIPropertyAccess, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIAMHIPropertyAccess, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIAMHIPropertyAccess, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIAMHIPropertyAccess, classe) \
 \
HRESULT __stdcall  TIEDNBIAMHIPropertyAccess##classe::SetStringAttribute(const CATBSTR & iAttrName, const CATBSTR & iAttrValue) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&iAttrName,&iAttrValue); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetStringAttribute(iAttrName,iAttrValue); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&iAttrName,&iAttrValue); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIPropertyAccess##classe::GetStringAttribute(const CATBSTR & iAttrName, CATBSTR & oAttrValue) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&iAttrName,&oAttrValue); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetStringAttribute(iAttrName,oAttrValue); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&iAttrName,&oAttrValue); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIPropertyAccess##classe::SetLongAttribute(const CATBSTR & iAttrName, CATLONG iAttrValue) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&iAttrName,&iAttrValue); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetLongAttribute(iAttrName,iAttrValue); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&iAttrName,&iAttrValue); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIPropertyAccess##classe::GetLongAttribute(const CATBSTR & iAttrName, CATLONG & oAttrValue) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&iAttrName,&oAttrValue); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetLongAttribute(iAttrName,oAttrValue); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&iAttrName,&oAttrValue); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIPropertyAccess##classe::SetDoubleAttribute(const CATBSTR & iAttrName, double iAttrValue) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&iAttrName,&iAttrValue); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetDoubleAttribute(iAttrName,iAttrValue); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&iAttrName,&iAttrValue); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIPropertyAccess##classe::GetDoubleAttribute(const CATBSTR & iAttrName, double & oAttrValue) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&iAttrName,&oAttrValue); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetDoubleAttribute(iAttrName,oAttrValue); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&iAttrName,&oAttrValue); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIPropertyAccess##classe::SetBooleanAttribute(const CATBSTR & iAttrName, CAT_VARIANT_BOOL iAttrValue) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&iAttrName,&iAttrValue); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetBooleanAttribute(iAttrName,iAttrValue); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&iAttrName,&iAttrValue); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIPropertyAccess##classe::GetBooleanAttribute(const CATBSTR & iAttrName, CAT_VARIANT_BOOL & oAttrValue) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&iAttrName,&oAttrValue); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetBooleanAttribute(iAttrName,oAttrValue); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&iAttrName,&oAttrValue); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMHIPropertyAccess##classe::SetQueryObjectClass(const CATBSTR & iQueryObjectClass) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&iQueryObjectClass); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetQueryObjectClass(iQueryObjectClass); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&iQueryObjectClass); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIPropertyAccess##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIPropertyAccess##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,11,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,11,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIPropertyAccess##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,12,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,12,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIPropertyAccess##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,13,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,13,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMHIPropertyAccess##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,14,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,14,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIAMHIPropertyAccess(classe) \
 \
 \
declare_TIE_DNBIAMHIPropertyAccess(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAMHIPropertyAccess##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAMHIPropertyAccess,"DNBIAMHIPropertyAccess",DNBIAMHIPropertyAccess::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAMHIPropertyAccess(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIAMHIPropertyAccess, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAMHIPropertyAccess##classe(classe::MetaObject(),DNBIAMHIPropertyAccess::MetaObject(),(void *)CreateTIEDNBIAMHIPropertyAccess##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIAMHIPropertyAccess(classe) \
 \
 \
declare_TIE_DNBIAMHIPropertyAccess(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAMHIPropertyAccess##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAMHIPropertyAccess,"DNBIAMHIPropertyAccess",DNBIAMHIPropertyAccess::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAMHIPropertyAccess(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIAMHIPropertyAccess, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAMHIPropertyAccess##classe(classe::MetaObject(),DNBIAMHIPropertyAccess::MetaObject(),(void *)CreateTIEDNBIAMHIPropertyAccess##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIAMHIPropertyAccess(classe) TIE_DNBIAMHIPropertyAccess(classe)
#else
#define BOA_DNBIAMHIPropertyAccess(classe) CATImplementBOA(DNBIAMHIPropertyAccess, classe)
#endif

#endif
