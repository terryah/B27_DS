/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef DNBIAMHIAttributeAccess_h
#define DNBIAMHIAttributeAccess_h

#include "CATCORBABoolean.h"
#include "CAT_VARIANT_BOOL.h"

#ifndef ExportedByMfgHubPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __MfgHubPubIDL
#define ExportedByMfgHubPubIDL __declspec(dllexport)
#else
#define ExportedByMfgHubPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByMfgHubPubIDL
#endif
#endif

#include "CATBSTR.h"
#include "CATIABase.h"

class CATIAItem;

extern ExportedByMfgHubPubIDL IID IID_DNBIAMHIAttributeAccess;

class ExportedByMfgHubPubIDL DNBIAMHIAttributeAccess : public CATIABase
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall SetStringAttribute(const CATBSTR & iObjId, const CATBSTR & iAttrName, const CATBSTR & iAttrValue)=0;

    virtual HRESULT __stdcall SetLongAttribute(const CATBSTR & iObjId, const CATBSTR & iAttrName, CATLONG iAttrValue)=0;

    virtual HRESULT __stdcall SetDoubleAttribute(const CATBSTR & iObjId, const CATBSTR & iAttrName, double iAttrValue)=0;

    virtual HRESULT __stdcall SetBooleanAttribute(const CATBSTR & iObjId, const CATBSTR & iAttrName, CAT_VARIANT_BOOL iAttrValue)=0;

    virtual HRESULT __stdcall GetStringAttribute(const CATBSTR & iObjId, const CATBSTR & iAttrName, CATBSTR & oAttrValue)=0;

    virtual HRESULT __stdcall GetLongAttribute(const CATBSTR & iObjId, const CATBSTR & iAttrName, CATLONG & oAttrValue)=0;

    virtual HRESULT __stdcall GetDoubleAttribute(const CATBSTR & iObjId, const CATBSTR & iAttrName, double & oAttrValue)=0;

    virtual HRESULT __stdcall GetBooleanAttribute(const CATBSTR & iObjId, const CATBSTR & iAttrName, CAT_VARIANT_BOOL & oAttrValue)=0;

    virtual HRESULT __stdcall SetQueryObjectClass(const CATBSTR & iQueryObjectClass)=0;


};

CATDeclareHandler(DNBIAMHIAttributeAccess, CATIABase);

#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATSafeArray.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
