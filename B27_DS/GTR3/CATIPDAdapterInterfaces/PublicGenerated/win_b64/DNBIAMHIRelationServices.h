/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef DNBIAMHIRelationServices_h
#define DNBIAMHIRelationServices_h

#ifndef ExportedByMfgHubPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __MfgHubPubIDL
#define ExportedByMfgHubPubIDL __declspec(dllexport)
#else
#define ExportedByMfgHubPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByMfgHubPubIDL
#endif
#endif

#include "CATBSTR.h"
#include "CATIABase.h"
#include "CATSafeArray.h"

class CATIAItem;

extern ExportedByMfgHubPubIDL IID IID_DNBIAMHIRelationServices;

class ExportedByMfgHubPubIDL DNBIAMHIRelationServices : public CATIABase
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall GetRelation(const CATBSTR & iRelationSource, const CATBSTR & iRelationTarget, const CATBSTR & iRelationName, CATBSTR & oRelationObjectID)=0;

    virtual HRESULT __stdcall GetRelations(const CATSafeArrayVariant & iRelationSources, const CATSafeArrayVariant & iRelationTargets, const CATBSTR & iRelationName, CATSafeArrayVariant *& oListRelationObjectIDs)=0;


};

CATDeclareHandler(DNBIAMHIRelationServices, CATIABase);

#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
