#ifndef __TIE_DNBIAMfgHubSettingAtt
#define __TIE_DNBIAMfgHubSettingAtt

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIAMfgHubSettingAtt.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIAMfgHubSettingAtt */
#define declare_TIE_DNBIAMfgHubSettingAtt(classe) \
 \
 \
class TIEDNBIAMfgHubSettingAtt##classe : public DNBIAMfgHubSettingAtt \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIAMfgHubSettingAtt, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall get_AutoLoadMfgCtx(CAT_VARIANT_BOOL & oAutoLoadMfgCtx); \
      virtual HRESULT __stdcall put_AutoLoadMfgCtx(CAT_VARIANT_BOOL iAutoLoadMfgCtx); \
      virtual HRESULT __stdcall GetAutoLoadMfgCtxInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetAutoLoadMfgCtxLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_SaveV5CalcTime(CAT_VARIANT_BOOL & oSaveV5CalcTime); \
      virtual HRESULT __stdcall put_SaveV5CalcTime(CAT_VARIANT_BOOL iSaveV5CalcTime); \
      virtual HRESULT __stdcall GetSaveV5CalcTimeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetSaveV5CalcTimeLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_SavePPRNoDetailing(CAT_VARIANT_BOOL & oSavePPRNoDetailing); \
      virtual HRESULT __stdcall put_SavePPRNoDetailing(CAT_VARIANT_BOOL iSavePPRNoDetailing); \
      virtual HRESULT __stdcall GetSavePPRNoDetailingInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetSavePPRNoDetailingLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_SaveShowEffctvtPanel(CAT_VARIANT_BOOL & oSaveShowEffctvtPanel); \
      virtual HRESULT __stdcall put_SaveShowEffctvtPanel(CAT_VARIANT_BOOL iSaveShowEffctvtPanel); \
      virtual HRESULT __stdcall GetSaveShowEffctvtPanelInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetSaveShowEffctvtPanelLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_LoadENVGeomFromENVdb(CAT_VARIANT_BOOL & oLoadENVGeomFromENVdb); \
      virtual HRESULT __stdcall put_LoadENVGeomFromENVdb(CAT_VARIANT_BOOL iLoadENVGeomFromENVdb); \
      virtual HRESULT __stdcall GetLoadENVGeomFromENVdbInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetLoadENVGeomFromENVdbLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_RmvNotAssgnPrdResOnSync(CAT_VARIANT_BOOL & oRmvNotAssgnPrdResOnSync); \
      virtual HRESULT __stdcall put_RmvNotAssgnPrdResOnSync(CAT_VARIANT_BOOL iRmvNotAssgnPrdResOnSync); \
      virtual HRESULT __stdcall GetRmvNotAssgnPrdResOnSyncInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetRmvNotAssgnPrdResOnSyncLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_LockAssignedPrdOnLoad(CAT_VARIANT_BOOL & oLockAssignedPrdOnLoad); \
      virtual HRESULT __stdcall put_LockAssignedPrdOnLoad(CAT_VARIANT_BOOL iLockAssignedPrdOnLoad); \
      virtual HRESULT __stdcall GetLockAssignedPrdOnLoadInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetLockAssignedPrdOnLoadLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_Load3dStateAndPos(CAT_VARIANT_BOOL & oLoad3dStateAndPos); \
      virtual HRESULT __stdcall put_Load3dStateAndPos(CAT_VARIANT_BOOL iLoad3dStateAndPos); \
      virtual HRESULT __stdcall GetLoad3dStateAndPosInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetLoad3dStateAndPosLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_LoadChildProcMfgCtx(CAT_VARIANT_BOOL & oLoadChildProcMfgCtx); \
      virtual HRESULT __stdcall put_LoadChildProcMfgCtx(CAT_VARIANT_BOOL iLoadChildProcMfgCtx); \
      virtual HRESULT __stdcall GetLoadChildProcMfgCtxInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetLoadChildProcMfgCtxLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_LoadUnconstrainedMfgCtx(CAT_VARIANT_BOOL & oLoadUnconstrainedMfgCtx); \
      virtual HRESULT __stdcall put_LoadUnconstrainedMfgCtx(CAT_VARIANT_BOOL iLoadUnconstrainedMfgCtx); \
      virtual HRESULT __stdcall GetLoadUnconstrainedMfgCtxInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetLoadUnconstrainedMfgCtxLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_LoadFeedProcMfgCtx(CAT_VARIANT_BOOL & oLoadFeedProcMfgCtx); \
      virtual HRESULT __stdcall put_LoadFeedProcMfgCtx(CAT_VARIANT_BOOL iLoadFeedProcMfgCtx); \
      virtual HRESULT __stdcall GetLoadFeedProcMfgCtxInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetLoadFeedProcMfgCtxLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_PrevProcParseTypeForMfgCtx(short & oPrevProcParseTypeForMfgCtx); \
      virtual HRESULT __stdcall put_PrevProcParseTypeForMfgCtx(short iPrevProcParseTypeForMfgCtx); \
      virtual HRESULT __stdcall GetPrevProcParseTypeForMfgCtxInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetPrevProcParseTypeForMfgCtxLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_LoadPrdResUserAttribs(CAT_VARIANT_BOOL & oLoadPrdResUserAttribs); \
      virtual HRESULT __stdcall put_LoadPrdResUserAttribs(CAT_VARIANT_BOOL iLoadPrdResUserAttribs); \
      virtual HRESULT __stdcall GetLoadPrdResUserAttribsInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetLoadPrdResUserAttribsLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_LoadMfgAssmbly(CAT_VARIANT_BOOL & oLoadMfgAssmbly); \
      virtual HRESULT __stdcall put_LoadMfgAssmbly(CAT_VARIANT_BOOL iLoadMfgAssmbly); \
      virtual HRESULT __stdcall GetLoadMfgAssmblyInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetLoadMfgAssmblyLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_LoadMfgKits(CAT_VARIANT_BOOL & oLoadMfgKits); \
      virtual HRESULT __stdcall put_LoadMfgKits(CAT_VARIANT_BOOL iLoadMfgKits); \
      virtual HRESULT __stdcall GetLoadMfgKitsInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetLoadMfgKitsLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_SaveRelationToUnExposedPart(CAT_VARIANT_BOOL & oSaveRelationToUnExposedPart); \
      virtual HRESULT __stdcall put_SaveRelationToUnExposedPart(CAT_VARIANT_BOOL iSaveRelationToUnExposedPart); \
      virtual HRESULT __stdcall GetSaveRelationToUnExposedPartInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetSaveRelationToUnExposedPartLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_LoadDispMfgCtx(CAT_VARIANT_BOOL & oAutoLoadMfgCtx); \
      virtual HRESULT __stdcall put_LoadDispMfgCtx(CAT_VARIANT_BOOL iLoadDispMfgCtx); \
      virtual HRESULT __stdcall GetLoadDispMfgCtxInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetLoadDispMfgCtxLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_MfgCtxPrevProcRelationType(short & oPrevProcRelationType); \
      virtual HRESULT __stdcall put_MfgCtxPrevProcRelationType(short iPrevProcRelationType); \
      virtual HRESULT __stdcall GetMfgCtxPrevProcRelationTypeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetMfgCtxPrevProcRelationTypeLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_LoadAllChildProcMfgCtx(CAT_VARIANT_BOOL & oLoadAllChildProcMfgCtx); \
      virtual HRESULT __stdcall put_LoadAllChildProcMfgCtx(CAT_VARIANT_BOOL iLoadAllChildProcMfgCtx); \
      virtual HRESULT __stdcall GetLoadAllChildProcMfgCtxInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetLoadAllChildProcMfgCtxLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_LoadResGeo(CAT_VARIANT_BOOL & oLoadResGeo); \
      virtual HRESULT __stdcall put_LoadResGeo(CAT_VARIANT_BOOL iLoadResGeo); \
      virtual HRESULT __stdcall GetLoadResGeoInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetLoadResGeoLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_LoadAssocPrdResChild(CAT_VARIANT_BOOL & oLoadAssocPrdResChild); \
      virtual HRESULT __stdcall put_LoadAssocPrdResChild(CAT_VARIANT_BOOL iLoadAssocPrdResChild); \
      virtual HRESULT __stdcall GetLoadAssocPrdResChildInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetLoadAssocPrdResChildLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_ApplyLabelEffToAltChild(CAT_VARIANT_BOOL & oApplyLabelEffToAltChild); \
      virtual HRESULT __stdcall put_ApplyLabelEffToAltChild(CAT_VARIANT_BOOL iApplyLabelEffToAltChild); \
      virtual HRESULT __stdcall GetApplyLabelEffToAltChildInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetApplyLabelEffToAltChildLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_IssueRepositoryPath(CATBSTR & ioIssueRepositoryPath); \
      virtual HRESULT __stdcall put_IssueRepositoryPath(const CATBSTR & iIssueRepositoryPath); \
      virtual HRESULT __stdcall GetIssueRepositoryPathInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetIssueRepositoryPathLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_MailClientLaunchMode(CAT_VARIANT_BOOL & oMailClientLaunchMode); \
      virtual HRESULT __stdcall put_MailClientLaunchMode(CAT_VARIANT_BOOL iMailClientLaunchMode); \
      virtual HRESULT __stdcall GetMailClientLaunchModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetMailClientLaunchModeLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_LinkSendMode(short & oLinkSendMode); \
      virtual HRESULT __stdcall put_LinkSendMode(short iLinkSendMode); \
      virtual HRESULT __stdcall GetLinkSendModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetLinkSendModeLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_ShowOnlyFilteredObjects(CAT_VARIANT_BOOL & oShowOnlyFilteredObjects); \
      virtual HRESULT __stdcall put_ShowOnlyFilteredObjects(CAT_VARIANT_BOOL iShowOnlyFilteredObjects); \
      virtual HRESULT __stdcall GetShowOnlyFilteredObjectsInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetShowOnlyFilteredObjectsLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_LoadPSSData(CAT_VARIANT_BOOL & oLoadPSSData); \
      virtual HRESULT __stdcall put_LoadPSSData(CAT_VARIANT_BOOL iLoadPSSData); \
      virtual HRESULT __stdcall GetLoadPSSDataInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetLoadPSSDataLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_ProcProdRelations(CATSafeArrayVariant *& oProcProdRelations); \
      virtual HRESULT __stdcall put_ProcProdRelations(const CATSafeArrayVariant & iProcProdRelations); \
      virtual HRESULT __stdcall GetProcProdRelationsInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetProcProdRelationsLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_ProcResRelations(CATSafeArrayVariant *& oProcResRelations); \
      virtual HRESULT __stdcall put_ProcResRelations(const CATSafeArrayVariant & iProcResRelations); \
      virtual HRESULT __stdcall GetProcResRelationsInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetProcResRelationsLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_OpenModeForLoad(CATLONG & oOpenModeForLoad); \
      virtual HRESULT __stdcall put_OpenModeForLoad(CATLONG iOpenModeForLoad); \
      virtual HRESULT __stdcall GetOpenModeForLoadInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetOpenModeForLoadLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_SaveControlFlowInPROMode(CAT_VARIANT_BOOL & oSaveControlFlow); \
      virtual HRESULT __stdcall put_SaveControlFlowInPROMode(CAT_VARIANT_BOOL iSaveControlFlow); \
      virtual HRESULT __stdcall GetSaveControlFlowInPROModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetSaveControlFlowInPROModeLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_PackAndGoRepositoryPath(CATBSTR & ioPackAndGoRepositoryPath); \
      virtual HRESULT __stdcall put_PackAndGoRepositoryPath(const CATBSTR & iPackAndGoRepositoryPath); \
      virtual HRESULT __stdcall GetPackAndGoRepositoryPathInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetPackAndGoRepositoryPathLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_AutoLoadVolCtx(CAT_VARIANT_BOOL & oAutoLoadVolCtx); \
      virtual HRESULT __stdcall put_AutoLoadVolCtx(CAT_VARIANT_BOOL iAutoLoadVolCtx); \
      virtual HRESULT __stdcall GetAutoLoadVolCtxInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetAutoLoadVolCtxLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_AutoLoadSrvMfgCtx(CAT_VARIANT_BOOL & oAutoLoadSrvMfgCtx); \
      virtual HRESULT __stdcall put_AutoLoadSrvMfgCtx(CAT_VARIANT_BOOL iAutoLoadSrvMfgCtx); \
      virtual HRESULT __stdcall GetAutoLoadSrvMfgCtxInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetAutoLoadSrvMfgCtxLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_OnlyLoadCtxWithGeometry(CAT_VARIANT_BOOL & oOnlyLoadCtxWithGeometry); \
      virtual HRESULT __stdcall put_OnlyLoadCtxWithGeometry(CAT_VARIANT_BOOL iOnlyLoadCtxWithGeometry); \
      virtual HRESULT __stdcall GetOnlyLoadCtxWithGeometryInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetOnlyLoadCtxWithGeometryLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_LoadCtxWithFileGeometry(CAT_VARIANT_BOOL & oLoadCtxWithFileGeometry); \
      virtual HRESULT __stdcall put_LoadCtxWithFileGeometry(CAT_VARIANT_BOOL iLoadCtxWithFileGeometry); \
      virtual HRESULT __stdcall GetLoadCtxWithFileGeometryInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetLoadCtxWithFileGeometryLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_DisableShapeRollUp(CAT_VARIANT_BOOL & oDisableShapeRollUp); \
      virtual HRESULT __stdcall put_DisableShapeRollUp(CAT_VARIANT_BOOL iDisableShapeRollUp); \
      virtual HRESULT __stdcall GetDisableShapeRollUpInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetDisableShapeRollUpLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_AppendContextChkB(CAT_VARIANT_BOOL & oAppendContextChkB); \
      virtual HRESULT __stdcall put_AppendContextChkB(CAT_VARIANT_BOOL iAppendContextChkB); \
      virtual HRESULT __stdcall GetAppendContextChkBInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetAppendContextChkBLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_LoadDuplicatesInContextTreeChkB(CAT_VARIANT_BOOL & oLoadDuplicatesChkB); \
      virtual HRESULT __stdcall put_LoadDuplicatesInContextTreeChkB(CAT_VARIANT_BOOL iLoadDuplicatesChkB); \
      virtual HRESULT __stdcall GetLoadDuplicatesInContextTreeChkBInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetLoadDuplicatesInContextTreeChkBLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_PostLoadScriptOption(CAT_VARIANT_BOOL & oPostLoadScriptOption); \
      virtual HRESULT __stdcall put_PostLoadScriptOption(CAT_VARIANT_BOOL iPostLoadScriptOption); \
      virtual HRESULT __stdcall GetPostLoadScriptOptionInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetPostLoadScriptOptionLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_PostLoadScriptPath(CATBSTR & ioPostLoadScriptPath); \
      virtual HRESULT __stdcall put_PostLoadScriptPath(const CATBSTR & iPostLoadScriptPath); \
      virtual HRESULT __stdcall GetPostLoadScriptPathInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetPostLoadScriptPathLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_PostLoadVBAModule(CATBSTR & ioPostLoadVBAModule); \
      virtual HRESULT __stdcall put_PostLoadVBAModule(const CATBSTR & iPostLoadVBAModule); \
      virtual HRESULT __stdcall GetPostLoadVBAModuleInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetPostLoadVBAModuleLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall Commit(); \
      virtual HRESULT __stdcall Rollback(); \
      virtual HRESULT __stdcall ResetToAdminValues(); \
      virtual HRESULT __stdcall ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList); \
      virtual HRESULT __stdcall SaveRepository(); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIAMfgHubSettingAtt(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall get_AutoLoadMfgCtx(CAT_VARIANT_BOOL & oAutoLoadMfgCtx); \
virtual HRESULT __stdcall put_AutoLoadMfgCtx(CAT_VARIANT_BOOL iAutoLoadMfgCtx); \
virtual HRESULT __stdcall GetAutoLoadMfgCtxInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetAutoLoadMfgCtxLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_SaveV5CalcTime(CAT_VARIANT_BOOL & oSaveV5CalcTime); \
virtual HRESULT __stdcall put_SaveV5CalcTime(CAT_VARIANT_BOOL iSaveV5CalcTime); \
virtual HRESULT __stdcall GetSaveV5CalcTimeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetSaveV5CalcTimeLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_SavePPRNoDetailing(CAT_VARIANT_BOOL & oSavePPRNoDetailing); \
virtual HRESULT __stdcall put_SavePPRNoDetailing(CAT_VARIANT_BOOL iSavePPRNoDetailing); \
virtual HRESULT __stdcall GetSavePPRNoDetailingInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetSavePPRNoDetailingLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_SaveShowEffctvtPanel(CAT_VARIANT_BOOL & oSaveShowEffctvtPanel); \
virtual HRESULT __stdcall put_SaveShowEffctvtPanel(CAT_VARIANT_BOOL iSaveShowEffctvtPanel); \
virtual HRESULT __stdcall GetSaveShowEffctvtPanelInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetSaveShowEffctvtPanelLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_LoadENVGeomFromENVdb(CAT_VARIANT_BOOL & oLoadENVGeomFromENVdb); \
virtual HRESULT __stdcall put_LoadENVGeomFromENVdb(CAT_VARIANT_BOOL iLoadENVGeomFromENVdb); \
virtual HRESULT __stdcall GetLoadENVGeomFromENVdbInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetLoadENVGeomFromENVdbLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_RmvNotAssgnPrdResOnSync(CAT_VARIANT_BOOL & oRmvNotAssgnPrdResOnSync); \
virtual HRESULT __stdcall put_RmvNotAssgnPrdResOnSync(CAT_VARIANT_BOOL iRmvNotAssgnPrdResOnSync); \
virtual HRESULT __stdcall GetRmvNotAssgnPrdResOnSyncInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetRmvNotAssgnPrdResOnSyncLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_LockAssignedPrdOnLoad(CAT_VARIANT_BOOL & oLockAssignedPrdOnLoad); \
virtual HRESULT __stdcall put_LockAssignedPrdOnLoad(CAT_VARIANT_BOOL iLockAssignedPrdOnLoad); \
virtual HRESULT __stdcall GetLockAssignedPrdOnLoadInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetLockAssignedPrdOnLoadLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_Load3dStateAndPos(CAT_VARIANT_BOOL & oLoad3dStateAndPos); \
virtual HRESULT __stdcall put_Load3dStateAndPos(CAT_VARIANT_BOOL iLoad3dStateAndPos); \
virtual HRESULT __stdcall GetLoad3dStateAndPosInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetLoad3dStateAndPosLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_LoadChildProcMfgCtx(CAT_VARIANT_BOOL & oLoadChildProcMfgCtx); \
virtual HRESULT __stdcall put_LoadChildProcMfgCtx(CAT_VARIANT_BOOL iLoadChildProcMfgCtx); \
virtual HRESULT __stdcall GetLoadChildProcMfgCtxInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetLoadChildProcMfgCtxLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_LoadUnconstrainedMfgCtx(CAT_VARIANT_BOOL & oLoadUnconstrainedMfgCtx); \
virtual HRESULT __stdcall put_LoadUnconstrainedMfgCtx(CAT_VARIANT_BOOL iLoadUnconstrainedMfgCtx); \
virtual HRESULT __stdcall GetLoadUnconstrainedMfgCtxInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetLoadUnconstrainedMfgCtxLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_LoadFeedProcMfgCtx(CAT_VARIANT_BOOL & oLoadFeedProcMfgCtx); \
virtual HRESULT __stdcall put_LoadFeedProcMfgCtx(CAT_VARIANT_BOOL iLoadFeedProcMfgCtx); \
virtual HRESULT __stdcall GetLoadFeedProcMfgCtxInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetLoadFeedProcMfgCtxLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_PrevProcParseTypeForMfgCtx(short & oPrevProcParseTypeForMfgCtx); \
virtual HRESULT __stdcall put_PrevProcParseTypeForMfgCtx(short iPrevProcParseTypeForMfgCtx); \
virtual HRESULT __stdcall GetPrevProcParseTypeForMfgCtxInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetPrevProcParseTypeForMfgCtxLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_LoadPrdResUserAttribs(CAT_VARIANT_BOOL & oLoadPrdResUserAttribs); \
virtual HRESULT __stdcall put_LoadPrdResUserAttribs(CAT_VARIANT_BOOL iLoadPrdResUserAttribs); \
virtual HRESULT __stdcall GetLoadPrdResUserAttribsInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetLoadPrdResUserAttribsLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_LoadMfgAssmbly(CAT_VARIANT_BOOL & oLoadMfgAssmbly); \
virtual HRESULT __stdcall put_LoadMfgAssmbly(CAT_VARIANT_BOOL iLoadMfgAssmbly); \
virtual HRESULT __stdcall GetLoadMfgAssmblyInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetLoadMfgAssmblyLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_LoadMfgKits(CAT_VARIANT_BOOL & oLoadMfgKits); \
virtual HRESULT __stdcall put_LoadMfgKits(CAT_VARIANT_BOOL iLoadMfgKits); \
virtual HRESULT __stdcall GetLoadMfgKitsInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetLoadMfgKitsLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_SaveRelationToUnExposedPart(CAT_VARIANT_BOOL & oSaveRelationToUnExposedPart); \
virtual HRESULT __stdcall put_SaveRelationToUnExposedPart(CAT_VARIANT_BOOL iSaveRelationToUnExposedPart); \
virtual HRESULT __stdcall GetSaveRelationToUnExposedPartInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetSaveRelationToUnExposedPartLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_LoadDispMfgCtx(CAT_VARIANT_BOOL & oAutoLoadMfgCtx); \
virtual HRESULT __stdcall put_LoadDispMfgCtx(CAT_VARIANT_BOOL iLoadDispMfgCtx); \
virtual HRESULT __stdcall GetLoadDispMfgCtxInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetLoadDispMfgCtxLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_MfgCtxPrevProcRelationType(short & oPrevProcRelationType); \
virtual HRESULT __stdcall put_MfgCtxPrevProcRelationType(short iPrevProcRelationType); \
virtual HRESULT __stdcall GetMfgCtxPrevProcRelationTypeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetMfgCtxPrevProcRelationTypeLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_LoadAllChildProcMfgCtx(CAT_VARIANT_BOOL & oLoadAllChildProcMfgCtx); \
virtual HRESULT __stdcall put_LoadAllChildProcMfgCtx(CAT_VARIANT_BOOL iLoadAllChildProcMfgCtx); \
virtual HRESULT __stdcall GetLoadAllChildProcMfgCtxInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetLoadAllChildProcMfgCtxLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_LoadResGeo(CAT_VARIANT_BOOL & oLoadResGeo); \
virtual HRESULT __stdcall put_LoadResGeo(CAT_VARIANT_BOOL iLoadResGeo); \
virtual HRESULT __stdcall GetLoadResGeoInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetLoadResGeoLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_LoadAssocPrdResChild(CAT_VARIANT_BOOL & oLoadAssocPrdResChild); \
virtual HRESULT __stdcall put_LoadAssocPrdResChild(CAT_VARIANT_BOOL iLoadAssocPrdResChild); \
virtual HRESULT __stdcall GetLoadAssocPrdResChildInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetLoadAssocPrdResChildLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_ApplyLabelEffToAltChild(CAT_VARIANT_BOOL & oApplyLabelEffToAltChild); \
virtual HRESULT __stdcall put_ApplyLabelEffToAltChild(CAT_VARIANT_BOOL iApplyLabelEffToAltChild); \
virtual HRESULT __stdcall GetApplyLabelEffToAltChildInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetApplyLabelEffToAltChildLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_IssueRepositoryPath(CATBSTR & ioIssueRepositoryPath); \
virtual HRESULT __stdcall put_IssueRepositoryPath(const CATBSTR & iIssueRepositoryPath); \
virtual HRESULT __stdcall GetIssueRepositoryPathInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetIssueRepositoryPathLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_MailClientLaunchMode(CAT_VARIANT_BOOL & oMailClientLaunchMode); \
virtual HRESULT __stdcall put_MailClientLaunchMode(CAT_VARIANT_BOOL iMailClientLaunchMode); \
virtual HRESULT __stdcall GetMailClientLaunchModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetMailClientLaunchModeLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_LinkSendMode(short & oLinkSendMode); \
virtual HRESULT __stdcall put_LinkSendMode(short iLinkSendMode); \
virtual HRESULT __stdcall GetLinkSendModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetLinkSendModeLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_ShowOnlyFilteredObjects(CAT_VARIANT_BOOL & oShowOnlyFilteredObjects); \
virtual HRESULT __stdcall put_ShowOnlyFilteredObjects(CAT_VARIANT_BOOL iShowOnlyFilteredObjects); \
virtual HRESULT __stdcall GetShowOnlyFilteredObjectsInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetShowOnlyFilteredObjectsLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_LoadPSSData(CAT_VARIANT_BOOL & oLoadPSSData); \
virtual HRESULT __stdcall put_LoadPSSData(CAT_VARIANT_BOOL iLoadPSSData); \
virtual HRESULT __stdcall GetLoadPSSDataInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetLoadPSSDataLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_ProcProdRelations(CATSafeArrayVariant *& oProcProdRelations); \
virtual HRESULT __stdcall put_ProcProdRelations(const CATSafeArrayVariant & iProcProdRelations); \
virtual HRESULT __stdcall GetProcProdRelationsInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetProcProdRelationsLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_ProcResRelations(CATSafeArrayVariant *& oProcResRelations); \
virtual HRESULT __stdcall put_ProcResRelations(const CATSafeArrayVariant & iProcResRelations); \
virtual HRESULT __stdcall GetProcResRelationsInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetProcResRelationsLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_OpenModeForLoad(CATLONG & oOpenModeForLoad); \
virtual HRESULT __stdcall put_OpenModeForLoad(CATLONG iOpenModeForLoad); \
virtual HRESULT __stdcall GetOpenModeForLoadInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetOpenModeForLoadLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_SaveControlFlowInPROMode(CAT_VARIANT_BOOL & oSaveControlFlow); \
virtual HRESULT __stdcall put_SaveControlFlowInPROMode(CAT_VARIANT_BOOL iSaveControlFlow); \
virtual HRESULT __stdcall GetSaveControlFlowInPROModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetSaveControlFlowInPROModeLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_PackAndGoRepositoryPath(CATBSTR & ioPackAndGoRepositoryPath); \
virtual HRESULT __stdcall put_PackAndGoRepositoryPath(const CATBSTR & iPackAndGoRepositoryPath); \
virtual HRESULT __stdcall GetPackAndGoRepositoryPathInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetPackAndGoRepositoryPathLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_AutoLoadVolCtx(CAT_VARIANT_BOOL & oAutoLoadVolCtx); \
virtual HRESULT __stdcall put_AutoLoadVolCtx(CAT_VARIANT_BOOL iAutoLoadVolCtx); \
virtual HRESULT __stdcall GetAutoLoadVolCtxInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetAutoLoadVolCtxLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_AutoLoadSrvMfgCtx(CAT_VARIANT_BOOL & oAutoLoadSrvMfgCtx); \
virtual HRESULT __stdcall put_AutoLoadSrvMfgCtx(CAT_VARIANT_BOOL iAutoLoadSrvMfgCtx); \
virtual HRESULT __stdcall GetAutoLoadSrvMfgCtxInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetAutoLoadSrvMfgCtxLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_OnlyLoadCtxWithGeometry(CAT_VARIANT_BOOL & oOnlyLoadCtxWithGeometry); \
virtual HRESULT __stdcall put_OnlyLoadCtxWithGeometry(CAT_VARIANT_BOOL iOnlyLoadCtxWithGeometry); \
virtual HRESULT __stdcall GetOnlyLoadCtxWithGeometryInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetOnlyLoadCtxWithGeometryLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_LoadCtxWithFileGeometry(CAT_VARIANT_BOOL & oLoadCtxWithFileGeometry); \
virtual HRESULT __stdcall put_LoadCtxWithFileGeometry(CAT_VARIANT_BOOL iLoadCtxWithFileGeometry); \
virtual HRESULT __stdcall GetLoadCtxWithFileGeometryInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetLoadCtxWithFileGeometryLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_DisableShapeRollUp(CAT_VARIANT_BOOL & oDisableShapeRollUp); \
virtual HRESULT __stdcall put_DisableShapeRollUp(CAT_VARIANT_BOOL iDisableShapeRollUp); \
virtual HRESULT __stdcall GetDisableShapeRollUpInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetDisableShapeRollUpLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_AppendContextChkB(CAT_VARIANT_BOOL & oAppendContextChkB); \
virtual HRESULT __stdcall put_AppendContextChkB(CAT_VARIANT_BOOL iAppendContextChkB); \
virtual HRESULT __stdcall GetAppendContextChkBInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetAppendContextChkBLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_LoadDuplicatesInContextTreeChkB(CAT_VARIANT_BOOL & oLoadDuplicatesChkB); \
virtual HRESULT __stdcall put_LoadDuplicatesInContextTreeChkB(CAT_VARIANT_BOOL iLoadDuplicatesChkB); \
virtual HRESULT __stdcall GetLoadDuplicatesInContextTreeChkBInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetLoadDuplicatesInContextTreeChkBLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_PostLoadScriptOption(CAT_VARIANT_BOOL & oPostLoadScriptOption); \
virtual HRESULT __stdcall put_PostLoadScriptOption(CAT_VARIANT_BOOL iPostLoadScriptOption); \
virtual HRESULT __stdcall GetPostLoadScriptOptionInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetPostLoadScriptOptionLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_PostLoadScriptPath(CATBSTR & ioPostLoadScriptPath); \
virtual HRESULT __stdcall put_PostLoadScriptPath(const CATBSTR & iPostLoadScriptPath); \
virtual HRESULT __stdcall GetPostLoadScriptPathInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetPostLoadScriptPathLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_PostLoadVBAModule(CATBSTR & ioPostLoadVBAModule); \
virtual HRESULT __stdcall put_PostLoadVBAModule(const CATBSTR & iPostLoadVBAModule); \
virtual HRESULT __stdcall GetPostLoadVBAModuleInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetPostLoadVBAModuleLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall Commit(); \
virtual HRESULT __stdcall Rollback(); \
virtual HRESULT __stdcall ResetToAdminValues(); \
virtual HRESULT __stdcall ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList); \
virtual HRESULT __stdcall SaveRepository(); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIAMfgHubSettingAtt(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::get_AutoLoadMfgCtx(CAT_VARIANT_BOOL & oAutoLoadMfgCtx) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_AutoLoadMfgCtx(oAutoLoadMfgCtx)); \
} \
HRESULT __stdcall  ENVTIEName::put_AutoLoadMfgCtx(CAT_VARIANT_BOOL iAutoLoadMfgCtx) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_AutoLoadMfgCtx(iAutoLoadMfgCtx)); \
} \
HRESULT __stdcall  ENVTIEName::GetAutoLoadMfgCtxInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetAutoLoadMfgCtxInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetAutoLoadMfgCtxLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetAutoLoadMfgCtxLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_SaveV5CalcTime(CAT_VARIANT_BOOL & oSaveV5CalcTime) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_SaveV5CalcTime(oSaveV5CalcTime)); \
} \
HRESULT __stdcall  ENVTIEName::put_SaveV5CalcTime(CAT_VARIANT_BOOL iSaveV5CalcTime) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_SaveV5CalcTime(iSaveV5CalcTime)); \
} \
HRESULT __stdcall  ENVTIEName::GetSaveV5CalcTimeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetSaveV5CalcTimeInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetSaveV5CalcTimeLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetSaveV5CalcTimeLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_SavePPRNoDetailing(CAT_VARIANT_BOOL & oSavePPRNoDetailing) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_SavePPRNoDetailing(oSavePPRNoDetailing)); \
} \
HRESULT __stdcall  ENVTIEName::put_SavePPRNoDetailing(CAT_VARIANT_BOOL iSavePPRNoDetailing) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_SavePPRNoDetailing(iSavePPRNoDetailing)); \
} \
HRESULT __stdcall  ENVTIEName::GetSavePPRNoDetailingInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetSavePPRNoDetailingInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetSavePPRNoDetailingLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetSavePPRNoDetailingLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_SaveShowEffctvtPanel(CAT_VARIANT_BOOL & oSaveShowEffctvtPanel) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_SaveShowEffctvtPanel(oSaveShowEffctvtPanel)); \
} \
HRESULT __stdcall  ENVTIEName::put_SaveShowEffctvtPanel(CAT_VARIANT_BOOL iSaveShowEffctvtPanel) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_SaveShowEffctvtPanel(iSaveShowEffctvtPanel)); \
} \
HRESULT __stdcall  ENVTIEName::GetSaveShowEffctvtPanelInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetSaveShowEffctvtPanelInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetSaveShowEffctvtPanelLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetSaveShowEffctvtPanelLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_LoadENVGeomFromENVdb(CAT_VARIANT_BOOL & oLoadENVGeomFromENVdb) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_LoadENVGeomFromENVdb(oLoadENVGeomFromENVdb)); \
} \
HRESULT __stdcall  ENVTIEName::put_LoadENVGeomFromENVdb(CAT_VARIANT_BOOL iLoadENVGeomFromENVdb) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_LoadENVGeomFromENVdb(iLoadENVGeomFromENVdb)); \
} \
HRESULT __stdcall  ENVTIEName::GetLoadENVGeomFromENVdbInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetLoadENVGeomFromENVdbInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetLoadENVGeomFromENVdbLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetLoadENVGeomFromENVdbLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_RmvNotAssgnPrdResOnSync(CAT_VARIANT_BOOL & oRmvNotAssgnPrdResOnSync) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_RmvNotAssgnPrdResOnSync(oRmvNotAssgnPrdResOnSync)); \
} \
HRESULT __stdcall  ENVTIEName::put_RmvNotAssgnPrdResOnSync(CAT_VARIANT_BOOL iRmvNotAssgnPrdResOnSync) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_RmvNotAssgnPrdResOnSync(iRmvNotAssgnPrdResOnSync)); \
} \
HRESULT __stdcall  ENVTIEName::GetRmvNotAssgnPrdResOnSyncInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetRmvNotAssgnPrdResOnSyncInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetRmvNotAssgnPrdResOnSyncLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetRmvNotAssgnPrdResOnSyncLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_LockAssignedPrdOnLoad(CAT_VARIANT_BOOL & oLockAssignedPrdOnLoad) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_LockAssignedPrdOnLoad(oLockAssignedPrdOnLoad)); \
} \
HRESULT __stdcall  ENVTIEName::put_LockAssignedPrdOnLoad(CAT_VARIANT_BOOL iLockAssignedPrdOnLoad) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_LockAssignedPrdOnLoad(iLockAssignedPrdOnLoad)); \
} \
HRESULT __stdcall  ENVTIEName::GetLockAssignedPrdOnLoadInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetLockAssignedPrdOnLoadInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetLockAssignedPrdOnLoadLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetLockAssignedPrdOnLoadLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_Load3dStateAndPos(CAT_VARIANT_BOOL & oLoad3dStateAndPos) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_Load3dStateAndPos(oLoad3dStateAndPos)); \
} \
HRESULT __stdcall  ENVTIEName::put_Load3dStateAndPos(CAT_VARIANT_BOOL iLoad3dStateAndPos) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_Load3dStateAndPos(iLoad3dStateAndPos)); \
} \
HRESULT __stdcall  ENVTIEName::GetLoad3dStateAndPosInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetLoad3dStateAndPosInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetLoad3dStateAndPosLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetLoad3dStateAndPosLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_LoadChildProcMfgCtx(CAT_VARIANT_BOOL & oLoadChildProcMfgCtx) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_LoadChildProcMfgCtx(oLoadChildProcMfgCtx)); \
} \
HRESULT __stdcall  ENVTIEName::put_LoadChildProcMfgCtx(CAT_VARIANT_BOOL iLoadChildProcMfgCtx) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_LoadChildProcMfgCtx(iLoadChildProcMfgCtx)); \
} \
HRESULT __stdcall  ENVTIEName::GetLoadChildProcMfgCtxInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetLoadChildProcMfgCtxInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetLoadChildProcMfgCtxLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetLoadChildProcMfgCtxLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_LoadUnconstrainedMfgCtx(CAT_VARIANT_BOOL & oLoadUnconstrainedMfgCtx) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_LoadUnconstrainedMfgCtx(oLoadUnconstrainedMfgCtx)); \
} \
HRESULT __stdcall  ENVTIEName::put_LoadUnconstrainedMfgCtx(CAT_VARIANT_BOOL iLoadUnconstrainedMfgCtx) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_LoadUnconstrainedMfgCtx(iLoadUnconstrainedMfgCtx)); \
} \
HRESULT __stdcall  ENVTIEName::GetLoadUnconstrainedMfgCtxInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetLoadUnconstrainedMfgCtxInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetLoadUnconstrainedMfgCtxLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetLoadUnconstrainedMfgCtxLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_LoadFeedProcMfgCtx(CAT_VARIANT_BOOL & oLoadFeedProcMfgCtx) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_LoadFeedProcMfgCtx(oLoadFeedProcMfgCtx)); \
} \
HRESULT __stdcall  ENVTIEName::put_LoadFeedProcMfgCtx(CAT_VARIANT_BOOL iLoadFeedProcMfgCtx) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_LoadFeedProcMfgCtx(iLoadFeedProcMfgCtx)); \
} \
HRESULT __stdcall  ENVTIEName::GetLoadFeedProcMfgCtxInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetLoadFeedProcMfgCtxInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetLoadFeedProcMfgCtxLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetLoadFeedProcMfgCtxLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_PrevProcParseTypeForMfgCtx(short & oPrevProcParseTypeForMfgCtx) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_PrevProcParseTypeForMfgCtx(oPrevProcParseTypeForMfgCtx)); \
} \
HRESULT __stdcall  ENVTIEName::put_PrevProcParseTypeForMfgCtx(short iPrevProcParseTypeForMfgCtx) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_PrevProcParseTypeForMfgCtx(iPrevProcParseTypeForMfgCtx)); \
} \
HRESULT __stdcall  ENVTIEName::GetPrevProcParseTypeForMfgCtxInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetPrevProcParseTypeForMfgCtxInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetPrevProcParseTypeForMfgCtxLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetPrevProcParseTypeForMfgCtxLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_LoadPrdResUserAttribs(CAT_VARIANT_BOOL & oLoadPrdResUserAttribs) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_LoadPrdResUserAttribs(oLoadPrdResUserAttribs)); \
} \
HRESULT __stdcall  ENVTIEName::put_LoadPrdResUserAttribs(CAT_VARIANT_BOOL iLoadPrdResUserAttribs) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_LoadPrdResUserAttribs(iLoadPrdResUserAttribs)); \
} \
HRESULT __stdcall  ENVTIEName::GetLoadPrdResUserAttribsInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetLoadPrdResUserAttribsInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetLoadPrdResUserAttribsLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetLoadPrdResUserAttribsLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_LoadMfgAssmbly(CAT_VARIANT_BOOL & oLoadMfgAssmbly) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_LoadMfgAssmbly(oLoadMfgAssmbly)); \
} \
HRESULT __stdcall  ENVTIEName::put_LoadMfgAssmbly(CAT_VARIANT_BOOL iLoadMfgAssmbly) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_LoadMfgAssmbly(iLoadMfgAssmbly)); \
} \
HRESULT __stdcall  ENVTIEName::GetLoadMfgAssmblyInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetLoadMfgAssmblyInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetLoadMfgAssmblyLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetLoadMfgAssmblyLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_LoadMfgKits(CAT_VARIANT_BOOL & oLoadMfgKits) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_LoadMfgKits(oLoadMfgKits)); \
} \
HRESULT __stdcall  ENVTIEName::put_LoadMfgKits(CAT_VARIANT_BOOL iLoadMfgKits) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_LoadMfgKits(iLoadMfgKits)); \
} \
HRESULT __stdcall  ENVTIEName::GetLoadMfgKitsInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetLoadMfgKitsInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetLoadMfgKitsLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetLoadMfgKitsLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_SaveRelationToUnExposedPart(CAT_VARIANT_BOOL & oSaveRelationToUnExposedPart) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_SaveRelationToUnExposedPart(oSaveRelationToUnExposedPart)); \
} \
HRESULT __stdcall  ENVTIEName::put_SaveRelationToUnExposedPart(CAT_VARIANT_BOOL iSaveRelationToUnExposedPart) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_SaveRelationToUnExposedPart(iSaveRelationToUnExposedPart)); \
} \
HRESULT __stdcall  ENVTIEName::GetSaveRelationToUnExposedPartInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetSaveRelationToUnExposedPartInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetSaveRelationToUnExposedPartLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetSaveRelationToUnExposedPartLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_LoadDispMfgCtx(CAT_VARIANT_BOOL & oAutoLoadMfgCtx) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_LoadDispMfgCtx(oAutoLoadMfgCtx)); \
} \
HRESULT __stdcall  ENVTIEName::put_LoadDispMfgCtx(CAT_VARIANT_BOOL iLoadDispMfgCtx) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_LoadDispMfgCtx(iLoadDispMfgCtx)); \
} \
HRESULT __stdcall  ENVTIEName::GetLoadDispMfgCtxInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetLoadDispMfgCtxInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetLoadDispMfgCtxLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetLoadDispMfgCtxLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_MfgCtxPrevProcRelationType(short & oPrevProcRelationType) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_MfgCtxPrevProcRelationType(oPrevProcRelationType)); \
} \
HRESULT __stdcall  ENVTIEName::put_MfgCtxPrevProcRelationType(short iPrevProcRelationType) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_MfgCtxPrevProcRelationType(iPrevProcRelationType)); \
} \
HRESULT __stdcall  ENVTIEName::GetMfgCtxPrevProcRelationTypeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetMfgCtxPrevProcRelationTypeInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetMfgCtxPrevProcRelationTypeLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetMfgCtxPrevProcRelationTypeLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_LoadAllChildProcMfgCtx(CAT_VARIANT_BOOL & oLoadAllChildProcMfgCtx) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_LoadAllChildProcMfgCtx(oLoadAllChildProcMfgCtx)); \
} \
HRESULT __stdcall  ENVTIEName::put_LoadAllChildProcMfgCtx(CAT_VARIANT_BOOL iLoadAllChildProcMfgCtx) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_LoadAllChildProcMfgCtx(iLoadAllChildProcMfgCtx)); \
} \
HRESULT __stdcall  ENVTIEName::GetLoadAllChildProcMfgCtxInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetLoadAllChildProcMfgCtxInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetLoadAllChildProcMfgCtxLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetLoadAllChildProcMfgCtxLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_LoadResGeo(CAT_VARIANT_BOOL & oLoadResGeo) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_LoadResGeo(oLoadResGeo)); \
} \
HRESULT __stdcall  ENVTIEName::put_LoadResGeo(CAT_VARIANT_BOOL iLoadResGeo) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_LoadResGeo(iLoadResGeo)); \
} \
HRESULT __stdcall  ENVTIEName::GetLoadResGeoInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetLoadResGeoInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetLoadResGeoLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetLoadResGeoLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_LoadAssocPrdResChild(CAT_VARIANT_BOOL & oLoadAssocPrdResChild) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_LoadAssocPrdResChild(oLoadAssocPrdResChild)); \
} \
HRESULT __stdcall  ENVTIEName::put_LoadAssocPrdResChild(CAT_VARIANT_BOOL iLoadAssocPrdResChild) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_LoadAssocPrdResChild(iLoadAssocPrdResChild)); \
} \
HRESULT __stdcall  ENVTIEName::GetLoadAssocPrdResChildInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetLoadAssocPrdResChildInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetLoadAssocPrdResChildLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetLoadAssocPrdResChildLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_ApplyLabelEffToAltChild(CAT_VARIANT_BOOL & oApplyLabelEffToAltChild) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_ApplyLabelEffToAltChild(oApplyLabelEffToAltChild)); \
} \
HRESULT __stdcall  ENVTIEName::put_ApplyLabelEffToAltChild(CAT_VARIANT_BOOL iApplyLabelEffToAltChild) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_ApplyLabelEffToAltChild(iApplyLabelEffToAltChild)); \
} \
HRESULT __stdcall  ENVTIEName::GetApplyLabelEffToAltChildInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetApplyLabelEffToAltChildInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetApplyLabelEffToAltChildLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetApplyLabelEffToAltChildLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_IssueRepositoryPath(CATBSTR & ioIssueRepositoryPath) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_IssueRepositoryPath(ioIssueRepositoryPath)); \
} \
HRESULT __stdcall  ENVTIEName::put_IssueRepositoryPath(const CATBSTR & iIssueRepositoryPath) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_IssueRepositoryPath(iIssueRepositoryPath)); \
} \
HRESULT __stdcall  ENVTIEName::GetIssueRepositoryPathInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetIssueRepositoryPathInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetIssueRepositoryPathLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetIssueRepositoryPathLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_MailClientLaunchMode(CAT_VARIANT_BOOL & oMailClientLaunchMode) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_MailClientLaunchMode(oMailClientLaunchMode)); \
} \
HRESULT __stdcall  ENVTIEName::put_MailClientLaunchMode(CAT_VARIANT_BOOL iMailClientLaunchMode) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_MailClientLaunchMode(iMailClientLaunchMode)); \
} \
HRESULT __stdcall  ENVTIEName::GetMailClientLaunchModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetMailClientLaunchModeInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetMailClientLaunchModeLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetMailClientLaunchModeLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_LinkSendMode(short & oLinkSendMode) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_LinkSendMode(oLinkSendMode)); \
} \
HRESULT __stdcall  ENVTIEName::put_LinkSendMode(short iLinkSendMode) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_LinkSendMode(iLinkSendMode)); \
} \
HRESULT __stdcall  ENVTIEName::GetLinkSendModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetLinkSendModeInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetLinkSendModeLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetLinkSendModeLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_ShowOnlyFilteredObjects(CAT_VARIANT_BOOL & oShowOnlyFilteredObjects) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_ShowOnlyFilteredObjects(oShowOnlyFilteredObjects)); \
} \
HRESULT __stdcall  ENVTIEName::put_ShowOnlyFilteredObjects(CAT_VARIANT_BOOL iShowOnlyFilteredObjects) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_ShowOnlyFilteredObjects(iShowOnlyFilteredObjects)); \
} \
HRESULT __stdcall  ENVTIEName::GetShowOnlyFilteredObjectsInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetShowOnlyFilteredObjectsInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetShowOnlyFilteredObjectsLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetShowOnlyFilteredObjectsLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_LoadPSSData(CAT_VARIANT_BOOL & oLoadPSSData) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_LoadPSSData(oLoadPSSData)); \
} \
HRESULT __stdcall  ENVTIEName::put_LoadPSSData(CAT_VARIANT_BOOL iLoadPSSData) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_LoadPSSData(iLoadPSSData)); \
} \
HRESULT __stdcall  ENVTIEName::GetLoadPSSDataInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetLoadPSSDataInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetLoadPSSDataLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetLoadPSSDataLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_ProcProdRelations(CATSafeArrayVariant *& oProcProdRelations) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_ProcProdRelations(oProcProdRelations)); \
} \
HRESULT __stdcall  ENVTIEName::put_ProcProdRelations(const CATSafeArrayVariant & iProcProdRelations) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_ProcProdRelations(iProcProdRelations)); \
} \
HRESULT __stdcall  ENVTIEName::GetProcProdRelationsInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetProcProdRelationsInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetProcProdRelationsLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetProcProdRelationsLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_ProcResRelations(CATSafeArrayVariant *& oProcResRelations) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_ProcResRelations(oProcResRelations)); \
} \
HRESULT __stdcall  ENVTIEName::put_ProcResRelations(const CATSafeArrayVariant & iProcResRelations) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_ProcResRelations(iProcResRelations)); \
} \
HRESULT __stdcall  ENVTIEName::GetProcResRelationsInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetProcResRelationsInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetProcResRelationsLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetProcResRelationsLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_OpenModeForLoad(CATLONG & oOpenModeForLoad) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_OpenModeForLoad(oOpenModeForLoad)); \
} \
HRESULT __stdcall  ENVTIEName::put_OpenModeForLoad(CATLONG iOpenModeForLoad) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_OpenModeForLoad(iOpenModeForLoad)); \
} \
HRESULT __stdcall  ENVTIEName::GetOpenModeForLoadInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetOpenModeForLoadInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetOpenModeForLoadLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetOpenModeForLoadLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_SaveControlFlowInPROMode(CAT_VARIANT_BOOL & oSaveControlFlow) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_SaveControlFlowInPROMode(oSaveControlFlow)); \
} \
HRESULT __stdcall  ENVTIEName::put_SaveControlFlowInPROMode(CAT_VARIANT_BOOL iSaveControlFlow) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_SaveControlFlowInPROMode(iSaveControlFlow)); \
} \
HRESULT __stdcall  ENVTIEName::GetSaveControlFlowInPROModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetSaveControlFlowInPROModeInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetSaveControlFlowInPROModeLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetSaveControlFlowInPROModeLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_PackAndGoRepositoryPath(CATBSTR & ioPackAndGoRepositoryPath) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_PackAndGoRepositoryPath(ioPackAndGoRepositoryPath)); \
} \
HRESULT __stdcall  ENVTIEName::put_PackAndGoRepositoryPath(const CATBSTR & iPackAndGoRepositoryPath) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_PackAndGoRepositoryPath(iPackAndGoRepositoryPath)); \
} \
HRESULT __stdcall  ENVTIEName::GetPackAndGoRepositoryPathInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetPackAndGoRepositoryPathInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetPackAndGoRepositoryPathLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetPackAndGoRepositoryPathLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_AutoLoadVolCtx(CAT_VARIANT_BOOL & oAutoLoadVolCtx) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_AutoLoadVolCtx(oAutoLoadVolCtx)); \
} \
HRESULT __stdcall  ENVTIEName::put_AutoLoadVolCtx(CAT_VARIANT_BOOL iAutoLoadVolCtx) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_AutoLoadVolCtx(iAutoLoadVolCtx)); \
} \
HRESULT __stdcall  ENVTIEName::GetAutoLoadVolCtxInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetAutoLoadVolCtxInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetAutoLoadVolCtxLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetAutoLoadVolCtxLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_AutoLoadSrvMfgCtx(CAT_VARIANT_BOOL & oAutoLoadSrvMfgCtx) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_AutoLoadSrvMfgCtx(oAutoLoadSrvMfgCtx)); \
} \
HRESULT __stdcall  ENVTIEName::put_AutoLoadSrvMfgCtx(CAT_VARIANT_BOOL iAutoLoadSrvMfgCtx) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_AutoLoadSrvMfgCtx(iAutoLoadSrvMfgCtx)); \
} \
HRESULT __stdcall  ENVTIEName::GetAutoLoadSrvMfgCtxInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetAutoLoadSrvMfgCtxInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetAutoLoadSrvMfgCtxLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetAutoLoadSrvMfgCtxLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_OnlyLoadCtxWithGeometry(CAT_VARIANT_BOOL & oOnlyLoadCtxWithGeometry) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_OnlyLoadCtxWithGeometry(oOnlyLoadCtxWithGeometry)); \
} \
HRESULT __stdcall  ENVTIEName::put_OnlyLoadCtxWithGeometry(CAT_VARIANT_BOOL iOnlyLoadCtxWithGeometry) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_OnlyLoadCtxWithGeometry(iOnlyLoadCtxWithGeometry)); \
} \
HRESULT __stdcall  ENVTIEName::GetOnlyLoadCtxWithGeometryInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetOnlyLoadCtxWithGeometryInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetOnlyLoadCtxWithGeometryLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetOnlyLoadCtxWithGeometryLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_LoadCtxWithFileGeometry(CAT_VARIANT_BOOL & oLoadCtxWithFileGeometry) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_LoadCtxWithFileGeometry(oLoadCtxWithFileGeometry)); \
} \
HRESULT __stdcall  ENVTIEName::put_LoadCtxWithFileGeometry(CAT_VARIANT_BOOL iLoadCtxWithFileGeometry) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_LoadCtxWithFileGeometry(iLoadCtxWithFileGeometry)); \
} \
HRESULT __stdcall  ENVTIEName::GetLoadCtxWithFileGeometryInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetLoadCtxWithFileGeometryInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetLoadCtxWithFileGeometryLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetLoadCtxWithFileGeometryLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_DisableShapeRollUp(CAT_VARIANT_BOOL & oDisableShapeRollUp) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_DisableShapeRollUp(oDisableShapeRollUp)); \
} \
HRESULT __stdcall  ENVTIEName::put_DisableShapeRollUp(CAT_VARIANT_BOOL iDisableShapeRollUp) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_DisableShapeRollUp(iDisableShapeRollUp)); \
} \
HRESULT __stdcall  ENVTIEName::GetDisableShapeRollUpInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetDisableShapeRollUpInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetDisableShapeRollUpLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetDisableShapeRollUpLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_AppendContextChkB(CAT_VARIANT_BOOL & oAppendContextChkB) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_AppendContextChkB(oAppendContextChkB)); \
} \
HRESULT __stdcall  ENVTIEName::put_AppendContextChkB(CAT_VARIANT_BOOL iAppendContextChkB) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_AppendContextChkB(iAppendContextChkB)); \
} \
HRESULT __stdcall  ENVTIEName::GetAppendContextChkBInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetAppendContextChkBInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetAppendContextChkBLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetAppendContextChkBLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_LoadDuplicatesInContextTreeChkB(CAT_VARIANT_BOOL & oLoadDuplicatesChkB) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_LoadDuplicatesInContextTreeChkB(oLoadDuplicatesChkB)); \
} \
HRESULT __stdcall  ENVTIEName::put_LoadDuplicatesInContextTreeChkB(CAT_VARIANT_BOOL iLoadDuplicatesChkB) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_LoadDuplicatesInContextTreeChkB(iLoadDuplicatesChkB)); \
} \
HRESULT __stdcall  ENVTIEName::GetLoadDuplicatesInContextTreeChkBInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetLoadDuplicatesInContextTreeChkBInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetLoadDuplicatesInContextTreeChkBLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetLoadDuplicatesInContextTreeChkBLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_PostLoadScriptOption(CAT_VARIANT_BOOL & oPostLoadScriptOption) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_PostLoadScriptOption(oPostLoadScriptOption)); \
} \
HRESULT __stdcall  ENVTIEName::put_PostLoadScriptOption(CAT_VARIANT_BOOL iPostLoadScriptOption) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_PostLoadScriptOption(iPostLoadScriptOption)); \
} \
HRESULT __stdcall  ENVTIEName::GetPostLoadScriptOptionInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetPostLoadScriptOptionInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetPostLoadScriptOptionLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetPostLoadScriptOptionLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_PostLoadScriptPath(CATBSTR & ioPostLoadScriptPath) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_PostLoadScriptPath(ioPostLoadScriptPath)); \
} \
HRESULT __stdcall  ENVTIEName::put_PostLoadScriptPath(const CATBSTR & iPostLoadScriptPath) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_PostLoadScriptPath(iPostLoadScriptPath)); \
} \
HRESULT __stdcall  ENVTIEName::GetPostLoadScriptPathInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetPostLoadScriptPathInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetPostLoadScriptPathLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetPostLoadScriptPathLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_PostLoadVBAModule(CATBSTR & ioPostLoadVBAModule) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_PostLoadVBAModule(ioPostLoadVBAModule)); \
} \
HRESULT __stdcall  ENVTIEName::put_PostLoadVBAModule(const CATBSTR & iPostLoadVBAModule) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_PostLoadVBAModule(iPostLoadVBAModule)); \
} \
HRESULT __stdcall  ENVTIEName::GetPostLoadVBAModuleInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetPostLoadVBAModuleInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetPostLoadVBAModuleLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetPostLoadVBAModuleLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::Commit() \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)Commit()); \
} \
HRESULT __stdcall  ENVTIEName::Rollback() \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)Rollback()); \
} \
HRESULT __stdcall  ENVTIEName::ResetToAdminValues() \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)ResetToAdminValues()); \
} \
HRESULT __stdcall  ENVTIEName::ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)ResetToAdminValuesByName(iAttList)); \
} \
HRESULT __stdcall  ENVTIEName::SaveRepository() \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)SaveRepository()); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIAMfgHubSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIAMfgHubSettingAtt(classe)    TIEDNBIAMfgHubSettingAtt##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIAMfgHubSettingAtt(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIAMfgHubSettingAtt, classe) \
 \
 \
CATImplementTIEMethods(DNBIAMfgHubSettingAtt, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIAMfgHubSettingAtt, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIAMfgHubSettingAtt, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIAMfgHubSettingAtt, classe) \
 \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::get_AutoLoadMfgCtx(CAT_VARIANT_BOOL & oAutoLoadMfgCtx) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&oAutoLoadMfgCtx); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_AutoLoadMfgCtx(oAutoLoadMfgCtx); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&oAutoLoadMfgCtx); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::put_AutoLoadMfgCtx(CAT_VARIANT_BOOL iAutoLoadMfgCtx) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&iAutoLoadMfgCtx); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_AutoLoadMfgCtx(iAutoLoadMfgCtx); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&iAutoLoadMfgCtx); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::GetAutoLoadMfgCtxInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAutoLoadMfgCtxInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::SetAutoLoadMfgCtxLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetAutoLoadMfgCtxLock(iLocked); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::get_SaveV5CalcTime(CAT_VARIANT_BOOL & oSaveV5CalcTime) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&oSaveV5CalcTime); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_SaveV5CalcTime(oSaveV5CalcTime); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&oSaveV5CalcTime); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::put_SaveV5CalcTime(CAT_VARIANT_BOOL iSaveV5CalcTime) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&iSaveV5CalcTime); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_SaveV5CalcTime(iSaveV5CalcTime); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&iSaveV5CalcTime); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::GetSaveV5CalcTimeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetSaveV5CalcTimeInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::SetSaveV5CalcTimeLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetSaveV5CalcTimeLock(iLocked); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::get_SavePPRNoDetailing(CAT_VARIANT_BOOL & oSavePPRNoDetailing) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&oSavePPRNoDetailing); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_SavePPRNoDetailing(oSavePPRNoDetailing); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&oSavePPRNoDetailing); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::put_SavePPRNoDetailing(CAT_VARIANT_BOOL iSavePPRNoDetailing) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&iSavePPRNoDetailing); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_SavePPRNoDetailing(iSavePPRNoDetailing); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&iSavePPRNoDetailing); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::GetSavePPRNoDetailingInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,11,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetSavePPRNoDetailingInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,11,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::SetSavePPRNoDetailingLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,12,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetSavePPRNoDetailingLock(iLocked); \
   ExitAfterCall(this,12,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::get_SaveShowEffctvtPanel(CAT_VARIANT_BOOL & oSaveShowEffctvtPanel) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,13,&_Trac2,&oSaveShowEffctvtPanel); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_SaveShowEffctvtPanel(oSaveShowEffctvtPanel); \
   ExitAfterCall(this,13,_Trac2,&_ret_arg,&oSaveShowEffctvtPanel); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::put_SaveShowEffctvtPanel(CAT_VARIANT_BOOL iSaveShowEffctvtPanel) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,14,&_Trac2,&iSaveShowEffctvtPanel); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_SaveShowEffctvtPanel(iSaveShowEffctvtPanel); \
   ExitAfterCall(this,14,_Trac2,&_ret_arg,&iSaveShowEffctvtPanel); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::GetSaveShowEffctvtPanelInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,15,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetSaveShowEffctvtPanelInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,15,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::SetSaveShowEffctvtPanelLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,16,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetSaveShowEffctvtPanelLock(iLocked); \
   ExitAfterCall(this,16,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::get_LoadENVGeomFromENVdb(CAT_VARIANT_BOOL & oLoadENVGeomFromENVdb) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,17,&_Trac2,&oLoadENVGeomFromENVdb); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_LoadENVGeomFromENVdb(oLoadENVGeomFromENVdb); \
   ExitAfterCall(this,17,_Trac2,&_ret_arg,&oLoadENVGeomFromENVdb); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::put_LoadENVGeomFromENVdb(CAT_VARIANT_BOOL iLoadENVGeomFromENVdb) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,18,&_Trac2,&iLoadENVGeomFromENVdb); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_LoadENVGeomFromENVdb(iLoadENVGeomFromENVdb); \
   ExitAfterCall(this,18,_Trac2,&_ret_arg,&iLoadENVGeomFromENVdb); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::GetLoadENVGeomFromENVdbInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,19,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetLoadENVGeomFromENVdbInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,19,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::SetLoadENVGeomFromENVdbLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,20,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetLoadENVGeomFromENVdbLock(iLocked); \
   ExitAfterCall(this,20,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::get_RmvNotAssgnPrdResOnSync(CAT_VARIANT_BOOL & oRmvNotAssgnPrdResOnSync) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,21,&_Trac2,&oRmvNotAssgnPrdResOnSync); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_RmvNotAssgnPrdResOnSync(oRmvNotAssgnPrdResOnSync); \
   ExitAfterCall(this,21,_Trac2,&_ret_arg,&oRmvNotAssgnPrdResOnSync); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::put_RmvNotAssgnPrdResOnSync(CAT_VARIANT_BOOL iRmvNotAssgnPrdResOnSync) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,22,&_Trac2,&iRmvNotAssgnPrdResOnSync); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_RmvNotAssgnPrdResOnSync(iRmvNotAssgnPrdResOnSync); \
   ExitAfterCall(this,22,_Trac2,&_ret_arg,&iRmvNotAssgnPrdResOnSync); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::GetRmvNotAssgnPrdResOnSyncInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,23,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetRmvNotAssgnPrdResOnSyncInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,23,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::SetRmvNotAssgnPrdResOnSyncLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,24,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetRmvNotAssgnPrdResOnSyncLock(iLocked); \
   ExitAfterCall(this,24,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::get_LockAssignedPrdOnLoad(CAT_VARIANT_BOOL & oLockAssignedPrdOnLoad) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,25,&_Trac2,&oLockAssignedPrdOnLoad); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_LockAssignedPrdOnLoad(oLockAssignedPrdOnLoad); \
   ExitAfterCall(this,25,_Trac2,&_ret_arg,&oLockAssignedPrdOnLoad); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::put_LockAssignedPrdOnLoad(CAT_VARIANT_BOOL iLockAssignedPrdOnLoad) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,26,&_Trac2,&iLockAssignedPrdOnLoad); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_LockAssignedPrdOnLoad(iLockAssignedPrdOnLoad); \
   ExitAfterCall(this,26,_Trac2,&_ret_arg,&iLockAssignedPrdOnLoad); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::GetLockAssignedPrdOnLoadInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,27,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetLockAssignedPrdOnLoadInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,27,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::SetLockAssignedPrdOnLoadLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,28,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetLockAssignedPrdOnLoadLock(iLocked); \
   ExitAfterCall(this,28,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::get_Load3dStateAndPos(CAT_VARIANT_BOOL & oLoad3dStateAndPos) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,29,&_Trac2,&oLoad3dStateAndPos); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Load3dStateAndPos(oLoad3dStateAndPos); \
   ExitAfterCall(this,29,_Trac2,&_ret_arg,&oLoad3dStateAndPos); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::put_Load3dStateAndPos(CAT_VARIANT_BOOL iLoad3dStateAndPos) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,30,&_Trac2,&iLoad3dStateAndPos); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Load3dStateAndPos(iLoad3dStateAndPos); \
   ExitAfterCall(this,30,_Trac2,&_ret_arg,&iLoad3dStateAndPos); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::GetLoad3dStateAndPosInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,31,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetLoad3dStateAndPosInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,31,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::SetLoad3dStateAndPosLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,32,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetLoad3dStateAndPosLock(iLocked); \
   ExitAfterCall(this,32,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::get_LoadChildProcMfgCtx(CAT_VARIANT_BOOL & oLoadChildProcMfgCtx) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,33,&_Trac2,&oLoadChildProcMfgCtx); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_LoadChildProcMfgCtx(oLoadChildProcMfgCtx); \
   ExitAfterCall(this,33,_Trac2,&_ret_arg,&oLoadChildProcMfgCtx); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::put_LoadChildProcMfgCtx(CAT_VARIANT_BOOL iLoadChildProcMfgCtx) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,34,&_Trac2,&iLoadChildProcMfgCtx); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_LoadChildProcMfgCtx(iLoadChildProcMfgCtx); \
   ExitAfterCall(this,34,_Trac2,&_ret_arg,&iLoadChildProcMfgCtx); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::GetLoadChildProcMfgCtxInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,35,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetLoadChildProcMfgCtxInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,35,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::SetLoadChildProcMfgCtxLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,36,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetLoadChildProcMfgCtxLock(iLocked); \
   ExitAfterCall(this,36,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::get_LoadUnconstrainedMfgCtx(CAT_VARIANT_BOOL & oLoadUnconstrainedMfgCtx) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,37,&_Trac2,&oLoadUnconstrainedMfgCtx); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_LoadUnconstrainedMfgCtx(oLoadUnconstrainedMfgCtx); \
   ExitAfterCall(this,37,_Trac2,&_ret_arg,&oLoadUnconstrainedMfgCtx); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::put_LoadUnconstrainedMfgCtx(CAT_VARIANT_BOOL iLoadUnconstrainedMfgCtx) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,38,&_Trac2,&iLoadUnconstrainedMfgCtx); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_LoadUnconstrainedMfgCtx(iLoadUnconstrainedMfgCtx); \
   ExitAfterCall(this,38,_Trac2,&_ret_arg,&iLoadUnconstrainedMfgCtx); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::GetLoadUnconstrainedMfgCtxInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,39,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetLoadUnconstrainedMfgCtxInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,39,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::SetLoadUnconstrainedMfgCtxLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,40,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetLoadUnconstrainedMfgCtxLock(iLocked); \
   ExitAfterCall(this,40,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::get_LoadFeedProcMfgCtx(CAT_VARIANT_BOOL & oLoadFeedProcMfgCtx) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,41,&_Trac2,&oLoadFeedProcMfgCtx); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_LoadFeedProcMfgCtx(oLoadFeedProcMfgCtx); \
   ExitAfterCall(this,41,_Trac2,&_ret_arg,&oLoadFeedProcMfgCtx); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::put_LoadFeedProcMfgCtx(CAT_VARIANT_BOOL iLoadFeedProcMfgCtx) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,42,&_Trac2,&iLoadFeedProcMfgCtx); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_LoadFeedProcMfgCtx(iLoadFeedProcMfgCtx); \
   ExitAfterCall(this,42,_Trac2,&_ret_arg,&iLoadFeedProcMfgCtx); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::GetLoadFeedProcMfgCtxInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,43,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetLoadFeedProcMfgCtxInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,43,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::SetLoadFeedProcMfgCtxLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,44,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetLoadFeedProcMfgCtxLock(iLocked); \
   ExitAfterCall(this,44,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::get_PrevProcParseTypeForMfgCtx(short & oPrevProcParseTypeForMfgCtx) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,45,&_Trac2,&oPrevProcParseTypeForMfgCtx); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_PrevProcParseTypeForMfgCtx(oPrevProcParseTypeForMfgCtx); \
   ExitAfterCall(this,45,_Trac2,&_ret_arg,&oPrevProcParseTypeForMfgCtx); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::put_PrevProcParseTypeForMfgCtx(short iPrevProcParseTypeForMfgCtx) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,46,&_Trac2,&iPrevProcParseTypeForMfgCtx); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_PrevProcParseTypeForMfgCtx(iPrevProcParseTypeForMfgCtx); \
   ExitAfterCall(this,46,_Trac2,&_ret_arg,&iPrevProcParseTypeForMfgCtx); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::GetPrevProcParseTypeForMfgCtxInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,47,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetPrevProcParseTypeForMfgCtxInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,47,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::SetPrevProcParseTypeForMfgCtxLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,48,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetPrevProcParseTypeForMfgCtxLock(iLocked); \
   ExitAfterCall(this,48,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::get_LoadPrdResUserAttribs(CAT_VARIANT_BOOL & oLoadPrdResUserAttribs) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,49,&_Trac2,&oLoadPrdResUserAttribs); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_LoadPrdResUserAttribs(oLoadPrdResUserAttribs); \
   ExitAfterCall(this,49,_Trac2,&_ret_arg,&oLoadPrdResUserAttribs); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::put_LoadPrdResUserAttribs(CAT_VARIANT_BOOL iLoadPrdResUserAttribs) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,50,&_Trac2,&iLoadPrdResUserAttribs); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_LoadPrdResUserAttribs(iLoadPrdResUserAttribs); \
   ExitAfterCall(this,50,_Trac2,&_ret_arg,&iLoadPrdResUserAttribs); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::GetLoadPrdResUserAttribsInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,51,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetLoadPrdResUserAttribsInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,51,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::SetLoadPrdResUserAttribsLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,52,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetLoadPrdResUserAttribsLock(iLocked); \
   ExitAfterCall(this,52,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::get_LoadMfgAssmbly(CAT_VARIANT_BOOL & oLoadMfgAssmbly) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,53,&_Trac2,&oLoadMfgAssmbly); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_LoadMfgAssmbly(oLoadMfgAssmbly); \
   ExitAfterCall(this,53,_Trac2,&_ret_arg,&oLoadMfgAssmbly); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::put_LoadMfgAssmbly(CAT_VARIANT_BOOL iLoadMfgAssmbly) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,54,&_Trac2,&iLoadMfgAssmbly); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_LoadMfgAssmbly(iLoadMfgAssmbly); \
   ExitAfterCall(this,54,_Trac2,&_ret_arg,&iLoadMfgAssmbly); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::GetLoadMfgAssmblyInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,55,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetLoadMfgAssmblyInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,55,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::SetLoadMfgAssmblyLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,56,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetLoadMfgAssmblyLock(iLocked); \
   ExitAfterCall(this,56,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::get_LoadMfgKits(CAT_VARIANT_BOOL & oLoadMfgKits) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,57,&_Trac2,&oLoadMfgKits); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_LoadMfgKits(oLoadMfgKits); \
   ExitAfterCall(this,57,_Trac2,&_ret_arg,&oLoadMfgKits); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::put_LoadMfgKits(CAT_VARIANT_BOOL iLoadMfgKits) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,58,&_Trac2,&iLoadMfgKits); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_LoadMfgKits(iLoadMfgKits); \
   ExitAfterCall(this,58,_Trac2,&_ret_arg,&iLoadMfgKits); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::GetLoadMfgKitsInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,59,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetLoadMfgKitsInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,59,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::SetLoadMfgKitsLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,60,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetLoadMfgKitsLock(iLocked); \
   ExitAfterCall(this,60,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::get_SaveRelationToUnExposedPart(CAT_VARIANT_BOOL & oSaveRelationToUnExposedPart) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,61,&_Trac2,&oSaveRelationToUnExposedPart); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_SaveRelationToUnExposedPart(oSaveRelationToUnExposedPart); \
   ExitAfterCall(this,61,_Trac2,&_ret_arg,&oSaveRelationToUnExposedPart); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::put_SaveRelationToUnExposedPart(CAT_VARIANT_BOOL iSaveRelationToUnExposedPart) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,62,&_Trac2,&iSaveRelationToUnExposedPart); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_SaveRelationToUnExposedPart(iSaveRelationToUnExposedPart); \
   ExitAfterCall(this,62,_Trac2,&_ret_arg,&iSaveRelationToUnExposedPart); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::GetSaveRelationToUnExposedPartInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,63,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetSaveRelationToUnExposedPartInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,63,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::SetSaveRelationToUnExposedPartLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,64,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetSaveRelationToUnExposedPartLock(iLocked); \
   ExitAfterCall(this,64,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::get_LoadDispMfgCtx(CAT_VARIANT_BOOL & oAutoLoadMfgCtx) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,65,&_Trac2,&oAutoLoadMfgCtx); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_LoadDispMfgCtx(oAutoLoadMfgCtx); \
   ExitAfterCall(this,65,_Trac2,&_ret_arg,&oAutoLoadMfgCtx); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::put_LoadDispMfgCtx(CAT_VARIANT_BOOL iLoadDispMfgCtx) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,66,&_Trac2,&iLoadDispMfgCtx); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_LoadDispMfgCtx(iLoadDispMfgCtx); \
   ExitAfterCall(this,66,_Trac2,&_ret_arg,&iLoadDispMfgCtx); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::GetLoadDispMfgCtxInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,67,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetLoadDispMfgCtxInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,67,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::SetLoadDispMfgCtxLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,68,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetLoadDispMfgCtxLock(iLocked); \
   ExitAfterCall(this,68,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::get_MfgCtxPrevProcRelationType(short & oPrevProcRelationType) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,69,&_Trac2,&oPrevProcRelationType); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_MfgCtxPrevProcRelationType(oPrevProcRelationType); \
   ExitAfterCall(this,69,_Trac2,&_ret_arg,&oPrevProcRelationType); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::put_MfgCtxPrevProcRelationType(short iPrevProcRelationType) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,70,&_Trac2,&iPrevProcRelationType); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_MfgCtxPrevProcRelationType(iPrevProcRelationType); \
   ExitAfterCall(this,70,_Trac2,&_ret_arg,&iPrevProcRelationType); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::GetMfgCtxPrevProcRelationTypeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,71,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetMfgCtxPrevProcRelationTypeInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,71,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::SetMfgCtxPrevProcRelationTypeLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,72,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetMfgCtxPrevProcRelationTypeLock(iLocked); \
   ExitAfterCall(this,72,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::get_LoadAllChildProcMfgCtx(CAT_VARIANT_BOOL & oLoadAllChildProcMfgCtx) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,73,&_Trac2,&oLoadAllChildProcMfgCtx); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_LoadAllChildProcMfgCtx(oLoadAllChildProcMfgCtx); \
   ExitAfterCall(this,73,_Trac2,&_ret_arg,&oLoadAllChildProcMfgCtx); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::put_LoadAllChildProcMfgCtx(CAT_VARIANT_BOOL iLoadAllChildProcMfgCtx) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,74,&_Trac2,&iLoadAllChildProcMfgCtx); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_LoadAllChildProcMfgCtx(iLoadAllChildProcMfgCtx); \
   ExitAfterCall(this,74,_Trac2,&_ret_arg,&iLoadAllChildProcMfgCtx); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::GetLoadAllChildProcMfgCtxInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,75,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetLoadAllChildProcMfgCtxInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,75,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::SetLoadAllChildProcMfgCtxLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,76,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetLoadAllChildProcMfgCtxLock(iLocked); \
   ExitAfterCall(this,76,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::get_LoadResGeo(CAT_VARIANT_BOOL & oLoadResGeo) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,77,&_Trac2,&oLoadResGeo); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_LoadResGeo(oLoadResGeo); \
   ExitAfterCall(this,77,_Trac2,&_ret_arg,&oLoadResGeo); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::put_LoadResGeo(CAT_VARIANT_BOOL iLoadResGeo) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,78,&_Trac2,&iLoadResGeo); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_LoadResGeo(iLoadResGeo); \
   ExitAfterCall(this,78,_Trac2,&_ret_arg,&iLoadResGeo); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::GetLoadResGeoInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,79,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetLoadResGeoInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,79,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::SetLoadResGeoLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,80,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetLoadResGeoLock(iLocked); \
   ExitAfterCall(this,80,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::get_LoadAssocPrdResChild(CAT_VARIANT_BOOL & oLoadAssocPrdResChild) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,81,&_Trac2,&oLoadAssocPrdResChild); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_LoadAssocPrdResChild(oLoadAssocPrdResChild); \
   ExitAfterCall(this,81,_Trac2,&_ret_arg,&oLoadAssocPrdResChild); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::put_LoadAssocPrdResChild(CAT_VARIANT_BOOL iLoadAssocPrdResChild) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,82,&_Trac2,&iLoadAssocPrdResChild); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_LoadAssocPrdResChild(iLoadAssocPrdResChild); \
   ExitAfterCall(this,82,_Trac2,&_ret_arg,&iLoadAssocPrdResChild); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::GetLoadAssocPrdResChildInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,83,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetLoadAssocPrdResChildInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,83,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::SetLoadAssocPrdResChildLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,84,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetLoadAssocPrdResChildLock(iLocked); \
   ExitAfterCall(this,84,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::get_ApplyLabelEffToAltChild(CAT_VARIANT_BOOL & oApplyLabelEffToAltChild) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,85,&_Trac2,&oApplyLabelEffToAltChild); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_ApplyLabelEffToAltChild(oApplyLabelEffToAltChild); \
   ExitAfterCall(this,85,_Trac2,&_ret_arg,&oApplyLabelEffToAltChild); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::put_ApplyLabelEffToAltChild(CAT_VARIANT_BOOL iApplyLabelEffToAltChild) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,86,&_Trac2,&iApplyLabelEffToAltChild); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_ApplyLabelEffToAltChild(iApplyLabelEffToAltChild); \
   ExitAfterCall(this,86,_Trac2,&_ret_arg,&iApplyLabelEffToAltChild); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::GetApplyLabelEffToAltChildInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,87,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetApplyLabelEffToAltChildInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,87,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::SetApplyLabelEffToAltChildLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,88,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetApplyLabelEffToAltChildLock(iLocked); \
   ExitAfterCall(this,88,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::get_IssueRepositoryPath(CATBSTR & ioIssueRepositoryPath) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,89,&_Trac2,&ioIssueRepositoryPath); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_IssueRepositoryPath(ioIssueRepositoryPath); \
   ExitAfterCall(this,89,_Trac2,&_ret_arg,&ioIssueRepositoryPath); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::put_IssueRepositoryPath(const CATBSTR & iIssueRepositoryPath) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,90,&_Trac2,&iIssueRepositoryPath); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_IssueRepositoryPath(iIssueRepositoryPath); \
   ExitAfterCall(this,90,_Trac2,&_ret_arg,&iIssueRepositoryPath); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::GetIssueRepositoryPathInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,91,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetIssueRepositoryPathInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,91,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::SetIssueRepositoryPathLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,92,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetIssueRepositoryPathLock(iLocked); \
   ExitAfterCall(this,92,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::get_MailClientLaunchMode(CAT_VARIANT_BOOL & oMailClientLaunchMode) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,93,&_Trac2,&oMailClientLaunchMode); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_MailClientLaunchMode(oMailClientLaunchMode); \
   ExitAfterCall(this,93,_Trac2,&_ret_arg,&oMailClientLaunchMode); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::put_MailClientLaunchMode(CAT_VARIANT_BOOL iMailClientLaunchMode) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,94,&_Trac2,&iMailClientLaunchMode); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_MailClientLaunchMode(iMailClientLaunchMode); \
   ExitAfterCall(this,94,_Trac2,&_ret_arg,&iMailClientLaunchMode); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::GetMailClientLaunchModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,95,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetMailClientLaunchModeInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,95,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::SetMailClientLaunchModeLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,96,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetMailClientLaunchModeLock(iLocked); \
   ExitAfterCall(this,96,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::get_LinkSendMode(short & oLinkSendMode) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,97,&_Trac2,&oLinkSendMode); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_LinkSendMode(oLinkSendMode); \
   ExitAfterCall(this,97,_Trac2,&_ret_arg,&oLinkSendMode); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::put_LinkSendMode(short iLinkSendMode) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,98,&_Trac2,&iLinkSendMode); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_LinkSendMode(iLinkSendMode); \
   ExitAfterCall(this,98,_Trac2,&_ret_arg,&iLinkSendMode); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::GetLinkSendModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,99,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetLinkSendModeInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,99,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::SetLinkSendModeLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,100,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetLinkSendModeLock(iLocked); \
   ExitAfterCall(this,100,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::get_ShowOnlyFilteredObjects(CAT_VARIANT_BOOL & oShowOnlyFilteredObjects) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,101,&_Trac2,&oShowOnlyFilteredObjects); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_ShowOnlyFilteredObjects(oShowOnlyFilteredObjects); \
   ExitAfterCall(this,101,_Trac2,&_ret_arg,&oShowOnlyFilteredObjects); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::put_ShowOnlyFilteredObjects(CAT_VARIANT_BOOL iShowOnlyFilteredObjects) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,102,&_Trac2,&iShowOnlyFilteredObjects); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_ShowOnlyFilteredObjects(iShowOnlyFilteredObjects); \
   ExitAfterCall(this,102,_Trac2,&_ret_arg,&iShowOnlyFilteredObjects); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::GetShowOnlyFilteredObjectsInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,103,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetShowOnlyFilteredObjectsInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,103,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::SetShowOnlyFilteredObjectsLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,104,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetShowOnlyFilteredObjectsLock(iLocked); \
   ExitAfterCall(this,104,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::get_LoadPSSData(CAT_VARIANT_BOOL & oLoadPSSData) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,105,&_Trac2,&oLoadPSSData); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_LoadPSSData(oLoadPSSData); \
   ExitAfterCall(this,105,_Trac2,&_ret_arg,&oLoadPSSData); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::put_LoadPSSData(CAT_VARIANT_BOOL iLoadPSSData) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,106,&_Trac2,&iLoadPSSData); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_LoadPSSData(iLoadPSSData); \
   ExitAfterCall(this,106,_Trac2,&_ret_arg,&iLoadPSSData); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::GetLoadPSSDataInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,107,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetLoadPSSDataInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,107,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::SetLoadPSSDataLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,108,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetLoadPSSDataLock(iLocked); \
   ExitAfterCall(this,108,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::get_ProcProdRelations(CATSafeArrayVariant *& oProcProdRelations) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,109,&_Trac2,&oProcProdRelations); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_ProcProdRelations(oProcProdRelations); \
   ExitAfterCall(this,109,_Trac2,&_ret_arg,&oProcProdRelations); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::put_ProcProdRelations(const CATSafeArrayVariant & iProcProdRelations) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,110,&_Trac2,&iProcProdRelations); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_ProcProdRelations(iProcProdRelations); \
   ExitAfterCall(this,110,_Trac2,&_ret_arg,&iProcProdRelations); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::GetProcProdRelationsInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,111,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetProcProdRelationsInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,111,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::SetProcProdRelationsLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,112,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetProcProdRelationsLock(iLocked); \
   ExitAfterCall(this,112,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::get_ProcResRelations(CATSafeArrayVariant *& oProcResRelations) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,113,&_Trac2,&oProcResRelations); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_ProcResRelations(oProcResRelations); \
   ExitAfterCall(this,113,_Trac2,&_ret_arg,&oProcResRelations); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::put_ProcResRelations(const CATSafeArrayVariant & iProcResRelations) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,114,&_Trac2,&iProcResRelations); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_ProcResRelations(iProcResRelations); \
   ExitAfterCall(this,114,_Trac2,&_ret_arg,&iProcResRelations); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::GetProcResRelationsInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,115,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetProcResRelationsInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,115,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::SetProcResRelationsLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,116,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetProcResRelationsLock(iLocked); \
   ExitAfterCall(this,116,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::get_OpenModeForLoad(CATLONG & oOpenModeForLoad) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,117,&_Trac2,&oOpenModeForLoad); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_OpenModeForLoad(oOpenModeForLoad); \
   ExitAfterCall(this,117,_Trac2,&_ret_arg,&oOpenModeForLoad); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::put_OpenModeForLoad(CATLONG iOpenModeForLoad) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,118,&_Trac2,&iOpenModeForLoad); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_OpenModeForLoad(iOpenModeForLoad); \
   ExitAfterCall(this,118,_Trac2,&_ret_arg,&iOpenModeForLoad); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::GetOpenModeForLoadInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,119,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetOpenModeForLoadInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,119,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::SetOpenModeForLoadLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,120,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetOpenModeForLoadLock(iLocked); \
   ExitAfterCall(this,120,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::get_SaveControlFlowInPROMode(CAT_VARIANT_BOOL & oSaveControlFlow) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,121,&_Trac2,&oSaveControlFlow); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_SaveControlFlowInPROMode(oSaveControlFlow); \
   ExitAfterCall(this,121,_Trac2,&_ret_arg,&oSaveControlFlow); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::put_SaveControlFlowInPROMode(CAT_VARIANT_BOOL iSaveControlFlow) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,122,&_Trac2,&iSaveControlFlow); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_SaveControlFlowInPROMode(iSaveControlFlow); \
   ExitAfterCall(this,122,_Trac2,&_ret_arg,&iSaveControlFlow); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::GetSaveControlFlowInPROModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,123,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetSaveControlFlowInPROModeInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,123,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::SetSaveControlFlowInPROModeLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,124,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetSaveControlFlowInPROModeLock(iLocked); \
   ExitAfterCall(this,124,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::get_PackAndGoRepositoryPath(CATBSTR & ioPackAndGoRepositoryPath) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,125,&_Trac2,&ioPackAndGoRepositoryPath); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_PackAndGoRepositoryPath(ioPackAndGoRepositoryPath); \
   ExitAfterCall(this,125,_Trac2,&_ret_arg,&ioPackAndGoRepositoryPath); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::put_PackAndGoRepositoryPath(const CATBSTR & iPackAndGoRepositoryPath) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,126,&_Trac2,&iPackAndGoRepositoryPath); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_PackAndGoRepositoryPath(iPackAndGoRepositoryPath); \
   ExitAfterCall(this,126,_Trac2,&_ret_arg,&iPackAndGoRepositoryPath); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::GetPackAndGoRepositoryPathInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,127,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetPackAndGoRepositoryPathInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,127,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::SetPackAndGoRepositoryPathLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,128,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetPackAndGoRepositoryPathLock(iLocked); \
   ExitAfterCall(this,128,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::get_AutoLoadVolCtx(CAT_VARIANT_BOOL & oAutoLoadVolCtx) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,129,&_Trac2,&oAutoLoadVolCtx); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_AutoLoadVolCtx(oAutoLoadVolCtx); \
   ExitAfterCall(this,129,_Trac2,&_ret_arg,&oAutoLoadVolCtx); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::put_AutoLoadVolCtx(CAT_VARIANT_BOOL iAutoLoadVolCtx) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,130,&_Trac2,&iAutoLoadVolCtx); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_AutoLoadVolCtx(iAutoLoadVolCtx); \
   ExitAfterCall(this,130,_Trac2,&_ret_arg,&iAutoLoadVolCtx); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::GetAutoLoadVolCtxInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,131,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAutoLoadVolCtxInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,131,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::SetAutoLoadVolCtxLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,132,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetAutoLoadVolCtxLock(iLocked); \
   ExitAfterCall(this,132,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::get_AutoLoadSrvMfgCtx(CAT_VARIANT_BOOL & oAutoLoadSrvMfgCtx) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,133,&_Trac2,&oAutoLoadSrvMfgCtx); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_AutoLoadSrvMfgCtx(oAutoLoadSrvMfgCtx); \
   ExitAfterCall(this,133,_Trac2,&_ret_arg,&oAutoLoadSrvMfgCtx); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::put_AutoLoadSrvMfgCtx(CAT_VARIANT_BOOL iAutoLoadSrvMfgCtx) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,134,&_Trac2,&iAutoLoadSrvMfgCtx); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_AutoLoadSrvMfgCtx(iAutoLoadSrvMfgCtx); \
   ExitAfterCall(this,134,_Trac2,&_ret_arg,&iAutoLoadSrvMfgCtx); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::GetAutoLoadSrvMfgCtxInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,135,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAutoLoadSrvMfgCtxInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,135,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::SetAutoLoadSrvMfgCtxLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,136,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetAutoLoadSrvMfgCtxLock(iLocked); \
   ExitAfterCall(this,136,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::get_OnlyLoadCtxWithGeometry(CAT_VARIANT_BOOL & oOnlyLoadCtxWithGeometry) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,137,&_Trac2,&oOnlyLoadCtxWithGeometry); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_OnlyLoadCtxWithGeometry(oOnlyLoadCtxWithGeometry); \
   ExitAfterCall(this,137,_Trac2,&_ret_arg,&oOnlyLoadCtxWithGeometry); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::put_OnlyLoadCtxWithGeometry(CAT_VARIANT_BOOL iOnlyLoadCtxWithGeometry) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,138,&_Trac2,&iOnlyLoadCtxWithGeometry); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_OnlyLoadCtxWithGeometry(iOnlyLoadCtxWithGeometry); \
   ExitAfterCall(this,138,_Trac2,&_ret_arg,&iOnlyLoadCtxWithGeometry); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::GetOnlyLoadCtxWithGeometryInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,139,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetOnlyLoadCtxWithGeometryInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,139,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::SetOnlyLoadCtxWithGeometryLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,140,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetOnlyLoadCtxWithGeometryLock(iLocked); \
   ExitAfterCall(this,140,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::get_LoadCtxWithFileGeometry(CAT_VARIANT_BOOL & oLoadCtxWithFileGeometry) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,141,&_Trac2,&oLoadCtxWithFileGeometry); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_LoadCtxWithFileGeometry(oLoadCtxWithFileGeometry); \
   ExitAfterCall(this,141,_Trac2,&_ret_arg,&oLoadCtxWithFileGeometry); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::put_LoadCtxWithFileGeometry(CAT_VARIANT_BOOL iLoadCtxWithFileGeometry) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,142,&_Trac2,&iLoadCtxWithFileGeometry); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_LoadCtxWithFileGeometry(iLoadCtxWithFileGeometry); \
   ExitAfterCall(this,142,_Trac2,&_ret_arg,&iLoadCtxWithFileGeometry); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::GetLoadCtxWithFileGeometryInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,143,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetLoadCtxWithFileGeometryInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,143,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::SetLoadCtxWithFileGeometryLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,144,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetLoadCtxWithFileGeometryLock(iLocked); \
   ExitAfterCall(this,144,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::get_DisableShapeRollUp(CAT_VARIANT_BOOL & oDisableShapeRollUp) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,145,&_Trac2,&oDisableShapeRollUp); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_DisableShapeRollUp(oDisableShapeRollUp); \
   ExitAfterCall(this,145,_Trac2,&_ret_arg,&oDisableShapeRollUp); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::put_DisableShapeRollUp(CAT_VARIANT_BOOL iDisableShapeRollUp) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,146,&_Trac2,&iDisableShapeRollUp); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_DisableShapeRollUp(iDisableShapeRollUp); \
   ExitAfterCall(this,146,_Trac2,&_ret_arg,&iDisableShapeRollUp); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::GetDisableShapeRollUpInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,147,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetDisableShapeRollUpInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,147,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::SetDisableShapeRollUpLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,148,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetDisableShapeRollUpLock(iLocked); \
   ExitAfterCall(this,148,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::get_AppendContextChkB(CAT_VARIANT_BOOL & oAppendContextChkB) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,149,&_Trac2,&oAppendContextChkB); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_AppendContextChkB(oAppendContextChkB); \
   ExitAfterCall(this,149,_Trac2,&_ret_arg,&oAppendContextChkB); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::put_AppendContextChkB(CAT_VARIANT_BOOL iAppendContextChkB) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,150,&_Trac2,&iAppendContextChkB); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_AppendContextChkB(iAppendContextChkB); \
   ExitAfterCall(this,150,_Trac2,&_ret_arg,&iAppendContextChkB); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::GetAppendContextChkBInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,151,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAppendContextChkBInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,151,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::SetAppendContextChkBLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,152,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetAppendContextChkBLock(iLocked); \
   ExitAfterCall(this,152,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::get_LoadDuplicatesInContextTreeChkB(CAT_VARIANT_BOOL & oLoadDuplicatesChkB) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,153,&_Trac2,&oLoadDuplicatesChkB); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_LoadDuplicatesInContextTreeChkB(oLoadDuplicatesChkB); \
   ExitAfterCall(this,153,_Trac2,&_ret_arg,&oLoadDuplicatesChkB); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::put_LoadDuplicatesInContextTreeChkB(CAT_VARIANT_BOOL iLoadDuplicatesChkB) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,154,&_Trac2,&iLoadDuplicatesChkB); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_LoadDuplicatesInContextTreeChkB(iLoadDuplicatesChkB); \
   ExitAfterCall(this,154,_Trac2,&_ret_arg,&iLoadDuplicatesChkB); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::GetLoadDuplicatesInContextTreeChkBInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,155,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetLoadDuplicatesInContextTreeChkBInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,155,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::SetLoadDuplicatesInContextTreeChkBLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,156,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetLoadDuplicatesInContextTreeChkBLock(iLocked); \
   ExitAfterCall(this,156,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::get_PostLoadScriptOption(CAT_VARIANT_BOOL & oPostLoadScriptOption) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,157,&_Trac2,&oPostLoadScriptOption); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_PostLoadScriptOption(oPostLoadScriptOption); \
   ExitAfterCall(this,157,_Trac2,&_ret_arg,&oPostLoadScriptOption); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::put_PostLoadScriptOption(CAT_VARIANT_BOOL iPostLoadScriptOption) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,158,&_Trac2,&iPostLoadScriptOption); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_PostLoadScriptOption(iPostLoadScriptOption); \
   ExitAfterCall(this,158,_Trac2,&_ret_arg,&iPostLoadScriptOption); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::GetPostLoadScriptOptionInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,159,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetPostLoadScriptOptionInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,159,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::SetPostLoadScriptOptionLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,160,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetPostLoadScriptOptionLock(iLocked); \
   ExitAfterCall(this,160,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::get_PostLoadScriptPath(CATBSTR & ioPostLoadScriptPath) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,161,&_Trac2,&ioPostLoadScriptPath); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_PostLoadScriptPath(ioPostLoadScriptPath); \
   ExitAfterCall(this,161,_Trac2,&_ret_arg,&ioPostLoadScriptPath); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::put_PostLoadScriptPath(const CATBSTR & iPostLoadScriptPath) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,162,&_Trac2,&iPostLoadScriptPath); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_PostLoadScriptPath(iPostLoadScriptPath); \
   ExitAfterCall(this,162,_Trac2,&_ret_arg,&iPostLoadScriptPath); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::GetPostLoadScriptPathInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,163,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetPostLoadScriptPathInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,163,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::SetPostLoadScriptPathLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,164,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetPostLoadScriptPathLock(iLocked); \
   ExitAfterCall(this,164,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::get_PostLoadVBAModule(CATBSTR & ioPostLoadVBAModule) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,165,&_Trac2,&ioPostLoadVBAModule); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_PostLoadVBAModule(ioPostLoadVBAModule); \
   ExitAfterCall(this,165,_Trac2,&_ret_arg,&ioPostLoadVBAModule); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::put_PostLoadVBAModule(const CATBSTR & iPostLoadVBAModule) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,166,&_Trac2,&iPostLoadVBAModule); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_PostLoadVBAModule(iPostLoadVBAModule); \
   ExitAfterCall(this,166,_Trac2,&_ret_arg,&iPostLoadVBAModule); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::GetPostLoadVBAModuleInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,167,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetPostLoadVBAModuleInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,167,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::SetPostLoadVBAModuleLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,168,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetPostLoadVBAModuleLock(iLocked); \
   ExitAfterCall(this,168,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::Commit() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,169,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Commit(); \
   ExitAfterCall(this,169,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::Rollback() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,170,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Rollback(); \
   ExitAfterCall(this,170,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::ResetToAdminValues() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,171,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ResetToAdminValues(); \
   ExitAfterCall(this,171,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,172,&_Trac2,&iAttList); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ResetToAdminValuesByName(iAttList); \
   ExitAfterCall(this,172,_Trac2,&_ret_arg,&iAttList); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMfgHubSettingAtt##classe::SaveRepository() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,173,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SaveRepository(); \
   ExitAfterCall(this,173,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMfgHubSettingAtt##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,174,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,174,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMfgHubSettingAtt##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,175,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,175,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMfgHubSettingAtt##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,176,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,176,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMfgHubSettingAtt##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,177,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,177,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMfgHubSettingAtt##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,178,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,178,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIAMfgHubSettingAtt(classe) \
 \
 \
declare_TIE_DNBIAMfgHubSettingAtt(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAMfgHubSettingAtt##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAMfgHubSettingAtt,"DNBIAMfgHubSettingAtt",DNBIAMfgHubSettingAtt::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAMfgHubSettingAtt(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIAMfgHubSettingAtt, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAMfgHubSettingAtt##classe(classe::MetaObject(),DNBIAMfgHubSettingAtt::MetaObject(),(void *)CreateTIEDNBIAMfgHubSettingAtt##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIAMfgHubSettingAtt(classe) \
 \
 \
declare_TIE_DNBIAMfgHubSettingAtt(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAMfgHubSettingAtt##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAMfgHubSettingAtt,"DNBIAMfgHubSettingAtt",DNBIAMfgHubSettingAtt::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAMfgHubSettingAtt(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIAMfgHubSettingAtt, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAMfgHubSettingAtt##classe(classe::MetaObject(),DNBIAMfgHubSettingAtt::MetaObject(),(void *)CreateTIEDNBIAMfgHubSettingAtt##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIAMfgHubSettingAtt(classe) TIE_DNBIAMfgHubSettingAtt(classe)
#else
#define BOA_DNBIAMfgHubSettingAtt(classe) CATImplementBOA(DNBIAMfgHubSettingAtt, classe)
#endif

#endif
