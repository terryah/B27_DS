/**
* @CAA2Level L0
* @CAA2Usage U3 
*/
//===================================================================
// COPYRIGHT Dassault Systemes 2011-05-11
//===================================================================
// DNBIMHIPropertyAccess.cpp
// Header definition of class DNBIMHIPropertyAccess
//===================================================================
//
// Usage notes:
//
//===================================================================
//  2011-05-11 Creation: Code generated by the 3DS wizard   VDR
//===================================================================
#ifndef DNBIMHIPropertyAccess_H
#define DNBIMHIPropertyAccess_H

#include "CATIPDAdapterItf.h"
#include "CATBaseUnknown.h"

#include "CATUnicodeString.h"

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByCATIPDAdapterItf  IID IID_DNBIMHIPropertyAccess ;
#else
extern "C" const IID IID_DNBIMHIPropertyAccess ;
#endif

/**
* Interface representing a means to access the E5 object properties.
*
* <p> 
* DNBIMHIPropertyAccess is implemented on CATBaseUnknown.
* Applications and CAA partners should <b>NOT</b> implement this interface.
*
* @example
*  <pre>
*  CATIProduct_var spPrd;
*  CATBaseUnknown * pBU = spPrd->GetImpl();
*  if (pBU)
*  {
*     DNBIMHIPropertyAccess * pPropAccess = NULL;
*     HRESULT RC = pBU-&gt;QueryInterface(IID_DNBIMHIPropertyAccess,
*                                     (void**) &amp;pPropAccess);
*     if (SUCCEEDED(RC) && NULL != pPropAccess)
*     {
*        CATUnicodeString uAttrName = "name";
*        CATUnicodeString uAttrValue = "V5 Renamed Part";
*        RC = pPropAccess-&gt;SetStringAttribute( uAttrName, uAttrValue );
*         
*        pPropAccess-&gt;Release();
*        pPropAccess = NULL;
*      
*     } // if (SUCCEEDED(RC) && NULL != pPropAccess)
*
*  } // if (pBU)  
*  </pre>
*
* <b>Role</b>:
* This interface allows users to directly access and edit manufacturing hub attributes for objects 
* loaded into V5.
*
* <b>Note</b> <p> If the user has modified any attribute on an object in V5, then to get the updated value
* he should save this change before using the Get APIs. The user also needs
* to save the document after setting an attribute value through Set APIs.</p>
*
* <p>This interface expects caller to use attribute names defined in Manufactuing server
* <p>Set calls will succeed only if user has editing privileges for that object.
*
*/

class ExportedByCATIPDAdapterItf DNBIMHIPropertyAccess: public CATBaseUnknown
{
   CATDeclareInterface;

public:

	/**
    * This sets a String Attribute value to an input Object
    * @param iAttrName
    *    The Attribute Name whose value we need to set
    * @param iAttrValue
    *    CATUnicodeString value of the Attribute
    */    
    virtual HRESULT SetStringAttribute( const CATUnicodeString &  iAttrName,
                                        const CATUnicodeString & iAttrValue ) = 0;

    /**
    * This gets a String Attribute value of an input Object
    * @param iAttrName
    *    The name of the Attribute whose value we need
    * @return
    *    CATUnicodeString value of the Attribute   
    */    
    virtual HRESULT GetStringAttribute( const CATUnicodeString &  iAttrName,
                                        CATUnicodeString & oAttrValue ) = 0;

										 /**
    * This sets a Long Attribute value to an input Object
    * @param iAttrName
    *    The Attribute Name whose value we need to set
    * @return
    *    Value of iAttrName
    */    
    virtual HRESULT SetLongAttribute( const CATUnicodeString &  iAttrName, 
                                      const long & iAttrValue ) = 0;

    /**
    * This gets a Long Attribute value of an input Object
    * @param iAttrName
    *    The name of the Attribute whose value we need
    * @param oAttrValue
    *    Long value of the Attribute
    */    
    virtual HRESULT GetLongAttribute ( const CATUnicodeString &  iAttrName, 
                                       long & oAttrValue ) = 0;
                                          

    /**
    * This sets a Double Attribute value to an input Object
    * @param iAttrName
    *    The Attribute Name whose value we need to set
    * @param iAttrValue
    *    Double value of the Attribute
    * @return
    *    Value of iAttrName
    */    
    virtual HRESULT SetDoubleAttribute( const CATUnicodeString &  iAttrName,
                                        const double & iAttrValue ) = 0;

    /**
    * This gets a Double Attribute value of an input Object
    * @param iAttrName
    *    The name of the Attribute whose value we need
    * @param oAttrValue
    *    Double value of the Attribute
    */    
    virtual HRESULT GetDoubleAttribute( const CATUnicodeString &  iAttrName,
                                        double & oAttrValue ) = 0;                                          
                                                                                
    /**
    * This sets an CATBoolean Attribute value to an input Object
    * @param iAttrName
    *    The Attribute Name whose value we need to set
    * @param iAttrValue
    *    CATBoolean value of the Attribute
	* @return
    *    Value of iAttrName
    */    
    virtual HRESULT SetBooleanAttribute( const CATUnicodeString &  iAttrName,
                                         const boolean iAttrValue ) = 0;

    /**
    * This gets an CATBoolean Attribute value of an input Object
    * @param iAttrName
    *    The name of the Attribute whose value we need
    * @param oAttrValue
    *    CATBoolean value of the Attribute
    */    
    virtual HRESULT GetBooleanAttribute( const CATUnicodeString &  iAttrName,
                                         boolean oAttrValue ) = 0;

	/**
	* This method provided can be used to set the Table/Class Name
	* of Manufacturing Hub Object on which the query should be made.
	* By Default “ergocompbase” Table will be used for Query if User
	* does not specify any Table/Class Name. If user wants to query
	* for an object of any Class/Table other than “ergocompbase”, 
	* then this method should be used to specify the same.
	* @param iQueryObjectClass
	*    The class name on which query should be made. 
	*	"ergocompbase" by default
	* @return
	*    S_OK or E_FAIL
	*/    
	virtual HRESULT SetQueryObjectClass( const CATUnicodeString & iQueryObjectClass ) = 0;

};

//-----------------------------------------------------------------------
CATDeclareHandler( DNBIMHIPropertyAccess, CATBaseUnknown );

#endif
