#ifdef  _WINDOWS_SOURCE
#ifdef  __CATIPDAdapterItf
#define ExportedByCATIPDAdapterItf     __declspec(dllexport)
#else
#define ExportedByCATIPDAdapterItf     __declspec(dllimport)
#endif
#else
#define ExportedByCATIPDAdapterItf
#endif
