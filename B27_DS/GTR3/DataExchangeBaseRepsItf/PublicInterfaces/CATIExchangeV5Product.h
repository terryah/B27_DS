// COPYRIGHT Dassault Systemes Provence 2008
//===================================================================
//
// CATIExchangeV5Product.h
// Define the CATIExchangeV5Product interface
//
//===================================================================
//
//  Jul 2008  Creation: Code generated by the CAA wizard  JRX
//  20/09/2012: DFB: CAA L0 XCAD Interfaces since V5R23
//===================================================================
#ifndef CATIExchangeV5Product_H
#define CATIExchangeV5Product_H

/**
 * @CAA2Level L0
 * @CAA2Usage U0 
 */

#include "DataExchangeBaseRepsItfCPP.h"
#include "CATBaseUnknown.h"

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByDataExchangeBaseRepsItfCPP IID IID_CATIExchangeV5Product;
#else
extern "C" const IID IID_CATIExchangeV5Product ;
#endif

//------------------------------------------------------------------

/**
 * Interface to exchange the result product.
 * <b>Role</b>: This interface provides a access to the result product.
 * <br>Available from CATIA V5R23.
*/
class ExportedByDataExchangeBaseRepsItfCPP CATIExchangeV5Product: public CATBaseUnknown
{
  CATDeclareInterface;

  public:

     /**
     * Retrieves the V5 result product.
     * @param oCATIProduct
     *				the result product.
     */

      virtual HRESULT GetV5Product (CATBaseUnknown_var & oCATIProduct) = 0;

  // No constructors or destructors on this pure virtual base class
  // --------------------------------------------------------------
};

CATDeclareHandler(CATIExchangeV5Product,CATBaseUnknown);

//------------------------------------------------------------------

#endif
