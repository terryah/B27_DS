/* -*-c++-*- */
// COPYRIGHT Dassault Systemes 2005
//===================================================================
//
// CATIExchangeTessellatedRepresentationPlus.h
// Define the CATIExchangeTessellatedRepresentationPlus interface
//
//===================================================================
//
// Usage notes:
//   New interface: describe its use here
//
//===================================================================
//
//  Oct 2005  Creation: Code generated by the CAA wizard  cgu
// 2007/03/28: DFB: CAA Tag
//===================================================================

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

#ifndef CATIExchangeTessellatedRepresentationPlus_H
#define CATIExchangeTessellatedRepresentationPlus_H

#include "DataExchangeBaseRepsItfCPP.h"
#include "CATBaseUnknown.h"

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByDataExchangeBaseRepsItfCPP IID IID_CATIExchangeTessellatedRepresentationPlus;
#else
extern "C" const IID IID_CATIExchangeTessellatedRepresentationPlus ;
#endif

//------------------------------------------------------------------

/**
* Interface representing xxx.
*
* <br><b>Role</b>: Components that implement
* CATIExchangeTessellatedRepresentationPlus are ...
* <p>
* Do not use the CATIExchangeTessellatedRepresentationPlus interface for such and such

*
* @example
*  // example is optional
*  CATIExchangeTessellatedRepresentationPlus* currentDisplay = NULL;
*  rc = window-&gt;QueryInterface(IID_CATIExchangeTessellatedRepresentationPlus,
*                                     (void**) &amp;currentDisplay);
*
* @href ClassReference, Class#MethodReference, #InternalMethod...
*/
class ExportedByDataExchangeBaseRepsItfCPP CATIExchangeTessellatedRepresentationPlus: public CATBaseUnknown
{
  CATDeclareInterface;

  public:

    /**
     * Retrieves the "CGM Id" of a node.
     * @param iNodeID
     *				the node's ID.
     * @param oCGMId
     *				the "CGM Id" of the node.
     */
	virtual HRESULT GetNodeCGMId(const void* iNodeID, unsigned int* oCGMId) = 0;

  // No constructors or destructors on this pure virtual base class
  // --------------------------------------------------------------
};

//------------------------------------------------------------------

#endif
