/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIADocumentationSettingAtt_h
#define CATIADocumentationSettingAtt_h

#include "CATCORBABoolean.h"
#include "CAT_VARIANT_BOOL.h"

#ifndef ExportedByInfPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __InfPubIDL
#define ExportedByInfPubIDL __declspec(dllexport)
#else
#define ExportedByInfPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByInfPubIDL
#endif
#endif

#include "CATBSTR.h"
#include "CATDocContextualPriority.h"
#include "CATIASettingController.h"

extern ExportedByInfPubIDL IID IID_CATIADocumentationSettingAtt;

class ExportedByInfPubIDL CATIADocumentationSettingAtt : public CATIASettingController
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_TechnicalDocumentationPath(CATBSTR & ioDocPath)=0;

    virtual HRESULT __stdcall put_TechnicalDocumentationPath(const CATBSTR & iDocPath)=0;

    virtual HRESULT __stdcall GetTechnicalDocumentationPathInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetTechnicalDocumentationPathLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_DocLanguage(CATLONG & oDocLang)=0;

    virtual HRESULT __stdcall put_DocLanguage(CATLONG iDocLang)=0;

    virtual HRESULT __stdcall GetDocLanguageInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetDocLanguageLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_CompanionPath(CATBSTR & ioUserCompPath)=0;

    virtual HRESULT __stdcall put_CompanionPath(const CATBSTR & iUserCompPath)=0;

    virtual HRESULT __stdcall GetCompanionPathInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetCompanionPathLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_Priority(CATDocContextualPriority & oPriority)=0;

    virtual HRESULT __stdcall put_Priority(CATDocContextualPriority iPriority)=0;

    virtual HRESULT __stdcall GetPriorityInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetPriorityLock(CAT_VARIANT_BOOL iLocked)=0;


};

CATDeclareHandler(CATIADocumentationSettingAtt, CATIASettingController);

#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIABase.h"
#include "CATSafeArray.h"
#include "CATVariant.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
