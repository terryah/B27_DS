/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CatRenderingMode_h
#define CatRenderingMode_h

enum CatRenderingMode {
        catRenderShading,
        catRenderShadingWithEdges,
        catRenderWireFrame,
        catRenderHiddenLinesRemoval,
        catRenderQuickHiddenLinesRemoval,
        catRenderMaterial,
        catRenderMaterialWithEdges,
        catRenderShadingWithEdgesAndHiddenEdges,
        catRenderShadingWithEdgesWithoutSmoothEdges,
        catRenderWireFrameWithoutSmoothEdgesWithoutVertices,
        catRenderWireFrameWithHalfSmoothEdgesWithoutVertices,
        catRenderShadingWithEdgesWithOutlines,
        catRenderQuickHiddenLinesRemovalWithoutVertices,
        catRenderQuickHiddenLinesRemovalWithHiddenEdgesWithOutlines,
        catRenderQuickHiddenLinesRemovalWithHiddenEdgesWithOutlinesWithoutVertices,
        catRenderWireFrameWithHalfSmoothEdgeWithOutlinesWithoutVertices,
        catRenderWireFrameWithOutlinesWithoutSmoothEdgesWithoutVertices,
        catRenderQuickHiddenLinesRemovalWithHiddenEdgesWithoutSmoothEdgesWithoutVertices,
        catRenderQuickHiddenLinesRemovalWithHiddenEdgesWithHalfSmoothEdgeWithoutVertices,
        catRenderQuickHiddenLinesRemovalWithoutSmoothEdgesWithoutVertices,
        catRenderQuickHiddenLinesRemovalWithHalfSmoothEdgeWithoutVertices,
        catRenderShadingWithEdgesWithHalfSmoothEdgeWithoutVertices,
        catRenderShadingWithEdgesWithHalfSmoothEdge
};

#endif
