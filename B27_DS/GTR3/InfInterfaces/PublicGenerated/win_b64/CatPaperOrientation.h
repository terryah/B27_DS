/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CatPaperOrientation_h
#define CatPaperOrientation_h

enum CatPaperOrientation {
        catPaperPortrait,
        catPaperLandscape,
        catPaperBestFit
};

#endif
