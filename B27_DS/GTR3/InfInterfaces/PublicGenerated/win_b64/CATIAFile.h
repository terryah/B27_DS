/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAFile_h
#define CATIAFile_h

#ifndef ExportedByInfPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __InfPubIDL
#define ExportedByInfPubIDL __declspec(dllexport)
#else
#define ExportedByInfPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByInfPubIDL
#endif
#endif

#include "CATBSTR.h"
#include "CATIAFileComponent.h"

class CATIATextStream;

extern ExportedByInfPubIDL IID IID_CATIAFile;

class ExportedByInfPubIDL CATIAFile : public CATIAFileComponent
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_Size(CATLONG & oSize)=0;

    virtual HRESULT __stdcall get_Type(CATBSTR & oType)=0;

    virtual HRESULT __stdcall OpenAsTextStream(const CATBSTR & iMode, CATIATextStream *& oTextStream)=0;


};

CATDeclareHandler(CATIAFile, CATIAFileComponent);

#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIABase.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
