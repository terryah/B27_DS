/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAFiles_h
#define CATIAFiles_h

#ifndef ExportedByInfPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __InfPubIDL
#define ExportedByInfPubIDL __declspec(dllexport)
#else
#define ExportedByInfPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByInfPubIDL
#endif
#endif

#include "CATIACollection.h"

class CATIAFile;

extern ExportedByInfPubIDL IID IID_CATIAFiles;

class ExportedByInfPubIDL CATIAFiles : public CATIACollection
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall Item(CATLONG iNumber, CATIAFile *& oFile)=0;


};

CATDeclareHandler(CATIAFiles, CATIACollection);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
