/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CatVisLayerType_h
#define CatVisLayerType_h

enum CatVisLayerType {
        catVisLayerBasic,
        catVisLayerNone
};

#endif
