/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CatNavigationStyle_h
#define CatNavigationStyle_h

enum CatNavigationStyle {
        catNavigationExamine,
        catNavigationWalk,
        catNavigationFly
};

#endif
