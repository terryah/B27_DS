/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAViewer3D_h
#define CATIAViewer3D_h

#include "CATCORBABoolean.h"
#include "CAT_VARIANT_BOOL.h"

#ifndef ExportedByInfPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __InfPubIDL
#define ExportedByInfPubIDL __declspec(dllexport)
#else
#define ExportedByInfPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByInfPubIDL
#endif
#endif

#include "CATIAViewer.h"
#include "CATSafeArray.h"
#include "CatClippingMode.h"
#include "CatLightingMode.h"
#include "CatNavigationStyle.h"
#include "CatRenderingMode.h"

class CATIALightSources;
class CATIAViewpoint3D;

extern ExportedByInfPubIDL IID IID_CATIAViewer3D;

class ExportedByInfPubIDL CATIAViewer3D : public CATIAViewer
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_Viewpoint3D(CATIAViewpoint3D *& oViewpoint)=0;

    virtual HRESULT __stdcall put_Viewpoint3D(CATIAViewpoint3D * oViewpoint)=0;

    virtual HRESULT __stdcall Translate(const CATSafeArrayVariant & iVector)=0;

    virtual HRESULT __stdcall Rotate(const CATSafeArrayVariant & iAxis, double iAngle)=0;

    virtual HRESULT __stdcall get_LightSources(CATIALightSources *& oLightSources)=0;

    virtual HRESULT __stdcall get_LightingIntensity(double & oIntensity)=0;

    virtual HRESULT __stdcall put_LightingIntensity(double iIntensity)=0;

    virtual HRESULT __stdcall get_LightingMode(CatLightingMode & oLightingMode)=0;

    virtual HRESULT __stdcall put_LightingMode(CatLightingMode iLightingMode)=0;

    virtual HRESULT __stdcall get_NavigationStyle(CatNavigationStyle & oNavigationStyle)=0;

    virtual HRESULT __stdcall put_NavigationStyle(CatNavigationStyle iNavigationStyle)=0;

    virtual HRESULT __stdcall get_RenderingMode(CatRenderingMode & oRenderingMode)=0;

    virtual HRESULT __stdcall put_RenderingMode(CatRenderingMode iRenderingMode)=0;

    virtual HRESULT __stdcall get_NearLimit(double & oNearLimit)=0;

    virtual HRESULT __stdcall put_NearLimit(double iNearLimit)=0;

    virtual HRESULT __stdcall get_FarLimit(double & oFarLimit)=0;

    virtual HRESULT __stdcall put_FarLimit(double iFarLimit)=0;

    virtual HRESULT __stdcall get_ClippingMode(CatClippingMode & oClippingMode)=0;

    virtual HRESULT __stdcall put_ClippingMode(CatClippingMode iClippingMode)=0;

    virtual HRESULT __stdcall get_Foggy(CAT_VARIANT_BOOL & oFoggy)=0;

    virtual HRESULT __stdcall put_Foggy(CAT_VARIANT_BOOL iFoggy)=0;

    virtual HRESULT __stdcall get_Ground(CAT_VARIANT_BOOL & oGround)=0;

    virtual HRESULT __stdcall put_Ground(CAT_VARIANT_BOOL iGround)=0;


};

CATDeclareHandler(CATIAViewer3D, CATIAViewer);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIABase.h"
#include "CatCaptureFormat.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
