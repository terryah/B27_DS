#ifndef CATMultiSelectionMode_IDL
#define CATMultiSelectionMode_IDL

/*IDLREP*/

/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */

// COPYRIGHT DASSAULT SYSTEMES 2004

/**
 * Types of multi-selection.
 * <b>Role</b>: This enum is used by the @href CATIASelection#SelectElement3 method.
 * @param CATMonoSel
 *   Multi-selection is not supported
 * @param CATMultiSelTriggWhenSelPerf
 *   Multi-selection is supported (through a dedicated "Tools Palette" toolbar).</br>
 *   The selection (through a trap for example) is triggered when the selection is performed.<br>
 *   The CTRL and SHIFT keys are not supported.
 * @param CATMultiSelTriggWhenUserValidatesSelection
 *   Multi-selection is supported (through a dedicated "Tools Palette" toolbar).</br>
 *   The selection (through a trap for example) is triggered when the user validates the selection.<br>
 *   The CTRL and SHIFT keys are supported.
 */

enum CATMultiSelectionMode
{
  CATMonoSel,
  CATMultiSelTriggWhenSelPerf,
  CATMultiSelTriggWhenUserValidatesSelection
};

#endif
