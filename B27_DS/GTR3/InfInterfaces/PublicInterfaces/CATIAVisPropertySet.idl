#ifndef CATIAVisPropertySet_IDL
#define CATIAVisPropertySet_IDL
/*IDLREP*/
/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */
// COPYRIGHT DASSAULT SYSTEMES 2000

#include "CATIABase.idl"
#include "CatVisPropertyStatus.idl"
#include "CatVisPropertyTypes.idl"
#include "CatVisPropertyShow.idl"
#include "CatVisPropertyPick.idl"
#include "CatVisLayerType.idl"

#include "CATIAV5Level.h"

/**
* Represents the graphic properties for the current selection.
* <b>Role</b>: We retrieve the graphic properties of the current selection 
* thanks to @href CATIASelection#VisProperties
* <br>
* <br><b> The graphic properties are:</b>
* <ul>
* <li><i>The Color</i></li>
* <br>The Color is defined by 3 components (red,green,blue). Each component ranges from 0 to 255
* <li><i>The Opacity</i></li>
* <br>The opacity is defined from 255 (total opacity) to 0 (total transparency). 
* In Material visualization mode the transparency is real, so the element
* is truly  more or less opaque. But in another visualization mode, the transparency is a 
* simulation, so if the opacity is between 0 and 254 the element is transparent but with the same 
* visual effect and if the opacity is 255 the element is opaque.   
* <li><i>The width of a line</i></li>
* <br> Each index defines a width  customizable in Tools/Options.
* <li><i>The type of a line</i></li>
* <br>Solid, Dashed, ...
* <li><i>The symbol of a point</i></li>
* <br>Star, Dot, ...
* </ul>
* <b>A partial modification of an object</b>
* <br>A part (for example) contains faces, edges, lines, points. Interactively with the Edit 
* Properties command the end user can change their color, their line type and so one. To 
* go faster there is the Graphic Toolbar which contains a sub-set of properties.
* So this interface follows the behavior of the graphic toolbar. The color, the line type ...
* is applicated to the sub-element of the object defined by the application, and that you can
* retrieve in the graphic toolbar.   
* <br><br>
* <b>A multiple selection</b>
* <br>When we modify a graphic property using the Setxxx methods, we modify 
* one by one each element of the current selection. 
* <br>When we read a graphic property using the Getxxx methods, we retrieve an information
* which is valid for all elements of the current selection.
* <br><br>
* <b>Real versus Visible graphic properties</b>
* <br>Elements of the current selection are inside a specification tree:
* <pre>
*   Example :
*    
*   Product0
*        Part1
*           Part3
*        Part2
*
* </pre>
* In this sample, product0 and Part1 are nodes and Part3 and Part2 are leaves.
* <br>Each element (node and leaf) of this tree has its own graphic properties: that is 
* the <b>"Real"</b> graphic properties.
* <br>But there is an inheritance mecanism, so each element has also <b>"Visible"</b> 
* graphic properties. An element can be displayed with an another graphic properties that 
* its real graphic properties.
* <br><br><b>The inheritance is the following</b>: 
* <br>From the root of the tree, the first node with 
* an inheritance flag to 1, gives its property to each element below it. 
* For each graphic property there is an independantly inheritance. 
* <pre>
*   Example with the color property: ( color inheritance flag, color)
*    
*   Product0    (1,red)
*        Part1    (0,blue)
*           Part3   (1,green)
*        Part2    (0,Yellow)
*
* </pre> 
* In this sample the real colord of the product0 is red, blue for the part1, 
* green for the part3 and yellow for the part2. But the visible color of each element
* is red, because the product0 gives the red color at all the tree.
*
* @see CATIASelection
*/
interface CATIAVisPropertySet : CATIABase
{
        /**
        * Sets the real color and the color inheritance flag for the current selection.
        * @param iRed
        *  A value between 0 and 255
        * @param iGreen
        *  A value between 0 and 255
        * @param iBlue
        *  A value between 0 and 255
        * @param iInheritance
        * <b>Legal value:</b>
        * <dl>
        * <dt><tt>0</tt>
        * <dd>No heritance 
        * <dt><tt>1</tt>
        * <dd>Heritance
	* </dl>
	* @sample
	* The following sample shows how to change colour of current selection.
	* <pre>
	* Set visProperties1 = CATIA.ActiveDocument.Selection.VisProperties 
	* visProperties1.SetRealColor 255,0,0,1 
	* </pre>
        */
	HRESULT SetRealColor(in long iRed, in long iGreen, in long iBlue, in long iInheritance);

        /**
        * Sets the opacity and the opacity inheritance flag for the current selection.
        * @param iOpacity
        *  A value between 0 (total transparency) and 255 (total opacity). 
        * @param iInheritance
        * <b>Legal value:</b>
        * <dl>
        * <dt><tt>0</tt>
        * <dd>No heritance 
        * </dl>
	* @sample
	* The following sample shows how to change opacity of current selection.
	* <pre>
	* Set visProperties1 = CATIA.ActiveDocument.Selection.VisProperties 
	* visProperties1.SetRealOpacity 128,1 
	* </pre>
        */

	HRESULT SetRealOpacity(in long iOpacity, in long iInheritance);

        /**
        * Sets the real line width and the line width inheritance flag for the current selection.
        * @param iLineWidth
        * The value ranges from 1 to 63. Each indice is a thickness customizable in
        * the page Tools/Options/General/Display/thickness.
        * @param iInheritance
        * <b>Legal value:</b>
        * <dl>
        * <dt><tt>0</tt>
        * <dd>No heritance 
        * <dt><tt>1</tt>
        * <dd>Heritance
	* </dl>
	* @sample
	* The following sample shows how to change line width of current selection.
	* <pre>
	* Set visProperties1 = CATIA.ActiveDocument.Selection.VisProperties 
	* visProperties1.SetRealWidth 4,1 
	* </pre>
        */

	HRESULT SetRealWidth(in long iLineWidth, in long iInheritance);

	/**
        * Sets the real line type and the line type inheritance flag for the current selection.
        * @param iLineType
        * The value ranges from 1 to 63. Each indice is a line type customizable in
        * the page Tools/Options/General/Display/Line Type.
        * @param iInheritance
        * <b>Legal value:</b>
        * <dl>
        * <dt><tt>0</tt>
        * <dd>No heritance 
        * <dt><tt>1</tt>
        * <dd>Heritance
	* </dl>
	* @sample
	* The following sample shows how to change line type of current selection.
	* <pre>
	* Set visProperties1 = CATIA.ActiveDocument.Selection.VisProperties 
	* visProperties1.SetRealLineType 4,1 
	* </pre>
        */
	HRESULT SetRealLineType(in long iLineType, in long iInheritance);

	/**
        * Sets the symbol type.
        * <br><b>Note:</b>There is no heritage for symbols. That is why there is only one function
        * "SetSymbolType"  and no function "SetRealSymbolType" or "SetVisibleSymbolType"
        * @param iSymbolType
        *	The symbol type
	*	<br><b>legal values</b>:
        *	<ul>
        *	<li> 1 : a cross which looks like a "X".</li>
        *	<li> 2 : a cross which looks like a "+"</li>
        *	<li> 3 : an unfilled circle</li>
  *	<li> 4 : two unfilled concentric circles</li>
  *	<li> 5 : a filled circle</li>
  *	<li> 6 : a filled square</li>
  *	<li> 7 : a star which is the union of a 2D marker CROSS ,a 2D marker PLUS and a 2D marker DOT</li>
  *	<li> 8 : a dot </li>
  *	<li> 9 : a smalldot (one pixel)</li>
  *	<li> 10 : a kind of arrow which points to the bottom-left
  *	<pre>
  *	     /
  *	|  /
  *	|/__
  *	</pre>
  *	</li>
  *	<li> 11 : a kind of arrow which points to the top-rigth
  *	<pre>
  *	    ___ 	
  *	      /| 
  *	    /  |
  *	  /
  *	</pre>
  *	</li>
  *	<li> FULLCIRCLE2 : a big 12</li>
  *	<li> FULLSQUARE2 : a big 13</li>
  *	</ul>
	* @sample
	* The following sample shows how to change symbol type of current selection.
	* <pre>
	* Set visProperties1 = CATIA.ActiveDocument.Selection.VisProperties 
	* visProperties1.SetSymbolType 4
	* </pre>
        */
	HRESULT SetSymbolType(in long iSymbolType);
	
	/**
    * Retrieves the real color for the current selection.
    * @param oRed
    *  A value between 0 and 255
    * @param oGreen
    *  A value between 0 and 255
    * @param oBlue
    *  A value between 0 and 255
    * @param oStatus
    * <b>Legal value:</b>
    * <dl>
    * <dt><tt>catVisPropertyDefined</tt>
    * <dd>All elements in the current selection have the same real color, so 
	* oRed, oGreen and oBlue are valid
    * <dt><tt>catVisPropertyUnDefined</tt>
    * <dd>The real color is not the same for all elements of the current selection, so 
	* oRed, oGreen and oBlue are not valid
    * </dl>
	* @sample
	* The following sample shows how to retrieve real colors of current selection.
	* <pre>
	* Dim r, g, b 
	* r = CLng(0) 
	* g = CLng(0) 
	* b = CLng(0) 
	* Set visProperties1 = CATIA.ActiveDocument.Selection.VisProperties 
	* visProperties1.GetRealColor r, g, b 
	* MsgBox "r = " & r & " g = " & g & " b = " & b 
	* </pre>
    */

	HRESULT GetRealColor(out  long oRed, out  long oGreen, out long oBlue, out /*IDLRETVAL*/ CatVisPropertyStatus oStatus);

	/**
    * Retrieves the real opacity for the current selection.
    * @param oOpacity
    *  a value between 0 (total transparency) and 255 (total opacity)
    * @param oStatus
    * <b>Legal value:</b>
    * <dl>
    * <dt><tt>catVisPropertyDefined</tt>
    * <dd>All elements in the current selection have the same real opacity value, so 
	* oOpacity is valid
    * <dt><tt>catVisPropertyUnDefined</tt>
    * <dd>The real opacity value is not the same for all elements of the current selection, so 
	* oOpacity is not valid
	* <dt><tt>catVisProperty?</tt>
    * <dd>At least one element of the current selection is not concerned by 
	* this property, so oOpacity is not valid
	* </dl>
	* @sample
	* The following sample shows how to retrieve real opacity of current selection.
	* <pre>
	* Dim op
	* op = CLng(0) 
	* Set visProperties1 = CATIA.ActiveDocument.Selection.VisProperties 
	* visProperties1.GetRealOpacity op 
	* MsgBox "opacity = " & op
	* </pre>
    */
	HRESULT GetRealOpacity(out  long oOpacity,  out /*IDLRETVAL*/ CatVisPropertyStatus oStatus);

	/**
        * Retrieves the real line width for the current selection.
        * @param oLineWidth
        * The value ranges from 1 to 63. Each indice is a thickness customizable in
        * the page Tools/Options/General/Display/thickness.
        * @param oStatus
        * <b>Legal value:</b>
        * <dl>
        * <dt><tt>catVisPropertyDefined</tt>
        * <dd>All elements in the current selection have the same real width , so 
	* oLineWidth is valid
        * <dt><tt>catVisPropertyUnDefined</tt>
        * <dd>The real width is not the same for all elements of the current selection,
	* so oLineWidth is not valid
	* <dt><tt>catVisProperty?</tt>
        * <dd>At least one element of the current selection is not concerned by 
	* this property, so oLineWidth is not valid
        * </dl>	
	* @sample
	* The following sample shows how to retrieve real line width of current selection.
	* <pre>
	* Dim width
	* width = CLng(0) 
	* Set visProperties1 = CATIA.ActiveDocument.Selection.VisProperties 
	* visProperties1.GetRealWidth width
	* MsgBox "width = " & width
	* </pre>
        */
	HRESULT GetRealWidth(out  long oLineWidth,  out /*IDLRETVAL*/ CatVisPropertyStatus oStatus);

	/**
        * Retrieves the real line type for the current selection.
        * @param oLineType
        * The value ranges from 1 to 63. Each indice is a line type customizable in
        * the page Tools/Options/General/Display/Line Type.
        * @param oStatus
        * <b>Legal value:</b>
        * <dl>
        * <dt><tt>catVisPropertyDefined</tt>
        * <dd>All elements in the current selection have the same real line type , so 
	* oLineType is valid
        * <dt><tt>catVisPropertyUnDefined</tt>
        * <dd>The real line type is not the same for all elements of the current selection,
	* so oLineType is not valid
	* <dt><tt>catVisProperty?</tt>
        * <dd>At least one element of the current selection is not concerned by 
	* this property, so oLineType is not valid
        * </dl>
	* @sample
	* The following sample shows how to retrieve real line type of current selection.
	* <pre>
	* Dim linetype
	* linetype = CLng(0) 
	* Set visProperties1 = CATIA.ActiveDocument.Selection.VisProperties 
	* visProperties1.GetRealLineType linetype
	* MsgBox "linetype = " & linetype
	* </pre>
        */
	HRESULT GetRealLineType(out long oLineType,  out /*IDLRETVAL*/ CatVisPropertyStatus oStatus);
	

	/**
        * Retrieves the symbol type for the current selection.
        * @param oSymbolType
        * The symbol type. See @href #SetSymbolType to have values.
        * @param oStatus
        * <b>Legal value:</b>
        * <dl>
        * <dt><tt>catVisPropertyDefined</tt>
        * <dd>All elements in the current selection have the same symbol type , so 
	* oLineType is valid
        * <dt><tt>catVisPropertiesUnDefined</tt>
        * <dd>The symbol type is not the same for all elements of the current selection,
	* so oLineType is not valid
	* <dt><tt>catVisProperty?</tt>
        * <dd>At least one element of the current selection is not concerned by 
	* this property, so oSymbolType is not valid
        * </dl>
	* @sample
	* The following sample shows how to retrieve symbol line type of current selection.
	* <pre>
	* Dim symbol
	* symbol = CLng(0) 
	* Set visProperties1 = CATIA.ActiveDocument.Selection.VisProperties 
	* visProperties1.GetSymbolType symbol
	* MsgBox "Symbol = " & symbol
	* </pre>
        */
	HRESULT GetSymbolType(out long oSymbolType,  out /*IDLRETVAL*/ CatVisPropertyStatus oStatus);

	/**
        * Retrieves the real inheritance flag for the current selection.
		* @param iPropertyType
        *  The type of property: Color, Opacity, Line Width, Line Type 
        * @param oInheritance
        * <dl>
        * <dt><tt>0</tt>
        * <dd>No heritance 
        * <dt><tt>1</tt>
        * <dd>Heritance
		* </dl>
        * @param oStatus
        * <b>Legal value:</b>
        * <dl>
        * <dt><tt>catVisPropertyDefined</tt>
        * <dd>All elements in the current selection have the same real inheritance flag 
		* for the iPropertyType , so oInheritance is valid
        * <dt><tt>catVisPropertyUnDefined</tt>
        * <dd>The real inheritance flag for iPropertyType is not the same for all elements of the current selection,
		* so oInheritance is not valid
	    * </dl>
	* @sample
	* The following sample shows how to retrieve inheritance of current selection.
	* <pre>
	* Dim inhLineType, inhWidth, inhColor, inhOpacity
	* inhLineType = CLng(0) 
	* inhWidth = CLng(0) 
	* inhColor = CLng(0) 
	* inhOpacity = CLng(0) 
	* Set visProperties1 = CATIA.ActiveDocument.Selection.VisProperties 
	* visProperties1.GetRealInheritance catVisPropertyLineType, inhLineType
	* visProperties1.GetRealInheritance catVisPropertyWidth, inhWidth
	* visProperties1.GetRealInheritance catVisPropertyColor, inhColor
	* visProperties1.GetRealInheritance catVisPropertyOpacity, inhOpacity
	* MsgBox "Inheritance : linetype = " & inhLineType & "width =" & inhWidth & "Colour ="  & inhColor & "Opacity =" & inhOpacity
	* </pre>
    */
	HRESULT GetRealInheritance(in CatVisPropertyType iPropertyType, out  long oInheritance, out /*IDLRETVAL*/ CatVisPropertyStatus oStatus);

        /** @nodoc */
	HRESULT SetVisibleColor(in long iRed, in long iGreen, in long iBlue, in long iInheritance);
        /** @nodoc */
	HRESULT SetVisibleOpacity(in long iOpacity, in long iInheritance);
        /** @nodoc */
	HRESULT SetVisibleWidth(in long iWidth, in long iInheritance);
        /** @nodoc */
	HRESULT SetVisibleLineType(in long iLineType, in long iInheritance);

        /**
	* Retrieves the displayed (visible) color for the current selection.
        * @param oRed
        *  a value between 0 and 255
        * @param oGreen
        *  a value between 0 and 255
        * @param oBlue
        *  a value between 0 and 255
        * @param oStatus
        * <b>Legal value:</b>
        * <dl>
        * <dt><tt>catVisPropertyDefined</tt>
        * <dd>All elements in the current selection have the same visible color, so 
		* oRed, oGreen and oBlue are valid
        * <dt><tt>catVisPropertyUnDefined</tt>
        * <dd>The visible color is not the same for all elements of the current selection, so 
		* oRed, oGreen and oBlue are not valid
	    * </dl>	
	* @sample
	* The following sample shows how to retrieve displayed colors of current selection.
	* <pre>
	* Dim r, g, b 
	* r = CLng(0) 
	* g = CLng(0) 
	* b = CLng(0) 
	* Set visProperties1 = CATIA.ActiveDocument.Selection.VisProperties 
	* visProperties1.GetVisibleColor r, g, b 
	* MsgBox "r = " & r & " g = " & g & " b = " & b 
	* </pre>
	    */
	HRESULT GetVisibleColor(out  long oRed, out long oGreen, out  long oBlue,  out /*IDLRETVAL*/ CatVisPropertyStatus oStatus);

	/**
        * Retrieves the displayed (visible) opacity for the current selection.
        * @param oOpacity
        *  a value between 0 (total transparency) and 255 (total opacity)
        * @param oStatus
        * <b>Legal value:</b>
        * <dl>
        * <dt><tt>catVisPropertyDefined</tt>
        * <dd>All elements in the current selection have the same visible opacity value, so 
		* oOpacity is valid
        * <dt><tt>catVisPropertyUnDefined</tt>
        * <dd>The visible opacity value is not the same for all elements of the current selection, so 
		* oOpacity is not valid
		* <dt><tt>catVisProperty?</tt>
        * <dd>At least one element of the current selection is not concerned by 
		* this property, so oOpacity is not valid
	    * </dl>
	* @sample
	* The following sample shows how to retrieve displayed opacity of current selection.
	* <pre>
	* Dim op
	* op = CLng(0) 
	* Set visProperties1 = CATIA.ActiveDocument.Selection.VisProperties 
	* visProperties1.GetVisibleOpacity op 
	* MsgBox "opacity = " & op
	* </pre>
        */
	HRESULT GetVisibleOpacity(out  long oOpacity, out /*IDLRETVAL*/ CatVisPropertyStatus oStatus);

	/**
        * Retrieves the displayed (visible) line width for the current selection.
        * @param oLineWidth
        * A value ranges from 1 to 63. 
        * @param oStatus
        * <b>Legal value:</b>
        * <dl>
        * <dt><tt>catVisPropertyDefined</tt>
        * <dd>All elements in the current selection have the same visible width , so 
		* oLineWidth is valid
        * <dt><tt>catVisPropertyUnDefined</tt>
        * <dd>The visible width is not the same for all elements of the current selection,
		* so oLineWidth is not valid
		* <dt><tt>catVisProperty?</tt>
        * <dd>At least one element of the current selection is not concerned by 
		* this property, so oLineWidth is not valid
	    * </dl>
	* @sample
	* The following sample shows how to retrieve displayed line width of current selection.
	* <pre>
	* Dim width
	* width = CLng(0) 
	* Set visProperties1 = CATIA.ActiveDocument.Selection.VisProperties 
	* visProperties1.GetVisibleWidth width
	* MsgBox "width = " & width
	* </pre>
        */
	HRESULT GetVisibleWidth(out long oLineWidth, out /*IDLRETVAL*/ CatVisPropertyStatus oStatus);

	/**
        * Retrieves the displayed (visible) line type for the current selection.
        * @param oLineType
        * A value ranges from 1 to 63. 
        * @param oStatus
        * <b>Legal value:</b>
        * <dl>
        * <dt><tt>catVisPropertyDefined</tt>
        * <dd>All elements in the current selection have the same visible line type , so 
		* oLineType is valid
        * <dt><tt>catVisPropertyUnDefined</tt>
        * <dd>The visible line type is not the same for all elements of the current selection,
		* so oLineType is not valid
		* <dt><tt>catVisProperty?</tt>
        * <dd>At least one element of the current selection is not concerned by 
		* this property, so oLineType is not valid
	    * </dl>
	* @sample
	* The following sample shows how to retrieve displayed line type of current selection.
	* <pre>
	* Dim linetype
	* linetype = CLng(0) 
	* Set visProperties1 = CATIA.ActiveDocument.Selection.VisProperties 
	* visProperties1.GetVisibleLineType linetype
	* MsgBox "linetype = " & linetype
	* </pre>
        */
	HRESULT GetVisibleLineType(out  long oLineType, out /*IDLRETVAL*/ CatVisPropertyStatus oStatus);

	/**
        * Checks if the real property is hidden.
	* @param iPropertyType
        *  The type of property : Color, Opacity, Line Width, Line Type 
        * @param oInheritance
        * <dl>
        * <dt><tt>0</tt>
        * <dd>No heritance: All parents of each element of the current selection have an inheritance
	* flag to 0.
        * <dt><tt>1</tt>
        * <dd>Heritance: one parent of each element, perhaps the element itself, as a inheritance 
	* flag to 1. 
	* </dl>
        * @param oStatus
        * <b>Legal value:</b>
        * <dl>
        * <dt><tt>catVisPropertyDefined</tt>
        * <dd>All elements in the current selection have the same inheritance flag 
	* for the iPropertyType , so oInheritance is valid
        * <dt><tt>catVisPropertyUnDefined</tt>
        * <dd>The inheritance flag for iPropertyType is not the same for all elements of the current selection,
	* so oInheritance is not valid
	* </dl>
        */
	HRESULT GetVisibleInheritance(in CatVisPropertyType iPropertyType, out  long oInheritance, out /*IDLRETVAL*/ CatVisPropertyStatus oStatus);

        /**
        * Sets the state show mode for the current selection.
        * <br><b>Note</b>: This property is global for the object.
	* @sample
	* The following sample shows how to change show mode of current selection.
	* <pre>
	* Set visProperties1 = CATIA.ActiveDocument.Selection.VisProperties 
	* visProperties1.SetShow catVisPropertyNoShowAttr
	* </pre>
        */
	HRESULT SetShow(in CatVisPropertyShow iShow);

        /**
        * Returns the state show mode for the current selection.
        * <br><b>Note</b>: This property is global for the object.
	* @sample
	* The following sample shows how to retrieve show mode of current selection.
	* <pre>
	* Dim showstate As CatVisPropertyShow
	* Set visProperties1 = CATIA.ActiveDocument.Selection.VisProperties 
	* visProperties1.GetShow showstate
	* MsgBox "show = " & showstate
	* </pre>
        */
	HRESULT GetShow(out  CatVisPropertyShow oShow, out /*IDLRETVAL*/ CatVisPropertyStatus oStatus);

#ifdef CATIAV5R9
	/**
        * Sets the state pick mode for the current selection.
        * <br><b>Note</b>: This property is global for the object.
	* @sample
	* The following sample shows how to change pick mode of current selection.
	* <pre>
	* Set visProperties1 = CATIA.ActiveDocument.Selection.VisProperties 
	* visProperties1.SetPick catVisPropertyNoPickAttr
	* </pre>
        */
	HRESULT SetPick(in CatVisPropertyPick iPick);

        /**
        * Returns the state pick mode for the current selection.
        * <br><b>Note</b>: This property is global for the object.
	* @sample
	* The following sample shows how to retrieve pick mode of current selection.
	* <pre>
	* Dim pickstate As CatVisPropertyPick
	* Set visProperties1 = CATIA.ActiveDocument.Selection.VisProperties 
	* visProperties1.GetPick pickstate
	* MsgBox "pick = " & pickstate
	* </pre>
        */
	HRESULT GetPick(out  CatVisPropertyPick oPick, out /*IDLRETVAL*/ CatVisPropertyStatus oStatus);
#endif

	/**
    * Sets the layer for the current selection.
    * <br><b>Note</b>: This property is global for the object.
	* @param iLayerType
    *  the type of the layer
	* @param iLayerValue
    *  A value between 0 to 1000 <br>
	*  This parameter is used only when the type of the layer is catVisLayerBasic.
	* @sample
	* The following sample shows how to change layer of current selection.
	* <pre>
	* Set visProperties1 = CATIA.ActiveDocument.Selection.VisProperties 
	* visProperties1.SetLayer catVisLayerBasic, 100
	* </pre>
    */
	HRESULT SetLayer(in CatVisLayerType iLayerType, in long iLayerValue);

    /**
    * Returns the layer for the current selection.
    * <br><b>Note</b>: This property is global for the object.
	* @param oLayerType
    *  the type of the layer <br>
	*  When the type is equal to catVisLayerNone, the layer of the current selection is "none". <br>
	*  When the type is equal to catVisLayerBasic, the layer of the currection selection is indicated by the following parameter. <br>
	* @param oLayerValue
    *  A value between 0 to 1000 <br>
	*  This parameter is usefull only when the type of the layer is catVisLayerBasic.
	* @sample
	* The following sample shows how to retrieve layer of current selection.
	* <pre>
	* Dim layer
	* layer = CLng(0) 
	* Dim layertype As CatVisLayerType
	* Set visProperties1 = CATIA.ActiveDocument.Selection.VisProperties 
	* visProperties1.GetLayer layertype, layer
	* If (layertype = catVisLayerNone) Then 
	* MsgBox "Layer None"
	* End If
	* If (layertype = catVisLayerBasic) Then 
	* MsgBox "layer =" & layer
	* End If
	* </pre>
    */
	HRESULT GetLayer(out CatVisLayerType oLayerType, out long oLayerValue, out /*IDLRETVAL*/ CatVisPropertyStatus oStatus);
};

// Interface Name : CATIAVisPropertySet
#pragma ID CATIAVisPropertySet "DCE:69274990-9dbe-11d4-a74b0004ac37ae57"
#pragma DUAL CATIAVisPropertySet

// VB Object Name : Selection
#pragma ID VisPropertySet "DCE:d1596290-9dbe-11d4-a74b0004ac37ae57"
#pragma ALIAS CATIAVisPropertySet VisPropertySet

#endif
