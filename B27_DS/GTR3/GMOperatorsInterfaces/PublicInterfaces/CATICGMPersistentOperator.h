#ifndef CATICGMPersistentOperator_h_
#define CATICGMPersistentOperator_h_

// COPYRIGHT DASSAULT SYSTEMES 2013

/**
 * @CAA2Level L1
 * @CAA2Usage U3
*/

#include "CATGMOperatorsInterfaces.h"
#include "CATICGMDRepOperator.h"

class CATCGMPad;
class CATCGMStream;
class CATPersistentBody;
class CATPersistentCellInfra;
class CATPersistentExtTopOperator;
class CATlsoPersistentContext;

extern ExportedByCATGMOperatorsInterfaces IID IID_CATICGMPersistentOperator;

class ExportedByCATGMOperatorsInterfaces CATICGMPersistentOperator: public CATICGMDRepOperator
{
public:
  /**
   * Constructor
   */
  CATICGMPersistentOperator();

protected:
  /**
   * Destructor
   */
  virtual ~CATICGMPersistentOperator(); // -> delete can't be called
};

#endif /* CATICGMPersistentOperator_h_ */
