#include "CATIACGMLevel.h"
#ifdef CATIACGMR420CAA
#ifdef _STATIC_SOURCE
#define	ExportedByCATGMOperatorsInterfaces
#elif defined __CATGMOperatorsInterfaces


// COPYRIGHT DASSAULT SYSTEMES 2006 

/** @CAA2Required */

/*---------------------------------------------------------------------*/

/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */

/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */

/*---------------------------------------------------------------------*/
#define ExportedByCATGMOperatorsInterfaces DSYExport
#else
#define ExportedByCATGMOperatorsInterfaces DSYImport
#endif
#include "DSYExport.h"
#else
#ifdef _STATIC_SOURCE
#define	ExportedByCATGMOperatorsInterfaces
#elif defined _WINDOWS_SOURCE
#ifdef	__CATGMOperatorsInterfaces
// COPYRIGHT DASSAULT SYSTEMES 2006 
/** @CAA2Required */
/*---------------------------------------------------------------------*/
/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */
/*---------------------------------------------------------------------*/
#define	ExportedByCATGMOperatorsInterfaces	__declspec(dllexport)
#else
#define	ExportedByCATGMOperatorsInterfaces	__declspec(dllimport)
#endif
#else
#define	ExportedByCATGMOperatorsInterfaces
#endif
#endif

