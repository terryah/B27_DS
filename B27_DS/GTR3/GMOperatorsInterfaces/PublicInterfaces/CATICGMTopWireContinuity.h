#ifndef CATICGMTopWireContinuity_h_
#define CATICGMTopWireContinuity_h_

// COPYRIGHT DASSAULT SYSTEMES 2017

/**
 * @CAA2Level L1
 * @CAA2Usage U3
*/

#include "CATGMOperatorsInterfaces.h"
#include "CATICGMTopOperator.h"
#include "CATFreeFormDef.h"
#include "CATMathDef.h"

class CATBody;
class CATGeoFactory;
class CATTopData;

extern ExportedByCATGMOperatorsInterfaces IID IID_CATICGMTopWireContinuity;

/**
 *=============================================================================
 *
 * CATICGMTopWireContinuity
 * -----------------
 * Compute continuity information at the vertices of a wire body.
 * The wire body has a single wire domain that may be open or closed.
 * Operator returns :
 * - the G order of continuity at each vertex ( G0, G1 or G2 )
 * - the distance gap between incident edges at each vertex
 * - the angular gap between tangents to edges at each vertex, in degrees.
 * - the G2 curvature ratio at each vertex, in ]0..1]
 *<ul>
 * <li>A <tt>CATICGMTopWireContinuity</tt> operator is created with the <ttCATCreateTopWireContinuity</tt> global function:
 * It must be directly released with the <tt>Release</tt> method after use. It is not streamable.
 * <li>Optional parameter MeasureMode can be specified before using the <tt>Run</tt> method.
 * <li>The continuity order of the vertex is accessed by the <tt>GetGOrder</tt> method which takes the vertex index as an
 * argument.
 * <li>The position and continuity gaps are accessed by the <tt>GetDeltaPt</tt> and <tt>GetDeltaTg</tt> methods. The ratio
 * <li> of the smaller to the larger curvature is returned by the <tt>GetRatioCv</tt> method.
 *</ul>
*/
class ExportedByCATGMOperatorsInterfaces CATICGMTopWireContinuity: public CATICGMTopOperator
{
public:
  /**
   * Constructor
   */
  CATICGMTopWireContinuity();

  /**
  * Run : compute continuity informations on body wire
  */
  virtual int Run() = 0;

  /**
  * Get the number of vertices in the wire
  * Vertex index ranges from 1 to NbVertex()
  */
  virtual int NbVertex() = 0;

  /**
  * GetGOrder : returns G continuity order of vertex with rank iVertex
  * @param iVertex
  * rank of vertex. Same as rank in wirebody for open curves, 
  * add 1 to index of wire body for closed curves
  * @return
  * returns 0 if OK, nonzero on error
  */
  virtual CATLONG32 GetGOrder(int iVertex, CATFrFContinuity &oGOrder) = 0;

  /**
  * GetDeltaPt : returns oL = G0 gap between points at vertex iVertex
  * @param iVertex
  * rank of vertex. Same as rank in wirebody for open curves,
  * add 1 to index of wire body for closed curves
  * @return
  * returns 0 if OK, nonzero on error
  */
  virtual CATLONG32 GetDeltaPt(int iVertex, CATLength &oL) = 0;

  /**
  * GetDeltaTg : returns oA = G1 gap between tangents at vertex iVertex, in degrees
  * @param iVertex
  * rank of vertex. Same as rank in wirebody for open curves,
  * add 1 to index of wire body for closed curves
  * @return
  * returns 0 if OK, nonzero on error
  */
  virtual CATLONG32 GetDeltaTg(int iVertex, CATAngle &oA) = 0;

  virtual CATLength GetTolPt() = 0;

  virtual CATAngle GetTolTg() = 0;

  virtual double GetTolCv() = 0;

protected:
  /**
   * Destructor
   */
  virtual ~CATICGMTopWireContinuity(); // -> delete can't be called
};

/**
 * Creates the operator to evaluate continuity order and values at vertices of a wire.
 * @param iFactory
 * The pointer to the geometry factory.
 * @param iData
 * The pointer to the data defining the software configuration and the journal. If the journal inside <tt>iData</tt> 
 * @param iWireBody
 * The body to evaluate. It is a body made of a single wire domain.
 * @return
 * The pointer to the created operator. To delete with the usual C++ <tt>delete</tt> operator.
 */
ExportedByCATGMOperatorsInterfaces CATICGMTopWireContinuity *CATCGMCreateTopWireContinuity(
  CATGeoFactory *iFactory,
  CATTopData *iTopData,
  CATBody *iWireBody);

#endif /* CATICGMTopWireContinuity_h_ */
