# COPYRIGHT DASSAULT SYSTEMES 1999
#======================================================================
# Imakefile for module CATMldMoldDesignUI.m
#======================================================================
#
#  Oct 1999  Creation: Code generated by the CAA wizard				fdm
#  Dec 1999  Modify: SELECT  <=> CATIMove				 			bga
#  Jan 2000  Modify: Cut & Paste									pdy
#  Jan 2000  Modify: Add CATMldEjectorPinUI							fdm
#  Jan 2000  Modify: Add CATMldLeaderPinUI CATMldLocatingRingUI 	fdm
#  Jan 2000  Modify: Add GSMInterfaces et GSMModel					cqo
#  Jan 2000  Modify: Add AS0SEL	and CATAssCouMdl					bga
#  Feb 2000  Modify: Add DI0BUILD									fdm
#  Feb 2000  Modify: Add CATMldRunnerModel						cqo
#  Feb 2000  Modify: Add CATMldRunnerUI & suppress CATMldGateAndRunnerUI	cqo
#  Feb 2000  Modify: CATMldServicesUI Become alone ans independant  fdm
#  21/02/2000 : fdm; Correction mkmk-WARNING
#  21/02/2000 : bga; Add CATMmuConstraint
#  Mar 2000  Modify: Add CATMldGateUI.m	et CATMldGateModel.m		cqo
#  Mar 2000  Modify: pdy; Add CATMldSprueBushingUI inside the INCLUDED_MODULES label
#  Apr 2000  Modify: Add CATMoldInterfaces & CATMoldDesignFeature	cqo
#  Apr 2000  Modify: Add CATMldError                                chb
#  Apr 2000  Modify: pdy; Add CATMldCoolantChannelUI & CATMldCoolantChannelModel 
#                         inside the INCLUDED_MODULES label
#  03/05/2000 : chb; Add CATMldNotif
#  04/05/2000 : fdm; Add CATMldScrewUI
#  12/05/2000 : cqo; Suppression de CATMldGateModel
#  15/05/2000 : fdm; CATMldComponentUI
#  May 2000   : bga; Modify: Add CATMldLocateUI => add CD0WIN,CD0FRAME,
#					 YN000MFL,DI0APPLI,AC0XXLNK,JS0STR,JS0SCBAK,
#					 JS0CORBA,NS0S3STR,SketcherToolsUI .
#  05/06/2000 : fdm; Suppression de CATSkuBase
#  08/06/2000 : fdm; Ajout CATMldComponentSUI
#  13/06/2000 : fdm; Ajout CATMldCapScrewUI, 
#  14/06/2000 : fdm; Ajout CATMldSleeveUI, CATMldBushingUI
#  15/06/2000 : fdm; Desactivation CATMldScrewUI
#  05/07/2000 : fdm; Suppression CATMldComponentSUI
#  04/07/2000 : lch; Ajout CATMldEjectorUI + CATMldDowelPinUI + CATFreeStyleResources
#  05/07/2000 : lch; Ajout CATMldUserComponentUI
#  05/07/2000 : jcb; Ajout CATMldFlatEjectorUI, CATMldAnglePinUI + Delete
#  13/07/2000 : mlh; Suppression CATMldCoolantChannelModel
#  03/08/2000 : fdm; Ajout CATMldSettings
#  04/10/2000 : cqo; Ajout YI00IMPL
#  25/10/2000 : fdm; Deplacement de CATMldError
#  27/10/2000 : lch; Ajout composants CorePin, CountersunkScrew, EjectorSleeve
#					 EyeBolt, KnockOut, LockingScrew, SpruePuller, Insert
#  15/11/2000 : pem; Split du Fw LiteralFeatures dans le nouveau FW KnowledgeModele
#  07/12/2000 : lch; Ajout CATAssemblyInterfaces
#  15/03/2001 : lch; Ajout CATMldORingUI
#  07/06/2001 : fdm; Suppression reference a CATMoldDesignFeature
#  06/08/2001 : fdm; Suppression reference inutile
#  09/08/2001 : fdm; Suppression reference inutile
#  23/08/2001 : fdm; Gestion des unites
#  28/09/2001 : fdm; Use of CATToolingServices
#  03/10/2001 : lch; Ajout CATMldSliderUI
#  08/10/2001 : lch; Ajout CATMldRetainersUI
#  10/10/2001 : lch; Ajout CATMldPlugUI
#  14/01/2002 : fdm; Add KnowledgeItf
#  08/02/2002 : lch; Ajout CATMmrAxisSystem
#  06/02/2002 : fdm; Suppression de CATGmoIntegration
#  06/03/2002 : fdm; Creation du FW CATMoldDesignWorkbench
#  15/05/2002 : fdm; Add CATViz
#  22/05/2002 : lch; Ajout CATMldSpringUI
#  24/10/2002 : fdm; R11 
#  28/10/2002 : lch; Add CATTPSItf
#  14/11/2002 : lch; Add CATTPSUUID
#  29/11/2002 : lch; Add CATUdfInterfaces
#  16/01/2003 : fdm; Delete CATMathStream	
#  11/02/2003 : fdm; Use of WebBrowser
#  09/04/2003 : lch; Add CATMldBaffleUI
#  28/07/2003 : lch; Suppression de CATMmrAxisSystem
#  11/08/2003 : lch; Ajout AC0ITEMS	
#  24/03/2005 : juw; Ajout CATTechResInterfaces
#  28/12/2005 : lch; Ajout CATAssCommands
#  22/08/2005 : lch; Ajout CATFecServices
#======================================================================
#
# SHARED LIBRARY CATMldMoldDesignUI
#
BUILT_OBJECT_TYPE=SHARED LIBRARY
 
# DO NOT EDIT :: THE CAA2 WIZARDS WILL ADD CODE HERE
WIZARD_LINK_MODULES = 
# END WIZARD EDITION ZONE

INCLUDED_MODULES =  CATMldAnalyzeUI \
                    CATMldAnglePinUI \
					CATMldBaffleUI \
					CATMldBaseUI \
					CATMldBushingUI \
					CATMldCapScrewUI \
					CATMldComponentUI \
					CATMldDowelPinUI \
					CATMldFlatEjectorUI \
					CATMldGateUI \
					CATMldLocateUI \
					CATMldRunnerUI \
					CATMldLeaderPinUI \
					CATMldEjectorPinUI \
					CATMldEjectorUI \
					CATMldLocatingRingUI \
					CATMldNotif \
					CATMldSleeveUI \
					CATMldStopPinUI \
					CATMldSupportPillarUI \
					CATMldRunnerModel \
                    CATMldSprueBushingUI \
					CATMldUserComponentUI \
					CATMldCorePinUI \
					CATMldCountersunkScrewUI \
					CATMldEjectorSleeveUI \
					CATMldEyeBoltUI \
					CATMldKnockOutUI \
					CATMldLockingScrewUI \
                    CATMldSpruePullerUI \
                    CATMldInsertUI \
                    CATMldORingUI \
                    CATMldSliderUI \
                    CATMldRetainersUI \
                    CATMldPlugUI \
                    CATMldSpringUI \
					CATMldCoolantChannelUI 
                    

LINK_WITH =  CATApplicationFrame \				# ApplicationFrame
			 CD0WIN CD0FRAME \					# ApplicationFrame
			 CATAssemblyInterfaces \			# CATAssemblyInterfaces
			 CATAssCommands \					# CATAssCommands
			 CATTPSItf	\
			 CATTPSUUID	\
			 SELECT \							# CATIAApplicationFrame
			 CATMoldInterfaces \				# CATMoldInterfaces
			 CATMldServicesUI \					# CATMldServicesUI
			 CATTlgServices \					# CATToolingServices
			 CATCclInterfaces \					# ComponentsCatalogs
			 DI0PANV2  \						# Dialog
			 DI0APPLI DI0STATE \				# DialogEngine
			 CATFreeStyleResources \			# FreeStyleResources
			 CATFrrComponents	\				# FreeStyleResources
			 CATCGMGeoMath YP00IMPL \			# GeometricObjects
			 CATGitInterfaces \					# GSMInterfaces 
			 CATGmoUtilities \					# GSMModel 
			 CATInteractiveInterfaces \			# InteractiveInterfaces
			 CATKnowledgeModeler \				# KnowledgeModele
			 CATLiteralsEditor	\				# LiteralsEditor
			 CK0FEAT \							# LiteralFeatures
			 KnowledgeItf \						# KnowledgeInterfaces
			 CATMathematics YN000MFL \			# Mathematics
			 MF0STARTUP \						# MechanicalModeler
			 MechFeatCom \						# MechanicalModelerUI
			 CATMcoInterfaces \					# MechanicalCommands
			 CATUdfInterfaces \					# MechanicalCommands
			 MecModItfCPP \						# MecModInterfaces
			 YI00IMPL \							# NewTopologicalObjects
			 AD0XXBAS AC0XXLNK \				# ObjectModelerBase
			 AC0SPBAS AC0ITEMS \							# ObjectSpecsModeler
			 PartItf \							# PartInterfaces
			 ProductStructureItf \				# ProductStructureInterfaces
			 PRDCommands \						# ProductStructureUI
			 AS0STARTUP \						# ProductStructure
			 SketcherItf \						# SketcherInterfaces
			 CATSktAssistant CATSktSettings \	# SketcherToolsUI
			 JS0GROUP JS0FM JS0STR \			# System
			 JS0SCBAK JS0CORBA NS0S3STR \		# System
			 CATVisualization \					# Visualization
			 CATViz \							# VisualizationBase
          CATMathStream \
          CATConstraintModeler \
          CATConstraintModelerItf \
          CATConstraintModelerUI \
          CATTerTechnoResultItf \     #CATTechnoResultInterfaces
          CATTerTechnoResultInterfacesUUID \ #CATTechnoResultInterfaces
			CATFecServices				# FeatureCommands CATExternalLinkObject.h

			 

# System dependant variables
# 	

OS = AIX
#
OS = HP-UX
#
OS = IRIX
#
OS = SunOS
#
OS = Windows_NT
LINK_WITH =  CATApplicationFrame \				# ApplicationFrame
			 CD0WIN CD0FRAME \					# ApplicationFrame
			 CATAssemblyInterfaces \			# CATAssemblyInterfaces
			 CATAssCommands \					# CATAssCommands
			 CATTPSItf	\
			 CATTPSUUID	\
			 SELECT \							# CATIAApplicationFrame
			 CATMoldInterfaces \				# CATMoldInterfaces
			 CATMldServicesUI \					# CATMldServicesUI
			 CATTlgServices \					# CATToolingServices
			 CATCclInterfaces \					# ComponentsCatalogs
			 DI0PANV2 CATDlgHtml \				# Dialog
			 DI0APPLI DI0STATE \				# DialogEngine
			 CATFreeStyleResources \			# FreeStyleResources
			 CATFrrComponents	\				# FreeStyleResources
			 CATCGMGeoMath YP00IMPL \			# GeometricObjects
			 CATGitInterfaces \					# GSMInterfaces 
			 CATGmoUtilities \					# GSMModel 
			 CATInteractiveInterfaces \			# InteractiveInterfaces
			 CATKnowledgeModeler \				# KnowledgeModele
			 CATLiteralsEditor	\				# LiteralsEditor
			 CK0FEAT \							# LiteralFeatures
			 KnowledgeItf \						# KnowledgeInterfaces
			 CATMathematics YN000MFL \			# Mathematics
			 MF0STARTUP \						# MechanicalModeler
			 MechFeatCom \						# MechanicalModelerUI
			 CATMcoInterfaces \					# MechanicalCommands
			 CATUdfInterfaces \					# MechanicalCommands
			 MecModItfCPP \						# MecModInterfaces
			 YI00IMPL \							# NewTopologicalObjects
			 AD0XXBAS AC0XXLNK \				# ObjectModelerBase
			 AC0SPBAS AC0ITEMS \							# ObjectSpecsModeler
			 PartItf \							# PartInterfaces
			 ProductStructureItf \				# ProductStructureInterfaces
			 PRDCommands \						# ProductStructureUI
			 AS0STARTUP \						# ProductStructure
			 SketcherItf \						# SketcherInterfaces
			 CATSktAssistant CATSktSettings \	# SketcherToolsUI
			 JS0GROUP JS0FM JS0STR \			# System
			 JS0SCBAK JS0CORBA NS0S3STR \		# System
			 CATVisualization \					# Visualization
			 CATViz \							# VisualizationBase
          CATMathStream \
          CATConstraintModeler \
          CATConstraintModelerItf \
          CATConstraintModelerUI \
          CATTerTechnoResultItf \     #CATTechnoResultInterfaces
          CATTerTechnoResultInterfacesUUID \ #CATTechnoResultInterfaces
			CATFecServices				# FeatureCommands CATExternalLinkObject.h
