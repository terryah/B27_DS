#ifndef DNBIAMfgAssemblyType_IDL
#define DNBIAMfgAssemblyType_IDL
/*IDLREP*/

// COPYRIGHT DASSAULT SYSTEMES 2005

/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */

//								   
// DNBIAMfgAssemblyType:						   
// enumeration of the Manufacturing Assembly
//								   

/**  
 * This file defines the possible types of a Manufacturing Assembly.
 * <b>Role</b>: List the possible types of a Manufacturing Assembly
 * namely
 * <ul>
 * <li> "manufacturingAssembly" declares the base Manufacturing Assembly 
 * <li> "manufacturingKit" declares the Manufacturing Kit.
 * </ul>
 * <! @sample>
 * <dl>
 * <dt><b>Example:</b>
 * <dd>
 * This example fetches an types of the manufacturing assembly from the object of type MfgAssembly
 * <pre>
 * Dim maObject As MfgAssembly
 * .................
 * .................
 * Dim maType
 * set maType = maObject.MAType
 * MsgBox maType
 * </pre>
 * </dd>
 * </dl>
 * @see DNBIAMfgAssembly
*/

enum DNBIAMfgAssemblyType
{
	manufacturingAssembly,
	manufacturingKit,
	assemblySpecTree,
	manufacturingOutput,
	notSpecified
};

#endif // DNBIAMfgAssemblyType_IDL

