#
#   Imakefile.mk for DNBAsyActivityItf.m
#   Copyright (C) DELMIA Corp., 2000
#

BUILT_OBJECT_TYPE = SHARED LIBRARY

INCLUDED_MODULES = DNBAsyActivityItfCPP DNBAsyActivityProIDL DNBAsyActivityPubIDL

LINK_WITH =  JS0GROUP                        \ # System
             JS0CORBA                        \ # System
             DNBSimActivityItf               \ # DNBSimActivityInterfaces
             CATApplicationFrame             \ # ApplicationFrame

#
# Define the build options for the current module.
#
OS      = Windows_NT
BUILD   = YES

OS      = IRIX
BUILD   = YES

OS      = SunOS
BUILD   = YES

OS      = AIX
BUILD   = YES

OS      = HP-UX
BUILD   = YES

OS      = win_a
BUILD   = NO

