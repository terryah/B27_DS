#ifndef DNBIAsyMotionActivity_H
#define DNBIAsyMotionActivity_H

//-----------------------------------------------------------------------------
// COPYRIGHT DASSAULT SYSTEMES 2002
//-----------------------------------------------------------------------------
// DNBIAsyMotionActivity
//      This interface identifies Assembly Move Activity.
//-----------------------------------------------------------------------------
// HISTORY:
//     Author       Date            Purpose
//     ------       ----            -------
//     mso          11/07/2002      original implementation for APS.1119 
//                                  Save Band Analysis
//     sar          11/14/2002      Add methods related to Move Visu
//                                  Highlight APS.1105
//     sar          03/31/2003      Added methods to support both speed and 
//                                  time based moves.
//     sar          05/07/2003      Added methods to access motion speed.
//     sar          07/17/2003      Added methods to access reverse mode.
//     axb          07/30/2003      Added method GetObjects to combine items 
//                                  and resources list.
//     sar          09/04/2003      changed CreateMoveVisu() signature to fix
//                                  RI 405179.
//     axb          11/10/2003      Added methods GetReferenceObject, SetReferenceObject,
//                                  for Repositioning a move activity. 
//     mli          12/08/2004      Modifications to add monitor parameter to analyses functions,
//                                  in support of the Monitor window.
//     dcg          04/22/2005      Added two new functions to get/set section planes to a
//                                  move activity, for hightlight R16APS001
//     sar          07/11/2005      Signature change for CreateMoveVisu and RemoveMoveVisu and 
//                                  addition of UpdateMoveVisu - IR 502238
//-----------------------------------------------------------------------------


/**
 * @CAA2Level L0
 * @CAA2Usage U3
 */


#include "CATBaseUnknown.h"
#include "CATBooleanDef.h"
#include "CATISpecObject.h"
#include "CATMathTransformation.h"


class CATITrack;
class DNBAsyMotionVisu;
class CATListValCATBaseUnknown_var;


#include "ExportedByDNBAsyActivityItf.h"
extern ExportedByDNBAsyActivityItf IID IID_DNBIAsyMotionActivity;


/**
 * The motion mode is used to define the pace of movement along the 
 * trajectory for the move activity.
 * @param DNBAsySpeedMode
 *      The pace the motion occurs is at a constant speed.  A user
 *      specified amount is incremented each second of time.  The time
 *      is changed to reflect the speed constaint.
 * @param DNBAsyTimeMode
 *      The overall motion occurs in a user specified (constant 
 *      amount of) time.  The speed is changed to reflect the time constaint.
 */
enum DNBAsyMotionMode
{
    DNBAsySpeedMode,    // Speed based move
    DNBAsyTimeMode      // Time based move
};


class  ExportedByDNBAsyActivityItf DNBIAsyMotionActivity : public CATBaseUnknown
{
    CATDeclareInterface;
    
public: 
    
    /**
     * Adds (binds) an analysis object to the Move Activity.
     * @param iAnalysis
     *   The analysis to add (bind) to the Move Activity
     * @param iAnalysisMode
     *   Indicates the analysis status for iAnalysis. <br>
     *   Legal Values: <br>
     *   <ul>
     *      <li> <b>0</b> : To set analysis to <b>Off</b>
     *      <li> <b>1</b> : To set analysis to <b>On</b>
     *      <li> <b>2</b> : To set analysis to <b>Stop</b>
     *      <li> <b>3</b> : To set analysis to <b>Verbose</b>
     *   </ul>
     * @param iMonitorMode
     *   Indicates the monitor status for iAnalysis. <br>
     *   Legal Values: <br>
     *   <ul>
     *      <li> <b>0</b> : To set monitor to <b>Off</b>
     *      <li> <b>1</b> : To set monitor to <b>On</b>
     *   </ul>
     */
    virtual HRESULT AddAnalysis(CATBaseUnknown *iAnalysis, int iAnalysisMode, int iMonitorMode) = 0;


    /**
     * Returns the analysis objects bound to a Move Activity.
     * @param oNb
     *   The number of analyses bound to the Move Activity.
     * @param oAnalysis
     *   A list of analyses bound to the Move Activity.
     * @param oState
     *   A list of the analysis status for each analysis object.<br>
     *   Legal Values: <br>
     *   <ul>
     *      <li> <b>0</b> : To set analysis to <b>Off</b>
     *      <li> <b>1</b> : To set analysis to <b>On</b>
     *      <li> <b>2</b> : To set analysis to <b>Stop</b>
     *      <li> <b>3</b> : To set analysis to <b>Verbose</b>
     *   </ul>
     * @param oMonitor
     *   A list of the monitor status for each analysis object.<br>
     *   Legal Values: <br>
     *   <ul>
     *      <li> <b>0</b> : To set monitor to <b>Off</b>
     *      <li> <b>1</b> : To set monitor to <b>On</b>
     *   </ul>
     */
    virtual HRESULT GetAnalysis(int &oNb, CATBaseUnknown **&oAnalysis, int *&oState, int *&oMonitor) = 0;


    /**
     * Removes all the bind analysis objects.
     * <br><b>Role:</b> Removes all the bind analysis 
     * objects currently associated to the move activity.
     */
    virtual HRESULT RemoveAllAnalysis() = 0;
  

    /**
     * Creates a visualization object.
     * <br><b>Role:</b> Creates a visualization object for the move activity.
     * @param opMoveVisu
     *   The visualization object for the current assembly move.
     * @param iForceCreation
     *   Used to force creation and show the visualization. <br>
     *   Legal Values: <br>
     *   <ul>
     *      <li> <b>FALSE</b> : Update the HasVisu variable only if it exists. (default).
     *      <li> <b>TRUE</b> : Always update the HasVisu variable.
     *   <\ul>
     */
    virtual HRESULT CreateMoveVisu(DNBAsyMotionVisu ** opMoveVisu, boolean iForceCreation = FALSE) = 0;


    /**
     * Returns the visualization object.
     * <br><b>Role:</b> Retrieves the visualization object for the current
     * move activity.
     * @return 
     *   The visualization object fot the current move activity.
     */
    virtual DNBAsyMotionVisu* GetMoveVisu() = 0;


    /**
     * Removes the visualization object.
     * <br><b>Role: </b> Deletes the corresponding visualization object 
     * for the current move activity.
     * @param iUpdateModel
     *   Used to update the HasVisu attribute.<br>
     *   Legal Values:
     *   <ul>
     *      <li> <b>TRUE</b> : Update the HasVisu attribute to FALSE,
     *           that is the visualization is not present (default).
     *      <li> <b>FALSE</b> : Do not update the HasVisu attribute.
     *   </ul>
     */
    virtual HRESULT RemoveMoveVisu(boolean iUpdateModel = TRUE) = 0;


    /**
      * Update the visualization for the move activity.
      * <br><b>Role:</b> Update the corresponding visualization object for
      * the move activity.
      */
    virtual HRESULT UpdateMoveVisu() = 0;


    /**
      * Set the state of the visibility of the move activity's trajectory.
      * <br><b>Role:</b> Change the state of the visual representation of 
      * the trajectory for the move activity.  That is, to either show or
      * hide it.
      * @param iVis
      * Desired visibility state. <br>
      *   Legal Values:
      *   <ul>
      *      <li> <b>TRUE</b>: to <b>Show</b> the trajectory visibility.
      *      <li> <b>FALSE</b>: to <b>Hide</b> the trajectory visibility.
      *   </ul>
      */
    virtual HRESULT SetMotionVisibility(boolean iVis) = 0;


    /**
      * Get the status of the visibility of the move activity's trajectory.
      * <br><b>Role:</b> Get the status of the visual representation of 
      * the trajectory for the move activity.  That is if its currently shown
      * or hidden.
      * @param oVis
      * Current visibility state. <br>
      *   Legal Values:
      *   <ul>
      *      <li> <b>TRUE</b>: The trajectory visibility is currently <b>Shown</b> 
      *      <li> <b>FALSE</b>: The trajectory visibility is currently <b>Hidden</b> 
      *   </ul>
      */
    virtual HRESULT GetMotionVisibility(boolean &oVis) = 0;


    /**
      * Get the move mode of the current move activity.
      * <br><b>Role</b>: Obtain the current move mode of the move activity.
      * When a move activity is created/edited, the trajectory 
      * can be defined in either time or speed based modes.
      * @param oMoveMode
      *   The move mode of the current move activity.
      */
    virtual HRESULT GetMoveMode(DNBAsyMotionMode &oMoveMode) = 0;


    /**
      * Sets the move mode of the current activity.
      * <br><b>Role</b>: Sets the current move mode of the move activity.
      * When a move activity is created/edited, the trajectory
      * can be defined in eiter time or speed based modes.
      * @param iMoveMode
      *   The new move mode for the current move activity.
      */
    virtual HRESULT SetMoveMode(DNBAsyMotionMode iMoveMode) = 0;


    /**
      * Gets the speed of the move activity.
      * <br><b>Role</b>: Gets the speed at which the move activity's related
      * items move along the trajectory at.
      * @param oMotionSpeed
      *   The speed of the move activity.
      */
    virtual HRESULT GetMotionSpeed( double &oMotionSpeed) = 0;


    /**
      * Sets the speed of the move activity.
      * <br><b>Role</b>: Sets the speed at which the move activity's related
      * items move along the trajectory at.
      * @param iMotionSpeed
      *   The value of the speed for the move activity.  It should be greater
      *   than zero.
      */
    virtual HRESULT SetMotionSpeed( double iMotionSpeed) = 0;


    /** @nodoc
      * For internal use, mainly related to how items are positioned
      * when editing is done.  Currently there are no requests from any users
      * to have such functionality exposed.
      *
      * Checks if the Activity is reversed and gets the status.
      * TRUE - Reversed, FALSE - Not reversed.
      */
    virtual HRESULT IsReversed( boolean &oRev) = 0;


    /**
      * Gets the move activity's items and resources.
      * <br><b>Role</b>: Retrieves the items and resources that are 
      * associated to the move activity and places them all into one
      * list.
      * @param outputList
      *   The list used to contain all the related objects.
      */
    virtual HRESULT GetObjects(CATListValCATBaseUnknown_var * & outputList) = 0;


    /** @nodoc
      * For internal use, mainly related to attach/update object track 
      * functionality with a move activity.  Currently there are no requests
      * from any users to have such functionality exposed.
      *
      * Used to get the object associated to the move activity.
      */
    virtual HRESULT GetReferenceObject(CATISpecObject * & oSelectObject) = 0;


    /** @nodoc
      * For internal use, mainly related to attach/update object track 
      * functionality with a move activity.  Currently there are no requests
      * from any users to have such functionality exposed.
      * 
      * Used to set the object associated to the move activity.
      */
    virtual HRESULT SetReferenceObject(CATISpecObject * iSelectObject) = 0;


    /** @nodoc
      * Get the list of associated sections.  This list is of FITSection objects.
      * Currently in order to associate a section to a move activity we need to 
      * perform various items with FITSection objects and that code is not a the 
      * public interface level yet.
      */
    virtual HRESULT GetSectionList (CATListValCATBaseUnknown_var & sectionList) = 0;


    /** @nodoc
      * Set the list of associated sections.  This list is of FITSection objects.
      * Currently in order to associate a section to a move activity we need to 
      * perform various items with FITSection objects and that code is not a the 
      * public interface level yet.
      */
    virtual HRESULT SetSectionList (CATListValCATBaseUnknown_var & sectionList) = 0;


    /**
      * Gets a CATITrack object derived from a move activity.
      * <br><b>Role:</b> Obtains a DMU Fitting Track object that contains
      * all of the trajectory information from the move activity.
      * The track will contain all of targets/shots of the move activity
      * along with speed, time and move mode.  Also note that the items 
      * and resources that are associated to the move activity will be 
      * all placed into a shuttle which is associated to the track.
      * @param oTrack
      *   The track that will be used to contain the move activity's
      *   information.  Previous information in the track will be
      *   overwritten.
      */
    virtual HRESULT GetTrack (CATITrack *& oTrack) = 0;


    /**
      * Updates the move activity with data from a CATITrack object.
      * <br><b>Role:</b> Updates the move activity with information
      * stored in a DMU Fitting Track.  The move activity will be
      * updated with all of the targets/shots from the track along 
      * with the speed and time.  Please note that the contents of 
      * the move activity will be overwritten.
      * @param iTrack
      *   The source track to update the move activity with.
      */
    virtual HRESULT UpdateWithTrackData (CATITrack * iTrack) = 0;

};

CATDeclareHandler( DNBIAsyMotionActivity, CATBaseUnknown );

#endif // DNBIAsyMotionActivity_H
