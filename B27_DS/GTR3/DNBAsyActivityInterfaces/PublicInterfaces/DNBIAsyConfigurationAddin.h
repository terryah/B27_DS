#ifndef DNBIAsyConfigurationAddin_h
#define DNBIAsyConfigurationAddin_h

//===================================================================
// COPYRIGHT DELMIA CORP. 1999
// COPYRIGHT DASSAULT SYSTEMES 2006
//===================================================================
//
// DNBIAsyConfigurationAddin.h
// Definition of the interface to provide an addin for workbench DNBAsy.
//
//===================================================================
//
// Usage notes:
// This class defines a new addin interface for the workbench 
// DNBAsy.
//
//===================================================================
// Code History : 
//      cre     ???     ??/??/????      Creation
//      mod     bpl     04/12/2006      Moved from DNBAssemblyUI to DNBAsyActivityInterfaces
//===================================================================

/**  
*  @CAA2Level  L0
*  @CAA2Usage  U5    
*/  

#include <CATIWorkbenchAddin.h>
#include "ExportedByDNBAsyActivityItf.h"

#ifndef LOCAL_DEFINITION_FOR_IID
    extern ExportedByDNBAsyActivityItf IID IID_DNBIAsyConfigurationAddin;
#else
    extern "C" const IID IID_DNBIAsyConfigurationAddin;
#endif

/**  
*  Interface  to  create  an  addin  within  DPM Assembly workbench.  
*  <b>Role</b>:  DPM Assembly  workbench  can  be  customized  by  the  client    
*  application  programmer.  This  customization  consists  in  putting  commands  into  one  or    
*  several  new  toolbars,  and  add  these  toolbars  to  the  workbench.    
*  See  @href  CATIWorkbenchAddin#CreateCommands  or  @href  CATIWorkbenchAddin#CreateToolbars  methods  
*  that  should  be  implemented.  
*/  
class ExportedByDNBAsyActivityItf DNBIAsyConfigurationAddin : public CATIWorkbenchAddin
{
    CATDeclareInterface;
    
    public:
};

CATDeclareHandler (DNBIAsyConfigurationAddin, CATIWorkbenchAddin);

#endif // DNBIAsyConfigurationAddin_h
