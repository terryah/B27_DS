//=================================================================== 
//  COPYRIGHT  Dassault  Systemes  2010
//=================================================================== 
#ifdef  _WINDOWS_SOURCE
#ifdef  __DNBAsyActivityItfCPP
#define ExportedByDNBAsyActivityItf        __declspec(dllexport)
#else
#define ExportedByDNBAsyActivityItf        __declspec(dllimport)
#endif
#else
#define ExportedByDNBAsyActivityItf
#endif

/**  
*  @CAA2Level  L0
*  @CAA2Usage  U2  
*/ 
