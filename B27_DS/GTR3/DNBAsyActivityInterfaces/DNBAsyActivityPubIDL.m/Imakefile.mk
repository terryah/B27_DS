# COPYRIGHT DASSAULT SYSTEMES 2005
#======================================================================
# Imakefile for module DNBAsyActivityPubIDL
# Module for compilation of the public IDL interfaces
#======================================================================
#
#  Sept 2002  Creation:                                             DCG
#======================================================================
#
# NO BUILD             
#

BUILT_OBJECT_TYPE=NONE

SOURCES_PATH=PublicInterfaces
