#ifndef __TIE_DNBIASectionVisibilityActivity
#define __TIE_DNBIASectionVisibilityActivity

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIASectionVisibilityActivity.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIASectionVisibilityActivity */
#define declare_TIE_DNBIASectionVisibilityActivity(classe) \
 \
 \
class TIEDNBIASectionVisibilityActivity##classe : public DNBIASectionVisibilityActivity \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIASectionVisibilityActivity, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall AddSections(const CATSafeArrayVariant & iListSections); \
      virtual HRESULT __stdcall GetSections(CATSafeArrayVariant & oListSections); \
      virtual HRESULT __stdcall RemoveSections(const CATSafeArrayVariant & iListSections); \
      virtual HRESULT __stdcall get_Activated(CAT_VARIANT_BOOL & oActivated); \
      virtual HRESULT __stdcall put_Activated(CAT_VARIANT_BOOL iActivated); \
      virtual HRESULT __stdcall get_SectionsCount(CATLONG & onbSections); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIASectionVisibilityActivity(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall AddSections(const CATSafeArrayVariant & iListSections); \
virtual HRESULT __stdcall GetSections(CATSafeArrayVariant & oListSections); \
virtual HRESULT __stdcall RemoveSections(const CATSafeArrayVariant & iListSections); \
virtual HRESULT __stdcall get_Activated(CAT_VARIANT_BOOL & oActivated); \
virtual HRESULT __stdcall put_Activated(CAT_VARIANT_BOOL iActivated); \
virtual HRESULT __stdcall get_SectionsCount(CATLONG & onbSections); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIASectionVisibilityActivity(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::AddSections(const CATSafeArrayVariant & iListSections) \
{ \
return (ENVTIECALL(DNBIASectionVisibilityActivity,ENVTIETypeLetter,ENVTIELetter)AddSections(iListSections)); \
} \
HRESULT __stdcall  ENVTIEName::GetSections(CATSafeArrayVariant & oListSections) \
{ \
return (ENVTIECALL(DNBIASectionVisibilityActivity,ENVTIETypeLetter,ENVTIELetter)GetSections(oListSections)); \
} \
HRESULT __stdcall  ENVTIEName::RemoveSections(const CATSafeArrayVariant & iListSections) \
{ \
return (ENVTIECALL(DNBIASectionVisibilityActivity,ENVTIETypeLetter,ENVTIELetter)RemoveSections(iListSections)); \
} \
HRESULT __stdcall  ENVTIEName::get_Activated(CAT_VARIANT_BOOL & oActivated) \
{ \
return (ENVTIECALL(DNBIASectionVisibilityActivity,ENVTIETypeLetter,ENVTIELetter)get_Activated(oActivated)); \
} \
HRESULT __stdcall  ENVTIEName::put_Activated(CAT_VARIANT_BOOL iActivated) \
{ \
return (ENVTIECALL(DNBIASectionVisibilityActivity,ENVTIETypeLetter,ENVTIELetter)put_Activated(iActivated)); \
} \
HRESULT __stdcall  ENVTIEName::get_SectionsCount(CATLONG & onbSections) \
{ \
return (ENVTIECALL(DNBIASectionVisibilityActivity,ENVTIETypeLetter,ENVTIELetter)get_SectionsCount(onbSections)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIASectionVisibilityActivity,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIASectionVisibilityActivity,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIASectionVisibilityActivity,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIASectionVisibilityActivity,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIASectionVisibilityActivity,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIASectionVisibilityActivity(classe)    TIEDNBIASectionVisibilityActivity##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIASectionVisibilityActivity(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIASectionVisibilityActivity, classe) \
 \
 \
CATImplementTIEMethods(DNBIASectionVisibilityActivity, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIASectionVisibilityActivity, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIASectionVisibilityActivity, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIASectionVisibilityActivity, classe) \
 \
HRESULT __stdcall  TIEDNBIASectionVisibilityActivity##classe::AddSections(const CATSafeArrayVariant & iListSections) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&iListSections); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->AddSections(iListSections); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&iListSections); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASectionVisibilityActivity##classe::GetSections(CATSafeArrayVariant & oListSections) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&oListSections); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetSections(oListSections); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&oListSections); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASectionVisibilityActivity##classe::RemoveSections(const CATSafeArrayVariant & iListSections) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&iListSections); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->RemoveSections(iListSections); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&iListSections); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASectionVisibilityActivity##classe::get_Activated(CAT_VARIANT_BOOL & oActivated) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&oActivated); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Activated(oActivated); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&oActivated); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASectionVisibilityActivity##classe::put_Activated(CAT_VARIANT_BOOL iActivated) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&iActivated); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Activated(iActivated); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&iActivated); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIASectionVisibilityActivity##classe::get_SectionsCount(CATLONG & onbSections) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&onbSections); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_SectionsCount(onbSections); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&onbSections); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIASectionVisibilityActivity##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIASectionVisibilityActivity##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIASectionVisibilityActivity##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIASectionVisibilityActivity##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIASectionVisibilityActivity##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,11,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,11,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIASectionVisibilityActivity(classe) \
 \
 \
declare_TIE_DNBIASectionVisibilityActivity(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIASectionVisibilityActivity##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIASectionVisibilityActivity,"DNBIASectionVisibilityActivity",DNBIASectionVisibilityActivity::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIASectionVisibilityActivity(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIASectionVisibilityActivity, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIASectionVisibilityActivity##classe(classe::MetaObject(),DNBIASectionVisibilityActivity::MetaObject(),(void *)CreateTIEDNBIASectionVisibilityActivity##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIASectionVisibilityActivity(classe) \
 \
 \
declare_TIE_DNBIASectionVisibilityActivity(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIASectionVisibilityActivity##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIASectionVisibilityActivity,"DNBIASectionVisibilityActivity",DNBIASectionVisibilityActivity::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIASectionVisibilityActivity(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIASectionVisibilityActivity, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIASectionVisibilityActivity##classe(classe::MetaObject(),DNBIASectionVisibilityActivity::MetaObject(),(void *)CreateTIEDNBIASectionVisibilityActivity##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIASectionVisibilityActivity(classe) TIE_DNBIASectionVisibilityActivity(classe)
#else
#define BOA_DNBIASectionVisibilityActivity(classe) CATImplementBOA(DNBIASectionVisibilityActivity, classe)
#endif

#endif
