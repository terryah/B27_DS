#ifndef __TIE_DNBIAsyConfigurationAddin
#define __TIE_DNBIAsyConfigurationAddin

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "DNBIAsyConfigurationAddin.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIAsyConfigurationAddin */
#define declare_TIE_DNBIAsyConfigurationAddin(classe) \
 \
 \
class TIEDNBIAsyConfigurationAddin##classe : public DNBIAsyConfigurationAddin \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIAsyConfigurationAddin, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual void              CreateCommands () ; \
      virtual CATCmdContainer * CreateToolbars () ; \
};



#define ENVTIEdeclare_DNBIAsyConfigurationAddin(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual void              CreateCommands () ; \
virtual CATCmdContainer * CreateToolbars () ; \


#define ENVTIEdefine_DNBIAsyConfigurationAddin(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
void               ENVTIEName::CreateCommands ()  \
{ \
 (ENVTIECALL(DNBIAsyConfigurationAddin,ENVTIETypeLetter,ENVTIELetter)CreateCommands ()); \
} \
CATCmdContainer *  ENVTIEName::CreateToolbars ()  \
{ \
return (ENVTIECALL(DNBIAsyConfigurationAddin,ENVTIETypeLetter,ENVTIELetter)CreateToolbars ()); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIAsyConfigurationAddin(classe)    TIEDNBIAsyConfigurationAddin##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIAsyConfigurationAddin(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIAsyConfigurationAddin, classe) \
 \
 \
CATImplementTIEMethods(DNBIAsyConfigurationAddin, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIAsyConfigurationAddin, classe, 1) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIAsyConfigurationAddin, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIAsyConfigurationAddin, classe) \
 \
void               TIEDNBIAsyConfigurationAddin##classe::CreateCommands ()  \
{ \
   ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateCommands (); \
} \
CATCmdContainer *  TIEDNBIAsyConfigurationAddin##classe::CreateToolbars ()  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateToolbars ()); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIAsyConfigurationAddin(classe) \
 \
 \
declare_TIE_DNBIAsyConfigurationAddin(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAsyConfigurationAddin##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAsyConfigurationAddin,"DNBIAsyConfigurationAddin",DNBIAsyConfigurationAddin::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAsyConfigurationAddin(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIAsyConfigurationAddin, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAsyConfigurationAddin##classe(classe::MetaObject(),DNBIAsyConfigurationAddin::MetaObject(),(void *)CreateTIEDNBIAsyConfigurationAddin##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIAsyConfigurationAddin(classe) \
 \
 \
declare_TIE_DNBIAsyConfigurationAddin(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAsyConfigurationAddin##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAsyConfigurationAddin,"DNBIAsyConfigurationAddin",DNBIAsyConfigurationAddin::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAsyConfigurationAddin(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIAsyConfigurationAddin, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAsyConfigurationAddin##classe(classe::MetaObject(),DNBIAsyConfigurationAddin::MetaObject(),(void *)CreateTIEDNBIAsyConfigurationAddin##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIAsyConfigurationAddin(classe) TIE_DNBIAsyConfigurationAddin(classe)
#else
#define BOA_DNBIAsyConfigurationAddin(classe) CATImplementBOA(DNBIAsyConfigurationAddin, classe)
#endif

#endif
