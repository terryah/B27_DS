/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef DNBIAAsySimActivity_h
#define DNBIAAsySimActivity_h

#ifndef ExportedByDNBAsyActivityPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __DNBAsyActivityPubIDL
#define ExportedByDNBAsyActivityPubIDL __declspec(dllexport)
#else
#define ExportedByDNBAsyActivityPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByDNBAsyActivityPubIDL
#endif
#endif

#include "CATIABase.h"

extern ExportedByDNBAsyActivityPubIDL IID IID_DNBIAAsySimActivity;

class ExportedByDNBAsyActivityPubIDL DNBIAAsySimActivity : public CATIABase
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall Synchronize()=0;


};

CATDeclareHandler(DNBIAAsySimActivity, CATIABase);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
