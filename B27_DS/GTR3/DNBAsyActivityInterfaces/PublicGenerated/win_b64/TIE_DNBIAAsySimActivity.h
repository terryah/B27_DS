#ifndef __TIE_DNBIAAsySimActivity
#define __TIE_DNBIAAsySimActivity

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "DNBIAAsySimActivity.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIAAsySimActivity */
#define declare_TIE_DNBIAAsySimActivity(classe) \
 \
 \
class TIEDNBIAAsySimActivity##classe : public DNBIAAsySimActivity \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIAAsySimActivity, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall Synchronize(); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIAAsySimActivity(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall Synchronize(); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIAAsySimActivity(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::Synchronize() \
{ \
return (ENVTIECALL(DNBIAAsySimActivity,ENVTIETypeLetter,ENVTIELetter)Synchronize()); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIAAsySimActivity,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIAAsySimActivity,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIAAsySimActivity,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIAAsySimActivity,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIAAsySimActivity,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIAAsySimActivity(classe)    TIEDNBIAAsySimActivity##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIAAsySimActivity(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIAAsySimActivity, classe) \
 \
 \
CATImplementTIEMethods(DNBIAAsySimActivity, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIAAsySimActivity, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIAAsySimActivity, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIAAsySimActivity, classe) \
 \
HRESULT __stdcall  TIEDNBIAAsySimActivity##classe::Synchronize() \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Synchronize()); \
} \
HRESULT  __stdcall  TIEDNBIAAsySimActivity##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication)); \
} \
HRESULT  __stdcall  TIEDNBIAAsySimActivity##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent)); \
} \
HRESULT  __stdcall  TIEDNBIAAsySimActivity##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  TIEDNBIAAsySimActivity##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  TIEDNBIAAsySimActivity##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj)); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIAAsySimActivity(classe) \
 \
 \
declare_TIE_DNBIAAsySimActivity(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAAsySimActivity##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAAsySimActivity,"DNBIAAsySimActivity",DNBIAAsySimActivity::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAAsySimActivity(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIAAsySimActivity, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAAsySimActivity##classe(classe::MetaObject(),DNBIAAsySimActivity::MetaObject(),(void *)CreateTIEDNBIAAsySimActivity##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIAAsySimActivity(classe) \
 \
 \
declare_TIE_DNBIAAsySimActivity(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAAsySimActivity##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAAsySimActivity,"DNBIAAsySimActivity",DNBIAAsySimActivity::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAAsySimActivity(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIAAsySimActivity, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAAsySimActivity##classe(classe::MetaObject(),DNBIAAsySimActivity::MetaObject(),(void *)CreateTIEDNBIAAsySimActivity##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIAAsySimActivity(classe) TIE_DNBIAAsySimActivity(classe)
#else
#define BOA_DNBIAAsySimActivity(classe) CATImplementBOA(DNBIAAsySimActivity, classe)
#endif

#endif
