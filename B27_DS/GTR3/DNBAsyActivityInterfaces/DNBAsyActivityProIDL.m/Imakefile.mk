# COPYRIGHT DASSAULT SYSTEMES 2002
#======================================================================
# Imakefile for module DNBAsyActivityProIDL
# Module for compilation of the protected IDL interfaces
#======================================================================
#
#  Jun 2002  Creation:										SAR
#======================================================================
#
# NO BUILD             
#

BUILT_OBJECT_TYPE=NONE

SOURCES_PATH=ProtectedInterfaces
