#======================================================================
# COPYRIGHT DASSAULT SYSTEMES 2002
#======================================================================
# Imakefile for module DNBAsyTypeLib.m
# Module for compilation of the typelib
#======================================================================
#   Modification History:
#       cre     SAR     06/01/2002      Original implementation.
#       mod     mli     09/04/2003      Updated due to change at DS.
#======================================================================
#
# TYPELIB
#
#======================================================================

BUILT_OBJECT_TYPE=TYPELIB
# no more BUILD_PRIORITY=30

LINK_WITH =         InfTypeLib                      \ #
                    ProcessTypeLib                  \ #
                    PSTypeLib                       \ #
