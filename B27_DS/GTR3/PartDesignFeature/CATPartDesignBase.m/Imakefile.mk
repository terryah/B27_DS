#@ autoformat 16:08:07

BUILT_OBJECT_TYPE=SHARED LIBRARY
ALIASES_ON_IMPORT=CATPartDesignBase PartFeatureBase
LINK_WITH_V5_ONLY=
#
LINK_WITH_V6_ONLY=
#
INCLUDED_MODULES = PRTUPGRADE PRTICON PRTNAVIG PRTVISUImpl PrtCtxMenu PRTKweHide PRTIsolate PRTUTILITYBASE

LINK_WITH= \
    $(LINK_WITH_V5_ONLY)           \
    $(LINK_WITH_V6_ONLY)           \
    ON0FRAME                       \ # CATIAApplicationFrame          CATIAApplicationFrame
    CATMathStream                  \ # CATMathStream                  CATMathStream
    CATMecModExtendItf             \ # CATMecModExtendItf             CATMecModExtendItf
    CATMecModLiveUseItf            \ # CATMecModLiveUseItf            CATMecModLiveUseItf
    CATMecModUseItf                \ # CATMecModUseItf                CATMecModUseItf
    CATMmFunctionalItfCPP          \ # CATMmFunctionalInterfaces      CATMmFunctionalItf
    PrtEnv                         \ # CATMmrVisualization            CATMmrVisu
    CATSurfacicInterfaces          \ # CATSurfacicInterfaces          CATSurfacicInterfaces
    CATSurfResMecMod               \ # CATSurfacicResources           CATSurfResMecMod
    CATSurfacicUtilities           \ # CATSurfacicResources           CATSurfacicResources
    CATSurfacicResourcesInit       \ # CATSurfacicResources           CATSurfacicResourcesInit
    CATTPSItf                      \ # CATTPSInterfaces               CATTPSItf
    MF0DimInt                      \ # ConstraintModeler              CATConstraintModeler
    CATGMModelInterfaces           \ # GMModelInterfaces              CATGMModelInterfaces
    CATGMOperatorsInterfaces       \ # GMOperatorsInterfaces          CATGMOperatorsInterfaces
    CATGitInterfaces               \ # GSMInterfaces                  CATGitInterfaces
    GN0GRAPH                       \ # GenericNaming                  CATGngGraph
    SI0REPER                       \ # GenericNaming                  CATGngReportUtilities
    CATGMGeometricInterfaces       \ # GeometricObjects               CATGMGeometricInterfaces
    YP00IMPL                       \ # GeometricObjects               CATGeometricObjects
    Y3DYNOPE                       \ # GeometricOperators             CATGeometricOperators
    CATGraph                       \ # Graph                          CATGraph
    ON0GRAPH                       \ # Graph                          CATGraph
    VE0GRPH2                       \ # Graph                          CATGraph
    CATInteractiveInterfaces       \ # InteractiveInterfaces          CATInteractiveInterfaces
    KnowledgeItf                   \ # KnowledgeInterfaces            KnowledgeItf
    KnowledgeItfCPP                \ # KnowledgeInterfaces            KnowledgeItf
    CK0FEAT                        \ # LiteralFeatures                CATLiteralFeatures
    CATMathematics                 \ # Mathematics                    CATMathematics
    YN000MFL                       \ # Mathematics                    CATMathematics
    CATMecModInterfaces            \ # MecModInterfaces               CATMecModInterfaces
    MecModItfCPP                   \ # MecModInterfaces               CATMecModInterfaces
    CATMecModLiveInterfaces        \ # MecModLiveInterfaces           CATMecModLiveInterfaces
    MechanicalBuildInfraItfCPP     \ # MechanicalBuildInfraItf        MechanicalBuildInfraItf
    MF0STARTUP                     \ # MechanicalModeler              CATMechanicalModeler
    CATMmrSpecToVisu               \ # MechanicalModeler              CATMmrSpecToVisu
    CATMmrVisProperties            \ # MechanicalModeler              CATMmrVisProperties
    CATMechanicalModelerUI         \ # MechanicalModelerUI            CATMechanicalModelerUI
    AD0XXBAS                       \ # ObjectModelerBase              CATObjectModelerBase
    ON0GREXT                       \ # ObjectModelerBase              CATObjectModelerBase
    CATObjectModelerNavigator      \ # ObjectModelerNavigator         CATObjectModelerNavigator
    ObjectModelerSystem            \ # ObjectModelerSystem            ObjectModelerSystem
    AC0SPBAS                       \ # ObjectSpecsModeler             CATObjectSpecsModeler
    PartFeatureBase                \ # PartFeatureBase                PartFeatureBase
    PartItf                        \ # PartInterfaces                 CATPartInterfaces
    CATProductStructureInterfaces  \ # ProductStructureInterfaces     CATProductStructureInterfaces
    SketcherItf                    \ # SketcherInterfaces             CATSketcherInterfaces
    JS0CATLM                       \ # System                         JS0GROUP
    JS0GROUP                       \ # System                         JS0GROUP
    CATSysTS                       \ # SystemTS                       CATSysTS
#

OS = COMMON
#if defined(CATIAR201)
LINK_WITH_V6_ONLY= \
    CATAfrComponentsModel          \ # AfrComponentsModel             CATAfrComponentsModel
    CATAfrFoundation               \ # AfrFoundation                  CATAfrFoundation
    CATAfrItf                      \ # AfrInterfaces                  CATAfrItf
    CATAfrNavigator                \ # AfrNavigator                   CATAfrNavigator
    CAT3DDimVisuIntf               \ # CAT3DDimVisuInterfaces         CAT3DDimVisuIntf
    CATTPSItfBase                  \ # CATTPSInterfacesBase           CATTPSItfBase
    CATV6V5ModelServices           \ # CATV6V5ModelServices           CATV6V5ModelServices
    CATConstraint2Interfaces       \ # Constraint2Interfaces          CATConstraint2Interfaces
    CATMmlSys                      \ # MechanicalModelerLive          CATMmlSys
    CATPLMModeler3DInterfaces      \ # PLMModelerBaseInterfaces       CATPLMModelerBaseInterfaces
    CATVisController               \ # VisualizationController        CATVisController
    CATVisFoundation               \ # VisualizationFoundation        CATVisFoundation
    SGInfra                        \ # VisualizationFoundation        SGInfra
    CATVisItf                      \ # VisualizationInterfaces        CATVisItf
#
#else
#if defined(CATIAV5R21)
LINK_WITH_V5_ONLY= \
    CATViz                         \ #                                
    CATVisualization               \ #                                
    CATApplicationFrame            \ #                                
    CATProductStructure1           \ #                                
#
#endif
#endif
