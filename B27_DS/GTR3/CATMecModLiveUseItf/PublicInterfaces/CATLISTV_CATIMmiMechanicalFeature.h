
#ifndef CATLISTV_CATIMmiMechanicalFeature_H
#define CATLISTV_CATIMmiMechanicalFeature_H

// COPYRIGHT DASSAULT SYSTEMES 2008

/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */

/**
 * @collection CATLISTV(CATIMmiMechanicalFeature_var)
 * Collection class for mechanical features.
 * All the methods of handlers collection classes are available.
 * Refer to the articles dealing with collections in the encyclopedia.
 */

#include "CATLISTHand_Clean.h"

#include "CATLISTHand_AllFunct.h"

#include "CATLISTHand_Declare.h"

#include "CATMecModLiveUseItf.h"
#undef  CATCOLLEC_ExportedBy
#define	CATCOLLEC_ExportedBy ExportedByCATMecModLiveUseItf

#include "CATIMmiMechanicalFeature.h"
CATLISTHand_DECLARE(CATIMmiMechanicalFeature_var)

#endif // CATLISTV_CATIMmiMechanicalFeature_H
