// COPYRIGHT DASSAULT SYSTEMES  2009
/** @CAA2Required */
//**********************************************************************
//* DON T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS *
//* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPEAR AT ANY TIME *
//**********************************************************************

#ifndef __CATMecModLiveUseItf_h__ 
#define __CATMecModLiveUseItf_h__
#ifdef _WINDOWS_SOURCE
#ifdef  __CATMecModLiveUseItf
#define ExportedByCATMecModLiveUseItf    __declspec(dllexport)
#else
#define ExportedByCATMecModLiveUseItf    __declspec(dllimport)
#endif
#else
#define ExportedByCATMecModLiveUseItf
#endif

#endif

