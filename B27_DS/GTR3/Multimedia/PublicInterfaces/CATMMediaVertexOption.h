#ifndef CATMMEDIAVERTEXOPTION_H
#define CATMMEDIAVERTEXOPTION_H
// COPYRIGHT DASSAULT SYSTEMES 1999

/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */

/*
* No Vertex option 
* @param CATPRINTVERTEX_ACTIVE
*   No vertices will be visible on output.
* @param CATPRINTVERTEX_INACTIVE
*   Vertices will be visible on output.
* @param CATPRINTVERTEX_DISABLE
*   No Vertices option is disabled and gives default behaviour.
*/
enum CATPrintVertexOption      {CATPRINTVERTEX_ACTIVE, CATPRINTVERTEX_INACTIVE,CATPRINTVERTEX_DISABLE};

#endif
