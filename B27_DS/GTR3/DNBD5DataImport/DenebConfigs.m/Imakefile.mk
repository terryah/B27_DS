# COPYRIGHT DASSAULT SYSTEMES 2004
#======================================================================
# Imakefile for module DenebConfigs.m
#======================================================================
#
#  Jul 2003  Creation:                                              awn
#  Dec 2003  Modified: support DocChooser                           mmh
#  Jan 2004  Modified: implement setting controller                 mmh
#======================================================================
#
# SHARED LIBRARY 
#
BUILT_OBJECT_TYPE=SHARED LIBRARY 
 
# DO NOT EDIT :: THE CAA2 WIZARDS WILL ADD CODE HERE
WIZARD_LINK_MODULES = JS0GROUP \
                      CATInfInterfaces \
                      DNBD5IPubIDL \
                      DNBD5IInterfacesUUID \
                      DNBD5IItfCPP 
# END WIZARD EDITION ZONE

LINK_WITH = $(WIZARD_LINK_MODULES) \
                        OM0EDPRO        \
                        DI0PANV2        \
                        JS0CORBA        \
                        JS0FM           \
                        AD0XXBAS        \
                        JS0FM           \
                        CATDlgStandard  \
                        Collections     \
                        CD0WIN          \
                        CATFileMenu     \


#
# Define the build options for the current module.
#
OS      = Windows_NT
BUILD   = YES

OS      = IRIX
BUILD   = YES

OS      = SunOS
BUILD   = YES

OS      = AIX
BUILD   = YES

OS      = HP-UX
BUILD   = YES

OS      = win_a
BUILD   = NO




