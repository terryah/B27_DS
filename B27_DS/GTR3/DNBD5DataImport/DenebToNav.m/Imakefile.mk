## COPYRIGHT DASSAULT SYSTEMES / DELMIA Corp 2003
##=============================================================================
##
## Imakefile.mk for DenebToNav.m
##    -> need to do some cleaning on compilation options
##=============================================================================
## Usage Notes :
##
##=============================================================================
##  Apr.  2003  Cleaning                                                    awn
##=============================================================================

BUILT_OBJECT_TYPE=SHARED LIBRARY

INCLUDED_MODULES=   DNBD5ExtComp  

LINK_WITH =   \
              d5                      \
              DNBD5DataImport         \
              DNBD5Device             \
              DenebConfigs            \
              CATVisualization        \
              CATObjectModelerBase    \
              CATMathematics          \
              AS0STARTUP              \
              SpaceAnalysisItf        \
              SimulationItf           \
              CATSimulationBase       \
              DI0PANV2                \
              CATObjectSpecsModeler   \
              JS0FM                   \
              JS0CORBA                \
              AD0XXBAS                \
              AC0SPBAS                \
              CD0WIN                  \       # for CATIPersistentCamera CATFrmLayout
              CATPrsRep               \
              CATViz                  \
              KnowledgeItf            \
              CATNavigatorItf         \       # for CATIPersistent3DMarker
              ArrUtility              \
              ArrIgpAttach            \
              CATArrangementItf       \
              DNBManufacturingLayoutItf \
              ArrUIInterfaces         \       # for CATIArrAttachmentFactory
              DNBD5IItfCPP            \
              DNBD5DataImpItf         \
              CATProcessInterfaces    \
              OE0MEASU                \		  # For CATMeasureCalculations	
              CATClnBase \
              CATClnSpecs
#              JS03TRA                 \
#              CD0FRAME                \
#              ProductStructureItf     \
#              JS0SCBAK                \
#              JS0STR                  \
#              NavigatorProIDL         \
#              NS0S1MSG                \
#              ON0PROP                 \
#              CO0LSTPV                \
#              CO0LSTST                \
#              DI0STATE                \
#              JS0STDLIB               \
#              JS0ERROR                \
#              NS0S3STR                \
#              VE0BASE                 \
#              VE0MDL                  \
#              VE0PIX                  \
#              YN000MAT                \
#              YN000M2D                \
#              YN000MFL                \
#              PRDCommands             \
#              SP0BBITF                \
#              SB0COMF                 \


#OS = IRIX
#BUILD = YES
#SYS_LIBS = -lmalloc

OS = HP-UX
BUILD = YES
LOCAL_CFLAGS = -DHP_700 -D_HPUX_SOURCE +Z -DWINDOWLESS -DSOFT_3D  -DD5I
LOCAL_CCFLAGS = -DHP_700 -D_HPUX_SOURCE +Z -DWINDOWLESS -DSOFT_3D -DD5I


OS = irix_a
BUILD = YES
LOCAL_CFLAGS =  -n32 -xgot -DIRIS -DIRIS_4D -DIRIX6 -DWINDOWLESS -DSOFT_3D -DD5I
LOCAL_CCFLAGS =  -n32 -xgot -DIRIS -DIRIS_4D -DIRIX6 -DWINDOWLESS -DSOFT_3D -DD5I

OS = irix_a64
BUILD = YES
LOCAL_CFLAGS =  -xgot -DIRIS -DIRIS_4D -DIRIX6 -DWINDOWLESS -DSOFT_3D -DD5I
LOCAL_CCFLAGS =  -xgot -DIRIS -DIRIS_4D -DIRIX6 -DWINDOWLESS -DSOFT_3D -DD5I

OS = aix_a
BUILD = YES
LOCAL_CFLAGS = -DIBM -DWINDOWLESS -DSOFT_3D -brtl -DD5I
LOCAL_CCFLAGS = -DIBM -DWINDOWLESS -DSOFT_3D -brtl -DD5I

OS = aix_a64
BUILD =YES 
LOCAL_CFLAGS = -DIBM -DWINDOWLESS -DSOFT_3D -brtl -DD5I
LOCAL_CCFLAGS = -DIBM -DWINDOWLESS -DSOFT_3D -brtl -DD5I

OS = intel_a
LOCAL_CFLAGS =  -MD -DWIN32  -DWINNT -DWINDOWLESS -DDOS -DAXXESS_DLL -DSOFT_3D -DD5I
LOCAL_CCFLAGS =  -MD -DWIN32  -DWINNT -DWINDOWLESS -DDOS -DAXXESS_DLL -DSOFT_3D -DD5I

OS = intel_c
BUILD = YES
LOCAL_CFLAGS= -Zi -MD -DWIN32 -DWINNT -DDOS -DWINDOWLESS -DAXXESS_DLL -DSOFT_3D -DD5I
LOCAL_CCFLAGS= -Zi -MD -DWIN32 -DWINNT -DDOS -DWINDOWLESS -DAXXESS_DLL -DSOFT_3D -DD5I

OS = intel_a64
LOCAL_CFLAGS =  -Ze -MD -DWIN32 -DWINNT -DWINDOWLESS -DDOS -DAXXESS_DLL -DSOFT_3D -DD5I
LOCAL_CCFLAGS =   -Ze -MD -DWIN32 -DWINNT -DWINDOWLESS -DDOS -DAXXESS_DLL -DSOFT_3D -DD5I

OS = win_b64
LOCAL_CFLAGS =  -Ze -MD -DWIN32 -DWINNT -DWINDOWLESS -DDOS -DAXXESS_DLL -DSOFT_3D -DD5I
LOCAL_CCFLAGS =   -Ze -MD -DWIN32 -DWINNT -DWINDOWLESS -DDOS -DAXXESS_DLL -DSOFT_3D -DD5I

OS =  SunOS
LOCAL_CFLAGS =  -DSUN -O -DWINDOWLESS -DDEVELOPMENT -DSOFT_3D -DD5I
LOCAL_CCFLAGS = -DSUN -O -DWINDOWLESS -DDEVELOPMENT -DSOFT_3D -DD5I
SYS_LIBS     = -lc -lm 

# WINDOWS 98
OS = win_a
BUILD = NO





