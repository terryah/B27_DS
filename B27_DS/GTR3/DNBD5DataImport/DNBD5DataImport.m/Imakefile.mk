#
#
# LOAD MODULE for DenebDataImportGeneral.m
#
BUILT_OBJECT_TYPE=SHARED LIBRARY

INCLUDED_MODULES=   DenebDataImportGeneral  \
                    DNBD5ICommands          \
#                    DNBD5Utils              \

LINK_WITH = JS0FM                       \
            JS0CORBA                    \
            JS0SCBAK                    \
            JS0STR                      \
            VE0BASE                     \
            CO0HTAB                     \
            YN000MFL                    \
            AD0XXBAS                    \
            AS0STNAV                    \
            ProductStructureItf         \
            AS0STARTUP                  \
            SpecsModeler                \
            CD0FRAME                    \
            DI0PANV2                    \
            CATDialogEngine             \
            DenebToNav                  \
            DenebConfigs                \
            CATSimulationBase           \
            SimulationItf               \
            CATViz                      \
            AC0XXLNK                    \
            DNBD5DataImpItf             \
            DNBD5ExtComp                \
            ProcessInterfaces           \     # for ProcessInterfaces
            CATFileMenu                 \
            CATCclInterfaces            \     
            PRDWorkshop                 \     # to remove in CXR13
            KnowledgeItf                \
            DNBD5IItfCPP                \
            CATInteractiveInterfaces    \

# WINDOWS 98
OS = win_a
BUILD=NO

