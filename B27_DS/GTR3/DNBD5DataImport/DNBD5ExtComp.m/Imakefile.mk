# COPYRIGHT DASSAULT SYSTEMES 2003\DELMIA Corp.
#==============================================================================
# Imakefile for module DNBD5ExtComp.m
#==============================================================================
#
# xxx xxxx  Creation                                                        xxx
# Jun 2003  Cleaning                                                        awn
#==============================================================================
#
# SHARED LIBRARY
#



#BUILT_OBJECT_TYPE=SHARED LIBRARY
BUILT_OBJECT_TYPE=NONE

LINK_WITH = \
            JS0FM                   \
            JS0CORBA                \
            JS0SCBAK                \
            JS0STR                  \
            AD0XXBAS                \
            AC0XXLNK                \
            AS0STARTUP              \
            AC0SPBAS                \
            Mathematics             \
            CATViz                  \
            CATVisualization        \
            DenebConfigs            \
            DenebToNav              \
            DenebDataImportGeneral  \
            DNBD5Device             \
            DNBD5IItfCPP            \
#           DNBD5ExtComp            \
#            AS0STNAV                \
#            VE0BASE                 \
#            CO0HTAB                 \
#            YN000MFL                \




#OS = IRIX
#BUILD = YES
#SYS_LIBS = -lmalloc

OS = HP-UX
BUILD = YES
LOCAL_CFLAGS =  -DHP_700 -DSOFT_3D -D_HPUX_SOURCE +Z -DWINDOWLESS -DD5I
LOCAL_CCFLAGS =  -DHP_700 -DSOFT_3D -D_HPUX_SOURCE +Z -DWINDOWLESS -DD5I


OS = irix_a
BUILD = YES
LOCAL_CFLAGS =  -n32 -xgot -DIRIS -DIRIS_4D -DIRIX6 -DWINDOWLESS -DSOFT_3D -DD5I
LOCAL_CCFLAGS =  -n32 -xgot -DIRIS -DIRIS_4D -DIRIX6 -DWINDOWLESS -DSOFT_3D -DD5I

OS = irix_a64
BUILD = YES
LOCAL_CFLAGS =  -xgot -DIRIS -DIRIS_4D -DIRIX6 -DWINDOWLESS -DSOFT_3D -DD5I
LOCAL_CCFLAGS =  -xgot -DIRIS -DIRIS_4D -DIRIX6 -DWINDOWLESS -DSOFT_3D -DD5I

OS = AIX
BUILD = YES
LOCAL_CFLAGS = -DIBM -DWINDOWLESS -brtl -DSOFT_3D -DD5I
LOCAL_CCFLAGS = -DIBM -DWINDOWLESS -brtl -DSOFT_3D -DD5I

OS = intel_a
BUILD = YES
LOCAL_CFLAGS =  -MD -DWIN32  -DWINNT -DWINDOWLESS -DDOS -DAXXESS_DLL -DSOFT_3D -DD5I
LOCAL_CCFLAGS =  -MD -DWIN32  -DWINNT -DWINDOWLESS -DDOS -DAXXESS_DLL -DSOFT_3D -DD5I

OS = intel_c
BUILD = YES
LOCAL_CFLAGS= -Zi -MD -DWIN32 -DWINNT -DDOS -DWINDOWLESS -DAXXESS_DLL -DSOFT_3D -DD5I
LOCAL_CCFLAGS= -Zi -MD -DWIN32 -DWINNT -DDOS -DWINDOWLESS -DAXXESS_DLL -DSOFT_3D -DD5I

OS = intel_a64
BUILD = YES
LOCAL_CFLAGS = -Ze -MD -DWIN32  -DWINNT -DWINDOWLESS -DDOS -DAXXESS_DLL -DSOFT_3D -DD5I
LOCAL_CCFLAGS = -Ze -MD -DWIN32  -DWINNT -DWINDOWLESS -DDOS -DAXXESS_DLL -DSOFT_3D -DD5I

OS = win_b64
BUILD = YES
LOCAL_CFLAGS = -Ze -MD -DWIN32  -DWINNT -DWINDOWLESS -DDOS -DAXXESS_DLL -DSOFT_3D -DD5I
LOCAL_CCFLAGS = -Ze -MD -DWIN32  -DWINNT -DWINDOWLESS -DDOS -DAXXESS_DLL -DSOFT_3D -DD5I


OS =  SunOS
BUILD = YES
LOCAL_CFLAGS = -Xa -DSUN -O -DWINDOWLESS -DDEVELOPMENT -DSOFT_3D -DD5I
LOCAL_CCFLAGS = -Xa -DSUN -O -DWINDOWLESS -DDEVELOPMENT -DSOFT_3D -DD5I
SYS_LIBS     = -lc -lm 

# WINDOWS 98
OS = win_a
BUILD = NO
