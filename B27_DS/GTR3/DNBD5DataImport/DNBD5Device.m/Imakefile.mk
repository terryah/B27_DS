# COPYRIGHT DASSAULT SYSTEMES 2003\DELMIA Corp.
#==============================================================================
# Imakefile for module DNBD5Device.m
#
# maybe a merge can be considered with DenebToNav
#==============================================================================
#
# xxx xxxx  Creation                                                        xxx
# Jun 2003  Cleaning                                                        awn
#==============================================================================
#
# SHARED LIBRARY
#
BUILT_OBJECT_TYPE=SHARED LIBRARY

#
OS = aix_a
BUILD = YES
LOCAL_CFLAGS = -DIBM -DWINDOWLESS -DSOFT_3D -DD5I

OS = aix_a64
BUILD =YES 
LOCAL_CFLAGS = -DIBM -DWINDOWLESS -DSOFT_3D -DD5I

OS = IRIX
BUILD = YES
LOCAL_CFLAGS = -n32 -xgot -DNDEBUG -DIRIS -DIRIS_4D -DIRIX6 -DWINDOWLESS -DSOFT_3D -DD5I
#SYS_LIBS = -lmalloc  -lm  -lmalloc  -lc 
SYS_LIBS = -lm  -lc 

OS = intel_a
BUILD = YES
LOCAL_CFLAGS=-G4 -Ze -MD -DWIN32 -DWINNT -DDOS -DWINDOWLESS -DSOFT_3D -DD5I

OS = intel_c
BUILD = YES
LOCAL_CFLAGS=-Zi -MD -DWIN32 -DWINNT -DDOS -DWINDOWLESS -DSOFT_3D -DD5I

OS = win_b64
BUILD = YES
LOCAL_CFLAGS= -Ze -MD -DWIN32 -DNDEBUG -DWINNT -DDOS -DWINDOWLESS -DSOFT_3D -DD5I


OS = HP-UX
BUILD = YES
LOCAL_CFLAGS = -DWINDOWLESS -DHP_700 -DSOFT_3D -D_HPUX_SOURCE +Z -DD5I

OS = SunOS
BUILD =YES
LOCAL_CFLAGS = -Xa -DSUN -DWINDOWLESS -DSOFT_3D -DD5I
SYS_LIBS =  -lsocket -lnsl




OS = alpha_a
BUILD = NO

# WINDOWS 98
OS = win_a
BUILD = NO

OS = COMMON

LINK_WITH =  DenebToNav JS0CORBA 

