/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAExpertRuleBaseComponentRuntimes_h
#define CATIAExpertRuleBaseComponentRuntimes_h

#ifndef ExportedByGenerativeKnowledgePubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __GenerativeKnowledgePubIDL
#define ExportedByGenerativeKnowledgePubIDL __declspec(dllexport)
#else
#define ExportedByGenerativeKnowledgePubIDL __declspec(dllimport)
#endif
#else
#define ExportedByGenerativeKnowledgePubIDL
#endif
#endif

#include "CATIACollection.h"
#include "CATVariant.h"

class CATIAExpertRuleBaseComponentRuntime;

extern ExportedByGenerativeKnowledgePubIDL IID IID_CATIAExpertRuleBaseComponentRuntimes;

class ExportedByGenerativeKnowledgePubIDL CATIAExpertRuleBaseComponentRuntimes : public CATIACollection
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall Item(const CATVariant & iIndex, CATIAExpertRuleBaseComponentRuntime *& oExpertRuleBaseComponentRuntime)=0;

    virtual HRESULT __stdcall ShallowItem(const CATVariant & iIndex, CATIAExpertRuleBaseComponentRuntime *& oExpertRuleBaseComponentRuntime)=0;

    virtual HRESULT __stdcall ShallowCount(CATLONG & oNbItems)=0;

    virtual HRESULT __stdcall Remove(const CATVariant & iIndex)=0;

    virtual HRESULT __stdcall ShallowRemove(const CATVariant & iIndex)=0;


};

CATDeclareHandler(CATIAExpertRuleBaseComponentRuntimes, CATIACollection);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
