/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAExpertCheck_h
#define CATIAExpertCheck_h

#ifndef ExportedByGenerativeKnowledgePubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __GenerativeKnowledgePubIDL
#define ExportedByGenerativeKnowledgePubIDL __declspec(dllexport)
#else
#define ExportedByGenerativeKnowledgePubIDL __declspec(dllimport)
#endif
#else
#define ExportedByGenerativeKnowledgePubIDL
#endif
#endif

#include "CATBSTR.h"
#include "CATIAExpertCheckRuntime.h"

extern ExportedByGenerativeKnowledgePubIDL IID IID_CATIAExpertCheck;

class ExportedByGenerativeKnowledgePubIDL CATIAExpertCheck : public CATIAExpertCheckRuntime
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_Variables(CATBSTR & oVariablesBSTR)=0;

    virtual HRESULT __stdcall put_Variables(const CATBSTR & iVariablesBSTR)=0;

    virtual HRESULT __stdcall get_Body(CATBSTR & oBodyBSTR)=0;

    virtual HRESULT __stdcall put_Body(const CATBSTR & iBodyBSTR)=0;

    virtual HRESULT __stdcall get_Language(CATLONG & oLanguage)=0;

    virtual HRESULT __stdcall put_Language(CATLONG iLanguage)=0;


};

CATDeclareHandler(CATIAExpertCheck, CATIAExpertCheckRuntime);

#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIABase.h"
#include "CATIACollection.h"
#include "CATIAExpertReportObject.h"
#include "CATIAExpertReportObjects.h"
#include "CATIAExpertRuleBaseComponentRuntime.h"
#include "CATSafeArray.h"
#include "CATVariant.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
