#ifdef LOCAL_DEFINITION_FOR_IID
LINK_WITH_FOR_IID = \
GenKnowledgeInterfacesUUID
#else
LINK_WITH_FOR_IID =
#endif
# COPYRIGHT DASSAULT SYSTEMES 1998
#======================================================================
# Imakefile for module GenerativeKnowledgeItf.m 
#======================================================================
#
#  Nov 1999  Creation: Code generated by the CAA wizard  IJE
#======================================================================
#
# SHARED LIBRARY 
#
BUILT_OBJECT_TYPE=SHARED LIBRARY
 
# DO NOT EDIT :: THE CAA2 WIZARDS WILL ADD CODE HERE
WIZARD_LINK_MODULES =  \
  JS0GROUP

# END WIZARD EDITION ZONE

LINK_WITH=$(LINK_WITH_FOR_IID)  $(WIZARD_LINK_MODULES)  KnowledgeItf
#InfItf
INCLUDED_MODULES = \
                  GenerativeKnowledgePubIDL \
									GenKnowledgeItfCPP \
									GenKnowledgeProIDL
