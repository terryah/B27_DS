#include "CATIACGMLevel.h"
#ifndef PrimitivesLight_h
#define PrimitivesLight_h

// COPYRIGHT DASSAULT SYSTEMES 2010

/** @CAA2Required */
/*---------------------------------------------------------------------*/
/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */
/*---------------------------------------------------------------------*/
#ifdef CATIACGMR420CAA
#ifdef _STATIC_SOURCE
#define	ExportedByPrimitivesLight
#elif defined(__PrimitivesLight)
#define ExportedByPrimitivesLight DSYExport
#else
#define ExportedByPrimitivesLight DSYImport
#endif
#include "DSYExport.h"
#else
#ifdef _STATIC_SOURCE
#define	ExportedByPrimitivesLight
#elif defined _WINDOWS_SOURCE
#ifdef	__PrimitivesLight
#define	ExportedByPrimitivesLight	__declspec(dllexport)
#else
#define	ExportedByPrimitivesLight	__declspec(dllimport)
#endif
#else
#define	ExportedByPrimitivesLight
#endif
#endif
//#include "TopologicalOperatorsLightCommonDec.h"
#endif

