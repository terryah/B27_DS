#include "CATIACGMLevel.h"
#ifdef CATIACGMR420CAA
#ifdef _STATIC_SOURCE
#define	ExportedByAdvTopoSketchLight
#elif defined __AdvTopoSketchLight


// COPYRIGHT DASSAULT SYSTEMES 2010

/** @CAA2Required */

/*---------------------------------------------------------------------*/

/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */

/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */

/*---------------------------------------------------------------------*/
#define ExportedByAdvTopoSketchLight DSYExport
#else
#define ExportedByAdvTopoSketchLight DSYImport
#endif
#include "DSYExport.h"
#else
#ifdef _STATIC_SOURCE
#define	ExportedByAdvTopoSketchLight
#elif defined _WINDOWS_SOURCE
// COPYRIGHT DASSAULT SYSTEMES 2010

/** @CAA2Required */
/*---------------------------------------------------------------------*/
/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */
/*---------------------------------------------------------------------*/
#ifdef	__AdvTopoSketchLight
#define	ExportedByAdvTopoSketchLight	__declspec(dllexport)
#else
#define	ExportedByAdvTopoSketchLight	__declspec(dllimport)
#endif
#else
#define	ExportedByAdvTopoSketchLight
#endif
#endif
//#include <TopologicalOperatorsLightCommonDec.h>
