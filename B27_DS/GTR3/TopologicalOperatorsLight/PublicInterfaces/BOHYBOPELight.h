#include "CATIACGMLevel.h"
#ifdef CATIACGMR420CAA
#ifdef _STATIC_SOURCE
#define	ExportedByBOHYBOPELight
#elif defined __BOHYBOPELight


// COPYRIGHT DASSAULT SYSTEMES 2010

/** @CAA2Required */

/*---------------------------------------------------------------------*/

/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */

/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */

/*---------------------------------------------------------------------*/
#define ExportedByBOHYBOPELight DSYExport
#else
#define ExportedByBOHYBOPELight DSYImport
#endif
#include "DSYExport.h"
#else
#ifdef _STATIC_SOURCE
#define	ExportedByBOHYBOPELight
#elif defined _WINDOWS_SOURCE

// COPYRIGHT DASSAULT SYSTEMES 2010

/** @CAA2Required */
/*---------------------------------------------------------------------*/
/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */
/*---------------------------------------------------------------------*/
#ifdef	__BOHYBOPELight
#define	ExportedByBOHYBOPELight	__declspec(dllexport)
#else
#define	ExportedByBOHYBOPELight	__declspec(dllimport)
#endif
#else
#define	ExportedByBOHYBOPELight
#endif
#endif
//#include "TopologicalOperatorsLightCommonDec.h"
