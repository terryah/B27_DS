#include "CATIACGMLevel.h"
//------------------------------------------------------------------------------
#ifdef CATIACGMR420CAA
#ifdef _STATIC_SOURCE
#define	ExportedByAnalysisToolsLight
#elif defined __AnalysisToolsLight


// COPYRIGHT DASSAULT SYSTEMES 2010

/** @CAA2Required */

/*---------------------------------------------------------------------*/

/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */

/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */

/*---------------------------------------------------------------------*/
#define ExportedByAnalysisToolsLight DSYExport
#else
#define ExportedByAnalysisToolsLight DSYImport
#endif
#include "DSYExport.h"
#else
#ifdef _STATIC_SOURCE
#define	ExportedByAnalysisToolsLight
#elif defined _WINDOWS_SOURCE
// COPYRIGHT DASSAULT SYSTEMES 2010

/** @CAA2Required */
/*---------------------------------------------------------------------*/
/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */
/*---------------------------------------------------------------------*/

#ifdef __AnalysisToolsLight
#define ExportedByAnalysisToolsLight  __declspec(dllexport) 
#else
#define ExportedByAnalysisToolsLight  __declspec(dllimport) 
#endif
#else
#define ExportedByAnalysisToolsLight
#endif
#endif
//#include "TopologicalOperatorsLightCommonDec.h"
//------------------------------------------------------------------------------
