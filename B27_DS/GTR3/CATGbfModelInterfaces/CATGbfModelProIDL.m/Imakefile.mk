# COPYRIGHT DASSAULT SYSTEMES 2000
#======================================================================
# Imakefile for module CATGbfModelProIDL
# Module for compilation of the protected IDL interfaces
#======================================================================
#
#  Nov 2000  Creation: Code generated by the CAA wizard  JDT
#======================================================================
#
# NO BUILD             
#

BUILT_OBJECT_TYPE=NONE

SOURCES_PATH=ProtectedInterfaces
COMPILATION_IDL=YES

