# COPYRIGHT Dassault Systemes 2012
#======================================================================
# Imakefile for module CATCAAABFAccess.m
#======================================================================
#
#  Mar 2012  Creation: Code generated by the CAA wizard  BS5
#======================================================================
#
# SHARED LIBRARY 
#
BUILT_OBJECT_TYPE=SHARED LIBRARY 
 
# DO NOT EDIT :: THE CAA2 WIZARDS WILL ADD CODE HERE
WIZARD_LINK_MODULES = JS0GROUP
# END WIZARD EDITION ZONE

LINK_WITH = $(WIZARD_LINK_MODULES) \
			CATGbfModelItf \
			AS0STARTUP \
			AD0XXBAS \
			AC0XXLNK \
			JS0CORBA \

# System dependant variables
#
OS = AIX
#
OS = HP-UX
#
OS = IRIX
#
OS = SunOS
#
OS = Windows_NT
