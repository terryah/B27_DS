//===================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2014 - 2017
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//===================================================================
// CATIABFImport.h
// Define the CATIABFImport interface
//===================================================================
//
// Usage notes: 
//
//===================================================================
//  2017/07/14 Creation: Code generated by the 3DS wizard
//===================================================================
#ifndef CATIABFImport_H
#define CATIABFImport_H

#include "CATBaseUnknown.h"
class CATUnicodeString;

#ifdef  _WINDOWS_SOURCE
#ifdef  __CATGbfModelItfCPP
#define ExportedByCATGbfModelItfCAA     __declspec(dllexport)
#else
#define ExportedByCATGbfModelItfCAA     __declspec(dllimport)
#endif
#else
#define ExportedByCATGbfModelItfCAA
#endif

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByCATGbfModelItfCAA IID IID_CATIABFImport;
#else
extern "C" const IID IID_CATIABFImport ;
#endif

//------------------------------------------------------------------

/**
* Interface to import fastening data from a neutral format file (.txt file) under a refrence product.
*
* <br><b>Role</b>: Import ABF data, from ABF neutral file, under a reference product.</br>
*
* <ul>
* <li>Queryinterface on product reference to create a pointer to this interface. </li>
* <li> Release this interface when it is no longer needed. </li> 
* </ul>
*/


class ExportedByCATGbfModelItfCAA CATIABFImport: public CATBaseUnknown
{
  CATDeclareInterface;
  public:

   /**
   * Method imports fastening data from a neutral file(.txt) under the product on which it is called.
   * Also it generates log file containing warning(s) in the import operation.
	 *   @param iImportFilePath[in]
   *      The import neutral file pathname.
   *   @param iLOGFilePath[in]
   *      The ABF LOG file pathname.
   *   @return
   *    <dl>
   *    <dt>S_OK</dt>
   *    <dd>if import operation is successful.</dd>
   *    </dl>
   */
    virtual HRESULT Import (const CATUnicodeString & iImportFilePath, 
	                         const CATUnicodeString & iLOGFilePath) =0;

};

//------------------------------------------------------------------

#endif
