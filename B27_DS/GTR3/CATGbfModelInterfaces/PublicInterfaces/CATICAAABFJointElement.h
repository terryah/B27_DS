// COPYRIGHT Dassault Systemes 2012
//===================================================================
//
// CATICAAABFJointElement.h
// Define the CATICAAABFJointElement interface
//
//===================================================================
//
// Usage notes:
//   New interface: describe its use here
//
//===================================================================
//
//  Feb 2012  Creation: Code generated by the CAA wizard  bs5
//===================================================================
#ifndef CATICAAABFJointElement_H
#define CATICAAABFJointElement_H

/**
* @CAA2Level L0
* @CAA2Usage U3
*/

#include "CATBaseUnknown.h"
#include "CATUnicodeString.h"
#include "CATBoolean.h"
#include "CATMathPoint.h"
#include "CATISpecObject.h"
#include "CATICkeParm.h"
#include "CATListPtrCATIProduct.h"

class CATICAAABFJointBody;
class CATMathDirection;
class CATIProduct;

#ifdef  _WINDOWS_SOURCE
#ifdef  __CATGbfModelItfCPP
#define ExportedByCATGbfModelItfCAA     __declspec(dllexport)
#else
#define ExportedByCATGbfModelItfCAA     __declspec(dllimport)
#endif
#else
#define ExportedByCATGbfModelItfCAA
#endif


#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByCATGbfModelItfCAA IID IID_CATICAAABFJointElement;
#else
extern "C" const IID IID_CATICAAABFJointElement ;
#endif


/**	
* Interface representing ABF Joint Element.
* <b>Role:</b> this interface concerns joint elements.
*
*/
class ExportedByCATGbfModelItfCAA CATICAAABFJointElement: public CATBaseUnknown
{
  CATDeclareInterface;

  public:

	/**
	*       
	*   GetJointElementName returns the Joint Element Name.
	*   @param strJointElementName
	*      The joint element name.
	* 
	*/
	virtual HRESULT GetJointElementName (CATUnicodeString &ostrJointElementName) = 0 ;

	/**
	*       
	*   GetFatherJointBody returns the parent Joint Body interface.
	*   @param opJointBody
	*      Parent joint body interface pointer of the joint element.
	* 
	*/
	virtual HRESULT GetFatherJointBody(CATICAAABFJointBody **opJointBody) = 0 ;
	
	/**
	*       
	*   GetDiameter returns the diameter of the Joint Element.
	*   @param oDiameter
	*      Diameter of the joint element as double.
	* 
	*/	
	virtual HRESULT GetDiameter(double * oDiameter) = 0 ;

	/**
	*       
	*   GetXYZLocation returns the X, Y, Z location coordinantes of Joint Element.
	*   @param oX
	*      X axis location coordinate
	*   @param oY
	*      Y axis location coordinate
	*   @param oZ
	*      Z axis location coordinate
  *   @icMasterProduct
	*      Root Product reference
	*   @icInstanceProduct
  *      Currently this argument is not supported
	*/
	//15 Oct 2012
	virtual HRESULT GetXYZLocation(double *oX, double *oY, double *oZ, const IUnknown * const icMasterProduct = NULL, const IUnknown * const icInstanceProduct = NULL) = 0 ;
	//15 Oct 2012

	/**
	*       
	*   GetIFlag returns the I Flag of the Joint Element.
	*   @param oIFlag
	*      Boolean value of the I Flag.
	* 
	*/
	virtual HRESULT GetIFlag(CATBoolean * oIFlag) = 0 ;

	/**
	*       
	*   GetKFlag returns the K Flag of the Joint Element.
	*   @param oKFlag
	*      Boolean value of the K Flag.
	* 
	*/
	virtual HRESULT GetKFlag(CATBoolean * oKFlag) = 0 ;

	/**
	*       
	*   GetUVector returns the UVector direction of the Joint Element.
	*   @param oTangentVector
	*      U Vetor of the Joint Element.
	* 
	*/
	virtual HRESULT GetUVector(CATMathDirection ** oTangentVector) = 0 ;

	/**
	*       
	*   GetVVector returns the VVector direction of the Joint Element.
	*   @param oDirection
	*      V Vetor of the Joint Element.
	* 
	*/
	virtual HRESULT GetVVector(CATMathDirection ** oDirection) = 0 ;

	/**
	*       
	*   GetWVector returns the WVector direction of the Joint Element.
	*   @param oNormal
	*      W Vetor of the Joint Element.
	* 
	*/
	virtual HRESULT GetWVector(CATMathDirection ** oNormal) = 0 ;

	/**
	*       
	*   GetProcessType returns process type of the Joint element.
	*   @param oProcessType
	*      Process type of the Joint Element.
	* 
	*/
	virtual HRESULT GetProcessType(CATUnicodeString * oProcessType) = 0 ;

	/**
	*       
	*   GetProcessCategory returns the process category of the Joint element.
	*   @param oProcessCategory
	*      Process category of the joint element.
	* 
	*/
	virtual HRESULT GetProcessCategory (CATUnicodeString * oProcessCategory) = 0 ;

	/**
	*       
	*   GetFastenerType returns the fastener type.
	*   @param ostrFastenerType
	*      Type of the fastener
	*	   <b>Legal values</b>: SpotPoint, SpotProjection, Bead
	* 
	*/
	virtual HRESULT GetFastenerType(CATUnicodeString& ostrFastenerType) = 0 ;

	/**
	*       
	*   IsFastenerOnSupport returns if the fastener is on support or not.
	*   @param oFastenerOnSupport
	*      Returns wether fasterner is on suppor or not.
	*		<b>Legal values</b>: TRUE - Fastener is on support. False - Fastener is not on support.
	* 
	*/
	virtual HRESULT IsFastenerOnSupport(CATBoolean &oFastenerOnSupport) = 0 ;

	/**
	*       
	*   CheckFastenerThicknessCrossing checks the fastener crossing thickness.
	*   @param oChkFastenerThicknessCrossing
	*      Returns status of the fastener crossing thickness.
	*		<b>Legal values</b>: TRUE - Fastener crossing thickness is yes. False - Fastener crossing thickness is no.
	*   @param oZonesList
	*     List of zone crossed by the fastener.
	*   @param oJoinedCompList
	*      List of component list by crossed by fastener.
	* 
	*/
	virtual HRESULT CheckFastenerThicknessCrossing(CATBoolean &oChkFastenerThicknessCrossing, CATListOfCATUnicodeString **oZonesList, CATLISTP(CATIProduct) **oJoinedCompList) = 0;

	/**
	*       
	*   GetCurveBeadLength returns curve bead length.
	*   @param oLength
	*      Curve bead length.
	* 
	*		<b>NOTE</b>: Succeeds only when the fastener is curve(bead) type
	*/
	virtual HRESULT GetCurveBeadLength(double * oLength) = 0;

	/**
	*       
	*   GetStartPointOnCurveBead returns the start point of curve bead.
	*   @param oStartPoint
	*      The start point of the curve bead fastener.
	* 
	*		<b>NOTE</b>: Succeeds only when the fastener is curve(bead) type
	*/
	virtual HRESULT GetStartPointOnCurveBead(CATMathPoint * oStartPoint) = 0;

	/**
	*       
	*   GetEndPointOnCurveBead returns the end point of curve bead.
	*   @param oEndPoint
	*      The end point of the curve bead fastener.
	* 
	*		<b>NOTE</b>: Succeeds only when the fastener is curve(bead) type
	*/
	virtual HRESULT GetEndPointOnCurveBead(CATMathPoint * oEndPoint) = 0;

	/**
	*       
	*   GetLocationMethod returns the type of the location method of the Joint element.
	*   @param oLocationMethod
	*      Location method type of the joint element.
	* 
	*/
	virtual HRESULT GetLocationMethod(CATString * oLocationMethod) = 0;

	/**
	*       
	*   GetCurve returns the support curve of the joint element.
	*   @param oSupport
	*      Support curve as a CATISpecObject.
	* 
	*		<b>NOTE</b>: Succeeds only when the fastener Location method is of type - "AlongCurve" & "FromCurveOnSurface"
	*/
	virtual HRESULT GetCurve(CATISpecObject_var& oSupport) = 0;

	/**
	*       
	*   GetSurface returns the support surface of the joint element.
	*   @param oSurface
	*      Support surface as a CATISpecObject.
	* 
	*		<b>NOTE</b>: Succeeds only when the fastener Location method is of type - "OnSurface" (Spot case only as this not for Bead case) & "FromCurveOnSurface"
	*/
	virtual HRESULT GetSurface(CATISpecObject_var& oSurface) = 0;

	/**
	*       
	*   GetOffset returns the offset of the joint element.
	*   @param oOffset
	*      Offset of the join element.
	* 
	*		<b>NOTE</b>: Succeeds only when the fastener Location method is of type - "FromCurveOnSurface"
	*/
	virtual HRESULT GetOffset(CATICkeParm_var     *oOffset) = 0;

	/**
	*       
	*   GetDistanceFromReference returns the distance of joint element from reference point.
	*   @param oDistance
	*      Distance of the joint elment from the reference point.
	* 
	*		<b>NOTE</b>: Succeeds only when the fastener is Spot type and Location method is of type - "OnSurface", "FromCurveOnSurface" & "AlongCurve"
	*/
	virtual HRESULT GetDistanceFromReference(CATICkeParm_var	&oDistance) = 0;
  // No constructors or destructors on this pure virtual base class
  // --------------------------------------------------------------
};

//------------------------------------------------------------------

#endif
