#
# Copyright Dassault Systemes Provence 2003, all rights reserved
#
#==============================================================================
# Imakefile for the bigger module CATSdoGeo.m
#==============================================================================
#
# 06/02/2003 : MMO : Portage 64bits
# 04/02/2003 : XXX : Optimisation O2 sur NT
# 10/03/2003 : MMO : Creation
#              
#==============================================================================
#

BUILT_OBJECT_TYPE=NONE

INCLUDED_MODULES = CATSdoObjects

# LINK_WITH=

#==============================================================================

OS = AIX
SYS_LIBS = -lxlf -lxlf90 -lxlfpad

OS = IRIX
SYS_LIBS = -lftn
#
OS = Windows_NT
#if os win_b64
#else
OPTIMIZATION_CPP = /O2
#endif
#
OS = HP-UX
#if os hpux_a
SYS_LIBS = -lf
#else
SYS_LIBS= -lF90
#endif                

OS = hpux_b64 
#if os hpux_a
SYS_LIBS = -lf
#else
SYS_LIBS= -lF90 -lcps 
#endif

OS = SunOS
SYS_LIBS = -lF77 -lM77
