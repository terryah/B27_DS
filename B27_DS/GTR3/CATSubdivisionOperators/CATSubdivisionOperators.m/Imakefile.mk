#
# Copyright Dassault Systemes Provence 2003, all rights reserved
#
#==============================================================================
# Imakefile for the bigger module CATSubdivisionOperators.m
#==============================================================================
#
# 06/02/2003 : MMO : Portage 64bits
# 04/02/2003 : XXX : Optimisation O2 sur NT
# 31/12/2003 : ANR : Ajout de CATAdvancedMathematics ( pour les matrix)
# 23/07/2003 : MMO : On supprime CATTopologicalOperators CATAdvancedTopologicalOpe
# 12/06/2003 : JCV : 3 Modules CATSdoTopo CATSdoGeo CATSdoObjects
# 27/03/2003 : JCV : CATGeometricObjects + CATMathStream CATCGMGeoMath (warning)
# 10/03/2003 : MMO : Creation
#
#==============================================================================
#

BUILT_OBJECT_TYPE=SHARED LIBRARY 

INCLUDED_MODULES = CATSdoTopo CATSdoGeo CATSdoObjects SubdivisionOpeItf

LINK_WITH = JS0GROUP                 \
            CATMathematics           \
            CATGeometricObjects      \ 
            CATMathStream            \
            CATCGMGeoMath            \
            CATTopologicalObjects    \
			CATAdvancedMathematics   \
            CATGMModelInterfaces     \
            CATGMAdvancedOperatorsInterfaces

#ifdef CATIAV5R20
ALIASES_ON_IMPORT=CATSubdivisionOperators CATGMModelInterfaces CATGMOperatorsInterfaces CATGMAdvancedOperatorsInterfaces
#endif

#==============================================================================

OS = AIX
SYS_LIBS = -lxlf -lxlf90 -lxlfpad

OS = IRIX
SYS_LIBS = -lftn
#
OS = Windows_NT
#if os win_b64
#else
OPTIMIZATION_CPP = /O2
#endif
#
OS = HP-UX
#if os hpux_a
SYS_LIBS = -lf
#else
SYS_LIBS= -lF90
#endif                

OS = hpux_b64 
#if os hpux_a
SYS_LIBS = -lf
#else
SYS_LIBS= -lF90 -lcps 
#endif

OS = SunOS
SYS_LIBS = -lF77 -lM77
