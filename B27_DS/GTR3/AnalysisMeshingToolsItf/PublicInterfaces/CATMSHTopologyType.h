#ifndef  CATMSHTopologyType_h
#define  CATMSHTopologyType_h

// COPYRIGHT Dassault Systemes 2003

/**
 * @CAA2Level L0 
 * @CAA2Usage U1
 */

class CATMSHTopVertex;
class CATMSHTopEdge;
class CATMSHTopContour;
class CATMSHTopDomain;

typedef  CATMSHTopVertex          TopVertex;
typedef  CATMSHTopEdge            TopEdge;
typedef  CATMSHTopContour         TopContour;
typedef  CATMSHTopDomain          TopDomain;

#endif
