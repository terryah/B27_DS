
#ifndef __FeatureModelerExt_h__
#define __FeatureModelerExt_h__


// COPYRIGHT DASSAULT SYSTEMES 2010

/** @CAA2Required */
//**********************************************************************
//* DON T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS *
//* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPEAR AT ANY TIME *
//**********************************************************************


#ifdef  _WINDOWS_SOURCE
#ifdef  __FeatureModelerExt
#define ExportedByFeatureModelerExt __declspec(dllexport)
#else
#define ExportedByFeatureModelerExt __declspec(dllimport)
#endif
#else
#define ExportedByFeatureModelerExt
#endif


#endif // __FeatureModelerExt_h__
