
#ifndef __CATFmFeatureModelerID_h__
#define __CATFmFeatureModelerID_h__


// COPYRIGHT DASSAULT SYSTEMES 2010

/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */


// FeatureModelerExt forwards and headers.
class CATFmInfrastructureID;
#include "FeatureModelerExt.h" // ExportedByFeatureModelerExt


/**
 * Infrastructure ID for FeatureModeler.
 */
extern ExportedByFeatureModelerExt const CATFmInfrastructureID CATFmFeatureModelerID;


#endif // __CATFmFeatureModelerID_h__
