#ifndef DNBIEnterZoneActivityFactory_H
#define DNBIEnterZoneActivityFactory_H

// COPYRIGHT Dassault Systemes 2003

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

#include "DNBWSQItfCPP.h"
#include "CATBaseUnknown.h"

class DNBIEnterZoneActivity;
class CATISPPChildManagement;

extern ExportedByDNBWSQItfCPP IID IID_DNBIEnterZoneActivityFactory;

/**
 * Interface to create Enter Zone Activity.
 * <b>Role:</b>
 * This interface provides method to create a Enter Zone activity.
 */
class ExportedByDNBWSQItfCPP DNBIEnterZoneActivityFactory: public CATBaseUnknown
{
   /**
    * @nodoc
    */
   CATDeclareInterface;

  public:

      /**
       * Creates a Enter Zone activity as child of the given activity.
       * @param ispFatherActivity
       *   The parent activity.
       * @param opIEnterZoneActivity
       *   Newly created Enter Zone activity.
       * @return
       *   An HRESULT.
       *   <br><b>Legal values</b>:
       *   <dl>
       *     <dt>S_OK</dt>
       *     <dd>The component is successfully created
       *         and the interface pointer is successfully returned</dd>
       *     <dt>E_FAIL </dt>
       *     <dd>The component was successfully created,
       *         but the interface query failed</dd>
       *     <dt>E_NOINTERFACE </dt>
       *     <dd>The component was successfully created,
       *         but the it doesn't implement the requested interface</dd>
       *     <dt>E_OUTOFMEMORY </dt>
       *     <dd><dd>The component allocation failed</dd>
       *   </dl>
       */
      virtual HRESULT CreateChildEnterZoneActivity(CATISPPChildManagement *ispFatherActivity, 
                                                   DNBIEnterZoneActivity** opIEnterZoneActivity ) =0 ;

};

/**
 * @nodoc
 */
CATDeclareHandler( DNBIEnterZoneActivityFactory, CATBaseUnknown );
#endif
