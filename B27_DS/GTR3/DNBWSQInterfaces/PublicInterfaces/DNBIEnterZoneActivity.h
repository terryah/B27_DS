#ifndef DNBIEnterZoneActivity_H
#define DNBIEnterZoneActivity_H

// COPYRIGHT Dassault Systemes 2003

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

#include "DNBWSQItfCPP.h"
#include "CATBaseUnknown.h"

class DNBIIZone;

extern ExportedByDNBWSQItfCPP IID IID_DNBIEnterZoneActivity;


/**
 * Interface to manage Enter Zone Activity.
 * <b>Role:</b>
 * This interface provides methods to manage Enter Zone activities.
 */
class ExportedByDNBWSQItfCPP DNBIEnterZoneActivity: public CATBaseUnknown
{
   /**
    * @nodoc
    */
   CATDeclareInterface;

  public:


      /**
       * Defines the Interference Zone that owns the activity.
       * @param ipIZone
       *   The Interference Zone.
       * @return
       *   An HRESULT.
       *   <br><b>Legal values</b>:
       *   <dl>
       *     <dt>S_OK</dt>
       *     <dd>The zone was corrrectly set</dd>
       *     <dt>E_FAIL </dt>
       *     <dd>The zone was not corrrectly set</dd>
       *   </dl>
       */
      virtual HRESULT SetOwnerZone( DNBIIZone* ipIZone ) = 0;

      /**
       * Retrieves the Interference Zone that owns the activity.
       * @param opIZone
       *   The Interference Zone.
       * @return
       *   An HRESULT.
       *   <br><b>Legal values</b>:
       *   <dl>
       *     <dt>S_OK</dt>
       *     <dd>The component is successfully created
       *         and the interface pointer is successfully returned</dd>
       *     <dt>E_FAIL </dt>
       *     <dd>The component was successfully created,
       *         but the interface query failed</dd>
       *     <dt>E_NOINTERFACE </dt>
       *     <dd>The component was successfully created,
       *         but the it doesn't implement the requested interface</dd>
       *     <dt>E_OUTOFMEMORY </dt>
       *     <dd><dd>The component allocation failed</dd>
       *   </dl>
       */
      virtual HRESULT GetOwnerZone( DNBIIZone** opIZone ) = 0;

      /**
       * Removes the activity.
       * @param iForCloseContext
       *   Not Used.
       * @return
       *   An HRESULT.
       *   <br><b>Legal values</b>:
       *   <dl>
       *     <dt>S_OK</dt>
       *     <dd>The activity was corrrectly removed</dd>
       *     <dt>E_FAIL </dt>
       *     <dd>The activity was not corrrectly removed</dd>
       *   </dl>
       */
      virtual HRESULT Remove(int iForCloseContext) = 0;

};

/**
 * @nodoc
 */
CATDeclareHandler( DNBIEnterZoneActivity, CATBaseUnknown );
#endif
