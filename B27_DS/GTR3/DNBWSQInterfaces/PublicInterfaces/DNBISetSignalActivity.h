#ifndef DNBISetSignalActivity_H
#define DNBISetSignalActivity_H

// COPYRIGHT Dassault Systemes 2003

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

#include "DNBWSQItfCPP.h"

#include "CATBaseUnknown.h"
#include "CATVariant.h"

class DNBIIOItem;
class CATUnicodeString;

extern ExportedByDNBWSQItfCPP IID IID_DNBISetSignalActivity ;

/**
 * Interface to manage Set Signal Activity.
 * <b>Role:</b>
 * This interface provides methods to define/modify Set Signal Activity.
 */
class ExportedByDNBWSQItfCPP DNBISetSignalActivity: public CATBaseUnknown
{
   /**
    * @nodoc
    */
   CATDeclareInterface;

  public:

      /**
       * Defines the IO Item for the activity.
       * @param ipIIOItem
       *   The IO Item.
       * @return
       *   An HRESULT.
       *   <br><b>Legal values</b>:
       *   <dl>
       *     <dt>S_OK</dt>
       *     <dd>The IO Item was corrrectly set</dd>
       *     <dt>E_FAIL </dt>
       *     <dd>The IO Item was not corrrectly set</dd>
       *   </dl>
       */
      virtual HRESULT SetIOItem( DNBIIOItem * ipIIOItem ) = 0;

      /**
       * Defines the IO Value for the activity.
       * @param iIOValue
       *   The IO Value.
       * @return
       *   An HRESULT.
       *   <br><b>Legal values</b>:
       *   <dl>
       *     <dt>S_OK</dt>
       *     <dd>The IO Value was corrrectly set</dd>
       *     <dt>E_FAIL </dt>
       *     <dd>The IO Value was not corrrectly set</dd>
       *   </dl>
       */
      virtual HRESULT SetIOValue( const VARIANT &iIOValue ) = 0;

      /**
       * Retrieves the IO Item for the activity.
       * @param opIIOItem
       *   The IO Item.
       * @return
       *   An HRESULT.
       *   <br><b>Legal values</b>:
       *   <dl>
       *     <dt>S_OK</dt>
       *     <dd>The component is successfully created
       *         and the interface pointer is successfully returned</dd>
       *     <dt>E_FAIL </dt>
       *     <dd>The component was successfully created,
       *         but the interface query failed</dd>
       *     <dt>E_NOINTERFACE </dt>
       *     <dd>The component was successfully created,
       *         but the it doesn't implement the requested interface</dd>
       *     <dt>E_OUTOFMEMORY </dt>
       *     <dd><dd>The component allocation failed</dd>
       *   </dl>
       */
      virtual HRESULT GetIOItem(DNBIIOItem **opIIOItem) const = 0;

      /**
       * Retrieves the IO Value for the activity.
       * @param oIOValue
       *   The IO Value.
       * @return
       *   An HRESULT.
       *   <br><b>Legal values</b>:
       *   <dl>
       *     <dt>S_OK</dt>
       *     <dd>The IO Value was corrrectly retrieved</dd>
       *     <dt>E_FAIL </dt>
       *     <dd>The IO Value was not corrrectly retrieved</dd>
       *   </dl>
       */
      virtual HRESULT GetIOValue(VARIANT &oIOValue) const = 0;

      /**
       * Retrieves the maximum time for the activity.
       * @param oMaxTime
       *   The maximum time.
       * @return
       *   An HRESULT.
       *   <br><b>Legal values</b>:
       *   <dl>
       *     <dt>S_OK</dt>
       *     <dd>The maximum time was corrrectly retrieved</dd>
       *     <dt>E_FAIL </dt>
       *     <dd>The maximum time was not corrrectly retrieved</dd>
       *   </dl>
       */
      virtual HRESULT GetMaxTime( double &oMaxTime ) const = 0;

      /**
       * Defines the maximum time for the activity.
       * @param iMaxTime
       *   The maximum time.
       * @return
       *   An HRESULT.
       *   <br><b>Legal values</b>:
       *   <dl>
       *     <dt>S_OK</dt>
       *     <dd>The maximum time was corrrectly set</dd>
       *     <dt>E_FAIL </dt>
       *     <dd>The maximum time was not corrrectly set</dd>
       *   </dl>
       */
      virtual HRESULT SetMaxTime( const double &iMaxTime ) = 0;


      /**
       * @nodoc
       * set the generatedBy option
       **/
      virtual HRESULT setGeneratedBy( CATUnicodeString generatedBy ) = 0;

      /**
       * @nodoc
       * get the generatedBy Option
       **/
      virtual HRESULT getGeneratedBy( CATUnicodeString& generatedBy ) const = 0;

};

/**
 * @nodoc
 */
CATDeclareHandler( DNBISetSignalActivity, CATBaseUnknown );
#endif
