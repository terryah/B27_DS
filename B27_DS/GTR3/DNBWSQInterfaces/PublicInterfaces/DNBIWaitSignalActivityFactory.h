#ifndef DNBIWaitSignalActivityFactory_H
#define DNBIWaitSignalActivityFactory_H

// COPYRIGHT Dassault Systemes 2003

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

#include "DNBWSQItfCPP.h"
#include "CATBaseUnknown.h"

class DNBIWaitSignalActivity;
class CATISPPChildManagement;

extern ExportedByDNBWSQItfCPP IID IID_DNBIWaitSignalActivityFactory ;

/**
 * Interface to create Wait Signal Activity.
 * <b>Role:</b>
 * This interface provides method to create a Wait Signal activity.
 */
class ExportedByDNBWSQItfCPP DNBIWaitSignalActivityFactory: public CATBaseUnknown
{
   /**
    * @nodoc
    */
   CATDeclareInterface;

  public:

      /**
       * Creates a Wait Signal activity as child of the given activity.
       * @param ispFatherActivity
       *   The parent activity.
       * @param opIWaitSignalActivity
       *   Newly created Wait Signal activity.
       * @return
       *   An HRESULT.
       *   <br><b>Legal values</b>:
       *   <dl>
       *     <dt>S_OK</dt>
       *     <dd>The component is successfully created
       *         and the interface pointer is successfully returned</dd>
       *     <dt>E_FAIL </dt>
       *     <dd>The component was successfully created,
       *         but the interface query failed</dd>
       *     <dt>E_NOINTERFACE </dt>
       *     <dd>The component was successfully created,
       *         but the it doesn't implement the requested interface</dd>
       *     <dt>E_OUTOFMEMORY </dt>
       *     <dd><dd>The component allocation failed</dd>
       *   </dl>
       */
      virtual HRESULT CreateChildWaitSignalActivity(CATISPPChildManagement *ispFatherActivity, 
                                                    DNBIWaitSignalActivity** opIWaitSignalActivity ) =0;
};

/**
 * @nodoc
 */
CATDeclareHandler( DNBIWaitSignalActivityFactory, CATBaseUnknown );
#endif
