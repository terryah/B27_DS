#ifndef CATSamAnimationLoopMode_H
#define CATSamAnimationLoopMode_H
//=============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//=============================================================================
//
// CATSamAnimationLoopMode : loop mode for animation
//
//=============================================================================
// April 2005 Creation
//=============================================================================

/**
  * @CAA2Level L0
  * @CAA2Usage U1
  */

/**
 * Animation loop mode
 */ 

enum CATSamAnimationLoopMode 
{
  CATSamAnimationLoopMode_OneWay,
  CATSamAnimationLoopMode_LoopBack,
  CATSamAnimationLoopMode_LoopOn
};
#endif
