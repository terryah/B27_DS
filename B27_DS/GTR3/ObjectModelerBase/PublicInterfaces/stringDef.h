/* -*-c++-*- */
#ifndef stringDef_h
#define stringDef_h
// COPYRIGHT DASSAULT SYSTEMES 1999

/** @CAA2Required */

#ifndef _WIN64
/**
 * @nodoc
 * Replace string by char*, except if you explicitly use the C string native type 
 * Type to use to define a string.
 */
typedef char* string ;

#endif

#endif
