/* -*-c++-*- */

///////////////////////////////////////////////////////////////////////////////
// COPYRIGHT DASSAULT SYSTEMES  1996                                         //
/** @CAA2Required */
//**********************************************************************
//* DON T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS *
//* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPEAR AT ANY TIME *
//**********************************************************************
// ========================================================================= //
//                                                                           //
// No class declared inside                                                  //
//                                                                           //
// ========================================================================= //
// Usage Notes:                                                              //
//                                                                           //
// Global includes for error management                                      //
//   This is THE file you will include in your code                          //
//   when you want to use CATTry/CATCatch to manage exceptions               //
//   "CATThrown" by O.M. services                                            //
//                                                                           //
// ========================================================================= //
// Nov. 96   Creation                                   J-L MALAVAL          //
///////////////////////////////////////////////////////////////////////////////

#ifndef CAT_OBJECT_MODELER_ERROR_H
#define CAT_OBJECT_MODELER_ERROR_H

// --------------------------------------------------------------------------
//                             Error enums
// --------------------------------------------------------------------------

#include "CATObjectModelerErrors.h"

// --------------------------------------------------------------------------
//                             Exceptions
// --------------------------------------------------------------------------

#include "CATObjectModelerExceptions.h"

#endif

