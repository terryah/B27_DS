// COPYRIGHT DASSAULT SYSTEMES 2000
/** @CAA2Required */
//**********************************************************************
//* DON T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS *
//* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPEAR AT ANY TIME *
//**********************************************************************

/* Generated from CATObjectModelerErrors.msg on Wed Jan 28 10:14:24 1998
 */
#ifndef _CATOBJECTMODELERERRORS_INCLUDE
#define _CATOBJECTMODELERERRORS_INCLUDE

typedef enum {

            OMInputNullReference = 0x00020001u,
         OMInputInvalidReference = 0x00020002u,

           OMInternalDocLinkFail = 0x00020001u,
          OMInternalUUIDLinkFail = 0x00020002u,
        OMInternalObjectLinkFail = 0x00020003u

} ObjectModelerErrors;

#endif
