// COPYRIGHT Dassault Systemes 2007
//===================================================================
//
// CATIOmbContainerLinksUpdateStatus.h
// 
// This interface is dedicated to containers embedding features pointing to 
// external documents. This interface enables activation of an internal DS 
// mechanism preventing GhostLinks generation. 
// This interface will be only called by the interactive save mechanism. 
//
//===================================================================
//  Nov 2007  Creation: Code generated by the CAA wizard  BES
//===================================================================

/**
* @CAA2Level L1
* @CAA2Usage U5
*/

#ifndef CATIOmbContainerLinksUpdateStatus_H
#define CATIOmbContainerLinksUpdateStatus_H

#include "AC0XXLNK.h"
#include "CATBaseUnknown.h"
#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByAC0XXLNK IID IID_CATIOmbContainerLinksUpdateStatus;
#else
extern "C" const IID IID_CATIOmbContainerLinksUpdateStatus;
#endif

/**
* This interface is dedicated to containers embedding features pointing to 
* external documents. This interface enables activation of an internal DS 
* mechanism preventing GhostLinks generation. 
* This interface will be only called by the interactive save mechanism on every containers (loaded or not) embedded in the document being saved. 
*/ 
class ExportedByAC0XXLNK CATIOmbContainerLinksUpdateStatus : public CATBaseUnknown
{
  CATDeclareInterface;
public:
  /**      
  *  Determines if the container has to be included in the DS mechanism against GhostLinks. 
  * <Role>  
  *   GhostLinks prevention may impact Save performances and is by default inactivated. 
  *   In order to turn it on for a specific container, container has to implement this interface and set oStatus to TRUE. 
  *   <br/>
  *   if oStatus is  set to TRUE, save mechanism will perform the following tasks
  *     <ul>
  *      <li/> load non loaded container
  *      <li/> apply the DS mechanism against GhostLinks on the container.   
  *     </ul>
  *   if oStatus is set to FALSE, save mechanism will perform the following tasks
  *    <ul>    
  *      <li/> no additional operation will be performed for the container. 
  *    </ul>
  *
  *   Typical implementation will look like 
  *   <code>
  *   {
  *     oStatus=TRUE;
  *     return S_OK;
  *   }
  *   </code>
  * @param [out] oStatus
  *   <ul>
  *   <li/>oStatus==TRUE  : Container will be included in DS mechanism against GhostLinks
  *   <li/>oStatus==FALSE : GhostLinks prevention will not be performed on the container.  
  *   </ul>
  * @return
  *   <ul>
  *   <li/><code>S_OK</code>   oStatus has been set with success
  *   <li/><code>E_FAIL</code> unexpected error. GhostLinks prevention will not be performed on the container.  
  *   </ul>
  */              
  virtual HRESULT NeedsToUpdateLinkForPointedRename(CATBoolean& oStatus) = 0;
};

//------------------------------------------------------------------
#endif


