/* -*-c++-*- */
#ifndef CATLISTV_CAT_INTERFACE_OBJECT_H
#define CATLISTV_CAT_INTERFACE_OBJECT_H
/** @CAA2Required */
//**********************************************************************
//* DON T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS *
//* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPEAR AT ANY TIME *
//**********************************************************************

// COPYRIGHT DASSAULT SYSTEMES 1999


// ==========================================================================
//           Declarations pour les listes de CATBaseUnknown_var
// ==========================================================================


// --------------------------------------------------------------------------
//                              Exported by module
// --------------------------------------------------------------------------

#include "AD0XXBAS.h"

// The real list definition is now in : 

#include "CATLISTV_CATBaseUnknown.h"

#endif


