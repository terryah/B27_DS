#ifndef DNBIFeatureGroupProperties_H
#define DNBIFeatureGroupProperties_H

// COPYRIGHT DASSAULT SYSTEMES, 2006,2007

/**
 * @CAA2Level L0
 * @CAA2Usage U3
 */

#include "DNBFastenerInterfaces.h"

// System
#include "CATBaseUnknown.h"
#include "CATLISTV_CATBaseUnknown.h"


extern ExportedByDNBFastenerInterfaces IID IID_DNBIFeatureGroupProperties;

//------------------------------------------------------------------

/** INTERFACE : 
  *   <b>DNBIFeatureGroupProperties</b>
  *<br>
  * DESCRIPTION :
  *   Methods to get properties of feature groups.
  */
class ExportedByDNBFastenerInterfaces DNBIFeatureGroupProperties: public CATBaseUnknown
{
  CATDeclareInterface;

  public:

    /**
      * METHODS :
      *  ListAllPointFasteners
      *
      * DESCRIPTION
      *  This method passes back all point fasteners of this feature group.
      *
      * @param oListPointFasteners
      *  List of CATBaseUnknown with all point fasteners of this feature group.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT ListAllPointFasteners(CATListValCATBaseUnknown_var& oListPointFasteners) = 0;

     
    /**
      * METHODS :
      *  ListAllFasteners
      *
      * DESCRIPTION
      *  This method passes back all fasteners of this feature group.
      *
      * @param oListFasteners
      *  List of CATBaseUnknown with all fasteners of this feature group.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT ListAllFasteners(CATListValCATBaseUnknown_var& oListFasteners) = 0;

     
    /**
      * METHODS :
      *  ListJoinedParts
      *
      * DESCRIPTION
      *  This method passes back all joined parts of this feature group.
      *
      * @param oListPointFasteners
      *  List of CATBaseUnknown with joint parts of this feature group.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT ListJoinedParts(CATListValCATBaseUnknown_var& oListJoinedParts) = 0;



  // No constructors or destructors on this pure virtual base class
  // --------------------------------------------------------------
};
// Handler definition
CATDeclareHandler(DNBIFeatureGroupProperties, CATBaseUnknown);
//------------------------------------------------------------------

#endif
