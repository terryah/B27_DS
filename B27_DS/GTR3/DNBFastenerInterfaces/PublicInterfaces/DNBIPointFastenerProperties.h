#ifndef DNBIPointFastenerProperties_H
#define DNBIPointFastenerProperties_H

// COPYRIGHT DASSAULT SYSTEMES, 2006,2007

/**
 * @CAA2Level L0
 * @CAA2Usage U3
 */

#include "DNBFastenerInterfaces.h"

// System
#include "CATBaseUnknown.h"
#include "CATListOfInt.h"
#include "CATListOfDouble.h"
#include "CATLISTV_CATBaseUnknown.h"


// Class forward declarations
class CATListValCATUnicodeString;
class CATMathTransformation;
class CATMathPoint;
class CATMathDirection;
class CATIProduct;


extern ExportedByDNBFastenerInterfaces IID IID_DNBIPointFastenerProperties;


//------------------------------------------------------------------

/** INTERFACE : 
  *   <b>DNBIPointFastenerProperties</b>
  *<br>
  * DESCRIPTION :
  *   Methods to get and set properties of point fasteners.
  */
class ExportedByDNBFastenerInterfaces DNBIPointFastenerProperties: public CATBaseUnknown
{
  CATDeclareInterface;

  public:

     /**
      * METHODS :
      *  GetProcessType
      *
      * DESCRIPTION
      *  This method passes back the process type of this point fastener.
      *
      * @param oProcessType
      *  CATUnicodeString with the process type of this point fastener.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT GetProcessType(CATUnicodeString& oProcessType) = 0;

     /**
      * METHODS :
      *  GetAssignStatus
      *
      * DESCRIPTION
      *  This method passes back the status of the assignment of this point fastener.
      *
      * @param oAssignStatus
      *  Unsigned integer value with the assigned status.
      *  0 = NoStatus
	   *  1 = NoAssignment
	   *  2 = ValidAssignment
	   *  3 = InvalidAssignment
	   *  4 = NotValidated
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT GetAssignStatus(unsigned int& oAssignStatus) = 0;

     /**
      * METHODS :
      *  GetX
      *
      * DESCRIPTION
      *  This method passes back the X coordinate value of the point fastener.
      *
      * @param oX
      *  Double value with the X coordinate value.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT GetX(double& oX) = 0;
     
     /**
      * METHODS :
      *  GetY
      *
      * DESCRIPTION
      *  This method passes back the Y coordinate value of the point fastener.
      *
      * @param oY
      *  Double value with the Y coordinate value.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT GetY(double& oY) = 0;
     
     /**
      * METHODS :
      *  GetZ
      *
      * DESCRIPTION
      *  This method passes back the Z coordinate value of the point fastener.
      *
      * @param oZ
      *  Double value with the Z coordinate value.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT GetZ(double& oZ) = 0;

     /**
      * METHODS :
      *  GetYaw
      *
      * DESCRIPTION
      *  This method passes back the Yaw orientation value of the point fastener.
      *
      * @param oYaw
      *  Double value with the Yaw orientation value.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT GetYaw(double& oYaw) = 0;

     /**
      * METHODS :
      *  GetPitch
      *
      * DESCRIPTION
      *  This method passes back the Pitch orientation value of the point fastener.
      *
      * @param oPitch
      *  Double value with the Pitch orientation value.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT GetPitch(double& oPitch) = 0;

     /**
      * METHODS :
      *  GetRoll
      *
      * DESCRIPTION
      *  This method passes back the Roll orientation value of the point fastener.
      *
      * @param oRoll
      *  Double value with the Roll orientation value.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT GetRoll(double& oRoll) = 0;

     /**
      * METHODS :
      *  GetSymbol
      *
      * DESCRIPTION
      *  This method passes back the Symbol of the point fastener.
      *
      * @param oSymbol
      *  CATUnicodeString with the Symbol value.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT GetSymbol(CATUnicodeString& oSymbol) = 0;

     /**
      * METHODS :
      *  SetSymbol
      *
      * DESCRIPTION
      *  This method sets the Symbol of the point fastener.
      *
      * @param iSymbol
      *  CATUnicodeString with the Symbol value.
      *  Valid symbols:
      *     CROSS
      *     PLUS
      *     CONCENTRIC
      *     COINCIDENT
      *     FULLCIRCLE
      *     FULLSQUARE
      *     STAR
      *     DOT
      *     SMALLDOT
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT SetSymbol(const CATUnicodeString& iSymbol) = 0;

     /**
      * METHODS :
      *  GetColor
      *
      * DESCRIPTION
      *  This method passes back the RGB color definition of the point fastener.
      *
      * @param oRed
      *  Unsigned integer value with the value of the red color component.
      * @param oGreen
      *  Unsigned integer value with the value of the green color component.
      * @param oBlue
      *  Unsigned integer value with the value of the blue color component.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT GetColor(unsigned int& oRed, unsigned int& oGreen, unsigned int& oBlue) = 0;

     /**
      * METHODS :
      *  SetColor
      *
      * DESCRIPTION
      *  This method passes back the RGB color definition of the point fastener.
      *
      * @param iRed
      *  Unsigned integer value with the value of the red color component.
      * @param iGreen
      *  Unsigned integer value with the value of the green color component.
      * @param iBlue
      *  Unsigned integer value with the value of the blue color component.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT SetColor(const unsigned int& iRed, const unsigned int& iGreen, const unsigned int& iBlue) = 0;

     /**
      * METHODS :
      *  GetJoiningParts
      *
      * DESCRIPTION
      *  This method passes back the parts joined by this point fastener.
      *
      * @param oListJoiningProducts
      *  List of CATBaseUnknown with the joining parts.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT GetJoiningParts(CATListValCATBaseUnknown_var& oListJoiningProducts) = 0;

     /**
      * METHODS :
      *  HasJoiningParts
      *
      * DESCRIPTION
      *  This method passes back if joining parts are defined for this point fastener.
      *
      * @param oHasJoiningParts
      *  CATBoolean if joining parts are defined.
      *  TRUE if fastener has joining parts.
      *  FALSE if fastener has no joining parts.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT HasJoiningParts(CATBoolean& oHasJoiningParts) = 0;

     /**
      * METHODS :
      *  NumberOfJoiningParts
      *
      * DESCRIPTION
      *  This method passes back the number of parts joined by this point fastener.
      *
      * @param oHasJoiningParts
      *  Integer value with the number of joining parts.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT NumberOfJoiningParts(int& oNumberJoiningParts) = 0;

     /**
      * METHODS :
      *  GetParent
      *
      * DESCRIPTION
      *  This method passes back the parent feature of this point fastener.
      *
      * @param oParentOfFeature
      *  CATBaseUnknown with the parent of this point fastener.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT GetParent(CATBaseUnknown*& oParentOfFeature) = 0;

     /**
      * METHODS :
      *  GetAssignedActivities
      *
      * DESCRIPTION
      *  This method passes back the assigned activities of this point fastener.
      *
      * @param oListOfAssignedActivities
      *  List of CATBaseUnknown with the assigned activities of this point fastener.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT GetAssignedActivities(CATListValCATBaseUnknown_var& oListOfAssignedActivities) = 0;

     /**
      * METHODS :
      *  IsAssigned
      *
      * DESCRIPTION
      *  This method passes back if this point fastener is assigned to an activity.
      *
      * @param ipActivity
      *  CATBaseUnknown with the activity to check the assignment.
      *  Use NULL to check against all activities.
      * @param oIsAssigned
      *  CATBoolean if this point fastener is assigned to the activity.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT IsAssigned(const CATBaseUnknown* ipActivity, CATBoolean& oIsAssigned) = 0;
     
     /**
      * METHODS :
      *  IsAssignedInE5
      *
      * DESCRIPTION
      *  This method passes back if this point fastener is assigned to an activity.
      *
      * @param ipIUE5Activity
      *  IUnknown with the activity to check the assignment.
      *  Use NULL to check against all activities.
      * @param oIsAssigned
      *  CATBoolean if this point fastener is assigned to the activity.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT IsAssignedInE5(const IUnknown* ipIUE5Activity, CATBoolean& oIsAssigned) = 0;

     /**
      * METHODS :
      *  GetTASProcessType
      *
      * DESCRIPTION
      *  This method passes back the TAS process type of this point fastener.
      *
      * @param oTASProcessType
      *  CATUnicodeString with the TAS process type of this point fastener.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT GetTASProcessType(CATUnicodeString& oTASProcessType) = 0;

     /**
      * METHODS :
      *  GetFasteningContext
      *
      * DESCRIPTION
      *  This method passes back the fastening context of this point fastener.
      *
      * @param oFasteningContext
      *  CATUnicodeString with the fastening context of this point fastener.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT GetFasteningContext(CATUnicodeString& oFasteningContext) = 0;

     /**
      * METHODS :
      *  GetDesignPosition
      *
      * DESCRIPTION
      *  This method passes back the point fastener's design position.
      *
      * @param oList
      *  List of doubles with the design position.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT GetDesignPosition(CATListOfDouble* oList) = 0;
     
     /**
      * METHODS :
      *  GetManufacturingPosition
      *
      * DESCRIPTION
      *  This method passes back the point fastener's manufacturing position.
      *
      * @param oList
      *  List of doubles with the manufacturing position.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT GetManufacturingPosition(CATListOfDouble* oList) = 0;

     /**
      * METHODS :
      *  GetManufacturingOffset
      *
      * DESCRIPTION
      *  This method passes back the point fastener's manufacturing offset.
      *
      * @param oManufacturingOffset
      *  Double value with the manufacturing offset.
      *  The manufacturing offset is returned in degree.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT GetManufacturingOffset(double& oManufacturingOffset) = 0;
        
     /**
      * METHODS :
      *  BuildAbsPosition
      *
      * DESCRIPTION
      *  This method passes back the position of the point fastener in absolute coordinates.
      *
      * @param oTransfo
      *  CATMathTransformation with the absolute position of the point fastener.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT BuildAbsPosition(CATMathTransformation& oTransfo) = 0;

     /**
      * METHODS :
      *  BuildLocalPosition
      *
      * DESCRIPTION
      *  This method passes back the position of the point fastener in local coordinates.
      *
      * @param oTransfo
      *  CATMathTransformation with the relative position of the point fastener.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT BuildLocalPosition(CATMathTransformation& oTransfo) = 0;

     /**
      * METHODS :
      *  BuildAbsManufPosition
      *
      * DESCRIPTION
      *  This method passes back the position of the point fastener's 
      *  manufacturing position in absolute coordinates.
      *
      * @param oTransfo
      *  CATMathTransformation with the absolute position of the 
      *  point fastener's manufacturing position.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT BuildAbsManufPosition(CATMathTransformation& oTransfo) = 0;

     /**
      * METHODS :
      *  BuildLocalManufPosition
      *
      * DESCRIPTION
      *  This method passes back the position of the point fastener's 
      *  manufacturing position in local coordinates.
      *
      * @param oTransfo
      *  CATMathTransformation with the relative position of the 
      *  point fastener's manufacturing position.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT BuildLocalManufPosition(CATMathTransformation& oTransfo) = 0;

     /**
      * METHODS :
      *  GetOwnerProduct
      *
      * DESCRIPTION
      *  This method passes back the owning product of the point fastener.
      *
      * @param opiOwnerProduct
      *  CATIProduct interface on the owning product of this point fastener.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT GetOwnerProduct(CATIProduct*& opiOwnerProduct) = 0;  

     /**
      * METHODS :
      *  GetDesignPointAndDirection
      *
      * DESCRIPTION
      *  This method passes back the point fastener's design position and direction.
      *
      * @param oMathPoint
      *  CATMathPoint with the design position.
      * @param oDirection
      *  CATMathPoint with the direction at the design position.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT GetDesignPointAndDirection(CATMathPoint& oMathPoint, CATMathDirection& oDirection) = 0;

     
     /**
      * METHODS :
      *  GetStringAttributeE5
      *
      * DESCRIPTION
      *  This method passes back the value of an E5 string attribute.
      *  Attributes from the actuell type and base types of the plantype are regarded.
      *
      * @param iAttributeName
      *  CATUnicodeString with the internal name of the attribute.
      * @param oAttributeValue
      *  CATUnicodeString with the attribute value.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT GetStringAttributeE5(const CATUnicodeString& iAttributeName, 
                                          CATUnicodeString& oAttributeValue) = 0;

     /**
      * METHODS :
      *  GetLongAttributeE5
      *
      * DESCRIPTION
      *  This method passes back the value of an E5 integer attribute.
      *  Attributes from the actuell type and base types of the plantype are regarded.
      *
      * @param iAttributeName
      *  CATUnicodeString with the internal name of the attribute.
      * @param oAttributeValue
      *  Long value with the attribute value.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT GetLongAttributeE5(const CATUnicodeString& iAttributeName, 
                                        long& oAttributeValue) = 0;

     /**
      * METHODS :
      *  GetDoubleAttributeE5
      *
      * DESCRIPTION
      *  This method passes back the value of an E5 double attribute.
      *  Attributes from the actuell type and base types of the plantype are regarded.
      *
      * @param iAttributeName
      *  CATUnicodeString with the internal name of the attribute.
      * @param oAttributeValue
      *  Double value with the attribute value.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT GetDoubleAttributeE5(const CATUnicodeString& iAttributeName, 
                                          double& oAttributeValue) = 0;

     /**
      * METHODS :
      *  GetBooleanAttributeE5
      *
      * DESCRIPTION
      *  This method passes back the value of an E5 boolean attribute.
      *  Attributes from the actuell type and base types of the plantype are regarded.
      *
      * @param iAttributeName
      *  CATUnicodeString with the internal name of the attribute.
      * @param oAttributeValue
      *  CATBoolean with the attribute value.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT GetBooleanAttributeE5(const CATUnicodeString& iAttributeName, 
                                           CATBoolean& oAttributeValue) = 0;


  // No constructors or destructors on this pure virtual base class
  // --------------------------------------------------------------
};
// Handler definition
CATDeclareHandler(DNBIPointFastenerProperties, CATBaseUnknown);
//------------------------------------------------------------------

#endif
