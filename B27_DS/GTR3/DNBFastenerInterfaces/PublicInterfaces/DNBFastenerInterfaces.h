// COPYRIGHT DASSAULT SYSTEMES, 2006,2007
/** @CAA2Required */
/**********************************************************************/
/* DON T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPEAR AT ANY TIME */
/**********************************************************************/
#ifdef  _WINDOWS_SOURCE
#ifdef  __DNBFastenerInterfaces
#define ExportedByDNBFastenerInterfaces     __declspec(dllexport)
#else
#define ExportedByDNBFastenerInterfaces     __declspec(dllimport)
#endif
#else
#define ExportedByDNBFastenerInterfaces
#endif
