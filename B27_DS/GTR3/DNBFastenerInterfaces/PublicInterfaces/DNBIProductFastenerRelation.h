#ifndef DNBIProductFastenerRelation_H
#define DNBIProductFastenerRelation_H

// COPYRIGHT DASSAULT SYSTEMES, 2006,2007

/**
 * @CAA2Level L0
 * @CAA2Usage U3
 */

#include "DNBFastenerInterfaces.h"

// System
#include "CATBaseUnknown.h"
#include "CATLISTV_CATBaseUnknown.h"
#include "CATBoolean.h"

// Class forward declaration
class CATIProduct;


extern ExportedByDNBFastenerInterfaces IID IID_DNBIProductFastenerRelation;

//------------------------------------------------------------------

/** INTERFACE : 
  *   <b>DNBIProductFastenerRelation</b>
  *<br>
  * DESCRIPTION :
  *   Methods to get the relationship between a product and assigned fasteners.
  */
class ExportedByDNBFastenerInterfaces DNBIProductFastenerRelation: public CATBaseUnknown
{
  CATDeclareInterface;

  public:

     /**
      * METHODS :
      *  GetJoinedPartsCount
      *
      * DESCRIPTION
      *  This method passes back the number of joined parts of a specified fastener.
      *
      * @param iFeature
      *  CATBaseUnknown with the fastener to check.
      * @param oCount
      *  Integer value with the number of joined parts of this fastener.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT GetJoinedPartsCount(CATBaseUnknown* iFeature, int& oCount) = 0;

     /**
      * METHODS :
      *  GetJoinedPart
      *
      * DESCRIPTION
      *  This method passes back the joined part at a specified position of a fastener.
      *
      * @param iFeature
      *  CATBaseUnknown with the fastener to check.
      * @param iPosition
      *  Integer value with the position of the joined part in the fastener's list of joined parts.
      * @param oJoinedProduct
      *  CATIProduct interface with the joint part at the specified position.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT GetJoinedPart(CATBaseUnknown* iFeature, const int& iPosition, CATIProduct*& oJoinedProduct) = 0;

     /**
      * METHODS :
      *  ListJoinedParts
      *
      * DESCRIPTION
      *  This method passes back the joined parts of a specified fastener.
      *
      * @param iFeature
      *  CATBaseUnknown with the fastener to check.
      * @param oListChildren
      *  List of CATBaseUnknown with the joined parts of the specified fastener.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT ListJoinedParts(CATBaseUnknown* iFeature, CATListValCATBaseUnknown_var& oListChildren) = 0;

     /**
      * METHODS :
      *  GetJointFeature
      *
      * DESCRIPTION
      *  This method passes back the joint feature of ???.
      *
      * @param iFeature
      *  ???
      * @param oJointFeature
      *  CATBaseUnknown interface with the joint fastener.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT GetJointFeature(CATBaseUnknown* iFeature, CATBaseUnknown*& oJointFeature) = 0;

     /**
      * METHODS :
      *  GetFeaturesOnPart
      *
      * DESCRIPTION
      *  This method passes back the BIWFeatures on a specified part.
      *
      * @param iProduct
      *  CATBaseUnknown with the part to check.
      * @param oListFeatures
      *  List of CATBaseUnknown with the BIWFeatures on this part.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT GetFeaturesOnPart(CATBaseUnknown* iProduct, CATListValCATBaseUnknown_var& oListFeatures) = 0;

     /**
      * METHODS :
      *  GetAllFastenersOnPart
      *
      * DESCRIPTION
      *  This method passes back all fasteners of a specified part.
      *
      * @param iProduct
      *  CATBaseUnknown with the part to check.
      * @param oListFasteners
      *  List of CATBaseUnknown with the fasteners of the specified part.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT GetAllFastenersOnPart(CATBaseUnknown* iProduct, 
                                           CATListValCATBaseUnknown_var& oListFasteners) = 0;

     /**
      * METHODS :
      *  GetAllFeatureGroupsOnPart
      *
      * DESCRIPTION
      *  This method passes back all feature groups of a specified part.
      *
      * @param iProduct
      *  CATBaseUnknown with the part to check.
      * @param oListFeatureGroups
      *  List of CATBaseUnknown with the feature groups of the specified part.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT GetAllFeatureGroupsOnPart(CATBaseUnknown* iProduct, 
                                               CATListValCATBaseUnknown_var& oListFeatureGroups) = 0;

     /**
      * METHODS :
      *  GetFeaturesWithNoParts
      *
      * DESCRIPTION
      *  This method passes back the BIWFeatures which have no joined parts defined.
      *
      * @param oListFeatures
      *  List of CATBaseUnknown with BIWFeatures without joined parts.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT GetFeaturesWithNoParts(CATListValCATBaseUnknown_var& oListFeatures) = 0;

     /**
      * METHODS :
      *  GetFastenersJoiningParts
      *
      * DESCRIPTION
      *  This method passes back the fasteners which join the specified parts.
      *
      * @param iListParts
      *  List of CATBaseUnknown with the parts to check.
      * @param oListFasteners
      *  List of CATBaseUnknown with the fasteners who join the specified parts.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT GetFastenersJoiningParts(CATListValCATBaseUnknown_var& iListParts, 
                                              CATListValCATBaseUnknown_var& oListFasteners) = 0;

  // No constructors or destructors on this pure virtual base class
  // --------------------------------------------------------------
};
// Handler definition
CATDeclareHandler (DNBIProductFastenerRelation, CATBaseUnknown);
//------------------------------------------------------------------


#endif
