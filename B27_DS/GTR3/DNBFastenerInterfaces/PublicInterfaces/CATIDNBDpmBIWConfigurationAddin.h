#ifndef CATIDNBDpmBIWConfigurationAddin_h
#define CATIDNBDpmBIWConfigurationAddin_h

// COPYRIGHT DASSAULT SYSTEMES, 2006,2007

/**
 * @CAA2Level L0
 * @CAA2Usage U5
 */

// --- ApplicationFrame
#include "CATIWorkbenchAddin.h" 
#include "DNBFastenerInterfaces.h"

extern ExportedByDNBFastenerInterfaces IID IID_CATIDNBDpmBIWConfigurationAddin;

/**
 * Interface to add toolbars in the Fastening Process Planning workbench.
 * @see CATIWorkbenchAddin
 */

class ExportedByDNBFastenerInterfaces CATIDNBDpmBIWConfigurationAddin : public CATIWorkbenchAddin
{
   CATDeclareInterface;
public:
};

CATDeclareHandler (CATIDNBDpmBIWConfigurationAddin, CATIWorkbenchAddin);

#endif
