#ifndef DELMIAFastenerGroup_IDL
#define DELMIAFastenerGroup_IDL

/*IDLREP*/
/* -*-idl-*- */

// COPYRIGHT Dassault Systemes 2005 2007

/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */

#include "CATIABase.idl"
#include "CATIAProduct.idl"


/*
  *<b>Role:</b> User gets the handle of the SpotWeld from a collection of 
  *  SpotWelds.
  */

interface DELMIAFastenerGroup : CATIABase
{

 /**
  * Sets the color of the fasteners.
  * <!-- @sample -->
  * <dl>
  * <dt><b>Example:</b>
  * <dd>
  * This example sets the Color type of all the fasteners to red
  * <pre>
  * redCol  =16711680 'Encoded RGB color within long integer (R=255 G=0 B=0)
  * MyFastenerGroup.Color = redCol
  * </pre>
  * </dl>
  */
#pragma PROPERTY Color
  HRESULT put_Color(in long iColor);

//------------------------------------------------------------------------------
 /**
  *Sets the CGR Style representation for the Spot Welds in the 3D Viewer.
  *@param <i>iStyle</i>
  *Type of the CGR Style to be used for representation. 
  *<br>	The valid set of Styles are :- 
  *<ul>
  *  <li>As specified in XML in tools-options page</li>
  *</ul>
  * <!-- @sample -->
  * <dl>
  * <dt><b>Example:</b>
  * <dd>
  * This example sets the CGR Style type of all the fasteners
  * to Sphere provided it is available in XML file
  * <pre>
  * MyFastenerGroup.Style = "Sphere"
  * </pre>
  * </dl>
  */
#pragma PROPERTY Style
  HRESULT put_Style(in CATBSTR iStyle);

  //------------------------------------------------------------------------------
 /**
  *Sets the symbol for representation for the Spot Welds in the 3D Viewer.
  *@param <i>iSymbol</i>
  *Type of the symbol to be used for representation. 
  *<br>	The valid set of symbols are :- 
  *	<ul>
  *  <li>	CROSS </li>
  *  <li>	PLUS </li>
  *  <li>	CONCENTRIC </li>
  *  <li>	COINCIDENT </li>
  *  <li>	FULLCIRCLE </li>
  *  <li>	FULLSQUARE </li>
  *  <li>	STAR </li>
  *  <li>	DOT </li>
  *  <li>	SMALLDOT </li>
  *</ul>
  * <!-- @sample -->
  * <dl>
  * <dt><b>Example:</b>
  * <dd>
  * This example sets the Symbol type of all the fasteners to FULLSquare
  * <pre>
  * MyFastenerGroup.Symbol = "FULLSQUARE"
  * </pre>
  * </dl>
  */
#pragma PROPERTY Symbol
  HRESULT put_Symbol(in CATBSTR iColor);

  //------------------------------------------------------------------------------
 /**
  * Returns the number of parts joined by the fastener group.
  * @param oNumOfParts
  *   The number of parts
  * <!-- @sample -->
  * </dl>
  * <dt><b>Example:</b>
  * <dd>
  * This example gets number of parts joined by fastener
  * <pre>
  * Dim NumberOfParts
  * NumberOfParts = MyFastenerGroup.NumberOfJoiningParts
  * </pre>
  * </dl>
  */
  HRESULT NumberOfJoiningParts(out /*IDLRETVAL*/ long oNumOfParts);

//------------------------------------------------------------------------------
 /**
  * Returns the list of part names joined by fastener.
  * @param oListOfParts
  *   The list of parts
  * <!-- @sample -->
  * </dl>
  * <dt><b>Example:</b>
  * <dd>
  * This example gets list of parts joined by fastener
  * <pre>
  * Dim NumberOfParts
  * Dim JoiningParts()
  * NumberOfParts = MyFastenerGroup.NumberOfJoiningParts
  * ReDim JoiningParts(NumParts-1)
  * MyFastenerGroup.GetJoinedPartNames(JoiningParts)
  * </pre>
  * </dl>
  */
  HRESULT GetJoinedPartNames(inout  CATSafeArrayVariant oListOfParts);

  //------------------------------------------------------------------------------
 /**
  * Returns the entities in the fastener group of the type which is input.
  * which are direct children only
  * <!-- @sample -->
  * </dl>
  * <dt><b>Example:</b>
  * <dd>
  * This example gets welds in the fastener Group
  * <pre>
  * Dim NumberOfWelds
  * NumberOfWelds = MyFastenerGroup.GetItems("DELMIASpotWeld")
  * </pre>
  * This example gets the FastenerGroups in the fastener Group
  * <pre>
  * Dim NumberOfGroups
  * NumberOfGroups = MyFastenerGroup.GetItems("DELMIAFastenerGroup")
  * </pre>
  * </dl>
  */
  HRESULT  GetItems(in CATBSTR IDName, inout CATSafeArrayVariant oListOfEntities);
  

  //------------------------------------------------------------------------------
 /**
  * Returns the number of entities in the fastener group of the Type.
  * @param oNumOfEntities
  *   The number of Entities
  * <!-- @sample -->
  * </dl>
  * <dt><b>Example:</b>
  * <dd>
  * This example gets number of welds in the fastener Group
  * <pre>
  * Dim NumberOfWelds
  * NumberOfWelds = MyFastenerGroup.GetCount("DELMIASpotWeld")
  * </pre>
  * This example gets number of FastenerGroups in the fastener Group
  * <pre>
  * Dim NumberOfGroups
  * NumberOfGroups = MyFastenerGroup.GetCount("DELMIAFastenerGroup")
  * </pre>
  * </dl>
  */
  HRESULT GetCount(in CATBSTR IDName,out /*IDLRETVAL*/ long oNumOfWelds);

  //------------------------------------------------------------------------------
 /**
  * Hides all the entities in the fastener group.
  * <!-- @sample -->
  * </dl>
  * <dt><b>Example:</b>
  * <dd>
  * This example hides all the welds in the fastener Group
  * <pre>
  * MyFastenerGroup.Hide
  * </pre>
  * </dl>
  */
  HRESULT Hide();


  //------------------------------------------------------------------------------
 /**
  * Shows all the entities in the fastener group.
  * <!-- @sample -->
  * </dl>
  * <dt><b>Example:</b>
  * <dd>
  * This example shows all the welds in the fastener Group
  * <pre>
  * MyFastenerGroup.Show
  * </pre>
  * </dl>
  */
  HRESULT Show();

  	//------------------------------------------------------------------------------
	/**
	* Returns the product at the specified index from the list of parts joined by fastener group.
	* @param index
	*   Index should be greater than equal to 1.
	* <!-- @sample -->
	* </dl>
	* <dt><b>Example:</b>
	* <dd>
	* This example gets the part at index 1
	* <pre>
	* Dim MyProduct As Product
	* Set MyProduct = MyFastenerGroup.GetPart(1)
	* </pre>
	* </dl>
	*/


  HRESULT GetPart (in short index, out /*IDLRETVAL*/ CATIAProduct oProduct);
  
};

// Interface name : DELMIAFastenerGroup
#pragma ID DELMIAFastenerGroup "DCE:DFA75814-683E-4667-BDDC365C94560566"
#pragma DUAL DELMIAFastenerGroup

// VB object name : FastenerGroup (Id used in Visual Basic)
#pragma ID FastenerGroup "DCE:FAA4A5D2-966B-476b-8E7CBFB544152F56"
#pragma ALIAS DELMIAFastenerGroup FastenerGroup

#endif /*DELMIAFastenerGroup_IDL*/
