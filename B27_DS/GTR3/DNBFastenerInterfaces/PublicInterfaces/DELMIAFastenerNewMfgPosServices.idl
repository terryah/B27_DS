#ifndef DELMIAFastenerNewMfgPosServices_IDL
#define DELMIAFastenerNewMfgPosServices_IDL

/*IDLREP*/
/* -*-idl-*- */

// COPYRIGHT Dassault Systemes 2011

/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */

/*
* Utility class to flag,unflag manufacturing fasteners and get tagged fasteners. 
* This enables user to flag a manufacturing fastener to create a new manufacturing fastener in its place. 
*/


#include "CATIABase.idl"
#include "DELMIAFastener.idl"
#include "CATSafeArray.idl" 



interface DELMIAFastenerNewMfgPosServices : CATIABase
{
	//------------------------------------------------------------------------------
	/**
	* flags a specified DELMIAFastener to create a new manufacturing fastener in its place.
	*	* <!-- @sample -->
	* </dl>
	* <dt><b>Example:</b>
	* <dd>
	* Set myObject = CATIA.GetItem("DNBFastenerNewMfgPosServices")
	* myObject.SetFlag MyFastener
	* always returns S_OK even the function is not successful. For example the fastener is already flagged.
	* </pre>
	* </dl>

	*
	*/
	HRESULT SetFlag (in DELMIAFastener iFastener);

	//------------------------------------------------------------------------------
	/**
	* flags a list of DELMIAFasteners to create  new manufacturing fasteners in their place.
	*	* <!-- @sample -->
	* </dl>
	* <dt><b>Example:</b>
	* <dd>
	* Set myObject = CATIA.GetItem("DNBFastenerNewMfgPosServices")
	* myObject.SetFlagOnFasteners ArrayFastener
	* always returns S_OK even the function is not successful. For example the fasteners are already flagged.
	* </pre>
	* </dl>

	*
	*/
	HRESULT SetFlagOnFasteners (in CATSafeArrayVariant iFastenerArray);

	//------------------------------------------------------------------------------
	/**
	* unflags a flagged DELMIAFastener to create a new manufacturing fastener in its place.
	*	* <!-- @sample -->
	* </dl>
	* <dt><b>Example:</b>
	* <dd>
	* Set myObject = CATIA.GetItem("DNBFastenerNewMfgPosServices")
	* myObject.UnsetFlag MyFastener
	* always returns S_OK even the function is not successful. For example the fastener is not flagged.
	*
	*/
	HRESULT UnsetFlag(in DELMIAFastener iFastener);

	//------------------------------------------------------------------------------
	/**
	* unflags a list of DELMIAFasteners to create  new manufacturing fasteners in their place.
	*	* <!-- @sample -->
	* </dl>
	* <dt><b>Example:</b>
	* <dd>
	* Set myObject = CATIA.GetItem("DNBFastenerNewMfgPosServices")
	* myObject.UnsetFlagOnFasteners ArrayFastener
	* always returns S_OK even the function is not successful. For example the fasteners are not flagged.
	*
	*/
	HRESULT UnsetFlagOnFasteners(in CATSafeArrayVariant  iFastenerArray);

	//------------------------------------------------------------------------------
	/**
	* Gets number of flagged DELMIAFasteners.
	*	* <!-- @sample -->
	* </dl>
	* <dt><b>Example:</b>
	* <dd>
	* Set myObject = CATIA.GetItem("DNBFastenerNewMfgPosServices")
	* NbFastener = myObject.GetNumberOfFlaggedFasteners 	
	*
	*/
	HRESULT GetNumberOfFlaggedFasteners (out /*IDLRETVAL*/ long oNbFasteners);

	//------------------------------------------------------------------------------
	/**
	* Gets List of flagged DELMIAFasteners.
	*	* <!-- @sample -->
	* </dl>
	* <dt><b>Example:</b>
	* <dd>
	* Set myObject = CATIA.GetItem("DNBFastenerNewMfgPosServices")
	* myObject.GetFlaggedFasteners ArrayFastener	
	*
	*/
	HRESULT GetFlaggedFasteners(inout CATSafeArrayVariant oFastenerArray);


};

// Interface name : DELMIAFastenerNewMfgPosServices
#pragma ID DELMIAFastenerNewMfgPosServices "DCE:A65AAF30-2C13-415e-AF89F5FB7341ED32"  
#pragma DUAL DELMIAFastenerNewMfgPosServices

// VB object name : FastenerItemServices (Id used in Visual Basic)
// {63AB5CA3-8D73-4e15-BBD24842DE5BCD5D}
#pragma ID DNBFastenerNewMfgPosServices "DCE:63AB5CA3-8D73-4e15-BBD24842DE5BCD5D"
#pragma ALIAS DELMIAFastenerNewMfgPosServices DNBFastenerNewMfgPosServices

#endif /*DELMIAFastenerNewMfgPosServices_IDL*/



