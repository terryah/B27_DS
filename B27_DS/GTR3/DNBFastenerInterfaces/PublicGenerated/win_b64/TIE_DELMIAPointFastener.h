#ifndef __TIE_DELMIAPointFastener
#define __TIE_DELMIAPointFastener

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DELMIAPointFastener.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DELMIAPointFastener */
#define declare_TIE_DELMIAPointFastener(classe) \
 \
 \
class TIEDELMIAPointFastener##classe : public DELMIAPointFastener \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DELMIAPointFastener, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall get_Symbol(CATBSTR & oColor); \
      virtual HRESULT __stdcall put_Symbol(const CATBSTR & iColor); \
      virtual HRESULT __stdcall get_Style(CATBSTR & oStyle); \
      virtual HRESULT __stdcall put_Style(const CATBSTR & iStyle); \
      virtual HRESULT __stdcall ResetPosition(CAT_VARIANT_BOOL iVal); \
      virtual HRESULT __stdcall GetTag(CATBaseDispatch *& oTag); \
      virtual HRESULT __stdcall GetDesignXYZYPR(CATSafeArrayVariant & ioXYZYPR); \
      virtual HRESULT __stdcall get_ProcessType(CATBSTR & oProcessType); \
      virtual HRESULT __stdcall get_Color(CATLONG & oColor); \
      virtual HRESULT __stdcall put_Color(CATLONG iColor); \
      virtual HRESULT __stdcall NumberOfJoiningParts(CATLONG & oNumOfParts); \
      virtual HRESULT __stdcall SetTextIDVisibility(CAT_VARIANT_BOOL iStatus); \
      virtual HRESULT __stdcall GetStringUserAttribute(const CATBSTR & iAttributeLabel, CATBSTR & oStringValue); \
      virtual HRESULT __stdcall GetIntUserAttribute(const CATBSTR & iAttributeLabel, CATLONG & oIntValue); \
      virtual HRESULT __stdcall GetDoubleUserAttribute(const CATBSTR & iAttributeLabel, double & oDoubleValue); \
      virtual HRESULT __stdcall GetParts(CATSafeArrayVariant & oListOfParts); \
      virtual HRESULT __stdcall AllPartsLoaded(CAT_VARIANT_BOOL & oAllPartsLoaded); \
      virtual HRESULT __stdcall NumberOfAssignedProcesses(CATLONG & oNumOfProcesses); \
      virtual HRESULT __stdcall GetProcess(short index, CATIAActivity *& oProcess); \
      virtual HRESULT __stdcall GetPart(short index, CATIAProduct *& oProduct); \
      virtual HRESULT __stdcall SetStringUserAttribute(const CATBSTR & iAttributeLabel, const CATBSTR & iStringValue); \
      virtual HRESULT __stdcall SetIntUserAttribute(const CATBSTR & iAttributeLabel, CATLONG iIntValue); \
      virtual HRESULT __stdcall SetDoubleUserAttribute(const CATBSTR & iAttributeLabel, double iDoubleValue); \
      virtual HRESULT __stdcall RemoveFromPPR(CAT_VARIANT_BOOL iForceRemoveIfAssigned, DNBPPRRemoveStatus & eStatus); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DELMIAPointFastener(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall get_Symbol(CATBSTR & oColor); \
virtual HRESULT __stdcall put_Symbol(const CATBSTR & iColor); \
virtual HRESULT __stdcall get_Style(CATBSTR & oStyle); \
virtual HRESULT __stdcall put_Style(const CATBSTR & iStyle); \
virtual HRESULT __stdcall ResetPosition(CAT_VARIANT_BOOL iVal); \
virtual HRESULT __stdcall GetTag(CATBaseDispatch *& oTag); \
virtual HRESULT __stdcall GetDesignXYZYPR(CATSafeArrayVariant & ioXYZYPR); \
virtual HRESULT __stdcall get_ProcessType(CATBSTR & oProcessType); \
virtual HRESULT __stdcall get_Color(CATLONG & oColor); \
virtual HRESULT __stdcall put_Color(CATLONG iColor); \
virtual HRESULT __stdcall NumberOfJoiningParts(CATLONG & oNumOfParts); \
virtual HRESULT __stdcall SetTextIDVisibility(CAT_VARIANT_BOOL iStatus); \
virtual HRESULT __stdcall GetStringUserAttribute(const CATBSTR & iAttributeLabel, CATBSTR & oStringValue); \
virtual HRESULT __stdcall GetIntUserAttribute(const CATBSTR & iAttributeLabel, CATLONG & oIntValue); \
virtual HRESULT __stdcall GetDoubleUserAttribute(const CATBSTR & iAttributeLabel, double & oDoubleValue); \
virtual HRESULT __stdcall GetParts(CATSafeArrayVariant & oListOfParts); \
virtual HRESULT __stdcall AllPartsLoaded(CAT_VARIANT_BOOL & oAllPartsLoaded); \
virtual HRESULT __stdcall NumberOfAssignedProcesses(CATLONG & oNumOfProcesses); \
virtual HRESULT __stdcall GetProcess(short index, CATIAActivity *& oProcess); \
virtual HRESULT __stdcall GetPart(short index, CATIAProduct *& oProduct); \
virtual HRESULT __stdcall SetStringUserAttribute(const CATBSTR & iAttributeLabel, const CATBSTR & iStringValue); \
virtual HRESULT __stdcall SetIntUserAttribute(const CATBSTR & iAttributeLabel, CATLONG iIntValue); \
virtual HRESULT __stdcall SetDoubleUserAttribute(const CATBSTR & iAttributeLabel, double iDoubleValue); \
virtual HRESULT __stdcall RemoveFromPPR(CAT_VARIANT_BOOL iForceRemoveIfAssigned, DNBPPRRemoveStatus & eStatus); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DELMIAPointFastener(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::get_Symbol(CATBSTR & oColor) \
{ \
return (ENVTIECALL(DELMIAPointFastener,ENVTIETypeLetter,ENVTIELetter)get_Symbol(oColor)); \
} \
HRESULT __stdcall  ENVTIEName::put_Symbol(const CATBSTR & iColor) \
{ \
return (ENVTIECALL(DELMIAPointFastener,ENVTIETypeLetter,ENVTIELetter)put_Symbol(iColor)); \
} \
HRESULT __stdcall  ENVTIEName::get_Style(CATBSTR & oStyle) \
{ \
return (ENVTIECALL(DELMIAPointFastener,ENVTIETypeLetter,ENVTIELetter)get_Style(oStyle)); \
} \
HRESULT __stdcall  ENVTIEName::put_Style(const CATBSTR & iStyle) \
{ \
return (ENVTIECALL(DELMIAPointFastener,ENVTIETypeLetter,ENVTIELetter)put_Style(iStyle)); \
} \
HRESULT __stdcall  ENVTIEName::ResetPosition(CAT_VARIANT_BOOL iVal) \
{ \
return (ENVTIECALL(DELMIAPointFastener,ENVTIETypeLetter,ENVTIELetter)ResetPosition(iVal)); \
} \
HRESULT __stdcall  ENVTIEName::GetTag(CATBaseDispatch *& oTag) \
{ \
return (ENVTIECALL(DELMIAPointFastener,ENVTIETypeLetter,ENVTIELetter)GetTag(oTag)); \
} \
HRESULT __stdcall  ENVTIEName::GetDesignXYZYPR(CATSafeArrayVariant & ioXYZYPR) \
{ \
return (ENVTIECALL(DELMIAPointFastener,ENVTIETypeLetter,ENVTIELetter)GetDesignXYZYPR(ioXYZYPR)); \
} \
HRESULT __stdcall  ENVTIEName::get_ProcessType(CATBSTR & oProcessType) \
{ \
return (ENVTIECALL(DELMIAPointFastener,ENVTIETypeLetter,ENVTIELetter)get_ProcessType(oProcessType)); \
} \
HRESULT __stdcall  ENVTIEName::get_Color(CATLONG & oColor) \
{ \
return (ENVTIECALL(DELMIAPointFastener,ENVTIETypeLetter,ENVTIELetter)get_Color(oColor)); \
} \
HRESULT __stdcall  ENVTIEName::put_Color(CATLONG iColor) \
{ \
return (ENVTIECALL(DELMIAPointFastener,ENVTIETypeLetter,ENVTIELetter)put_Color(iColor)); \
} \
HRESULT __stdcall  ENVTIEName::NumberOfJoiningParts(CATLONG & oNumOfParts) \
{ \
return (ENVTIECALL(DELMIAPointFastener,ENVTIETypeLetter,ENVTIELetter)NumberOfJoiningParts(oNumOfParts)); \
} \
HRESULT __stdcall  ENVTIEName::SetTextIDVisibility(CAT_VARIANT_BOOL iStatus) \
{ \
return (ENVTIECALL(DELMIAPointFastener,ENVTIETypeLetter,ENVTIELetter)SetTextIDVisibility(iStatus)); \
} \
HRESULT __stdcall  ENVTIEName::GetStringUserAttribute(const CATBSTR & iAttributeLabel, CATBSTR & oStringValue) \
{ \
return (ENVTIECALL(DELMIAPointFastener,ENVTIETypeLetter,ENVTIELetter)GetStringUserAttribute(iAttributeLabel,oStringValue)); \
} \
HRESULT __stdcall  ENVTIEName::GetIntUserAttribute(const CATBSTR & iAttributeLabel, CATLONG & oIntValue) \
{ \
return (ENVTIECALL(DELMIAPointFastener,ENVTIETypeLetter,ENVTIELetter)GetIntUserAttribute(iAttributeLabel,oIntValue)); \
} \
HRESULT __stdcall  ENVTIEName::GetDoubleUserAttribute(const CATBSTR & iAttributeLabel, double & oDoubleValue) \
{ \
return (ENVTIECALL(DELMIAPointFastener,ENVTIETypeLetter,ENVTIELetter)GetDoubleUserAttribute(iAttributeLabel,oDoubleValue)); \
} \
HRESULT __stdcall  ENVTIEName::GetParts(CATSafeArrayVariant & oListOfParts) \
{ \
return (ENVTIECALL(DELMIAPointFastener,ENVTIETypeLetter,ENVTIELetter)GetParts(oListOfParts)); \
} \
HRESULT __stdcall  ENVTIEName::AllPartsLoaded(CAT_VARIANT_BOOL & oAllPartsLoaded) \
{ \
return (ENVTIECALL(DELMIAPointFastener,ENVTIETypeLetter,ENVTIELetter)AllPartsLoaded(oAllPartsLoaded)); \
} \
HRESULT __stdcall  ENVTIEName::NumberOfAssignedProcesses(CATLONG & oNumOfProcesses) \
{ \
return (ENVTIECALL(DELMIAPointFastener,ENVTIETypeLetter,ENVTIELetter)NumberOfAssignedProcesses(oNumOfProcesses)); \
} \
HRESULT __stdcall  ENVTIEName::GetProcess(short index, CATIAActivity *& oProcess) \
{ \
return (ENVTIECALL(DELMIAPointFastener,ENVTIETypeLetter,ENVTIELetter)GetProcess(index,oProcess)); \
} \
HRESULT __stdcall  ENVTIEName::GetPart(short index, CATIAProduct *& oProduct) \
{ \
return (ENVTIECALL(DELMIAPointFastener,ENVTIETypeLetter,ENVTIELetter)GetPart(index,oProduct)); \
} \
HRESULT __stdcall  ENVTIEName::SetStringUserAttribute(const CATBSTR & iAttributeLabel, const CATBSTR & iStringValue) \
{ \
return (ENVTIECALL(DELMIAPointFastener,ENVTIETypeLetter,ENVTIELetter)SetStringUserAttribute(iAttributeLabel,iStringValue)); \
} \
HRESULT __stdcall  ENVTIEName::SetIntUserAttribute(const CATBSTR & iAttributeLabel, CATLONG iIntValue) \
{ \
return (ENVTIECALL(DELMIAPointFastener,ENVTIETypeLetter,ENVTIELetter)SetIntUserAttribute(iAttributeLabel,iIntValue)); \
} \
HRESULT __stdcall  ENVTIEName::SetDoubleUserAttribute(const CATBSTR & iAttributeLabel, double iDoubleValue) \
{ \
return (ENVTIECALL(DELMIAPointFastener,ENVTIETypeLetter,ENVTIELetter)SetDoubleUserAttribute(iAttributeLabel,iDoubleValue)); \
} \
HRESULT __stdcall  ENVTIEName::RemoveFromPPR(CAT_VARIANT_BOOL iForceRemoveIfAssigned, DNBPPRRemoveStatus & eStatus) \
{ \
return (ENVTIECALL(DELMIAPointFastener,ENVTIETypeLetter,ENVTIELetter)RemoveFromPPR(iForceRemoveIfAssigned,eStatus)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DELMIAPointFastener,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DELMIAPointFastener,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DELMIAPointFastener,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DELMIAPointFastener,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DELMIAPointFastener,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DELMIAPointFastener(classe)    TIEDELMIAPointFastener##classe


/* Common methods inside a TIE */
#define common_TIE_DELMIAPointFastener(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DELMIAPointFastener, classe) \
 \
 \
CATImplementTIEMethods(DELMIAPointFastener, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DELMIAPointFastener, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DELMIAPointFastener, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DELMIAPointFastener, classe) \
 \
HRESULT __stdcall  TIEDELMIAPointFastener##classe::get_Symbol(CATBSTR & oColor) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&oColor); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Symbol(oColor); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&oColor); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAPointFastener##classe::put_Symbol(const CATBSTR & iColor) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&iColor); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Symbol(iColor); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&iColor); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAPointFastener##classe::get_Style(CATBSTR & oStyle) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&oStyle); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Style(oStyle); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&oStyle); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAPointFastener##classe::put_Style(const CATBSTR & iStyle) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&iStyle); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Style(iStyle); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&iStyle); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAPointFastener##classe::ResetPosition(CAT_VARIANT_BOOL iVal) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&iVal); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ResetPosition(iVal); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&iVal); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAPointFastener##classe::GetTag(CATBaseDispatch *& oTag) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&oTag); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetTag(oTag); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&oTag); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAPointFastener##classe::GetDesignXYZYPR(CATSafeArrayVariant & ioXYZYPR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&ioXYZYPR); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetDesignXYZYPR(ioXYZYPR); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&ioXYZYPR); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAPointFastener##classe::get_ProcessType(CATBSTR & oProcessType) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&oProcessType); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_ProcessType(oProcessType); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&oProcessType); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAPointFastener##classe::get_Color(CATLONG & oColor) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&oColor); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Color(oColor); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&oColor); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAPointFastener##classe::put_Color(CATLONG iColor) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&iColor); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Color(iColor); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&iColor); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAPointFastener##classe::NumberOfJoiningParts(CATLONG & oNumOfParts) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,11,&_Trac2,&oNumOfParts); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->NumberOfJoiningParts(oNumOfParts); \
   ExitAfterCall(this,11,_Trac2,&_ret_arg,&oNumOfParts); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAPointFastener##classe::SetTextIDVisibility(CAT_VARIANT_BOOL iStatus) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,12,&_Trac2,&iStatus); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetTextIDVisibility(iStatus); \
   ExitAfterCall(this,12,_Trac2,&_ret_arg,&iStatus); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAPointFastener##classe::GetStringUserAttribute(const CATBSTR & iAttributeLabel, CATBSTR & oStringValue) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,13,&_Trac2,&iAttributeLabel,&oStringValue); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetStringUserAttribute(iAttributeLabel,oStringValue); \
   ExitAfterCall(this,13,_Trac2,&_ret_arg,&iAttributeLabel,&oStringValue); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAPointFastener##classe::GetIntUserAttribute(const CATBSTR & iAttributeLabel, CATLONG & oIntValue) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,14,&_Trac2,&iAttributeLabel,&oIntValue); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetIntUserAttribute(iAttributeLabel,oIntValue); \
   ExitAfterCall(this,14,_Trac2,&_ret_arg,&iAttributeLabel,&oIntValue); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAPointFastener##classe::GetDoubleUserAttribute(const CATBSTR & iAttributeLabel, double & oDoubleValue) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,15,&_Trac2,&iAttributeLabel,&oDoubleValue); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetDoubleUserAttribute(iAttributeLabel,oDoubleValue); \
   ExitAfterCall(this,15,_Trac2,&_ret_arg,&iAttributeLabel,&oDoubleValue); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAPointFastener##classe::GetParts(CATSafeArrayVariant & oListOfParts) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,16,&_Trac2,&oListOfParts); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetParts(oListOfParts); \
   ExitAfterCall(this,16,_Trac2,&_ret_arg,&oListOfParts); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAPointFastener##classe::AllPartsLoaded(CAT_VARIANT_BOOL & oAllPartsLoaded) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,17,&_Trac2,&oAllPartsLoaded); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->AllPartsLoaded(oAllPartsLoaded); \
   ExitAfterCall(this,17,_Trac2,&_ret_arg,&oAllPartsLoaded); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAPointFastener##classe::NumberOfAssignedProcesses(CATLONG & oNumOfProcesses) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,18,&_Trac2,&oNumOfProcesses); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->NumberOfAssignedProcesses(oNumOfProcesses); \
   ExitAfterCall(this,18,_Trac2,&_ret_arg,&oNumOfProcesses); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAPointFastener##classe::GetProcess(short index, CATIAActivity *& oProcess) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,19,&_Trac2,&index,&oProcess); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetProcess(index,oProcess); \
   ExitAfterCall(this,19,_Trac2,&_ret_arg,&index,&oProcess); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAPointFastener##classe::GetPart(short index, CATIAProduct *& oProduct) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,20,&_Trac2,&index,&oProduct); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetPart(index,oProduct); \
   ExitAfterCall(this,20,_Trac2,&_ret_arg,&index,&oProduct); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAPointFastener##classe::SetStringUserAttribute(const CATBSTR & iAttributeLabel, const CATBSTR & iStringValue) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,21,&_Trac2,&iAttributeLabel,&iStringValue); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetStringUserAttribute(iAttributeLabel,iStringValue); \
   ExitAfterCall(this,21,_Trac2,&_ret_arg,&iAttributeLabel,&iStringValue); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAPointFastener##classe::SetIntUserAttribute(const CATBSTR & iAttributeLabel, CATLONG iIntValue) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,22,&_Trac2,&iAttributeLabel,&iIntValue); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetIntUserAttribute(iAttributeLabel,iIntValue); \
   ExitAfterCall(this,22,_Trac2,&_ret_arg,&iAttributeLabel,&iIntValue); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAPointFastener##classe::SetDoubleUserAttribute(const CATBSTR & iAttributeLabel, double iDoubleValue) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,23,&_Trac2,&iAttributeLabel,&iDoubleValue); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetDoubleUserAttribute(iAttributeLabel,iDoubleValue); \
   ExitAfterCall(this,23,_Trac2,&_ret_arg,&iAttributeLabel,&iDoubleValue); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAPointFastener##classe::RemoveFromPPR(CAT_VARIANT_BOOL iForceRemoveIfAssigned, DNBPPRRemoveStatus & eStatus) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,24,&_Trac2,&iForceRemoveIfAssigned,&eStatus); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->RemoveFromPPR(iForceRemoveIfAssigned,eStatus); \
   ExitAfterCall(this,24,_Trac2,&_ret_arg,&iForceRemoveIfAssigned,&eStatus); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAPointFastener##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,25,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,25,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAPointFastener##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,26,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,26,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAPointFastener##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,27,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,27,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAPointFastener##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,28,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,28,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAPointFastener##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,29,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,29,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DELMIAPointFastener(classe) \
 \
 \
declare_TIE_DELMIAPointFastener(classe) \
 \
 \
CATMetaClass * __stdcall TIEDELMIAPointFastener##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DELMIAPointFastener,"DELMIAPointFastener",DELMIAPointFastener::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DELMIAPointFastener(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DELMIAPointFastener, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDELMIAPointFastener##classe(classe::MetaObject(),DELMIAPointFastener::MetaObject(),(void *)CreateTIEDELMIAPointFastener##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DELMIAPointFastener(classe) \
 \
 \
declare_TIE_DELMIAPointFastener(classe) \
 \
 \
CATMetaClass * __stdcall TIEDELMIAPointFastener##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DELMIAPointFastener,"DELMIAPointFastener",DELMIAPointFastener::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DELMIAPointFastener(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DELMIAPointFastener, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDELMIAPointFastener##classe(classe::MetaObject(),DELMIAPointFastener::MetaObject(),(void *)CreateTIEDELMIAPointFastener##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DELMIAPointFastener(classe) TIE_DELMIAPointFastener(classe)
#else
#define BOA_DELMIAPointFastener(classe) CATImplementBOA(DELMIAPointFastener, classe)
#endif

#endif
