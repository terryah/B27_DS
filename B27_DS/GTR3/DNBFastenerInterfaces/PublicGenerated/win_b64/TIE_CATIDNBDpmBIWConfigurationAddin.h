#ifndef __TIE_CATIDNBDpmBIWConfigurationAddin
#define __TIE_CATIDNBDpmBIWConfigurationAddin

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATIDNBDpmBIWConfigurationAddin.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface CATIDNBDpmBIWConfigurationAddin */
#define declare_TIE_CATIDNBDpmBIWConfigurationAddin(classe) \
 \
 \
class TIECATIDNBDpmBIWConfigurationAddin##classe : public CATIDNBDpmBIWConfigurationAddin \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(CATIDNBDpmBIWConfigurationAddin, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual void              CreateCommands () ; \
      virtual CATCmdContainer * CreateToolbars () ; \
};



#define ENVTIEdeclare_CATIDNBDpmBIWConfigurationAddin(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual void              CreateCommands () ; \
virtual CATCmdContainer * CreateToolbars () ; \


#define ENVTIEdefine_CATIDNBDpmBIWConfigurationAddin(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
void               ENVTIEName::CreateCommands ()  \
{ \
 (ENVTIECALL(CATIDNBDpmBIWConfigurationAddin,ENVTIETypeLetter,ENVTIELetter)CreateCommands ()); \
} \
CATCmdContainer *  ENVTIEName::CreateToolbars ()  \
{ \
return (ENVTIECALL(CATIDNBDpmBIWConfigurationAddin,ENVTIETypeLetter,ENVTIELetter)CreateToolbars ()); \
} \


/* Name of the TIE class */
#define class_TIE_CATIDNBDpmBIWConfigurationAddin(classe)    TIECATIDNBDpmBIWConfigurationAddin##classe


/* Common methods inside a TIE */
#define common_TIE_CATIDNBDpmBIWConfigurationAddin(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(CATIDNBDpmBIWConfigurationAddin, classe) \
 \
 \
CATImplementTIEMethods(CATIDNBDpmBIWConfigurationAddin, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(CATIDNBDpmBIWConfigurationAddin, classe, 1) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(CATIDNBDpmBIWConfigurationAddin, classe) \
CATImplementCATBaseUnknownMethodsForTIE(CATIDNBDpmBIWConfigurationAddin, classe) \
 \
void               TIECATIDNBDpmBIWConfigurationAddin##classe::CreateCommands ()  \
{ \
   ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateCommands (); \
} \
CATCmdContainer *  TIECATIDNBDpmBIWConfigurationAddin##classe::CreateToolbars ()  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateToolbars ()); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_CATIDNBDpmBIWConfigurationAddin(classe) \
 \
 \
declare_TIE_CATIDNBDpmBIWConfigurationAddin(classe) \
 \
 \
CATMetaClass * __stdcall TIECATIDNBDpmBIWConfigurationAddin##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_CATIDNBDpmBIWConfigurationAddin,"CATIDNBDpmBIWConfigurationAddin",CATIDNBDpmBIWConfigurationAddin::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_CATIDNBDpmBIWConfigurationAddin(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(CATIDNBDpmBIWConfigurationAddin, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicCATIDNBDpmBIWConfigurationAddin##classe(classe::MetaObject(),CATIDNBDpmBIWConfigurationAddin::MetaObject(),(void *)CreateTIECATIDNBDpmBIWConfigurationAddin##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_CATIDNBDpmBIWConfigurationAddin(classe) \
 \
 \
declare_TIE_CATIDNBDpmBIWConfigurationAddin(classe) \
 \
 \
CATMetaClass * __stdcall TIECATIDNBDpmBIWConfigurationAddin##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_CATIDNBDpmBIWConfigurationAddin,"CATIDNBDpmBIWConfigurationAddin",CATIDNBDpmBIWConfigurationAddin::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_CATIDNBDpmBIWConfigurationAddin(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(CATIDNBDpmBIWConfigurationAddin, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicCATIDNBDpmBIWConfigurationAddin##classe(classe::MetaObject(),CATIDNBDpmBIWConfigurationAddin::MetaObject(),(void *)CreateTIECATIDNBDpmBIWConfigurationAddin##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_CATIDNBDpmBIWConfigurationAddin(classe) TIE_CATIDNBDpmBIWConfigurationAddin(classe)
#else
#define BOA_CATIDNBDpmBIWConfigurationAddin(classe) CATImplementBOA(CATIDNBDpmBIWConfigurationAddin, classe)
#endif

#endif
