#ifndef __TIE_DELMIAFastenerGroup
#define __TIE_DELMIAFastenerGroup

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DELMIAFastenerGroup.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DELMIAFastenerGroup */
#define declare_TIE_DELMIAFastenerGroup(classe) \
 \
 \
class TIEDELMIAFastenerGroup##classe : public DELMIAFastenerGroup \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DELMIAFastenerGroup, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall put_Color(CATLONG iColor); \
      virtual HRESULT __stdcall put_Style(const CATBSTR & iStyle); \
      virtual HRESULT __stdcall put_Symbol(const CATBSTR & iColor); \
      virtual HRESULT __stdcall NumberOfJoiningParts(CATLONG & oNumOfParts); \
      virtual HRESULT __stdcall GetJoinedPartNames(CATSafeArrayVariant & oListOfParts); \
      virtual HRESULT __stdcall GetItems(const CATBSTR & IDName, CATSafeArrayVariant & oListOfEntities); \
      virtual HRESULT __stdcall GetCount(const CATBSTR & IDName, CATLONG & oNumOfWelds); \
      virtual HRESULT __stdcall Hide(); \
      virtual HRESULT __stdcall Show(); \
      virtual HRESULT __stdcall GetPart(short index, CATIAProduct *& oProduct); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DELMIAFastenerGroup(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall put_Color(CATLONG iColor); \
virtual HRESULT __stdcall put_Style(const CATBSTR & iStyle); \
virtual HRESULT __stdcall put_Symbol(const CATBSTR & iColor); \
virtual HRESULT __stdcall NumberOfJoiningParts(CATLONG & oNumOfParts); \
virtual HRESULT __stdcall GetJoinedPartNames(CATSafeArrayVariant & oListOfParts); \
virtual HRESULT __stdcall GetItems(const CATBSTR & IDName, CATSafeArrayVariant & oListOfEntities); \
virtual HRESULT __stdcall GetCount(const CATBSTR & IDName, CATLONG & oNumOfWelds); \
virtual HRESULT __stdcall Hide(); \
virtual HRESULT __stdcall Show(); \
virtual HRESULT __stdcall GetPart(short index, CATIAProduct *& oProduct); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DELMIAFastenerGroup(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::put_Color(CATLONG iColor) \
{ \
return (ENVTIECALL(DELMIAFastenerGroup,ENVTIETypeLetter,ENVTIELetter)put_Color(iColor)); \
} \
HRESULT __stdcall  ENVTIEName::put_Style(const CATBSTR & iStyle) \
{ \
return (ENVTIECALL(DELMIAFastenerGroup,ENVTIETypeLetter,ENVTIELetter)put_Style(iStyle)); \
} \
HRESULT __stdcall  ENVTIEName::put_Symbol(const CATBSTR & iColor) \
{ \
return (ENVTIECALL(DELMIAFastenerGroup,ENVTIETypeLetter,ENVTIELetter)put_Symbol(iColor)); \
} \
HRESULT __stdcall  ENVTIEName::NumberOfJoiningParts(CATLONG & oNumOfParts) \
{ \
return (ENVTIECALL(DELMIAFastenerGroup,ENVTIETypeLetter,ENVTIELetter)NumberOfJoiningParts(oNumOfParts)); \
} \
HRESULT __stdcall  ENVTIEName::GetJoinedPartNames(CATSafeArrayVariant & oListOfParts) \
{ \
return (ENVTIECALL(DELMIAFastenerGroup,ENVTIETypeLetter,ENVTIELetter)GetJoinedPartNames(oListOfParts)); \
} \
HRESULT __stdcall  ENVTIEName::GetItems(const CATBSTR & IDName, CATSafeArrayVariant & oListOfEntities) \
{ \
return (ENVTIECALL(DELMIAFastenerGroup,ENVTIETypeLetter,ENVTIELetter)GetItems(IDName,oListOfEntities)); \
} \
HRESULT __stdcall  ENVTIEName::GetCount(const CATBSTR & IDName, CATLONG & oNumOfWelds) \
{ \
return (ENVTIECALL(DELMIAFastenerGroup,ENVTIETypeLetter,ENVTIELetter)GetCount(IDName,oNumOfWelds)); \
} \
HRESULT __stdcall  ENVTIEName::Hide() \
{ \
return (ENVTIECALL(DELMIAFastenerGroup,ENVTIETypeLetter,ENVTIELetter)Hide()); \
} \
HRESULT __stdcall  ENVTIEName::Show() \
{ \
return (ENVTIECALL(DELMIAFastenerGroup,ENVTIETypeLetter,ENVTIELetter)Show()); \
} \
HRESULT __stdcall  ENVTIEName::GetPart(short index, CATIAProduct *& oProduct) \
{ \
return (ENVTIECALL(DELMIAFastenerGroup,ENVTIETypeLetter,ENVTIELetter)GetPart(index,oProduct)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DELMIAFastenerGroup,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DELMIAFastenerGroup,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DELMIAFastenerGroup,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DELMIAFastenerGroup,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DELMIAFastenerGroup,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DELMIAFastenerGroup(classe)    TIEDELMIAFastenerGroup##classe


/* Common methods inside a TIE */
#define common_TIE_DELMIAFastenerGroup(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DELMIAFastenerGroup, classe) \
 \
 \
CATImplementTIEMethods(DELMIAFastenerGroup, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DELMIAFastenerGroup, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DELMIAFastenerGroup, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DELMIAFastenerGroup, classe) \
 \
HRESULT __stdcall  TIEDELMIAFastenerGroup##classe::put_Color(CATLONG iColor) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&iColor); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Color(iColor); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&iColor); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAFastenerGroup##classe::put_Style(const CATBSTR & iStyle) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&iStyle); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Style(iStyle); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&iStyle); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAFastenerGroup##classe::put_Symbol(const CATBSTR & iColor) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&iColor); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Symbol(iColor); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&iColor); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAFastenerGroup##classe::NumberOfJoiningParts(CATLONG & oNumOfParts) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&oNumOfParts); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->NumberOfJoiningParts(oNumOfParts); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&oNumOfParts); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAFastenerGroup##classe::GetJoinedPartNames(CATSafeArrayVariant & oListOfParts) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&oListOfParts); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetJoinedPartNames(oListOfParts); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&oListOfParts); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAFastenerGroup##classe::GetItems(const CATBSTR & IDName, CATSafeArrayVariant & oListOfEntities) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&IDName,&oListOfEntities); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItems(IDName,oListOfEntities); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&IDName,&oListOfEntities); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAFastenerGroup##classe::GetCount(const CATBSTR & IDName, CATLONG & oNumOfWelds) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&IDName,&oNumOfWelds); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetCount(IDName,oNumOfWelds); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&IDName,&oNumOfWelds); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAFastenerGroup##classe::Hide() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Hide(); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAFastenerGroup##classe::Show() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Show(); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAFastenerGroup##classe::GetPart(short index, CATIAProduct *& oProduct) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&index,&oProduct); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetPart(index,oProduct); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&index,&oProduct); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAFastenerGroup##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,11,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,11,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAFastenerGroup##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,12,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,12,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAFastenerGroup##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,13,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,13,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAFastenerGroup##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,14,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,14,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAFastenerGroup##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,15,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,15,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DELMIAFastenerGroup(classe) \
 \
 \
declare_TIE_DELMIAFastenerGroup(classe) \
 \
 \
CATMetaClass * __stdcall TIEDELMIAFastenerGroup##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DELMIAFastenerGroup,"DELMIAFastenerGroup",DELMIAFastenerGroup::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DELMIAFastenerGroup(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DELMIAFastenerGroup, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDELMIAFastenerGroup##classe(classe::MetaObject(),DELMIAFastenerGroup::MetaObject(),(void *)CreateTIEDELMIAFastenerGroup##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DELMIAFastenerGroup(classe) \
 \
 \
declare_TIE_DELMIAFastenerGroup(classe) \
 \
 \
CATMetaClass * __stdcall TIEDELMIAFastenerGroup##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DELMIAFastenerGroup,"DELMIAFastenerGroup",DELMIAFastenerGroup::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DELMIAFastenerGroup(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DELMIAFastenerGroup, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDELMIAFastenerGroup##classe(classe::MetaObject(),DELMIAFastenerGroup::MetaObject(),(void *)CreateTIEDELMIAFastenerGroup##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DELMIAFastenerGroup(classe) TIE_DELMIAFastenerGroup(classe)
#else
#define BOA_DELMIAFastenerGroup(classe) CATImplementBOA(DELMIAFastenerGroup, classe)
#endif

#endif
