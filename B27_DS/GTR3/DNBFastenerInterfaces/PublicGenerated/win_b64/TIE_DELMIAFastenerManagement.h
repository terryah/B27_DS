#ifndef __TIE_DELMIAFastenerManagement
#define __TIE_DELMIAFastenerManagement

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DELMIAFastenerManagement.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DELMIAFastenerManagement */
#define declare_TIE_DELMIAFastenerManagement(classe) \
 \
 \
class TIEDELMIAFastenerManagement##classe : public DELMIAFastenerManagement \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DELMIAFastenerManagement, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall CreateCurveFastener(const CATBSTR & iName, DELMIAFastenerGroup *& iParentGroupBody, const CATSafeArrayVariant & iListOfJoiningParts, const CATBSTR & iProcessType, short iFasteningContext, const CATSafeArrayVariant & iDesignPointsPositions, const CATBSTR & iFeatureCGRPath, DELMIACurveFastener *& oCurveFastener); \
      virtual HRESULT __stdcall CreatePointFastener(const CATBSTR & iName, DELMIAFastenerGroup *& iParentGroupBody, const CATSafeArrayVariant & iListOfJoiningParts, const CATBSTR & iProcessType, short iFasteningContext, const CATSafeArrayVariant & iDesignPosition, DELMIAPointFastener *& oPointFastener); \
      virtual HRESULT __stdcall CreateFastenerGroup(const CATBSTR & iName, DELMIAFastenerGroup *& iParentGroupBody, const CATSafeArrayVariant & iListOfJoiningParts, DELMIAFastenerGroup *& oFastenerGroup); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DELMIAFastenerManagement(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall CreateCurveFastener(const CATBSTR & iName, DELMIAFastenerGroup *& iParentGroupBody, const CATSafeArrayVariant & iListOfJoiningParts, const CATBSTR & iProcessType, short iFasteningContext, const CATSafeArrayVariant & iDesignPointsPositions, const CATBSTR & iFeatureCGRPath, DELMIACurveFastener *& oCurveFastener); \
virtual HRESULT __stdcall CreatePointFastener(const CATBSTR & iName, DELMIAFastenerGroup *& iParentGroupBody, const CATSafeArrayVariant & iListOfJoiningParts, const CATBSTR & iProcessType, short iFasteningContext, const CATSafeArrayVariant & iDesignPosition, DELMIAPointFastener *& oPointFastener); \
virtual HRESULT __stdcall CreateFastenerGroup(const CATBSTR & iName, DELMIAFastenerGroup *& iParentGroupBody, const CATSafeArrayVariant & iListOfJoiningParts, DELMIAFastenerGroup *& oFastenerGroup); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DELMIAFastenerManagement(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::CreateCurveFastener(const CATBSTR & iName, DELMIAFastenerGroup *& iParentGroupBody, const CATSafeArrayVariant & iListOfJoiningParts, const CATBSTR & iProcessType, short iFasteningContext, const CATSafeArrayVariant & iDesignPointsPositions, const CATBSTR & iFeatureCGRPath, DELMIACurveFastener *& oCurveFastener) \
{ \
return (ENVTIECALL(DELMIAFastenerManagement,ENVTIETypeLetter,ENVTIELetter)CreateCurveFastener(iName,iParentGroupBody,iListOfJoiningParts,iProcessType,iFasteningContext,iDesignPointsPositions,iFeatureCGRPath,oCurveFastener)); \
} \
HRESULT __stdcall  ENVTIEName::CreatePointFastener(const CATBSTR & iName, DELMIAFastenerGroup *& iParentGroupBody, const CATSafeArrayVariant & iListOfJoiningParts, const CATBSTR & iProcessType, short iFasteningContext, const CATSafeArrayVariant & iDesignPosition, DELMIAPointFastener *& oPointFastener) \
{ \
return (ENVTIECALL(DELMIAFastenerManagement,ENVTIETypeLetter,ENVTIELetter)CreatePointFastener(iName,iParentGroupBody,iListOfJoiningParts,iProcessType,iFasteningContext,iDesignPosition,oPointFastener)); \
} \
HRESULT __stdcall  ENVTIEName::CreateFastenerGroup(const CATBSTR & iName, DELMIAFastenerGroup *& iParentGroupBody, const CATSafeArrayVariant & iListOfJoiningParts, DELMIAFastenerGroup *& oFastenerGroup) \
{ \
return (ENVTIECALL(DELMIAFastenerManagement,ENVTIETypeLetter,ENVTIELetter)CreateFastenerGroup(iName,iParentGroupBody,iListOfJoiningParts,oFastenerGroup)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DELMIAFastenerManagement,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DELMIAFastenerManagement,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DELMIAFastenerManagement,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DELMIAFastenerManagement,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DELMIAFastenerManagement,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DELMIAFastenerManagement(classe)    TIEDELMIAFastenerManagement##classe


/* Common methods inside a TIE */
#define common_TIE_DELMIAFastenerManagement(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DELMIAFastenerManagement, classe) \
 \
 \
CATImplementTIEMethods(DELMIAFastenerManagement, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DELMIAFastenerManagement, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DELMIAFastenerManagement, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DELMIAFastenerManagement, classe) \
 \
HRESULT __stdcall  TIEDELMIAFastenerManagement##classe::CreateCurveFastener(const CATBSTR & iName, DELMIAFastenerGroup *& iParentGroupBody, const CATSafeArrayVariant & iListOfJoiningParts, const CATBSTR & iProcessType, short iFasteningContext, const CATSafeArrayVariant & iDesignPointsPositions, const CATBSTR & iFeatureCGRPath, DELMIACurveFastener *& oCurveFastener) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&iName,&iParentGroupBody,&iListOfJoiningParts,&iProcessType,&iFasteningContext,&iDesignPointsPositions,&iFeatureCGRPath,&oCurveFastener); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateCurveFastener(iName,iParentGroupBody,iListOfJoiningParts,iProcessType,iFasteningContext,iDesignPointsPositions,iFeatureCGRPath,oCurveFastener); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&iName,&iParentGroupBody,&iListOfJoiningParts,&iProcessType,&iFasteningContext,&iDesignPointsPositions,&iFeatureCGRPath,&oCurveFastener); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAFastenerManagement##classe::CreatePointFastener(const CATBSTR & iName, DELMIAFastenerGroup *& iParentGroupBody, const CATSafeArrayVariant & iListOfJoiningParts, const CATBSTR & iProcessType, short iFasteningContext, const CATSafeArrayVariant & iDesignPosition, DELMIAPointFastener *& oPointFastener) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&iName,&iParentGroupBody,&iListOfJoiningParts,&iProcessType,&iFasteningContext,&iDesignPosition,&oPointFastener); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreatePointFastener(iName,iParentGroupBody,iListOfJoiningParts,iProcessType,iFasteningContext,iDesignPosition,oPointFastener); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&iName,&iParentGroupBody,&iListOfJoiningParts,&iProcessType,&iFasteningContext,&iDesignPosition,&oPointFastener); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAFastenerManagement##classe::CreateFastenerGroup(const CATBSTR & iName, DELMIAFastenerGroup *& iParentGroupBody, const CATSafeArrayVariant & iListOfJoiningParts, DELMIAFastenerGroup *& oFastenerGroup) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&iName,&iParentGroupBody,&iListOfJoiningParts,&oFastenerGroup); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateFastenerGroup(iName,iParentGroupBody,iListOfJoiningParts,oFastenerGroup); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&iName,&iParentGroupBody,&iListOfJoiningParts,&oFastenerGroup); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAFastenerManagement##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAFastenerManagement##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAFastenerManagement##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAFastenerManagement##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAFastenerManagement##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DELMIAFastenerManagement(classe) \
 \
 \
declare_TIE_DELMIAFastenerManagement(classe) \
 \
 \
CATMetaClass * __stdcall TIEDELMIAFastenerManagement##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DELMIAFastenerManagement,"DELMIAFastenerManagement",DELMIAFastenerManagement::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DELMIAFastenerManagement(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DELMIAFastenerManagement, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDELMIAFastenerManagement##classe(classe::MetaObject(),DELMIAFastenerManagement::MetaObject(),(void *)CreateTIEDELMIAFastenerManagement##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DELMIAFastenerManagement(classe) \
 \
 \
declare_TIE_DELMIAFastenerManagement(classe) \
 \
 \
CATMetaClass * __stdcall TIEDELMIAFastenerManagement##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DELMIAFastenerManagement,"DELMIAFastenerManagement",DELMIAFastenerManagement::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DELMIAFastenerManagement(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DELMIAFastenerManagement, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDELMIAFastenerManagement##classe(classe::MetaObject(),DELMIAFastenerManagement::MetaObject(),(void *)CreateTIEDELMIAFastenerManagement##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DELMIAFastenerManagement(classe) TIE_DELMIAFastenerManagement(classe)
#else
#define BOA_DELMIAFastenerManagement(classe) CATImplementBOA(DELMIAFastenerManagement, classe)
#endif

#endif
