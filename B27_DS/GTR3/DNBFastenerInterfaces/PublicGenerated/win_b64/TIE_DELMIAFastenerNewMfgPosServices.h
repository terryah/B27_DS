#ifndef __TIE_DELMIAFastenerNewMfgPosServices
#define __TIE_DELMIAFastenerNewMfgPosServices

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DELMIAFastenerNewMfgPosServices.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DELMIAFastenerNewMfgPosServices */
#define declare_TIE_DELMIAFastenerNewMfgPosServices(classe) \
 \
 \
class TIEDELMIAFastenerNewMfgPosServices##classe : public DELMIAFastenerNewMfgPosServices \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DELMIAFastenerNewMfgPosServices, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall SetFlag(DELMIAFastener * iFastener); \
      virtual HRESULT __stdcall SetFlagOnFasteners(const CATSafeArrayVariant & iFastenerArray); \
      virtual HRESULT __stdcall UnsetFlag(DELMIAFastener * iFastener); \
      virtual HRESULT __stdcall UnsetFlagOnFasteners(const CATSafeArrayVariant & iFastenerArray); \
      virtual HRESULT __stdcall GetNumberOfFlaggedFasteners(CATLONG & oNbFasteners); \
      virtual HRESULT __stdcall GetFlaggedFasteners(CATSafeArrayVariant & oFastenerArray); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DELMIAFastenerNewMfgPosServices(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall SetFlag(DELMIAFastener * iFastener); \
virtual HRESULT __stdcall SetFlagOnFasteners(const CATSafeArrayVariant & iFastenerArray); \
virtual HRESULT __stdcall UnsetFlag(DELMIAFastener * iFastener); \
virtual HRESULT __stdcall UnsetFlagOnFasteners(const CATSafeArrayVariant & iFastenerArray); \
virtual HRESULT __stdcall GetNumberOfFlaggedFasteners(CATLONG & oNbFasteners); \
virtual HRESULT __stdcall GetFlaggedFasteners(CATSafeArrayVariant & oFastenerArray); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DELMIAFastenerNewMfgPosServices(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::SetFlag(DELMIAFastener * iFastener) \
{ \
return (ENVTIECALL(DELMIAFastenerNewMfgPosServices,ENVTIETypeLetter,ENVTIELetter)SetFlag(iFastener)); \
} \
HRESULT __stdcall  ENVTIEName::SetFlagOnFasteners(const CATSafeArrayVariant & iFastenerArray) \
{ \
return (ENVTIECALL(DELMIAFastenerNewMfgPosServices,ENVTIETypeLetter,ENVTIELetter)SetFlagOnFasteners(iFastenerArray)); \
} \
HRESULT __stdcall  ENVTIEName::UnsetFlag(DELMIAFastener * iFastener) \
{ \
return (ENVTIECALL(DELMIAFastenerNewMfgPosServices,ENVTIETypeLetter,ENVTIELetter)UnsetFlag(iFastener)); \
} \
HRESULT __stdcall  ENVTIEName::UnsetFlagOnFasteners(const CATSafeArrayVariant & iFastenerArray) \
{ \
return (ENVTIECALL(DELMIAFastenerNewMfgPosServices,ENVTIETypeLetter,ENVTIELetter)UnsetFlagOnFasteners(iFastenerArray)); \
} \
HRESULT __stdcall  ENVTIEName::GetNumberOfFlaggedFasteners(CATLONG & oNbFasteners) \
{ \
return (ENVTIECALL(DELMIAFastenerNewMfgPosServices,ENVTIETypeLetter,ENVTIELetter)GetNumberOfFlaggedFasteners(oNbFasteners)); \
} \
HRESULT __stdcall  ENVTIEName::GetFlaggedFasteners(CATSafeArrayVariant & oFastenerArray) \
{ \
return (ENVTIECALL(DELMIAFastenerNewMfgPosServices,ENVTIETypeLetter,ENVTIELetter)GetFlaggedFasteners(oFastenerArray)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DELMIAFastenerNewMfgPosServices,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DELMIAFastenerNewMfgPosServices,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DELMIAFastenerNewMfgPosServices,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DELMIAFastenerNewMfgPosServices,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DELMIAFastenerNewMfgPosServices,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DELMIAFastenerNewMfgPosServices(classe)    TIEDELMIAFastenerNewMfgPosServices##classe


/* Common methods inside a TIE */
#define common_TIE_DELMIAFastenerNewMfgPosServices(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DELMIAFastenerNewMfgPosServices, classe) \
 \
 \
CATImplementTIEMethods(DELMIAFastenerNewMfgPosServices, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DELMIAFastenerNewMfgPosServices, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DELMIAFastenerNewMfgPosServices, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DELMIAFastenerNewMfgPosServices, classe) \
 \
HRESULT __stdcall  TIEDELMIAFastenerNewMfgPosServices##classe::SetFlag(DELMIAFastener * iFastener) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&iFastener); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetFlag(iFastener); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&iFastener); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAFastenerNewMfgPosServices##classe::SetFlagOnFasteners(const CATSafeArrayVariant & iFastenerArray) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&iFastenerArray); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetFlagOnFasteners(iFastenerArray); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&iFastenerArray); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAFastenerNewMfgPosServices##classe::UnsetFlag(DELMIAFastener * iFastener) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&iFastener); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->UnsetFlag(iFastener); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&iFastener); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAFastenerNewMfgPosServices##classe::UnsetFlagOnFasteners(const CATSafeArrayVariant & iFastenerArray) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&iFastenerArray); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->UnsetFlagOnFasteners(iFastenerArray); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&iFastenerArray); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAFastenerNewMfgPosServices##classe::GetNumberOfFlaggedFasteners(CATLONG & oNbFasteners) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&oNbFasteners); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetNumberOfFlaggedFasteners(oNbFasteners); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&oNbFasteners); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAFastenerNewMfgPosServices##classe::GetFlaggedFasteners(CATSafeArrayVariant & oFastenerArray) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&oFastenerArray); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetFlaggedFasteners(oFastenerArray); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&oFastenerArray); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAFastenerNewMfgPosServices##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAFastenerNewMfgPosServices##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAFastenerNewMfgPosServices##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAFastenerNewMfgPosServices##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAFastenerNewMfgPosServices##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,11,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,11,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DELMIAFastenerNewMfgPosServices(classe) \
 \
 \
declare_TIE_DELMIAFastenerNewMfgPosServices(classe) \
 \
 \
CATMetaClass * __stdcall TIEDELMIAFastenerNewMfgPosServices##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DELMIAFastenerNewMfgPosServices,"DELMIAFastenerNewMfgPosServices",DELMIAFastenerNewMfgPosServices::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DELMIAFastenerNewMfgPosServices(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DELMIAFastenerNewMfgPosServices, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDELMIAFastenerNewMfgPosServices##classe(classe::MetaObject(),DELMIAFastenerNewMfgPosServices::MetaObject(),(void *)CreateTIEDELMIAFastenerNewMfgPosServices##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DELMIAFastenerNewMfgPosServices(classe) \
 \
 \
declare_TIE_DELMIAFastenerNewMfgPosServices(classe) \
 \
 \
CATMetaClass * __stdcall TIEDELMIAFastenerNewMfgPosServices##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DELMIAFastenerNewMfgPosServices,"DELMIAFastenerNewMfgPosServices",DELMIAFastenerNewMfgPosServices::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DELMIAFastenerNewMfgPosServices(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DELMIAFastenerNewMfgPosServices, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDELMIAFastenerNewMfgPosServices##classe(classe::MetaObject(),DELMIAFastenerNewMfgPosServices::MetaObject(),(void *)CreateTIEDELMIAFastenerNewMfgPosServices##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DELMIAFastenerNewMfgPosServices(classe) TIE_DELMIAFastenerNewMfgPosServices(classe)
#else
#define BOA_DELMIAFastenerNewMfgPosServices(classe) CATImplementBOA(DELMIAFastenerNewMfgPosServices, classe)
#endif

#endif
