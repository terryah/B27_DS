#ifndef __TIE_DELMIAFastenerItemServices
#define __TIE_DELMIAFastenerItemServices

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DELMIAFastenerItemServices.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DELMIAFastenerItemServices */
#define declare_TIE_DELMIAFastenerItemServices(classe) \
 \
 \
class TIEDELMIAFastenerItemServices##classe : public DELMIAFastenerItemServices \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DELMIAFastenerItemServices, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall AssignFastenerToProcess(DELMIAFastener * iFastener, CATIAActivity * iProcess, DNBAssignStatus & eStatus); \
      virtual HRESULT __stdcall AssignFastenerToResource(DELMIAFastener * iFastener, CATIAResource * iResource, DNBAssignStatus & eStatus); \
      virtual HRESULT __stdcall RemoveFastenerFromProcess(DELMIAFastener * iFastener, CATIAActivity * iProcess); \
      virtual HRESULT __stdcall RemoveFastenerFromResource(DELMIAFastener * iFastener, CATIAResource * iResource); \
      virtual HRESULT __stdcall GetFastenersFromProcess(CATIAActivity * iOperation, DELMIAFasteners *& iFasteners); \
      virtual HRESULT __stdcall GetFastenersFromResource(CATIAResource * iResource, DELMIAFasteners *& iFasteners); \
      virtual HRESULT __stdcall GetNumberOfFasteners(const CATSafeArrayVariant & iProducts, CAT_VARIANT_BOOL bJoiningFasteners, CATLONG & oNbFasteners); \
      virtual HRESULT __stdcall GetFasteners(const CATSafeArrayVariant & iProducts, CAT_VARIANT_BOOL bJoiningFasteners, CATSafeArrayVariant & oFastenerArray); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DELMIAFastenerItemServices(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall AssignFastenerToProcess(DELMIAFastener * iFastener, CATIAActivity * iProcess, DNBAssignStatus & eStatus); \
virtual HRESULT __stdcall AssignFastenerToResource(DELMIAFastener * iFastener, CATIAResource * iResource, DNBAssignStatus & eStatus); \
virtual HRESULT __stdcall RemoveFastenerFromProcess(DELMIAFastener * iFastener, CATIAActivity * iProcess); \
virtual HRESULT __stdcall RemoveFastenerFromResource(DELMIAFastener * iFastener, CATIAResource * iResource); \
virtual HRESULT __stdcall GetFastenersFromProcess(CATIAActivity * iOperation, DELMIAFasteners *& iFasteners); \
virtual HRESULT __stdcall GetFastenersFromResource(CATIAResource * iResource, DELMIAFasteners *& iFasteners); \
virtual HRESULT __stdcall GetNumberOfFasteners(const CATSafeArrayVariant & iProducts, CAT_VARIANT_BOOL bJoiningFasteners, CATLONG & oNbFasteners); \
virtual HRESULT __stdcall GetFasteners(const CATSafeArrayVariant & iProducts, CAT_VARIANT_BOOL bJoiningFasteners, CATSafeArrayVariant & oFastenerArray); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DELMIAFastenerItemServices(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::AssignFastenerToProcess(DELMIAFastener * iFastener, CATIAActivity * iProcess, DNBAssignStatus & eStatus) \
{ \
return (ENVTIECALL(DELMIAFastenerItemServices,ENVTIETypeLetter,ENVTIELetter)AssignFastenerToProcess(iFastener,iProcess,eStatus)); \
} \
HRESULT __stdcall  ENVTIEName::AssignFastenerToResource(DELMIAFastener * iFastener, CATIAResource * iResource, DNBAssignStatus & eStatus) \
{ \
return (ENVTIECALL(DELMIAFastenerItemServices,ENVTIETypeLetter,ENVTIELetter)AssignFastenerToResource(iFastener,iResource,eStatus)); \
} \
HRESULT __stdcall  ENVTIEName::RemoveFastenerFromProcess(DELMIAFastener * iFastener, CATIAActivity * iProcess) \
{ \
return (ENVTIECALL(DELMIAFastenerItemServices,ENVTIETypeLetter,ENVTIELetter)RemoveFastenerFromProcess(iFastener,iProcess)); \
} \
HRESULT __stdcall  ENVTIEName::RemoveFastenerFromResource(DELMIAFastener * iFastener, CATIAResource * iResource) \
{ \
return (ENVTIECALL(DELMIAFastenerItemServices,ENVTIETypeLetter,ENVTIELetter)RemoveFastenerFromResource(iFastener,iResource)); \
} \
HRESULT __stdcall  ENVTIEName::GetFastenersFromProcess(CATIAActivity * iOperation, DELMIAFasteners *& iFasteners) \
{ \
return (ENVTIECALL(DELMIAFastenerItemServices,ENVTIETypeLetter,ENVTIELetter)GetFastenersFromProcess(iOperation,iFasteners)); \
} \
HRESULT __stdcall  ENVTIEName::GetFastenersFromResource(CATIAResource * iResource, DELMIAFasteners *& iFasteners) \
{ \
return (ENVTIECALL(DELMIAFastenerItemServices,ENVTIETypeLetter,ENVTIELetter)GetFastenersFromResource(iResource,iFasteners)); \
} \
HRESULT __stdcall  ENVTIEName::GetNumberOfFasteners(const CATSafeArrayVariant & iProducts, CAT_VARIANT_BOOL bJoiningFasteners, CATLONG & oNbFasteners) \
{ \
return (ENVTIECALL(DELMIAFastenerItemServices,ENVTIETypeLetter,ENVTIELetter)GetNumberOfFasteners(iProducts,bJoiningFasteners,oNbFasteners)); \
} \
HRESULT __stdcall  ENVTIEName::GetFasteners(const CATSafeArrayVariant & iProducts, CAT_VARIANT_BOOL bJoiningFasteners, CATSafeArrayVariant & oFastenerArray) \
{ \
return (ENVTIECALL(DELMIAFastenerItemServices,ENVTIETypeLetter,ENVTIELetter)GetFasteners(iProducts,bJoiningFasteners,oFastenerArray)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DELMIAFastenerItemServices,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DELMIAFastenerItemServices,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DELMIAFastenerItemServices,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DELMIAFastenerItemServices,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DELMIAFastenerItemServices,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DELMIAFastenerItemServices(classe)    TIEDELMIAFastenerItemServices##classe


/* Common methods inside a TIE */
#define common_TIE_DELMIAFastenerItemServices(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DELMIAFastenerItemServices, classe) \
 \
 \
CATImplementTIEMethods(DELMIAFastenerItemServices, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DELMIAFastenerItemServices, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DELMIAFastenerItemServices, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DELMIAFastenerItemServices, classe) \
 \
HRESULT __stdcall  TIEDELMIAFastenerItemServices##classe::AssignFastenerToProcess(DELMIAFastener * iFastener, CATIAActivity * iProcess, DNBAssignStatus & eStatus) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&iFastener,&iProcess,&eStatus); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->AssignFastenerToProcess(iFastener,iProcess,eStatus); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&iFastener,&iProcess,&eStatus); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAFastenerItemServices##classe::AssignFastenerToResource(DELMIAFastener * iFastener, CATIAResource * iResource, DNBAssignStatus & eStatus) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&iFastener,&iResource,&eStatus); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->AssignFastenerToResource(iFastener,iResource,eStatus); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&iFastener,&iResource,&eStatus); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAFastenerItemServices##classe::RemoveFastenerFromProcess(DELMIAFastener * iFastener, CATIAActivity * iProcess) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&iFastener,&iProcess); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->RemoveFastenerFromProcess(iFastener,iProcess); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&iFastener,&iProcess); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAFastenerItemServices##classe::RemoveFastenerFromResource(DELMIAFastener * iFastener, CATIAResource * iResource) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&iFastener,&iResource); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->RemoveFastenerFromResource(iFastener,iResource); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&iFastener,&iResource); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAFastenerItemServices##classe::GetFastenersFromProcess(CATIAActivity * iOperation, DELMIAFasteners *& iFasteners) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&iOperation,&iFasteners); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetFastenersFromProcess(iOperation,iFasteners); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&iOperation,&iFasteners); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAFastenerItemServices##classe::GetFastenersFromResource(CATIAResource * iResource, DELMIAFasteners *& iFasteners) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&iResource,&iFasteners); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetFastenersFromResource(iResource,iFasteners); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&iResource,&iFasteners); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAFastenerItemServices##classe::GetNumberOfFasteners(const CATSafeArrayVariant & iProducts, CAT_VARIANT_BOOL bJoiningFasteners, CATLONG & oNbFasteners) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&iProducts,&bJoiningFasteners,&oNbFasteners); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetNumberOfFasteners(iProducts,bJoiningFasteners,oNbFasteners); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&iProducts,&bJoiningFasteners,&oNbFasteners); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAFastenerItemServices##classe::GetFasteners(const CATSafeArrayVariant & iProducts, CAT_VARIANT_BOOL bJoiningFasteners, CATSafeArrayVariant & oFastenerArray) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&iProducts,&bJoiningFasteners,&oFastenerArray); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetFasteners(iProducts,bJoiningFasteners,oFastenerArray); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&iProducts,&bJoiningFasteners,&oFastenerArray); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAFastenerItemServices##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAFastenerItemServices##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAFastenerItemServices##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,11,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,11,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAFastenerItemServices##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,12,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,12,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAFastenerItemServices##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,13,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,13,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DELMIAFastenerItemServices(classe) \
 \
 \
declare_TIE_DELMIAFastenerItemServices(classe) \
 \
 \
CATMetaClass * __stdcall TIEDELMIAFastenerItemServices##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DELMIAFastenerItemServices,"DELMIAFastenerItemServices",DELMIAFastenerItemServices::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DELMIAFastenerItemServices(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DELMIAFastenerItemServices, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDELMIAFastenerItemServices##classe(classe::MetaObject(),DELMIAFastenerItemServices::MetaObject(),(void *)CreateTIEDELMIAFastenerItemServices##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DELMIAFastenerItemServices(classe) \
 \
 \
declare_TIE_DELMIAFastenerItemServices(classe) \
 \
 \
CATMetaClass * __stdcall TIEDELMIAFastenerItemServices##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DELMIAFastenerItemServices,"DELMIAFastenerItemServices",DELMIAFastenerItemServices::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DELMIAFastenerItemServices(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DELMIAFastenerItemServices, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDELMIAFastenerItemServices##classe(classe::MetaObject(),DELMIAFastenerItemServices::MetaObject(),(void *)CreateTIEDELMIAFastenerItemServices##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DELMIAFastenerItemServices(classe) TIE_DELMIAFastenerItemServices(classe)
#else
#define BOA_DELMIAFastenerItemServices(classe) CATImplementBOA(DELMIAFastenerItemServices, classe)
#endif

#endif
