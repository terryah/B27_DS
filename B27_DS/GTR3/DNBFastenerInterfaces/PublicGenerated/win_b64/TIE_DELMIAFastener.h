#ifndef __TIE_DELMIAFastener
#define __TIE_DELMIAFastener

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DELMIAFastener.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DELMIAFastener */
#define declare_TIE_DELMIAFastener(classe) \
 \
 \
class TIEDELMIAFastener##classe : public DELMIAFastener \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DELMIAFastener, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall get_ProcessType(CATBSTR & oProcessType); \
      virtual HRESULT __stdcall get_Color(CATLONG & oColor); \
      virtual HRESULT __stdcall put_Color(CATLONG iColor); \
      virtual HRESULT __stdcall NumberOfJoiningParts(CATLONG & oNumOfParts); \
      virtual HRESULT __stdcall SetTextIDVisibility(CAT_VARIANT_BOOL iStatus); \
      virtual HRESULT __stdcall GetStringUserAttribute(const CATBSTR & iAttributeLabel, CATBSTR & oStringValue); \
      virtual HRESULT __stdcall GetIntUserAttribute(const CATBSTR & iAttributeLabel, CATLONG & oIntValue); \
      virtual HRESULT __stdcall GetDoubleUserAttribute(const CATBSTR & iAttributeLabel, double & oDoubleValue); \
      virtual HRESULT __stdcall GetParts(CATSafeArrayVariant & oListOfParts); \
      virtual HRESULT __stdcall AllPartsLoaded(CAT_VARIANT_BOOL & oAllPartsLoaded); \
      virtual HRESULT __stdcall NumberOfAssignedProcesses(CATLONG & oNumOfProcesses); \
      virtual HRESULT __stdcall GetProcess(short index, CATIAActivity *& oProcess); \
      virtual HRESULT __stdcall GetPart(short index, CATIAProduct *& oProduct); \
      virtual HRESULT __stdcall SetStringUserAttribute(const CATBSTR & iAttributeLabel, const CATBSTR & iStringValue); \
      virtual HRESULT __stdcall SetIntUserAttribute(const CATBSTR & iAttributeLabel, CATLONG iIntValue); \
      virtual HRESULT __stdcall SetDoubleUserAttribute(const CATBSTR & iAttributeLabel, double iDoubleValue); \
      virtual HRESULT __stdcall RemoveFromPPR(CAT_VARIANT_BOOL iForceRemoveIfAssigned, DNBPPRRemoveStatus & eStatus); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DELMIAFastener(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall get_ProcessType(CATBSTR & oProcessType); \
virtual HRESULT __stdcall get_Color(CATLONG & oColor); \
virtual HRESULT __stdcall put_Color(CATLONG iColor); \
virtual HRESULT __stdcall NumberOfJoiningParts(CATLONG & oNumOfParts); \
virtual HRESULT __stdcall SetTextIDVisibility(CAT_VARIANT_BOOL iStatus); \
virtual HRESULT __stdcall GetStringUserAttribute(const CATBSTR & iAttributeLabel, CATBSTR & oStringValue); \
virtual HRESULT __stdcall GetIntUserAttribute(const CATBSTR & iAttributeLabel, CATLONG & oIntValue); \
virtual HRESULT __stdcall GetDoubleUserAttribute(const CATBSTR & iAttributeLabel, double & oDoubleValue); \
virtual HRESULT __stdcall GetParts(CATSafeArrayVariant & oListOfParts); \
virtual HRESULT __stdcall AllPartsLoaded(CAT_VARIANT_BOOL & oAllPartsLoaded); \
virtual HRESULT __stdcall NumberOfAssignedProcesses(CATLONG & oNumOfProcesses); \
virtual HRESULT __stdcall GetProcess(short index, CATIAActivity *& oProcess); \
virtual HRESULT __stdcall GetPart(short index, CATIAProduct *& oProduct); \
virtual HRESULT __stdcall SetStringUserAttribute(const CATBSTR & iAttributeLabel, const CATBSTR & iStringValue); \
virtual HRESULT __stdcall SetIntUserAttribute(const CATBSTR & iAttributeLabel, CATLONG iIntValue); \
virtual HRESULT __stdcall SetDoubleUserAttribute(const CATBSTR & iAttributeLabel, double iDoubleValue); \
virtual HRESULT __stdcall RemoveFromPPR(CAT_VARIANT_BOOL iForceRemoveIfAssigned, DNBPPRRemoveStatus & eStatus); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DELMIAFastener(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::get_ProcessType(CATBSTR & oProcessType) \
{ \
return (ENVTIECALL(DELMIAFastener,ENVTIETypeLetter,ENVTIELetter)get_ProcessType(oProcessType)); \
} \
HRESULT __stdcall  ENVTIEName::get_Color(CATLONG & oColor) \
{ \
return (ENVTIECALL(DELMIAFastener,ENVTIETypeLetter,ENVTIELetter)get_Color(oColor)); \
} \
HRESULT __stdcall  ENVTIEName::put_Color(CATLONG iColor) \
{ \
return (ENVTIECALL(DELMIAFastener,ENVTIETypeLetter,ENVTIELetter)put_Color(iColor)); \
} \
HRESULT __stdcall  ENVTIEName::NumberOfJoiningParts(CATLONG & oNumOfParts) \
{ \
return (ENVTIECALL(DELMIAFastener,ENVTIETypeLetter,ENVTIELetter)NumberOfJoiningParts(oNumOfParts)); \
} \
HRESULT __stdcall  ENVTIEName::SetTextIDVisibility(CAT_VARIANT_BOOL iStatus) \
{ \
return (ENVTIECALL(DELMIAFastener,ENVTIETypeLetter,ENVTIELetter)SetTextIDVisibility(iStatus)); \
} \
HRESULT __stdcall  ENVTIEName::GetStringUserAttribute(const CATBSTR & iAttributeLabel, CATBSTR & oStringValue) \
{ \
return (ENVTIECALL(DELMIAFastener,ENVTIETypeLetter,ENVTIELetter)GetStringUserAttribute(iAttributeLabel,oStringValue)); \
} \
HRESULT __stdcall  ENVTIEName::GetIntUserAttribute(const CATBSTR & iAttributeLabel, CATLONG & oIntValue) \
{ \
return (ENVTIECALL(DELMIAFastener,ENVTIETypeLetter,ENVTIELetter)GetIntUserAttribute(iAttributeLabel,oIntValue)); \
} \
HRESULT __stdcall  ENVTIEName::GetDoubleUserAttribute(const CATBSTR & iAttributeLabel, double & oDoubleValue) \
{ \
return (ENVTIECALL(DELMIAFastener,ENVTIETypeLetter,ENVTIELetter)GetDoubleUserAttribute(iAttributeLabel,oDoubleValue)); \
} \
HRESULT __stdcall  ENVTIEName::GetParts(CATSafeArrayVariant & oListOfParts) \
{ \
return (ENVTIECALL(DELMIAFastener,ENVTIETypeLetter,ENVTIELetter)GetParts(oListOfParts)); \
} \
HRESULT __stdcall  ENVTIEName::AllPartsLoaded(CAT_VARIANT_BOOL & oAllPartsLoaded) \
{ \
return (ENVTIECALL(DELMIAFastener,ENVTIETypeLetter,ENVTIELetter)AllPartsLoaded(oAllPartsLoaded)); \
} \
HRESULT __stdcall  ENVTIEName::NumberOfAssignedProcesses(CATLONG & oNumOfProcesses) \
{ \
return (ENVTIECALL(DELMIAFastener,ENVTIETypeLetter,ENVTIELetter)NumberOfAssignedProcesses(oNumOfProcesses)); \
} \
HRESULT __stdcall  ENVTIEName::GetProcess(short index, CATIAActivity *& oProcess) \
{ \
return (ENVTIECALL(DELMIAFastener,ENVTIETypeLetter,ENVTIELetter)GetProcess(index,oProcess)); \
} \
HRESULT __stdcall  ENVTIEName::GetPart(short index, CATIAProduct *& oProduct) \
{ \
return (ENVTIECALL(DELMIAFastener,ENVTIETypeLetter,ENVTIELetter)GetPart(index,oProduct)); \
} \
HRESULT __stdcall  ENVTIEName::SetStringUserAttribute(const CATBSTR & iAttributeLabel, const CATBSTR & iStringValue) \
{ \
return (ENVTIECALL(DELMIAFastener,ENVTIETypeLetter,ENVTIELetter)SetStringUserAttribute(iAttributeLabel,iStringValue)); \
} \
HRESULT __stdcall  ENVTIEName::SetIntUserAttribute(const CATBSTR & iAttributeLabel, CATLONG iIntValue) \
{ \
return (ENVTIECALL(DELMIAFastener,ENVTIETypeLetter,ENVTIELetter)SetIntUserAttribute(iAttributeLabel,iIntValue)); \
} \
HRESULT __stdcall  ENVTIEName::SetDoubleUserAttribute(const CATBSTR & iAttributeLabel, double iDoubleValue) \
{ \
return (ENVTIECALL(DELMIAFastener,ENVTIETypeLetter,ENVTIELetter)SetDoubleUserAttribute(iAttributeLabel,iDoubleValue)); \
} \
HRESULT __stdcall  ENVTIEName::RemoveFromPPR(CAT_VARIANT_BOOL iForceRemoveIfAssigned, DNBPPRRemoveStatus & eStatus) \
{ \
return (ENVTIECALL(DELMIAFastener,ENVTIETypeLetter,ENVTIELetter)RemoveFromPPR(iForceRemoveIfAssigned,eStatus)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DELMIAFastener,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DELMIAFastener,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DELMIAFastener,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DELMIAFastener,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DELMIAFastener,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DELMIAFastener(classe)    TIEDELMIAFastener##classe


/* Common methods inside a TIE */
#define common_TIE_DELMIAFastener(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DELMIAFastener, classe) \
 \
 \
CATImplementTIEMethods(DELMIAFastener, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DELMIAFastener, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DELMIAFastener, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DELMIAFastener, classe) \
 \
HRESULT __stdcall  TIEDELMIAFastener##classe::get_ProcessType(CATBSTR & oProcessType) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&oProcessType); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_ProcessType(oProcessType); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&oProcessType); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAFastener##classe::get_Color(CATLONG & oColor) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&oColor); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Color(oColor); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&oColor); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAFastener##classe::put_Color(CATLONG iColor) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&iColor); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Color(iColor); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&iColor); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAFastener##classe::NumberOfJoiningParts(CATLONG & oNumOfParts) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&oNumOfParts); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->NumberOfJoiningParts(oNumOfParts); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&oNumOfParts); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAFastener##classe::SetTextIDVisibility(CAT_VARIANT_BOOL iStatus) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&iStatus); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetTextIDVisibility(iStatus); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&iStatus); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAFastener##classe::GetStringUserAttribute(const CATBSTR & iAttributeLabel, CATBSTR & oStringValue) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&iAttributeLabel,&oStringValue); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetStringUserAttribute(iAttributeLabel,oStringValue); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&iAttributeLabel,&oStringValue); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAFastener##classe::GetIntUserAttribute(const CATBSTR & iAttributeLabel, CATLONG & oIntValue) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&iAttributeLabel,&oIntValue); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetIntUserAttribute(iAttributeLabel,oIntValue); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&iAttributeLabel,&oIntValue); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAFastener##classe::GetDoubleUserAttribute(const CATBSTR & iAttributeLabel, double & oDoubleValue) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&iAttributeLabel,&oDoubleValue); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetDoubleUserAttribute(iAttributeLabel,oDoubleValue); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&iAttributeLabel,&oDoubleValue); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAFastener##classe::GetParts(CATSafeArrayVariant & oListOfParts) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&oListOfParts); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetParts(oListOfParts); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&oListOfParts); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAFastener##classe::AllPartsLoaded(CAT_VARIANT_BOOL & oAllPartsLoaded) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&oAllPartsLoaded); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->AllPartsLoaded(oAllPartsLoaded); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&oAllPartsLoaded); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAFastener##classe::NumberOfAssignedProcesses(CATLONG & oNumOfProcesses) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,11,&_Trac2,&oNumOfProcesses); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->NumberOfAssignedProcesses(oNumOfProcesses); \
   ExitAfterCall(this,11,_Trac2,&_ret_arg,&oNumOfProcesses); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAFastener##classe::GetProcess(short index, CATIAActivity *& oProcess) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,12,&_Trac2,&index,&oProcess); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetProcess(index,oProcess); \
   ExitAfterCall(this,12,_Trac2,&_ret_arg,&index,&oProcess); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAFastener##classe::GetPart(short index, CATIAProduct *& oProduct) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,13,&_Trac2,&index,&oProduct); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetPart(index,oProduct); \
   ExitAfterCall(this,13,_Trac2,&_ret_arg,&index,&oProduct); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAFastener##classe::SetStringUserAttribute(const CATBSTR & iAttributeLabel, const CATBSTR & iStringValue) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,14,&_Trac2,&iAttributeLabel,&iStringValue); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetStringUserAttribute(iAttributeLabel,iStringValue); \
   ExitAfterCall(this,14,_Trac2,&_ret_arg,&iAttributeLabel,&iStringValue); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAFastener##classe::SetIntUserAttribute(const CATBSTR & iAttributeLabel, CATLONG iIntValue) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,15,&_Trac2,&iAttributeLabel,&iIntValue); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetIntUserAttribute(iAttributeLabel,iIntValue); \
   ExitAfterCall(this,15,_Trac2,&_ret_arg,&iAttributeLabel,&iIntValue); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAFastener##classe::SetDoubleUserAttribute(const CATBSTR & iAttributeLabel, double iDoubleValue) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,16,&_Trac2,&iAttributeLabel,&iDoubleValue); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetDoubleUserAttribute(iAttributeLabel,iDoubleValue); \
   ExitAfterCall(this,16,_Trac2,&_ret_arg,&iAttributeLabel,&iDoubleValue); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAFastener##classe::RemoveFromPPR(CAT_VARIANT_BOOL iForceRemoveIfAssigned, DNBPPRRemoveStatus & eStatus) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,17,&_Trac2,&iForceRemoveIfAssigned,&eStatus); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->RemoveFromPPR(iForceRemoveIfAssigned,eStatus); \
   ExitAfterCall(this,17,_Trac2,&_ret_arg,&iForceRemoveIfAssigned,&eStatus); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAFastener##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,18,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,18,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAFastener##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,19,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,19,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAFastener##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,20,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,20,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAFastener##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,21,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,21,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAFastener##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,22,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,22,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DELMIAFastener(classe) \
 \
 \
declare_TIE_DELMIAFastener(classe) \
 \
 \
CATMetaClass * __stdcall TIEDELMIAFastener##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DELMIAFastener,"DELMIAFastener",DELMIAFastener::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DELMIAFastener(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DELMIAFastener, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDELMIAFastener##classe(classe::MetaObject(),DELMIAFastener::MetaObject(),(void *)CreateTIEDELMIAFastener##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DELMIAFastener(classe) \
 \
 \
declare_TIE_DELMIAFastener(classe) \
 \
 \
CATMetaClass * __stdcall TIEDELMIAFastener##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DELMIAFastener,"DELMIAFastener",DELMIAFastener::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DELMIAFastener(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DELMIAFastener, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDELMIAFastener##classe(classe::MetaObject(),DELMIAFastener::MetaObject(),(void *)CreateTIEDELMIAFastener##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DELMIAFastener(classe) TIE_DELMIAFastener(classe)
#else
#define BOA_DELMIAFastener(classe) CATImplementBOA(DELMIAFastener, classe)
#endif

#endif
