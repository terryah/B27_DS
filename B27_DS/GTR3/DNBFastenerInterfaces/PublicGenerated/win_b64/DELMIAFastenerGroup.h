/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef DELMIAFastenerGroup_h
#define DELMIAFastenerGroup_h

#ifndef ExportedByDNBFastenerInterfaces
#ifdef _WINDOWS_SOURCE
#ifdef __DNBFastenerInterfaces
#define ExportedByDNBFastenerInterfaces __declspec(dllexport)
#else
#define ExportedByDNBFastenerInterfaces __declspec(dllimport)
#endif
#else
#define ExportedByDNBFastenerInterfaces
#endif
#endif

#include "CATBSTR.h"
#include "CATIABase.h"
#include "CATSafeArray.h"

class CATIAProduct;

extern ExportedByDNBFastenerInterfaces IID IID_DELMIAFastenerGroup;

class ExportedByDNBFastenerInterfaces DELMIAFastenerGroup : public CATIABase
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall put_Color(CATLONG iColor)=0;

    virtual HRESULT __stdcall put_Style(const CATBSTR & iStyle)=0;

    virtual HRESULT __stdcall put_Symbol(const CATBSTR & iColor)=0;

    virtual HRESULT __stdcall NumberOfJoiningParts(CATLONG & oNumOfParts)=0;

    virtual HRESULT __stdcall GetJoinedPartNames(CATSafeArrayVariant & oListOfParts)=0;

    virtual HRESULT __stdcall GetItems(const CATBSTR & IDName, CATSafeArrayVariant & oListOfEntities)=0;

    virtual HRESULT __stdcall GetCount(const CATBSTR & IDName, CATLONG & oNumOfWelds)=0;

    virtual HRESULT __stdcall Hide()=0;

    virtual HRESULT __stdcall Show()=0;

    virtual HRESULT __stdcall GetPart(short index, CATIAProduct *& oProduct)=0;


};

CATDeclareHandler(DELMIAFastenerGroup, CATIABase);

#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIAAnalyze.h"
#include "CATIACollection.h"
#include "CATIAMove.h"
#include "CATIAParameters.h"
#include "CATIAPosition.h"
#include "CATIAProduct.h"
#include "CATIAPublications.h"
#include "CATVariant.h"
#include "CatFileType.h"
#include "CatProductSource.h"
#include "CatRepType.h"
#include "CatWorkModeType.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
