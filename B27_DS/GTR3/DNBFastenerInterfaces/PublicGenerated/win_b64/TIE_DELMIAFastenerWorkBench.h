#ifndef __TIE_DELMIAFastenerWorkBench
#define __TIE_DELMIAFastenerWorkBench

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DELMIAFastenerWorkBench.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DELMIAFastenerWorkBench */
#define declare_TIE_DELMIAFastenerWorkBench(classe) \
 \
 \
class TIEDELMIAFastenerWorkBench##classe : public DELMIAFastenerWorkBench \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DELMIAFastenerWorkBench, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall GetNumberOfPointFasteners(CATLONG & oNbFasteners); \
      virtual HRESULT __stdcall GetNumberOfCurveFasteners(CATLONG & oNbFasteners); \
      virtual HRESULT __stdcall GetNumberOfFastenerGroups(CATLONG & oNbFasteners); \
      virtual HRESULT __stdcall PointFasteners(CATSafeArrayVariant & oPointFastenerArray); \
      virtual HRESULT __stdcall CurveFasteners(CATSafeArrayVariant & oCurveFastenerArray); \
      virtual HRESULT __stdcall FastenerGroups(CATSafeArrayVariant & oFastenerGroupArray); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DELMIAFastenerWorkBench(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall GetNumberOfPointFasteners(CATLONG & oNbFasteners); \
virtual HRESULT __stdcall GetNumberOfCurveFasteners(CATLONG & oNbFasteners); \
virtual HRESULT __stdcall GetNumberOfFastenerGroups(CATLONG & oNbFasteners); \
virtual HRESULT __stdcall PointFasteners(CATSafeArrayVariant & oPointFastenerArray); \
virtual HRESULT __stdcall CurveFasteners(CATSafeArrayVariant & oCurveFastenerArray); \
virtual HRESULT __stdcall FastenerGroups(CATSafeArrayVariant & oFastenerGroupArray); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DELMIAFastenerWorkBench(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::GetNumberOfPointFasteners(CATLONG & oNbFasteners) \
{ \
return (ENVTIECALL(DELMIAFastenerWorkBench,ENVTIETypeLetter,ENVTIELetter)GetNumberOfPointFasteners(oNbFasteners)); \
} \
HRESULT __stdcall  ENVTIEName::GetNumberOfCurveFasteners(CATLONG & oNbFasteners) \
{ \
return (ENVTIECALL(DELMIAFastenerWorkBench,ENVTIETypeLetter,ENVTIELetter)GetNumberOfCurveFasteners(oNbFasteners)); \
} \
HRESULT __stdcall  ENVTIEName::GetNumberOfFastenerGroups(CATLONG & oNbFasteners) \
{ \
return (ENVTIECALL(DELMIAFastenerWorkBench,ENVTIETypeLetter,ENVTIELetter)GetNumberOfFastenerGroups(oNbFasteners)); \
} \
HRESULT __stdcall  ENVTIEName::PointFasteners(CATSafeArrayVariant & oPointFastenerArray) \
{ \
return (ENVTIECALL(DELMIAFastenerWorkBench,ENVTIETypeLetter,ENVTIELetter)PointFasteners(oPointFastenerArray)); \
} \
HRESULT __stdcall  ENVTIEName::CurveFasteners(CATSafeArrayVariant & oCurveFastenerArray) \
{ \
return (ENVTIECALL(DELMIAFastenerWorkBench,ENVTIETypeLetter,ENVTIELetter)CurveFasteners(oCurveFastenerArray)); \
} \
HRESULT __stdcall  ENVTIEName::FastenerGroups(CATSafeArrayVariant & oFastenerGroupArray) \
{ \
return (ENVTIECALL(DELMIAFastenerWorkBench,ENVTIETypeLetter,ENVTIELetter)FastenerGroups(oFastenerGroupArray)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DELMIAFastenerWorkBench,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DELMIAFastenerWorkBench,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DELMIAFastenerWorkBench,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DELMIAFastenerWorkBench,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DELMIAFastenerWorkBench,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DELMIAFastenerWorkBench(classe)    TIEDELMIAFastenerWorkBench##classe


/* Common methods inside a TIE */
#define common_TIE_DELMIAFastenerWorkBench(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DELMIAFastenerWorkBench, classe) \
 \
 \
CATImplementTIEMethods(DELMIAFastenerWorkBench, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DELMIAFastenerWorkBench, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DELMIAFastenerWorkBench, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DELMIAFastenerWorkBench, classe) \
 \
HRESULT __stdcall  TIEDELMIAFastenerWorkBench##classe::GetNumberOfPointFasteners(CATLONG & oNbFasteners) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&oNbFasteners); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetNumberOfPointFasteners(oNbFasteners); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&oNbFasteners); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAFastenerWorkBench##classe::GetNumberOfCurveFasteners(CATLONG & oNbFasteners) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&oNbFasteners); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetNumberOfCurveFasteners(oNbFasteners); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&oNbFasteners); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAFastenerWorkBench##classe::GetNumberOfFastenerGroups(CATLONG & oNbFasteners) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&oNbFasteners); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetNumberOfFastenerGroups(oNbFasteners); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&oNbFasteners); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAFastenerWorkBench##classe::PointFasteners(CATSafeArrayVariant & oPointFastenerArray) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&oPointFastenerArray); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->PointFasteners(oPointFastenerArray); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&oPointFastenerArray); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAFastenerWorkBench##classe::CurveFasteners(CATSafeArrayVariant & oCurveFastenerArray) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&oCurveFastenerArray); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CurveFasteners(oCurveFastenerArray); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&oCurveFastenerArray); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAFastenerWorkBench##classe::FastenerGroups(CATSafeArrayVariant & oFastenerGroupArray) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&oFastenerGroupArray); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->FastenerGroups(oFastenerGroupArray); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&oFastenerGroupArray); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAFastenerWorkBench##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAFastenerWorkBench##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAFastenerWorkBench##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAFastenerWorkBench##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAFastenerWorkBench##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,11,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,11,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DELMIAFastenerWorkBench(classe) \
 \
 \
declare_TIE_DELMIAFastenerWorkBench(classe) \
 \
 \
CATMetaClass * __stdcall TIEDELMIAFastenerWorkBench##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DELMIAFastenerWorkBench,"DELMIAFastenerWorkBench",DELMIAFastenerWorkBench::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DELMIAFastenerWorkBench(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DELMIAFastenerWorkBench, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDELMIAFastenerWorkBench##classe(classe::MetaObject(),DELMIAFastenerWorkBench::MetaObject(),(void *)CreateTIEDELMIAFastenerWorkBench##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DELMIAFastenerWorkBench(classe) \
 \
 \
declare_TIE_DELMIAFastenerWorkBench(classe) \
 \
 \
CATMetaClass * __stdcall TIEDELMIAFastenerWorkBench##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DELMIAFastenerWorkBench,"DELMIAFastenerWorkBench",DELMIAFastenerWorkBench::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DELMIAFastenerWorkBench(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DELMIAFastenerWorkBench, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDELMIAFastenerWorkBench##classe(classe::MetaObject(),DELMIAFastenerWorkBench::MetaObject(),(void *)CreateTIEDELMIAFastenerWorkBench##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DELMIAFastenerWorkBench(classe) TIE_DELMIAFastenerWorkBench(classe)
#else
#define BOA_DELMIAFastenerWorkBench(classe) CATImplementBOA(DELMIAFastenerWorkBench, classe)
#endif

#endif
