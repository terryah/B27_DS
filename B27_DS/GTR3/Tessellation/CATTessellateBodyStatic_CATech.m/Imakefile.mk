# SHARED LIBRARY
#
BUILT_OBJECT_TYPE = SHARED LIBRARY
#  
LINK_WITH = \
  JS0GROUP \
  JS0FM \
  CATSysTS \
  CATSysAllocator \
  CATMathematics \
  CATMathStream \
  CATGeometricObjects \
  CATGMGeometricInterfaces \
  CATGMModelInterfaces \
  CATCGMGeoMath \
  CATTechTools \
  Topology \
  TessellateCommon \
  TessBodyAdapters \
  CATPolyhedralInterfaces
# 
