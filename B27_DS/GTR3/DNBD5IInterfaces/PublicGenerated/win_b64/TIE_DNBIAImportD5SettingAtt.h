#ifndef __TIE_DNBIAImportD5SettingAtt
#define __TIE_DNBIAImportD5SettingAtt

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIAImportD5SettingAtt.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIAImportD5SettingAtt */
#define declare_TIE_DNBIAImportD5SettingAtt(classe) \
 \
 \
class TIEDNBIAImportD5SettingAtt##classe : public DNBIAImportD5SettingAtt \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIAImportD5SettingAtt, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall get_ImportLibrary(CATBSTR & oImportLibrary); \
      virtual HRESULT __stdcall put_ImportLibrary(const CATBSTR & iImportLibrary); \
      virtual HRESULT __stdcall GetImportLibraryExpanded(CATBSTR & oImportLibrary); \
      virtual HRESULT __stdcall GetImportLibraryInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetImportLibraryLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_ImportConfigFile(CATBSTR & oImportConfigFile); \
      virtual HRESULT __stdcall put_ImportConfigFile(const CATBSTR & iImportConfigFile); \
      virtual HRESULT __stdcall GetImportConfigFileExpanded(CATBSTR & oImportConfigFile); \
      virtual HRESULT __stdcall GetImportConfigFileInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetImportConfigFileLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_ImportPDBCache(CATBSTR & oImportPDBCache); \
      virtual HRESULT __stdcall put_ImportPDBCache(const CATBSTR & iImportPDBCache); \
      virtual HRESULT __stdcall GetImportPDBCacheExpanded(CATBSTR & oImportPDBCache); \
      virtual HRESULT __stdcall GetImportPDBCacheInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetImportPDBCacheLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_ImportRecording(CAT_VARIANT_BOOL & oImportRecording); \
      virtual HRESULT __stdcall put_ImportRecording(CAT_VARIANT_BOOL iImportRecording); \
      virtual HRESULT __stdcall GetImportRecordingInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetImportRecordingLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_ImportUserViews(CAT_VARIANT_BOOL & oImportUserViews); \
      virtual HRESULT __stdcall put_ImportUserViews(CAT_VARIANT_BOOL iImportUserViews); \
      virtual HRESULT __stdcall GetImportUserViewsInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetImportUserViewsLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_ImportAnnotation(CAT_VARIANT_BOOL & oImportAnnotation); \
      virtual HRESULT __stdcall put_ImportAnnotation(CAT_VARIANT_BOOL iImportAnnotation); \
      virtual HRESULT __stdcall GetImportAnnotationInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetImportAnnotationLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_ImportWclMessage(CAT_VARIANT_BOOL & oImportWclMessage); \
      virtual HRESULT __stdcall put_ImportWclMessage(CAT_VARIANT_BOOL iImportWclMessage); \
      virtual HRESULT __stdcall GetImportWclMessageInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetImportWclMessageLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_ImportCollision(CAT_VARIANT_BOOL & oImportCollision); \
      virtual HRESULT __stdcall put_ImportCollision(CAT_VARIANT_BOOL iImportCollision); \
      virtual HRESULT __stdcall GetImportCollisionInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetImportCollisionLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_ImportFloor(CAT_VARIANT_BOOL & oImportFloor); \
      virtual HRESULT __stdcall put_ImportFloor(CAT_VARIANT_BOOL iImportFloor); \
      virtual HRESULT __stdcall GetImportFloorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetImportFloorLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_ImportUserAttr(CAT_VARIANT_BOOL & oImportUserAttr); \
      virtual HRESULT __stdcall put_ImportUserAttr(CAT_VARIANT_BOOL iImportUserAttr); \
      virtual HRESULT __stdcall GetImportUserAttrInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetImportUserAttrLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_ImportEdge(CAT_VARIANT_BOOL & oImportEdge); \
      virtual HRESULT __stdcall put_ImportEdge(CAT_VARIANT_BOOL iImportEdge); \
      virtual HRESULT __stdcall GetImportEdgeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetImportEdgeLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_ImportCoorsys(CAT_VARIANT_BOOL & oImportCoorsys); \
      virtual HRESULT __stdcall put_ImportCoorsys(CAT_VARIANT_BOOL iImportCoorsys); \
      virtual HRESULT __stdcall GetImportCoorsysInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetImportCoorsysLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_ImportToolFrm(CAT_VARIANT_BOOL & oImportToolFrm); \
      virtual HRESULT __stdcall put_ImportToolFrm(CAT_VARIANT_BOOL iImportToolFrm); \
      virtual HRESULT __stdcall GetImportToolFrmInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetImportToolFrmLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_ImportBaseFrm(CAT_VARIANT_BOOL & oImportBaseFrm); \
      virtual HRESULT __stdcall put_ImportBaseFrm(CAT_VARIANT_BOOL iImportBaseFrm); \
      virtual HRESULT __stdcall GetImportBaseFrmInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetImportBaseFrmLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_ImportWclPath(CAT_VARIANT_BOOL & oImportWclPath); \
      virtual HRESULT __stdcall put_ImportWclPath(CAT_VARIANT_BOOL iImportWclPath); \
      virtual HRESULT __stdcall GetImportWclPathInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetImportWclPathLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_VisCoorsys(FrameVisibility & oVisCoorsys); \
      virtual HRESULT __stdcall put_VisCoorsys(FrameVisibility iVisCoorsys); \
      virtual HRESULT __stdcall GetVisCoorsysInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetVisCoorsysLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_VisToolFrm(FrameVisibility & oVisToolFrm); \
      virtual HRESULT __stdcall put_VisToolFrm(FrameVisibility iVisToolFrm); \
      virtual HRESULT __stdcall GetVisToolFrmInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetVisToolFrmLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_VisBaseFrm(FrameVisibility & oVisBaseFrm); \
      virtual HRESULT __stdcall put_VisBaseFrm(FrameVisibility iVisBaseFrm); \
      virtual HRESULT __stdcall GetVisBaseFrmInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetVisBaseFrmLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_VisWclPath(FrameVisibility & oVisWclPath); \
      virtual HRESULT __stdcall put_VisWclPath(FrameVisibility iVisWclPath); \
      virtual HRESULT __stdcall GetVisWclPathInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetVisWclPathLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_TypeCoorsys(CATLONG & oTypeCoorsys); \
      virtual HRESULT __stdcall put_TypeCoorsys(CATLONG iTypeCoorsys); \
      virtual HRESULT __stdcall GetTypeCoorsysInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetTypeCoorsysLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_TypeToolFrm(CATLONG & oTypeToolFrm); \
      virtual HRESULT __stdcall put_TypeToolFrm(CATLONG iTypeToolFrm); \
      virtual HRESULT __stdcall GetTypeToolFrmInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetTypeToolFrmLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_TypeBaseFrm(CATLONG & oTypeBaseFrm); \
      virtual HRESULT __stdcall put_TypeBaseFrm(CATLONG iTypeBaseFrm); \
      virtual HRESULT __stdcall GetTypeBaseFrmInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetTypeBaseFrmLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_TypeWclPath(CATLONG & oTypeWclPath); \
      virtual HRESULT __stdcall put_TypeWclPath(CATLONG iTypeWclPath); \
      virtual HRESULT __stdcall GetTypeWclPathInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetTypeWclPathLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall Commit(); \
      virtual HRESULT __stdcall Rollback(); \
      virtual HRESULT __stdcall ResetToAdminValues(); \
      virtual HRESULT __stdcall ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList); \
      virtual HRESULT __stdcall SaveRepository(); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIAImportD5SettingAtt(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall get_ImportLibrary(CATBSTR & oImportLibrary); \
virtual HRESULT __stdcall put_ImportLibrary(const CATBSTR & iImportLibrary); \
virtual HRESULT __stdcall GetImportLibraryExpanded(CATBSTR & oImportLibrary); \
virtual HRESULT __stdcall GetImportLibraryInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetImportLibraryLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_ImportConfigFile(CATBSTR & oImportConfigFile); \
virtual HRESULT __stdcall put_ImportConfigFile(const CATBSTR & iImportConfigFile); \
virtual HRESULT __stdcall GetImportConfigFileExpanded(CATBSTR & oImportConfigFile); \
virtual HRESULT __stdcall GetImportConfigFileInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetImportConfigFileLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_ImportPDBCache(CATBSTR & oImportPDBCache); \
virtual HRESULT __stdcall put_ImportPDBCache(const CATBSTR & iImportPDBCache); \
virtual HRESULT __stdcall GetImportPDBCacheExpanded(CATBSTR & oImportPDBCache); \
virtual HRESULT __stdcall GetImportPDBCacheInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetImportPDBCacheLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_ImportRecording(CAT_VARIANT_BOOL & oImportRecording); \
virtual HRESULT __stdcall put_ImportRecording(CAT_VARIANT_BOOL iImportRecording); \
virtual HRESULT __stdcall GetImportRecordingInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetImportRecordingLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_ImportUserViews(CAT_VARIANT_BOOL & oImportUserViews); \
virtual HRESULT __stdcall put_ImportUserViews(CAT_VARIANT_BOOL iImportUserViews); \
virtual HRESULT __stdcall GetImportUserViewsInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetImportUserViewsLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_ImportAnnotation(CAT_VARIANT_BOOL & oImportAnnotation); \
virtual HRESULT __stdcall put_ImportAnnotation(CAT_VARIANT_BOOL iImportAnnotation); \
virtual HRESULT __stdcall GetImportAnnotationInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetImportAnnotationLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_ImportWclMessage(CAT_VARIANT_BOOL & oImportWclMessage); \
virtual HRESULT __stdcall put_ImportWclMessage(CAT_VARIANT_BOOL iImportWclMessage); \
virtual HRESULT __stdcall GetImportWclMessageInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetImportWclMessageLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_ImportCollision(CAT_VARIANT_BOOL & oImportCollision); \
virtual HRESULT __stdcall put_ImportCollision(CAT_VARIANT_BOOL iImportCollision); \
virtual HRESULT __stdcall GetImportCollisionInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetImportCollisionLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_ImportFloor(CAT_VARIANT_BOOL & oImportFloor); \
virtual HRESULT __stdcall put_ImportFloor(CAT_VARIANT_BOOL iImportFloor); \
virtual HRESULT __stdcall GetImportFloorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetImportFloorLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_ImportUserAttr(CAT_VARIANT_BOOL & oImportUserAttr); \
virtual HRESULT __stdcall put_ImportUserAttr(CAT_VARIANT_BOOL iImportUserAttr); \
virtual HRESULT __stdcall GetImportUserAttrInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetImportUserAttrLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_ImportEdge(CAT_VARIANT_BOOL & oImportEdge); \
virtual HRESULT __stdcall put_ImportEdge(CAT_VARIANT_BOOL iImportEdge); \
virtual HRESULT __stdcall GetImportEdgeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetImportEdgeLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_ImportCoorsys(CAT_VARIANT_BOOL & oImportCoorsys); \
virtual HRESULT __stdcall put_ImportCoorsys(CAT_VARIANT_BOOL iImportCoorsys); \
virtual HRESULT __stdcall GetImportCoorsysInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetImportCoorsysLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_ImportToolFrm(CAT_VARIANT_BOOL & oImportToolFrm); \
virtual HRESULT __stdcall put_ImportToolFrm(CAT_VARIANT_BOOL iImportToolFrm); \
virtual HRESULT __stdcall GetImportToolFrmInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetImportToolFrmLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_ImportBaseFrm(CAT_VARIANT_BOOL & oImportBaseFrm); \
virtual HRESULT __stdcall put_ImportBaseFrm(CAT_VARIANT_BOOL iImportBaseFrm); \
virtual HRESULT __stdcall GetImportBaseFrmInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetImportBaseFrmLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_ImportWclPath(CAT_VARIANT_BOOL & oImportWclPath); \
virtual HRESULT __stdcall put_ImportWclPath(CAT_VARIANT_BOOL iImportWclPath); \
virtual HRESULT __stdcall GetImportWclPathInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetImportWclPathLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_VisCoorsys(FrameVisibility & oVisCoorsys); \
virtual HRESULT __stdcall put_VisCoorsys(FrameVisibility iVisCoorsys); \
virtual HRESULT __stdcall GetVisCoorsysInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetVisCoorsysLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_VisToolFrm(FrameVisibility & oVisToolFrm); \
virtual HRESULT __stdcall put_VisToolFrm(FrameVisibility iVisToolFrm); \
virtual HRESULT __stdcall GetVisToolFrmInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetVisToolFrmLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_VisBaseFrm(FrameVisibility & oVisBaseFrm); \
virtual HRESULT __stdcall put_VisBaseFrm(FrameVisibility iVisBaseFrm); \
virtual HRESULT __stdcall GetVisBaseFrmInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetVisBaseFrmLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_VisWclPath(FrameVisibility & oVisWclPath); \
virtual HRESULT __stdcall put_VisWclPath(FrameVisibility iVisWclPath); \
virtual HRESULT __stdcall GetVisWclPathInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetVisWclPathLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_TypeCoorsys(CATLONG & oTypeCoorsys); \
virtual HRESULT __stdcall put_TypeCoorsys(CATLONG iTypeCoorsys); \
virtual HRESULT __stdcall GetTypeCoorsysInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetTypeCoorsysLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_TypeToolFrm(CATLONG & oTypeToolFrm); \
virtual HRESULT __stdcall put_TypeToolFrm(CATLONG iTypeToolFrm); \
virtual HRESULT __stdcall GetTypeToolFrmInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetTypeToolFrmLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_TypeBaseFrm(CATLONG & oTypeBaseFrm); \
virtual HRESULT __stdcall put_TypeBaseFrm(CATLONG iTypeBaseFrm); \
virtual HRESULT __stdcall GetTypeBaseFrmInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetTypeBaseFrmLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_TypeWclPath(CATLONG & oTypeWclPath); \
virtual HRESULT __stdcall put_TypeWclPath(CATLONG iTypeWclPath); \
virtual HRESULT __stdcall GetTypeWclPathInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetTypeWclPathLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall Commit(); \
virtual HRESULT __stdcall Rollback(); \
virtual HRESULT __stdcall ResetToAdminValues(); \
virtual HRESULT __stdcall ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList); \
virtual HRESULT __stdcall SaveRepository(); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIAImportD5SettingAtt(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::get_ImportLibrary(CATBSTR & oImportLibrary) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)get_ImportLibrary(oImportLibrary)); \
} \
HRESULT __stdcall  ENVTIEName::put_ImportLibrary(const CATBSTR & iImportLibrary) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)put_ImportLibrary(iImportLibrary)); \
} \
HRESULT __stdcall  ENVTIEName::GetImportLibraryExpanded(CATBSTR & oImportLibrary) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)GetImportLibraryExpanded(oImportLibrary)); \
} \
HRESULT __stdcall  ENVTIEName::GetImportLibraryInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)GetImportLibraryInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetImportLibraryLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)SetImportLibraryLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_ImportConfigFile(CATBSTR & oImportConfigFile) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)get_ImportConfigFile(oImportConfigFile)); \
} \
HRESULT __stdcall  ENVTIEName::put_ImportConfigFile(const CATBSTR & iImportConfigFile) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)put_ImportConfigFile(iImportConfigFile)); \
} \
HRESULT __stdcall  ENVTIEName::GetImportConfigFileExpanded(CATBSTR & oImportConfigFile) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)GetImportConfigFileExpanded(oImportConfigFile)); \
} \
HRESULT __stdcall  ENVTIEName::GetImportConfigFileInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)GetImportConfigFileInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetImportConfigFileLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)SetImportConfigFileLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_ImportPDBCache(CATBSTR & oImportPDBCache) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)get_ImportPDBCache(oImportPDBCache)); \
} \
HRESULT __stdcall  ENVTIEName::put_ImportPDBCache(const CATBSTR & iImportPDBCache) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)put_ImportPDBCache(iImportPDBCache)); \
} \
HRESULT __stdcall  ENVTIEName::GetImportPDBCacheExpanded(CATBSTR & oImportPDBCache) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)GetImportPDBCacheExpanded(oImportPDBCache)); \
} \
HRESULT __stdcall  ENVTIEName::GetImportPDBCacheInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)GetImportPDBCacheInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetImportPDBCacheLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)SetImportPDBCacheLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_ImportRecording(CAT_VARIANT_BOOL & oImportRecording) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)get_ImportRecording(oImportRecording)); \
} \
HRESULT __stdcall  ENVTIEName::put_ImportRecording(CAT_VARIANT_BOOL iImportRecording) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)put_ImportRecording(iImportRecording)); \
} \
HRESULT __stdcall  ENVTIEName::GetImportRecordingInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)GetImportRecordingInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetImportRecordingLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)SetImportRecordingLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_ImportUserViews(CAT_VARIANT_BOOL & oImportUserViews) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)get_ImportUserViews(oImportUserViews)); \
} \
HRESULT __stdcall  ENVTIEName::put_ImportUserViews(CAT_VARIANT_BOOL iImportUserViews) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)put_ImportUserViews(iImportUserViews)); \
} \
HRESULT __stdcall  ENVTIEName::GetImportUserViewsInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)GetImportUserViewsInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetImportUserViewsLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)SetImportUserViewsLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_ImportAnnotation(CAT_VARIANT_BOOL & oImportAnnotation) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)get_ImportAnnotation(oImportAnnotation)); \
} \
HRESULT __stdcall  ENVTIEName::put_ImportAnnotation(CAT_VARIANT_BOOL iImportAnnotation) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)put_ImportAnnotation(iImportAnnotation)); \
} \
HRESULT __stdcall  ENVTIEName::GetImportAnnotationInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)GetImportAnnotationInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetImportAnnotationLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)SetImportAnnotationLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_ImportWclMessage(CAT_VARIANT_BOOL & oImportWclMessage) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)get_ImportWclMessage(oImportWclMessage)); \
} \
HRESULT __stdcall  ENVTIEName::put_ImportWclMessage(CAT_VARIANT_BOOL iImportWclMessage) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)put_ImportWclMessage(iImportWclMessage)); \
} \
HRESULT __stdcall  ENVTIEName::GetImportWclMessageInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)GetImportWclMessageInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetImportWclMessageLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)SetImportWclMessageLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_ImportCollision(CAT_VARIANT_BOOL & oImportCollision) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)get_ImportCollision(oImportCollision)); \
} \
HRESULT __stdcall  ENVTIEName::put_ImportCollision(CAT_VARIANT_BOOL iImportCollision) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)put_ImportCollision(iImportCollision)); \
} \
HRESULT __stdcall  ENVTIEName::GetImportCollisionInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)GetImportCollisionInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetImportCollisionLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)SetImportCollisionLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_ImportFloor(CAT_VARIANT_BOOL & oImportFloor) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)get_ImportFloor(oImportFloor)); \
} \
HRESULT __stdcall  ENVTIEName::put_ImportFloor(CAT_VARIANT_BOOL iImportFloor) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)put_ImportFloor(iImportFloor)); \
} \
HRESULT __stdcall  ENVTIEName::GetImportFloorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)GetImportFloorInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetImportFloorLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)SetImportFloorLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_ImportUserAttr(CAT_VARIANT_BOOL & oImportUserAttr) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)get_ImportUserAttr(oImportUserAttr)); \
} \
HRESULT __stdcall  ENVTIEName::put_ImportUserAttr(CAT_VARIANT_BOOL iImportUserAttr) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)put_ImportUserAttr(iImportUserAttr)); \
} \
HRESULT __stdcall  ENVTIEName::GetImportUserAttrInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)GetImportUserAttrInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetImportUserAttrLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)SetImportUserAttrLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_ImportEdge(CAT_VARIANT_BOOL & oImportEdge) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)get_ImportEdge(oImportEdge)); \
} \
HRESULT __stdcall  ENVTIEName::put_ImportEdge(CAT_VARIANT_BOOL iImportEdge) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)put_ImportEdge(iImportEdge)); \
} \
HRESULT __stdcall  ENVTIEName::GetImportEdgeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)GetImportEdgeInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetImportEdgeLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)SetImportEdgeLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_ImportCoorsys(CAT_VARIANT_BOOL & oImportCoorsys) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)get_ImportCoorsys(oImportCoorsys)); \
} \
HRESULT __stdcall  ENVTIEName::put_ImportCoorsys(CAT_VARIANT_BOOL iImportCoorsys) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)put_ImportCoorsys(iImportCoorsys)); \
} \
HRESULT __stdcall  ENVTIEName::GetImportCoorsysInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)GetImportCoorsysInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetImportCoorsysLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)SetImportCoorsysLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_ImportToolFrm(CAT_VARIANT_BOOL & oImportToolFrm) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)get_ImportToolFrm(oImportToolFrm)); \
} \
HRESULT __stdcall  ENVTIEName::put_ImportToolFrm(CAT_VARIANT_BOOL iImportToolFrm) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)put_ImportToolFrm(iImportToolFrm)); \
} \
HRESULT __stdcall  ENVTIEName::GetImportToolFrmInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)GetImportToolFrmInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetImportToolFrmLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)SetImportToolFrmLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_ImportBaseFrm(CAT_VARIANT_BOOL & oImportBaseFrm) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)get_ImportBaseFrm(oImportBaseFrm)); \
} \
HRESULT __stdcall  ENVTIEName::put_ImportBaseFrm(CAT_VARIANT_BOOL iImportBaseFrm) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)put_ImportBaseFrm(iImportBaseFrm)); \
} \
HRESULT __stdcall  ENVTIEName::GetImportBaseFrmInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)GetImportBaseFrmInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetImportBaseFrmLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)SetImportBaseFrmLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_ImportWclPath(CAT_VARIANT_BOOL & oImportWclPath) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)get_ImportWclPath(oImportWclPath)); \
} \
HRESULT __stdcall  ENVTIEName::put_ImportWclPath(CAT_VARIANT_BOOL iImportWclPath) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)put_ImportWclPath(iImportWclPath)); \
} \
HRESULT __stdcall  ENVTIEName::GetImportWclPathInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)GetImportWclPathInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetImportWclPathLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)SetImportWclPathLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_VisCoorsys(FrameVisibility & oVisCoorsys) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)get_VisCoorsys(oVisCoorsys)); \
} \
HRESULT __stdcall  ENVTIEName::put_VisCoorsys(FrameVisibility iVisCoorsys) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)put_VisCoorsys(iVisCoorsys)); \
} \
HRESULT __stdcall  ENVTIEName::GetVisCoorsysInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)GetVisCoorsysInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetVisCoorsysLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)SetVisCoorsysLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_VisToolFrm(FrameVisibility & oVisToolFrm) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)get_VisToolFrm(oVisToolFrm)); \
} \
HRESULT __stdcall  ENVTIEName::put_VisToolFrm(FrameVisibility iVisToolFrm) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)put_VisToolFrm(iVisToolFrm)); \
} \
HRESULT __stdcall  ENVTIEName::GetVisToolFrmInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)GetVisToolFrmInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetVisToolFrmLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)SetVisToolFrmLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_VisBaseFrm(FrameVisibility & oVisBaseFrm) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)get_VisBaseFrm(oVisBaseFrm)); \
} \
HRESULT __stdcall  ENVTIEName::put_VisBaseFrm(FrameVisibility iVisBaseFrm) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)put_VisBaseFrm(iVisBaseFrm)); \
} \
HRESULT __stdcall  ENVTIEName::GetVisBaseFrmInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)GetVisBaseFrmInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetVisBaseFrmLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)SetVisBaseFrmLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_VisWclPath(FrameVisibility & oVisWclPath) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)get_VisWclPath(oVisWclPath)); \
} \
HRESULT __stdcall  ENVTIEName::put_VisWclPath(FrameVisibility iVisWclPath) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)put_VisWclPath(iVisWclPath)); \
} \
HRESULT __stdcall  ENVTIEName::GetVisWclPathInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)GetVisWclPathInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetVisWclPathLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)SetVisWclPathLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_TypeCoorsys(CATLONG & oTypeCoorsys) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)get_TypeCoorsys(oTypeCoorsys)); \
} \
HRESULT __stdcall  ENVTIEName::put_TypeCoorsys(CATLONG iTypeCoorsys) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)put_TypeCoorsys(iTypeCoorsys)); \
} \
HRESULT __stdcall  ENVTIEName::GetTypeCoorsysInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)GetTypeCoorsysInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetTypeCoorsysLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)SetTypeCoorsysLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_TypeToolFrm(CATLONG & oTypeToolFrm) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)get_TypeToolFrm(oTypeToolFrm)); \
} \
HRESULT __stdcall  ENVTIEName::put_TypeToolFrm(CATLONG iTypeToolFrm) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)put_TypeToolFrm(iTypeToolFrm)); \
} \
HRESULT __stdcall  ENVTIEName::GetTypeToolFrmInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)GetTypeToolFrmInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetTypeToolFrmLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)SetTypeToolFrmLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_TypeBaseFrm(CATLONG & oTypeBaseFrm) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)get_TypeBaseFrm(oTypeBaseFrm)); \
} \
HRESULT __stdcall  ENVTIEName::put_TypeBaseFrm(CATLONG iTypeBaseFrm) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)put_TypeBaseFrm(iTypeBaseFrm)); \
} \
HRESULT __stdcall  ENVTIEName::GetTypeBaseFrmInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)GetTypeBaseFrmInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetTypeBaseFrmLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)SetTypeBaseFrmLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_TypeWclPath(CATLONG & oTypeWclPath) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)get_TypeWclPath(oTypeWclPath)); \
} \
HRESULT __stdcall  ENVTIEName::put_TypeWclPath(CATLONG iTypeWclPath) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)put_TypeWclPath(iTypeWclPath)); \
} \
HRESULT __stdcall  ENVTIEName::GetTypeWclPathInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)GetTypeWclPathInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetTypeWclPathLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)SetTypeWclPathLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::Commit() \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)Commit()); \
} \
HRESULT __stdcall  ENVTIEName::Rollback() \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)Rollback()); \
} \
HRESULT __stdcall  ENVTIEName::ResetToAdminValues() \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)ResetToAdminValues()); \
} \
HRESULT __stdcall  ENVTIEName::ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)ResetToAdminValuesByName(iAttList)); \
} \
HRESULT __stdcall  ENVTIEName::SaveRepository() \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)SaveRepository()); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIAImportD5SettingAtt,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIAImportD5SettingAtt(classe)    TIEDNBIAImportD5SettingAtt##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIAImportD5SettingAtt(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIAImportD5SettingAtt, classe) \
 \
 \
CATImplementTIEMethods(DNBIAImportD5SettingAtt, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIAImportD5SettingAtt, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIAImportD5SettingAtt, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIAImportD5SettingAtt, classe) \
 \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::get_ImportLibrary(CATBSTR & oImportLibrary) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&oImportLibrary); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_ImportLibrary(oImportLibrary); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&oImportLibrary); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::put_ImportLibrary(const CATBSTR & iImportLibrary) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&iImportLibrary); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_ImportLibrary(iImportLibrary); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&iImportLibrary); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::GetImportLibraryExpanded(CATBSTR & oImportLibrary) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&oImportLibrary); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetImportLibraryExpanded(oImportLibrary); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&oImportLibrary); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::GetImportLibraryInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetImportLibraryInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::SetImportLibraryLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetImportLibraryLock(iLocked); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::get_ImportConfigFile(CATBSTR & oImportConfigFile) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&oImportConfigFile); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_ImportConfigFile(oImportConfigFile); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&oImportConfigFile); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::put_ImportConfigFile(const CATBSTR & iImportConfigFile) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&iImportConfigFile); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_ImportConfigFile(iImportConfigFile); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&iImportConfigFile); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::GetImportConfigFileExpanded(CATBSTR & oImportConfigFile) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&oImportConfigFile); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetImportConfigFileExpanded(oImportConfigFile); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&oImportConfigFile); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::GetImportConfigFileInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetImportConfigFileInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::SetImportConfigFileLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetImportConfigFileLock(iLocked); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::get_ImportPDBCache(CATBSTR & oImportPDBCache) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,11,&_Trac2,&oImportPDBCache); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_ImportPDBCache(oImportPDBCache); \
   ExitAfterCall(this,11,_Trac2,&_ret_arg,&oImportPDBCache); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::put_ImportPDBCache(const CATBSTR & iImportPDBCache) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,12,&_Trac2,&iImportPDBCache); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_ImportPDBCache(iImportPDBCache); \
   ExitAfterCall(this,12,_Trac2,&_ret_arg,&iImportPDBCache); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::GetImportPDBCacheExpanded(CATBSTR & oImportPDBCache) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,13,&_Trac2,&oImportPDBCache); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetImportPDBCacheExpanded(oImportPDBCache); \
   ExitAfterCall(this,13,_Trac2,&_ret_arg,&oImportPDBCache); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::GetImportPDBCacheInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,14,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetImportPDBCacheInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,14,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::SetImportPDBCacheLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,15,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetImportPDBCacheLock(iLocked); \
   ExitAfterCall(this,15,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::get_ImportRecording(CAT_VARIANT_BOOL & oImportRecording) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,16,&_Trac2,&oImportRecording); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_ImportRecording(oImportRecording); \
   ExitAfterCall(this,16,_Trac2,&_ret_arg,&oImportRecording); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::put_ImportRecording(CAT_VARIANT_BOOL iImportRecording) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,17,&_Trac2,&iImportRecording); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_ImportRecording(iImportRecording); \
   ExitAfterCall(this,17,_Trac2,&_ret_arg,&iImportRecording); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::GetImportRecordingInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,18,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetImportRecordingInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,18,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::SetImportRecordingLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,19,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetImportRecordingLock(iLocked); \
   ExitAfterCall(this,19,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::get_ImportUserViews(CAT_VARIANT_BOOL & oImportUserViews) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,20,&_Trac2,&oImportUserViews); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_ImportUserViews(oImportUserViews); \
   ExitAfterCall(this,20,_Trac2,&_ret_arg,&oImportUserViews); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::put_ImportUserViews(CAT_VARIANT_BOOL iImportUserViews) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,21,&_Trac2,&iImportUserViews); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_ImportUserViews(iImportUserViews); \
   ExitAfterCall(this,21,_Trac2,&_ret_arg,&iImportUserViews); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::GetImportUserViewsInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,22,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetImportUserViewsInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,22,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::SetImportUserViewsLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,23,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetImportUserViewsLock(iLocked); \
   ExitAfterCall(this,23,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::get_ImportAnnotation(CAT_VARIANT_BOOL & oImportAnnotation) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,24,&_Trac2,&oImportAnnotation); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_ImportAnnotation(oImportAnnotation); \
   ExitAfterCall(this,24,_Trac2,&_ret_arg,&oImportAnnotation); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::put_ImportAnnotation(CAT_VARIANT_BOOL iImportAnnotation) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,25,&_Trac2,&iImportAnnotation); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_ImportAnnotation(iImportAnnotation); \
   ExitAfterCall(this,25,_Trac2,&_ret_arg,&iImportAnnotation); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::GetImportAnnotationInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,26,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetImportAnnotationInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,26,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::SetImportAnnotationLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,27,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetImportAnnotationLock(iLocked); \
   ExitAfterCall(this,27,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::get_ImportWclMessage(CAT_VARIANT_BOOL & oImportWclMessage) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,28,&_Trac2,&oImportWclMessage); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_ImportWclMessage(oImportWclMessage); \
   ExitAfterCall(this,28,_Trac2,&_ret_arg,&oImportWclMessage); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::put_ImportWclMessage(CAT_VARIANT_BOOL iImportWclMessage) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,29,&_Trac2,&iImportWclMessage); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_ImportWclMessage(iImportWclMessage); \
   ExitAfterCall(this,29,_Trac2,&_ret_arg,&iImportWclMessage); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::GetImportWclMessageInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,30,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetImportWclMessageInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,30,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::SetImportWclMessageLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,31,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetImportWclMessageLock(iLocked); \
   ExitAfterCall(this,31,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::get_ImportCollision(CAT_VARIANT_BOOL & oImportCollision) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,32,&_Trac2,&oImportCollision); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_ImportCollision(oImportCollision); \
   ExitAfterCall(this,32,_Trac2,&_ret_arg,&oImportCollision); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::put_ImportCollision(CAT_VARIANT_BOOL iImportCollision) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,33,&_Trac2,&iImportCollision); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_ImportCollision(iImportCollision); \
   ExitAfterCall(this,33,_Trac2,&_ret_arg,&iImportCollision); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::GetImportCollisionInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,34,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetImportCollisionInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,34,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::SetImportCollisionLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,35,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetImportCollisionLock(iLocked); \
   ExitAfterCall(this,35,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::get_ImportFloor(CAT_VARIANT_BOOL & oImportFloor) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,36,&_Trac2,&oImportFloor); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_ImportFloor(oImportFloor); \
   ExitAfterCall(this,36,_Trac2,&_ret_arg,&oImportFloor); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::put_ImportFloor(CAT_VARIANT_BOOL iImportFloor) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,37,&_Trac2,&iImportFloor); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_ImportFloor(iImportFloor); \
   ExitAfterCall(this,37,_Trac2,&_ret_arg,&iImportFloor); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::GetImportFloorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,38,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetImportFloorInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,38,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::SetImportFloorLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,39,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetImportFloorLock(iLocked); \
   ExitAfterCall(this,39,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::get_ImportUserAttr(CAT_VARIANT_BOOL & oImportUserAttr) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,40,&_Trac2,&oImportUserAttr); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_ImportUserAttr(oImportUserAttr); \
   ExitAfterCall(this,40,_Trac2,&_ret_arg,&oImportUserAttr); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::put_ImportUserAttr(CAT_VARIANT_BOOL iImportUserAttr) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,41,&_Trac2,&iImportUserAttr); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_ImportUserAttr(iImportUserAttr); \
   ExitAfterCall(this,41,_Trac2,&_ret_arg,&iImportUserAttr); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::GetImportUserAttrInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,42,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetImportUserAttrInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,42,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::SetImportUserAttrLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,43,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetImportUserAttrLock(iLocked); \
   ExitAfterCall(this,43,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::get_ImportEdge(CAT_VARIANT_BOOL & oImportEdge) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,44,&_Trac2,&oImportEdge); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_ImportEdge(oImportEdge); \
   ExitAfterCall(this,44,_Trac2,&_ret_arg,&oImportEdge); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::put_ImportEdge(CAT_VARIANT_BOOL iImportEdge) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,45,&_Trac2,&iImportEdge); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_ImportEdge(iImportEdge); \
   ExitAfterCall(this,45,_Trac2,&_ret_arg,&iImportEdge); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::GetImportEdgeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,46,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetImportEdgeInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,46,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::SetImportEdgeLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,47,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetImportEdgeLock(iLocked); \
   ExitAfterCall(this,47,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::get_ImportCoorsys(CAT_VARIANT_BOOL & oImportCoorsys) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,48,&_Trac2,&oImportCoorsys); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_ImportCoorsys(oImportCoorsys); \
   ExitAfterCall(this,48,_Trac2,&_ret_arg,&oImportCoorsys); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::put_ImportCoorsys(CAT_VARIANT_BOOL iImportCoorsys) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,49,&_Trac2,&iImportCoorsys); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_ImportCoorsys(iImportCoorsys); \
   ExitAfterCall(this,49,_Trac2,&_ret_arg,&iImportCoorsys); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::GetImportCoorsysInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,50,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetImportCoorsysInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,50,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::SetImportCoorsysLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,51,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetImportCoorsysLock(iLocked); \
   ExitAfterCall(this,51,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::get_ImportToolFrm(CAT_VARIANT_BOOL & oImportToolFrm) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,52,&_Trac2,&oImportToolFrm); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_ImportToolFrm(oImportToolFrm); \
   ExitAfterCall(this,52,_Trac2,&_ret_arg,&oImportToolFrm); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::put_ImportToolFrm(CAT_VARIANT_BOOL iImportToolFrm) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,53,&_Trac2,&iImportToolFrm); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_ImportToolFrm(iImportToolFrm); \
   ExitAfterCall(this,53,_Trac2,&_ret_arg,&iImportToolFrm); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::GetImportToolFrmInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,54,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetImportToolFrmInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,54,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::SetImportToolFrmLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,55,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetImportToolFrmLock(iLocked); \
   ExitAfterCall(this,55,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::get_ImportBaseFrm(CAT_VARIANT_BOOL & oImportBaseFrm) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,56,&_Trac2,&oImportBaseFrm); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_ImportBaseFrm(oImportBaseFrm); \
   ExitAfterCall(this,56,_Trac2,&_ret_arg,&oImportBaseFrm); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::put_ImportBaseFrm(CAT_VARIANT_BOOL iImportBaseFrm) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,57,&_Trac2,&iImportBaseFrm); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_ImportBaseFrm(iImportBaseFrm); \
   ExitAfterCall(this,57,_Trac2,&_ret_arg,&iImportBaseFrm); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::GetImportBaseFrmInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,58,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetImportBaseFrmInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,58,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::SetImportBaseFrmLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,59,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetImportBaseFrmLock(iLocked); \
   ExitAfterCall(this,59,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::get_ImportWclPath(CAT_VARIANT_BOOL & oImportWclPath) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,60,&_Trac2,&oImportWclPath); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_ImportWclPath(oImportWclPath); \
   ExitAfterCall(this,60,_Trac2,&_ret_arg,&oImportWclPath); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::put_ImportWclPath(CAT_VARIANT_BOOL iImportWclPath) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,61,&_Trac2,&iImportWclPath); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_ImportWclPath(iImportWclPath); \
   ExitAfterCall(this,61,_Trac2,&_ret_arg,&iImportWclPath); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::GetImportWclPathInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,62,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetImportWclPathInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,62,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::SetImportWclPathLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,63,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetImportWclPathLock(iLocked); \
   ExitAfterCall(this,63,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::get_VisCoorsys(FrameVisibility & oVisCoorsys) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,64,&_Trac2,&oVisCoorsys); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_VisCoorsys(oVisCoorsys); \
   ExitAfterCall(this,64,_Trac2,&_ret_arg,&oVisCoorsys); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::put_VisCoorsys(FrameVisibility iVisCoorsys) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,65,&_Trac2,&iVisCoorsys); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_VisCoorsys(iVisCoorsys); \
   ExitAfterCall(this,65,_Trac2,&_ret_arg,&iVisCoorsys); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::GetVisCoorsysInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,66,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetVisCoorsysInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,66,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::SetVisCoorsysLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,67,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetVisCoorsysLock(iLocked); \
   ExitAfterCall(this,67,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::get_VisToolFrm(FrameVisibility & oVisToolFrm) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,68,&_Trac2,&oVisToolFrm); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_VisToolFrm(oVisToolFrm); \
   ExitAfterCall(this,68,_Trac2,&_ret_arg,&oVisToolFrm); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::put_VisToolFrm(FrameVisibility iVisToolFrm) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,69,&_Trac2,&iVisToolFrm); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_VisToolFrm(iVisToolFrm); \
   ExitAfterCall(this,69,_Trac2,&_ret_arg,&iVisToolFrm); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::GetVisToolFrmInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,70,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetVisToolFrmInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,70,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::SetVisToolFrmLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,71,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetVisToolFrmLock(iLocked); \
   ExitAfterCall(this,71,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::get_VisBaseFrm(FrameVisibility & oVisBaseFrm) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,72,&_Trac2,&oVisBaseFrm); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_VisBaseFrm(oVisBaseFrm); \
   ExitAfterCall(this,72,_Trac2,&_ret_arg,&oVisBaseFrm); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::put_VisBaseFrm(FrameVisibility iVisBaseFrm) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,73,&_Trac2,&iVisBaseFrm); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_VisBaseFrm(iVisBaseFrm); \
   ExitAfterCall(this,73,_Trac2,&_ret_arg,&iVisBaseFrm); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::GetVisBaseFrmInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,74,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetVisBaseFrmInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,74,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::SetVisBaseFrmLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,75,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetVisBaseFrmLock(iLocked); \
   ExitAfterCall(this,75,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::get_VisWclPath(FrameVisibility & oVisWclPath) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,76,&_Trac2,&oVisWclPath); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_VisWclPath(oVisWclPath); \
   ExitAfterCall(this,76,_Trac2,&_ret_arg,&oVisWclPath); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::put_VisWclPath(FrameVisibility iVisWclPath) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,77,&_Trac2,&iVisWclPath); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_VisWclPath(iVisWclPath); \
   ExitAfterCall(this,77,_Trac2,&_ret_arg,&iVisWclPath); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::GetVisWclPathInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,78,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetVisWclPathInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,78,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::SetVisWclPathLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,79,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetVisWclPathLock(iLocked); \
   ExitAfterCall(this,79,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::get_TypeCoorsys(CATLONG & oTypeCoorsys) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,80,&_Trac2,&oTypeCoorsys); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_TypeCoorsys(oTypeCoorsys); \
   ExitAfterCall(this,80,_Trac2,&_ret_arg,&oTypeCoorsys); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::put_TypeCoorsys(CATLONG iTypeCoorsys) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,81,&_Trac2,&iTypeCoorsys); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_TypeCoorsys(iTypeCoorsys); \
   ExitAfterCall(this,81,_Trac2,&_ret_arg,&iTypeCoorsys); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::GetTypeCoorsysInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,82,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetTypeCoorsysInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,82,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::SetTypeCoorsysLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,83,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetTypeCoorsysLock(iLocked); \
   ExitAfterCall(this,83,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::get_TypeToolFrm(CATLONG & oTypeToolFrm) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,84,&_Trac2,&oTypeToolFrm); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_TypeToolFrm(oTypeToolFrm); \
   ExitAfterCall(this,84,_Trac2,&_ret_arg,&oTypeToolFrm); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::put_TypeToolFrm(CATLONG iTypeToolFrm) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,85,&_Trac2,&iTypeToolFrm); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_TypeToolFrm(iTypeToolFrm); \
   ExitAfterCall(this,85,_Trac2,&_ret_arg,&iTypeToolFrm); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::GetTypeToolFrmInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,86,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetTypeToolFrmInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,86,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::SetTypeToolFrmLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,87,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetTypeToolFrmLock(iLocked); \
   ExitAfterCall(this,87,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::get_TypeBaseFrm(CATLONG & oTypeBaseFrm) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,88,&_Trac2,&oTypeBaseFrm); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_TypeBaseFrm(oTypeBaseFrm); \
   ExitAfterCall(this,88,_Trac2,&_ret_arg,&oTypeBaseFrm); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::put_TypeBaseFrm(CATLONG iTypeBaseFrm) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,89,&_Trac2,&iTypeBaseFrm); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_TypeBaseFrm(iTypeBaseFrm); \
   ExitAfterCall(this,89,_Trac2,&_ret_arg,&iTypeBaseFrm); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::GetTypeBaseFrmInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,90,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetTypeBaseFrmInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,90,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::SetTypeBaseFrmLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,91,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetTypeBaseFrmLock(iLocked); \
   ExitAfterCall(this,91,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::get_TypeWclPath(CATLONG & oTypeWclPath) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,92,&_Trac2,&oTypeWclPath); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_TypeWclPath(oTypeWclPath); \
   ExitAfterCall(this,92,_Trac2,&_ret_arg,&oTypeWclPath); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::put_TypeWclPath(CATLONG iTypeWclPath) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,93,&_Trac2,&iTypeWclPath); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_TypeWclPath(iTypeWclPath); \
   ExitAfterCall(this,93,_Trac2,&_ret_arg,&iTypeWclPath); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::GetTypeWclPathInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,94,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetTypeWclPathInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,94,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::SetTypeWclPathLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,95,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetTypeWclPathLock(iLocked); \
   ExitAfterCall(this,95,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::Commit() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,96,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Commit(); \
   ExitAfterCall(this,96,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::Rollback() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,97,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Rollback(); \
   ExitAfterCall(this,97,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::ResetToAdminValues() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,98,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ResetToAdminValues(); \
   ExitAfterCall(this,98,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,99,&_Trac2,&iAttList); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ResetToAdminValuesByName(iAttList); \
   ExitAfterCall(this,99,_Trac2,&_ret_arg,&iAttList); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAImportD5SettingAtt##classe::SaveRepository() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,100,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SaveRepository(); \
   ExitAfterCall(this,100,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAImportD5SettingAtt##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,101,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,101,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAImportD5SettingAtt##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,102,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,102,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAImportD5SettingAtt##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,103,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,103,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAImportD5SettingAtt##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,104,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,104,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAImportD5SettingAtt##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,105,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,105,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIAImportD5SettingAtt(classe) \
 \
 \
declare_TIE_DNBIAImportD5SettingAtt(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAImportD5SettingAtt##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAImportD5SettingAtt,"DNBIAImportD5SettingAtt",DNBIAImportD5SettingAtt::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAImportD5SettingAtt(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIAImportD5SettingAtt, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAImportD5SettingAtt##classe(classe::MetaObject(),DNBIAImportD5SettingAtt::MetaObject(),(void *)CreateTIEDNBIAImportD5SettingAtt##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIAImportD5SettingAtt(classe) \
 \
 \
declare_TIE_DNBIAImportD5SettingAtt(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAImportD5SettingAtt##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAImportD5SettingAtt,"DNBIAImportD5SettingAtt",DNBIAImportD5SettingAtt::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAImportD5SettingAtt(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIAImportD5SettingAtt, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAImportD5SettingAtt##classe(classe::MetaObject(),DNBIAImportD5SettingAtt::MetaObject(),(void *)CreateTIEDNBIAImportD5SettingAtt##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIAImportD5SettingAtt(classe) TIE_DNBIAImportD5SettingAtt(classe)
#else
#define BOA_DNBIAImportD5SettingAtt(classe) CATImplementBOA(DNBIAImportD5SettingAtt, classe)
#endif

#endif
