# COPYRIGHT DASSAULT SYSTEMES 2002
#==============================================================================
# Imakefile for module DNBD5IItf.m 
#==============================================================================
#
#  Jul 2003  Creation:                                                      awn
#  Jan 2004  Modified to support CAA/IDL code                               mmh
#==============================================================================
#
# SHARED LIBRARY 
#
BUILT_OBJECT_TYPE=SHARED LIBRARY
 
# DO NOT EDIT :: THE CAA2 WIZARDS WILL ADD CODE HERE
WIZARD_LINK_MODULES = \
    JS0GROUP   \
JS0FM DNBD5IInterfacesUUID 
# END WIZARD EDITION ZONE

LINK_WITH = $(WIZARD_LINK_MODULES)

INCLUDED_MODULES = \
                  DNBD5IItfCPP \
                  DNBD5IPubIDL \
                  DNBD5IProIDL \


#
# Define the build options for the current module.
#
OS      = Windows_NT
BUILD   = YES

OS      = IRIX
BUILD   = YES

OS      = SunOS
BUILD   = YES

OS      = AIX
BUILD   = YES

OS      = HP-UX
BUILD   = YES

OS      = win_a
BUILD   = NO

