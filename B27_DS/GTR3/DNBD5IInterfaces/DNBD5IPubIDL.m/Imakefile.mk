# COPYRIGHT DASSAULT SYSTEMES 2004
#==============================================================================
# Imakefile for module DNBD5IPubIdl.m
# Module for compilation of the public IDL interfaces
#==============================================================================
#  Jan 2004  Creation                                                       awn
#==============================================================================
#
# NO BUILD           
#

BUILT_OBJECT_TYPE=NONE

SOURCES_PATH=PublicInterfaces
COMPILATION_IDL=YES

