# COPYRIGHT DASSAULT SYSTEMES 2004
#==============================================================================
# Imakefile for module DNBD5IProIDL
# Module for compilation of the protected IDL interfaces
#==============================================================================
#  Jan 2004  Creation                                                       awn
#==============================================================================
#
# NO BUILD             
#

BUILT_OBJECT_TYPE=NONE

SOURCES_PATH=ProtectedInterfaces
COMPILATION_IDL=YES

