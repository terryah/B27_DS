//
// COPYRIGHT DASSAULT SYSTEMES 2007
//===================================================================
// 
// DNBID5IExtractKinematics.h
//
//   This interface allows to extract D5 Device kinematics data.
//
//===================================================================
//
// May 2007    Creation:                shl
//===================================================================

#ifndef DNBID5IExtractKinematics_H
#define DNBID5IExtractKinematics_H

/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */

#include "DNBD5IItfCPP.h"
#include "CATBaseUnknown.h"
#include "CATIProduct.h"
#include "CATUnicodeString.h"
#include "CATLISTV_CATBaseUnknown.h"
#include "CATMathTransformation.h"
#include "DNBD5JointType.h"

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByDNBD5IItfCPP IID IID_DNBID5IExtractKinematics;
#else
extern "C" const IID IID_DNBID5IExtractKinematics;
#endif

//------------------------------------------------------------------

/**
 * <b>Role</b>: Interface to extract kinematics information of D5 imported device.
 * The methods of this interface allows to explore D5 device data and extract
 * kinematic joint details of device parts.
 * Note: For proper use of this interface, users should turn ON env variable 
 * DLM_DISABLE_D5_KIN_IMPORT while importing D5 devices. This will ensure
 * loading of only geometric data and not the kinematics data of the imported
 * D5 device.
 */

class ExportedByDNBD5IItfCPP DNBID5IExtractKinematics : public CATBaseUnknown
{
  CATDeclareInterface;
  
  public :
  
    /** 
     * Gets all the parts of the device in a list.
     * @param oPartList
     *    List of parts belonging to the Device.
     * @return
     *  <b>Legal values</b>:
     *  <br><tt>S_OK :</tt>   on Success
     *  <br><tt>E_FAIL:</tt>  on failure
     */
    virtual HRESULT  GetPartsOfDevice (
        CATListValCATBaseUnknown_var & oPartList) = 0;

    /** 
     * Gets fixed part of the D5 device mechanism.
     * @param oFixedPart
     *    Fixed part of the D5 mechanism.
     * @return
     *  <b>Legal values</b>:
     *  <br><tt>S_OK :</tt>   on Success
     *  <br><tt>E_FAIL:</tt>  on failure
     */
    virtual HRESULT GetFixedPart (CATIProduct_var & oFixedPart) = 0;

    /**
     * Gets the list of inner parts. For all the joints defined on the part 
     * (iOuterPart) where part happens to be outer part of the joint, get
     * inner part of the joint and add to the inner parts list (oInnerPartList).
     * @param iOuterPart
     *    Part which happens to be outer part of a joint.
     * @param oInnerPartList
     *    List of inner part of joints defined on the iOuterPart.
     * @return
     *  <b>Legal values</b>:
     *  <br><tt>S_OK :</tt>   on Success
     *  <br><tt>E_FAIL:</tt>  on failure
     */ 

    virtual HRESULT GetAssociatedKinParts (
        const CATIProduct_var & iOuterPart, 
        CATListValCATBaseUnknown_var & oInnerPartList) = 0;

    /**
     * From the inner part and outer part pair get the underlying joint
     * details
     * @param iOuterPart
     *    Outer part of the joint
     * @param iInnerPart
     *    Inner part of the joint
     * @param oJointName
     *    Name of the joint
     * @param oJntType
     *    Joint type
     * @param oOuterAxisPrd
     *    Frame of interest indicating joint location with respect to Outer
     *    part
     * @param oInnerAxisPrd
     *    Frame of intereat indicating joint location with respect to inner
     *    part
     * @param oCommand
     *    Integer value, a non zero value indicates the given joint is command
     *    joint
     * @return
     *  <b>Legal values</b>:
     *  <br><tt>S_OK :</tt>   on Success
     *  <br><tt>E_FAIL:</tt>  on failure
     */

    virtual HRESULT GetPartKinematicsInfo (
        const CATIProduct_var & iOuterPart,
        const CATIProduct_var & iInnerPart,
        CATUnicodeString & oJointName,
        DNBD5JointType & oJntType,
        CATIProduct_var & oOuterAxisPrd,
        CATIProduct_var & oInnerAxisPrd,
        int & oCommand) = 0;

    /**
     * From the inner part and outer part pair get the underlying joint
     * details
     * @param iOuterPart
     *    Outer part of the joint
     * @param iInnerPart
     *    Inner part of the joint
     * @param oJointName
     *    Name of the joint
     * @param oJntType
     *    Joint type
     * @param oOuterTransform
     *    Absolute Transformation matrix indicating join location  with
     *    respect to outer part
     * @param oInnerTransform
     *    Absolute Transformation matrix indicating join location  with
     *    respect to inner part
     * @param oCommand
     *    Integer value, a non zero value indicates the given joint is command
     *    joint
     * @return
     *  <b>Legal values</b>:
     *  <br><tt>S_OK :</tt>   on Success
     *  <br><tt>E_FAIL:</tt>  on failure
     */
     virtual HRESULT GetPartKinematicsInfo (
        const CATIProduct_var & iOuterPart,
        const CATIProduct_var & iInnerPart,
        CATUnicodeString & oJointName,
        DNBD5JointType & oJntType,
        CATMathTransformation & oOuterTransform,
        CATMathTransformation & oInnerTransform,
        int & oCommand) = 0;


};

//------------------------------------------------------------------
CATDeclareHandler( DNBID5IExtractKinematics, CATBaseUnknown) ;

        
#endif
