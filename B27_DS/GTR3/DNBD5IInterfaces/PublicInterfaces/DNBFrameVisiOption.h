#ifndef DNBFrameVisiOption_h
#define DNBFrameVisiOption_h

// COPYRIGHT DASSAULT SYSTEMES 2006
//--------------------------------------------------------------------------
// Enum - Visibility option for Frames of interest
//--------------------------------------------------------------------------

/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */

enum  FrameVisibility { VisNo, VisYes, VisRetain };

#endif
