#ifndef DNBJointType_h
#define DNBJointType_h

// COPYRIGHT DASSAULT SYSTEMES 2007
//--------------------------------------------------------------------------
// Enum - various Joint types 
//--------------------------------------------------------------------------

/**
 * @CAA2Level L0
 * @CAA2Usage U3
 */

enum  DNBD5JointType { DNB_RIGID,
                     DNB_TRANS_X,
                     DNB_TRANS_Y,
                     DNB_TRANS_Z,
                     DNB_ROTATE_X,
                     DNB_ROTATE_Y,
                     DNB_ROTATE_Z,
                     DNB_TRANS_X_NEG,
                     DNB_TRANS_Y_NEG,
                     DNB_TRANS_Z_NEG,
                     DNB_ROTATE_X_NEG,
                     DNB_ROTATE_Y_NEG,
                     DNB_ROTATE_Z_NEG
                    };

#endif
