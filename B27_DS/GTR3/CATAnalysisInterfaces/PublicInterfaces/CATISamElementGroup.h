//===================================================================
// COPYRIGHT Microsoft 2012/08/23
//===================================================================
// CATISamElementGroup.cpp
// Header definition of class CATISamElementGroup
//===================================================================
//
// Usage notes:
//
//===================================================================
//  2012/08/23 Creation: Code generated by the 3DS wizard
//===================================================================
#ifndef CATISamElementGroup_H
#define CATISamElementGroup_H

/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */

#include "CATAnalysisInterface.h"
#include "CATBaseUnknown.h"



#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByCATAnalysisInterface IID IID_CATISamElementGroup ;
#else
extern "C" const IID IID_CATISamElementGroup ;
#endif


class CATMSHElement;

class ExportedByCATAnalysisInterface CATISamElementGroup: public CATBaseUnknown
{
CATDeclareInterface;

public:

	virtual  int GetElementList(CATMSHElement** & oMshElementList)= 0;

//
// TODO: Add your methods for this interface here.
//

};

//-----------------------------------------------------------------------
CATDeclareHandler( CATISamElementGroup, CATBaseUnknown );
#endif
