/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAAnalysisGlobalSensor_h
#define CATIAAnalysisGlobalSensor_h

#ifndef ExportedByCATAnalysisPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __CATAnalysisPubIDL
#define ExportedByCATAnalysisPubIDL __declspec(dllexport)
#else
#define ExportedByCATAnalysisPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByCATAnalysisPubIDL
#endif
#endif

#include "CATBSTR.h"
#include "CATIAAnalysisSensor.h"

extern ExportedByCATAnalysisPubIDL IID IID_CATIAAnalysisGlobalSensor;

class ExportedByCATAnalysisPubIDL CATIAAnalysisGlobalSensor : public CATIAAnalysisSensor
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall SetIdentifier(const CATBSTR & iTypeBSTR, const CATBSTR & iSubTypeBSTR)=0;

    virtual HRESULT __stdcall GetIdentifier(CATBSTR & oTypeBSTR, CATBSTR & oSubTypeBSTR)=0;


};

CATDeclareHandler(CATIAAnalysisGlobalSensor, CATIAAnalysisSensor);

#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIAAnalysisEntity.h"
#include "CATIABase.h"
#include "CATIACollection.h"
#include "CATIAParameters.h"
#include "CATVariant.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
