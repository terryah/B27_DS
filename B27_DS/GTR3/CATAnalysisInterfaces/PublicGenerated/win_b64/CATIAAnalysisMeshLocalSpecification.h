/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAAnalysisMeshLocalSpecification_h
#define CATIAAnalysisMeshLocalSpecification_h

#ifndef ExportedByCATAnalysisPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __CATAnalysisPubIDL
#define ExportedByCATAnalysisPubIDL __declspec(dllexport)
#else
#define ExportedByCATAnalysisPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByCATAnalysisPubIDL
#endif
#endif

#include "CATBSTR.h"
#include "CATIABase.h"
#include "CATVariant.h"

class CATIAProduct;
class CATIAPublication;
class CATIAReference;

extern ExportedByCATAnalysisPubIDL IID IID_CATIAAnalysisMeshLocalSpecification;

class ExportedByCATAnalysisPubIDL CATIAAnalysisMeshLocalSpecification : public CATIABase
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_Type(CATBSTR & oType)=0;

    virtual HRESULT __stdcall SetAttribute(const CATBSTR & iName, const CATVariant & iValue)=0;

    virtual HRESULT __stdcall AddSupportFromReference(const CATBSTR & iName, CATIAProduct * iProduct, CATIAReference * iSupport)=0;

    virtual HRESULT __stdcall AddSupportFromPublication(const CATBSTR & iName, CATIAProduct * iProduct, CATIAPublication * iSupport)=0;


};

CATDeclareHandler(CATIAAnalysisMeshLocalSpecification, CATIABase);

#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
