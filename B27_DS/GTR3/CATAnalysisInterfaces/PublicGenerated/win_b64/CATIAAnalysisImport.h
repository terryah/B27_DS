/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAAnalysisImport_h
#define CATIAAnalysisImport_h

#ifndef ExportedByCATAnalysisPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __CATAnalysisPubIDL
#define ExportedByCATAnalysisPubIDL __declspec(dllexport)
#else
#define ExportedByCATAnalysisPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByCATAnalysisPubIDL
#endif
#endif

#include "CATBSTR.h"
#include "CATIABase.h"

class CATIAAnalysisCase;
class CATIAAnalysisManager;
class CATIAProduct;

extern ExportedByCATAnalysisPubIDL IID IID_CATIAAnalysisImport;

class ExportedByCATAnalysisPubIDL CATIAAnalysisImport : public CATIABase
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall AddSupport(CATIAAnalysisManager * iManager, CATIAProduct * iProductSupport, CATIABase * iSupport)=0;

    virtual HRESULT __stdcall ImportDisp(CATIAAnalysisCase * iFatherCase, const CATBSTR & iFullPath, CATIAAnalysisManager * iManager, CATIABase * iAxis)=0;

    virtual HRESULT __stdcall ImportForce(CATIAAnalysisCase * iFatherCase, const CATBSTR & iFullPath, CATIAAnalysisManager * iManager, CATIABase * iAxis)=0;


};

CATDeclareHandler(CATIAAnalysisImport, CATIABase);

#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
