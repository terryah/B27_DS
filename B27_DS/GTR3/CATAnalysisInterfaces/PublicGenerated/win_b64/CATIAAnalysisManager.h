/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAAnalysisManager_h
#define CATIAAnalysisManager_h

#ifndef ExportedByCATAnalysisPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __CATAnalysisPubIDL
#define ExportedByCATAnalysisPubIDL __declspec(dllexport)
#else
#define ExportedByCATAnalysisPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByCATAnalysisPubIDL
#endif
#endif

#include "CATBSTR.h"
#include "CATIABase.h"
#include "CATSafeArray.h"

class CATIAAnalysisLinkedDocuments;
class CATIAAnalysisModels;
class CATIAAnalysisSets;
class CATIADocument;
class CATIAParameters;
class CATIAProduct;
class CATIAReference;
class CATIARelations;

extern ExportedByCATAnalysisPubIDL IID IID_CATIAAnalysisManager;

class ExportedByCATAnalysisPubIDL CATIAAnalysisManager : public CATIABase
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_AnalysisModels(CATIAAnalysisModels *& oFEMModels)=0;

    virtual HRESULT __stdcall get_LinkedDocuments(CATIAAnalysisLinkedDocuments *& oLinkedDocuments)=0;

    virtual HRESULT __stdcall get_Parameters(CATIAParameters *& oParameters)=0;

    virtual HRESULT __stdcall get_Relations(CATIARelations *& oRelations)=0;

    virtual HRESULT __stdcall Import(CATIADocument * iDocumentToImport)=0;

    virtual HRESULT __stdcall ImportFile(const CATBSTR & iDocumentPath)=0;

    virtual HRESULT __stdcall ImportDefineFile(const CATBSTR & iDocumentPath, const CATBSTR & iTypeLate, const CATSafeArrayVariant & iValues)=0;

    virtual HRESULT __stdcall CreateReferenceFromObject(CATIABase * iObject, CATIAReference *& oRef)=0;

    virtual HRESULT __stdcall CreateReferenceFromGeometry(CATIAProduct * iProduct, CATIAReference * iGeometry, CATIAReference *& oRef)=0;

    virtual HRESULT __stdcall get_AnalysisSets(CATIAAnalysisSets *& oAnalysisSets)=0;


};

CATDeclareHandler(CATIAAnalysisManager, CATIABase);

#include "CATAnalysisSetType.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIAAnalysisLinkedDocuments.h"
#include "CATIAAnalysisSets.h"
#include "CATIAAnalyze.h"
#include "CATIACollection.h"
#include "CATIADocument.h"
#include "CATIAMove.h"
#include "CATIAParameters.h"
#include "CATIAPosition.h"
#include "CATIAProduct.h"
#include "CATIAPublications.h"
#include "CATIAReference.h"
#include "CATIARelations.h"
#include "CATVariant.h"
#include "CatFileType.h"
#include "CatProductSource.h"
#include "CatRepType.h"
#include "CatWorkModeType.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
