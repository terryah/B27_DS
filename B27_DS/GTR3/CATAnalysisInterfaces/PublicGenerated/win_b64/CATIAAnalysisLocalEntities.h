/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAAnalysisLocalEntities_h
#define CATIAAnalysisLocalEntities_h

#ifndef ExportedByCATAnalysisPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __CATAnalysisPubIDL
#define ExportedByCATAnalysisPubIDL __declspec(dllexport)
#else
#define ExportedByCATAnalysisPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByCATAnalysisPubIDL
#endif
#endif

#include "CATBSTR.h"
#include "CATIACollection.h"
#include "CATVariant.h"

class CATIAAnalysisLocalEntity;

extern ExportedByCATAnalysisPubIDL IID IID_CATIAAnalysisLocalEntities;

class ExportedByCATAnalysisPubIDL CATIAAnalysisLocalEntities : public CATIACollection
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall Add(const CATBSTR & iType, CATIAAnalysisLocalEntity *& oEntity)=0;

    virtual HRESULT __stdcall Item(const CATVariant & iIndex, CATIAAnalysisLocalEntity *& oEntity)=0;

    virtual HRESULT __stdcall Remove(const CATVariant & iIndex)=0;


};

CATDeclareHandler(CATIAAnalysisLocalEntities, CATIACollection);

#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIAAnalysisLocalEntity.h"
#include "CATIABase.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
