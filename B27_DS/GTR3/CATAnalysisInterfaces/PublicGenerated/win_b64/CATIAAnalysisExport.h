/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAAnalysisExport_h
#define CATIAAnalysisExport_h

#ifndef ExportedByCATAnalysisPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __CATAnalysisPubIDL
#define ExportedByCATAnalysisPubIDL __declspec(dllexport)
#else
#define ExportedByCATAnalysisPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByCATAnalysisPubIDL
#endif
#endif

#include "CATBSTR.h"
#include "CATIABase.h"
#include "CATSafeArray.h"

class CATIAAnalysisManager;

extern ExportedByCATAnalysisPubIDL IID IID_CATIAAnalysisExport;

class ExportedByCATAnalysisPubIDL CATIAAnalysisExport : public CATIABase
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall Export(const CATBSTR & iFullPath, const CATBSTR & iType, const CATSafeArrayVariant & iOccurrences, CATIAAnalysisManager * iManager)=0;


};

CATDeclareHandler(CATIAAnalysisExport, CATIABase);

#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
