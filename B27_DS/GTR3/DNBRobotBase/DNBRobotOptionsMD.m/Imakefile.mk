#
#   Imakefile.mk for DNBRobotOptionsMD
#   Copyright (C) DELMIA Corp., 2001
#
BUILT_OBJECT_TYPE=SHARED LIBRARY

# DO NOT EDIT :: THE CAA2 WIZARDS WILL ADD CODE HERE
WIZARD_LINK_MODULES =  \
        JS0GROUP CATInfInterfaces DNBRobotItfCPP DNBRobotPubIDL DNBRobotInterfacesUUID 
# END WIZARD EDITION ZONE


LINK_WITH           =   $(WIZARD_LINK_MODULES)    \
                        OM0EDPRO        \
                        JS03TRA         \
                        DI0PANV2        \
                        JS0CORBA        \
                        JS0FM           \
                        AD0XXBAS        \
                        JS0FM           \
                        CATDlgStandard  \
#            CATApplicationFrame \
         

#
# Define the build options for the current module.
#
OS      = Windows_NT
BUILD   = YES

OS      = IRIX
BUILD   = YES

OS      = SunOS
BUILD   = YES

OS      = AIX
BUILD   = YES

OS      = HP-UX
BUILD   = YES

OS      = win_a
BUILD   = NO

# awn : NO WDM stuff here : it is not necessary and should not even mentionned.
#       Settings is about Dialog, and application frame.
