// COPYRIGHT Dassault Systemes 2005
/** @CAA2Required */
/*---------------------------------------------------------------------*/
/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */
/*---------------------------------------------------------------------*/
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_ROBOTBASE_H_
#define _DNB_ROBOTBASE_H_


#ifdef  _WINDOWS_SOURCE
#ifdef  __DNBRobotBase
#define ExportedByDNBRobotBase    __declspec(dllexport)
#else
#define ExportedByDNBRobotBase    __declspec(dllimport)
#endif
#else
#define ExportedByDNBRobotBase    /* nothing */
#endif


#endif  /* _DNB_ROBOTBASE_H_ */
