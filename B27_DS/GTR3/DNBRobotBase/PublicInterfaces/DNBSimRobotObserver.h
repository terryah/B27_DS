//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
//
// DNBSimRobotObserver.h
//      TCP observer for DNBBasicDevice3D objects (robots only).
//
//==============================================================================
//
// Usage notes: 
//      Use this class to observe the TCP values of a robot during 
//      simulation.
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     mmh          12/03/2001   Initial implementation
//     awn          11/06/2002   Add time
//     NPJ          28-04-03     Turn Number and Config changes
//     rmy          29/04/2003   added method to read the tcp wrt base
//
//==============================================================================

#if DNB_HAS_PRAGMA_ONCE
#pragma once
#endif
 
#ifndef _DNB_SIMROBOTOBSERVER_H
#define _DNB_SIMROBOTOBSERVER_H

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>

#include <DNBObserver.h>
#include <DNBBasicDevice3D.h>

#include <DNBRobotBase.h>

//------------------------------------------------------------------------------

/**
 * TCP observer for DNBBasicDevice3D objects (robots only).
 */
class ExportedByDNBRobotBase DNBSimObserveRobot
{
    DNB_DECLARE_OBSERVER_ADAPTOR( DNBBasicDevice3D );

public:
    /**
     * Constructs a DNBSimObserveRobot object.
     */
    DNBSimObserveRobot()
        DNB_THROW_SPEC_NULL;
    /**
     * Destructs a DNBSimObserveRobot object.
     */
    ~DNBSimObserveRobot()
        DNB_THROW_SPEC_NULL;

    /**
     * Gets the TCP values.
     */
    void getTCPValue( DNBXform3D &oTCPValue );

    /**
     * Gets the TCP wrt Base link
     */

    void getTCPwrtBase(DNBXform3D &oTCPwrtBase);
    /**
     * Gets the time.
     */
    void getTime( double& dtime );

    /**
     * Gets Turn Number
     */
    void getTurnNumber( int* TurnNumber);
    
	 /**
     * Gets the Config
     */
	void getConfig( scl_wstring& Name);

   /**
     * Gets the TCP Speed
     */
	void getTCPSpeed( double& dTCPSpeed);

private:
   DNBXform3D      _TCPMatrixWRTObjectFrame;
   double          _dTime;
   DNBXform3D      _TCPMatrixWRTBase;
   int				 *_TurnNumber;
   int                _NumDOFs;
   scl_wstring			 _ConfigName;
   double          _dTCPSpeed;
};

//------------------------------------------------------------------------------

typedef DNBObserver< DNBSimObserveRobot, DNBBasicDevice3D > 
                                                        DNBSimRobotObserver;

DNB_DECLARE_SHARED_OBJECT_INST( DNBSimRobotObserver, 
                "ba24809e-e824-11d5-b967-00b0d078dba8" );

#endif // _DNB_SIMROBOTOBSERVER_H
