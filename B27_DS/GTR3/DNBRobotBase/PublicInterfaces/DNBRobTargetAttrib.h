//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2006
//==============================================================================
////////////////////////////////////////////////////////////////////////
//
// API's to set tag point and DOF target attribute values with  controller
// profile attribute values.
//
//        jyv 		10-03-2001		original code
//        jyv           10-16-2001              modified with input from
//						ali & jod.
//	  akp           01-31-2002              Add SetTagPointAuxAxesValue 
//	                                        method
//	  akp           09-10-2002              Add SetTagPointSyncAttribs
//	                                        and SetDOFTargetSyncAttribs
//	  akp           06-16-2003              Add SetTagPointContSpecAttribs
//	                                        and SetDOFTargetContSpecAttribs
//
////////////////////////////////////////////////////////////////////////
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNBROBTARGETATTRIB_H_
#define _DNBROBTARGETATTRIB_H_

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */


#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>

#include <DNBBasicTagPoint3D.h>


#include <CATBaseUnknown.h>
#include <CATBooleanDef.h>
#include <CATListOfCATUnicodeString.h>

#include <DNBCartesianPlannerExt.h>
//#include <DNBBasicRobot3D.h>

#include <DNBRobotBase.h>

class DNBIGenericToolProfile_var;
class DNBIGenericMotionProfile_var;
class DNBIGenericAccuracyProfile_var;
class DNBIGenericObjFrameProfile_var;

class DNBRobConfiguration;

class ExportedByDNBRobotBase DNBRobTargetAttrib 
{

public:

    //
    // Set auxiliary axes value attributes for given tag point
    //
    // NOTE: Length of aux_axes_value vector must equal the number 
    //       of aux axes that the associated device has!
    //
    static HRESULT
    SetTagPointAuxAxesValue (
		   DNBBasicTagPoint3D::Handle          hTag,
                   DNBMathVector                       &aux_axes_value);

    static HRESULT
    SetTagPointAttribPosture(
		   DNBBasicTagPoint3D::Handle          hTag,
                   DNBBasicRobot3D::Posture	       posture  );

    static HRESULT
    SetTagPointAttribConfig(
		   DNBBasicTagPoint3D::Handle          hTag,
                   DNBRobConfiguration   	       &config  );

    static HRESULT
    SetTagPointAttribs(
                       DNBBasicTagPoint3D::Handle 	  hTarget,
		       DNBReal       		          accelvalue,
		       DNBReal                            rounding,
		       DNBCartesianPlannerExt::MotionType moType
 				);

    static HRESULT
    SetTagPointAttribDuration(
                       DNBBasicTagPoint3D::Handle 	  hTarget,
	               DNBSimTime    		          duration
			);

    static HRESULT
    SetTagPointAttribSpeed(
                       DNBBasicTagPoint3D::Handle 	  hTarget,
		       DNBReal 			          speedvalue
		       );

    static HRESULT
    SetTagPointSyncAttribs(
                       DNBBasicTagPoint3D::Handle 	  hTarget,
		       bool 			          sync_target,
                       bool                               last_target
		       );

    static HRESULT
    SetTagPointContSpecAttribs(
                       DNBBasicTagPoint3D::Handle 	  hTarget,
                       const CATListOfCATUnicodeString&   aAttribNames,
                       const CATListOfCATUnicodeString&   aAttribTypes,
                       const CATListOfCATUnicodeString&   aAttribVals
                       );

#if 0

    static HRESULT 
    SetTagPointAttribs( 
		   DNBBasicTagPoint3D::Handle          hTag,
		   DNBIRobGenericController_var	       spController,
                   const CATUnicodeString	       &toolName,		
                   const CATUnicodeString	       &motName,	
                   const CATUnicodeString	       &accName,	
                   const CATUnicodeString	       &objFName	
		);


    static HRESULT 
    SetTagPointAttribs( 
		   DNBBasicTagPoint3D::Handle          hTag,
		   DNBIRobGenericController_var	       spController,
                   const CATUnicodeString	       &toolName		
		);


    static HRESULT 
    SetTagPointAttribs( 
		   DNBBasicTagPoint3D::Handle          hTag,
		   DNBIRobGenericController_var	       spController,
                   const CATUnicodeString	       &motName		
		);


    static HRESULT 
    SetTagPointAttribs( 
		   DNBBasicTagPoint3D::Handle          hTag,
		   DNBIRobGenericController_var	       spController,
                   const CATUnicodeString	       &accName		
		);


    static HRESULT 
    SetTagPointAttribs( 
		   DNBBasicTagPoint3D::Handle          hTag,
		   DNBIRobGenericController_var	       spController,
                   const CATUnicodeString	       &objFName		
		);

#endif

    //
    // Set tool, motion, accuracy and object frame profile related 
    // attribute values of a tag point.
    // An attribute value is associated with a name of the form:
    // [ControllerType].[Profile].[version].[ProfileAttribute], eg.
    // the attribute Generic.Motion1.MotionBasis of a tag point is 
    // version 1 of this attribute which is identical to the Generic 
    // motion profile attribute motion basis.  
    //
    static HRESULT 
    SetTagPointAttribs( 
		   DNBBasicTagPoint3D::Handle          hTag,
                   DNBIGenericToolProfile_var          spTool,
                   DNBIGenericMotionProfile_var        spMot,
                   DNBIGenericAccuracyProfile_var      spAcc,
                   DNBIGenericObjFrameProfile_var      spObjFrm ); 
                        

    //
    // Set tool profile related attribute values.
    //
    static HRESULT
    SetTagPointToolAttribs( 
		      DNBBasicTagPoint3D::Handle   hTag,
                      DNBIGenericToolProfile_var   spTool ); 


    //
    // Set motion profile related attribute values. In addition
    // sets the following motion related legacy attribute:
    // Motion legacy attribute "Accel" with acceleration
    // value of the motion profile.
    //
    static HRESULT
    SetTagPointMotionAttribs( 
			DNBBasicTagPoint3D::Handle          hTag,
                        DNBIGenericMotionProfile_var        spMot ); 

    //
    // Set accuracy profile related attribute values. In addition
    // sets the following motion related legacy attribute:
    // Motion legacy attribute "Rounding" based on the flyby mode 
    // and accuracy type of the accuracy profile. 
    //
    static HRESULT
    SetTagPointAccuracyAttribs( 
			  DNBBasicTagPoint3D::Handle       hTag,
                          DNBIGenericAccuracyProfile_var   spAcc ); 


    //
    // Set object frame profile related attribute values.
    //
    static HRESULT
    SetTagPointObjFrameAttribs( 
			  DNBBasicTagPoint3D::Handle      hTag,
                          DNBIGenericObjFrameProfile_var  spObjFrm ); 


    static HRESULT
    SetDOFTargetAttribs(
                       DNBBasicDOFTarget::Handle 	  hTarget,
	               DNBSimTime    		          duration,
		       DNBReal 			          speedvalue,
		       DNBReal       		          accelvalue,
		       DNBReal                            rounding
 			);

    static HRESULT
    SetDOFTargetMotionType( 
                       DNBBasicDOFTarget::Handle          hTarg,
                       DNBCartesianPlannerExt::MotionType moType);


    static HRESULT
    SetDOFTargetSyncAttribs(
                       DNBBasicDOFTarget::Handle          hTarget,
		       bool 			          sync_target,
                       bool                               last_target
		       );

    static HRESULT
    SetDOFTargetContSpecAttribs(
                       DNBBasicDOFTarget::Handle          hTarget,
                       const CATListOfCATUnicodeString&   aAttribNames,
                       const CATListOfCATUnicodeString&   aAttribTypes,
                       const CATListOfCATUnicodeString&   aAttribVals
                       );

    //
    // Set tool, motion, accuracy and object frame profile related 
    // attribute values of a DOF target.
    //
    static HRESULT
    SetDOFTargetAttribs( 
		       DNBBasicDOFTarget::Handle           hTarg,
                       DNBIGenericToolProfile_var          spTool,
                       DNBIGenericMotionProfile_var        spMot,
                       DNBIGenericAccuracyProfile_var      spAcc,
                       DNBIGenericObjFrameProfile_var      spObjFrm ); 
                        

    //
    // Set tool profile related attribute values.
    //
    static HRESULT
    SetDOFTargetToolAttribs( 
			  DNBBasicDOFTarget::Handle    hTarg,
                          DNBIGenericToolProfile_var   spTool ); 


    //
    // Set motion profile related attribute values. In addition
    // sets the following motion related legacy attribute:
    // Motion legacy attribute "Accel" with acceleration
    // value of motion profile.
    //
    static HRESULT
    SetDOFTargetMotionAttribs( 
			    DNBBasicDOFTarget::Handle           hTarg,
                            DNBIGenericMotionProfile_var        spMot ); 

    //
    // Set accuracy profile related attribute values. In addition
    // sets the following motion related legacy attribute:
    // Motion legacy attribute "Rounding" based on the flyby mode 
    // and accuracy type of the accuracy profile. 
    //
    static HRESULT
    SetDOFTargetAccuracyAttribs( 
			      DNBBasicDOFTarget::Handle        hTarg,
                              DNBIGenericAccuracyProfile_var   spAcc ); 


    //
    // Set object frame profile related attribute values.
    //
    static HRESULT
    SetDOFTargetObjFrameAttribs( 
			      DNBBasicDOFTarget::Handle       hTarg,
                              DNBIGenericObjFrameProfile_var  spObjFrm ); 

    DNBRobTargetAttrib(); 
    ~DNBRobTargetAttrib(); 

private:

    static double
    GetRoundingValue( DNBIGenericAccuracyProfile_var iAccuracyProf );
    
};





#endif

