#ifndef __TIE_DELMIAMoveHomeActivity
#define __TIE_DELMIAMoveHomeActivity

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "DELMIAMoveHomeActivity.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DELMIAMoveHomeActivity */
#define declare_TIE_DELMIAMoveHomeActivity(classe) \
 \
 \
class TIEDELMIAMoveHomeActivity##classe : public DELMIAMoveHomeActivity \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DELMIAMoveHomeActivity, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall get_HomeName(CATBSTR & oHomeName); \
      virtual HRESULT __stdcall put_HomeName(const CATBSTR & iHomeName); \
      virtual HRESULT __stdcall get_CornerRounding(double & oCornerRounding); \
      virtual HRESULT __stdcall put_CornerRounding(double iCornerRounding); \
      virtual HRESULT __stdcall get_SpeedPercent(double & oSpeedPercent); \
      virtual HRESULT __stdcall put_SpeedPercent(double iSpeedPercent); \
      virtual HRESULT __stdcall get_Acceleration(double & oAccel); \
      virtual HRESULT __stdcall put_Acceleration(double iAccel); \
      virtual HRESULT __stdcall get_MotionBasis(CATBSTR & oMotionBasis); \
      virtual HRESULT __stdcall put_MotionBasis(const CATBSTR & iMotionBasis); \
      virtual HRESULT __stdcall get_MechanismIndex(short & oMechanismIndex); \
      virtual HRESULT __stdcall put_MechanismIndex(short iMechanismIndex); \
      virtual HRESULT __stdcall IsSubTypeOf(const CATBSTR & iName, CAT_VARIANT_BOOL & oVal); \
      virtual HRESULT __stdcall AttrValue(const CATVariant & iIndex, CATVariant & oAttVal); \
      virtual HRESULT __stdcall AttrName(CATLONG iIndex, CATBSTR & oName); \
      virtual HRESULT __stdcall get_Type(CATBSTR & oType); \
      virtual HRESULT __stdcall get_Description(CATBSTR & oDescriptionBSTR); \
      virtual HRESULT __stdcall put_Description(const CATBSTR & iDescriptionBSTR); \
      virtual HRESULT __stdcall get_CycleTime(double & oCT); \
      virtual HRESULT __stdcall put_CycleTime(double iCT); \
      virtual HRESULT __stdcall get_CalculatedCycleTime(double & oCCT); \
      virtual HRESULT __stdcall get_BeginningDate(double & oBegin); \
      virtual HRESULT __stdcall put_BeginningDate(double iSBT); \
      virtual HRESULT __stdcall get_EndDate(double & oEnd); \
      virtual HRESULT __stdcall get_ChildrenActivities(CATIAActivities *& oChildren); \
      virtual HRESULT __stdcall CreateChild(const CATBSTR & iTypeOfChild, CATIAActivity *& oCreatedChild); \
      virtual HRESULT __stdcall CreateLink(CATIAActivity * iSecondActivity); \
      virtual HRESULT __stdcall RemoveLink(CATIAActivity * iSecondActivity); \
      virtual HRESULT __stdcall get_NextCFActivities(CATIAActivities *& oNextCF); \
      virtual HRESULT __stdcall get_PreviousCFActivities(CATIAActivities *& oPreviousCF); \
      virtual HRESULT __stdcall get_NextPRFActivities(CATIAActivities *& oNextPRF); \
      virtual HRESULT __stdcall get_PreviousPRFActivities(CATIAActivities *& oPreviousPRF); \
      virtual HRESULT __stdcall get_AttrCount(CATLONG & oNbAttr); \
      virtual HRESULT __stdcall get_Items(CATIAItems *& oItems); \
      virtual HRESULT __stdcall get_Outputs(CATIAOutputs *& oOutputs); \
      virtual HRESULT __stdcall get_Resources(CATIAResources *& oResources); \
      virtual HRESULT __stdcall get_Relations(CATIARelations *& oRelations); \
      virtual HRESULT __stdcall get_Parameters(CATIAParameters *& oParameters); \
      virtual HRESULT __stdcall GetTechnologicalObject(const CATBSTR & iApplicationType, CATBaseDispatch *& oApplicativeObj); \
      virtual HRESULT __stdcall get_PrecedenceActivities(CATIAActivities *& oActivities); \
      virtual HRESULT __stdcall get_PossiblePrecedenceActivities(CATIAActivities *& oActivities); \
      virtual HRESULT __stdcall get_ProcessID(CATBSTR & oProcessID); \
      virtual HRESULT __stdcall SetProcessID(const CATBSTR & iProcessID, CAT_VARIANT_BOOL iCheckUnique); \
      virtual HRESULT __stdcall get_CalculatedBeginTime(double & oCBT); \
      virtual HRESULT __stdcall AddAttr(const CATBSTR & iAttributeName, SPPProcessAttributeType AttrType, const CATBSTR & iAttributePromptName); \
      virtual HRESULT __stdcall RemoveAttr(const CATBSTR & iAttributeName); \
      virtual HRESULT __stdcall AddActivityConstraint(CATIAActivity * iActivity, SPPProcessConstraintType iConstraintType); \
      virtual HRESULT __stdcall RemoveActivityConstraint(CATIAActivity * iActivity, SPPProcessConstraintType iConstraintType); \
      virtual HRESULT __stdcall GetActivityConstraints(SPPProcessConstraintType iConstraintType, CATIAActivities *& oConstrtList); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DELMIAMoveHomeActivity(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall get_HomeName(CATBSTR & oHomeName); \
virtual HRESULT __stdcall put_HomeName(const CATBSTR & iHomeName); \
virtual HRESULT __stdcall get_CornerRounding(double & oCornerRounding); \
virtual HRESULT __stdcall put_CornerRounding(double iCornerRounding); \
virtual HRESULT __stdcall get_SpeedPercent(double & oSpeedPercent); \
virtual HRESULT __stdcall put_SpeedPercent(double iSpeedPercent); \
virtual HRESULT __stdcall get_Acceleration(double & oAccel); \
virtual HRESULT __stdcall put_Acceleration(double iAccel); \
virtual HRESULT __stdcall get_MotionBasis(CATBSTR & oMotionBasis); \
virtual HRESULT __stdcall put_MotionBasis(const CATBSTR & iMotionBasis); \
virtual HRESULT __stdcall get_MechanismIndex(short & oMechanismIndex); \
virtual HRESULT __stdcall put_MechanismIndex(short iMechanismIndex); \
virtual HRESULT __stdcall IsSubTypeOf(const CATBSTR & iName, CAT_VARIANT_BOOL & oVal); \
virtual HRESULT __stdcall AttrValue(const CATVariant & iIndex, CATVariant & oAttVal); \
virtual HRESULT __stdcall AttrName(CATLONG iIndex, CATBSTR & oName); \
virtual HRESULT __stdcall get_Type(CATBSTR & oType); \
virtual HRESULT __stdcall get_Description(CATBSTR & oDescriptionBSTR); \
virtual HRESULT __stdcall put_Description(const CATBSTR & iDescriptionBSTR); \
virtual HRESULT __stdcall get_CycleTime(double & oCT); \
virtual HRESULT __stdcall put_CycleTime(double iCT); \
virtual HRESULT __stdcall get_CalculatedCycleTime(double & oCCT); \
virtual HRESULT __stdcall get_BeginningDate(double & oBegin); \
virtual HRESULT __stdcall put_BeginningDate(double iSBT); \
virtual HRESULT __stdcall get_EndDate(double & oEnd); \
virtual HRESULT __stdcall get_ChildrenActivities(CATIAActivities *& oChildren); \
virtual HRESULT __stdcall CreateChild(const CATBSTR & iTypeOfChild, CATIAActivity *& oCreatedChild); \
virtual HRESULT __stdcall CreateLink(CATIAActivity * iSecondActivity); \
virtual HRESULT __stdcall RemoveLink(CATIAActivity * iSecondActivity); \
virtual HRESULT __stdcall get_NextCFActivities(CATIAActivities *& oNextCF); \
virtual HRESULT __stdcall get_PreviousCFActivities(CATIAActivities *& oPreviousCF); \
virtual HRESULT __stdcall get_NextPRFActivities(CATIAActivities *& oNextPRF); \
virtual HRESULT __stdcall get_PreviousPRFActivities(CATIAActivities *& oPreviousPRF); \
virtual HRESULT __stdcall get_AttrCount(CATLONG & oNbAttr); \
virtual HRESULT __stdcall get_Items(CATIAItems *& oItems); \
virtual HRESULT __stdcall get_Outputs(CATIAOutputs *& oOutputs); \
virtual HRESULT __stdcall get_Resources(CATIAResources *& oResources); \
virtual HRESULT __stdcall get_Relations(CATIARelations *& oRelations); \
virtual HRESULT __stdcall get_Parameters(CATIAParameters *& oParameters); \
virtual HRESULT __stdcall GetTechnologicalObject(const CATBSTR & iApplicationType, CATBaseDispatch *& oApplicativeObj); \
virtual HRESULT __stdcall get_PrecedenceActivities(CATIAActivities *& oActivities); \
virtual HRESULT __stdcall get_PossiblePrecedenceActivities(CATIAActivities *& oActivities); \
virtual HRESULT __stdcall get_ProcessID(CATBSTR & oProcessID); \
virtual HRESULT __stdcall SetProcessID(const CATBSTR & iProcessID, CAT_VARIANT_BOOL iCheckUnique); \
virtual HRESULT __stdcall get_CalculatedBeginTime(double & oCBT); \
virtual HRESULT __stdcall AddAttr(const CATBSTR & iAttributeName, SPPProcessAttributeType AttrType, const CATBSTR & iAttributePromptName); \
virtual HRESULT __stdcall RemoveAttr(const CATBSTR & iAttributeName); \
virtual HRESULT __stdcall AddActivityConstraint(CATIAActivity * iActivity, SPPProcessConstraintType iConstraintType); \
virtual HRESULT __stdcall RemoveActivityConstraint(CATIAActivity * iActivity, SPPProcessConstraintType iConstraintType); \
virtual HRESULT __stdcall GetActivityConstraints(SPPProcessConstraintType iConstraintType, CATIAActivities *& oConstrtList); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DELMIAMoveHomeActivity(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::get_HomeName(CATBSTR & oHomeName) \
{ \
return (ENVTIECALL(DELMIAMoveHomeActivity,ENVTIETypeLetter,ENVTIELetter)get_HomeName(oHomeName)); \
} \
HRESULT __stdcall  ENVTIEName::put_HomeName(const CATBSTR & iHomeName) \
{ \
return (ENVTIECALL(DELMIAMoveHomeActivity,ENVTIETypeLetter,ENVTIELetter)put_HomeName(iHomeName)); \
} \
HRESULT __stdcall  ENVTIEName::get_CornerRounding(double & oCornerRounding) \
{ \
return (ENVTIECALL(DELMIAMoveHomeActivity,ENVTIETypeLetter,ENVTIELetter)get_CornerRounding(oCornerRounding)); \
} \
HRESULT __stdcall  ENVTIEName::put_CornerRounding(double iCornerRounding) \
{ \
return (ENVTIECALL(DELMIAMoveHomeActivity,ENVTIETypeLetter,ENVTIELetter)put_CornerRounding(iCornerRounding)); \
} \
HRESULT __stdcall  ENVTIEName::get_SpeedPercent(double & oSpeedPercent) \
{ \
return (ENVTIECALL(DELMIAMoveHomeActivity,ENVTIETypeLetter,ENVTIELetter)get_SpeedPercent(oSpeedPercent)); \
} \
HRESULT __stdcall  ENVTIEName::put_SpeedPercent(double iSpeedPercent) \
{ \
return (ENVTIECALL(DELMIAMoveHomeActivity,ENVTIETypeLetter,ENVTIELetter)put_SpeedPercent(iSpeedPercent)); \
} \
HRESULT __stdcall  ENVTIEName::get_Acceleration(double & oAccel) \
{ \
return (ENVTIECALL(DELMIAMoveHomeActivity,ENVTIETypeLetter,ENVTIELetter)get_Acceleration(oAccel)); \
} \
HRESULT __stdcall  ENVTIEName::put_Acceleration(double iAccel) \
{ \
return (ENVTIECALL(DELMIAMoveHomeActivity,ENVTIETypeLetter,ENVTIELetter)put_Acceleration(iAccel)); \
} \
HRESULT __stdcall  ENVTIEName::get_MotionBasis(CATBSTR & oMotionBasis) \
{ \
return (ENVTIECALL(DELMIAMoveHomeActivity,ENVTIETypeLetter,ENVTIELetter)get_MotionBasis(oMotionBasis)); \
} \
HRESULT __stdcall  ENVTIEName::put_MotionBasis(const CATBSTR & iMotionBasis) \
{ \
return (ENVTIECALL(DELMIAMoveHomeActivity,ENVTIETypeLetter,ENVTIELetter)put_MotionBasis(iMotionBasis)); \
} \
HRESULT __stdcall  ENVTIEName::get_MechanismIndex(short & oMechanismIndex) \
{ \
return (ENVTIECALL(DELMIAMoveHomeActivity,ENVTIETypeLetter,ENVTIELetter)get_MechanismIndex(oMechanismIndex)); \
} \
HRESULT __stdcall  ENVTIEName::put_MechanismIndex(short iMechanismIndex) \
{ \
return (ENVTIECALL(DELMIAMoveHomeActivity,ENVTIETypeLetter,ENVTIELetter)put_MechanismIndex(iMechanismIndex)); \
} \
HRESULT __stdcall  ENVTIEName::IsSubTypeOf(const CATBSTR & iName, CAT_VARIANT_BOOL & oVal) \
{ \
return (ENVTIECALL(DELMIAMoveHomeActivity,ENVTIETypeLetter,ENVTIELetter)IsSubTypeOf(iName,oVal)); \
} \
HRESULT __stdcall  ENVTIEName::AttrValue(const CATVariant & iIndex, CATVariant & oAttVal) \
{ \
return (ENVTIECALL(DELMIAMoveHomeActivity,ENVTIETypeLetter,ENVTIELetter)AttrValue(iIndex,oAttVal)); \
} \
HRESULT __stdcall  ENVTIEName::AttrName(CATLONG iIndex, CATBSTR & oName) \
{ \
return (ENVTIECALL(DELMIAMoveHomeActivity,ENVTIETypeLetter,ENVTIELetter)AttrName(iIndex,oName)); \
} \
HRESULT __stdcall  ENVTIEName::get_Type(CATBSTR & oType) \
{ \
return (ENVTIECALL(DELMIAMoveHomeActivity,ENVTIETypeLetter,ENVTIELetter)get_Type(oType)); \
} \
HRESULT __stdcall  ENVTIEName::get_Description(CATBSTR & oDescriptionBSTR) \
{ \
return (ENVTIECALL(DELMIAMoveHomeActivity,ENVTIETypeLetter,ENVTIELetter)get_Description(oDescriptionBSTR)); \
} \
HRESULT __stdcall  ENVTIEName::put_Description(const CATBSTR & iDescriptionBSTR) \
{ \
return (ENVTIECALL(DELMIAMoveHomeActivity,ENVTIETypeLetter,ENVTIELetter)put_Description(iDescriptionBSTR)); \
} \
HRESULT __stdcall  ENVTIEName::get_CycleTime(double & oCT) \
{ \
return (ENVTIECALL(DELMIAMoveHomeActivity,ENVTIETypeLetter,ENVTIELetter)get_CycleTime(oCT)); \
} \
HRESULT __stdcall  ENVTIEName::put_CycleTime(double iCT) \
{ \
return (ENVTIECALL(DELMIAMoveHomeActivity,ENVTIETypeLetter,ENVTIELetter)put_CycleTime(iCT)); \
} \
HRESULT __stdcall  ENVTIEName::get_CalculatedCycleTime(double & oCCT) \
{ \
return (ENVTIECALL(DELMIAMoveHomeActivity,ENVTIETypeLetter,ENVTIELetter)get_CalculatedCycleTime(oCCT)); \
} \
HRESULT __stdcall  ENVTIEName::get_BeginningDate(double & oBegin) \
{ \
return (ENVTIECALL(DELMIAMoveHomeActivity,ENVTIETypeLetter,ENVTIELetter)get_BeginningDate(oBegin)); \
} \
HRESULT __stdcall  ENVTIEName::put_BeginningDate(double iSBT) \
{ \
return (ENVTIECALL(DELMIAMoveHomeActivity,ENVTIETypeLetter,ENVTIELetter)put_BeginningDate(iSBT)); \
} \
HRESULT __stdcall  ENVTIEName::get_EndDate(double & oEnd) \
{ \
return (ENVTIECALL(DELMIAMoveHomeActivity,ENVTIETypeLetter,ENVTIELetter)get_EndDate(oEnd)); \
} \
HRESULT __stdcall  ENVTIEName::get_ChildrenActivities(CATIAActivities *& oChildren) \
{ \
return (ENVTIECALL(DELMIAMoveHomeActivity,ENVTIETypeLetter,ENVTIELetter)get_ChildrenActivities(oChildren)); \
} \
HRESULT __stdcall  ENVTIEName::CreateChild(const CATBSTR & iTypeOfChild, CATIAActivity *& oCreatedChild) \
{ \
return (ENVTIECALL(DELMIAMoveHomeActivity,ENVTIETypeLetter,ENVTIELetter)CreateChild(iTypeOfChild,oCreatedChild)); \
} \
HRESULT __stdcall  ENVTIEName::CreateLink(CATIAActivity * iSecondActivity) \
{ \
return (ENVTIECALL(DELMIAMoveHomeActivity,ENVTIETypeLetter,ENVTIELetter)CreateLink(iSecondActivity)); \
} \
HRESULT __stdcall  ENVTIEName::RemoveLink(CATIAActivity * iSecondActivity) \
{ \
return (ENVTIECALL(DELMIAMoveHomeActivity,ENVTIETypeLetter,ENVTIELetter)RemoveLink(iSecondActivity)); \
} \
HRESULT __stdcall  ENVTIEName::get_NextCFActivities(CATIAActivities *& oNextCF) \
{ \
return (ENVTIECALL(DELMIAMoveHomeActivity,ENVTIETypeLetter,ENVTIELetter)get_NextCFActivities(oNextCF)); \
} \
HRESULT __stdcall  ENVTIEName::get_PreviousCFActivities(CATIAActivities *& oPreviousCF) \
{ \
return (ENVTIECALL(DELMIAMoveHomeActivity,ENVTIETypeLetter,ENVTIELetter)get_PreviousCFActivities(oPreviousCF)); \
} \
HRESULT __stdcall  ENVTIEName::get_NextPRFActivities(CATIAActivities *& oNextPRF) \
{ \
return (ENVTIECALL(DELMIAMoveHomeActivity,ENVTIETypeLetter,ENVTIELetter)get_NextPRFActivities(oNextPRF)); \
} \
HRESULT __stdcall  ENVTIEName::get_PreviousPRFActivities(CATIAActivities *& oPreviousPRF) \
{ \
return (ENVTIECALL(DELMIAMoveHomeActivity,ENVTIETypeLetter,ENVTIELetter)get_PreviousPRFActivities(oPreviousPRF)); \
} \
HRESULT __stdcall  ENVTIEName::get_AttrCount(CATLONG & oNbAttr) \
{ \
return (ENVTIECALL(DELMIAMoveHomeActivity,ENVTIETypeLetter,ENVTIELetter)get_AttrCount(oNbAttr)); \
} \
HRESULT __stdcall  ENVTIEName::get_Items(CATIAItems *& oItems) \
{ \
return (ENVTIECALL(DELMIAMoveHomeActivity,ENVTIETypeLetter,ENVTIELetter)get_Items(oItems)); \
} \
HRESULT __stdcall  ENVTIEName::get_Outputs(CATIAOutputs *& oOutputs) \
{ \
return (ENVTIECALL(DELMIAMoveHomeActivity,ENVTIETypeLetter,ENVTIELetter)get_Outputs(oOutputs)); \
} \
HRESULT __stdcall  ENVTIEName::get_Resources(CATIAResources *& oResources) \
{ \
return (ENVTIECALL(DELMIAMoveHomeActivity,ENVTIETypeLetter,ENVTIELetter)get_Resources(oResources)); \
} \
HRESULT __stdcall  ENVTIEName::get_Relations(CATIARelations *& oRelations) \
{ \
return (ENVTIECALL(DELMIAMoveHomeActivity,ENVTIETypeLetter,ENVTIELetter)get_Relations(oRelations)); \
} \
HRESULT __stdcall  ENVTIEName::get_Parameters(CATIAParameters *& oParameters) \
{ \
return (ENVTIECALL(DELMIAMoveHomeActivity,ENVTIETypeLetter,ENVTIELetter)get_Parameters(oParameters)); \
} \
HRESULT __stdcall  ENVTIEName::GetTechnologicalObject(const CATBSTR & iApplicationType, CATBaseDispatch *& oApplicativeObj) \
{ \
return (ENVTIECALL(DELMIAMoveHomeActivity,ENVTIETypeLetter,ENVTIELetter)GetTechnologicalObject(iApplicationType,oApplicativeObj)); \
} \
HRESULT __stdcall  ENVTIEName::get_PrecedenceActivities(CATIAActivities *& oActivities) \
{ \
return (ENVTIECALL(DELMIAMoveHomeActivity,ENVTIETypeLetter,ENVTIELetter)get_PrecedenceActivities(oActivities)); \
} \
HRESULT __stdcall  ENVTIEName::get_PossiblePrecedenceActivities(CATIAActivities *& oActivities) \
{ \
return (ENVTIECALL(DELMIAMoveHomeActivity,ENVTIETypeLetter,ENVTIELetter)get_PossiblePrecedenceActivities(oActivities)); \
} \
HRESULT __stdcall  ENVTIEName::get_ProcessID(CATBSTR & oProcessID) \
{ \
return (ENVTIECALL(DELMIAMoveHomeActivity,ENVTIETypeLetter,ENVTIELetter)get_ProcessID(oProcessID)); \
} \
HRESULT __stdcall  ENVTIEName::SetProcessID(const CATBSTR & iProcessID, CAT_VARIANT_BOOL iCheckUnique) \
{ \
return (ENVTIECALL(DELMIAMoveHomeActivity,ENVTIETypeLetter,ENVTIELetter)SetProcessID(iProcessID,iCheckUnique)); \
} \
HRESULT __stdcall  ENVTIEName::get_CalculatedBeginTime(double & oCBT) \
{ \
return (ENVTIECALL(DELMIAMoveHomeActivity,ENVTIETypeLetter,ENVTIELetter)get_CalculatedBeginTime(oCBT)); \
} \
HRESULT __stdcall  ENVTIEName::AddAttr(const CATBSTR & iAttributeName, SPPProcessAttributeType AttrType, const CATBSTR & iAttributePromptName) \
{ \
return (ENVTIECALL(DELMIAMoveHomeActivity,ENVTIETypeLetter,ENVTIELetter)AddAttr(iAttributeName,AttrType,iAttributePromptName)); \
} \
HRESULT __stdcall  ENVTIEName::RemoveAttr(const CATBSTR & iAttributeName) \
{ \
return (ENVTIECALL(DELMIAMoveHomeActivity,ENVTIETypeLetter,ENVTIELetter)RemoveAttr(iAttributeName)); \
} \
HRESULT __stdcall  ENVTIEName::AddActivityConstraint(CATIAActivity * iActivity, SPPProcessConstraintType iConstraintType) \
{ \
return (ENVTIECALL(DELMIAMoveHomeActivity,ENVTIETypeLetter,ENVTIELetter)AddActivityConstraint(iActivity,iConstraintType)); \
} \
HRESULT __stdcall  ENVTIEName::RemoveActivityConstraint(CATIAActivity * iActivity, SPPProcessConstraintType iConstraintType) \
{ \
return (ENVTIECALL(DELMIAMoveHomeActivity,ENVTIETypeLetter,ENVTIELetter)RemoveActivityConstraint(iActivity,iConstraintType)); \
} \
HRESULT __stdcall  ENVTIEName::GetActivityConstraints(SPPProcessConstraintType iConstraintType, CATIAActivities *& oConstrtList) \
{ \
return (ENVTIECALL(DELMIAMoveHomeActivity,ENVTIETypeLetter,ENVTIELetter)GetActivityConstraints(iConstraintType,oConstrtList)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DELMIAMoveHomeActivity,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DELMIAMoveHomeActivity,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DELMIAMoveHomeActivity,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DELMIAMoveHomeActivity,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DELMIAMoveHomeActivity,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DELMIAMoveHomeActivity(classe)    TIEDELMIAMoveHomeActivity##classe


/* Common methods inside a TIE */
#define common_TIE_DELMIAMoveHomeActivity(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DELMIAMoveHomeActivity, classe) \
 \
 \
CATImplementTIEMethods(DELMIAMoveHomeActivity, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DELMIAMoveHomeActivity, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DELMIAMoveHomeActivity, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DELMIAMoveHomeActivity, classe) \
 \
HRESULT __stdcall  TIEDELMIAMoveHomeActivity##classe::get_HomeName(CATBSTR & oHomeName) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_HomeName(oHomeName)); \
} \
HRESULT __stdcall  TIEDELMIAMoveHomeActivity##classe::put_HomeName(const CATBSTR & iHomeName) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_HomeName(iHomeName)); \
} \
HRESULT __stdcall  TIEDELMIAMoveHomeActivity##classe::get_CornerRounding(double & oCornerRounding) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_CornerRounding(oCornerRounding)); \
} \
HRESULT __stdcall  TIEDELMIAMoveHomeActivity##classe::put_CornerRounding(double iCornerRounding) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_CornerRounding(iCornerRounding)); \
} \
HRESULT __stdcall  TIEDELMIAMoveHomeActivity##classe::get_SpeedPercent(double & oSpeedPercent) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_SpeedPercent(oSpeedPercent)); \
} \
HRESULT __stdcall  TIEDELMIAMoveHomeActivity##classe::put_SpeedPercent(double iSpeedPercent) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_SpeedPercent(iSpeedPercent)); \
} \
HRESULT __stdcall  TIEDELMIAMoveHomeActivity##classe::get_Acceleration(double & oAccel) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Acceleration(oAccel)); \
} \
HRESULT __stdcall  TIEDELMIAMoveHomeActivity##classe::put_Acceleration(double iAccel) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Acceleration(iAccel)); \
} \
HRESULT __stdcall  TIEDELMIAMoveHomeActivity##classe::get_MotionBasis(CATBSTR & oMotionBasis) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_MotionBasis(oMotionBasis)); \
} \
HRESULT __stdcall  TIEDELMIAMoveHomeActivity##classe::put_MotionBasis(const CATBSTR & iMotionBasis) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_MotionBasis(iMotionBasis)); \
} \
HRESULT __stdcall  TIEDELMIAMoveHomeActivity##classe::get_MechanismIndex(short & oMechanismIndex) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_MechanismIndex(oMechanismIndex)); \
} \
HRESULT __stdcall  TIEDELMIAMoveHomeActivity##classe::put_MechanismIndex(short iMechanismIndex) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_MechanismIndex(iMechanismIndex)); \
} \
HRESULT __stdcall  TIEDELMIAMoveHomeActivity##classe::IsSubTypeOf(const CATBSTR & iName, CAT_VARIANT_BOOL & oVal) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->IsSubTypeOf(iName,oVal)); \
} \
HRESULT __stdcall  TIEDELMIAMoveHomeActivity##classe::AttrValue(const CATVariant & iIndex, CATVariant & oAttVal) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->AttrValue(iIndex,oAttVal)); \
} \
HRESULT __stdcall  TIEDELMIAMoveHomeActivity##classe::AttrName(CATLONG iIndex, CATBSTR & oName) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->AttrName(iIndex,oName)); \
} \
HRESULT __stdcall  TIEDELMIAMoveHomeActivity##classe::get_Type(CATBSTR & oType) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Type(oType)); \
} \
HRESULT __stdcall  TIEDELMIAMoveHomeActivity##classe::get_Description(CATBSTR & oDescriptionBSTR) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Description(oDescriptionBSTR)); \
} \
HRESULT __stdcall  TIEDELMIAMoveHomeActivity##classe::put_Description(const CATBSTR & iDescriptionBSTR) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Description(iDescriptionBSTR)); \
} \
HRESULT __stdcall  TIEDELMIAMoveHomeActivity##classe::get_CycleTime(double & oCT) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_CycleTime(oCT)); \
} \
HRESULT __stdcall  TIEDELMIAMoveHomeActivity##classe::put_CycleTime(double iCT) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_CycleTime(iCT)); \
} \
HRESULT __stdcall  TIEDELMIAMoveHomeActivity##classe::get_CalculatedCycleTime(double & oCCT) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_CalculatedCycleTime(oCCT)); \
} \
HRESULT __stdcall  TIEDELMIAMoveHomeActivity##classe::get_BeginningDate(double & oBegin) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_BeginningDate(oBegin)); \
} \
HRESULT __stdcall  TIEDELMIAMoveHomeActivity##classe::put_BeginningDate(double iSBT) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_BeginningDate(iSBT)); \
} \
HRESULT __stdcall  TIEDELMIAMoveHomeActivity##classe::get_EndDate(double & oEnd) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_EndDate(oEnd)); \
} \
HRESULT __stdcall  TIEDELMIAMoveHomeActivity##classe::get_ChildrenActivities(CATIAActivities *& oChildren) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_ChildrenActivities(oChildren)); \
} \
HRESULT __stdcall  TIEDELMIAMoveHomeActivity##classe::CreateChild(const CATBSTR & iTypeOfChild, CATIAActivity *& oCreatedChild) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateChild(iTypeOfChild,oCreatedChild)); \
} \
HRESULT __stdcall  TIEDELMIAMoveHomeActivity##classe::CreateLink(CATIAActivity * iSecondActivity) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateLink(iSecondActivity)); \
} \
HRESULT __stdcall  TIEDELMIAMoveHomeActivity##classe::RemoveLink(CATIAActivity * iSecondActivity) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->RemoveLink(iSecondActivity)); \
} \
HRESULT __stdcall  TIEDELMIAMoveHomeActivity##classe::get_NextCFActivities(CATIAActivities *& oNextCF) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_NextCFActivities(oNextCF)); \
} \
HRESULT __stdcall  TIEDELMIAMoveHomeActivity##classe::get_PreviousCFActivities(CATIAActivities *& oPreviousCF) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_PreviousCFActivities(oPreviousCF)); \
} \
HRESULT __stdcall  TIEDELMIAMoveHomeActivity##classe::get_NextPRFActivities(CATIAActivities *& oNextPRF) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_NextPRFActivities(oNextPRF)); \
} \
HRESULT __stdcall  TIEDELMIAMoveHomeActivity##classe::get_PreviousPRFActivities(CATIAActivities *& oPreviousPRF) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_PreviousPRFActivities(oPreviousPRF)); \
} \
HRESULT __stdcall  TIEDELMIAMoveHomeActivity##classe::get_AttrCount(CATLONG & oNbAttr) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_AttrCount(oNbAttr)); \
} \
HRESULT __stdcall  TIEDELMIAMoveHomeActivity##classe::get_Items(CATIAItems *& oItems) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Items(oItems)); \
} \
HRESULT __stdcall  TIEDELMIAMoveHomeActivity##classe::get_Outputs(CATIAOutputs *& oOutputs) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Outputs(oOutputs)); \
} \
HRESULT __stdcall  TIEDELMIAMoveHomeActivity##classe::get_Resources(CATIAResources *& oResources) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Resources(oResources)); \
} \
HRESULT __stdcall  TIEDELMIAMoveHomeActivity##classe::get_Relations(CATIARelations *& oRelations) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Relations(oRelations)); \
} \
HRESULT __stdcall  TIEDELMIAMoveHomeActivity##classe::get_Parameters(CATIAParameters *& oParameters) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parameters(oParameters)); \
} \
HRESULT __stdcall  TIEDELMIAMoveHomeActivity##classe::GetTechnologicalObject(const CATBSTR & iApplicationType, CATBaseDispatch *& oApplicativeObj) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetTechnologicalObject(iApplicationType,oApplicativeObj)); \
} \
HRESULT __stdcall  TIEDELMIAMoveHomeActivity##classe::get_PrecedenceActivities(CATIAActivities *& oActivities) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_PrecedenceActivities(oActivities)); \
} \
HRESULT __stdcall  TIEDELMIAMoveHomeActivity##classe::get_PossiblePrecedenceActivities(CATIAActivities *& oActivities) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_PossiblePrecedenceActivities(oActivities)); \
} \
HRESULT __stdcall  TIEDELMIAMoveHomeActivity##classe::get_ProcessID(CATBSTR & oProcessID) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_ProcessID(oProcessID)); \
} \
HRESULT __stdcall  TIEDELMIAMoveHomeActivity##classe::SetProcessID(const CATBSTR & iProcessID, CAT_VARIANT_BOOL iCheckUnique) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetProcessID(iProcessID,iCheckUnique)); \
} \
HRESULT __stdcall  TIEDELMIAMoveHomeActivity##classe::get_CalculatedBeginTime(double & oCBT) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_CalculatedBeginTime(oCBT)); \
} \
HRESULT __stdcall  TIEDELMIAMoveHomeActivity##classe::AddAttr(const CATBSTR & iAttributeName, SPPProcessAttributeType AttrType, const CATBSTR & iAttributePromptName) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->AddAttr(iAttributeName,AttrType,iAttributePromptName)); \
} \
HRESULT __stdcall  TIEDELMIAMoveHomeActivity##classe::RemoveAttr(const CATBSTR & iAttributeName) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->RemoveAttr(iAttributeName)); \
} \
HRESULT __stdcall  TIEDELMIAMoveHomeActivity##classe::AddActivityConstraint(CATIAActivity * iActivity, SPPProcessConstraintType iConstraintType) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->AddActivityConstraint(iActivity,iConstraintType)); \
} \
HRESULT __stdcall  TIEDELMIAMoveHomeActivity##classe::RemoveActivityConstraint(CATIAActivity * iActivity, SPPProcessConstraintType iConstraintType) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->RemoveActivityConstraint(iActivity,iConstraintType)); \
} \
HRESULT __stdcall  TIEDELMIAMoveHomeActivity##classe::GetActivityConstraints(SPPProcessConstraintType iConstraintType, CATIAActivities *& oConstrtList) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetActivityConstraints(iConstraintType,oConstrtList)); \
} \
HRESULT  __stdcall  TIEDELMIAMoveHomeActivity##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication)); \
} \
HRESULT  __stdcall  TIEDELMIAMoveHomeActivity##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent)); \
} \
HRESULT  __stdcall  TIEDELMIAMoveHomeActivity##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  TIEDELMIAMoveHomeActivity##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  TIEDELMIAMoveHomeActivity##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj)); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DELMIAMoveHomeActivity(classe) \
 \
 \
declare_TIE_DELMIAMoveHomeActivity(classe) \
 \
 \
CATMetaClass * __stdcall TIEDELMIAMoveHomeActivity##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DELMIAMoveHomeActivity,"DELMIAMoveHomeActivity",DELMIAMoveHomeActivity::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DELMIAMoveHomeActivity(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DELMIAMoveHomeActivity, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDELMIAMoveHomeActivity##classe(classe::MetaObject(),DELMIAMoveHomeActivity::MetaObject(),(void *)CreateTIEDELMIAMoveHomeActivity##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DELMIAMoveHomeActivity(classe) \
 \
 \
declare_TIE_DELMIAMoveHomeActivity(classe) \
 \
 \
CATMetaClass * __stdcall TIEDELMIAMoveHomeActivity##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DELMIAMoveHomeActivity,"DELMIAMoveHomeActivity",DELMIAMoveHomeActivity::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DELMIAMoveHomeActivity(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DELMIAMoveHomeActivity, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDELMIAMoveHomeActivity##classe(classe::MetaObject(),DELMIAMoveHomeActivity::MetaObject(),(void *)CreateTIEDELMIAMoveHomeActivity##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DELMIAMoveHomeActivity(classe) TIE_DELMIAMoveHomeActivity(classe)
#else
#define BOA_DELMIAMoveHomeActivity(classe) CATImplementBOA(DELMIAMoveHomeActivity, classe)
#endif

#endif
