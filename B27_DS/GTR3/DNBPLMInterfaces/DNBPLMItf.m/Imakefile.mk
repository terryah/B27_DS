# COPYRIGHT DASSAULT SYSTEMES 2003
#======================================================================
# Imakefile for module DNBPLMItf.m
#======================================================================
#
#  Dec 2003  Creation: Code generated by the CAA wizard  yms
#======================================================================
#
# SHARED LIBRARY 
#
BUILT_OBJECT_TYPE=SHARED LIBRARY 
 
# DO NOT EDIT :: THE CAA2 WIZARDS WILL ADD CODE HERE
WIZARD_LINK_MODULES =  \
JS0FM JS0GROUP DNBPLMInterfacesUUID 
# END WIZARD EDITION ZONE

LINK_WITH = $(WIZARD_LINK_MODULES)

# System dependant variables
#
OS = AIX
#
OS = HP-UX
#
OS = IRIX
#
OS = SunOS
#
OS = Windows_NT
