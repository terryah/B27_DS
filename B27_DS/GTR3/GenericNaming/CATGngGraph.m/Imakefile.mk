#ifdef LOCAL_DEFINITION_FOR_IID
LINK_WITH_FOR_IID = \
SystemUUID \
ObjectSpecsModelerUUID \
ObjectModelerBaseUUID  \
#else
LINK_WITH_FOR_IID =
#endif
BUILT_OBJECT_TYPE=SHARED LIBRARY
INCLUDED_MODULES = GN0GRAPH GN0ATTRIBUTES

LINK_WITH=$(LINK_WITH_FOR_IID)  GN0NAME \
            Collections \
            JS0GROUP \
	        AC0SPBAS \
            NS0SI18N \
			AD0XXBAS \
			NS0S3STR \
			CATCdbEntity \
			AC0CATPL \
			DataAdmin \
			JS0CORBA \
			CATMathematics \
			YI00IMPL \
			YP00IMPL \
			CATSysAllocator \
			CATMathStream \
			CATOmxBase
