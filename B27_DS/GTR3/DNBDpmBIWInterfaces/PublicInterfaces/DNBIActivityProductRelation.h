#ifndef DNBIActivityProductRelation_H
#define DNBIActivityProductRelation_H

// COPYRIGHT DASSAULT SYSTEMES, 2006,2007

/**
 * @CAA2Level L0
 * @CAA2Usage U3
 */

#include "DpmFastenerInf.h"

// System 
#include "CATBaseUnknown.h"
#include "CATLISTV_CATBaseUnknown.h"

// Class forward declarations
class CATUnicodeString;


extern ExportedByDpmFastenerInf IID IID_DNBIActivityProductRelation;

//------------------------------------------------------------------

/** INTERFACE : 
  *   <b>DNBIActivityProductRelation</b>
  *<br>
  * DESCRIPTION :
  *   Methods to get the relationship 
  *   between an activity and assigned products.
  *   Only implemented for BIWActivityWelding, BIWActivityStation and BIWActivityGrouping.
  */
class ExportedByDpmFastenerInf DNBIActivityProductRelation: public CATBaseUnknown
{
  CATDeclareInterface;

  public:

     /**
      * METHODS :
      *  GetTypeString
      *
      * DESCRIPTION
      *  This method passes back the type of this activity.
      *
      * @param oType
      *  CATUnicodeString with the type of the activity.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT GetTypeString(CATUnicodeString& oType) = 0;

     /**
      * METHODS :
      *  GetInputProducts
      *
      * DESCRIPTION
      *  This method passes back the product inputs of this activity.
      *
      * @param oInputProducts
      *  List of CATBaseUnknown with the product inputs of the activity.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT GetInputProducts(CATListValCATBaseUnknown_var& oInputProducts) = 0;

     /**
      * METHODS :
      *  GetOutputProducts
      *
      * DESCRIPTION
      *  This method passes back the product outputs of this activity.
      *
      * @param oOutputProducts
      *  List of CATBaseUnknown with the product outputs of the activity.
      *  First element in the list is called SETUP, don't know what this is used for.
      *  Output products will start at second element of the list.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT GetOutputProducts(CATListValCATBaseUnknown_var& oOutputProducts) = 0;


     // No constructors or destructors on this pure virtual base class
     // --------------------------------------------------------------
};

// Handler definition
CATDeclareHandler(DNBIActivityProductRelation, CATBaseUnknown);
//------------------------------------------------------------------

#endif
