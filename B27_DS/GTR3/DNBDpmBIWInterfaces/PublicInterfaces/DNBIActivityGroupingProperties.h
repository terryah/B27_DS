#ifndef DNBIActivityGroupingProperties_H
#define DNBIActivityGroupingProperties_H

// COPYRIGHT DASSAULT SYSTEMES, 2006,2007

/**
 * @CAA2Level L0
 * @CAA2Usage U3
 */

#include "DpmFastenerInf.h"

// System 
#include "CATBaseUnknown.h"

// Class forward declarations
class CATUnicodeString;
class CATListValCATUnicodeString;
class CATRawCollPV;


extern ExportedByDpmFastenerInf IID IID_DNBIActivityGroupingProperties;

//------------------------------------------------------------------

/** INTERFACE : 
  *   <b>DNBIActivityGroupingProperties</b>
  *<br>
  * DESCRIPTION :
  *   This interfaces provides methods to get some properties of grouping activity.
  */
class ExportedByDpmFastenerInf DNBIActivityGroupingProperties: public CATBaseUnknown
{
  CATDeclareInterface;

  public:

     /**
      * METHODS :
      *  GetTypeString
      *
      * DESCRIPTION
      *  This method passes back the type of this activity.
      *
      * @param oType
      *  CATUnicodeString with the type of the activity.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT GetTypeString(CATUnicodeString& oType) = 0;

     /**
      * METHODS :
      *  CalculatePointFastenerCapacity
      *
      * DESCRIPTION
      *  This method calculates point fastener capacity of the recursive child fastening activites.
      *
      * @param oCapacity
      *  Double value with the calculated capacity.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT CalculatePointFastenerCapacity(double& oCapacity) = 0;

     /**
      * METHODS :
      *  GetTimeBetweenProductExits
      *
      * DESCRIPTION
      *  This method passes back the time between product exits attribute.
      *
      * @param oTime
      *  Double value with the time between product exits attribute.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT GetTimeBetweenProductExits(double& oTime) = 0;

     /**
      * METHODS :
      *  GetCycleTime
      *
      * DESCRIPTION
      *  This method passes back the cycle time.
      *
      * @param oTime
      *  Double value with the cycle time.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT GetCycleTime(double& oTime) = 0;

     /**
      * METHODS :
      *  GetTargetCost
      *
      * DESCRIPTION
      *  This method passes back the target cost.
      *
      * @param oTargetCost
      *  Double value with the target cost.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT GetTargetCost(double& oTargetCost) = 0;

     /**
      * METHODS :
      *  GetGoalInvestmentCost
      *
      * DESCRIPTION
      *  This method passes back the goal investment cost.
      *
      * @param oGoalInvestmentCost
      *  Double value with the goal investment cost.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT GetGoalInvestmentCost(double& oGoalInvestmentCost) = 0;

     /**
      * METHODS :
      *  CalculateTotalInvestmentCost
      *
      * DESCRIPTION
      *  This method calculates the total investment cost.
      *
      * @param oTotalInvestmentCost
      *  Double value with the total investment cost.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT CalculateTotalInvestmentCost(double& oTotalInvestmentCost) = 0;

     /**
      * METHODS :
      *  GetTargetCost
      *
      * DESCRIPTION
      *  This method passes back the sum of number of specified point fasteners on child point 
      *  fastening activities.
      *
      * @param oSumNumber
      *  Double value with the sum of number of specified point fasteners on child point 
      *  fastening activities.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT SumChildGoalNumberPointFasteners(double& oSumNumber) = 0;

     /**
      * METHODS :
      *  SumNumberSpecifiedOnPointFasteningActivities
      *
      * DESCRIPTION
      *  This method passes back the sum of number of specified point fasteners 
      *  on child point fastening activities.
      *
      * @param oSumOfNumber
      *  Double value with the sum of number of specified point fasteners
      *  on child point fastening activities.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT SumNumberSpecifiedOnPointFasteningActivities(double& oSumOfNumber) = 0;

     /**
      * METHODS :
      *  ListActivityAttributeNamesAndValues
      *
      * DESCRIPTION
      *  This method passes back names and values of activity attributes.
      *
      * @param oListAttributeNames
      *  List of CATUnicodeString with attribute names.
      * @param oListAttributeValues
      *  List of void* with attribute values.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT ListActivityAttributeNamesAndValues(CATListValCATUnicodeString& oListAttributeNames,
                                                         CATRawCollPV& oListAttributeValues) = 0;
     
     // No constructors or destructors on this pure virtual base class
     // --------------------------------------------------------------
};
// Handler definition
CATDeclareHandler(DNBIActivityGroupingProperties, CATBaseUnknown);
//------------------------------------------------------------------

#endif
