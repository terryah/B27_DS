#ifndef DNBIActivityWeldingProperties_H
#define DNBIActivityWeldingProperties_H

// COPYRIGHT DASSAULT SYSTEMES, 2006,2007

/**
 * @CAA2Level L0
 * @CAA2Usage U3
 */

#include "DpmFastenerInf.h"

// System
#include "CATBaseUnknown.h"

// Class forward declarations
class CATUnicodeString;
class CATListValCATUnicodeString;
class CATRawCollPV;

extern ExportedByDpmFastenerInf IID IID_DNBIActivityWeldingProperties;

//------------------------------------------------------------------

/** INTERFACE : 
  *   <b>DNBIActivityWeldingProperties</b>
  *<br>
  * DESCRIPTION :
  *   This interfaces provides methods to get some properties of welding activity.
  */
class ExportedByDpmFastenerInf DNBIActivityWeldingProperties: public CATBaseUnknown
{
  CATDeclareInterface;

  public:

     /**
      * METHODS :
      *  GetTypeString
      *
      * DESCRIPTION
      *  This method passes back the type of this activity.
      *
      * @param oType
      *  CATUnicodeString with the type of the activity.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT GetTypeString(CATUnicodeString& oType) = 0;

     /**
      * METHODS :
      *  CalculatePointFastenerCapacity
      *
      * DESCRIPTION
      *  This method calculates amount of fastening that can be done in available time.
      *
      * @param iTimeAvailable
      *  Double value with the available time.
      * @param oCapacity
      *  Double value with the calculated capacity.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT CalculatePointFastenerCapacity(const double& iTimeAvailable, double& oCapacity) = 0;

     /**
      * METHODS :
      *  CalculateCycleTime
      *
      * DESCRIPTION
      *  This method calculates the cycle time of the activity.
      *
      * @param oCycleTime
      *  Double value with the calculated cycle time.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT CalculateCycleTime(double& oCycleTime) = 0;

     /**
      * METHODS :
      *  CalculateCycleTime
      *
      * DESCRIPTION
      *  This method passes back the rate of point fastening.
      *
      * @param oFasteningRate
      *  Double value with the rate of point fastening.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT GetPointFasteningRate(double& oFasteningRate) = 0;

     /**
      * METHODS :
      *  ListActivityAttributeNamesAndValues
      *
      * DESCRIPTION
      *  This method passes back names and values of activity attributes.
      *
      * @param oListAttributeNames
      *  List of CATUnicodeString with attribute names.
      * @param oListAttributeValues
      *  List of void* with attribute values.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT ListActivityAttributeNamesAndValues(CATListValCATUnicodeString& oListAttributeNames,
                                                         CATRawCollPV& oListAttributeValues) = 0;

    

     // No constructors or destructors on this pure virtual base class
     // --------------------------------------------------------------
};
CATDeclareHandler(DNBIActivityWeldingProperties, CATBaseUnknown);
//------------------------------------------------------------------

#endif
