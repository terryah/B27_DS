#ifndef DNBIActivityStationProperties_H
#define DNBIActivityStationProperties_H

// COPYRIGHT DASSAULT SYSTEMES, 2006,2007

/**
 * @CAA2Level L0
 * @CAA2Usage U3
 */

#include "DpmFastenerInf.h"

// System 
#include "CATBaseUnknown.h"

// Class forward declarations
class CATUnicodeString;
class CATListValCATUnicodeString;
class CATRawCollPV;

extern ExportedByDpmFastenerInf IID IID_DNBIActivityStationProperties;

//------------------------------------------------------------------

/** INTERFACE : 
  *   <b>DNBIActivityStationProperties</b>
  *<br>
  * DESCRIPTION :
  *   This interfaces provides methods to get some properties of station activity.
  */
class ExportedByDpmFastenerInf DNBIActivityStationProperties: public CATBaseUnknown
{
  CATDeclareInterface;

  public:

     /**
      * METHODS :
      *  GetTypeString
      *
      * DESCRIPTION
      *  This method passes back the type of this activity.
      *
      * @param oType
      *  CATUnicodeString with the type of the activity.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT GetTypeString(CATUnicodeString& oType) = 0;

     /**
      * METHODS :
      *  CalculatePointFastenerCapacity
      *
      * DESCRIPTION
      *  This method calculates the point fastener production capacity of the station 
      *  by summing the capacity of the child point fastening activities.
      *
      * @param oCapacity
      *  Double value with the calculated capacity.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT CalculatePointFastenerCapacity(double& oCapacity) = 0;

     /**
      * METHODS :
      *  CalculateAvailableValuedAddedTime
      *
      * DESCRIPTION
      *  This method calculates available value added time by summing cycle times of 
      *  non-fastening child activities and subtracting value from station's cycle time.
      *
      * @param oTime
      *  Double value with the calculated time.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT CalculateAvailableValuedAddedTime(double& oTime) = 0;

     /**
      * METHODS :
      *  GetEstimatedInvestmentCost
      *
      * DESCRIPTION
      *  This method passes back the estimated investment cost.
      *
      * @param oInvestmentCost
      *  Double value with the estimated investment cost.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT GetEstimatedInvestmentCost(double& oInvestmentCost) = 0;

     /**
      * METHODS :
      *  GetStationType
      *
      * DESCRIPTION
      *  This method passes back the station type.
      *
      * @param oStationType
      *  CATUnicodeString with the station type.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT GetStationType(CATUnicodeString& oStationType) = 0;

     /**
      * METHODS :
      *  SumNumberSpecifiedOnPointFasteningActivities
      *
      * DESCRIPTION
      *  This method passes back the sum of number of specified point fasteners 
      *  on child point fastening activities.
      *
      * @param oSumOfNumber
      *  Double value with the sum of number of specified point fasteners
      *  on child point fastening activities.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT SumNumberSpecifiedOnPointFasteningActivities(double& oSumOfNumber) = 0;

     /**
      * METHODS :
      *  GetTimeBetweenProductExits
      *
      * DESCRIPTION
      *  This method passes back the time between product exits attribute.
      *
      * @param oTime
      *  Double value with the time between product exits attribute.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT GetTimeBetweenProductExits(double& oTime) = 0;

     /**
      * METHODS :
      *  ListActivityAttributeNamesAndValues
      *
      * DESCRIPTION
      *  This method passes back names and values of activity attributes.
      *
      * @param oListAttributeNames
      *  List of CATUnicodeString with attribute names.
      * @param oListAttributeValues
      *  List of void* with attribute values.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT ListActivityAttributeNamesAndValues(CATListValCATUnicodeString& oListAttributeNames,
                                                         CATRawCollPV& oListAttributeValues) = 0;
     
     // No constructors or destructors on this pure virtual base class
     // --------------------------------------------------------------
};
// Handler definition
CATDeclareHandler(DNBIActivityStationProperties, CATBaseUnknown);
//------------------------------------------------------------------

#endif
