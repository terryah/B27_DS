#ifndef DNBIActivityFastenerRelation_H
#define DNBIActivityFastenerRelation_H

// COPYRIGHT DASSAULT SYSTEMES, 2006,2007

/**
 * @CAA2Level L0
 * @CAA2Usage U3
 */

#include "DpmFastenerInf.h"

// System 
#include "CATBaseUnknown.h"
#include "CATLISTV_CATBaseUnknown.h"

// Class forward declarations
class CATUnicodeString;


extern ExportedByDpmFastenerInf IID IID_DNBIActivityFastenerRelation;

//------------------------------------------------------------------

/** INTERFACE : 
  *   <b>DNBIActivityFastenerRelation</b>
  *<br>
  * DESCRIPTION :
  *   Methods to get the relationship 
  *   between an activity and assigned fasteners.
  *   Only implemented for BIWActivityWelding, BIWActivityStation and BIWActivityGrouping.
  */
class ExportedByDpmFastenerInf DNBIActivityFastenerRelation: public CATBaseUnknown
{
  CATDeclareInterface;

  public:
     
     /**
      * METHODS :
      *  GetTypeString
      *
      * DESCRIPTION
      *  This method passes back the type of this activity.
      *
      * @param oType
      *  CATUnicodeString with the type of the activity.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT GetTypeString(CATUnicodeString& oType) = 0;

     /**
      * METHODS :
      *  GetNumberAssignedPointFasteners
      *
      * DESCRIPTION
      *  This method passes back the number of assigned point fasteners.
      *
      * @param oNumberAssignedPointFasteners
      *  Double value with the number of assigned point fasteners.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT GetNumberAssignedPointFasteners(double& oNumberAssignedPointFasteners) = 0;

     /**
      * METHODS :
      *  GetListOfAssignedPointFasteners
      *
      * DESCRIPTION
      *  This method passes back thelist of assigned point fasteners.
      *
      * @param oListAssignedPointFasteners
      *  List of CATBaseUnknown with all assigned point fasteners of this activity.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT GetListOfAssignedPointFasteners(CATListValCATBaseUnknown_var& oListAssignedPointFasteners) = 0;

     /**
      * METHODS :
      *  GetNumberSpecifiedPointFasteners
      *
      * DESCRIPTION
      *  This method passes back the number of assigned point fasteners.
      *
      * @param oNumberSpecifiedPointFasteners
      *  Double value with the number of specified point fasteners.
      * @return <i>HRESULT</i>
      *  Returns S_OK if data was collected successfully.
      */
     virtual HRESULT GetNumberSpecifiedPointFasteners(double& oNumberSpecifiedPointFasteners) = 0;
     
     
     // No constructors or destructors on this pure virtual base class
     // --------------------------------------------------------------
};

// Handler definition
CATDeclareHandler(DNBIActivityFastenerRelation, CATBaseUnknown);
//------------------------------------------------------------------

#endif

