#ifndef __TIE_DELMIABIWActivityUpdateServices
#define __TIE_DELMIABIWActivityUpdateServices

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DELMIABIWActivityUpdateServices.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DELMIABIWActivityUpdateServices */
#define declare_TIE_DELMIABIWActivityUpdateServices(classe) \
 \
 \
class TIEDELMIABIWActivityUpdateServices##classe : public DELMIABIWActivityUpdateServices \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DELMIABIWActivityUpdateServices, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall UpdateOutputForActivity(CATIAActivity * iActivity); \
      virtual HRESULT __stdcall UpdateOutputForAllActivity(); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DELMIABIWActivityUpdateServices(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall UpdateOutputForActivity(CATIAActivity * iActivity); \
virtual HRESULT __stdcall UpdateOutputForAllActivity(); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DELMIABIWActivityUpdateServices(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::UpdateOutputForActivity(CATIAActivity * iActivity) \
{ \
return (ENVTIECALL(DELMIABIWActivityUpdateServices,ENVTIETypeLetter,ENVTIELetter)UpdateOutputForActivity(iActivity)); \
} \
HRESULT __stdcall  ENVTIEName::UpdateOutputForAllActivity() \
{ \
return (ENVTIECALL(DELMIABIWActivityUpdateServices,ENVTIETypeLetter,ENVTIELetter)UpdateOutputForAllActivity()); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DELMIABIWActivityUpdateServices,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DELMIABIWActivityUpdateServices,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DELMIABIWActivityUpdateServices,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DELMIABIWActivityUpdateServices,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DELMIABIWActivityUpdateServices,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DELMIABIWActivityUpdateServices(classe)    TIEDELMIABIWActivityUpdateServices##classe


/* Common methods inside a TIE */
#define common_TIE_DELMIABIWActivityUpdateServices(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DELMIABIWActivityUpdateServices, classe) \
 \
 \
CATImplementTIEMethods(DELMIABIWActivityUpdateServices, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DELMIABIWActivityUpdateServices, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DELMIABIWActivityUpdateServices, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DELMIABIWActivityUpdateServices, classe) \
 \
HRESULT __stdcall  TIEDELMIABIWActivityUpdateServices##classe::UpdateOutputForActivity(CATIAActivity * iActivity) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&iActivity); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->UpdateOutputForActivity(iActivity); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&iActivity); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIABIWActivityUpdateServices##classe::UpdateOutputForAllActivity() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->UpdateOutputForAllActivity(); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIABIWActivityUpdateServices##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIABIWActivityUpdateServices##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIABIWActivityUpdateServices##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIABIWActivityUpdateServices##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIABIWActivityUpdateServices##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DELMIABIWActivityUpdateServices(classe) \
 \
 \
declare_TIE_DELMIABIWActivityUpdateServices(classe) \
 \
 \
CATMetaClass * __stdcall TIEDELMIABIWActivityUpdateServices##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DELMIABIWActivityUpdateServices,"DELMIABIWActivityUpdateServices",DELMIABIWActivityUpdateServices::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DELMIABIWActivityUpdateServices(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DELMIABIWActivityUpdateServices, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDELMIABIWActivityUpdateServices##classe(classe::MetaObject(),DELMIABIWActivityUpdateServices::MetaObject(),(void *)CreateTIEDELMIABIWActivityUpdateServices##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DELMIABIWActivityUpdateServices(classe) \
 \
 \
declare_TIE_DELMIABIWActivityUpdateServices(classe) \
 \
 \
CATMetaClass * __stdcall TIEDELMIABIWActivityUpdateServices##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DELMIABIWActivityUpdateServices,"DELMIABIWActivityUpdateServices",DELMIABIWActivityUpdateServices::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DELMIABIWActivityUpdateServices(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DELMIABIWActivityUpdateServices, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDELMIABIWActivityUpdateServices##classe(classe::MetaObject(),DELMIABIWActivityUpdateServices::MetaObject(),(void *)CreateTIEDELMIABIWActivityUpdateServices##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DELMIABIWActivityUpdateServices(classe) TIE_DELMIABIWActivityUpdateServices(classe)
#else
#define BOA_DELMIABIWActivityUpdateServices(classe) CATImplementBOA(DELMIABIWActivityUpdateServices, classe)
#endif

#endif
