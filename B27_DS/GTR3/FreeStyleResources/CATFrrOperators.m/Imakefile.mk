#@ autoformat 12:03:19
#
# SHARED LIBRARY
#
BUILT_OBJECT_TYPE = NONE

LINK_WITH = \
    AdvancedMathematics            \ # AdvancedMathematics            CATAdvancedMathematics
    FrFTopologicalOpe              \ # AdvancedTopologicalOpe         CATAdvancedTopologicalOpe
    PowerTopologicalOpe            \ # AdvancedTopologicalOpe         CATAdvancedTopologicalOpe
    CD0FRAME                       \ # ApplicationFrame               CATApplicationFrame
    BasicTopology                  \ # BasicTopologicalOpe            CATBasicTopologicalOpe
    TopoNurbsTools                 \ # CurveBasedTopoOperators        CATCurveBasedTopoOperators
    DI0PANV2                       \ # Dialog                         DI0PANV2
    FrFAdvancedObjects             \ # FreeFormOperators              CATFreeFormOperators
    FrFAdvancedOpeSur              \ # FreeFormOperators              CATFreeFormOperators
    FrFFitting                     \ # FreeFormOperators              CATFreeFormOperators
    FrFObjects                     \ # FreeFormOperators              CATFreeFormOperators
    FrFOpeCrv                      \ # FreeFormOperators              CATFreeFormOperators
    FrFOpeSur                      \ # FreeFormOperators              CATFreeFormOperators
    PowerFrFOperators              \ # FreeFormOperators              CATFreeFormOperators
    CATFreeFormOperatorsFFit       \ # FreeFormOperators              CATFreeFormOperatorsFFit
    GN0GRAPH                       \ # GenericNaming                  CATGngGraph
    YP00IMPL                       \ # GeometricObjects               CATGeometricObjects
    Y300IINT                       \ # GeometricOperators             CATGeometricOperators
    Y30E3PMG                       \ # GeometricOperators             CATGeometricOperators
    Y30UIUTI                       \ # GeometricOperators             CATGeometricOperators
    Mathematics                    \ # Mathematics                    CATMathematics
    YN000MAT                       \ # Mathematics                    CATMathematics
    YN000MFL                       \ # Mathematics                    CATMathematics
    BOOPER                         \ # NewTopologicalObjects          CATTopologicalObjects
    CATTopologicalObjects          \ # NewTopologicalObjects          CATTopologicalObjects
    YI00IMPL                       \ # NewTopologicalObjects          CATTopologicalObjects
    AD0XXBAS                       \ # ObjectModelerBase              CATObjectModelerBase
    AC0SPBAS                       \ # ObjectSpecsModeler             CATObjectSpecsModeler
    JS0FM                          \ # System                         JS0FM
    CO0LSTPV                       \ # System                         JS0GROUP
    CO0LSTST                       \ # System                         JS0GROUP
    CO0RCINT                       \ # System                         JS0GROUP
    JS03TRA                        \ # System                         JS0GROUP
    JS0CORBA                       \ # System                         JS0GROUP
    JS0SCBAK                       \ # System                         JS0GROUP
    JS0STR                         \ # System                         JS0GROUP
    NS0S3STR                       \ # System                         JS0GROUP
    TessAPI                        \ # Tessellation                   CATTessellation
    BODYNOPE                       \ # TopologicalOperators           CATTopologicalOperators
    CATTopologicalOperators        \ # TopologicalOperators           CATTopologicalOperators
    HybBoolean                     \ # TopologicalOperators           CATTopologicalOperators
    Primitives                     \ # TopologicalOperators           CATTopologicalOperators
    VE0BASE                        \ # Visualization                  CATVisualization
#
            

OS = AIX
SYS_INCPATH = 
SYS_LIBS = 
SYS_LIBPATH = 

OS = HP-UX
SYS_INCPATH =  
SYS_LIBS = 
SYS_LIBPATH =

OS = IRIX
SYS_INCPATH = 
SYS_LIBS = 
SYS_LIBPATH =

OS=irix_a64
LOCAL_CCFLAGS=-G0
