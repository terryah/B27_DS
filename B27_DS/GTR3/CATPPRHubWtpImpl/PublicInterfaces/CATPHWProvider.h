// COPYRIGHT Dassault Systemes 2002
//===================================================================
//
// CATPHWProvider.h
// Provide implementation to interface
//    CATIPPRHubProvider (adapter)
//
//===================================================================
//
// Usage notes:
//
//===================================================================
//CAA2 Wizard Generation Report
//   IMPLEMENTATION
//   TIE: CATIPPRHubProvider
//End CAA2 Wizard Generation Report
//
//  Jun 2002  Creation: Code generated by the CAA wizard  PCM
//===================================================================
#ifndef CATPHWProvider_H
#define CATPHWProvider_H

/**
  * @CAA2Level L0
  * @CAA2Usage U2
**/

#include "CATPPRHubWtpBase.h"
#include "CATBaseUnknown.h"
class CATListPtrCATBaseUnknown;
class CATUnicodeString;
class CATString;
class CATListValCATString;
class CATListPtrCATIPPRHubObject;
class CATPPRHubURL;
class CATPPRHubObjectDescriptor;
//-----------------------------------------------------------------------

/**
 * Class Object modeler implementation class.
 * <br>
 * It implements the interfaces :
 *  <ol>
 *  <li>@see CATPPRHubWtpInterfaces.CATIPPRHubProvider
 *  </ol>
 * Using this prefered syntax will enable mkdoc to document your class.
 */
class ExportedByCATPPRHubWtpBase CATPHWProvider: public CATBaseUnknown
{
  CATDeclareClass;

  public:

  // Standard constructors and destructors for an implementation class
  // -----------------------------------------------------------------
     CATPHWProvider ();
     virtual ~CATPHWProvider ();

  
    virtual HRESULT GetURL(CATPPRHubURL **oppURL) const ;

    virtual HRESULT Connect(CATUnicodeString *ipUser,
      CATUnicodeString *ipPwd,
      CATUnicodeString *ipRole,
      CATUnicodeString *ipServer,
      CATUnicodeString *ipPort,
      void** oppToken)  ;
    virtual HRESULT Ping();

    HRESULT SetURL(const CATPPRHubURL *ipURL);

	HRESULT GetObjectFromDesc(const CATPPRHubObjectDescriptor *iDesc, CATListPtrCATBaseUnknown ** oPPRObj);
  
  protected:

  CATPPRHubURL* _pURL;

  private:
  // The copy constructor and the equal operator must not be implemented
  // -------------------------------------------------------------------
  CATPHWProvider (CATPHWProvider &);
  CATPHWProvider& operator=(CATPHWProvider&);

};

//-----------------------------------------------------------------------

#endif
