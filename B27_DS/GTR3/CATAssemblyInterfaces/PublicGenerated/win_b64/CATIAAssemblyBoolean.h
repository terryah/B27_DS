/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAAssemblyBoolean_h
#define CATIAAssemblyBoolean_h

#ifndef ExportedByCATAssemblyPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __CATAssemblyPubIDL
#define ExportedByCATAssemblyPubIDL __declspec(dllexport)
#else
#define ExportedByCATAssemblyPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByCATAssemblyPubIDL
#endif
#endif

#include "CATIAAssemblyFeature.h"

class CATIABody;
class CATIAProduct;

extern ExportedByCATAssemblyPubIDL IID IID_CATIAAssemblyBoolean;

class ExportedByCATAssemblyPubIDL CATIAAssemblyBoolean : public CATIAAssemblyFeature
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_Body(CATIABody *& oBody)=0;

    virtual HRESULT __stdcall get_BodyComponent(CATIAProduct *& oBodyComp)=0;


};

CATDeclareHandler(CATIAAssemblyBoolean, CATIAAssemblyFeature);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIABase.h"
#include "CATSafeArray.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
