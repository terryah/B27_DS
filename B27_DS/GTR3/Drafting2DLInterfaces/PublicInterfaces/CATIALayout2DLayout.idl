// COPYRIGHT DASSAULT SYSTEMES 2005
#ifndef CATIALayout2DLayout_IDL
#define CATIALayout2DLayout_IDL

/*IDLREP*/

/**
* @CAA2Level L1
* @CAA2Usage U3
*/

/**
* Interface to manage the 2D Layout root.
*/

interface CATIALayout2DSheet;
interface CATIALayout2DSheets;
interface CATIAParameters;
interface CATIARelations;

#include "CatDrawingStandard.idl"
#include "CatVisuIn3DMode.idl"
#include "CatRenderingMode.idl"
#include "CATSafeArray.idl"
#include "CATIABase.idl"

interface  CATIALayout2DLayout : CATIABase
{
 /**
  * Returns the collection of Layout2D sheets of the part document.
  * <!-- @sample -->
  * <dl>
  * <dt><b>Example:</b>
  * <dd>
  * This example retrieves in <tt>SheetCollection</tt> the collection of
  * sheets currently managed by the layout root of a part of the active document, 
  * supposed to be a part document.
  * <pre>
  * Dim MyRoot As Layout2DRoot
  * Set MyRoot = CATIA.ActiveDocument.Part.GetItem("CATLayout2DRoot)"
  * Dim SheetCollection As Layout2DSheets
  * Set SheetCollection = MyRoot.<font color="red">Sheets</font>.
  * </pre>
  * </dl>
  */
#pragma PROPERTY Sheets
  HRESULT get_Sheets(out /*IDLRETVAL*/ CATIALayout2DSheets oSheets);

 /**
  * Returns the collection of relations of the part document.
  * @sample
  * This example retrieves in <tt>layoutRelations</tt> the collection of
  * relations currently managed by the layout root of a part of the active document, 
  * supposed to be a part document.
  * <pre>
  * Dim MyRoot As Layout2DRoot
  * Set MyRoot = CATIA.ActiveDocument.Part.GetItem("CATLayout2DRoot)"
  * Dim layoutRelations As Relations
  * Set layoutRelations = MyRoot.<font color="red">Relations</font>
  * </pre>
  */
#pragma PROPERTY Relations
  HRESULT get_Relations(out /*IDLRETVAL*/ CATIARelations oRelations);

  /**
  * Returns the collection of parameters of the layout.
  * @sample
  * This example retrieves in <tt>layoutParameters</tt> the collection of
  * parameters currently managed by the layout root of a part of the active document, 
  * supposed to be a part document.
  * <pre>
  * Dim MyRoot As Layout2DRoot
  * Set MyRoot = CATIA.ActiveDocument.Part.GetItem("CATLayout2DRoot)"
  * Dim layoutParameters As Parameters
  * Set layoutParameters = MyRoot.<font color="red">Parameters</font>
  * </pre>
  */
#pragma PROPERTY Parameters
  HRESULT get_Parameters(out /*IDLRETVAL*/ CATIAParameters oParameters);

 /**
  * Retrieves or sets the active sheet of the layout.
  * <!-- @sample -->
  * <dl>
  * <dt><b>Example:</b>
  * <dd>
  * This example retrieves the active sheet 
  * currently managed by the layout root of a part of the active document, 
  * supposed to be a part document.
  * <pre>
  * Dim MyRoot As Layout2DRoot
  * Set MyRoot = CATIA.ActiveDocument.Part.GetItem("CATLayout2DRoot)"
  * Dim MySheet = MyRoot.<font color="red">GetActiveSheet</font>
  * </pre>
  * </dl>
  */
#pragma PROPERTY ActiveSheet
  HRESULT get_ActiveSheet(out /*IDLRETVAL*/ CATIALayout2DSheet oSheet);
  HRESULT put_ActiveSheet(in CATIALayout2DSheet iSheet);


 /**
  * Returns or sets the DrawingStandard of the part document.
  * <!-- @sample -->
  * <dl>
  * <dt><b>Example:</b>
  * <dd>
  * This example sets the drawing standard 
  * currently managed by the layout root of a part of the active document, 
  * supposed to be a part document, to ISO.
  * <pre>
  * Dim MyRoot As Layout2DRoot
  * Set MyRoot = CATIA.ActiveDocument.Part.GetItem("CATLayout2DRoot)"
  * MyRoot.<font color="red">Standard</font> = catISO
  * </pre>
  * </dl>
  */
#pragma PROPERTY Standard
  HRESULT get_Standard(out /*IDLRETVAL*/ CatDrawingStandard oStandard);
  HRESULT put_Standard(in CatDrawingStandard iStandard);

  /**
  * Set/Get the 3D visualization mode of the layout in the 3D Viewer
  * ie in the 3D windows and in the background of each view in every 2D context.
  * @see CatVisuIn3DMode
  */
#pragma PROPERTY VisuIn3D
  HRESULT get_VisuIn3D(out /*IDLRETVAL*/ CatVisuIn3DMode oMode);
  HRESULT put_VisuIn3D(in CatVisuIn3DMode iMode);


  /**
  * Set/Get the rendering mode of Layout2D. get_RenderingMode method can fail if rendering value stored on Layout is not a value defined in CatRenderingMode enum.
  * <! @sample >
  * <dl>
  * <dt><b>Example:</b>
  * <dd>
  * This example sets the rendering mode to <tt>catRenderShadingWithEdges</tt> for the layout root of a part of the active document.
  * <pre>
  * Dim MyRoot As Layout2DRoot
  * Set MyRoot = CATIA.ActiveDocument.Part.GetItem("CATLayout2DRoot)"
  * MyRoot.<font color="red"> RenderingMode </font> = catRenderShadingWithEdges
  * </pre> 
  * </dl>
  */
#pragma PROPERTY RenderingMode
  HRESULT get_RenderingMode(out /*IDLRETVAL*/ CatRenderingMode oRenderingMode);
  HRESULT put_RenderingMode(in CatRenderingMode iRenderingMode);


  /**
   * Changes the positions of the sheets in this drawing according to the given 
   * ordered list.
   * iOrderedSheets is the result of a permutation applied to 
   * the list of <b>all</b> the sheets of this drawing, with the following 
   * constraint: For every non-detail sheet, there is not any detail sheet 
   * appearing before in iOrderedSheets.
   *
   * <!-- @sample -->
   * <dl>
   * <dt><b>Example:</b>
   * <dd>
   * This example inverts the sheet order of a drawing made of exactly two 
   * regular sheets.
   * <pre>
   * Set drwsheetsorder =  CATIA.ActiveDocument.Part.GetItem("CATLayoutRoot")
   * Set drwsheets = drwsheetsorder.Sheets
   * Set sheet1 = drwsheets.item(1)
   * Set sheet2 = drwsheets.item(2)
   * newsheetorder = Array(sheet2, sheet1)
   * drwsheetsorder.<font color="red">reorder_Sheets</font>(newsheetorder)
   * </pre>
   * </dl>
   */
  HRESULT reorder_Sheets(in CATSafeArrayVariant iOrderedSheets);

};

// Interface name : CATIALayout2DLayout
#pragma ID CATIALayout2DLayout "DCE:8b2d7dd5-60b8-49b7-892ef9bf22185865"
#pragma DUAL CATIALayout2DLayout

// VB object name : Layout2DRoot (Id used in Visual Basic)
#pragma ID Layout2DRoot "DCE:c6217d2e-6f85-408d-9343a902d775061b"
#pragma ALIAS CATIALayout2DLayout Layout2DRoot


#endif

