/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CatViewType_h
#define CatViewType_h

enum CatViewType {
        catAuxiliaryView,
        catSectionView,
        catSectionCutView
};

#endif
