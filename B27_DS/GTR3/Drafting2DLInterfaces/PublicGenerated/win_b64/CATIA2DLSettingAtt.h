/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIA2DLSettingAtt_h
#define CATIA2DLSettingAtt_h

#include "CATCORBABoolean.h"
#include "CAT_VARIANT_BOOL.h"

#ifndef ExportedByLayout2DPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __Layout2DPubIDL
#define ExportedByLayout2DPubIDL __declspec(dllexport)
#else
#define ExportedByLayout2DPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByLayout2DPubIDL
#endif
#endif

#include "CATBSTR.h"
#include "CATIASettingController.h"
#include "Cat2DLSettingAtt.h"

extern ExportedByLayout2DPubIDL IID IID_CATIA2DLSettingAtt;

class ExportedByLayout2DPubIDL CATIA2DLSettingAtt : public CATIASettingController
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_HideIn3D(CAT_VARIANT_BOOL & oValue)=0;

    virtual HRESULT __stdcall put_HideIn3D(CAT_VARIANT_BOOL iValue)=0;

    virtual HRESULT __stdcall GetHideIn3DInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetHideIn3DLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_ViewBackgroundMode(CatViewBackgroundMode & oValue)=0;

    virtual HRESULT __stdcall put_ViewBackgroundMode(CatViewBackgroundMode iValue)=0;

    virtual HRESULT __stdcall GetViewBackgroundModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetViewBackgroundModeLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_ViewFilterCreationMode(CatViewFilterCreationMode & oValue)=0;

    virtual HRESULT __stdcall put_ViewFilterCreationMode(CatViewFilterCreationMode iValue)=0;

    virtual HRESULT __stdcall GetViewFilterCreationModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetViewFilterCreationModeLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_DedicatedFilterType(CatDedicatedFilterType & oValue)=0;

    virtual HRESULT __stdcall put_DedicatedFilterType(CatDedicatedFilterType iValue)=0;

    virtual HRESULT __stdcall GetDedicatedFilterTypeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetDedicatedFilterTypeLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_EditDedicatedFilterDialogBox(CAT_VARIANT_BOOL & oValue)=0;

    virtual HRESULT __stdcall put_EditDedicatedFilterDialogBox(CAT_VARIANT_BOOL iValue)=0;

    virtual HRESULT __stdcall GetEditDedicatedFilterDialogBoxInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetEditDedicatedFilterDialogBoxLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_ClippingFrame(CAT_VARIANT_BOOL & oValue)=0;

    virtual HRESULT __stdcall put_ClippingFrame(CAT_VARIANT_BOOL iValue)=0;

    virtual HRESULT __stdcall GetClippingFrameInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetClippingFrameLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_ClippingFrameReframeOnMode(CatClippingFrameReframeOnMode & oValue)=0;

    virtual HRESULT __stdcall put_ClippingFrameReframeOnMode(CatClippingFrameReframeOnMode iValue)=0;

    virtual HRESULT __stdcall GetClippingFrameReframeOnModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetClippingFrameReframeOnModeLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_BackClippingPlane(CAT_VARIANT_BOOL & oValue)=0;

    virtual HRESULT __stdcall put_BackClippingPlane(CAT_VARIANT_BOOL iValue)=0;

    virtual HRESULT __stdcall GetBackClippingPlaneInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetBackClippingPlaneLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_PropagateHighlight(CAT_VARIANT_BOOL & oValue)=0;

    virtual HRESULT __stdcall put_PropagateHighlight(CAT_VARIANT_BOOL iValue)=0;

    virtual HRESULT __stdcall GetPropagateHighlightInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetPropagateHighlightLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_FitAllInSheetFormat(CAT_VARIANT_BOOL & oValue)=0;

    virtual HRESULT __stdcall put_FitAllInSheetFormat(CAT_VARIANT_BOOL iValue)=0;

    virtual HRESULT __stdcall GetFitAllInSheetFormatInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetFitAllInSheetFormatLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_TileLayoutWindow(CAT_VARIANT_BOOL & oValue)=0;

    virtual HRESULT __stdcall put_TileLayoutWindow(CAT_VARIANT_BOOL iValue)=0;

    virtual HRESULT __stdcall GetTileLayoutWindowInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetTileLayoutWindowLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_DisplayBackAndCuttingPlane(CAT_VARIANT_BOOL & oValue)=0;

    virtual HRESULT __stdcall put_DisplayBackAndCuttingPlane(CAT_VARIANT_BOOL iValue)=0;

    virtual HRESULT __stdcall GetDisplayBackAndCuttingPlaneInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetDisplayBackAndCuttingPlaneLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_Activate2DMode(CAT_VARIANT_BOOL & oValue)=0;

    virtual HRESULT __stdcall put_Activate2DMode(CAT_VARIANT_BOOL iValue)=0;

    virtual HRESULT __stdcall GetActivate2DModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetActivate2DModeLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_DisplayClippingOutline(CAT_VARIANT_BOOL & oValue)=0;

    virtual HRESULT __stdcall put_DisplayClippingOutline(CAT_VARIANT_BOOL iValue)=0;

    virtual HRESULT __stdcall GetDisplayClippingOutlineInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetDisplayClippingOutlineLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall GetClippingViewOutlineColor(CATLONG & oValueR, CATLONG & oValueG, CATLONG & oValueB)=0;

    virtual HRESULT __stdcall SetClippingViewOutlineColor(CATLONG iValueR, CATLONG iValueG, CATLONG iValueB)=0;

    virtual HRESULT __stdcall GetClippingViewOutlineColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetClippingViewOutlineColorLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_ClippingViewOutlineLinetype(CATLONG & oValue)=0;

    virtual HRESULT __stdcall put_ClippingViewOutlineLinetype(CATLONG iValue)=0;

    virtual HRESULT __stdcall GetClippingViewOutlineLinetypeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetClippingViewOutlineLinetypeLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_ClippingViewOutlineThickness(CATLONG & oValue)=0;

    virtual HRESULT __stdcall put_ClippingViewOutlineThickness(CATLONG iValue)=0;

    virtual HRESULT __stdcall GetClippingViewOutlineThicknessInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetClippingViewOutlineThicknessLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_Boundaries2DLDisplay(CAT_VARIANT_BOOL & oValue)=0;

    virtual HRESULT __stdcall put_Boundaries2DLDisplay(CAT_VARIANT_BOOL iValue)=0;

    virtual HRESULT __stdcall GetBoundaries2DLDisplayInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetBoundaries2DLDisplayLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall GetBoundaries2DLColor(CATLONG & oValueR, CATLONG & oValueG, CATLONG & oValueB)=0;

    virtual HRESULT __stdcall SetBoundaries2DLColor(CATLONG iValueR, CATLONG iValueG, CATLONG iValueB)=0;

    virtual HRESULT __stdcall GetBoundaries2DLColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetBoundaries2DLColorLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_Boundaries2DLLineType(CATLONG & oValue)=0;

    virtual HRESULT __stdcall put_Boundaries2DLLineType(CATLONG iValue)=0;

    virtual HRESULT __stdcall GetBoundaries2DLLineTypeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetBoundaries2DLLineTypeLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_Boundaries2DLThickness(CATLONG & oValue)=0;

    virtual HRESULT __stdcall put_Boundaries2DLThickness(CATLONG iValue)=0;

    virtual HRESULT __stdcall GetBoundaries2DLThicknessInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetBoundaries2DLThicknessLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_CalloutCreationDialogBox(CAT_VARIANT_BOOL & oValue)=0;

    virtual HRESULT __stdcall put_CalloutCreationDialogBox(CAT_VARIANT_BOOL iValue)=0;

    virtual HRESULT __stdcall GetCalloutCreationDialogBoxInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetCalloutCreationDialogBoxLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_CalloutCreationInActiveView(CAT_VARIANT_BOOL & oValue)=0;

    virtual HRESULT __stdcall put_CalloutCreationInActiveView(CAT_VARIANT_BOOL iValue)=0;

    virtual HRESULT __stdcall GetCalloutCreationInActiveViewInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetCalloutCreationInActiveViewLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_CreateAssociativeUseEdges(CAT_VARIANT_BOOL & oValue)=0;

    virtual HRESULT __stdcall put_CreateAssociativeUseEdges(CAT_VARIANT_BOOL iValue)=0;

    virtual HRESULT __stdcall GetCreateAssociativeUseEdgesInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetCreateAssociativeUseEdgesLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall GetProtectedElementsColor(CATLONG & oValueR, CATLONG & oValueG, CATLONG & oValueB)=0;

    virtual HRESULT __stdcall SetProtectedElementsColor(CATLONG iValueR, CATLONG iValueG, CATLONG iValueB)=0;

    virtual HRESULT __stdcall GetProtectedElementsColorInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetProtectedElementsColorLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_LayoutDefaultRenderStyle(CATLONG & oValue)=0;

    virtual HRESULT __stdcall put_LayoutDefaultRenderStyle(CATLONG iValue)=0;

    virtual HRESULT __stdcall GetLayoutDefaultRenderStyleInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetLayoutDefaultRenderStyleLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_InsureSheetNamesUniqueness(CAT_VARIANT_BOOL & oValue)=0;

    virtual HRESULT __stdcall put_InsureSheetNamesUniqueness(CAT_VARIANT_BOOL iValue)=0;

    virtual HRESULT __stdcall GetInsureSheetNamesUniquenessInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetInsureSheetNamesUniquenessLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_InsureViewNamesUniqueness(CAT_VARIANT_BOOL & oValue)=0;

    virtual HRESULT __stdcall put_InsureViewNamesUniqueness(CAT_VARIANT_BOOL iValue)=0;

    virtual HRESULT __stdcall GetInsureViewNamesUniquenessInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetInsureViewNamesUniquenessLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_InsureViewNamesUniquenessScope(CatInsureViewNamesUniquenessScope & oValue)=0;

    virtual HRESULT __stdcall put_InsureViewNamesUniquenessScope(CatInsureViewNamesUniquenessScope iValue)=0;

    virtual HRESULT __stdcall GetInsureViewNamesUniquenessScopeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetInsureViewNamesUniquenessScopeLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_InsureFilterNamesUniqueness(CAT_VARIANT_BOOL & oValue)=0;

    virtual HRESULT __stdcall put_InsureFilterNamesUniqueness(CAT_VARIANT_BOOL iValue)=0;

    virtual HRESULT __stdcall GetInsureFilterNamesUniquenessInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetInsureFilterNamesUniquenessLock(CAT_VARIANT_BOOL iLocked)=0;


};

CATDeclareHandler(CATIA2DLSettingAtt, CATIASettingController);

#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIABase.h"
#include "CATSafeArray.h"
#include "CATVariant.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
