/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIALayout2DViews_h
#define CATIALayout2DViews_h

#ifndef ExportedByLayout2DPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __Layout2DPubIDL
#define ExportedByLayout2DPubIDL __declspec(dllexport)
#else
#define ExportedByLayout2DPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByLayout2DPubIDL
#endif
#endif

#include "CATBSTR.h"
#include "CATIACollection.h"
#include "CATSafeArray.h"
#include "CATVariant.h"
#include "CatViewSide.h"
#include "CatViewType.h"

class CATIALayout2DView;

extern ExportedByLayout2DPubIDL IID IID_CATIALayout2DViews;

class ExportedByLayout2DPubIDL CATIALayout2DViews : public CATIACollection
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall AddPrimary(double iX, double iY, CATIALayout2DView *& oNewLayout2DView)=0;

    virtual HRESULT __stdcall AddRelated(CATIALayout2DView * iReferenceView, CatViewSide iSide, double iX, double iY, CATIALayout2DView *& oNewLayout2DView)=0;

    virtual HRESULT __stdcall AddAuxiliary(CATIALayout2DView * iReferenceView, const CATSafeArrayVariant & iBCSegment, double iXorient, double iYorient, CatViewType iSectionType, double iX, double iY, CATIALayout2DView *& oNewLayout2DView)=0;

    virtual HRESULT __stdcall AddFrom3DPlane(const CATSafeArrayVariant & iPlane, CatViewType iViewType, double iX, double iY, CATIALayout2DView *& oNewLayout2DView)=0;

    virtual HRESULT __stdcall get_ActiveView(CATIALayout2DView *& oView)=0;

    virtual HRESULT __stdcall Remove(const CATVariant & iIndex)=0;

    virtual HRESULT __stdcall Item(const CATVariant & iIndex, CATIALayout2DView *& oItem)=0;

    virtual HRESULT __stdcall AddDetail(const CATBSTR & iDetailName, CATIALayout2DView *& oView)=0;


};

CATDeclareHandler(CATIALayout2DViews, CATIACollection);

#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
