#ifndef CATLISTV_CATICciMaterialCache_H
#define CATLISTV_CATICciMaterialCache_H


// COPYRIGHT DASSAULT SYSTEMES 2009
//==============================================================================
// @collection CATLISTV(CATICciMaterialCache_var)
//  Collection class for MaterialCache.
//     All the methods of handlers collection classes are available.
//    Refer to the articles dealing with collections in the encyclopedia.
//==============================================================================

/**
* @XXX2Level L0
* @CAA2Usage U1
*/

#include "CATLISTHand_Clean.h"
#include "CATLISTHand_AllFunct.h"
#include "CATLISTHand_Declare.h"

#include "CATICciMaterialCache.h"
#include "CAACompositesItf.h"

#ifdef  CATCOLLEC_ExportedBy
#undef  CATCOLLEC_ExportedBy
#endif
#define	CATCOLLEC_ExportedBy ExportedByCAACompositesItf

CATLISTHand_DECLARE(CATICciMaterialCache_var)

#undef  CATCOLLEC_ExportedBy

#endif
