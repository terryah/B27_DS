#ifndef CATLISTV_CATICciLimitContour_H
#define CATLISTV_CATICciLimitContour_H

// COPYRIGHT DASSAULT SYSTEMES 2015
//==============================================================================
// @collection CATLISTV(CATICciLimitContour_var)
//  Collection class for Limit contours.
//     All the methods of handlers collection classes are available.
//    Refer to the articles dealing with collections in the encyclopedia.
//==============================================================================


/**
* @CAA2Level L0
* @CAA2Usage U1
*/

#include "CATLISTHand_Clean.h"
#include "CATLISTHand_AllFunct.h"
#include "CATLISTHand_Declare.h"

#include "CATICciLimitContour.h"
#include "CAACompositesItf.h"

#ifdef  CATCOLLEC_ExportedBy
#undef  CATCOLLEC_ExportedBy
#endif
#define	CATCOLLEC_ExportedBy ExportedByCAACompositesItf

CATLISTHand_DECLARE(CATICciLimitContour_var)

#undef  CATCOLLEC_ExportedBy

#endif
