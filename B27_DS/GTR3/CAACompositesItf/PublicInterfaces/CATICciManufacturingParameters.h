
//===================================================================
// COPYRIGHT DASSAULT SYSTEMES 2012/07/17
//===================================================================
// CATICciManufacturingParameters.cpp
// Header definition of class CATICciManufacturingParameters
//===================================================================
//
// Usage notes:
//
//===================================================================
//  2012/07/17 Creation: Code generated by the 3DS wizard
//===================================================================
#ifndef CATICciManufacturingParameters_H
#define CATICciManufacturingParameters_H
/**
* @CAA2Level L0
* @CAA2Usage U5
*/

#include "CAACompositesItf.h"
#include "CATBaseUnknown.h"
#include "CATUnicodeString.h"

extern ExportedByCAACompositesItf  IID IID_CATICciManufacturingParameters ;

/**
* Interface representing composites manufacturing parameters.
* <b>Role</b>: Allows to manage partner's parameter for composites manufacturing
*/
class ExportedByCAACompositesItf CATICciManufacturingParameters: public CATBaseUnknown
{
    CATDeclareInterface;

public:

    /**
    * Gets partner's id dcorresponding to manufacturing parameters definition. 
    * @param oPartnerName
    *    Partner's id 
    */

    virtual HRESULT GetPartnerName (CATUnicodeString & oPartnerName) = 0;

};

//-----------------------------------------------------------------------
CATDeclareHandler( CATICciManufacturingParameters, CATBaseUnknown );

#endif
