/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBSimProcessToolBar.h
//      Toolbar used by the process simulation command.
//
//==============================================================================
//
// Usage notes: 
//      Toolbar used by the process simulation command in order to control the 
//      simulation.
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     mmh          01/03/2002   Initial implementation
//     mmh          01/11/2002   Add method to enable/disable the buttons
//     sha          06/18/2002   Implement the automatic real-time simulation
//                               feature
//     sha          07/16/2002   Add new data member: _initialStepSize
//     mmh          11/27/2002   Support command migration to new base class
//     mmh          02/14/2003   Add "close" button for Shop Viewer
//     mmg          03/06/2003   moved autorealtime handling from  
//                               DNBSimProcessCommand
//     rtl          04/07/2003   Code change for supporting simulating between
//                               user-specified begintime, endtime and untilTime
//     rtl          04/10/2003   Added method(functor) to enable-disable 
//                               _editortime
//     rtl          06/16/2003   Added a new member, _editorTime_ro, for displaying
//                               a read-only time editor while simulation is 
//                               running
//     smw          05/10/2004   Replaced RWTimer with DNBStopWatch
//     mmh          06/16/2004   Enhance player: add mask and new buttons
//     mmh          03/17/2005   Add data members for NLS support
//     smw          05/06/2005   Replaced RWMutexLocks
//     mmh          11/12/2005   Enhanced control for step,mode,time editor,cb
//     smw          04/120/2006  Changed the parent command from being a 
//                               DNBSimProcessCommand to a DNBTimeBasedSimCmd so that
//                               this tool bar can be used in the resource behaviour 
//                               simulation command as well ( since the resource behaviour
//                               simulation command is a DNBTimeBasedSimCmd
//                               The father command can be down casted to the different subtypes for
//                               specific behaviour//
//==============================================================================
#ifndef DNBSimProcessToolBar_H
#define DNBSimProcessToolBar_H

#include <DNBSystemBase.h>
//#include <rw/thr/mutex.h>
#include <DNBSysMutexLock.h>
#include <DNBSysLockGuard.h>
#include <DNBSystemDefs.h>

#include <DNBStopwatch.h>
#include <DNBIntervalTimer.h>
#include <DNBSimletController.h>
#include <DNBTimeletClock.h>
#include <DNBNotificationAgent.h>
#include <DNBStartFinishObserver.h>

#include <DNBCATBase.h>
#include <CATDlgToolBar.h>
#include <DNBSimActOpt.h>
#include <DNBSimulationCommands.h>
#include <DNBCATDefs.h>

class CATDlgPushButton;
class CATDlgSpinner;
class CATDlgEditor;
class CATUnicodeString;
class DNBTimeBasedSimCmd;
class DNBSimlet;

//------------------------------------------------------------------------------

/**
 * Toolbar used by the process simulation command.
 */
class ExportedByDNBSimulationCommands DNBSimProcessToolBar : public CATDlgToolBar
{
    DeclareResource( DNBSimProcessToolBar, CATDlgToolBar )

public:
    /**
     * Constructor for a DNBSimProcessToolBar object.
     */
    DNBSimProcessToolBar( DNBTimeBasedSimCmd *iCmd, int iMask = DNB_SP_ALL );
    /**
     * Destructor for a DNBSimProcessToolBar object.
     */
    ~DNBSimProcessToolBar();

    /**
     * Sets the current simulation time.
     */
    void SetTime( double );
    /**
     * Sets the current simulation step size.
     */
    void SetStepSize( float );
    /**
     * Sets the automatic real-time simulation mode ON/OFF.
     */
    void SetAutoRealtime( boolean );
    /**
     * Gets the automatic real-time simulation mode ON/OFF.
     */
    boolean GetAutoRealtime();
    /**
     * Callback used to automatically adjust the step size real-time simulation 
     * synchronization.
     */
    void AutoRealtimeCB( const DNBTimeletClock::CycleCallbackData &data );
    /**
     * Enables/disables the buttons from the toolbar.
     */
    void SetButtonSensitivity( CATULong iState, int iMask = DNB_SP_ALL );
    /**
     * Sets the simulation environment (controller, interval timer).
     */
    void SetSimEnv();

protected:

    CATDlgPushButton                        *_buttonJumpStart;
    CATDlgPushButton                        *_buttonPlayBack;
    CATDlgPushButton                        *_buttonStepBack;
    CATDlgPushButton                        *_buttonPause;
    CATDlgPushButton                        *_buttonStep;
    CATDlgPushButton                        *_buttonPlay;
    CATDlgPushButton                        *_buttonJumpEnd;
    CATDlgPushButton                        *_buttonClose;
    CATDlgPushButton                        *_buttonLoop;
    int                                     _loopMode;
    CATDlgSpinner                           *_spinnerStepSize;
    CATDlgEditor                            *_editorTime;
    DNBSysMutexLock                         _AutoRealtimeLock;
    boolean                                 _bAutoRealtime;
    boolean                                 _bInitialStep;
    boolean                                 _bSyncedUp;
    DNBSimTime                              _initialStepSize, _origStepSize, _crtStepSize;
    DNBSimTime                              _simStart, _simStop;
    DNBStopwatch                            _stepTimer;
    int                                     _nCount;
    DNBTimeBasedSimCmd                      *_cmd;
    DNBTimeletClock                         *_controller;
    DNBIntervalTimer                        *_IntervalTimer;
    DNBSimlet                               *_LoopProclet;
    double                                  _timeScale;
    DNBSimletController::PhaseCallback      _stepCB;
    DNBTimeletClock::CycleCallback          _autoRealtimeCB;
    DNBSimTime                              _untilTime;
    DNBSimTime                              _currentTime;

    /**
     * Callback activated when a button is pressed (start, play back, step back,
     * pause, step, play, end).
     */
    virtual void cb_SimButton( CATCommand*, CATNotification*, CATCommandClientData );
    /**
     * Callback activated when the Close button is pressed.
     */
    virtual void cb_CloseButton( CATCommand*, CATNotification*, CATCommandClientData );
    /**
     * Callback activated when the simulation mode is changed (single/loop).
     */
    virtual void cb_SimMode( CATCommand*, CATNotification*, CATCommandClientData );
    /**
     * Callback activated when the simulation step size is changed.
     */
    virtual void cb_StepSize( CATCommand*, CATNotification*, CATCommandClientData );
    /**
     * Callback used to change the step size for the simulation controller.
     */
    virtual void SetStepSizeCB( const DNBSimletController::UpdateEventData&,
        const DNBSimletController::UpdatePhase&,
        double, DNBIntervalTimer* );
    /**
     * Callback activated when until time is specified
     */
    virtual void cb_editorTime( CATCommand*, CATNotification*, CATCommandClientData );

    CATUnicodeString        _sTimeTooBig;
    CATUnicodeString        _sTimeBig;
    CATUnicodeString        _sSimStopEnd;
    CATUnicodeString        _sTimeSmall;
    CATUnicodeString        _sSimStopBegin;
};

//------------------------------------------------------------------------------

#endif  // DNBSimProcessToolBar_H
