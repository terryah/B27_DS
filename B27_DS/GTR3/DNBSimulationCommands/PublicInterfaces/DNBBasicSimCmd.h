/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2002
//==============================================================================
//
// DNBBasicSimCmd.h
//      Base class for a simulation command.
//
//==============================================================================
//
// Usage notes: 
//      This class is used as a base class by the DNBTimeBasedSimCmd class
//      and DNBStepBasedSimCmd class.
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     mmh          10/17/2002   Initial implementation from DNBSimCommand
//     sha          11/12/2002   move PostDisplayCD()/RemoveWindowCB() methods
//                               in the DNBVisualizeMessage itself
//     mmh          11/15/2002   Use the CATDeclareClass macro.
//     mmh          01/14/2003   Add parameter for the WaitForRunState: allow 
//                               WaitForRunState to wait for 1 particular state
//     mmh          03/27/2003   Read dynamic clash mode from settings
//     mmh          08/13/2003   Read option for State Management
//     mmg          01/26/2004   removed DistanceMessage, InterferenceMessage
//                               now processes distance checks
//     mmg          02/25/2004   removed obsolete code
//     smw          09/10/2004   added setting to enable / disable annotation 
//                               activities
//     mmh          04/13/2005   add settings for behavior options (R16DMR003)
//     rtl          04/28/2005   Add support for applying End Conditions
//     smw          05/06/2005   Migrated to use DNBMutexLocks instead of RogueWave's
//
//==============================================================================

#ifndef DNBBasicSimCmd_H
#define DNBBasicSimCmd_H

#include <DNBSystemBase.h>
//#include <rw/thr/mutex.h>
#include <DNBSysFastMutexLock.h>
#include <DNBSystemDefs.h>

#include <DNBObserverList.h>
#include <DNBParametricCamera3D.h>
#include <DNBBasicWorld.h>
#include <DNBGroupMessage.h>
#include <DNBSimletController.h>
#include <DNBCATTraversers.h>

#include <DNBCATBase.h>
#include <CATStateCommand.h>
#include <CATBaseUnknown.h>
#include <DNBISimEnv.h>
#include <DNBSimActOpt.h>
#include <DNBActBehaviorType.h>
#include <DNBHlnkBehaviorType.h>
#include <DNBCATDefs.h>

#include <DNBSimulationCommands.h>

class CATDlgDialog;
class CATFrmLayout;
class CATFrmWindow;
class DNBApplyObslistMessage;
class DNBObslistProcessedMessage;
class DNBVisualizeMessage;
class DNBImgCaptureMessage;
class DNBInterferenceMessage;
class DNBAnalysis;
class DNBProcessController;
class DNBIAnalysisConfiguration;

//------------------------------------------------------------------------------

/**
 * Base class for a simulation command.
 */
class ExportedByDNBSimulationCommands DNBBasicSimCmd : public CATStateCommand
{
    CATDeclareClass;

public:

    struct DNBSettingsVal
    {
        int iText;
        int iViewpoint;
        int iHyperlnk;
        int iAnnotation;
        int iDisableSync;
        int iPause;
        float fStepSize;
        int iDynClash;
        int iStateMgt;
        int iEndCondition;
        DNBActBehaviorType eBText;
        DNBHlnkBehaviorType eBHlnk;
        DNBActBehaviorType eBAnnot;
        DNBActBehaviorType eBVis;

        DNBSettingsVal() :
        iText(0), iViewpoint(0), iHyperlnk(0), iAnnotation(0),
        iDisableSync(0), iPause(0), fStepSize(0.0),
        iDynClash(0), iStateMgt(0), iEndCondition(0), eBText(DNBBehaviorProcess),
        eBHlnk(DNBBehaviorContinue), eBAnnot(DNBBehaviorProcess), 
        eBVis(DNBBehaviorProcess)
        {}
    };

    /**
     * Public method to wrap private method for adding a close window callback
     * to a dialog parented to a command from outside the command path.
     */
    void AddDialogCB( CATDlgDialog *, CATCommandMethod );
    /**
     * Callback to be executed when dialog is closed.
     */
    void CloseDlgCB( CATCommand *, CATNotification*, CATCommandClientData );
    /**
     * Push changes to main thread when changes were initiated by main thread.
     */
    virtual void FlushObserverlist();
    /**
     * Push changes to main thread when changes were initiated by main thread.
     * In addition, this method performs collision analysis if required by
     * the given argument.
     */
    virtual boolean FlushObserverlist( boolean check_collision );
    /**
     * Callback set on CATViewpoint published events: VIEWPOINT_MODIFIED(),
     * VIEWPOINT_ANIMATION_STARTED() and VIEWPOINT_ANIMATION_STOPED().
     */
    void ViewpointChangedCB( CATCallbackEvent, void *, 
        CATNotification *, CATSubscriberData, CATCallback );
    /**
     * Callback used to set the run state.
     */
    void RunStateCB( const DNBSimletController::CycleCallbackData& );
    /**
     * Callback used to set the update state.
     */
    void UpdateStateCB( const DNBSimletController::UpdateEventData&,
        const DNBSimletController::UpdatePhase& );
    /**
     * Returns the handle to the world.
     */
    DNBBasicWorld::Handle GetWorld();
    /**
     * TO BE IMPLEMENTED IN ALL DERIVED CLASSES.
     */
    virtual DNBSimletController *GetSimController() = 0;
    /**
     * Virtual function - returns NULL. To be implemented in the derived 
     * classes, as needed.
     */
    virtual DNBProcessController *GetProcessController();
    /**
     * Initialize the run and update state before running the controller.
     */
    void PreRunController();
    /**
     * Halts the controller and waits for RunHalted state.
     */
    void HaltController();
    /**
     * Stops the controller and waits for RunJoinable state.
     */
    void StopController();
    /**
     * Starts the controller and waits for RunIdle state.
     */
    void StartController();
    /**
     * Resets the controller and waits for RunIdle state.
     */
    void ResetController();
    /**
     * Waits for a given state (active wait - the PCQueue will be evaluated
     * at least once).
     * @param state
     *  The state to be reached.
     */
    void WaitForRunState( DNBSimletController::RunState state, 
                          bool exact = false );
    /**
     * Returns TRUE. To be implemented in the derived classes, as needed.
     */
    virtual boolean hasActivityType( const CATString& type = "Activity" );
    /**
     * Checks the analysis mode and performs the necessary changes for the
     * analysis objects.
     */
    virtual void UpdateAnalysisList( bool refresh );
    /**
     * Returns the simulation mode.
     */
    int GetSimMode() const;
    /**
     * Retrieves the value of the settings.
     */
    void GetSettings( DNBSettingsVal* sVal ) const;
    /**
     * Modify the value of the settings.
     * @param iSVal
     * The value to be set.
     */
    virtual void SetSettings( DNBSettingsVal* iSVal );
    /**
     * Activates the command. 
     * <br><b>Role</b>: Called by the command selector to give the focus
     * to the command.
     * @return The command status.
     */
    virtual CATStatusChangeRC Activate(CATCommand*,CATNotification*);
    /**
     * Deactivates the command.
     * <br><b>Role</b>: Called by the command selector to temporarily
     * withdraw the focus from the command.
     * @return The command status.
     */
    virtual CATStatusChangeRC Desactivate(CATCommand*,CATNotification*);
    /**
     * Cancels the command.
     * <br><b>Role</b>: Called by the command selector to definitely
     * withdraw the focus from the command.
     * The command should then request its destruction.
     * @param iCmd
     *   The command that requests to cancel the current one
     * @param iNotif
     *   The notification sent
     * @return The command status.
     */
    virtual CATStatusChangeRC Cancel(CATCommand *iCmd,CATNotification *iNotif);
    /**
     * Returns the value of the reference from the WDM manager.
     */
    int GetRefWDMMgt();
    /**
     * Returns TRUE if the Simulation Environment was created.
     */
    boolean IsSimEnvCreated();
    
protected:
    /**
     * Constructs a DNBBasicSimCmd object.
     * @param cname
     *  The command identifier. It must be unique and is used to retrieve the 
     *  undo/redo titles in the command message file.
     * @param iBehavior
     *  The behavior of the command.
     * @param iMode
     *  The focus mode.
     *  <br><b>Legal values</b>: With the default mode 
     *  <tt>CATCommandModeExclusive</tt>, the command takes the focus and 
     *  cancels the other commands. This mode is mandatory if the command 
     *  modifies the model.
     *  <br>The other possible mode is <tt>CATCommandModeShared</tt>: the 
     *  command only deactivates the active command and takes the focus. The 
     *  previous command will resume when the current command ends.
     *  <br>The <tt>CATCommandModeUndefined</tt> mode is forbidden with a main 
     *  state command since it needs to take the focus.
     * @param simMode
     *  The simulation mode.
     *  <br><b>Legal values</b>: <tt>DNB_BUILD_TIME</tt> (default value), 
     *  <tt>DNB_RUN_TIME</tt>, <tt>DNB_UNDEFINED</tt>. For the 
     *  <tt>DNB_BUILD_TIME</tt> and <tt>DNB_RUN_TIME</tt> modes, the settings 
     *  from Tools/Options <tt>Simulation</tt> will be considered.
     */
    DNBBasicSimCmd( const CATString &cname,
        CATDlgEngBehavior iBehavior = NULL,
        CATCommandMode iMode = CATCommandModeExclusive,
        int simMode = DNB_BUILD_TIME );

    virtual ~DNBBasicSimCmd();
    /**
     * TO BE IMPLEMENTED IN ALL DERIVED CLASSES.
     */
    virtual void BuildGraph() = 0;
    /**
     * Builds the <tt>Product</tt> branch from the PPR tree into WDM entities.
     */
    virtual void BuildWDMProduct();
    /**
     * Builds the <tt>Applicative Data</tt> from the PPR tree into WDM entities.
     */
    virtual void BuildApplicativeData();
    /**
     * Builds the <tt>Applicative Data</tt> from the PPR tree into WDM entities
     * (pre-order).
     */
    virtual void PreBuildApplicativeData();
    /**
     * Builds the <tt>Applicative Data</tt> from the PPR tree into WDM entities
     * (post-order).
     */
    virtual void PostBuildApplicativeData();
    /**
     * Builds <tt>Applicative Data</tt> from the PPR tree into WDM entities.
     */
    virtual void BuildApplicativeData( CATIdent, CATIdent, CATIdent );
    /**
     * Builds the camera into WDM.
     */
    virtual void BuildWDMCamera();
    /**
     * Push the list of observers to the main thread.
     */
    int PushObserverlist( bool force = true );
    /**
     * To be implemented in the derived classes, as needed.
     */
    virtual void ModifyPendingMsg();
    /**
     * Reads the settings from the repository, depending on the simulation mode.
     */
    virtual void ReadSettings();
    /**
     * Builds the World and the root product.
     */
    void BuildWorld();
    
private:
    /**
     * Callback to be executed when a 3d window is closed.
     */
    void RemoveCameraObserverCB(CATCallbackEvent, void *, 
        CATNotification *, CATSubscriberData, CATCallback);

protected:
    CATFrmLayout                    *_Layout;
    CATFrmWindow                    *_CamWindow;
    CATFrmEditor                    *_CrtEditor;
    CATDocument                     *_CrtDocument;
    
    DNBBasicWorld::Handle           _hWorld;
    DNBObserverList::Handle         _hObslist;
    DNBParametricCamera3D::Handle   _hCamera;
    DNBGroupMessage                 _PendingMsg;
    
    DNBAnalysis                     *_Analysis;
    DNBApplyObslistMessage          *_ApplyMsg;
    DNBObslistProcessedMessage      *_ProcessedMsg;
    DNBVisualizeMessage             *_VisMsg;
    DNBImgCaptureMessage            *_ImgCapMsg;
    DNBInterferenceMessage          *_InfrMsg;
    DNBProductTraverser             *_ProdTrav;
    DNBCATListTraverser             *_ApplicTrav;
    DNBGroupMessage                  _AppMsgs;

    DNBIAnalysisConfiguration       *_pAnalysisConfig;
    DNBSettingsVal                  _settingsVal;
    int                             _simMode;
    unsigned int                    _analysisVersion;
    int                             _refWDMMgt;
    boolean                         _CreatedSimEnv;
    DNBISimEnv_var                  _simEnv;

private:
    DNBGroupMessage                 _UpdateMsg;
    CATCallback                     _CameraCB;
    
//  static RWMutexLock                     _RunStateLock;
    static DNBSysFastMutexLock             _RunStateLock;
    static DNBSimletController::RunState   _RunState;
    static DNBSimletController::RunState   _UpdateState;
    boolean                         _apply_viewpoint_change;
    CATCallback                     _VPCB1;
    CATCallback                     _VPCB2;
    CATCallback                     _VPCB3;
    int                             _dynBuff;
};

//------------------------------------------------------------------------------

#endif
