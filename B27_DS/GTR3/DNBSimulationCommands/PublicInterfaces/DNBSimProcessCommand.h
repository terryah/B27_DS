/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBSimProcessCommand.h
//      Command used to simulate an activity (can be the whole process).
//
//==============================================================================
//
// Usage notes: 
//      Use this command when you want to simulate the process or individual 
//      activities. It can be found on the Simulation toolbar.
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     mmg          01/01/1999   Initial implementation
//     mmh          11/16/2001   Check if there is a process to simulate
//     mmh          12/31/2001   Create a dynamic toolbar for simulation options
//     sha          05/20/2002   Add new method: anyProcessToSimulate()
//     mmh          05/29/2002   Add get/set StopActivity methods
//     sha          06/18/2002   Add new PostFunc() method for real-time sim
//     sha          11/07/2002   Provide performance instrumentation
//     mmh          11/26/2002   Change base class to DNBTimeBasedSimCmd
//     mmh          12/05/2002   Make the InitSimEnv virtual
//     sha          12/12/2002   Enhanced performance traces (use macros)
//     mmh          02/17/2003   Add method to close command (fix IR A0382552)
//     mmg          03/06/2003   moved autorealtime handling to 
//                               DNBSimProcessToolBar
//     mmh          07/24/2003   Changes for Marino:Multiple Resource simulation
//     sha          12/15/2003   Add a default argument: disable simulation
//     sha          01/12/2004   Make anyProcessToSimulate() method virtual
//     mmh          03/15/2004   Code clean-up (remove old simulation dialog)
//
//==============================================================================
#ifndef DNBSimProcessCommand_H
#define DNBSimProcessCommand_H

#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>

#include <DNBTimeBasedSimCmd.h>

#include <DNBCATBase.h>
#include <CATISiAnalysisList.h>
#include <DNBTimeUtilities.h>
#include <DNBCATDefs.h>

#include <DNBSimulationCommands.h>


class CATDlgNotify;
class DNBSimDynamicToolBar;
class DNBSimProcessToolBar;
class DNBReportInfoDetails;

//------------------------------------------------------------------------------

/**
 * Command used to simulate an activity (can be the whole process).
 */
class ExportedByDNBSimulationCommands DNBSimProcessCommand 
                                    : public DNBTimeBasedSimCmd
{
    DNB_DECLARE_CLASS_PERF_TRACE

public:
    DeclareResource ( DNBSimProcessCommand, DNBTimeBasedSimCmd )
        
    /**
     * Constructs a DNBSimProcessCommand object.
     */
    DNBSimProcessCommand( boolean *arg = NULL );
    virtual ~DNBSimProcessCommand();
    
    /**
     * Builds the simulation command state chart.
     */
    void BuildGraph();
    /**
     * Activates the command. 
     * <br><b>Role</b>: Called by the command selector to give the focus
     * to the command.
     * @param iCmd
     *   The command that requests to activate the current one
     * @param iNotif
     *   The notification sent
     * @return The command status.
     */
    CATStatusChangeRC Activate(CATCommand *iCmd,CATNotification *iNotif);
    /**
     * Deactivates the command.
     * <br><b>Role</b>: Called by the command selector to temporarily
     * withdraw the focus from the command.
     * @param iCmd
     *   The command that requests to deactivate the current one
     * @param iNotif
     *   The notification sent
     * @return The command status.
     */
    CATStatusChangeRC Desactivate(CATCommand *iCmd,CATNotification *iNotif);
    /**
     * Cancels the command.
     * <br><b>Role</b>: Called by the command selector to definitely
     * withdraw the focus from the command.
     * The command should then request its destruction.
     * @param iCmd
     *   The command that requests to cancel the current one
     * @param iNotif
     *   The notification sent
     * @return The command status.
     */
    CATStatusChangeRC Cancel(CATCommand *iCmd,CATNotification *iNotif);
    /**
     * Returns the selected activity to be simulated.
     */
    CATISPPChildManagement_var getCurrentActivity();
    /**
     * Returns the stop activity for the simulation.
     */
    CATISPPChildManagement_var getStopActivity();
    /**
     * Sets the stop activity for the simulation.
     */
    void setStopActivity( const CATISPPChildManagement_var &iAct );
    
    /**
     * Forces the command to close.
     */
    void ForceClose();
  
    virtual void manageInstantReplay( bool state, bool shutdown = false );
    
protected:
    /**
     * Method called in the InitSimEnv to initialize the simulation environment.
     */
    virtual void BuildCurrentProcess();
    /**
     * Method called in the InitSimEnv to initialize the simulation environment
     * (adds the simulation callbacks).
     */
    virtual void AddSimulationCallbacks();
    /**
     * Initializes the simulation environment based on the activity from CSO,
     * if any, otherwise use the root process.
     * @param data
     *  Not used.
     * @return FALSE if there is no editor, TRUE otherwise.
     */
    virtual boolean InitSimEnv( void *data = NULL );
    /**
     * Initializes the dialog agents.
     * @param data
     *  Not used.
     * @return TRUE.
     */
    boolean Finish(void *data);
    /**
     * Search the CSO and initialize the activity to be simulated if there is
     * a valid activity in CSO.
     * @param data
     *  Not used.
     * @return FALSE if there is no current editor, TRUE otherwise.
     */
    boolean WhatIsInCSO(void *data);
    /**
     * Method called if there is no process/product to simulate.
     */
    boolean NoProcessOK(void *data = NULL);
    
    /**
     * Returns TRUE if there is any process to simulate: this includes any 
     * resource tasks or the root process.
     */
    virtual boolean anyProcessToSimulate();
    

    CATDialogAgent                  *_PushCLOSEWin;
    
    CATISiAnalysisList              _AnalysisList;
    
    CATISPPChildManagement_var      _Activity;
    CATISPPChildManagement_var      _StopActivity;

    CATDialogAgent                  *_PSWarning;
    CATDlgNotify                    *_PSMsg;

    DNBSimDynamicToolBar            *_SimDynToolbar;
    DNBSimProcessToolBar            *_SimProcToolbar;
    
};

//------------------------------------------------------------------------------

#endif
