/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/*
 * @quickReview MMH 06:09:15
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2002
//==============================================================================
//
// DNBTimeBasedSimCmd.h
//      Base class for the time based simulation commmands.
//
//==============================================================================
//
// Usage notes: 
//      Use this class as a base class for all the time based simulation 
//      commands.
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     mmh          10/18/2002   Initial implementation from the previous base
//                               class: DNBSimProcessBaseCommand
//     mmh          11/15/2002   Use the CATDeclareClass macro.
//     mmh          11/19/2002   Expose _rproclet as protected
//     sha          12/12/2002   Added performance traces
//     mmg          02/11/2003   added _flushModelMask to set which observers
//                               should be flushed to the CNEXT model
//     mmg          02/17/2003   use adaptive flush to model instead of 
//                               _flushModelMask
//     rtl          03/27/2003   Changes to support/control user-defined 
//                               graphical updates during simulation
//     rtl          04/07/2003   Support simulation bet. user-specified begin
//                               and endtimes
//     mmh          05/06/2003   Rename UpdateSimu -> UpdateWorld
//     mmh          07/08/2003   Add support for clash preview window
//     sha          08/08/2003   Add support for computing the distance in 
//                               FlushObserverlist( boolean ) method
//     sha          12/15/2003   Add new data member: _disableSimulation
//     mmh          12/17/2003   Change the default value for CATCommandMode 
//                               argument in the constructor - use utility
//                               method - allow edit from Auto Sync command
//     mmh          02/12/2004   Add argument in UpdateWorld to allow simulation
//                               to be synchronized without updates
//     mmh          03/16/2004   Allow users to customize the Simulation Control
//                               Tools toolbar
//     mmh          05/12/2004   Fix A0450204/A0450216 - make data members 
//                               protected - to be set to NULL in the Cancel method
//                               method of derived commands
//     sha          09/21/2004   Adhere to new interface DNBISimUpdate (for
//                               updating the sim in analysis verbose mode)
//     mmh          12/29/2004   Fix A0465056: add NotifyPause method
//     mmh          03/17/2005   Add NLS support for MaxTime message
//     mmh          11/11/2005   E0497819: add new data member to force the
//                               call of AbortLightTransaction in Cancel
//     smw          09/13/2006   Fix IR A0554628
//     mmh          12/15/2006   Fix A0565018: provide data member to manage
//                               simulation context
//
//==============================================================================
#ifndef DNBTimeBasedSimCmd_H
#define DNBTimeBasedSimCmd_H

#include <DNBSystemBase.h>
#include <scl_string.h>
#include <scl_list.h>
#include <DNBSystemDefs.h>

#include <DNBSimTime.h>
#include <DNBTimeletClock.h>
// #include <DNBIntervalTimer.h>        // smw; caa
class DNBIntervalTimer;                 // smw; caa
#include <DNBProclet.h>
#include <DNBBasicSimCmd.h>
// #include <DNBStepletBase.h>          // smw; caa
#include <DNBTimeUtilities.h>

#include <DNBCATBase.h>
#include <CATISPPChildManagement.h>
#include <CATISPPAbstractActivity.h>
#include <DNBIBuild.h>
#include <DNBFunctorUtilities.h>
#include <DNBCATDefs.h>

#include <DNBSimulationCommands.h>

class DNBTimeProclet;
class DNBRunBetweenProclet;
class DNBRestoreInitProclet;
class DNBProcessController;
class CATPathElementAgent;
class CATSO;
class CATDialogState;
class DNBSimActAgent;
class DNBCleanAgent;
class CATDlgEditor;

//------------------------------------------------------------------------------

/**
 * Base class for the time based simulation commmands.
 */
class ExportedByDNBSimulationCommands DNBTimeBasedSimCmd : public DNBBasicSimCmd
{
    CATDeclareClass;
    
    DNB_DECLARE_CLASS_PERF_TRACE

public:

    struct DNBSimCtrlTools_Custom
    {
        int         iOptions;
        int         iActOpt;
        DNBSimTime  bStartTime;
        DNBSimTime  bStopTime;
        int         iVisOpt;
        int         iSimSteps;

        DNBSimCtrlTools_Custom() :
        iOptions(0), iActOpt(0),
        bStartTime(0.0), bStopTime(0.0),
        iVisOpt(0), iSimSteps(0)
        {}
    } ;

    /**
     * Constructs a DNBTimeBasedSimCmd object.
     * @param cname
     *  The command identifier. It must be unique and is used to retrieve the 
     *  undo/redo titles in the command message file.
     * @param iBehavior
     *  The behavior of the command.
     * @param iMode
     *  The focus mode.
     *  <br><b>Legal values</b>: With the default mode 
     *  <tt>CATCommandModeExclusive</tt>, the command takes the focus and 
     *  cancels the other commands. This mode is mandatory if the command 
     *  modifies the model.
     *  <br>The other possible mode is <tt>CATCommandModeShared</tt>: the 
     *  command only deactivates the active command and takes the focus. The 
     *  previous command will resume when the current command ends.
     *  <br>The <tt>CATCommandModeUndefined</tt> mode is forbidden with a main 
     *  state command since it needs to take the focus.
     * @param simMode
     *  The simulation mode.
     *  <br><b>Legal values</b>: <tt>DNB_BUILD_TIME</tt> (default value), 
     *  <tt>DNB_RUN_TIME</tt>, <tt>DNB_UNDEFINED</tt>. For the 
     *  <tt>DNB_BUILD_TIME</tt> and <tt>DNB_RUN_TIME</tt> modes, the settings 
     *  from Tools/Options <tt>Simulation</tt> will be considered.
     * @param displayInventorySpaceWnd
     *  The inventory space window status.
     *  <br><b>Legal values</b>: <tt>TRUE</tt> (default value), <tt>FALSE</tt>.
     */
    DNBTimeBasedSimCmd( const CATString &cname,
        CATDlgEngBehavior iBehavior = NULL,
        CATCommandMode iMode = DNBGetSimCommandMode(),
        int simMode = DNB_BUILD_TIME,
        boolean displayInventorySpaceWnd = TRUE,
        int resourceContext = DNB_PROCESS_CTX );
    virtual ~DNBTimeBasedSimCmd();
    /**
     * Builds the command state chart.
     */
    void BuildGraph();
    /**
     * Activates the command. 
     * <br><b>Role</b>: Called by the command selector to give the focus
     * to the command.
     * @param iCmd
     *   The command that requests to activate the current one
     * @param iNotif
     *   The notification sent
     * @return The command status.
     */
    virtual CATStatusChangeRC Activate( CATCommand* iCmd,
                                        CATNotification* iNotif );
    /**
     * Deactivates the command.
     * <br><b>Role</b>: Called by the command selector to temporarily
     * withdraw the focus from the command.
     * @param iCmd
     *   The command that requests to deactivate the current one
     * @param iNotif
     *   The notification sent
     * @return The command status.
     */
    virtual CATStatusChangeRC Desactivate( CATCommand* iCmd,
                                        CATNotification* iNotif );
    /**
     * Cancels the command.
     * <br><b>Role</b>: Called by the command selector to definitely
     * withdraw the focus from the command.
     * The command should then request its destruction.
     * @param iCmd
     *   The command that requests to cancel the current one
     * @param iNotif
     *   The notification sent
     * @return The command status.
     */
    virtual CATStatusChangeRC Cancel( CATCommand* iCmd,
                                        CATNotification* iNotif );
    /**
     * Simulation controller callback called at the end of every cycle time. It
     * allows the command to perform specific operations, depending on the 
     * controller status and time.
     * @param data
     *  Important information about the simulation controller, containing
     *  time and status information.
     */
    void PostFunc( const DNBTimeletClock::CycleCallbackData &data);
    void PostFunc2( const DNBSimletController::CycleCallbackData& data);
    /**
     * Returns the simulation controller.
     */
    DNBTimeletClock *GetController();

    /**
     * Returns the controller. Implements the base class pure virtual function.
     * @return The pointer to the simulation controller, converted into a 
     *  pointer to the controller base class.
     */
    DNBSimletController *GetSimController();
    /**
     * Returns the interval timer. Used usually for accessing the simulation 
     * step size.
     */
    DNBIntervalTimer* GetIntervalTimer();
    /**
     * Returns the process controller (extension of the simulation controller).
     */
    DNBProcessController *GetProcessController();
    /**
     * Creates the simulation controller, builds the WDM entities and adds the 
     * necessary functors, proclets and callbacks.
     * @param postfunc
     *  If TRUE, the <tt>PostFunc</tt> callback will be created (default TRUE).
     * @param buildproc
     *  If TRUE, the process will be built into WDM (default TRUE)..
     * @param buildcycletime
     *  If TRUE, the DNBActivityVisitor will be appended and the functors used
     *  by Gantt chart and activities highlight will be created (default FALSE).
     * @return TRUE if the WDM was created, FALSE otherwise.
     */
    boolean SetCommandDefaults( boolean postfunc = TRUE, 
                                boolean buildproc = TRUE,
                                boolean visualfeedback = FALSE );
    /**
     * Sets the reps update only if the current running command is not in
     * RunTime mode, otherwise calls ::DNBSetApplyToReps(1).
     */
    void DNBSetApplyToReps();
    /**
     * Retrieves the simulation controller.
     */
    void CreateController();
    /**
     * Retrieves the process traversers.
     */
    void CreateProcTraversers();
    /**
     * Updates the geometry at the beginning or at the end of a given activity.
     * @param activity
     *  The activity to be used as a reference for update.
     * @param oldActivity
     *  The previous activity; if this one exists, it's end breakpoint will be 
     *  removed.
     * @param simulateToEnd
     *  If TRUE, the breakpoint will be set at the end of the activity, 
     *  otherwise it will be set at beginning (default TRUE).
     * @param animate
     *  If TRUE, the update will be animated (when the simulation runs forward)
     *  (default FALSE).
     * @param wait
     *  If TRUE, the execution will wait for the simulation to finish 
     *  (default FALSE).
     * @param reset
     *  If TRUE, the controller will be reseted (default FALSE).
     * @param update
     *  If FALSE, the simulation will not do graphical updates (applied only 
     *  if wait parameter is TRUE) (default TRUE).
     */
    void UpdateWorld( const CATISPPAbstractActivity_var &activity,
        const CATISPPAbstractActivity_var &oldActivity,
        boolean simulateToEnd = TRUE,
        boolean animate = FALSE,
        boolean wait = FALSE,
        boolean reset = FALSE,
        boolean update = TRUE );  
    /**
     * Updates the simulation at the beginning/end of a given proclet.
     * @param newPlet
     *  The proclet to be used as a reference for update.
     * @param oldPlet
     *  The previous plet; if this one exists, it's begin/end breakpoint will be 
     *  removed.
     * @param animate
     *  If TRUE, the update will be animated (when the simulation runs forward)
     *  (default FALSE).
     * @param reset
     *  If TRUE, the controller will be reseted (default FALSE).
     * @param wait
     *  If TRUE, the execution will wait for the simulation to finish 
     *  (default FALSE).
     * @param update
     *  If FALSE, the simulation will not do graphical updates (applied only 
     *  if wait parameter is TRUE) (default TRUE).
     * @param simulateToEnd
     *  If TRUE, the breakpoint will be set at the end of the given proclet, 
     *  otherwise it will be set at beginning (default TRUE).
     */    
    void UpdateWorld( DNBProclet* newPlet,
        DNBProclet* oldPlet,
        boolean animate = FALSE,
        boolean reset = FALSE,
        boolean wait = FALSE,
        boolean update = TRUE,
		boolean simulateToEnd = TRUE);
    /**
     * Updates the simulation at the given time.
     * @param newTime
     *  The time to be used for update.
     * @param animate
     *  If TRUE, the update will be animated (when the simulation runs forward)
     *  (default FALSE).
     * @param reset
     *  If TRUE, the controller will be reseted (default FALSE).
     * @param wait
     *  If TRUE, the execution will wait for the simulation to finish 
     *  (default FALSE).
     * @param enableAnalysis
     *  If TRUE, the analysis will be disabled (default FALSE).
     * @param update
     *  If FALSE, the simulation will not do graphical updates (applied only 
     *  if wait parameter is TRUE) (default TRUE).
     */
    void UpdateWorld( DNBSimTime& newTime,
        boolean animate = FALSE,
        boolean reset = FALSE,
        boolean wait = FALSE,
        boolean enableAnalysis = FALSE,
        boolean update = TRUE );
    /**
     * Enables/disables the text/camera/hyperlink observers.
     * @param sVal
     *  The settings used for controlling the enable/disable operations.
     * @return TRUE if the settings were applied, FALSE otherwise.
     */
    boolean ApplySettings( const DNBBasicSimCmd::DNBSettingsVal& sVal )
        DNB_THROW_SPEC_NULL;

    typedef DNBProclet::BreakpointLocation BreakpointLocation;
    typedef DNBProclet::BreakpointType BreakpointType;
    typedef DNBProclet::BreakpointData BreakpointData;
    
    /**
     * Sets a breakpoint on a given activity, the simulation controller will 
     * execute specific operations when the breakpoint is reached, depending on 
     * the breakpoint type.
     * @param act
     *  The activity on which the breakpoint will be set.
     * @param where
     *  Where to set the breakpoint.
     *  <br><b>Legal values</b>:
     *  <ul>
     *  <li><tt>DNBProclet::BreakAtBegin</tt>: the breakpoint will be set at 
     *  the beginning of the activity;</li>
     *  <li><tt>DNBProclet::BreakAtEnd</tt> the breakpoint will be set at the 
     *  end of the activity.</li>
     *  </ul>
     * @param type
     *  The type of the breakpoint: 
     *  <br><b>Legal values</b>: 
     *  <ul>
     *  <li><tt>DNBProclet::BreakNone</tt>: signal when the simulation reaches
     *  the breakpoint so other actions can be done</li>
     *  <li><tt>DNBProclet::BreakHalt</tt>: the simulation controller will be
     *  halted</li>
     *  <li><tt>DNBProclet::BreakDone</tt>: the simulation controller will be
     *  done</li>
     *  </ul>
     * @param data
     *  Useful information for <tt>BreakNone</tt> breakpoint type.
     */
    void setBreakpoint( const CATISPPAbstractActivity_var & act, 
        BreakpointLocation where, 
        BreakpointType type, 
        BreakpointData data );
    /**
     * Removes a breakpoint from a given activity.
     * @param act
     *  The activity from which the breakpoint will be removed.
     * @param where
     *  From where to remove the breakpoint.
     *  <br><b>Legal values</b>:
     *  <ul>
     *  <li><tt>DNBProclet::BreakAtBegin</tt>: removes the breakpoint from the 
     *  beginning of the activity;</li>
     *  <li><tt>DNBProclet::BreakAtEnd</tt> removes the breakpoint from the end 
     *  of the activity.</li>
     *  </ul>
     */
    void resetBreakpoint( const CATISPPAbstractActivity_var & act, 
        BreakpointLocation where );
    /**
     * Builds the specified activity into WDM.
     * @param act
     *  The activity to be built.
     * @param buildType
     *  The type of build (default <tt>BranchBuild</tt>).
     */
    HRESULT buildActivity( const CATISPPAbstractActivity_var &act, 
        DNBBuildType buildType = BranchBuild );
    /**
     * Destroys the specified activity from WDM.
     * @param act
     *  The activity to be destroyed.
     */
    void destroyActivity( const CATISPPAbstractActivity_var &act );
    /**
     * Searches throughout the Process for specific Activity types.
     * @param type
     *  The name of the Activity type used for the search, by default, equals
     *  to Activity (Physical).
     * @return TRUE if the activity type was found, FALSE otherwise.
     */
    boolean hasActivityType(const CATString& type ="Activity");
    /**
     * Attaches a selection agent to the given dialog state.
     */
    void attachSelAgent( CATDialogState* iState );
    /**
     * Callback related to the selection agent (RMB).
     */
    boolean cbSelAgent(void *);
    /**
     * Callback related to the multi-selection agent (LMB).
     */
    boolean cbMultiSelAgent(void *);
    /**
     * Callback related to the clean selection agent.
     */
    boolean cbCleanSelAgent(void *);
    /**
     * Condition evaluated when the clean selection agent is activated.
     */
    boolean condCleanSelAgent(void *);
    /**
     * Builds the correspondent handle for the given object and sets the proper
     * values for the analysis configuration, message window system.
     */
    DNBBasicEntity::Handle buildHandle( const CATBaseUnknown_var &object,
                    const CATBaseUnknown_var &father = NULL_var );
    /**
     * Builds the correspondent handle for the given object and sets the proper
     * values for the analysis configuration, message window system, functors.
     */
    DNBBasicEntity::Handle buildHandle( unsigned int iMask, 
                    const CATBaseUnknown_var &object,
                    const CATBaseUnknown_var &father = NULL_var );
    /**
     * Sets the current process to be simulated.
     */
    void setCurrentProcess( const CATISPPChildManagement_var& );
    /**
     * Returns the current process to be simulated.
     */
    CATISPPChildManagement_var getCurrentProcess();
    /**
     * Adds the observers and functors needed by the visual feedback (highlight,
     * time line in Gantt Chart).
     */
    void AddVisualFeedback();
    /**
     * Flushes the reps to the model. Can be used by the derived commands to
     * syncronize the model earlier than the end of the command (Cancel).
     */
    void FlushModel();
    /**
     * Push changes to main thread when changes were initiated by main thread.
     */
    virtual void FlushObserverlist();
    /**
     * Push changes to main thread when changes were initiated by main thread.
     * In addition, this method computes the existing analysis if required by
     * the given argument.
     */
    virtual boolean FlushObserverlist( boolean compute_analysis );
    /**
     * Enables or disables the viewpoint simulation.
     */
    HRESULT ValidateViewpoint();

    /**
     * Sets the visuStatus. This controls the graphical upate
     * during simulation
     */
    void setVisuStatus(boolean );
    /**
     * Returns the currnent Visualization status
     */
    boolean getVisuStatus();
    /**
     * Sets stepsPerUpdate. This controls the steps per graphical update
     * during simulation
     */
    void setStepsPerUpdate(int );
    /**
     * Returns the current steps per graphical update
     */
    int getStepsPerUpdate();


    /**
     * Updates simulation to the time specified 
     */
    DNBSimletController::RunState jumpToUntilTime(DNBSimTime time, boolean timer = FALSE);

    /**
     * Format time with units in the editor
     */
    void FormatTime(CATDlgEditor *, double time);

    /**
     * Returns the current simulation time
     */
    DNBSimTime getSimTime();

    enum DNBSpecifiedTime
    {
        BeginTime,
        EndTime
    };


    /**
     * Attaches the Proclet representing the user specified begintime/endtime with activities' start
     * and stop proclet
     */
    void attach(CATISPPChildManagement_var, CATISPPChildManagement_var, DNBSimTime, DNBSpecifiedTime);

    // same as above, for resource context 
    void attachResourceContext( DNBSimTime _beginTime, DNBSpecifiedTime);

    void detach(DNBSpecifiedTime);

    /**
     * Checks  if the user has specified a begin/endtime
     */
    bool isTimeSpecified(DNBSpecifiedTime);

    /**
     * Sets break point in begin/end Proclets that represent user-specified begin/endtime
     */
    void setBreakpoint( DNBSpecifiedTime spTime,
        BreakpointLocation where, 
        BreakpointType type, 
        BreakpointData data );

    /**
     * Resets break point in begin/end Proclets that represent user-specified begin/endtime
     */
    void resetBreakpoint( DNBSpecifiedTime spTime,
        BreakpointLocation where);

    /**
     * Return user-specified begin/endtime
     */
    DNBSimTime getSpecifiedTime(DNBSpecifiedTime);

    boolean isReset();

    void setReset(boolean);

    /**
     * Notifies the command that a new command will be activated.
     */
    void SetSpecEdit( const boolean iInEdit );
    /**
     * Notifies the command that a Pause activity was simulated.
     */
    virtual HRESULT NotifyPause( void *iModelId, double iSimTime );

    virtual void manageInstantReplay( bool state, bool shutdown = false );

    /**
     * Returns the simulation context.
     */
    int GetSimContext();

protected:

    void manageInstantReplaySteplets( bool state );
    /**
     * Builds the camera into WDM.
     */
    virtual void BuildWDMCamera();
    /**
     * Builds the <tt>Product</tt> branch from the PPR tree into WDM entities.
     */
    virtual void BuildWDMProduct();
    /**
     * Builds the <tt>Process</tt> branch from the PPR tree into WDM entities.
     */
    virtual void BuildWDMProcess();
    /**
     * Destroys the WDM entities for the current Process.
     */
    virtual void DestroyWDMProcess();
    /**
     * Simulation controller callback called at the end of every cycle time. It
     * determines whether the sim frame should be visualized
     * @param data
     *  Important information about the simulation controller, containing
     *  time and status information.
     */
    void FilterFrame( const DNBTimeletClock::CycleCallbackData &data );

    /**
     * Returns the first state from the graph.
     */
    CATDialogState* GetFirstGraphState();
    /**
     * Returns the last state from the graph.
     */
    CATDialogState* GetLastGraphState();

    /**
     * Checks to see if a graphical update is required at the current
     * time step. Updates the Visualise message as required.
     */
    void checkAnimation( const DNBTimeletClock::CycleCallbackData &data );


    DNBIntervalTimer            *_IntervalTimer;
    
    CATPathElementAgent         *_SelAgent;
    CATBaseUnknown_var          _objectSel;
    CATPathElementAgent         *_MultiSelAgent;
    CATSO                       * _prodSel;
    DNBCleanAgent               *_CleanSelAgent;

    int                         _errorFound;
    CATISPPChildManagement_var  _currentProcess;

    DNBProcessTraverser         *_ProcTrav;
    DNBProcessTraverser         *_DestroyProcTrav;

    CATDialogState              *_FirstState;
    CATDialogState              *_LastState;

    DNBSimActAgent              *_ActSelAgent;

    boolean                     _DefaultGraph;
    boolean                     _EnableViewpoint;
    int                         _ActSelAgentOpt;
    boolean                     _EnableSimulation;
    DNBRestoreInitProclet       *_rproclet;

    DNBSimTime                  _usBeginTime;
    DNBSimTime                  _usEndTime;
    DNBRunBetweenProclet        *_beginPlet;
    DNBRunBetweenProclet        *_endPlet;
    boolean                     _reset;

    boolean                     _disableSimulation;

    static boolean              _IsSpecInEdit;
    static CATString           *_DefaultCommand;

    DNBSimCtrlTools_Custom      _dynToolsCustom;
    DNBTimeProclet              *_tproclet;

    scl_wstring                 _wsMaxTime;

    boolean                     _bAbortLightTrans;

    // SMW: the members below are only applicable in the resource context
    CATISPPChildManagement_var    _topmostBehavior;  // first behavior node found in the resource list
    // this helps us decide that we do have a behavior to simulate and also gives us a node to use for the
    // Gantt chart time solver used by resource simulation command, update cycle time for resource simulation,etc..
    DNBProclet                  * _CurrentStartProclet;
    DNBProclet                  * _CurrentEndProclet;

    int                         _simContext;
	int							_SwitchActivityOverride;

public:
    /**
     * Halts the simulation controller.
     */
    HRESULT HaltSimController();
    /**
     * Update the 3D world at the desired simulation time.
     */
    HRESULT Update3DWorld( double time,
                           boolean animate = FALSE,
                           boolean reset = FALSE,
                           boolean wait = FALSE,
                           boolean enableAnalysis = FALSE,
                           boolean update = TRUE );

    // smw: The following is only valid / applicable in the resource context
    // It is not stored as a beahvior because of CAA issues
    CATISPPChildManagement_var GetTopmostBehavior( CATIProduct_var StartingFromResource = NULL_var );
    // Set / Get the current start / end  proclet which are the start / end proclet of the whole 
    // resource hierarchy.  Derived commands (e.g. resource simulation command ) 
    // can overwrite these with the pre-selected resource's proclets, if any
    DNBProclet * GetCurrentStartProclet();
    DNBProclet * GetCurrentEndProclet();
    void  SetCurrentStartProclet( DNBProclet * );
    void  SetCurrentEndProclet( DNBProclet * );

    void SelectiveV5AttachBuild();

private:
    /**
     * Gets the Start and Stop proclets for the given activity.
     */
    HRESULT PrevStopNextStart( const CATISPPAbstractActivity_var &act, 
        DNBProclet **prevStop, DNBProclet **nextStart );

    DNBSimTime                  _simTime;

    /** 
     * Members to manage graphical updates during simulation
     */
    boolean                     _visuStatus;
    int                         _stepsPerUpdate;
    int                         _updateCounter;
};

//------------------------------------------------------------------------------

#endif
