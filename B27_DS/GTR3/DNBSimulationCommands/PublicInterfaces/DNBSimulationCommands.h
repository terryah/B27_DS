//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/** @CAA2Required */
/*---------------------------------------------------------------------*/
/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */
/*---------------------------------------------------------------------*/
//
//    This Header file is included by all the Module.h header files in 
//    PublicInterfaces
//

#ifndef DNBSIMULATIONCOMMANDS_H

#define DNBSIMULATIONCOMMANDS_H DNBSimulationCommands

#ifdef _WINDOWS_SOURCE
#if defined(__DNBSimulationCommands)
#define ExportedByDNBSimulationCommands __declspec(dllexport)
#else
#define ExportedByDNBSimulationCommands __declspec(dllimport)
#endif
#else
#define ExportedByDNBSimulationCommands
#endif

#endif /* DNBSIMULATIONCOMMANDS_H */
