/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBSimDynamicToolBar.h
//      Dynamic toolbar used by the simulation commands.
//
//==============================================================================
//
// Usage notes: 
//      Dynamic toolbar used by the simulation commands in order to provide 
//      simulation specific options/commands.
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     mmh          12/31/2001   Initial implementation
//     mmh          05/29/2002   Add new button for activity options
//     mmh          06/11/2002   Add button for Real-time simulation
//     sha          06/18/2002   Add new argument to the constructor:
//                               DNBSimProcessToolBar *
//     mmh          11/26/2002   Support command migration to new base class
//     rtl          03/28/2003   Changes to support graphical updates during sim.
//     mmh          06/24/2003   Disable viewpoint for animation OFF
//     mmh          09/03/2003   Update Cycle Time in simulation
//     mmh          09/17/2003   Add data for update cycle time option
//     mmh          03/16/2004   Allow user to customize this toolbar
//     mmh          06/16/2004   Expose data members as protected, methods as
//                               virtual to allow customization
//     mmh          11/29/2004   Fix A0473826: add method for correct clean-up
//     mmh          04/15/2005   support UI change in DNBActStatusDlg(R16DMR003)
//	   aku			11/22/2005	 support Analysis Status
//
//==============================================================================

#ifndef DNBSimDynamicToolBar_H
#define DNBSimDynamicToolBar_H

#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>

#include <DNBTimeBasedSimCmd.h>
#include <DNBCATBase.h>
#include <CATDlgToolBar.h>
#include <CATBooleanDef.h>
#include <DNBSimulationCommands.h>
#include <DNBCATDefs.h>

class CATDlgCheckButton;
class CATDlgEditor;
class CATDlgLabel;
class CATDocument;
class DNBSimAnalysisDialog;
class DNBSimActOptionsDlg;
class DNBSimVisuOptionsDlg;
class DNBSimProcessToolBar;

//------------------------------------------------------------------------------

/**
 * Dynamic toolbar used by the simulation commands.
 */
class ExportedByDNBSimulationCommands DNBSimDynamicToolBar : public CATDlgToolBar
{
    DeclareResource( DNBSimDynamicToolBar, CATDlgToolBar )

public:
    /**
     * Constructor for a DNBSimDynamicToolBar object.
     */
    DNBSimDynamicToolBar( DNBTimeBasedSimCmd *, const CATString &, 
                            DNBSimProcessToolBar *, 
                            DNBTimeBasedSimCmd::DNBSimCtrlTools_Custom & );
    /**
     * Destructor for a DNBSimDynamicToolBar object.
     */
    ~DNBSimDynamicToolBar();

    /**
     * Desactivates the Options button on the toolbar.
     */
    virtual void ResetSimOptions();
    /**
     * Desactivates the Simulated Activities button on the toolbar.
     */
    virtual void ResetSimActivities();
    /**
     * Desactivates the Activity Options button on the toolbar.
     */
    virtual void ResetSimActOptions();

    /**
     * Deactivates the Visulization Options button on the toolbar.
     */
    virtual void ResetSimVisOptions();

    /**
     * Desactivates the Activity Options button on the toolbar.
     */
    virtual void ResetSimInstantReplay();

    /**
     * Controls the viewpoint option when the animation is turned ON/OFF.
     * @param iStatus
     * The value that specifies if the viewpoint has to be disabled or 
     * restored to the previous value.
     *  <br><b>Legal values</b>: 
     *     <tt>1 (default)</tt> restores the previous value, or 
     *     <tt>0</tt> disables the viewpoint.
     */
    virtual void ControlViewpoint( int iStatus = 1 );

    /**
     * Gets the status of the update cycle time flag from the associated dialog.
     */
    virtual boolean GetUpdateCTStatus();

    /**
     * Cleans up the document-depending objects.
     */
    virtual void CleanUp( CATDocument *iDoc, int action = 0 );
    virtual void Hide( CATDocument *iDoc );
    virtual void Show( CATDocument *iDoc );

    void SetButtonSensitivity( CATULong iState, int iMask = DNB_SCT_ALL );

protected:
    CATDlgCheckButton       *_bSimOptions;
    CATDlgCheckButton       *_bSimActivities;
    CATDlgCheckButton       *_bSimActOptions;
    CATDlgCheckButton       *_bSimRealtime;
    CATDlgCheckButton       *_bAnimation;
    CATDlgLabel             *_bSeparatorIR;
    CATDlgCheckButton       *_bSimInstantReplay;
    CATDlgEditor            *_bSimBufferIR;

    DNBTimeBasedSimCmd      *_Father;
    CATString               _fatherName;

    DNBSimProcessToolBar    *_simProcessToolBar;

    DNBSimAnalysisDialog    *_simAnalysisDlg;
    DNBSimActOptionsDlg     *_simActOptDlg;
    DNBSimVisuOptionsDlg    *_simVisOptDlg;

    boolean                 _updateCycleTime;

    // customized values to be used
    int                     _iActOpt;
    DNBSimTime              _bStartTime;
    DNBSimTime              _bStopTime;
    int                     _iVisOpt;
    int                     _iSimSteps;

    /**
     * Callback activated when clicking on the Options button on the toolbar.
     * Toggles On/Off the Options dialog.
     */
    virtual void cb_SimOptions( CATCommand*,CATNotification*,CATCommandClientData );
    /**
     * Callback activated when clicking on the Simulated Activities button on 
     * the toolbar. Toggles On/Off the Simulated Activities dialog.
     */
    virtual void cb_SimActivities( CATCommand*,CATNotification*,CATCommandClientData );
    /**
     * Callback activated when clicking on the Activity Options button on the 
     * toolbar. Toggles On/Off the Activity Options dialog.
     */
    virtual void cb_SimActOptions( CATCommand*,CATNotification*,CATCommandClientData );
    /**
     * Callback activated when clicking on the Realtime Simulation button on the
     * toolbar.
     */
    virtual void cb_SimRealtime( CATCommand*,CATNotification*,CATCommandClientData );

    /**
     * Callback activated when clicking on the Animation button on the
     * toolbar.
     */
    virtual void cb_Animation( CATCommand*,CATNotification*,CATCommandClientData );

    /**
     * Callback activated when clicking on the Instant Replay button on the
     * toolbar.
     */
    virtual void cb_SimInstantReplay( CATCommand*,CATNotification*,CATCommandClientData );
    /**
     * Callback activated when the Instant Replay Buffer Size is edited.
     */
    virtual void cb_editIRBufferSize( CATCommand *cmd, CATNotification *evt, 
                                    CATCommandClientData data );
};

//------------------------------------------------------------------------------

#endif  // DNBSimDynamicToolBar_H
