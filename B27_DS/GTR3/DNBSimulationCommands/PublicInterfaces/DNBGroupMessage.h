/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBGroupMessage.h
//      Message used as a container of messages.
//
//==============================================================================
//
// Usage notes: 
//      This message is used as a container of messages. Every time this message
//      is evaluated, it tries to evaluate all enclosed messages.
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     mmg          01/01/1999   Initial implementation
//     mmg          08/30/2002   override post method
//     mmg          10/10/2002   add remove_message method
//
//==============================================================================
#if DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _DNB_GROUPMESSAGE_H_
#define _DNB_GROUPMESSAGE_H_

#include <DNBSystemBase.h>
#include <scl_memory.h>
#include <scl_list.h>
#include <DNBSystemDefs.h>
#include <DNBBasicTHRMessage.h>

#include <DNBSimulationCommands.h>

//------------------------------------------------------------------------------

/**
 * Message used as a container of messages.
 * <b>Role</b>: This message is used as a container of messages. Every time this
 * message is evaluated, it tries to evaluate all enclosed messages.
 */
class ExportedByDNBSimulationCommands DNBGroupMessage : 
                                        public DNBBasicTHRMessage
{
public:
    /**
     * Constructs a DNBGroupMessage.
     * @param sync
     *  This parameter is sent to the base class constructor.
     */
    DNBGroupMessage( bool keep_messages = false, int sync = 1 )
        DNB_THROW_SPEC_NULL;
    
    /**
     * Copy constructor.
     */
    DNBGroupMessage( const DNBGroupMessage & )
        DNB_THROW_SPEC_NULL;
    
    /**
     * Assignment operator - assigns the contained messages.
     */
    void operator=( const DNBGroupMessage & )
        DNB_THROW_SPEC_NULL;
    
    ~DNBGroupMessage()
        DNB_THROW_SPEC_NULL;
    
    /**
     * Evaluates the message - tries to evaluate all contained messages.
     * @return TRUE if all messages were evaluated, FALSE otherwise.
     */
    bool evaluate( bool flushing )
        DNB_THROW_SPEC_ANY;

    bool evaluate()
        DNB_THROW_SPEC_ANY;

    /**
     * Called prior to posting message
     */
    bool
    prepost()
        DNB_THROW_SPEC_ANY;
    
    /**
     * Appends a message at the end of the list.
     * @param iMsg
     *  The message to be appended.
     */
    void push_message( DNBBasicTHRMessage *iMsg )
        DNB_THROW_SPEC_NULL;
    
    /**
     * Inserts a message in the list, before a specified one.
     * @param iMsg
     *  The message to be inserted.
     * @param iBefore
     *  The message in front of which the new message will be inserted.
     */
    void insert_message( DNBBasicTHRMessage *iMsg, 
                         DNBBasicTHRMessage *iBefore )
        DNB_THROW_SPEC_NULL;
    
    /**
     * Removes a message from the list
     * @param iMsg
     *  The message to be removed.
     */
    void remove_message( DNBBasicTHRMessage *iMsg )
        DNB_THROW_SPEC_NULL;

    /**
     * Erases all contained messages.
     */
    void clear()
        DNB_THROW_SPEC_NULL;
    
private:
    typedef scl_vector<DNBBasicTHRMessage*
                         ,DNB_ALLOCATOR(DNBBasicTHRMessage*) > THRMsgVec;
    typedef scl_vector<bool, DNB_ALLOCATOR(bool) > THRMsgPostVec;

    bool          _keep;
    THRMsgVec     _msgs;
    THRMsgPostVec _post;
};

//------------------------------------------------------------------------------

#endif // _DNB_GROUPMESSAGE_H_
