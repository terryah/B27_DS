#
# Copyright Dassault Systemes Provence 2003-2008, all rights reserved
#
#==================================================================================================
# Imakefile for the module CATSubdivisionTopoOperators.m
#==================================================================================================
# 06.06.2012 : RAQ : Ajout CATStoHybridOperators
# 13.10.2011 : RAQ : Ajout MathMeshParam
# 18.11.2010 : RAQ : Ajout CATStoUtil
# 09/04/2008 : MMO : Nettoyage de printemps suite deplacement du NET
# 14/02/2008 : RBD : Ajout CATStoFitting en R18/R203 & R19/R204 pour op�rateurs topo
# 14/02/2008 : RBD : Ajout CATTopologicalOperators, TessPolygon & CATTessellation 
#					 en R18/R203 & R19/R204 pour op�rateurs topo
# 05/10/2007 : RBD : Ajout CATSmaMeshAdvOperators en R18 / R203 pour op�rateurs topo 
# 25/10/2006 : RAQ : Ajout CATSmaFillSubdivision en R18 / R203 pour op�rateurs topo
#                    Ajout CATSubdivisionMeshGenerator en R18 / R203 pour op�rateurs topo
#                    Ajout CATViz en R18 / R203 pour pr�visu topo
# 03/07/2006 : PMG : CATStoDeformation est inclus dans la liste des modules
# 15/09/2005 : RAQ : CATStoTopoOperators est inclus dans la liste des modules
# 08/09/2005 : MMO : Suppression de CATIAV5R17 utilisation uniquement de PROTO_NET
# 27/05/2005 : ANR : Ajout de  CATSurfacicMathematics
# 10/05/2005 : FZA : Ajout de CATStoTopoOperators et de CATFreeFormOperators
# 28/01/2005 : ANR : Ajout de CATSmoOperators ; 
# 24/05/2004 : RAQ : Ajout CATGeometricOperators pour update topo
# 09/02/2004 : RAQ : Optimisation O2 + Portage 64 bits
# 05/01/2004 : RAQ : Cr�ation de CATSubdivisionTopoOperators � partir de CATSubdivisionOperators
# 23/07/2003 : MMO : On supprime CATTopologicalOperators CATAdvancedTopologicalOpe
# 12/06/2003 : JCV : 3 Modules CATSdoTopo CATSdoGeo CATSdoObjects
# 27/03/2003 : JCV : CATGeometricObjects + CATMathStream CATCGMGeoMath (warning)
# 10/03/2003 : MMO : Creation
#
#==================================================================================================
#

BUILT_OBJECT_TYPE=SHARED LIBRARY 


#if defined CATIAV5R25 || defined CATIAR418
INCLUDED_MODULES = CATStoTopo  \
                   CATStoTopoOperators	\
                   CATStoDeformation	\
                   CATStoConv \
                   SubdivTopOpeItf  \
                   CATStoDebug      \
                   CATStoHybridOperators  \
                   CATStoUtil

#elif defined CATIAV5R19 || defined CATIAR204

INCLUDED_MODULES = CATStoTopo  \
                   CATStoTopoOperators	\
                   CATStoDeformation	\
                   CATStoConv \
                   SubdivTopOpeItf  \
                   CATStoDebug      \
                   CATStoHybridOperators

#else
INCLUDED_MODULES = CATStoTopo  \
                   CATStoTopoOperators \
                   CATStoDeformation
#endif

#if defined(PROTO_NET)
LINK_WITH = JS0GROUP                \
			CATSobObjects			\
			CATSapApprox			\
			CATSmoOperators         \
			CATSgoOperators			\
            CATGeometricObjects     \
            CATGMGeometricInterfaces \ 
            CATTopologicalObjects	\
			CATMathematics			\
			CATCGMGeoMath			\
			CATMathStream			\
			CATGeometricOperators   \
            CATFreeFormOperators    \
			CATSurfacicMathematics

#endif

#if defined (CATIAV5R16) || defined (CATIAR201)

LINK_WITH = JS0GROUP                \
            CATSobObjects			\
            CATSapApprox			\
            CATSmoOperators         \
            CATSgoOperators			\
            CATGeometricObjects     \
            CATGMGeometricInterfaces \ 
            CATTopologicalObjects	\
            CATMathematics			\
            CATCGMGeoMath			\
            CATMathStream			\
            CATGeometricOperators   \
            CATFreeFormOperators    \
            CATSurfacicGeoOperators \
            CATAdvancedMathematics \
            CATGMModelInterfaces \
            CATGMOperatorsInterfaces \
            CATGMAdvancedOperatorsInterfaces
# else

LINK_WITH = JS0GROUP                \
			CATSobObjects			\
			CATSapApprox			\
			CATSmoOperators         \
			CATSgoOperators			\
            CATGeometricObjects     \ 
            CATTopologicalObjects	\
			CATMathematics			\
			CATCGMGeoMath			\
			CATMathStream			\
			CATGeometricOperators    
			
#endif

#if defined CATIAV5R18 || defined CATIAR203

LINK_WITH = JS0GROUP                \
            CATStoFitting           \
            CATSobObjects           \
            CATSapApprox            \
            CATSmoOperators         \
            CATSgoOperators         \
            CATGeometricObjects     \
            CATGMGeometricInterfaces \
            CATTopologicalObjects   \
            CATMathematics          \
            CATCGMGeoMath           \
            CATMathStream           \
            CATGeometricOperators   \
            CATFreeFormOperators    \
            CATSurfacicGeoOperators \
            CATAdvancedMathematics  \
            CATSmaFillSubdivision   \
            CATSmaMeshAdvOperators  \
            CATSubdivisionMeshGenerator \
            CATViz \
  CATGMModelInterfaces \
  CATGMOperatorsInterfaces \
  CATGMAdvancedOperatorsInterfaces
#endif

#if defined CATIAV5R19 || defined CATIAR204

LINK_WITH = JS0GROUP                \
			CATStoFitting			\
            CATSobObjects			\
            CATSapApprox			\
            CATSmoOperators         \
            CATSgoOperators			\
            CATGeometricObjects     \
            CATGMGeometricInterfaces \
            CATTopologicalObjects	\
            CATMathematics			\
            CATCGMGeoMath			\
            CATMathStream			\
            CATGeometricOperators   \
            CATFreeFormOperators    \
            CATSurfacicGeoOperators \
            CATAdvancedMathematics  \
            CATSmaFillSubdivision   \
            CATSmaMeshAdvOperators  \
            CATSubdivisionMeshGenerator \
            CATViz					\
            CATPolyhedralObjects \
            CATGMModelInterfaces \
            CATGMOperatorsInterfaces \
            CATGMAdvancedOperatorsInterfaces  \
            CATSobUtilities \
            CATStoUtil      \
            GeoNurbsTools   \
            MathMeshParam  \
            PolyhedralModel
#endif

#if defined CATIAV5R25 || defined CATIAR418

LINK_WITH = JS0GROUP                \
			CATStoFitting			\
            CATSobObjects			\
            CATSapApprox			\
            CATSmoOperators         \
            CATSgoOperators			\
            CATGeometricObjects     \
            CATGMGeometricInterfaces \
            CATTopologicalObjects	\
            CATMathematics			\
            CATCGMGeoMath			\
            CATMathStream			\
            CATGeometricOperators   \
            CATFreeFormOperators    \
            CATSurfacicGeoOperators \
            CATAdvancedMathematics  \
            CATSmaFillSubdivision   \
            CATSmaMeshAdvOperators  \
            CATSubdivisionMeshGenerator \
            CATViz					\
            CATPolyhedralObjects \
            CATGMModelInterfaces \
            CATGMOperatorsInterfaces \
            CATGMAdvancedOperatorsInterfaces  \
            CATSobUtilities \
            GeoNurbsTools   \
            MathMeshParam  \
            PolyhedralModel
#endif

#ifdef CATIAV5R20
ALIASES_ON_IMPORT=CATSubdivisionTopOperators CATGMModelInterfaces CATGMOperatorsInterfaces CATGMAdvancedOperatorsInterfaces
#endif

#==============================================================================

OS = AIX
SYS_LIBS = -lxlf -lxlf90 -lxlfpad

OS = IRIX
SYS_LIBS = -lftn
#
OS = Windows_NT
#if os win_b64
#else
OPTIMIZATION_CPP = /O2
#endif
#
OS = HP-UX
#if os hpux_a
SYS_LIBS = -lf
#else
SYS_LIBS= -lF90
#endif                

OS = hpux_b64 
#if os hpux_a
SYS_LIBS = -lf
#else
SYS_LIBS= -lF90 -lcps 
#endif

OS = SunOS
SYS_LIBS = -lF77 -lM77
