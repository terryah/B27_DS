#
#   Imakefile.mk for DNBClnDBG.m
#   Copyright (C) DELMIA Corp., 2011
#
BUILT_OBJECT_TYPE   = SHARED LIBRARY


LINK_WITH   = JS0GROUP                      \ # System
              JS0FM                         \ # System
              CATClnBase                    \ # CATDataCompatibilityInfra
              CATClnSpecs                   \ # ObjectSpecsModeler
              CATObjectModelerBase          \ # ObjectModelerBase
              CATObjectSpecsModeler         \ # ObjectSpecsModeler
              CATProductStructureInterfaces \ # ProdStructureInterfaces
              CATProductStructure1          \ # ProductStructure
              CATMechanisms                 \ # Mechanisms
              DNBDeviceItf                  \ # DNBDeviceInterfaces
              DNBRobotItf                  \ # DNBDeviceInterfaces
				
