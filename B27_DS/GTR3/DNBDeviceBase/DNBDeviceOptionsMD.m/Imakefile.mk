#
#   Imakefile.mk for DNBDeviceOptionsMD
#   Copyright (C) DELMIA Corp., 2001
#
BUILT_OBJECT_TYPE=SHARED LIBRARY

# DO NOT EDIT :: THE CAA2 WIZARDS WILL ADD CODE HERE
WIZARD_LINK_MODULES =  \
        JS0GROUP CATInfInterfaces DNBDeviceItfCPP DNBDevicePubIDL DNBDeviceInterfacesUUID 
# END WIZARD EDITION ZONE

LINK_WITH           = $(WIZARD_LINK_MODULES)    \
                        OM0EDPRO        \
                        DI0PANV2        \
                        JS0CORBA        \
                        JS0FM           \
                        AD0XXBAS        \
                        JS0FM           \
                        CATDlgStandard  \

#
# Define the build options for the current module.
#
OS      = Windows_NT
BUILD   = YES

OS      = IRIX
BUILD   = YES

OS      = SunOS
BUILD   = YES

OS      = AIX
BUILD   = YES

OS      = HP-UX
BUILD   = YES

OS      = win_a
BUILD   = NO
