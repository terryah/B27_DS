//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
inline  void
DNBBasicFrame3D::setFrameTheta( DNBReal frameTheta )
    DNB_THROW_SPEC_NULL
{
    frameTheta_ = frameTheta;
    postEvent( EventFrameThetaModified );
}


inline  DNBReal
DNBBasicFrame3D::getFrameTheta( ) const
    DNB_THROW_SPEC_NULL
{
    return frameTheta_;
}


inline  void
DNBBasicFrame3D::setFrameAlpha( DNBReal frameAlpha )
    DNB_THROW_SPEC_NULL
{
    frameAlpha_ = frameAlpha;
    postEvent( EventFrameAlphaModified );
}


inline  DNBReal
DNBBasicFrame3D::getFrameAlpha( ) const
    DNB_THROW_SPEC_NULL
{
    return frameAlpha_;
}


inline  void
DNBBasicFrame3D::setFrameLength( DNBReal frameLength )
    DNB_THROW_SPEC_NULL
{
    frameLength_ = frameLength;
    postEvent( EventFrameLengthModified );
}


inline  DNBReal
DNBBasicFrame3D::getFrameLength( ) const
    DNB_THROW_SPEC_NULL
{
    return frameLength_;
}


inline  void
DNBBasicFrame3D::setFrameDistance( DNBReal frameDistance )
    DNB_THROW_SPEC_NULL
{
    frameDistance_ = frameDistance;
    postEvent( EventFrameDistanceModified );
}


inline  DNBReal
DNBBasicFrame3D::getFrameDistance( ) const
    DNB_THROW_SPEC_NULL
{
    return frameDistance_;
}
