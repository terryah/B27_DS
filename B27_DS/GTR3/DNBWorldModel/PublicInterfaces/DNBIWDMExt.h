/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBIWDMExt.h
//   Defines the DNBIWDMExt interface.
//
//==============================================================================
//
// Usage notes: 
//   This interface is used to establish the V5->WDM link by storing the 
//   corresponding WDM handle for the implementing object.
//   Also this interface implements the selective build protocol, which means
//   when GetHandle( TRUE ) is issued, the interface will ensure that the 
//   object as well as all ascendents/descendents are build and then return
//   the handle.
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     mmg          01/01/1999   Initial implementation
//     sha          03/01/2001   Modified GetHandle() method to take two extra
//                               arguments used by selective build protocol:
//                                  father (for V4/V5 mechanism only)
//                                  disableDescendingRecursion (for Replay)
//                               Also added new methods: BuildBranch() and
//                               IsBuild()
//     mmh          03/07/2002   Add method BuildHandle() to deal with functors
//     mmh          08/14/2002   Add new AddFunctors() method to add functors 
//                               after the entity was built
//     mmg          02/11/2003   added DNB_FUNCTOR_OBS_COLOR mask
//     awn          03/13/2003   Removal of const
//     sha          09/23/2003   Change the export from DNBSimulationInterfaces
//                               to DNBWorldModel
//
//==============================================================================
#ifndef DNBI_WDMEXT_H
#define DNBI_WDMEXT_H

#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>
#include <DNBBasicEntity.h>

#include <DNBCATBase.h>
#include <CATBaseUnknown.h>
#include <CATBooleanDef.h>
#include <DNBCATDefs.h>

#include <DNBWorldModel.h>

#define DNB_FUNCTOR_OBS_POSITION    0x00000001
#define DNB_FUNCTOR_OBS_VISIBILITY  0x00000002
#define DNB_FUNCTOR_OBS_VIEWPOINT   0x00000004
#define DNB_FUNCTOR_OBS_TEXT        0x00000008
#define DNB_FUNCTOR_OBS_COLOR       0x00000010
#define DNB_FUNCTOR_OBS_MOVEMENT    0x00000020
#define DNB_FUNCTOR_REC_POSITION    0x00010000
#define DNB_FUNCTOR_REC_VISIBILITY  0x00020000
#define DNB_FUNCTOR_REC_VIEWPOINT   0x00040000
#define DNB_FUNCTOR_REC_TEXT        0x00080000
#define DNB_FUNCTOR_REC_COLOR       0x00100000
#define DNB_FUNCTOR_OBS_ALL         0x0000ffff
#define DNB_FUNCTOR_REC_ALL         0xffff0000

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByDNBWorldModel  IID IID_DNBIWDMExt ;
#else
extern "C" const IID IID_DNBIWDMExt ;
#endif

//------------------------------------------------------------------------------

/**
 *
 */
class ExportedByDNBWorldModel DNBIWDMExt : public CATBaseUnknown
{
    CATDeclareInterface; 

public:

    /**
     * Returns the querried's object correspondent handle.
     * If the build flag is TRUE, this method has to ensure that all ascendents
     * and all descendents for the querried objects are build. This method
     * implements the selective build protocol.
     * The disableDescendingRecursion flag is used only by Replay. When set to
     * TRUE, will prevent the descendents of the querried object to be build.
     */
    virtual DNBBasicEntity::Handle GetHandle( 
        boolean build = FALSE, 
        const CATBaseUnknown_var &father = NULL_var,
        boolean disableDescendingRecursion = FALSE ) =0;

    /**
     * Returns the querried's object correspondent handle, applies the correct
     * functors, based on the mask.
     * All ascendents and all descendents for the querried objects are build. 
     * This method implements the selective build protocol.
     * The disableDescendingRecursion flag is used only by Replay. When set to
     * TRUE, it will prevent the descendents of the querried object to be built.
     */
    virtual DNBBasicEntity::Handle BuildHandle( 
        unsigned int iMask = 0, 
        const CATBaseUnknown_var &father = NULL_var,
        boolean disableDescendingRecursion = FALSE ) =0;

    /**
     * Applies the correct functors, based on the mask.
     */
    virtual HRESULT AddFunctors( unsigned int iMask = 0, 
        const CATBaseUnknown_var &father = NULL_var ) =0;

    /**
     * Sets the querried object with the given handle.
     */
    virtual void SetHandle( const DNBBasicEntity::Handle &handle ) =0;

    /**
     * This method has to be implemented by all objects (e.g. D5Device) that
     * need to ensure that all their descendents are build and not only a 
     * subset of them. By default BuildBranch() returns FALSE.
     */
    virtual boolean BuildBranch() =0;

    /**
     * Returns the build status for the querried object.
     */
    virtual boolean IsBuilt() =0;
};

//------------------------------------------------------------------------------

CATDeclareHandler( DNBIWDMExt, CATBaseUnknown );

#endif //DNBI_WDMEXT_H
