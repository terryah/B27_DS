//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 */

//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     joy         02/05/99    Initial implementation
//*     bkh         11/06/03    Implementation of the new documentation style.
//* 
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _DNB_ATTRIBUTECONSTREVITERATOR_H_
#define _DNB_ATTRIBUTECONSTREVITERATOR_H_




#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>


#include <DNBWorldModel.h>

#include <DNBException.h>
#include <DNBAttributeSet.h>



/**
  * Class to iterate through an extension of attribute data elements 
  * in a reverse order.
  * 
  * <br><B>Template Parameter(s)</B><br>
  * @param  Key 
  * The key associated with each attribute data element.  
  * This class must provide the following public member functions:
  * <UL>
  * <LI> # Key() }
  * <LI> # Key( const Key& ) }
  * <LI> # Key operator=( const Key& ) }
  * <LI> # ~Key() }
  * </UL>
  * @param  Type 
  * The attribute data extension containing attribute data, each 
  * associated with a key. This class must be an extension to a 
  * shared object.
  * #
  * <br><B>Description</B><br>
  * This class represents the encapsulation of a const reverse iterator to 
  * iterate through an extension to a shared object, containing  attribute 
  * data elements, in a reverse order. 
  * 
  * 
  */
template <class Key, class Type >
class DNBAttributeConstRevIterator
{

public:

/**
  * The element type.
  * 
  * <br><B>Description</B><br>
  * This type definition specifies the type of object stored in the
  * iterator.
  * 
  * 
  */
    typedef DNBAttributeBase           value_type;


/**
  * An unsigned integral type.
  * 
  * <br><B>Description</B><br>
  * This type definition specifies an unsigned integral type that can
  * represent the maximum number of elements in the iterator.
  * 
  * 
  */
    typedef size_t      size_type;


/**
  * A signed integral type.
  * 
  * <br><B>Description</B><br>
  * This type definition specifies a signed integral type used to
  * represent the distance between two iterators.
  * 
  * 
  */
    typedef ptrdiff_t   difference_type;



/**
  * A pointer to the element type.
  * 
  * <br><B>Description</B><br>
  * This type definition represents a const pointer to the iterator's 
  * element type.
  * 
  * 
  */
    typedef const DNBAttributeBase*         const_pointer;


/**
  * A const reference to the element type.
  * 
  * <br><B>Description</B><br>
  * This type definition represents a const reference to the 
  * iterator's element type.
  * 
  * 
  */
    typedef const DNBAttributeBase&         const_reference;

/**
  * Encapsulation of a const reverse iterator to the attribute data 
  * in an extension.
  * 
  * <br><B>Description</B><br>
  * This type definition represents an encapsulation of a const reverse 
  * iterator to the attribute data in an extension. 
  * 
  * 
  */
    typedef typename DNBAttributeSet<Key>::ConstReverseIterator	 ConstReverseIterator;


/**
  * Encapsulation of a key-value pair.
  * 
  * <br><B>Description</B><br>
  * This type definition specifies the encapsulation of a key-value
  * pair. 
  * 
  * 
  */
    typedef typename DNBAttributeSet<Key>::ValueType	 ValueType;


/**
  * Constructor.
  * @param  hExt 
  * Handle to extension.
  * @param  iter 
  * Iterator to be encapsulated.
  * 
  * @return
  * Nothing.
  * 
  * <br><B>Description</B><br>
  * This function creates an object of type DNBAttributeRevConstIterator
  * encapsulating <tt>iter</tt>.
  * 
  */
    DNBAttributeConstRevIterator( DNBSharedHandle<Type>  hExt, 
				  ConstReverseIterator  iter )
  				DNB_THROW_SPEC_NULL;

/**
  * Destructor.
  * 
  * @return
  * Nothing.
  * 
  * <br><B>Description</B><br>
  * Destructor of DNBAttributeConstRevIterator.
  * 
  * 
  */
    ~DNBAttributeConstRevIterator( )
  		DNB_THROW_SPEC_NULL;

/**
  * Prefix increment operator.
  * 
  * @return
  * Object of type DNBAttributeConstRevIterator. 
  * 
  * <br><B>Description</B><br>
  * This function increments the const reverse iterator to the next 
  * position.
  * 
  */
    inline DNBAttributeConstRevIterator<Key, Type> operator++()
  		DNB_THROW_SPEC_NULL;

/**
  * Prefix decrement operator.
  * 
  * @return
  * Object of type DNBAttributeConstRevIterator. 
  * 
  * <br><B>Description</B><br>
  * This function decrements the const reverse iterator to the previous 
  * position.
  * 
  */
    inline DNBAttributeConstRevIterator<Key, Type> operator--()
  		DNB_THROW_SPEC_NULL;

/**
  * Postfix increment operator.
  * 
  * @return
  * Object of type DNBAttributeConstRevIterator. 
  * 
  * <br><B>Description</B><br>
  * This function increments the const reverse iterator to the next 
  * position.
  * 
  * 
  */
    inline DNBAttributeConstRevIterator<Key, Type> operator++( int )
  		DNB_THROW_SPEC_NULL;

/**
  * Postfix decrement operator.
  * 
  * @return
  * Object of type DNBAttributeConstRevIterator. 
  * 
  * <br><B>Description</B><br>
  * This function decrements the const reverse iterator to the previous 
  * position.
  * 
  */
    inline DNBAttributeConstRevIterator<Key, Type> operator--( int )
  		DNB_THROW_SPEC_NULL;

/**
  * Equality operator.
  * @param  right 
  * Value to be compared against.
  * 
  * @return
  * Boolean true or false. 
  * 
  * <br><B>Description</B><br>
  * This function returns a true if <tt>right</tt> is equal to <tt>self</tt>.
  * 
  */
    inline bool operator==( const DNBAttributeConstRevIterator<Key, Type>& right ) const
  		DNB_THROW_SPEC_NULL;

/**
  * Inequality operator.
  * @param  right 
  * Value to be compared against.
  * 
  * @return
  * Boolean true or false. 
  * 
  * <br><B>Description</B><br>
  * This function returns a true if <tt>right</tt> is not equal to <tt>self</tt>.
  * 
  */
    inline bool operator!=( const DNBAttributeConstRevIterator<Key, Type>& right ) const
  		DNB_THROW_SPEC_NULL;

/**
  * Dereferencing operator.
  * 
  * @return
  * Reference to an element type. 
  * 
  * <br><B>Description</B><br>
  * This function returns a reference to an element type.  
  * 
  */
    inline const_reference operator*() const
  		DNB_THROW_SPEC_NULL;

/**
  * Member access operator.
  * 
  * @return
  * Const pointer to the element type. 
  * 
  * <br><B>Description</B><br>
  * This function returns a const pointer to the element type.  
  * 
  * 
  */
    inline const_pointer operator->() const
  		DNB_THROW_SPEC_NULL;
/**
  * Returns key associated with an element type.
  * 
  * @return
  * Key associated with an element type.
  * 
  * <br><B>Description</B><br>
  * This function returns the key associated with an element type.
  * 
  */
    inline const Key& getKey() const
  		DNB_THROW_SPEC_NULL;

/**
  * Returns data associated with an element type.
  * 
  * @return
  * Data associated with an element type.
  * 
  * <br><B>Description</B><br>
  * This function returns the data associated with an element type.
  * 
  */
    inline const_reference getData() const
  		DNB_THROW_SPEC_NULL;

/**
  * Returns an associated key-value pair.
  * 
  * @return
  * Returns an associated pair.
  * 
  * <br><B>Description</B><br>
  * This function returns an associated key-value pair.
  * 
  */
    inline ValueType getPair( ) const
  		DNB_THROW_SPEC_NULL;


private:

    DNBSharedHandle<Type>	        hExt_; 
    ConstReverseIterator   		iter_;


};


//
// Inline function definitions.
//
#include "DNBAttributeConstRevIter.cc" 

#endif
