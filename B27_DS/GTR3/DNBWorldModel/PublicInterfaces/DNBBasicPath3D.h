//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//
//  Notes:
//    + DUPLICATION IS CURRENTLY A NO NO
//
//  TODO:
//      Need to get a way to duplicate paths and points?
//
//
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_BASICPATH3D_H_
#define _DNB_BASICPATH3D_H_


#include <DNBSystemBase.h>
#include <scl_vector.h>
#include <DNBSystemDefs.h>


#include <DNBWorldModel.h>


#include <DNBBasicEntity3D.h>
#include <DNBBasicTagPoint3D.h>


//
//  Provide the necessary forward declarations.
//
class DNBBasicBody3D;

//
//
//  Note:
//         It is possible to have single tag points in 
//         multiple paths. When inserting a tag point in
//         to a path, if the tag point is NOT a child of
//         any other 3D entity then that path will take 
//         that tag point to be its child. If there is a
//         parent for that tag point then the path will not
//         alter it.
//
//         This is important to remember! If a tag point
//         has a spatial parent that goes away, the tag point
//         in the path will keep that point around until the
//         point is removed from the path.
//
//         If you wish to avoid conflicts with this and DO NOT
//         NEED single points in multiple paths then never set
//         the spatial or logical parent of a tag point that 
//         is entered into a path.
//
class ExportedByDNBWorldModel DNBBasicPath3D : public DNBBasicEntity3D
{
    DNB_DECLARE_DYNAMIC_CREATE( DNBBasicPath3D );
    DNB_DECLARE_SHARED_OBJECT( DNBBasicPath3D );
    DNB_DECLARE_EXTENDIBLE_OBJECT( DNBBasicPath3D );
    DNB_DECLARE_EXTENDIBLE_FACTORY( DNBBasicPath3D );

public:
    DNBBasicPath3D()
        DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit, DNBENetworkError));

    DNBBasicPath3D( const DNBBasicPath3D &right, CopyMode mode )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit, DNBENetworkError));

    virtual
    ~DNBBasicPath3D( )
        DNB_THROW_SPEC_NULL;

    virtual DNBMessage
    getEntityTypeName( ) const
        DNB_THROW_SPEC_NULL;

    DNBUnsigned32
    getTagPointCount() const
        DNB_THROW_SPEC_NULL;

    //
    // NOTE: all indices are 0 based
    //
    DNBBasicTagPoint3D::Handle
    getTagPoint( const DNBUnsigned32& index) const
        DNB_THROW_SPEC_NULL;

    void
    getTagPoints( DNBBasicTagPoint3D::List& listTagPoints ) const
        DNB_THROW_SPEC_NULL;

    void
    getTagPoints( DNBBasicTagPoint3D::Vector& vectorTagPoints ) const
        DNB_THROW_SPEC_NULL;

    void
    setTagPoints( DNBBasicTagPoint3D::List& listTagPoints )
        DNB_THROW_SPEC_NULL;
    void
    setTagPoints( DNBBasicTagPoint3D::Vector& vectorTagPoints )
        DNB_THROW_SPEC_NULL;

    RequestStatus
    setTagPointRelativeLocation( DNBUnsigned32 index, DNBXform3D relXform )
        DNB_THROW_SPEC_NULL;

    void 
    createPoints( DNBUnsigned32 count, 
                  scl_wstring wstrNamePrefix, 
                  DNBUnsigned16 start  )
        DNB_THROW_SPEC((scl_bad_alloc));

    //
    // insert moves the point into the requested position
    // and moves current occupied point and the rest up 1 position 
    //
    // Note that duplicate points can exist in a path.
    //
    bool
    insertTagPoint( DNBUnsigned32 index, DNBBasicTagPoint3D::Handle hTagPoint )
        DNB_THROW_SPEC_NULL;

    //
    // addTagPoint adds passed tag point to the end ( push's to end of vector )
    //
    bool
    addTagPoint( DNBBasicTagPoint3D::Handle hTagPoint )
        DNB_THROW_SPEC_NULL;

    bool
    removeTagPoint( DNBBasicTagPoint3D::Handle hTagPoint )
        DNB_THROW_SPEC_NULL;

    bool
    removeTagPoint( DNBUnsigned32 index )
        DNB_THROW_SPEC_NULL;

    void 
    removeTagPoints( )
        DNB_THROW_SPEC_NULL;

    DNBInteger32 
    getIndex( DNBBasicTagPoint3D::Handle hTagPoint ) const
        DNB_THROW_SPEC_NULL;

    bool 
    swap( DNBUnsigned32 fromIndex, DNBUnsigned32 toIndex ) 
        DNB_THROW_SPEC_NULL;

    enum Events
    {
        EventFirst = DNBBasicEntity3D::EventLast - 1,
        EventPathLocationModified,
        EventPointsModified,
        EventLast
    };


protected:
    //
    //  The following methods are used to set/get the current profile of <self>
    //  with respect to the parent body.
    //
    virtual RequestStatus
    setRelativeLocation( DNBXform3D &relativeLocation )
        DNB_THROW_SPEC_NULL;

    virtual DNBXform3D
    getRelativeLocation( ) const
        DNB_THROW_SPEC_NULL;

    virtual RequestStatus
    setRelativeProfile( DNBProfile3D &relativeProfile )
        DNB_THROW_SPEC_NULL;

    virtual DNBProfile3D
    getRelativeProfile( ) const
        DNB_THROW_SPEC_NULL;

    void onPointsChange( const DNBBasicTagPoint3D::ConstPointer& pointer,
                         const EventSet& events )
        DNB_THROW_SPEC_NULL;

private:
    bool
    _addTagPoint( DNBBasicTagPoint3D::Handle hTagPoint )
        DNB_THROW_SPEC_NULL;


    DNBXform3D         pathLocation_;   // Pos. wrt base frame of spatial parent
    DNBBasicTagPoint3D::List   tagPointL_;

};




#endif  /* _DNB_BASICPATH3D_H_ */

