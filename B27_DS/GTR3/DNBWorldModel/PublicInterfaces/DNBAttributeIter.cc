//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//*
//* FILE:
//*     AttributeIter.cc           - principle implementation file
//*
//* MODULE:
//*     DNBAttributeIterator       - single non-template class
//*
//* OVERVIEW:
//*     This module defines an iterator preventing any usage outside 
//*     the scope of a transaction.
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     joy         02/05/99    Initial implementation
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1998, 99 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*
template < class Key, class Type >
DNBAttributeIterator<Key, Type>::DNBAttributeIterator(
				 DNBSharedHandle<Type>  hExt,
			         typename DNBAttributeIterator<Key, Type>::Iterator iter )
  				DNB_THROW_SPEC_NULL
                     		 : hExt_( hExt ), iter_( iter ) 
{
    DNB_PRECONDITION( hExt._getObject( )->isWriteLocked( ));
}


template < class Key, class Type >
DNBAttributeIterator<Key, Type>::~DNBAttributeIterator( )
  				DNB_THROW_SPEC_NULL
{
    // Nothing.
}

template < class Key, class Type >
inline
DNBAttributeIterator<Key, Type>
DNBAttributeIterator<Key, Type>::operator++( )
  				DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION( hExt_._getObject( )->isWriteLocked( ));
    ++iter_;
    return *this;

}


template < class Key, class Type >
inline
DNBAttributeIterator<Key, Type>
DNBAttributeIterator<Key, Type>::operator--( )
  				DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION( hExt_._getObject( )->isWriteLocked( ));
    --iter_;
    return *this;
     

}


template < class Key, class Type >
inline
DNBAttributeIterator<Key, Type>
DNBAttributeIterator<Key, Type>::operator++( int )
  				DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION( hExt_._getObject( )->isWriteLocked( ));
    DNBAttributeIterator<Key, Type> tmp = *this;
    ++iter_;
    return tmp;

}


template < class Key, class Type >
inline
DNBAttributeIterator<Key, Type>
DNBAttributeIterator<Key, Type>::operator--( int )
  				DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION( hExt_._getObject( )->isWriteLocked( ));
    DNBAttributeIterator<Key, Type> tmp = *this;
    --iter_;
    return tmp;
     

}


template < class Key, class Type >
inline
bool
DNBAttributeIterator<Key, Type>::operator==( 
				const DNBAttributeIterator<Key, Type>& right ) const
  				DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION( hExt_._getObject( )->isWriteLocked( ));
    return ( iter_ == right.iter_ );

}


template < class Key, class Type >
inline
bool
DNBAttributeIterator<Key, Type>::operator!=(
				 const DNBAttributeIterator<Key, Type>& right ) const
  				DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION( hExt_._getObject( )->isWriteLocked( ));
    return ( iter_ != right.iter_ );

}


template < class Key, class Type >
inline
typename
DNBAttributeIterator<Key, Type>::reference
DNBAttributeIterator<Key, Type>::operator*( ) const
  				DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION( hExt_._getObject( )->isWriteLocked( ));
    return *((*iter_).second );

}


template < class Key, class Type >
inline
typename
DNBAttributeIterator<Key, Type>::pointer
DNBAttributeIterator<Key, Type>::operator->() const
  				DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION( hExt_._getObject( )->isWriteLocked( ));
    return ((*iter_).second );

}


template < class Key, class Type >
inline
const Key&
DNBAttributeIterator<Key, Type>::getKey( ) const
  				DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION( hExt_._getObject( )->isWriteLocked( ));
    return (*iter_).first;

}


template < class Key, class Type >
inline
typename
DNBAttributeIterator<Key, Type>::const_reference
DNBAttributeIterator<Key, Type>::getData( ) const
  				DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION( hExt_._getObject( )->isWriteLocked( ));
    return *((*iter_).second );

}


template < class Key, class Type >
inline
typename
DNBAttributeIterator<Key, Type>::ValueType
DNBAttributeIterator<Key, Type>::getPair( ) const 
  				DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION( hExt_._getObject( )->isWriteLocked( ));
    return (*iter_);

}
