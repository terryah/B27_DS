//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//*
//* FILE:
//*     AttributeExt.cc       - principle implementation file
//*
//* MODULE:
//*     DNBAttributeExt       - single non-template class
//*
//* OVERVIEW:
//*     This module defines an attribute extension.
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     joy         02/05/99    Initial implementation
//*     xin         09/05/01    Removed DNBENotFound Execption from
//*                             getAttribute( const string& name, 
//*                                           DNBAttributeBase& data ) const
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1998, 99 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*

inline void
DNBAttributeExt::addAttribute( const scl_wstring& 		name, 
			       const DNBAttributeBase&  data )
 	DNB_THROW_SPEC((scl_bad_alloc, DNBEAlreadyRegistered))
{
   attrib_.addAttribute( name, data ); 
}


inline void
DNBAttributeExt::setAttribute( const scl_wstring& 		name, 
			       const DNBAttributeBase&  data )
 	DNB_THROW_SPEC((scl_bad_alloc))
{
   attrib_.setAttribute( name, data ); 
}

inline const DNBAttributeBase&
DNBAttributeExt::getAttribute( const scl_wstring& name ,
				DNBAttributeBase*attr,
				bool*wantException ) const 
	 DNB_THROW_SPEC((DNBENotFound))
{
    return attrib_.getAttribute( name ,attr,wantException);

}


/*
inline bool
DNBAttributeExt::getAttribute( const scl_wstring&    name, 
			       DNBAttributeBase& data ) const 
	 DNB_THROW_SPEC(( DNBEInvalidCast))
{
    return attrib_.getAttribute( name, data );

}
*/

inline void
DNBAttributeExt::removeAttribute( const scl_wstring& name )
        DNB_THROW_SPEC((DNBENotFound))
{
    attrib_.removeAttribute( name );

}


inline bool
DNBAttributeExt::isDefined( const scl_wstring& name ) const
        DNB_THROW_SPEC_NULL
{
    return ( attrib_.isDefined( name ) );
}


inline bool
DNBAttributeExt::isModifiable( const scl_wstring& name ) const
        DNB_THROW_SPEC((DNBENotFound))
{
    return ( attrib_.isModifiable( name ) );
}


inline size_t
DNBAttributeExt::size( ) const
        DNB_THROW_SPEC_NULL
{
    return ( attrib_.size( ) );
}


inline size_t
DNBAttributeExt::max_size( ) const
        DNB_THROW_SPEC_NULL
{
    return ( attrib_.max_size( ) );
}


inline DNBAttributeExt::ExtIterator
DNBAttributeExt::begin( )  
	DNB_THROW_SPEC_NULL
{
    ExtIterator iter( DNBAttributeExt::Handle( this ) , attrib_.begin( ) );
    return (iter);    
}



inline DNBAttributeExt::ExtIterator
DNBAttributeExt::end( )   
	DNB_THROW_SPEC_NULL
{
    ExtIterator iter( DNBAttributeExt::Handle( this ), attrib_.end( ) );
    return (iter);    
}



inline DNBAttributeExt::ExtConstIterator
DNBAttributeExt::begin( ) const 
	DNB_THROW_SPEC_NULL
{
    ExtConstIterator iter( DNBAttributeExt::Handle( 
			   DNB_CONST_CAST(  DNBAttributeExt*, this ) ), 
			   attrib_.begin( ));
    return (iter);    
}



inline DNBAttributeExt::ExtConstIterator
DNBAttributeExt::end( ) const  
	DNB_THROW_SPEC_NULL
{
    ExtConstIterator iter( DNBAttributeExt::Handle( 
			   DNB_CONST_CAST(  DNBAttributeExt*, this ) ), 
			   attrib_.end( ));
    return (iter);    
}


inline DNBAttributeExt::ExtReverseIterator
DNBAttributeExt::rbegin( )  
    DNB_THROW_SPEC_NULL
{
    ExtReverseIterator iter( DNBAttributeExt::Handle( this ), attrib_.rbegin( ) );
    return (iter);    
}


inline DNBAttributeExt::ExtReverseIterator
DNBAttributeExt::rend( )  
    DNB_THROW_SPEC_NULL
{
    ExtReverseIterator iter( DNBAttributeExt::Handle( this ), attrib_.rend( ) );
    return (iter);    
}


inline DNBAttributeExt::ExtConstReverseIterator
DNBAttributeExt::rbegin( ) const 
    DNB_THROW_SPEC_NULL
{
    ExtConstReverseIterator iter( DNBAttributeExt::Handle( 
			          DNB_CONST_CAST(  DNBAttributeExt*, this ) ), 
   				  attrib_.rbegin( ) );
    return (iter);    

}

inline DNBAttributeExt::ExtConstReverseIterator
DNBAttributeExt::rend( ) const 
    DNB_THROW_SPEC_NULL
{
    ExtConstReverseIterator iter( DNBAttributeExt::Handle( 
			          DNB_CONST_CAST(  DNBAttributeExt*, this ) ), 
				  attrib_.rend( ) );
    return (iter);    
}


inline void
DNBAttributeExt::clear( )
    DNB_THROW_SPEC_NULL
{
    attrib_.clear( );
}

inline bool
DNBAttributeExt::empty( ) const
    DNB_THROW_SPEC_NULL
{
    return (attrib_.empty( ));
}
