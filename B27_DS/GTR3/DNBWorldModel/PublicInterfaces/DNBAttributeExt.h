//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 */

//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     joy         02/05/99    Initial implementation
//*     xin         09/05/01    Removed DNBENotFound Execption from
//*                             getAttribute( const string& name, 
//*                                           DNBAttributeBase& data ) const
//*     bkh         11/06/03    Implementation of the new documentation style.
//*
//* 
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_ATTRIBUTEXT_H_
#define _DNB_ATTRIBUTEXT_H_



#include <DNBSystemBase.h>
#include <scl_string.h>
#include <DNBSystemDefs.h>


#include <DNBWorldModel.h>

#include <DNBException.h>
#include <DNBExtensionObject.h>
#include <DNBBasicEntity.h>
#include <DNBAttributeSet.h>
#include <DNBAttributeIter.h>
#include <DNBAttributeConstIter.h>
#include <DNBAttributeRevIter.h>
#include <DNBAttributeConstRevIter.h>




/**
  * An extension to an entity containing data of arbitrary type.   
  * 
  * <br><B>Description</B><br>
  * This class is an extension to DNBBasicEntity containing data of
  * arbitrary type ( auxiliary data as in IGRIP ).The data is represented 
  * by DNBAttributeBase which is the base class of DNBAttributeData, which 
  * contains the parameterized data. Each data element is associated with 
  * an unique scl_wstring key. Apart from standard data manipulation and 
  * access functions, the extension provides iterators to iterate through 
  * each data element.   
  * 
  * <br><B>Example</B><br>
  * <pre>
  * 
  * #include DNBSystemBase.h
  * #include DNBSystemDefs.h
  * #include DNBBasicPart3D.h
  * #include DNBAttribExt.h
  * #include DNBAttribData.h
  * 
  * void
  * DNBGetValue( const DNBAttributeBase& base );
  * 
  * int
  * main()
  * {
  * 
  * typedef DNBAttributeData<int>       intData;
  * typedef DNBAttributeData<float>     fltData;
  * typedef DNBAttributeData<scl_wstring>   strData;
  * const scl_wstring CountName = L"Count";
  * const scl_wstring CountNameNew = L"CountNew";
  * const scl_wstring PiName = L"Pi";
  * const scl_wstring MsgName = L"Area of circle";
  * const float Pi = 3.141596;
  * 
  * cout << "//////////////Begin testing////////////////" << endl;
  * DNBAttributeSet<scl_wstring>                    attribDB;
  * //
  * //  Create parts.
  * //
  * 
  * DNBBasicPart3D::Handle  hPart0 = DNBBasicPart3D::create( L"PUMA-560.0" );
  * DNBAttributeExt::Handle hAxd0 = DNBAttributeExt::create( hPart0 );
  * {
  * 
  * DNBAttributeExt::Pointer p0( hAxd0 );
  * 
  * p0->addAttribute( CountName, intData( 0,true ) );
  * p0->addAttribute( PiName, fltData( Pi, false ) );
  * p0->addAttribute( MsgName, strData(  L"Pi x radius^2", true) );
  * 
  * DNBAttributeData<float> fltData2( Pi, true );
  * 
  * fltData fltVal = p0->getAttribute( PiName );
  * cout << "Attribute name = " << PiName << ", Value = " <<
  * fltVal.getData() << endl;
  * intData intVal = p0->getAttribute( CountName );
  * cout << "Attribute name = " << CountName << ", Value = " <<
  * intVal.getData() << endl;
  * 
  * if( intVal.isModifiable() )
  * {
  * intVal.setData( 20 );
  * }
  * cout << "Attribute name = " << CountName << ", New value = " <<
  * intVal.getData() << endl;
  * 
  * try
  * {
  * fltVal = p0->getAttribute( MsgName );
  * }
  * catch( DNBEInvalidCast& error )
  * {
  * cerr << error.what() << endl;
  * 
  * const DNBAttributeBase& attr = p0->getAttribute( MsgName );
  * strData strVal;
  * if( strVal.isInstanceOf( attr ) )
  * {
  * strVal = p0->getAttribute( MsgName );
  * }
  * cout << "Attribute name = " << MsgName << ", Value = " <<
  * strVal.getData() << endl;
  * 
  * }
  * 
  * 
  * p0->addAttribute( CountNameNew, intData(0) );
  * p0->setAttribute( CountNameNew, intData( 36 ) );
  * intVal = p0->getAttribute( CountNameNew );
  * cout << "Attribute name = " << CountNameNew << ", Value = " <<
  * intVal.getData() << endl;
  * 
  * 
  * cout << "Testing const iterator" << endl;
  * DNBAttributeExt::ExtIterator iter = p0->begin();
  * DNBAttributeExt::ExtIterator last = p0->end();
  * 
  * for( ;iter != last; ++iter )
  * {
  * cout << "Attribute name = " << ((iter).getKey( )) << endl;
  * DNBGetValue( *iter );
  * }
  * 
  * }
  * 
  * cout << "////////////////End testing//////////////////" << endl;
  * return 0;
  * 
  * }
  * 
  * 
  * void
  * DNBGetValue( const DNBAttributeBase& base )
  * {
  * 
  * 
  * DNBAttributeData<int>               intData;
  * DNBAttributeData<float>             fltData;
  * DNBAttributeData<scl_wstring>           wstrData;
  * if( intData.isInstanceOf( base ) )
  * {
  * intData = base;
  * int iVal = intData.getData();
  * cout << "Value = " << iVal << endl;
  * 
  * }
  * else if( fltData.isInstanceOf( base ) )
  * {
  * fltData = base;
  * float fVal = fltData.getData();
  * cout << "Value = " << fVal << endl;
  * 
  * }
  * else if( wstrData.isInstanceOf( base ) )
  * {
  * wstrData = base;
  * scl_wstring wstr = wstrData.getData();
  * cout << "Value = " << wstr << endl;
  * 
  * }
  * 
  * }
  * 
  *
  * 
  * OUTPUT:
  * 
  * ////////////////Begin testing////////////////////
  * Attribute name = Pi, Value = 3.1416
  * Attribute name = Count, Value = 0
  * Attribute name = Count, New value = 20
  * Invalid cast 
  * Attribute name = Area of circle, Value = Pi x radius^2
  * Attribute name = CountNew, Value = 36
  * Testing const iterator
  * Attribute name = Area of circle
  * Value = Pi x radius^2
  * Attribute name = Count
  * Value = 0
  * Attribute name = CountNew
  * Value = 36
  * Attribute name = Pi
  * Value = 3.1416
  * /////////////////End testing////////////////////////
  * 
  * </pre>
  *
  */
class ExportedByDNBWorldModel DNBAttributeExt : public DNBExtensionObject
{
    DNB_DECLARE_DYNAMIC_CREATE( DNBAttributeExt );
    DNB_DECLARE_SHARED_OBJECT( DNBAttributeExt );
    DNB_DECLARE_EXTENSION_OBJECT( DNBAttributeExt, DNBBasicEntity );
    DNB_DECLARE_EXTENSION_FACTORY( DNBAttributeExt, DNBBasicEntity );

public:

/**
  * Specifies a collection of attribute data elements.  
  * 
  * <br><B>Description</B><br>
  * This type definition represents a collection of attribute data, 
  * each  associated with an unique scl_wstring key.    
  * 
  * 
  */
    typedef DNBAttributeSet<scl_wstring> 	        ExtSet; 


/**
  * An iterator to iterate through the attribute elements.
  * 
  * <br><B>Description</B><br>
  * This type definition represents an iterator that can be used to
  * iterate through the attribute elements.
  * 
  * 
  */
    typedef DNBAttributeIterator<scl_wstring, DNBAttributeExt> 	ExtIterator; 

/**
  * A const iterator to iterate through the attribute elements.
  * 
  * <br><B>Description</B><br>
  * This type definition represents a const iterator that can be used to
  * iterate through the attribute elements.
  * 
  * 
  */
    typedef DNBAttributeConstIterator<scl_wstring, DNBAttributeExt>  ExtConstIterator; 

/**
  * A reverse iterator to iterate through attribute elements.
  * 
  * <br><B>Description</B><br>
  * This type definition represents a reverse iterator that can be 
  * used to iterate through the attribute elements.
  * 
  * 
  */
    typedef DNBAttributeRevIterator<scl_wstring, DNBAttributeExt>  ExtReverseIterator; 

/**
  * A const reverse iterator to iterate through attribute elements.
  * 
  * <br><B>Description</B><br>
  * This type definition represents a const reverse  iterator that can 
  * be used to iterate through the attribute elements.
  * 
  * 
  */
    typedef DNBAttributeConstRevIterator<scl_wstring, DNBAttributeExt>  ExtConstReverseIterator; 


/**
  * This functions adds a new attribute element to <tt>self</tt>.
  * @param  name 
  * Name associated with attribute.
  * @param  data 
  * Attribute data.
  * 
  * @return
  * Nothing.
  * 
  * <br><B>Description</B><br>
  * This function adds a new attribute element with a <tt>name</tt> and 
  * associated <tt>data</tt> to <tt>self</tt>.
  * @exception scl_bad_alloc
  * The data element could not be inserted because of 
  * insufficient free memory.
  * @exception DNBEAlreadyREgistered
  * Attribute by the given name already exists.
  * 
  * 
  */
    inline void
    addAttribute( const scl_wstring& name, const DNBAttributeBase& data ) 
	  DNB_THROW_SPEC((scl_bad_alloc, DNBEAlreadyRegistered));

/**
  * This function sets the value of an attribute attribute in <tt>self</tt>.
  * @param  key 
  * Key associated with attribute.
  * @param  value 
  * Value of the attribute.
  * 
  * @return
  * Nothing.
  * 
  * <br><B>Description</B><br>
  * This function sets the value of an attribute attribute in <tt>self</tt>
  * by the given <tt>key</tt>. If an attribute by the given <tt>key</tt> is not 
  * found, an attribute with the given <tt>value</tt> and <tt>key</tt> is inserted.
  * @exception scl_bad_alloc
  * The data element could not be inserted because of 
  * insufficient free memory.
  * 
  * 
  */
    void
    setAttribute( const scl_wstring& 	    name, 
                  const DNBAttributeBase&   data )
        DNB_THROW_SPEC((scl_bad_alloc));


/**
  * Gets attribute base of given <tt>name</tt> from <tt>self</tt>.
  * @param  name 
  * Name of attribute.
  * 
  * @return
  * Attribute base.
  * 
  * <br><B>Description</B><br>
  * This function gets the attribute base associated with <tt>name</tt> 
  * from <tt>self</tt>.
  * @exception DNBENotFound
  * Attribute base by given <tt>name</tt> was not found.
  * 
  * 
  */
    inline const DNBAttributeBase& 
    getAttribute( const scl_wstring& name ,DNBAttributeBase*attr=NULL,bool*wantException=NULL ) const
	 DNB_THROW_SPEC((DNBENotFound));

/**
  * Gets an attribute data of given <tt>name</tt> from <tt>self</tt>.
  * @param  name 
  * Name of attribute.
  * @param  data 
  * Attribute data base.
  * 
  * @return
  * Nothing.
  * 
  * <br><B>Description</B><br>
  * This function gets the attribute data associated with <tt>name</tt> 
  * from <tt>self</tt>.
  * @exception DNBENotFound
  * Attribute by the given <tt>name</tt> was not found.
  * @exception DNBEInvalidCast
  * Data type of <tt>data</tt> does not correspond to data associated
  * with <tt>name</tt>.
  * 
  * 
  */
//    inline bool
//    getAttribute( const scl_wstring& name, DNBAttributeBase& data ) const
//	 DNB_THROW_SPEC(( DNBEInvalidCast));

/**
  * Removes an attibute from <tt>self</tt>.
  * @param  name 
  * Name of attribute to be removed.
  * 
  * @return
  * Nothing.
  * 
  * <br><B>Description</B><br>
  * This function removes an attribute of given <tt>name</tt> from <tt>self</tt>.
  * @exception DNBENotFound
  * Attribute by the given <tt>name</tt> was not found. 
  * 
  * 
  */
    inline void
    removeAttribute( const scl_wstring& name )
        DNB_THROW_SPEC((DNBENotFound));
 
/**
  * Determines whether an attribute by given <tt>name</tt> is present in
  * <tt>self</tt>.
  * @param  name 
  * Name of the attribute.
  * 
  * @return
  * Boolean true or false.
  * 
  * <br><B>Description</B><br>
  * This function returns a boolean true if an attribute by given 
  * <tt>name</tt> is present in <tt>self</tt>. Otherwise a boolean false is returned. 
  * 
  * 
  */
    inline bool
    isDefined( const scl_wstring& name ) const
        DNB_THROW_SPEC_NULL;

/**
  * Determines whether an attribute by given <tt>name</tt> is modifiable.  
  * @param  name 
  * Name of attribute.
  * 
  * @return
  * Boolean true or false.
  * 
  * <br><B>Description</B><br>
  * This function returns a boolean true if an attribute by given
  * <tt>name</tt> is modifiable. Otherwise a boolean false is returned. 
  * @exception DNBENotFound
  * Attribute by the given <tt>name</tt> was not found.
  * 
  * 
  */
    inline bool 
    isModifiable( const scl_wstring& name ) const
        DNB_THROW_SPEC((DNBENotFound));

/**
  * Gets the number of attribute elements.
  * 
  * @return
  * Number of attribute elements in <tt>self</tt>.
  * 
  * <br><B>Description</B><br>
  * This function gets the number of attribute elements
  * in <tt>self</tt>.
  * 
  * 
  */
    inline size_t
    size() const
        DNB_THROW_SPEC_NULL;

/**
  * Returns the maximum number of attribute elements that can 
  * be held in <tt>self</tt>. 
  * 
  * @return
  * Maximum number of attribute elements held in <tt>self</tt>.
  * 
  * <br><B>Description</B><br>
  * This function returns the maximum number of attribute elements
  * that can be held in <tt>self</tt>.
  * 
  * 
  */
    inline size_t
    max_size() const
        DNB_THROW_SPEC_NULL;

/**
  * Retrieves an iterator pointing to the first attribute element.
  * 
  * @return
  * An iterator positioned at the first attribute element. 
  * 
  * <br><B>Description</B><br>
  * This function retrieves an iterator positioned at the first 
  * attribute element.
  * 
  * 
  */
    inline ExtIterator 
    begin( )  
	DNB_THROW_SPEC_NULL;

/**
  * Retrieves an iterator pointing immmediately after the last 
  * attribute element.
  * 
  * @return
  * An iterator positioned immediately after the last attribute 
  * element. 
  * 
  * <br><B>Description</B><br>
  * This function retrieves an iterator positioned immediately after the
  * last attribute element.
  * 
  * 
  */
    inline ExtIterator 
    end( )  
	DNB_THROW_SPEC_NULL;

/**
  * Retrieves a const iterator pointing to the first attribute element.
  * 
  * @return
  * A const iterator positioned at the first attribute element.
  * 
  * <br><B>Description</B><br>
  * This function retrieves a const iterator positioned at the first
  * attribute element.
  * 
  * 
  */
    inline ExtConstIterator 
    begin( ) const 
	DNB_THROW_SPEC_NULL;

/**
  * Retrieves a const iterator pointing immediately after the last
  * attribute element.
  * 
  * @return
  * A const iterator positioned immediately after the last attribute 
  * element.
  * 
  * <br><B>Description</B><br>
  * This function retrieves a const iterator positioned immediately
  * after the last attribute element.
  * 
  * 
  */
    inline ExtConstIterator 
    end( ) const 
	DNB_THROW_SPEC_NULL;

/**
  * Retrieves a reverse iterator pointing to the last attribute 
  * element.
  * 
  * @return
  * A reverse iterator positioned at the last attribute element.
  * 
  * <br><B>Description</B><br>
  * This function retrieves a reverse iterator positioned at the 
  * last attribute element. 
  * 
  * 
  */
    inline ExtReverseIterator
    rbegin( ) 
        DNB_THROW_SPEC_NULL;

/**
  * Retrieves a reverse iterator pointing immediately before the first
  * attribute element.
  * 
  * @return
  * A reverse iterator positioned immediately before the first 
  * attribute element.  
  * 
  * <br><B>Description</B><br>
  * This function retrieves a reverse iterator positioned immediately
  * before the first attribute element.
  * 
  * 
  */
    inline ExtReverseIterator
    rend( ) 
        DNB_THROW_SPEC_NULL;

/**
  * Retrieves a const reverse iterator pointing to the last attribute 
  * element.
  * 
  * @return
  * A const reverse iterator positioned at the last attribute element. 
  * 
  * <br><B>Description</B><br>
  * This function retrieves a const reverse iterator positioned 
  * at the last attribute element. 
  * 
  * 
  */
    inline ExtConstReverseIterator
    rbegin( ) const
        DNB_THROW_SPEC_NULL;

/**
  * Retrieves a const reverse iterator pointing immediately before 
  * the first attribute element.
  * 
  * @return
  * A const reverse iterator positioned immediately before the first 
  * attribute element.  
  * 
  * <br><B>Description</B><br>
  * This function retrieves a const reverse iterator positioned 
  * immediately before the first attribute element.
  * 
  * 
  */
    inline ExtConstReverseIterator
    rend( ) const
        DNB_THROW_SPEC_NULL;

/**
  * Clears <tt>self</tt>. 
  * 
  * @return
  * 
  * <br><B>Description</B><br>
  * This functions clears <tt>self</tt>.
  * 
  * 
  */
    inline void
    clear( )
        DNB_THROW_SPEC_NULL;

/**
  * Returns whether <tt>self</tt> is empty.
  * 
  * @return
  * Boolean true or false.
  * 
  * <br><B>Description</B><br>
  * This function returns a boolean true if <tt>self</tt> is empty.
  * Otherwise a boolean false is returned. 
  * 
  * 
  */
    inline bool
    empty( ) const
        DNB_THROW_SPEC_NULL;

protected:

/**
  * Constructs an extension without any attribute element.
  * 
  * @return
  * Nothing.
  * 
  * <br><B>Description</B><br>
  * This function constructs an extension without any attribute
  * element.
  * @exception scl_bad_alloc
  * The extension could not be created because of 
  * insufficient free memory.
  * @exception DNBEResourceLimit
  * Insufficient system resources exist to complete the requested
  * service.
  * 
  * 
  * 
  */
    DNBAttributeExt()
        DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit)); 
    
/**
  * Special copy constructor.
  * @param  right 
  * Element to be copied.
  * @param  mode 
  * Deep or shallow copy mode.
  * 
  * @return
  * Nothing.
  * 
  * <br><B>Description</B><br>
  * This function constructs an object of type DNBAttributeExt.
  * @exception scl_bad_alloc
  * The extension could not be created because of 
  * insufficient free memory.
  * @exception DNBEResourceLimit
  * Insufficient system resources exist to complete the requested
  * service.
  * 
  * 
  * 
  */
    DNBAttributeExt( const DNBAttributeExt& right, CopyMode mode )
    	DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit)); 

/**
  * Destroys an object of type DNBAttributeExt.
  * 
  * @return
  * Nothing.
  * 
  * <br><B>Description</B><br>
  * This function destroys an object of type DNBAttributeExt.
  * 
  * 
  */
    virtual
    ~DNBAttributeExt( )
	DNB_THROW_SPEC_NULL;

private:
    ExtSet				attrib_;
};


//
// Inline function definitions.
//
#include "DNBAttributeExt.cc" 

#endif     /* _DNB_ATTRIBUTEXT_H_ */
