//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//
//  Notes:
// mmg  08/29/03    make isAttached a const method
// mmh  11/30/05    add hV5AttachBody_ to store father for persistent V5 attachments 
// rtl  05/03/06    IR A0535315 fix. Provide static grabBody /detachBody methods
//                  wherein we lock the ancestors first before locking the grabee
//
//  TODO:
//    + Add the following notifications:
//        o NotifFrameAdded             (specify frame)
//        o NotifFrameRemoved           (specify frame)
//    +	Save hJoint if any ?	     
//
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_BASICBODY3D_H_
#define _DNB_BASICBODY3D_H_


#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>


#include <DNBWorldModel.h>
#include <DNBNotificationBase.h>


#include <DNBBasicEntity3D.h>
#include <DNBBasicFrame3D.h>
#include <DNBBasicTagPoint3D.h>
#include <DNBBasicPath3D.h>


//
//  Provide the necessary forward declarations.
//
class DNBBasicConnection3D;
struct DNBNotifyBasicBody3DList;


//
//  This class represents an abstract body with extent.
//
class ExportedByDNBWorldModel DNBBasicBody3D : public DNBBasicEntity3D
{
    DNB_DECLARE_DYNAMIC_CREATE( DNBBasicBody3D );

    DNB_DECLARE_SHARED_OBJECT( DNBBasicBody3D );

    DNB_DECLARE_EXTENDIBLE_OBJECT( DNBBasicBody3D );

public:
    typedef DNBSharedHandle<DNBBasicConnection3D>   Connection3DHandle;

    virtual DNBMessage
    getEntityTypeName( ) const
        DNB_THROW_SPEC_NULL;

    //
    //  The following methods manage the set of attached frames.
    //
    void
    addFrame( DNBBasicFrame3D::Handle hFrame )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEAlreadyRegistered));

    void
    removeFrame( DNBBasicFrame3D::Handle hFrame )
        DNB_THROW_SPEC((DNBENotFound));

    void
    removeFrames( )
        DNB_THROW_SPEC_NULL;

    void
    destroyFrame( DNBBasicFrame3D::Handle hFrame )
        DNB_THROW_SPEC((DNBENotFound));

    void
    destroyFrames( )
        DNB_THROW_SPEC_NULL;

    bool
    hasFrame( DNBBasicFrame3D::Handle hFrame ) const
        DNB_THROW_SPEC_NULL;

    void
    getFrames( DNBBasicFrame3D::Vector &gFrames ) const
        DNB_THROW_SPEC((scl_bad_alloc));

    void
    getFrames( DNBBasicFrame3D::List   &gFrames ) const
        DNB_THROW_SPEC((scl_bad_alloc));

    size_t
    getFrameCount( ) const
        DNB_THROW_SPEC_NULL;

    //
    //  The following methods are used to manage the set of attached tag points.
    //
    void
    addTagPoint( DNBBasicTagPoint3D::Handle hTagPoint )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEAlreadyRegistered));

    void
    removeTagPoint( DNBBasicTagPoint3D::Handle hTagPoint )
        DNB_THROW_SPEC((DNBENotFound));

    void
    removeTagPoints( )
        DNB_THROW_SPEC_NULL;

    void
    destroyPoint( DNBBasicTagPoint3D::Handle hPoint )
        DNB_THROW_SPEC((DNBENotFound));

    void
    destroyPoints( )
        DNB_THROW_SPEC_NULL;

    bool
    hasPoint( DNBBasicTagPoint3D::Handle hPoint ) const
        DNB_THROW_SPEC_NULL;

    void
    getTagPoints( DNBBasicTagPoint3D::Vector &gTagPoints ) const
        DNB_THROW_SPEC((scl_bad_alloc));

    void
    getTagPoints( DNBBasicTagPoint3D::List   &gTagPoints ) const
        DNB_THROW_SPEC((scl_bad_alloc));

    size_t
    getTagPointCount( ) const
        DNB_THROW_SPEC_NULL;

    //
    //  The following methods are used to manage the set of attached paths.
    //
    void
    addPath( DNBBasicPath3D::Handle hPath )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEAlreadyRegistered));

    void
    removePath( DNBBasicPath3D::Handle hPath )
        DNB_THROW_SPEC((DNBENotFound));

    void
    removePaths( )
        DNB_THROW_SPEC_NULL;

    void
    destroyPath( DNBBasicPath3D::Handle hPath )
        DNB_THROW_SPEC((DNBENotFound));

    void
    destroyPaths( )
        DNB_THROW_SPEC_NULL;

    bool
    hasPath( DNBBasicPath3D::Handle hPath ) const
        DNB_THROW_SPEC_NULL;

    void
    getPaths( DNBBasicPath3D::Vector &gPaths ) const
        DNB_THROW_SPEC((scl_bad_alloc));

    void
    getPaths( DNBBasicPath3D::List   &gPaths ) const
        DNB_THROW_SPEC((scl_bad_alloc));

    size_t
    getPathCount( ) const
        DNB_THROW_SPEC_NULL;

    void
    attachBody( DNBBasicBody3D::Handle hBody )
 	DNB_THROW_SPEC((scl_bad_alloc, DNBEAlreadyRegistered, DNBEInvalidRelation));

    void
    attachBody( DNBBasicBody3D::Handle hBody,
		DNBBasicFrame3D::Handle hFrame )
 	DNB_THROW_SPEC((scl_bad_alloc, DNBEAlreadyRegistered, DNBEInvalidRelation));

    void
    attachBody( DNBBasicBody3D::Handle hBody, DNBXform3D &offset )
 	DNB_THROW_SPEC((scl_bad_alloc, DNBEAlreadyRegistered, DNBEInvalidRelation));

    void 
    grabBody( DNBBasicBody3D::Handle hBody, bool transient = false ) 
 	DNB_THROW_SPEC((scl_bad_alloc, DNBEAlreadyRegistered, DNBEInvalidRelation));

    void
    detachBody( )
 	DNB_THROW_SPEC((scl_bad_alloc, DNBEAlreadyRegistered, DNBEInvalidRelation));

    bool
    isAttached() const
	DNB_THROW_SPEC_NULL;

    //
    //  The following methods retrieve information about the set of adjacent
    //  bodies.
    //
    bool
    hasInnerBody( ) const
        DNB_THROW_SPEC_NULL;

    DNBBasicBody3D::Handle
    getInnerBody( ) const
        DNB_THROW_SPEC_NULL;

    DNBBasicBody3D::Handle
    getInnerBodyOriginal() const
	DNB_THROW_SPEC_NULL;

    bool
    hasInnerConnection( ) const
        DNB_THROW_SPEC_NULL;

    Connection3DHandle
    getInnerConnection( ) const
        DNB_THROW_SPEC_NULL;

    bool
    hasOuterBody( DNBBasicBody3D::Handle hBody ) const
        DNB_THROW_SPEC_NULL;

    void
    getOuterBodies( DNBBasicBody3D::Vector &gBodies ) const
        DNB_THROW_SPEC((scl_bad_alloc));

    void
    getOuterBodies( DNBBasicBody3D::List   &gBodies ) const
        DNB_THROW_SPEC((scl_bad_alloc));

    size_t
    getOuterBodyCount( ) const
        DNB_THROW_SPEC_NULL;

    void
    setInnerBody( const DNBBasicBody3D::Handle &hBody,
        const Connection3DHandle &hConnection )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEAlreadyRegistered));

    bool
    getTransientState( ) const
        DNB_THROW_SPEC_NULL;

    void
    setTransientState( bool iState = true )
        DNB_THROW_SPEC_NULL;

    void
    grab_list_cb( const DNBNotificationAgent::ConstPointer &, 
                  const DNBNotificationBase *)
        DNB_THROW_SPEC_NULL;

    enum Events
    {
        EventFirst = DNBBasicEntity3D::EventLast - 1,
        EventFramesRemoved,
        EventTagPointsRemoved,
        EventPathsRemoved,
        EventInnerBodyModified,
        EventInnerConnectionModified,
        EventBodyAttached,
        EventBodyDetached,
        EventAttachedBody,
        EventDetachedBody,
        EventLast
    };

    void 
    setV5AttachBody( DNBBasicBody3D::Handle hBody )
        DNB_THROW_SPEC_NULL;

    DNBBasicBody3D::Handle
    getV5AttachBody() const
        DNB_THROW_SPEC_NULL;

	static bool
	grabBody( DNBBasicBody3D::Handle hGrabbingObj,
			  DNBBasicBody3D::Handle hGrabbedObj )
	DNB_THROW_SPEC_NULL;

	static bool
	detachBody( DNBBasicBody3D::Handle hGrabbedObj,
				DNBBasicBody3D::Handle hGrabbingObj = NULL )
	DNB_THROW_SPEC_NULL;


protected:
    //
    //  By defining the following methods in the protected section, this class
    //  can only be used as a base class.
    //
    DNBBasicBody3D( )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit, DNBENetworkError));

    DNBBasicBody3D( const DNBBasicBody3D &right, CopyMode mode )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit, DNBENetworkError));

    virtual
    ~DNBBasicBody3D( )
        DNB_THROW_SPEC_NULL;


    //
    //  The following methods are used to set/get the current profile of <self>
    //  with respect to the parent body.
    //
    virtual RequestStatus
    setRelativeLocation( DNBXform3D &relativeLocation )
        DNB_THROW_SPEC_NULL;

    virtual DNBXform3D
    getRelativeLocation( ) const
        DNB_THROW_SPEC_NULL;

    virtual RequestStatus
    setRelativeProfile( DNBProfile3D &relativeProfile )
        DNB_THROW_SPEC_NULL;

    virtual DNBProfile3D
    getRelativeProfile( ) const
        DNB_THROW_SPEC_NULL;

    virtual DNBDynamicObject *
    duplicateObject( CopyMode mode ) const
        DNB_THROW_SPEC((scl_bad_alloc, DNBException));

private:
    void
    cloneFrames( const DNBBasicBody3D &right, CopyMode mode )
        DNB_THROW_SPEC((scl_bad_alloc, DNBException));

    void
    cloneTagPoints( const DNBBasicBody3D &right, CopyMode mode )
        DNB_THROW_SPEC((scl_bad_alloc, DNBException));

    void
    clonePaths( const DNBBasicBody3D &right, CopyMode mode )
        DNB_THROW_SPEC((scl_bad_alloc, DNBException));

    void
    clearInnerBody( )
        DNB_THROW_SPEC_NULL;

    void
    clearOuterBody( )
        DNB_THROW_SPEC_NULL;

    void
    setAttachment( Connection3DHandle hAttachment )
	DNB_THROW_SPEC_NULL;

    Connection3DHandle  
    getAttachment( ) const
	DNB_THROW_SPEC_NULL;

    void 
    setInnerBodyOriginal( DNBBasicBody3D::Handle hBody )
	DNB_THROW_SPEC_NULL;

    void
    attachBodies( DNBBasicBody3D::Handle hBody ) 
        DNB_THROW_SPEC((scl_bad_alloc, DNBEAlreadyRegistered, DNBEInvalidRelation));

    Connection3DHandle      		hInnerConnection_;
    Connection3DHandle      		hAttachment_;
    DNBBasicBody3D::Handle		hInnerBodyOriginal_;
    DNBBasicBody3D::Handle      hV5AttachBody_;

    bool                        isTransient_;
    //
    //  The following declaration allows the class DNBBasicConnection3D to call
    //  the methods setInnerConnection() and clearInnerConnection().
    //
    friend class DNBBasicConnection3D;

};



// #include <DNBBasicConnection3D.h> ; smw ; caa

class ExportedByDNBWorldModel DNBNotifyBasicBody3DList : public DNBNotificationBase
{
    DNB_DECLARE_DYNAMIC_CREATE( DNBNotifyBasicBody3DList );

public:
    DNBNotifyBasicBody3DList()
        DNB_THROW_SPEC_NULL;

    DNBNotifyBasicBody3DList( const DNBNotifyBasicBody3DList& right,
                                CopyMode mode )
        DNB_THROW_SPEC_NULL;

    virtual
    ~DNBNotifyBasicBody3DList()
        DNB_THROW_SPEC_NULL;

    DNBBasicBody3D::List data_;
};




#endif  /* _DNB_BASICBODY3D_H_ */
