//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//*
//* TODO:
//*     o Review addBody() where existence of innerBody is being checked  
//*	  This remains in the existing code, but after forward kinematics
//*	  has been implemented this need to be revisited.
//*     o Test addBody with frame/offset
//*     o Test removeBody, etc.
//*
//* COPYRIGHT:
//*     Copyright (C) 1998, 99 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*

#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_BASICMFGASSEMBLY3D_H_
#define _DNB_BASICMFGASSEMBLY3D_H_


#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>


#include <DNBWorldModel.h>


#include <DNBBasicWorld.h>
#include <DNBBasicBody3D.h>
#include <DNBBasicPart3D.h>



class ExportedByDNBWorldModel DNBBasicMfgAssembly3D : public DNBBasicBody3D
{
    DNB_DECLARE_DYNAMIC_CREATE( DNBBasicMfgAssembly3D );

    DNB_DECLARE_SHARED_OBJECT( DNBBasicMfgAssembly3D );

    DNB_DECLARE_EXTENDIBLE_OBJECT( DNBBasicMfgAssembly3D );

    DNB_DECLARE_EXTENDIBLE_FACTORY( DNBBasicMfgAssembly3D );

public:

    static DNBBasicMfgAssembly3D::Handle
    DNBGetRootAssembly( DNBBasicWorld::Handle hWorld )
        DNB_THROW_SPEC_NULL;

    virtual DNBMessage
    getEntityTypeName( ) const
        DNB_THROW_SPEC_NULL;

    void
    addBody( DNBBasicBody3D::Handle hBody )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEInvalidRelation));


    void
    addBody( DNBBasicBody3D::Handle hBody,
                                const DNBXform3D& innerOffset,
                                const DNBXform3D& outerOffset )
    	DNB_THROW_SPEC((scl_bad_alloc, DNBEInvalidRelation));

    void
    addBody( DNBBasicBody3D::Handle hBody,
                                DNBBasicFrame3D::Handle hInnerFrame,
                                DNBBasicFrame3D::Handle hOuterFrame )
    	DNB_THROW_SPEC((scl_bad_alloc, DNBEInvalidRelation));

    size_t
    getBodyCount( ) const
	DNB_THROW_SPEC_NULL;	


    DNBBasicBody3D::Handle 
    getBody( const scl_wstring& name ) const
        DNB_THROW_SPEC_NULL;


    void 
    getBodies( DNBBasicBody3D::Vector& gParts ) const
        DNB_THROW_SPEC((scl_bad_alloc));

    void 
    getBodies( DNBBasicBody3D::List& gParts ) const
        DNB_THROW_SPEC((scl_bad_alloc));


#if 0
    void
    getConstraints( DNBBasicJoint3D::Vector& gConstraint ) const
	DNB_THROW_SPEC((scl_bad_alloc));
		    
    void
    getConstraints( DNBBasicJoint3D::List& gConstraint ) const
	DNB_THROW_SPEC((scl_bad_alloc));
#endif

    DNBBasicMfgAssembly3D::Handle
    getParentAssembly( ) const
        DNB_THROW_SPEC_NULL;

    DNBBasicMfgAssembly3D::Handle
    getRootAssembly( ) const 
	DNB_THROW_SPEC_NULL;

    RequestStatus
    setAssemblyLocation( DNBXform3D &relativeLocation )
        DNB_THROW_SPEC_NULL;

    DNBXform3D
    getAssemblyLocation( ) const
        DNB_THROW_SPEC_NULL;

    void
    replaceBody( DNBBasicBody3D::Handle hOldChild,
        DNBBasicBody3D::Handle hNewChild )
        DNB_THROW_SPEC((scl_bad_alloc, DNBENotFound, DNBEAlreadyRegistered, DNBEInvalidRelation));


    void
    destroyAll( )
	DNB_THROW_SPEC_NULL;


    void
    destroyBody( const scl_wstring& name )
        DNB_THROW_SPEC((DNBENotFound));

    void
    destroyBody( DNBBasicBody3D::Handle hBody )
        DNB_THROW_SPEC((DNBENotFound));


    void
    removeAll( )
        DNB_THROW_SPEC_NULL;

    void
    removeBody( const scl_wstring& name )
        DNB_THROW_SPEC((DNBENotFound));

    void
    removeBody( DNBBasicBody3D::Handle hBody )
        DNB_THROW_SPEC((DNBENotFound));


#if 1
    void
    dump( ostream& oStr ) const
	DNB_THROW_SPEC_NULL;
#endif

    enum Events
    {
        EventFirst = DNBBasicBody3D::EventLast - 1,
        EventWorkcellLocationModified,
        EventLast
    };

protected:
    //
    //  By defining the following methods in the protected section, application
    //  programmers must use the static factory methods to create and destroy
    //  instances of this class.
    //
    DNBBasicMfgAssembly3D( )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit, DNBENetworkError));

    DNBBasicMfgAssembly3D( const DNBBasicMfgAssembly3D &right, CopyMode mode )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit, DNBENetworkError));

    virtual
    ~DNBBasicMfgAssembly3D( )
        DNB_THROW_SPEC_NULL;

    virtual DNBDynamicObject*
    duplicateObject( CopyMode mode ) const
        DNB_THROW_SPEC((scl_bad_alloc, DNBException));

private :
    
    typedef DNBBasicEntity::Vector::const_iterator    ChildEntityConstIterator;
    typedef DNBBasicEntity::Vector::iterator    ChildEntityIterator;
    typedef DNBBasicEntity3D::Vector::const_iterator    ChildEntity3DConstIterator;
    
    void
    cloneAssemblies( const DNBBasicMfgAssembly3D &right, CopyMode mode )
        DNB_THROW_SPEC((scl_bad_alloc, DNBException));

    void
    cloneParts( const DNBBasicMfgAssembly3D &right, CopyMode mode )
        DNB_THROW_SPEC((scl_bad_alloc, DNBException));

    ChildEntityConstIterator 
    findChildEntity(  const DNBBasicEntity::Vector& gChildren,
                      const scl_wstring& name ) const
        DNB_THROW_SPEC_NULL;

    void
    destroyAttachments( const DNBClassInfo& classInfo )              
	DNB_THROW_SPEC_NULL;

    void
    destroyAttachments( )              
	DNB_THROW_SPEC_NULL;

};



#endif  /* _DNB_BASICMFGASSEMBLY3D_H_ */
