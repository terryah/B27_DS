//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
#if DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _DNBBASICCAMERA3D_H_
#define _DNBBASICCAMERA3D_H_


#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>

#include <DNBBasicBody3D.h>
#include <DNBBasicRelationship3D.h>
#include <DNBException.h>


class DNBBasicTracking3D;


class ExportedByDNBWorldModel DNBBasicCamera3D : public DNBBasicBody3D
{
    DNB_DECLARE_DYNAMIC_CREATE( DNBBasicCamera3D );

    DNB_DECLARE_SHARED_OBJECT( DNBBasicCamera3D );

    DNB_DECLARE_EXTENDIBLE_OBJECT( DNBBasicCamera3D );

    DNB_DECLARE_EXTENDIBLE_FACTORY( DNBBasicCamera3D );

public:
    virtual DNBMessage
    getEntityTypeName( ) const
        DNB_THROW_SPEC_NULL;

    bool
    hasRelationship( ) const
        DNB_THROW_SPEC_NULL;

    DNBBasicRelationship3D::Handle
    getRelationship( ) const
        DNB_THROW_SPEC_NULL;

protected:
    DNBBasicCamera3D( )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit, DNBENetworkError));

    DNBBasicCamera3D( const DNBBasicCamera3D &other, CopyMode mode )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit, DNBENetworkError));

    ~DNBBasicCamera3D( )
        DNB_THROW_SPEC_NULL;

private:
    void
    setRelationship( DNBBasicRelationship3D::Handle hRelation )
        DNB_THROW_SPEC((DNBEAlreadyRegistered));

    void
    clearRelationship( )
        DNB_THROW_SPEC_NULL;

    void
    onNotification( )
        DNB_THROW_SPEC((scl_bad_alloc, DNBException));

    DNBBasicRelationship3D::Handle hRelation_;

    friend class DNBBasicTracking3D;
};


#endif /* _DNBBASICCAMERA3D_H_ */
