//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2010
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//
//  Notes:
//    + None
//
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_BASICDATACACHE_H_
#define _DNB_BASICDATACACHE_H_


#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>


#include <DNBWorldModel.h>


#include <DNBBasicEntity.h>
#include <DNBXform3D.h>


//
//  This class represents an arbitrary data cache
//

class ExportedByDNBWorldModel DNBBasicDataCache : public DNBBasicEntity
{
    DNB_DECLARE_DYNAMIC_CREATE( DNBBasicDataCache );

    DNB_DECLARE_SHARED_OBJECT( DNBBasicDataCache );

    DNB_DECLARE_EXTENDIBLE_OBJECT( DNBBasicDataCache );

    DNB_DECLARE_EXTENDIBLE_FACTORY( DNBBasicDataCache );
public:

/**
  * Returns a cached base location.
  */
    const DNBXform3D &
    getCachedBaseLocation( ) const
        DNB_THROW_SPEC_NULL;

/**
  * Set a cached base location.
  */
    void
    setCachedBaseLocation( const DNBXform3D &baseLocation )
        DNB_THROW_SPEC_NULL;

protected:
    //
    //  By defining the following methods in the protected section, application
    //  programmers must use the static factory methods to create and destroy
    //  instances of this class.
    //

/**
  * Constructor.
  * @exception @href scl_bad_alloc if the memory is insufficient for memory allocation.
  * @exception <b>DNBEResourceLimit</b> if the resources are limited. See @href DNBERuntimeError .
  * @exception <b>DNBENetworkError</b> if there is a network error. See @href DNBERuntimeError .
  */
    DNBBasicDataCache( )
        /**@SOM*/DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit, DNBENetworkError))/**@EOM*/;

/**
  * Constructor.
  * @param right
  * Object to copy.
  * @param mode
  * Mode of copy.
  * @exception @href scl_bad_alloc if the memory is insufficient for memory allocation.
  * @exception <b>DNBEResourceLimit</b> if the resources are limited. See @href DNBERuntimeError .
  * @exception <b>DNBENetworkError</b> if there is a network error. See @href DNBERuntimeError .
  */
    DNBBasicDataCache( const DNBBasicDataCache &right, CopyMode mode )
        /**@SOM*/DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit, DNBENetworkError))/**@EOM*/;

/**
  * Destructor for DNBBasicDataCache.
  */
    virtual
    ~DNBBasicDataCache( )
        DNB_THROW_SPEC_NULL;

private:
    DNBXform3D      baseXform_; 
};




#endif  /* _DNB_BASICDATACACHE_H_ */
