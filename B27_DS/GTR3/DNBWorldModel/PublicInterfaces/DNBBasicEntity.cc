//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
inline const DNBUUID &
DNBBasicEntity::getEntityID( ) const
    DNB_THROW_SPEC_NULL
{
    return entityID_;
}


inline void
DNBBasicEntity::setDescription( const scl_wstring &description )
    DNB_THROW_SPEC((scl_bad_alloc))
{
    description_ = description;
    postEvent( EventDescriptionModified );
}


inline const scl_wstring &
DNBBasicEntity::getDescription( ) const
    DNB_THROW_SPEC_NULL
{
    return description_;
}


inline void
DNBBasicEntity::setHistoryStatus( bool status )
    DNB_THROW_SPEC_NULL
{
    historyStatus_ = status;
    postEvent( EventHistoryStatusModified );
}


inline bool
DNBBasicEntity::getHistoryStatus( ) const
    DNB_THROW_SPEC_NULL
{
    return historyStatus_;
}


inline void
DNBBasicEntity::setEntityID( const DNBUUID &entityID )
    DNB_THROW_SPEC_NULL
{
    entityID_ = entityID;
    postEvent( EventEntityIDModified );
}
