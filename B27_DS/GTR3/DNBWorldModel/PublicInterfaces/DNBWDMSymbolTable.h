//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/

#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_WDMSYMBOLTABLE_H_
#define _DNB_WDMSYMBOLTABLE_H_


#include <DNBSystemBase.h>
#include <scl_strstream.h>
#include <scl_map.h>
#include <DNBSystemDefs.h>

#include <DNBWorldModel.h>

#include <DNBBasicEntity.h>
#include <DNBSymbolTable.h>

// Added 09/15/2000 by bpl
#define DNB_CREATE_TEMPORARY_SYMBOLS

#ifdef DNB_CREATE_TEMPORARY_SYMBOLS
#include <DNBRealData.h>
#endif
/*
Change made 9/15/2000 by bpl
  The symbol table can now create symbols that it can not find.  Symbols 
are stored at the local level in the enitiy that the table has a handle 
to.  Symbols are created with the full name passed in (it is assumed that 
the name will not have a path attached to it).  This change only effects 
DNBReal symbols.
  This change was done to allow IGCALC expressions that use temporary 
symbols to be loaded in.  It is temporary untill the symbols stuff is 
redesigned.
*/


class ExportedByDNBWorldModel DNBWDMSymbolTable : public DNBSymbolTable
{

  public:
    DNBWDMSymbolTable()
        DNB_THROW_SPEC_NULL;

    DNBWDMSymbolTable( const DNBBasicEntity::Handle& hEntity )
        DNB_THROW_SPEC_NULL;

    ~DNBWDMSymbolTable()
        DNB_THROW_SPEC_NULL;


    virtual
    DNBboolSymbol
    getboolSymbol(  const scl_wstring& strPath,
                    const wchar_t  delimiter = L'/',
                    const wchar_t  attrDelim = L':' )
        DNB_THROW_SPEC_NULL;

    virtual
    DNBcharSymbol
    getcharSymbol( const scl_wstring& strPath,
                   const wchar_t  delimiter = L'/',
                   const wchar_t  attrDelim = L':' )
        DNB_THROW_SPEC_NULL;

    virtual
    DNBwchar_tSymbol
    getwhar_tSymbol( const scl_wstring& strPath,
                     const wchar_t  delimiter = L'/',
                     const wchar_t  attrDelim = L':' )
        DNB_THROW_SPEC_NULL;

    virtual
    DNBstringSymbol
    getstringSymbol( const scl_wstring& strPath,
                     const wchar_t  delimiter = L'/',
                     const wchar_t  attrDelim = L':' )
        DNB_THROW_SPEC_NULL;

    virtual
    DNBwstringSymbol
    getwstringSymbol( const scl_wstring& strPath,
                      const wchar_t  delimiter = L'/',
                      const wchar_t  attrDelim = L':' )
        DNB_THROW_SPEC_NULL;

    virtual
    DNBInteger32Symbol
    getInteger32Symbol( const scl_wstring& strPath,
                        const wchar_t  delimiter = L'/',
                        const wchar_t  attrDelim = L':' )
       DNB_THROW_SPEC_NULL;

    virtual
    DNBInt32VecSymbol
    getInt32VectSymbol( const scl_wstring& strPath,
                        const wchar_t  delimiter = L'/',
                        const wchar_t  attrDelim = L':' )
        DNB_THROW_SPEC_NULL;

    virtual
    DNBInteger64Symbol
    getInteger64Symbol( const scl_wstring& strPath,
                        const wchar_t  delimiter = L'/',
                        const wchar_t  attrDelim = L':' )
        DNB_THROW_SPEC_NULL;

    virtual
    DNBInt64VecSymbol
    getInt64VectSymbol( const scl_wstring& strPath,
                        const wchar_t  delimiter = L'/',
                        const wchar_t  attrDelim = L':' )
        DNB_THROW_SPEC_NULL;

    virtual
    DNBRealSymbol
    getRealSymbol( const scl_wstring& strPath,
                   const wchar_t  delimiter = L'/',
                   const wchar_t  attrDelim = L':' )
        DNB_THROW_SPEC_NULL;

    virtual
    DNBRealVecSymbol
    getRealVectSymbol( const scl_wstring& strPath,
                       const wchar_t  delimiter = L'/',
                       const wchar_t  attrDelim = L':' )
        DNB_THROW_SPEC_NULL;

    virtual
    DNBVec3DSymbol
    getVect3DSymbol( const scl_wstring& strPath,
                     const wchar_t  delimiter = L'/',
                     const wchar_t  attrDelim = L':' )
      DNB_THROW_SPEC_NULL;

    virtual
    DNBEuler3DSymbol
    getEulerSymbol( const scl_wstring& strPath,
                    const wchar_t  delimiter = L'/',
                    const wchar_t  attrDelim = L':' )
    DNB_THROW_SPEC_NULL;

#if DNB_HAS_BOOL
    virtual
    void
    getSymbol( DNBboolSymbol& symbol,
               const scl_wstring& strPath,
               const wchar_t  delimiter = L'/',
               const wchar_t  attrDelim = L':' )
    DNB_THROW_SPEC_NULL;
#endif // DNB_NO_BOOL


    virtual
    void
    getSymbol( DNBcharSymbol& symbol,
               const scl_wstring& strPath,
               const wchar_t  delimiter = L'/',
               const wchar_t  attrDelim = L':' )
    DNB_THROW_SPEC_NULL;


    virtual
    void
    getSymbol( DNBwchar_tSymbol& symbol,
               const scl_wstring& strPath,
               const wchar_t  delimiter = L'/',
               const wchar_t  attrDelim = L':' )
    DNB_THROW_SPEC_NULL;


    virtual
    void
    getSymbol( DNBstringSymbol& symbol,
               const scl_wstring& strPath,
               const wchar_t  delimiter = L'/',
               const wchar_t  attrDelim = L':' )
    DNB_THROW_SPEC_NULL;


    virtual
    void
    getSymbol( DNBwstringSymbol& symbol,
               const scl_wstring& strPath,
               const wchar_t  delimiter = L'/',
               const wchar_t  attrDelim = L':' )
    DNB_THROW_SPEC_NULL;


    virtual
    void
    getSymbol( DNBInteger32Symbol& symbol,
               const scl_wstring& strPath,
               const wchar_t  delimiter = L'/',
               const wchar_t  attrDelim = L':' )
    DNB_THROW_SPEC_NULL;


    virtual
    void
    getSymbol( DNBInt32VecSymbol& symbol,
               const scl_wstring& strPath,
               const wchar_t  delimiter = L'/',
               const wchar_t  attrDelim = L':' )
    DNB_THROW_SPEC_NULL;


    virtual
    void
    getSymbol( DNBInteger64Symbol& symbol,
               const scl_wstring& strPath,
               const wchar_t  delimiter = L'/',
               const wchar_t  attrDelim = L':' )
    DNB_THROW_SPEC_NULL;


    virtual
    void
    getSymbol( DNBInt64VecSymbol& symbol,
               const scl_wstring& strPath,
               const wchar_t  delimiter = L'/',
               const wchar_t  attrDelim = L':' )
    DNB_THROW_SPEC_NULL;


    virtual
    void
    getSymbol( DNBRealSymbol& symbol,
               const scl_wstring& strPath,
               const wchar_t  delimiter = L'/',
               const wchar_t  attrDelim = L':' )
    DNB_THROW_SPEC_NULL;


    virtual
    void
    getSymbol( DNBRealVecSymbol& symbol,
               const scl_wstring& strPath,
               const wchar_t  delimiter = L'/',
               const wchar_t  attrDelim = L':' )
    DNB_THROW_SPEC_NULL;


    virtual
    void
    getSymbol( DNBVec3DSymbol& symbol,
               const scl_wstring& strPath,
               const wchar_t  delimiter = L'/',
               const wchar_t  attrDelim = L':' )
    DNB_THROW_SPEC_NULL;


    virtual
    void
    getSymbol( DNBEuler3DSymbol& symbol,
               const scl_wstring& strPath,
               const wchar_t  delimiter = L'/',
               const wchar_t  attrDelim = L':' )
    DNB_THROW_SPEC_NULL;

    const DNBBasicEntity::Handle&
    getEntity()
        DNB_THROW_SPEC_NULL;

    void
    setEntity( const DNBBasicEntity::Handle& hEntity )
        DNB_THROW_SPEC_NULL;


  private:
    DNBSymbolBase*
    locateSymbol_( const scl_wstring& strPath,
                   const wchar_t  delimiter = L'/',
                   const wchar_t  attrDelim = L':' )
        DNB_THROW_SPEC_NULL;


#ifdef DNB_CREATE_TEMPORARY_SYMBOLS
    DNBSymbolBase*
    createSymbol_( const scl_wstring& strPath,
                   const wchar_t  delimiter = L'/',
                   const wchar_t  attrDelim = L':' )
        DNB_THROW_SPEC_NULL;
#endif

    DNBBasicEntity::Handle hEntity_;
};



#endif  /* _DNB_WDMSYMBOLTABLE_H_ */
