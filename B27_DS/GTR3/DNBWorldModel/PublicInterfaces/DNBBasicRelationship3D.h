//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_BASICRELATIONSHIP3D_H_
#define _DNB_BASICRELATIONSHIP3D_H_


#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>


#include <DNBBasicEntity.h>
#include <DNBBasicEntity3D.h>
#include <DNBProfile3D.h>
#include <DNBException.h>


//
//  This class represents a logical relation between a pair of entities.
//
class ExportedByDNBWorldModel DNBBasicRelationship3D : public DNBBasicEntity
{
    DNB_DECLARE_DYNAMIC_RTTI( DNBBasicRelationship3D );

    DNB_DECLARE_SHARED_OBJECT( DNBBasicRelationship3D );

    DNB_DECLARE_EXTENDIBLE_OBJECT( DNBBasicRelationship3D );

public:
    virtual DNBMessage
    getEntityTypeName( ) const
        DNB_THROW_SPEC_NULL;

    void
    setRelationship( DNBBasicEntity3D::Handle hInnerEntity,
                     DNBBasicEntity3D::Handle hOuterEntity )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEAlreadyRegistered, DNBEInvalidRelation));

    void
    releaseRelationship( )
        DNB_THROW_SPEC_NULL;

    bool
    isRelationshipSet( ) const
        DNB_THROW_SPEC_NULL;

    DNBBasicEntity3D::Handle
    getInnerEntity( ) const
        DNB_THROW_SPEC_NULL;

    DNBBasicEntity3D::Handle
    getOuterEntity( ) const
        DNB_THROW_SPEC_NULL;

    bool
    verifyRelationship( ) const
        DNB_THROW_SPEC_NULL;

    void
    synchronize( )
        DNB_THROW_SPEC((scl_bad_alloc, DNBException));

    enum SyncMode
    {
	Auto,
	Manu
    };

    void
    setSyncMode( SyncMode mode )
        DNB_THROW_SPEC_NULL;

    SyncMode
    getSyncMode( ) const
        DNB_THROW_SPEC_NULL;

    enum Events
    {
        EventFirst = DNBBasicEntity::EventLast - 1,
        EventRelationshipSet,
        EventRelationshipReleased,
        EventLast
    };

protected:
    DNBBasicRelationship3D( )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit, DNBENetworkError));

    DNBBasicRelationship3D( const DNBBasicRelationship3D &right, CopyMode mode )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit, DNBENetworkError));

    virtual
    ~DNBBasicRelationship3D( )
        DNB_THROW_SPEC_NULL;

    virtual void
    initRelationship( )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEAlreadyRegistered, DNBEInvalidRelation));

    virtual void
    cleanup( )
        DNB_THROW_SPEC_NULL;

    virtual bool
    isValid( const DNBProfile3D &variableProfile ) const
        DNB_THROW_SPEC_NULL = 0;

    virtual void
    enforceRelationship( )
        DNB_THROW_SPEC((scl_bad_alloc, DNBException)) = 0;

    virtual DNBProfile3D
    calcVariableProfile( ) const
        DNB_THROW_SPEC_NULL;

private:
    DNBBasicEntity3D::Handle  hInnerEntity_;
    DNBBasicEntity3D::Handle  hOuterEntity_;

    SyncMode                mode_;
};


#endif  /* _DNB_BASICRELATIONSHIP3D_H_ */
