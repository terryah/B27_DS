//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//
//  Notes:
//      mmg     09/08/2003      added enum for lifecycle management
//
//  TODO:
//    + Add the following notifications:
//        o NotifEntityCloned           (specify new entity) ???
//        o NotifLogicalChildAdded      (specify child)
//        o NotifLogicalChildRemoved    (specify child)
//
//    + Define initialize() and finalize() methods which are called at the
//      beginning and end, respectively, of a simulation run.
//
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_BASICENTITY_H_
#define _DNB_BASICENTITY_H_


#include <DNBSystemBase.h>
#include <scl_string.h>
#include <scl_list.h>
#include <DNBSystemDefs.h>


#include <DNBWorldModel.h>


#include <DNBExtendibleObject.h>
#include <DNBUUID.h>
#include <DNBMessage.h>
#include <DNBSimTime.h>
//#include <DNBSymbol.h>        //smw; caa
class DNBSymbolBase;            // smw; caa


//
//  Provide the necessary forward declarations.
//
class DNBBasicWorld;
class DNBWDMSymbolTable;



//
//  This class represents an abstract entity of arbitrary dimension.  It is the
//  base class of all basic objects.
//
class ExportedByDNBWorldModel DNBBasicEntity : public DNBExtendibleObject
{
    DNB_DECLARE_DYNAMIC_CREATE( DNBBasicEntity );

    DNB_DECLARE_SHARED_OBJECT( DNBBasicEntity );

    DNB_DECLARE_EXTENDIBLE_OBJECT( DNBBasicEntity );

public:
    enum LifeCycle
    {
        EntityCreated=0,
        EntityModified,
        EntityDestroyed
    };

    enum RequestStatus
    {
        RequestRejected,
        RequestModified,
        RequestAccepted
    };

    //
    //  Name and description attributes.
    //
    inline const DNBUUID &
    getEntityID( ) const
        DNB_THROW_SPEC_NULL;

    static const wchar_t    NameSeparator;
    static const wchar_t    ExternalNameSeparator;

    virtual DNBMessage
    getEntityTypeName( ) const
        DNB_THROW_SPEC_NULL;

    enum EntityNameMode
    {
        EntityBaseName,                 // Short name of the entity.
        EntityFullName                  // Hierarchical name of the entity.
    };

    void
    setEntityName( const scl_wstring &baseName )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEInvalidName, DNBEDuplicateName));

    scl_wstring
    getEntityName( EntityNameMode mode   = EntityFullName,
                   wchar_t nameDelimiter = ExternalNameSeparator  ) const
        DNB_THROW_SPEC_NULL;

    DNBMessage
    getEntityTitle( EntityNameMode mode   = EntityFullName,
                    wchar_t nameDelimiter = ExternalNameSeparator  ) const
        DNB_THROW_SPEC((scl_bad_alloc));

    DNBBasicEntity::Handle
    findEntity( const scl_wstring &pathName,
                wchar_t nameDelimiter = ExternalNameSeparator  ) const
        DNB_THROW_SPEC_NULL;


    inline void
    setDescription( const scl_wstring &description )
        DNB_THROW_SPEC((scl_bad_alloc));

    inline const scl_wstring &
    getDescription( ) const
        DNB_THROW_SPEC_NULL;

    void
    resetEntityState( bool flush = true )
        DNB_THROW_SPEC((scl_bad_alloc, DNBException));

    //
    //  Logical entity hierarchy.
    //
    DNBSharedHandle<DNBBasicWorld>
    getWorld( ) const
        DNB_THROW_SPEC_NULL;

    bool
    hasLogicalParent( ) const
        DNB_THROW_SPEC_NULL;

    DNBBasicEntity::Handle
    getLogicalParent( ) const
        DNB_THROW_SPEC_NULL;

    bool
    hasLogicalAncestor( DNBBasicEntity::Handle hAncestor ) const
        DNB_THROW_SPEC_NULL;

    DNBBasicEntity::Handle
    getLogicalAncestor( const DNBClassInfo &classInfo ) const
        DNB_THROW_SPEC_NULL;

    void
    getLogicalAncestors( DNBBasicEntity::Vector &gAncestors ) const
        DNB_THROW_SPEC((scl_bad_alloc));

    void
    getLogicalAncestors( DNBBasicEntity::List   &gAncestors ) const
        DNB_THROW_SPEC((scl_bad_alloc));

    size_t
    getLogicalAncestorsCount( ) const
        DNB_THROW_SPEC_NULL;

    bool
    hasLogicalChild( DNBBasicEntity::Handle hChild ) const
        DNB_THROW_SPEC_NULL;

    bool
    hasLogicalChild( const scl_wstring& childName ) const
        DNB_THROW_SPEC_NULL;

    DNBBasicEntity::Handle
    getLogicalChild( const scl_wstring& PathName ) const
        DNB_THROW_SPEC_NULL;

    void
    getLogicalChildren( const DNBClassInfo &classInfo,
                        DNBBasicEntity::Vector &gChildren ) const
        DNB_THROW_SPEC((scl_bad_alloc));

    void
    getLogicalChildren( const DNBClassInfo &classInfo,
                        DNBBasicEntity::List   &gChildren ) const
        DNB_THROW_SPEC((scl_bad_alloc));

    size_t
    getLogicalChildrenCount( const DNBClassInfo &classInfo ) const
        DNB_THROW_SPEC_NULL;

    void
    getLogicalChildren( DNBBasicEntity::Vector &gChildren ) const
        DNB_THROW_SPEC((scl_bad_alloc));

    void
    getLogicalChildren( DNBBasicEntity::List   &gChildren ) const
        DNB_THROW_SPEC((scl_bad_alloc));

    size_t
    getLogicalChildrenCount( ) const
        DNB_THROW_SPEC_NULL;

    void
    flushLogicalState( )
        DNB_THROW_SPEC_NULL;

    //
    //  History status facility (currently not supported).
    //
    inline void
    setHistoryStatus( bool status )
        DNB_THROW_SPEC_NULL;

    inline bool
    getHistoryStatus( ) const
        DNB_THROW_SPEC_NULL;

    virtual void
    clearHistory( )
        DNB_THROW_SPEC_NULL;


    //
    // Symbol Attribute maintenance
    //
    //
    void
    addSymbol( DNBSymbolBase* newSymbol )
        DNB_THROW_SPEC((scl_bad_alloc));
    
    DNBSymbolBase*
    getSymbol( const scl_wstring& wstrSymbolName )
        DNB_THROW_SPEC((scl_bad_alloc));

    void
    removeSymbol( const scl_wstring& wstrSymbolName )
        DNB_THROW_SPEC((scl_bad_alloc));


    //
    //  Misc. attributes.
    //
    virtual DNBSimTime
    getSimulationTime( ) const
        DNB_THROW_SPEC_NULL;

    enum Events
    {
        EventFirst = DNBExtendibleObject::EventLast - 1,
        EventEntityDestroyed,           // TODO: Move to entity factory.
        EventEntityIDModified,
        EventBaseNameModified,
        EventFullNameModified,
        EventDescriptionModified,
        EventLogicalAncestorsModified,
        EventHistoryStatusModified,
        EventHistoryCleared,
        EventEntityStateReset,
        EventLast
    };

protected:
    //
    //  By defining the following methods in the protected section, this class
    //  can only be used as a base class.
    //
    DNBBasicEntity( )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit, DNBENetworkError));

    DNBBasicEntity( const DNBBasicEntity &right, CopyMode mode )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit, DNBENetworkError));

    virtual
    ~DNBBasicEntity( )
        DNB_THROW_SPEC_NULL;

    inline void
    setEntityID( const DNBUUID &entityID )
        DNB_THROW_SPEC_NULL;

    void
    addLogicalChild( DNBBasicEntity::Handle hChild )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEAlreadyRegistered));

    void
    removeLogicalChild( DNBBasicEntity::Handle hChild )
        DNB_THROW_SPEC((DNBENotFound));

    void
    removeLogicalChildren( const DNBClassInfo &classInfo )
        DNB_THROW_SPEC_NULL;

    void
    removeLogicalChildren( )
        DNB_THROW_SPEC_NULL;

    void
    orphanLogicalChild( DNBBasicEntity::Handle hChild )
        DNB_THROW_SPEC((DNBENotFound));

    void
    orphanLogicalChildren( const DNBClassInfo &classInfo )
        DNB_THROW_SPEC_NULL;

    void
    orphanLogicalChildren( )
        DNB_THROW_SPEC_NULL;

    void
    removeLogicalParent( )
        DNB_THROW_SPEC_NULL;

    virtual DNBDynamicObject *
    duplicateObject( CopyMode mode ) const
        DNB_THROW_SPEC((scl_bad_alloc, DNBException));

    virtual void
    resetEntity( )
        DNB_THROW_SPEC_NULL;

private:
    static const scl_wstring                        EmptyName;

    static const scl_wstring                        DefaultBaseName;

    static const scl_wstring                        DefaultFullName;

    static const scl_wstring                        DefaultCreateName;

    static const scl_wstring                        DefaultCloneName;

    static const scl_wstring                        DefaultDescription;

    typedef DNBBasicEntity::List                LogicalAncestorDB;

    typedef LogicalAncestorDB::iterator         LogicalAncestorIterator;

    typedef LogicalAncestorDB::const_iterator   LogicalAncestorConstIterator;

    typedef DNBBasicEntity::List                LogicalChildDB;

    typedef LogicalChildDB::iterator            LogicalChildIterator;

    typedef LogicalChildDB::const_iterator      LogicalChildConstIterator;

    typedef scl_list< DNBSymbolBase* , 
                  DNB_ALLOCATOR( DNBSymbolBase* ) >  SymbolDB;


    void
    updateFullName( )
        DNB_THROW_SPEC((scl_bad_alloc));

    void
    updateFullName( const scl_wstring &parentName )
        DNB_THROW_SPEC((scl_bad_alloc));

    void
    updateLogicalAncestors( const LogicalAncestorDB &parentAncestors )
        DNB_THROW_SPEC((scl_bad_alloc));

    void
    clearLogicalAncestors( )
        DNB_THROW_SPEC_NULL;

    LogicalChildIterator
    findLogicalChild( const DNBBasicEntity::Handle &hChild )
        DNB_THROW_SPEC((DNBENotFound));

    void
    setLogicalParent( const DNBBasicEntity::Handle &hParent,
        const scl_wstring &parentName, const LogicalAncestorDB &parentAncestors )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEAlreadyRegistered));

    void
    setLogicalChild( const DNBBasicEntity::Handle &hChild )
        DNB_THROW_SPEC((scl_bad_alloc));

    void
    clearLogicalParent( )
        DNB_THROW_SPEC_NULL;

    void
    clearLogicalChild( LogicalChildIterator iChild )
        DNB_THROW_SPEC_NULL;

    void
    cloneLogicalChildren( const DNBBasicEntity &right, CopyMode mode )
        DNB_THROW_SPEC((scl_bad_alloc, DNBException));

    void
    propagateLogicalAncestors( ) const
        DNB_THROW_SPEC_NULL;

    void
    propagateFullName( ) const
        DNB_THROW_SPEC_NULL;

    void
    onNotification( )
        DNB_THROW_SPEC_NULL;

    scl_wstring
    replaceDelimiter( const scl_wstring& strPathName,
                      wchar_t  oldDelimiter,
                      wchar_t  newDelimiter ) const
    DNB_THROW_SPEC_NULL;

    void
    removeAllSymbols( )
        DNB_THROW_SPEC_NULL;


    DNBUUID             entityID_;              // Unique ID of the entity
    scl_wstring             baseName_;              // Short name of the entity
    scl_wstring             fullName_;              // Complete name of the entity
    scl_wstring             description_;           // Entity description (optional)
    Handle              hLogicalParent_;        // Handle to logical parent
    LogicalAncestorDB   logicalAncestorDB_;     // Database of logical ancestors
    LogicalChildDB      logicalChildDB_;        // Database of logical children
    bool                historyStatus_;         // History buffer status
    SymbolDB            symbols_;               // Attribute symbols for this entity
};




//
//  Include the public definition file.
//
#include "DNBBasicEntity.cc"




#endif  /* _DNB_BASICENTITY_H_ */
