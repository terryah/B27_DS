//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//
//  Notes:
//    + None
//
//  TODO:
//    + None
//
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_BASICFRAME3D_H_
#define _DNB_BASICFRAME3D_H_


#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>


#include <DNBWorldModel.h>


#include <DNBBasicEntity3D.h>


//
//  Provide the necessary forward declarations.
//
class DNBBasicBody3D;




//
//  This class represents a fixed coordinate system on a body.  It will
//  eventually include support for spatial transforms.
//
class ExportedByDNBWorldModel DNBBasicFrame3D : public DNBBasicEntity3D
{
    DNB_DECLARE_DYNAMIC_CREATE( DNBBasicFrame3D );

    DNB_DECLARE_SHARED_OBJECT( DNBBasicFrame3D );

    DNB_DECLARE_EXTENDIBLE_OBJECT( DNBBasicFrame3D );

    DNB_DECLARE_EXTENDIBLE_FACTORY( DNBBasicFrame3D );

public:
    virtual DNBMessage
    getEntityTypeName( ) const
        DNB_THROW_SPEC_NULL;

    DNBSharedHandle<DNBBasicBody3D>
    getAttachedBody( ) const
        DNB_THROW_SPEC_NULL;

    RequestStatus
    setFrameLocation( DNBXform3D &frameLocation )
        DNB_THROW_SPEC_NULL;

    DNBXform3D
    getFrameLocation( ) const
        DNB_THROW_SPEC_NULL;

    inline  void
    setFrameTheta( DNBReal frameTheta )
        DNB_THROW_SPEC_NULL;

    inline  DNBReal
    getFrameTheta( ) const
        DNB_THROW_SPEC_NULL;

    inline  void
    setFrameAlpha( DNBReal frameAlpha )
        DNB_THROW_SPEC_NULL;

    inline  DNBReal
    getFrameAlpha( ) const
        DNB_THROW_SPEC_NULL;

    inline  void
    setFrameLength( DNBReal frameLength )
        DNB_THROW_SPEC_NULL;

    inline  DNBReal
    getFrameLength( ) const
        DNB_THROW_SPEC_NULL;

    inline  void
    setFrameDistance( DNBReal frameDistance )
        DNB_THROW_SPEC_NULL;

    inline  DNBReal
    getFrameDistance( ) const
        DNB_THROW_SPEC_NULL;

    enum Events
    {
        EventFirst = DNBBasicEntity3D::EventLast - 1,
        EventFrameLocationModified,
        EventFrameThetaModified,
        EventFrameAlphaModified,
        EventFrameLengthModified,
        EventFrameDistanceModified,
        EventLast
    };

protected:
    //
    //  By defining the following methods in the protected section, application
    //  programmers must use the static factory methods to create and destroy
    //  instances of this class.
    //
    DNBBasicFrame3D( )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit, DNBENetworkError));

    DNBBasicFrame3D( const DNBBasicFrame3D &right, CopyMode mode )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit, DNBENetworkError));

    virtual
    ~DNBBasicFrame3D( )
        DNB_THROW_SPEC_NULL;

    //
    //  The following methods are used to set/get the current profile of <self>
    //  with respect to its body.
    //
    virtual RequestStatus
    setRelativeLocation( DNBXform3D &relativeLocation )
        DNB_THROW_SPEC_NULL;

    virtual DNBXform3D
    getRelativeLocation( ) const
        DNB_THROW_SPEC_NULL;

    virtual RequestStatus
    setRelativeProfile( DNBProfile3D &relativeProfile )
        DNB_THROW_SPEC_NULL;

    virtual DNBProfile3D
    getRelativeProfile( ) const
        DNB_THROW_SPEC_NULL;

private:
    DNBXform3D      frameLocation_;     // Pose wrt base frame of body.
    DNBReal         frameTheta_;        // Angular kinematics attribute (rad).
    DNBReal         frameAlpha_;        // Angular kinematics attribute (rad).
    DNBReal         frameLength_;       // Linear kinematics attribute (m).
    DNBReal         frameDistance_;     // Linear kinematics attribute (m).
};




//
//  Include the public definition file.
//
#include "DNBBasicFrame3D.cc"




#endif  /* _DNB_BASICFRAME3D_H_ */
