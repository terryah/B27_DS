//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//*
//* FILE:
//*     AttributeConstIter.cc           - principle implementation file
//*
//* MODULE:
//*     DNBAttributeConstIterator       - single non-template class
//*
//* OVERVIEW:
//*     This module defines a const iterator preventing any usage outside 
//*     the scope of a transaction.
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     joy         02/05/99    Initial implementation
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1998, 99 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*

template < class Key, class Type >
DNBAttributeConstIterator<Key, Type>::DNBAttributeConstIterator( 	
				DNBSharedHandle<Type>  			 hExt,
			        ConstIterator iter )
  		       		DNB_THROW_SPEC_NULL
                     		: hExt_( hExt ), iter_( iter ) 
{
    DNB_PRECONDITION( hExt._getObject( )->isReadLocked( ));
}


template < class Key, class Type >
DNBAttributeConstIterator<Key, Type>::~DNBAttributeConstIterator( )
  				DNB_THROW_SPEC_NULL
{
    // Nothing.
}

template < class Key, class Type >
inline
DNBAttributeConstIterator<Key, Type>
DNBAttributeConstIterator<Key, Type>::operator++( ) 
  				DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION( hExt_._getObject( )->isReadLocked( ));
    ++iter_;
    return *this;

}


template < class Key, class Type >
inline
DNBAttributeConstIterator<Key, Type>
DNBAttributeConstIterator<Key, Type>::operator--( ) 
  				DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION( hExt_._getObject( )->isReadLocked( ));
    --iter_;
    return *this;
     

}


template < class Key, class Type >
inline
DNBAttributeConstIterator<Key, Type>
DNBAttributeConstIterator<Key, Type>::operator++( int ) 
  				DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION( hExt_._getObject( )->isReadLocked( ));
    DNBAttributeConstIterator<Key, Type> tmp = *this;
    ++iter_;
    return tmp;

}


template < class Key, class Type >
inline
DNBAttributeConstIterator<Key, Type>
DNBAttributeConstIterator<Key, Type>::operator--( int ) 
  				DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION( hExt_._getObject( )->isReadLocked( ));
    DNBAttributeConstIterator<Key, Type> tmp = *this;
    --iter_;
    return tmp;
     

}


template < class Key, class Type >
inline
bool
DNBAttributeConstIterator<Key, Type>::operator==( 
				const DNBAttributeConstIterator<Key, Type>& right ) const
  				DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION( hExt_._getObject( )->isReadLocked( ));
    return ( iter_ == right.iter_ );

}


template < class Key, class Type >
inline
bool
DNBAttributeConstIterator<Key, Type>::operator!=( 
				const DNBAttributeConstIterator<Key, Type>& right ) const
  				DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION( hExt_._getObject( )->isReadLocked( ));
    return ( iter_ != right.iter_ );

}


template < class Key, class Type >
inline
typename
DNBAttributeConstIterator<Key, Type>::const_reference
DNBAttributeConstIterator<Key, Type>::operator*( ) const 
  				DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION( hExt_._getObject( )->isReadLocked( ));
    return *((*iter_).second );

}


template < class Key, class Type >
inline
typename
DNBAttributeConstIterator<Key, Type>::const_pointer
DNBAttributeConstIterator<Key, Type>::operator->() const
  				DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION( hExt_._getObject( )->isReadLocked( ));
    return ((*iter_).second );

}

template < class Key, class Type >
inline
const Key&
DNBAttributeConstIterator<Key, Type>::getKey( ) const 
  				DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION( hExt_._getObject( )->isReadLocked( ));
    return (*iter_).first;

}


template < class Key, class Type >
inline
typename
DNBAttributeConstIterator<Key, Type>::const_reference
DNBAttributeConstIterator<Key, Type>::getData( ) const 
  				DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION( hExt_._getObject( )->isReadLocked( ));
    return *((*iter_).second );

}


template < class Key, class Type >
inline
typename
DNBAttributeConstIterator<Key, Type>::ValueType
DNBAttributeConstIterator<Key, Type>::getPair( ) const 
  				DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION( hExt_._getObject( )->isReadLocked( ));
    return (*iter_);

}
