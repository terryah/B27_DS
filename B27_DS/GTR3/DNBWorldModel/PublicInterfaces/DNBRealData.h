//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//*
//* FILE:
//*     DNBRealData.h        - public header file
//*
//* MODULE:
//*     DNBSymbolMD.m  - single template class
//*
//* OVERVIEW:
//*     This file provides data storage for temporary DNBReal symbols
//*
//* HISTORY:
//*     Author    Date       Purpose
//*     ------    ----       -------
//*     bpl       09/15/2000  Initial implementation
//*
//* REVIEWED:
//*     Reviewer  Date       Remarks
//*     --------  ----       -------
//*     xxx       mm/dd/yy   xxxx
//*
//* TODO:
//*     Developer Date       Suggestion
//*     --------- ----       ----------
//*     xxx       mm/dd/yy   xxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 2000 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _DNBREALDATA_H_
#define _DNBREALDATA_H_

#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>

#include <DNBWorldModel.h>
#include <DNBSymbol.h>


// add 09/15/2000 by bpl
//   The Data class is a method for creating DNBSymbols on the fly.
// This class provides a storage location for the value the symbol
// points too.  A copy of this class is created on the heap and a symbol
// is created that connects to value.  At this time there is no
// mechanism for deleting the instances on the heap, so there will
// be a memory leak.
//   This is a temporary solution till Joe work out an overall design.

//   Change to WDMSymbolTable:  If the symbol table can not find a symbol
// of type DNBReal, it will create one as well as a DNBRealData to store
// its value.

class ExportedByDNBWorldModel DNBRealData
{
public:
    // Typedef DNBRealData to self
    typedef DNBRealData self;

    // Typedef self* to pointer
    typedef self* pointer;

    // Default constructor
    DNBRealData()
        DNB_THROW_SPEC_NULL;

    // Inilization constructor, takes a DNBSymbol, name, and value
    DNBRealData( DNBSymbol<DNBReal>& symbol, const scl_wstring &name, DNBReal value )
        DNB_THROW_SPEC_NULL;

    // Copy constuctor
    DNBRealData( const self& right)
        DNB_THROW_SPEC_NULL;

    // Destructor
    ~DNBRealData()
        DNB_THROW_SPEC_NULL;

    // Returns the value of the symbol, used by functor
    DNBReal
    getRealDataValue()
        DNB_THROW_SPEC_NULL;

    // Sets to value of the symbol, used by functor
    void
    setRealDataValue( DNBReal value )
        DNB_THROW_SPEC_NULL;

private:
    // Symbol name
    scl_wstring name_;

    // Symbol value
    DNBReal value_;
};

#endif  /* _DNBREALDATA_H_ */
