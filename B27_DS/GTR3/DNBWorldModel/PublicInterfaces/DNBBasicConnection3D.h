//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//
//  Notes:
//    + None
//
//  TODO:
//    + Nothing
//
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_BASICCONNECTION3D_H_
#define _DNB_BASICCONNECTION3D_H_


#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>


#include <DNBWorldModel.h>


#include <DNBBasicEntity.h>
#include <DNBBasicFrame3D.h>
#include <DNBBasicBody3D.h>




//
//  This class represents a physical connection between a pair of bodies.  The
//  connection is defined by the relative location of two frames, one from each
//  body.  When the connection is placed in its "home" position, the frames are
//  coincident and their axes are aligned.
//
class ExportedByDNBWorldModel DNBBasicConnection3D : public DNBBasicEntity
{
    DNB_DECLARE_DYNAMIC_RTTI( DNBBasicConnection3D );

    DNB_DECLARE_SHARED_OBJECT( DNBBasicConnection3D );

    DNB_DECLARE_EXTENDIBLE_OBJECT( DNBBasicConnection3D );

public:
    virtual DNBMessage
    getEntityTypeName( ) const
        DNB_THROW_SPEC_NULL;

    //
    //  The following methods are used to manage connections between adjacent
    //  bodies.
    //
    void
    connectBodies(
        DNBBasicFrame3D::Handle hInnerFrame,
        DNBBasicFrame3D::Handle hOuterFrame )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEAlreadyRegistered, DNBEInvalidRelation));

    void
    connectBodies(
        DNBBasicBody3D::Handle hInnerBody,
        DNBBasicBody3D::Handle hOuterBody,
        const DNBXform3D &innerOffset,
        const DNBXform3D &outerOffset = DNBXform3D::Identity )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEAlreadyRegistered, DNBEInvalidRelation));

    void
    disconnectBodies( )
        DNB_THROW_SPEC_NULL;

    bool
    isConnected( ) const
        DNB_THROW_SPEC_NULL;

    DNBBasicBody3D::Handle
    getInnerBody( ) const
        DNB_THROW_SPEC_NULL;

    DNBBasicBody3D::Handle
    getOuterBody( ) const
        DNB_THROW_SPEC_NULL;

    DNBBasicFrame3D::Handle
    getInnerFrame( ) const
        DNB_THROW_SPEC_NULL;

    DNBBasicFrame3D::Handle
    getOuterFrame( ) const
        DNB_THROW_SPEC_NULL;

    const DNBXform3D &
    getInnerOffset( ) const
        DNB_THROW_SPEC_NULL;

    const DNBXform3D &
    getOuterOffset( ) const
        DNB_THROW_SPEC_NULL;

    virtual void
    updateKinematics( )
        DNB_THROW_SPEC_NULL;


    enum Events
    {
        EventFirst = DNBBasicEntity::EventLast - 1,
        EventBodiesConnected,
        EventBodiesDisconnected,
        EventInnerOffsetModified,
        EventOuterOffsetModified,
        EventRelativeLocationModified,  // Instantaneous change in location
        EventRelativeProfileModified,   // Complete profile modified
        EventLast
    };

protected:
    //
    //  By defining the following methods in the protected section, this class
    //  can only be used as a base class.
    //
    DNBBasicConnection3D( )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit, DNBENetworkError));

    DNBBasicConnection3D( const DNBBasicConnection3D &right, CopyMode mode )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit, DNBENetworkError));

    virtual
    ~DNBBasicConnection3D( )
        DNB_THROW_SPEC_NULL;

    //
    //  The following methods are used to set/get the cached profile of the
    //  outer body with respect to the inner body.
    //
    void
    setCachedRelativeLocation( const DNBXform3D &relativeLocation )
        DNB_THROW_SPEC_NULL;

    const DNBXform3D &
    getCachedRelativeLocation( ) const
        DNB_THROW_SPEC_NULL;

    void
    setCachedRelativeProfile( const DNBProfile3D &relativeProfile )
        DNB_THROW_SPEC_NULL;

    const DNBProfile3D &
    getCachedRelativeProfile( ) const
        DNB_THROW_SPEC_NULL;

    //
    //  The following methods are used to set/get the current profile of the
    //  outer body with respect to the inner body.  These methods may be
    //  overridden in the derived classes.
    //
    virtual RequestStatus
    setRelativeLocation( DNBXform3D &relativeLocation )
        DNB_THROW_SPEC_NULL;

    virtual DNBXform3D
    getRelativeLocation( ) const
        DNB_THROW_SPEC_NULL;

    virtual RequestStatus
    setRelativeProfile( DNBProfile3D &relativeProfile )
        DNB_THROW_SPEC_NULL;

    virtual DNBProfile3D
    getRelativeProfile( ) const
        DNB_THROW_SPEC_NULL;

    //
    //  The following method computes the relative location of the outer body
    //  with respect to the inner body, using the current configuration of the
    //  connection.  The method may be optimized in derived classes.
    //
    virtual void
    updateRelativeLocation( )
        DNB_THROW_SPEC_NULL;

    //
    //  The following method is used to get the variable (or joint) location
    //  of the outer frame with respect to the inner frame.  Each derived class
    //  must provide an implementation of this method.
    //
#if 0
    virtual void 
    setVariableLocation( DNBXform3D& variableLocation ) 
        DNB_THROW_SPEC_NULL = 0;
#endif

    virtual DNBXform3D
    getVariableLocation( ) const
        DNB_THROW_SPEC_NULL = 0;

private:
    void
    setInnerOffset( const DNBXform3D &innerOffset )
        DNB_THROW_SPEC_NULL;

    void
    setOuterOffset( const DNBXform3D &outerOffset )
        DNB_THROW_SPEC_NULL;

//    void
//    setInnerFrame( DNBBasicFrame3D::Handle innerFrame )
//        DNB_THROW_SPEC_NULL;


    void
    propagateLocation( )
        DNB_THROW_SPEC_NULL;

    void
    propagateProfile( )
        DNB_THROW_SPEC_NULL;

    void
    onEvent( const DNBBasicFrame3D::ConstPointer &pPublisher,
        const EventSet &events )
        DNB_THROW_SPEC_NULL;

    static  DNBXform3D
    calcRelativeLocation( const DNBXform3D &innerOffset,
        const DNBXform3D &variableLocation, const DNBXform3D &outerOffset )
        DNB_THROW_SPEC_NULL;

    static  DNBXform3D
    calcOuterOffset( const DNBXform3D &relativeLocation,
        const DNBXform3D &innerOffset, const DNBXform3D &variableLocation )
        DNB_THROW_SPEC_NULL;

    DNBBasicBody3D::Handle  hInnerBody_;
    DNBBasicBody3D::Handle  hOuterBody_;
    DNBBasicFrame3D::Handle hInnerFrame_;
    DNBBasicFrame3D::Handle hOuterFrame_;
    DNBXform3D              innerOffset_;       // Inner base to inner frame
    DNBXform3D              outerOffset_;       // Outer base to outer frame
    DNBProfile3D            relativeProfile_;   // Inner base to outer base

    //
    //  The following declaration allows the class DNBBasicBody3D to call the
    //  methods {set,get}RelativeLocation() and {set,get}RelativeProfile().
    //
    friend class DNBBasicBody3D;
    friend class DNBBasicMfgAssembly3D;
    friend class DNBForwardKinSolverGraph;
};




#endif  /* _DNB_BASICCONNECTION3D_H_ */
