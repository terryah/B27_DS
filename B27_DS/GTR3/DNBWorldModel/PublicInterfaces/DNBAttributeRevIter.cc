//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//*
//* FILE:
//*     AttributeRevIter.cc           - principle implementation file
//*
//* MODULE:
//*     DNBAttributeRevIterator       - single non-template class
//*
//* OVERVIEW:
//*     This module defines a reverse iterator preventing any usage outside 
//*     the scope of a transaction.
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     joy         02/05/99    Initial implementation
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1998, 99 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*
template < class Key, class Type >
DNBAttributeRevIterator<Key, Type>::DNBAttributeRevIterator(
				 DNBSharedHandle<Type> 			  hExt,
			         ReverseIterator iter )
  			DNB_THROW_SPEC_NULL
                        : hExt_( hExt ), iter_( iter ) 
{
    DNB_PRECONDITION( hExt._getObject( )->isWriteLocked( ));
}

template < class Key, class Type >
DNBAttributeRevIterator<Key, Type>::~DNBAttributeRevIterator( )
  			DNB_THROW_SPEC_NULL
{
    // Nothing.
}

template < class Key, class Type >
inline
DNBAttributeRevIterator<Key, Type>
DNBAttributeRevIterator<Key, Type>::operator++( )
  			DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION( hExt_._getObject( )->isWriteLocked( ));
    ++iter_;
    return *this;

}


template < class Key, class Type >
inline
DNBAttributeRevIterator<Key, Type>
DNBAttributeRevIterator<Key, Type>::operator--( )
  			DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION( hExt_._getObject( )->isWriteLocked( ));
    --iter_;
    return *this;
     

}


template < class Key, class Type >
inline
DNBAttributeRevIterator<Key, Type>
DNBAttributeRevIterator<Key, Type>::operator++( int )
  			DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION( hExt_._getObject( )->isWriteLocked( ));
    DNBAttributeRevIterator<Key, Type> tmp = *this;
    ++iter_;
    return tmp;

}


template < class Key, class Type >
inline
DNBAttributeRevIterator<Key, Type>
DNBAttributeRevIterator<Key, Type>::operator--( int )
  			DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION( hExt_._getObject( )->isWriteLocked( ));
    DNBAttributeRevIterator<Key, Type> tmp = *this;
    --iter_;
    return tmp;
     

}


template < class Key, class Type >
inline
bool
DNBAttributeRevIterator<Key, Type>::operator==( 
			const DNBAttributeRevIterator<Key, Type>& right ) const 
  			DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION( hExt_._getObject( )->isWriteLocked( ));
    return ( iter_ == right.iter_ );

}


template < class Key, class Type >
inline
bool
DNBAttributeRevIterator<Key, Type>::operator!=( 
			const DNBAttributeRevIterator<Key, Type>& right ) const
  			DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION( hExt_._getObject( )->isWriteLocked( ));
    return ( iter_ != right.iter_ );

}


template < class Key, class Type >
inline
typename
DNBAttributeRevIterator<Key, Type>::reference
DNBAttributeRevIterator<Key, Type>::operator*( ) const
  			DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION( hExt_._getObject( )->isWriteLocked( ));
    return *((*iter_).second );

}


template < class Key, class Type >
inline
typename
DNBAttributeRevIterator<Key, Type>::pointer
DNBAttributeRevIterator<Key, Type>::operator->() const
  				DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION( hExt_._getObject( )->isWriteLocked( ));
    return ((*iter_).second );

}


template < class Key, class Type >
inline
const Key&
DNBAttributeRevIterator<Key, Type>::getKey( ) const
  			DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION( hExt_._getObject( )->isWriteLocked( ));
    return (*iter_).first;

}


template < class Key, class Type >
inline
typename
DNBAttributeRevIterator<Key, Type>::const_reference
DNBAttributeRevIterator<Key, Type>::getData( ) const
  			DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION( hExt_._getObject( )->isWriteLocked( ));
    return *((*iter_).second );

}


template < class Key, class Type >
inline
typename
DNBAttributeRevIterator<Key, Type>::ValueType
DNBAttributeRevIterator<Key, Type>::getPair( ) const 
  			DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION( hExt_._getObject( )->isWriteLocked( ));
    return (*iter_);

}
