//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//
//  Notes:
//    + None
//
//  TODO:
//
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_BASICTAGPOINT3D_H_
#define _DNB_BASICTAGPOINT3D_H_


#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>


#include <DNBWorldModel.h>


#include <DNBSimTime.h>
#include <DNBBasicEntity3D.h>


//
//  Provide the necessary forward declarations.
//
class DNBBasicBody3D;


//
//  This class represents an abstract body with extent.  It will eventually
//  include support for tag points and paths.
//
class ExportedByDNBWorldModel DNBBasicTagPoint3D : public DNBBasicEntity3D
{
    DNB_DECLARE_DYNAMIC_CREATE( DNBBasicTagPoint3D );
    DNB_DECLARE_SHARED_OBJECT( DNBBasicTagPoint3D );
    DNB_DECLARE_EXTENDIBLE_OBJECT( DNBBasicTagPoint3D );
    DNB_DECLARE_EXTENDIBLE_FACTORY( DNBBasicTagPoint3D );

public:
    DNBBasicTagPoint3D( )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit, DNBENetworkError));

    DNBBasicTagPoint3D( const DNBBasicTagPoint3D &right, CopyMode mode )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit, DNBENetworkError));

    virtual
    ~DNBBasicTagPoint3D( )
        DNB_THROW_SPEC_NULL;


    virtual DNBMessage
    getEntityTypeName( ) const
        DNB_THROW_SPEC_NULL;

    DNBSharedHandle< DNBBasicBody3D >
    getAttachedBody( ) const
        DNB_THROW_SPEC_NULL;

    RequestStatus
    setTagPointLocation( DNBXform3D &pointLocation )
        DNB_THROW_SPEC_NULL;

    DNBXform3D
    getPointLocation( ) const
        DNB_THROW_SPEC_NULL;
/*
    DNBSimTime
    getMoveTime( ) const
        DNB_THROW_SPEC_NULL;

    void 
    setMoveTime( DNBSimTime& moveTime )
        DNB_THROW_SPEC_NULL;
*/
    enum Events
    {
        EventFirst = DNBBasicEntity3D::EventLast - 1,
        EventPointLocationModified,
        EventLast
    };

   enum TimingMode
    {	
		DURATION,
		VELOCITY    
	};


	void setDuration(DNBSimTime &value)
        DNB_THROW_SPEC_NULL;

	// % for joint, absolute for linear
	void setVelocity(DNBReal value)
        DNB_THROW_SPEC_NULL;

	DNBSimTime getDuration() const
        DNB_THROW_SPEC_NULL;

	DNBReal getVelocity() const
        DNB_THROW_SPEC_NULL;

 	TimingMode getTimingMode()
        DNB_THROW_SPEC_NULL;

    void lockRelativeLocation()
        DNB_THROW_SPEC_NULL;

    void unlockRelativeLocation()
        DNB_THROW_SPEC_NULL;

protected:
    virtual RequestStatus
    setRelativeLocation( DNBXform3D &relativeLocation )
        DNB_THROW_SPEC_NULL;

    virtual DNBXform3D
    getRelativeLocation( ) const
        DNB_THROW_SPEC_NULL;

    virtual RequestStatus
    setRelativeProfile( DNBProfile3D &relativeProfile )
        DNB_THROW_SPEC_NULL;

    virtual DNBProfile3D
    getRelativeProfile( ) const
        DNB_THROW_SPEC_NULL;

private:
    DNBXform3D      pointLocation_;     // Pos. wrt base frame of spatial parent
//    DNBSimTime      moveTime_;
	TimingMode		timingmode_;
	DNBSimTime		duration_;
	DNBReal			velocity_;
    bool            relativelocked_;
};




#endif  /* _DNB_BASICTAGPOINT3D_H_ */

