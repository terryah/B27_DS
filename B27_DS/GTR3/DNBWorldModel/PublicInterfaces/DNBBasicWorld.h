//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//
//  Notes:
//    + None
//
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_BASICWORLD_H_
#define _DNB_BASICWORLD_H_


#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>


#include <DNBWorldModel.h>


#include <DNBBasicEntity.h>
#include <DNBSimTime.h>


//
//  This class represents a simulated world.
//
class ExportedByDNBWorldModel DNBBasicWorld : public DNBBasicEntity
{
    DNB_DECLARE_DYNAMIC_CREATE( DNBBasicWorld );

    DNB_DECLARE_SHARED_OBJECT( DNBBasicWorld );

    DNB_DECLARE_EXTENDIBLE_OBJECT( DNBBasicWorld );

    DNB_DECLARE_EXTENDIBLE_FACTORY( DNBBasicWorld );

public:
    virtual DNBMessage
    getEntityTypeName( ) const
        DNB_THROW_SPEC_NULL;

    //
    //  Simulation time management.
    //
#if 0
    void
    setSimulationTime( size_t index, const DNBSimTime &simTime );

    DNBSimTime
    getSimulationTime( size_t index ) const;
#endif

    void
    setSimulationTime( const DNBSimTime &simTime )
        DNB_THROW_SPEC_NULL;

    virtual DNBSimTime
    getSimulationTime( ) const
        DNB_THROW_SPEC_NULL;

    //
    //  Logical entity hierarchy.
    //
    void
    addEntity( DNBBasicEntity::Handle hEntity )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEAlreadyRegistered));

    void
    removeEntity( DNBBasicEntity::Handle hEntity )
        DNB_THROW_SPEC((DNBENotFound));

    void
    removeEntities( const DNBClassInfo &classInfo )
        DNB_THROW_SPEC_NULL;

    void
    removeEntities( )
        DNB_THROW_SPEC_NULL;

    void
    destroyEntity( DNBBasicEntity::Handle hEntity )
        DNB_THROW_SPEC((DNBENotFound));

    void
    destroyEntities( const DNBClassInfo &classInfo )
        DNB_THROW_SPEC_NULL;

    void
    destroyEntities( )
        DNB_THROW_SPEC_NULL;

    bool
    hasEntity( DNBBasicEntity::Handle hEntity ) const
        DNB_THROW_SPEC_NULL;

    void
    getEntities( const DNBClassInfo &classInfo,
        DNBBasicEntity::Vector &gEntities ) const
        DNB_THROW_SPEC((scl_bad_alloc));

    void
    getEntities( const DNBClassInfo &classInfo,
        DNBBasicEntity::List   &gEntities ) const
        DNB_THROW_SPEC((scl_bad_alloc));

    size_t
    getEntityCount( const DNBClassInfo &classInfo ) const
        DNB_THROW_SPEC_NULL;

    void
    getEntities( DNBBasicEntity::Vector &gEntities ) const
        DNB_THROW_SPEC((scl_bad_alloc));

    void
    getEntities( DNBBasicEntity::List   &gEntities ) const
        DNB_THROW_SPEC((scl_bad_alloc));

    size_t
    getEntityCount( ) const
        DNB_THROW_SPEC_NULL;

    enum Events
    {
        EventFirst = DNBBasicEntity::EventLast - 1,
        EventSimulationTimeChanged,
        EventLast
    };

protected:
    //
    //  By defining the following methods in the protected section, application
    //  programmers must use the static factory methods to create and destroy
    //  instances of this class.
    //
    DNBBasicWorld( )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit, DNBENetworkError));

    DNBBasicWorld( const DNBBasicWorld &right, CopyMode mode )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit, DNBENetworkError));

    virtual
    ~DNBBasicWorld( )
        DNB_THROW_SPEC_NULL;

private:
    DNBSimTime      simTime_;           // Current simulation time
};




#endif  /* _DNB_BASICWORLD_H_ */
