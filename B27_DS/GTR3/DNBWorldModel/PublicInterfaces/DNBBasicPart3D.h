//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//
//  Notes:
//    + None
//
//  TODO:
//    + Add support for affixments.
//
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_BASICPART3D_H_
#define _DNB_BASICPART3D_H_


#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>


#include <DNBWorldModel.h>


#include <DNBBasicBody3D.h>




//
//  This class represents a rigid body.
//
class ExportedByDNBWorldModel DNBBasicPart3D : public DNBBasicBody3D
{
    DNB_DECLARE_DYNAMIC_CREATE( DNBBasicPart3D );

    DNB_DECLARE_SHARED_OBJECT( DNBBasicPart3D );

    DNB_DECLARE_EXTENDIBLE_OBJECT( DNBBasicPart3D );

    DNB_DECLARE_EXTENDIBLE_FACTORY( DNBBasicPart3D );

public:
    virtual DNBMessage
    getEntityTypeName( ) const
        DNB_THROW_SPEC_NULL;

    enum Events
    {
        EventFirst = DNBBasicBody3D::EventLast - 1,
        EventLast
    };

protected:
    //
    //  By defining the following methods in the protected section, application
    //  programmers must use the static factory methods to create and destroy
    //  instances of this class.
    //
    DNBBasicPart3D( )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit, DNBENetworkError));

    DNBBasicPart3D( const DNBBasicPart3D &right, CopyMode mode )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit, DNBENetworkError));

    virtual
    ~DNBBasicPart3D( )
        DNB_THROW_SPEC_NULL;
};




#endif  /* _DNB_BASICPART3D_H_ */
