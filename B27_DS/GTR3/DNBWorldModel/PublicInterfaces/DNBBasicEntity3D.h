//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//sure
//  Notes:
//    + None
//
//  TODO:
//    + Implement a history buffer for baseProfile_.
//
//    + Add the following notifications:
//        o NotifSpatialChildAdded      (specify child)
//        o NotifSpatialChildRemoved    (specify child)
//
//    + Implement a facility which computes the base profile on demand.  This
//      computation is triggered when a client calls getBaseLocation() or
//      getBaseProfile().  The relative profile will be computed instantly,
//      since this operation is rarely performed during simulation.
//
//      mmg     03/21/2003      added Events and support for tracking
//                              relative transform changes
//      mmg     03/21/2003      added a cache of the spatial parents profile
//                              to avoid a possible deadlock when calculating
//                              the transform relative to the spatial parent
//      mmg     05/13/2004      added method to destroy spatial children without
//                              updating their base location since they are
//                              being destroyed anyway
//
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_BASICENTITY3D_H_
#define _DNB_BASICENTITY3D_H_


#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>


// #include <DNBNotificationBase.h>  // smw; caa
#include <DNBWorldModel.h>


#include <DNBBasicEntity.h>
#include <DNBSimTime.h>
// #include <DNBMath3D.h> // smw ; caa;  
// #include <DNBVector3D.h> // smw; caa; 
#include <DNBXform3D.h>
#include <DNBProfile3D.h>
#include <DNBBasicDataCache.h>



//
//  This class represents a three-dimensional entity with a base coordinate
//  system.  The three-dimensional entities in the world are arranged in a
//  singly-root tree, which characterizes their spatial relationships.
//
class ExportedByDNBWorldModel DNBBasicEntity3D : public DNBBasicEntity
{
    DNB_DECLARE_DYNAMIC_RTTI( DNBBasicEntity3D );

    DNB_DECLARE_SHARED_OBJECT( DNBBasicEntity3D );

    DNB_DECLARE_EXTENDIBLE_OBJECT( DNBBasicEntity3D );

public:
    virtual DNBMessage
    getEntityTypeName( ) const
        DNB_THROW_SPEC_NULL;

    //
    //  Spatial entity hierarchy.
    //
    bool
    hasSpatialParent( ) const
        DNB_THROW_SPEC_NULL;

    DNBBasicEntity3D::Handle
    getSpatialParent( ) const
        DNB_THROW_SPEC_NULL;

    DNBBasicDataCache::Handle
    getSpatialParentCache( ) const
        DNB_THROW_SPEC_NULL;

    bool
    hasSpatialAncestor( DNBBasicEntity3D::Handle hAncestor ) const
        DNB_THROW_SPEC_NULL;

    DNBBasicEntity3D::Handle
    getSpatialAncestor( const DNBClassInfo &classInfo ) const
        DNB_THROW_SPEC_NULL;

    void
    getSpatialAncestors( DNBBasicEntity3D::Vector &gAncestors ) const
        DNB_THROW_SPEC((scl_bad_alloc));

    void
    getSpatialAncestors( DNBBasicEntity3D::List   &gAncestors ) const
        DNB_THROW_SPEC((scl_bad_alloc));

    size_t
    getSpatialAncestorsCount( ) const
        DNB_THROW_SPEC_NULL;

////////DJB

    bool
    hasSpatialDescendant( DNBBasicEntity3D::Handle hDescendant ) const
        DNB_THROW_SPEC_NULL;

    DNBBasicEntity3D::Handle
    getSpatialDescendant( const DNBClassInfo &classInfo ) const
        DNB_THROW_SPEC_NULL;

    void
    getSpatialDescendants( DNBBasicEntity3D::Vector &gDescendant ) const
        DNB_THROW_SPEC((scl_bad_alloc));

    void
    getSpatialDescendants( DNBBasicEntity3D::List   &gDescendant ) const
        DNB_THROW_SPEC((scl_bad_alloc));

    size_t
    getSpatialDescendantsCount( ) const
        DNB_THROW_SPEC_NULL;

////////DJB

    bool
    hasSpatialChild( DNBBasicEntity3D::Handle hChild ) const
        DNB_THROW_SPEC_NULL;

    void
    getSpatialChildren( const DNBClassInfo &classInfo,
        DNBBasicEntity3D::Vector &gChildren ) const
        DNB_THROW_SPEC((scl_bad_alloc));

    void
    getSpatialChildren( const DNBClassInfo &classInfo,
        DNBBasicEntity3D::List   &gChildren ) const
        DNB_THROW_SPEC((scl_bad_alloc));

    size_t
    getSpatialChildrenCount( const DNBClassInfo &classInfo ) const
        DNB_THROW_SPEC_NULL;

    void
    getSpatialChildren( DNBBasicEntity3D::Vector &gChildren ) const
        DNB_THROW_SPEC((scl_bad_alloc));

    void
    getSpatialChildren( DNBBasicEntity3D::List   &gChildren ) const
        DNB_THROW_SPEC((scl_bad_alloc));

    size_t
    getSpatialChildrenCount( ) const
        DNB_THROW_SPEC_NULL;

    bool
    propagateLocationEnabled( ) const
        DNB_THROW_SPEC_NULL;
    
    //
    //  The following methods are used to set/get the past and present profile
    //  of <self> with respect to the world.
    //
#if 0
    void
    setBaseLocation( size_t index, const DNBSimTime &simTime,
        const DNBXform3D &baseLocation )
        DNB_THROW_SPEC_NULL;

    void
    getBaseLocation( size_t index, DNBSimTime &simTime,
        DNBXform3D &baseLocation ) const
        DNB_THROW_SPEC_NULL;

    void
    setBaseProfile( size_t index, const DNBSimTime &simTime,
        const DNBProfile3D &baseProfile )
        DNB_THROW_SPEC_NULL;

    void
    getBaseProfile( size_t index, DNBSimTime &simTime,
        DNBProfile3D &baseProfile ) const
        DNB_THROW_SPEC_NULL;
#endif

    //
    //  The following methods are used to set/get the current profile of <self>
    //  with respect to the world.
    //
    RequestStatus
    setBaseLocation( DNBXform3D &baseLocation )
        DNB_THROW_SPEC_NULL;

    const DNBXform3D &
    getBaseLocation( ) const
        DNB_THROW_SPEC_NULL;

    RequestStatus
    setBaseProfile( DNBProfile3D &baseProfile )
        DNB_THROW_SPEC_NULL;

    const DNBProfile3D &
    getBaseProfile( ) const
        DNB_THROW_SPEC_NULL;

    //
    //  The following methods are used to set/get the current profile of <self>
    //  with respect to the entity <hOther>.
    //
    RequestStatus
    setBaseLocation( const DNBXform3D &relativeLocation,
        DNBBasicEntity3D::Handle hOther )
        DNB_THROW_SPEC_NULL;

    DNBXform3D
    getBaseLocation( DNBBasicEntity3D::Handle hOther ) const
        DNB_THROW_SPEC_NULL;

    RequestStatus
    setBaseProfile( const DNBProfile3D &relativeProfile,
        DNBBasicEntity3D::Handle hOther )
        DNB_THROW_SPEC_NULL;

    DNBProfile3D
    getBaseProfile( DNBBasicEntity3D::Handle hOther ) const
        DNB_THROW_SPEC_NULL;

    //
    //  The following methods perform instantaneous translations and rotations
    //  on the base frame of <self>.
    //
    RequestStatus
    translateBase( const DNBVector3D &deltaP,
        DNBMath3D::ReferenceFrame refFrame = DNBMath3D::Entity )
        DNB_THROW_SPEC_NULL;

    RequestStatus
    translateBase( const DNBVector3D &deltaP, DNBBasicEntity3D::Handle hOther )
        DNB_THROW_SPEC_NULL;

    RequestStatus
    rotateBase( DNBMath3D::AxisType axis, DNBReal angle,
        DNBMath3D::ReferenceFrame refFrame = DNBMath3D::Entity )
        DNB_THROW_SPEC_NULL;

    RequestStatus
    rotateBase( DNBMath3D::AxisType axis, DNBReal angle,
        DNBBasicEntity3D::Handle hOther )
        DNB_THROW_SPEC_NULL;

    RequestStatus
    rotateBase( const DNBVector3D &kVector, DNBReal angle,
        DNBMath3D::ReferenceFrame refFrame = DNBMath3D::Entity )
        DNB_THROW_SPEC((DNBEZeroVector));

    RequestStatus
    rotateBase( const DNBVector3D &kVector, DNBReal angle,
        DNBBasicEntity3D::Handle hOther )
        DNB_THROW_SPEC((DNBEZeroVector));

#if 0
    RequestStatus
    rotateBase( DNBMath3D::EulerSystem system, DNBReal angle1, DNBReal angle2,
        DNBReal angle3, DNBMath3D::ReferenceFrame refFrame = DNBMath3D::Entity )
        DNB_THROW_SPEC_NULL;

    RequestStatus
    rotateBase( DNBMath3D::EulerSystem system, DNBReal angle1, DNBReal angle2,
        DNBReal angle3, DNBBasicEntity3D::Handle hOther )
        DNB_THROW_SPEC_NULL;
#endif

    RequestStatus
    moveBase( const DNBXform3D &deltaX,
        DNBMath3D::ReferenceFrame refFrame = DNBMath3D::Entity )
        DNB_THROW_SPEC_NULL;

    RequestStatus
    moveBase( const DNBXform3D &deltaX, DNBBasicEntity3D::Handle hOther )
        DNB_THROW_SPEC_NULL;

    void
    flushBaseLocation( )
        DNB_THROW_SPEC_NULL;

    void
    flushSpatialState( )
        DNB_THROW_SPEC_NULL;

    void
    cancelSpatialState( )
        DNB_THROW_SPEC_NULL;

    enum Events
    {
        EventFirst = DNBBasicEntity::EventLast - 1,
        EventSpatialAncestorsModified,
        EventRelativeLocationModified,  // Instantaneous rel change in location
        EventBaseLocationModified,      // Instantaneous change in location
        EventAncestorRelativeLocationModified,  // propagated rel change
        EventRelativeProfileModified,   // Instantaneous rel change in profile
        EventBaseProfileModified,       // Complete profile modified
        EventAncestorRelativeProfileModified,  // propagated rel change
        EventLast
    };

protected:
    //
    //  By defining the following methods in the protected section, this class
    //  can only be used as a base class.
    //
    DNBBasicEntity3D( )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit, DNBENetworkError));

    DNBBasicEntity3D( const DNBBasicEntity3D &right, CopyMode mode )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEResourceLimit, DNBENetworkError));

    virtual
    ~DNBBasicEntity3D( )
        DNB_THROW_SPEC_NULL;

    //
    //  The following methods compute the current profile of <self> with respect
    //  to the world.  They are called primarily by the spatial parent in the
    //  routines propagateBaseLocation() and propagateBaseProfile().
    //
    void
    updateParentLocation( const DNBXform3D &parentLocation )
        DNB_THROW_SPEC_NULL;

    void
    updateParentProfile( const DNBProfile3D &parentProfile )
        DNB_THROW_SPEC_NULL;

    //
    //  The following methods compute the current profile of <self> with respect
    //  to the world.  They are called primarily by the derived classes after
    //  invoking the methods setRelativeLocation() and setRelativeProfile().
    //
    void
    updateRelativeLocation( const DNBXform3D &relativeLocation )
        DNB_THROW_SPEC_NULL;

    void
    updateRelativeProfile( const DNBProfile3D &relativeProfile )
        DNB_THROW_SPEC_NULL;

    //
    //  The following methods are used to set/get the current profile of <self>
    //  with respect to its spatial parent.  These methods must be overridden
    //  in each derived class.  Note: The client code must recompute the base
    //  profile after calling one of the set methods.
    //
    virtual RequestStatus
    setRelativeLocation( DNBXform3D &relativeLocation )
        DNB_THROW_SPEC_NULL = 0;

    virtual DNBXform3D
    getRelativeLocation( ) const
        DNB_THROW_SPEC_NULL = 0;

    virtual RequestStatus
    setRelativeProfile( DNBProfile3D &relativeProfile )
        DNB_THROW_SPEC_NULL = 0;

    virtual DNBProfile3D
    getRelativeProfile( ) const
        DNB_THROW_SPEC_NULL = 0;

    //
    //  The following methods propagate spatial changes to the children of
    //  <self>.  This propagation automatically occurs at the end of a write
    //  transaction.  However, it can be performed during a transaction by
    //  calling flushSpatialState().  The methods may be overridden in derived
    //  classes to implement special update protocols (such as those required
    //  by an assembly or device).
    //
    virtual void
    propagateBaseLocation( ) const
        DNB_THROW_SPEC_NULL;

    virtual void
    propagateBaseProfile( ) const
        DNB_THROW_SPEC_NULL;

    virtual void
    propagateSpatialAncestors( ) const
        DNB_THROW_SPEC_NULL;

    //
    //  The following methods manage the spatial entity hierarchy.
    //
    void
    enablePropagateLocation( )
        DNB_THROW_SPEC_NULL;

    void
    addSpatialChild( DNBBasicEntity3D::Handle hChild )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEAlreadyRegistered));

    void
    removeSpatialChild( DNBBasicEntity3D::Handle hChild )
        DNB_THROW_SPEC((DNBENotFound));

    void
    removeSpatialChildren( const DNBClassInfo &classInfo )
        DNB_THROW_SPEC_NULL;

    void
    removeSpatialChildren( )
        DNB_THROW_SPEC_NULL;

    void
    destroySpatialChildren( )
        DNB_THROW_SPEC_NULL;

    void
    replaceSpatialChild( DNBBasicEntity3D::Handle hOldChild,
        DNBBasicEntity3D::Handle hNewChild )
        DNB_THROW_SPEC((scl_bad_alloc, DNBENotFound, DNBEAlreadyRegistered));

    void
    addSpatialParent( DNBBasicEntity3D::Handle hParent )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEAlreadyRegistered));

    void
    removeSpatialParent( )
        DNB_THROW_SPEC_NULL;

private:
    typedef DNBBasicEntity3D::List              SpatialAncestorDB;

    typedef SpatialAncestorDB::iterator         SpatialAncestorIterator;

    typedef SpatialAncestorDB::const_iterator   SpatialAncestorConstIterator;

    typedef DNBBasicEntity3D::List              SpatialChildDB;

    typedef SpatialChildDB::iterator            SpatialChildIterator;

    typedef SpatialChildDB::const_iterator      SpatialChildConstIterator;

    void
    setCachedBaseLocation( const DNBXform3D &baseLocation )
        DNB_THROW_SPEC_NULL;

    const DNBXform3D &
    getCachedBaseLocation( ) const
        DNB_THROW_SPEC_NULL;

    void
    setCachedRelativeLocation( const DNBXform3D &relLocation )
        DNB_THROW_SPEC_NULL;

    const DNBXform3D &
    getCachedRelativeLocation( ) const
        DNB_THROW_SPEC_NULL;

    void
    setCachedBaseProfile( const DNBProfile3D &baseProfile )
        DNB_THROW_SPEC_NULL;

    const DNBProfile3D &
    getCachedBaseProfile( ) const
        DNB_THROW_SPEC_NULL;

    void
    updateSpatialAncestors( const SpatialAncestorDB &parentAncestors )
        DNB_THROW_SPEC((scl_bad_alloc));

    void
    clearSpatialAncestors( )
        DNB_THROW_SPEC_NULL;

    SpatialChildIterator
    findSpatialChild( const DNBBasicEntity3D::Handle &hChild )
        DNB_THROW_SPEC((DNBENotFound));

    void
    setSpatialParent( const DNBBasicEntity3D::Handle &hParent,
        const DNBXform3D &parentLocation,
        const SpatialAncestorDB &parentAncestors )
        DNB_THROW_SPEC((scl_bad_alloc, DNBEAlreadyRegistered));

    void
    setSpatialChild( const DNBBasicEntity3D::Handle &hChild )
        DNB_THROW_SPEC((scl_bad_alloc));

    void
    setSpatialParentCache( const DNBBasicDataCache::Handle &hCache )
        DNB_THROW_SPEC_NULL;

    void
    clearSpatialParent( bool reposition )
        DNB_THROW_SPEC_NULL;

    void
    clearSpatialChild( SpatialChildIterator iChild )
        DNB_THROW_SPEC_NULL;

    void
    onNotification( )
        DNB_THROW_SPEC_NULL;

    static  DNBXform3D
    calcBaseLocation( const DNBXform3D &parentLocation,
        const DNBXform3D &relativeLocation )
        DNB_THROW_SPEC_NULL;

    static  DNBProfile3D
    calcBaseProfile( const DNBProfile3D &parentProfile,
        const DNBProfile3D &relativeProfile )
        DNB_THROW_SPEC_NULL;

    static  DNBXform3D
    calcRelativeLocation( const DNBXform3D &parentLocation,
        const DNBXform3D &baseLocation )
        DNB_THROW_SPEC_NULL;

    static  DNBProfile3D
    calcRelativeProfile( const DNBProfile3D &parentProfile,
        const DNBProfile3D &baseProfile )
        DNB_THROW_SPEC_NULL;

    DNBXform3D
    getEntityLocation( DNBBasicEntity3D::Handle &hOther ) const
        DNB_THROW_SPEC_NULL;

    DNBProfile3D
    getEntityProfile( DNBBasicEntity3D::Handle &hOther ) const
        DNB_THROW_SPEC_NULL;

    bool
    isThereASpatialDescendant( DNBBasicEntity3D::Handle TopNode,
                               DNBBasicEntity3D::Handle hDescendant ) const
        DNB_THROW_SPEC_NULL;

    size_t
    getALLSpatialDescendants( DNBBasicEntity3D::Handle TopNode,
                              DNBBasicEntity3D::List   &gLDescendant,
                              DNBBasicEntity3D::Vector &gVDescendant,
                              char type = 'N' ) const
        DNB_THROW_SPEC((scl_bad_alloc));

    DNBBasicEntity3D::Handle
    returnSpatialDescendant( DNBBasicEntity3D::Handle TopNode,
                             const DNBClassInfo &classInfo ) const
        DNB_THROW_SPEC_NULL;

    bool
    locateSpatialDescendant( DNBBasicEntity3D::Handle TopNode,
                             DNBBasicEntity3D::Handle hDescendant ) const
        DNB_THROW_SPEC_NULL;

    DNBProfile3D        baseProfile_;           // Profile of <self> wrt WCS
    DNBProfile3D        relProfile_;           
    DNBProfile3D        spatialParentProfile_;  // Profile of spatial parent
    Handle              hSpatialParent_;        // Handle to spatial parent
    SpatialAncestorDB   spatialAncestorDB_;     // Database of spatial ancestors
    SpatialChildDB      spatialChildDB_;        // Database of spatial children
    DNBBasicDataCache::Handle hCache_;
    DNBBasicDataCache::Handle hSpatialParentCache_;
    bool                      propagateEnabled_;

    friend 	class   DNBBasicDevice3D;
    friend 	class   DNBDesignLocExt;
};

#endif  /* _DNB_BASICENTITY3D_H_ */
