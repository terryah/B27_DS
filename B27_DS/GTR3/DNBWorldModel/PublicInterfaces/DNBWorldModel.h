//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/** @CAA2Required */
/*---------------------------------------------------------------------*/
/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */
/*---------------------------------------------------------------------*/
//
//    This Header file is included by all the Module.h header files in 
//    PublicInterfaces
//

#ifndef DNBWORLDMODEL_H

#define DNBWORLDMODEL_H DNBWorldModel

#ifdef _WINDOWS_SOURCE
#if defined(__DNBWorldModel)
#define ExportedByDNBWorldModel __declspec(dllexport)
#else
#define ExportedByDNBWorldModel __declspec(dllimport)
#endif
#else
#define ExportedByDNBWorldModel
#endif

#endif /* DNBWORLDMODEL_H */
