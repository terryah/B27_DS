// COPYRIGHT DASSAULT SYSTEMES 2005
/** @CAA2Required */
/**********************************************************************/
/* DON T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPEAR AT ANY TIME */
/**********************************************************************/

/**
 * @fullreview AZH AZI 06:01:18
*/
#ifdef  _WINDOWS_SOURCE
#ifdef  __AUTLcmEditorItfCPP
#define ExportedByAUTLcmEditorItfCPP     __declspec(dllexport)
#else
#define ExportedByAUTLcmEditorItfCPP     __declspec(dllimport)
#endif
#else
#define ExportedByAUTLcmEditorItfCPP
#endif
