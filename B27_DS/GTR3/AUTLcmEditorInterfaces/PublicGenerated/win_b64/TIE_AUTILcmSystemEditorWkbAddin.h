#ifndef __TIE_AUTILcmSystemEditorWkbAddin
#define __TIE_AUTILcmSystemEditorWkbAddin

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "AUTILcmSystemEditorWkbAddin.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface AUTILcmSystemEditorWkbAddin */
#define declare_TIE_AUTILcmSystemEditorWkbAddin(classe) \
 \
 \
class TIEAUTILcmSystemEditorWkbAddin##classe : public AUTILcmSystemEditorWkbAddin \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(AUTILcmSystemEditorWkbAddin, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual void              CreateCommands () ; \
      virtual CATCmdContainer * CreateToolbars () ; \
};



#define ENVTIEdeclare_AUTILcmSystemEditorWkbAddin(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual void              CreateCommands () ; \
virtual CATCmdContainer * CreateToolbars () ; \


#define ENVTIEdefine_AUTILcmSystemEditorWkbAddin(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
void               ENVTIEName::CreateCommands ()  \
{ \
 (ENVTIECALL(AUTILcmSystemEditorWkbAddin,ENVTIETypeLetter,ENVTIELetter)CreateCommands ()); \
} \
CATCmdContainer *  ENVTIEName::CreateToolbars ()  \
{ \
return (ENVTIECALL(AUTILcmSystemEditorWkbAddin,ENVTIETypeLetter,ENVTIELetter)CreateToolbars ()); \
} \


/* Name of the TIE class */
#define class_TIE_AUTILcmSystemEditorWkbAddin(classe)    TIEAUTILcmSystemEditorWkbAddin##classe


/* Common methods inside a TIE */
#define common_TIE_AUTILcmSystemEditorWkbAddin(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(AUTILcmSystemEditorWkbAddin, classe) \
 \
 \
CATImplementTIEMethods(AUTILcmSystemEditorWkbAddin, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(AUTILcmSystemEditorWkbAddin, classe, 1) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(AUTILcmSystemEditorWkbAddin, classe) \
CATImplementCATBaseUnknownMethodsForTIE(AUTILcmSystemEditorWkbAddin, classe) \
 \
void               TIEAUTILcmSystemEditorWkbAddin##classe::CreateCommands ()  \
{ \
   ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateCommands (); \
} \
CATCmdContainer *  TIEAUTILcmSystemEditorWkbAddin##classe::CreateToolbars ()  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateToolbars ()); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_AUTILcmSystemEditorWkbAddin(classe) \
 \
 \
declare_TIE_AUTILcmSystemEditorWkbAddin(classe) \
 \
 \
CATMetaClass * __stdcall TIEAUTILcmSystemEditorWkbAddin##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_AUTILcmSystemEditorWkbAddin,"AUTILcmSystemEditorWkbAddin",AUTILcmSystemEditorWkbAddin::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_AUTILcmSystemEditorWkbAddin(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(AUTILcmSystemEditorWkbAddin, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicAUTILcmSystemEditorWkbAddin##classe(classe::MetaObject(),AUTILcmSystemEditorWkbAddin::MetaObject(),(void *)CreateTIEAUTILcmSystemEditorWkbAddin##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_AUTILcmSystemEditorWkbAddin(classe) \
 \
 \
declare_TIE_AUTILcmSystemEditorWkbAddin(classe) \
 \
 \
CATMetaClass * __stdcall TIEAUTILcmSystemEditorWkbAddin##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_AUTILcmSystemEditorWkbAddin,"AUTILcmSystemEditorWkbAddin",AUTILcmSystemEditorWkbAddin::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_AUTILcmSystemEditorWkbAddin(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(AUTILcmSystemEditorWkbAddin, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicAUTILcmSystemEditorWkbAddin##classe(classe::MetaObject(),AUTILcmSystemEditorWkbAddin::MetaObject(),(void *)CreateTIEAUTILcmSystemEditorWkbAddin##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_AUTILcmSystemEditorWkbAddin(classe) TIE_AUTILcmSystemEditorWkbAddin(classe)
#else
#define BOA_AUTILcmSystemEditorWkbAddin(classe) CATImplementBOA(AUTILcmSystemEditorWkbAddin, classe)
#endif

#endif
