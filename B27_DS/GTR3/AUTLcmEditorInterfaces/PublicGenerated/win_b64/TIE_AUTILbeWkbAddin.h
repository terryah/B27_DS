#ifndef __TIE_AUTILbeWkbAddin
#define __TIE_AUTILbeWkbAddin

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "AUTILbeWkbAddin.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface AUTILbeWkbAddin */
#define declare_TIE_AUTILbeWkbAddin(classe) \
 \
 \
class TIEAUTILbeWkbAddin##classe : public AUTILbeWkbAddin \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(AUTILbeWkbAddin, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual void              CreateCommands () ; \
      virtual CATCmdContainer * CreateToolbars () ; \
};



#define ENVTIEdeclare_AUTILbeWkbAddin(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual void              CreateCommands () ; \
virtual CATCmdContainer * CreateToolbars () ; \


#define ENVTIEdefine_AUTILbeWkbAddin(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
void               ENVTIEName::CreateCommands ()  \
{ \
 (ENVTIECALL(AUTILbeWkbAddin,ENVTIETypeLetter,ENVTIELetter)CreateCommands ()); \
} \
CATCmdContainer *  ENVTIEName::CreateToolbars ()  \
{ \
return (ENVTIECALL(AUTILbeWkbAddin,ENVTIETypeLetter,ENVTIELetter)CreateToolbars ()); \
} \


/* Name of the TIE class */
#define class_TIE_AUTILbeWkbAddin(classe)    TIEAUTILbeWkbAddin##classe


/* Common methods inside a TIE */
#define common_TIE_AUTILbeWkbAddin(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(AUTILbeWkbAddin, classe) \
 \
 \
CATImplementTIEMethods(AUTILbeWkbAddin, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(AUTILbeWkbAddin, classe, 1) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(AUTILbeWkbAddin, classe) \
CATImplementCATBaseUnknownMethodsForTIE(AUTILbeWkbAddin, classe) \
 \
void               TIEAUTILbeWkbAddin##classe::CreateCommands ()  \
{ \
   ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateCommands (); \
} \
CATCmdContainer *  TIEAUTILbeWkbAddin##classe::CreateToolbars ()  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateToolbars ()); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_AUTILbeWkbAddin(classe) \
 \
 \
declare_TIE_AUTILbeWkbAddin(classe) \
 \
 \
CATMetaClass * __stdcall TIEAUTILbeWkbAddin##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_AUTILbeWkbAddin,"AUTILbeWkbAddin",AUTILbeWkbAddin::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_AUTILbeWkbAddin(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(AUTILbeWkbAddin, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicAUTILbeWkbAddin##classe(classe::MetaObject(),AUTILbeWkbAddin::MetaObject(),(void *)CreateTIEAUTILbeWkbAddin##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_AUTILbeWkbAddin(classe) \
 \
 \
declare_TIE_AUTILbeWkbAddin(classe) \
 \
 \
CATMetaClass * __stdcall TIEAUTILbeWkbAddin##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_AUTILbeWkbAddin,"AUTILbeWkbAddin",AUTILbeWkbAddin::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_AUTILbeWkbAddin(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(AUTILbeWkbAddin, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicAUTILbeWkbAddin##classe(classe::MetaObject(),AUTILbeWkbAddin::MetaObject(),(void *)CreateTIEAUTILbeWkbAddin##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_AUTILbeWkbAddin(classe) TIE_AUTILbeWkbAddin(classe)
#else
#define BOA_AUTILbeWkbAddin(classe) CATImplementBOA(AUTILbeWkbAddin, classe)
#endif

#endif
