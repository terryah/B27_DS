# COPYRIGHT DASSAULT SYSTEMES 2001
#======================================================================
# Imakefile for module DNBPertItf.m 
#======================================================================
#
#  Feb 2001  Creation: Code generated by the CAA wizard  aditya
#======================================================================
#
# SHARED LIBRARY 
#
BUILT_OBJECT_TYPE=SHARED LIBRARY
 
# DO NOT EDIT :: THE CAA2 WIZARDS WILL ADD CODE HERE
WIZARD_LINK_MODULES =  \
JS0INF
# END WIZARD EDITION ZONE

LINK_WITH = $(WIZARD_LINK_MODULES)

INCLUDED_MODULES = \
                  DNBPertItfCPP \
                  DNBPertPubIDL \
                  DNBPertProIDL \


OS = Windows_NT
#LOCAL_CCFLAGS=-FR
