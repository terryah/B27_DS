# COPYRIGHT DASSAULT SYSTEMES/DELMIA CORP 2005
#-------------------------------------------------------------------------------
# File Type  :
#              Imakefile.mk
#
# Description:
#-------------------------------------------------------------------------------
# mm-dd-yyyy  Change                                                         Tri
# 11-15-2005  Cre                                                            rtt
#-------------------------------------------------------------------------------
BUILT_OBJECT_TYPE=TYPELIB
# no more BUILD_PRIORITY=4

LINK_WITH = InfTypeLib ProcessTypeLib


# Define the build options for the current module.

OS=Windows_NT
BUILD=YES

OS=IRIX
BUILD=YES

OS=SunOS
BUILD=YES

OS=AIX
BUILD=YES

OS=HP-UX
BUILD=YES

OS=win_a
BUILD=NO


#INSERTION ZONE NOT FOUND, MOVE AND APPEND THIS VARIABLE IN YOUR LINK STATEMENT
#LINK_WITH = ... $(WIZARD_LINK_MODULES) ...
# DO NOT EDIT :: THE CAA2 WIZARDS WILL ADD CODE HERE
WIZARD_LINK_MODULES =  \
JS0GROUP JS0FM JS0GROUP 
# END WIZARD EDITION ZONE
