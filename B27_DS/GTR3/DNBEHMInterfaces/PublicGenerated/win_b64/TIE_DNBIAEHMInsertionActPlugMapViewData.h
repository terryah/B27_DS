#ifndef __TIE_DNBIAEHMInsertionActPlugMapViewData
#define __TIE_DNBIAEHMInsertionActPlugMapViewData

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIAEHMInsertionActPlugMapViewData.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIAEHMInsertionActPlugMapViewData */
#define declare_TIE_DNBIAEHMInsertionActPlugMapViewData(classe) \
 \
 \
class TIEDNBIAEHMInsertionActPlugMapViewData##classe : public DNBIAEHMInsertionActPlugMapViewData \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIAEHMInsertionActPlugMapViewData, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall get_ConnectorName(CATBSTR & ioConnectorName); \
      virtual HRESULT __stdcall get_ConnectorPartNumber(CATBSTR & ioConnectorPartNumber); \
      virtual HRESULT __stdcall get_ConnectorRefDesignator(CATBSTR & ioConnectorRefDesignator); \
      virtual HRESULT __stdcall get_ConnectorType(CATBSTR & ioConnectorType); \
      virtual HRESULT __stdcall get_ConnectorSubType(CATBSTR & ioConnectorSubType); \
      virtual HRESULT __stdcall get_NumInsertedWires(CATLONG & oNumInsertedWires); \
      virtual HRESULT __stdcall put_WireID(CATLONG iWireID); \
      virtual HRESULT __stdcall get_TerminationName(CATBSTR & ioTerminationName); \
      virtual HRESULT __stdcall get_TerminationNumber(CATBSTR & ioTerminationNumber); \
      virtual HRESULT __stdcall get_TerminationIDNumber(CATBSTR & ioTerminationIDNumber); \
      virtual HRESULT __stdcall get_TerminationRefDesignator(CATBSTR & ioTerminationRefDesignator); \
      virtual HRESULT __stdcall get_TerminationType(CATBSTR & ioTerminationType); \
      virtual HRESULT __stdcall get_TerminationSubType(CATBSTR & ioTerminationSubType); \
      virtual HRESULT __stdcall get_WireName(CATBSTR & ioWireName); \
      virtual HRESULT __stdcall get_WirePartNumber(CATBSTR & ioWirePartNumber); \
      virtual HRESULT __stdcall get_WireRefDesignator(CATBSTR & ioWireRefDesignator); \
      virtual HRESULT __stdcall get_WireType(CATBSTR & ioWireType); \
      virtual HRESULT __stdcall get_WireSubType(CATBSTR & ioWireSubType); \
      virtual HRESULT __stdcall get_WireDiameter(double & oWireDiameter); \
      virtual HRESULT __stdcall get_WireLength(double & oWireLength); \
      virtual HRESULT __stdcall get_WireColor(CATBSTR & ioWireColor); \
      virtual HRESULT __stdcall get_WireSeparationCode(CATBSTR & ioWireSeparationCode); \
      virtual HRESULT __stdcall get_WireSignalID(CATBSTR & ioWireSignalID); \
      virtual HRESULT __stdcall get_ContactName(CATBSTR & ioContactName); \
      virtual HRESULT __stdcall get_ContactPartNumber(CATBSTR & ioContactPartNumber); \
      virtual HRESULT __stdcall get_ContactRefDesignator(CATBSTR & ioContactRefDesignator); \
      virtual HRESULT __stdcall get_ContactType(CATBSTR & ioContactType); \
      virtual HRESULT __stdcall get_ContactSubType(CATBSTR & ioContactSubType); \
      virtual HRESULT __stdcall get_ContactElecBarrelDiameter(double & oContactElecBarrelDiameter); \
      virtual HRESULT __stdcall IsSubTypeOf(const CATBSTR & iName, CAT_VARIANT_BOOL & oVal); \
      virtual HRESULT __stdcall AttrValue(const CATVariant & iIndex, CATVariant & oAttVal); \
      virtual HRESULT __stdcall AttrName(CATLONG iIndex, CATBSTR & oName); \
      virtual HRESULT __stdcall get_Type(CATBSTR & oType); \
      virtual HRESULT __stdcall get_Description(CATBSTR & oDescriptionBSTR); \
      virtual HRESULT __stdcall put_Description(const CATBSTR & iDescriptionBSTR); \
      virtual HRESULT __stdcall get_CycleTime(double & oCT); \
      virtual HRESULT __stdcall put_CycleTime(double iCT); \
      virtual HRESULT __stdcall get_CalculatedCycleTime(double & oCCT); \
      virtual HRESULT __stdcall get_BeginningDate(double & oBegin); \
      virtual HRESULT __stdcall put_BeginningDate(double iSBT); \
      virtual HRESULT __stdcall get_EndDate(double & oEnd); \
      virtual HRESULT __stdcall get_ChildrenActivities(CATIAActivities *& oChildren); \
      virtual HRESULT __stdcall CreateChild(const CATBSTR & iTypeOfChild, CATIAActivity *& oCreatedChild); \
      virtual HRESULT __stdcall CreateLink(CATIAActivity * iSecondActivity); \
      virtual HRESULT __stdcall RemoveLink(CATIAActivity * iSecondActivity); \
      virtual HRESULT __stdcall get_NextCFActivities(CATIAActivities *& oNextCF); \
      virtual HRESULT __stdcall get_PreviousCFActivities(CATIAActivities *& oPreviousCF); \
      virtual HRESULT __stdcall get_NextPRFActivities(CATIAActivities *& oNextPRF); \
      virtual HRESULT __stdcall get_PreviousPRFActivities(CATIAActivities *& oPreviousPRF); \
      virtual HRESULT __stdcall get_AttrCount(CATLONG & oNbAttr); \
      virtual HRESULT __stdcall get_Items(CATIAItems *& oItems); \
      virtual HRESULT __stdcall get_Outputs(CATIAOutputs *& oOutputs); \
      virtual HRESULT __stdcall get_Resources(CATIAResources *& oResources); \
      virtual HRESULT __stdcall get_Relations(CATIARelations *& oRelations); \
      virtual HRESULT __stdcall get_Parameters(CATIAParameters *& oParameters); \
      virtual HRESULT __stdcall GetTechnologicalObject(const CATBSTR & iApplicationType, CATBaseDispatch *& oApplicativeObj); \
      virtual HRESULT __stdcall get_PrecedenceActivities(CATIAActivities *& oActivities); \
      virtual HRESULT __stdcall get_PossiblePrecedenceActivities(CATIAActivities *& oActivities); \
      virtual HRESULT __stdcall get_ProcessID(CATBSTR & oProcessID); \
      virtual HRESULT __stdcall SetProcessID(const CATBSTR & iProcessID, CAT_VARIANT_BOOL iCheckUnique); \
      virtual HRESULT __stdcall get_CalculatedBeginTime(double & oCBT); \
      virtual HRESULT __stdcall AddAttr(const CATBSTR & iAttributeName, SPPProcessAttributeType AttrType, const CATBSTR & iAttributePromptName); \
      virtual HRESULT __stdcall RemoveAttr(const CATBSTR & iAttributeName); \
      virtual HRESULT __stdcall AddActivityConstraint(CATIAActivity * iActivity, SPPProcessConstraintType iConstraintType); \
      virtual HRESULT __stdcall RemoveActivityConstraint(CATIAActivity * iActivity, SPPProcessConstraintType iConstraintType); \
      virtual HRESULT __stdcall GetActivityConstraints(SPPProcessConstraintType iConstraintType, CATIAActivities *& oConstrtList); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIAEHMInsertionActPlugMapViewData(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall get_ConnectorName(CATBSTR & ioConnectorName); \
virtual HRESULT __stdcall get_ConnectorPartNumber(CATBSTR & ioConnectorPartNumber); \
virtual HRESULT __stdcall get_ConnectorRefDesignator(CATBSTR & ioConnectorRefDesignator); \
virtual HRESULT __stdcall get_ConnectorType(CATBSTR & ioConnectorType); \
virtual HRESULT __stdcall get_ConnectorSubType(CATBSTR & ioConnectorSubType); \
virtual HRESULT __stdcall get_NumInsertedWires(CATLONG & oNumInsertedWires); \
virtual HRESULT __stdcall put_WireID(CATLONG iWireID); \
virtual HRESULT __stdcall get_TerminationName(CATBSTR & ioTerminationName); \
virtual HRESULT __stdcall get_TerminationNumber(CATBSTR & ioTerminationNumber); \
virtual HRESULT __stdcall get_TerminationIDNumber(CATBSTR & ioTerminationIDNumber); \
virtual HRESULT __stdcall get_TerminationRefDesignator(CATBSTR & ioTerminationRefDesignator); \
virtual HRESULT __stdcall get_TerminationType(CATBSTR & ioTerminationType); \
virtual HRESULT __stdcall get_TerminationSubType(CATBSTR & ioTerminationSubType); \
virtual HRESULT __stdcall get_WireName(CATBSTR & ioWireName); \
virtual HRESULT __stdcall get_WirePartNumber(CATBSTR & ioWirePartNumber); \
virtual HRESULT __stdcall get_WireRefDesignator(CATBSTR & ioWireRefDesignator); \
virtual HRESULT __stdcall get_WireType(CATBSTR & ioWireType); \
virtual HRESULT __stdcall get_WireSubType(CATBSTR & ioWireSubType); \
virtual HRESULT __stdcall get_WireDiameter(double & oWireDiameter); \
virtual HRESULT __stdcall get_WireLength(double & oWireLength); \
virtual HRESULT __stdcall get_WireColor(CATBSTR & ioWireColor); \
virtual HRESULT __stdcall get_WireSeparationCode(CATBSTR & ioWireSeparationCode); \
virtual HRESULT __stdcall get_WireSignalID(CATBSTR & ioWireSignalID); \
virtual HRESULT __stdcall get_ContactName(CATBSTR & ioContactName); \
virtual HRESULT __stdcall get_ContactPartNumber(CATBSTR & ioContactPartNumber); \
virtual HRESULT __stdcall get_ContactRefDesignator(CATBSTR & ioContactRefDesignator); \
virtual HRESULT __stdcall get_ContactType(CATBSTR & ioContactType); \
virtual HRESULT __stdcall get_ContactSubType(CATBSTR & ioContactSubType); \
virtual HRESULT __stdcall get_ContactElecBarrelDiameter(double & oContactElecBarrelDiameter); \
virtual HRESULT __stdcall IsSubTypeOf(const CATBSTR & iName, CAT_VARIANT_BOOL & oVal); \
virtual HRESULT __stdcall AttrValue(const CATVariant & iIndex, CATVariant & oAttVal); \
virtual HRESULT __stdcall AttrName(CATLONG iIndex, CATBSTR & oName); \
virtual HRESULT __stdcall get_Type(CATBSTR & oType); \
virtual HRESULT __stdcall get_Description(CATBSTR & oDescriptionBSTR); \
virtual HRESULT __stdcall put_Description(const CATBSTR & iDescriptionBSTR); \
virtual HRESULT __stdcall get_CycleTime(double & oCT); \
virtual HRESULT __stdcall put_CycleTime(double iCT); \
virtual HRESULT __stdcall get_CalculatedCycleTime(double & oCCT); \
virtual HRESULT __stdcall get_BeginningDate(double & oBegin); \
virtual HRESULT __stdcall put_BeginningDate(double iSBT); \
virtual HRESULT __stdcall get_EndDate(double & oEnd); \
virtual HRESULT __stdcall get_ChildrenActivities(CATIAActivities *& oChildren); \
virtual HRESULT __stdcall CreateChild(const CATBSTR & iTypeOfChild, CATIAActivity *& oCreatedChild); \
virtual HRESULT __stdcall CreateLink(CATIAActivity * iSecondActivity); \
virtual HRESULT __stdcall RemoveLink(CATIAActivity * iSecondActivity); \
virtual HRESULT __stdcall get_NextCFActivities(CATIAActivities *& oNextCF); \
virtual HRESULT __stdcall get_PreviousCFActivities(CATIAActivities *& oPreviousCF); \
virtual HRESULT __stdcall get_NextPRFActivities(CATIAActivities *& oNextPRF); \
virtual HRESULT __stdcall get_PreviousPRFActivities(CATIAActivities *& oPreviousPRF); \
virtual HRESULT __stdcall get_AttrCount(CATLONG & oNbAttr); \
virtual HRESULT __stdcall get_Items(CATIAItems *& oItems); \
virtual HRESULT __stdcall get_Outputs(CATIAOutputs *& oOutputs); \
virtual HRESULT __stdcall get_Resources(CATIAResources *& oResources); \
virtual HRESULT __stdcall get_Relations(CATIARelations *& oRelations); \
virtual HRESULT __stdcall get_Parameters(CATIAParameters *& oParameters); \
virtual HRESULT __stdcall GetTechnologicalObject(const CATBSTR & iApplicationType, CATBaseDispatch *& oApplicativeObj); \
virtual HRESULT __stdcall get_PrecedenceActivities(CATIAActivities *& oActivities); \
virtual HRESULT __stdcall get_PossiblePrecedenceActivities(CATIAActivities *& oActivities); \
virtual HRESULT __stdcall get_ProcessID(CATBSTR & oProcessID); \
virtual HRESULT __stdcall SetProcessID(const CATBSTR & iProcessID, CAT_VARIANT_BOOL iCheckUnique); \
virtual HRESULT __stdcall get_CalculatedBeginTime(double & oCBT); \
virtual HRESULT __stdcall AddAttr(const CATBSTR & iAttributeName, SPPProcessAttributeType AttrType, const CATBSTR & iAttributePromptName); \
virtual HRESULT __stdcall RemoveAttr(const CATBSTR & iAttributeName); \
virtual HRESULT __stdcall AddActivityConstraint(CATIAActivity * iActivity, SPPProcessConstraintType iConstraintType); \
virtual HRESULT __stdcall RemoveActivityConstraint(CATIAActivity * iActivity, SPPProcessConstraintType iConstraintType); \
virtual HRESULT __stdcall GetActivityConstraints(SPPProcessConstraintType iConstraintType, CATIAActivities *& oConstrtList); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIAEHMInsertionActPlugMapViewData(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::get_ConnectorName(CATBSTR & ioConnectorName) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)get_ConnectorName(ioConnectorName)); \
} \
HRESULT __stdcall  ENVTIEName::get_ConnectorPartNumber(CATBSTR & ioConnectorPartNumber) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)get_ConnectorPartNumber(ioConnectorPartNumber)); \
} \
HRESULT __stdcall  ENVTIEName::get_ConnectorRefDesignator(CATBSTR & ioConnectorRefDesignator) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)get_ConnectorRefDesignator(ioConnectorRefDesignator)); \
} \
HRESULT __stdcall  ENVTIEName::get_ConnectorType(CATBSTR & ioConnectorType) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)get_ConnectorType(ioConnectorType)); \
} \
HRESULT __stdcall  ENVTIEName::get_ConnectorSubType(CATBSTR & ioConnectorSubType) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)get_ConnectorSubType(ioConnectorSubType)); \
} \
HRESULT __stdcall  ENVTIEName::get_NumInsertedWires(CATLONG & oNumInsertedWires) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)get_NumInsertedWires(oNumInsertedWires)); \
} \
HRESULT __stdcall  ENVTIEName::put_WireID(CATLONG iWireID) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)put_WireID(iWireID)); \
} \
HRESULT __stdcall  ENVTIEName::get_TerminationName(CATBSTR & ioTerminationName) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)get_TerminationName(ioTerminationName)); \
} \
HRESULT __stdcall  ENVTIEName::get_TerminationNumber(CATBSTR & ioTerminationNumber) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)get_TerminationNumber(ioTerminationNumber)); \
} \
HRESULT __stdcall  ENVTIEName::get_TerminationIDNumber(CATBSTR & ioTerminationIDNumber) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)get_TerminationIDNumber(ioTerminationIDNumber)); \
} \
HRESULT __stdcall  ENVTIEName::get_TerminationRefDesignator(CATBSTR & ioTerminationRefDesignator) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)get_TerminationRefDesignator(ioTerminationRefDesignator)); \
} \
HRESULT __stdcall  ENVTIEName::get_TerminationType(CATBSTR & ioTerminationType) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)get_TerminationType(ioTerminationType)); \
} \
HRESULT __stdcall  ENVTIEName::get_TerminationSubType(CATBSTR & ioTerminationSubType) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)get_TerminationSubType(ioTerminationSubType)); \
} \
HRESULT __stdcall  ENVTIEName::get_WireName(CATBSTR & ioWireName) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)get_WireName(ioWireName)); \
} \
HRESULT __stdcall  ENVTIEName::get_WirePartNumber(CATBSTR & ioWirePartNumber) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)get_WirePartNumber(ioWirePartNumber)); \
} \
HRESULT __stdcall  ENVTIEName::get_WireRefDesignator(CATBSTR & ioWireRefDesignator) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)get_WireRefDesignator(ioWireRefDesignator)); \
} \
HRESULT __stdcall  ENVTIEName::get_WireType(CATBSTR & ioWireType) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)get_WireType(ioWireType)); \
} \
HRESULT __stdcall  ENVTIEName::get_WireSubType(CATBSTR & ioWireSubType) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)get_WireSubType(ioWireSubType)); \
} \
HRESULT __stdcall  ENVTIEName::get_WireDiameter(double & oWireDiameter) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)get_WireDiameter(oWireDiameter)); \
} \
HRESULT __stdcall  ENVTIEName::get_WireLength(double & oWireLength) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)get_WireLength(oWireLength)); \
} \
HRESULT __stdcall  ENVTIEName::get_WireColor(CATBSTR & ioWireColor) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)get_WireColor(ioWireColor)); \
} \
HRESULT __stdcall  ENVTIEName::get_WireSeparationCode(CATBSTR & ioWireSeparationCode) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)get_WireSeparationCode(ioWireSeparationCode)); \
} \
HRESULT __stdcall  ENVTIEName::get_WireSignalID(CATBSTR & ioWireSignalID) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)get_WireSignalID(ioWireSignalID)); \
} \
HRESULT __stdcall  ENVTIEName::get_ContactName(CATBSTR & ioContactName) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)get_ContactName(ioContactName)); \
} \
HRESULT __stdcall  ENVTIEName::get_ContactPartNumber(CATBSTR & ioContactPartNumber) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)get_ContactPartNumber(ioContactPartNumber)); \
} \
HRESULT __stdcall  ENVTIEName::get_ContactRefDesignator(CATBSTR & ioContactRefDesignator) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)get_ContactRefDesignator(ioContactRefDesignator)); \
} \
HRESULT __stdcall  ENVTIEName::get_ContactType(CATBSTR & ioContactType) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)get_ContactType(ioContactType)); \
} \
HRESULT __stdcall  ENVTIEName::get_ContactSubType(CATBSTR & ioContactSubType) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)get_ContactSubType(ioContactSubType)); \
} \
HRESULT __stdcall  ENVTIEName::get_ContactElecBarrelDiameter(double & oContactElecBarrelDiameter) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)get_ContactElecBarrelDiameter(oContactElecBarrelDiameter)); \
} \
HRESULT __stdcall  ENVTIEName::IsSubTypeOf(const CATBSTR & iName, CAT_VARIANT_BOOL & oVal) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)IsSubTypeOf(iName,oVal)); \
} \
HRESULT __stdcall  ENVTIEName::AttrValue(const CATVariant & iIndex, CATVariant & oAttVal) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)AttrValue(iIndex,oAttVal)); \
} \
HRESULT __stdcall  ENVTIEName::AttrName(CATLONG iIndex, CATBSTR & oName) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)AttrName(iIndex,oName)); \
} \
HRESULT __stdcall  ENVTIEName::get_Type(CATBSTR & oType) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)get_Type(oType)); \
} \
HRESULT __stdcall  ENVTIEName::get_Description(CATBSTR & oDescriptionBSTR) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)get_Description(oDescriptionBSTR)); \
} \
HRESULT __stdcall  ENVTIEName::put_Description(const CATBSTR & iDescriptionBSTR) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)put_Description(iDescriptionBSTR)); \
} \
HRESULT __stdcall  ENVTIEName::get_CycleTime(double & oCT) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)get_CycleTime(oCT)); \
} \
HRESULT __stdcall  ENVTIEName::put_CycleTime(double iCT) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)put_CycleTime(iCT)); \
} \
HRESULT __stdcall  ENVTIEName::get_CalculatedCycleTime(double & oCCT) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)get_CalculatedCycleTime(oCCT)); \
} \
HRESULT __stdcall  ENVTIEName::get_BeginningDate(double & oBegin) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)get_BeginningDate(oBegin)); \
} \
HRESULT __stdcall  ENVTIEName::put_BeginningDate(double iSBT) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)put_BeginningDate(iSBT)); \
} \
HRESULT __stdcall  ENVTIEName::get_EndDate(double & oEnd) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)get_EndDate(oEnd)); \
} \
HRESULT __stdcall  ENVTIEName::get_ChildrenActivities(CATIAActivities *& oChildren) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)get_ChildrenActivities(oChildren)); \
} \
HRESULT __stdcall  ENVTIEName::CreateChild(const CATBSTR & iTypeOfChild, CATIAActivity *& oCreatedChild) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)CreateChild(iTypeOfChild,oCreatedChild)); \
} \
HRESULT __stdcall  ENVTIEName::CreateLink(CATIAActivity * iSecondActivity) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)CreateLink(iSecondActivity)); \
} \
HRESULT __stdcall  ENVTIEName::RemoveLink(CATIAActivity * iSecondActivity) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)RemoveLink(iSecondActivity)); \
} \
HRESULT __stdcall  ENVTIEName::get_NextCFActivities(CATIAActivities *& oNextCF) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)get_NextCFActivities(oNextCF)); \
} \
HRESULT __stdcall  ENVTIEName::get_PreviousCFActivities(CATIAActivities *& oPreviousCF) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)get_PreviousCFActivities(oPreviousCF)); \
} \
HRESULT __stdcall  ENVTIEName::get_NextPRFActivities(CATIAActivities *& oNextPRF) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)get_NextPRFActivities(oNextPRF)); \
} \
HRESULT __stdcall  ENVTIEName::get_PreviousPRFActivities(CATIAActivities *& oPreviousPRF) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)get_PreviousPRFActivities(oPreviousPRF)); \
} \
HRESULT __stdcall  ENVTIEName::get_AttrCount(CATLONG & oNbAttr) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)get_AttrCount(oNbAttr)); \
} \
HRESULT __stdcall  ENVTIEName::get_Items(CATIAItems *& oItems) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)get_Items(oItems)); \
} \
HRESULT __stdcall  ENVTIEName::get_Outputs(CATIAOutputs *& oOutputs) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)get_Outputs(oOutputs)); \
} \
HRESULT __stdcall  ENVTIEName::get_Resources(CATIAResources *& oResources) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)get_Resources(oResources)); \
} \
HRESULT __stdcall  ENVTIEName::get_Relations(CATIARelations *& oRelations) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)get_Relations(oRelations)); \
} \
HRESULT __stdcall  ENVTIEName::get_Parameters(CATIAParameters *& oParameters) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)get_Parameters(oParameters)); \
} \
HRESULT __stdcall  ENVTIEName::GetTechnologicalObject(const CATBSTR & iApplicationType, CATBaseDispatch *& oApplicativeObj) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)GetTechnologicalObject(iApplicationType,oApplicativeObj)); \
} \
HRESULT __stdcall  ENVTIEName::get_PrecedenceActivities(CATIAActivities *& oActivities) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)get_PrecedenceActivities(oActivities)); \
} \
HRESULT __stdcall  ENVTIEName::get_PossiblePrecedenceActivities(CATIAActivities *& oActivities) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)get_PossiblePrecedenceActivities(oActivities)); \
} \
HRESULT __stdcall  ENVTIEName::get_ProcessID(CATBSTR & oProcessID) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)get_ProcessID(oProcessID)); \
} \
HRESULT __stdcall  ENVTIEName::SetProcessID(const CATBSTR & iProcessID, CAT_VARIANT_BOOL iCheckUnique) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)SetProcessID(iProcessID,iCheckUnique)); \
} \
HRESULT __stdcall  ENVTIEName::get_CalculatedBeginTime(double & oCBT) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)get_CalculatedBeginTime(oCBT)); \
} \
HRESULT __stdcall  ENVTIEName::AddAttr(const CATBSTR & iAttributeName, SPPProcessAttributeType AttrType, const CATBSTR & iAttributePromptName) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)AddAttr(iAttributeName,AttrType,iAttributePromptName)); \
} \
HRESULT __stdcall  ENVTIEName::RemoveAttr(const CATBSTR & iAttributeName) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)RemoveAttr(iAttributeName)); \
} \
HRESULT __stdcall  ENVTIEName::AddActivityConstraint(CATIAActivity * iActivity, SPPProcessConstraintType iConstraintType) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)AddActivityConstraint(iActivity,iConstraintType)); \
} \
HRESULT __stdcall  ENVTIEName::RemoveActivityConstraint(CATIAActivity * iActivity, SPPProcessConstraintType iConstraintType) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)RemoveActivityConstraint(iActivity,iConstraintType)); \
} \
HRESULT __stdcall  ENVTIEName::GetActivityConstraints(SPPProcessConstraintType iConstraintType, CATIAActivities *& oConstrtList) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)GetActivityConstraints(iConstraintType,oConstrtList)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIAEHMInsertionActPlugMapViewData,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIAEHMInsertionActPlugMapViewData(classe)    TIEDNBIAEHMInsertionActPlugMapViewData##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIAEHMInsertionActPlugMapViewData(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIAEHMInsertionActPlugMapViewData, classe) \
 \
 \
CATImplementTIEMethods(DNBIAEHMInsertionActPlugMapViewData, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIAEHMInsertionActPlugMapViewData, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIAEHMInsertionActPlugMapViewData, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIAEHMInsertionActPlugMapViewData, classe) \
 \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::get_ConnectorName(CATBSTR & ioConnectorName) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&ioConnectorName); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_ConnectorName(ioConnectorName); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&ioConnectorName); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::get_ConnectorPartNumber(CATBSTR & ioConnectorPartNumber) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&ioConnectorPartNumber); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_ConnectorPartNumber(ioConnectorPartNumber); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&ioConnectorPartNumber); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::get_ConnectorRefDesignator(CATBSTR & ioConnectorRefDesignator) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&ioConnectorRefDesignator); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_ConnectorRefDesignator(ioConnectorRefDesignator); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&ioConnectorRefDesignator); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::get_ConnectorType(CATBSTR & ioConnectorType) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&ioConnectorType); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_ConnectorType(ioConnectorType); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&ioConnectorType); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::get_ConnectorSubType(CATBSTR & ioConnectorSubType) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&ioConnectorSubType); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_ConnectorSubType(ioConnectorSubType); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&ioConnectorSubType); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::get_NumInsertedWires(CATLONG & oNumInsertedWires) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&oNumInsertedWires); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_NumInsertedWires(oNumInsertedWires); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&oNumInsertedWires); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::put_WireID(CATLONG iWireID) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&iWireID); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_WireID(iWireID); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&iWireID); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::get_TerminationName(CATBSTR & ioTerminationName) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&ioTerminationName); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_TerminationName(ioTerminationName); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&ioTerminationName); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::get_TerminationNumber(CATBSTR & ioTerminationNumber) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&ioTerminationNumber); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_TerminationNumber(ioTerminationNumber); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&ioTerminationNumber); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::get_TerminationIDNumber(CATBSTR & ioTerminationIDNumber) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&ioTerminationIDNumber); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_TerminationIDNumber(ioTerminationIDNumber); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&ioTerminationIDNumber); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::get_TerminationRefDesignator(CATBSTR & ioTerminationRefDesignator) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,11,&_Trac2,&ioTerminationRefDesignator); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_TerminationRefDesignator(ioTerminationRefDesignator); \
   ExitAfterCall(this,11,_Trac2,&_ret_arg,&ioTerminationRefDesignator); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::get_TerminationType(CATBSTR & ioTerminationType) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,12,&_Trac2,&ioTerminationType); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_TerminationType(ioTerminationType); \
   ExitAfterCall(this,12,_Trac2,&_ret_arg,&ioTerminationType); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::get_TerminationSubType(CATBSTR & ioTerminationSubType) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,13,&_Trac2,&ioTerminationSubType); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_TerminationSubType(ioTerminationSubType); \
   ExitAfterCall(this,13,_Trac2,&_ret_arg,&ioTerminationSubType); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::get_WireName(CATBSTR & ioWireName) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,14,&_Trac2,&ioWireName); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_WireName(ioWireName); \
   ExitAfterCall(this,14,_Trac2,&_ret_arg,&ioWireName); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::get_WirePartNumber(CATBSTR & ioWirePartNumber) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,15,&_Trac2,&ioWirePartNumber); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_WirePartNumber(ioWirePartNumber); \
   ExitAfterCall(this,15,_Trac2,&_ret_arg,&ioWirePartNumber); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::get_WireRefDesignator(CATBSTR & ioWireRefDesignator) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,16,&_Trac2,&ioWireRefDesignator); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_WireRefDesignator(ioWireRefDesignator); \
   ExitAfterCall(this,16,_Trac2,&_ret_arg,&ioWireRefDesignator); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::get_WireType(CATBSTR & ioWireType) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,17,&_Trac2,&ioWireType); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_WireType(ioWireType); \
   ExitAfterCall(this,17,_Trac2,&_ret_arg,&ioWireType); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::get_WireSubType(CATBSTR & ioWireSubType) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,18,&_Trac2,&ioWireSubType); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_WireSubType(ioWireSubType); \
   ExitAfterCall(this,18,_Trac2,&_ret_arg,&ioWireSubType); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::get_WireDiameter(double & oWireDiameter) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,19,&_Trac2,&oWireDiameter); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_WireDiameter(oWireDiameter); \
   ExitAfterCall(this,19,_Trac2,&_ret_arg,&oWireDiameter); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::get_WireLength(double & oWireLength) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,20,&_Trac2,&oWireLength); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_WireLength(oWireLength); \
   ExitAfterCall(this,20,_Trac2,&_ret_arg,&oWireLength); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::get_WireColor(CATBSTR & ioWireColor) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,21,&_Trac2,&ioWireColor); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_WireColor(ioWireColor); \
   ExitAfterCall(this,21,_Trac2,&_ret_arg,&ioWireColor); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::get_WireSeparationCode(CATBSTR & ioWireSeparationCode) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,22,&_Trac2,&ioWireSeparationCode); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_WireSeparationCode(ioWireSeparationCode); \
   ExitAfterCall(this,22,_Trac2,&_ret_arg,&ioWireSeparationCode); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::get_WireSignalID(CATBSTR & ioWireSignalID) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,23,&_Trac2,&ioWireSignalID); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_WireSignalID(ioWireSignalID); \
   ExitAfterCall(this,23,_Trac2,&_ret_arg,&ioWireSignalID); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::get_ContactName(CATBSTR & ioContactName) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,24,&_Trac2,&ioContactName); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_ContactName(ioContactName); \
   ExitAfterCall(this,24,_Trac2,&_ret_arg,&ioContactName); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::get_ContactPartNumber(CATBSTR & ioContactPartNumber) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,25,&_Trac2,&ioContactPartNumber); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_ContactPartNumber(ioContactPartNumber); \
   ExitAfterCall(this,25,_Trac2,&_ret_arg,&ioContactPartNumber); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::get_ContactRefDesignator(CATBSTR & ioContactRefDesignator) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,26,&_Trac2,&ioContactRefDesignator); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_ContactRefDesignator(ioContactRefDesignator); \
   ExitAfterCall(this,26,_Trac2,&_ret_arg,&ioContactRefDesignator); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::get_ContactType(CATBSTR & ioContactType) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,27,&_Trac2,&ioContactType); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_ContactType(ioContactType); \
   ExitAfterCall(this,27,_Trac2,&_ret_arg,&ioContactType); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::get_ContactSubType(CATBSTR & ioContactSubType) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,28,&_Trac2,&ioContactSubType); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_ContactSubType(ioContactSubType); \
   ExitAfterCall(this,28,_Trac2,&_ret_arg,&ioContactSubType); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::get_ContactElecBarrelDiameter(double & oContactElecBarrelDiameter) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,29,&_Trac2,&oContactElecBarrelDiameter); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_ContactElecBarrelDiameter(oContactElecBarrelDiameter); \
   ExitAfterCall(this,29,_Trac2,&_ret_arg,&oContactElecBarrelDiameter); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::IsSubTypeOf(const CATBSTR & iName, CAT_VARIANT_BOOL & oVal) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,30,&_Trac2,&iName,&oVal); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->IsSubTypeOf(iName,oVal); \
   ExitAfterCall(this,30,_Trac2,&_ret_arg,&iName,&oVal); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::AttrValue(const CATVariant & iIndex, CATVariant & oAttVal) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,31,&_Trac2,&iIndex,&oAttVal); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->AttrValue(iIndex,oAttVal); \
   ExitAfterCall(this,31,_Trac2,&_ret_arg,&iIndex,&oAttVal); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::AttrName(CATLONG iIndex, CATBSTR & oName) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,32,&_Trac2,&iIndex,&oName); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->AttrName(iIndex,oName); \
   ExitAfterCall(this,32,_Trac2,&_ret_arg,&iIndex,&oName); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::get_Type(CATBSTR & oType) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,33,&_Trac2,&oType); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Type(oType); \
   ExitAfterCall(this,33,_Trac2,&_ret_arg,&oType); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::get_Description(CATBSTR & oDescriptionBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,34,&_Trac2,&oDescriptionBSTR); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Description(oDescriptionBSTR); \
   ExitAfterCall(this,34,_Trac2,&_ret_arg,&oDescriptionBSTR); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::put_Description(const CATBSTR & iDescriptionBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,35,&_Trac2,&iDescriptionBSTR); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Description(iDescriptionBSTR); \
   ExitAfterCall(this,35,_Trac2,&_ret_arg,&iDescriptionBSTR); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::get_CycleTime(double & oCT) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,36,&_Trac2,&oCT); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_CycleTime(oCT); \
   ExitAfterCall(this,36,_Trac2,&_ret_arg,&oCT); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::put_CycleTime(double iCT) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,37,&_Trac2,&iCT); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_CycleTime(iCT); \
   ExitAfterCall(this,37,_Trac2,&_ret_arg,&iCT); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::get_CalculatedCycleTime(double & oCCT) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,38,&_Trac2,&oCCT); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_CalculatedCycleTime(oCCT); \
   ExitAfterCall(this,38,_Trac2,&_ret_arg,&oCCT); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::get_BeginningDate(double & oBegin) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,39,&_Trac2,&oBegin); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_BeginningDate(oBegin); \
   ExitAfterCall(this,39,_Trac2,&_ret_arg,&oBegin); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::put_BeginningDate(double iSBT) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,40,&_Trac2,&iSBT); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_BeginningDate(iSBT); \
   ExitAfterCall(this,40,_Trac2,&_ret_arg,&iSBT); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::get_EndDate(double & oEnd) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,41,&_Trac2,&oEnd); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_EndDate(oEnd); \
   ExitAfterCall(this,41,_Trac2,&_ret_arg,&oEnd); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::get_ChildrenActivities(CATIAActivities *& oChildren) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,42,&_Trac2,&oChildren); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_ChildrenActivities(oChildren); \
   ExitAfterCall(this,42,_Trac2,&_ret_arg,&oChildren); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::CreateChild(const CATBSTR & iTypeOfChild, CATIAActivity *& oCreatedChild) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,43,&_Trac2,&iTypeOfChild,&oCreatedChild); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateChild(iTypeOfChild,oCreatedChild); \
   ExitAfterCall(this,43,_Trac2,&_ret_arg,&iTypeOfChild,&oCreatedChild); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::CreateLink(CATIAActivity * iSecondActivity) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,44,&_Trac2,&iSecondActivity); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateLink(iSecondActivity); \
   ExitAfterCall(this,44,_Trac2,&_ret_arg,&iSecondActivity); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::RemoveLink(CATIAActivity * iSecondActivity) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,45,&_Trac2,&iSecondActivity); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->RemoveLink(iSecondActivity); \
   ExitAfterCall(this,45,_Trac2,&_ret_arg,&iSecondActivity); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::get_NextCFActivities(CATIAActivities *& oNextCF) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,46,&_Trac2,&oNextCF); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_NextCFActivities(oNextCF); \
   ExitAfterCall(this,46,_Trac2,&_ret_arg,&oNextCF); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::get_PreviousCFActivities(CATIAActivities *& oPreviousCF) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,47,&_Trac2,&oPreviousCF); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_PreviousCFActivities(oPreviousCF); \
   ExitAfterCall(this,47,_Trac2,&_ret_arg,&oPreviousCF); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::get_NextPRFActivities(CATIAActivities *& oNextPRF) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,48,&_Trac2,&oNextPRF); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_NextPRFActivities(oNextPRF); \
   ExitAfterCall(this,48,_Trac2,&_ret_arg,&oNextPRF); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::get_PreviousPRFActivities(CATIAActivities *& oPreviousPRF) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,49,&_Trac2,&oPreviousPRF); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_PreviousPRFActivities(oPreviousPRF); \
   ExitAfterCall(this,49,_Trac2,&_ret_arg,&oPreviousPRF); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::get_AttrCount(CATLONG & oNbAttr) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,50,&_Trac2,&oNbAttr); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_AttrCount(oNbAttr); \
   ExitAfterCall(this,50,_Trac2,&_ret_arg,&oNbAttr); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::get_Items(CATIAItems *& oItems) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,51,&_Trac2,&oItems); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Items(oItems); \
   ExitAfterCall(this,51,_Trac2,&_ret_arg,&oItems); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::get_Outputs(CATIAOutputs *& oOutputs) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,52,&_Trac2,&oOutputs); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Outputs(oOutputs); \
   ExitAfterCall(this,52,_Trac2,&_ret_arg,&oOutputs); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::get_Resources(CATIAResources *& oResources) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,53,&_Trac2,&oResources); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Resources(oResources); \
   ExitAfterCall(this,53,_Trac2,&_ret_arg,&oResources); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::get_Relations(CATIARelations *& oRelations) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,54,&_Trac2,&oRelations); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Relations(oRelations); \
   ExitAfterCall(this,54,_Trac2,&_ret_arg,&oRelations); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::get_Parameters(CATIAParameters *& oParameters) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,55,&_Trac2,&oParameters); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parameters(oParameters); \
   ExitAfterCall(this,55,_Trac2,&_ret_arg,&oParameters); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::GetTechnologicalObject(const CATBSTR & iApplicationType, CATBaseDispatch *& oApplicativeObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,56,&_Trac2,&iApplicationType,&oApplicativeObj); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetTechnologicalObject(iApplicationType,oApplicativeObj); \
   ExitAfterCall(this,56,_Trac2,&_ret_arg,&iApplicationType,&oApplicativeObj); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::get_PrecedenceActivities(CATIAActivities *& oActivities) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,57,&_Trac2,&oActivities); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_PrecedenceActivities(oActivities); \
   ExitAfterCall(this,57,_Trac2,&_ret_arg,&oActivities); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::get_PossiblePrecedenceActivities(CATIAActivities *& oActivities) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,58,&_Trac2,&oActivities); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_PossiblePrecedenceActivities(oActivities); \
   ExitAfterCall(this,58,_Trac2,&_ret_arg,&oActivities); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::get_ProcessID(CATBSTR & oProcessID) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,59,&_Trac2,&oProcessID); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_ProcessID(oProcessID); \
   ExitAfterCall(this,59,_Trac2,&_ret_arg,&oProcessID); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::SetProcessID(const CATBSTR & iProcessID, CAT_VARIANT_BOOL iCheckUnique) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,60,&_Trac2,&iProcessID,&iCheckUnique); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetProcessID(iProcessID,iCheckUnique); \
   ExitAfterCall(this,60,_Trac2,&_ret_arg,&iProcessID,&iCheckUnique); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::get_CalculatedBeginTime(double & oCBT) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,61,&_Trac2,&oCBT); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_CalculatedBeginTime(oCBT); \
   ExitAfterCall(this,61,_Trac2,&_ret_arg,&oCBT); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::AddAttr(const CATBSTR & iAttributeName, SPPProcessAttributeType AttrType, const CATBSTR & iAttributePromptName) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,62,&_Trac2,&iAttributeName,&AttrType,&iAttributePromptName); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->AddAttr(iAttributeName,AttrType,iAttributePromptName); \
   ExitAfterCall(this,62,_Trac2,&_ret_arg,&iAttributeName,&AttrType,&iAttributePromptName); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::RemoveAttr(const CATBSTR & iAttributeName) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,63,&_Trac2,&iAttributeName); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->RemoveAttr(iAttributeName); \
   ExitAfterCall(this,63,_Trac2,&_ret_arg,&iAttributeName); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::AddActivityConstraint(CATIAActivity * iActivity, SPPProcessConstraintType iConstraintType) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,64,&_Trac2,&iActivity,&iConstraintType); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->AddActivityConstraint(iActivity,iConstraintType); \
   ExitAfterCall(this,64,_Trac2,&_ret_arg,&iActivity,&iConstraintType); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::RemoveActivityConstraint(CATIAActivity * iActivity, SPPProcessConstraintType iConstraintType) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,65,&_Trac2,&iActivity,&iConstraintType); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->RemoveActivityConstraint(iActivity,iConstraintType); \
   ExitAfterCall(this,65,_Trac2,&_ret_arg,&iActivity,&iConstraintType); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::GetActivityConstraints(SPPProcessConstraintType iConstraintType, CATIAActivities *& oConstrtList) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,66,&_Trac2,&iConstraintType,&oConstrtList); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetActivityConstraints(iConstraintType,oConstrtList); \
   ExitAfterCall(this,66,_Trac2,&_ret_arg,&iConstraintType,&oConstrtList); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,67,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,67,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,68,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,68,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,69,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,69,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,70,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,70,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAEHMInsertionActPlugMapViewData##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,71,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,71,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIAEHMInsertionActPlugMapViewData(classe) \
 \
 \
declare_TIE_DNBIAEHMInsertionActPlugMapViewData(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAEHMInsertionActPlugMapViewData##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAEHMInsertionActPlugMapViewData,"DNBIAEHMInsertionActPlugMapViewData",DNBIAEHMInsertionActPlugMapViewData::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAEHMInsertionActPlugMapViewData(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIAEHMInsertionActPlugMapViewData, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAEHMInsertionActPlugMapViewData##classe(classe::MetaObject(),DNBIAEHMInsertionActPlugMapViewData::MetaObject(),(void *)CreateTIEDNBIAEHMInsertionActPlugMapViewData##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIAEHMInsertionActPlugMapViewData(classe) \
 \
 \
declare_TIE_DNBIAEHMInsertionActPlugMapViewData(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAEHMInsertionActPlugMapViewData##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAEHMInsertionActPlugMapViewData,"DNBIAEHMInsertionActPlugMapViewData",DNBIAEHMInsertionActPlugMapViewData::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAEHMInsertionActPlugMapViewData(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIAEHMInsertionActPlugMapViewData, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAEHMInsertionActPlugMapViewData##classe(classe::MetaObject(),DNBIAEHMInsertionActPlugMapViewData::MetaObject(),(void *)CreateTIEDNBIAEHMInsertionActPlugMapViewData##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIAEHMInsertionActPlugMapViewData(classe) TIE_DNBIAEHMInsertionActPlugMapViewData(classe)
#else
#define BOA_DNBIAEHMInsertionActPlugMapViewData(classe) CATImplementBOA(DNBIAEHMInsertionActPlugMapViewData, classe)
#endif

#endif
