#ifndef __TIE_DNBIAPlugMapViewSettingAtt
#define __TIE_DNBIAPlugMapViewSettingAtt

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIAPlugMapViewSettingAtt.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIAPlugMapViewSettingAtt */
#define declare_TIE_DNBIAPlugMapViewSettingAtt(classe) \
 \
 \
class TIEDNBIAPlugMapViewSettingAtt##classe : public DNBIAPlugMapViewSettingAtt \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIAPlugMapViewSettingAtt, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall get_LogicalDataAttrList(CATSafeArrayVariant *& oLogicalDataAttrList); \
      virtual HRESULT __stdcall put_LogicalDataAttrList(const CATSafeArrayVariant & iLogicalDataAttrList); \
      virtual HRESULT __stdcall GetLogicalDataAttrListInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetLogicalDataAttrListLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall AddtoLogicalDataAttrList(const CATBSTR & iparameterName, const CATBSTR & iRefparamName); \
      virtual HRESULT __stdcall RemovefromLogicalDataAttrList(const CATBSTR & iparameterName); \
      virtual HRESULT __stdcall get_TerminationAttrList(CATSafeArrayVariant *& oTerminationAttrList); \
      virtual HRESULT __stdcall put_TerminationAttrList(const CATSafeArrayVariant & iTerminationAttrList); \
      virtual HRESULT __stdcall GetTerminationAttrListInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetTerminationAttrListLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall AddtoTerminationAttrList(const CATBSTR & iparameterName, const CATBSTR & iRefparamName); \
      virtual HRESULT __stdcall RemovefromTerminationAttrList(const CATBSTR & iparameterName); \
      virtual HRESULT __stdcall Commit(); \
      virtual HRESULT __stdcall Rollback(); \
      virtual HRESULT __stdcall ResetToAdminValues(); \
      virtual HRESULT __stdcall ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList); \
      virtual HRESULT __stdcall SaveRepository(); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIAPlugMapViewSettingAtt(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall get_LogicalDataAttrList(CATSafeArrayVariant *& oLogicalDataAttrList); \
virtual HRESULT __stdcall put_LogicalDataAttrList(const CATSafeArrayVariant & iLogicalDataAttrList); \
virtual HRESULT __stdcall GetLogicalDataAttrListInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetLogicalDataAttrListLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall AddtoLogicalDataAttrList(const CATBSTR & iparameterName, const CATBSTR & iRefparamName); \
virtual HRESULT __stdcall RemovefromLogicalDataAttrList(const CATBSTR & iparameterName); \
virtual HRESULT __stdcall get_TerminationAttrList(CATSafeArrayVariant *& oTerminationAttrList); \
virtual HRESULT __stdcall put_TerminationAttrList(const CATSafeArrayVariant & iTerminationAttrList); \
virtual HRESULT __stdcall GetTerminationAttrListInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetTerminationAttrListLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall AddtoTerminationAttrList(const CATBSTR & iparameterName, const CATBSTR & iRefparamName); \
virtual HRESULT __stdcall RemovefromTerminationAttrList(const CATBSTR & iparameterName); \
virtual HRESULT __stdcall Commit(); \
virtual HRESULT __stdcall Rollback(); \
virtual HRESULT __stdcall ResetToAdminValues(); \
virtual HRESULT __stdcall ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList); \
virtual HRESULT __stdcall SaveRepository(); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIAPlugMapViewSettingAtt(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::get_LogicalDataAttrList(CATSafeArrayVariant *& oLogicalDataAttrList) \
{ \
return (ENVTIECALL(DNBIAPlugMapViewSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_LogicalDataAttrList(oLogicalDataAttrList)); \
} \
HRESULT __stdcall  ENVTIEName::put_LogicalDataAttrList(const CATSafeArrayVariant & iLogicalDataAttrList) \
{ \
return (ENVTIECALL(DNBIAPlugMapViewSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_LogicalDataAttrList(iLogicalDataAttrList)); \
} \
HRESULT __stdcall  ENVTIEName::GetLogicalDataAttrListInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAPlugMapViewSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetLogicalDataAttrListInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetLogicalDataAttrListLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAPlugMapViewSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetLogicalDataAttrListLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::AddtoLogicalDataAttrList(const CATBSTR & iparameterName, const CATBSTR & iRefparamName) \
{ \
return (ENVTIECALL(DNBIAPlugMapViewSettingAtt,ENVTIETypeLetter,ENVTIELetter)AddtoLogicalDataAttrList(iparameterName,iRefparamName)); \
} \
HRESULT __stdcall  ENVTIEName::RemovefromLogicalDataAttrList(const CATBSTR & iparameterName) \
{ \
return (ENVTIECALL(DNBIAPlugMapViewSettingAtt,ENVTIETypeLetter,ENVTIELetter)RemovefromLogicalDataAttrList(iparameterName)); \
} \
HRESULT __stdcall  ENVTIEName::get_TerminationAttrList(CATSafeArrayVariant *& oTerminationAttrList) \
{ \
return (ENVTIECALL(DNBIAPlugMapViewSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_TerminationAttrList(oTerminationAttrList)); \
} \
HRESULT __stdcall  ENVTIEName::put_TerminationAttrList(const CATSafeArrayVariant & iTerminationAttrList) \
{ \
return (ENVTIECALL(DNBIAPlugMapViewSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_TerminationAttrList(iTerminationAttrList)); \
} \
HRESULT __stdcall  ENVTIEName::GetTerminationAttrListInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAPlugMapViewSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetTerminationAttrListInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetTerminationAttrListLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAPlugMapViewSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetTerminationAttrListLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::AddtoTerminationAttrList(const CATBSTR & iparameterName, const CATBSTR & iRefparamName) \
{ \
return (ENVTIECALL(DNBIAPlugMapViewSettingAtt,ENVTIETypeLetter,ENVTIELetter)AddtoTerminationAttrList(iparameterName,iRefparamName)); \
} \
HRESULT __stdcall  ENVTIEName::RemovefromTerminationAttrList(const CATBSTR & iparameterName) \
{ \
return (ENVTIECALL(DNBIAPlugMapViewSettingAtt,ENVTIETypeLetter,ENVTIELetter)RemovefromTerminationAttrList(iparameterName)); \
} \
HRESULT __stdcall  ENVTIEName::Commit() \
{ \
return (ENVTIECALL(DNBIAPlugMapViewSettingAtt,ENVTIETypeLetter,ENVTIELetter)Commit()); \
} \
HRESULT __stdcall  ENVTIEName::Rollback() \
{ \
return (ENVTIECALL(DNBIAPlugMapViewSettingAtt,ENVTIETypeLetter,ENVTIELetter)Rollback()); \
} \
HRESULT __stdcall  ENVTIEName::ResetToAdminValues() \
{ \
return (ENVTIECALL(DNBIAPlugMapViewSettingAtt,ENVTIETypeLetter,ENVTIELetter)ResetToAdminValues()); \
} \
HRESULT __stdcall  ENVTIEName::ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList) \
{ \
return (ENVTIECALL(DNBIAPlugMapViewSettingAtt,ENVTIETypeLetter,ENVTIELetter)ResetToAdminValuesByName(iAttList)); \
} \
HRESULT __stdcall  ENVTIEName::SaveRepository() \
{ \
return (ENVTIECALL(DNBIAPlugMapViewSettingAtt,ENVTIETypeLetter,ENVTIELetter)SaveRepository()); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIAPlugMapViewSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIAPlugMapViewSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIAPlugMapViewSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIAPlugMapViewSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIAPlugMapViewSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIAPlugMapViewSettingAtt(classe)    TIEDNBIAPlugMapViewSettingAtt##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIAPlugMapViewSettingAtt(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIAPlugMapViewSettingAtt, classe) \
 \
 \
CATImplementTIEMethods(DNBIAPlugMapViewSettingAtt, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIAPlugMapViewSettingAtt, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIAPlugMapViewSettingAtt, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIAPlugMapViewSettingAtt, classe) \
 \
HRESULT __stdcall  TIEDNBIAPlugMapViewSettingAtt##classe::get_LogicalDataAttrList(CATSafeArrayVariant *& oLogicalDataAttrList) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&oLogicalDataAttrList); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_LogicalDataAttrList(oLogicalDataAttrList); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&oLogicalDataAttrList); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAPlugMapViewSettingAtt##classe::put_LogicalDataAttrList(const CATSafeArrayVariant & iLogicalDataAttrList) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&iLogicalDataAttrList); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_LogicalDataAttrList(iLogicalDataAttrList); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&iLogicalDataAttrList); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAPlugMapViewSettingAtt##classe::GetLogicalDataAttrListInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetLogicalDataAttrListInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAPlugMapViewSettingAtt##classe::SetLogicalDataAttrListLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetLogicalDataAttrListLock(iLocked); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAPlugMapViewSettingAtt##classe::AddtoLogicalDataAttrList(const CATBSTR & iparameterName, const CATBSTR & iRefparamName) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&iparameterName,&iRefparamName); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->AddtoLogicalDataAttrList(iparameterName,iRefparamName); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&iparameterName,&iRefparamName); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAPlugMapViewSettingAtt##classe::RemovefromLogicalDataAttrList(const CATBSTR & iparameterName) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&iparameterName); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->RemovefromLogicalDataAttrList(iparameterName); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&iparameterName); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAPlugMapViewSettingAtt##classe::get_TerminationAttrList(CATSafeArrayVariant *& oTerminationAttrList) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&oTerminationAttrList); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_TerminationAttrList(oTerminationAttrList); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&oTerminationAttrList); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAPlugMapViewSettingAtt##classe::put_TerminationAttrList(const CATSafeArrayVariant & iTerminationAttrList) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&iTerminationAttrList); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_TerminationAttrList(iTerminationAttrList); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&iTerminationAttrList); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAPlugMapViewSettingAtt##classe::GetTerminationAttrListInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetTerminationAttrListInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAPlugMapViewSettingAtt##classe::SetTerminationAttrListLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetTerminationAttrListLock(iLocked); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAPlugMapViewSettingAtt##classe::AddtoTerminationAttrList(const CATBSTR & iparameterName, const CATBSTR & iRefparamName) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,11,&_Trac2,&iparameterName,&iRefparamName); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->AddtoTerminationAttrList(iparameterName,iRefparamName); \
   ExitAfterCall(this,11,_Trac2,&_ret_arg,&iparameterName,&iRefparamName); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAPlugMapViewSettingAtt##classe::RemovefromTerminationAttrList(const CATBSTR & iparameterName) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,12,&_Trac2,&iparameterName); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->RemovefromTerminationAttrList(iparameterName); \
   ExitAfterCall(this,12,_Trac2,&_ret_arg,&iparameterName); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAPlugMapViewSettingAtt##classe::Commit() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,13,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Commit(); \
   ExitAfterCall(this,13,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAPlugMapViewSettingAtt##classe::Rollback() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,14,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Rollback(); \
   ExitAfterCall(this,14,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAPlugMapViewSettingAtt##classe::ResetToAdminValues() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,15,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ResetToAdminValues(); \
   ExitAfterCall(this,15,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAPlugMapViewSettingAtt##classe::ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,16,&_Trac2,&iAttList); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ResetToAdminValuesByName(iAttList); \
   ExitAfterCall(this,16,_Trac2,&_ret_arg,&iAttList); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAPlugMapViewSettingAtt##classe::SaveRepository() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,17,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SaveRepository(); \
   ExitAfterCall(this,17,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAPlugMapViewSettingAtt##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,18,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,18,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAPlugMapViewSettingAtt##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,19,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,19,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAPlugMapViewSettingAtt##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,20,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,20,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAPlugMapViewSettingAtt##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,21,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,21,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAPlugMapViewSettingAtt##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,22,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,22,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIAPlugMapViewSettingAtt(classe) \
 \
 \
declare_TIE_DNBIAPlugMapViewSettingAtt(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAPlugMapViewSettingAtt##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAPlugMapViewSettingAtt,"DNBIAPlugMapViewSettingAtt",DNBIAPlugMapViewSettingAtt::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAPlugMapViewSettingAtt(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIAPlugMapViewSettingAtt, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAPlugMapViewSettingAtt##classe(classe::MetaObject(),DNBIAPlugMapViewSettingAtt::MetaObject(),(void *)CreateTIEDNBIAPlugMapViewSettingAtt##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIAPlugMapViewSettingAtt(classe) \
 \
 \
declare_TIE_DNBIAPlugMapViewSettingAtt(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAPlugMapViewSettingAtt##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAPlugMapViewSettingAtt,"DNBIAPlugMapViewSettingAtt",DNBIAPlugMapViewSettingAtt::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAPlugMapViewSettingAtt(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIAPlugMapViewSettingAtt, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAPlugMapViewSettingAtt##classe(classe::MetaObject(),DNBIAPlugMapViewSettingAtt::MetaObject(),(void *)CreateTIEDNBIAPlugMapViewSettingAtt##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIAPlugMapViewSettingAtt(classe) TIE_DNBIAPlugMapViewSettingAtt(classe)
#else
#define BOA_DNBIAPlugMapViewSettingAtt(classe) CATImplementBOA(DNBIAPlugMapViewSettingAtt, classe)
#endif

#endif
