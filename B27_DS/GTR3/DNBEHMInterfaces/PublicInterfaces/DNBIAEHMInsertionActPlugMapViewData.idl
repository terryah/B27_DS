// COPYRIGHT Dassault Systemes 2005
//=============================================================================
//
// DNBIAEHMInsertionActPlugMapViewData.idl
// Automation interface for the EHMInsertionActPlugMapViewData element 
//
//=============================================================================
//
// Usage notes:
//   New interface: describe its use here
//
//=============================================================================
//  mm-dd-yyyy  History                                                    User
//  11-15-2005  Cre                                                         rtt
//=============================================================================

#ifndef DNBIAEHMInsertionActPlugMapViewData_IDL
#define DNBIAEHMInsertionActPlugMapViewData_IDL

/*IDLREP*/

#include "CATIAActivity.idl"

/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */

/**
* Interface to access Plug Map view data associated to an Insertion activity.
*
* <br><b>Role</b>: Component that implement
* DNBIAEHMInsertionActPlugMapViewData is DNBEHMModel.
* <p>
*
* When using DNBIAEHMInsertionActPlugMapViewData interface in C++, query  
* from interface DNBIEHMInsertionActivity.
* <br>When using from VB Script, use GetTechnologicalObject to get handle to interface.
* <br>
* <br>PlugMap Data consists of 2 parts namely Connector data & inserted wires data.
* <br>To get data about inserted wires, first get the number of wires using method 
* <br>get_NumInsertedWires, then set index of wire using put_WireID and then access information about that wire.
* <br>Please note that default returned string for all PROPERTY of type CATBSTR is "UnSet".
* <br>
* <br><b>Sample Code in VB Script is as given below</b>
* <br>'---- InsertionAct is activity for Plug Map view data.
* <br>Dim InsertionAct As Activity
* <br>Set InsertionAct = get activity DNBEHMInsertionAct.1
* <br>Set PlugMapData = InsertionAct.GetTechnologicalObject( "EHMInsertionActPlugMapViewData" )
* <br>Dim ConnectorName As String
* <br>ConnectorName = PlugMapData.ConnectorName 
* <br>Dim NumInsertedWires, ii As Integer
* <br>NumInsertedWires = PlugMapData.NumInsertedWires
* <br>Dim TerminationName, WireName, ContactName As String
* <br>For ii = 1 to NumInsertedWires
* <br>  PlugMapData.WireID = ii
* <br>  TerminationName = PlugMapData.TerminationName
* <br>  WireName = PlugMapData.WireName
* <br>Next
*
*
*/

interface DNBIAEHMInsertionActPlugMapViewData : CATIAActivity
{
#pragma PROPERTY ConnectorName

/** Gets ConnectorName.
 *   @param oConnectorName
 *     the Connector Name.
 *   @return                    
 *     An HRESULT
 *   <br><b>Legal values</b>:
 *   <ul>
 *   <li>S_OK if the Property is returned successfully. </li>
 *   <li>E_FAIL otherwise </li>.
 *   </ul>
 */

  HRESULT get_ConnectorName( inout /*IDLRETVAL*/ CATBSTR ioConnectorName );


#pragma PROPERTY ConnectorPartNumber

/** Gets Connector Part Number.
 *   @param oConnectorPartNumber
 *     the Connector Part Number.
 *   @return                    
 *     An HRESULT
 *   <br><b>Legal values</b>:
 *   <ul>
 *   <li>S_OK if the Property is returned successfully. </li>
 *   <li>E_FAIL otherwise </li>.
 *   </ul>
 */

  HRESULT get_ConnectorPartNumber( inout /*IDLRETVAL*/ CATBSTR ioConnectorPartNumber );

#pragma PROPERTY ConnectorRefDesignator

/** Gets Connector Reference Designator.
 *   @param oConnectorRefDesignator
 *     the Connector Reference Designator.
 *   @return                    
 *     An HRESULT
 *   <br><b>Legal values</b>:
 *   <ul>
 *   <li>S_OK if the Property is returned successfully. </li>
 *   <li>E_FAIL otherwise </li>.
 *   </ul>
 */

  HRESULT get_ConnectorRefDesignator( inout /*IDLRETVAL*/ CATBSTR ioConnectorRefDesignator );

#pragma PROPERTY ConnectorType

/** Gets Connector Type.
 *   @param oConnectorType
 *     the Connector Type.
 *   @return                    
 *     An HRESULT
 *   <br><b>Legal values</b>:
 *   <ul>
 *   <li>S_OK if the Property is returned successfully. </li>
 *   <li>E_FAIL otherwise </li>.
 *   </ul>
 */

  HRESULT get_ConnectorType( inout /*IDLRETVAL*/ CATBSTR ioConnectorType );

#pragma PROPERTY ConnectorSubType

/** Gets Connector Sub Type.
 *   @param oConnectorSubType
 *     the Connector Sub Type.
 *   @return                    
 *     An HRESULT
 *   <br><b>Legal values</b>:
 *   <ul>
 *   <li>S_OK if the Property is returned successfully. </li>
 *   <li>E_FAIL otherwise </li>.
 *   </ul>
 */

  HRESULT get_ConnectorSubType( inout /*IDLRETVAL*/ CATBSTR ioConnectorSubType );

#pragma PROPERTY NumInsertedWires

/** Gets the number of inserted wires.
 *   @param NumInsertedWires
 *     the number of inserted wires.
 *   @return                    
 *     An HRESULT
 *   <br><b>Legal values</b>:
 *   <ul>
 *   <li>S_OK if the wire ID is set successfully. </li>
 *   <li>E_FAIL otherwise </li>.
 *   </ul>
 */

  HRESULT get_NumInsertedWires( out /*IDLRETVAL*/ long oNumInsertedWires );

#pragma PROPERTY WireID

/** Sets the index of wire.
 *   @param iWireID
 *     Index of wire between 1 & NumInsertedWires associated to an Insertion Activity.
 *   @return                    
 *     An HRESULT
 *   <br><b>Legal values</b>:
 *   <ul>
 *   <li>S_OK if the wire ID is set successfully. </li>
 *   <li>E_FAIL otherwise </li>.
 *   </ul>
 */

  HRESULT put_WireID( in long iWireID );

#pragma PROPERTY TerminationName

/** Gets Termination Name.
 *   @param oTerminationName
 *     the Termination Name.
 *   @return                    
 *     An HRESULT
 *   <br><b>Legal values</b>:
 *   <ul>
 *   <li>S_OK if the Property is returned successfully. </li>
 *   <li>E_FAIL otherwise </li>.
 *   </ul>
 */

  HRESULT get_TerminationName( inout /*IDLRETVAL*/ CATBSTR ioTerminationName );

#pragma PROPERTY TerminationNumber

/** Gets Termination Number.
 *   @param oTerminationNumber
 *     the Termination Number.
 *   @return                    
 *     An HRESULT
 *   <br><b>Legal values</b>:
 *   <ul>
 *   <li>S_OK if the Property is returned successfully. </li>
 *   <li>E_FAIL otherwise </li>.
 *   </ul>
 */

  HRESULT get_TerminationNumber( inout /*IDLRETVAL*/ CATBSTR ioTerminationNumber );

#pragma PROPERTY TerminationIDNumber

/** Gets Termination ID Number.
 *   @param oTerminationIDNumber.
 *     the Termination ID Number
 *   @return                    
 *     An HRESULT
 *   <br><b>Legal values</b>:
 *   <ul>
 *   <li>S_OK if the Property is returned successfully. </li>
 *   <li>E_FAIL otherwise </li>.
 *   </ul>
 */

  HRESULT get_TerminationIDNumber( inout /*IDLRETVAL*/ CATBSTR ioTerminationIDNumber );

#pragma PROPERTY TerminationRefDesignator

/** Gets Termination Reference Designator.
 *   @param oTerminationRefDesignator
 *     the Termination Ref Designator.
 *   @return                    
 *     An HRESULT
 *   <br><b>Legal values</b>:
 *   <ul>
 *   <li>S_OK if the Property is returned successfully. </li>
 *   <li>E_FAIL otherwise </li>.
 *   </ul>
 */

  HRESULT get_TerminationRefDesignator( inout /*IDLRETVAL*/ CATBSTR ioTerminationRefDesignator );

#pragma PROPERTY TerminationType

/** Gets Termination Type.
 *   @param oTerminationType
 *     the Termination Type.
 *   @return                    
 *     An HRESULT
 *   <br><b>Legal values</b>:
 *   <ul>
 *   <li>S_OK if the Property is returned successfully. </li>
 *   <li>E_FAIL otherwise </li>.
 *   </ul>
 */

  HRESULT get_TerminationType( inout /*IDLRETVAL*/ CATBSTR ioTerminationType );

#pragma PROPERTY TerminationSubType

/** Gets Termination Sub Type.
 *   @param oTerminationSubType
 *     the Termination Sub Type.
 *   @return                    
 *     An HRESULT
 *   <br><b>Legal values</b>:
 *   <ul>
 *   <li>S_OK if the Property is returned successfully. </li>
 *   <li>E_FAIL otherwise </li>.
 *   </ul>
 */

  HRESULT get_TerminationSubType( inout /*IDLRETVAL*/ CATBSTR ioTerminationSubType );

#pragma PROPERTY WireName

/** Gets Wire Name.
 *   @param oWireName
 *     the Wire Name.
 *   @return                    
 *     An HRESULT
 *   <br><b>Legal values</b>:
 *   <ul>
 *   <li>S_OK if the Property is returned successfully. </li>
 *   <li>E_FAIL otherwise </li>.
 *   </ul>
 */

  HRESULT get_WireName( inout /*IDLRETVAL*/ CATBSTR ioWireName );

#pragma PROPERTY WirePartNumber

/** Gets Wire Part Number.
 *   @param oWirePartNumber
 *     the Wire Part Number.
 *   @return                    
 *     An HRESULT
 *   <br><b>Legal values</b>:
 *   <ul>
 *   <li>S_OK if the Property is returned successfully. </li>
 *   <li>E_FAIL otherwise </li>.
 *   </ul>
 */

  HRESULT get_WirePartNumber( inout /*IDLRETVAL*/ CATBSTR ioWirePartNumber );

#pragma PROPERTY WireRefDesignator

/** Gets Wire Reference Designator.
 *   @param oWireRefDesignator
 *     the Wire Ref Designator.
 *   @return                    
 *     An HRESULT
 *   <br><b>Legal values</b>:
 *   <ul>
 *   <li>S_OK if the Property is returned successfully. </li>
 *   <li>E_FAIL otherwise </li>.
 *   </ul>
 */

  HRESULT get_WireRefDesignator( inout /*IDLRETVAL*/ CATBSTR ioWireRefDesignator );

#pragma PROPERTY WireType

/** Gets Wire Type.
 *   @param oWireType
 *     the Wire Type.
 *   @return                    
 *     An HRESULT
 *   <br><b>Legal values</b>:
 *   <ul>
 *   <li>S_OK if the Property is returned successfully. </li>
 *   <li>E_FAIL otherwise </li>.
 *   </ul>
 */

  HRESULT get_WireType( inout /*IDLRETVAL*/ CATBSTR ioWireType );

#pragma PROPERTY WireSubType

/** Gets Wire Sub Type.
 *   @param oWireSubType
 *     the Wire Sub Type.
 *   @return                    
 *     An HRESULT
 *   <br><b>Legal values</b>:
 *   <ul>
 *   <li>S_OK if the Property is returned successfully. </li>
 *   <li>E_FAIL otherwise </li>.
 *   </ul>
 */

  HRESULT get_WireSubType( inout /*IDLRETVAL*/ CATBSTR ioWireSubType );

#pragma PROPERTY WireDiameter

/** Gets Wire Diameter.
 *   @param oWireDiameter
 *     the Wire Diameter.
 *   @return                    
 *     An HRESULT
 *   <br><b>Legal values</b>:
 *   <ul>
 *   <li>S_OK if the Property is returned successfully. </li>
 *   <li>E_FAIL otherwise </li>.
 *   </ul>
 */

  HRESULT get_WireDiameter( out /*IDLRETVAL*/ double oWireDiameter );

#pragma PROPERTY WireLength

/** Gets Wire Length.
 *   @param oWireLength
 *     the Wire Length.
 *   @return                    
 *     An HRESULT
 *   <br><b>Legal values</b>:
 *   <ul>
 *   <li>S_OK if the Property is returned successfully. </li>
 *   <li>E_FAIL otherwise </li>.
 *   </ul>
 */

  HRESULT get_WireLength( out /*IDLRETVAL*/ double oWireLength );

#pragma PROPERTY WireColor

/** Gets Wire Color.
 *   @param oWireColor
 *     the Wire Color.
 *   @return                    
 *     An HRESULT
 *   <br><b>Legal values</b>:
 *   <ul>
 *   <li>S_OK if the Property is returned successfully. </li>
 *   <li>E_FAIL otherwise </li>.
 *   </ul>
 */

  HRESULT get_WireColor( inout /*IDLRETVAL*/ CATBSTR ioWireColor );

#pragma PROPERTY WireSeparationCode

/** Gets Wire Separation Code.
 *   @param oWireSeparationCode
 *     the Wire Separation Code.
 *   @return                    
 *     An HRESULT
 *   <br><b>Legal values</b>:
 *   <ul>
 *   <li>S_OK if the Property is returned successfully. </li>
 *   <li>E_FAIL otherwise </li>.
 *   </ul>
 */

  HRESULT get_WireSeparationCode( inout /*IDLRETVAL*/ CATBSTR ioWireSeparationCode );

#pragma PROPERTY WireSignalID

/** Gets Wire Signal ID.
 *   @param oWireSignalID
 *     the Wire Signal ID.
 *   @return                    
 *     An HRESULT
 *   <br><b>Legal values</b>:
 *   <ul>
 *   <li>S_OK if the Property is returned successfully. </li>
 *   <li>E_FAIL otherwise </li>.
 *   </ul>
 */

  HRESULT get_WireSignalID( inout /*IDLRETVAL*/ CATBSTR ioWireSignalID );

#pragma PROPERTY ContactName

/** Gets Contact Name.
 *   @param oContactName
 *     the Contact Name.
 *   @return                    
 *     An HRESULT
 *   <br><b>Legal values</b>:
 *   <ul>
 *   <li>S_OK if the Property is returned successfully. </li>
 *   <li>E_FAIL otherwise </li>.
 *   </ul>
 */

  HRESULT get_ContactName( inout /*IDLRETVAL*/ CATBSTR ioContactName );

#pragma PROPERTY ContactPartNumber

/** Gets Contact Part Number.
 *   @param oContactPartNumber
 *     the Contact Part Number.
 *   @return                    
 *     An HRESULT
 *   <br><b>Legal values</b>:
 *   <ul>
 *   <li>S_OK if the Property is returned successfully. </li>
 *   <li>E_FAIL otherwise </li>.
 *   </ul>
 */

  HRESULT get_ContactPartNumber( inout /*IDLRETVAL*/ CATBSTR ioContactPartNumber );

#pragma PROPERTY ContactRefDesignator

/** Gets Contact Reference Designator.
 *   @param oContactRefDesignator
 *     the Contact Ref Designator.
 *   @return                    
 *     An HRESULT
 *   <br><b>Legal values</b>:
 *   <ul>
 *   <li>S_OK if the Property is returned successfully. </li>
 *   <li>E_FAIL otherwise </li>.
 *   </ul>
 */

  HRESULT get_ContactRefDesignator( inout /*IDLRETVAL*/ CATBSTR ioContactRefDesignator );

#pragma PROPERTY ContactType

/** Gets Contact Type.
 *   @param oContactType
 *     the Contact Type.
 *   @return                    
 *     An HRESULT
 *   <br><b>Legal values</b>:
 *   <ul>
 *   <li>S_OK if the Property is returned successfully. </li>
 *   <li>E_FAIL otherwise </li>.
 *   </ul>
 */

  HRESULT get_ContactType( inout /*IDLRETVAL*/ CATBSTR ioContactType );

#pragma PROPERTY ContactSubType

/** Gets Contact Sub Type.
 *   @param oContactSubType
 *     the Contact Sub Type.
 *   @return                    
 *     An HRESULT
 *   <br><b>Legal values</b>:
 *   <ul>
 *   <li>S_OK if the Property is returned successfully. </li>
 *   <li>E_FAIL otherwise </li>.
 *   </ul>
 */

  HRESULT get_ContactSubType( inout /*IDLRETVAL*/ CATBSTR ioContactSubType );

#pragma PROPERTY ContactElecBarrelDiameter

/** Gets Contact Elec Barrel Diameter.
 *   @param oContactElecBarrelDiameter
 *     the Contact Elec Barrel Diameter.
 *   @return                    
 *     An HRESULT
 *   <br><b>Legal values</b>:
 *   <ul>
 *   <li>S_OK if the Property is returned successfully. </li>
 *   <li>E_FAIL otherwise </li>.
 *   </ul>
 */

  HRESULT get_ContactElecBarrelDiameter( out /*IDLRETVAL*/ double oContactElecBarrelDiameter );
};

// Interface name : DNBIAEHMInsertionActPlugMapViewData
#pragma ID DNBIAEHMInsertionActPlugMapViewData "DCE:67a2adbf-58a5-4b4d-b23b7a4d4881b8df"
#pragma DUAL DNBIAEHMInsertionActPlugMapViewData

// VB object name : EHMInsertionActPlugMapViewData (Id used in Visual Basic)
#pragma ID EHMInsertionActPlugMapViewData "DCE:3b82d632-e87a-4460-993a9cea5d056d94"
#pragma ALIAS DNBIAEHMInsertionActPlugMapViewData EHMInsertionActPlugMapViewData

#endif
