#
# SHARED LIBRARY
#
BUILT_OBJECT_TYPE=SHARED LIBRARY

INCLUDED_MODULES=WBxInfraUtilities 

LINK_WITH_COMMON = \
  JS0GROUP \
  KnowledgeItf \
  CATWBx \
  CATCDSUtilities

#ifdef CATIAR201
LINK_WITH = \
  $(LINK_WITH_COMMON) \
  CATMathStream \
  CATMagnitude
#else
LINK_WITH = \
  $(LINK_WITH_COMMON)\
  CATLiteralFeatures
#endif
                  
#
OS = Windows_NT
#
OPTIMIZATION_CPP=/O2
