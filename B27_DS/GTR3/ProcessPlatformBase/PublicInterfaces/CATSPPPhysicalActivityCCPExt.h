// COPYRIGHT DASSAULT SYSTEMES  1998
//=============================================================================
//
// CATSPPPhysicalActivityCCPExt : --EXTENSION--
// <explain concisely class main purpose and nature>
//
//=============================================================================
// Usage Notes:
//
//        ATTENTION: CE HEADER EST EXPOSE PROVISOIREMENT EN PROTECTEDINTERFACES
//                   POUR UN BESOIN DU MANUFACTURING
//                   MERCI AU AUTRES APPLIS QUI VOUDRAIENT EN DERIVER
//                   D'INFORMER PMM OU PCM AVENT.
//
//
//
// <describe here how you intend this class to be used>
//=============================================================================
// May. 98   Creation                                   P. Motte
// Feb 2007	 Expose to PublicInterfaces for CAA			gny
//=============================================================================

/** 
* @CAA2Level L0
* @CAA2Usage U2 
*/

#ifndef CATSPPPhysicalActivityCCPExt_H
#define CATSPPPhysicalActivityCCPExt_H

#include "SP0BBOSM.h"
#include "CATSPPActivityCCPExt.h"

//-----------------------------------------------------------------------------
class ExportedBySP0BBOSM CATSPPPhysicalActivityCCPExt : public CATSPPActivityCCPExt
{
  public:
  
    CATDeclareClass ;
    //------
    // Default constructor/copy constructor/other constructors/Destructor
    //------
    CATSPPPhysicalActivityCCPExt();
    virtual ~CATSPPPhysicalActivityCCPExt();

    //-----------------------------------
    // CATICutAndPastable interface
    //-----------------------------------

  virtual ListOfVarBaseUnknown Paste(ListOfVarBaseUnknown & object_to_copy,
                                     ListOfVarBaseUnknown * to_cur_objects = NULL,
                                     const CATFormat * an_imposed_format = NULL);
  virtual int Create(CATBaseUnknown_Associations & association_of_objects,
                     const CATFormat * an_imposed_format);
  virtual int Update(CATBaseUnknown_Associations & ioAssociationOfObjects,
                     const CATFormat * iAnImposedFormat=NULL,
                     ListOfVarBaseUnknown * iToCurObjects=NULL);
  virtual int BoundaryExtract(ListOfVarBaseUnknown & objects_already_in_boundary,
                              const ListOfVarBaseUnknown * objects_to_add = NULL,
                              const CATFormat * an_imposed_format = NULL) const;
  virtual int BoundaryRemove(ListOfVarBaseUnknown & objects_already_in_boundary,
                             const ListOfVarBaseUnknown * objects_to_remove = NULL,
                             const CATFormat * an_imposed_format = NULL);

    protected:
    
  // Pour la gestion du Drag'n'Drop d une activit� sur une de ses filles :
  // Situation par d�faut :
  //   + l activit� parente est appel�e sur le BoundaryRemove par un CanCut
  //   + l activit� est copi�e
  //   + la copie de l activit� est coll�e sur la destination
  //   + l activit� initiale est supprim�e (le Drag'n'Drop est l �quivalent d un Cut+Paste)
  // Problemes :
  //   + l activit� initiale est d�truite avec toute sa d�scendence
  //   + on peut d�tecter le probl�me au niveau du D'n'D, mais cela d�pend du workbench est de l objet actif
  //   + on ne peut d�tecter le probl�me dans le Cut-Copy-Paste qu qu moment du Coller
  // Solution :
  //   + Faire �chouer le Coller en cas de d�tection d un tel probl�me :
  //     - Dans le Paste, on vide la liste des objets � copier, information qui remonte
  //       jusqu au D'n'D qui consid�re que le Paste a �chou� et interdit le Cut
  //     - Voir info compl�te ailleurs

  static int _DnDActiveDnDTransaction;
  static int _DnDCurrentTransactionNb;
  static ListOfVarBaseUnknown _DnDCurrentToBeCutObjects;
  static boolean ForbidThisPaste(const ListOfVarBaseUnknown *);

};
#endif
