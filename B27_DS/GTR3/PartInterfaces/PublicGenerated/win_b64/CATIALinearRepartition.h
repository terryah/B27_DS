/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIALinearRepartition_h
#define CATIALinearRepartition_h

#ifndef ExportedByPartPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __PartPubIDL
#define ExportedByPartPubIDL __declspec(dllexport)
#else
#define ExportedByPartPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByPartPubIDL
#endif
#endif

#include "CATIARepartition.h"

class CATIALength;

extern ExportedByPartPubIDL IID IID_CATIALinearRepartition;

class ExportedByPartPubIDL CATIALinearRepartition : public CATIARepartition
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_Spacing(CATIALength *& oSpacing)=0;


};

CATDeclareHandler(CATIALinearRepartition, CATIARepartition);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIABase.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
