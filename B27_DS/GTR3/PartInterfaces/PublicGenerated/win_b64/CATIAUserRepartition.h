/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAUserRepartition_h
#define CATIAUserRepartition_h

#ifndef ExportedByPartPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __PartPubIDL
#define ExportedByPartPubIDL __declspec(dllexport)
#else
#define ExportedByPartPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByPartPubIDL
#endif
#endif

#include "CATIARepartition.h"

class CATIABase;

extern ExportedByPartPubIDL IID IID_CATIAUserRepartition;

class ExportedByPartPubIDL CATIAUserRepartition : public CATIARepartition
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_FeatureToLocatePositions(CATIABase *& oFeatureToLocatePositions)=0;

    virtual HRESULT __stdcall AddFeatureToLocatePositions(CATIABase * iFeatureToLocatePositions)=0;


};

CATDeclareHandler(CATIAUserRepartition, CATIARepartition);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIABase.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
