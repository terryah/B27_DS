/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIATritangentFillet_h
#define CATIATritangentFillet_h

#ifndef ExportedByPartPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __PartPubIDL
#define ExportedByPartPubIDL __declspec(dllexport)
#else
#define ExportedByPartPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByPartPubIDL
#endif
#endif

#include "CATIAFillet.h"

class CATIAReference;

extern ExportedByPartPubIDL IID IID_CATIATritangentFillet;

class ExportedByPartPubIDL CATIATritangentFillet : public CATIAFillet
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_FirstFace(CATIAReference *& oFirstFace)=0;

    virtual HRESULT __stdcall put_FirstFace(CATIAReference * iFirstFace)=0;

    virtual HRESULT __stdcall get_SecondFace(CATIAReference *& oSecondFace)=0;

    virtual HRESULT __stdcall put_SecondFace(CATIAReference * iSecondFace)=0;

    virtual HRESULT __stdcall get_FaceToRemove(CATIAReference *& oFaceToRemove)=0;

    virtual HRESULT __stdcall put_FaceToRemove(CATIAReference * iFaceToRemove)=0;


};

CATDeclareHandler(CATIATritangentFillet, CATIAFillet);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATFilletDefs.h"
#include "CATIABase.h"
#include "CATIADressUpShape.h"
#include "CATIAShape.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
