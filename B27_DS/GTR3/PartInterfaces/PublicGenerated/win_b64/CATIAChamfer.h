/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAChamfer_h
#define CATIAChamfer_h

#ifndef ExportedByPartPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __PartPubIDL
#define ExportedByPartPubIDL __declspec(dllexport)
#else
#define ExportedByPartPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByPartPubIDL
#endif
#endif

#include "CATChamferDefs.h"
#include "CATIADressUpShape.h"

class CATIAAngle;
class CATIALength;
class CATIAReference;
class CATIAReferences;

extern ExportedByPartPubIDL IID IID_CATIAChamfer;

class ExportedByPartPubIDL CATIAChamfer : public CATIADressUpShape
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_Mode(CatChamferMode & oMode)=0;

    virtual HRESULT __stdcall put_Mode(CatChamferMode iMode)=0;

    virtual HRESULT __stdcall get_Propagation(CatChamferPropagation & oPropagation)=0;

    virtual HRESULT __stdcall put_Propagation(CatChamferPropagation iPropagation)=0;

    virtual HRESULT __stdcall get_Orientation(CatChamferOrientation & oOrientation)=0;

    virtual HRESULT __stdcall put_Orientation(CatChamferOrientation iOrientation)=0;

    virtual HRESULT __stdcall get_Length1(CATIALength *& oLength1)=0;

    virtual HRESULT __stdcall get_Length2(CATIALength *& oLength2)=0;

    virtual HRESULT __stdcall get_Angle(CATIAAngle *& oAngle)=0;

    virtual HRESULT __stdcall get_ElementsToChamfer(CATIAReferences *& oElementsToChamfer)=0;

    virtual HRESULT __stdcall AddElementToChamfer(CATIAReference * iElementToChamfer)=0;

    virtual HRESULT __stdcall WithdrawElementToChamfer(CATIAReference * iElementToWithdraw)=0;


};

CATDeclareHandler(CATIAChamfer, CATIADressUpShape);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIABase.h"
#include "CATIAShape.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
