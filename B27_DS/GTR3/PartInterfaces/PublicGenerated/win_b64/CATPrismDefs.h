/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATPrismDefs_h
#define CATPrismDefs_h

enum CatPrismExtrusionDirection {
        catNormalToSketchDirection,
        catNotNormalToSketchDirection
};

enum CatPrismOrientation {
        catRegularOrientation,
        catInverseOrientation
};

#endif
