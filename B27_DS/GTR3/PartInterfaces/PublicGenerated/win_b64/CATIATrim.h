/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIATrim_h
#define CATIATrim_h

#ifndef ExportedByPartPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __PartPubIDL
#define ExportedByPartPubIDL __declspec(dllexport)
#else
#define ExportedByPartPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByPartPubIDL
#endif
#endif

#include "CATIABooleanShape.h"

class CATIAReference;

extern ExportedByPartPubIDL IID IID_CATIATrim;

class ExportedByPartPubIDL CATIATrim : public CATIABooleanShape
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall AddFaceToRemove(CATIAReference * iFaceToRemove)=0;

    virtual HRESULT __stdcall AddFaceToRemove2(CATIAReference * iFaceToRemove, CATIAReference * iFaceAdjacentForRemove)=0;

    virtual HRESULT __stdcall WithdrawFaceToRemove(CATIAReference * iFaceToWithdraw)=0;

    virtual HRESULT __stdcall WithdrawFaceToRemove2(CATIAReference * iFaceToWithdraw, CATIAReference * iFaceAdjacentForRemove)=0;

    virtual HRESULT __stdcall AddFaceToKeep(CATIAReference * iFaceToKeep)=0;

    virtual HRESULT __stdcall AddFaceToKeep2(CATIAReference * iFaceToKeep, CATIAReference * iFaceAdjacentForKeep)=0;

    virtual HRESULT __stdcall WithdrawFaceToKeep(CATIAReference * iFaceToWithdraw)=0;

    virtual HRESULT __stdcall WithdrawFaceToKeep2(CATIAReference * iFaceToWithdraw, CATIAReference * iFaceAdjacentForKeep)=0;


};

CATDeclareHandler(CATIATrim, CATIABooleanShape);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIABase.h"
#include "CATIAShape.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
