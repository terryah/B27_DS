/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATThreadDefs_h
#define CATThreadDefs_h

enum CatThreadSide {
        catRightSide,
        catLeftSide
};

enum CatThreadStandard {
        catMetricThinPitch,
        catMetricThickPitch
};

enum CatThreadPolarity {
        catThread,
        catTap
};

#endif
