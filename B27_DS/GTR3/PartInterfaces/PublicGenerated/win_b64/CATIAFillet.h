/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAFillet_h
#define CATIAFillet_h

#ifndef ExportedByPartPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __PartPubIDL
#define ExportedByPartPubIDL __declspec(dllexport)
#else
#define ExportedByPartPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByPartPubIDL
#endif
#endif

#include "CATFilletDefs.h"
#include "CATIADressUpShape.h"

extern ExportedByPartPubIDL IID IID_CATIAFillet;

class ExportedByPartPubIDL CATIAFillet : public CATIADressUpShape
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_FilletBoundaryRelimitation(CatFilletBoundaryRelimitation & oRelimitation)=0;

    virtual HRESULT __stdcall put_FilletBoundaryRelimitation(CatFilletBoundaryRelimitation iRelimitation)=0;

    virtual HRESULT __stdcall get_FilletTrimSupport(CatFilletTrimSupport & oRelimitation)=0;

    virtual HRESULT __stdcall put_FilletTrimSupport(CatFilletTrimSupport iRelimitation)=0;


};

CATDeclareHandler(CATIAFillet, CATIADressUpShape);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIABase.h"
#include "CATIAShape.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
