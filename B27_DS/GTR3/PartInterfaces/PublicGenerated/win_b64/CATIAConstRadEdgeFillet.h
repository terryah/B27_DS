/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAConstRadEdgeFillet_h
#define CATIAConstRadEdgeFillet_h

#ifndef ExportedByPartPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __PartPubIDL
#define ExportedByPartPubIDL __declspec(dllexport)
#else
#define ExportedByPartPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByPartPubIDL
#endif
#endif

#include "CATIAEdgeFillet.h"

class CATIALength;
class CATIAReference;
class CATIAReferences;

extern ExportedByPartPubIDL IID IID_CATIAConstRadEdgeFillet;

class ExportedByPartPubIDL CATIAConstRadEdgeFillet : public CATIAEdgeFillet
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_ObjectsToFillet(CATIAReferences *& oObjectsToFillet)=0;

    virtual HRESULT __stdcall get_Radius(CATIALength *& oRadius)=0;

    virtual HRESULT __stdcall AddObjectToFillet(CATIAReference * iObjectToFillet)=0;

    virtual HRESULT __stdcall WithdrawObjectToFillet(CATIAReference * iObjectToWithdraw)=0;


};

CATDeclareHandler(CATIAConstRadEdgeFillet, CATIAEdgeFillet);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATFilletDefs.h"
#include "CATIABase.h"
#include "CATIADressUpShape.h"
#include "CATIAFillet.h"
#include "CATIAShape.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
