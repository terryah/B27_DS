/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAAffinity_h
#define CATIAAffinity_h

#ifndef ExportedByPartPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __PartPubIDL
#define ExportedByPartPubIDL __declspec(dllexport)
#else
#define ExportedByPartPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByPartPubIDL
#endif
#endif

#include "CATIAShape.h"

class CATIAHybridShape;

extern ExportedByPartPubIDL IID IID_CATIAAffinity;

class ExportedByPartPubIDL CATIAAffinity : public CATIAShape
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_HybridShape(CATIAHybridShape *& oHybridShapeAffinity)=0;


};

CATDeclareHandler(CATIAAffinity, CATIAShape);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATGeometricType.h"
#include "CATIABase.h"
#include "CATIAGeometricElement.h"
#include "CATIAHybridShape.h"
#include "CATIAReference.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
