/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIADefeaturingFilter_h
#define CATIADefeaturingFilter_h

#ifndef ExportedByPartPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __PartPubIDL
#define ExportedByPartPubIDL __declspec(dllexport)
#else
#define ExportedByPartPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByPartPubIDL
#endif
#endif

#include "CATIABase.h"

extern ExportedByPartPubIDL IID IID_CATIADefeaturingFilter;

class ExportedByPartPubIDL CATIADefeaturingFilter : public CATIABase
{
    CATDeclareInterface;

public:

};

CATDeclareHandler(CATIADefeaturingFilter, CATIABase);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
