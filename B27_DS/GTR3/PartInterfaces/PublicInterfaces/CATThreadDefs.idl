#ifndef CATThreadDefs_IDL
#define CATThreadDefs_IDL
/*IDLREP*/

/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */

//=================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//=================================================================
//								   
// CATThreadDefs:						   
// enumerations for Mechanical thread
//								   
//=================================================================
// Usage notes:
//
//=================================================================
// Janv 2001 Creation                                 JL. Delvordre
//=================================================================


// Threading side
/**
 * Threading/Tapping side
 * It describes the side of thread/tap.
 * @param catRightSide
 *  The Thread/tap is right-threaded
 * @param catLeftSide
 *  The Thread/tap is left-threaded
 */
enum CatThreadSide	
{
	catRightSide,
	catLeftSide
};

// Thread/Tap Standard
/**
 * Thread/Tap Standard
 * It describes the standard to be used for thread/tap.
 * @param catMetricThinPitch
 *  The thread/tap standard is Metric Thin Pitch 
 * @param catMetricThickPitch
 *  The thread/tap standard is Metric Thick Pitch
 */
enum CatThreadStandard	
{
	catMetricThinPitch,
	catMetricThickPitch
};

// Thread/Tap Explicit polarity management
/**
 * Thread/Tap Polarity Management
 * It describes the thread polarity.
 * @param catThread
 *  The thread/tap polarity is explicitly set to Thread
 * @param catTap
 *  The thread/tap polarity is explicitly set to Tap
 */
enum CatThreadPolarity
{
	catThread,
	catTap
};

#endif // CATThreadDefs_IDL
