# COPYRIGHT DASSAULT SYSTEMES 2003
#======================================================================
# Imakefile for module CATARTDaemon.m
#======================================================================
#
#  Jul 2003  Creation: Code generated by the CAA wizard  aur
#======================================================================
#
# SHARED LIBRARY 
#
BUILT_OBJECT_TYPE=SHARED LIBRARY 
 
# DO NOT EDIT :: THE CAA2 WIZARDS WILL ADD CODE HERE
WIZARD_LINK_MODULES =  \
JS0GROUP  \
CATVisVR CATPolDaemon \
CATMathematics \
JS0MT \
VE0BASE \
CATObjectModelerBase \
CATVizBaseUUID
 \
JS0FM CATVisUUID 
# END WIZARD EDITION ZONE

LINK_WITH = $(WIZARD_LINK_MODULES) CATViz

# System dependant variables
#
OS = AIX
SYS_LIBS = -lpthreads
#
OS = HP-UX
#if os hpux_a
SYS_LIBS = -lc_r -ldce
#else
SYS_LIBS= -lpthread
#endif
#
OS = IRIX
SYS_LIBS = -lpthread
LOCAL_CCFLAGS = -DCATPTHREADS
#
OS = SunOS
LOCAL_LDFLAGS = -lpthread -lsocket
LOCAL_CCFLAGS = -mt
#
OS = Windows_NT
SYS_LIBS = wsock32.lib
