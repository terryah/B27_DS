# COPYRIGHT DASSAULT SYSTEMES 2003
#======================================================================
# Imakefile for module CATARTDriver.m
#======================================================================
#
#  Jul 2003  Creation: Code generated by the CAA wizard  aur
#======================================================================
#
# LOAD MODULE 
#
BUILT_OBJECT_TYPE=LOAD MODULE 
 
# DO NOT EDIT :: THE CAA2 WIZARDS WILL ADD CODE HERE
WIZARD_LINK_MODULES =  \
CATVisVR 
# END WIZARD EDITION ZONE

LINK_WITH = $(WIZARD_LINK_MODULES) \
CATPolDaemon CATVisualization JS0CORBA JS0MT YN000MAT CATARTDaemon

# System dependant variables
#
OS = AIX
#
OS = HP-UX
#
OS = IRIX
#
OS = SunOS
#
OS = Windows_NT
