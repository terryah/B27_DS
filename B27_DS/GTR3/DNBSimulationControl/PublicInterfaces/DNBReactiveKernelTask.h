/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBReactiveKernelTask.h
//      Brief description...
//
//==============================================================================
//
// Usage notes: 
//      Brief usage explanation...
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     chris        01/01/1999   Initial Development
//
//==============================================================================
#if DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _INCLUDE_DNBREACTIVEKERNELTASK
#define _INCLUDE_DNBREACTIVEKERNELTASK

    /**
 * Standard includes.
 */
#include <DNBSystemBase.h>
//#include <rw/thr/handbody.h>
#include <DNBSystemDefs.h>

#include <DNBSimulationControl.h>
#include <DNBSysHandleBase.h>
#include <DNBSysBodyBase.h>

class ExportedByDNBSimulationControl DNBReactiveKernelTaskBody : public DNBSysBodyBase
{
public:
    
    /**
     * I always do this.
     */
    typedef DNBReactiveKernelTaskBody                        Self;
    
    /**
     * Define a type which determines the priority order in which tasks will be
     * executed.  This is obviously a very simple partial order but should
     * work for now.
     */
    typedef unsigned int                                     Priority;
    
    /**
     * Interface to execute called by a ReactiveKernel thread.
     */
    virtual
    void
    execute()
        DNB_THROW_SPEC_ANY= 0;
    
    /**
     * Return the priority of the handler.  I may eventually make this a
     * functor as well which would do some cooler associative stuff to
     * establish a partial ordering.
     */
    inline
    Priority
    getPriority() const
        DNB_THROW_SPEC_ANY;
    
protected:
    
    /**
     * Constructor - protected because this is an interface.
     */
    DNBReactiveKernelTaskBody( const Priority& priority )
        DNB_THROW_SPEC_ANY;
    
    /**
     * copy constructor
     */
    DNBReactiveKernelTaskBody( const Self& rhs )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Destructor - protected because this is an interface.
     */
    virtual
    ~DNBReactiveKernelTaskBody()
        DNB_THROW_SPEC_NULL;
    
private:
    
    /**
     * Instance data.
     */
    Priority        priority_;
    
    /**
     * Undefined for now.
     */
    Self& operator=( const Self& rhs );
};

class ExportedByDNBSimulationControl DNBReactiveKernelTask : public DNBSysHandleBase
{
public:
    
    /**
     * I always do this.
     */
    typedef DNBReactiveKernelTask                            Self;
    
    /**
     * I always do this.
     */
    typedef DNBReactiveKernelTaskBody                        Body;
    
    /**
     * Define a type which determines the priority order in which tasks will be
     * executed.  This is obviously a very simple partial order but should
     * work for now.
     */
    typedef Body::Priority                                   Priority;
    
    enum
    {
        MinPrio         = 0,
        MaxPrio         = 100
    };
    
    /**
     * Default constructor.
     */
    DNBReactiveKernelTask()
        DNB_THROW_SPEC_ANY;

    /**
     * Body constructor.
     */
    DNBReactiveKernelTask( Body* body )
        DNB_THROW_SPEC_ANY;

    /**
     * Copy constructor.
     */
    DNBReactiveKernelTask( const Self& rhs )
        DNB_THROW_SPEC_ANY;

    /**
     * Destructor.
     */
    ~DNBReactiveKernelTask()
        DNB_THROW_SPEC_ANY;

    /**
     * Assignment operator.
     */
    inline
    Self&
    operator=( const Self& rhs )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Interface to execute called by a ReactiveKernel thread.
     */
    inline
    void
    execute()
        DNB_THROW_SPEC_ANY;
    
    /**
     * Return the priority of the handler.  I may eventually make this a
     * functor as well which would do some cooler associative stuff to
     * establish a partial ordering.
     */
    inline
    Priority
    getPriority() const
        DNB_THROW_SPEC_ANY;
    
    /**
     * Comparator - used to order handlers via their priority.
     */
    inline
    bool
    operator<( const Self& rhs ) const
        DNB_THROW_SPEC_ANY;
    
protected:
    
    /**
     * Convenience function to get at the body.
     */
    inline
    Body&
    body() const
        DNB_THROW_SPEC_ANY;
};

//
// Snarf the public inline definition file.
//
#include <DNBReactiveKernelTask.cc>

#endif // _INCLUDE_DNBREACTIVEKERNELTASK
