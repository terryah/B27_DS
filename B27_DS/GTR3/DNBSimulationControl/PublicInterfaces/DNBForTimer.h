/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBForTimer.h
//      For Timer implementation class -   Used for participatating
//      in clock advancement and time-based functor dispatching.
//  
//      DNBForTimer proposes a time based on the time passed on to it.
//
//==============================================================================
//
// Usage notes: 
//      Usually tied to the Time based Simlets(proclets). The time-based Simlets
//      register their timers with the Clock.  The timers participate in Clock
//      advancement and time-based functor dispatching. 
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     chris        01/01/1999   Initial Development
//     rtl          03/10/2004   Replace RWTValSortedVector with std set container
//
//==============================================================================
#if DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _INCLUDE_DNBFORTIMER
#define _INCLUDE_DNBFORTIMER

/**
 * Standard includes.
 */
#include <DNBSystemBase.h>
#include <scl_functional.h>         // for less
#include <scl_set.h>
#include <DNBSystemDefs.h>

/**
 * Deneb includes.
 */
#include <DNBSimulationControl.h>
#include <DNBTimeletTimer.h>

class ExportedByDNBSimulationControl DNBForTimerImpl : public DNBTimeletTimerImpl
{
public:
    
    /**
     * I always do this.
     */
    typedef DNBForTimerImpl           Self;
    
    /**
     * Pull the time vector definition into inner namespace.
     */
    typedef scl_set <Time, scl_less<Time> > TimeVector;
    
    /**
     * For time constructor.
     */
    DNBForTimerImpl( const Time& begin,
        const Time& end,
        const Time& increment )
        DNB_THROW_SPEC_ANY;
    
    /**
     * For time constructor.
     */
    DNBForTimerImpl( const TimeVector& sample )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Copy constructor.
     */
    DNBForTimerImpl( const Self& copy )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Assignment operforor.
     */
    virtual
    ~DNBForTimerImpl()
        DNB_THROW_SPEC_NULL;
    
    /**
     * Expired if the supplied time is greater than the 'for' time.
     */
    bool
    expired( const Time& time )
        DNB_THROW_SPEC_ANY;
    
    /**
     * If the timer has not expired, this simply proposes the single for time.
     */
    Time
    propose( const Time& minTime, const Time& maxTime )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Included only if the supplied time is the same as the 'for' time.
     */
    bool
    contains( const Time& minTime, const Time& maxTime )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Get the sample vector.
     */
    inline
    TimeVector
    getTimeVector() const
        DNB_THROW_SPEC_ANY;
    
private:
    
    /**
     * Store the time vector sample.
     */
    TimeVector                sample_;
};

/**
 * The 'for' timer interface.  For now, changing the 'for' time is disallowed
 * because I need COW semantics and I don't have time to properly implement
 * them.  TBD.
 */
class ExportedByDNBSimulationControl DNBForTimer : public DNBTimeletTimer
{
public:
    
    /**
     * I always do this.
     */
    typedef DNBForTimer               Self;
    
    /**
     * Shorten the type name for the implementation.
     */
    typedef DNBForTimerImpl           Impl;
    
    /**
     * Bring in the type name for the implementation.
     */
    typedef Impl::TimeVector          TimeVector;
    
    /**
     * Default constructor creates an empty time vector.
     */
    DNBForTimer()
        DNB_THROW_SPEC_ANY;
    
    /**
     * For time constructor.
     */
    DNBForTimer( const TimeVector& sample )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Copy constructor performs shallow copy.
     */
    DNBForTimer( const Self& copy )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Destructor.
     */
    virtual
     ~DNBForTimer()
        DNB_THROW_SPEC_ANY;
    
    /**
     * Assignment operforor.
     */
    inline
    Self&
    operator=( const Self& rhs )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Get the sample vector.
     */
    inline
    TimeVector
        getTimeVector() const
        DNB_THROW_SPEC_ANY;
    
protected:
    
    /**
     * Convenience for getting at the implementation.
     */
    inline
        Impl&
        body() const
        DNB_THROW_SPEC_ANY;
};

/**
 * Snarf the public inline definition file.
 */
#include <DNBForTimer.cc>

#endif // _INCLUDE_DNBFORTIMER

