//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2003
/** @CAA2Required */
/**********************************************************************/
/* DON T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPEAR AT ANY TIME */
/**********************************************************************/
//==============================================================================
//
// DNBSimGraph.h
// A stripped-down version of DNBGraph, customized to be used in 
// DNBProcessController
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     rtl          01/14/2003   Initial Implementation
//     rtl          03/26/2003   Added method validateCycle() to detect whether
//                               there are cycles in the graph
//     rtl          03/26/2004   Provide utility methods for retrieving graph's 
//                               root and terminal count
//     rtl          05/05/2005   Added methods for getting a nodeCount between
//                               a source and a target node, assuming that they
//                               are linked
//                               
//
//==============================================================================


#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _DNB_SIMGRAPH_H_
#define _DNB_SIMGRAPH_H_

#include <DNBSystemBase.h>
#include <scl_algorithm.h>
#include <scl_list.h>
#include <scl_map.h>
#include <scl_utility.h>		// for scl_pair used in .cc file
#include <DNBSystemDefs.h>

#include <DNBCollection.h>
#include <DNBSimulationControl.h>
#include <DNBException.h>

// Forward declare the edge implementation and counted pointer
template <class NodeData, class EdgeData>
class DNBGraphEdge;

//  Private type declarations
template <class NodeData, class EdgeData>
struct DNBGraphNode
{
    typedef
        DNBGraphNode<NodeData, EdgeData>
        self;

    typedef
        DNBGraphEdge<NodeData,EdgeData> *
        _DNBGraphEdge;

	enum colortype { white, grey, black, initial };

    DNBGraphNode( NodeData &data )
        DNB_THROW_SPEC_NULL;

    ~DNBGraphNode()
        DNB_THROW_SPEC_NULL;

    // Data
    NodeData data_;
	colortype color;

    // Edge information
    scl_list<_DNBGraphEdge, DNB_ALLOCATOR(_DNBGraphEdge) > inEdges_;
    scl_list<_DNBGraphEdge, DNB_ALLOCATOR(_DNBGraphEdge) > outEdges_;

private:
    // Disable these

    DNBGraphNode();

    DNBGraphNode( const self &rhs );

    self
    operator=( const self &rhs );
};




template <class NodeData, class EdgeData>
class DNBSimGraph;

template <class NodeData, class EdgeData>
class DNBGraphEdge
{
public:
    typedef DNBGraphEdge<NodeData, EdgeData> self;

    typedef DNBGraphNode<NodeData,EdgeData> *_DNBGraphNode;

    DNBGraphEdge( DNBSimGraph<NodeData,EdgeData> *graph,
                        const EdgeData &data,
                        _DNBGraphNode source,
                        _DNBGraphNode target )
        DNB_THROW_SPEC_NULL;

    DNBGraphEdge( const self &rhs )
        DNB_THROW_SPEC_NULL;

    ~DNBGraphEdge()
        DNB_THROW_SPEC_NULL;

    // Data
    EdgeData data_;
    _DNBGraphNode source_;
    _DNBGraphNode target_;
    DNBSimGraph<NodeData,EdgeData> *graph_;

private:
    // Disable these

    DNBGraphEdge();

    self
    operator=( const self &rhs );
};


template <class NodeData, class EdgeData>
class DNBGraphEdgeIterator;


template <class NodeData, class EdgeData>
class DNBGraphNodeIterator
{
public:


    typedef DNBGraphNode<NodeData,EdgeData> *_DNBGraphNode;



    typedef DNBGraphNodeIterator<NodeData,EdgeData> node_iterator;


    typedef DNBGraphEdgeIterator<NodeData,EdgeData> edge_iterator;


    typedef NodeData value_type;


    typedef value_type &reference;


    typedef const value_type &const_reference;


    typedef value_type *pointer;


    typedef const pointer const_pointer;


    typedef size_t size_type;


    typedef	ptrdiff_t difference_type;


    DNBGraphNodeIterator()
        DNB_THROW_SPEC_NULL;



    DNBGraphNodeIterator( const node_iterator &rhs )
        DNB_THROW_SPEC_NULL;


    virtual
    ~DNBGraphNodeIterator()
        DNB_THROW_SPEC_NULL;


    reference
    operator*() 
	DNB_THROW_SPEC_NULL;



    node_iterator &
    operator++()
	DNB_THROW_SPEC_NULL;


    node_iterator
    operator++( int )
	DNB_THROW_SPEC_NULL;



    node_iterator &
    operator--()
	DNB_THROW_SPEC_NULL;



    node_iterator
    operator--( int )
	DNB_THROW_SPEC_NULL;



    bool
    operator==( const node_iterator &rhs ) const
        DNB_THROW_SPEC_NULL;

    bool
    operator!=( const node_iterator &rhs ) const
        DNB_THROW_SPEC_NULL;

    bool
    operator<( const node_iterator &rhs ) const
        DNB_THROW_SPEC_NULL;


    size_t
    inDegree() const
	DNB_THROW_SPEC_NULL;


    size_t
    outDegree() const
	DNB_THROW_SPEC_NULL;


    size_t
    degree() const
	DNB_THROW_SPEC_NULL;


    bool
    isRoot() const
	DNB_THROW_SPEC_NULL;


    bool
    isTerminal() const
	DNB_THROW_SPEC_NULL;


    edge_iterator
    begin_in_edge()
        DNB_THROW_SPEC_NULL;




    edge_iterator
    end_in_edge()
        DNB_THROW_SPEC_NULL;

    edge_iterator
    begin_out_edge()
        DNB_THROW_SPEC_NULL;


    edge_iterator
    end_out_edge()
        DNB_THROW_SPEC_NULL;


private:
    // Characterizes the associated list and its iterator state

#if DNB_HAS_RW_STANDARD_LIB
    typedef scl_map <NodeData, _DNBGraphNode, scl_less<NodeData> > MAP;
#else
    typedef scl_map <NodeData, _DNBGraphNode, scl_less<NodeData> > MAP;
#endif

    MAP *map_;
    typename MAP::iterator  mapitr_;

    // This specifically uses the implementation so that the
    // iterators do not hang onto a reference to the node.
    DNBGraphNode<NodeData,EdgeData> *node_;

    friend class DNBSimGraph<NodeData,EdgeData>;
    friend class DNBGraphEdge<NodeData,EdgeData>;
    friend class DNBGraphEdgeIterator<NodeData,EdgeData>;
};



template <class NodeData, class EdgeData>
class DNBGraphEdgeIterator
{
public:

    typedef DNBGraphEdge<NodeData,EdgeData> *_DNBGraphEdge;

    typedef DNBGraphNodeIterator<NodeData,EdgeData> node_iterator;

    typedef DNBGraphEdgeIterator<NodeData,EdgeData> edge_iterator;

    typedef EdgeData value_type;
    
    typedef value_type &reference;

    typedef const value_type &const_reference;

    typedef value_type *pointer;

    typedef const pointer const_pointer;

    typedef size_t size_type;

    typedef ptrdiff_t difference_type;

    DNBGraphEdgeIterator() 
        DNB_THROW_SPEC_NULL;

    DNBGraphEdgeIterator( const edge_iterator &rhs )
        DNB_THROW_SPEC_NULL;

    virtual
    ~DNBGraphEdgeIterator()
        DNB_THROW_SPEC_NULL;

    reference 
    operator*() const
	DNB_THROW_SPEC_NULL;

    edge_iterator &
    operator++()
	DNB_THROW_SPEC_NULL;

    edge_iterator
    operator++( int )
	DNB_THROW_SPEC_NULL;


    edge_iterator &
    operator--()
	DNB_THROW_SPEC_NULL;

    edge_iterator
    operator--( int )
	DNB_THROW_SPEC_NULL;


    bool
    operator==( const edge_iterator &rhs ) const
        DNB_THROW_SPEC_NULL;

    bool
    operator!=( const edge_iterator &rhs ) const
        DNB_THROW_SPEC_NULL;

    bool
    operator<( const edge_iterator &rhs ) const
        DNB_THROW_SPEC_NULL;

    node_iterator
    getSource() const
	DNB_THROW_SPEC_NULL;

    node_iterator
    getTarget() const
        DNB_THROW_SPEC_NULL;

private:
    // Characterizes the associated list and its iterator state
    scl_list<_DNBGraphEdge, DNB_ALLOCATOR(_DNBGraphEdge) > *list_;
    typename scl_list<_DNBGraphEdge, DNB_ALLOCATOR(_DNBGraphEdge) >::iterator itr_;

    // This specifically uses the implementation so that the
    // iterators do not hang on to a reference to the edge.
    DNBGraphEdge<NodeData,EdgeData> *edge_;

    friend class DNBSimGraph<NodeData,EdgeData>;
    friend class DNBGraphNodeIterator<NodeData,EdgeData>;
};


template <class NodeData, class EdgeData>
class DNBSimGraph
{
public:


    typedef DNBSimGraph<NodeData,EdgeData> self;

    typedef DNBGraphNode<NodeData,EdgeData> *_DNBGraphNode;

    typedef DNBGraphEdge<NodeData,EdgeData> *_DNBGraphEdge;

    typedef DNBGraphNodeIterator<NodeData,EdgeData> node_iterator;

    typedef DNBGraphEdgeIterator<NodeData,EdgeData> edge_iterator;


    typedef NodeData node_value_type;

    typedef node_value_type &node_reference;

    typedef node_value_type *node_pointer;

    typedef size_t node_size_type;


    typedef EdgeData edge_value_type;


    typedef edge_value_type &edge_reference;


    typedef edge_value_type *edge_pointer;


    DNBSimGraph()
        DNB_THROW_SPEC_NULL;
         

    ~DNBSimGraph()
        DNB_THROW_SPEC_NULL;

    size_t
    nodeCount() const
        DNB_THROW_SPEC_NULL;

    size_t
    edgeCount() const
        DNB_THROW_SPEC_NULL;

    size_t
    rootCount() const
        DNB_THROW_SPEC_NULL;

    size_t
    terminalCount() const
        DNB_THROW_SPEC_NULL;

    bool
    isEmpty() const
        DNB_THROW_SPEC_NULL;

    node_iterator
    addNode( NodeData &nd )
        DNB_THROW_SPEC((scl_bad_alloc));

    void
    removeNode( node_iterator ni )
        DNB_THROW_SPEC_NULL;

    edge_iterator
    addEdge( EdgeData &ed, node_iterator &source, node_iterator &target )
        DNB_THROW_SPEC((scl_bad_alloc));


    void
    removeEdge( edge_iterator ei )
        DNB_THROW_SPEC_NULL;

    void
    clear()
        DNB_THROW_SPEC_NULL;

    void
    destroyGraph()
        DNB_THROW_SPEC_NULL;

	/*
	 * checks the graph to see if there is a cycle. It assumes that the graph
	 * is undirected.
	 * Returns true if there is no cycle, false otherwise
	 */
	bool
	validateCycle()
	    DNB_THROW_SPEC_ANY;
	/*
	bool BFS();
	    DNB_THROW_SPEC_ANY;

	bool DFS()
	    DNB_THROW_SPEC_ANY;
	*/

	bool DFS_Visit(node_iterator &)
	    DNB_THROW_SPEC_ANY;


    node_iterator
    begin_node()
        DNB_THROW_SPEC_NULL;


    node_iterator
    end_node()
        DNB_THROW_SPEC_NULL;

    edge_iterator
    begin_edge()
        DNB_THROW_SPEC_NULL;


    edge_iterator
    end_edge()
        DNB_THROW_SPEC_NULL;


    node_iterator
    begin_root()
        DNB_THROW_SPEC_NULL;


    node_iterator
    end_root()
        DNB_THROW_SPEC_NULL;


    node_iterator
    begin_terminal()
        DNB_THROW_SPEC_NULL;


    node_iterator
    end_terminal()
        DNB_THROW_SPEC_NULL;

    void
    resetColor(node_iterator& sourceNode, node_iterator& targetNode)
        DNB_THROW_SPEC_ANY;

    void
    nodeCount(node_iterator& sourceNode, node_iterator& targetNode, int& nodeCount)
        DNB_THROW_SPEC_ANY;





#if DNB_HAS_RW_STANDARD_LIB
    typedef scl_map <NodeData, _DNBGraphNode, scl_less<NodeData> > MapDB;
#else
    typedef scl_map <NodeData, _DNBGraphNode, scl_less<NodeData> > MapDB;
#endif

    typedef typename MapDB::iterator       MapDBIterator;

    typedef typename MapDB::value_type     MapDBValueType;

    typedef typename MapDB::key_type       MapDBKeyType;

    /*
     * Method for filling the map with values ( <Nodedata, node_iterator> pair )
     */
    void
    populateDB()
    DNB_THROW_SPEC_ANY;

    /*
     * find the Nodedata in the map
     */
    bool
    findDB(NodeData,node_iterator&)
    DNB_THROW_SPEC_ANY;


private:
    // Disable these until their correct semantics are identified.
    DNBSimGraph<NodeData,EdgeData> &operator=
				( const DNBSimGraph<NodeData,EdgeData> &right );
    DNBSimGraph( const DNBSimGraph<NodeData,EdgeData> &right );


    scl_list<_DNBGraphEdge, DNB_ALLOCATOR(_DNBGraphEdge) > edges_;

    // Map Database that contains <Nodedata, node_iterator> pair values.
    MapDB nodesMap;
    MapDB rootsMap;
    MapDB terminalsMap;

    friend class DNBGraphEdgeIterator<NodeData,EdgeData>;
};

#include <DNBSimGraph.cc>

#endif // _DNB_SIMGRAPH_H_
