/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview HRN trangara 01:08:20
 */
// --------------------------------------------------------


//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBAtTimer.cc
//      DNBAtTimer inline functions Implementation file
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     xxx          mm/dd/yyyy   Comment
//
//==============================================================================


//------------------------------------------------------------------------------
// Implements DNBAtTimerImpl::getAtTime
//------------------------------------------------------------------------------
inline
DNBSimTime
DNBAtTimerImpl::getAtTime() const
    DNB_THROW_SPEC_ANY
{
    return at_;
}


//------------------------------------------------------------------------------
// Implements DNBAtTimerImpl::setAtTime
//------------------------------------------------------------------------------
inline
void
DNBAtTimerImpl::setAtTime( const Time& time )
    DNB_THROW_SPEC_ANY
{
    at_ = time;
}


//------------------------------------------------------------------------------
// Implements DNBAtTimer::operator=
//------------------------------------------------------------------------------
inline
DNBAtTimer&
DNBAtTimer::operator=( const Self& rhs )
    DNB_THROW_SPEC_ANY
{
    return (Self&) DNBSysHandleBase::operator=( rhs );
}


//------------------------------------------------------------------------------
// Implements DNBAtTimer::getAtTime
//------------------------------------------------------------------------------
inline
DNBAtTimer::Time
DNBAtTimer::getAtTime() const
    DNB_THROW_SPEC_ANY
{
    return body().getAtTime();
}


//------------------------------------------------------------------------------
// Implements DNBAtTimer::setAtTime
//------------------------------------------------------------------------------
inline
void
DNBAtTimer::setAtTime( const Time& time )
    DNB_THROW_SPEC_ANY
{
    body().setAtTime( time );
}


//------------------------------------------------------------------------------
// Implements DNBAtTimer::body
//------------------------------------------------------------------------------
inline
DNBAtTimer::Impl&
DNBAtTimer::body() const
    DNB_THROW_SPEC_ANY
{
    return (Impl&) DNBSysHandleBase::body();
}

