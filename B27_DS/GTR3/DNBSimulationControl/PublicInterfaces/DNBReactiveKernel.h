/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBReactiveKernel.h
//      Brief description...
//
//==============================================================================
//
// Usage notes: 
//      Brief usage explanation...
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     chris        01/01/1999   Initial Development
//     smw			04/08/2005   Replaced RWPCValQueue with DNBSysPCValQueue
//     smw          04/13/2005   Replaced RWServerPool and RWRunnableFunction with our own
//									DNBSysThreadPool and DNBSysRunnableFunction
//     smw          05/06/2005   Migrated Synchronization primitives
//
//==============================================================================
#define USE_RW_PCQUEUE 0
#define USE_RW_THREADPOOL 0

#if DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _INCLUDE_DNBREACTIVEKERNEL
#define _INCLUDE_DNBREACTIVEKERNEL

/**
 * Standard includes.
 */
#include <DNBSystemBase.h>
#include <DNBSysFastMutexLock.h>
#include <DNBSysPCValQueue.h>
#include <DNBSysRunnableFunctor.h>
#include <DNBSysThreadPool.h>
#include <DNBSystemDefs.h>

#include <DNBSimulationControl.h>
#include <DNBReactiveKernelTask.h>
#include <DNBSimTrace.h>

class ExportedByDNBSimulationControl DNBReactiveKernel
{
public:
    
    /**
     * I always do this.
     */
    typedef DNBReactiveKernel                            Self;
    
    /**
     * Shorten the typename for the basic event type.
     */
    typedef DNBReactiveKernelTask                        Task;
    
    /**
     * Anonymous enum for default number of threads to be created in the pool.
     */
    enum
    {
        DefaultThreads     = 3
    };
    
    /**
     * Called to actually start processing agent requests.  This call will
     * result in the creation of the internal thread pool which dispatches
     * requests queued by the various reactive agents.
     */
    static
    void
    activate( size_t numThreads = DefaultThreads )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Called to stop the processing of agent requests.  This call will result
     * in the destruction of all threads in the thread pool.
     */
    static
    void
    deactivate()
        DNB_THROW_SPEC_ANY;
    
    /**
     * Returns the active state of reactive kernel.
     */
    static
    bool
    isActive()
        DNB_THROW_SPEC_ANY;
    
    /**
     * Schedule a handler to be executed in the thread pool.
     */
    
    static
    void
    execute( const Task& task )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Notify the kernel that a thread is about to go to sleep so it can
     * increase the size of the pool if need be.
     */
    static
    void
    sleep()
        DNB_THROW_SPEC_ANY;
    
    /**
     * Notify the kernel that a thread has awakened and is out of it's wait
     * state.  The will cause the kernel to defer pool expansion in case the
     * waking thread is about to become available for task execution.
     */
    static
    void
    wake()
        DNB_THROW_SPEC_ANY;
    
private:
    
    /**
     * Opaquify the type used to feed tasks to the thread pool.
     */
    typedef DNBSysPCValQueue<Task>                        TaskQueue;
    
    /**
     * Opaquify the type used to feed tasks to the thread pool.
     */
    typedef DNBSysThreadPool                              ThreadPool;
    
    /**
     * Shorten the lock guard type name.
     */
//  typedef RWMutexLock::LockGuard                        LockGuard;
//  typedef RWMutexLock::UnlockGuard                      UnlockGuard;

    typedef DNBSysFastMutexLock::LockGuard                LockGuard;
    typedef DNBSysFastMutexLock::UnlockGuard              UnlockGuard;
    
    /**
     * Constructor.
     */
    DNBReactiveKernel( size_t numThreads )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Destructor.
     */
    ~DNBReactiveKernel()
        DNB_THROW_SPEC_ANY;

    /**
     * The entry point where each thread in the thread pool will go to sit and
     * spin on the task queue.  This loop has the internal nuts and bolts to
     * control the thread pool without having to maintain a full-blown server
     * pool with multiple sub-queues.
     */
    static
    void
    mainloop()
    DNB_THROW_SPEC_ANY;
    
    /**
     * Instance data.
     */
    size_t                numSleeping_;
    ThreadPool            runPool_;
    TaskQueue             runQueue_;
    
    /**
     * Class data.
     */
    static Self*          kernel_;
    static size_t         activeCount_;
//  static RWMutexLock    contextLock_;
    static DNBSysFastMutexLock contextLock_;
};


#endif // _INCLUDE_DNBREACTIVEKERNEL


