/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBReactiveVar.h
//      Brief description...
//
//==============================================================================
//
// Usage notes: 
//      Brief usage explanation...
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     chris        01/01/1999   Initial Development
//     rtl          10/01/2000   Changed the addHandler() method to prevent
//                               multiple registration of a handler
//	   smw          05/08/2005   phased out the use of Rogue Wave sync primitives
//
//==============================================================================

#if DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _INCLUDE_DNBREACTIVEVAR
#define _INCLUDE_DNBREACTIVEVAR

/**
 * Standard includes.
 */
#include <DNBSystemBase.h>
#include <scl_list.h>
//#include <rw/thr/monitor.h>
#include <DNBSysMonitor.h>
//#include <rw/thr/mutex.h>
#include <DNBSysMutexLock.h>
#include <DNBSystemDefs.h>

#include <DNBSimulationControl.h>
#include <DNBReactiveHandler.h>
#include <DNBSimTrace.h>

template <class EvnT>
//class DNBReactiveVar : public RWMonitor<RWMutexLock>
class DNBReactiveVar : public DNBSysMonitor
{
public:
    
    /**
     * I always do this to localize the impact of naming in case I need to
     * change something later on.  
     */
    typedef DNBReactiveVar<EvnT>                          Self;
    
    /**
     * Opaquify the handler type to make it nicer to deal with.
     */
    typedef DNBReactiveHandler<EvnT>                      Handler;
    
    /**
     * Constructor.
     */
    DNBReactiveVar()
        DNB_THROW_SPEC_ANY;

    /**
     * Copy Constructor
     */
    DNBReactiveVar( const Self& copy )
        DNB_THROW_SPEC_ANY;

    /**
     * No destructor because I don't think these are going to be derived from
     * and it's more efficient not to provide one.
     */
    ~DNBReactiveVar()
        DNB_THROW_SPEC_ANY;

    /**
     * I'll support the assignment operator because it doesn't hurt, but I
     * don't know yet what it means to assign a controller.
     */
    Self&
    operator=( const Self& copy )
        DNB_THROW_SPEC_ANY;
    
    /**
     * The notifier function dispatches all of the handlers with the new value
     * and then transfers any exceptions back through to the caller.
     */
    void
    notify( EvnT& event )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Add an event handler which will be invoked when the notifier is called
     * with the value.  Note that there is no policy against multiple
     * registration of a handler nor is this considered to be incorrect from
     * the implementation..... chris
     */
    
    /**
     * This has been modified not to allow multiple registration of a handler
     * for house-keeping purpose ....... rtl
     */
    void
    addHandler( const Handler& handler )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Remove an event handler which would have otherwise been called when a
     * value is via notify.  If a handler has been multiply registered, then
     * the first instance will be unregistered.  If the handler is not
     * currently registered, then a precondition violation will be signalled.
     */
    void
    removeHandler( const Handler& handler )
        DNB_THROW_SPEC_ANY;
    
    /**
     *  The following relational operator is required to pacify the AIX C++
     *  compiler.
     */
    bool
    operator==( const Self &right ) const
        DNB_THROW_SPEC_ANY;
    
private:
    
    /**
     * Opaquify the handler database type so we can replace it with something
     * else should we ever get an auto-sorted container type again.  It also
     * makes this thing a bit nicer to deal with.
     */
    typedef scl_list<Handler,DNB_ALLOCATOR(Handler) >            HandlerList;
    
    /**
     * Instance data.
     */
    HandlerList    handlers_;
};

/**
 * Snarf the public inline definition file.
 */
#include <DNBReactiveVar.cc>

#endif // _INCLUDE_DNBREACTIVEVAR
