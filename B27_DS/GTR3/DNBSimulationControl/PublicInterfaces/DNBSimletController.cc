/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview HRN trangara 01:08:20
 */
// --------------------------------------------------------


//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBSimletController.cc
//      DNBSimletController inline functions implementation file
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     chris        01/01/1999   Initial Development
//     smw          04/08/2005   removed the DNBSimletController::raise() method
//
//==============================================================================



//------------------------------------------------------------------------------
// Implements DNBSimletController::addPhaseCallback
//------------------------------------------------------------------------------
inline
void
DNBSimletController::addPhaseCallback( const UpdatePhase& phase,
                                      const PhaseCallback& callback )
DNB_THROW_SPEC_ANY
{
    LockGuard lock( monitor() );
    phaseCallbacks_[phase].push_back( callback );
}




//------------------------------------------------------------------------------
// Implements DNBSimletController::removePhaseCallback
//------------------------------------------------------------------------------
inline
void
DNBSimletController::removePhaseCallback( const UpdatePhase& phase,
                                         const PhaseCallback& callback )
DNB_THROW_SPEC_ANY
{
    LockGuard lock( monitor() );
    phaseCallbacks_[phase].remove( callback );
}




//------------------------------------------------------------------------------
// Implements DNBSimletController::addCycleCallback
//------------------------------------------------------------------------------
inline
void
DNBSimletController::addCycleCallback( const CycleCallback& callback )
DNB_THROW_SPEC_ANY
{
    LockGuard lock( monitor() );
    cycleCallbacks_.push_back( callback );
}




//------------------------------------------------------------------------------
// Implements DNBSimletController::removeCycleCallback
//------------------------------------------------------------------------------
inline
void
DNBSimletController::removeCycleCallback( const CycleCallback& callback )
DNB_THROW_SPEC_ANY
{
    LockGuard lock( monitor() );
    cycleCallbacks_.remove( callback );
}




//------------------------------------------------------------------------------
// Implements DNBSimletController::getStep
//------------------------------------------------------------------------------
inline
DNBSimletController::Step
DNBSimletController::getStep() const
DNB_THROW_SPEC_ANY
{
#ifdef LATER
    LockGuard lock( monitor() );
#endif
    return getStepTr();
}




//------------------------------------------------------------------------------
// Implements DNBSimletController::getRunState
//------------------------------------------------------------------------------
inline
DNBSimletController::RunState
DNBSimletController::getRunState()
DNB_THROW_SPEC_ANY
{
#ifdef LATER
    LockGuard lock( monitor() );
#endif
    return getRunStateTr();
}



/*
//------------------------------------------------------------------------------
// Implements DNBSimletController::raise
//------------------------------------------------------------------------------
inline
void
DNBSimletController::raise() const
DNB_THROW_SPEC_ANY
{
    LockGuard lock( monitor() );
    if( exception_ )
    {
        exception_->raise();
    }
}
*/



//------------------------------------------------------------------------------
// Implements DNBSimletController::getStepTr
//------------------------------------------------------------------------------
inline
DNBSimletController::Step
DNBSimletController::getStepTr() const
DNB_THROW_SPEC_ANY
{
    return currentStep_;
}




//------------------------------------------------------------------------------
// Implements DNBSimletController::addRunStateHandler
//------------------------------------------------------------------------------
inline
void
DNBSimletController::addRunStateHandler( const RunStateHandler& handler )
DNB_THROW_SPEC_ANY
{
    runHandlers_.addHandler( handler );
}




//------------------------------------------------------------------------------
// Implements DNBSimletController::removeRunStateHandler
//------------------------------------------------------------------------------
inline
void
DNBSimletController::removeRunStateHandler( const RunStateHandler& handler )
DNB_THROW_SPEC_ANY
{
    runHandlers_.removeHandler( handler );
}




//------------------------------------------------------------------------------
// Implements DNBSimletController::addUpdateHandler
//------------------------------------------------------------------------------
inline
void
DNBSimletController::addUpdateHandler( const UpdateHandler& handler )
DNB_THROW_SPEC_ANY
{
    updateHandlers_.addHandler( handler );
}




//------------------------------------------------------------------------------
// Implements DNBSimletController::removeUpdateHandler
//------------------------------------------------------------------------------
inline
void
DNBSimletController::removeUpdateHandler( const UpdateHandler& handler )
DNB_THROW_SPEC_ANY
{
    updateHandlers_.removeHandler( handler );
}




//------------------------------------------------------------------------------
// Implements DNBSimletController::getRunStateTr
//------------------------------------------------------------------------------
inline
DNBSimletController::RunState
DNBSimletController::getRunStateTr() const
DNB_THROW_SPEC_ANY
{
    return runState_;
}



//------------------------------------------------------------------------------
// Implements operator==
//------------------------------------------------------------------------------
template <class Evnt>
inline  
bool
operator==( const DNBReactiveVar<Evnt> &, const DNBReactiveVar<Evnt> &)
{
    // does nothing!
    return true;
}
