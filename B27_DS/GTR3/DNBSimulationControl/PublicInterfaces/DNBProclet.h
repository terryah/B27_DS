/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBProclet.h
//   A Process representation. Each activity in a Process is represented
//   internally by one or a group of Proclets. It is the basic simulation 
//   object. Application-specific proclets are derived from this base proclet
//
//==============================================================================
//
// Usage notes: 
//   During the Activity build stage the Process tree is scanned and for each 
//   Activity "Build" interface defined on the late type of each Primitive 
//   Activity is invoked. This creates appropriate instances of Proclets.
//   The Proclet instances are added to the Process Controller in the order the 
//   corresponding activities appear in the Process tree.
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     chris        01/01/1999   Initial Development
//     rtl          25/08/2000   Changed the DNBProclet class in line with the 
//                               development of the DNBProcessController,
//                               in order to make sequencing, ordering and 
//                               scheduling of Proclet instances from the 
//                               Process Controller feasible
//     sha          05/18/2001   Changed triggerBreakpoint method: not static,
//                               modify its argument to take BreakpointLocation
//     rtl          04/15/2002   Code change for simulating part of process, introduce
//                               loops in process chain
//     rtl          05/02/2002   Provide "lock-free" methods for getting/setting
//                               Process Controller
//     rtl          06/03/2002   Included class macros to make the class traceable
//     rtl          06/03/2002   Provided method for resetting Start time of proclet.
//                               Provided "presetStartTime_", and "visited" data
//                               members. 
//     mmh          08/13/2002   Add new cleanUp() method
//     rtl          09/19/2002   Added getActiveProcletList() to return the list
//                               of active proclets during simulation. Used
//                               by the zone controller.
//     mmh          09/19/2002   Add support for RCS look-ahead
//     rtl          10/24/2002   Added new methods for re-using sub-graphs.
//                               Provide RTTI facility in proclet
//     rtl          12/18/2002   Override setClock method
//     rtl          03/07/2003   IR A0383906 Fix. Provide method setStartTime()
//     rtl          04/15/2003   Change RTTI macro to the one with no ctr
//     rtl          07/14/2003   IR A0401597 . Provide a member to store the
//                               wait duration of the proclet and method to
//                               set the wait duration
//     mmh          07/16/2003   Add method to remove all breakpoints
//     mmh          10/30/2003   Keep _plets_w_bkp as a map - fix CD when using
//                               Interference Window after stepping through the
//                               process
//     rtl          11/06/2003   Code change for using the new Trace facility
//     rtl          11/04/2003   Add method to set Resource data and stopTime
//     mmh          01/05/2005   Add Dump method for debugging purposes (Dump 
//                               graph command).
//     rtl          03/14/2005   Added member resHandle ( and related accessor and
//                               mutator methods )
//     rtl          05/04/2005   Add support for enabling/disabling a sequence
//                               of proclets.
//     rtl          08/25/2006   Support for simulation time point determination for 
//                               resources with behaviour
//     rtl          12/06/2006   IR 0561990 Fix. Code change for scheduling resource tasks
//                               based on their handles( and not based on their names )
//     rtl          12/19/2006   R18 Highlight ( LineTracking ). Schedule/ execute proclets
//                               based on their priorities
//     rtl          01/10/2007   Code cleanup. Remove activeBranch code
//     rtl          06/06/2007   A0586171 fix. Made setProcessed() method virtual
//
//==============================================================================

#if DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _INCLUDE_DNBPROCLET
#define _INCLUDE_DNBPROCLET

#include <DNBSystemBase.h>
#include <scl_map.h>
#include <DNBSystemDefs.h>

/**
 * Deneb includes
 */
#include <DNBSubTimer.h>
#include <DNBIntervalTimer.h>
#include <DNBAtTimer.h>
#include <DNBSimulationControl.h>
#include <DNBTimelet.h>
#include <DNBProcletMessage.h>
#include <DNBSimGraphTrace.h>

/**
 *Forward Declaration to avoid circular dependency
 */
class DNBProcessController;

/**
 * The DNBProclet interface
 */
class ExportedByDNBSimulationControl DNBProclet : public DNBTimelet
{
    DNB_DECLARE_SIMLET_NOCTR(DNBProclet);

public:
    
    /**
     * I always do this.
     */
    typedef DNBProclet       Self;

    typedef Self *Proclet;

	static const unsigned short DefaultPriority;
	static const unsigned short MaxPriority;
	static const unsigned short MinPriority;

    typedef scl_list<Proclet,DNB_ALLOCATOR(Proclet) > ProcletList;

    typedef ProcletList::iterator ProcListIterator;
  
    typedef DNBProcletMessage::MessageType Mtype;

    enum ProcletType {Default, Resource};
    
    enum BreakpointStatus {BreakON, BreakOFF};
    enum BreakpointLocation {BreakAtBegin, BreakAtEnd};
    enum BreakpointType  {BreakNone, BreakHalt, BreakDone};
    
    typedef int BreakpointData;

    typedef scl_map < Proclet, BreakpointLocation, scl_less<Proclet> > ProcletMap;
    
    struct Breakpoint
    {
        BreakpointStatus    status;
        BreakpointLocation  where;
        BreakpointType      type;
        BreakpointData      data;
        
        Breakpoint() : 
        status(BreakOFF), where(BreakAtBegin),
            type(BreakNone), data(0)
        {}
    };
    
    
    
    ~DNBProclet()
        DNB_THROW_SPEC_ANY;
    
    inline
    Time
    getScheduledStart() const
        DNB_THROW_SPEC_ANY;
    
    inline
    void
    setScheduledStart( const Time& scheduledStart )
        DNB_THROW_SPEC_ANY;

    inline
    Time
    getScheduledDuration() const
        DNB_THROW_SPEC_ANY;
    
    inline
    void
    setScheduledDuration( const Time& scheduledDuration )
        DNB_THROW_SPEC_ANY;

    
    inline
    Time
    getScheduledEnd() const
        DNB_THROW_SPEC_ANY;
    
    virtual
    void
    begin( const Time& time )
        DNB_THROW_SPEC_ANY;
    
    virtual
    void
    update( const Time& time )
        DNB_THROW_SPEC_ANY;
    
    virtual
    void
    end( const Time& time )
        DNB_THROW_SPEC_ANY;
    

    inline
    void
    setProcessController(DNBProcessController *pc) 
        DNB_THROW_SPEC_ANY;

    inline
    DNBProcessController*
    getProcessController()
       DNB_THROW_SPEC_ANY;

   
   //
   // The foll: two methods should be used with cation.
   // These are "lock-free" methods for getting/setting
   // the proclet's controller.
   // These can be used during building the graph before
   // the simulation. 
   // If you need to get/set the controller during the 
   // simulation run, then use the above two methods
   // 
    inline 
    void
    setProcessControllerTr(DNBProcessController *pc)
       DNB_THROW_SPEC_ANY;
    
    inline
    DNBProcessController*
    getProcessControllerTr()
       DNB_THROW_SPEC_ANY;
   

   // 
   // Check to see if the Controller is already set
   //
    inline
    bool
    isControllerSet()
       DNB_THROW_SPEC_ANY;

    inline
    TimeHandler& 
    getBeginHandler()
        DNB_THROW_SPEC_ANY;
    
    inline
    TimeHandler& 
    getUpdateHandler()
        DNB_THROW_SPEC_ANY;
    
    
    inline
    TimeHandler& 
    getEndHandler()
        DNB_THROW_SPEC_ANY;
    
    
    inline
    DNBAtTimer& 
    getBeginTimer()
        DNB_THROW_SPEC_ANY;
    
    
    inline
    DNBSubTimer& 
    getUpdateTimer()
        DNB_THROW_SPEC_ANY;
    
    inline
    DNBIntervalTimer& 
    getHeartBeatTimer()
        DNB_THROW_SPEC_ANY;
    /**
     *Set the Heart Beat Timer 
     */
    inline
    void
    setHeartBeatTimer(const Time& frequency)
        DNB_THROW_SPEC_ANY;
    
    /**
     *Change the HeartBeat Timer's rate
     */
    inline
    void
    setHeartBeatRate(const Time& newfrequency)
        DNB_THROW_SPEC_ANY;
    
    inline
    void
    shiftTimeBase(const Time& shift)
        DNB_THROW_SPEC_ANY;
    
    /**
     *Check to see if the heartbeat Timer is enabled
     */
    inline
    bool
    isheartbeatEnabled()
        DNB_THROW_SPEC_ANY;
    
    
    void
   _begin(const TimeEvent& evTime )
        DNB_THROW_SPEC_ANY;
    
    
    void
    _update(const TimeEvent& evTime ) 
        DNB_THROW_SPEC_ANY;
    
    void
        _end( const TimeEvent& evTime )
        DNB_THROW_SPEC_ANY;
    
    
    void
    setBreakpoint( BreakpointLocation where, BreakpointType type, BreakpointData data )
        DNB_THROW_SPEC_NULL;
    
    void
    resetBreakpoint( BreakpointLocation where )
        DNB_THROW_SPEC_NULL;
    
    void
    triggerBreakpoint( BreakpointLocation where )
        DNB_THROW_SPEC_NULL;
    
    static bool
    isBreakpointTriggered( BreakpointData&, bool reset = true )
        DNB_THROW_SPEC_NULL;

    bool
    isBreakpoint( BreakpointLocation where )
        DNB_THROW_SPEC_NULL;
    
    
    /**
     *Check if the proclet is processed or not
     *A proclet is considered 'processed' when its end method is called
     */
    inline
    bool
    isProcessed()
        DNB_THROW_SPEC_NULL;
    
	virtual
    void
    setProcessed(bool value=false)
        DNB_THROW_SPEC_NULL;
    
    inline
    void
    resetprevTime()
        DNB_THROW_SPEC_NULL;

    /**
     * Used by the Controller to reset the Start times of the proclets registered with the
     * Controller. Called from the Controller's reset method. 
     */
    void
    resetStartTime()
        DNB_THROW_SPEC_ANY;

    /**
     * Sets the start time of the proclet to the specified time. Every time the proclet is
     * reset the scheduledStartTime of the proclet is reinitialized with this value
     */
    inline
    void
    setStartTime(const Time& startTime)
        DNB_THROW_SPEC_ANY;


    inline
    void
    setLoop(bool)
        DNB_THROW_SPEC_ANY;

    inline
    bool
    getLoop()
        DNB_THROW_SPEC_ANY;

    inline
    void
    setVisited(bool)
        DNB_THROW_SPEC_ANY;

    inline
    bool
    getVisited()
        DNB_THROW_SPEC_ANY;
    
    virtual
    void
    cleanUp()
        DNB_THROW_SPEC_ANY;

    // Support for RCS look-ahead
    void
    setSyncTarget( bool iSyncTarget )
        DNB_THROW_SPEC_ANY;
    bool
    getSyncTarget()
        DNB_THROW_SPEC_ANY;

    void
    setLastTarget( bool iLastTarget )
        DNB_THROW_SPEC_ANY;
    bool
    getLastTarget()
        DNB_THROW_SPEC_ANY;

    void
    setTempTarget( bool iTempTarget )
        DNB_THROW_SPEC_ANY;
    bool
    getTempTarget()
        DNB_THROW_SPEC_ANY;


    ProcletList
    getActiveProcletList()
        DNB_THROW_SPEC_ANY;

    /**
     * Returns the map of proclets that have a breakpoint set.
     */
    static ProcletMap
    getProcletBkpMap()
        DNB_THROW_SPEC_ANY;

    /**
     * Resets all the breakpoints from all the proclets.
     */
    static void
    resetAllBreakpoints()
        DNB_THROW_SPEC_ANY;

    /*
     * sets the Clock 
     */
    inline
    virtual
    void
    setClock( DNBTimeletClock* clock )
        DNB_THROW_SPEC_ANY;

    void
    setEnabled(bool value)
        DNB_THROW_SPEC_ANY;
    bool
    getEnabled()
        DNB_THROW_SPEC_ANY;

    inline
    void
    setProcletType(ProcletType )
        DNB_THROW_SPEC_ANY;

    inline
    void
    setResourceName(scl_wstring )
        DNB_THROW_SPEC_ANY;

    inline
    void
    setTaskName(scl_wstring )
        DNB_THROW_SPEC_ANY;

    inline
    ProcletType
    getProcletType()
        DNB_THROW_SPEC_ANY;

    inline
    scl_wstring
    getResourceName()
        DNB_THROW_SPEC_ANY;

    inline
    scl_wstring
    getTaskName()
        DNB_THROW_SPEC_ANY;

    inline 
    Time
    getStopTime()
        DNB_THROW_SPEC_ANY;

    inline
    void
    setStopTime(const Time &time)
        DNB_THROW_SPEC_ANY;

    void
    setResourceData(scl_wstring &resName, scl_wstring &TaskName)
        DNB_THROW_SPEC_ANY;

    void
    setResourceData(void * resHandle, void * taskHandle )
        DNB_THROW_SPEC_ANY;
	

    virtual 
    scl_wstring
    dumpCustomPletInfo()
        DNB_THROW_SPEC_ANY;

    virtual
    void
    setCustomPletInfo( scl_wstring& customStr )
        DNB_THROW_SPEC_ANY;

    void
    setResourceHandle(void * )
        DNB_THROW_SPEC_ANY;

    void
    setTaskHandle(void * )
        DNB_THROW_SPEC_ANY;

    const void *
    getResourceHandle( )
        DNB_THROW_SPEC_ANY;


    const void *
    getTaskHandle( )
        DNB_THROW_SPEC_ANY;

    void
    setActivated(bool value)
        DNB_THROW_SPEC_ANY;
    bool
    getActivated()
        DNB_THROW_SPEC_ANY;

	void
	setReady( bool isReady)
		DNB_THROW_SPEC_ANY;

	bool
	isReady()
		DNB_THROW_SPEC_ANY;


	virtual bool
	isReadyToBeProcessed(const Time &time)
		DNB_THROW_SPEC_ANY;

    void
    setStopPletFlag( int iFlag )
        DNB_THROW_SPEC_ANY;
    int
    getStopPletFlag()
        DNB_THROW_SPEC_ANY;

	void
	setPriority( unsigned short value )
		DNB_THROW_SPEC_ANY;

	unsigned short
	getPriority()
		DNB_THROW_SPEC_ANY;

protected:
    /**
     *Constructor
     */
    DNBProclet( const Time& scheduledStart = 0,
        const Time& scheduledDuration = 0, 
        const Time& updateFrequency = 0,
		unsigned short proirity = DefaultPriority) 
        DNB_THROW_SPEC_ANY;
    
    inline
    Time
    getScheduledStartTr() const
        DNB_THROW_SPEC_ANY;
    
    inline
    void
    setScheduledStartTr( const Time& scheduledStart )
        DNB_THROW_SPEC_ANY;
    
    inline
    Time
    getScheduledDurationTr() const
        DNB_THROW_SPEC_ANY;
    
    inline
    void
    setScheduledDurationTr( const Time& scheduledDuration )
        DNB_THROW_SPEC_ANY;
    
    
    inline
    Time
    getScheduledEndTr() const
        DNB_THROW_SPEC_ANY;

    virtual
    void
    setDuration( const Time& scheduledDuration)
        DNB_THROW_SPEC_ANY;
    
    scl_wstring          dumpInfo_;
	bool                 isReady_;
    
private:
    
    Time                 scheduledStart_;
    Time                 scheduledDuration_;
    Time                 scheduledEnd_;
    Time                 prevTime_;
    Time                 waitDuration_;

    Time                 stopTime_;


    // Represents a start time delay Activity. 
    // Used while resetting the start times of the proclet
    Time                 presetStartTime_;
    
    /**
     *Timers
     */
    DNBAtTimer           beginTimer_;
    DNBSubTimer          updateTimer_;
    DNBIntervalTimer     heartbeatTimer_;
    
    /**
     *TimeHandlers
     */
    TimeHandler          beginCron_;
    TimeHandler          updateCron_;
    TimeHandler          endCron_;
    
    bool                 processed_;


    /*
     *Flag that indicates if this proclet is the beginning node of a loop
     *
     */
    bool                 isLoop;

    bool                 visited;

    
    DNBProcessController *Controller_;
    
    Breakpoint           _bkBegin;
    Breakpoint           _bkEnd;
    
    static Breakpoint    _BreakpointTriggered;
    static bool          _TriggerSet;
    
    // Support for RCS look-ahead
    bool                _isSyncTarget;
    bool                _isLastTarget;
    bool                _isTempTarget;


    bool                isActivated;
    int                 activateCount;

    ProcletType         procletType;

    scl_wstring         resourceName;
    scl_wstring         taskName;

    void *              resourceHandle;
	void *              taskHandle;

    static ProcletMap   _plets_w_bkp;

    bool                isEnabled;

    int                 isStopPlet_;

	unsigned short      priority_;

};

#include <DNBProclet.cc>

#endif //_INCLUDE_DNBPROCLET

