/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview HRN trangara 01:08:20
 * @quickReview mmg 05:05:20
 */
// --------------------------------------------------------


//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBReactiveHandler.cc
//      Brief description...
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     chris        01/01/1999   Initial Development
//     rtl          06/18/2001   Dispatched the handler itself instead of a copy
//                               cloned from the handler (see CLONE_HANDLER)
//     rtl          05/20/2005   Change the way exceptions are handled in the
//                               handler's execute method ( removal of RW's Threaded
//                               exceptions) 
//
//==============================================================================

#define CLONE_HANDLER

//------------------------------------------------------------------------------
// Implements DNBReactiveHandlerBody<EvnT>::dispatch
//------------------------------------------------------------------------------
template <class EvnT>
bool
DNBReactiveHandlerBody<EvnT>::dispatch( EvnT& event )
DNB_THROW_SPEC_ANY
{
    LockGuard lock( monitor() );
    
    //
    // If the filter handle is invalid, thats a special way (for now) of saying
    // 'always' dispatch the handler.  I'd eventually like to find a way to
    // make the filter type a tepmlate parameter, but that would require some
    // careful thought so for now...
    //
    
    bool check = (filter_.isValid()) ? filter_( event ) : true;
    
    //
    // Check the filter before sending this thing down to the kernel for
    // execution.  This is an optimization to avoid unnecessary clutter in the
    // kernel's execution queue.
    //
    
    if( check )
    {
#ifdef CLONE_HANDLER	
        //
        // dispatch a copy of this handler to the kernel for its execution. Sending the
        // "this" body instance to the kernel causes a race condition. 
        //
        Self *cpy = clone();
        cpy->intern( event );
        lock.release();
        
        DNBReactiveHandler<EvnT> cpyHandle=cpy;
        
        DNBReactiveKernel::execute((const DNBReactiveKernelTask &) cpyHandle);
#else
        this->intern( event );
        lock.release();

        DNBReactiveHandler<EvnT> cpyHandle=this;

        DNBReactiveKernel::execute((const DNBReactiveKernelTask &) cpyHandle);
#endif
    }
    
    return check;
    
}


//------------------------------------------------------------------------------
// Implements DNBReactiveHandlerBody<EvnT>::intern
//------------------------------------------------------------------------------
template <class EvnT>
void
DNBReactiveHandlerBody<EvnT>::intern( EvnT& event )
DNB_THROW_SPEC_ANY
{
    LockGuard lock( monitor() );
    
    event.bind();
    event_     = event;
    signalled_ = false;
}

template <class EvnT>
typename
DNBReactiveHandler<EvnT>::Self&
DNBReactiveHandler<EvnT>::operator=( const Self& rhs )
DNB_THROW_SPEC_ANY
{
    return (Self&) DNBSysHandleBase::operator=( rhs );
}



//------------------------------------------------------------------------------
// Implements DNBReactiveHandler<EvnT>::body
//------------------------------------------------------------------------------
template <class EvnT>
inline
typename
DNBReactiveHandler<EvnT>::Body&
DNBReactiveHandler<EvnT>::body() const
DNB_THROW_SPEC_ANY
{
    return (Body&) DNBSysHandleBase::body();
}



//
// --- Out-of-line function definitions ---
//


//------------------------------------------------------------------------------
// Implements DNBReactiveHandlerBody<EvnT>::execute
//------------------------------------------------------------------------------
template <class EvnT>
void
DNBReactiveHandlerBody<EvnT>::execute()
DNB_THROW_SPEC_ANY
{
    
/*This lock had  to be commented out to take care of the 
deadlock problem. It is still not very clear as to
how this results in deadlock. But as of now 
removing this code solves the deadlock problem. Hence it 
stays like that.........rtl 
    */
    
#if defined(_SUNOS_SOURCE)
    LockGuard lock( monitor() );
#endif
    
    
    //
    // Invoke the handling function with the cached event values and
    // then see if any exceptions pop up.
    //

   
    try
    {
        invoke( event_ );
    }
    
    //
    // If it's a Deneb exception, then we know how to deal with that by 
    // caching a copy in the event.
    //
    
    catch( DNBEStop& ex )
    {
        signalled_ = true;
        event_.insertExceptionData( scl_string("Stop"), ex.getFormatStr(), ex.getSrcFile(), ex.getSrcLine() );
        event_.signal();
    }
    
    catch( DNBEHalt& ex )
    {
        signalled_ = true;
        event_.insertExceptionData( scl_string("Halt"), ex.getFormatStr(), ex.getSrcFile(), ex.getSrcLine() );
        event_.signal();
    }


    catch( DNBEBreak& ex )
    {
        signalled_ = true;
        event_.insertExceptionData( scl_string("Break"), ex.getFormatStr(), ex.getSrcFile(), ex.getSrcLine() );
        event_.signal();
    }
    
    catch( DNBEDone& ex )
    {
        signalled_ = true;
        event_.insertExceptionData( scl_string("Done"), ex.getFormatStr(), ex.getSrcFile(), ex.getSrcLine() );
        event_.signal();
    }
    
    catch( DNBException& ex )
    {
        signalled_ = true;
        event_.insertExceptionData( scl_string("Done"), ex.getFormatStr(), ex.getSrcFile(), ex.getSrcLine() );
        event_.signal();
    }

    /*
    catch( RWTHRxmsg& ex )
    {
        signalled_ = true;
        event_.signal( ex.clone() );
    }
    */
    
    //
    // Otherwise we don't know what it was so for now just send an unknown
    // exception back.  I will eventually create a compatible exception
    // class which propogates other exception types as well.
    //
    
    catch ( ... )
    {
        signalled_ = true;

        event_.insertExceptionData( scl_string("Done"), 
									scl_wstring( L"Unknown Exception Caught" ),
									__FILE__,
									__LINE__ );
        event_.signal();
    }
    
    //
    // If nothing bad already happened, then signal that we're already done.
    //
    
    if( !signalled_ )
    {
        signalled_ = true;
        event_.signal();
    }
}



//------------------------------------------------------------------------------
// Implements DNBReactiveHandlerBody<EvnT>::DNBReactiveHandlerBody
//------------------------------------------------------------------------------
template <class EvnT>
DNBReactiveHandlerBody<EvnT>::DNBReactiveHandlerBody( const Filter& filter,
                                                     const Priority& priority )
                                                     DNB_THROW_SPEC_ANY
                                                     : DNBReactiveKernelTaskBody( priority ),
                                                     event_(),
                                                     signalled_( false ),
                                                     filter_( filter )
{
}



//------------------------------------------------------------------------------
// Implements DNBReactiveHandlerBody<EvnT>::getFilter
//------------------------------------------------------------------------------
template <class EvnT>
inline
const
typename
DNBReactiveHandlerBody<EvnT>::Filter&
DNBReactiveHandlerBody<EvnT>::getFilter() const
DNB_THROW_SPEC_ANY
{
    return filter_;
}

template <class EvnT>
DNBReactiveHandlerBody<EvnT>::~DNBReactiveHandlerBody()
DNB_THROW_SPEC_NULL
{
    //
    // If the handler gets blown away before it's been dispatched and there's
    // an unsignalled event, then signal before we die.
    //
    
    if( event_.isValid() && !signalled_ )
    {
        event_.signal();
    }
}




//------------------------------------------------------------------------------
// Implements DNBReactiveHandler<EvnT>::DNBReactiveHandler
//------------------------------------------------------------------------------
template <class EvnT>
DNBReactiveHandler<EvnT>::DNBReactiveHandler()
DNB_THROW_SPEC_ANY
: DNBReactiveKernelTask()
{
}



//------------------------------------------------------------------------------
// Implements DNBReactiveHandler<EvnT>::dispatch
//------------------------------------------------------------------------------
template <class EvnT>
bool
DNBReactiveHandler<EvnT>::dispatch( EvnT& event )
DNB_THROW_SPEC_ANY
{
    return body().dispatch( event );
}




//------------------------------------------------------------------------------
// Implements DNBReactiveHandler<EvnT>::DNBReactiveHandler
//------------------------------------------------------------------------------
template <class EvnT>
DNBReactiveHandler<EvnT>::DNBReactiveHandler( Body* body )
DNB_THROW_SPEC_ANY
: DNBReactiveKernelTask( body )
{
}




//------------------------------------------------------------------------------
// Implements DNBReactiveHandler<EvnT>::DNBReactiveHandler
//------------------------------------------------------------------------------
template <class EvnT>
DNBReactiveHandler<EvnT>::DNBReactiveHandler( const Self& copy )
DNB_THROW_SPEC_ANY
: DNBReactiveKernelTask( copy )
{
}




//------------------------------------------------------------------------------
// Implements DNBReactiveHandler<EvnT>::~DNBReactiveHandler
//------------------------------------------------------------------------------
template <class EvnT>
DNBReactiveHandler<EvnT>::~DNBReactiveHandler()
DNB_THROW_SPEC_ANY
{
}
