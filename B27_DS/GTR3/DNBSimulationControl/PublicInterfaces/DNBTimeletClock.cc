/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview HRN trangara 01:08:20
 * @quickreview SHA 04:03:10
 */
// --------------------------------------------------------


//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBTimeletClock.cc
//      DNBTimeletClock inline functions implementation file
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     xxx          mm/dd/yyyy   Comment
//     rtl          03/10/2004   Replace tvdlist with std list (scl_list)
//
//==============================================================================



//------------------------------------------------------------------------------
// Implements DNBTimeletClock::addCycleCallback
//------------------------------------------------------------------------------
inline
void
DNBTimeletClock::addCycleCallback( const CycleCallback& callback )
DNB_THROW_SPEC_ANY
{
    LockGuard lock( monitor() );
    cycleCallbacks_.push_back( callback );
}



//------------------------------------------------------------------------------
// Implements DNBTimeletClock::removeCycleCallback
//------------------------------------------------------------------------------
inline
void
DNBTimeletClock::removeCycleCallback( const CycleCallback& callback )
DNB_THROW_SPEC_ANY
{
    LockGuard lock( monitor() );
    cycleCallbacks_.remove( callback );
}



//------------------------------------------------------------------------------
// Implements DNBTimeletClock::addTimeHandler
//------------------------------------------------------------------------------
inline
void
DNBTimeletClock::addTimeHandler( const TimeHandler& handler )
DNB_THROW_SPEC_ANY
{
    DNB_PRECONDITION( handler.isValid() );
    timeHandlers_.addHandler( handler );
}




//------------------------------------------------------------------------------
// Implements DNBTimeletClock::removeTimeHandler
//------------------------------------------------------------------------------
inline
void
DNBTimeletClock::removeTimeHandler( const TimeHandler& handler )
DNB_THROW_SPEC_ANY
{
    DNB_PRECONDITION( handler.isValid() );
    timeHandlers_.removeHandler( handler );
}




//------------------------------------------------------------------------------
// Implements DNBTimeletClock::addTimer
//------------------------------------------------------------------------------
inline
void
DNBTimeletClock::addTimer( const Timer& timer )
DNB_THROW_SPEC_ANY
{
    LockGuard lock( monitor() );
    addTimerTr( timer );
}




//------------------------------------------------------------------------------
// Implements DNBTimeletClock::removeTimer
//------------------------------------------------------------------------------
inline
void
DNBTimeletClock::removeTimer( const Timer& timer )
DNB_THROW_SPEC_ANY
{
    LockGuard lock( monitor() );
    removeTimerTr( timer );
}




//------------------------------------------------------------------------------
// Implements DNBTimeletClock::getTime
//------------------------------------------------------------------------------
inline
DNBSimTime
DNBTimeletClock::getTime() const
DNB_THROW_SPEC_ANY
{
    LockGuard lock( monitor() );
    return getTimeTr();
}


//------------------------------------------------------------------------------
// Implements DNBTimeletClock::addTimerTr
//------------------------------------------------------------------------------
inline
void
DNBTimeletClock::addTimerTr( const Timer& timer )
DNB_THROW_SPEC_ANY
{
    DNB_PRECONDITION( timer.isValid() );
    timers_.push_front(timer);
}




//------------------------------------------------------------------------------
// Implements DNBTimeletClock::removeTimerTr
//------------------------------------------------------------------------------
inline
void
DNBTimeletClock::removeTimerTr( const Timer& timer )
DNB_THROW_SPEC_ANY
{
    DNB_PRECONDITION( timer.isValid() );
    timers_.remove( timer );
}




//------------------------------------------------------------------------------
// Implements DNBTimeletClock::getTimeTr
//------------------------------------------------------------------------------
inline
DNBSimTime
DNBTimeletClock::getTimeTr() const
DNB_THROW_SPEC_ANY
{
    return currentTime_;
}


//------------------------------------------------------------------------------
// Implements DNBTimeletClock::addTimeCallback
//------------------------------------------------------------------------------
inline
void
DNBTimeletClock::addTimeCallback(const TimePhase& phase,const TimeCallback& callback )
DNB_THROW_SPEC_ANY
{
    LockGuard lock( monitor() );
    addTimeCallbackTr( phase, callback );
}



//------------------------------------------------------------------------------
// Implements DNBTimeletClock::removeTimeCallback
//------------------------------------------------------------------------------
inline
void
DNBTimeletClock::removeTimeCallback(const TimePhase& phase,const TimeCallback& callback )
DNB_THROW_SPEC_ANY
{
    LockGuard lock( monitor() );
    removeTimeCallbackTr( phase, callback );
}



//------------------------------------------------------------------------------
// Implements DNBTimeletClock::addTimeCallbackTr
//------------------------------------------------------------------------------
inline
void
DNBTimeletClock::addTimeCallbackTr( const TimePhase& phase,const TimeCallback& callback )
DNB_THROW_SPEC_ANY
{
    timeCallbacks_[phase].push_back( callback );
}



//------------------------------------------------------------------------------
// Implements DNBTimeletClock::removeTimeCallbackTr
//------------------------------------------------------------------------------
inline
void
DNBTimeletClock::removeTimeCallbackTr( const TimePhase& phase,const TimeCallback& callback )
DNB_THROW_SPEC_ANY
{
    timeCallbacks_[phase].remove( callback );
}

