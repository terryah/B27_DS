/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview HRN trangara 01:08:20
 */
// --------------------------------------------------------


//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBReactiveKernelTask.cc
//      Brief description...
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     xxx          mm/dd/yyyy   Comment
//
//==============================================================================


//------------------------------------------------------------------------------
// Implements DNBReactiveKernelTaskBody::getPriority
//------------------------------------------------------------------------------
inline
DNBReactiveKernelTaskBody::Priority
DNBReactiveKernelTaskBody::getPriority() const
    DNB_THROW_SPEC_ANY
{
    return priority_;
}



//------------------------------------------------------------------------------
// Implements DNBReactiveKernelTask::operator=
//------------------------------------------------------------------------------
inline
DNBReactiveKernelTask&
DNBReactiveKernelTask::operator=( const Self& rhs )
    DNB_THROW_SPEC_ANY
{
    return (Self&) DNBSysHandleBase::operator=( rhs );
}



//------------------------------------------------------------------------------
// Implements DNBReactiveKernelTask::execute
//------------------------------------------------------------------------------
inline
void
DNBReactiveKernelTask::execute()
    DNB_THROW_SPEC_ANY
{
    body().execute();
}




//------------------------------------------------------------------------------
// Implements DNBReactiveKernelTask::getPriority
//------------------------------------------------------------------------------
inline
DNBReactiveKernelTask::Priority
DNBReactiveKernelTask::getPriority() const
    DNB_THROW_SPEC_ANY
{
    return body().getPriority();
}



//------------------------------------------------------------------------------
// Implements DNBReactiveKernelTask::operator<
//------------------------------------------------------------------------------
inline
bool
DNBReactiveKernelTask::operator<( const Self& rhs ) const
    DNB_THROW_SPEC_ANY
{
    //
    // We want it sorted in descending order so we adjust from MaxPrio.
    //

    return (( MaxPrio - getPriority() ) < ( MaxPrio - rhs.getPriority() ));
}



//------------------------------------------------------------------------------
// Implements DNBReactiveKernelTask::body
//------------------------------------------------------------------------------
inline
DNBReactiveKernelTask::Body&
DNBReactiveKernelTask::body() const
    DNB_THROW_SPEC_ANY
{
    return (Body&) DNBSysHandleBase::body();
}

