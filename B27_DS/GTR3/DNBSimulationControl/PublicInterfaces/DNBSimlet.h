/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBSimlet.h
//      The DNBSimlet class serves as the base class for the simulation control
//      model.  It is still considered to be abstract since it adds only 
//      the message management implementation and does nothing else.
//
//==============================================================================
//
// Usage notes: 
//      Brief usage explanation...
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     chris        01/01/1999   Initial Development
//     mmg          12/11/2002   Modify setContrllerTr() to add the update
//                               handlers selectively
//     rtl          10/24/2002   Provide RTTI facility
//     rtl          12/16/2002   Modify SetControllerTr() to add the runstate
//                               handlers selectively.  Provide methods to
//                               add/remove handlers to/from the controller
//     rtl          02/24/2003   Move handler methods from protected to public 
//                               section
//     rtl          04/15/2003   Provide new RTTI macro with default constructor
//                               for the "_CLASS"
//     smw          05/08/2005   phased out the use of Rogue Wave sync primitives
//
//==============================================================================
#if DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _INCLUDE_DNBSIMLET
#define _INCLUDE_DNBSIMLET

/**
 * Standard includes
 */
#include <DNBSystemBase.h>
//#include <rw/thr/monitor.h>
//#include <rw/thr/mutex.h>
//#include <rw/thr/recursiv.h>
#include <DNBSysMonitor.h>
#include <DNBSystemDefs.h>

/**
 * Deneb includes
 */
#include <DNBSimulationControl.h>
#include <DNBSimletController.h>
#include <DNBDynamicObject.h>

/**
 * MACROS
 */
#define _DNB_DECLARE_METHOD(_CLASS)                                      \
public:                                                                  \
    _CLASS( const _CLASS &right, CopyMode mode )                         \
        DNB_THROW_SPEC((DNBEResourceLimit));


#define _DNB_DEFINE_METHOD(_CLASS,_BASE)                                 \
_CLASS::_CLASS( const _CLASS &right, CopyMode mode )                     \
DNB_THROW_SPEC((DNBEResourceLimit))                                      \
:_BASE(right, mode)                                                      \
{}                                                                       \
void _CLASS::orphanMembers()                                             \
DNB_THROW_SPEC_NULL                                                      \
{}
    
    
/**
 * The macro DNB_DECLARE_SIMLET must be included in the
 * header file of everyclass derived from DNBSimlet, and 
 * doesn't have a default constructor
 * DNB_DECLARE_SIMLET( classname )
 */
#define DNB_DECLARE_SIMLET(_CLASS)                                      \
    _DNB_DECLARE_METHOD(_CLASS)                                         \
protected:                                                              \
    _CLASS()                                                            \
       DNB_THROW_SPEC_NULL                                              \
       {}                                                               \
    DNB_DECLARE_DYNAMIC_CREATE(_CLASS) 

   
/**
 * The macro DNB_DECLARE_SIMLET must be included in the
 * header file of everyclass derived from DNBSimlet, and 
 * has a defulat constructor
 * DNB_DECLARE_SIMLET_NOCTR( classname )
 */
#define DNB_DECLARE_SIMLET_NOCTR(_CLASS)                                \
    _DNB_DECLARE_METHOD(_CLASS)                                         \
    DNB_DECLARE_DYNAMIC_CREATE(_CLASS)
    
/**
 * The macro DNB_DEFINE_SIMLET must be included in the
 * .cpp file of everyclass derived from DNBBasicSimEventClass
 * DNB_DEFINE_SIMLET( classname, baseclassname )
 */
#define DNB_DEFINE_SIMLET(_CLASS,_BASE)                                \
    DNB_DEFINE_DYNAMIC_CREATE_B1(_CLASS,_BASE);                        \
    _DNB_DEFINE_METHOD(_CLASS,_BASE)

/*
#if !defined(__xlC__)
class MyRWRecursiveLock : public RWRecursiveLock<RWMutexLock>
{
public:
    MyRWRecursiveLock() {};
    MyRWRecursiveLock(RWStaticCtor) {};
#if (defined(__HP_aCC))|| ( defined (_MSC_VER) || (_MSC_VER >= 1300))
    MyRWRecursiveLock(RWNoInitialization) {};
#endif
};
#endif */  // smw commented out above; 

/**
 * The DNBSimlet class serves as the base class for the simulation control
 * model.  It is still considered to be abstract since it adds only the message
 * management implementation and does nothing else.
 */
class ExportedByDNBSimulationControl DNBSimlet : 
public DNBSysMonitor, public DNBDynamicObject
/*
#if defined(__xlC__)
public RWMonitor<RWRecursiveLock<RWMutexLock> >,public DNBDynamicObject
#else
public RWMonitor<MyRWRecursiveLock>, public DNBDynamicObject
#endif
*/

{

    DNB_DECLARE_SIMLET_NOCTR(DNBSimlet);
public:
    
    /**
     * I always do this.
     */
    typedef DNBSimlet                                       Self;
    
    /**
     * Forward declare the back-pointer to a controller type.
     */
    typedef DNBSimletController                             Controller;
    typedef Controller::UpdateEvent                         UpdateEvent;
    typedef Controller::UpdateEventData                     UpdateEventData;
    typedef Controller::RunStateEvent                       RunStateEvent;
    typedef Controller::RunStateEventData                   RunStateEventData;
    
    /**
     * Destructor
     */
    virtual
    ~DNBSimlet()
        DNB_THROW_SPEC_ANY;
    
    /**
     * Get the simlet controller which is responsible for managing the run.
     */
    inline
    Controller*
    getController() const
        DNB_THROW_SPEC_ANY;
    
    /**
     * Set the simlet controller which is responsible for managing the run.
     */
    inline
    void
    setController( Controller* controller )
        DNB_THROW_SPEC_ANY;
    
    /**
     * MMH - add name check - for steplet needs
     */
    scl_wstring
    getName() const
        DNB_THROW_SPEC_NULL;
    
    void
    setName( scl_wstring name )
        DNB_THROW_SPEC_NULL;

    // END MMH


    
    /**
     * The handler which is called in response to an init message.
     */
    virtual
    void
    init( const RunStateEventData& data )
        DNB_THROW_SPEC_ANY;
    
    /**
     * The handler which is called in response to an update message.
     */
    virtual
    void
    update( const UpdateEventData& data )
        DNB_THROW_SPEC_ANY;
    
    /**
     * The handler which is called in response to a suspend message.
     */
    virtual
    void
    suspend( const RunStateEventData& data )
        DNB_THROW_SPEC_ANY;
    
    /**
     * The handler which is called in response to an update message.
     */
    virtual
    void
    resume( const RunStateEventData& data )
        DNB_THROW_SPEC_ANY;
    
    /**
     * The handler which is called in response to a done message.
     */
    virtual
    void
    fini( const RunStateEventData& data )
        DNB_THROW_SPEC_ANY;
    
    /**
     * The handler which is called in response to an idle message.
     */
    virtual
    void
    reset( const RunStateEventData& data )
        DNB_THROW_SPEC_ANY;
    
    /**
     * The handler which is called in response to a join message.
     */
    virtual
    void
    destroy( const RunStateEventData& data )
        DNB_THROW_SPEC_ANY;

    /*
     * Accessors for updatehandler and runstatehandler
     */
    inline
    Controller::RunStateHandler
    getRunStateHandler()
    DNB_THROW_SPEC_ANY;

    inline
    Controller::UpdateHandler
    getUpdateHandler()
    DNB_THROW_SPEC_ANY;

  
    /*
     * Adds the runstate handler to the controller
     */
    inline
    void
    addRunStateHandler(const Controller::RunStateHandler &handler)
    DNB_THROW_SPEC_ANY;

    /*
     * Removes the runstate handler to the controller
     */
    inline
    void
    removeRunStateHandler(const Controller::RunStateHandler &handler)
    DNB_THROW_SPEC_ANY;


    /*
     * Adds the updatehandler to the controller
     */
    inline
    void
    addUpdateHandler(const Controller::UpdateHandler &handler)
    DNB_THROW_SPEC_ANY;

    /*
     * Removes the updatehandler to the controller
     */
    inline
    void
    removeUpdateHandler(const Controller::UpdateHandler &handler)
    DNB_THROW_SPEC_ANY;

protected:
    
    /**
     * The main constructor - requires that the simlet be created within a
     * specific control context.
     */
    DNBSimlet( Controller* controller = 0 )
        DNB_THROW_SPEC_ANY;
    

    
    /**
     * Set the simlet controller which is responsible for accepting handlers
     * for dispatch.
     */
    inline
    Controller*
    getControllerTr() const
        DNB_THROW_SPEC_ANY;
    
    /**
     * Set the simlet controller which is responsible for accepting handlers
     * for dispatch.  This function is too complex to be inlined.
     */
    void
    setControllerTr( Controller* controller, bool addHandlers = true )
        DNB_THROW_SPEC_ANY;
    
    /**
     * MMH - 03/04/01 - add name for steplet needs
     */
    scl_wstring       _name;
    // END MMH

    /**
     * The handler which is called in response to a run-state event;
     */
    void
    runStateHandler( const RunStateEvent& event )
        DNB_THROW_SPEC_ANY;


    /**
     * The handler which is called in response to an update event;
     */
    void
    updateHandler( const UpdateEvent& event )
        DNB_THROW_SPEC_ANY;
    

private:
    

    
    /**
     * Instance data.
     */
    Controller*                 controller_;
    Controller::UpdateHandler   updateHandler_;
    Controller::RunStateHandler runStateHandler_;
    bool                        suspended_;
    
    /**
     * Undefined - for now.
     */
    DNBSimlet( const Self& rhs );
    void operator=( const Self& rhs );
};

//
// Snarf the public inline definition file
//
#include <DNBSimlet.cc>


#endif //_INCLUDE_DNBSIMLET
