/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBBasicSimEvent.h
//      The Basic Simulation Event class. All application-specific simulation events
//      will be derived from this class.  
//      Serves as the basic event in the Event-Notifier scheme. 
//      Subscribers subscribe to type of events.
//      Publishers publish instance of events. 
//      Event-Notifier notifies the subscribers of the events as and when 
//      they are  published.
//
//==============================================================================
//
// Usage notes: 
//      All Subscribers subscribe to type of events.
//      Publishers publish instance of events. 
//      Event-Notifier notifies the subscribers of the events as and when 
//      they are  published.
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     rtl          01/20/2001   Initial Development
//
//==============================================================================
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _DNB_DNBBasicSimEvent_H_
#define _DNB_DNBBasicSimEvent_H_

#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>

#include <DNBDynamicObject.h>
#include <DNBSimulationControl.h>
// #include <DNBClassInfo.h>            // smw; caa
#include <DNBSimEventService.h>


/**
 * MACROS
 */
#define _DNB_DEFINE_SIMEVENT(_CLASS)                                     \
public:                                                                  \
    static bool                                                          \
    subscribe(DNBSimEventService::EventHandler handler)                  \
    {                                                                    \
        DNBSimEventService* Notifier= DNBSimEventService::Instance();    \
        return Notifier->subscribe(_CLASS::ClassInfo(),handler);         \
    }                                                                    \
    static bool                                                          \
    unsubscribe(DNBSimEventService::EventHandler handler)                \
    {                                                                    \
        DNBSimEventService* Notifier= DNBSimEventService::Instance();    \
        return Notifier->unsubscribe(_CLASS::ClassInfo(),handler);       \
    }

    
/**
 * The macro DNB_DECLARE_SIMEVENT must be included in the
 * header file of everyclass derived from DNBBasicSimEventClass
 * DNB_DECLARE_SIMEVENT( classname )
 */
#define DNB_DECLARE_SIMEVENT(_CLASS)                                     \
    _DNB_DEFINE_SIMEVENT(_CLASS)                                         \
    DNB_DECLARE_DYNAMIC_CREATE(_CLASS)

   
    
/**
 * The macro DNB_DEFINE_SIMEVENT must be included in the
 * .cpp file of everyclass derived from DNBBasicSimEventClass
 * DNB_DEFINE_SIMEVENT( classname, baseclassname )
 */
#define DNB_DEFINE_SIMEVENT(_CLASS,_BASE)                                \
    DNB_DEFINE_DYNAMIC_CREATE_B1(_CLASS,_BASE);


 
/**
 *Base Event Class. The application-generated classes will derive from
 *this class
 */
class ExportedByDNBSimulationControl DNBBasicSimEvent : public DNBDynamicObject
{
    
    DNB_DECLARE_SIMEVENT(DNBBasicSimEvent);
    
public:
    
    typedef DNBBasicSimEvent     Self;
    
    
    virtual void publish()
        DNB_THROW_SPEC_NULL;
    /**
     *Copy constructor. 
     *All derived classes will have to provide a copy constructor.
     */
    DNBBasicSimEvent( const DNBBasicSimEvent &right, CopyMode mode )
        DNB_THROW_SPEC((DNBEResourceLimit));
    
    ~DNBBasicSimEvent()
        DNB_THROW_SPEC_NULL;
    
    
    DNBBasicSimEvent()
        DNB_THROW_SPEC_NULL;
    
};



#endif //_DNB_DNBBasicSimEvent_H_
