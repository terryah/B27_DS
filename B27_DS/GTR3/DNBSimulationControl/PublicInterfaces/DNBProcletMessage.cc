/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview HRN trangara 01:08:20
 */
// --------------------------------------------------------


//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBProcletMessage.cc
//      DNBProcletMessage inline functions implementation file
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     xxx          mm/dd/yyyy   Comment
//
//==============================================================================


//------------------------------------------------------------------------------
// Implements DNBProcletMessage::set
//------------------------------------------------------------------------------
inline
void
DNBProcletMessage::set(Proclet sender,MessageType message)
DNB_THROW_SPEC_ANY
{
    //Set the Sender of the message and the message itself.
    sender_=sender;
    message_=message;
}



//------------------------------------------------------------------------------
// Implements DNBProcletMessage::getSender
//------------------------------------------------------------------------------
inline
DNBProcletMessage::Proclet
DNBProcletMessage::getSender()
DNB_THROW_SPEC_ANY
{
    //Retrieve the Sender of the message
    return sender_;
}



//------------------------------------------------------------------------------
// Implements DNBProcletMessage::getMessage
//------------------------------------------------------------------------------
inline
DNBProcletMessage::MessageType 
DNBProcletMessage::getMessage()
DNB_THROW_SPEC_ANY
{
    //Retrieve the message
    return message_;
}



//------------------------------------------------------------------------------
// Implements DNBProcletMessage::operator==
//------------------------------------------------------------------------------
inline
bool
DNBProcletMessage::operator==( const Self &right ) const
DNB_THROW_SPEC_ANY
{
    return true;
}



//------------------------------------------------------------------------------
// Implements DNBProcletMessage::operator<
//------------------------------------------------------------------------------
inline
bool
DNBProcletMessage::operator<(const Self &right ) const
DNB_THROW_SPEC_ANY
{
    return true;
}



