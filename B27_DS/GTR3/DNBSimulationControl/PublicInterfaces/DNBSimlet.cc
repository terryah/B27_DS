/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview HRN trangara 01:08:20
 */
// --------------------------------------------------------


//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBSimlet.cc
//      DNBSimlet inline functions Implementation file
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     xxx          mm/dd/yyyy   Comment
//     rtl          12/16/2002   Provide methods to add/remove handlers 
//                               to/from the controller
//
//==============================================================================



//------------------------------------------------------------------------------
// Implements DNBSimlet::getController
//------------------------------------------------------------------------------
inline
DNBSimlet::Controller*
DNBSimlet::getController() const
    DNB_THROW_SPEC_ANY
{
    LockGuard lock( monitor() );
    return getControllerTr();
}





//------------------------------------------------------------------------------
// Implements DNBSimlet::setController
//------------------------------------------------------------------------------
inline
void
DNBSimlet::setController( Controller* controller )
    DNB_THROW_SPEC_ANY
{
    LockGuard lock( monitor() );
    setControllerTr( controller );
}





//------------------------------------------------------------------------------
// Implements DNBSimlet::getControllerTr
//------------------------------------------------------------------------------
inline
DNBSimlet::Controller*
DNBSimlet::getControllerTr() const
    DNB_THROW_SPEC_ANY
{
    return controller_;
}


//------------------------------------------------------------------------------
// Implements DNBSimlet::getRunStateHandler
//------------------------------------------------------------------------------
inline
DNBSimletController::RunStateHandler
DNBSimlet::getRunStateHandler()
DNB_THROW_SPEC_ANY
{
    return runStateHandler_;
}


//------------------------------------------------------------------------------
// Implements DNBSimlet::getUpdateHandler
//------------------------------------------------------------------------------
inline
DNBSimletController::UpdateHandler
DNBSimlet::getUpdateHandler()
DNB_THROW_SPEC_ANY
{
    return updateHandler_;
}



//------------------------------------------------------------------------------
// Implements DNBSimlet::addRunStateHandler
//------------------------------------------------------------------------------
inline
void
DNBSimlet::addRunStateHandler(const Controller::RunStateHandler &handler)
DNB_THROW_SPEC_ANY
{
    if(controller_)
    {
        controller_->addRunStateHandler(handler);
    }
}



//------------------------------------------------------------------------------
// Implements DNBSimlet::removeRunStateHandler
//------------------------------------------------------------------------------
inline
void
DNBSimlet::removeRunStateHandler(const Controller::RunStateHandler &handler)
DNB_THROW_SPEC_ANY
{
    if(controller_)
    {
        controller_->removeRunStateHandler(handler);
    }
}


//------------------------------------------------------------------------------
// Implements DNBSimlet::addUpdateHandler
//------------------------------------------------------------------------------
inline
void
DNBSimlet::addUpdateHandler(const Controller::UpdateHandler &handler)
DNB_THROW_SPEC_ANY
{
    if(controller_)
    {
        controller_->addUpdateHandler(handler);
    }
}



//------------------------------------------------------------------------------
// Implements DNBSimlet::removeUpdateHandler
//------------------------------------------------------------------------------
inline
void
DNBSimlet::removeUpdateHandler(const Controller::UpdateHandler &handler)
DNB_THROW_SPEC_ANY
{
    if(controller_)
    {
        controller_->removeUpdateHandler(handler);
    }
}

