/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBTimeletClock.h
//      The DNBTimeletClock class ties the TimeletMaster interface to a steplet
//      protocol so that a StepletMaster can drive the clock.  This class
//      represents the main simulation clock and is capable of being tied with
//      a hardware timer to achieve near-realtime scheduling.
//
//==============================================================================
//
// Usage notes: 
//      Brief usage explanation...
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     chris        01/01/1999   Initial Development
//     rtl                       Code change for proposing same time tick
//     rtl          04/23/2002   Included trace macros to make class traceable
//     rtl          04/07/2003   Change for allowing user to specify type of
//                               exception to be thrown from runUntil method
//     rtl          11/06/2003   Code change for using the new Trace facility
//     rtl          03/10/2004   Replace tvdlist with std list (scl_list)
//     rtl          04/23/2004   Added ZeroTime 
//     mmh          09/08/2006   Add reproposeTime to repropose same time
//                               (support Review activities)
//
//==============================================================================
#if DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _INCLUDE_DNBTIMELETCLOCK
#define _INCLUDE_DNBTIMELETCLOCK

/**
 * External includes.
 */
#include <DNBSystemBase.h>
#include <scl_list.h>
#include <DNBSystemDefs.h>

/**
 * Deneb includes.
 */
#include <DNBSimulationControl.h>
#include <DNBSimletController.h>
#include <DNBSimTime.h>
#include <DNBTimeletTimer.h>
// #include <DNBAtTimer.h>          // smw; caa 

/**
 * The DNBTimeletClock class ties the TimeletMaster interface to a steplet
 * protocol so that a StepletMaster can drive the clock.  This class represents
 * the main simulation clock and is capable of being tied with a hardware
 * timer to achieve near-realtime scheduling.
 */

class ExportedByDNBSimulationControl DNBTimeletClock : public DNBSimletController
{
public:
    
    /**
     * I always do this.
     */
    typedef DNBTimeletClock                                 Self;
    
    /**
     * Shorten the type name for time.
     */
    typedef DNBSimTime                                      Time;

    enum DNBExceptionType
    {
        Halt,
        Done
    };
    
    /**
     * Define some special time values.
     */
    static const Time MinTime;
    static const Time MaxTime;
    static const Time NullTime;
    static const Time ZeroTime;
    
    /**
     * Bring in the event, data, and handler types.
     */
    typedef DNBSimletController::UpdateEventData            UpdateEventData;
    typedef DNBSimletController::UpdateEvent                UpdateEvent;
    typedef DNBSimletController::UpdateHandler              UpdateHandler;
    typedef DNBSimletController::RunStateEventData          RunStateEventData;
    typedef DNBSimletController::RunStateEvent              RunStateEvent;
    typedef DNBSimletController::RunStateHandler            RunStateHandler;
    
    /**
     * Define the time event that will be used to notify of time changes.
     */
    struct TimeEventData
    {
        Self*            sender;
        Time             time;
        Time             minTime;
        Time             maxTime;
        UpdateEventData  update;
    };
    
    typedef DNBReactiveDataEvent<TimeEventData>             TimeEvent;
    typedef DNBReactiveHandler<TimeEvent>                   TimeHandler;
    typedef TimeHandler::Filter                             TimeFilter;
    
    /**
     * Define different phases at which to dispatch callbacks relative to the
     * simulation update.  I'd like to deep six this in favor of update event
     * prioritization, but this would take a far more sophisticated kernel than
     * I have now implemented so for now it stays.
     */
    enum TimePhase
    {
        PreTime,
        PostTime
    };
    
    /**
     * Define the callback type with which to do some pre or post-processing
     * of an update.
     */

    typedef DNBFunctor2<const TimeEventData&,const TimePhase&>   TimeCallback;

    typedef TimeCallback *                                       TimeCallbackF;
    
    struct CycleCallbackData
    {
        Self*           sender;
        Time            time;
        Time            minTime;
        Time            maxTime;
        DNBSimletController::CycleCallbackData cyc;
    };
    
    typedef DNBFunctor1<const CycleCallbackData&>         CycleCallback;

    typedef CycleCallback *                               CycleCallbackClock;
    
    /**
     * Shorten the type name for the timer type as well.
     */
    typedef DNBTimeletTimer                                 Timer;
    
    /**
     * Constructor.
     */
    DNBTimeletClock( const Time& minRes = MinTime,const Time& maxRes = MaxTime )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Destructor.
     */
    virtual
    ~DNBTimeletClock()
        DNB_THROW_SPEC_ANY;
    
    /**
     * Add a timelet.
     */
    inline
    void
    addTimeHandler( const TimeHandler& handler )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Remove a timelet.
     */
    inline
    void
    removeTimeHandler( const TimeHandler& handler )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Add a phase callback.
     */
    inline
    void
    addTimeCallback( const TimePhase& phase,const TimeCallback& callback )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Remove a phase callback.
     */
    inline
    void
    removeTimeCallback( const TimePhase& phase,const TimeCallback& callback )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Add a cycle callback.
     */
    inline
    void
    addCycleCallback( const CycleCallback& callback )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Remove a cycle callback.
     */
    inline
    void
    removeCycleCallback( const CycleCallback& callback )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Add a timelet.
     */
    inline
    void
    addTimer( const Timer& timer )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Remove a timelet.
     */
    inline
    void
    removeTimer( const Timer& timer )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Get the current time.
     */
    inline
    Time
    getTime() const
        DNB_THROW_SPEC_ANY;
    
    /**
     * Tell the clock to run until a specific time.
     */
    void
        runUntil( const Time& time, 
		          bool resetTime = false,
                  DNBExceptionType type = Halt )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Sets a flag, to indicate if the same time should be proposed again.
     */
    void setReproposeTimeFlag( bool bFlag )
        DNB_THROW_SPEC_ANY;

protected:
    
    /**
     * Add a phase callback.
     */
    inline
    void
    addTimeCallbackTr( const TimePhase& phase,const TimeCallback& callback )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Remove a phase callback.
     */
    inline
    void
    removeTimeCallbackTr( const TimePhase& phase,const TimeCallback& callback )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Remove a phase callback.
     */
    void
    dispatchTimeCallbacksTr( const TimePhase& phase,const TimeEventData& data )
                 DNB_THROW_SPEC_ANY;
    
    /**
     * Add a timer which will help the clock arbitrate the evolution of time.
     */
    inline
    void
    addTimerTr( const Timer& timer )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Add a timer which will help the clock arbitrate the evolution of time.
     */
    inline
    void
    removeTimerTr( const Timer& timer )
        DNB_THROW_SPEC_ANY;
    
    //
    // Get the current time.
    //
    inline
    Time
    getTimeTr() const
        DNB_THROW_SPEC_ANY;
    
    //
    // Fill Cycle callback data.
    //
    void
    fillCycleCallbackData( CycleCallbackData & )
        DNB_THROW_SPEC_NULL;
    
    //
    // Dispatch Cycle callbacks.
    //
    virtual void
    dispatchCycleCallbacks()
        DNB_THROW_SPEC_ANY;
    
    bool        reproposeTime_;

private:
    
    /**
     * Define the container for timers.
     */
    typedef scl_list<Timer, DNB_ALLOCATOR(Timer) >                 TimerList;
    
    /**
     * Define the event handler database types.
     */
    typedef DNBReactiveVar<TimeEvent>                           TimeVar;
    
    /**
     * Shorten the phase callback database type.
     */
    typedef scl_list<TimeCallback,DNB_ALLOCATOR(TimeCallback) > TimeCallbackList;
    
    /**
     * Shorten the cycle callback database type.
     */
    typedef scl_list<CycleCallback,DNB_ALLOCATOR(CycleCallback) > CycleCallbackList;
    
    /**
     * Helper function which controls the run-until.
     */
    void
    doRunUntil( const Time& time )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Private handler to initialize the clock whenever the controller is
     * reset for whatever reason.
     */
    void
    resetHandler( const RunStateEvent& event )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Private handler to drive the updating of the clock from the base
     * controller's update cycle.
     */
    void
    updateHandler( const UpdateEvent& event )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Private attributes.
     */
    Time               currentTime_;
    Time               untilTime_;
    Timer              untilTimer_;
    Time               maxRes_;
    Time               minRes_;
    TimerList          timers_;
    TimeVar            timeHandlers_;
    RunStateHandler    resetHandler_;
    UpdateHandler      updateHandler_;
    TimeCallbackList   timeCallbacks_[2];
    CycleCallbackList  cycleCallbacks_;
    TimeEventData      timeData_;
    DNBExceptionType   exType_;
    
    
    /**
     * Undefined - for now anyway.
     */
    DNBTimeletClock( const DNBTimeletClock& rhs );
    void operator=( const DNBTimeletClock& copy );
};

/**
 * Snarf the public inline definition file.
 */
#include <DNBTimeletClock.cc>

#endif // _INCLUDE_DNBTIMELETCLOCK

