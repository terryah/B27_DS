//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
#ifndef _INCLUDE_DNBSIMTRACE
#define _INCLUDE_DNBSIMTRACE

#include <DNBSystemBase.h>
// #include <rw/thr/threadid.h>
#include <DNBSystemDefs.h>

#include <DNBSimulationControl.h>

class ExportedByDNBSimulationControl DNBSimTrace
{
public:

    enum
    {
	ReactiveMD = 1,
	SimletMD   = 2
    };

    DNBSimTrace( const char* msg, int lev ) 
	: msg_( msg ),
	  yesno_( false )
	{ 
	    if( lev >= lev_ ) 
	    {
            /*
		cout << rwThreadHash( rwThreadId() ) 
		     << ": in " << msg << endl;
             */
		yesno_ = true;
	    }
	}
    ~DNBSimTrace() 
	{
	    if( yesno_ ) 
        {}
            /*
		cout << rwThreadHash( rwThreadId() ) 
		     << ": out " << msg_ << endl;
             */
	}
    static void setLevel( int level ) { lev_ = level; }

private:
    const char *msg_;
    bool yesno_;
    static int lev_;
};

#endif
