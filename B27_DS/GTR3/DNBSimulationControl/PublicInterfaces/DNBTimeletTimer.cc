/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview HRN trangara 01:08:20
 * @quickReview RTL 03:03:06
 */
// --------------------------------------------------------


//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBTimeletTimer.cc
//      DNBTimeletTimer inline functions implementation file
//
//==============================================================================
// HISTORY:
//     Author      Date         Purpose
//     ------      ----         -------
//     xxx         mm/dd/yyyy   Comment
//     mmg         03/06/2003   moved enable() to cpp file because it is virtual
//
//==============================================================================


//------------------------------------------------------------------------------
// Implements DNBTimeletTimerImpl::disable
//------------------------------------------------------------------------------
inline
void
DNBTimeletTimerImpl::disable()
DNB_THROW_SPEC_ANY
{
    enabled_ = false;
}


//------------------------------------------------------------------------------
// Implements DNBTimeletTimerImpl::isEnabled
//------------------------------------------------------------------------------
inline
bool
DNBTimeletTimerImpl::isEnabled()
DNB_THROW_SPEC_ANY
{
    return enabled_;
}


//------------------------------------------------------------------------------
// Implements DNBTimeletTimer::operator=
//------------------------------------------------------------------------------
inline
DNBTimeletTimer::Self&
DNBTimeletTimer::operator=( const Self& rhs )
DNB_THROW_SPEC_ANY
{
    return (Self&) DNBSysHandleBase::operator=( rhs );
}



//------------------------------------------------------------------------------
// Implements DNBTimeletTimer::expired
//------------------------------------------------------------------------------
inline
bool
DNBTimeletTimer::expired( const Time& current ) const
DNB_THROW_SPEC_ANY
{
    return body().expired( current );
}


//------------------------------------------------------------------------------
// Implements DNBTimeletTimer::propose
//------------------------------------------------------------------------------
inline
DNBTimeletTimer::Time
DNBTimeletTimer::propose( const Time& minTime, const Time& maxTime )
DNB_THROW_SPEC_ANY
{
    return body().propose( minTime, maxTime );
}


//------------------------------------------------------------------------------
// Implements DNBTimeletTimer::contains
//------------------------------------------------------------------------------
inline
bool
DNBTimeletTimer::contains( const Time& minTime, const Time& maxTime ) const
DNB_THROW_SPEC_ANY
{
    return body().contains( minTime, maxTime );
}


//------------------------------------------------------------------------------
// Implements DNBTimeletTimer::enable
//------------------------------------------------------------------------------
inline
void
DNBTimeletTimer::enable()
DNB_THROW_SPEC_ANY
{
    body().enable();
}


//------------------------------------------------------------------------------
// Implements DNBTimeletTimer::disable
//------------------------------------------------------------------------------
inline
void
DNBTimeletTimer::disable()
DNB_THROW_SPEC_ANY
{
    body().disable();
}


//------------------------------------------------------------------------------
// Implements DNBTimeletTimer::isEnabled
//------------------------------------------------------------------------------
inline
bool
DNBTimeletTimer::isEnabled()
DNB_THROW_SPEC_ANY
{
    return body().isEnabled();
}


//------------------------------------------------------------------------------
// Implements DNBTimeletTimer::body
//------------------------------------------------------------------------------
inline
DNBTimeletTimer::Impl&
DNBTimeletTimer::body() const
DNB_THROW_SPEC_ANY
{
    return (Impl&) DNBSysHandleBase::body();
}
