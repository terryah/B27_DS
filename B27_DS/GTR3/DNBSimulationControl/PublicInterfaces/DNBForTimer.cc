/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview HRN trangara 01:08:20
 */
// --------------------------------------------------------


//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBForTimer.cc
//      DNBForTimer inline functions implementation file
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     xxx          mm/dd/yyyy   Comment
//
//==============================================================================



//------------------------------------------------------------------------------
// Implements DNBForTimerImpl::getTimeVector
//------------------------------------------------------------------------------
inline
DNBForTimerImpl::TimeVector
DNBForTimerImpl::getTimeVector() const
    DNB_THROW_SPEC_ANY
{
    return sample_;
}


//------------------------------------------------------------------------------
// Implements DNBForTimer::operator=
//------------------------------------------------------------------------------
inline
DNBForTimer::Self&
DNBForTimer::operator=( const Self& rhs )
    DNB_THROW_SPEC_ANY
{
    return (Self&) DNBSysHandleBase::operator=( rhs );
}


//------------------------------------------------------------------------------
// Implements DNBForTimer::getTimeVector
//------------------------------------------------------------------------------
inline
DNBForTimer::TimeVector
DNBForTimer::getTimeVector() const
    DNB_THROW_SPEC_ANY
{
    return body().getTimeVector();
}


//------------------------------------------------------------------------------
// Implements DNBForTimer::body
//------------------------------------------------------------------------------
inline
DNBForTimer::Impl&
DNBForTimer::body() const
    DNB_THROW_SPEC_ANY
{
    return (Impl&) DNBSysHandleBase::body();
}

