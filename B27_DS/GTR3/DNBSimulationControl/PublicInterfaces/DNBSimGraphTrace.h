//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//*
//* FILE:
//*     DNBSimGraphTrace.h
//*
//* MODULE:
//*     DNBSimulationControl.m
//*
//* OVERVIEW:
//*     This file creates a TraceLog client for use in DELMIA toolkit.
//*     By default all trace messages are directed to the standard error
//*     stream. The trace messages can be redirected to another stream
//*     by modifying the TraceLog client
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 2001 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*
#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>
#include <DNBSimulationControl.h>
#include <DNBTraceLog.h>

ExportedByDNBSimulationControl extern DNBTraceLog DNBSimGraphTrace;
