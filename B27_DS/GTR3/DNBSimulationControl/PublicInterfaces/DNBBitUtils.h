//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
#if DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _INCLUDE_DNBBITUTILS
#define _INCLUDE_DNBBITUTILS

#define DNB_BIT(type,bitnum)             (((type)1) << bitnum )
#define DNB_BITNONE(type)                ((type)0)
#define DNB_BITALL(type)                 ~DNB_BITNONE(type)
#define DNB_BITANYSET(type,test,mask)    ((bit & mask) != 0)
#define DNB_BITALLSET(type,test,mask)    ((test & mask) == mask)

#endif // _INCLUDE_DNBBITUTILS

