/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBAtTimer.h
//      At Timer implementation class -   Used for participatating
//      in clock advancement and time-based functor dispatching.
//  
//      DNBAtTimer proposes a time based on the time passed on to it.
//
//==============================================================================
//
// Usage notes: 
//      Usually tied to the Time based Simlets(proclets). The time-based Simlets
//      register their timers with the Clock.  The timers participate in Clock
//      advancement and time-based functor dispatching. 
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     chris        01/01/1999   Initial Development
//
//==============================================================================
#if DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _INCLUDE_DNBATTIMER
#define _INCLUDE_DNBATTIMER

/**
 * Standard includes
 */
#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>

/**
 * Deneb includes.
 */
#include <DNBSimulationControl.h>
#include <DNBTimeletTimer.h>

class ExportedByDNBSimulationControl DNBAtTimerImpl : public DNBTimeletTimerImpl
{
public:
    
    /**
     * I always do this.
     */
    typedef DNBAtTimerImpl                             Self;
    
    /**
     * At time constructor.
     */
    DNBAtTimerImpl( const Time& atTime )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Copy constructor.
     */
    DNBAtTimerImpl( const Self& copy )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Destructor.
     */
    virtual
    ~DNBAtTimerImpl()
        DNB_THROW_SPEC_NULL;
    
    /**
     * Expired if the supplied time is greater than the 'at' time.
     */
    bool
    expired( const Time& current )
        DNB_THROW_SPEC_ANY;
    
    /**
     * If the timer has not expired, this simply proposes the single at time.
     */
    Time
    propose( const Time& minTime, const Time& maxTime )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Included only if the supplied time is the same as the 'at' time.
     */
    bool
    contains( const Time& minTime, const Time& maxTime )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Return the 'at' time.
     */
    inline
    Time
    getAtTime() const
        DNB_THROW_SPEC_ANY;
    
    /**
     * Set the current 'at' time.
     */
    inline
    void
    setAtTime( const Time& time )
        DNB_THROW_SPEC_ANY;
    
private:
    
    /**
     * Store the 'at' time.
     */
    Time          at_;
};

/**
 * The 'at' timer interface.  For now, changing the 'at' time is disallowed
 * because I need COW semantics and I don't have time to properly implement
 * them.  TBD.
 */
class ExportedByDNBSimulationControl DNBAtTimer : public DNBTimeletTimer
{
public:
    
    /**
     * I always do this.
     */
    typedef DNBAtTimer               Self;
    
    /**
     * I always do this.
     */
    typedef DNBAtTimerImpl           Impl;
    
    /**
     * Default constructor creates an at time of 0.0.
     */
    DNBAtTimer()
        DNB_THROW_SPEC_ANY;
    
    /**
     * Time constructor creates with the supplied at time.
     */
    DNBAtTimer( const Time& atTime )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Copy constructor performs shallow copy.
     */
    DNBAtTimer( const Self& copy )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Destructor.
     */
    virtual
    ~DNBAtTimer()
        DNB_THROW_SPEC_ANY;
    
    /**
     * Assignment operator.
     */
    inline
    Self&
    operator=( const Self& rhs )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Get the current 'at' time.
     */
    inline
    Time
    getAtTime() const
        DNB_THROW_SPEC_ANY;
    
    /**
     * Set the current 'at' time.
     */
    inline
    void
    setAtTime( const Time& time )
    DNB_THROW_SPEC_ANY;
    
private:
    
    /**
     * Convenience to get at the implementation.
     */
    inline
    Impl&
    body() const
    DNB_THROW_SPEC_ANY;
};

#include <DNBAtTimer.cc>

#endif // _INCLUDE_DNBATTIMER
