/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview HRN trangara 01:08:20
 * @quickReview RTL 03:03:06
 */
// --------------------------------------------------------


//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBIntervalTimer.cc
//      DNBIntervalTimer inline functions implementation file
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     xxx          mm/dd/yyyy   Comment
//     mmg          03/06/2003   added method to set minRes
//
//==============================================================================


//------------------------------------------------------------------------------
// Implements DNBIntervalTimer::operator=
//------------------------------------------------------------------------------
inline
DNBIntervalTimer::Self&
DNBIntervalTimer::operator=( const Self& rhs )
DNB_THROW_SPEC_ANY
{
    return (Self&) DNBSysHandleBase::operator=( rhs );
}



//------------------------------------------------------------------------------
// Implements DNBIntervalTimer::body
//------------------------------------------------------------------------------
inline
DNBIntervalTimer::Impl&
DNBIntervalTimer::body() const
DNB_THROW_SPEC_ANY
{
    return (Impl&) DNBSysHandleBase::body();
}


//------------------------------------------------------------------------------
// Implements DNBIntervalTimerImpl::getInterval
//------------------------------------------------------------------------------
inline
DNBSimTime
DNBIntervalTimerImpl::getInterval() const
DNB_THROW_SPEC_ANY
{
    return intervalTime_;
}


//------------------------------------------------------------------------------
// Implements DNBIntervalTimerImpl::setInterval
//------------------------------------------------------------------------------
inline
void
DNBIntervalTimerImpl::setInterval( const Time& intervalTime )
DNB_THROW_SPEC_ANY
{
    intervalTime_ = intervalTime;
}


//------------------------------------------------------------------------------
// Implements DNBIntervalTimerImpl::getBeginTime
//------------------------------------------------------------------------------
inline
DNBSimTime
DNBIntervalTimerImpl::getBeginTime() const
DNB_THROW_SPEC_ANY
{
    return beginTime_;
}


//------------------------------------------------------------------------------
// Implements DNBIntervalTimerImpl::setBeginTime
//------------------------------------------------------------------------------
inline
void
DNBIntervalTimerImpl::setBeginTime( const Time& beginTime )
DNB_THROW_SPEC_ANY
{
    beginTime_ = beginTime;
}


//------------------------------------------------------------------------------
// Implements DNBIntervalTimerImpl::getEndTime
//------------------------------------------------------------------------------
inline
DNBSimTime
DNBIntervalTimerImpl::getEndTime() const
DNB_THROW_SPEC_ANY
{
    return endTime_;
}


//------------------------------------------------------------------------------
// Implements DNBIntervalTimerImpl::setEndTime
//------------------------------------------------------------------------------
inline
void
DNBIntervalTimerImpl::setEndTime( const Time& endTime )
DNB_THROW_SPEC_ANY
{
    endTime_ = endTime;
}


//------------------------------------------------------------------------------
// Implements DNBIntervalTimerImpl::getShift
//------------------------------------------------------------------------------
inline
DNBSimTime
DNBIntervalTimerImpl::getShift() const
DNB_THROW_SPEC_ANY
{ 
    return shift_;
}



//------------------------------------------------------------------------------
// Implements DNBIntervalTimerImpl::setShift
//------------------------------------------------------------------------------
inline
void
DNBIntervalTimerImpl::setShift( const Time& shift )
DNB_THROW_SPEC_ANY
{
    if(shift < intervalTime_)
        shift_=shift;
}

//------------------------------------------------------------------------------
// Implements DNBIntervalTimerImpl::setMinRes
//------------------------------------------------------------------------------
inline
void
DNBIntervalTimerImpl::setMinRes( const Time& minRes )
DNB_THROW_SPEC_ANY
{
    minRes_ = minRes;
}

//------------------------------------------------------------------------------
// Implements DNBIntervalTimer::getInterval
//------------------------------------------------------------------------------
inline
DNBSimTime
DNBIntervalTimer::getInterval() const
DNB_THROW_SPEC_ANY
{
    return body().getInterval();
}


//------------------------------------------------------------------------------
// Implements DNBIntervalTimer::setInterval
//------------------------------------------------------------------------------
inline
void
DNBIntervalTimer::setInterval( const Time& intervalTime )
DNB_THROW_SPEC_ANY
{
    body().setInterval( intervalTime );
}


//------------------------------------------------------------------------------
// Implements DNBIntervalTimer::getBeginTime
//------------------------------------------------------------------------------
inline
DNBSimTime
DNBIntervalTimer::getBeginTime() const
DNB_THROW_SPEC_ANY
{
    return body().getBeginTime();
}


//------------------------------------------------------------------------------
// Implements DNBIntervalTimer::setBeginTime
//------------------------------------------------------------------------------
inline
void
DNBIntervalTimer::setBeginTime( const Time& beginTime )
DNB_THROW_SPEC_ANY
{
    body().setBeginTime( beginTime );
}


//------------------------------------------------------------------------------
// Implements DNBIntervalTimer::getEndTime
//------------------------------------------------------------------------------
inline
DNBSimTime
DNBIntervalTimer::getEndTime() const
DNB_THROW_SPEC_ANY
{
    return body().getEndTime();
}


//------------------------------------------------------------------------------
// Implements DNBIntervalTimer::setEndTime
//------------------------------------------------------------------------------
inline
void
DNBIntervalTimer::setEndTime( const Time& endTime )
DNB_THROW_SPEC_ANY
{
    body().setEndTime( endTime );
}



//------------------------------------------------------------------------------
// Implements DNBIntervalTimer::getShift
//------------------------------------------------------------------------------
inline
DNBSimTime
DNBIntervalTimer::getShift() const
DNB_THROW_SPEC_ANY
{ 
    return body().getShift();
}


//------------------------------------------------------------------------------
// Implements DNBIntervalTimer::setShift
//------------------------------------------------------------------------------
inline
void
DNBIntervalTimer::setShift( const Time& shift )
DNB_THROW_SPEC_ANY
{
    body().setShift( shift );
}


//------------------------------------------------------------------------------
// Implements DNBIntervalTimer::onInterval
//------------------------------------------------------------------------------
inline
bool
DNBIntervalTimer::onInterval( const Time& time )
DNB_THROW_SPEC_ANY
{
    return body().onInterval( time );
}


//------------------------------------------------------------------------------
// Implements DNBIntervalTimer::setMinRes
//------------------------------------------------------------------------------
inline
void
DNBIntervalTimer::setMinRes( const Time& minRes )
DNB_THROW_SPEC_ANY
{
    body().setMinRes( minRes );
}

