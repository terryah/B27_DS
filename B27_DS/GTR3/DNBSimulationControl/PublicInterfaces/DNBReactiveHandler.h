/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBReactiveHandler.h
//      Brief description...
//
//==============================================================================
//
// Usage notes: 
//      Brief usage explanation...
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     chris        01/01/1999   Initial Development
//
//==============================================================================
#define HAS_DNB_FILTER 1

#if DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _INCLUDE_DNBREACTIVEHANDLER
#define _INCLUDE_DNBREACTIVEHANDLER

/**
 * Standard includes.
 */
#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>

#include <DNBSimulationControl.h>
#include <DNBReactiveKernel.h>
#include <DNBSimletException.h>
#include <DNBSimTrace.h>
#include <DNBCallback.h>

template <class EvnT>
class DNBReactiveHandlerBody : public DNBReactiveKernelTaskBody
{
public:

    /**
     * I always do this to localize the impact of naming in case I need to
     * change something later on.  
     */
    typedef DNBReactiveHandlerBody<EvnT>                      Self;

    /**
     * Define the filter type.  I would like to change this to be a template
     * parameter so I can make it cheaper, but for now this should work.
     */
    typedef DNBFunctor1wRet<bool,const EvnT&>                 Filter;

    typedef Filter *                                          FilterF;

    /**
     * Pull in the definition of priority.
     */
    typedef DNBReactiveKernelTask::Priority                   Priority;

    /**
     * The DNBReactiveKernelTaskBody override which will be called by the
     * kernel to execute this handler.  The overridden implementation calls
     * invoke() with the cached values of event and then caches
     * any exceptions which are incurred in the event.
     */
    void
    execute()
        DNB_THROW_SPEC_ANY;

    /**
     * The intern member caches the sends and event information which is then
     * passed through the handler.
     */
    void
    intern( EvnT& event )
        DNB_THROW_SPEC_ANY;

    /**
     * The big-kahoona function which will first call the filter and it returns
     * "true" will then call the functor.
     */
    bool
    dispatch( EvnT& event )
        DNB_THROW_SPEC_ANY;

    /**
     * A pure function which allows us to invoke different types of handlers.
     */
    virtual
    void
    invoke( const EvnT& event )
        DNB_THROW_SPEC_ANY = 0;
    
    /**
     * A pure function to allow the derived classes to clone their objects
     */
    virtual
    Self*
    clone()
        DNB_THROW_SPEC_ANY = 0;
    
protected:

    /**
     * Constructor - protected because this is an interface.
     */
    DNBReactiveHandlerBody(const DNBFunctor1wRet<bool,const EvnT&>& filter,
    const Priority& priority )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Destructor - protected because this is an interface.
     */
    ~DNBReactiveHandlerBody()
        DNB_THROW_SPEC_NULL;

    /**
     * trusted private data accessors 
     */
    inline const DNBFunctor1wRet<bool,const EvnT&>& getFilter() const
        DNB_THROW_SPEC_ANY;

private:

    /**
     * Instance data.
     */
    EvnT        event_;
    bool        signalled_;
    Filter      filter_;

    /**
     * Unsupported for now.
     */
    DNBReactiveHandlerBody( const Self& copy );
    void operator=( const Self& rhs );
};

template <class EvnT>
class DNBReactiveHandler : public DNBReactiveKernelTask
{
public:

    /**
     * I always do this to localize the impact of naming in case I need to
     * change something later on.  
     */
    typedef DNBReactiveHandler<EvnT>                        Self;

    /**
     * Shorten the type name for the body type.
     */
    typedef DNBReactiveHandlerBody<EvnT>                    Body;

    /**
     * Shorten the type name for the filter type.
     */
    typedef typename Body::Filter                            Filter;

    /**
     * Save the event type just in case someone is interested.
     */
    typedef EvnT                                            EventT;

    /**
     * Pull in the definition of priority.
     */
    typedef typename Body::Priority                         Priority;

    /**
     * Constructor 
     */
    DNBReactiveHandler()
        DNB_THROW_SPEC_ANY;
    
    /**
     * Constructor
     */
    DNBReactiveHandler( Body* body )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Constructor
     */
    DNBReactiveHandler( const Self& copy )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Destructor 
     */
    virtual 
    ~DNBReactiveHandler()
        DNB_THROW_SPEC_ANY;

    /**
     * Assignment operator.
     */
    Self&
    operator=( const Self& rhs )
        DNB_THROW_SPEC_ANY;

    //
    // The big-kahoona function which will first call the filter and it returns
    // "true" will then call the functor.  The reason this returns a bool is
    // that because internally it calls the filter before execution.  If the
    // filter returns 'false', the handler does not dispatch.
    //
    bool
    dispatch( EvnT& event )
        DNB_THROW_SPEC_ANY;

protected:

    //
    // Convenience function to get at the body.
    //
    inline
    Body&
    body() const
        DNB_THROW_SPEC_ANY;
};

//
// A global template function which disables filtering altogether.
//
#ifdef _CAN_INSTANTIATE_ON_T
template <class EvnT>
DNBReactiveHandler<EvnT>::Filter
DNBFilterNone()
    DNB_THROW_SPEC_ANY
{
    return DNBReactiveHandler<EvnT>::Filter();
}
#endif

#define DNBFilterNone(evt) DNBReactiveHandler<evt>::Filter()

/**
 * --- Pointer-to-Member handlers that take no extra arguments ---
 */

    //typedef DNBFunctor1wRet<bool,const EvnT&>                     Filter;

template <class EvnT, class RcvT>
class DNBReactiveHandlerBodyM0 : public DNBReactiveHandlerBody<EvnT>
{
public:
    typedef void (RcvT::* Functor)( const EvnT& ) DNB_THROW_SPEC_ANY;
    typedef DNBReactiveHandlerBodyM0<EvnT, RcvT> Self;

    DNBReactiveHandlerBodyM0( const  DNBFunctor1wRet< bool,const EvnT& >& filter,
    RcvT* instance,
    const Functor& func,
    const DNBReactiveKernelTask::Priority& priority )
        DNB_THROW_SPEC_ANY;

    void invoke( const EvnT& event )
        DNB_THROW_SPEC_ANY;
    
    DNBReactiveHandlerBody<EvnT>* clone()
        DNB_THROW_SPEC_ANY;

private:
    RcvT*      instance_;
    Functor    functor_;
};

template <class EvnT, class RcvT>
DNBReactiveHandlerBodyM0<EvnT,RcvT>::
DNBReactiveHandlerBodyM0( const DNBFunctor1wRet<bool,const EvnT&>& filter,
RcvT* instance,
const Functor& functor,
const DNBReactiveKernelTask::Priority& priority )
    DNB_THROW_SPEC_ANY
    : DNBReactiveHandlerBody<EvnT>( filter, priority ),
      instance_( instance ),
      functor_( functor )
{
}

template <class EvnT, class RcvT>
void
DNBReactiveHandlerBodyM0<EvnT,RcvT>::invoke( const EvnT& event )
    DNB_THROW_SPEC_ANY
{
    (instance_->*functor_)( event );
}

template <class EvnT, class RcvT>
DNBReactiveHandlerBody<EvnT>*
DNBReactiveHandlerBodyM0<EvnT,RcvT>::clone()
    DNB_THROW_SPEC_ANY
{
    return  DNB_NEW DNBReactiveHandlerBodyM0<EvnT,RcvT>( DNBReactiveHandlerBody<EvnT>::getFilter(), instance_, functor_, DNBReactiveKernelTaskBody::getPriority() );
}

template <class EvnT, class RcvT>
inline
DNBReactiveHandlerBody<EvnT>*
DNBMakeReactiveHandler( 
                       const typename DNBReactiveHandlerBody<EvnT>::Filter& filter,
                       RcvT* instance,
                       const typename DNBReactiveHandlerBodyM0<EvnT,RcvT>::Functor& functor,
                       const typename DNBReactiveHandler<EvnT>::Priority& priority )
    DNB_THROW_SPEC_ANY
{
    DNB_NEW DNBReactiveHandlerBodyM0<EvnT,RcvT>( 
        filter, instance, functor, priority );
}

#define DNBMakeReactiveHandlerM0(eve,flt,rcv,ins,ftr,pri)                 \
    DNB_NEW DNBReactiveHandlerBodyM0<eve,rcv>(                            \
flt, ins, ftr, pri )

/**
 * --- Pointer-to-Member handlers that take one extra argument ---
 */

template <class EvnT, class RcvT, class T1>
class DNBReactiveHandlerBodyM1 : public DNBReactiveHandlerBody<EvnT>
{
public:
    typedef void (RcvT::* Functor)( const EvnT&, T1 )
        DNB_THROW_SPEC_ANY;
    DNBReactiveHandlerBodyM1( const typename DNBReactiveHandlerBody<EvnT>::Filter& filter,
        RcvT* instance,
        const Functor& func,
        T1 a1,
        const DNBReactiveKernelTask::Priority& priority )
        DNB_THROW_SPEC_ANY;
    void invoke( const EvnT& event )
        DNB_THROW_SPEC_ANY;
    DNBReactiveHandlerBody<EvnT>* clone()
        DNB_THROW_SPEC_ANY;
private:
    RcvT*       instance_;
    Functor     functor_;
    T1          a1_;
};

template <class EvnT, class RcvT, class T1>
DNBReactiveHandlerBodyM1<EvnT,RcvT,T1>::
DNBReactiveHandlerBodyM1( const typename DNBReactiveHandlerBody<EvnT>::Filter& filter,
                         RcvT* instance,
                         const Functor& functor,
                         T1 a1,
                         const DNBReactiveKernelTask::Priority& priority )
                         DNB_THROW_SPEC_ANY
                         : DNBReactiveHandlerBody<EvnT>( filter, priority ),
                         instance_( instance ),
                         functor_( functor ),
                         a1_( a1 )
{
}

template <class EvnT, class RcvT, class T1>
void
DNBReactiveHandlerBodyM1<EvnT,RcvT,T1>::invoke( const EvnT& event )
DNB_THROW_SPEC_ANY
{
    (instance_->*functor_)( event, a1_ );
}

template <class EvnT, class RcvT, class T1>
DNBReactiveHandlerBody<EvnT>*
DNBReactiveHandlerBodyM1<EvnT,RcvT, T1>::clone()
DNB_THROW_SPEC_ANY
{
    return  DNB_NEW DNBReactiveHandlerBodyM1<EvnT,RcvT, T1>( DNBReactiveHandlerBodyM1<EvnT,RcvT, T1>::getFilter(), instance_, functor_, a1_, DNBReactiveKernelTaskBody::getPriority() );
}

template <class EvnT, class RcvT, class T1>
inline
DNBReactiveHandlerBody<EvnT>*
DNBMakeReactiveHandler( 
                       const typename DNBReactiveHandlerBody<EvnT>::Filter& filter,
                       RcvT* instance,
                       const typename DNBReactiveHandlerBodyM1<EvnT,RcvT,T1>::Functor& functor,
                       T1 a1,
                       const typename DNBReactiveHandler<EvnT>::Priority& priority )
                       DNB_THROW_SPEC_ANY
{
    DNB_NEW DNBReactiveHandlerBodyM1<EvnT,RcvT,T1>( 
        filter, instance, functor, a1, priority );
}

#define DNBMakeReactiveHandlerM1(eve,flt,rcv,ins,ftr,T1,a1,pri)           \
    DNB_NEW DNBReactiveHandlerBodyM1<eve,rcv,T1>(                    \
flt, ins, ftr, a1, pri )

/**
 * --- Pointer-to-Member handlers that take two extra arguments ---
 */

template <class EvnT, class RcvT, class T1, class T2>
class DNBReactiveHandlerBodyM2 : public DNBReactiveHandlerBody<EvnT>
{
public:
    typedef void (RcvT::* Functor)( const EvnT&, T1, T2 )
        DNB_THROW_SPEC_ANY;
    DNBReactiveHandlerBodyM2( DNBFunctor1wRet<bool,const EvnT&>& filter,
        RcvT* instance,
        const Functor& func,
        T1 a1,
        T2 a2,
        const DNBReactiveKernelTask::Priority& priority )
        DNB_THROW_SPEC_ANY;
    void invoke( const EvnT& event )
        DNB_THROW_SPEC_ANY;
    DNBReactiveHandlerBody<EvnT>* clone()
        DNB_THROW_SPEC_ANY;
private:
    RcvT*      instance_;
    Functor     functor_;
    T1          a1_;
    T1          a2_;
};

template <class EvnT, class RcvT, class T1, class T2>
DNBReactiveHandlerBodyM2<EvnT,RcvT,T1,T2>::
DNBReactiveHandlerBodyM2( DNBFunctor1wRet<bool,const EvnT& >& filter,
                         RcvT* instance,
                         const Functor& functor,
                         T1 a1,
                         T2 a2,
                         const DNBReactiveKernelTask::Priority& priority )
                         DNB_THROW_SPEC_ANY
                         : DNBReactiveHandlerBody<EvnT>( filter, priority ),
                         instance_( instance ),
                         functor_( functor ),
                         a1_( a1 ),
                         a2_( a2 )
{
}

template <class EvnT, class RcvT, class T1, class T2>
void
DNBReactiveHandlerBodyM2<EvnT,RcvT,T1,T2>::invoke( const EvnT& event )
DNB_THROW_SPEC_ANY
{
    (instance_->*functor_)( event, a1_, a2_ );
}

template <class EvnT, class RcvT, class T1, class T2>
DNBReactiveHandlerBody<EvnT>*
DNBReactiveHandlerBodyM2<EvnT,RcvT, T1, T2>::clone()
DNB_THROW_SPEC_ANY
{
    return  DNB_NEW DNBReactiveHandlerBodyM2<EvnT,RcvT, T1, T2>( DNBReactiveHandlerBodyM2<EvnT,RcvT, T1, T2>::getFilter(), instance_, functor_, a1_, a2_, DNBReactiveKernelTaskBody::getPriority() ); 
}

template <class EvnT, class RcvT, class T1, class T2>
inline
DNBReactiveHandlerBody<EvnT>*
DNBMakeReactiveHandler( 
                       const typename DNBReactiveHandlerBody<EvnT>::Filter& filter,
                       RcvT* instance,
                       const typename DNBReactiveHandlerBodyM2<EvnT,RcvT,T1,T2>::Functor& functor,
                       T1 a1,
                       T2 a2,
                       const typename DNBReactiveHandler<EvnT>::Priority& priority )
                       DNB_THROW_SPEC_ANY
{
    DNB_NEW DNBReactiveHandlerBodyM2<EvnT,RcvT,T1,T2>( 
        filter, instance, functor, a1, a2, priority );
}

#define DNBMakeReactiveHandlerM2(eve,flt,rcv,ins,ftr,T1,a1,T2,a2,pri)     \
    DNB_NEW DNBReactiveHandlerBodyM2<eve,rcv,T1,T2>(                 \
flt, ins, ftr, a1, a2, pri )

/**
 * --- Global function handlers that take no extra arguments ---
 */

template <class EvnT>
class DNBReactiveHandlerBodyG0 : public DNBReactiveHandlerBody<EvnT>
{
public:
    typedef void (*Functor)( const EvnT& )
        DNB_THROW_SPEC_ANY;
    DNBReactiveHandlerBodyG0( DNBFunctor1wRet<bool,const EvnT&>& filter,
        const Functor& func,
        const DNBReactiveKernelTask::Priority& priority )
        DNB_THROW_SPEC_ANY;
    void invoke( const EvnT& event )
        DNB_THROW_SPEC_ANY;
    DNBReactiveHandlerBody<EvnT>* clone()
        DNB_THROW_SPEC_ANY;
    
private:
    Functor    functor_;
};

template <class EvnT>
DNBReactiveHandlerBodyG0<EvnT>::
DNBReactiveHandlerBodyG0( DNBFunctor1wRet<bool,const EvnT&>& filter,
                         const Functor& functor,
                         const DNBReactiveKernelTask::Priority& priority )
                         DNB_THROW_SPEC_ANY
                         : DNBReactiveHandlerBody<EvnT>( filter, priority ),
                         functor_( functor )
{
}

template <class EvnT>
void
DNBReactiveHandlerBodyG0<EvnT>::invoke( const EvnT& event )
DNB_THROW_SPEC_ANY
{
    (*functor_)( event );
}

template <class EvnT>
DNBReactiveHandlerBody<EvnT>*
DNBReactiveHandlerBodyG0<EvnT>::clone()
DNB_THROW_SPEC_ANY
{
    return  DNB_NEW DNBReactiveHandlerBodyG0<EvnT>( DNBReactiveHandlerBodyG0<EvnT>::getFilter(), functor_, DNBReactiveKernelTaskBody::getPriority() ); 
}

template <class EvnT>
inline
DNBReactiveHandlerBody<EvnT>*
DNBMakeReactiveHandler( 
                       const typename DNBReactiveHandlerBody<EvnT>::Filter& filter,
                       const typename DNBReactiveHandlerBodyG0<EvnT>::Functor& functor,
                       const typename DNBReactiveHandler<EvnT>::Priority& priority )
                       DNB_THROW_SPEC_ANY
{
    DNB_NEW DNBReactiveHandlerBodyG0<EvnT>( 
        filter, functor, priority );
}

#define DNBMakeReactiveHandlerG0(eve,flt,ftr)                              \
DNB_NEW DNBReactiveHandlerBodyG0<eve>( flt, ftr )

/**
 * --- Global function handlers that take one extra argument ---
 */

template <class EvnT, class T1>
class DNBReactiveHandlerBodyG1 : public DNBReactiveHandlerBody<EvnT>
{
public:
    typedef void (*Functor)( const EvnT&, T1 )
        DNB_THROW_SPEC_ANY;
    DNBReactiveHandlerBodyG1( DNBFunctor1wRet<bool,const EvnT&>& filter,
        const Functor& func,
        T1 a1,
        const DNBReactiveKernelTask::Priority& priority )
        DNB_THROW_SPEC_ANY;
    void invoke( const EvnT& event )
        DNB_THROW_SPEC_ANY;
    DNBReactiveHandlerBody<EvnT>* clone()
        DNB_THROW_SPEC_ANY;
private:
    Functor     functor_;
    T1          a1_;
};

template <class EvnT, class T1>
DNBReactiveHandlerBodyG1<EvnT,T1>::
DNBReactiveHandlerBodyG1( DNBFunctor1wRet<bool,const EvnT&>& filter,
                         const Functor& functor,
                         T1 a1,
                         const DNBReactiveKernelTask::Priority& priority )
                         DNB_THROW_SPEC_ANY
                         : DNBReactiveHandlerBody<EvnT>( filter, priority ),
                         functor_( functor ),
                         a1_( a1 )
{
}

template <class EvnT, class T1>
void
DNBReactiveHandlerBodyG1<EvnT,T1>::invoke( const EvnT& event )
DNB_THROW_SPEC_ANY
{
    (*functor_)( event, a1_ );
}

template <class EvnT, class T1>
DNBReactiveHandlerBody<EvnT>*
DNBReactiveHandlerBodyG1<EvnT, T1>::clone()
DNB_THROW_SPEC_ANY
{
    return  DNB_NEW DNBReactiveHandlerBodyG1<EvnT, T1>( DNBReactiveHandlerBodyG1<EvnT, T1>::getFilter(), functor_, a1_, DNBReactiveKernelTaskBody::getPriority() ); 
}

template <class EvnT, class T1>
inline
DNBReactiveHandlerBody<EvnT>*
DNBMakeReactiveHandler( 
                       const typename DNBReactiveHandlerBody<EvnT>::Filter& filter,
                       const typename DNBReactiveHandlerBodyG1<EvnT,T1>::
                       Functor& functor,
                       T1 a1,
                       const typename DNBReactiveHandler<EvnT>::Priority& priority )
                       DNB_THROW_SPEC_ANY
{
    DNB_NEW DNBReactiveHandlerBodyG1<EvnT,T1>( 
        filter, functor, a1, priority );
}

#define DNBMakeReactiveHandlerG1(eve,flt,ftr,T1,a1,pri)                   \
    DNB_NEW DNBReactiveHandlerBodyG1<eve,T1>(                        \
flt, ftr, a1, pri )

/**
 * --- Global function handlers that take two extra arguments ---
 */

template <class EvnT, class T1, class T2>
class DNBReactiveHandlerBodyG2 : public DNBReactiveHandlerBody<EvnT>
{
public:
    typedef void (*Functor)( const EvnT&, T1, T2 )
        DNB_THROW_SPEC_ANY;
    DNBReactiveHandlerBodyG2( DNBFunctor1wRet<bool,const EvnT&>& filter,
        const Functor& func,
        T1 a1,
        T2 a2,
        const DNBReactiveKernelTask::Priority& priority )
        DNB_THROW_SPEC_ANY;
    void invoke( const EvnT& event )
        DNB_THROW_SPEC_ANY;
    DNBReactiveHandlerBody<EvnT>* clone()
        DNB_THROW_SPEC_ANY;
    
private:
    Functor     functor_;
    T1          a1_;
    T1          a2_;
};

template <class EvnT, class T1, class T2>
DNBReactiveHandlerBodyG2<EvnT,T1,T2>::
DNBReactiveHandlerBodyG2( DNBFunctor1wRet<bool,const EvnT&>& filter,
                         const Functor& functor,
                         T1 a1,
                         T2 a2,
                         const DNBReactiveKernelTask::Priority& priority )
                         DNB_THROW_SPEC_ANY
                         : DNBReactiveHandlerBody<EvnT>( filter, priority ),
                         functor_( functor ),
                         a1_( a1 ),
                         a2_( a2 )
{
}

template <class EvnT, class T1, class T2>
void
DNBReactiveHandlerBodyG2<EvnT,T1,T2>::invoke( const EvnT& event )
DNB_THROW_SPEC_ANY
{
    (*functor_)( event, a1_, a2_ );
}

template <class EvnT, class T1, class T2>
DNBReactiveHandlerBody<EvnT>*
DNBReactiveHandlerBodyG2<EvnT, T1, T2>::clone()
DNB_THROW_SPEC_ANY
{
    return  DNB_NEW DNBReactiveHandlerBodyG2<EvnT, T1, T2>( DNBReactiveHandlerBodyG2<EvnT, T1, T2>::getFilter(), functor_, a1_, a2_, DNBReactiveKernelTaskBody::getPriority() ); 
}

template <class EvnT, class T1, class T2>
inline
DNBReactiveHandlerBody<EvnT>*
DNBMakeReactiveHandler( 
                       const typename DNBReactiveHandler<EvnT>::Filter& filter,
                       const typename DNBReactiveHandlerBodyG2<EvnT,T1,T2>::
                       Functor& functor,
                       T1 a1,
                       T2 a2,
                       const typename DNBReactiveHandler<EvnT>::Priority& priority )
    DNB_THROW_SPEC_ANY
{
    DNB_NEW DNBReactiveHandlerBodyG2<EvnT,T1,T2>( 
        filter, functor, a1, a2, priority );
}

#define DNBMakeReactiveHandlerG2(eve,flt,ftr,T1,a1,T2,a2,pri)             \
    DNB_NEW DNBReactiveHandlerBodyG2<eve,T1,T2>(                     \
flt, ftr, a1, a2, pri )

/**
 * Snarf the public inline definition file.
 */
#include <DNBReactiveHandler.cc>

/**
 * Convenience filters.
 */

/*
template <class EvnT>
typename 
DNBReactiveHandler<EvnT>::Filter
DNBFilterAlways( const EvnT& event )
{
    return rwMakeFunctor2<const EvnT&>;
}
*/


#endif // _INCLUDE_DNBREACTIVEHANDLER
