//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
#ifndef _INCLUDE_DNBSIMSTEP
#define _INCLUDE_DNBSIMSTEP

#ifdef UL64IsDefined
#include <limits.h>
typedef DNBUnsigned64 DNBSimStep;
#define DNB_STEP_MIN       0
#define DNB_STEP_MAX       0xffffffffffffffffui64 - 1
#else
typedef unsigned long DNBSimStep;
#define DNB_STEP_MIN       0
#define DNB_STEP_MAX       ~DNB_STEP_MIN - 1
#endif

#endif // _INCLUDE_DNBSIMSTEP
