/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBIntervalTimer.h
//      Interval Timer implementation class -   Used for participatating
//      in clock advancement and time-based functor dispatching.
//  
//      DNBIntervalTimer proposes a time based on the time passed on to it.
//
//==============================================================================
//
// Usage notes: 
//      Usually tied to the Time based Simlets(proclets). The time-based Simlets
//      register their timers with the Clock.  The timers participate in Clock
//      advancement and time-based functor dispatching. 
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     chris        01/01/1999   Initial Development
//     rtl          10/01/2000   Included a "shift_" time . This was essentially
//                               added to provide ability to "shift" the time 
//                               base during an activity(proclet)  execution,
//                               while retaining the interval
//     mmg          03/06/2003   added minRes_ data member and methods
//     
//
//==============================================================================
#if DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _INCLUDE_DNBINTERVALTIMER
#define _INCLUDE_DNBINTERVALTIMER

/**
 * Standard includes.
 */
#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>

/**
 * Deneb includes.
 */
#include <DNBSimulationControl.h>
#include <DNBTimeletTimer.h>
#include <DNBSimTimeLimits.h>

class ExportedByDNBSimulationControl DNBIntervalTimerImpl :
public DNBTimeletTimerImpl
{
public:
    
    /**
     * I always do this.
     */
    typedef DNBIntervalTimerImpl           Self;
    
    /**
     * Interval time constructor.
     */
    DNBIntervalTimerImpl( const Time& intervalTime,
        const Time& beginTime,
        const Time& endTime,
        const Time& shift=0.0 )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Assignment operator.
     */
    virtual
    ~DNBIntervalTimerImpl()
        DNB_THROW_SPEC_NULL;
    
    /**
     * Expired if the supplied time is greater than the 'interval' time.
     */
    bool
    expired( const Time& time )
        DNB_THROW_SPEC_ANY;
    
    /**
     * If the timer has not expired, this calculates the next time to propose.
     */
    Time
    propose( const Time& minTime, const Time& maxTime )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Included only if the supplied time is the same as the 'interval' time.
     */
    bool
    contains( const Time& minTime, const Time& maxTime )
        DNB_THROW_SPEC_ANY;

    /**
     * TRUE if supplied time is on the the 'interval' time.
     */
    bool
    onInterval( const Time& time )
        DNB_THROW_SPEC_ANY;
    
    /**
     * enable timer
     */
    void
    enable()
        DNB_THROW_SPEC_ANY;

    /**
     * Get the interval time.
     */
    inline
    Time
    getInterval() const
        DNB_THROW_SPEC_ANY;
    
    /**
     * Set the interval time.
     */
    inline
    void
    setInterval( const Time& interval )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Get the start time.
     */
    inline
    Time
    getBeginTime() const
        DNB_THROW_SPEC_ANY;
    
    /**
     * Set the start time.
     */
    inline
    void
    setBeginTime( const Time& startTime )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Get the end time.
     */
    inline
    Time
    getEndTime() const
        DNB_THROW_SPEC_ANY;
    
    /**
     * Set the end time.
     */
    inline
    void
    setEndTime( const Time& endTime )
        DNB_THROW_SPEC_ANY;
    
    
    /**
     * Get the Shift
     */
    inline
    Time
    getShift() const
        DNB_THROW_SPEC_ANY;
    
    /**
     * Set the Shift
     */
    inline
    void
    setShift( const Time& shift )
        DNB_THROW_SPEC_ANY;

    /**
     * Set the minRes from the clock
     */
    inline
    void
    setMinRes( const Time& minRes )
        DNB_THROW_SPEC_ANY;

private:
    
    /**
     * Store the time vector sample.
     */
    Time        beginTime_;
    Time        endTime_;
    Time        intervalTime_;
    Time        shift_;

    Time        lastIntervalTime_;
    Time        internalShift_;
    Time         minRes_;
    
    /**
     * Undefined for now since this is read-only handle-body.
     */
    DNBIntervalTimerImpl( const Self& copy )
        DNB_THROW_SPEC_ANY;
    void operator=( const Self& rhs );
};

/**
 * The 'interval' timer interface.  For now, changing the 'interval' time is
 * disallowed because I need COW semantics and I don't have time to properly
 * implement them.  TBD.
 */
class ExportedByDNBSimulationControl DNBIntervalTimer : public DNBTimeletTimer
{
public:
    
    /**
     * I always do this.
     */
    typedef DNBIntervalTimer               Self;
    
    /**
     * Shorten the type name for the implementation.
     */
    typedef DNBIntervalTimerImpl           Impl;
    
    /**
     * Default constructor creates an empty time vector.
     */
    DNBIntervalTimer()
        DNB_THROW_SPEC_ANY;
    
    /**
     * Time constructor creates with the supplied for time.
     */
    DNBIntervalTimer( const Time& intervalTime, 
        const Time& beginTime = scl_numeric_limits<DNBSimTime>::min(),
        const Time& endTime   = scl_numeric_limits<DNBSimTime>::max(),
        const Time& shift     = 0.0 )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Copy constructor performs shallow copy.
     */
    DNBIntervalTimer( const Self& copy )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Destructor.
     */
    virtual
    ~DNBIntervalTimer()
        DNB_THROW_SPEC_ANY;
    
    /**
     * Assignment operator.
     */
    inline
    Self&
    operator=( const Self& rhs )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Get the interval time.
     */
    inline
    Time
    getInterval() const
        DNB_THROW_SPEC_ANY;
    
    /**
     * Set the interval time.
     */
    inline
    void
    setInterval( const Time& interval )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Get the start time.
     */
    inline
    Time
    getBeginTime() const
        DNB_THROW_SPEC_ANY;
    
    /**
     * Set the start time.
     */
    inline
    void
    setBeginTime( const Time& startTime )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Get the end time.
     */
    inline
    Time
    getEndTime() const
        DNB_THROW_SPEC_ANY;
    
    /**
     * Set the end time.
     */
    inline
    void
    setEndTime( const Time& endTime )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Get the Shift
     */
    inline
    Time
    getShift() const
        DNB_THROW_SPEC_ANY;
    
    /**
     * Set the Shift
     */
    inline
    void
    setShift( const Time& shift )
        DNB_THROW_SPEC_ANY;
    
    /**
     * check for an Interval
     */
    bool
    onInterval( const Time& time )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Set the minRes from the clock
     */
    inline
    void
    setMinRes( const Time& minRes )
        DNB_THROW_SPEC_ANY;

protected:
    
    /**
     * Convenience for getting at the implementation.
     */
    inline
    Impl&
    body() const
        DNB_THROW_SPEC_ANY;
};

/**
 * Snarf the public inline definition file.
 */
#include <DNBIntervalTimer.cc>

#endif // _INCLUDE_DNBINTERVALTIMER

