/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBSimEventService.h
//      The Event Notifier scheme.  
//      Responsible for dispatching the publised sim events to the interested
//      subscribers by dispatching the functors.  
//
//==============================================================================
//
// Usage notes: 
//      DNBSimEventService is a Singleton. It is tied to the Process Controller.
//      The Subscribers subscribe to the interested Events by registering their
//      event handlers with the Event Service.  
//      Publishers publish the events with the Event Service.  
//      Event Service is responsible for notifing the interested subsicribers of
//      events whenever they are published. 
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     rtl          01/20/2001   Initial Development
//     smw          04/08/2005   Replaced RWPCPtrQueue < T > with DNBSysPCValQueue< T * >
//     rtl          02/16/2006   Provide method to clear all the subscribers
//
//==============================================================================
#define USE_RW 0

#define HAS_DNB_SIMEVENT 1

#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _DNB_DNBSimEventService_H_
#define _DNB_DNBSimEventService_H_

#include <DNBSystemBase.h>
#include <scl_list.h>
#include <DNBSysPCValQueue.h>

#include <DNBSystemDefs.h>

#include <DNBSimulationControl.h>
#include <DNBClassInfo.h>
#include <DNBCallback.h>


class DNBBasicSimEvent;


/**
 *  The following relational operators are required to pacify the AIX C++
 *  compiler.
 */
bool
operator==(const DNBBasicSimEvent &left, const DNBBasicSimEvent &right );

bool
operator<(const DNBBasicSimEvent &left, const DNBBasicSimEvent &right ) ;



/**
 *DNBSimEventService Class
 */
class ExportedByDNBSimulationControl DNBSimEventService
{
public:

    /**
     *Signature of the event handler
     */

    typedef DNBFunctor1<const DNBBasicSimEvent* > EventHandler;

    //typedef EventHandler *                        EvencHandlerF;
    
    /**
     *Static Instance Method. Used by clients to get to the single
     *Instance of Event Service.
     */
    static DNBSimEventService* Instance();
    
    ~DNBSimEventService()
        DNB_THROW_SPEC_NULL;
    
    /**
     *Called by publishers to publish events 
     */
    void publish(DNBBasicSimEvent* event)
        DNB_THROW_SPEC_NULL;
    
    
    /**
     *Called by subscribers to register for events of interest
     *The subscribers need to pass the ClassInfo of the events they are
     *interested in and the corresponding event handlers
     */
    bool subscribe(const DNBClassInfo& eventClassInfo, const EventHandler& handler)
        DNB_THROW_SPEC_NULL;
    
    
    /**
     *To unsubscribe the registered events and respective event handlers
     */
    bool unsubscribe(const DNBClassInfo& eventClassInfo, const EventHandler& handler)
        DNB_THROW_SPEC_NULL;
    
    
    /**
     *Check to see if there are any events in the queue
     */
    bool isEventPending()
        DNB_THROW_SPEC_NULL;
    
    
    /**
     *dispatch the events
     */
    void dispatch()
        DNB_THROW_SPEC_NULL;

	/**
	 * Clears all the subscribers
	 */
	void clearSubscribers()
		DNB_THROW_SPEC_NULL;

    
protected:

    DNBSimEventService()
        DNB_THROW_SPEC_NULL;
    
    /**
     *Notify the interested subscribers   
     */
    void notify(DNBBasicSimEvent* event)
        DNB_THROW_SPEC_NULL;
    
    
    
        /**
     *DNBSubscription Class
     */
    class DNBSubscription
    {
    public:

        /**
         *Default Constructor
         */
        DNBSubscription( )
            DNB_THROW_SPEC_NULL;
        
        
        /**
         *Constructor
         */
        DNBSubscription(const DNBClassInfo& eventtype,const EventHandler& handler)
            DNB_THROW_SPEC_NULL;
        
        
        /**
         *Copy Constructor
         */
        DNBSubscription( const DNBSubscription &right )
            DNB_THROW_SPEC_NULL;
        
        
        /**
         *Destructor
         */
        ~DNBSubscription()
            DNB_THROW_SPEC_NULL;
        
        
        /**
         *Overloaded assignment operator
         */
        void operator=( const DNBSubscription &right )
            DNB_THROW_SPEC_NULL;
        
        
        /**
         *Overloaded == operator
         */
        bool operator==( const DNBSubscription &right ) const
            DNB_THROW_SPEC_NULL;
        
        
        /**
         *Overloaded < operator
         */
        bool operator<( const DNBSubscription &right ) const
            DNB_THROW_SPEC_NULL;
        
        const DNBClassInfo*  eventtype_;
        EventHandler         handler_;
    };
    
    typedef scl_list<DNBSubscription, DNB_ALLOCATOR(DNBSubscription) > SubscriptionList;

    typedef DNBSysPCValQueue<DNBBasicSimEvent * > PublishedEventQueue;
    
    typedef SubscriptionList::iterator SubscriptionIterator;
    
private:
        
    /**
     *List of Subscriptions
     */
    SubscriptionList eventSubscriptionList_;
    
    
    /**
     *Queue of Published Events 
     */
    PublishedEventQueue publishedEventQueue_;
    
    static DNBSimEventService* instance_;
    
};


#endif // _DNB_DNBSimEventService_H_
