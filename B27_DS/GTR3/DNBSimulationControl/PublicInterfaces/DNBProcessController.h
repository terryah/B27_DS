//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
/** @CAA2Required */
/**********************************************************************/
/* DON T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPEAR AT ANY TIME */
/**********************************************************************/
//==============================================================================
//
// DNBProcessController.h
//      Process Controller maintains a graph with
//      a) all the proclets as its nodes and 
//      b) proclets' sequence  and order  as edges 
//
//      The Process Controller controls the scheduling of the proclets based on 
//      the sequence in which they are stored in the graph. 
//
//==============================================================================
//
// Usage notes: 
//      The Process Controller registers its handler with DNBTimeletClock
//      and is executed on every time tick the Clock proposes. 
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     rtl          08/25/2000   Initial Development
//     sha          06/05/2001   Updated syncronize() to accept a second default
//                               argument which represents the last proclet.
//     rtl          04/22/2002   Includee class macros to make the class traceable
//     rtl          04/15/2002   Code change for simulating part of process, introduce
//                               loops in process chain
//     rtl          05/02/2002   Changed return-type of addNode method
//     rtl          09/19/2002   Provided methods removeFromActiveList(), 
//                               getActiveProcletList()
//     rtl          10/24/2002   Provided new methods , modified existing methods
//                               for re-using subgraphs
//     rtl          12/11/2002   Provided methods to manipulate iterators rather
//                               than nodes: isPath(), getChildren()
//     rtl          12/16/2002   Provide methods for managing runstate notifications.
//                               Provide methods for destroying the graph, and populating
//                               graph's map
//     mmh          02/06/2003   Add method to retrieve the graph
//     rtl          02/24/2003   Add datamember/methods for processing a list of 
//                               simlets. These simlets' reset will be processed
//                               before any of activities' proclets reset() are called
//     rtl          03/25/2003   Add method to check for cycles in the graph
//     rtl          04/15/2003   Change RTTI macro to the one with no ctor
//     rtl          05/12/2003   Provided methods to propogate exceptional conditions
//                               from PC (to the appln.)
//     rtl          11/06/2003   Code change for using the new Trace facility
//     rtl          11/04/2003   Provided methods to schedule/stop/re-schedule 
//                               proclets based on thier resource and task Names
//     rtl          03/26/2004   Fix for IR0435975. Modify removeFromActiveList()
//                               to handle maxWaitTime
//     rtl          04/14/2004   Fix for IR0439778. Maintain a list of proclets
//                               that are in wait state, to set their controller
//                               (clock) back in the reset method
//     rtl          04/30/2004   Added methods to suspend and cancel tasks. Renamed
//                               stopTask to suspendTask and schedule to scheduleTask
//     rtl          03/14/2005   Added methods to schedule Tasks based on resourceHandle
//     mmh          03/17/2005   Add method to push a scl_wstring into the list
//                               of exception messages
//     smw          06/04/2005   Removed the use of Rogue Wave producer consumer queue
//     rtl          05/04/2005   Add support for enabling/disabling a sequence
//                               of proclets.
//     rtl          04/25/2006   A0534427 Fix for starting simulation from the 
//                               "Specified Start time" of the selected activity
//     rtl          08/25/2006   Support for simulation time point determination for 
//                               resources with behaviour
//     rtl          12/06/2006   IR 0561990 Fix. Code change for scheduling resource tasks
//                               based on their handles( and not based on their names )
//     rtl          12/19/2006   R18 Highlight ( LineTracking ). Schedule/ execute proclets
//                               based on their priorities
//     rtl          01/10/2007   Code cleanup. Remove activeBranch code
//     mmh          10/29/2007   Fix 598945: set priorities for proclets 
//                               belonging to resource tasks, to prevent deadlock
//                               when multiple tasks of the same resource are
//                               simulated in parallel
//
//==============================================================================

#define USE_RW 0       // smw: temporary for smoother migration

#if DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _INCLUDE_DNBPROCESSCONTROLLER
#define _INCLUDE_DNBPROCESSCONTROLLER

#include <DNBSystemBase.h>
#include <scl_list.h>
#include <scl_map.h>
#include <DNBSysPCValQueue.h>
#include <DNBSystemDefs.h>

//
// Deneb includes
//

#include <DNBTimelet.h>
#include <DNBSimlet.h>
#include <DNBTimeletClock.h>

#include <DNBSimGraph.h>
#include <DNBSimulationControl.h>
#include <DNBProcletMessage.h>
#include <DNBProclet.h>

class DNBSimEventService;

class  ExportedByDNBSimulationControl DNBProcessController:public DNBTimelet
{
    DNB_DECLARE_SIMLET_NOCTR(DNBProcessController);
    
public:

    /**
     * Enums & Typedefs
     */

    enum GraphNode { Root, Terminal, Node };

    enum Action {Schedule,Cancel};

	enum DNBProcletLocation {None, Begin, Update, End};


    typedef DNBProcessController Self;

    typedef DNBProclet *Proclet;
    
    /**
     * Node data 
     */
    typedef Proclet Nodedata;

    
    /**
     * Edge data represents the state of its source node. If the
     * source node has completed the end operation then edge data is
     * set to true. 
     */
    typedef bool Edgedata;
    
    typedef	DNBSimGraph<Nodedata,Edgedata>::node_iterator NodeIterator;
    typedef	DNBSimGraph<Nodedata,Edgedata>::edge_iterator EdgeIterator;
    
    typedef DNBProcletMessage::MessageType Mtype;
    
    typedef scl_list<Proclet,DNB_ALLOCATOR(Proclet) > ProcletList;

    typedef ProcletList::iterator ProclistIterator;

    typedef scl_list<NodeIterator,DNB_ALLOCATOR(NodeIterator) > NodeIteratorList;

    typedef DNBSimlet *Simlet;
    typedef scl_list<Simlet, DNB_ALLOCATOR(Simlet) > SimletList;
    typedef SimletList::iterator SimListIterator;

    typedef scl_list<scl_wstring, DNB_ALLOCATOR(scl_wstring) > ExMsgList;
    typedef ExMsgList::iterator ExMsgIterator;

    typedef scl_map <const void*,ProcletList,scl_less<const void*> > ResPletsMap;

    //enum DNBStopLocation { None, Begin, Update };


    typedef scl_map <Proclet, DNBProcletLocation, scl_less<Proclet> > ResProcletMap;
    typedef ResProcletMap::iterator       MapIterator;
    typedef ResProcletMap::value_type     MapValueType;
    typedef ResProcletMap::key_type       MapKeyType;


    struct DisabledProclets
    {
        Proclet targetProclet_;
        int     nodeCount_;
       
        DisabledProclets(Proclet targetProclet=NULL, int nodeCount=0)
            :targetProclet_(targetProclet), nodeCount_(nodeCount)
        {}
        DisabledProclets(const DisabledProclets& second)
            :targetProclet_(second.targetProclet_),nodeCount_(second.nodeCount_)
        {}
    };

    typedef scl_map <Proclet, DisabledProclets, scl_less<Proclet> > DisabledProcletsMap;
    typedef DisabledProcletsMap::iterator       DMapIterator;
    typedef DisabledProcletsMap::value_type     DMapValueType;
    typedef DisabledProcletsMap::key_type       DMapKeyType;


	typedef scl_multimap <unsigned short, TimeHandler, scl_less<unsigned short> > PriorityMap;
    typedef PriorityMap::iterator       PriorityIterator;
    typedef PriorityMap::value_type     PriorityValueType;
    typedef PriorityMap::key_type       PriorityKeyType;
	typedef scl_pair <PriorityIterator, PriorityIterator> IteratorPair;

	//typedef scl_list<TimeHandler,DNB_ALLOCATOR(TimeHandler) >    HandlerList;

    
    /**
     * Constructor & Destructor
     */
    DNBProcessController()
        DNB_THROW_SPEC_ANY;

    
    ~DNBProcessController()
        DNB_THROW_SPEC_ANY;
    
    /**
     * init schedules all the begin handles of the root proclets with the clock
     */
    void
    init(const RunStateEventData& data)
        DNB_THROW_SPEC_ANY;    

    /**
     * Reset the graph and the PC
     */
    void
    reset(const RunStateEventData& data)
        DNB_THROW_SPEC_ANY;

    /**
     * Other state notification methods
     */
    void
    suspend( const RunStateEventData& data )
        DNB_THROW_SPEC_ANY;
    
    void
    resume( const RunStateEventData& data )
        DNB_THROW_SPEC_ANY;
    
    void
    fini( const RunStateEventData& data )
        DNB_THROW_SPEC_ANY;
    
    void
    destroy( const RunStateEventData& data )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Synchronizes the state after adding a new proclet.
     * Should be called when a new proclet is added during a Simulation run.
     */
    void
    synchronize( Proclet startProclet, Proclet stopProclet = NULL )
        DNB_THROW_SPEC_ANY;
    

    /**
     * FUNCTIONS FOR BUILDING AND EDITING GRAPH
     */

    /**
     * Adds node to the graph_
     */
    bool
    addNode(Proclet nodeproclet)
    DNB_THROW_SPEC_ANY;


    /**
     * Remove a node from the graph_
     */
    bool
    removeNode(Proclet nodeProclet)
        DNB_THROW_SPEC_ANY;
    

    /**
     * Connects two nodes of the graph_
     */
    bool
    setPrecedence(Proclet sourceProclet,Proclet targetProclet,bool edgevalue=false)
    DNB_THROW_SPEC_ANY;


    /**
     * Removes the link between two nodes in the graph_
     */
    bool 
    removePrecedence(Proclet sourceProclet, Proclet targetProclet)
        DNB_THROW_SPEC_ANY;

    /**
     * If the soruce and target proclets are linked, then sets
     * the targetProclet's isEnabled with value
     */
    bool
    markLink(Proclet sourceProclet, Proclet targetProclet,bool value)
        DNB_THROW_SPEC_ANY;

    /**
     * Cleanup the all the nodes starting from sourceproclet to targetproclet
     */

    bool
    cleanupNodes(Proclet sourceProclet, Proclet targetProclet)
        DNB_THROW_SPEC_ANY;
    

    /**
     * Query function to find out whether two proclets(nodes) are linked in 
     * the graph_
     */
    bool
    isLinked(Proclet sourceProclet, Proclet targetProclet)
        DNB_THROW_SPEC_ANY;


    /**
     * Query function to find out whether there is a path between two proclets
     * in the graph_
     */
    bool
    isPath(Proclet sourceProclet, Proclet targetProclet)
        DNB_THROW_SPEC_ANY;


    /**
     * Query function to find out whether there is a path between two proclets
     * in the graph_ (takes iterators rather than nodes in the graph)
     */
    bool
    isPath(NodeIterator &sourceNode, NodeIterator &targetNode)
        DNB_THROW_SPEC_ANY;


    /**
     * Recursive function for finding if there exists a path between any two 
     * nodes(proclets) in the graph_
     */
    void
    findPath(NodeIterator &sourceNode, 
             NodeIterator &targetNode, 
             bool &isLinked)
        DNB_THROW_SPEC_ANY;
    

    /**
     * Notify the process controller what function(begin,update or end) the 
     * proclet has executed
     */
    inline
    void
    notify(Proclet pc,Mtype message)
        DNB_THROW_SPEC_ANY;
    
    
    /**
     * Schedules/Removes the begin timers and handlers from the clock
     */
    void
    doBegin(Proclet &proclet,Action action)
        DNB_THROW_SPEC_ANY;

    
    /**
     * Schedules/Removes the update timers and handlers from the clock
     */
    void
    doUpdate(Proclet &proclet,Action action)
        DNB_THROW_SPEC_ANY;
    

    /**
     * Schedules the timers with the clock
     */
    void
    scheduleTimer(Proclet &proclet, DNBProcletLocation location)
        DNB_THROW_SPEC_ANY;
    

    /**
     * Schedules the TimeHandlers with the priorityDispatcher_
     */
    void
    scheduleHandler(Proclet &proclet, DNBProcletLocation location, const TimeEvent& evTime)
        DNB_THROW_SPEC_ANY;

    /**
     * Schedules the Timers and TimeHandlers with the priorityDispatcher_
     */
    void
    scheduleTimerHandler(Proclet &proclet, DNBProcletLocation location, const TimeEvent& evTime)
        DNB_THROW_SPEC_ANY;
    

    /**
     * Removes the timers from the clock
     */
    void
    removeTimer(Proclet &proclet, DNBProcletLocation location)
        DNB_THROW_SPEC_ANY;
    

    /**
     * Removes the TimeHandlers from the priorityDispatcher_
     */
    void
    removeHandler(Proclet &proclet, DNBProcletLocation location)
        DNB_THROW_SPEC_ANY;

    /**
     * Removes the Timers and TimeHandlers from the priorityDispatcher_
     */
    void
    removeTimerHandler(Proclet &proclet, DNBProcletLocation location)
        DNB_THROW_SPEC_ANY;


    /**
     * Runs through the tempList and schedules the proclets which are due to be
     * scheduled at a given time step
     */
    void
    processTempList(const TimeEvent& evTime,
					ProcletList &templist,
					bool &breakpoint)
        DNB_THROW_SPEC_ANY;

  

    /**
     * Sets a flag which forces a DNBEDone exception to be thrown when the 
     * simulation is over
     */
    inline
    void
    setDoneException()
        DNB_THROW_SPEC_ANY;

    /**
     * Resets the DNBEDone throw exception flag at the end of the simulation
     */
    inline
    void
    resetDoneException()
        DNB_THROW_SPEC_ANY;
    

    /**
     * Check to see if we have reached the last(terminal) node in the graph
     */
    inline
    bool
    checkIfSimulationOver()
        DNB_THROW_SPEC_ANY;


    /*
     *Adds a proclet to the active list. This results in the proclet being
     *processed/simulated from the current time
     */
    void
    addToActiveList(Proclet proclet)
        DNB_THROW_SPEC_ANY;

    /*
     *Removes the proclet from active list, during simulation.
     */
    void
    removeFromActiveList( DNBProclet* proclet,bool maxTimeSpecified = false)
        DNB_THROW_SPEC_ANY;

    /*
     *Gets the list of proclets which are currently active, ie. which are
     *being processed/simulated. 
     */
    ProcletList
    getActiveProcletList()
        DNB_THROW_SPEC_ANY;


    /*
     * Populates the graph's map with values (<Proclet,node_iterator> pair)
     */
    inline
    void
    populateMapDB()
        DNB_THROW_SPEC_ANY;

    /*
     * Destroys the graph by deleting the nodes and edges from the graph
     */
    inline
    void
    destroyGraph()
        DNB_THROW_SPEC_ANY;


    /*
     * Finds the node in the graph  to which the proclet belongs to
     * Search can be done in all the nodes, root nodes or terminal nodes
     */
    bool
    findNode(Proclet &proclet,
             NodeIterator first,
             NodeIterator last,
             NodeIterator &result)
        DNB_THROW_SPEC_ANY;

    /*
     * Finds the resource node in the graph to which the proclet belongs to
     * Search can be done in root nodes or terminal nodes
     */
    bool
    findResourceNode(scl_wstring resName, scl_wstring taskName,Proclet& plet, GraphNode where)
        DNB_THROW_SPEC_ANY;

    /*
     * Finds if the proclet is present in the given proclet list(proclist)
     */
    bool
    findProclet(Proclet proclet, ProclistIterator& iter, ProcletList& proclist)
        DNB_THROW_SPEC_ANY;

    /**
     * Finds if the proclet with resName/taskName is present in the given
     * list
     */
    bool
    findProclet(scl_wstring resName,
                scl_wstring taskName,
                ProclistIterator& iter,
                DNBProcessController::ProcletList& proclist)
    DNB_THROW_SPEC_ANY;


    /**
     * Finds if the proclet with resName is present in the given
     * list
     */
    bool
    findProclet(scl_wstring resName,
                ProclistIterator& iter,
                DNBProcessController::ProcletList& proclist)
    DNB_THROW_SPEC_ANY;



    /**
     * Finds if the proclet with resName/taskName is present in the given
     * map
     */
    bool
    findProclet(scl_wstring resName,
                MapIterator& iter,
                ResProcletMap& procMap)
    DNB_THROW_SPEC_ANY;


    /*
     * Accessors and Mutators
     */
    inline
    void
    setProcletType(DNBProclet::ProcletType type)
        DNB_THROW_SPEC_ANY;

    inline
    void
    setResourceName(scl_wstring resName)
        DNB_THROW_SPEC_ANY;

    inline
    void
    setTaskName(scl_wstring taskName)
        DNB_THROW_SPEC_ANY;
    
    inline
    DNBProclet::ProcletType
    getProcletType()
        DNB_THROW_SPEC_ANY;

    inline
    scl_wstring
    getResourceName()
        DNB_THROW_SPEC_ANY;

    inline
    scl_wstring
    getTaskName()
        DNB_THROW_SPEC_ANY;

    /**
     * Get the children of the given proclet
     */
    bool
    getChildren(Proclet plet,ProcletList& childrenList)
        DNB_THROW_SPEC_ANY;

	/**
	 * Get the all the proclets that are pointing to iPlet
	 * (ie. plets that have In-edges to iPlet )
	 */

	bool
	getPredecessorList(Proclet iPlet, ProcletList& oPredecessorList )
		DNB_THROW_SPEC_ANY;

	/**
	 * Get the all the proclets that iPlet points to 
	 * (ie. all plets that iPlet has out-edge to )
	 */
	bool
	getSuccessorList(Proclet iPlet, ProcletList& oSuccessorList )
		DNB_THROW_SPEC_ANY;

    void
    handleBeginProcessed()
        DNB_THROW_SPEC_ANY;

    void
    handleUpdateProcessed()
        DNB_THROW_SPEC_ANY;

    void
    handleEndProcessed()
        DNB_THROW_SPEC_ANY;
    
    /**
     * Returns the graph.
     */
    DNBSimGraph<Nodedata,Edgedata>&
    getSimGraph()
        DNB_THROW_SPEC_ANY;

    void
    insertSimlet(Simlet slet)
        DNB_THROW_SPEC_ANY;

	/*
	 * checks the graph to see if there is are cycles in the graph. 
	 * Returns true if there is no cycle, false otherwise
	 */
	bool
	checkCycle()
	    DNB_THROW_SPEC_ANY;


    void
    pushExMsg(scl_string str)
        DNB_THROW_SPEC_ANY;

    void
    pushWExMsg(scl_wstring wstr)
        DNB_THROW_SPEC_ANY;

    ExMsgList &
    getExMessageList()
        DNB_THROW_SPEC_ANY;

    /**
     * Schedules the proclet(s) that match the
     * resName and taskName
     */
    bool
    scheduleTask(const scl_wstring &resName, const scl_wstring &taskName)
        DNB_THROW_SPEC_ANY;

    /**
     * Stops the proclet with resName/taskname
     */
    bool
    suspendTask(const scl_wstring &resName)
    DNB_THROW_SPEC_ANY;

    /**
     * Re-schedules the stopped proclet with resName/taskName
     */
    bool
    resumeTask(const scl_wstring &resName)
    DNB_THROW_SPEC_ANY;

    /**
     * Query function for checking whether a task with a given 
     * Resource/Task Name has been suspended
     * 
     */
    bool
    isSuspended(const scl_wstring &resName)
    DNB_THROW_SPEC_ANY;

    /**
     * Cancels a suspended task with the given Resource/Task Name
     * Returns true is the cancellation was successful , false otherwise
     */
    bool
    cancelTask(const scl_wstring &resName)
    DNB_THROW_SPEC_ANY;
     

    /**
     * Sets either all the proclets resName/taskName including the begin and
     * end proclets or just the begin and end Proclets 
     */
    bool
    markResourceProclets(Proclet beginProclet,
                         Proclet endProclet,
                         scl_wstring &resName, 
                         scl_wstring &taskName,
                         void* resHandle = NULL,
						 void* taskHandle = NULL,
                         bool allProclets=false)
    DNB_THROW_SPEC_ANY;

    bool
    setPriorityResPlets()
    DNB_THROW_SPEC_ANY;


    bool
    findProclet(const void* resHandle,
                ProclistIterator& iter,
                DNBProcessController::ProcletList& proclist)
    DNB_THROW_SPEC_ANY;


    bool
    findProclet(const void* resHandle,
                MapIterator& iter,
                ResProcletMap& procMap)
    DNB_THROW_SPEC_ANY;


    bool
    findResourceNode(const void* resHandle,
					 scl_wstring taskName,
					 Proclet& plet,
					 GraphNode where)
        DNB_THROW_SPEC_ANY;


    bool
    findResourceNode(const void* resHandle,
					 const void* taskHandle,
					 Proclet& plet,
					 GraphNode where)
        DNB_THROW_SPEC_ANY;


    bool
    scheduleTask(const void* resHandle, const scl_wstring &taskName)
        DNB_THROW_SPEC_ANY;

    bool
    scheduleTask(const void* resHandle, const void* taskHandle)
        DNB_THROW_SPEC_ANY;



    bool
    suspendTask(const void* resHandle)
    DNB_THROW_SPEC_ANY;


    bool
    resumeTask(const void* resHandle)
    DNB_THROW_SPEC_ANY;


    bool
    isSuspended(const void* resHandle)
    DNB_THROW_SPEC_ANY;


    bool
    cancelTask(const void* resHandle)
    DNB_THROW_SPEC_ANY;

    bool
    disableProcletList(Proclet sourceProclet, Proclet targetProclet)
    DNB_THROW_SPEC_ANY;

    bool
    enableProcletList(Proclet sourceProclet)
    DNB_THROW_SPEC_ANY;

    bool
    enableProcletList(Proclet sourceProclet, Proclet targetProclet)
    DNB_THROW_SPEC_ANY;

    void
    enableAllProclets()
    DNB_THROW_SPEC_ANY;
     
    // smw added 04/28/2006
    /*
     * Returns a list of the current root proclets of the graph
     */
    scl_vector<DNBProclet*> 
    GetRootProcletList();

    // smw added 05/9/2006
    /*
     * Returns a list of the current root proclets of the graph
     */
    scl_vector<DNBProclet*> 
    GetTerminalProcletList();

    void
    processNotReadyList(const TimeEvent& evTime,
						ProcletList &currentActiveList,
						bool &breakpoint )
		DNB_THROW_SPEC_ANY;

protected:

    /**
     * Method that takes the first disabled proclet in the
     * sequence and returns the list of enabled proclets that are
     * immediately past that disabled sequence
     */
    void
    getNextEnabledProclets(Proclet disabledProclet, ProcletList& nextEnabledList)
    DNB_THROW_SPEC_ANY;


    void
    disableProcletList(NodeIterator &beginNode,
                       NodeIterator &endNode,
                       bool value = false)
                       
    DNB_THROW_SPEC_ANY;

    void
    markResourceProclets(NodeIterator &beginNode,
                         NodeIterator &endProclet,
                         scl_wstring &resname, 
                         scl_wstring &taskname,
                         void* resHandle,
						 void* taskHandle)
    DNB_THROW_SPEC_ANY;
    
    void
    setPriorityResPlets( NodeIterator &beginNode,
                         NodeIterator &endNode,
                         unsigned short iPriority )
    DNB_THROW_SPEC_ANY;

    /**
     * Traverse to the next level of nodes
     */
    void
    traverse(NodeIterator &node,
                 const TimeEvent& evTime,
                 ProcletList &currentActiveList,
                 bool &breakpoint)
        DNB_THROW_SPEC_ANY;
    
    
    /**
     * Set all  out-edge attributes of the given node 
     */
    void
    setOutEdgeAttribute(NodeIterator node,bool value)
        DNB_THROW_SPEC_ANY;
    
    
    /**
     * Get the children (iterator list) of the given node
     */
    void
    getChildrenList(NodeIterator node,NodeIteratorList& childrenList)
        DNB_THROW_SPEC_ANY;  
    
    /**
     * Get the children (proclet list) of the given node
     */
    void
    getChildrenList(NodeIterator node,ProcletList& childrenList)
        DNB_THROW_SPEC_ANY;    

    /**
     * Get the predecessors (proclet list) of the given node
     */
    void
    getPredecessorList(NodeIterator node,ProcletList& childrenList)
        DNB_THROW_SPEC_ANY;    
    
    /**
     * Check if all the in-edges of the given node are true.
     */
    bool
    checkExecutionDependency(NodeIterator node)
        DNB_THROW_SPEC_ANY;


    /**
     * Used to cleanup proclets
     */
    void
    cleanup(NodeIterator &sourceNode, NodeIterator &targetNode) 
        DNB_THROW_SPEC_ANY;



    /**
     * DEPRECATED FUNCTIONS. 
     */
    void
    processbegin(Proclet currentProclet, ProcletList &templist)
        DNB_THROW_SPEC_ANY;

    void
    setEdgeAttribute(NodeIterator& node,bool value)
        DNB_THROW_SPEC_ANY;
   
private:
    
    void 
    OnTimeTick(const TimeEvent& evTime);


	/*
	 * Dispatches the Handlers to the Reactive Kernel based on priority
	 * set in the proclet
	 */
    void 
    dispatchHandlers(const TimeEvent& evTime)
		DNB_THROW_SPEC_ANY;

    /**
     * A graph_ object to hold all the nodes.Each node in the graph represent 
     * a proclet.
     */
    DNBSimGraph <Nodedata,Edgedata>    graph_;
    

    /**
     * A list of all active proclets;
     */
    ProcletList                     activeProcletList_;
    ProcletList                     tempList_;
    //
    //List of all Wait proclets. Normally Wait proclets are used as the first 
    //proclet in a loop. 
    ProcletList                     waitList_;
    
    
    /**
     * Message queue to hold the messages sent by the proclets
     */
     DNBSysPCValQueue<DNBProcletMessage> messageQueue_;

    /**
     * TimeHandler 
     */
    TimeHandler                     onTimeHandler_;


    /**
     * Flags to indicate whether the Simulation is over and/or breakpoint 
     * reached 
     */
    bool                            simOver;
    bool                            breakpoint;

    
    /**
     * Flag to indicate whether a DNBEDone exception is/is not thrown at the end
     * of the simulation.
     */
    bool                            forceDoneException_;

    
    /**
     * Event Notifier
     */
    DNBSimEventService*             Notifier;


    DNBProclet::ProcletType         procletType_;
    scl_wstring                         resourceName_;
    scl_wstring                         taskName_;

    /**
     * List of Simlets. Typically used for processing those
     * simlets whose state transition actions(reset, suspend...) should happen before
     * any of the activities' proclets
     */
    SimletList                      simList_;


    /**
     * List to manage exception messages, to be notified to the user
     */
    ExMsgList                       exMsgList_;
    scl_string                          exMsg_;
    
    /**
     * Map to manage arbitrary stoppage/suspension and resumption of resource Proclets
     *
     */
    ResProcletMap                   resProcletMap_;

    /**
     * List to manage the begin and end proclets of a Resource Task
     */
    ProcletList                     resProcRootList_;
    ProcletList                     resProcTerminalList_;

    /**
     * List to manage the proclets that are in wait state
     */
    ProcletList                     waitProcletList_;

    /**
     * List of proclets that were scheduled by..... the schedule method :) 
     */
    ProcletList                     rootTempList_;


    /**
     * Map to manage sequence of proclets that are disabled. 
     * Source Proclet = key 
     * Target Proclet = value
     *
     */
    DisabledProcletsMap                   disabledProcletsMap_;

	ProcletList                           notReadyList_;

	PriorityMap                           priorityDispatcher_;
	ProcletList                           updateHandlerList_;
	bool                                  isUpdateProcessed_;
	bool                                  disablePriorityScheduling_;
    
    
};

#include <DNBProcessController.cc>

#endif //_INCLUDE_DNBPROCESSCONTROLLER
