/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBSimletException.h
//      DNB Exceptions Class
//
//==============================================================================
//
// Usage notes: 
//      DNB Exceptions Class
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     chris        01/01/1999   Initial Development
//     rtl          05/21/2001   Added DNBEBreak Exception
//
//==============================================================================
#ifndef _INCLUDE_DNBSIMLETEXCEPTION
#define _INCLUDE_DNBSIMLETEXCEPTION

#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>

#include <DNBSimulationControl.h>
#include <DNBException.h>

DNB_DECLARE_EXCEPTION(DNBSimulationControl,DNBEInvalidRange,DNBException);
DNB_DECLARE_EXCEPTION(DNBSimulationControl,DNBEUnknownException,DNBException);
DNB_DECLARE_EXCEPTION(DNBSimulationControl,DNBESimletException,DNBException);
DNB_DECLARE_EXCEPTION(DNBSimulationControl,DNBEDone,DNBESimletException);
DNB_DECLARE_EXCEPTION(DNBSimulationControl,DNBEHalt,DNBESimletException);
DNB_DECLARE_EXCEPTION(DNBSimulationControl,DNBEStop,DNBESimletException);
DNB_DECLARE_EXCEPTION(DNBSimulationControl,DNBEBreak,DNBESimletException);

#endif // _INCLUDE_DNBSIMLETEXCEPTION
