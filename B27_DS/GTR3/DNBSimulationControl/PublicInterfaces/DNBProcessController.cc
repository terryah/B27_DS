//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
/** @CAA2Required */
/**********************************************************************/
/* DON T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPEAR AT ANY TIME */
/**********************************************************************/
//==============================================================================
/**
 * @fullreview HRN trangara 01:08:20
 *
 * @quickReview sha 02:11:04
 * @quickreview SHA 03:01:15
 * @quickreview SHA 04:04:30
 * @quickreview SHA 05:05:04
 */
// --------------------------------------------------------

//==============================================================================
//
// DNBProcessController.cc
//      Inline Function Implementation of DNBProcessController.h
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     rtl          08/25/2000   Initial Implementation
//     rtl          04/12/2002   Code change for implementing branching 
//                               and loop logic
//     rtl          12/11/2002   Provided overloaded getChildrenList method that 
//                               returns a list of iterators 
//     rtl          12/24/2002   Add new methods: populateMapDB and destroyGraph
//     rtl          05/04/2005   Move methods to .cpp file
//==============================================================================



//------------------------------------------------------------------------------
// Implements DNBProcessController::notify
//------------------------------------------------------------------------------
inline
void
DNBProcessController::notify(Proclet pc,Mtype message)
DNB_THROW_SPEC_ANY
{
    //LockGuard lock( monitor() );               
    DNBProcletMessage m(pc,message);
    messageQueue_.write(m);
    
}


//------------------------------------------------------------------------------
// Implements DNBProcessController::setDoneException
//------------------------------------------------------------------------------
inline
void
DNBProcessController::setDoneException()
DNB_THROW_SPEC_ANY
{
    forceDoneException_ = true;
}


//------------------------------------------------------------------------------
// Implements DNBProcessController::resetDoneException
//------------------------------------------------------------------------------
inline
void
DNBProcessController::resetDoneException()
DNB_THROW_SPEC_ANY
{
    forceDoneException_ = false;
}


//------------------------------------------------------------------------------
// Implements DNBProcessController::setProcletType
//------------------------------------------------------------------------------
inline
void
DNBProcessController::setProcletType(DNBProclet::ProcletType type)
DNB_THROW_SPEC_ANY
{
    procletType_ = type;
}


//------------------------------------------------------------------------------
// Implements DNBProcessController::setResourceName
//------------------------------------------------------------------------------
inline
void
DNBProcessController::setResourceName(scl_wstring resName)
DNB_THROW_SPEC_ANY
{
    resourceName_ = resName;
}


//------------------------------------------------------------------------------
// Implements DNBProcessController::setTaskNo
//------------------------------------------------------------------------------
inline
void
DNBProcessController::setTaskName(scl_wstring taskName)
DNB_THROW_SPEC_ANY
{
    taskName_ = taskName;
}


//------------------------------------------------------------------------------
// Implements DNBProcessController::getProcletType
//------------------------------------------------------------------------------
inline
DNBProclet::ProcletType
DNBProcessController::getProcletType()
DNB_THROW_SPEC_ANY
{
    return procletType_;
}


//------------------------------------------------------------------------------
// Implements DNBProcessController::getResourceName
//------------------------------------------------------------------------------
inline
scl_wstring 
DNBProcessController::getResourceName()
DNB_THROW_SPEC_ANY
{
    return resourceName_;
}



//------------------------------------------------------------------------------
// Implements DNBProcessController::getTaskNo
//------------------------------------------------------------------------------
inline
scl_wstring 
DNBProcessController::getTaskName()
DNB_THROW_SPEC_ANY
{
    return taskName_;
}


//------------------------------------------------------------------------------
// Implements DNBProcessController::populateMapDB
//------------------------------------------------------------------------------
inline
void
DNBProcessController::populateMapDB()
DNB_THROW_SPEC_ANY
{
    /*
     * This is called to populate the graph's map with <proclet , node_iterator>
     * pairs to be used during runtime
     */
    graph_.populateDB();
}


//------------------------------------------------------------------------------
// Implements DNBProcessController::destroyGraph
//------------------------------------------------------------------------------
inline
void
DNBProcessController::destroyGraph()
DNB_THROW_SPEC_ANY
{
    /*
     * Destroy the graph by deleting all the nodes and edges in the graph
     */

    graph_.destroyGraph();
}
