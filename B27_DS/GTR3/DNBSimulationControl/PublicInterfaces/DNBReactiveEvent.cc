/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview HRN trangara 01:08:20
 * @quickReview smw 05:05:12
 */
// --------------------------------------------------------


//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBReactiveEvent.cc
//      DNBReactiveEvent inline functions and templatized out-of-line functions
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     xxx          mm/dd/yyyy   Comment
//
//==============================================================================


//------------------------------------------------------------------------------
// Implements DNBReactiveEvent::body
//------------------------------------------------------------------------------
inline
DNBReactiveEvent::Body&
DNBReactiveEvent::body() const
DNB_THROW_SPEC_ANY
{
    return (DNBReactiveEvent::Body&) DNBSysHandleBase::body();
}




//
// --- Public out-of-line definitions ---
//

//------------------------------------------------------------------------------
// Implements DNBReactiveDataEventBody<VarT>::DNBReactiveDataEventBody
//------------------------------------------------------------------------------
template <class VarT>
DNBReactiveDataEventBody<VarT>::DNBReactiveDataEventBody( const VarT& value )
DNB_THROW_SPEC_ANY
: DNBReactiveEventBody(),
value_( value )
{
}

//------------------------------------------------------------------------------
// Implements DNBReactiveDataEventBody<VarT>::~DNBReactiveDataEventBody
//------------------------------------------------------------------------------
template <class VarT>
DNBReactiveDataEventBody<VarT>::~DNBReactiveDataEventBody()
DNB_THROW_SPEC_NULL
{
}


//------------------------------------------------------------------------------
// Implements DNBReactiveDataEventBody<VarT>::data
//------------------------------------------------------------------------------
template <class VarT>
const VarT&
DNBReactiveDataEventBody<VarT>::data() const
DNB_THROW_SPEC_ANY
{
    LockGuard lock( monitor() );
    return value_;
}




//------------------------------------------------------------------------------
// Implements DNBReactiveDataEvent<VarT>::DNBReactiveDataEvent
//------------------------------------------------------------------------------
template <class VarT>
DNBReactiveDataEvent<VarT>::DNBReactiveDataEvent()
DNB_THROW_SPEC_ANY
: DNBReactiveEvent( EmptyCtor )
{
}



//------------------------------------------------------------------------------
// Implements DNBReactiveDataEvent<VarT>::DNBReactiveDataEvent
//------------------------------------------------------------------------------
template <class VarT>
DNBReactiveDataEvent<VarT>::DNBReactiveDataEvent( const VarT& value )
DNB_THROW_SPEC_ANY
: DNBReactiveEvent( DNB_NEW DNBReactiveDataEventBody<VarT>( value ) )
{
}



//------------------------------------------------------------------------------
// Implements DNBReactiveDataEvent<VarT>::DNBReactiveDataEvent
//------------------------------------------------------------------------------
template <class VarT>
DNBReactiveDataEvent<VarT>::DNBReactiveDataEvent( const Self& copy )
DNB_THROW_SPEC_ANY
: DNBReactiveEvent( copy )
{
}



//------------------------------------------------------------------------------
// Implements DNBReactiveDataEvent<VarT>::~DNBReactiveDataEvent
//------------------------------------------------------------------------------
template <class VarT>
DNBReactiveDataEvent<VarT>::~DNBReactiveDataEvent()
DNB_THROW_SPEC_ANY
{
}



//------------------------------------------------------------------------------
// Implements DNBReactiveDataEvent<VarT>::operator=
//------------------------------------------------------------------------------
template <class VarT>
typename
DNBReactiveDataEvent<VarT>::Self&
DNBReactiveDataEvent<VarT>::operator=( const Self& rhs )
DNB_THROW_SPEC_ANY
{
    return (Self&) DNBReactiveEvent::operator=( rhs );
}



//------------------------------------------------------------------------------
// Implements DNBReactiveDataEvent<VarT>::data
//------------------------------------------------------------------------------
template <class VarT>
const VarT&
DNBReactiveDataEvent<VarT>::data() const
DNB_THROW_SPEC_ANY
{
    return body().data();
}



//------------------------------------------------------------------------------
// Implements DNBReactiveDataEvent<VarT>::body
//------------------------------------------------------------------------------
template <class VarT>
inline
typename
DNBReactiveDataEvent<VarT>::Body&
DNBReactiveDataEvent<VarT>::body() const
DNB_THROW_SPEC_ANY
{
    return (Body&) DNBReactiveEvent::body();
}
