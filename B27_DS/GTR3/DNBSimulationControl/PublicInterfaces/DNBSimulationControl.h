//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/** @CAA2Required */
/*---------------------------------------------------------------------*/
/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */
/*---------------------------------------------------------------------*/
//
//    This Header file is included by all the Module.h header files in 
//    PublicInterfaces
//

#ifndef DNBSIMULATIONCONTROL_H
#define DNBSIMULATIONCONTROL_H DNBSimulationControl

#ifdef _WINDOWS_SOURCE
#if defined(__DNBSimulationControl)
#define ExportedByDNBSimulationControl __declspec(dllexport)
#else
#define ExportedByDNBSimulationControl __declspec(dllimport)
#endif
#else
#define ExportedByDNBSimulationControl
#endif

#endif /* DNBSIMULATIONCONTROL_H */
