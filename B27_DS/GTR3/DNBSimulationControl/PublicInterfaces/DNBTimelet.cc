/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview HRN trangara 01:08:20
 */
// --------------------------------------------------------


//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBTimelet.cc
//      DNBTimelet inline function implementation file
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     xxx          mm/dd/yyyy   Comment
//     rtl          12/16/20002  Change setClock method to add/remove runstate
//                               handlers to/from the clock
//
//==============================================================================




//------------------------------------------------------------------------------
// Implements DNBTimelet::setClock
//------------------------------------------------------------------------------
inline
void
DNBTimelet::setClock( DNBTimeletClock* clock )
    DNB_THROW_SPEC_ANY
{
    LockGuard lock( monitor() );
    setClockTr( clock );
    if(clock)
        clock->addRunStateHandler( getRunStateHandler() );
    else
        clock->removeRunStateHandler( getRunStateHandler() );
}




//------------------------------------------------------------------------------
// Implements DNBTimelet::getClock
//------------------------------------------------------------------------------
inline
DNBTimeletClock*
DNBTimelet::getClock() const
    DNB_THROW_SPEC_ANY
{
    LockGuard lock( monitor() );
    return getClockTr();
}



//------------------------------------------------------------------------------
// Implements DNBTimelet::getClockTr
//------------------------------------------------------------------------------
inline
DNBTimeletClock*
DNBTimelet::getClockTr() const
    DNB_THROW_SPEC_ANY
{
    return clock_;
}





