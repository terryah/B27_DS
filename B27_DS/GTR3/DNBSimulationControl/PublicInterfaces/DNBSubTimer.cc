/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview HRN trangara 01:08:20
 */
// --------------------------------------------------------


//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBSubTimer.cc
//      DNBSubTimer  functions Implementation file
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     xxx          mm/dd/yyyy   Comment
//
//==============================================================================



//------------------------------------------------------------------------------
// Implements DNBSubTimerImpl::getStartTime
//------------------------------------------------------------------------------
inline
DNBSubTimerImpl::Time
DNBSubTimerImpl::getStartTime() const
DNB_THROW_SPEC_ANY
{
    return startTime_;
}



//------------------------------------------------------------------------------
// Implements DNBSubTimerImpl::setStartTime
//------------------------------------------------------------------------------
inline
void
DNBSubTimerImpl::setStartTime( const Time& time )
DNB_THROW_SPEC_ANY
{
    startTime_ = time;
}




//------------------------------------------------------------------------------
// Implements DNBSubTimerImpl::getEndTime
//------------------------------------------------------------------------------
inline
DNBSubTimerImpl::Time
DNBSubTimerImpl::getEndTime() const
DNB_THROW_SPEC_ANY
{
    return endTime_;
}




//------------------------------------------------------------------------------
// Implements DNBSubTimerImpl::setEndTime
//------------------------------------------------------------------------------
inline
void
DNBSubTimerImpl::setEndTime( const Time& time )
DNB_THROW_SPEC_ANY
{
    endTime_ = time;
}




//------------------------------------------------------------------------------
// Implements DNBSubTimer::operator=
//------------------------------------------------------------------------------
inline
DNBSubTimer&
DNBSubTimer::operator=( const Self& rhs )
DNB_THROW_SPEC_ANY
{
    return (Self&) DNBSysHandleBase::operator=( rhs );
}




//------------------------------------------------------------------------------
// Implements DNBSubTimer::getStartTime
//------------------------------------------------------------------------------
inline
DNBSubTimer::Time
DNBSubTimer::getStartTime() const
DNB_THROW_SPEC_ANY
{
    return body().getStartTime();
}




//------------------------------------------------------------------------------
// Implements DNBSubTimer::setStartTime
//------------------------------------------------------------------------------
inline
void
DNBSubTimer::setStartTime( const Time& time )
DNB_THROW_SPEC_ANY
{
    body().setStartTime( time );
}




//------------------------------------------------------------------------------
// Implements DNBSubTimer::getEndTime
//------------------------------------------------------------------------------
inline
DNBSubTimer::Time
DNBSubTimer::getEndTime() const
DNB_THROW_SPEC_ANY
{
    return body().getEndTime();
}




//------------------------------------------------------------------------------
// Implements DNBSubTimer::setEndTime
//------------------------------------------------------------------------------
inline
void
DNBSubTimer::setEndTime( const Time& time )
DNB_THROW_SPEC_ANY
{
    body().setEndTime( time );
}




//------------------------------------------------------------------------------
// Implements DNBSubTimer::body
//------------------------------------------------------------------------------
inline
DNBSubTimer::Impl&
DNBSubTimer::body() const
DNB_THROW_SPEC_ANY
{
    return (Impl&) DNBSysHandleBase::body();
}

