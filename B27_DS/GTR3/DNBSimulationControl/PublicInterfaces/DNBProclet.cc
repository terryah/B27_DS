/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview HRN trangara 01:08:20
 *
 * @quickReview sha 02:11:04
 * @quickreview SHA 03:01:15
 * @quickreview SHA 03:11:07
 * @quickreview SHA 04:10:13
 * @quickreview MMH 07:01:09
 */
// --------------------------------------------------------


//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBProclet.cc
//      DNBProclet inline functions implementation file
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     rtl          05/02/2002   Provide lock-free methods for getting/setting
//                               the controller
//     sha          05/09/2002   Clean up the debug traces
//     rtl          12/18/2002   Override setClock method
//     rtl          03/07/2003   IR A0383906 Fix. Provide method setStartTime()
//     rtl          07/14/2003   IR A0401597 . Provide a member to store the
//                               wait duration of the proclet and method to
//                               set the wait duration
//     rtl          11/04/2003   Add method to set Resource data and stopTime
//     rtl          10/13/2004   Added checks to ensure that scheduledStart_ and
//                               scheduledDuration_ are not negative
//     rtl          01/10/2007   Code cleanup. Remove activeBranch code
//     rtl          06/06/2007   A0586171 fix. Moved setProcessed() implementation 
//                               from .cc to .cpp 
//
//==============================================================================


//------------------------------------------------------------------------------
// Implements DNBProclet::getScheduledStart
//------------------------------------------------------------------------------
inline
DNBSimTime
DNBProclet::getScheduledStart() const
DNB_THROW_SPEC_ANY
{
    LockGuard lock( monitor() );
    return getScheduledStartTr();
}



//------------------------------------------------------------------------------
// Implements DNBProclet::setScheduledStart
//------------------------------------------------------------------------------
inline
void
DNBProclet::setScheduledStart( const Time& scheduledStart )
DNB_THROW_SPEC_ANY
{
    LockGuard lock( monitor() );

    DNB_ASSERT(scheduledStart >= DNBSimTime(0.0) );

    if(scheduledStart < DNBSimTime(0.0) )
    {
        DNB_TRACE_WARNING0(DNBSimGraphTrace,
                           " scheduledStart is a negative number. Setting scheduledStart_ to 0.0"
                           );

        setScheduledStartTr( DNBSimTime(0.0) );
    }
    else
        setScheduledStartTr( scheduledStart );
}

//------------------------------------------------------------------------------
// Implements DNBProclet::getScheduledDuration
//------------------------------------------------------------------------------
inline
DNBSimTime
DNBProclet::getScheduledDuration() const
DNB_THROW_SPEC_ANY
{
    LockGuard lock( monitor() );
    return getScheduledDurationTr();
}


//------------------------------------------------------------------------------
// Implements DNBProclet::setScheduledDuration
//------------------------------------------------------------------------------
inline
void
DNBProclet::setScheduledDuration( const Time& scheduledDuration )
DNB_THROW_SPEC_ANY
{
    LockGuard lock( monitor() );

    DNB_ASSERT(scheduledDuration >= DNBSimTime(0.0) );

    if(scheduledDuration < DNBSimTime(0.0) )
    {
        DNB_TRACE_WARNING0(DNBSimGraphTrace,
                           " scheduledDuration is a negative number. Setting scheduledDuration_to 0.0"
                           );

        setScheduledDurationTr( DNBSimTime(0.0) );
    }
    else
        setScheduledDurationTr( scheduledDuration );
}


//------------------------------------------------------------------------------
// Implements DNBProclet::getScheduledStartTr
//------------------------------------------------------------------------------
inline
DNBSimTime
DNBProclet::getScheduledStartTr() const
DNB_THROW_SPEC_ANY
{
    return scheduledStart_;
}


//------------------------------------------------------------------------------
// Implements DNBProclet::setScheduledStartTr
//------------------------------------------------------------------------------
inline
void
DNBProclet::setScheduledStartTr( const Time& scheduledStart )
DNB_THROW_SPEC_ANY
{
    scheduledStart_ = scheduledStart;
    beginTimer_.setAtTime( scheduledStart_ );
    updateTimer_.setStartTime( scheduledStart_ );
    
    if(isheartbeatEnabled())
        heartbeatTimer_.setBeginTime(scheduledStart_);
    
}


//------------------------------------------------------------------------------
// Implements DNBProclet::getScheduledDurationTr
//------------------------------------------------------------------------------
inline
DNBSimTime
DNBProclet::getScheduledDurationTr() const
DNB_THROW_SPEC_ANY
{
    if(waitDuration_ ==0.0)
        return scheduledDuration_;
    else
        return waitDuration_;

}


//------------------------------------------------------------------------------
// Implements DNBProclet::setScheduledDurationTr
//------------------------------------------------------------------------------
inline
void
DNBProclet::setScheduledDurationTr( const Time& scheduledDuration )
DNB_THROW_SPEC_ANY
{
    scheduledDuration_ = scheduledDuration;
    scheduledEnd_ = scheduledStart_ + scheduledDuration_;
    updateTimer_.setEndTime( scheduledEnd_ );
    
    if(isheartbeatEnabled())
        heartbeatTimer_.setEndTime( scheduledEnd_ );
    
    
}



//------------------------------------------------------------------------------
// Implements DNBProclet::getScheduledEnd
//------------------------------------------------------------------------------
inline
DNBSimTime
DNBProclet::getScheduledEnd() const
DNB_THROW_SPEC_ANY
{
    LockGuard lock( monitor() );
    return getScheduledEndTr();
}




//------------------------------------------------------------------------------
// Implements DNBProclet::getScheduledEndTr
//------------------------------------------------------------------------------
inline
DNBSimTime
DNBProclet::getScheduledEndTr() const
DNB_THROW_SPEC_ANY
{
    return scheduledEnd_;
}



//------------------------------------------------------------------------------
// Implements DNBProclet::setProcessController
//------------------------------------------------------------------------------
inline
void
DNBProclet::setProcessController(DNBProcessController *Controller) 
DNB_THROW_SPEC_ANY
{
    LockGuard lock( monitor() );
    setProcessControllerTr(Controller);
}


//------------------------------------------------------------------------------
// Implements DNBProclet::getProcessController
//------------------------------------------------------------------------------
inline
DNBProcessController*
DNBProclet::getProcessController()
DNB_THROW_SPEC_ANY
{
    LockGuard lock(monitor() );
    return getProcessControllerTr();
}


//------------------------------------------------------------------------------
// Implements DNBProclet::setProcessControllerTr
//------------------------------------------------------------------------------
inline
void
DNBProclet::setProcessControllerTr(DNBProcessController *Controller)
DNB_THROW_SPEC_ANY
{
    Controller_=Controller;
}


//------------------------------------------------------------------------------
// Implements DNBProclet::getProcessControllerTr
//------------------------------------------------------------------------------
inline
DNBProcessController*
DNBProclet::getProcessControllerTr()
DNB_THROW_SPEC_ANY
{
    return Controller_;
}

//------------------------------------------------------------------------------
// Implements DNBProclet::isControllerSet
//------------------------------------------------------------------------------
inline
bool
DNBProclet::isControllerSet()
DNB_THROW_SPEC_ANY
{
    return( (Controller_== NULL) ? false : true);
}

//------------------------------------------------------------------------------
// Implements DNBProclet::getBeginHandler
//------------------------------------------------------------------------------
inline
DNBTimeletClock::TimeHandler&
DNBProclet::getBeginHandler()
DNB_THROW_SPEC_ANY

{
    LockGuard lock( monitor() );
    return beginCron_;
}



//------------------------------------------------------------------------------
// Implements DNBProclet::getUpdateHandler
//------------------------------------------------------------------------------
inline
DNBTimeletClock::TimeHandler&
DNBProclet::getUpdateHandler()


{
    LockGuard lock( monitor() );
    return updateCron_;
}




//------------------------------------------------------------------------------
// Implements DNBProclet::getEndHandler
//------------------------------------------------------------------------------
inline
DNBTimeletClock::TimeHandler&
DNBProclet::getEndHandler()


{
    LockGuard lock( monitor() );
    return endCron_;
}



//------------------------------------------------------------------------------
// Implements DNBProclet::getBeginTimer
//------------------------------------------------------------------------------
inline
DNBAtTimer&
DNBProclet::getBeginTimer()


{
    LockGuard lock( monitor() );
    return beginTimer_;
}



//------------------------------------------------------------------------------
// Implements DNBProclet::getUpdateTimer
//------------------------------------------------------------------------------
inline
DNBSubTimer&
DNBProclet::getUpdateTimer()


{
    LockGuard lock( monitor() );
    return updateTimer_;
}



//------------------------------------------------------------------------------
// Implements DNBProclet::getHeartBeatTimer
//------------------------------------------------------------------------------
inline
DNBIntervalTimer& 
DNBProclet::getHeartBeatTimer()
DNB_THROW_SPEC_ANY
{
    LockGuard lock( monitor() );
    return heartbeatTimer_;
}


//------------------------------------------------------------------------------
// Implements DNBProclet::isheartbeatEnabled
//------------------------------------------------------------------------------
inline
bool
DNBProclet::isheartbeatEnabled()
DNB_THROW_SPEC_ANY
{
    return heartbeatTimer_.isEnabled();
}



//------------------------------------------------------------------------------
// Implements DNBProclet::setHeartBeatTimer
//------------------------------------------------------------------------------
inline
void
DNBProclet::setHeartBeatTimer(const Time& frequency)
DNB_THROW_SPEC_ANY
{
    LockGuard lock( monitor() );
    if(frequency > DNBTimeletClock::ZeroTime)
    {
        if(isheartbeatEnabled())
            heartbeatTimer_.setInterval(frequency);
        else
        {
            heartbeatTimer_ = DNBIntervalTimer(frequency, scheduledStart_, scheduledEnd_ );
            heartbeatTimer_.enable();
        }
    }
}



//------------------------------------------------------------------------------
// Implements DNBProclet::setHeartBeatRate
//------------------------------------------------------------------------------
inline
void
DNBProclet::setHeartBeatRate(const Time& newfrequency)
DNB_THROW_SPEC_ANY
{
    if(isheartbeatEnabled())
    {
        heartbeatTimer_.setInterval(newfrequency);
    }
}



//------------------------------------------------------------------------------
// Implements DNBProclet::shiftTimeBase
//------------------------------------------------------------------------------
inline
void
DNBProclet::shiftTimeBase(const Time& shift)
DNB_THROW_SPEC_ANY
{
    if(isheartbeatEnabled())
        heartbeatTimer_.setShift(shift);
}




//------------------------------------------------------------------------------
// Implements DNBProclet::isProcessed
//------------------------------------------------------------------------------
inline
bool
DNBProclet::isProcessed()
DNB_THROW_SPEC_NULL
{
    
    return processed_;
}


//------------------------------------------------------------------------------
// Implements DNBProclet::resetprevTime
//------------------------------------------------------------------------------
inline
void
DNBProclet::resetprevTime()
DNB_THROW_SPEC_NULL
{
    prevTime_=0.0;
}


//------------------------------------------------------------------------------
// Implements DNBProclet::setLoop
//------------------------------------------------------------------------------
inline
void
DNBProclet::setLoop(bool value)
DNB_THROW_SPEC_ANY
{
    LockGuard lock(monitor() );
    isLoop=value;
}



//------------------------------------------------------------------------------
// Implements DNBProclet::getLoop
//------------------------------------------------------------------------------
inline
bool
DNBProclet::getLoop()
DNB_THROW_SPEC_ANY
{
    LockGuard lock(monitor() );
    return isLoop;
}

//------------------------------------------------------------------------------
// Implements DNBProclet::setVisited
//------------------------------------------------------------------------------
inline
void
DNBProclet::setVisited(bool value)
DNB_THROW_SPEC_ANY
{
    visited=value;
}



//------------------------------------------------------------------------------
// Implements DNBProclet::getLoop
//------------------------------------------------------------------------------
inline
bool
DNBProclet::getVisited()
DNB_THROW_SPEC_ANY
{
    return visited;
}


//------------------------------------------------------------------------------
// Implements DNBProclet::setClock
//------------------------------------------------------------------------------
inline
void
DNBProclet::setClock( DNBTimeletClock* clock )
DNB_THROW_SPEC_ANY
{
    LockGuard lock( monitor() );
    setClockTr( clock );
}



//------------------------------------------------------------------------------
// Implements DNBProclet::setProcletType
//------------------------------------------------------------------------------
inline
void
DNBProclet::setProcletType(DNBProclet::ProcletType type )
DNB_THROW_SPEC_ANY
{
    procletType = type;
}




//------------------------------------------------------------------------------
// Implements DNBProclet::setResourceName
//------------------------------------------------------------------------------
inline
void
DNBProclet::setResourceName(scl_wstring rname)
DNB_THROW_SPEC_ANY
{
    resourceName = rname;
}


//------------------------------------------------------------------------------
// Implements DNBProclet::setTaskName
//------------------------------------------------------------------------------
inline
void
DNBProclet::setTaskName(scl_wstring tname )
DNB_THROW_SPEC_ANY
{
    taskName = tname;
}



//------------------------------------------------------------------------------
// Implements DNBProclet::getProcletType
//------------------------------------------------------------------------------
inline
DNBProclet::ProcletType
DNBProclet::getProcletType()
DNB_THROW_SPEC_ANY
{
    return procletType;
}




//------------------------------------------------------------------------------
// Implements DNBProclet::getResourceName
//------------------------------------------------------------------------------
inline
scl_wstring 
DNBProclet::getResourceName()
DNB_THROW_SPEC_ANY
{
    return resourceName;
}




//------------------------------------------------------------------------------
// Implements DNBProclet::getTaskName
//------------------------------------------------------------------------------
inline
scl_wstring 
DNBProclet::getTaskName()
DNB_THROW_SPEC_ANY
{
    return taskName;
}




//------------------------------------------------------------------------------
// Implements DNBProclet::setStartTime
//------------------------------------------------------------------------------
inline
void
DNBProclet::setStartTime(const Time& startTime)
DNB_THROW_SPEC_ANY
{
    presetStartTime_ = startTime;
}


//------------------------------------------------------------------------------
// Implements DNBProclet::getStopTime()
//------------------------------------------------------------------------------
inline
DNBSimTime
DNBProclet::getStopTime()
    DNB_THROW_SPEC_ANY
{
    return stopTime_;
}



//------------------------------------------------------------------------------
// Implements DNBProclet::setStopTime(Time &time)
//------------------------------------------------------------------------------
inline
void
DNBProclet::setStopTime(const Time &time)
    DNB_THROW_SPEC_ANY
{
    stopTime_ = time;
}



















