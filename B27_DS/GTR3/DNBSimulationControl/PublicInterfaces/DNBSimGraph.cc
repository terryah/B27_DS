//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2003
/** @CAA2Required */
/**********************************************************************/
/* DON T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPEAR AT ANY TIME */
/**********************************************************************/
//==============================================================================
/**
 * @fullreview SHA trangara 03:01:15
 * @quickReview SHA 03:03:26
 * @quickReview SHA 04:03:26
 * @quickReview SHA 05:05:05
 */

//==============================================================================
//
// DNBSimGraph.cc
//   Inline functions implementation file
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     rtl          01/14/2003   Initial Implementation
//     rtl          03/25/2003   Added method validateCycle() to detect whether
//                               there are cycles in the graph
//     rtl          08/13/2003   Fix MemoryCheck UMR Errors
//     rtl          03/26/2004   Provide utility methods for retrieving graph's 
//                               root and terminal count
//     rtl          05/05/2005   Added methods for getting a nodeCount between
//                               a source and a target node, assuming that they
//                               are linked
//     mmh          09/22/2008   Fix A0639117: prevent recursive calls for nodes 
//                               already visited
//
//==============================================================================


///////////////////////////////////////////////////////////////////////////////
//
// DNBGraphNode definitions
//
///////////////////////////////////////////////////////////////////////////////

template <class NodeData, class EdgeData>
DNBGraphNode<NodeData,EdgeData>::DNBGraphNode( NodeData &data )
    DNB_THROW_SPEC_NULL
    : data_( data )
{
    color = DNBGraphNode<NodeData,EdgeData>::initial;
    // Nothing
}

template <class NodeData, class EdgeData>
DNBGraphNode<NodeData,EdgeData>::~DNBGraphNode()
    DNB_THROW_SPEC_NULL
{
    // Nothing
}

///////////////////////////////////////////////////////////////////////////////
//
// DNBGraphEdge definitions
//
///////////////////////////////////////////////////////////////////////////////

template <class NodeData, class EdgeData>
DNBGraphEdge<NodeData,EdgeData>::DNBGraphEdge(
        DNBSimGraph<NodeData,EdgeData> *graph,
        const EdgeData &data,
        _DNBGraphNode source,
        _DNBGraphNode target )
    DNB_THROW_SPEC_NULL
    : data_( data ),
        source_( source ),
        target_( target ),
        graph_( graph )
{
    // Nothing
}

template <class NodeData, class EdgeData>
DNBGraphEdge<NodeData,EdgeData>::DNBGraphEdge( const self &rhs )
    DNB_THROW_SPEC_NULL
    : data_( rhs.data_ ),
        source_( rhs.source_ ),
        target_( rhs.target_ ),
        graph_( rhs.graph_ )
{
    // Nothing
}

template <class NodeData, class EdgeData>
DNBGraphEdge<NodeData,EdgeData>::~DNBGraphEdge()
    DNB_THROW_SPEC_NULL
{
    // Nothing
}



///////////////////////////////////////////////////////////////////////////////
//
// DNBGraphNodeIterator
//
///////////////////////////////////////////////////////////////////////////////

template <class NodeData, class EdgeData>
DNBGraphNodeIterator<NodeData,EdgeData>::DNBGraphNodeIterator()
    DNB_THROW_SPEC_NULL
    : map_( 0 ),
        node_( 0 )
{
    // Nothing
}

template <class NodeData, class EdgeData>
DNBGraphNodeIterator<NodeData,EdgeData>::DNBGraphNodeIterator( const node_iterator &rhs )
    DNB_THROW_SPEC_NULL
    : map_( rhs.map_ ),
        mapitr_( rhs.mapitr_ ),
        node_( rhs.node_ )
{
    // Nothing
}

template <class NodeData, class EdgeData>
DNBGraphNodeIterator<NodeData,EdgeData>::~DNBGraphNodeIterator()
    DNB_THROW_SPEC_NULL
{
    // Nothing
}

template <class NodeData, class EdgeData>
typename
DNBGraphNodeIterator<NodeData,EdgeData>::reference
DNBGraphNodeIterator<NodeData,EdgeData>::operator*()
    DNB_THROW_SPEC_NULL
{
    //NodeData tmp = (*mapitr_).first;
    //return tmp;
    node_ = (*mapitr_).second;
    return node_->data_;
    
}

template <class NodeData, class EdgeData>
DNBGraphNodeIterator<NodeData,EdgeData> &
DNBGraphNodeIterator<NodeData,EdgeData>::operator++()
    DNB_THROW_SPEC_NULL
{
    ++mapitr_;
    if(mapitr_ != map_->end() )
    {   
        node_ = (*mapitr_).second;
    }
    return *this;
}

template <class NodeData, class EdgeData>
DNBGraphNodeIterator<NodeData,EdgeData>
DNBGraphNodeIterator<NodeData,EdgeData>::operator++( int )
    DNB_THROW_SPEC_NULL
{
    node_iterator tmp = *this;

    ++mapitr_;
    if(mapitr_ != map_->end() )
    {   
        node_ = (*mapitr_).second;
    }

    return tmp;
}

template <class NodeData, class EdgeData>
DNBGraphNodeIterator<NodeData,EdgeData> &
DNBGraphNodeIterator<NodeData,EdgeData>::operator--()
    DNB_THROW_SPEC_NULL
{
    --mapitr_;
    if(mapitr_ != map_->end() )
    {   
        node_ = (*mapitr_).second;
    }
    return *this;
}

template <class NodeData, class EdgeData>
DNBGraphNodeIterator<NodeData,EdgeData>
DNBGraphNodeIterator<NodeData,EdgeData>::operator--( int )
    DNB_THROW_SPEC_NULL
{
    node_iterator tmp = *this;

    --mapitr_;
    if(mapitr_ != map_->end() )
    {   
        node_ = (*mapitr_).second;
    }

    return tmp;
}

template <class NodeData, class EdgeData>
bool
DNBGraphNodeIterator<NodeData,EdgeData>::operator==( const node_iterator &rhs ) const
    DNB_THROW_SPEC_NULL
{
    return mapitr_  == rhs.mapitr_;
    //return node_ == rhs.node_;
}

template <class NodeData, class EdgeData>
bool
DNBGraphNodeIterator<NodeData,EdgeData>::operator!=( const node_iterator &rhs ) const
    DNB_THROW_SPEC_NULL
{
    return mapitr_  != rhs.mapitr_;
    //return node_ == rhs.node_;
}

template <class NodeData, class EdgeData>
bool
DNBGraphNodeIterator<NodeData,EdgeData>::operator<( const node_iterator &rhs ) const
    DNB_THROW_SPEC_NULL
{
    return node_ < rhs.node_;
}

template <class NodeData, class EdgeData>
size_t
DNBGraphNodeIterator<NodeData,EdgeData>::inDegree() const
    DNB_THROW_SPEC_NULL
{
    return node_->inEdges_.size();
}

template <class NodeData, class EdgeData>
size_t
DNBGraphNodeIterator<NodeData,EdgeData>::outDegree() const
    DNB_THROW_SPEC_NULL
{
    return node_->outEdges_.size();
}

template <class NodeData, class EdgeData>
size_t
DNBGraphNodeIterator<NodeData,EdgeData>::degree() const
    DNB_THROW_SPEC_NULL
{
    return node_->inEdges_.size() + node_->outEdges_.size();
}

template <class NodeData, class EdgeData>
bool
DNBGraphNodeIterator<NodeData,EdgeData>::isRoot() const
    DNB_THROW_SPEC_NULL
{
    return node_->inEdges_.empty();
}

template <class NodeData, class EdgeData>
bool
DNBGraphNodeIterator<NodeData,EdgeData>::isTerminal() const
    DNB_THROW_SPEC_NULL
{
    return node_->outEdges_.empty();
}

template <class NodeData, class EdgeData>
typename
DNBGraphNodeIterator<NodeData,EdgeData>::edge_iterator
DNBGraphNodeIterator<NodeData,EdgeData>::begin_in_edge()
    DNB_THROW_SPEC_NULL
{
    edge_iterator iei;

    iei.list_ = &node_->inEdges_;
    iei.itr_ = node_->inEdges_.begin();
    if(iei.itr_ != node_->inEdges_.end() )
    {
        iei.edge_ = *iei.itr_;
    }

    return iei;
}


template <class NodeData, class EdgeData>
typename
DNBGraphNodeIterator<NodeData,EdgeData>::edge_iterator
DNBGraphNodeIterator<NodeData,EdgeData>::end_in_edge()
    DNB_THROW_SPEC_NULL
{
    edge_iterator iei;

    iei.list_ = &node_->inEdges_;
    iei.itr_ = node_->inEdges_.end();
    //iei.edge_ = *iei.itr_;

    return iei;
}


template <class NodeData, class EdgeData>
typename
DNBGraphNodeIterator<NodeData,EdgeData>::edge_iterator
DNBGraphNodeIterator<NodeData,EdgeData>::begin_out_edge()
    DNB_THROW_SPEC_NULL
{
    edge_iterator iei;

    iei.list_ = &node_->outEdges_;
    iei.itr_ = node_->outEdges_.begin();
    if(iei.itr_ != node_->outEdges_.end() )
    {
        iei.edge_ = *iei.itr_;
    }

    return iei;
}



template <class NodeData, class EdgeData>
typename
DNBGraphNodeIterator<NodeData,EdgeData>::edge_iterator
DNBGraphNodeIterator<NodeData,EdgeData>::end_out_edge()
    DNB_THROW_SPEC_NULL
{
    edge_iterator iei;

    iei.list_ = &node_->outEdges_;
    iei.itr_ = node_->outEdges_.end();
    //iei.edge_ = *iei.itr_;

    return iei;
}





///////////////////////////////////////////////////////////////////////////////
//
// DNBGraphEdgeIterator
//
///////////////////////////////////////////////////////////////////////////////

template <class NodeData, class EdgeData>
DNBGraphEdgeIterator<NodeData,EdgeData>::DNBGraphEdgeIterator()
    DNB_THROW_SPEC_NULL
    : list_( 0 ),
        edge_( 0 )
{
    // Nothing
}

template <class NodeData, class EdgeData>
DNBGraphEdgeIterator<NodeData,EdgeData>::DNBGraphEdgeIterator( const edge_iterator &rhs )
    DNB_THROW_SPEC_NULL
    : list_( rhs.list_ ),
        itr_( rhs.itr_ ),
        edge_( rhs.edge_ )
{
    // Nothing
}

template <class NodeData, class EdgeData>
DNBGraphEdgeIterator<NodeData,EdgeData>::~DNBGraphEdgeIterator()
    DNB_THROW_SPEC_NULL
{
    // Nothing
}

template <class NodeData, class EdgeData>
typename
DNBGraphEdgeIterator<NodeData,EdgeData>::reference
DNBGraphEdgeIterator<NodeData,EdgeData>::operator *() const
    DNB_THROW_SPEC_NULL
{
    return (*itr_)->data_;
}

template <class NodeData, class EdgeData>
DNBGraphEdgeIterator<NodeData,EdgeData> &
DNBGraphEdgeIterator<NodeData,EdgeData>::operator++()
    DNB_THROW_SPEC_NULL
{
    ++itr_;
    if(itr_ != list_->end() )
    {
        edge_ = *itr_;
    }

    return *this;
}

template <class NodeData, class EdgeData>
DNBGraphEdgeIterator<NodeData,EdgeData>
DNBGraphEdgeIterator<NodeData,EdgeData>::operator++( int )
    DNB_THROW_SPEC_NULL
{
    edge_iterator tmp = *this;
    ++itr_;
    if(itr_ != list_->end() )
    {
        edge_ = *itr_;
    }

    return tmp;
}

template <class NodeData, class EdgeData>
DNBGraphEdgeIterator<NodeData,EdgeData> &
DNBGraphEdgeIterator<NodeData,EdgeData>::operator--()
    DNB_THROW_SPEC_NULL
{
    --itr_;
    if(itr_ != list_->end() )
    {
        edge_ = *itr_;
    }

    return *this;
}

template <class NodeData, class EdgeData>
DNBGraphEdgeIterator<NodeData,EdgeData>
DNBGraphEdgeIterator<NodeData,EdgeData>::operator--( int )
    DNB_THROW_SPEC_NULL
{
    edge_iterator tmp = *this;
    --itr_;
    if(itr_ != list_->end() )
    {
        edge_ = *itr_;
    }

    return tmp;
}

template <class NodeData, class EdgeData>
bool
DNBGraphEdgeIterator<NodeData,EdgeData>::operator==( const edge_iterator &rhs ) const
    DNB_THROW_SPEC_NULL
{
    return itr_  == rhs.itr_;
    //return edge_ == rhs.edge_;
}


template <class NodeData, class EdgeData>
bool
DNBGraphEdgeIterator<NodeData,EdgeData>::operator!=( const edge_iterator &rhs ) const
    DNB_THROW_SPEC_NULL
{
    return itr_  != rhs.itr_;
    //return edge_ == rhs.edge_;
}

template <class NodeData, class EdgeData>
bool
DNBGraphEdgeIterator<NodeData,EdgeData>::operator<( const edge_iterator &rhs ) const
    DNB_THROW_SPEC_NULL
{
    return edge_ < rhs.edge_;
}

template <class NodeData, class EdgeData>
typename
DNBGraphEdgeIterator<NodeData,EdgeData>::node_iterator
DNBGraphEdgeIterator<NodeData,EdgeData>::getSource() const
    DNB_THROW_SPEC_NULL
{
    DNBSimGraph<NodeData,EdgeData> *g = edge_->graph_;
    node_iterator ni;

    ni.map_ = &g->nodesMap;

    ni.mapitr_ = g->nodesMap.find(edge_->source_->data_);

    ni.node_ = (*ni.mapitr_).second;

    return ni;
}

template <class NodeData, class EdgeData>
typename
DNBGraphEdgeIterator<NodeData,EdgeData>::node_iterator
DNBGraphEdgeIterator<NodeData,EdgeData>::getTarget() const
    DNB_THROW_SPEC_NULL
{
    DNBSimGraph<NodeData,EdgeData> *g = edge_->graph_;
    node_iterator ni;

    ni.map_ = &g->nodesMap;

    ni.mapitr_ = g->nodesMap.find(edge_->target_->data_);

    ni.node_ = (*ni.mapitr_).second;

    return ni;
}




///////////////////////////////////////////////////////////////////////////////
//
// DNBSimGraph definitions
//
///////////////////////////////////////////////////////////////////////////////

template <class NodeData, class EdgeData>
DNBSimGraph<NodeData,EdgeData>::DNBSimGraph()
DNB_THROW_SPEC_NULL
{

    // not really needed since we have the traits hardcoded to this
    // constructor, but is a little safer if we change ValidateTraits
    // and this constructor is not changed.
    
}

template <class NodeData, class EdgeData>
DNBSimGraph<NodeData,EdgeData>::~DNBSimGraph()
    DNB_THROW_SPEC_NULL
{
    destroyGraph();
}


template <class NodeData, class EdgeData>
size_t
DNBSimGraph<NodeData,EdgeData>::nodeCount() const
    DNB_THROW_SPEC_NULL
{
    return nodesMap.size();
}

template <class NodeData, class EdgeData>
size_t
DNBSimGraph<NodeData,EdgeData>::edgeCount() const
    DNB_THROW_SPEC_NULL
{
    return edges_.size();
}

template <class NodeData, class EdgeData>
size_t
DNBSimGraph<NodeData,EdgeData>::rootCount() const
    DNB_THROW_SPEC_NULL
{
    return rootsMap.size();
}

template <class NodeData, class EdgeData>
size_t
DNBSimGraph<NodeData,EdgeData>::terminalCount() const
    DNB_THROW_SPEC_NULL
{
    return terminalsMap.size();
}

template <class NodeData, class EdgeData>
bool
DNBSimGraph<NodeData,EdgeData>::isEmpty() const
    DNB_THROW_SPEC_NULL
{
    return nodesMap.empty();
}



template <class NodeData, class EdgeData>
void
DNBSimGraph<NodeData,EdgeData>::populateDB()
DNB_THROW_SPEC_ANY
{
    /*
     * Check to see if the map is not empty. 
     * If it is not empty then it is safe to assume 
     * that the map has already been populated with values
     * If it is empty, then we populate it with values now.
     */
     //Nothing

}


template <class NodeData, class EdgeData>
bool
DNBSimGraph<NodeData,EdgeData>::findDB(NodeData data,node_iterator& itr)
DNB_THROW_SPEC_ANY
{
    node_iterator ni;
    ni.map_ = &nodesMap;
    ni.mapitr_ = nodesMap.find(data);
    if(ni.mapitr_ !=nodesMap.end() )
    {
        ni.node_ = (*ni.mapitr_).second;
        itr = ni;
        return true;
    }
    else
        return false;
}



template <class NodeData, class EdgeData>
typename
DNBSimGraph<NodeData,EdgeData>::node_iterator
DNBSimGraph<NodeData,EdgeData>::addNode( NodeData &nd )
    DNB_THROW_SPEC((scl_bad_alloc))
{
    _DNBGraphNode tmp = DNB_NEW DNBGraphNode<NodeData,EdgeData>( nd );

    node_iterator ni;
    ni.map_ = &nodesMap;
    ni.node_ = tmp;

    MapDBValueType entry(ni.node_->data_,ni.node_);
    scl_pair <MapDBIterator,bool> result = nodesMap.insert(entry);
    if(result.second)
        ni.mapitr_ = result.first;
    /*
    else
        cout<<"Not inserted in nodesMap"<<endl;
    */

    result = rootsMap.insert(entry);
    /*
    if(!result.second)
        cout<<"Not inserted in rootsMap"<<endl;
    */


    result = terminalsMap.insert(entry);
    /*
    if(!result.second)
        cout<<"Not inserted in terminalsMap"<<endl;
    */

    return ni;
    
}

template <class NodeData, class EdgeData>
void
DNBSimGraph<NodeData,EdgeData>::removeNode( node_iterator ni )
    DNB_THROW_SPEC_NULL
{
    typename DNBSimGraph<NodeData,EdgeData>::MapDBIterator itr;

    // If it has no in-edges, its a root and remove it from the roots.
    // Otherwise, get rid of its in-edges, and then remove it from the roots.
    while( ni.inDegree() > 0 )
        removeEdge( ni.begin_in_edge() );

    /*
    if(!rootsMap.erase(ni.node_->data_) )
        cout<<"From removeNode : Not removed from rootsMap"<<endl;
    */
    rootsMap.erase(ni.node_->data_);

    // If it has no out-edges, its a terminal and remove it from the terminals.
    // Otherwise, get rid of its out-edges, and then remove it from the terminals
    while( ni.outDegree() > 0 )
        removeEdge( ni.begin_out_edge() );

    /*
    if(!terminalsMap.erase(ni.node_->data_) )
        cout<<"From removeNode : Not removed from terminalsMap"<<endl;
    */
    terminalsMap.erase(ni.node_->data_);

    /*
    if(!nodesMap.erase(ni.node_->data_) )
        cout<<"From removeNode : Not removed from nodesMap"<<endl;
    */
    nodesMap.erase(ni.node_->data_);

    DNB_DELETE ni.node_;
}

template <class NodeData, class EdgeData>
typename
DNBSimGraph<NodeData,EdgeData>::edge_iterator
DNBSimGraph<NodeData,EdgeData>::addEdge( EdgeData &ed,
                                        node_iterator &source,
                                        node_iterator &target )
    DNB_THROW_SPEC((scl_bad_alloc))
{
    _DNBGraphEdge tmp =
        DNB_NEW DNBGraphEdge<NodeData,EdgeData> ( this, ed, (*source.mapitr_).second, (*target.mapitr_).second );

    edge_iterator ei;
    ei.list_ = &edges_;
    ei.edge_ = tmp;
    edges_.push_back( tmp );
    ei.itr_ = edges_.end();
    --ei.itr_;

    _DNBGraphNode node = (*source.mapitr_).second;

    DNBGraphNode<NodeData,EdgeData> *impl = node;
    impl->outEdges_.push_back( tmp );

    node = (*target.mapitr_).second;
    impl = node;
    impl->inEdges_.push_back( tmp );

    // Remove the node from roots_ and terminals
    typename DNBSimGraph<NodeData,EdgeData>::MapDBIterator itr;


    /*
     if(!rootsMap.erase(target.node_->data_) )
         cout<<"From addEdge : Not removed from rootsMap"<<endl;
    */
    rootsMap.erase(target.node_->data_);

    /*
     if(!terminalsMap.erase(source.node_->data_) )
         cout<<"From addEdge : Not removed from terminalsMap"<<endl;
    */
    terminalsMap.erase(source.node_->data_);


    return ei;
}

template <class NodeData, class EdgeData>
void
DNBSimGraph<NodeData,EdgeData>::removeEdge( edge_iterator ei )
    DNB_THROW_SPEC_NULL
{
    // remove edge from source.outEdges_
    typename scl_list<_DNBGraphEdge,DNB_ALLOCATOR(_DNBGraphEdge) >::iterator itr;
    _DNBGraphNode source = ei.edge_->source_;
    itr = scl_find( source->outEdges_.begin(), source->outEdges_.end(), ei.edge_ );
    source->outEdges_.erase( itr );

    node_iterator ni;
    //if outDegree of the source is 0 then add it to the terminal node list
    if(source->outEdges_.size() == 0 )
    {
        MapDBValueType entry(source->data_,source);
        scl_pair <MapDBIterator,bool> result = terminalsMap.insert(entry);
        if(result.second)
        {
            ni.mapitr_ = result.first;
            //cout<<"From removeEdge : Inserted to terminalsMap"<<endl;
        }
        /*
        else
            cout<<"Not inserted in terminalsMap"<<endl;
        */
    }


    // remove edge from target.inEdges_
    _DNBGraphNode target = ei.edge_->target_;
    itr = scl_find( target->inEdges_.begin(), target->inEdges_.end(), ei.edge_ );
    target->inEdges_.erase( itr );

    //if inDegree of the target is 0 then add it to the root node list
    if(target->inEdges_.size() == 0 )
    {
        MapDBValueType entry(target->data_,target);
        scl_pair <MapDBIterator,bool> result = rootsMap.insert(entry);
        if(result.second)
        {
            ni.mapitr_ = result.first;
            //cout<<"From removeEdge : Inserted to rootsMap"<<endl;
        }
        /*
        else
            cout<<"Not inserted in nodesMap"<<endl;
        */
    }

    // remove edge from edges_
    typename scl_list<_DNBGraphEdge,DNB_ALLOCATOR(_DNBGraphEdge) >::iterator litr;
    litr = scl_find( edges_.begin(), edges_.end(), ei.edge_ );

    // Go ahead and remove it
    edges_.erase( litr );

    DNB_DELETE ei.edge_;
}

template <class NodeData, class EdgeData>
void
DNBSimGraph<NodeData,EdgeData>::clear()
  DNB_THROW_SPEC_NULL
{
    nodesMap.erase( nodesMap.begin(), nodesMap.end() );
    rootsMap.erase( rootsMap.begin(), rootsMap.end() );
    terminalsMap.erase( terminalsMap.begin(), terminalsMap.end() );
    edges_.erase( edges_.begin(), edges_.end() );
}


template<class NodeData, class EdgeData>
void
DNBSimGraph<NodeData,EdgeData>::destroyGraph()
    DNB_THROW_SPEC_NULL
{
    for(edge_iterator ei = begin_edge();ei!= end_edge(); ei++)
    {
        DNB_DELETE ei.edge_;
        ei.edge_=NULL;
    }

    for(node_iterator ni = begin_node();ni!= end_node(); ni++)
    {
        DNB_DELETE ni.node_;
        ni.node_=NULL;
    }

    clear();
}


template <class NodeData, class EdgeData>
bool
DNBSimGraph<NodeData,EdgeData>::validateCycle()
DNB_THROW_SPEC_ANY
{

	typedef DNBGraphNodeIterator<NodeData,EdgeData> node_iterator;
    
	for(node_iterator ni1 = begin_node(); ni1!= end_node(); ni1++)
	{
		ni1.node_->color = DNBGraphNode<NodeData,EdgeData>::white;
	}


	for(node_iterator ni2 = begin_node(); ni2 != end_node(); ni2++)
	{
		if(ni2.node_->color == DNBGraphNode<NodeData,EdgeData>::white)
		{	
			if( !DFS_Visit( ni2 ) )
				return false;
		}
	}
	return true;
}

template <class NodeData, class EdgeData>
bool
DNBSimGraph<NodeData,EdgeData>::DFS_Visit(node_iterator &ni)
DNB_THROW_SPEC_ANY
{
	ni.node_->color = DNBGraphNode<NodeData,EdgeData>::grey;

	for( edge_iterator ei = ni.begin_out_edge();ei!=ni.end_out_edge();ei++ )
	{
		node_iterator nai = ei.getTarget();
		if(nai.node_->color == DNBGraphNode<NodeData,EdgeData>::grey)
			return false;
		if(nai.node_->color == DNBGraphNode<NodeData,EdgeData>::white)
		{
			if(! DFS_Visit(nai) )
				return false;
		}
		nai.node_->color = DNBGraphNode<NodeData,EdgeData>::black;
	}
	return true;
}



template <class NodeData, class EdgeData>
typename
DNBSimGraph<NodeData,EdgeData>::node_iterator
DNBSimGraph<NodeData,EdgeData>::begin_node()
    DNB_THROW_SPEC_NULL
{
    node_iterator itr;

    itr.map_ = &nodesMap;
    itr.mapitr_ = nodesMap.begin();

    if(itr.mapitr_ != nodesMap.end() )
    {
        itr.node_ = (*itr.mapitr_).second;
    }

    return itr;
}


template <class NodeData, class EdgeData>
typename
DNBSimGraph<NodeData,EdgeData>::node_iterator
DNBSimGraph<NodeData,EdgeData>::end_node()
    DNB_THROW_SPEC_NULL
{
    node_iterator itr;

    itr.map_ = &nodesMap;
    itr.mapitr_ = nodesMap.end();
    //itr.node_ = (*itr.mapitr_).second;

    return itr;
}


template <class NodeData, class EdgeData>
typename
DNBSimGraph<NodeData,EdgeData>::edge_iterator
DNBSimGraph<NodeData,EdgeData>::begin_edge()
    DNB_THROW_SPEC_NULL
{
    edge_iterator itr;

    itr.list_ = &edges_;
    itr.itr_ = edges_.begin();
    if(itr.itr_ != edges_.end() )
    {
        itr.edge_ = *itr.itr_;
    }

    return itr;
}

template <class NodeData, class EdgeData>
typename
DNBSimGraph<NodeData,EdgeData>::edge_iterator
DNBSimGraph<NodeData,EdgeData>::end_edge()
    DNB_THROW_SPEC_NULL
{
    edge_iterator itr;

    itr.list_ = &edges_;
    itr.itr_ = edges_.end();
    //itr.edge_ = *itr.itr_;

    return itr;
}


template <class NodeData, class EdgeData>
typename
DNBSimGraph<NodeData,EdgeData>::node_iterator
DNBSimGraph<NodeData,EdgeData>::begin_root()
    DNB_THROW_SPEC_NULL
{
    node_iterator itr;

    itr.map_ = &rootsMap;
    itr.mapitr_ = rootsMap.begin();
    if(itr.mapitr_ != rootsMap.end() )
    {
        itr.node_ = (*itr.mapitr_).second;
    }

    return itr;
}

template <class NodeData, class EdgeData>
typename
DNBSimGraph<NodeData,EdgeData>::node_iterator
DNBSimGraph<NodeData,EdgeData>::end_root()
    DNB_THROW_SPEC_NULL
{
    node_iterator itr;

    itr.map_ = &rootsMap;
    itr.mapitr_ = rootsMap.end();
    //itr.node_ = (*itr.mapitr_).second;

    return itr;
}


template <class NodeData, class EdgeData>
typename
DNBSimGraph<NodeData,EdgeData>::node_iterator
DNBSimGraph<NodeData,EdgeData>::begin_terminal()
    DNB_THROW_SPEC_NULL
{
    node_iterator itr;

    itr.map_ = &terminalsMap;
    itr.mapitr_ = terminalsMap.begin();
    if(itr.mapitr_ != terminalsMap.end() )
    {
        itr.node_ = (*itr.mapitr_).second;
    }

    return itr;

}


template <class NodeData, class EdgeData>
typename
DNBSimGraph<NodeData,EdgeData>::node_iterator
DNBSimGraph<NodeData,EdgeData>::end_terminal()
    DNB_THROW_SPEC_NULL
{
    node_iterator itr;

    itr.map_ = &terminalsMap;
    itr.mapitr_ = terminalsMap.end();
    //itr.node_ = (*itr.mapitr_).second;

    return itr;
}

template <class NodeData, class EdgeData>
void
DNBSimGraph<NodeData,EdgeData>::resetColor(node_iterator& sourceNode, node_iterator& targetNode)
DNB_THROW_SPEC_ANY
{
    
    if( sourceNode.node_->color == DNBGraphNode<NodeData,EdgeData>::white )
    {   // node and its successors were visited already
        return;
    }
    if(sourceNode == targetNode)
    {   // target node was reached
        sourceNode.node_->color = DNBGraphNode<NodeData,EdgeData>::white;
        return;
    }

    for (edge_iterator edge=sourceNode.begin_out_edge();edge!=sourceNode.end_out_edge();edge++)
    {
        node_iterator nextNode = edge.getTarget();
        resetColor(nextNode, targetNode);
    }
    sourceNode.node_->color = DNBGraphNode<NodeData,EdgeData>::white;
}


template <class NodeData, class EdgeData>
void
DNBSimGraph<NodeData,EdgeData>::nodeCount(node_iterator& sourceNode, node_iterator& targetNode, int& iNodeCount)
DNB_THROW_SPEC_ANY
{
    if(sourceNode.node_->color != DNBGraphNode<NodeData,EdgeData>::grey )
    {
        iNodeCount++;
    }
    else
    {   // node and its successors were visited already
        return;
    }
    if(sourceNode == targetNode)
    {   // target node was reached
        sourceNode.node_->color = DNBGraphNode<NodeData,EdgeData>::grey;
        return;
    }

    for (edge_iterator edge=sourceNode.begin_out_edge();edge!=sourceNode.end_out_edge();edge++)
    {
        node_iterator nextNode = edge.getTarget();
        nodeCount(nextNode, targetNode, iNodeCount);
    }
    sourceNode.node_->color = DNBGraphNode<NodeData,EdgeData>::grey;
}
