/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBSubTimer.h
//      Sub Timer implementation class -   Used for participatating
//      in clock advancement and time-based functor dispatching.
//  
//      DNBSubTimer proposes a time based on the time passed on to it.
//
//==============================================================================
//
// Usage notes: 
//      Usually tied to the Time based Simlets(proclets). The time-based Simlets
//      register their timers with the Clock.  The timers participate in Clock
//      advancement and time-based functor dispatching. 
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     chris        01/01/1999   Initial Development
//
//==============================================================================
#if DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _INCLUDE_DNBSUBTIMER
#define _INCLUDE_DNBSUBTIMER

/**
 * Standard includes
 */
#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>

/**
 * Deneb includes.
 */
#include <DNBSimulationControl.h>
#include <DNBTimeletTimer.h>

class ExportedByDNBSimulationControl DNBSubTimerImpl : public DNBTimeletTimerImpl
{
public:
    
    /**
     * I always do this.
     */
    typedef DNBSubTimerImpl                             Self;
    
    /**
     * Sub time constructor.
     */
    DNBSubTimerImpl( const Time& startTime,
    const Time& endTime )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Copy constructor.
     */
    DNBSubTimerImpl( const Self& copy )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Destructor.
     */
    virtual
    ~DNBSubTimerImpl()
        DNB_THROW_SPEC_NULL;
    
    /**
     * Expired if the supplied time is greater than the 'at' time.
     */
    bool
    expired( const Time& current )
        DNB_THROW_SPEC_ANY;
    
    /**
     * If the timer has not expired, this simply proposes the single at time.
     */
    Time
    propose( const Time& minTime, const Time& maxTime )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Included only if the supplied time is the same as the 'at' time.
     */
    bool
    contains( const Time& minTime, const Time& maxTime )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Return the start time.
     */
    inline
    Time
    getStartTime() const
        DNB_THROW_SPEC_ANY;
    
    /**
     * Set the start time.
     */
    inline
    void
    setStartTime( const Time& time )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Return the start time.
     */
    inline
    Time
    getEndTime() const
        DNB_THROW_SPEC_ANY;
    
    /**
     * Set the start time.
     */
    inline
    void
    setEndTime( const Time& time )
        DNB_THROW_SPEC_ANY;
    
private:
    
    Time           startTime_;
    Time           endTime_;
};

/**
 * The 'at' timer interface.  For now, changing the 'at' time is disallowed
 * because I need COW semantics and I don't have time to properly implement
 * them.  TBD.
 */
class ExportedByDNBSimulationControl DNBSubTimer : public DNBTimeletTimer
{
public:
    
    /**
     * I always do this.
     */
    typedef DNBSubTimer                                       Self;
    
    /**
     * I always do this.
     */
    typedef DNBSubTimerImpl                                   Impl;
    
    /**
     * Default constructor creates an at time of 0.0.
     */
    DNBSubTimer()
        DNB_THROW_SPEC_ANY;
    
    /**
     * Time constructor creates with the supplied at time.
     */
    DNBSubTimer( const Time&  startTime,
        const Time&  endTime )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Copy constructor performs shallow copy.
     */
    DNBSubTimer( const Self& copy )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Destructor.
     */
    virtual
    ~DNBSubTimer()
        DNB_THROW_SPEC_ANY;
    
    /**
     * Assignment operator.
     */
    inline
    Self&
    operator=( const Self& rhs )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Get the start time.
     */
    inline
    Time
    getStartTime() const
        DNB_THROW_SPEC_ANY;
    
    /**
     * Set the start time.
     */
    inline
    void
    setStartTime( const Time& timer )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Get the end time.
     */
    inline
    Time
    getEndTime() const
        DNB_THROW_SPEC_ANY;
    
    /**
     * Set the end time.
     */
    inline
    void
    setEndTime( const Time& timer )
        DNB_THROW_SPEC_ANY;
    
private:
    
    /**
     * Convenience to get at the implementation.
     */
    inline
    Impl&
    body() const
        DNB_THROW_SPEC_ANY;
};

#include <DNBSubTimer.cc>

#endif // _INCLUDE_DNBSUBTIMER
