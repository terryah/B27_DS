/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview HRN trangara 01:08:20
 */
// --------------------------------------------------------


//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBReactiveVar.cc
//      Brief description...
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     xxx          mm/dd/yyyy   Comment
//
//==============================================================================


#include <scl_algorithm.h>
//------------------------------------------------------------------------------
// Implements DNBReactiveVar<EventT>::operator=
//------------------------------------------------------------------------------
template <class EventT>
DNBReactiveVar<EventT>&
DNBReactiveVar<EventT>::operator=( const Self& rhs )
DNB_THROW_SPEC_ANY
{
    LockGuard lock( monitor() );
    if( *this != rhs )
    {
        this->handlers_ = rhs.handlers_;
    }
    return *this;
}



//------------------------------------------------------------------------------
// Implements DNBReactiveVar<EventT>::addHandler
//------------------------------------------------------------------------------
template <class EventT>
void
DNBReactiveVar<EventT>::addHandler( const Handler& handler )
DNB_THROW_SPEC_ANY
{
    LockGuard lock( monitor() );
    typename HandlerList::iterator ii=scl_find(handlers_.begin(),handlers_.end(),handler);
    if(ii==handlers_.end())
        handlers_.push_back( handler );
}




//------------------------------------------------------------------------------
// Implements DNBReactiveVar<EventT>::removeHandler
//------------------------------------------------------------------------------
template <class EventT>
void
DNBReactiveVar<EventT>::removeHandler( const Handler& handler )
DNB_THROW_SPEC_ANY
{
    LockGuard lock( monitor() );
    size_t lsize = handlers_.size();
    handlers_.remove( handler );
    //    DNB_ASSERT( lsize != handlers_.size() );
}



//
// --- Out-of-line template definitions ---
//



//------------------------------------------------------------------------------
// Implements DNBReactiveVar<EvnT>::DNBReactiveVar
//------------------------------------------------------------------------------
template <class EvnT>
DNBReactiveVar<EvnT>::DNBReactiveVar()
DNB_THROW_SPEC_ANY
//: RWMonitor<RWMutexLock>( ),
: DNBSysMonitor( ),
handlers_()
{
}



//------------------------------------------------------------------------------
// Implements DNBReactiveVar<EvnT>::DNBReactiveVar
//------------------------------------------------------------------------------
template <class EvnT>
DNBReactiveVar<EvnT>::DNBReactiveVar( const Self& copy )
DNB_THROW_SPEC_ANY
: handlers_( copy.handlers_ )
{
}



//------------------------------------------------------------------------------
// Implements DNBReactiveVar<EvnT>::~DNBReactiveVar
//------------------------------------------------------------------------------
template <class EvnT>
DNBReactiveVar<EvnT>::~DNBReactiveVar()
DNB_THROW_SPEC_ANY
{
}



//------------------------------------------------------------------------------
// Implements DNBReactiveVar<EvnT>::notify
//------------------------------------------------------------------------------
template <class EvnT>
void
DNBReactiveVar<EvnT>::notify( EvnT& event )
DNB_THROW_SPEC_ANY
{
    LockGuard lock( monitor() );
    
    if( !handlers_.empty() )
    {
        for( typename HandlerList::iterator ii = handlers_.begin();
        ii != handlers_.end();
        ++ii )
        {
            //
            // Go ahead and dispatch the handler to the kernel for execution
            // in the thread pool.  It is the kernel's job to execute stuff in
            // priority order which is currently not implemented.
            //
            
            (*ii).dispatch( event );
        }
    }
}



//------------------------------------------------------------------------------
// Implements DNBReactiveVar<EvnT>::operator==
//------------------------------------------------------------------------------
template <class EvnT>
bool
DNBReactiveVar<EvnT>::operator==( const Self &right ) const
DNB_THROW_SPEC_ANY
{
    return ( handlers_ == right.handlers_ );
}
