/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBProcletMessage.h
//      Message Mechanism. Used for communication between proclet and the 
//      Process Controller(Inter-Class Communication)
//
//==============================================================================
//
// Usage notes: 
//      Each proclet sends messages to the Process Controller when their begin,
//      update, end methods are called. The Controller maintains a messagebox
//      where it checks for messages for scheduling and processing the proclets
//      
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     rtl          08/25/2000   Initial Development
//     rtl          05/21/2001   Added messages PostEndHalt, PostEndDone for
//                               for managing breakpoints
//     rtl          08/25/2006   Support for simulation time point determination for 
//                               resources with behaviour
//
//==============================================================================
#if DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _INCLUDE_DNBMessage	
#define _INCLUDE_DNBMessage

#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>

/**
 *Forward Declaration to avoid circular dependency
 */
class DNBProclet;


class DNBProcletMessage
{
    
public:
    
    
    typedef DNBProcletMessage       Self;
    
    typedef DNBProclet *Proclet;
    
    enum MessageType 
    {
        None,
        PreBeginHalt,
        PreBeginDone,
        BeginProcessed,
        BreakBeginHalt,
        BreakBeginDone,
		BeginNotReady,

        UpdateProcessed,
        BreakUpdateHalt,
        BreakUpdateDone,

        EndProcessed,
        BreakEndHalt,
        BreakEndDone,
        PostEndHalt,
        PostEndDone
    };
    
    DNBProcletMessage()
        DNB_THROW_SPEC_ANY;
    
    /**
     *Constructor
     */
    DNBProcletMessage(Proclet sender, MessageType message)
        DNB_THROW_SPEC_ANY;
    
    /**
     *Destructor
     */
    ~DNBProcletMessage()
        DNB_THROW_SPEC_ANY;
    
    /**
     *Function to set the Sender of the message and the message itself.
     */
    inline
    void
    set(Proclet sender,MessageType message)
        DNB_THROW_SPEC_ANY;
    
    /**
     *Function to retrieve the Sender of the message
     */
    inline
    Proclet
    getSender()
        DNB_THROW_SPEC_ANY;


    /**
     *Retrieve the message
     */
    inline
    MessageType 
    getMessage()
        DNB_THROW_SPEC_ANY;
    
    inline
    bool
    operator==( const Self &right ) const
        DNB_THROW_SPEC_ANY;
    
    inline 
    bool
    operator<(const Self &right ) const
        DNB_THROW_SPEC_ANY;
    
    
private:
    
    Proclet     sender_;
    MessageType message_;
};

#include "DNBProcletMessage.cc"

#endif      //_INCLUDE_DNBMessage
