/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBReactiveEvent.h
//      The DNBReactiveEvent class is the basis for defining events which
//      are queued and then dispatched within reactives as the primary means of
//      inter-reactive communications.
//
//==============================================================================
//
// Usage notes: 
//      Brief usage explanation...
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     chris        01/01/1999   Initial Development
//     rtl          03/25/2003   Fix MLK Error. provide member ("raisedExceptions_")
//                               to collect all the raised exceptions and delete
//                               them in the destructor 
//     rtl          05/20/2005   Change the way exceptions are handled and 
//                               raised from the event
//
//==============================================================================
#if DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _INCLUDE_DNBREACTIVEEVENT
#define _INCLUDE_DNBREACTIVEEVENT

/**
 * includes.
 */
#include <DNBSystemBase.h>
#include <scl_list.h>
#include <DNBSysMonitor.h>
#include <DNBSysCondition.h>
// #include <rw/thr/except.h>
#include <DNBSystemDefs.h>

#include <DNBSysHandleBase.h>
#include <DNBSysBodyBase.h>

/**
 * Deneb includes.
 */
#include <DNBSimulationControl.h>
#include <DNBException.h>
#include <DNBSimTrace.h>

/**
 * The DNBReactiveEventBody class is the basis for defining events which
 * are queued and then dispatched within reactives as the primary means of
 * inter-reactive communications.
 */
class ExportedByDNBSimulationControl DNBReactiveEventBody : public DNBSysBodyBase
{
public:
    
    /**
     * I always do this.
     */
    typedef DNBReactiveEventBody                        Self;
    
    /**
     * The default constructor.
     */
    DNBReactiveEventBody()
        DNB_THROW_SPEC_ANY;
    
    /**
     * Destructor.
     */
    virtual
    ~DNBReactiveEventBody()
        DNB_THROW_SPEC_NULL;
    
    /**
     * Called whenever the event is referenced, i.e. another reactive uses
     * it.  This causes the waitCount to be incremented.  The wait() function
     * won't return until all clients who have queued have also signaled.
     */
    void
    bind()
        DNB_THROW_SPEC_ANY;
    
    /**
     * Receiver must signal that the event has been processed - OK.
     */
    void
    signal()
        DNB_THROW_SPEC_ANY;
    
    /**
     * Receiver can queue exception to be rethrown by the sender.
     */
    /*
    void
    signal( RWTHRxmsg* ex )
        DNB_THROW_SPEC_ANY;
    */
    
    /**
     * Sender can raise any exceptions that were thrown by a receiver.
     */
    void
    raise()
        DNB_THROW_SPEC_ANY;
    
    /**
     * Wait for the event to clear.  This call will block until the
     * wait count reaches zero and the event variable is signalled.
     * Caveat: a call may be dispatched to a number of reactives which
     * may actually finish before others receive the call in their queues.
     * Thus, wait should only be called after all have received the event.
     */
    void
    wait()
        DNB_THROW_SPEC_ANY;

    void
    insertExceptionData(const scl_string &EType, 
						const DNBMsgData::StringType &formatStr,
						const char *srcFile,
						int Line  )
        DNB_THROW_SPEC_ANY;
    
private:
    
    /**
     * Opaquify the container used to store exceptions.
     */
    
    struct ExceptionData
    {
        scl_string				type_;
        DNBMsgData::StringType	formatStr_;
        const char *					srcFile_;
		int								srcLine_;

        ExceptionData(scl_string type = " ", DNBMsgData::StringType formatStr = DNB_TEXT(" "),
					  const char *srcFile = " ", int srcLine = 0 )
            :type_(type),formatStr_(formatStr), srcFile_(srcFile),srcLine_(srcLine)
        {}
        ExceptionData( const ExceptionData &rhs)
			:type_(rhs.type_),formatStr_(rhs.formatStr_),
			 srcFile_(rhs.srcFile_),srcLine_(rhs.srcLine_)
        {}

    };

  
    /**
     * Instance data.
     */
    size_t				count_;
    //RWCondition         done_;
    DNBSysCondition         done_;
	ExceptionData       exception_;
	bool                isExceptionData_;
    
    /**
     * Undefined - for now.
     */
    DNBReactiveEventBody( const Self& copy );
    Self& operator=( const Self& rhs );
};

    /**
 * The DNBReactiveEvent class is a handle to DNBReactiveEventBody
 * and provides the handle semantics including reference counting the
 * data.  This is especially important for event management because there
 * needs to be a reliable way to know when it's safe to delete the event.
 */
class ExportedByDNBSimulationControl DNBReactiveEvent : public DNBSysHandleBase
{
public:
    
    /**
     * I always do this.
     */
    typedef DNBReactiveEvent                            Self;
    
    /**
     * Shorthand the implementation.
     */
    typedef DNBReactiveEventBody                        Body;
    
    /**
     * We need a special constructor for derived classes to call that won't
     * bomb out the body.
     */
    enum SpecialCtor
    {
        EmptyCtor
    };
    
    /**
     * The default constructor - makes it's own implementation.
     */
    DNBReactiveEvent()
        DNB_THROW_SPEC_ANY;
    
    /**
     * The state constructor - does not make an implementation.
     */
    DNBReactiveEvent( const SpecialCtor& makeEmpty )
        DNB_THROW_SPEC_ANY;
    
    /**
     * The implementation constructor - this is the means by which most
     * events will be constructed, i.e. with data.
     */
    DNBReactiveEvent( Body* data )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Copy constructor - creates a shallow copy of the event which should
     * not be a problem as event data is const.
     */
    DNBReactiveEvent( const Self& copy )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Destructor.
     */
    virtual
    ~DNBReactiveEvent()
        DNB_THROW_SPEC_ANY;
    
    /**
     * Assignment operator - does shallow assignment which like the copy
     * constructor should not be a problem because the data is const. If I
     * ever find the need to do event enrichment, I will implement COW
     * semantics on the event infrastructure.
     */
    Self&
    operator=( const Self& rhs )
        DNB_THROW_SPEC_ANY;
    
    /**
     * See DNBReactiveEventBody::reference
     */
    void
    bind()
        DNB_THROW_SPEC_ANY;
    
    /**
     * See DNBReactiveEventBody::signal
     */
    void
    signal()
        DNB_THROW_SPEC_ANY;
    
    /**
     * See DNBReactiveEventBody::signal
     */
    /*
    void
    signal( RWTHRxmsg* ex )
        DNB_THROW_SPEC_ANY;
    */
    
    /**
     * See DNBReactiveEventBody::raise
     */
    void
    raise()
        DNB_THROW_SPEC_ANY;
    
    /**
     * Wait for the event to clear.
     */
    void
    wait()
        DNB_THROW_SPEC_ANY;

    void
		insertExceptionData(const scl_string &EType,
							const DNBMsgData::StringType &formatStr,
							const char *srcFile,
							int Line  )
        DNB_THROW_SPEC_ANY;
    
protected:
    
    /**
     * Convenience function which returns a reference to the
     * DNBReactiveEventBody implementation - beware the empty pointer.
     */
    inline
    Body&
    body() const
        DNB_THROW_SPEC_ANY;
};

/**
 * --- Template class body that adds one piece of data to the event ---
 */
template <class VarT>
class DNBReactiveDataEventBody : public DNBReactiveEventBody
{
public:
    
    /**
     * Constructor.
     */
    DNBReactiveDataEventBody( const VarT& value )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Destructor.
     */
    ~DNBReactiveDataEventBody()
        DNB_THROW_SPEC_NULL;
    
    /**
     * Value accessor - returns a const reference to avoid an unnecessary copy.
     */
    const VarT&
    data() const
        DNB_THROW_SPEC_ANY;
    
private:
    
    /**
     * Instance data.
     */
    VarT      value_;
};

template <class VarT>
class DNBReactiveDataEvent : public DNBReactiveEvent
{
public:
    
    /**
     * I always do this.
     */
    typedef DNBReactiveDataEvent<VarT>                  Self;
    
    /**
     * Shorthand the implementation.
     */
    typedef DNBReactiveDataEventBody<VarT>              Body;
    
    /**
     * The default constructor - makes it's own implementation.
     */
    DNBReactiveDataEvent()
        DNB_THROW_SPEC_ANY;
    
    /**
     * The implementation constructor - this is the means by which most
     * events will be constructed, i.e. with data.
     */
    DNBReactiveDataEvent( const VarT& value )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Copy constructor - creates a shallow copy of the event which should
     * not be a problem as event data is const.
     */
    DNBReactiveDataEvent( const Self& copy )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Destructor.
     */
    virtual
    ~DNBReactiveDataEvent()
        DNB_THROW_SPEC_ANY;
    
    /**
     * Assignment operator - does shallow assignment which like the copy
     * constructor should not be a problem because the data is const. If I
     * ever find the need to do event enrichment, I will implement COW
     * semantics on the event infrastructure.
     */
    Self&
    operator=( const Self& rhs )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Value accessor.
     */
    const VarT&
    data() const
        DNB_THROW_SPEC_ANY;
    
protected:
    
    /**
     * Convenience function to return the body.
     */
    inline
    Body&
    body() const
        DNB_THROW_SPEC_ANY;
};

/**
 * Pull in the public inline definition file.
 */
#include <DNBReactiveEvent.cc>

#endif // _INCLUDE_DNBREACTIVEEVENT
