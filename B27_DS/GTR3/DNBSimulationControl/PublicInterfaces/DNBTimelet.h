/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBTimelet.h
//      A time-sensitive simulation object. 
//      It may be either "time-constrained" or "time-regulating". 
//      A time-regulating timelet instructs the clock to pass through
//      the time ticks of its interest. //
//==============================================================================
//
// Usage notes: 
//      
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     chris        01/01/1999   Initial Development
//     rtl          10/24/2002   Provide RTTI facility
//     rtl          12/16/20002  Change setClock method to add/remove runstate
//                               handlers to/from the clock 
//                               Make setClock method virtual
//     rtl          04/15/2003   Change RTTI macro to the one with no ctor
//     rtl          03/10/2004   Remove unused RWTools headers
//
//==============================================================================
#if DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _INCLUDE_DNBTIMELET
#define _INCLUDE_DNBTIMELET

#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>

/**
 * Deneb includes
 */
#include <DNBSimulationControl.h>
#include <DNBSimlet.h>
#include <DNBTimeletClock.h>
// #include <DNBAtTimer.h>          // smw; caa
#include <DNBForTimer.h>
// #include <DNBIntervalTimer.h>    // smw; caa

/**
 * The DNBTimelet interface
 */
class ExportedByDNBSimulationControl DNBTimelet : public DNBSimlet
{

    DNB_DECLARE_SIMLET_NOCTR(DNBTimelet);
public:
    
    /**
     * I always do this.
     */
    typedef DNBTimelet                                       Self;
    
    /**
     * Pull in the definition of time.
     */
    typedef DNBTimeletClock::Time                            Time;
    
    /**
     * Pull in all of the event types and handlers.
     */
    typedef DNBTimeletClock::TimeEventData                   TimeEventData;
    typedef DNBTimeletClock::TimeEvent                       TimeEvent;
    typedef DNBTimeletClock::TimeHandler                     TimeHandler;
    typedef DNBTimeletClock::UpdateEventData                 UpdateEventData;
    typedef DNBTimeletClock::UpdateEvent                     UpdateEvent;
    typedef DNBTimeletClock::UpdateHandler                   UpdateHandler;
    typedef DNBTimeletClock::RunStateEventData               RunStateEventData;
    typedef DNBTimeletClock::RunStateEvent                   RunStateEvent;
    typedef DNBTimeletClock::RunStateHandler                 RunStateHandler;
    
    /**
     * Pull in the timer and filter types for time events.
     */
    typedef DNBTimeletClock::TimeFilter                      TimeFilter;
    typedef DNBTimeletClock::Timer                           Timer;


    /**
     * Define the container in which to cache timers asscoiated with this
     * timelet.  We need this because when we remove this timelet from the
     * clock, we also need to remove all of the handlers and timers.
     */
    typedef scl_list<Timer,DNB_ALLOCATOR(Timer) >                TimerList;
    typedef scl_list<TimeHandler,DNB_ALLOCATOR(TimeHandler) >    HandlerList;
    
    /**
     * Destructor.
     */
    ~DNBTimelet()
        DNB_THROW_SPEC_ANY;
    
    /**
     * Set the timelet master (clock) controlling instance.
     */
    inline
    virtual
    void
    setClock( DNBTimeletClock* clock )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Get the timelet master (clock) controlling instance.
     */
    inline
    DNBTimeletClock*
    getClock() const
        DNB_THROW_SPEC_ANY;
    
    
protected:
    
    void
    removeAllHandlers( DNBTimeletClock* clock )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Constructor.
     */
    DNBTimelet( DNBTimeletClock* clock = 0 )
        DNB_THROW_SPEC_ANY;

    
    /**
     * Set the timelet master (clock) controlling instance.
     */
    void
    setClockTr( DNBTimeletClock* clock )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Get the timelet master (clock) controlling instance.
     */
    inline
    DNBTimeletClock*
    getClockTr() const
        DNB_THROW_SPEC_ANY;
    
    /**
     * Add a timelet.
     */
    void
    addTimeHandlerTr( const TimeHandler& handler )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Remove a timelet.
     */
    void
    removeTimeHandlerTr( const TimeHandler& handler )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Add a timelet.
     */
    void
    addTimerTr( const Timer& timer )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Remove a timelet.
     */
    void
    removeTimerTr( const Timer& timer )
        DNB_THROW_SPEC_ANY;
    
    
    
    /**
     * Register a time-based conditional functor.
     */
    
    void
    scheduleTr( const Timer& timer,
    const TimeHandler& handler )
        DNB_THROW_SPEC_ANY;
    
    
    /**
     * Cancel a time-based conditional functor.
     */
    void
    cancelTr( const Timer& timer,
    const TimeHandler& handler )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Class method which can be used as a filter function for a cron.  The
     * 'Always' method constructs a filter that will cause the cron to be
     * triggered at every time step.
     */
    static
    TimeFilter
    Always()
        DNB_THROW_SPEC_ANY;
    
    /**
     * Class method which can be used as a filter function for a cron.  The
     * 'OnBeat' method constructs a filter that will cause the cron to be
     * triggered on the natural beat of it's corresponding timer.
     */
    static
    TimeFilter
    OnTime( const Timer& timer )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Class method which can be used as a filter function for a cron.  The
     * 'OffBeat' method constructs a filter that will cause the cron to be
     * triggered for every time step except the natural beat of it's
     * corresponding timer.
     */
    static
    TimeFilter
    OffTime( const Timer& timer )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Convenience function to construct a cron timer that is interested only
     * in a specific time.  The timer is the part of the cron mechanism which
     * determines which times to register with the clock.
     */
    static
    Timer
    At( const Time& time )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Convenience function to construct a cron timer that is interested only
     * in a sample of times.  The timer is the part of the cron mechanism which
     * determines which times to register with the clock.
     */
    static
    Timer
    Foreach( const DNBForTimer::TimeVector& sample )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Convenience function to construct a cron timer that is interested only
     * in a sample of times.  The timer is the part of the cron mechanism which
     * determines which times to register with the clock.
     */
    static
    Timer
    Interval( const Time& intervalTime = 1.0,
            const Time& beginTime    = DNBTimeletClock::MinTime,
            const Time& endTime      = DNBTimeletClock::MaxTime )
        DNB_THROW_SPEC_ANY;
    
private:
   
    /**
     * Helpers that deal with timer and handler registration.
     */
    void
    addAllTimers( DNBTimeletClock* clock )
        DNB_THROW_SPEC_ANY;
    
    void
    removeAllTimers( DNBTimeletClock* clock )
        DNB_THROW_SPEC_ANY;
    
    void
    addAllHandlers( DNBTimeletClock* clock )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Called to initialize the timelet when a run is initiated
     */
    void
    timeHandler( const TimeEvent& evTime )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Member attributes
     */
    DNBTimeletClock*      clock_;
    TimeHandler           timeHandler_;
    TimerList             timers_;
    HandlerList           timeHandlers_;
    
    /**
     * Undefined - for now.
     */
    DNBTimelet( const Self& copy );
    Self& operator=( const Self& rhs );
};

/**
 * Snarf the public inline definition file.
 */
#include <DNBTimelet.cc>

#endif // _INCLUDE_DNBTIMELET
