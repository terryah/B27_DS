/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBTimeletTimer.h
//      The simulation timer implementation class - this class specifies the
//      interface for timers that wish to participate in clock advancement and
//      time-based functor dispatching.

//
//==============================================================================
//
// Usage notes: 
//      Brief usage explanation...
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     chris        01/01/1999   Initial Development
//     mmg          03/06/2003   made enable() virtual
//
//==============================================================================
#if DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _INCLUDE_DNBTIMELETTIMER
#define _INCLUDE_DNBTIMELETTIMER

/**
 * Rogue Wave includes
 */
#include <DNBSystemBase.h>
//#include <rw/thr/handbody.h>
#include <DNBSystemDefs.h>

/**
 * Deneb includes
 */
#include <DNBSimulationControl.h>
#include <DNBSimTime.h>

#include <DNBSysHandleBase.h>
#include <DNBSysBodyBase.h>

/**
 * The simulation timer implementation class - this class specifies the
 * interface for timers that wish to participate in clock advancement and
 * time-based functor dispatching.
 */
class ExportedByDNBSimulationControl DNBTimeletTimerImpl : public DNBSysBodyBase
{
public:
    
    /**
     * I always do this.
     */
    typedef DNBTimeletTimerImpl             Self;
    
    /**
     * Shorten the type name of time.
     */
    typedef DNBSimTime                      Time;
    
    /**
     * Destructor.
     */
    virtual 
    ~DNBTimeletTimerImpl()
        DNB_THROW_SPEC_NULL;
    
    /**
     * Has the timer expired relative to the supplied time?
     */
    virtual
    bool
    expired( const Time& time )
        DNB_THROW_SPEC_ANY = 0;
    
    /**
     * Suggest a decreasing time value that is smaller than maxTime but larger
     */ 
    virtual
    Time
    propose( const Time& minTime, const Time& maxTime )
        DNB_THROW_SPEC_ANY = 0;
    
    /**
     * Is the supplied time within the specified range?  This can be used by
     * filters to determine whether a timer is responsible for the current
     * time so the handler knows whether to fire or not.
     */
    virtual
    bool
    contains( const Time& minTime, const Time& maxTime )
        DNB_THROW_SPEC_ANY = 0;
    
    /**
     * Enable the timer.
     */
    virtual
    void
    enable()
        DNB_THROW_SPEC_ANY;
    
    /**
     * Disable the timer.
     */
    inline
    void
    disable()
        DNB_THROW_SPEC_ANY;
    
    /**
     * Check the enabled state.
     */
    inline
    bool
    isEnabled()
        DNB_THROW_SPEC_ANY;
    
protected:
    
    /**
     * Constructor - protected because this is an ABC.
     */
    DNBTimeletTimerImpl()
        DNB_THROW_SPEC_ANY;

private:
    
    /**
     * Instance data.
     */
    bool         enabled_;
};	

class ExportedByDNBSimulationControl DNBTimeletTimer : public DNBSysHandleBase
{
public:
    
    /**
     * I always do this.
     */
    typedef DNBTimeletTimer       Self;
    
    /**
     * I always do this.
     */
    typedef DNBTimeletTimerImpl   Impl;
    
    /**
     * Shorten the type name of time.
     */
    typedef Impl::Time                 Time;
    
    /**
     * Default constructor.
     */
    DNBTimeletTimer()
        DNB_THROW_SPEC_ANY;
    
    /**
     * Implementation constructor.
     */
    DNBTimeletTimer( Impl* impl )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Copy constructor.
     */
    DNBTimeletTimer( const Self& copy )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Destructor.
     */
    virtual
    ~DNBTimeletTimer()
        DNB_THROW_SPEC_ANY;
    
    /**
     * Assignment operator
     */
    inline
    Self&
    operator=( const Self& rhs )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Has the timer expired?
     */
    inline
    bool
    expired( const Time& current ) const
        DNB_THROW_SPEC_ANY;
    
    /**
     * Suggest a decreasing time value that is larger than the specified time.
     */ 
    inline
    Time
    propose( const Time& minTime, const Time& maxTime )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Does the supplied time match the timer.
     */
    inline
    bool
    contains( const Time& minTime, const Time& maxTime ) const
        DNB_THROW_SPEC_ANY;
    
    /**
     * Enable the timer.
     */
    inline
    void
    enable()
        DNB_THROW_SPEC_ANY;
    
    /**
     * Disable the timer.
     */
    inline
    void
    disable()
        DNB_THROW_SPEC_ANY;
    
    /**
     * Check the enabled state.
     */
    inline
    bool
    isEnabled()
        DNB_THROW_SPEC_ANY;
    
protected:
    
    /**
     * Convenience function to get the implementation.
     */
    inline
    Impl&
    body() const
        DNB_THROW_SPEC_ANY;
};

#include <DNBTimeletTimer.cc>

#endif // _INCLUDE_DNBTIMELETTIMER
