/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//==============================================================================
//
// DNBSimletController.h
//
//      The DNBSimletController class provides an implementation of a master
//      controller which drives the simulation by communicating with a bunch of
//      slaved step agents to coordinate them.
//

//
//==============================================================================
//
// Usage notes: 
//      Brief usage explanation...
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//     chris        01/01/1999   Initial Development
//     rtl          01/03/2001   Included breaktoRunning flag to  aid in 
//                               reproposing the same time step 
//     rtl          05/21/2001   Included a new controller state RunBreak, and 
//                               functions to transition from and to the new
//                               state
//     rtl          03/10/2004   Remove unused RWTools headers
//     smw          04/08/2005   Removed use of RWPCValQueue, RWCondition, RWMutex, 
//								 RWMonitor and RWTHRxmsg
//     smw          04/13/2005   Replaced RWThreadFunction with DNBSysThread
//
//==============================================================================

#define USE_RW_PCQUEUE  0		// temporarily for smoother migration
#define USE_RW_THREAD   0       // remove once everything is OK

#define HAS_DNB_CYCLECB 1
#define HAS_DNB_PHASECB 1

#if DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _INCLUDE_DNBSIMLETCONTROLLER
#define _INCLUDE_DNBSIMLETCONTROLLER

/**
 * Standard includes.
 */
#include <DNBSystemBase.h>
#include <DNBSysCondition.h>
#include <DNBSysMonitor.h>
#include <DNBSysMutexLock.h>
#include <DNBSysPCValQueue.h>
#include <DNBSysThread.h>
#include <DNBSystemDefs.h>

/**
 * Deneb includes
 */
#include <DNBSimulationControl.h>
#include <DNBBitUtils.h>
#include <DNBSimStep.h>
#include <DNBReactiveEvent.h>
#include <DNBReactiveHandler.h>
#include <DNBReactiveVar.h>


/**
 * The DNBStepController class provides an implementation of a master
 * controller which drives the simulation by communicating with a bunch of
 * slaved step agents to coordinate them.
 */
class ExportedByDNBSimulationControl DNBSimletController : public DNBSysMonitor
{
public:
    
    /**
     * I always do this.
     */
    typedef DNBSimletController                          Self;
    
    /**
     * Enumeration which describes the control state the controller can be
     * currently in.  The first few are explicitely assigned the value they
     * would have implicitely received anyway because I want to make it very
     * clear that the finite state machine wants these values.  Do not change
     * the relative ordering unless you plan on changing the FSM as well.
     */
    enum RunState
    {
        RunNull,        //0
        RunIdle,        //1
        RunRunning,     //2
        RunHalted,      //3
        RunStepping,    //4
        RunDone,        //5
        RunJoinable,    //6
        RunBreak        //7
    };

    
#ifdef LATER
    /**
     * Set up a bitmask to deal with more than one state at a time.
     */
    typedef unsigned int                                 RunStateMask;
    
    enum 
    {
        mskNull      = DNB_BIT( RunStateMask, RunNull ),
        mskIdle      = DNB_BIT( RunStateMask, RunIdle ),
        mskRunning   = DNB_BIT( RunStateMask, RunRunning ),
        mskHalted    = DNB_BIT( RunStateMask, RunHalted ),
        mskStepping  = DNB_BIT( RunStateMask, RunStepping ),
        mskDone      = DNB_BIT( RunStateMask, RunDone ),
        mskJoinable  = DNB_BIT( RunStateMask, RunJoinable )
        mskJoinable  = DNB_BIT( RunStateMask, RunBreak)
    };
#endif
    
    struct RunStateEventData
    {
        Self*           sender;
        RunState        previous;
        RunState        state;
    };
    
    typedef DNBReactiveDataEvent<RunStateEventData>      RunStateEvent;
    typedef DNBReactiveHandler<RunStateEvent>            RunStateHandler;
    
    /**
     * Define the step type that this controller uses along with some special
     * values which define the operating range of the type.
     */
    typedef DNBSimStep                                   Step;
    
    enum
    {
        MinStep       = DNB_STEP_MIN,      // the lowest possible step.
        MaxStep       = DNB_STEP_MAX - 1,  // the highest possible step.
        NullStep      = DNB_STEP_MAX       // the step which doesn't exist.
    };
    
    /**
     * Define the update event data whcih will be used to drive synchronized
     * updating throughout the system.
     */
    struct UpdateEventData
    {
        Self*           sender; // Who generated the event.
        Step            step;   // The step id of the update.
        RunState        state;  // Supplied in case the client needs it.
    };
    
    typedef DNBReactiveDataEvent<UpdateEventData>        UpdateEvent;
    typedef DNBReactiveHandler<UpdateEvent>              UpdateHandler;
    
    /**
     * Define different phases at which to dispatch callbacks relative to the
     * simulation update.  I'd like to deep six this in favor of update event
     * prioritization, but this would take a far more sophisticated kernel than
     * I have now implemented so for now it stays.
     */
    enum UpdatePhase
    {
        PreUpdate,
        PostUpdate
    };
    
    /**
     * Define the callback type with which to do some pre or post-processing
     * of an update.
     */
    typedef DNBFunctor2<const UpdateEventData&,const UpdatePhase&> PhaseCallback;
     
    typedef PhaseCallback *                                    PhaseCallbackF;
    
    /**
     * Define the callback type with which to do some pre or post-processing
     * of an update.
     */
    struct CycleCallbackData
    {
        Self*           sender;
        RunState        state;
        Step            step;
    };
    
    typedef DNBFunctor1<const CycleCallbackData&>         CycleCallback;

    typedef CycleCallback *                               CycleCallbackF ;
    
    /**
     * Constructor
     */
    DNBSimletController()
        DNB_THROW_SPEC_ANY;
    
    /**
     * Destructor
     */
    ~DNBSimletController()
        DNB_THROW_SPEC_ANY;
    
    /**
     * Add a phase callback.
     */
    inline
    void
    addPhaseCallback( const UpdatePhase& phase,
    const PhaseCallback& callback )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Remove a phase callback.
     */
    inline
    void
    removePhaseCallback( const UpdatePhase& phase,
    const PhaseCallback& callback )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Add a cycle callback.
     */
    inline
    void
    addCycleCallback( const CycleCallback& callback )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Remove a cycle callback.
     */
    inline
    void
    removeCycleCallback( const CycleCallback& callback )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Get the current step.
     */
    inline
    Step
    getStep() const
        DNB_THROW_SPEC_ANY;
    
    /**
     * Get the current run state.
     */
    inline
    RunState
    getRunState()
        DNB_THROW_SPEC_ANY;
    
    /**
     * Add an event handler that responds to run state events.
     */
    inline
    void
    addRunStateHandler( const RunStateHandler& handler )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Remove an event handler that responds to run state events.
     */
    inline
    void
    removeRunStateHandler( const RunStateHandler& handler )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Add an event handler that responds to update events.
     */
    inline
    void
    addUpdateHandler( const UpdateHandler& handler )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Remove an event handler that responds to update events.
     */
    inline
    void
    removeUpdateHandler( const UpdateHandler& handler )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Client interface function to cancel the simulation.  This does not mean
     * that the simulation is actually reset because the controller may be on
     * the verge of being destroyed.
     */
    void
    start()
        DNB_THROW_SPEC_ANY;
    
    /**
     * Client interface function to start the run.  The resulting Call
     * condition will not be signalled until the run is halted or finished.
     */
    void
    run()
        DNB_THROW_SPEC_ANY;
    
    /**
     * Client interface function to step the run after a halt.  The
     * resulting Call condition will not be signalled until the run again
     * resumed.
     */
    void
    step()
        DNB_THROW_SPEC_ANY;
    
    /**
     * Client interface function to halt the run.  The resulting call condition
     * will not be signalled until until the controller has reached the
     * "Halted" state.
     */
    void
    halt()
        DNB_THROW_SPEC_ANY;
    
    /**
     * Client interface function to cancel the simulation.  This does not mean
     * that the simulation is actually reset because the controller may be on
     * the verge of being destroyed.
     */
    void
    stop()
        DNB_THROW_SPEC_ANY;
    
    /**
     * Client interface to abort the current simulation if it is running and
     * then issue a reset event.  Sub-controllers that don't register reset
     * handlers will not be otherwise notified of the reset.
     */
    void
    join()
        DNB_THROW_SPEC_ANY;
    
    /**
     * Client interface to abort the current simulation if it is running and
     * then issue a reset event.  Sub-controllers that don't register reset
     * handlers will not be otherwise notified of the reset.
     */
    void
    reset()
        DNB_THROW_SPEC_ANY;

    
    /**
     * Rethrow any exceptions that propogated back to simuation main.
     */
/*    inline
    void
    raise() const
        DNB_THROW_SPEC_ANY; */
    
#ifdef LATER
    /**
     * Convenience function to construct a run-state event filter that checks
     * for a specific run state before triggering.
     */
    static
    RunStateHandler::Filter
    isRunState( const RunState& state )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Convenience function to construct a run-state event filter that checks
     * for a specific run-state in a mask of run states before triggering.
     */
    static
    RunStateHandler::Filter
    isRunStateMask( const RunStateMask& mask )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Blocking wait for a particular run state.
     */
    void
    waitForState( const RunState& state )
        DNB_THROW_SPEC_NULL;
    
    /**
     * Blocking wait for any run state in the mask.
     */
    RunState
    waitForStateMask( const RunStateMask& mask )
        DNB_THROW_SPEC_NULL;
#endif
    
protected:
    
    /**
     * Fill Cycle callback data.
     */
    void
    fillCycleCallbackData( CycleCallbackData & )
        DNB_THROW_SPEC_NULL;
    
    /**
     * Dispatch Cycle callbacks.
     */
    virtual void
    dispatchCycleCallbacks()
        DNB_THROW_SPEC_ANY;
    
    /**
     * Get the current step.
     */
    inline
    Step
    getStepTr() const
        DNB_THROW_SPEC_ANY;
    
    /**
     * Command the fsm to transition to a particular state.
     */
    void
    gotoRunState( const RunState& state )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Get the current run state.
     */
    inline
    RunState
    getRunStateTr() const
        DNB_THROW_SPEC_ANY;
    
    /**
     * Set the current run state.
     */
    void
    setRunState( const RunState& state )
        DNB_THROW_SPEC_ANY;
    
    /**
     *rtl
     */
    bool breakToRunning;
    
    
#ifdef LATER
    /**
     * Blocking wait for a particular run state.
     */
    void
    waitForStateTr( const RunState& state )
        DNB_THROW_SPEC_NULL;
    
    /**
     * Blocking wait for any run state in the mask.
     */
    RunState
    waitForStateMaskTr( const RunStateMask& mask )
        DNB_THROW_SPEC_NULL;
#endif
    
private:
    
    /**
     * Shorten the phase callback database type.
     */
    typedef scl_list<PhaseCallback,DNB_ALLOCATOR(PhaseCallback) > PhaseCallbackList;
    
    /**
     * Shorten the cycle callback database type.
     */
    typedef scl_list<CycleCallback,DNB_ALLOCATOR(CycleCallback) > CycleCallbackList;
    
    /**
     * Define the run-state handler database.
     */
    typedef DNBReactiveVar<RunStateEvent>                    RunStateEventVar;
    
    /**
     * Define the update handler database.
     */
    typedef DNBReactiveVar<UpdateEvent>                      UpdateEventVar;
    
    /**
     * Create the request queue that drives the controller.
     */
    typedef DNBSysPCValQueue<RunState>                       CommandQueue;
    
    /**
     * Dispatch phase callbacks.
     */
    void
    dispatchPhaseCallbacks( const UpdatePhase& phase,
    const UpdateEventData& updateData )
        DNB_THROW_SPEC_ANY;
    
    /**
     * Entry to run the simulation.
     */
    void
    go()
        DNB_THROW_SPEC_ANY;
    
    /**
     * The main control loop that controls the run.
     */
    void
    mainloop()
        DNB_THROW_SPEC_ANY;
    
    void
    doUpdate()
        DNB_THROW_SPEC_ANY;
    
    /**
     * Finite state machine functions which drive the tranistioning between
     * controller states.  The names speak for themselves.
     */
    void
    fsmNullToIdle()
        DNB_THROW_SPEC_ANY;

    
    /**
     *Transition functions from IDLE to other allowable controller states
     */
    void
    fsmIdleToRunning()
        DNB_THROW_SPEC_ANY;
    
    void
    fsmIdleToHalted()
        DNB_THROW_SPEC_ANY;
    
    void
    fsmIdleToStepping()
        DNB_THROW_SPEC_ANY;
    
    void
    fsmIdleToDone()
        DNB_THROW_SPEC_ANY;
    
    void
    fsmIdleToJoinable()
        DNB_THROW_SPEC_ANY;

    void
    fsmIdleToBreak()
        DNB_THROW_SPEC_ANY;



    /**
     *Transition functions from RUNNING to other allowable controller states
     */
    void
    fsmRunningToIdle()
        DNB_THROW_SPEC_ANY;
    
    void
    fsmRunningToHalted()
        DNB_THROW_SPEC_ANY;
    
    void
    fsmRunningToStepping()
        DNB_THROW_SPEC_ANY;
    
    void
    fsmRunningToDone()
        DNB_THROW_SPEC_ANY;
    
    void
    fsmRunningToJoinable()
        DNB_THROW_SPEC_ANY;

    void
    fsmRunningToBreak()
        DNB_THROW_SPEC_ANY;



    /**
     *Transition functions from HALTED to other allowable controller states
     */
    void
    fsmHaltedToIdle()
        DNB_THROW_SPEC_ANY;
    
    void
   fsmHaltedToRunning()
        DNB_THROW_SPEC_ANY;
    
    void
    fsmHaltedToStepping()
        DNB_THROW_SPEC_ANY;
    
    void
    fsmHaltedToDone()
        DNB_THROW_SPEC_ANY;
    
    void
    fsmHaltedToJoinable()
        DNB_THROW_SPEC_ANY;


    
    /**
     *Transition functions from STEPPING to other allowable controller states
     */
    void
    fsmSteppingToIdle()
        DNB_THROW_SPEC_ANY;
    
    void
    fsmSteppingToRunning()
        DNB_THROW_SPEC_ANY;
    
    void
    fsmSteppingToHalted()
        DNB_THROW_SPEC_ANY;
    
    void
    fsmSteppingToDone()
        DNB_THROW_SPEC_ANY;
    
    void
    fsmSteppingToJoinable()
        DNB_THROW_SPEC_ANY;

    void
    fsmSteppingToBreak()
        DNB_THROW_SPEC_ANY;

   

    /**
     *Transition functions from DONE to other allowable controller states
     */
    void
    fsmDoneToIdle()
        DNB_THROW_SPEC_ANY;
    
    void
    fsmDoneToRunning()
        DNB_THROW_SPEC_ANY;
    
    void
    fsmDoneToHalted()
        DNB_THROW_SPEC_ANY;
    
    void
    fsmDoneToStepping()
        DNB_THROW_SPEC_ANY;
    void
    fsmDoneToJoinable()
        DNB_THROW_SPEC_ANY;

    void
    fsmDoneToBreak()
        DNB_THROW_SPEC_ANY;



    /**
     *Transition functions from BREAK to other allowable controller states
     */
    void
    fsmBreakToIdle()
        DNB_THROW_SPEC_ANY;
    
    void
   fsmBreakToRunning()
        DNB_THROW_SPEC_ANY;
    
    void
    fsmBreakToStepping()
        DNB_THROW_SPEC_ANY;
    
    void
    fsmBreakToDone()
        DNB_THROW_SPEC_ANY;
    
    void
    fsmBreakToJoinable()
        DNB_THROW_SPEC_ANY;



    
    /**
     * Instance attributes.
     */
    Step                 currentStep_;
    RunStateEventVar     runHandlers_;
    UpdateEventVar       updateHandlers_;
    RunState             runState_;
	DNBSysThread		 runThread_;
    CommandQueue         runQueue_;
#ifdef LATER
    DNBSysMutexLock      runLock_;
    DNBSysCondition      runCondition_;
#endif
//  RWTHRxmsg*           exception_;  // smw: not used
    PhaseCallbackList    phaseCallbacks_[2];
    CycleCallbackList    cycleCallbacks_;
    bool                 started_;
    
    /**
     * Class attributes.
     */
    typedef void (DNBSimletController::* Transition)() DNB_THROW_SPEC_ANY;
    static const Transition fsm[8][8];
};


/**
 * The following relational operator is totally meaningless. 
 * We have to define it only to keep the AIX compiler happy. AIX compiler
 * has a nasty tendency to fully instantiate each template class,
 * which means that all prerequisites of the class must be satisfied. In this
 * case, the SimletController uses a the ReactiveVar as a data member.
 */

template <class Evnt>
inline  bool
operator==( const DNBReactiveVar<Evnt> &v1, const DNBReactiveVar<Evnt> &v2 );

/**
 * Snarf the public inline definition file.
 */
#include <DNBSimletController.cc>

#endif // _INCLUDE_DNBSIMLETCONTROLLER
