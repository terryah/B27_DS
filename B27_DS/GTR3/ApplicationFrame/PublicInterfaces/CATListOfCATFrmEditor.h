#ifndef	CATListOfCATFrmEditor_h
#define	CATListOfCATFrmEditor_h
// COPYRIGHT DASSAULT SYSTEMES 2001

/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */

/**
 * @collection CATLISTP(CATFrmEditor)
 * Collection class for pointers to <tt>CATFrmEditor</tt>.
 * All the methods of pointer collection classes are available.
 * Refer to the articles dealing with collections in the encyclopedia.
 * @see CATFrmEditor
 */

#include "CD0FRAME.h"

#include "CATLISTP_Clean.h"

#include "CATLISTP_PublicInterface.h"

#include "CATLISTP_Declare.h"

class CATFrmEditor;

#undef CATCOLLEC_ExportedBy
#define CATCOLLEC_ExportedBy ExportedByCD0FRAME

CATLISTP_DECLARE( CATFrmEditor )

#endif
