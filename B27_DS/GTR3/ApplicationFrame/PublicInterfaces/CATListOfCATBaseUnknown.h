#ifndef CATListOfCATBaseUnknown_h
#define CATListOfCATBaseUnknown_h

// COPYRIGHT DASSAULT SYSTEMES 1999

/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */

/**
 * @collection CATLISTP(CATBaseUnknown)
 * Collection class for pointers to <tt>CATBaseUnknown</tt>.
 * All the methods of pointer collection classes are available.
 * Refer to the articles dealing with collections in the encyclopedia.
 * @see CATBaseUnknown
 */

#include "CATLISTV_CATBaseUnknown.h"
#include "sequence_CATBaseUnknown_ptr.h"

#endif
