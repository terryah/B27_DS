#ifndef __CATAfrCATIInPlaceSiteVetoIID_h
#define __CATAfrCATIInPlaceSiteVetoIID_h

// COPYRIGHT DASSAULT SYSTEMES 2004

/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */

/**Header to expose the Class ID of the protected CATIInPlaceSiteVeto interface.
*<b>Role</b>: the CATIInPlaceSiteVeto ID is necessary if this interface is customized by implementing @href CATIWorkbench#GetCustomInterfaces.
*/

#ifndef LOCAL_DEFINITION_FOR_IID
extern IID ExportedByApplicationFrame IID_CATIInPlaceSiteVeto;
#else
extern "C" const IID IID_CATIInPlaceSiteVeto;
#endif

#endif//__CATAfrCATIInPlaceSiteVetoIID_h
