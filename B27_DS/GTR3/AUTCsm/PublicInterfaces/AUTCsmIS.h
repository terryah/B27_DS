// COPYRIGHT DASSAULT SYSTEMES 2005
/** @CAA2Required */
/**********************************************************************/
/* DON T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPEAR AT ANY TIME */
/**********************************************************************/

/**
 * @fullreview AZH 05:03:24
 */

#ifdef  _WINDOWS_SOURCE
#ifdef  __AUTCsmIS
#define ExportedByAUTCsmIS     __declspec(dllexport)
#else
#define ExportedByAUTCsmIS     __declspec(dllimport)
#endif
#else
#define ExportedByAUTCsmIS
#endif
