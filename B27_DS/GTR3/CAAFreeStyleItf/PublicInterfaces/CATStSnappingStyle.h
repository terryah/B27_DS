#ifndef CATStSnappingStyle_H
#define CATStSnappingStyle_H

/**
  * @CAA2Level L0
  * @CAA2Usage U0
  */

//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2002
//==============================================================================
//
//
//==============================================================================
// Usage Notes :
//  --------------------------------------------------
//   IMPORTANT : DASSAULT SYSTEMES INTERNAL USE ONLY.
//  --------------------------------------------------
//
//------------------------------------------------------------------------------
//  
//==============================================================================
// Dec. 02   Creation                                     Tatiana Dmitrieva
//==============================================================================

enum FSSnapping
{
SNAP_ON_VERTEX = 2,
SNAP_ON_BORDER = 4,
SNAP_ON_CTRLPT = 8,
SNAP_ON_SEGMENT = 16  
};

#endif
