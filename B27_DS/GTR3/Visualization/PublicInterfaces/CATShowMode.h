#ifndef CATShowMode_h
#define CATShowMode_h
/** @CAA2Required */
/**********************************************************************/
/* DON T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPEAR AT ANY TIME */
/**********************************************************************/

// COPYRIGHT DASSAULT SYSTEMES 1999


/** @nodoc */
enum CATShowMode { CATNoShow, CATShow, CATNeutral, CATPermanentShow }; 
    
#endif
