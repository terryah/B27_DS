/* -*-c++-*- */
#ifndef CATVisPropertyEditorType_H
#define CATVisPropertyEditorType_H
/** @CAA2Required */
/**********************************************************************/
/* DON T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPEAR AT ANY TIME */
/**********************************************************************/
//
// COPYRIGHT DASSAULT SYSTEMES 1999
//

/** @nodoc */
enum CATVisPropertyEditorType 
{
	CATVPToolbar,
	CATVPPanelEditor,
	CATVPSearch
};

#endif

