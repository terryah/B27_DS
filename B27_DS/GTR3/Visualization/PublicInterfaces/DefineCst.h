#ifndef DefineCst_H
#define DefineCst_H
/** @CAA2Required */
/**********************************************************************/
/* DON T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPEAR AT ANY TIME */
/**********************************************************************/

// COPYRIGHT DASSAULT SYSTEMES 1999



/** @nodoc */

enum VisuType { TYPE_2D, TYPE_3D } ;

/** @nodoc */

//enum Operation { csoOperation, pathManipulatorOperation };
#include "CATGenerativeAttribute.h"

//typedef CATGenerativeAttribute Operation ;


#endif
