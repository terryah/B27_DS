// COPYRIGHT DASSAULT SYSTEMES 2000
//===================================================================
//
// CATIMfg3DToolPathVisu.h
// Define the CATIMfg3DToolPathVisu interface
//
//===================================================================
//
// Usage notes:
//   Provide 
//
//===================================================================
//
//  Apr 2000  Creation: Code generated by the CAA wizard  ffz
//===================================================================
#ifndef CATIMfg3DToolPathVisu_H
#define CATIMfg3DToolPathVisu_H

/**
* @CAA2Level L1
* @CAA2Usage U5
*/

#include "TPEItfEnv.h"
#include "CATIVisu.h"

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByTPEItfEnv IID IID_CATIMfg3DToolPathVisu ;
#else
extern "C" const IID IID_CATIMfg3DToolPathVisu ;
#endif

//------------------------------------------------------------------

/**   
 * Interface dedicated to Tool path objects 3D Visualization.
 * <b>Role</b>: This interface offers services to manage mainly tool path visualization in 3D mode.<br>
 * 
 * @see CATI3DGeoVisu
 */
class ExportedByTPEItfEnv CATIMfg3DToolPathVisu: public CATIVisu
{
  CATDeclareInterface;

  public:


  // No constructors or destructors on this pure virtual base class
  // --------------------------------------------------------------
};

//------------------------------------------------------------------

CATDeclareHandler( CATIMfg3DToolPathVisu, CATIVisu );

#endif
