// COPYRIGHT DASSAULT SYSTEMES 2000
//===================================================================
//
// CATIMfg3DToolPathVisuData.h
// Define the CATIMfg3DToolPathVisuData interface
//
//===================================================================
//
// Usage notes:
//   New interface: describe its use here
//
//===================================================================
//
//  Apr 2000  Creation: Code generated by the CAA wizard  ffz
// 21/12/01 : CRA : Ajout de WithCircle.
// 18/12/03 : CRA : Ajout de WithoutHoles.
//===================================================================
#ifndef CATIMfg3DToolPathVisuData_H
#define CATIMfg3DToolPathVisuData_H

/**
* @CAA2Level L1
* @CAA2Usage U3
*/

#include "TPEItfEnv.h"
#include "CATBaseUnknown.h"

#include "CATCORBABoolean.h"
#include "CATBoolean.h"
#include "CATBooleanDef.h"

class CATRep;

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByTPEItfEnv IID IID_CATIMfg3DToolPathVisuData ;
#else
extern "C" const IID IID_CATIMfg3DToolPathVisuData ;
#endif

//------------------------------------------------------------------

/**   
 * Interface dedicated to the management of datas for Tool path Visualization.
 * <b>Role</b>: This interface offers services to manage tool path visualization in 3D mode.<br>
 * 
 * @see CATIMfg3DToolPathVisu
 */
class ExportedByTPEItfEnv CATIMfg3DToolPathVisuData: public CATBaseUnknown
{
  CATDeclareInterface;

  public:

    /**
	  *       
	  *   Retrieves the polyline rep of the tool path.
	  *   @param oRep
	  *      The polyline rep
     */

	  virtual HRESULT GetPolylineRep (CATRep** oRep) = 0;

    /**
	  *       
	  *   Retrieves the rep of the points of the tool path.
      *   This method works only if DisplayMode is 1.
	  *   @param oRep
	  *      The points rep
     */

	  virtual HRESULT GetPointsRep (CATRep** oRep) = 0;

    /**
     *       
     *   Sets points display mode.
     *   @param iMode
     *      <br><b>Legal values</b>:
     *      <ul>
     *      <li>0 : Points of the tool path are not displayed (default value) </li>
     *      <li>1 : Points of the tool path are displayed and can be selected </li>
     *      </ul>
     */

	  virtual HRESULT SetDisplayMode (int iMode) = 0;

    /**
     *   Retrieves points display mode.
     *   @param oMode
     *      <br><b>Legal values</b>:
     *      <ul>
     *      <li>0 : Points of the tool path are not displayed </li>
     *      <li>1 : Points of the tool path are displayed and can be selected </li>
     *      </ul>
     */

	  virtual HRESULT GetDisplayMode (int &oMode) = 0;

	/**
     *   Sets how circle motions are displayed.
     *   @param iCircle
     *      <br><b>Legal values</b>:
     *      <li>FALSE :  circle motions are displayed by polyline reps</li>
     *      <li>TRUE  :  circle motions are displayed by arc reps</li>
     *      </ul>
	 */     	  
	  virtual HRESULT DrawCircle (CATBoolean iCircle) = 0;

	/**
     *   Retrieves how circle motions are displayed. 
     *   @param oCircle
     *      <br><b>Legal values</b>:
     *      <li>FALSE :  circle motions are displayed by polyline reps</li>
     *      <li>TRUE  :  circle motions are displayed by arc reps</li>
     *      </ul>
	*/     	  
     virtual HRESULT CanDrawCircle (CATBoolean & oCircle)= 0;

    /**
      *   Sets the corner radius of the tool.
      *   <br><b>Role</b>: The corner radius is necessary to display the tool path in tool center points instead of tip.
      *   @param iCornerRadius
      *      The corner radius of the tool associated with the tool path
      */
	  virtual HRESULT SetCornerRadius (double iCornerRadius) = 0;

    /**
      *   Retrieves the corner radius of the tool.
      *   @param oCornerRadius
      *      The corner radius of the tool associated with the tool path
      */
	  virtual HRESULT GetCornerRadius (double &oCornerRadius) = 0;

	/**
     *   Displays or not tool path holes.
     *   @param iWithHoles
     *      <br><b>Legal values</b>:
     *      <li>FALSE :  holes are filled</li>
     *      <li>TRUE  :  holes are displayed</li>
     *      </ul>
	 */     	  
     virtual HRESULT SetWithHoles (CATBoolean iWithHoles)= 0;

	/**
     *   Checks if tool path holes are displayed.
     *   @param oWithHoles
     *      <br><b>Legal values</b>:
     *      <li>FALSE :  holes are filled</li>
     *      <li>TRUE  :  holes are displayed</li>
     *      </ul>
	 */     	  
     virtual HRESULT WithHoles (CATBoolean & oWithHoles)= 0;


};

//------------------------------------------------------------------

CATDeclareHandler( CATIMfg3DToolPathVisuData, CATBaseUnknown );

#endif
