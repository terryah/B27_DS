#
# SHARED LIBRARY
#
BUILT_OBJECT_TYPE=SHARED LIBRARY

INCLUDED_MODULES= \ 
   CDSProfile \
   CDSApprox \
   CDSLinearAlgebra


LINK_WITH= \
  JS0GROUP \
  CATCDS \
  CATCDSUtilities \
  CATMathematics \
  CATAdvancedMathematics \
  CATGMModelInterfaces \
  CATGeometricObjects \
  CATGMGeometricInterfaces \
  CATMathStream 

LOCAL_CCFLAGS_ASSERT=-DNOT_CDS_ASSERT $(MKMK_DEBUG:+"-DCDS_ASSERT")

#if os Windows
LOCAL_CCFLAGS=-D_HAS_EXCEPTIONS=0 $(LOCAL_CCFLAGS_ASSERT)
#elif os Linux
LOCAL_CCFLAGS=-std=c++0x -DCPP11_AVALAIBLE $(LOCAL_CCFLAGS_ASSERT)
#else
LOCAL_CCFLAGS=$(LOCAL_CCFLAGS_ASSERT)
#endif

