# COPYRIGHT DASSAULT SYSTEMES 2002
#======================================================================
# Imakefile for module DNBProgramInterfaces
#======================================================================
#
#  April 2002  Creation: Code generated by the CAA wizard  GRN
#  Jan   2004  Added DNBIgpResourceProgramPubIDL           SAX
#======================================================================
#
# SHARED LIBRARY 
#
BUILT_OBJECT_TYPE=SHARED LIBRARY
 

WIZARD_LINK_MODULES =   JS0GROUP \
                        JS0CORBA \
						      ProcessInterfaces \
                        ApplicationFrame \
                        ON0FRAME

# END WIZARD EDITION ZONE 

LINK_WITH = $(WIZARD_LINK_MODULES)

INCLUDED_MODULES = DNBResourceProgramItfCPP \
				       DNBIgpResourceProgramPubIDL 
                    
				
#
# Define the build options for the current module.
#
OS      = Windows_NT
BUILD   = YES
#
OS      = IRIX
BUILD   = YES

OS      = SunOS
BUILD   = YES

OS      = AIX
BUILD   = YES

OS      = HP-UX
BUILD   = YES

OS      = win_a
BUILD   = NO

 
 
 

