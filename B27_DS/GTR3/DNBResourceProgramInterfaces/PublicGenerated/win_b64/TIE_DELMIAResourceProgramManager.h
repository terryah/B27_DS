#ifndef __TIE_DELMIAResourceProgramManager
#define __TIE_DELMIAResourceProgramManager

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DELMIAResourceProgramManager.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DELMIAResourceProgramManager */
#define declare_TIE_DELMIAResourceProgramManager(classe) \
 \
 \
class TIEDELMIAResourceProgramManager##classe : public DELMIAResourceProgramManager \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DELMIAResourceProgramManager, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall GetAllTasks(CATSafeArrayVariant & oTaskList); \
      virtual HRESULT __stdcall GetTask(const CATBSTR & iTaskName, DELMIATask *& oTask); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DELMIAResourceProgramManager(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall GetAllTasks(CATSafeArrayVariant & oTaskList); \
virtual HRESULT __stdcall GetTask(const CATBSTR & iTaskName, DELMIATask *& oTask); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DELMIAResourceProgramManager(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::GetAllTasks(CATSafeArrayVariant & oTaskList) \
{ \
return (ENVTIECALL(DELMIAResourceProgramManager,ENVTIETypeLetter,ENVTIELetter)GetAllTasks(oTaskList)); \
} \
HRESULT __stdcall  ENVTIEName::GetTask(const CATBSTR & iTaskName, DELMIATask *& oTask) \
{ \
return (ENVTIECALL(DELMIAResourceProgramManager,ENVTIETypeLetter,ENVTIELetter)GetTask(iTaskName,oTask)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DELMIAResourceProgramManager,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DELMIAResourceProgramManager,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DELMIAResourceProgramManager,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DELMIAResourceProgramManager,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DELMIAResourceProgramManager,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DELMIAResourceProgramManager(classe)    TIEDELMIAResourceProgramManager##classe


/* Common methods inside a TIE */
#define common_TIE_DELMIAResourceProgramManager(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DELMIAResourceProgramManager, classe) \
 \
 \
CATImplementTIEMethods(DELMIAResourceProgramManager, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DELMIAResourceProgramManager, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DELMIAResourceProgramManager, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DELMIAResourceProgramManager, classe) \
 \
HRESULT __stdcall  TIEDELMIAResourceProgramManager##classe::GetAllTasks(CATSafeArrayVariant & oTaskList) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&oTaskList); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAllTasks(oTaskList); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&oTaskList); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAResourceProgramManager##classe::GetTask(const CATBSTR & iTaskName, DELMIATask *& oTask) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&iTaskName,&oTask); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetTask(iTaskName,oTask); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&iTaskName,&oTask); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAResourceProgramManager##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAResourceProgramManager##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAResourceProgramManager##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAResourceProgramManager##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAResourceProgramManager##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DELMIAResourceProgramManager(classe) \
 \
 \
declare_TIE_DELMIAResourceProgramManager(classe) \
 \
 \
CATMetaClass * __stdcall TIEDELMIAResourceProgramManager##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DELMIAResourceProgramManager,"DELMIAResourceProgramManager",DELMIAResourceProgramManager::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DELMIAResourceProgramManager(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DELMIAResourceProgramManager, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDELMIAResourceProgramManager##classe(classe::MetaObject(),DELMIAResourceProgramManager::MetaObject(),(void *)CreateTIEDELMIAResourceProgramManager##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DELMIAResourceProgramManager(classe) \
 \
 \
declare_TIE_DELMIAResourceProgramManager(classe) \
 \
 \
CATMetaClass * __stdcall TIEDELMIAResourceProgramManager##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DELMIAResourceProgramManager,"DELMIAResourceProgramManager",DELMIAResourceProgramManager::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DELMIAResourceProgramManager(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DELMIAResourceProgramManager, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDELMIAResourceProgramManager##classe(classe::MetaObject(),DELMIAResourceProgramManager::MetaObject(),(void *)CreateTIEDELMIAResourceProgramManager##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DELMIAResourceProgramManager(classe) TIE_DELMIAResourceProgramManager(classe)
#else
#define BOA_DELMIAResourceProgramManager(classe) CATImplementBOA(DELMIAResourceProgramManager, classe)
#endif

#endif
