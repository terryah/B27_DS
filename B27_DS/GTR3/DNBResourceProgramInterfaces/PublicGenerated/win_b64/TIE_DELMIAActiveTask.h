#ifndef __TIE_DELMIAActiveTask
#define __TIE_DELMIAActiveTask

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DELMIAActiveTask.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DELMIAActiveTask */
#define declare_TIE_DELMIAActiveTask(classe) \
 \
 \
class TIEDELMIAActiveTask##classe : public DELMIAActiveTask \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DELMIAActiveTask, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall SetActiveTask(CATIABase * iResource, DELMIATask * iTask); \
      virtual HRESULT __stdcall GetActiveTask(CATIABase * iResource, DELMIATask *& oTask); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DELMIAActiveTask(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall SetActiveTask(CATIABase * iResource, DELMIATask * iTask); \
virtual HRESULT __stdcall GetActiveTask(CATIABase * iResource, DELMIATask *& oTask); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DELMIAActiveTask(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::SetActiveTask(CATIABase * iResource, DELMIATask * iTask) \
{ \
return (ENVTIECALL(DELMIAActiveTask,ENVTIETypeLetter,ENVTIELetter)SetActiveTask(iResource,iTask)); \
} \
HRESULT __stdcall  ENVTIEName::GetActiveTask(CATIABase * iResource, DELMIATask *& oTask) \
{ \
return (ENVTIECALL(DELMIAActiveTask,ENVTIETypeLetter,ENVTIELetter)GetActiveTask(iResource,oTask)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DELMIAActiveTask,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DELMIAActiveTask,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DELMIAActiveTask,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DELMIAActiveTask,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DELMIAActiveTask,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DELMIAActiveTask(classe)    TIEDELMIAActiveTask##classe


/* Common methods inside a TIE */
#define common_TIE_DELMIAActiveTask(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DELMIAActiveTask, classe) \
 \
 \
CATImplementTIEMethods(DELMIAActiveTask, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DELMIAActiveTask, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DELMIAActiveTask, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DELMIAActiveTask, classe) \
 \
HRESULT __stdcall  TIEDELMIAActiveTask##classe::SetActiveTask(CATIABase * iResource, DELMIATask * iTask) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&iResource,&iTask); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetActiveTask(iResource,iTask); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&iResource,&iTask); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDELMIAActiveTask##classe::GetActiveTask(CATIABase * iResource, DELMIATask *& oTask) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&iResource,&oTask); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetActiveTask(iResource,oTask); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&iResource,&oTask); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAActiveTask##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAActiveTask##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAActiveTask##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAActiveTask##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDELMIAActiveTask##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DELMIAActiveTask(classe) \
 \
 \
declare_TIE_DELMIAActiveTask(classe) \
 \
 \
CATMetaClass * __stdcall TIEDELMIAActiveTask##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DELMIAActiveTask,"DELMIAActiveTask",DELMIAActiveTask::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DELMIAActiveTask(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DELMIAActiveTask, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDELMIAActiveTask##classe(classe::MetaObject(),DELMIAActiveTask::MetaObject(),(void *)CreateTIEDELMIAActiveTask##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DELMIAActiveTask(classe) \
 \
 \
declare_TIE_DELMIAActiveTask(classe) \
 \
 \
CATMetaClass * __stdcall TIEDELMIAActiveTask##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DELMIAActiveTask,"DELMIAActiveTask",DELMIAActiveTask::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DELMIAActiveTask(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DELMIAActiveTask, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDELMIAActiveTask##classe(classe::MetaObject(),DELMIAActiveTask::MetaObject(),(void *)CreateTIEDELMIAActiveTask##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DELMIAActiveTask(classe) TIE_DELMIAActiveTask(classe)
#else
#define BOA_DELMIAActiveTask(classe) CATImplementBOA(DELMIAActiveTask, classe)
#endif

#endif
