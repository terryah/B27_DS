//=============================================================================
//
// DELMIAActiveTask:
// Exposed interface for Task
//
//=============================================================================
// Usage notes:
//   New interface: describe its use here
//
//=============================================================================
// Jan 2004     Creation                                       SAX 
//=============================================================================

#ifndef DELMIATask_IDL
#define DELMIATask_IDL

/*IDLREP*/

// COPYRIGHT Dassault Systemes 2004

/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */

#include "CATIAActivity.idl"
#include "CATBSTR.idl"
#include "CATIAProduct.idl"

  
/**
* Represents the Task of a resource.
*
* <br><b>Role</b>: Task is the object used to access and manage the attributes of the task.
* 
* <br>The following code snippet can be used to obtain the Task from the Resource program manager. 
* <pre>
*   Dim objResourceProgramManager As ResourceProgramManager
*   Dim oTask As Task
*   
*   objResourceProgramManager.GetTask  "RobotTask.1", oTask
* </pre>
*/

interface DELMIATask : CATIAActivity
{
    /**
     * Get the name of the Task
     * @param oName
     *   The user defined name of the Task.
     * @return
     *   An HRESULT.
     *   <br><b>Legal values</b>:
     *   <dl>
     *     <dt>S_OK</dt>
     *     <dd>The name of the task was corrrectly retrieved</dd>
     *     <dt>E_FAIL </dt>
     *     <dd>The name of the task was not corrrectly retrieved</dd>
     *   </dl>
     *   <dl>
     *   <dt><b>Example:</b>
     *   <dd>The following example get the name of the task.
     *   <pre>
     *   Dim objTask As Task
     *   Dim strTaskName As String
     *   ..
     *   objTask.GetName strTaskName
     *   </pre>
     *   </dl>
     */ 

    HRESULT GetName(inout CATBSTR oName);

    /**
     * Set the name of the task
     * @param iName
     *   The user defined name of the Task.
     * @return
     *   An HRESULT.
     *   <br><b>Legal values</b>:
     *   <dl>
     *     <dt>S_OK</dt>
     *     <dd>The name of the task was set corrrectly </dd>
     *     <dt>E_FAIL </dt>
     *     <dd>The name of the task was not set corrrectly </dd>
     *   </dl>
     *   <dl>
     *   <dt><b>Example:</b>
     *   <dd>The following example set the name of the task.
     *   <pre>
     *   Dim objTask As Task     
     *   ..
     *   objTask.SetName("RobotTask.2")
     *   </pre>
     *   </dl>
     */
    HRESULT SetName(in CATBSTR iName);
    
   /**
    * Gets the Owning Resource
    * @param oRes
    *   The Owning Resource.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The owning resource was corrrectly retrieved</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The owning resource was not corrrectly retrieved</dd>
    *   </dl>
    *   <dl>
     *   <dt><b>Example:</b>
     *   <dd>The following example retrieves the owning resource of the task.
     *   <pre>
     *   Dim objTask As Task   
     *   Dim objRobot as Product
     *   ..
     *   objTask.GetResource objRobot
     *   </pre>
     *   </dl>
    */
    HRESULT GetResource(out CATIAProduct oRes );

};

// Interface name : DELMIATask
#pragma ID DELMIATask "DCE:463d013f-086b-4190-9039095a2ea0cc08"
						   

#pragma DUAL DELMIATask

// VB object name : Task (Id used in Visual Basic)
#pragma ID Task "DCE:5646dfee-c447-490c-987d4b77536b1223"
					 
		     

#pragma ALIAS DELMIATask Task

#endif /*DELMIATask_IDL*/
