// COPYRIGHT Dassault Systemes 2003
/** @CAA2Required */
/*---------------------------------------------------------------------*/
/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */
/*---------------------------------------------------------------------*/
#ifdef  _WINDOWS_SOURCE
#ifdef  __DNBResourceProgramItfCPP
#define ExportedByDNBResourceProgramItfCPP     __declspec(dllexport)
#else
#define ExportedByDNBResourceProgramItfCPP     __declspec(dllimport)
#endif
#else
#define ExportedByDNBResourceProgramItfCPP
#endif
