//=============================================================================
//
// DELMIAResourceProgramManager:
// Exposed interface for retriving the tasks associated with the resource 
//
//=============================================================================
// Usage notes:
//   New interface: describe its use here
//
//=============================================================================
// Jan 2004     Creation                                       SAX 
//=============================================================================

#ifndef DELMIAResourceProgramManager_IDL
#define DELMIAResourceProgramManager_IDL

/*IDLREP*/

// COPYRIGHT Dassault Systemes 2004

/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */

#include "CATIABase.idl"
#include "DELMIATask.idl"
#include "CATSafeArray.idl"


/**
* Represents the Resource Program manager of a resource.
*
* <br><b>Role</b>: Resource Program manager is the object used to access the tasks associated to the resource.
* 
* <br>The following code snippet can be used to obtain the Resource Program manager from the robot product. 
* <pre>
*   Dim objResourceProgramManager As ResourceProgramManager
*   Dim objRobot as Product
*   
*   Set objResourceProgramManager = objRobot.GetTechnologicalObject("ResourceProgramManager" )
* </pre>
*/

interface DELMIAResourceProgramManager : CATIABase
{
	 /**
       * Retrieves all the Tasks corresponding to this Resource.
       * @param oTaskList
       *   The list of Tasks.
       * @return
       *   An HRESULT.
       *   <br><b>Legal values</b>:
       *   <dl>
       *     <dt>S_OK</dt>
       *     <dd>The Tasks were corrrectly retrieved</dd>
       *     <dt>E_FAIL </dt>
       *     <dd>The Tasks were not corrrectly retrieved</dd>
       *   </dl>
       *   <dl>
       *   <dt><b>Example:</b>
       *   <dd>The following example retrieves the list of tasks on the resource.
       *   <pre>
       *   Dim objResourceProgramManager As ResourceProgramManager
       *   Dim TaskList(3) As Task
       *   ..
       *   objResourceProgramManager.<font color="blue">GetAllTasks</font> TaskList
       *   </pre>
       *   </dl>
       */
      HRESULT GetAllTasks( inout CATSafeArrayVariant oTaskList );
      
	  /**
       * Retrieves the Tasks corresponding to the given Task Name.
       * @param iTaskName
       *   The name of the Task.
	   * @param oTask
       *   The output Task.
       * @return
       *   An HRESULT.
       *   <br><b>Legal values</b>:
       *   <dl>
       *     <dt>S_OK</dt>
       *     <dd>The Task was corrrectly retrieved</dd>
       *     <dt>E_FAIL </dt>
       *     <dd>The Task was not corrrectly retrieved</dd>
       *   </dl>
       *   <dl>
       *   <dt><b>Example:</b>
       *   <dd>The following example Retrieves the task corresponding the given task name.
       *   <pre>
       *   Dim objResourceProgramManager As ResourceProgramManager
       *   Dim oTask As Task
       *   ..
       *   objResourceProgramManager.GetTask  "RobotTask.1", oTask
       *   </pre>
       *   </dl>
       */
	  HRESULT GetTask( in CATBSTR iTaskName, 
                               inout DELMIATask oTask );

      
      
};

// Interface name : DELMIAResourceProgramManager
#pragma ID DELMIAResourceProgramManager "DCE:d2f9de05-09d4-44f7-aba3ef2e00e33b3a"
											 
#pragma DUAL DELMIAResourceProgramManager

// VB object name : ResourceProgramManager (Id used in Visual Basic)
#pragma ID ResourceProgramManager "DCE:6e71f832-6cfb-4253-8689c3550df38829"
									   
#pragma ALIAS DELMIAResourceProgramManager ResourceProgramManager



#endif /*DELMIAResourceProgramManager_IDL*/
