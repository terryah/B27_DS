// COPYRIGHT DASSAULT SYSTEMES 2002
//===================================================================
//
// DNBITaskFactory.h
// Define the DNBITaskFactory interface. All the D5 devices adhere to 
// this interface
//
//===================================================================
//
// Usage notes:
//===================================================================
//
// 05/20/2002 Creation                                        GRN
// 09/11/2003 Modified:CAA Changes                            NPJ
//
//===================================================================
#ifndef DNBITaskFactory_H
#define DNBITaskFactory_H

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

#include "DNBResourceProgramItfCPP.h"

//System
#include "CATBaseUnknown.h"
#include "CATUnicodeString.h"

class DNBITask;

extern ExportedByDNBResourceProgramItfCPP IID IID_DNBITaskFactory ;

/**
 * Interface to create a Task.
 * <b>Role:</b>
 * This interface provides methods for creating Tasks. 
 */
class ExportedByDNBResourceProgramItfCPP DNBITaskFactory: public CATBaseUnknown
{
   /**
    * @nodoc
    */
   CATDeclareInterface;

   public:

      /**
       * Create a new task.
       * @param iTaskName
       *   Name of the Task.
       * @param oTask
       *   Created Task.
       * @param iTaskType
       *   Type of Task.
       * @return
       *   An HRESULT.
       *   <br><b>Legal values</b>:
       *   <dl>
       *     <dt>S_OK</dt>
       *     <dd>The name of IO Item was corrrectly retrieved</dd>
       *     <dt>E_FAIL </dt>
       *     <dd>The name of IO Item was not corrrectly retrieved</dd>
       *   </dl>
       */
      virtual HRESULT CreateTask( const CATUnicodeString &iTaskName, 
                                  DNBITask ** oTask,
                                  const CATUnicodeString &iTaskType ) = 0;

};

/**
 * @nodoc
 */
CATDeclareHandler( DNBITaskFactory, CATBaseUnknown );

#endif
