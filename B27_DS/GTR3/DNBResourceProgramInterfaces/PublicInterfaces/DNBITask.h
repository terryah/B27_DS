// COPYRIGHT DASSAULT SYSTEMES 2002
//===================================================================
//
// DNBITask.h
// Define the DNBITask interface
//
//===================================================================
//
// Usage notes:
//   New interface: describe its use here
//
//===================================================================
//
// 17/04/2002 Creation                                        GRN
// 05/29/2002 Modified:added method GetResource               GRN
// 09/11/2003 Modified:CAA Changes                            NPJ
//
//===================================================================
#ifndef DNBITask_H
#define DNBITask_H

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

#include "DNBResourceProgramItfCPP.h"

// System
#include "CATBaseUnknown.h"

class CATUnicodeString;

extern ExportedByDNBResourceProgramItfCPP IID IID_DNBITask;

/**
 * Interface to manage Tasks.
 * <b>Role:</b>
 * This interface provides methods to Get/Set on Tasks.
 * @see DNBITaskFactory
 */

class ExportedByDNBResourceProgramItfCPP DNBITask: public CATBaseUnknown
{
   /**
    * @nodoc
    */
  CATDeclareInterface;

   public:

      /**
       * Get the name of the Task
       * @param oName
       *   The user defined name of the Task.
       * @return
       *   An HRESULT.
       *   <br><b>Legal values</b>:
       *   <dl>
       *     <dt>S_OK</dt>
       *     <dd>The name of IO Item was corrrectly retrieved</dd>
       *     <dt>E_FAIL </dt>
       *     <dd>The name of IO Item was not corrrectly retrieved</dd>
       *   </dl>
       */
      virtual HRESULT GetName( CATUnicodeString & oName ) = 0;

      /**
       * Set the name of the task
       * @param iName
       *   The user defined name of the Task.
       * @return
       *   An HRESULT.
       *   <br><b>Legal values</b>:
       *   <dl>
       *     <dt>S_OK</dt>
       *     <dd>The name of IO Item was corrrectly retrieved</dd>
       *     <dt>E_FAIL </dt>
       *     <dd>The name of IO Item was not corrrectly retrieved</dd>
       *   </dl>
       */
      virtual HRESULT SetName( const CATUnicodeString &iName ) = 0;
      
      /**
       * Gets the Owning Resource
       * @param oRes
       *   The Owning Resource.
       * @return
       *   An HRESULT.
       *   <br><b>Legal values</b>:
       *   <dl>
       *     <dt>S_OK</dt>
       *     <dd>The name of IO Item was corrrectly retrieved</dd>
       *     <dt>E_FAIL </dt>
       *     <dd>The name of IO Item was not corrrectly retrieved</dd>
       *   </dl>
       */
      virtual HRESULT GetResource( CATBaseUnknown ** oRes ) = 0;

};

/**
 * @nodoc
 */
CATDeclareHandler( DNBITask, CATBaseUnknown );
//------------------------------------------------------------------
#endif
