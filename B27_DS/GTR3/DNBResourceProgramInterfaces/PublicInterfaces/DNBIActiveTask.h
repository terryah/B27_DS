#ifndef DNBIActiveTask_H
#define DNBIActiveTask_H


// COPYRIGHT Dassault Systemes 2003

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

#include "DNBResourceProgramItfCPP.h"
#include "CATBaseUnknown.h"
#include "CATLISTV_CATBaseUnknown.h"

extern ExportedByDNBResourceProgramItfCPP IID IID_DNBIActiveTask ;

/**
 * Interface to manage Active Tasks.
 * <b>Role:</b>
 * This interface provides methods to define/modify Active Tasks of an activity.
 * Active Tasks can be defined for the resources assigned to an activity. On simulation,
 * the all Active Tasks will be run in parallel. Only one task per resource can be made 
 * active.
 */
class ExportedByDNBResourceProgramItfCPP DNBIActiveTask: public CATBaseUnknown
{
   /**
    * @nodoc
    */
   CATDeclareInterface;

  public:

      /**
       * Defines the Active Tasks for an activity.
       * @param iResourceList
       *   The list of resources.
       * @param iTaskList
       *   The list of Active Tasks.
       * @return
       *   An HRESULT.
       *   <br><b>Legal values</b>:
       *   <dl>
       *     <dt>S_OK</dt>
       *     <dd>The Active Tasks were corrrectly set</dd>
       *     <dt>E_FAIL </dt>
       *     <dd>The Active Tasks were not corrrectly set</dd>
       *   </dl>
       */
      virtual HRESULT SetActiveResourceTask( const CATListValCATBaseUnknown_var &iResourceList,
                                             const CATListValCATBaseUnknown_var &iTaskList ) = 0;
      
      /**
       * Retrieves the Active Tasks corresponding to the Resource List given.
       * @param iResourceList
       *   The list of resources.
       * @param oTaskList
       *   The list of Active Tasks.
       * @return
       *   An HRESULT.
       *   <br><b>Legal values</b>:
       *   <dl>
       *     <dt>S_OK</dt>
       *     <dd>The Active Tasks were corrrectly retrieved</dd>
       *     <dt>E_FAIL </dt>
       *     <dd>The Active Tasks were not corrrectly retrieved</dd>
       *   </dl>
       */
      virtual HRESULT GetActiveResourceTask( const CATListValCATBaseUnknown_var &iResourceList,
                                             CATListValCATBaseUnknown_var &oTaskList ) = 0;
      
      /**
       * Defines the Active Task for an activity for a particular Resource.
       * @param iResource
       *   The resources that owns the Task.
       * @param iTask
       *   The Tasks to be made active.
       * @return
       *   An HRESULT.
       *   <br><b>Legal values</b>:
       *   <dl>
       *     <dt>S_OK</dt>
       *     <dd>The Active Task was corrrectly set</dd>
       *     <dt>E_FAIL </dt>
       *     <dd>The Active Task was not corrrectly set</dd>
       *   </dl>
       */
      virtual HRESULT SetActiveTask( CATBaseUnknown *iResource,
                                     CATBaseUnknown *iTask ) = 0;
      
      /**
       * Retrieves the Active Task for an activity for a particular Resource.
       * @param iResource
       *   The resources.
       * @param oTask
       *   The Active Tasks.
       * @return
       *   An HRESULT.
       *   <br><b>Legal values</b>:
       *   <dl>
       *     <dt>S_OK</dt>
       *     <dd>The component is successfully created
       *         and the interface pointer is successfully returned</dd>
       *     <dt>E_FAIL </dt>
       *     <dd>The component was successfully created,
       *         but the interface query failed</dd>
       *     <dt>E_NOINTERFACE </dt>
       *     <dd>The component was successfully created,
       *         but the it doesn't implement the requested interface</dd>
       *     <dt>E_OUTOFMEMORY </dt>
       *     <dd><dd>The component allocation failed</dd>
       *   </dl>
       */
      virtual HRESULT GetActiveTask( CATBaseUnknown *iResource,
                                     CATBaseUnknown **oTask ) = 0;
      
      /**
       * @nodoc
       * This method is deprecated. DONOT use.
       */
      virtual HRESULT GetActiveTask( const CATBaseUnknown_var& iRes,
          CATBaseUnknown_var& oTask ) = 0;


};

/**
 * @nodoc
 */
CATDeclareHandler( DNBIActiveTask, CATBaseUnknown );
#endif
