# COPYRIGHT DASSAULT SYSTEMES 2004
#======================================================================
# Imakefile for module DNBIgpResourceProgramPubIDL.m
# Module for compilation of the public IDL interfaces
#======================================================================
#
#  Jan 2004  Creation: Code generated by the CAA wizard  sax
#======================================================================
#
# NO BUILD           
#

BUILT_OBJECT_TYPE=NONE

SOURCES_PATH=PublicInterfaces
COMPILATION_IDL=YES

#
# Define the build options for the current module.
#
OS      = Windows_NT
BUILD   = YES

OS      = IRIX
BUILD   = YES

OS      = SunOS
BUILD   = YES

OS      = AIX
BUILD   = YES

OS      = HP-UX
BUILD   = YES

OS      = win_a
BUILD   = NO
