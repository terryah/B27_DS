// COPYRIGHT DASSAULT SYSTEMES  2009
/** @CAA2Required */
//**********************************************************************
//* DON T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS *
//* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPEAR AT ANY TIME *
//**********************************************************************

#ifndef __CATMecModExtendItf_h__
#define __CATMecModExtendItf_h__
#ifdef _WINDOWS_SOURCE
#ifdef  __CATMecModExtendItf
#define ExportedByCATMecModExtendItf    __declspec(dllexport)
#else
#define ExportedByCATMecModExtendItf    __declspec(dllimport)
#endif
#else
#define ExportedByCATMecModExtendItf
#endif

#endif

