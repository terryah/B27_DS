#@ autoformat 14:06:24
# -----------------------------------------------
#
#      Imakefile of GSMUI/GSMUIBasic.m
#
# -----------------------------------------------

BUILT_OBJECT_TYPE=SHARED LIBRARY

LINK_WITH_V5_ONLY=
#
LINK_WITH_V6_ONLY=
#

INCLUDED_MODULES=CATGuiMecMod

LINK_WITH= \
    $(LINK_WITH_V5_ONLY)           \
    $(LINK_WITH_V6_ONLY)           \
    CATGraphicProperties           \ # CATGraphicProperties           CATGraphicProperties
    CATMecModExtendItf             \ # CATMecModExtendItf             CATMecModExtendItf
    CATMecModLiveUseItf            \ # CATMecModLiveUseItf            CATMecModLiveUseItf
    CATMecModUseItf                \ # CATMecModUseItf                CATMecModUseItf
    PrtEnv                         \ # CATMmrVisualization            CATMmrVisu
    PrtProperties                  \ # CATMmrVisualization            CATMmrVisu
    CATSurfResMecMod               \ # CATSurfacicResources           CATSurfResMecMod
    CATSurfacicResourcesInit       \ # CATSurfacicResources           CATSurfacicResourcesInit
    CATSurfacicSharedItf           \ # CATSurfacicSharedItf           CATSurfacicSharedItf
    CATSurfacicComponents          \ # CATSurfacicUIResources         CATSurfacicUIResources
    CATSurfacicUIResources         \ # CATSurfacicUIResources         CATSurfacicUIResources
    CATSurfacicUtilities           \ # CATSurfacicResources           CATSurfacicResources
    DI0PANV2                       \ # Dialog                         DI0PANV2
    CATGMModelInterfaces           \ # GMModelInterfaces              CATGMModelInterfaces
    CATGitInterfaces               \ # GSMInterfaces                  CATGitInterfaces
    CATGmoMecMod                   \ # GSMModel                       GSMModelBasic
    CATGupBase                     \ # GSMUIPrivate                   CATGupBase
	CATGotInterfaces               \ # GSOInterfaces                  CATGotInterfaces
    CATGraph                       \ # Graph                          CATGraph
    KnowledgeItf                   \ # KnowledgeInterfaces            KnowledgeItf
    CATMecModInterfaces            \ # MecModInterfaces               CATMecModInterfaces
    CATMecModLiveInterfaces        \ # MecModLiveInterfaces           CATMecModLiveInterfaces
    CATMechanicalModeler           \ # MechanicalModeler              CATMechanicalModeler
    CATMechanicalModelerUI         \ # MechanicalModelerUI            CATMechanicalModelerUI
    CATObjectModelerBase           \ # ObjectModelerBase              CATObjectModelerBase
    CATObjectModelerNavigator      \ # ObjectModelerNavigator         CATObjectModelerNavigator
    ObjectModelerSystem            \ # ObjectModelerSystem            ObjectModelerSystem
    CATObjectSpecsModeler          \ # ObjectSpecsModeler             CATObjectSpecsModeler
    JS0FM                          \ # System                         JS0FM
    JS0GROUP                       \ # System                         JS0GROUP
    NS0S1MSG                       \ # System                         JS0GROUP
    CATPartInterfaces              \ # PartInterfaces                 CATPartInterfaces
#

OS = COMMON
#if defined(CATIAR201)
LINK_WITH_V6_ONLY= \
    CATAfrFoundation               \ # AfrFoundation                  CATAfrFoundation
    CATAfrItf                      \ # AfrInterfaces                  CATAfrItf
    CATAfrNavigator                \ # AfrNavigator                   CATAfrNavigator
    CAT3DDimVisuIntf               \ # CAT3DDimVisuInterfaces         CAT3DDimVisuIntf
    CATGSMVersionning              \ # CATGSMTopoServices             CATGSMTopoServices
    CATMathStream                  \ # CATMathStream                  CATMathStream
    CATMmrLiveUI                   \ # CATMmrLiveUI                   CATMmrLiveUI
    CATTPSItfBase                  \ # CATTPSInterfacesBase           CATTPSItfBase
    CATConstraint2Interfaces       \ # Constraint2Interfaces          CATConstraint2Interfaces
    CATMmlSys                      \ # MechanicalModelerLive          CATMmlSys
    CATSysTS                       \ # SystemTS                       CATSysTS
    CATVisItf                      \ # VisualizationInterfaces        CATVisItf
#
#else
#if defined(CATIAV5R21)
LINK_WITH_V5_ONLY= \
    CATIAApplicationFrame          \ # CATIAApplicationFrame          CATIAApplicationFrame
    CATTPSItf                      \ # CATTPSInterfaces               CATTPSItf
    CATGSMVersionning              \ # GSMModel
    CATInteractiveInterfaces       \ # InteractiveInterfaces          CATInteractiveInterfaces
    CATMathStream                  \ # Mathematics
    CATMcoModel                    \ # MechanicalCommands             CATMcoModel
    CATViz                         \ #                                
    CATVisualization               \ #                                
    CATApplicationFrame            \ #                                
    CATFeatureCommands             \ #                                
#
#endif
#endif
