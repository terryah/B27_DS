#
#   Imakefile.mk for DNBWorkItf.m
#   Copyright (C) DASSAULT SYSTEMES, 2002
#

BUILT_OBJECT_TYPE = SHARED LIBRARY

INCLUDED_MODULES =  DNBWorkItfCPP \  
					DNBWorkPubIDL \
                    DNBWorkProIDL \

COMMON_LINK_WITH =  JS0GROUP                  \ # System
                    JS0CORBA                  \ # System                    

LINK_WITH = $(COMMON_LINK_WITH)

#
# Define the build options for the current module.
#
OS      = Windows_NT
BUILD   = YES

OS      = IRIX
BUILD   = YES

OS      = SunOS
BUILD   = YES

OS      = AIX
BUILD   = YES

OS      = HP-UX
BUILD   = YES

OS      = win_a
BUILD   = NO

