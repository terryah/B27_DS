#ifndef __TIE_DNBIAWIChangeNotification
#define __TIE_DNBIAWIChangeNotification

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIAWIChangeNotification.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIAWIChangeNotification */
#define declare_TIE_DNBIAWIChangeNotification(classe) \
 \
 \
class TIEDNBIAWIChangeNotification##classe : public DNBIAWIChangeNotification \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIAWIChangeNotification, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall GetParametersList(CATSafeArrayVariant *& ioListOfParameters); \
      virtual HRESULT __stdcall SetParametersList(const CATSafeArrayVariant & iListOfParameters); \
      virtual HRESULT __stdcall get_FixedText(CATBSTR & oFixedText); \
      virtual HRESULT __stdcall put_FixedText(const CATBSTR & iFixedText); \
      virtual HRESULT __stdcall get_UnresolvedText(CATBSTR & oUnresolvedText); \
      virtual HRESULT __stdcall put_UnresolvedText(const CATBSTR & iUnresolvedText); \
      virtual HRESULT __stdcall get_ResolvedText(CATBSTR & oResolvedText); \
      virtual HRESULT __stdcall put_ResolvedText(const CATBSTR & iResolvedText); \
      virtual HRESULT __stdcall GetHyperLinks(CATSafeArrayVariant *& ioHyperlinks); \
      virtual HRESULT __stdcall SetHyperLinks(const CATSafeArrayVariant & iHyperlinks); \
      virtual HRESULT __stdcall GetGeomAssociatedToAnnotation(ItemAssignmentType iAssignmentType, CATBaseDispatch *& ioPointGeom); \
      virtual HRESULT __stdcall GetAttribute(const CATBSTR & iAttrName, CATBSTR & oAttrValue); \
      virtual HRESULT __stdcall SetAttribute(const CATBSTR & iAttrName, const CATBSTR & iAttrValue); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIAWIChangeNotification(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall GetParametersList(CATSafeArrayVariant *& ioListOfParameters); \
virtual HRESULT __stdcall SetParametersList(const CATSafeArrayVariant & iListOfParameters); \
virtual HRESULT __stdcall get_FixedText(CATBSTR & oFixedText); \
virtual HRESULT __stdcall put_FixedText(const CATBSTR & iFixedText); \
virtual HRESULT __stdcall get_UnresolvedText(CATBSTR & oUnresolvedText); \
virtual HRESULT __stdcall put_UnresolvedText(const CATBSTR & iUnresolvedText); \
virtual HRESULT __stdcall get_ResolvedText(CATBSTR & oResolvedText); \
virtual HRESULT __stdcall put_ResolvedText(const CATBSTR & iResolvedText); \
virtual HRESULT __stdcall GetHyperLinks(CATSafeArrayVariant *& ioHyperlinks); \
virtual HRESULT __stdcall SetHyperLinks(const CATSafeArrayVariant & iHyperlinks); \
virtual HRESULT __stdcall GetGeomAssociatedToAnnotation(ItemAssignmentType iAssignmentType, CATBaseDispatch *& ioPointGeom); \
virtual HRESULT __stdcall GetAttribute(const CATBSTR & iAttrName, CATBSTR & oAttrValue); \
virtual HRESULT __stdcall SetAttribute(const CATBSTR & iAttrName, const CATBSTR & iAttrValue); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIAWIChangeNotification(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::GetParametersList(CATSafeArrayVariant *& ioListOfParameters) \
{ \
return (ENVTIECALL(DNBIAWIChangeNotification,ENVTIETypeLetter,ENVTIELetter)GetParametersList(ioListOfParameters)); \
} \
HRESULT __stdcall  ENVTIEName::SetParametersList(const CATSafeArrayVariant & iListOfParameters) \
{ \
return (ENVTIECALL(DNBIAWIChangeNotification,ENVTIETypeLetter,ENVTIELetter)SetParametersList(iListOfParameters)); \
} \
HRESULT __stdcall  ENVTIEName::get_FixedText(CATBSTR & oFixedText) \
{ \
return (ENVTIECALL(DNBIAWIChangeNotification,ENVTIETypeLetter,ENVTIELetter)get_FixedText(oFixedText)); \
} \
HRESULT __stdcall  ENVTIEName::put_FixedText(const CATBSTR & iFixedText) \
{ \
return (ENVTIECALL(DNBIAWIChangeNotification,ENVTIETypeLetter,ENVTIELetter)put_FixedText(iFixedText)); \
} \
HRESULT __stdcall  ENVTIEName::get_UnresolvedText(CATBSTR & oUnresolvedText) \
{ \
return (ENVTIECALL(DNBIAWIChangeNotification,ENVTIETypeLetter,ENVTIELetter)get_UnresolvedText(oUnresolvedText)); \
} \
HRESULT __stdcall  ENVTIEName::put_UnresolvedText(const CATBSTR & iUnresolvedText) \
{ \
return (ENVTIECALL(DNBIAWIChangeNotification,ENVTIETypeLetter,ENVTIELetter)put_UnresolvedText(iUnresolvedText)); \
} \
HRESULT __stdcall  ENVTIEName::get_ResolvedText(CATBSTR & oResolvedText) \
{ \
return (ENVTIECALL(DNBIAWIChangeNotification,ENVTIETypeLetter,ENVTIELetter)get_ResolvedText(oResolvedText)); \
} \
HRESULT __stdcall  ENVTIEName::put_ResolvedText(const CATBSTR & iResolvedText) \
{ \
return (ENVTIECALL(DNBIAWIChangeNotification,ENVTIETypeLetter,ENVTIELetter)put_ResolvedText(iResolvedText)); \
} \
HRESULT __stdcall  ENVTIEName::GetHyperLinks(CATSafeArrayVariant *& ioHyperlinks) \
{ \
return (ENVTIECALL(DNBIAWIChangeNotification,ENVTIETypeLetter,ENVTIELetter)GetHyperLinks(ioHyperlinks)); \
} \
HRESULT __stdcall  ENVTIEName::SetHyperLinks(const CATSafeArrayVariant & iHyperlinks) \
{ \
return (ENVTIECALL(DNBIAWIChangeNotification,ENVTIETypeLetter,ENVTIELetter)SetHyperLinks(iHyperlinks)); \
} \
HRESULT __stdcall  ENVTIEName::GetGeomAssociatedToAnnotation(ItemAssignmentType iAssignmentType, CATBaseDispatch *& ioPointGeom) \
{ \
return (ENVTIECALL(DNBIAWIChangeNotification,ENVTIETypeLetter,ENVTIELetter)GetGeomAssociatedToAnnotation(iAssignmentType,ioPointGeom)); \
} \
HRESULT __stdcall  ENVTIEName::GetAttribute(const CATBSTR & iAttrName, CATBSTR & oAttrValue) \
{ \
return (ENVTIECALL(DNBIAWIChangeNotification,ENVTIETypeLetter,ENVTIELetter)GetAttribute(iAttrName,oAttrValue)); \
} \
HRESULT __stdcall  ENVTIEName::SetAttribute(const CATBSTR & iAttrName, const CATBSTR & iAttrValue) \
{ \
return (ENVTIECALL(DNBIAWIChangeNotification,ENVTIETypeLetter,ENVTIELetter)SetAttribute(iAttrName,iAttrValue)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIAWIChangeNotification,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIAWIChangeNotification,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIAWIChangeNotification,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIAWIChangeNotification,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIAWIChangeNotification,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIAWIChangeNotification(classe)    TIEDNBIAWIChangeNotification##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIAWIChangeNotification(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIAWIChangeNotification, classe) \
 \
 \
CATImplementTIEMethods(DNBIAWIChangeNotification, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIAWIChangeNotification, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIAWIChangeNotification, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIAWIChangeNotification, classe) \
 \
HRESULT __stdcall  TIEDNBIAWIChangeNotification##classe::GetParametersList(CATSafeArrayVariant *& ioListOfParameters) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&ioListOfParameters); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetParametersList(ioListOfParameters); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&ioListOfParameters); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAWIChangeNotification##classe::SetParametersList(const CATSafeArrayVariant & iListOfParameters) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&iListOfParameters); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetParametersList(iListOfParameters); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&iListOfParameters); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAWIChangeNotification##classe::get_FixedText(CATBSTR & oFixedText) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&oFixedText); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_FixedText(oFixedText); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&oFixedText); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAWIChangeNotification##classe::put_FixedText(const CATBSTR & iFixedText) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&iFixedText); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_FixedText(iFixedText); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&iFixedText); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAWIChangeNotification##classe::get_UnresolvedText(CATBSTR & oUnresolvedText) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&oUnresolvedText); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_UnresolvedText(oUnresolvedText); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&oUnresolvedText); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAWIChangeNotification##classe::put_UnresolvedText(const CATBSTR & iUnresolvedText) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&iUnresolvedText); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_UnresolvedText(iUnresolvedText); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&iUnresolvedText); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAWIChangeNotification##classe::get_ResolvedText(CATBSTR & oResolvedText) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&oResolvedText); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_ResolvedText(oResolvedText); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&oResolvedText); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAWIChangeNotification##classe::put_ResolvedText(const CATBSTR & iResolvedText) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&iResolvedText); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_ResolvedText(iResolvedText); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&iResolvedText); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAWIChangeNotification##classe::GetHyperLinks(CATSafeArrayVariant *& ioHyperlinks) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&ioHyperlinks); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetHyperLinks(ioHyperlinks); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&ioHyperlinks); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAWIChangeNotification##classe::SetHyperLinks(const CATSafeArrayVariant & iHyperlinks) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&iHyperlinks); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetHyperLinks(iHyperlinks); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&iHyperlinks); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAWIChangeNotification##classe::GetGeomAssociatedToAnnotation(ItemAssignmentType iAssignmentType, CATBaseDispatch *& ioPointGeom) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,11,&_Trac2,&iAssignmentType,&ioPointGeom); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetGeomAssociatedToAnnotation(iAssignmentType,ioPointGeom); \
   ExitAfterCall(this,11,_Trac2,&_ret_arg,&iAssignmentType,&ioPointGeom); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAWIChangeNotification##classe::GetAttribute(const CATBSTR & iAttrName, CATBSTR & oAttrValue) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,12,&_Trac2,&iAttrName,&oAttrValue); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAttribute(iAttrName,oAttrValue); \
   ExitAfterCall(this,12,_Trac2,&_ret_arg,&iAttrName,&oAttrValue); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAWIChangeNotification##classe::SetAttribute(const CATBSTR & iAttrName, const CATBSTR & iAttrValue) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,13,&_Trac2,&iAttrName,&iAttrValue); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetAttribute(iAttrName,iAttrValue); \
   ExitAfterCall(this,13,_Trac2,&_ret_arg,&iAttrName,&iAttrValue); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAWIChangeNotification##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,14,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,14,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAWIChangeNotification##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,15,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,15,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAWIChangeNotification##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,16,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,16,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAWIChangeNotification##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,17,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,17,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAWIChangeNotification##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,18,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,18,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIAWIChangeNotification(classe) \
 \
 \
declare_TIE_DNBIAWIChangeNotification(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAWIChangeNotification##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAWIChangeNotification,"DNBIAWIChangeNotification",DNBIAWIChangeNotification::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAWIChangeNotification(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIAWIChangeNotification, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAWIChangeNotification##classe(classe::MetaObject(),DNBIAWIChangeNotification::MetaObject(),(void *)CreateTIEDNBIAWIChangeNotification##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIAWIChangeNotification(classe) \
 \
 \
declare_TIE_DNBIAWIChangeNotification(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAWIChangeNotification##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAWIChangeNotification,"DNBIAWIChangeNotification",DNBIAWIChangeNotification::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAWIChangeNotification(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIAWIChangeNotification, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAWIChangeNotification##classe(classe::MetaObject(),DNBIAWIChangeNotification::MetaObject(),(void *)CreateTIEDNBIAWIChangeNotification##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIAWIChangeNotification(classe) TIE_DNBIAWIChangeNotification(classe)
#else
#define BOA_DNBIAWIChangeNotification(classe) CATImplementBOA(DNBIAWIChangeNotification, classe)
#endif

#endif
