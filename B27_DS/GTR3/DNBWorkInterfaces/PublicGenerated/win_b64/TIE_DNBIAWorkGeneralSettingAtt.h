#ifndef __TIE_DNBIAWorkGeneralSettingAtt
#define __TIE_DNBIAWorkGeneralSettingAtt

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIAWorkGeneralSettingAtt.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIAWorkGeneralSettingAtt */
#define declare_TIE_DNBIAWorkGeneralSettingAtt(classe) \
 \
 \
class TIEDNBIAWorkGeneralSettingAtt##classe : public DNBIAWorkGeneralSettingAtt \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIAWorkGeneralSettingAtt, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall get_StyleSheetPath(CATBSTR & iofilepath); \
      virtual HRESULT __stdcall put_StyleSheetPath(const CATBSTR & ifilepath); \
      virtual HRESULT __stdcall GetStyleSheetPathInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetStyleSheetPathLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_DNBWINumColumnList(CATSafeArrayVariant *& olist); \
      virtual HRESULT __stdcall put_DNBWINumColumnList(const CATSafeArrayVariant & ilist); \
      virtual HRESULT __stdcall GetDNBWINumColumnListInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetDNBWINumColumnListLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_TextActivity(CAT_VARIANT_BOOL & oTextActivity); \
      virtual HRESULT __stdcall put_TextActivity(CAT_VARIANT_BOOL iTextActivity); \
      virtual HRESULT __stdcall GetTextActivityInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetTextActivityLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_AnnotationActivity(CAT_VARIANT_BOOL & oAnnotationActivity); \
      virtual HRESULT __stdcall put_AnnotationActivity(CAT_VARIANT_BOOL iAnnotationActivity); \
      virtual HRESULT __stdcall GetAnnotationActivityInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetAnnotationActivityLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_VisibilityActivity(CAT_VARIANT_BOOL & oVisibilityActivity); \
      virtual HRESULT __stdcall put_VisibilityActivity(CAT_VARIANT_BOOL iVisibilityActivity); \
      virtual HRESULT __stdcall GetVisibilityActivityInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetVisibilityActivityLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_ViewPointActivity(CAT_VARIANT_BOOL & oViewPointActivity); \
      virtual HRESULT __stdcall put_ViewPointActivity(CAT_VARIANT_BOOL iViewPointActivity); \
      virtual HRESULT __stdcall GetViewPointActivityInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetViewPointActivityLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall get_MoveActivity(CAT_VARIANT_BOOL & oMoveActivity); \
      virtual HRESULT __stdcall put_MoveActivity(CAT_VARIANT_BOOL iMoveActivity); \
      virtual HRESULT __stdcall GetMoveActivityInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
      virtual HRESULT __stdcall SetMoveActivityLock(CAT_VARIANT_BOOL iLocked); \
      virtual HRESULT __stdcall Commit(); \
      virtual HRESULT __stdcall Rollback(); \
      virtual HRESULT __stdcall ResetToAdminValues(); \
      virtual HRESULT __stdcall ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList); \
      virtual HRESULT __stdcall SaveRepository(); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIAWorkGeneralSettingAtt(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall get_StyleSheetPath(CATBSTR & iofilepath); \
virtual HRESULT __stdcall put_StyleSheetPath(const CATBSTR & ifilepath); \
virtual HRESULT __stdcall GetStyleSheetPathInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetStyleSheetPathLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_DNBWINumColumnList(CATSafeArrayVariant *& olist); \
virtual HRESULT __stdcall put_DNBWINumColumnList(const CATSafeArrayVariant & ilist); \
virtual HRESULT __stdcall GetDNBWINumColumnListInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetDNBWINumColumnListLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_TextActivity(CAT_VARIANT_BOOL & oTextActivity); \
virtual HRESULT __stdcall put_TextActivity(CAT_VARIANT_BOOL iTextActivity); \
virtual HRESULT __stdcall GetTextActivityInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetTextActivityLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_AnnotationActivity(CAT_VARIANT_BOOL & oAnnotationActivity); \
virtual HRESULT __stdcall put_AnnotationActivity(CAT_VARIANT_BOOL iAnnotationActivity); \
virtual HRESULT __stdcall GetAnnotationActivityInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetAnnotationActivityLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_VisibilityActivity(CAT_VARIANT_BOOL & oVisibilityActivity); \
virtual HRESULT __stdcall put_VisibilityActivity(CAT_VARIANT_BOOL iVisibilityActivity); \
virtual HRESULT __stdcall GetVisibilityActivityInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetVisibilityActivityLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_ViewPointActivity(CAT_VARIANT_BOOL & oViewPointActivity); \
virtual HRESULT __stdcall put_ViewPointActivity(CAT_VARIANT_BOOL iViewPointActivity); \
virtual HRESULT __stdcall GetViewPointActivityInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetViewPointActivityLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall get_MoveActivity(CAT_VARIANT_BOOL & oMoveActivity); \
virtual HRESULT __stdcall put_MoveActivity(CAT_VARIANT_BOOL iMoveActivity); \
virtual HRESULT __stdcall GetMoveActivityInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified); \
virtual HRESULT __stdcall SetMoveActivityLock(CAT_VARIANT_BOOL iLocked); \
virtual HRESULT __stdcall Commit(); \
virtual HRESULT __stdcall Rollback(); \
virtual HRESULT __stdcall ResetToAdminValues(); \
virtual HRESULT __stdcall ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList); \
virtual HRESULT __stdcall SaveRepository(); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIAWorkGeneralSettingAtt(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::get_StyleSheetPath(CATBSTR & iofilepath) \
{ \
return (ENVTIECALL(DNBIAWorkGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_StyleSheetPath(iofilepath)); \
} \
HRESULT __stdcall  ENVTIEName::put_StyleSheetPath(const CATBSTR & ifilepath) \
{ \
return (ENVTIECALL(DNBIAWorkGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_StyleSheetPath(ifilepath)); \
} \
HRESULT __stdcall  ENVTIEName::GetStyleSheetPathInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAWorkGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetStyleSheetPathInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetStyleSheetPathLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAWorkGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetStyleSheetPathLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_DNBWINumColumnList(CATSafeArrayVariant *& olist) \
{ \
return (ENVTIECALL(DNBIAWorkGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_DNBWINumColumnList(olist)); \
} \
HRESULT __stdcall  ENVTIEName::put_DNBWINumColumnList(const CATSafeArrayVariant & ilist) \
{ \
return (ENVTIECALL(DNBIAWorkGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_DNBWINumColumnList(ilist)); \
} \
HRESULT __stdcall  ENVTIEName::GetDNBWINumColumnListInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAWorkGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetDNBWINumColumnListInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetDNBWINumColumnListLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAWorkGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetDNBWINumColumnListLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_TextActivity(CAT_VARIANT_BOOL & oTextActivity) \
{ \
return (ENVTIECALL(DNBIAWorkGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_TextActivity(oTextActivity)); \
} \
HRESULT __stdcall  ENVTIEName::put_TextActivity(CAT_VARIANT_BOOL iTextActivity) \
{ \
return (ENVTIECALL(DNBIAWorkGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_TextActivity(iTextActivity)); \
} \
HRESULT __stdcall  ENVTIEName::GetTextActivityInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAWorkGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetTextActivityInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetTextActivityLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAWorkGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetTextActivityLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_AnnotationActivity(CAT_VARIANT_BOOL & oAnnotationActivity) \
{ \
return (ENVTIECALL(DNBIAWorkGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_AnnotationActivity(oAnnotationActivity)); \
} \
HRESULT __stdcall  ENVTIEName::put_AnnotationActivity(CAT_VARIANT_BOOL iAnnotationActivity) \
{ \
return (ENVTIECALL(DNBIAWorkGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_AnnotationActivity(iAnnotationActivity)); \
} \
HRESULT __stdcall  ENVTIEName::GetAnnotationActivityInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAWorkGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetAnnotationActivityInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetAnnotationActivityLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAWorkGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetAnnotationActivityLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_VisibilityActivity(CAT_VARIANT_BOOL & oVisibilityActivity) \
{ \
return (ENVTIECALL(DNBIAWorkGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_VisibilityActivity(oVisibilityActivity)); \
} \
HRESULT __stdcall  ENVTIEName::put_VisibilityActivity(CAT_VARIANT_BOOL iVisibilityActivity) \
{ \
return (ENVTIECALL(DNBIAWorkGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_VisibilityActivity(iVisibilityActivity)); \
} \
HRESULT __stdcall  ENVTIEName::GetVisibilityActivityInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAWorkGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetVisibilityActivityInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetVisibilityActivityLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAWorkGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetVisibilityActivityLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_ViewPointActivity(CAT_VARIANT_BOOL & oViewPointActivity) \
{ \
return (ENVTIECALL(DNBIAWorkGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_ViewPointActivity(oViewPointActivity)); \
} \
HRESULT __stdcall  ENVTIEName::put_ViewPointActivity(CAT_VARIANT_BOOL iViewPointActivity) \
{ \
return (ENVTIECALL(DNBIAWorkGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_ViewPointActivity(iViewPointActivity)); \
} \
HRESULT __stdcall  ENVTIEName::GetViewPointActivityInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAWorkGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetViewPointActivityInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetViewPointActivityLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAWorkGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetViewPointActivityLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::get_MoveActivity(CAT_VARIANT_BOOL & oMoveActivity) \
{ \
return (ENVTIECALL(DNBIAWorkGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_MoveActivity(oMoveActivity)); \
} \
HRESULT __stdcall  ENVTIEName::put_MoveActivity(CAT_VARIANT_BOOL iMoveActivity) \
{ \
return (ENVTIECALL(DNBIAWorkGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_MoveActivity(iMoveActivity)); \
} \
HRESULT __stdcall  ENVTIEName::GetMoveActivityInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
return (ENVTIECALL(DNBIAWorkGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetMoveActivityInfo(ioAdminLevel,ioLocked,oModified)); \
} \
HRESULT __stdcall  ENVTIEName::SetMoveActivityLock(CAT_VARIANT_BOOL iLocked) \
{ \
return (ENVTIECALL(DNBIAWorkGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)SetMoveActivityLock(iLocked)); \
} \
HRESULT __stdcall  ENVTIEName::Commit() \
{ \
return (ENVTIECALL(DNBIAWorkGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)Commit()); \
} \
HRESULT __stdcall  ENVTIEName::Rollback() \
{ \
return (ENVTIECALL(DNBIAWorkGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)Rollback()); \
} \
HRESULT __stdcall  ENVTIEName::ResetToAdminValues() \
{ \
return (ENVTIECALL(DNBIAWorkGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)ResetToAdminValues()); \
} \
HRESULT __stdcall  ENVTIEName::ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList) \
{ \
return (ENVTIECALL(DNBIAWorkGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)ResetToAdminValuesByName(iAttList)); \
} \
HRESULT __stdcall  ENVTIEName::SaveRepository() \
{ \
return (ENVTIECALL(DNBIAWorkGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)SaveRepository()); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIAWorkGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIAWorkGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIAWorkGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIAWorkGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIAWorkGeneralSettingAtt,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIAWorkGeneralSettingAtt(classe)    TIEDNBIAWorkGeneralSettingAtt##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIAWorkGeneralSettingAtt(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIAWorkGeneralSettingAtt, classe) \
 \
 \
CATImplementTIEMethods(DNBIAWorkGeneralSettingAtt, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIAWorkGeneralSettingAtt, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIAWorkGeneralSettingAtt, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIAWorkGeneralSettingAtt, classe) \
 \
HRESULT __stdcall  TIEDNBIAWorkGeneralSettingAtt##classe::get_StyleSheetPath(CATBSTR & iofilepath) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&iofilepath); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_StyleSheetPath(iofilepath); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&iofilepath); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAWorkGeneralSettingAtt##classe::put_StyleSheetPath(const CATBSTR & ifilepath) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&ifilepath); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_StyleSheetPath(ifilepath); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&ifilepath); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAWorkGeneralSettingAtt##classe::GetStyleSheetPathInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetStyleSheetPathInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAWorkGeneralSettingAtt##classe::SetStyleSheetPathLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetStyleSheetPathLock(iLocked); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAWorkGeneralSettingAtt##classe::get_DNBWINumColumnList(CATSafeArrayVariant *& olist) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&olist); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_DNBWINumColumnList(olist); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&olist); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAWorkGeneralSettingAtt##classe::put_DNBWINumColumnList(const CATSafeArrayVariant & ilist) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&ilist); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_DNBWINumColumnList(ilist); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&ilist); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAWorkGeneralSettingAtt##classe::GetDNBWINumColumnListInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetDNBWINumColumnListInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAWorkGeneralSettingAtt##classe::SetDNBWINumColumnListLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetDNBWINumColumnListLock(iLocked); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAWorkGeneralSettingAtt##classe::get_TextActivity(CAT_VARIANT_BOOL & oTextActivity) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&oTextActivity); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_TextActivity(oTextActivity); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&oTextActivity); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAWorkGeneralSettingAtt##classe::put_TextActivity(CAT_VARIANT_BOOL iTextActivity) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&iTextActivity); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_TextActivity(iTextActivity); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&iTextActivity); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAWorkGeneralSettingAtt##classe::GetTextActivityInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,11,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetTextActivityInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,11,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAWorkGeneralSettingAtt##classe::SetTextActivityLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,12,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetTextActivityLock(iLocked); \
   ExitAfterCall(this,12,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAWorkGeneralSettingAtt##classe::get_AnnotationActivity(CAT_VARIANT_BOOL & oAnnotationActivity) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,13,&_Trac2,&oAnnotationActivity); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_AnnotationActivity(oAnnotationActivity); \
   ExitAfterCall(this,13,_Trac2,&_ret_arg,&oAnnotationActivity); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAWorkGeneralSettingAtt##classe::put_AnnotationActivity(CAT_VARIANT_BOOL iAnnotationActivity) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,14,&_Trac2,&iAnnotationActivity); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_AnnotationActivity(iAnnotationActivity); \
   ExitAfterCall(this,14,_Trac2,&_ret_arg,&iAnnotationActivity); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAWorkGeneralSettingAtt##classe::GetAnnotationActivityInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,15,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAnnotationActivityInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,15,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAWorkGeneralSettingAtt##classe::SetAnnotationActivityLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,16,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetAnnotationActivityLock(iLocked); \
   ExitAfterCall(this,16,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAWorkGeneralSettingAtt##classe::get_VisibilityActivity(CAT_VARIANT_BOOL & oVisibilityActivity) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,17,&_Trac2,&oVisibilityActivity); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_VisibilityActivity(oVisibilityActivity); \
   ExitAfterCall(this,17,_Trac2,&_ret_arg,&oVisibilityActivity); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAWorkGeneralSettingAtt##classe::put_VisibilityActivity(CAT_VARIANT_BOOL iVisibilityActivity) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,18,&_Trac2,&iVisibilityActivity); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_VisibilityActivity(iVisibilityActivity); \
   ExitAfterCall(this,18,_Trac2,&_ret_arg,&iVisibilityActivity); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAWorkGeneralSettingAtt##classe::GetVisibilityActivityInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,19,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetVisibilityActivityInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,19,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAWorkGeneralSettingAtt##classe::SetVisibilityActivityLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,20,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetVisibilityActivityLock(iLocked); \
   ExitAfterCall(this,20,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAWorkGeneralSettingAtt##classe::get_ViewPointActivity(CAT_VARIANT_BOOL & oViewPointActivity) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,21,&_Trac2,&oViewPointActivity); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_ViewPointActivity(oViewPointActivity); \
   ExitAfterCall(this,21,_Trac2,&_ret_arg,&oViewPointActivity); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAWorkGeneralSettingAtt##classe::put_ViewPointActivity(CAT_VARIANT_BOOL iViewPointActivity) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,22,&_Trac2,&iViewPointActivity); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_ViewPointActivity(iViewPointActivity); \
   ExitAfterCall(this,22,_Trac2,&_ret_arg,&iViewPointActivity); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAWorkGeneralSettingAtt##classe::GetViewPointActivityInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,23,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetViewPointActivityInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,23,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAWorkGeneralSettingAtt##classe::SetViewPointActivityLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,24,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetViewPointActivityLock(iLocked); \
   ExitAfterCall(this,24,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAWorkGeneralSettingAtt##classe::get_MoveActivity(CAT_VARIANT_BOOL & oMoveActivity) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,25,&_Trac2,&oMoveActivity); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_MoveActivity(oMoveActivity); \
   ExitAfterCall(this,25,_Trac2,&_ret_arg,&oMoveActivity); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAWorkGeneralSettingAtt##classe::put_MoveActivity(CAT_VARIANT_BOOL iMoveActivity) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,26,&_Trac2,&iMoveActivity); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_MoveActivity(iMoveActivity); \
   ExitAfterCall(this,26,_Trac2,&_ret_arg,&iMoveActivity); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAWorkGeneralSettingAtt##classe::GetMoveActivityInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,27,&_Trac2,&ioAdminLevel,&ioLocked,&oModified); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetMoveActivityInfo(ioAdminLevel,ioLocked,oModified); \
   ExitAfterCall(this,27,_Trac2,&_ret_arg,&ioAdminLevel,&ioLocked,&oModified); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAWorkGeneralSettingAtt##classe::SetMoveActivityLock(CAT_VARIANT_BOOL iLocked) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,28,&_Trac2,&iLocked); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetMoveActivityLock(iLocked); \
   ExitAfterCall(this,28,_Trac2,&_ret_arg,&iLocked); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAWorkGeneralSettingAtt##classe::Commit() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,29,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Commit(); \
   ExitAfterCall(this,29,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAWorkGeneralSettingAtt##classe::Rollback() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,30,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Rollback(); \
   ExitAfterCall(this,30,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAWorkGeneralSettingAtt##classe::ResetToAdminValues() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,31,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ResetToAdminValues(); \
   ExitAfterCall(this,31,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAWorkGeneralSettingAtt##classe::ResetToAdminValuesByName(const CATSafeArrayVariant & iAttList) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,32,&_Trac2,&iAttList); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->ResetToAdminValuesByName(iAttList); \
   ExitAfterCall(this,32,_Trac2,&_ret_arg,&iAttList); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAWorkGeneralSettingAtt##classe::SaveRepository() \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,33,&_Trac2); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SaveRepository(); \
   ExitAfterCall(this,33,_Trac2,&_ret_arg); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAWorkGeneralSettingAtt##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,34,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,34,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAWorkGeneralSettingAtt##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,35,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,35,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAWorkGeneralSettingAtt##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,36,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,36,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAWorkGeneralSettingAtt##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,37,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,37,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAWorkGeneralSettingAtt##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,38,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,38,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIAWorkGeneralSettingAtt(classe) \
 \
 \
declare_TIE_DNBIAWorkGeneralSettingAtt(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAWorkGeneralSettingAtt##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAWorkGeneralSettingAtt,"DNBIAWorkGeneralSettingAtt",DNBIAWorkGeneralSettingAtt::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAWorkGeneralSettingAtt(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIAWorkGeneralSettingAtt, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAWorkGeneralSettingAtt##classe(classe::MetaObject(),DNBIAWorkGeneralSettingAtt::MetaObject(),(void *)CreateTIEDNBIAWorkGeneralSettingAtt##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIAWorkGeneralSettingAtt(classe) \
 \
 \
declare_TIE_DNBIAWorkGeneralSettingAtt(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAWorkGeneralSettingAtt##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAWorkGeneralSettingAtt,"DNBIAWorkGeneralSettingAtt",DNBIAWorkGeneralSettingAtt::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAWorkGeneralSettingAtt(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIAWorkGeneralSettingAtt, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAWorkGeneralSettingAtt##classe(classe::MetaObject(),DNBIAWorkGeneralSettingAtt::MetaObject(),(void *)CreateTIEDNBIAWorkGeneralSettingAtt##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIAWorkGeneralSettingAtt(classe) TIE_DNBIAWorkGeneralSettingAtt(classe)
#else
#define BOA_DNBIAWorkGeneralSettingAtt(classe) CATImplementBOA(DNBIAWorkGeneralSettingAtt, classe)
#endif

#endif
