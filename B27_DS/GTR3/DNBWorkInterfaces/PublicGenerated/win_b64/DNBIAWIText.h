/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef DNBIAWIText_h
#define DNBIAWIText_h

#ifndef ExportedByDNBWorkPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __DNBWorkPubIDL
#define ExportedByDNBWorkPubIDL __declspec(dllexport)
#else
#define ExportedByDNBWorkPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByDNBWorkPubIDL
#endif
#endif

#include "CATBSTR.h"
#include "CATIABase.h"
#include "CATSafeArray.h"
#include "DNBItemAssignmentType.h"

class CATBaseDispatch;

extern ExportedByDNBWorkPubIDL IID IID_DNBIAWIText;

class ExportedByDNBWorkPubIDL DNBIAWIText : public CATIABase
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_FixedText(CATBSTR & oFixedText)=0;

    virtual HRESULT __stdcall put_FixedText(const CATBSTR & iFixedText)=0;

    virtual HRESULT __stdcall get_UnresolvedText(CATBSTR & oUnresolvedText)=0;

    virtual HRESULT __stdcall put_UnresolvedText(const CATBSTR & iUnresolvedText)=0;

    virtual HRESULT __stdcall get_ResolvedText(CATBSTR & oResolvedText)=0;

    virtual HRESULT __stdcall put_ResolvedText(const CATBSTR & iResolvedText)=0;

    virtual HRESULT __stdcall GetHyperLinks(CATSafeArrayVariant *& ioHyperlinks)=0;

    virtual HRESULT __stdcall SetHyperLinks(const CATSafeArrayVariant & iHyperlinks)=0;

    virtual HRESULT __stdcall GetGeomAssociatedToAnnotation(ItemAssignmentType iAssignmentType, CATBaseDispatch *& ioPointGeom)=0;

    virtual HRESULT __stdcall GetAttribute(const CATBSTR & iAttrName, CATBSTR & oAttrValue)=0;

    virtual HRESULT __stdcall SetAttribute(const CATBSTR & iAttrName, const CATBSTR & iAttrValue)=0;


};

CATDeclareHandler(DNBIAWIText, CATIABase);

#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
