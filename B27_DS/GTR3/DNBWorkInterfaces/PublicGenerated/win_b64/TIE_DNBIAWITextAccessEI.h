#ifndef __TIE_DNBIAWITextAccessEI
#define __TIE_DNBIAWITextAccessEI

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIAWITextAccessEI.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIAWITextAccessEI */
#define declare_TIE_DNBIAWITextAccessEI(classe) \
 \
 \
class TIEDNBIAWITextAccessEI##classe : public DNBIAWITextAccessEI \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIAWITextAccessEI, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall GetGeomReferedByAnnotation(short iOporAct, CATLONG iAssignedEIIndex, ItemAssignmentType iAssignmentType, CATBaseDispatch *& ioPointGeom); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIAWITextAccessEI(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall GetGeomReferedByAnnotation(short iOporAct, CATLONG iAssignedEIIndex, ItemAssignmentType iAssignmentType, CATBaseDispatch *& ioPointGeom); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIAWITextAccessEI(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::GetGeomReferedByAnnotation(short iOporAct, CATLONG iAssignedEIIndex, ItemAssignmentType iAssignmentType, CATBaseDispatch *& ioPointGeom) \
{ \
return (ENVTIECALL(DNBIAWITextAccessEI,ENVTIETypeLetter,ENVTIELetter)GetGeomReferedByAnnotation(iOporAct,iAssignedEIIndex,iAssignmentType,ioPointGeom)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIAWITextAccessEI,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIAWITextAccessEI,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIAWITextAccessEI,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIAWITextAccessEI,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIAWITextAccessEI,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIAWITextAccessEI(classe)    TIEDNBIAWITextAccessEI##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIAWITextAccessEI(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIAWITextAccessEI, classe) \
 \
 \
CATImplementTIEMethods(DNBIAWITextAccessEI, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIAWITextAccessEI, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIAWITextAccessEI, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIAWITextAccessEI, classe) \
 \
HRESULT __stdcall  TIEDNBIAWITextAccessEI##classe::GetGeomReferedByAnnotation(short iOporAct, CATLONG iAssignedEIIndex, ItemAssignmentType iAssignmentType, CATBaseDispatch *& ioPointGeom) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&iOporAct,&iAssignedEIIndex,&iAssignmentType,&ioPointGeom); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetGeomReferedByAnnotation(iOporAct,iAssignedEIIndex,iAssignmentType,ioPointGeom); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&iOporAct,&iAssignedEIIndex,&iAssignmentType,&ioPointGeom); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAWITextAccessEI##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAWITextAccessEI##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAWITextAccessEI##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAWITextAccessEI##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAWITextAccessEI##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIAWITextAccessEI(classe) \
 \
 \
declare_TIE_DNBIAWITextAccessEI(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAWITextAccessEI##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAWITextAccessEI,"DNBIAWITextAccessEI",DNBIAWITextAccessEI::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAWITextAccessEI(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIAWITextAccessEI, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAWITextAccessEI##classe(classe::MetaObject(),DNBIAWITextAccessEI::MetaObject(),(void *)CreateTIEDNBIAWITextAccessEI##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIAWITextAccessEI(classe) \
 \
 \
declare_TIE_DNBIAWITextAccessEI(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAWITextAccessEI##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAWITextAccessEI,"DNBIAWITextAccessEI",DNBIAWITextAccessEI::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAWITextAccessEI(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIAWITextAccessEI, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAWITextAccessEI##classe(classe::MetaObject(),DNBIAWITextAccessEI::MetaObject(),(void *)CreateTIEDNBIAWITextAccessEI##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIAWITextAccessEI(classe) TIE_DNBIAWITextAccessEI(classe)
#else
#define BOA_DNBIAWITextAccessEI(classe) CATImplementBOA(DNBIAWITextAccessEI, classe)
#endif

#endif
