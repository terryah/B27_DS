# COPYRIGHT DASSAULT SYSTEMES 2003
#======================================================================
# Imakefile for module CATVizVoxels.m
#======================================================================
#
#  Apr 2003  Creation: Code generated by the CAA wizard  jut
#======================================================================
#
# SHARED LIBRARY 
#
BUILT_OBJECT_TYPE=SHARED LIBRARY 
 
# DO NOT EDIT :: THE CAA2 WIZARDS WILL ADD CODE HERE
WIZARD_LINK_MODULES = CATVizBase JS0CORBA YN000MAT CATMathStream
# END WIZARD EDITION ZONE

LINK_WITH = $(WIZARD_LINK_MODULES)

# System dependant variables
#
OS = AIX
#
OS = HP-UX
#
OS = IRIX
#
OS = SunOS
#
OS = Windows_NT
