BUILT_OBJECT_TYPE = SHARED LIBRARY
LINK_WITH = VisCG JS0ERROR

FLEX_VERSION = 2.5.35
BISON_VERSION = 2.4.1

# LOCAL_BYFLAGS = --report-file E:\tmp\bison.log
# LOCAL_FLFLAGS= --header-file=

## System dependant variables

#
OS = Windows
BUILD = YES


## Disable build on other systems
OS = COMMON
BUILD = NO
