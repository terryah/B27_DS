// COPYRIGHT DASSAULT SYSTEMES 2002
/** @CAA2Required */
/**********************************************************************/
/* DON T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPEAR AT ANY TIME */
/**********************************************************************/


#include "CATIAV5Level.h"
#ifdef CATIAV5R11
#define RepWeakRef_R10 1
#else
#undef RepWeakRef_R10
#endif
