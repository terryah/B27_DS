// COPYRIGHT DASSAULT SYSTEMES 2003
#ifndef CATMultiThreadCullingAlgorithm_H
#define CATMultiThreadCullingAlgorithm_H
/** @CAA2Required */
/**********************************************************************/
/* DON T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPEAR AT ANY TIME */
/**********************************************************************/

//#define CULLING_REPARTITION

#endif
