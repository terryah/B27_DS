#ifndef CATVis2DModeType_H
#define CATVis2DModeType_H
/** @CAA2Required */
/**********************************************************************/
/* DON T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPEAR AT ANY TIME */
/**********************************************************************/

// COPYRIGHT DASSAULT SYSTEMES 2005

enum CATVis2DModeType { CATVis2DModeNotActivated=0, CATVis2DModeNoDisplay, CATVis2DModeLowLight, CATVis2DModeNoPick, CATVis2DModeLowLightNoPick};

#define CATVis2DModeNoShow CATVis2DModeNoDisplay

#endif //CATVis2DModeType_H
