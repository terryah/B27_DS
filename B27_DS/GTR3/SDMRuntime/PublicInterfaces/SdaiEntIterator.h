// COPYRIGHT DASSAULT SYSTEMES  2001
#ifndef	_SDAI_ENT_ITERATOR_H_
#define	_SDAI_ENT_ITERATOR_H_

// ------------------------------------------------------------------------------------------------
//  SDAI -- C++ Language Binding -- SdaiIterator
// ------------------------------------------------------------------------------------------------
// SDAI -- C++ Language Binding -- SdaiIterInstance -- Header
// ------------------------------------------------------------------------------------------------
// Auteurs :
//	PYR  (Novembre 1995)
// ------------------------------------------------------------------------------------------------
//  Historique :
//
//	MODIFICATION	: 01
//	DATE		: Nov 1995
//	AUTEUR		: PYR
//	NATURE		: Refonte complete de la version d'aout 93,suppression des 
//                        templates.  
//	DESCRIPTION	: Norme ISO 10303-22 (annexe D)
//
// ------------------------------------------------------------------------------------------------

/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */

#include <SdaiMacro.h>
#include  <SdaiIterInstance.h>

#include  <SdaiAggr_Of_SdaiEntityInstanceH.h> 

class SdaiLogical;

/** @nodoc */
#define SDAI_ENT_ITER_HEADER(TypeH)						SDAIITER_HEADER(TypeH,INSTANCE)
#endif /* _SDAI_ENT_ITERATOR_H_ */













