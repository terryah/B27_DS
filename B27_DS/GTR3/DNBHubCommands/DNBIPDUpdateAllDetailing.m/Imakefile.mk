# COPYRIGHT DASSAULT SYSTEMES 2003
#======================================================================
# Imakefile for module DNBIPDUpdateAllDetailing.m
#======================================================================
#
# cre  yms  10/23/2003  Original implementation
# mod  18-10-2012   H82  V5-6R2013 SP1: Function 026330 changes - DNBMHIBase added
#======================================================================

BUILT_OBJECT_TYPE=LOAD MODULE 
 
LINK_WITH = CATObjectModelerBase \
		JS0GROUP \
		JS0FM \
        	DNBMfgHubIntigrationCommands \ 
        	DNBMHIBase \ 

# System dependant variables
#
OS = AIX
BUILD=NO
#
OS = HP-UX
BUILD=NO
#
OS = IRIX
BUILD=NO
#
OS = SunOS
BUILD=NO
#
OS = Windows_NT
