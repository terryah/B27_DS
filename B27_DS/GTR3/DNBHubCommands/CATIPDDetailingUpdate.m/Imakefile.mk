# COPYRIGHT DASSAULT SYSTEMES 2001
#======================================================================
# Imakefile for module CATIPDDetailingUpdate.m
#======================================================================
#
#  July 2002  Creation: Code generated by the CAA wizard  ymenezes
#======================================================================
# mod  28-08-2013   H82  V5-6R2014: changes - to enable ODT on Windows 64bit OS
#======================================================================
#
# LOAD MODULE 
#
BUILT_OBJECT_TYPE=LOAD MODULE 
 
LINK_WITH = CATIPDAdapterItf \
			CATIPDBatchServices \
			CATObjectModelerBase \
			JS0GROUP \
			CATIPDAdapter \
            CATIPDProcess \ 
			

# System dependant variables
#
OS = AIX
BUILD=NO
#
OS = HP-UX
BUILD=NO
#
OS = IRIX
BUILD=NO
#
OS = SunOS
BUILD=NO
#
OS = Windows_NT
LOCAL_CCFLAGS =/I"$(MkmkMSVCDIR)\atl\include"
SYS_LIBS= atl.lib
#
OS = win_b64
#BUILD=NO
