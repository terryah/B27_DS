#======================================================================
# Imakefile for module CATWTTTesselationBase
#======================================================================
# 12/05/06 : CSO ; Archi WTT-CGM - Creation
# 18/07/06 : DGA ; Ajout CATBASLib et CATGDPLib dans MachinistAlgosWTTBase
# 21/12/07 : CTJ ; Ajout CATWTTAutomaticContour dans INCLUDED_MODULES  et 
#                  CATPolyhedralMathematicsDummy CATPolyhedralObjects CATPolyhedralInterfaces CATPolyhedralOperators en LINK_WITH
# 21/12/07 : CTJ ; Ajout CATPartAuto et CATINTIntersector dans INCLUDED_MODULES  et CATCloudBasicResources dans LINK_WITH pour le Part Auto
# 15/04/08 : TYF ; Ajout CATINTIntersector3D dans INCLUDED_MODULES
# 03/06/2009 : TYF : Retrait du module CATWTTAutomaticContour dans INCLUDED_MODULES  
#======================================================================
#

# Toolkit contenant les algos  de Base du WTT et de la SDM

BUILT_OBJECT_TYPE = SHARED LIBRARY


#Modules constitutifs.
INCLUDED_MODULES = CATEMCV5Base CATSDM_ManufDomain CATPartAuto CATINTIntersector3D CATToolsBase
          
             
OS = Windows_NT
LINK_WITH =  CATCGMGeoMath  CATMathStream  \
             JS0GROUP CATGeometricObjects CATTopologicalObjects  \
             CATMathematics \
             CATPolyhedralMathematicsDummy CATPolyhedralObjects CATPolyhedralInterfaces CATPolyhedralOperators 

	           
OS = AIX
SYS_LIBS = -lxlf -lxlf90 -lxlfpad
LINK_WITH =  CATCGMGeoMath  CATMathStream  \
             JS0GROUP CATGeometricObjects CATTopologicalObjects  \
             CATMathematics \
             CATPolyhedralMathematicsDummy CATPolyhedralObjects CATPolyhedralInterfaces CATPolyhedralOperators 


OS = IRIX
SYS_LIBS = -lftn
LINK_WITH =  CATCGMGeoMath  CATMathStream  \
             JS0GROUP CATGeometricObjects CATTopologicalObjects  \
             CATMathematics \
             CATPolyhedralMathematicsDummy CATPolyhedralObjects CATPolyhedralInterfaces CATPolyhedralOperators 


OS = HP-UX
#if os hpux_a
SYS_LIBS = -lf
#else
SYS_LIBS = -lF90
#endif
LINK_WITH =  CATCGMGeoMath  CATMathStream  \
             JS0GROUP CATGeometricObjects CATTopologicalObjects  \
             CATMathematics \
             CATPolyhedralMathematicsDummy CATPolyhedralObjects CATPolyhedralInterfaces CATPolyhedralOperators \
             CATCloudBasicResources


OS = SunOS
SYS_LIBS = -lF77 -lM77
LINK_WITH = JS0CORBA CATWTTesselationBase


             
