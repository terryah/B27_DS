# COPYRIGHT DASSAULT SYSTEMES 2004
#======================================================================
# Imakefile for fw CATDataExchControlIntf
#======================================================================
#
#  May 2004  Creation: Code generated by the CAA wizard  jfi
#======================================================================
#
# NONE 
#

BUILT_OBJECT_TYPE=SHARED LIBRARY

INCLUDED_MODULES = DECCAInterfaces DECCAServices

LINK_WITH=							   \
            CATConstraintModeler       \ # Module [CATConstraintModeler.m], which is define as an alias, was automatically added in LINK_WITH.
            CATConstraintModelerItf    \ # Module [CATConstraintModelerItf.m], which is define as an alias, was automatically added in LINK_WITH.
            CATMecModInterfaces        \ # Module [CATMecModInterfaces.m], which is define as an alias, was automatically added in LINK_WITH.
            CATObjectModelerBase       \ #OK
            CATObjectSpecsModeler      \ #OK
            CATVisualization           \ #OK
            CATViz                     \ #OK
            JS0GROUP                   \ #OK
            MF0GEOM                    \ #OK
     

# System dependant variables
#
OS = AIX
#
OS = HP-UX
#
OS = IRIX
#
OS = SunOS
#
OS = Windows_NT
