/**
 * @CAA2Level L0
 * @CAA2Usage U3
 */
// COPYRIGHT Dassault Systemes 2001
//===================================================================
//
// CATISilhouetteFactory.h
// Define the CATISilhouetteFactory interface
//
//===================================================================
//
// Usage notes:
//   New interface: describe its use here
//
//===================================================================
//
//  Oct 2001  Creation: Code generated by the CAA wizard  ABB
//===================================================================
#ifndef CATISilhouetteFactory_H
#define CATISilhouetteFactory_H

#include "SMTInterfacesItfCPP.h"
#include "CATBaseUnknown.h"
#include "CATListOfFloat.h"

class CATListValCATBaseUnknown_var;
class CATDocument;
class CATUnicodeString;

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedBySMTInterfacesItfCPP IID IID_CATISilhouetteFactory;
#else
extern "C" const IID IID_CATISilhouetteFactory ;
#endif

//------------------------------------------------------------------

/**
 * Describe the functionality of your interface here
 * <p>
 * Using this prefered syntax will enable mkdoc to document your class.
 */
class ExportedBySMTInterfacesItfCPP CATISilhouetteFactory: public CATBaseUnknown
{
  CATDeclareInterface;

  public:

    /**
     * Remember that interfaces define only virtual pure methods.
     * Dont forget to document your methods.
     * <dl>
     * <dt><b>Example:</b>
     * <pre>
     *       
           *   MyFunction does this and that
           *   @param Arg1
           *      The first argument of MyFunction.
           *   @return
           *      Error code of function.
           * 
     * virtual int MyFunction (int Arg1) = 0;
     * </pre>
     * </dl>
     */
     virtual HRESULT ComputeASilhouette(CATListValCATBaseUnknown_var listOfSelectedProducts,
          CATListOfFloat listOfViewPoints, double iAccuracy, double iAccuracyForSimplification,
          CATDocument*& SilhouetteDocument) = 0;

    /**
	 * This method is not available, only maybe V5R11
     */
     virtual HRESULT ComputeASilhouetteWithAReference(CATListValCATBaseUnknown_var listOfSelectedProducts,
         CATBaseUnknown* iReferenceProduct, CATListOfFloat listOfViewPoints, double iAccuracy, 
         double iAccuracyForSimplification, CATDocument*& SilhouetteDocument) = 0;


          /**
     * Remember that interfaces define only virtual pure methods.
     * Dont forget to document your methods.
     * <dl>
     * <dt><b>Example:</b>
     * <pre>
     *       
           *   MyFunction does this and that
           *   @param Arg1
           *      The first argument of MyFunction.
           *   @return
           *      Error code of function.
           * 
     * virtual int MyFunction (int Arg1) = 0;
     * </pre>
     * </dl>
     */
     virtual HRESULT SilhouetteShapeName(CATUnicodeString& Name) = 0;

     virtual HRESULT CleanUp() = 0;
};


CATDeclareHandler( CATISilhouetteFactory, CATBaseUnknown );
//------------------------------------------------------------------

#endif
