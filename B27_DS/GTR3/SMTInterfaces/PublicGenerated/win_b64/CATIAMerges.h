/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAMerges_h
#define CATIAMerges_h

#ifndef ExportedBySMTInterfacesPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __SMTInterfacesPubIDL
#define ExportedBySMTInterfacesPubIDL __declspec(dllexport)
#else
#define ExportedBySMTInterfacesPubIDL __declspec(dllimport)
#endif
#else
#define ExportedBySMTInterfacesPubIDL
#endif
#endif

#include "CATBSTR.h"
#include "CATIACollection.h"

class CATIADocument;
class CATIAGroup;

extern ExportedBySMTInterfacesPubIDL IID IID_CATIAMerges;

class ExportedBySMTInterfacesPubIDL CATIAMerges : public CATIACollection
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall ComputeMerge(CATIAGroup * GroupOfSelectedProducts, double iAccuracyForSimplification, CATLONG iKeepEdges, CATLONG iDecoration, CATIADocument *& MergeDocument)=0;

    virtual HRESULT __stdcall MergeShapeName(CATBSTR *& Name)=0;

    virtual HRESULT __stdcall CleanUp()=0;


};

CATDeclareHandler(CATIAMerges, CATIACollection);

#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIABase.h"
#include "CATIADocument.h"
#include "CATIAGroup.h"
#include "CATSafeArray.h"
#include "CATVariant.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
