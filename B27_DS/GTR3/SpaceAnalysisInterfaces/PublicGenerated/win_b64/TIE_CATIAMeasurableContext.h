#ifndef __TIE_CATIAMeasurableContext
#define __TIE_CATIAMeasurableContext

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATIAMeasurableContext.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface CATIAMeasurableContext */
#define declare_TIE_CATIAMeasurableContext(classe) \
 \
 \
class TIECATIAMeasurableContext##classe : public CATIAMeasurableContext \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(CATIAMeasurableContext, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall GetMinimumDistanceInContext(CATIAReference * iMeasuredItem, CATIABase * iProductInstance, double & oDistance); \
      virtual HRESULT __stdcall GetMinimumDistancePointsInContext(CATIAReference * iMeasuredItem, CATIABase * iProductInstance, CATSafeArrayVariant & oCoordinates); \
      virtual HRESULT __stdcall GetAngleBetweenInContext(CATIAReference * iMeasuredItem, CATIABase * iProductInstance, double & oAngle); \
      virtual HRESULT __stdcall get_GeometryName(CatMeasurableName & oGeometryName); \
      virtual HRESULT __stdcall get_Volume(double & oVolume); \
      virtual HRESULT __stdcall get_Area(double & oArea); \
      virtual HRESULT __stdcall GetCOG(CATSafeArrayVariant & oCoordinates); \
      virtual HRESULT __stdcall get_Perimeter(double & oPerimeter); \
      virtual HRESULT __stdcall GetPlane(CATSafeArrayVariant & oComponents); \
      virtual HRESULT __stdcall get_Radius(double & oRadius); \
      virtual HRESULT __stdcall GetCenter(CATSafeArrayVariant & oCoordinates); \
      virtual HRESULT __stdcall GetDirection(CATSafeArrayVariant & oDirection); \
      virtual HRESULT __stdcall GetAxis(CATSafeArrayVariant & oAxisVector); \
      virtual HRESULT __stdcall get_Angle(double & oAngle); \
      virtual HRESULT __stdcall get_Length(double & oLength); \
      virtual HRESULT __stdcall GetPointsOnCurve(CATSafeArrayVariant & oCoordinates); \
      virtual HRESULT __stdcall GetPointsOnAxis(CATSafeArrayVariant & oCoordinates); \
      virtual HRESULT __stdcall GetPoint(CATSafeArrayVariant & oCoordinates); \
      virtual HRESULT __stdcall GetAxisSystem(CATSafeArrayVariant & oComponents); \
      virtual HRESULT __stdcall GetMinimumDistance(CATIAReference * iMeasuredItem, double & oDistance); \
      virtual HRESULT __stdcall GetMinimumDistancePoints(CATIAReference * iMeasuredItem, CATSafeArrayVariant & oCoordinates); \
      virtual HRESULT __stdcall GetAngleBetween(CATIAReference * iMeasuredItem, double & oAngle); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_CATIAMeasurableContext(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall GetMinimumDistanceInContext(CATIAReference * iMeasuredItem, CATIABase * iProductInstance, double & oDistance); \
virtual HRESULT __stdcall GetMinimumDistancePointsInContext(CATIAReference * iMeasuredItem, CATIABase * iProductInstance, CATSafeArrayVariant & oCoordinates); \
virtual HRESULT __stdcall GetAngleBetweenInContext(CATIAReference * iMeasuredItem, CATIABase * iProductInstance, double & oAngle); \
virtual HRESULT __stdcall get_GeometryName(CatMeasurableName & oGeometryName); \
virtual HRESULT __stdcall get_Volume(double & oVolume); \
virtual HRESULT __stdcall get_Area(double & oArea); \
virtual HRESULT __stdcall GetCOG(CATSafeArrayVariant & oCoordinates); \
virtual HRESULT __stdcall get_Perimeter(double & oPerimeter); \
virtual HRESULT __stdcall GetPlane(CATSafeArrayVariant & oComponents); \
virtual HRESULT __stdcall get_Radius(double & oRadius); \
virtual HRESULT __stdcall GetCenter(CATSafeArrayVariant & oCoordinates); \
virtual HRESULT __stdcall GetDirection(CATSafeArrayVariant & oDirection); \
virtual HRESULT __stdcall GetAxis(CATSafeArrayVariant & oAxisVector); \
virtual HRESULT __stdcall get_Angle(double & oAngle); \
virtual HRESULT __stdcall get_Length(double & oLength); \
virtual HRESULT __stdcall GetPointsOnCurve(CATSafeArrayVariant & oCoordinates); \
virtual HRESULT __stdcall GetPointsOnAxis(CATSafeArrayVariant & oCoordinates); \
virtual HRESULT __stdcall GetPoint(CATSafeArrayVariant & oCoordinates); \
virtual HRESULT __stdcall GetAxisSystem(CATSafeArrayVariant & oComponents); \
virtual HRESULT __stdcall GetMinimumDistance(CATIAReference * iMeasuredItem, double & oDistance); \
virtual HRESULT __stdcall GetMinimumDistancePoints(CATIAReference * iMeasuredItem, CATSafeArrayVariant & oCoordinates); \
virtual HRESULT __stdcall GetAngleBetween(CATIAReference * iMeasuredItem, double & oAngle); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_CATIAMeasurableContext(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::GetMinimumDistanceInContext(CATIAReference * iMeasuredItem, CATIABase * iProductInstance, double & oDistance) \
{ \
return (ENVTIECALL(CATIAMeasurableContext,ENVTIETypeLetter,ENVTIELetter)GetMinimumDistanceInContext(iMeasuredItem,iProductInstance,oDistance)); \
} \
HRESULT __stdcall  ENVTIEName::GetMinimumDistancePointsInContext(CATIAReference * iMeasuredItem, CATIABase * iProductInstance, CATSafeArrayVariant & oCoordinates) \
{ \
return (ENVTIECALL(CATIAMeasurableContext,ENVTIETypeLetter,ENVTIELetter)GetMinimumDistancePointsInContext(iMeasuredItem,iProductInstance,oCoordinates)); \
} \
HRESULT __stdcall  ENVTIEName::GetAngleBetweenInContext(CATIAReference * iMeasuredItem, CATIABase * iProductInstance, double & oAngle) \
{ \
return (ENVTIECALL(CATIAMeasurableContext,ENVTIETypeLetter,ENVTIELetter)GetAngleBetweenInContext(iMeasuredItem,iProductInstance,oAngle)); \
} \
HRESULT __stdcall  ENVTIEName::get_GeometryName(CatMeasurableName & oGeometryName) \
{ \
return (ENVTIECALL(CATIAMeasurableContext,ENVTIETypeLetter,ENVTIELetter)get_GeometryName(oGeometryName)); \
} \
HRESULT __stdcall  ENVTIEName::get_Volume(double & oVolume) \
{ \
return (ENVTIECALL(CATIAMeasurableContext,ENVTIETypeLetter,ENVTIELetter)get_Volume(oVolume)); \
} \
HRESULT __stdcall  ENVTIEName::get_Area(double & oArea) \
{ \
return (ENVTIECALL(CATIAMeasurableContext,ENVTIETypeLetter,ENVTIELetter)get_Area(oArea)); \
} \
HRESULT __stdcall  ENVTIEName::GetCOG(CATSafeArrayVariant & oCoordinates) \
{ \
return (ENVTIECALL(CATIAMeasurableContext,ENVTIETypeLetter,ENVTIELetter)GetCOG(oCoordinates)); \
} \
HRESULT __stdcall  ENVTIEName::get_Perimeter(double & oPerimeter) \
{ \
return (ENVTIECALL(CATIAMeasurableContext,ENVTIETypeLetter,ENVTIELetter)get_Perimeter(oPerimeter)); \
} \
HRESULT __stdcall  ENVTIEName::GetPlane(CATSafeArrayVariant & oComponents) \
{ \
return (ENVTIECALL(CATIAMeasurableContext,ENVTIETypeLetter,ENVTIELetter)GetPlane(oComponents)); \
} \
HRESULT __stdcall  ENVTIEName::get_Radius(double & oRadius) \
{ \
return (ENVTIECALL(CATIAMeasurableContext,ENVTIETypeLetter,ENVTIELetter)get_Radius(oRadius)); \
} \
HRESULT __stdcall  ENVTIEName::GetCenter(CATSafeArrayVariant & oCoordinates) \
{ \
return (ENVTIECALL(CATIAMeasurableContext,ENVTIETypeLetter,ENVTIELetter)GetCenter(oCoordinates)); \
} \
HRESULT __stdcall  ENVTIEName::GetDirection(CATSafeArrayVariant & oDirection) \
{ \
return (ENVTIECALL(CATIAMeasurableContext,ENVTIETypeLetter,ENVTIELetter)GetDirection(oDirection)); \
} \
HRESULT __stdcall  ENVTIEName::GetAxis(CATSafeArrayVariant & oAxisVector) \
{ \
return (ENVTIECALL(CATIAMeasurableContext,ENVTIETypeLetter,ENVTIELetter)GetAxis(oAxisVector)); \
} \
HRESULT __stdcall  ENVTIEName::get_Angle(double & oAngle) \
{ \
return (ENVTIECALL(CATIAMeasurableContext,ENVTIETypeLetter,ENVTIELetter)get_Angle(oAngle)); \
} \
HRESULT __stdcall  ENVTIEName::get_Length(double & oLength) \
{ \
return (ENVTIECALL(CATIAMeasurableContext,ENVTIETypeLetter,ENVTIELetter)get_Length(oLength)); \
} \
HRESULT __stdcall  ENVTIEName::GetPointsOnCurve(CATSafeArrayVariant & oCoordinates) \
{ \
return (ENVTIECALL(CATIAMeasurableContext,ENVTIETypeLetter,ENVTIELetter)GetPointsOnCurve(oCoordinates)); \
} \
HRESULT __stdcall  ENVTIEName::GetPointsOnAxis(CATSafeArrayVariant & oCoordinates) \
{ \
return (ENVTIECALL(CATIAMeasurableContext,ENVTIETypeLetter,ENVTIELetter)GetPointsOnAxis(oCoordinates)); \
} \
HRESULT __stdcall  ENVTIEName::GetPoint(CATSafeArrayVariant & oCoordinates) \
{ \
return (ENVTIECALL(CATIAMeasurableContext,ENVTIETypeLetter,ENVTIELetter)GetPoint(oCoordinates)); \
} \
HRESULT __stdcall  ENVTIEName::GetAxisSystem(CATSafeArrayVariant & oComponents) \
{ \
return (ENVTIECALL(CATIAMeasurableContext,ENVTIETypeLetter,ENVTIELetter)GetAxisSystem(oComponents)); \
} \
HRESULT __stdcall  ENVTIEName::GetMinimumDistance(CATIAReference * iMeasuredItem, double & oDistance) \
{ \
return (ENVTIECALL(CATIAMeasurableContext,ENVTIETypeLetter,ENVTIELetter)GetMinimumDistance(iMeasuredItem,oDistance)); \
} \
HRESULT __stdcall  ENVTIEName::GetMinimumDistancePoints(CATIAReference * iMeasuredItem, CATSafeArrayVariant & oCoordinates) \
{ \
return (ENVTIECALL(CATIAMeasurableContext,ENVTIETypeLetter,ENVTIELetter)GetMinimumDistancePoints(iMeasuredItem,oCoordinates)); \
} \
HRESULT __stdcall  ENVTIEName::GetAngleBetween(CATIAReference * iMeasuredItem, double & oAngle) \
{ \
return (ENVTIECALL(CATIAMeasurableContext,ENVTIETypeLetter,ENVTIELetter)GetAngleBetween(iMeasuredItem,oAngle)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(CATIAMeasurableContext,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(CATIAMeasurableContext,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(CATIAMeasurableContext,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(CATIAMeasurableContext,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(CATIAMeasurableContext,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_CATIAMeasurableContext(classe)    TIECATIAMeasurableContext##classe


/* Common methods inside a TIE */
#define common_TIE_CATIAMeasurableContext(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(CATIAMeasurableContext, classe) \
 \
 \
CATImplementTIEMethods(CATIAMeasurableContext, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(CATIAMeasurableContext, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(CATIAMeasurableContext, classe) \
CATImplementCATBaseUnknownMethodsForTIE(CATIAMeasurableContext, classe) \
 \
HRESULT __stdcall  TIECATIAMeasurableContext##classe::GetMinimumDistanceInContext(CATIAReference * iMeasuredItem, CATIABase * iProductInstance, double & oDistance) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetMinimumDistanceInContext(iMeasuredItem,iProductInstance,oDistance)); \
} \
HRESULT __stdcall  TIECATIAMeasurableContext##classe::GetMinimumDistancePointsInContext(CATIAReference * iMeasuredItem, CATIABase * iProductInstance, CATSafeArrayVariant & oCoordinates) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetMinimumDistancePointsInContext(iMeasuredItem,iProductInstance,oCoordinates)); \
} \
HRESULT __stdcall  TIECATIAMeasurableContext##classe::GetAngleBetweenInContext(CATIAReference * iMeasuredItem, CATIABase * iProductInstance, double & oAngle) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAngleBetweenInContext(iMeasuredItem,iProductInstance,oAngle)); \
} \
HRESULT __stdcall  TIECATIAMeasurableContext##classe::get_GeometryName(CatMeasurableName & oGeometryName) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_GeometryName(oGeometryName)); \
} \
HRESULT __stdcall  TIECATIAMeasurableContext##classe::get_Volume(double & oVolume) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Volume(oVolume)); \
} \
HRESULT __stdcall  TIECATIAMeasurableContext##classe::get_Area(double & oArea) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Area(oArea)); \
} \
HRESULT __stdcall  TIECATIAMeasurableContext##classe::GetCOG(CATSafeArrayVariant & oCoordinates) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetCOG(oCoordinates)); \
} \
HRESULT __stdcall  TIECATIAMeasurableContext##classe::get_Perimeter(double & oPerimeter) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Perimeter(oPerimeter)); \
} \
HRESULT __stdcall  TIECATIAMeasurableContext##classe::GetPlane(CATSafeArrayVariant & oComponents) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetPlane(oComponents)); \
} \
HRESULT __stdcall  TIECATIAMeasurableContext##classe::get_Radius(double & oRadius) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Radius(oRadius)); \
} \
HRESULT __stdcall  TIECATIAMeasurableContext##classe::GetCenter(CATSafeArrayVariant & oCoordinates) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetCenter(oCoordinates)); \
} \
HRESULT __stdcall  TIECATIAMeasurableContext##classe::GetDirection(CATSafeArrayVariant & oDirection) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetDirection(oDirection)); \
} \
HRESULT __stdcall  TIECATIAMeasurableContext##classe::GetAxis(CATSafeArrayVariant & oAxisVector) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAxis(oAxisVector)); \
} \
HRESULT __stdcall  TIECATIAMeasurableContext##classe::get_Angle(double & oAngle) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Angle(oAngle)); \
} \
HRESULT __stdcall  TIECATIAMeasurableContext##classe::get_Length(double & oLength) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Length(oLength)); \
} \
HRESULT __stdcall  TIECATIAMeasurableContext##classe::GetPointsOnCurve(CATSafeArrayVariant & oCoordinates) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetPointsOnCurve(oCoordinates)); \
} \
HRESULT __stdcall  TIECATIAMeasurableContext##classe::GetPointsOnAxis(CATSafeArrayVariant & oCoordinates) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetPointsOnAxis(oCoordinates)); \
} \
HRESULT __stdcall  TIECATIAMeasurableContext##classe::GetPoint(CATSafeArrayVariant & oCoordinates) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetPoint(oCoordinates)); \
} \
HRESULT __stdcall  TIECATIAMeasurableContext##classe::GetAxisSystem(CATSafeArrayVariant & oComponents) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAxisSystem(oComponents)); \
} \
HRESULT __stdcall  TIECATIAMeasurableContext##classe::GetMinimumDistance(CATIAReference * iMeasuredItem, double & oDistance) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetMinimumDistance(iMeasuredItem,oDistance)); \
} \
HRESULT __stdcall  TIECATIAMeasurableContext##classe::GetMinimumDistancePoints(CATIAReference * iMeasuredItem, CATSafeArrayVariant & oCoordinates) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetMinimumDistancePoints(iMeasuredItem,oCoordinates)); \
} \
HRESULT __stdcall  TIECATIAMeasurableContext##classe::GetAngleBetween(CATIAReference * iMeasuredItem, double & oAngle) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAngleBetween(iMeasuredItem,oAngle)); \
} \
HRESULT  __stdcall  TIECATIAMeasurableContext##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication)); \
} \
HRESULT  __stdcall  TIECATIAMeasurableContext##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent)); \
} \
HRESULT  __stdcall  TIECATIAMeasurableContext##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  TIECATIAMeasurableContext##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  TIECATIAMeasurableContext##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj)); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_CATIAMeasurableContext(classe) \
 \
 \
declare_TIE_CATIAMeasurableContext(classe) \
 \
 \
CATMetaClass * __stdcall TIECATIAMeasurableContext##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_CATIAMeasurableContext,"CATIAMeasurableContext",CATIAMeasurableContext::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_CATIAMeasurableContext(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(CATIAMeasurableContext, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicCATIAMeasurableContext##classe(classe::MetaObject(),CATIAMeasurableContext::MetaObject(),(void *)CreateTIECATIAMeasurableContext##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_CATIAMeasurableContext(classe) \
 \
 \
declare_TIE_CATIAMeasurableContext(classe) \
 \
 \
CATMetaClass * __stdcall TIECATIAMeasurableContext##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_CATIAMeasurableContext,"CATIAMeasurableContext",CATIAMeasurableContext::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_CATIAMeasurableContext(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(CATIAMeasurableContext, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicCATIAMeasurableContext##classe(classe::MetaObject(),CATIAMeasurableContext::MetaObject(),(void *)CreateTIECATIAMeasurableContext##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_CATIAMeasurableContext(classe) TIE_CATIAMeasurableContext(classe)
#else
#define BOA_CATIAMeasurableContext(classe) CATImplementBOA(CATIAMeasurableContext, classe)
#endif

#endif
