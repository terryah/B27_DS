/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIADistance_h
#define CATIADistance_h

#ifndef ExportedBySPAPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __SPAPubIDL
#define ExportedBySPAPubIDL __declspec(dllexport)
#else
#define ExportedBySPAPubIDL __declspec(dllimport)
#endif
#else
#define ExportedBySPAPubIDL
#endif
#endif

#include "CATIABase.h"
#include "CATSafeArray.h"
#include "CatDistanceComputationType.h"
#include "CatDistanceMeasureType.h"

class CATIAAnnotatedViews;
class CATIAGroup;
class CATIAMarker3Ds;
class CATIAProduct;

extern ExportedBySPAPubIDL IID IID_CATIADistance;

class ExportedBySPAPubIDL CATIADistance : public CATIABase
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_ComputationType(CatDistanceComputationType & oType)=0;

    virtual HRESULT __stdcall put_ComputationType(CatDistanceComputationType iType)=0;

    virtual HRESULT __stdcall get_MeasureType(CatDistanceMeasureType & oType)=0;

    virtual HRESULT __stdcall put_MeasureType(CatDistanceMeasureType iType)=0;

    virtual HRESULT __stdcall get_MinimumDistance(double & oMinimumDistance)=0;

    virtual HRESULT __stdcall put_MinimumDistance(double iMinimumDistance)=0;

    virtual HRESULT __stdcall get_MaximumDistance(double & oMaximumDistance)=0;

    virtual HRESULT __stdcall put_MaximumDistance(double iMaximumDistance)=0;

    virtual HRESULT __stdcall get_Accuracy(double & oAccuracy)=0;

    virtual HRESULT __stdcall put_Accuracy(double iAccuracy)=0;

    virtual HRESULT __stdcall get_FirstGroup(CATIAGroup *& oGroup)=0;

    virtual HRESULT __stdcall put_FirstGroup(CATIAGroup * iGroup)=0;

    virtual HRESULT __stdcall get_SecondGroup(CATIAGroup *& oGroup)=0;

    virtual HRESULT __stdcall put_SecondGroup(CATIAGroup * iGroup)=0;

    virtual HRESULT __stdcall Compute()=0;

    virtual HRESULT __stdcall get_IsDefined(CATLONG & oDiagnosis)=0;

    virtual HRESULT __stdcall get_FirstProduct(CATIAProduct *& oProduct)=0;

    virtual HRESULT __stdcall GetFirstPointCoordinates(CATSafeArrayVariant & oCoordinates)=0;

    virtual HRESULT __stdcall get_SecondProduct(CATIAProduct *& oProduct)=0;

    virtual HRESULT __stdcall GetSecondPointCoordinates(CATSafeArrayVariant & oCoordinates)=0;

    virtual HRESULT __stdcall get_Value(double & oValue)=0;

    virtual HRESULT __stdcall get_AnnotatedViews(CATIAAnnotatedViews *& oAnnotatedViews)=0;

    virtual HRESULT __stdcall get_Marker3Ds(CATIAMarker3Ds *& oMarker3Ds)=0;


};

CATDeclareHandler(CATIADistance, CATIABase);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIAAnnotatedView.h"
#include "CATIAAnnotatedViews.h"
#include "CATIACollection.h"
#include "CATIAGroup.h"
#include "CATIAMarker2D.h"
#include "CATIAMarker2Ds.h"
#include "CATIAMarker3D.h"
#include "CATIAMarker3Ds.h"
#include "CATIAViewer.h"
#include "CATIAViewer3D.h"
#include "CATIAViewpoint3D.h"
#include "CATVariant.h"
#include "CatAnnotatedViewBehavior.h"
#include "CatCaptureFormat.h"
#include "CatClippingMode.h"
#include "CatLightingMode.h"
#include "CatMarker2DType.h"
#include "CatMarker3DType.h"
#include "CatMarkerTextOrientation.h"
#include "CatNavigationStyle.h"
#include "CatProjectionMode.h"
#include "CatRenderingMode.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
