/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAClashResults_h
#define CATIAClashResults_h

#ifndef ExportedBySPAPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __SPAPubIDL
#define ExportedBySPAPubIDL __declspec(dllexport)
#else
#define ExportedBySPAPubIDL __declspec(dllimport)
#endif
#else
#define ExportedBySPAPubIDL
#endif
#endif

#include "CATBSTR.h"
#include "CATIACollection.h"
#include "CATVariant.h"
#include "CatClashImportType.h"

class CATIAClashResult;

extern ExportedBySPAPubIDL IID IID_CATIAClashResults;

class ExportedBySPAPubIDL CATIAClashResults : public CATIACollection
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall AddFromXML(const CATBSTR & iPath, CatClashImportType iType, CATIAClashResult *& oClashResult)=0;

    virtual HRESULT __stdcall Item(const CATVariant & iIndex, CATIAClashResult *& oClashResult)=0;

    virtual HRESULT __stdcall Remove(const CATVariant & iIndex)=0;


};

CATDeclareHandler(CATIAClashResults, CATIACollection);

#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIAAnalyze.h"
#include "CATIABase.h"
#include "CATIAClashResult.h"
#include "CATIAConflict.h"
#include "CATIAConflicts.h"
#include "CATIAMove.h"
#include "CATIAParameters.h"
#include "CATIAPosition.h"
#include "CATIAProduct.h"
#include "CATIAPublications.h"
#include "CATSafeArray.h"
#include "CatClashExportType.h"
#include "CatConflictComparison.h"
#include "CatConflictStatus.h"
#include "CatConflictType.h"
#include "CatFileType.h"
#include "CatProductSource.h"
#include "CatRepType.h"
#include "CatWorkModeType.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
