/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIASPAWorkbench_h
#define CATIASPAWorkbench_h

#ifndef ExportedBySPAPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __SPAPubIDL
#define ExportedBySPAPubIDL __declspec(dllexport)
#else
#define ExportedBySPAPubIDL __declspec(dllimport)
#endif
#else
#define ExportedBySPAPubIDL
#endif
#endif

#include "CATIAWorkbench.h"

class CATIAClashes;
class CATIADistances;
class CATIAInertias;
class CATIAMeasurable;
class CATIAReference;
class CATIASections;

extern ExportedBySPAPubIDL IID IID_CATIASPAWorkbench;

class ExportedBySPAPubIDL CATIASPAWorkbench : public CATIAWorkbench
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall GetMeasurable(CATIAReference * iMeasuredItem, CATIAMeasurable *& oMeasurable)=0;

    virtual HRESULT __stdcall get_Inertias(CATIAInertias *& oInertias)=0;

    virtual HRESULT __stdcall get_Clashes(CATIAClashes *& oClashes)=0;

    virtual HRESULT __stdcall get_Sections(CATIASections *& oSections)=0;

    virtual HRESULT __stdcall get_Distances(CATIADistances *& oDistances)=0;


};

CATDeclareHandler(CATIASPAWorkbench, CATIAWorkbench);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIAAnalyze.h"
#include "CATIAAnnotatedView.h"
#include "CATIAAnnotatedViews.h"
#include "CATIABase.h"
#include "CATIAClash.h"
#include "CATIAClashes.h"
#include "CATIACollection.h"
#include "CATIAConflict.h"
#include "CATIAConflicts.h"
#include "CATIADistance.h"
#include "CATIADistances.h"
#include "CATIADocument.h"
#include "CATIAGroup.h"
#include "CATIAInertia.h"
#include "CATIAInertias.h"
#include "CATIAMarker2D.h"
#include "CATIAMarker2Ds.h"
#include "CATIAMarker3D.h"
#include "CATIAMarker3Ds.h"
#include "CATIAMeasurable.h"
#include "CATIAMove.h"
#include "CATIAParameters.h"
#include "CATIAPosition.h"
#include "CATIAProduct.h"
#include "CATIAPublications.h"
#include "CATIAReference.h"
#include "CATIASection.h"
#include "CATIASections.h"
#include "CATIAViewer.h"
#include "CATIAViewer3D.h"
#include "CATIAViewpoint3D.h"
#include "CATSafeArray.h"
#include "CATVariant.h"
#include "CatAnnotatedViewBehavior.h"
#include "CatCaptureFormat.h"
#include "CatClashComputationType.h"
#include "CatClashExportType.h"
#include "CatClashInterferenceType.h"
#include "CatClippingMode.h"
#include "CatConflictComparison.h"
#include "CatConflictStatus.h"
#include "CatConflictType.h"
#include "CatDistanceComputationType.h"
#include "CatDistanceMeasureType.h"
#include "CatFileType.h"
#include "CatLightingMode.h"
#include "CatMarker2DType.h"
#include "CatMarker3DType.h"
#include "CatMarkerTextOrientation.h"
#include "CatNavigationStyle.h"
#include "CatProductSource.h"
#include "CatProjectionMode.h"
#include "CatRenderingMode.h"
#include "CatRepType.h"
#include "CatSectionBehavior.h"
#include "CatSectionType.h"
#include "CatWorkModeType.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
