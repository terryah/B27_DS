#ifndef __TIE_CATIASPAWorkbenchInContext
#define __TIE_CATIASPAWorkbenchInContext

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "CATIASPAWorkbenchInContext.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface CATIASPAWorkbenchInContext */
#define declare_TIE_CATIASPAWorkbenchInContext(classe) \
 \
 \
class TIECATIASPAWorkbenchInContext##classe : public CATIASPAWorkbenchInContext \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(CATIASPAWorkbenchInContext, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall GetMeasurableInContext(CATIAReference * iMeasuredItem, CATIABase * iProductInstance, CATIAMeasurableContext *& oMeasurable); \
      virtual HRESULT __stdcall GetMeasurable(CATIAReference * iMeasuredItem, CATIAMeasurable *& oMeasurable); \
      virtual HRESULT __stdcall get_Inertias(CATIAInertias *& oInertias); \
      virtual HRESULT __stdcall get_Clashes(CATIAClashes *& oClashes); \
      virtual HRESULT __stdcall get_Sections(CATIASections *& oSections); \
      virtual HRESULT __stdcall get_Distances(CATIADistances *& oDistances); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_CATIASPAWorkbenchInContext(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall GetMeasurableInContext(CATIAReference * iMeasuredItem, CATIABase * iProductInstance, CATIAMeasurableContext *& oMeasurable); \
virtual HRESULT __stdcall GetMeasurable(CATIAReference * iMeasuredItem, CATIAMeasurable *& oMeasurable); \
virtual HRESULT __stdcall get_Inertias(CATIAInertias *& oInertias); \
virtual HRESULT __stdcall get_Clashes(CATIAClashes *& oClashes); \
virtual HRESULT __stdcall get_Sections(CATIASections *& oSections); \
virtual HRESULT __stdcall get_Distances(CATIADistances *& oDistances); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_CATIASPAWorkbenchInContext(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::GetMeasurableInContext(CATIAReference * iMeasuredItem, CATIABase * iProductInstance, CATIAMeasurableContext *& oMeasurable) \
{ \
return (ENVTIECALL(CATIASPAWorkbenchInContext,ENVTIETypeLetter,ENVTIELetter)GetMeasurableInContext(iMeasuredItem,iProductInstance,oMeasurable)); \
} \
HRESULT __stdcall  ENVTIEName::GetMeasurable(CATIAReference * iMeasuredItem, CATIAMeasurable *& oMeasurable) \
{ \
return (ENVTIECALL(CATIASPAWorkbenchInContext,ENVTIETypeLetter,ENVTIELetter)GetMeasurable(iMeasuredItem,oMeasurable)); \
} \
HRESULT __stdcall  ENVTIEName::get_Inertias(CATIAInertias *& oInertias) \
{ \
return (ENVTIECALL(CATIASPAWorkbenchInContext,ENVTIETypeLetter,ENVTIELetter)get_Inertias(oInertias)); \
} \
HRESULT __stdcall  ENVTIEName::get_Clashes(CATIAClashes *& oClashes) \
{ \
return (ENVTIECALL(CATIASPAWorkbenchInContext,ENVTIETypeLetter,ENVTIELetter)get_Clashes(oClashes)); \
} \
HRESULT __stdcall  ENVTIEName::get_Sections(CATIASections *& oSections) \
{ \
return (ENVTIECALL(CATIASPAWorkbenchInContext,ENVTIETypeLetter,ENVTIELetter)get_Sections(oSections)); \
} \
HRESULT __stdcall  ENVTIEName::get_Distances(CATIADistances *& oDistances) \
{ \
return (ENVTIECALL(CATIASPAWorkbenchInContext,ENVTIETypeLetter,ENVTIELetter)get_Distances(oDistances)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(CATIASPAWorkbenchInContext,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(CATIASPAWorkbenchInContext,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(CATIASPAWorkbenchInContext,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(CATIASPAWorkbenchInContext,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(CATIASPAWorkbenchInContext,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_CATIASPAWorkbenchInContext(classe)    TIECATIASPAWorkbenchInContext##classe


/* Common methods inside a TIE */
#define common_TIE_CATIASPAWorkbenchInContext(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(CATIASPAWorkbenchInContext, classe) \
 \
 \
CATImplementTIEMethods(CATIASPAWorkbenchInContext, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(CATIASPAWorkbenchInContext, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(CATIASPAWorkbenchInContext, classe) \
CATImplementCATBaseUnknownMethodsForTIE(CATIASPAWorkbenchInContext, classe) \
 \
HRESULT __stdcall  TIECATIASPAWorkbenchInContext##classe::GetMeasurableInContext(CATIAReference * iMeasuredItem, CATIABase * iProductInstance, CATIAMeasurableContext *& oMeasurable) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&iMeasuredItem,&iProductInstance,&oMeasurable); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetMeasurableInContext(iMeasuredItem,iProductInstance,oMeasurable); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&iMeasuredItem,&iProductInstance,&oMeasurable); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIECATIASPAWorkbenchInContext##classe::GetMeasurable(CATIAReference * iMeasuredItem, CATIAMeasurable *& oMeasurable) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&iMeasuredItem,&oMeasurable); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetMeasurable(iMeasuredItem,oMeasurable); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&iMeasuredItem,&oMeasurable); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIECATIASPAWorkbenchInContext##classe::get_Inertias(CATIAInertias *& oInertias) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&oInertias); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Inertias(oInertias); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&oInertias); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIECATIASPAWorkbenchInContext##classe::get_Clashes(CATIAClashes *& oClashes) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&oClashes); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Clashes(oClashes); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&oClashes); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIECATIASPAWorkbenchInContext##classe::get_Sections(CATIASections *& oSections) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&oSections); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Sections(oSections); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&oSections); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIECATIASPAWorkbenchInContext##classe::get_Distances(CATIADistances *& oDistances) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&oDistances); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Distances(oDistances); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&oDistances); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIECATIASPAWorkbenchInContext##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIECATIASPAWorkbenchInContext##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIECATIASPAWorkbenchInContext##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIECATIASPAWorkbenchInContext##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIECATIASPAWorkbenchInContext##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,11,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,11,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_CATIASPAWorkbenchInContext(classe) \
 \
 \
declare_TIE_CATIASPAWorkbenchInContext(classe) \
 \
 \
CATMetaClass * __stdcall TIECATIASPAWorkbenchInContext##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_CATIASPAWorkbenchInContext,"CATIASPAWorkbenchInContext",CATIASPAWorkbenchInContext::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_CATIASPAWorkbenchInContext(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(CATIASPAWorkbenchInContext, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicCATIASPAWorkbenchInContext##classe(classe::MetaObject(),CATIASPAWorkbenchInContext::MetaObject(),(void *)CreateTIECATIASPAWorkbenchInContext##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_CATIASPAWorkbenchInContext(classe) \
 \
 \
declare_TIE_CATIASPAWorkbenchInContext(classe) \
 \
 \
CATMetaClass * __stdcall TIECATIASPAWorkbenchInContext##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_CATIASPAWorkbenchInContext,"CATIASPAWorkbenchInContext",CATIASPAWorkbenchInContext::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_CATIASPAWorkbenchInContext(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(CATIASPAWorkbenchInContext, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicCATIASPAWorkbenchInContext##classe(classe::MetaObject(),CATIASPAWorkbenchInContext::MetaObject(),(void *)CreateTIECATIASPAWorkbenchInContext##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_CATIASPAWorkbenchInContext(classe) TIE_CATIASPAWorkbenchInContext(classe)
#else
#define BOA_CATIASPAWorkbenchInContext(classe) CATImplementBOA(CATIASPAWorkbenchInContext, classe)
#endif

#endif
