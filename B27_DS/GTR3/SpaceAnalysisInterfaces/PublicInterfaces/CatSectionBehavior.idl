// COPYRIGHT DASSAULT SYSTEMES 2002
//===================================================================
//
// CatSectionBehavior.idl
//   This automation declaration enumerates values of interface parameter.
//
//===================================================================
#ifndef CatSectionBehavior_IDL
#define CatSectionBehavior_IDL
/*IDLREP*/

/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */

/**
 * The different types of a section.
 * It is used by the @href CATIASection object to specify the section Behavior.
 * @param catSectionBehaviorManual
 *   The update of the section has to be done manually.
 *   The icon is changing to show that the section result is not updated.
 * @param catSectionBehaviorAutomatic
 *   The update of the section while a product is moving is done automatically.
 * @param catSectionBehaviorFreeze
 *   The section result is freezed. No result will be recalculated untill the user
 *   unfreeze it.
 */
enum CatSectionBehavior
{
  catSectionBehaviorManual,
  catSectionBehaviorAutomatic,
  catSectionBehaviorFreeze
};

#endif
