// COPYRIGHT Dassault Systemes 2009
//===================================================================
//
// CATIDMUUpdateDuringSimulation.h
// Define the CATIDMUUpdateDuringSimulation interface
//
//===================================================================
//
// Usage notes: 
//			Interface to allow use of "Light Transaction Mechanism" for update of DMU data during simulation.
//
//===================================================================
//
//  Jul 2009  Creation: Code generated by the CAA wizard  GZT
//===================================================================

/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */

#ifndef CATIDMUUpdateDuringSimulation_H
#define CATIDMUUpdateDuringSimulation_H

#include "SpaceAnalysisItf.h"
#include "CATBaseUnknown.h"

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedBySpaceAnalysisItf IID IID_CATIDMUUpdateDuringSimulation;
#else
extern "C" const IID IID_CATIDMUUpdateDuringSimulation ;
#endif

//------------------------------------------------------------------

/**
* This interface facilitates use of "Light Transaction Mechanism" to minimise memory consumption during update of DMU data.
* <p>
* This interface can be queried on a document.
* User should call BeginLightUpdateTransaction & EndLightUpdateTransaction at start & end of transaction.
*/
class ExportedBySpaceAnalysisItf CATIDMUUpdateDuringSimulation: public CATBaseUnknown
{
	CATDeclareInterface;

public:
	
	virtual HRESULT BeginLightUpdateTransaction () = 0;
	virtual HRESULT EndLightUpdateTransaction   () = 0; 
};

//------------------------------------------------------------------
CATDeclareHandler(CATIDMUUpdateDuringSimulation, CATBaseUnknown);

#endif
