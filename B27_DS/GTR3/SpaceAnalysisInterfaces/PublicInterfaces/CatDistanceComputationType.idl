// COPYRIGHT DASSAULT SYSTEMES 2000
//===================================================================
//
// CatDistanceComputationType.idl
//   This automation declaration enumerates values of interface parameter.
//
//===================================================================
#ifndef CatDistanceComputationType_IDL
#define CatDistanceComputationType_IDL
/*IDLREP*/

/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */

/**
 * The different types of clash computation.
 * It is used by the @href CATIADistance object to specify the computation type.
 * @param catDistanceComputationTypeInsideOne
 *   The computation tests each product of the selection/group against all other products in the same selection.
 * @param catDistanceComputationTypeAgainstAll
 *   The computation tests each product in the defined selection/group against all other products in the document.
 * @param catDistanceComputationTypeBetweenTwo
 *   The computation tests each product in the first selection/group against all products in the second group.
 */
enum CatDistanceComputationType
{
  catDistanceComputationTypeInsideOne,
  catDistanceComputationTypeAgainstAll,
  catDistanceComputationTypeBetweenTwo
};

#endif
