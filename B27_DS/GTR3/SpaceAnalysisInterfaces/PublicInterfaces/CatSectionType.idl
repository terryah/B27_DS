// COPYRIGHT DASSAULT SYSTEMES 2000
//===================================================================
//
// CatSectionType.idl
//   This automation declaration enumerates values of interface parameter.
//
//===================================================================
#ifndef CatSectionType_IDL
#define CatSectionType_IDL
/*IDLREP*/

/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */

/**
 * The different types of a section.
 * It is used by the @href CATIASection object to specify the section type.
 * @param catSectionTypePlane
 *   The section is a plane.
 * @param catSectionTypeSlice
 *   The section is made of two parallel planes separated by a thickness.
 * @param catSectionTypeBox
 *   The section is a box.
 */
enum CatSectionType
{
  catSectionTypePlane,
  catSectionTypeSlice,
  catSectionTypeBox
};

#endif
