//=============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
//=============================================================================
#ifndef _VPMDicTYPE_h
#define _VPMDicTYPE_h
/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */

/**
 * ENOVIA dictionary types.
 * @param VPMDicPRIMITIVE
 *   The ENOVIA dictionary object is an ENOVIA primitive
 * @param VPMDicCATEGORY
 *   The ENOVIA dictionary object is a category
 * @param VPMDicENUM
 *   The ENOVIA dictionary object is an enum
 * @param VPMDicSIMPLE
 *   The ENOVIA dictionary object is a simple type
 * @param VPMDicCLASS
 *   The ENOVIA dictionary object is a class
 * @param VPMDicRELATIONSHIP
 *   The ENOVIA dictionary object is a relationship
 * @param VPMDicINVERSE
 *   The ENOVIA dictionary object is an inverse
 * @param VPMDicINTERFACE
 *   The ENOVIA dictionary object is an interface
 * @param VPMDicMETHOD
 *   The ENOVIA dictionary object is a method
 * @param VPMDicPARAMETER
 *   The ENOVIA dictionary object is a parameter
 * @param VPMDicPACKAGE
 *   The ENOVIA dictionary object is a package
 * @param VPMDicBOBJECT
 *   The ENOVIA dictionary object is a business object
 * @param VPMDicBOLINK
 *   The ENOVIA dictionary object is a business link
 * @param VPMDicUNIQUE
 *   The ENOVIA dictionary object is unique
 * @param VPMDicCOMMAND
 *   The ENOVIA dictionary object is a command
 * @param VPMDicIMPLEMENT
 *   The ENOVIA dictionary object is an implementation
 * @param VPMDicSECURITY
 *   The ENOVIA dictionary object is a secutity
 * @param VPMDicCOMMANDPARAMETER
 *   The ENOVIA dictionary object is a command parameter
 * @param VPMDicENUMERATOR
 *   The ENOVIA dictionary object is an enumerator
 * @param VPMDicATTRIBUTEBLOCK
 *   The ENOVIA dictionary object is an attribute block
 * @param VPMDicRBOCLASS
 *   The ENOVIA dictionary object is an RBO class
 * @param VPMDicRBOMAPPING
 *   The ENOVIA dictionary object is an RBO mapping
 * @param VPMDicRBOROOT
 *   The ENOVIA dictionary object is an RBO root
 * @param VPMDicRBOCOMPONENT
 *   The ENOVIA dictionary object is an RBO component
 * @param VPMDicRBOPATHEXPRESSION
 *   The ENOVIA dictionary object is an RBO path expression
 */


enum VPMDicTYPE
{
     VPMDicPRIMITIVE         = 1
    ,VPMDicCATEGORY          = 2
    ,VPMDicENUM              = 3
    ,VPMDicSIMPLE            = 4
    ,VPMDicCLASS             = 5
    ,VPMDicRELATIONSHIP      = 6
    ,VPMDicINVERSE           = 7
    ,VPMDicINTERFACE         = 8
    ,VPMDicMETHOD            = 9
    ,VPMDicPARAMETER         = 10
    ,VPMDicPACKAGE           = 11
    ,VPMDicBOBJECT           = 12
    ,VPMDicEXTENSION         = 13
    ,VPMDicBOLINK            = 14
    ,VPMDicUNIQUE            = 15
    ,VPMDicCOMMAND           = 16
    ,VPMDicIMPLEMENT         = 17
    ,VPMDicSECURITY          = 18
    ,VPMDicCOMMANDPARAMETER  = 19
    ,VPMDicENUMERATOR        = 20
    ,VPMDicATTRIBUTEBLOCK    = 21
    ,VPMDicRBOCLASS          = 22
    ,VPMDicRBOMAPPING        = 23
    ,VPMDicRBOROOT           = 24
    ,VPMDicRBOCOMPONENT      = 25
    ,VPMDicRBOPATHEXPRESSION = 26
}
;

#endif

