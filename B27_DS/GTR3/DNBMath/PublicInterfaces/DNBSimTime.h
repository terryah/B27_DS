//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 */

//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     srv         02/19/99    Original code
//*	    sdh	        03/09/99	Productionize the code and add error free
//*				                division algorithm
//*	    sdh	        04/25/99	Modify the implementation of the class to
//*				                use the handle/body pattern
//*     bkh         11/05/03    Implementation of CAA style documentation.
//*		smw			01/07/05	Removed extractors due to iostream migration to the
//*								new, standard, templated iostream, and due to migration to 64 bit
//*								builds.  Extractor for all math classes have been moved, 
//*								for easy rescucitation, if necessary,
//*								to DNBMath.tst/PrivateInterfaces/DNBMathExtractors.h
//* 
//* 
#if DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _DNB_SIMTIME_H_
#define _DNB_SIMTIME_H_

#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>
#include <math.h>
#include <DNBMath.h>
#include <scl_iosfwd.h>         // smw; caa cleanup and restructuring 

#include <DNBException.h>


/**
  * The numeric representation of the simulation time.
  * 
  * <br><B>Description</B><br>
  * This class represents a representation of scalar time, which provides
  * nanosecond fixed point resolution over an entire range of +/- 2.9e+11
  * years.  This provides almost error free numerical stability over a
  * range of +/- 540000 years for most mathematical operations.
  * The DNBSimTime class is implemented using the handle/body pattern with
  * copy-on-write semantics (a body is shared by multiple handles, but when
  * one tries to modify the body, gets its own copy of body). The class is
  * thread-safe and possess referential integrity.
  * 
  * <br><B>Example</B><br>
  * <pre>
  * DNBSimTime t1( DNBReal( 17.0 ) );  // Initialize to 17 seconds
  * DNBSimTime t2( 15, 300000000 ); // Initialize to 15.3 seconds
  * DNBReal r1 = t1.seconds();
  * </pre>
  *
  * <br><B>Motivation</B><br>
  * Rough analysis of Deneb's problem domain called for nanosecod
  * resolution for many computational areas, including dynamics,
  * kinematics, and discrete event.  Discrete event also expressed a need
  * for a range of +/- 100 years while maintaining this resolution.  The
  * result is rounded up to the next power of 2^32.
  * 
  * <br><B>Warnings</B><br>
  * Although many efforts have been made to make this class as compatible
  * with floating point numbers as possible, its nature as a fixed point
  * type can cause inadvertant inconsistencies, particularly where
  * conversions into and out of floating point representations are
  * involved.  Whenever possible, perform assignments using the native
  * fixed point version of the constructor and perform other operations
  * against other DNBSimTime objects or integer scalars where appropriate.
  * 
  * 
  */
class ExportedByDNBMath DNBSimTime
{
public:
    enum
    {
	nsPerSec = 1000000000
    };

/**
  * Default constructor.
  * 
  * @return
  * Nothing.
  * 
  * <br><B>Description</B><br>
  * This function constructs <tt>self</tt> with a value of (0,0).
  * 
  * <br><B>Example</B><br>
  * <pre>
  * DNBSimTime t1;
  * DNB_ASSERT( t1 == 0.0 );
  * 
  * </pre>
  *
  */
    DNBSimTime()
	DNB_THROW_SPEC_NULL;

/**
  * Copy constructor.
  * @param  rhs 
  * Reference to the const object from which to copy.
  * 
  * @return
  * Nothing.
  * 
  * <br><B>Description</B><br>
  * This function copies <tt>rhs</tt> into <tt>self</tt>.
  * 
  * <br><B>Example</B><br>
  * <pre>
  * DNBSimTime t1( 15, 300000000 );
  * DNBSimTime t2( t1 );
  * 
  * </pre>
  *
  */
    DNBSimTime( const DNBSimTime &rhs )
	DNB_THROW_SPEC_NULL;

/**
  * Constructs from a floating point number.
  * @param  rhs 
  * The floating point number from which to initialize.
  * 
  * @return
  * Nothing.
  * 
  * <br><B>Description</B><br>
  * This function constructs <tt>self</tt> with the converted value of the
  * floating point number represented by <tt>rhs</tt>.
  * 
  * <br><B>Example</B><br>
  * <pre>
  * DNBSimTime t1( 17.0 );
  * 
  * </pre>
  *
  */
    DNBSimTime( DNBReal rhs )
	DNB_THROW_SPEC_NULL;

/**
  * Construct from integers for each of the parts.
  * @param  sec 
  * The integral number of seconds.
  * @param  nsec 
  * The integral number of nanoseconds.
  * 
  * @return
  * Nothing.
  * 
  * <br><B>Description</B><br>
  * This function initializes <tt>self</tt> with the values supplied. It throws
  * an exception if the <tt>nsec</tt> parameter exceedes the maximum number
  * of nanoseconds in a second.
  * @exception  DNBEDomainError 
  * The <tt>nsec</tt> parameter overflows.
  * 
  * <br><B>Example</B><br>
  * <pre>
  * DNBSimTime t1( 15, 300000000 ); // Initialize to 15.3 seconds
  * 
  * </pre>
  *
  */
    DNBSimTime( DNBInteger64 sec, DNBInteger32 nsec )
	DNB_THROW_SPEC((DNBEDomainError));

/**
  * Destructor.
  * 
  * @return
  * Nothing.
  * 
  * <br><B>Description</B><br>
  * This function destroys <tt>self</tt>.
  * 
  * 
  */
    ~DNBSimTime()
	DNB_THROW_SPEC_NULL;

/**
  * Set the value of <tt>self</tt> by components.
  * @param  sec 
  * The integral number of seconds.
  * @param  nsec 
  * The integral number of nanoseconds.
  * 
  * @return
  * Nothing.
  * 
  * <br><B>Description</B><br>
  * This function sets <tt>self</tt> to the values supplied. It throws
  * an exception if the <tt>nsec</tt> parameter exceedes the maximum number
  * of nanoseconds in a second.
  * @exception  DNBEDomainError 
  * The <tt>nsec</tt> parameter overflows.
  * 
  * <br><B>Example</B><br>
  * <pre>
  * DNBSimTime t1;
  * t1.set( 15, 300000000 );
  * 
  * </pre>
  *
  */
    void
    set( DNBInteger64 sec, DNBInteger32 nsec = 0 )
	DNB_THROW_SPEC((DNBEDomainError));

/**
  * Returns the real value of <self> where appropriate.
  * 
  * @return
  * The real value of <tt>self</tt>.
  * 
  * <br><B>Description</B><br>
  * This function returns the real value of <tt>self</tt>.
  * @exception  DNBEDomainError 
  * Specigy a nanosecond domain violation.
  * 
  * <br><B>Example</B><br>
  * <pre>
  * DNBSimTime t1( 15, 300000000 );
  * DNBReal r = t1.seconds();
  * 
  * </pre>
  *
  */
    inline DNBReal
    seconds() const
	DNB_THROW_SPEC_NULL;

/**
  * Assignment operator.
  * @param  rhs 
  * Const reference to the right hand side.
  * 
  * @return
  * The reference to <tt>self</tt> after assignment.
  * 
  * <br><B>Description</B><br>
  * This function assigns <tt>rhs</tt> to <tt>self</tt>.
  * 
  * <br><B>Example</B><br>
  * <pre>
  * DNBSimTime t1( 17, 0 );
  * DNBSimTime t2;
  * t2 = t1;
  * 
  * </pre>
  *
  */
    inline DNBSimTime &
    operator=( const DNBSimTime &rhs )
	DNB_THROW_SPEC_NULL;

/**
  * Additive accumulation operator.
  * 
  * 
  */
    inline DNBSimTime &
    operator+=( const DNBSimTime &rhs )
	DNB_THROW_SPEC_NULL;

/**
  * Subtractive accumulation operator.
  * 
  * 
  */
    inline DNBSimTime &
    operator-=( const DNBSimTime &rhs )
	DNB_THROW_SPEC_NULL;

/**
  * Multiplicative accumulation operator.
  * 
  * 
  */
    DNBSimTime &
    operator*=( const DNBSimTime &rhs )
	DNB_THROW_SPEC_NULL;

/**
  * Divisive accumulation operator.
  * @exception  DNBEZeroDivide 
  * Division by zero.
  * @exception  DNBEOverflowError 
  * Division overflow.
  * 
  * 
  */
    DNBSimTime &
    operator/=( const DNBSimTime &rhs )
	DNB_THROW_SPEC((DNBEZeroDivide, DNBEOverflowError));

/**
  * Pre-increment operator.
  * 
  * 
  */
    inline DNBSimTime &
    operator++()
	DNB_THROW_SPEC_NULL;

/**
  * Post-increment operator.
  * 
  * 
  */
    inline DNBSimTime
    operator++( int )
	DNB_THROW_SPEC_NULL;

/**
  * Pre-decrement operator.
  * 
  * 
  */
    inline DNBSimTime &
    operator--()
	DNB_THROW_SPEC_NULL;

/**
  * Post-decrement operator.
  * 
  * 
  */
    inline DNBSimTime
    operator--( int )
	DNB_THROW_SPEC_NULL;

/**
  * Unary plus operator.
  * 
  * 
  */
    inline DNBSimTime
    operator+()
	DNB_THROW_SPEC_NULL;

/**
  * Negation operator.
  * 
  * 
  */
    inline DNBSimTime
    operator-()
	DNB_THROW_SPEC_NULL;

/**
  * Addition operator.
  * 
  * 
  */
    friend inline ExportedByDNBMath
    DNBSimTime
    operator+( const DNBSimTime &lhs, const DNBSimTime &rhs )
	DNB_THROW_SPEC_NULL;

/**
  * Subtraction operator.
  * 
  * 
  */
    friend inline ExportedByDNBMath
    DNBSimTime
    operator-( const DNBSimTime &lhs, const DNBSimTime &rhs )
	DNB_THROW_SPEC_NULL;

/**
  * Multiplication operator.
  * 
  * 
  */
    friend inline ExportedByDNBMath
    DNBSimTime
    operator*( const DNBSimTime &lhs, const DNBSimTime &rhs )
	DNB_THROW_SPEC_NULL;

/**
  * Division operator.
  * @exception  DNBEZeroDivide 
  * Division by zero.
  * @exception  DNBEOverflowError 
  * Division overflow.
  * 
  * 
  */
    friend inline ExportedByDNBMath
    DNBSimTime
    operator/( const DNBSimTime &lhs, const DNBSimTime &rhs )
	DNB_THROW_SPEC((DNBEZeroDivide, DNBEOverflowError));

/**
  * Equal operator.
  * 
  * 
  */
    friend inline ExportedByDNBMath
    bool
    operator==( const DNBSimTime &lhs, const DNBSimTime &rhs )
	DNB_THROW_SPEC_NULL;

/**
  * Not equal operator.
  * 
  * 
  */
    friend inline ExportedByDNBMath
    bool
    operator!=( const DNBSimTime &lhs, const DNBSimTime &rhs )
	DNB_THROW_SPEC_NULL;

/**
  * Less than operator.
  * 
  * 
  */
    friend inline ExportedByDNBMath
    bool
    operator<( const DNBSimTime &lhs, const DNBSimTime &rhs )
	DNB_THROW_SPEC_NULL;

/**
  * Greater than operator.
  * 
  * 
  */
    friend inline ExportedByDNBMath
    bool
    operator>( const DNBSimTime &lhs, const DNBSimTime &rhs )
	DNB_THROW_SPEC_NULL;

/**
  * Less than or equal operator.
  * 
  * 
  */
    friend inline ExportedByDNBMath
    bool
    operator<=( const DNBSimTime &lhs, const DNBSimTime &rhs )
	DNB_THROW_SPEC_NULL;

/**
  * Greater than or equal operator.
  * 
  * 
  */
    friend inline ExportedByDNBMath
    bool
    operator>=( const DNBSimTime &lhs, const DNBSimTime &rhs )
	DNB_THROW_SPEC_NULL;

/**
  * Computes the arc cosine in the range 0 to PI.
  * 
  * 
  */
    friend inline ExportedByDNBMath
    DNBSimTime
    acos( const DNBSimTime &x )
	DNB_THROW_SPEC_NULL;

/**
  * Computes the arc sine in the range -PI/2 to PI/2.
  * 
  * 
  */
    friend inline ExportedByDNBMath
    DNBSimTime
    asin( const DNBSimTime &x )
	DNB_THROW_SPEC_NULL;

/**
  * Computes the arc tangent in the range -PI/2 to PI/2.
  * 
  * 
  */
    friend inline ExportedByDNBMath
    DNBSimTime
    atan( const DNBSimTime &x )
	DNB_THROW_SPEC_NULL;

/**
  * Computes the arc tangent of y/x in the range -PI/2 to PI/2.
  * 
  * 
  */
    friend inline ExportedByDNBMath
    DNBSimTime
    atan2( const DNBSimTime &y, const DNBSimTime &x )
	DNB_THROW_SPEC_NULL;

/**
  * Computes the cosine.
  * 
  * 
  */
    friend inline ExportedByDNBMath
    DNBSimTime
    cos( const DNBSimTime &x )
	DNB_THROW_SPEC_NULL;

/**
  * Computes the sine.
  * 
  * 
  */
    friend inline ExportedByDNBMath
    DNBSimTime
    sin( const DNBSimTime &x )
	DNB_THROW_SPEC_NULL;

/**
  * Computes the tangent.
  * 
  * 
  */
    friend inline ExportedByDNBMath
    DNBSimTime
    tan( const DNBSimTime &x )
	DNB_THROW_SPEC_NULL;

/**
  * Computes the hyperbolic cosine.
  * 
  * 
  */
    friend inline ExportedByDNBMath
    DNBSimTime
    cosh( const DNBSimTime &x )
	DNB_THROW_SPEC_NULL;

/**
  * Computes the hyperbolic sine.
  * 
  * 
  */
    friend inline ExportedByDNBMath
    DNBSimTime
    sinh( const DNBSimTime &x )
	DNB_THROW_SPEC_NULL;

/**
  * Computes the hyperbolic tangent.
  * 
  * 
  */
    friend inline ExportedByDNBMath
    DNBSimTime
    tanh( const DNBSimTime &x )
	DNB_THROW_SPEC_NULL;

/**
  * Computes the exponential function.
  * 
  * 
  */
    friend inline ExportedByDNBMath
    DNBSimTime
    exp( const DNBSimTime &x )
	DNB_THROW_SPEC_NULL;

/**
  * Computes the natural logarithm.
  * 
  * 
  */
    friend inline ExportedByDNBMath
    DNBSimTime
    log( const DNBSimTime &x )
	DNB_THROW_SPEC_NULL;

/**
  * Computes the base 10 logarithm.
  * 
  * 
  */
    friend inline ExportedByDNBMath
    DNBSimTime
    log10( const DNBSimTime &x )
	DNB_THROW_SPEC_NULL;

/**
  * Computes the signed factional part of x and store the integral in ip.
  * 
  * 
  */
    friend inline ExportedByDNBMath
    DNBSimTime
    modf( const DNBSimTime &x, DNBSimTime *ip )
	DNB_THROW_SPEC_NULL;

/**
  * Computes the power of x to y.
  * 
  * 
  */
    friend inline ExportedByDNBMath
    DNBSimTime
    pow( const DNBSimTime &x, const DNBSimTime &y )
	DNB_THROW_SPEC_NULL;

/**
  * Computes the square root.
  * 
  * 
  */
    friend inline ExportedByDNBMath
    DNBSimTime
    sqrt( const DNBSimTime &x )
	DNB_THROW_SPEC_NULL;

/**
  * Computes the absolute value.
  * 
  * 
  */
    friend inline ExportedByDNBMath
    DNBSimTime
    abs( const DNBSimTime &x )
	DNB_THROW_SPEC_NULL;

/**
  * Computes the smallest value not less than x.
  * 
  * 
  */
    friend inline ExportedByDNBMath
    DNBSimTime
    ceil( const DNBSimTime &x )
	DNB_THROW_SPEC_NULL;

/**
  * Computes the largest value not greater than x.
  * 
  * 
  */
    friend inline ExportedByDNBMath
    DNBSimTime
    floor( const DNBSimTime &x )
	DNB_THROW_SPEC_NULL;

/**
  * Computes the remainder of devision of x by y.
  * 
  * 
  */
    friend inline ExportedByDNBMath
    DNBSimTime
    fmod( const DNBSimTime &x, const DNBSimTime &y )
	DNB_THROW_SPEC((DNBEZeroDivide, DNBEOverflowError));

/**
  * Inserter operator.
  * 
  * 
  */
    friend ExportedByDNBMath
    ostream &
    operator<<( ostream &o, const DNBSimTime &x )
	DNB_THROW_SPEC_NULL;

/**
  * Extractor operator.
  * 
  * 
  */
/* smw removed Extractor due to IOSS errors and due to porting to 64 bit */

private:
    void
    normalize_()
	DNB_THROW_SPEC_NULL;

    inline int
    sign() const
	DNB_THROW_SPEC_NULL;
	
    DNBInteger64 sec_;
    DNBInteger32 nsec_;
    
    friend class _DNBSimTimeInteger;
    
};


#include <DNBSimTime.cc>

#endif  /* _DNB_SIMTIME_H_ */
