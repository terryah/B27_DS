//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview CZO smw 03:09:12
 */
/*******************************************************************************************/
// File name: DNBMathMatrix.cc
/*******************************************************************************************/

//*
//* FILE:
//*     DNBMathExtractors.h    - Contains all the removed extractors ( i.e. operator>> ) from 
//*								- The math classes
//*
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     smw         01/01/03??    Initial Implementation
//*		smw			01/07/05	Removed extractors due to iostream migration to the
//*								new, standard, templated iostream, and due to migration to 64 bit
//*								builds.  Extractor for all math classes have been moved, 
//*								for easy rescucitation, if necessary,
//*								to DNBMath.tst/PrivateInterfaces/DNBMathExtractors.h
//*     smw        02/28/2006   Fix Memory Leak revealed by running the MemoryCheck tool 
//*                             with the DNBPoolAllocator disabled ( was done on winb64 )
//*     smw        01/18/2008   Fix MLK revealed by running MemoryCheck with DNBPoolAllocator
//*                             disabled (reshape() after creating empty matrix)
//*




#include <scl_memory.h>
#define DNB_DEFAULT_MATRIX_ROW_CAPACITY 6


/*****************************************************************************************
// Constructors
/****************************************************************************************/


/**
  * Constructs an empty matrix ( 0 by 0). It can later be resized or reshaped
  */
template <class T>
DNBBasicMathMatrix<T>::
DNBBasicMathMatrix() 
DNB_THROW_SPEC((scl_bad_alloc)) : rowCapacity_(DNB_DEFAULT_MATRIX_ROW_CAPACITY),
                              rows_ ( NULL ),
                              rows1_ ( NULL),
                              numRows_ (0),
                              numColumns_ (0),
                              alloc_ (DNBPoolAllocator::Global())

{
    localAllocate(rowCapacity_, numColumns_);
}


/**
  * Constructs an m by n matrix with uninitialized elements
  */ 
template <class T>
DNBBasicMathMatrix<T>::
DNBBasicMathMatrix (size_t m, size_t n, UninitializedType dummy) 
DNB_THROW_SPEC((scl_bad_alloc)): rowCapacity_(m),
                              rows_ ( NULL ),
                              rows1_ ( NULL),
                              numRows_ (m),
                              numColumns_ (n),
                              alloc_ (DNBPoolAllocator::Global())

{
    localAllocate(rowCapacity_, numColumns_);

#if DNB_VERIFY
    T       NaN;
    DNBSetNaN( NaN );
#else
    T       NaN = T(0);
#endif
    for (int i = 0; i < numRows_; i++)
      for (int j = 0; j < numColumns_; j++)
          rows_[i][j] = NaN;
}

/**
  * Constructs an m by n  matrix with all elements initialized to initVal.
  */
template <class T>
DNBBasicMathMatrix<T>::DNBBasicMathMatrix (size_t m, size_t n, const T& initVal)
DNB_THROW_SPEC((scl_bad_alloc)): rowCapacity_(m),
                              rows_ ( NULL ),
                              rows1_ ( NULL),
                              numRows_ (m),
                              numColumns_ (n),
                              alloc_ (DNBPoolAllocator::Global())

{
    localAllocate(rowCapacity_, numColumns_);
    for (int i = 0; i < numRows_; i++)
      for (int j = 0; j < numColumns_; j++)
          rows_[i][j] = initVal;
}


/**
  * Constructs an m by n matrix, using the data pointed to by dat as initial data. 
  * A copy of dat is made. The data pointed must have at least m times n elements.
  * Assumes data stores in dat in row major format (i.e. first row first, second row, etc
  */
template <class T>
DNBBasicMathMatrix<T>::DNBBasicMathMatrix (const T* dat,size_t m, size_t n)
DNB_THROW_SPEC((scl_bad_alloc)): rowCapacity_(m),
                              rows_ ( NULL ),
                              rows1_ ( NULL),
                              numRows_ (m),
                              numColumns_ (n),
                              alloc_ (DNBPoolAllocator::Global())
{
    localAllocate(rowCapacity_, numColumns_);
    int k = 0;
    for (int i = 0; i < numRows_; i++)
        for (int j = 0; j < numColumns_; j++)
            rows_[i][j] = dat[k++];
}


/**
  * Constructs an m by n matrix from a DNBMathVector of length m * n
  * Assumes data stores in dat in row major format (i.e. first row first, second row, etc
  */
template <class T>
DNBBasicMathMatrix<T>::DNBBasicMathMatrix (const DNBBasicMathVector<T>& v, 
                                           size_t m, 
                                           size_t n)
DNB_THROW_SPEC((scl_bad_alloc)): rowCapacity_(m),
                              rows_ ( NULL ),
                              rows1_ ( NULL),
                              numRows_ (m),
                              numColumns_ (n),
                              alloc_ (DNBPoolAllocator::Global())
{
    localAllocate(rowCapacity_, numColumns_);
    int k = 0;
    for (int i = 0; i < numRows_; i++)
        for (int j = 0; j < numColumns_; j++)
            rows_[i][j] = v[k++];
}


/**
  * Constructs a matrix from the null terminated character string s. The format of the 
  * character string is the same as that expected by the global operator operator>> 
  */
// DNBBasicMathMatrix(const char *s) DNB_THROW_SPEC((scl_bad_alloc)); // TODO


/**
  * Copy constructor; This actually makes copy of data
  */
template <class T>
DNBBasicMathMatrix<T>::DNBBasicMathMatrix (const DNBBasicMathMatrix<T>& matrix)
DNB_THROW_SPEC((scl_bad_alloc)):  rowCapacity_(matrix.getRowCapacity()),
                              rows_ ( NULL ),
                              rows1_ ( NULL),
                              numRows_ (matrix.numRows()),
                              numColumns_ (matrix.numColumns()),
                              alloc_ (DNBPoolAllocator::Global())
{
    localAllocate(rowCapacity_, numColumns_);
    for (int i = 0; i < numRows_; i++)
        for (int j = 0; j < numColumns_; j++)
            rows_[i][j] = matrix[i][j];
}


/*****************************************************************************************/
/* Helper methods to localize all memory allocations in constructors */
/*****************************************************************************************/


template <class T>
void
DNBBasicMathMatrix<T>::localAllocate(size_t nRows, size_t nColumns)
{


    rows_ = (DNBBasicMathVector<T>*) alloc_.allocate(nRows * sizeof(DNBBasicMathVector<T>));
    rows1_ = rows_ - 1;
    DNBBasicMathVector<T> vec(nColumns);
    scl_uninitialized_fill_n(rows_, nRows, vec);
}


/*template <class T>
void
DNBBasicMathMatrix<T>::uninitialized_fill_n(DNBBasicMathVector<T> * location, size_t nRows, size_t nColumns)
{
    for (int i = 0; i < nRows; i++)
    {
    void * ptr = &location[i];
	DNBBasicMathVector<T> * temp = ::operator new(ptr) DNBBasicMathVector<T>(nColumns);
    }
}*/




/*****************************************************************************************/
/* Destructor */
/*****************************************************************************************/

template <class T>
DNBBasicMathMatrix<T>::~DNBBasicMathMatrix()
{
     // destructor of vectors gets called automatically?
    /*for (int i = 0; i < numRows_; i++)
        // alloc_.deallocate(&rows_[i], rows_[i].getCapacity()*sizeof(T)); // using the matrix's allocator but it is the same anyway
        // (rows_[i]).~DNBBasicMathVector<T>();
    */    
    _scl_destroy( rows_, rows_+ rowCapacity_ ); // SMW fix MemoryLeak masked by the DNBPoolAllocator
    alloc_.deallocate(rows_, rowCapacity_*sizeof(DNBBasicMathVector<T>));
    rows_ = NULL;
    rows1_ = NULL;
}

/*******************************************************************************************/
/*  Member Functions - Access */
/*******************************************************************************************/

/**
 * Returns the matrix element at row i and column j, where i ranges from 0 to and
 * including numRows - 1  and j ranges from 0 to and including numCols - 1.
 */

template <class T>
T&
DNBBasicMathMatrix<T>::operator() (size_t i, size_t j) 
DNB_THROW_SPEC_NULL
{
    return rows_[i][j];
}


/**
 * Constant version of above.
 */
template <class T>
const T&
DNBBasicMathMatrix<T>::operator()(size_t i, size_t j) const 
DNB_THROW_SPEC_NULL
{
    return rows_[i][j];
}


/**
 * Returns the matrix element at row i and column j, where i ranges from 1 to and
 * including numRows and j ranges from 1 to and including numCols.
 */
template <class T>
T&
DNBBasicMathMatrix<T>::fromOne(size_t i, size_t j) 
DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION(i > 0);
    DNB_PRECONDITION(j > 0);
    return rows1_[i].fromOne(j);
}


/**
  * Constant version of above
  */
template <class T>
const T&
DNBBasicMathMatrix<T>::fromOne(size_t i, size_t j) const
DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION(i > 0);
    DNB_PRECONDITION(j > 0);
    return rows1_[i].fromOne(j);
}


template <class T>
DNBBasicMathVector<T>
DNBBasicMathMatrix<T>::col ( size_t j)
DNB_THROW_SPEC_NULL
{
    DNBBasicMathVector<T> vec(numRows_);
    for (int i = 0; i < numRows_; i++)
        vec[i] = rows_[i][j];
    return vec; 
}

/**
  * Constant version of above.
  * Returns a vector that contains column j of the matrix. Data is copied!!
  * Can't we consolidate the two to share code?
  */
template <class T>
const DNBBasicMathVector<T>
DNBBasicMathMatrix<T>::col ( size_t j) const 
DNB_THROW_SPEC_NULL
{
    DNBBasicMathVector<T> vec(numRows_);
    for (int i = 0; i < numRows_; i++)
        vec[i] = rows_[i][j];
    return vec; 
}

// Note: Rest of access member functions were inlined

/*****************************************************************************************
* Member Functions - Miscellaneous
*****************************************************************************************/
/**
  * Changes the size of the matrix to m rows and n columns.  The resulting matrix, 
  * if larger, has additional elements which are not initialized, ie they contain 
  * garbage
  */
template <class T>
void
DNBBasicMathMatrix<T>::reshape(size_t m, size_t n, bool init)
DNB_THROW_SPEC((scl_bad_alloc))
{
    // reshape each available and needed row first
    int availAndNeededIndex = scl_min((int)rowCapacity_, (int)m);
    for (int i = 0; i < availAndNeededIndex; i++) rows_[i].reshape(n, init);

    if (m > rowCapacity_)
    {
        // create new space for the rows of the matrix, called temp
        DNBBasicMathVector<T> * temp = (DNBBasicMathVector<T>*) alloc_.allocate(m * sizeof(DNBBasicMathVector<T>)) ;               // was new DNBBasicMathVector<T>[m];
        
        // construct temp to hold old as well as additional rows
        DNBBasicMathVector<T> tempVec(n);
        scl_uninitialized_fill_n(temp, m, tempVec);  // no way to disable initialization here!!

        // copy old rows to temp  
        for (int i = 0; i < numRows_; i ++) 
        {
            for ( int j = 0; j < numColumns_; j++ ) temp[i][j] = rows_[i][j]; // smw: fix UMR; do not inlcude data in any new columns
        }
        // deallocate space for all vectors in the old rows 
        for ( i = 0; i < rowCapacity_; i++ )
        {
            alloc_.deallocate((void *)rows_[i].data_, rows_[i].getCapacity() * sizeof(T));
            //rows_[i] = NULL; not needed anymore?
            //rows1_[i] = NULL;
        }

        // deallocate the old space for rows_
        alloc_.deallocate(rows_, rowCapacity_ * sizeof(DNBBasicMathVector<T>));             // was delete [] rows_;

        // Make the new space temp the rows_ of the matrix
        rows_ = temp;
        rows1_ = rows_ - 1;
        rowCapacity_ = m;
    }

    if (init) // not needed since additional rows were automaticallt initialized ?
        // put 0's in additional rows, if any
        for (int i = numRows_; i < m; i++)
            rows_[i] = T(0);           // vector assignment by scalar (0-element of field defined by T)

    numRows_ = m;
    numColumns_ = n;
}

/**
  * Changes the size of the matrix to m rows and n columns.  The resulting matrix, 
  * if larger, has additional elements initialized to 0
  */
template <class T>
void
DNBBasicMathMatrix<T>::resize(size_t m, size_t n)
DNB_THROW_SPEC((scl_bad_alloc))
{
    reshape(m, n, true);
}



/*****************************************************************************************
/* Mathematical operators
/* ***************************************************************************************/

/**
 * Increment each element of self. This is invoked when the operator is used as a 
 * prefix operator.
 */
template <class T>
DNBBasicMathMatrix<T>&       
DNBBasicMathMatrix<T>::operator++()  
DNB_THROW_SPEC_NULL 
{
    for (int i = 0; i < numRows_; i++)
    {
         rows_[i]++;
    }
     return (*this);
}

/**
 * Increment each element of self. This is invoked when the operator is used as a 
 * postfix operator.
 */
template <class T>
void                
DNBBasicMathMatrix<T>::operator++(int) DNB_THROW_SPEC_NULL
{
  for (int i = 0; i < numRows_; i++)
    {
         rows_[i]++;
    }
}


/**
 * Decrement each element of self. This is invoked when the operator is used as a 
 * prefix operator.
 */
template <class T>
DNBBasicMathMatrix<T>&      
DNBBasicMathMatrix<T>::operator--() DNB_THROW_SPEC_NULL
{
  for (int i = 0; i < numRows_; i++)
    {
         rows_[i]--;
    }
  return (*this);

}


/**
 * Decrement each element of self.  This is invoked when the operator is used as a 
 * postfix operator.
 */
template <class T>
void                
DNBBasicMathMatrix<T>::operator--(int) DNB_THROW_SPEC_NULL
{
  for (int i = 0; i < numRows_; i++)
    {
         rows_[i]--;
    }
}


/**
  * Assignment operator.  Resizes self if necessary to accomodate the data to be 
  * assigned
  */
template <class T>
void
DNBBasicMathMatrix<T>::operator=(const DNBBasicMathMatrix<T>& mat)
DNB_THROW_SPEC_NULL
{
    reshape(mat.numRows(), mat.numColumns());
    for (int i = 0; i < numRows_; i++) 
        for (int j = 0; j< numColumns_; j++) rows_[i][j] = mat[i][j];
}


/**
  * Assigns a value of type T to each element of the matrix
  */
template <class T>
void
DNBBasicMathMatrix<T>::operator=(const T& ValT)
DNB_THROW_SPEC_NULL
{
  if(numRows_ != 0)
  for (int i=0; i<numRows_; i++)
    {
      rows_[i] = ValT;
    }
  
}


/**
  * Adds each element of the matrix mat to the corresponding element of this matrix.
  * mat must have the same number of rows and columns as this matrix.
  */
template <class T>
void
DNBBasicMathMatrix<T>::operator+=(const DNBBasicMathMatrix<T>& mat)
DNB_THROW_SPEC_NULL
{
  int numRows = mat.numRows();
  int numColumns = mat.numColumns();
  DNB_PRECONDITION ((numRows_ == mat.numRows()) && (numColumns_ == mat.numColumns()));
  
  for (int i=0; i<numRows_ ; i++)
       rows_[i] = rows_[i] + mat[i];
}


/**
  * Subtracts each element of the matrix mat from the corresponding element of this matrix
  * mat must have the same number of rows and columns as this matrix.
  */
template <class T>
void
DNBBasicMathMatrix<T>::operator-=(const DNBBasicMathMatrix<T>& mat)
DNB_THROW_SPEC_NULL
{
  int numRows = mat.numRows();
  int numColumns = mat.numColumns();
  DNB_PRECONDITION ((numRows_ == mat.numRows()) && (numColumns_ == mat.numColumns()));
  
  for (int i=0; i<numRows_ ; i++)
         rows_[i] = rows_[i] - mat[i];
}


/**
  * Performs matrix multiplication as defined in Linear Algebra.
  * self = self * mat
  * the number of columns of self must equal the number of rows of mat.
  * Self is expanded or shrunk to contain the result, if needed.
  */
template <class T>
void
DNBBasicMathMatrix<T>::operator*=(const DNBBasicMathMatrix<T>& mat)
DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION (numColumns_ == mat.numRows());
    if ( mat.numColumns() > numColumns_)                // resize
    {
        resize(numRows_, mat.numColumns());
    }
    for (int i = 0; i < numRows_; i++)
        rows_[i] += rows_[i] * mat[i];
        
}


/** 
  *Divide each element of the matrix mat by the corresponding element of this matrix
  * mat must have the same number of rows and columns as this matrix.
  // Comment it out due to ambiguity and different interpretation of matrix division
  */
/*template <class T>
void
DNBBasicMathMatrix<T>::operator/=(const DNBBasicMathMatrix<T>& mat)
DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION (numColumns_ == mat.numRows());
    if ( mat.numColumns() > numColumns_)                // resize
    {
        resize(numRows_, mat.numColumns());
    }
    for (int i = 0; i < numRows_; i++)
           rows_[i] = rows_[i]/ mat[i];
       
}*/


/**
  * Adds the value of type T to each element of this matrix
  */
template <class T>
void
DNBBasicMathMatrix<T>::operator+=(const T& ValT)
DNB_THROW_SPEC_NULL
{
  for (int i=0; i<numRows_ ; i++)
         rows_[i] = rows_[i] + ValT;
}


/**
  * Subtracts the value of type T from each element of this matrix
  */
template <class T>
void
DNBBasicMathMatrix<T>::operator-=(const T& ValT)
DNB_THROW_SPEC_NULL
{
  for (int i=0; i<numRows_ ; i++)
        rows_[i] -= ValT;

}

/**
  * Multiplies each element of this matrix by the value of type T
  */
template <class T>
void
DNBBasicMathMatrix<T>::operator*=(const T& ValT)
DNB_THROW_SPEC_NULL
{
  for (int i=0; i<numRows_ ; i++)
        rows_[i] = rows_[i] * ValT;
}


/**
  * Divides each element of this matrix by the value of type T
  */
template <class T>
void
DNBBasicMathMatrix<T>::operator/=(const T& ValT)
DNB_THROW_SPEC_NULL
{
  for (int i=0; i<numRows_ ; i++)
        rows_[i] = rows_[i] / ValT;
}



/*****************************************************************************************
// Constructors that do math
*****************************************************************************************/


/**
  * @nodoc
  * Additive inverse
  */
template <class T>
DNBBasicMathMatrix<T>::
DNBBasicMathMatrix( const DNBBasicMathMatrix<T>& matrix,
                    const _DNBSubOp&)
DNB_THROW_SPEC((scl_bad_alloc)) : rowCapacity_(matrix.getRowCapacity()),
                              numRows_(matrix.numRows()),
                              numColumns_(matrix.numColumns()),
                              rows_ ( NULL ),
                              rows1_ ( NULL ),
                              alloc_ (DNBPoolAllocator::Global())
{
    localAllocate(rowCapacity_, numColumns_);
    for (int i = 0; i < numRows_; i++)
       rows_[i] = - matrix[i] ;    // FIXME: do we really need vector assignment?
                                    // + : we get unrolling 
                                    // - : additional call on stack, additional unnecessary check and assignment
}


/**
 * @nodoc
 * Matrix Addition
 */
template <class T>
DNBBasicMathMatrix<T>::
DNBBasicMathMatrix( const DNBBasicMathMatrix<T>& left,
                    const DNBBasicMathMatrix<T>& right, 
                    const _DNBAddOp & )
DNB_THROW_SPEC((scl_bad_alloc)) : rowCapacity_(left.getRowCapacity()),
                              numRows_(left.numRows()),
                              numColumns_(left.numColumns()),
                              rows_ ( NULL ),
                              rows1_ ( NULL ),
                              alloc_ (DNBPoolAllocator::Global())
{
    localAllocate(rowCapacity_, numColumns_);
    for (int i = 0; i < numRows_; i++)
       rows_[i] = left[i] + right[i];   // FIXME: do we really need vector assignment?
} 


/**
 * @nodoc
 * Matrix subtraction
 */
template <class T>
DNBBasicMathMatrix<T>::
DNBBasicMathMatrix( const DNBBasicMathMatrix<T>& left,
                    const DNBBasicMathMatrix<T>& right, 
                    const _DNBSubOp & )
DNB_THROW_SPEC((scl_bad_alloc)):  rowCapacity_(left.getRowCapacity()),
                              numRows_(left.numRows()),
                              numColumns_(left.numColumns()),
                              rows_ ( NULL ),
                              rows1_ ( NULL ),
                              alloc_ (DNBPoolAllocator::Global())
{
    localAllocate(rowCapacity_, numColumns_);
    for (int i = 0; i < numRows_; i++)
       rows_[i] = left[i] - right[i];   // FIXME: do we really need vector assignment?
}


/**
 * @nodoc
 * Matrix multiplication as defined by Linear Algebra
 */
template <class T>
DNBBasicMathMatrix<T>::
DNBBasicMathMatrix( const DNBBasicMathMatrix<T>& left,
                    const DNBBasicMathMatrix<T>& right, 
                    const _DNBMulOp & )
DNB_THROW_SPEC((scl_bad_alloc)): rowCapacity_(left.getRowCapacity()),
                              numRows_(left.numRows()),
                              numColumns_(right.numColumns()),
                              rows_ ( NULL ),
                              rows1_ ( NULL ),
                              alloc_ (DNBPoolAllocator::Global())
{
   localAllocate(rowCapacity_, numColumns_);
   for (int i = 0; i < numRows_; i++)
        for (int j = 0; j < numColumns_; j++)
            rows_[i][j] = DNBDot(left.row(i), right.col(j));
}



/**
 * @nodoc
 * Matrix + scalar
 */
template <class T>
DNBBasicMathMatrix<T>::
DNBBasicMathMatrix( const DNBBasicMathMatrix<T> &left, 
                    const T& aVal, 
                    const _DNBAddOp & )
DNB_THROW_SPEC((scl_bad_alloc)) : rowCapacity_(left.getRowCapacity()),
                              numRows_(left.numRows()),
                              numColumns_(left.numColumns()),
                              rows_ ( NULL ),
                              rows1_ ( NULL ),
                              alloc_ (DNBPoolAllocator::Global())
{
    localAllocate(rowCapacity_, numColumns_);
    for (int i = 0; i < numRows_; i++)
    {
       rows_[i] = left[i] + aVal;        
    }
    
}

/**
 * @nodoc
 * scalar + matrix
 */
template <class T>
DNBBasicMathMatrix<T>::
DNBBasicMathMatrix( const T& aVal, 
                    const DNBBasicMathMatrix<T> &right,
                    const _DNBAddOp & )
DNB_THROW_SPEC((scl_bad_alloc)): rowCapacity_(right.getRowCapacity()),
                              numRows_(right.numRows()),
                              numColumns_(right.numColumns()),
                              rows_ ( NULL ),
                              rows1_ ( NULL ),
                              alloc_ (DNBPoolAllocator::Global())
{
    localAllocate(rowCapacity_, numColumns_);
    for (int i = 0; i < numRows_; i++)
       rows_[i] = aVal + right[i] ;        
}


/**
 * @nodoc
 * matrix - scalar
 */
template <class T>
DNBBasicMathMatrix<T>::
DNBBasicMathMatrix( const DNBBasicMathMatrix<T> &left,
                    const T& aVal,
                    const _DNBSubOp & )
DNB_THROW_SPEC((scl_bad_alloc)): rowCapacity_(left.getRowCapacity()),
                              numRows_(left.numRows()),
                              numColumns_(left.numColumns()),
                              rows_ ( NULL ),
                              rows1_ ( NULL ),
                              alloc_ (DNBPoolAllocator::Global())
{
    localAllocate(rowCapacity_, numColumns_);
    for (int i = 0; i < numRows_; i++)
       rows_[i] = left[i] - aVal;        
}


/**
 * @nodoc
 * scalar - matrix
 */
template <class T>
DNBBasicMathMatrix<T>::
DNBBasicMathMatrix( const T& aVal, 
                    const DNBBasicMathMatrix<T> &right,
                    const _DNBSubOp & )
DNB_THROW_SPEC((scl_bad_alloc)) : rowCapacity_(right.getRowCapacity()),
                              numRows_(right.numRows()),
                              numColumns_(right.numColumns()),
                              rows_ ( NULL ),
                              rows1_ ( NULL ),
                              alloc_ (DNBPoolAllocator::Global())
{
    localAllocate(rowCapacity_, numColumns_);
    for (int i = 0; i < numRows_; i++)
       rows_[i] = aVal - right[i] ;        
}


/**
 * @nodoc
 * matrix * scalar
 */
template <class T>
DNBBasicMathMatrix<T>::
DNBBasicMathMatrix( const DNBBasicMathMatrix<T> &left,
                    const T& aVal, 
                    const _DNBMulOp & )
DNB_THROW_SPEC((scl_bad_alloc)): rowCapacity_(left.getRowCapacity()),
                              numRows_(left.numRows()),
                              numColumns_(left.numColumns()),
                              rows_ ( NULL ),
                              rows1_ ( NULL ),
                              alloc_ (DNBPoolAllocator::Global())
{
   localAllocate(rowCapacity_, numColumns_);
    for (int i = 0; i < numRows_; i++)
       rows_[i] = left[i] * aVal;        
}

/**
 * @nodoc
 * scalar * matrix
 */
template <class T>
DNBBasicMathMatrix<T>::
DNBBasicMathMatrix( const T& aVal,
                    const DNBBasicMathMatrix<T> &right,
                    const _DNBMulOp & )
DNB_THROW_SPEC((scl_bad_alloc)): rowCapacity_(right.getRowCapacity()),
                              numRows_(right.numRows()),
                              numColumns_(right.numColumns()),
                              rows_ ( NULL ),
                              rows1_ ( NULL ),
                              alloc_ (DNBPoolAllocator::Global())
{
    localAllocate(rowCapacity_, numColumns_);
    for (int i = 0; i < numRows_; i++)
    {
       rows_[i] = aVal * right[i];        
    }
    
}

/**
 * @nodoc
 * matrix / scalar
 */
template <class T>
DNBBasicMathMatrix<T>::
DNBBasicMathMatrix( const DNBBasicMathMatrix<T> &left,
                    const T& aVal, 
                    const _DNBDivOp & )
DNB_THROW_SPEC((scl_bad_alloc)):  rowCapacity_(left.getRowCapacity()),
                              numRows_(left.numRows()),
                              numColumns_(left.numColumns()),
                              rows_ ( NULL ),
                              rows1_ ( NULL ),
                              alloc_ (DNBPoolAllocator::Global())
{
    localAllocate(rowCapacity_, numColumns_);
    for (int i = 0; i < numRows_; i++)
       rows_[i] = left[i] / aVal;        
}


/**
 * @nodoc
 * scalar / matrix
 */
template <class T>
DNBBasicMathMatrix<T>::
DNBBasicMathMatrix( const T& aVal,
                    const DNBBasicMathMatrix<T> &right,
                    const _DNBDivOp & )
DNB_THROW_SPEC((scl_bad_alloc)) : rowCapacity_(right.getRowCapacity()),
                              numRows_(right.numRows()),
                              numColumns_(right.numColumns()),
                              rows_ ( NULL ),
                              rows1_ ( NULL ),
                              alloc_ (DNBPoolAllocator::Global())
{
    localAllocate(rowCapacity_, numColumns_);
    for (int i = 0; i < numRows_; i++)
       rows_[i] = aVal / right[i];        
}


/**********************************************************************************
* Global Mathematical Operators
***********************************************************************************/


/**
  * Returns true if left is equal to right, i.e.
  * they both have the same number of rows and columns and each element
  * in one is equal to the corresponding element in the other
  */
template <class T>
bool
                        operator==( const DNBBasicMathMatrix<T>& left,
                                    const DNBBasicMathMatrix<T>& right) 
                        DNB_THROW_SPEC_NULL
{
    int numRows = left.numRows();
    if ((numRows != right.numRows()) || (left.numColumns() != right.numColumns())) 
        return FALSE;
    for (int i = 0; i < numRows; i++)
    {
        if (left.row(i) != right.row(i)) return FALSE;
    }
    return TRUE;
}


/**
  * Returns true if every element of left is less than its counterpart in right
  */
template <class T>
bool
                        operator<( const DNBBasicMathMatrix<T>& left,
                                   const DNBBasicMathMatrix<T>& right ) 
                        DNB_THROW_SPEC_NULL
{
    int numRows = left.numRows();
    if ((numRows != right.numRows()) || (left.numColumns() != right.numColumns())) 
        return FALSE;
    for (int i = 0; i < numRows; i++)
    {
        if (left.row(i) > right.row(i)) return FALSE;
    }
    return TRUE;
}



/**
  * Returns the additive inverse of the argument matrix 
  */
template <class T>
DNBBasicMathMatrix<T>    
                          operator-(const DNBBasicMathMatrix<T>& left)
                          DNB_THROW_SPEC((scl_bad_alloc))
{
    return DNBBasicMathMatrix<T>(left, _DNBSubOp());
}



/** 
 * returns a new matrix where each element is the sum of the two corresponding
 * elements of the two input matrices.
 * The two input matrices must have the same number of rows and columns.
 */
template <class T>
DNBBasicMathMatrix<T>    
                          operator+(const DNBBasicMathMatrix<T>& left,
                                    const DNBBasicMathMatrix<T>& right)
                          DNB_THROW_SPEC((scl_bad_alloc))
{
    DNB_PRECONDITION (( left.numRows() == right.numRows()) 
                  && (left.numColumns() == right.numColumns()));
    return DNBBasicMathMatrix<T>(left, right, _DNBAddOp());
}

/** 
 * returns a new matrix where each element is the difference of the two corresponding
 * elements of the two input matrices ( first input matrix - second input matrix)
 * The two input matrices must have the same number of rows and columns.
 */
template <class T>
DNBBasicMathMatrix<T>
                        operator-(const DNBBasicMathMatrix<T>& left,
                                  const DNBBasicMathMatrix<T>& right)
                        DNB_THROW_SPEC((scl_bad_alloc))
{
DNB_PRECONDITION((left.numRows() == right.numRows()) && 
                  left.numColumns() == right.numColumns());
return (DNBBasicMathMatrix<T>(left, right, _DNBSubOp()));
}

/** 
 * returns a new matrix which is the product of the two matrices, using the Linear
 * Algebra definition of matrix multiplication
 * result = left * right
 * the number of columns of left must equal the number of rows of right.
 */
template <class T>
DNBBasicMathMatrix<T>    operator*(const DNBBasicMathMatrix<T>& left,
                                   const DNBBasicMathMatrix<T>& right)
                         DNB_THROW_SPEC((scl_bad_alloc))
{
    DNB_PRECONDITION (left.numColumns() == right.numRows());
    return DNBBasicMathMatrix<T>(left, right, _DNBMulOp() );

}


/** 
 * Same as above with left argument being a DNBBasicMathVector<T>
 * the length of left must equal the number of rows of right.
 */
template <class T>
DNBBasicMathVector<T>    operator*(const DNBBasicMathVector<T>& vec,
                                   const DNBBasicMathMatrix<T>& mat)
                         DNB_THROW_SPEC((scl_bad_alloc))
{
    DNB_PRECONDITION (vec.length() == mat.numRows());
    int n = mat.numColumns();
    DNBBasicMathVector<T> vecresult(n);
    for (int i = 0; i < n; i++)
      {
        vecresult[i] = DNBDot(vec, mat.col(i));
      }

    return vecresult;
}

/** 
 * Same as above with right argument being a DNBBasicMathVector<T>
 * the number of columns of left must equal the length of right.
 */
template <class T>
DNBBasicMathVector<T>    operator*(const DNBBasicMathMatrix<T>& mat,
                                   const DNBBasicMathVector<T>& vec)
                         DNB_THROW_SPEC((scl_bad_alloc))
{
    DNB_PRECONDITION (vec.length() == mat.numRows());
    int n = mat.numColumns();
    DNBBasicMathVector<T> vecresult(n);
    for (int i = 0; i < n; i++)
      {
        vecresult[i] = DNBDot(mat.col(i),vec);
      }

    return vecresult;      
}


/** 
 * returns a new matrix where each element is the ratio of the two corresponding
 * elements of the two input matrices (firs input matrix element/ second input matrix element)
  * The two input matrices must have the same number of rows and columns.
*/
/*template <class T>
DNBBasicMathMatrix<T>    operator/(const DNBBasicMathMatrix<T>&,
                                   const DNBBasicMathMatrix<T>&)
                         DNB_THROW_SPEC((scl_bad_alloc))
{
}*/

/** 
 * returns a new matrix where each element is the sum of corresponding
 * element of the input matrix and the input constant
 */
template <class T>
DNBBasicMathMatrix<T>    operator+(const DNBBasicMathMatrix<T>& left,
                                   const T& right)
                         DNB_THROW_SPEC((scl_bad_alloc))
{
    return DNBBasicMathMatrix<T>(left, right, _DNBAddOp() );
}

/** 
 * returns a new matrix where each element is the sum of corresponding
 * element of the input matrix and the input constant
 */
template <class T>
DNBBasicMathMatrix<T>    operator+(const T& left,
                                   const DNBBasicMathMatrix<T>& right)
                         DNB_THROW_SPEC((scl_bad_alloc))
{
    return DNBBasicMathMatrix<T>(left, right, _DNBAddOp() );
}

/** 
 * returns a new matrix where each element is the differnece of corresponding
 * element of the input matrix and the input constant
 */
template <class T>
DNBBasicMathMatrix<T>    operator-(const DNBBasicMathMatrix<T>& left,
                                   const T& right)
                         DNB_THROW_SPEC((scl_bad_alloc))
{
    return DNBBasicMathMatrix<T>(left, right, _DNBSubOp() );
}

/** 
 * returns a new matrix where each element is the difference of corresponding
 * element of the input matrix and the input constant
 */
template <class T>
DNBBasicMathMatrix<T>    operator-(const T& left,
                                   const DNBBasicMathMatrix<T>& right)
                         DNB_THROW_SPEC((scl_bad_alloc))
{
    return DNBBasicMathMatrix<T>(left, right, _DNBSubOp() );
}

/** 
 * returns a new matrix where each element is the product of corresponding
 * element of the input matrix and the input constant
 */
template <class T>
DNBBasicMathMatrix<T>    operator*(const DNBBasicMathMatrix<T>& left,
                                   const T& aVal)
                         DNB_THROW_SPEC((scl_bad_alloc))
{
    return DNBBasicMathMatrix<T>(left, aVal, _DNBMulOp() );
}

/** 
 * returns a new matrix where each element is the product of corresponding
 * element of the input matrix and the input constant
 */
template <class T>
DNBBasicMathMatrix<T>    operator*(const T& aVal,
                                   const DNBBasicMathMatrix<T>& right)
                         DNB_THROW_SPEC((scl_bad_alloc))
{
    return DNBBasicMathMatrix<T>(right, aVal, _DNBMulOp() );
}


/**
  * Returns a new matrix where each element is the product of the two corresponding elements
  * of the two input matrices.
  * The two input matrices must have the same number of rows and columns.
  */
template <class T>
DNBBasicMathMatrix<T>    elementProduct(const DNBBasicMathMatrix<T>& left, 
                                        const DNBBasicMathMatrix<T>& right) 
                                        DNB_THROW_SPEC((scl_bad_alloc))
{
    DNB_PRECONDITION ((left.numColumns() == right.numColumns()) 
                      && (left.numRows() == right.numRows()));
      int n = left.numRows();
      int m = left.numColumns();
      DNBBasicMathMatrix<T> result(n,m);
      for ( int i = 0; i< n ; i++)
        {
          result[i] = left[i] * right[i];
        }
        return result;
}

/** 
 * returns a new matrix where each element is the ratio of corresponding
 * element of the input matrix and the input constant
 */
template <class T>
DNBBasicMathMatrix<T>    operator/(const DNBBasicMathMatrix<T>& left,
                                   const T& aVal)
                         DNB_THROW_SPEC((scl_bad_alloc))
{
    DNB_PRECONDITION(aVal != T(0));
    return DNBBasicMathMatrix<T>(left, aVal, _DNBDivOp() );
}

/** 
 * returns a new matrix where each element is the ratio of input constant and 
 * the corresponding element of the input matrix
 */
template <class T>
DNBBasicMathMatrix<T>    operator/(const T& aVal,
                                   const DNBBasicMathMatrix<T>& right)
                         DNB_THROW_SPEC((scl_bad_alloc))
{
    DNB_PRECONDITION(DNBMinAbsValue(right) != 0);
    return DNBBasicMathMatrix<T>(aVal, right, _DNBDivOp() );
}


/**
 * Returns the min absolute value of all elements
 */
template <class T>
T
DNBMinAbsValue( const DNBBasicMathMatrix<T> &mat )
DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION(mat.numRows() != 0);
    T min = DNBLinfNorm(mat);
    int numRows = mat.numRows();
    T rowMin = 0;
    for (int i = 0; i < numRows; i++)
    {
        rowMin = DNBMinAbsValue(mat.row(i));
        if (min > rowMin)
            min = rowMin;
    }
    return min;
}


/**
 * Returns the max absolute value of all elements
 */
template <class T>
T
DNBLinfNorm( const DNBBasicMathMatrix<T> &mat )
DNB_THROW_SPEC_NULL
{
    T max = 0;
    T rowMax = 0;
    int numRows = mat.numRows();
    for (int i = 0; i < numRows; i++)
    {
        rowMax = DNBLinfNorm(mat.row(i));
        if (max < rowMax)
            max = rowMax;
    }
    return max;
}


/**
 * By definition, this returns the max l1Norm of all columns of the matrix
 */
template <class T>
T
DNBL1Norm( const DNBBasicMathMatrix<T> &mat )
DNB_THROW_SPEC_NULL
{

    T result = 0;
    T max = 0;
    int numCols = mat.numColumns();

    for (int k = 0; k < numCols; k++)
    {
        max = DNBL1Norm(mat.col(k));
        if ( max > result)
            result = max;
    }
    return result;
}

/**
 * Returns the transpose of the given matrix.  Data is copied.
 */
template <class T>
DNBBasicMathMatrix<T>
transpose( const DNBBasicMathMatrix<T> &mat )
DNB_THROW_SPEC_NULL
{
    int numRows = mat.numRows();
    int numCols = mat.numColumns();
    DNBBasicMathMatrix<T> result(numCols, numRows);
    for (int i = 0; i < numRows; i++)
        for (int j = 0; j < numCols; j++)
            result[j][i] = mat[i][j];
    return result;
}



/***********************************************************************************
* Global Streaming in and out
**********************************************************************************/

/**
 * Outputs a matrix m to output stream s row by row, starting with a [ and ending in a ]
 * Each starts on a new line. The numbers are separated by spaces. 
 * Example: below is a 2 by 3 matrix.
 * [
 *  1 2 3
 *  4 5 6
 * ]
 */
template <class T>
ostream&    
operator<<(ostream& s, const DNBBasicMathMatrix<T>& mat)
DNB_THROW_SPEC_NULL
{
    //using std::setw;
    int n =mat.numRows();
    int m = mat.numColumns();
    s << n << "X" << m << ":";
    //int w = s.width((int)0);
    s << "\n[\n";
    for (int i = 0; i < n; i++)   
    {
      for (int j = 0; j < m; j++)         
        s << setw(14) << mat[i][j] << " ";

    s << "\n"; 
    }
    s << "]\n";
    return s;
}



/*********************************************************************************
// Global Methods used by the batch ODT for testing
/********************************************************************************/

template <class T>
bool
DNBNearAbs( const DNBBasicMathMatrix<T> &left, 
            const DNBBasicMathMatrix<T> &right,
            const T &tolerance )
DNB_THROW_SPEC_NULL
{
    if ( tolerance <= T(0) )
        return ( left == right );
    DNBBasicMathMatrix<T> diff = left - right;
    T   normError = DNBLinfNorm(diff);
    return ( normError <= tolerance );
}


template <class T>
bool
DNBNearRel( const DNBBasicMathMatrix<T> &left, 
            const DNBBasicMathMatrix<T> &right,
            const T &tolerance )
DNB_THROW_SPEC_NULL
{
    if ( tolerance <= T(0) )
        return ( left == right );

    T   normLeft  = DNBLinfNorm( left );
    T   normRight = DNBLinfNorm( right );
    T   normError = DNBLinfNorm( left - right );

    return ( normError <= (tolerance * scl_max(normLeft, normRight)) );
}


/******************************************************************************************
* END OF FILE
******************************************************************************************/
