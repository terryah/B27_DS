//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview czo/BPL 01:11:13
 */


#include "DNBTriangle.h"
#include "DNBQuadrilateral.h"


template < class T >
T
DNBQuadrilateralSASSSA1( const T& a, const T& A, 
		         const T& b, const T& c, const T& d )
	DNB_THROW_SPEC_NULL
{
    T e = DNBTriangleSASS( a, A, b );
    return ( DNBTriangleSASA( a, A, b ) + DNBTriangleSSSA( d, c, e ) ); 

}


template < class T >
T
DNBQuadrilateralSASSSA2( const T& a, const T& A, 
		         const T& b, const T& c, const T& d )
	DNB_THROW_SPEC_NULL
{
    T e = DNBTriangleSASS( a, A, b );
    return ( DNBTriangleSSSA( e, d, c ) );

}



template < class T >
T
DNBQuadrilateralSASASS( const T& a, const T& A, 
		         const T& b, const T& B, const T& c )
	DNB_THROW_SPEC_NULL
{
    T e = DNBTriangleSASS( a, A, b );
    return ( DNBTriangleSASS( e, B - DNBTriangleSSSA( a, b, e ), c ));

}


template < class T >
T
DNBQuadrilateralSASASA( const T& a, const T& A, 
		         const T& b, const T& B, const T& c )
	DNB_THROW_SPEC_NULL
{

    T e = DNBTriangleSASS( a, A, b );
    return ( DNBTriangleSASA( e, B - DNBTriangleSSSA( a, b, e ), c ));

}







