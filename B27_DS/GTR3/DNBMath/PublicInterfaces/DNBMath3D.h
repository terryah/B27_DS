//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 */ 

#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_MATH3D_H_
#define _DNB_MATH3D_H_


#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>


#include <DNBMath.h>


/**
  * This class represents the base class for all Cartesian quantities, such as
  *  point vectors and homogeneous transforms.
  */
class ExportedByDNBMath DNBMath3D
{
public:
  /**
    * Specifies the coordinate values of a point vector.
    *
    * <br><B>Description</B><br>
    *     This enumeration type specifies the first three components of a
    *     point vector.
    */
    enum CoordinateIndex
    {
        X = 0,
        Y = 1,
        Z = 2
    };


/**
  * Specifies the scale factor of a point vector.
  * 
  * <br><B>Description</B><br>
  * This enumeration type specifies the fourth component of a point
  * vector.
  * 
  * 
  */
    enum ScaleIndex
    {
        W = 3
    };


/**
  * Specifies the direction vectors of a rotation matrix.
  * 
  * <br><B>Description</B><br>
  * This enumeration type specifies the first three columns of a
  * rotation matrix.
  * 
  * 
  */
    enum DirectionIndex
    {
/**
  * Specifies the direction of the x-axis.
  * 
  * <br><B>Description</B><br>
  * This enumeration constant specifies the first column of a
  * rotation matrix.
  * 
  * 
  */
        N = 0,

/**
  * Specifies the direction of the y-axis.
  * 
  * <br><B>Description</B><br>
  * This enumeration constant specifies the second column of a
  * rotation matrix.
  * 
  * 
  */
        S = 1,

/**
  * Specifies the direction of the z-axis.
  * 
  * <br><B>Description</B><br>
  * This enumeration constant specifies the third column of a
  * rotation matrix.
  * 
  * 
  */
        A = 2
    };


/**
  * Specifies the position vector of a transformation matrix.
  * 
  * <br><B>Description</B><br>
  * This enumeration type specifies the fourth column of a
  * transformation matrix.
  * 
  * 
  */
    enum PositionIndex
    {
/**
  * Specifies the position of the origin.
  * 
  * <br><B>Description</B><br>
  * This enumeration constant specifies the fourth column of a
  * transformation matrix.
  * 
  * 
  */
        P = 3
    };


/**
  * Specifies the three axes of a coordinate system.
  * 
  * <br><B>Description</B><br>
  * This enumeration type specifies the x, y, and z-axes of a coordinate
  * system.
  * 
  * 
  */
    enum AxisType
    {
/**
  * Specifies the x-axis.
  * 
  * <br><B>Description</B><br>
  * This enumeration constant specifies the x-axis of a coordinate
  * system.
  * 
  * 
  */
        XAxis = 0,

/**
  * Specifies the y-axis.
  * 
  * <br><B>Description</B><br>
  * This enumeration constant specifies the y-axis of a coordinate
  * system.
  * 
  * 
  */
        YAxis = 1,

/**
  * Specifies the z-axis.
  * 
  * <br><B>Description</B><br>
  * This enumeration constant specifies the z-axis of a coordinate
  * system.
  * 
  * 
  */
        ZAxis = 2
    };

/**
  * Specifies the Translation opcode
  * 
  * <br><B>Description</B><br>
  * This enumeration type specifies Translate opcode
  * 
  */
    enum TranslateOpcode
    {
        Translate = 0
    };

/**
  * Specifies the three axes of a Rotation matrix.
  * 
  * <br><B>Description</B><br>
  * This enumeration type specifies the x, y, and z-axes of a rotation matrix.
  * 
  */

    enum RotateAxisOpcode
    {

/**
  * Specifies the x-axis.
  * 
  * <br><B>Description</B><br>
  * This enumeration constant specifies the x-axis of a coordinate
  * system.
  *
  */
        RotateX = 0,

/**
  * Specifies the y-axis.
  * 
  * <br><B>Description</B><br>
  * This enumeration constant specifies the y-axis of a coordinate
  * system.
  * 
  * 
  */
        RotateY = 1,

/**
  * Specifies the z-axis.
  * 
  * <br><B>Description</B><br>
  * This enumeration constant specifies the z-axis of a coordinate
  * system.
  * 
  * 
  */
        RotateZ = 2
    };

/**
  * Specifies the Rotational Vector opcode
  * 
  */
    enum RotateVectorOpcode
    {
        RotateK = 0
    };

/**
  * Specifies the reference frame.
  * 
  */
    enum ReferenceFrame
    {
        Origin = 0,
        Local  = 1,
        /* LocalOrigin = 2 */

        World  = 0,
        Entity = 1
        /* EntityWorld = 2 */
    };


/**
  * Specifies the Euler System.
  * 
  */
    enum EulerSystem
    {
        EulerianAngles,
        EulerAngles,
        RollPitchYaw
    };


/**
  * An enum type for the uninitialized type.
  * 
  */
    enum UninitializedType
    {
        Uninitialized
    };

protected:
    //
    //  By defining the following methods in the protected section, this class
    //  can only be used as a base class.
    //
    DNBMath3D( )
        DNB_THROW_SPEC_NULL;

    DNBMath3D( UninitializedType dummy )
        DNB_THROW_SPEC_NULL;

    DNBMath3D( const DNBMath3D &right )
        DNB_THROW_SPEC_NULL;

    ~DNBMath3D( )
        DNB_THROW_SPEC_NULL;

    DNBMath3D &
    operator=( const DNBMath3D &right )
        DNB_THROW_SPEC_NULL;
};




#endif  /* _DNB_MATH3D_H_ */
