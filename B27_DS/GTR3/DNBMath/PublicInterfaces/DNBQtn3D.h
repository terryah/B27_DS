//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 */

//* HISTORY:
//*     Author    Date       Purpose
//*     ------    ----       -------
//*     CZO       09/09/99   Initial implementation
//*     bkh       11/05/03   Implemented CAA style documentation.
//*		smw			01/07/05	Removed extractors due to iostream migration to the
//*								new, standard, templated iostream, and due to migration to 64 bit
//*								builds.  Extractor for all math classes have been moved,
//*								for easy rescucitation, if necessary,
//*								to DNBMath.tst/PrivateInterfaces/DNBMathExtractors.h
//*
//*
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNBQTN3D_H_
#define _DNBQTN3D_H_


#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>


#include <DNBMath3D.h>
#include <DNBVector3D.h>
#include <DNBException.h>




//
//  Forward declarations.
//
template <class T>
class DNBBasicRotate3D;

template <class T>
class DNBBasicEuler3D;

template <class T>
class DNBBasicQtn3D;

template <class T>
T
DNBL1Norm( const DNBBasicQtn3D<T> &qtn )
    DNB_THROW_SPEC_NULL;

template <class T>
T
DNBL2Norm( const DNBBasicQtn3D<T> &qtn )
    DNB_THROW_SPEC_NULL;

template <class T>
T
DNBLinfNorm( const DNBBasicQtn3D<T> &qtn )
    DNB_THROW_SPEC_NULL;

template <class T>
T
DNBL2NormSqr( const DNBBasicQtn3D<T> &qtn )
    DNB_THROW_SPEC_NULL;

template <class T>
bool
DNBNearRel( const DNBBasicQtn3D<T> &left, const DNBBasicQtn3D<T> &right,
    const T &angularTol, const T &linearTol )
    DNB_THROW_SPEC_NULL;

template <class T>
bool
DNBNearAbs( const DNBBasicQtn3D<T> &left, const DNBBasicQtn3D<T> &right,
    const T &angularTol, const T &linearTol )
    DNB_THROW_SPEC_NULL;

template <class T>
bool
operator==( const DNBBasicQtn3D<T> &left, const DNBBasicQtn3D<T> &right )
    DNB_THROW_SPEC_NULL;

template <class T>
bool
operator!=( const DNBBasicQtn3D<T> &left, const DNBBasicQtn3D<T> &right )
    DNB_THROW_SPEC_NULL;

template <class T>
DNBBasicQtn3D<T>
operator*( const DNBBasicQtn3D<T> &left, const DNBBasicQtn3D<T> &right )
    DNB_THROW_SPEC_NULL;

template <class T>
DNBBasicQtn3D<T>
operator/( const DNBBasicQtn3D<T> &left, const DNBBasicQtn3D<T> &right )
    DNB_THROW_SPEC((DNBENumericError));

template <class T>
DNBBasicVector3D<T>
operator*( const DNBBasicQtn3D<T> &left, const DNBBasicVector3D<T> &right )
    DNB_THROW_SPEC_NULL;

template <class T>
ostream &
operator<<( ostream &ostr, const DNBBasicQtn3D<T> &item )
    DNB_THROW_SPEC((scl_bad_alloc));




/**
  * A templated class that represents a three-dimensional rotation.
  *
  * <br><B>Template Parameter(s)</B><br>
  * @param T
  * The base type of the quaternion elements.
  *
  * <br><B>Description</B><br>
  * This module defines a class quaternion that contains 4 values. A
  * quaterion is equivalent to a 3X3 cosine matrix which represents
  * a three dimensional rotation, thus a quaternion can be viewed as
  * a rotational operator. This class is intended to be used to efficiently
  * model a 3-D rotation.
  *
  *
  */
template <class T>
class DNBBasicQtn3D : public DNBMath3D
{
public:
/**
  * Creates the identity quaternion.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function creates a quaternion which represents the identity
  * rotation.
  *
  *
  */
    DNBBasicQtn3D( )
    DNB_THROW_SPEC_NULL;

/**
  * Creates a quaternion using copy semantics.
  * @param right
  * The quaternion to be copied.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function copies the elements of <tt>right</tt> to <tt>self</tt>.
  *
  *
  */
    DNBBasicQtn3D( const DNBBasicQtn3D<T> &right )
	DNB_THROW_SPEC_NULL;

/**
  * Creates an uninitialized quaternion.
  * @param dummy
  * The UninitializedType object.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function creates a quaternion without setting its
  * elements to the valid state, thus returns quickly. The existing
  * of this function is purely for efficiency. See the example below.
  *
  * EXAMPLES:
  * DNBQtn3D    qtn( DNBMath3D::Uninitialized );
  * xform.getQtn( qtn );
  *
  *
  */
    DNBBasicQtn3D( UninitializedType dummy )
	DNB_THROW_SPEC_NULL;

/**
  * Destroys <tt>self</tt>.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function destroys <tt>self</tt>.
  *
  *
  */
    ~DNBBasicQtn3D( )
	DNB_THROW_SPEC_NULL;

/**
  * Gets the equivalent axis and angle.
  * @param axis
  * The 3-D vector object  to receive the axis.
  * @param angle
  * The base type object to receive the angle.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function gets <tt>self</tt>'s equivalent axis and angle.
  *
  *
  */
    void
    getEAA( DNBBasicVector3D<T> &axis, T &angle ) const
	DNB_THROW_SPEC_NULL;

/**
  * Gets <tt>self</tt>'s equivalent 3X3 cosine matrix.
  * @param rotate
  * The 3-D rotation object to receive the cosine matrix.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function gets <tt>self</tt>'s equivalent 3X3 cosine matrix.
  *
  *
  */
    void
    getRotate( DNBBasicRotate3D<T> &rotate ) const
	DNB_THROW_SPEC_NULL;

/**
  * Gets <tt>self</tt>'s equivalent roll, pitch and yaw.
  * @param roll
  * The base type object to receive the roll value.
  * @param pitch
  * The base type object to receive the pitch value.
  * @param yaw
  * The base type object to recieve the yaw value.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function gets <tt>self</tt>'s equivalent roll, pitch and yaw. The
  * corresponding rotation axes are Z, Y and X.
  *
  *
  */
    void
    getRPY( T &roll, T &pitch, T &yaw ) const
	DNB_THROW_SPEC_NULL;

/**
  * Gets the three columns of <tt>self</tt>'s equivalent cosine matrix.
  * @param vecN
  * The 3-D vector object to receive the column X.
  * @param vecN
  * The 3-D vector object to receive the column Y.
  * @param vecN
  * The 3-D vector object to receive the column Z.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function retrieves the three columns of <tt>self</tt>'s equivalent
  * cosine matrix.
  *
  *
  */
    void
    getNSA( DNBBasicVector3D<T> &vecN,
	    DNBBasicVector3D<T> &vecS,
	    DNBBasicVector3D<T> &vecA ) const
	DNB_THROW_SPEC_NULL;

/**
  * Gets the raw values of <tt>self</tt>.
  * @param scalar
  * The base type object to receive the scalar component.
  * @param vec
  * The 3-D vector object to receive the vector component.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function gets the raw values of <tt>self</tt>, the scalar component
  * and the vector component.
  *
  *
  */
    void
    get( T &scalar, DNBBasicVector3D<T> &vec ) const
	DNB_THROW_SPEC_NULL;

/**
  * Sets <tt>self</tt> to the rotation that is equivalent to rotate a certain
  * angle about the given axis.
  * @param axis
  * The 3-D vector object representing the axis to rotate about.
  * @param angle
  * The base type object representing the angle to rotate.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function Sets <tt>self</tt> to the rotation that is equivalent
  * to rotate a certain angle about a given axis.
  * @exception DNBEZeroVector
  * The argument <tt>axis</tt> is zero length vector.
  *
  *
  */
    void
    setEAA( const DNBBasicVector3D<T> &axis, const T &angle )
	DNB_THROW_SPEC((DNBEZeroVector));

/**
  * Set <tt>self</tt> to the rotation that is equivalent to rotate a certain
  * angle about a given coordinate axis.
  * @param axisXYZ
  * The AxisType object represents the coordinate axis to
  * rotate about.
  * @param angle
  * The angle to rotate.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function scl_set <tt>self</tt> to the rotation that is equivalent
  * to rotate a certain angle about a given coordinate axis.
  *
  *
  */
    void
    setCAA( AxisType axisXYZ, const T &angle )
	DNB_THROW_SPEC_NULL;

/**
  * Sets <tt>self</tt> to the rotation that is equivalent to an Euler.
  * @param euler
  * The Euler object represents three successive rotations about
  * three coordinate axes.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function sets <tt>self</tt> to the rotation represented by an Euler
  * which is three successive rotations about three coordinate axes.
  *
  *
  */
    void
    setEuler( const DNBBasicEuler3D<T> &euler )
	DNB_THROW_SPEC_NULL;

/**
  * Sets <tt>self</tt> to the rotation that is equivalent to three successive
  * rotations about Z, Y and X, respectively.
  * @param roll
  * The angle to rotate about axis Z.
  * @param pitch
  * The angle to rotate about axis Y.
  * @param yaw
  * The angle to rotate about axis X.
  *
  * <br><B>Description</B><br>
  * This function sets <tt>self</tt> to the rotation that is equivalent
  * to three successive rotations about Z, Y and X, respectively.
  *
  *
  */
    void
    setRPY( const T &roll, const T &pitch, const T &yaw )
	DNB_THROW_SPEC_NULL;

/**
  * Sets <tt>self</tt> to the rotation represented by a 3X3 cosine matrix.
  * @param rotate
  * The 3X3 cosine matrix representing a 3D rotation.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function sets <tt>self</tt> to the rotation represented by
  * a 3X3 cosine matrix.
  *
  *
  */
    void
    setRotate( const DNBBasicRotate3D<T> &rotate )
	DNB_THROW_SPEC_NULL;

/**
  * Sets <tt>self</tt> to the rotation represented by three vectors.
  * @param vecN
  * The 3-D vector representing the rotated X axis.
  * @param vecS
  * The 3-D vector representing the rotated Y axis.
  * @param vecA
  * The 3-D vector representing the rotated Z axis.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function creates a rotation cosine matrix from three vectors,
  * and then assigns this rotation to <tt>self</tt>.
  * @exception DNBESingularMatrix
  * The given three vectors form a singular matrix.
  * @exception DNBEIllegalMatrix
  * The given three vectors do not form a valid
  * right-hand ruled rotation cosine matrix.
  *
  *
  */
    void
    setNSA( const DNBBasicVector3D<T> &vecN,
	    const DNBBasicVector3D<T> &vecS,
	    const DNBBasicVector3D<T> &vecA )
	DNB_THROW_SPEC((DNBESingularMatrix, DNBEIllegalMatrix));

/**
  * Sets <tt>self</tt>'s raw values.
  * @param scalar
  * The scalar component.
  * @param vec
  * The vector component.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function scl_set <tt>self</tt> from the given raw values.
  * @exception DNBENumericError
  * The given raw values do not form a unit length quaternion.
  *
  *
  */
    void
    set( const T &scalar, const DNBBasicVector3D<T> &vec )
	DNB_THROW_SPEC((DNBENumericError));

    T
    norm( ) const
	DNB_THROW_SPEC_NULL;

/**
  * Normalizes <tt>self</tt>.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function normalizes <tt>self</tt>, after the normalization, <tt>self</tt>
  * is a unit length quaternion.
  * @exception DNBENumericError
  * The quaternion could not be unitized, because its length
  * is zero.
  *
  *
  */
    void
    normalize( )
	DNB_THROW_SPEC((DNBENumericError));

/**
  * Sets <tt>self</tt> to its conjugate.
  *
  * @return
  * The conjugate of <tt>self</tt>.
  *
  * <br><B>Description</B><br>
  * This function sets <tt>self</tt> to its conjugate.
  *
  *
  */
    DNBBasicQtn3D<T>
    conjugate( )
	DNB_THROW_SPEC_NULL;

/**
  * Inverts <tt>self</tt>.
  *
  * @return
  * The inverted quaternion.
  *
  * <br><B>Description</B><br>
  * This function inverts <tt>self</tt>.
  * @exception DNBENumericError
  * This quaternion could not be inverted.
  *
  *
  */
    DNBBasicQtn3D<T>
    invert( )
	DNB_THROW_SPEC((DNBENumericError));

/**
  * Negates <tt>self</tt>.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function negates <tt>self</tt>. In representing a 3-D rotation, a
  * quaternion and its negative counterpart are equivalent. The function
  * is primarily used to choose the acute angle to avoid the unneccessary
  * rotation in interpolation.
  *
  *
  */
    void
    negate( )
	DNB_THROW_SPEC_NULL;

/**
  * Computes the dot-product of <tt>self</tt> with another quaternion.
  * @param other
  * The quaternion object.
  *
  * @return
  * The value of the dot-product.
  *
  * <br><B>Description</B><br>
  * This function computes the dot-product of <tt>self</tt> with another
  * quaternion. The quaternions are treated as a four-valued vectors.
  *
  * EXCEPTION:
  *
  *
  */
    T
    dot( const DNBBasicQtn3D<T> &other ) const
	DNB_THROW_SPEC_NULL;

/**
  * Exchanges the value of <tt>self</tt> with that of the given quaternion.
  * @param right
  * The quaternion object.
  *
  * RETRURNS:
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function exchanges the value of <tt>self</tt> with
  * that of the given quaternion.
  *
  *
  */
    void
    swap( DNBBasicQtn3D<T> &right )
	DNB_THROW_SPEC_NULL;

/**
  * Rotates <tt>self</tt> about a principle axis.
  * @param axis
  * The coordinate axis to rotate about.
  * @param angle
  * The angle to rotate.
  * @param refFrame
  * The reference frame for the rotation.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function constructs a quaternion from the give axis and angle,
  * and multiplies it by <tt>self</tt> if <tt>refFrame</tt> is "DNBMath3D::Local",
  * or multiplies <tt>self</tt> by the quaternion if <tt>refFrame</tt> is
  * "DNBMath3D::Origin", and then stores the result in <tt>self</tt>.
  *
  *
  */
    void
    rotate( AxisType axis, const T &angle, ReferenceFrame refFrame = Local )
	DNB_THROW_SPEC_NULL;

/**
  * Rotates <tt>self</tt> about an arbitrarily given axis.
  * @param axis
  * The axis to rotate about.
  * @param angle
  * The angle to rotate.
  * @param refFrame
  * The reference frame for the rotation.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function constructs a quaternion from the given axis and angle,
  * and then multiplies it by <tt>self</tt> if <tt>refFrame</tt> is "DNBMath3D::Local",
  * or multiplies <tt>self</tt> by the quaternion if <tt>refFrame</tt> is
  * "DNBMath3D::Origin", and then stores the result in <tt>self</tt>.
  * @exception DNBEZeroVector
  * The given vecotor's length is zero.
  *
  *
  */
    void
    rotate( const DNBBasicVector3D<T> &vec, const T &angle,
	    ReferenceFrame refFrame = Local )
	DNB_THROW_SPEC((DNBEZeroVector));

/**
  * Rotates <tt>self</tt> by the given quaternion <tt>qtn</tt>.
  * @param qtn
  * The quaternion to be applied to <tt>self</tt>.
  * @param refFrame
  * The reference frame for the rotation.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function multiplies the given quaternion <tt>qtn</tt> by <tt>self</tt> if
  * <tt>refFrame</tt> is "Origin", or multiplies <tt>self</tt> by <tt>qtn</tt> if
  * <tt>refFrame</tt> is "Local", and the stores the result in <tt>self</tt>.
  *
  *
  */
    void
    rotate( const DNBBasicQtn3D<T> &qtn, ReferenceFrame refFrame = Local )
	DNB_THROW_SPEC_NULL;

/**
  * Multiplies the given quaternion <tt>qtn</tt> by <tt>self</tt>, and then stores
  * the result in <tt>self</tt>.
  * @param qtn
  * The quaternion object.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function multiplies the give quaternion <tt>qtn</tt> by <tt>self</tt>, and
  * the stores the result in <tt>self</tt>.
  *
  *
  */
    void
    preMultiply( const DNBBasicQtn3D<T> &qtn )
	DNB_THROW_SPEC_NULL;

/**
  * Mulitplies <tt>self</tt> by the given quaternion <tt>qtn</tt>, and then stores
  * the result in <tt>self</tt>.
  * @param qtn
  * The quaternion object.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function mulitplies <tt>self</tt> by the given quaternion <tt>qtn</tt>,
  * and then stores the result in <tt>self</tt>.
  *
  *
  */
    void
    postMultiply( const DNBBasicQtn3D<T> &qtn )
	DNB_THROW_SPEC_NULL;

    DNBBasicQtn3D<T>
    operator-( ) const
	DNB_THROW_SPEC_NULL;

/**
  * Copy assignment.
  * @param right
  * The quaternion object.
  *
  * @return
  * The reference to <tt>self</tt>.
  *
  * <br><B>Description</B><br>
  * This function copies the given quaternion to <tt>self</tt>.
  *
  * EXCEPTION:
  *
  *
  */
    DNBBasicQtn3D<T> &
    operator=( const DNBBasicQtn3D<T> &right )
	DNB_THROW_SPEC_NULL;

/**
  * Concatenates <tt>self</tt> with the given quaternion.
  * @param right
  * The quaternion to be multiplied.
  *
  * @return
  * A reference to <tt>self</tt>.
  *
  * <br><B>Description</B><br>
  * This function concatenates <tt>self</tt> with the given quaternion, and
  * stores the result in <tt>self</tt>.
  *
  *
  */
    DNBBasicQtn3D<T> &
    operator*=( const DNBBasicQtn3D<T> &right )
	DNB_THROW_SPEC_NULL;

/**
  * Post-multiplies <tt>self</tt> by the inverse of the given quaterion.
  * @param right
  * The quaternion to be divided.
  *
  * @return
  * A reference to <tt>self</tt>.
  *
  * <br><B>Description</B><br>
  * This function post-multiplies <tt>self</tt> by the inverse of the
  * given quaternion and stores the result in <tt>self</tt>.
  * @exception DNBENumericError
  * The given quaterion <tt>righ</tt> could not be inverted.
  *
  *
  */
    DNBBasicQtn3D<T> &
    operator/=( const DNBBasicQtn3D<T> &right )
	DNB_THROW_SPEC((DNBENumericError));

/**
  * Rotates the given vector.
  * @param vec
  * The vector to be rotated.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function rotates the given vector and stores the result in
  * the given vector.
  *
  *
  */
    void
    rotateVec( DNBBasicVector3D<T> &vec ) const
	DNB_THROW_SPEC_NULL;

#if DNB_HAS_FRIEND_TEMPLATE

/**
  * Computes the L-1 norm of the given quaternion.
  * @param qtn
  * The quaternion object that represents the operand.
  *
  * @return
  * The L-1 norm of <tt>qtn</tt>
  *
  * <br><B>Description</B><br>
  * This function computes the L-1 norm of the given quaternion.
  *
  *
  */
    friend ExportedByDNBMath T
    DNBL1Norm( const DNBBasicQtn3D<T> &qtn )
	DNB_THROW_SPEC_NULL;

/**
  * Computes the L-2 norm of the given quaternion.
  * @param qtn
  * The quaternion object that represents the operand.
  *
  * @return
  * The L-2 norm of <tt>qtn</tt>
  *
  * <br><B>Description</B><br>
  * This function computes the L-2 norm of the given quaternion.
  *
  *
  */
    friend ExportedByDNBMath T
    DNBL2Norm( const DNBBasicQtn3D<T> &qtn )
	DNB_THROW_SPEC_NULL;

/**
  * Computes the L-infinity norm of the given quaternion.
  * @param qtn
  * The quaternion object that represents the operand.
  *
  * @return
  * The L-infinity norm of <tt>qtn</tt>
  *
  * <br><B>Description</B><br>
  * This function computes the L-infinity norm of the given quaternion.
  *
  *
  */
    friend ExportedByDNBMath T
    DNBLinfNorm( const DNBBasicQtn3D<T> &qtn )
	DNB_THROW_SPEC_NULL;

/**
  * Computes the square of the L-2 norm of the given quaternion.
  * @param qtn
  * The quaternion object that represents the operand.
  *
  * @return
  * The square of the L-2 norm of <tt>qtn</tt>
  *
  * <br><B>Description</B><br>
  * This function computes the square of the L-2 norm of the
  * given quaternion.
  *
  *
  */
    friend ExportedByDNBMath T
    DNBL2NormSqr( const DNBBasicQtn3D<T> &qtn )
	DNB_THROW_SPEC_NULL;

/**
  * Determines if a pair of quaternions are "relatively"
  * close to one another.
  * @param left
  * The quaternion object representing the left operand.
  * @param right
  * The quaternion object representing the right operand.
  * @param angularTol
  * The relative tolerance used in comparing the angles.
  * @param linearTol
  * The relative tolerance used in comparing the axes.
  *
  * @return
  * The bool value true, if <tt>left</tt> is "relatively" close to <tt>right</tt>,
  * and false otherwise.
  *
  * <br><B>Description</B><br>
  * This function determines if a pair of quaternions are "relatively"
  * close to one another.
  *
  *
  */
    friend ExportedByDNBMath bool
    DNBNearRel( const DNBBasicQtn3D<T> &left, const DNBBasicQtn3D<T> &right,
		const T &angularTol, const T &linearTol )
        DNB_THROW_SPEC_NULL;

/**
  * Determines if a pair of quaternions are "absolutely"
  * close to one another.
  * @param left
  * The quaternion object representing the left operand.
  * @param right
  * The quaternion object representing the right operand.
  * @param angularTol
  * The relative tolerance used in comparing the angles.
  * @param linearTol
  * The relative tolerance used in comparing the axes.
  *
  * @return
  * The bool value true, if <tt>left</tt> is "absolutely" close to <tt>right</tt>,
  * and false otherwise.
  *
  * <br><B>Description</B><br>
  * This function determines if a pair of quaternions are "absolutely"
  * close to one another.
  *
  *
  */
    friend ExportedByDNBMath bool
    DNBNearAbs( const DNBBasicQtn3D<T> &left, const DNBBasicQtn3D<T> &right,
		const T &angularTol, const T &linearTol )
        DNB_THROW_SPEC_NULL;

/**
  * Determines if the given two quaternions are equivalent.
  * @param left
  * The quaternion object <tt>left</tt> representing the left operand.
  * @param right
  * The quaternion object <tt>left</tt> representing the right operand.
  *
  * @return
  * The bool value true, if <tt>left</tt> is equivalent to <tt>right</tt>,
  * false otherwise.
  *
  * <br><B>Description</B><br>
  * This function determines if the given two quaternions are equivalent.
  *
  *
  */
    friend ExportedByDNBMath bool
    operator==( const DNBBasicQtn3D<T> &left, const DNBBasicQtn3D<T> &right )
        DNB_THROW_SPEC_NULL;

/**
  * Determines if the given two quaternions are equivalent.
  * @param left
  * The quaternion object <tt>left</tt> representing the left operand.
  * @param right
  * The quaternion object <tt>left</tt> representing the right operand.
  *
  * @return
  * The bool value true, if <tt>left</tt> is equivalent to <tt>right</tt>,
  * false otherwise.
  *
  * <br><B>Description</B><br>
  * This function determines if the given two quaternions are equivalent.
  *
  *
  */
    friend ExportedByDNBMath bool
    operator!=( const DNBBasicQtn3D<T> &left, const DNBBasicQtn3D<T> &right )
        DNB_THROW_SPEC_NULL;

/**
  * Multiplies a pair of quaternions.
  * @param left
  * The quaternion representing the left operand.
  * @param right
  * The quaternion representing the right operand.
  *
  * @return
  * The product of the given two quaternions.
  *
  * <br><B>Description</B><br>
  * This function computes the product of the give two quaternions.
  *
  *
  */
    friend ExportedByDNBMath DNBBasicQtn3D<T>
    operator*( const DNBBasicQtn3D<T> &left, const DNBBasicQtn3D<T> &right )
        DNB_THROW_SPEC_NULL;

/**
  * Divides a pair of quaternions.
  * @param left
  * The quaternion representing the left operand.
  * @param right
  * The quaternion representing the right operand.
  *
  * @return
  * The quaterion corresponding to "left / right".
  *
  * <br><B>Description</B><br>
  * This function divides one quaternion by another.
  * @exception DNBENumericError
  * The quaternion to divide could not be inverted.
  *
  *
  */
    friend ExportedByDNBMath DNBBasicQtn3D<T>
    operator/( const DNBBasicQtn3D<T> &left, const DNBBasicQtn3D<T> &right )
        DNB_THROW_SPEC((DNBENumericError));

/**
  * Rotates a vector.
  * @param left
  * The quaternion representing the left operand.
  * @param right
  * The vector representing the left operand.
  *
  * @return
  * The rotated vector.
  *
  * <br><B>Description</B><br>
  * This function multiplies a quaternion by a vector and returns the
  * result by value.
  *
  *
  */
    friend ExportedByDNBMath DNBBasicVector3D<T>
    operator*( const DNBBasicQtn3D<T> &left, const DNBBasicVector3D<T> &right )
        DNB_THROW_SPEC_NULL;

/**
  * Writes a quaternion to an output stream.
  * @param ostr
  * The output stream that is to receive the data.
  * @param item
  * The qtn to be inserted.
  *
  * @return
  * A reference to <tt>ostr</tt>.
  *
  * <br><B>Description</B><br>
  * This function writes a quaternion to a output stream.
  * @exception scl_bad_alloc
  * The internal buffer could not be allocated due to the
  * insufficient memory.
  *
  *
  */
    friend ExportedByDNBMath ostream &
    operator<<( ostream &ostr, const DNBBasicQtn3D<T> &item )
        DNB_THROW_SPEC((scl_bad_alloc));

#endif  /* DNB_HAS_FRIEND_TEMPLATE */

    static void
    EAAInterp( DNBBasicQtn3D<T> &res, T alpha,
               const DNBBasicQtn3D<T> &from, const DNBBasicQtn3D<T> &to )
        DNB_THROW_SPEC_NULL;


/**
  * Spherical interpolation between two quaternions.
  * @param result
  * The quaternion object to receive the result.
  * @param alpha
  * The base type object between 0 and 1, inclusive.
  * @param from
  * The starting quaternion.
  * @param to
  * The ending quaternion.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function computes the spherical linear interpolated quaternion
  * between two quaternions at the requestiong point <tt>alpha</tt>.
  *
  *
  */
    static void
    slerp( DNBBasicQtn3D<T> &result, const T &alpha,
           const DNBBasicQtn3D<T> &from, const DNBBasicQtn3D<T> &to )
        DNB_THROW_SPEC_NULL;

/**
  * Spherical cubic interpolation.
  * @param result
  * The quaternion to receive the result.
  * @param alpha
  * The coefficient between 0 and 1.
  * @param from
  * The starting quaternion.
  * @param viaA
  * The quaterion that the interpolated line passes thru.
  * @param viaB
  * The quaterion that the interpolated line passes thru.
  * @param to
  * The ending quaternion.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function computes the spherical cubic interpolation from
  * starting quaternion <tt>from</tt> via two intermediate quaternions
  * <tt>viaA</tt> and <tt>viaB</tt> to the end quaternion <tt>to</tt>.
  *
  *
  */
    static void
    squad( DNBBasicQtn3D<T> &result, const T &alpha,
	   const DNBBasicQtn3D<T> &from, const DNBBasicQtn3D<T> &viaA,
	   const DNBBasicQtn3D<T> &viaB, const DNBBasicQtn3D<T> &to )
        DNB_THROW_SPEC_NULL;

/**
  * The identity quaternion.
  *
  * <br><B>Description</B><br>
  * This object defines a quaternion that represents no rotation.
  * The values of its elements are as follows:
  *
  * [1  0  0  0]
  *
  *
  */
    static const DNBBasicQtn3D<T> Identity;

#if DNB_HAS_FRIEND_TEMPLATE
private:
#else
public:
#endif
    static const T  Tolerance;

    static void
    multiply( DNBBasicQtn3D<T> &result, const DNBBasicQtn3D<T> &left,
	      const DNBBasicQtn3D<T> &right )
        DNB_THROW_SPEC_NULL;

    static void
    multiply( DNBBasicVector3D<T> &result, const DNBBasicQtn3D<T> &left,
	      const DNBBasicVector3D<T> &right )
        DNB_THROW_SPEC_NULL;


    T                   s_;
    DNBBasicVector3D<T> v_;
};


typedef DNBBasicQtn3D<DNBReal> DNBQtn3D;


//
//  Include the public definition file.
//
#if     defined(DNB_COMPILE_INSTANTIATE) || defined(_DNBQTN3D_CC_)
#include "DNBQtn3D.cc"
#elif   defined(_WINDOWS_SOURCE)
/*
extern DNB_DEFINE_CLASS_INST ExportedByDNBMath
DNBBasicQtn3D<float>; */

extern DNB_DEFINE_CLASS_INST ExportedByDNBMath
DNBBasicQtn3D<double>;
#endif


#endif  /* _DNBQTN3D_H_ */
