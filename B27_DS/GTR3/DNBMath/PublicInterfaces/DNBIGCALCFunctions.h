//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 */

//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     bpl         09/19/2000  Initial Implementation
//*     bkh         11/04/03    Implementation of new documentation style.
//* 


#ifndef _DNB_IGCALCFUNCTIONS_H_
#define _DNB_IGCALCFUNCTIONS_H_


#include <DNBSystemBase.h>
#include <scl_limits.h>
#include <DNBSystemDefs.h>

/**
  * Returns 1 if b <tt>= a <tt> c   ( i.e. a</tt>=b && a<tt>c )
  * 
  * <br><B>Template Parameter(s)</B><br>
  * @param  T 
  * <tt>T</tt> is the numerical type
  * A @href a
  * Value to be compaired
  * A @href b
  * Lower limit
  * A @href c
  * Upper limit
  * 
  * @return
  * T
  * 
  * <br><B>Description</B><br>
  * Returns 1 if b <tt>= a <tt> c, zero otherwise
  * ( i.e. Returns 1 if a</tt>=b && a<tt>c, zero otherwise )
  * The effect of c </tt> b is not fomaly defined
  * This verion is not valid for numeric types that can not convert 
  * the value '1'
  * 
  * 
  */
template < class T >
T
DNBInrange( const T& a, const T& b, const T& c )
	DNB_THROW_SPEC_NULL;

/**
  * Returns a if b<tt>a, c if b</tt>c, b otherwise
  * 
  * <br><B>Template Parameter(s)</B><br>
  * @exception  T 
  * <tt>T</tt> is the numerical type
  * A @href a
  * Lower limit
  * A @href b
  * Value to be ranged
  * A @href c
  * Upper limit
  * 
  * @return
  * T
  * 
  * <br><B>Description</B><br>
  * Returns a if b<tt>a, c if b</tt>c, b otherwise.
  * Assumes a<tt>c
  * 
  * 
  */
template < class T >
T
DNBRange( const T& a, const T& b, const T& c )
	DNB_THROW_SPEC_NULL;

/**
  * Ramps xx on slope between (x1, y1) and (x2, y2)
  * 
  * <br><B>Template Parameter(s)</B><br>
  * @exception  T 
  * <tt>T</tt> is the numerical type
  * A @href xx
  * input x value
  * A @href x1
  * x value, first point
  * A @href y1
  * y value, first point
  * A @href x2
  * x value, second point
  * A @href y2
  * y value, second point
  * 
  * @return
  * T
  * 
  * <br><B>Description</B><br>
  * Returns the yy value for the given xx and the slope between (x1,y1)
  * and (x2,y2). As a bonus, values below y1 are set to y1 and values 
  * above y2 are set to y2.  (So, y1 must be less than y2!)
  * 
  * 
  */
template < class T >
T
DNBRamp( const T& xx, const T& x1, const T& y1, const T& x2, const T& y2 )
	DNB_THROW_SPEC_NULL;

/**
  * Returns the interger value of the value passed in
  * 
  * <br><B>Template Parameter(s)</B><br>
  * @exception  T 
  * <tt>T</tt> is the numerical type
  * A @href a
  * Input value
  * 
  * @return
  * T
  * 
  * <br><B>Description</B><br>
  * Returns the interger value of the value passed in.
  * Type T must be able to convert 'a' to a DNBInteger32.
  * 
  * 
  */
template < class T >
T
DNBInt( const T& a )
	DNB_THROW_SPEC_NULL;


#include "DNBIGCALCFunctions.cc"


#endif
