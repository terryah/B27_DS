//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 */ 

//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     vjk         01/11/99    Initial implementation.
//*     smw         05/26/03    Changed from templated class DNBBasicMathConst to namespace DNBMathConst with DNBReal as the type,
//*                             Removed the typedef and the DNBMathCons.cc file
//*     smw         06/22/03    Since SunOS does not support namespaces, addded conditional 
//*                             compilation so that under SunOS, constants are defined in a
//*                             non-templated class as static variables
//*     bkh         11/05/03    Implementation of CAA style documentation.
//* 
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNBMATHCONST_H_
#define _DNBMATHCONST_H_


#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>



#include <DNBMath.h>


// Definition of BSD Unix constants; for compliance with C and a flexible C++ approach
// SMW: temporarily comment out due to the following build errors on AIX:
// DNBIgpCalibCommands/DNBIgpOlpCalibration.m/src/DNBIgpCalibUtils.cpp
// "/usr/include/math.h", line 231.9: 1540-0848 (S) The macro name "M_E" 
//  is already defined with a different definition.
//  Our hearder is being pulled before the os's math header, and the os's math hearder does not have 
//  #ifndef .... #define. 
// FIXME

#ifndef _AIX_SOURCE

#ifndef M_E
#define M_E             2.7182818284590452354       // e
#endif
#ifndef M_LOG2E
#define M_LOG2E         1.4426950408889634074       // log base 2 of e
#endif
#ifndef M_LOG10E
#define M_LOG10E        0.43429448190325182765      // log base 10 of e
#endif
#ifndef M_LN2
#define M_LN2           0.69314718055994530942      // natural log (i.e. log base e) of 2 
#endif
#ifndef M_LN10
#define M_LN10          2.30258509299404568402      // natual log (i.e. log base e) of 10
#endif
#ifndef M_PI
#define M_PI            3.14159265358979323846      // PI
#endif
#ifndef M_PI_2
#define M_PI_2          1.57079632679489661923      // PI over 2
#endif
#ifndef M_PI_4
#define M_PI_4          0.78539816339744830962      // PI over 4
#endif
#ifndef M_1_PI
#define M_1_PI          0.31830988618379067154      // 1 over PI
#endif
#ifndef M_2_PI
#define M_2_PI          0.63661977236758134308      // 2 over PI
#endif
#ifndef M_2_SQRTPI
#define M_2_SQRTPI      1.12837916709551257390      // 2 over square root of PI
#endif
#ifndef M_SQRT2
#define M_SQRT2         1.41421356237309504880      // square root of 2
#endif
#ifndef M_SQRT1_2
#define M_SQRT1_2       0.70710678118654752440      // square root of 1/2
#endif

#endif // _AIX_SOURCE

/**
  * Mathematical constants which arise in engineering analysis.
  * 
  * <br><B>Description</B><br>
  * This namespace defines various mathematical constants which are used in the
  * Xenon toolkit.
  * 
  * <br><B>Example</B><br>
  * <pre>
  * #include DNBSystemBase.h
  * #include DNB_IOSTREAM
  * #include DNBSystemDefs.h
  * #include DNBCMath.h
  * #include DNBMathConst.h
  * 
  * //
  * // Calculate the area of a circle with the specified radius
  * //
  * DNBReal
  * calcArea( DNBReal radius )
  * DNB_THROW_SPEC_NULL
  * {
  * return ( DNBMathConst::Pi * DNBSqr(radius) );
  * }
  * 
  * int
  * main( )
  * {
  * DNBReal SIArea = calcArea( 4.0 );
  * cout << "computed area = " << SIArea << " square meters" << endl;
  * cout << "expected area = 50.2655 square meters" << endl;
  * 
  * return (0);
  * }
  * 
  * </pre>
  *
  */

#if !defined(_SUNOS_SOURCE)
namespace DNBMathConst
{
    //
    //  Values derived from PI.
    //

/**
  * This constant specifies the value of PI.
  * 
  * 
  */
    const DNBReal Pi = DNBReal(3.14159265358979323846); // M_PI


/**
  * This constant specifies the value of two times PI.
  * 
  * 
  */
    const DNBReal _2_Pi = DNBReal(6.28318530717958647692);


/**
  * This constant specifies the value of PI divided by two.
  * 
  * 
  */
    const DNBReal Pi_2 = 1.57079632679489661923; // M_PI_2


/**
  * This constant specifies the value of PI divided by four.
  * 
  * 
  */
    const DNBReal Pi_4 = 0.78539816339744830962 ; //M_PI_4;


    //
    //  Irrational values.
    //

/**
  * This constant specifies the value of square root of two.
  * 
  * 
  */
    const DNBReal Sqrt_2 = 1.41421356237309504880; //M_SQRT2;


/**
  * This constant specifies the value of square root of three.
  * 
  * 
  */
    const DNBReal Sqrt_3 = 1.7320508075688772935;
};

#else  /* SUNOS_SOURCE is defined */
class DNBMathConst
{
public:
   /**  @nodoc (since already documented above)     
     * This constant specifies the value of PI.
    */
    static const DNBReal Pi;


    /** @nodoc
      * This constant specifies the value of two times PI.
      *
      */
    static const DNBReal _2_Pi;


    /**
      * This constant specifies the value of PI divided by two.
     */
    static const DNBReal Pi_2;


    static const DNBReal Pi_4;

    //
    //  Irrational values.
    //

    static const DNBReal Sqrt_2;


    static const DNBReal Sqrt_3;
};

/* The actual definition of the constants; for the Solaris platform*/
//
//  Values derived from PI.
//
const DNBReal
DNBMathConst::Pi        = DNBReal( 3.14159265358979323846); // M_PI

const DNBReal
DNBMathConst::_2_Pi     = DNBReal( 6.28318530717958647692 );

const DNBReal
DNBMathConst::Pi_2      = DNBReal( 1.57079632679489661923 ); // M_PI_2

const DNBReal
DNBMathConst::Pi_4      = DNBReal( 0.78539816339744830962); // M_PI_4


//
//  Irrational values.
//
const DNBReal
DNBMathConst::Sqrt_2    = DNBReal( 1.4142135623730950488 );

const DNBReal
DNBMathConst::Sqrt_3    = DNBReal( 1.7320508075688772935 );


#endif   /* SUNOS_SOURCE */

#endif  /* _DNBMATHCONST_H_ */
