/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview CZO smw 03:09:12
 */
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2002
//==============================================================================
//
// DNBMathMatrix.h
//      Interface for the DNBMathMatrix templated class.
//      Provides a n-dimensional vector of m-dimensional
//      vectors ( equivalent to a two dimensional array), with capability to do 
//      Linear Algebra mathematical operations.
//      Provides a replacement for RWGenMat
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//      smw         6/18/2003       Initial Implementation
//      bkh         7/16/2003       Added functions increment, decrement and assignment operator.
//      bkh         7/22/2003       Added Functions - Matrix multiplication, division, inverse.
//      bkh         8/07/2003       Added Global streaming in and out
//      smw         12/02/2003      Clean up and use of DNBPoolAllocator
//*		smw			01/07/2005	Removed extractors due to iostream migration to the
//*								new, standard, templated iostream, and due to migration to 64 bit
//*								builds.  Extractor for all math classes have been moved, 
//*								for easy rescucitation, if necessary,
//*								to DNBMath.tst/PrivateInterfaces/DNBMathExtractors.h
//
//===============================================================================================

#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _DNBMATHMATRIX_H_
#define _DNBMATHMATRIX_H_

#include <DNBSystemBase.h>
#include <scl_iomanip.h>        // for streaming out 
#include <DNBSystemDefs.h>
#include <DNBCMath.h>      // needed for NaN?

#include <DNBMathVector.h>
#include <DNBPoolAllocator.h>



/*****************************************************************************************
//  The DNBBasicMatrix templated class
/****************************************************************************************/

template <class T> class DNBBasicMathMatrix
{
public:

/*****************************************************************************************
// Constructors
/****************************************************************************************/

/**
  * Constructs an empty matrix ( 0 by 0). It can later be resized or reshaped
  */
DNBBasicMathMatrix ()  DNB_THROW_SPEC((scl_bad_alloc));



/**
  * Constructs an m by n matrix with uninitialized elements
  */ 
DNBBasicMathMatrix (size_t m, size_t n, UninitializedType dummy) 
DNB_THROW_SPEC((scl_bad_alloc));


/**
  * Constructs an m by n  matrix with all elements initialized to initVal.  If no
  * initVal is given, it is taken to be the 0 member of the field defined by the type T.
  * For example, the initVal is 0 if T is double or float.
  */
DNBBasicMathMatrix (size_t m, size_t n, const T& initVal = T(0)) 
DNB_THROW_SPEC((scl_bad_alloc));


/**
  * Constructs a matrix with n elements, using the data pointed to by dat as initial data. 
  * A copy of dat is made. The data pointed must have at least m times n elements.
  */
DNBBasicMathMatrix (const T* dat,size_t m, size_t n) 
DNB_THROW_SPEC((scl_bad_alloc));


/**
  * Constructs an m by n matrix from a DNBMathVector of length m * n
  */
DNBBasicMathMatrix (const DNBBasicMathVector<T>& v, size_t m, size_t n) 
DNB_THROW_SPEC((scl_bad_alloc));

/**
  * Constructs a matrix from the null terminated character string s. The format of the 
  * character string is the same as that expected by the global operator operator>> 
  */
// DNBBasicMathMatrix(const char *s) DNB_THROW_SPEC((scl_bad_alloc)); // FIXME


/**
  * Copy constructor; It actually makes copy of data
  */
DNBBasicMathMatrix (const DNBBasicMathMatrix<T>& mat) 
DNB_THROW_SPEC((scl_bad_alloc));


/*****************************************************************************************
// Destructor
/****************************************************************************************/
~DNBBasicMathMatrix();

/*********************************************************************************
// Member functions: Access
*********************************************************************************/


/**
  * Returns the row i, where i ranges from 0 to and including numRows-1.
  */
DNBBasicMathVector<T>&
operator[] (size_t i) DNB_THROW_SPEC_NULL 
{   
    return rows_[i];
};


/**
  * Constant version of above.  
  */
const DNBBasicMathVector<T>&
operator[] (size_t i) const DNB_THROW_SPEC_NULL 
{   
    return rows_[i];
};


/**
 * Returns the matrix element at row i and column j, where i ranges from 0 to and
 * including numRows - 1  and j ranges from 0 to and including numCols - 1.
 */
T&
operator() (size_t i, size_t j) DNB_THROW_SPEC_NULL;


/**
 * Constant version of above.
 */
const T&
operator()(size_t i, size_t j) const DNB_THROW_SPEC_NULL;


/**
 * Returns the matrix element at row i and column j, where i ranges from 1 to and
 * including numRows and j ranges from 1 to and including numCols.
 */
T&
fromOne(size_t i, size_t j) DNB_THROW_SPEC_NULL;

/**
  * Constant version of above.
  */
const T&
fromOne(size_t i, size_t j) const DNB_THROW_SPEC_NULL;


/*****************************************************************************************
// Member Functions - Misclleaneous
*****************************************************************************************/

/**
  * Returns the number of rows in the matrix
  */
size_t 
numRows() const DNB_THROW_SPEC_NULL {return numRows_;};


/**
  * @nodoc - provided only for compatibility with RW
  * Deprecated.  Use numRows instead.
  * Returns the number of rows in the matrix
  */
size_t 
rows() const DNB_THROW_SPEC_NULL {return numRows_;};


/**
  * Returns the number of rows that this matrix can have without having to reallocate
  * memory
  */
size_t
getRowCapacity() const DNB_THROW_SPEC_NULL {return rowCapacity_;};

/**
  * Returns a vector that contains row i of the matrix.  Data is not copied.
  */
DNBBasicMathVector<T>&
row (size_t i) DNB_THROW_SPEC_NULL 
{ 
    return rows_[i];
};

/**
  * Constant version of above.
  */
const DNBBasicMathVector<T>&
row (size_t i) const DNB_THROW_SPEC_NULL 
{
    return rows_[i];
};

/**
  * Returns the number of columns in the matrix
  */
size_t 
numColumns () const DNB_THROW_SPEC_NULL {return numColumns_;};


/**
  * @nodoc ; provided for compatibility with RW
  * Deprecated.  Returns the number of columns in the matrix.  
  */
size_t
cols () const DNB_THROW_SPEC_NULL {return numColumns_;};


/**
  * Returns a vector that contains column j of the matrix. Data is copied!!
  */
DNBBasicMathVector<T> 
col (size_t j) DNB_THROW_SPEC_NULL;


/**
  * Constant version of above. Not needed?
  */
const DNBBasicMathVector<T> 
col (size_t j) const DNB_THROW_SPEC_NULL;



/**
  * Returns the diagonal elements of the matrix.  Assumes the matrix is square.
  * TODO if needed
  */
/*DNBBasicMathVector<T>
diagonal() DNB_THROW_SPEC_NULL;*/


/**
  * Returns a submatrix with elements extracted from the matrix if the argument vectors
  * vec1 and vec2 specify a 1 at the elements row index and column index, respectively.
  * TO DO if necessary
  */
/*DNBMathMatrix
pick(const DNBBasicMathVector<int> vec1, const DNBBasicMathVector<int> vec2)
DNB_THROW_SPEC_NULL;*/


/**
  * Changes the size of the matrix to m rows and n columns.  The resulting matrix, 
  * if larger, has additional elements which are not initialized, ie they contain 
  * garbage
  */
void
reshape(size_t m, size_t n, bool init = false) DNB_THROW_SPEC((scl_bad_alloc));

/**
  * Changes the size of the matrix to m rows and n columns.  The resulting matrix, if larger,
  * has additional elements initialized to 0
  */
void
resize(size_t m, size_t n) DNB_THROW_SPEC((scl_bad_alloc));



/*********************************************************************************
// Member functions/operators - Mathematical
/*********************************************************************************/

/**
 * Increment each element of self. This is invoked when the operator is used as a 
 * prefix operator.
 */
DNBBasicMathMatrix<T>&      
operator++() DNB_THROW_SPEC_NULL;

/**
 * Increment each element of self. This is invoked when the operator is used as a 
 * postfix operator.
 */
void                
operator++(int) DNB_THROW_SPEC_NULL;


/**
 * Decrement each element of self. This is invoked when the operator is used as a 
 * prefix operator.
 */
DNBBasicMathMatrix<T>&    
operator--() DNB_THROW_SPEC_NULL;


/**
 * Decrement each element of self.  This is invoked when the operator is used as a 
 * postfix operator.
 */
void                
operator--(int) DNB_THROW_SPEC_NULL;


/**
  * Assignment operator.  Resizes self if necessary to accomocate the data to be 
  * assigned
  */
void
operator=(const DNBBasicMathMatrix<T>& m) DNB_THROW_SPEC_NULL;


/**
  * Assigns a value of type T to each element of the matrix
  */
void
operator=(const T&) DNB_THROW_SPEC_NULL;


/**
  * Adds each element of the matrix mat to the corresponding element of this matrix.
  * mat must have the same number of rows and columns as this matrix.
  */
void
operator+=(const DNBBasicMathMatrix<T>& mat) DNB_THROW_SPEC_NULL;


/**
  * Subtracts each element of the matrix mat from the corresponding element of this matrix
  * mat must have the same number of rows and columns as this matrix.
  */
void
operator-=(const DNBBasicMathMatrix<T>& mat) DNB_THROW_SPEC_NULL;


/**
  * Performs matrix multiplication as defined in Linear Algebra.
  * self = self * mat
  * the number of columns of self must equal the number of rows of mat.
  * Self is expanded or shrunk to contain the result, if needed.
  */
void
operator*=(const DNBBasicMathMatrix<T>& mat) DNB_THROW_SPEC_NULL;


/**
  * Divide each element of the matrix mat by the corresponding element of this matrix
  * mat must have the same number of rows and columns as this matrix.
  */
// Comment it out due ambuiguity or interpretation of matrix division
/*void
operator/=(const DNBBasicMathMatrix<T>& mat) DNB_THROW_SPEC_NULL;*/


/**
  * Adds the value of type T to each element of this matrix
  */
void
operator+=(const T&) DNB_THROW_SPEC_NULL;


/**
  * Subtracts the value of type T from each element of this matrix
  */
void
operator-=(const T&) DNB_THROW_SPEC_NULL;

/**
  * Multiplies each element of this matrix by the value of type T
  */
void
operator*=(const T&) DNB_THROW_SPEC_NULL;


/**
  * Divides each element of this matrix by the value of type T
  */
void
operator/=(const T&) DNB_THROW_SPEC_NULL;



/*****************************************************************************************
// Constructors that do math
*****************************************************************************************/


/**
  * @nodoc
  * Additive inverse
  */
DNBBasicMathMatrix( const DNBBasicMathMatrix<T> &matrix,
                    const _DNBSubOp&)
                    DNB_THROW_SPEC((scl_bad_alloc));

/**
 * @nodoc
 * Matrix Addition
 */
DNBBasicMathMatrix( const DNBBasicMathMatrix<T> &left,
                    const DNBBasicMathMatrix<T> &right, 
                    const _DNBAddOp & )
                    DNB_THROW_SPEC((scl_bad_alloc));


/**
 * @nodoc
 * Matrix subtraction
 */
DNBBasicMathMatrix( const DNBBasicMathMatrix<T> &left,
                    const DNBBasicMathMatrix<T> &right, 
                    const _DNBSubOp & )
                    DNB_THROW_SPEC((scl_bad_alloc));


/**
 * @nodoc
 * Matrix multiplication (as defined by Linear Algebra)
 */
DNBBasicMathMatrix( const DNBBasicMathMatrix<T> &left,
                    const DNBBasicMathMatrix<T> &right, 
                    const _DNBMulOp & )
                    DNB_THROW_SPEC((scl_bad_alloc));
/**
 * @nodoc
 * Matrix + scalar
 */
DNBBasicMathMatrix( const DNBBasicMathMatrix<T> &left, 
                    const T& aVal, 
                    const _DNBAddOp & )
                    DNB_THROW_SPEC((scl_bad_alloc));

/**
 * @nodoc
 * scalar + matrix
 */
DNBBasicMathMatrix( const T& aVal, 
                    const DNBBasicMathMatrix<T> &right,
                    const _DNBAddOp & )
                    DNB_THROW_SPEC((scl_bad_alloc));


/**
 * @nodoc
 * matrix - scalar
 */
DNBBasicMathMatrix( const DNBBasicMathMatrix<T> &left,
                    const T& aVal,
                    const _DNBSubOp & )
                    DNB_THROW_SPEC((scl_bad_alloc));


/**
 * @nodoc
 * scalar - matrix
 */
DNBBasicMathMatrix( const T& aVal, 
                    const DNBBasicMathMatrix<T> &right,
                    const _DNBSubOp & )
                    DNB_THROW_SPEC((scl_bad_alloc));


/**
 * @nodoc
 * matrix * scalar
 */
DNBBasicMathMatrix( const DNBBasicMathMatrix<T> &left,
                    const T& aVal, 
                    const _DNBMulOp & )
                    DNB_THROW_SPEC((scl_bad_alloc));

/**
 * @nodoc
 * scalar * matrix
 */
DNBBasicMathMatrix( const T& aVal,
                    const DNBBasicMathMatrix<T> &right,
                    const _DNBMulOp & )
                    DNB_THROW_SPEC((scl_bad_alloc));

/**
 * @nodoc
 * matrix / scalar
 */
DNBBasicMathMatrix( const DNBBasicMathMatrix<T> &left,
                    const T& aVal, 
                    const _DNBDivOp & )
                    DNB_THROW_SPEC((scl_bad_alloc));


/**
 * @nodoc
 * scalar / matrix
 */
DNBBasicMathMatrix( const T& aVal,
                    const DNBBasicMathMatrix<T> &right,
                    const _DNBDivOp & )
                    DNB_THROW_SPEC((scl_bad_alloc));


private:

    DNBBasicMathVector<T> * rows_;
    DNBBasicMathVector<T> * rows1_;
    size_t numRows_;
    size_t numColumns_;
    size_t rowCapacity_;
    void   localAllocate(size_t nRows, size_t nColumns = 0);
    DNBPoolAllocator & alloc_;

};


/**********************************************************************************
* Global Mathematical Operators
***********************************************************************************/

/**
  * Returns true if left is equal to right, i.e.
  * they both have the same number of rows and columns and each element
  * in one is equal to the corresponding element in the other
  */
template <class T>
bool
                          operator==( const DNBBasicMathMatrix<T>& left,
                                      const DNBBasicMathMatrix<T>& right ) 
                          DNB_THROW_SPEC_NULL;


/**
  * Returns true if every element of left is less than its counterpart in right
  */
template <class T>
bool
                        operator<( const DNBBasicMathMatrix<T>& left,
                                   const DNBBasicMathMatrix<T>& right ) 
                        DNB_THROW_SPEC_NULL;


/**
  * Returns the additive inverse of the argument matrix 
  */
template <class T>
DNBBasicMathMatrix<T>    
                          operator-(const DNBBasicMathMatrix<T>&)
                          DNB_THROW_SPEC((scl_bad_alloc));



/** 
 * returns a new matrix where each element is the sum of the two corresponding
 * elements of the two input matrices.
 * The two input matrices must have the same number of rows and columns.
 */
template <class T>
DNBBasicMathMatrix<T>    
                          operator+(const DNBBasicMathMatrix<T>&,
                                    const DNBBasicMathMatrix<T>&)
                          DNB_THROW_SPEC((scl_bad_alloc));

/** 
 * returns a new matrix where each element is the difference of the two corresponding
 * elements of the two input matrices ( first input matrix - second input matrix)
 * The two input matrices must have the same number of rows and columns.
 */
template <class T>
DNBBasicMathMatrix<T>
                        operator-(const DNBBasicMathMatrix<T>&,
                                  const DNBBasicMathMatrix<T>&)
                        DNB_THROW_SPEC((scl_bad_alloc));

/** 
 * returns a new matrix which is the product of the two matrices, using the Linear
 * Algebra definition of matrix multiplication
 * result = left * right
 * the number of columns of left must equal the number of rows of right.
 */
template <class T>
DNBBasicMathMatrix<T>    operator*(const DNBBasicMathMatrix<T>& left,
                                   const DNBBasicMathMatrix<T>& right)
                         DNB_THROW_SPEC((scl_bad_alloc));


/** 
 * Same as above with left argument being a DNBBasicMathVector<T>
 * the length of left must equal the number of rows of right.
 */
template <class T>
DNBBasicMathVector<T>    operator*(const DNBBasicMathVector<T>& left,
                                   const DNBBasicMathMatrix<T>& right)
                         DNB_THROW_SPEC((scl_bad_alloc));

/** 
 * Same as above with right argument being a DNBBasicMathVector<T>
 * the number of columns of left must equal the length of right.
 */
template <class T>
DNBBasicMathVector<T>    operator*(const DNBBasicMathMatrix<T>& left,
                                   const DNBBasicMathVector<T>& right)
                         DNB_THROW_SPEC((scl_bad_alloc));


/**
  * Returns a new matrix where each element is the product of the two corresponding elements
  * of the two input matrices.
  * The two input matrices must have the same number of rows and columns.
  */
template <class T>
DNBBasicMathMatrix<T>    elementProduct(const DNBBasicMathMatrix<T>& left, 
                                        const DNBBasicMathMatrix<T>& right) 
                                        DNB_THROW_SPEC((scl_bad_alloc));


/**
 * @nodoc 
 * returns a new matrix where each element is the ratio of the two corresponding
 * elements of the two input matrices (firs input matrix element/ second input matrix element)
  * The two input matrices must have the same number of rows and columns.
  // comment it out due to ambuiguious meaning or interpretation of matrix division
*/
/*template <class T>
DNBBasicMathMatrix<T>    operator/(const DNBBasicMathMatrix<T>&,
                                   const DNBBasicMathMatrix<T>&)
                         DNB_THROW_SPEC((scl_bad_alloc));*/

/** 
 * returns a new matrix where each element is the sum of corresponding
 * element of the input matrix and the input constant
 */
template <class T>
DNBBasicMathMatrix<T>    operator+(const DNBBasicMathMatrix<T>&,
                                   const T&)
                         DNB_THROW_SPEC((scl_bad_alloc));

/** 
 * returns a new matrix where each element is the sum of corresponding
 * element of the input matrix and the input constant
 */
template <class T>
DNBBasicMathMatrix<T>    operator+(const T&,
                                   const DNBBasicMathMatrix<T>&)
                         DNB_THROW_SPEC((scl_bad_alloc));

/** 
 * returns a new matrix where each element is the differnece of corresponding
 * element of the input matrix and the input constant
 */
template <class T>
DNBBasicMathMatrix<T>    operator-(const DNBBasicMathMatrix<T>&,
                                   const T&)
                         DNB_THROW_SPEC((scl_bad_alloc));

/** 
 * returns a new matrix where each element is the difference of corresponding
 * element of the input matrix and the input constant
 */
template <class T>
DNBBasicMathMatrix<T>    operator-(const T&,
                                   const DNBBasicMathMatrix<T>&)
                         DNB_THROW_SPEC((scl_bad_alloc));

/** 
 * returns a new matrix where each element is the product of corresponding
 * element of the input matrix and the input constant
 */
template <class T>
DNBBasicMathMatrix<T>    operator*(const DNBBasicMathMatrix<T>&,
                                   const T&)
                         DNB_THROW_SPEC((scl_bad_alloc));

/** 
 * returns a new matrix where each element is the product of corresponding
 * element of the input matrix and the input constant
 */
template <class T>
DNBBasicMathMatrix<T>    operator*(const T&,
                                   const DNBBasicMathMatrix<T>&)
                         DNB_THROW_SPEC((scl_bad_alloc));

/** 
 * returns a new matrix where each element is the ratio of corresponding
 * element of the input matrix and the input constant
 */
template <class T>
DNBBasicMathMatrix<T>    operator/(const DNBBasicMathMatrix<T>&,
                                   const T&)
                         DNB_THROW_SPEC((scl_bad_alloc));

/** 
 * returns a new matrix where each element is the ratio of corresponding
 * element of the input matrix and the input constant
 */
template <class T>
DNBBasicMathMatrix<T>    operator/(const T&,
                                   const DNBBasicMathMatrix<T>&)
                         DNB_THROW_SPEC((scl_bad_alloc));


/**
 * Returns the min absolute value of all elements
 */

template <class T>
T
DNBMinAbsValue( const DNBBasicMathMatrix<T> &mat )
DNB_THROW_SPEC_NULL;


/**
 * Returns the max absolute value of all elements
 */
template <class T>
T
DNBLinfNorm( const DNBBasicMathMatrix<T> &right )
DNB_THROW_SPEC_NULL;


/**
 * Returns the max L1Norm of all columns of the matrix
 */
template <class T>
T
DNBL1Norm( const DNBBasicMathMatrix<T> &mat )
DNB_THROW_SPEC_NULL;


/**
 * Returns the transpose of the given matrix.  Data is copied.
 */
template <class T>
DNBBasicMathMatrix<T>
transpose( const DNBBasicMathMatrix<T> &mat )
DNB_THROW_SPEC_NULL;



// From RW: Other Related Global Functions (templetized)

// use APPLY to do most of this
//acos         asin         atan
//atan2        bg10         ceil         
//cos          cosh         dot
//exp          floor        log
//maxIndex     maxValue     mean         
//minIndex     minValue     pow          
//prod         product(DO)  sin          
//sinh         sqrt         sum          
//tan          tanh         transpose (DONE)

// From RW: Other Global Functions (non-templetized)

//abs (DO)      adjoint     arg                    
//conj          frobNorm(DO) imag
//linfNorm(done) l1Norm(DONE)    maxnorm
//norm(DO)      real    toChar
//toFloat      toInt
//variance     


/***********************************************************************************
* Global Streaming out
**********************************************************************************/

/**
 * Outputs a matrix m to output stream s row by row, starting with a [ and ending in a ]
 * Each starts on a new line. The numbers are separated by spaces. 
 * Example: below is a 2 by 3 matrix.
 * [1 2 3
 *  4 5 6]
 */
template <class T>
ostream&    
operator<<(ostream& s, const DNBBasicMathMatrix<T>& mat) DNB_THROW_SPEC_NULL;


/*********************************************************************************
// Global Methods used by the batch ODT for testing
/********************************************************************************/

template <class T>
bool
DNBNearAbs(const DNBBasicMathMatrix<T> &left, 
           const DNBBasicMathMatrix<T> &right,
           const T &tolerance)
DNB_THROW_SPEC_NULL;


template <class T>
bool
DNBNearRel( const DNBBasicMathMatrix<T> &left, 
            const DNBBasicMathMatrix<T> &right,
            const T &tolerance)
DNB_THROW_SPEC_NULL;



/*********************************************************************************
// The typedef, etc..
*********************************************************************************/

typedef DNBBasicMathMatrix<DNBReal> DNBMathMatrix; 

//
//  Include the public definition file.
//
#if     defined(DNB_COMPILE_INSTANTIATE) || defined(_DNBMATHMATRIX_CC_) 
#include "DNBMathMatrix.cc"
#endif

#endif /*_DNBMATHMATRIX_H_ */

