//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/** @CAA2Required */
/*---------------------------------------------------------------------*/
/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */
/*---------------------------------------------------------------------*/
//
//  This Header file is included by all the Module.h header files in
//  PublicInterfaces
//
#ifndef _DNBMATH_H_
#define _DNBMATH_H_


#ifdef  _WINDOWS_SOURCE
#ifdef  __DNBMath
#define ExportedByDNBMath __declspec(dllexport)
#else
#define ExportedByDNBMath __declspec(dllimport)
#endif
#else
#define ExportedByDNBMath
#endif


#endif  /* _DNBMATH_H_ */
