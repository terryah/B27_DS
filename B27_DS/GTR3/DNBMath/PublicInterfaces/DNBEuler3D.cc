//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview czo/BPL 01:11:13
 */

//*
//* FILE:
//*     DNBEuler3D.cc     - principle implementation file
//*
//* MODULE:
//*     DNBBasicEuler3D   - single template class
//*
//* OVERVIEW:
//*     This module defines Euler angles rotation, which is three consective
//*     rotations about three principle axes.
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     CZO         09/09/99    Initial-implementation.
//		smw			01/07/2005		Removed extractors due to iostream migration to the
//									new, standard, templated iostream, and due to migration 
//									to 64 bit builds.  Extractor for all math classes have 
//									been moved, for easy rescucitation, if necessary,
//									to DNBMath.tst/PrivateInterfaces/DNBMathExtractors.h
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1997-2002 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*
#include <DNBSystemBase.h>
#include <scl_vector.h>
#include <DNBSystemDefs.h>

#include <DNBMathVector.h>
#include <DNBVector3D.h>


template <class T>
DNBBasicEuler3D<T>::DNBBasicEuler3D( )
    DNB_THROW_SPEC_NULL
{
    angles_[0] = T(0);
    angles_[1] = T(0);
    angles_[2] = T(0);

    axes_[0] = ZAxis;
    axes_[1] = YAxis;
    axes_[2] = ZAxis;
}


template <class T>
DNBBasicEuler3D<T>::DNBBasicEuler3D( const DNBBasicEuler3D<T> &right )
    DNB_THROW_SPEC_NULL
{
    angles_[0] = right.angles_[0];
    angles_[1] = right.angles_[1];
    angles_[2] = right.angles_[2];

    axes_[0] = right.axes_[0];
    axes_[1] = right.axes_[1];
    axes_[2] = right.axes_[2];
}


template <class T>
DNBBasicEuler3D<T>::DNBBasicEuler3D( const T &angle1, AxisType axis1,
				     const T &angle2, AxisType axis2,
				     const T &angle3, AxisType axis3 )
    DNB_THROW_SPEC_NULL
{
    DNB_ASSERT( (axis1 >= 0) && (axis1 <= 2) &&
                (axis2 >= 0) && (axis2 <= 2) &&
                (axis3 >= 0) && (axis3 <= 2) );

    angles_[0] = angle1;
    angles_[1] = angle2;
    angles_[2] = angle3;

    axes_[0] = axis1;
    axes_[1] = axis2;
    axes_[2] = axis3;
}


template <class T>
DNBBasicEuler3D<T>::~DNBBasicEuler3D( )
    DNB_THROW_SPEC_NULL
{
    //nothing
}


template <class T>
void
DNBBasicEuler3D<T>::getAngles( T &angle1, T &angle2, T &angle3 ) const
    DNB_THROW_SPEC_NULL
{
    angle1 = angles_[0];
    angle2 = angles_[1];
    angle3 = angles_[2];
}


template <class T>
void
DNBBasicEuler3D<T>::getAxes( AxisType &axis1, AxisType &axis2,
			     AxisType &axis3 ) const
    DNB_THROW_SPEC_NULL
{
    axis1 = (AxisType) axes_[0];
    axis2 = (AxisType) axes_[1];
    axis3 = (AxisType) axes_[2];
}


template <class T>
void
DNBBasicEuler3D<T>::get( int step, T &angle, AxisType &axis ) const
    DNB_THROW_SPEC_NULL
{
    DNB_ASSERT( (step >= 0) && (step <= 2) );

    angle = angles_[step];
    axis = (AxisType) axes_[step];
}


template <class T>
void
DNBBasicEuler3D<T>::setAngles( const T &angle1, const T &angle2, const T &angle3 )
    DNB_THROW_SPEC_NULL
{
    angles_[0] = angle1;
    angles_[1] = angle2;
    angles_[2] = angle3;
}


template <class T>
void
DNBBasicEuler3D<T>::setAxes( AxisType axis1, AxisType axis2, AxisType axis3 )
    DNB_THROW_SPEC_NULL
{
    DNB_ASSERT( (axis1 >= 0) && (axis1 <= 2) &&
		(axis2 >= 0) && (axis2 <= 2) &&
		(axis3 >= 0) && (axis3 <= 3) );

    axes_[0] = axis1;
    axes_[1] = axis2;
    axes_[2] = axis3;
}


template <class T>
void
DNBBasicEuler3D<T>::set( int step, const T &angle, AxisType axis )
    DNB_THROW_SPEC_NULL
{
    DNB_ASSERT( (step >= 0) && (step <= 2) &&
		(axis >= 0) && (axis <= 2) );

    angles_[step] = angle;
    axes_[step] = axis;
}


template <class T>
void
DNBBasicEuler3D<T>::invert( )
    DNB_THROW_SPEC_NULL
{
    T tmpAngle = -angles_[0];
    angles_[0] = -angles_[2];
    angles_[1] = -angles_[1];
    angles_[2] = tmpAngle;

    unsigned int tmpAxis = axes_[0];
    axes_[0] = axes_[2];
    axes_[1] = -axes_[1];
    axes_[2] = tmpAxis;
}


template <class T>
DNBBasicEuler3D<T> &
DNBBasicEuler3D<T>::operator=( const DNBBasicEuler3D<T> &right )
    DNB_THROW_SPEC_NULL
{
    if( this != &right )
    {
        angles_[0] = right.angles_[0];
        angles_[1] = right.angles_[1];
        angles_[2] = right.angles_[2];

	axes_[0] = right.axes_[0];
	axes_[1] = right.axes_[1];
	axes_[2] = right.axes_[2];
    }

    return *this;
}


template <class T>
bool
DNBBasicEuler3D<T>::operator==( const DNBBasicEuler3D<T> &right )
    DNB_THROW_SPEC_NULL
{
    return ( angles_[0] == right.angles_[0] ) &&
           ( angles_[1] == right.angles_[1] ) &&
           ( angles_[2] == right.angles_[2] ) &&
	   ( axes_[0] == right.axes_[0] ) &&
	   ( axes_[1] == right.axes_[1] ) &&
	   ( axes_[2] == right.axes_[2] );
}


template <class T>
bool
DNBBasicEuler3D<T>::operator!=( const DNBBasicEuler3D<T> &right )
    DNB_THROW_SPEC_NULL
{
    return !( *this == right );
}


template <class T>
ostream &
operator<<( ostream &ostr, const DNBBasicEuler3D<T> &euler )
    DNB_THROW_SPEC((scl_bad_alloc))
{
    DNBBasicMathVector<T> angles( euler.angles_, 3 );
    ostr<<angles;

    //RWMathVec<int> axes( euler.axes_, 3 );
    //ostr<<axes;
    ostr << "[ " << euler.axes_[0] << "  " << euler.axes_[1] << "  " << euler.axes_[2] << " ]";
    return ostr;
}



template <class T>
const DNBBasicEuler3D<T>    DNBBasicEuler3D<T>::Identity;
