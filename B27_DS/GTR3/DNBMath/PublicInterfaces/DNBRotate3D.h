//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 */

//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     CZO         09/09/99    Initial implementation.
//*     bkh         11/05/03    Implementation of CAA style documentation.
//*		smw			01/07/05	Removed extractors due to iostream migration to the
//*								new, standard, templated iostream, and due to migration to 64 bit
//*								builds.  Extractor for all math classes have been moved,
//*								for easy rescucitation, if necessary,
//*								to DNBMath.tst/PrivateInterfaces/DNBMathExtractors.h
//*
//*
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNBROTATE3D_H_
#define _DNBROTATE3D_H_


#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>


#include <DNBMath3D.h>
#include <DNBException.h>
#include <DNBVector3D.h>
#include <DNBQtn3D.h>
#include <DNBMathMatrix.h>



//
//  Forward declarations.
//
template <class T>
class DNBBasicEuler3D;

template <class T>
class DNBBasicXform3D;

template <class T>
class DNBBasicRotate3D;

template <class T>
bool
operator==( const DNBBasicRotate3D<T> &left, const DNBBasicRotate3D<T> &right)
    DNB_THROW_SPEC_NULL;

template <class T>
bool
operator!=( const DNBBasicRotate3D<T> &left, const DNBBasicRotate3D<T> &right)
    DNB_THROW_SPEC_NULL;

template <class T>
DNBBasicRotate3D<T>
operator*( const DNBBasicRotate3D<T> &left, const DNBBasicRotate3D<T> &right )
    DNB_THROW_SPEC_NULL;

template <class T>
DNBBasicRotate3D<T>
operator/( const DNBBasicRotate3D<T> &left, const DNBBasicRotate3D<T> &right )
    DNB_THROW_SPEC((DNBESingularMatrix));

template <class T>
DNBBasicVector3D<T>
operator*( const DNBBasicRotate3D<T> &left, const DNBBasicVector3D<T> &right )
    DNB_THROW_SPEC_NULL;

template <class T>
ostream &
operator<<( ostream &ostr, const DNBBasicRotate3D<T> &item )
    DNB_THROW_SPEC((scl_bad_alloc));

template <class T>
bool
DNBNearRel( const DNBBasicRotate3D<T> &left, const DNBBasicRotate3D<T> &right,
    const T &angularTol, const T &linearTol )
    DNB_THROW_SPEC_NULL;

template <class T>
bool
DNBNearAbs( const DNBBasicRotate3D<T> &left, const DNBBasicRotate3D<T> &right,
    const T &angularTol, const T &linearTol )
    DNB_THROW_SPEC_NULL;




/**
  * A templated class which represents a three-dimensional rotation matrix.
  *
  * <br><B>Template Parameter(s)</B><br>
  * @param T
  * The base type of the matrix elements.
  *
  * <br><B>Description</B><br>
  * This module defines a class which contains 3X3 values. This
  * collection of values is intended to be used as a three dimensional
  * ortho-normal matrix (no scaling or shear). Every instance of this
  * class must and will be ortho-normal.
  *
  * The main motivation for this class was to provide a very high
  * performance matrix manipulation class.
  *
  * An instance of this  class is equivalent to that of a quaternion class
  * (DNBBasicQtn3D).
  *
  * A Note on the Matrix Internals:
  * The elements of the matrix are organized into  rows and 3 columns.
  * The ros are identified as N, S and A. The columns are identified
  * as X, Y and Z. A figure of the matrix is depicted below:
  *
  * Nx  Ny  Nz
  * Sx  Sy  Sz
  * Ax  Ay  Az
  *
  *
  */
template <class T>
class DNBBasicRotate3D : public DNBMath3D
{
public:
/**
  * Creates the identity cosine matrix.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function creates a 3X3 cosine matrix which represents
  * the identity rotation.
  *
  *
  */
    DNBBasicRotate3D( )
DNB_THROW_SPEC_NULL;

/**
  * Creates a cosine matrix using copy semantics.
  * @param right
  * The rotation matrix to be copied.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function copies the elements of <tt>right</tt> to <tt>self</tt>.
  *
  *
  */
    DNBBasicRotate3D( const DNBBasicRotate3D<T> &right )
        DNB_THROW_SPEC_NULL;

/**
  * Creates a rotation matrix without initializing its elements.
  * @param dummy
  * The UninitializedType object indicating <tt>self</tt>'s elements
  * will not be initialized.
  *
  * @return
  * Nothing.
  *
  *  <br><B>Description</B><br>
  * This function creates a 3X3 matrix without setting its elements
  * to a valid state, thus returns quickly. The existing of this
  * function is purely for efficiency, see the example below.
  *
  * EXAMPLES:
  * DNBRotate3D    rotate( DNBMath3D::Uninitialized );
  * xform.getRotate( rotate );
  *
  *
  */
    DNBBasicRotate3D( UninitializedType dummy )
        DNB_THROW_SPEC_NULL;

/**
  * Creates a rotation matrix from a 3X3 two dimensional array.
  * @param right
  * The 3X3 two-dimensional array.
  * @param transposed
  * The bool value true indicating that the input array already
  * contains rowwise elements. False, the default value, indicates
  * that N, S and A are stored in <tt>right</tt> as three columns.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function creates a rotation matrix from a 3X3 two-dimensional
  * array.
  * @exception DNBESingularMatrix
  * Matrix <tt>right</tt> is singular.
  * @param DNBEIllegalMatrix
  * Matrix <tt>right</tt> is not a valid right-hand rule cosine matrix.
  *
  *
  */
    explicit
    DNBBasicRotate3D( const T** right, bool transposed = false )
        DNB_THROW_SPEC((DNBESingularMatrix, DNBEIllegalMatrix));

/**
  * Creates a rotation matrix from a rogue wave matrix.
  * @param right
  * The rogue wave generic matrix.
  * @param transposed
  * The bool value true indicating that the input array already
  * contains rowwise elements. False, the default value, indicates
  * that N, S and A are stored in <tt>right</tt> as three columns.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function creates a rotation matrix from a rogue wave 3X3
  * generic matrix.
  * @exception DNBESingularMatrix
  * Matrix <tt>right</tt> is singular.
  * @param DNBEIllegalMatrix
  * Matrix <tt>right</tt> is not a valid right-hand rule cosine matrix.
  *
  *
  */
    explicit
    DNBBasicRotate3D( const DNBBasicMathMatrix<T> &right, bool transposed = false )
        DNB_THROW_SPEC((DNBESingularMatrix, DNBEIllegalMatrix));


/**
  * Destroys <tt>self</tt>.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function destroys <tt>self</tt>
  *
  *
  */
    ~DNBBasicRotate3D( )
        DNB_THROW_SPEC_NULL;

/**
  * Retrieves the three columns N, S and A of the rotation matrix.
  * @param vecN
  * The 3-D vector object to receive vector N.
  * @param vecN
  * The 3-D vector object to receive vector S.
  * @param vecN
  * The 3-D vector object to receive vector A.
  *
  * @return
  * Nothing.
  *
  *  <br><B>Description</B><br>
  * This function retrieves the three  columns N, S and A of the rotation
  * matrix.
  *
  * @exception none
  *
  *
  */
    void
    getNSA( DNBBasicVector3D<T> &vecN,
            DNBBasicVector3D<T> &vecS,
            DNBBasicVector3D<T> &vecA ) const
        DNB_THROW_SPEC_NULL;

/**
  * Retrieves <tt>self</tt>'s equivalent axis and angle.
  * @param vec
  * The 3-D vector object to receive the axis.
  * @param angle
  * The base value to receive the angle.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function retrieves <tt>self</tt>'s equivalent axis and angle.
  *
  *
  */
    void
    getEAA( DNBBasicVector3D<T> &vec, T &angle ) const
        DNB_THROW_SPEC_NULL;

/**
  * Retrieves <tt>self</tt>'s roll, pitch and yaw.
  * @param roll
  * The base type object to receive the roll value.
  * @param pitch
  * The base type object to receive the pitch value.
  * @param yaw
  * The base type object to receive the yaw value.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function retrieves <tt>self</tt>'s roll, pitch and yaw.
  *
  *
  */
    void
    getRPY( T &roll, T &pitch, T &yaw ) const
        DNB_THROW_SPEC_NULL;

/**
  * Gets a 3X3 matrix from <tt>self</tt>.
  * @param right
  * The 3X3 matrix to receive the values.
  * @param transposed
  * The bool value true indicating that the output array will
  * contain rowwise elements. False, the default value, indicates
  * that N, S and A will be stored in <tt>right</tt> as three columns.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function gets a 3X3 matrix from <tt>self</tt>.
  *
  *
  */
    void
    get( T** right, bool transposed = false ) const
        DNB_THROW_SPEC_NULL;

/**
  * Gets a 3X3 matrix from <tt>self</tt>.
  * @param right
  * The 3X3 rogue wave generic matrix to receive the values.
  * @param transposed
  * The bool value true indicating that the output array will
  * contain rowwise elements. False, the default value, indicates
  * that N, S and A will be stored in <tt>right</tt> as three columns.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function gets a 3X3 rogue wave generic matrix from <tt>self</tt>.
  *
  *
  */
    void
    get( DNBBasicMathMatrix<T> &right, bool transposed = false ) const
        DNB_THROW_SPEC_NULL;

/**
  * Constructs a rotation matrix from the three given vectors, and
  * stores it in <tt>self</tt>.
  * @param vecN
  * The 3-D vector object contains the values of N.
  * @param vecS
  * The 3-D vector object contains the values of S.
  * @param vecA
  * The 3-D vector object contains the values of A.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function constructs a rotation matrix from the three
  * given vectors, and stores it in <tt>self</tt>.
  * @exception DNBESingularMatrix
  * The matrix formed by the three given vector is singular.
  * @param DNBEIllegalMatrix
  * The given three vectors do not form a valid rotation matrix.
  *
  *
  */
    void
    setNSA( const DNBBasicVector3D<T> &vecN,
            const DNBBasicVector3D<T> &vecS,
            const DNBBasicVector3D<T> &vecA )
        DNB_THROW_SPEC((DNBESingularMatrix, DNBEIllegalMatrix));

/**
  * Constructs a rotation matrix from the given axis and angle, and
  * stores it in <tt>self</tt>.
  * @param vec
  * The 3-D vector object contains the axis to rotate about.
  * @param angle
  * The base type object contains the angle to rotate.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function constructs a rotation matrix from the given
  * axis and angle, and stores it in <tt>self</tt>.
  * @exception DNBEZeroVector
  * The given axis is a zero length vector.
  *
  *
  */
    void
    setEAA( const DNBBasicVector3D<T> &vec, const T &angle )
        DNB_THROW_SPEC((DNBEZeroVector));

/**
  * Constructs a rotation matrix from the given major axis and anagle,
  * and stores it in <tt>self</tt>.
  * @param axisXYZ
  * The AxisType object.
  * @param angle
  * The base type object constains the angle to rotate.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function constructs a rotation matrix from the given
  * major axis and angle, and stores it in <tt>self</tt>.
  *
  *
  */
    void
    setCAA( AxisType axisXYZ, const T &angle )
        DNB_THROW_SPEC_NULL;

/**
  * Constructs a rotation matrix from the given Euler object, and stores
  * it in <tt>self</tt>.
  * @param euler
  * The Euler object which is three consective rotations about
  * three principle axes.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function constructs a rotation matrix from the given
  * Euler object, and stores it in <tt>self</tt>.
  *
  *
  */
    void
    setEuler( const DNBBasicEuler3D<T> &euler )
        DNB_THROW_SPEC_NULL;

/**
  * Constructs a rotation matrix from the given roll, pitch and yaw
  * values and stores it in <tt>self</tt>.
  * @param roll
  * The base type object contains the roll value.
  * @param pitch
  * The base type object contains the pitch value.
  * @param yaw
  * The base type object contains the yaw value.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function constructs a rotation matrix from the
  * given roll, pitch and yaw values and stores it in <tt>self</tt>.
  *
  *
  */
    void
    setRPY( const T &roll, const T &pitch, const T &yaw )
        DNB_THROW_SPEC_NULL;

/**
  * Constructs a rotation matrix from the given 3X3 matrix and
  * stores it in <tt>self</tt>.
  * @param right
  * The 3X3 matrix contains the raw values.
  * @param transposed
  * The bool value true indicating that the input array already
  * contains rowwise elements. False, the default value, indicates
  * that N, S and A are stored in <tt>right</tt> as three columns.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function constructs a rotation matrix from the
  * given 3X3 matrix and stores it in <tt>self</tt>.
  * @exception DNBESingularMatrix
  * Matrix <tt>right</tt> is singular.
  * @exception DNBEIllegalMatrix
  * Matrix <tt>right</tt> is not a valid right-hand rule cosine matrix.
  *
  *
  */
    void
    set( const T** right, bool transposed = false )
        DNB_THROW_SPEC((DNBESingularMatrix, DNBEIllegalMatrix));

/**
  * Constructs a rotation matrix from the given 3X3 rogue wave generic
  * matrix and stores it in <tt>self</tt>.
  * @param right
  * The 3X3 rogue wave generic matrix contains the raw values.
  * @param transposed
  * The bool value true indicating that the input array already
  * contains rowwise elements. False, the default value, indicates
  * that N, S and A are stored in <tt>right</tt> as three columns.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function constructs a rotation matrix from the
  * given 3X3 rogue wave generic matrix and stores it in <tt>self</tt>.
  * @exception DNBESingularMatrix
  * Matrix <tt>right</tt> is singular.
  * @exception DNBEIllegalMatrix
  * Matrix <tt>right</tt> is not a valid right-hand rule cosine matrix.
  *
  *
  */
    void
    set( const DNBBasicMathMatrix<T> &right, bool transposed = false )
        DNB_THROW_SPEC((DNBESingularMatrix, DNBEIllegalMatrix));

/**
  * Returns a quaternion object that is equivalent to <tt>self</tt>.
  *
  * @return
  * The quaternion object that is equivalent to <tt>self</tt>.
  *
  * <br><B>Description</B><br>
  * This function returns a quaternion that is equivalent to <tt>self</tt>.
  *
  *
  */
    DNBBasicQtn3D<T>
    toQtn( ) const
        DNB_THROW_SPEC_NULL;

/**
  * Returns a rogue wave generic matrix constains the values of <tt>self</tt>.
  *
  * @return
  * The rogue wave generic matrix constians the values of <tt>self</tt>.
  *
  * DECRIPTION:
  * This function returns a rogue wave generic matrix constains
  * the values of <tt>self</tt>.
  *
  *
  */
    DNBBasicMathMatrix<T>
    toDNBBasicMathMatrix( ) const
        DNB_THROW_SPEC((scl_bad_alloc));

/**
  * Exchanges the values of <tt>self</tt> with another rotation object.
  * @param other
  * The rotation object.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function exchanges the values of <tt>self</tt> with another
  * rotation object.
  *
  *
  */
    void
    swap( DNBBasicRotate3D<T> &other )
        DNB_THROW_SPEC_NULL;

/**
  * Returns <tt>self</tt>'s determinant.
  *
  * @return
  * The determinant of <tt>self</tt>.
  *
  * <br><B>Description</B><br>
  * This function computes and returns <tt>self</tt>'s determinant.
  *
  *
  */
    T
    det( ) const
        DNB_THROW_SPEC_NULL;


/**
  * Normalizes the rotation matrix of <tt>self</tt>.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function normalizes the rotation matrix of <tt>self</tt>.
  * @exception DNBESingularMatrix
  * <tt>self</tt> could not be normalized due to its singularity.
  * @exception DNBEIllegalMatrix
  * <tt>self</tt> could not be normalized because it not a valid
  * right-hand rule rotation cosine matrix.
  *
  *
  */
    void
    normalize( )
        DNB_THROW_SPEC((DNBESingularMatrix, DNBEIllegalMatrix));

/**
  * Rotate <tt>self</tt> about a principle axis.
  * @param axisXYZ
  * X, Y or Z axis specification.
  * @param angle
  * The angle to rotate.
  * @param refFrame
  * The reference frame for the rotation.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function constructs a rotation matrix from the given axis and
  * angle, and then pre-multiplies it by <tt>self</tt> if refFrame is Local, or
  * post-multipy it by <tt>self</tt> if refFrame is Origin.
  *
  *
  */
    void
    rotate( AxisType axisXYZ, const T &angle, ReferenceFrame refFrame = Local )
        DNB_THROW_SPEC_NULL;

/**
  * Rotates <tt>self</tt> about an arbitrarily given axis.
  * @param vec
  * The 3-D vecto object contains the axis to rotate about.
  * @param angle
  * The base type object constains the angle to rotate.
  * @param refFrame
  * The reference frame for the rotation.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function constructs a rotation matrix from the given axis and
  * angle, and then pre-multiplies it by <tt>self</tt> if refFrame is Local, or
  * post-multipy it by <tt>self</tt> if refFrame is Origin.
  * @exception DNBEZeroVector
  * The given axis is a zero length vector.
  *
  *
  */
    void
    rotate( const DNBBasicVector3D<T> &vec, const T &angle,
            ReferenceFrame refFrame = Local )
        DNB_THROW_SPEC((DNBEZeroVector));

/**
  * Rotates <tt>self</tt> by the given rotation matrix.
  * @param rotate
  * The rotation matrix.
  * @param refFrame
  * The reference frame for the rotation.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function pre-multiplies <tt>rotate</tt> by <tt>self</tt> if refFrame is Local,
  * or post-multipy <tt>rotate</tt> by <tt>self</tt> if refFrame is Origin.
  *
  * EXCPTIONS:
  *
  *
  */
    void
    rotate( const DNBBasicRotate3D<T> &rotate, ReferenceFrame refFrame = Local )
        DNB_THROW_SPEC_NULL;

/**
  * Rotates a given vector by <tt>self</tt>.
  * @param vec
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function rotates the given scl_vector<tt>vec</tt> by <tt>self</tt> and stores
  * the value in <tt>vec</tt>.
  *
  *
  */
    void
    rotateVec( DNBBasicVector3D<T> &vec ) const
        DNB_THROW_SPEC_NULL;

/**
  * Concatenates <tt>self</tt> with the given rotation matrix.
  * @param left
  * The rotation matrix object.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function multiplies the given rotation matrix <tt>left</tt> by <tt>self</tt>
  * and stores the result in <tt>self</tt>.
  *
  *
  */
    void
    preMultiply( const DNBBasicRotate3D<T> &left )
        DNB_THROW_SPEC_NULL;

/**
  * Concatenates <tt>self</tt> with the given rotation matrix.
  * @param right
  * The rotation matrix object.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function multiplies <tt>self</tt> by the given rotation matrix <tt>right</tt>
  * and stores the result in <tt>self</tt>.
  *
  *
  */
    void
    postMultiply( const DNBBasicRotate3D<T> &right )
        DNB_THROW_SPEC_NULL;

/**
  * Inverts <tt>self</tt>.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function computes the inverse of <tt>self</tt> and stores the result
  * in <tt>self</tt>.
  * @exception DNBESingularMatrix
  * <tt>self</tt> cannot be inverted due to its singularity.
  *
  *
  */
    void
    invert( )
        DNB_THROW_SPEC((DNBESingularMatrix));

/**
  * Retrieves the direction vector of <tt>self</tt> by value.
  * @param dirIndex
  * The index of the desired direction vector.
  *
  * @return
  * A copy of the direction vector of <tt>self</tt>.
  *
  * <br><B>Description</B><br>
  * This function returns a copy of the direction vector of <tt>self</tt>
  * specified by <tt>dirIndex</tt>.
  *
  *
  */
    DNBBasicVector3D<T>
    operator()( DirectionIndex dirIndex ) const
        DNB_THROW_SPEC_NULL;

/**
  * Retrieves a directional coordinate of <tt>self</tt> by value.
  * @param dirIndex
  * The index of the desired directin vector.
  * @param coordIndex
  * The index of the desired coordinate.
  *
  * @return
  * A copy of the directional coordinate of <tt>self</tt> specified
  * by <tt>dirIndex</tt> and <tt>coordIndex</tt>.
  *
  * <br><B>Description</B><br>
  * This function returns a copy of the directional coordinate of <tt>self</tt>
  * specified by <tt>dirIndex</tt> and <tt>coordIndex</tt>.
  *
  *
  */
    T
    operator()( DirectionIndex dirIndex,
		CoordinateIndex coordIndex ) const
        DNB_THROW_SPEC_NULL;

/**
  * Copies another rotation matrix <tt>right</tt> to <tt>self</tt>
  * @param right
  * The given rotation matrix to be copied.
  *
  * @return
  * A reference of <tt>self</tt>.
  *
  * <br><B>Description</B><br>
  * This function copies another rotation matrix <tt>right</tt> to <tt>self</tt>.
  *
  *
  */
    DNBBasicRotate3D<T> &
    operator=( const DNBBasicRotate3D<T> &right )
        DNB_THROW_SPEC_NULL;

/**
  * Concatenates <tt>self</tt> with another given rotation matrix.
  * @param right
  * The rotation matrix object to concatenate with <tt>self</tt>.
  *
  * @return
  * A reference of <tt>self</tt>.
  *
  * <br><B>Description</B><br>
  * This function multiplies <tt>self</tt> by the given rotation matrix
  * <tt>right</tt> and stores the result in <tt>self</tt>.
  *
  *
  */
    DNBBasicRotate3D<T> &
    operator*=( const DNBBasicRotate3D<T> &right )
        DNB_THROW_SPEC_NULL;

/**
  * Multiplies <tt>self</tt> by the inverse of the given rotation matrix,
  * and stores the result in <tt>self</tt>.
  * @param right
  * The rotation matrix object.
  *
  * @return
  * A reference of <tt>self</tt>.
  *
  * <br><B>Description</B><br>
  * This function multiplies <tt>self</tt> by the inverse of the given
  * rotation matrix, and stores the result in <tt>self</tt>.
  * @exception DNBESingularMatrix
  * The given rotation matrix <tt>right</tt> cannot be inverted.
  *
  *
  */
    DNBBasicRotate3D<T> &
    operator/=( const DNBBasicRotate3D<T> &right )
        DNB_THROW_SPEC((DNBESingularMatrix));

#if DNB_HAS_FRIEND_TEMPLATE

/**
  * Compares two rotation matrices whether they are equal.
  * @param left
  * The rotation matrix object as the left operand.
  * @param righ
  * The rotation matrix object as the right operand.
  *
  * @return
  * The bool value true, if <tt>left</tt> is equal to <tt>right</tt>,
  * and false otherwise.
  *
  * <br><B>Description</B><br>
  * This function compares two rotation matrices whether they are equal.
  *
  *
  */
    friend ExportedByDNBMath bool
    operator==( const DNBBasicRotate3D<T> &left,
		const DNBBasicRotate3D<T> &right)
        DNB_THROW_SPEC_NULL;


/**
  * Compares two rotation matrices whether they are not equal.
  * @param left
  * The rotation matrix object as the left operand.
  * @param righ
  * The rotation matrix object as the right operand.
  *
  * @return
  * The bool value true, if <tt>left</tt> is not equal to <tt>right</tt>,
  * and false otherwise.
  *
  * <br><B>Description</B><br>
  * This function compares two rotation matrices whether they are equal.
  *
  *
  */
    friend ExportedByDNBMath bool
    operator!=( const DNBBasicRotate3D<T> &left,
		const DNBBasicRotate3D<T> &right)
        DNB_THROW_SPEC_NULL;


/**
  * Multiplies two given rotation matrices.
  *
  * PAMAMETERS:
  * @param left
  * The rotation matrix object as the left operand.
  * @param right
  * The rotation matrix object as the right operand.
  *
  * @return
  * The rotation matrix object contains the result.
  *
  * <br><B>Description</B><br>
  * This function multiplies two given rotation matrices, and returns
  * the result by value.
  *
  *
  */
    friend ExportedByDNBMath DNBBasicRotate3D<T>
    operator*( const DNBBasicRotate3D<T> &left, const DNBBasicRotate3D<T> &right )
        DNB_THROW_SPEC_NULL;

/**
  * Divides two given rotation matrices.
  *
  * PAMAMETERS:
  * @param left
  * The rotation matrix object as the left operand.
  * @param right
  * The rotation matrix object as the right operand.
  *
  * @return
  * The rotation matrix object contains the result.
  *
  * <br><B>Description</B><br>
  * This function divides two given rotation matrices, and returns
  * the result by value.
  *
  *
  */
    friend ExportedByDNBMath DNBBasicRotate3D<T>
    operator/( const DNBBasicRotate3D<T> &left, const DNBBasicRotate3D<T> &right )
        DNB_THROW_SPEC((DNBESingularMatrix));

/**
  * Rotates the given scl_vector<tt>right</tt> by a rotation matrix <tt>left</tt>.
  * @param left
  * The rotation matrix object as the left operand.
  * @param right
  * The 3-D vector object as the right operand.
  *
  * @return
  * A 3-D vector object contains the result.
  *
  * <br><B>Description</B><br>
  * This function rotates the given scl_vector<tt>right</tt> by a rotation
  * matrix <tt>left</tt>, and returns the result by value.
  *
  *
  */
    friend ExportedByDNBMath DNBBasicVector3D<T>
    operator*( const DNBBasicRotate3D<T> &left, const DNBBasicVector3D<T> &right )
        DNB_THROW_SPEC_NULL;

/**
  * Writes the given rotation matrix object <tt>item</tt> to an output stream.
  * @param ostr
  * The output stream to receive the data.
  * @param item
  * The rotation matrix object.
  *
  * @return
  * A reference of the ouput stream.
  *
  * <br><B>Description</B><br>
  * This function write the given rotation matrix object <tt>item</tt> to
  * an output stream <tt>ostr</tt>.
  * @exception scl_bad_alloc
  * The internal storage could not be allocated due to the
  * insufficient memory.
  *
  *
  */
    friend ExportedByDNBMath ostream &
    operator<<( ostream &ostr, const DNBBasicRotate3D<T> &item )
        DNB_THROW_SPEC((scl_bad_alloc));

/**
  * Compares two rotation matrices whether they are "relatively" close.
  * @param left
  * The rotation matrix object.
  * @param right
  * The rotation matrix object.
  * @param angularTol
  * The relative tolerance used in comparing the angles of rotation
  * beteen <tt>left</tt> and <tt>right</tt>.
  * @param linearTol
  * The relative tolerance used in comparing the axes of rotation
  * between <tt>left</tt> and <tt>right</tt>.
  *
  * @return
  * The bool value true, if <tt>left</tt> is "relatively" close to <tt>right</tt>,
  * and false otherwise.
  *
  * <br><B>Description</B><br>
  * This function compares two rotation matrices whether
  * they are "relatively" close.
  *
  * @exception none
  *
  *
  */
    friend ExportedByDNBMath bool
    DNBNearRel( const DNBBasicRotate3D<T> &left, const DNBBasicRotate3D<T> &right,
        const T &angularTol, const T &linearTol )
        DNB_THROW_SPEC_NULL;

/**
  * Compares two rotation matrices whether they are "absolutely" close.
  * @param left
  * The rotation matrix object.
  * @param right
  * The rotation matrix object.
  * @param angularTol
  * The relative tolerance used in comparing the angles of rotation
  * beteen <tt>left</tt> and <tt>right</tt>.
  * @param linearTol
  * The relative tolerance used in comparing the axes of rotation
  * between <tt>left</tt> and <tt>right</tt>.
  *
  * @return
  * The bool value true, if <tt>left</tt> is "absolutely" close to <tt>right</tt>,
  * and false otherwise.
  *
  * <br><B>Description</B><br>
  * This function compares two rotation matrices whether
  * they are "absolutely" close.
  *
  * @exception none
  *
  *
  */
    friend ExportedByDNBMath bool
    DNBNearAbs( const DNBBasicRotate3D<T> &left, const DNBBasicRotate3D<T> &right,
        const T &angularTol, const T &linearTol )
        DNB_THROW_SPEC_NULL;

#endif  /* DNB_HAS_FRIEND_TEMPLATE */


/**
  * The identity rotation matrix.
  *
  * DESCRITION:
  * This object defines a rotation matrix containing the following
  * elements:
  * \begin{veratim}
  * 1  0  0
  * 0  1  0
  * 0  0  1
  * \end{verbatim}
  *
  *
  */
    static const DNBBasicRotate3D<T>    Identity;

#if DNB_HAS_FRIEND_TEMPLATE
private:
#else
public:
#endif
    enum { NumRows = 3 };
    enum { NumCols = 3 };

    static const T                  ZeroTolerance;
    static const T                  DetTolerance;

    static void
    multiply( DNBBasicRotate3D<T> &result, const DNBBasicRotate3D<T> &left,
        const DNBBasicRotate3D<T> &right )
        DNB_THROW_SPEC_NULL;

    static void
    multiply( DNBBasicVector3D<T> &result, const DNBBasicRotate3D<T> &left,
        const DNBBasicVector3D<T> &right )
        DNB_THROW_SPEC_NULL;

    static void
    divide( DNBBasicRotate3D<T> &result, const DNBBasicRotate3D<T> &left,
        const DNBBasicRotate3D<T> &right )
        DNB_THROW_SPEC((DNBESingularMatrix));


    T       data_[ NumRows ][ NumCols ];

    friend class DNBBasicQtn3D<T>;
    friend class DNBBasicXform3D<T>;
};




typedef DNBBasicRotate3D<DNBReal>    DNBRotate3D;




//
//  Include the public definition file.
//
#if     defined(DNB_COMPILE_INSTANTIATE) || defined(_DNBROTATE3D_CC_)
#include "DNBRotate3D.cc"
#elif   defined(_WINDOWS_SOURCE)
/*
extern DNB_DEFINE_CLASS_INST ExportedByDNBMath
DNBBasicRotate3D<float>; */

extern DNB_DEFINE_CLASS_INST ExportedByDNBMath
DNBBasicRotate3D<double>;
#endif



#endif  /* _DNBROTATE3D_H_ */
