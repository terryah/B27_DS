//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview czo/BPL 01:11:13
 */


//*
//* FILE:
//*     SIUnit.cc - public definition file
//*
//* MODULE:
//*     SIUnit    - single template class
//*
//* OVERVIEW:
//*     This module defines the template class DNBBasicSIUnit.  This class
//*     defines all the conversion constants which are used to convert
//*     units to International System of Units (SI).
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     vjk         12/15/98    Initial implementation.
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1998 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*


//
//  Metric prefixes.
//
template <class T>
const T
DNBBasicSIUnit<T>::Tera     = T( 1.0e+12 * 1.0 );

template <class T>
const T
DNBBasicSIUnit<T>::Giga     = T( 1.0e+09 * 1.0 );

template <class T>
const T
DNBBasicSIUnit<T>::Mega     = T( 1.0e+06 * 1.0 );

template <class T>
const T
DNBBasicSIUnit<T>::Kilo     = T( 1.0e+03 * 1.0 );

template <class T>
const T
DNBBasicSIUnit<T>::Hecto    = T( 1.0e+02 * 1.0 );

template <class T>
const T
DNBBasicSIUnit<T>::Deka     = T( 1.0e+01 * 1.0 );

template <class T>
const T
DNBBasicSIUnit<T>::Deci     = T( 1.0e-01 * 1.0 );

template <class T>
const T
DNBBasicSIUnit<T>::Centi    = T( 1.0e-02 * 1.0 );

template <class T>
const T
DNBBasicSIUnit<T>::Milli    = T( 1.0e-03 * 1.0 );

template <class T>
const T
DNBBasicSIUnit<T>::Micro    = T( 1.0e-06 * 1.0 );

template <class T>
const T
DNBBasicSIUnit<T>::Nano     = T( 1.0e-09 * 1.0 );

template <class T>
const T
DNBBasicSIUnit<T>::Pico     = T( 1.0e-12 * 1.0 );


//
//  Angular measures.
//
template <class T>
const T
DNBBasicSIUnit<T>::Radian   = T( 1.0 * 1.0 );

template <class T>
const T
DNBBasicSIUnit<T>::Degree   = T( DNBMathConst::Pi / 180.0 );


//
//  Time units.
//
template <class T>
const T
DNBBasicSIUnit<T>::Second   = T( 1.0 * 1.0 );

template <class T>
const T
DNBBasicSIUnit<T>::Minute   = T( 60.0 * 1.0 );

template <class T>
const T
DNBBasicSIUnit<T>::Hour     = T( 60.0 * 60.0 );

template <class T>
const T
DNBBasicSIUnit<T>::Day      = T( 24.0 * 60.0 * 60.0 );

template <class T>
const T
DNBBasicSIUnit<T>::Week     = T( 7.0 * 24.0 * 60.0 * 60.0 );


//
//  Length units.
//
template <class T>
const T
DNBBasicSIUnit<T>::Meter    = T( 1.0 * 1.0 );

template <class T>
const T
DNBBasicSIUnit<T>::Inch     = T( 1.0 * 2.54e-02 );

template <class T>
const T
DNBBasicSIUnit<T>::Foot     = T( 12.0 * 2.54e-02 );             // 12 inches

template <class T>
const T
DNBBasicSIUnit<T>::Yard     = T( 3.0 * 12.0 * 2.54e-02 );       // 3 feet

template <class T>
const T
DNBBasicSIUnit<T>::Fathom   = T( 6.0 * 12.0 * 2.54e-02 );       // 6 feet

template <class T>
const T
DNBBasicSIUnit<T>::Mile     = T( 5280.0 * 12.0 * 2.54e-02 );    // 5280 feet
