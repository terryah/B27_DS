//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview czo/BPL 01:11:13
 */

//*
//* FILE:
//*     DNBIGCALCFunctions.cc    - 
//*
//* MODULE:
//*     DNBMath
//*
//* OVERVIEW:
//*     This file provides functions that match functions used by D5 IGCALC
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     bpl         09/19/2000  Initial Implementation
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 2000 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*

template < class T >
T
DNBInrange( const T& a, const T& b, const T& c )
	DNB_THROW_SPEC_NULL
{
    // 
    // If b>c, the result is not formaly defined
    // 
    if ( ( a >= b ) && ( a < c ) )
    {
        return( 1 );
    }
    return( 0 );
}

template < class T >
T
DNBRange( const T& a, const T& b, const T& c )
	DNB_THROW_SPEC_NULL
{
    if ( b < a )
    {
        return( a );
    }
    if ( b > c )
    {
        return( c );
    }
    return( b );
}    

template < class T >
T
DNBRamp( const T& xx, const T& x1, const T& y1, const T& x2, const T& y2 )
	DNB_THROW_SPEC_NULL
{
    T a = (y2-y1)/(x2-x1);
    T b = y1 - a*x1;
    T value = a*xx + b;
    if ( value < y1 )
    {
        return( y1 );
    }
    else if ( value > y2 )
    {
        return( y2 );
    }
    return( value );
}

template < class T >
T
DNBInt( const T& a )
	DNB_THROW_SPEC_NULL
{
    T value = ( DNBInteger32 ) a;
    return( value );
}
