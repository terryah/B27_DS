//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 */


//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     CZO         09/09/99    Initial implementation.
//*     bkh         11/04/03    Implementation of new documentation style.
//		smw			01/07/2005		Removed extractors due to iostream migration to the
//									new, standard, templated iostream, and due to migration
//									to 64 bit builds.  Extractor for all math classes have
//									been moved, for easy rescucitation, if necessary,
//									to DNBMath.tst/PrivateInterfaces/DNBMathExtractors.h
//*
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNBEULER3D_H_
#define _DNBEULER3D_H_


#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>

#include <DNBMath3D.h>




//
//  Forward declarations.
//
template <class T>
class DNBBasicRotate3D;

template <class T>
class DNBBasicEuler3D;

template <class T>
ostream &
operator<<( ostream &ostr, const DNBBasicEuler3D<T> &euler )
    DNB_THROW_SPEC((scl_bad_alloc));




/**
  * A templated class which represents a three-dimensional rotation.
  *
  * <br><B>Template Parameter(s)</B><br>
  * @param T
  * The base type of the euler elements.
  *
  * <br><B>Description</B><br>
  * This module defines a class which contains three floating point values
  * to contain three angles, and three integer values to contain three axes
  * corresponding to the three angles respectively.
  *
  * An instance of this class represents three consective rotations about
  * the principle axes. The main motivation for this class was to provide
  * an interface, since there are totally 12 different combinations of axes,
  * i.e. ZYZ, ZYX ... This class supports all possible combinations.
  *
  * Interanlly, this class uses 0 as axis X, 1 for Y, and 2 for Z, but
  * usrs are encouraged to use DNBMath3D::XAxis, DNBMath3D::YAxis and
  * DNBMath3D::ZAxis. However, the angles can be any floating point values.
  *
  *
  */
template <class T>
class DNBBasicEuler3D : public DNBMath3D
{
public:
/**
  * Constructs an euler object which represents an identity.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function creates an euler object which represents an identity.
  *
  *
  */
    DNBBasicEuler3D( )
	DNB_THROW_SPEC_NULL;


/**
  * Creates an euler object using copy semantics.
  * @param right
  * The euler object to be copies.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function creates an euler, and then copies the elements
  * of <tt>right</tt> to <tt>self</tt>.
  *
  *
  */
    DNBBasicEuler3D( const DNBBasicEuler3D<T> &right )
	DNB_THROW_SPEC_NULL;

/**
  * Creates an euler from three angles and three axes.
  * @param angle1
  * The angle for the first rotation.
  * @param angle2
  * The angle for the second rotation.
  * @param angle3
  * The angle for the third rotation.
  * @param axis1
  * The axis for the first rotation.
  * @param axis2
  * The axis for the second rotation.
  * @param axis3
  * The axis for the third rotation.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function creates an euler from three angle-axis pairs.
  *
  *
  */
    DNBBasicEuler3D( const T &angle1, AxisType axis1,
		     const T &angle2 = T(0), AxisType axis2 = YAxis,
		     const T &angle3 = T(0), AxisType axis3 = ZAxis )
        DNB_THROW_SPEC_NULL;

/**
  * Destroys <tt>self</tt>.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function destroys <tt>self</tt>.
  *
  *
  */
    ~DNBBasicEuler3D( )
	DNB_THROW_SPEC_NULL;

/**
  * Retrieves the three angles from <tt>self</tt>.
  * @param angle1
  * The base type object to receive the angle for the first rotation.
  * @param angle2
  * The base type object to receive the angle for the second rotation.
  * @param angle3
  * The base type object to receive the angle for the third rotation.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function retrieves the three angles from <tt>self</tt>.
  *
  *
  */
    void
    getAngles( T &angle1, T &angle2, T &angle3 ) const
	DNB_THROW_SPEC_NULL;

/**
  * Retrieves the three axes from <tt>self</tt>.
  * @param axis1
  * The AxisType object to recieve the axis for the first rotation.
  * @param axis2
  * The AxisType object to recieve the axis for the second rotation.
  * @param axis3
  * The AxisType object to recieve the axis for the third rotation.
  *
  * @return
  * Nothing.
  *
  * DECRIPTION:
  * This function retrieves the three axes of <tt>self</tt>.
  *
  *
  */
    void
    getAxes( AxisType &axis1, AxisType &axis2, AxisType &axis3 ) const
	DNB_THROW_SPEC_NULL;

/**
  * Retrieves the angle, axis pair for rotation indexed by <tt>step</tt>.
  * @param step
  * The integer between 0 and 2. 0 for the first rotation,
  * 1 for the second, and 2 for the third rotation.
  * @param angle
  * The base type object to receive the angle.
  * @param axis
  * The AxisType object to receive the axis.
  *
  * @return
  * Nothing.
  *
  * DESCRIPTIONS:
  * Retrieves the angle, axis pair indexed by <tt>step</tt>.
  *
  *
  */
    void
    get( int step, T &angle, AxisType &axis ) const
	DNB_THROW_SPEC_NULL;

/**
  * Changes the three angles of <tt>self</tt>.
  * @param angle1
  * The angle for rotation one.
  * @param angle2
  * The angle for rotation two.
  * @param angle3
  * The angle for rotation third.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function changes <tt>self</tt>'s three angles.
  *
  *
  */
    void
    setAngles( const T &angle1, const T &angle2, const T &angle3 )
	DNB_THROW_SPEC_NULL;

    void
    setAxes( AxisType axis1, AxisType axis2, AxisType axis3 )
	DNB_THROW_SPEC_NULL;

/**
  * Changes the angle, axis pair for the rotation indexed by <tt>step</tt>.
  * @param step
  * An integer value between 0 and 2, inclusive. 0 indicats
  * rotation one, 1 for rotation two, and 2 for rotation three.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function changes the angle, axis pair for the rotation
  * indexed by <tt>step</tt>.
  *
  *
  */
    void
    set( int step, const T &angle, AxisType axis )
	DNB_THROW_SPEC_NULL;

/**
  * Inverts <tt>self</tt>.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function inverts <tt>self</tt>.
  *
  *
  */
    void
    invert( )
	DNB_THROW_SPEC_NULL;

/**
  * Copies elements of <tt>right</tt> to <tt>self</tt>.
  * @param right
  * The euler object to be copied.
  *
  * @return
  * A reference of <tt>self</tt>.
  *
  * <br><B>Description</B><br>
  * This function copies the elements of <tt>right</tt> to <tt>self</tt>.
  *
  *
  */
    DNBBasicEuler3D<T> &
    operator=( const DNBBasicEuler3D<T> &right )
	DNB_THROW_SPEC_NULL;

/**
  * Compares <tt>self</tt> and another euler <tt>right</tt> whether they are equal.
  * @param right
  * The euler object to be compared with <tt>self</tt>.
  *
  * @return
  * The bool value true, if <tt>self</tt> is equal to <tt>right</tt>,
  * and false otherwise.
  *
  * <br><B>Description</B><br>
  * This function compares <tt>self</tt> and another euler <tt>right</tt>
  * whether they are equal.
  *
  *
  */
    bool
    operator==( const DNBBasicEuler3D<T> &right )
	DNB_THROW_SPEC_NULL;

/**
  * Compares <tt>self</tt> and another euler <tt>right</tt> whether they are not equal.
  * @param right
  * The euler object to be compared with <tt>self</tt>.
  *
  * @return
  * The bool value true, if <tt>self</tt> is not equal to <tt>right</tt>, and
  * false otherwise.
  *
  * <br><B>Description</B><br>
  * This function compares <tt>self</tt> and another euler <tt>right</tt>
  * whether they are not equal.
  *
  *
  */
    bool
    operator!=( const DNBBasicEuler3D<T> &right )
	DNB_THROW_SPEC_NULL;

#if DNB_HAS_FRIEND_TEMPLATE

/**
  * Writes the given euler to an output stream.
  * @param ostr
  * The output stream to receive the data.
  * @param euler
  * The euler object to insert to the output stream.
  *
  * @return
  * A reference of the output stream.
  *
  * <br><B>Description</B><br>
  * This function writes the given euler object to an output stream.
  * @exception scl_bad_alloc
  * The interanl storage cannot be allocated due to the system
  * insufficient memory.
  *
  *
  */
    friend ExportedByDNBMath ostream &
    operator<<( ostream &ostr, const DNBBasicEuler3D<T> &euler )
        DNB_THROW_SPEC((scl_bad_alloc));

#endif  /* DNB_HAS_FRIEND_TEMPLATE */

/**
  * The identity Euler angles.
  *
  * <br><B>Description</B><br>
  * The object defines an Euler object consisting the following elements:
  * three angles are zero, three axes are DNBMath3D::ZAxis,
  * DNBMath3D:YAxis and DNBMath3D::ZAxis.
  *
  *
  */
    static const DNBBasicEuler3D<T> Identity;

#if DNB_HAS_FRIEND_TEMPLATE
private:
#else
public:
#endif
    T   angles_[3];
    int axes_[3];

    friend class DNBBasicRotate3D<T>;
};




typedef DNBBasicEuler3D<DNBReal> DNBEuler3D;




//
//  Include the public definition file.
//
#if     defined(DNB_COMPILE_INSTANTIATE) || defined(_DNBEULER3D_CC_)
#include "DNBEuler3D.cc"
#elif   defined(_WINDOWS_SOURCE)
/*
extern DNB_DEFINE_CLASS_INST ExportedByDNBMath
DNBBasicEuler3D<float>; */

extern DNB_DEFINE_CLASS_INST ExportedByDNBMath
DNBBasicEuler3D<double>;
#endif




#endif  /* _DNBEULER3D_H_ */
