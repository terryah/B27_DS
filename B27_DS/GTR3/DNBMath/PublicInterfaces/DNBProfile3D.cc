//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview czo/BPL 01:11:13
 */


template <class T>
DNBBasicProfile3D<T>::DNBBasicProfile3D( )
    DNB_THROW_SPEC_NULL :

    DNBMath3D       ( ),
    location_       ( ),                // Identity matrix
    linearVel_      ( ),                // Zero vector
    angularVel_     ( ),                //      "
    linearAccel_    ( ),                //      "
    angularAccel_   ( )                 //      "
{
    // Nothing
}


template <class T>
DNBBasicProfile3D<T>::DNBBasicProfile3D( const DNBBasicProfile3D<T> &right )
    DNB_THROW_SPEC_NULL :

    DNBMath3D       ( right ),
    location_       ( right.location_ ),
    linearVel_      ( right.linearVel_ ),
    angularVel_     ( right.angularVel_ ),
    linearAccel_    ( right.linearAccel_ ),
    angularAccel_   ( right.angularAccel_ )
{
    // Nothing
}


template <class T>
DNBBasicProfile3D<T>::~DNBBasicProfile3D( )
    DNB_THROW_SPEC_NULL
{
    // Nothing
}


template <class T>
DNBBasicProfile3D<T> &
DNBBasicProfile3D<T>::operator=( const DNBBasicProfile3D<T> &right )
    DNB_THROW_SPEC_NULL
{
    if ( this == &right )               // Prevent self-assignment.
        return *this;

    DNBMath3D::operator=( right );
    location_     = right.location_;
    linearVel_    = right.linearVel_;
    angularVel_   = right.angularVel_;
    linearAccel_  = right.linearAccel_;
    angularAccel_ = right.angularAccel_;

    return *this;
}


template <class T>
void
DNBBasicProfile3D<T>::setLocation( const Xform3D &location )
    DNB_THROW_SPEC_NULL
{
    location_ = location;
}


template <class T>
void
DNBBasicProfile3D<T>::getLocation( Xform3D &location ) const
    DNB_THROW_SPEC_NULL
{
    location = location_;
}


template <class T>
const
typename
DNBBasicProfile3D<T>::Xform3D &
DNBBasicProfile3D<T>::getLocation( ) const
    DNB_THROW_SPEC_NULL
{
    return location_;
}


template <class T>
void
DNBBasicProfile3D<T>::clearVelocity( )
    DNB_THROW_SPEC_NULL
{
    linearVel_  = Vector3D::Zero;
    angularVel_ = Vector3D::Zero;
}


template <class T>
void
DNBBasicProfile3D<T>::setVelocity( const Vector3D &linear,
    const Vector3D &angular, ReferenceFrame frame )
    DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION( frame == Origin );

    linearVel_  = linear;
    angularVel_ = angular;
}


template <class T>
void
DNBBasicProfile3D<T>::getVelocity( Vector3D &linear, Vector3D &angular,
    ReferenceFrame frame ) const
    DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION( frame == Origin );

    linear  = linearVel_;
    angular = angularVel_;
}


template <class T>
void
DNBBasicProfile3D<T>::clearAcceleration( )
    DNB_THROW_SPEC_NULL
{
    linearAccel_  = Vector3D::Zero;
    angularAccel_ = Vector3D::Zero;
}


template <class T>
void
DNBBasicProfile3D<T>::setAcceleration( const Vector3D &linear,
    const Vector3D &angular, ReferenceFrame frame )
    DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION( frame == Origin );

    linearAccel_  = linear;
    angularAccel_ = angular;
}


template <class T>
void
DNBBasicProfile3D<T>::getAcceleration( Vector3D &linear, Vector3D &angular,
    ReferenceFrame frame ) const
    DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION( frame == Origin );

    linear  = linearAccel_;
    angular = angularAccel_;
}


template <class T>
const DNBBasicProfile3D<T>  DNBBasicProfile3D<T>::Identity;
