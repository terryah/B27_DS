//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//
//  Notes:
//    + This class will eventually include support for spatial transforms and
//      vectors.
//
//  TODO:
//    + Implement the conversion operations for setVelocity(), getVelocity(),
//      setAcceleration(), and getAcceleration().
//
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNBPROFILE3D_H_
#define _DNBPROFILE3D_H_


#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>


#include <DNBMath3D.h>
#include <DNBVector3D.h>
#include <DNBXform3D.h>




//
//  This class characterizes the instantaneous location, velocity, and
//  acceleration of a Cartesian coordinate system.  These quantities are
//  collectively referred to as the profile of the coordinate system.
//
template <class T>
class DNBBasicProfile3D : public DNBMath3D
{
public:
    typedef DNBBasicVector3D<T>     Vector3D;

    typedef DNBBasicXform3D<T>      Xform3D;

    DNBBasicProfile3D( )
        DNB_THROW_SPEC_NULL;

    DNBBasicProfile3D( const DNBBasicProfile3D<T> &right )
        DNB_THROW_SPEC_NULL;

    ~DNBBasicProfile3D( )
        DNB_THROW_SPEC_NULL;

    DNBBasicProfile3D<T> &
    operator=( const DNBBasicProfile3D<T> &right )
        DNB_THROW_SPEC_NULL;

    void
    setLocation( const Xform3D &location )
        DNB_THROW_SPEC_NULL;

    void
    getLocation( Xform3D &location ) const
        DNB_THROW_SPEC_NULL;

    const Xform3D &
    getLocation( ) const
        DNB_THROW_SPEC_NULL;

    void
    clearVelocity( )
        DNB_THROW_SPEC_NULL;

    void
    setVelocity( const Vector3D &linear, const Vector3D &angular,
        ReferenceFrame frame = Origin )
        DNB_THROW_SPEC_NULL;

    void
    getVelocity( Vector3D &linear, Vector3D &angular,
        ReferenceFrame frame = Origin ) const
        DNB_THROW_SPEC_NULL;

    void
    clearAcceleration( )
        DNB_THROW_SPEC_NULL;

    void
    setAcceleration( const Vector3D &linear, const Vector3D &angular,
        ReferenceFrame frame = Origin )
        DNB_THROW_SPEC_NULL;

    void
    getAcceleration( Vector3D &linear, Vector3D &angular,
        ReferenceFrame frame = Origin ) const
        DNB_THROW_SPEC_NULL;

    static const DNBBasicProfile3D<T>   Identity;

private:
    //
    //  The following quantities specify the kinematic state of the local frame
    //  with respect to the origin.
    //
    Xform3D     location_;
    Vector3D    linearVel_;
    Vector3D    angularVel_;
    Vector3D    linearAccel_;
    Vector3D    angularAccel_;
};




typedef DNBBasicProfile3D<DNBReal>      DNBProfile3D;




//
//  Include the public definition file.
//
#if     defined(DNB_COMPILE_INSTANTIATE) || defined(_DNBPROFILE3D_H_)
#include "DNBProfile3D.cc"
#endif




#endif  /* _DNBPROFILE3D_H_ */
