/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview CZO smw 03:09:12
 */
//==============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2002
//==============================================================================
//
// DNBMathVector.h
//      Interface for the DNBBasicMathVector templated class. This is a class that provides 
//      n-dimensional vector ( equivalent to a one dimensional array), with capability to do 
//      mathematical operations.
//      DNBMathVector is also used as the basis for DNBMathMatrix.
//      Provides a replacement for RWMathVec, previously used in DELMIA code.
//
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//      smw         6/18/2003       Initial Implementation
//      bkh         08/07/2003      Added Global streaming in.
//      smw         11/24/2003      Integrated the use of DNBPoolAllocator
//		smw			01/07/2005		Removed extractors due to iostream migration to the
//									new, standard, templated iostream, and due to migration 
//									to 64 bit builds.  Extractor for all math classes have 
//									been moved, for easy rescucitation, if necessary,
//									to DNBMath.tst/PrivateInterfaces/DNBMathExtractors.h
// 
//
//=====================================================================

#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif

#ifndef _DNBMATHVECTOR_H_
#define _DNBMATHVECTOR_H_

#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>
#include <DNBException.h>

#include <DNBCMath.h>     // Needed for NaN
#include <DNBMathND.h>    // Basic definitions used in N-Dimenstional math
#include <DNBPoolAllocator.h>


template <class T>
class DNBBasicMathMatrix;            // needed for declaring the matrix a friend class.
                                     // Matrix needs to access the vector when
                                     // explicitly constructing one, which the matrix
                                     // needs to do if it uses the pool allocator



/***********************************************************************************
// The DNBBasicVector templated class
/**********************************************************************************/


template <class T> class DNBBasicMathVector
{
public:


/*****************************************************************************************
// Default Vector capacity: an empty vector will have that much reserved for it
*****************************************************************************************/

enum { defaultCapacity = 6 };



/***********************************************************************************
// Constructors
/***********************************************************************************/

/**
  * Constructs an empty vector ( has length = 0). It can later be resized or reshaped.
  * A certain amount of space (equal to defaultCapacity) is reserved for the 
  * vector.  Bad_alloc is thrown if there is not enough space in memory to reserve for 
  * the empty vector
  */
DNBBasicMathVector ()
         DNB_THROW_SPEC((scl_bad_alloc));


/**
  * Constructs a vector with n elements, initialized to initVal.  If no initVal
  * is supplied, vector is initialized with the zero of the field defined by T.
  */
DNBBasicMathVector (size_t n, const T& initVal = T(0))
        DNB_THROW_SPEC((scl_bad_alloc));


/**
  * Constructs a vector with n uninitialized elements
  */ 
DNBBasicMathVector (size_t n, UninitializedType dummy)
        DNB_THROW_SPEC((scl_bad_alloc));


/**
  * Constructs a vector with n elements, and initializes it with the data stored in memory 
  * location pointed to by dat. 
  * A copy of the data is made. dat must point to at least n elements.
  */
DNBBasicMathVector (const T* dat,size_t n)
        DNB_THROW_SPEC((scl_bad_alloc));



/**
  * Constructs a vector from the null terminated character string s. The format of the 
  * character string is the same as that expected by the global operator operator>>
  * Example string: "[0.0 1.0 3.0]"
  */
/*
DNBBasicMathVector(const char *s)
        DNB_THROW_SPEC((scl_bad_alloc));  // FIXME; This is not yet implemented; 
*/



/**
* Copy constructor
*/
DNBBasicMathVector(const DNBBasicMathVector<T>& v) 
        DNB_THROW_SPEC((scl_bad_alloc));



/*****************************************************************************************
// Destructor
*****************************************************************************************/
~DNBBasicMathVector() DNB_THROW_SPEC_NULL ;

/*****************************************************************************************
// Memory Management using DNBPoolAllocator
*****************************************************************************************/
// Commented out because it causes problems when we use placement new on the vector from
// within the matrix class:  placement calls this new below and not the placement new
/*
void*
operator new(size_t n)
DNB_THROW_SPEC((scl_bad_alloc))
{
     return( DNBPoolAllocator::Global().allocate(n));
};

void
operator delete(void* ptr, size_t size)
{
    (DNBBasicMathVector<T> *) ptr->~DNBBasicMathVector<T>();
    DNBPoolAllocator::Global().deallocate( ptr, size );
};

void*
operator new[]( size_t size)
DNB_THROW_SPEC((scl_bad_alloc))
{
    return DNBPoolAllocator::Global().allocate( size );
};

void
operator delete[](void* ptr, size_t size)
{

    DNBPoolAllocator::Global().deallocate( ptr, size );

};
*/

/*****************************************************************************************
// Member Functions
*****************************************************************************************/


/**
* Returns the number of elements in the vector.
*/
size_t
length() const
DNB_THROW_SPEC_NULL;


/**
* Changes the length of the vector to n elements. After reshaping, the contents of additional
* vector elements, if any, are undefined, unless init is specified to be true.
*/
void
reshape(size_t n, bool init = false)
DNB_THROW_SPEC((scl_bad_alloc));


/**
 * Changes the size of the vector to n elements, adding 0s or truncating as necessary.
 */
void
resize(size_t n)
DNB_THROW_SPEC((scl_bad_alloc));


/**
  * Returns the allocator object used to allocate memory for this vector
  */
const DNBPoolAllocator*
getAllocator() const DNB_THROW_SPEC_NULL { return alloc_;};


/**
  * Returns the capacity of the vector, i.e. the number of slots reserved for
  * the vector ( room to grow the vector without need to re-allocate memory).
  */
size_t 
getCapacity() const DNB_THROW_SPEC_NULL { return capacity_;};


/**
 * Returns a copy of the vector
 */
DNBBasicMathVector<T>
copy() const DNB_THROW_SPEC((scl_bad_alloc)) {return DNBBasicMathVector<T>((*this));};


/**
 * Returns a vector which is a copy of this vector from index = start to but not
 * including start+n;
 */
DNBBasicMathVector<T>
slice(int start, size_t n) const DNB_THROW_SPEC_NULL 
{
    T* dat = data_ + start;
    return (DNBBasicMathVector<T> (dat, n));
}


/*********************************************************************************
// Access
*********************************************************************************/

/**
 * Returns the vector element at index where index ranges from 0 to and 
 * including length()-1.
 */
T&
operator[](size_t index)
DNB_THROW_SPEC_NULL;


/**
  * Const version of above.
 */

const T&
operator[](size_t index) const
DNB_THROW_SPEC_NULL;


/**
 * Same as operator[]
 */
T&
operator() (size_t index)
DNB_THROW_SPEC_NULL;


/**
 * Same as operator[], const version
 */
const T&
operator() (size_t index) const
DNB_THROW_SPEC_NULL;


/**
  * Returns element at index i of the vector where i ranges from 1 to and including
  * length
  */
T&
fromOne(size_t i) DNB_THROW_SPEC_NULL { 
    DNB_PRECONDITION( i > 0 );
    return data1_[i];};


/**
  * Same as above, const version
  */

const T&
fromOne(size_t i) const DNB_THROW_SPEC_NULL { 
    DNB_PRECONDITION( i > 0);
    return data1_[i];};


/*********************************************************************************/
// Mathematical
/*********************************************************************************/

/**
 * Increment each element of self. This is invoked when the operator is used as a 
 * postfix operator. 
 */
void                
operator++(int)
DNB_THROW_SPEC_NULL;

/**
 * Increment each element of self. This is invoked when the operator is used as a 
 * prefix operator. Returns the vector after increment.
 */
DNBBasicMathVector<T>&     
operator++()
DNB_THROW_SPEC_NULL;


/**
 * Decrement each element of self.  This is invoked when the operator is used as a 
 * postfix operator.
 */
void                
operator--(int)
DNB_THROW_SPEC_NULL;

/**
 * Decrement each element of self. This is invoked when the operator is used as a 
 * prefix operator. Returns the vector after decrement.
 */
DNBBasicMathVector<T>&     
operator--()
DNB_THROW_SPEC_NULL;


/**
 * Assignment operator
 */
void
operator=(const DNBBasicMathVector<T>& v)
DNB_THROW_SPEC_NULL;


/**
  * Assigns a value of type T to each element of the vector
  */
void
operator=(const T&)
DNB_THROW_SPEC_NULL;


/**
  * Adds each element of the vector v to the corresponding element of this vector
  */
void
operator+=(const DNBBasicMathVector<T>& v)
DNB_THROW_SPEC_NULL;


/**
  * Subtracts each element of the vector v from the corresponding element of this vector
  */
void
operator-=(const DNBBasicMathVector<T>& v)
DNB_THROW_SPEC_NULL;


/**
  * Multiplies each element of the vector v by the corresponding element of this vector
  */

void
operator*=(const DNBBasicMathVector<T>& v)
DNB_THROW_SPEC_NULL;

/**
  * Divide each element of the vector v by the corresponding element of this vector
  */
void
operator/=(const DNBBasicMathVector<T>& v)
DNB_THROW_SPEC_NULL;


/**
  * Adds the value of type T to each element of this vector
  */
void
operator+=(const T&)
DNB_THROW_SPEC_NULL;


/**
  * Subtracts the value of type T from each element of this vector
  */
void
operator-=(const T&)
DNB_THROW_SPEC_NULL;

/**
  * Multiplies each element of this vector by the value of type T
  */
void
operator*=(const T&)
DNB_THROW_SPEC_NULL;


/**
  * Divides each element of this vector by the value of type T
  */
void
operator/=(const T&)
DNB_THROW_SPEC_NULL;


/***********************************************************************************/
// Relational Logical Operators
/**********************************************************************************/
/**
 * Returns True if the vectors have the same length and all corresponding elements
 * are equal
 */
bool 
operator==(const DNBBasicMathVector<T>& v2) const
DNB_THROW_SPEC_NULL;


/**
 * Returns True if the vectors are of different size, or if they are of the same size and at 
 * least one element in v1 is different from its counterpart in v2
 */
bool 
operator!=(const DNBBasicMathVector<T>& v2) const
DNB_THROW_SPEC_NULL;



/**
 * Returns True if every element in v1 is less than its counterpart in v2.  Assumes both 
 * vectors have the same size.
 */
bool operator<(const DNBBasicMathVector<T>& v2) const
DNB_THROW_SPEC_NULL;


/**
 * Returns True if every element in v1 is less than or equal to its counterpart in v2.  
 * Assumes both vectors have the same size.
 */
bool operator<=(const DNBBasicMathVector<T>& v2) const
DNB_THROW_SPEC_NULL;



/**
 * Returns True if every element in v1 is greater than its counterpart in v2.  Assumes both 
 * vectors have the same size.
 */
bool 
operator>(const DNBBasicMathVector<T>& v2) const
DNB_THROW_SPEC_NULL;



/**
 * Returns True if every element in v1 is greater than or equal to its counterpart in v2.  
 * Assumes both vectors have the same size.
 */
bool operator>=(const DNBBasicMathVector<T>& v2) const
DNB_THROW_SPEC_NULL;


protected:
    static void Assign ( T* dest, size_t destSize, const T* source);
    static void Assign (T* dest, size_t destSize, const T& value);

private:
    size_t              size_;
    T *                 data_;
    T *                 data1_;
    DNBPoolAllocator*   alloc_;
    size_t              capacity_;
    void                allocate(size_t reqSize);
    void                reallocate( size_t reqSize, bool init );
    void                deallocate();
    friend class DNBBasicMathMatrix<T>;


/**********************************************************************************/
//  Constructors that do MATH!  called by the global math operators on vectors
/**********************************************************************************/

public:
/**
  * @nodoc
  * Additive inverse
  */
DNBBasicMathVector( const DNBBasicMathVector<T> &v,
                    const _DNBSubOp&)
                    DNB_THROW_SPEC((scl_bad_alloc));

/**
 * @nodoc
 * Vector Addition
 */
DNBBasicMathVector( const DNBBasicMathVector<T> &left,
                    const DNBBasicMathVector<T> &right, 
                    const _DNBAddOp &)
                    DNB_THROW_SPEC((scl_bad_alloc));


/**
 * @nodoc
 * Vector subtraction
 */
DNBBasicMathVector( const DNBBasicMathVector<T> &left,
                    const DNBBasicMathVector<T> &right, 
                    const _DNBSubOp &)
                    DNB_THROW_SPEC((scl_bad_alloc));


/**
 * @nodoc
 * Vector element by element multiplication
 */
DNBBasicMathVector( const DNBBasicMathVector<T> &left,
                    const DNBBasicMathVector<T> &right, 
                    const _DNBMulOp &)
                    DNB_THROW_SPEC((scl_bad_alloc));



/**
 * @nodoc
 * Vector element by element division (first argument divided by second argument)
 */
DNBBasicMathVector( const DNBBasicMathVector<T> &left,
                    const DNBBasicMathVector<T> &right, 
                    const _DNBDivOp &)
                    DNB_THROW_SPEC((scl_bad_alloc));


/**
 * @nodoc
 * Vector + scalar or scalar + vector
 */
DNBBasicMathVector( const DNBBasicMathVector<T> &vec, 
                    const T& aVal, 
                    const _DNBAddOp &)
                    DNB_THROW_SPEC((scl_bad_alloc));


/**
 * @nodoc
 * vector - scalar
 */
DNBBasicMathVector( const DNBBasicMathVector<T> &left,
                    const T& aVal,
                    const _DNBSubOp &)
                    DNB_THROW_SPEC((scl_bad_alloc));

/**
 * @nodoc
 * scalar - vector
 */
DNBBasicMathVector( const T& aVal, 
                    const DNBBasicMathVector<T> &right,
                    const _DNBSubOp &)
                    DNB_THROW_SPEC((scl_bad_alloc));


/**
 * @nodoc
 * vector * scalar or scalar * vector
 */
DNBBasicMathVector( const DNBBasicMathVector<T> &vec,
                    const T& aVal, 
                    const _DNBMulOp &)
                    DNB_THROW_SPEC((scl_bad_alloc));

/**
 * @nodoc
 * vector / scalar
 */
DNBBasicMathVector( const DNBBasicMathVector<T> &left,
                    const T& aVal, 
                    const _DNBDivOp &)
                    DNB_THROW_SPEC((scl_bad_alloc));


/**
 * @nodoc
 * scalar / vector
 */
DNBBasicMathVector( const T& aVal,
                    const DNBBasicMathVector<T> &right,
                    const _DNBDivOp &)
                    DNB_THROW_SPEC((scl_bad_alloc));

};


/**********************************************************************************
* Global Mathematical Operators
***********************************************************************************/


/**
  * Returns the additive inverse of the argument vector 
  */
template <class T>
DNBBasicMathVector<T>    
                          operator-(const DNBBasicMathVector<T>&)
                          DNB_THROW_SPEC((scl_bad_alloc));


/** 
 * returns a new vector where each element is the sum of the two corresponding
 * elements of the two input vectors
 */
template <class T>
DNBBasicMathVector<T>    
                          operator+(const DNBBasicMathVector<T>&,
                                    const DNBBasicMathVector<T>&)
                                    DNB_THROW_SPEC((scl_bad_alloc));

/** 
 * returns a new vector where each element is the difference of the two corresponding
 * elements of the two input vectors ( first input vector - second input vector)
 */
template <class T>
DNBBasicMathVector<T>
                        operator-(const DNBBasicMathVector<T>&,
                                  const DNBBasicMathVector<T>&)
                                  DNB_THROW_SPEC((scl_bad_alloc));

/** 
 * returns a new vector where each element is the product of the two corresponding
 * elements of the two input vectors
 */
template <class T>
DNBBasicMathVector<T>    operator*(const DNBBasicMathVector<T>&,
                                   const DNBBasicMathVector<T>&)
                                   DNB_THROW_SPEC((scl_bad_alloc));

/** 
 * returns a new vector where each element is the ratio of the two corresponding
 * elements of the two input vectors (firs input vector element/ second input vector element)
 */
template <class T>
DNBBasicMathVector<T>    operator/(const DNBBasicMathVector<T>&,
                                   const DNBBasicMathVector<T>&)
                                   DNB_THROW_SPEC((scl_bad_alloc));

/** 
 * returns a new vector where each element is the sum of corresponding
 * element of the input vector and the input constant
 */
template <class T>
DNBBasicMathVector<T>    operator+(const DNBBasicMathVector<T>&,
                                   const T&)
                                   DNB_THROW_SPEC((scl_bad_alloc));

/** 
 * returns a new vector where each element is the sum of corresponding
 * element of the input vector and the input constant
 */
template <class T>
DNBBasicMathVector<T>    operator+(const T&,
                                   const DNBBasicMathVector<T>&)
                                   DNB_THROW_SPEC((scl_bad_alloc));

/** 
 * returns a new vector where each element is the differnece of corresponding
 * element of the input vector and the input constant
 */
template <class T>
DNBBasicMathVector<T>    operator-(const DNBBasicMathVector<T>&,
                                   const T&)
                                   DNB_THROW_SPEC((scl_bad_alloc));

/** 
 * returns a new vector where each element is the difference of corresponding
 * element of the input vector and the input constant
 */
template <class T>
DNBBasicMathVector<T>    operator-(const T&,
                                   const DNBBasicMathVector<T>&)
                                   DNB_THROW_SPEC((scl_bad_alloc));

/** 
 * returns a new vector where each element is the product of corresponding
 * element of the input vector and the input constant
 */
template <class T>
DNBBasicMathVector<T>    operator*(const DNBBasicMathVector<T>&,
                                   const T&)
                                   DNB_THROW_SPEC((scl_bad_alloc));

/** 
 * returns a new vector where each element is the product of corresponding
 * element of the input vector and the input constant
 */
template <class T>
DNBBasicMathVector<T>    operator*(const T&,
                                   const DNBBasicMathVector<T>&)
                                   DNB_THROW_SPEC((scl_bad_alloc));

/** 
 * returns a new vector where each element is the ratio of corresponding
 * element of the input vector and the input constant
 */
template <class T>
DNBBasicMathVector<T>    operator/(const DNBBasicMathVector<T>&,
                                   const T&)
                                   DNB_THROW_SPEC((scl_bad_alloc));

/** 
 * returns a new vector where each element is the ratio of corresponding
 * element of the input vector and the input constant
 */
template <class T>
DNBBasicMathVector<T>    operator/(const T&,
                                   const DNBBasicMathVector<T>&)
                                   DNB_THROW_SPEC((scl_bad_alloc));


/**
 * Returns the dot product of the two vectors
 */
template <class T>
T DNBDot (const DNBBasicMathVector<T>& ,
          const DNBBasicMathVector<T>&)
          DNB_THROW_SPEC_NULL;


/**
 * Returns the max absolute value of all elements
 */
template <class T>
T
DNBLinfNorm( const DNBBasicMathVector<T> &vec)
           DNB_THROW_SPEC_NULL;


/**
 * Returns the sum of the absolute values of all elements
 */
template <class T>
T
DNBL1Norm( const DNBBasicMathVector<T> &vec)
           DNB_THROW_SPEC_NULL;


/**
  * Returns the minimum absolute value of all elements
  */
template <class T>
T
DNBMinAbsValue(const DNBBasicMathVector<T> &vec)
    DNB_THROW_SPEC_NULL;



// From RW: Other Related Global Functions (templetized)

// use APPLY to do most of this
//acos         asin         atan
//atan2        ceil         cos
//cosh         cumsum       delta
//dot(DONE)          exp          expandEven
//expandOdd    floor        log
//log10        maxIndex     maxValue
//mean         minIndex     minValue
//pow          prod         reverse (DO)
//sin          sinh         sqrt
//sum          tan          tanh

// From RW: Other Related Global Functions (non-templetized)

//abs (DO)         arg                    conj
//conjDot      expandConjugateEven    expandConjugateOdd
//frobNorm(DO)     imag                   l1Norm(DONE)
//l2Norm(DO)       linfNorm(DONE)               norm(DO same as RW)
//maxNorm      real                   rootsOfOne
//variance     spectralVariance       toChar
//toFloat      toInt


/*********************************************************************************
* Global Method Streaming out
*********************************************************************************/

/**
 * Outputs a vector v to ostream s beginning with a left bracket [ and terminating 
 * with a right bracket ]. The output numbers are separated by 1 tab. 
 *If T is a user-defined type, the following operator must be defined:
 *<tt>          operator<<(ostream&s, constT& t); </tt>
 */
template <class T>
ostream&    
operator<<(ostream& s, const DNBBasicMathVector<T>& v)
DNB_THROW_SPEC_NULL;



/*********************************************************************************
// Global  Methods used by the batch ODT for testing
/********************************************************************************/

template <class T>
bool
DNBNearAbs( const DNBBasicMathVector<T> &left, 
            const DNBBasicMathVector<T> &right,
            const T &tolerance)
            DNB_THROW_SPEC_NULL;

template <class T>
bool
DNBNearRel( const DNBBasicMathVector<T> &left, 
            const DNBBasicMathVector<T> &right,
            const T &tolerance)
            DNB_THROW_SPEC_NULL;


/*********************************************************************************
// The typedef, etc..
*********************************************************************************/

typedef DNBBasicMathVector<DNBReal> DNBMathVector; 

//
//  Include the public definition file (.cc file), if compile time instantiate is supported,
//  or if this .h file is being included within the .cpp file)
//
#if     defined(DNB_COMPILE_INSTANTIATE) || defined(_DNBMATHVECTOR_CC_) 
#include "DNBMathVector.cc"
#endif

#endif              /* _DNBMATHVECTOR_H_ */
