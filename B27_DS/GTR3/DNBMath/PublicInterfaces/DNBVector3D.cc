//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview czo/BPL 01:11:13
 */


//*
//* FILE:
//*     DNBVector3D.cc    - principle implementation file
//*
//* MODULE:
//*     DNBBasicVector3D  - single template class
//*
//* OVERVIEW:
//*     This module defines a three-dimensional point vector.
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     pjf         09/02/97    Initial implementation
//*     jad         08/07/98    Ported to MSVC 5.0.  Added support for reverse
//*                             iterators and other standard STL methods.  Made
//*                             several changes to enhance compatibility with
//*                             the class RWMathVec<T>.
//*     jad         08/10/98    Added the base class DNBMath3D and renamed the
//*                             class to DNBBasicVector3D.
//*     jad         08/26/98    Renamed the function DNBEqual to DNBNearAbs.
//*                             Added the functions DNBNearRel and DNBAbs.
//*     smw         05/22/03    Added support for Spherical and Cylindrical coordinates.
//*     smw         08/02/03    Removing interoperability with RWMathVec
//*		smw			01/07/05	Removed extractors due to iostream migration to the
//*								new, standard, templated iostream, and due to migration to 64 bit
//*								builds.  Extractor for all math classes have been moved, 
//*								for easy rescucitation, if necessary,
//*								to DNBMath.tst/PrivateInterfaces/DNBMathExtractors.h
//* 
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1997-2002 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*
#include <DNBSystemBase.h>
#include <scl_limits.h>
#include <scl_algorithm.h>
#include <DNBSystemDefs.h>


#include <DNBCMath.h>
#include <DNBMathVector.h>                  // for streaming

template <class T>
DNBBasicVector3D<T>::DNBBasicVector3D( )
    DNB_THROW_SPEC_NULL :
    DNBMath3D   ( )
{
    data_[ X ] = T( 0 );
    data_[ Y ] = T( 0 );
    data_[ Z ] = T( 0 );
}


template <class T>
DNBBasicVector3D<T>::DNBBasicVector3D( const DNBBasicVector3D<T> &right )
    DNB_THROW_SPEC_NULL :
    DNBMath3D   ( right )
{
    data_[ X ] = right.data_[ X ];
    data_[ Y ] = right.data_[ Y ];
    data_[ Z ] = right.data_[ Z ];
}


template <class T>
DNBBasicVector3D<T>::DNBBasicVector3D( UninitializedType dummy )
    DNB_THROW_SPEC_NULL :
    DNBMath3D   ( dummy )
{
#if DNB_VERIFY
    T       NaN;
    DNBSetNaN( NaN );

    data_[ X ] = NaN;
    data_[ Y ] = NaN;
    data_[ Z ] = NaN;
#endif
}


template <class T>
DNBBasicVector3D<T>::DNBBasicVector3D( const T &x, const T &y, const T &z )
    DNB_THROW_SPEC_NULL :
    DNBMath3D   ( )
{
    data_[ X ] = x;
    data_[ Y ] = y;
    data_[ Z ] = z;
}


// SMW - Support spherical coord

/**
* Sets the vector using spherical coordinates
*/
template <class T>
void DNBBasicVector3D<T>::setSpherical( const T &theta, const T &phi, const T &r )
    DNB_THROW_SPEC_NULL
    {
    T sinTheta, cosTheta, sinPhi, cosPhi;
    DNBSinCos(theta, sinTheta, cosTheta);
    DNBSinCos(phi, sinPhi, cosPhi);
    data_[ X ] = r * cosTheta * sinPhi;
    data_[ Y ] = r * sinTheta * sinPhi;
    data_[ Z ] = r * cosPhi;
    }

/**
* Gets the vector's spherical coordinates
*/
template <class T>
void DNBBasicVector3D<T>::getSpherical (T &theta, T &phi, T &r)
    DNB_THROW_SPEC_NULL
    {
    theta = atan(data_[ Y ] / data_[ X]);
    phi = atan (sqrt((data_[ X ]*data_[ X ] + data_[ Y ]*data_[ Y ])) / data_[ Z ]);
    r = sqrt(data_[ X ]*data_[ X ] + data_[ Y ]*data_[ Y ] + data_[ Z ]*data_[ Z ]);
    }


// SMW Support cylindrical coord

/**
* Sets the vector using cylindrical coordinates
*/
template <class T>
void DNBBasicVector3D<T>::setCylindrical( const T &theta, const T &r, const T &d )
    DNB_THROW_SPEC_NULL
    {
    T sinTheta, cosTheta;
    DNBSinCos(theta, sinTheta, cosTheta);
    data_[ X ] = r * cosTheta;
    data_[ Y ] = r * sinTheta;
    data_[ Z ] = d;

    }

/**
* Gets the vector's spherical coordinates
*/
template <class T>
void DNBBasicVector3D<T>::getCylindrical (T &theta, T &r, T &d)
    DNB_THROW_SPEC_NULL
    {
    theta = atan(data_[ Y ] / data_[ X ]);
    r = sqrt(data_[ X ] * data_[ X ] + data_[ Y ] * data_[ Y ]);
    d = data_[ Z ];
    }

//----------------------------------------------------------------------------------------------------
template <class T>
DNBBasicVector3D<T>::DNBBasicVector3D( const T &right )
    DNB_THROW_SPEC_NULL :
    DNBMath3D   ( )
{
    data_[ X ] = right;
    data_[ Y ] = right;
    data_[ Z ] = right;
}


template <class T>
DNBBasicVector3D<T>::DNBBasicVector3D( const T *right )
    DNB_THROW_SPEC_NULL :
    DNBMath3D   ( )
{
    DNB_PRECONDITION( right != NULL );

    data_[ X ] = right[ 0 ];
    data_[ Y ] = right[ 1 ];
    data_[ Z ] = right[ 2 ];
}

template <class T>
DNBBasicVector3D<T>::DNBBasicVector3D( const DNBBasicMathVector<T> &right )
    DNB_THROW_SPEC_NULL :
    DNBMath3D   ( )
{
    DNB_PRECONDITION( right.length( ) >= 3 );

    data_[ X ] = right( 0 );
    data_[ Y ] = right( 1 );
    data_[ Z ] = right( 2 );
}

template <class T>
DNBBasicVector3D<T>::~DNBBasicVector3D( )
    DNB_THROW_SPEC_NULL
{
    // Nothing
}


template <class T>
typename
DNBBasicVector3D<T>::iterator
DNBBasicVector3D<T>::begin(  )
    DNB_THROW_SPEC_NULL
{
    return iterator(data_);
}


template <class T>
typename
DNBBasicVector3D<T>::const_iterator
DNBBasicVector3D<T>::begin(  ) const
    DNB_THROW_SPEC_NULL
{
    return const_iterator(data_);
}


template <class T>
typename
DNBBasicVector3D<T>::iterator
DNBBasicVector3D<T>::end(  )
    DNB_THROW_SPEC_NULL
{
    return iterator(data_ + 3);
}


template <class T>
typename
DNBBasicVector3D<T>::const_iterator
DNBBasicVector3D<T>::end(  ) const
    DNB_THROW_SPEC_NULL
{
    return const_iterator(data_ + 3);
}


template <class T>
typename
DNBBasicVector3D<T>::reverse_iterator
DNBBasicVector3D<T>::rbegin( )
    DNB_THROW_SPEC_NULL
{
    reverse_iterator        itr( end() );
    return itr;
}


template <class T>
typename
DNBBasicVector3D<T>::const_reverse_iterator
DNBBasicVector3D<T>::rbegin( ) const
    DNB_THROW_SPEC_NULL
{
    const_reverse_iterator  itr( end() );
    return itr;
}


template <class T>
typename
DNBBasicVector3D<T>::reverse_iterator
DNBBasicVector3D<T>::rend( )
    DNB_THROW_SPEC_NULL
{
    reverse_iterator        itr( begin() );
    return itr;
}


template <class T>
typename
DNBBasicVector3D<T>::const_reverse_iterator
DNBBasicVector3D<T>::rend( ) const
    DNB_THROW_SPEC_NULL
{
    const_reverse_iterator  itr( begin() );
    return itr;
}


template <class T>
void
DNBBasicVector3D<T>::set( const T &x, const T &y, const T &z )
    DNB_THROW_SPEC_NULL
{
    data_[ X ] = x;
    data_[ Y ] = y;
    data_[ Z ] = z;
}

template <class T>
void
DNBBasicVector3D<T>::setCartesian( const T &x, const T &y, const T &z )
    DNB_THROW_SPEC_NULL
{
    data_[ X ] = x;
    data_[ Y ] = y;
    data_[ Z ] = z;
}


template <class T>
void
DNBBasicVector3D<T>::get( T &x, T &y, T &z ) const
    DNB_THROW_SPEC_NULL
{
    x = data_[ X ];
    y = data_[ Y ];
    z = data_[ Z ];
}

template <class T>
void
DNBBasicVector3D<T>::getCartesian( T &x, T &y, T &z ) const
    DNB_THROW_SPEC_NULL
{
    x = data_[ X ];
    y = data_[ Y ];
    z = data_[ Z ];
}


template <class T>
void
DNBBasicVector3D<T>::swap( DNBBasicVector3D<T> &other )
    DNB_THROW_SPEC_NULL
{
    DNBBasicVector3D<T> temp( *this );
    *this = other;
    other = temp;
}


template <class T>
void
DNBBasicVector3D<T>::normalize( )
    DNB_THROW_SPEC((DNBEZeroVector))
{
    scl_numeric_limits<T>   traits;
    DNB_ASSERT( traits.is_specialized );

    T       length = DNBL2Norm( *this );

    if ( length <= traits.epsilon() )
    {
        DNBEZeroVector  error( DNB_FORMAT("The vector has zero length") );
        throw error;
    }

    data_[ X ] /= length;
    data_[ Y ] /= length;
    data_[ Z ] /= length;
}


template <class T>
typename
DNBBasicVector3D<T>::reference
DNBBasicVector3D<T>::operator()( CoordinateIndex coordIndex )
    DNB_THROW_SPEC_NULL
{
    return data_[ coordIndex ];
}


template <class T>
typename
DNBBasicVector3D<T>::const_reference
DNBBasicVector3D<T>::operator()( CoordinateIndex coordIndex ) const
    DNB_THROW_SPEC_NULL
{
    return data_[ coordIndex ];
}


template <class T>
typename
DNBBasicVector3D<T>::reference
DNBBasicVector3D<T>::operator[]( CoordinateIndex coordIndex )
    DNB_THROW_SPEC_NULL
{
    return data_[ coordIndex ];
}


template <class T>
typename
DNBBasicVector3D<T>::const_reference
DNBBasicVector3D<T>::operator[]( CoordinateIndex coordIndex ) const
    DNB_THROW_SPEC_NULL
{
    return data_[ coordIndex ];
}


template <class T>
typename
DNBBasicVector3D<T>::reference
DNBBasicVector3D<T>::operator[]( size_type index )
    DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION( index < 3 );
    return data_[ index ];
}


template <class T>
typename
DNBBasicVector3D<T>::const_reference
DNBBasicVector3D<T>::operator[]( size_type index ) const
    DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION( index < 3 );
    return data_[ index ];
}


template <class T>
DNBBasicVector3D<T>
DNBBasicVector3D<T>::operator-( ) const
    DNB_THROW_SPEC_NULL
{
    return DNBBasicVector3D<T>( -data_[ X ], -data_[ Y ], -data_[ Z ] );
}


template <class T>
DNBBasicVector3D<T>
DNBBasicVector3D<T>::operator+( ) const
    DNB_THROW_SPEC_NULL
{
    return *this;
}


template <class T>
DNBBasicVector3D<T> &
DNBBasicVector3D<T>::operator++( )
    DNB_THROW_SPEC_NULL
{
    ++data_[ X ];
    ++data_[ Y ];
    ++data_[ Z ];

    return *this;
}


template <class T>
DNBBasicVector3D<T>
DNBBasicVector3D<T>::operator++( int )
    DNB_THROW_SPEC_NULL
{
    DNBBasicVector3D<T> temp( *this );

    data_[ X ]++;
    data_[ Y ]++;
    data_[ Z ]++;

    return temp;
}


template <class T>
DNBBasicVector3D<T> &
DNBBasicVector3D<T>::operator--( )
    DNB_THROW_SPEC_NULL
{
    --data_[ X ];
    --data_[ Y ];
    --data_[ Z ];

    return *this;
}


template <class T>
DNBBasicVector3D<T>
DNBBasicVector3D<T>::operator--( int )
    DNB_THROW_SPEC_NULL
{
    DNBBasicVector3D<T>     temp( *this );

    data_[ X ]--;
    data_[ Y ]--;
    data_[ Z ]--;

    return temp;
}


template <class T>
DNBBasicVector3D<T> &
DNBBasicVector3D<T>::operator=( const DNBBasicVector3D<T> &right )
    DNB_THROW_SPEC_NULL
{
    if ( this == &right )               // Prevent self-assignment.
        return *this;

    data_[ X ] = right.data_[ X ];
    data_[ Y ] = right.data_[ Y ];
    data_[ Z ] = right.data_[ Z ];

    return *this;
}


template <class T>
DNBBasicVector3D<T> &
DNBBasicVector3D<T>::operator+=( const DNBBasicVector3D<T> &right )
    DNB_THROW_SPEC_NULL
{
    data_[ X ] += right.data_[ X ];
    data_[ Y ] += right.data_[ Y ];
    data_[ Z ] += right.data_[ Z ];

    return *this;
}


template <class T>
DNBBasicVector3D<T> &
DNBBasicVector3D<T>::operator-=( const DNBBasicVector3D<T> &right )
    DNB_THROW_SPEC_NULL
{
    data_[ X ] -= right.data_[ X ];
    data_[ Y ] -= right.data_[ Y ];
    data_[ Z ] -= right.data_[ Z ];

    return *this;
}


template <class T>
DNBBasicVector3D<T> &
DNBBasicVector3D<T>::operator*=( const DNBBasicVector3D<T> &right )
    DNB_THROW_SPEC_NULL
{
    data_[ X ] *= right.data_[ X ];
    data_[ Y ] *= right.data_[ Y ];
    data_[ Z ] *= right.data_[ Z ];

    return *this;
}


template <class T>
DNBBasicVector3D<T> &
DNBBasicVector3D<T>::operator/=( const DNBBasicVector3D<T> &right )
    DNB_THROW_SPEC_NULL
{
    data_[ X ] /= right.data_[ X ];
    data_[ Y ] /= right.data_[ Y ];
    data_[ Z ] /= right.data_[ Z ];

    return *this;
}


template <class T>
DNBBasicVector3D<T> &
DNBBasicVector3D<T>::operator=( const T &right )
    DNB_THROW_SPEC_NULL
{
    data_[ X ] = right;
    data_[ Y ] = right;
    data_[ Z ] = right;

    return *this;
}


template <class T>
DNBBasicVector3D<T> &
DNBBasicVector3D<T>::operator+=( const T &right )
    DNB_THROW_SPEC_NULL
{
    data_[ X ] += right;
    data_[ Y ] += right;
    data_[ Z ] += right;

    return *this;
}


template <class T>
DNBBasicVector3D<T> &
DNBBasicVector3D<T>::operator-=( const T &right )
    DNB_THROW_SPEC_NULL
{
    data_[ X ] -= right;
    data_[ Y ] -= right;
    data_[ Z ] -= right;

    return *this;
}


template <class T>
DNBBasicVector3D<T> &
DNBBasicVector3D<T>::operator*=( const T &right )
    DNB_THROW_SPEC_NULL
{
    data_[ X ] *= right;
    data_[ Y ] *= right;
    data_[ Z ] *= right;

    return *this;
}


template <class T>
DNBBasicVector3D<T> &
DNBBasicVector3D<T>::operator/=( const T &right )
    DNB_THROW_SPEC_NULL
{
    data_[ X ] /= right;
    data_[ Y ] /= right;
    data_[ Z ] /= right;

    return *this;
}


template <class T>
bool
DNBBasicVector3D<T>::operator==( const DNBBasicVector3D<T> &right ) const
    DNB_THROW_SPEC_NULL
{
    return
        (data_[ X ] == right.data_[ X ]) &&
        (data_[ Y ] == right.data_[ Y ]) &&
        (data_[ Z ] == right.data_[ Z ]);
}


template <class T>
bool
DNBBasicVector3D<T>::operator!=( const DNBBasicVector3D<T> &right ) const
    DNB_THROW_SPEC_NULL
{
    return
        (data_[ X ] != right.data_[ X ]) ||
        (data_[ Y ] != right.data_[ Y ]) ||
        (data_[ Z ] != right.data_[ Z ]);
}

template <class T>
DNBBasicVector3D<T>::operator DNBBasicMathVector<T>() const
    DNB_THROW_SPEC((scl_bad_alloc))
{
    return DNBBasicMathVector<T>( data_, 3 );
}

template <class T>
DNBBasicVector3D<T>
operator+( const DNBBasicVector3D<T> &left, const DNBBasicVector3D<T> &right )
    DNB_THROW_SPEC_NULL
{
    return DNBBasicVector3D<T>(
        left.data_[ DNBMath3D::X ] + right.data_[ DNBMath3D::X ],
        left.data_[ DNBMath3D::Y ] + right.data_[ DNBMath3D::Y ],
        left.data_[ DNBMath3D::Z ] + right.data_[ DNBMath3D::Z ] );
}


template <class T>
DNBBasicVector3D<T>
operator-( const DNBBasicVector3D<T> &left, const DNBBasicVector3D<T> &right )
    DNB_THROW_SPEC_NULL
{
    return DNBBasicVector3D<T>(
        left.data_[ DNBMath3D::X ] - right.data_[ DNBMath3D::X ],
        left.data_[ DNBMath3D::Y ] - right.data_[ DNBMath3D::Y ],
        left.data_[ DNBMath3D::Z ] - right.data_[ DNBMath3D::Z ] );
}


template <class T>
DNBBasicVector3D<T>
operator*( const DNBBasicVector3D<T> &left, const DNBBasicVector3D<T> &right )
    DNB_THROW_SPEC_NULL
{
    return DNBBasicVector3D<T>(
        left.data_[ DNBMath3D::X ] * right.data_[ DNBMath3D::X ],
        left.data_[ DNBMath3D::Y ] * right.data_[ DNBMath3D::Y ],
        left.data_[ DNBMath3D::Z ] * right.data_[ DNBMath3D::Z ] );
}


template <class T>
DNBBasicVector3D<T>
operator/( const DNBBasicVector3D<T> &left, const DNBBasicVector3D<T> &right )
    DNB_THROW_SPEC_NULL
{
    return DNBBasicVector3D<T>(
        left.data_[ DNBMath3D::X ] / right.data_[ DNBMath3D::X ],
        left.data_[ DNBMath3D::Y ] / right.data_[ DNBMath3D::Y ],
        left.data_[ DNBMath3D::Z ] / right.data_[ DNBMath3D::Z ] );
}


template <class T>
DNBBasicVector3D<T>
operator+( const DNBBasicVector3D<T> &left, const T &right )
    DNB_THROW_SPEC_NULL
{
    return DNBBasicVector3D<T>(
        left.data_[ DNBMath3D::X ] + right,
        left.data_[ DNBMath3D::Y ] + right,
        left.data_[ DNBMath3D::Z ] + right );
}


template <class T>
DNBBasicVector3D<T>
operator+( const T &left, const DNBBasicVector3D<T> &right )
    DNB_THROW_SPEC_NULL
{
    return DNBBasicVector3D<T>(
        left + right.data_[ DNBMath3D::X ],
        left + right.data_[ DNBMath3D::Y ],
        left + right.data_[ DNBMath3D::Z ] );
}


template <class T>
DNBBasicVector3D<T>
operator-( const DNBBasicVector3D<T> &left, const T &right )
    DNB_THROW_SPEC_NULL
{
    return DNBBasicVector3D<T>(
        left.data_[ DNBMath3D::X ] - right,
        left.data_[ DNBMath3D::Y ] - right,
        left.data_[ DNBMath3D::Z ] - right );
}


template <class T>
DNBBasicVector3D<T>
operator-( const T &left, const DNBBasicVector3D<T> &right )
    DNB_THROW_SPEC_NULL
{
    return DNBBasicVector3D<T>(
        left - right.data_[ DNBMath3D::X ],
        left - right.data_[ DNBMath3D::Y ],
        left - right.data_[ DNBMath3D::Z ] );
}


template <class T>
DNBBasicVector3D<T>
operator*( const DNBBasicVector3D<T> &left, const T &right )
    DNB_THROW_SPEC_NULL
{
    return DNBBasicVector3D<T>(
        left.data_[ DNBMath3D::X ] * right,
        left.data_[ DNBMath3D::Y ] * right,
        left.data_[ DNBMath3D::Z ] * right );
}


template <class T>
DNBBasicVector3D<T>
operator*( const T &left, const DNBBasicVector3D<T> &right )
    DNB_THROW_SPEC_NULL
{
    return DNBBasicVector3D<T>(
        left * right.data_[ DNBMath3D::X ],
        left * right.data_[ DNBMath3D::Y ],
        left * right.data_[ DNBMath3D::Z ] );
}


template <class T>
DNBBasicVector3D<T>
operator/( const DNBBasicVector3D<T> &left, const T &right )
    DNB_THROW_SPEC_NULL
{
    return DNBBasicVector3D<T>(
        left.data_[ DNBMath3D::X ] / right,
        left.data_[ DNBMath3D::Y ] / right,
        left.data_[ DNBMath3D::Z ] / right );
}


template <class T>
DNBBasicVector3D<T>
operator/( const T &left, const DNBBasicVector3D<T> &right )
    DNB_THROW_SPEC_NULL
{
    return DNBBasicVector3D<T>(
        left / right.data_[ DNBMath3D::X ],
        left / right.data_[ DNBMath3D::Y ],
        left / right.data_[ DNBMath3D::Z ] );
}


template <class T>
ostream &
operator<<( ostream &ostr, const DNBBasicVector3D<T> &item )
    DNB_THROW_SPEC((scl_bad_alloc))
{
    DNBBasicMathVector<T>    vecItem( item.data_, 3 );
    return (ostr << vecItem);
}



template <class T>
T
DNBSum( const DNBBasicVector3D<T> &right )
    DNB_THROW_SPEC_NULL
{
    return (
        right.data_[ DNBMath3D::X ] +
        right.data_[ DNBMath3D::Y ] +
        right.data_[ DNBMath3D::Z ] );
}


template <class T>
DNBBasicVector3D<T>
DNBAbs( const DNBBasicVector3D<T> &right )
    DNB_THROW_SPEC_NULL
{
    return DNBBasicVector3D<T>(
        abs( right.data_[ DNBMath3D::X ] ),
        abs( right.data_[ DNBMath3D::Y ] ),
        abs( right.data_[ DNBMath3D::Z ] ) );
}


template <class T>
T
DNBL1Norm( const DNBBasicVector3D<T> &right )
    DNB_THROW_SPEC_NULL
{
    T   sum =
        abs( right.data_[ DNBMath3D::X ] ) +
        abs( right.data_[ DNBMath3D::Y ] ) +
        abs( right.data_[ DNBMath3D::Z ] );

    return sum;
}


template <class T>
T
DNBL2Norm( const DNBBasicVector3D<T> &right )
    DNB_THROW_SPEC_NULL
{
    T   sum =
        DNBSqr( right.data_[ DNBMath3D::X ] ) +
        DNBSqr( right.data_[ DNBMath3D::Y ] ) +
        DNBSqr( right.data_[ DNBMath3D::Z ] );

    return sqrt( sum );
}


template <class T>
T
DNBLinfNorm( const DNBBasicVector3D<T> &right )
    DNB_THROW_SPEC_NULL
{
    T   absX = abs( right.data_[ DNBMath3D::X ] );
    T   absY = abs( right.data_[ DNBMath3D::Y ] );
    T   absZ = abs( right.data_[ DNBMath3D::Z ] );

    T   result = scl_max( scl_max( absX, absY ), absZ );
    return result;
}


template <class T>
T
DNBL2NormSqr( const DNBBasicVector3D<T> &right )
    DNB_THROW_SPEC_NULL
{
    T   sum =
        DNBSqr( right.data_[ DNBMath3D::X ] ) +
        DNBSqr( right.data_[ DNBMath3D::Y ] ) +
        DNBSqr( right.data_[ DNBMath3D::Z ] );

    return sum;
}


template <class T>
bool
DNBNearRel( const DNBBasicVector3D<T> &left, const DNBBasicVector3D<T> &right,
    const T &tolerance )
    DNB_THROW_SPEC_NULL
{
    if ( tolerance <= T(0) )
        return ( left == right );

    T   normLeft  = DNBLinfNorm( left );
    T   normRight = DNBLinfNorm( right );
    T   normError = DNBLinfNorm( left - right );

    return ( normError <= (tolerance * scl_max(normLeft, normRight)) );
}


template <class T>
bool
DNBNearAbs( const DNBBasicVector3D<T> &left, const DNBBasicVector3D<T> &right,
    const T &tolerance )
    DNB_THROW_SPEC_NULL
{
    if ( tolerance <= T(0) )
        return ( left == right );

    T   normError = DNBLinfNorm( left - right );

    return ( normError <= tolerance );
}


template <class T>
T
DNBDot( const DNBBasicVector3D<T> &left, const DNBBasicVector3D<T> &right )
    DNB_THROW_SPEC_NULL
{
    return (
        left.data_[ DNBMath3D::X ] * right.data_[ DNBMath3D::X ] +
        left.data_[ DNBMath3D::Y ] * right.data_[ DNBMath3D::Y ] +
        left.data_[ DNBMath3D::Z ] * right.data_[ DNBMath3D::Z ] );
}


template <class T>
DNBBasicVector3D<T>
DNBCross( const DNBBasicVector3D<T> &left, const DNBBasicVector3D<T> &right )
    DNB_THROW_SPEC_NULL
{
    return DNBBasicVector3D<T>(
        left.data_[ DNBMath3D::Y ] * right.data_[ DNBMath3D::Z ] -
        left.data_[ DNBMath3D::Z ] * right.data_[ DNBMath3D::Y ],

        left.data_[ DNBMath3D::Z ] * right.data_[ DNBMath3D::X ] -
        left.data_[ DNBMath3D::X ] * right.data_[ DNBMath3D::Z ],

        left.data_[ DNBMath3D::X ] * right.data_[ DNBMath3D::Y ] -
        left.data_[ DNBMath3D::Y ] * right.data_[ DNBMath3D::X ] );
}


template <class T>
void
DNBBasicVector3D<T>::lerp( DNBBasicVector3D<T> &result, const T &t,
			   const DNBBasicVector3D<T> &from,
  		           const DNBBasicVector3D<T> &to )
    DNB_THROW_SPEC_NULL
{
    if( t <= T(0) )
    {
	result = from;
	return;
    }
    else if( t >= T(1) )
    {
	result = to;
	return;
    }

    result = ( T(1) - t ) * from + t * to;
}


template <class T>
const DNBBasicVector3D<T>   DNBBasicVector3D<T>::Zero  =
    DNBBasicVector3D<T>( T(0), T(0), T(0) );


template <class T>
const DNBBasicVector3D<T>   DNBBasicVector3D<T>::UnitX =
    DNBBasicVector3D<T>( T(1), T(0), T(0) );


template <class T>
const DNBBasicVector3D<T>   DNBBasicVector3D<T>::UnitY =
    DNBBasicVector3D<T>( T(0), T(1), T(0) );


template <class T>
const DNBBasicVector3D<T>   DNBBasicVector3D<T>::UnitZ =
    DNBBasicVector3D<T>( T(0), T(0), T(1) );
