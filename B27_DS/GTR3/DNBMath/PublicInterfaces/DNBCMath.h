//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNB_CMATH_H_
#define _DNB_CMATH_H_


#include <DNBSystemBase.h>
#include <scl_cmath.h>
#include <DNBSystemDefs.h>


#include <DNBMath.h>


//
//  ANSI C mathematical functions for single-precision numbers.
//
inline float sin( float x );
inline float cos( float x );
inline float tan( float x );
inline float asin( float x );
inline float acos( float x );
inline float atan( float x );
inline float atan2( float y, float x );

inline float sinh( float x );
inline float cosh( float x );
inline float tanh( float x );

inline float exp( float x );
inline float log( float x );
inline float log10( float x );
inline float modf( float x, float *ip );

inline float sqrt( float x );
inline float pow( float x, float y );

inline float ceil( float x );
inline float floor( float x );
inline float abs( float x );
inline float fmod( float x, float y );


//
//  ANSI C mathematical functions for double-precision numbers.
//
#if     defined(_MSC_EXTENSIONS) || defined(__sgi)
inline double abs( double x );
#endif


template <class T>
void
DNBSinCos( const T &angle, T &sinValue, T &cosValue )
    DNB_THROW_SPEC_NULL;


template <class T>
inline T
DNBSignum( const T &right )
    DNB_THROW_SPEC_NULL;


template <class T>
inline T
DNBSqr( const T &right )
    DNB_THROW_SPEC_NULL;


template <class T>
inline T
DNBCube( const T &right )
    DNB_THROW_SPEC_NULL;


template <class T>
inline T
DNBSqrt( const T &arg )
    DNB_THROW_SPEC_NULL;

template <class T>
inline T
DNBAsin( const T &arg )
    DNB_THROW_SPEC_NULL;

template <class T>
inline T
DNBAcos( const T &arg )
    DNB_THROW_SPEC_NULL;

template <class T>
inline T
DNBAtan( const T &arg )
    DNB_THROW_SPEC_NULL;

template <class T>
inline T
DNBAtan2( const T &arg1, const T &arg2 )
    DNB_THROW_SPEC_NULL;

//
//  This function determines if two floating-point values are "relatively" close
//  to one another.  It returns the boolean result:
//
//         | left - right |
//      ----------------------  <= tolerance
//      max( |left|, |right| )
//
//  If <tolerance> <= 0, the function determines if <left> is precisely equal
//  to <right>.
//
inline bool
DNBNearRel( float left, float right, float tolerance = 1.0e-5 )
    DNB_THROW_SPEC_NULL;


inline bool
DNBNearRel( double left, double right, double tolerance = 1.0e-5 )
    DNB_THROW_SPEC_NULL;


//
//  This function determines if two floating-point values are "absolutely" close
//  to one another.  It returns the boolean result:
//
//      | left - right | <= tolerance
//
//  If <tolerance> <= 0, the function determines if <left> is precisely equal
//  to <right>.
//
inline bool
DNBNearAbs( float left, float right, float tolerance = 1.0e-5 )
    DNB_THROW_SPEC_NULL;


inline bool
DNBNearAbs( double left, double right, double tolerance = 1.0e-5 )
    DNB_THROW_SPEC_NULL;


//
//  IEEE floating-point infinity.
//
inline bool
DNBIsInf( float value )
    DNB_THROW_SPEC_NULL;


inline bool
DNBIsInf( double value )
    DNB_THROW_SPEC_NULL;


inline void
DNBSetInf( float &value )
    DNB_THROW_SPEC_NULL;


inline void
DNBSetInf( double &value )
    DNB_THROW_SPEC_NULL;


//
//  IEEE floating-point NaN.
//
inline bool
DNBIsNaN( float value )
    DNB_THROW_SPEC_NULL;


inline bool
DNBIsNaN( double value )
    DNB_THROW_SPEC_NULL;


inline void
DNBSetNaN( float &value )
    DNB_THROW_SPEC_NULL;


inline void
DNBSetNaN( double &value )
    DNB_THROW_SPEC_NULL;




//
//  Include the public definition file.
//
#include "DNBCMath.cc"




#endif  /* _DNB_CMATH_H_ */
