//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview czo/BPL 01:11:13
 *
 * Enhancement    jyv    06/10/02	Fix IR 328347A: Add sqrt and inverse
 *					trig. function wrappers;hacked from
 *					fmath.c
 *
 */
#include <DNBSystemBase.h>
#include <DNBMathConst.h>

#include <scl_algorithm.h>
#include <scl_limits.h>

#if     defined(_WIN32)
#include <float.h>
#elif   defined(__SUNPRO_CC) || defined(__sgi)
#include <ieeefp.h>
#elif   defined(__xlC__)
#include <float.h>
#elif   defined(__HP_aCC)
#include <math.h>
#else
#error  "Unsupported platform."
#endif

//#if     defined(__SUNPRO_CC) 
//#include <rw/mathdefs.h>                // for abs(double)
//#endif

#include <DNBSystemDefs.h>


//
//  ANSI C mathematical functions for single-precision numbers.
//
#if     !DNB_HAS_FLOAT_MATH


#if     DNB_HAS_FLOAT_MATHF

inline float sin( float x )             { return sinf( x ); }
inline float cos( float x )             { return cosf( x ); }
inline float tan( float x )             { return tanf( x ); }
inline float asin( float x )            { return asinf( x ); }
inline float acos( float x )            { return acosf( x ); }
inline float atan( float x )            { return atanf( x ); }
inline float atan2( float y, float x )  { return atan2f( y, x ); }

inline float sinh( float x )            { return sinhf( x ); }
inline float cosh( float x )            { return coshf( x ); }
inline float tanh( float x )            { return tanhf( x ); }

inline float exp( float x )             { return expf( x ); }
inline float log( float x )             { return logf( x ); }
inline float log10( float x )           { return log10f( x ); }

#ifdef  __HP_aCC
inline float modf( float x, float *ip )
    { double dp, dr = modf( double(x), &dp ); *ip = dp; return dr; }
#else
inline float modf( float x, float *ip ) { return modff( x, ip ); }
#endif

inline float sqrt( float x )            { return sqrtf( x ); }
inline float pow( float x, float y )    { return powf( x, y ); }

#ifdef  __HP_aCC
inline float ceil( float x )            { return ceil( double(x) ); }
inline float floor( float x )           { return floor( double(x) ); }
#else
inline float ceil( float x )            { return ceilf( x ); }
inline float floor( float x )           { return floorf( x ); }
#endif

inline float abs( float x )             { return fabsf( x ); }
inline float fmod( float x, float y )   { return fmodf( x, y ); }

#elif   DNB_HAS_FLOAT_FMATH

inline float sin( float x )             { return fsin( x ); }
inline float cos( float x )             { return fcos( x ); }
inline float tan( float x )             { return ftan( x ); }
inline float asin( float x )            { return fasin( x ); }
inline float acos( float x )            { return facos( x ); }
inline float atan( float x )            { return fatan( x ); }
inline float atan2( float y, float x )  { return fatan2( y, x ); }

inline float sinh( float x )            { return fsinh( x ); }
inline float cosh( float x )            { return fcosh( x ); }
inline float tanh( float x )            { return ftanh( x ); }

inline float exp( float x )             { return fexp( x ); }
inline float log( float x )             { return flog( x ); }
inline float log10( float x )           { return flog10( x ); }
inline float modf( float x, float *ip ) { return modff( x, ip ); }  // sic

inline float sqrt( float x )            { return fsqrt( x ); }
inline float pow( float x, float y )    { return powf( x, y ); }    // sic

inline float ceil( float x )            { return fceil( x ); }
inline float floor( float x )           { return ffloor( x ); }
inline float abs( float x )             { return fabsf( x ); }      // sic
inline float fmod( float x, float y )   { return fmodf( x, y ); }   // sic

#else   /* !DNB_HAS_FLOAT_MATHF && !DNB_HAS_FLOAT_FMATH */

inline float sin( float x )             { return sin( double(x) ); }
inline float cos( float x )             { return cos( double(x) ); }
inline float tan( float x )             { return tan( double(x) ); }
inline float asin( float x )            { return asin( double(x) ); }
inline float acos( float x )            { return acos( double(x) ); }
inline float atan( float x )            { return atan( double(x) ); }
inline float atan2( float y, float x )  { return atan2( double(y), double(x) );}

inline float sinh( float x )            { return sinh( double(x) ); }
inline float cosh( float x )            { return cosh( double(x) ); }
inline float tanh( float x )            { return tanh( double(x) ); }

inline float exp( float x )             { return exp( double(x) ); }
inline float log( float x )             { return log( double(x) ); }
inline float log10( float x )           { return log10( double(x) ); }
inline float modf( float x, float *ip )
    { double dp, dr = modf( double(x), &dp ); *ip = dp; return dr; }

inline float sqrt( float x )            { return sqrt( double(x) ); }
inline float pow( float x, float y )    { return pow( double(x), double(y) ); }

inline float ceil( float x )            { return ceil( double(x) ); }
inline float floor( float x )           { return floor( double(x) ); }
//#ifndef __SUNPRO_CC
inline float abs( float x )             { return fabs( double(x) ); }
//#endif
inline float fmod( float x, float y )   { return fmod( double(x), double(y) ); }

#endif  /* !DNB_HAS_FLOAT_MATHF && !DNB_HAS_FLOAT_FMATH */


#endif  /* !DNB_HAS_FLOAT_MATH */


//
//  ANSI C mathematical functions for double-precision numbers.
//
#if     !DNB_HAS_DOUBLE_ABS
inline double abs( double x ) { return fabs( x ); }
#endif


template <class T>
void
DNBSinCos( const T &angle, T &sinValue, T &cosValue )
    DNB_THROW_SPEC_NULL
{
    scl_numeric_limits<T>   traits;
    DNB_ASSERT( traits.is_specialized );

    sinValue = sin( angle );
    cosValue = cos( angle );

    if( abs(sinValue) <= traits.epsilon() )
        sinValue = T(0);
    else if( abs(cosValue) <= traits.epsilon() )
        cosValue = T(0);
}


template <class T>
inline T
DNBSignum( const T &right )
    DNB_THROW_SPEC_NULL
{
    return ( right < T(0) ? T(-1) : T(1) );
}


template <class T>
inline T
DNBSqr( const T &right )
    DNB_THROW_SPEC_NULL
{
    return ( right * right );
}

template <class T>
inline T
DNBCube( const T &right )
    DNB_THROW_SPEC_NULL
{
    return ( right * right * right );
}

template <class T>
inline T
DNBSqrt( const T &arg )
    DNB_THROW_SPEC_NULL
{
    if( arg < 1.0e-20 )
        return( 0.0 );
    return( sqrt( arg ) );
}

template <class T>
inline T
DNBAsin( const T &arg )
    DNB_THROW_SPEC_NULL
{
    if( arg < -1.0 )
        return( -DNBMathConst::Pi_2 );
    if( arg > 1.0 )
        return( DNBMathConst::Pi_2 );
    return( asin( arg ) );
}

template <class T>
inline T
DNBAcos( const T &arg )
    DNB_THROW_SPEC_NULL
{
    if( arg < -1.0 )
        return( DNBMathConst::Pi );
    if( arg > 1.0 )
        return( 0.0 );
    return( acos( arg ) );
}

template <class T>
inline T
DNBAtan( const T &arg )
    DNB_THROW_SPEC_NULL
{
    if( fabs( arg ) < 1.0e-10 )
        return( 0.0 );
    return( atan( arg ) );
}

template <class T>
inline T
DNBAtan2( const T &arg1, const T &arg2 )
    DNB_THROW_SPEC_NULL
{
    if( fabs( arg1 ) < 1.0e-10 && fabs( arg2 ) < 1.0e-10 )
        return( 0.0 );
    return( atan2( arg1, arg2 ) );
}


template <class T>
inline bool
_DNBNearRel( const T &left, const T &right, const T &tolerance )
    DNB_THROW_SPEC_NULL
{
    if ( tolerance <= T(0) )
        return ( left == right );

    T   absLeft  = abs( left );
    T   absRight = abs( right );
    T   absError = abs( left - right );

    return ( absError <= (tolerance * scl_max(absLeft, absRight)) );
}


inline bool
DNBNearRel( float left, float right, float tolerance )
    DNB_THROW_SPEC_NULL
{
    return _DNBNearRel( left, right, tolerance );
}


inline bool
DNBNearRel( double left, double right, double tolerance )
    DNB_THROW_SPEC_NULL
{
    return _DNBNearRel( left, right, tolerance );
}


template <class T>
inline bool
_DNBNearAbs( const T &left, const T &right, const T &tolerance )
    DNB_THROW_SPEC_NULL
{
    if ( tolerance <= T(0) )
        return ( left == right );

    T   absError = abs( left - right );

    return ( absError <= tolerance );
}


inline bool
DNBNearAbs( float left, float right, float tolerance )
    DNB_THROW_SPEC_NULL
{
    return _DNBNearAbs( left, right, tolerance );
}


inline bool
DNBNearAbs( double left, double right, double tolerance )
    DNB_THROW_SPEC_NULL
{
    return _DNBNearAbs( left, right, tolerance );
}


inline bool
DNBIsInf( float value )
    DNB_THROW_SPEC_NULL
{
    return DNBIsInf( double(value) );
}


inline bool
DNBIsInf( double value )
    DNB_THROW_SPEC_NULL
{
#if     defined(_WIN32)
    const int       status = _fpclass( value );
    return ( (status == _FPCLASS_PINF) || (status == _FPCLASS_NINF) );
#elif   defined(__SUNPRO_CC) || defined(__sgi)
    const fpclass_t status = fpclass( value );
    return ( (status == FP_PINF) || (status == FP_NINF) );
#elif   defined(__HP_aCC) && (__HP_aCC >= 033000)
    const int status = fpclassify( value );
    return (status == FP_INFINITE );
#elif   defined(__HP_aCC) && (__HP_aCC == 012100)
    const int status = fpclassify( value );
    return ( (status == FP_PLUS_INF) || (status == FP_MINUS_INF) );
#elif   defined(__xlC__)
    const int       status = _class( value );
    return ( (status == FP_PLUS_INF) || (status == FP_MINUS_INF) );
#else
#error  "Unsupported platform."
#endif
}


inline void
DNBSetInf( float &value )
    DNB_THROW_SPEC_NULL
{
#if   defined(__SUNPRO_CC) || defined(__sgi) || defined(_WIN32) || defined(__HP_aCC)
    DNBUnsigned32   data[ sizeof(float) / sizeof(DNBUnsigned32) ] =
        { 0x7f800000u };                // Bit-pattern of +infinity.
    value = *DNB_REINTERPRET_CAST( float *, &data );
#elif   defined(__xlC__)
    value = FLT_INFINITY;
#else
#error  "Unsupported platform."
#endif

    DNB_ASSERT( DNBIsInf(value) );
}


inline void
DNBSetInf( double &value )
    DNB_THROW_SPEC_NULL
{
#if     defined(_WIN32)
    DNBUnsigned32   data[ sizeof(double) / sizeof(DNBUnsigned32) ] =
        { 0, 0x7ff00000u };             // Bit-pattern of +infinity.
    value = *DNB_REINTERPRET_CAST( double *, &data );
#elif   defined(__SUNPRO_CC) || defined(__sgi) || defined(__HP_aCC)
    DNBUnsigned32   data[ sizeof(double) / sizeof(DNBUnsigned32) ] =
        { 0x7ff00000u, 0 };             // Bit-pattern of +infinity.
    value = *DNB_REINTERPRET_CAST( double *, &data );
#elif   defined(__xlC__)
    value = DBL_INFINITY;
#else
#error  "Unsupported platform."
#endif

    DNB_ASSERT( DNBIsInf(value) );
}


inline bool
DNBIsNaN( float value )
    DNB_THROW_SPEC_NULL
{
    return DNBIsNaN( double(value) );
}


inline bool
DNBIsNaN( double value )
    DNB_THROW_SPEC_NULL
{
#if     defined(_WIN32)
    const int       status = _fpclass( value );
    return ( (status == _FPCLASS_QNAN) || (status == _FPCLASS_SNAN) );
#elif   defined(__SUNPRO_CC) || defined(__sgi)
    const fpclass_t status = fpclass( value );
    return ( (status == FP_QNAN) || (status == FP_SNAN) );
#elif   defined(__HP_aCC) && (__HP_aCC >= 033000)
    const int status = fpclassify( value );
    return (status == FP_NAN);
#elif   defined(__HP_aCC) && (__HP_aCC == 012100)
    const int status = fpclassify( value );
    return ( (status == FP_QNAN) || (status == FP_SNAN) );
#elif   defined(__xlC__)
    const int       status = _class( value );
    return ( (status == FP_QNAN) || (status == FP_SNAN) );
#else
#error  "Unsupported platform."
#endif
}


inline void
DNBSetNaN( float &value )
    DNB_THROW_SPEC_NULL
{
#if     defined(_WIN32)
    DNBUnsigned32   data[ sizeof(float) / sizeof(DNBUnsigned32) ] =
        { 0xffc00000u };                // Bit-pattern of quiet NaN.
    value = *DNB_REINTERPRET_CAST( float *, &data );
#elif   defined(__SUNPRO_CC) || defined(__sgi) || defined(__HP_aCC)
    DNBUnsigned32   data[ sizeof(float) / sizeof(DNBUnsigned32) ] =
        { 0x7fc10000u };                // Bit-pattern of signaling NaN.
    value = *DNB_REINTERPRET_CAST( float *, &data );
#elif   defined(__xlC__)
    value = FLT_SNAN;
#else
#error  "Unsupported platform."
#endif

    DNB_ASSERT( DNBIsNaN(value) );
}


inline void
DNBSetNaN( double &value )
    DNB_THROW_SPEC_NULL
{
#if     defined(_WIN32)
    DNBUnsigned32   data[ sizeof(double) / sizeof(DNBUnsigned32) ] =
        { 0, 0xfff80000u };             // Bit-pattern of quiet NaN.
    value = *DNB_REINTERPRET_CAST( double *, &data );
#elif   defined(__SUNPRO_CC) || defined(__sgi) || defined(__HP_aCC)
    DNBUnsigned32   data[ sizeof(double) / sizeof(DNBUnsigned32) ] =
        { 0x7ff90000u, 0 };             // Bit-pattern of signaling NaN.
    value = *DNB_REINTERPRET_CAST( double *, &data );
#elif   defined(__xlC__)
    value = DBL_SNAN;
#else
#error  "Unsupported platform."
#endif

    DNB_ASSERT( DNBIsNaN(value) );
}
