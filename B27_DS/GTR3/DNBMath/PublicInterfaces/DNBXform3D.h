//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 */

//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     pjf         08/14/97    Initial implementation
//*     jad         08/07/98    Ported to MSVC 5.0.  Added the base class
//*                             DNBMath3D.
//*     jad         09/07/98    Implemented numerous enhancements to the class.
//*     mmd         01/12/98    documented the methods
//*     cwz         09/09/99    re-implementation.
//*     bkh         11/05/03    Implementation of CAA style documentation.
//*		smw			01/07/05	Removed extractors due to iostream migration to the
//*								new, standard, templated iostream, and due to migration to 64 bit
//*								builds.  Extractor for all math classes have been moved,
//*								for easy rescucitation, if necessary,
//*								to DNBMath.tst/PrivateInterfaces/DNBMathExtractors.h
//*
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNBXFORM3D_H_
#define _DNBXFORM3D_H_


#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>


#include <DNBMath.h>
#include <DNBMath3D.h>
#include <DNBException.h>
#include <DNBVector3D.h>
#include <DNBRotate3D.h>
#include <DNBMathMatrix.h>


//
//  Forward declarations.
//
template <class T>
class DNBBasicEuler3D;


template <class T>
class DNBBasicQtn3D;

template <class T>
class DNBBasicXform3D;

template <class T>
bool
operator==( const DNBBasicXform3D<T> &left, const DNBBasicXform3D<T> &right )
    DNB_THROW_SPEC_NULL;

template <class T>
bool
operator!=( const DNBBasicXform3D<T> &left, const DNBBasicXform3D<T> &right )
    DNB_THROW_SPEC_NULL;

template <class T>
DNBBasicXform3D<T>
operator*( const DNBBasicXform3D<T> &left, const DNBBasicXform3D<T> &right )
    DNB_THROW_SPEC_NULL;

template <class T>
DNBBasicXform3D<T>
operator/( const DNBBasicXform3D<T> &left, const DNBBasicXform3D<T> &right )
    DNB_THROW_SPEC((DNBESingularMatrix));

template <class T>
DNBBasicVector3D<T>
operator*( const DNBBasicXform3D<T> &left, const DNBBasicVector3D<T> &right )
    DNB_THROW_SPEC_NULL;

template <class T>
ostream &
operator<<( ostream &ostr, const DNBBasicXform3D<T> &item )
    DNB_THROW_SPEC((scl_bad_alloc));

template <class T>
bool
DNBNearRel( const DNBBasicXform3D<T> &left, const DNBBasicXform3D<T> &right,
    const T &angularTol, const T &linearTol )
    DNB_THROW_SPEC_NULL;

template <class T>
bool
DNBNearAbs( const DNBBasicXform3D<T> &left, const DNBBasicXform3D<T> &right,
    const T &angularTol, const T &linearTol )
    DNB_THROW_SPEC_NULL;




/**
  * A templated class which represents a three-dimensional homogeneous
  * transformation matrix.
  *
  * <br><B>Template Parameter(s)</B><br>
  * @param  T
  * The base type of the matrix elements.
  *
  * <br><B>Description</B><br>
  * This module defines a class which contains 4X3 values.  This
  * collection of values is intended to be used as a three dimensional
  * ortho-normal matrix (no scaling or shear).  Every instance of this
  * class must and will be ortho-normal.
  *
  * The main motivation for this class was to provide a very high
  * performance matrix manipulation class.
  *
  * A Note on the Matrix Internals:
  * The elements of the matrix are organized into 4 rows and 3 columns.
  * Typical matrices will have 4 rows and 4 column.  But since this is
  * a specialized matrix class, the fourth column will always be assumed
  * to have values of 0,0,0,1.
  *
  * The rows are identified as N, S, A, and P.  The columns are identified
  * as X, Y, and Z.  A figure of the matrix is depicted below:
  *
  * Nx  Ny  Nz  0
  * Sx  Sy  Sz  0
  * Ax  Ay  Az  0
  * Px  Py  Pz  1
  *
  * The first 3 rows represent the rotational component and
  * the last row represents the translation component.
  *
  *
  * This class also defines the standard iterators (i.e., begin, end).
  * With these standard iterators, other standard matrix manipulation
  * libraries may be used to manipulate these matrices.  In other
  * words, if there is a function not explicitly defined in this
  * class, other global functions may be used to manipulate these
  * matrices.
  *
  * Since this is a template class, the user may define the type of
  * values which may be stored in the instances of the 3D matrix class
  * object. It is recommended that some form of floating point numbers be
  * used to instantiate this class.  If other data types are used, then
  * some functions defined by this template class may return undesirable
  * results.
  *
  * <br><B>Example</B><br>
  * <pre>
  * const size_t    NumRows = 4;
  * const size_t    NumCols = 4;
  *
  * DNBBasicMathMatrix<DNBReal>   matXform1(NumRows,NumCols, 0.0);
  * matXform1(0,0) = 1.0;
  * matXform1(1,1) = 1.0;
  * matXform1(2,2) = 1.0;
  *
  * DNBReal             realArray[NumRows][NumCols] = {{1.0, 0.0, 0.0},
  * {0.0, 1.0, 0.0},
  * {0.0, 0.0, 1.0},
  * {0.0, 0.0, 0.0}};
  *
  * DNBXform3D          xform1;
  * DNBXform3D          xform2(xform1);
  * DNBXform3D          xform3(matXform1);
  * DNBXform3D          xform4(realArray);
  *
  * DNBBasicMathMatrix<DNBReal>   matXform2 = xform2.toDNBBasicMathMatrix;
  *
  * </pre>
  *
  */
template <class T>
class DNBBasicXform3D : public DNBMath3D
{
public:
/**
  * The element type.
  *
  * <br><B>Description</B><br>
  * This type definition specifies the type of object stored in the
  * transformation matrix.
  *
  *
  */
    typedef T           value_type;


/**
  * An unsigned integral type.
  *
  * <br><B>Description</B><br>
  * This type definition specifies an unsigned integral type that can
  * represent the maximum number of elements in the transformation
  * matrix.
  *
  *
  */
    typedef size_t      size_type;


/**
  * A signed integral type.
  *
  * <br><B>Description</B><br>
  * This type definition specifies a signed integral type used to
  * represent the distance between two iterators.
  *
  *
  */
    typedef ptrdiff_t   difference_type;


/**
  * Constructs the identity transformation matrix.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function constructs a transformation matrix which represents
  * the identity transform.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBXform3D      xform;
  *
  * </pre>
  *
  */
    DNBBasicXform3D( )
DNB_THROW_SPEC_NULL;


/**
  * Constructs a transformation matrix using copy semantics.
  * @param  right
  * The transformation matrix to be copied.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function copies the elements of <tt>right</tt> to <tt>self</tt>.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBXform3D      xform1;
  * DNBXform3D      xform2( xform1 );
  *
  * </pre>
  *
  */
    DNBBasicXform3D( const DNBBasicXform3D<T> &right )
        DNB_THROW_SPEC_NULL;


/**
  * Constructs an uninitialized transformation matrix.
  * @param  dummy
  * The UninitializedType object.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function creates a transformation matrix without setting
  * the object to a definitive state, thus returns quickly.
  * The existing of this constructor is purely for efficiency.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBXform3D      xform1( Uninitialized );
  * xform1.set( vecN, vecS, vecA );
  *
  * </pre>
  *
  */
    DNBBasicXform3D( UninitializedType dummy )
        DNB_THROW_SPEC_NULL;


/**
  * Creates a transform matrix from D-H parameters.
  * @param theta
  * The angle between X(i) and X(i+1) axes.
  * @param d
  * The distance of the two origins of the two ajacent links on Z(i).
  * @param alpha
  * The angle between Z(i) and Z(i+1) axes.
  * @param a
  * The distance of the two origins of the two ajacent links
  * on X(i+1).
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function creates a transform matrix from the given D-H parameters.
  *
  *
  */
    DNBBasicXform3D( const T &theta, const T &d, const T &alpha, const T &a )
        DNB_THROW_SPEC_NULL;


/**
  * Creates a transform from the given 4X4 matrix.
  * @param  valueArray
  * An array which specifies the initial values for each
  * matrix element.  The array must contain 4X4
  * elements.  The values are expected to represent an
  * ortho-normal matrix.
  * @param  transposed
  * A flag indicating if the input array already contains
  * rowwise elements. Pass a true value to indicate a rowwise
  * arrangement in valueArray. The default is false in which case
  * the resultant matrix will be constructed from the transpose of
  * of the input matrix.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function creates a transform matrix from the given 4X4 matrix,
  * and then stores it in <tt>self</tt>.
  * @exception DNBESingularMatrix
  * The given matrix <tt>right</tt> is singular.
  * @exception DNBEIllegalMatrix
  * The given matrix <tt>right</tt> is not a valid right-hand rule
  * transformation matrix.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBReal         matrix1[ 4 ][ 4 ] =
  * {   // N    S    A    P
  * { 1.0, 0.0, 0.0, 1.0 },
  * { 0.0, 1.0, 0.0, 2.0 },
  * { 0.0, 0.0, 1.0, 3.0 },
  * { 0.0, 0.0, 0.0, 1.0 }
  * };
  * DNBXform3D      xform1( (const DNBReal **) matrix1 );
  *
  * DNBReal         matrix2[ 4 ][ 4 ] =
  * {
  * { 1.0, 0.0, 0.0, 0.0 },   // N
  * { 0.0, 1.0, 0.0, 0.0 },   // S
  * { 0.0, 0.0, 1.0, 0.0 },   // A
  * { 1.0, 2.0, 3.0, 1.0 }    // P
  * };
  * DNBXform3D      xform2( (const DNBReal **) matrix2, true );
  *
  * </pre>
  *
  */
    explicit
    DNBBasicXform3D( const T (*right)[4], bool transposed = false )
        DNB_THROW_SPEC((DNBESingularMatrix, DNBEIllegalMatrix));


/**
  * Creates a transform from a 4X4 DNBBasicMathMatrix<T> matrix.
  * @param  matGenMatrix
  * A General Matrix which specifies the initial
  * values for each matrix element.  The matrix must contain
  * 4 X 4 elements.
  * @param transposed
  * A flag indicating if the input array already contains
  * rowwise elements. Pass a true value to indicate a rowwise
  * arrangement in valueArray. The default is false in which case
  * the resultant matrix will be constructed from the transpose of
  * of the input matrix.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function constructs an transform matrix from the given
  * DNBBasicMathMatix<tt>T</tt> matrix, and then stores it in <tt>self</tt>.
  * @exception DNBESingularMatrix
  * The given matrix <tt>right</tt> is singular.
  * @exception DNBEIllegalMatrix
  * The given matrix <tt>right</tt> is not a valid right-hand rule
  * transform matrix.
  *
  * <br><B>Example</B><br>
  * <pre>
  * // N    S    A    P
  * char        values[] = "4x4 [ 1.0  0.0  0.0  1.0  \
  * 0.0  1.0  0.0  2.0  \
  * 0.0  0.0  1.0  3.0  \
  * 0.0  0.0  0.0  1.0 ]";
  * DNBBasicMathMatrix<DNBReal>   matrix1( values );
  *
  * DNBXform3D          xform1( matrix1 );
  *
  * DNBBasicMathMatrix<DNBReal>   matrix2 = transpose( matrix1 );
  *
  * DNBXform3D          xform2( matrix2, true );
  *
  * </pre>
  *
  */
    explicit
    DNBBasicXform3D( const DNBBasicMathMatrix<T> &right, bool transposed = false )
        DNB_THROW_SPEC((DNBESingularMatrix, DNBEIllegalMatrix));


/**
  * Destroys <self>.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function destroys <tt>self</tt>.
  *
  *
  */
    ~DNBBasicXform3D( )
        DNB_THROW_SPEC_NULL;


/**
  * Retrieves the rotation component.
  * @param rotate
  * The rotation matrix object to receive the data.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function retrieves the rotation component of <tt>self</tt> and stores
  * it in the given argument <tt>rotate</tt>.
  *
  *
  */
    void
    getRotate( DNBBasicRotate3D<T> &rotate ) const
        DNB_THROW_SPEC_NULL;


/**
  * Retrieves <tt>self</tt>'s rotation component.
  * @param qtn
  * The quaternion object to receive the rotation.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function retrieves <tt>self</tt>'s rotation component and stores
  * it in the given argument <tt>qtn</tt>.
  *
  *
  */
    void
    getQtn( DNBBasicQtn3D<T> &qtn ) const
        DNB_THROW_SPEC_NULL;


/**
  * Gets the N, S and A vectors.
  * @param vecN
  * The vector object to receive N.
  * @param vecS
  * The vector object to receive S.
  * @param vecA
  * The vector object to receive A.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function retrieves the three vectors that represent the
  * rotation component of <tt>self</tt>.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     NVector;
  * DNBVector3D     SVector;
  * DNBVector3D     AVector;
  *
  * DNBXform3D      xform;
  * xform.getNSA( NVector, SVector, AVector );
  *
  * </pre>
  *
  */
    void
    getNSA( DNBBasicVector3D<T> &vecN,
            DNBBasicVector3D<T> &vecS,
            DNBBasicVector3D<T> &vecA ) const
        DNB_THROW_SPEC_NULL;


/**
  * Retrieves <self>'s rotation component.
  * @param vec
  * The 3-D vector to receive the axis.
  * @param angle
  * The base type object to receive the angle value.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function retrieves <tt>self</tt>'s rotation component in
  * the equivalent axis and angle format and stores
  * them in the given arguments <tt>vec</tt> and <tt>angle</tt>.
  *
  *
  */
    void
    getEAA( DNBBasicVector3D<T> &vec, T &angle ) const
        DNB_THROW_SPEC_NULL;


/**
  * get the roll, picth and yaw angles.
  * @param roll
  * The base type object to receive roll value.
  * @param  pitch
  * The base type object to receive pitch value.
  * @param  yaw
  * The base type object to receive yaw value.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function retrieves the roll, pitch and yaw
  * angles associted with the transformation matrix.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBXform3D      xform;
  * DNBReal         roll, pitch, yaw;
  * xform.getRPY( roll, pitch, yaw );
  *
  * </pre>
  *
  */
    void
    getRPY( T &roll, T &pitch, T &yaw ) const
        DNB_THROW_SPEC_NULL;


/**
  * retrieves the position vector from the transformation matrix
  * @param vecP
  * The 3-D vector to receive the position vector.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function retrieves the position vector from the transformation
  * matrix.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     PVector;
  * DNBXform3D      xform;
  * xform.getPosition( PVector );
  *
  * </pre>
  *
  */
    void
    getPosition( DNBBasicVector3D<T> &vecP ) const
        DNB_THROW_SPEC_NULL;


/**
  * get a general 4x4 matrix from a DNBBasicXform3D
  * @param  right
  * general 4x4 matrix
  * @param  transposed
  * A flag indicating if the input array already contains
  * rowwise elements. Pass a true value to indicate a rowwise
  * arrangement in valueArray. The default is false in which case
  * the resultant matrix will be constructed from the transpose of
  * of the input matrix.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function builds a general 4x4 matrix from <tt>self</tt>.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBReal         matrix[ 4 ][ 4 ] =
  * {   // N    S    A    P
  * { 1.0, 0.0, 0.0, 1.0 },
  * { 0.0, 1.0, 0.0, 2.0 },
  * { 0.0, 0.0, 1.0, 3.0 },
  * { 0.0, 0.0, 0.0, 1.0 }
  * };
  * DNBXform3D      xform( matrix );
  *
  * DNBReal         matrix1[ 4 ][ 4 ];
  * xform.get( matrix1 );
  *
  * DNBReal         matrix2[ 4 ][ 4 ];
  * xform.get( matrix2, true );
  *
  * </pre>
  *
  */
    void
    get( T (*right)[4], bool transposed = false ) const
        DNB_THROW_SPEC_NULL;

/**
  * get a general 12x1 matrix from a DNBBasicXform3D
  * @param  right
  * general 12x1 matrix
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function builds a general 12x1 matrix from <tt>self</tt>.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBXform3D      xform( matrix );
  *
  * DNBReal         matrix1[12];
  * xform.get( matrix1 ); // in the order N,S,A,P.
  *
  *
  * </pre>
  *
  */
    void
    get( T *right ) const
        DNB_THROW_SPEC_NULL;


/**
  * get a DNBBasicMathMatrix from a DNBBasicXform3D
  * @param  right
  * The general DNBBasicMathMatrix matrix
  * @param  transposed
  * A flag indicating if the input array already contains
  * rowwise elements. Pass a true value to indicate a rowwise
  * arrangement in valueArray. The default is false in which case
  * the resultant matrix will be constructed from the transpose of
  * of the input matrix.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function builds a DNBBasicMathMatrix general matrix from <tt>self</tt>.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBReal         matrix[ 4 ][ 4 ] =
  * {   // N    S    A    P
  * { 1.0, 0.0, 0.0, 1.0 },
  * { 0.0, 1.0, 0.0, 2.0 },
  * { 0.0, 0.0, 1.0, 3.0 },
  * { 0.0, 0.0, 0.0, 1.0 }
  * };
  * DNBXform3D      xform( matrix );
  *
  * DNBBasicMathMatrix<DNBReal>   matrix1( 4, 4, Uninitialized );
  * xform.get( matrix1 );
  *
  * DNBBasicMathMatrix<DNBReal>   matrix2( 4, 4, Uninitialized );
  * xform.get( matrix2, true );
  *
  * </pre>
  *
  */
    void
    get( DNBBasicMathMatrix<T> &right, bool transposed = false ) const
        DNB_THROW_SPEC_NULL;


/**
  * Assigns the given rotation matrix object to <self>'s rotation
  * component.
  * @param rotate
  * The 3X3 cosine rotation matrix object.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function sets <tt>self</tt>'s rotation component with the given
  * <tt>rotate</tt>. This function do not change <tt>self</tt>'s position.
  *
  *
  */
    void
    setRotate( const DNBBasicRotate3D<T> &rotate )
        DNB_THROW_SPEC_NULL;


/**
  * sets <tt>self</tt>'s rotation component with the given quaternion.
  * @param qtn
  * The quaternion object.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function sets <tt>self</tt>'s rotationg component with the given
  * quaternion. This function does not change <tt>self</tt>'s position.
  *
  *
  */
    void
    setQtn( const DNBBasicQtn3D<T> &qtn )
        DNB_THROW_SPEC_NULL;


/**
  * Sets the N, S and A vectors.
  * @param vecN
  * The N vector.
  * @param vecS
  * The S vector.
  * @param vecA
  * The A vector.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function creates a 3X3 rotation matrix from the three given
  * vectors, and assigns it to <tt>self</tt>'s rotation component. This function
  * does not change <tt>self</tt>'s position.
  * @exception DNBSingularMatrix
  * The given vectors form a singular matrix.
  * @exception DNBEIllegalMatrix
  * The given vectors could not form a valid right-hand rule
  * rotation matrix.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     NVector( 1.0, 0.0, 0.0 );
  * DNBVector3D     SVector( 0.0, 1.0, 0.0 );
  * DNBVector3D     AVector( 0.0, 0.0, 1.0 );
  *
  * DNBXform3D      xform( DNBMath3D::Uninitialized );
  * xform.setNSA( NVector, SVector, AVector );
  *
  * </pre>
  *
  */
    void
    setNSA( const DNBBasicVector3D<T> &vecN,
            const DNBBasicVector3D<T> &vecS,
            const DNBBasicVector3D<T> &vecA )
        DNB_THROW_SPEC((DNBESingularMatrix, DNBEIllegalMatrix));



/**
  * Changes <self>'s rotation component based on the given axis and angle.
  * @param vec
  * The axis to rotate about.
  * @param  angle
  * The angle to rotate.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function creates a 3X3 rotation matrix from the given
  * axis and angle, and then assigns it to <tt>self</tt>'s rotation component.
  * This function does not change <tt>self</tt>'s position.
  * @exception DNBEZeroVector
  * The given vector is zero length.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     axis( 1.0, 0.0, 0.0 );
  * DNBXform3D      xform;
  * xform.setEAA( axis, M_PI / 2.0 );
  *
  * </pre>
  *
  */
    void
    setEAA( const DNBBasicVector3D<T> &vec, const T &angle )
        DNB_THROW_SPEC((DNBEZeroVector));


/**
  * Sets <self>'s rotation component to a rotation that is equivalent
  * to rotate a certain angle about a given coordinate axis.
  * @param axis
  * The AxisType object indicating the rotation axis.
  * @param angle
  * The angle to rotate.
  *
  * RETURN:
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function sets <tt>self</tt>'s rotational component to a rotation
  * that is equivalent to rotate a certain angle about a given
  * coordinate axis. This function does not change <tt>self</tt>'s position.
  *
  *
  */
    void
    setCAA( AxisType axis, const T &angle )
        DNB_THROW_SPEC_NULL;


/**
  * sets <tt>self</tt>'s rotation component with the given euler.
  * @param euler
  * The Euler object.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function constructs a rotation from the given euler and
  * assigns it to <tt>self</tt>'s rotation component. This function does
  * not change <tt>self</tt>'s position.
  *
  *
  */
    void
    setEuler( const DNBBasicEuler3D<T> &euler )
        DNB_THROW_SPEC_NULL;


/**
  * set the roll, picth, yax angles
  * @param  roll
  * Roll angle.
  * @param  pitch
  * Yaw angle.
  * @param  yaw
  * Pitch angle.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function creates a 3X3 rotation matrix from the given roll,
  * pitch and yaw values, and then assigns it to <tt>self</tt>'s rotation
  * component. This function does not change <tt>self</tt>'s position.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBXform3D      xform;
  * xform.setRPY( 0.15, -0.20, -0.45 );
  *
  * </pre>
  *
  */
    void
    setRPY( const T &roll, const T &pitch, const T &yaw )
        DNB_THROW_SPEC_NULL;


/**
  * Changes <self>'s position.
  * @param vecP
  * The translational vector.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function changes <tt>self</tt>'s position, but does not change
  * <tt>self</tt>'s rotation.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     PVector( 1.0, 2.0, 3.0 );
  * DNBXform3D      xform;
  * xform.setPosition( PVector );
  *
  * </pre>
  *
  */
    void
    setPosition( const DNBBasicVector3D<T> &vecP )
        DNB_THROW_SPEC_NULL;


/**
  * Creates a transform matrix from D-H parameters and assigns
  * it to <self>.
  * @param theta
  * The angle between X(i) and X(i+1) axes of the two ajacent links.
  * @param d
  * The distance of the two origins of the two ajacent links on Z(i).
  * @param alpha
  * The angle between Z(i) and Z(i+1) axes of the two ajacent links.
  * @param a
  * The distance of the two origins of the two ajacent links
  * on X(i+1).
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function creates a 4X4 transform matrix from the given D-H
  * parameters, and then assigns it to <tt>self</tt>.
  *
  *
  */
    void
    setDH( const T &theta, const T &d, const T &alpha, const T &a )
        DNB_THROW_SPEC_NULL;


/**
  * Builds a transform matrix from a 4x4 matrix.
  * @param right
  * The general 4x4 matrix.
  * @param transposed
  * A flag indicating if the input array already contains
  * rowwise elements. Pass a true value to indicate a rowwise
  * arrangement in valueArray. The default is false in which case
  * the resultant matrix will be constructed from the transpose of
  * of the input matrix.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function builds a transform matrix from a general 4x4 matrix,
  * and the assigns it to <tt>self</tt>.
  * @exception DNBESinglularMatrix
  * <tt>right</tt> is a singular matrix.
  * @exception DNBEIllegalMatrix
  * <tt>right</tt> is not a valid right-hand rule transform matrix.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBReal         matrix1[ 4 ][ 4 ] =
  * {   // N    S    A    P
  * { 1.0, 0.0, 0.0, 1.0 },
  * { 0.0, 1.0, 0.0, 2.0 },
  * { 0.0, 0.0, 1.0, 3.0 },
  * { 0.0, 0.0, 0.0, 1.0 }
  * };
  * DNBXform3D      xform1;
  * xform1.set( (const DNBReal **) matrix1 );
  *
  * DNBReal         matrix2[ 4 ][ 4 ] =
  * {
  * { 1.0, 0.0, 0.0, 0.0 },   // N
  * { 0.0, 1.0, 0.0, 0.0 },   // S
  * { 0.0, 0.0, 1.0, 0.0 },   // A
  * { 1.0, 2.0, 3.0, 1.0 }    // P
  * };
  * DNBXform3D      xform2;
  * xform2.set( (const DNBReal **) matrix2, true );
  *
  * </pre>
  *
  */
    void
    set( const T (*right)[4], bool transposed = false )
        DNB_THROW_SPEC((DNBESingularMatrix, DNBEIllegalMatrix));

/**
  * Builds a transform matrix from a 12x1 matrix.
  * @param right
  * The general 12x1 matrix.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function builds a transform matrix from a general 12x1 matrix,
  * containing elements in the order N,S,A,P and the assigns it to
  * <tt>self</tt>.
  * @exception DNBESinglularMatrix
  * <tt>right</tt> is a singular matrix.
  * @exception DNBEIllegalMatrix
  * <tt>right</tt> is not a valid right-hand rule transform matrix.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBReal         matrix1[ 12 ] =
  * {
  * 1.0, 0.0, 0.0,     // N
  * 0.0, 1.0, 0.0,     // S
  * 0.0, 0.0, 1.0,,    // A
  * 10.0, 20.0, 30.0   // P
  * };
  * DNBXform3D      xform1;
  * xform1.set( matrix1 );
  *
  *
  *
  * </pre>
  *
  */
    void
    set( const T* right )
        DNB_THROW_SPEC((DNBESingularMatrix, DNBEIllegalMatrix));

/**
  * Sets the transform matrix from a DNBBasicMathMatrix general matrix.
  * @param  right
  * the DNBBasicMathMatrix general matrix
  * @param  transposed
  * A flag indicating if the input array already contains
  * rowwise elements. Pass a true value to indicate a rowwise
  * arrangement in valueArray. The default is false in which case
  * the resultant matrix will be constructed from the transpose of
  * of the input matrix.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function builds a transform matrix from the DNBBasicMathMatrix
  * generic matrix, and then assigns it to <tt>self</tt>.
  * @exception DNBESinglularMatrix
  * <tt>right</tt> is a singular matrix.
  * @exception DNBEIllegalMatrix
  * <tt>right</tt> is not a valid right-hand rule transform matrix.
  *
  * <br><B>Example</B><br>
  * <pre>
  * // N    S    A    P
  * char        values[] = "4x4 [ 1.0  0.0  0.0  1.0  \
  * 0.0  1.0  0.0  2.0  \
  * 0.0  0.0  1.0  3.0  \
  * 0.0  0.0  0.0  1.0 ]";
  * DNBBasicMathMatrix<DNBReal>   matrix1( values );
  *
  * DNBXform3D          xform1;
  * xform1.set( matrix1 );
  *
  * DNBBasicMathMatrix<DNBReal>   matrix2 = transpose( matrix1 );
  *
  * DNBXform3D          xform2;
  * xform2.set( matrix2, true );
  *
  * </pre>
  *
  */
    void
    set( const DNBBasicMathMatrix<T> &right, bool transposed = false )
        DNB_THROW_SPEC((DNBESingularMatrix, DNBEIllegalMatrix));


/**
  * Exchanges <self> with another transformation matrix.
  * @param  other
  * The transformation matrix whose elements are exchanged with
  * <tt>self</tt>.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function swaps each element of <tt>self</tt> with the corresponding
  * element of <tt>other</tt>.
  *
  *
  */
    void
    swap( DNBBasicXform3D<T> &other )
        DNB_THROW_SPEC_NULL;


/**
  * Normalizes the rotation matrix of <tt>self</tt>.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function converts the rotation matrix of <tt>self</tt> into an
  * orthonormal right-hand rule matrix. It is typically called
  * after a transformation matrix has undergone several
  * rotations/translations.
  * @exception DNBESingularMatrix
  * The rotation matrix of <tt>self</tt> is singular.
  * @exception DNBEIllegalMatrix
  * The rotation matrix of <tt>self</tt> is not a valid right-hand rule
  * transform matrix.
  *
  *
  */
    void
    normalize( )
        DNB_THROW_SPEC((DNBESingularMatrix, DNBEIllegalMatrix));


/**
  * Translates <tt>self</tt> by the given vector.
  * @param  deltaP
  * The translation vector.
  * @param  refFrame
  * The reference frame for the translation.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function will translate <tt>self</tt> by <tt>deltaP</tt>.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBXform3D      xform;
  * DNBVector3D     deltaP( 1.0, 2.0, 3.0 );
  * xform.translate( deltaP );
  *
  * </pre>
  *
  */
    void
    translate( const DNBBasicVector3D<T> &deltaP,
               ReferenceFrame refFrame = Local )
        DNB_THROW_SPEC_NULL;


/**
  * Rotates <self> about a principle axis.
  * @param  axis
  * X, Y, or Z axis specification
  * @param  angle
  * Angle of rotation in radians.
  * @param  refFrame
  * The reference frame for the rotation.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function will rotate <tt>self</tt> about the scl_vector<tt>axis</tt>
  * by the angle <tt>angle</tt>.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBXform3D      xform;
  * xform.rotate( DNBMath3D::XAxis, M_PI / 2.0 );
  *
  * </pre>
  *
  */
    void
    rotate( AxisType axis, const T &angle, ReferenceFrame refFrame = Local )
        DNB_THROW_SPEC_NULL;


/**
  * Rotates <self> about the given axis.
  * @param  angle
  * Angle of rotation in radians.
  * @param  axis
  * Normalized vector which represents the axis rotation.
  * @param  refFrame
  * The reference frame for the rotation.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function will rotate <tt>self</tt> about the scl_vector<tt>axis</tt>
  * by the angle <tt>angle</tt>.
  * @exception DNBEZeroVector
  * The length of the given vector is zero.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     axis( 1.0, 0.0, 0.0 );
  * DNBXform3D      xform;
  * xform.rotate( axis, M_PI / 2.0 );
  *
  * </pre>
  *
  */
    void
    rotate( const DNBBasicVector3D<T> &vec, const T &angle,
        ReferenceFrame refFrame = Local )
        DNB_THROW_SPEC((DNBEZeroVector));


/**
  * Moves <self> by the given transformation matrix.
  * @param  xform
  * The transformation matrix.
  * @param  refFrame
  * The reference frame for the motion.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function moves <tt>self</tt> by <tt>xform</tt>.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBXform3D      xform;
  * DNBXform3D      delta( DNBMath3D::RotateX, M_PI / 2.0 );
  * xform.move( delta );
  *
  * </pre>
  *
  */
    void
    move( const DNBBasicXform3D<T> &xform, ReferenceFrame refFrame = Local )
        DNB_THROW_SPEC_NULL;


/**
  * Post-multiplies a given vector.
  * @param vec
  * The 3-D vector to be rotated.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function transforms a given vector and stores the result
  * in the given vector.
  *
  *
  */
    void
    rotateVec( DNBBasicVector3D<T> &vec ) const
        DNB_THROW_SPEC_NULL;


/**
  * Pre-multiplies <tt>self</tt> by a transformation matrix.
  * @param  left
  * The transformation matrix to be multiplied.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function pre-multiplies <tt>self</tt> by the transformation matrix
  * <tt>left</tt> and stores the result in <tt>self</tt>.
  *
  *
  */
    void
    preMultiply( const DNBBasicXform3D<T> &left )
        DNB_THROW_SPEC_NULL;


/**
  * Post-multiplies <tt>self</tt> by a transformation matrix.
  * @param  right
  * The transformation matrix to be multiplied.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function post-multiplies <tt>self</tt> by the transformation matrix
  * <tt>right</tt> and stores the result in <tt>self</tt>.
  *
  *
  */
    void
    postMultiply( const DNBBasicXform3D<T> &right )
        DNB_THROW_SPEC_NULL;


/**
  * Inverts <tt>self</tt>.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function will invert <tt>self</tt>.
  * @exception DNBESingularMatrix
  * <tt>self</tt> is singular and cannot be inverted.
  *
  *
  */
    void
    invert( )
        DNB_THROW_SPEC((DNBESingularMatrix));


/**
  * Retrieves a direction vector of <tt>self</tt> by value.
  * @param  dirIndex
  * The index of the desired direction vector (e.g., DNBMath3D::N).
  *
  * @return
  * A copy of the direction vector of <tt>self</tt> specified by <tt>dirIndex</tt>.
  *
  * <br><B>Description</B><br>
  * This function returns a copy of the direction vector of <tt>self</tt>
  * specified by <tt>dirIndex</tt>.
  *
  *
  *
  */
    DNBBasicVector3D<T>
    operator()( DirectionIndex dirIndex ) const
        DNB_THROW_SPEC_NULL;


/**
  * Retrieves the position vector of <tt>self</tt> by value.
  * @param  posIndex
  * The index of the position vector (i.e., DNBMath3D::P).
  *
  * @return
  * A copy of the position vector of <tt>self</tt>.
  *
  * <br><B>Description</B><br>
  * This function returns a copy of the position vector of <tt>self</tt>.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBXform3D      xform;
  * DNBVector3D     PVector = xform( DNBMath3D::P );
  *
  * </pre>
  *
  */
    DNBBasicVector3D<T>
    operator()( PositionIndex posIndex ) const
        DNB_THROW_SPEC_NULL;


/**
  * Retrieves a directional coordinate of <self> by value.
  * @param  dirIndex
  * The index of the desired direction vector (e.g., DNBMath3D::N).
  * @param  coordIndex
  * The index of the desired coordinate (e.g., DNBMath3D::X).
  *
  * @return
  * A copy of the directional coordinate of <tt>self</tt> specified by
  * <tt>dirIndex</tt> and <tt>coordIndex</tt>.
  *
  * <br><B>Description</B><br>
  * This function returns a copy of the directional coordinate of <tt>self</tt>
  * specified by <tt>dirIndex</tt> and <tt>coordIndex</tt>.
  *
  *
  */
    T
    operator()( DirectionIndex dirIndex, CoordinateIndex coordIndex ) const
        DNB_THROW_SPEC_NULL;


/**
  * Retrieves a positional coordinate of <tt>self</tt> by value.
  * @param  posIndex
  * The index of the position vector (i.e., DNBMath3D::P).
  * @param  coordIndex
  * The index of the desired coordinate (e.g., DNBMath3D::X).
  *
  * @return
  * A copy of the positional coordinate of <tt>self</tt> specified by
  * <tt>posIndex</tt> and <tt>coordIndex</tt>.
  *
  * <br><B>Description</B><br>
  * This function returns a copy of the positional coordinate of <tt>self</tt>
  * specified by <tt>posIndex</tt> and <tt>coordIndex</tt>.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBXform3D      xform;
  * DNBReal         PX = xform( DNBMath3D::P, DNBMath3D::X );
  *
  * </pre>
  *
  */
    T
    operator()( PositionIndex posIndex, CoordinateIndex coordIndex ) const
        DNB_THROW_SPEC_NULL;


/**
  * Copies a transformation matrix to <self>.
  * @param  right
  * The transformation matrix to be copied.
  *
  * @return
  * A reference of <tt>self</tt>.
  *
  * <br><B>Description</B><br>
  * This function copies each element of <tt>right</tt> to the corresponding
  * element of <tt>self</tt>.
  *
  *
  */
    DNBBasicXform3D<T> &
    operator=( const DNBBasicXform3D<T> &right )
        DNB_THROW_SPEC_NULL;


/**
  * Gets a copy of <tt>self</tt> in DNBBasicMathMatrix format.
  *
  * @return
  * An instance of the class DNBBasicMathMatrix<tt>T</tt> which is initialized from
  * <tt>self</tt>.
  *
  * <br><B>Description</B><br>
  * This function returns an instance of the class DNBBasicMathMatrix<tt>T</tt> which
  * contains the elements of <tt>self</tt>.  The resulting matrix is organized
  * as follows:
  * \begin{verbatim}
  * Nx  Sx  Ax  Px
  * Ny  Sy  Ay  Py
  * Nz  Sz  Az  Pz
  * 0   0   0   1
  * \end{verbatim}
  * @exception scl_bad_alloc
  * The DNBBasicMathMatrix instance could not be allocated because of
  * insufficient free memory.
  *
  *
  *
  */
    DNBBasicMathMatrix<T>
    toDNBBasicMathMatrix() const
        DNB_THROW_SPEC((scl_bad_alloc));


/**
  * Post-multiplies <tt>self</tt> by a transformation matrix.
  * @param  right
  * The transformation matrix to be multiplied.
  *
  * @return
  * A reference of <tt>self</tt>.
  *
  * <br><B>Description</B><br>
  * This function post-multiplies <tt>self</tt> by the transformation matrix
  * <tt>right</tt> and stores the result in <tt>self</tt>.
  *
  *
  */
    DNBBasicXform3D<T> &
    operator*=( const DNBBasicXform3D<T> &right )
        DNB_THROW_SPEC_NULL;


/**
  * Post-multiplies <tt>self</tt> by the inverse of a transformation matrix.
  * @param  right
  * The transformation matrix to be inverted and multiplied.
  *
  * @return
  * A reference to <tt>self</tt>.
  *
  * <br><B>Description</B><br>
  * This function post-multiplies <tt>self</tt> by the inverse of the
  * transformation matrix <tt>right</tt> and stores the result in <tt>self</tt>.
  * @exception DNBESingularMatrix
  * The transformation matrix <tt>right</tt> is singular.
  *
  *
  */
    DNBBasicXform3D<T> &
    operator/=( const DNBBasicXform3D<T> &right )
        DNB_THROW_SPEC((DNBESingularMatrix));


#if DNB_HAS_FRIEND_TEMPLATE

/**
  * Determines if <tt>left</tt> and <tt>right</tt> are equal.
  * @param  left
  * The transformation matrix used in the comparison.
  * @param  right
  * The transformation matrix used in the comparison.
  *
  * @return
  * true if <tt>left</tt> and <tt>right</tt> are equivalent, and false otherwise.
  *
  * <br><B>Description</B><br>
  * This function returns true if <tt>left</tt> and <tt>right</tt> are equal, and
  * false otherwise.
  *
  *
  */
    friend ExportedByDNBMath  bool
    operator==( const DNBBasicXform3D<T> &left, const DNBBasicXform3D<T> &right )
        DNB_THROW_SPEC_NULL;

    friend ExportedByDNBMath  bool
    operator!=( const DNBBasicXform3D<T> &left, const DNBBasicXform3D<T> &right )
        DNB_THROW_SPEC_NULL;



/**
  * Multiplies a pair of transformation matrices.
  * @param  left
  * The transformation matrix that represents the left operand.
  * @param  right
  * The transformation matrix that represents the right operand.
  *
  * @return
  * The product of <tt>left</tt> and <tt>right</tt>.
  *
  * <br><B>Description</B><br>
  * This function post-multiplies <tt>left</tt> by <tt>right</tt> and returns the
  * result.
  *
  *
  */
    friend ExportedByDNBMath  DNBBasicXform3D<T>
    operator*( const DNBBasicXform3D<T> &left, const DNBBasicXform3D<T> &right )
        DNB_THROW_SPEC_NULL;


/**
  * Multiplies a transformation matrix by the inverse of another
  * transformation matrix.
  * @param  left
  * The transformation matrix that represents the left operand.
  * @param  right
  * The transformation matrix that represents the right operand.
  *
  * @return
  * The transform matrix containing the result.
  *
  * <br><B>Description</B><br>
  * This function post-multiplies <tt>left</tt> by the inverse of <tt>right</tt> and
  * returns the result.
  * @exception DNBESingularMatrix
  * The transformation matrix <tt>right</tt> is singular.
  *
  *
  */
    friend ExportedByDNBMath  DNBBasicXform3D<T>
    operator/( const DNBBasicXform3D<T> &left, const DNBBasicXform3D<T> &right )
        DNB_THROW_SPEC((DNBESingularMatrix));


/**
  * Multiplies a transformation matrix by a vector.
  * @param  left
  * The transformation matrix that represents the left operand.
  * @param  right
  * The point vector that represents the right operand.
  *
  * @return
  * The 3-D vector containing the rotated vector.
  *
  * <br><B>Description</B><br>
  * This function post-multiplies <tt>left</tt> by <tt>right</tt> and returns the
  * result.
  *
  *
  */
    friend ExportedByDNBMath  DNBBasicVector3D<T>
    operator*( const DNBBasicXform3D<T> &left, const DNBBasicVector3D<T> &right )
        DNB_THROW_SPEC_NULL;


/**
  * Writes a transformation matrix to a stream.
  * @param  ostr
  * The output stream that is to receive the data item.
  * @param  item
  * The transformation matrix to be inserted.
  *
  * @return
  * A reference to <tt>ostr</tt>.
  *
  * <br><B>Description</B><br>
  * This function writes the transformation matrix <tt>item</tt> to the output
  * stream <tt>ostr</tt>, using the formatting conventions of the class
  * DNBBasicMathMatrix<tt>T</tt>.
  * @exception scl_bad_alloc
  * An internal buffer could not be allocated because of
  * insufficient free memory.
  *
  *
  */
    friend ExportedByDNBMath  ostream &
    operator<<( ostream &ostr, const DNBBasicXform3D<T> &item )
        DNB_THROW_SPEC((scl_bad_alloc));



/**
  * Determines if a pair of transformation matrices are "relatively"
  * close to one another.
  * @param  left
  * The transformation matrix that represents the left operand.
  * @param  right
  * The transformation matrix that represents the right operand.
  * @param  angularTol
  * The relative tolerance used in comparing the angles of rotation
  * between <tt>left</tt> and <tt>right</tt>.
  * @param  linearTol
  * The relative tolerance used in comparing the axes of rotation/
  * translation between <tt>left</tt> and <tt>right</tt>.
  *
  * @return
  * The bool value true if <tt>left</tt> and <tt>right</tt> are "relatively" close
  * to one another, and false otherwise.
  *
  * <br><B>Description</B><br>
  * This function returns true if <tt>left</tt> and <tt>right</tt> are "relatively"
  * close to one another, and false otherwise.  If <tt>angularTol</tt> or
  * <tt>linearTol</tt> is less than zero, the function determines if <tt>left</tt> is
  * precisely equal to <tt>right</tt>.
  *
  *
  */
    friend ExportedByDNBMath  bool
    DNBNearRel( const DNBBasicXform3D<T> &left, const DNBBasicXform3D<T> &right,
                const T &angularTol, const T &linearTol )
        DNB_THROW_SPEC_NULL;


/**
  * Determines if a pair of transformation matrices are "absolutely"
  * close to one another.
  * @param  left
  * The transformation matrix that represents the left operand.
  * @param  right
  * The transformation matrix that represents the right operand.
  * @param  angularTol
  * The absolute tolerance used in comparing the angles of rotation
  * between <tt>left</tt> and <tt>right</tt>.
  * @param  linearTol
  * The absolute tolerance used in comparing the axes of rotation/
  * translation between <tt>left</tt> and <tt>right</tt>.
  *
  * @return
  * The bool value true if <tt>left</tt> and <tt>right</tt> are "absolutely" close
  * to one another, and false otherwise.
  *
  * <br><B>Description</B><br>
  * This function returns true if <tt>left</tt> and <tt>right</tt> are "absolutely"
  * close to one another, and false otherwise.  If <tt>angularTol</tt> or
  * <tt>linearTol</tt> is less than zero, the function determines if <tt>left</tt> is
  * precisely equal to <tt>right</tt>.
  *
  *
  */
    friend ExportedByDNBMath  bool
    DNBNearAbs( const DNBBasicXform3D<T> &left, const DNBBasicXform3D<T> &right,
                const T &angularTol, const T &linearTol )
        DNB_THROW_SPEC_NULL;

#endif  /* DNB_HAS_FRIEND_TEMPLATE */

    static void
    EAAInterp( DNBBasicXform3D<T> &res, const T &alpha,
               const DNBBasicXform3D<T> &from, const DNBBasicXform3D<T> &to )
        DNB_THROW_SPEC_NULL;


    static void
    slerp( DNBBasicXform3D<T> &res, const T &alpha,
           const DNBBasicXform3D<T> &from, const DNBBasicXform3D<T> &to )
        DNB_THROW_SPEC_NULL;

/**
  * The identity transformation matrix.
  *
  * <br><B>Description</B><br>
  * This object defines a transformation matrix consisting of the
  * following elements:
  *
  * 1  0  0  0
  * 0  1  0  0
  * 0  0  1  0
  * 0  0  0  1
  *
  *
  *
  */
    static const DNBBasicXform3D<T>    Identity;

#if DNB_HAS_FRIEND_TEMPLATE
private:
#else
public:
#endif
    static void
    multiply( DNBBasicXform3D<T> &result,
	      const DNBBasicXform3D<T> &left,
	      const DNBBasicXform3D<T> &right )
        DNB_THROW_SPEC_NULL;

    static void
    multiply( DNBBasicVector3D<T> &result,
	      const DNBBasicXform3D<T> &left,
	      const DNBBasicVector3D<T> &right )
        DNB_THROW_SPEC_NULL;

    static void
    divide( DNBBasicXform3D<T> &result,
	    const DNBBasicXform3D<T> &left,
	    const DNBBasicXform3D<T> &right )
        DNB_THROW_SPEC((DNBESingularMatrix));

    DNBBasicRotate3D<T>  rot_;
    DNBBasicVector3D<T>  pos_;
};




/**
  * A transformation matrix of type DNBReal.
  *
  * <br><B>Description</B><br>
  * This type definition represents the default transformation matrix class
  * used in the Xenon toolkit.
  *
  *
  */
typedef DNBBasicXform3D<DNBReal>    DNBXform3D;


//
//  Include the public definition file.
//
#if     defined(DNB_COMPILE_INSTANTIATE) || defined(_DNBXFORM3D_CC_)
#include "DNBXform3D.cc"
#elif   defined(_WINDOWS_SOURCE)
/*
extern DNB_DEFINE_CLASS_INST ExportedByDNBMath
DNBBasicXform3D<float>; */

extern DNB_DEFINE_CLASS_INST ExportedByDNBMath
DNBBasicXform3D<double>;
#endif


#endif  /* _DNBXFORM3D_H_ */
