//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview czo/BPL 01:11:13
 */


//*
//* FILE:
//*     SimTime.cc   - public definition file
//*
//* MODULE:
//*     SimTime      - public implementation file
//*
//* OVERVIEW:
//*     This module provides implementations for the DNBSimTime (handle)
//*	inline functions.
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     srv         02/19/99    Document original source
//*     srv         02/25/99    Fix intermediate value truncation problem in
//*                             operator*(DNBSimTime,DNBSimTime)
//*	sdh	    03/09/99	Productionize the code and add error free
//*				division algorithm
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1999 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*


inline DNBReal
DNBSimTime::seconds() const
    DNB_THROW_SPEC_NULL
{
    return DNBReal( sec_ ) + DNBReal( nsec_ ) / DNBReal( DNBSimTime::nsPerSec );
}

inline DNBSimTime &
DNBSimTime::operator=( const DNBSimTime &rhs )
    DNB_THROW_SPEC_NULL
{
    if( this != &rhs )
    {
	sec_ = rhs.sec_;
	nsec_ = rhs.nsec_;
    }
    return *this;
}

inline DNBSimTime &
DNBSimTime::operator+=( const DNBSimTime &rhs )
    DNB_THROW_SPEC_NULL
{
    sec_ += rhs.sec_;
    nsec_ += rhs.nsec_;

    normalize_();

    return *this;
}

inline DNBSimTime &
DNBSimTime::operator-=( const DNBSimTime &rhs )
    DNB_THROW_SPEC_NULL
{
    sec_ -= rhs.sec_;
    nsec_ -= rhs.nsec_;

    normalize_();

    return *this;
}


inline DNBSimTime &
DNBSimTime::operator++()
    DNB_THROW_SPEC_NULL
{
    sec_++;
    return *this;
}

inline DNBSimTime
DNBSimTime::operator++( int )
    DNB_THROW_SPEC_NULL
{
    DNBSimTime tmp( *this );
    operator++();
    return tmp;
}

inline DNBSimTime &
DNBSimTime::operator--()
    DNB_THROW_SPEC_NULL
{
    sec_--;
    return *this;
}

inline DNBSimTime
DNBSimTime::operator--( int )
    DNB_THROW_SPEC_NULL
{
    DNBSimTime tmp( *this );
    operator--();
    return tmp;
}

inline DNBSimTime
DNBSimTime::operator+()
    DNB_THROW_SPEC_NULL
{
    return *this;
}

inline DNBSimTime
DNBSimTime::operator-()
    DNB_THROW_SPEC_NULL
{
    sec_ = -sec_;
    nsec_ = -nsec_;
    return *this;
}


inline DNBSimTime
operator+( const DNBSimTime &lhs, const DNBSimTime &rhs )
    DNB_THROW_SPEC_NULL
{
    DNBSimTime tmp( lhs );
    tmp += rhs;
    return tmp;
}

inline DNBSimTime
operator-( const DNBSimTime &lhs, const DNBSimTime &rhs )
    DNB_THROW_SPEC_NULL
{
    DNBSimTime tmp( lhs );
    tmp -= rhs;
    return tmp;
}

inline DNBSimTime
operator*( const DNBSimTime &lhs, const DNBSimTime &rhs )
    DNB_THROW_SPEC_NULL
{
    DNBSimTime tmp( lhs );
    tmp *= rhs;
    return tmp;
}

inline DNBSimTime
operator/( const DNBSimTime &lhs, const DNBSimTime &rhs )
    DNB_THROW_SPEC((DNBEZeroDivide, DNBEOverflowError))
{
    DNBSimTime tmp( lhs );
    tmp /= rhs;
    return tmp;
}

inline bool
operator==( const DNBSimTime &lhs, const DNBSimTime &rhs )
    DNB_THROW_SPEC_NULL
{
    return lhs.sec_ == rhs.sec_ && lhs.nsec_ == rhs.nsec_;
}

inline bool
operator!=( const DNBSimTime &lhs, const DNBSimTime &rhs )
    DNB_THROW_SPEC_NULL
{
    return !( lhs == rhs );
}

inline bool
operator<( const DNBSimTime &lhs, const DNBSimTime &rhs )
    DNB_THROW_SPEC_NULL
{
    return lhs.sec_ < rhs.sec_
           || ( lhs.sec_ == rhs.sec_ && lhs.nsec_ < rhs.nsec_ );
}

inline bool
operator>( const DNBSimTime &lhs, const DNBSimTime &rhs )
    DNB_THROW_SPEC_NULL
{
    return rhs < lhs;
}

inline bool
operator<=( const DNBSimTime &lhs, const DNBSimTime &rhs )
    DNB_THROW_SPEC_NULL
{
    return !( rhs < lhs );
}

inline bool
operator>=( const DNBSimTime &lhs, const DNBSimTime &rhs )
    DNB_THROW_SPEC_NULL
{
    return !( lhs < rhs );
}

inline DNBSimTime
acos( const DNBSimTime &x )
    DNB_THROW_SPEC_NULL
{
    return acos( x.seconds() );
}

inline DNBSimTime
asin( const DNBSimTime &x )
    DNB_THROW_SPEC_NULL
{
    return asin( x.seconds() );
}

inline DNBSimTime
atan( const DNBSimTime &x )
    DNB_THROW_SPEC_NULL
{
    return atan( x.seconds() );
}

inline DNBSimTime
atan2( const DNBSimTime &y, const DNBSimTime &x )
    DNB_THROW_SPEC_NULL
{
    return atan2( y.seconds(), x.seconds() );
}

inline DNBSimTime
cos( const DNBSimTime &x )
    DNB_THROW_SPEC_NULL
{
    return cos( x.seconds() );
}

inline DNBSimTime
sin( const DNBSimTime &x )
    DNB_THROW_SPEC_NULL
{
    return sin( x.seconds() );
}

inline DNBSimTime
tan( const DNBSimTime &x )
    DNB_THROW_SPEC_NULL
{
    return tan( x.seconds() );
}

inline DNBSimTime
cosh( const DNBSimTime &x )
    DNB_THROW_SPEC_NULL
{
    return cosh( x.seconds() );
}

inline DNBSimTime
sinh( const DNBSimTime &x )
    DNB_THROW_SPEC_NULL
{
    return sinh( x.seconds() );
}

inline DNBSimTime
tanh( const DNBSimTime &x )
    DNB_THROW_SPEC_NULL
{
    return tanh( x.seconds() );
}

inline DNBSimTime
exp( const DNBSimTime &x )
    DNB_THROW_SPEC_NULL
{
    return exp( x.seconds() );
}

inline DNBSimTime
log( const DNBSimTime &x )
    DNB_THROW_SPEC_NULL
{
    return log( x.seconds() );
}

inline DNBSimTime
log10( const DNBSimTime &x )
    DNB_THROW_SPEC_NULL
{
    return log10( x.seconds() );
}

inline DNBSimTime
modf( const DNBSimTime &x, DNBSimTime *ip )
    DNB_THROW_SPEC_NULL
{
    DNBSimTime TimeReturn;

    TimeReturn.nsec_ = x.nsec_;
    ip->nsec_ = 0;
    ip->sec_ = x.sec_;

    return TimeReturn;
}

inline DNBSimTime
pow( const DNBSimTime &x, const DNBSimTime &y )
    DNB_THROW_SPEC_NULL
{
    return pow( x.seconds(), y.seconds() );
}

inline DNBSimTime
sqrt( const DNBSimTime &x )
    DNB_THROW_SPEC_NULL
{
    return sqrt( x.seconds() );
}

inline DNBSimTime
abs( const DNBSimTime &x )
    DNB_THROW_SPEC_NULL
{
    DNBSimTime tmp( x );
    if( tmp.sec_ < 0 )
	tmp.sec_ = -tmp.sec_;
    tmp.nsec_ = ::abs( tmp.nsec_ );
    return tmp;
}

inline DNBSimTime
ceil( const DNBSimTime &x )
    DNB_THROW_SPEC_NULL
{
    DNBSimTime tmp( x );
    if( tmp.nsec_ == 0 )
	return tmp;

    if( tmp.sec_ >= 0 )
	tmp.sec_ ++;
    tmp.nsec_ = 0;
    return tmp;
}

inline DNBSimTime
floor( const DNBSimTime &x )
    DNB_THROW_SPEC_NULL
{
    DNBSimTime tmp( x );
    if( tmp.nsec_ == 0 )
	return tmp;

    if( tmp.sec_ < 0 )
	tmp.sec_ --;
    tmp.nsec_ = 0;
    return tmp;
}

inline DNBSimTime
fmod( const DNBSimTime &x, const DNBSimTime &y )
    DNB_THROW_SPEC((DNBEZeroDivide, DNBEOverflowError))
{
    DNBSimTime I;
    modf( x/y, &I );

    return x - I*y;
}


