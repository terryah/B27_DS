//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview czo/BPL 01:11:13
 */


//*
//* FILE:
//*     DNBQtn3D.cc   - principle implementation file
//*
//* MODULE:
//*     DNBBasicQtn3D - single template class
//*
//* OVERVIEW:
//*     This module defines a quaternion representing a 3-D rotation.
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     CZO         09/09/99    Initial implementation
//*		smw			01/07/05	Removed extractors due to iostream migration to the
//*								new, standard, templated iostream, and due to migration to 64 bit
//*								builds.  Extractor for all math classes have been moved, 
//*								for easy rescucitation, if necessary,
//*								to DNBMath.tst/PrivateInterfaces/DNBMathExtractors.h
//*     rtl         05/07/26    Fix for FPE ( floating-point exception ) in getEAA method
//* 
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1997-2002 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*
#include <DNBSystemBase.h>
#include <scl_limits.h>
#include <DNBSystemDefs.h>


#include <DNBMathConst.h>
#include <DNBRotate3D.h>
#include <DNBEuler3D.h>

//smw
#define         sqrt            DNBSqrt
#define         asin            DNBAsin
#define         acos            DNBAcos
#define         atan            DNBAtan
#define         atan2           DNBAtan2
// end smw

template <class T>
DNBBasicQtn3D<T>::DNBBasicQtn3D( )
    DNB_THROW_SPEC_NULL :
    DNBMath3D( ),
    s_ ( T(1) ),
    v_ ( )
{
    //nothing
}


template <class T>
DNBBasicQtn3D<T>::DNBBasicQtn3D( const DNBBasicQtn3D<T> &right )
    DNB_THROW_SPEC_NULL :
    DNBMath3D( ),
    s_ ( right.s_ ),
    v_ ( right.v_ )
{
    //nothing
}


template <class T>
DNBBasicQtn3D<T>::DNBBasicQtn3D( UninitializedType dummy )
    DNB_THROW_SPEC_NULL :
    DNBMath3D( dummy ),
    v_( dummy )
{
#if DNB_VERIFY
    T nan;
    DNBSetNaN( nan );
    DNB_ASSERT( DNBIsNaN( nan ) );
    s_ = nan;
#endif
}


template <class T>
DNBBasicQtn3D<T>::~DNBBasicQtn3D( )
    DNB_THROW_SPEC_NULL
{
    //nothing
}


template <class T>
void
DNBBasicQtn3D<T>::getEAA( DNBBasicVector3D<T> &axis, T &angle ) const
    DNB_THROW_SPEC_NULL
{
    if( s_ == T(0) )
    {
        axis = v_;
        angle = DNBMathConst::Pi;
        return;  // smw
    }

    axis = DNBSignum( s_ ) * v_;

    T length = DNBL2Norm( axis );

    if( length > scl_numeric_limits<T>::epsilon() )
    {
        axis /= length;
		//fix for FPE Error. Check the value of the scalar unit (s_) for overflow
		//(i.e. value being greater than 1) before calling arc cosine fn.....rtl
		T tmp = s_;
		if( tmp > T(1) )
			tmp = T(1);
        angle = T(2) * acos( abs( tmp ) );
    }
    else
    {
        axis = DNBBasicVector3D<T>::UnitZ;
        angle = T(0);
    }
}


template <class T>
void
DNBBasicQtn3D<T>::getRotate( DNBBasicRotate3D<T> &rotate ) const
    DNB_THROW_SPEC_NULL
{
    T q0Sqr = T(2) * DNBSqr( s_ ) - T(1);

    T q1_q0 = T(2) * v_[X] * s_;
    T q2_q0 = T(2) * v_[Y] * s_;
    T q3_q0 = T(2) * v_[Z] * s_;

    T q1_q2 = T(2) * v_[X] * v_[Y];
    T q1_q3 = T(2) * v_[X] * v_[Z];
    T q2_q3 = T(2) * v_[Y] * v_[Z];


    rotate.data_[N][X] = T(2) * DNBSqr( v_[X] ) + q0Sqr;
    rotate.data_[N][Y] = q1_q2 + q3_q0;
    rotate.data_[N][Z] = q1_q3 - q2_q0;

    rotate.data_[S][X] = q1_q2 - q3_q0;
    rotate.data_[S][Y] = T(2) * DNBSqr( v_[Y] ) + q0Sqr;
    rotate.data_[S][Z] = q2_q3 + q1_q0;

    rotate.data_[A][X] = q1_q3 + q2_q0;
    rotate.data_[A][Y] = q2_q3 - q1_q0;
    rotate.data_[A][Z] = T(2) * DNBSqr( v_[Z] ) + q0Sqr;
}


template <class T>
void
DNBBasicQtn3D<T>::getRPY( T &roll, T &pitch, T &yaw ) const
    DNB_THROW_SPEC_NULL
{
    DNBBasicRotate3D<T> rotate( Uninitialized );
    getRotate( rotate );
    rotate.getRPY( roll, pitch, yaw );
}


template <class T>
void
DNBBasicQtn3D<T>::getNSA( DNBBasicVector3D<T> &vecN,
			  DNBBasicVector3D<T> &vecS,
			  DNBBasicVector3D<T> &vecA ) const
    DNB_THROW_SPEC_NULL
{
    DNBBasicRotate3D<T> rotate( Uninitialized );
    getRotate( rotate );
    rotate.getNSA( vecN, vecS, vecA );
}


template <class T>
void
DNBBasicQtn3D<T>::get( T &scalar, DNBBasicVector3D<T> &vec ) const
    DNB_THROW_SPEC_NULL
{
    scalar = s_;
    vec = v_;
}


template <class T>
void
DNBBasicQtn3D<T>::setEAA( const DNBBasicVector3D<T> &axis, const T &angle )
    DNB_THROW_SPEC((DNBEZeroVector))
{
    if( angle == T(0) )
    {
	*this = Identity;
	return;
    }

    v_ = axis;
    v_.normalize( );

    T sinVal, cosVal;
    DNBSinCos( angle/T(2), sinVal, cosVal );
    v_ *= sinVal;
    s_ = cosVal;
}


template <class T>
void
DNBBasicQtn3D<T>::setCAA( AxisType axisXYZ, const T &angle )
    DNB_THROW_SPEC_NULL
{
    if( angle == T(0) )
    {
	*this = Identity;
	return;
    }

    T sinVal, cosVal;
    DNBSinCos( angle/T(2), sinVal, cosVal );
    s_ = cosVal;

    switch( axisXYZ )
    {
	case XAxis:
	    v_[X] = sinVal;
	    v_[Y] = T(0);
	    v_[Z] = T(0);
	    break;

        case YAxis:
	    v_[X] = T(0);
	    v_[Y] = sinVal;
	    v_[Z] = T(0);
	    break;

        case ZAxis:
	    v_[X] = T(0);
	    v_[Y] = T(0);
	    v_[Z] = sinVal;
	    break;

	default:
	    DNB_PROGRAM_ERROR( "Invalid rotation axis" );
	    break;
    }
}


template <class T>
void
DNBBasicQtn3D<T>::setEuler( const DNBBasicEuler3D<T> &euler )
    DNB_THROW_SPEC_NULL
{
    DNBBasicRotate3D<T> rotate( Uninitialized );
    rotate.setEuler( euler );
    setRotate( rotate );
}


template <class T>
void
DNBBasicQtn3D<T>::setRPY( const T &roll, const T &pitch, const T &yaw )
    DNB_THROW_SPEC_NULL
{
    DNBBasicRotate3D<T> rotate( Uninitialized );
    rotate.setRPY( roll, pitch, yaw );
    setRotate( rotate );
}


template <class T>
void
DNBBasicQtn3D<T>::setRotate( const DNBBasicRotate3D<T> &rotate )
    DNB_THROW_SPEC_NULL
{
    T trace = rotate.data_[N][X] + rotate.data_[S][Y] + rotate.data_[A][Z];

    T num = (T(1) - trace) / T(4);

    T q0Sqr = abs( (trace + T(1)) / T(4) );
    T q1Sqr = abs( rotate.data_[N][X] / T(2) + num );
    T q2Sqr = abs( rotate.data_[S][Y] / T(2) + num );
    T q3Sqr = abs( rotate.data_[A][Z] / T(2) + num );

    s_ = sqrt( q0Sqr );

    T q1Abs = sqrt( q1Sqr );
    T q2Abs = sqrt( q2Sqr );
    T q3Abs = sqrt( q3Sqr );

    if( (q1Abs >= q2Abs) && (q1Abs >= q3Abs) )
    {
	v_[X] = DNBSignum( rotate.data_[S][Z] - rotate.data_[A][Y] ) * q1Abs;
	v_[Y] = DNBSignum( v_[X] * ( rotate.data_[N][Y] + rotate.data_[S][X] ) ) *
		q2Abs;
	v_[Z] = DNBSignum( v_[X] * ( rotate.data_[N][Z] + rotate.data_[A][X] ) )
		* q3Abs;
    }
    else if( (q2Abs >= q3Abs) && (q2Abs >= q1Abs) )
    {
	v_[Y] = DNBSignum( rotate.data_[A][X] - rotate.data_[N][Z] ) * q2Abs;
	v_[Z] = DNBSignum( v_[Y] * ( rotate.data_[S][Z] + rotate.data_[A][Y] ) )
		* q3Abs;
	v_[X] = DNBSignum( v_[Y] * ( rotate.data_[S][X] + rotate.data_[N][Y] ) )
		* q1Abs;
    }
    else
    {
	v_[Z] = DNBSignum( rotate.data_[N][Y] - rotate.data_[S][X] ) * q3Abs;
	v_[X] = DNBSignum( v_[Z] * ( rotate.data_[A][X] + rotate.data_[N][Z] ) )
		* q1Abs;
	v_[Y] = DNBSignum( v_[Z] * ( rotate.data_[A][Y] + rotate.data_[S][Z] ) )
		* q2Abs;
    }

    T mag = sqrt( q0Sqr + q1Sqr + q2Sqr + q3Sqr );

    s_ /= mag;
    v_ /= mag;
}


template <class T>
void
DNBBasicQtn3D<T>::setNSA( const DNBBasicVector3D<T> &vecN,
		          const DNBBasicVector3D<T> &vecS,
			  const DNBBasicVector3D<T> &vecA )
    DNB_THROW_SPEC((DNBESingularMatrix, DNBEIllegalMatrix))
{
    DNBBasicRotate3D<T> tmp( Uninitialized );
    tmp.setNSA( vecN, vecS, vecA );
    setRotate( tmp );
}


template <class T>
void
DNBBasicQtn3D<T>::set( const T &scalar, const DNBBasicVector3D<T> &vec )
    DNB_THROW_SPEC((DNBENumericError))
{
    s_ = scalar;
    v_ = vec;

    normalize( );
}


template <class T>
T
DNBBasicQtn3D<T>::norm( ) const
    DNB_THROW_SPEC_NULL
{
    return DNBL2Norm( *this );
}


template <class T>
void
DNBBasicQtn3D<T>::normalize( )
    DNB_THROW_SPEC((DNBENumericError))
{
    T nm = norm( );

    if( nm <= Tolerance )
    {
	DNBENumericError error( DNB_FORMAT("Zero length quaternion") );
	throw error;
    }

    s_ /= nm;
    v_ /= nm;
}


template <class T>
DNBBasicQtn3D<T>
DNBBasicQtn3D<T>::conjugate( )
    DNB_THROW_SPEC_NULL
{
    v_ = -v_;
    return *this;
}


template <class T>
DNBBasicQtn3D<T>
DNBBasicQtn3D<T>::invert( )
    DNB_THROW_SPEC((DNBENumericError))
{
    return conjugate( );
}


template <class T>
void
DNBBasicQtn3D<T>::negate( )
    DNB_THROW_SPEC_NULL
{
    s_ = - s_;

    v_.data_[X] = - v_.data_[X];
    v_.data_[Y] = - v_.data_[Y];
    v_.data_[Z] = - v_.data_[Z];
}


template <class T>
T
DNBBasicQtn3D<T>::dot( const DNBBasicQtn3D<T> &other ) const
    DNB_THROW_SPEC_NULL
{
    return ( s_ * other.s_ +
	     v_.data_[X] * other.v_.data_[X] +
	     v_.data_[Y] * other.v_.data_[Y] +
	     v_.data_[Z] * other.v_.data_[Z] );
}


template <class T>
void
DNBBasicQtn3D<T>::swap( DNBBasicQtn3D<T> &right )
    DNB_THROW_SPEC_NULL
{
    DNBBasicQtn3D<T>  tmp( *this );
    *this = right;
    right = tmp;
}


template <class T>
void
DNBBasicQtn3D<T>::rotate( AxisType axis, const T &angle,
			  ReferenceFrame refFrame )
    DNB_THROW_SPEC_NULL
{
    DNB_ASSERT( (axis >= 0) && (axis <= 2) );

    T sinVal, cosVal;
    DNBSinCos( angle/T(2), sinVal, cosVal );

    DNBBasicQtn3D<T> result( Uninitialized );

    result.s_ = cosVal * s_ - sinVal * v_[axis];
    result.v_ = cosVal * v_;
    result.v_[axis] += s_ * sinVal;

    int i = (axis + 1) % 3;
    int j = (axis + 2) % 3;

    switch( refFrame )
    {
	case Origin:
	    result.v_[j] += sinVal * v_[i];
	    result.v_[i] -= sinVal * v_[j];

	    break;

	case Local:
	    result.v_[i] += sinVal * v_[j];
	    result.v_[j] -= sinVal * v_[i];

	    break;

	default:
	    DNB_PROGRAM_ERROR( "Invalid reference frame." );
	    break;
    }

    *this = result;
}


template <class T>
void
DNBBasicQtn3D<T>::rotate( const DNBBasicVector3D<T> &vec, const T &angle,
			  ReferenceFrame refFrame )
    DNB_THROW_SPEC((DNBEZeroVector))
{
    DNBBasicQtn3D<T> qtn( Uninitialized );
    qtn.setEAA( vec, angle );
    DNBBasicQtn3D<T> tmp( *this );

    switch( refFrame )
    {
	case Origin:
	    multiply( *this, qtn, tmp );
	    break;

	case Local:
	    multiply( *this, tmp, qtn );
	    break;

	default:
	    DNB_PROGRAM_ERROR( "Invalid reference frame." );
	    break;
    }
}


template <class T>
void
DNBBasicQtn3D<T>::rotate( const DNBBasicQtn3D<T> &qtn, ReferenceFrame refFrame )
    DNB_THROW_SPEC_NULL
{
    DNBBasicQtn3D<T> tmp( *this );

    switch( refFrame )
    {
	case Origin:
	    multiply( *this, qtn, tmp );
	    break;

	case Local:
	    multiply( *this, tmp, qtn );
	    break;

	default:
	    DNB_PROGRAM_ERROR( "Invalid reference frame." );
	    break;
    }

}


template <class T>
void
DNBBasicQtn3D<T>::preMultiply( const DNBBasicQtn3D<T> &qtn )
    DNB_THROW_SPEC_NULL
{
    DNBBasicQtn3D<T> tmp( *this );
    multiply( *this, qtn, tmp );
}


template <class T>
void
DNBBasicQtn3D<T>::postMultiply( const DNBBasicQtn3D<T> &qtn )
    DNB_THROW_SPEC_NULL
{
    DNBBasicQtn3D<T> tmp( *this );
    multiply( *this, tmp, qtn );
}


template <class T>
DNBBasicQtn3D<T>
DNBBasicQtn3D<T>::operator-( ) const
    DNB_THROW_SPEC_NULL
{
    DNBBasicQtn3D<T> tmp( Uninitialized );
    tmp.s_ = -s_;
    tmp.v_.data_[X] = -v_.data_[X];
    tmp.v_.data_[Y] = -v_.data_[Y];
    tmp.v_.data_[Z] = -v_.data_[Z];

    return tmp;
}


template <class T>
DNBBasicQtn3D<T> &
DNBBasicQtn3D<T>::operator=( const DNBBasicQtn3D<T> &right )
    DNB_THROW_SPEC_NULL
{
    if( this != &right )
    {
        s_ = right.s_;
        v_ = right.v_;
    }

    return *this;
}


template <class T>
DNBBasicQtn3D<T> &
DNBBasicQtn3D<T>::operator*=( const DNBBasicQtn3D<T> &right )
    DNB_THROW_SPEC_NULL
{
    DNBBasicQtn3D<T>  result( Uninitialized );
    multiply( result, *this, right );

    return ( *this = result );

}


template <class T>
DNBBasicQtn3D<T> &
DNBBasicQtn3D<T>::operator/=( const DNBBasicQtn3D<T> &right )
    DNB_THROW_SPEC((DNBENumericError))
{
    DNBBasicQtn3D<T>  result( Uninitialized );
    DNBBasicQtn3D<T> tmp( right );
    multiply( result, *this, tmp.invert( ) );

    return ( *this = result );
}


template <class T>
void DNBBasicQtn3D<T>::rotateVec( DNBBasicVector3D<T> &vec ) const
    DNB_THROW_SPEC_NULL
{
    DNBBasicVector3D<T>  result( Uninitialized );
    multiply( result, *this, vec );
    vec = result;
}


template <class T>
T
DNBL1Norm( const DNBBasicQtn3D<T> &qtn )
    DNB_THROW_SPEC_NULL
{
    return sqrt( DNBSqr( qtn.s_ ) + DNBSqr( DNBL1Norm( qtn.v_ ) ) );
}


template <class T>
T
DNBL2Norm( const DNBBasicQtn3D<T> &qtn )
    DNB_THROW_SPEC_NULL
{
    return sqrt( DNBSqr( qtn.s_ ) + DNBL2NormSqr( qtn.v_ ) );
}


template <class T>
T
DNBLinfNorm( const DNBBasicQtn3D<T> &qtn )
    DNB_THROW_SPEC_NULL
{
    return sqrt( DNBSqr( qtn.s_ ) + DNBSqr( DNBLinfNorm( qtn.v_ ) ) );
}


template <class T>
T
DNBL2NormSqr( const DNBBasicQtn3D<T> &qtn )
    DNB_THROW_SPEC_NULL
{
    return DNBSqr( qtn.s_ ) + DNBL2NormSqr( qtn.v_ );
}


template <class T>
bool
DNBNearRel( const DNBBasicQtn3D<T> &left, const DNBBasicQtn3D<T> &right,
	    const T &angularTol, const T &linearTol )
    DNB_THROW_SPEC_NULL
{
    if( (angularTol <= T(0) ) || (linearTol <= T(0) ) )
	return ( left == right );

    DNBBasicVector3D<T>  leftQAxis( DNBMath3D::Uninitialized );
    T  leftQAngle;

    left.getEAA( leftQAxis, leftQAngle );

    DNBBasicVector3D<T>  rightQAxis( DNBMath3D::Uninitialized );
    T  rightQAngle;

    right.getEAA( rightQAxis, rightQAngle );

    return DNBNearRel( leftQAngle, rightQAngle, angularTol ) &&
	   DNBNearRel( leftQAxis,  rightQAxis,  linearTol );
}


template <class T>
bool
DNBNearAbs( const DNBBasicQtn3D<T> &left, const DNBBasicQtn3D<T> &right,
	    const T &angularTol, const T &linearTol )
    DNB_THROW_SPEC_NULL
{
    if( (angularTol <= T(0) ) || (linearTol <= T(0) ) )
	return ( left == right );

    DNBBasicVector3D<T>  leftQAxis( DNBMath3D::Uninitialized );
    T  leftQAngle;

    left.getEAA( leftQAxis, leftQAngle );

    DNBBasicVector3D<T>  rightQAxis( DNBMath3D::Uninitialized );
    T  rightQAngle;

    right.getEAA( rightQAxis, rightQAngle );

    return DNBNearAbs( leftQAngle, rightQAngle, angularTol ) &&
	   DNBNearAbs( leftQAxis,  rightQAxis,  linearTol );

}


template <class T>
bool
operator==( const DNBBasicQtn3D<T> &left, const DNBBasicQtn3D<T> &right )
    DNB_THROW_SPEC_NULL
{
    return ( left.s_ == right.s_ ) && ( left.v_ == right.v_ );
}

template <class T>
bool
operator!=( const DNBBasicQtn3D<T> &left, const DNBBasicQtn3D<T> &right )
    DNB_THROW_SPEC_NULL
{
    return ( left.s_ != right.s_ ) || ( left.v_ != right.v_ );
}


template <class T>
DNBBasicQtn3D<T>
operator*(const DNBBasicQtn3D<T> &left, const DNBBasicQtn3D<T> &right )
    DNB_THROW_SPEC_NULL
{
    DNBBasicQtn3D<T> result( DNBMath3D::Uninitialized );
    DNBBasicQtn3D<T>::multiply( result, left, right );

    return result;
}


template <class T>
DNBBasicQtn3D<T>
operator/(const DNBBasicQtn3D<T> &left, const DNBBasicQtn3D<T> &right )
    DNB_THROW_SPEC((DNBENumericError))
{
    DNBBasicQtn3D<T> result( DNBMath3D::Uninitialized );
    DNBBasicQtn3D<T> tmp( right );
    DNBBasicQtn3D<T>::multiply( result, left, tmp.invert( ) );

    return result;
}


template <class T>
DNBBasicVector3D<T>
operator*( const DNBBasicQtn3D<T> &left, const DNBBasicVector3D<T> &right )
    DNB_THROW_SPEC_NULL
{
    DNBBasicVector3D<T> result( DNBMath3D::Uninitialized );
    DNBBasicQtn3D<T>::multiply( result, left, right );

    return result;
}


template <class T>
ostream &
operator<<( ostream &ostr, const DNBBasicQtn3D<T> &qtn )
    DNB_THROW_SPEC((scl_bad_alloc))
{
    DNBBasicMathVector<T>  vec( 4, Uninitialized );
    vec(0) = qtn.s_;
    vec(1) = (qtn.v_)[DNBMath3D::X];
    vec(2) = (qtn.v_)[DNBMath3D::Y];
    vec(3) = (qtn.v_)[DNBMath3D::Z];

    return (ostr << vec);
}



template <class T>
void
DNBBasicQtn3D<T>::EAAInterp( DNBBasicQtn3D<T> &res, T alpha,
                             const DNBBasicQtn3D<T> &from,
			     const DNBBasicQtn3D<T> &to )
    DNB_THROW_SPEC_NULL
{
    if ( alpha < T(0) )
        alpha = T(0);
    else if ( alpha > T(1) )
        alpha = T(1);

    DNBBasicQtn3D<T> tmp( from );
    tmp.invert();
    tmp.postMultiply( to );

    DNBBasicVector3D<T> axis;
    T angle;

    tmp.getEAA( axis, angle );
    tmp.setEAA( axis, alpha * angle );

    res = from * tmp;
}


template <class T>
void
DNBBasicQtn3D<T>::slerp( DNBBasicQtn3D<T> &result, const T &alpha,
                         const DNBBasicQtn3D<T> &from,
			 const DNBBasicQtn3D<T> &to )
    DNB_THROW_SPEC_NULL
{
    if( alpha <= T(0) )
    {
	result = from;
	return;
    }
    else if( alpha >= T(1) )
    {
	result = to;
	return;
    }

    T cosVal = from.s_ * to.s_ +
	       from.v_.data_[X] * to.v_.data_[X] +
	       from.v_.data_[Y] * to.v_.data_[Y] +
	       from.v_.data_[Z] * to.v_.data_[Z];

    if( cosVal < T(-1) )
	cosVal = T(-1);
    else if( cosVal > T(1) )
	cosVal = T(1);

    T angle = acos( cosVal );
    T sinVal = sin( angle );

    T co0, co1;

    if( sinVal < scl_numeric_limits<T>::epsilon() )
    {
	co0 = T(1) - alpha;
	co1 = alpha;
    }
    else
    {
	co0 = sin( (T(1) - alpha) * angle ) / sinVal;
	co1 = sin( alpha * angle ) / sinVal;
    }

    result.s_ = co0 * from.s_ + co1 * to.s_;
    result.v_.data_[X] = co0 * from.v_.data_[X] + co1 * to.v_.data_[X];
    result.v_.data_[Y] = co0 * from.v_.data_[Y] + co1 * to.v_.data_[Y];
    result.v_.data_[Z] = co0 * from.v_.data_[Z] + co1 * to.v_.data_[Z];
}


template <class T>
void
DNBBasicQtn3D<T>::squad( DNBBasicQtn3D<T> &result, const T &alpha,
			 const DNBBasicQtn3D<T> &from,
			 const DNBBasicQtn3D<T> &viaA,
			 const DNBBasicQtn3D<T> &viaB,
			 const DNBBasicQtn3D<T> &to )
    DNB_THROW_SPEC_NULL
{
    if( alpha <= T(0) )
    {
	result = from;
	return;
    }
    else if( alpha >= T(1) )
    {
	result = to;
	return;
    }

    DNBBasicQtn3D<T> tmp0( Uninitialized );
    DNBBasicQtn3D<T> tmp1( Uninitialized );

    slerp( tmp0, alpha, from, to );
    slerp( tmp1, alpha, viaA, viaB );
    slerp( result, T(2) * alpha * ( T(1) - alpha ), tmp0, tmp1 );
}


template <class T>
const DNBBasicQtn3D<T> DNBBasicQtn3D<T>::Identity;


template <class T>
const T  DNBBasicQtn3D<T>::Tolerance = T(100) * scl_numeric_limits<T>::epsilon( );


template <class T>
void
DNBBasicQtn3D<T>::multiply( DNBBasicQtn3D<T> &result,
			    const DNBBasicQtn3D<T> &left,
			    const DNBBasicQtn3D<T> &right )
    DNB_THROW_SPEC_NULL
{
    result.s_ = left.s_ * right.s_ - DNBDot( left.v_, right.v_ );
    result.v_ = left.s_ * right.v_ + right.s_ * left.v_ +
		DNBCross( left.v_, right.v_ );
}


template <class T>
void
DNBBasicQtn3D<T>::multiply( DNBBasicVector3D<T> &result,
			    const DNBBasicQtn3D<T> &left,
			    const DNBBasicVector3D<T> &right )
    DNB_THROW_SPEC_NULL
{
    DNBBasicVector3D<T>  tmp( DNBCross( left.v_, right ) );
    result = right + T(2) * ( left.s_ * tmp + DNBCross( left.v_, tmp ) );
}
