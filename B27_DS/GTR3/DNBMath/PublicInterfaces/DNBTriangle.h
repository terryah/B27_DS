//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/


#ifndef _DNB_BASICTRIANGLE_H_
#define _DNB_BASICTRIANGLE_H_


#include <DNBSystemBase.h>
#include <scl_limits.h>
#include <DNBSystemDefs.h>

template < class T >
void    
DNBTriangleAASSSA( const T& A, const T& B, const T& a, 
		  T& b, T& c, T& C )
	DNB_THROW_SPEC_NULL;

template < class T >
void    
DNBTriangleASASAS( const T& A, const T& c, const T& B, 
		T& a, T& C, T& b )
	DNB_THROW_SPEC_NULL;


template < class T >
T
DNBTriangleSASS( const T& b, const T& A, const T& c )
        DNB_THROW_SPEC_NULL;


template < class T >
T
DNBTriangleSASA( const T& b, const T& A, const T& c )
        DNB_THROW_SPEC_NULL;


template < class T >
T
DNBTriangleSSSA( const T& a, const T& b, const T& c )
        DNB_THROW_SPEC_NULL;


#include "DNBTriangle.cc"


#endif
