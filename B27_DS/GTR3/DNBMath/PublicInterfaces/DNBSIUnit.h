//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 */

//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     vjk         12/15/98    Initial implementation.
//*     bkh         11/05/03    Implementation of CAA style documentation.
//* 
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNBSIUNIT_H_
#define _DNBSIUNIT_H_


#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>


#include <DNBMath.h>


#include <DNBMathConst.h>




/**
  * Conversion constants which convert from standard units to the
  * International System of Units (SI).
  * 
  * <br><B>Description</B><br>
  * This class defines a set of scale factors which are used to convert from
  * standard units to the International System of Units (SI).
  * 
  * <br><B>Example</B><br>
  * <pre>
  * #include DNBSystemBase.h
  * #include DNB_IOSTREAM
  * #include DNBSystemDefs.h
  * #include DNBSIUnit.h
  * 
  * //
  * //  Calculate the area of a rectangle with the specified width and
  * //  length expressed in meters.
  * //
  * DNBReal
  * calcArea( DNBReal width, DNBReal length )
  * DNB_THROW_SPEC_NULL
  * {
  * return ( width * length );
  * }
  * 
  * int
  * main( )
  * {
  * DNBReal SIArea = calcArea( 2.5 * DNBSIUnit::Foot,
  * 10.0 * DNBSIUnit::Centi * DNBSIUnit::Meter );
  * 
  * cout << "computed area = " << SIArea << " square meters" << endl;
  * cout << "expected area = 0.0762 square meters" << endl;
  * 
  * return (0);
  * }
  * 
  * </pre>
  *
  */
template <class T>
class DNBBasicSIUnit
{
public:
    //
    //  Metric prefixes.
    //

/**
  * This constant converts physical measurements expressed in tera
  * units (10^12) to standard units.
  * 
  * 
  */
    static const T Tera;


/**
  * This constant converts physical measurements expressed in giga
  * units (10^9) to standard units.
  * 
  * 
  */
    static const T Giga;


/**
  * This constant converts physical measurements expressed in mega
  * units (10^6) to standard units.
  * 
  * 
  */
    static const T Mega;


/**
  * This constant converts physical measurements expressed in kilo
  * units (10^3) to standard units.
  * 
  * 
  */
    static const T Kilo;


/**
  * This constant converts physical measurements expressed in hecto
  * units (10^2) to standard units.
  * 
  * 
  */
    static const T Hecto;


/**
  * This constant converts physical measurements expressed in deka
  * units (10^1) to standard units.
  * 
  * 
  */
    static const T Deka;


/**
  * This constant converts physical measurements expressed in deci
  * units (10^-1) to standard units.
  * 
  * 
  */
    static const T Deci;


/**
  * This constant converts physical measurements expressed in centi
  * units (10^-2) to standard units.
  * 
  * 
  */
    static const T Centi;


/**
  * This constant converts physical measurements expressed in milli
  * units (10^-3) to standard units.
  * 
  * 
  */
    static const T Milli;


/**
  * This constant converts physical measurements expressed in micro
  * units (10^-6) to standard units.
  * 
  * 
  */
    static const T Micro;


/**
  * This constant converts physical measurements expressed in nano
  * units (10^-9) to standard units.
  * 
  * 
  */
    static const T Nano;


/**
  * This constant converts physical measurements expressed in pico
  * units (10^-12) to standard units.
  * 
  * 
  */
    static const T Pico;


    //
    //  Angular measures.
    //

/**
  * This constant converts an angular value expressed in radians to
  * radians.
  * 
  * 
  */
    static const T Radian;


/**
  * This constant converts an angular value expressed in degrees to
  * radians.
  * 
  * 
  */
    static const T Degree;


    //
    //  Time units.
    //

/**
  * This constant specifies the conversion factor from seconds to
  * seconds.
  * 
  * 
  */
    static const T Second;


/**
  * This constant specifies the conversion factor from minutes to
  * seconds.
  * 
  * 
  */
    static const T Minute;


/**
  * This constant specifies the conversion factor from hours to
  * seconds.
  * 
  * 
  */
    static const T Hour;


/**
  * This constant specifies the conversion factor from days to
  * seconds.
  * 
  * 
  */
    static const T Day;


/**
  * This constant specifies the conversion factor from weeks to
  * seconds.
  * 
  * 
  */
    static const T Week;


    //
    //  Length units.
    //

/**
  * This constant specifies the conversion factor from meters to meters.
  * 
  * 
  */
    static const T Meter;


/**
  * This constant specifies the conversion factor from inches to meters.
  * 
  * 
  */
    static const T Inch;


/**
  * This constant specifies the conversion factor from feet to meters.
  * 
  * 
  */
    static const T Foot;


/**
  * This constant specifies the conversion factor from yards to meters.
  * 
  * 
  */
    static const T Yard;


/**
  * This constant specifies the conversion factor from fathoms to
  * meters.
  * 
  * 
  */
    static const T Fathom;


/**
  * This constant specifies the conversion factor from miles to meters.
  * 
  * 
  */
    static const T Mile;
};




/**
  * A class which converts physical quantities from standard units to SI
  * units.
  * 
  * <br><B>Description</B><br>
  * This type definition represents the set of scale factors required to
  * convert physical quantities expressed in various standard units to their
  * equivalent values expressed in the International System of Units (SI).
  * 
  * 
  */
typedef DNBBasicSIUnit<DNBReal>     DNBSIUnit;




//
//  Include the public definition file.
//
#include "DNBSIUnit.cc"




#endif  /* _DNBSIUNIT_H_ */
