//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/

#ifndef _DNB_BASICQUADRILATERAL_H_
#define _DNB_BASICQUADRILATERAL_H_


#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>

template < class T >
T
DNBQuadrilateralSASSSA1( const T& a, const T& A, 
		         const T& b, const T& c, const T& d )
	DNB_THROW_SPEC_NULL;


template < class T >
T
DNBQuadrilateralSASSSA2( const T& a, const T& A, 
		         const T& b, const T& c, const T& d )
	DNB_THROW_SPEC_NULL;



template < class T >
T
DNBQuadrilateralSASASS( const T& a, const T& A, 
		         const T& b, const T& B, const T& c )
	DNB_THROW_SPEC_NULL;



template < class T >
T
DNBQuadrilateralSASASA( const T& a, const T& A, 
		         const T& b, const T& B, const T& c )
	DNB_THROW_SPEC_NULL;



#include "DNBQuadrilateral.cc"

#endif

