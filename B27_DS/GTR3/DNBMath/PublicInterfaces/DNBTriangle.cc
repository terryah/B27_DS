//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview czo/BPL 01:11:13
 * @quickreview upe/rtl 09:10:02
 * @quickreview k7b 16:09:28 SSSA should return PI/2 if b or c = 0 not PI
 */


#include "DNBTriangle.h"
#  ifndef M_PI
#    define M_PI 3.14159265358979323846
#  endif

template < class T >
void    
DNBTriangleAASSSA( const T& A, const T& B, const T& a, 
		  T& b, T& c, T& C )
	DNB_THROW_SPEC_NULL
{
    scl_numeric_limits< T > epsilon;
    DNB_PRECONDITION( abs( A ) > epsilon.epsilon() ); 
     
    C = M_PI - A - B;
    b = sin( B )/sin( A )*a;
    c = b*cos( A ) + a*cos( B ); 

}


template < class T >
void    
DNBTriangleASASAS( const T& A, const T& c, const T& B, 
		T& a, T& C, T& b )
	DNB_THROW_SPEC_NULL
{
    scl_numeric_limits< T > epsilon;
    C = M_PI - A - B;

    DNB_PRECONDITION( abs( C ) > epsilon.epsilon() ); 
    a = sin( A )/sin( C ) * c;  
    b = sin( B )/sin( C ) * c;  

}

template < class T >
T    
DNBTriangleSASS( const T& b, const T& A, const T& c )
	DNB_THROW_SPEC_NULL
{

    return ( sqrt( b*b + c*c - 2.0*b*c*cos( A ) ));


}



template < class T >
T
DNBTriangleSASA( const T& b, const T& A, const T& c )
	DNB_THROW_SPEC_NULL
{
    return ( atan2( b*sin( A ), c - b*cos( A ) ));

}


template < class T >
T
DNBTriangleSASASA( const T& b, const T& A, const T& c,
		   T& B, T& a, T& C )
	DNB_THROW_SPEC_NULL
{
    B = DNBTriangleSASA( b, A, c );
    C = M_PI - A - B;
    a = DNBTriangleSASS( b, A, c );
}



template < class T >
T 
DNBTriangleSSSA( const T& a, const T& b, const T& c )
	DNB_THROW_SPEC_NULL
{

#if defined(_SUNOS_SOURCE)
    T temp = ( b*b + c*c - a*a )/( 2.0*b*c );
    if( temp < -1.0 ) return M_PI;
    if( temp > 1.0 ) return 0.0;
    return ( acos( temp ));
#else
    //New Impl to handle the case of denominator being 0
    scl_numeric_limits< T >  traits;
    if ( abs( b ) < traits.epsilon() || abs( c ) < traits.epsilon() )
        return M_PI/2;

    T  temp  =  (  b*b + c*c - a*a  )/( 2.0*b*c );
    if ( temp < -1.0 )  return  M_PI;
    if ( temp >  1.0 )  return  0.0;
    return ( acos(temp) );
#endif
}  
   
/*
{
    T temp = ( b*b + c*c - a*a )/( 2.0*b*c );
    if( temp < -1.0 ) return M_PI;
    if( temp > 1.0 ) return 0.0;
    return ( acos( temp ));
}
*/

