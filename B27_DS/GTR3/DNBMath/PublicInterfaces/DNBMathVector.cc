//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/

/**
 * @fullreview CZO smw 03:09:10
 * @error MISC Y err_1 By definition size_t is non-negative integer, check DNB_PRECONDITION ( index < size_ ) instead
 */

//==============================================================================
// File name: DNBMathVector.cc
//
//      Implementation file for the DNBBasicMathVector templated class.
//
//==============================================================================
// HISTORY:
//     Author       Date         Purpose
//     ------       ----         -------
//      smw         6/18/2003       Initial Implementation
//      smw         11/24/2003      Integrated the use of DNBPoolAllocator
//      smw         01/07/2003      More cleanup
//		smw			01/07/2005		Removed extractors due to iostream migration to the
//									new, standard, templated iostream, and due to migration 
//									to 64 bit builds.  Extractor for all math classes have 
//									been moved, for easy rescucitation, if necessary,
//									to DNBMath.tst/PrivateInterfaces/DNBMathExtractors.h
//
//=====================================================================

#include <scl_memory.h>

/*****************************************************************************************
// Unrolling of loops for parallelism of arithmetic
/*****************************************************************************************/

#define DNB_UNROLL_LOOPS DNB_YES



//*****************************************************************************************
// UNROLLING MACROS
// In oder for the compiler to optimize, Unroll loops every four elements;
//*****************************************************************************************

// The do while loop serves two purposes: 1) ensures a local scope
// and 2) requires caller to put semicolunm after call to macro

#if DNB_UNROLL_LOOPS==DNB_YES

#define APPLY_SCALAR_OP_VECTOR(arg1, anOp, arg2)    do {                \
    int noOfLoops = (int)size_ / 4;                                     \
    int leftover = (int)size_ % 4;                                      \
    int last = noOfLoops*4;                                             \
    for (int i = 0; i < last; i += 4)                                   \
    {                                                                   \
            data_[i] = arg1 anOp arg2[i];                               \
            data_[i+1] = arg1 anOp arg2[i+1];                           \
            data_[i+2] = arg1 anOp arg2[i+2];                           \
            data_[i+3] = arg1 anOp arg2[i+3];                           \
    }                                                                   \
    switch ( leftover )                                                 \
    {                                                                   \
        case 3:                                                         \
            data_[i] = arg1 anOp arg2[i];                               \
            ++i;                                                        \
            /* Fall Through */                                          \
        case 2:                                                         \
            data_[i] = arg1 anOp arg2[i];                               \
            ++i;                                                        \
            /* Fall Through */                                          \
        case 1:                                                         \
            data_[i] = arg1 anOp arg2[i];                               \
            ++i;                                                        \
    }                                                                   \
} while (0) 

#else

#define APPLY_SCALAR_OP_VECTOR(arg1, anOp, arg2) do {                   \
    for (int i = 0; i<size_; i++) data_[i] = arg1 anOp arg2[i];         \
} while (0)

#endif



#if DNB_UNROLL_LOOPS==DNB_YES

#define APPLY_VECTOR_OP_VECTOR(arg1, anOp, arg2)    do {                \
    int noOfLoops = (int)size_ / 4;                                     \
    int leftover = (int)size_ % 4;                                      \
    int last = noOfLoops*4;                                             \
    for (int i = 0; i < last; i += 4)                                   \
    {                                                                   \
            data_[i] = arg1[i] anOp arg2[i];                            \
            data_[i+1] = arg1[i+1] anOp arg2[i+1];                      \
            data_[i+2] = arg1[i+2] anOp arg2[i+2];                      \
            data_[i+3] = arg1[i+3] anOp arg2[i+3];                      \
    }                                                                   \
    switch ( leftover )                                                 \
    {                                                                   \
        case 3:                                                         \
            data_[i] = arg1[i] anOp arg2[i];                            \
            ++i;                                                        \
            /* Fall Through */                                          \
        case 2:                                                         \
            data_[i] = arg1[i] anOp arg2[i];                            \
            ++i;                                                        \
            /* Fall Through */                                          \
        case 1:                                                         \
            data_[i] = arg1[i] anOp arg2[i];                            \
            ++i;                                                        \
    }                                                                   \
} while (0)

#else

#define APPLY_VECTOR_OP_VECTOR(arg1, anOp, arg2)    do {                \
    for (int i = 0; i < size_; i++) data_[i] = arg1[i] anOp arg2[i];    \
} while (0)

#endif


#if DNB_UNROLL_LOOPS==DNB_YES

#define APPLY_VECTOR_OP_SCALAR(arg1, anOp, arg2)   do {                 \
    int noOfLoops = (int)size_ / 4;                                     \
    int leftover = (int)size_ % 4;                                      \
    int last = noOfLoops*4;                                             \
    for (int i = 0; i < last; i += 4)                                   \
    {                                                                   \
            data_[i] = arg1[i] anOp arg2;                               \
            data_[i+1] = arg1[i+1] anOp arg2;                           \
            data_[i+2] = arg1[i+2] anOp arg2;                           \
            data_[i+3] = arg1[i+3] anOp arg2;                           \
    }                                                                   \
    switch ( leftover )                                                 \
    {                                                                   \
        case 3:                                                         \
            data_[i] = arg1[i] anOp arg2;                               \
            ++i;                                                        \
            /* Fall Through */                                          \
        case 2:                                                         \
            data_[i] = arg1[i] anOp arg2;                               \
            ++i;                                                        \
            /* Fall Through */                                          \
        case 1:                                                         \
            data_[i] = arg1[i] anOp arg2;                               \
            ++i;                                                        \
    }                                                                   \
} while (0)

#else

#define APPLY_VECTOR_OP_SCALAR(arg1, anOp, arg2)   do {                 \
    for (int i = 0; i < size_; i++) data_[i] = arg1[i] anOp arg2;       \
} while (0)

#endif



#if DNB_UNROLL_LOOPS == DNB_YES

#define APPLY_ASSIGNMENT(arg) do {                                      \
    int noOfLoops = (int)size_ / 4;                                     \
    int leftover = (int)size_ % 4;                                      \
    int last = noOfLoops*4;                                             \
    for (int i = 0; i < last; i += 4)                                   \
    {                                                                   \
            data_[i] = arg[i];                                          \
            data_[i+1] = arg[i+1];                                      \
            data_[i+2] = arg[i+2];                                      \
            data_[i+3] = arg[i+3] ;                                     \
    }                                                                   \
    switch ( leftover )                                                 \
    {                                                                   \
        case 3:                                                         \
            data_[i] = arg[i];                                          \
            ++i;                                                        \
            /* Fall Through */                                          \
        case 2:                                                         \
            data_[i] = arg[i];                                          \
            ++i;                                                        \
            /* Fall Through */                                          \
        case 1:                                                         \
            data_[i] = arg[i];                                          \
            ++i;                                                        \
    }                                                                   \
} while (0)                                                             

#else

#define APPLY_ASSIGNMENT(arg) do {                                      \
    for (int i = 0; i < size_; i++) data_[i] = arg[i];                  \
} while (0)
#endif


/*****************************************************************************************
// Constructors
/****************************************************************************************/

/**
* Constructs a vector with no elements. It can later be resized and filled with data
*/ 


template <class T>
DNBBasicMathVector<T>::
DNBBasicMathVector() 
DNB_THROW_SPEC((scl_bad_alloc)) : 
     alloc_(&DNBPoolAllocator::Global())
{
    allocate(0);
}



/**
* Constructs a vector with n elements initialized to initVal, which defaults to T(0)
*/ 
template <class T>
DNBBasicMathVector<T>::
DNBBasicMathVector(size_t n, const T& initVal)  
DNB_THROW_SPEC((scl_bad_alloc)) : alloc_(&DNBPoolAllocator::Global())

{
    allocate(n);
    Assign(data_, size_, initVal);
}


/**
* Constructs a vector with n uninitialized elements
*/ 
template <class T>
DNBBasicMathVector<T>::
DNBBasicMathVector(size_t n, UninitializedType dummy)  
DNB_THROW_SPEC((scl_bad_alloc)) : alloc_(&DNBPoolAllocator::Global())

{
    allocate(n);
#if DNB_VERIFY
    T       NaN;
    DNBSetNaN( NaN );
    Assign(data_, size_, NaN);
#endif
}


/**
* Constructs a vector with n elements, initialized with T* dat, which must have n elements
* A copy of dat is made.
*/
template <class T>
DNBBasicMathVector<T>::
DNBBasicMathVector(const T* dat,size_t n)  
DNB_THROW_SPEC((scl_bad_alloc)) : alloc_(&DNBPoolAllocator::Global())

{
    allocate(n);
    Assign ( data_, size_, dat);
}


/**
  * Constructs a vector from the null terminated character string s. The format of the 
  * character string is the same as that expected by the global operator operator>> 
  */
/*
template <class T>
DNBBasicMathVector<T>::
DNBBasicMathVector(const char *s)  
DNB_THROW_SPEC((scl_bad_alloc)) : alloc_(&DNBPoolAllocator::Global())
{
DNB_PROGRAM_ERROR("This method is not yet implemented");
}
*/

/**
* Copy constructor
*/
template <class T>
DNBBasicMathVector<T>::
DNBBasicMathVector(const DNBBasicMathVector<T> & v) 
DNB_THROW_SPEC((scl_bad_alloc)) : alloc_(&DNBPoolAllocator::Global())  // or v.getAllocator()
                             
{
    allocate(v.length());
    APPLY_ASSIGNMENT(v);  // cannot use the Assign method here because v is not a T*
}


//************************************************************************************
// Destructor
//************************************************************************************

template <class T>
DNBBasicMathVector<T>::
~DNBBasicMathVector() DNB_THROW_SPEC_NULL
{
    deallocate();
}

//************************************************************************************
// Member functions
//************************************************************************************

/**
 * Returns the vector element at index where index ranges from 0 to length-1;
 */
template <class T>
T&
DNBBasicMathVector<T>::operator[] (size_t index)  DNB_THROW_SPEC_NULL
{
       DNB_PRECONDITION ( index < size_);
       return(data_[index]);
}


/**
 * Const version of above.
 */
template <class T>
const T&
DNBBasicMathVector<T>::operator[] (size_t index) const DNB_THROW_SPEC_NULL
{
        DNB_PRECONDITION ( index < size_);
        return(data_[index]);
}


/**
 * Same as operator[]
 */
template <class T>
T&
DNBBasicMathVector<T>::operator() (size_t index) DNB_THROW_SPEC_NULL
{
        DNB_PRECONDITION ( index < size_);
        return(data_[index]);
}


/**
 * Same as operator[], const version
 */
template <class T>
const T&
DNBBasicMathVector<T>::operator() (size_t index) const DNB_THROW_SPEC_NULL
{
        DNB_PRECONDITION ( index < size_);
        return(data_[index]);
}


/**
* Returns the number of elements in the vector.
*/
template <class T>
size_t
DNBBasicMathVector<T>::length() const DNB_THROW_SPEC_NULL
{
    return size_;
}

/**
* Changes the length of the vector to n elements. After reshaping, the contents 
* of the vector are undefined; that is, they can be and probably will be garbage.
*/
template <class T>
void
DNBBasicMathVector<T>::reshape(size_t n, bool init) DNB_THROW_SPEC((scl_bad_alloc))
{
    reallocate(n, init);
}

/**
* Changes the size of the vector to n elements, adding 0s or truncating as necessary.
*/
template <class T>
void
DNBBasicMathVector<T>::resize(size_t n) DNB_THROW_SPEC((scl_bad_alloc))
{
    reallocate(n, true);
}



/*****************************************************************************************
/* Mathematical operators
/* ***************************************************************************************/


/**
 * Increment each element of self. This is invoked when the operator is used as a 
 * postfix operator.
 */
template <class T>
void                
DNBBasicMathVector<T>::operator++(int) DNB_THROW_SPEC_NULL
{
    APPLY_SCALAR_OP_VECTOR(1, +, data_);
}


/**
 * Increment each element of self. This is invoked when the operator is used as a 
 * prefix operator. It returns the vector after increment
 */

template <class T>
DNBBasicMathVector<T> &     
DNBBasicMathVector<T>::operator++() DNB_THROW_SPEC_NULL
{
    (*this)++;
    return(*this);
}


/**
 * Decrement each element of self.  This is invoked when the operator is used as a 
 * postfix operator.
 */
template <class T>
void                
DNBBasicMathVector<T>::operator--(int) DNB_THROW_SPEC_NULL
{
   APPLY_VECTOR_OP_SCALAR(data_, -, 1);
}


/**
 * Decrement each element of self. This is invoked when the operator is used as a 
 * prefix operator.  It returns the vector after decrement.
 */
template <class T>
DNBBasicMathVector<T> &    
DNBBasicMathVector<T>::operator--() DNB_THROW_SPEC_NULL
{
    (*this)--;
    return(*this);
}



/**
  * Assignment operator
  */
template <class T>
void
DNBBasicMathVector<T>::operator= (const DNBBasicMathVector<T> & v) DNB_THROW_SPEC_NULL
{
    reallocate(v.length(), false);
    APPLY_ASSIGNMENT(v);
}


/**
  * Assigns a value of type T to each element of the vector
  */
template <class T>
void   
DNBBasicMathVector<T>::operator=(const T& aVal) DNB_THROW_SPEC_NULL
{
    Assign(data_, size_, aVal);
}


template <class T>
void
DNBBasicMathVector<T>::operator+=(const DNBBasicMathVector<T>& v) 
DNB_THROW_SPEC_NULL
{
      DNB_PRECONDITION ( size_ == v.length());   
      APPLY_VECTOR_OP_VECTOR(data_, + , v);
}


template <class T>
void   
DNBBasicMathVector<T>::operator-=(const DNBBasicMathVector<T>& v) DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION ( size_ == v.length());   
    APPLY_VECTOR_OP_VECTOR(data_, -, v);
}


template <class T>
void   
DNBBasicMathVector<T>::operator*=(const DNBBasicMathVector<T>& v) DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION ( size_ == v.length());   
    APPLY_VECTOR_OP_VECTOR(data_, *, v);
}


template <class T>
void   
DNBBasicMathVector<T>::operator/=(const DNBBasicMathVector<T>& v) DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION ( size_ == v.length());
    DNB_PRECONDITION(DNBMinAbsValue(v) != 0);  // none of the elements is 0         
    APPLY_VECTOR_OP_VECTOR(data_, / , v);
}


template <class T>
void   
DNBBasicMathVector<T>::operator+=(const T& aVal) DNB_THROW_SPEC_NULL
{
    APPLY_VECTOR_OP_SCALAR(data_, +, aVal);
}


template <class T>
void   
DNBBasicMathVector<T>::operator-=(const T& aVal) DNB_THROW_SPEC_NULL
{
    APPLY_VECTOR_OP_SCALAR(data_, -, aVal);
}


template <class T>
void   
DNBBasicMathVector<T>::operator*=(const T& aVal) DNB_THROW_SPEC_NULL
{
    APPLY_VECTOR_OP_SCALAR(data_, *, aVal);
}


template <class T>
void   
DNBBasicMathVector<T>::operator/=(const T& aVal) DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION ( aVal != 0);
    APPLY_VECTOR_OP_SCALAR(data_, /, aVal);
}

/////////////////////////////////////////////////////////////////////////////////////////
// Utilities to localize and modularize memory allocation in constructors and destructor
////////////////////////////////////////////////////////////////////////////////////////


// Called only in ctors.
template <class T>
void
DNBBasicMathVector<T>::allocate( size_t reqSize )
{
    if ( reqSize < defaultCapacity)
    {
	    data_ = (T*) alloc_->allocate( defaultCapacity * sizeof(T) );
    	capacity_ = defaultCapacity;
    }

    else 
    {
        data_ = (T*) alloc_->allocate( reqSize * sizeof(T) );
        capacity_ = reqSize;
    }

	data1_ = data_ - 1 ;
	size_ = reqSize;
}

// Called outside of ctors.
template <class T>
void
DNBBasicMathVector<T>::reallocate( size_t newSize, bool init )
{
	if ( newSize > capacity_ )
	{
        // allocate new space for the vector
		T *temp = (T*) alloc_->allocate( newSize * sizeof(T) );

        // copy data from old location to new
		Assign( temp, size_, data_);	// note: use old size_

        // deallocate space of old data
		alloc_->deallocate( (void*) data_, capacity_ * sizeof(T) );

        // make the new location this vector's data_
		data_  = temp;
		data1_ = data_ - 1;
		capacity_ = newSize;
	}

	if ( init && newSize > size_ )
		Assign( data_ + size_, newSize - size_, T(0) );

	size_ = newSize;
}

// Called only in dtor.
template <class T>
void
DNBBasicMathVector<T>::deallocate( )
{
	alloc_->deallocate( data_, capacity_ * sizeof(T) );
	data_ = NULL;
	data1_ = NULL;
	size_ = 0;
	capacity_ = 0;
}



///////////////////////////////////////////////////////////////////////////////////////
// Constructors that do math!  ( called by the global mathematical operators on vectors)
///////////////////////////////////////////////////////////////////////////////////////

/**
  * @nodoc
  * Additive inverse
  */
template <class T>
DNBBasicMathVector<T>::
DNBBasicMathVector( const DNBBasicMathVector<T>& v, 
                       const _DNBSubOp&)
                       DNB_THROW_SPEC((scl_bad_alloc)) : alloc_(&DNBPoolAllocator::Global())
{
    allocate(v.length());
    APPLY_ASSIGNMENT(-v);
}



/**
  * @nodoc
  * Vector Addition
  */
template <class T>
DNBBasicMathVector<T>::
DNBBasicMathVector( const DNBBasicMathVector<T> &left,
                       const DNBBasicMathVector<T> &right, 
                       const _DNBAddOp &)
                       DNB_THROW_SPEC((scl_bad_alloc)) : alloc_(&DNBPoolAllocator::Global())
{
    allocate(left.length());
    APPLY_VECTOR_OP_VECTOR(left, + , right);
}

/**
 * @nodoc
 * Vector subtraction
 */
template <class T>
DNBBasicMathVector<T>::
DNBBasicMathVector( const DNBBasicMathVector<T> &left,
                    const DNBBasicMathVector<T> &right, 
                    const _DNBSubOp &
                   )
                  DNB_THROW_SPEC((scl_bad_alloc)) : 
                  alloc_(&DNBPoolAllocator::Global())
                                                    
{   
    allocate(left.length());
    APPLY_VECTOR_OP_VECTOR(left, - , right);
}


/**
 * @nodoc
 * Vector element by element multiplication
 */
template <class T>
DNBBasicMathVector<T>::
DNBBasicMathVector( const DNBBasicMathVector<T> &left,
                    const DNBBasicMathVector<T> &right, 
                    const _DNBMulOp &
                    )
                  DNB_THROW_SPEC((scl_bad_alloc))
                  : alloc_(&DNBPoolAllocator::Global())
  
{
    allocate(left.length());
    APPLY_VECTOR_OP_VECTOR(left, * , right);
}

/**
 * @nodoc
 * Vector element by element division (first argument divided by second argument)
 */
template <class T>
DNBBasicMathVector<T>::
DNBBasicMathVector( const DNBBasicMathVector<T> &left,
                       const DNBBasicMathVector<T> &right, 
                       const _DNBDivOp & )
                       DNB_THROW_SPEC((scl_bad_alloc)) :
                  alloc_(&DNBPoolAllocator::Global())
  
{
    allocate(left.length());
    APPLY_VECTOR_OP_VECTOR(left, / , right); // precondition in calling method insures no divide by 0
}

/**
 * @nodoc
 * Vector + scalar or scalar + vector
 */
template <class T>
DNBBasicMathVector<T>::
DNBBasicMathVector( const DNBBasicMathVector<T> &vec,
                       const T& aVal, 
                       const _DNBAddOp &) 
                       DNB_THROW_SPEC((scl_bad_alloc)) :
                  alloc_(&DNBPoolAllocator::Global())
  
{
    allocate(vec.length());
    APPLY_VECTOR_OP_SCALAR(vec, + , aVal);
}


/**
 * @nodoc
 * vector - scalar
 */
template <class T>
DNBBasicMathVector<T>::
DNBBasicMathVector( const DNBBasicMathVector<T> &left,
                       const T& aVal, 
                       const _DNBSubOp &)
                       DNB_THROW_SPEC((scl_bad_alloc)) : alloc_(&DNBPoolAllocator::Global())
{
    allocate(left.length());
    APPLY_VECTOR_OP_SCALAR(left, - , aVal);
}

/**
 * @nodoc
 * scalar - vector
 */
template <class T>
DNBBasicMathVector<T>::
DNBBasicMathVector( const T& aVal, 
                   const DNBBasicMathVector<T> &right,
                   const _DNBSubOp &)
                   DNB_THROW_SPEC((scl_bad_alloc)) :  
                   alloc_(&DNBPoolAllocator::Global())
  
{
    allocate(right.length());
    APPLY_SCALAR_OP_VECTOR(aVal, - , right);
}

/**
 * @nodoc
 * vector * scalar or scalar * vector
 */
template <class T>
DNBBasicMathVector<T>::
DNBBasicMathVector( const DNBBasicMathVector<T> &vec,
                       const T& aVal, 
                       const _DNBMulOp &)
                       DNB_THROW_SPEC((scl_bad_alloc)) :
                  alloc_(&DNBPoolAllocator::Global())
  
{
    allocate(vec.length());
    APPLY_VECTOR_OP_SCALAR(vec, *, aVal);
}


/**
 * @nodoc
 * vector / scalar
 */
template <class T>
DNBBasicMathVector<T>::
DNBBasicMathVector( const DNBBasicMathVector<T> &left,
                       const T& aVal, 
                       const _DNBDivOp &)
                       DNB_THROW_SPEC((scl_bad_alloc)) : 
                  alloc_(&DNBPoolAllocator::Global())
{
    allocate(left.length());
    APPLY_VECTOR_OP_SCALAR(left, / , aVal);
}

/**
 * @nodoc
 * scalar / vector
 */
template <class T>
DNBBasicMathVector<T>::
DNBBasicMathVector( const T& aVal, 
                       const DNBBasicMathVector<T> &right,
                       const _DNBDivOp &)
                       DNB_THROW_SPEC((scl_bad_alloc)) : 
                       alloc_(&DNBPoolAllocator::Global())  
{
    allocate(right.length());
    APPLY_SCALAR_OP_VECTOR(aVal, / , right);
}

//***********************************************************************************
//  Logical Relational Operators
//***********************************************************************************


template <class T>
bool 
DNBBasicMathVector<T>::
operator==(const DNBBasicMathVector<T>& v2) const
DNB_THROW_SPEC_NULL
{
    if (size_ != v2.length()) return FALSE;
    for (int i = 0; i < size_; i++)
        if (data_[i] != v2[i]) return FALSE;
    return TRUE;
}


/**
 * Returns True if the vectors are of different size, or if they are of the same size and 
 * at least one element in v1 is different from its counterpart in v2
 */
template <class T>
bool 
DNBBasicMathVector<T>::
operator!=(const DNBBasicMathVector<T>& v2) const
DNB_THROW_SPEC_NULL
{
    return !(*this == v2);
}


/**
 * Returns True if every element in v1 is less than or equal to its counterpart in v2. 
 * Assumes both vectors have the same size.
 */
template <class T>
bool 
DNBBasicMathVector<T>::
operator<=(const DNBBasicMathVector<T>& v2) const
DNB_THROW_SPEC_NULL
{
    return ( ! (*this > v2));
}


/**
 * Returns True if every element in v1 is greater than its counterpart in v2.  Assumes both 
 * vectors have the same size.
 */
template <class T>
bool 
DNBBasicMathVector<T>::
operator>(const DNBBasicMathVector<T>& v2) const
DNB_THROW_SPEC_NULL
{
    return( v2 < *this );
}



/**
 * Returns True if every element in v1 is greater than or equal to its counterpart in v2.  
 * Assumes both vectors have the same size.
 */
template <class T>
bool 
DNBBasicMathVector<T>::
operator>=(const DNBBasicMathVector<T>& v2) const
DNB_THROW_SPEC_NULL
{
    return (! (*this < v2));
}


/**
  * Returns true if both vectors are of same size and every element is < its counterpart.
  */
template <class T>
bool 
DNBBasicMathVector<T>::
operator<(const DNBBasicMathVector<T>& v2) const
DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION(size_ == v2.length());
    for (int i = 0; i < size_; i++)
        {
        if (data_[i] >= v2[i]) return FALSE; 
        }
    return TRUE;
}


template <class T>
void 
DNBBasicMathVector<T>::Assign ( T* dest, size_t destSize, const T* source)
{ 
#if DNB_UNROLL_LOOPS == DNB_NO
    for (int i = 0; i < destSize; i ++) dest[i] = source[i];
#else

    int noOfLoops = (int)destSize / 4;                         
    int leftover = (int)destSize % 4;                          
    int last = noOfLoops*4;                                   
    for (int i = 0; i < last; i += 4)                          
    {                                                         
            dest[i] = source[i];                              
            dest[i+1] = source[i+1];                          
            dest[i+2] = source[i+2];                          
            dest[i+3] = source[i+3];                          
    }                                                         
    switch ( leftover )                                       
    {                                                         
        case 3:                                               
            dest[i] = source[i];                              
            ++i;                                              
            /* Fall Through */                                
        case 2:                                               
            dest[i] = source[i];                              
            ++i;                                              
            /* Fall Through */                                
        case 1:                                               
            dest[i] = source[i];                              
            ++i;                                              
    }                                                                   
#endif
}

template <class T>
void 
DNBBasicMathVector<T>::Assign ( T* dest, size_t destSize, const T& value)
{ 
#if DNB_UNROLL_LOOPS == DNB_NO
    for (int i = 0; i < destSize; i ++) dest[i] = value;
#else

    int noOfLoops = (int)destSize / 4;                         
    int leftover = (int)destSize % 4;                          
    int last = noOfLoops*4;                                   
    for (int i = 0; i < last; i += 4)                          
    {                                                         
            dest[i] = value;                              
            dest[i+1] = value;                          
            dest[i+2] = value;                          
            dest[i+3] = value;                          
    }                                                         
    switch ( leftover )                                       
    {                                                         
        case 3:                                               
            dest[i] = value;                              
            ++i;                                              
            /* Fall Through */                                
        case 2:                                               
            dest[i] = value;                              
            ++i;                                              
            /* Fall Through */                                
        case 1:                                               
            dest[i] = value;                              
            ++i;                                              
    }                                
#endif
}  



//***********************************************************************************
// Global Mathematical Operators
//***********************************************************************************

/**
  * Returns the additive inverse of the argument vector 
  */
template <class T>
DNBBasicMathVector<T>    
                          operator-(const DNBBasicMathVector<T>& v)
                          DNB_THROW_SPEC((scl_bad_alloc))
{
    return DNBBasicMathVector<T>(v, _DNBSubOp());    
}


/** 
 * returns a new vector where each element is the sum of the two corresponding
 * elements of the two input vectors
 */
template <class T>
DNBBasicMathVector<T>    
                          operator+(const DNBBasicMathVector<T>& left,
                                    const DNBBasicMathVector<T>& right)
                          DNB_THROW_SPEC((scl_bad_alloc))
{
    DNB_PRECONDITION ( left.length() == right.length());
    return DNBBasicMathVector<T>(left, right, _DNBAddOp());

}

/** 
 * returns a new vector where each element is the difference of the two corresponding
 * elements of the two input vectors ( first input vector - second input vector)
 */
template <class T>
DNBBasicMathVector<T>
                        operator-(const DNBBasicMathVector<T>& left,
                                  const DNBBasicMathVector<T>& right)
                        DNB_THROW_SPEC((scl_bad_alloc))
{
    DNB_PRECONDITION ( left.length() == right.length());
    return DNBBasicMathVector<T>(left, right, _DNBSubOp());
}

/** 
 * returns a new vector where each element is the product of the two corresponding
 * elements of the two input vectors
 */
template <class T>
DNBBasicMathVector<T>    operator*(const DNBBasicMathVector<T>& left,
                                   const DNBBasicMathVector<T>& right)
                         DNB_THROW_SPEC((scl_bad_alloc))
{
    DNB_PRECONDITION ( left.length() == right.length());
    return DNBBasicMathVector<T>(left, right, _DNBMulOp());

}

/** 
 * returns a new vector where each element is the ratio of the two corresponding
 * elements of the two input vectors (firs input vector element/ second input vector element)
 */
template <class T>
DNBBasicMathVector<T>    operator/(const DNBBasicMathVector<T>& left,
                                   const DNBBasicMathVector<T>& right)
                         DNB_THROW_SPEC((scl_bad_alloc))
{
    DNB_PRECONDITION ( left.length() == right.length());
    DNB_PRECONDITION ( DNBMinAbsValue(right) != 0);  // none of the elements is 0         
    return DNBBasicMathVector<T>(left, right, _DNBDivOp());

}

/** 
 * returns a new vector where each element is the sum of corresponding
 * element of the input vector and the input constant
 */
template <class T>
DNBBasicMathVector<T>    operator+(const DNBBasicMathVector<T>& left,
                                   const T& aVal)
                         DNB_THROW_SPEC((scl_bad_alloc))
{
    return DNBBasicMathVector<T>(left, aVal, _DNBAddOp());

}

/** 
 * returns a new vector where each element is the sum of corresponding
 * element of the input vector and the input constant
 */
template <class T>
DNBBasicMathVector<T>    operator+(const T& aVal,
                                   const DNBBasicMathVector<T>& right)
                         DNB_THROW_SPEC((scl_bad_alloc))
{
    return DNBBasicMathVector<T>(right, aVal, _DNBAddOp());

}

/** 
 * returns a new vector where each element is the differnece of corresponding
 * element of the input vector and the input constant
 */
template <class T>
DNBBasicMathVector<T>    operator-(const DNBBasicMathVector<T>& left,
                                   const T& aVal)
                         DNB_THROW_SPEC((scl_bad_alloc))
{
    return DNBBasicMathVector<T>(left, aVal, _DNBSubOp());

}

/** 
 * returns a new vector where each element is the difference of corresponding
 * element of the input vector and the input constant
 */
template <class T>
DNBBasicMathVector<T>    operator-(const T& aVal,
                                   const DNBBasicMathVector<T>& right)
                         DNB_THROW_SPEC((scl_bad_alloc))
{
    return DNBBasicMathVector<T>(aVal, right, _DNBSubOp());

}

/** 
 * returns a new vector where each element is the product of corresponding
 * element of the input vector and the input constant
 */
template <class T>
DNBBasicMathVector<T>    operator*(const DNBBasicMathVector<T>& left,
                                   const T& aVal)
                         DNB_THROW_SPEC((scl_bad_alloc))
{
    return DNBBasicMathVector<T>(left, aVal, _DNBMulOp());
}

/** 
 * returns a new vector where each element is the product of corresponding
 * element of the input vector and the input constant
 */
template <class T>
DNBBasicMathVector<T>    operator*(const T& aVal,
                                   const DNBBasicMathVector<T>& right)
                         DNB_THROW_SPEC((scl_bad_alloc))
{
    return DNBBasicMathVector<T>(right, aVal, _DNBMulOp());

}

/** 
 * returns a new vector where each element is the ratio of corresponding
 * element of the input vector and the input constant
 */
template <class T>
DNBBasicMathVector<T>    operator/(const DNBBasicMathVector<T>& left,
                                   const T& aVal)
                         DNB_THROW_SPEC((scl_bad_alloc))
{
    DNB_PRECONDITION ( aVal != 0 );
    return DNBBasicMathVector<T>(left, aVal, _DNBDivOp());
}

/** 
 * returns a new vector where each element is the ratio of the input constant to the
 * corresponding element of the input vector
 */
template <class T>
DNBBasicMathVector<T>    
operator/(const T& aVal,
          const DNBBasicMathVector<T>& right)
DNB_THROW_SPEC((scl_bad_alloc))
{
    DNB_PRECONDITION ( DNBMinAbsValue(right) != 0);  // none of the elements is 0         
    return DNBBasicMathVector<T>(aVal, right, _DNBDivOp());
}



/**
 * Returns the dot product of the two vectors
 */
template <class T>
T 
DNBDot (const DNBBasicMathVector<T>& left ,
        const DNBBasicMathVector<T>& right)
DNB_THROW_SPEC_NULL
{
    T result = 0;
    size_t size_ = left.length();
    DNB_PRECONDITION ( size_ == right.length());
    for (int i = 0; i < size_; i++)
        result += left[i]*right[i];
    return result;
}

/**
  * Returns the maximum absolute value of all elements
  */
template <class T>
T
DNBLinfNorm( const DNBBasicMathVector<T> &vec)
    DNB_THROW_SPEC_NULL
{
    size_t size = vec.length();
    DNB_PRECONDITION(size != 0);
    T maxAbs = 0;
    T temp;
    for (int i = 0; i <size; i++)
    {
      temp = abs(vec[i]);
      if (temp > maxAbs ) 
      {
        maxAbs = temp;
      }
    }
    return maxAbs;
}

/**
  * Returns the minimum absolute value of all elements
  */
template <class T>
T
DNBMinAbsValue(const DNBBasicMathVector<T> &vec)
    DNB_THROW_SPEC_NULL
{
    size_t size = vec.length();
    DNB_PRECONDITION(size != 0);
    T minAbs = DNBLinfNorm(vec);
    T temp;
    for (int i = 0; i<size; i++)
    {
        temp = abs(vec[i]);
        if ( temp < minAbs) minAbs = temp;
    }
    return minAbs;
}


/**
  * Returns the sum of the absolute values of all elements.
  */
template <class T>
T
DNBL1Norm( const DNBBasicMathVector<T> &vec)
    DNB_THROW_SPEC_NULL
{
    T result = 0;
    int size = vec.length();
    for (int i = 0; i< size; i++)
        result += abs(vec[i]);
    return result;
}



/***********************************************************************************
* Global Streaming in and out
**********************************************************************************/

/**
 * Outputs a vector v to ostream s beginning with a left bracket [ and terminating 
 * with a right bracket ]. The output numbers are separated by tabs. 
 *If T is a user-defined type, the following operator must be defined:
 *          operator<<(ostream&s, const T& t);
 */
template <class T>
ostream&    
operator<<(ostream& s, const DNBBasicMathVector<T>& v) DNB_THROW_SPEC_NULL
{
    int n = v.length();
    s << "[ ";
    for (int i = 0; i < n; i++)         // since our vectors are "small", we do not
                                        // worry about line-wrapping
    {
        if (DNBIsNaN(v[i])) 
            s << "NaN   "; 
        else
        s << v[i] << "  ";
    }
    s << "]\n"; 
    return s;
}



template <class T>
bool
DNBNearAbs( const DNBBasicMathVector<T> &left, const DNBBasicMathVector<T> &right,
    const T &tolerance )
    DNB_THROW_SPEC_NULL
{
    if ( tolerance <= T(0) )
        return ( left.operator==(right) );
    DNBBasicMathVector<T> diff = left - right;
    T   normError = DNBLinfNorm(diff);
    return ( normError <= tolerance );
}



template <class T>
bool
DNBNearRel( const DNBBasicMathVector<T> &left, const DNBBasicMathVector<T> &right,
    const T &tolerance )
    DNB_THROW_SPEC_NULL
{
    if ( tolerance <= T(0) )
        return ( left.operator==(right) );

    T   normLeft  = DNBLinfNorm( left );
    T   normRight = DNBLinfNorm( right );
    T   normError = DNBLinfNorm( left - right );

    return ( normError <= (tolerance * scl_max(normLeft, normRight)) );
}

