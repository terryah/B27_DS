//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview czo/BPL 01:11:13
 * @quickReview MMH 03:02:24
 */

//*
//*  HISTORY:
//*  jyv         05/14/02    fix IR 346834A
//*

//*
//* FILE:
//*     DNBXform3D.cc     - principle implementation file
//*
//* MODULE:
//*     DNBBasicXform3D   - single template class
//*
//* OVERVIEW:
//*     This module defines a three-dimensional homogeneous transformation
//*     matrix.
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     pjf         09/02/97    Initial implementation
//*     jad         08/07/98    Ported to MSVC 5.0.  Added the base class
//*                             DNBMath3D.
//*     jad         09/07/98    Implemented numerous enhancements to the class.
//*     cwz         09/09/99    re-implementation.
//*     mmg         02/14/03    swapped order of == operator to check pos_ first
//*		smw			01/07/05	Removed extractors due to iostream migration to the
//*								new, standard, templated iostream, and due to migration to 64 bit
//*								builds.  Extractor for all math classes have been moved, 
//*								for easy rescucitation, if necessary,
//*								to DNBMath.tst/PrivateInterfaces/DNBMathExtractors.h
//* 
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1997-2002 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*
#include <DNBSystemBase.h>
#include <DNBSystemDefs.h>


#include <DNBCMath.h>
#include <DNBEuler3D.h>
#include <DNBQtn3D.h>


template <class T>
DNBBasicXform3D<T>::DNBBasicXform3D( )
    DNB_THROW_SPEC_NULL :
    DNBMath3D( ),
    rot_( ),
    pos_( )
{
    //Nothing
}


template <class T>
DNBBasicXform3D<T>::DNBBasicXform3D( const DNBBasicXform3D<T> &right )
    DNB_THROW_SPEC_NULL :
    DNBMath3D( ),
    rot_( right.rot_ ),
    pos_( right.pos_ )
{
    //Nothing
}


template <class T>
DNBBasicXform3D<T>::DNBBasicXform3D( UninitializedType dummy )
    DNB_THROW_SPEC_NULL :
    DNBMath3D( dummy ),
    rot_( dummy ),
    pos_( dummy )
{
    //Nothing
}


template <class T>
DNBBasicXform3D<T>::DNBBasicXform3D( const T &theta, const T &d,
				     const T &alpha, const T &a )
    DNB_THROW_SPEC_NULL :
    DNBMath3D( ),
    rot_( Uninitialized ),
    pos_( Uninitialized )
{
    setDH( theta, d, alpha, a );
}


template <class T>
DNBBasicXform3D<T>::DNBBasicXform3D( const T (*right)[4], bool transposed )
    DNB_THROW_SPEC((DNBESingularMatrix, DNBEIllegalMatrix)) :
    DNBMath3D( ),
    rot_( Uninitialized ),
    pos_( Uninitialized )
{
    set( right, transposed );
}


template <class T>
DNBBasicXform3D<T>::DNBBasicXform3D( const DNBBasicMathMatrix<T> &right, bool transposed )
    DNB_THROW_SPEC((DNBESingularMatrix, DNBEIllegalMatrix)) :
    DNBMath3D( ),
    rot_( Uninitialized ),
    pos_( Uninitialized )
{
    set( right, transposed );
}


template <class T>
DNBBasicXform3D<T>::~DNBBasicXform3D( )
    DNB_THROW_SPEC_NULL
{
    // Nothing
}


template <class T>
void
DNBBasicXform3D<T>::getRotate( DNBBasicRotate3D<T> &rotate ) const
    DNB_THROW_SPEC_NULL
{
    rotate = rot_;
}


template <class T>
void
DNBBasicXform3D<T>::getQtn( DNBBasicQtn3D<T> &qtn ) const
    DNB_THROW_SPEC_NULL
{
    qtn.setRotate( rot_ );
}


template <class T>
void
DNBBasicXform3D<T>::getNSA( DNBBasicVector3D<T> &vecN,
                            DNBBasicVector3D<T> &vecS,
                            DNBBasicVector3D<T> &vecA ) const
    DNB_THROW_SPEC_NULL
{
    rot_.getNSA( vecN, vecS, vecA );
}


template <class T>
void
DNBBasicXform3D<T>::getEAA( DNBBasicVector3D<T> &vec, T &angle ) const
    DNB_THROW_SPEC_NULL
{
    rot_.getEAA( vec, angle );
}


template <class T>
void
DNBBasicXform3D<T>::getRPY( T &roll, T &pitch, T &yaw ) const
    DNB_THROW_SPEC_NULL
{
    rot_.getRPY( roll, pitch, yaw );
}


template <class T>
void
DNBBasicXform3D<T>::getPosition( DNBBasicVector3D<T> &vectorP ) const
    DNB_THROW_SPEC_NULL
{
    vectorP = pos_;
}


template <class T>
void
DNBBasicXform3D<T>::get( T (*right)[4], bool transposed ) const
    DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION( right != NULL );

    size_t row;
    size_t col;

    if( transposed )
    {
        for( row = 0; row < 3; row++ )
	{
	    for( col = 0; col < 3; col++ )
	    {
		right[row][col] = rot_.data_[row][col];
	    }
	}

	right[3][0] = pos_[0];
	right[3][1] = pos_[1];
	right[3][2] = pos_[2];

	right[0][3] = T(0);
	right[1][3] = T(0);
	right[2][3] = T(0);
	right[3][3] = T(1);
    }
    else
    {
        for( row = 0; row < 3; row++ )
	{
	    for( col = 0; col < 3; col++ )
	    {
		right[col][row] = rot_.data_[row][col];
	    }
	}

	right[0][3] = pos_[0];
	right[1][3] = pos_[1];
	right[2][3] = pos_[2];

	right[3][0] = T(0);
	right[3][1] = T(0);
	right[3][2] = T(0);
	right[3][3] = T(1);
    }
}


template <class T>
void
DNBBasicXform3D<T>::get( T *right ) const
    DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION( right != NULL );

    size_t row;
    size_t col;


    for ( row = 0; row < 3; row++ )
    {
        for ( col = 0; col < 3; col++ )
        {
            right[3*row + col] = rot_.data_[row][col];
        }
    }

    right[9] = pos_[0];;
    right[10] = pos_[1];
    right[11] = pos_[2];

}


template <class T>
void
DNBBasicXform3D<T>::get( DNBBasicMathMatrix<T> &right, bool transposed ) const
    DNB_THROW_SPEC_NULL
{
    DNB_PRECONDITION( right.rows() == 4 );
    DNB_PRECONDITION( right.cols() == 4 );

    size_t row;
    size_t col;

    if( transposed )
    {
        for( row = 0; row < 3; row++ )
	{
	    for( col = 0; col < 3; col++ )
	    {
		right( row, col ) = rot_.data_[row][col];
	    }
	}

	right( 3, 0 ) = pos_[0];
	right( 3, 1 ) = pos_[1];
	right( 3, 2 ) = pos_[2];

	right( 0, 3 ) = T(0);
	right( 1, 3 ) = T(0);
	right( 2, 3 ) = T(0);
	right( 3, 3 ) = T(1);

    }
    else
    {
        for( row = 0; row < 3; row++ )
	{
	    for( col = 0; col < 3; col++ )
	    {
		right( col, row ) = rot_.data_[row][col];
	    }
	}

	right( 0, 3 ) = pos_[0];
	right( 1, 3 ) = pos_[1];
	right( 2, 3 ) = pos_[2];

	right( 3, 0 ) = T(0);
	right( 3, 1 ) = T(0);
	right( 3, 2 ) = T(0);
	right( 3, 3 ) = T(1);
    }
}


template <class T>
void
DNBBasicXform3D<T>::setRotate( const DNBBasicRotate3D<T> &rotate )
    DNB_THROW_SPEC_NULL
{
    rot_ = rotate;
}


template <class T>
void
DNBBasicXform3D<T>::setQtn( const DNBBasicQtn3D<T> &qtn )
    DNB_THROW_SPEC_NULL
{
    qtn.getRotate( rot_ );
}


template <class T>
void
DNBBasicXform3D<T>::setNSA( const DNBBasicVector3D<T> &vecN,
                            const DNBBasicVector3D<T> &vecS,
                            const DNBBasicVector3D<T> &vecA )
    DNB_THROW_SPEC((DNBESingularMatrix, DNBEIllegalMatrix))
{
    rot_.setNSA( vecN, vecS, vecA );
}


template <class T>
void
DNBBasicXform3D<T>::setEAA( const DNBBasicVector3D<T> &axis, const T &angle )
    DNB_THROW_SPEC((DNBEZeroVector))
{
    rot_.setEAA( axis, angle );
}


template <class T>
void
DNBBasicXform3D<T>::setCAA( AxisType axis, const T &angle )
    DNB_THROW_SPEC_NULL
{
    rot_.setCAA( axis, angle );
}


template <class T>
void
DNBBasicXform3D<T>::setEuler( const DNBBasicEuler3D<T> &euler)
    DNB_THROW_SPEC_NULL
{
    rot_.setEuler( euler );
}


template <class T>
void
DNBBasicXform3D<T>::setRPY( const T &roll, const T &pitch, const T &yaw )
    DNB_THROW_SPEC_NULL
{
    rot_.setRPY( roll, pitch, yaw );
}


template <class T>
void
DNBBasicXform3D<T>::setPosition( const DNBBasicVector3D<T> &vecP )
    DNB_THROW_SPEC_NULL
{
    pos_ = vecP;
}


template <class T>
void
DNBBasicXform3D<T>::setDH( const T &theta, const T &d,
			   const T &alpha, const T &a )
    DNB_THROW_SPEC_NULL
{
    T sinTheta, cosTheta, sinAlpha, cosAlpha;
    DNBSinCos( theta, sinTheta, cosTheta );
    DNBSinCos( alpha, sinAlpha, cosAlpha );

    rot_.data_[N][X] = cosTheta;
    rot_.data_[N][Y] = sinTheta;
    rot_.data_[N][Z] = T(0);

    rot_.data_[S][X] = - sinTheta * cosAlpha;
    rot_.data_[S][Y] = cosTheta * cosAlpha;
    rot_.data_[S][Z] = sinAlpha;

    rot_.data_[A][X] = sinTheta * sinAlpha;
    rot_.data_[A][Y] = - cosTheta * sinAlpha;
    rot_.data_[A][Z] = cosAlpha;

    pos_.data_[X] = a * cosTheta;
    pos_.data_[Y] = a * sinTheta;
    pos_.data_[Z] = d;
}


template <class T>
void
DNBBasicXform3D<T>::set( const T (*right)[4], bool transposed )
    DNB_THROW_SPEC((DNBESingularMatrix, DNBEIllegalMatrix))
{
    DNB_PRECONDITION( right != NULL );

    size_t row;
    size_t col;

    if( transposed )
    {
        DNB_PRECONDITION( right[0][3] == T(0) );
        DNB_PRECONDITION( right[1][3] == T(0) );
        DNB_PRECONDITION( right[2][3] == T(0) );
        DNB_PRECONDITION( right[3][3] == T(1) );

        for ( row = 0; row < 3; row++ )
        {
            for ( col = 0; col < 3; col++ )
            {
                rot_.data_[row][col] = right[row][col];
            }
        }

	pos_[0] = right[3][0];
	pos_[1] = right[3][1];
	pos_[2] = right[3][2];
    }
    else
    {
        DNB_PRECONDITION( right[3][0] == T(0) );
        DNB_PRECONDITION( right[3][1] == T(0) );
        DNB_PRECONDITION( right[3][2] == T(0) );
        DNB_PRECONDITION( right[3][3] == T(1) );

        for ( row = 0; row < 3; row++ )
        {
            for ( col = 0; col < 3; col++ )
            {
                rot_.data_[row][col] = right[col][row];
            }
        }

	pos_[0] = right[0][3];
	pos_[1] = right[1][3];
	pos_[2] = right[2][3];
    }

    normalize( );
}


template <class T>
void
DNBBasicXform3D<T>::set( const T *right )
    DNB_THROW_SPEC((DNBESingularMatrix, DNBEIllegalMatrix))
{
    DNB_PRECONDITION( right != NULL );

    size_t row, col;


    for ( row = 0; row < 3; row++ )
    {
        for ( col = 0; col < 3; col++ )
        {
            rot_.data_[row][col] = right[3*row + col];
        }
    }

    pos_[0] = right[9];
    pos_[1] = right[10];
    pos_[2] = right[11];

    normalize( );

}


template <class T>
void
DNBBasicXform3D<T>::set( const DNBBasicMathMatrix<T> &right, bool transposed )
    DNB_THROW_SPEC((DNBESingularMatrix, DNBEIllegalMatrix))
{
    DNB_PRECONDITION( right.rows() == 4 );
    DNB_PRECONDITION( right.cols() == 4 );

    size_t      row;
    size_t      col;

    if ( transposed )
    {
        DNB_PRECONDITION( right( 0, 3 ) == T(0) );
        DNB_PRECONDITION( right( 1, 3 ) == T(0) );
        DNB_PRECONDITION( right( 2, 3 ) == T(0) );
        DNB_PRECONDITION( right( 3, 3 ) == T(1) );

        for( row = 0; row < 3; row++ )
        {
            for( col = 0; col < 3; col++ )
            {
                rot_.data_[row][col] = right( row, col );
            }
        }

	pos_[0] = right( 3, 0 );
	pos_[1] = right( 3, 1 );
	pos_[2] = right( 3, 2 );
    }
    else
    {
        DNB_PRECONDITION( right( 3, 0 ) == T(0) );
        DNB_PRECONDITION( right( 3, 1 ) == T(0) );
        DNB_PRECONDITION( right( 3, 2 ) == T(0) );
        DNB_PRECONDITION( right( 3, 3 ) == T(1) );

        for( row = 0; row < 3; row++ )
        {
            for( col = 0; col < 3; col++ )
            {
                rot_.data_[row][col] = right( col, row );
            }
        }

	pos_[0] = right( 0, 3 );
	pos_[1] = right( 1, 3 );
	pos_[2] = right( 2, 3 );
    }

    normalize( );
}


template <class T>
void
DNBBasicXform3D<T>::swap( DNBBasicXform3D<T> &other )
    DNB_THROW_SPEC_NULL
{
    DNBBasicXform3D<T>     tmp( *this );
    *this = other;
    other = tmp;
}


template <class T>
void
DNBBasicXform3D<T>::normalize( )
    DNB_THROW_SPEC((DNBESingularMatrix, DNBEIllegalMatrix))
{
    rot_.normalize( );
}


template <class T>
void
DNBBasicXform3D<T>::translate( const DNBBasicVector3D<T> &deltaP,
                               ReferenceFrame refFrame )
    DNB_THROW_SPEC_NULL
{
    switch ( refFrame )
    {
        case Origin:
            pos_.data_[X] += deltaP.data_[X];
            pos_.data_[Y] += deltaP.data_[Y];
            pos_.data_[Z] += deltaP.data_[Z];
            break;

        case Local:
            pos_.data_[X] += rot_.data_[N][X] * deltaP.data_[X] +
                             rot_.data_[S][X] * deltaP.data_[Y] +
                             rot_.data_[A][X] * deltaP.data_[Z];

            pos_.data_[Y] += rot_.data_[N][Y] * deltaP.data_[X] +
                             rot_.data_[S][Y] * deltaP.data_[Y] +
                             rot_.data_[A][Y] * deltaP.data_[Z];

            pos_.data_[Z] += rot_.data_[N][Z] * deltaP.data_[X] +
                             rot_.data_[S][Z] * deltaP.data_[Y] +
                             rot_.data_[A][Z] * deltaP.data_[Z];
            break;

        default:
            DNB_PROGRAM_ERROR( "Invalid reference frame" );
            break;
    }
}


template <class T>
void
DNBBasicXform3D<T>::rotate( AxisType axis, const T &angle,
			    ReferenceFrame refFrame )
    DNB_THROW_SPEC_NULL
{
    DNB_ASSERT( ( axis >= 0 ) && ( axis <= 2 ) );

    T cosAngle, sinAngle;
    DNBSinCos( angle, sinAngle, cosAngle );

    int i = ( axis + 1 ) % 3;
    int j = ( i + 1 ) % 3;

    T tmp0, tmp1, tmp2;

    switch( refFrame )
    {
	case Origin:
	    tmp0 = cosAngle * rot_.data_[N][i] - sinAngle * rot_.data_[N][j];
	    tmp1 = cosAngle * rot_.data_[S][i] - sinAngle * rot_.data_[S][j];
	    tmp2 = cosAngle * rot_.data_[A][i] - sinAngle * rot_.data_[A][j];

            rot_.data_[N][j] = sinAngle * rot_.data_[N][i] +
			       cosAngle * rot_.data_[N][j];
            rot_.data_[S][j] = sinAngle * rot_.data_[S][i] +
			       cosAngle * rot_.data_[S][j];
	    rot_.data_[A][j] = sinAngle * rot_.data_[A][i] +
			       cosAngle * rot_.data_[A][j];

            rot_.data_[N][i] = tmp0;
	    rot_.data_[S][i] = tmp1;
	    rot_.data_[A][i] = tmp2;

            tmp0 = cosAngle * pos_.data_[i] - sinAngle * pos_.data_[j];
	    tmp1 = sinAngle * pos_.data_[i] + cosAngle * pos_.data_[j];

	    pos_.data_[i] = tmp0;
	    pos_.data_[j] = tmp1;

            break;

	case Local:
            tmp0 = cosAngle * rot_.data_[i][X] + sinAngle * rot_.data_[j][X];
            tmp1 = cosAngle * rot_.data_[i][Y] + sinAngle * rot_.data_[j][Y];
	    tmp2 = cosAngle * rot_.data_[i][Z] + sinAngle * rot_.data_[j][Z];

            rot_.data_[j][X] = cosAngle * rot_.data_[j][X] -
			       sinAngle * rot_.data_[i][X];
	    rot_.data_[j][Y] = cosAngle * rot_.data_[j][Y] -
			       sinAngle * rot_.data_[i][Y];
	    rot_.data_[j][Z] = cosAngle * rot_.data_[j][Z] -
			       sinAngle * rot_.data_[i][Z];

            rot_.data_[i][X] = tmp0;
	    rot_.data_[i][Y] = tmp1;
	    rot_.data_[i][Z] = tmp2;

            break;

	default:
            DNB_PROGRAM_ERROR( "Invalid reference frame" );
            break;
    }
}


template <class T>
void
DNBBasicXform3D<T>::rotate( const DNBBasicVector3D<T> &vec, const T &angle,
                            ReferenceFrame refFrame )
    DNB_THROW_SPEC((DNBEZeroVector))
{
    DNBBasicRotate3D<T> tmp( Uninitialized );
    tmp.setEAA( vec, angle );

    switch( refFrame )
    {
	case Origin:
	    rot_.preMultiply( tmp );
	    tmp.rotateVec( pos_ );
	    break;

        case Local:
	    rot_.postMultiply( tmp );
	    break;

        default:
            DNB_PROGRAM_ERROR( "Invalid reference frame" );
            break;
    }

}


template <class T>
void
DNBBasicXform3D<T>::move( const DNBBasicXform3D<T> &xform, ReferenceFrame refFrame )
    DNB_THROW_SPEC_NULL
{
    switch ( refFrame )
    {
        case Origin:
            preMultiply( xform );
            break;

        case Local:
            postMultiply( xform );
            break;

        default:
            DNB_PROGRAM_ERROR( "Invalid reference frame" );
            break;
    }
}


template <class T>
void
DNBBasicXform3D<T>::rotateVec( DNBBasicVector3D<T> & vec ) const
    DNB_THROW_SPEC_NULL
{
    DNBBasicVector3D<T> result( Uninitialized );
    multiply( result, *this, vec );
    vec = result;
}


template <class T>
void
DNBBasicXform3D<T>::preMultiply( const DNBBasicXform3D<T> &left )
    DNB_THROW_SPEC_NULL
{
    DNBBasicXform3D<T>     result( Uninitialized );
    multiply( result, left, *this );
    *this = result;
}


template <class T>
void
DNBBasicXform3D<T>::postMultiply( const DNBBasicXform3D<T> &right )
    DNB_THROW_SPEC_NULL
{
    DNBBasicXform3D<T>     result( Uninitialized );
    multiply( result, *this, right );
    *this = result;
}


template <class T>
void
DNBBasicXform3D<T>::invert( )
    DNB_THROW_SPEC((DNBESingularMatrix))
{
    //
    //  Compute the new position vector.
    //
    T   px = rot_.data_[ N ][ X ] * pos_.data_[ X ] +
             rot_.data_[ N ][ Y ] * pos_.data_[ Y ] +
             rot_.data_[ N ][ Z ] * pos_.data_[ Z ];

    T   py = rot_.data_[ S ][ X ] * pos_.data_[ X ] +
             rot_.data_[ S ][ Y ] * pos_.data_[ Y ] +
             rot_.data_[ S ][ Z ] * pos_.data_[ Z ];

    T   pz = rot_.data_[ A ][ X ] * pos_.data_[ X ] +
             rot_.data_[ A ][ Y ] * pos_.data_[ Y ] +
             rot_.data_[ A ][ Z ] * pos_.data_[ Z ];

    pos_.data_[ X ] = -px;
    pos_.data_[ Y ] = -py;
    pos_.data_[ Z ] = -pz;

    //
    //  Compute the transpose of the direction cosine matrix.
    //
    T   temp;

    temp                 = rot_.data_[ N ][ Y ];
    rot_.data_[ N ][ Y ] = rot_.data_[ S ][ X ];
    rot_.data_[ S ][ X ] = temp;

    temp                 = rot_.data_[ N ][ Z ];
    rot_.data_[ N ][ Z ] = rot_.data_[ A ][ X ];
    rot_.data_[ A ][ X ] = temp;

    temp                 = rot_.data_[ S ][ Z ];
    rot_.data_[ S ][ Z ] = rot_.data_[ A ][ Y ];
    rot_.data_[ A ][ Y ] = temp;
}


template <class T>
DNBBasicVector3D<T>
DNBBasicXform3D<T>::operator()( DirectionIndex dirIndex ) const
    DNB_THROW_SPEC_NULL
{
    return rot_( dirIndex );
}


template <class T>
DNBBasicVector3D<T>
DNBBasicXform3D<T>::operator()( PositionIndex posIndex ) const
    DNB_THROW_SPEC_NULL
{
    return pos_;
}


template <class T>
T
DNBBasicXform3D<T>::operator()( DirectionIndex dirIndex,
                                CoordinateIndex coordIndex ) const
    DNB_THROW_SPEC_NULL
{
    return rot_.data_[dirIndex][coordIndex];
}


template <class T>
T
DNBBasicXform3D<T>::operator()( PositionIndex posIndex,
                                CoordinateIndex coordIndex ) const
    DNB_THROW_SPEC_NULL
{
    return pos_.data_[coordIndex];
}


template <class T>
DNBBasicXform3D<T> &
DNBBasicXform3D<T>::operator=( const DNBBasicXform3D<T> &right )
    DNB_THROW_SPEC_NULL
{
    if( this != &right )               // Prevent self-assignment.
    {
	rot_ = right.rot_;
	pos_ = right.pos_;
    }

    return *this;
}


template <class T>
bool
operator==( const DNBBasicXform3D<T> &left, const DNBBasicXform3D<T> &right )
    DNB_THROW_SPEC_NULL
{
    return ( left.pos_ == right.pos_ ) && ( left.rot_ == right.rot_ );
}


template <class T>
bool
operator!=( const DNBBasicXform3D<T> &left, const DNBBasicXform3D<T> &right )
    DNB_THROW_SPEC_NULL
{
    return ( left.pos_ != right.pos_ ) || ( left.rot_ != right.rot_ );
}


template <class T>
DNBBasicMathMatrix<T>
DNBBasicXform3D<T>::toDNBBasicMathMatrix() const
    DNB_THROW_SPEC((scl_bad_alloc))
{
    DNBBasicMathMatrix<T>     result( 4, 4, Uninitialized );
    get( result, false );

    return result;
}


template <class T>
DNBBasicXform3D<T> &
DNBBasicXform3D<T>::operator*=( const DNBBasicXform3D<T> &right )
    DNB_THROW_SPEC_NULL
{
    DNBBasicXform3D<T>    result( Uninitialized );
    multiply( result, *this, right );
    *this = result;

    return *this;
}


template <class T>
DNBBasicXform3D<T> &
DNBBasicXform3D<T>::operator/=( const DNBBasicXform3D<T> &right )
    DNB_THROW_SPEC((DNBESingularMatrix))
{
    DNBBasicXform3D<T>    result( Uninitialized );
    divide( result, *this, right );
    *this = result;

    return *this;
}


template <class T>
DNBBasicXform3D<T>
operator*( const DNBBasicXform3D<T> &left, const DNBBasicXform3D<T> &right )
    DNB_THROW_SPEC_NULL
{
    DNBBasicXform3D<T>    result( DNBMath3D::Uninitialized );
    DNBBasicXform3D<T>::multiply( result, left, right );

    return result;
}


template <class T>
DNBBasicXform3D<T>
operator/( const DNBBasicXform3D<T> &left, const DNBBasicXform3D<T> &right )
    DNB_THROW_SPEC((DNBESingularMatrix))
{
    DNBBasicXform3D<T>    result( DNBMath3D::Uninitialized );
    DNBBasicXform3D<T>::divide( result, left, right );

    return result;
}


template <class T>
DNBBasicVector3D<T>
operator*( const DNBBasicXform3D<T> &left, const DNBBasicVector3D<T> &right )
    DNB_THROW_SPEC_NULL
{
    DNBBasicVector3D<T>    result;
    DNBBasicXform3D<T>::multiply( result, left, right );

    return result;
}


template <class T>
ostream &
operator<<( ostream &ostr, const DNBBasicXform3D<T> &item )
    DNB_THROW_SPEC((scl_bad_alloc))
{
    DNBBasicMathMatrix<T>    matItem( 4, 4);
    item.get( matItem, false );

    return (ostr << matItem);
}


template <class T>
bool
DNBNearRel( const DNBBasicXform3D<T> &left, const DNBBasicXform3D<T> &right,
    const T &angularTol, const T &linearTol )
    DNB_THROW_SPEC_NULL
{
    return DNBNearRel( left.rot_, right.rot_, angularTol, linearTol ) &&
	   DNBNearRel( left.pos_, right.pos_, linearTol );
}


template <class T>
bool
DNBNearAbs( const DNBBasicXform3D<T> &left, const DNBBasicXform3D<T> &right,
    const T &angularTol, const T &linearTol )
    DNB_THROW_SPEC_NULL
{
    return DNBNearAbs( left.rot_, right.rot_, angularTol, linearTol ) &&
	   DNBNearAbs( left.pos_, right.pos_, linearTol );
}

//#ifndef __HP_aCC

template <class T>
void
DNBBasicXform3D<T>::EAAInterp( DNBBasicXform3D<T> &res, const T &alpha,
           const DNBBasicXform3D<T> &from, const DNBBasicXform3D<T> &to )
    DNB_THROW_SPEC_NULL
{
    DNBBasicQtn3D<T>    fromQtn, toQtn, qtn;
    DNBBasicVector3D<T> fromVec, toVec, vec;

    from.getQtn( fromQtn );
    from.getPosition( fromVec );
    to.getQtn( toQtn );
    to.getPosition( toVec );

    DNBBasicQtn3D<T>::EAAInterp( qtn, alpha, fromQtn, toQtn );
    DNBBasicVector3D<T>::lerp( vec, alpha, fromVec, toVec );

    res.setQtn( qtn );
    res.setPosition( vec );
}


template <class T>
void
DNBBasicXform3D<T>::slerp( DNBBasicXform3D<T> &res, const T &alpha,
           const DNBBasicXform3D<T> &from, const DNBBasicXform3D<T> &to )
    DNB_THROW_SPEC_NULL
{
    DNBBasicQtn3D<T>    fromQtn, toQtn, resQtn;
    DNBBasicVector3D<T> fromVec, toVec, resVec;

    from.getQtn( fromQtn );
    from.getPosition( fromVec );
    to.getQtn( toQtn );
    to.getPosition( toVec );

    DNBBasicQtn3D<T>::slerp( resQtn, alpha, fromQtn, toQtn );
    DNBBasicVector3D<T>::lerp( resVec, alpha, fromVec, toVec );

    res.setQtn( resQtn );
    res.setPosition( resVec );
}


template <class T>
const DNBBasicXform3D<T>    DNBBasicXform3D<T>::Identity;

//#endif // __HP_aCC

template <class T>
void
DNBBasicXform3D<T>::multiply( DNBBasicXform3D<T> &result,
                              const DNBBasicXform3D<T> &left,
                              const DNBBasicXform3D<T> &right )
    DNB_THROW_SPEC_NULL
{
    result.rot_.data_[N][X] = left.rot_.data_[N][X] * right.rot_.data_[N][X] +
			      left.rot_.data_[S][X] * right.rot_.data_[N][Y] +
			      left.rot_.data_[A][X] * right.rot_.data_[N][Z];

    result.rot_.data_[N][Y] = left.rot_.data_[N][Y] * right.rot_.data_[N][X] +
		              left.rot_.data_[S][Y] * right.rot_.data_[N][Y] +
			      left.rot_.data_[A][Y] * right.rot_.data_[N][Z];

    result.rot_.data_[N][Z] = left.rot_.data_[N][Z] * right.rot_.data_[N][X] +
			      left.rot_.data_[S][Z] * right.rot_.data_[N][Y] +
			      left.rot_.data_[A][Z] * right.rot_.data_[N][Z];

    result.rot_.data_[S][X] = left.rot_.data_[N][X] * right.rot_.data_[S][X] +
			      left.rot_.data_[S][X] * right.rot_.data_[S][Y] +
			      left.rot_.data_[A][X] * right.rot_.data_[S][Z];

    result.rot_.data_[S][Y] = left.rot_.data_[N][Y] * right.rot_.data_[S][X] +
			      left.rot_.data_[S][Y] * right.rot_.data_[S][Y] +
			      left.rot_.data_[A][Y] * right.rot_.data_[S][Z];

    result.rot_.data_[S][Z] = left.rot_.data_[N][Z] * right.rot_.data_[S][X] +
			      left.rot_.data_[S][Z] * right.rot_.data_[S][Y] +
			      left.rot_.data_[A][Z] * right.rot_.data_[S][Z];


   //
   // IR 346834A : compute the A vector by fully multiplying out the
   // two given rotation matrices. For LH coordinate system in SO(3),
   // det(R) = -1 and ixj=-k. Hence we cannot apply the optimization
   // ixj=k without computing det(R). jyv, 05/14/02.
   //

   result.rot_.data_[A][X] = left.rot_.data_[N][X] * right.rot_.data_[A][X] +
                           left.rot_.data_[S][X] * right.rot_.data_[A][Y] +
                           left.rot_.data_[A][X] * right.rot_.data_[A][Z];


   result.rot_.data_[A][Y] = left.rot_.data_[N][Y] * right.rot_.data_[A][X] +
                           left.rot_.data_[S][Y] * right.rot_.data_[A][Y] +
                           left.rot_.data_[A][Y] * right.rot_.data_[A][Z];

   result.rot_.data_[A][Z] = left.rot_.data_[N][Z] * right.rot_.data_[A][X] +
                           left.rot_.data_[S][Z] * right.rot_.data_[A][Y] +
                           left.rot_.data_[A][Z] * right.rot_.data_[A][Z];



    result.pos_.data_[X] = left.rot_.data_[N][X] * right.pos_[X] +
			   left.rot_.data_[S][X] * right.pos_[Y] +
			   left.rot_.data_[A][X] * right.pos_[Z] +
			   left.pos_[X];

    result.pos_.data_[Y] = left.rot_.data_[N][Y] * right.pos_[X] +
			   left.rot_.data_[S][Y] * right.pos_[Y] +
			   left.rot_.data_[A][Y] * right.pos_[Z] +
			   left.pos_[Y];

    result.pos_.data_[Z] = left.rot_.data_[N][Z] * right.pos_[X] +
			   left.rot_.data_[S][Z] * right.pos_[Y] +
			   left.rot_.data_[A][Z] * right.pos_[Z] +
			   left.pos_[Z];
}


template <class T>
void
DNBBasicXform3D<T>::multiply( DNBBasicVector3D<T> &result,
                              const DNBBasicXform3D<T> &left,
                              const DNBBasicVector3D<T> &right )
    DNB_THROW_SPEC_NULL
{
    result.data_[ X ] = left.rot_.data_[ N ][ X ] * right.data_[ X ] +
                        left.rot_.data_[ S ][ X ] * right.data_[ Y ] +
                        left.rot_.data_[ A ][ X ] * right.data_[ Z ] +
                        left.pos_.data_[ X ];

    result.data_[ Y ] = left.rot_.data_[ N ][ Y ] * right.data_[ X ] +
                        left.rot_.data_[ S ][ Y ] * right.data_[ Y ] +
                        left.rot_.data_[ A ][ Y ] * right.data_[ Z ] +
                        left.pos_.data_[ Y ];

    result.data_[ Z ] = left.rot_.data_[ N ][ Z ] * right.data_[ X ] +
                        left.rot_.data_[ S ][ Z ] * right.data_[ Y ] +
                        left.rot_.data_[ A ][ Z ] * right.data_[ Z ] +
                        left.pos_.data_[ Z ];
}


template <class T>
void
DNBBasicXform3D<T>::divide( DNBBasicXform3D<T> &result,
                            const DNBBasicXform3D<T> &left,
                            const DNBBasicXform3D<T> &right )
    DNB_THROW_SPEC((DNBESingularMatrix))
{
    result.rot_.data_[N][X] = left.rot_.data_[N][X] * right.rot_.data_[N][X] +
                              left.rot_.data_[S][X] * right.rot_.data_[S][X] +
                              left.rot_.data_[A][X] * right.rot_.data_[A][X];
    result.rot_.data_[N][Y] = left.rot_.data_[N][Y] * right.rot_.data_[N][X] +
                              left.rot_.data_[S][Y] * right.rot_.data_[S][X] +
                              left.rot_.data_[A][Y] * right.rot_.data_[A][X];
    result.rot_.data_[N][Z] = left.rot_.data_[N][Z] * right.rot_.data_[N][X] +
                              left.rot_.data_[S][Z] * right.rot_.data_[S][X] +
                              left.rot_.data_[A][Z] * right.rot_.data_[A][X];

    result.rot_.data_[S][X] = left.rot_.data_[N][X] * right.rot_.data_[N][Y] +
                              left.rot_.data_[S][X] * right.rot_.data_[S][Y] +
                              left.rot_.data_[A][X] * right.rot_.data_[A][Y];
    result.rot_.data_[S][Y] = left.rot_.data_[N][Y] * right.rot_.data_[N][Y] +
                              left.rot_.data_[S][Y] * right.rot_.data_[S][Y] +
                              left.rot_.data_[A][Y] * right.rot_.data_[A][Y];
    result.rot_.data_[S][Z] = left.rot_.data_[N][Z] * right.rot_.data_[N][Y] +
                              left.rot_.data_[S][Z] * right.rot_.data_[S][Y] +
                              left.rot_.data_[A][Z] * right.rot_.data_[A][Y];


    //
    // IR 346834A : compute the A vector by fully dividing left by right.
    // For LH coordinate system in SO(3), det(R) = -1 and ixj=-k. Hence we
    // cannot apply the optimization ixj=k without computing det(R).
    // jyv, 05/14/02.
    //

    result.rot_.data_[A][X] = left.rot_.data_[N][X] * right.rot_.data_[N][Z] +
                           left.rot_.data_[S][X] * right.rot_.data_[S][Z] +
                           left.rot_.data_[A][X] * right.rot_.data_[A][Z];

    result.rot_.data_[A][Y] = left.rot_.data_[N][Y] * right.rot_.data_[N][Z] +
                           left.rot_.data_[S][Y] * right.rot_.data_[S][Z] +
                           left.rot_.data_[A][Y] * right.rot_.data_[A][Z];

    result.rot_.data_[A][Z] = left.rot_.data_[N][Z] * right.rot_.data_[N][Z] +
                           left.rot_.data_[S][Z] * right.rot_.data_[S][Z] +
                           left.rot_.data_[A][Z] * right.rot_.data_[A][Z];


    result.pos_.data_[X] = left.pos_.data_[X] -
                           result.rot_.data_[N][X] * right.pos_.data_[X] -
                           result.rot_.data_[S][X] * right.pos_.data_[Y] -
                           result.rot_.data_[A][X] * right.pos_.data_[Z];

    result.pos_.data_[Y] = left.pos_.data_[Y] -
                           result.rot_.data_[N][Y] * right.pos_.data_[X] -
                           result.rot_.data_[S][Y] * right.pos_.data_[Y] -
                           result.rot_.data_[A][Y] * right.pos_.data_[Z];

    result.pos_.data_[Z] = left.pos_.data_[Z] -
                           result.rot_.data_[N][Z] * right.pos_.data_[X] -
                           result.rot_.data_[S][Z] * right.pos_.data_[Y] -
                           result.rot_.data_[A][Z] * right.pos_.data_[Z];
}
