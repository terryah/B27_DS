//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 * @fullreview czo/BPL 01:11:13
 */

//*
//*  HISTORY:
//*  jyv         05/22/02    fix IR 346834A
//*


//*
//* FILE:
//*     Rotate3D.cc        - principle implementation file
//*
//* MODULE:
//*     DNBBasicRotate3D   - single template class
//*
//* OVERVIEW:
//*     This module defines a three-dimensional rotation cosin matrix.
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     CZO         09/09/99    Initial implementation.
//*		smw			01/07/05	Removed extractors due to iostream migration to the
//*								new, standard, templated iostream, and due to migration to 64 bit
//*								builds.  Extractor for all math classes have been moved, 
//*								for easy rescucitation, if necessary,
//*								to DNBMath.tst/PrivateInterfaces/DNBMathExtractors.h
//* 
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1997-2002 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*
#include <DNBSystemBase.h>
#include <scl_limits.h>
#include <DNBSystemDefs.h>


#include <DNBEuler3D.h>


template <class T>
DNBBasicRotate3D<T>::DNBBasicRotate3D( )
    DNB_THROW_SPEC_NULL :
    DNBMath3D   ( )
{
    data_[ N ][ X ] = T( 1 );
    data_[ N ][ Y ] = T( 0 );
    data_[ N ][ Z ] = T( 0 );

    data_[ S ][ X ] = T( 0 );
    data_[ S ][ Y ] = T( 1 );
    data_[ S ][ Z ] = T( 0 );

    data_[ A ][ X ] = T( 0 );
    data_[ A ][ Y ] = T( 0 );
    data_[ A ][ Z ] = T( 1 );
}


template <class T>
DNBBasicRotate3D<T>::DNBBasicRotate3D( const DNBBasicRotate3D<T> &right )
    DNB_THROW_SPEC_NULL :
    DNBMath3D   ( )
{
    data_[ N ][ X ] = right.data_[ N ][ X ];
    data_[ N ][ Y ] = right.data_[ N ][ Y ];
    data_[ N ][ Z ] = right.data_[ N ][ Z ];

    data_[ S ][ X ] = right.data_[ S ][ X ];
    data_[ S ][ Y ] = right.data_[ S ][ Y ];
    data_[ S ][ Z ] = right.data_[ S ][ Z ];

    data_[ A ][ X ] = right.data_[ A ][ X ];
    data_[ A ][ Y ] = right.data_[ A ][ Y ];
    data_[ A ][ Z ] = right.data_[ A ][ Z ];
}


template <class T>
DNBBasicRotate3D<T>::DNBBasicRotate3D( UninitializedType dummy )
    DNB_THROW_SPEC_NULL :
    DNBMath3D   ( dummy )
{
#if DNB_VERIFY
    T nan;
    DNBSetNaN( nan );
    DNB_ASSERT( DNBIsNaN( nan ) );

    data_[ N ][ X ] = nan;
    data_[ N ][ Y ] = nan;
    data_[ N ][ Z ] = nan;

    data_[ S ][ X ] = nan;
    data_[ S ][ Y ] = nan;
    data_[ S ][ Z ] = nan;

    data_[ A ][ X ] = nan;
    data_[ A ][ Y ] = nan;
    data_[ A ][ Z ] = nan;
#endif
}


template <class T>
DNBBasicRotate3D<T>::DNBBasicRotate3D( const T** right, bool transposed )
    DNB_THROW_SPEC((DNBESingularMatrix, DNBEIllegalMatrix)) :
    DNBMath3D   ( )
{
    set( right, transposed );
}


template <class T>
DNBBasicRotate3D<T>::DNBBasicRotate3D( const DNBBasicMathMatrix<T> &right, bool transposed )
    DNB_THROW_SPEC((DNBESingularMatrix, DNBEIllegalMatrix)) :
    DNBMath3D   ( )
{
    set( right, transposed );
}


template <class T>
DNBBasicRotate3D<T>::~DNBBasicRotate3D( )
    DNB_THROW_SPEC_NULL
{
    // Nothing
}


template <class T>
void
DNBBasicRotate3D<T>::getNSA( DNBBasicVector3D<T> &vecN,
                             DNBBasicVector3D<T> &vecS,
                             DNBBasicVector3D<T> &vecA ) const
    DNB_THROW_SPEC_NULL
{
    vecN.data_[ X ] = data_[ N ][ X ];
    vecN.data_[ Y ] = data_[ N ][ Y ];
    vecN.data_[ Z ] = data_[ N ][ Z ];

    vecS.data_[ X ] = data_[ S ][ X ];
    vecS.data_[ Y ] = data_[ S ][ Y ];
    vecS.data_[ Z ] = data_[ S ][ Z ];

    vecA.data_[ X ] = data_[ A ][ X ];
    vecA.data_[ Y ] = data_[ A ][ Y ];
    vecA.data_[ Z ] = data_[ A ][ Z ];
}


template <class T>
void
DNBBasicRotate3D<T>::getEAA( DNBBasicVector3D<T> &vec, T &angle ) const
    DNB_THROW_SPEC_NULL
{
    DNBBasicQtn3D<T> qtn( Uninitialized );
    qtn.setRotate( *this );
    qtn.getEAA( vec, angle );
}


template <class T>
void
DNBBasicRotate3D<T>::getRPY( T &roll, T &pitch, T &yaw ) const
    DNB_THROW_SPEC_NULL
{
    //
    //  Compute the roll, pitch, and yaw values.  The following algorithm is
    //  taken from "Robot Manipulators: Mathematics, Programming, and Control",
    //  Richard P. Paul, pp. 70-71.
    //
	// SMW: IR HF-085334V5R20SP4 
	// ZeroTolerance WAS being used here below;  Changed to  0.00001. Here is the analysis from Uday Pathre ( UPE )
	// The purpose of ZeroTolerance is to prevent divide by 0 errors.
	// However, in this algorithm, we are not interested in preventing divide by zero math issues. 
	// In the legacy D5 code, a value of 0.0001 was used for the same algorithm because the intent is for
	// the singular RPY case if the rotation matrix is aligned such that the new X axis is aligned within 0.005 degrees 
	// of the new old Z axis direction, then we will round off the rotation angle representations such that ROLL 
	// value will be exactly 0.0 and the PITCH and YAW will alone capture the orientation change.
	
    if ( (abs(data_[ N ][ X ]) >= 0.00001) ||
         (abs(data_[ N ][ Y ]) >= 0.00001) )
    {
        T sinRoll;
        T cosRoll;

        //
        //  Compute the first solution.
        //
        DNBBasicVector3D<T>     soln1;      // X = Roll, Y = Pitch, Y = Yaw

        soln1[ X ] = atan2( data_[ N ][ Y ], data_[ N ][ X ] );

	DNBSinCos( soln1[X], sinRoll, cosRoll );

        soln1[ Y ] = atan2( -data_[ N ][ Z ],
            cosRoll * data_[ N ][ X ] + sinRoll * data_[ N ][ Y ] );

        soln1[ Z ] = atan2(
            sinRoll * data_[ A ][ X ] - cosRoll * data_[ A ][ Y ],
            cosRoll * data_[ S ][ Y ] - sinRoll * data_[ S ][ X ] );

        //
        //  Compute the second solution.
        //
        DNBBasicVector3D<T>     soln2;      // X = Roll, Y = Pitch, Y = Yaw

        soln2[ X ] = atan2( -data_[ N ][ Y ], -data_[ N ][ X ] );

	DNBSinCos( soln2[X], sinRoll, cosRoll );

        soln2[ Y ] = atan2( -data_[ N ][ Z ],
            cosRoll * data_[ N ][ X ] + sinRoll * data_[ N ][ Y ] );

        soln2[ Z ] = atan2(
            sinRoll * data_[ A ][ X ] - cosRoll * data_[ A ][ Y ],
            cosRoll * data_[ S ][ Y ] - sinRoll * data_[ S ][ X ] );

        //
        //  Choose the solution closest to zero.
        //
        if ( DNBL2NormSqr(soln1) <= DNBL2NormSqr(soln2) )
        {
            roll  = soln1[ X ];
            pitch = soln1[ Y ];
            yaw   = soln1[ Z ];
        }
        else
        {
            roll  = soln2[ X ];
            pitch = soln2[ Y ];
            yaw   = soln2[ Z ];
        }
    }
    else
    {
        roll  = T( 0 );
        pitch = atan2( -data_[ N ][ Z ], T( 0 ) );
        yaw   = atan2( -data_[ A ][ Y ], data_[ S ][ Y ] );
    }
}


template <class T>
void
DNBBasicRotate3D<T>::get( T ** right, bool transposed ) const
    DNB_THROW_SPEC_NULL
{
    size_t      row;
    size_t      col;

    DNB_PRECONDITION( right != NULL );

    if ( transposed )
    {
        for ( row = 0; row < NumRows; row++ )
        {
            for ( col = 0; col < NumCols; col++ )
            {
                right[ row ][ col ] = data_[ row ][ col ];
            }
        }
    }
    else
    {
        for ( row = 0; row < NumRows; row++ )
        {
            for ( col = 0; col < NumCols; col++ )
            {
                right[ col ][ row ] = data_[ row ][ col ];
            }
        }
    }
}


template <class T>
void
DNBBasicRotate3D<T>::get( DNBBasicMathMatrix<T> &right, bool transposed ) const
    DNB_THROW_SPEC_NULL
{
    size_t      row;
    size_t      col;

    DNB_PRECONDITION( right.rows() == 3 );
    DNB_PRECONDITION( right.cols() == 3 );

    if ( transposed )
    {
        for ( row = 0; row < NumRows; row++ )
        {
            for ( col = 0; col < NumCols; col++ )
            {
                right( row, col ) = data_[ row ][ col ];
            }
        }
    }
    else
    {
        for ( row = 0; row < NumRows; row++ )
        {
            for ( col = 0; col < NumCols; col++ )
            {
                right( col, row ) = data_[ row ][ col ];
            }
        }
    }
}


template <class T>
void
DNBBasicRotate3D<T>::setNSA( const DNBBasicVector3D<T> &vecN,
                             const DNBBasicVector3D<T> &vecS,
                             const DNBBasicVector3D<T> &vecA )
    DNB_THROW_SPEC((DNBESingularMatrix, DNBEIllegalMatrix))
{
    data_[ N ][ X ] = vecN.data_[ X ];
    data_[ N ][ Y ] = vecN.data_[ Y ];
    data_[ N ][ Z ] = vecN.data_[ Z ];

    data_[ S ][ X ] = vecS.data_[ X ];
    data_[ S ][ Y ] = vecS.data_[ Y ];
    data_[ S ][ Z ] = vecS.data_[ Z ];

    data_[ A ][ X ] = vecA.data_[ X ];
    data_[ A ][ Y ] = vecA.data_[ Y ];
    data_[ A ][ Z ] = vecA.data_[ Z ];

    normalize( );
}


template <class T>
void
DNBBasicRotate3D<T>::setEAA( const DNBBasicVector3D<T> &vec, const T &angle )
    DNB_THROW_SPEC((DNBEZeroVector))
{
    if ( angle == T(0) )
    {
        *this = Identity;
        return;
    }

    //
    //  Normalize the axis of rotation.
    //
    DNBBasicVector3D<T> kAxis( vec );
    kAxis.normalize( );

    T   kx = kAxis.data_[ X ];
    T   ky = kAxis.data_[ Y ];
    T   kz = kAxis.data_[ Z ];

    //
    //  Compute some intermediate values.
    //
    T   sinAngle, cosAngle;
    DNBSinCos( angle, sinAngle, cosAngle );

    T   versAngle = T( 1 ) - cosAngle;

    T   kxVersAngle = kx * versAngle;
    T   kyVersAngle = ky * versAngle;
    T   kzVersAngle = kz * versAngle;

    T   kxSinAngle  = kx * sinAngle;
    T   kySinAngle  = ky * sinAngle;
    T   kzSinAngle  = kz * sinAngle;

    //
    //  Compute the transformation matrix.
    //
    data_[ N ][ X ] = kx * kxVersAngle +   cosAngle;
    data_[ N ][ Y ] = kx * kyVersAngle + kzSinAngle;
    data_[ N ][ Z ] = kx * kzVersAngle - kySinAngle;

    data_[ S ][ X ] = ky * kxVersAngle - kzSinAngle;
    data_[ S ][ Y ] = ky * kyVersAngle +   cosAngle;
    data_[ S ][ Z ] = ky * kzVersAngle + kxSinAngle;

    data_[ A ][ X ] = kz * kxVersAngle + kySinAngle;
    data_[ A ][ Y ] = kz * kyVersAngle - kxSinAngle;
    data_[ A ][ Z ] = kz * kzVersAngle +   cosAngle;
}


template <class T>
void
DNBBasicRotate3D<T>::setCAA( AxisType axisXYZ, const T &angle )
    DNB_THROW_SPEC_NULL
{
    *this = Identity;

    if ( angle == T(0) )
        return;

    T   sinAngle, cosAngle;
    DNBSinCos( angle, sinAngle, cosAngle );

    switch ( axisXYZ)
    {
        case XAxis:
            data_[ S ][ Y ] =  cosAngle;
            data_[ S ][ Z ] =  sinAngle;

            data_[ A ][ Y ] = -sinAngle;
            data_[ A ][ Z ] =  cosAngle;
            break;

        case YAxis:
            data_[ N ][ X ] =  cosAngle;
            data_[ N ][ Z ] = -sinAngle;

            data_[ A ][ X ] =  sinAngle;
            data_[ A ][ Z ] =  cosAngle;
            break;

        case ZAxis:
            data_[ N ][ X ] =  cosAngle;
            data_[ N ][ Y ] =  sinAngle;

            data_[ S ][ X ] = -sinAngle;
            data_[ S ][ Y ] =  cosAngle;
            break;

        default:
            DNB_PROGRAM_ERROR( "Invalid rotation axis" );
            break;
    }
}


template <class T>
void
DNBBasicRotate3D<T>::setEuler( const DNBBasicEuler3D<T> &euler )
    DNB_THROW_SPEC_NULL
{
    *this = Identity;
    DNBBasicRotate3D<T> tmp( Uninitialized );

    for( size_t k = 0; k < 3; k++ )
    {
	if( euler.angles_[k] != T(0) )
	{
	    tmp = Identity;
	    int i = ( euler.axes_[k] + 1 ) % 3;
	    int j = ( i + 1 ) % 3;
	    T sinVal, cosVal;
	    DNBSinCos( euler.angles_[k], sinVal, cosVal );

	    tmp.data_[i][i] = cosVal;
	    tmp.data_[j][j] = cosVal;
	    tmp.data_[j][i] = -sinVal;
	    tmp.data_[i][j] = sinVal;

	    if( k == 0 )
	        *this = tmp;
            else
	        (*this) *= tmp;
        }
    }

}


template <class T>
void
DNBBasicRotate3D<T>::setRPY( const T &roll, const T &pitch, const T &yaw )
    DNB_THROW_SPEC_NULL
{
    T   sinRoll, cosRoll;
    DNBSinCos( roll, sinRoll, cosRoll );

    T   sinPitch, cosPitch;
    DNBSinCos( pitch, sinPitch, cosPitch );

    T   sinYaw, cosYaw;
    DNBSinCos( yaw, sinYaw, cosYaw );

    T   temp;

    data_[ N ][ X ] =  cosRoll * cosPitch;
    data_[ N ][ Y ] =  sinRoll * cosPitch;
    data_[ N ][ Z ] = -sinPitch;

    temp = sinPitch * sinYaw;
    data_[ S ][ X ] = cosRoll * temp - sinRoll * cosYaw;
    data_[ S ][ Y ] = sinRoll * temp + cosRoll * cosYaw;
    data_[ S ][ Z ] = cosPitch * sinYaw;

    temp = sinPitch * cosYaw;
    data_[ A ][ X ] = cosRoll * temp + sinRoll * sinYaw;
    data_[ A ][ Y ] = sinRoll * temp - cosRoll * sinYaw;
    data_[ A ][ Z ] = cosPitch * cosYaw;
}


template <class T>
void
DNBBasicRotate3D<T>::set( const T** right, bool transposed )
    DNB_THROW_SPEC((DNBESingularMatrix, DNBEIllegalMatrix))
{
    size_t      row;
    size_t      col;

    DNB_PRECONDITION( right != NULL );

    if ( transposed )
    {
        for ( row = 0; row < NumRows; row++ )
        {
            for ( col = 0; col < NumCols; col++ )
            {
                data_[ row ][ col ] = right[ row ][ col ];
            }
        }
    }
    else
    {
        for ( row = 0; row < NumRows; row++ )
        {
            for ( col = 0; col < NumCols; col++ )
            {
                data_[ row ][ col ] = right[ col ][ row ];
            }
        }
    }

    normalize( );
}


template <class T>
void
DNBBasicRotate3D<T>::set( const DNBBasicMathMatrix<T> &right, bool transposed )
    DNB_THROW_SPEC((DNBESingularMatrix, DNBEIllegalMatrix))
{
    size_t      row;
    size_t      col;

    DNB_PRECONDITION( right.rows() == 3 );
    DNB_PRECONDITION( right.cols() == 3 );

    if ( transposed )
    {
        for ( row = 0; row < NumRows; row++ )
        {
            for ( col = 0; col < NumCols; col++ )
            {
                data_[ row ][ col ] = right( row, col );
            }
        }
    }
    else
    {
        for ( row = 0; row < NumRows; row++ )
        {
            for ( col = 0; col < NumCols; col++ )
            {
                data_[ row ][ col ] = right( col, row );
            }
        }
    }

    normalize( );
}


template <class T>
DNBBasicQtn3D<T>
DNBBasicRotate3D<T>::toQtn( ) const
    DNB_THROW_SPEC_NULL
{
    DNBBasicQtn3D<T> qtn( Uninitialized );
    qtn.setRotate( *this );

    return qtn;
}


template <class T>
DNBBasicMathMatrix<T>
DNBBasicRotate3D<T>::toDNBBasicMathMatrix( ) const
    DNB_THROW_SPEC((scl_bad_alloc))
{
    DNBBasicMathMatrix<T>     result( 3, 3, Uninitialized );
    get( result, false );

    return result;
}


template <class T>
void
DNBBasicRotate3D<T>::swap( DNBBasicRotate3D<T> &other )
    DNB_THROW_SPEC_NULL
{
    DNBBasicRotate3D<T>     temp( *this );
    *this = other;
    other = temp;
}


template <class T>
T
DNBBasicRotate3D<T>::det( ) const
    DNB_THROW_SPEC_NULL
{
    T result = data_[N][X] * data_[S][Y] * data_[A][Z] +
	       data_[N][Y] * data_[S][Z] * data_[A][X] +
	       data_[N][Z] * data_[S][X] * data_[A][Y] -
	       data_[N][Z] * data_[S][Y] * data_[A][X] -
	       data_[N][Y] * data_[S][X] * data_[A][Z] -
	       data_[N][X] * data_[S][Z] * data_[A][Y];

    return result;
}


template <class T>
void
DNBBasicRotate3D<T>::normalize( )
    DNB_THROW_SPEC((DNBESingularMatrix, DNBEIllegalMatrix))
{
#if 0  // disabled because of scale and shear coming from D5 products
    if( abs( det() - T(1) ) >= DetTolerance )
    {
	DNBEIllegalMatrix error( DNB_FORMAT("The rotation matrix is illegal") );
	throw error;
    }
#endif

    scl_numeric_limits<T>   traits;
    DNB_ASSERT( traits.is_specialized );

    //
    //  Compute an orthogonal basis for the rotation matrix using the (modified)
    //  Gram-Schmidt algorithm.
    //
    DNBBasicVector3D<T> vectorN( data_[ N ] );
    DNBBasicVector3D<T> vectorS( data_[ S ] );
    DNBBasicVector3D<T> vectorA( data_[ A ] );

    DNBBasicVector3D<T> errTerm;

    //
    //  Compute N.
    //
    DNBBasicVector3D<T> resultN( vectorN );

    T       lengthN = DNBL2NormSqr( resultN );
    if ( lengthN <= traits.epsilon() )
    {
#if DNB_VERIFY
        DNBESingularMatrix  error(
            DNB_FORMAT("The rotation matrix is singular") );
        throw error;
#else
	*this = Identity;
	return;
#endif
    }

    //
    //  Compute S.
    //
    DNBBasicVector3D<T> resultS( vectorS );
    errTerm = ( DNBDot( vectorS, resultN ) / lengthN ) * resultN;
    resultS -= errTerm;

    T       lengthS = DNBL2NormSqr( resultS );
    if ( lengthS <= traits.epsilon() )
    {
#if DNB_VERIFY
        DNBESingularMatrix  error(
            DNB_FORMAT("The rotation matrix is singular") );
        throw error;
#else
	*this = Identity;
	return;
#endif
    }

    //
    //  Compute A.
    //
    DNBBasicVector3D<T> resultA( vectorA );
    errTerm = ( DNBDot( vectorA, resultN ) / lengthN ) * resultN;
    resultA -= errTerm;

    errTerm = ( DNBDot( vectorA, resultS ) / lengthS ) * resultS;
    resultA -= errTerm;

    T       lengthA = DNBL2NormSqr( resultA );
    if ( lengthA <= traits.epsilon() )
    {
#if DNB_VERIFY
        DNBESingularMatrix  error(
            DNB_FORMAT("The rotation matrix is singular") );
        throw error;
#else
	*this = Identity;
	return;
#endif
    }

    //
    //  Convert the resulting vectors into unit vectors.
    //
    resultN /= sqrt( lengthN );
    resultS /= sqrt( lengthS );
    resultA /= sqrt( lengthA );

    //
    //  Update the rotation matrix.
    //
    data_[ N ][ X ] = resultN.data_[ X ];
    data_[ N ][ Y ] = resultN.data_[ Y ];
    data_[ N ][ Z ] = resultN.data_[ Z ];

    data_[ S ][ X ] = resultS.data_[ X ];
    data_[ S ][ Y ] = resultS.data_[ Y ];
    data_[ S ][ Z ] = resultS.data_[ Z ];

    data_[ A ][ X ] = resultA.data_[ X ];
    data_[ A ][ Y ] = resultA.data_[ Y ];
    data_[ A ][ Z ] = resultA.data_[ Z ];
}


template <class T>
void
DNBBasicRotate3D<T>::rotate( AxisType axis, const T &angle,
                             ReferenceFrame refFrame )
    DNB_THROW_SPEC_NULL
{
    DNB_ASSERT( ( axis >= 0 ) && ( axis <= 2 ) );

    T cosAngle, sinAngle;
    DNBSinCos( angle, sinAngle, cosAngle );

    int i = ( axis + 1 ) % 3;
    int j = ( i + 1 ) % 3;

    T tmp0, tmp1, tmp2;

    switch ( refFrame )
    {
        case Origin:
	    tmp0 = cosAngle * data_[N][i] - sinAngle * data_[N][j];
	    tmp1 = cosAngle * data_[S][i] - sinAngle * data_[S][j];
	    tmp2 = cosAngle * data_[A][i] - sinAngle * data_[A][j];

	    data_[N][j] = sinAngle * data_[N][i] + cosAngle * data_[N][j];
	    data_[S][j] = sinAngle * data_[S][i] + cosAngle * data_[S][j];
	    data_[A][j] = sinAngle * data_[A][i] + cosAngle * data_[A][j];

	    data_[N][i] = tmp0;
	    data_[S][i] = tmp1;
	    data_[A][i] = tmp2;

            break;

        case Local:
	    tmp0 = cosAngle * data_[i][X] + sinAngle * data_[j][X];
	    tmp1 = cosAngle * data_[i][Y] + sinAngle * data_[j][Y];
	    tmp2 = cosAngle * data_[i][Z] + sinAngle * data_[j][Z];

	    data_[j][X] = cosAngle * data_[j][X] - sinAngle * data_[i][X];
	    data_[j][Y] = cosAngle * data_[j][Y] - sinAngle * data_[i][Y];
	    data_[j][Z] = cosAngle * data_[j][Z] - sinAngle * data_[i][Z];

	    data_[i][X] = tmp0;
	    data_[i][Y] = tmp1;
	    data_[i][Z] = tmp2;

            break;

        default:
            DNB_PROGRAM_ERROR( "Invalid reference frame" );
            break;
    }
}


template <class T>
void
DNBBasicRotate3D<T>::rotate( const DNBBasicVector3D<T> &vec, const T &angle,
                             ReferenceFrame refFrame )
    DNB_THROW_SPEC((DNBEZeroVector))
{
    DNBBasicRotate3D<T>     tmp( Uninitialized );
    tmp.setEAA( vec, angle );

    switch ( refFrame )
    {
        case Origin:
            preMultiply( tmp );
            break;

        case Local:
            postMultiply( tmp );
            break;

        default:
            DNB_PROGRAM_ERROR( "Invalid reference frame" );
            break;
    }
}


template <class T>
void
DNBBasicRotate3D<T>::rotate( const DNBBasicRotate3D<T> &rotate,
			     ReferenceFrame refFrame )
    DNB_THROW_SPEC_NULL
{
    switch ( refFrame )
    {
        case Origin:
            preMultiply( rotate );
            break;

        case Local:
            postMultiply( rotate );
            break;

        default:
            DNB_PROGRAM_ERROR( "Invalid reference frame" );
            break;
    }
}


template <class T>
void
DNBBasicRotate3D<T>::rotateVec( DNBBasicVector3D<T> &vec ) const
    DNB_THROW_SPEC_NULL
{
    DNBBasicVector3D<T> result( Uninitialized );
    multiply( result, *this, vec );
    vec = result;
}


template <class T>
void
DNBBasicRotate3D<T>::preMultiply( const DNBBasicRotate3D<T> &left )
    DNB_THROW_SPEC_NULL
{
    DNBBasicRotate3D<T>     result( Uninitialized );
    multiply( result, left, *this );
    *this = result;
}


template <class T>
void
DNBBasicRotate3D<T>::postMultiply( const DNBBasicRotate3D<T> &right )
    DNB_THROW_SPEC_NULL
{
    DNBBasicRotate3D<T>     result( Uninitialized );
    multiply( result, *this, right );
    *this = result;
}


template <class T>
void
DNBBasicRotate3D<T>::invert( )
    DNB_THROW_SPEC((DNBESingularMatrix))
{
    //
    //  Compute the transpose of the direction cosine matrix.
    //
    T   temp;

    temp            = data_[ N ][ Y ];
    data_[ N ][ Y ] = data_[ S ][ X ];
    data_[ S ][ X ] = temp;

    temp            = data_[ N ][ Z ];
    data_[ N ][ Z ] = data_[ A ][ X ];
    data_[ A ][ X ] = temp;

    temp            = data_[ S ][ Z ];
    data_[ S ][ Z ] = data_[ A ][ Y ];
    data_[ A ][ Y ] = temp;
}


template <class T>
DNBBasicVector3D<T>
DNBBasicRotate3D<T>::operator()( DirectionIndex dirIndex ) const
    DNB_THROW_SPEC_NULL
{
    return DNBBasicVector3D<T>( data_[ dirIndex ] );
}


template <class T>
T
DNBBasicRotate3D<T>::operator()( DirectionIndex dirIndex,
				 CoordinateIndex coordIndex ) const
    DNB_THROW_SPEC_NULL
{
    return data_[dirIndex][coordIndex];
}


template <class T>
DNBBasicRotate3D<T> &
DNBBasicRotate3D<T>::operator=( const DNBBasicRotate3D<T> &right )
    DNB_THROW_SPEC_NULL
{
    if( this != &right )               // Prevent self-assignment.
    {
        data_[ N ][ X ] = right.data_[ N ][ X ];
        data_[ N ][ Y ] = right.data_[ N ][ Y ];
        data_[ N ][ Z ] = right.data_[ N ][ Z ];

        data_[ S ][ X ] = right.data_[ S ][ X ];
        data_[ S ][ Y ] = right.data_[ S ][ Y ];
        data_[ S ][ Z ] = right.data_[ S ][ Z ];

        data_[ A ][ X ] = right.data_[ A ][ X ];
        data_[ A ][ Y ] = right.data_[ A ][ Y ];
        data_[ A ][ Z ] = right.data_[ A ][ Z ];
    }

    return *this;
}


template <class T>
DNBBasicRotate3D<T> &
DNBBasicRotate3D<T>::operator*=( const DNBBasicRotate3D<T> &right )
    DNB_THROW_SPEC_NULL
{
    DNBBasicRotate3D<T> result( Uninitialized );
    multiply( result, *this, right );
    *this = result;

    return *this;
}


template <class T>
DNBBasicRotate3D<T> &
DNBBasicRotate3D<T>::operator/=( const DNBBasicRotate3D<T> &right )
    DNB_THROW_SPEC((DNBESingularMatrix))
{
    DNBBasicRotate3D<T> result( Uninitialized );
    divide( result, *this, right );
    *this = result;

    return *this;
}


template <class T>
bool
operator==( const DNBBasicRotate3D<T> &left, const DNBBasicRotate3D<T> &right )
    DNB_THROW_SPEC_NULL
{
    for ( size_t row = 0; row <DNBBasicRotate3D<T>:: NumRows; row++ )
    {
        for ( size_t col = 0; col < DNBBasicRotate3D<T>::NumCols; col++ )
        {
            if ( left.data_[ row ][ col ] != right.data_[ row ][ col ] )
                return false;
        }
    }

    return true;
}


template <class T>
bool
operator!=( const DNBBasicRotate3D<T> &left, const DNBBasicRotate3D<T> &right )
    DNB_THROW_SPEC_NULL
{
    for ( size_t row = 0; row <DNBBasicRotate3D<T>:: NumRows; row++ )
    {
        for ( size_t col = 0; col < DNBBasicRotate3D<T>::NumCols; col++ )
        {
            if ( left.data_[ row ][ col ] != right.data_[ row ][ col ] )
                return true;
        }
    }

    return false;
}


template <class T>
DNBBasicRotate3D<T>
operator*( const DNBBasicRotate3D<T> &left, const DNBBasicRotate3D<T> &right )
    DNB_THROW_SPEC_NULL
{
    DNBBasicRotate3D<T> result( DNBMath3D::Uninitialized );
    DNBBasicRotate3D<T>::multiply( result, left, right );

    return result;
}


template <class T>
DNBBasicRotate3D<T>
operator/( const DNBBasicRotate3D<T> &left, const DNBBasicRotate3D<T> &right )
    DNB_THROW_SPEC((DNBESingularMatrix))
{
    DNBBasicRotate3D<T> result( DNBMath3D::Uninitialized );
    DNBBasicRotate3D<T>::divide( result, left, right );

    return result;
}


template <class T>
DNBBasicVector3D<T>
operator*( const DNBBasicRotate3D<T> &left, const DNBBasicVector3D<T> &right )
    DNB_THROW_SPEC_NULL
{
    DNBBasicVector3D<T> result( DNBMath3D::Uninitialized );
    DNBBasicRotate3D<T>::multiply( result, left, right );

    return result;
}


template <class T>
ostream &
operator<<( ostream &ostr, const DNBBasicRotate3D<T> &item )
    DNB_THROW_SPEC((scl_bad_alloc))
{
    DNBBasicMathMatrix<T> matItem( 3, 3, Uninitialized );
    item.get( matItem, false );

    return (ostr << matItem);
}


template <class T>
bool
DNBNearRel( const DNBBasicRotate3D<T> &left, const DNBBasicRotate3D<T> &right,
            const T &angularTol, const T &linearTol )
    DNB_THROW_SPEC_NULL
{
    if ( (angularTol <= T(0)) || (linearTol <= T(0)) )
        return ( left == right );

    DNBBasicQtn3D<T> leftQtn( DNBMath3D::Uninitialized );
    leftQtn.setRotate( left );

    DNBBasicQtn3D<T> rightQtn( DNBMath3D::Uninitialized );
    rightQtn.setRotate( right );

    return DNBNearRel( leftQtn, rightQtn, angularTol, linearTol );
}


template <class T>
bool
DNBNearAbs( const DNBBasicRotate3D<T> &left, const DNBBasicRotate3D<T> &right,
            const T &angularTol, const T &linearTol )
    DNB_THROW_SPEC_NULL
{
    if ( (angularTol <= T(0)) || (linearTol <= T(0)) )
        return ( left == right );

    DNBBasicQtn3D<T> leftQtn( DNBMath3D::Uninitialized );
    leftQtn.setRotate( left );

    DNBBasicQtn3D<T> rightQtn( DNBMath3D::Uninitialized );
    rightQtn.setRotate( right );

    return DNBNearAbs( leftQtn, rightQtn, angularTol, linearTol );
}


template <class T>
const DNBBasicRotate3D<T>    DNBBasicRotate3D<T>::Identity;


template <class T>
const T  DNBBasicRotate3D<T>::ZeroTolerance = T(100) * scl_numeric_limits<T>::epsilon( );

template <class T>
const T  DNBBasicRotate3D<T>::DetTolerance = T(0.5);


template <class T>
void
DNBBasicRotate3D<T>::multiply( DNBBasicRotate3D<T> &result,
                               const DNBBasicRotate3D<T> &left,
                               const DNBBasicRotate3D<T> &right )
    DNB_THROW_SPEC_NULL
{
    result.data_[ N ][ X ] = left.data_[ N ][ X ] * right.data_[ N ][ X ] +
                             left.data_[ S ][ X ] * right.data_[ N ][ Y ] +
                             left.data_[ A ][ X ] * right.data_[ N ][ Z ];

    result.data_[ N ][ Y ] = left.data_[ N ][ Y ] * right.data_[ N ][ X ] +
                             left.data_[ S ][ Y ] * right.data_[ N ][ Y ] +
                             left.data_[ A ][ Y ] * right.data_[ N ][ Z ];

    result.data_[ N ][ Z ] = left.data_[ N ][ Z ] * right.data_[ N ][ X ] +
                             left.data_[ S ][ Z ] * right.data_[ N ][ Y ] +
                             left.data_[ A ][ Z ] * right.data_[ N ][ Z ];

    result.data_[ S ][ X ] = left.data_[ N ][ X ] * right.data_[ S ][ X ] +
                             left.data_[ S ][ X ] * right.data_[ S ][ Y ] +
                             left.data_[ A ][ X ] * right.data_[ S ][ Z ];

    result.data_[ S ][ Y ] = left.data_[ N ][ Y ] * right.data_[ S ][ X ] +
                             left.data_[ S ][ Y ] * right.data_[ S ][ Y ] +
                             left.data_[ A ][ Y ] * right.data_[ S ][ Z ];

    result.data_[ S ][ Z ] = left.data_[ N ][ Z ] * right.data_[ S ][ X ] +
                             left.data_[ S ][ Z ] * right.data_[ S ][ Y ] +
                             left.data_[ A ][ Z ] * right.data_[ S ][ Z ];

  //
  // IR 346834A : compute the A vector by fully multiplying out the
  // two given rotation matrices. For LH coordinate system in SO(3),
  // det(R) = -1 and ixj=-k. Hence we cannot apply the optimization
  // ixj=k without computing det(R). jyv, 05/14/02.
  //

  result.data_[A][X] = left.data_[N][X] * right.data_[A][X] +
                       left.data_[S][X] * right.data_[A][Y] +
                       left.data_[A][X] * right.data_[A][Z];


  result.data_[A][Y] = left.data_[N][Y] * right.data_[A][X] +
                       left.data_[S][Y] * right.data_[A][Y] +
                       left.data_[A][Y] * right.data_[A][Z];

  result.data_[A][Z] = left.data_[N][Z] * right.data_[A][X] +
                       left.data_[S][Z] * right.data_[A][Y] +
                       left.data_[A][Z] * right.data_[A][Z];



}


template <class T>
void
DNBBasicRotate3D<T>::multiply( DNBBasicVector3D<T> &result,
                               const DNBBasicRotate3D<T> &left,
                               const DNBBasicVector3D<T> &right )
    DNB_THROW_SPEC_NULL
{
    result.data_[ X ] = left.data_[ N ][ X ] * right.data_[ X ] +
                        left.data_[ S ][ X ] * right.data_[ Y ] +
                        left.data_[ A ][ X ] * right.data_[ Z ];

    result.data_[ Y ] = left.data_[ N ][ Y ] * right.data_[ X ] +
                        left.data_[ S ][ Y ] * right.data_[ Y ] +
                        left.data_[ A ][ Y ] * right.data_[ Z ];

    result.data_[ Z ] = left.data_[ N ][ Z ] * right.data_[ X ] +
                        left.data_[ S ][ Z ] * right.data_[ Y ] +
                        left.data_[ A ][ Z ] * right.data_[ Z ];
}


template <class T>
void
DNBBasicRotate3D<T>::divide( DNBBasicRotate3D<T> &result,
                             const DNBBasicRotate3D<T> &left,
                             const DNBBasicRotate3D<T> &right )
    DNB_THROW_SPEC((DNBESingularMatrix))
{
    result.data_[ N ][ X ] = left.data_[ N ][ X ] * right.data_[ N ][ X ] +
                             left.data_[ S ][ X ] * right.data_[ S ][ X ] +
                             left.data_[ A ][ X ] * right.data_[ A ][ X ];

    result.data_[ N ][ Y ] = left.data_[ N ][ Y ] * right.data_[ N ][ X ] +
                             left.data_[ S ][ Y ] * right.data_[ S ][ X ] +
                             left.data_[ A ][ Y ] * right.data_[ A ][ X ];

    result.data_[ N ][ Z ] = left.data_[ N ][ Z ] * right.data_[ N ][ X ] +
                             left.data_[ S ][ Z ] * right.data_[ S ][ X ] +
                             left.data_[ A ][ Z ] * right.data_[ A ][ X ];

    result.data_[ S ][ X ] = left.data_[ N ][ X ] * right.data_[ N ][ Y ] +
                             left.data_[ S ][ X ] * right.data_[ S ][ Y ] +
                             left.data_[ A ][ X ] * right.data_[ A ][ Y ];

    result.data_[ S ][ Y ] = left.data_[ N ][ Y ] * right.data_[ N ][ Y ] +
                             left.data_[ S ][ Y ] * right.data_[ S ][ Y ] +
                             left.data_[ A ][ Y ] * right.data_[ A ][ Y ];

    result.data_[ S ][ Z ] = left.data_[ N ][ Z ] * right.data_[ N ][ Y ] +
                             left.data_[ S ][ Z ] * right.data_[ S ][ Y ] +
                             left.data_[ A ][ Z ] * right.data_[ A ][ Y ];


    //
    // IR 346834A : compute the A vector by fully dividing left by right.
    // For LH coordinate system in SO(3), det(R) = -1 and ixj=-k. Hence we
    // cannot apply the optimization ixj=k without computing det(R).
    // jyv, 05/14/02.
    //
    result.data_[ A ][ X ] = left.data_[ N ][ X ] * right.data_[ N ][ Z ] +
                             left.data_[ S ][ X ] * right.data_[ S ][ Z ] +
                             left.data_[ A ][ X ] * right.data_[ A ][ Z ];

    result.data_[ A ][ Y ] = left.data_[ N ][ Y ] * right.data_[ N ][ Z ] +
                             left.data_[ S ][ Y ] * right.data_[ S ][ Z ] +
                             left.data_[ A ][ Y ] * right.data_[ A ][ Z ];

    result.data_[ A ][ Z ] = left.data_[ N ][ Z ] * right.data_[ N ][ Z ] +
                             left.data_[ S ][ Z ] * right.data_[ S ][ Z ] +
                             left.data_[ A ][ Z ] * right.data_[ A ][ Z ];

}
