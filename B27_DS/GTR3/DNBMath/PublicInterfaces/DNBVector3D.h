//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
/**
 */

//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     pjf         08/05/97    Initial implementation
//*     jad         08/07/98    Ported to MSVC 5.0.  Added support for reverse
//*                             iterators and other standard STL methods.  Made
//*                             several changes to enhance compatibility with
//*                             the class RWMathVec<T>.
//*     jad         08/10/98    Added the base class DNBMath3D and renamed the
//*                             class to DNBBasicVector3D.
//*     jad         08/26/98    Renamed the function DNBEqual to DNBNearAbs.
//*                             Added the functions DNBNearRel and DNBAbs.
//*     smw         09/02/03    Removing all references to RWMathVec
//*     bkh         11/05/03    Implementation of CAA style documentation.
//*		smw			01/07/05	Removed extractors due to iostream migration to the
//*								new, standard, templated iostream, and due to migration to 64 bit
//*								builds.  Extractor for all math classes have been moved,
//*								for easy rescucitation, if necessary,
//*								to DNBMath.tst/PrivateInterfaces/DNBMathExtractors.h
//*
//*
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNBVECTOR3D_H_
#define _DNBVECTOR3D_H_


#include <DNBSystemBase.h>
#include <scl_iterator.h>
#include <DNBSystemDefs.h>


#include <DNBMath3D.h>
#include <DNBCMath.h>
#include <DNBException.h>
#include <DNBMathVector.h>



//
//  Forward declarations.
//
template <class T>
class DNBBasicXform3D;

template <class T>
class DNBBasicRotate3D;

template <class T>
class DNBBasicQtn3D;

template <class T>
class DNBBasicVector3D;

template <class T>
DNBBasicVector3D<T>
operator+( const DNBBasicVector3D<T> &left, const DNBBasicVector3D<T> &right )
    DNB_THROW_SPEC_NULL;

template <class T>
DNBBasicVector3D<T>
operator-( const DNBBasicVector3D<T> &left, const DNBBasicVector3D<T> &right )
    DNB_THROW_SPEC_NULL;

template <class T>
DNBBasicVector3D<T>
operator*( const DNBBasicVector3D<T> &left, const DNBBasicVector3D<T> &right )
    DNB_THROW_SPEC_NULL;

template <class T>
DNBBasicVector3D<T>
operator/( const DNBBasicVector3D<T> &left, const DNBBasicVector3D<T> &right )
    DNB_THROW_SPEC_NULL;

template <class T>
DNBBasicVector3D<T>
operator+( const DNBBasicVector3D<T> &left, const T &right )
    DNB_THROW_SPEC_NULL;

template <class T>
DNBBasicVector3D<T>
operator+( const T &left, const DNBBasicVector3D<T> &right )
    DNB_THROW_SPEC_NULL;

template <class T>
DNBBasicVector3D<T>
operator-( const DNBBasicVector3D<T> &left, const T &right )
    DNB_THROW_SPEC_NULL;

template <class T>
DNBBasicVector3D<T>
operator-( const T &left, const DNBBasicVector3D<T> &right )
    DNB_THROW_SPEC_NULL;

template <class T>
DNBBasicVector3D<T>
operator*( const DNBBasicVector3D<T> &left, const T &right )
    DNB_THROW_SPEC_NULL;

template <class T>
DNBBasicVector3D<T>
operator*( const T &left, const DNBBasicVector3D<T> &right )
    DNB_THROW_SPEC_NULL;

template <class T>
DNBBasicVector3D<T>
operator/( const DNBBasicVector3D<T> &left, const T &right )
    DNB_THROW_SPEC_NULL;

template <class T>
DNBBasicVector3D<T>
operator/( const T &left, const DNBBasicVector3D<T> &right )
    DNB_THROW_SPEC_NULL;

template <class T>
ostream &
operator<<( ostream &ostr, const DNBBasicVector3D<T> &item )
    DNB_THROW_SPEC((scl_bad_alloc));

template <class T>
T
DNBSum( const DNBBasicVector3D<T> &right )
    DNB_THROW_SPEC_NULL;

template <class T>
DNBBasicVector3D<T>
DNBAbs( const DNBBasicVector3D<T> &right )
    DNB_THROW_SPEC_NULL;

template <class T>
T
DNBL1Norm( const DNBBasicVector3D<T> &right )
    DNB_THROW_SPEC_NULL;

template <class T>
T
DNBL2Norm( const DNBBasicVector3D<T> &right )
    DNB_THROW_SPEC_NULL;

template <class T>
T
DNBLinfNorm( const DNBBasicVector3D<T> &right )
    DNB_THROW_SPEC_NULL;

template <class T>
T
DNBL2NormSqr( const DNBBasicVector3D<T> &right )
    DNB_THROW_SPEC_NULL;

template <class T>
bool
DNBNearRel( const DNBBasicVector3D<T> &left, const DNBBasicVector3D<T> &right,
    const T &tolerance )
    DNB_THROW_SPEC_NULL;

template <class T>
bool
DNBNearAbs( const DNBBasicVector3D<T> &left, const DNBBasicVector3D<T> &right,
    const T &tolerance )
    DNB_THROW_SPEC_NULL;

template <class T>
T
DNBDot( const DNBBasicVector3D<T> &left, const DNBBasicVector3D<T> &right )
    DNB_THROW_SPEC_NULL;

template <class T>
DNBBasicVector3D<T>
DNBCross( const DNBBasicVector3D<T> &left, const DNBBasicVector3D<T> &right )
    DNB_THROW_SPEC_NULL;


/**
* CLASS:
*     DNBBasicVector3D
*
* SUMMARY:
*     A templated class which represents a three-dimensional vector.
*
* TEMPLATING:
*     \param[in]{ class T }
*         The base type of the vector elements.
*
* DESCRIPTION:
*     This module defines a class which contains three values.  This
*     collection of values is intended to be used as a three-dimensional
*     vector.  The recommended type of the values contained in this
*     class would be any floating point numeric type.  If an integer
*     data type is used, then certain functions may not return values
*     as accurately as desired (e.g., the length of a vector).
*
*     The functions available for this class are all typical vector
*     related functions (e.g., dot product, cross product, normalize).
*
*     This class also defines the standard iterators (e.g., begin, end).
*     With these standard iterators, other standard vector manipulation
*     libraries may be used to manipulate these vectors.  In other
*     words, if there is a function not explicitly defined in this
*     class, other global functions may be used to manipulate these
*     vectors.
*
*     This class also provides operators to convert an instance of this
*     class to a DNBBasicMathVector<T>).
*     Even more vector functions are provided by the DNBBasicMathVector
*     vector class.
*
*     Since this is a class template, the user may define the type of
*     values which may be stored in the instances of the 3D vector class
*     object. It is recommended that some form of floating point numbers be
*     used to instantiate this class.  If other data types are used, then
*     some functions defined by this template class may return undesirable
*     results.
*
*/
template <class T>
class DNBBasicVector3D : public DNBMath3D
{
public:
/**
  *  The element type.
  *
  * <br><B>Description</B><br>
  *     This type definition specifies the type of object stored in the
  *     vector.
  */
    typedef T           value_type;


/**
  *  An unsigned integral type.
  *
  * <br><B>Description</B><br>
  *     This type definition specifies an unsigned integral type that can
  *     represent the maximum number of elements in the vector.
  */
    typedef size_t      size_type;


/**
  * A signed integral type.
  *
  * <br><B>Description</B><br>
  * This type definition specifies a signed integral type used to
  * represent the distance between two iterators.
  *
  *
  */
    typedef ptrdiff_t   difference_type;


/**
  * A pointer to the element type.
  *
  * <br><B>Description</B><br>
  * This type definition represents a pointer to the vector's element
  * type.
  *
  *
  */
    typedef T *         pointer;


/**
  * A const pointer to the element type.
  *
  * <br><B>Description</B><br>
  * This type definition represents a const pointer to the vector's
  * element type.
  *
  *
  */
    typedef const T *   const_pointer;


/**
  * A reference to the element type.
  *
  * <br><B>Description</B><br>
  * This type definition represents a reference to the vector's element
  * type.
  *
  *
  */
    typedef T &         reference;


/**
  * A const reference to the element type.
  *
  * <br><B>Description</B><br>
  * This type definition represents a const reference to the vector's
  * element type.
  *
  *
  */
    typedef const T &   const_reference;


/**
  * An iterator for the vector.
  *
  * <br><B>Description</B><br>
  * This type definition represents an iterator that can be used to
  * iterate through the vector's elements.
  *
  *
  */
    typedef scl_pointer_iterator<T>         iterator;


/**
  * A const iterator for the vector.
  *
  * <br><B>Description</B><br>
  * This type definition represents a const iterator that can be used to
  * iterate through the vector's elements.
  *
  *
  */
    typedef scl_pointer_const_iterator<T>   const_iterator;


/**
  * A const reverse iterator for the vector.
  *
  * <br><B>Description</B><br>
  * This type definition represents a const iterator that can be used to
  * iterate backwards through the vector's elements.
  *
  *
  */
    typedef scl_reverse_iterator<const_iterator>        const_reverse_iterator;


/**
  * A reverse iterator for the vector.
  *
  * <br><B>Description</B><br>
  * This type definition represents an iterator that can be used to
  * iterate backwards through the vector's elements.
  *
  *
  */
    typedef scl_reverse_iterator<iterator>              reverse_iterator;


/**
  * Constructs the three-dimensional zero vector.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function constructs a three-dimensional vector in which each
  * element is set to zero.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     vector;
  *
  * </pre>
  *
  */
    DNBBasicVector3D( )
        DNB_THROW_SPEC_NULL;


/**
  * Constructs a three-dimensional vector using copy semantics.
  * @param  right
  * The three-dimensional vector to be copied.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function copies the elements of <tt>right</tt> to <tt>self</tt>.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     vector1( 1.0, 2.0, 3.0 );
  * DNBVector3D     vector2( vector1 );
  *
  * </pre>
  *
  */
    DNBBasicVector3D( const DNBBasicVector3D<T> &right )
        DNB_THROW_SPEC_NULL;


/**
  * Constructs an uninitialized three-dimensional vector.
  * @param  dummy
  * The UninitializedType object indicationg <tt>self</tt> will be
  * constructed but will not be initialized.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function creates a three-dimensional vector. Components
  * of the vector will not be initialized. Vectors constructed with
  * this constructor are expected to be set to a known state before
  * any use.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     vecN( DNBMath3D::Uninitialized );
  * DNBVector3D     vecS( DNBMath3D::Uninitialized );
  * DNBVector3D     vecA( DNBMath3D::Uninitialized );
  * DNBVector3D     vecP( DNBMath3D::Uninitialized );
  *
  * DNBXform3D      xform;
  * xform.get( vecN, vecS, vecA, vecP );
  *
  * </pre>
  *
  */
    DNBBasicVector3D( UninitializedType dummy )
        DNB_THROW_SPEC_NULL;


/**
  * Constructs a three-dimensional vector from three scalar values.
  * @param  x
  * The x-component of the vector.
  * @param  y
  * The y-component of the vector.
  * @param  z
  * The z-component of the vector.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function assigns <tt>x</tt>, <tt>y</tt>, and <tt>z</tt> to the corresponding
  * elements of <tt>self</tt>.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     vector( 1.0, 2.0, 3.0 );
  *
  * </pre>
  *
  */
    DNBBasicVector3D( const T &x, const T &y, const T &z )
DNB_THROW_SPEC_NULL;



    // SMW - Support spherical coord

    /**
    * Sets the vector using spherical coordinates
    * @param theta  Angle in XY plane, measured counterclockwise from X axis
    * @param phi    Angle in a plane containing the Z axis and the vector, measured counterclockwise from XY plane
    * @param r      Radius: distance from origin to tip of vector.
    */
    void setSpherical( const T &theta, const T &phi, const T &r )
        DNB_THROW_SPEC_NULL;

    /**
    * Gets the vector's spherical coordinates
    * @param theta  Angle in XY plane, measured counterclockwise from X axis
    * @param phi    Angle in a plane containing the Z axis and the vector, measured counterclockwise from XY plane
    * @param r      Radius: distance from origin to tip of vector.
    */
    void getSpherical (T &theta, T &phi, T &r)
        DNB_THROW_SPEC_NULL;


    // SMW Support cylindrical coord

    /**
    * Sets the vector using cylindrical coordinates
    * @param theta  Angle in XY plane, measured counterclockwise from X axis
    * @param r      Radius in XY plane: distance between origin and tip of vector projected onto the XY plane
    * @param d      Distance along z axis; This is the same as the z coordinate of the vector
    */
    void setCylindrical( const T &theta, const T &r, const T &d )
        DNB_THROW_SPEC_NULL;

    /**
    * Gets the vector's spherical coordinates
    * @param theta  Angle in XY plane, measured counterclockwise from X axis
    * @param r      Radius in XY plane: distance between origin and tip of vector projected onto the XY plane
    * @param d      Distance along z axis; This is the same as the z coordinate of the vector
    */
    void getCylindrical (T &theta, T &r, T &d)
        DNB_THROW_SPEC_NULL;

/**
  * Constructs a three-dimensional vector from a scalar value.
  * @param  right
  * The value assigned to each element of <tt>self</tt>.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function assigns <tt>right</tt> to each element of <tt>self</tt>.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     vector( 1.0 );
  *
  * </pre>
  *
  */
    explicit
    DNBBasicVector3D( const T &right )
        DNB_THROW_SPEC_NULL;


/**
  * Constructs a three-dimensional vector from a scalar array.
  * @param  right
  * The pointer to the first element of an array which is assigned
  * to <tt>self</tt>.  The array must contain at least three elements.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function assigns <tt>right[0]</tt>, <tt>right[1]</tt>, and <tt>right[2]</tt> to the
  * corresponding elements of <tt>self</tt>.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBReal         array[ 3 ] = { 1.0, 2.0, 3.0 };
  * DNBVector3D     vector( array );
  *
  * </pre>
  *
  */
    explicit
    DNBBasicVector3D( const T *right )
        DNB_THROW_SPEC_NULL;


/**
  * Constructs a three-dimensional vector from an DNBBasicMathVector instance.
  * @param  right
  * An instance of the class DNBBasicMathVector<tt>T</tt> which is assigned to
  * <tt>self</tt>.  The vector must contain at least three elements.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function assigns the first three elements of <tt>right</tt> to the
  * corresponding elements of <tt>self</tt>.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBReal             array[ 3 ] = { 1.0, 2.0, 3.0 };
  * DNBBasicMathVector<DNBReal>  vector1( array, 3 );
  * DNBVector3D         vector2( vector1 );
  *
  * </pre>
  *
  */
    explicit
    DNBBasicVector3D( const DNBBasicMathVector<T> &right )
        DNB_THROW_SPEC_NULL;


/**
  * Destroys a three-dimensional vector.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function destroys a three-dimensional vector.
  *
  *
  */
    ~DNBBasicVector3D( )
        DNB_THROW_SPEC_NULL;


/**
  * Retrieves an iterator pointing to the beginning of <tt>self</tt>.
  *
  * @return
  * An iterator positioned at the first element of the vector.
  *
  * <br><B>Description</B><br>
  * This function retrieves an iterator positioned at the first element
  * of <tt>self</tt>.
  *
  *
  */
    iterator
    begin( )
        DNB_THROW_SPEC_NULL;


/**
  * Retrieves a const iterator pointing to the beginning of <tt>self</tt>.
  *
  * @return
  * A const iterator positioned at the first element of the vector.
  *
  * <br><B>Description</B><br>
  * This function retrieves a const iterator positioned at the first
  * element of <tt>self</tt>.
  *
  *
  */
    const_iterator
    begin( ) const
        DNB_THROW_SPEC_NULL;


/**
  * Retrieves an iterator pointing to the end of <tt>self</tt>.
  *
  * @return
  * An iterator positioned immediately after the last element of the
  * vector.
  *
  * <br><B>Description</B><br>
  * This function retrieves an iterator positioned immediately after the
  * last element of <tt>self</tt>.
  *
  *
  */
    iterator
    end( )
        DNB_THROW_SPEC_NULL;


/**
  * Retrieves a const iterator pointing to the end of <tt>self</tt>.
  *
  * @return
  * A const iterator positioned immediately after the last element of
  * the vector.
  *
  * <br><B>Description</B><br>
  * This function retrieves a const iterator positioned immediately
  * after the last element of <tt>self</tt>.
  *
  *
  */
    const_iterator
    end( ) const
        DNB_THROW_SPEC_NULL;


/**
  * Retrieves a reverse iterator pointing to the beginning of the
  * reversed scl_vector<tt>self</tt>.
  *
  * @return
  * A reverse iterator positioned at the last element of the vector.
  *
  * <br><B>Description</B><br>
  * This function retrieves a reverse iterator positioned at the last
  * element of <tt>self</tt>.
  *
  *
  */
    reverse_iterator
    rbegin( )
        DNB_THROW_SPEC_NULL;


/**
  * Retrieves a const reverse iterator pointing to the beginning of the
  * reversed scl_vector<tt>self</tt>.
  *
  * @return
  * A const reverse iterator positioned at the last element of the
  * vector.
  *
  * <br><B>Description</B><br>
  * This function retrieves a const reverse iterator positioned at the
  * last element of <tt>self</tt>.
  *
  *
  */
    const_reverse_iterator
    rbegin( ) const
        DNB_THROW_SPEC_NULL;


/**
  * Retrieves a reverse iterator pointing to the end of the reversed
  * scl_vector<tt>self</tt>.
  *
  * @return
  * A reverse iterator positioned immediately before the first element
  * of the vector.
  *
  * <br><B>Description</B><br>
  * This function retrieves a reverse iterator positioned immediately
  * before the first element of <tt>self</tt>.
  *
  *
  */
    reverse_iterator
    rend( )
        DNB_THROW_SPEC_NULL;


/**
  * Retrieves a const reverse iterator pointing to the end of the
  * reversed scl_vector<tt>self</tt>.
  *
  * @return
  * A const reverse iterator positioned immediately before the first
  * element of the vector.
  *
  * <br><B>Description</B><br>
  * This function retrieves a const reverse iterator positioned
  * immediately before the first element of <tt>self</tt>.
  *
  *
  */
    const_reverse_iterator
    rend( ) const
        DNB_THROW_SPEC_NULL;


    //*
    //* Function Set is deprecated; It is renamed to setCartesian to distinguish it from methods setCylindircal and setSpherical
    //*
    void
    set( const T &x, const T &y, const T &z )
        DNB_THROW_SPEC_NULL;

/**
  * Initializes the elements of <tt>self</tt> using  cartesian coordinates
  * @param  x
  * The x-component of the vector.
  * @param  y
  * The y-component of the vector.
  * @param  z
  * The z-component of the vector.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function copies <tt>x</tt>, <tt>y</tt>, and <tt>z</tt> to the corresponding elements
  * of <tt>self</tt>.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     vector;
  * vector.setCartesian( 1.0, 2.0, 3.0 );
  *
  * </pre>
  *
  */
    void setCartesian( const T &x, const T &y, const T &z )
        DNB_THROW_SPEC_NULL;

    //*
    //* Function get is deprecated. It is renamed to getCartesian to distinguish it from getCylindrical and get Spherical.
    //*
    //*
    void
    get( T &x, T &y, T &z ) const
        DNB_THROW_SPEC_NULL;

/**
  * Retrieves the cartesian coordinates of <self>.
  * @param  x
  * The x-component of the vector.
  * @param  y
  * The y-component of the vector.
  * @param  z
  * The z-component of the vector.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function copies the elements of <tt>self</tt> to <tt>x</tt>, <tt>y</tt>, and <tt>z</tt>.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     vector( 1.0, 2.0, 3.0 );
  * DNBReal         x, y, z;
  * vector.getCartesian( x, y, z );
  *
  * </pre>
  *
  */
    void
    getCartesian( T &x, T &y, T &z ) const
        DNB_THROW_SPEC_NULL;

/**
  * Swaps the elements of <self> with another three-dimensional vector.
  * @param  other
  * The three-dimensional vector whose elements are exchanged with
  * <tt>self</tt>.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function swaps each element of <tt>self</tt> with the corresponding
  * element of <tt>other</tt>.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     vector1( 1.0, 2.0, 3.0 );
  * DNBVector3D     vector2( 4.0, 5.0, 6.0 );
  * vector1.swap( vector2 );
  *
  * </pre>
  *
  */
    void
    swap( DNBBasicVector3D<T> &other )
        DNB_THROW_SPEC_NULL;


/**
  * Converts <self> into a unit vector.
  *
  * @return
  * Nothing.
  *
  * <br><B>Description</B><br>
  * This function converts <tt>self</tt> into a unit vector, while preserving
  * its direction.  The length of the vector is computed using the
  * L-2 norm.
  * @exception DNBEZeroVector
  * The length of <tt>self</tt> is zero.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     vector( 1.0, 2.0, 3.0 );
  * vector.normalize( );
  *
  * </pre>
  *
  */
    void
    normalize( )
        DNB_THROW_SPEC((DNBEZeroVector));


    /**
    * FUNCTION:
    *     operator()
    *
    * SUMMARY:
    *     Retrieves a reference to an element of <self>.
    *
    *
    *@param coordIndex
    *         [in] The index of the desired coordinate (e.g., DNBMath3D::X).
    *
    *@return
    *     A reference to component <coordIndex> of <self>.
    **/

   /** <br><B>Description</B><br>
    *     This function returns a reference to component <coordIndex> of
    *     <self>.
    *
    * @exception none.
    *
    * br><B>Example</B><br>
    *     DNBVector3D     vector;
    *     vector( DNBMath3D::X ) = 1.0;
    *     vector( DNBMath3D::Y ) = 2.0;
    *     vector( DNBMath3D::Z ) = 3.0;
    *     DNBReal         x = vector( DNBMath3D::X );
    */
    reference
    operator()( CoordinateIndex coordIndex )
        DNB_THROW_SPEC_NULL;


/**
  * Retrieves a constant reference to an element of <self>.
  * @param  coordIndex
  * The index of the desired coordinate (e.g., DNBMath3D::X).
  *
  * @return
  * A constant reference to component <tt>coordIndex</tt> of <tt>self</tt>.
  *
  * <br><B>Description</B><br>
  * This function returns a constant reference to component <tt>coordIndex</tt>
  * of <tt>self</tt>.
  *
  * <br><B>Example</B><br>
  * <pre>
  * const DNBVector3D   vector( 1.0, 2.0, 3.0 );
  * DNBReal     x = vector( DNBMath3D::X );
  *
  * </pre>
  *
  */
    const_reference
    operator()( CoordinateIndex coordIndex ) const
        DNB_THROW_SPEC_NULL;


    /**
    * Retrieves a reference to an element of <tt>self</tt>.
    *
    * @param coordIndex
    *        The index of the desired coordinate (e.g., DNBMath3D::X).
    *
    * @return
    *     A reference to component <coordIndex> of <tt>self</tt>.
    *<p>
    * <TAB INDENT=-10>
    *<b> DESCRIPTION:</b>
    *     This function returns a reference to component <coordIndex> of
    *     <tt>self</tt>.</p>
    *
    * @exception
    *    None.
    *<p>
    *<TAB ALIGN=left>
    * <b>EXAMPLE:</b>
    * <pre>
    *     DNBVector3D     vector;
    *     vector[ DNBMath3D::X ] = 1.0;
    *     vector[ DNBMath3D::Y ] = 2.0;
    *     vector[ DNBMath3D::Z ] = 3.0;
    *     DNBReal         x = vector[ DNBMath3D::X ];
    * </pre>
    */
    reference
    operator[]( CoordinateIndex coordIndex )
        DNB_THROW_SPEC_NULL;


/**
  * Retrieves a constant reference to an element of <self>.
  * @param  coordIndex
  * The index of the desired coordinate (e.g., DNBMath3D::X).
  *
  * @return
  * A constant reference to component <tt>coordIndex</tt> of <tt>self</tt>.
  *
  * <br><B>Description</B><br>
  * This function returns a constant reference to component <tt>coordIndex</tt>
  * of <tt>self</tt>.
  *
  * <br><B>Example</B><br>
  * <pre>
  * const DNBVector3D   vector( 1.0, 2.0, 3.0 );
  * DNBReal     x = vector[ DNBMath3D::X ];
  *
  * </pre>
  *
  */
    const_reference
    operator[]( CoordinateIndex coordIndex ) const
        DNB_THROW_SPEC_NULL;


/**
  * Retrieves a reference to an element of <self>.
  * @param  index
  * The index of the desired element (i.e., 0, 1, or 2).
  *
  * @return
  * A reference to element <tt>index</tt> of <tt>self</tt>.
  *
  * <br><B>Description</B><br>
  * This function returns a reference to element <tt>index</tt> of <tt>self</tt>.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     vector;
  * vector[ 0 ] = 1.0;
  * vector[ 1 ] = 2.0;
  * vector[ 2 ] = 3.0;
  * DNBReal         x = vector[ 0 ];
  *
  * </pre>
  *
  */
    reference
    operator[]( size_type index )
        DNB_THROW_SPEC_NULL;


/**
  * Retrieves a constant reference to an element of <self>.
  * @param  index
  * The index of the desired element (i.e., 0, 1, or 2).
  *
  * @return
  * A constant reference to element <tt>index</tt> of <tt>self</tt>.
  *
  * <br><B>Description</B><br>
  * This function returns a constant reference to element <tt>index</tt> of
  * <tt>self</tt>.
  *
  * <br><B>Example</B><br>
  * <pre>
  * const DNBVector3D   vector( 1.0, 2.0, 3.0 );
  * DNBReal     x = vector[ 0 ];
  *
  * </pre>
  *
  */
    const_reference
    operator[]( size_type index ) const
        DNB_THROW_SPEC_NULL;


/**
  * Computes the additive inverse of <self>.
  *
  * @return
  * The additive inverse of <tt>self</tt>.
  *
  * <br><B>Description</B><br>
  * This function returns the vector that results when operator- is
  * applied to each element of <tt>self</tt>.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     vector1( 1.0, 2.0, 3.0 );
  * DNBVector3D     vector2;
  * vector2 = -vector1;
  *
  * </pre>
  *
  */
    DNBBasicVector3D<T>
    operator-( ) const
        DNB_THROW_SPEC_NULL;


/**
  * Returns a copy of <self>.
  *
  * @return
  * A copy of <tt>self</tt>.
  *
  * <br><B>Description</B><br>
  * This function returns the vector that results when operator+ is
  * applied to each element of <tt>self</tt>.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     vector1( 1.0, 2.0, 3.0 );
  * DNBVector3D     vector2;
  * vector2 = +vector1;
  *
  * </pre>
  *
  */
    DNBBasicVector3D<T>
    operator+( ) const
        DNB_THROW_SPEC_NULL;


/**
  * Increments each element of <self>.
  *
  * @return
  * A reference to <tt>self</tt>
  *
  * <br><B>Description</B><br>
  * This function pre-increments each element of <tt>self</tt> and then returns
  * a reference to <tt>self</tt>.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     vector1( 1.0, 2.0, 3.0 );
  * DNBVector3D     vector2 = ++vector1;
  *
  * </pre>
  *
  */
    DNBBasicVector3D<T> &
    operator++( )
        DNB_THROW_SPEC_NULL;


/**
  * Increments each element of <self>.
  *
  * @return
  * The original value of <tt>self</tt>.
  *
  * <br><B>Description</B><br>
  * This function post-increments each element of <tt>self</tt> and then
  * returns the original value of <tt>self</tt>.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     vector1( 1.0, 2.0, 3.0 );
  * DNBVector3D     vector2 = vector1++;
  *
  * </pre>
  *
  */
    DNBBasicVector3D<T>
    operator++( int )
        DNB_THROW_SPEC_NULL;


/**
  * Decrements each element of <self>.
  *
  * @return
  * A reference to <tt>self</tt>
  *
  * <br><B>Description</B><br>
  * This function pre-decrements each element of <tt>self</tt> and then returns
  * a reference to <tt>self</tt>.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     vector1( 1.0, 2.0, 3.0 );
  * DNBVector3D     vector2 = --vector1;
  *
  * </pre>
  *
  */
    DNBBasicVector3D<T> &
    operator--( )
        DNB_THROW_SPEC_NULL;


/**
  * Decrements each element of <self>.
  *
  * @return
  * The original value of <tt>self</tt>.
  *
  * <br><B>Description</B><br>
  * This function post-decrements each element of <tt>self</tt> and then
  * returns the original value of <tt>self</tt>.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     vector1( 1.0, 2.0, 3.0 );
  * DNBVector3D     vector2 = vector1--;
  *
  * </pre>
  *
  */
    DNBBasicVector3D<T>
    operator--( int )
        DNB_THROW_SPEC_NULL;


/**
  * Copies a three-dimensional vector to <self>.
  * @param  right
  * The three-dimensional vector to be copied.
  *
  * @return
  * A reference to <tt>self</tt>.
  *
  * <br><B>Description</B><br>
  * This function copies each element of <tt>right</tt> to the corresponding
  * element of <tt>self</tt>.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     vector1( 1.0, 2.0, 3.0 );
  * DNBVector3D     vector2;
  * vector2 = vector1;
  *
  * </pre>
  *
  */
    DNBBasicVector3D<T> &
    operator=( const DNBBasicVector3D<T> &right )
        DNB_THROW_SPEC_NULL;


/**
  * Adds a three-dimensional vector to <self>.
  * @param  right
  * The three-dimensional vector to be added.
  *
  * @return
  * A reference to <tt>self</tt>.
  *
  * <br><B>Description</B><br>
  * This function adds each element of <tt>right</tt> to the corresponding
  * element of <tt>self</tt> and stores the result in <tt>self</tt>.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     vector1( 1.0, 2.0, 3.0 );
  * DNBVector3D     vector2( 4.0, 5.0, 6.0 );
  * vector2 += vector1;
  *
  * </pre>
  *
  */
    DNBBasicVector3D<T> &
    operator+=( const DNBBasicVector3D<T> &right )
        DNB_THROW_SPEC_NULL;


/**
  * Subtracts a three-dimensional vector from <self>.
  * @param  right
  * The three-dimensional vector to be subtracted.
  *
  * @return
  * A reference to <tt>self</tt>.
  *
  * <br><B>Description</B><br>
  * This function subtracts each element of <tt>right</tt> from the
  * corresponding element of <tt>self</tt> and stores the result in <tt>self</tt>.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     vector1( 1.0, 2.0, 3.0 );
  * DNBVector3D     vector2( 4.0, 5.0, 6.0 );
  * vector2 -= vector1;
  *
  * </pre>
  *
  */
    DNBBasicVector3D<T> &
    operator-=( const DNBBasicVector3D<T> &right )
        DNB_THROW_SPEC_NULL;


/**
  * Multiplies <self> by a three-dimensional vector.
  * @param  right
  * The three-dimensional vector to be multiplied.
  *
  * @return
  * A reference to <tt>self</tt>.
  *
  * <br><B>Description</B><br>
  * This function multiplies each element of <tt>self</tt> by the corresponding
  * element of <tt>right</tt> and stores the result in <tt>self</tt>.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     vector1( 1.0, 2.0, 3.0 );
  * DNBVector3D     vector2( 4.0, 5.0, 6.0 );
  * vector2 *= vector1;
  *
  * </pre>
  *
  */
    DNBBasicVector3D<T> &
    operator*=( const DNBBasicVector3D<T> &right )
        DNB_THROW_SPEC_NULL;


/**
  * Divides <self> by a three-dimensional vector.
  * @param  right
  * The three-dimensional vector which represents the divisor.
  *
  * @return
  * A reference to <tt>self</tt>.
  *
  * <br><B>Description</B><br>
  * This function divides each element of <tt>self</tt> by the corresponding
  * element of <tt>right</tt> and stores the result in <tt>self</tt>.  The function
  * does not detect divide-by-zero errors.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     vector1( 1.0, 2.0, 3.0 );
  * DNBVector3D     vector2( 4.0, 5.0, 6.0 );
  * vector2 /= vector1;
  *
  * </pre>
  *
  */
    DNBBasicVector3D<T> &
    operator/=( const DNBBasicVector3D<T> &right )
        DNB_THROW_SPEC_NULL;


/**
  * Assigns a scalar value to <self>.
  * @param  right
  * The scalar to be assigned.
  *
  * @return
  * A reference to <tt>self</tt>.
  *
  * <br><B>Description</B><br>
  * This function assigns <tt>right</tt> to each element of <tt>self</tt>.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     vector;
  * vector = 1.0;
  *
  * </pre>
  *
  */
    DNBBasicVector3D<T> &
    operator=( const T &right )
        DNB_THROW_SPEC_NULL;


/**
  * Adds a scalar to <self>.
  * @param  right
  * The scalar to be added.
  *
  * @return
  * A reference to <tt>self</tt>.
  *
  * <br><B>Description</B><br>
  * This function adds <tt>right</tt> to each element of <tt>self</tt> and stores the
  * result in <tt>self</tt>.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     vector( 1.0, 2.0, 3.0 );
  * vector += 3.0;
  *
  * </pre>
  *
  */
    DNBBasicVector3D<T> &
    operator+=( const T &right )
        DNB_THROW_SPEC_NULL;


/**
  * Subtracts a scalar from <self>.
  * @param  right
  * The scalar to be subtracted.
  *
  * @return
  * A reference to <tt>self</tt>.
  *
  * <br><B>Description</B><br>
  * This function subtracts <tt>right</tt> from each element of <tt>self</tt> and
  * stores the result in <tt>self</tt>.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     vector( 1.0, 2.0, 3.0 );
  * vector -= 3.0;
  *
  * </pre>
  *
  */
    DNBBasicVector3D<T> &
    operator-=( const T &right )
        DNB_THROW_SPEC_NULL;


/**
  * Multiplies <self> by a scalar.
  * @param  right
  * The scalar to be multiplied.
  *
  * @return
  * A reference to <tt>self</tt>.
  *
  * <br><B>Description</B><br>
  * This function multiplies each element of <tt>self</tt> by <tt>right</tt> and
  * stores the result in <tt>self</tt>.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     vector( 1.0, 2.0, 3.0 );
  * vector *= 3.0;
  *
  * </pre>
  *
  */
    DNBBasicVector3D<T> &
    operator*=( const T &right )
        DNB_THROW_SPEC_NULL;


/**
  * Divides <self> by a scalar.
  * @param  right
  * The scalar which represents the divisor.
  *
  * @return
  * A reference to <tt>self</tt>.
  *
  * <br><B>Description</B><br>
  * This function divides each element of <tt>self</tt> by <tt>right</tt> and stores
  * the result in <tt>self</tt>.  The function does not detect divide-by-zero
  * errors.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     vector( 1.0, 2.0, 3.0 );
  * vector /= 2.0;
  *
  * </pre>
  *
  */
    DNBBasicVector3D<T> &
    operator/=( const T &right )
        DNB_THROW_SPEC_NULL;


/**
  * Determines if <self> and another three-dimensional vector are
  * equivalent.
  * @param  right
  * The three-dimensional vector used in the comparison.
  *
  * @return
  * true if <tt>self</tt> and <tt>right</tt> are equivalent, and false otherwise.
  *
  * <br><B>Description</B><br>
  * This function returns true if <tt>self</tt> and <tt>right</tt> are equivalent, and
  * false otherwise.  To be equivalent, each element of <tt>self</tt> must
  * equal the corresponding element of <tt>right</tt>.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     vector1( 1.0, 2.0, 3.0 );
  * DNBVector3D     vector2( 1.0, 2.0, 3.0 );
  * if ( vector1 == vector2 )
  * cout << "vector1 and vector2 are equivalent" << endl;
  *
  * </pre>
  *
  */
    bool
    operator==( const DNBBasicVector3D<T> &right ) const
        DNB_THROW_SPEC_NULL;


/**
  * Determines if <self> and another three-dimensional vector are not
  * equivalent.
  * @param  right
  * The three-dimensional vector used in the comparison.
  *
  * @return
  * false if <tt>self</tt> and <tt>right</tt> are equivalent, and true otherwise.
  *
  * <br><B>Description</B><br>
  * This function returns false if <tt>self</tt> and <tt>right</tt> are equivalent,
  * and true otherwise.  To be equivalent, each element of <tt>self</tt> must
  * equal the corresponding element of <tt>right</tt>.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     vector1( 1.0, 2.0, 3.0 );
  * DNBVector3D     vector2( 1.0, 2.0, 3.1 );
  * if ( vector1 != vector2 )
  * cout << "vector1 and vector2 are not equivalent" << endl;
  *
  * </pre>
  *
  */
    bool
    operator!=( const DNBBasicVector3D<T> &right ) const
        DNB_THROW_SPEC_NULL;


/**
  * Converts <self> into an DNBBasicMathVector instance.
  *
  * @return
  * An instance of the class DNBBasicMathVector<tt>T</tt> which is initialized from
  * <tt>self</tt>.
  *
  * <br><B>Description</B><br>
  * This function returns an instance of the class DNBBasicMathVector<tt>T</tt> which
  * contains the elements of <tt>self</tt>.
  * @exception scl_bad_alloc
  * The DNBBasicMathVector instance could not be allocated because of
  * insufficient free memory.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D                  vector1( 1.0, 2.0, 3.0 );
  * DNBBasicMathVector<DNBReal>  vector2 = vector1;
  *
  * </pre>
  *
  */
    operator DNBBasicMathVector<T>() const
        DNB_THROW_SPEC((scl_bad_alloc));


#if DNB_HAS_FRIEND_TEMPLATE

/**
  * Adds a pair of three-dimensional vectors.
  * @param  left
  * The three-dimensional vector that represents the left operand.
  * @param  right
  * The three-dimensional vector that represents the right operand.
  *
  * @return
  * The three-dimensional vector corresponding to "left + right".
  *
  * <br><B>Description</B><br>
  * This function adds each element of <tt>left</tt> to the corresponding
  * element of <tt>right</tt> and returns the result.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     vector1( 1.0, 2.0, 3.0 );
  * DNBVector3D     vector2( 4.0, 5.0, 6.0 );
  * DNBVector3D     result = vector1 + vector2;
  *
  * </pre>
  *
  */
    friend ExportedByDNBMath DNBBasicVector3D<T>
    operator+( const DNBBasicVector3D<T> &left,
        const DNBBasicVector3D<T> &right)
        DNB_THROW_SPEC_NULL;


/**
  * Subtracts a pair of three-dimensional vectors.
  * @param  left
  * The three-dimensional vector that represents the left operand.
  * @param  right
  * The three-dimensional vector that represents the right operand.
  *
  * @return
  * The three-dimensional vector corresponding to "left - right".
  *
  * <br><B>Description</B><br>
  * This function subtracts each element of <tt>right</tt> from the
  * corresponding element of <tt>left</tt> and returns the result.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     vector1( 1.0, 2.0, 3.0 );
  * DNBVector3D     vector2( 4.0, 5.0, 6.0 );
  * DNBVector3D     result = vector1 - vector2;
  *
  * </pre>
  *
  */
    friend ExportedByDNBMath DNBBasicVector3D<T>
    operator-( const DNBBasicVector3D<T> &left,
        const DNBBasicVector3D<T> &right)
        DNB_THROW_SPEC_NULL;


/**
  * Multiplies a pair of three-dimensional vectors.
  * @param  left
  * The three-dimensional vector that represents the left operand.
  * @param  right
  * The three-dimensional vector that represents the right operand.
  *
  * @return
  * The three-dimensional vector corresponding to "left * right".
  *
  * <br><B>Description</B><br>
  * This function multiplies each element of <tt>left</tt> by the corresponding
  * element of <tt>right</tt> and returns the result.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     vector1( 1.0, 2.0, 3.0 );
  * DNBVector3D     vector2( 4.0, 5.0, 6.0 );
  * DNBVector3D     result = vector1 * vector2;
  *
  * </pre>
  *
  */
    friend ExportedByDNBMath DNBBasicVector3D<T>
    operator*( const DNBBasicVector3D<T> &left,
        const DNBBasicVector3D<T> &right)
        DNB_THROW_SPEC_NULL;


/**
  * Divides a pair of three-dimensional vectors.
  * @param  left
  * The three-dimensional vector that represents the left operand.
  * @param  right
  * The three-dimensional vector that represents the right operand.
  *
  * @return
  * The three-dimensional vector corresponding to "left / right".
  *
  * <br><B>Description</B><br>
  * This function divides each element of <tt>left</tt> by the corresponding
  * element of <tt>right</tt> and returns the result.  The function does not
  * detect divide-by-zero errors.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     vector1( 1.0, 2.0, 3.0 );
  * DNBVector3D     vector2( 4.0, 5.0, 6.0 );
  * DNBVector3D     result = vector1 / vector2;
  *
  * </pre>
  *
  */
    friend ExportedByDNBMath DNBBasicVector3D<T>
    operator/( const DNBBasicVector3D<T> &left,
        const DNBBasicVector3D<T> &right)
        DNB_THROW_SPEC_NULL;


/**
  * Adds a three-dimensional vector to a scalar.
  * @param  left
  * The three-dimensional vector that represents the left operand.
  * @param  right
  * The scalar that represents the right operand.
  *
  * @return
  * The three-dimensional vector corresponding to "left + right".
  *
  * <br><B>Description</B><br>
  * This function adds <tt>right</tt> to each element of <tt>left</tt> and returns the
  * result.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     vector1( 1.0, 2.0, 3.0 );
  * DNBVector3D     vector2 = vector1 + 5.0;
  *
  * </pre>
  *
  */
    friend ExportedByDNBMath DNBBasicVector3D<T>
    operator+( const DNBBasicVector3D<T> &left, const T &right )
        DNB_THROW_SPEC_NULL;


/**
  * Adds a scalar to a three-dimensional vector.
  * @param  left
  * The scalar that represents the left operand.
  * @param  right
  * The three-dimensional vector that represents the right operand.
  *
  * @return
  * The three-dimensional vector corresponding to "left + right".
  *
  * <br><B>Description</B><br>
  * This function adds <tt>left</tt> to each element of <tt>right</tt> and returns the
  * result.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     vector1( 1.0, 2.0, 3.0 );
  * DNBVector3D     vector2 = 5.0 + vector1;
  *
  * </pre>
  *
  */
    friend ExportedByDNBMath DNBBasicVector3D<T>
    operator+( const T &left, const DNBBasicVector3D<T> &right )
        DNB_THROW_SPEC_NULL;


/**
  * Subtracts a scalar from a three-dimensional vector.
  * @param  left
  * The three-dimensional vector that represents the left operand.
  * @param  right
  * The scalar that represents the right operand.
  *
  * @return
  * The three-dimensional vector corresponding to "left - right".
  *
  * <br><B>Description</B><br>
  * This function subtracts <tt>right</tt> from each element of <tt>left</tt> and
  * returns the result.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     vector1( 1.0, 2.0, 3.0 );
  * DNBVector3D     vector2 = vector1 - 5.0;
  *
  * </pre>
  *
  */
    friend ExportedByDNBMath DNBBasicVector3D<T>
    operator-( const DNBBasicVector3D<T> &left, const T &right )
        DNB_THROW_SPEC_NULL;


/**
  * Subtracts a three-dimensional vector from a scalar.
  * @param  left
  * The scalar that represents the left operand.
  * @param  right
  * The three-dimensional vector that represents the right operand.
  *
  * @return
  * The three-dimensional vector corresponding to "left - right".
  *
  * <br><B>Description</B><br>
  * This function subtracts each element of <tt>right</tt> from <tt>left</tt> and
  * returns the result.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     vector1( 1.0, 2.0, 3.0 );
  * DNBVector3D     vector2 = 5.0 - vector1;
  *
  * </pre>
  *
  */
    friend ExportedByDNBMath DNBBasicVector3D<T>
    operator-( const T &left, const DNBBasicVector3D<T> &right )
        DNB_THROW_SPEC_NULL;


/**
  * Multiplies a three-dimensional vector by a scalar.
  * @param  left
  * The three-dimensional vector that represents the left operand.
  * @param  right
  * The scalar that represents the right operand.
  *
  * @return
  * The three-dimensional vector corresponding to "left * right".
  *
  * <br><B>Description</B><br>
  * This function multiplies each element of <tt>left</tt> by <tt>right</tt> and
  * returns the result.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     vector1( 1.0, 2.0, 3.0 );
  * DNBVector3D     vector2 = vector1 * 5.0;
  *
  * </pre>
  *
  */
    friend ExportedByDNBMath DNBBasicVector3D<T>
    operator*( const DNBBasicVector3D<T> &left, const T &right )
        DNB_THROW_SPEC_NULL;


/**
  * Multiplies a scalar by a three-dimensional vector.
  * @param  left
  * The scalar that represents the left operand.
  * @param  right
  * The three-dimensional vector that represents the right operand.
  *
  * @return
  * The three-dimensional vector corresponding to "left * right".
  *
  * <br><B>Description</B><br>
  * This function multiplies each element of <tt>right</tt> by <tt>left</tt> and
  * returns the result.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     vector1( 1.0, 2.0, 3.0 );
  * DNBVector3D     vector2 = 5.0 * vector1;
  *
  * </pre>
  *
  */
    friend ExportedByDNBMath DNBBasicVector3D<T>
    operator*( const T &left, const DNBBasicVector3D<T> &right )
        DNB_THROW_SPEC_NULL;


/**
  * Divides a three-dimensional vector by a scalar.
  * @param  left
  * The three-dimensional vector that represents the left operand.
  * @param  right
  * The scalar that represents the right operand.
  *
  * @return
  * The three-dimensional vector corresponding to "left / right".
  *
  * <br><B>Description</B><br>
  * This function divides each element of <tt>left</tt> by <tt>right</tt> and returns
  * the result.  The function does not detect divide-by-zero errors.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     vector1( 1.0, 2.0, 3.0 );
  * DNBVector3D     vector2 = vector1 / 5.0;
  *
  * </pre>
  *
  */
    friend ExportedByDNBMath DNBBasicVector3D<T>
    operator/( const DNBBasicVector3D<T> &left, const T &right )
        DNB_THROW_SPEC_NULL;


/**
  * Divides a scalar by a three-dimensional vector.
  * @param  left
  * The scalar that represents the left operand.
  * @param  right
  * The three-dimensional vector that represents the right operand.
  *
  * @return
  * The three-dimensional vector corresponding to "left / right".
  *
  * <br><B>Description</B><br>
  * This function divides <tt>left</tt> by each element of <tt>right</tt> and returns
  * the result.  The function does not detect divide-by-zero errors.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     vector1( 1.0, 2.0, 3.0 );
  * DNBVector3D     vector2 = 5.0 / vector1;
  *
  * </pre>
  *
  */
    friend ExportedByDNBMath DNBBasicVector3D<T>
    operator/( const T &left, const DNBBasicVector3D<T> &right )
        DNB_THROW_SPEC_NULL;


/**
  * Writes a three-dimensional vector to a stream.
  * @param  ostr
  * The output stream that is to receive the data item.
  * @param  item
  * The three-dimensional vector to be inserted.
  *
  * @return
  * A reference to <tt>ostr</tt>.
  *
  * <br><B>Description</B><br>
  * This function writes the three-dimensional scl_vector<tt>item</tt> to the
  * output stream <tt>ostr</tt>, using the formatting conventions of the class
  * DNBBasicMathVector<tt>T</tt>.
  * @exception scl_bad_alloc
  * An internal buffer could not be allocated because of
  * insufficient free memory.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     vector1( 1.0, 2.0, 3.0 );
  * cout << vector1 << endl;
  *
  * </pre>
  *
  */
    friend ExportedByDNBMath ostream &
    operator<<( ostream &ostr, const DNBBasicVector3D<T> &item )
        DNB_THROW_SPEC((scl_bad_alloc));



/**
  * Computes the sum of the elements of a three-dimensional vector.
  * @param  right
  * The three-dimensional vector that represents the operand.
  *
  * @return
  * The sum of the elements of <tt>right</tt>.
  *
  * <br><B>Description</B><br>
  * This function returns the sum of the elements of <tt>right</tt>.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     vector1( 1.0, 2.0, 3.0 );
  * DNBReal         result = DNBSum( vector1 );
  *
  * </pre>
  *
  */
    friend ExportedByDNBMath T
    DNBSum( const DNBBasicVector3D<T> &right )
        DNB_THROW_SPEC_NULL;


/**
  * Computes the absolute value of a three-dimensional vector.
  * @param  right
  * The three-dimensional vector that represents the operand.
  *
  * @return
  * The absolute value of <tt>right</tt>.
  *
  * <br><B>Description</B><br>
  * This function returns a three-dimensional vector consisting of the
  * absolute values of <tt>right</tt>'s elements.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     vector1( -1.0, 2.0, -3.0 );
  * DNBVector3D     result = DNBAbs( vector1 );
  *
  * </pre>
  *
  */
    friend ExportedByDNBMath DNBBasicVector3D<T>
    DNBAbs( const DNBBasicVector3D<T> &right )
        DNB_THROW_SPEC_NULL;


/**
  * Computes the L-1 norm of a three-dimensional vector.
  * @param  right
  * The three-dimensional vector that represents the operand.
  *
  * @return
  * The L-1 norm of <tt>right</tt>.
  *
  * <br><B>Description</B><br>
  * This function returns the sum of the absolute values of <tt>right</tt>'s
  * elements.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     vector1( -1.0, 1.0, -2.0 );
  * DNBReal         result = DNBL1Norm( vector1 );
  *
  * </pre>
  *
  */
    friend ExportedByDNBMath T
    DNBL1Norm( const DNBBasicVector3D<T> &right )
        DNB_THROW_SPEC_NULL;


/**
  * Computes the L-2 norm (or Euclidean distance) of a three-dimensional
  * vector.
  * @param  right
  * The three-dimensional vector that represents the operand.
  *
  * @return
  * The L-2 norm of <tt>right</tt>.
  *
  * <br><B>Description</B><br>
  * This function returns the square root of the sum of squares of
  * <tt>right</tt>'s elements.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     vector1( -1.0, 1.0, -2.0 );
  * DNBReal         result = DNBL2Norm( vector1 );
  *
  * </pre>
  *
  */
    friend ExportedByDNBMath T
    DNBL2Norm( const DNBBasicVector3D<T> &right )
        DNB_THROW_SPEC_NULL;


/**
  * Computes the L-infinity norm of a three-dimensional vector.
  * @param  right
  * The three-dimensional vector that represents the operand.
  *
  * @return
  * The L-infinity norm of <tt>right</tt>.
  *
  * <br><B>Description</B><br>
  * This function returns the maximum of the absolute values of
  * <tt>right</tt>'s elements.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     vector1( -1.0, 1.0, -2.0 );
  * DNBReal         result = DNBLinfNorm( vector1 );
  *
  * </pre>
  *
  */
    friend ExportedByDNBMath T
    DNBLinfNorm( const DNBBasicVector3D<T> &right )
        DNB_THROW_SPEC_NULL;


/**
  * Computes the square of the L-2 norm of a three-dimensional vector.
  * @param  right
  * The three-dimensional vector that represents the operand.
  *
  * @return
  * The square of the L-2 norm of <tt>right</tt>.
  *
  * <br><B>Description</B><br>
  * This function returns the sum of squares of <tt>right</tt>'s elements.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     vector1( -1.0, 1.0, -2.0 );
  * DNBReal         result = DNBL2NormSqr( vector1 );
  *
  * </pre>
  *
  */
    friend ExportedByDNBMath T
    DNBL2NormSqr( const DNBBasicVector3D<T> &right )
        DNB_THROW_SPEC_NULL;


/**
  * Determines if a pair of three-dimensional vectors are "relatively"
  * close to one another.
  * @param  left
  * The three-dimensional vector that represents the left operand.
  * @param  right
  * The three-dimensional vector that represents the right operand.
  * @param  tolerance
  * The relative tolerance used in comparing <tt>left</tt> and <tt>right</tt>.
  *
  * @return
  * true if <tt>left</tt> and <tt>right</tt> are "relatively" close to one another,
  * and false otherwise.
  *
  * <br><B>Description</B><br>
  * This function returns the boolean result:
  *
  * || left - right ||
  * ---------------------------- <tt>= tolerance
  * max( ||left||, ||right|| )
  *
  * where ||@|| denotes the L-infinity norm.  If <tt>tolerance</tt> <tt>= 0, the
  * function determines if <tt>left</tt> is precisely equal to <tt>right</tt>.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     vector1( 1.0, 2.0, 3.0 );
  * DNBVector3D     vector2( 1.0, 2.0, 3.0001 );
  * if ( DNBNearRel(vector1, vector2, 0.001) )
  * cout << "vector1 and vector2 are equivalent" << endl;
  *
  * </pre>
  *
  */
    friend ExportedByDNBMath bool
    DNBNearRel( const DNBBasicVector3D<T> &left,
        const DNBBasicVector3D<T> &right, const T &tolerance )
        DNB_THROW_SPEC_NULL;


/**
  * Determines if a pair of three-dimensional vectors are "absolutely"
  * close to one another.
  * @param  left
  * The three-dimensional vector that represents the left operand.
  * @param  right
  * The three-dimensional vector that represents the right operand.
  * @param  tolerance
  * The absolute tolerance used in comparing <tt>left</tt> and <tt>right</tt>.
  *
  * @return
  * true if <tt>left</tt> and <tt>right</tt> are "absolutely" close to one another,
  * and false otherwise.
  *
  * <br><B>Description</B><br>
  * This function returns the boolean result:
  *
  * || left - right || <tt>= tolerance
  *
  * where ||@|| denotes the L-infinity norm.  If <tt>tolerance</tt> <tt>= 0, the
  * function determines if <tt>left</tt> is precisely equal to <tt>right</tt>.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     vector1( 1.0, 2.0, 3.0 );
  * DNBVector3D     vector2( 1.0, 2.0, 3.0001 );
  * if ( DNBNearAbs(vector1, vector2, 0.001) )
  * cout << "vector1 and vector2 are equivalent" << endl;
  *
  * </pre>
  *
  */
    friend ExportedByDNBMath bool
    DNBNearAbs( const DNBBasicVector3D<T> &left,
        const DNBBasicVector3D<T> &right, const T &tolerance )
        DNB_THROW_SPEC_NULL;


/**
  * Computes the dot product of two three-dimensional vectors.
  * @param  left
  * The three-dimensional vector that represents the left operand.
  * @param  right
  * The three-dimensional vector that represents the right operand.
  *
  * @return
  * The vector dot product of <tt>left</tt> and <tt>right</tt>.
  *
  * <br><B>Description</B><br>
  * This function returns the vector dot product of <tt>left</tt> and <tt>right</tt>.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     vector1( 0.0, 1.0, 2.0 );
  * DNBVector3D     vector2( 1.0, 3.0, 2.0 );
  * DNBReal         result = DNBDot( vector1, vector2 );
  *
  * </pre>
  *
  */
    friend ExportedByDNBMath T
    DNBDot( const DNBBasicVector3D<T> &left, const DNBBasicVector3D<T> &right )
        DNB_THROW_SPEC_NULL;


/**
  * Computes the cross product of two three-dimensional vectors.
  * @param  left
  * The three-dimensional vector that represents the left operand.
  * @param  right
  * The three-dimensional vector that represents the right operand.
  *
  * @return
  * The vector cross product of <tt>left</tt> and <tt>right</tt>.
  *
  * <br><B>Description</B><br>
  * This function returns the vector cross product of <tt>left</tt> and
  * <tt>right</tt>.
  *
  * <br><B>Example</B><br>
  * <pre>
  * DNBVector3D     vector1( 1.0, 2.0, 3.0 );
  * DNBVector3D     vector2( 4.0, 6.0, 8.0 );
  * DNBVector3D     result = DNBCross( vector1, vector2 );
  *
  * </pre>
  *
  */
    friend ExportedByDNBMath DNBBasicVector3D<T>
    DNBCross( const DNBBasicVector3D<T> &left,
        const DNBBasicVector3D<T> &right )
        DNB_THROW_SPEC_NULL;

#endif  /* DNB_HAS_FRIEND_TEMPLATE */


    static void
    lerp( DNBBasicVector3D<T> &result, const T &t,
	  const DNBBasicVector3D<T> &from, const DNBBasicVector3D<T> &to )
        DNB_THROW_SPEC_NULL;

/**
  * The zero vector.
  *
  * <br><B>Description</B><br>
  * This object defines a three-dimensional vector consisting of the
  * elements [ 0, 0, 0 ].
  *
  *
  */
    static const DNBBasicVector3D<T>    Zero;


/**
  * The unit-X vector.
  *
  * <br><B>Description</B><br>
  * This object defines a three-dimensional vector consisting of the
  * elements [ 1, 0, 0 ].
  *
  *
  */
    static const DNBBasicVector3D<T>    UnitX;


/**
  * The unit-Y vector.
  *
  * <br><B>Description</B><br>
  * This object defines a three-dimensional vector consisting of the
  * elements [ 0, 1, 0 ].
  *
  *
  */
    static const DNBBasicVector3D<T>    UnitY;


/**
  * The unit-Z vector.
  *
  * <br><B>Description</B><br>
  * This object defines a three-dimensional vector consisting of the
  * elements [ 0, 0, 1 ].
  *
  *
  */
    static const DNBBasicVector3D<T>    UnitZ;

#if DNB_HAS_FRIEND_TEMPLATE
private:
#else
public:
#endif
    T       data_[ 3 ];

    friend class DNBBasicXform3D<T>;
    friend class DNBBasicRotate3D<T>;
    friend class DNBBasicQtn3D<T>;
};




/**
  * A three-dimensional vector of type DNBReal.
  *
  * <br><B>Description</B><br>
  * This type definition represents the default three-dimensional vector
  * class used in the Xenon toolkit.
  *
  *
  */
typedef DNBBasicVector3D<DNBReal>   DNBVector3D;




//
//  Include the public definition file.
//
#if     defined(DNB_COMPILE_INSTANTIATE) || defined(_DNBVECTOR3D_CC_)
#include "DNBVector3D.cc"
#elif   defined(_WINDOWS_SOURCE)
/*
extern DNB_DEFINE_CLASS_INST ExportedByDNBMath
DNBBasicVector3D<float>; */

extern DNB_DEFINE_CLASS_INST ExportedByDNBMath
DNBBasicVector3D<double>;
#endif

#endif  /* _DNBVECTOR3D_H_ */
