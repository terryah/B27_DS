//==============================================================================
//  COPYRIGHT DASSAULT SYSTEMES 2005
//==============================================================================
/**
* @CAA2Level L0
* @CAA2Usage U0
*/
//*
//* FILE:
//*     SimTimeLimits.h   - public header file
//*
//* MODULE:
//*     SimTimeLimits     - public concrete class
//*
//* OVERVIEW:
//*     This module provides the definition of scl_numeric_limits<DNBSimTime>
//*     specialization.
//*
//* HISTORY:
//*     Author      Date        Purpose
//*     ------      ----        -------
//*     sdh         03/03/99    Original code
//*
//* REVIEWED:
//*     Reviewer    Date        Remarks
//*     --------    ----        -------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* TODO:
//*     Developer   Date        Suggestion
//*     ---------   ----        ----------
//*     xxx         mm/dd/yy    xxxxxx
//*
//* COPYRIGHT:
//*     Copyright (C) 1998-99 Deneb Robotics, Inc.
//*     All Rights Reserved.
//*
//* LICENSE:
//*     The software and information contained herein are proprietary to, and
//*     comprise valuable trade secrets of, Deneb Robotics, Inc., which intends
//*     to preserve as trade secrets such software and information.  This
//*     software is furnished pursuant to a written license agreement and may
//*     be used, copied, transmitted, and stored only in accordance with the
//*     terms of such license and with the inclusion of the above copyright
//*     notice.  This software and information or any other copies thereof may
//*     not be provided or otherwise made available to any other person.
//*
#if     DNB_HAS_PRAGMA_ONCE
#pragma once
#endif


#ifndef _DNBSIMTIMELIMITS_H_
#define _DNBSIMTIMELIMITS_H_


#include <DNBSystemBase.h>
#include <scl_limits.h>
#include <DNBSystemDefs.h>


#include <DNBMath.h>
#include <DNBSimTime.h>



DNB_DEFINE_CLASS_SPEC ExportedByDNBMath
scl_numeric_limits<DNBSimTime>
{
public:
    static const bool is_specialized;

    static DNBSimTime min ()
        DNB_THROW_SPEC_NULL;

    static DNBSimTime max ()
        DNB_THROW_SPEC_NULL;

    static const int digits;
    static const int digits10;

    static const bool is_signed;
    static const bool is_integer;
    static const bool is_exact;
    static const int  radix;

    static DNBSimTime epsilon     ()
        DNB_THROW_SPEC_NULL;

    static DNBSimTime round_error ()
        DNB_THROW_SPEC_NULL;

    static const int min_exponent;
    static const int min_exponent10;
    static const int max_exponent;
    static const int max_exponent10;

    static const bool has_infinity;
    static const bool has_quiet_NaN;
    static const bool has_signaling_NaN;
    static const bool has_denorm;
    static const bool has_denorm_loss;

    static DNBSimTime infinity      ()
        DNB_THROW_SPEC_NULL;

    static DNBSimTime quiet_NaN     ()
        DNB_THROW_SPEC_NULL;

    static DNBSimTime signaling_NaN ()
        DNB_THROW_SPEC_NULL;

    static DNBSimTime denorm_min    ()
        DNB_THROW_SPEC_NULL;

    static const bool is_iec559;
    static const bool is_bounded;
    static const bool is_modulo;

    static const bool traps;
    static const bool tinyness_before;

    static const scl_float_round_style round_style;
};


#endif  /* _DNBSIMTIMELIMITS_H_ */
