// COPYRIGHT DASSAULT SYSTEMES 2007
   AttachComponent("CAADrafting2DLInterfaces.tst");
   AddPrereqComponent("CATIAApplicationFrame",Public);
   AddPrereqComponent("ApplicationFrame",Public);
   AddPrereqComponent("System",Public);
   AddPrereqComponent("ObjectModelerBase",Public);
   AddPrereqComponent("ObjectSpecsModeler",Public);
   AddPrereqComponent("MecModInterfaces",Public);
   AddPrereqComponent("CATTPSInterfaces",Public);
   AddPrereqComponent("Drafting2DLInterfaces",Public);
   AddPrereqComponent("DraftingInterfaces",Public);
   AddPrereqComponent ("Dialog",Public);
   AddPrereqComponent ("DialogEngine",Public);
   AddPrereqComponent ("BatchInfrastructure",Public);
   AddPrereqComponent ("PrintBase",Public);
   AddPrereqComponent ("Mathematics",Public);

