//
// COPYRIGHT DASSAULT SYSTEMES 2001
//
// -->Prereq Components Declaration
AddPrereqComponent ("System"                     ,Public);
AddPrereqComponent ("ObjectModelerBase"          ,Public);
AddPrereqComponent ("ObjectSpecsModeler"         ,Public);
AddPrereqComponent ("LiteralFeatures"            ,Public);
AddPrereqComponent ("ProductStructure"           ,Public);
AddPrereqComponent ("MecModInterfaces"           ,Public);
AddPrereqComponent ("ElectricalInterfaces"       ,Public);
AddPrereqComponent ("ElecDeviceItf"             ,Public);
AddPrereqComponent ("ProductStructureInterfaces"             ,Public);
AddPrereqComponent ("InfInterfaces"             ,Public);
AddPrereqComponent ("GSMInterfaces"              ,Public);
AddPrereqComponent ("KnowledgeInterfaces"              ,Public);

