// COPYRIGHT DASSAULT SYSTEMES 2000

// -->Prereq Components Declaration


AddPrereqComponent("System"               ,Public) ;
AddPrereqComponent("ObjectModelerBase"    ,Public);
AddPrereqComponent("ObjectSpecsModeler"   ,Public);
AddPrereqComponent("DMAPSInterfaces"      ,Public);
AddPrereqComponent("ProductStructure",Public);
//
