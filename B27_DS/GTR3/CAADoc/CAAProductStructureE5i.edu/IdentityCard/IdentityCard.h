// COPYRIGHT DASSAULT SYSTEMES 2003

AddPrereqComponent("CATPDMBase", Public);
AddPrereqComponent("CATPDMBaseInterfaces", Public);
AddPrereqComponent("ProductStructureInterfaces", Public);
AddPrereqComponent("ObjectModelerBase" , Public);
AddPrereqComponent("System"            , Public);
AddPrereqComponent("ProductStructure", Public);
AddPrereqComponent("ObjectModelerInterfaces"    ,Public);
AddPrereqComponent("ApplicationFrame", Public);
AddPrereqComponent("Dialog", Public);
AddPrereqComponent("InteractiveInterfaces", Public);
AddPrereqComponent("DialogEngine", Public);
