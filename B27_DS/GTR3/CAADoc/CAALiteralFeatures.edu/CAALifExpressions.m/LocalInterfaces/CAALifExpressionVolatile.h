// COPYRIGHT DASSAULT SYSTEMES  1994 2002

#ifndef CAALifExpressionVolatile_H
#define CAALifExpressionVolatile_H

#include "CATCke.h"
// ================================================================
// 
// ABSTRACT
// --------
// 
// Illustrates how to manage volatile expressions.
//
// ===========================================================================


CATCke::Boolean CAALifExpressionVolatile();               
#endif








































































