// COPYRIGHT DASSAULT SYSTEMES  1994 2000

#ifndef CAALifParametersVolatile_H
#define CAALifParametersVolatile_H

#include "CATCke.h"
// ================================================================
// 
// ABSTRACT
// --------
// 
// Illustrates how to manage volatile parameters.
// Volatile parameters are literals that are not saved in any form. 
// Unlike persistent parameters, they cannot be retrieved when a document is unloaded. 
// Here are the parameter types examplified in this sample: 
//   1 ) Simple type parameters (integer, real, string and boolean)
//   2 ) Dimension type parameters (Length and Angle)
//   3 ) Magnitude type parameters other than Length and Angle
//
// ===========================================================================


CATCke::Boolean CAALifParametersVolatile();               
#endif








































































