// COPYRIGHT DASSAULT SYSTEMES 2002

// Code components
   AddPrereqComponent("System",             Public);
   AddPrereqComponent("ObjectModelerBase",  Public);
   AddPrereqComponent("ObjectSpecsModeler", Public);
   AddPrereqComponent("FeatureModelerExt", Public);
   AddPrereqComponent("ObjectModelerCATIA", Public);
   AddPrereqComponent("Dialog",             Public);
   AddPrereqComponent("MecModInterfaces",   Public);
   AddPrereqComponent("KnowledgeInterfaces",Public);
   AddPrereqComponent("ObjectSpecsLegacy",Public);

