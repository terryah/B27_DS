#ifndef CAALifFormula_H
#define CAALifFormula_H

// COPYRIGHT DASSAULT SYSTEMES  2000

class CATICkeParmFactory;
class CATIParmPublisher;

#include "CATCke.h"

// =========================================================================
//
// ABSTRACT
// --------
// 
// Illustrates how to:
//   * create parameters
//   * create a formula which extracts a substring from a string
//   * modify the formula expression
//   * retrieve and modify the activity of the formula.
//
// =========================================================================

CATCke::Boolean CAALifFormula(CATIParmPublisher*);  
CATCke::Boolean CAALifAsynchronousFormula(CATICkeParmFactory*);               

#endif








































































