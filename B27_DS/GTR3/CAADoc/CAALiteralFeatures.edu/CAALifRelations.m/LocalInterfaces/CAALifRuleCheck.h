#ifndef CAALifRuleCheck_H
#define CAALifRuleCheck_H

// COPYRIGHT DASSAULT SYSTEMES  2000

class CATIParmPublisher;

#include "CATCke.h"

// ======================================================================
// 
// ABSTRACT
// --------
//
// Illustrates how to:
//   * create the rule below:
//      If (Width > 9 m)
//        {v=Width*Length*Height}
//      else{v=100*surface*Length}
//   * display its body or expression
//   * create a check.
//
// ======================================================================

CATCke::Boolean CAALifRuleCheck(CATIParmPublisher*);               
#endif








































































