#
# COPYRIGHT DASSAULT SYSTEMES 2007
# CAACloPlacePart.m     

BUILT_OBJECT_TYPE=LOAD MODULE

LINK_WITH = CATObjectModelerBase \
            CATObjectSpecsModeler \
            CATProductStructure1 CATProductStructureInterfaces \
            CATMechanicalModeler \
            GSMItf \
            CAAPspUtilities \
            CATPspUUID \
            CATArrangementItfCPP \
            JS0CORBA \
            SystemUUID \
            CATMathematics \
