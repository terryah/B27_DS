#
# COPYRIGHT DASSAULT SYSTEMES 2010
# CAACloSpecPlacePart.m     

BUILT_OBJECT_TYPE=LOAD MODULE

LINK_WITH = CATObjectModelerBase \
            CATObjectSpecsModeler \
            CATProductStructure1 CATProductStructureInterfaces \
            CATMechanicalModeler \
            GSMItf \
            CAAPspUtilities \
            CATPspUUID \
            CATArrangementItfCPP \
            JS0CORBA \
            SystemUUID \
            CATMathematics CATCommonLayoutItfUUID \
