// COPYRIGHT DASSAULT SYSTEMES 2001
//
// CAACommonLayoutItf.edu -->Prereq Components Declaration
AddPrereqComponent("System"                       ,Public);
AddPrereqComponent("Mathematics"                  ,Public);
AddPrereqComponent("ObjectSpecsModeler"           ,Public);
AddPrereqComponent("ObjectModelerBase"            ,Public);
AddPrereqComponent("KnowledgeInterfaces"          ,Public);
AddPrereqComponent("ProductStructure"             ,Public);
AddPrereqComponent("ProductStructureInterfaces"   ,Public);
AddPrereqComponent("MechanicalModeler"            ,Public);
AddPrereqComponent("GSMInterfaces"                ,Public);
AddPrereqComponent("ComponentsCatalogsInterfaces" ,Public);
AddPrereqComponent("CATArrangementInterfaces"     ,Public);
AddPrereqComponent("CATPlantShipInterfaces"       ,Public);
AddPrereqComponent("CATPlantShipModeler"          ,Public);
AddPrereqComponent("CATCommonLayoutInterfaces"    ,Public);
AddPrereqComponent("CAAPlantShipInterfaces.edu"   ,Protected);

