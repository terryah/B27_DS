# COPYRIGHT DASSAULT SYSTEMES 2005
#======================================================================
# Imakefile for module CAAOBMInterfacesCmd.m
#======================================================================
#
#  May 2005  Creation: Code generated by the CAA wizard  auw
#======================================================================
#
# SHARED LIBRARY 
#
BUILT_OBJECT_TYPE=SHARED LIBRARY 
 
# DO NOT EDIT :: THE CAA2 WIZARDS WILL ADD CODE HERE
WIZARD_LINK_MODULES = JS0GROUP \
JS0GROUP JS0FM DI0PANV2 CATApplicationFrame  \ 
# END WIZARD EDITION ZONE

LINK_WITH = $(WIZARD_LINK_MODULES) \
CATBehaviorItf CATOBMItfCPP CATDialogEngine KnowledgeItf \
AD0XXBAS CATProductStructure1 CATObjectSpecsModeler 

# System dependant variables
#
OS = AIX
#
OS = HP-UX
#
OS = IRIX
#
OS = SunOS
#
OS = Windows_NT
