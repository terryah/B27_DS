//
// COPYRIGHT DASSAULT SYSTEMES 1999
//
AddPrereqComponent( "System"                      , Public ) ;
AddPrereqComponent( "CATIAV4Interfaces"           , Public ) ;
AddPrereqComponent( "CATIADataBasics"             , Public ) ;

AddPrereqComponent("ObjectModelerBase"            , Public ) ;
AddPrereqComponent("ObjectSpecsModeler"           , Public ) ;
AddPrereqComponent("MecModInterfaces"             , Public ) ;

AddPrereqComponent( "ObjectModelerCATIA"          , Public ) ;

AddPrereqComponent( "V5ToV4Geo"                   , Public ) ;
AddPrereqComponent( "GeometricObjects"            , Public ) ;
