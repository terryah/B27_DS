// COPYRIGHT DASSAULT SYSTEMES 2002
//===================================================================
//
// IdentityCard.h
// Supplies the list of prerequisite components for framework CAAxPDMInterfaces.edu
//
//===================================================================
//
// Usage notes:
//   For every prereq framework FW, use the syntax:
//   AddPrereqComponent ("FW", Public);
//
//===================================================================
AddPrereqComponent("System",Public);
AddPrereqComponent("ApplicationFrame",Public);
AddPrereqComponent("Dialog",Public);
AddPrereqComponent("ObjectModelerBase",Public); 
AddPrereqComponent("InteractiveInterfaces",Public);
AddPrereqComponent("Mathematics",Public);
AddPrereqComponent("PartInterfaces",Public);
AddPrereqComponent("ProductStructureUI",Public);
AddPrereqComponent("ProductStructureInterfaces",Public);
AddPrereqComponent("ProductStructure",Public);
AddPrereqComponent("CATxPDMInterfaces",Public); 
AddPrereqComponent("Visualization",Public);
AddPrereqComponent("VisualizationBase",Public);
AddPrereqComponent("DialogEngine",Public);
