// COPYRIGHT DASSAULT SYSTEMES 2000
//
//============================================================================================
//  Abstract:
//  ---------
//
//  Batch program which computes the inertia of subassemblies of a product in a *.CATProduct document
//
//  Illustrates :
//     Document loading in session
//     Access to subassemblies within document
//     Computation of inertia data
//
//============================================================================================
//  Usage:
//  ------
//
//  Type        : CAASpaComputeInertia inputDirectory inputFile.CATProduct
//
//  Inputs      :
//     inputDirectory        : directory containing the file
//     inputFile.CATProduct  : name of the file
//
//  Return Code :
//     0 : OK
//     1 : Bad input (argument number or file)
//     2 : Problem in loading the file
//     3 : Computation error
//
//============================================================================================
#ifndef CAASpaComputeInertia_H
#define CAASpaComputeInertia_H

#endif

