// COPYRIGHT DASSAULT SYSTEMES 2000

AddPrereqComponent("System",                     Protected);
AddPrereqComponent("Dialog",                     Protected);
AddPrereqComponent("ObjectModelerBase",          Protected);
AddPrereqComponent("ApplicationFrame",           Protected);
AddPrereqComponent("ProductStructure",           Protected);
AddPrereqComponent("SpaceAnalysisInterfaces",    Protected);
AddPrereqComponent("MecModInterfaces",           Protected);

// Ajout pour Pb SUN DEBUG en CXR9rel
AddPrereqComponent("ObjectSpecsModeler",         Protected);


