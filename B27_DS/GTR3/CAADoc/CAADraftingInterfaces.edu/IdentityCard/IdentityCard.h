// COPYRIGHT DASSAULT SYSTEMES 2000
   AttachComponent("CAADraftingInterfaces.tst");
   AddPrereqComponent("CATTPSInterfaces",Public);
   AddPrereqComponent("DraftingInterfaces",Public);
   AddPrereqComponent("SketcherInterfaces",Public);
   AddPrereqComponent("Mathematics",Public);
   AddPrereqComponent("NewTopologicalObjects",Public);
   AddPrereqComponent("GeometricObjects",Public);
   AddPrereqComponent("ProductStructure",Public);
   AddPrereqComponent("MecModInterfaces",Public);
   AddPrereqComponent("ObjectModelerBase",Public);
   AddPrereqComponent("ObjectSpecsModeler",Public);
   AddPrereqComponent("ApplicationFrame",Public);
   AddPrereqComponent("DialogEngine",Public);
   AddPrereqComponent("Dialog",Public);
   AddPrereqComponent ("VisualizationBase",Public);
   AddPrereqComponent("Visualization",Public);
   AddPrereqComponent("InfInterfaces",Public);   
   AddPrereqComponent("System",Public);




