// COPYRIGHT DASSAULT SYSTEMES 2007

// Local Framework
#include "CAAIMmrCCDataExtensionFactory.h"

IID IID_CAAIMmrCCDataExtensionFactory = { 0x84ad4b2e, 0xbd3c, 0x44ac,
{ 0xb9, 0xab, 0xdf, 0x76, 0xbe, 0x5c, 0x7, 0x5d } };

CATImplementInterface(CAAIMmrCCDataExtensionFactory, CATBaseUnknown);

