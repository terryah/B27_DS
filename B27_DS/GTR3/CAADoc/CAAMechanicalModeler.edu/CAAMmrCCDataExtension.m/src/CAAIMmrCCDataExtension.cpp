// COPYRIGHT DASSAULT SYSTEMES 2007

// Local Framework
#include "CAAIMmrCCDataExtension.h"

IID IID_CAAIMmrCCDataExtension = { 0x17532121, 0x67a0, 0x4da8,
{ 0x8e, 0x9, 0xe9, 0xa5, 0x2e, 0x3f, 0x84, 0xdc } };

CATImplementInterface(CAAIMmrCCDataExtension, CATBaseUnknown);

