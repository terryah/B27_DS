// COPYRIGHT DASSAULT SYSTEMES 2000

// Local Framework
#include "CAAIMmrCombinedCurve.h"

IID IID_CAAIMmrCombinedCurve = { 0x12ddfdc0, 0xdedb, 0x11d3, { 0xb8, 0x44, 0x00, 0x08, 0xc7, 0x3f, 0x25, 0xe8} };


CATImplementInterface(CAAIMmrCombinedCurve, CATBaseUnknown);

