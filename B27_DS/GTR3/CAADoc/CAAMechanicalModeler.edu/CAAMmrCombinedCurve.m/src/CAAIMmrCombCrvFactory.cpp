// COPYRIGHT DASSAULT SYSTEMES 2000

// Local Framework
#include "CAAIMmrCombCrvFactory.h"

IID IID_CAAIMmrCombCrvFactory = { 0x0a61c156, 0x71d8, 0x4be0, 
{ 0xa5, 0xac, 0x79, 0x7b, 0xd4, 0x54, 0xf8, 0x31} };

CATImplementInterface(CAAIMmrCombCrvFactory, CATBaseUnknown);

