#
# COPYRIGHT DASSAULT SYSTEMES 2000
#
BUILT_OBJECT_TYPE= LOAD MODULE
LINK_WITH= JS0GROUP \
           CATObjectModelerBase \
           CATObjectSpecsModeler \
           CATGeometricObjects CATCGMGeoMath\
           CATConstraintModelerItf \
           CATNewTopologicalObjects \
           CATMecModInterfaces \
           CATMechanicalModeler \
           CATVisualization CATViz\
					 KnowledgeItf
