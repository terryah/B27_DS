// COPYRIGHT DASSAULT SYSTEMES 2007
#ifdef  _WINDOWS_SOURCE
#ifdef  __CAAMmrMultiMeasureAndMeasureSet
#define ExportedByCAAMmrMultiMeasureAndMeasureSet     __declspec(dllexport)
#else
#define ExportedByCAAMmrMultiMeasureAndMeasureSet     __declspec(dllimport)
#endif
#else
#define ExportedByCAAMmrMultiMeasureAndMeasureSet
#endif
