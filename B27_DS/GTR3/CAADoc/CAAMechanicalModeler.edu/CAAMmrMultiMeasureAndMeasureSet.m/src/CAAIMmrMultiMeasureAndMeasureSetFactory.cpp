// COPYRIGHT DASSAULT SYSTEMES 2007

// Local Framework
#include "CAAIMmrMultiMeasureAndMeasureSetFactory.h"

IID IID_CAAIMmrMultiMeasureAndMeasureSetFactory = { 0xd23b2747, 0x6211, 0x45ab,
{ 0xb3, 0xbd, 0xd2, 0x8d, 0x97, 0x7, 0x14, 0xd2 } };
// {D23B2747-6211-45ab-B3BD-D28D970714D2}

CATImplementInterface(CAAIMmrMultiMeasureAndMeasureSetFactory, CATBaseUnknown);

