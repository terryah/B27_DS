// COPYRIGHT DASSAULT SYSTEMES 2007

// Local Framework
#include "CAAIMmrMultiMeasure.h"

IID IID_CAAIMmrMultiMeasure ={ 0xc4b6c336, 0xd178, 0x4419, 
                             { 0xb6, 0xd5, 0x35, 0x98, 0x70, 0x89, 0x6d, 0x4e } };
// {C4B6C336-D178-4419-B6D5-359870896D4E}

CATImplementInterface(CAAIMmrMultiMeasure, CATBaseUnknown);

