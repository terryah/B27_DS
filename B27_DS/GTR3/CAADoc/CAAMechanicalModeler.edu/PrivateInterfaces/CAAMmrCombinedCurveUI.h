// COPYRIGHT DASSAULT SYSTEMES 2000
#ifdef  _WINDOWS_SOURCE
#ifdef  __CAAMmrCombinedCurveUI
#define ExportedByCAAMmrCombinedCurveUI     __declspec(dllexport)
#else
#define ExportedByCAAMmrCombinedCurveUI     __declspec(dllimport)
#endif
#else
#define ExportedByCAAMmrCombinedCurveUI
#endif
