// COPYRIGHT DASSAULT SYSTEMES 2007
/**
 * GUID is Applicative Identifier
 * <br><b>Role:</b>This GUID is input to the copying process to associate it with the Mechanical Import, an end result ofcopy
 * with link. 
 * Finally, this GUID is specified to the retrieving mechanism, which 
 * authorizes us to retrieve the source feature, its aggregating Product Instance 
 * and its load status
**/


#ifndef CAAMmrGUIDForApplicativeImport_H
#define CAAMmrGUIDForApplicativeImport_H
 

GUID CAAMmrGUIDForApplicativeImport = {
    0x10a6b771,
    0x9b15,
    0x41a6,
    {0x98, 0xdb, 0x36, 0x3b, 0xf4, 0x44, 0x93, 0x82}
  };
#endif
