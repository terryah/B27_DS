
// COPYRIGHT DASSAULT SYSTEMES 1999
AddPrereqComponent("Material",               Public );
AddPrereqComponent("CATMatInterfaces",       Public );
AddPrereqComponent("ObjectModelerBase",      Public );
AddPrereqComponent("ObjectSpecsModeler",     Public );
AddPrereqComponent("MecModInterfaces",       Public);
AddPrereqComponent("System",                 Public );

