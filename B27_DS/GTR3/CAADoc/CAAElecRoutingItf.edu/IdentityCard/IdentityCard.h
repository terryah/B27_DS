// -->Prereq Components Declaration
AddPrereqComponent("System"                    ,Public);
AddPrereqComponent("Mathematics"               ,Public);
AddPrereqComponent("ObjectModelerBase"         ,Public);
AddPrereqComponent("ProductStructure"          ,Public);
AddPrereqComponent("ElectricalInterfaces"      ,Public);
AddPrereqComponent("ElecRoutingItf"            ,Public);
AddPrereqComponent("ObjectSpecsModeler"        ,Public);
AddPrereqComponent("KnowledgeInterfaces"       ,Public);
AddPrereqComponent("ProductStructureInterfaces",Public);



