/**
 * @fullreview GJA NPJ 03:09:11
 * @quickReview SAX 04:05:05
 * @quickReview SAX 04:06:24
 * @quickReview NPJ 05:10:19
 */

// COPYRIGHT DELMIA Corp 2003
//=============================================================================
// DNBWSQResourceIO.cpp
// This is an UseCase to create IZ's and IO's for Robots and create "Set" 
// and "Wait" signal activities and "Enter" and "Clear" activities
// under Operations in a Process Document.
//=============================================================================
// IGRIP on CAA2
//=============================================================================
// 05/09/2003   Creation                                           NPJ
// 05/05/2004	 Modified		Fixed mkCheckSource Errors				 NPJ
// 23/06/2004	 Modified		Use exposed methods     				 NPJ
//
//=============================================================================
#ifdef _WINDOWS_SOURCE
#define DIR_DELIMITER "\\"
#else
#define DIR_DELIMITER "/"
#endif

#include "CAADNBWSQIOUtils.h"

#include "DNBISetSignalActivityFactory.h"
#include "DNBIWaitSignalActivityFactory.h"
#include "DNBISetSignalActivity.h"
#include "DNBIWaitSignalActivity.h"

#include "CATUnicodeString.h"
#include "CATLISTV_CATBaseUnknown.h"
#include "CATDocumentServices.h"
#include "CATDocument.h"
#include "CATSession.h"

#include "CATIContainer.h"

#include "CATISPPChildManagement.h"
#include "CATISPPProcessContainer.h"
#include "CATGetEnvValue.h"
#include "CATLib.h"
#include "CATToken.h"

#include "DNBIIOLinks.h"
#include "DNBIIOLinksFactory.h"

#include "DNBIResourceIZoneFactory.h"
#include "DNBIResourceIZone.h"
#include "DNBIIZoneFactory.h"
#include "DNBIIZone.h"

#include "DNBIEnterZoneActivityFactory.h"
#include "DNBIEnterZoneActivity.h"
#include "DNBIClearZoneActivityFactory.h"
#include "DNBIClearZoneActivity.h"

#include "DNBIResourceIO.h"
#include "DNBIResourceIOFactory.h"
#include "DNBIIOItem.h"
#include "DNBIRobotTaskFactory.h"
#include "DNBIRobotTask.h"
#include "DNBITaskFactory.h"
#include "DNBIOperationFactory.h"
#include "DNBITask.h"
#include "DNBListOfRobotTask.h"
#include "DNBListOfOperations.h"
#include "DNBIOperation.h"

#include "CATIProduct.h"

#include "iostream.h"

int main(int argc, char * argv[] )
{ 
   HRESULT RC = E_FAIL;
   
   RC = DNBWSQCreateSession();
   if(RC == E_FAIL)
   {
      cout <<  "Session pointer could not be retrieved..." << endl;
      return 0;
   }
   
   char *Value = NULL;

   CATGetEnvValue("CATStartupPath", &Value);
	CATUnicodeString token(Value);

   if(Value)
   {
      free( Value );
      Value = NULL;
   }

   CATUnicodeString fileName = "ResourceIO.CATProcess";
   
   CATToken startupEnv(token );
	CATUnicodeString delim = ";";
   CATUnicodeString libPath= startupEnv.GetNextToken(delim);

   CATUnicodeString libFile = libPath;
   libFile.Append( DIR_DELIMITER );
   libFile.Append( fileName );
   
   RC = DNBWSQGetDocument(libFile);
   if(RC == E_FAIL)  
   {
      RC = DNBWSQDeleteSession();
      cout <<  "Document could not be retrieved..." << endl;
      return 0;
   }
   
   RC = DNBWSQInitialiseDocForBatch();
   if(RC == E_FAIL)  
   {
      RC = DNBWSQDeleteSession();
      cout <<  "Document not Initialized properly..." << endl; 
      return 0;
   }
   
   // find the Robots
   CATDocument *currentDoc = CATSession::GetCurrentDoc();
   if( currentDoc )
   {
      CATISPPProcessContainer *piProcessCont = (CATISPPProcessContainer* )
         currentDoc->GetRootContainer(CATISPPProcessContainer::ClassName());
      
      if ( piProcessCont )
      {
         CATListValCATBaseUnknown_var*	resourceCnxt = piProcessCont->GetResourceContext();
         
         if ( resourceCnxt && resourceCnxt->Size() > 1 )
         {
            DNBIIOItem * waitIOItem = NULL;
            DNBIIOItem * setIOItem = NULL;

            CATIProduct_var spec1( (*resourceCnxt)[1] );
            if(!!spec1)
            {
               //Create IZone with owner as spec1
               DNBIResourceIZoneFactory_var resZoneFact = spec1;
               if( NULL_var != resZoneFact )
               {        
                  DNBIResourceIZone* resZone = NULL;
                  HRESULT hr = resZoneFact->CreateIZoneForResource( &resZone );
                  if(resZone)
                  {
                     DNBIIZoneFactory_var iZoneFact = resZone;
                     if( NULL_var != iZoneFact )
                     {          
                        CATUnicodeString izName("Zone_1");
                        DNBIIZone* pIzone = NULL;
                        hr = iZoneFact->CreateInterferenceZone( izName, &pIzone ); 
                        if( SUCCEEDED(hr) && pIzone)
                        {
                           CATBaseUnknown_var unkIZ = pIzone;
                           hr = resZone->SetIZone( unkIZ );  
                           
                           DNBIResourceIO* IZResIO = NULL;
                           hr = pIzone->QueryInterface( IID_DNBIResourceIO, (void**)&IZResIO );
                           if( FAILED(hr) )
                           {
                              DNBIResourceIOFactory_var resFact( pIzone );
                              if( NULL_var != resFact )
                              {
                                 hr = resFact->CreateResourceIO( &IZResIO );
                              }
                           }

                           if(IZResIO )
                           {                                    
                              DNBIIOItem* IZIOItemOut = NULL;
                              IZResIO->GetOutputByName( izName, &IZIOItemOut );
                              if( NULL == IZIOItemOut )
                              {
                                 hr = IZResIO->CreateOutput( &IZIOItemOut );
                                 if( SUCCEEDED(hr) && NULL != IZIOItemOut )
                                 {    
                                    IZIOItemOut->SetName( izName );
                                    IZIOItemOut->SetType( DNBIIOItem::ioLONG );
                                 }
                              }
                              
                              DNBIIOItem* IZIOItemIn = NULL;
                              IZResIO->GetInputByName( izName, &IZIOItemIn );
                              if( NULL == IZIOItemIn )
                              {
                                 hr = IZResIO->CreateInput( &IZIOItemIn );
                                 if( SUCCEEDED(hr) && NULL != IZIOItemIn )
                                 { 
                                    IZIOItemIn->SetName( izName );
                                    IZIOItemIn->SetType( DNBIIOItem::ioLONG );  
                                 }
                              }
                              
                              if( IZIOItemOut && IZIOItemIn )
                              {
                                 // Get the IOLinks on the pIzone
                                 DNBIIOLinksFactory_var linkFact( pIzone );
                                 
                                 if ( NULL_var != linkFact )
                                 {
                                    DNBIIOLinks* ioLinks = NULL;
                                    HRESULT hr = linkFact->CreateIOLinks( &ioLinks );
                                    if( SUCCEEDED(hr) && NULL != ioLinks  )
                                    {
                                       for( int ii = 1; ii <= 2; ii++ )
                                       {
                                          CATIProduct_var unkProd = (*resourceCnxt)[ii];
                                          DNBIResourceIO* resIO = NULL;

                                          hr = unkProd->QueryInterface( IID_DNBIResourceIO, (void**)&resIO );
                                          if( FAILED(hr) )
                                          {
                                             DNBIResourceIOFactory_var resFact( unkProd );
                                             if( NULL_var != resFact )
                                             {
                                                hr = resFact->CreateResourceIO( &resIO );
                                             }
                                          }
                                               
                                          if( resIO )
                                          {
                                             DNBIIOItem* ioItemOut = NULL;
                                             hr = resIO->CreateOutput( &ioItemOut );
                                             if( SUCCEEDED(hr) && NULL != ioItemOut )
                                             {    
                                                ioItemOut->SetName( izName );
                                                ioItemOut->SetType( DNBIIOItem::ioLONG );
                                             
                                                hr = ioLinks->CreateLink( ioItemOut, IZIOItemIn );

                                                ioItemOut->Release();
                                                ioItemOut = NULL;
                                             }    
                                             
                                             DNBIIOItem* ioItemIn = NULL;
                                             hr = resIO->CreateInput( &ioItemIn );
                                             if( SUCCEEDED(hr) && NULL != ioItemIn )
                                             {    
                                                ioItemIn->SetName( izName );
                                                ioItemIn->SetType( DNBIIOItem::ioLONG );
                                             
                                                hr = ioLinks->CreateLink( IZIOItemOut, ioItemIn );

                                                ioItemIn->Release();
                                                ioItemIn = NULL;
                                             } 

                                             resIO->Release();
                                             resIO = NULL;
                                          }
                                       }

                                       ioLinks->Release();
                                       ioLinks = NULL;
                                    }
                                 }
                                 
                                 IZIOItemOut->Release();
                                 IZIOItemOut = NULL;
                                 
                                 IZIOItemIn->Release();
                                 IZIOItemIn = NULL;
                              }
                              
                              IZResIO->Release();
                              IZResIO = NULL;
                           }
                           
                           DNBITaskFactory_var spITaskFactory = pIzone;
                           if( NULL_var != spITaskFactory )
                           {
                              CATUnicodeString TaskName = "Task.1";
                              
                              DNBITask *pITask = NULL;
                              hr = spITaskFactory->CreateTask( TaskName, &pITask, "DNBRobotTask");
                              if( SUCCEEDED(hr) && NULL != pITask )
                              {
                                 DNBIRobotTask_var spRobotTask(pITask);
                                 DNBIOperation* pCreatedOperation = NULL;

                                 if(!!spRobotTask)
                                 {
                                    hr = spRobotTask->CreateOperation( NULL, NULL, &pCreatedOperation);
                                 }

                                 if(pCreatedOperation)
                                 {
                                    DNBISetSignalActivityFactory_var spIFactory( pCreatedOperation );
                                    if ( !!spIFactory )
                                    {
                                       CATISPPChildManagement_var spIChildMgt( pCreatedOperation );
                                       
                                       pCreatedOperation->Release();
                                       pCreatedOperation = NULL;

                                       DNBISetSignalActivity* pISetSignalAct = NULL;
                                       HRESULT hr = spIFactory->CreateChildSetSignalActivity(spIChildMgt, &pISetSignalAct );
                                       if( SUCCEEDED(hr) && NULL != pISetSignalAct )
                                       {
                                          DNBIResourceIO_var spResIO = pIzone;
                                          DNBIIOItem* ioItem = NULL;
                                          if(!!spResIO)
                                          {
                                             spResIO->GetOutputByName( izName, &ioItem );
                                          }
                                          if(ioItem)
                                             pISetSignalAct->SetIOItem( ioItem );
                                          
                                          DNBIWaitSignalActivityFactory_var spIFactory( pISetSignalAct );
                                          if ( !!spIFactory )
                                          {
                                             CATISPPChildManagement_var spIChildMgt( pISetSignalAct );
                                             
                                             DNBIWaitSignalActivity* pIWaitSignalAct = NULL;
                                             HRESULT hr = spIFactory->CreateChildWaitSignalActivity(spIChildMgt, &pIWaitSignalAct );
                                             if( SUCCEEDED(hr) && NULL != pIWaitSignalAct )
                                             {
                                                DNBIResourceIO_var spResIO = pIzone;
                                                DNBIIOItem* waitIOItem = NULL;
                                                if(!!spResIO)
                                                {
                                                   spResIO->GetInputByName( izName, &waitIOItem );
                                                }
                                                pIWaitSignalAct->SetIOItem( waitIOItem );
                                               
                                                DNBISetSignalActivityFactory_var spIFactory( pIWaitSignalAct );
                                                if ( !!spIFactory )
                                                {
                                                   CATISPPChildManagement_var spIChildMgt( pIWaitSignalAct );
                                                   
                                                   DNBISetSignalActivity* pISetSignalAct = NULL;
                                                   HRESULT hr = spIFactory->CreateChildSetSignalActivity(spIChildMgt, &pISetSignalAct );
                                                   if( SUCCEEDED(hr) && NULL != pISetSignalAct )
                                                   {
                                                      DNBIResourceIO_var spResIO = pIzone;
                                                      DNBIIOItem* ioItem = NULL;
                                                      if(!!spResIO)
                                                      {
                                                         spResIO->GetOutputByName( izName, &ioItem );
                                                      }
                                                      if(ioItem)
                                                         pISetSignalAct->SetIOItem( ioItem );
                                                    
                                                      DNBIWaitSignalActivityFactory_var spIFactory( pISetSignalAct );
                                                      if ( !!spIFactory )
                                                      {
                                                         CATISPPChildManagement_var spIChildMgt( pISetSignalAct );
                                                         
                                                         DNBIWaitSignalActivity* pIWaitSignalAct = NULL;
                                                         HRESULT hr = spIFactory->CreateChildWaitSignalActivity(spIChildMgt, &pIWaitSignalAct );
                                                         if( SUCCEEDED(hr) && NULL != pIWaitSignalAct )
                                                         {
                                                            DNBIResourceIO_var spResIO = pIzone;
                                                            DNBIIOItem* waitIOItem = NULL;
                                                            if(!!spResIO)
                                                            {
                                                               spResIO->GetInputByName( izName, &waitIOItem );
                                                            }
                                                            pIWaitSignalAct->SetIOItem( waitIOItem );
                                                            
                                                            pIWaitSignalAct->Release();
                                                            pIWaitSignalAct = NULL;
                                                         }
                                                      }
                                                    
                                                      pISetSignalAct->Release();
                                                      pISetSignalAct = NULL;
                                                   }
                                                }
                                             
                                                pIWaitSignalAct->Release();
                                                pIWaitSignalAct = NULL;
                                             }
                                          }
                                          
                                          pISetSignalAct->Release();
                                          pISetSignalAct = NULL;
                                       }
                                    }
                                 }
                              }
                           }
                           
                           hr = pIzone->SetResourceInZone( (*resourceCnxt)[1] );
                           hr = pIzone->SetResourceInZone( (*resourceCnxt)[2] );
                           
                           pIzone->Release();
                           pIzone = NULL;
                        }
                     }

                     resZone->Release();
                     resZone = NULL;
                  }
               }

               //Robot1
               DNBIResourceIOFactory_var fact( spec1 );
               if(!!fact)
               {
                  DNBIResourceIO * iResourceIO = NULL;
                  HRESULT hr = fact->CreateResourceIO( &iResourceIO ); 
                  if(iResourceIO)
                  {  
                     hr = iResourceIO->CreateOutput( &setIOItem );
                     
                     if ( SUCCEEDED(hr) && setIOItem != NULL )
                     {
                        int ioItemNumber = 1;
                        CATUnicodeString name("setIOItem");
                        
                        setIOItem->SetName( name );
                        setIOItem->SetNumber( ioItemNumber );
                        setIOItem->SetType( DNBIIOItem::ioLONG );
                        setIOItem->SetResource( iResourceIO );
                        
                        DNBIRobotTaskFactory_var spTaskFact = spec1;
                        if(!!spTaskFact)
                        {
                           DNBListOfRobotTask RobotTaskList;
                           spTaskFact->GetAllRobotTasks(RobotTaskList);
                           if( RobotTaskList.Size() > 0 )
                           {
                              DNBIRobotTask_var spRobotTask( (RobotTaskList)[1] );
                              if(!!spRobotTask)
                              {
                                 DNBListOfOperations OperationList;
                                 spRobotTask->GetAllOperations(OperationList);
                                 if( OperationList.Size() > 1 )
                                 {
                                    DNBIOperation_var spOperation( (OperationList)[1] );
                                    if(!!spOperation)
                                    {
                                       DNBISetSignalActivityFactory_var spIFactory( spOperation );
                                       if ( !!spIFactory )
                                       {
                                          CATISPPChildManagement_var spIChildMgt( spOperation );
                                          
                                          DNBISetSignalActivity* pISetSignalAct = NULL;
                                          HRESULT hr = spIFactory->CreateChildSetSignalActivity(spIChildMgt, &pISetSignalAct );
                                          if( SUCCEEDED(hr) && NULL != pISetSignalAct )
                                          {
                                             pISetSignalAct->SetIOItem( setIOItem );

                                             pISetSignalAct->Release();
                                             pISetSignalAct = NULL;
                                          }
                                       }

                                       //Create ENTER ZONE Activity

                                       DNBIEnterZoneActivityFactory_var spIezFact( spOperation );
                                       if ( NULL_var != spIezFact )
                                       {
                                          CATISPPChildManagement_var spIChildMgt( spOperation );
                                          
                                          DNBIEnterZoneActivity* pIEnterZoneAct = NULL;
                                          
                                          HRESULT hr = spIezFact->CreateChildEnterZoneActivity(spIChildMgt, &pIEnterZoneAct );
                                          if( SUCCEEDED(hr) && NULL != pIEnterZoneAct )
                                          {
                                             DNBIResourceIZone_var spResZone = spec1;
                                             if(!!spResZone)
                                             {
                                                CATListValCATBaseUnknown_var Zones;
                                                spResZone->GetIZones( Zones );
                                                if(Zones.Size() > 0)
                                                {
                                                   DNBIIZone_var spZone = ((Zones)[1]);
                                                   if(!!spZone)
                                                   {
                                                      pIEnterZoneAct->SetOwnerZone( spZone );
                                                   }
                                                }
                                             }

                                             DNBIWaitSignalActivityFactory_var spIFactory( pIEnterZoneAct );
                                             if ( !!spIFactory )
                                             {
                                                CATISPPChildManagement_var spIChildMgt( pIEnterZoneAct );
                                                
                                                DNBIWaitSignalActivity* pIWaitSignalAct = NULL;
                                                HRESULT hr = spIFactory->CreateChildWaitSignalActivity(spIChildMgt, &pIWaitSignalAct );
                                                if( SUCCEEDED(hr) && NULL != pIWaitSignalAct )
                                                {
                                                   DNBIResourceIO_var resIO = spec1;
                                                   
                                                   if( NULL_var != resIO )
                                                   {
                                                      CATUnicodeString itemName("Zone_1");
                                                      DNBIIOItem* ioItem = NULL;
                                                      resIO->GetInputByName(itemName, &ioItem );
                                                      if(ioItem)
                                                         pIWaitSignalAct->SetIOItem( ioItem );
                                                   }
                                                   
                                                   pIWaitSignalAct->Release();
                                                   pIWaitSignalAct = NULL;
                                                }
                                             }

                                             DNBISetSignalActivityFactory_var spISetIOFact( pIEnterZoneAct );
                                             if ( NULL_var != spISetIOFact )
                                             {
                                                CATISPPChildManagement_var spIChildMgt( pIEnterZoneAct );
                                                DNBISetSignalActivity* pISetIOAct = NULL;
                                                hr = spISetIOFact->CreateChildSetSignalActivity( spIChildMgt, &pISetIOAct );
                                                if( SUCCEEDED(hr) && NULL != pISetIOAct )
                                                {
                                                   DNBIResourceIO_var resIO = spec1;
                                                   
                                                   if( NULL_var != resIO )
                                                   {
                                                      CATUnicodeString itemName("Zone_1");
                                                      DNBIIOItem* ioItem = NULL;
                                                      resIO->GetOutputByName(itemName, &ioItem );
                                                      if(ioItem)
                                                         pISetIOAct->SetIOItem( ioItem );
                                                   }

                                                   pISetIOAct->Release();
                                                   pISetIOAct = NULL;
                                                }
                                             }   
                                          }
                                       }

                                       //Create CLEAR ZONE Activity

                                       DNBIClearZoneActivityFactory_var spClZonFact( (OperationList)[2]);
                                       if ( NULL_var != spClZonFact )
                                       {
                                          CATISPPChildManagement_var spIChildMgt( (OperationList)[2] );
                                          
                                          DNBIClearZoneActivity* pIClearZoneAct = NULL;
                                          
                                          HRESULT hr = spClZonFact->CreateChildClearZoneActivity(spIChildMgt, &pIClearZoneAct );
                                          if( SUCCEEDED(hr) && NULL != pIClearZoneAct )
                                          {
                                             DNBIResourceIZone_var spResZone = spec1;
                                             if(!!spResZone)
                                             {
                                                CATListValCATBaseUnknown_var Zones;
                                                spResZone->GetIZones( Zones );
                                                if(Zones.Size() > 0)
                                                {
                                                   DNBIIZone_var spZone = ((Zones)[1]);
                                                   if(!!spZone)
                                                   {
                                                      pIClearZoneAct->SetOwnerZone( spZone );
                                                   }
                                                }
                                             }

                                             DNBISetSignalActivityFactory_var spISetIOFact( pIClearZoneAct );
                                             if ( NULL_var != spISetIOFact )
                                             {
                                                CATISPPChildManagement_var spIChildMgt( pIClearZoneAct );
                                                DNBISetSignalActivity* pISetIOAct = NULL;
                                                hr = spISetIOFact->CreateChildSetSignalActivity( spIChildMgt, &pISetIOAct );
                                                if( SUCCEEDED(hr) && NULL != pISetIOAct )
                                                {
                                                   DNBIResourceIO_var resIO = spec1;
                                                   
                                                   if( NULL_var != resIO )
                                                   {
                                                      CATUnicodeString itemName("Zone_1");
                                                      DNBIIOItem* ioItem = NULL;
                                                      resIO->GetOutputByName(itemName, &ioItem );
                                                      if(ioItem)
                                                         pISetIOAct->SetIOItem( ioItem );
                                                   }

                                                   pISetIOAct->Release();
                                                   pISetIOAct = NULL;
                                                }
                                             }   
                                          }
                                       }
                                    }
                                 }

                                 int OpSize = OperationList.Size();
                                 
                                 for(int ii=1;ii<=OpSize;ii++)
                                 {
                                    DNBIOperation_var spOperation(OperationList[ii]);
												if(!!spOperation)
												{
													spOperation->Release();
													spOperation = NULL;
												}
                                 }
                              }
                           }

                           int TaskSize = RobotTaskList.Size();
                           
                           for(int ii=1;ii<=TaskSize;ii++)
                           {
                              DNBIRobotTask_var spRobotTask(RobotTaskList[ii]);
										if(!!spRobotTask)
										{
											spRobotTask->Release();
											spRobotTask = NULL;
										}
                           }
                        } 
                     } 

                     iResourceIO->Release();
                     iResourceIO = NULL;
                  }
               }
            }
            
            //Robot2
            CATIProduct_var spec2( (*resourceCnxt)[2] );
            if(!!spec2)
            {
               DNBIResourceIOFactory_var fact( spec2 );
               if(!!fact)
               {
                  DNBIResourceIO * iResourceIO = NULL;
                  HRESULT hr = fact->CreateResourceIO( &iResourceIO );
                  if(iResourceIO)
                  {
                     hr = iResourceIO->CreateInput( &waitIOItem );
                     
                     if ( SUCCEEDED(hr) && waitIOItem != NULL )
                     {
                        int ioItemNumber = 2;
                        CATUnicodeString name("setIOItem");
                        
                        waitIOItem->SetName( name );
                        waitIOItem->SetNumber( ioItemNumber );
                        waitIOItem->SetType( DNBIIOItem::ioLONG );
                        waitIOItem->SetResource( iResourceIO );

                        DNBIRobotTaskFactory_var spTaskFact = spec2;
                        if(!!spTaskFact)
                        {
                           DNBListOfRobotTask RobotTaskList;
                           spTaskFact->GetAllRobotTasks(RobotTaskList);
                           if( RobotTaskList.Size() > 0 )
                           {
                              DNBIRobotTask_var spRobotTask( (RobotTaskList)[1] );
                              if(!!spRobotTask)
                              {
                                 DNBListOfOperations OperationList;
                                 spRobotTask->GetAllOperations(OperationList);
                                 if( OperationList.Size() > 0 )
                                 {
                                    DNBIOperation_var spOperation( (OperationList)[1] );
                                    if(!!spOperation)
                                    {
                                       DNBIWaitSignalActivityFactory_var spIFactory( spOperation );
                                       if ( !!spIFactory )
                                       {
                                          CATISPPChildManagement_var spIChildMgt( spOperation );
                                          
                                          DNBIWaitSignalActivity* pIWaitSignalAct = NULL;
                                          HRESULT hr = spIFactory->CreateChildWaitSignalActivity(spIChildMgt, &pIWaitSignalAct );
                                          if( SUCCEEDED(hr) && NULL != pIWaitSignalAct )
                                          {
                                             pIWaitSignalAct->SetIOItem( waitIOItem );

                                             pIWaitSignalAct->Release();
                                             pIWaitSignalAct = NULL;
                                          }
                                       }
                                    }
                                 }

                                 int OpSize = OperationList.Size();
                                 
                                 for(int ii=1;ii<=OpSize;ii++)
                                 {
                                    DNBIOperation_var spOperation(OperationList[ii]);
												if(!!spOperation)
												{
													spOperation->Release();
													spOperation = NULL;
												}
                                 }
                              }
                           }

                           int TaskSize = RobotTaskList.Size();
                           
                           for(int ii=1;ii<=TaskSize;ii++)
                           {
                              DNBIRobotTask_var spRobotTask(RobotTaskList[ii]);
										if(!!spRobotTask)
										{
											spRobotTask->Release();
											spRobotTask = NULL;
										}
                           }
                        }    
                     }

                     iResourceIO->Release();
                     iResourceIO = NULL;
                  }
               }
            }

            //Create Link between the IO Items
            if(!!spec2)
            {
               DNBIIOLinksFactory_var iofact( spec2 );
               if(!!iofact)
               {
                  DNBIIOLinks * iIOLinks = NULL;
                  HRESULT hr = iofact->CreateIOLinks( &iIOLinks );
                  if(iIOLinks)
                  {
                     if(setIOItem && waitIOItem)
                     {
                        hr = iIOLinks->CreateLink( setIOItem, waitIOItem );
                        iIOLinks->Release();
                        iIOLinks = NULL;
                     }
                  }
               }
            }

            if(setIOItem)
            {
               setIOItem->Release();
               setIOItem = NULL;
            }
            if(waitIOItem)
            {
               waitIOItem->Release();
               waitIOItem = NULL; 
            }

            delete resourceCnxt;
            resourceCnxt = NULL;
         }
         
         piProcessCont->Release();
         piProcessCont = NULL;
      }

      
      char *Value = NULL;
      
      CATGetEnvValue("CATStartupPath", &Value);
      CATUnicodeString token(Value); 
      
      if(Value)
      {
         free( Value );
         Value = NULL;
      }

		CATUnicodeString fileName = "Test";
		
		CATToken startupEnv(token );
		CATUnicodeString delim = ";";
		CATUnicodeString libPath= startupEnv.GetNextToken(delim);
		
		CATUnicodeString libFile = libPath;
		libFile.Append( DIR_DELIMITER );
		libFile.Append( fileName );
		
		CATUnicodeString Format("CATProcess");
		
		HRESULT rc = CATDocumentServices::SaveAs( *currentDoc, libFile, Format, FALSE );
		
      if(rc != S_OK)
      {
         cout << "File Not Saved..." << endl;
      }
   }
   
   RC = DNBWSQDeleteSession();
   if(RC == E_FAIL)
      cout << "No session is currently running" << endl;  
   
   return 0;
}    


