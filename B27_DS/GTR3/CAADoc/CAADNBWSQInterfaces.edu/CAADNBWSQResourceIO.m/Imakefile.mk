# COPYRIGHT DASSAULT SYSTEMES 2003
#======================================================================
# Imakefile for module DNBWSQTestResourceIO.m
#======================================================================
#
#  Sep 2003  Creation: Code generated by the CAA wizard  nphanira
#======================================================================
#
# LOAD MODULE 
#
BUILT_OBJECT_TYPE=LOAD MODULE

COMMON_LINK_WITH =	JS0FM JS0GROUP \
					CAADNBWSQIOUtils \
					AC0XXLNK AD0XXBAS \
					AS0STARTUP ProcessInterfaces \
					DNBIgpSetupItfCPP \
					DNBSimIOItfCPP \
					DNBWSQItfCPP \
					DNBResourceProgramItfCPP AC0SPBAS

LINK_WITH           = $(COMMON_LINK_WITH)
					
#
# Define the build options for the current module.
#
OS      = Windows_NT
BUILD   = YES

OS      = IRIX
BUILD   = YES

OS      = SunOS
BUILD   = YES

OS      = AIX
BUILD   = YES

OS      = HP-UX
BUILD   = YES

OS      = win_a
BUILD   = NO

