/**
 * @fullreview GJA NPJ 03:09:11
 * @quickReview SAX 04:05:05
 */

// COPYRIGHT DELMIA Corp. 2003
//=============================================================================
// CAADNBWSQIOUtils.cpp
//=============================================================================
// IGRIP on CAA2
//=============================================================================
// 05/09/2003   Creation																		NPJ
// 05/05/2004	 Modified		Replace Deprecated methods with exposed methods	NPJ
//
//=============================================================================

#include <iostream.h>

//System
#include "CATUnicodeString.h"
#include "CATLib.h"

//ApplicationFrame
#include "CATInit.h"

//ObjectModelerBase
#include "CATSession.h"
#include "CATDocumentServices.h"
#include "CATSessionServices.h"

//local
#include "CAADNBWSQIOUtils.h"

//DMAPSInterfaces
#include "CATISPPProcessContainer.h"
#include "CATISPPActivityRoot.h"

#include <stdlib.h>

//-------------------------------------------------------------------------
// DNBWSQCreateSession()
//-------------------------------------------------------------------------

HRESULT DNBWSQCreateSession()
{   
   HRESULT RC = E_FAIL;

	CATSession * session = NULL;
   ::GetPtrSession(session) ;
   if ( session != NULL )
   {   
      DNBWSQDeleteSession();
   }
   
   // Create new session
   
   ::Create_Session( "IOSession", session );
   
   if ( session )
   {
      RC = S_OK;
   }
   
   return (RC);
}

//-------------------------------------------------------------------------
// DNBWSQDeleteSession()
//-------------------------------------------------------------------------

HRESULT DNBWSQDeleteSession()
{
   HRESULT RC = E_FAIL;

   // Get handle to the current session
   
	CATSession * session = NULL;
	::GetPtrSession(session) ;
   if ( session )
   { 
      // Delete session
      RC = ::Delete_Session( "IOSession" );
   }
   
   return (RC);
}


//-------------------------------------------------------------------------
// DNBWSQGetDocument()
//-------------------------------------------------------------------------

HRESULT DNBWSQGetDocument(CATUnicodeString filename)
{
   HRESULT RC = E_FAIL;
   // Get handle to the current session
   
   CATSession * session = NULL;
	::GetPtrSession(session) ;
   
   if(session)
   {
      // Get the document
      
      CATUnicodeString DocFile = filename;
      CATDocument * ProcessDoc = NULL;
      
      CATDocumentServices::OpenDocument( DocFile, ProcessDoc, 0);
      
      if ( ProcessDoc )
      {
         // Set Layout document as the current document 
         session->SetCurrentDoc( ProcessDoc );
         RC = S_OK;
      }
   }

   return (RC);
}

//-------------------------------------------------------------------------
// DNBWSQInitialiseDocForBatch()
//-------------------------------------------------------------------------

HRESULT DNBWSQInitialiseDocForBatch()
{
   HRESULT RC = E_FAIL;

   // Get a handle to the document Root container
   
   CATDocument *document = CATSession::GetCurrentDoc();
   if(document)
   {
      // Get a handle to the CATInit interface
      
      CATInit_var spIDocInit( document );
      
      if ( !!spIDocInit )
      {
         // Initialise document
         
         spIDocInit->Init( TRUE ); 

         //Get container
         CATISPPProcessContainer *piProcessCont = (CATISPPProcessContainer* )
            document->GetRootContainer(CATISPPProcessContainer::ClassName());
         if ( piProcessCont  )
         {
            // Get all processes
            CATLISTV(CATBaseUnknown_var)* pProcessList =
               piProcessCont->GetAllProcess();
            if ((pProcessList) || ((*pProcessList).Size() > 0))
            { 
               // Get feat file
               
               CATISPPActivityRoot_var Root ((*pProcessList)[1]);
               if(!!Root)
               {
                  Root->AddLibraryToUse( "DNBIgpSetupLibrary.feat" );
                  Root->AddLibraryToUse( "DNBRobController.feat" );
                  Root->AddLibraryToUse( "DNBWSQ.feat" );
                  
                  RC = S_OK;
               }

               delete pProcessList;
               pProcessList=NULL;
            }
            
            piProcessCont -> Release();
            piProcessCont = NULL;
         }
      }
   }
   
   return (RC);
}


