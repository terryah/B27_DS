//  COPYRIGHT DASSAULT SYSTEMES 2003
AddPrereqComponent("System", Public);
AddPrereqComponent("DNBSimIOInterfaces", Public);
AddPrereqComponent("DNBWSQInterfaces", Public);
AddPrereqComponent("ObjectModelerBase", Public);
AddPrereqComponent("DMAPSInterfaces", Public);
AddPrereqComponent("ProductStructure", Public);
AddPrereqComponent("DNBIgpSetupInterfaces", Public);
AddPrereqComponent("DNBResourceProgramInterfaces", Public);
AddPrereqComponent("ObjectSpecsModeler", Public);
