//COPYRIGHT DELMIA Corp 2003
//=============================================================================
// CAADNBWSQIOUtils.h
// Contains declaration for global utility functions.
//=============================================================================
// 05/09/2003   Creation                                           NPJ 
//=============================================================================

#ifndef CAADNBWSQIOUtils_H
#define CAADNBWSQIOUtils_H

#include "CATBooleanDef.h"
#include "CATBaseUnknown.h"

#ifdef  _WINDOWS_SOURCE
#ifdef  __CAADNBWSQIOUtils
#define ExportedByCAADNBWSQIOUtils  __declspec(dllexport)
#else
#define ExportedByCAADNBWSQIOUtils  __declspec(dllimport)
#endif
#else
#define ExportedByCAADNBWSQIOUtils
#endif

class CATUnicodeString;

ExportedByCAADNBWSQIOUtils HRESULT DNBWSQCreateSession();

ExportedByCAADNBWSQIOUtils HRESULT DNBWSQDeleteSession();

ExportedByCAADNBWSQIOUtils HRESULT DNBWSQGetDocument(CATUnicodeString fileName);

ExportedByCAADNBWSQIOUtils HRESULT DNBWSQInitialiseDocForBatch();

#endif
