// COPYRIGHT 2006 DASSAULT SYSTEMES
// C++ CAA Use Cases for the SSO (Single Sign-On) client

AddPrereqComponent("System",Public);
AddPrereqComponent("ApplicationFrame", Public);
AddPrereqComponent("Dialog", Public);
AddPrereqComponent("ObjectModelerBase", Public);

AddPrereqComponent("DialogEngine", Public);
AddPrereqComponent("Visualization", Public);
AddPrereqComponent("VisualizationBase", Public);

AddPrereqComponent("PLMSecuritySSOCClient", Public);



