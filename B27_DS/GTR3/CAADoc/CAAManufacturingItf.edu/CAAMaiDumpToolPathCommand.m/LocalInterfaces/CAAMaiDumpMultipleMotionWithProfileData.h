// COPYRIGHT DASSAULT SYSTEMES 2003
// ===================================================================
//
// CAAMaiDumpMultipleMotionWithProfileData :
//
// External service called by CAAMaiDumpToolPathCommand to dump data from 
// a CATIMfgTPMultipleMotionWithProfileData object (description of cutter
// profile data inside tool motions)
//                                      
//=============================================================================

#ifndef CAAMaiDumpMultipleMotionWithProfileData_h
#define CAAMaiDumpMultipleMotionWithProfileData_h

#include "CATIMfgTPMultipleMotionWithProfileData.h"

void  CAAMaiDumpMultipleMotionWithProfileData       (int,const CATIMfgTPMultipleMotionWithProfileData_var&);

#endif

