// COPYRIGHT DASSAULT SYSTEMES 2003
// ===========================================================================
//
// CAAMaiDumpChildren
//
// External service called by CAAMaiDumpToolPathCommand to dump the 
//    children of a CATIMfgCompoundTraject object 
//                                      
//=============================================================================

#ifndef CAAMaiDumpChildren_h
#define CAAMaiDumpChildren_h

class CATIMfgCompoundTraject_var;

void CAAMaiDumpChildren                      (int,const CATIMfgCompoundTraject_var&);

#endif

