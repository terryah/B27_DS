// COPYRIGHT DASSAULT SYSTEMES 2003
// ===========================================================================
//
// CAAMaiDumpCompoundTraject
//
// External service called by CAAMaiDumpToolPathCommand to dump a 
//    CATIMfgCompoundTraject object 
//                                      
//=============================================================================

#ifndef CAAMaiDumpCompoundTraject_h
#define CAAMaiDumpCompoundTraject_h

class CATIMfgCompoundTraject_var;

void  CAAMaiDumpCompoundTraject       (int,const CATIMfgCompoundTraject_var&);

#endif

