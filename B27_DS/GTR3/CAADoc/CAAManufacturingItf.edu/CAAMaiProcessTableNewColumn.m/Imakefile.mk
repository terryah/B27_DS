# COPYRIGHT DASSAULT SYSTEMES 2004
#======================================================================
# Imakefile for module CAAMaiProcessTableNewColumn.m
#======================================================================
#
#  Jul 2004  Creation: Code generated by the CAA wizard  xmn
#======================================================================
#
# SHARED LIBRARY 
#
BUILT_OBJECT_TYPE=SHARED LIBRARY 
 
# DO NOT EDIT :: THE CAA2 WIZARDS WILL ADD CODE HERE
WIZARD_LINK_MODULES = JS0GROUP \
JS0FM ManufacturingInterfacesUUID
# END WIZARD EDITION ZONE

LINK_WITH = $(WIZARD_LINK_MODULES) CATManufacturingInterfaces KnowledgeItf CATObjectSpecsModeler 

# System dependant variables
#
OS = AIX
#
OS = HP-UX
#
OS = IRIX
#
OS = SunOS
#
OS = Windows_NT
