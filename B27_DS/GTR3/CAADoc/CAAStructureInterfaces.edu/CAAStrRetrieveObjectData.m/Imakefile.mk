#======================================================================
# COPYRIGHT DASSAULT SYSTEMES 2003+
#======================================================================
# Imakefile for module CAAStrRetrieveObjectData.m
#======================================================================
#
#  Jul 2003  Creation: Code generated by the CAA wizard  (Brian Yin)
#======================================================================
#
# LOAD MODULE 
#
BUILT_OBJECT_TYPE=LOAD MODULE 
 
LINK_WITH = CATProductStructure1 \
            CATObjectModelerBase \
            JS0GROUP \
						CATStrInterfaces \
						YP00IMPL \
						CATMathematics \
						CATSketcherInterfaces \
						AC0SPBAS \
						KnowledgeItf \
						StructureInterfacesUUID \
						CATMatItfCPP \

# System dependant variables
#
OS = AIX
#
OS = HP-UX
#
OS = IRIX
#
OS = SunOS
#
OS = Windows_NT
