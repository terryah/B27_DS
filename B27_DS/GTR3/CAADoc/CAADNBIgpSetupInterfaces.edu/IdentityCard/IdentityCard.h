//  COPYRIGHT DASSAULT SYSTEMES 2003
AddPrereqComponent("System", Public);
AddPrereqComponent("DMAPSInterfaces", Public);
AddPrereqComponent("DNBRobotInterfaces", Public);
AddPrereqComponent("DNBDeviceInterfaces", Public);
AddPrereqComponent("DNBIgpSetupInterfaces", Public);
AddPrereqComponent("Dialog", Public);
AddPrereqComponent("Mathematics", Public);
AddPrereqComponent("ProductStructureInterfaces", Public);
AddPrereqComponent("ProductStructure", Public);
AddPrereqComponent("DialogEngine", Public);
AddPrereqComponent("ObjectModelerBase", Public);
AddPrereqComponent("ObjectSpecsModeler", Public);
AddPrereqComponent("ApplicationFrame", Public);
