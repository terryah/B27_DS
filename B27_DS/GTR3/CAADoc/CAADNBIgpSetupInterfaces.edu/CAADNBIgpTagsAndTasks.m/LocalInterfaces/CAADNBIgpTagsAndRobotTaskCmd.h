// COPYRIGHT DENEB ROBOTICS INC. 2003
//=============================================================================
// CAADNBIgpTagsAndRobotTaskCmd.h
//=============================================================================
// IGRIP on CAA2
//=============================================================================
// 25/08/2003   Creation                                           NPJ
//=============================================================================
#ifndef CAADNBIgpTagsAndRobotTaskCmd_H
#define CAADNBIgpTagsAndRobotTaskCmd_H

#include "CAADNBIgpTagsAndTaskOptionsDlg.h"

// DialogEngine
#include "CATStateCommand.h"

//Mathematics
#include "CATMathTransformation.h"

class CATPathElementAgent;
class CATDialogAgent;
class CATDialogState;

class CAADNBIgpTagsAndRobotTaskCmd : public CATStateCommand
{    
   DeclareResource(CAADNBIgpTagsAndRobotTaskCmd, CATStateCommand)
      
public:

                                       CAADNBIgpTagsAndRobotTaskCmd();
   
   virtual                             ~CAADNBIgpTagsAndRobotTaskCmd();
   
   void                                BuildGraph();

   boolean                             CreateTagGroupAndTag(void *data);
   boolean                             CreateRobotTask(void *data);
   boolean                             ShowDlg(void *data);
   boolean                             HideDlg(void *data);
   boolean                             OnOptionOkCancel(void * data);

private:

   static int FromYPRToCATMath(CATMathTransformation *iTransfo, 
								double &xx, double &yy, double &zz, 
								double &roll, double &pitch, double &yaw);

   CATPathElementAgent                 *_daTagOptionAgent;
   CATPathElementAgent                 *_daRobotTaskOptionAgent;
   CATDialogAgent                      *_daOk;
   CATDialogAgent                      *_daCancel;

   CATDialogState                      *_stInitial;
   CATDialogState                      *_stEnd;

   CAADNBIgpTagsAndTaskOptionsDlg      *_OptionsDlg;
};

#endif
