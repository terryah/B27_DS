# COPYRIGHT DASSAULT SYSTEMES 2003
#
# CAADNBIgpTagsAndTasks SHARED LIBRARY
#

BUILT_OBJECT_TYPE=SHARED LIBRARY

COMMON_LINK_WITH =	DI0PANV2 \
					JS0FM JS0GROUP \
					CATDialogEngine CATMathematics CD0WIN CD0FRAME \
					AC0XXLNK AC0SPBAS AS0STARTUP ProcessInterfaces \
					DNBIgpSetupItfCPP \
					DNBIgpSetupPubIDL \
					DNBIgpSetupProIDL \
					DNBRobotItfCPP \
					ProductStructurePubIDL CATMathStream

LINK_WITH           = $(COMMON_LINK_WITH)

#
# Define the build options for the current module.
#
OS      = Windows_NT
BUILD   = YES

OS      = IRIX
BUILD   = YES

OS      = SunOS
BUILD   = YES

OS      = AIX
BUILD   = YES

OS      = HP-UX
BUILD   = YES

OS      = win_a
BUILD   = NO
