/**
 * @fullreview RMY NPJ 03:09:03
 * @quickreview MMR LYN 04:02:25
 * @quickreview GJA 05:06:20
 */

// COPYRIGHT DENEB ROBOTICS INC. 2003
//=============================================================================
// CAADNBIgpTagsAndRobotTaskCmd.cpp
// This is an UseCase to create TagGroup, Tag, RobotTask, Operation, Robot
// Target Motion, Actions in a Process Document.
// The methods used to create TagGroup/Tag and RobotTask in this file
// are just to demonstrate the usage of the exposed CAA methods and the task
// model built from these methods is NOT complete/Ready to be simulated.
//
//=============================================================================
// IGRIP on CAA2
//=============================================================================
// 25/08/2003   Creation                                           NPJ
//  Feb 2004 LCDMMCBU fix                                                  LYN
//  Jun 2005    Replace Nodoc with valid methods                   NPJ
//
//=============================================================================
#include "CAADNBIgpTagsAndRobotTaskCmd.h"

//System
#include "CATBaseUnknown.h"
#include "CATCreateExternalObject.h"

//ObjectModelerBase
#include "CATDocument.h"
#include "CATIDocId.h"

//ApplicationFrame
#include "CATFrmEditor.h"

//Mathematics
#include "CATMath3x3Matrix.h"
#include "CATMathVector.h"

//ProductStructure
#include "CATIProduct.h"
#include "CATIMovable.h"

// DialogEngine
#include "CATDialogAgent.h"
#include "CATPathElementAgent.h"

//DNBIgpSetupInterfaces
#include "DNBITagGroupFactory.h"
#include "DNBITagFactory.h"
#include "DNBITagGroup.h"
#include "DNBITag.h"
#include "DNBFrameType.h"
#include "DNBIRobotTaskFactory.h"
#include "DNBIRobotTask.h"
#include "DNBIOperation.h"
#include "DNBIRobotTargetMotion.h"
#include "DNBIRobotTargetMotionFactory.h"
#include "DNBIGenericAction.h"
#include "DNBIGenericActionFactory.h"

CATCreateClass(CAADNBIgpTagsAndRobotTaskCmd);

//-------------------------------------------------------------------------
// Constructor()
//-------------------------------------------------------------------------
CAADNBIgpTagsAndRobotTaskCmd::CAADNBIgpTagsAndRobotTaskCmd()
:CATStateCommand("TagsNTaskCmd")
{
   _daTagOptionAgent = NULL;
   _daRobotTaskOptionAgent = NULL;
   _stInitial = NULL;
   _stEnd = NULL;
   _daOk = NULL;
   _daCancel = NULL;

   _OptionsDlg = new CAADNBIgpTagsAndTaskOptionsDlg();
}

//-------------------------------------------------------------------------
// Destructor()
//-------------------------------------------------------------------------
CAADNBIgpTagsAndRobotTaskCmd::~CAADNBIgpTagsAndRobotTaskCmd()
{
   _daTagOptionAgent = NULL;
   _daRobotTaskOptionAgent = NULL;
   _stInitial = NULL;
   _stEnd = NULL;
   _daOk = NULL;
   _daCancel = NULL;

   _OptionsDlg = NULL;
}

//----------------------------------------------------------------------------
// BuildGraph()
//----------------------------------------------------------------------------
void CAADNBIgpTagsAndRobotTaskCmd::BuildGraph()
{
   _stInitial = GetInitialState( "_stInitial" );

   _stEnd = AddDialogState( "_stEnd" );

   _daOk =new CATDialogAgent("OkAgent");
   _daCancel =new CATDialogAgent("CancelAgent");

   _stInitial->AddDialogAgent(_daOk);
   _stInitial->AddDialogAgent(_daCancel);
   
   _daOk->AcceptOnNotify( _OptionsDlg, 
   _OptionsDlg->GetDiaOKNotification() );
   
   _daCancel->AcceptOnNotify( _OptionsDlg, 
   _OptionsDlg->GetDiaCANCELNotification() );

   _stInitial->SetEnterAction( Action( (ActionMethod) 
                              &CAADNBIgpTagsAndRobotTaskCmd::ShowDlg ) );
   
   _stInitial->SetLeaveAction( Action( (ActionMethod) 
                              &CAADNBIgpTagsAndRobotTaskCmd::HideDlg ) );

   AddTransition (_stInitial, _stEnd,IsOutputSetCondition( _daOk),
      Action ((ActionMethod) &CAADNBIgpTagsAndRobotTaskCmd::OnOptionOkCancel));
   AddTransition (_stInitial, NULL,IsOutputSetCondition( _daCancel),
      Action ((ActionMethod) &CAADNBIgpTagsAndRobotTaskCmd::OnOptionOkCancel));
}  

//-------------------------------------------------------------------------
// OnOptionOkCancel ()
//-------------------------------------------------------------------------
boolean CAADNBIgpTagsAndRobotTaskCmd::OnOptionOkCancel( void *data )
{
   return TRUE;
}

//----------------------------------------------------------------------------
// ShowDlg()
//----------------------------------------------------------------------------
boolean CAADNBIgpTagsAndRobotTaskCmd::ShowDlg( void *data )
{
   _OptionsDlg->SetVisibility( CATDlgShow );
   
   return TRUE;
}

//----------------------------------------------------------------------------
// HideDlg()
//----------------------------------------------------------------------------
boolean CAADNBIgpTagsAndRobotTaskCmd::HideDlg( void * data )
{
   
   if(_OptionsDlg->GetCreateAsRefButton()->GetState() == CATDlgCheck ||
      _OptionsDlg->GetCreateAsAttachButton()->GetState() == CATDlgCheck)
   {  
      _daTagOptionAgent = new CATPathElementAgent("Select Product");
      _daTagOptionAgent->SetBehavior(CATDlgEngWithPrevaluation | CATDlgEngWithCSO);

      const IID TypeID(IID_CATIProduct);
      _daTagOptionAgent->AddElementType(TypeID);
      if( _stEnd )
         _stEnd -> AddDialogAgent (_daTagOptionAgent);
      
      AddTransition (_stEnd, NULL,IsOutputSetCondition( _daTagOptionAgent),
         Action( (ActionMethod)& CAADNBIgpTagsAndRobotTaskCmd::CreateTagGroupAndTag));
   }
   else if(_OptionsDlg->GetCreateInDocButton()->GetState() == CATDlgCheck )
   {
      AddTransition( _stEnd, NULL, NULL, Action ( (ActionMethod) &CAADNBIgpTagsAndRobotTaskCmd::CreateTagGroupAndTag) );
   }
   
   if(_OptionsDlg->GetCreateRobotTaskButton()->GetState() == CATDlgCheck  )
   {
      _daRobotTaskOptionAgent = new CATPathElementAgent("Select Robot");
      _daRobotTaskOptionAgent->SetBehavior(CATDlgEngWithPrevaluation | CATDlgEngWithCSO);
      
      const IID TypeID(IID_CATIProduct);
      _daRobotTaskOptionAgent->AddElementType(TypeID);
      if( _stEnd )
         _stEnd -> AddDialogAgent (_daRobotTaskOptionAgent);
      
      AddTransition (_stEnd, NULL,IsOutputSetCondition( _daRobotTaskOptionAgent),
         Action( (ActionMethod)& CAADNBIgpTagsAndRobotTaskCmd::CreateRobotTask)); 
   }
   
   _OptionsDlg->SetVisibility( CATDlgHide );
   
   return TRUE;
}

//----------------------------------------------------------------------------
// CreateTagGroupAndTag() Creates a TagGroup and a Tag under it
//----------------------------------------------------------------------------
boolean CAADNBIgpTagsAndRobotTaskCmd::CreateTagGroupAndTag(void *data)
{
   CATDocument * currentDoc = NULL;
   currentDoc = CATFrmEditor::GetCurrentEditor()->GetDocument();
   
   CATUnicodeString DocType("");
   
   if(currentDoc)
   {
      CATIDocId *pDocId = NULL;
      currentDoc->GetDocId(&pDocId);
      if(pDocId)
      {
         pDocId->GetType(DocType);
         
         pDocId->Release();
         pDocId = NULL;
      }
   }
   if(DocType == "CATProcess")
   {
      DNBITagGroupFactory_var spITagGroupFactory( currentDoc );
      if(!!spITagGroupFactory )
      {  
         CATBaseUnknown *pProd = NULL;
         if(_daTagOptionAgent)
            pProd = _daTagOptionAgent->GetElementValue();
         CATIProduct_var spProduct(pProd);

         CATUnicodeString TagGroupName("TagGroup");
         
         DNBITagGroup *pTagGroup = NULL;
         
         if(!!spProduct )
         {
            //Create a TagGroup and a Tag by Modifying the Reference
            if(_OptionsDlg->GetCreateAsRefButton()->GetState() == CATDlgCheck)
               spITagGroupFactory->CreateTagGroup(TagGroupName, TRUE, spProduct, &pTagGroup);
            //Create a TagGroup and a Tag as an Attachment
            else if(_OptionsDlg->GetCreateAsAttachButton()->GetState() == CATDlgCheck)
               spITagGroupFactory->CreateTagGroup(TagGroupName, FALSE, spProduct, &pTagGroup);
         }
         else if(_OptionsDlg->GetCreateInDocButton()->GetState() == CATDlgCheck)
            //Create a TagGroup and a Tag in the Document => under the TagList
            spITagGroupFactory->CreateTagGroup(TagGroupName, FALSE, NULL, &pTagGroup);

         if(pTagGroup)
         {
            DNBITagFactory_var spTagFactory(pTagGroup);
            if(!!spTagFactory)
            {
               DNBITag *pTag = NULL;
               
               CATUnicodeString TagName("Tag");
               spTagFactory->CreateTag(TagName, &pTag);
               
               if(pTag)
               {  
                  //Set Tag Properties
                  pTag->SetType(DNBDesignFrame);
                  pTag->SetXYZ(1000.0, -1500.0, 1000.0);
                  
                  pTag->Release();
                  pTag = NULL;
               }
            }
            pTagGroup->Release();
            pTagGroup = NULL;
         }
      }
   }
   return TRUE;
}

//----------------------------------------------------------------------------
// CreateRobotTask() Creates a RobotTask with Operations, Robot Target Motions
//                   and Actions.
//----------------------------------------------------------------------------
boolean CAADNBIgpTagsAndRobotTaskCmd::CreateRobotTask(void *data)
{
   if(_OptionsDlg->GetCreateRobotTaskButton()->GetState() == CATDlgCheck)
   {
      CATBaseUnknown *pProd = _daRobotTaskOptionAgent->GetElementValue();
      DNBIRobotTaskFactory_var spTaskFactory(pProd);
      if(!!spTaskFactory)
      {
         CATDocument * currentDoc = NULL;
         currentDoc = CATFrmEditor::GetCurrentEditor()->GetDocument();
         
         DNBITagGroup *pTagGroup = NULL;
         DNBITag      *pTag = NULL;
         
         CATUnicodeString DocType("");
         
         if(currentDoc)
         {
            CATIDocId *pDocId = NULL;
            currentDoc->GetDocId(&pDocId);
            if(pDocId)
            {
               pDocId->GetType(DocType);
               
               pDocId->Release();
               pDocId = NULL;
            }
         }

         //Create a Tag at a Specified Location.
         if(DocType == "CATProcess")
         {
            DNBITagGroupFactory_var spITagGroupFactory( currentDoc );
            if(!!spITagGroupFactory )
            {  
               CATUnicodeString TagGroupName("TagGroup");
               spITagGroupFactory->CreateTagGroup(TagGroupName, FALSE, NULL, &pTagGroup);
               if(pTagGroup)
               {
                  DNBITagFactory_var spTagFactory(pTagGroup);
                  if(!!spTagFactory)
                  {
                     pTagGroup->Release();
                     pTagGroup = NULL;
                     
                     CATMathTransformation tagTransfo; 
                     double xx = -600, yy = 700, zz = 1700, roll = 0, pitch = 0, yaw = 0;
                     FromYPRToCATMath(&tagTransfo, xx, yy, zz, roll, pitch, yaw);
                     
                     CATUnicodeString TagName("Tag");
                     spTagFactory->CreateTagAtLocation(TagName, tagTransfo, &pTag);
                  }
               }
            }
         }
         
         //Create a Robot Task
         DNBIRobotTask*  pRobotTask = NULL;
         spTaskFactory->CreateRobotTask("RobotTask_1",&pRobotTask);
         if(pRobotTask)
         {
            DNBIOperation *pOperation1 = NULL;
            DNBIOperation *pOperation2 = NULL;
            DNBIOperation *pOperation3 = NULL;
            
            //Create an Operation
            HRESULT RC = pRobotTask->CreateOperation(NULL, NULL, &pOperation1);
            if(SUCCEEDED(RC) && pOperation1)
            {
               DNBIRobotTargetMotionFactory_var spTargetMotionFactory1(pOperation1);
               if(!!spTargetMotionFactory1)
               {
                  DNBIRobotTargetMotion* pTargetMotion1 = NULL;
                  
                  //Create a Robot Target Motion under an Operation
                  spTargetMotionFactory1->CreateRobotTargetMotion(&pTargetMotion1);
                  
                  if(pTargetMotion1)
                  {
                     //Set Cartesian Target on the Motion
                     CATMathTransformation tagTransfo; 
                     double xx = 600, yy = -700, zz = 1700, roll = 0, pitch = 0, yaw = 0;
                     FromYPRToCATMath(&tagTransfo, xx, yy, zz, roll, pitch, yaw);
                     pTargetMotion1->SetCartesianTarget(tagTransfo);

                     //Create a Spot Pick Action after the Robot Target Motion
                     DNBIGenericAction* pGenericAction1 = NULL;
                     DNBIGenericActionFactory_var spGenericActionFactory(pOperation1);
                     if(!!spGenericActionFactory)
                     {
                        spGenericActionFactory->CreateAction("DNBIgpSpotPick", FALSE, pTargetMotion1, &pGenericAction1);
                     }
                     
                     pTargetMotion1->Release();
                     pTargetMotion1 = NULL;
                     
                     if(pGenericAction1)
                     {
                        pGenericAction1->Release();
                        pGenericAction1 = NULL;
                     }
                  }
                  
                  //Create a SpotWeld Action after an Operation => Create a new Operation
                  //After the reference Operation and the create the SpotWeld action under it
                  DNBIGenericAction* pGenericAction2 = NULL;
                  DNBIGenericActionFactory_var spGenericActionFactory(pOperation1);
                  if(!!spGenericActionFactory)
                  {
                     spGenericActionFactory->CreateAction("DNBIgpSpotWeld", FALSE, pOperation1, &pGenericAction2);
                  }
                  if(pGenericAction2)
                  {
                     pGenericAction2->Release();
                     pGenericAction2 = NULL;
                  }
               }
               
               //Create pOperation3 with a Reference pOperation1 => Copy from the
               //Reference pOperation1
               RC = pRobotTask->CreateOperation(pOperation1, pOperation1, &pOperation3);
               
               if(SUCCEEDED(RC) && pOperation3)
               {
                  DNBIRobotTargetMotion* pTargetMotion = NULL;
                   
                  pOperation3->GetRobotTargetMotion(&pTargetMotion);
                  
                  if(pTargetMotion)
                  {
                     CATMathTransformation tagTransfo; 
                     double xx = -600, yy = -700, zz = 1500, roll = 0, pitch = 0, yaw = 0;
                     FromYPRToCATMath(&tagTransfo, xx, yy, zz, roll, pitch, yaw);
                     pTargetMotion->SetCartesianTarget(tagTransfo);
                     
                     pTargetMotion->Release();
                     pTargetMotion = NULL;
                  }

                  pOperation3->Release();
                  pOperation3 = NULL;
               }
               
               //Create an pOperation2 after the Specified pOperation1
               RC = pRobotTask->CreateOperation(NULL, pOperation1, &pOperation2);
               
               if(SUCCEEDED(RC) && pOperation2)
               {
                  DNBIRobotTargetMotionFactory_var spTargetMotionFactory2(pOperation2);
                  if(!!spTargetMotionFactory2)
                  {
                     DNBIGenericAction* pGenericAction3 = NULL;
                     DNBIGenericActionFactory_var spGenericActionFactory(pOperation2);
                     if(!!spGenericActionFactory)
                     {
                        spGenericActionFactory->CreateAction("DNBIgpSpotRetract", FALSE, NULL, &pGenericAction3);
                     }
                     
                     if(pGenericAction3)
                     {
                        DNBIRobotTargetMotion* pTargetMotion2 = NULL;
                        spTargetMotionFactory2->CreateRobotTargetMotion(&pTargetMotion2, FALSE, pGenericAction3);
                        if(pTargetMotion2)
                        {  
                           if(pTag)
                           {
                              //Set Tag Target for the Motion
                              pTargetMotion2->SetTagTarget(pTag);
                              
                              pTag->Release();
                              pTag = NULL;
                           }
                           
                           pTargetMotion2->Release();
                           pTargetMotion2 = NULL;
                        }
                        pGenericAction3->Release();
                        pGenericAction3 = NULL;
                     }
                  }  
                  pOperation2->Release();
                  pOperation2 = NULL;
               }
               pOperation1->Release();
               pOperation1 = NULL;
            }
            pRobotTask->Release();
            pRobotTask = NULL;
         }
      }
   }   

   return TRUE;
}

//----------------------------------------------------------------------------
// FromYPRToCATMath()
//----------------------------------------------------------------------------
int CAADNBIgpTagsAndRobotTaskCmd::FromYPRToCATMath(CATMathTransformation *iTransfo, double &xx, double &yy, double &zz, double &roll, double &pitch, double &yaw)
{
	if( iTransfo )
	{
		CATMath3x3Matrix iMatrix;
      iTransfo->GetMatrix(iMatrix);

		CATMathVector iVector;
      iTransfo->GetVector(iVector);

		double c_phi = cos(roll);
		double c_teta = cos(pitch);
		double c_psi = cos(yaw);
		double s_phi = sin(roll);
		double s_teta = sin(pitch);
		double s_psi = sin(yaw);

		double matrice[12];
		matrice[0]  = c_phi * c_teta;
		matrice[1]  = s_phi * c_teta;
		matrice[2]  = - s_teta;
		matrice[3]  = (c_phi * s_teta * s_psi) - (s_phi * c_psi);
		matrice[4]  = (s_phi * s_teta * s_psi) + (c_phi * c_psi);
		matrice[5]  = c_teta * s_psi;
		matrice[6]  = (c_phi * s_teta * c_psi) + (s_phi * s_psi);
		matrice[7]  = (s_phi * s_teta * c_psi) - (c_phi * s_psi);
		matrice[8]  = c_teta * c_psi;
		matrice[9]  = xx;
		matrice[10] = yy;
		matrice[11] = zz;


		CATMathVector FirstColumn_Vector(matrice[0],matrice[1],matrice[2]);
		CATMathVector SecondColumn_Vector(matrice[3],matrice[4],matrice[5]);
		CATMathVector ThirdColumn_Vector(matrice[6],matrice[7],matrice[8]);
		CATMathVector root_Vector(matrice[9],matrice[10],matrice[11]);

		iMatrix.SetFirstColumn(FirstColumn_Vector); 
		iMatrix.SetSecondColumn(SecondColumn_Vector);
		iMatrix.SetThirdColumn(ThirdColumn_Vector); 

		CATMathTransformation new_Matrix(iMatrix,root_Vector);
		*iTransfo = new_Matrix;
		
		return TRUE;
	}
	else
		return FALSE;
}
