// COPYRIGHT DASSAULT SYSTEMES 2003

AddPrereqComponent("PSNInteroperability",           Public);
AddPrereqComponent("System",                        Public);
AddPrereqComponent("CATPDMBaseInterfaces",          Public);
AddPrereqComponent("ObjectModelerBase",             Public);
AddPrereqComponent("CATPDMBase",                    Public);
AddPrereqComponent("CDMAInteroperability",          Public);
AddPrereqComponent("ObjectSpecsModeler",            Public);
AddPrereqComponent("ProductStructure",              Public);
AddPrereqComponent("ProductStructureInterfaces",    Public);

