// COPYRIGHT DASSAULT SYSTEMES 2000
#ifndef CATEdiSampleParameters_H
#define CATEdiSampleParameters_H

// ----------------------------------------------------------------------------
// Application
// ----------------------------------------------------------------------------
#define SCHELEClass_Application            "CATElectricalDiagram"

// ----------------------------------------------------------------------------
// PRODUCT
// ----------------------------------------------------------------------------
#define SCHELEClass_Component              "Component_Function"
#define SCHELEClass_Plug                   "CATElePlugFunction"
#define SCHELEClass_Socket                 "CATEleSocketFunction"
#define SCHELEClass_TerminalBoard          "CATEleTerminalBoardFunc"
#define SCHELEClass_JunctionBox            "CATEleJunctionBoxFunc"
#define SCHELEClass_Equipment              "CATEleEquipmentFunction"
#define SCHELEClass_Fuse                   "CATEleFuseFunction"
#define SCHELEClass_Switch                 "CATEleSwitchFunction"
#define SCHELEClass_Cable                  "CATEleCable"  

// ----------------------------------------------------------------------------
// CONNECTOR
// ----------------------------------------------------------------------------
#define SCHELEClass_CableExtremityCtr      "CATEleCableExtremityCtr"
#define SCHELEClass_PinCtr                 "CATElePinCtr" 
#define SCHELEClass_WidePinCtr             "CATEleWidePinCtr"
#define SCHELEClass_ChildCtr               "CATEleChildCtr"
#define SCHELEClass_ParentCtr              "CATEleParentCtr"
#define SCHELEClass_PlugMatingCtr          "CATElePlugMatingCtr"
#define SCHELEClass_SocketMatingCtr        "CATEleSocketMatingCtr"
#define SCHELEClass_WireCtr                "CATEleWireCtr"
#endif














