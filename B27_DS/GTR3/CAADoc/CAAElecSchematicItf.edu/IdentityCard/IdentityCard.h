//
// COPYRIGHT DASSAULT SYSTEMES 2001
//
// -->Prereq Components Declaration
AddPrereqComponent("System"                  ,Public);
AddPrereqComponent("ObjectModelerBase"       ,Public);
AddPrereqComponent("ObjectSpecsModeler"      ,Public);
AddPrereqComponent("ProductStructure"        ,Public);
AddPrereqComponent("DraftingInterfaces"      ,Public);
AddPrereqComponent("SketcherInterfaces"      ,Public);
AddPrereqComponent("CATSchPlatformInterfaces",Public);
AddPrereqComponent("Mathematics"             ,Public);
AddPrereqComponent("ElecSchematicItf"        ,Public);
AddPrereqComponent("CATPlantShipInterfaces"  ,Public);



 
