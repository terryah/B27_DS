#include "CAAIPuiPRDWorkshopConfigFactory.h"

#ifndef LOCAL_DEFINITION_FOR_IID
IID IID_CAAIPuiPRDWorkshopConfigFactory = { 0x79ca610e, 0xeabc, 0x471c, { 0x82, 0x7c, 0x4e, 0x5d, 0xd5, 0xe8, 0x55, 0x18} };
#endif

CATImplementInterface(CAAIPuiPRDWorkshopConfigFactory, CATIGenericFactory);
