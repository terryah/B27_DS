// COPYRIGHT Dassault Systemes 2003
//===================================================================
//
// CAAPuiPRDWorkshopAddinCmd.cpp
// The command: CAAPuiPRDWorkshopAddinCmd
//
//===================================================================
//
// Usage notes:
//
//===================================================================
//
//  Aug 2003  Creation: Code generated by the CAA wizard  avm
//===================================================================
#include "CAAPuiPRDWorkshopAddinCmd.h"
#include "CAAPuiPRDWorkshopAddinTrace.h"

// ApplicationFrame framework
#include "CATApplicationFrame.h"

// Dialog framework
#include "CATDlgNotify.h"

#include "CATCreateExternalObject.h"
CATCreateClass(CAAPuiPRDWorkshopAddinCmd);

//-------------------------------------------------------------------------
// Constructor
//-------------------------------------------------------------------------
CAAPuiPRDWorkshopAddinCmd::CAAPuiPRDWorkshopAddinCmd() :
	CATCommand(NULL, "CAAPuiPRDWorkshopAddinCmd")
{
	RequestStatusChange(CATCommandMsgRequestExclusiveMode);
}

//-------------------------------------------------------------------------
// Destructor
//-------------------------------------------------------------------------
CAAPuiPRDWorkshopAddinCmd::~CAAPuiPRDWorkshopAddinCmd()
{
}


// Activates a command.
//   iFromClient :The command that requests to activate the current one.
//   iEvtDat :The notification sent.
// ----------------------------------------------------
CATStatusChangeRC CAAPuiPRDWorkshopAddinCmd::Activate(CATCommand *iFromClient, 
						      CATNotification *iEvtDat)
{
	CAAPuiPRDWorkshopAddinTRACE (">> CAAPuiPRDWorkshopAddinCmd::Activate");
  
	CATApplicationFrame *pApplication = CATApplicationFrame::GetFrame(); 
	if (NULL != pApplication) { 		
 		CATDlgWindow * pMainWindow = pApplication->GetMainWindow();
		CATDlgNotify *pNotifyDlg = new CATDlgNotify
			(pMainWindow, "CAAPuiPRDWorkshopAddin", CATDlgNfyOK);
		if (NULL != pNotifyDlg) {
			pNotifyDlg->DisplayBlocked
				("Hello World!",
				 "CAAPuiPRDWorkshopAddin UseCase");
			pNotifyDlg->RequestDelayedDestruction(); 
		}
	}
	RequestDelayedDestruction();

	CAAPuiPRDWorkshopAddinTRACE ("<< CAAPuiPRDWorkshopAddinCmd::Activate");
	return CATStatusChangeRCCompleted;
}
