//============================================================================================
// COPYRIGHT DASSAULT SYSTEMES  1999
//
// IdentityCard for CAAAssemblyUI.edu Framework
// demonstrating CAA2 Assembly and CATAssemblyUI capabilities
//============================================================================================

AddPrereqComponent ("System"            , Public);
AddPrereqComponent ("ObjectModelerBase" , Public);
AddPrereqComponent ("ObjectSpecsModeler", Public);
AddPrereqComponent ("ProductStructure"  , Public);
AddPrereqComponent ("MecModInterfaces"  , Public);
AddPrereqComponent ("KnowledgeInterfaces"  , Public);
AddPrereqComponent ("ConstraintModelerInterfaces"  , Public);
AddPrereqComponent ("ConstraintModeler"  , Public);
AddPrereqComponent ("CATAssemblyInterfaces",Public);
