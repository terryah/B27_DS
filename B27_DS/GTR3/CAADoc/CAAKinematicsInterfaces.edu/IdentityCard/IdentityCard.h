// COPYRIGHT DASSAULT SYSTEMES 2000

AddPrereqComponent("System",                     Public);
AddPrereqComponent("Dialog",                     Public);
AddPrereqComponent("ObjectModelerBase",          Public);
AddPrereqComponent("ObjectSpecsModeler",         Public);
AddPrereqComponent("GeometricObjects",           Public);
AddPrereqComponent("Mathematics",                Public);
AddPrereqComponent("MecModInterfaces",           Public);
AddPrereqComponent("ProductStructure",           Public);
AddPrereqComponent("KinematicsInterfaces",       Public);
AddPrereqComponent("ApplicationFrame",           Public);
AddPrereqComponent("CATAssemblyInterfaces",      Public);
