#ifndef CAACreationRuleBase_H
#define CAACreationRuleBase_H

#include "CATISpecObject.h"
#include "CATIRuleBase.h"

CATIRuleBase_var CreationRuleBase(CATISpecObject_var ispPart);

#endif
