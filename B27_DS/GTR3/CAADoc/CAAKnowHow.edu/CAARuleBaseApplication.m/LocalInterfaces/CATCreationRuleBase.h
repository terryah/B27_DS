#ifndef CATCreationRuleBase_H
#define CATCreationRuleBase_H

#include "CATISpecObject.h"
#include "CATIRuleBase.h"

CATIRuleBase_var CreationRuleBase(CATISpecObject_var ispPart);

#endif
