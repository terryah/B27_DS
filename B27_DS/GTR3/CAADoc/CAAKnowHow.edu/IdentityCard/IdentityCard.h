// COPYRIGHT DASSAULT SYSTEMES 2002
// -->Prereq Components Declaration

AddPrereqComponent("System"                ,Public);
AddPrereqComponent("ObjectSpecsModeler"    ,Public);
AddPrereqComponent("ObjectModelerBase"     ,Public);
AddPrereqComponent("KnowHow"               ,Public);
AddPrereqComponent("KnowledgeInterfaces",Public);
AddPrereqComponent("ProductStructure",Public);
AddPrereqComponent("MecModInterfaces",Public);
