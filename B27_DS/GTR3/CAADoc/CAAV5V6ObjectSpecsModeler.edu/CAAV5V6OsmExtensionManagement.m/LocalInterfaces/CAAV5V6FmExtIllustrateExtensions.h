//
// COPYRIGHT DASSAULT SYSTEMES 2012
//

#ifndef CAAV5V6FmExtIllustrateExtensions_H
#define CAAV5V6FmExtIllustrateExtensions_H

//System Framework
#include "CATString.h"

/**
 * Service to illustrate working with feature extensions.

 */

int CAAV5V6FmExtIllustrateExtensions(const char *pExtensionsCatalog1Name,
									const char *pExtensionsCatalog2Name,
									const char *pFeatureCatalogName,
									 CATString iEnvToUse,
									 CATString iRepository);
			      
			       

#endif
