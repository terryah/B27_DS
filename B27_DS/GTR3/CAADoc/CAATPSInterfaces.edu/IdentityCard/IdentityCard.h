// COPYRIGHT DASSAULT SYSTEMES 2000
//=============================================================================
//
// IdentityCard for framework CAATPSInterfaces
//
//=============================================================================
// Usage notes:
//
//=============================================================================
// Jan. 2000  Creation
//=============================================================================

//------------------------------------------------------------- Test Framework
AttachComponent("CAATPSInterfaces.tst");

//--------------------------------------------------------------------- Prereqs
AddPrereqComponent ("System",                     Public);
AddPrereqComponent ("Mathematics",                Public);
AddPrereqComponent ("Visualization",              Public);
AddPrereqComponent ("VisualizationBase",          Public);
AddPrereqComponent ("ApplicationFrame",           Public);
AddPrereqComponent ("InteractiveInterfaces",      Public);
AddPrereqComponent ("ObjectModelerBase",          Public);
AddPrereqComponent ("ObjectSpecsModeler",         Public);
AddPrereqComponent ("GeometricObjects",           Public);
AddPrereqComponent ("NewTopologicalObjects",      Public);
AddPrereqComponent ("Dialog",                     Public);
AddPrereqComponent ("DialogEngine",               Public);
AddPrereqComponent ("ProductStructure",           Public);
AddPrereqComponent ("ProductStructureInterfaces", Public);
AddPrereqComponent ("MecModInterfaces",           Public);
AddPrereqComponent ("DraftingInterfaces",         Public);
AddPrereqComponent ("CATTTRSInterfaces",          Public);
AddPrereqComponent ("CATTPSInterfaces",           Public);
