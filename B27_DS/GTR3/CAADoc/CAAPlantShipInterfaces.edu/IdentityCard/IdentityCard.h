// COPYRIGHT DASSAULT SYSTEMES 2003
//
// CAAPlantShipInterfaces.edu -->Prereq Components Declaration
AddPrereqComponent ("ObjectSpecsModeler"      , Public);
AddPrereqComponent ("ObjectModelerBase"       , Public);
AddPrereqComponent ("System"                  , Public);
AddPrereqComponent ("ProductStructure"        , Public);
AddPrereqComponent ("Mathematics"             , Public);
AddPrereqComponent ("CATPlantShipInterfaces"  , Public);
AddPrereqComponent ("CATPlantShipModeler"     , Public);
AddPrereqComponent ("KnowledgeInterfaces"     , Public);
AddPrereqComponent ("CATArrangementInterfaces", Public);
