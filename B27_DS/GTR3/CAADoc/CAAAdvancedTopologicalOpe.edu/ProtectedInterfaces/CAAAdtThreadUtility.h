// COPYRIGHT DASSAULT SYSTEMES 2002
#if defined(__CAAAdtThreadUtility)
#define ExportedByCAAAdtThreadUtility DSYExport
#else
#define ExportedByCAAAdtThreadUtility DSYImport
#endif
#include "DSYExport.h"
