// COPYRIGHT DASSAULT SYSTEMES 2002
#if defined(__CAAAdtJournalThreadUtility)
#define ExportedByCAAAdtJournalThreadUtility DSYExport
#else
#define ExportedByCAAAdtJournalThreadUtility DSYImport
#endif
#include "DSYExport.h"
