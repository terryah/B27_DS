# 
# COPYRIGHT DASSAULT SYSTEMES  2001
#
BUILT_OBJECT_TYPE=LOAD MODULE
#
OS = COMMON

LINK_WITH = \
  JS0GROUP \
  Mathematics \
  CATMathStream \
  CATCGMGeoMath \
  CATGeometricObjects \
  CATTopologicalOperators \
  CATTopologicalObjects \
  CATBasicTopologicalOpe \
  CAATopDumpJournal \
  CAATopCheckForPart
	                               

