// COPYRIGHT DASSAULT SYSTEMES 2002

#ifndef CAATopRollingEdges_H
#define CAATopRollingEdges_H


class CATGeoFactory;
class CATTopData;
class CATBody;

// ================================================================
// 
// ABSTRACT
// --------
// 
// Illustrates how to create a constant radius fillet on rolling edges
//
// ===========================================================================
//  Usage:
//  ------
//  Type  CAATopFilletsMain
//  
//===========================================================================
//  Inheritance:
//  ------------
//         None
//
//===========================================================================
//  Main Method:
//  ------------
//
//  CAATopRollingFillets
//
//===========================================================================

int CAATopRollingEdges(CATGeoFactory * iFactory, CATTopData * iTopData, CATBody * iBody);               
#endif








































































