# 
# COPYRIGHT DASSAULT SYSTEMES  20022
#
BUILT_OBJECT_TYPE=LOAD MODULE
#
OS = COMMON

LINK_WITH = \
  JS0GROUP \
  CATMathematics \
  CATMathStream \
  CATCGMGeoMath \
  CATGeometricObjects \
  CATTopologicalObjects \
  CATTopologicalOperators \
		     CATBasicTopologicalOpe
	


