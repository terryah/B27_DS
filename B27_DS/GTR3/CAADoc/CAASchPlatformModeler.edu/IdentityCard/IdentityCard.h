// COPYRIGHT DASSAULT SYSTEMES 2001
//
// CAASchPlatformModeler.edu -->Prereq Components Declaration
AddPrereqComponent ("ObjectSpecsModeler"    , Public);
AddPrereqComponent ("ObjectModelerBase"     , Public);
AddPrereqComponent ("System"                , Public);
AddPrereqComponent ("ProductStructure"      , Public);
AddPrereqComponent ("SketcherInterfaces"    , Public);
AddPrereqComponent ("DraftingInterfaces"    , Public);
AddPrereqComponent ("CATSchPlatformInterfaces",Public);
AddPrereqComponent ("ApplicationFrame"      , Public);
AddPrereqComponent ("Visualization"         , Public);
AddPrereqComponent ("VisualizationBase"         , Public);
AddPrereqComponent ("ProductStructureInterfaces",Public);
