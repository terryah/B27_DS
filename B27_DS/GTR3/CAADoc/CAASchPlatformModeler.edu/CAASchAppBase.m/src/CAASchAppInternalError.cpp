// COPYRIGHT DASSAULT SYSTEMES  2000+
//=============================================================================
//
//CAASchAppInternalError - error class for CATSchPlatformModeler.edu framework
//
#include "CAASchAppInternalError.h"

CATImplementErrorClass(CAASchAppInternalError,CATInternalError)
CATImplementNLSErrorClass(CAASchAppInternalError)

