// COPYRIGHT DASSAULT SYSTEMES 2001+
//=============================================================================
//
// CAASchApp.h
//
//=============================================================================
#ifndef CAASchApp_H
#define CAASchApp_H

// Graphical Representation Names

#define CAASCHAPP_GRR_REP1 "Primary Rep"
#define CAASCHAPP_GRR_REP2 "Secondary Rep"
#define CAASCHAPP_GRR_REP3 "Third Rep"
#define CAASCHAPP_GRR_REP4 "Fourth Rep"
#define CAASCHAPP_GRR_REP5 "Fifth Rep"
#define CAASCHAPP_GRR_REP6 "Sixth Rep"
#define CAASCHAPP_GRR_REP7 "Seventh Rep"


#endif
