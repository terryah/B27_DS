# COPYRIGHT DASSAULT SYSTEMES 2003
BUILT_OBJECT_TYPE = LOAD MODULE
#
LINK_WITH = JS0GROUP                \ 
		    CATProductStructure1    \
	     	CATLiteralFeatures      \
            CATObjectModelerBase    \
            CATObjectSpecsModeler   \
            CATGitInterfaces        \
            CATEhiInterfaces        \
            CATElectricalInterfaces \
			CATMecModInterfaces     \
			KnowledgeItf            \
			CATViz		            \
			CATVisualization
#
OS = COMMON

