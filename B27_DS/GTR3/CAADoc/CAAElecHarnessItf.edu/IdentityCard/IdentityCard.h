//
// COPYRIGHT DASSAULT SYSTEMES 2001
//
// -->Prereq Components Declaration
AddPrereqComponent ("System"                     ,Public);
AddPrereqComponent ("ObjectModelerBase"          ,Public);
AddPrereqComponent ("ObjectSpecsModeler"         ,Public);
AddPrereqComponent ("LiteralFeatures"            ,Public);
AddPrereqComponent ("ProductStructure"           ,Public);
AddPrereqComponent ("ProductStructureInterfaces" ,Public);
AddPrereqComponent ("GSMInterfaces"              ,Public);
AddPrereqComponent ("MecModInterfaces"           ,Public);
AddPrereqComponent ("ElectricalInterfaces"       ,Public);
AddPrereqComponent ("ElecHarnessItf"             ,Public);
AddPrereqComponent ("Visualization"              ,Public);
AddPrereqComponent ("VisualizationBase"          ,Public);
AddPrereqComponent ("KnowledgeInterfaces"        ,Public);



