//
// Copyright DASSAULT SYSTEMES 2003
//
// Entry point for the CAAPstBlockMovable use case
//
#include "CAAPstBlockMovable.h"

int main(int argc, const char *argv[])
{
	CAAPstBlockMovable obj;

	return  obj.Run(argc, argv);
}
