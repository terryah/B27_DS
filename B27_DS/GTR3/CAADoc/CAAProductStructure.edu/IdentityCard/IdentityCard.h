// COPYRIGHT DASSAULT SYSTEMES 1999
//

//// INF   Group - User Interface
AddPrereqComponent ("ApplicationFrame"             , Public);
AddPrereqComponent ("Dialog"                       , Public);
AddPrereqComponent ("DialogEngine"                 , Public);

// INF   Group - Base
AddPrereqComponent ("System"                       , Public);
AddPrereqComponent ("Mathematics"                  , Public);
AddPrereqComponent ("ObjectModelerBase"            , Public);

AddPrereqComponent ("ObjectSpecsModeler"           , Public);
AddPrereqComponent ("Visualization"                , Public);
AddPrereqComponent ("GeometryVisualization"        , Public);

AddPrereqComponent ("ProductStructure"             , Public);
AddPrereqComponent ("ProductStructureInterfaces"   , Public);
AddPrereqComponent ("KnowledgeInterfaces"          , Public);
