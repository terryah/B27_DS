// COPYRIGHT DASSAULT SYSTEMES 2002
#include "CAAIPstINFRoot.h"

IID IID_CAAIPstINFRoot = { 0x82ee9602, 0x34c5, 0x11d6, { 0x85, 0x0c, 0x00, 0x03, 0x47, 0x6e, 0xe1, 0x75} };

CATImplementInterface(CAAIPstINFRoot, CATBaseUnknown);
