// COPYRIGHT DASSAULT SYSTEMES 2002
#include "CAAIPstINFPoint.h"

IID IID_CAAIPstINFPoint = { 0xceff9ef4, 0x1f92, 0x11d6, { 0x85, 0x0a, 0x00, 0x03, 0x47, 0x6e, 0xe1, 0x75} };

CATImplementInterface(CAAIPstINFPoint, CATBaseUnknown);
