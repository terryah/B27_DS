// COPYRIGHT DASSAULT SYSTEMES 2002
#include "CAAIPstINFLine.h"

IID IID_CAAIPstINFLine = { 0x03631442, 0x1fc6, 0x11d6, { 0x85, 0x0a, 0x00, 0x03, 0x47, 0x6e, 0xe1, 0x75} };

CATImplementInterface(CAAIPstINFLine, CATBaseUnknown);
