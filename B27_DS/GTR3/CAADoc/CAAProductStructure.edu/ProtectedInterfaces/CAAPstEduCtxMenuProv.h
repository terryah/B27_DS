// COPYRIGHT DASSAULT SYSTEMES 2004
#ifdef	_WINDOWS_SOURCE
#ifdef	__CAAPstEduCtxMenuProv
#define	ExportedByCAAPstEduCtxMenuProv	__declspec(dllexport)
#else
#define	ExportedByCAAPstEduCtxMenuProv	__declspec(dllimport)
#endif
#else
#define	ExportedByCAAPstEduCtxMenuProv
#endif
