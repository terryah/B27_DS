// COPYRIGHT Dassault Systemes 2005
#ifndef CAAPstCtxMenuCmd_H
#define CAAPstCtxMenuCmd_H

#include "CATCommand.h"

/**
 * Command for the CAAPstCtxMenu use case
 */
class CAAPstCtxMenuCmd: public CATCommand {

public:

	CAAPstCtxMenuCmd();
	virtual ~CAAPstCtxMenuCmd();

};

#endif
