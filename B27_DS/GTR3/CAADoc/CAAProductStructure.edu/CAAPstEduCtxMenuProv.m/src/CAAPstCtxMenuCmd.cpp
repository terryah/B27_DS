// COPYRIGHT Dassault Systemes 2005
#include "CAAPstCtxMenuCmd.h"

#include "CATCreateExternalObject.h"
CATCreateClass(CAAPstCtxMenuCmd);

//-------------------------------------------------------------------------
// Constructor
//-------------------------------------------------------------------------
CAAPstCtxMenuCmd::CAAPstCtxMenuCmd() :
	CATCommand(NULL, "CAAPstCtxMenuCmd")
{
}

//-------------------------------------------------------------------------
// Destructor
//-------------------------------------------------------------------------
CAAPstCtxMenuCmd::~CAAPstCtxMenuCmd()
{
}
