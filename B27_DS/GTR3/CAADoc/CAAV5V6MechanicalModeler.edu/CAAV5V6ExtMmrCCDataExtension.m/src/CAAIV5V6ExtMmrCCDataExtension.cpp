// COPYRIGHT DASSAULT SYSTEMES 2012

// Local Framework
#include "CAAIV5V6ExtMmrCCDataExtension.h"

IID IID_CAAIV5V6ExtMmrCCDataExtension = { 0x431d1b66, 0x19cd, 0x4018,
{ 0xaa, 0x81, 0xb3, 0xe1, 0xa0, 0x47, 0x8c, 0x0c } };

CATImplementInterface(CAAIV5V6ExtMmrCCDataExtension, CATBaseUnknown);
CATImplementHandler(CAAIV5V6ExtMmrCCDataExtension, CATBaseUnknown);
