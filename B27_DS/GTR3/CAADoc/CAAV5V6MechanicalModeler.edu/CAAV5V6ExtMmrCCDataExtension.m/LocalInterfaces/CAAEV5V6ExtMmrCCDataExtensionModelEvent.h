// COPYRIGHT DASSAULT SYSTEMES 2012

#ifndef CAAEV5V6ExtMmrCCDataExtensionModelEvent_H
#define CAAEV5V6ExtMmrCCDataExtensionModelEvent_H

// Visualization Framework in V5
// VisualizationController Framework in V6
#include "CATExtIModelEvents.h"

//===========================================================================
//  Abstract of the class:
//  ----------------------
//
//  CATIModelEvents implementation for V5V6ExtMmrDataExtensionCont
//  - Standard Links between Model and Visu.
//===========================================================================
//===========================================================================

class CAAEV5V6ExtMmrCCDataExtensionModelEvent: public CATExtIModelEvents
{
CATDeclareClass;

public:
  CAAEV5V6ExtMmrCCDataExtensionModelEvent();
  ~CAAEV5V6ExtMmrCCDataExtensionModelEvent();
};
#endif
