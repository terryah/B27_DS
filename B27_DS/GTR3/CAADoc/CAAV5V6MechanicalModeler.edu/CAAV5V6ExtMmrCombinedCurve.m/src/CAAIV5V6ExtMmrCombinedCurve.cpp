// COPYRIGHT DASSAULT SYSTEMES 2012

// Local Framework
#include "CAAIV5V6ExtMmrCombinedCurve.h"

IID IID_CAAIV5V6ExtMmrCombinedCurve = { 0xfd78c473, 0x98c2, 0x4d44, {0xab, 0xd5, 0x48, 0x11, 0xc1, 0x39, 0x49, 0x84} };

CATImplementInterface(CAAIV5V6ExtMmrCombinedCurve, CATBaseUnknown);
CATImplementHandler(CAAIV5V6ExtMmrCombinedCurve, CATBaseUnknown);

