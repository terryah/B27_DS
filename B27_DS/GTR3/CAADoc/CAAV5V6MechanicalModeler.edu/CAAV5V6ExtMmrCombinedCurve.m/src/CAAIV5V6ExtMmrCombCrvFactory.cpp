// COPYRIGHT DASSAULT SYSTEMES 2012

// Local Framework
#include "CAAIV5V6ExtMmrCombCrvFactory.h"

IID IID_CAAIV5V6ExtMmrCombCrvFactory = { 0x16a6b0ed, 0x99d8, 0x49a9,
{ 0x8d, 0xf0, 0xda, 0x65, 0xb9, 0xbe, 0x02, 0xe4} };

CATImplementInterface(CAAIV5V6ExtMmrCombCrvFactory, CATBaseUnknown);
CATImplementHandler(CAAIV5V6ExtMmrCombCrvFactory, CATBaseUnknown);


