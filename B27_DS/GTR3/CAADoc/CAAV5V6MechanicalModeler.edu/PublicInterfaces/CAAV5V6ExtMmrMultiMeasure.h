// COPYRIGHT DASSAULT SYSTEMES 2012
#ifdef  _WINDOWS_SOURCE
#ifdef  __CAAV5V6ExtMmrMultiMeasure
#define ExportedByCAAV5V6ExtMmrMultiMeasure     __declspec(dllexport)
#else
#define ExportedByCAAV5V6ExtMmrMultiMeasure     __declspec(dllimport)
#endif
#else
#define ExportedByCAAV5V6ExtMmrMultiMeasure
#endif
