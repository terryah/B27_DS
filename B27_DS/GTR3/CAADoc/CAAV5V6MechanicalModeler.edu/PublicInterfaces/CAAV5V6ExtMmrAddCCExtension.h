#ifndef CAAV5V6ExtMmrAddCCExtension_H
#define CAAV5V6ExtMmrAddCCExtension_H

//COPYRIGHT DASSAULT SYSTEMES 2001

class CAAIV5V6ExtMmrCCDataExtension ;
#include "CAAV5V6ExtMmrCCDataExtension.h"
#include  "CATBaseUnknown.h"

HRESULT ExportedByCAAV5V6ExtMmrCCDataExtension CAAV5V6ExtMmrAddCCExtension(const CATBaseUnknown *iBaseFeature,CAAIV5V6ExtMmrCCDataExtension **ioMmrCCDataExtension) ;
                                                       

#endif

