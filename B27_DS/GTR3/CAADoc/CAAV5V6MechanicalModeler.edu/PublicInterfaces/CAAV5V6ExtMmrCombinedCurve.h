// COPYRIGHT DASSAULT SYSTEMES 2012
#ifdef  _WINDOWS_SOURCE
#ifdef  __CAAV5V6ExtMmrCombinedCurve
#define ExportedByCAAV5V6ExtMmrCombinedCurve     __declspec(dllexport)
#else
#define ExportedByCAAV5V6ExtMmrCombinedCurve     __declspec(dllimport)
#endif
#else
#define ExportedByCAAV5V6ExtMmrCombinedCurve
#endif
