// COPYRIGHT DASSAULT SYSTEMES 2012
#ifdef  _WINDOWS_SOURCE
#ifdef  __CAAV5V6ExtMmrCCDataExtension
#define ExportedByCAAV5V6ExtMmrCCDataExtension     __declspec(dllexport)
#else
#define ExportedByCAAV5V6ExtMmrCCDataExtension     __declspec(dllimport)
#endif
#else
#define ExportedByCAAV5V6ExtMmrCCDataExtension
#endif
