// COPYRIGHT DASSAULT SYSTEMES 2012

// Local Framework
#include "CAAIV5V6ExtMmrMultiMeasureFactory.h"

IID IID_CAAIV5V6ExtMmrMultiMeasureFactory = { 0x2c0a2b76, 0x567e, 0x4e20,
{0xb9, 0x28, 0x19, 0x09, 0xba, 0x9a, 0x7e, 0x5c} };

// {2c0a2b76-567e-4e20-b928-1909ba9a7e5c}

CATImplementInterface(CAAIV5V6ExtMmrMultiMeasureFactory, CATBaseUnknown);
CATImplementHandler(CAAIV5V6ExtMmrMultiMeasureFactory, CATBaseUnknown);
