// COPYRIGHT DASSAULT SYSTEMES 2012

// Local Framework
#include "CAAIV5V6ExtMmrMultiMeasure.h"

IID IID_CAAIV5V6ExtMmrMultiMeasure ={  0xcf11eb53,  0x550f,  0x4245, 
                               {0x8a, 0x1b, 0x44, 0xbf, 0xbb, 0x09, 0x79, 0xf8} };

// {cf11eb53-550f-4245-8a1b-44bfbb0979f8}
CATImplementInterface(CAAIV5V6ExtMmrMultiMeasure, CATBaseUnknown);
CATImplementHandler(CAAIV5V6ExtMmrMultiMeasure, CATBaseUnknown);
