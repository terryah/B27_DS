#ifndef CAAV5V6MmrRetrieveCornerAndVectorsFromPad_H
#define CAAV5V6MmrRetrieveCornerAndVectorsFromPad_H

//COPYRIGHT DASSAULT SYSTEMES 2012

#include "CATUnicodeString.h" 
#include "CATBaseUnknown.h" 

HRESULT CAAV5V6MmrRetrieveCornerAndVectorsFromPad(CATBaseUnknown *  iInputPad, 
                                              CATBaseUnknown ** oVectorX,
				                              CATBaseUnknown ** oVectorY);

#endif

