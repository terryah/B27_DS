// COPYRIGHT DASSAULT SYSTEMES 1999


BUILT_OBJECT_TYPE=SHARED LIBRARY

LINK_WITH = JS0GROUP \
            CATNewTopologicalObjects \
            CATMathematics \
            CATGeometricObjects \
            MecModItf \
            PartItf\
            CATObjectModelerBase\
            CATObjectSpecsModeler \
            CATMechanicalModeler \
            CATTopologicalOperators \
            CATLiteralFeatures \
            CATMmiUUID \
            MechanicalModelerUUID \
            ObjectSpecsModelerUUID \
            PartInterfacesUUID \
            CATMathStream \
            CATCGMGeoMath \
            CATConstraintModeler \
            CATConstraintModelerItf \
            KnowledgeItf \
