// COPYRIGHT DASSAULT SYSTEMES 1999

BUILT_OBJECT_TYPE=LOAD MODULE

LINK_WITH = SketcherItf\
            JS0GROUP \
            CATNewTopologicalObjects \
            CATMathematics \
            CATGeometricObjects \
            MecModItf \
            PartItf\
            CATObjectModelerBase \
            CATObjectSpecsModeler \
            CATMechanicalModeler \
            CATTopologicalOperators \
            CATLiteralFeatures \
            CATMathStream \
            CATCGMGeoMath \
            CATConstraintModeler \
            CATConstraintModelerItf \
            KnowledgeItf \
