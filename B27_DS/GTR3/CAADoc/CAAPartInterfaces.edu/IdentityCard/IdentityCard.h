// Copyright � 1994-2000 Dassault Syst�mes.
// All right reserved

//
// -->Prereq Components Declaration

AddPrereqComponent("System",                  Public);
AddPrereqComponent("Mathematics",             Public);
AddPrereqComponent("Visualization",           Public);
AddPrereqComponent("VisualizationBase",       Public);
AddPrereqComponent("ApplicationFrame",        Public);
AddPrereqComponent("Dialog",                  Public);
AddPrereqComponent("DialogEngine",            Public);
AddPrereqComponent("ObjectModelerBase",       Public);
AddPrereqComponent("ObjectSpecsModeler",      Public);
AddPrereqComponent("KnowledgeInterfaces",     Public);
AddPrereqComponent("LiteralFeatures",         Public);
AddPrereqComponent("GeometricObjects",        Public);
AddPrereqComponent("NewTopologicalObjects",   Public);
AddPrereqComponent("TopologicalOperators",    Public);
AddPrereqComponent("InteractiveInterfaces",   Public);
AddPrereqComponent("MecModInterfaces",        Public);
AddPrereqComponent("MechanicalModeler",       Public);
AddPrereqComponent("MechanicalModelerUI",     Public);
AddPrereqComponent("ConstraintModeler",       Public);
AddPrereqComponent("ConstraintModelerUI",     Public);
AddPrereqComponent("ConstraintModelerInterfaces",  Public);
AddPrereqComponent("SketcherInterfaces",      Public);
AddPrereqComponent("PartInterfaces",          Public);
