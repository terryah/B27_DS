// COPYRIGHT DASSAULT SYSTEMES 1999

BUILT_OBJECT_TYPE=SHARED LIBRARY

LINK_WITH = JS0GROUP \
    CATSketcherInterfaces\
    CATMathematics \
    CATPartInterfaces\
    CATObjectModelerBase \
    CATObjectSpecsModeler \
    CATMechanicalModeler \
    CATViz \
    CATVisualization \
            CATMathStream \
            CATCGMGeoMath \
            CATConstraintModeler \
            CATConstraintModelerItf \
            CATMecModInterfaces \
MechanicalModelerUUID \
ObjectSpecsModelerUUID \
SketcherInterfacesUUID \
