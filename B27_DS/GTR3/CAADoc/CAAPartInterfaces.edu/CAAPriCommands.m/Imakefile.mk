# COPYRIGHT DASSAULT SYSTEMES 2002
#
BUILT_OBJECT_TYPE=SHARED LIBRARY 

LINK_WITH = JS0GROUP \
            JS0FM \
            DI0PANV2 \
            CATApplicationFrame  \
            CATDialogEngine \
            CATViz\
            CATVisualization \
            CATObjectModelerBase \
            CATObjectSpecsModeler \
            CATInteractiveInterfaces \
            CATMechanicalModelerUI \
            CATConstraintModelerUI \
            CATSketcherInterfaces \
            CATPartInterfaces \
