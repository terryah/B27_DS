// Copyright � 1994-2000 Dassault Syst�mes.
// All right reserved

BUILT_OBJECT_TYPE=SHARED LIBRARY

LINK_WITH= JS0GROUP \
           CATMathematics \
           CATMathStream \
           CATVisualization \
           CATViz \
           CATObjectModelerBase \
           CATObjectSpecsModeler \
           ObjectSpecsModelerUUID \
           CATGeometricObjects \
           CATCGMGeoMath \
           CATNewTopologicalObjects \
           CATTopologicalOperators \
           MecModItf \
           CATMmiUUID \
           MechanicalModelerUUID \
           CATConstraintModeler \
           CATConstraintModelerItf \
           CATMechanicalModeler \
           CATPartInterfaces \


