// Copyright � 1994-2000 Dassault Syst�mes.
// All right reserved


BUILT_OBJECT_TYPE=LOAD MODULE

LINK_WITH = JS0GROUP \
            CATMathematics \
            CATViz \
            CATVisualization \
            CATObjectModelerBase\
            CATObjectSpecsModeler \
            SketcherItf\
            CATNewTopologicalObjects \
            CATGeometricObjects \
            MecModItf \
            PartItf\
            CATMechanicalModeler \
            CATTopologicalOperators \
            CATLiteralFeatures \
            CATMathStream \
            CATCGMGeoMath \
            CATConstraintModeler \
            CATConstraintModelerItf \
            KnowledgeItf \
