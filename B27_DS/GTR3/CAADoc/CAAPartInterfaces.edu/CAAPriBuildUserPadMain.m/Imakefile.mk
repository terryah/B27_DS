// Copyright � 1994-2000 Dassault Syst�mes.
// All right reserved


BUILT_OBJECT_TYPE=LOAD MODULE

LINK_WITH = JS0GROUP \
            CATSketcherInterfaces \
            CATMathematics \
            CATMathStream \
            CATObjectModelerBase \
            CATObjectSpecsModeler \
            KnowledgeItf \
            CATLiteralFeatures \
            CATGeometricObjects \
            CATCGMGeoMath \
            CATNewTopologicalObjects \
            CATTopologicalOperators \
            CATMecModInterfaces \
            CATConstraintModelerItf \
            CATConstraintModeler \
            CATMechanicalModeler \
            CATPartInterfaces \
