// Copyright � 1994-2000 Dassault Syst�mes.
// All right reserved

BUILT_OBJECT_TYPE=LOAD MODULE

LINK_WITH = JS0GROUP \
            CATMathematics \
            CATMathStream \
            CATObjectModelerBase \
            CATObjectSpecsModeler \
            CATGeometricObjects \
            CATCGMGeoMath \
            CATMecModInterfaces \
            CATMechanicalModeler \
            CATConstraintModelerItf \
            CATConstraintModeler \
            CATSketcherInterfaces \
            CATPartInterfaces \
