// Copyright DASSAULT SYSTEMES 2000

   AddPrereqComponent("MecModInterfaces",        Public);
   AddPrereqComponent("System",        Public);
   AddPrereqComponent("ObjectModelerBase",        Public);
   AddPrereqComponent("ObjectSpecsModeler",        Public);
   AddPrereqComponent("Visualization",        Public);
   AddPrereqComponent("VisualizationBase",        Public);

