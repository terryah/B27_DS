// COPYRIGHT DASSAULT SYSTEMES 2003
//
// CAAPlantShipInterfaces.edu -->Prereq Components Declaration
AddPrereqComponent ("ObjectSpecsModeler"        , Public);
AddPrereqComponent ("ObjectModelerBase"         , Public);
AddPrereqComponent ("System"                    , Public);
AddPrereqComponent ("ProductStructure"          , Public);
AddPrereqComponent ("Mathematics"               , Public);
AddPrereqComponent ("KnowledgeInterfaces"       , Public);
AddPrereqComponent ("CATArrangementInterfaces"  , Public);
AddPrereqComponent ("Visualization"             , Public);
AddPrereqComponent ("CATPlantShipInterfaces"    , Public);
