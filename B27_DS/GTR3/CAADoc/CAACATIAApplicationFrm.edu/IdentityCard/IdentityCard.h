//
// COPYRIGHT DASSAULT SYSTEMES 1999
//
// -->Prereq Components Declaration
   AddPrereqComponent("ApplicationFrame",Public);
   AddPrereqComponent("CATIAApplicationFrame",Public);
   AddPrereqComponent("Dialog",Public);
   AddPrereqComponent("Mathematics",Public);
   AddPrereqComponent("ObjectModelerBase", Public);
   AddPrereqComponent("System",Public);
   AddPrereqComponent("Visualization",Public);
   AddPrereqComponent("VisualizationBase",Public);
   AddPrereqComponent("InteractiveInterfaces",Public);
   AddPrereqComponent("CAASystem.edu",Public);
   AddPrereqComponent("KnowledgeInterfaces",Public);
   AddPrereqComponent("DialogEngine",Public);

