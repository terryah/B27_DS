// COPYRIGHT DASSAULT SYSTEMES 1999

#include <CAAICafGeometryEltSettingAtt.h>

// Use uuid_gen -C on UNIX or uuidgen -s on NT  to generate the IID 
//
IID IID_CAAICafGeometryEltSettingAtt={ /* 8a2969d2-0e0d-11d5-85c1-00108335648a */
    0x8a2969d2,
    0x0e0d,
    0x11d5,
    {0x85, 0xc1, 0x00, 0x10, 0x83, 0x35, 0x64, 0x8a}
  };




