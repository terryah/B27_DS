// COPYRIGHT DASSAULT SYSTEMES 1999

#include <CAAICafGeometryViewSettingAtt.h>

// Use uuid_gen -C on UNIX or uuidgen -s on NT  to generate the IID 
//
IID IID_CAAICafGeometryViewSettingAtt={ /* ee7a2e40-1f5a-11d5-85c1-00108335648a */
    0xee7a2e40,
    0x1f5a,
    0x11d5,
    {0x85, 0xc1, 0x00, 0x10, 0x83, 0x35, 0x64, 0x8a}
  };




