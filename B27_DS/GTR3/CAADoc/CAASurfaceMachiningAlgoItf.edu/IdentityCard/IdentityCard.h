//===================================================================
// COPYRIGHT DASSAULT SYSTEMES 2006
//===================================================================
// -->Prereq Components Declaration

AddPrereqComponent("ManufacturingInterfaces",Public);
AddPrereqComponent("SurfaceMachiningAlgoInterfaces",Public);

AddPrereqComponent("GeometricObjects",Public);
AddPrereqComponent("Mathematics",Public);
AddPrereqComponent("ObjectModelerBase",Public);
AddPrereqComponent("ObjectSpecsModeler",Public);
AddPrereqComponent("System",Public);
AddPrereqComponent("SpecialAPI",Public);
AddPrereqComponent("Visualization",Public);
AddPrereqComponent("VisualizationBase",Public);
AddPrereqComponent("MecModInterfaces",Public);
AddPrereqComponent("NewTopologicalObjects",Public);
AddPrereqComponent("GSMInterfaces",Public);
