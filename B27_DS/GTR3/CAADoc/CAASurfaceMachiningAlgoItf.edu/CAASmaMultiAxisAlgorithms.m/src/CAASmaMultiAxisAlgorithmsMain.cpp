//=============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2006
//=============================================================================
//
// CAASmaMultiAxisAlgorithmsMain:
//
// Use case for   : Machining Algorithms
//
// Type           : Batch program
//
// Mission        : Describes how to use of CATIMfgMultiAxisAlgorithm interface.
//
// Role           : Uses multi-axis machining algorithms to create geometrical points
//                  - Open a CATPart and retrieve pre-defined geometry
//                  - Runs multi-axis sweeping algorithm on pre-defined geometry
//                  - Runs multi-axis contour driven algorithm on pre-defined geometry
//                  - Creates geometrical points from tool paths computed by machining algorithms
//
// How to execute : mkrun -c "CAASmaMultiAxisAlgorithms InputPath [OutputPath]"
//                      with InputPath : The Part document path. You will find it :
//                              Unix : InstallRootDirectory/CAASurfaceMachiningAlgoItf.edu/InputData 
//                              Windows : InstallRootDirectory\CAASurfaceMachiningAlgoItf.edu\InputData 
//                      with OutputPath : The output path where the Part document will be saved (with created points).
//                              If this path is empty, the file will be saved in the current directory. 
//                  Example: mkrun -c "CAASmaMultiAxisAlgorithms c:\catia\CAASurfaceMachiningAlgoItf.edu\InputData\CAAMultiAxisAlgorithms c:\output\CAAMultiAxisAlgorithms_Saved"
//
// Return Code    : 0- Successful execution 
//                  1- Session fails
//                  2- CATPart document can not be opened
//                  3- Part container is not found
//                  4- No geometry is found
//                  5- CATProcess document can not be created
//                  6- Process container is not found
//                  7- There is no toolpath container
//                  8- Sweeping algo cannot be instanciated
//                  9- Sweeping computation fails
//                 10- Contour driven algo cannot be instanciated
//                 11- Contour driven computation fails
//                 12- Geometry creation from multi-axis sweeping tool path fails
//                 13- Geometry creation from multi-axis contour driven tool path fails
//                 14- Part document can not be saved
//                 15- Document can not be removed
//
//=============================================================================

// Manufacturing
#include "CATIMfgManufacturingFactories.h"
#include "CATIMfgMachiningContainer.h"
#include "CATIMfgMultiAxisAlgorithm.h"
#include "CATIMfgToolPathComponents.h"
#include "CATIMfgTPMultipleMotion.h"

// Object Modeler Base Interfaces
#include "CATIContainer.h"
#include "CATSession.h"
#include "CATSessionServices.h"
#include "CATInit.h"
#include "CATDocumentServices.h"

// Object Specs Modeler Interfaces
#include "CATISpecObject.h" 
#include "CATIDescendants.h"
#include "LifeCycleObject.h"

// System Interfaces
#include "CATUnicodeString.h"
#include "CATString.h"
#include "CATIAlias.h"
#include "CATInstantiateComponent.h"

// DMA Interfaces
#include "CATISPPProcessContainer.h"

// Mech Mod Interfaces
#include "CATIPartRequest.h"
#include "CATIPrtContainer.h"
#include "CATIMechanicalRootFactory.h"

// Topology Interfaces
#include "CATListOfCATCells.h"
#include "CATListOfCATCurves.h"
#include "ListPOfCATFace.h"
#include "CATIGeometricalElement.h"
#include "CATBody.h"
#include "CATFace.h"
#include "CATCurve.h"
#include "CATEdgeCurve.h"
#include "CATEdge.h"

// GSM Interfaces
#include "CATIGSMFactory.h"
#include "CATIGSMPoint.h"

// Visualization Interfaces
#include "CATIVisProperties.h"
#include "CATVisPropertiesValues.h"

// Mathematics Interfaces
#include "CATMathPlane.h"



//=============================================================================
// CreatePointsFromToolPath
// Scans tool path and creates explicit points in a new geometrical set
//=============================================================================
HRESULT CreatePointsFromToolPath(const CATBaseUnknown_var & ispToolPath, const CATBaseUnknown_var & ispPartContainer)
{
    HRESULT rc = S_OK;

    // Creates a geometrical set
    CATISpecObject_var ospGeomSet;
    CATIMechanicalRootFactory_var spMechRootFactory(ispPartContainer);
    if (NULL_var != spMechRootFactory)
    {
        CATIPrtContainer_var spPrtContainer = ispPartContainer;
        if (NULL_var != spPrtContainer)
        {
            CATISpecObject_var spRootPart = spPrtContainer->GetPart();
            spMechRootFactory->CreateGeometricalSet("",spRootPart,ospGeomSet);
        }
    }
    CATIDescendants_var spDescendantOnGeomSet = ospGeomSet;
    if (NULL_var == spDescendantOnGeomSet) return E_FAIL;
    
    // Scans Tool Path
    int NbCreatedPoints = 0;
    CATIGSMFactory_var spGSMFactory (ispPartContainer);
    CATIMfgToolPathComponents_var spTPComponents (ispToolPath);
    if (NULL_var != spTPComponents && NULL_var != spGSMFactory)
    {
        CATListValCATBaseUnknown_var* pListOfMultipleMotion = spTPComponents ->GetAllElements ();
        if (pListOfMultipleMotion)
        {
            if (pListOfMultipleMotion->Size() > 0)
            {
                CATIMfgTPMultipleMotion_var spMultipleMotion ((*pListOfMultipleMotion)[1]);
                if (NULL_var != spMultipleMotion)
                {
                    int GreenColor = 0;
                    int NbSubTrajects = 0;
                    spMultipleMotion->GetNumberOfSubTrajects(NbSubTrajects);

                    for (int ia=1;ia<=NbSubTrajects;ia++)
                    {
                        CATIMfgTPMultipleMotion::SubTrajectType SubTrajectType;
                        spMultipleMotion->GetSubTrajectType(ia,SubTrajectType);
                        if (CATIMfgTPMultipleMotion::UserSyntax == SubTrajectType) // Syntax defined in macros
                        {
                            CATUnicodeString Syntax;
                            spMultipleMotion->GetUserSyntaxCharacteristics(ia,Syntax);
                            if (0 != Syntax.Compare("START")) GreenColor = 1;
                            else if (0 != Syntax.Compare("END")) GreenColor = 0;
                        }
                        else // Traject
                        {
                            int StartNumber =0, EndNumber =0;
                            spMultipleMotion->GetStartAndEndNumber(ia,StartNumber,EndNumber);
                            for (int ib=StartNumber;ib<=EndNumber;ib++)
                            {
                                double x=0.,y=0.,z=0.;
                                spMultipleMotion->GetTipPoint(ib,x,y,z);

                                double Coord [3] = {x,y,z};
                                CATIGSMPoint_var spGSMPoint = spGSMFactory->CreatePoint(Coord);
                                CATISpecObject_var spSpecPoint = spGSMPoint;
                                if (NULL_var != spSpecPoint)
                                {
                                    spDescendantOnGeomSet->Append(spSpecPoint);

                                    if (1 == GreenColor) // Points lying on parts will be green
                                    {
						                CATIVisProperties_var spGraphicsPoint = spGSMPoint;
						                if (NULL_var != spGraphicsPoint)
						                {
							                CATVisPropertiesValues VisProperties;
							                VisProperties.SetColor(0, 255, 0); // Green color
							                spGraphicsPoint->SetPropertiesAtt(VisProperties, CATVPColor, CATVPPoint);
                                            
                                            NbCreatedPoints ++;
						                }
                                    }
                                    spSpecPoint->Update();
                                }
                            }
                        }

                    }
                }
            }
            delete pListOfMultipleMotion;
            pListOfMultipleMotion = NULL;
        }
    }    
    else rc = E_FAIL;

    if (0 == NbCreatedPoints) rc = E_FAIL;
    return rc;
}

//=============================================================================
// ExitSession
// Deletes session before exit
//=============================================================================
int ExitSession(const int ErrCode)
{
    ::Delete_Session("CAAMfgSession"); 
    return ErrCode;
}


//=============================================================================
// Main Program
//=============================================================================
int main (int iArgc, char * argv[] )
{ 
	HRESULT rc = E_FAIL;
    CATMathVector XVector(1.,0.,0.), YVector(0.,1.,0.);
    CATMathPoint Pt150 (0.,0.,150.);
    CATMathPlane MacroPlane; MacroPlane.SetOrigin (Pt150);


	//=============================================================================
	// 1 - Creates a session
	//=============================================================================
	CATSession *pSession = NULL;
	rc = ::Create_Session("CAAMfgSession", pSession);
	if (FAILED(rc)) return ExitSession(1); // Session fails


    //=============================================================================
	// 2 - Opens CATPart document and retrieves its root container
    // Name of Part is given as first argument of main program
	//=============================================================================
    CATDocument *pPartDoc = NULL;
    CATUnicodeString InputPathName(argv[1]);
    if (InputPathName.SearchSubString(".CATPart") <0) InputPathName = InputPathName + ".CATPart"; // Add CATPart format if not given
    rc = CATDocumentServices::OpenDocument(InputPathName, pPartDoc);
    if (FAILED(rc) || NULL == pPartDoc) return ExitSession(2); // CATPart document can not be opened

    // Retrieves root container of part document
    CATBaseUnknown_var spPartContainer;
	CATBaseUnknown_var spPart;
    CATInit *piInitOnPrtDoc = NULL;
    rc = pPartDoc->QueryInterface(IID_CATInit, (void**) &piInitOnPrtDoc);
    if (SUCCEEDED(rc))
    {
        CATIPrtContainer * pPrtContainer = (CATIPrtContainer *) piInitOnPrtDoc->GetRootContainer(CATIPrtContainer::ClassName());
	    if (pPrtContainer)
	    {
		    spPartContainer = pPrtContainer;
            spPart = pPrtContainer->GetPart();
            pPrtContainer->Release();
            pPrtContainer = NULL;
        }
        piInitOnPrtDoc->Release();
        piInitOnPrtDoc = NULL;
    }
    if (NULL_var == spPartContainer || NULL_var == spPart) return ExitSession(3); // Part container is not found


    //=============================================================================
    // 3 - Retrieves geometry of the CATPart
    //      Surface "PARTS1" defines parts of Sweeping
    //      Surface "PARTS2" defines parts of Contour Driven
    //      Join "GUIDE1" defines First guide of Contour Driven
    //      Join "GUIDE2" defines Second guide of Contour Driven
 	//=============================================================================
    CATLISTP(CATFace) ListOfParts1, ListOfParts2;
    CATLISTP(CATCurve) ListOfGuide1, ListOfGuide2;
    // Scans part document
    CATIPartRequest_var spPartRequest(spPart); 
    if (NULL_var != spPartRequest)
    {
        // Retrieves first geometrical set
        CATListValCATBaseUnknown_var ListOfSurfacicSets;
        spPartRequest->GetSurfBodies(CATUnicodeString (""), ListOfSurfacicSets);
        if (ListOfSurfacicSets.Size() > 0)
        {
            CATIDescendants_var spDescOnSurfacicSet = ListOfSurfacicSets[1];
            if (NULL_var != spDescOnSurfacicSet)
            {
                // Retrieves geometrical elements
                CATListValCATISpecObject_var ListOfGeometricalElts;
                spDescOnSurfacicSet->GetDirectChildren(CATIGeometricalElement::ClassName(), ListOfGeometricalElts);
                int NbGeometricalElts = ListOfGeometricalElts.Size();
                for (int ig=1;ig<=NbGeometricalElts;ig++)
                {
                    CATIGeometricalElement_var spGeomElement = ListOfGeometricalElts[ig];
                    if (NULL_var != spGeomElement)
                    {
                        CATBody_var spBody = spGeomElement->GetBodyResult();
                        if (NULL_var != spBody)
                        {
                            // Compare name
                            CATLISTP(CATCell) ListOfCells;
                            CATUnicodeString sEltName;
                            CATIAlias_var spAliasOnGeomElt = spGeomElement;
                            if (NULL_var != spAliasOnGeomElt) sEltName =  spAliasOnGeomElt->GetAlias();
                            int IsParts1 = sEltName.Compare("PARTS1");
                            int IsParts2 = sEltName.Compare("PARTS2");
                            int IsGuide1 = sEltName.Compare("GUIDE1");
                            int IsGuide2 = sEltName.Compare("GUIDE2");

                            // Retrieves faces for parts
                            if (0 != IsParts1 || 0 != IsParts2)
                            {
                                spBody->GetAllCells(ListOfCells,2);
                                int NbCells = ListOfCells.Size();
                                for (int i=1;i<=NbCells;i++)
                                {
                                    CATFace_var spFace = ListOfCells[i];
                                    if (NULL_var != spFace)
                                    {
                                        if (0 != IsParts1) ListOfParts1.Append(spFace);
                                        else ListOfParts2.Append(spFace);
                                    }
                                }      
                            }
                            // Retrieves curves for guides
                            else if (0 != IsGuide1 || 0 != IsGuide2)
                            {
                                spBody->GetAllCells(ListOfCells,1);
                                int NbCells = ListOfCells.Size();
                                for (int i=1;i<=NbCells;i++)
                                {
                                    CATEdge_var spEdge = ListOfCells[i];
                                    if (NULL_var != spEdge)
                                    {
                                        CATEdgeCurve * pEdgeCurve = spEdge->GetCurve();        
                                        CATCurve_var spCurve = pEdgeCurve;
                                        if (NULL_var != spCurve)
                                        {
                                            if (0 != IsGuide1) ListOfGuide1.Append(spCurve);
                                            else ListOfGuide2.Append(spCurve);
                                        }
                                    }
                                }      
                            }
                        }
                    }                            
                }
            }
        }
    }
    if (ListOfParts1.Size() == 0 || ListOfParts2.Size() == 0 || ListOfGuide1.Size() == 0 || ListOfGuide2.Size() == 0) 
        return ExitSession(4); // No geometry is found


    //=============================================================================
    // 4 - Creates a CATProcess and retrieves its root container
    //=============================================================================
    CATDocument *pProcessDoc = NULL;
	rc = CATDocumentServices::New("Process", pProcessDoc );
    if (FAILED(rc) || NULL == pProcessDoc) return ExitSession(5); // CATProcess document can not be created
    CATSession::SetCurrentDoc(pProcessDoc);
    
    CATISPPProcessContainer *piProcessContainer = NULL; 
    CATInit *piInitOnProcDoc = NULL;
    rc = pProcessDoc->QueryInterface(IID_CATInit, (void**) &piInitOnProcDoc);
    if (SUCCEEDED(rc))
    {
        piProcessContainer = (CATISPPProcessContainer*) piInitOnProcDoc->GetRootContainer("CATISPPProcessContainer");
        piInitOnProcDoc->Release();
        piInitOnProcDoc = NULL;
    }
    if (NULL == piProcessContainer) return ExitSession(6); // Process container is not found


	//=============================================================================
    // 5 - Initializes manufacturing environment
	//=============================================================================
    CATIMfgMachiningContainer * piMfgEnvt = NULL;
    piProcessContainer->QueryInterface(CATIMfgMachiningContainer::ClassId(), (void**)&piMfgEnvt);
    if (piMfgEnvt)
    {
        piMfgEnvt->InitContainer(FALSE,0);
        piMfgEnvt->Release();
        piMfgEnvt = NULL;
    }
    piProcessContainer->Release();
    piProcessContainer = NULL;


	//=============================================================================
    // 6 - Gets tool path container
	//=============================================================================
	CATIContainer_var spTPContainer;
    CATIMfgManufacturingFactories *piFact =NULL;
	CATString ClassName("CATMfgManufacturingFactories");
    ::CATInstantiateComponent (ClassName, CATIMfgManufacturingFactories::ClassId(), (void**)& piFact);
	if (piFact)
	{
        piFact->GetManufacturingToolPathFactory(spTPContainer);
        piFact->Release();
        piFact = NULL;
    }
    if (NULL_var == spTPContainer) return ExitSession(7); // There is no toolpath container


	//=============================================================================
    // 7 - Creates a multi-axis sweeping algorithm
    //     And sets parameters :
    //      * Numerical Parameters
    //          Machining tolerance = 0.1mm
    //          Maximum discretization step = 100.mm
    //          Distance on part = 10.mm
    //      * Directions
    //          View dir = +X
    //          Start dir = +Y
    //      * Geometry
    //          Parts = PARTS1
    //      * Macros
    //          Approach macro = Word "start" + Tangent + UptoAPlane(Z=150)
    //          Retract macro = Word "end" + UptoAPlane(Z=150)
    //          Linking Approach macro = Word "start" + Axial
    //          Linking Retract macro = Word "end" + Axial
    //          Return in a level Approach macro = Word "start" + Circular
    //          Return in a level Retract macro = Word "end" + Circular
	//=============================================================================
    CATIMfgMultiAxisAlgorithm *piMMSweepingAlgo =NULL;
    ::CATInstantiateComponent (MultiAxisSweepingInstanceName, CATIMfgMultiAxisAlgorithm::ClassId(), (void**)& piMMSweepingAlgo);
    if (NULL == piMMSweepingAlgo) return ExitSession(8); // Sweeping algo cannot be instanciated
    
    rc = piMMSweepingAlgo->SetValue(MfgAlgMachiningTolerance,0.1); // Machining tolerance
    rc = piMMSweepingAlgo->SetValue(MfgAlgMaxDiscretizationStep,100.); // Maximum discretization step
    rc = piMMSweepingAlgo->SetValue(MfgAlgMaxDistance,10.); // Distance on part

    rc = piMMSweepingAlgo->SetDirection(MfgAlgViewDirection,XVector); // View dir
    rc = piMMSweepingAlgo->SetDirection(MfgAlgStartDirection,YVector); // Start dir

    rc = piMMSweepingAlgo->SetSurfacicGeometry(MfgAlgParts,ListOfParts1); // Parts

    rc = piMMSweepingAlgo->AddMacroSyntax(1,"START"); // Approach macro
    rc = piMMSweepingAlgo->AddMacroTangentMotion(1,10.,90.,0.);
    rc = piMMSweepingAlgo->AddMacroToAPlaneMotion(1,MacroPlane);

    rc = piMMSweepingAlgo->AddMacroSyntax(2,"END"); // Retract macro
    rc = piMMSweepingAlgo->AddMacroToAPlaneMotion(2,MacroPlane);

    rc = piMMSweepingAlgo->AddMacroSyntax(3,"START"); // Linking Approach macro
    rc = piMMSweepingAlgo->AddMacroAxialMotion(3);

    rc = piMMSweepingAlgo->AddMacroSyntax(4,"END"); // Linking Retract macro
    rc = piMMSweepingAlgo->AddMacroAxialMotion(4);

    rc = piMMSweepingAlgo->AddMacroSyntax(5,"START"); // Return in a level Approach macro
    rc = piMMSweepingAlgo->AddMacroCircularMotion(5,90.,90.,10.);

    rc = piMMSweepingAlgo->AddMacroSyntax(6,"END"); // Return in a level Retract macro
    rc = piMMSweepingAlgo->AddMacroCircularMotion(6,90.,90.,10.);


    //=============================================================================
    // 8 - Computes and gets toolpath
    //=============================================================================
    CATBaseUnknown_var spSweepingTP;
    rc = piMMSweepingAlgo->ComputeToolPath(spTPContainer,spSweepingTP);
    piMMSweepingAlgo->Release();
    piMMSweepingAlgo = NULL;
    if (FAILED(rc) || NULL_var == spSweepingTP) return ExitSession(9); // Sweeping computation fails


	//=============================================================================
    // 9 - Creates a multi-axis contour driven algorithm
    //     And sets parameters :
    //      * Numerical Parameters
    //          Machining tolerance = 0.1mm
    //          Distance on part = 10.mm
    //          Offset on guides = -1mm
    //          Tool path style = One-way
    //          Guiding strategy = Between Contour
    //      * Directions
    //          View dir = Normal
    //          Start dir = +Y
    //      * Geometry
    //          Parts = PARTS2
    //          First guide = GUIDE1
    //          Second guide = GUIDE2
    //      * Macros
    //          Approach macro = Word "start" + Tangent + UptoAPlane(Z=150)
    //          Retract macro = Word "end" + Tangent + UptoAPlane(Z=150)
	//=============================================================================
    CATIMfgMultiAxisAlgorithm *piMMContourDriven =NULL;
    ::CATInstantiateComponent (MultiAxisContourDrivenInstanceName, CATIMfgMultiAxisAlgorithm::ClassId(), (void**)& piMMContourDriven);
    if (NULL == piMMContourDriven) return ExitSession(10); // Contour driven algo cannot be instanciated

    rc = piMMContourDriven->SetValue(MfgAlgMachiningTolerance,0.1); // Machining tolerance
    rc = piMMContourDriven->SetValue(MfgAlgMaxDistance,10.); // Distance on part
    rc = piMMContourDriven->SetValue(MfgAlgOffsetOnGuide1,-1.); // Offset on guide 1
    rc = piMMContourDriven->SetValue(MfgAlgOffsetOnGuide2,-1.); // Offset on guide 2
    rc = piMMContourDriven->SetValue(MfgAlgContouringMode,1); // Between Contour guiding strategy
    rc = piMMContourDriven->SetValue(MfgAlgMachiningMode,2); // One-way tool path style

    CATMathVector NormalView(2.,0.,1.);
    rc = piMMContourDriven->SetDirection(MfgAlgViewDirection,NormalView); // View dir
    rc = piMMContourDriven->SetDirection(MfgAlgStartDirection,YVector); // Start dir

    rc = piMMContourDriven->SetSurfacicGeometry(MfgAlgParts,ListOfParts2); // Parts
    rc = piMMContourDriven->SetWireFrameGeometry(MfgAlgGuide1,ListOfGuide1); // First guide
    rc = piMMContourDriven->SetWireFrameGeometry(MfgAlgGuide2,ListOfGuide2); // Second guide

    rc = piMMContourDriven->AddMacroSyntax(1,"START"); // Approach macro
    rc = piMMContourDriven->AddMacroTangentMotion(1,10.,90.,0.);
    rc = piMMContourDriven->AddMacroToAPlaneMotion(1,MacroPlane);

    rc = piMMContourDriven->AddMacroSyntax(2,"END"); // Retract macro
    rc = piMMContourDriven->AddMacroTangentMotion(2,10.,90.,0.);
    rc = piMMContourDriven->AddMacroToAPlaneMotion(2,MacroPlane);


    //=============================================================================
    // 10 - Computes and gets toolpath
    //=============================================================================
    CATBaseUnknown_var spContourDrivenTP;
    rc = piMMContourDriven->ComputeToolPath(spTPContainer,spContourDrivenTP);
    piMMContourDriven->Release();
    piMMContourDriven = NULL;
    if (FAILED(rc) || NULL_var == spContourDrivenTP) return ExitSession(11);  // Contour driven computation fails


    //=============================================================================
    // 11 - Creates points from tool path of multi-axis sweeping algorithm
    //=============================================================================
    rc = CreatePointsFromToolPath(spSweepingTP, spPartContainer);
    if (FAILED(rc)) return ExitSession(12); // Geometry creation from multi-axis sweeping tool path fails


    //=============================================================================
    // 12 - Creates points from tool path of multi-axis contour driven algorithm
    //=============================================================================
    rc = CreatePointsFromToolPath(spContourDrivenTP, spPartContainer);
    if (FAILED(rc)) return ExitSession(13); // Geometry creation from multi-axis contour driven tool path fails


	//=============================================================================
    // 13 - Saves CATPart document
    // Saved Name is given as second argument of main program
 	//=============================================================================
    CATUnicodeString OutputPathName = "CAAMultiAxisAlgorithms_Saved";
    if  (3 == iArgc) OutputPathName = argv[2];
    if (OutputPathName.SearchSubString(".CATPart") <0) OutputPathName = OutputPathName + ".CATPart"; // Add CATPart format if not given
    rc = CATDocumentServices::SaveAs (*pPartDoc,OutputPathName);
    if (FAILED(rc)) return ExitSession(14); // Part document can not be saved


	//=============================================================================
    // 14 - Deletes tool path results
	//=============================================================================
    LifeCycleObject_var spLifeOnSweepTP = spSweepingTP;
    if (NULL_var != spLifeOnSweepTP)
        spLifeOnSweepTP->remove();
    LifeCycleObject_var spLifeOnCDrivenTP = spContourDrivenTP;
    if (NULL_var != spLifeOnCDrivenTP)
        spLifeOnCDrivenTP->remove();


	//=============================================================================
    // 15 - Removes opened documents
	//=============================================================================
    rc = CATDocumentServices::Remove (*pProcessDoc);
    if (FAILED(rc)) return ExitSession(15); // Document can not be removed

    rc = CATDocumentServices::Remove (*pPartDoc);
    if (FAILED(rc)) return ExitSession(15); // Document can not be removed

	
	//=============================================================================
    // 16 -  Deletes the session	
	//=============================================================================
    ExitSession(0);

	return 0;
}
