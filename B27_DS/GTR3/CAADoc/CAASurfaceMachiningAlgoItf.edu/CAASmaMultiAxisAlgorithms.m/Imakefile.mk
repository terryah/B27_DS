#======================================================================
# COPYRIGHT DASSAULT SYSTEMES 2006
#======================================================================
# Imakefile for module CAASmaMultiAxisAlgorithms.m
#======================================================================
#
# LOAD MODULE 
#
BUILT_OBJECT_TYPE=LOAD MODULE 
 
LINK_WITH = 	JS0GROUP AC0SPBAS YP00IMPL\
				CATObjectModelerBase \
				MecModItfCPP YI00IMPL CATMathematics CATMathStream CATVisualization CATGitInterfaces \
				CATManufacturingInterfaces CATSurfaceMachiningAlgoInterfaces 

# System dependant variables
#
OS = AIX
#
OS = HP-UX
#
OS = IRIX
#
OS = SunOS
#
OS = Windows_NT
