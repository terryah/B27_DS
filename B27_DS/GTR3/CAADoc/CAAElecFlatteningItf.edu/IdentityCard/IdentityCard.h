// COPYRIGHT DASSAULT SYSTEMES 2005

// -->Prereq Components Declaration

// Prereq de COM
AddPrereqComponent ("System"                      ,Public);
AddPrereqComponent ("ObjectModelerBase"           ,Public);
AddPrereqComponent ("ProductStructure"            ,Public);
AddPrereqComponent ("Mathematics"                 ,Public);
AddPrereqComponent ("ObjectSpecsModeler"          ,Public);

//
AddPrereqComponent ("ElecFlatteningItf"           ,Public);

// SUW 28-08-2003
AddPrereqComponent ("ElectricalInterfaces"        ,Public);

// GLY, 27:01:2007
//AddPrereqComponent ("ElecHarnessItf"              ,Public);

