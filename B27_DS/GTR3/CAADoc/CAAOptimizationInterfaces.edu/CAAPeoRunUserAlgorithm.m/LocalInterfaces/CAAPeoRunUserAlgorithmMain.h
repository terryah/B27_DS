// COPYRIGHT DASSAULT SYSTEMES  2002
#ifndef CAAPeoRunUserAlgorithmMain_H
#define CAAPeoRunUserAlgorithmMain_H

// ==============================================================================
//
// ABSTRACT
// --------
//
// A simple program which teaches you how to create your own optimization algorithm
// and how to use it in an optimization problem.
// The working document is a .CATPart file.
// Name of the executable : CAAPeoRunUserAlgorithm.exe
// ODT provided in CAAOptimizationInterfaces.tst :  CAAPeoRunUserAlgorithm.sh

// The progam takes 1 optional argument :
// - 1rst argument   : the path of the optimization log file
//					   (ex : E:\users\tmp\OptimizationResults.txt).
//					   It can be a text file (all OS) or an Excel file (Windows only)
//					   If you give it, it will store the optimization historic in the
//					   model (and not directly in a file as done in CAAPeoGettingStarted)
//					   but we will export an image of the historic in a file to allow you
//					   to see what has been saved .
//					   This argument is optional because an optimization can run
//					   without keeping the historic.
//
// ==============================================================================  
#endif

