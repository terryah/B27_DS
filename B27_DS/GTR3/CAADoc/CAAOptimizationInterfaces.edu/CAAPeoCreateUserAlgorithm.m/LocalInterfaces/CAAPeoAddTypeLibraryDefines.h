// COPYRIGHT DASSAULT SYSTEMES  2002
//---------------------------------------------------------
//
// This file contains all the defines relative to the
// CAAPeoUserAlgo algorithm type.
//
//---------------------------------------------------------


#ifndef CAAPeoAddTypeLibraryDefines_H
#define CAAPeoAddTypeLibraryDefines_H

// The NLS file name
#define CAAPeoNLSFileName							"CAAPeoNLSFile"

// the internal package name
#define CAAPeoPackageName							"CAAPeoPackage"

// the name of the file that implements CATIDelegateInstanciation
#define CAAPeoDelegateInstanciationName				"CAAPeoDelegateInstanciation"

// the internal name of the user algorithm type
#define CAAPeoUserAlgorithmCATIType					"CAAPeoUserAlgorithm"


#endif
