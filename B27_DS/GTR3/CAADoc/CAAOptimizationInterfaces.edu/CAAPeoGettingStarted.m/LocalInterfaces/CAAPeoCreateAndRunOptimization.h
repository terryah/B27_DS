// COPYRIGHT DASSAULT SYSTEMES  2002
#ifndef CAACreateAndRunOptimization_H
#define CAACreateAndRunOptimization_H

// ==============================================================================
//
// ABSTRACT
// --------
//
// A simple program which teaches you how to create an optimization problem
// with a CATIA algorithm and how to run it.
// The working document is a .CATPart file.
// Name of the executable : CAAPeoGettingStarted.exe
// ODT provided in CAAOptimizationInterfaces.tst :  CAAPeoGettingStarted.sh
//
// The progam takes 2 arguments which correspond to files to save :
// - 1rst argument : the .CATPart file path (ex: E:\users\tmp\CreatingOptimizationTest.CATPart)
//				     in which all the created features will be stored.
// - 2d argument   : the path of the optimization log file
//					 (ex : E:\users\tmp\OptimizationResults.txt).
//					 It can be a text file or an Excel file (Windows only)
//
// Notice that we DO NOT CHECK the validity of the 2 arguments
// so that example may fail if you give a ReadOnly Path etc ...
// ==============================================================================  

#endif

