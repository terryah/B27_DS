// COPYRIGHT Dassault Systemes 2002
#ifdef  _WINDOWS_SOURCE
#ifdef  __CAAPeoCreateUserAlgorithmUI
#define ExportedByCAAPeoCreateUserAlgorithmUI     __declspec(dllexport)
#else
#define ExportedByCAAPeoCreateUserAlgorithmUI     __declspec(dllimport)
#endif
#else
#define ExportedByCAAPeoCreateUserAlgorithmUI
#endif
