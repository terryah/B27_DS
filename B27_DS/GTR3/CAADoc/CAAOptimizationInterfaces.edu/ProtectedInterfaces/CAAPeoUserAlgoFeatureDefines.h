// COPYRIGHT DASSAULT SYSTEMES  2002
//---------------------------------------------------------
//
// This file contains all the defines relative to the
// CAAPeoUserAlgo algorithm feature.
//
//---------------------------------------------------------

#ifndef CAAPeoUserAlgoFeatureDefines_H
#define CAAPeoUserAlgoFeatureDefines_H

// the user catalog name (located in the \CAAOptimizationInterfaces.edu\Cnext\Ressources\graphic\ directory)
#define UserCatalogName					"CAAPeoUserCatalog.CATfct"

// the algorithm start up (stored in the user catalog) name
#define UserAlgoSUName					"CAAPeoUserAlgo"

// the algorithm start up type
#define UserAlgoSUType					"CAAPeoUserAlgo"

// the "maximum number of updates" setting of the user algorithm
#define UserAlgoNbUpdatesMaxSetting		"NbUpdateMax"

// the "maximum time" setting of the user algorithm
#define UserAlgoMaxTimeSetting			"MaxTime"

#endif

