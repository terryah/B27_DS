// COPYRIGHT DASSAULT SYSTEMES 2002
//===================================================================
//
// IdentityCard.h
// Supplies the list of prerequisite components for framework CAAOptimizationInterfaces.edu
//
//===================================================================
//
// Usage notes:
//   For every prereq framework FW, use the syntax:
//   AddPrereqComponent ("FW", Public);
//
//===================================================================
//
//  May 2002  Creation: Code generated by the CAA wizard  SOC
//===================================================================

   AddPrereqComponent("System",             Public);
   AddPrereqComponent("ObjectModelerBase",  Public);
   AddPrereqComponent("ObjectSpecsModeler", Public);
   AddPrereqComponent("Dialog",             Public);
   AddPrereqComponent("KnowledgeInterfaces",Public);
   AddPrereqComponent("OptimizationInterfaces", Public);
   AddPrereqComponent("MecModInterfaces",Public);
