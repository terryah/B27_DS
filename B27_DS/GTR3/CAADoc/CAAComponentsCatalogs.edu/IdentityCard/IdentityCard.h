// COPYRIGHT DASSAULT SYSTEMES 1999
// -->Prereq Components Declaration
AddPrereqComponent("ComponentsCatalogsInterfaces",Public);
AddPrereqComponent("System",Public);
AddPrereqComponent("ObjectModelerBase",Public);
AddPrereqComponent("ObjectSpecsModeler",Public);
AddPrereqComponent("MecModInterfaces",Public);
AddPrereqComponent("KnowledgeInterfaces",Public);
