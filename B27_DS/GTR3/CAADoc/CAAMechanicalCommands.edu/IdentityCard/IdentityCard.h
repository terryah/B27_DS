// COPYRIGHT DASSAULT SYSTEMES 1999
// -->Prereq Components Declaration
AddPrereqComponent("System",Public);
AddPrereqComponent("ObjectModelerBase",Public);
AddPrereqComponent("ObjectSpecsModeler",Public);
AddPrereqComponent("Visualization",Public);
AddPrereqComponent("VisualizationBase",Public);
AddPrereqComponent("Mathematics",Public);
AddPrereqComponent("MecModInterfaces",Public);
AddPrereqComponent("GSMInterfaces",Public);
AddPrereqComponent("MechanicalModeler",Public);
AddPrereqComponent("MechanicalModelerUI",Public);
AddPrereqComponent("MechanicalCommands",Public);
AddPrereqComponent("LiteralFeatures",Public);
AddPrereqComponent("KnowledgeInterfaces",Public);
AddPrereqComponent("ApplicationFrame",Public);
AddPrereqComponent("DialogEngine",Public);
AddPrereqComponent("Dialog",Public);
AddPrereqComponent("InteractiveInterfaces",Public);
AddPrereqComponent("ComponentsCatalogsInterfaces",Public);
AddPrereqComponent("ConstraintModelerUI",Public);
AddPrereqComponent("ConstraintModeler",Public);
AddPrereqComponent("ConstraintModelerInterfaces",Public);
AddPrereqComponent("GeometricObjects",Public);

