# COPYRIGHT DASSAULT SYSTEMES 2001
#======================================================================
#
BUILT_OBJECT_TYPE=LOAD MODULE 

LINK_WITH=  JS0GROUP JS0FM \
            CATObjectModelerBase \
            CATUdfInterfaces \
            CATObjectSpecsModeler  \
            CATMecModInterfaces \
            CATMechanicalModeler \
            CATGeometricObjects \
            CATCGMGeoMath \
            CATConstraintModeler CATConstraintModelerItf \
            CATMechanicalCommands \
            CATInteractiveInterfaces \
            CAAMcaUtilities
