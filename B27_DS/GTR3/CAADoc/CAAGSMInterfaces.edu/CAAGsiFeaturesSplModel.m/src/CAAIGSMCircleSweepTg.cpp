// COPYRIGHT Dassault Systemes 2006
#include "CAAIGSMCircleSweepTg.h"

#ifndef LOCAL_DEFINITION_FOR_IID
IID IID_CAAIGSMCircleSweepTg = { 0x98589da6, 0x1592, 0x48a4, { 0xb8, 0x25, 0x53, 0x15, 0x98, 0xc8, 0xd0, 0xb0} };
#endif

CATImplementInterface(CAAIGSMCircleSweepTg, CATBaseUnknown);
CATImplementHandler(CAAIGSMCircleSweepTg, CATBaseUnknown );

