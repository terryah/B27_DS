// COPYRIGHT Dassault Systemes 2006
#include "CAAIGSMFeaturesSplFactory.h"

#ifndef LOCAL_DEFINITION_FOR_IID
IID IID_CAAIGSMFeaturesSplFactory = { 0x900614ea, 0x7f58, 0x4c91, { 0xb8, 0x5d, 0xcc, 0x30, 0xfd, 0x7d, 0x09, 0xb3} };
#endif

CATImplementInterface(CAAIGSMFeaturesSplFactory, CATBaseUnknown);
CATImplementHandler(CAAIGSMFeaturesSplFactory, CATBaseUnknown );
