// COPYRIGHT Dassault Systemes 2006
#include "CAAIGSMSewSkinBasic.h"

#ifndef LOCAL_DEFINITION_FOR_IID
IID IID_CAAIGSMSewSkinBasic = { 0xe994890d, 0xd2fe, 0x42ef, { 0xbf, 0xa5, 0x16, 0xa6, 0xf7, 0xb3, 0x28, 0x66} };
#endif

CATImplementInterface(CAAIGSMSewSkinBasic, CATBaseUnknown);
CATImplementHandler(CAAIGSMSewSkinBasic, CATBaseUnknown );
