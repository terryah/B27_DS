// -------------------------------------------------------------------
// Copyright Dassault Systemes 2006
//---------------------------------------------------------------------
// Shade Design And Styling 
//---------------------------------------------------------------------
// Responsable: BIW
//---------------------------------------------------------------------
// CAAGSMFrmSelectorCtx
//---------------------------------------------------------------------
// 
//  Dialog widget integrating  GSD contextual menu  activation /Notification 
//
//---------------------------------------------------------------------
// Historique:
// Creation     : 06/06/01 > PEY 
//---------------------------------------------------------------------
#include "CAAGSMFrmSelectorCtxNotifications.h"
//------------------------------------------------------------
CATImplementClass(CAAGSMFrmSelectorCtxNotifClear,Implementation,CATNotification,CATNull);

CAAGSMFrmSelectorCtxNotifClear::CAAGSMFrmSelectorCtxNotifClear()
{}
CAAGSMFrmSelectorCtxNotifClear::~CAAGSMFrmSelectorCtxNotifClear()
{}


