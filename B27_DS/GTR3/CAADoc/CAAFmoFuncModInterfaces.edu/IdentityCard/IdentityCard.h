// Copyright � 2007 Dassault Syst�mes.
// All right reserved

//
// -->Prereq Components Declaration

// Ajout dve/loc  13/03/2007
AddPrereqComponent ("System",                  Public);
AddPrereqComponent ("ObjectModelerBase",       Public);
AddPrereqComponent ("MecModInterfaces",        Public);
AddPrereqComponent ("ObjectSpecsModeler",      Public);
AddPrereqComponent ("CATFmoFuncModInterfaces", Public);
AddPrereqComponent ("VisualizationBase",       Public);
AddPrereqComponent ("Visualization",           Public);
AddPrereqComponent ("InteractiveInterfaces",   Public);
