// COPYRIGHT DASSAULT SYSTEMES 1999

// Local Framework
#include "CAADlgModifNotification.h"

// To declare that the class is a component main class  
CATImplementClass(CAADlgModifNotification,Implementation,CATBaseUnknown,CATNull);

CAADlgModifNotification::CAADlgModifNotification()
{
}

CAADlgModifNotification::~CAADlgModifNotification()
{
}


