//=============================================================================
//
// COPYRIGHT DASSAULT SYSTEMES  2000
//
// Sample code for : Geometric Modeler
// Mission         : Use of the MProc system.

#include <stdio.h>
#include "CAAGobMProcImpl.h"

//------------------------------------------------------------------------------
int main( int argc, char** argv)
{
	return CustomGobMProcExample( argc, argv);
}
