// COPYRIGHT DASSAULT SYSTEMES 1999

// -->Prereq Components Declaration
   AddPrereqComponent("System",Public);
   AddPrereqComponent("Mathematics",Public);
   AddPrereqComponent("Dialog",Public);
   AddPrereqComponent("Visualization",Public);
   AddPrereqComponent("VisualizationBase",Public);
   AddPrereqComponent("Print",Public);
   AddPrereqComponent("PrintBase",Public);
