// COPYRIGHT DASSAULT SYSTEMES  2003
//=============================================================================
//
// CAAArrAttachments - This sample illustrates how to use the following
// interfaces CATIArrAttachmentFactory, CATIArrIgpAttachment, CATIArrAttachSubscriber
//
//
#include <iostream.h>

// This framework
#include "CAAArrAttachments.h"

//CATArrangementInterfaces
#include "CATIArrAttachmentFactory.h"
#include "CATIArrIgpAttachment.h"
#include "CATIArrAttachSubscriber.h"

// ObjectModelerBase
#include "CATDocument.h"
#include "CATDocumentServices.h"
#include "CATIContainer.h"
#include "LifeCycleObject.h"
#include "CATIDocRoots.h"
#include "CATAppliContServices.h"

// ObjectSpecsModeler
//#include "CATCatalogFactoryServices.h"
//#include "CATICatalog.h"

// ObjectSpecsModeler
#include "CATISpecObject.h"

// ProductStructure
#include "CATIProduct.h"
#include "CATInit.h"
#include "CATIContainer.h"

// System
#include "CATBaseUnknown.h"
#include "CATICStringList.h"
#include "CATBooleanDef.h"
#include "CATString.h"

// Visualization
#include "CATIModelEvents.h"
#include "CATCreate.h"

//=============================================================================
//  Constructor
//=============================================================================
CAAArrAttachments::CAAArrAttachments() 
{
  cout << "CAAArrAttachments::CAAArrAttachments()" << endl;
}

//=============================================================================
//  Destructor
//=============================================================================
CAAArrAttachments::~CAAArrAttachments()
{
  cout << "CAAArrAttachments::~CAAArrAttachments()" << endl;
}

//=============================================================================
//  DoSample for Piping domain and application
//=============================================================================
int CAAArrAttachments::DoSample(const CATUnicodeString &iFileToBeLoaded)
{
  cout <<"============================================================"<< endl;
  cout <<"===       CAAArrAttachments::DoSample Start              ==="<< endl;
  cout <<"============================================================"<< endl;

  HRESULT rc;

  // Interface pointer variables used below in the try section.
  CATIArrAttachmentFactory      * piFactory             = NULL;
  CATDocument                   * pDoc                  = NULL;
  CATIDocRoots                  * piDocRootsOnDoc       = NULL;
  CATListValCATBaseUnknown_var  * lstPrd                = NULL;
  CATListValCATBaseUnknown_var  * pRootProducts         = NULL;
  CATISpecObject                * piS1                  = NULL;
  CATISpecObject                * piS2                  = NULL;
  //CATObject                     * piObj                 = NULL;
  CATIProduct                   * piParent              = NULL;
  CATIProduct                   * piChild               = NULL;
  CATIArrIgpAttachment          * oAttach               = NULL;
  CATIProduct                   * oParent               = NULL;
  CATIProduct                   * oChild                = NULL;
  CATIArrAttachSubscriber       * piArrAttachSubscriber = NULL;
  LifeCycleObject               * piLifecycleObject     = NULL;
  
  CATISpecObject_var  spParent;
  CATISpecObject_var  spChild;
  CATIProduct_var     spRootPrd;
  CATBaseUnknown_var  spSpec1;
  CATBaseUnknown_var  spSpec2; 

  CATUnicodeString uNameP, uNamePP;
  CATUnicodeString uNameC, uNameCC;

  CATBoolean oStatus, oStatus02, oStatus03;

  CATTry 
  {
    // Load input document
    CAAMfgBaseEnv::CreateCATProductEnv (iFileToBeLoaded);
    
    // It is required for all batch programs accessing Routable/Arrangement
    // objects to call ApplicationInit() so that CMG geometries are saved 
    CAAMfgBaseEnv::ApplicationInit();
    
    // Retrieve the handle to the Document
    pDoc = CAAMfgBaseEnv::GetDocument();

    if(pDoc)
    {
      // Obtain the Root of the Document
      if(SUCCEEDED( pDoc->QueryInterface(IID_CATIDocRoots, (void**) &piDocRootsOnDoc) ))
      {
        pRootProducts = piDocRootsOnDoc->GiveDocRoots();
        if( pRootProducts && pRootProducts -> Size()> 0)
        {
	        spRootPrd = (*pRootProducts)[1];
          
          delete pRootProducts; 
          pRootProducts = NULL; 
        }
        piDocRootsOnDoc -> Release(); piDocRootsOnDoc = NULL;
      }

      if(!! spRootPrd)
      {
        // Retrieve the Children of the Document
        lstPrd = spRootPrd->GetChildren();
        if ( lstPrd && (lstPrd->Size() >= 2) )
        {
          spSpec1 = (*lstPrd)[1];
          if ( NULL_var != spSpec1 )
          {
            if(SUCCEEDED( spSpec1->QueryInterface(IID_CATISpecObject,(void**)&piS1) )) 
            {
              CATUnicodeString uSpec1Name = piS1->GetName();
              cout << "\n Root Product :: Child No 1 is " << uSpec1Name.ConvertToChar() << endl;
              spParent = piS1;

              piS1->Release(); piS1 = NULL;
            }
            else
            {
              cout << "\n Parent Child of Root could not be obtained "<< endl;
              return 023;
            }
          }
          
          spSpec2 = (*lstPrd)[2];
          if ( NULL_var != spSpec2 )
          {
            if(SUCCEEDED( spSpec2->QueryInterface(IID_CATISpecObject,(void**)&piS2) )) 
            {
              CATUnicodeString uSpec2Name = piS2->GetName(); 
              cout << "\n Root Product :: Child No 2 is " << uSpec2Name.ConvertToChar() << endl;
              spChild = piS2;

              piS2->Release(); piS2 = NULL;
            }
            else
            {
              cout << "\n Child Child of Root could not be obtained "<< endl;
              return 024;
            }
          }
          delete lstPrd; 
          lstPrd = NULL; 
        }
        else
        {
          cout << "\n Children of the RootProduct could not be obtained " << endl;
          return 022;
        }

				// Get handle on the Attachment applicative container
				CATIContainer_var spApplicativeCont;
				CATUnicodeString userID("ArrExtensionCont");
			  CATBaseUnknown * oBaseUnkApplicativeContainer = NULL;
				CATBaseUnknown_var spBunkAppliCont;

				// first check if the Attachment applicative container exists, else create the container.
				if(FAILED(CATGetApplicativeContainer(&oBaseUnkApplicativeContainer, pDoc, IID_CATIContainer, userID)))
				{
					// applicative container does not exist, so create one
					cout << "\n spApplicativeCont soes not exist, so create one"<<endl;

					CATBaseUnknown *piBUCont = NULL;
					CATIdent        IdentContType( userID );
					CATIdent        IdentSuprType( "CATFeatCont" );

					if( FAILED( CATCreateApplicativeContainer( &piBUCont,
												pDoc, IdentContType, IID_CATIContainer,
												IdentSuprType, userID ) ) ) 
					{
						cout << "\n call to CATCreateApplicativeContainer is KO"<<endl;
						return 999;
					}
					else
					{
					  cout << "\n call to CATCreateApplicativeContainer is OK"<<endl;
						spBunkAppliCont = piBUCont;
						if(piBUCont) { piBUCont->Release(); piBUCont = NULL; }

						spApplicativeCont = spBunkAppliCont;
					}
				}
				else
				{
				  spBunkAppliCont = oBaseUnkApplicativeContainer;
				  if(oBaseUnkApplicativeContainer) { oBaseUnkApplicativeContainer->Release(); oBaseUnkApplicativeContainer = NULL; }
					spApplicativeCont = spBunkAppliCont;
				}

				// if call to CATGetApplicativeContainer returns a valid container, 
				// the initialization of the container need not be done. 
				// Also note that the Catalog need not be loaded in such a case.
				// However invoking the Init and catalog load will ensure that 
				// the applicative container is well set from new attachment creation perspective
				
				// if call to CATGetApplicativeContainer does not return a container, 
				// call to CATCreateApplicativeContainer is made
				// inthis case, we need to load the catalog and also create the initialization of the container
				
				if(!! spApplicativeCont)
				{
					// load the Attachment feat catalog in the Applicative Container (OLD WAY)
					/*CATICatalogManager_var Manager = spApplicativeCont; 
				  if(!! Manager) 
					{
						CATIContainer * su = (CATIContainer *)Manager->OpenCatalog("CATArrAttachResource.feat",CATIContainer::ClassName()); 
				    if ( su ) su->Release(); 
					}*/

				  // load the Attachment feat catalog in the Applicative Container (NEW WAY)
				  /*const CATUnicodeString iCatalogStorageName = "CATArrAttachResource.feat";
				  const CATUnicodeString iCatalogClientId = "SAMPLES";

				  CATICatalog		* pICatalog = NULL;

				  HRESULT RC = ::AccessCatalog(&iCatalogStorageName, &iCatalogClientId,
					  spApplicativeCont,
					  &pICatalog);
					
				  if (SUCCEEDED(RC) && pICatalog) 
					  cout << "\n call to CATCatalogFactoryServices : AccessCatalog OK " << endl << flush;
				  else
				  {
					  cout << "\n CATCatalogFactoryServices : AccessCatalog Failed " << endl << flush;
					  return 050;
				  }	

				  if(pICatalog)
				  {
					  pICatalog->Release();
					  pICatalog = NULL;
				  }*/


					// Invoke Initialization of the applicative container
					CATInit_var spInit(spApplicativeCont);
					if(!! spInit) spInit->Init(FALSE);

					CATIContainer* piContainer = NULL;
					if( SUCCEEDED( spApplicativeCont -> QueryInterface( IID_CATIContainer, (void **)&piContainer ) ) )
					{
						pDoc -> AddTransactionsOnNewContainers( piContainer );
						if(piContainer) piContainer->Release(); piContainer=NULL;
					}

					// Obtain handle on the compound container and declare the applicativecontainer as created under 
					// this compound container
					CATIContainer_var spICompCont = pDoc->GetCompoundContainer();
					CATIModelEvents_var spModelEvents = spICompCont;
					if(!! spModelEvents)
					{
					  spModelEvents->ConnectTo(spApplicativeCont);
					  CATCreate createNotif((CATBaseUnknown*) spApplicativeCont,(CATBaseUnknown*) spICompCont);
					  spModelEvents->Dispatch(createNotif);
					}

        //piObj = new CATObject("ArrExtensionCont");
        //if(piObj)
        //{ 
          //  Get handle to CATIArrAttachmentFactory interface from the ArrExtensionCont object
          if(SUCCEEDED(spApplicativeCont -> QueryInterface(IID_CATIArrAttachmentFactory,(void **)&piFactory)))
          {
            if(!! spParent && !! spChild)
            {
              spParent -> QueryInterface(IID_CATIProduct,(void **)&piParent);
              spChild -> QueryInterface(IID_CATIProduct,(void **)&piChild);

              if(piParent && piChild)
              {
                // Create the Attachment
                piFactory -> Attach (piParent, piChild, &oAttach);

                piParent -> Release(); piParent = NULL;
                piChild -> Release();  piChild  = NULL;
              }

              // oAttach is a handle to the interface CATIArrIgpAttachment
              if(oAttach)
              {
                rc = S_OK;
                // retrieve parent object
                if(SUCCEEDED(oAttach -> GetParent(& oParent))) 
                {
                  oParent->GetPrdInstanceName(uNameP);
                  cout << "\n Parent Name from Attachment = " << uNameP.ConvertToChar() << endl;
                  
                  oParent -> Release(); oParent = NULL;
                }
                else
                {
                  cout << "\n CATIArrIgpAttachment : GetParent Failed"<<endl;
                  return 28;
                }
                // retrieve child object
                if(SUCCEEDED(oAttach -> GetChild(& oChild)))
                {
                  oChild->GetPrdInstanceName(uNameC);
                  cout << "\n Child Name from Attachment = " << uNameC.ConvertToChar() << endl;
                  
                  oChild -> Release(); oChild = NULL;
                }
                else
                {
                  cout << "\n CATIArrIgpAttachment : GetChild Failed"<<endl;
                  return 29;
                }

                // get a handle to CATIArrAttachSubscriber from the Attachment
                if(SUCCEEDED( oAttach -> QueryInterface(IID_CATIArrAttachSubscriber,(void**)& piArrAttachSubscriber) ))
                {
                  // check if the objects within the attachment are connected
                  piArrAttachSubscriber -> IsConnected(&oStatus);
                  if(oStatus)
                  {
                    cout << "\n CATIArrIgpAttachment is connected"<<endl;
                    
                    // Dis-connect the objects within the attachment
                    if(SUCCEEDED( piArrAttachSubscriber -> RemoveConnection() ))
                    {
                      cout << "\n The Attachment connection has been removed"<<endl;

                      if(SUCCEEDED(oAttach -> GetParent(& oParent))) 
                      {
                        oParent->GetPrdInstanceName(uNamePP);
                        cout << "\n Parent Name from Attachment after call to RemoveConnection = " << uNamePP.ConvertToChar() << endl;
                  
                        oParent -> Release(); oParent = NULL;
                      }
                      else
                      {
                        cout << "\n CATIArrIgpAttachment : GetParent Failed after call to RemoveConnection "<<endl;
                        return 30;
                      }

                      if(SUCCEEDED(oAttach -> GetChild(& oChild)))
                      {
                        oChild->GetPrdInstanceName(uNameCC);
                        cout << "\n Child Name from Attachment after call to RemoveConnection = " << uNameCC.ConvertToChar() << endl;
                  
                        oChild -> Release(); oChild = NULL;
                      }
                      else
                      {
                        cout << "\n CATIArrIgpAttachment : GetChild Failed after call to RemoveConnection "<<endl;
                        return 31;
                      }
                    }
                    else
                      cout << "\n The connection cannot be removed"<<endl;
                  }
                  else
                    cout << "\n CATIArrIgpAttachment is NOT connected" << endl;

                  piArrAttachSubscriber -> IsConnected(&oStatus02);
                  if(oStatus02)
                    cout << "\n CATIArrIgpAttachment is still connected after call to RemoveConnection "<<endl;
                  else
                    cout << "\n CATIArrIgpAttachment is no longer connected after call to RemoveConnection "<<endl;

                  // Dis-connect the objects within the attachment
                  if(SUCCEEDED( piArrAttachSubscriber -> DoConnection() ))
                  {
                    piArrAttachSubscriber -> IsConnected(&oStatus03);
                    if(oStatus03)
                      cout << "\n CATIArrIgpAttachment Status is (connected) after call to DoConnection "<<endl;
                    else
                      cout << "\n CATIArrIgpAttachment Status is (not connected) after call to DoConnection "<<endl;
                  }
                  else
                  {
                    cout << "\n The connection between the attachment objects could not be made again"<<endl;
                    return 32;
                  }

                  // Get a handle to LifeCycleObject Interface from the attachment
                  if(SUCCEEDED( piArrAttachSubscriber -> QueryInterface(IID_LifeCycleObject,(void**)&piLifecycleObject) ))
                  {
                    piLifecycleObject -> remove();
                    //cout << "\n Removal of the Attachment succeeded"<<endl;

                    piLifecycleObject->Release(); piLifecycleObject = NULL;
                  }
                  else
                  {
                    //cout << "\n Removal of the Attachment failed"<<endl;
                    return 33;
                  }

                  piArrAttachSubscriber->Release(); piArrAttachSubscriber = NULL;

				  //Remove attachment using the new factory method
				  HRESULT RC = piFactory->Remove(oAttach);
				  if(SUCCEEDED(RC))
					  cout << "\n Removal of the Attachment succeeded"<<endl;
				  else
				  {
					  cout << "\n Removal of the Attachment failed"<<endl;
					  return 51;
				  }
                }
                oAttach -> Release(); oAttach = NULL;
              }
              else
              {
                cout << "\n Attachment Could not be obtained"<<endl;
                return 027;
              }
            }
            piFactory -> Release(); piFactory = NULL;
          }
          else
          {
            cout << "\n CATIArrAttachmentFactory could not be obtained" << endl;
            return 026;
          }
          //piObj->Release(); piObj = NULL;
        }
        else
        {
          cout << "\n Failed to get handle on Applicative container (ArrExtensionCont)"<<endl;
          return 025;
        }
    
        //CATUnicodeString StorageNameDocument ;
				//StorageNameDocument = pDoc->StorageName();
				//cout << "\n StorageNameDocument = " << StorageNameDocument << endl;
				//HRESULT RRC = CATDocumentServices::SaveAs(*pDoc,StorageNameDocument);
      }
      else
      {
        cout << "\n Root Product of Document could not be obtained "<<endl;
        return 021;
      }
    }
    else
    {
      cout << "\n Pointer to current Document is KO "<<endl;
      return 020;
    }
    
  } // end CATTry

  CATCatch (CATError, error)
  {
    cout << "CAAArrAttachments::DoSample *** CATRethrow" << endl;
    CATRethrow;
  } 
  CATEndTry;

  if(lstPrd         ) { delete                 lstPrd;  lstPrd          = NULL; }
  if(pRootProducts  ) { delete          pRootProducts;  pRootProducts   = NULL; }
  if(piFactory      ) { piFactory       ->  Release();  piFactory       = NULL; }
  if(piS1           ) { piS1            ->  Release();  piS1            = NULL; }
  if(piS2           ) { piS2            ->  Release();  piS2            = NULL; }
  //if(piObj          ) { piObj           ->  Release();  piObj           = NULL; }
  if(piParent       ) { piParent        ->  Release();  piParent        = NULL; }
  if(piChild        ) { piChild         ->  Release();  piChild         = NULL; }
  if(oAttach        ) { oAttach         ->  Release();  oAttach         = NULL; }
  if(oParent        ) { oParent         ->  Release();  oParent         = NULL; }
  if(oChild         ) { oChild          ->  Release();  oChild          = NULL; }
  if(piDocRootsOnDoc) { piDocRootsOnDoc ->  Release();  piDocRootsOnDoc = NULL; }
  
  if(piArrAttachSubscriber)
  {  piArrAttachSubscriber -> Release(); piArrAttachSubscriber = NULL; }
  
  if(piLifecycleObject)
  {  piLifecycleObject->Release(); piLifecycleObject = NULL; }

  cout <<"============================================================"<< endl;
  cout <<"===       CAAArrAttachments::DoSample End                ==="<< endl;
  cout <<"============================================================"<< endl;

  return rc;
}
