// COPYRIGHT DASSAULT SYSTEMES 2003
//=============================================================================
//
// CAAArrAttachmentsMain
//
//  This sample illustrates how to create and read an arrangement attachment.
//
//  This sample will open an input document containing Arrangement resources,
//
//  Prerequisite: 
//  -------------------
//  This sample uses the input drawing CAAArrEduIn.CATProduct.
//
//  Running the program:
//  -------------------
//  To run this program, you can use the command:
//  
//        mkrun -c "CAAArrAttachments CAAArrEduIn.CATProduct"
//  
//  where "CAAArrEduIn.CATProduct" is the full path name of the input drawing.
//
//=============================================================================
//
#include <iostream.h>
#include <string.h>

// This framework
#include "CAAArrAttachments.h"

// System
#include "CATErrorMacros.h"
#include "CATUnicodeString.h"

//=============================================================================
//   Main
//=============================================================================
int main (int argc, char **argv)
{
  cout << "Start main CAAArrAttachments" << endl;

  CATUnicodeString FileToBeLoaded;

  cout<<"argc = "<<argc<<endl;
  if (argc > 1)
  {
    FileToBeLoaded = argv[1];
  }

  int rc = 0;

  CATTry 
  {
    if (!FileToBeLoaded.GetLengthInChar())
    {
       cout << "**** must input the file name of " << endl;
       cout << "a CATProduct with objects to attach " << endl;
    }
    CAAArrAttachments myObject;
    rc = myObject.DoSample(FileToBeLoaded);
  }

  CATCatch (CATError, error)
  {
    cout << "error in main " << endl;
    cout << "error message : "
         << (error->GetNLSMessage()).ConvertToChar()
         << endl;
    delete error;
    rc = 999;
  } 
  CATEndTry;

  cout << "End main CAAArrAttachments" << endl;
  return rc;
}
