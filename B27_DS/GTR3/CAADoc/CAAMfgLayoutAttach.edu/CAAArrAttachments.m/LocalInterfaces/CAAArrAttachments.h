// COPYRIGHT DASSAULT SYSTEMES  2003
//=============================================================================
//
// CAAArrAttachments
//
#ifndef CAAArrAttachments_H
#define CAAArrAttachments_H

#include "CAAMfgBaseEnv.h"

class CAAArrAttachments : public CAAMfgBaseEnv
                                    
{
  public:
    //-------------------------------------------------------------------------
    // Constructor/Destructor
    //-------------------------------------------------------------------------
    CAAArrAttachments ();
    virtual ~CAAArrAttachments();

    //-------------------------------------------------------------------------
    // Input: iFileToBeLoaded - path of document name to be loaded (CATProduct
    //        containing geometry, objects ...
    //        iPathToOutputFile - path to save the file in 
    //-------------------------------------------------------------------------
    int DoSample(const CATUnicodeString &iFileToBeLoaded);

};
#endif
