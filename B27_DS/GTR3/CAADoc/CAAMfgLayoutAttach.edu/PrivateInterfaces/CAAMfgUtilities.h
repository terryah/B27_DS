// COPYRIGHT DASSAULT SYSTEMES 2003

#ifdef _WINDOWS_SOURCE
#ifdef __CAAMfgUtilities
#define ExportedByCAAMfgUtilities __declspec(dllexport)
#else
#define ExportedByCAAMfgUtilities __declspec(dllimport)
#endif
#else
#define ExportedByCAAMfgUtilities
#endif
