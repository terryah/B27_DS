// COPYRIGHT DASSAULT SYSTEMES  2003
//=============================================================================
//

#ifndef CAAMfgBaseEnv_H
#define CAAMfgBaseEnv_H

#include "CAAMfgUtilities.h"
#include "CATIContainer.h"
#include "CATErrorDef.h"
#include "CATUnicodeString.h"

class CATDocument;
class CATSession;
class CATIPspPhysical;
class CATIPspLogicalLine;

//-----------------------------------------------------------------------------

class ExportedByCAAMfgUtilities CAAMfgBaseEnv
                                    
{
  public:
    //-------------------------------------------------------------------------
    // Constructor/Destructor
    //-------------------------------------------------------------------------
    CAAMfgBaseEnv ();
    virtual ~CAAMfgBaseEnv();

    //-------------------------------------------------------------------------
    // Get data
    //-------------------------------------------------------------------------
    CATDocument * GetDocument ();
    CATIContainer * GetRootContainer();

    //-------------------------------------------------------------------------
    // Initialize application
    //-------------------------------------------------------------------------
    virtual void ApplicationInit ();

    //-------------------------------------------------------------------------
    // Save document
    //-------------------------------------------------------------------------
    virtual void SaveDocument (const CATUnicodeString &iFileName);

    //-------------------------------------------------------------------------
    // Create environment
    //-------------------------------------------------------------------------
    virtual void CreateCATProductEnv (const CATUnicodeString &iFileNameToBeLoaded);

    //-------------------------------------------------------------------------
    // Cleanup
    //-------------------------------------------------------------------------
    virtual void CleanupSession ();

  private:

    CATDocument       *_pDocument;
    CATIContainer     *_piRootCont;
    CATSession        *_pSession;
    char              *_pSessionIdent;

};
#endif
