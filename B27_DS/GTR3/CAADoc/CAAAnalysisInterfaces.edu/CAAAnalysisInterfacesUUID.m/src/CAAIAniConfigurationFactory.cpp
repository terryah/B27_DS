// COPYRIGHT DASSAULT SYSTEMES 2000
//===================================================================
#include "IUnknown.h"
extern "C" const IID IID_CAAIAniConfigurationFactory  = { 
  /* c6998a90-8b5c-11d5-8518-00d0b7addac3 */
    0xc6998a90,
    0x8b5c,
    0x11d5,
    {0x85, 0x18, 0x00, 0xd0, 0xb7, 0xad, 0xda, 0xc3}
  };


