// COPYRIGHT DASSAULT SYSTEMES 2000
//===================================================================
#include "CAAAniCfg.h"
#include "CAAAniConfigurationFactory.h"
#include "TIE_CAAIAniConfigurationFactory.h"

CATImplementConfigurationFactory(CAAAniCfg, CAAIAniConfigurationFactory);
