// COPYRIGHT DASSAULT SYSTEMES 2000
//===================================================================
#ifndef CAAAniConfigurationFactory_h
#define CAAAniConfigurationFactory_h

#include "CATWorkshopConfigurationFactory.h"

CATDeclareConfigurationFactory(CAAAniCfg);

#endif
