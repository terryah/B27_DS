// COPYRIGHT DASSAULT SYSTEMES 2000
//===================================================================
#ifndef CAAIAniConfigurationFactory_h
#define CAAIAniConfigurationFactory_h

#include "CATIGenericFactory.h"

extern IID IID_CAAIAniConfigurationFactory;

class CAAIAniConfigurationFactory : public CATIGenericFactory
{
  CATDeclareInterface;
public:
};


#endif
