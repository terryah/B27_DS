#ifndef CAAAniTypeLib_tplib
#define CAAAniTypeLib_tplib

// Typelib LIBID declaration
#pragma REPID CAAAniTypeLib "DCE:E0F4872C-C021-420b-BC67E337F222CFE2"
#pragma REPBEGIN CAAAniTypeLib

// Prerequisite typelibs
//#pragma REPREQ InfTypeLib

// Data types included in the typelib
#include "CAAIAniExport.idl"

#pragma REPEND CAAAniTypeLib

#endif // CAAAniTypeLib_tplib




