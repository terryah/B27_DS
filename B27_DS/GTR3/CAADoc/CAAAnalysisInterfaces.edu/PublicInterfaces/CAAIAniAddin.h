// COPYRIGHT DASSAULT SYSTEMES 2000
//===================================================================

#ifndef CAAIAniAddin_h
#define CAAIAniAddin_h

#include "CATIWorkbenchAddin.h"
#include "CAAAniWB.h"

extern IID ExportedByCAAAniWB IID_CAAIAniAddin;

class ExportedByCAAAniWB CAAIAniAddin : public CATIWorkbenchAddin
{
	CATDeclareInterface;

	public:

};

#endif
