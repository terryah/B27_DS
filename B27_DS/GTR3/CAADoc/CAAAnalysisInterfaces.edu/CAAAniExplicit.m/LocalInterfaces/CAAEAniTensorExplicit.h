// COPYRIGHT Dassault Systemes 2002
//===================================================================
//
// CAAEAniTensorExplicit.h
// Header definition of CAAEAniTensorExplicit
//
//===================================================================
//
// Usage notes:
//
//===================================================================
//
//  Dec 2002  Creation: Code generated by the CAA wizard  
//===================================================================
#ifndef CAAEAniTensorExplicit_H
#define CAAEAniTensorExplicit_H

#include "CATBaseUnknown.h"

class CATISpecObject;
class CATISamAnalysisModel;
class CATAnalysisExplicitListUsr;

class CAAEAniTensorExplicit: public CATBaseUnknown
{
  CATDeclareClass;

  // Standard constructors and destructors for an implementation class
  // ----------------------------------------------------------------
     CAAEAniTensorExplicit ();
     virtual ~CAAEAniTensorExplicit ();

 /**
  * Implements a function Explicit from an interface.
  * @see CATAnalysisModeler.CATISamExplicitation#TranslateToFieldModel
  */
		HRESULT TranslateToFieldModel(CATISpecObject* iFeatToTranslate, 
									  CATISamAnalysisModel* iFEMModel, 
									  CATAnalysisExplicitListUsr& iOldExplObjects, 
									  CATAnalysisExplicitListUsr& oNewExplObjects);

  private:
  // The copy constructor and the equal operator must not be implemented
  // ----------------------------------------------------------------
  CAAEAniTensorExplicit (CAAEAniTensorExplicit &);
  CAAEAniTensorExplicit& operator=(CAAEAniTensorExplicit&);


};

//-----------------------------------------------------------------------

#endif
