// COPYRIGHT DASSAULT SYSTEMES 2000
//
#include "CAAIBiogNovel.h"

IID IID_CAAIBiogNovel = { 0xc36df0e2, 0xd28e, 0x11d4, { 0x9f, 0x14, 0x00, 0xd0, 0xb7, 0x51, 0x1d, 0xfd} };

CATImplementInterface(CAAIBiogNovel, CATBaseUnknown);
