// COPYRIGHT DASSAULT SYSTEMES 2000

AddPrereqComponent("System",                     Public);
AddPrereqComponent("Dialog",                     Public);
AddPrereqComponent("ObjectModelerBase",          Public);
AddPrereqComponent("ApplicationFrame",           Public);
AddPrereqComponent("ProductStructure",           Public);
AddPrereqComponent("SimulationBase",             Public);
AddPrereqComponent("SimulationInterfaces",       Public);
AddPrereqComponent("ObjectSpecsModeler",         Public);
