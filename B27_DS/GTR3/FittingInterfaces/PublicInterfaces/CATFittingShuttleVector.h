#ifndef CATFittingShuttleVector_H
#define CATFittingShuttleVector_H


/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */
// COPYRIGHT DASSAULT SYSTEMES 2004
//--------------------------------------------------------------------------
// CATFittingShuttleVector Enum
//--------------------------------------------------------------------------

    /**
     * The Shuttle Vector setting attribute range of values.
     * @param caaFittingShuttleVectorX
     *      Use the X axis as the shuttle vector
     * @param caaFittingShuttleVectorY
     *      Use the Y axis as the shuttle vector
     * @param caaFittingShuttleVectorZ
     *      Use the Z axis as the shuttle vector
     */

enum CATFittingShuttleVector {
    CATFittingShuttleVectorX,
    CATFittingShuttleVectorY,
    CATFittingShuttleVectorZ
};
#endif
