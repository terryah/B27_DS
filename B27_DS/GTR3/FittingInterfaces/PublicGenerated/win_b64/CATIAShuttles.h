/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAShuttles_h
#define CATIAShuttles_h

#ifndef ExportedByFittingPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __FittingPubIDL
#define ExportedByFittingPubIDL __declspec(dllexport)
#else
#define ExportedByFittingPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByFittingPubIDL
#endif
#endif

#include "CATIACollection.h"
#include "CATVariant.h"

class CATIAShuttle;

extern ExportedByFittingPubIDL IID IID_CATIAShuttles;

class ExportedByFittingPubIDL CATIAShuttles : public CATIACollection
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall Add(CATIAShuttle *& oNewShuttle)=0;

    virtual HRESULT __stdcall AddFromSel(CATIAShuttle *& oNewShuttle)=0;

    virtual HRESULT __stdcall Item(const CATVariant & iIndex, CATIAShuttle *& oShuttle)=0;

    virtual HRESULT __stdcall Remove(CATIAShuttle * iShuttle)=0;


};

CATDeclareHandler(CATIAShuttles, CATIACollection);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIABase.h"
#include "CATIAGroup.h"
#include "CATIAMove.h"
#include "CATIAPosition.h"
#include "CATIAShuttle.h"
#include "CATSafeArray.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
