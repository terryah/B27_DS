# COPYRIGHT DASSAULT SYSTEMES 2006
#======================================================================
# Imakefile for module AUTCsmUIArrays.m
#======================================================================
#
#  Jul 2006  Creation: azi
#======================================================================
#
# SHARED LIBRARY 
#
BUILT_OBJECT_TYPE=SHARED LIBRARY 
 
# DO NOT EDIT :: THE CAA2 WIZARDS WILL ADD CODE HERE
WIZARD_LINK_MODULES = JS0GROUP JS0FM  CATApplicationFrame CATObjectModelerBase DI0PANV2 \
					  CATVisualization CATViz  \
JS0GROUP 
# END WIZARD EDITION ZONE

LINK_WITH = $(WIZARD_LINK_MODULES) AUTCmnView \
								   CATView CATViewItf \
								   AUTCmnItfCPP AUTCmnDialog

# System dependant variables
#
OS = AIX
BUILD=NO
#
OS = HP-UX
BUILD=NO
#
OS = IRIX
BUILD=NO
#
OS = SunOS
BUILD=NO
#
OS = Windows_NT

#
OS = win_b64
BUILD=YES
