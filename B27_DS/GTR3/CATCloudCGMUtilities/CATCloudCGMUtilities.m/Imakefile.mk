# COPYRIGHT DASSAULT SYSTEMES 2010
#======================================================================
# Imakefile for module CATCloudCGMUtilities.m
#======================================================================
# 2010/09/14 MMO/HLN ajout de deux modules 
# 2010/06/08 Creation: Code generated by the 3DS wizard
#======================================================================
#

BUILT_OBJECT_TYPE=SHARED LIBRARY

#INSERTION ZONE NOT FOUND, MOVE AND APPEND THIS VARIABLE IN YOUR LINK STATEMENT

INCLUDED_MODULES =        \
  CATCldBuildFaceCanonic  \
  CATPolyBodyToBRep       \
  CAT3DViaBRepToDRep \
  CATCloudToBRep

LINK_WITH =  \
JS0GROUP \
CATGeometricObjects \
CATCGMGeoMath \
CATGMModelInterfaces \
CATGMOperatorsInterfaces \
CATMathematics \
CATMathStream \
CATCloudBasicResources \
PolyhedralModel \
PolyMODEL \
TessBodyAdapters \
PolyMathContainers \
CATGMAdvancedOperatorsInterfaces \
YN000FUN \ 
CATPolySegmentCommon \
CATPolyCanonicOperators \
PolyBodyBuilders

# CATTopologicalObjects \ hln14092010TEMPORAIRE

# System dependant variables
#
OS = AIX
#
OS = HP-UX
#
OS = IRIX
#
OS = SunOS
#
OS = Windows_NT

