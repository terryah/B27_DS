// COPYRIGHT DASSAULT SYSTEMES 2007
/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */
//===================================================================
//
// CATCreateCATIListOfCATIFmoFunctionalSpecification.h
//
//===================================================================
//
//  Fev 2007  Creation: Code generated by the CAA wizard  loc
//===================================================================


#ifndef CATCreateCATIListOfCATIFmoFunctionalSpecification_H  
#define CATCreateCATIListOfCATIFmoFunctionalSpecification_H  


#include "CATFmoFuncModItf.h"
#include "CATErrorDef.h"
class CATIListOfCATIFmoFunctionalSpecification;


/**
* Creates a list of CATIFmoFunctionalSpecification.
*
* @param opList [out, CATBaseUnknown#Release]
*   the interface pointer on the created list
*
* @return
*   S_OK if the list has been correctly created
*   E_INVALIDARG if "opList" is NULL
*   E_FAIL otherwise
*
*/
HRESULT ExportedByCATFmoFuncModItf CATCreateCATIListOfCATIFmoFunctionalSpecification (CATIListOfCATIFmoFunctionalSpecification **opiList);

#endif  
