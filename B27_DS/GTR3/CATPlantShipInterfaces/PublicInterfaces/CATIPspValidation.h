// COPYRIGHT Dassault Systemes 2006
/**
  * @CAA2Level L1
  * @CAA2Usage U4 CATEAPspValidation
  */
//===================================================================
//
// CATIPspValidation.h
// Define the CATIPspValidation interface
//
//===================================================================
// This interface is a base on which CATIA other validation checks
// are built. The user should not implement this interface directly.
//
//===================================================================
//
//  May 2006  Creation: Code generated by the CAA wizard  bpk
//===================================================================

#ifndef CATIPspValidation_H
#define CATIPspValidation_H

#include "CATPspValidationDeclarations.h"
#include "CATListPV.h"
#include "IUnknown.h"

class CATBaseUnknown;
class CATIPspGroup;
class CATIUnknownList;
class CATPspViolationProduct;

extern "C" const IID IID_CATIPspValidation ;

//------------------------------------------------------------------

/**
 * Interface base for all types of validation check interfaces. Should not be implemented.
 * <b>Role</b>: Base for other validation check interfaces.
 * <p><b>BOA  information</b>:  this  interface  can  be  implemented 
 *  using  the  BOA  (Basic  Object  Adapter). 
 *  To  know  more  about  the  BOA,  refer  to  the  CAA  Encyclopedia  home  page.   
 *  Click  Middleware  at  the  bottom  left,  then  click  the  Object  Modeler  tab  page. 
 *  Several  articles  deal  with  the  BOA.</p>
 */

class CATIPspValidation: public IUnknown
{
  public:

    /**
    * Get the validation checks associated with this type (e.g. Spec01, Spec02)
    * @param oChecks
    *   The list of validation checks
    * @return HRESULT
    *   S_OK if checks exist; E_FAIL no checks exist
    */
    virtual HRESULT GetValidationChecks( CATListValCATUnicodeString& oChecks ) = 0;

    /**
    * Get the validation check classification for this type (e.g. Object)
    * @param oClassification
    *   The validation check classification
    * @return HRESULT
    *   S_OK if oClassification is non-blank
    */
    virtual HRESULT GetClassification( CATUnicodeString& oClassification ) = 0;

    /**
    * Get the NLS name for the given validation check
    * @param iCheck
    *   The validation check (e.g. Spec01)
    * @param oName
    *   The validation check name (e.g. Out-of-spec )
    * @return HRESULT
    *   S_OK if oName is non-blank
    */
    virtual HRESULT GetCheckName( const CATUnicodeString& iCheck, 
                                  CATUnicodeString& oName ) = 0;

    /**
    * Get the violation object type
    * @param oType
    *   The violation object type
    * @return HRESULT
    *   S_OK if oType is non-blank
    */
    virtual HRESULT GetViolationObjectType( CATUnicodeString& oType ) = 0;

    /**
    * Is the object in violation with any of the given validation checks?
    * @param iObject
    *   The object to be checked
    * @param iChecks
    *   The list of check to be processed
    * @param oViolation
    *   The check violation, CATPspCheckViolation, information.  Delete array
    *   when done.
    * @param iErrorFile
    *   The file descriptor to the opened error file.
    * @return HRESULT
    *   S_OK iObject violated at least one check
    *   S_FALSE iObject did not violate any checks or not applicable for check
    *   E_FAIL invalid input or error found such that check can not be performed
    */
    virtual HRESULT IsObjectAViolation( const CATBaseUnknown* iObject,
                                        const CATListValCATUnicodeString& iChecks,
                                        CATListPV** oViolation,
                                        const unsigned int& iErrorFile = 0 ) = 0;

    /**
    * Is the group members in violation with any of the given validation checks?
    * @parm iGroup
    *   The group being processed
    * @param iMembers
    *   iGroup's members.
    * @param iObject
    *   The group member being processed
    * @param iChecks
    *   The list of check to be processed
    * @param oViolation 
    *   The check violation, CATPspCheckViolation, information. Delete array
    *   when done.
    * @param iErrorFile
    *   The file descriptor to the opened error file.
    * @return HRESULT
    *   S_OK iObject violated at least one check
    *   S_FALSE iObject did not violate any checks or not applicable for check
    *   E_FAIL invalid input or error found such that check can not be performed
    */
    virtual HRESULT IsMemberAViolation( const CATIPspGroup* iGroup,
                                        const CATIUnknownList* iMembers,
                                        const IUnknown* iObject,
                                        const CATListValCATUnicodeString& iChecks,
                                        CATListPV** oViolation,
                                        const unsigned int& iErrorFile = 0 ) = 0;

    /**
    * Get the NLS column titles for the given validation type. The data
    * return is specific to the validation type does NOT include the
    * column titles for the violation object.
    * @param oTitles
    *   The column titles (e.g. Part Spec   Line Spec )
    * @return HRESULT
    *   S_OK if oTitles is not empty
    */
    virtual HRESULT GetColumnTitles( CATListValCATUnicodeString& oTitles ) = 0;

    /**
    * Get the NLS column titles of the document and the object for a given validation type.
    * There is a one to one relation between oDocTitles and oObjectTitles 
    * (for each title listed in oDocTitles, there must be a title in oObjectTitles.)
    * @param oDocTitles
    *   The column titles for the documents (e.g. "Document Name", "Function Document Name")
    *   The documents listed in this column will be available for viewing when the report is shown.
    * @param oObjectTitles
    *   The column titles for the object to be reframed (e.g. "Part Instance Name")
    *   The object listed in this column will be reframed. If there is no object to be reframed, 
    *   a null should be appended to oObjectTitles.
    * @return HRESULT
    *   S_OK if oDocTitles and oObjectTitles are not empty
    */
    virtual HRESULT GetDocColumnTitles( CATListValCATUnicodeString& oDocTitles,
                                        CATListValCATUnicodeString& oObjectTitles) = 0;

    /**
    * If this object is applicable for this check, then retain this object for
    * post-processing. This object will be processed through the validation
    * check.
    * @param iObject
    *   The object to be checked
    * @param iErrorFile
    *   The file descriptor to the opened error file.
    * @return HRESULT
    *   E_FAIL invalid input or error found such that check can not be performed;
    *   otherwise S_OK
    */
    virtual HRESULT LogContextObject( const CATBaseUnknown* iObject,
                                      const unsigned int& iErrorFile=0 ) = 0;

    /**
    *@nodoc
    *
    */
    virtual HRESULT GetContextObjectViolation( const int& iIndex,
                                               CATPspViolationProduct** oProduct,
                                               CATListPV** oViolation ) = 0;

    /**
    * Get the number of violation objects (e.g. CATPspViolationProduct) that
    * will be generated as a result of this validation check.  This method
    * must be execute after the whole context has been processed through
    * LogContextObject method.
    * @param oNumber
    *   The number of violation products to be generated.
    * @return HRESULT
    *   S_OK if oNumber > 0
    */
    virtual HRESULT GetContextObjectViolationCount( int& oNumber ) = 0;

    /**
    * Determine if the validation type has integration validation checks.
    * @return HRESULT
    *   S_OK Is an integration check; else FALSE
    */
    virtual HRESULT IsAnIntegrationCheck() = 0;

    /**
    * Determine if the validation type has FROM/TO validation checks.
    * @return HRESULT
    *   S_OK Is a FROM/TO check; else FALSE
    */
    virtual HRESULT IsAFromToCheck() = 0;

  // No constructors or destructors on this pure virtual base class
  // --------------------------------------------------------------
};

//------------------------------------------------------------------

#endif
