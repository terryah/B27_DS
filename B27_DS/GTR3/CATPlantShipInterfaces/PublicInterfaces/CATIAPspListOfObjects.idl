#ifndef CATIAPspListOfObjects_IDL
#define CATIAPspListOfObjects_IDL

/*IDLREP*/
/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */


//	COPYRIGHT DASSAULT SYSTEMES 2003

#include "CATIABase.idl"

/**
 * Represents a collection of Objects.
 * <b>Role</b>: Collection of Object.
 */

interface CATIAPspListOfObjects : CATIABase
{
  /**
  * Returns the number of objects in the list.
  * <! @sample >
  * <dl>
  * <dt><b>Example:</b>
  * <dd>
  * This example retrieves in  <tt>NumberOfObjects</tt> the number of objects
  * currently gathered in <tt>MyList</tt>.
  * <pre>
  * NumberOfObjects = MyList.<font color="red">Count</font>
  * </pre>
  * </dl>
  */
#pragma PROPERTY Count
  HRESULT get_Count (out /*IDLRETVAL*/ long oNbItems);

  /**
  * Returns an object from its index in the list.
  * @param iIndex
  *   The index of the first object in the collection is 1, and
  *   the index of the last object is Count.
  * @param iInterfaceName
  *   The interface name of oObj.
  *
  * @return the retrieved object.
  *
  * @sample
  * The following example returns in the third object in the list.
  * <pre>
  * Dim MyObject As PspID
  * Dim MyList As PspListOfObjects
  * Set MyObject = PspListOfObjects.<font color="red">Item</font>(3,"CATIAPspID")
  * </pre>
  */
  HRESULT Item (in long iIndex, in CATBSTR iInterfaceName, out /*IDLRETVAL*/ CATIABase oObject);

  /**
  * Adds an object to the end of the list.
  * @param iObject
  *   The object to be added to the list.
  *
  * @sample
  * The following example appends an object to the list.
  * <pre>
  * Dim MyObject As AnyObject
  * Dim MyList As PspListOfObjects
  * MyList.<font color="red">Append</font>(MyObject)
  * </pre>
  */
  HRESULT Append (in CATIABase iObject);

  /**
  * Remove an object from the list by specifying its position in the list.
  * @param iIndex
  *   The position of the object to be removed in the list.
  *
  * @sample
  * The following example removes the second entry in the list. Please note that the
  * list index starts with 1.
  * <pre>
  * Dim MyList As PspListOfObjects
  * MyList.<font color="red">RemoveByIndex</font> (2)
  * </pre>
  */

  HRESULT RemoveByIndex (in long iIndex);

};

// Interface name : CATIAPspListOfObjects
#pragma ID CATIAPspListOfObjects "DCE:128a0542-e271-4c00-89d12277a6a9a1a3"
#pragma DUAL CATIAPspListOfObjects

// VB object name : PspListOfObjects
#pragma ID PspListOfObjects "DCE:2b587ac6-c257-4e63-9384b47c22dc80a9"
#pragma ALIAS CATIAPspListOfObjects PspListOfObjects

#endif
