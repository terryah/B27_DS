/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAPspGroupable_h
#define CATIAPspGroupable_h

#ifndef ExportedByCATPspPlantShipPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __CATPspPlantShipPubIDL
#define ExportedByCATPspPlantShipPubIDL __declspec(dllexport)
#else
#define ExportedByCATPspPlantShipPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByCATPspPlantShipPubIDL
#endif
#endif

#include "CATIABase.h"

class CATIAPspListOfObjects;

extern ExportedByCATPspPlantShipPubIDL IID IID_CATIAPspGroupable;

class ExportedByCATPspPlantShipPubIDL CATIAPspGroupable : public CATIABase
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_Groups(CATIAPspListOfObjects *& oLGroups)=0;


};

CATDeclareHandler(CATIAPspGroupable, CATIABase);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
