/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAPspAppFactory_h
#define CATIAPspAppFactory_h

#ifndef ExportedByCATPspPlantShipPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __CATPspPlantShipPubIDL
#define ExportedByCATPspPlantShipPubIDL __declspec(dllexport)
#else
#define ExportedByCATPspPlantShipPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByCATPspPlantShipPubIDL
#endif
#endif

#include "CATBSTR.h"
#include "CATIABase.h"
#include "CatPspIDLDomainID.h"

class CATIAProduct;
class CATIAPspGroup;
class CATIAPspListOfObjects;
class CATIAPspLogicalLine;

extern ExportedByCATPspPlantShipPubIDL IID IID_CATIAPspAppFactory;

class ExportedByCATPspPlantShipPubIDL CATIAPspAppFactory : public CATIABase
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall ListPhysicals(CATIAProduct * iCurrentProduct, CatPspIDLDomainID iDomainID, CATIAPspListOfObjects *& oListPhysicals)=0;

    virtual HRESULT __stdcall GetLogicalLine(CATIAProduct * iCurrentProduct, const CATBSTR & iLogicalLineID, CATIAPspLogicalLine *& oLogicalLine)=0;

    virtual HRESULT __stdcall ListLogicalLines(CATIAProduct * iCurrentProduct, CATIAPspListOfObjects *& oListLogLines)=0;

    virtual HRESULT __stdcall DeleteLogicalLine(CATIAPspLogicalLine * iLogicalLine)=0;

    virtual HRESULT __stdcall GetCompartment(CATIAProduct * iCurrentProduct, const CATBSTR & iCompartmentID, CATIAPspGroup *& oCompartment)=0;

    virtual HRESULT __stdcall ListCompartments(CATIAProduct * iCurrentProduct, CATIAPspListOfObjects *& oListCompartments)=0;

    virtual HRESULT __stdcall DeleteCompartment(CATIAPspGroup * iCompartment)=0;

    virtual HRESULT __stdcall CreateGroup(CATIAProduct * iCurrentProduct, const CATBSTR & iGroupType, const CATBSTR & iGroupID, CATIAPspGroup *& oGroup)=0;

    virtual HRESULT __stdcall ListGroups(CATIAProduct * iCurrentProduct, CATIAPspListOfObjects *& oListGroups)=0;

    virtual HRESULT __stdcall DeleteGroup(CATIAPspGroup * iGroup)=0;

    virtual HRESULT __stdcall DeletePart(CATIAProduct * iPart)=0;


};

CATDeclareHandler(CATIAPspAppFactory, CATIABase);

#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIAAnalyze.h"
#include "CATIACollection.h"
#include "CATIAMove.h"
#include "CATIAParameters.h"
#include "CATIAPosition.h"
#include "CATIAProduct.h"
#include "CATIAPublications.h"
#include "CATSafeArray.h"
#include "CATVariant.h"
#include "CatFileType.h"
#include "CatProductSource.h"
#include "CatRepType.h"
#include "CatWorkModeType.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
