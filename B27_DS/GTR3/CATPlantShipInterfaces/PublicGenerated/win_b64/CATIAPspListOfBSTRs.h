/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAPspListOfBSTRs_h
#define CATIAPspListOfBSTRs_h

#ifndef ExportedByCATPspPlantShipPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __CATPspPlantShipPubIDL
#define ExportedByCATPspPlantShipPubIDL __declspec(dllexport)
#else
#define ExportedByCATPspPlantShipPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByCATPspPlantShipPubIDL
#endif
#endif

#include "CATBSTR.h"
#include "CATIABase.h"

extern ExportedByCATPspPlantShipPubIDL IID IID_CATIAPspListOfBSTRs;

class ExportedByCATPspPlantShipPubIDL CATIAPspListOfBSTRs : public CATIABase
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_Count(CATLONG & oNbItems)=0;

    virtual HRESULT __stdcall Item(CATLONG iIndex, CATBSTR & oBSTR)=0;

    virtual HRESULT __stdcall Append(const CATBSTR & iBSTR)=0;

    virtual HRESULT __stdcall RemoveByIndex(CATLONG iIndex)=0;


};

CATDeclareHandler(CATIAPspListOfBSTRs, CATIABase);

#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
