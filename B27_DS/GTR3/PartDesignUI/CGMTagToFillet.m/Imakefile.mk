LINK_WITH_FOR_IID =
#
BUILT_OBJECT_TYPE = SHARED LIBRARY
#
INCLUDED_MODULES = CGMTagToFillet
#
OS = COMMON
#
LINK_WITH= \
  $(LINK_WITH_FOR_IID) \
  JS0FM \
  JS0GROUP \
  DI0STATE \
  DI0PANV2 \
  CATDialogEngine \
  CATApplicationFrame \
  #CATIAApplicationFrame \
  #CAT3DControl \
  #CATVisualization \
  CATMechanicalModelerUI \
  CATObjectSpecsModeler \
  AD0XXBAS \
  CATGitInterfaces \
  CATGMModelInterfaces \
  CATGMOperatorsInterfaces \
  PartItf \
  CATMecModUseItf \	
  MecModItfCPP \	    
  Mathematics \
  CATMathStream \
  CATGMGeometricInterfaces \
  CATGeometricOperators \
  CATTopologicalObjects \
  #CATTopologicalOperators \
  #CATFunctionalTopologicalOpe \
  CATGeometricObjects \
  PRTNOTIF \
  CATVisualization \
#                
#---------------------------------------------------------------------------------------
# Afin d'eviter un Build CXR1 
#---------------------------------------------------------------------------------------
OS = wnt40i
BUILD=NONE
OS = wnt40a
BUILD=NONE
OS = irix53
BUILD=NONE
OS = hp1020ansi
BUILD=NONE
OS = aix410
BUILD=NONE
OS = sun53
BUILD=NONE
