
//*****************************************************************************
// CATIA Version 5 Release 15 Framework StructureFeatures
// � Dassault Systemes, 2005, 2007
//*****************************************************************************
//  Abstract:
//  ---------
//
//*****************************************************************************
//  Usage:
//  ------
// 
//
//
//*****************************************************************************
//  Inheritance:
//  ------------
//  
//*****************************************************************************
//  Main Methods:
//  -------------
//
//*****************************************************************************
//  Historic
//  --------
//
//  Author: Christophe Daguet
//  Date  : 01/10/05
//  Goal  : Define the CATIStructureMfgBridgedOpeningData interface
//
//*****************************************************************************
#ifndef CATIStructureMfgBridgedOpeningData_H
#define CATIStructureMfgBridgedOpeningData_H

/**
* @CAA2Level L0
* @CAA2Usage U3
*/

// System Framework
#include "IUnknown.h"

#include "ListPOfCATEdge.h"
#include "CATStructureMfgDef.h"

extern "C" const IID IID_CATIStructureMfgBridgedOpeningData;

//------------------------------------------------------------------

/**
* Interface to manage the manufacturing bridged opening information.
* <b>Role</b>: To retrieve data from a bridged opening.
*/

class CATIStructureMfgBridgedOpeningData : public IUnknown
{
  
public:
  
  /**
  * Get geometry.
  *   @param oFirstContourListOfEdges
  *      List of edges for the first contour.
  *   @param oSecondContourListOfEdges
  *      List of edges for the second contour.
  *   @return
  *   An HRESULT value.
  *   <br><b>Legal values</b>:
  *   <dl>
  *     <dt>S_OK</dt>
  *     <dd>operation is successful</dd>
  *     <dt>E_FAIL</dt>
  *     <dd>operation failed</dd>
  *   </dl>
  */
  virtual HRESULT GetGeometry(CATLISTP(CATEdge) &oFirstContourListOfEdges, CATLISTP(CATEdge) &oSecondContourListOfEdges) const = 0;

};

//------------------------------------------------------------------

#endif
