
//*****************************************************************************
// CATIA Version 5 Release 16 Framework StructureInterfaces
// � Dassault Systemes, 2005, 2007
//*****************************************************************************
//  Abstract:
//  ---------
//
//*****************************************************************************
//  Usage:
//  ------
// 
//
//
//*****************************************************************************
//  Inheritance:
//  ------------
//  
//*****************************************************************************
//  Main Methods:
//  -------------
//
//*****************************************************************************
//  Historic
//  --------
//
//  Author: Philippe DEBUISSON
//  Date  : 16/09/05
//  Goal  : Define the CATIStructureMfgMarkingTextData interface
//
//*****************************************************************************
#ifndef CATIStructureMfgMarkingTextData_H
#define CATIStructureMfgMarkingTextData_H

/**
* @CAA2Level L0
* @CAA2Usage U3
*/

// System Framework
#include "IUnknown.h"

#include "CATStructureMfgDef.h"

extern "C" const IID IID_CATIStructureMfgMarkingTextData;

//------------------------------------------------------------------

/**
* Interface to manage the manufacturing marking text information.
* <b>Role</b>: To retrieve data from a marking text.
*/

class CATISpecObject;

class CATIStructureMfgMarkingTextData : public IUnknown
{
  
public:
  
  /**
  * Get type.
  *   @param oType
  *      the type of the marking text.
  *   @return
  *   An HRESULT value.
  *   <br><b>Legal values</b>:
  *   <dl>
  *     <dt>S_OK</dt>
  *     <dd>operation is successful</dd>
  *     <dt>E_FAIL</dt>
  *     <dd>operation failed</dd>
  *   </dl>
  */
  virtual HRESULT GetType(CATStrMfgMarkingTextType& oType) const = 0;

  /**
  * Get marked side.
  *   @param oSide
  *      the side where the text is marked.
  *   @return
  *   An HRESULT value.
  *   <br><b>Legal values</b>:
  *   <dl>
  *     <dt>S_OK</dt>
  *     <dd>operation is successful</dd>
  *     <dt>E_FAIL</dt>
  *     <dd>operation failed</dd>
  *   </dl>
  */
  virtual HRESULT GetMarkedSide(CATStrMfgSide& oSide) const = 0;

  /**
  * Get text.
  *   @param oText
  *      the text object.
  *   @return
  *   An HRESULT value.
  *   <br><b>Legal values</b>:
  *   <dl>
  *     <dt>S_OK</dt>
  *     <dd>operation is successful</dd>
  *     <dt>E_FAIL</dt>
  *     <dd>operation failed</dd>
  *   </dl>
  */
  virtual HRESULT GetText(CATISpecObject** opText) const = 0;

};

//------------------------------------------------------------------

#endif
