
//*****************************************************************************
// CATIA Version 5 Release 15 Framework StructureFeatures
// � Dassault Systemes, 2005, 2007
//*****************************************************************************
//  Abstract:
//  ---------
//
//*****************************************************************************
//  Usage:
//  ------
// 
//
//
//*****************************************************************************
//  Inheritance:
//  ------------
//  
//*****************************************************************************
//  Main Methods:
//  -------------
//
//*****************************************************************************
//  Historic
//  --------
//
//  Author: Christophe Daguet
//  Date  : 01/10/05
//  Goal  : Define the CATIStructureMfgMemberData interface
//
//*****************************************************************************
#ifndef CATIStructureMfgMemberData_H
#define CATIStructureMfgMemberData_H

/**
* @CAA2Level L0
* @CAA2Usage U3
*/

// System Framework
#include "IUnknown.h"

#include "CATStructureMfgDef.h"
#include "CATCollec.h"
#include "CATUnicodeString.h"
class CATListPtrCATEdge;
class CATMathPoint;
class CATMathVector;
extern "C" const IID IID_CATIStructureMfgMemberData;

//------------------------------------------------------------------

/**
* Interface to manage the manufacturing member information.
* <b>Role</b>: To retrieve data from a member such as
* marking lines
*/

class CATIStructureMfgMemberData : public IUnknown
{
  
public:
  
  /**
  * Get name of the part.
  *   @param oPartName
  *      Name of the part.
  *   @return
  *   An HRESULT value.
  *   <br><b>Legal values</b>:
  *   <dl>
  *     <dt>S_OK</dt>
  *     <dd>operation is successful</dd>
  *     <dt>E_FAIL</dt>
  *     <dd>operation failed</dd>
  *   </dl>
  */  
  virtual HRESULT GetPartName(CATUnicodeString &oPartName) const = 0;
	
  /**
  * Get Material throw orientation and the anchor point.
  *   @param oAnchorPoint
  *      Anchor point.
  *   @param oMTODir
  *      Material throw orientation.  
  *   @return
  *   An HRESULT value.
  *   <br><b>Legal values</b>:
  *   <dl>
  *     <dt>S_OK</dt>
  *     <dd>operation is successful</dd>
  *     <dt>E_FAIL</dt>
  *     <dd>operation failed</dd>
  *   </dl>
  */
  virtual HRESULT GetMTOAndAnchorPoint( CATMathPoint &oAnchorPoint, CATMathVector &oMTODir ) const = 0;
  
  /**
  * Get marking lines.
  *   @param iPlateSide
  *      Plate side.
  *   @param iMarkingLineType
  *      Marking line type.
  *   @param oListOfEdges
  *      List of CATEdge.
  *   @return
  *   An HRESULT value.
  *   <br><b>Legal values</b>:
  *   <dl>
  *     <dt>S_OK</dt>
  *     <dd>operation is successful</dd>
  *     <dt>E_FAIL</dt>
  *     <dd>operation failed</dd>
  *   </dl>
  */
  virtual HRESULT GetMarkingLines(const CATStrMfgPlateSide iPlateSide, const CATStrMfgMarkingLineType iMarkingLineType,
                                      CATLISTP(CATEdge) &oListOfEdges) const = 0;

};

//------------------------------------------------------------------

#endif
