
//*****************************************************************************
// CATIA Version 5 Release 15 Framework StructureFeatures
// � Dassault Systemes, 2005, 2007
//*****************************************************************************
//  Abstract:
//  ---------
//
//*****************************************************************************
//  Usage:
//  ------
// 
//
//
//*****************************************************************************
//  Inheritance:
//  ------------
//  
//*****************************************************************************
//  Main Methods:
//  -------------
//
//*****************************************************************************
//  Historic
//  --------
//
//  Author: Christophe Daguet
//  Date  : 01/10/05
//  Goal  : Define the CATIStructureMfgTabbedOpeningData interface
//
//*****************************************************************************
#ifndef CATIStructureMfgTabbedOpeningData_H
#define CATIStructureMfgTabbedOpeningData_H

/**
* @CAA2Level L0
* @CAA2Usage U3
*/

// System Framework
#include "IUnknown.h"

#include "CATLISTV_CATMathPoint.h"
#include "CATStructureMfgDef.h"
class CATListPtrCATEdge;

extern "C" const IID IID_CATIStructureMfgTabbedOpeningData;

//------------------------------------------------------------------

/**
* Interface to manage the manufacturing tabbed opening information.
* <b>Role</b>: To retrieve data from a tabbed opening.
*/

class CATIStructureMfgTabbedOpeningData : public IUnknown
{
  
public:
  
  /**
  * Get the micro-junction points.
  *   @param oListOfPoints
  *      List of micro-junction points.
  *   @return
  *   An HRESULT value.
  *   <br><b>Legal values</b>:
  *   <dl>
  *     <dt>S_OK</dt>
  *     <dd>operation is successful</dd>
  *     <dt>E_FAIL</dt>
  *     <dd>operation failed</dd>
  *   </dl>
  */
  virtual HRESULT GetListOfMicroJunctionPoints(CATListValCATMathPoint &oListOfPoints) const=0;  

  /**
  * Get radius micro-junction points.
  *   @param oRadius
  *      Radius.
  *   @return
  *   An HRESULT value.
  *   <br><b>Legal values</b>:
  *   <dl>
  *     <dt>S_OK</dt>
  *     <dd>operation is successful</dd>
  *     <dt>E_FAIL</dt>
  *     <dd>operation failed</dd>
  *   </dl>
  */
  virtual HRESULT GetMicroJunctionRadius(double &oRadius) const=0;  

  /**
  * Get geometry.
  *   @param oListOfEdges
  *      List of edges.
  *   @return
  *   An HRESULT value.
  *   <br><b>Legal values</b>:
  *   <dl>
  *     <dt>S_OK</dt>
  *     <dd>operation is successful</dd>
  *     <dt>E_FAIL</dt>
  *     <dd>operation failed</dd>
  *   </dl>
  */
  virtual HRESULT GetGeometry(CATLISTP(CATEdge) &oListOfEdges) const=0;
};

//------------------------------------------------------------------

#endif
