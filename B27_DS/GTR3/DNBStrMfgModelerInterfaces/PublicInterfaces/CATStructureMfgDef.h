
//*****************************************************************************
// CATIA Version 5 Release 15 Framework StructureFeatures
// � Dassault Systemes, 2005, 2007
//*****************************************************************************
//  Abstract:
//  ---------
//
//*****************************************************************************
//  Usage:
//  ------
// 
//
//
//*****************************************************************************
//  Inheritance:
//  ------------
//  
//*****************************************************************************
//  Main Methods:
//  -------------
//
//*****************************************************************************
//  Historic
//  --------
//
//  Author: Christophe Daguet
//  Date  : 01/10/05
//  Goal  : Define keyword for manufacturing.
//
//*****************************************************************************
#ifndef CATStructureMfgDef_H
#define CATStructureMfgDef_H

// COPYRIGHT DASSAULT SYSTEMES 2008

/**
* @CAA2Level L0
* @CAA2Usage U3
*/


/**
* Defines face to consider.
*/
enum CATStrMfgPlateSide {CatStrMfgBottomSide = 0, CatStrMfgTopSide = +1 };

/**
* Defines orientation to consider.
*/
enum CATStrMfgOrientation {CatStrMfgClockWise = 0, CatStrMfgCounterClockWise = +1 };

/**
* Defines marking line type.
*/
enum CATStrMfgMarkingLineType {
CatStrMfgAttachmentLineType = 0,
CatStrMfgAlignmentLineType = +1, 
CatStrMfgReferenceLineType = +2,
CatStrMfgMarkedOpeningLineType = +3,
CatStrMfgTemplateLocationLineType = +4,
CatStrMfgTemplateSightLineType = +5,
CatStrMfgInverseBendingCurveLineType = +6,
CatStrMfgPositiveMarginLineType = +7 };

/**
* Defines side to consider.
*/
enum CATStrMfgSide {CatStrMfgMoldedSide = 0,  CatStrMfgThickSide = +1 };

/**
* Defines marking text type.
*/
enum CATStrMfgMarkingTextType {
CatStrMfgAllText = 0 ,
CatStrMfgUserText = +1,
CatStrMfgEdgePrepText = +2,
CatStrMfgAttachmentLineText = +3,
CatStrMfgMarginText = +4,
CatStrMfgReferenceLineText = +5,
CatStrMfgOpeningPrepText = +6,
CatStrMfgOtherMarkingLineText = +7 };

//------------------------------------------------------------------

#endif
