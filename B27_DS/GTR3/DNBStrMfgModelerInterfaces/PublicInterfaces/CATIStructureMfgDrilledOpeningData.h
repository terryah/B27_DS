
//*****************************************************************************
// CATIA Version 5 Release 15 Framework StructureFeatures
// � Dassault Systemes, 2005, 2007
//*****************************************************************************
//  Abstract:
//  ---------
//
//*****************************************************************************
//  Usage:
//  ------
// 
//
//
//*****************************************************************************
//  Inheritance:
//  ------------
//  
//*****************************************************************************
//  Main Methods:
//  -------------
//
//*****************************************************************************
//  Historic
//  --------
//
//  Author: Christophe Daguet
//  Date  : 01/10/05
//  Goal  : Define the CATIStructureMfgDrilledOpeningData interface
//
//*****************************************************************************
#ifndef CATIStructureMfgDrilledOpeningData_H
#define CATIStructureMfgDrilledOpeningData_H

/**
* @CAA2Level L0
* @CAA2Usage U3
*/

// System Framework
#include "IUnknown.h"

#include "CATStructureMfgDef.h"

class CATMathPoint;

extern "C" const IID IID_CATIStructureMfgDrilledOpeningData;

//------------------------------------------------------------------

/**
* Interface to manage the manufacturing drilled opening information.
* <b>Role</b>: To retrieve data from a drilled opening.
*/

class CATIStructureMfgDrilledOpeningData : public IUnknown
{
  
public:
  
  /**
  * Get center.
  *   @param oPoints
  *      Center.
  *   @return
  *   An HRESULT value.
  *   <br><b>Legal values</b>:
  *   <dl>
  *     <dt>S_OK</dt>
  *     <dd>operation is successful</dd>
  *     <dt>E_FAIL</dt>
  *     <dd>operation failed</dd>
  *   </dl>
  */
  virtual HRESULT GetCenter(CATMathPoint &oPoint) const = 0;

  /**
  * Get raidus.
  *   @param oRadius
  *      Radius.
  *   @return
  *   An HRESULT value.
  *   <br><b>Legal values</b>:
  *   <dl>
  *     <dt>S_OK</dt>
  *     <dd>operation is successful</dd>
  *     <dt>E_FAIL</dt>
  *     <dd>operation failed</dd>
  *   </dl>
  */
  virtual HRESULT GetRadius(double &oRadius) const = 0;

};

//------------------------------------------------------------------

#endif
