
//*****************************************************************************
// CATIA Version 5 Release 16 Framework StructureInterfaces
// � Dassault Systemes, 2005, 2007
//*****************************************************************************
//  Abstract:
//  ---------
//
//*****************************************************************************
//  Usage:
//  ------
// 
//
//
//*****************************************************************************
//  Inheritance:
//  ------------
//  
//*****************************************************************************
//  Main Methods:
//  -------------
//
//*****************************************************************************
//  Historic
//  --------
//
//  Author: Philippe DEBUISSON
//  Date  : 16/09/05
//  Goal  : Define the CATIStructureMfgEdgePrepData interface
//
//*****************************************************************************
#ifndef CATIStructureMfgEdgePrepData_H
#define CATIStructureMfgEdgePrepData_H

/**
* @CAA2Level L0
* @CAA2Usage U3
*/

// System Framework
#include "IUnknown.h"

#include "CATStructureMfgDef.h"
#include "CATUnicodeString.h"
#include "CATListOfCATUnicodeString.h"
#include "CATListOfDouble.h"

extern "C" const IID IID_CATIStructureMfgEdgePrepData;

//------------------------------------------------------------------

/**
* Interface to manage the manufacturing edge preparation information.
* <b>Role</b>: To retrieve data from an edge preparation.
*/

class CATIStructureMfgEdgePrepData : public IUnknown
{
  
public:
  
  /**
  * Get name.
  *   @param oName
  *      the name of the edge preparation.
  *   @return
  *   An HRESULT value.
  *   <br><b>Legal values</b>:
  *   <dl>
  *     <dt>S_OK</dt>
  *     <dd>operation is successful</dd>
  *     <dt>E_FAIL</dt>
  *     <dd>operation failed</dd>
  *   </dl>
  */
  virtual HRESULT GetName(CATUnicodeString& oName) const = 0;

  /**
  * Get parameters.
  *   @param ospNameList
  *      the list of parameter names.
  *   @param ospValueList
  *      the list of associated parameter values.
  *   @return
  *   An HRESULT value.
  *   <br><b>Legal values</b>:
  *   <dl>
  *     <dt>S_OK</dt>
  *     <dd>operation is successful</dd>
  *     <dt>E_FAIL</dt>
  *     <dd>operation failed</dd>
  *   </dl>
  */
  virtual HRESULT GetParameters(CATListOfCATUnicodeString& ospNameList, CATListOfDouble& ospValueList) const = 0;

  /**
  * Get reference side.
  *   @param oSideMode
  *      reference side of edge preparation.
  *   @return
  *   An HRESULT value.
  *   <br><b>Legal values</b>:
  *   <dl>
  *     <dt>S_OK</dt>
  *     <dd>operation is successful</dd>
  *     <dt>E_FAIL</dt>
  *     <dd>operation failed</dd>
  *   </dl>
  */
  virtual HRESULT GetReferenceSide(CATStrMfgSide& oSide) const = 0;

};

//------------------------------------------------------------------

#endif
