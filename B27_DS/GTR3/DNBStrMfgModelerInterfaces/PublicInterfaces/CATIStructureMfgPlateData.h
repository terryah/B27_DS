
//*****************************************************************************
// CATIA Version 5 Release 15 Framework StructureFeatures
// � Dassault Systemes, 2005, 2007
//*****************************************************************************
//  Abstract:
//  ---------
//
//*****************************************************************************
//  Usage:
//  ------
// 
//
//
//*****************************************************************************
//  Inheritance:
//  ------------
//  
//*****************************************************************************
//  Main Methods:
//  -------------
//
//*****************************************************************************
//  Historic
//  --------
//
//  Author: Christophe Daguet
//  Date  : 01/10/05
//  Goal  : Define the CATIStructureMfgPlateData interface
//
//  Author: Philippe DEBUISSON
//  Date  : 16/09/05
//  Goal  : Add new methods: GetMarkingText
//          Return edge preparation data in GetContour and GetInnerContours methods
//
//*****************************************************************************
#ifndef CATIStructureMfgPlateData_H
#define CATIStructureMfgPlateData_H

// COPYRIGHT DASSAULT SYSTEMES 2008

/**
* @CAA2Level L0
* @CAA2Usage U3
*/

// System Framework
#include "IUnknown.h"

#include "ListPOfCATEdge.h"
#include "CATLISTV_CATISpecObject.h"
#include "CATLISTV_CATMathPoint.h"
#include "CATLISTP_CATMathVector.h"
#include "CATStructureMfgDef.h"
#include "CATListOfInt.h"
#include "CATUnicodeString.h"

class CATISpecObject;
class CATMathPlane;

extern "C" const IID IID_CATIStructureMfgPlateData;

//------------------------------------------------------------------

/**
* Interface to manage the manufacturing plate information.
* <b>Role</b>: To retrieve data from a plate such as
* marking lines, support, contour...
*/

class CATIStructureMfgPlateData : public IUnknown
{
  
public:
  
  /**
  * Get name of the part.
  *   @param oPartName
  *      Name of the part.
  *   @return
  *   An HRESULT value.
  *   <br><b>Legal values</b>:
  *   <dl>
  *     <dt>S_OK</dt>
  *     <dd>operation is successful</dd>
  *     <dt>E_FAIL</dt>
  *     <dd>operation failed</dd>
  *   </dl>
  */  
  virtual HRESULT GetPartName(CATUnicodeString &oPartName) const = 0;

  /**
  * Get plane defining the support.
  *   @param opSupport
  *      Support containing the molded surface. The normal of the support is oriented
  *      on same direction as burn side up.
  *   @return
  *   An HRESULT value.
  *   <br><b>Legal values</b>:
  *   <dl>
  *     <dt>S_OK</dt>
  *     <dd>operation is successful</dd>
  *     <dt>E_FAIL</dt>
  *     <dd>operation failed</dd>
  *   </dl>
  */
  virtual HRESULT GetSupportPlane(CATMathPlane &oSupport) const = 0;

  /**
  * Get the contour taking into all design and manufacturing features except bevels and chamfers.
  * The contour is returned according to normal orientation of the support.
  *   @param oListOfEdges
  *      List of CATEdge.
  *   @return
  *   An HRESULT value.
  *   <br><b>Legal values</b>:
  *   <dl>
  *     <dt>S_OK</dt>
  *     <dd>operation is successful</dd>
  *     <dt>E_FAIL</dt>
  *     <dd>operation failed</dd>
  *   </dl>
  */
  virtual HRESULT GetContour(const CATStrMfgOrientation iOrientation, CATLISTP(CATEdge) &oListOfEdges) const = 0;
  
  
  /**
  * Get egdes of contours inside the boundary of the plate.
  * The contours are returned according to normal orientation of the support.
  *   @param oListOfEdges
  *      List of CATEdge.
  *   @return
  *   An HRESULT value.
  *   <br><b>Legal values</b>:
  *   <dl>
  *     <dt>S_OK</dt>
  *     <dd>operation is successful</dd>
  *     <dt>E_FAIL</dt>
  *     <dd>operation failed</dd>
  *   </dl>
  */
  virtual HRESULT GetInnerContours(const CATStrMfgOrientation iOrientation, CATLISTP(CATEdge) &oListOfEdges) const = 0;

  /**
  * Get the contour taking into account all design and manufacturing features except
  * bevels and chamfers.
  * The contour is returned according to normal orientation of the support.
  *   @param oListOfEdges
  *      List of CATEdge.
  *   @param ospPrepared
  *      the list of integer that described if the associated edge has been prepared (value is 1)
  *      or not (value is 0).
  *   @param ospEdgePrepList
  *      the list of associated edge preparation object if the edge has been prepared.
  *   @return
  *   An HRESULT value.
  *   <br><b>Legal values</b>:
  *   <dl>
  *     <dt>S_OK</dt>
  *     <dd>operation is successful</dd>
  *     <dt>S_FALSE</dt>
  *     <dd>operation is successful but some none orthogonal external lateral faces
  *         have been detected. In that case the external contour may be not accurate</dd>
  *     <dt>E_FAIL</dt>
  *     <dd>operation failed</dd>
  *   </dl>
  */
  virtual HRESULT GetContour(const CATStrMfgOrientation iOrientation,
                             CATLISTP(CATEdge) &oListOfEdges,
                             CATListOfInt& ospPrepared,
                             CATListValCATISpecObject_var& ospEdgePrepList) const = 0;
  
  
  /**
  * Get egdes of contours inside the boundary of the plate.
  * The contours are returned according to normal orientation of the support.
  *   @param oListOfEdges
  *      List of CATEdge.
  *   @param ospPrepared
  *      the list of integer that described if the associated edge has been prepared (value is 1)
  *      or not (value is 0).
  *   @param ospEdgePrepList
  *      the list of associated edge preparation object if the edge has been prepared.
  *   @return
  *   An HRESULT value.
  *   <br><b>Legal values</b>:
  *   <dl>
  *     <dt>S_OK</dt>
  *     <dd>operation is successful</dd>
  *     <dt>S_FALSE</dt>
  *     <dd>operation is successful but some none orthogonal inner lateral faces
  *         have been detected. In that case the inner contour may be not accurate</dd>
  *     <dt>E_FAIL</dt>
  *     <dd>operation failed</dd>
  *   </dl>
  */
  virtual HRESULT GetInnerContours(const CATStrMfgOrientation iOrientation,
                                   CATLISTP(CATEdge) &oListOfEdges,
                                   CATListOfInt& ospPrepared,
                                   CATListValCATISpecObject_var& ospEdgePrepList) const = 0;
  
  /**
  * @deprecated V5R16
  * Get marking lines.
  *   @param iPlateSide
  *      Plate side.
  *   @param iMarkingLineType
  *      Marking line type.
  *   @param oListOfEdges
  *      List of CATEdge.
  *   @return
  *   An HRESULT value.
  *   <br><b>Legal values</b>:
  *   <dl>
  *     <dt>S_OK</dt>
  *     <dd>operation is successful</dd>
  *     <dt>E_FAIL</dt>
  *     <dd>operation failed</dd>
  *   </dl>
  */
  virtual HRESULT GetMarkingLines(const CATStrMfgPlateSide iPlateSide,
                                  const CATStrMfgMarkingLineType iMarkingLineType,
                                  CATLISTP(CATEdge) &oListOfEdges) const = 0;

  /**
  * Get marking lines.
  *   @param iPlateSide
  *      Plate side.
  *   @param iMarkingLineType
  *      Marking line type.
  *   @param oListOfEdges
  *      List of CATEdge.
  *   @return
  *   An HRESULT value.
  *   <br><b>Legal values</b>:
  *   <dl>
  *     <dt>S_OK</dt>
  *     <dd>operation is successful</dd>
  *     <dt>E_FAIL</dt>
  *     <dd>operation failed</dd>
  *   </dl>
  */
  virtual HRESULT GetMarkingLines(const CATStrMfgSide iPlateSide,
                                  const CATStrMfgMarkingLineType iMarkingLineType,
                                  CATLISTP(CATEdge) &oListOfEdges) const = 0;

  /**  
	* Get marking lines - For attachment Lines only
	*   @param iPlateSide
	*      Plate side.
	*   @param iMarkingLineType
	*      Marking line type.
	*   @param oListOfEdges
	*      List of CATEdge.
	*   @param oListOfMTODir
	*      List of Material Throw Orientation.
	*   @param oListOfMTOOrigin
	*      List of points of MTO Origin.
	*   @return
	*   An HRESULT value.
	*   <br><b>Legal values</b>:
	*   <dl>
	*     <dt>S_OK</dt>
	*     <dd>operation is successful</dd>
	*     <dt>E_FAIL</dt>
	*     <dd>operation failed</dd>
	*   </dl>
	*/
	virtual HRESULT GetMarkingLines(const CATStrMfgSide iPlateSide,
									const CATStrMfgMarkingLineType iMarkingLineType,
									CATLISTP(CATEdge) &oListOfEdges,
									CATLISTP(CATMathVector) &oListOfMTODir, 
									CATLISTV(CATMathPoint) &oListOfMTOOrigin) const = 0;

  /**
	* Get list of openings.
	*   @param iTypeIID
	*     It is the identifier of an interface that the expected objects must implement.
    *   @param oListOfOpenings
	*     List of openings.
	*   @return
	*   An HRESULT value.
	*   <br><b>Legal values</b>:
	*   <dl>
	*     <dt>S_OK</dt>
	*     <dd>operation is successful</dd>
	*     <dt>E_FAIL</dt>
	*     <dd>operation failed</dd>
	*   </dl>
	*/
	virtual HRESULT GetListOfOpenings( const IID & iTypeIID, CATListValCATISpecObject_var &oListOfOpenings) const = 0;

  /**
	* Get list of marking text.
	*   @param iSide
	*     the side of the structure object where the text is marked.
   *   @param iType
	*     the type of the marking text.
   *   @param ospMarkingTextList
	*     the list of marking texts of 'iType' marked on 'iSide' side.
	*   @return
	*   An HRESULT value.
	*   <br><b>Legal values</b>:
	*   <dl>
	*     <dt>S_OK</dt>
	*     <dd>operation is successful</dd>
	*     <dt>E_FAIL</dt>
	*     <dd>operation failed</dd>
	*   </dl>
	*/
   virtual HRESULT GetMarkingText(const CATStrMfgSide& iSide,
                                  const CATStrMfgMarkingTextType& iType,
                                  CATListValCATISpecObject_var& ospMarkingTextList) const = 0;

   /**
	* Get list of marking text.
	*   @param iSide
	*     the side of the structure object where the text is marked.
    *   @param iType
	*     the type of the marking text.
    *   @param ospMarkingTextList
	*     the list of marking texts of 'iType' marked on 'iSide' side.
	*   @param ospMarkingTextOrientation
	*     the list of the marking text orientations.
    *   @param ospMarkingTextOrigin
	*     the list of marking text origin.
	*   @return
	*   An HRESULT value.
	*   <br><b>Legal values</b>:
	*   <dl>
	*     <dt>S_OK</dt>
	*     <dd>operation is successful</dd>
	*     <dt>E_FAIL</dt>
	*     <dd>operation failed</dd>
	*   </dl>
	*/
   virtual HRESULT GetMarkingText(const CATStrMfgMarkingTextType& iType,
                                  CATListValCATUnicodeString& ospMarkingTextList,
								  CATLISTP(CATMathVector) &ospMarkingTextOrientation,
								  CATLISTV(CATMathPoint) &ospMarkingTextOrigin) const = 0;

   /**
	* Get list of user text.	
    *   @param ispUserTextNameList
	*     the list of names of user texts.
	*   @param ospUserTextValueList
	*     the list of the values user texts.
    *   @param ospMarkingTextOrigin
	*     the list of marking text origin.
	*   @return
	*   An HRESULT value.
	*   <br><b>Legal values</b>:
	*   <dl>
	*     <dt>S_OK</dt>
	*     <dd>operation is successful</dd>
	*     <dt>E_FAIL</dt>
	*     <dd>operation failed</dd>
	*   </dl>
	*/   
   virtual HRESULT GetUserText( CATListValCATUnicodeString ispUserTextNameList,
								CATListValCATUnicodeString &ospUserTextValueList,
								CATMathPoint &ospUserTextOrigin) const = 0;

  /**
	* Get burn side up side.
	*   @param oSide
	*     the burn side up side.
	*   @return
	*   An HRESULT value.
	*   <br><b>Legal values</b>:
	*   <dl>
	*     <dt>S_OK</dt>
	*     <dd>operation is successful</dd>
	*     <dt>E_FAIL</dt>
	*     <dd>operation failed</dd>
	*   </dl>
	*/
   virtual HRESULT GetBurnSideUpSide(CATStrMfgSide& oSide) const = 0;

   /**
	* Get burn side up vector.
	*   @param oBSUDir
	*     the burn side up vector.
	*   @return
	*   An HRESULT value.
	*   <br><b>Legal values</b>:
	*   <dl>
	*     <dt>S_OK</dt>
	*     <dd>operation is successful</dd>
	*     <dt>E_FAIL</dt>
	*     <dd>operation failed</dd>
	*   </dl>
	*/
   virtual HRESULT GetBSUVector(CATMathVector& oBSUDir) const = 0;

};

//------------------------------------------------------------------

#endif
