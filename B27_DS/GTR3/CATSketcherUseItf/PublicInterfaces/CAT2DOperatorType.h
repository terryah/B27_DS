/* -*-c++-*- */
#ifndef CAT2DOperatorType_h
#define CAT2DOperatorType_h

// COPYRIGHT DASSAULT SYSTEMES 2001

/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */

enum CAT2DOperatorType
{
  Operator2DType_Projection   = 1,
  Operator2DType_Intersection = 2,
  Operator2DType_Silhouette   = 3,
  Operator2DType_Offset       = 4
};

#endif
