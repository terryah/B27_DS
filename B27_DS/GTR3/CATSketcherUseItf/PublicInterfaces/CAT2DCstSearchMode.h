/* -*-c++-*- */
#ifndef CAT2DCstSearchMode_h
#define CAT2DCstSearchMode_h

// COPYRIGHT DASSAULT SYSTEMES 1999

/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */

enum CAT2DCstSearchMode
{
  Cst2DSearch_Stacked = 1,
  Cst2DSearch_Chained = 2
};

#endif
