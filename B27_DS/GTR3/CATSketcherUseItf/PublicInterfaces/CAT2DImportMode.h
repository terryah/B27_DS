/* -*-c++-*- */
#ifndef CAT2DImportMode_h
#define CAT2DImportMode_h

// COPYRIGHT DASSAULT SYSTEMES 1999

/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */

enum CAT2DImportMode
{
  Import2DMode_Projection   = 1,
  Import2DMode_Intersection = 2,
  Import2DMode_Silhouette   = 3
};

#endif
