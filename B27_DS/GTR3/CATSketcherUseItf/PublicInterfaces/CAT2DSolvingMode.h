/* -*-c++-*- */
#ifndef CAT2DSolvingMode_h
#define CAT2DSolvingMode_h

// COPYRIGHT DASSAULT SYSTEMES 1999

/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */

enum CAT2DSolvingMode
{
  Solving2DMode_Standard          = 0,
  Solving2DMode_MinimumMove       = 1,
  Solving2DMode_Relaxation        = 2,
  Solving2DMode_WeightedStandard  = 3
};

#endif
