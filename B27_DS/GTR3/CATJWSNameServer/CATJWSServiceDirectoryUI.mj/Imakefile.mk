# COPYRIGHT DASSAULT SYSTEMES 2005
# Interactive JDialog application used to administrate internal/CAA Web services in a Service Directory instance

BUILT_OBJECT_TYPE=JAVA
TYPE = SERVER

LINK_WITH = CATJSystem CATJDialog CATJWSInfra CATJWSInfraImpl CATJWSUddiItf

LOCAL_JAVA_FLAGS=-J-ms16m -J-mx96m

