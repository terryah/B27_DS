BUILT_OBJECT_TYPE=SHARED LIBRARY

LINK_WITH =  \
  CATMathematics \
  CATMathStream \
  CATPolyhedralInterfaces \
  CATPolyhedralMathematics \
  CATPolyhedralObjects \
  CATPolyhedralOperators \
  CATPolyhedralVisualization \
  JS0GROUP \
  YN000FUN

INCLUDED_MODULES = \
  PolyBodyCanonicDetection \
  CATPolyCanonicRecongition \
  CATPolyCanonicProjection \
  PolyAdvDeform

#
OS = Windows_NT
#if os Windows_NT
#if defined (CATIAV5R28) || defined (CATIAR420)
LOCAL_CCFLAGS = /EHsc /D_CATNoWarningPromotion_
#else
LOCAL_CCFLAGS = /EHsc
#endif
#else
OPTIMIZATION_CPP = /O2
#endif
#
