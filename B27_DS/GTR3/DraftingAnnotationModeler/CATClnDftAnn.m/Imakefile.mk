BUILT_OBJECT_TYPE=SHARED LIBRARY

LINK_WITH = CATClnSpecs \
            CATClnBase \
            CATObjectSpecsModeler \
            CATObjectModelerBase \
            JS0GROUP \
            CATDraftingInterfaces \
            CATDraftingInfrastructure \
            CATDraftingAnnotationModeler \
            CATAnnotationModeler \
            CATSketcherInterfaces \
			CATVisualization \
            CATLiteralFeatures
