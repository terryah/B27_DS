BUILT_OBJECT_TYPE = SHARED LIBRARY

LINK_WITH = CATMathematics \
            JS0FM \
            JS0GROUP \
            CATObjectModelerBase \
            CATObjectSpecsModeler \
            CATDraftingInterfaces \
            CATDraftingBaseInfra \
            CATDraftingInfrastructure \
            CATMecModInterfaces \
            CATAnnotationModeler \
            CATDraftingAnnotationModeler
