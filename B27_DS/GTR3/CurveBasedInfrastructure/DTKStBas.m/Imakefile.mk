# COPYRIGHT DASSAULT SYSTEMES 1999
#======================================================================
# Imakefile for module DTKStBas.m
#======================================================================
#  Jul 2004  Optimisation O2                                       amr
#  Nov 1999  BUILT_OBJECT_TYPE=NONE for the bigger module          mmo
#            CATCurveBasedInfrastructure.m
#  Oct 1999  Supression de JS0ERROR JS0CORBA  DTKSystem            jfi
#  Jun 1999  Creation: Code generated by the CAA wizard            jfi
#======================================================================
#
# SHARED LIBRARY 
#
BUILT_OBJECT_TYPE=NONE 

COMDYN_MODULE = DTKSystem

IMPACT_ON_IMPORT=YES 

OS = AIX
SYS_LIBS = -lxlf -lxlf90 -lxlfpad

OS = IRIX
SYS_LIBS = -lftn

OS = Windows_NT
#if os win_b64
#else
OPTIMIZATION_CPP = /O2
#endif

OS = HP-UX
SYS_LIBS = -lf

OS = SunOS
SYS_LIBS = -lF77 -lM77
