#ifdef LOCAL_DEFINITION_FOR_IID
LINK_WITH_FOR_IID = \
CATAssemblyUIUUID \
CATIAApplicationFrameUUID
#else
LINK_WITH_FOR_IID =
#endif
# COPYRIGHT DASSAULT SYSTEMES 2000
#======================================================================
# Imakefile for module CATAssSearch.m
#======================================================================
#
#  Feb 2000  Creation: Code generated by the CAA wizard  CRX
#======================================================================
#
# SHARED LIBRARY 
#
BUILT_OBJECT_TYPE=SHARED LIBRARY 
 
# DO NOT EDIT :: THE CAA2 WIZARDS WILL ADD CODE HERE
WIZARD_LINK_MODULES = 
# END WIZARD EDITION ZONE

LINK_WITH=$(LINK_WITH_FOR_IID)   AD0XXBAS \
   CATCafSearch \
   JS0FM \
   JS0STR

# System dependant variables
#
OS = AIX
#
OS = HP-UX
#
OS = IRIX
#
OS = SunOS
#
OS = Windows_NT
