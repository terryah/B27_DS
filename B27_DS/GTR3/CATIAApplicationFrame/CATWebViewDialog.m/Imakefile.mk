#======================================================================
# Imakefile for module CATWebViewDialog.m
#======================================================================

BUILT_OBJECT_TYPE = SHARED LIBRARY

LINK_WITH = NS0S3STR JS0SCBAK JS0FM \ # System
            DI0PANV2 CATDlgHtml \ # Dialog
            CATCefWrapper CATCefCommon \ # ChromiumEmbedded

BUILD = NO

#
OS = Windows
BUILD = YES
# For official CATIAV5PrecompiledHeader.h to include afxwin.h
MKMFC_DEPENDENCY = yes
