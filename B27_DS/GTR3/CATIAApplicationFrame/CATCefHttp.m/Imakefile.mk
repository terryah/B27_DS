#======================================================================
# Imakefile for CATCefHttp.m
#======================================================================

BUILT_OBJECT_TYPE = SHARED LIBRARY
BUILD = NO

LINK_WITH = JS0GROUP JS0FM \ #System
            CATCefWrapper CATCefWrapperMT libcef libcef_dll_wrapper \ #ChromiumEmbedded

OS = Windows
BUILD = YES
LOCAL_CCFLAGS = /D CHROMIUM_BUILD /D USING_CEF_SHARED /D NDEBUG
LOCAL_POST_CCFLAGS = /wd4530 /wd4127 /wd4577 /wd4800
