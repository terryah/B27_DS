#ifndef CATExtIDlgTableViewLineModel_h
#define CATExtIDlgTableViewLineModel_h

// COPYRIGHT DASSAULT SYSTEMES 2007

/** 
 * @CAA2Level L1
 * @CAA2Usage U2
 */

#include "CATBaseUnknown.h"
#include "CATDlgView.h"
#include "CATDlgTableView.h" // Pour les styles

class ExportedByCATDlgView CATExtIDlgTableViewLineModel : public CATBaseUnknown
{

};

#endif
 
 
