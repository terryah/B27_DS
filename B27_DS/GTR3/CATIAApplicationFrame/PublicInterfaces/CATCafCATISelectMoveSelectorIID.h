#ifndef CATISelectMoveSelectorIID_h
#define CATISelectMoveSelectorIID_h

// COPYRIGHT DASSAULT SYSTEMES 2004

/** 
 * @CAA2Level L1
 * @CAA2Usage U1
 */ 

#include "SELECT.h"

/**
 * IID of interface CATISelectMoveSelector.
 */
#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedBySELECT IID IID_CATISelectMoveSelector;
#else
extern "C" const IID IID_CATISelectMoveSelector;
#endif

#endif
