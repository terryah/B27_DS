#ifndef CATCafSearchInformationAdapter_H
#define CATCafSearchInformationAdapter_H
#include "Search.h"

// COPYRIGHT DASSAULT SYSTEMES 2008
// public Search.m

/**
 * @CAA2Level L1
 * @CAA2Usage U2
 */

#include "CATBaseUnknown.h"

class ExportedBySearch CATCafSearchInformationAdapter : public CATBaseUnknown
{
  
  /** @nodoc */
  CATDeclareClass;

public:
  
  CATCafSearchInformationAdapter ();
  virtual ~CATCafSearchInformationAdapter ();

};

#endif
