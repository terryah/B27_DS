# Imakefile for all CATIAApplicationFrame modules
BUILT_OBJECT_TYPE= NONE

INCLUDED_MODULES=CD0CGR CD0SHOW CD0STWIN ON0FRAME SELECT CD0NTOG \
	VE0GEDIT CD0VRML OM0EDPRO

LINK_WITH=JS0CORBA JS0FM JS0SCBAK JS03TRA JS0STR JS0CTYP \
	JS0CATLM JS0LIB0 JS0ERROR JS0SETT JS0DSPA JS0LOGRP JS0MRSHL \
	YN000M2D YN000MFL \
	JS0CATLM JS0INF InfItf \
	NS0S3STR \
	DI0PANV2 \
	CO0LSTPV CO0LSTST \
        VE0BASE VE0MDL VE0LOD VE0PIX \
	AD0XXBAS \
	CD0FRAME CD0WIN CD0EDIT \
	VE0GRPH2 \
	DI0STATE DI0APPLI \
	ON0MAIN ON0FRAME ON0GRAPH YP00IMPL
