#======================================================================
# Imakefile for module 3DSWebHelper.m
#======================================================================
# Load module / Executable
#======================================================================

BUILT_OBJECT_TYPE = LOAD MODULE
BUILD = NO
DESCRIPTION = "CATIA"

LINK_WITH = libcef libcef_dll_wrapper CATCefWrapperMT \ #ChromiumEmbedded

#
OS = Windows
BUILD = YES
LOCAL_CCFLAGS = /D CHROMIUM_BUILD /D USING_CEF_SHARED
LOCAL_LDFLAGS = /SUBSYSTEM:WINDOWS
