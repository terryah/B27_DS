#======================================================================
# Imakefile for module CATCefWrapperMT.m
#======================================================================

BUILT_OBJECT_TYPE = SHARED LIBRARY
BUILD = NO

LINK_WITH = JS0GROUP JS0FM CATSysMultiThreading CATSysTS CATSysMainThreadMQ \ #System
            CATCefCommon libcef_dll_wrapper libcef                          \ #ChromiumEmbedded
            CATOmxKernel                                                    \ #ObjectModelerCollection
            AD0XXBAS                                                        \ #ObjectModelerBase

OS = Windows
BUILD = YES
LOCAL_CCFLAGS = /D CHROMIUM_BUILD /D USING_CEF_SHARED /D NDEBUG
