#======================================================================
# Imakefile for module CATCefWrapper.m
#======================================================================

BUILT_OBJECT_TYPE = SHARED LIBRARY
BUILD = NO

LINK_WITH = JS0GROUP JS0FM                                         \ #System
            CATCefCommon CATCefWrapperMT libcef libcef_dll_wrapper \ #ChromiumEmbedded

OS = Windows
BUILD = YES
LOCAL_CCFLAGS = /D CHROMIUM_BUILD /D USING_CEF_SHARED /D NDEBUG
