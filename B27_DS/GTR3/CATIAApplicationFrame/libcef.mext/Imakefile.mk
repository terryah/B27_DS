BUILT_OBJECT_TYPE = SHARED LIBRARY
PROGRAM_NAME = libcef
BUILD = NO

OS = Windows
BUILD = YES

OS = Windows64
#if (defined MK_MSCVER) && (MK_MSCVER >= 1900)
LOCAL_SRCPATH=win_b64/vc14.0
#else
LOCAL_SRCPATH=win_b64/vc11.0
#endif

OS = Windows32
#if (defined MK_MSCVER) && (MK_MSCVER >= 1900)
LOCAL_SRCPATH=intel_a/vc14.0
#else
LOCAL_SRCPATH=intel_a/vc11.0
#endif
