//=============================================================================
// COPYRIGHT DASSAULT SYSTEMES   2002
//=============================================================================
// 22 novembre 2002 - Cr�ation par JAC
//=============================================================================
/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */
#ifndef CATIniSearchBooleanCriterionComponent_H
#define CATIniSearchBooleanCriterionComponent_H
#include "CATInteractiveInterfaces.h"
#include "IUnknown.h"
/**
 * Component linked to the @href CATIIniSearchBooleanCriterion interface.
 * <b>Role:</b> This component implements the @href CATIIniSearchBooleanCriterion 
 * interface.
 */
extern ExportedByCATInteractiveInterfaces CLSID CLSID_CATIniSearchBooleanCriterionComponent;
//Pour plus tard en R11GA
//CATDeclareComponent(CATIniSearchBooleanCriterionComponent);

#endif
