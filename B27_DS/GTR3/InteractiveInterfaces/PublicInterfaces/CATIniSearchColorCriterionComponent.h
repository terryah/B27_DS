//=============================================================================
// COPYRIGHT DASSAULT SYSTEMES   2002
//=============================================================================
// 22 novembre 2002 - Cr�ation par JAC
//=============================================================================
/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */
#ifndef CATIniSearchColorCriterionComponent_H
#define CATIniSearchColorCriterionComponent_H
#include "CATInteractiveInterfaces.h"
#include "IUnknown.h"
/**
 * Component linked to the @href CATIIniSearchColorCriterion interface.
 * <b>Role:</b> This component implements the @href CATIIniSearchColorCriterion 
 * interface.
 */
extern ExportedByCATInteractiveInterfaces CLSID CLSID_CATIniSearchColorCriterionComponent;
//Pour plus tard en R11GA
//CATDeclareComponent(CATIniSearchColorCriterionComponent);

#endif
