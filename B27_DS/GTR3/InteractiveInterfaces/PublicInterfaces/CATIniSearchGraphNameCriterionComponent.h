//=============================================================================
// COPYRIGHT DASSAULT SYSTEMES   2002
//=============================================================================
// 22 novembre 2002 - Cr�ation par JAC
//=============================================================================
/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */
#ifndef CATIniSearchGraphNameCriterionComponent_H
#define CATIniSearchGraphNameCriterionComponent_H
#include "CATInteractiveInterfaces.h"
#include "IUnknown.h"
/**
 * Component linked to the @href CATIIniSearchGraphNameCriterion interface.
 * <b>Role:</b> This component implements the @href CATIIniSearchGraphNameCriterion 
 * interface.
 */
extern ExportedByCATInteractiveInterfaces CLSID CLSID_CATIniSearchGraphNameCriterionComponent;
//Pour plus tard en R11GA
//CATDeclareComponent(CATIniSearchGraphNameCriterionComponent);

#endif
