//=============================================================================
// COPYRIGHT DASSAULT SYSTEMES   2002
//=============================================================================
// 22 novembre 2002 - Cr�ation par JAC
//=============================================================================
/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */
#ifndef CATIniSearchIntegerCriterionComponent_H
#define CATIniSearchIntegerCriterionComponent_H
#include "CATInteractiveInterfaces.h"
#include "IUnknown.h"
/**
 * Component linked to the @href CATIIniSearchIntegerCriterion interface.
 * <b>Role:</b> This component implements the @href CATIIniSearchIntegerCriterion 
 * interface.
 */
extern ExportedByCATInteractiveInterfaces CLSID CLSID_CATIniSearchIntegerCriterionComponent;
//Pour plus tard en R11GA
//CATDeclareComponent(CATIniSearchIntegerCriterionComponent);

#endif
