//=============================================================================
// COPYRIGHT DASSAULT SYSTEMES   2002
//=============================================================================
// 22 novembre 2002 - Cr�ation par JAC
//=============================================================================
/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */
#ifndef CATIniSearchV4ModelCriterionComponent_H
#define CATIniSearchV4ModelCriterionComponent_H
#include "CATInteractiveInterfaces.h"
#include "IUnknown.h"
/**
 * Component linked to the @href CATIIniSearchV4ModelCriterion interface.
 * <b>Role:</b> This component implements the @href CATIIniSearchV4ModelCriterion 
 * interface.
 */
extern ExportedByCATInteractiveInterfaces CLSID CLSID_CATIniSearchV4ModelCriterionComponent;
//Pour plus tard en R11GA
//CATDeclareComponent(CATIniSearchV4ModelCriterionComponent);

#endif
