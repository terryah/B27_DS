//=============================================================================
// COPYRIGHT DASSAULT SYSTEMES   2002
//=============================================================================
// 22 novembre 2002 - Cr�ation par JAC
//=============================================================================
/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */
#ifndef CATIniSearchContextComponent_H
#define CATIniSearchContextComponent_H
#include "CATInteractiveInterfaces.h"
#include "IUnknown.h"
/**
 * Component linked to the @href CATIIniSearchContext interface.
 * <b>Role:</b> This component implements the @href CATIIniSearchContext 
 * interface.
 */
extern ExportedByCATInteractiveInterfaces CLSID CLSID_CATIniSearchContextComponent;
//Pour plus tard en R11GA
//CATDeclareComponent(CATIniSearchContextComponent);

#endif
