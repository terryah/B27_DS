//=============================================================================
// COPYRIGHT DASSAULT SYSTEMES   2002
//=============================================================================
// 22 novembre 2002 - Cr�ation par JAC
//=============================================================================
/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */
#ifndef CATIniSearchOrCriterionComponent_H
#define CATIniSearchOrCriterionComponent_H
#include "CATInteractiveInterfaces.h"
#include "IUnknown.h"
/**
 * Component linked to the @href CATIIniSearchOrCriterion interface.
 * <b>Role:</b> This component implements the @href CATIIniSearchOrCriterion 
 * interface.
 */
extern ExportedByCATInteractiveInterfaces CLSID CLSID_CATIniSearchOrCriterionComponent;
//Pour plus tard en R11GA
//CATDeclareComponent(CATIniSearchOrCriterionComponent);

#endif
