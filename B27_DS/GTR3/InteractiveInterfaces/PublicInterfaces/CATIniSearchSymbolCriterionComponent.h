#ifndef CATIniSearchSymbolCriterionComponent_H
#define CATIniSearchSymbolCriterionComponent_H
//=============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2005
//=============================================================================
#include "CATInteractiveInterfaces.h"
/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */
#include "IUnknown.h"
/**
 * Component linked to the @href CATIIniSearchSymbolCriterion interface.
 * <b>Role:</b> This component implements the @href CATIIniSearchSymbolCriterion 
 * interface.
 */
extern ExportedByCATInteractiveInterfaces CLSID CLSID_CATIniSearchSymbolCriterionComponent;

#endif
