//=============================================================================
// COPYRIGHT DASSAULT SYSTEMES   2002
//=============================================================================
// 22 novembre 2002 - Cr�ation par JAC
//=============================================================================
/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */
#ifndef CATIniSearchTypeCriterionComponent_H
#define CATIniSearchTypeCriterionComponent_H
#include "CATInteractiveInterfaces.h"
#include "IUnknown.h"
/**
 * Component linked to the @href CATIIniSearchTypeCriterion interface.
 * <b>Role:</b> This component implements the @href CATIIniSearchTypeCriterion 
 * interface.
 */
extern ExportedByCATInteractiveInterfaces CLSID CLSID_CATIniSearchTypeCriterionComponent;
//Pour plus tard en R11GA
//CATDeclareComponent(CATIniSearchTypeCriterionComponent);

#endif
