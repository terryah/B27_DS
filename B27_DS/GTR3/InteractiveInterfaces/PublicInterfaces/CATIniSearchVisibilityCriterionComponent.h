//=============================================================================
// COPYRIGHT DASSAULT SYSTEMES   2002
//=============================================================================
// 22 novembre 2002 - Cr�ation par JAC
//=============================================================================
/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */
#ifndef CATIniSearchVisibilityCriterionComponent_H
#define CATIniSearchVisibilityCriterionComponent_H
#include "CATInteractiveInterfaces.h"
#include "IUnknown.h"
/**
 * Component linked to the @href CATIIniSearchVisibilityCriterion interface.
 * <b>Role:</b> This component implements the @href CATIIniSearchVisibilityCriterion 
 * interface.
 */
extern ExportedByCATInteractiveInterfaces CLSID CLSID_CATIniSearchVisibilityCriterionComponent;
//Pour plus tard en R11GA
//CATDeclareComponent(CATIniSearchVisibilityCriterionComponent);

#endif
