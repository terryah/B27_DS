// COPYRIGHT Dassault Systemes 2004

/** 
  * @CAA2Level L1
  * @CAA2Usage U1
  */ 

/**
 * @collection CATLISTP(CATIIniLayer)
 * Collection class for pointers to @href CATIIniLayer.
 * All the methods of pointer collection classes are available.
 * Refer to the articles dealing with collections in the encyclopedia.
 */


#ifndef CATIniListOfCATIIniLayer_H
#define CATIniListOfCATIIniLayer_H

#include "CATInteractiveInterfaces.h"

#include "CATLISTP_Clean.h"
#include "CATLISTP_PublicInterface.h"
#include "CATLISTP_Declare.h"

class CATIIniLayer;

#undef	CATCOLLEC_ExportedBy
#define	CATCOLLEC_ExportedBy	ExportedByCATInteractiveInterfaces


CATLISTP_DECLARE (CATIIniLayer)

#endif




