//=============================================================================
// COPYRIGHT DASSAULT SYSTEMES   2002
//=============================================================================
// 22 novembre 2002 - Cr�ation par JAC
//=============================================================================
/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */
#ifndef CATIniSearchUserCriterionComponent_H
#define CATIniSearchUserCriterionComponent_H
#include "CATInteractiveInterfaces.h"
#include "IUnknown.h"
/**
 * Component linked to the @href CATIIniSearchUserCriterion interface.
 * <b>Role:</b> This component implements the @href CATIIniSearchUserCriterion 
 * interface.
 */
extern ExportedByCATInteractiveInterfaces CLSID CLSID_CATIniSearchUserCriterionComponent;
//Pour plus tard en R11GA
//CATDeclareComponent(CATIniSearchUserCriterionComponent);

#endif
