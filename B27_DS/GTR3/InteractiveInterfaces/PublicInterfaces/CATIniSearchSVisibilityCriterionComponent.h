#ifndef CATIniSearchSVisibilityCriterionComponent_H
#define CATIniSearchSVisibilityCriterionComponent_H
#include "CATInteractiveInterfaces.h"
//=============================================================================
// COPYRIGHT DASSAULT SYSTEMES   2004
//=============================================================================
// 16 april 2004 - Created by JFS
//=============================================================================
/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */
#include "IUnknown.h"
/**
 * Component linked to the @href CATIIniSearchSVisibilityCriterion interface.
 * <b>Role:</b> This component implements the @href CATIIniSearchSVisibilityCriterion 
 * interface.
 */
extern ExportedByCATInteractiveInterfaces CLSID CLSID_CATIniSearchSVisibilityCriterionComponent;
//Pour plus tard en R11GA
//CATDeclareComponent(CATIniSearchSVisibilityCriterionComponent);

#endif
