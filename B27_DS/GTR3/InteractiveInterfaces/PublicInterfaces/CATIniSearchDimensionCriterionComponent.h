//=============================================================================
// COPYRIGHT DASSAULT SYSTEMES   2002
//=============================================================================
// 22 novembre 2002 - Cr�ation par JAC
//=============================================================================
/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */
#ifndef CATIniSearchDimensionCriterionComponent_H
#define CATIniSearchDimensionCriterionComponent_H
#include "CATInteractiveInterfaces.h"
#include "IUnknown.h"
/**
 * Component linked to the @href CATIIniSearchDimensionCriterion interface.
 * <b>Role:</b> This component implements the @href CATIIniSearchDimensionCriterion 
 * interface.
 */
extern ExportedByCATInteractiveInterfaces CLSID CLSID_CATIniSearchDimensionCriterionComponent;
//Pour plus tard en R11GA
//CATDeclareComponent(CATIniSearchDimensionCriterionComponent);

#endif
