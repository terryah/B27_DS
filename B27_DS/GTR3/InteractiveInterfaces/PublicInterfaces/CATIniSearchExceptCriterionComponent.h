//=============================================================================
// COPYRIGHT DASSAULT SYSTEMES   2002
//=============================================================================
// 22 novembre 2002 - Cr�ation par JAC
//=============================================================================
/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */
#ifndef CATIniSearchExceptCriterionComponent_H
#define CATIniSearchExceptCriterionComponent_H
#include "CATInteractiveInterfaces.h"
#include "IUnknown.h"
/**
 * Component linked to the @href CATIIniSearchExceptCriterion interface.
 * <b>Role:</b> This component implements the @href CATIIniSearchExceptCriterion 
 * interface.
 */
extern ExportedByCATInteractiveInterfaces CLSID CLSID_CATIniSearchExceptCriterionComponent;
//Pour plus tard en R11GA
//CATDeclareComponent(CATIniSearchExceptCriterionComponent);

#endif
