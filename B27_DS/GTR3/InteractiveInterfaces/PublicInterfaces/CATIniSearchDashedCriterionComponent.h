//=============================================================================
// COPYRIGHT DASSAULT SYSTEMES   2002
//=============================================================================
// 22 novembre 2002 - Cr�ation par JAC
//=============================================================================
/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */
#ifndef CATIniSearchDashedCriterionComponent_H
#define CATIniSearchDashedCriterionComponent_H
#include "CATInteractiveInterfaces.h"
#include "IUnknown.h"
/**
 * Component linked to the @href CATIIniSearchDashedCriterion interface.
 * <b>Role:</b> This component implements the @href CATIIniSearchDashedCriterion 
 * interface.
 */
extern ExportedByCATInteractiveInterfaces CLSID CLSID_CATIniSearchDashedCriterionComponent;
//Pour plus tard en R11GA
//CATDeclareComponent(CATIniSearchDashedCriterionComponent);

#endif
