//=============================================================================
// COPYRIGHT DASSAULT SYSTEMES   2002
//=============================================================================
// 22 novembre 2002 - Cr�ation par JAC
//=============================================================================
/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */
#ifndef CATIniSearchLayerCriterionComponent_H
#define CATIniSearchLayerCriterionComponent_H
#include "CATInteractiveInterfaces.h"
#include "IUnknown.h"
/**
 * Component linked to the @href CATIIniSearchLayerCriterion interface.
 * <b>Role:</b> This component implements the @href CATIIniSearchLayerCriterion 
 * interface.
 */
extern ExportedByCATInteractiveInterfaces CLSID CLSID_CATIniSearchLayerCriterionComponent;
//Pour plus tard en R11GA
//CATDeclareComponent(CATIniSearchLayerCriterionComponent);

#endif
