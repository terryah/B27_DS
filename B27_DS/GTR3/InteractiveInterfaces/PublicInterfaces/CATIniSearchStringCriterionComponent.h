//=============================================================================
// COPYRIGHT DASSAULT SYSTEMES   2002
//=============================================================================
// 22 novembre 2002 - Cr�ation par JAC
//=============================================================================
/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */
#ifndef CATIniSearchStringCriterionComponent_H
#define CATIniSearchStringCriterionComponent_H
#include "CATInteractiveInterfaces.h"
#include "IUnknown.h"
/**
 * Component linked to the @href CATIIniSearchStringCriterion interface.
 * <b>Role:</b> This component implements the @href CATIIniSearchStringCriterion 
 * interface.
 */
extern ExportedByCATInteractiveInterfaces CLSID CLSID_CATIniSearchStringCriterionComponent;
//Pour plus tard en R11GA
//CATDeclareComponent(CATIniSearchStringCriterionComponent);

#endif
