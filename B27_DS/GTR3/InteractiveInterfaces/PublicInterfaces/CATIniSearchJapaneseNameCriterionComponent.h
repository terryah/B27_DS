//=============================================================================
// COPYRIGHT DASSAULT SYSTEMES   2007
//=============================================================================
// 28 novembre 2007 - Cr�ation par JFS
//=============================================================================
/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */
#ifndef CATIniSearchJapaneseNameCriterionComponent_H
#define CATIniSearchJapaneseNameCriterionComponent_H
#include "CATInteractiveInterfaces.h"
#include "IUnknown.h"
/**
 * Component linked to the CATIIniSearchJapaneseNameCriterion interface.
 * <b>Role:</b> This component implements the @href CATIIniSearchJapaneseNameCriterion 
 * interface.
 */
extern ExportedByCATInteractiveInterfaces CLSID CLSID_CATIniSearchJapaneseNameCriterionComponent;
//Pour plus tard en R11GA
//CATDeclareComponent(CATIniSearchJapaneseNameCriterionComponent);

#endif
