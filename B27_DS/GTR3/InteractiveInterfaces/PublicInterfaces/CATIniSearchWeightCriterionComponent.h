//=============================================================================
// COPYRIGHT DASSAULT SYSTEMES   2002
//=============================================================================
// 22 novembre 2002 - Cr�ation par JAC
//=============================================================================
/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */
#ifndef CATIniSearchWeightCriterionComponent_H
#define CATIniSearchWeightCriterionComponent_H
#include "CATInteractiveInterfaces.h"
#include "IUnknown.h"
/**
 * Component linked to the @href CATIIniSearchWeightCriterion interface.
 * <b>Role:</b> This component implements the @href CATIIniSearchWeightCriterion 
 * interface.
 */
extern ExportedByCATInteractiveInterfaces CLSID CLSID_CATIniSearchWeightCriterionComponent;
//Pour plus tard en R11GA
//CATDeclareComponent(CATIniSearchWeightCriterionComponent);

#endif
