//=============================================================================
// COPYRIGHT DASSAULT SYSTEMES   2002
//=============================================================================
// 22 novembre 2002 - Cr�ation par JAC
//=============================================================================
/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */
#ifndef CATIniSearchListingCriterionComponent_H
#define CATIniSearchListingCriterionComponent_H
#include "CATInteractiveInterfaces.h"
#include "IUnknown.h"
/**
 * Component linked to the @href CATIIniSearchListingCriterion interface.
 * <b>Role:</b> This component implements the @href CATIIniSearchListingCriterion 
 * interface.
 */
extern ExportedByCATInteractiveInterfaces CLSID CLSID_CATIniSearchListingCriterionComponent;
//Pour plus tard en R11GA
//CATDeclareComponent(CATIniSearchListingCriterionComponent);

#endif
