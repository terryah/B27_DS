//=============================================================================
// COPYRIGHT DASSAULT SYSTEMES   2002
//=============================================================================
// 22 novembre 2002 - Cr�ation par JAC
//=============================================================================
/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */
#ifndef CATIniSearchNameCriterionComponent_H
#define CATIniSearchNameCriterionComponent_H
#include "CATInteractiveInterfaces.h"
#include "IUnknown.h"
/**
 * Component linked to the @href CATIIniSearchNameCriterion interface.
 * <b>Role:</b> This component implements the @href CATIIniSearchNameCriterion 
 * interface.
 */
extern ExportedByCATInteractiveInterfaces CLSID CLSID_CATIniSearchNameCriterionComponent;
//Pour plus tard en R11GA
//CATDeclareComponent(CATIniSearchNameCriterionComponent);

#endif
