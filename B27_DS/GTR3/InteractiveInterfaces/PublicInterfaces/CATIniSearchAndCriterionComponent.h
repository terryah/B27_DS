//=============================================================================
// COPYRIGHT DASSAULT SYSTEMES   2002
//=============================================================================
// 22 novembre 2002 - Cr�ation par JAC
//=============================================================================
/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */
#ifndef CATIniSearchAndCriterionComponent_H
#define CATIniSearchAndCriterionComponent_H
#include "CATInteractiveInterfaces.h"
#include "IUnknown.h"
/**
 * Component linked to the @href CATIIniSearchAndCriterion interface.
 * <b>Role:</b> This component implements the @href CATIIniSearchAndCriterion 
 * interface.
 */
extern ExportedByCATInteractiveInterfaces CLSID CLSID_CATIniSearchAndCriterionComponent;
//Pour plus tard en R11GA
//CATDeclareComponent(CATIniSearchAndCriterionComponent);

#endif
