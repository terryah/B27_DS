//=============================================================================
// COPYRIGHT DASSAULT SYSTEMES   2007
//=============================================================================
// 28 novembre 2007 - Cr�ation par JFS
//=============================================================================
/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */
#ifndef CATIniSearchJapaneseGraphNameCriterionComponent_H
#define CATIniSearchJapaneseGraphNameCriterionComponent_H
#include "CATInteractiveInterfaces.h"
#include "IUnknown.h"
/**
 * Component linked to the CATIIniSearchJapaneseGraphNameCriterion interface.
 * <b>Role:</b> This component implements the @href CATIIniSearchJapaneseGraphNameCriterion 
 * interface.
 */
extern ExportedByCATInteractiveInterfaces CLSID CLSID_CATIniSearchJapaneseGraphNameCriterionComponent;
//Pour plus tard en R11GA
//CATDeclareComponent(CATIniSearchJapaneseGraphNameCriterionComponent);

#endif
