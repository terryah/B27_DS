//=============================================================================
// COPYRIGHT DASSAULT SYSTEMES   2002
//=============================================================================
// 22 novembre 2002 - Cr�ation par JAC
//=============================================================================
/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */
#ifndef CATIniSearchEngineComponent_H
#define CATIniSearchEngineComponent_H
#include "CATInteractiveInterfaces.h"
#include "IUnknown.h"
/**
 * Component linked to the @href CATIIniSearchEngine interface.
 * <b>Role:</b> This component implements the @href CATIIniSearchEngine 
 * interface.
 */
extern ExportedByCATInteractiveInterfaces CLSID CLSID_CATIniSearchEngineComponent;
//Pour plus tard en R11GA
//CATDeclareComponent(CATIniSearchEngineComponent);

#endif
