#ifndef __TIE_AUTIRteEngine
#define __TIE_AUTIRteEngine

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "AUTIRteEngine.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface AUTIRteEngine */
#define declare_TIE_AUTIRteEngine(classe) \
 \
 \
class TIEAUTIRteEngine##classe : public AUTIRteEngine \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(AUTIRteEngine, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT Initialization(const CATUnicodeString &iName, const CATBaseUnknown_var &ispBlock, const CATListOfCATUnicodeString &iParam) ; \
      virtual HRESULT SetTraceManager(const AUTIRteTraceManager_var &iTrace) ; \
      virtual HRESULT GetImplementationName(CATUnicodeString & oImplementationName) ; \
      virtual HRESULT GetName(CATUnicodeString &oName) ; \
      virtual HRESULT GetBlock(CATBaseUnknown_var &ospBlock) ; \
      virtual HRESULT GetDescription(CATUnicodeString &oDescription) ; \
      virtual HRESULT GetController(AUTIRteControl_var &ospControl) ; \
      virtual HRESULT GetIOManager(AUTIRteIO_var &ospIO) ; \
      virtual HRESULT GetDebugManager(AUTIRteDebug_var &ospDebug) ; \
      virtual HRESULT Close() ; \
};



#define ENVTIEdeclare_AUTIRteEngine(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT Initialization(const CATUnicodeString &iName, const CATBaseUnknown_var &ispBlock, const CATListOfCATUnicodeString &iParam) ; \
virtual HRESULT SetTraceManager(const AUTIRteTraceManager_var &iTrace) ; \
virtual HRESULT GetImplementationName(CATUnicodeString & oImplementationName) ; \
virtual HRESULT GetName(CATUnicodeString &oName) ; \
virtual HRESULT GetBlock(CATBaseUnknown_var &ospBlock) ; \
virtual HRESULT GetDescription(CATUnicodeString &oDescription) ; \
virtual HRESULT GetController(AUTIRteControl_var &ospControl) ; \
virtual HRESULT GetIOManager(AUTIRteIO_var &ospIO) ; \
virtual HRESULT GetDebugManager(AUTIRteDebug_var &ospDebug) ; \
virtual HRESULT Close() ; \


#define ENVTIEdefine_AUTIRteEngine(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT  ENVTIEName::Initialization(const CATUnicodeString &iName, const CATBaseUnknown_var &ispBlock, const CATListOfCATUnicodeString &iParam)  \
{ \
return (ENVTIECALL(AUTIRteEngine,ENVTIETypeLetter,ENVTIELetter)Initialization(iName,ispBlock,iParam)); \
} \
HRESULT  ENVTIEName::SetTraceManager(const AUTIRteTraceManager_var &iTrace)  \
{ \
return (ENVTIECALL(AUTIRteEngine,ENVTIETypeLetter,ENVTIELetter)SetTraceManager(iTrace)); \
} \
HRESULT  ENVTIEName::GetImplementationName(CATUnicodeString & oImplementationName)  \
{ \
return (ENVTIECALL(AUTIRteEngine,ENVTIETypeLetter,ENVTIELetter)GetImplementationName(oImplementationName)); \
} \
HRESULT  ENVTIEName::GetName(CATUnicodeString &oName)  \
{ \
return (ENVTIECALL(AUTIRteEngine,ENVTIETypeLetter,ENVTIELetter)GetName(oName)); \
} \
HRESULT  ENVTIEName::GetBlock(CATBaseUnknown_var &ospBlock)  \
{ \
return (ENVTIECALL(AUTIRteEngine,ENVTIETypeLetter,ENVTIELetter)GetBlock(ospBlock)); \
} \
HRESULT  ENVTIEName::GetDescription(CATUnicodeString &oDescription)  \
{ \
return (ENVTIECALL(AUTIRteEngine,ENVTIETypeLetter,ENVTIELetter)GetDescription(oDescription)); \
} \
HRESULT  ENVTIEName::GetController(AUTIRteControl_var &ospControl)  \
{ \
return (ENVTIECALL(AUTIRteEngine,ENVTIETypeLetter,ENVTIELetter)GetController(ospControl)); \
} \
HRESULT  ENVTIEName::GetIOManager(AUTIRteIO_var &ospIO)  \
{ \
return (ENVTIECALL(AUTIRteEngine,ENVTIETypeLetter,ENVTIELetter)GetIOManager(ospIO)); \
} \
HRESULT  ENVTIEName::GetDebugManager(AUTIRteDebug_var &ospDebug)  \
{ \
return (ENVTIECALL(AUTIRteEngine,ENVTIETypeLetter,ENVTIELetter)GetDebugManager(ospDebug)); \
} \
HRESULT  ENVTIEName::Close()  \
{ \
return (ENVTIECALL(AUTIRteEngine,ENVTIETypeLetter,ENVTIELetter)Close()); \
} \


/* Name of the TIE class */
#define class_TIE_AUTIRteEngine(classe)    TIEAUTIRteEngine##classe


/* Common methods inside a TIE */
#define common_TIE_AUTIRteEngine(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(AUTIRteEngine, classe) \
 \
 \
CATImplementTIEMethods(AUTIRteEngine, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(AUTIRteEngine, classe, 1) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(AUTIRteEngine, classe) \
CATImplementCATBaseUnknownMethodsForTIE(AUTIRteEngine, classe) \
 \
HRESULT  TIEAUTIRteEngine##classe::Initialization(const CATUnicodeString &iName, const CATBaseUnknown_var &ispBlock, const CATListOfCATUnicodeString &iParam)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Initialization(iName,ispBlock,iParam)); \
} \
HRESULT  TIEAUTIRteEngine##classe::SetTraceManager(const AUTIRteTraceManager_var &iTrace)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetTraceManager(iTrace)); \
} \
HRESULT  TIEAUTIRteEngine##classe::GetImplementationName(CATUnicodeString & oImplementationName)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetImplementationName(oImplementationName)); \
} \
HRESULT  TIEAUTIRteEngine##classe::GetName(CATUnicodeString &oName)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetName(oName)); \
} \
HRESULT  TIEAUTIRteEngine##classe::GetBlock(CATBaseUnknown_var &ospBlock)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetBlock(ospBlock)); \
} \
HRESULT  TIEAUTIRteEngine##classe::GetDescription(CATUnicodeString &oDescription)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetDescription(oDescription)); \
} \
HRESULT  TIEAUTIRteEngine##classe::GetController(AUTIRteControl_var &ospControl)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetController(ospControl)); \
} \
HRESULT  TIEAUTIRteEngine##classe::GetIOManager(AUTIRteIO_var &ospIO)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetIOManager(ospIO)); \
} \
HRESULT  TIEAUTIRteEngine##classe::GetDebugManager(AUTIRteDebug_var &ospDebug)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetDebugManager(ospDebug)); \
} \
HRESULT  TIEAUTIRteEngine##classe::Close()  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Close()); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_AUTIRteEngine(classe) \
 \
 \
declare_TIE_AUTIRteEngine(classe) \
 \
 \
CATMetaClass * __stdcall TIEAUTIRteEngine##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_AUTIRteEngine,"AUTIRteEngine",AUTIRteEngine::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_AUTIRteEngine(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(AUTIRteEngine, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicAUTIRteEngine##classe(classe::MetaObject(),AUTIRteEngine::MetaObject(),(void *)CreateTIEAUTIRteEngine##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_AUTIRteEngine(classe) \
 \
 \
declare_TIE_AUTIRteEngine(classe) \
 \
 \
CATMetaClass * __stdcall TIEAUTIRteEngine##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_AUTIRteEngine,"AUTIRteEngine",AUTIRteEngine::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_AUTIRteEngine(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(AUTIRteEngine, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicAUTIRteEngine##classe(classe::MetaObject(),AUTIRteEngine::MetaObject(),(void *)CreateTIEAUTIRteEngine##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_AUTIRteEngine(classe) TIE_AUTIRteEngine(classe)
#else
#define BOA_AUTIRteEngine(classe) CATImplementBOA(AUTIRteEngine, classe)
#endif

#endif
