#ifndef __TIE_AUTIRteControl
#define __TIE_AUTIRteControl

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "AUTIRteControl.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface AUTIRteControl */
#define declare_TIE_AUTIRteControl(classe) \
 \
 \
class TIEAUTIRteControl##classe : public AUTIRteControl \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(AUTIRteControl, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT Initialization(const CATListOfCATUnicodeString &iParam) ; \
      virtual HRESULT SetCycleTime(const int &iCycleTime, const CATBoolean &iRealTime) ; \
      virtual HRESULT GetCycleTime(int &oCycleTime, CATBoolean &oRealTime) ; \
      virtual HRESULT Run1Cycle(CATBoolean &oEnd) ; \
      virtual HRESULT IsActive(CATBoolean &oActive) ; \
      virtual HRESULT Suspend() ; \
      virtual HRESULT Resume() ; \
      virtual HRESULT Reset() ; \
      virtual HRESULT Close() ; \
};



#define ENVTIEdeclare_AUTIRteControl(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT Initialization(const CATListOfCATUnicodeString &iParam) ; \
virtual HRESULT SetCycleTime(const int &iCycleTime, const CATBoolean &iRealTime) ; \
virtual HRESULT GetCycleTime(int &oCycleTime, CATBoolean &oRealTime) ; \
virtual HRESULT Run1Cycle(CATBoolean &oEnd) ; \
virtual HRESULT IsActive(CATBoolean &oActive) ; \
virtual HRESULT Suspend() ; \
virtual HRESULT Resume() ; \
virtual HRESULT Reset() ; \
virtual HRESULT Close() ; \


#define ENVTIEdefine_AUTIRteControl(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT  ENVTIEName::Initialization(const CATListOfCATUnicodeString &iParam)  \
{ \
return (ENVTIECALL(AUTIRteControl,ENVTIETypeLetter,ENVTIELetter)Initialization(iParam)); \
} \
HRESULT  ENVTIEName::SetCycleTime(const int &iCycleTime, const CATBoolean &iRealTime)  \
{ \
return (ENVTIECALL(AUTIRteControl,ENVTIETypeLetter,ENVTIELetter)SetCycleTime(iCycleTime,iRealTime)); \
} \
HRESULT  ENVTIEName::GetCycleTime(int &oCycleTime, CATBoolean &oRealTime)  \
{ \
return (ENVTIECALL(AUTIRteControl,ENVTIETypeLetter,ENVTIELetter)GetCycleTime(oCycleTime,oRealTime)); \
} \
HRESULT  ENVTIEName::Run1Cycle(CATBoolean &oEnd)  \
{ \
return (ENVTIECALL(AUTIRteControl,ENVTIETypeLetter,ENVTIELetter)Run1Cycle(oEnd)); \
} \
HRESULT  ENVTIEName::IsActive(CATBoolean &oActive)  \
{ \
return (ENVTIECALL(AUTIRteControl,ENVTIETypeLetter,ENVTIELetter)IsActive(oActive)); \
} \
HRESULT  ENVTIEName::Suspend()  \
{ \
return (ENVTIECALL(AUTIRteControl,ENVTIETypeLetter,ENVTIELetter)Suspend()); \
} \
HRESULT  ENVTIEName::Resume()  \
{ \
return (ENVTIECALL(AUTIRteControl,ENVTIETypeLetter,ENVTIELetter)Resume()); \
} \
HRESULT  ENVTIEName::Reset()  \
{ \
return (ENVTIECALL(AUTIRteControl,ENVTIETypeLetter,ENVTIELetter)Reset()); \
} \
HRESULT  ENVTIEName::Close()  \
{ \
return (ENVTIECALL(AUTIRteControl,ENVTIETypeLetter,ENVTIELetter)Close()); \
} \


/* Name of the TIE class */
#define class_TIE_AUTIRteControl(classe)    TIEAUTIRteControl##classe


/* Common methods inside a TIE */
#define common_TIE_AUTIRteControl(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(AUTIRteControl, classe) \
 \
 \
CATImplementTIEMethods(AUTIRteControl, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(AUTIRteControl, classe, 1) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(AUTIRteControl, classe) \
CATImplementCATBaseUnknownMethodsForTIE(AUTIRteControl, classe) \
 \
HRESULT  TIEAUTIRteControl##classe::Initialization(const CATListOfCATUnicodeString &iParam)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Initialization(iParam)); \
} \
HRESULT  TIEAUTIRteControl##classe::SetCycleTime(const int &iCycleTime, const CATBoolean &iRealTime)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetCycleTime(iCycleTime,iRealTime)); \
} \
HRESULT  TIEAUTIRteControl##classe::GetCycleTime(int &oCycleTime, CATBoolean &oRealTime)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetCycleTime(oCycleTime,oRealTime)); \
} \
HRESULT  TIEAUTIRteControl##classe::Run1Cycle(CATBoolean &oEnd)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Run1Cycle(oEnd)); \
} \
HRESULT  TIEAUTIRteControl##classe::IsActive(CATBoolean &oActive)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->IsActive(oActive)); \
} \
HRESULT  TIEAUTIRteControl##classe::Suspend()  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Suspend()); \
} \
HRESULT  TIEAUTIRteControl##classe::Resume()  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Resume()); \
} \
HRESULT  TIEAUTIRteControl##classe::Reset()  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Reset()); \
} \
HRESULT  TIEAUTIRteControl##classe::Close()  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Close()); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_AUTIRteControl(classe) \
 \
 \
declare_TIE_AUTIRteControl(classe) \
 \
 \
CATMetaClass * __stdcall TIEAUTIRteControl##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_AUTIRteControl,"AUTIRteControl",AUTIRteControl::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_AUTIRteControl(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(AUTIRteControl, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicAUTIRteControl##classe(classe::MetaObject(),AUTIRteControl::MetaObject(),(void *)CreateTIEAUTIRteControl##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_AUTIRteControl(classe) \
 \
 \
declare_TIE_AUTIRteControl(classe) \
 \
 \
CATMetaClass * __stdcall TIEAUTIRteControl##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_AUTIRteControl,"AUTIRteControl",AUTIRteControl::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_AUTIRteControl(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(AUTIRteControl, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicAUTIRteControl##classe(classe::MetaObject(),AUTIRteControl::MetaObject(),(void *)CreateTIEAUTIRteControl##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_AUTIRteControl(classe) TIE_AUTIRteControl(classe)
#else
#define BOA_AUTIRteControl(classe) CATImplementBOA(AUTIRteControl, classe)
#endif

#endif
