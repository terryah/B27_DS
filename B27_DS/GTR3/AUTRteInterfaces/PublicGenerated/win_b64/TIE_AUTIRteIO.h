#ifndef __TIE_AUTIRteIO
#define __TIE_AUTIRteIO

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "AUTIRteIO.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface AUTIRteIO */
#define declare_TIE_AUTIRteIO(classe) \
 \
 \
class TIEAUTIRteIO##classe : public AUTIRteIO \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(AUTIRteIO, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT Initialization(const CATListOfCATUnicodeString &iParam, CATListValAUTIRteIOData_var &ioInputsList, CATListValAUTIRteIOData_var &ioOutputsList) ; \
      virtual HRESULT GetInputs(CATListValAUTIRteIOData_var &iList) ; \
      virtual HRESULT SetInputs(CATListValAUTIRteIOData_var &iList) ; \
      virtual HRESULT GetOutputs(CATListValAUTIRteIOData_var &iList) ; \
      virtual HRESULT Close() ; \
};



#define ENVTIEdeclare_AUTIRteIO(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT Initialization(const CATListOfCATUnicodeString &iParam, CATListValAUTIRteIOData_var &ioInputsList, CATListValAUTIRteIOData_var &ioOutputsList) ; \
virtual HRESULT GetInputs(CATListValAUTIRteIOData_var &iList) ; \
virtual HRESULT SetInputs(CATListValAUTIRteIOData_var &iList) ; \
virtual HRESULT GetOutputs(CATListValAUTIRteIOData_var &iList) ; \
virtual HRESULT Close() ; \


#define ENVTIEdefine_AUTIRteIO(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT  ENVTIEName::Initialization(const CATListOfCATUnicodeString &iParam, CATListValAUTIRteIOData_var &ioInputsList, CATListValAUTIRteIOData_var &ioOutputsList)  \
{ \
return (ENVTIECALL(AUTIRteIO,ENVTIETypeLetter,ENVTIELetter)Initialization(iParam,ioInputsList,ioOutputsList)); \
} \
HRESULT  ENVTIEName::GetInputs(CATListValAUTIRteIOData_var &iList)  \
{ \
return (ENVTIECALL(AUTIRteIO,ENVTIETypeLetter,ENVTIELetter)GetInputs(iList)); \
} \
HRESULT  ENVTIEName::SetInputs(CATListValAUTIRteIOData_var &iList)  \
{ \
return (ENVTIECALL(AUTIRteIO,ENVTIETypeLetter,ENVTIELetter)SetInputs(iList)); \
} \
HRESULT  ENVTIEName::GetOutputs(CATListValAUTIRteIOData_var &iList)  \
{ \
return (ENVTIECALL(AUTIRteIO,ENVTIETypeLetter,ENVTIELetter)GetOutputs(iList)); \
} \
HRESULT  ENVTIEName::Close()  \
{ \
return (ENVTIECALL(AUTIRteIO,ENVTIETypeLetter,ENVTIELetter)Close()); \
} \


/* Name of the TIE class */
#define class_TIE_AUTIRteIO(classe)    TIEAUTIRteIO##classe


/* Common methods inside a TIE */
#define common_TIE_AUTIRteIO(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(AUTIRteIO, classe) \
 \
 \
CATImplementTIEMethods(AUTIRteIO, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(AUTIRteIO, classe, 1) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(AUTIRteIO, classe) \
CATImplementCATBaseUnknownMethodsForTIE(AUTIRteIO, classe) \
 \
HRESULT  TIEAUTIRteIO##classe::Initialization(const CATListOfCATUnicodeString &iParam, CATListValAUTIRteIOData_var &ioInputsList, CATListValAUTIRteIOData_var &ioOutputsList)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Initialization(iParam,ioInputsList,ioOutputsList)); \
} \
HRESULT  TIEAUTIRteIO##classe::GetInputs(CATListValAUTIRteIOData_var &iList)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetInputs(iList)); \
} \
HRESULT  TIEAUTIRteIO##classe::SetInputs(CATListValAUTIRteIOData_var &iList)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetInputs(iList)); \
} \
HRESULT  TIEAUTIRteIO##classe::GetOutputs(CATListValAUTIRteIOData_var &iList)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetOutputs(iList)); \
} \
HRESULT  TIEAUTIRteIO##classe::Close()  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Close()); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_AUTIRteIO(classe) \
 \
 \
declare_TIE_AUTIRteIO(classe) \
 \
 \
CATMetaClass * __stdcall TIEAUTIRteIO##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_AUTIRteIO,"AUTIRteIO",AUTIRteIO::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_AUTIRteIO(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(AUTIRteIO, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicAUTIRteIO##classe(classe::MetaObject(),AUTIRteIO::MetaObject(),(void *)CreateTIEAUTIRteIO##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_AUTIRteIO(classe) \
 \
 \
declare_TIE_AUTIRteIO(classe) \
 \
 \
CATMetaClass * __stdcall TIEAUTIRteIO##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_AUTIRteIO,"AUTIRteIO",AUTIRteIO::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_AUTIRteIO(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(AUTIRteIO, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicAUTIRteIO##classe(classe::MetaObject(),AUTIRteIO::MetaObject(),(void *)CreateTIEAUTIRteIO##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_AUTIRteIO(classe) TIE_AUTIRteIO(classe)
#else
#define BOA_AUTIRteIO(classe) CATImplementBOA(AUTIRteIO, classe)
#endif

#endif
