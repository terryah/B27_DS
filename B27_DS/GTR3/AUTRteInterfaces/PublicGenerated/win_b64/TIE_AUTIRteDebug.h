#ifndef __TIE_AUTIRteDebug
#define __TIE_AUTIRteDebug

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "AUTIRteDebug.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface AUTIRteDebug */
#define declare_TIE_AUTIRteDebug(classe) \
 \
 \
class TIEAUTIRteDebug##classe : public AUTIRteDebug \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(AUTIRteDebug, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT Initialization(const CATListOfCATUnicodeString &iParam) ; \
      virtual __declspec(deprecated) HRESULT GetApplicationInfo(AppInfo &oAppInfo) ; \
      virtual HRESULT GetApplicationInfo(ApplicationInfo &oAppInfo) ; \
      virtual __declspec(deprecated) HRESULT GetApplicationData(AppData &oAppData) ; \
      virtual HRESULT GetApplicationData(ApplicationData &oAppData) ; \
      virtual __declspec(deprecated) HRESULT SetApplicationData(AppData &oAppData) ; \
      virtual HRESULT SetApplicationData(const ApplicationData &iAppData) ; \
      virtual HRESULT SetForcings(CATListValAUTIRteDebugForcing_var &iList) ; \
      virtual HRESULT SetBreakPoints(CATListValAUTIRteDebugBreakPoint_var & iList) ; \
      virtual HRESULT Close() ; \
};



#define ENVTIEdeclare_AUTIRteDebug(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT Initialization(const CATListOfCATUnicodeString &iParam) ; \
virtual __declspec(deprecated) HRESULT GetApplicationInfo(AppInfo &oAppInfo) ; \
virtual HRESULT GetApplicationInfo(ApplicationInfo &oAppInfo) ; \
virtual __declspec(deprecated) HRESULT GetApplicationData(AppData &oAppData) ; \
virtual HRESULT GetApplicationData(ApplicationData &oAppData) ; \
virtual __declspec(deprecated) HRESULT SetApplicationData(AppData &oAppData) ; \
virtual HRESULT SetApplicationData(const ApplicationData &iAppData) ; \
virtual HRESULT SetForcings(CATListValAUTIRteDebugForcing_var &iList) ; \
virtual HRESULT SetBreakPoints(CATListValAUTIRteDebugBreakPoint_var & iList) ; \
virtual HRESULT Close() ; \


#define ENVTIEdefine_AUTIRteDebug(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT  ENVTIEName::Initialization(const CATListOfCATUnicodeString &iParam)  \
{ \
return (ENVTIECALL(AUTIRteDebug,ENVTIETypeLetter,ENVTIELetter)Initialization(iParam)); \
} \
__declspec(deprecated) HRESULT  ENVTIEName::GetApplicationInfo(AppInfo &oAppInfo)  \
{ \
return (ENVTIECALL(AUTIRteDebug,ENVTIETypeLetter,ENVTIELetter)GetApplicationInfo(oAppInfo)); \
} \
HRESULT  ENVTIEName::GetApplicationInfo(ApplicationInfo &oAppInfo)  \
{ \
return (ENVTIECALL(AUTIRteDebug,ENVTIETypeLetter,ENVTIELetter)GetApplicationInfo(oAppInfo)); \
} \
__declspec(deprecated) HRESULT  ENVTIEName::GetApplicationData(AppData &oAppData)  \
{ \
return (ENVTIECALL(AUTIRteDebug,ENVTIETypeLetter,ENVTIELetter)GetApplicationData(oAppData)); \
} \
HRESULT  ENVTIEName::GetApplicationData(ApplicationData &oAppData)  \
{ \
return (ENVTIECALL(AUTIRteDebug,ENVTIETypeLetter,ENVTIELetter)GetApplicationData(oAppData)); \
} \
__declspec(deprecated) HRESULT  ENVTIEName::SetApplicationData(AppData &oAppData)  \
{ \
return (ENVTIECALL(AUTIRteDebug,ENVTIETypeLetter,ENVTIELetter)SetApplicationData(oAppData)); \
} \
HRESULT  ENVTIEName::SetApplicationData(const ApplicationData &iAppData)  \
{ \
return (ENVTIECALL(AUTIRteDebug,ENVTIETypeLetter,ENVTIELetter)SetApplicationData(iAppData)); \
} \
HRESULT  ENVTIEName::SetForcings(CATListValAUTIRteDebugForcing_var &iList)  \
{ \
return (ENVTIECALL(AUTIRteDebug,ENVTIETypeLetter,ENVTIELetter)SetForcings(iList)); \
} \
HRESULT  ENVTIEName::SetBreakPoints(CATListValAUTIRteDebugBreakPoint_var & iList)  \
{ \
return (ENVTIECALL(AUTIRteDebug,ENVTIETypeLetter,ENVTIELetter)SetBreakPoints(iList)); \
} \
HRESULT  ENVTIEName::Close()  \
{ \
return (ENVTIECALL(AUTIRteDebug,ENVTIETypeLetter,ENVTIELetter)Close()); \
} \


/* Name of the TIE class */
#define class_TIE_AUTIRteDebug(classe)    TIEAUTIRteDebug##classe


/* Common methods inside a TIE */
#define common_TIE_AUTIRteDebug(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(AUTIRteDebug, classe) \
 \
 \
CATImplementTIEMethods(AUTIRteDebug, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(AUTIRteDebug, classe, 1) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(AUTIRteDebug, classe) \
CATImplementCATBaseUnknownMethodsForTIE(AUTIRteDebug, classe) \
 \
HRESULT  TIEAUTIRteDebug##classe::Initialization(const CATListOfCATUnicodeString &iParam)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Initialization(iParam)); \
} \
__declspec(deprecated) HRESULT  TIEAUTIRteDebug##classe::GetApplicationInfo(AppInfo &oAppInfo)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetApplicationInfo(oAppInfo)); \
} \
HRESULT  TIEAUTIRteDebug##classe::GetApplicationInfo(ApplicationInfo &oAppInfo)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetApplicationInfo(oAppInfo)); \
} \
__declspec(deprecated) HRESULT  TIEAUTIRteDebug##classe::GetApplicationData(AppData &oAppData)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetApplicationData(oAppData)); \
} \
HRESULT  TIEAUTIRteDebug##classe::GetApplicationData(ApplicationData &oAppData)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetApplicationData(oAppData)); \
} \
__declspec(deprecated) HRESULT  TIEAUTIRteDebug##classe::SetApplicationData(AppData &oAppData)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetApplicationData(oAppData)); \
} \
HRESULT  TIEAUTIRteDebug##classe::SetApplicationData(const ApplicationData &iAppData)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetApplicationData(iAppData)); \
} \
HRESULT  TIEAUTIRteDebug##classe::SetForcings(CATListValAUTIRteDebugForcing_var &iList)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetForcings(iList)); \
} \
HRESULT  TIEAUTIRteDebug##classe::SetBreakPoints(CATListValAUTIRteDebugBreakPoint_var & iList)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetBreakPoints(iList)); \
} \
HRESULT  TIEAUTIRteDebug##classe::Close()  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Close()); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_AUTIRteDebug(classe) \
 \
 \
declare_TIE_AUTIRteDebug(classe) \
 \
 \
CATMetaClass * __stdcall TIEAUTIRteDebug##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_AUTIRteDebug,"AUTIRteDebug",AUTIRteDebug::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_AUTIRteDebug(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(AUTIRteDebug, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicAUTIRteDebug##classe(classe::MetaObject(),AUTIRteDebug::MetaObject(),(void *)CreateTIEAUTIRteDebug##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_AUTIRteDebug(classe) \
 \
 \
declare_TIE_AUTIRteDebug(classe) \
 \
 \
CATMetaClass * __stdcall TIEAUTIRteDebug##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_AUTIRteDebug,"AUTIRteDebug",AUTIRteDebug::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_AUTIRteDebug(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(AUTIRteDebug, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicAUTIRteDebug##classe(classe::MetaObject(),AUTIRteDebug::MetaObject(),(void *)CreateTIEAUTIRteDebug##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_AUTIRteDebug(classe) TIE_AUTIRteDebug(classe)
#else
#define BOA_AUTIRteDebug(classe) CATImplementBOA(AUTIRteDebug, classe)
#endif

#endif
