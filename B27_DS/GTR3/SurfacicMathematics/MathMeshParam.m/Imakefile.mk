# COPYRIGHT DASSAULT SYSTEMES 2003
#======================================================================
# Imakefile for module MathMeshParam.m
#======================================================================
#
#  Jul 2004  : amr : Optimisation O2
#  Feb 2003  Creation module CATParamaterization.m:llr
#  Feb 2003  ktu : Localisation dans MathMeshParam.m
#======================================================================
#
# SHARED LIBRARY 
#
BUILT_OBJECT_TYPE = NONE
 
#LINK_WITH = JS0GROUP CATMathematics

# System dependant variables
#
OS = AIX
#
OS = HP-UX
#
OS = IRIX
#
OS = SunOS
#
OS = Windows_NT
#if os win_b64
#else
OPTIMIZATION_CPP = /O2
#endif
