#@ autoformat 11:12:06

BUILT_OBJECT_TYPE = SHARED LIBRARY

LINK_WITH = \
    CATIAApplicationFrame          \ # CATIAApplicationFrame          CATIAApplicationFrame
    CATDraftingInterfaces          \ # DraftingInterfaces             CATDraftingInterfaces
    CATInteractiveInterfaces       \ # InteractiveInterfaces          CATInteractiveInterfaces
    KnowledgeItf                   \ # KnowledgeInterfaces            KnowledgeItf
    CATLiteralFeatures             \ # LiteralFeatures                CATLiteralFeatures
    CATMathematics                 \ # Mathematics                    CATMathematics
    CATObjectModelerBase           \ # ObjectModelerBase              CATObjectModelerBase
    CATObjectSpecsModeler          \ # ObjectSpecsModeler             CATObjectSpecsModeler
    CATPrint                       \ # Print                          CATPrint
    CATPrtBase                     \ # PrintBase                      CATPrt
    JS0FM                          \ # System                         JS0FM
    JS0GROUP                       \ # System                         JS0GROUP
    SystemUUID                     \ # System                         SystemUUID
    CATVisualization               \
    CATLayoutInfraCatalog
#
INCLUDED_MODULES = \
    CATAnnInfraBase \
    CATAnnIndicator \
#
