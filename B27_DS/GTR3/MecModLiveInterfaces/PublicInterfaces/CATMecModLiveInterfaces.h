// COPYRIGHT DASSAULT SYSTEMES  2008
/** @CAA2Required */
//**********************************************************************
//* DON T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS *
//* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPEAR AT ANY TIME *
//**********************************************************************

#ifndef __CATMecModLiveInterfaces_h__
#define __CATMecModLiveInterfaces_h__
#ifdef _WINDOWS_SOURCE
#ifdef  __CATMecModLiveInterfaces
#define ExportedByCATMecModLiveInterfaces    __declspec(dllexport)
#else
#define ExportedByCATMecModLiveInterfaces    __declspec(dllimport)
#endif
#else
#define ExportedByCATMecModLiveInterfaces
#endif

#endif

