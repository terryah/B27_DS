# COPYRIGHT DASSAULT SYSTEMES 2004
#======================================================================
# Imakefile for module CATAuiHandle.m
#======================================================================
#
#  Aug 2004  Creation: Code generated by the CAA wizard  bqg
#======================================================================

BUILT_OBJECT_TYPE = NONE

LINK_WITH_V5_ONLY=
LINK_WITH_V6_ONLY=

LINK_WITH = 
    $(LINK_WITH_V5_ONLY)           \
    $(LINK_WITH_V6_ONLY)           \

OS = COMMON
#if defined(CATIAR201)
LINK_WITH_V6_ONLY= \
#
#else
#if defined(CATIAV5R21)
LINK_WITH_V5_ONLY= \
#
#endif
#endif
