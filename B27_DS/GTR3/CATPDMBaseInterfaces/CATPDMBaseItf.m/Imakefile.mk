# COPYRIGHT DASSAULT SYSTEMES 2001
#======================================================================
# Imakefile for module CATPDMBaseItf.m 
#======================================================================
#
#  Nov 2001  Creation: Code generated by the CAA wizard  JRZ
#======================================================================
#
# SHARED LIBRARY 
#
BUILT_OBJECT_TYPE=SHARED LIBRARY
 
# DO NOT EDIT :: THE CAA2 WIZARDS WILL ADD CODE HERE
WIZARD_LINK_MODULES =  \
JS0FM JS0GROUP CATPDMBaseInterfacesUUID CATObjectModelerBase
# END WIZARD EDITION ZONE

LINK_WITH = $(WIZARD_LINK_MODULES)

INCLUDED_MODULES = \
                  CATPDMBaseItfCPP \
                  CATPDMBasePubIDL \
                  CATPDMBaseProIDL \
