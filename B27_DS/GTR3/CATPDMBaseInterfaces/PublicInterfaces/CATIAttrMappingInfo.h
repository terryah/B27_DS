// COPYRIGHT Dassault Systemes 2003
//===================================================================
//
// CATIAttrMappingInfo.h
// Define the CATIAttrMappingInfo interface
//
//===================================================================
//
// Usage notes:
//   New interface: describe its use here
//
//===================================================================
//
//  May 2003  Creation: Code generated by the CAA wizard  aih
//===================================================================
#ifndef CATIAttrMappingInfo_H
#define CATIAttrMappingInfo_H

#include "CATPDMBaseItfCPP.h"
#include "CATEnoAttrMappingEnumDef.h"

// --- System ---
#include "CATBaseUnknown.h"

// --- ObjectModelerBase --- 
#include "sequence_octet.h"

class CATUnicodeString;

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByCATPDMBaseItfCPP IID IID_CATIAttrMappingInfo;
#else
extern "C" const IID IID_CATIAttrMappingInfo ;
#endif

//------------------------------------------------------------------

/**
 * @CAA2Level L1
 * @CAA2Usage U3
 *
 * Describe the functionality of your interface here
 * <p>
 * Using this prefered syntax will enable mkdoc to document your class.
 */

/**
 * Interface to be used by user code to get the infomation regarding 
 *   1./ CATIA or ENOVIA part 
 *   2./ Details of attribute like name, type etc.
 * User code will receive the pointer to CATIAttrMappingInfo in save/import 
 * process. User can query the varioue info (listed below) by using the pointer
 * <b>Role</b>: This is an input interface for the user code in attribute 
 * mapping process. 
 **/
class ExportedByCATPDMBaseItfCPP CATIAttrMappingInfo: public CATBaseUnknown
{
  CATDeclareInterface;

  public:

  /**
   * Returns attribute name
   *   @param oAttrName
   *      User will get attribute name in oAttrName parameter
   *   @return HRESULT
   *      Error code of function.
   *		  S_OK   : Success
   *      E_FAIL : Error
   */
  virtual HRESULT GetAttributeName(CATUnicodeString& oAttrName)                           = 0;

  /**
   * Returns type of attribute
   *   @param oAttrType
   *      User will get attribute type in oAttrType parameter
   *   @return HRESULT
   *      Error code of function.
   *		  S_OK   : Success
   *      E_FAIL : Error
   */
  virtual HRESULT GetTypeOfAttribue(CATEnoAttrMappingEnumDef::AttributeTypes& oAttrType)  = 0;

  
  /**
   * Function used while saving the part from CATIA to ENOVIA
   * Returns attribute name in CATIA. 
   *   @param oAttrCXName
   *      User will get name of the attribute in CATIA
   *   @return HRESULT
   *      Error code of function.
   *		  S_OK   : Success
   *      E_FAIL : Error
   */
  virtual HRESULT GetAttrCXName(CATUnicodeString& oAttrCXName)                            = 0;


  /**
   * Function used while saving the part from CATIA to ENOVIA
   * Returns part pointer. User code then query the return pointer for 
   * neccessary information regarding part.
   *   @param opIUnkOfPart
   *      User will get part pointer in opIUnkOfPart
   *   @return HRESULT
   *      Error code of function.
   *		  S_OK   : Success
   *      E_FAIL : Error
   */
  virtual HRESULT GetPartPointer(CATBaseUnknown*& opIUnkOfPart)                           = 0;

  
  /**
   * Function used while importing the part from ENOVIA to CATIA
   * Returns attribute name in ENOVIA
   *   @param oAttrDBName
   *      User will get name of the attribute in ENOVIA in oAttrDBName
   *   @return HRESULT
   *      Error code of function.
   *		  S_OK   : Success
   *      E_FAIL : Error
   */
  virtual HRESULT GetAttrDBName(CATUnicodeString& oAttrDBName)                            = 0;

  /**
   * Function used while importing the part from ENOVIA to CATIA
   * Returns uuid of the ENOVIA entity
   *   @param oUuid
   *      User will get uuid in oUuid
   *   @return HRESULT
   *      Error code of function.
   *		  S_OK   : Success
   *      E_FAIL : Error
   */
  virtual HRESULT GetDBUuid(SEQUENCE(octet)& oUuid)                                       = 0;

  /**
   * Function used while importing the part from ENOVIA to CATIA
   * This function will return the existing value of the of the attribute in 
   * ENOVIA. This is an optimisation. User need not go to database to query 
   * the existing value of the ENOVIA entity. User should first query for type 
   * of attribute using the function GetTypeOfAttribue and depending on the 
   * output of GetTypeOfAttribue it should call the proper overload version of 
   * GetExistingUEAttValFromDB
   *   @param oValue
   *      User will get existing interger value in ENOVIA in oValue
   *   @return HRESULT
   *      Error code of function.
   *		  S_OK   : Success
   *      E_FAIL : Error
   */
  virtual HRESULT GetExistingUEAttValFromDB( int& oValue )                                = 0;

  /**
   * Function used while importing the part from ENOVIA to CATIA
   * This function will return the existing value of the of the attribute in 
   * ENOVIA. This is an optimisation. User need not go to database to query 
   * the existing value of the ENOVIA entity. User should first query for type 
   * of attribute using the function GetTypeOfAttribue and depending on the 
   * output of GetTypeOfAttribue it should call the proper overload version of 
   * GetExistingUEAttValFromDB
   *   @param oValue
   *      User will get existing double value in ENOVIA in oValue
   *   @return HRESULT
   *      Error code of function.
   *		  S_OK   : Success
   *      E_FAIL : Error
   */
  virtual HRESULT GetExistingUEAttValFromDB( double& oValue )                             = 0;

   /**
   * Function used while importing the part from ENOVIA to CATIA
   * This function will return the existing value of the of the attribute in 
   * ENOVIA. This is an optimisation. User need not go to database to query 
   * the existing value of the ENOVIA entity. User should first query for type 
   * of attribute using the function GetTypeOfAttribue and depending on the 
   * output of GetTypeOfAttribue it should call the proper overload version of 
   * GetExistingUEAttValFromDB
   *   @param oValue
   *      User will get existing string value in ENOVIA in oValue
   *   @return HRESULT
   *      Error code of function.
   *		  S_OK   : Success
   *      E_FAIL : Error
   */
  virtual HRESULT GetExistingUEAttValFromDB( CATUnicodeString& oValue )                   = 0;

  /**
   * Function used while importing the part from ENOVIA to CATIA
   * This function will return the existing value of the of the attribute in 
   * ENOVIA. This is an optimisation. User need not go to database to query 
   * the existing value of the ENOVIA entity. User should first query for type 
   * of attribute using the function GetTypeOfAttribue and depending on the 
   * output of GetTypeOfAttribue it should call the proper overload version of 
   * GetExistingUEAttValFromDB
   *   @param oValue
   *      User will get existing boolean value in ENOVIA in oValue
   *   @return HRESULT
   *      Error code of function.
   *		  S_OK   : Success
   *      E_FAIL : Error
   */
  virtual HRESULT GetExistingUEAttValFromDB( CATBoolean& oValue )                         = 0;
};

//------------------------------------------------------------------

#endif
