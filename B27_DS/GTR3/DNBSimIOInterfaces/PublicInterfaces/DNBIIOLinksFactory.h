// COPYRIGHT Dassault Systemes 2001
//===================================================================
//
// DNBIIOLinksFactory.h
// Define the DNBIIOLinksFactory interface
//
//===================================================================
//
// Usage notes:
//   New interface: describe its use here
//
//===================================================================
//
//  Dec 2001  Creation: Code generated by the CAA wizard  walker
//  Jun 2002  Modify  : Add new method to remove IOlinks  SRA
//  Sep 2003  Modified: CAA Changes                       NPJ
//
//===================================================================
#ifndef DNBIIOLinksFactory_H
#define DNBIIOLinksFactory_H

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

#include "DNBSimIOItfCPP.h"

//System
#include "CATBaseUnknown.h"

class DNBIIOLinks;

extern ExportedByDNBSimIOItfCPP IID IID_DNBIIOLinksFactory ;

/**
 * Interface to create IO Links.
 * <b>Role:</b>
 * This interface provides methods for Creating/Removing IO Links.
 */
class ExportedByDNBSimIOItfCPP DNBIIOLinksFactory: public CATBaseUnknown
{
  /**
   * @nodoc
   */
  CATDeclareInterface;

  public:

   /**
    * create IOLinks object.
    * @param oNewObj
    *   The Newly created IO Link Object.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
    *   </dl>
    */
    
    virtual HRESULT CreateIOLinks( DNBIIOLinks ** oNewObj ) = 0;


   /**
    * Removes IOLinks from the Container
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
    *   </dl>
    */
    virtual HRESULT RemoveIOLinks() = 0;


  // No constructors or destructors on this pure virtual base class
  // --------------------------------------------------------------
};

/**
 * @nodoc
 */
CATDeclareHandler( DNBIIOLinksFactory, CATBaseUnknown );
//------------------------------------------------------------------

#endif
