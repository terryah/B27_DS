#ifndef DNBIIOItem_H
#define DNBIIOItem_H

// COPYRIGHT Dassault Systemes 2003

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

#include <CATBaseUnknown.h>
#include <CATUnicodeString.h>

#include <DNBSimIOItfCPP.h>

class DNBIResourceIO;

extern ExportedByDNBSimIOItfCPP IID IID_DNBIIOItem ;

/**
 * Interface to manage IO Items.
 * <b>Role:</b>
 * This interface provides methods to define/modify IO Items.
 * @see DNBIIOItemFactory
 */
class ExportedByDNBSimIOItfCPP DNBIIOItem: public CATBaseUnknown
{
   /**
    * @nodoc
    */
  CATDeclareInterface;

  public:

      /**
       * IO Item Type.
       * @param ioBIT
       *   Boolean.
       * @param ioSHORT
       *   Short int.
       * @param ioUSHORT
       *   Unsigned short int.
       * @param ioLONG
       *   Interger. Currently only this is supported at execution time.
       * @param ioULONG
       *   Unsigned integer.
       * @param ioDOUBLE
       *   Double.
       * @param ioFLOAT
       *   Float.
       * @param ioSTRING
       *   String.
       */
      enum DNBIOType 
      { 
          ioBIT = VT_BOOL,
          ioSHORT = VT_I2,
          ioUSHORT = VT_UI2,
          ioLONG = VT_I4,
          ioULONG = VT_UI4,
          ioDOUBLE = VT_R8,
          ioFLOAT = VT_R4,
          ioSTRING = VT_BSTR
      };


   /**
    * Retrieves the name of the IO Item.
    * @param oIOName
    *   The user defined name of the IO Item.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The name of IO Item was corrrectly retrieved</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The name of IO Item was not corrrectly retrieved</dd>
    *   </dl>
    */
    virtual HRESULT GetName( CATUnicodeString &oIOName ) const = 0;

   /**
    * Defines the name of the IO Item.
    * @param iIOName
    *   The user defined name of the IO Item.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The name of IO Item was corrrectly set</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The name of IO Item was not corrrectly set</dd>
    *   </dl>
    */
    virtual HRESULT SetName( const CATUnicodeString &iIOName ) = 0;

   /**
    * Retrieves the data type of the IO Item.
    * @param oIOType
    *   The data type of the IO Item.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The type of IO Item was corrrectly retrieved</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The type of IO Item was not corrrectly retrieved</dd>
    *   </dl>
    */
    virtual HRESULT GetType(DNBIIOItem::DNBIOType &oIOType) const = 0;

   /**
    * Defines the data type of the IO Item.
    * @param iIOType
    *   The data type of the IO Item.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The type of IO Item was corrrectly set</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The type of IO Item was not corrrectly set</dd>
    *   </dl>
    */
    virtual HRESULT SetType(const DNBIIOItem::DNBIOType &iIOType) = 0;

   /**
    * Retrieves the default value for the IO Item.
    * @param oDefaultValue
    *   The default value for the IO Item.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The default value for IO Item was corrrectly retrieved</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The default value for IO Item was not corrrectly retrieved</dd>
    *   </dl>
    */
    virtual HRESULT GetDefaultValue(VARIANT &oDefaultValue) const = 0;

   /**
    * Defines the default value for the IO Item.
    * @param iDefaultValue
    *   The default value for the IO Item.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The default value for IO Item was corrrectly set</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The default value for IO Item was not corrrectly set</dd>
    *   </dl>
    */
    virtual HRESULT SetDefaultValue( const VARIANT &iDefaultValue ) = 0;

   /**
    * Retrieves the resource that owns the IO Item.
    * @param oDefaultValue
    *   The resource that owns the IO Item.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
    *   </dl>
    */
    virtual HRESULT GetResource(DNBIResourceIO **opIResource ) const = 0;

   /**
    * Defines the resource that owns the IO Item.
    * @param ipIResource
    *   The resource that owns the IO Item.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The resource for IO Item was corrrectly set</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The resource for IO Item was not corrrectly set</dd>
    *   </dl>
    */
    virtual HRESULT SetResource(DNBIResourceIO *ipIResource ) = 0;

   /**
    * Retrieves the IO number for the IO Item.
    * @param oIONumber
    *   The IO number for the IO Item.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The IO number for IO Item was corrrectly retrieved</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The IO number for IO Item was not corrrectly retrieved</dd>
    *   </dl>
    */
    virtual HRESULT GetNumber( int &oIONumber ) const = 0;

   /**
    * Defines the IO number for the IO Item.
    * @param iIONumber
    *   The IO number for the IO Item.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The IO number for IO Item was corrrectly set</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The IO number for IO Item was not corrrectly set</dd>
    *   </dl>
    */
    virtual HRESULT SetNumber( const int &iIONumber ) = 0;

    

    /**
     * @nodoc
     * stores the IO server handle used to identify this IO point.
     */
    virtual HRESULT setClientHandle( DWORD handle ) = 0;

    /**
     * @nodoc
     * Returns the IO server handle used to identify this IO point.
     */
    virtual DWORD getClientHandle() const = 0;

    /**
     * @nodoc
     * stores the IO server handle used to identify this IO point.
     */
    virtual HRESULT setServerHandle( DWORD handle ) = 0;

    /**
     * @nodoc
     * Returns the IO server handle used to identify this IO point.
     */
    virtual DWORD getServerHandle() const = 0;

    /**
     * @nodoc
     * stores the IO server handle used to identify this IO point.
     */
    virtual HRESULT setGroupHandle( DWORD handle ) = 0;

    /**
     * @nodoc
     * Returns the IO server handle used to identify this IO point.
     */
    virtual DWORD getGroupHandle() const = 0;

    /**
     * @nodoc
     * stores the IO server handle used to identify this IO point.
     */
    virtual HRESULT setClientGroupHandle( DWORD handle ) = 0;

    /**
     * @nodoc
     * Returns the IO server handle used to identify this IO point.
     */
    virtual DWORD getClientGroupHandle() const = 0;

    /**
     * @nodoc
     * stores the IO group name used to identify this IO point.
     */
    virtual HRESULT setGroupName( const CATUnicodeString& groupName ) = 0;

    /**
     * @nodoc
     * Returns the IO group name used to identify this IO point.
     */
    virtual CATUnicodeString getGroupName() const = 0;


};

/**
 * @nodoc
 */
CATDeclareHandler( DNBIIOItem, CATBaseUnknown );

#endif
