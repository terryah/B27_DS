#ifndef DNBIResourceIO_H
#define DNBIResourceIO_H

// COPYRIGHT Dassault Systemes 2003

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

#include <DNBSimIOItfCPP.h>

#include <CATBaseUnknown.h>
#include <CATUnicodeString.h>

class CATListValCATBaseUnknown_var;
class IOPCServer;

extern ExportedByDNBSimIOItfCPP IID IID_DNBIResourceIO ;

class DNBIIOItem;

/**
 * Interface to manage IOs of a resource.
 * <b>Role:</b>
 * This interface provides methods to create/retreive/remove IO Items
 * for a resource.
 * @see DNBIResourceIOFactory, DNBIIOItemFactory
 */
class ExportedByDNBSimIOItfCPP DNBIResourceIO: public CATBaseUnknown
{
   /**
    * @nodoc
    */
   CATDeclareInterface;

  public:
     /**
      * @nodoc
      */
      enum DNBIOServerType 
      { 
          INTERNAL, 
          REMOTE 
      };

   /**
    * Retrieves the number of inputs.
    * @param oNumInputs
    *   The number of inputs defined on the current resource.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The number of inputs was corrrectly retrieved</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The number of inputs was not corrrectly retrieved</dd>
    *   </dl>
    */
    virtual HRESULT GetNumInputs(int &oNumInputs) const = 0;

   /**
    * Retrieves a single input item. 1 based index.
    * @param iIndex
    *   The index of the needed input item.
    * @param opIInputItem
    *   The input item.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
    *   </dl>
    */
    virtual HRESULT GetInput( const int &iIndex, DNBIIOItem **opIInputItem) const = 0;

   /**
    * Retrieves a list containing all the inputs.
    * @param oInputItemList
    *   List of inputs defined on the resource.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
    *   </dl>
    */
    virtual HRESULT GetInputs(CATListValCATBaseUnknown_var *& oInputItemList) const = 0;

   /**
    * Retrieves an input item given it's name.
    * @param iInputName
    *   The name of the needed input item.
    * @param opIInputItem
    *   The input item.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
    *   </dl>
    */
    virtual HRESULT GetInputByName(const CATUnicodeString& iInputName, DNBIIOItem **opIInputItem) const = 0;

   /**
    * Creates a new input on the resource.
    * @param opIInputItem
    *   The Newly created input item.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
    *   </dl>
    */
    virtual HRESULT CreateInput( DNBIIOItem **opIInputItem ) = 0;

   /**
    * Retrieves the number of outputs.
    * @param oNumOutputs
    *   The number of outputs defined on the current resource.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The number of outputs was corrrectly retrieved</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The number of outputs was not corrrectly retrieved</dd>
    *   </dl>
    */
    virtual HRESULT GetNumOutputs( int &oNumOutputs ) const = 0;
 
   /**
    * Retrieves a single output. 1 based index.
    * @param iIndex
    *   The index of the output item required.
    * @param opIOutputItem
    *   The output item.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
    *   </dl>
    */
    virtual HRESULT GetOutput( const int &iIndex, DNBIIOItem **opIOutputItem ) const = 0;

   /**
    * Retrieves a list containing all the outputs.
    * @param oOutputItemList
    *   The list of output items defined on the resource.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
    *   </dl>
    */
    virtual HRESULT GetOutputs(CATListValCATBaseUnknown_var *&oOutputItemList ) const = 0;

   /**
    * Retrieves an output item given it's name.
    * @param iIOName
    *   The name of the output item required.
    * @param opIOutputItem
    *   The output item.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
    *   </dl>
    */
    virtual HRESULT GetOutputByName( const CATUnicodeString& iIOName, DNBIIOItem **opIOutputItem ) const = 0;

   /**
    * Creates a new output on the resource.
    * @param opIOutputItem
    *   The Newly created output item.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
    *   </dl>
    */
    virtual HRESULT CreateOutput( DNBIIOItem ** opIOutputItem ) = 0;

   /**
    * Remove Input Item By Name.
    * @param iIOName
    *   The name of the input item to remove.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component was removed successfully</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component removal failed</dd>
    *   </dl>
    */
    virtual HRESULT RemoveInputByName( const CATUnicodeString &iIOName ) = 0;

   /**
    * Remove Output Item By Name.
    * @param iIOName
    *   The name of the output item to remove.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component was removed successfully</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component removal failed</dd>
    *   </dl>
    */
    virtual HRESULT RemoveOutputByName( const CATUnicodeString &iIOName ) = 0;

   /**
    * Remove Input Item given it's index.
    * @param iIONumber
    *   The index of the input item to remove.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component was removed successfully</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component removal failed</dd>
    *   </dl>
    */
    virtual HRESULT RemoveInputByNumber( const int &iIONumber ) = 0;

   /**
    * Remove Output Item given it's index.
    * @param iIONumber
    *   The index of the output item to remove.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component was removed successfully</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component removal failed</dd>
    *   </dl>
    */
    virtual HRESULT RemoveOutputByNumber( const int &iIONumber ) = 0;

   /**
    * Retrieves an input item given it's index.
    * @param iIONumber
    *   The index of the input item to retrieve.
    * @param opIInputItem
    *   The Newly created ResourceIO.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
    *   </dl>
    */
    virtual HRESULT GetInputByNumber( const int& iIONumber, DNBIIOItem **opIInputItem ) const = 0;

   /**
    * Retrieves an output item given it's index.
    * @param iIONumber
    *   The index of the output item to retrieve.
    * @param opIOutputItem
    *   The Newly created ResourceIO.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
    *   </dl>
    */
    virtual HRESULT GetOutputByNumber( const int& iIONumber, DNBIIOItem **opIOutputItem ) const = 0;

   /**
    * @nodoc
    * Removes the given number of outputs from the end
    */
    virtual HRESULT pruneOutputs( int cnt ) = 0;

   /**
    * @nodoc
    * Removes the given number of inputs from the end
    */
    virtual HRESULT pruneInputs( int cnt ) = 0;

   /**
    * @nodoc
    * Retrieves the IO server for the resource
    */
    virtual HRESULT getOPCServer( IOPCServer ** oServer ) = 0;


   /**
    * @nodoc
    * Releases the reference count on the IOPCServer held by the object.
    */
    virtual void releaseOPCServer() = 0;


   /**
    * @nodoc
    * This method builds the IO into the server for simulation purposes.
    * Each IO point on the resource is registered with the server.
    */
    virtual HRESULT buildIO() = 0;
    

   /**
    * @nodoc
    * This method destroys the IO
    */
    virtual HRESULT destroyIO() = 0;

   /**
    * @nodoc
    * This method resets the IO to default values for simulation purposes.
    */
    virtual HRESULT resetIO() = 0;

   /**
    * @nodoc
    * Returns the type of IO server for the Resource. IO can
    * be manipulated via the general sim server (LOCAL) or from
    * a 3rd party OPC server (REMOTE). In the case of REMOTE
    * getServerString() will contain information on how to connect
    * to the server. The server will be created when needed by the
    * system.
    */
    virtual DNBIResourceIO::DNBIOServerType getServerType() const = 0;

   /**
    * @nodoc
    * Retrieves the Server string to use when connecting to a 
    * 3rd party OPC server. 
    */
    virtual CATUnicodeString getServerString() const = 0;

   /**
    * @nodoc
    * Retrieves the Server Access Path to use when connecting to a 
    * 3rd party OPC server. 
    */
    virtual CATUnicodeString getServerAccessPath() const = 0;

   /**
    * @nodoc
    * This method sets the server type to LOCAL or REMOTE
    */
    virtual HRESULT setServerType( DNBIResourceIO::DNBIOServerType type ) = 0;

   /**
    * @nodoc
    * This method sets the server String to use when connecting to a 
    * 3rd party OPC server. 
    */
    virtual HRESULT setServerString( const CATUnicodeString& serverString ) = 0;

   /**
    * @nodoc
    * This method sets the server access path to use when connecting to a 
    * 3rd party OPC server. 
    */
    virtual HRESULT setServerAccessPath( const CATUnicodeString& serverString ) = 0;

   /**
    * Creates equivalent LCM signals corresponding to the existing IOItems of a given
	* ResourceIO.
    */
#if defined (_WINDOWS_SOURCE) 
	virtual HRESULT ConvertIOtoSignals() = 0;
#endif

};

/**
 * @nodoc
 */
CATDeclareHandler( DNBIResourceIO, CATBaseUnknown );
#endif
