#ifndef DNBIResourceIOFactory_H
#define DNBIResourceIOFactory_H

// COPYRIGHT Dassault Systemes 2003

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

#include "CATBaseUnknown.h"

#include "DNBSimIOItfCPP.h"

class DNBIResourceIO;

extern ExportedByDNBSimIOItfCPP IID IID_DNBIResourceIOFactory ;

/**
 * Interface to create/remove a ResourceIO.
 * <b>Role:</b>
 * This interface provides methods for creating/removing ResourceIOs. 
 */
class ExportedByDNBSimIOItfCPP DNBIResourceIOFactory: public CATBaseUnknown
{
   /**
    * @nodoc
    */
   CATDeclareInterface;

  public:

   /**
    * Creates a ResourceIO for the resource.
    * @param opINewObj
    *   The Newly created ResourceIO.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
    *   </dl>
    */
    virtual HRESULT CreateResourceIO( DNBIResourceIO ** opINewObj ) = 0;

   /**
    * Removes the ResourceIO.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
    *   </dl>
    */
    virtual HRESULT RemoveResourceIO() = 0;

};

/**
 * @nodoc
 */
CATDeclareHandler( DNBIResourceIOFactory, CATBaseUnknown );

#endif
