#ifndef DNBIIZoneFactory_H
#define DNBIIZoneFactory_H


// COPYRIGHT Dassault Systemes 2003

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

#include "DNBSimIOItfCPP.h"

#include "CATBaseUnknown.h"

class CATUnicodeString;
class DNBIIZone;

extern ExportedByDNBSimIOItfCPP IID IID_DNBIIZoneFactory ;

/**
 * Interface to create Interference Zones(IZ).
 * <b>Role:</b>
 * This interface provides methods to create IZs.
 */
class ExportedByDNBSimIOItfCPP DNBIIZoneFactory: public CATBaseUnknown
{
   /**
    * @nodoc
    */
  CATDeclareInterface;

  public:

      /**
       * Creates a Interfernce Zone.
       * @param iName
       *   The name of the Interfernce Zone to create.
       * @param opINewObj
       *   The Newly created Interfernce Zone.
       * @return
       *   An HRESULT.
       *   <br><b>Legal values</b>:
       *   <dl>
       *     <dt>S_OK</dt>
       *     <dd>The component is successfully created
       *         and the interface pointer is successfully returned</dd>
       *     <dt>E_FAIL </dt>
       *     <dd>The component was successfully created,
       *         but the interface query failed</dd>
       *     <dt>E_NOINTERFACE </dt>
       *     <dd>The component was successfully created,
       *         but the it doesn't implement the requested interface</dd>
       *     <dt>E_OUTOFMEMORY </dt>
       *     <dd><dd>The component allocation failed</dd>
       *   </dl>
       */
      virtual HRESULT CreateInterferenceZone( const CATUnicodeString &iName, DNBIIZone ** opINewObj ) = 0;

};

/**
 * @nodoc
 */
CATDeclareHandler( DNBIIZoneFactory, CATBaseUnknown );
#endif
