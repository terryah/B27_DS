#ifndef DNBIIZone_H
#define DNBIIZone_H


// COPYRIGHT Dassault Systemes 2003

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

#include "DNBSimIOItfCPP.h"

#include "CATBaseUnknown.h"

class CATListValCATBaseUnknown_var;
class CATUnicodeString;

extern ExportedByDNBSimIOItfCPP IID IID_DNBIIZone ;

/**
 * Interface to manage Interference zones.
 * <b>Role:</b>
 * This interface provides methods to define/modify interference zones.
 * @see DNBIIZoneFactory
 */
class ExportedByDNBSimIOItfCPP DNBIIZone: public CATBaseUnknown
{
   /**
    * @nodoc
    */
  CATDeclareInterface;

  public:


      /**
       * Retrieves the resources in the Interference Zone.
       * @param oResourceList
       *   The list of resources present in the IZ.
       * @return
       *   An HRESULT.
       *   <br><b>Legal values</b>:
       *   <dl>
       *     <dt>S_OK</dt>
       *     <dd>The component is successfully created
       *         and the interface pointer is successfully returned</dd>
       *     <dt>E_FAIL </dt>
       *     <dd>The component was successfully created,
       *         but the interface query failed</dd>
       *     <dt>E_NOINTERFACE </dt>
       *     <dd>The component was successfully created,
       *         but the it doesn't implement the requested interface</dd>
       *     <dt>E_OUTOFMEMORY </dt>
       *     <dd><dd>The component allocation failed</dd>
       *   </dl>
       */
     virtual HRESULT GetResourcesInZone( CATListValCATBaseUnknown_var& oResourceList ) = 0;

      /**
       * Defines the resources in the Interference Zone.
       * @param iResourceList
       *   The list of resources to set in the IZ.
       * @return
       *   An HRESULT.
       *   <br><b>Legal values</b>:
       *   <dl>
       *     <dt>S_OK</dt>
       *     <dd>The component is successfully created
       *         and the interface pointer is successfully returned</dd>
       *     <dt>E_FAIL </dt>
       *     <dd>The component was successfully created,
       *         but the interface query failed</dd>
       *     <dt>E_NOINTERFACE </dt>
       *     <dd>The component was successfully created,
       *         but the it doesn't implement the requested interface</dd>
       *     <dt>E_OUTOFMEMORY </dt>
       *     <dd><dd>The component allocation failed</dd>
       *   </dl>
       */
     virtual HRESULT SetResourcesInZone( const CATListValCATBaseUnknown_var& iResourceList ) = 0;

      /**
       * Adds a resource in the Interference Zone.
       * @param iResource
       *   The resource to add in the IZ.
       * @return
       *   An HRESULT.
       *   <br><b>Legal values</b>:
       *   <dl>
       *     <dt>S_OK</dt>
       *     <dd>The component is successfully created
       *         and the interface pointer is successfully returned</dd>
       *     <dt>E_FAIL </dt>
       *     <dd>The component was successfully created,
       *         but the interface query failed</dd>
       *     <dt>E_NOINTERFACE </dt>
       *     <dd>The component was successfully created,
       *         but the it doesn't implement the requested interface</dd>
       *     <dt>E_OUTOFMEMORY </dt>
       *     <dd><dd>The component allocation failed</dd>
       *   </dl>
       */
     virtual HRESULT SetResourceInZone( CATBaseUnknown *iResource ) = 0;  
     
      /**
       * Removes resource in the Interference Zone.
       * @param iResource
       *   The resource to set in the IZ.
       * @return
       *   An HRESULT.
       *   <br><b>Legal values</b>:
       *   <dl>
       *     <dt>S_OK</dt>
       *     <dd>The component is successfully created
       *         and the interface pointer is successfully returned</dd>
       *     <dt>E_FAIL </dt>
       *     <dd>The component was successfully created,
       *         but the interface query failed</dd>
       *     <dt>E_NOINTERFACE </dt>
       *     <dd>The component was successfully created,
       *         but the it doesn't implement the requested interface</dd>
       *     <dt>E_OUTOFMEMORY </dt>
       *     <dd><dd>The component allocation failed</dd>
       *   </dl>
       */
     virtual HRESULT RemoveResourceInZone( CATBaseUnknown *iResource ) = 0;

      /**
       * Removes all the resources defined in the Interference Zone.
       * @return
       *   An HRESULT.
       *   <br><b>Legal values</b>:
       *   <dl>
       *     <dt>S_OK</dt>
       *     <dd>The component is successfully created
       *         and the interface pointer is successfully returned</dd>
       *     <dt>E_FAIL </dt>
       *     <dd>The component was successfully created,
       *         but the interface query failed</dd>
       *     <dt>E_NOINTERFACE </dt>
       *     <dd>The component was successfully created,
       *         but the it doesn't implement the requested interface</dd>
       *     <dt>E_OUTOFMEMORY </dt>
       *     <dd><dd>The component allocation failed</dd>
       *   </dl>
       */
     virtual HRESULT RemoveAllResourcesInZone() = 0;

     /**
      * @nodoc
      * get Tasks in the Zone
      **/
     virtual HRESULT getResourceTasksInZone( CATListValCATBaseUnknown_var& oresTaskList ) = 0;

     /**
      * @nodoc
      * set (associate) Tasks to the zone
      **/
     virtual HRESULT setResourceTasksInZone( const CATListValCATBaseUnknown_var& iresTaskList ) = 0;

     /**
      * @nodoc
      * remove a Resource task from Zone
      **/
     virtual HRESULT removeResourceTaskInZone( const CATBaseUnknown_var& iresTask ) = 0;

     /**
      * @nodoc
      * remove all resource Tasks from a Zone
      **/
     virtual HRESULT removeAllResourceTasksInZone() = 0;

     /**
      * @nodoc
      * set generatedBy option to the Zone
      **/
     virtual HRESULT setGeneratedBy( CATUnicodeString generatedBy ) = 0;

     /**
      * @nodoc
      * get generatedBy option from the Zone
      **/
     virtual HRESULT getGeneratedBy( CATUnicodeString& generatedBy ) const = 0;

};

/**
 * @nodoc
 */
CATDeclareHandler( DNBIIZone, CATBaseUnknown );

#endif
