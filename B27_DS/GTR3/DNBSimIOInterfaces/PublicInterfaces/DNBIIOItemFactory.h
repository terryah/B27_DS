#ifndef DNBIIOItemFactory_H
#define DNBIIOItemFactory_H

// COPYRIGHT Dassault Systemes 2003

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

#include "CATBaseUnknown.h"

#include "DNBSimIOItfCPP.h"

class DNBIIOItem;
class CATISpecObject;

extern ExportedByDNBSimIOItfCPP IID IID_DNBIIOItemFactory ;

/**
 * Interface to create a IO Item.
 * <b>Role:</b>
 * This interface provides methods for creating IO Items. 
 */
class ExportedByDNBSimIOItfCPP DNBIIOItemFactory: public CATBaseUnknown
{
   /**
    * @nodoc
    */
  CATDeclareInterface;

  public:

   /**
    * Creates a IO Item as component to the passed specobject.
    * @param ipISO
    *   The specobject to add the new IO Item.
    * @param opINewIOItem
    *   The Newly created IO Item.
    * @return
    *   An HRESULT.
    *   <br><b>Legal values</b>:
    *   <dl>
    *     <dt>S_OK</dt>
    *     <dd>The component is successfully created
    *         and the interface pointer is successfully returned</dd>
    *     <dt>E_FAIL </dt>
    *     <dd>The component was successfully created,
    *         but the interface query failed</dd>
    *     <dt>E_NOINTERFACE </dt>
    *     <dd>The component was successfully created,
    *         but the it doesn't implement the requested interface</dd>
    *     <dt>E_OUTOFMEMORY </dt>
    *     <dd><dd>The component allocation failed</dd>
    *   </dl>
    */
    virtual HRESULT CreateAsComponent(CATISpecObject *ipISO, DNBIIOItem **opINewIOItem) = 0;

   
};

#endif
