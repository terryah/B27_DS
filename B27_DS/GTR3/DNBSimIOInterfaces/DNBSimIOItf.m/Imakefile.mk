# COPYRIGHT DASSAULT SYSTEMES 2001
#======================================================================
# Imakefile for module DNBSimIOItf.m 
#======================================================================
#
#  Dec 2001  Creation: Code generated by the CAA wizard  walker
#======================================================================
#
# SHARED LIBRARY 
#
BUILT_OBJECT_TYPE=SHARED LIBRARY
 
# DO NOT EDIT :: THE CAA2 WIZARDS WILL ADD CODE HERE
WIZARD_LINK_MODULES = JS0GROUP \
                      AD0XXBAS
# END WIZARD EDITION ZONE

LINK_WITH = $(WIZARD_LINK_MODULES)

INCLUDED_MODULES = \
                  DNBSimIOItfCPP \
                  DNBSimIOProIDL

OS      = Windows_NT
BUILD   = YES

OS      = IRIX
BUILD   = YES

OS      = SunOS
BUILD   = YES

OS      = AIX
BUILD   = YES

OS      = HP-UX
BUILD   = YES

OS      = win_a
BUILD   = NO
