#include "CATIACGMLevel.h"
#ifdef CATIACGMR420CAA
#ifdef _STATIC_SOURCE
#define	ExportedByFrFTopologicalOpeLight
#elif defined __FrFTopologicalOpeLight


// COPYRIGHT DASSAULT SYSTEMES 1999

/** @CAA2Required */
#define ExportedByFrFTopologicalOpeLight DSYExport
#else
#define ExportedByFrFTopologicalOpeLight DSYImport
#endif
#include "DSYExport.h"
#else
#ifdef _STATIC_SOURCE
#define	ExportedByFrFTopologicalOpeLight
#elif defined _WINDOWS_SOURCE
#ifdef	__FrFTopologicalOpeLight
// COPYRIGHT DASSAULT SYSTEMES 1999
/** @CAA2Required */
#define	ExportedByFrFTopologicalOpeLight	__declspec(dllexport)
#else
#define	ExportedByFrFTopologicalOpeLight	__declspec(dllimport)
#endif
#else
#define	ExportedByFrFTopologicalOpeLight
#endif
#endif
//#include <AdvCommonDec.h>
#include <CATTrackNSTOL.h>
