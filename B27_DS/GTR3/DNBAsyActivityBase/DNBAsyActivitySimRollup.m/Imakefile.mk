# COPYRIGHT DASSAULT SYSTEMES 2006
#======================================================================
# Imakefile for module DNBAsyActivitySimRollup.m
#======================================================================
#
#  Oct 2006  Creation: Code generated by the CAA wizard  ssz
#======================================================================

BUILT_OBJECT_TYPE = SHARED LIBRARY
 
# DO NOT EDIT :: THE CAA2 WIZARDS WILL ADD CODE HERE
# END WIZARD EDITION ZONE

LINK_WITH =   JS0FM \
              JS0GROUP \
              DNBPLMItf \
              DNBPLMInterfacesUUID \
              DNBAsyActivityItf \
              AD0XXBAS \
              DNBSimActivityBase \
              CATProcessInterfaces \
              CATXMLParserItf \
              DNBSimActivityItf \
              CATMathematics \
              CATObjectSpecsModeler \
              DNBSimRollUp \
              DNBAsyActivityBase \
              CATProductStructure1 \
              
              
# define the build platforms

OS      = Windows_NT
BUILD   = YES

OS      = IRIX
BUILD   = NO

OS      = SunOS
BUILD   = NO

OS      = AIX
BUILD   = NO

OS      = HP-UX
BUILD   = NO

OS      = win_a
BUILD   = NO

