//=================================================================== 
//  COPYRIGHT  Dassault  Systemes  2010
//=================================================================== 
#ifdef  _WINDOWS_SOURCE
#ifdef  __DNBAsyActivityBase
#define ExportedByDNBAsyActivityBase        __declspec(dllexport)
#else
#define ExportedByDNBAsyActivityBase        __declspec(dllimport)
#endif
#else
#define ExportedByDNBAsyActivityBase
#endif

/**  
*  @CAA2Level  L0
*  @CAA2Usage  U2  
*/ 
