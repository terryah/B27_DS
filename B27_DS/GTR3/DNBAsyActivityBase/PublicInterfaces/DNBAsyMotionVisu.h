
//===================================================================
// CATIA Version 5 Release 16 Framework DNBAsyActivityBase
// Copyright   : Dassault Systemes 2005
//----------------------------------------------------------------
// Description : C++ class header file for the class named "DNBAsyMotionVisu"
//
//===================================================================
//
// Usage notes: Implementation class. Is used as a dummy object to provide
//              visualization for MoveActivity.
//
//      cre     sar     11/12/2002      creation
//      mod     sar     09/04/2003      do not need track to be stored.
//                                      changed the signature of the ctor to fix
//                                      RI 405179.
//      mod     lhg     04/05/2004      Remove the pointer to CATISO as data member
//                                      due to its volatility. IR 442539.
//
//===================================================================

#ifndef DNBAsyMotionVisu_H
#define DNBAsyMotionVisu_H


/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */


#include "DNBAsyActivityBase.h"

#include "CATBaseUnknown.h"
#include "CATDlgEngUtility.h"

class CATISO;
class DNBIAsyMotionActivity;

//--------------------------------------------------------------------

class ExportedByDNBAsyActivityBase DNBAsyMotionVisu : public CATBaseUnknown
{
  public:

    CATDeclareClass;

    //----------------------------------------------------------------
    // Constructor
    //----------------------------------------------------------------

    /**
      * Used to manage the visualization for the move activity.
      * <br><b>Role:</b> Constructor used to manage the visualization 
      * for the move activity.
      * @param ipMotion
      *   The move activity to have its visualization managed.
      */
    DNBAsyMotionVisu (DNBIAsyMotionActivity *ipMotion);


    /**
      * @nodoc 
      * Standard destructor, not called by the user, so it won't documented.
      */
    virtual ~DNBAsyMotionVisu();


    /**
      * Set the visibility of the move activity.
      * <br><b>Role:</b> Change the visibility of the visualization of the
      * move activity.  This includes showing (creating) and hiding destroying)
      * the visualization.
      * @param iVisib
      *   Used to set the visiblity of the visualization.<br>
      *   Legal values:<br>
      *   <ul>
      *      <li><b>CATDlgEngShow</b> : Actually sends create event to the model
      *          which in turn will generate the visualization.
      *      <li><b>CATDlgEngHide</b> : Actually sends delete event to the model
      *          which in turn will destory the visualization.
      *   </ul>
      */
    void SetVisibility (CATDlgEngVisibility iVisib);


    /**
      * Gets the associated move activity.
      * <br><b>Role:</b> Gets the move activity that is associated with this
      * visualization.
      * @return 
      *   The move activity that is associated to this visualization.
      */
    DNBIAsyMotionActivity * GetAsyMotionActivity ();


  private:
    
    DNBIAsyMotionActivity   *_pMotionAct;

};
#endif DNBAsyMotionVisu_H
