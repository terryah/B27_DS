#include "CATIACGMLevel.h"
#ifdef CATIACGMR420CAA
#ifdef _STATIC_SOURCE
#define	ExportedByYN000M2D
#elif defined __YN000M2D


// COPYRIGHT DASSAULT SYSTEMES  1999

/** @CAA2Required */
#define ExportedByYN000M2D DSYExport
#else
#define ExportedByYN000M2D DSYImport
#endif
#include "DSYExport.h"
#else
#ifdef _STATIC_SOURCE
#define	ExportedByYN000M2D
#elif defined _WINDOWS_SOURCE
// COPYRIGHT DASSAULT SYSTEMES  1999
/** @CAA2Required */
#ifdef	__YN000M2D
#define	ExportedByYN000M2D	__declspec(dllexport)
#else
#define	ExportedByYN000M2D	__declspec(dllimport)
#endif
#else
#define	ExportedByYN000M2D
#endif
#endif
