#include "CATIACGMLevel.h"
#ifdef CATIACGMR420CAA
#ifdef _STATIC_SOURCE
#define	ExportedByYN000MFL
#elif defined __YN000MFL


// COPYRIGHT DASSAULT SYSTEMES  1999

/** @CAA2Required */
#define ExportedByYN000MFL DSYExport
#else
#define ExportedByYN000MFL DSYImport
#endif
#include "DSYExport.h"
#else
#ifdef _STATIC_SOURCE
#define	ExportedByYN000MFL
#elif defined _WINDOWS_SOURCE
// COPYRIGHT DASSAULT SYSTEMES  1999
/** @CAA2Required */
#ifdef	__YN000MFL
#define	ExportedByYN000MFL	__declspec(dllexport)
#else
#define	ExportedByYN000MFL	__declspec(dllimport)
#endif
#else
#define	ExportedByYN000MFL
#endif
#endif
