// COPYRIGHT DASSAULT SYSTEMES 1999
/** @CAA2Required */
#include "CATIACGMLevel.h"
#ifdef CATIACGMR420CAA
#ifdef _STATIC_SOURCE
#define	ExportedByCATMathStream
#elif defined __CATMathStream

#define ExportedByCATMathStream DSYExport
#else
#define ExportedByCATMathStream DSYImport
#endif
#include "DSYExport.h"
#else
#ifdef _STATIC_SOURCE
#define	ExportedByCATMathStream
#elif defined _WINDOWS_SOURCE
#ifdef	__CATMathStream
#define	ExportedByCATMathStream	__declspec(dllexport)
#else
#define	ExportedByCATMathStream	__declspec(dllimport)
#endif
#else
#define	ExportedByCATMathStream
#endif
#endif
