// COPYRIGHT DASSAULT SYSTEMES 2011

#ifndef MPROCTools_h
#define MPROCTools_h

/** @CAA2Required */
#include "CATIACGMLevel.h"
#ifdef CATIACGMR420CAA
#ifdef _STATIC_SOURCE
#define	ExportedByMPROCTools
#elif defined __MPROCTools

#define ExportedByMPROCTools DSYExport
#else
#define ExportedByMPROCTools DSYImport
#endif
#include "DSYExport.h"
#else
#ifdef _STATIC_SOURCE
#define	ExportedByMPROCTools
#elif defined _WINDOWS_SOURCE
#ifdef	__MPROCTools
#define	ExportedByMPROCTools	__declspec(dllexport)
#else
#define	ExportedByMPROCTools	__declspec(dllimport)
#endif
#else
#define	ExportedByMPROCTools
#endif
#endif

#endif
