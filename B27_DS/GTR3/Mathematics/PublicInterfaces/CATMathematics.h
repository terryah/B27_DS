//=============================================================================
// COPYRIGHT DASSAULT SYSTEMES  1999
//=============================================================================
/** @CAA2Required */
/*---------------------------------------------------------------------*/
/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */
/*---------------------------------------------------------------------*/
#include "CATIACGMLevel.h"
#ifdef CATIACGMR420CAA
#ifdef _STATIC_SOURCE
#define	ExportedByCATMathematics
#elif defined __CATMathematics
#define ExportedByCATMathematics DSYExport
#else
#define ExportedByCATMathematics DSYImport
#endif
#include "DSYExport.h"
#else
#ifdef _STATIC_SOURCE
#define	ExportedByCATMathematics
#elif defined _WINDOWS_SOURCE
#ifdef	__CATMathematics
#define	ExportedByCATMathematics	__declspec(dllexport)
#else
#define	ExportedByCATMathematics	__declspec(dllimport)
#endif
#else
#define	ExportedByCATMathematics
#endif
#endif
