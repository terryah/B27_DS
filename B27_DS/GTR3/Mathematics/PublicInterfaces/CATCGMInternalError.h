#ifndef CATCGMInternalError_h
#define CATCGMInternalError_h
/** @CAA2Required */

// COPYRIGHT DASSAULT SYSTEMES  1999
#include "CATMathematics.h"
#include "CATInternalError.h"
#include "CATCGMNewArray.h"

class CATCGMInputError;

class ExportedByCATMathematics CATCGMInternalError : public CATInternalError
{
 public:
   
   /** @nodoc */
   virtual void Throw (const char *loc, int line, void *ptr_to_delete = NULL);
   
   /** @nodoc */
   CATDeclareClass;   
   
   /** @nodoc */
   CATCGMInternalError(const CATCGMInternalError &iError);	

   /** @nodoc */
   CATCGMInternalError(CATErrorId id);	
   CATCGMInternalError(CATErrorId id, const char *msgId, const char *catalog = NULL);
   CATCGMInternalError( const char *msgId, const char *catalog ); 

   /** @nodoc  */
   virtual ~CATCGMInternalError();
   
   /** @nodoc */
   CATCGMNewClassArrayDeclare;

   /** @nodoc */
   void DefineCGMErrParams(const CATCGMInternalError &iError);	

   /** @nodoc */
   void DefineCGMErrParams(const CATCGMInputError &iError);	

   friend class CATCGMInputError;
};


#endif
