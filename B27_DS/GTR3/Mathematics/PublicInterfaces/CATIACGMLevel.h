/** @CAA2Required */
/*---------------------------------------------------------------------*/
/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */
/*---------------------------------------------------------------------*/
// COPYRIGHT DASSAULT SYSTEMES 2004
#include "CATIAV5Level.h"

#ifdef CATIAV5R15
  #define CATIACGMV5R15
#endif

#ifdef CATIAV5R16
  #define CATIACGMV5R16
#endif

#ifdef CATIAV5R17
  #define CATIACGMV5R17
#endif

#ifdef CATIAV5R18
  #define CATIACGMV5R18
#endif

#ifdef CATIAV5R19
  #define CATIACGMV5R19
#endif

#ifdef CATIAV5R20
  #define CATIACGMV5R20
#endif

#ifdef CATIAV5R21
  #define CATIACGMV5R21
#endif

#ifdef CATIAV5R22
  #define CATIACGMV5R22
#endif

#ifdef CATIAV5R23
  #define CATIACGMV5R23
#endif

#ifdef CATIAV5R23
  #define CATIACGMR214CAA
  #define CATIACGMR214Code
#endif

#ifdef CATIAV5R24
  #define CATIACGMR215CAA
  #define CATIACGMR215Code
  #define CATIACGMR216CAA
  #define CATIACGMR216Code
#endif

#ifdef CATIAV5R25
  #define CATIACGMR217CAA
  #define CATIACGMR217Code
  #define CATIACGMR417CAA
  #define CATIACGMR417Code
#endif

#ifdef CATIAV5R26
  #define CATIACGMR418CAA
  #define CATIACGMR418Code
#endif

#ifdef CATIAV5R27
  #define CATIACGMR419CAA
  #define CATIACGMR419Code
#endif

#ifdef CATIAV5R28
  #define CATIACGMR420CAA
  #define CATIACGMR420Code
#endif
