#include "CATIACGMLevel.h"
#ifdef CATIACGMR420CAA
#ifdef _STATIC_SOURCE
#define	ExportedByYN000MAT
#elif defined __YN000MAT


// COPYRIGHT DASSAULT SYSTEMES  1999

/** @CAA2Required */
#define ExportedByYN000MAT DSYExport
#else
#define ExportedByYN000MAT DSYImport
#endif
#include "DSYExport.h"
#else
#ifdef _STATIC_SOURCE
#define	ExportedByYN000MAT
#elif defined _WINDOWS_SOURCE
// COPYRIGHT DASSAULT SYSTEMES  1999
/** @CAA2Required */
#ifdef	__YN000MAT
#define	ExportedByYN000MAT	__declspec(dllexport)
#else
#define	ExportedByYN000MAT	__declspec(dllimport)
#endif
#else
#define	ExportedByYN000MAT
#endif
#endif
