#ifndef __CATSmgDefs_h__
#define __CATSmgDefs_h__
//=================================================================================
// COPYRIGHT DASSAULT SYSTEMES 2002
//=================================================================================

/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */

#include "SmgItfEnv.h"
class CATUnicodeString;

ExportedBySmgItfEnv extern const CATUnicodeString SmgAreaType;
ExportedBySmgItfEnv extern const CATUnicodeString SmgEdgeType;
ExportedBySmgItfEnv extern const CATUnicodeString SmgPlaneType;
ExportedBySmgItfEnv extern const CATUnicodeString SmgPointType;

#endif

