/*=======================================================================*/
/* COPYRIGHT DASSAULT SYSTEMES 1996                                      */
/*=======================================================================*/
/*                                                                       */
/*  CATP2PNotif                                                        */
/*                                                                       */
/*  Usage Notes:            */
/*                                                                       */
/*=======================================================================*/
/*  Creation May	2002                                 jnm             */
/*=======================================================================*/
#ifndef _CATP2PDefs
#define _CATP2PDefs

// COPYRIGHT DASSAULT SYSTEMES 2003

//------------------------------------------------------------------
/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */

typedef const char *  CATP2PServiceName;

#define SIZE_SRV_NAME    32
#define SIZE_PEER_NAME   32





#endif
