// COPYRIGHT Dassault Systemes 2002
//===================================================================
//
// CATIP2PMessage.h
// Define the CATIP2PMessage interface
//
//===================================================================
//
// Usage notes:
//   New interface: describe its use here
//
//===================================================================
//
//  May 2002  Creation: Code generated by the CAA wizard  jnm
//===================================================================
#ifndef CATIP2PMessage_H
#define CATIP2PMessage_H

// COPYRIGHT DASSAULT SYSTEMES 2003


/**
 * @CAA2Level L0
 * @CAA2Usage U3
 */
 

#include "CATP2PCore.h"
#include "CATBaseUnknown.h"

#include "CATP2PDefs.h"

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByCATP2PCore IID IID_CATIP2PMessage;
#else
extern "C" const IID IID_CATIP2PMessage ;
#endif

//------------------------------------------------------------------


class CATUnicodeString;

/**
 * Interface 
 * <b>Role</b>: Tu be used in the implementation of HandleMessage(CATIP2PMessage* iMess) for your service.
 * This method is used at the message reception and it allows you to retrieve information about your message and also its content.
 *   @see CATP2PSerivce
 */


class ExportedByCATP2PCore CATIP2PMessage: public CATBaseUnknown
{
  CATDeclareInterface;

  public:

/** 
 *  Message information method
 * <br><b>Role</b>: This method retrieves the message group.
 * @param oGroup 
 *  message group
 */
	 virtual HRESULT GetMessageGroup(CATUnicodeString& oGroup) = 0;

/** 
 *  Message information method
 * <br><b>Role</b>: This method retrieves the name of the sending peer name.
 * @param oPeer_name 
 * sending peer name
 */
	 virtual HRESULT GetSendingPeerName(CATUnicodeString& oPeer_name) = 0;
/** 
 *  Message information method
 * <br><b>Role</b>: This method retrieves the length of the buffer you put into the message.
 * @param oLength 
 * buffer length
 */
	 virtual HRESULT GetBufferLenght(int& oLength) = 0;
/** 
 *  Message information method
 * <br><b>Role</b>: This method retrieves the buffer you put into the message.
 * @param oBuffer 
 * buffer 
 */

	 virtual HRESULT GetBuffer(char* oBuffer) = 0; // must be allocated by the caller with corresponding length
/** 
 *  Message information method
 * <br><b>Role</b>: This method returns true if your message has been sent by a multicast endpoint.
 */
	 virtual int IsMulticast() = 0;
/** 
 *  Message information method
 * <br><b>Role</b>: This method returns true if your message has been sent by a unicast endpoint.
 */
	 virtual int IsUnicast() = 0;
};

//------------------------------------------------------------------

#endif
