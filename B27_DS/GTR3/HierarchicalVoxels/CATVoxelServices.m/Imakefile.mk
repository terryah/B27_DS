# COPYRIGHT DASSAULT SYSTEMES 2000
#======================================================================
# Imakefile for module CATVoxelServices.m
#======================================================================
#
#  Jul 2000  Creation: Code generated by the CAA wizard  DCY
#======================================================================
#
# SHARED LIBRARY 
#
BUILT_OBJECT_TYPE=SHARED LIBRARY 
 
# DO NOT EDIT :: THE CAA2 WIZARDS WILL ADD CODE HERE
WIZARD_LINK_MODULES =  \
JS0GROUP 
# END WIZARD EDITION ZONE


#ifdef CATIAV5R5
LINK_WITH = $(WIZARD_LINK_MODULES) Vps CATMathematics JS0FM CATMathStream CATViz
#else
LINK_WITH = $(WIZARD_LINK_MODULES)
#endif
 


# System dependant variables
#
OS = AIX
#
OS = HP-UX
#
OS = IRIX
#
OS = SunOS
#
OS = Windows_NT
