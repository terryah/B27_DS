/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */
// COPYRIGHT Dassault Systemes 2007
//===================================================================
//
// CATIEhiMechanicalImport.h
// Define the CATIEhiMechanicalImport interface
//
//===================================================================
//
// Usage notes:
//   New interface: describe its use here
//
//===================================================================
//
//  Aug 2007  Creation: Code generated by the CAA wizard  EHR
//===================================================================
#ifndef CATIEhiMechanicalImport_H
#define CATIEhiMechanicalImport_H

#include "CATEhiInterfaces.h"
#include "CATBaseUnknown.h"

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByCATEhiInterfaces IID IID_CATIEhiMechanicalImport;
#else
extern "C" const IID IID_CATIEhiMechanicalImport ;
#endif

class CATISpecObject_var;


//------------------------------------------------------------------

/**
* Interface used to manage external references inside Multi-Branchable CATPart, Bundle Segment CATPart and Electrical Protection CATPart.
*
* <br><b>Role</b>: Components that implement
* CATIEhiMechanicalImport are only the external references inside Multi-Branchable CATPart, Bundle Segment CATPart and Electrical Protection CATPart.
* So, external references not inside this supported CATPart will not be manageable with this interface.
* This interface is a clone of CATIMechanicalImport with a subset of its methods, restricted to EHI objects.
* <p>
*/
class ExportedByCATEhiInterfaces CATIEhiMechanicalImport: public CATBaseUnknown
{
  CATDeclareInterface;

  public:

 /**
  * Returns the part instance in a CATProduct document corresponding to the selected mechanical part.
  */
  virtual CATISpecObject_var GetSourceProduct( ) = 0 ; 

 /**
  * Returns the reference of the current external reference.
  */
  virtual CATISpecObject_var GetSelectedFeatureInSourceProduct() const = 0 ; 

 /**
  * Removes the link between the external reference and its reference. 
  * @return
  * S_OK if succeeded, E_FAIL otherwise.
  */
  virtual HRESULT BreakLink() =0 ; 

 /**
  * Force loading of the reference document
  */
  virtual void ForceLoad() const = 0;


  // No constructors or destructors on this pure virtual base class
  // --------------------------------------------------------------
};

//------------------------------------------------------------------

#endif
