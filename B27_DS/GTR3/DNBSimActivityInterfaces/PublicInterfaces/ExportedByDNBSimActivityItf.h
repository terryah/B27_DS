/** @CAA2Required */
/*---------------------------------------------------------------------*/
/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */
/*---------------------------------------------------------------------*/
#ifdef  _WINDOWS_SOURCE
#ifdef  __DNBSimActivityItfCPP
#define ExportedByDNBSimActivityItf        __declspec(dllexport)
#else
#define ExportedByDNBSimActivityItf        __declspec(dllimport)
#endif
#else
#define ExportedByDNBSimActivityItf
#endif
