// COPYRIGHT DELMIA Corp 2000
//===================================================================
//
// DNBIGrabActivity.h
// Define the DNBIGrabActivity interface
//
//===================================================================
//
// Usage notes:
//   DNBIGrabActivity: interface used to manipulate grab 
//   activities
//
//===================================================================
//
//  Feb 2000  Creation: Code generated by the CAA wizard          YAO
//  Apr 2003  Modified: added new argument to all the methods.    SAR
//  Jun 2003  Modified: removed new argument from all the methods MMG
//  Sep 2003  Modified: expose to CAA, L0/U0                  SHA
//===================================================================
#ifndef DNBIGrabActivity_H
#define DNBIGrabActivity_H
#undef DNB_DEPRECIATE

/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

// Local
#include "ExportedByDNBSimActivityItf.h"

// System
#include "CATBaseUnknown.h"
#include "CATBooleanDef.h"

// ProductStructure
class CATIProduct;
class CATListValCATBaseUnknown_var;
#ifndef DNB_DEPRECIATE
class CATIProduct_var;
#endif  /* DNB_DEPRECIATE */

extern ExportedByDNBSimActivityItf IID IID_DNBIGrabActivity ;

//------------------------------------------------------------------

/**
 * Interface to manipulate grab activities.
 * <b>Role</b>: allows to set/retreive information pertaining to the
 * grab activity.
 * @see DNBIGrabActivityFactory
 */
class ExportedByDNBSimActivityItf DNBIGrabActivity: public CATBaseUnknown
{
   /**
    * @nodoc
    */
    CATDeclareInterface;
    
public:
    
    /**
     * Retrieves the grabbing object for the activity.
     * @param oGrabbingObj
     *   The grabbing object.
     * @return
     *   An HRESULT.
     *   <br><b>Legal values</b>:
     *   <dl>
     *     <dt>S_OK</dt>
     *     <dd>The grabbing object was correctly retrieved</dd>
     *     <dt>E_FAIL </dt>
     *     <dd>The grabbing object was not correctly retrieved</dd>
     *   </dl>
     */
    virtual HRESULT GetGrabbingObj( CATIProduct **oGrabbingObj ) = 0;

    /**
     * Defines the grabbing object for the activity.
     * @param iGrabbingObj
     *   The grabbing object.
     * @return
     *   An HRESULT.
     *   <br><b>Legal values</b>:
     *   <dl>
     *     <dt>S_OK</dt>
     *     <dd>The grabbing object was correctly set</dd>
     *     <dt>E_FAIL </dt>
     *     <dd>The grabbing object was not correctly set</dd>
     *   </dl>
     */
    virtual HRESULT SetGrabbingObj( CATIProduct *iGrabbingObj ) = 0;

    /**
     * Retrieves the grabbed objects for the activity.
     * @param oGrabbedObj
     *   The grabbed objects.
     * @return
     *   An HRESULT.
     *   <br><b>Legal values</b>:
     *   <dl>
     *     <dt>S_OK</dt>
     *     <dd>The grabbed objects were correctly retrieved</dd>
     *     <dt>E_FAIL </dt>
     *     <dd>The grabbed objects were not correctly retrieved</dd>
     *   </dl>
     */
    virtual HRESULT GetGrabbedObj( CATListValCATBaseUnknown_var &oGrabbedObj ) = 0;

    /**
     * Defines the grabbed objects for the activity.
     * @param iGrabbedObj
     *   The grabbed objects.
     * @return
     *   An HRESULT.
     *   <br><b>Legal values</b>:
     *   <dl>
     *     <dt>S_OK</dt>
     *     <dd>The grabbed objects were correctly set</dd>
     *     <dt>E_FAIL </dt>
     *     <dd>The grabbed objects were not correctly set</dd>
     *   </dl>
     */
    virtual HRESULT SetGrabbedObj( CATListValCATBaseUnknown_var &iGrabbedObj ) = 0;

#ifndef DNB_DEPRECIATE
    /**
     * @nodoc
     * This method is deprecated. DO NOT USE!
     */
    virtual CATIProduct_var GetGrabbingObj() = 0;
    /**
     * @nodoc
     * This method is deprecated. DO NOT USE!
     */
    virtual CATIProduct_var GetGrabbedObj() = 0;
    /**
     * @nodoc
     * This method is deprecated. DO NOT USE!
     */
    virtual HRESULT GetGrabbedObj( CATIProduct **oGrabbedObj ) = 0;
    
    /**
     * @nodoc
     * This method is deprecated. DO NOT USE!
     */
    virtual void SetGrabbingObj(CATIProduct_var& grabbingPart ) = 0;
    /**
     * @nodoc
     * This method is deprecated. DO NOT USE!
     */
    virtual void SetGrabbedObj(CATIProduct_var& grabbedPart ) = 0;
    /**
     * @nodoc
     * This method is deprecated. DO NOT USE!
     */
    virtual HRESULT SetGrabbedObj( CATIProduct *iGrabbedObj ) = 0;
#endif  /* DNB_DEPRECIATE */

    // No constructors or destructors on this pure virtual base class
    // --------------------------------------------------------------
};

/**
 * @nodoc
 */
CATDeclareHandler( DNBIGrabActivity, CATBaseUnknown );

//------------------------------------------------------------------

#endif
