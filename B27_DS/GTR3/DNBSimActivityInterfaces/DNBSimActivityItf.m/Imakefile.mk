#
#   Imakefile.mk for DNBSimActivityItf.m
#   Copyright (C) DELMIA Corp., 1999
#

BUILT_OBJECT_TYPE = SHARED LIBRARY
#
LINK_WITH =  JS0GROUP                    \ # System
             JS0CORBA                    \ # System
             ProcessInterfaces           \ # DMAPSInterfaces
             CATProductStructureInterfaces \ # ProductStructureInterfaces


#
INCLUDED_MODULES = DNBSimActivityItfCPP DNBSimActivityProIDL DNBSimActivityPubIDL

#
# System dependent variables
#
OS = Windows_NT
BUILD=YES
#
OS = IRIX
BUILD=YES
#
OS = AIX
BUILD=YES
#
OS = SunOS
BUILD=YES
#
OS = HP-UX
BUILD=YES

