# COPYRIGHT DASSAULT SYSTEMES 2007
#======================================================================
# Imakefile for module DNBSimActivityProIDL
# Module for compilation of the public IDL interfaces
#======================================================================
#
#  Jan 2007  Creation:                                          ATJ
#======================================================================
#
# NO BUILD             
#

BUILT_OBJECT_TYPE=NONE

SOURCES_PATH=PublicInterfaces
COMPILATION_IDL=YES
