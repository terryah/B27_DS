#ifndef __TIE_DNBIAAnnotationActivity2
#define __TIE_DNBIAAnnotationActivity2

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIAAnnotationActivity2.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIAAnnotationActivity2 */
#define declare_TIE_DNBIAAnnotationActivity2(classe) \
 \
 \
class TIEDNBIAAnnotationActivity2##classe : public DNBIAAnnotationActivity2 \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIAAnnotationActivity2, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall get_VisibilityStatus(CAT_VARIANT_BOOL & oVisibility); \
      virtual HRESULT __stdcall put_VisibilityStatus(CAT_VARIANT_BOOL iVisibility); \
      virtual HRESULT __stdcall GetFTAList(DNBIAFTAList2 *& oTPSObjects); \
      virtual HRESULT __stdcall get_CompletionStatus(DNBAnnotationStatus2 & oStatus); \
      virtual HRESULT __stdcall put_CompletionStatus(DNBAnnotationStatus2 iStatus); \
      virtual HRESULT __stdcall GetAnnotatedView(CATVariant & oAnnotatedView); \
      virtual HRESULT __stdcall SetAnnotatedView(const CATVariant & iAnnotatedView); \
      virtual HRESULT __stdcall GetMarker3DsList(CATSafeArrayVariant & oMarker3Ds); \
      virtual HRESULT __stdcall SetMarker3DsList(const CATSafeArrayVariant & iMarker3Ds); \
      virtual HRESULT __stdcall RemoveMarker3DsList(const CATSafeArrayVariant & iMarker3Ds); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIAAnnotationActivity2(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall get_VisibilityStatus(CAT_VARIANT_BOOL & oVisibility); \
virtual HRESULT __stdcall put_VisibilityStatus(CAT_VARIANT_BOOL iVisibility); \
virtual HRESULT __stdcall GetFTAList(DNBIAFTAList2 *& oTPSObjects); \
virtual HRESULT __stdcall get_CompletionStatus(DNBAnnotationStatus2 & oStatus); \
virtual HRESULT __stdcall put_CompletionStatus(DNBAnnotationStatus2 iStatus); \
virtual HRESULT __stdcall GetAnnotatedView(CATVariant & oAnnotatedView); \
virtual HRESULT __stdcall SetAnnotatedView(const CATVariant & iAnnotatedView); \
virtual HRESULT __stdcall GetMarker3DsList(CATSafeArrayVariant & oMarker3Ds); \
virtual HRESULT __stdcall SetMarker3DsList(const CATSafeArrayVariant & iMarker3Ds); \
virtual HRESULT __stdcall RemoveMarker3DsList(const CATSafeArrayVariant & iMarker3Ds); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIAAnnotationActivity2(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::get_VisibilityStatus(CAT_VARIANT_BOOL & oVisibility) \
{ \
return (ENVTIECALL(DNBIAAnnotationActivity2,ENVTIETypeLetter,ENVTIELetter)get_VisibilityStatus(oVisibility)); \
} \
HRESULT __stdcall  ENVTIEName::put_VisibilityStatus(CAT_VARIANT_BOOL iVisibility) \
{ \
return (ENVTIECALL(DNBIAAnnotationActivity2,ENVTIETypeLetter,ENVTIELetter)put_VisibilityStatus(iVisibility)); \
} \
HRESULT __stdcall  ENVTIEName::GetFTAList(DNBIAFTAList2 *& oTPSObjects) \
{ \
return (ENVTIECALL(DNBIAAnnotationActivity2,ENVTIETypeLetter,ENVTIELetter)GetFTAList(oTPSObjects)); \
} \
HRESULT __stdcall  ENVTIEName::get_CompletionStatus(DNBAnnotationStatus2 & oStatus) \
{ \
return (ENVTIECALL(DNBIAAnnotationActivity2,ENVTIETypeLetter,ENVTIELetter)get_CompletionStatus(oStatus)); \
} \
HRESULT __stdcall  ENVTIEName::put_CompletionStatus(DNBAnnotationStatus2 iStatus) \
{ \
return (ENVTIECALL(DNBIAAnnotationActivity2,ENVTIETypeLetter,ENVTIELetter)put_CompletionStatus(iStatus)); \
} \
HRESULT __stdcall  ENVTIEName::GetAnnotatedView(CATVariant & oAnnotatedView) \
{ \
return (ENVTIECALL(DNBIAAnnotationActivity2,ENVTIETypeLetter,ENVTIELetter)GetAnnotatedView(oAnnotatedView)); \
} \
HRESULT __stdcall  ENVTIEName::SetAnnotatedView(const CATVariant & iAnnotatedView) \
{ \
return (ENVTIECALL(DNBIAAnnotationActivity2,ENVTIETypeLetter,ENVTIELetter)SetAnnotatedView(iAnnotatedView)); \
} \
HRESULT __stdcall  ENVTIEName::GetMarker3DsList(CATSafeArrayVariant & oMarker3Ds) \
{ \
return (ENVTIECALL(DNBIAAnnotationActivity2,ENVTIETypeLetter,ENVTIELetter)GetMarker3DsList(oMarker3Ds)); \
} \
HRESULT __stdcall  ENVTIEName::SetMarker3DsList(const CATSafeArrayVariant & iMarker3Ds) \
{ \
return (ENVTIECALL(DNBIAAnnotationActivity2,ENVTIETypeLetter,ENVTIELetter)SetMarker3DsList(iMarker3Ds)); \
} \
HRESULT __stdcall  ENVTIEName::RemoveMarker3DsList(const CATSafeArrayVariant & iMarker3Ds) \
{ \
return (ENVTIECALL(DNBIAAnnotationActivity2,ENVTIETypeLetter,ENVTIELetter)RemoveMarker3DsList(iMarker3Ds)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIAAnnotationActivity2,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIAAnnotationActivity2,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIAAnnotationActivity2,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIAAnnotationActivity2,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIAAnnotationActivity2,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIAAnnotationActivity2(classe)    TIEDNBIAAnnotationActivity2##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIAAnnotationActivity2(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIAAnnotationActivity2, classe) \
 \
 \
CATImplementTIEMethods(DNBIAAnnotationActivity2, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIAAnnotationActivity2, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIAAnnotationActivity2, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIAAnnotationActivity2, classe) \
 \
HRESULT __stdcall  TIEDNBIAAnnotationActivity2##classe::get_VisibilityStatus(CAT_VARIANT_BOOL & oVisibility) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&oVisibility); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_VisibilityStatus(oVisibility); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&oVisibility); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnnotationActivity2##classe::put_VisibilityStatus(CAT_VARIANT_BOOL iVisibility) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&iVisibility); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_VisibilityStatus(iVisibility); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&iVisibility); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnnotationActivity2##classe::GetFTAList(DNBIAFTAList2 *& oTPSObjects) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&oTPSObjects); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetFTAList(oTPSObjects); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&oTPSObjects); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnnotationActivity2##classe::get_CompletionStatus(DNBAnnotationStatus2 & oStatus) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&oStatus); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_CompletionStatus(oStatus); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&oStatus); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnnotationActivity2##classe::put_CompletionStatus(DNBAnnotationStatus2 iStatus) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&iStatus); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_CompletionStatus(iStatus); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&iStatus); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnnotationActivity2##classe::GetAnnotatedView(CATVariant & oAnnotatedView) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&oAnnotatedView); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAnnotatedView(oAnnotatedView); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&oAnnotatedView); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnnotationActivity2##classe::SetAnnotatedView(const CATVariant & iAnnotatedView) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&iAnnotatedView); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetAnnotatedView(iAnnotatedView); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&iAnnotatedView); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnnotationActivity2##classe::GetMarker3DsList(CATSafeArrayVariant & oMarker3Ds) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&oMarker3Ds); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetMarker3DsList(oMarker3Ds); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&oMarker3Ds); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnnotationActivity2##classe::SetMarker3DsList(const CATSafeArrayVariant & iMarker3Ds) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&iMarker3Ds); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetMarker3DsList(iMarker3Ds); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&iMarker3Ds); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAAnnotationActivity2##classe::RemoveMarker3DsList(const CATSafeArrayVariant & iMarker3Ds) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&iMarker3Ds); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->RemoveMarker3DsList(iMarker3Ds); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&iMarker3Ds); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAAnnotationActivity2##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,11,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,11,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAAnnotationActivity2##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,12,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,12,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAAnnotationActivity2##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,13,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,13,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAAnnotationActivity2##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,14,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,14,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAAnnotationActivity2##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,15,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,15,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIAAnnotationActivity2(classe) \
 \
 \
declare_TIE_DNBIAAnnotationActivity2(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAAnnotationActivity2##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAAnnotationActivity2,"DNBIAAnnotationActivity2",DNBIAAnnotationActivity2::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAAnnotationActivity2(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIAAnnotationActivity2, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAAnnotationActivity2##classe(classe::MetaObject(),DNBIAAnnotationActivity2::MetaObject(),(void *)CreateTIEDNBIAAnnotationActivity2##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIAAnnotationActivity2(classe) \
 \
 \
declare_TIE_DNBIAAnnotationActivity2(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAAnnotationActivity2##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAAnnotationActivity2,"DNBIAAnnotationActivity2",DNBIAAnnotationActivity2::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAAnnotationActivity2(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIAAnnotationActivity2, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAAnnotationActivity2##classe(classe::MetaObject(),DNBIAAnnotationActivity2::MetaObject(),(void *)CreateTIEDNBIAAnnotationActivity2##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIAAnnotationActivity2(classe) TIE_DNBIAAnnotationActivity2(classe)
#else
#define BOA_DNBIAAnnotationActivity2(classe) CATImplementBOA(DNBIAAnnotationActivity2, classe)
#endif

#endif
