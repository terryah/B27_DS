#ifndef __TIE_DELMIAGrabActivity
#define __TIE_DELMIAGrabActivity

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "DELMIAGrabActivity.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DELMIAGrabActivity */
#define declare_TIE_DELMIAGrabActivity(classe) \
 \
 \
class TIEDELMIAGrabActivity##classe : public DELMIAGrabActivity \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DELMIAGrabActivity, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall get_GrabbingObject(CATIAProduct *& oGrabbingObject); \
      virtual HRESULT __stdcall put_GrabbingObject(CATIAProduct * iGrabbingObject); \
      virtual HRESULT __stdcall GetGrabbedObjects(CATSafeArrayVariant & oGrabbedObjects); \
      virtual HRESULT __stdcall PutGrabbedObjects(const CATSafeArrayVariant & iGrabbedObjects); \
      virtual HRESULT __stdcall IsSubTypeOf(const CATBSTR & iName, CAT_VARIANT_BOOL & oVal); \
      virtual HRESULT __stdcall AttrValue(const CATVariant & iIndex, CATVariant & oAttVal); \
      virtual HRESULT __stdcall AttrName(CATLONG iIndex, CATBSTR & oName); \
      virtual HRESULT __stdcall get_Type(CATBSTR & oType); \
      virtual HRESULT __stdcall get_Description(CATBSTR & oDescriptionBSTR); \
      virtual HRESULT __stdcall put_Description(const CATBSTR & iDescriptionBSTR); \
      virtual HRESULT __stdcall get_CycleTime(double & oCT); \
      virtual HRESULT __stdcall put_CycleTime(double iCT); \
      virtual HRESULT __stdcall get_CalculatedCycleTime(double & oCCT); \
      virtual HRESULT __stdcall get_BeginningDate(double & oBegin); \
      virtual HRESULT __stdcall put_BeginningDate(double iSBT); \
      virtual HRESULT __stdcall get_EndDate(double & oEnd); \
      virtual HRESULT __stdcall get_ChildrenActivities(CATIAActivities *& oChildren); \
      virtual HRESULT __stdcall CreateChild(const CATBSTR & iTypeOfChild, CATIAActivity *& oCreatedChild); \
      virtual HRESULT __stdcall CreateLink(CATIAActivity * iSecondActivity); \
      virtual HRESULT __stdcall RemoveLink(CATIAActivity * iSecondActivity); \
      virtual HRESULT __stdcall get_NextCFActivities(CATIAActivities *& oNextCF); \
      virtual HRESULT __stdcall get_PreviousCFActivities(CATIAActivities *& oPreviousCF); \
      virtual HRESULT __stdcall get_NextPRFActivities(CATIAActivities *& oNextPRF); \
      virtual HRESULT __stdcall get_PreviousPRFActivities(CATIAActivities *& oPreviousPRF); \
      virtual HRESULT __stdcall get_AttrCount(CATLONG & oNbAttr); \
      virtual HRESULT __stdcall get_Items(CATIAItems *& oItems); \
      virtual HRESULT __stdcall get_Outputs(CATIAOutputs *& oOutputs); \
      virtual HRESULT __stdcall get_Resources(CATIAResources *& oResources); \
      virtual HRESULT __stdcall get_Relations(CATIARelations *& oRelations); \
      virtual HRESULT __stdcall get_Parameters(CATIAParameters *& oParameters); \
      virtual HRESULT __stdcall GetTechnologicalObject(const CATBSTR & iApplicationType, CATBaseDispatch *& oApplicativeObj); \
      virtual HRESULT __stdcall get_PrecedenceActivities(CATIAActivities *& oActivities); \
      virtual HRESULT __stdcall get_PossiblePrecedenceActivities(CATIAActivities *& oActivities); \
      virtual HRESULT __stdcall get_ProcessID(CATBSTR & oProcessID); \
      virtual HRESULT __stdcall SetProcessID(const CATBSTR & iProcessID, CAT_VARIANT_BOOL iCheckUnique); \
      virtual HRESULT __stdcall get_CalculatedBeginTime(double & oCBT); \
      virtual HRESULT __stdcall AddAttr(const CATBSTR & iAttributeName, SPPProcessAttributeType AttrType, const CATBSTR & iAttributePromptName); \
      virtual HRESULT __stdcall RemoveAttr(const CATBSTR & iAttributeName); \
      virtual HRESULT __stdcall AddActivityConstraint(CATIAActivity * iActivity, SPPProcessConstraintType iConstraintType); \
      virtual HRESULT __stdcall RemoveActivityConstraint(CATIAActivity * iActivity, SPPProcessConstraintType iConstraintType); \
      virtual HRESULT __stdcall GetActivityConstraints(SPPProcessConstraintType iConstraintType, CATIAActivities *& oConstrtList); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DELMIAGrabActivity(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall get_GrabbingObject(CATIAProduct *& oGrabbingObject); \
virtual HRESULT __stdcall put_GrabbingObject(CATIAProduct * iGrabbingObject); \
virtual HRESULT __stdcall GetGrabbedObjects(CATSafeArrayVariant & oGrabbedObjects); \
virtual HRESULT __stdcall PutGrabbedObjects(const CATSafeArrayVariant & iGrabbedObjects); \
virtual HRESULT __stdcall IsSubTypeOf(const CATBSTR & iName, CAT_VARIANT_BOOL & oVal); \
virtual HRESULT __stdcall AttrValue(const CATVariant & iIndex, CATVariant & oAttVal); \
virtual HRESULT __stdcall AttrName(CATLONG iIndex, CATBSTR & oName); \
virtual HRESULT __stdcall get_Type(CATBSTR & oType); \
virtual HRESULT __stdcall get_Description(CATBSTR & oDescriptionBSTR); \
virtual HRESULT __stdcall put_Description(const CATBSTR & iDescriptionBSTR); \
virtual HRESULT __stdcall get_CycleTime(double & oCT); \
virtual HRESULT __stdcall put_CycleTime(double iCT); \
virtual HRESULT __stdcall get_CalculatedCycleTime(double & oCCT); \
virtual HRESULT __stdcall get_BeginningDate(double & oBegin); \
virtual HRESULT __stdcall put_BeginningDate(double iSBT); \
virtual HRESULT __stdcall get_EndDate(double & oEnd); \
virtual HRESULT __stdcall get_ChildrenActivities(CATIAActivities *& oChildren); \
virtual HRESULT __stdcall CreateChild(const CATBSTR & iTypeOfChild, CATIAActivity *& oCreatedChild); \
virtual HRESULT __stdcall CreateLink(CATIAActivity * iSecondActivity); \
virtual HRESULT __stdcall RemoveLink(CATIAActivity * iSecondActivity); \
virtual HRESULT __stdcall get_NextCFActivities(CATIAActivities *& oNextCF); \
virtual HRESULT __stdcall get_PreviousCFActivities(CATIAActivities *& oPreviousCF); \
virtual HRESULT __stdcall get_NextPRFActivities(CATIAActivities *& oNextPRF); \
virtual HRESULT __stdcall get_PreviousPRFActivities(CATIAActivities *& oPreviousPRF); \
virtual HRESULT __stdcall get_AttrCount(CATLONG & oNbAttr); \
virtual HRESULT __stdcall get_Items(CATIAItems *& oItems); \
virtual HRESULT __stdcall get_Outputs(CATIAOutputs *& oOutputs); \
virtual HRESULT __stdcall get_Resources(CATIAResources *& oResources); \
virtual HRESULT __stdcall get_Relations(CATIARelations *& oRelations); \
virtual HRESULT __stdcall get_Parameters(CATIAParameters *& oParameters); \
virtual HRESULT __stdcall GetTechnologicalObject(const CATBSTR & iApplicationType, CATBaseDispatch *& oApplicativeObj); \
virtual HRESULT __stdcall get_PrecedenceActivities(CATIAActivities *& oActivities); \
virtual HRESULT __stdcall get_PossiblePrecedenceActivities(CATIAActivities *& oActivities); \
virtual HRESULT __stdcall get_ProcessID(CATBSTR & oProcessID); \
virtual HRESULT __stdcall SetProcessID(const CATBSTR & iProcessID, CAT_VARIANT_BOOL iCheckUnique); \
virtual HRESULT __stdcall get_CalculatedBeginTime(double & oCBT); \
virtual HRESULT __stdcall AddAttr(const CATBSTR & iAttributeName, SPPProcessAttributeType AttrType, const CATBSTR & iAttributePromptName); \
virtual HRESULT __stdcall RemoveAttr(const CATBSTR & iAttributeName); \
virtual HRESULT __stdcall AddActivityConstraint(CATIAActivity * iActivity, SPPProcessConstraintType iConstraintType); \
virtual HRESULT __stdcall RemoveActivityConstraint(CATIAActivity * iActivity, SPPProcessConstraintType iConstraintType); \
virtual HRESULT __stdcall GetActivityConstraints(SPPProcessConstraintType iConstraintType, CATIAActivities *& oConstrtList); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DELMIAGrabActivity(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::get_GrabbingObject(CATIAProduct *& oGrabbingObject) \
{ \
return (ENVTIECALL(DELMIAGrabActivity,ENVTIETypeLetter,ENVTIELetter)get_GrabbingObject(oGrabbingObject)); \
} \
HRESULT __stdcall  ENVTIEName::put_GrabbingObject(CATIAProduct * iGrabbingObject) \
{ \
return (ENVTIECALL(DELMIAGrabActivity,ENVTIETypeLetter,ENVTIELetter)put_GrabbingObject(iGrabbingObject)); \
} \
HRESULT __stdcall  ENVTIEName::GetGrabbedObjects(CATSafeArrayVariant & oGrabbedObjects) \
{ \
return (ENVTIECALL(DELMIAGrabActivity,ENVTIETypeLetter,ENVTIELetter)GetGrabbedObjects(oGrabbedObjects)); \
} \
HRESULT __stdcall  ENVTIEName::PutGrabbedObjects(const CATSafeArrayVariant & iGrabbedObjects) \
{ \
return (ENVTIECALL(DELMIAGrabActivity,ENVTIETypeLetter,ENVTIELetter)PutGrabbedObjects(iGrabbedObjects)); \
} \
HRESULT __stdcall  ENVTIEName::IsSubTypeOf(const CATBSTR & iName, CAT_VARIANT_BOOL & oVal) \
{ \
return (ENVTIECALL(DELMIAGrabActivity,ENVTIETypeLetter,ENVTIELetter)IsSubTypeOf(iName,oVal)); \
} \
HRESULT __stdcall  ENVTIEName::AttrValue(const CATVariant & iIndex, CATVariant & oAttVal) \
{ \
return (ENVTIECALL(DELMIAGrabActivity,ENVTIETypeLetter,ENVTIELetter)AttrValue(iIndex,oAttVal)); \
} \
HRESULT __stdcall  ENVTIEName::AttrName(CATLONG iIndex, CATBSTR & oName) \
{ \
return (ENVTIECALL(DELMIAGrabActivity,ENVTIETypeLetter,ENVTIELetter)AttrName(iIndex,oName)); \
} \
HRESULT __stdcall  ENVTIEName::get_Type(CATBSTR & oType) \
{ \
return (ENVTIECALL(DELMIAGrabActivity,ENVTIETypeLetter,ENVTIELetter)get_Type(oType)); \
} \
HRESULT __stdcall  ENVTIEName::get_Description(CATBSTR & oDescriptionBSTR) \
{ \
return (ENVTIECALL(DELMIAGrabActivity,ENVTIETypeLetter,ENVTIELetter)get_Description(oDescriptionBSTR)); \
} \
HRESULT __stdcall  ENVTIEName::put_Description(const CATBSTR & iDescriptionBSTR) \
{ \
return (ENVTIECALL(DELMIAGrabActivity,ENVTIETypeLetter,ENVTIELetter)put_Description(iDescriptionBSTR)); \
} \
HRESULT __stdcall  ENVTIEName::get_CycleTime(double & oCT) \
{ \
return (ENVTIECALL(DELMIAGrabActivity,ENVTIETypeLetter,ENVTIELetter)get_CycleTime(oCT)); \
} \
HRESULT __stdcall  ENVTIEName::put_CycleTime(double iCT) \
{ \
return (ENVTIECALL(DELMIAGrabActivity,ENVTIETypeLetter,ENVTIELetter)put_CycleTime(iCT)); \
} \
HRESULT __stdcall  ENVTIEName::get_CalculatedCycleTime(double & oCCT) \
{ \
return (ENVTIECALL(DELMIAGrabActivity,ENVTIETypeLetter,ENVTIELetter)get_CalculatedCycleTime(oCCT)); \
} \
HRESULT __stdcall  ENVTIEName::get_BeginningDate(double & oBegin) \
{ \
return (ENVTIECALL(DELMIAGrabActivity,ENVTIETypeLetter,ENVTIELetter)get_BeginningDate(oBegin)); \
} \
HRESULT __stdcall  ENVTIEName::put_BeginningDate(double iSBT) \
{ \
return (ENVTIECALL(DELMIAGrabActivity,ENVTIETypeLetter,ENVTIELetter)put_BeginningDate(iSBT)); \
} \
HRESULT __stdcall  ENVTIEName::get_EndDate(double & oEnd) \
{ \
return (ENVTIECALL(DELMIAGrabActivity,ENVTIETypeLetter,ENVTIELetter)get_EndDate(oEnd)); \
} \
HRESULT __stdcall  ENVTIEName::get_ChildrenActivities(CATIAActivities *& oChildren) \
{ \
return (ENVTIECALL(DELMIAGrabActivity,ENVTIETypeLetter,ENVTIELetter)get_ChildrenActivities(oChildren)); \
} \
HRESULT __stdcall  ENVTIEName::CreateChild(const CATBSTR & iTypeOfChild, CATIAActivity *& oCreatedChild) \
{ \
return (ENVTIECALL(DELMIAGrabActivity,ENVTIETypeLetter,ENVTIELetter)CreateChild(iTypeOfChild,oCreatedChild)); \
} \
HRESULT __stdcall  ENVTIEName::CreateLink(CATIAActivity * iSecondActivity) \
{ \
return (ENVTIECALL(DELMIAGrabActivity,ENVTIETypeLetter,ENVTIELetter)CreateLink(iSecondActivity)); \
} \
HRESULT __stdcall  ENVTIEName::RemoveLink(CATIAActivity * iSecondActivity) \
{ \
return (ENVTIECALL(DELMIAGrabActivity,ENVTIETypeLetter,ENVTIELetter)RemoveLink(iSecondActivity)); \
} \
HRESULT __stdcall  ENVTIEName::get_NextCFActivities(CATIAActivities *& oNextCF) \
{ \
return (ENVTIECALL(DELMIAGrabActivity,ENVTIETypeLetter,ENVTIELetter)get_NextCFActivities(oNextCF)); \
} \
HRESULT __stdcall  ENVTIEName::get_PreviousCFActivities(CATIAActivities *& oPreviousCF) \
{ \
return (ENVTIECALL(DELMIAGrabActivity,ENVTIETypeLetter,ENVTIELetter)get_PreviousCFActivities(oPreviousCF)); \
} \
HRESULT __stdcall  ENVTIEName::get_NextPRFActivities(CATIAActivities *& oNextPRF) \
{ \
return (ENVTIECALL(DELMIAGrabActivity,ENVTIETypeLetter,ENVTIELetter)get_NextPRFActivities(oNextPRF)); \
} \
HRESULT __stdcall  ENVTIEName::get_PreviousPRFActivities(CATIAActivities *& oPreviousPRF) \
{ \
return (ENVTIECALL(DELMIAGrabActivity,ENVTIETypeLetter,ENVTIELetter)get_PreviousPRFActivities(oPreviousPRF)); \
} \
HRESULT __stdcall  ENVTIEName::get_AttrCount(CATLONG & oNbAttr) \
{ \
return (ENVTIECALL(DELMIAGrabActivity,ENVTIETypeLetter,ENVTIELetter)get_AttrCount(oNbAttr)); \
} \
HRESULT __stdcall  ENVTIEName::get_Items(CATIAItems *& oItems) \
{ \
return (ENVTIECALL(DELMIAGrabActivity,ENVTIETypeLetter,ENVTIELetter)get_Items(oItems)); \
} \
HRESULT __stdcall  ENVTIEName::get_Outputs(CATIAOutputs *& oOutputs) \
{ \
return (ENVTIECALL(DELMIAGrabActivity,ENVTIETypeLetter,ENVTIELetter)get_Outputs(oOutputs)); \
} \
HRESULT __stdcall  ENVTIEName::get_Resources(CATIAResources *& oResources) \
{ \
return (ENVTIECALL(DELMIAGrabActivity,ENVTIETypeLetter,ENVTIELetter)get_Resources(oResources)); \
} \
HRESULT __stdcall  ENVTIEName::get_Relations(CATIARelations *& oRelations) \
{ \
return (ENVTIECALL(DELMIAGrabActivity,ENVTIETypeLetter,ENVTIELetter)get_Relations(oRelations)); \
} \
HRESULT __stdcall  ENVTIEName::get_Parameters(CATIAParameters *& oParameters) \
{ \
return (ENVTIECALL(DELMIAGrabActivity,ENVTIETypeLetter,ENVTIELetter)get_Parameters(oParameters)); \
} \
HRESULT __stdcall  ENVTIEName::GetTechnologicalObject(const CATBSTR & iApplicationType, CATBaseDispatch *& oApplicativeObj) \
{ \
return (ENVTIECALL(DELMIAGrabActivity,ENVTIETypeLetter,ENVTIELetter)GetTechnologicalObject(iApplicationType,oApplicativeObj)); \
} \
HRESULT __stdcall  ENVTIEName::get_PrecedenceActivities(CATIAActivities *& oActivities) \
{ \
return (ENVTIECALL(DELMIAGrabActivity,ENVTIETypeLetter,ENVTIELetter)get_PrecedenceActivities(oActivities)); \
} \
HRESULT __stdcall  ENVTIEName::get_PossiblePrecedenceActivities(CATIAActivities *& oActivities) \
{ \
return (ENVTIECALL(DELMIAGrabActivity,ENVTIETypeLetter,ENVTIELetter)get_PossiblePrecedenceActivities(oActivities)); \
} \
HRESULT __stdcall  ENVTIEName::get_ProcessID(CATBSTR & oProcessID) \
{ \
return (ENVTIECALL(DELMIAGrabActivity,ENVTIETypeLetter,ENVTIELetter)get_ProcessID(oProcessID)); \
} \
HRESULT __stdcall  ENVTIEName::SetProcessID(const CATBSTR & iProcessID, CAT_VARIANT_BOOL iCheckUnique) \
{ \
return (ENVTIECALL(DELMIAGrabActivity,ENVTIETypeLetter,ENVTIELetter)SetProcessID(iProcessID,iCheckUnique)); \
} \
HRESULT __stdcall  ENVTIEName::get_CalculatedBeginTime(double & oCBT) \
{ \
return (ENVTIECALL(DELMIAGrabActivity,ENVTIETypeLetter,ENVTIELetter)get_CalculatedBeginTime(oCBT)); \
} \
HRESULT __stdcall  ENVTIEName::AddAttr(const CATBSTR & iAttributeName, SPPProcessAttributeType AttrType, const CATBSTR & iAttributePromptName) \
{ \
return (ENVTIECALL(DELMIAGrabActivity,ENVTIETypeLetter,ENVTIELetter)AddAttr(iAttributeName,AttrType,iAttributePromptName)); \
} \
HRESULT __stdcall  ENVTIEName::RemoveAttr(const CATBSTR & iAttributeName) \
{ \
return (ENVTIECALL(DELMIAGrabActivity,ENVTIETypeLetter,ENVTIELetter)RemoveAttr(iAttributeName)); \
} \
HRESULT __stdcall  ENVTIEName::AddActivityConstraint(CATIAActivity * iActivity, SPPProcessConstraintType iConstraintType) \
{ \
return (ENVTIECALL(DELMIAGrabActivity,ENVTIETypeLetter,ENVTIELetter)AddActivityConstraint(iActivity,iConstraintType)); \
} \
HRESULT __stdcall  ENVTIEName::RemoveActivityConstraint(CATIAActivity * iActivity, SPPProcessConstraintType iConstraintType) \
{ \
return (ENVTIECALL(DELMIAGrabActivity,ENVTIETypeLetter,ENVTIELetter)RemoveActivityConstraint(iActivity,iConstraintType)); \
} \
HRESULT __stdcall  ENVTIEName::GetActivityConstraints(SPPProcessConstraintType iConstraintType, CATIAActivities *& oConstrtList) \
{ \
return (ENVTIECALL(DELMIAGrabActivity,ENVTIETypeLetter,ENVTIELetter)GetActivityConstraints(iConstraintType,oConstrtList)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DELMIAGrabActivity,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DELMIAGrabActivity,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DELMIAGrabActivity,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DELMIAGrabActivity,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DELMIAGrabActivity,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DELMIAGrabActivity(classe)    TIEDELMIAGrabActivity##classe


/* Common methods inside a TIE */
#define common_TIE_DELMIAGrabActivity(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DELMIAGrabActivity, classe) \
 \
 \
CATImplementTIEMethods(DELMIAGrabActivity, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DELMIAGrabActivity, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DELMIAGrabActivity, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DELMIAGrabActivity, classe) \
 \
HRESULT __stdcall  TIEDELMIAGrabActivity##classe::get_GrabbingObject(CATIAProduct *& oGrabbingObject) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_GrabbingObject(oGrabbingObject)); \
} \
HRESULT __stdcall  TIEDELMIAGrabActivity##classe::put_GrabbingObject(CATIAProduct * iGrabbingObject) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_GrabbingObject(iGrabbingObject)); \
} \
HRESULT __stdcall  TIEDELMIAGrabActivity##classe::GetGrabbedObjects(CATSafeArrayVariant & oGrabbedObjects) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetGrabbedObjects(oGrabbedObjects)); \
} \
HRESULT __stdcall  TIEDELMIAGrabActivity##classe::PutGrabbedObjects(const CATSafeArrayVariant & iGrabbedObjects) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->PutGrabbedObjects(iGrabbedObjects)); \
} \
HRESULT __stdcall  TIEDELMIAGrabActivity##classe::IsSubTypeOf(const CATBSTR & iName, CAT_VARIANT_BOOL & oVal) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->IsSubTypeOf(iName,oVal)); \
} \
HRESULT __stdcall  TIEDELMIAGrabActivity##classe::AttrValue(const CATVariant & iIndex, CATVariant & oAttVal) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->AttrValue(iIndex,oAttVal)); \
} \
HRESULT __stdcall  TIEDELMIAGrabActivity##classe::AttrName(CATLONG iIndex, CATBSTR & oName) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->AttrName(iIndex,oName)); \
} \
HRESULT __stdcall  TIEDELMIAGrabActivity##classe::get_Type(CATBSTR & oType) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Type(oType)); \
} \
HRESULT __stdcall  TIEDELMIAGrabActivity##classe::get_Description(CATBSTR & oDescriptionBSTR) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Description(oDescriptionBSTR)); \
} \
HRESULT __stdcall  TIEDELMIAGrabActivity##classe::put_Description(const CATBSTR & iDescriptionBSTR) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Description(iDescriptionBSTR)); \
} \
HRESULT __stdcall  TIEDELMIAGrabActivity##classe::get_CycleTime(double & oCT) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_CycleTime(oCT)); \
} \
HRESULT __stdcall  TIEDELMIAGrabActivity##classe::put_CycleTime(double iCT) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_CycleTime(iCT)); \
} \
HRESULT __stdcall  TIEDELMIAGrabActivity##classe::get_CalculatedCycleTime(double & oCCT) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_CalculatedCycleTime(oCCT)); \
} \
HRESULT __stdcall  TIEDELMIAGrabActivity##classe::get_BeginningDate(double & oBegin) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_BeginningDate(oBegin)); \
} \
HRESULT __stdcall  TIEDELMIAGrabActivity##classe::put_BeginningDate(double iSBT) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_BeginningDate(iSBT)); \
} \
HRESULT __stdcall  TIEDELMIAGrabActivity##classe::get_EndDate(double & oEnd) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_EndDate(oEnd)); \
} \
HRESULT __stdcall  TIEDELMIAGrabActivity##classe::get_ChildrenActivities(CATIAActivities *& oChildren) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_ChildrenActivities(oChildren)); \
} \
HRESULT __stdcall  TIEDELMIAGrabActivity##classe::CreateChild(const CATBSTR & iTypeOfChild, CATIAActivity *& oCreatedChild) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateChild(iTypeOfChild,oCreatedChild)); \
} \
HRESULT __stdcall  TIEDELMIAGrabActivity##classe::CreateLink(CATIAActivity * iSecondActivity) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->CreateLink(iSecondActivity)); \
} \
HRESULT __stdcall  TIEDELMIAGrabActivity##classe::RemoveLink(CATIAActivity * iSecondActivity) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->RemoveLink(iSecondActivity)); \
} \
HRESULT __stdcall  TIEDELMIAGrabActivity##classe::get_NextCFActivities(CATIAActivities *& oNextCF) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_NextCFActivities(oNextCF)); \
} \
HRESULT __stdcall  TIEDELMIAGrabActivity##classe::get_PreviousCFActivities(CATIAActivities *& oPreviousCF) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_PreviousCFActivities(oPreviousCF)); \
} \
HRESULT __stdcall  TIEDELMIAGrabActivity##classe::get_NextPRFActivities(CATIAActivities *& oNextPRF) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_NextPRFActivities(oNextPRF)); \
} \
HRESULT __stdcall  TIEDELMIAGrabActivity##classe::get_PreviousPRFActivities(CATIAActivities *& oPreviousPRF) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_PreviousPRFActivities(oPreviousPRF)); \
} \
HRESULT __stdcall  TIEDELMIAGrabActivity##classe::get_AttrCount(CATLONG & oNbAttr) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_AttrCount(oNbAttr)); \
} \
HRESULT __stdcall  TIEDELMIAGrabActivity##classe::get_Items(CATIAItems *& oItems) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Items(oItems)); \
} \
HRESULT __stdcall  TIEDELMIAGrabActivity##classe::get_Outputs(CATIAOutputs *& oOutputs) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Outputs(oOutputs)); \
} \
HRESULT __stdcall  TIEDELMIAGrabActivity##classe::get_Resources(CATIAResources *& oResources) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Resources(oResources)); \
} \
HRESULT __stdcall  TIEDELMIAGrabActivity##classe::get_Relations(CATIARelations *& oRelations) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Relations(oRelations)); \
} \
HRESULT __stdcall  TIEDELMIAGrabActivity##classe::get_Parameters(CATIAParameters *& oParameters) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parameters(oParameters)); \
} \
HRESULT __stdcall  TIEDELMIAGrabActivity##classe::GetTechnologicalObject(const CATBSTR & iApplicationType, CATBaseDispatch *& oApplicativeObj) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetTechnologicalObject(iApplicationType,oApplicativeObj)); \
} \
HRESULT __stdcall  TIEDELMIAGrabActivity##classe::get_PrecedenceActivities(CATIAActivities *& oActivities) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_PrecedenceActivities(oActivities)); \
} \
HRESULT __stdcall  TIEDELMIAGrabActivity##classe::get_PossiblePrecedenceActivities(CATIAActivities *& oActivities) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_PossiblePrecedenceActivities(oActivities)); \
} \
HRESULT __stdcall  TIEDELMIAGrabActivity##classe::get_ProcessID(CATBSTR & oProcessID) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_ProcessID(oProcessID)); \
} \
HRESULT __stdcall  TIEDELMIAGrabActivity##classe::SetProcessID(const CATBSTR & iProcessID, CAT_VARIANT_BOOL iCheckUnique) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->SetProcessID(iProcessID,iCheckUnique)); \
} \
HRESULT __stdcall  TIEDELMIAGrabActivity##classe::get_CalculatedBeginTime(double & oCBT) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_CalculatedBeginTime(oCBT)); \
} \
HRESULT __stdcall  TIEDELMIAGrabActivity##classe::AddAttr(const CATBSTR & iAttributeName, SPPProcessAttributeType AttrType, const CATBSTR & iAttributePromptName) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->AddAttr(iAttributeName,AttrType,iAttributePromptName)); \
} \
HRESULT __stdcall  TIEDELMIAGrabActivity##classe::RemoveAttr(const CATBSTR & iAttributeName) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->RemoveAttr(iAttributeName)); \
} \
HRESULT __stdcall  TIEDELMIAGrabActivity##classe::AddActivityConstraint(CATIAActivity * iActivity, SPPProcessConstraintType iConstraintType) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->AddActivityConstraint(iActivity,iConstraintType)); \
} \
HRESULT __stdcall  TIEDELMIAGrabActivity##classe::RemoveActivityConstraint(CATIAActivity * iActivity, SPPProcessConstraintType iConstraintType) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->RemoveActivityConstraint(iActivity,iConstraintType)); \
} \
HRESULT __stdcall  TIEDELMIAGrabActivity##classe::GetActivityConstraints(SPPProcessConstraintType iConstraintType, CATIAActivities *& oConstrtList) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetActivityConstraints(iConstraintType,oConstrtList)); \
} \
HRESULT  __stdcall  TIEDELMIAGrabActivity##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication)); \
} \
HRESULT  __stdcall  TIEDELMIAGrabActivity##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent)); \
} \
HRESULT  __stdcall  TIEDELMIAGrabActivity##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  TIEDELMIAGrabActivity##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  TIEDELMIAGrabActivity##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj)); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DELMIAGrabActivity(classe) \
 \
 \
declare_TIE_DELMIAGrabActivity(classe) \
 \
 \
CATMetaClass * __stdcall TIEDELMIAGrabActivity##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DELMIAGrabActivity,"DELMIAGrabActivity",DELMIAGrabActivity::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DELMIAGrabActivity(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DELMIAGrabActivity, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDELMIAGrabActivity##classe(classe::MetaObject(),DELMIAGrabActivity::MetaObject(),(void *)CreateTIEDELMIAGrabActivity##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DELMIAGrabActivity(classe) \
 \
 \
declare_TIE_DELMIAGrabActivity(classe) \
 \
 \
CATMetaClass * __stdcall TIEDELMIAGrabActivity##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DELMIAGrabActivity,"DELMIAGrabActivity",DELMIAGrabActivity::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DELMIAGrabActivity(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DELMIAGrabActivity, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDELMIAGrabActivity##classe(classe::MetaObject(),DELMIAGrabActivity::MetaObject(),(void *)CreateTIEDELMIAGrabActivity##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DELMIAGrabActivity(classe) TIE_DELMIAGrabActivity(classe)
#else
#define BOA_DELMIAGrabActivity(classe) CATImplementBOA(DELMIAGrabActivity, classe)
#endif

#endif
