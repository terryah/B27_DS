#======================================================================
# COPYRIGHT DELMIA CORP. 2003
#======================================================================
# Imakefile for module DNBSimActTypeLib.m
# Module for compilation of the typelib
#======================================================================
#   Modification History:
#       cre     SAR     12/17/2003      Original implementation.
#       mod     BKH     04/14/2004      CATTPSInterfaces
#       mod     ATJ     01/08/2008      ProductStructureInterfaces
#======================================================================
#
# TYPELIB
#
#======================================================================

BUILT_OBJECT_TYPE=TYPELIB
# no more BUILD_PRIORITY=30

LINK_WITH =         InfTypeLib                      \ # InfInterfaces
                    ProcessTypeLib                  \ # DMAPSInterfaces
                    CATAnnotationTypeLib            \ # CATTPSInterfaces
                    PSTypeLib                       \ # ProductStructureInterfaces
