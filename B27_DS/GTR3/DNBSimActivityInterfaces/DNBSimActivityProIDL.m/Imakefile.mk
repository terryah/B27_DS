# COPYRIGHT DASSAULT SYSTEMES 2003
#======================================================================
# Imakefile for module DNBSimActivityProIDL
# Module for compilation of the protected IDL interfaces
#======================================================================
#
#  Jun 2002  Creation:										SAR
#======================================================================
#
# NO BUILD             
#

BUILT_OBJECT_TYPE=NONE

SOURCES_PATH=ProtectedInterfaces
COMPILATION_IDL=YES
