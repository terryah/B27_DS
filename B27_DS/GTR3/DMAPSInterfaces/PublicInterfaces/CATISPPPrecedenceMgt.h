// COPYRIGHT DASSAULT SYSTEMES 1999
//===================================================================
//
// CATISPPPrecedenceMgt.h
// Define the CATISPPPrecedenceMgt interface
//
//===================================================================
//
// Usage notes:
//   New interface: describe its use here
//
//===================================================================
//
//  Aug 1999  Creation: Code generated by the CAA wizard  PCM
//===================================================================
#ifndef CATISPPPrecedenceMgt_H
#define CATISPPPrecedenceMgt_H
/**
 * @CAA2Level L0
 * @CAA2Usage U0
 */

#include "ProcessInterfaces.h"
#include "CATBaseUnknown.h"

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByProcessInterfaces IID IID_CATISPPPrecedenceMgt ;
#else
extern "C" const IID IID_CATISPPPrecedenceMgt ;
#endif

//------------------------------------------------------------------
class CATISPPAbstractActivity;
class CATListValCATBaseUnknown_var;
//------------------------------------------------------------------
/**
* Interface to use to manage Precedence Constraints for any activity.
* <br><b>Role: </b> this interface allow user to add and remove a precedence constraints
*/
class ExportedByProcessInterfaces CATISPPPrecedenceMgt: public CATBaseUnknown
{
  CATDeclareInterface;

  public:

     /**
    * Add an activity as .Precedence Activity 
	 * @param iActivity
	 *        Activity Handle
    * @return
    *	<dt><tt>S_OK</tt>		On Success
    */
    virtual HRESULT AddPrevious(const CATISPPAbstractActivity* iActivity) = 0;
   /**
    * Remove an activity as .Precedence Activity 
	 * @param iActivity
	 *        Activity Handle
    * @return
    *	<dt><tt>S_OK</tt>		On Success
    */

    virtual HRESULT RemovePrevious(CATISPPAbstractActivity* iActivity) = 0;
      /**
    * Get List of Precedence Activity 
    * @return
    *	<dt><tt>List of Precedence Activity </tt>		
    */
    virtual CATListValCATBaseUnknown_var* ListPrevious() = 0;
    /**
    * Retrun the list of Possible Previous Activities 
	 * @return
    *	<dt><tt>Possible Previous Activities </tt>		
    */
    virtual CATListValCATBaseUnknown_var*   ListPossiblePrevious () = 0;
   
};
//------------------------------------------------------------------
CATDeclareHandler(CATISPPPrecedenceMgt,CATBaseUnknown);
#endif
