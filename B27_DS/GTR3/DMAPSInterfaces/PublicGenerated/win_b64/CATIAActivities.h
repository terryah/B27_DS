/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAActivities_h
#define CATIAActivities_h

#ifndef ExportedByProcessPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __ProcessPubIDL
#define ExportedByProcessPubIDL __declspec(dllexport)
#else
#define ExportedByProcessPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByProcessPubIDL
#endif
#endif

#include "CATIACollection.h"
#include "CATVariant.h"

class CATIAActivity;

extern ExportedByProcessPubIDL IID IID_CATIAActivities;

class ExportedByProcessPubIDL CATIAActivities : public CATIACollection
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall Item(const CATVariant & iIndex, CATIAActivity *& oActivity)=0;

    virtual HRESULT __stdcall Remove(const CATVariant & iIndex)=0;

    virtual HRESULT __stdcall Add(CATIAActivity * iActivity)=0;


};

CATDeclareHandler(CATIAActivities, CATIACollection);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
