/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAActivityAttributes_h
#define CATIAActivityAttributes_h

#ifndef ExportedByProcessPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __ProcessPubIDL
#define ExportedByProcessPubIDL __declspec(dllexport)
#else
#define ExportedByProcessPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByProcessPubIDL
#endif
#endif

#include "CATIABase.h"
#include "CATVariant.h"

extern ExportedByProcessPubIDL IID IID_CATIAActivityAttributes;

class ExportedByProcessPubIDL CATIAActivityAttributes : public CATIABase
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_AttrCount(CATLONG & oNbAttr)=0;

    virtual HRESULT __stdcall GetAttrValue(const CATVariant & iIndex, CATVariant & oAttVal)=0;

    virtual HRESULT __stdcall SetAttrValue(const CATVariant & iIndex, const CATVariant & iAttrVal)=0;


};

CATDeclareHandler(CATIAActivityAttributes, CATIABase);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
