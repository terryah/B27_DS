/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIATreeTabSettingAtt_h
#define CATIATreeTabSettingAtt_h

#include "CATCORBABoolean.h"
#include "CAT_VARIANT_BOOL.h"

#ifndef ExportedByProcessPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __ProcessPubIDL
#define ExportedByProcessPubIDL __declspec(dllexport)
#else
#define ExportedByProcessPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByProcessPubIDL
#endif
#endif

#include "CATBSTR.h"
#include "CATIASettingController.h"

extern ExportedByProcessPubIDL IID IID_CATIATreeTabSettingAtt;

class ExportedByProcessPubIDL CATIATreeTabSettingAtt : public CATIASettingController
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_ResourceFilter(CATLONG & oResFilter)=0;

    virtual HRESULT __stdcall put_ResourceFilter(CATLONG iResFilter)=0;

    virtual HRESULT __stdcall GetResourceFilterInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetResourceFilterLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_ItemsFilter(CATLONG & oItemsFilter)=0;

    virtual HRESULT __stdcall put_ItemsFilter(CATLONG iItemsFilter)=0;

    virtual HRESULT __stdcall GetItemsFilterInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetItemsFilterLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_ItemsPerRelationFilter(CATLONG & oItemsRelFilter)=0;

    virtual HRESULT __stdcall put_ItemsPerRelationFilter(CATLONG iItemsRelFilter)=0;

    virtual HRESULT __stdcall GetItemsPerRelationFilterInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetItemsPerRelationFilterLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_AttributesFilter(CATLONG & oItemsRelFilter)=0;

    virtual HRESULT __stdcall put_AttributesFilter(CATLONG iItemsRelFilter)=0;

    virtual HRESULT __stdcall GetAttributesFilterInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetAttributesFilterLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_ApplicativeDataFilter(CATLONG & ovalue)=0;

    virtual HRESULT __stdcall put_ApplicativeDataFilter(CATLONG ivalue)=0;

    virtual HRESULT __stdcall GetApplicativeDataFilterInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetApplicativeDataFilterLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_OutputProductFilter(CATLONG & ovalue)=0;

    virtual HRESULT __stdcall put_OutputProductFilter(CATLONG ivalue)=0;

    virtual HRESULT __stdcall GetOutputProductFilterInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetOutputProductFilterLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_LogicalActFilter(CATLONG & ovalue)=0;

    virtual HRESULT __stdcall put_LogicalActFilter(CATLONG ivalue)=0;

    virtual HRESULT __stdcall GetLogicalActFilterInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetLogicalActFilterLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_CollapseExpandFilter(CATLONG & ovalue)=0;

    virtual HRESULT __stdcall put_CollapseExpandFilter(CATLONG ivalue)=0;

    virtual HRESULT __stdcall GetCollapseExpandFilterInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetCollapseExpandFilterLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_AssignedViewer(CATLONG & ovalue)=0;

    virtual HRESULT __stdcall put_AssignedViewer(CATLONG ivalue)=0;

    virtual HRESULT __stdcall GetAssignedViewerInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetAssignedViewerLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_DisplayNameMode(CATLONG & ovalue)=0;

    virtual HRESULT __stdcall put_DisplayNameMode(CATLONG ivalue)=0;

    virtual HRESULT __stdcall GetDisplayNameModeInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetDisplayNameModeLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_RenderStyle(CATLONG & ovalue)=0;

    virtual HRESULT __stdcall put_RenderStyle(CATLONG ivalue)=0;

    virtual HRESULT __stdcall GetRenderStyleInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetRenderStyleLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_DisplayOrder(CATLONG & oDisplayOrder)=0;

    virtual HRESULT __stdcall put_DisplayOrder(CATLONG iDisplayOrder)=0;

    virtual HRESULT __stdcall GetDisplayOrderInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetDisplayOrderLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_DisplayProcessOrder(CATLONG & oDisplayOrder)=0;

    virtual HRESULT __stdcall put_DisplayProcessOrder(CATLONG iDisplayOrder)=0;

    virtual HRESULT __stdcall GetDisplayProcessOrderInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetDisplayProcessOrderLock(CAT_VARIANT_BOOL iLocked)=0;

    virtual HRESULT __stdcall get_PasteSameInstance(CATLONG & oDisplayOrder)=0;

    virtual HRESULT __stdcall put_PasteSameInstance(CATLONG iDisplayOrder)=0;

    virtual HRESULT __stdcall GetPasteSameInstanceInfo(CATBSTR & ioAdminLevel, CATBSTR & ioLocked, CAT_VARIANT_BOOL & oModified)=0;

    virtual HRESULT __stdcall SetPasteSameInstanceLock(CAT_VARIANT_BOOL iLocked)=0;


};

CATDeclareHandler(CATIATreeTabSettingAtt, CATIASettingController);

#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIABase.h"
#include "CATSafeArray.h"
#include "CATVariant.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
