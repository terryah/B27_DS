/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAConstraintActivities_h
#define CATIAConstraintActivities_h

#ifndef ExportedByProcessPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __ProcessPubIDL
#define ExportedByProcessPubIDL __declspec(dllexport)
#else
#define ExportedByProcessPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByProcessPubIDL
#endif
#endif

#include "CATIAActivities.h"

extern ExportedByProcessPubIDL IID IID_CATIAConstraintActivities;

class ExportedByProcessPubIDL CATIAConstraintActivities : public CATIAActivities
{
    CATDeclareInterface;

public:

};

CATDeclareHandler(CATIAConstraintActivities, CATIAActivities);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIACollection.h"
#include "CATVariant.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
