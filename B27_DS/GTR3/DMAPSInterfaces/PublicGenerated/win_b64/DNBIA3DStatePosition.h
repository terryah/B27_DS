/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef DNBIA3DStatePosition_h
#define DNBIA3DStatePosition_h

#ifndef ExportedByProcessPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __ProcessPubIDL
#define ExportedByProcessPubIDL __declspec(dllexport)
#else
#define ExportedByProcessPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByProcessPubIDL
#endif
#endif

#include "CATIABase.h"
#include "CATSafeArray.h"

class CATBaseDispatch;

extern ExportedByProcessPubIDL IID IID_DNBIA3DStatePosition;

class ExportedByProcessPubIDL DNBIA3DStatePosition : public CATIABase
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall SetPosition(const CATSafeArrayVariant & iTransformation)=0;

    virtual HRESULT __stdcall SynchronizePositionWithReference(CATBaseDispatch * iReferenceObject)=0;

    virtual HRESULT __stdcall SetReference(CATBaseDispatch * iReferenceObject)=0;

    virtual HRESULT __stdcall SetParent(CATBaseDispatch * iParentObject)=0;

    virtual HRESULT __stdcall GetPosition(CATSafeArrayVariant & oTransformation)=0;

    virtual HRESULT __stdcall GetReference(CATBaseDispatch *& oReferenceObject)=0;

    virtual HRESULT __stdcall GetParent(CATBaseDispatch *& oParentObject)=0;

    virtual HRESULT __stdcall SynchronizePosition()=0;


};

CATDeclareHandler(DNBIA3DStatePosition, CATIABase);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
