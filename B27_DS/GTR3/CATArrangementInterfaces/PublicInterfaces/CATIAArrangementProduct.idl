#ifndef CATIAArrangementProduct_IDL
#define CATIAArrangementProduct_IDL
// COPYRIGHT DASSAULT SYSTEMES 2000
/*IDLREP*/
/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */

interface CATIAArrangementAreas;
interface CATIAArrangementRectangles;
interface CATIAArrangementItemReservations;
interface CATIAArrangementRuns;
interface CATIAArrangementPathways;
interface CATIAArrangementBoundaries;

#include "CATIABase.idl"

/**  
 * Use this object as a factory for Arrangement collection objects.
 * <b>Role:</b>
 * Use this interface to get access to the Arrangement 
 * collections (Areas, Rectangles, ItemReservations, Runs, Pathways, Boundaries) aggregated a given Product.
 */
interface CATIAArrangementProduct : CATIABase 
{

  /**
   * Returns the collection of ArrangementAreas under the current ArrangementProduct.
   * <! @sample >
   * <dl>
   * <dt><b>Example:</b>
   * <dd>
   * This example retrieves the ArrangementAreas collection, <tt>oArrAreas </tt>, for the <tt>objArrProd1</tt>
   * ArrangementProduct object.
   * <pre>
   * Dim oArrAreas As ArrangementAreas
   * Set oArrAreas = objArrProd1.<font color="red">Areas</font>
   * </pre>
   * </dl>
   */

#pragma PROPERTY ArrangementAreas
  HRESULT get_ArrangementAreas(out /*IDLRETVAL*/ CATIAArrangementAreas oArrAreas);

  /**
   * Returns the collection of ArrangementRectangles under the current ArrangementProduct.
   * <! @sample >
   * <dl>
   * <dt><b>Example:</b>
   * <dd>
   * This example retrieves the ArrangementRectangles collection, <tt>oArrRectangles </tt>, for the <tt>objArrProd1</tt>
   * ArrangementProduct object.
   * <pre>
   * Dim oArrRectangles As ArrangementRectangles
   * Set oArrRectangles = objArrProd1.<font color="red">Rectangles</font>
   * </pre>
   * </dl>
   */

#pragma PROPERTY ArrangementRectangles
  HRESULT get_ArrangementRectangles(out /*IDLRETVAL*/ CATIAArrangementRectangles oArrRectangles);

  /**
   * Returns the collection of ArrangementItemReservations under the current ArrangementProduct.
   * <! @sample >
   * <dl>
   * <dt><b>Example:</b>
   * <dd>
   * This example retrieves the ArrangementItemReservations collection, <tt>oArrItemReservations </tt>, for the <tt>objArrProd1</tt>
   * ArrangementProduct object.
   * <pre>
   * Dim oArrItemReservations As ArrangementItemReservations
   * Set oArrItemReservations = objArrProd1.<font color="red">ItemReservations</font>
   * </pre>
   * </dl>
   */

#pragma PROPERTY ArrangementItemReservations
  HRESULT get_ArrangementItemReservations(out /*IDLRETVAL*/ CATIAArrangementItemReservations oArrItemReservations);

  /**
   * Returns the collection of ArrangementRuns under the current ArrangementProduct.
   * <! @sample >
   * <dl>
   * <dt><b>Example:</b>
   * <dd>
   * This example retrieves the ArrangementRuns collection, <tt>oArrRuns </tt>, for the <tt>objArrProd1</tt>
   * ArrangementProduct object.
   * <pre>
   * Dim oArrRuns As ArrangementRuns
   * Set oArrRuns = objArrProd1.<font color="red">Runs</font>
   * </pre>
   * </dl>
   */

#pragma PROPERTY ArrangementRuns
  HRESULT get_ArrangementRuns(out /*IDLRETVAL*/ CATIAArrangementRuns oArrRuns);

  /**
   * Returns the collection of ArrangementPathways under the current ArrangementProduct.
   * <! @sample >
   * <dl>
   * <dt><b>Example:</b>
   * <dd>
   * This example retrieves the ArrangementPathways collection, <tt>oArrPathways </tt>, for the <tt>objArrProd1</tt>
   * ArrangementProduct object.
   * <pre>
   * Dim oArrPathways As ArrangementPathways
   * Set oArrPathways = objArrProd1.<font color="red">Pathways</font>
   * </pre>
   * </dl>
   */

#pragma PROPERTY ArrangementPathways
  HRESULT get_ArrangementPathways(out /*IDLRETVAL*/ CATIAArrangementPathways oArrPathways);


  /**
   * Returns the collection of ArrangementBoundaries under the current ArrangementProduct.
   * <! @sample >
   * <dl>
   * <dt><b>Example:</b>
   * <dd>
   * This example retrieves the ArrangementBoundaries collection, <tt>oArrBoundaries </tt>, for the <tt>objArrProd1</tt>
   * ArrangementProduct object.
   * <pre>
   * Dim oArrBoundaries As ArrangementBoundaries
   * Set oArrBoundaries = objArrProd1.<font color="red">oArrObjType</font>
   * </pre>
   * </dl>
   */
#pragma PROPERTY ArrangementBoundaries
  HRESULT get_ArrangementBoundaries(out /*IDLRETVAL*/ CATIAArrangementBoundaries oArrBoundaries);

  /**
   * Returns the Type of the ArrangementProduct in the form of a String.
   * <! @sample >
   * <dl>
   * <dt><b>Example:</b>
   * <dd>
   * This example retrieves the type information as a string for the <tt>objArrProd1</tt>
   * ArrangementProduct object.
   * <pre>
   * Dim oArrObjType  As String
   * oArrObjType  = objArrProd1.<font color="red">Type</font>
   * </pre>
   * </dl>
   */
#pragma PROPERTY Type
  HRESULT get_Type(inout /*IDLRETVAL*/ CATBSTR oArrObjType);

  /**
   * Sets the nomenclature of the ArrangementProduct.
   * @return
   *   An HRESULT value.
   *   <br><b>Legal values</b>:
   *   <dl>
   *     <dt>S_OK</dt>
   *     <dd>operation is successful</dd>
   *     <dt>E_FAIL</dt>
   *     <dd>operation failed</dd>
   *   </dl>
   * <! @sample >
   * <dl>
   * <dt><b>Example:</b>
   * <dd>
   * This example sets the ArrangementNomenclature for <tt>objArrProd1</tt>
   * ArrangementProduct object.
   * <pre>
   * objArrProd1.<font color="red">SetArrangementNomenclature</font> = "Building"
   * </pre>
   * </dl>
   */

  HRESULT SetArrangementNomenclature(in CATBSTR iNomenclature);

  /**
   * Causes the name of the ArrangementProduct automatically.
   * @return
   *   An HRESULT value.
   *   <br><b>Legal values</b>:
   *   <dl>
   *     <dt>S_OK</dt>
   *     <dd>operation is successful</dd>
   *     <dt>E_FAIL</dt>
   *     <dd>operation failed</dd>
   *   </dl>
   * <! @sample >
   * <dl>
   * <dt><b>Example:</b>
   * <dd>
   * This example shows how the automatic naming of the <tt>objArrProd1</tt>
   * ArrangementProduct object can be done.
   * <pre>
   * objArrProd1.<font color="red">SetAutoName</font>
   * </pre>
   * </dl>
   */
  HRESULT SetAutoName();

  /**
   * Returns the product's applicative data which type is the given parameter.
   * @param iApplicationType
   *   The type of applicative data searched.
   * @param oApplicativeObj
   *   The matched applicative object. 
   * <! @sample >
   * <dl>
   * <dt><b>Example:</b>
   * <dd>
   * This example retrieves the desired applicative object from the <tt>objArrProd1</tt> object.
   * <pre>
   * Dim objProd   As Product
   * objProd  = objArrProd1.<font color="red">GetTechnologicalObject("Product")</font>
   * </pre>
   * </dl>
   */
  HRESULT GetTechnologicalObject (  in CATBSTR iApplicationType, 
                                    out /*IDLRETVAL*/ CATBaseDispatch oApplicativeObj);

};

// Interface name : CATIAArrangementProduct
#pragma ID CATIAArrangementProduct "DCE:40081963-b984-11d4-8def00d0b7acadba"
#pragma DUAL CATIAArrangementProduct

// VB object name : ArrangementProduct (Id used in Visual Basic)
#pragma ID ArrangementProduct "DCE:40081964-b984-11d4-8def00d0b7acadba"
#pragma ALIAS CATIAArrangementProduct ArrangementProduct

#endif
