/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAArrangementContour_h
#define CATIAArrangementContour_h

#ifndef ExportedByCATArrangementPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __CATArrangementPubIDL
#define ExportedByCATArrangementPubIDL __declspec(dllexport)
#else
#define ExportedByCATArrangementPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByCATArrangementPubIDL
#endif
#endif

#include "CATIABase.h"

extern ExportedByCATArrangementPubIDL IID IID_CATIAArrangementContour;

class ExportedByCATArrangementPubIDL CATIAArrangementContour : public CATIABase
{
    CATDeclareInterface;

public:

};

CATDeclareHandler(CATIAArrangementContour, CATIABase);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
