/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAArrangementItemReservation_h
#define CATIAArrangementItemReservation_h

#ifndef ExportedByCATArrangementPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __CATArrangementPubIDL
#define ExportedByCATArrangementPubIDL __declspec(dllexport)
#else
#define ExportedByCATArrangementPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByCATArrangementPubIDL
#endif
#endif

#include "CATArrangementItemResVisuMode.h"
#include "CATBSTR.h"
#include "CATIABase.h"
#include "CATSafeArray.h"

class CATBaseDispatch;

extern ExportedByCATArrangementPubIDL IID IID_CATIAArrangementItemReservation;

class ExportedByCATArrangementPubIDL CATIAArrangementItemReservation : public CATIABase
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_XLength(double & oXLength)=0;

    virtual HRESULT __stdcall put_XLength(double iXLength)=0;

    virtual HRESULT __stdcall get_YLength(double & oWidth)=0;

    virtual HRESULT __stdcall put_YLength(double iWidth)=0;

    virtual HRESULT __stdcall get_Height(double & oHeight)=0;

    virtual HRESULT __stdcall put_Height(double iHeight)=0;

    virtual HRESULT __stdcall get_VisuMode(CATArrangementItemResVisuMode & oVisuMode)=0;

    virtual HRESULT __stdcall put_VisuMode(CATArrangementItemResVisuMode iVisuMode)=0;

    virtual HRESULT __stdcall GetDimension(CATSafeArrayVariant & ioItemResDimensions)=0;

    virtual HRESULT __stdcall SetDimension(const CATSafeArrayVariant & iItemResDimensions)=0;

    virtual HRESULT __stdcall GetTechnologicalObject(const CATBSTR & iApplicationType, CATBaseDispatch *& oApplicativeObj)=0;


};

CATDeclareHandler(CATIAArrangementItemReservation, CATIABase);

#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
