# COPYRIGHT Dassault Systemes 2012
#======================================================================
# Imakefile for module MechanicalBuildInfra.m
#======================================================================
#
#  March 2012  Creation: Code generated by the CAA wizard  dtn
#======================================================================
#
# SHARED LIBRARY 
#
BUILT_OBJECT_TYPE=SHARED LIBRARY 
 
# DO NOT EDIT :: THE CAA2 WIZARDS WILL ADD CODE HERE
WIZARD_LINK_MODULES = \
    JS0GROUP                       \ # System                         JS0GROUP
#
# END WIZARD EDITION ZONE


INCLUDED_MODULES = MechanicalBuildInfraAdpt MechanicalBuildInfraServices  

#--- Module                        \ # Framework                      exportedby
LINK_WITH = \
    $(WIZARD_LINK_MODULES)         \
    MechanicalBuildInfraItfCPP     \ # MechanicalBuildInfraItf        MechanicalBuildInfraItf
    MechanicalBuildInfraDbg        \ # MechanicalBuildInfra           MechanicalBuildInfra
    CATGMModelInterfaces           \ # GMModelInterfaces              CATGMModelInterfaces
    CATGMOperatorsInterfaces       \ # GMOperatorsInterfaces          CATGMOperatorsInterfaces
    YP00IMPL                       \ # GeometricObjects               CATGeometricObjects
    CK0FEAT                        \ # LiteralFeatures                CATLiteralFeatures
    CATMathematics                 \ # Mathematics                    CATMathematics
    MecModItf                      \ # MecModInterfaces               CATMecModInterfaces
    MF0GEOM                        \ # MechanicalModeler              CATMechanicalModeler
    ObjectModeler                  \ # ObjectModelerBase              CATObjectModelerBase
    AC0SPBAS                       \ # ObjectSpecsModeler             CATObjectSpecsModeler
    AS0STARTUP                     \ # ProductStructure               AS0STARTUP
    NS0SI18N                       \ # System                         JS0GROUP
    JS03TRA                        \ # System                         JS03TRA
    JS0FM                          \ # System                         JS0FM     
    GN0GRAPH                       \ # GenericNaming                  GN0GRAPH 
    CATMecModLiveUseItf            \ # CATMecModLiveUseItf            CATMecModLiveUseItf
    CATVisualization               \ # Visualization                  CATVisualization
    CATViz                         \ # VisualizationBase              CATViz
    CATInteractiveInterfaces       \ # InteractiveInterfaces          CATInteractiveInterfaces
	BODYNOPE                       \ # TopologicalOperators           BODYNOPE
    XMLParserItf                   \ # XMLparser                      XMLParserItf    
#
# System dependant variables
#
OS = AIX
#
OS = HP-UX
#
OS = IRIX
#
OS = SunOS
#
OS = Windows_NT
