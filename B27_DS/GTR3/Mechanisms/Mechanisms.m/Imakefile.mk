#
# LOAD MODULE
#
BUILT_OBJECT_TYPE = NONE
#
INCLUDED_MODULES = K10MECA K10SIMUL 
#

LINK_WITH = AC0XXLNK AD0XXBAS \
            AS0STARTUP \
            CD0WIN CD0FRAME \
            CO0LSTPV CO0LSTST CO0RCDBL \
            DI0PANV2 \
            DI0STATE \
            FE0XY\
            FunctionEditorItfCPP \
            JS0CORBA JS03TRA JS0ERROR JS0STR JS0SCBAK \
            JS0FM \
            KinematicsItf \
            NS0S3STR NS0S1MSG \
            CATSimulationBase \
            DNBSimulationBaseLegacy \
            SimulationItf \
            CK0FEAT CATKnowledgeModeler \
            VE0BASE VE0MDL \
            YP00IMPL YN000MAT CATVisualization

#            SpaceAnalysisItf \
#            
OS = COMMON
#
OS = Windows_NT
#
