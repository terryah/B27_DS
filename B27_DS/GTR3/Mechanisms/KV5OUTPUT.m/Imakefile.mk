#
# Loaded Module For Numeric OUTPUT
#
BUILT_OBJECT_TYPE = SHARED LIBRARY

#ifdef LOCAL_DEFINITION_FOR_IID
LINK_WITH_FOR_IID = \
CATAfrUUID \
CATIAApplicationFrameUUID \
CATVisUUID \
FunctionEditorInterfacesUUID \
InfInterfacesUUID \
KinematicsInterfacesUUID \
KnowledgeInterfacesUUID \
LiteralFeaturesUUID \
MechanicalModelerUUID \
MechanismsUUID \
NavigatorInterfacesUUID \
ObjectModelerBaseUUID \
ObjectSpecsModelerUUID \
ProductStructureInterfacesUUID \
ProductStructureUUID \
SimulationInterfacesUUID
#else
LINK_WITH_FOR_IID =
#endif

LINK_WITH =$(LINK_WITH_FOR_IID) JS0GROUP JS0FM DI0PANV2\ 
      CATLiteralFeatures CATObjectModelerBase\
      CATKiiKinematicsItf CATMechanisms K10TOOLB\
      SIMItf SimulationItf FunctionEditorItfCPP CATSIMProbeCommand\
      KnowledgeItf CATSaiSpaceAnalysisItf CATApplicationFrame\
      CATProductStructure1 CATGeometricObjects CATMathematics\
      CATDialogEngine CATObjectSpecsModeler CATVisualization\
      CATAxisBody CATProductStructure2\
      CATAssCouMdl CATSimulationBase DNBSimulationBaseLegacy\
      CATGitInterfaces MecModItf SMTInterfacesItfCPP CATProductStructureInterfaces \
      CATFileMenu CATMechanicalModelerUI CATInteractiveInterfaces CATAssSelection \
      CATNavigatorItf CATMechanicalModeler YI00IMPL CATAssCommands
