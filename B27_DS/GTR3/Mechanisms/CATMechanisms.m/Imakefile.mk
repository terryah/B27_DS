#ifdef LOCAL_DEFINITION_FOR_IID
LINK_WITH_FOR_IID = \
CATAfrUUID \
CATIAApplicationFrameUUID \
CATVisUUID \
FunctionEditorInterfacesUUID \
InfInterfacesUUID \
KinematicsInterfacesUUID \
LiteralFeaturesUUID \
MechanicalModelerUUID \
MechanismsUUID \
NavigatorInterfacesUUID \
ObjectModelerBaseUUID \
ObjectSpecsModelerUUID \
ProductStructureInterfacesUUID \
ProductStructureUUID \
SimulationInterfacesUUID \ 
CATTTRSUUID
#else
LINK_WITH_FOR_IID = \
MechanismsUUID
#endif
#
# LOAD MODULE
#
BUILT_OBJECT_TYPE = SHARED LIBRARY
#
INCLUDED_MODULES = Mechanisms  K10MECA K10SIMUL KV5CMD KV5VIEW KV5MODEL KV5CST KCAA2 KV5PDM KV5CONVERT

LINK_WITH=$(LINK_WITH_FOR_IID)  CATObjectModelerBase CATObjectModelerCATIA CATObjectSpecsModeler \
            JS0GROUP JS0FM DI0PANV2 CATSysFile CATV4System CATDialogEngine \
            CATInfInterfaces   \
            CATVisualization CATMathematics CATViz \
            CATApplicationFrame CATIAApplicationFrame \
            CATGeometricObjects CATGeometricOperators CATNewTopologicalObjects \
            CATSketcherInterfaces CATGitInterfaces \
            FE0XY  FunctionEditorItfCPP \
            K10TOOLB CATKiiKinematicsItf \
            CATSiqSimulationV4F CATSiqSimulationV4C \
            CATSimulationBase CATSimulationCommand \
            DNBSimulationBaseLegacy DNBSimulationCommandLegacy \
			CATDynClash \
            CK0FEAT CATKnowledgeModeler CATLiteralsEditor \
            MecModItf CATMechanicalModeler CATMechanicalModelerUI \
            CATAssSelection CATAssCouMdl \
            CATProductStructureInterfaces  CATNavigatorItf  \
            CATProductStructure1 PRDWorkshop    \
            CATAssCommands KV5OUTPUT CATSIMProbeCommand   \
            CATSaiSpaceAnalysisItf CATInteractiveInterfaces \
            AC0ITEMS VE0GRPH2 SMTInterfacesItfCPP \
            SIMItf SimulationItf CATSIMSequenceModel \
            CATTTRSItf CATTTRSModel  CATDMUBase CATMmuSettings \
            CATAssemblyInterfaces CATIAEntity YN000FUN CATAutoItf CATV4iSettings



# migration TTRS : TTRSModeler enleve, remplace par la ligne ci-dessus.
