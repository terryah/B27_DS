#ifdef LOCAL_DEFINITION_FOR_IID
LINK_WITH_FOR_IID = \
ObjectSpecsModelerUUID
#else
LINK_WITH_FOR_IID =
#endif

BUILT_OBJECT_TYPE=SHARED LIBRARY
INCLUDED_MODULES =

LINK_WITH=$(LINK_WITH_FOR_IID) \
        CATClnBase \
        CATClnSpecs \
        CATMathematics\
        CATMechanisms \
		K10TOOLB \
        CATSimulationBase \
		CATSimulationInterfaces \
		CATSiqSimulationV4C \
        DNBSimulationBaseLegacy \
        CATObjectModelerBase \
        CATObjectSpecsModeler \
        JS0GROUP \
        CATGeometricObjects \
        CATKiiKinematicsItf \
        CATProductStructure1 \
        CATMechanicalModeler \
        CATAssCouMdl


