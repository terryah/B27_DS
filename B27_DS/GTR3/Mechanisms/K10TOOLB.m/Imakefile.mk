#ifdef LOCAL_DEFINITION_FOR_IID
LINK_WITH_FOR_IID = \
CATAfrUUID \
CATAssemblyInterfacesUUID \
KinematicsInterfacesUUID \
MechanismsUUID \
ObjectModelerBaseUUID \
ObjectSpecsModelerUUID \
ProductStructureUIUUID \
ProductStructureUUID
#else
LINK_WITH_FOR_IID = \
MechanismsUUID
#endif
#
# DEFINITION DU WORKBENCH Kinematics
#
BUILT_OBJECT_TYPE = SHARED LIBRARY

COMMON_LINK_WITH = CATObjectSpecsModeler \
			CATObjectModelerBase \
			CATApplicationFrame \
			JS0GROUP \
			CATVisualization CATViz \
			CATIAApplicationFrame \
			CATPrsWksPRDWorkshop \
			CATKiiKinematicsItf \
	        JS0FM DI0PANV2 \
			CATProductStructure1 \
			CK0FEAT \
			CATAssemblyInterfaces MF0CST CATAssWkbAssembly \
      SimulationItfCPP CATMathematics CATDMUWorkBench \
      CATNavigatorItf CATNavigator2Itf \
      CATProductStructureInterfaces    \
      CATCamController                     # Camera ajout STX

LINK_WITH=$(LINK_WITH_FOR_IID)  $(COMMON_LINK_WITH) 
