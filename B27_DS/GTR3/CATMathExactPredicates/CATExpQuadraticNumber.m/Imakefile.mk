#
# Copyright Dassault Systemes Provence 2013-2014, all rights reserved
#
#==============================================================================
# Imakefile for module CATExpQuadraticNumber.m
#==============================================================================
#
# Historique :
#  15/05/2014 : MMO : correction des cycles sur Android a partir de R25 et R417
#  02/05/2013 : T6L : Creation
#
#==============================================================================
#

BUILT_OBJECT_TYPE=SHARED LIBRARY 

#if defined CATIAV5R25 || defined CATIAR417

INCLUDED_MODULES = CATExpNumbersComparisons

LINK_WITH = CATExpIntervalArithmetic CATExpExactArithmetic

#else

INCLUDED_MODULES =

LINK_WITH = CATExpIntervalArithmetic CATExpExactArithmetic CATExpNumbersComparisons

#endif

#==============================================================================

OS = AIX
SYS_LIBS = -lxlf -lxlf90 -lxlfpad

OS = IRIX
SYS_LIBS = -lftn

OS = Windows_NT
#if os win_b64
LOCAL_CCFLAGS=-DWNT /EHsc
#else
OPTIMIZATION_CPP = /O2
LOCAL_CCFLAGS=-DWNT /EHsc
#endif

OS = HP-UX
#if os hpux_a
SYS_LIBS = -lf
#else
SYS_LIBS= -lF90
#endif                

OS = hpux_b64 
#if os hpux_a
SYS_LIBS = -lf
#else
SYS_LIBS= -lF90 -lcps 
#endif

OS = SunOS
SYS_LIBS = -lF77 -lM77
