#
# Copyright Dassault Systemes Provence 2013, all rights reserved
#
#==============================================================================
# Imakefile for module CATExpExactArithmetic.m
#==============================================================================
#
# Historique :
#  02/05/2013 : T6L : Creation
#
#==============================================================================
#

BUILT_OBJECT_TYPE=SHARED LIBRARY 

INCLUDED_MODULES =

LINK_WITH = CATExpPoolAllocator

#==============================================================================

OS = AIX
SYS_LIBS = -lxlf -lxlf90 -lxlfpad

OS = IRIX
SYS_LIBS = -lftn

OS = Windows_NT
#if os win_b64
LOCAL_CCFLAGS=-DWNT /EHsc
#else
OPTIMIZATION_CPP = /O2
LOCAL_CCFLAGS=-DWNT /EHsc
#endif

OS = HP-UX
#if os hpux_a
SYS_LIBS = -lf
#else
SYS_LIBS= -lF90
#endif                

OS = hpux_b64 
#if os hpux_a
SYS_LIBS = -lf
#else
SYS_LIBS= -lF90 -lcps 
#endif

OS = SunOS
SYS_LIBS = -lF77 -lM77
