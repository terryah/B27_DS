/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAScene_h
#define CATIAScene_h

#include "CATCORBABoolean.h"
#include "CAT_VARIANT_BOOL.h"

#ifndef ExportedByOSMInterfacesPubIDL
#ifdef _WINDOWS_SOURCE
#ifdef __OSMInterfacesPubIDL
#define ExportedByOSMInterfacesPubIDL __declspec(dllexport)
#else
#define ExportedByOSMInterfacesPubIDL __declspec(dllimport)
#endif
#else
#define ExportedByOSMInterfacesPubIDL
#endif
#endif

#include "CATIABase.h"
#include "CatSceneType.h"

class CATIAProduct;
class CATIASceneProduct;

extern ExportedByOSMInterfacesPubIDL IID IID_CATIAScene;

class ExportedByOSMInterfacesPubIDL CATIAScene : public CATIABase
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_Type(CatSceneType & oType)=0;

    virtual HRESULT __stdcall ExistsInScene(CATIAProduct * iProduct, CAT_VARIANT_BOOL & oExistsInScene)=0;

    virtual HRESULT __stdcall GetSceneProductData(CATIAProduct * iProduct, CATIASceneProduct *& oSceneProductData)=0;

    virtual HRESULT __stdcall Copy(CatSceneType iType, CATIAScene *& oProductScene)=0;


};

CATDeclareHandler(CATIAScene, CATIABase);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIAAnalyze.h"
#include "CATIACollection.h"
#include "CATIAMove.h"
#include "CATIAParameters.h"
#include "CATIAPosition.h"
#include "CATIAProduct.h"
#include "CATIAPublications.h"
#include "CATIASceneProduct.h"
#include "CATSafeArray.h"
#include "CATVariant.h"
#include "CatFileType.h"
#include "CatProductSource.h"
#include "CatRepType.h"
#include "CatWorkModeType.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
