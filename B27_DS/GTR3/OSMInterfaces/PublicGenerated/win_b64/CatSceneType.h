/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CatSceneType_h
#define CatSceneType_h

enum CatSceneType {
        CatSceneTypeDelta,
        CatSceneTypeFull
};

#endif
