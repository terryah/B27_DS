#ifdef  _WINDOWS_SOURCE
#ifdef  __CCWItfCPP
#define ExportedByCCWItfCPP     __declspec(dllexport)
#else
#define ExportedByCCWItfCPP     __declspec(dllimport)
#endif
#else
#define ExportedByCCWItfCPP
#endif
