// COPYRIGHT Dassault Systemes 2005
//===================================================================
//
// CATIxPDMSendToService.h
// Define the CATIxPDMSendToService interface
//
//===================================================================
//
// Usage notes:
//   The usage of this interface requires the installation of CATIA - PPR xPDM Gateway 1 Product (PX1)
//   or the installation of the CATIA-SmarTeam plugin.
//   In case one of these products is not granted any method of the interface should fail.
//   This interface can be accessed using the @href #CATxPDMFileServices#CreateSendToService .
//
//===================================================================
//  Apr 2005  Creation: Code generated by the CAA wizard  FAR
//===================================================================
#ifndef CATIxPDMSendToService_H
#define CATIxPDMSendToService_H

/**
 * @CAA2Level L1
 * @CAA2Usage U3
 */

#include "CATBaseUnknown.h"
#include "CATIxPDMItem.h"
#include "CATListOfCATIxPDMItem.h" 
#include "ExportedByCATxPDMInterfaces.h"
#include "CATUnicodeString.h"
#include "CATListOfInt.h"

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByCATxPDMInterfaces IID IID_CATIxPDMSendToService;
#else
extern "C" const IID IID_CATIxPDMSendToService ;
#endif

//------------------------------------------------------------------

/**
 * Interface to manage the Send To functionality.
 * <b>Role</b>:
 * This interface must be used to copy any CATIA V5 file document to a directory as soon as the document is linked to others.
 * The Send to functionality automatically reroutes the internal link in order to get in the target directory
 * a consistency set of linked documents. The target directory is those given as argument of the 
 * @href CATxPDMFileServices#CreateSendToService method.
 * 
 * <p>Functionalities are offered to rename, add or remove files to be send to a target directory.
 * Using the @href #Simulate method, you can predict the result of the @href #Run method which executes the resulting Send to operation.</p>
 * <p>The usage of this interface requires the installation of CATIA - PPR xPDM Gateway 1 Product (PX1)
 * or the installation of the CATIA-SmarTeam plugin.
 * In case one of these products is not granted any method of the interface fails.</p>
 * It is required to not modify and save the documents in session between 2 consecutive calls to the @href #Add method and the
 * @href #Simulate or @href CATIxPDMSendToService#Run methods. This could leads to inconsitent result because of links modifications.
 * <p>An interface pointer is accessible using the @href CATxPDMFileServices#CreateSendToService method.
 * </p>
 * @see CATIxPDMItem
*/
class ExportedByCATxPDMInterfaces CATIxPDMSendToService: public CATBaseUnknown
{
  CATDeclareInterface;

  public:

    /**
     * Adds a file to the list of the files to be copied. 
     * <br><b>Role:</b></br> Verifies that the given input file is valid (exists on disk and is not a directory),
     * and recursively adds the pointed documents of the added file.   
     *   @param iInputItem
     *     The item to add. It can be retrieved from @href CATxPDMFileServices#GetItemFromFile service.
     *   @return
     *  <ul>
     *  <li><tt>S_OK</tt> : item is well added to the list of to be copied files. </li>
     *  <li><tt>E_FAIL</tt>: The method has failed. The possible <b>returned errors </b> (listed by their identifier) are:</li>
     *  <ul>
     *      <li><tt>InputERR_1001:</tt></li>
     *       The PX1 license is not available.
     *      <li><tt>InputERR_2004:</tt></li>
     *       Item already in List.
     *      <li><tt>InputERR_2005:</tt></li>
     *       Invalid input item.
     *      <li><tt>InputERR_2006:</tt></li>
     *       The input item has no read permission.
     *  </ul>
     *  </ul>
     */
  virtual HRESULT Add(CATIxPDMItem_var & iInputItem) = 0;
 
   /**
    *nodoc
    */
  virtual HRESULT AddFromDirectory(CATIxPDMItem_var & iInputItem,const CATListOfCATUnicodeString iListOfDirectory,const CATBoolean iScanRecursively) = 0;
  
  /**
     * Removes a file from the list of files to be copied.   
     *   @param iInputItem
     *     The item to remove from the list of file to be copied.
     *     It can be retrieved from @href CATxPDMFileServices#GetItemFromFile service.
     *   @return
     *  <ul>
     *  <li><tt>S_OK</tt> : if item is well removed from the list of to be copied files. </li>
     *  <li><tt>E_FAIL</tt>: The method has failed. The possible <b>returned errors </b> (listed by their identifier) are:</li>
     *  <ul>
     *      <li><tt>InputERR_1001:</tt></li>
     *       The PX1 license is not available.
     *      <li><tt>InputERR_2005:</tt></li>
     *       Invalid input item.
     *      <li><tt>InputERR_2007:</tt></li>
     *       Item not in the list. 
     *      <li><tt>InputERR_2014:</tt></li>
     *       Item already removed.
     *  </ul>
     *  </ul>
     */
  virtual HRESULT Remove(CATIxPDMItem_var & iInputItem) = 0;
 
    /**
     * Renames one file to be copied. The new name may not have invalid characters.   
     *   @param iInputItem
     *     The item to rename.
     *   @param iNewName
     *     New file identifier of the item (file name without extension).
     *   @return
     *  <ul>
     *  <li><tt>S_OK</tt> : item is well renamed. </li>
     *  <li><tt>E_FAIL</tt>: The method has failed. The possible <b>returned errors </b> (listed by their identifier) are:</li>
     *  <ul>
     *      <li><tt>InputERR_1001:</tt></li>
     *       The PX1 license is not available.
     *      <li><tt>InputERR_2005:</tt></li>
     *       The Invalid input item.
     *      <li><tt>InputERR_2007:</tt></li>
     *       Item not in the list. 
     *      <li><tt>InputERR_2008:</tt></li>
     *       Invalid file identifier.  
     *      <li><tt>InputERR_2011:</tt></li>
     *       Input new name /P1 already used in the list.
     *      <li><tt>InputERR_2013:</tt></li>
     *       Item to rename has been removed from list.
     *  </ul>
     *  </ul>
     */
  virtual HRESULT Rename(CATIxPDMItem_var & iInputItem,CATUnicodeString& iNewName) = 0;
 
    /**
     * Executes the Send To action.   
     *   @param oListOutputItems
     *     List of all the final items in the targetted directory.
     *     The targetted directory is the iTargetDirectory input parameter of the @href CATxPDMFileServices#CreateSendToService service.
     *   @return
     *  <ul>
     *  <li><tt>S_OK</tt> : Send to successfully done. </li>
     *  <li><tt>E_FAIL</tt>: The method has failed. The possible <b>returned errors </b> (listed by their identifier) are:</li>
     *  <ul>
     *      <li><tt>InputERR_1119:</tt></li>
     *       An item is loaded and modified in session, it should be saved before. (The NLS message includes the name of this wrong document).
     *      <li><tt>InputERR_1001:</tt></li>
     *       The PX1 license is not available.
     *      <li><tt>InputERR_2009:</tt></li>
     *       No data to Send To. At least 1 item has to be added.
     *      <li><tt>InputERR_2010:</tt></li>
     *       Internal error.
     *      <li><tt>InputERR_2012:</tt></li>
     *       2 different documents to copy have the same final identification. 
     *       (The NLS message includes the name of this wrong document)
     *      <li><tt>InputERR_2015:</tt></li>
     *       No data to Send To. All items have been removed.
	 *      <li><tt>InputERR_2017:</tt></li>
     *       Some of the items being Send To, pointing to PDM document. SendTo cannot be performed.
     *  </ul>
     *  </ul>
     */
  virtual HRESULT Run(CATListValCATIxPDMItem_var  &oListOutputItems) = 0;
 
    /**
     * Simulates the Send To operation.
     * <br><b>Role:</b></br> gives for each item included in the initial set of
     * documents its status and its final localization. 
     * In case of a not null status, the corresponding output Items are set to NULL_var.
     * 
     *   @param oStatusListOK
     *     For each item to be copied, targeted status.
     *     The status can be:
     *     <ul>
     *      <li><tt> 0</tt>: The Item will be theorically successfully sent to. </li>
     *      <li><tt>-1</tt>: The Item will not be copied because it has been removed from the list of files to Send To.</li>
     *      <li><tt> 1</tt>: The Item will not be copied because the item is missing. This is a pointed document not retrieved on disk.</li>
     *      <li><tt> 2</tt>: The Item will not be copied because the final item name is already used.</li>
     *      <li><tt> 3</tt>: The Item will not be copied because it is modified in session.</li>
     *     </ul>  
     *   @param oListInputItems
     *     List of all the input items
     *   @param oListOutputItems
     *     List of all the final items. 
     *     Note that because this is only a simulation, the real final files are not generated. Dummy objects are computed.
     *     They implement only the @href CATIxPDMItem#GetDocFileName method of the @href CATIxPDMItem interface.
     *   @return
     *  <ul>
     *  <li><tt>S_OK</tt> : Send To simulation successfully done. </li>
     *  <li><tt>E_FAIL</tt>: The method has failed. The possible <b>returned errors </b> (listed by their identifier) are:</li>
     *  <ul>
     *      <li><tt>InputERR_1001:</tt></li>
     *       The PX1 license is not available.
     *      <li><tt>InputERR_1119:</tt></li>
     *       An item is loaded and modified in session, it should be saved before. (The NLS message includes the name of this wrong document).
     *      <li><tt>InputERR_2009:</tt></li>
     *       No data to Send To. At least 1 item has to be added.
     *      <li><tt>InputERR_2010:</tt></li>
     *       Internal error.
     *      <li><tt>InputERR_2012:</tt></li>
     *       2 different documents to copy have the same final identification. 
     *       (The NLS message includes the name of this wrong document)
     *      <li><tt>InputERR_2015:</tt></li>
     *       No data to Send To. All items have been removed.
	 *      <li><tt>InputERR_2017:</tt></li>
     *       Some of the items being Send To, pointing to PDM document. SendTo cannot be performed.
     *  </ul>
     *  </ul>  
     */
  virtual HRESULT Simulate(CATListOfInt &oStatusListOK, 
                           CATListValCATIxPDMItem_var  &oListInputItems, 
                           CATListValCATIxPDMItem_var  &oListOutputItems) = 0;
 

  // No constructors or destructors on this pure virtual base class
  // --------------------------------------------------------------
};

CATDeclareHandler(CATIxPDMSendToService, CATBaseUnknown);


//------------------------------------------------------------------

#endif
