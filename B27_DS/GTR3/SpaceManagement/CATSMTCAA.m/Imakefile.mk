#ifdef LOCAL_DEFINITION_FOR_IID
LINK_WITH_FOR_IID = \
NavigatorInterfacesUUID \
SpaceManagementUUID \
ProductStructureUUID \
SMTInterfacesUUID
#else
LINK_WITH_FOR_IID =
#endif
#
# SHARED LIBRARY
#
BUILT_OBJECT_TYPE=SHARED LIBRARY

LINK_WITH=$(LINK_WITH_FOR_IID) JS0SCBAK JS0STR JS0ERROR\
            AD0XXBAS  AS0STARTUP \
		    CATMathematics CATVisualization \
            CATProductStructureInterfaces CATInfInterfaces \
			CATNavigatorItf CATNavigator2Itf \
			O203DMAP O203DMAPOperators O20MERGER \
			LODDMU SB03DMAP SB0COMF CATSpaceManagement \
			SMTInterfacesItfCPP CATDMOTOOLB \
			SimulationItf \
			CATAfrInterfaces \
			DNBSimulationBaseLegacy CATFittingInterfaces \
            CATIntInterfaces \

OS = AIX
SYS_LIBS = -lXt -lX11

OS = HP-UX
SYS_LIBS = -lXt -lX11

OS = IRIX
SYS_LIBS = -lXt -lX11 

OS = SunOS
SYS_LIBS = -lXt -lX11

