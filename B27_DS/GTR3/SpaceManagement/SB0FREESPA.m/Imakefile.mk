#ifdef LOCAL_DEFINITION_FOR_IID
LINK_WITH_FOR_IID = \
CATAfrUUID \
SMTInterfacesUUID
#else
LINK_WITH_FOR_IID =
#endif
#
# SHARED LIBRARY
#
BUILT_OBJECT_TYPE=SHARED LIBRARY

LINK_WITH=$(LINK_WITH_FOR_IID)  JS0FM JS0GROUP \
		AD0XXBAS CD0FRAME \
		AS0STARTUP \
		SpecsModeler \
		SimulationItf \
		DI0PANV2 \
		CATSMTInterfaces \
		CATProductStructureInterfaces  \
		VE0BASE  SpaceManagement CO0LSTPV DI0STATE NS0S3STR CATSimulationBase SB03DMAP CATDMOTOOLB \
		YN000MFL O203DMAP O203DMAPOperators CATInfInterfaces CK0FEAT CK0UNIT \
		LODDMU \
		SpaceAnalysisItf \
		CATVoxelServices Vps \
        CATIntInterfaces \
#ifdef CATIAV5R9
		SIMItf 
#endif





