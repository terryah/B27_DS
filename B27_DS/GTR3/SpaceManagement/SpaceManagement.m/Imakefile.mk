#ifdef LOCAL_DEFINITION_FOR_IID
LINK_WITH_FOR_IID = \
CATAfrUUID \
CATSpaceMapUUID \
InterferenceUUID \
ObjectModelerBaseUUID \
ProductStructureUUID \
SMTInterfacesUUID \
SpaceManagementUUID
#else
LINK_WITH_FOR_IID =
#endif
#
# SHARED LIBRARY
#
BUILT_OBJECT_TYPE=SHARED LIBRARY

INCLUDED_MODULES = SB03DMAP CD03DMAP CATSpaceManagement CATSMTDic

LINK_WITH=$(LINK_WITH_FOR_IID)       \
       AC0XXLNK \
       AD0XXBAS \
       AS0STARTUP \
	   CD0WIN \
       CO0LSTPV \
       CO0RCINT \
	   DI0PANV2 \
       JS03TRA \
       JS0CORBA \
	   JS0FM \
       JS0LIB0 \
       JS0SCBAK \
       JS0STR \
       NS0S3STR \
       SpecsModeler \
	   VE0MDL \
	   VE0BASE \
	   CATCdbEntity VI0REPV4\
	   O203DMAP O203DMAPOperators O20MERGER \
	   CATSimulationBase \
	   CATSMTInterfaces \
	   CATSimulationInterfaces \
	   SimulationItf \
	   YN000MFL \
	   SB0COMF CATSimulationBase \
	   CK0FEAT\
       CK0UNIT \
       CATDMOTOOLB \
	   CATProductStructureInterfaces CATInfInterfaces \
	   CATDialogEngine CD0SHOW \
	   CATDMUModel CATNavigatorItf CATNavigator2Itf CATAfrInterfaces HLRV4Interface HLRUti \
	   CATFittingInterfaces AC0ITEMS CATPrsRep CATFeatObjects CD0AUTOSTART CATPrint\
	   LODDMU SB0FREESPA\
         CATMecModInterfaces \
         CATDMUManip CATDMUBase \
         SpaceAnalysisItf CATMeasureGeometryInterfaces \
         JS0FILE \
         CATIntInterfaces \
         SU0STL1 \
         CATDMUWind \
         CATV4iServices \
         OSMInterfacesItf \

# JXO 13/05/2004 : enleve pour cause de data update errors :
#	   CATMmrAxisSystem \

#CATIAApplicationFrame CD0CGR 	   PG0BASE 
     
#
LOCAL_CCFLAGS = 

