#include "CATIAV5Level.h"
#if defined(CATIAV5R28) || defined (CATIAR420)
#ifdef  __CATCDS

// COPYRIGHT DASSAULT SYSTEMES 2012 
/** @CAA2Required */
/*---------------------------------------------------------------------*/
/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */
/*---------------------------------------------------------------------*/
#define ExportedByCDSInterface DSYExport
#else
#define ExportedByCDSInterface DSYImport
#endif
#include "DSYExport.h"

#ifdef  __CATCDS
#define ExportedBySolverInterface DSYExport
#else
#define ExportedBySolverInterface DSYImport
#endif
#include "DSYExport.h"
#else
#ifdef  _WINDOWS_SOURCE
#ifdef  __CATCDS
// COPYRIGHT DASSAULT SYSTEMES 2012 
/** @CAA2Required */
/*---------------------------------------------------------------------*/
/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */
/*---------------------------------------------------------------------*/
#define ExportedByCDSInterface     __declspec(dllexport)
#else
#define ExportedByCDSInterface     __declspec(dllimport)
#endif
#else
#define ExportedByCDSInterface
#endif

#ifdef  _WINDOWS_SOURCE
#ifdef  __CATCDS
#define ExportedBySolverInterface     __declspec(dllexport)
#else
#define ExportedBySolverInterface     __declspec(dllimport)
#endif
#else
#define ExportedBySolverInterface
#endif
#endif


/**
 * @nodoc
 */
#define CATICDSDeclareInterface(Name) \
public: \
  virtual ~Name() {}

/**
 * @nodoc
 */
#define CATICDSProtectClassCopy(Name) \
  private: \
    Name(const Name&); \
    void operator=(const Name&);
