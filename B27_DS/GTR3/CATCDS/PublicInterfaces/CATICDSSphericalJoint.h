#ifndef CATICDSSphericalJoint_H
#define CATICDSSphericalJoint_H

// COPYRIGHT DASSAULT SYSTEMES  2012

/**
 * @CAA2Level L0
 * @CAA2Usage U1
 */

#include "CATCDS.h"

#include "CATICDSJoint.h"

class CATICDSFactory;
class CATICDSVariable;

/**
 * Spherical joint is a type of joint which connects two axes thus three
 * degrees of freedom between them are available. The degrees are rotations in
 * three-dimensional space commanded by angle variables. Namely, Tait-Bryan
 * angles with z-x-y convention are used. The origines of axes are coincident.
 */
class ExportedByCDSInterface CATICDSSphericalJoint : public CATICDSJoint
{
  CATICDSDeclareInterface(CATICDSSphericalJoint)

public:

  /**
   * Retrieves axis connected with the joint.
   * @param opGeom1
   *   The first geometry.
   * @param opGeom2
   *   The second geometry.
   *
   * @RETURNARRAY
   */
  virtual void GetAxis(CATICDSAxis *&opAxis1, CATICDSAxis *&opAxis2) const = 0;

  /**
   * Retrieves the angle variables.
   * @param opAngle1
   *   The first angle variable.
   * @param opAngle2
   *   The second angle variable.
   * @param opAngle3
   *   The third angle variable.
   *
   * @RETURNARRAY
   */
  virtual void GetAngles(CATICDSVariable *&opAngle1,
                         CATICDSVariable *&opAngle2,
                         CATICDSVariable *&opAngle3) const = 0;

  /**
   * Creates an instance of the CATICDSSphericalJoint joint.
   * @param ipFactory
   *   Pointer to the instance of CATICDSFactory to use.
   * @param ipAxis1
   *   Pointer to the first CATICDSAxis that the joint is being placed on.
   * @param ipAxis2
   *   Pointer to the second CATICDSAxis that the joint is being placed on.
   * @param ipAngle1
   *   The first angle variable.
   * @param ipAngle2
   *   The second angle variable.
   * @param ipAngle3
   *   The third angle variable.
   */
  static CATICDSSphericalJoint* Create(CATICDSFactory *ipFactory,
                                       CATICDSAxis* ipAxis1,
                                       CATICDSAxis* ipAxis2,
                                       CATICDSVariable* ipAngle1,
                                       CATICDSVariable* ipAngle2,
                                       CATICDSVariable* ipAngle3);
};

#endif
