// COPYRIGHT Dassault Systemes 2004
//===================================================================
//
// CATIMfgMachinableAreaUserFeature.h
// Define the CATIMfgMachinableAreaUserFeature interface
//
//===================================================================
//
// Usage notes:
//   Allow access to User features included 
//   inside a Machinable Area Feature
//
//===================================================================
//
//  Jun 2004  Creation: Code generated by the CAA wizard  cvr
//===================================================================
#ifndef CATIMfgMachinableAreaUserFeature_H
#define CATIMfgMachinableAreaUserFeature_H

/**
* @CAA2Level L1
* @CAA2Usage U3
*/

#include "MfgItfEnv.h"
#include "CATBaseUnknown.h"

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByMfgItfEnv IID IID_CATIMfgMachinableAreaUserFeature;
#else
extern "C" const IID IID_CATIMfgMachinableAreaUserFeature ;
#endif

//------------------------------------------------------------------

/**
 * Interface dedicated to Machinable Area Feature / User Feature management.
 * <p>
 * <br><b>Role</b>: Retreives User Feature information for a Machinable Area Feature.
 */
class ExportedByMfgItfEnv CATIMfgMachinableAreaUserFeature: public CATBaseUnknown
{
  CATDeclareInterface;

  public:

    /**
     * Retrieves the handler of the User Feature or Design Feature
     * pointed by the Machinable Area.
     *   @param ohFeature
     *     The handler of the Feature
     */

     virtual HRESULT GetMAFFeature(CATBaseUnknown_var & ohFeature) = 0;

  // No constructors or destructors on this pure virtual base class
  // --------------------------------------------------------------
};

//------------------------------------------------------------------

CATDeclareHandler(CATIMfgMachinableAreaUserFeature,CATBaseUnknown);

#endif
