/* -*-c++-*- */
//===================================================================
// COPYRIGHT Dassault Systemes 2001
//===================================================================
//
// CATIMfgNavigatePatternView.h
// Define the CATIMfgNavigatePatternView interface
//
//===================================================================
//  Jan 2001  Creation: Code generated by the CAA wizard  SMU
//===================================================================
/**
 * @CAA2Level L1
 * @CAA2Usage U3
*/
#ifndef CATIMfgNavigatePatternView_H
#define CATIMfgNavigatePatternView_H

#include "MfgItfEnv.h"

#include "CATINavigateObject.h"

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByMfgItfEnv IID IID_CATIMfgNavigatePatternView ;
#else
extern "C" const IID IID_CATIMfgNavigatePatternView;
#endif


/**
 * Navigation interface dedicated to navigate through the manufacturing model 
 * when displayed by patterns.
 */
class ExportedByMfgItfEnv CATIMfgNavigatePatternView: public CATINavigateObject
{
    CATDeclareInterface;

public :

    // No constructors or destructors on this pure virtual base class
    // --------------------------------------------------------------
};

CATDeclareHandler(CATIMfgNavigatePatternView, CATINavigateObject) ;

#endif
