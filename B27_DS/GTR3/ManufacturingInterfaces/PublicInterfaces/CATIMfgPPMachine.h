// COPYRIGHT Dassault Systemes 2008
//===================================================================
//
// CATIMfgPPMachine.h
// Define the CATIMfgPPMachine interface
//
//===================================================================
//
// Usage notes:
//   New interface: Interface to get the Machine Handle during NCCode Generation
//
//===================================================================
//
//  Jun 2008  Creation: Code generated by the CAA wizard  pyc
//===================================================================
#ifndef CATIMfgPPMachine_H
#define CATIMfgPPMachine_H

/**
  * @CAA2Level L1
  * @CAA2Usage U3
  */

#include "MfgItfEnv.h"
#include "CATBaseUnknown.h"

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByMfgItfEnv IID IID_CATIMfgPPMachine;
#else
extern "C" const IID IID_CATIMfgPPMachine ;
#endif

//------------------------------------------------------------------
/**   
 * Interface dedicated to get machine handle during Post Processing Output files.
 * <b>Role</b>: This interface offers services to get machine handle during NCCode Generation.<br>
 * Interface will also repond on CATMfgExtPPManagement(i.e. for External PP Provider) 
 */
class ExportedByMfgItfEnv CATIMfgPPMachine: public CATBaseUnknown
{
  CATDeclareInterface;

  public:

	/**
	 * Gives the machine handle.
	 * @param oMachineBU The machine used by the task for which NCCode is being generated.
	 */
	virtual HRESULT GetMachine(CATBaseUnknown_var &oMachineBU) = 0;

};

CATDeclareHandler(CATIMfgPPMachine, CATBaseUnknown) ;
//------------------------------------------------------------------

#endif
