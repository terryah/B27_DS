// COPYRIGHT Dassault Systemes 2004
//===================================================================
//
// CATIMfgUserOppositeHandMachiningOptions.h
// Define the CATIMfgUserOppositeHandMachiningOptions interface
//
//===================================================================
//
// Usage notes:
//   Interface to define opposite hand machining options
//
//===================================================================
//
//  Dec 2004  Creation: Code generated by the CAA wizard  alc
//===================================================================
#ifndef CATIMfgUserOppositeHandMachiningOptions_H
#define CATIMfgUserOppositeHandMachiningOptions_H

/**
  * @CAA2Level L1
  * @CAA2Usage U5
  */

#include "MfgItfEnv.h"
#include "CATBaseUnknown.h"
#include "CATUnicodeString.h"

#ifndef LOCAL_DEFINITION_FOR_IID
extern ExportedByMfgItfEnv IID IID_CATIMfgUserOppositeHandMachiningOptions;
#else
extern "C" const IID IID_CATIMfgUserOppositeHandMachiningOptions ;
#endif

/**
 * Interface to define opposite hand machining options.
 * <b>Role</b>: set check-box name and associated setting names
 * of a user-operation in the opposite hand machining options panel.
 */
class ExportedByMfgItfEnv CATIMfgUserOppositeHandMachiningOptions: public CATBaseUnknown
{
	CATDeclareInterface;
	
public:
	
  /**
    * Set check-box name and associated setting names
	* of a user-operation in the opposite hand machining options panel.
    * @param oOperationName
	*   Check-box operation name 
	* @param oSettingName
	*   Setting name
	* @param oRepositoryName
	*   Repository name
    */
	virtual HRESULT GetNames(CATUnicodeString & oOperationName, CATUnicodeString & oSettingName, CATUnicodeString & oRepositoryName) = 0;
};
CATDeclareHandler(CATIMfgUserOppositeHandMachiningOptions, CATBaseUnknown);
#endif
