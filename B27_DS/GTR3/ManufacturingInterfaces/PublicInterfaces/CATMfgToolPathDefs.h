// COPYRIGHT DASSAULT SYSTEMES  2000
//=============================================================================

#ifndef __CATMfgToolPathDefs_h__
#define __CATMfgToolPathDefs_h__

/**
* @CAA2Level L1
* @CAA2Usage U3
*/

#include "MfgItfEnv.h"

class CATUnicodeString;

/**
* @nodoc
*/
ExportedByMfgItfEnv extern const CATUnicodeString TPApproachFeedrate;
ExportedByMfgItfEnv extern const CATUnicodeString TPMachiningFeedrate;
ExportedByMfgItfEnv extern const CATUnicodeString TPRetractFeedrate;
ExportedByMfgItfEnv extern const CATUnicodeString TPRapidFeedrate;
ExportedByMfgItfEnv extern const CATUnicodeString TPFinishingFeedrate;
ExportedByMfgItfEnv extern const CATUnicodeString TPChamferingFeedrate;
ExportedByMfgItfEnv extern const CATUnicodeString TPPlungeFeedrate;
ExportedByMfgItfEnv extern const CATUnicodeString TPAirCuttingFeedrate;

ExportedByMfgItfEnv extern const CATUnicodeString TPApproachTraject;
ExportedByMfgItfEnv extern const CATUnicodeString TPMachiningTraject;
ExportedByMfgItfEnv extern const CATUnicodeString TPRetractTraject;
ExportedByMfgItfEnv extern const CATUnicodeString TPLinkingTraject;
ExportedByMfgItfEnv extern const CATUnicodeString TPBetweenPathTraject;
ExportedByMfgItfEnv extern const CATUnicodeString TPApproachTrajectAlongSection;
ExportedByMfgItfEnv extern const CATUnicodeString TPLinkingTrajectAlongSection;
ExportedByMfgItfEnv extern const CATUnicodeString TPRetractTrajectAlongSection;




#endif
