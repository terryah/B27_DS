/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAManufacturingPrecedences_h
#define CATIAManufacturingPrecedences_h

#ifndef ExportedByManufacturingPubIdl
#ifdef _WINDOWS_SOURCE
#ifdef __ManufacturingPubIdl
#define ExportedByManufacturingPubIdl __declspec(dllexport)
#else
#define ExportedByManufacturingPubIdl __declspec(dllimport)
#endif
#else
#define ExportedByManufacturingPubIdl
#endif
#endif

#include "CATIACollection.h"
#include "CATManufacturingPrecedenceType.h"
#include "CATVariant.h"

class CATIAManufacturingActivity;
class CATIAManufacturingPrecedence;

extern ExportedByManufacturingPubIdl IID IID_CATIAManufacturingPrecedences;

class ExportedByManufacturingPubIdl CATIAManufacturingPrecedences : public CATIACollection
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall AddOperation(CATIAManufacturingActivity * iObject, CatManufacturingPrecedenceType iType)=0;

    virtual HRESULT __stdcall Item(const CATVariant & iIndex, CATIAManufacturingPrecedence *& oPrecedence)=0;

    virtual HRESULT __stdcall Remove(CATLONG iIndex)=0;

    virtual HRESULT __stdcall RemoveAll()=0;


};

CATDeclareHandler(CATIAManufacturingPrecedences, CATIACollection);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
