/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAManufacturingToolAssembly2_h
#define CATIAManufacturingToolAssembly2_h

#ifndef ExportedByManufacturingPubIdl
#ifdef _WINDOWS_SOURCE
#ifdef __ManufacturingPubIdl
#define ExportedByManufacturingPubIdl __declspec(dllexport)
#else
#define ExportedByManufacturingPubIdl __declspec(dllimport)
#endif
#else
#define ExportedByManufacturingPubIdl
#endif
#endif

#include "CATIABase.h"

class CATIAManufacturingInsert;
class CATIAManufacturingTool;

extern ExportedByManufacturingPubIdl IID IID_CATIAManufacturingToolAssembly2;

class ExportedByManufacturingPubIdl CATIAManufacturingToolAssembly2 : public CATIABase
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall put_Tool(CATIAManufacturingTool * iMfgTool)=0;

    virtual HRESULT __stdcall put_Insert(CATIAManufacturingInsert * iMfgInsert)=0;


};

CATDeclareHandler(CATIAManufacturingToolAssembly2, CATIABase);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIAManufacturingInsert.h"
#include "CATIAManufacturingTool.h"
#include "CATIAManufacturingToolCorrector.h"
#include "CATIAParameter.h"
#include "CATSafeArray.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
