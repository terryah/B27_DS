/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAManufacturingMachine_h
#define CATIAManufacturingMachine_h

#ifndef ExportedByManufacturingPubIdl
#ifdef _WINDOWS_SOURCE
#ifdef __ManufacturingPubIdl
#define ExportedByManufacturingPubIdl __declspec(dllexport)
#else
#define ExportedByManufacturingPubIdl __declspec(dllimport)
#endif
#else
#define ExportedByManufacturingPubIdl
#endif
#endif

#include "CATBSTR.h"
#include "CATIABase.h"
#include "CATSafeArray.h"

class CATIAParameter;

extern ExportedByManufacturingPubIdl IID IID_CATIAManufacturingMachine;

class ExportedByManufacturingPubIdl CATIAManufacturingMachine : public CATIABase
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_MachineType(CATBSTR & oType)=0;

    virtual HRESULT __stdcall DefaultName(CATBSTR & oTypeNLS)=0;

    virtual HRESULT __stdcall get_Comment(CATBSTR & oComment)=0;

    virtual HRESULT __stdcall get_RotaryAxis(CATBSTR & oRotaryAxis)=0;

    virtual HRESULT __stdcall put_RotaryAxis(const CATBSTR & iRotaryAxis)=0;

    virtual HRESULT __stdcall get_PreferedToolCatalogName(CATBSTR & oPreferedToolCatalogName)=0;

    virtual HRESULT __stdcall put_PreferedToolCatalogName(const CATBSTR & iToolCatalog)=0;

    virtual HRESULT __stdcall get_PPTableName(CATBSTR & oPPTableName)=0;

    virtual HRESULT __stdcall put_PPTableName(const CATBSTR & iPPTableName)=0;

    virtual HRESULT __stdcall set_DefaultValues()=0;

    virtual HRESULT __stdcall GetAttribute(const CATBSTR & iAttribut, CATIAParameter *& oAttributCke)=0;

    virtual HRESULT __stdcall get_NumberOfAttributes(short & oNumber)=0;

    virtual HRESULT __stdcall GetListOfAttributes(CATSafeArrayVariant & oListOfAttributes)=0;

    virtual HRESULT __stdcall GetListOfAttributeUnits(CATSafeArrayVariant & oListOfAttributeUnits)=0;

    virtual HRESULT __stdcall get_NumberOfNumericalControlAttributes(short & oNumber)=0;

    virtual HRESULT __stdcall GetListOfNumericalControlAttributes(CATSafeArrayVariant & oListOfAttributes)=0;

    virtual HRESULT __stdcall get_NumberOfSpindleAttributes(short & oNumber)=0;

    virtual HRESULT __stdcall GetListOfSpindleAttributes(CATSafeArrayVariant & oListOfAttributes)=0;

    virtual HRESULT __stdcall get_NumberOfToolChangeAttributes(short & oNumber)=0;

    virtual HRESULT __stdcall GetListOfToolChangeAttributes(CATSafeArrayVariant & oListOfAttributes)=0;

    virtual HRESULT __stdcall get_NumberOfRotaryTableAttributes(short & oNumber)=0;

    virtual HRESULT __stdcall GetListOfRotaryTableAttributes(CATSafeArrayVariant & oListOfAttributes)=0;

    virtual HRESULT __stdcall GetAttributeNLSName(const CATBSTR & iAttributName, CATBSTR & oNLSName)=0;

    virtual HRESULT __stdcall get_PostProcessorFile(CATBSTR & oPostProcessorFile)=0;

    virtual HRESULT __stdcall put_PostProcessorFile(const CATBSTR & iPostProcessorFile)=0;


};

CATDeclareHandler(CATIAManufacturingMachine, CATIABase);

#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIAParameter.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
