/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAManufacturingPrecedence_h
#define CATIAManufacturingPrecedence_h

#ifndef ExportedByManufacturingPubIdl
#ifdef _WINDOWS_SOURCE
#ifdef __ManufacturingPubIdl
#define ExportedByManufacturingPubIdl __declspec(dllexport)
#else
#define ExportedByManufacturingPubIdl __declspec(dllimport)
#endif
#else
#define ExportedByManufacturingPubIdl
#endif
#endif

#include "CATIABase.h"
#include "CATManufacturingPrecedenceType.h"

class CATIAManufacturingActivity;

extern ExportedByManufacturingPubIdl IID IID_CATIAManufacturingPrecedence;

class ExportedByManufacturingPubIdl CATIAManufacturingPrecedence : public CATIABase
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_TargetOperation(CATIAManufacturingActivity *& oTarget)=0;

    virtual HRESULT __stdcall get_PrecedenceType(CatManufacturingPrecedenceType & oType)=0;


};

CATDeclareHandler(CATIAManufacturingPrecedence, CATIABase);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIAActivity.h"
#include "CATIAAnalyze.h"
#include "CATIACollection.h"
#include "CATIAItems.h"
#include "CATIAManufacturingActivity.h"
#include "CATIAManufacturingFeature.h"
#include "CATIAManufacturingInsert.h"
#include "CATIAManufacturingMachinableArea.h"
#include "CATIAManufacturingMachinableFeat.h"
#include "CATIAManufacturingMachinableGeom.h"
#include "CATIAManufacturingTool.h"
#include "CATIAManufacturingToolAssembly.h"
#include "CATIAManufacturingToolCorrector.h"
#include "CATIAMove.h"
#include "CATIAOutputs.h"
#include "CATIAParameter.h"
#include "CATIAParameters.h"
#include "CATIAPosition.h"
#include "CATIAProduct.h"
#include "CATIAPublications.h"
#include "CATIARelations.h"
#include "CATSPPDeclarations.h"
#include "CATSafeArray.h"
#include "CATVariant.h"
#include "CatFileType.h"
#include "CatProductSource.h"
#include "CatRepType.h"
#include "CatWorkModeType.h"
#include "DNBItemAssignmentType.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
