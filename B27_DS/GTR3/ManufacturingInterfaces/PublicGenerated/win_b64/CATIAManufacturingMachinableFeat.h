/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAManufacturingMachinableFeat_h
#define CATIAManufacturingMachinableFeat_h

#ifndef ExportedByManufacturingPubIdl
#ifdef _WINDOWS_SOURCE
#ifdef __ManufacturingPubIdl
#define ExportedByManufacturingPubIdl __declspec(dllexport)
#else
#define ExportedByManufacturingPubIdl __declspec(dllimport)
#endif
#else
#define ExportedByManufacturingPubIdl
#endif
#endif

#include "CATBSTR.h"
#include "CATIAManufacturingFeature.h"

extern ExportedByManufacturingPubIdl IID IID_CATIAManufacturingMachinableFeat;

class ExportedByManufacturingPubIdl CATIAManufacturingMachinableFeat : public CATIAManufacturingFeature
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall get_FeatType(CATBSTR & oFeatType)=0;

    virtual HRESULT __stdcall put_FeatType(const CATBSTR & iFeatType)=0;

    virtual HRESULT __stdcall get_FeatRemark(CATBSTR & oFeatRemark)=0;

    virtual HRESULT __stdcall put_FeatRemark(const CATBSTR & iFeatRemark)=0;


};

CATDeclareHandler(CATIAManufacturingMachinableFeat, CATIAManufacturingFeature);

#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIABase.h"
#include "CATIAParameter.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
