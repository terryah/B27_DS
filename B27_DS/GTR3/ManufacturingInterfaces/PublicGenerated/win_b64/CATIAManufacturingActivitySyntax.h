/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAManufacturingActivitySyntax_h
#define CATIAManufacturingActivitySyntax_h

#ifndef ExportedByManufacturingPubIdl
#ifdef _WINDOWS_SOURCE
#ifdef __ManufacturingPubIdl
#define ExportedByManufacturingPubIdl __declspec(dllexport)
#else
#define ExportedByManufacturingPubIdl __declspec(dllimport)
#endif
#else
#define ExportedByManufacturingPubIdl
#endif
#endif

#include "CATBSTR.h"
#include "CATIABase.h"

extern ExportedByManufacturingPubIdl IID IID_CATIAManufacturingActivitySyntax;

class ExportedByManufacturingPubIdl CATIAManufacturingActivitySyntax : public CATIABase
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall ResetPPWORDSyntax()=0;

    virtual HRESULT __stdcall SetPPWORDSyntax(const CATBSTR & iPPWORDs)=0;


};

CATDeclareHandler(CATIAManufacturingActivitySyntax, CATIABase);

#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
