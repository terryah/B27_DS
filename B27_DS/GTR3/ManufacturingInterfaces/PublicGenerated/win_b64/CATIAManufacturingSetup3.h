/*====================================================
 *  (c) 1999 Dassault Systemes. All rights reserved  =
 *     Generated by CNextBackEnd version 0.1 on      =
 *===================================================*/

#ifndef CATIAManufacturingSetup3_h
#define CATIAManufacturingSetup3_h

#ifndef ExportedByManufacturingPubIdl
#ifdef _WINDOWS_SOURCE
#ifdef __ManufacturingPubIdl
#define ExportedByManufacturingPubIdl __declspec(dllexport)
#else
#define ExportedByManufacturingPubIdl __declspec(dllimport)
#endif
#else
#define ExportedByManufacturingPubIdl
#endif
#endif

#include "CATIAManufacturingActivity.h"

class CATIABase;

extern ExportedByManufacturingPubIdl IID IID_CATIAManufacturingSetup3;

class ExportedByManufacturingPubIdl CATIAManufacturingSetup3 : public CATIAManufacturingActivity
{
    CATDeclareInterface;

public:

    virtual HRESULT __stdcall AddFixture(CATIABase * iGeometry, CATIABase * iProduct, short iPosition)=0;


};

CATDeclareHandler(CATIAManufacturingSetup3, CATIAManufacturingActivity);

#include "CATBSTR.h"
#include "CATBaseDispatch.h"
#include "CATBaseUnknown.h"
#include "CATIAActivity.h"
#include "CATIAAnalyze.h"
#include "CATIABase.h"
#include "CATIACollection.h"
#include "CATIAItems.h"
#include "CATIAManufacturingFeature.h"
#include "CATIAManufacturingInsert.h"
#include "CATIAManufacturingMachinableArea.h"
#include "CATIAManufacturingMachinableFeat.h"
#include "CATIAManufacturingMachinableGeom.h"
#include "CATIAManufacturingTool.h"
#include "CATIAManufacturingToolAssembly.h"
#include "CATIAManufacturingToolCorrector.h"
#include "CATIAMove.h"
#include "CATIAOutputs.h"
#include "CATIAParameter.h"
#include "CATIAParameters.h"
#include "CATIAPosition.h"
#include "CATIAProduct.h"
#include "CATIAPublications.h"
#include "CATIARelations.h"
#include "CATSPPDeclarations.h"
#include "CATSafeArray.h"
#include "CATVariant.h"
#include "CatFileType.h"
#include "CatProductSource.h"
#include "CatRepType.h"
#include "CatWorkModeType.h"
#include "DNBItemAssignmentType.h"
#include "IDispatch.h"
#include "IUnknown.h"


#endif
