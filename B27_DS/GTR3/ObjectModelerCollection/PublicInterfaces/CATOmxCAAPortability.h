#ifndef __CATOmxCAAPortability_h__
#define __CATOmxCAAPortability_h__
// COPYRIGHT DASSAULT SYSTEMES 2013
// Avoid cross platform typical build errors.
// THIS File should not be directly included
/** @CAA2Required */
#include "CATIAV5Level.h"

#include <new.h>
#include "CATSysBoolean.h"
#include "CATCORBABoolean.h"
#include "CATDataType.h"
#include "CATErrorDef.h"
#include "IUnknown.h"
#include "CATOmx.h"
#include <stddef.h>
/** @nodoc */
typedef unsigned int CATHashKey;
/** @nodoc */
typedef unsigned char octet;
/** @nodoc */
typedef CATBoolean boolean;
#endif

