/* -*-c++-*- */
// COPYRIGHT DASSAULT SYSTEMES 2000
#ifndef sequence_CORBAAny_h
#define sequence_CORBAAny_h
/**
 * @CAA2Level L1
 * @CAA2Usage U1
 */
#include "sequence.h"
#include "CORBAAny.h"
#include "CATLISTV_CATBaseUnknown.h"
/** Define a sequence of CORBAAny. */
DEF_SEQUENCE(CORBAAny, CATOmxKernel)
#endif
