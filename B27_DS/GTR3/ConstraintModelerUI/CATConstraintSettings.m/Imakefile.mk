# COPYRIGHT DASSAULT SYSTEMES 2002
#======================================================================
# Imakefile for module CATConstraintSettings.m
#======================================================================
#
#  Mar 2002  Creation: Code generated by the CAA wizard  PCC
#======================================================================
#
# SHARED LIBRARY 
#
BUILT_OBJECT_TYPE=SHARED LIBRARY 
 
# DO NOT EDIT :: THE CAA2 WIZARDS WILL ADD CODE HERE
WIZARD_LINK_MODULES =  \
JS0GROUP \
AD0XXBAS \
CK0UNIT \
DI0PANV2 \
VE0GEDIT \
JS0FM \
CD0FRAME \
CATLifRelations \
DI0PANV2 \
CATDlgStandard \
CATLiteralFeatures \
CATVisualization \
YN000MFL \
CATObjectSpecsModeler \
KnowledgeItf \
CATViz \
CATMathStream \
# END WIZARD EDITION ZONE

LINK_WITH = $(WIZARD_LINK_MODULES)

# System dependant variables
#
OS = AIX
#
OS = HP-UX
#
OS = IRIX
#
OS = SunOS
#
OS = Windows_NT
