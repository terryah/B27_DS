#ifndef __TIE_CATIArrAttachmentFactory
#define __TIE_CATIArrAttachmentFactory

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATIArrAttachmentFactory.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface CATIArrAttachmentFactory */
#define declare_TIE_CATIArrAttachmentFactory(classe) \
 \
 \
class TIECATIArrAttachmentFactory##classe : public CATIArrAttachmentFactory \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(CATIArrAttachmentFactory, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT Attach (CATIProduct *iParrent, CATIProduct *iChild) ; \
      virtual HRESULT Attach (CATIProduct *iParrent, CATIProduct *iChild, CATIArrIgpAttachment **oAttach) ; \
      virtual HRESULT AttachMA (CATISpecObject * iParent , CATISpecObject * iChild ) ; \
      virtual HRESULT AttachMA (CATISpecObject * iParent , CATISpecObject * iChild, CATIArrIgpAttachment **oAttach ) ; \
      virtual HRESULT Remove ( CATIArrIgpAttachment *iAttach ) ; \
};



#define ENVTIEdeclare_CATIArrAttachmentFactory(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT Attach (CATIProduct *iParrent, CATIProduct *iChild) ; \
virtual HRESULT Attach (CATIProduct *iParrent, CATIProduct *iChild, CATIArrIgpAttachment **oAttach) ; \
virtual HRESULT AttachMA (CATISpecObject * iParent , CATISpecObject * iChild ) ; \
virtual HRESULT AttachMA (CATISpecObject * iParent , CATISpecObject * iChild, CATIArrIgpAttachment **oAttach ) ; \
virtual HRESULT Remove ( CATIArrIgpAttachment *iAttach ) ; \


#define ENVTIEdefine_CATIArrAttachmentFactory(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT  ENVTIEName::Attach (CATIProduct *iParrent, CATIProduct *iChild)  \
{ \
return (ENVTIECALL(CATIArrAttachmentFactory,ENVTIETypeLetter,ENVTIELetter)Attach (iParrent,iChild)); \
} \
HRESULT  ENVTIEName::Attach (CATIProduct *iParrent, CATIProduct *iChild, CATIArrIgpAttachment **oAttach)  \
{ \
return (ENVTIECALL(CATIArrAttachmentFactory,ENVTIETypeLetter,ENVTIELetter)Attach (iParrent,iChild,oAttach)); \
} \
HRESULT  ENVTIEName::AttachMA (CATISpecObject * iParent , CATISpecObject * iChild )  \
{ \
return (ENVTIECALL(CATIArrAttachmentFactory,ENVTIETypeLetter,ENVTIELetter)AttachMA (iParent ,iChild )); \
} \
HRESULT  ENVTIEName::AttachMA (CATISpecObject * iParent , CATISpecObject * iChild, CATIArrIgpAttachment **oAttach )  \
{ \
return (ENVTIECALL(CATIArrAttachmentFactory,ENVTIETypeLetter,ENVTIELetter)AttachMA (iParent ,iChild,oAttach )); \
} \
HRESULT  ENVTIEName::Remove ( CATIArrIgpAttachment *iAttach )  \
{ \
return (ENVTIECALL(CATIArrAttachmentFactory,ENVTIETypeLetter,ENVTIELetter)Remove (iAttach )); \
} \


/* Name of the TIE class */
#define class_TIE_CATIArrAttachmentFactory(classe)    TIECATIArrAttachmentFactory##classe


/* Common methods inside a TIE */
#define common_TIE_CATIArrAttachmentFactory(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(CATIArrAttachmentFactory, classe) \
 \
 \
CATImplementTIEMethods(CATIArrAttachmentFactory, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(CATIArrAttachmentFactory, classe, 1) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(CATIArrAttachmentFactory, classe) \
CATImplementCATBaseUnknownMethodsForTIE(CATIArrAttachmentFactory, classe) \
 \
HRESULT  TIECATIArrAttachmentFactory##classe::Attach (CATIProduct *iParrent, CATIProduct *iChild)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Attach (iParrent,iChild)); \
} \
HRESULT  TIECATIArrAttachmentFactory##classe::Attach (CATIProduct *iParrent, CATIProduct *iChild, CATIArrIgpAttachment **oAttach)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Attach (iParrent,iChild,oAttach)); \
} \
HRESULT  TIECATIArrAttachmentFactory##classe::AttachMA (CATISpecObject * iParent , CATISpecObject * iChild )  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->AttachMA (iParent ,iChild )); \
} \
HRESULT  TIECATIArrAttachmentFactory##classe::AttachMA (CATISpecObject * iParent , CATISpecObject * iChild, CATIArrIgpAttachment **oAttach )  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->AttachMA (iParent ,iChild,oAttach )); \
} \
HRESULT  TIECATIArrAttachmentFactory##classe::Remove ( CATIArrIgpAttachment *iAttach )  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Remove (iAttach )); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_CATIArrAttachmentFactory(classe) \
 \
 \
declare_TIE_CATIArrAttachmentFactory(classe) \
 \
 \
CATMetaClass * __stdcall TIECATIArrAttachmentFactory##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_CATIArrAttachmentFactory,"CATIArrAttachmentFactory",CATIArrAttachmentFactory::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_CATIArrAttachmentFactory(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(CATIArrAttachmentFactory, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicCATIArrAttachmentFactory##classe(classe::MetaObject(),CATIArrAttachmentFactory::MetaObject(),(void *)CreateTIECATIArrAttachmentFactory##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_CATIArrAttachmentFactory(classe) \
 \
 \
declare_TIE_CATIArrAttachmentFactory(classe) \
 \
 \
CATMetaClass * __stdcall TIECATIArrAttachmentFactory##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_CATIArrAttachmentFactory,"CATIArrAttachmentFactory",CATIArrAttachmentFactory::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_CATIArrAttachmentFactory(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(CATIArrAttachmentFactory, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicCATIArrAttachmentFactory##classe(classe::MetaObject(),CATIArrAttachmentFactory::MetaObject(),(void *)CreateTIECATIArrAttachmentFactory##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_CATIArrAttachmentFactory(classe) TIE_CATIArrAttachmentFactory(classe)
#else
#define BOA_CATIArrAttachmentFactory(classe) CATImplementBOA(CATIArrAttachmentFactory, classe)
#endif

#endif
