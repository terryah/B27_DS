#ifndef __TIE_DNBIAMRLDocAttachments
#define __TIE_DNBIAMRLDocAttachments

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIAMRLDocAttachments.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIAMRLDocAttachments */
#define declare_TIE_DNBIAMRLDocAttachments(classe) \
 \
 \
class TIEDNBIAMRLDocAttachments##classe : public DNBIAMRLDocAttachments \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIAMRLDocAttachments, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall GetListofAttachments(CATSafeArrayVariant *& oAttachList); \
      virtual HRESULT __stdcall GetAttachmentFactory(CATBaseDispatch *& oAttachFactory); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIAMRLDocAttachments(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall GetListofAttachments(CATSafeArrayVariant *& oAttachList); \
virtual HRESULT __stdcall GetAttachmentFactory(CATBaseDispatch *& oAttachFactory); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIAMRLDocAttachments(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::GetListofAttachments(CATSafeArrayVariant *& oAttachList) \
{ \
return (ENVTIECALL(DNBIAMRLDocAttachments,ENVTIETypeLetter,ENVTIELetter)GetListofAttachments(oAttachList)); \
} \
HRESULT __stdcall  ENVTIEName::GetAttachmentFactory(CATBaseDispatch *& oAttachFactory) \
{ \
return (ENVTIECALL(DNBIAMRLDocAttachments,ENVTIETypeLetter,ENVTIELetter)GetAttachmentFactory(oAttachFactory)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIAMRLDocAttachments,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIAMRLDocAttachments,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIAMRLDocAttachments,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIAMRLDocAttachments,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIAMRLDocAttachments,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIAMRLDocAttachments(classe)    TIEDNBIAMRLDocAttachments##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIAMRLDocAttachments(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIAMRLDocAttachments, classe) \
 \
 \
CATImplementTIEMethods(DNBIAMRLDocAttachments, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIAMRLDocAttachments, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIAMRLDocAttachments, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIAMRLDocAttachments, classe) \
 \
HRESULT __stdcall  TIEDNBIAMRLDocAttachments##classe::GetListofAttachments(CATSafeArrayVariant *& oAttachList) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&oAttachList); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetListofAttachments(oAttachList); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&oAttachList); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMRLDocAttachments##classe::GetAttachmentFactory(CATBaseDispatch *& oAttachFactory) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&oAttachFactory); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetAttachmentFactory(oAttachFactory); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&oAttachFactory); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMRLDocAttachments##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMRLDocAttachments##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMRLDocAttachments##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMRLDocAttachments##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMRLDocAttachments##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIAMRLDocAttachments(classe) \
 \
 \
declare_TIE_DNBIAMRLDocAttachments(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAMRLDocAttachments##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAMRLDocAttachments,"DNBIAMRLDocAttachments",DNBIAMRLDocAttachments::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAMRLDocAttachments(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIAMRLDocAttachments, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAMRLDocAttachments##classe(classe::MetaObject(),DNBIAMRLDocAttachments::MetaObject(),(void *)CreateTIEDNBIAMRLDocAttachments##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIAMRLDocAttachments(classe) \
 \
 \
declare_TIE_DNBIAMRLDocAttachments(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAMRLDocAttachments##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAMRLDocAttachments,"DNBIAMRLDocAttachments",DNBIAMRLDocAttachments::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAMRLDocAttachments(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIAMRLDocAttachments, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAMRLDocAttachments##classe(classe::MetaObject(),DNBIAMRLDocAttachments::MetaObject(),(void *)CreateTIEDNBIAMRLDocAttachments##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIAMRLDocAttachments(classe) TIE_DNBIAMRLDocAttachments(classe)
#else
#define BOA_DNBIAMRLDocAttachments(classe) CATImplementBOA(DNBIAMRLDocAttachments, classe)
#endif

#endif
