#ifndef __TIE_DNBIAMRLAttachment
#define __TIE_DNBIAMRLAttachment

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIAMRLAttachment.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIAMRLAttachment */
#define declare_TIE_DNBIAMRLAttachment(classe) \
 \
 \
class TIEDNBIAMRLAttachment##classe : public DNBIAMRLAttachment \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIAMRLAttachment, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall GetParent(CATIAProduct *& oParent); \
      virtual HRESULT __stdcall GetChild(CATIAProduct *& oChild); \
      virtual HRESULT __stdcall GetParentMA(CATBaseDispatch *& oParentMA); \
      virtual HRESULT __stdcall GetChildMA(CATBaseDispatch *& oChildMA); \
      virtual HRESULT __stdcall IsParentMA(CAT_VARIANT_BOOL & iParentMA); \
      virtual HRESULT __stdcall IsChildMA(CAT_VARIANT_BOOL & iChildMA); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIAMRLAttachment(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall GetParent(CATIAProduct *& oParent); \
virtual HRESULT __stdcall GetChild(CATIAProduct *& oChild); \
virtual HRESULT __stdcall GetParentMA(CATBaseDispatch *& oParentMA); \
virtual HRESULT __stdcall GetChildMA(CATBaseDispatch *& oChildMA); \
virtual HRESULT __stdcall IsParentMA(CAT_VARIANT_BOOL & iParentMA); \
virtual HRESULT __stdcall IsChildMA(CAT_VARIANT_BOOL & iChildMA); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIAMRLAttachment(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::GetParent(CATIAProduct *& oParent) \
{ \
return (ENVTIECALL(DNBIAMRLAttachment,ENVTIETypeLetter,ENVTIELetter)GetParent(oParent)); \
} \
HRESULT __stdcall  ENVTIEName::GetChild(CATIAProduct *& oChild) \
{ \
return (ENVTIECALL(DNBIAMRLAttachment,ENVTIETypeLetter,ENVTIELetter)GetChild(oChild)); \
} \
HRESULT __stdcall  ENVTIEName::GetParentMA(CATBaseDispatch *& oParentMA) \
{ \
return (ENVTIECALL(DNBIAMRLAttachment,ENVTIETypeLetter,ENVTIELetter)GetParentMA(oParentMA)); \
} \
HRESULT __stdcall  ENVTIEName::GetChildMA(CATBaseDispatch *& oChildMA) \
{ \
return (ENVTIECALL(DNBIAMRLAttachment,ENVTIETypeLetter,ENVTIELetter)GetChildMA(oChildMA)); \
} \
HRESULT __stdcall  ENVTIEName::IsParentMA(CAT_VARIANT_BOOL & iParentMA) \
{ \
return (ENVTIECALL(DNBIAMRLAttachment,ENVTIETypeLetter,ENVTIELetter)IsParentMA(iParentMA)); \
} \
HRESULT __stdcall  ENVTIEName::IsChildMA(CAT_VARIANT_BOOL & iChildMA) \
{ \
return (ENVTIECALL(DNBIAMRLAttachment,ENVTIETypeLetter,ENVTIELetter)IsChildMA(iChildMA)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIAMRLAttachment,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIAMRLAttachment,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIAMRLAttachment,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIAMRLAttachment,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIAMRLAttachment,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIAMRLAttachment(classe)    TIEDNBIAMRLAttachment##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIAMRLAttachment(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIAMRLAttachment, classe) \
 \
 \
CATImplementTIEMethods(DNBIAMRLAttachment, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIAMRLAttachment, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIAMRLAttachment, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIAMRLAttachment, classe) \
 \
HRESULT __stdcall  TIEDNBIAMRLAttachment##classe::GetParent(CATIAProduct *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&oParent); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetParent(oParent); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMRLAttachment##classe::GetChild(CATIAProduct *& oChild) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&oChild); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetChild(oChild); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&oChild); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMRLAttachment##classe::GetParentMA(CATBaseDispatch *& oParentMA) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&oParentMA); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetParentMA(oParentMA); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&oParentMA); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMRLAttachment##classe::GetChildMA(CATBaseDispatch *& oChildMA) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&oChildMA); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetChildMA(oChildMA); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&oChildMA); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMRLAttachment##classe::IsParentMA(CAT_VARIANT_BOOL & iParentMA) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&iParentMA); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->IsParentMA(iParentMA); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&iParentMA); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMRLAttachment##classe::IsChildMA(CAT_VARIANT_BOOL & iChildMA) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&iChildMA); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->IsChildMA(iChildMA); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&iChildMA); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMRLAttachment##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMRLAttachment##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,8,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,8,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMRLAttachment##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,9,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,9,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMRLAttachment##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,10,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,10,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMRLAttachment##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,11,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,11,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIAMRLAttachment(classe) \
 \
 \
declare_TIE_DNBIAMRLAttachment(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAMRLAttachment##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAMRLAttachment,"DNBIAMRLAttachment",DNBIAMRLAttachment::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAMRLAttachment(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIAMRLAttachment, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAMRLAttachment##classe(classe::MetaObject(),DNBIAMRLAttachment::MetaObject(),(void *)CreateTIEDNBIAMRLAttachment##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIAMRLAttachment(classe) \
 \
 \
declare_TIE_DNBIAMRLAttachment(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAMRLAttachment##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAMRLAttachment,"DNBIAMRLAttachment",DNBIAMRLAttachment::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAMRLAttachment(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIAMRLAttachment, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAMRLAttachment##classe(classe::MetaObject(),DNBIAMRLAttachment::MetaObject(),(void *)CreateTIEDNBIAMRLAttachment##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIAMRLAttachment(classe) TIE_DNBIAMRLAttachment(classe)
#else
#define BOA_DNBIAMRLAttachment(classe) CATImplementBOA(DNBIAMRLAttachment, classe)
#endif

#endif
