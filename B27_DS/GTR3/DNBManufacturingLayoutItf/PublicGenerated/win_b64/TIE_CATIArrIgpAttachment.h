#ifndef __TIE_CATIArrIgpAttachment
#define __TIE_CATIArrIgpAttachment

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATIArrIgpAttachment.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface CATIArrIgpAttachment */
#define declare_TIE_CATIArrIgpAttachment(classe) \
 \
 \
class TIECATIArrIgpAttachment##classe : public CATIArrIgpAttachment \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(CATIArrIgpAttachment, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT GetParent(CATIProduct **oParent) ; \
      virtual HRESULT GetChild(CATIProduct **oChild) ; \
      virtual HRESULT GetChildMA (CATBaseUnknown ** oChildMA) ; \
      virtual HRESULT GetParentMA (CATBaseUnknown ** oParentMA) ; \
      virtual HRESULT UnDelete() ; \
      virtual HRESULT IsDeleted() ; \
};



#define ENVTIEdeclare_CATIArrIgpAttachment(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT GetParent(CATIProduct **oParent) ; \
virtual HRESULT GetChild(CATIProduct **oChild) ; \
virtual HRESULT GetChildMA (CATBaseUnknown ** oChildMA) ; \
virtual HRESULT GetParentMA (CATBaseUnknown ** oParentMA) ; \
virtual HRESULT UnDelete() ; \
virtual HRESULT IsDeleted() ; \


#define ENVTIEdefine_CATIArrIgpAttachment(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT  ENVTIEName::GetParent(CATIProduct **oParent)  \
{ \
return (ENVTIECALL(CATIArrIgpAttachment,ENVTIETypeLetter,ENVTIELetter)GetParent(oParent)); \
} \
HRESULT  ENVTIEName::GetChild(CATIProduct **oChild)  \
{ \
return (ENVTIECALL(CATIArrIgpAttachment,ENVTIETypeLetter,ENVTIELetter)GetChild(oChild)); \
} \
HRESULT  ENVTIEName::GetChildMA (CATBaseUnknown ** oChildMA)  \
{ \
return (ENVTIECALL(CATIArrIgpAttachment,ENVTIETypeLetter,ENVTIELetter)GetChildMA (oChildMA)); \
} \
HRESULT  ENVTIEName::GetParentMA (CATBaseUnknown ** oParentMA)  \
{ \
return (ENVTIECALL(CATIArrIgpAttachment,ENVTIETypeLetter,ENVTIELetter)GetParentMA (oParentMA)); \
} \
HRESULT  ENVTIEName::UnDelete()  \
{ \
return (ENVTIECALL(CATIArrIgpAttachment,ENVTIETypeLetter,ENVTIELetter)UnDelete()); \
} \
HRESULT  ENVTIEName::IsDeleted()  \
{ \
return (ENVTIECALL(CATIArrIgpAttachment,ENVTIETypeLetter,ENVTIELetter)IsDeleted()); \
} \


/* Name of the TIE class */
#define class_TIE_CATIArrIgpAttachment(classe)    TIECATIArrIgpAttachment##classe


/* Common methods inside a TIE */
#define common_TIE_CATIArrIgpAttachment(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(CATIArrIgpAttachment, classe) \
 \
 \
CATImplementTIEMethods(CATIArrIgpAttachment, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(CATIArrIgpAttachment, classe, 1) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(CATIArrIgpAttachment, classe) \
CATImplementCATBaseUnknownMethodsForTIE(CATIArrIgpAttachment, classe) \
 \
HRESULT  TIECATIArrIgpAttachment##classe::GetParent(CATIProduct **oParent)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetParent(oParent)); \
} \
HRESULT  TIECATIArrIgpAttachment##classe::GetChild(CATIProduct **oChild)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetChild(oChild)); \
} \
HRESULT  TIECATIArrIgpAttachment##classe::GetChildMA (CATBaseUnknown ** oChildMA)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetChildMA (oChildMA)); \
} \
HRESULT  TIECATIArrIgpAttachment##classe::GetParentMA (CATBaseUnknown ** oParentMA)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetParentMA (oParentMA)); \
} \
HRESULT  TIECATIArrIgpAttachment##classe::UnDelete()  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->UnDelete()); \
} \
HRESULT  TIECATIArrIgpAttachment##classe::IsDeleted()  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->IsDeleted()); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_CATIArrIgpAttachment(classe) \
 \
 \
declare_TIE_CATIArrIgpAttachment(classe) \
 \
 \
CATMetaClass * __stdcall TIECATIArrIgpAttachment##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_CATIArrIgpAttachment,"CATIArrIgpAttachment",CATIArrIgpAttachment::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_CATIArrIgpAttachment(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(CATIArrIgpAttachment, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicCATIArrIgpAttachment##classe(classe::MetaObject(),CATIArrIgpAttachment::MetaObject(),(void *)CreateTIECATIArrIgpAttachment##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_CATIArrIgpAttachment(classe) \
 \
 \
declare_TIE_CATIArrIgpAttachment(classe) \
 \
 \
CATMetaClass * __stdcall TIECATIArrIgpAttachment##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_CATIArrIgpAttachment,"CATIArrIgpAttachment",CATIArrIgpAttachment::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_CATIArrIgpAttachment(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(CATIArrIgpAttachment, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicCATIArrIgpAttachment##classe(classe::MetaObject(),CATIArrIgpAttachment::MetaObject(),(void *)CreateTIECATIArrIgpAttachment##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_CATIArrIgpAttachment(classe) TIE_CATIArrIgpAttachment(classe)
#else
#define BOA_CATIArrIgpAttachment(classe) CATImplementBOA(CATIArrIgpAttachment, classe)
#endif

#endif
