#ifndef __TIE_DNBIAMRLAttachmentFactory
#define __TIE_DNBIAMRLAttachmentFactory

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATExitJournal.h"
#include "DNBIAMRLAttachmentFactory.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface DNBIAMRLAttachmentFactory */
#define declare_TIE_DNBIAMRLAttachmentFactory(classe) \
 \
 \
class TIEDNBIAMRLAttachmentFactory##classe : public DNBIAMRLAttachmentFactory \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(DNBIAMRLAttachmentFactory, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT __stdcall Attach(CATBaseDispatch * iParent, CATBaseDispatch * iChild, CATBaseDispatch *& oAttachment); \
      virtual HRESULT __stdcall Remove(CATBaseDispatch * iAttachment); \
      virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
      virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
      virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
      virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
      virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \
};



#define ENVTIEdeclare_DNBIAMRLAttachmentFactory(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT __stdcall Attach(CATBaseDispatch * iParent, CATBaseDispatch * iChild, CATBaseDispatch *& oAttachment); \
virtual HRESULT __stdcall Remove(CATBaseDispatch * iAttachment); \
virtual HRESULT  __stdcall get_Application(CATIAApplication *& oApplication); \
virtual HRESULT  __stdcall get_Parent(CATBaseDispatch *& oParent); \
virtual HRESULT  __stdcall get_Name(CATBSTR & oNameBSTR); \
virtual HRESULT  __stdcall put_Name(const CATBSTR & iNameBSTR); \
virtual HRESULT  __stdcall GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj); \


#define ENVTIEdefine_DNBIAMRLAttachmentFactory(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT __stdcall  ENVTIEName::Attach(CATBaseDispatch * iParent, CATBaseDispatch * iChild, CATBaseDispatch *& oAttachment) \
{ \
return (ENVTIECALL(DNBIAMRLAttachmentFactory,ENVTIETypeLetter,ENVTIELetter)Attach(iParent,iChild,oAttachment)); \
} \
HRESULT __stdcall  ENVTIEName::Remove(CATBaseDispatch * iAttachment) \
{ \
return (ENVTIECALL(DNBIAMRLAttachmentFactory,ENVTIETypeLetter,ENVTIELetter)Remove(iAttachment)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Application(CATIAApplication *& oApplication) \
{ \
return (ENVTIECALL(DNBIAMRLAttachmentFactory,ENVTIETypeLetter,ENVTIELetter)get_Application(oApplication)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Parent(CATBaseDispatch *& oParent) \
{ \
return (ENVTIECALL(DNBIAMRLAttachmentFactory,ENVTIETypeLetter,ENVTIELetter)get_Parent(oParent)); \
} \
HRESULT  __stdcall  ENVTIEName::get_Name(CATBSTR & oNameBSTR) \
{ \
return (ENVTIECALL(DNBIAMRLAttachmentFactory,ENVTIETypeLetter,ENVTIELetter)get_Name(oNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::put_Name(const CATBSTR & iNameBSTR) \
{ \
return (ENVTIECALL(DNBIAMRLAttachmentFactory,ENVTIETypeLetter,ENVTIELetter)put_Name(iNameBSTR)); \
} \
HRESULT  __stdcall  ENVTIEName::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
return (ENVTIECALL(DNBIAMRLAttachmentFactory,ENVTIETypeLetter,ENVTIELetter)GetItem(IDName,RealObj)); \
} \


/* Name of the TIE class */
#define class_TIE_DNBIAMRLAttachmentFactory(classe)    TIEDNBIAMRLAttachmentFactory##classe


/* Common methods inside a TIE */
#define common_TIE_DNBIAMRLAttachmentFactory(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(DNBIAMRLAttachmentFactory, classe) \
 \
 \
CATImplementTIEMethods(DNBIAMRLAttachmentFactory, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(DNBIAMRLAttachmentFactory, classe, 2) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(DNBIAMRLAttachmentFactory, classe) \
CATImplementCATBaseUnknownMethodsForTIE(DNBIAMRLAttachmentFactory, classe) \
 \
HRESULT __stdcall  TIEDNBIAMRLAttachmentFactory##classe::Attach(CATBaseDispatch * iParent, CATBaseDispatch * iChild, CATBaseDispatch *& oAttachment) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,1,&_Trac2,&iParent,&iChild,&oAttachment); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Attach(iParent,iChild,oAttachment); \
   ExitAfterCall(this,1,_Trac2,&_ret_arg,&iParent,&iChild,&oAttachment); \
   return(_ret_arg); \
} \
HRESULT __stdcall  TIEDNBIAMRLAttachmentFactory##classe::Remove(CATBaseDispatch * iAttachment) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,2,&_Trac2,&iAttachment); \
   HRESULT  _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Remove(iAttachment); \
   ExitAfterCall(this,2,_Trac2,&_ret_arg,&iAttachment); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMRLAttachmentFactory##classe::get_Application(CATIAApplication *& oApplication) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,3,&_Trac2,&oApplication); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Application(oApplication); \
   ExitAfterCall(this,3,_Trac2,&_ret_arg,&oApplication); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMRLAttachmentFactory##classe::get_Parent(CATBaseDispatch *& oParent) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,4,&_Trac2,&oParent); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Parent(oParent); \
   ExitAfterCall(this,4,_Trac2,&_ret_arg,&oParent); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMRLAttachmentFactory##classe::get_Name(CATBSTR & oNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,5,&_Trac2,&oNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->get_Name(oNameBSTR); \
   ExitAfterCall(this,5,_Trac2,&_ret_arg,&oNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMRLAttachmentFactory##classe::put_Name(const CATBSTR & iNameBSTR) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,6,&_Trac2,&iNameBSTR); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->put_Name(iNameBSTR); \
   ExitAfterCall(this,6,_Trac2,&_ret_arg,&iNameBSTR); \
   return(_ret_arg); \
} \
HRESULT  __stdcall  TIEDNBIAMRLAttachmentFactory##classe::GetItem(const CATBSTR & IDName, CATBaseDispatch *& RealObj) \
{ \
   CATICallTrac2 *_Trac2; \
   ExitBeforeCall(this,7,&_Trac2,&IDName,&RealObj); \
   HRESULT   _ret_arg = ((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->GetItem(IDName,RealObj); \
   ExitAfterCall(this,7,_Trac2,&_ret_arg,&IDName,&RealObj); \
   return(_ret_arg); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_DNBIAMRLAttachmentFactory(classe) \
 \
 \
declare_TIE_DNBIAMRLAttachmentFactory(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAMRLAttachmentFactory##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAMRLAttachmentFactory,"DNBIAMRLAttachmentFactory",DNBIAMRLAttachmentFactory::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAMRLAttachmentFactory(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(DNBIAMRLAttachmentFactory, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAMRLAttachmentFactory##classe(classe::MetaObject(),DNBIAMRLAttachmentFactory::MetaObject(),(void *)CreateTIEDNBIAMRLAttachmentFactory##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_DNBIAMRLAttachmentFactory(classe) \
 \
 \
declare_TIE_DNBIAMRLAttachmentFactory(classe) \
 \
 \
CATMetaClass * __stdcall TIEDNBIAMRLAttachmentFactory##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_DNBIAMRLAttachmentFactory,"DNBIAMRLAttachmentFactory",DNBIAMRLAttachmentFactory::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_DNBIAMRLAttachmentFactory(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(DNBIAMRLAttachmentFactory, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicDNBIAMRLAttachmentFactory##classe(classe::MetaObject(),DNBIAMRLAttachmentFactory::MetaObject(),(void *)CreateTIEDNBIAMRLAttachmentFactory##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_DNBIAMRLAttachmentFactory(classe) TIE_DNBIAMRLAttachmentFactory(classe)
#else
#define BOA_DNBIAMRLAttachmentFactory(classe) CATImplementBOA(DNBIAMRLAttachmentFactory, classe)
#endif

#endif
