#ifndef __TIE_CATIArrAttachSubscriber
#define __TIE_CATIArrAttachSubscriber

#include <string.h>
#include "CATBaseUnknown.h"
#include "CATMetaClass.h"
#include "CATMacForTie.h"
#include "CATIArrAttachSubscriber.h"
#include "JS0DSPA.h"


#ifdef _WINDOWS_SOURCE
#define Exported __declspec(dllexport)
#define Imported __declspec(dllimport)
#else
#define Exported 
#define Imported 
#endif


/* To link an implementation with the interface CATIArrAttachSubscriber */
#define declare_TIE_CATIArrAttachSubscriber(classe) \
 \
 \
class TIECATIArrAttachSubscriber##classe : public CATIArrAttachSubscriber \
{ \
   private: \
      CATDeclareCommonTIEMembers \
   public: \
      CATDeclareTIEMethods(CATIArrAttachSubscriber, classe) \
      CATDeclareIUnknownMethodsForCATBaseUnknownTIE \
      CATDeclareIDispatchMethodsForCATBaseUnknownTIE \
      CATDeclareCATBaseUnknownMethodsForTIE \
      virtual HRESULT DoConnection() ; \
      virtual HRESULT IsConnected(CATBoolean *oStatus) ; \
      virtual HRESULT RemoveConnection() ; \
      virtual HRESULT Subscribe() ; \
      virtual HRESULT UnSubscribe() ; \
};



#define ENVTIEdeclare_CATIArrAttachSubscriber(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
virtual HRESULT DoConnection() ; \
virtual HRESULT IsConnected(CATBoolean *oStatus) ; \
virtual HRESULT RemoveConnection() ; \
virtual HRESULT Subscribe() ; \
virtual HRESULT UnSubscribe() ; \


#define ENVTIEdefine_CATIArrAttachSubscriber(ENVTIEName,ENVTIETypeLetter,ENVTIELetter) \
HRESULT  ENVTIEName::DoConnection()  \
{ \
return (ENVTIECALL(CATIArrAttachSubscriber,ENVTIETypeLetter,ENVTIELetter)DoConnection()); \
} \
HRESULT  ENVTIEName::IsConnected(CATBoolean *oStatus)  \
{ \
return (ENVTIECALL(CATIArrAttachSubscriber,ENVTIETypeLetter,ENVTIELetter)IsConnected(oStatus)); \
} \
HRESULT  ENVTIEName::RemoveConnection()  \
{ \
return (ENVTIECALL(CATIArrAttachSubscriber,ENVTIETypeLetter,ENVTIELetter)RemoveConnection()); \
} \
HRESULT  ENVTIEName::Subscribe()  \
{ \
return (ENVTIECALL(CATIArrAttachSubscriber,ENVTIETypeLetter,ENVTIELetter)Subscribe()); \
} \
HRESULT  ENVTIEName::UnSubscribe()  \
{ \
return (ENVTIECALL(CATIArrAttachSubscriber,ENVTIETypeLetter,ENVTIELetter)UnSubscribe()); \
} \


/* Name of the TIE class */
#define class_TIE_CATIArrAttachSubscriber(classe)    TIECATIArrAttachSubscriber##classe


/* Common methods inside a TIE */
#define common_TIE_CATIArrAttachSubscriber(classe) \
 \
 \
/* Static initialization */ \
CATDefineCommonTIEMembers(CATIArrAttachSubscriber, classe) \
 \
 \
CATImplementTIEMethods(CATIArrAttachSubscriber, classe) \
CATImplementIUnknownMethodsForCATBaseUnknownTIE(CATIArrAttachSubscriber, classe, 1) \
CATImplementIDispatchMethodsForCATBaseUnknownTIE(CATIArrAttachSubscriber, classe) \
CATImplementCATBaseUnknownMethodsForTIE(CATIArrAttachSubscriber, classe) \
 \
HRESULT  TIECATIArrAttachSubscriber##classe::DoConnection()  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->DoConnection()); \
} \
HRESULT  TIECATIArrAttachSubscriber##classe::IsConnected(CATBoolean *oStatus)  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->IsConnected(oStatus)); \
} \
HRESULT  TIECATIArrAttachSubscriber##classe::RemoveConnection()  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->RemoveConnection()); \
} \
HRESULT  TIECATIArrAttachSubscriber##classe::Subscribe()  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->Subscribe()); \
} \
HRESULT  TIECATIArrAttachSubscriber##classe::UnSubscribe()  \
{ \
   return(((classe *)Tie_Method(NecessaryData.ForTIE,ptstat))->UnSubscribe()); \
} \



/* Macro used to link an implementation with an interface */
#define TIE_CATIArrAttachSubscriber(classe) \
 \
 \
declare_TIE_CATIArrAttachSubscriber(classe) \
 \
 \
CATMetaClass * __stdcall TIECATIArrAttachSubscriber##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_CATIArrAttachSubscriber,"CATIArrAttachSubscriber",CATIArrAttachSubscriber::MetaObject(),classe::MetaObject(),TIE); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_CATIArrAttachSubscriber(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIECreation(CATIArrAttachSubscriber, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicCATIArrAttachSubscriber##classe(classe::MetaObject(),CATIArrAttachSubscriber::MetaObject(),(void *)CreateTIECATIArrAttachSubscriber##classe)



/* Macro used to link an implementation with an interface */
/* This TIE is chained on the implementation object */
#define TIEchain_CATIArrAttachSubscriber(classe) \
 \
 \
declare_TIE_CATIArrAttachSubscriber(classe) \
 \
 \
CATMetaClass * __stdcall TIECATIArrAttachSubscriber##classe::MetaObject() \
{ \
   if (!meta_object) \
   { \
      meta_object=new CATMetaClass(&IID_CATIArrAttachSubscriber,"CATIArrAttachSubscriber",CATIArrAttachSubscriber::MetaObject(),classe::MetaObject(),TIEchain); \
   } \
   return(meta_object); \
} \
 \
 \
common_TIE_CATIArrAttachSubscriber(classe) \
 \
 \
/* creator function of the interface */ \
/* encapsulate the new */ \
CATImplementTIEchainCreation(CATIArrAttachSubscriber, classe) \
 \
/* to put information into the dictionary */ \
static CATFillDictionary DicCATIArrAttachSubscriber##classe(classe::MetaObject(),CATIArrAttachSubscriber::MetaObject(),(void *)CreateTIECATIArrAttachSubscriber##classe)


/* Macro to switch between BOA and TIE at build time */ 
#ifdef CATSYS_BOA_IS_TIE
#define BOA_CATIArrAttachSubscriber(classe) TIE_CATIArrAttachSubscriber(classe)
#else
#define BOA_CATIArrAttachSubscriber(classe) CATImplementBOA(CATIArrAttachSubscriber, classe)
#endif

#endif
