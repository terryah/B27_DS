#
#   Imakefile.mk for DNBManufacturingLayoutItf
#   Copyright (C) DELMIA Corp., 2005
#

BUILT_OBJECT_TYPE   = SHARED LIBRARY

INCLUDED_MODULES    = DNBManufacturingLayoutItfCPP DNBManufacturingLayoutItfProIDL DNBManufacturingLayoutItfPubIDL 

LINK_WITH    = JS0CORBA \						#System
               CD0WIN \							#ApplicationFrame
               CATProductStructureInterfaces \	#ProductStructureInterfaces
               SELECT \                         #CATIAApplicationFrame
               InfProIDL \						#InfInterfaces
               AS0STARTUP \                     #ProductStructure


