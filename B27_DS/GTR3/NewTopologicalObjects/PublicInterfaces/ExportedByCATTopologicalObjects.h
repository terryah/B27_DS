#include "CATIACGMLevel.h"
#ifdef CATIACGMR420CAA
#ifdef _STATIC_SOURCE
#define	ExportedByCATTopologicalObjects
#elif defined __CATTopologicalObjects
#define ExportedByCATTopologicalObjects DSYExport
#else
#define ExportedByCATTopologicalObjects DSYImport
#endif
#include "DSYExport.h"
#else
#ifdef _STATIC_SOURCE
#define	ExportedByCATTopologicalObjects
#elif defined _WINDOWS_SOURCE
#ifdef	__CATTopologicalObjects

// COPYRIGHT DASSAULT SYSTEMES 1999
/** @CAA2Required */
#define	ExportedByCATTopologicalObjects	__declspec(dllexport)
#else
#define	ExportedByCATTopologicalObjects	__declspec(dllimport)
#endif
#else
#define	ExportedByCATTopologicalObjects
#endif
#endif
