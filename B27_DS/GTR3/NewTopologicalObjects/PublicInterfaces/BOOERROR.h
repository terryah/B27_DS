#include "CATIACGMLevel.h"
#ifndef BOOERROR_h
#define BOOERROR_h

// COPYRIGHT DASSAULT SYSTEMES 1999
/** @CAA2Required */
/*---------------------------------------------------------------------*/
/* DON'T DIRECTLY INCLUDE THIS HEADER IN YOUR APPLICATION CODE. IT IS  */
/* REQUIRED TO BUILD CAA APPLICATIONS BUT IT MAY DISAPPEAR AT ANY TIME */
/*---------------------------------------------------------------------*/
#ifdef CATIACGMR420CAA
#ifdef _STATIC_SOURCE
#define	ExportedByBOOERROR
#elif defined(__BOOERROR)
#define ExportedByBOOERROR DSYExport
#else
#define ExportedByBOOERROR DSYImport
#endif
#include "DSYExport.h"
#else
#ifdef _STATIC_SOURCE
#define	ExportedByBOOERROR
#elif defined _WINDOWS_SOURCE
#ifdef	__BOOERROR
#define	ExportedByBOOERROR	__declspec(dllexport)
#else
#define	ExportedByBOOERROR	__declspec(dllimport)
#endif
#else
#define	ExportedByBOOERROR
#endif
#endif

#endif
