/*! Copyright Dassault Systemes 2016 */

var dscef;
if (!dscef) {
  dscef = {};
}

(function() {
  'use strict';

  // ------------------------------------------------------------------------
  // Bindings for Native Record.
  // ------------------------------------------------------------------------

  /* jshint ignore:start */
  native function __startRecordManager();
  native function __startInteractionInterpreter();
  native function __isRecording();
  native function __isReplaying();
  native function __fileExists();
  native function __capture();
  native function __replay();
  native function __handleReplayResult();
  native function __abort();
  native function __notifyInterpreterLoad();
  /* jshint ignore:end */

  /**
   * Utility bindings for WebRecord.
   * Only used when recording or replaying an ODT.
   * @namespace dscef.Record
   * @property {Object} manager
   * This will hold the RecordManager instance.
   * More details in the WebRecordTool framework and its WebRecordClient.
   * @property {Object} interpreter
   * This will hold the InteractionInterpreterNative instance.</p>
   * Details in the WebRecordTool framework and its WebRecordReplayDriver module.
   */
  dscef.Record = {
    // RecordManager.
    manager: null,
    // InteractionInterpreter
    interpreter: null,
    // WebRecordAnalyzer.
    analyzer: null,

    /**
     * Loads RecordManager.js, saves the instance in Record.manager
     * and starts recording in the current context (main world).
     * Chrome Extensions are different: they load content scripts in isolated worlds.
     * @function startManager
     * @memberof dscef.Record
     * @return {undefined}
     */
    startManager: function() {
      return __startRecordManager(); // jshint ignore:line
    },

    /**
     * Stops the recording.
     * @function stopManager
     * @memberof dscef.Record
     * @return {undefined}
     */
    stopManager: function() {
      if (!this.manager) {
        throw new Error('Cannot stop RecordManager because there is no instance.');
      }

      this.manager.stop();
      this.manager = null;
    },

    /**
     * Loads InteractionInterpreterNative.js and saves the instance in Record.interpreter.
     * @function startInterpreter
     * @memberof dscef.Record
     * @return {undefined}
     */
    startInterpreter: function() {
      return __startInteractionInterpreter(); // jshint ignore:line
    },

    /**
     * Releases Record.interpreter.
     * @function stopInterpreter
     * @memberof dscef.Record
     * @return {undefined}
     */
    stopInterpreter: function() {
      this.interpreter = null;
    },

    /**
     * Checks if native record is active, using capture mode.
     * @function isRecording
     * @memberof dscef.Record
     * @return {Boolean}
     * True if capture mode is currently enabled.
     */
    isRecording: function() {
      return __isRecording(); // jshint ignore:line
    },

    /**
     * Checks if native record is active, using replay mode.
     * @function isReplaying
     * @memberof dscef.Record
     * @return {Boolean}
     * True if replay mode is currently enabled.
     */
    isReplaying: function() {
      return __isReplaying(); // jshint ignore:line
    },

    /**
     * Checks if the file exists in the local file system.
     * @function fileExists
     * @memberof dscef.Record
     * @param {String} path
     * The absolute path to check.
     * @return {Boolean}
     * True if the file exists.
     */
    fileExists: function(path) {
      return __fileExists(path); // jshint ignore:line
    },

    /**
     * Sends data to the native client so it can be registered by
     * the native record platform.
     * @function capture
     * @memberof dscef.Record
     * @param {String} interaction
     * The record data for one interaction.
     * @return {undefined}
     */
    capture: function(interaction) {
      return __capture(interaction); // jshint ignore:line
    },

    /**
     * Parses an interaction (JSON) and uses the interpreter to replay it.
     * @param {String} interaction
     * Interaction properties as stored in the capture files (JSON).
     * @return {Promise}
     * The promise from the interpreter, if it is defined.
     * Or a rejected promise if it is undefined.
     */
    interpret: function(interaction) {
      if (!dscef.Record.interpreter) {
        var msg = 'The replay interpreter is undefined';
        console.error(msg);
        return Promise.reject(new Error(msg));
      }

      var props;
      try {
        props = JSON.parse(interaction);
      } catch (error) {
        console.error('Failed to parse interaction', interaction);
        console.error(error);
      }

      return dscef.Record.interpreter.interpret(props)
        .then(function(result) {
          return dscef.Record.handleReplayResult(result);
        })
        .catch(function(error) {
          var isError = error instanceof Error;
          var isReplayError = error.name !== undefined && error.name === 'ReplayError';
          var errorText = isError && error.stack !== undefined
            ? error.toString()
            : error;

          console.error(errorText);
          return dscef.Record.abort(
            isReplayError ? error.code : 200,
            isError ? error.message : String(error)
          );
        });
    },

    /**
     * Notifies the native record platform after or while replaying one interaction.
     * It has only one behavior for now:
     *   - Device events:
     *     It will replay a device event, like 'mousedown' or 'keydown'.
     *     The options should include the event data.
     * Abstract events are replayed directly by the interaction interpreter
     * and results should be passed to Record.handleReplayResult().
     * @function replay
     * @memberof dscef.Record
     * @param {String} type
     * The event type: only 'device' for now.
     * @param {Object} options
     * The event parameters: coordinates, button id, etc.
     * @param {Number} timestamp
     * Captured timestamp for the event, in milliseconds and relative to the test start.
     * @return {undefined}
     */
    replay: function(type, options, timestamp) {
      return dscef._internals.promisify(__replay, {
        type: type,
        options: options,
        timestamp: timestamp
      });
    },

    /**
     * Takes a replay result and allows the native replay to proceed
     * with the next interaction.
     * The current native replay implementation locks the main thread while
     * waiting for the ongoing interaction to finish.
     * @function handleReplayResult
     * @memberof dscef.Record
     * @param {*} result
     * Should be the replay result from InteractionInterpreterNative.interpret().
     * It will be considered a failure if the result is undefined.
     * Any other value is valid.
     * @return {undefined}
     */
    handleReplayResult: function(result) {
      return __handleReplayResult(result); // jshint ignore:line
    },

    /**
     * Notifies the native record platform that an error occurred and the replay has to stop.
     * @function abort
     * @memberof dscef.Record
     * @param {Number} code
     * The error code.
     * @param {String} message
     * The error description.
     * @return {undefined}
     */
    abort: function(code, message) {
      return __abort(code, message); // jshint ignore:line
    },

    /**
     * Private callback to signal the native client that the InteractionInterpreter
     * has been successfully loaded by the browser.
     * @private
     * @return {undefined}
     */
    notifyInterpreterLoad: function() {
      return __notifyInterpreterLoad(); // jshint ignore:line
    },

    /**
     * Highlights recordable and non-recordable elements.
     * @param {Element} target
     * The root element to inspect.
     * @param {Boolean} recordableOnly
     * If true, only highlight element that have a record ID.
     * @return {undefined}
     */
    inspect: function(target, recordableOnly) {
      return this.analyzer.inspect(target, recordableOnly, false);
    },

    /**
     * Removes the highlights added by this.inspect().
     * @return {undefined}
     */
    clearInspect: function() {
      return this.analyzer.clear();
    }
  };
}());

/**
 * Shared namespace for web and native platforms.
 * Record/Replay only.
 * @namespace
 */
var DSWebRecord = {};
DSWebRecord.env = {};
DSWebRecord.getenv = dscef.getEnv;
