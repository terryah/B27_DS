/* global dscef */

// This file should be executed when the DOMContentLoaded event is caught.
(function() {
  'use strict';

  var MAX_ATTEMPTS = 25;

  var intervalId = 0;
  var attempts = 0;

  function startInterpreter() {
    if (++attempts > MAX_ATTEMPTS) {
      var message =
        'Impossible to load the interaction interpreter: AmdLoader not found';
      console.error(message);
      dscef.Record.abort(200, message);
      return false;
    }

    if (define && define.amd) {
      dscef.Record.startInterpreter();

      if (intervalId !== 0) {
        clearInterval(intervalId);
      }

      return true;
    }

    return false;
  }

  // Try once immediately.
  var started = startInterpreter();

  // Then start the recurring attempts if it failed.
  if (!started) {
    intervalId = setInterval(startInterpreter, 200);
  }
})();
