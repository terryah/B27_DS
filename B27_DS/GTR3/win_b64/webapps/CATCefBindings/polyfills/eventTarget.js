/* global dscef */

(function() {
  'use strict';

  // For Chromium versions < 49, `EventTarget.prototype.addEventListener` and
  // `EventTarget.prototype.removeEventListener` only accept boolean values for the 3rd argument.
  // This results in code using the new signature and object values being converted to booleans,
  // always attaching event listeners on the capture phase instead of the bubbling phase.

  dscef._internals.addEventListener = EventTarget.prototype.addEventListener;
  dscef._internals.removeEventListener = EventTarget.prototype.removeEventListener;

  EventTarget.prototype.addEventListener = function(type, listener, optionsOrUseCapture) {
    var useCapture = false;

    if (optionsOrUseCapture && optionsOrUseCapture.capture) {
      useCapture = true;
    } else if (typeof optionsOrUseCapture === 'boolean') {
      useCapture = optionsOrUseCapture;
    }

    dscef._internals.addEventListener.call(this, type, listener, useCapture);
  };

  EventTarget.prototype.removeEventListener = function(type, listener, optionsOrUseCapture) {
    var useCapture = false;

    if (optionsOrUseCapture && optionsOrUseCapture.capture) {
      useCapture = true;
    } else if (typeof optionsOrUseCapture === 'boolean') {
      useCapture = optionsOrUseCapture;
    }

    dscef._internals.removeEventListener.call(this, type, listener, useCapture);
  };
})();
