if (typeof (window.dsCATIAV5OpAPIhandlers) === 'function') {
  console.log("V5 Integration: dsCATIAV5OpAPIhandlers already defined");
}
else {
  window.dsCATIAV5OpAPIhandlers = function () {

    'use strict';

    var usageContext = "%USAGE_CONTEXT%";

    var NotifyOnReadyInterval = null;

    var searchByUploadUrl = ""

    // Declare the handlers
    var myHandlers = {

      //=================================================================
      // OnePart is ready to get message
      //=================================================================
      ready: function (data) {

        console.log("V5 Integration: READY message received");

        if (data != undefined) {
            console.log("V5 Integration: Data:" + JSON.stringify(data));
            searchByUploadUrl = data.searchByUploadUrl;
        }
        else {
            console.log("V5 Integration: no data for ready");
        }

        // Stop event
        clearReadyEvent();

        if (usageContext==="DocChooser") {
          OpApi.sendMessage('limitActions', {
            download: false,
            open: false,
            insert: false,
            replace: false
          });
        }
        else {
          OpApi.sendMessage('limitActions', {
            download: false,
            open: true,
            insert: true,
            replace: true
          });
        }

        sendPSEventToWin('partsupply_ready', data);

      },

      //=================================================================
      // OnePart will no more receive messages from container
      //=================================================================
      over: function () {
        console.log('V5 Integration: OVER message received');

        NotifyOnReadyInterval = setInterval(notifyOnReady, 500);

        sendPSEventToWin('partsupply_over', {});

      },

      //=============================================================================================================
      // User start a insert action
      //=============================================================================================================
      insert: function (data) {
        console.log('V5 Integration: start insert: ' + data.label + ', use url: ' + data.downloadUrl);
        sendPSEventToWin('partsupply_insert', data);
      },

      //=============================================================================================================
      // User start a replace action.
      //=============================================================================================================
      replace: function (data) {
        console.log('V5 Integration: start replace: ' + data.label + ', use url: ' + data.downloadUrl);
        sendPSEventToWin('partsupply_replace', data);
      },

      //=============================================================================================================
      // User start a open action
      //=============================================================================================================
      open: function (data) {
        console.log('V5 Integration: start open: ' + data.label + ', use url: ' + data.downloadUrl);
        sendPSEventToWin('partsupply_open', data);
      },
      //=============================================================================================================
      // PreviewDisplay
      //=============================================================================================================
      previewDisplay: function (data) {
        console.log('V5 Integration: previewDisplay');
        sendPSEventToWin('partsupply_previewDisplay', data);
      },
    };

    //-------------------------------------------------------------------------------------------------------------
    function sendPSEventToWin(event, data) {
      var data2 = { 'event': event, 'content': data };
      var sjson = JSON.stringify(data2);
      if (typeof (dscef.sendString) === 'function') {
        dscef.sendString(sjson);
      }
    }

    //-------------------------------------------------------------------------------------------------------------
    function clearReadyEvent() {
      if (NotifyOnReadyInterval != undefined) {
        clearInterval(NotifyOnReadyInterval);
        NotifyOnReadyInterval = null;
      }
    }

    //-------------------------------------------------------------------------------------------------------------
    function waitForPageReady() {
      if (window.OpApi === undefined) {
        console.log('V5 Integration: wait for OpApi availability');
        setTimeout(waitForPageReady, 250);
      }
      else {
        console.log('V5 Integration: register OpApi handlers');
        OpApi.registerHandler('ready', myHandlers.ready);
        OpApi.registerHandler('over', myHandlers.over);

        if (usageContext==="DocChooser") {
          OpApi.registerHandler('previewDisplay', myHandlers.previewDisplay);
        }
        else {
          //OpApi.registerHandler('previewDisplay', myHandlers.previewDisplay);
          //OpApi.registerHandler('searchResult', myHandlers.searchResult);
          //OpApi.registerHandler('download', myHandlers.download);
          OpApi.registerHandler('insert', myHandlers.insert);
          OpApi.registerHandler('replace', myHandlers.replace);
          OpApi.registerHandler('open', myHandlers.open);
        }

        NotifyOnReadyInterval = setInterval(notifyOnReady, 500);
      }
    }

    waitForPageReady();

    //-------------------------------------------------------------------------------------------------------------
    function notifyOnReady() {
      console.log('V5 Integration: Send READY');
      OpApi.sendMessage('ready', {});
    }

  };
  window.dsCATIAV5OpAPIhandlers();
}

