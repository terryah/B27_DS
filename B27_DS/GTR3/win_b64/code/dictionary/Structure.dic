#*****************************************************************************
# CATIA Version 5 Release 1 Framework StructureModelerBase
# Copyright Dassault Systemes 1996
#*****************************************************************************
#  Abstract:
#  ---------
#    
#*****************************************************************************
#  Usage:
#  ------
#    
#*****************************************************************************
#  Historic
#  --------
#
#  Author: Alain Debuisson
#  Date  : 16/12/97
#  Goal  : Creation
#
#  Author: Frederic Crabouillet
#  Date  : 29/01/02
#  Goal  : Drawing production
#
#  Modified by: Keyvan Valafar
#  Date  : 20/01/03
#  Goal  : Adding late type for the conversion command
#
#  Modified by: Sudhi Gulur
#  Date  : 28/03/03
#  Goal  : Interim Product support.
#
#*****************************************************************************

#*****************************************************************************
# VB 
#*****************************************************************************

CATIAWorkbenchImpl			CATIAStrWorkbench                libCATStructure
CATIAWorkbenchImpl			CATIAStrComputeServices          libCATStructure
ASMPRODUCT					CATIAStrObjectFactory            libCATStructure
ASMPRODUCT					CATIAStrMembers                  libCATStructure
ASMPRODUCT					CATIAStrPlates                   libCATStructure
CATStrFactoryObj			CATIAStrObjectFactory            libCATStructure
CATStrMembersObj			CATIAStrMembers                  libCATStructure
CATStrPlatesObj				CATIAStrPlates                   libCATStructure
CATStrFoundationsObj		CATIAStrFoundations              libCATStructure
STRMember					CATIAStrMember                   libCATStructure
STRPlate					CATIAStrPlate                    libCATStructure
CATPart						CATIAStrSection                  libCATStructure
CATPart						CATIAStrAnchorPoints             libCATStructure
2DPoint						CATIAStrAnchorPoint              libCATStructure
CATStrDefExtremity			CATIABase                        libCATStructure
ASSYFEATEXT					CATIAStrCutback                  libCATStructure
ASSYFEATEXT					CATIAAssemblyFeature             libCATStructure
StructureMembers			CATITechnoObjProvider            libCATStructure 
StructurePlates				CATITechnoObjProvider            libCATStructure
StructureObjectFactory		CATITechnoObjProvider            libCATStructure
StructureFoundations  		CATITechnoObjProvider            libCATStructure
CATIAStrMemberExtremityImpl CATIAStrMemberExtremity			 libCATStructure
StrFoundationExt            CATIAStrFoundation               libCATStructure  
STRMember                   CATIAProduct                     libCATStructure
STRMember                   CATIAStrObject                   libCATStructure
STRPlate                    CATIAProduct                     libCATStructure
STRPlate                    CATIAStrObject                   libCATStructure




#*****************************************************************************
# CAA2 
#*****************************************************************************

ASMPRODUCT              CATIStrFactory                   libCATStructure  CATStrFactoryPreCond
StrFoundationExt        CATIStrFoundation                libCATStructure  
ASMPRODUCT              CATIStrProductServices           libCATStructure  
CATPart                 CATIStrSection                   libCATStructure
CATPart                 CATIStrSectionNamingDefinition   libCATStructure
STRMember               CATIStrMemberOnSupport           libCATStructure  CATStrMemberOnSupportCond
STRMember               CATIStrDimMember                 libCATStructure  CATStrDimMemberCond
STRMember               CATIStrMember                    libCATStructure  
STRMember               CATIStrInterruptedMember         libCATStructure  
STRMember               CATIStrObjectPublication         libCATStructure  
STRMember               CATIStrMemberPublication         libCATStructure  
STRMember               CATIStrMemberMigration           libCATStructure  
STRMember               CATIStrObject                    libCATStructure  
STRMember               CATISamBeam                      libCATStructure  
STRMember               CATIStrSplit                     libCATStructure  
STRMember               CATIStrMerge                     libCATStructure  
STRMember               CATIPspID                        libCATStructure
STRMember               CATIStructureDiscipline	         libCATStructure 
STRMember               CATIBuild	                     libCATStructure 
STRMember               CATIStrObjectCleaner             libCATStructure 

STRPlate                CATIPspID                        libCATStructure
STRPlate                CATIStrPlate                     libCATStructure
STRPlate                CATIStrObject                    libCATStructure
STRPlate                CATIStrObjectPublication         libCATStructure
STRPlate                CATIStrPlatePublication          libCATStructure
STRPlate                CATIStrSplit                     libCATStructure
STRPlate                CATIStrMerge                     libCATStructure
STRPlate                CATIStrPlateMigration            libCATStructure  
STRPlate	            CATIStructureDiscipline	         libCATStructure
STRPlate	            CATIBuild          	             libCATStructure
STRPlate                CATIStrObjectCleaner             libCATStructure 

GeometricalElement3D    CATIStrImport                    libCATStructure
CATStrDefExtremity      CATIStrDefExtremity              libCATStructure
CATStrMemberDefinition  CATIStrMemberDefinition          libCATStructure
CATStrPlateDefinition   CATIStrPlateDefinition           libCATStructure
CATSpecObject           CATIStrStretch                   libCATStructure  CATStrStretchCond
ThickSurface            CATIStrPlateFeature              libCATStructure
Pad                     CATIStrPlateFeature              libCATStructure
Wall                    CATIStrPlateFeature              libCATStructure
Wall_FD                 CATIStrPlateFeature              libCATStructure
CATStrBehaviourCont     CATIIconProvider                 libCATStructure
CATStrBehaviourCont     CATInit                          libCATStructure
ASMPRODUCT              CATIStrInterimProductServices    libCATStructure

#*****************************************************************************
# Structure Functional Design 
#*****************************************************************************

CATStructureFunctionalDesign  CATIFdeApplicationDictionary     libCATStructure
CATStructureFunctionalDesign  CATIPspClass                     libCATStructure
CATStructureFunctionalDesign  CATIPspApplication               libCATStructure
CATStructureFunctionalDesign  CATIPspObject                    libCATStructure

CATStrPlateSystem       CATIStrPlateSystem               libCATStructure

CATSFDExtCont           CATInit                          libCATStructure

CATStrStiffenerSystem   CATIStrStiffenerSystem           libCATStructure
CATStrStiffenerSystem   CATIFdeDictionaryClass           libCATStructure

CATSFD                  CATIPspDomainEnvironment         libCATStructure
CATSFD                  CATIPspDomainExtendable          libCATStructure

CATStrPanelSystem       CATIFdeDictionaryClass           libCATStructure
CATStrPlateSystem       CATIFdeDictionaryClass           libCATStructure

CATStrSystem            CATIStrFunSystem                 libCATStructure
CATStrSystem            CATIPspObject                    libCATStructure

ASMPRODUCT              CATIStrFunFactory                libCATStructure
ASMPRODUCT              CATIStrIntFunFactory             libCATStructure

CATStrSystem            CATIStrFunSkeletonFactory        libCATStructure
CATStrSystem            CATIStrOpenableObject            libCATStructure
CATStrSystem            CATIStrOpeningObject             libCATStructure
CATStrSystem            CATIStrOpeningFactory            libCATStructure
CATStrSystem            CATIProduct                      libCATStructure
CATStrSystem            CATIPrdActivation                libCATStructure
CATStrSystem            CATIProductContext               libCATStructure
CATStrSystem            CATIProductInSession             libCATStructure
CATStrSystem            CATIReferenceDocument            libCATStructure
CATStrSystem            CATIPrd3DVisuRepresentations     libCATStructure
CATStrSystem            CATINavigateObjectToGraph        libCATStructure
CATStrSystem            CATIPspAttribute                 libCATStructure
CATSddSystem            CATINavigateObjectToGraph        libCATStructure

CATStrSkeleton          CATIStrSkeleton                  libCATStructure
CATStrSkeleton          CATIStrPlateSystemSkeleton       libCATStructure
CATStrSkeleton          CATIStrStiffenerSystemSkeleton   libCATStructure
CATStrSkeleton          CATIPspID                        libCATStructure
CATStrSkeleton			CATIPspObject                    libCATStructure
CATStrSkeleton          CATIProduct                      libCATStructure
CATStrSkeleton          CATIPrdActivation                libCATStructure
CATStrSkeleton          CATIProductContext               libCATStructure
CATStrSkeleton          CATIProductInSession             libCATStructure
CATStrSkeleton          CATIReferenceDocument            libCATStructure
CATStrSkeleton          CATIPrd3DVisuRepresentations     libCATStructure

CATStrSkeleton_node     CATINavigModify                  libCATStructure

CATStrOpening           CATIStrOpening                   libCATStructure
CATStrOpening           CATIPspID                        libCATStructure

CATStrConOpening_node   CATINavigModify                  libCATStructure
CATStrConOpening        CATIPspObject                    libCATStructure

CATStrFunOpening        CATIPspObject                    libCATStructure
CATStrFunOpening        CATICutAndPastable               libCATStructure
CATStrFunOpening_node   CATINavigModify                  libCATStructure

CATStrFMFSkeleton_node  CATINavigModify                  libCATStructure
CATStrFMFSkeleton       CATIStrFunMoldedForm             libCATStructure

CATStrFunPlate          CATIStructureDiscipline          libCATStructure
CATStrFunInsertPlate    CATIStructureDiscipline          libCATStructure
CATStrFunStiffener      CATIStructureDiscipline          libCATStructure
CATStrFunPillar         CATIStructureDiscipline          libCATStructure
CATStrSkeleton          CATIStructureDiscipline          libCATStructure
CATStrOpening           CATIStructureDiscipline          libCATStructure
CATStrSystem            CATIStructureDiscipline          libCATStructure

#*****************************************************************************
# Interne 
#*****************************************************************************

ASMPRODUCT              CATIStrIntFactory                libCATStructure
STRPlate                CATIStrIntPlate                  libCATStructure
STRPlate_node           CATINavigModify                  libCATStructure
STRMember_node          CATINavigModify                  libCATStructure

#*****************************************************************************
# KnowledgeWare
#*****************************************************************************

STRMember               CATIInstance                     libCATStrKnowledgeDef
STRMember               CATIInstanceAttributes           libCATStrKnowledgeDef
STRMember               CATIAttributesDescription        libCATStrKnowledgeDef

STRPlate                CATIInstance                     libCATStrKnowledgeDef
STRPlate                CATIInstanceAttributes           libCATStrKnowledgeDef
STRPlate                CATIAttributesDescription        libCATStrKnowledgeDef

CATStrFunOpening        CATIInstance                     libCATStrKnowledgeDef
CATStrFunOpening        CATIInstanceAttributes           libCATStrKnowledgeDef
CATStrFunOpening        CATIAttributesDescription        libCATStrKnowledgeDef

CATStrSkeleton          CATIInstance                     libCATStrKnowledgeDef
CATStrSkeleton          CATIInstanceAttributes           libCATStrKnowledgeDef
CATStrSkeleton          CATIAttributesDescription        libCATStrKnowledgeDef

CATStrPanelSystem       CATIInstance                     libCATStrKnowledgeDef
CATStrPanelSystem       CATIInstanceAttributes           libCATStrKnowledgeDef
CATStrPanelSystem       CATIAttributesDescription        libCATStrKnowledgeDef

CATStrPlateSystem       CATIInstance                     libCATStrKnowledgeDef
CATStrPlateSystem       CATIInstanceAttributes           libCATStrKnowledgeDef
CATStrPlateSystem       CATIAttributesDescription        libCATStrKnowledgeDef

CATStrStiffenerSystem   CATIInstance                     libCATStrKnowledgeDef
CATStrStiffenerSystem   CATIInstanceAttributes           libCATStrKnowledgeDef
CATStrStiffenerSystem   CATIAttributesDescription        libCATStrKnowledgeDef

CATEStrKwePackage 	    CATIAddTypeLibrary				 libCATStrKnowledgeDef
CATEStrKwePackage		CATICreateInstance			     libCATStrKnowledgeDef
CATEStrKweAttrAccess    CATIAttributeAccess              libCATStrKnowledgeDef
CATEStrKweAttrAccess    CATICreateInstance               libCATStrKnowledgeDef

#CATESFDKwePackage 	    CATIAddTypeLibrary				 libCATStrKnowledgeDef
#CATESFDKwePackage		CATICreateInstance			     libCATStrKnowledgeDef

StrFoundationExt        CATIInstance                     libCATStrKnowledgeDef
StrFoundationExt        CATIInstanceAttributes           libCATStrKnowledgeDef
StrFoundationExt        CATIAttributesDescription        libCATStrKnowledgeDef

CATStrInterimProductExt	 CATIInstanceExtension	          libCATStrKnowledgeDef

STRPlate	            CATIComputedAttr	             libCATStrKnowledgeDef
STRMember               CATIComputedAttr	             libCATStrKnowledgeDef

#*****************************************************************************
# Part Design - are le 20/05/99
#*****************************************************************************

CATFeatCont             CATIStrPrtFeatureFactory         libCATStructure
CATFeatCont             CATIStrMemberSkeletonSUFactory		 libCATStructure
CATFeatCont             CATIStrMemberSkeletonFactory			 libCATStructure
StrSkeleton             CATIStrMemberSkeleton                  libCATStructure
StrSkeleton             CATIIcon		                 libCATStructure
StrSkeleton             CATIReplace		                 libCATStructure
StrSkeleton				CATINavigateObject               libCATStructure

CATFeatCont             CATIStrRibStartUpsFactory	     libCATStructure
CATFeatCont             CATIStrRibFactory				 libCATStructure
StructuralRib           CATIBuild                        libCATStructure
StructuralRib           CATIStrRib						 libCATStructure
StructuralRib           CATIIcon                         libCATStructure
StructuralRib           CATINavigateObject               libCATStructure
StructuralRib           CATIReplace				         libCATStructure
StructuralRib           CATIAttrBehavior    	         libCATStructure
StructuralRib           CATIShapeFeatureProperties       libCATStructure

#ifdef CATIAV5R6
CATFeatCont             CATIStrPrtCutBackFactory		 libCATStructure
StrPrtCutBack           CATIStrPrtCutBack                libCATStructure
StrPrtCutBack           LifeCycleObject                  libCATStructure
StrPrtCutBackMitered         CATIBuild                   libCATStructure
StrPrtCutBackMitered         CATIIcon                    libCATStructure
StrPrtCutBackNormalWelded    CATIBuild                   libCATStructure
StrPrtCutBackNormalWelded    CATIIcon                    libCATStructure
StrPrtCutBackNormalWelded    CATIStrPrtCutBackNormalWelded  libCATStructure

Assy_StrPrtCutBackMitered     CATIAssyFeatureImpact           libCATStructure
Assy_StrPrtCutBackMitered     CATIAssyFeatureShape            libCATStructure
Assy_StrPrtCutBackNormalWelded    CATIAssyFeatureImpact       libCATStructure
Assy_StrPrtCutBackNormalWelded    CATIAssyFeatureShape        libCATStructure

StrFunRibSweep           CATIBuild                       libCATStructure
StrFunRibSweep           CATIUpdateError                 libCATStructure
StrFunRibSweep           CATIStrRib                      libCATStructure
StrFunRibSweep           CATIIcon                        libCATStructure

CATFeatCont             CATIStrIntSectionFactory         libCATStructure
IntSection              CATIStrIntSection                libCATStructure
IntSection              CATIBuild                        libCATStructure
IntSection              CATIIcon                         libCATStructure

#### FGO July 18, 2003 : Do not implement Replace for IntSection otherwise the section of a Shape will be seen as an input in Assembly Template...
#IntSection              CATIReplace                      libCATStructure
#IntSection              CATIAttrBehavior                 libCATStructure
####

CATFeatCont                CATIStrBodySkeletonFactory        libCATStructure
CATStrBodySkeleton         CATIStrBodySkeleton               libCATStructure
CATStrBodySkeleton         CATIReplace	                     libCATStructure
CATStrBodySkeleton         CATIIcon		                     libCATStructure

CATFeatCont                CATIStrAxisFeatureFactory        libCATStructure
CATStrAxisFeature          CATIStrAxisFeature               libCATStructure
CATStrAxisFeature          CATIBuild		                libCATStructure
CATStrAxisFeature          CATIIcon		                    libCATStructure

CATStrNewAxisFeature          CATIStrAxisFeature            libCATStructure
CATStrNewAxisFeature          CATIBuild		                libCATStructure
CATStrNewAxisFeature          CATIIcon		                libCATStructure
CATStrNewAxisFeature          CATIReplace                   libCATStructure
CATStrNewAxisFeature          CATIAttrBehavior              libCATStructure


#*****************************************************************************
# System skeleton  are-2001
#*****************************************************************************
StrCutoutSkeleton			   CATIStrCutoutsManagement			libCATStructure
StrReferenceSkeleton		   CATIStrReferencesManagement		libCATStructure
StrInsertSurfaceSkeleton       CATIStrInsertSurfacesManagement	libCATStructure

#endif 


#*****************************************************************************
# Connection
#*****************************************************************************

ASMPRODUCT           CATIStrConnectionFactory                   libCATStructure
ASMPRODUCT           CATIStrConnectable                         libCATStructure
CATStrJointElementExt  CATIStrJointElement                      libCATStructure
CATStrJointElementExt  CATIAsdTechnoElmt                        libCATStructure
CATStrJointExt       CATIStrJoint                               libCATStructure
CATStrJointExt_Rep       CATIAlias                              libCATStructure
CATStrJointExt_Rep       CATINavigateObject                     libCATStructure
CATAsdJointBody      CATIStrJointBody                           libCATStructure CATStrJointBodyPreCond
CATStrJointExt_Rep_node CATINavigModify                         libCATStructure
CATStrJointElementExt_Rep_node CATINavigModify                  libCATStructure
CATStrJointElementExt_Rep CATINavigateObject                    libCATStructure

 
#*****************************************************************************
# Drawing production - FCU 
# 021705 - GSMGridsubSet and GSMGridFace removed
#*****************************************************************************
#-- Interface Generative Drafting
STRMember            CATIDftGenRequest          libCATStrDwgProduction
CATStrFunStiffener   CATIDftGenRequest          libCATStrDwgProduction
GSMGridSet			 CATIDftGenRequest			libCATStrDwgProduction
CATStrSystem		 CATIDftGenRequest			libCATStrDwgProduction
STRPlate			 CATIDftGenRequest          libCATStrDwgProduction
CATStrOpening		 CATIDftGenRequest          libCATStrDwgProduction
CATStrFunPillar		 CATIDftGenRequest			libCATStrDwgProduction
CATStrSkeleton		 CATIDftGenRequest			libCATStrDwgProduction
CATStrJointExt       CATIDftGenRequest			libCATStrDwgProduction


#-- Generative View Style
STRMember            CATIDftGenParamAccess      libCATStrDwgProduction
CATStrFunStiffener   CATIDftGenParamAccess      libCATStrDwgProduction
GSMGridSet		     CATIDftGenParamAccess	    libCATStrDwgProduction
CATStrSystem		 CATIDftGenParamAccess		libCATStrDwgProduction
STRPlate			 CATIDftGenParamAccess      libCATStrDwgProduction	
CATStrOpening		 CATIDftGenParamAccess      libCATStrDwgProduction
CATStrFunPillar		 CATIDftGenParamAccess		libCATStrDwgProduction
CATStrJointExt       CATIDftGenParamAccess		libCATStrDwgProduction


#-- Plan(s) par defaut
CATStrSystem 		 CATIDftGenView3DSelection	libCATStrDwgProduction
STRMember            CATIDftGenView3DSelection	libCATStrDwgProduction
STRPlate             CATIDftGenView3DSelection	libCATStrDwgProduction


#STRMember    CATIDftGenBox3DOperand     libCATStrDwgProduction


#*****************************************************************************
#STD to SDD Conversion KVR
#*****************************************************************************
CATStrPlateNMember          CATIStrConversion               libCATStructure


#*****************************************************************************
# FDE Integration of Structure Design Plates/Shapes/Interim Products
#
#  CATStdInterimProductExt	   CATIFdeExtensionManager	        libCATStructure
#  CATStdInterimProductExt	   CATIFdeDictionaryClass	          libCATStructure
#*****************************************************************************
CATStructureDesignExt      CATIFdeApplicationDictionary     libCATStructure
CATStructureDesignAsmExt   CATIFdeApplicationDictionary     libCATStructure
CATStructureDesignExt      CATIFdeApplicationCatalog        libCATStructure
CATStdPlateExt             CATIFdeDictionaryClass	          libCATStructure
CATStdShapeExt             CATIFdeDictionaryClass	          libCATStructure
CATStdBaseObjectExt        CATIFdeDictionaryClass	          libCATStructure
STRMember                  CATIStrExtensionManager          libCATStructure
STRPlate                   CATIStrExtensionManager          libCATStructure
CATStrInterimProductExt	   CATIPspAttribute	                libCATStructure
#CATStdPlateExt	           CATIFdeExtensionManager	        libCATStructure
#CATStdShapeExt 	         CATIFdeExtensionManager	        libCATStructure
CATStdPlateExt	           CATIPspAttribute	                libCATStructure
CATStdShapeExt 	           CATIPspAttribute	                libCATStructure

#*****************************************************************************
# Manufacturing drawing production - FCU
# MFG features, GenRequest moved to DPM framework.
#*****************************************************************************
#-- Interface Generative Drafting




#*****************************************************************************
# Drawing: objects dimensionning - FCU
#*****************************************************************************
STRMember                  CATIDimVisualization              libCATStrDwgDimensions
CATStrFunStiffener         CATIDimVisualization              libCATStrDwgDimensions
STRPlate                   CATIDimVisualization              libCATStrDwgDimensions


STRMember	                  CATIStrInterimShape	              libCATStructure
#STRPlate	                  CATIStrInterimPlate	              libCATStructure
CATStrExtComponents	        CATIStrExternalComponents	        libCATStructure

#*****************************************************************************
# CAAInterface : expose Plate and Member data - BYN
#*****************************************************************************
STRMember	                  CATIStructureMember	              libCATStrObjectImpl
STRPlate	                  CATIStructurePlate	              libCATStrObjectImpl
STRMember	                  CATIStructureObject	              libCATStrObjectImpl
STRPlate	                  CATIStructureObject	              libCATStrObjectImpl
ASMPRODUCT	                CATIStructureFactory	            libCATStructure
ASMPRODUCT	                CATIStuctureMemberFactory	        libCATStructure

#*****************************************************************************
# CCP - JPR
#*****************************************************************************
STRMember               CATICCPable               libCATStructure
STRPlate                CATICCPable               libCATStructure

#*****************************************************************************
# Configuration
#*****************************************************************************
STRMember              CATIEsuBuildNewReference  libCATStructure
STRPlate               CATIEsuBuildNewReference  libCATStructure  
CATStrFunStiffener     CATIEsuBuildNewReference  libCATStructure
CATStrFunPlate         CATIEsuBuildNewReference  libCATStructure
CATStrStiffenerSystem  CATIEsuBuildNewReference  libCATStructure
CATStrPlateSystem      CATIEsuBuildNewReference  libCATStructure
CATStrFunInsertPlate   CATIEsuBuildNewReference  libCATStructure
CATStrOpening          CATIEsuBuildNewReference  libCATStructure
CATStrFunOpening       CATIEsuBuildNewReference  libCATStructure
CATStrSkeleton         CATIEsuBuildNewReference  libCATStructure
CATStrFMFSkeleton      CATIEsuBuildNewReference  libCATStructure
CATStrFunPillar        CATIEsuBuildNewReference  libCATStructure

STRMember              CATIEV5BuildNewReference  libCATStructure
STRPlate               CATIEV5BuildNewReference  libCATStructure  
CATStrFunStiffener     CATIEV5BuildNewReference  libCATStructure
CATStrFunPlate         CATIEV5BuildNewReference  libCATStructure
CATStrStiffenerSystem  CATIEV5BuildNewReference  libCATStructure
CATStrPlateSystem      CATIEV5BuildNewReference  libCATStructure
CATStrFunInsertPlate   CATIEV5BuildNewReference  libCATStructure
CATStrOpening          CATIEV5BuildNewReference  libCATStructure
CATStrFunOpening       CATIEV5BuildNewReference  libCATStructure
CATStrSkeleton         CATIEV5BuildNewReference  libCATStructure
CATStrFMFSkeleton      CATIEV5BuildNewReference  libCATStructure
CATStrFunPillar        CATIEV5BuildNewReference  libCATStructure

#*****************************************************************************
# ATS - KNY
#*****************************************************************************
STRMember               CATIPspObject              libCATStructure
STRPlate                CATIPspObject              libCATStructure

#*****************************************************************************
# For Automation -RDW
#*****************************************************************************
STRMember  CATIAStrFeatureFactory	 libCATStructure
STRPlate   CATIAStrFeatureFactory	 libCATStructure
CATStrNibblingFeature	CATIAStrNibblingFeature	libCATStructure
CATStrNibblingContextualFeature	CATIAStrNibblingFeature	libCATStructure
CATStrNibblingBOFeature	CATIAStrNibblingFeature	libCATStructure
CATStrCutoutFeature			CATIAStrCutoutFeature			 libCATStructure

# THE CAA2 WIZARDS HAS INSERTED CODE AT THE BOTTOM OF THIS FILE
# YOU MAY HAVE TO RELOCATE THIS SOURCE IF COMPILATION FAILS
# DO NOT EDIT :: THE CAA2 WIZARDS WILL ADD CODE HERE

