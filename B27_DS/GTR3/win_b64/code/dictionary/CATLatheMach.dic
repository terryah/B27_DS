# Feature pour operations Lathe
# ==============================
LatheFeature                       CATIMfgFeature                   libCATLatheMach
LatheFeature                       CATIMfgMachiningAxisSystemMgt    libCATLatheMach
MfgLatheLongRoughFeature           CATIMfgFeature                   libCATLatheMach
MfgLatheGroovingFeature            CATIMfgFeature                   libCATLatheMach
MfgLatheRecessingFeature           CATIMfgFeature                   libCATLatheMach
MfgLatheProfileFinishFeature       CATIMfgFeature                   libCATLatheMach
MfgLatheGrooveFinishFeature        CATIMfgFeature                   libCATLatheMach
MfgLatheThreadingFeature           CATIMfgFeature                   libCATLatheMach
MfgLatheRampingRecessingFeature    CATIMfgFeature                   libCATLatheMach
MfgLatheRampingRoughingFeature     CATIMfgFeature                   libCATLatheMach
MfgLatheRampingRoughingFeature     CATILatheFeature                 libCATLatheMach
MfgLatheSequentialFeature          CATIMfgFeature                   libCATLatheMach
MfgLatheSequentialGlobalGeomFeature CATIMfgFeature                  libCATLatheMach

# View by geometry for DPM Machining Process Planner
# ==============================
LatheFeature                       CATIMfgUMFRepresentation         libCATLatheInProcessModel

# Parts
# ==============================
MfgLatheGrooveFinishFeature        CATIMfgParts                     libCATLatheMach
MfgLatheGroovingFeature            CATIMfgParts                     libCATLatheMach
MfgLatheLongRoughFeature           CATIMfgParts                     libCATLatheMach
MfgLatheProfileFinishFeature       CATIMfgParts                     libCATLatheMach
MfgLatheRampingRecessingFeature    CATIMfgParts                     libCATLatheMach
MfgLatheRampingRoughingFeature     CATIMfgParts                     libCATLatheMach
MfgLatheRecessingFeature           CATIMfgParts                     libCATLatheMach
MfgLatheSequentialFollowFeature    CATIMfgParts                     libCATLatheMach
MfgLatheSequentialGoStdFeature     CATIMfgParts                     libCATLatheMach
MfgLatheSequentialIndirvFeature    CATIMfgParts                     libCATLatheMach
MfgLatheThreadingFeature           CATIMfgParts                     libCATLatheMach

# Feature Geometry
# ==============================
LatheFeature                       CATIMfgFeatureGeometry           libCATLatheMach
MfgLatheGrooveFinishFeature        CATIMfgFeatureGeometry           libCATLatheMach 
MfgLatheProfileFinishFeature       CATIMfgFeatureGeometry           libCATLatheMach 
MfgLatheRecessingFeature           CATIMfgFeatureGeometry           libCATLatheMach 
MfgLatheGroovingFeature            CATIMfgFeatureGeometry           libCATLatheMach 
MfgLatheLongRoughFeature           CATIMfgFeatureGeometry           libCATLatheMach 
MfgLatheThreadingFeature           CATIMfgFeatureGeometry           libCATLatheMach 
MfgLatheSequentialFeature          CATIMfgFeatureGeometry           libCATLatheMach
MfgLatheSequentialGlobalGeomFeature CATIMfgFeatureGeometry          libCATLatheMach 
MfgLatheRampingRoughingFeature     CATIMfgFeatureGeometry           libCATLatheMach   
MfgLatheRampingRecessingFeature    CATIMfgFeatureGeometry           libCATLatheMach

# Lathe Feature
# ====================
LatheFeature                       CATILatheFeature                 libCATLatheMach
MfgLatheLongRoughFeature           CATILatheFeature                 libCATLatheMach
MfgLatheGroovingFeature            CATILatheFeature                 libCATLatheMach
MfgLatheProfileFinishFeature       CATILatheFeature                 libCATLatheMach
MfgLatheRecessingFeature           CATILatheFeature                 libCATLatheMach
MfgLatheGrooveFinishFeature        CATILatheFeature                 libCATLatheMach
MfgLatheThreadingFeature           CATILatheFeature                 libCATLatheMach
MfgLatheSequentialFeature          CATILatheFeature                 libCATLatheMach
MfgLatheRampingRecessingFeature    CATILatheFeature                 libCATLatheMach
MfgLatheSequentialGlobalGeomFeature CATILatheFeature                libCATLatheMach

# Base
# ====================
LatheFeature                       CATIMfgBase                      libCATLatheMach
MfgLatheLongRoughFeature           CATIMfgBase                      libCATLatheMach
MfgLatheGroovingFeature            CATIMfgBase                      libCATLatheMach
MfgLatheRecessingFeature           CATIMfgBase                      libCATLatheMach
MfgLatheProfileFinishFeature       CATIMfgBase                      libCATLatheMach
MfgLatheGrooveFinishFeature        CATIMfgBase                      libCATLatheMach
MfgLatheThreadingFeature           CATIMfgBase                      libCATLatheMach
MfgLatheRampingRecessingFeature    CATIMfgBase                      libCATLatheMach
MfgLatheRampingRoughingFeature     CATIMfgBase                      libCATLatheMach
MfgLatheSequentialFeature          CATIMfgBase                      libCATLatheMach
MfgLatheSequentialGlobalGeomFeature  CATIMfgBase                    libCATLatheMach

# Part
# ====================
MfgLatheLongRoughFeature           CATILathePartBody                libCATLatheMach
MfgLatheGroovingFeature            CATILathePartBody                libCATLatheMach
MfgLatheRecessingFeature           CATILathePartBody                libCATLatheMach
MfgLatheThreadingFeature           CATILathePartBody                libCATLatheMach
MfgLatheProfileFinishFeature       CATILathePartBody                libCATLatheMach
MfgLatheGrooveFinishFeature        CATILathePartBody                libCATLatheMach
MfgLatheRampingRoughingFeature     CATILathePartBody                libCATLatheMach
MfgLatheRampingRecessingFeature    CATILathePartBody                libCATLatheMach
MfgLatheSequentialGoStdFeature     CATILathePartBody                libCATLatheMach
MfgLatheSequentialIndirvFeature    CATILathePartBody                libCATLatheMach
MfgLatheSequentialFollowFeature    CATILathePartBody                libCATLatheMach

# Machining Feature
# ====================
LatheFeature                       CATIMfgMachiningFeature          libCATLatheMach
MfgLatheLongRoughFeature           CATIMfgMachiningFeature          libCATLatheMach
MfgLatheGroovingFeature            CATIMfgMachiningFeature          libCATLatheMach
MfgLatheRecessingFeature           CATIMfgMachiningFeature          libCATLatheMach
MfgLatheProfileFinishFeature       CATIMfgMachiningFeature          libCATLatheMach
MfgLatheGrooveFinishFeature        CATIMfgMachiningFeature          libCATLatheMach
MfgLatheThreadingFeature           CATIMfgMachiningFeature          libCATLatheMach
MfgLatheRampingRecessingFeature    CATIMfgMachiningFeature          libCATLatheMach
MfgLatheRampingRoughingFeature     CATIMfgMachiningFeature          libCATLatheMach
MfgLatheSequentialFeature          CATIMfgMachiningFeature          libCATLatheMach
MfgLatheSequentialGlobalGeomFeature CATIMfgMachiningFeature         libCATLatheMach

# Stock
# ====================
MfgLatheLongRoughFeature           CATILatheStockBody               libCATLatheMach
MfgLatheGroovingFeature            CATILatheStockBody               libCATLatheMach
MfgLatheRecessingFeature           CATILatheStockBody               libCATLatheMach
MfgLatheRampingRoughingFeature     CATILatheStockBody               libCATLatheMach
MfgLatheRampingRecessingFeature    CATILatheStockBody               libCATLatheMach

# Drives
# ====================
MfgLatheGroovingFeature            CATIMfgDrives                        libCATLatheMach
MfgLatheLongRoughFeature           CATIMfgDrives                        libCATLatheMach
MfgLatheRampingRecessingFeature    CATIMfgDrives                        libCATLatheMach
MfgLatheRampingRoughingFeature     CATIMfgDrives                        libCATLatheMach
MfgLatheRecessingFeature           CATIMfgDrives                        libCATLatheMach
MfgMachinableLatheStockGeomFeature CATIMfgDrives                        libCATLatheMach

# Agregate
# ====================
MfgLatheSequentialDeltaFeature     CATIMfgAgregate                        libCATLatheMach
MfgLatheSequentialOperation        CATIMfgAgregate                        libCATLatheMach
MfgMachinableLatheStockGeomFeature CATIMfgAgregate                        libCATLatheMach
MfgLatheSequentialFollowFeature    CATIMfgAgregate                        libCATLatheMach

# Geometry Agregate
# ====================
MfgLatheSequentialDeltaFeature     CATIMfgGeometryAgregate                libCATLatheMach
MfgMachinableLatheStockGeomFeature CATIMfgGeometryAgregate                libCATLatheMach
MfgLatheSequentialFollowFeature    CATIMfgGeometryAgregate                libCATLatheMach

# Start element
# ====================
MfgLatheProfileFinishFeature       CATILatheStartElement            libCATLatheMach
MfgLatheGrooveFinishFeature        CATILatheStartElement            libCATLatheMach
MfgLatheThreadingFeature           CATILatheStartElement            libCATLatheMach
MfgLatheSequentialDeltaFeature     CATILatheStartElement            libCATLatheMach
MfgLatheSequentialIndirvFeature    CATILatheStartElement            libCATLatheMach

# End element
# ====================
MfgLatheLongRoughFeature           CATILatheEndElement              libCATLatheMach
MfgLatheGroovingFeature            CATILatheEndElement              libCATLatheMach
MfgLatheProfileFinishFeature       CATILatheEndElement              libCATLatheMach
MfgLatheGrooveFinishFeature        CATILatheEndElement              libCATLatheMach
MfgLatheThreadingFeature           CATILatheEndElement              libCATLatheMach
MfgLatheRampingRoughingFeature     CATILatheEndElement              libCATLatheMach
MfgLatheSequentialGoStdFeature     CATILatheEndElement              libCATLatheMach

# Relimiting elements
# ====================
MfgLatheLongRoughFeature           CATIMfgRelimitingElements        libCATLatheMach
MfgLatheRampingRoughingFeature     CATIMfgRelimitingElements        libCATLatheMach
MfgLatheSequentialDeltaFeature     CATIMfgRelimitingElements        libCATLatheMach
MfgLatheSequentialGoStdFeature     CATIMfgRelimitingElements        libCATLatheMach
MfgLatheSequentialIndirvFeature    CATIMfgRelimitingElements        libCATLatheMach

# First Relimiting element
# ====================
MfgLatheGrooveFinishFeature        CATIMfgFirstRelimitingElement        libCATLatheMach
MfgLatheProfileFinishFeature       CATIMfgFirstRelimitingElement        libCATLatheMach
MfgLatheSequentialDeltaFeature     CATIMfgFirstRelimitingElement        libCATLatheMach
MfgLatheSequentialIndirvFeature    CATIMfgFirstRelimitingElement        libCATLatheMach
MfgLatheThreadingFeature           CATIMfgFirstRelimitingElement        libCATLatheMach

# Second Relimiting element
# ====================
MfgLatheGrooveFinishFeature        CATIMfgSecondRelimitingElement       libCATLatheMach
MfgLatheLongRoughFeature           CATIMfgSecondRelimitingElement       libCATLatheMach
MfgLatheProfileFinishFeature       CATIMfgSecondRelimitingElement       libCATLatheMach
MfgLatheRampingRoughingFeature     CATIMfgSecondRelimitingElement       libCATLatheMach
MfgLatheSequentialGoStdFeature     CATIMfgSecondRelimitingElement       libCATLatheMach
MfgLatheThreadingFeature           CATIMfgSecondRelimitingElement       libCATLatheMach


# MfgLatheOperation
# =====================
MfgLatheOperation                  CATIMfgMachiningAxisSystemMgt    libCATLatheMach

# Activity Spindle Mode
# =====================
MfgLatheOperation                  CATIMfgActivitySpindleMode       libCATLatheMach

# Mfg Activity
# ============
MfgLatheOperation                  CATIMfgActivity                  libCATLatheMach
MfgLatheThreading                  CATIMfgActivity                  libCATLatheMach 
MfgLatheSequentialOperation        CATIMfgActivity                  libCATLatheMach

# Activity Initialization
# =======================
MfgLatheOperation                  CATIMfgActivityInitialization    libCATLatheMach
MfgLatheRampingRecessing           CATIMfgActivityInitialization    libCATLatheMach

# Tool Activity
# ============
MfgLatheGrooveFinish               CATIMfgToolActivity              libCATLatheMach
MfgLatheGrooving                   CATIMfgToolActivity              libCATLatheMach
MfgLatheLongRough                  CATIMfgToolActivity              libCATLatheMach
MfgLatheProfileFinish              CATIMfgToolActivity              libCATLatheMach
MfgLatheRecessing                  CATIMfgToolActivity              libCATLatheMach
MfgLatheThreading                  CATIMfgToolActivity              libCATLatheMach
MfgLatheSequentialOperation        CATIMfgToolActivity              libCATLatheMach
MfgLatheRampingRoughing            CATIMfgToolActivity              libCATLatheMach
MfgLatheRampingRecessing           CATIMfgToolActivity              libCATLatheMach

# Sequential motions
# ========
MfgLatheSequentialOperation        CATIMfgLatheSeqMotionsMgt        libCATLatheMach

MfgLatheSequentialOperation        CATIMfgSeqMotionsMgt             libCATLatheMach
MfgLatheSequentialOperation        CATIMfgSequentialMotions         libCATLatheMach
MfgLatheSequentialOperation        CATIMfgActivityGeometry          libCATLatheToolPath  
MfgSeqMotionLatheBase              CATIMfgSeqMotionGeometry         libCATLatheMach

# Sequential Motion
# =================
MfgSeqMotionLatheBase              CATIMfgLatheSeqMotion            libCATLatheMach
MfgSeqMotionLatheGoStd             CATIMfgLatheSeqMotion            libCATLatheMach
MfgSeqMotionLatheDelta             CATIMfgLatheSeqMotion            libCATLatheMach
MfgSeqMotionLatheFollow            CATIMfgLatheSeqMotion            libCATLatheMach
MfgSeqMotionLatheIndirv            CATIMfgLatheSeqMotion            libCATLatheMach
MfgSeqMotionPPWord                 CATIMfgLatheSeqMotion            libCATLatheMach

# Tool Insert Mgt
# ===============
MfgLatheGrooveFinish               CATIMfgToolInsertMgt             libCATLatheMach
MfgLatheGrooving                   CATIMfgToolInsertMgt             libCATLatheMach
MfgLatheLongRough                  CATIMfgToolInsertMgt             libCATLatheMach
MfgLatheProfileFinish              CATIMfgToolInsertMgt             libCATLatheMach
MfgLatheRecessing                  CATIMfgToolInsertMgt             libCATLatheMach
MfgLatheThreading                  CATIMfgToolInsertMgt             libCATLatheMach
MfgLatheSequentialOperation        CATIMfgToolInsertMgt             libCATLatheMach
MfgLatheRampingRoughing            CATIMfgToolInsertMgt             libCATLatheMach
MfgLatheRampingRecessing           CATIMfgToolInsertMgt             libCATLatheMach
ToolChangeLathe                    CATIMfgToolInsertMgt             libCATLatheMach

# ToolPath
# ========
MfgLatheLongRough                  CATIMfgComputeToolPath           libCATLatheMach
MfgLatheGrooving                   CATIMfgComputeToolPath           libCATLatheMach
MfgLatheRecessing                  CATIMfgComputeToolPath           libCATLatheMach
MfgLatheProfileFinish              CATIMfgComputeToolPath           libCATLatheMach
MfgLatheGrooveFinish               CATIMfgComputeToolPath           libCATLatheMach
MfgLatheThreading                  CATIMfgComputeToolPath           libCATLatheMach
MfgLatheSequentialOperation        CATIMfgComputeToolPath           libCATLatheMach
MfgLatheRampingRoughing            CATIMfgComputeToolPath           libCATLatheMach
MfgLatheRampingRecessing           CATIMfgComputeToolPath           libCATLatheMach

# V4 To V5 Parameters
# ===================
MfgLatheOperation                  CATIMfgV4ToV5Parameters          libCATLatheMach
MfgLatheThreading                  CATIMfgV4ToV5Parameters          libCATLatheMach

# Lathe Operation
# ===============
MfgLatheOperation                  CATIMfgLatheOperation            libCATLatheMach
MfgLatheSequentialOperation        CATIMfgLatheOperation            libCATLatheMach
MfgLatheThreading                  CATIMfgLatheOperation            libCATLatheMach

# Life Cycle Object
# ===============
MfgLatheOperation                  LifeCycleObject                  libCATLatheMach
MfgSeqMotionLatheBase              LifeCycleObject                  libCATLatheMach

# Stock calcule
# ====================
MfgMachinableLatheStockGeomFeature CATILatheComputedStockBody       libCATLatheMach

# Specific
# ====================

MfgLatheOperation                  CATIMfgInProcessMachOp           libCATLatheInProcessModel  CATMfgIsNotLatheThreading
MfgLatheSequentialOperation        CATIMfgInProcessMachOp           libCATLatheInProcessModel  
MfgLatheOperation                  CATIMfgIPMFromTool               libCATLatheInProcessModel

MfgLatheThreading                  CATIMfgLatheThreadOperation      libCATLatheMach

MfgLatheLongRoughFeature           CATIMfgLatheLongRoughFeature     libCATLatheMach

MfgLatheGroovingFeature            CATIMfgLatheGroovingFeature      libCATLatheMach

MfgLatheRecessingFeature           CATIMfgLatheRecessingFeature     libCATLatheMach

MfgLatheProfileFinishFeature       CATIMfgLatheProfileFinishFeature libCATLatheMach

MfgLatheGrooveFinishFeature        CATIMfgLatheGrooveFinishFeature  libCATLatheMach

MfgLatheThreadingFeature           CATIMfgLatheThreadingFeature     libCATLatheMach

MfgLatheSequentialFeature          CATIMfgLatheSequentialFeature    libCATLatheMach
MfgLatheSequentialFeature          CATIMfgDuplicate                 libCATLatheMach

MfgLatheRampingRecessingFeature    CATIMfgLatheRampingRecessingFeature libCATLatheMach

MfgLatheRampingRoughingFeature     CATIMfgLatheRampingRoughingFeature  libCATLatheMach

MfgLatheSequentialGlobalGeomFeature CATIMfgLatheSequentialGlobalGeomFeature libCATLatheMach

LatheFeature  CATIMfgLatheLocalPlaneElement  libCATLatheMach

MfgLatheOperation                  CATIMfgActivityUpgradeParameters  libCATLatheMach
MfgLatheSequentialOperation        CATIMfgLatheSeqOpMgmt             libCATLatheMach
