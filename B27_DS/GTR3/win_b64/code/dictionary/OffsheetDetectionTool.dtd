<?xml version="1.0" encoding="UTF-8"?>

<!-- @version: -->

<!-- Electrical Cable Database Batch Importer DTD - version 1.0 -->
<!--     
 This is the DTD file that describes the format to be used for the
 XML file that is required input into the "OffsheetDetectionTool"  
 batch application.  This application is used to dump information 
 related to offsheet connectors against schematic diagrams under
 a PRC.
                                                                   
 Explanation of format:                                                 
                                                                        
 Every Offsheet Detection Tool XML file starts and ends with exactly one     
 OffsheetDetectionTool tag.  The OffsheetDetectionTool tag         
 contains a LoginInfo section, one PRCInfo section, and an   
 optional ReportDef section.                                            
                                                                          
 The LoginInfo section contains the information necessary to log in to  
 the ENOVIA database and consists of LCAUser, LCAPwd, LCAHost, LCAPort,    
 and LCARole tags to allow you to provide the user ID, password,        
 host (or server) and role desired for log in.  The password should be  
 encrypted by implementing the "CATIUExitCrypt" interface.  See the     
 CAA documentation for further information.  If you don't implement     
 this interface, the passed in password is assumed to be encoded in an     
 algorithm similar to the one used by the PLM Batch architecture.       
 Therefore, using one of the PLM Batch applications to connect to ENOVIA 
 and saving the resulting parameter file will yield an encoded password 
 that you can then pass in to this application.                         
 One occurrence of the LoginInfo section is required, containing        
 exactly one occurrence of each of the LCAUser, LCAPwd, LCARole, LCAPort,  
 and LCAHost tags.  Here is an example:   
                               
 	<LoginInfo>	    
 		<LCAUser>LogonID</LCAUser>
 		<LCAPwd>EncryptedPassword</LCAPwd>
 		<LCARole>RoleX</LCARole>
 		<LCAHost>HostName</LCAHost>
 		<LCAPort>9999</LCAPort>		
 	</LoginInfo>
                                                                        
 There must be one PRCInfo section in the XML file.  This    
 is a PRC-based application and the PRCInfo defines the PRC to  
 be processed (and also which work packages to process within them).  
 
 The PRCInfo section consists of a PRCName element followed by zero or 
 more DocumentIDFilter elements.  A  DocumentIDFilter element is simply 
 a text string that describes one or more document IDs that you would 
 like processed.  The text string can contain wildcards.  This program 
 is hardcoded to report on the  latest document revision for the document 
 ID(s) specified (i.e., the last revision  by creation date).  
 
 The DocumentIDFilter element itself is totally optional.  The default behavior is:
 
	DocumentIDFilter - "*" (i.e., all document IDs will be processed)
 
 Here are an example of a PRCInfo section:
 
 	 <PRCInfo>
 		<PRCName>SamplePRC1</PRCName>
       <DocumentIDFilter>*ELD1*</DocumentIDFilter>
       <DocumentIDFilter>*ELD2*</DocumentIDFilter>
       <DocumentIDFilter>*ELD3*</DocumentIDFilter>
	 </PRCInfo>
                                                                         
 The "ReportDef" element should occur zero or one time.  Its purpose is
 to specify a location and a file name for an output report that will
 be generated for this batch run.  Only text format is supported for
 this report.
 
 The ReportDef is defined by these attributes - DirectoryPath and FileName. 
 Even though the ReportDef element is optional, if you do 
 include one, then you must specify these attributes.  The FileName 
 should not include the file extension.  
 Here is a sample:
 
     <ReportDef DirectoryPath="D:\MySampleDirectory" 
                FileName="SampleFileName" />

 This statement would lead to an output report being generated in 
 "D:\MySampleDirectory\SampleFileName.txt".  
 
 The output report will contain a dump of offsheet connector related
 information for all of the cables in each of the diagrams specified.  If you 
 don't include a ReportDef element in your XML file (or this application 
 doesn't get to successfully parse the XML file), then this application 
 will write out all of its information to STDOUT and STDERR.                          
-->

<!ELEMENT OffsheetDetectionTool (LoginInfo,PRCInfo,ReportDef?)>

<!ELEMENT LoginInfo (LCAUser,LCAPwd,LCARole,LCAHost,LCAPort)>
<!ELEMENT LCAUser (#PCDATA)>
<!ELEMENT LCAPwd  (#PCDATA)>
<!ELEMENT LCARole (#PCDATA)>
<!ELEMENT LCAHost (#PCDATA)>
<!ELEMENT LCAPort (#PCDATA)>

<!ELEMENT PRCInfo (PRCName,DocumentIDFilter*)>
<!ELEMENT PRCName (#PCDATA)>

<!ELEMENT DocumentIDFilter (#PCDATA)>

<!ELEMENT ReportDef EMPTY>
<!ATTLIST ReportDef DirectoryPath CDATA #IMPLIED>
<!ATTLIST ReportDef FileName CDATA #IMPLIED>
<!ATTLIST ReportDef Format (Text) "Text">
