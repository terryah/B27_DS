#=====================================================================================
#                                     CNEXT
#                          COPYRIGHT DASSAULT SYSTEMES 2000
#-------------------------------------------------------------------------------------
# FILENAME    :    CATAnnotationModeler.dic
# LOCATION    :    CATAnnotationModeler
# AUTHOR      :    PPD
# DATE        :    13.11.2000
#-------------------------------------------------------------------------------------
# DESCRIPTION :    Dictionnary for annotations (dimensions ...) in  fwk CATAnnotationModeler
#-------------------------------------------------------------------------------------
# COMMENTS    :
#-------------------------------------------------------------------------------------
# MODIFICATIONS     user  date        purpose
#    HISTORY        ----  ----        -------
#      01           jbb   23.02.2001  Migration layers.
#=====================================================================================

# Container V4

V4AnndCont          CATIAnnotationFactory           libCATAnnotationModeler
V4AnndCont          CATIAnnotationStandard          libCATAnnotationModeler
V4AnndCont          CATIDrwMappingTable             libCATAnnotationModeler

# Standard

DrwStandard         CATIDftStandard   	            libCATAnnotationModeler
DrwStandard         CATIDrwStandard                 libCATAnnotationModeler
DrwStandard         CATIDftAnnDataFromStd           libCATAnnotationModeler
DrwAnnotation       CATIDftElementWithStandard      libCATAnnotationModeler
DrwAreaFill         CATIDftElementWithStandard      libCATAnnotationModeler
DrwArrow            CATIDftElementWithStandard      libCATAnnotationModeler
DrwAxisLine         CATIDftElementWithStandard      libCATAnnotationModeler
DrwDetail           CATIDftElementWithStandard      libCATAnnotationModeler
DrwCenterLine       CATIDftElementWithStandard      libCATAnnotationModeler
ConstraintDYS       CATIDftElementWithStandard      libCATAnnotationModeler
DrwLeader           CATIDftElementWithStandard      libCATAnnotationModeler
SketchElement       CATIDftElementWithStandard      libCATAnnotationModeler
Sheet               CATIDftElementWithStandard      libCATAnnotationModeler
Sheet2DL	CATIDftElementWithStandard	libCATAnnotationModeler
View                CATIDftElementWithStandard	  libCATAnnotationModeler
View2DL	CATIDftElementWithStandard	libCATAnnotationModeler
CATDrwCont          CATIDftElementWithStandard	  libCATAnnotationModeler
CAT2DLCont          CATIDftElementWithStandard	  libCATAnnotationModeler
DrwAnnFrame         CATIDftElementWithStandard	    libCATAnnotationModeler
DrwDrawing          CATIDftElementWithStandard	  libCATAnnotationModeler
Layout2DL	CATIDftElementWithStandard	libCATAnnotationModeler
AnnBOM	CATIDftElementWithStandard	libCATAnnotationModeler
DrwProjectionCallout    CATIDftElementWithStandard	  libCATAnnotationModeler
DrwSectionCallout    CATIDftElementWithStandard	  libCATAnnotationModeler
DrwDetailCallout    CATIDftElementWithStandard	  libCATAnnotationModeler
Section2DL          CATIDftElementWithStandard    libCATAnnotationModeler

# Styles/Defauts
DrwStandard			CATIDftStyleManager         libCATAnnotationModeler
Sheet				CATIDftElementWithStyle     libCATAnnotationModeler
Sheet2DL	CATIDftElementWithStyle	libCATAnnotationModeler
DrwDatumTarget      CATIDftElementWithStyle     libCATAnnotationModeler
DrwDatumFeature     CATIDftElementWithStyle     libCATAnnotationModeler
DrwDimension		CATIDftElementWithStyle     libCATAnnotationModeler
DrwRough			CATIDftElementWithStyle     libCATAnnotationModeler
DrwCoordDimension	CATIDftElementWithStyle		libCATAnnotationModeler
DrwBalloon			CATIDftElementWithStyle		libCATAnnotationModeler
DrwGDT				CATIDftElementWithStyle		libCATAnnotationModeler

CATDftStyle          CATIDftStyle                 libCATAnnotationModeler
CATDftStyle          CATIVisProperties            libCATAnnotationModeler
CATDftStyle          CATIDrwProperties            libCATAnnotationModeler
CATDftStyle          CATIDrwTextProperties        libCATAnnotationModeler
CATDftStyle          CATIDrwDimensionProperties   libCATAnnotationModeler

# V4 Elements

CAT_98_1            CATIV4Text                      libCATAnnotationModeler
CAT_98_1            CATINewDuplicate                libCATAnnotationModeler
CAT_98_2            CATIV4Dimension                 libCATAnnotationModeler
CAT_98_2            CATINewDuplicate                libCATAnnotationModeler
CAT_98_3            CATIV4DimensionSystem           libCATAnnotationModeler
CAT_98_3            CATINewDuplicate                libCATAnnotationModeler

# Annotation

DrwAnnotation           CATIDrwAnnotation                 libCATAnnotationModeler

DrwAnnotation           CATIMove2D                        libCATAnnotationModeler
DrwAnnotation           CATISpecObject                    libCATAnnotationModeler
DrwAnnotation       CATIAlias                       libCATAnnotationModeler
DrwAnnotation       LifeCycleObject                 libCATAnnotationModeler
DrwAnnotation           CATIDftEquivMatrixForExplode      libCATAnnotationModeler
DrwAnnotation       CATI2DSubstituteColor	          libCATAnnotationModeler
DrwAnnotation       CATIParmPublisher               libCATAnnotationModeler
DrwAnnotation       CATIIsolate                     libCATAnnotationModeler
DrwAnnotation       CATIAnnProperties               libCATAnnotationModeler

DrwAnnotationComponent  CATIDrwAnnotation               libCATAnnotationModeler
DrwAnnotationComponent  CATIDrwProperties               libCATAnnotationModeler
DrwAnnotationComponent  CATISpecObject                  libCATAnnotationModeler
DrwAnnotationComponent  CATIDrwAnnotationComponent      libCATAnnotationModeler
DrwAnnotationComponent  CATIDrwEltWithLeader            libCATAnnotationModeler
DrwAnnotationComponent  CATIDrwElement                  libCATAnnotationModeler
DrwAnnotationComponent  LifeCycleObject                 libCATAnnotationModeler
DrwAnnotationComponent  CATIDrwTextProperties           libCATAnnotationModeler
DrwAnnotationComponent  CATIMove2D                      libCATAnnotationModeler
DrwAnnotationComponent  CATIDftMirroringGroupMaster     libCATAnnotationModeler
DrwAnnotationComponent  CATIDftMirroringAxis            libCATAnnotationModeler
DrwAnnotationComponent  CATIExternalLinkValuate         libCATAnnotationModeler
DrwAnnotationComponent  CATIActivate                    libCATAnnotationModeler
DrwAnnotationComponent  CATIActivateInternal            libCATAnnotationModeler
DrwAnnotationComponent  CATIDrwSubscriber               libCATAnnotationModeler
DrwAnnotationComponent  CATIDftEquivMatrixForExplode    libCATAnnotationModeler
DrwAnnotationComponent  CATIAnnComponentFormattedValues libCATAnnotationModeler
DrwAnnotationComponent  CATIAnnIsolate                  libCATAnnotationModeler
DrwAnnotationComponent  CATIDftViewPositioningObserver  libCATAnnotationModeler

DrwAnnFrame         CATIDrwAnnFrame                 libCATAnnotationModeler

# Dimension

DrwDimension        CATIDrwDimDimension             libCATAnnotationModeler
DrwDimension        CATIDrwElement                  libCATAnnotationModeler
DrwDimension        CATIDimWDimension               libCATAnnotationModeler
DrwDimension        CATISpecObject                  libCATAnnotationModeler
DrwDimension        LifeCycleObject                 libCATAnnotationModeler
DrwDimension        CATIDrwTextProperties           libCATAnnotationModeler
DrwDimension        CATICst                         libCATAnnotationModeler
DrwDimension        CATIDrwDimDimensionMdl          libCATAnnotationModeler
DrwDimension        CATIDrwDimDimensionMdlLocal     libCATAnnotationModeler
DrwDimension        CATIParmSelector                libCATAnnotationModeler
DrwDimension        CATIDftDimCumulate              libCATAnnotationModeler
DrwDimension        CATIDftDimChamfer               libCATAnnotationModeler
DrwDimension        CATIDrwProperties               libCATAnnotationModeler
DrwDimension        CATIDftMirroringAxis            libCATAnnotationModeler
DrwDimension        CATIIsolate                     libCATAnnotationModeler
DrwDimension		CATIDrwSubscriber			 libCATAnnotationModeler
DrwDimension		CATIInstance			 libCATAnnotationModeler
DrwDimension		CATIInstanceAttributes			 libCATAnnotationModeler
DrwDimension		CATIMove2D			 libCATAnnotationModeler
DrwDimension		CATIParmPublisher			 libCATAnnotationModeler
DrwDimension		CATInterfaceEvents			 libCATAnnotationModeler
DrwDimension       CATIDftEquivMatrixForExplode                 libCATAnnotationModeler
DrwDimension       CATICutAndPastable               libCATAnnotationModeler
DrwDimension		CATI2DLPDFExportData			libCATAnnotationModeler
DrwDimension       CATIDrwNumericalProperties  libCATAnnotationModeler
DrwDimension        CATI2DSubstituteColor	          libCATAnnotationModeler
DrwDimension        CATIDrwAnnotationComponent      libCATAnnotationModeler
DrwDimension        CATIDrwEltWithLeader            libCATAnnotationModeler
DrwDimension        CATIDrwAnnotation               libCATAnnotationModeler
DrwDimension        CATIDrwForLocalRef              libCATAnnotationModeler
DrwDimension        CATIDftElementInSystem          libCATAnnotationModeler
DrwDimension        CATIViewPlaneObserver           libCATAnnotationModeler
DrwDimension        CATIViewOperatorsObserver       libCATAnnotationModeler
DrwDimension        CATIDrwDimClipping              libCATAnnotationModeler
DrwDimension        CATIAnnObserver                 libCATAnnotationModeler
DrwDimension        CATIAnnDimensionValueEditor     libCATAnnotationModeler
DrwDimension        CATIAnnDimensionLineForeshortenedEditor	libCATAnnotationModeler

CircularExtLine     CATIDrwDimExtensionLineCircular libCATAnnotationModeler
CircularExtLine     CATIDrwDimExtensionLine         libCATAnnotationModeler
CircularDimLine     CATIDrwDimDimensionLineCircular libCATAnnotationModeler
CircularDimLine     CATIDrwDimDimensionLine         libCATAnnotationModeler
CurvilinearDimLine  CATIDrwDimDimensionLine         libCATAnnotationModeler
CurvilinearDimLine  CATIDrwDimDimensionLineCurvilinear   libCATAnnotationModeler



# Dimension early features

CATDrwDimensionEarly  LifeCycleObject               libCATAnnotationModeler
CATDrwDimensionEarly  CATIDrwSubscriber             libCATAnnotationModeler
CATDrwDimensionEarly  CATIDrwUndoRedo               libCATAnnotationModeler
CATDrwDimensionEarly  CATILoadModeListener          libCATAnnotationModeler
CATDrwDimensionEarly  CATISpecSynchronize           libCATAnnotationModeler
CATDrwDimensionEarly  CATICutAndPastable            libCATAnnotationModeler

CATDrwDimLinesEarly CATIDrwDimExtensionLine         libCATAnnotationModeler
CATDrwDimLinesEarly CATIDrwDimExtensionLineLinear   libCATAnnotationModeler
CATDrwDimLinesEarly CATIDrwDimFunnel                libCATAnnotationModeler
CATDrwDimLinesEarly CATIDrwDimDimensionLine         libCATAnnotationModeler
CATDrwDimLinesEarly CATIDrwDimDimensionLineLinear   libCATAnnotationModeler
CATDrwDimLinesEarly CATIDrwDimDimensionLineForshortened	libCATAnnotationModeler
CATDrwDimLinesEarly   CATISpecSynchronize           libCATAnnotationModeler
CATDrwDimLinesEarly CATIAnnDimensionLineForeshortened	libCATAnnotationModeler
CATDrwDimLinesEarly         CATIDrwUndoRedo          libCATAnnotationModeler

CATDrwDimValueEarly CATIDrwDimValue                 libCATAnnotationModeler
CATDrwDimValueEarly CATIDrwDimFakeComponent         libCATAnnotationModeler
CATDrwDimValueEarly CATIDrwDimFrame                 libCATAnnotationModeler
CATDrwDimValueEarly   CATISpecSynchronize           libCATAnnotationModeler
CATDrwDimValueEarly         CATIDrwUndoRedo          libCATAnnotationModeler

CATDrwDimValueSubset CATIDrwDimTolerance            libCATAnnotationModeler
CATDrwDimValueSubset CATIDrwDimToleranceNum         libCATAnnotationModeler
CATDrwDimValueSubset CATIDrwDimToleranceAlphaNum    libCATAnnotationModeler
CATDrwDimValueSubset	CATIDrwDimValueComponent       libCATAnnotationModeler
CATDrwDimValueSubset	CATIDrwDimFormat               libCATAnnotationModeler
CATDrwDimValueSubset	CATIDrwDimDressUp              libCATAnnotationModeler
CATDrwDimValueSubset	CATIDrwDimText                 libCATAnnotationModeler

CATDrwDimSecondaryValueSubset	CATIDrwDimFormat       libCATAnnotationModeler

# Cote de coordonnees

DrwCoordDimension   CATIDrwCoordDimension           libCATAnnotationModeler
DrwCoordDimension   CATIDftCoordDimension           libCATAnnotationModeler
DrwCoordDimension   CATIDrwTextProperties           libCATAnnotationModeler
DrwCoordDimension   CATIDrwProperties               libCATAnnotationModeler
DrwCoordDimension   CATIDrwSimpleText               libCATAnnotationModeler
DrwCoordDimension   CATIDrwAnnotationComponent      libCATAnnotationModeler
DrwCoordDimension   CATIDrwEltWithLeader            libCATAnnotationModeler
DrwCoordDimension   CATIDrwAnnotation               libCATAnnotationModeler
DrwCoordDimension   CATIMove2D                      libCATAnnotationModeler
DrwCoordDimension   CATISpecObject                  libCATAnnotationModeler
DrwCoordDimension   LifeCycleObject                 libCATAnnotationModeler
DrwCoordDimension   CATIDftEquivMatrixForExplode    libCATAnnotationModeler
DrwCoordDimension   CATIAnnIsolate                  libCATAnnotationModeler
DrwCoordDimension   CATIDrwDimFormat                libCATAnnotationModeler
DrwCoordDimension   CATIDrwNumericalProperties      libCATAnnotationModeler
DrwCoordDimension   CATIAnnLeadersAnchor            libCATAnnotationModeler
DrwCoordDimension   CATIParmPublisher               libCATAnnotationModeler
DrwCoordDimension   CATIInstance                    libCATAnnotationModeler
DrwCoordDimension   CATIInstanceAttributes          libCATAnnotationModeler

# Simple text

DrwSimpleText       CATIDrwElement                  libCATAnnotationModeler
DrwSimpleText       CATIDrwSimpleText               libCATAnnotationModeler
DrwSimpleText	     CATIDrwTextProperties	          libCATAnnotationModeler
DrwSimpleText       CATIDrwAnnotation               libCATAnnotationModeler
DrwSimpleText       CATIMove2D                      libCATAnnotationModeler
DrwSimpleText       CATIDrwAnnotationComponent      libCATAnnotationModeler
DrwSimpleText       CATIDrwEltWithLeader            libCATAnnotationModeler
DrwSimpleText       CATIDrwProperties               libCATAnnotationModeler
DrwSimpleText       CATISpecObject                  libCATAnnotationModeler
DrwSimpleText       LifeCycleObject                 libCATAnnotationModeler
DrwSimpleText       CATIDftEquivMatrixForExplode                 libCATAnnotationModeler
DrwSimpleText		CATI2DLPDFExportData			libCATAnnotationModeler
DrwSimpleText    CATIDrwForLocalRef           libCATAnnotationModeler

DrwSubText          CATIDrwSubText                  libCATAnnotationModeler
DrwTextStyle        CATIDrwTextStyle                libCATAnnotationModeler


# GDT

DrwGDT              CATIDrwGDT                      libCATAnnotationModeler
DrwGDT              CATIDrwTextProperties           libCATAnnotationModeler
DrwGDT              CATIDrwProperties               libCATAnnotationModeler
DrwGDT              CATIDrwAnnotationComponent      libCATAnnotationModeler
DrwGDT              CATIDrwEltWithLeader            libCATAnnotationModeler
DrwGDT              CATIDrwAnnotation               libCATAnnotationModeler
DrwGDT              CATIMove2D                      libCATAnnotationModeler
DrwGDT              CATISpecObject                  libCATAnnotationModeler
DrwGDT              LifeCycleObject                 libCATAnnotationModeler
DrwGDT              CATIDftGDTCheck                 libCATAnnotationModeler
DrwGDT             CATIDftEquivMatrixForExplode           libCATAnnotationModeler
DrwGDT    CATIDrwForLocalRef           libCATAnnotationModeler
DrwGDT              CATI2DLPDFExportData            libCATAnnotationModeler

DftGDT              CATIDftGDTCheck                libCATAnnotationModeler 


# Balloon

DrwBalloon          CATIDrwBalloon                  libCATAnnotationModeler
DrwBalloon          CATIDrwTextProperties	          libCATAnnotationModeler
DrwBalloon          CATIDrwProperties               libCATAnnotationModeler
DrwBalloon          CATIDrwSimpleText               libCATAnnotationModeler
DrwBalloon          CATIDrwAnnotationComponent      libCATAnnotationModeler
DrwBalloon          CATIDrwEltWithLeader            libCATAnnotationModeler
DrwBalloon          CATIDrwAnnotation               libCATAnnotationModeler
DrwBalloon          CATIMove2D                      libCATAnnotationModeler
DrwBalloon          CATISpecObject                  libCATAnnotationModeler
DrwBalloon          LifeCycleObject                 libCATAnnotationModeler
DrwBalloon          CATIDftEquivMatrixForExplode    libCATAnnotationModeler
DrwBalloon			CATIDftBalloon					libCATAnnotationModeler
DrwBalloon			CATIDftText						libCATAnnotationModeler
DrwBalloon			CATIDftAnnotation				libCATAnnotationModeler


# DatumFeature/DatumTarget

DrwDatumFeature     CATIDrwDatumFeature             libCATAnnotationModeler
DrwDatumFeature     CATIDftDatumFeature             libCATAnnotationModeler
DrwDatumFeature	    CATIDrwTextProperties	          libCATAnnotationModeler
DrwDatumFeature     CATIDrwProperties               libCATAnnotationModeler
DrwDatumFeature     CATIDrwSimpleText               libCATAnnotationModeler
DrwDatumFeature     CATIDrwAnnotationComponent      libCATAnnotationModeler
DrwDatumFeature     CATIDrwEltWithLeader            libCATAnnotationModeler
DrwDatumFeature     CATIDrwAnnotation               libCATAnnotationModeler
DrwDatumFeature     CATIMove2D                      libCATAnnotationModeler
DrwDatumFeature     CATISpecObject                  libCATAnnotationModeler
DrwDatumFeature     LifeCycleObject                 libCATAnnotationModeler
DrwDatumFeature     CATIDftEquivMatrixForExplode    libCATAnnotationModeler
DrwDatumFeature		  CATIAnnIsolate                  libCATAnnotationModeler
DrwDatumFeature		  CATIAnnLeadersAnchor            libCATAnnotationModeler
DrwDatumFeature     CATIParmPublisher               libCATAnnotationModeler
DrwDatumFeature     CATIInstance                    libCATAnnotationModeler
DrwDatumFeature     CATIInstanceAttributes          libCATAnnotationModeler

DrwDatumTarget      CATIDrwDatumTarget              libCATAnnotationModeler
DrwDatumTarget      CATIDftDatumTarget              libCATAnnotationModeler
DrwDatumTarget	    CATIDrwTextProperties	          libCATAnnotationModeler
DrwDatumTarget      CATIDrwProperties               libCATAnnotationModeler
DrwDatumTarget      CATIDrwSimpleText               libCATAnnotationModeler
DrwDatumTarget      CATIDrwAnnotationComponent      libCATAnnotationModeler
DrwDatumTarget      CATIDrwEltWithLeader            libCATAnnotationModeler
DrwDatumTarget      CATIDrwAnnotation               libCATAnnotationModeler
DrwDatumTarget      CATIMove2D                      libCATAnnotationModeler
DrwDatumTarget      CATISpecObject                  libCATAnnotationModeler
DrwDatumTarget      LifeCycleObject                 libCATAnnotationModeler
DrwDatumTarget      CATIDftDatumTargetCheck         libCATAnnotationModeler
DrwDatumTarget      CATIDftEquivMatrixForExplode    libCATAnnotationModeler
DrwDatumTarget		CATIAnnIsolate                  libCATAnnotationModeler
DrwDatumTarget		CATIAnnLeadersAnchor            libCATAnnotationModeler
DrwDatumTarget      CATIParmPublisher               libCATAnnotationModeler
DrwDatumTarget      CATIInstance                    libCATAnnotationModeler
DrwDatumTarget      CATIInstanceAttributes          libCATAnnotationModeler
DrwDatumTarget      CATIDftElementWithLeader        libCATAnnotationModeler

# Roughness Symbol

DrwRough            CATIDrwRough                    libCATAnnotationModeler
DrwRough            CATIDrwProperties               libCATAnnotationModeler
DrwRough            CATIDrwTextProperties           libCATAnnotationModeler
DrwRough            CATIDrwSimpleText               libCATAnnotationModeler
DrwRough            CATIDrwAnnotationComponent      libCATAnnotationModeler
DrwRough            CATIDrwEltWithLeader            libCATAnnotationModeler
DrwRough            CATIDrwAnnotation               libCATAnnotationModeler
DrwRough            CATIMove2D                      libCATAnnotationModeler
DrwRough            CATISpecObject                  libCATAnnotationModeler
DrwRough            LifeCycleObject                 libCATAnnotationModeler
DrwRough            CATIDftEquivMatrixForExplode    libCATAnnotationModeler


# Leader & Arrow
DrwLeader           CATIIsolate                     libCATAnnotationModeler


# Broken dimension (from .model)

DrwAnnBroken        CATIDrwAnnBroken                libCATAnnotationModeler
DrwAnnBroken        CATIDrwAnnotation               libCATAnnotationModeler
DrwAnnBroken        CATIDrwAnnotationComponent      libCATAnnotationModeler
DrwAnnBroken        CATIDrwEltWithLeader            libCATAnnotationModeler
DrwAnnBroken        CATIMove2D                      libCATAnnotationModeler
DrwAnnBroken        CATISpecObject                  libCATAnnotationModeler
DrwAnnBroken        LifeCycleObject                 libCATAnnotationModeler
DrwAnnBroken        CATIDftEquivMatrixForExplode    libCATAnnotationModeler
DrwAnnBroken        CATICutAndPastable              libCATAnnotationModeler
DrwAnnBroken        CATIDrwForLocalRef              libCATAnnotationModeler

# Connectors

LIGHTCONNECTOR      CATISpecObject                  libCATAnnotationModeler
LIGHTCONNECTOR      CATIMove                        libCATAnnotationModeler
LIGHTCONNECTOR      CATIBloc                        libCATAnnotationModeler
LIGHTCONNECTOR      CATICstElement                  libCATAnnotationModeler
LIGHTCONNECTOR      CATIDrwElement                  libCATAnnotationModeler
LIGHTCONNECTOR      CATIDrwEltInView                libCATAnnotationModeler
LIGHTCONNECTOR      LifeCycleObject                 libCATAnnotationModeler
LIGHTCONNECTOR      CATIExternalLinkValuate         libCATAnnotationModeler
LIGHTCONNECTOR      CATI2DLPDFExportData            libCATAnnotationModeler
LIGHTCONNECTOR      CATIConnector                   libCATAnnotationModeler

DrwConnector        CATIDftEquivMatrixForExplode    libCATAnnotationModeler    CntrOnFurtGeomPreCond

DrwConnector        CATISpecObject                  libCATAnnotationModeler
DrwConnector        CATIDrwConnector                libCATAnnotationModeler
DrwConnector        CATIMove                        libCATAnnotationModeler
DrwConnector        CATIBloc                        libCATAnnotationModeler
DrwConnector        CATICstElement                  libCATAnnotationModeler

DrwConnector        CATIDrwElement                  libCATAnnotationModeler
DrwConnector        CATIDrwSubscriber	            libCATAnnotationModeler
DrwConnector        CATIConnector                   libCATAnnotationModeler
DrwConnector        LifeCycleObject                 libCATAnnotationModeler
DrwConnector        CATIDrwEltInView                libCATAnnotationModeler

DrwConnector        CATICutAndPastable              libCATAnnotationModeler
DrwConnector		    CATI2DLPDFExportData			      libCATAnnotationModeler
DrwConnector        CATIDftCacheCompAppUndo         libCATAnnotationModeler

DrwConnector        CATIViewPlaneObserver           libCATAnnotationModeler

CATDrwConnectorCacheUndoVisitor CATIUndoVisitor libCATAnnotationModeler
CATDbiASyncUpdateListener CATILoadModeListener libCATAnnotationModeler
DrwConnector CATISCHTOLExportData libCATAnnotationModeler
DrwConnector CATIGlobalCopyIdentifierExportData libCATAnnotationModeler

# Dimension systems

DrwDimSystem		    CATIDrwDimSystemPro				      libCATAnnotationModeler
DrwDimSystem        CATIDrwDimSystem                libCATAnnotationModeler
DrwDimSystem        CATIDrwChainedDimSystem         libCATAnnotationModeler   CATAnnChainedDimSystPreCond
DrwDimSystem        CATIDrwStackedDimSystem         libCATAnnotationModeler   CATAnnStackedDimSystPreCond
DrwDimSystem		    CATIDrwSymmetricDimSystem		    libCATAnnotationModeler		CATAnnSymmetricDimSystPreCond
DrwDimSystem        CATIDrwCumulatedDimSystem       libCATAnnotationModeler   CATAnnCumulatedDimSystPreCond
DrwDimSystem        CATIDrwDimLineUp                libCATAnnotationModeler
DrwDimSystem        CATIDftElementWithStyle         libCATAnnotationModeler
DrwDimSystem        CATIDftElementWithStandard      libCATAnnotationModeler
DrwDimSystem        CATIMove2D                      libCATAnnotationModeler
DrwDimSystem        CATIDrwUndoRedo                 libCATAnnotationModeler
DrwDimSystem        CATIDrwElement                  libCATAnnotationModeler
DrwDimSystem        CATISpecObject                  libCATAnnotationModeler
DrwDimSystem        LifeCycleObject                 libCATAnnotationModeler
DrwDimSystem        CATIDrwTextProperties           libCATAnnotationModeler
DrwDimSystem        CATIInstance                    libCATAnnotationModeler
DrwDimSystem        CATIOwner                       libCATAnnotationModeler
DrwDimSystem        CATIIsolate                     libCATAnnotationModeler
DrwDimSystem        CATICutAndPastable              libCATAnnotationModeler
DrwDimSystem        CATIDrwDimSystemMdl             libCATAnnotationModeler
DrwDimSystem        CATIDrwProperties               libCATAnnotationModeler
DrwDimSystem        CATIInstanceAttributes          libCATAnnotationModeler
DrwDimSystem        CATIParmPublisher               libCATAnnotationModeler
DrwDimSystem        CATIDrwNumericalProperties      libCATAnnotationModeler
DrwDimSystem        CATI2DSubstituteColor           libCATAnnotationModeler
DrwDimSystem        CATIAlias                       libCATAnnotationModeler
DrwDimSystem        CATIDftEquivMatrixForExplode    libCATAnnotationModeler
DrwDimSystem        CATIDrwForLocalRef              libCATAnnotationModeler


######################
##  Adhesion pour CAA2
######################
DrwAnnotation       CATIDftAnnotation               libCATAnnotationModeler
DrwAnnotation       CATIDftProperties               libCATAnnotationModeler
DrwGDT              CATIDftElementWithLeader        libCATAnnotationModeler
DrwSimpleText       CATIDftElementWithLeader        libCATAnnotationModeler


DrwSimpleText        CATIDrwUndoRedo          libCATAnnotationModeler
DrwDatumTarget       CATIDrwUndoRedo          libCATAnnotationModeler
DrwDatumFeature      CATIDrwUndoRedo          libCATAnnotationModeler
DrwBalloon           CATIDrwUndoRedo          libCATAnnotationModeler
DrwCoordDimension    CATIDrwUndoRedo          libCATAnnotationModeler
DrwGDT               CATIDrwUndoRedo          libCATAnnotationModeler
DrwRough             CATIDrwUndoRedo          libCATAnnotationModeler
DrwDimension         CATIDrwUndoRedo          libCATAnnotationModeler

######################
##  Adhesion pour Cleaner
######################
ConstraintDRW                CATICheckObject     libCATClnAnn
ConstraintDRW                CATICleanObject     libCATClnAnn
ConstraintHYB                CATICheckObject     libCATClnAnn
ConstraintHYB                CATICleanObject     libCATClnAnn
DrwConnector                 CATICheckObject     libCATClnAnn
DrwConnector                 CATICleanObject     libCATClnAnn
DrwSimpleText                CATICheckObject     libCATClnAnn
DrwSimpleText                CATICleanObject     libCATClnAnn
DrwBalloon                   CATICheckObject     libCATClnAnn
DrwBalloon                   CATICleanObject     libCATClnAnn
DrwDimension                 CATICheckObject     libCATClnAnn
DrwDimension                 CATICleanObject     libCATClnAnn
DrwAnnotationComponent       CATICheckObject     libCATClnAnn
DrwAnnotationComponent       CATICleanObject     libCATClnAnn
DrwLeader                    CATICheckObject     libCATClnAnn
DrwLeader                    CATICleanObject     libCATClnAnn

# devrait plutot etre dans DraftingInfrastructure, mais la correction ne pouvait attendre
CATDrwCont          CATICleanObject     libCATClnAnn
CATDrwCont          CATICheckObject     libCATClnAnn
CATDrwMirrorCont    CATICleanObject     libCATClnAnn
CATDrwMirrorCont    CATICheckObject     libCATClnAnn
CAT2DLCont          CATICleanObject     libCATClnAnn
CAT2DLCont          CATICheckObject     libCATClnAnn


# CGM Data to add an attribute of orientation with the CGM objects will be used only when we will required to stream the data

CATCGMDefOrientAttr CATICGMDomainBinder							libCATAnnotationModeler

# divers
CATDftFurtiveCircle          IDMCircle2D               libCATAnnotationModeler
CATDftFurtiveCircle          IDMCurve2D                libCATAnnotationModeler
CATDftFurtiveLine            IDMLine2D                 libCATAnnotationModeler
CATDftFurtiveLine            IDMCurve2D                libCATAnnotationModeler
CATDftFurtiveLine            CATI2DGeoVisu             Delegated
ConstraintDRW                CATIDrwDimRepresentation  libCATAnnotationModeler
ConstraintDYS                CATIDrwDimRepresentation  libCATAnnotationModeler
CATDftVolatileFeatureDimRep  CATIDrwDimRepresentation  libCATAnnotationModeler
CATDftVolatileGenDimRep      CATIDrwDimRepresentation  libCATAnnotationModeler

DrwText               CATIDftTextProperties     libCATAnnotationModeler
DrwText               CATIDftProperties         libCATAnnotationModeler
DrwText               CATICheckObject           libCATClnAnn
DrwText               CATICleanObject           libCATClnAnn

View                  CATIDftAnnotDuplicator    libCATAnnotationModeler
View2DL               CATIDftAnnotDuplicator    libCATAnnotationModeler


# Nouvelle adhesion pour le contrainte hybride
ConstraintHYB         CATIExternalLinkValuate   libCATAnnotationModeler

CATV4AnnStandardUpgradeModif CATMarshallableCorba libCATAnnotationModeler

# AnnFormattedValue
AnnFormattedValue     CATIAnnFormattedValue     libCATAnnotationModeler

# AnnFormat
AnnFormat             CATIDrwDimFormat          libCATAnnotationModeler

# Factory de connector pour FTA RGE/CG

CATDrwCont                  CATIDrwConnectorFactory             libCATAnnotationModeler

# CATDrwAnnNumericalDescription
CATDrwAnnNumericalDescription             CATIAnnNumericalDescription          libCATAnnotationModeler

CATAnnotationStandardResetModif CATMarshallableCorba libCATAnnotationModeler

AnnCallout             CATIDftElementWithStandard          libCATAnnotationModeler

# CATIAnnForeshortenedBeginingPointReference
TPSCGLine              CATIAnnForeshortenedBeginingPointReference   libCATAnnotationModeler
TPSCGPlane             CATIAnnForeshortenedBeginingPointReference   libCATAnnotationModeler   
2DLine                 CATIAnnForeshortenedBeginingPointReference   libCATAnnotationModeler
CATPath2DLine          CATIAnnForeshortenedBeginingPointReference   libCATAnnotationModeler   CATPreCondForPath2DLineFSBeginingPoint
CATRSurImpl            CATIAnnForeshortenedBeginingPointReference   libCATAnnotationModeler   CATPreCondForRSurFSBeginingPoint
CATBorderREdgeImpl     CATIAnnForeshortenedBeginingPointReference   libCATAnnotationModeler   
CATWireREdgeImpl       CATIAnnForeshortenedBeginingPointReference   libCATAnnotationModeler   
CATREdgeImpl           CATIAnnForeshortenedBeginingPointReference   libCATAnnotationModeler   


2DLine                     CATIAnnModificationObservable                libCATAnnotationModeler  
2DLine                     CATIAnnDeletionObservable                    libCATAnnotationModeler  

DrwConnector               CATIAnnModificationObservable                libCATAnnotationModeler  

CATDrwCont CATIDftLegacyStdImportOpFactory libCATAnnotationModeler
CAT2DLCont CATIDftLegacyStdImportOpFactory libCATAnnotationModeler
CATDrwCont CATIAnnV4StdImportOpFactory     libCATAnnotationModeler
CAT2DLCont CATIAnnV4StdImportOpFactory     libCATAnnotationModeler

CATAnnSelectionSetFacade CATIAnnSelectionSetFacade libCATAnnotationModeler

AnnRasterImageStream  CATI2DGeoVisu             libCATAnnotationModeler
AnnRasterImageStream  CATIAnnImageStreamEditor  libCATAnnotationModeler
AnnRasterImageStream  CATIAnnRasterImageStream  libCATAnnotationModeler
AnnRasterImageStream  CATIAnnImageStream        libCATAnnotationModeler
AnnRasterImageStream  CATICutAndPastable        libCATAnnotationModeler

AnnVectorImageStream  CATI2DGeoVisu             libCATAnnotationModeler
AnnVectorImageStream  CATIAnnImageStreamEditor  libCATAnnotationModeler
AnnVectorImageStream  CATIAnnVectorImageStream  libCATAnnotationModeler
AnnVectorImageStream  CATIAnnImageStream        libCATAnnotationModeler
AnnVectorImageStream  CATICutAndPastable        libCATAnnotationModeler

DrwAnnotationComponent CATIAnnMirrorable libCATAnnotationModeler

DrwMappingTable LifeCycleObject libCATAnnotationModeler

DrwConnectorTechResHole			CATIDrwConnectorFeatureAccess			libCATAnnotationModeler
DrwConnectorTechResUserFeat		CATIDrwConnectorFeatureAccess			libCATAnnotationModeler
DrwConnectorBase				CATIDrwConnectorBaseAccess				libCATAnnotationModeler
DrwConnectorTechResThread		CATIDrwConnectorFeatureAccess			libCATAnnotationModeler
DrwConnectorTechnologicalResult	CATIDrwConnectorTechResReconciliation	libCATAnnotationModeler

AnnIndicator CATIDftElementWithStandard libCATAnnotationModeler
