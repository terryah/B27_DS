@echo off

rem PPGExecution_DeletedRefs.txt format.  This is a tab-delimited file.
rem Column 1   PiecePartLocation    
rem Column 2   ExecutionLogFileName
rem
rem Setting CATIA Environment Param
set CATIA=C:\CATIA\B20\win_b64\code\bin
set ENV=CATIA_P3.V5R20.B20
set DIRENV=C:\ProgramData\DassaultSystemes\CATEnv
set myApplication=PPEMovePieceParts.exe
set myApplicationParms=C:\Services\PPGExecution_DeletedRefs\PPGExecution_DeletedRefs.txt

"%CATIA%\CATSTART.exe" -run "%myApplication% %myApplicationParms%" -env %ENV% -direnv "%DIRENV%"
