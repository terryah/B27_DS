' COPYRIGHT DELMA CORP. 2006
'=====================================================================
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''    MacroName..........CopyMoveActivity.CATScript 
''    AUTEUR:............BPL
''    DATE:..............3/02/2006
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' This script will copy the data from an existing 
' Move Activity to a newly created Move Activity.
' This version of the script uses transforms in 
' the format of x/y/z/roll/pitch/yaw.
' Data:
'  This script assumes that root process will have as children
' a Start activity and then a 'father' activity.  This father 
' will have a move activity as its 1st child (no start 
' or stop).  This move activity can have any number of shots 
' and assigned items and resources.
'  The father activity can be any activity type created from a 
' process library.
'=====================================================================

Language="VBSCRIPT"

Sub CATMain()

    ' Get the document
    Dim CurrentDoc As Document
    Set CurrentDoc = CATIA.ActiveDocument

    ' Get the process document
    Dim ProcessDoc As PPRDocument
    Set ProcessDoc = CurrentDoc.PPRDocument

    ' Get the "Process" Activity
    Dim RootActivity
    Set RootActivity = CurrentDoc.GetItem("Process")

    ' Now get the root activity's children
    Dim RootChildren
    Set RootChildren = RootActivity.ChildrenActivities

    ' Since the first activity is the Start activity we get the second 
    ' activity.
    Dim Father
    Set Father = RootChildren.Item(2)

    ' Create a selection and add our activity to it
    Set TheSelection = CurrentDoc.Selection
    TheSelection.Add(Father)

    ' Get an Activity pointer on the father
    Dim FatherActivity as Activity
    Set FatherActivity = TheSelection.FindObject("CATIAActivity")

    ' Create the 2nd Move activity
    Dim SecondActivity as Activity
    Set SecondActivity = FatherActivity.CreateChild("DNBAsyMotionActivity")

    ' Get access to children of father
    Dim FathersChildren as CATIAActivities
    Set FathersChildren = FatherActivity.ChildrenActivities

    ' Get 1st child of father, which is the existing move activity
    Dim FirstActivity as Activity
    Set FirstActivity = FathersChildren.Item(1)

    ' Get Move Activity interface on 1st move activity
    TheSelection.Clear
    TheSelection.Add(FirstActivity)
    Dim FirstMoveActivity 
    Set FirstMoveActivity = TheSelection.FindObject("DNBIAAsyMotionActivity")

    ' Get Move Activity interface on 2st move activity
    TheSelection.Clear
    TheSelection.Add(SecondActivity)
    Dim SecondMoveActivity 
    Set SecondMoveActivity = TheSelection.FindObject("DNBIAAsyMotionActivity")

    ' Copy the profile transform
    Dim Profile(6)
    FirstMoveActivity.GetProfilePosition Profile, AsyMotionActivityXYZRPY
    SecondMoveActivity.SetProfilePosition Profile, AsyMotionActivityXYZRPY

    ' Copy the mode
    SecondMoveActivity.MoveMode = FirstMoveActivity.MoveMode

    ' Set track speed
    SecondMoveActivity.MotionSpeed = FirstMoveActivity.MotionSpeed

    ' Get targets lists from activities
    Dim FirstTargetList As AsyMotionTargets
    Set FirstTargetList = FirstMoveActivity.GetTargets
    Dim SecondTargetList As AsyMotionTargets
    Set SecondTargetList = SecondMoveActivity.GetTargets

    ' Obtain the number of targets
    NumberTargets = FirstTargetList.Count

    ' Copy earch target
    ' Prep data
    Dim FirstTarget as DNBIAAsyMotionTarget
    Dim SecondTarget as DNBIAAsyMotionTarget
    Dim ObjectPos(6)
    Dim CompassPos(6)
    For i = 1 to NumberTargets 
        ' Get the 1st and 2nd targets
        Set FirstTarget = FirstTargetList.Item(i)
        FirstTarget.GetObjectPosition ObjectPos, AsyMotionTargetXYZRPY
        FirstTarget.GetCompassPosition CompassPos, AsyMotionTargetXYZRPY

        ' Create the new target at the end of the list 
        ' and set the target data 
        SecondTargetList.InsertEmptyTarget i
        Set SecondTarget = SecondTargetList.Item(i)
        ' Set the time first to ensure a correct redraw of the track
        SecondTarget.TargetTime = FirstTarget.TargetTime
        SecondTarget.SetObjectPosition ObjectPos, AsyMotionTargetXYZRPY
        SecondTarget.SetCompassPosition CompassPos, AsyMotionTargetXYZRPY
    Next

    ' Copy the assignment of items
    Dim FirstItems as Items
    Set FirstItems = FirstActivity.Items
    Dim SecondItems as Items
    Set SecondItems = SecondActivity.Items
    Dim AnItem as Item

    NumberItems = FirstItems.Count
    For j = 1 to NumberItems 
        Set AnItem = FirstItems.Item(j)
        SecondItems.AddByAssignmentType AnItem, ProcessProcesses 
    Next

    ' Copy the assignment of resources
    Dim FirstResources as Resources
    Set FirstResources = FirstActivity.Resources
    Dim SecondResources as Resources
    Set SecondResources = SecondActivity.Resources    
    Dim AResource as Resource

    NumberResources = FirstResources.Count
    For j = 1 to NumberResources 
        Set AResource = FirstResources.Item(i)
        ' Note: I sure hope this is an assignment of the correct type...
        SecondResources.Add AResource
    Next

    ' Copy the activity name
    SecondActivity.Name = FirstActivity.Name

    ' Copy the cycle time
    SecondActivity.CycleTime = FirstActivity.CycleTime

    ' Clean up
    TheSelection.Clear()

End Sub
