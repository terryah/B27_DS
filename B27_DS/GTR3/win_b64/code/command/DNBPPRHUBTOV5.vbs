'VB Script file which will be called from the batch file 
'The path of the batch file needs to set in DPE
' "TOOLS - Settings - Other DS Software Modules" Tab
'Please don't change/modify the VB file
'This must be used for Open In DPM Command from DPE alone
'This script works only on selected releases where the Interface is delivered
'It was delivered CXR23SP4, CXR25

Main()
Sub Main()
	Dim DELMIA
	'Get the handle to Delmia application if it is opened already 
	'Otherwise launch Delmia
	On Error Resume Next
		Set DELMIA = GetObject("DELMIA.Application")
			If (Err.Number <> 0) Then
				Err.Clear
				Set DELMIA = CreateObject("DELMIA.Application")				 
			Err.Clear
		End If
	
	Dim objScriptingShell
	Dim objUserVariables	
	
	Set objScriptingShell =  CreateObject("WScript.Shell")
	Set objUserVariables = objScriptingShell.Environment("PROCESS") 
	Dim selectedObjectID, sChosenFilters, sParentObjectID, sProjectID, sUserName, sPassword
	
	'Get the required environment variables 
	'These environment variables need to be set throught Open Accessor Interface of DELMIA
	selectedObjectID = objUserVariables("/id:")	
	sChosenFilters = objUserVariables("/currentfiltersettings:")
	sParentObjectID = objUserVariables("/parent_ids:")
	sProjectID = objUserVariables("/projectid:")
	sUserName = objUserVariables("/username:")
	sPassword = objUserVariables("/userpwd:")
	
	Dim oAccessorObject
	Set oAccessorObject = DELMIA.GetItem("DNBE5OpenAccessor")
	
	Dim ErrorMessages()
	oAccessorObject.SetEnvironmentForVBSLaunch selectedObjectID, sChosenFilters, sParentObjectID, sProjectID, sUserName, sPassword, ErrorMessages
		
	Dim retVal
	retVal = UBound(ErrorMessages)			 
	
	If retVal < 0 Then
		DELMIA.Application.StartCommand("DELMPrcFilterPPR_ProjectHdr")	
	Else		
		PrintArray(ErrorMessages)			
	End If
	
End Sub 


Sub PrintArray(Array1)
	MsgBoxValue = "Please Contact Administrator."& vbNewLine	
	for i = UBound(Array1) To 0 Step -1	
		Val = Array1(i)
		MsgBoxValue = MsgBoxValue & Val & vbNewLine				
	Next
	'Display the Error Message to the User
	MsgBox MsgBoxValue
	
End Sub


