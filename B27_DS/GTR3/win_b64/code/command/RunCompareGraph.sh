#!/bin/ksh

#echo "/tmp/InputBatch-${USER}-$(hostname)-$(date | awk '{print $1"-"$2"-"$3}').xml

echo "Running the RunCompareGraph.sh==========="
# argument 1 is inputfile path+name
arg1=$1
# argument 2 is outputfile path+name
arg2=$2
# argument 3 is database instance for VPM environment setting up.
arg3=$3

# comment the following line to ignore traces
export _DEBUG_=1

#########################################################

# set the VPM runtime environment
. /home/adm14ga/env/VPMWsUser.sh #${arg3}

# set the environment description file
envFile=~adm14ga/data/Environments.xml

# Above 2 lines must be updated by the VPM administrator
#########################################################

# last binary option is "classic" or "leaves". It is the comparison mode.

CompareGraph $arg1 $arg2 $envFile classic

rc=$?

# If the batch failed, create an output XML file with the error code (>=1)
if [[ ${rc} != "0" ]];
then
    echo '<?xml version = "1.0" encoding = "UTF-8" ?>'   >  ${arg2}
    echo '<PartVersionReport status = "'${rc}'">'        >> ${arg2}
    echo '   <Date>'$(date)'</Date>'                     >> ${arg2}
    echo '</PartVersionReport>'                          >> ${arg2}
fi

echo "" >> ${arg2}

echo ""
echo "wrote file : ${arg2}"
echo "rc="${rc}
echo ""

return ${rc}
