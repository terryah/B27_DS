' COPYRIGHT DELMA CORP. 2006
'=====================================================================
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''    MacroName..........ReadFatherOfMoveActFromExcel.CATScript 
''    Author:............Brian Perles (BPL)
''    Date:..............3/02/2006
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'  This script reads move activities from Excel files.  It is intended to allow 
' manual sim roll up.
'  Pre-conditions: The destination process must contain the same operation
' activity as the source process.  It should have assigned to it all the 
' products that their child move activities will move.  The (filled) Excel data 
' files have already been generated and located in the input directory.
'  Usage: Select a single operation activity and this script.  The move 
' activities will be created as children and the correct products will be 
' assigned to them.
'
' Limitations:
'  Father activity names can not have periods in them.
'  Does not create process flow between the created child move activities.
'  Supports only simple item assignment (PPP) of products to move activities.
'  The directory path for the data files is hard coded in the script.
'
' Excel File Format: 
' "Father of APS Move Activities"
' <Number of move activities in this file>
' <Move Activity 1 Name>
' <Number of Assigned Items> <Name of assigned item #1> <Name of assigned item #2> …
' <Speed (mm/s)>
' <Time (seconds)>
' <Move Mode (integer)>
' <Profile Position, X, Y, Z (mm), Roll, Pitch, Yaw (degrees)>
' <Number of Targets>
' <Target 1 Object Position, xyzrpy>, <Target 1 Compass Position, xyzrpy>
' <Target 1 duration>
' <Target 2 Object Position, xyzrpy>, <Target 2 Compass Position, xyzrpy>
' <Target 2 duration>
' <Target 3 ...>
' <Move Activity 2 Name>
' <...>
' Note: The Move Mode is stored as the integer value of the enum value.
' The 1st value is equal to zero, the 2nd is equal to one, and so on.
'===================================== Start of Script ========================================

Language="VBSCRIPT"

Sub CATMain()

    ' Open an Excel session
    Dim Indexfs
    Dim OutPath
    Dim OutIndex
    Dim wbk   As Excel.Workbook
    Dim xlApp As Excel.Application

    ' The location of the data file
    OutPath = "C:\TEMP\AutomationOutput\"

    ' Get the process document
    Dim CurrentDoc As Document
    Set CurrentDoc = CATIA.ActiveDocument

    ' The user must have selected the activity to be acted appon    
    Set TheSelection = CurrentDoc.Selection
    Dim FatherActivity as Activity
    Set FatherActivity = TheSelection.FindObject("CATIAActivity")

    ' The data file is named after the selected activity.
    Dim TheName as string
    TheName = FatherActivity.Name
    OutIndex = TheName & ".xls"
    MsgBox "Data File: " & OutPath & OutIndex

    ' Note: This will only open an existing workbook
    Set xlapp = CreateObject("Excel.Application")
    Set Indexfs = CATIA.Application.FileSystem
    Set wbk = xlapp.Workbooks.Open(OutPath & OutIndex)

    Dim CellRows as Integer
    Dim CellColumns as Integer
    CellRows = 1
    ' Ignore first row
    CellRows = CellRows + 1

    Dim ChildrenToCreate as Integer
    ChildrenToCreate = wbk.ActiveSheet.Cells(CellRows, 1)
    CellRows = CellRows + 1

    Dim CurActivity
    Dim ii As Integer
    For ii = 1 To ChildrenToCreate
        CreateMoveActivity FatherActivity, wbk, CellRows
    Next
    
    ' clean up
    wbk.Close
    xlapp.quit
    
End Sub
' End Main

Sub CreateMoveActivity( FatherActivity as Activity, wbk As Excel.Workbook, CellRows as Integer )

    Dim i As Integer
    Dim j As Integer
    Dim jj As Integer
    
    ' Get the document
    Dim CurrentDoc As Document
    Set CurrentDoc = CATIA.ActiveDocument
    Set TheSelection = CurrentDoc.Selection
    TheSelection.Clear()

    ' Create the new move activity    
    Dim TheNewActivity as Activity
    Set TheNewActivity = FatherActivity.CreateChild("DNBAsyMotionActivity")
    Dim TheMoveActivity as DNBIAAsyMotionActivity
    TheSelection.Add(TheNewActivity)
    Set TheMoveActivity = TheSelection.FindObject("DNBIAAsyMotionActivity")

    ' Name the new activity
    Dim MoveActName as String
    MoveActName = wbk.ActiveSheet.Cells(CellRows, 1)
    TheNewActivity.Name = MoveActName 
    CellRows = CellRows + 1
    MsgBox "Creating: " & MoveActName

    ' Set the new activity's assigned items
    Dim FatherItemsList as Items
    Set FatherItemsList = FatherActivity.Items
    Dim NumberItemsOfFather as Integer
    NumberItemsOfFather = FatherItemsList.Count
    Dim FathersItem as Item

    Dim ItemsList as Items
    Set ItemsList = TheNewActivity.Items
    Dim TheItem as Item

    Dim NumberItemsToRead as Integer
    NumberItemsToRead = wbk.ActiveSheet.Cells(CellRows, 1)
    CellColumns = 2
    Dim GoalItemName as string
    Dim FoundItemName as string
    ' Walk through the item names in the data file.
    ' For each, try and match it up to an item assign to the father (selected) activity.
    For j = 1 to NumberItemsToRead
        GoalItemName = wbk.ActiveSheet.Cells(CellRows, CellColumns)
        For i = 1 to NumberItemsOfFather
            Set FathersItem = FatherItemsList.Item(i)
            If GoalItemName = FathersItem.Name Then
                ItemsList.AddByAssignmentType FathersItem, ProcessProcesses 
            End If
        Next
        CellColumns = CellColumns + 1
    Next
    CellRows = CellRows + 1

    ' Read in the activity attribute data      
    Dim TheName as string
    Dim TheSpeed as double
    Dim TheTotalDuration as double
    Dim TheMoveMode as AsyMotionActivityMoveMode
    Dim TheMoveModeD as double

    TheSpeed = wbk.ActiveSheet.Cells(CellRows, 1)
    CellRows = CellRows + 1
    TheTotalDuration = wbk.ActiveSheet.Cells(CellRows, 1)
    CellRows = CellRows + 1
    TheMoveModeD = wbk.ActiveSheet.Cells(CellRows, 1) 
    CellRows = CellRows + 1
    ' Note: The move mode is stored in the data file as an integer

    TheMoveActivity.MotionSpeed = TheSpeed 
    TheNewActivity.CycleTime = TheTotalDuration
    If 0 = TheMoveModeD Then
        TheMoveMode = AsyMotionActivitySpeedMode
    Else
        TheMoveMode = AsyMotionActivityTimeMode
    End If
    TheMoveActivity.MoveMode = TheMoveMode

    ' Read In the Profile position
    Dim Profile(6) As CATSafeArrayVariant
    For k = 0 to 5
        kk = k + 1
        Profile(k) = wbk.ActiveSheet.Cells(CellRows, kk)
    Next
    CellRows = CellRows + 1
    TheMoveActivity.SetProfilePosition Profile, AsyMotionActivityXYZRPY

    ' Read in the target data
    ' Get the targets list    
    Dim TargetList As AsyMotionTargets
    Set TargetList = TheMoveActivity.GetTargets
    
    ' Write out the number of targets
    NumOfTargets = wbk.ActiveSheet.Cells(CellRows, 1)
    CellRows = CellRows + 1

    ' Read in each target
    Dim TargetDuration as double
    Dim TheTarget as DNBIAAsyMotionTarget
    Dim ObjectPos(6)  As CATSafeArrayVariant
    Dim CompassPos(6)  As CATSafeArrayVariant
    For i = 1 to NumOfTargets
        TargetList.InsertEmptyTarget i
        Set TheTarget = TargetList.Item(i)
        
        For a = 0 to 5
            aa = a+1
            ObjectPos(a) = wbk.ActiveSheet.Cells(CellRows, aa)
        Next
        For b = 0 to 5
            bb = b+1+6
            CompassPos(b) = wbk.ActiveSheet.Cells(CellRows, bb)
        Next
        CellRows = CellRows + 1
        TargetDuration = wbk.ActiveSheet.Cells(CellRows, 1)
        CellRows = CellRows + 1
        
        TheTarget.TargetTime = TargetDuration 
        TheTarget.SetObjectPosition ObjectPos, AsyMotionTargetXYZRPY
        TheTarget.SetCompassPosition CompassPos, AsyMotionTargetXYZRPY
    Next

End Sub
