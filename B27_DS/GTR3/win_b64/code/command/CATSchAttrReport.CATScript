'// COPYRIGHT DASSAULT SYSTEMES  2000
'//============================================================================
'//
'// CATSchAttrReport.CATScript
'//
'// Language="VBSCRIPT"
'// To report values of attributes specIfied by end user. The format is 
'// read from the user provided .txt template.
'// CATIA provides the following sample .txt templates :
'// The .txt template file must have a format line to create an html or csv (comma separated) file
'//     TextFormat=HTML
'//       or
'//     TextFormat=CSV
'//
'// EquipmentFunctionReportSample.txt        - Equipment list
'// PipingDiagramPartReportSample.txt        - Piping part list
'// PipingFromToReportSample.txt             - Piping line from-to list
'// PipingPartsListReportSample.txt          - Piping Parts List
'// PipingPipeCutLengthReportSample.txt      - Piping Cut Length Report
'// PipingSpecificationStatusReportSample.txt- Piping Specification status report
'// InstrumentDiagramPartReportSample.txt    - Instrumentation list
'// HVACFromToReportSample.txt               - HVAC from-to list
'// HVACDiagramPartReportSample.txt          - HVAC Part list
'// 
'//
'//============================================================================
'// Major CATIA interface used:
'//
'// interface           VB Name         Remarks
'// -----------       ------------    --------------
'// CATIADocument       Document        access the current document
'// CATIASchWorkbench   SchWorkbench    access the Schematic workbench 
'// CATIASelection      Selection       to get the object from current selection
'//                                     in the interactive section
'// CATIAProduct        Product         to get the Product attributes and methods
'// CATIAPspAttributes  PspAttributes   to get the PlantShip object attribute
'//                                     values
'// CATIAPspAttributeReport Report      Creates a .csv or .html file
'//
'//============================================================================
'// object type         IDL-supported attributes
'// ------------        -------------------------
'//                     
'// Schematic objects   ID (Product Structure attribute)
'// (component,route..) Instance Name (Product Structure attribute)
'//                     Class (Feature attribute)
'//                     Version (Product Structure attribute)
'//                     Description (Product Structure attribute)
'//                     Part Number (Product Structure attribute)
'//                     Part Name (Product Structure attribute)
'//                     Line ID
'//                     Piping Line ID
'//                     Loop ID
'//                     Equipment Line ID
'//                     All user defined attributes.
'//
'// Logical line        From/To
'// (a schematic object)F/T Major
'//                     F/T Minor
'//                     Member
'//                     All attributes supported by Schematic objects except
'//                     LineID, Piping Line ID, Loop ID and Equipment Line ID
'//
'//============================================================================           
'//
'// Audit Trail:
'//----------------------------------------------------------------------------
'//   Author     : Vic Szeto
'//   Date       : 5/01
'//   Chg id     :
'//   Chg nature :
'//----------------------------------------------------------------------------
'//   ModIfied   : ttm
'//   Date       : 5/01
'//   Chg id     :
'//   Chg nature : Made it OS independent.  Initialized default for output file.
'//----------------------------------------------------------------------------
'//   ModIfied   : gnn
'//   Date       : 10/01
'//   Chg id     :
'//   Chg nature : Added entries for Tubing and HVAC templates.
'//----------------------------------------------------------------------------
'//   ModIfied   : agq
'//   Date       : 12/2004
'//   Chg id     :
'//   Chg nature : Use the new mechanism of generating report

'//============================================================================

Option Explicit

'******************************************************************************
'  GLOBAL variable declarations section
'******************************************************************************
'// ---------- Debug Traces
Const intG_TRACEON = 0

Dim  intGCancel                  As Integer
Dim  intGCancel2                 As Integer

'// ---------- CATIAV5 application objects
Dim objCATIAV5Document As Document
Dim objCATIAV5PspWorkbench As Workbench
Dim objCATIAV5PspAttrReport As PspAttributeReport
Dim objCATIAV5DocCollection As Documents
Dim objCATIAV5CurDocument As Document   

Dim  strGDefaultReportOutputFile As String
Dim  strGDefaultFormatFile(2) As String

Dim  strGReportOutputPath  As String
Dim  strGReportFormatFilePath As String

Dim  strGInputPanelTitle         As String
Dim  strGInputPanelPrompt As String
Dim  strGInputPanelPrompt2 As String

Const intNT   = 0
Const intUNIX = 1

Dim  intOS                  As Integer

Dim strDirectorySlashArray(2) As String
strDirectorySlashArray(intNT) = "\"
strDirectorySlashArray(intUNIX) = "/"

Dim strCATStartupPath As String
strCATStartupPath = CATIA.SystemService.Environ("CATStartupPath") 
Dim intSlashLocation As Integer
intSlashLocation  = Instr(3, strCATStartupPath, strDirectorySlashArray(intUNIX))  
       
'---------- Search for a slash 
If (intSlashLocation > 0) Then    
  '// ----------- Unix Operating System
  DbgTrace " ----------- Unix Operating System",0
  intOS = intUNIX
Else
  '// ----------- NT Operating System
  DbgTrace " ----------- NT Operating System",0
  intOS = intNT
End If


'// ----------- NT Sampel Templates
strGDefaultFormatFile(intNT) = "EquipmentAndSystems\Piping\SampleData\PipingPartsListReportSample.txt"
'strGDefaultFormatFile(intNT) = "EquipmentAndSystems\Piping\SampleData\PipingFromToReportSample.txt"
'strGDefaultFormatFile(intNT) = "EquipmentAndSystems\Piping\SampleData\PipingDiagramPartReportSample.txt"
'strGDefaultFormatFile(intNT) = "EquipmentAndSystems\Piping\SampleData\PipingPartsListReportSample.txt"
'strGDefaultFormatFile(intNT) = "EquipmentAndSystems\Piping\SampleData\PipingPipeCutLengthReportSample.txt"
'strGDefaultFormatFile(intNT) = "EquipmentAndSystems\Piping\SampleData\PipingSpecificationStatusReportSample.txt"
'strGDefaultFormatFile(intNT) = "EquipmentAndSystems\Tubing\SampleData\TubingPartsListReportSample.txt"
'strGDefaultFormatFile(intNT) = "EquipmentAndSystems\Tubing\SampleData\TubingTubeCutLengthReportSample.txt"
'strGDefaultFormatFile(intNT) = "EquipmentAndSystems\Tubing\SampleData\TubingSpecificationStatusReportSample.txt"
'strGDefaultFormatFile(intNT) = "EquipmentAndSystems\HVAC\SampleData\HVACPartsListReportSample.txt"
'strGDefaultFormatFile(intNT) = "EquipmentAndSystems\HVAC\SampleData\HVACCutLengthReportSample.txt"
'strGDefaultFormatFile(intNT) = "EquipmentAndSystems\HVAC\SampleData\HVACSpecificationStatusReportSample.txt"
'strGDefaultFormatFile(intNT) = "EquipmentAndSystems\Equipment\SampleData\EquipmentFunctionReportSample.txt"
'strGDefaultFormatFile(intNT) = "EquipmentAndSystems\Instrument\SampleData\InstrumentDiagramPartReportSample.txt"
'strGDefaultFormatFile(intNT) = "EquipmentAndSystems\HVAC\SampleData\HVACFromToReportSample.txt"
'strGDefaultFormatFile(intNT) = "EquipmentAndSystems\HVAC\SampleData\HVACDiagramPartReportSample.txt"

'// ----------- UNIX Sampel Templates
strGDefaultFormatFile(intUNIX) = "EquipmentAndSystems/Piping/SampleData/PipingPartsListReportSample.txt"
'strGDefaultFormatFile(intUNIX) = "EquipmentAndSystems/Piping/SampleData/PipingFromToReportSample.txt"
'strGDefaultFormatFile(intUNIX) = "EquipmentAndSystems/Piping/SampleData/PipingDiagramPartReportSample.txt"
'strGDefaultFormatFile(intUNIX) = "EquipmentAndSystems/Piping/SampleData/PipingPartsListReportSample.txt"
'strGDefaultFormatFile(intUNIX) = "EquipmentAndSystems/Piping/SampleData/PipingPipeCutLengthReportSample.txt"
'strGDefaultFormatFile(intUNIX) = "EquipmentAndSystems/Piping/SampleData/PipingSpecificationStatusReportSample.txt"
'strGDefaultFormatFile(intUNIX) = "EquipmentAndSystems/Tubing/SampleData/TubingPartsListReportSample.txt"
'strGDefaultFormatFile(intUNIX) = "EquipmentAndSystems/Tubing/SampleData/TubingTubeCutLengthReportSample.txt"
'strGDefaultFormatFile(intUNIX) = "EquipmentAndSystems/Tubing/SampleData/TubingSpecificationStatusReportSample.txt"
'strGDefaultFormatFile(intUNIX) = "EquipmentAndSystems/HVAC/SampleData/HVACPartsListReportSample.txt"
'strGDefaultFormatFile(intUNIX) = "EquipmentAndSystems/HVAC/SampleData/HVACCutLengthReportSample.txt"
'strGDefaultFormatFile(intUNIX) = "EquipmentAndSystems/HVAC/SampleData/HVACSpecificationStatusReportSample.txt"
'strGDefaultFormatFile(intUNIX) = "EquipmentAndSystems/Equipment/SampleData/EquipmentFunctionReportSample.txt"
'strGDefaultFormatFile(intUNIX) = "EquipmentAndSystems/Instrument/SampleData/InstrumentDiagramPartReportSample.txt"
'strGDefaultFormatFile(intUNIX) = "EquipmentAndSystems/HVAC/SampleData/HVACFromToReportSample.txt"
'strGDefaultFormatFile(intUNIX) = "EquipmentAndSystems/HVAC/SampleData/HVACDiagramPartReportSample.txt"

strGDefaultReportOutputFile = "CATSchAttrReport.html"

strGInputPanelTitle = "Report Generation"

strGInputPanelPrompt = "Define report format file"
strGInputPanelPrompt2 = "Define report output file"


'------------------------------------------------------------------------------
Sub DbgTrace (iStrMsgString, iIntYesError)
'------------------------------------------------------------------------------
If (intG_TRACEON = 1) Then
  CATIA.SystemService.Print iStrMsgString 
  If (iIntYesError = 1) Then 
    CATIA.SystemService.Print "Err Number = " & Err.Number
  End If
End If

End Sub '//////////////////////////////////////////////////////////// DbgTrace


'------------------------------------------------------------------------------
Sub StartCATIAV5 ()
'------------------------------------------------------------------------------

  Set objCATIAV5Document = CATIA.ActiveDocument
  DbgTrace "V5: Active Document",1

  '//---------- Get Schematic from current document
  'Set objCATIAV5PspWorkbench = objCATIAV5Document.GetWorkbench("PspWorkbench") 
  'DbgTrace "V5: GetWorkbench",1

  Dim objProductRoot As Product

  Set objProductRoot = CATIA.ActiveDocument.Product

  Set objCATIAV5PspWorkbench = objProductRoot.GetTechnologicalObject ("PspWorkbench")

  If ( objCATIAV5PspWorkbench Is Nothing  ) Then
     CATIA.SystemService.Print "Unable to get objCATIAV5PspWorkbench"          
  Else
     CATIA.SystemService.Print "Success in get objCATIAV5PspWorkbench"          
  End If

  Set objCATIAV5PspAttrReport = objCATIAV5PspWorkbench.GetInterface ("CATIAPspAttributeReport",objCATIAV5Document)
  DbgTrace "V5: FindInterface",1

  Dim Ret
  Ret = objCATIAV5PspAttrReport.GenerateReport(strGReportFormatFilePath,strGReportOutputPath)

  Dim nBtn

  If ( Ret = 0 ) Then
     nBtn = MsgBox("Report Generation Complete",0,strGInputPanelTitle)
  Else
     nBtn = MsgBox("Report Generation Failed",0,strGInputPanelTitle) 
  End If
  
End Sub '/////////////////////////////////////////////////////////// StartCATIAV5

'------------------------------------------------------------------------------
Sub GetFormatFile ()
'------------------------------------------------------------------------------
  Dim strCATStartupPath As String
  Dim ApplIfileSys      As Object
  Dim strDefaultLocation As String
  Dim intSemiColonLocation As Integer
  Dim strDelimeterArray(2) As String
  strDelimeterArray(intNT) = ";"
  strDelimeterArray(intUNIX) = ":"

  strCATStartupPath = CATIA.SystemService.Environ("CATStartupPath") 
  Set ApplIfileSys = CATIA.FileSystem 

  DbgTrace "Path for template = " & strCATStartupPath ,0

  intSemiColonLocation  = Instr(3, strCATStartupPath, strDelimeterArray(intOS))  
       
  '---------- Search for template in the CNext run time environment 
  If (intSemiColonLocation > 0) Then    
    Do While (intSemiColonLocation > 0)
       intSemiColonLocation = intSemiColonLocation -1
       strDefaultLocation = Left(strCATStartupPath, intSemiColonLocation) _
         + strDirectorySlashArray(intOS) + strGDefaultFormatFile(intOS)
	   DbgTrace "Try location " & strDefaultLocation ,0
       If (ApplIfileSys.FileExists(strDefaultLocation)) Then
		  DbgTrace "Found in : " & strDefaultLocation,0
		  strGReportFormatFilePath = strDefaultLocation
          Exit Do 
       End If

       Err.Clear
       intSemiColonLocation =  intSemiColonLocation + 2
       strCATStartupPath = Mid(strCATStartupPath, intSemiColonLocation)
       intSemiColonLocation  = Instr(3, strCATStartupPath, strDelimeterArray(intOS))
    Loop     ' Exit outer loop immediately.
  '---------- when there is no path concatenation, there is no ";" 
  Else
    If (Len (strCATStartupPath) > 0) Then
       strGReportFormatFilePath = strCATStartupPath + strDirectorySlashArray(intOS) + strGDefaultFormatFile(intOS)
    End If
  End If   

  '---------- Confirm by the user
  '---------- loop until found
  Dim Found 
  Found = 0
  Do While (Found = 0)
    '---------- InputBox returns the string when user press the Enter/OK key
	'---------- and returns null string when user press Cancel
    strGReportFormatFilePath = InputBox(strGInputPanelPrompt,strGInputPanelTitle,strGReportFormatFilePath)
    If ( Len (strGReportFormatFilePath) = 0) Then
      intGCancel = 1
      Exit Do
    End If
    If (ApplIfileSys.FileExists(strGReportFormatFilePath)) Then Found = 1
  Loop
  DbgTrace "Input Template file = " & strGReportFormatFilePath ,0

  '---------- No need to display error message, the above loop will not
  '---------- be exited until a valid template is found or Cancel is selected
  'If (Not(AppliFileSys.FileExists(strGReportFormatFilePath))) Then
     'Dim strMessage
     'strMessage  = "Error Report Format File:" + strGReportFormatFilePath + "not found"
     'msgbox (strMessage)    
     'Exit Sub
  'End If


End Sub '/////////////////////////////////////////////// GetPath


'------------------------------------------------------------------------------
Sub GetReportFile ()
'------------------------------------------------------------------------------
  Dim strCATTempPath As String

  Dim ApplIfileSys      As Object
  Set ApplIfileSys = CATIA.FileSystem 

  '// Find a temp directory to place the output file
  strCATTempPath    = CATIA.SystemService.Environ("CATTemp") 
  DbgTrace "CATTemp path = " & strCATTempPath ,0

  strGReportOutputPath = strCATTempPath + strDirectorySlashArray(intOS) + "OutputFile.html"

    '---------- InputBox returns the string when user press the Enter/OK key
	'---------- and returns null string when user press Cancel
    strGReportOutputPath = InputBox(strGInputPanelPrompt2, strGInputPanelTitle, strGReportOutputPath)

    If ( Len (strGReportOutputPath) = 0) Then
      intGCancel2 = 1
    End If

  '---------- No need to display error message, the above loop will not
  '---------- be exited until a valid template is found or Cancel is selected
  If ( intGCancel = 0 And AppliFileSys.FileExists(strGReportOutputPath)) Then
     Dim strMessage
	 Dim nBtn

     strMessage  = "Report File: " + strGReportOutputPath + " exists, Overwrite?"
	 ' Yes/No button = 4, vBQuestion 32
     nBtn = MsgBox(strMessage,36,strGInputPanelTitle)
	 If ( nBtn = 7 ) Then
	   intGCancel2 = 1
	 End If
     Exit Sub
  End If


End Sub '/////////////////////////////////////////////// 


'====================================================================================
Sub CATMain ()
'====================================================================================
intGCancel = 0
intGCancel2 = 0

GetFormatFile
If (intGCancel = 0) Then

	GetReportFile

	If ( intGCancel2 = 0 ) Then

		StartCATIAV5
    End If
End If

End Sub '/////////////////////////////////////////////////////////// CATMain
