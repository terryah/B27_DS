'// COPYRIGHT DASSAULT SYSTEMES  2005
'//============================================================================
'//
'// CablePullListReport_AllKeyStations.CATScript
'//
'// Language="VBSCRIPT"
'//
'//============================================================================
'// Major CATIA interface used:
'//
'// interface                   VB Name         Remarks
'// -----------                 ------------    --------------
'// CATIADocument               Document        access the current document
'// CATIAEdbWorkbench           EdbWorkbench    access the ECDB workbench
'//
'//============================================================================

Option Explicit

'******************************************************************************
'  GLOBAL variable declarations section
'******************************************************************************
'// ---------- Debug Traces
Const intG_TRACEON = 0

'// ---------- CATIAV5 application objects
Dim objGV5Document As Document
Dim objGV5EdbWkb As EdbWorkbench

Dim strGReportOutputPath  As String
Dim intGCancel As Integer

Const intNT   = 0
Const intUNIX = 1

Dim  intOS                  As Integer

Dim strDirSlash(2) As String
strDirSlash(intNT) = "\"
strDirSlash(intUNIX) = "/"

Dim strCATStartupPath As String
strCATStartupPath = CATIA.SystemService.Environ("CATStartupPath") 
Dim intSlashLocation As Integer
intSlashLocation  = Instr(3, strCATStartupPath, strDirSlash(intUNIX))  
       
'---------- Search for a slash 
If (intSlashLocation > 0) Then    
  '// ----------- Unix Operating System
  DbgTrace " ----------- Unix Operating System",0
  intOS = intUNIX
Else
  '// ----------- NT Operating System
  DbgTrace " ----------- NT Operating System",0
  intOS = intNT
End If

Dim strGDefaultReportName As String
Dim strGInputPanelTitle As String
Dim strGInputPanelPrompt As String
                        
strGDefaultReportName = "CablePullListReport_AllKeyStations.xls"
strGInputPanelTitle = "Report Generation"
strGInputPanelPrompt = "Define report output file"

'------------------------------------------------------------------------------
Sub DbgTrace (iStrMsgString, iIntYesError)
'------------------------------------------------------------------------------
If (intG_TRACEON = 1) Then
  CATIA.SystemService.Print iStrMsgString 
  If (iIntYesError = 1) Then 
    CATIA.SystemService.Print "Err Number = " & Err.Number
  End If
End If

End Sub '//////////////////////////////////////////////////////////// DbgTrace


'------------------------------------------------------------------------------
Sub StartCATIAV5 ()
'------------------------------------------------------------------------------

  Set objGV5Document = CATIA.ActiveDocument
  DbgTrace "V5: Active Document",1

  '//---------- Get EDB workbench from current document
  Set objGV5EdbWkb  = objGV5Document.GetWorkbench("EdbWorkbench") 
  DbgTrace "V5: GetWorkbench",1
 
End Sub '/////////////////////////////////////////////////////////// StartCATIAV5


'------------------------------------------------------------------------------
Sub GetReportFile ()
'------------------------------------------------------------------------------
  Dim strCATTempPath As String

  Dim ApplIfileSys      As Object
  Set ApplIfileSys = CATIA.FileSystem 

  '// Find a temp directory to place the output file
  '//strCATTempPath    = CATIA.SystemService.Environ("CATTemp") 
  '//DbgTrace "CATTemp path = " & strCATTempPath ,0

  strCATTempPath = "."
  strGReportOutputPath = strCATTempPath + strDirSlash(intOS) +  _
    strGDefaultReportName

  '---------- InputBox returns the string when user press the Enter/OK key
	'---------- and returns null string when user press Cancel
    strGReportOutputPath = InputBox (strGInputPanelPrompt, _
      strGInputPanelTitle, strGReportOutputPath)

    If ( Len (strGReportOutputPath) = 0) Then
      intGCancel = 1
    End If

  '---------- No need to display error message, the above loop will not
  '---------- be exited until a valid template is found or Cancel is selected
  If ( intGCancel = 0 And AppliFileSys.FileExists(strGReportOutputPath)) Then
     Dim strMessage
	   Dim nBtn

     strMessage  = "Report File: " + strGReportOutputPath + " exists, Overwrite?"
 	 ' Yes/No button = 4, vBQuestion 32
     nBtn = MsgBox(strMessage,36,strGInputPanelTitle)
  	 If ( nBtn = 7 ) Then
	     intGCancel = 1
	   End If
     Exit Sub
  End If
End Sub '/////////////////////////////////////////////// 


'====================================================================================
Sub CATMain ()
'====================================================================================
intGCancel = 0


GetReportFile

If ( intGCancel = 0 ) Then
	StartCATIAV5

  If ( objGV5EdbWkb  Is Nothing  ) Then
     CATIA.SystemService.Print "Unable to get EDB workbench"          
  Else
     CATIA.SystemService.Print "EDB workbench ------------------------------"  

     '---- Generate Report ----
     On Error Resume Next      ' Disable automatic error handling
     
     Dim strErrorMsg
     objGV5EdbWkb.GenerateReport  "CablePullListReport_AllKeyStations", strGReportOutputPath, strErrorMsg 

     If (Len (strErrorMsg) <> 0) Then
        MsgBox  strErrorMsg, 16, "Report Generation: ERROR"
     End If 
     '---- End of Generate Report ----
  End If

End If

End Sub '/////////////////////////////////////////////////////////// CATMain
