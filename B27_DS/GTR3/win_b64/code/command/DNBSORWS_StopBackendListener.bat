@Echo off
set sorPATH=%SOR_INSTALL_PATH%
set sorJH=%SOR_JAVA_HOME%

::This batch file is to Stop the Listener
set CLASSPATH=%sorPATH%\intel_a\docs\javaserver\DNBSORListener.jar;%sorPATH%\intel_a\docs\javaserver\DNBSORJNI.jar;%sorPATH%\intel_a\docs\javaserver\dnbsorwebservices_ws.jar;%sorPATH%\intel_a\docs\javaserver\DNBSORWebServicesImpl_ws.jar;%CLASSPATH%

set PATH=%sorJH%\bin;%sorPATH%\intel_a\code\bin;%PATH%

echo "java com.dassault_systemes.dnbsorbase.dnbsorlistener.DNBSORSocketListener" > NUL

java com.dassault_systemes.dnbsorbase.dnbsorlistener.DNBSORSocketListener STOP

