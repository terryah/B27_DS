@Echo off
set sorPATH=%SOR_INSTALL_PATH%
set sorJH=%SOR_JAVA_HOME%

::This batch file is to Start the Listener
set CLASSPATH=%sorPATH%\intel_a\docs\javaserver\DNBSORListener.jar;%sorPATH%\intel_a\docs\javaserver\DNBSORJNI.jar;%sorPATH%\intel_a\docs\javaserver\dnbsorwebservices_ws.jar;%sorPATH%\intel_a\docs\javaserver\DNBSORWebServicesImpl_ws.jar;%CLASSPATH%

set PATH=%sorJH%\bin;%sorPATH%\intel_a\code\bin;%PATH%

echo "java com.dassault_systemes.dnbsorbase.dnbsorlistener.DNBSORSocketListener" > NUL
java com.dassault_systemes.dnbsorbase.dnbsorlistener.DNBSORSocketListener START

::If you want to test JNIObject you can comment above line and uncomment below line and give proper xml file as second argument at the end.
::java com.dassault_systemes.dnbsorbase.dnbsorjni.IDNBSORJNI GenerateSOI D:\SORWS_Test_Folder\CreateSOISOISOI_001.xml
