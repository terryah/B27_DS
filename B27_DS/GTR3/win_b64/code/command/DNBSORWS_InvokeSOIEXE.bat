::There are 6 Arguments in this Batch file
::1. \....\DNBSORSOI.exe. You dont need to change this argument provided "SOR_INSTALL_PATH" set on MyComputer. Mostly it is a path of installation of Delmia in your machine.
::2. -env                 You dont need to change this argument.
::3. LocalEnv             This is the env file name. You need to give appropriate name for which your exe is set.
::4. -direnv.             You need to give the path of the directory in which above environment file resides.
::5. -xml                 You dont need to change this argument.
::6. %1                   You dont need to change this argument.

set sorPATH=%SOR_INSTALL_PATH%

"%sorPATH%\intel_a\code\bin\DNBSORSOI.exe" -env LocalEnv -direnv "C:\Documents and Settings\pcsuser\Application Data\DassaultSystemes\CATEnv" -xml %1
