<?xml version="1.0" encoding='utf-8' ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html"/>

  <xsl:template match="/">

    <body>
      <table width="95%" border="1" bordercolor="red" align="center">
        <!-- start of GLOBAL TABLE -->
        <th bgcolor="#9acd32">Work Instructions</th>
        <th bgcolor="#9acd32">List of Hyperlinks</th>
        <tr>
          <td width="60%">
            <!-- ##########################################################  -->
            <table>
              <xsl:for-each select="WorkInstructionNode/WorkInstruction/FixedInstruction">
                <tr>
                  <td>
                    <xsl:value-of select="current()"/>
                  </td>
                </tr>
              </xsl:for-each>
              <xsl:for-each select="WorkInstructionNode/WorkInstruction/VariableInstruction">
                <tr>
                  <td>
                    <xsl:value-of select="current()"/>
                  </td>
                </tr>
              </xsl:for-each>
            </table>
            <!-- ##########################################################  -->
          </td>
          <td width="40%">
            <!-- ##########################################################  -->
            <table border="1" width="100%" bgcolor="#cfffff" bordercolor="#9acd32">
              <!-- start of table of hyper links -->
              <!--
      <tr>
        <th>description</th>
        <th>hyperlink</th>
      </tr>
          -->

              <xsl:for-each select="WorkInstructionNode/WorkInstruction/links/link">
                <tr>
                  <td>
                    <xsl:value-of select="@description"/>
                  </td>
                  <td>
                    <a>
                      <xsl:attribute name="href">
                        <xsl:value-of select="@url"/>
                      </xsl:attribute>
                      <xsl:attribute name="target">_blank</xsl:attribute>
                      <xsl:value-of select="@url"/>
                    </a>
                  </td>
                </tr>
              </xsl:for-each>
            </table>
            <!-- end of table of hyper links -->
            <!-- ##########################################################  -->
          </td>
        </tr>
        <th bgcolor="#9acd32" colspan="2"> List of Parameters </th>
        <tr>
          <td colspan="2">
            <!-- ##########################################################  -->
            <table border="1">
              <!-- start of table of params -->
              <xsl:for-each select="WorkInstructionNode/WorkInstruction/parameters/parameter">
                <tr>
                  <td>
                    <xsl:value-of select="@name"/>
                  </td>
                  <td>
                    <form name="@name">
                      <input type="text" size="20" name="@name"/>
                    </form>
                  </td>
                </tr>
              </xsl:for-each>
            </table>
            <!-- end of table of params -->
            <!-- ##########################################################  -->
          </td>
        </tr>
      </table>
      <!-- end of GLOBAL TABLE -->
      <!-- ##########################################################  -->
    </body>

  </xsl:template>

</xsl:stylesheet>


