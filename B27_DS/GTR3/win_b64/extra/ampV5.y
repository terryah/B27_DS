/******************* ampV5.y ************/
/* Jul 06 pty Creation			*/
/****************************************/
%skeleton "lalr1.cc"
%define "parser_class_name" "DNBAMPParser"
%define "filename_type" "scl_string"
%defines
%output="DNBAMPParser.cpp"
%{
/* 
 * @quickreview PTY CQE 06:08:16
 * @quickreview PTY CQE 06:08:09
 * @fullreview PTY CQE 06:08:01
 */
// LocalInterfaces
#include "DNBAMPInterpreter.h"
// System
#include "CATUnicodeString.h"
#include "CATString.h"
#include "CATListOfCATUnicodeString.h"
#include "CATListOfDouble.h"
#include "CATListOfInt.h"
union YYSTYPE;

// Declration for Flex scanner
#define YY_DECL \
int yylex(YYSTYPE *yylval, location *yylloc )
// Declration for Parser
YY_DECL;

static int min_check = -1;
static int max_check = -1;
static CATListValCATUnicodeString stringList;
static CATListOfDouble numList;
static CATListOfInt stringOrNum;

%}
%parse-param { DNBAMPInterpreter& iInterpreter }
%parse-param { CATString& errBuff }
%locations
%union {
	int tok;
	int integer;
	double real;
	CATString* identifier;
	CATUnicodeString* text;
	}

%token	<tok>		HEADER END COMMENT  DEFAULTS USER PRIMITIVE SEAM
%token	<tok>		A_FIXED A_FREE GLOBAL MIN MAX LABEL LIST
%token	<tok>		UNITS UNITLESS ANGLE LENGTH PERCENT PROMPT
%token	<tok>		SKIP DIAGRAM METHOD FOR BY TO DO EQ UPDATE
%token	<tok>		CHECK LIMITS GOTO COLLISIONS GRAPHICS
%token	<tok>		INIT DEFAULT CONFIRMATION ABORT CURRENT AMP_IGNORE_1
%token	<tok>		PAGE USER_STRING ROBOT TORCH GANTRY INCLUDE
%token	<tok>		USER_REAL USER_INTEGER
%token  <identifier>  	NAME EXPR SURF_EXPR
%token 	<text>		STRING TWO_HYPHENS_COMMENT NL_WS
%token	<real>		REAL
%token	<integer>	INTEGER ON OFF

%type	<inst>		stmt
%type	<text>		label prompt diagram name
%type	<real>		num min max mn mx by
%type	<integer>	array units user
%destructor { delete $$; } label prompt diagram name STRING NAME EXPR TWO_HYPHENS_COMMENT NL_WS
%start  list
%%

list:
	| list NL_WS
	    {
	    iInterpreter.ProcessNewLineWS( *($2), errBuff);
	    delete $2 ;
	    }
	| list stmt NL_WS
	    {
	    iInterpreter.ProcessNewLineWS( *($3), errBuff);
	    delete $3 ;
	    }
	| list error NL_WS
	    {
	    iInterpreter.ProcessNewLineWS( *($3), errBuff);
	    delete $3 ;
	    
		if( errBuff.IsNull() )
			errBuff = "Syntax Error";
			
		@1.step();
		error( @1, errBuff);
		YYERROR;
		//return (1);
	    }
	;
stmt:	HEADER STRING
	    {
		CATBoolean result = FALSE;
		result = iInterpreter.ProcessHeader( *($2), errBuff);
		delete $2 ;
		if( !result )
		{
		    error( @$, errBuff );
		    YYERROR;
		    //return !result;
		}
	    }
	| GLOBAL
	    {
		CATBoolean result = FALSE;
		result = iInterpreter.ProcessGlobals( errBuff );
		if( !result )
		{
		    error( @$, errBuff );
		    YYERROR;
		    //return !result;
		}
	    }
	| COMMENT STRING 
	    {
		CATBoolean result = FALSE;
		result = iInterpreter.ProcessComment( *($2), errBuff );
		delete $2 ;
		if( !result )
		{
		    error( @$, errBuff );
		    YYERROR;
		    //return !result;
		}
	    }
	| COMMENT 
	    {
		CATBoolean result = FALSE;
		result = iInterpreter.ProcessComment( CATUnicodeString(""), errBuff );
		if( !result ) 
		{
		    error( @$, errBuff );
		    YYERROR;
		    //return !result;
		}
	    }
	| TWO_HYPHENS_COMMENT
		{
		CATBoolean result = FALSE;
		result = iInterpreter.ProcessTwoHyphensComment( *($1), errBuff );
		delete $1 ;
		if( !result ) 
		{
		    error( @$, errBuff );
		    YYERROR;
		    //return !result;
		}
	    }
	| DIAGRAM STRING
	    {
		CATBoolean result = FALSE;
		result = iInterpreter.ProcessDiagram( *($2), errBuff );
		delete $2 ;
		if( !result )
		{
		    error( @$, errBuff );
		    YYERROR;
		    //return !result;
		}
	    }
	| DEFAULTS
	    {
		CATBoolean result = FALSE;
		result = iInterpreter.ProcessState( DEFAULTS, errBuff );
		if( !result )
		{
		    error( @$, errBuff );
		    YYERROR;
		    //return !result;
		}
	    }
	| END DEFAULTS
	    {
		CATBoolean result = FALSE;
		result = iInterpreter.ProcessDoEnd( DEFAULTS, errBuff );
		if( !result )
		{
		    error( @$, errBuff );
		    YYERROR;
		    //return !result;
		}
	    }
	| END
	    {
		CATBoolean result = FALSE;
		result = iInterpreter.ProcessDoEnd( HEADER, errBuff );
		if( !result )
		{
		    error( @$, errBuff );
		    YYERROR;
		    //return !result;
		}
	    }
	| SEAM
	    {
		CATBoolean result = FALSE;
		result = iInterpreter.ProcessInitializeSeam( -1, errBuff );
		if( !result )
		{
		    error( @$, errBuff );
		    YYERROR;
		    //return !result;
		}
	    }
	| SEAM INTEGER
	    {
		CATBoolean result = FALSE;
		result = iInterpreter.ProcessInitializeSeam( $2, errBuff );
		if( !result )
		{
		    error( @$, errBuff );
		    YYERROR;
		    //return !result;
		}
	    }
	| END SEAM
	    {
		CATBoolean result = FALSE;
		result = iInterpreter.ProcessDoEnd( SEAM, errBuff );
		if( !result )
		{
		    error( @$, errBuff );
		    YYERROR;
		    //return !result;
		}
	    }
	| NAME A_FREE num units min max label prompt user diagram
	    {
		CATBoolean result = FALSE;
		result = iInterpreter.ProcessSeamVariable( *($1), A_FREE, $3, $4, min_check, $5, max_check, $6, $7, $8, $9, $10, errBuff ) ;
		delete $1 ;
		delete $7 ;
		delete $8 ;
		delete $10 ;
		if( !result )
		{
		    error( @$, errBuff );
		    YYERROR;
		    //return !result;
		}
	    }
	| NAME A_FIXED num units min max label prompt user diagram
	    {
		CATBoolean result = FALSE;
		result = iInterpreter.ProcessSeamVariable( *($1), A_FIXED, $3, $4, min_check, $5, max_check, $6, $7, $8, $9, $10, errBuff ) ;
		delete $1 ;
		delete $7 ;
		delete $8 ;
		delete $10 ;
		if( !result )
		{
		    error( @$, errBuff );
		    YYERROR;
		    //return !result;
		}
	    }
	| NAME num prompt diagram
	    {
		CATBoolean result = FALSE;
		result = iInterpreter.ProcessSeamVariable( *($1), $2, $3, $4, errBuff);
		delete $1 ;
		delete $3 ;
		delete $4 ;
		if( !result )
		{
		    error( @$, errBuff );
		    YYERROR;
		    //return !result;
		}
	    }
	| NAME A_FREE STRING label prompt user diagram
	    {
		CATBoolean result;
		result = iInterpreter.ProcessSeamVariable( *($1), A_FREE, *($3), $4, $5, $6, $7, errBuff );
		delete $1 ;
		delete $3 ;
		delete $4 ;
		delete $5 ;
		delete $7 ;
		if( !result )
		{
		    error( @$, errBuff );
		    YYERROR;
		    //return !result;
		}
	    }
	| NAME A_FIXED STRING prompt user diagram
	    {
		CATBoolean result;
		result = iInterpreter.ProcessSeamVariable( *($1), A_FIXED, *($3), $4, $5, $6, errBuff );
		delete $1 ;
		delete $3 ;
		delete $4 ;
		delete $6 ;
		if( !result )
		{
		    error( @$, errBuff );
		    YYERROR;
		    //return !result;
		}
	    }
	| NAME LIST STRING user
	    {
		CATBoolean result;
		result = iInterpreter.ProcessListVariable( *($1), *($3), $4, errBuff);
		delete $1 ;
		delete $3 ;
		if( !result )
		{
		    error( @$, errBuff );
		    YYERROR;
		    //return !result;
		}
	    }
	| NAME EXPR
	    {
		CATBoolean result;
		result = iInterpreter.ProcessExpression( *($1), *($2), errBuff);
		delete $1 ;
		delete $2 ;
		if( !result )
		{
		    error( @$, errBuff );
		    YYERROR;
		    //return !result;
		}
	    }
	| SURF_EXPR
	    {
		CATBoolean result;
		result = iInterpreter.ProcessSurfaceExpression( *($1), errBuff);
		delete $1 ;
		if( !result )
		{
		    error( @$, errBuff );
		    YYERROR;
		    //return !result;
		}
	    }
	| NAME '{' num num num '}'
	    {
		double vector[3];
		vector[0] = $3;
		vector[1] = $4;
		vector[2] = $5;
		CATBoolean result;
		result = iInterpreter.ProcessVector( *($1), $3, $4, $5, errBuff );
		delete $1 ;
		if( !result )
		{
		    error( @$, errBuff );
		    YYERROR;
		    //return !result;
		}
	    }
	| NAME A_FREE '{' num num num '}'
	    {
		double vector[3];
		vector[0] = $4;
		vector[1] = $5;
		vector[2] = $6;
		CATBoolean result;
		result = iInterpreter.ProcessVector( *($1), A_FREE, $4, $5, $6, errBuff );
		delete $1 ;
		if( !result )
		{
		    error( @$, errBuff );
		    YYERROR;
		    //return !result;
		}
	    }
	| NAME PROMPT STRING diagram
	    {	
		CATBoolean result;
		result = iInterpreter.ProcessSeamVariable( *($1), *($3), $4, errBuff );
		delete $1 ;
		delete $3 ;
		delete $4 ;
		if( !result )
		{
		    error( @$, errBuff );
		    YYERROR;
		    //return !result;
		}    
	    }
	| NAME DIAGRAM STRING
	    {
		CATBoolean result;
		result = iInterpreter.ProcessSeamVariable( *($1), *($3), errBuff );
		delete $1 ;
		delete $3 ;
		if( !result )
		{
		    error( @$, errBuff );
		    YYERROR;
		    //return !result;
		}    
	    }
	| NAME NAME prompt user diagram
	    {
		CATBoolean result;
		CATUnicodeString *pValue = new CATUnicodeString(*($2));
		result = iInterpreter.ProcessSeamVariable( *($1), *($2), $3, $4, $5, errBuff );
		delete pValue;
		pValue = NULL;
		delete $1 ;
		delete $2 ;
		delete $3 ;
		delete $5 ;
		if( !result )
		{
		    error( @$, errBuff );
		    YYERROR;
		    //return !result;
		}     
	    }
	| NAME STRING prompt user diagram
	    {
		CATBoolean result;
		result = iInterpreter.ProcessSeamVariable( *($1), *($2), $3, $4, $5, errBuff );
		delete $1 ;
		delete $2 ;
		delete $3 ;
		delete $5 ;
		if( !result )
		{
		    error( @$, errBuff );
		    YYERROR;
		    //return !result;
		}    
	    }
	| NAME GLOBAL
	    {
		CATBoolean result;
		result = iInterpreter.ProcessGlobals( *($1), errBuff );
		delete $1 ;
		if( !result )
		{
		    error( @$, errBuff );
		    YYERROR;
		    //return !result;
		}
	    }
	| NAME '(' array ')'
	    {
		CATBoolean result;
		result = iInterpreter.ProcessArray( *($1), stringList, numList, stringOrNum, errBuff );
		stringList.RemoveAll();
		numList.RemoveAll();
		stringOrNum.RemoveAll() ;
		delete $1 ;
		if( !result )
		{
		    error( @$, errBuff );
		    YYERROR;
		}
	    }
	| NAME A_FREE '(' array ')' units min max label
	    {
		CATBoolean result;
		result = iInterpreter.ProcessArray( *($1), A_FREE, $6, $7, $8, $9, stringList, numList, stringOrNum, max_check, min_check, errBuff );
		stringList.RemoveAll();
		numList.RemoveAll();
		stringOrNum.RemoveAll() ;
		delete $1 ;
		delete $9 ;
		if( !result )
		{
		    error( @$, errBuff );
		    YYERROR;
		}
	    }
	| NAME A_FIXED '(' array ')'
	    {
		CATBoolean result;
		result = iInterpreter.ProcessArray( *($1), A_FIXED, stringList, numList, stringOrNum, errBuff );
		stringList.RemoveAll();
		numList.RemoveAll();
		stringOrNum.RemoveAll() ;
		delete $1 ;
		if( !result )
		{
		    error( @$, errBuff );
		    YYERROR;
		}
	    }
	 | stmt TWO_HYPHENS_COMMENT
		{
		CATBoolean result = FALSE;
		result = iInterpreter.ProcessTwoHyphensComment( *($2), errBuff );
		delete $2 ;
		if( !result ) 
		{
		    error( @$, errBuff );
		    YYERROR;
		    //return !result;
		}
	    }
user:
	{
	$$ = 0;
	}
	| USER USER_REAL
	{
	$$ = 2;
	}
	| USER USER_INTEGER
	{
	$$ = 1;
	}
	| USER LIST
	{
	$$ = 3;
	}
	| USER USER_STRING
	{
	$$ = 4;
	}
	| USER
	{
	$$ = 2;
	}

units:
	{
	$$ = -1;
	}
	| UNITS UNITLESS
	{
	$$ = 1;
	}
	| UNITS ANGLE
	{
	$$ = 2;
	}
	| UNITS LENGTH
	{
	$$ = 0;
	}
	| UNITS PERCENT
	{
	$$ = 6;
	}
	;

name:	INTEGER
	{
	    $$ = new CATUnicodeString;
	    $$->BuildFromNum( $1 );		
	}
	| NAME
	{
	    $$ = new CATUnicodeString( *($1) );
		delete $1 ;
	}
	;

by:	{
	$$ = 1.0;
	}
	| BY num
	{
	$$ = $2;
	}
	;

mn:	num
	{
	$$ = $1;
	min_check = 0;
	}
	| MIN 
	{
	$$ = 0.0;
	min_check = 1;
	}
	| INIT 
	{
	$$ = 0.0;
	min_check = 2;
	}
	| DEFAULT 
	{
	$$ = 0.0;
	min_check = 3;
	}
	| CURRENT 
	{
	$$ = 0.0;
	min_check = 4;
	}
	;

mx:	num
	{
	$$ = $1;
	max_check = 0;
	}
	| MAX 
	{
	$$ = 0.0;
	max_check = 1;
	}
	| INIT 
	{
	$$ = 0.0;
	max_check = 2;
	}
	| DEFAULT 
	{
	$$ = 0.0;
	max_check = 3;
	}
	;

min:
	{
	min_check = FALSE;
	$$ = 0.0;
	}
	| MIN num
	{
	min_check = TRUE;
	$$ = $2;
	}
	;

max:
	{
	max_check = FALSE;
	$$ = 0.0;
	}
	| MAX num
	{
	max_check = TRUE;
	$$ = $2;
	}
	;

label:
	{
	    $$ = NULL;
	}
	| LABEL STRING
	{
	    $$ = new CATUnicodeString( *($2) );		
		delete $2 ;
	}
	;

prompt:
	{
	    $$ = NULL;
	}
	| PROMPT STRING
	{
	    $$ = new CATUnicodeString( *($2) );		
		delete $2 ;
	}
	;

diagram:
	{
	    $$ = NULL;
	}
	| DIAGRAM STRING
	{
	    $$ = new CATUnicodeString( *($2) );		
		delete $2 ;
	}
	;


num:	INTEGER
	{
	$$ = $1;
        }
	| REAL
	{
        }
	;

array:	num
	{
	    stringList.Append( CATUnicodeString("") );
	    numList.Append( $1 );
		stringOrNum.Append(1) ;
	//Array[0] = $1;
	//TArray[0] = NULL;
	//$$ = 1;
        }
	| STRING
	{
	    stringList.Append( *($1) );
		delete $1 ;
	    numList.Append( 0.0 );
		stringOrNum.Append(2) ;
	//Array[0] = 0;
	//TArray[0] = strass( $1 );
	//$$ = 1;
        }
	| array num
	{
	    stringList.Append( CATUnicodeString("") );
	    numList.Append( $2 );
		stringOrNum.Append(1) ;
	//Array[$$] = $2;
	//TArray[$$] = NULL;
	//$$ = $$ + 1;
        }
	| array STRING
	{
	    stringList.Append( *($2) );
		delete $2 ;
	    numList.Append( 0.0 );
		stringOrNum.Append(2) ;
	//Array[$$] = 0;
	//TArray[$$] = strass( $2 );
	//$$ = $$ + 1;
        }
	;

%%
void DNBAMPParser::error( const location_type& iString, CATString& errorMsg )
{
    CATString errMessage = "Error Location: ";
    errMessage << iString;
    errMessage += "\n";
    errMessage += " Reason: ";
    errMessage += errorMsg;
    errorMsg = errMessage;
}

void DNBAMPParser::error( const location_type& iLoc, const scl_string& errMsg )
{
}

