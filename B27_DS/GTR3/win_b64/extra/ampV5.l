%{
// LocalInterfaces
#include "DNBAMPParser.hpp"
// System
#include "CATString.h"
#include "CATUnicodeString.h"
#include "CATUnicodeChar.h"
#include "CATStdIO.h"
#include "CATMath.h"
#include "CATPower.h"

CATUnicodeString numConvertor("");
int DNBAMPLookup( const CATString& iString, YYSTYPE *yylval );
int amplineno = 0;
unsigned int yyinFile = 0;
void ReadFromFile( char *buf, int& oResult, int MaxSize );
void LEXConvertStrToNum( const CATUnicodeString& numStr, double* oNum ) ;
#define YY_NEVER_INTERACTIVE 1
#define YY_INPUT(buf, result, max_size ) \
    ReadFromFile( buf, result, max_size )
%}

%option noyywrap nounput

ws		[ \t]+
integer		[+-]?[0-9]+
areal		([+-]?[0-9]*[\.\,][0-9]+)
breal		([+-]?[0-9]+[\.\,][0-9]*)
ereal		([+-]?[0-9]*[\.\,][0-9]+[eEdD][+-]?[0-9]+)
dreal		([+-]?[0-9]+[\.\,][0-9]*[eEdD][+-]?[0-9]+)
real		{areal}|{breal}|{ereal}|{dreal}
comment		\-\-.*
string		\"[^"^\r^\n]*\"
eq		\:=
expr		\=[^=].*
surf_expr	[A-Z]+[a-zA-Z]+[[:blank:]]*@[[:blank:]]*[A-Z]+[a-zA-Z]+[[:blank:]]*\[[[:blank:]]*[0-9]+[[:blank:]]*\]
nl_WS		\r?\n[[:blank:]]*

%{
#define YY_USER_ACTION yylloc->columns(yyleng);
%}

%%
%{
    yylloc->step();
%}


{ws}			yylloc->step();
{comment}		{ 
			CATUnicodeString *retString = new CATUnicodeString( yytext ) ;
			yylval->text = retString ;
			return( TWO_HYPHENS_COMMENT ); 
			}
{integer}		{
			numConvertor = yytext;
			numConvertor.ConvertToNum( &(yylval->integer) );
			return( INTEGER );
			}
{real}			{
			numConvertor = yytext;
			LEXConvertStrToNum( numConvertor, &(yylval->real) ) ;
			return( REAL );
			}
{string}		{ 
			numConvertor = yytext;
			CATUnicodeString *retString = new CATUnicodeString("");
			*retString = numConvertor.Strip(CATUnicodeString::CATStripModeAll, CATUnicodeChar('\"'));
			yylval->text = retString;
			return( STRING ); 
			}
{eq}			{
			CATString *retString = new CATString( yytext );
			yylval->identifier = retString;
			return( EQ );
			}
{expr}			{
			CATString *retString = new CATString( yytext );
			yylval->identifier = retString;
			return( EXPR );
			}
{surf_expr}			{
			CATString *retString = new CATString( yytext );
			yylval->identifier = retString;
			return( SURF_EXPR );
			}
[a-zA-Z][a-zA-Z_]*	{
			CATString textString( yytext );
			return( DNBAMPLookup( textString, yylval ) );
			}
[a-zA-Z][a-zA-Z_0-9]*	{ 
			CATString *retString = new CATString( yytext );
			yylval->identifier = retString;
			return( NAME ); 
			}
{nl_WS}			{
			CATUnicodeString *retString = new CATUnicodeString( yytext ) ;
			yylval->text = retString ;
						
			yylloc->lines ( 1 );
			yylloc->step();
			amplineno ++;
			
			return( NL_WS );
			}
\032			; /* CTRL Z */
.                       return( yytext[0] );

%%

class KeyWordDB
    {
	public:
	    KeyWordDB( const CATString& iString, int iKeyval ):
		stringKey( iString ), keyVal( iKeyval )
	    {};
	    const CATString& getKeyString(){ return stringKey; };
	    int getKeyVal() { return keyVal; };
	private:
	CATString stringKey;
	int       keyVal;
    };
static KeyWordDB keywords [] =
	{
	    KeyWordDB(CATString("HEADER"),	HEADER),
	    KeyWordDB(CATString("COMMENT"),	COMMENT),
	    KeyWordDB(CATString("END"),		END),
	    KeyWordDB(CATString("DEFAULTS"),	DEFAULTS),
	    KeyWordDB(CATString("USER"),	USER),
	    KeyWordDB(CATString("PRIMITIVE"),	PRIMITIVE),
	    KeyWordDB(CATString("SEAM"),	SEAM),
	    KeyWordDB(CATString("FIXED"),	A_FIXED),
	    KeyWordDB(CATString("FREE"),	A_FREE),
	    KeyWordDB(CATString("GLOBAL"),	GLOBAL),
	    KeyWordDB(CATString("LIST"),	LIST),
	    KeyWordDB(CATString("LABEL"),	LABEL),
	    KeyWordDB(CATString("MIN"),		MIN),
	    KeyWordDB(CATString("MINI"),	MIN),
	    KeyWordDB(CATString("MINIM"),	MIN),
	    KeyWordDB(CATString("MINIMU"),	MIN),
	    KeyWordDB(CATString("MINIMUM"),	MIN),
	    KeyWordDB(CATString("MAX"),		MAX),
	    KeyWordDB(CATString("MAXI"),	MAX),
	    KeyWordDB(CATString("MAXIM"),	MAX),
	    KeyWordDB(CATString("MAXIMU"),	MAX),
	    KeyWordDB(CATString("MAXIMUM"),	MAX),
	    KeyWordDB(CATString("UNITS"),	UNITS),
	    KeyWordDB(CATString("UNITLESS"),	UNITLESS),
	    KeyWordDB(CATString("ANGLE"),	ANGLE),
	    KeyWordDB(CATString("LENGTH"),	LENGTH),
	    KeyWordDB(CATString("PERCENT"),	PERCENT),
	    KeyWordDB(CATString("PROMPT"),	PROMPT),
	    KeyWordDB(CATString("SKIP"),	SKIP),
	    KeyWordDB(CATString("METHOD"),	METHOD),
	    KeyWordDB(CATString("FOR"),		FOR),
	    KeyWordDB(CATString("BY"),		BY),
	    KeyWordDB(CATString("TO"),		TO),
	    KeyWordDB(CATString("DO"),		DO),
	    KeyWordDB(CATString("UPDATE"),	UPDATE),
	    KeyWordDB(CATString("DIAGRAM"),	DIAGRAM),
	    KeyWordDB(CATString("CHECK"),	CHECK),
	    KeyWordDB(CATString("LIMITS"),	LIMITS),
	    KeyWordDB(CATString("GOTO"),	GOTO),
	    KeyWordDB(CATString("INIT"),	INIT),
	    KeyWordDB(CATString("DEFAULT"),	DEFAULT),
	    KeyWordDB(CATString("CONFIRMATION"),CONFIRMATION),
	    KeyWordDB(CATString("ABORT"),	ABORT),
	    KeyWordDB(CATString("CURRENT"),	CURRENT),
	    KeyWordDB(CATString("IGNORE"),	AMP_IGNORE_1),
	    KeyWordDB(CATString("PAGE"),	PAGE),
	    KeyWordDB(CATString("REAL"),	USER_REAL),
	    KeyWordDB(CATString("INTEGER"),	USER_INTEGER),
	    KeyWordDB(CATString("STRING"),	USER_STRING),
	    KeyWordDB(CATString("COLLISIONS"),	COLLISIONS),
	    KeyWordDB(CATString("GRAPHICS"),	GRAPHICS),
	    KeyWordDB(CATString("ON"),		ON),
	    KeyWordDB(CATString("OFF"),		OFF),
	    KeyWordDB(CATString("ROBOT"),	ROBOT),
	    KeyWordDB(CATString("GANTRY"),	GANTRY),
	    KeyWordDB(CATString("TORCH"),	TORCH),
	    KeyWordDB(CATString("INCLUDE"),	INCLUDE),
	    KeyWordDB( CATString("\0"), 	0 )
	};

int DNBAMPLookup( const CATString& iString, YYSTYPE *yylval )
{
    int ii;
    for( ii = 0; keywords[ii].getKeyString() != "" ; ++ii )
	if( keywords[ii].getKeyString().CompareByCase( iString ) != 0 )
	    return keywords[ii].getKeyVal();
    
    CATString *retString = new CATString( iString );
    if( yylval )
        yylval->identifier = retString;
    
    return NAME;
}

// Reads from the current file at most iMaxSize characters
void ReadFromFile(char *oBuff, int& oResult, int iMaxSize )
{

    if( oBuff && yyinFile )
    {
	unsigned int resultSize = 0;
	HRESULT hr = CATFRead( yyinFile, oBuff, iMaxSize, &resultSize );
	oResult = resultSize;
    }
    /*if( oBuff && yyinFile )
    {
	unsigned int resultSize = 0;
	HRESULT hr = CATFGets( oBuff, iMaxSize, yyinFile );
	CATUnicodeString oString( oBuff );
	oResult = oString.GetLengthInChar();
    }*/
}

// Sets the current file being used using the file descriptor
// returned by CATFOpen - Used for further reading
void SetCurrentFile( unsigned int iFileDesc )
{
    yyinFile = iFileDesc;
}

void LEXConvertStrToNum( const CATUnicodeString& numStr, double* oNum )
{
	*oNum = 0.0 ;
	const char* iStr = numStr.CastToCharPtr() ;
	
	CATListOfInt nonDecimalPart ;
	CATListOfInt decimalPart ;
	CATListOfInt* pDigits = &nonDecimalPart ;

	while( *iStr != '\0' )
	{
		switch(*iStr)
		{
			case '.':
			case ',':
			{
				if( pDigits != &decimalPart )
					pDigits = &decimalPart ;
				else
					return ;

				break ;
			}
			case '0': { pDigits->Append(0) ; break ; }
			case '1': { pDigits->Append(1) ; break ; }
			case '2': { pDigits->Append(2) ; break ; }
			case '3': { pDigits->Append(3) ; break ; }
			case '4': { pDigits->Append(4) ; break ; }
			case '5': { pDigits->Append(5) ; break ; }
			case '6': { pDigits->Append(6) ; break ; }
			case '7': { pDigits->Append(7) ; break ; }
			case '8': { pDigits->Append(8) ; break ; }
			case '9': { pDigits->Append(9) ; break ; }
			default: return ;
		}

		++iStr ;
	}

	double sum = 0.0 ;
	int size1 = nonDecimalPart.Size() ;
	int size2 = decimalPart.Size() ;

	if( !size1 && !size2 )
		return ;

	int i = 1 ;
	for( i = 1 ; i <= size1 ; ++i )
	{
		sum = sum + nonDecimalPart[i]*( CATPower( 10, size1 - i ) ) ;
	}
	for( i = 1 ; i <= size2 ; ++i )
	{
		sum = sum + decimalPart[i]*( 1 / (CATPower(10,i)) ) ;
	}

	*oNum = sum ;	
}
