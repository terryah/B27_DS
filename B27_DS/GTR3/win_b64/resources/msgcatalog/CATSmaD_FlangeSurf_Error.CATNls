// ===============================================================================================
// COPYRIGHT DASSAULT SYSTEMES 2001
// ===============================================================================================
// Objectif         : Catalogue des erreurs du FlangeSurf 
// Responsable      : PJB
// date de création : 02/02/2002
// Commentaires     : 
//
// Revue LGK le 09/01/2004
// ===============================================================================================

//-------------------------------------------------------------------
// FlangeSurf_General
//-------------------------------------------------------------------

CATSmaD_FlangeSurf_Error_ERR_0001.Request    = "";
CATSmaD_FlangeSurf_Error_ERR_0001.Diagnostic = "An undetermined error occurred while computing the folded flange.";
CATSmaD_FlangeSurf_Error_ERR_0001.Advice     = "";

CATSmaD_FlangeSurf_Error_ERR_0002.Request    = "";
CATSmaD_FlangeSurf_Error_ERR_0002.Diagnostic = "An undetermined error occurred while computing the flattened flange.";
CATSmaD_FlangeSurf_Error_ERR_0002.Advice     = "";

CATSmaD_FlangeSurf_Error_ERR_0003.Request    = "Flattened Flange Computation";
CATSmaD_FlangeSurf_Error_ERR_0003.Diagnostic = "The bend radius or K factor values caused an error.";
CATSmaD_FlangeSurf_Error_ERR_0003.Advice     = "Check these values. The default K factor formula involves the global default bend radius.";

//-------------------------------------------------------------------
// FlangeSurf_specs (specs user)
// 10 -> 49
//-------------------------------------------------------------------

CATSmaD_FlangeSurf_Error_ERR_0010.Request    = "";
CATSmaD_FlangeSurf_Error_ERR_0010.Diagnostic = "Thickness not found in specifications";
CATSmaD_FlangeSurf_Error_ERR_0010.Advice     = "Modify the parameters you entered.";

CATSmaD_FlangeSurf_Error_ERR_0011.Request    = "";
CATSmaD_FlangeSurf_Error_ERR_0011.Diagnostic = "Bend radius type not found";
CATSmaD_FlangeSurf_Error_ERR_0011.Advice     = "Modify the parameters you entered.";

CATSmaD_FlangeSurf_Error_ERR_0012.Request    = "";
CATSmaD_FlangeSurf_Error_ERR_0012.Diagnostic = "The bend radius type does not exist";
CATSmaD_FlangeSurf_Error_ERR_0012.Advice     = "Modify the parameters you entered.";

CATSmaD_FlangeSurf_Error_ERR_0013.Request    = "";
CATSmaD_FlangeSurf_Error_ERR_0013.Diagnostic = "Bend radius not found in specifications";
CATSmaD_FlangeSurf_Error_ERR_0013.Advice     = "Modify the parameters you entered.";

CATSmaD_FlangeSurf_Error_ERR_0014.Request    = "";
CATSmaD_FlangeSurf_Error_ERR_0014.Diagnostic = "Flange direction not found";
CATSmaD_FlangeSurf_Error_ERR_0014.Advice     = "Modify the parameters you entered.";

CATSmaD_FlangeSurf_Error_ERR_0015.Request    = "";
CATSmaD_FlangeSurf_Error_ERR_0015.Diagnostic = "Base feature direction not found";
CATSmaD_FlangeSurf_Error_ERR_0015.Advice     = "Modify the parameters you entered.";

CATSmaD_FlangeSurf_Error_ERR_0016.Request    = "";
CATSmaD_FlangeSurf_Error_ERR_0016.Diagnostic = "Flange material direction not found";
CATSmaD_FlangeSurf_Error_ERR_0016.Advice     = "Modify the parameters you entered.";

CATSmaD_FlangeSurf_Error_ERR_0017.Request    = "";
CATSmaD_FlangeSurf_Error_ERR_0017.Diagnostic = "Base feature material direction not found";
CATSmaD_FlangeSurf_Error_ERR_0017.Advice     = "Modify the parameters you entered.";

CATSmaD_FlangeSurf_Error_ERR_0020.Request    = "";
CATSmaD_FlangeSurf_Error_ERR_0020.Diagnostic = "Flange support not found in specifications";
CATSmaD_FlangeSurf_Error_ERR_0020.Advice     = "Modify the parameters you entered.";

CATSmaD_FlangeSurf_Error_ERR_0021.Request    = "";
CATSmaD_FlangeSurf_Error_ERR_0021.Diagnostic = "Flange support type not found";
CATSmaD_FlangeSurf_Error_ERR_0021.Advice     = "Modify the parameters you entered.";

CATSmaD_FlangeSurf_Error_ERR_0022.Request    = "";
CATSmaD_FlangeSurf_Error_ERR_0022.Diagnostic = "The flange support type does not exist";
CATSmaD_FlangeSurf_Error_ERR_0022.Advice     = "Modify the parameters you entered.";

CATSmaD_FlangeSurf_Error_ERR_0023.Request    = "";
CATSmaD_FlangeSurf_Error_ERR_0023.Diagnostic = "Approximation length not found in specifications";
CATSmaD_FlangeSurf_Error_ERR_0023.Advice     = "Modify the parameters you entered.";

CATSmaD_FlangeSurf_Error_ERR_0024.Request    = "";
CATSmaD_FlangeSurf_Error_ERR_0024.Diagnostic = "Angle value not found in specifications";
CATSmaD_FlangeSurf_Error_ERR_0024.Advice     = "Modify the parameters you entered.";

CATSmaD_FlangeSurf_Error_ERR_0030.Request    = "";
CATSmaD_FlangeSurf_Error_ERR_0030.Diagnostic = "EOP element no found in specifications";
CATSmaD_FlangeSurf_Error_ERR_0030.Advice     = "Modify the parameters you entered.";

CATSmaD_FlangeSurf_Error_ERR_0031.Request    = "";
CATSmaD_FlangeSurf_Error_ERR_0031.Diagnostic = "EOP type not found";
CATSmaD_FlangeSurf_Error_ERR_0031.Advice     = "Modify the parameters you entered.";

CATSmaD_FlangeSurf_Error_ERR_0032.Request    = "";
CATSmaD_FlangeSurf_Error_ERR_0032.Diagnostic = "The EOP type does not exist";
CATSmaD_FlangeSurf_Error_ERR_0032.Advice     = "Modify the parameters you entered.";

CATSmaD_FlangeSurf_Error_ERR_0033.Request    = "";
CATSmaD_FlangeSurf_Error_ERR_0033.Diagnostic = "EOP length not found in specifications";
CATSmaD_FlangeSurf_Error_ERR_0033.Advice     = "Modify the parameters you entered.";

CATSmaD_FlangeSurf_Error_ERR_0036.Request    = "";
CATSmaD_FlangeSurf_Error_ERR_0036.Diagnostic = "Side element not found in specifications";
CATSmaD_FlangeSurf_Error_ERR_0036.Advice     = "Modify the parameters you entered.";

CATSmaD_FlangeSurf_Error_ERR_0037.Request    = "";
CATSmaD_FlangeSurf_Error_ERR_0037.Diagnostic = "Side type not found";
CATSmaD_FlangeSurf_Error_ERR_0037.Advice     = "Modify the parameters you entered.";

CATSmaD_FlangeSurf_Error_ERR_0038.Request    = "";
CATSmaD_FlangeSurf_Error_ERR_0038.Diagnostic = "The side type does not exist";
CATSmaD_FlangeSurf_Error_ERR_0038.Advice     = "Modify the parameters you entered.";

CATSmaD_FlangeSurf_Error_ERR_0040.Request    = "";
CATSmaD_FlangeSurf_Error_ERR_0041.Diagnostic = "Corner 1 type not found";
CATSmaD_FlangeSurf_Error_ERR_0041.Advice     = "Modify the parameters you entered.";

CATSmaD_FlangeSurf_Error_ERR_0042.Request    = "";
CATSmaD_FlangeSurf_Error_ERR_0042.Diagnostic = "The corner 1 type does not exist";
CATSmaD_FlangeSurf_Error_ERR_0042.Advice     = "Modify the parameters you entered.";

CATSmaD_FlangeSurf_Error_ERR_0043.Request    = "";
CATSmaD_FlangeSurf_Error_ERR_0043.Diagnostic = "Corner 1 radius not found in specifications";
CATSmaD_FlangeSurf_Error_ERR_0043.Advice     = "Modify the parameters you entered.";

CATSmaD_FlangeSurf_Error_ERR_0044.Request    = "";
CATSmaD_FlangeSurf_Error_ERR_0044.Diagnostic = "Corner 2 type not found";
CATSmaD_FlangeSurf_Error_ERR_0044.Advice     = "Modify the parameters you entered.";

CATSmaD_FlangeSurf_Error_ERR_0045.Request    = "";
CATSmaD_FlangeSurf_Error_ERR_0045.Diagnostic = "The corner 2 type does not exist";
CATSmaD_FlangeSurf_Error_ERR_0045.Advice     = "Modify the parameters you entered.";

CATSmaD_FlangeSurf_Error_ERR_0046.Request    = "";
CATSmaD_FlangeSurf_Error_ERR_0046.Diagnostic = "Corner 2 radius not found in specifications";
CATSmaD_FlangeSurf_Error_ERR_0046.Advice     = "Modify the parameters you entered.";

//-------------------------------------------------------------------
// FlangeSurf_specs (internal structure)
// 50 -> 99
//-------------------------------------------------------------------

CATSmaD_FlangeSurf_Error_ERR_0050.Request    = "";
CATSmaD_FlangeSurf_Error_ERR_0050.Diagnostic = "Specifications for the body to bend not found";
CATSmaD_FlangeSurf_Error_ERR_0050.Advice     = "";

CATSmaD_FlangeSurf_Error_ERR_0051.Request    = "";
CATSmaD_FlangeSurf_Error_ERR_0051.Diagnostic = "Body to bend not found";
CATSmaD_FlangeSurf_Error_ERR_0051.Advice     = "";

CATSmaD_FlangeSurf_Error_ERR_0052.Request    = "The input elements for Side 1 and Side 2 are identical.";
CATSmaD_FlangeSurf_Error_ERR_0052.Diagnostic = "This configuration is not supported.";
CATSmaD_FlangeSurf_Error_ERR_0052.Advice     = "Select different elements for Side 1 and Side 2.";

CATSmaD_FlangeSurf_Error_ERR_0053.Request    = "";
CATSmaD_FlangeSurf_Error_ERR_0053.Diagnostic = "";
CATSmaD_FlangeSurf_Error_ERR_0053.Advice     = "";

CATSmaD_FlangeSurf_Error_ERR_0080.Request    = "";
CATSmaD_FlangeSurf_Error_ERR_0080.Diagnostic = "Interface on flange surface support not found";
CATSmaD_FlangeSurf_Error_ERR_0080.Advice     = "";

CATSmaD_FlangeSurf_Error_ERR_0081.Request    = "";
CATSmaD_FlangeSurf_Error_ERR_0081.Diagnostic = "Folded result of flange surface support internal feature not found";
CATSmaD_FlangeSurf_Error_ERR_0081.Advice     = "";

CATSmaD_FlangeSurf_Error_ERR_0090.Request    = "";
CATSmaD_FlangeSurf_Error_ERR_0090.Diagnostic = "Folded result of relimited internal base feature not found";
CATSmaD_FlangeSurf_Error_ERR_0090.Advice     = "";
//-------------------------------------------------------------------
// FlangeSurf_Build_Utilities
//-------------------------------------------------------------------

CATSmaD_FlangeSurf_Error_ERR_0111.Request    = "";
CATSmaD_FlangeSurf_Error_ERR_0111.Diagnostic = "An error occurred while computing the parallel element.";
CATSmaD_FlangeSurf_Error_ERR_0111.Advice     = "";

//-------------------------------------------------------------------
// FlangeSurf_UI_Utilities
//-------------------------------------------------------------------

CATSmaD_FlangeSurf_Error_ERR_0501.Request    = "";
CATSmaD_FlangeSurf_Error_ERR_0501.Diagnostic = "An error occurred while computing.";
CATSmaD_FlangeSurf_Error_ERR_0501.Advice     = "";

//-------------------------------------------------------------------
// FlangeSurf_Support
//-------------------------------------------------------------------
//
// FD
//
CATSmaD_FlangeSurf_Error_ERR_1100.Request    = ".";
CATSmaD_FlangeSurf_Error_ERR_1100.Diagnostic = "An error occurred  while computing the surface.";
CATSmaD_FlangeSurf_Error_ERR_1100.Advice     = "Modify the parameters you entered.";

CATSmaD_FlangeSurf_Error_ERR_1101.Request    = ".";
CATSmaD_FlangeSurf_Error_ERR_1101.Diagnostic = "An error occurred while computing the intersection.";
CATSmaD_FlangeSurf_Error_ERR_1101.Advice     = "Decrease the approximation length value.";

CATSmaD_FlangeSurf_Error_ERR_1110.Request    = "You are trying to approximate the surface by a ruled surface.";
CATSmaD_FlangeSurf_Error_ERR_1110.Diagnostic = "An error occurred while computing the surface.";
CATSmaD_FlangeSurf_Error_ERR_1110.Advice     = "Modify the parameters you entered.";

CATSmaD_FlangeSurf_Error_ERR_1111.Request    = "You are trying to approximate the surface by a ruled surface.";
CATSmaD_FlangeSurf_Error_ERR_1111.Diagnostic = "The approximation length you entered is too high to define the surface.";
CATSmaD_FlangeSurf_Error_ERR_1111.Advice     = "Decrease the approximation length value.";

CATSmaD_FlangeSurf_Error_ERR_1112.Request    = "The Support Surface should not contain more than one domain.";
CATSmaD_FlangeSurf_Error_ERR_1112.Diagnostic = "Extract the desired input domain and use it as the input instead.";
CATSmaD_FlangeSurf_Error_ERR_1112.Advice     = "";

//
// FP
//
CATSmaD_FlangeSurf_Error_ERR_1180.Request    = "";
CATSmaD_FlangeSurf_Error_ERR_1180.Diagnostic = "The flat pattern of the support cannot be created.";
CATSmaD_FlangeSurf_Error_ERR_1180.Advice     = "";

//-------------------------------------------------------------------
// FlangeSurf_EOP_LengthFromOML 
//-------------------------------------------------------------------
//
// FD
//
CATSmaD_FlangeSurf_Error_ERR_1200.Request    = "";
CATSmaD_FlangeSurf_Error_ERR_1200.Diagnostic = "An error occurred while computing the EOP.";
CATSmaD_FlangeSurf_Error_ERR_1200.Advice     = "";

CATSmaD_FlangeSurf_Error_ERR_1201.Request    = "You are trying to define the EOP of the flange with a length.";
CATSmaD_FlangeSurf_Error_ERR_1201.Diagnostic = "The input length you entered is too high to define the EOP.";
CATSmaD_FlangeSurf_Error_ERR_1201.Advice     = "Decrease the length value.";
//
// FP
//
CATSmaD_FlangeSurf_Error_ERR_1280.Request    = "";
CATSmaD_FlangeSurf_Error_ERR_1280.Diagnostic = "The flat pattern of the EOP cannot be created.";
CATSmaD_FlangeSurf_Error_ERR_1280.Advice     = "";

//-------------------------------------------------------------------
// FlangeSurf_SideStandard 
//-------------------------------------------------------------------
//
// FD
//
CATSmaD_FlangeSurf_Error_ERR_1300.Request    = " ";
CATSmaD_FlangeSurf_Error_ERR_1300.Diagnostic = "An error occurred while computing the side";
CATSmaD_FlangeSurf_Error_ERR_1300.Advice     = " Decreasing the flange support length may help.";

CATSmaD_FlangeSurf_Error_ERR_1301.Request    = "You are trying to define a side of the flange with the standard option.";
CATSmaD_FlangeSurf_Error_ERR_1301.Diagnostic = "The geometry makes it impossible to define such a side.";
CATSmaD_FlangeSurf_Error_ERR_1301.Advice     = "Modify the parameters you entered.";

CATSmaD_FlangeSurf_Error_ERR_1310.Request    = "You are trying to define a side of the flange with the standard option.";
CATSmaD_FlangeSurf_Error_ERR_1310.Diagnostic = "There is no intersection between the flange support and the last folded result of the Part.";
CATSmaD_FlangeSurf_Error_ERR_1310.Advice     = "Modify the support of the flange.";
//
// FP
//
CATSmaD_FlangeSurf_Error_ERR_1380.Request    = "";
CATSmaD_FlangeSurf_Error_ERR_1380.Diagnostic = "The flat pattern of the side cannot be created.";
CATSmaD_FlangeSurf_Error_ERR_1380.Advice     = "";

//-------------------------------------------------------------------
// FlangeSurf_Relimit 
//-------------------------------------------------------------------
//
// FD
//
CATSmaD_FlangeSurf_Error_ERR_1400.Request    = "You have defined all flange input parameters";
CATSmaD_FlangeSurf_Error_ERR_1400.Diagnostic = "The parameters you entered do not allow the flange to be relimited.";
CATSmaD_FlangeSurf_Error_ERR_1400.Advice     = "Modify the parameters you entered.";

CATSmaD_FlangeSurf_Error_ERR_1401.Request    = "";
CATSmaD_FlangeSurf_Error_ERR_1401.Diagnostic = "The relimitation of the flange will generate a multi-ribbon fillet.";
CATSmaD_FlangeSurf_Error_ERR_1401.Advice     = "Modify the parameters you entered.";
//
// FP
//
CATSmaD_FlangeSurf_Error_ERR_1480.Request    = "You have defined all flange input parameters.";
CATSmaD_FlangeSurf_Error_ERR_1480.Diagnostic = "The input parameters do not allow the flange to be relimited.";
CATSmaD_FlangeSurf_Error_ERR_1480.Advice     = "Modify the parameters you entered.";

//-------------------------------------------------------------------
// FlangeSurf_Fillet 
//-------------------------------------------------------------------
//
// FD
//
// Erreur indéterminée
CATSmaD_FlangeSurf_Error_ERR_1500.Request    = "";
CATSmaD_FlangeSurf_Error_ERR_1500.Diagnostic = "An internal error occurred: the folded flange could not be computed.";
CATSmaD_FlangeSurf_Error_ERR_1500.Advice     = "Modify the parameters you entered.";
// Corner
CATSmaD_FlangeSurf_Error_ERR_1510.Request    = "";
CATSmaD_FlangeSurf_Error_ERR_1510.Diagnostic = "The corner of the folded flange could not be computed.";
CATSmaD_FlangeSurf_Error_ERR_1510.Advice     = "Modify the parameters you entered.";
// Fillet
CATSmaD_FlangeSurf_Error_ERR_1520.Request    = "";
CATSmaD_FlangeSurf_Error_ERR_1520.Diagnostic = "The fillet of the folded flange could not be computed.";
CATSmaD_FlangeSurf_Error_ERR_1520.Advice     = "Modify the parameters you entered.";
//
// FP
//
// Erreur indéterminée
CATSmaD_FlangeSurf_Error_ERR_1580.Request    = "";
CATSmaD_FlangeSurf_Error_ERR_1580.Diagnostic = "The flattened flange could not be computed.";
CATSmaD_FlangeSurf_Error_ERR_1580.Advice     = "Modify the parameters you entered.";
//
// Erreur spécifique due aux 2 compensations qui se marchent sur les pieds...
CATSmaD_FlangeSurf_Error_ERR_1581.Request    = "You are trying to apply both joggle compensation and flange side compensation.";
CATSmaD_FlangeSurf_Error_ERR_1581.Diagnostic = "The flattened flange could not be computed.";
CATSmaD_FlangeSurf_Error_ERR_1581.Advice     = "Decrease/remove the flange side compensation : joggle compensation should be enough.";
//
// Corner
CATSmaD_FlangeSurf_Error_ERR_1585.Request    = "";
CATSmaD_FlangeSurf_Error_ERR_1585.Diagnostic = "The corner of the folded flange could not be computed.";
CATSmaD_FlangeSurf_Error_ERR_1585.Advice     = "Modify the parameters you entered.";
// Fillet
CATSmaD_FlangeSurf_Error_ERR_1590.Request    = "";
CATSmaD_FlangeSurf_Error_ERR_1590.Diagnostic = "The fillet of the folded flange could not be computed.";
CATSmaD_FlangeSurf_Error_ERR_1590.Advice     = "Modify the parameters you entered.";
// Flange adjacent : 
CATSmaD_FlangeSurf_Error_ERR_1591.Request    = "You are trying to perform an adjacent flange";
CATSmaD_FlangeSurf_Error_ERR_1591.Diagnostic = "The unfolded view could not be computed properly.";
CATSmaD_FlangeSurf_Error_ERR_1591.Advice     = "To workaround this temporary issue, we recommend you create a notch on the base feature before trying to create this surfacic flange.";

//--------------------------------------------------------------------------
// Warning
//--------------------------------------------------------------------------
FlangeSurf_Warning_01_01 = "There is at least one discontinuity in the flattening process between the Base Feature and the adjacent Surfacic Flange(s).";
FlangeSurf_Warning_01_02 = "     This may lead to problems while building holes/cut outs intersecting the Base Feature BTL of those adjacent Surfacic Flanges.";
FlangeSurf_Warning_02_01 = "The graphical properties of the BTLs are inverted in the flat view.";
FlangeSurf_Warning_02_02 = "    The feature must be recreated to correct this issue.";

FlangeSurf_Warning_03_01 = "Impossible to build the second OML.";
FlangeSurf_Warning_03_02 = "    The feature must be recreated to correct this issue.";

FlangeSurf_Warning_04_01 = "Impossible to build the CLB.";
FlangeSurf_Warning_04_02 = "    The feature must be recreated to correct this issue.";

FlangeSurf_Warning_05_01 = "The boundary of the surface support has been used to complete the relimitation.";
FlangeSurf_Warning_05_02 = "    If this is not your intent, check that the support is large enough for the limiting elements to intersect.";

