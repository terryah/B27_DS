PadTitle = "Pad Definition";
PocketTitle = "Pocket Definition";

LongHelp= "Creates or modifies pads or pockets.";

PadLabDim = "Length: ";
PocketLabDim = "Depth: ";

FrmRoot.FrmMain.FraProf.ChkThin.Title="Thick";

FrmMore.FrmThin.ChkMerge.Title="Merge Ends";

FrmMore.FrmThin.ChkFiber.Title= "Neutral Fiber";

FrmMore.FrmThin.LabThin1.Title="Thickness1";

FrmMore.FrmThin.LabThin2.Title="Thickness2:";

FrmThinPadTitle="Thin Pad";
FrmThinPocketTitle="Thin Pocket";

PlanarProfile="Profile"; 
NonPlanarProfile="Profile/Surface"; 

FrmRoot.FrmMain.FraEnd.Title = "First Limit";
FrmRoot.FrmMain.FraEnd.LabEndType.Title = "Type: ";
CmbEndLab0 = "Dimension";
CmbEndLab1 = "Up to next";
CmbEndLab2 = "Up to last";
CmbEndLab3 = "Up to plane";
CmbEndLab4 = "Up to surface";
FrmRoot.FrmMain.FraEnd.ParamFrame.Spinner.Title = "Dim1"; 
FrmRoot.FrmMain.FraEnd.ParamFrame.EnglobingFrame.IntermediateFrame.Spinner.Title = "Dim1"; 
FrmRoot.FrmMain.FraEnd.LabEndLim.Title = "Limit: ";
FrmRoot.FrmMain.FraEnd.FraEnd4.Spinner.Title = "Off1"; 
FrmRoot.FrmMain.FraEnd.FraEnd4.EnglobingFrame.IntermediateFrame.Spinner.Title = "Off1"; 
FrmRoot.FrmMain.FraEnd.LabEndOffset.Title = "Offset: ";
FrmRoot.FrmMain.FraSym.ChkSym.Title = "Mirrored extent";
FrmRoot.FrmMain.FraRev.PshRev.Title = " Reverse Direction ";
FrmRoot.FrmMain.FraProf.FraProf1.FraEdt.Lab.Title="Selection: ";
FrmRoot.FrmMain.FraProf.FraProf1.CtxProf.ItmProf.Title="Go to profile definition";
FrmRoot.FrmMain.FraProf.PshRevSide.Title = " Reverse Side       ";
FrmRoot.FrmMain.FraProf.ChkThin.LongHelp = "Creates a thin feature : profile is thicken in its support plane";

FrmMore.FrmThin.ChkFiber.LongHelp="Creates a thin feature using a neutral fiber";
FrmMore.FrmThin.ChkMerge.LongHelp="Merges ends of thin feature with around material";
FrmMore.FrmThin.LabThin1.LongHelp="First thickness of thin feature";
FrmMore.FrmThin.FraThin1.Spinner.Title="Thickness1";
FrmMore.FrmThin.FraThin1.EnglobingFrame.IntermediateFrame.Spinner.Title="Thickness1";
FrmMore.FrmThin.FraThin1.Spinner.LongHelp="First thickness of thin feature";

FrmMore.FrmThin.LabThin2.LongHelp="Second thickness of thin feature";
FrmMore.FrmThin.FraThin2.Spinner.LongHelp="Second thickness of thin feature";
FrmMore.FrmThin.FraThin2.Spinner.Title="Thickness2";
FrmMore.FrmThin.FraThin2.EnglobingFrame.IntermediateFrame.Spinner.Title="Thickness2";

FrmMore.FrmThin.LongHelp="Parameters for thin feature";

FrmMore.FraStart.Title = "Second Limit";
FrmMore.FraStart.LabStartType.Title = "Type: ";
CmbStartLab0 = "Dimension";
CmbStartLab1 = "Up to next";
CmbStartLab2 = "Up to last";
CmbStartLab3 = "Up to plane";
CmbStartLab4 = "Up to surface";
FrmMore.FraStart.ParamFrame.Spinner.Title = "Dim2";
FrmMore.FraStart.ParamFrame.EnglobingFrame.IntermediateFrame.Spinner.Title = "Dim2";
FrmMore.FraStart.LabStartLim.Title = "Limit: ";
FrmMore.FraStart.FraStart4.Spinner.Title = "Off2";
FrmMore.FraStart.FraStart4.EnglobingFrame.IntermediateFrame.Spinner.Title = "Off2";
FrmMore.FraStart.LabStartOffset.Title = "Offset: ";

FrmMore.FraDir.Title = "Direction";
FrmMore.FraDir.FraDir1.ChkNormal.Title = "Normal to profile";
FrmMore.FraDir.FraDir2.FraEdt.Lab.Title = "Reference:"; 


FrmRoot.FrmMain.FraEnd.LongHelp = 
"First Limit 
Specifies the limit defined 
in the direction of extrusion.";

FrmRoot.FrmMain.FraEnd.LabEndType.LongHelp = 
"Type of relimitation 
for the first limit.";
FrmRoot.FrmMain.FraEnd.CmbEnd.LongHelp = 
"Type of relimitation 
for the first limit.";

FrmRoot.FrmMain.FraEnd.LabEndValue.LongHelp = 
"Specifies the dimension 
for the first limit.";
FrmRoot.FrmMain.FraEnd.ParamFrame.LongHelp = 
"Specifies the dimension 
for the first limit.";
FrmRoot.FrmMain.FraEnd.ParamFrame.EnglobingFrame.IntermediateFrame.LongHelp = 
"Specifies the dimension 
for the first limit.";

FrmRoot.FrmMain.FraEnd.LabEndLim.LongHelp = 
"Identifies the limiting element 
for the first limit";
FrmRoot.FrmMain.FraEnd.FraEnd3.LongHelp = 
"Identifies the limiting element 
for the first limit";

FrmRoot.FrmMain.FraEnd.LabEndOffset.LongHelp = 
"Specifies the offset value 
for the first limit.";
FrmRoot.FrmMain.FraEnd.FraEnd4.LongHelp = 
"Specifies the offset value 
for the first limit.";

FrmRoot.FrmMain.FraSym.ChkSym.LongHelp = "Mirrors the extrusion offset.";
FrmRoot.FrmMain.FraRev.PshRev.LongHelp = 
"Reverses the direction of extrusion 
pointed by the arrow.";

FrmRoot.FrmMain.FraProf.FraProf1.LongHelp=
"Specifies the profile.";
FrmRoot.FrmMain.FraProf.PshProf.LongHelp=
"Edits the profile.";
FrmRoot.FrmMain.FraProf.PshRevSide.LongHelp=
"Reverses the material side shown by the arrow.";

FrmMore.FraStart.LongHelp = 
"Second Limit
Specifies the limit opposite 
to the direction of extrusion.";

FrmMore.FraStart.LabStartType.LongHelp = 
"Type of relimitation 
for the second limit.";
FrmMore.FraStart.CmbStart.LongHelp = 
"Type of relimitation 
for the second limit.";

FrmMore.FraStart.LabStartValue.LongHelp = 
"Specifies the dimension 
for the second limit.";
FrmMore.FraStart.ParamFrame.LongHelp = 
"Specifies the dimension 
for the second limit.";
FrmMore.FraStart.ParamFrame.EnglobingFrame.IntermediateFrame.LongHelp = 
"Specifies the dimension 
for the second limit.";

FrmMore.FraStart.LabStartLim.LongHelp = 
"Identifies the limiting element 
for the second limit";
FrmMore.FraStart.FraStart3.LongHelp = 
"Identifies the limiting element 
for the second limit";

FrmMore.FraStart.LabStartOffset.LongHelp = 
"Specifies the offset value 
for the second limit.";
FrmMore.FraStart.FraStart4.LongHelp = 
"Specifies the offset value 
for the second limit.";

FrmMore.FraDir.LongHelp = 
"Direction 
Specifies the direction of extrusion.";

FrmMore.FraDir.FraDir1.ChkNormal.LongHelp = 
"Makes the direction of extrusion
 normal to the profile.";
FrmMore.FraDir.FraDir2.Lab.LongHelp = 
"Specifies the element used  
to define the direction.";
FrmMore.FraDir.FraDir2.Edt.LongHelp = 
"Specifies the element used  
to define the direction.";
