Feed = "Feedrate =";

ToolPathEdition = "Tool path replay";

ToolAnimation = "Tool animation";

More = "More>>";
      
AnimationSpeed = "Animation speed ";

AllToolPathDisplayed = "All tool path displayed";

BeginningOfToolPath = "Beginning of tool path";

ToolAnimation.FirstPoint.ShortHelp = "Goes to start of tool path or to previous operation";
ToolAnimation.FirstPoint.LongHelp = "If start of tool path for current operation is reached, allows going to previous operation\nOtherwise, goes to start of tool path for current operation";

ToolAnimation.LastPoint.ShortHelp = "Displays complete tool path or goes to next operation";
ToolAnimation.LastPoint.LongHelp = "If complete tool path is already displayed for current operation, allows going to next operation\nOtherwise, displays complete tool path for current operation";

ToolAnimation.ReplayForward.ShortHelp = "Forward replay";
ToolAnimation.ReplayForward.LongHelp = "Replays tool path in forward direction according to replay mode"; 

ToolAnimation.ReplayBackward.ShortHelp = "Backward replay";
ToolAnimation.ReplayBackward.LongHelp = "Replays tool path in backward direction according to replay mode"; 

ToolAnimation.StopToolAnimation.ShortHelp = "Stops animation";
ToolAnimation.StopToolAnimation.LongHelp = "Stops tool animation";

ToolAnimation.Speed.ShortHelp = "Tool animation speed in number of points";
ToolAnimation.Speed.LongHelp = "Allows modifying the number of points to replay in the tool animation";

ToolAnimation.NumberOfPoints.ShortHelp = "Number of points to replay";
ToolAnimation.NumberOfPoints.LongHelp = "Allows modifying the number of points to replay in Point by Point replay mode";

ReplayPhotoFrame.ReplayModeIconBox.ShortHelp = "Replay mode";
ReplayPhotoFrame.ReplayModeIconBox.LongHelp = "Allows choosing either Continuous, Plane by Plane, 
Feedrate by Feedrate, Syntaxe by Syntaxe, Point by Point and Section by Section replay mode";

ReplayPhotoFrame.ReplayModeIconBox.ContinuReplayMode.ShortHelp = "Continuous replay of tool path";

ReplayPhotoFrame.ReplayModeIconBox.ByPlaneReplayMode.ShortHelp = "Plane by Plane replay of tool path";

ReplayPhotoFrame.ReplayModeIconBox.ByFeedrateReplayMode.ShortHelp = "Feedrate by Feedrate replay of tool path";

ReplayPhotoFrame.ReplayModeIconBox.BySyntaxReplayMode.ShortHelp = "Replay of tool path with stop on PP instructions";

ReplayPhotoFrame.ReplayModeIconBox.BySectionReplayMode.ShortHelp = "Sectioning of tool path";

ReplayPhotoFrame.ReplayModeIconBox.ByStepReplayMode.ShortHelp = "Point by Point replay of tool path";

ReplayPhotoFrame.VisuModeIconBox.ShortHelp = "Visualization mode";
ReplayPhotoFrame.VisuModeIconBox.LongHelp = "Allows choosing between last position display, all tool axes display, and all tools display";

ReplayPhotoFrame.VisuModeIconBox.NothingVisuMode.ShortHelp = "Only last tool position is displayed";

ReplayPhotoFrame.VisuModeIconBox.AxisVisuMode.ShortHelp = "All tool axes are displayed";

ReplayPhotoFrame.VisuModeIconBox.ToolsVisuMode.ShortHelp = "All tools are displayed";

ReplayPhotoFrame.VisuModeIconBox.SectionAeraVisuMode.ShortHelp = "Display the machined surface\n(Only available with sectioning replay mode)";

ReplayPhotoFrame.VisuModeIconBox.SectionAeraToolAxisVisuMode.ShortHelp = "Display the machined surface and all tool axes\n(Only available with sectioning replay mode)";

ReplayPhotoFrame.TrajectColorIconBox.ShortHelp = "Color mode ";
ReplayPhotoFrame.TrajectColorIconBox.LongHelp = "Allows choosing either the same color for all paths (faster) or a different color for each feedrate type";

ReplayPhotoFrame.TrajectColorIconBox.ColorOff.ShortHelp = "Same color";

ReplayPhotoFrame.TrajectColorIconBox.ColorOn.ShortHelp = "Different colors";

ReplayPhotoFrame.PointTypeIconBox.ShortHelp = "Point display mode";
ReplayPhotoFrame.PointTypeIconBox.LongHelp = "Allows choosing the point to be displayed in the trajectory: tool tip, center point and/or contact point (if it is stored)";

ReplayPhotoFrame.PointTypeIconBox.TipOnly.ShortHelp  = "Displays the tool tip or center point";

ReplayPhotoFrame.PointTypeIconBox.ContactOnly.ShortHelp = "Displays the contact point only (if it is stored) for tool trajectory";

ReplayPhotoFrame.PointTypeIconBox.TipAndContact.ShortHelp = "Displays the contact point (if it is stored) for tool trajectory. Otherwise, displays the tool tip or center point";

ReplayPhotoFrame.PointTypeIconBox.TipOrContact.ShortHelp = "Displays the tool tip or center point and the contact point (if it is stored) for tool trajectory.";

ReplayPhotoFrame.TracutModeIconBox.ShortHelp = "TRACUT display mode";
ReplayPhotoFrame.TracutModeIconBox.LongHelp = "Takes into account or not TRACUT instruction to display tool path";

ReplayPhotoFrame.TracutModeIconBox.TracutOFF.ShortHelp  = "TRACUT instructions are not taken into account to display tool path";

ReplayPhotoFrame.TracutModeIconBox.TracutON.ShortHelp = "TRACUT instructions are taken into account to display tool path";

ReplayPhotoFrame.HolderVisuModeIconBox.ShortHelp = "Holder Visibility Options";
ReplayPhotoFrame.HolderVisuModeIconBox.LongHelp = "Manages visibility of the holder";
ReplayPhotoFrame.HolderVisuModeIconBox.HolderON.ShortHelp  = "Displays the holder";
ReplayPhotoFrame.HolderVisuModeIconBox.HolderOFF.ShortHelp = "Hides the holder";


ReplayPhotoFrame.ResetVisu.ShortHelp = "Visualization refresh";
ReplayPhotoFrame.ResetVisu.LongHelp  = "Visualization refresh"; 

MaxDistEditor.Value.ShortHelp = "Maximum distance between axes and tools";
MaxDistEditor.Value.LongHelp  = "Axes or tools are added on tool path display whenever necessary to take into account the specified maximum distance.";

MaxDistEditor.Label = "Max Distance";

TotalTime = "Total time";

MachiningTime = "Machining time";

ReplayPositions = "Replay positions";

StartPosition = " Start ";

CurrentPosition = " Current ";

EndPosition = " End ";

ReplayPositions.StartPositionEditor.ShortHelp = "Enter initial tool position";
ReplayPositions.StartPositionEditor.LongHelp = "Enter the initial position of the tool for tool path replay";

ReplayPositions.CurrentPositionSpinner.ShortHelp = "Enter current tool position";
ReplayPositions.CurrentPositionSpinner.LongHelp = "Enter the current position of the tool for tool path replay";

ReplayPositions.EndPositionEditor.ShortHelp = "Enter final tool position";
ReplayPositions.EndPositionEditor.LongHelp = "Enter the final position of the tool for tool path replay";

ReplayPositions.StartPositionButton.Help = "Select initial tool position";
ReplayPositions.StartPositionButton.ShortHelp = "Select initial tool position";
ReplayPositions.StartPositionButton.LongHelp = "Select the initial position of the tool for tool path replay";

ReplayPositions.CurrentPositionButton.Help = "Select current tool position";
ReplayPositions.CurrentPositionButton.ShortHelp = "Select current tool position";
ReplayPositions.CurrentPositionButton.LongHelp = "Select the current position of the tool for tool path replay";

ReplayPositions.EndPositionButton.Help = "Select final tool position";
ReplayPositions.EndPositionButton.ShortHelp = "Select final tool position";
ReplayPositions.EndPositionButton.LongHelp = "Select the final position of the tool for tool path replay";
