//-----------------------------------------------------
// Resource file for CATCCSPartingSurfaceDlg class
// En_US
//-----------------------------------------------------
//=============================================================================
// COPYRIGHT DASSAULT SYSTEMES 2003
//=============================================================================
//
//   Resource file for NLS purpose. 
//
//=============================================================================
//
// Implementation Notes :
//
//=============================================================================
// 19/02/03 : jcb; creation
//=============================================================================

 Title="Parting Surface Definition";

 NoSelection = "No selection";

 // Operation to compute
 FrmRoot.FrmSrf.FrmOper.Title			= " Operation ";
 FrmRoot.FrmSrf.FrmOper.LbOper1.Title	= "    ";
 FrmRoot.FrmSrf.FrmOper.LbOper2.Title	= " ";
 FrmRoot.FrmSrf.FrmOper.LbOper3.Title	= "    ";
  
 FrmRoot.FrmSrf.FrmJoin.Title				= " Options ";
 FrmRoot.FrmSrf.FrmJoin.JoinCreation.Title	= " Join parting surface  ";
 FrmRoot.FrmSrf.FrmJoin.FrmToler.LbTolJoin.Title	= " Merging distance ";
 FrmRoot.FrmSrf.FrmJoin.FrmToler.LbTolSmooth.Title	= " Maximum deviation ";

 // Support to extrude
 FrmRoot.FrmSupport.Title				= " Profile Support   ";
 FrmRoot.FrmSupport.LbSupport.Title		= " Reference         ";
	
 // Profile Elements
 FrmRoot.FrmProfile.Title				= " Profile Definition ";
 FrmRoot.FrmProfile.LbStart.Title		= " Vertex 1:          ";
 FrmRoot.FrmProfile.LbEnd.Title			= " Vertex 2:          ";
 FrmRoot.FrmProfile.InvertProfile.Title = " Complementary      ";
 
 // Profile Direction
 FrmRoot.FrmDirection.Title				   = " Direction Definition ";
 FrmRoot.FrmDirection.LbFrmDirection.Title = " Up to sketch:    ";

 FrmRoot.FrmDirection.TabContainer.TabUpToSketch.Title = " Up To Sketch ";
 FrmRoot.FrmDirection.TabContainer.TabUpToSketch.LbDirSketch.Title = " Sketch ";

 FrmRoot.FrmDirection.TabContainer.TabDirLength.Title = " Direction+Length ";
 FrmRoot.FrmDirection.TabContainer.TabDirLength.LbDirLength.Title = " Direction ";
 FrmRoot.FrmDirection.TabContainer.TabDirLength.LbLength.Title = " Length ";

 
 // Extrusion
 FrmRoot.FrmExtrude.Title				= " Extrusion ";
 
 // Titres des colonnes de la multi liste 
 NumberColumnTitle		= "N�";
 ExtrudeColumnTitle		= "Extrude";
 SupportColumnTitle		= "Support";
 LengthColumnTitle		= "Length";
 LoftColumnTitle		= "Loft";

 // Contextual menu
 ReframeOnEltItem		= "Reframe On";
 LineRemoveItem			= "Remove ";
 HideItem				= "Hide ";
 ShowItem				= "Show ";
 EditItem				= "Edit ";
 ReverseItem			= "Reverse ";
 LengthItem				= "Length ";

 // Loft sections
 FrmRoot.FrmSection.Title				= " Sections definition ";
 FrmRoot.FrmSection.LbSection1.Title	= " Section1           ";
 FrmRoot.FrmSection.LbSection2.Title	= " Section2           ";
 
 // Loft guide
 FrmRoot.FrmGuide.Title					= " Guide definition ";
 FrmRoot.FrmGuide.LbGuide.Title			= " Reference        ";
 FrmRoot.FrmGuide.LbStartGuide.Title	= " Vertex 1:          ";
 FrmRoot.FrmGuide.LbEndGuide.Title		= " Vertex 2:          ";
 FrmRoot.FrmGuide.LoftInvertProfile.Title	= " Complementary";
 
 // Loft
 FrmRoot.FrmLoft.Title = " Loft ";

 // Help
 FrmRoot.FrmSrf.FrmOper.LongHelp = 
	"Selects the type of operation used to create the parting surface: extrusion or loft.";
 
 FrmRoot.FrmSrf.FrmJoin.JoinCreation.LongHelp = 
	"Joins all the elements of the resulting parting surface";
 
 FrmRoot.FrmSrf.FrmJoin.FrmToler.LbTolJoin.LongHelp = 
	"Defines the merging distance of the resulting Join ";
 FrmRoot.FrmSrf.FrmJoin.FrmToler.EditJoinToler.LongHelp = 
	"Defines the merging distance of the resulting Join ";

 FrmRoot.FrmSrf.FrmJoin.FrmToler.LbTolSmooth.LongHelp = 
	"Defines the maximum deviation of the resulting curve smooth ";
 FrmRoot.FrmSrf.FrmJoin.FrmToler.EditSmoothToler.LongHelp = 
	"Defines the maximum deviation of the resulting curve smooth ";
 
 FrmRoot.FrmSupport.LongHelp = 
	"Displays the name of the element on which you will create the parting surface.";
 
 FrmRoot.FrmProfile.LongHelp = 
	"Displays the vertices limiting the profile of the parting surface you are creating.
Once you have selected the profile support, the available vertices are displayed in white.
Pick the ones you need. The profile is displayed in green. 
Use the contextual menu to create additional points.";
 
 FrmRoot.FrmProfile.InvertProfile.LongHelp = 
 " Switches to the profile complementary to that proposed.";

 FrmRoot.FrmGuide.LongHelp = 
	"Displays the vertices limiting the guide of the parting surface you are creating.";

 FrmRoot.FrmGuide.LoftInvertProfile.LongHelp =
	" Switches to the profile complementary to that proposed.";

 FrmRoot.FrmSection.LongHelp = 
	"Displays the sections of the parting surface you are creating.";

 FrmRoot.FrmDirection.LongHelp = 
	"Defines the extrusion direction:
Pick a sketch,
The extraction direction is located in the plane of the sketch 
and is normal to the selected edge.";

 FrmRoot.FrmExtrude.LongHelp = 
	"Lists the details of the extrusions you have already created in this operation. 
A contextual menu is available to reframe on the extrusion, remove it, hide it or show it.
When you select an element in the list, it is highlighted in the graphic area.";

 FrmRoot.FrmLoft.LongHelp = 
	"Lists the details of the lofts you have already created in this operation. 
A contextual menu is available to reframe on the loft, remove it, hide it or show it.
When you select an element in the list, it is highlighted in the graphic area.";
