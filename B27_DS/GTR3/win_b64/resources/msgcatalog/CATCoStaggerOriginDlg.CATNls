// DO NOT EDIT :: THE CAA2 WIZARD WILL REPLACE ALL THE CODE HERE
Title = "Stagger Origin";
pFramePliesGroup.pLabelPliesGroup.Title = "Plies group ";
pFrameParameters.Title = "Parameters";
pFrameParameters.pRadioButtonWithMinimumStagger.Title = "with minimum stagger indices";
pFrameParameters.pRadioButtonWithMinimumStagger.ShortHelp="When selected, a minimum number of stagger indices is targeted";
pFrameParameters.pRadioButtonWithMinimumStagger.Help="When selected, a minimum number of stagger indices is targeted (if possible, only 0 & 1)";
pFrameParameters.pRadioButtonWithMinimumStagger.LongHelp="When selected, a minimum number of stagger indices is targeted (if possible, only 0 & 1)";

pFrameParameters.pRadioButtonWithMaximumStagger.Title = "with maximum stagger indices";
pFrameParameters.pRadioButtonWithMaximumStagger.ShortHelp="When selected, all possible stagger indices are used";
pFrameParameters.pRadioButtonWithMaximumStagger.Help="When selected, all possible stagger indices are used, target is to use dispatch value in full range";
pFrameParameters.pRadioButtonWithMaximumStagger.LongHelp="When selected, all possible stagger indices are used, target is to use dispatch value in full range";

pFrameParameters.pLabelMinimumStaggerValue.Title = "Minimum Stagger Value";
pFrameParameters.pSpinnerMinimumStagger.ShortHelp="Minimum distance between two stagger indices";
pFrameParameters.pSpinnerMinimumStagger.Help="Minimum distance between two stagger indices to get optimum shear strength";
pFrameParameters.pSpinnerMinimumStagger.LongHelp="Minimum distance between two stagger indices to get optimum shear strength";

pFrameParameters.pLabelWidth.Title = "Width";
pFrameParameters.pSpinnerWidth.ShortHelp="Material width of material";
pFrameParameters.pSpinnerWidth.Help="Material width of material (i.e. tape width)";
pFrameParameters.pSpinnerWidth.LongHelp="Material width of material (i.e. tape width)";

pFrameParameters.pLabelGap.Title = "Gap";
pFrameParameters.pSpinnerGap.ShortHelp="Gap value between two adjacent tapes";
pFrameParameters.pSpinnerGap.Help="Gap value between two adjacent tapes";
pFrameParameters.pSpinnerGap.LongHelp="Gap value between two adjacent tapes";

pFrameParameters.pLabelTolerance.Title = "Tolerance";
pFrameParameters.pSpinnerTolerance.ShortHelp="Maximum allowed distance between origin point and stagger index curve";
pFrameParameters.pSpinnerTolerance.Help="Maximum allowed distance between specific origin point and stagger origin calculated from stagger index";
pFrameParameters.pSpinnerTolerance.LongHelp="Maximum allowed distance between specific origin point and stagger origin calculated from stagger index";

pFrameParameters.pLabelMinimumNbPlies.Title = "Minimum nb plies";
pFrameParameters.pSpinnerMinimumNbPlies.ShortHelp="Minimum number of plies that must be located between two plies to isolate them";
pFrameParameters.pSpinnerMinimumNbPlies.Help="Minimum number of plies (covering more than the common area and having a different orientation) that must be located between two plies to isolate them";
pFrameParameters.pSpinnerMinimumNbPlies.LongHelp="Minimum number of plies (covering more than the common area and having a different orientation) that must be located between two plies to isolate them";

pFrameOptimization.Title = "Optimization";
pFrameOptimization.pRadioButtonGeodesicParallel.Title = "None (geodesic parallels)";
pFrameOptimization.pRadioButtonWithFallback.Title = "Euclidian parallels at geodesic distance point";
pFrameOptimization.pRadioButtonWithPointOnGeodesicLine.Title = "Points on geodesic line";

pFrameTable.Title = "Table";
pFrameTable.pFrameTableUp.pFrameFilter.pLabelFilter.Title = "filter";
pFrameTable.pFrameTableUp.pFrameFilter.pLabelHSP.Title = "Homogeneous set";
pFrameTable.pFrameTableUp.pFrameFilter.pComboHSP.Help="Display only selected Homogeneous Set of Plies";
pFrameTable.pFrameTableUp.pFrameFilter.pComboHSP.ShortHelp="Display only selected Homogeneous Set of Plies (same rosette, ref. point and orientation)";
pFrameTable.pFrameTableUp.pFrameFilter.pComboHSP.LongHelp="Display only selected Homogeneous Set of Plies (same rosette, ref. point and orientation)";

pFrameTable.pFrameTableUp.pFrameFilter.pLabelRosette.Title = "Rosette";
pFrameTable.pFrameTableUp.pFrameFilter.pComboRosette.Help="Display only selected rosette";
pFrameTable.pFrameTableUp.pFrameFilter.pComboRosette.ShortHelp="Display only selected rosette";
pFrameTable.pFrameTableUp.pFrameFilter.pComboRosette.LongHelp="Display only selected rosette";

pFrameTable.pFrameTableUp.pFrameFilter.pLabelReference.Title = "Reference";
pFrameTable.pFrameTableUp.pFrameFilter.pComboReference.Help="Display only selected reference point";
pFrameTable.pFrameTableUp.pFrameFilter.pComboReference.ShortHelp="Display only selected reference point";
pFrameTable.pFrameTableUp.pFrameFilter.pComboReference.LongHelp="Display only selected reference point";

pFrameTable.pFrameTableUp.pFrameFilter.pLabelOrientation.Title = "Orientation";
pFrameTable.pFrameTableUp.pFrameFilter.pComboOrientation.Help="Display only selected orientation";
pFrameTable.pFrameTableUp.pFrameFilter.pComboOrientation.ShortHelp="Display only selected orientation";
pFrameTable.pFrameTableUp.pFrameFilter.pComboOrientation.LongHelp="Display only selected orientation";

pFrameTable.pFrameTableUp.pFrameButton.pPushButtonCompute.Title = "COMPUTE";
pFrameTable.pFrameTableUp.pFrameButton.pPushButtonCompute.Help="Launch computation of all missing stagger origin points";
pFrameTable.pFrameTableUp.pFrameButton.pPushButtonCompute.ShortHelp="Launch computation of all missing stagger origin points";
pFrameTable.pFrameTableUp.pFrameButton.pPushButtonCompute.LongHelp="Launch computation of all missing stagger origin points and analysis status of all plies";

pFrameTable.pFrameTableUp.pFrameButton.pPushButtonCheck.Title = "CHECK";
pFrameTable.pFrameTableUp.pFrameButton.pPushButtonCheck.Help="Launch analysis of all plies";
pFrameTable.pFrameTableUp.pFrameButton.pPushButtonCheck.ShortHelp="Launch analysis of all plies";
pFrameTable.pFrameTableUp.pFrameButton.pPushButtonCheck.LongHelp="Launch analysis of stagger analysis for all plies ";
 
pFrameTable.pStatusMultiList.Column1.Title = "Ply";
pFrameTable.pStatusMultiList.Column1.Width = "5";
pFrameTable.pStatusMultiList.Column2.Title = "Homogeneous set";
pFrameTable.pStatusMultiList.Column2.Width = "5";
pFrameTable.pStatusMultiList.Column3.Title = "Rosette";
pFrameTable.pStatusMultiList.Column3.Width = "5";
pFrameTable.pStatusMultiList.Column4.Title = "Reference";
pFrameTable.pStatusMultiList.Column4.Width = "5";
pFrameTable.pStatusMultiList.Column5.Title = "Orientation";
pFrameTable.pStatusMultiList.Column5.Width = "5";
pFrameTable.pStatusMultiList.Column6.Title = "Origin point";
pFrameTable.pStatusMultiList.Column6.Width = "6";
pFrameTable.pStatusMultiList.Column7.Title = "Stagger index";
pFrameTable.pStatusMultiList.Column7.Width = "5";
pFrameTable.pStatusMultiList.Column8.Title = "OK/KO";
pFrameTable.pStatusMultiList.Column8.Width = "5";
pFrameTable.pStatusMultiList.Column9.Title = "Status";
pFrameTable.pStatusMultiList.Column9.Width = "12";

pFrameTable.pStatusMultiList.Column1.Help="Information and status for each ply";
pFrameTable.pStatusMultiList.Column1.ShortHelp="Information and status for each ply";
pFrameTable.pStatusMultiList.Column1.LongHelp="Information and status for each ply";
 
pFrameTable.pPushButtonExportTable.Title = "Export table ...";
pFrameTable.pPushButtonExportTable.Help="Export status table into a Excel/text file";
pFrameTable.pPushButtonExportTable.ShortHelp="Export status table into a Excel/text file";
pFrameTable.pPushButtonExportTable.LongHelp="Export status table into a Excel/text file";

pFramePointManagement.Title = "Point management";
pFramePointManagement.pLabelReferencePoint.Title = "Reference";

pFramePointManagement.pEditorReferencePoint.Help="Current / New reference point for selected plies";
pFramePointManagement.pEditorReferencePoint.ShortHelp="Current / New reference point for selected plies";
pFramePointManagement.pEditorReferencePoint.LongHelp="Current / New reference point for selected plies";

pFramePointManagement.pPushButtonResetDefault.Title = "reset default";
pFramePointManagement.pPushButtonResetDefault.Help  = "reset reference point";
pFramePointManagement.pPushButtonResetDefault.ShortHelp="reset reference point as origin of the rosette (default)";
pFramePointManagement.pPushButtonResetDefault.LongHelp="reset reference point as origin of the rosette (default)";

pFramePointManagement.pEditorOriginPoint.Help="Current / New origin point for selected plies";
pFramePointManagement.pEditorOriginPoint.ShortHelp="Current / New origin point for selected plies";
pFramePointManagement.pEditorOriginPoint.LongHelp="Current / New origin point for selected plies";

pFramePointManagement.pLabelOrigin.Title = "Origin";
pFramePointManagement.pPushButtonResetAsComputed.Title = "reset as computed";
pFramePointManagement.pPushButtonResetAsComputed.Help  = "reset origin point as computed";
pFramePointManagement.pPushButtonResetAsComputed.ShortHelp="reset origin point as computed one";
pFramePointManagement.pPushButtonResetAsComputed.LongHelp="reset origin point as computed one";

pFramePointManagement.pLabelModifyFromStagger.Title = "Modify from Stagger index";
pFramePointManagement.pComboModifySI.Help  = "Define origin point from its stagger index";
pFramePointManagement.pComboModifySI.ShortHelp="Define origin point from its stagger index";
pFramePointManagement.pComboModifySI.LongHelp="Define origin point from its stagger index";

// END WIZARD REPLACEMENT ZONE

ReferencePoint.Default="<default>";
OriginPoint.None="<NONE>";
OriginPoint.Computed="<computed>";
StatusSumUp.UnknownPrefix="? ";
StaggerIndex.None="<NONE>";
