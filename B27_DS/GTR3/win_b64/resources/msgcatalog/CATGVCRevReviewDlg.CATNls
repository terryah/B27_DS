//==============================================================================================
// CATGVCParamCompareDlg Dialog Box ressources (boite de parametres)
// 28/04/09 : RCD : Creation
//==============================================================================================

Title = "Compare for Review";

FrmRootReport.FrmList.Title = "Modifications";
FrmRootReport.FrmList.FrmSource.Old_Only.Title = "Old Only";
FrmRootReport.FrmList.FrmSource.Old_Only.LongHelp = "Shows only the first model (red) geometries";
FrmRootReport.FrmList.FrmSource.New_Only.Title = "New Only";
FrmRootReport.FrmList.FrmSource.New_Only.LongHelp = "Shows only the second model (green) geometries";
FrmRootReport.FrmList.FrmSource.Both_.Title = "Both";
FrmRootReport.FrmList.FrmSource.Both_.LongHelp = "Shows both models (red and green) geometries";
FrmRootReport.FrmList.FrmSource.Filter.Title = "Filter";
FrmRootReport.FrmList.FrmSource.Filter.LongHelp = "Filters modifications according to their current status";
FrmRootReport.FrmList.FrmSource.Transparency_.Title = "Transparency";
FrmRootReport.FrmList.FrmSource.Transparency_.LongHelp = "Tunes the transparency level on identical geometries";

FrmRootReport.FrmList.TabContainer.Part1.Title = "Modifications";
FrmRootReport.FrmList.TabContainer.Part1.LongHelp = "This is the list of modifications, including the identical geometries. 
Use the contextual menu on a line to edit its properties.";
FrmRootReport.FrmList.TabContainer.Part1.MultiList.CxmGapDetected.Reframe_on_default.Title = "Reframe On Default";
FrmRootReport.FrmList.TabContainer.Part1.MultiList.CxmGapDetected.Invert_default_camera.Title = "Invert Default Camera";
FrmRootReport.FrmList.TabContainer.Part1.MultiList.CxmGapDetected.Reframe_on.Title = "Reframe On...";
FrmRootReport.FrmList.TabContainer.Part1.MultiList.CxmGapDetected.Add_camera.Title = "Add Camera";
FrmRootReport.FrmList.TabContainer.Part1.MultiList.CxmGapDetected.Change_status.Title = "Change Status...";
FrmRootReport.FrmList.TabContainer.Part1.MultiList.CxmGapDetected.Change_status.OK.Title = "OK";
FrmRootReport.FrmList.TabContainer.Part1.MultiList.CxmGapDetected.Change_status.KO.Title = "KO";
FrmRootReport.FrmList.TabContainer.Part1.MultiList.CxmGapDetected.Change_status.Ignored.Title = "Ignored";
FrmRootReport.FrmList.TabContainer.Part1.MultiList.CxmGapDetected.Change_status.Not_Inspected.Title = "Not Inspected";
FrmRootReport.FrmList.TabContainer.Part1.MultiList.CxmGapDetected.Change_all_status.Title = "Change All Status...";
FrmRootReport.FrmList.TabContainer.Part1.MultiList.CxmGapDetected.Change_all_status.OK.Title = "OK";
FrmRootReport.FrmList.TabContainer.Part1.MultiList.CxmGapDetected.Change_all_status.KO.Title = "KO";
FrmRootReport.FrmList.TabContainer.Part1.MultiList.CxmGapDetected.Change_all_status.Ignored.Title = "Ignored";
FrmRootReport.FrmList.TabContainer.Part1.MultiList.CxmGapDetected.Change_all_status.Not_Inspected.Title = "Not Inspected";
FrmRootReport.FrmList.TabContainer.Part1.MultiList.CxmGapDetected.Switch_color_display.Title = "Display Color Scale";
FrmRootReport.FrmList.TabContainer.Part1.MultiList.CxmGapDetected.Edit_properties.Title = "Edit Properties...";

FrmRootReport.FrmList.TabContainer.Information.Title = "Information";
FrmRootReport.FrmList.TabContainer.Information.LongHelp = "This is the list of usefull information on modifications. You can edit a color by double-clicking on it.";
FrmRootReport.FrmList.TabContainer.Information.InfoFrameGR.check_0.Title = "Identical";
FrmRootReport.FrmList.TabContainer.Information.InfoFrameGR.check_1.Title = "New";
FrmRootReport.FrmList.TabContainer.Information.InfoFrameGR.check_2.Title = "Old";
FrmRootReport.FrmList.TabContainer.Information.InfoFrame.Distance.Title = "Comparison Criteria";
FrmRootReport.FrmList.TabContainer.Information.InfoFrame.Not_inspected.Title = "Not Inspected";
FrmRootReport.FrmList.TabContainer.Information.InfoFrame.Reported.Title = "Reported";
FrmRootReport.FrmList.TabContainer.Information.InfoFrame.Ignored.Title = "Ignored";

FrmRootReport.Review.Title = "Review";
FrmRootReport.Review.Annotation1.Title = "Annotation1";
FrmRootReport.Review.Annotation1.LongHelp = "Creates a new annotation for text";
FrmRootReport.Review.Annotation2.Title = "Annotation2";
FrmRootReport.Review.Annotation2.LongHelp = "Creates a new annotation for measure between two points";
FrmRootReport.Review.Annotation3.Title = "Annotation3";
FrmRootReport.Review.Annotation3.LongHelp = "Creates a new annotation for the deviation value on a point";
FrmRootReport.Review.Generate.Title = "Generate Report";
FrmRootReport.Review.Generate.LongHelp = "Generates a new Report in the CATReport directory";

FrmRootReport.Review.twin_display.Title = "Twin Display";

FilterAll = "All";
FilterNotInspected = "Not Inspected";
FilterNotIgnored = "Not Ignored";
FilterIgnored = "Ignored";

StatusOK = "OK";
StatusKO = "KO";
StatusIgnored = "Ignored";
StatusNotInspected = "Not Inspected";

ColumnTitleName = "Name";
ColumnTitleMaxDeviation = "Max Deviation";
ColumnTitleStatus = "Status";
ColumnTitleComment = "Comment";
