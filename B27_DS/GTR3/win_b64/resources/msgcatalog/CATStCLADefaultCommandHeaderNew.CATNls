
// Isoparametric Curve 
CATStCLADefaultCommandHeaderNew.IsoparamCrvHead.Title     = "Isoparametric Curve...";
CATStCLADefaultCommandHeaderNew.IsoparamCrvHead.ShortHelp = "Isoparametric Curve";
CATStCLADefaultCommandHeaderNew.IsoparamCrvHead.Help      = "Creates isoparametric curves on a support";
CATStCLADefaultCommandHeaderNew.IsoparamCrvHead.LongHelp  =
"Isoparametric Curve
Creates isoparametric curves on a support";

// Intersect
CATStCLADefaultCommandHeaderNew.IntersectHead.Title = "Intersect...";
CATStCLADefaultCommandHeaderNew.IntersectHead.ShortHelp = "Intersect";
CATStCLADefaultCommandHeaderNew.IntersectHead.Help = "Find intersection between two bodies";
CATStCLADefaultCommandHeaderNew.IntersectHead.LongHelp= 
"Intersect
Finds intersection between two bodies"; 

// Extrude Surface
CATStCLADefaultCommandHeaderNew.ExtrudeSurfaceHead.Title     = "Extrude Surface...";
CATStCLADefaultCommandHeaderNew.ExtrudeSurfaceHead.ShortHelp = "Extrude Surface";
CATStCLADefaultCommandHeaderNew.ExtrudeSurfaceHead.Help      = "Create a surface by extrusion from a curve";
CATStCLADefaultCommandHeaderNew.ExtrudeSurfaceHead.LongHelp  =
"Extrude Surface
Creates a surface by extrusion from a curve";

// Control Points
CATStCLADefaultCommandHeaderNew.FSControlPointsHead.Title = "Control Points...";
CATStCLADefaultCommandHeaderNew.FSControlPointsHead.ShortHelp = "Control Points";
CATStCLADefaultCommandHeaderNew.FSControlPointsHead.Help="Deforms a surface or a curve by its control points";
CATStCLADefaultCommandHeaderNew.FSControlPointsHead.LongHelp=
"Control Points
Modifies a surface or a curve 
moving its control points. 
Manages smoothing and degrees
and allows the mesh display of
several surfaces.";

// Break Surface or Curve
CATStCLADefaultCommandHeaderNew.BreakGeomHead.Title = "Break...";
CATStCLADefaultCommandHeaderNew.BreakGeomHead.ShortHelp = "Break Surface or Curve";
CATStCLADefaultCommandHeaderNew.BreakGeomHead.Help = "Removes a cell from a surface, or break a two-cell surface into two basic surfaces";
CATStCLADefaultCommandHeaderNew.BreakGeomHead.LongHelp =
"Break Surface or Curve
Removes a cell from a surface or break
a two-cell surface into two basic surfaces.";

// Break Curve
CATStCLADefaultCommandHeaderNew.BreakCurGeomHead.Title = "Break Curve";
CATStCLADefaultCommandHeaderNew.BreakCurGeomHead.ShortHelp = "Break Curve";
CATStCLADefaultCommandHeaderNew.BreakCurGeomHead.Help = "Removes a cell from a curve, or break a two-cell curve into two basic curves";
CATStCLADefaultCommandHeaderNew.BreakCurGeomHead.LongHelp =
"Break Curve
Removes a cell from a curve or break
a two-cell curve into two basic curves.";

// Break Surface
CATStCLADefaultCommandHeaderNew.BreakSurGeomHead.Title = "Break Surface";
CATStCLADefaultCommandHeaderNew.BreakSurGeomHead.ShortHelp = "Break Surface";
CATStCLADefaultCommandHeaderNew.BreakSurGeomHead.Help = "Removes a cell from a surface, or break a two-cell surface into two basic surfaces";
CATStCLADefaultCommandHeaderNew.BreakSurGeomHead.LongHelp =
"Break Surface
Removes a cell from a surface or break
a two-cell surface into two basic surfaces.";

// Styling Fillet
CATStCLADefaultCommandHeaderNew.FSSFilletHead.Title = "FSS Styling Fillet...";
CATStCLADefaultCommandHeaderNew.FSSFilletHead.ShortHelp = "Styling Fillet";
CATStCLADefaultCommandHeaderNew.FSSFilletHead.Help = "Creates a styling fillet.";
CATStCLADefaultCommandHeaderNew.FSSFilletHead.LongHelp =
"Styling Fillet
Creates a styling fillet between two sets of elements.";

// Matching Constraint
CATStCLADefaultCommandHeaderNew.ConstraintMatchHead.Title = "Matching Constraint...";
CATStCLADefaultCommandHeaderNew.ConstraintMatchHead.ShortHelp = "Matching Constraint";
CATStCLADefaultCommandHeaderNew.ConstraintMatchHead.Help = "Matches Surface, Curve using constraint's mechanism";
CATStCLADefaultCommandHeaderNew.ConstraintMatchHead.LongHelp = "Matches Surface, Curve using constraint's mechanism";

// Connect Checker Analysis
CATStCLADefaultCommandHeaderNew.NewCCKAnalysisHead.Title = "Connect Checker Analysis...";
CATStCLADefaultCommandHeaderNew.NewCCKAnalysisHead.ShortHelp = "Connect Checker Analysis";
CATStCLADefaultCommandHeaderNew.NewCCKAnalysisHead.Help = "Performs a connection analysis";
CATStCLADefaultCommandHeaderNew.NewCCKAnalysisHead.LongHelp =
"Connect Checker Analysis
checks connections between surfaces/curves
and measures G0, G1, G2 and G3 deviations";

// Cutting Plane Analysis
CATStCLADefaultCommandHeaderNew.CuttingPlaneAnalysisHead.Title = "Cutting Plane Analysis...";
CATStCLADefaultCommandHeaderNew.CuttingPlaneAnalysisHead.ShortHelp =  "Cutting Plane Analysis";
CATStCLADefaultCommandHeaderNew.CuttingPlaneAnalysisHead.Help =  "Analyzes surfaces using plane sections";
CATStCLADefaultCommandHeaderNew.CuttingPlaneAnalysisHead.LongHelp =
"Cutting Plane
Analyzes a set of surfaces 
based on plane intersections.";

// Free Style Dress-Up
CATStCLADefaultCommandHeaderNew.FSVisuDressUpHdr.Title = "Dress-Up...";
CATStCLADefaultCommandHeaderNew.FSVisuDressUpHdr.ShortHelp = "Dress-Up";
CATStCLADefaultCommandHeaderNew.FSVisuDressUpHdr.Help = "Applies chosen dress-up options on current selection";
CATStCLADefaultCommandHeaderNew.FSVisuDressUpHdr.LongHelp = 
"Dress-Up
Applies dress-up options
on the currently selected elements.";

// Mirror Analysis
CATStCLADefaultCommandHeaderNew.MirrorHead.Title = "Mirror Analysis...";
CATStCLADefaultCommandHeaderNew.MirrorHead.ShortHelp = "Mirror Analysis";
CATStCLADefaultCommandHeaderNew.MirrorHead.Help = "Analyses any selected object by its symmetry visualisation";
CATStCLADefaultCommandHeaderNew.MirrorHead.LongHelp= 
"Mirror Symmetry Analysis
Analyses any selected object constructing 
its symmetrical representation";

CATStCLADefaultCommandHeaderNew.MirrorUpdatePropertyHead.Title ="Update graphical properties";
CATStCLADefaultCommandHeaderNew.MirrorUpdatePropertyHead.ShortHelp ="Update graphical properties";
CATStCLADefaultCommandHeaderNew.MirrorUpdatePropertyHead.Help ="Update graphical properties";
CATStCLADefaultCommandHeaderNew.MirrorUpdatePropertyHead.LongHelp= 
"Update graphical properties of Mirror";

// INSERT CST NODE
CATStCLADefaultCommandHeaderNew.InsertCstNodeHead.Title     = "Insert Constraint Set";
CATStCLADefaultCommandHeaderNew.InsertCstNodeHead.ShortHelp = "Insert New Constraint Set";
CATStCLADefaultCommandHeaderNew.InsertCstNodeHead.Help      = "Insert a new Constraint Set";
CATStCLADefaultCommandHeaderNew.InsertCstNodeHead.LongHelp  = "Insert a new Constraint Set";

// Insert Free Form Aanalysis Root Node (Insert Menu)
CATStCLADefaultCommandHeaderNew.InsertDynAnalysisNodeHead.Title     = "Free Form Analysis Set...";
CATStCLADefaultCommandHeaderNew.InsertDynAnalysisNodeHead.ShortHelp = "Insert a new Free Form Analysis Set";
CATStCLADefaultCommandHeaderNew.InsertDynAnalysisNodeHead.Help      = "Insert a new Free Form Analysis Set";
CATStCLADefaultCommandHeaderNew.InsertDynAnalysisNodeHead.LongHelp  = "Insert a new Free Form Analysis Set";

// Insert Free Form Aanalysis Root Node (Contextual SubMenu)
CATStCLADefaultCommandHeaderNew.InsertDynAnalysisNodeCtxSubMenuHead.Title     = "Insert Free Form Analysis Set";
CATStCLADefaultCommandHeaderNew.InsertDynAnalysisNodeCtxSubMenuHead.ShortHelp = "Insert a new Free Form Analysis Set";
CATStCLADefaultCommandHeaderNew.InsertDynAnalysisNodeCtxSubMenuHead.Help      = "Insert a new Free Form Analysis Set";
CATStCLADefaultCommandHeaderNew.InsertDynAnalysisNodeCtxSubMenuHead.LongHelp  = "Insert a new Free Form Analysis Set";

// Extend
CATStCLADefaultCommandHeaderNew.ParamXtendHead.Title     = "Extend...";
CATStCLADefaultCommandHeaderNew.ParamXtendHead.ShortHelp = "Extend";
CATStCLADefaultCommandHeaderNew.ParamXtendHead.Help      = "Extends curves or surfaces of a given length";
CATStCLADefaultCommandHeaderNew.ParamXtendHead.LongHelp  =
"Extend
Extends curves or surfaces
of a given length";

// Planar Patch
CATStCLADefaultCommandHeaderNew.PlanePatchHead.Title     = "Planar Patch";
CATStCLADefaultCommandHeaderNew.PlanePatchHead.ShortHelp = "Planar Patch";
CATStCLADefaultCommandHeaderNew.PlanePatchHead.Help      = "Creates a planar single-patch surfacic element";
CATStCLADefaultCommandHeaderNew.PlanePatchHead.LongHelp  =
"Planar Patch
Creates a planar single-patch surface
starting where you first click.";

// 4 Points Patch
CATStCLADefaultCommandHeaderNew.4PointsPatchHead.Title     = "4-Point Patch";
CATStCLADefaultCommandHeaderNew.4PointsPatchHead.ShortHelp = "4-Point Patch";
CATStCLADefaultCommandHeaderNew.4PointsPatchHead.Help      = "Creates a single-patch surface passing through four points";
CATStCLADefaultCommandHeaderNew.4PointsPatchHead.LongHelp  =
"4-Point Patch
Creates a single-patch surface passing
through four points, where the first three
have to lie on existing geometry.";

// Net
CATStCLADefaultCommandHeaderNew.GenLoftHead.Title     = "Net Surface...";
CATStCLADefaultCommandHeaderNew.GenLoftHead.ShortHelp = "Net Surface";
CATStCLADefaultCommandHeaderNew.GenLoftHead.Help      = "Creates a surface on a net of curves";
CATStCLADefaultCommandHeaderNew.GenLoftHead.LongHelp  =
"Net Surface
Creates a surface from two sets of curves 
namely guides and profiles.";

// Compass Toolbar
CATStCLADefaultCommandHeaderNew.CompassHead.Title     = "Compass Management";
CATStCLADefaultCommandHeaderNew.CompassHead.ShortHelp = "Quick Compass Orientation";
CATStCLADefaultCommandHeaderNew.CompassHead.Help      = "Enables quick compass orientation";
CATStCLADefaultCommandHeaderNew.CompassHead.LongHelp  = 
"Quick Compass Orientation
Enables quick compass orientation
using a dedicated toolbar.";

// Converter Wizard
CATStCLADefaultCommandHeaderNew.SegmentationHead.Title     = "Converter Wizard...";
CATStCLADefaultCommandHeaderNew.SegmentationHead.ShortHelp = "Converter Wizard";
CATStCLADefaultCommandHeaderNew.SegmentationHead.Help      = "Converts an existing curve or surface";
CATStCLADefaultCommandHeaderNew.SegmentationHead.LongHelp  = 
"Converter Wizard
Converts a curve or a surface into NUPBS depending
on the tolerance or according to a specified 
order or maximum number of segments.";

// Stretch View
CATStCLADefaultCommandHeaderNew.StretchViewHead.Title     = "Stretch View";
CATStCLADefaultCommandHeaderNew.StretchViewHead.ShortHelp = "Stretch View";
CATStCLADefaultCommandHeaderNew.StretchViewHead.Help      = "Defines a limited area to apply a specific visualization on part of the screen";
CATStCLADefaultCommandHeaderNew.StretchViewHead.LongHelp  = 
"Stretch View
Indicates a rectangular region in the current
view to be stretched to the maximum visible size.
A scaling factor can be defined.";

// Standard View Manipulation
CATStCLADefaultCommandHeaderNew.DefaultViewManipHead.Title     = "Standard View Manipulation";
CATStCLADefaultCommandHeaderNew.DefaultViewManipHead.ShortHelp = "Standard View Manipulation";
CATStCLADefaultCommandHeaderNew.DefaultViewManipHead.Help      = "Resets view manipulation";
CATStCLADefaultCommandHeaderNew.DefaultViewManipHead.LongHelp  =
"Standard View Manipulation
Resets view manipulation to the standard manipulation.";

// X-Axis Rotation
CATStCLADefaultCommandHeaderNew.PetchRotateViewHead.Title     = "Rotation About X Screen Axis";
CATStCLADefaultCommandHeaderNew.PetchRotateViewHead.ShortHelp = "Rotation About X Screen Axis";
CATStCLADefaultCommandHeaderNew.PetchRotateViewHead.Help      = "Rotates elements about the X screen axis";
CATStCLADefaultCommandHeaderNew.PetchRotateViewHead.LongHelp  =
"Rotation About X Screen Axis
Freezes the rotation of 
the selected elements
about the screen X axis.";

// Y-Axis Rotation
CATStCLADefaultCommandHeaderNew.RollRotateViewHead.Title     = "Rotation About Y Screen Axis";
CATStCLADefaultCommandHeaderNew.RollRotateViewHead.ShortHelp = "Rotation About Y Screen Axis";
CATStCLADefaultCommandHeaderNew.RollRotateViewHead.Help      = "Rotates elements about the Y screen axis";
CATStCLADefaultCommandHeaderNew.RollRotateViewHead.LongHelp  =
"Rotation About Y Screen Axis
Freezes the rotation of 
the selected elements
about the screen Y axis.";

// Z-Axis Rotation
CATStCLADefaultCommandHeaderNew.LacetRotateViewHead.Title     = "Rotation About Z Screen Axis";
CATStCLADefaultCommandHeaderNew.LacetRotateViewHead.ShortHelp = "Rotation About Z Screen Axis";
CATStCLADefaultCommandHeaderNew.LacetRotateViewHead.Help      = "Rotates elements about the Z screen axis";
CATStCLADefaultCommandHeaderNew.LacetRotateViewHead.LongHelp  =
"Rotation About Z Screen Axis
Freezes the rotation of 
the selected elements
about the screen Z axis.";

// Zoom And Translate
CATStCLADefaultCommandHeaderNew.ZoomAndTranslateHead.Title     = "Zoom And Translate";
CATStCLADefaultCommandHeaderNew.ZoomAndTranslateHead.ShortHelp = "Zoom And Translate";
CATStCLADefaultCommandHeaderNew.ZoomAndTranslateHead.Help      = "Allows zooming and translation only";
CATStCLADefaultCommandHeaderNew.ZoomAndTranslateHead.LongHelp  =
"Zoom And Translate
Freezes the manipulation capabilities
to translation (vertical and horizontal)
and zooming (in and out).";

// 3D Curve
CATStCLADefaultCommandHeaderNew.3DCurveHead.Title     = "3D Curve...";
CATStCLADefaultCommandHeaderNew.3DCurveHead.ShortHelp = "3D Curve";
CATStCLADefaultCommandHeaderNew.3DCurveHead.Help      = "Creates 3D curves by clicking points either in space or on existing geometry (points,clouds,curves,surfaces)";
CATStCLADefaultCommandHeaderNew.3DCurveHead.LongHelp  =
"3D Curve
Creates a 3D curve passing through
points either in space or on existing geometric elements.
Double-click to end curve creation.";

// Disassemble
CATStCLADefaultCommandHeaderNew.BodyDisassembleHead.Title     = "Disassemble...";
CATStCLADefaultCommandHeaderNew.BodyDisassembleHead.ShortHelp = "Disassemble";
CATStCLADefaultCommandHeaderNew.BodyDisassembleHead.Help      = "Disassembles multi-cell bodies into mono-cell bodies";
CATStCLADefaultCommandHeaderNew.BodyDisassembleHead.LongHelp  = 
"Disassemble
Disassembles multi-cell bodies
into mono-cell bodies.";

// Untrim
CATStCLADefaultCommandHeaderNew.UntrimHead.Title     = "Untrim...";
CATStCLADefaultCommandHeaderNew.UntrimHead.ShortHelp = "Untrim Surface or Curve";
CATStCLADefaultCommandHeaderNew.UntrimHead.Help      = "Untrims previously trimmed surfaces and curves";
CATStCLADefaultCommandHeaderNew.UntrimHead.LongHelp  =
"Untrim Surface or Curve
Untrims previously trimmed
surfaces and curves.";

// Mirror Analysis
CATStCLADefaultCommandHeaderNew.MirrorHead.Title     = "Mirror Analysis...";
CATStCLADefaultCommandHeaderNew.MirrorHead.ShortHelp = "Mirror Analysis";
CATStCLADefaultCommandHeaderNew.MirrorHead.Help      = "Analyses any selected object by its symmetry visualisation";
CATStCLADefaultCommandHeaderNew.MirrorHead.LongHelp  = 
"Mirror Symmetry Analysis
Analyses any selected object constructing 
its symmetrical representation";

// Porcupine Analysis
CATStCLADefaultCommandHeaderNew.CurvaAnalysisHead.Title     = "Porcupine Curvature Analysis...";
CATStCLADefaultCommandHeaderNew.CurvaAnalysisHead.ShortHelp = "Porcupine Curvature Analysis";
CATStCLADefaultCommandHeaderNew.CurvaAnalysisHead.Help      = "Performs a porcupine curvature analysis on any curve";
CATStCLADefaultCommandHeaderNew.CurvaAnalysisHead.LongHelp  =
"Porcupine Curvature Analysis
Analyzes the curvature of
a set of curves using 
a porcupine representation.";

// Distance Analysis
CATStCLADefaultCommandHeaderNew.DynDistanceHead.Title     = "Distance Analysis...";
CATStCLADefaultCommandHeaderNew.DynDistanceHead.ShortHelp = "Distance Analysis";
CATStCLADefaultCommandHeaderNew.DynDistanceHead.Help      = "Analyzes distances between two sets of elements";
CATStCLADefaultCommandHeaderNew.DynDistanceHead.LongHelp  =
"Distance Analysis
Analyzes the distance between
a set of curves or surfaces 
and a set of any geometric elements.";

// Geometric Information
CATStCLADefaultCommandHeaderNew.GeomInfoHead.Title     = "Geometric Information";
CATStCLADefaultCommandHeaderNew.GeomInfoHead.ShortHelp = "Geometric Information";
CATStCLADefaultCommandHeaderNew.GeomInfoHead.Help      = "Gives a geometric information on a selected face/edge";
CATStCLADefaultCommandHeaderNew.GeomInfoHead.LongHelp  = 
"Geometric Information
Gives a geometric information on a selected face/edge";

// Isophotes Mapping Analysis
CATStCLADefaultCommandHeaderNew.IsoMappingAnalysisHead.Title     = "Isophotes Mapping...";
CATStCLADefaultCommandHeaderNew.IsoMappingAnalysisHead.ShortHelp = "Isophotes Mapping Analysis";
CATStCLADefaultCommandHeaderNew.IsoMappingAnalysisHead.Help      = "Analyzes surfaces using isophotes mapping";
CATStCLADefaultCommandHeaderNew.IsoMappingAnalysisHead.LongHelp  = 
"Isophotes Mapping
Dynamically applies texture
on a set of surfaces.";

// New Dashboard Launch command
CATStCLADefaultCommandHeaderNew.DBViewHead.Title     = "Dashboard command";
CATStCLADefaultCommandHeaderNew.DBViewHead.ShortHelp = "To visualize Dashboard dialog box";
CATStCLADefaultCommandHeaderNew.DBViewHead.Help      = "Visualize Dashboard dialog box";
CATStCLADefaultCommandHeaderNew.DBViewHead.LongHelp  =
"Visualize Dashboard dialog box
Visualize Dashboard dialog box and
launch the associated command.";

// SET VISIBLE MAPPING
CATStCLADefaultCommandHeaderNew.SetVisibleMappingHead.Title = "Set on top";
CATStCLADefaultCommandHeaderNew.SetVisibleMappingHead.ShortHelp = "Set on top";
CATStCLADefaultCommandHeaderNew.SetVisibleMappingHead.Help = "Set this mapping analysis on top of of the other mapping analysis";
CATStCLADefaultCommandHeaderNew.SetVisibleMappingHead.LongHelp= "Set this mapping analysis on top of of the other mapping analysis";

// Quick Access to Furtive Display Variants.
CATStCLADefaultCommandHeaderNew.FurtiveDisplayVariantsHead.Title     = "Furtive Display Variants";
CATStCLADefaultCommandHeaderNew.FurtiveDisplayVariantsHead.ShortHelp = "Quick Access to Furtive Display Variants";
CATStCLADefaultCommandHeaderNew.FurtiveDisplayVariantsHead.Help      = "Provides easy and quick access to tune Furtive Display Variants";
CATStCLADefaultCommandHeaderNew.FurtiveDisplayVariantsHead.LongHelp  =
"It provides easy and quick access
to tune Furtive Display Variants
while command is running.";

// Drop Compass (F8)
CATStCLADefaultCommandHeaderNew.DropCompassPlaneHead.Title     = "Create compass plane";
CATStCLADefaultCommandHeaderNew.DropCompassPlaneHead.ShortHelp = "Creates plane from compass base";
CATStCLADefaultCommandHeaderNew.DropCompassPlaneHead.Help      = "Creates the plane corresponding to the compass' base";
CATStCLADefaultCommandHeaderNew.DropCompassPlaneHead.LongHelp       =
"Creates the plane
corresponding to the compass' base.";

// Create Axis System from compass
CATStCLADefaultCommandHeaderNew.CreateAxisSystemHead.Title     = "Create axis system";
CATStCLADefaultCommandHeaderNew.CreateAxisSystemHead.ShortHelp = "Create axis system";
CATStCLADefaultCommandHeaderNew.CreateAxisSystemHead.Help      = "Creates axis system from the position of the compass in 3D.";
CATStCLADefaultCommandHeaderNew.CreateAxisSystemHead.LongHelp  =
"Creates axis system
from the position of the compass in 3D.";

// INSERT CST NODE 
CATStCLADefaultCommandHeaderNew.InsertCstNodeHeadInsertMenu.Title     = "Free Form Constraint Set...";
CATStCLADefaultCommandHeaderNew.InsertCstNodeHeadInsertMenu.ShortHelp = "Insert New Constraint Set";
CATStCLADefaultCommandHeaderNew.InsertCstNodeHeadInsertMenu.Help      = "Insert a new Constraint Set";
CATStCLADefaultCommandHeaderNew.InsertCstNodeHeadInsertMenu.LongHelp  = "Insert a new Constraint Set";

// Cutting Plane Analysis
CATStCLADefaultCommandHeaderNew.GlobalCuttingPlaneAnalysisHead.Title = "Global cutting Plane Analysis...";
CATStCLADefaultCommandHeaderNew.GlobalCuttingPlaneAnalysisHead.ShortHelp = "Global cutting Plane Analysis";
CATStCLADefaultCommandHeaderNew.GlobalCuttingPlaneAnalysisHead.Help =  "Analyzes surfaces using plane sections";
CATStCLADefaultCommandHeaderNew.GlobalCuttingPlaneAnalysisHead.LongHelp =
"Global cutting Plane
Analyzes a set of surfaces 
based on plane intersections.";

// New Distance Analysis in Light mode
CATStCLADefaultCommandHeaderNew.ClaNewDynDistanceAnalysisLight.Title  =  "Light Distance Analysis";  
CATStCLADefaultCommandHeaderNew.ClaNewDynDistanceAnalysisLight.ShortHelp  =  "Light Distance Analysis";  
CATStCLADefaultCommandHeaderNew.ClaNewDynDistanceAnalysisLight.Help  =  "Analyzes distances between two sets of elements";  
CATStCLADefaultCommandHeaderNew.ClaNewDynDistanceAnalysisLight.LongHelp  =  
"Distance Analysis Analyzes 
the distance between a set of 
curves or surfaces and a set 
of any geometric elements.";




