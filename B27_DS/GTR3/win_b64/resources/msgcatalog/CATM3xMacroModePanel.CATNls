M3xMacroMode = "Mode";
M3xMacroMode.LongHelp = "Specifies the macro mode which can be:
- along tool axis; the tool moves along the tool axis, 
- along a vector; the tool moves along a vector that 
  you define with the Approach/Retract direction: X/Y/Z boxes, 
- normal; the tool moves in a direction perpendicular to 
  the surface being machined, 
- tangent to movement; the approach/retract is tangent at 
  its end to the rest of the tool path,
- none; no approach/retract,
- back; the tool doubles back along a ramp,
- circular; the tool follows circular arc within
  a user-defined plane,
- box; the tool moves in a straight line or a curve along the diagonal  
across a theoretical box.
- prolonged movement; the tool moves along a slope then up along the tool axis.
- high speed; the tool describes a circular arc tangent to the tool pass.";

M3xMacroModeZL.LongHelp = "Specifies the tool motion from 
the end of one pass to the beginning of the next one. 
It can be:
- along tool axis; the tool moves along the tool axis, 
- ramping; the tool follows a slope defined by 
  the ramping angle, 
- circular; the tool describes a circle defined by the 
  value of Radius 
- circular or ramping; the tool uses either circular or ramping 
  mode depending on whichever is best adapted to the part 
  being machined
- prolonged movement; the tool moves on a sloped line then up 
  along the tool axis.";

M3xMacroModeHMR.LongHelp = "Specifies the engagement of the tool
in the material. It can be : 
- Plunge; the tool plunges vertically, 
- Drilling; the tool plunges into previously drilled 
  holes. You can change the drilling tool diameter, 
  angle and length, 
- Ramping; the tool moves progressively down at the 
  ramping angle, 
- Helix; the tool moves progressively down at the 
  ramping angle with its center along a (vertical) 
  circular helix of Helix diameter. ";
  
M3xZLevelBPDef = "Defined by Linking Approach/Retract";
M3xZLevelBPStraight = "Straight";
