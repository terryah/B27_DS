// ==========================================================================
// ==========================================================================
// SWKHmiReports English Message File
// ==========================================================================
// ==========================================================================

EditReportUndoTitle = "レポートの編集";
AddReportUndoTitle = "レポートの追加";

SWKHmiReports.SelectManikinState.UndoTitle = "マネキンの選択";
SWKHmiReports.SelectManikinState.RedoTitle = "マネキンの選択";
SWKHmiReports.SelectManikinState.Message = "マネキンを選択します...";

SWKHmiReports.ModifyPropState.Message = "レポートのプロパティを修正します...";

ReportLogTitle = "レポート表示 - /p1";
ReportLogDate = "Date";
ReportLogID = "ID";
ReportLogType = "解析のタイプ";
ReportLogInParms = "入力パラメータ";
ReportLogInParmsSepar = "";
ReportLogInParmsYes = "Yes";
ReportLogInParmsNo = "No";
ReportLogAna = "解析";
ReportLogRes1 = "結果 1";
ReportLogRes2 = "結果 2";
ReportLogMan = "Manikin name";
ReportLogItem = "追加情報";
ReportLogRep = "レポート名";
ReportLogSTime = "シミュレーション時間";
ReportLogETime = "所要時間 ";

ReportTitle = "レポート定義";
Main.Name.Title = "名前：     ";
DefaultName = "レポート/p1";
Main.LabelManikin.Title = "マネキン：";
TypeFrame.Title = "解析のタイプ：";
TypeFrame.AvailableLabel.Title = "使用可能";
TypeFrame.SelectedLabel.Title = "選択済み";
LabelOutput.Title = "Output File：";
PushButtonFile.Title = "ファイル...";
TxtFilesOnly = ".txt、.xml または .html ファイルのみが有効です";

XmlNotSupported = "このプラットフォームでは .xml ファイルをサポートしていません。";
XmlReportNotSupported = "シミュレーションでは XML レポートが生成されましたが、この形式はこのプラットフォームではサポートされません。";

TypeAnalysis1 = "プッシュ/プル";
TypeAnalysis2 = "運搬";
TypeAnalysis3 = "RULA";
TypeAnalysis4 = "RULA - 詳細";
TypeAnalysis5 = "生体力学 - 要約";
TypeAnalysis6 = "生体力学 - 脊椎の限界";
TypeAnalysis7 = "生体力学 - Joint Moment";
TypeAnalysis8 = "生体力学 - 応力とモーメント";
TypeAnalysis9 = "生体力学 - セグメントの角度";
TypeAnalysis10 = "姿勢スコア - 全 DOF の結果";
TypeAnalysis11 = "姿勢スコア - 全 DOF の詳細";
TypeAnalysis12 = "姿勢スコア - DOF1";
TypeAnalysis13 = "姿勢スコア - DOF2";
TypeAnalysis14 = "姿勢スコア - DOF3";
TypeAnalysis15 = "拘束の結果";
TypeAnalysis16 = "ビジョン解析";
TypeAnalysis17 = "歩行距離 - 各歩行";
TypeAnalysis18 = "歩行距離 - 累積";

SimulationTimeLabel     = "シミュレーション時間： ";
SimulationDurationLabel = "所要時間： ";

TreeReportLabelSingular = "/p1 (/p2 解析)";
TreeReportLabelPlural   = "/p1 (/p2 解析)";

ErrorReportName = "レポートの名前を指定する必要があります";
ErrorReportType = "レポートする解析を指定する必要があります";
ErrorReportLogSize = "ファイルを完全にロードできませんでした。";
ErrorOutputInvalid = "適切な出力ファイルを指定する必要があります";

PushPullReportName = "プッシュ/プル";
PushPullMaxInitPush = "押すための最大初期力 (/p1)";
PushPullMaxInitPull = "引くための最大初期力 (/p1)";
PushPullMaxSusPush = "押すための最大維持力 (/p1)";
PushPullMaxSusPull = "引くための最大維持力 (/p1)";

CarryReportName = "運搬";
CarryMaximumWeight = "最大許容重量 (/p1)";

RULAReportName = "RULA";
RULADetailsReportName = "RULA 詳細";
RULAGlobalScore = "グローバル スコア";
RULAUpperArmScore = "上腕スコア";
RULAForeArmScore = "前腕スコア";
RULAWristScore = "手首のスコア";
RULAWristTwistScore = "手首のツイスト スコア";
RULAPostureAScore = "姿勢 A スコア";
RULAMuscleScore = "筋肉 スコア";
RULAForceLoadScore = "フォース ロード スコア";
RULAWristArmScore = "リストと腕のスコア";
RULANeckScore = "頸部スコア";
RULATrunkScore = "胴体スコア";
RULALegScore = "脚のスコア";
RULAPostureBScore = "姿勢 B スコア";
RULANeckTrunkLegScore = "頚部、胴体、脚のスコア";

BiomechSummaryReportName = "Bio.サマリー";
BiomechL4L5M = "L4-L5 Moment (/p1)";
BiomechL4L5C = "L4-L5 Compression (/p1)";
BiomechBodyLoadComp = "ボディ ロード圧縮 (/p1)";
BiomechAxialTwistComp = "軸ねじれ圧縮 (/p1)";
BiomechFlexExtComp = "Flex/Ext 圧縮 (/p1)";
BiomechL4L5JointShear = "L4-L5 Joint Shear (/p1)";
BiomechAbdoForce = "腹部外力 (/p1)";
BiomechAbdoPressure = "腹部圧力 (/p1)";
BiomechGroundReactTotX = "Ground Reaction Total (X) (/p1)";
BiomechGroundReactTotY = "Ground Reaction Total (Y) (/p1)";
BiomechGroundReactTotZ = "Ground Reaction Total (Z) (/p1)";
BiomechGroundReactLeftFootX = "Ground Reaction Left Foot (X) (/p1)";
BiomechGroundReactLeftFootY = "Ground Reaction Left Foot (Y) (/p1)";
BiomechGroundReactLeftFootZ = "Ground Reaction Left Foot (Z) (/p1)";
BiomechGroundReactRightFootX = "Ground Reaction Right Foot (X) (/p1)";
BiomechGroundReactRightFootY = "Ground Reaction Right Foot (Y) (/p1)";
BiomechGroundReactRightFootZ = "Ground Reaction Right Foot (Z) (/p1)";

BiomechSpineLimitReportName = "Bio.L4-L5 Spine Limit";
BiomechCompLim = "圧縮制限 (/p1)";
BiomechJointShearLim = "関節部のせん断限界 (/p1)";

BiomechJointMomStrengthReportName = "Bio.Joint Moment Strength";
BiomechRightElbowFlexExt = "右エルボ Flex-Ext (%) (/p1)";
BiomechRightElbowSupPro = "右エルボ Sup-Pro (%) (/p1)";
BiomechLeftElbowFlexExt = "左エルボ Flex-Ext (%) (/p1)";
BiomechLeftElbowSupPro = "左エルボ Sup-Pro (%) (/p1)";
BiomechRightShoulderflexExt = "右肩 Flex-Ext (%) (/p1)";
BiomechRightShoulderAbdAdd = "右肩 Abd-Add (%) (/p1)";
BiomechRightShoulderInterExtRot = "右肩 Inter-Ext Rot. (%) (/p1)";
BiomechLeftShoulderFlexExt = "左肩 Flex-Ex (%) (/p1)";
BiomechLeftShoulderAbdAdd = "左肩 Abd-Add (%) (/p1)";
BiomechLeftShoulderInterExtRot = "左肩 Inter-Ext Rot. (%) (/p1)";
BiomechLumberFlexExt = "腰椎 Flex-Ext (%) (/p1)";
BiomechLumbarRLLatBend = "腰椎 右-左 Lat. Bend (%) (/p1)";
BiomechLumbarRLTwist = "腰椎 右-左ねじれ (%) (/p1)";
BiomechRightWristFlexExt = "右リスト Flex-Ent (%) (/p1)";
BiomechRightWristRadUlnar = "右リスト Rad-Ulnar Dev (%) (/p1)";
BiomechLeftWristFlexExt = "左リスト Flex-Ent (%) (/p1)";
BiomechLeftWristRadUlnar = "左リスト Rad-Ulnar Dev (%) (/p1)";

BiomechXLabel = " X (/p1) (/p2)";
BiomechYLabel = " Y (/p1) (/p2)";
BiomechZLabel = " Z (/p1) (/p2)";
BiomechForceMomReportNameDistal = "Bio. 応力とモーメント (遠位)";
BiomechForceMomReportNameProximal = "Bio. 応力とモーメント (近位)";

BiomechSegLabel = "(/p1) (/p2) (/p3)";
BiomechSegAngle = "Bio セグメントの角度";
BiomechSegCoord = "(/p1, /p2, /p3)";

PosturalScoreReportName = "姿勢スコア";
PosturalScoreAllDOFResult = "すべての DOF 結果 (%)";

PosturalScoreDetailsReportName = "姿勢スコアの詳細";
PosturalScoreDetailsDOF1ReportName = "姿勢スコア DOF1";
PosturalScoreDetailsDOF2ReportName = "姿勢スコア DOF2";
PosturalScoreDetailsDOF3ReportName = "姿勢スコア DOF3";
PosturalScoreCurrentDOFResult = "現在の DOF 結果 (%)";

WholeLeftHand = "左手全体 (%)";
WholeRightHand = "右手全体 (%)";
ScoreData = " (Score)";

CstReportName = "拘束";
CstAngle = "角度 (p1)";
CstDistance = "距離 (/p1)";
CstResolution = "Constraint resolution： ";
CstSuccess = "成功";
CstFail = "Failed";

VisionReportName = "ビジョン解析";
FocalPointDistance = "焦点距離 (/p1)";

WalkDistanceReportName = "プロセス シミュレーション";
WalkDistanceEach = "歩行距離 (/p1)";
WalkDistanceCumulative = "累積歩行距離 (/p1)";

XMLProgress.Title = "XmlReport 進行状況表示バー";
XMLProgress.Comment = "ソート：/p1/ /p2/ /p3  ";

JavaExeError= "Java を実行できません";

JavaVersionError = "Java の認識されないオプションまたは正しくないバージョンが使用されました - 
Version 1.4.2 以上の Java のを使用してください。
詳細についてはローカル管理者にお問い合わせください。";

JavaRunError = "Java コマンドを実行できませんでした。";

