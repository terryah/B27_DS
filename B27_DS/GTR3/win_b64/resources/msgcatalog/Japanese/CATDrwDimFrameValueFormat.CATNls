//=============================================================================
//                                     CNEXT - CXR1
//                          COPYRIGHT DASSAULT SYSTEMES 1997 
//-----------------------------------------------------------------------------
// FILENAME    :    CATDrwDimFrameValueFormat
// LOCATION    :    DraftingIntUI/CNext/resources/msgcatalog
// AUTHOR      :    jmt
// DATE        :    Nov. 03 1997
//------------------------------------------------------------------------------
// DESCRIPTION :    Resources File for NLS purpose related to Drafting WorkShop
//                  
//------------------------------------------------------------------------------
// COMMENTS    :
//------------------------------------------------------------------------------
//MODIFICATIONS     user  date      purpose
//   HISTORY        ----  ----      -------
//     01			fgx	  18.02.99	Ajout des unites d'angle MINUTE et SECOND
//     02			fgx	  08.03.99	Ajout du mode driving
//     03			fgx	  18.11.99	Dim type
//     04			LGK     25.06.02  Revision
//------------------------------------------------------------------------------

_titleDimType.Title="寸法ﾀｲﾌﾟ ";

ONE_PROJECTED_DIM="投影寸法";
SEV_PROJECTED_DIM="投影寸法";
ONE_TRUE_DIM="真の寸法";
SEV_TRUE_DIM="真の寸法";
UNDEFINED="未定義";

_frameDimType._checkDriving.Title="ﾄﾞﾗｲﾋﾞﾝｸﾞ";
_frameDimType._checkDriving.Help="寸法のﾄﾞﾘﾌﾞﾝ/ﾄﾞﾗｲﾋﾞﾝｸﾞ ﾓｰﾄﾞを設定します";
_frameDimType._checkDriving.LongHelp="ﾄﾞﾗｲﾋﾞﾝｸﾞ
寸法にﾄﾞﾘﾌﾞﾝ/ﾄﾞﾗｲﾋﾞﾝｸﾞ ﾓｰﾄﾞを
設定します｡";

_frameDimType._editorDriving.ShortHelp="値";
_frameDimType._editorDriving.Help="形状をﾄﾞﾗｲﾌﾞする場合に寸法値を編集します";
_frameDimType._editorDriving.LongHelp="値
形状をﾄﾞﾗｲﾌﾞする場合に
寸法値を編集します｡";

FrameBase.FrameLayoutTitle.LabelLayoutTitle.Title="値方向 ";
FrameBase.FrameDualValueTitle.LabelDualValueTitle.Title="二重値 ";
FrameFormatTitle.LabelFormatTitle.Title="形式 ";
FrameFakeTitle.CheckButtonFakeTitle.Title="疑似寸法 ";

FrameBase.FrameLayout.LabelReference.Title="基準: ";
FrameBase.FrameLayout.LabelOrientation.Title="方向: ";
FrameBase.FrameLayout.LabelAngle.Title="角度: ";
FrameBase.FrameLayout.LabelInOutValue.Title="位置: ";
FrameBase.FrameLayout.LabelOffset.Title="ｵﾌｾｯﾄ: ";
FrameFormat.LabelValue.Title="ﾒｲﾝ値";
FrameFormat.LabelDualValue.Title="二重値";

FrameFormat._labelUnitOrder.Title="表示: ";
FrameFormat._labelDescription.Title="記述: ";
FrameFormat._frameMoreInfoMain._pushMoreInfoMain.ShortHelp="ﾒｲﾝ記述をﾌﾞﾗｳｽﾞ";
FrameFormat._frameMoreInfoDual._pushMoreInfoDual.ShortHelp="二重記述をﾌﾞﾗｳｽﾞ";

FrameFormat.LabelPrecision.Title="精度: ";
FrameFormat.LabelFormat.Title="形式: ";
FrameFormat.LabelValueFrac.Title="   1 / ";
FrameFormat.LabelDualValueFrac.Title="   1 / ";
FrameFake.LabelFakeMain.Title="ﾒｲﾝ値: ";
FrameFake.LabelFakeDual.Title="二重値: ";
FrameFake.RadioButtonFakeNum.Title="数値";
FrameFake.RadioButtonFakeAlpha.Title="英数字";

FrameBase.FrameDualValue.CheckButtonDualValue.Title="二重値を表示 ";

ComboReferenceLine0="画面";
ComboReferenceLine1="図";
ComboReferenceLine2="寸法線";
ComboReferenceLine3="寸法補助線";

ComboInOutValueAUTO="自動";
ComboInOutValueIN="内側";
ComboInOutValueOUT="外側";

ComboDualValueLine0="下";
ComboDualValueLine1="分数";
ComboDualValueLine2="併記";

ComboValueFormatLine0="小数";
ComboValueFormatLine1="分数";

ComboDualValueFormatLine0="小数";
ComboDualValueFormatLine1="分数";

ComboOrientationLine0="平行";
ComboOrientationLine1="垂直";
ComboOrientationLine2="固定角度";

ComboOrientationLine00="水平";
ComboOrientationLine01="垂直";
ComboOrientationLine02="固定角度";

ONE_FACTOR="1要素";
TWO_FACTORS="2要素";
THREE_FACTORS="3要素";

DefaultFakeText="*疑似*";

FrameBase.FrameLayout.ComboReference.LongHelp="値配置の基準を提供します｡";
FrameBase.FrameLayout.ComboOrientation.LongHelp="基準に関連させて値の方向を
定義します｡";
FrameBase.FrameLayout.SpinnerAngle.LongHelp="基準に関連させて値の角度を
定義します｡";
FrameBase.FrameLayout.SpinnerOffset.LongHelp="寸法線からの値の垂直ｵﾌｾｯﾄを定義します｡";
FrameBase.FrameDualValue.CheckButtonDualValue.LongHelp="二重値を表示
ﾁｪｯｸすると二重値を表示します｡";
FrameBase.FrameDualValue.ComboDualValue.LongHelp="二重値の表示ﾓｰﾄﾞを選択します｡";

FrameFormat._labelUnitOrder.LongHelp="ﾒｲﾝおよび二重値の表示の単位要素の数を設定します｡";
FrameFormat._labelDescription.LongHelp="ﾒｲﾝおよび二重値の数値表示の記述を設定します｡";

FrameFormat.LabelPrecision.LongHelp="ﾒｲﾝおよび二重値の精度を設定します｡";
FrameFormat.LabelFormat.LongHelp="ﾒｲﾝおよび二重値の形式を設定します｡";

FrameFormat._comboUnitOrder.LongHelp="ﾒｲﾝ値の表示の単位要素の数を設定します｡";
FrameFormat._comboDescriptionMain.LongHelp="ﾒｲﾝ値の数値表示の記述を設定します｡";
FrameFormat._frameMoreInfoMain._pushMoreInfoMain.LongHelp="ﾀﾞｲｱﾛｸﾞﾎﾞｯｸｽを表示して､ﾒｲﾝの数値表示の記述を
ﾌﾞﾗｳｽﾞまたは変更します｡";
FrameFormat.ComboValuePrecision.LongHelp="ﾒｲﾝ値の精度を設定します｡";
FrameFormat.ComboValueFormat.LongHelp="ﾒｲﾝ値の形式を設定します｡";

FrameFormat._comboDualUnitOrder.LongHelp="二重値の表示の単位要素の数を設定します｡";
FrameFormat._comboDescriptionDual.LongHelp="二重値の数値表示の記述を設定します｡";
FrameFormat._frameMoreInfoDual._pushMoreInfoDual.LongHelp="ﾀﾞｲｱﾛｸﾞﾎﾞｯｸｽを表示して､二重の数値表示の記述を
ﾌﾞﾗｳｽﾞまたは変更します｡";
FrameFormat.ComboDualValuePrecision.LongHelp="二重値の精度を設定します｡";
FrameFormat.ComboDualValueFormat.LongHelp="二重値の形式を設定します｡";

FrameFakeTitle.CheckButtonFakeTitle.LongHelp="寸法を疑似寸法に変更します｡";

FrameFake.EditorFakeNumMain.LongHelp="ﾒｲﾝ値の数字の見かけの値を表示します｡";
FrameFake.EditorFakeAlphaMain.LongHelp="ﾒｲﾝ値の英数字の見かけの値を表示します｡";
FrameFake.EditorFakeNumDual.LongHelp="二重値の数字の見かけの値を表示します｡";
FrameFake.EditorFakeAlphaDual.LongHelp="二重値の英数字の見かけの値を表示します｡";

SeeChamfer="面取りを参照してください";

