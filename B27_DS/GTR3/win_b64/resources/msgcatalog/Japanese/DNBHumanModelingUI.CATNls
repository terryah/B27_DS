// ==========================================================================
// ==========================================================================
// DNBHumanModelingUI English Message File
// ==========================================================================
// ==========================================================================
MemoWindow.Title="説明 (/p1)";
MemoWindow.Editor.AnthroShortHelp="人体測定のコメント";
MemoWindow.Editor.ManikinShortHelp="マネキンのコメント";
MemoWindow.Editor.AnthroVarableShortHelp="人体測定変数のコメント";
MemoWindow.Editor.VisionShortHelp="視覚のコメント";
MemoWindow.Editor.PostureShortHelp="姿勢のコメント";
MemoWindow.Editor.PrefAngleShortHelp="優先角度のコメント";
PostureLabel.Title="\姿勢";

ReachDistance.Message="誤差範囲に達しました: ";
SelectSegment="セグメントを選択...";
SelectManikin="マネキンを選択...";
PutCompassOnTarget="ターゲットにロボットを配置...";
PlaceCompassAndSelectManikin="ターゲット位置にロボットを配置し、マネキンを選択...";
PlaceCompassAndSelectSegment="ターゲット位置にロボットを配置し、セグメントを選択...";
PlaceCompassToMoveManikin="マネキン '/p1' のターゲット位置にロボットを配置または他のマネキンを選択...";
LeftArmReachEnvelope="左腕の到達エンベロープ";
RightArmReachEnvelope="右腕の到達エンベロープ";
LeftLegReachEnvelope="左脚の到達エンベロープ";
RightLegReachEnvelope="右脚の到達エンベロープ";
LeftArmReachEnvelopeConstruction.Title="左腕の到達エンベロープ構造";
RightArmReachEnvelopeConstruction.Title="右腕の到達エンベロープ構造";
LeftLegReachEnvelopeConstruction.Title="左脚の到達エンベロープ構造";
RightLegReachEnvelopeConstruction.Title="右脚の到達エンベロープ構造";

PushIEdit.Title="編集...";
PushIAdd.Title="追加";
PushIRemove.Title="除去";
PushIPostureSubMenu="姿勢";
PushIPrefAngleSubMenu="設定角度";
PushIAngularLimitationSubMenu="境界設定";
PushIAttachSubMenu="アタッチ";
PushIReset.Title="リセット";
PushIDelete.Title="削除";
PushIFixPermanent.Title="永続固定";
PushIFixOnPermanent.Title="永続的に固定";

VisionNodeName="視覚";

PanelTitleDynClash="動的干渉";
DynClashOffChkBtn.Label="干渉を検出しない ";
DynClashOnChkBtn.Label="干渉を検出して表示 ";
DynClashStopChkBtn.Label="干渉を検出して停止 ";

ActiveDocumentIsNotCATProduct="このコマンドは 'CATProduct' タイプの
ドキュメントからのみ起動できます";

SegmentMustBelongToHandOrFoot="手、指、または足以外のセグメントは選択できません。";
ConfirmSave="変更を保存しますか?";
ConfirmSave.Title="確認";
SaveAs.Title="名前を付けて保存";
TextFile.Title="テキスト ファイル (*.txt)";

DisableAttachConfirm.Title="確認";
DisableAttachConfirm="このマネキンには手でアタッチ (またはピック) された 
1 つ以上のアクティブ拘束があります。
アタッチ (またはピック) を非活動化しますか?

アタッチまたはピックを非活動化した場合、
IK モードを終了したときに再設定されます。

アタッチを無効にする場合は [はい] をクリックします。
アタッチを維持する場合は [いいえ] をクリックします。";

DeletePartRelationConfirm="このキネマティクス チェーンに 1 つ以上のアクティブ パーツ リレーションがあります。
このパーツ リレーションを削除しますか?

パーツ リレーションを削除する場合は [はい] をクリックします。
パーツ リレーションをアクティブな状態に維持する場合は [いいえ] をクリックします。";

InternalError="内部エラーです。";
UnknownError="不明なエラーです。";
SegmentSelectionFailed="セグメントの選択に失敗しました。";
LicenseRequired="このオペレーションを実行するにはライセンスが必要です。";

IsNotPartRelation="この拘束に関連付けられた形状が見つかりません。";

CATPartDocCreatedForOffsets="新しい CATPart ドキュメント 
/p1 
が作成されました。";

ManikinPartNumber="マネキン";
ForearmPartNumber="前腕";

SWKConstraintNode="拘束";
SWKLoadNode="荷重";
BodyNodeName="体";
SWKManikinPostureNode="姿勢";
SWKManikinPositionNode="位置";
SWKManikinPropertiesNode="プロファイル";
SWKManikinSettingsNode="設定";
SWKPropDisplayNode="表示";
SWKPropColouringNode="カラーリング";
SWKPropAppearanceNode="外観";
SWKPropAttachesNode="アタッチ";
SWKPropReferentialNode="参照";
SWKPropIKBehavioursNode="IK ビヘイビア";
SWKPropSegmentsOffsetsNode="オフセット";
SWKLineOfSightNode="視線";
SWKPropAngLimsNode="角度制限";
SWKPropAngLimsDof1Node="DOF 1";
SWKPropAngLimsDof2Node="DOF 2";
SWKPropAngLimsDof3Node="DOF 3";
SWKPropPrefAngsNode="設定角度";
SWKPropPrefAngsDof1Node="DOF 1";
SWKPropPrefAngsDof2Node="DOF 2";
SWKPropPrefAngsDof3Node="DOF 3";
SWKReportsNode="レポート";
SWKEnterNewReportPath="新規レポートのパスを入力";
SWKReportPathOutputFile="出力ファイル ";
BrowseButtonTitle="参照...";
TxtFilesOnly=".txt または .html ファイルが有効です";

SegmentTrunkNodeName="背骨";
SegmentLumbarSpinesNodeName="腰部";
SegmentThoracicSpinesNodeName="胸部";
SegmentNeckSpinesNodeName="頭部";
SegmentRightArmNodeName="右腕";
SegmentRightFingersNodeName="右指";
SegmentRightThumbNodeName="親指";
SegmentRightIndexNodeName="人さし指";
SegmentRightMiddleFingerNodeName="中指";
SegmentRightAnnularNodeName="薬指";
SegmentRightAuricularNodeName="小指";
SegmentLeftArmNodeName="左腕";
SegmentLeftFingersNodeName="左指";
SegmentLeftThumbNodeName="親指";
SegmentLeftIndexNodeName="人さし指";
SegmentLeftMiddleFingerNodeName="中指";
SegmentLeftAnnularNodeName="薬指";
SegmentLeftAuricularNodeName="小指";
SegmentRightLegNodeName="右脚";
SegmentLeftLegNodeName="左脚";

SelectionNull="オブジェクトが選択されていません。";

SWKManikinEditor.Title="マネキン";
SWKProfileEditor.Title="プロファイル";
SWKSettingsEditor.Title="設定";
SWKManikinAppearanceTabPage.Title="外観";
SWKManikinDisplayTabPage.Title="表示";
SWKManikinColorTabPage.Title="カラーリング";
SWKManikinVisionTabPage.Title="視覚";
SWKManikinAttachTabPage.Title="アタッチ";
SWKManikinAnthroTabPage.Title="人体測定";
SWKManikinReferentialTabPage.Title="参照";
SWKManikinIKBehaviorsTabPage.Title="キネマティクス ソルバ設定 (IK ビヘイビア)";
SWKManikinSegmentsOffsetsTabPage.Title="オフセット";
SWKManikinLimitationsTabPage.Title="角度制限";
SWKManikinPrefAngsTabPage.Title="設定角度";

SWKVisionEditor.Title="視覚";
SWKSegmentsOffsetsEditor.Title="オフセット";
SWKLimitationsEditor.Title="角度制限";
SWKPrefAngsEditor.Title="設定角度";
SWKManikinPropertiesEditor.Title="マネキン";
SWKDisplayEditor.Title="表示";
SWKColouringEditor.Title="カラーリング";
SWKAppearanceEditor.Title="外観";
SWKIKBehavioursEditor.Title="キネマティクス ソルバ設定 (IK ビヘイビア)";
SWKAttachesEditor.Title="アタッチ";
SWKReferentialEditor.Title="参照";
SWKAnthroEditor.Title="人体測定";

ContextualSubMenu.EditVision.Title="視覚ウィンドウを開く"; // @validatedUsage brf 13:10:31 window

InvalidInput="無効な入力。
このコマンドでは前腕は使用できません。";

InvalidSegmentForReach="無効なセグメント。
骨盤、腰部、または胸部を到達に使用できません。";

UnEditableManikin="無効な入力です。
選択したマネキンは修正できません。";


NoMoreIKInThisMode="このコンテキストではインバース キネマティクスを実行できなくなりました。
このマネキンで IK モードを使用するには、まず親プロダクトを
活動化 (編集) する必要があります。";

NoMoreIKRootResource="このコンテキストではインバース キネマティクスを実行できません:
このマネキン リソースはルート プロダクトです。";


DOFLockedMsg="この DOF は現在ロックされています。";

WrongActiveProductForIK="このマネキンにインバース キネマティクス モードを適用できません。
親プロダクト ('/p2')
 がアクティブではないことが原因です。

マネキン '/p1' で IK モードを使用するには、まず親プロダクトを
活動化 (編集) する必要があります。";

CannotExitWorkbench="終了に失敗しました。
 マネキンが見つかりません。";

Locked="ロック済み";

NewLine="\n";
OneSpaceWithNewLine=" \n";
Information="情報を非表示/表示";
InfoMessage="このノードで [非表示/表示] コマンドを使用できません";
NoEditingAvailable="このコンテキストではオペレーションの編集はできません。";
PICKConstraintNotEditable="このコンテキストでは拘束のピックを編集できません。";

BrowserDoc.Title="ファイルの選択";

Text="テキスト ファイル (*.txt)";
Html="HTML ファイル (*.html)";
Xml="XML ファイル (*.xml)";
All="すべてのファイル (*.*)";

ActivityNameDlg.Title="レポート ID";
ActivityNameDlg.Label="レポート ID を入力してください:";

RootProductRequired="ヒューマン メジャメント エディターにアクセスする前に、マネキンを作成するか、読み込む必要があります。";

// --------------------------- CCP -------------------------------
OnlyOneLoadPasting="貼り付け対象として選択できる荷重はマネキンごとに 1 つのみです。";
MoreThanOneManikin="コピー対象として選択できるマネキンは 1 つのみです。";

CannotRunCommandOnReference="基準マネキンではこのコマンドを使用できません。";
FreeFeet="参照が脚の間の場合、両脚を固定してマネキンを移動できません。\n移動できるようにするために、両方の固定拘束を削除します。";
FixRelease="固定/リリース";
SwitchSide="側面を切り替え";

//------ Helps content for Manipulate/ Reach/ Grasp toolbar's options -------------
IKModeWorker.Help="ロボットの向きはマネキンの参照に基づきます";
IKModeSegment.Help="ロボットの向きはボディー パーツの参照に基づきます";
ReachFrame.Help="ターゲットはこの位置と向きに到達します";
ReachPoint.Help="ターゲットは位置にのみ到達します";
ReachInGrasp.Help="手のみで到達";
RulerInGrasp.Help="つかむ前に手の開き具合を調整する";
CylindricalGrasp.Help="シリンダ状グラスプでオブジェクトをつかむ";
SphericalGrasp.Help="球状グラスプ でオブジェクトをつかむ";
PinchGrasp.Help="手でオブジェクトをつまむ";

