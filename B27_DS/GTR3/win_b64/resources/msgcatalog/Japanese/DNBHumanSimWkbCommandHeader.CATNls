// COPYRIGHT DELMIA CORP. 2000
//===================================================================
//
// DNBHumanSimWkbCommandHeader.CATNls
//
//===================================================================
//  Sep 2000  Creation:                                        RSQ
//  Dec 2001  Added entries for DNBHTSDelay,					   VGL
//			  Viewpoint,Visibility and TextMessage activity
//
//  Dec 2001  Added entries for all other commands on PAR request JVT
//  Dec 2001  Commented out parts relating to DNBSimHeaders's commands 
//            to solve one missing icon problem     -  YMS
//  Feb 2002  Added entry for HTSUpdateWorld				   VGL
//  Apr 2003  Added entry for climbing activities               LFS
//  Apr 2003  Added entry for line tracking cmd                 ATJ
//  Jun 25 2003 - Added messages for r12 Task Energy Expenditure - JVT
//  Nov 12 2003 - Removed entry for HTSUpdateWorld              VGL

//===================================================================

// Added by JVT 12/18/2001
DNBHumanSimWkbCommandHeader.LinkDELMIA.Title="アクティビティ間のリンク";
DNBHumanSimWkbCommandHeader.LinkDELMIA.Help="選択した両方のアクティビティのリンク";
DNBHumanSimWkbCommandHeader.LinkDELMIA.ShortHelp="アクティビティ間をリンク";
DNBHumanSimWkbCommandHeader.LinkDELMIA.LongHelp="アクティビティを選択し､アクティビティ間のフローを作成します｡";

DNBHumanSimWkbCommandHeader.ProdCtxtDELMIA.Title="プロダクト コンテキスト";
DNBHumanSimWkbCommandHeader.ProdCtxtDELMIA.Help="プロダクトのインポート";
DNBHumanSimWkbCommandHeader.ProdCtxtDELMIA.ShortHelp="プロダクト コンテキスト";
DNBHumanSimWkbCommandHeader.ProdCtxtDELMIA.LongHelp="ダイアログ ボックスを開いて､
プロダクトをインポートします｡";

DNBHumanSimWkbCommandHeader.ResCtxtDELMIA.Title="リソース コンテキスト";
DNBHumanSimWkbCommandHeader.ResCtxtDELMIA.Help="リソースのインポート";
DNBHumanSimWkbCommandHeader.ResCtxtDELMIA.ShortHelp="リソース コンテキスト";
DNBHumanSimWkbCommandHeader.ResCtxtDELMIA.LongHelp="ダイアログ ボックスを開いて､
リソースをインポートします。";

DNBHumanSimWkbCommandHeader.AssignProdDPM.Title="項目を割り当て";
DNBHumanSimWkbCommandHeader.AssignProdDPM.Help="選択したアクティビティへのアイテムの割り当て";
DNBHumanSimWkbCommandHeader.AssignProdDPM.ShortHelp="項目を割り当て";
DNBHumanSimWkbCommandHeader.AssignProdDPM.LongHelp="割り当てるアイテムおよびアクティビティを
選択します";

DNBHumanSimWkbCommandHeader.UnassignProdDPM.Title="項目を割り当て解除";
DNBHumanSimWkbCommandHeader.UnassignProdDPM.Help="選択したアクティビティへのアイテムを割り当て解除します";
DNBHumanSimWkbCommandHeader.UnassignProdDPM.ShortHelp="項目を割り当て解除";
DNBHumanSimWkbCommandHeader.UnassignProdDPM.LongHelp="アクティビティを選択します";

DNBHumanSimWkbCommandHeader.AssignResDPM.Title="リソースの割り当て";
DNBHumanSimWkbCommandHeader.AssignResDPM.Help="選択したアクティビティへのリソースの割り当て";
DNBHumanSimWkbCommandHeader.AssignResDPM.ShortHelp="リソースを割り当て";
DNBHumanSimWkbCommandHeader.AssignResDPM.LongHelp="割り当てるリソースおよびアクティビティを
選択します";

DNBHumanSimWkbCommandHeader.UnassignResDPM.Title="リソースの割り当て解除";
DNBHumanSimWkbCommandHeader.UnassignResDPM.Help="選択したアクティビティへのリソースの割り当て解除";
DNBHumanSimWkbCommandHeader.UnassignResDPM.ShortHelp="リソースを割り当て解除";
DNBHumanSimWkbCommandHeader.UnassignResDPM.LongHelp="アクティビティを選択します";

DNBHumanSimWkbCommandHeader.RemoveFromPPRLists.Title="PPRリスト から除去";
DNBHumanSimWkbCommandHeader.RemoveFromPPRLists.Help="PPRリスト から除去";
DNBHumanSimWkbCommandHeader.RemoveFromPPRLists.ShortHelp="PPRリスト から除去";
DNBHumanSimWkbCommandHeader.RemoveFromPPRLists.LongHelp="プロダクトまたはリソース リストから除去します";

DNBHumanSimWkbCommandHeader.UseProcLibDELMIA.Title="使用するプロセス ライブラリの追加";
DNBHumanSimWkbCommandHeader.UseProcLibDELMIA.Help="使用するプロセス ライブラリの追加";
DNBHumanSimWkbCommandHeader.UseProcLibDELMIA.ShortHelp="使用するプロセス ライブラリを追加";
DNBHumanSimWkbCommandHeader.UseProcLibDELMIA.LongHelp="使用するプロセス ライブラリを追加する";

DNBHumanSimWkbCommandHeader.LayoutPERTDPM.Title="PERT グラフのレイアウト";
DNBHumanSimWkbCommandHeader.LayoutPERTDPM.Help="現行 PERT チャートのレイアウト";
DNBHumanSimWkbCommandHeader.LayoutPERTDPM.ShortHelp="PERT チャートをレイアウト";
DNBHumanSimWkbCommandHeader.LayoutPERTDPM.LongHelp="アクティビティを選択し､このアクティビティから PERT チャートをレイアウトします";

DNBHumanSimWkbCommandHeader.DocumentProcDPM.Title="ドキュメント";
DNBHumanSimWkbCommandHeader.DocumentProcDPM.Help="このプロセスに対するユーザー定義ドキュメンの生成";
DNBHumanSimWkbCommandHeader.DocumentProcDPM.ShortHelp="ドキュメントを生成";
DNBHumanSimWkbCommandHeader.DocumentProcDPM.LongHelp="ドキュメントとスクリプトへのプロセスを
選択可能にするダイアログを
開きます｡";

DNBHumanSimWkbCommandHeader.AddURLDPM.Title="ハイパーリンクの追加";
DNBHumanSimWkbCommandHeader.AddURLDPM.Help="アクティビティにハイパーリンクの追加 ";
DNBHumanSimWkbCommandHeader.AddURLDPM.ShortHelp="ハイパーリンクを追加";
DNBHumanSimWkbCommandHeader.AddURLDPM.LongHelp="アクティビティを選択して、ハイパーリンクを追加します";

DNBHumanSimWkbCommandHeader.OpenURLDPM.Title="ハイパーリンクを開く";
DNBHumanSimWkbCommandHeader.OpenURLDPM.Help="アクティビティでハイパーリンクを開く ";
DNBHumanSimWkbCommandHeader.OpenURLDPM.ShortHelp="ハイパーリンクを開く";
DNBHumanSimWkbCommandHeader.OpenURLDPM.LongHelp="アクティビティを選択して、ハイパーリンクを開きます";

DNBHumanSimWkbCommandHeader.RenumberDPM.Title="アクティビティをリナンバ";
DNBHumanSimWkbCommandHeader.RenumberDPM.Help="アクティビティをリナンバ";
DNBHumanSimWkbCommandHeader.RenumberDPM.ShortHelp="アクティビティをリナンバ";
DNBHumanSimWkbCommandHeader.RenumberDPM.LongHelp="すべてのアクティビティが固有名を持つようにリナンバします";

DNBHumanSimWkbCommandHeader.ImageToActivity.Title="イメージの添付";
DNBHumanSimWkbCommandHeader.ImageToActivity.ShortHelp="イメージをグラブおよび添付";
DNBHumanSimWkbCommandHeader.ImageToActivity.LongHelp="アクティビティにイメージをグラブおよび添付します";

DNBHumanSimWkbCommandHeader.RecenterActInPertViewer.Title="PERT グラフでアクティビティの再構成";
DNBHumanSimWkbCommandHeader.RecenterActInPertViewer.Help="PERT グラフでアクティビティの再構成";
DNBHumanSimWkbCommandHeader.RecenterActInPertViewer.ShortHelp="PERT グラフでアクティビティを再構成";
DNBHumanSimWkbCommandHeader.RecenterActInPertViewer.LongHelp="アクティビティを選択し、このアクティビティを PERT グラフの中心に配置します";
DNBHumanSimWkbCommandHeader.RecenterActInPertViewer.ErrMsg="選択したアクティビティのレベルで開く PERT グラフはありません";

// end modification JVT 12/18/2001

// Added by VGL 12/17/2001
DNBHumanSimWkbCommandHeader.HTSViewpointActivity.Title="視点アクティビティ";
DNBHumanSimWkbCommandHeader.HTSViewpointActivity.Help="3Dデータの視点を変更するプロセス アクティビティを作成します｡";
DNBHumanSimWkbCommandHeader.HTSViewpointActivity.ShortHelp="視点アクティビティを作成";
DNBHumanSimWkbCommandHeader.HTSViewpointActivity.LongHelp="シミュレーションの実行中に、3D データのビューを変更するプロセス アクティビティを作成します｡";

DNBHumanSimWkbCommandHeader.HTSVisibilityActivity.Title="表示/非表示アクティビティ";
DNBHumanSimWkbCommandHeader.HTSVisibilityActivity.Help="プロダクトの表示/非表示を変更するプロセス アクティビティを作成します｡";
DNBHumanSimWkbCommandHeader.HTSVisibilityActivity.ShortHelp="表示/非表示アクティビティを作成";
DNBHumanSimWkbCommandHeader.HTSVisibilityActivity.LongHelp="シミュレーションの実行中に、プロダクトの表示/非表示を変更するプロセス アクティビティを作成します｡";

DNBHumanSimWkbCommandHeader.HTSTextMessageActivity.Title="テキスト メッセージ アクティビティ";
DNBHumanSimWkbCommandHeader.HTSTextMessageActivity.Help="ダイアログ ボックスに、テキスト メッセージを表示するプロセス アクティビティを作成します｡";
DNBHumanSimWkbCommandHeader.HTSTextMessageActivity.ShortHelp="テキスト メッセージ アクティビティを作成";
DNBHumanSimWkbCommandHeader.HTSTextMessageActivity.LongHelp="シミュレーションの実行中に、ダイアログ ボックスにテキスト メッセージを表示するプロセス アクティビティを作成します｡";

DNBHumanSimWkbCommandHeader.DNBHTSDelay.Title="遅延";
DNBHumanSimWkbCommandHeader.DNBHTSDelay.Help="遅延アクティビティの作成";
DNBHumanSimWkbCommandHeader.DNBHTSDelay.ShortHelp="遅延アクティビティを作成";
DNBHumanSimWkbCommandHeader.DNBHTSDelay.LongHelp="遅延アクティビティを作成します";
// end modification VGL 12/17/2001

//Grab/Release Activities
DNBHumanSimWkbCommandHeader.HTSGrabActivity.Title="オブジェクトのグラブ";
DNBHumanSimWkbCommandHeader.HTSGrabActivity.Help="ﾋｭｰﾏﾝ ﾀｽｸにｸﾞﾗﾌﾞ ｱｸﾃｨﾋﾞﾃｨを作成";
DNBHumanSimWkbCommandHeader.HTSGrabActivity.ShortHelp="ﾋｭｰﾏﾝ ﾀｽｸにｸﾞﾗﾌﾞ ｱｸﾃｨﾋﾞﾃｨを作成";
DNBHumanSimWkbCommandHeader.HTSGrabActivity.LongHelp="シミュレーション中に、2 つのオブジェクトのキネマティック アタッチメント用にグラブ アクティビティを作成します";

DNBHumanSimWkbCommandHeader.HTSReleaseActivity.Title="オブジェクトのリリース";
DNBHumanSimWkbCommandHeader.HTSReleaseActivity.Help="ﾋｭｰﾏﾝ ﾀｽｸにﾘﾘｰｽ ｱｸﾃｨﾋﾞﾃｨを作成";
DNBHumanSimWkbCommandHeader.HTSReleaseActivity.ShortHelp="ﾋｭｰﾏﾝ ﾀｽｸにﾘﾘｰｽ ｱｸﾃｨﾋﾞﾃｨを作成";
DNBHumanSimWkbCommandHeader.HTSReleaseActivity.LongHelp="事前に接続されているオブジェクトの、キネマティック接続解除用リリース アクティビティを作成します";

DNBHumanSimWkbCommandHeader.TeachPosture.Title="MoveToPosture アクティビティ";
DNBHumanSimWkbCommandHeader.TeachPosture.Help="MoveToPosture アクティビティの作成";
DNBHumanSimWkbCommandHeader.TeachPosture.ShortHelp="MoveToPosture アクティビティを作成";
DNBHumanSimWkbCommandHeader.TeachPosture.LongHelp="MoveToPosture アクティビティを作成します";

DNBHumanSimWkbCommandHeader.WalkFwd.Title="Walk Fwd アクティビティ";
DNBHumanSimWkbCommandHeader.WalkFwd.Help="Walk Fwd アクティビティの作成";
DNBHumanSimWkbCommandHeader.WalkFwd.ShortHelp="Walk Fwd アクティビティを作成";
DNBHumanSimWkbCommandHeader.WalkFwd.LongHelp="Walk Fwd アクティビティを作成します";

DNBHumanSimWkbCommandHeader.WalkBwd.Title="Walk Bwd アクティビティ";
DNBHumanSimWkbCommandHeader.WalkBwd.Help="Walk Bwd アクティビティの作成";
DNBHumanSimWkbCommandHeader.WalkBwd.ShortHelp="Walk Bwd アクティビティを作成";
DNBHumanSimWkbCommandHeader.WalkBwd.LongHelp="Walk Bwd アクティビティを作成します";

DNBHumanSimWkbCommandHeader.SideStep.Title="Side Step アクティビティ";
DNBHumanSimWkbCommandHeader.SideStep.Help="Side Step アクティビティの作成";
DNBHumanSimWkbCommandHeader.SideStep.ShortHelp="Side Step アクティビティを作成";
DNBHumanSimWkbCommandHeader.SideStep.LongHelp="Side Step アクティビティを作成します";

DNBHumanSimWkbCommandHeader.AutoWalk.Title="AutoWalk アクティビティ";
DNBHumanSimWkbCommandHeader.AutoWalk.Help="AutoWalk アクティビティの作成";
DNBHumanSimWkbCommandHeader.AutoWalk.ShortHelp="AutoWalk アクティビティを作成";
DNBHumanSimWkbCommandHeader.AutoWalk.LongHelp="AutoWalk アクティビティを作成します";

DNBHumanSimWkbCommandHeader.TrackTrajectory.Title="Track Trajectory アクティビティ";
DNBHumanSimWkbCommandHeader.TrackTrajectory.Help="Track Trajectory アクティビティの作成";
DNBHumanSimWkbCommandHeader.TrackTrajectory.ShortHelp="Track Trajectory アクティビティを作成";
DNBHumanSimWkbCommandHeader.TrackTrajectory.LongHelp="Track Trajectory アクティビティを作成します";

DNBHumanSimWkbCommandHeader.ClimbStair.Title="Climb Stair アクティビティ";
DNBHumanSimWkbCommandHeader.ClimbStair.Help="Climb Stair アクティビティの作成";
DNBHumanSimWkbCommandHeader.ClimbStair.ShortHelp="Climb Stair アクティビティを作成";
DNBHumanSimWkbCommandHeader.ClimbStair.LongHelp="Climb Stair アクティビティを作成します";

DNBHumanSimWkbCommandHeader.ClimbLadder.Title="Climb Ladder アクティビティ";
DNBHumanSimWkbCommandHeader.ClimbLadder.Help="Climb Ladder アクティビティの作成";
DNBHumanSimWkbCommandHeader.ClimbLadder.ShortHelp="Climb Ladder アクティビティを作成";
DNBHumanSimWkbCommandHeader.ClimbLadder.LongHelp="Climb Ladder アクティビティを作成します";

DNBHumanSimWkbCommandHeader.TrackLine.Title="Line Tracking アクティビティ";
DNBHumanSimWkbCommandHeader.TrackLine.Help="Line Tracking アクティビティの作成";
DNBHumanSimWkbCommandHeader.TrackLine.ShortHelp="Line Tracking アクティビティを作成";
DNBHumanSimWkbCommandHeader.TrackLine.LongHelp="このコマンドでは、Line Tracking アクティビティを作成します";

DNBHumanSimWkbCommandHeader.ChngTrackLineWalk.Title="ﾗｲﾝ ﾄﾗｯｷﾝｸﾞ中に歩行ﾀｲﾌﾟを変更";
DNBHumanSimWkbCommandHeader.ChngTrackLineWalk.Help="ﾗｲﾝ ﾄﾗｯｷﾝｸﾞ中に歩行ﾀｲﾌﾟを変更します";
DNBHumanSimWkbCommandHeader.ChngTrackLineWalk.ShortHelp="ﾗｲﾝ ﾄﾗｯｷﾝｸﾞ中に歩行ﾀｲﾌﾟを変更します";
DNBHumanSimWkbCommandHeader.ChngTrackLineWalk.LongHelp="このコマンドでは、ライン トラッキング中に歩行タイプを変更します";

DNBHumanSimWkbCommandHeader.Pick.Title="オブジェクトのピック";
DNBHumanSimWkbCommandHeader.Pick.Help="Pick アクティビティの作成";
DNBHumanSimWkbCommandHeader.Pick.ShortHelp="Pick アクティビティを作成";
DNBHumanSimWkbCommandHeader.Pick.LongHelp="Pick アクティビティを作成します";

DNBHumanSimWkbCommandHeader.Place.Title="オブジェクトの配置";
DNBHumanSimWkbCommandHeader.Place.Help="Place アクティビティの作成";
DNBHumanSimWkbCommandHeader.Place.ShortHelp="Place アクティビティを作成";
DNBHumanSimWkbCommandHeader.Place.LongHelp="Place アクティビティを作成します";

DNBHumanSimWkbCommandHeader.OperateStanding.Title="Operate Standing アクティビティ";
DNBHumanSimWkbCommandHeader.OperateStanding.Help="Operate Standing アクティビティの作成";
DNBHumanSimWkbCommandHeader.OperateStanding.ShortHelp="Operate Standing アクティビティを作成";
DNBHumanSimWkbCommandHeader.OperateStanding.LongHelp="Operate Standing アクティビティを作成します";

DNBHumanSimWkbCommandHeader.ReverseOperateStanding.Title="Reverse Operate Standing アクティビティ";
DNBHumanSimWkbCommandHeader.ReverseOperateStanding.Help="逆順のﾜｰｸﾌﾛｰで立位操作ｱｸﾃｨﾋﾞﾃｨを作成します";
DNBHumanSimWkbCommandHeader.ReverseOperateStanding.ShortHelp="逆順のﾜｰｸﾌﾛｰで立位操作ｱｸﾃｨﾋﾞﾃｨを作成します";
DNBHumanSimWkbCommandHeader.ReverseOperateStanding.LongHelp="逆順のﾜｰｸﾌﾛｰで立位操作ｱｸﾃｨﾋﾞﾃｨを作成します";


DNBHumanSimWkbCommandHeader.OperateWalking.Title="Operate Walking アクティビティ";
DNBHumanSimWkbCommandHeader.OperateWalking.Help="Operate Walking アクティビティの作成";
DNBHumanSimWkbCommandHeader.OperateWalking.ShortHelp="Operate Walking アクティビティを作成";
DNBHumanSimWkbCommandHeader.OperateWalking.LongHelp="Operate Walking アクティビティを作成します";

DNBHumanSimWkbCommandHeader.ReverseOperateWalking.Title="Reverse Operate Walking アクティビティ";
DNBHumanSimWkbCommandHeader.ReverseOperateWalking.Help="逆順のワークフローで、Operate Walking アクティビティの作成";
DNBHumanSimWkbCommandHeader.ReverseOperateWalking.ShortHelp="逆順のワークフローで、Operate Walking アクティビティを作成";
DNBHumanSimWkbCommandHeader.ReverseOperateWalking.LongHelp="逆順のワークフローで、Operate Walking アクティビティを作成します";


DNBHumanSimWkbCommandHeader.HumanActivityGroup.Title="ヒューマン アクティビティ グループ アクティビティ";
DNBHumanSimWkbCommandHeader.HumanActivityGroup.Help="ヒューマン アクティビティ グループ アクティビティの作成";
DNBHumanSimWkbCommandHeader.HumanActivityGroup.ShortHelp="ヒューマン アクティビティ グループ アクティビティを作成";
DNBHumanSimWkbCommandHeader.HumanActivityGroup.LongHelp="ヒューマン アクティビティ グループ アクティビティを作成します";

DNBHumanSimWkbCommandHeader.HumanCallTask.Title="ヒューマン用の Call Task";
DNBHumanSimWkbCommandHeader.HumanCallTask.Help="ヒューマン用の Call Task アクティビティの作成";
DNBHumanSimWkbCommandHeader.HumanCallTask.ShortHelp="ヒューマン用の Call Task アクティビティを作成";
DNBHumanSimWkbCommandHeader.HumanCallTask.LongHelp="ヒューマン用の Call Task アクティビティを作成します";

DNBHumanSimWkbCommandHeader.PartRelations.Title="パーツ関係";
DNBHumanSimWkbCommandHeader.PartRelations.Help="パーツ関係の表示";
DNBHumanSimWkbCommandHeader.PartRelations.ShortHelp="パーツ関係を表示";
DNBHumanSimWkbCommandHeader.PartRelations.LongHelp="パーツ関係を表示します";

DNBHumanSimWkbCommandHeader.PartRelationsCopy.Title="パーツ関係のコピー";
DNBHumanSimWkbCommandHeader.PartRelationsCopy.Help="パーツ関係をコピー";
DNBHumanSimWkbCommandHeader.PartRelationsCopy.ShortHelp="パーツ関係をコピーします";
DNBHumanSimWkbCommandHeader.PartRelationsCopy.LongHelp="パーツ関係をコピーします";

DNBHumanSimWkbCommandHeader.HumanTaskVisu.Title="非表示/表示";
DNBHumanSimWkbCommandHeader.HumanTaskVisu.Help="ヒューマン タスクの非表示/表示";
DNBHumanSimWkbCommandHeader.HumanTaskVisu.ShortHelp="ヒューマン タスクを非表示/表示";
DNBHumanSimWkbCommandHeader.HumanTaskVisu.LongHelp="ヒューマン タスクを非表示/表示します";

DNBHumanSimWkbCommandHeader.MoveHumanTask.Title="移動";
DNBHumanSimWkbCommandHeader.MoveHumanTask.Help="ヒューマン タスクの移動";
DNBHumanSimWkbCommandHeader.MoveHumanTask.ShortHelp="ヒューマン タスクを移動";
DNBHumanSimWkbCommandHeader.MoveHumanTask.LongHelp="ヒューマン タスクを移動します";

DNBHumanSimWkbCommandHeader.UpdatePosture.Title="更新";
DNBHumanSimWkbCommandHeader.UpdatePosture.Help="姿勢の更新";
DNBHumanSimWkbCommandHeader.UpdatePosture.ShortHelp="ポーズを更新";
DNBHumanSimWkbCommandHeader.UpdatePosture.LongHelp="ポーズを更新します";

DNBHumanSimWkbCommandHeader.MoveWorkerActs.Title="移動";
DNBHumanSimWkbCommandHeader.MoveWorkerActs.Help="Worker Activities の移動";
DNBHumanSimWkbCommandHeader.MoveWorkerActs.ShortHelp="Worker Activities を移動";
DNBHumanSimWkbCommandHeader.MoveWorkerActs.LongHelp="Worker Activities を移動します";

DNBHumanSimWkbCommandHeader.RotateWorkerActs.Title="回転";
DNBHumanSimWkbCommandHeader.RotateWorkerActs.Help="Z軸のまわりに Worker Activities の回転";
DNBHumanSimWkbCommandHeader.RotateWorkerActs.ShortHelp="Z軸のまわりに Worker Activities を回転";
DNBHumanSimWkbCommandHeader.RotateWorkerActs.LongHelp="Z軸のまわりに Worker Activities を回転します";

DNBHumanSimWkbCommandHeader.RotateHumanTask.Title="回転";
DNBHumanSimWkbCommandHeader.RotateHumanTask.Help="Z軸のまわりにヒューマン タスの回転";
DNBHumanSimWkbCommandHeader.RotateHumanTask.ShortHelp="Z軸のまわりにヒューマン タスを回転";
DNBHumanSimWkbCommandHeader.RotateHumanTask.LongHelp="Z軸のまわりにヒューマン タスを回転します";

DNBHumanSimWkbCommandHeader.MirrorHumanTask.Title="ヒューマン タスクのミラーリング";
DNBHumanSimWkbCommandHeader.MirrorHumanTask.Help="平面のまわりに選択したタスクのミラーリング";
DNBHumanSimWkbCommandHeader.MirrorHumanTask.ShortHelp=" ヒューマン タスクをミラーリング";
DNBHumanSimWkbCommandHeader.MirrorHumanTask.LongHelp="ヒューマン タスクをミラーリングします";

DNBHumanSimWkbCommandHeader.MirrorWorkerActs.Title="Worker Activities のミラーリング";
DNBHumanSimWkbCommandHeader.MirrorWorkerActs.Help="平面のまわりに選択したアクティビティのミラーリング";
DNBHumanSimWkbCommandHeader.MirrorWorkerActs.ShortHelp=" Worker Activities をミラーリング";
DNBHumanSimWkbCommandHeader.MirrorWorkerActs.LongHelp="Worker Activities をミラーリングします";

DNBHumanSimWkbCommandHeader.SyncMTPWithConstraints.Title="3D モデルの拘束との同期";
DNBHumanSimWkbCommandHeader.SyncMTPWithConstraints.Help="3D モデルの拘束との同期";
DNBHumanSimWkbCommandHeader.SyncMTPWithConstraints.ShortHelp=" 3D モデルを拘束と同期";
DNBHumanSimWkbCommandHeader.SyncMTPWithConstraints.LongHelp="3D モデルを拘束と同期します";


// YMS Start 12/18/2001

// DNBHumanSimWkbCommandHeader.DNBAnalysisConfigCmd.Title="Analysis Configuration";
// DNBHumanSimWkbCommandHeader.DNBAnalysisConfigCmd.ShortHelp="Analysis Configuration";
// DNBHumanSimWkbCommandHeader.DNBAnalysisConfigCmd.Help="Defines a set of interference/distance specifications";
// DNBHumanSimWkbCommandHeader.DNBAnalysisConfigCmd.LongHelp="Defines a set of interference/distance specifications for simulation";

// DNBHumanSimWkbCommandHeader.DNBAnalysisOFF.Title="Analysis Status Off";
// DNBHumanSimWkbCommandHeader.DNBAnalysisOFF.ShortHelp="Analysis Status\n(Off)";
// DNBHumanSimWkbCommandHeader.DNBAnalysisOFF.Help="Deactivate automatic Analysis Status";
// DNBHumanSimWkbCommandHeader.DNBAnalysisOFF.LongHelp="Deactivates automatic Analysis Status for simulation";

// DNBHumanSimWkbCommandHeader.DNBAnalysisON.Title="Analysis Status On";
// DNBHumanSimWkbCommandHeader.DNBAnalysisON.ShortHelp="Analysis Status\n(On)";
// DNBHumanSimWkbCommandHeader.DNBAnalysisON.Help="Activate automatic Analysis Status";
// DNBHumanSimWkbCommandHeader.DNBAnalysisON.LongHelp="Activates automatic Analysis Status for simulation";

// DNBHumanSimWkbCommandHeader.DNBAnalysisSTOP.Title="Analysis Status Stop";
// DNBHumanSimWkbCommandHeader.DNBAnalysisSTOP.ShortHelp="Analysis Status\n(Stop)";
// DNBHumanSimWkbCommandHeader.DNBAnalysisSTOP.Help="Activate automatic Analysis Status on stop mode";
// DNBHumanSimWkbCommandHeader.DNBAnalysisSTOP.LongHelp="Activates automatic Analysis Status stop mode for simulation";

// DNBHumanSimWkbCommandHeader.DNBSpaceClash.Title="Clash";
// DNBHumanSimWkbCommandHeader.DNBSpaceClash.Help= "Analyzes interferences between products";
// DNBHumanSimWkbCommandHeader.DNBSpaceClash.ShortHelp="Clash";
// DNBHumanSimWkbCommandHeader.DNBSpaceClash.LongHelp= "Analyzes interferences between products";

// DNBHumanSimWkbCommandHeader.DNBSpaceMeasure.Title="Distance and Band Analysis";
// DNBHumanSimWkbCommandHeader.DNBSpaceMeasure.Help= "Measures the distance between and runs band analysis on selected products";
// DNBHumanSimWkbCommandHeader.DNBSpaceMeasure.ShortHelp="Distance and Band Analysis";
// DNBHumanSimWkbCommandHeader.DNBSpaceMeasure.LongHelp= "Measures the distance between and runs band analysis on selected products";

// YMS End 12/18/2001

// Reset Posture command
// ---------------------
DNBHumanSimWkbCommandHeader.DELMHmuResetPostureHdr.Title="リセット";
DNBHumanSimWkbCommandHeader.DELMHmuResetPostureHdr.Help="姿勢のリセット";
DNBHumanSimWkbCommandHeader.DELMHmuResetPostureHdr.ShortHelp="姿勢をリセット";
DNBHumanSimWkbCommandHeader.DELMHmuResetPostureHdr.LongHelp="このコマンドでは、選択したマネキンの
デフォルト （立位） を適用します。";


// Mirror Copy Posture command
// ---------------------------
DNBHumanSimWkbCommandHeader.DELMHmuMirrorCopyPostureHdr.Title="ミラー コピー";
DNBHumanSimWkbCommandHeader.DELMHmuMirrorCopyPostureHdr.Help="ミラー コピー";
DNBHumanSimWkbCommandHeader.DELMHmuMirrorCopyPostureHdr.ShortHelp="ミラー コピー";
DNBHumanSimWkbCommandHeader.DELMHmuMirrorCopyPostureHdr.LongHelp="このコマンドでは、マネキン側面の全体的または部分的な姿勢を
もう一方の側面にも適用します。";

// Swap Posture command
// --------------------
DNBHumanSimWkbCommandHeader.DELMHmuSwapPostureHdr.Title="スワップ";
DNBHumanSimWkbCommandHeader.DELMHmuSwapPostureHdr.Help="姿勢のスワップ";
DNBHumanSimWkbCommandHeader.DELMHmuSwapPostureHdr.ShortHelp="姿勢をスワップ";
DNBHumanSimWkbCommandHeader.DELMHmuSwapPostureHdr.LongHelp="このコマンドでは、マネキンの姿勢を
一方の側面からもう一方の側面にスワップします。";

// Process Verification Toolbar 
// ----------------------------
DNBHumanSimWkbCommandHeader.DNBHTSSimSaveInitState.Title="初期状態の保存";
DNBHumanSimWkbCommandHeader.DNBHTSSimSaveInitState.Help="DELMIA 初期状態の保存";
DNBHumanSimWkbCommandHeader.DNBHTSSimSaveInitState.ShortHelp="初期状態を保存";
DNBHumanSimWkbCommandHeader.DNBHTSSimSaveInitState.LongHelp="初期状態を保存します";

DNBHumanSimWkbCommandHeader.DNBHTSSimRestoreInitState.Title="初期状態の復元";
DNBHumanSimWkbCommandHeader.DNBHTSSimRestoreInitState.Help="DELMIA 初期状態の復元";
DNBHumanSimWkbCommandHeader.DNBHTSSimRestoreInitState.ShortHelp="初期状態を復元";
DNBHumanSimWkbCommandHeader.DNBHTSSimRestoreInitState.LongHelp="初期状態を復元します";

DNBHumanSimWkbCommandHeader.DNBHTSSimProcessSimulation.Title="プロセス シミュレーション";
DNBHumanSimWkbCommandHeader.DNBHTSSimProcessSimulation.Help="DELMIA プロセス シミュレーション";
DNBHumanSimWkbCommandHeader.DNBHTSSimProcessSimulation.ShortHelp="プロセス シミュレーション";
DNBHumanSimWkbCommandHeader.DNBHTSSimProcessSimulation.LongHelp="プロセスのシミュレーションを行います";

DNBHumanSimWkbCommandHeader.HTSProcessVerification.Title="プロセス検証";
DNBHumanSimWkbCommandHeader.HTSProcessVerification.Help="DELMIA プロセス検証";
DNBHumanSimWkbCommandHeader.HTSProcessVerification.ShortHelp="プロセスを検証";
DNBHumanSimWkbCommandHeader.HTSProcessVerification.LongHelp="プロセスを検証します";

DNBHumanSimWkbCommandHeader.DELMHmuCatalogBrowserHdr.Title="プロダクトとリソース用のカタログ ブラウザ";
DNBHumanSimWkbCommandHeader.DELMHmuCatalogBrowserHdr.Help="プロダクトとリソース用のカタログ コンポーネントの配置";
DNBHumanSimWkbCommandHeader.DELMHmuCatalogBrowserHdr.ShortHelp="プロダクトとリソース用のカタログ ブラウザ";
DNBHumanSimWkbCommandHeader.DELMHmuCatalogBrowserHdr.LongHelp="プロダクトとリソース用にカタログ コンポーネントを配置します。";
//Messages for HTS Reach and IK Mode Commands...
// Reach Mode (2 directions)
// ------------------------
DNBHumanSimWkbCommandHeader.DELMHTSReachModeDirectionHdr.Title="伸び (位置のみ)";
DNBHumanSimWkbCommandHeader.DELMHTSReachModeDirectionHdr.Help="伸び (位置のみ)";
DNBHumanSimWkbCommandHeader.DELMHTSReachModeDirectionHdr.ShortHelp="伸び (位置のみ)";
DNBHumanSimWkbCommandHeader.DELMHTSReachModeDirectionHdr.LongHelp="このコマンドでは、インバース キネマティクスを使用して、 
マネキンが 2 方向から特定位置に到達するようにします";

// Reach Mode (3 directions)
// -------------------------
DNBHumanSimWkbCommandHeader.DELMHTSReachModeFrameHdr.Title="伸び (位置と方向)";
DNBHumanSimWkbCommandHeader.DELMHTSReachModeFrameHdr.Help="伸び (位置と方向)";
DNBHumanSimWkbCommandHeader.DELMHTSReachModeFrameHdr.ShortHelp="伸び (位置と方向)";
DNBHumanSimWkbCommandHeader.DELMHTSReachModeFrameHdr.LongHelp="このコマンドではインバース キネマティクスを使用して、 
マネキンが 3 方向から特定位置に到達するようにします";

// IK Worker Frame Mode command
// ----------------------------
DNBHumanSimWkbCommandHeader.DELMHTSIKModeWorkerFrmHdr.Title="IK 作業側フレーム モード";
DNBHumanSimWkbCommandHeader.DELMHTSIKModeWorkerFrmHdr.Help="インバース キネマティクス作業側フレーム モード";
DNBHumanSimWkbCommandHeader.DELMHTSIKModeWorkerFrmHdr.ShortHelp="インバース キネマティクス作業側フレーム モード";
DNBHumanSimWkbCommandHeader.DELMHTSIKModeWorkerFrmHdr.LongHelp="このコマンドでは、選択したセグメントに自動コンパス スワップを作成し、コンパスを作業側フレームに
保持したまま IK を活動化します｡";

// IK Segment Frame Mode command
// -----------------------------
DNBHumanSimWkbCommandHeader.DELMHTSIKModeSegFrmHdr.Title="インバース キネマティクス フレーム モード";
DNBHumanSimWkbCommandHeader.DELMHTSIKModeSegFrmHdr.Help="インバース キネマティクス部分フレーム モード";
DNBHumanSimWkbCommandHeader.DELMHTSIKModeSegFrmHdr.ShortHelp="インバース キネマティクス部分フレーム モード";
DNBHumanSimWkbCommandHeader.DELMHTSIKModeSegFrmHdr.LongHelp="このコマンドでは、選択したセグメントに自動コンパス スワップを作成し、コンパスをセグメント フレームに
保持したままIKを活動化します｡";

//Resource Centric View Commands
DNBHumanSimWkbCommandHeader.CreateHumanTask.Title="タスクの作成";
DNBHumanSimWkbCommandHeader.CreateHumanTask.Help="タスクの作成 ";
DNBHumanSimWkbCommandHeader.CreateHumanTask.ShortHelp="選択したマネキン用のタスクを作成";
DNBHumanSimWkbCommandHeader.CreateHumanTask.LongHelp="選択したマネキン用のタスクを作成します";

DNBHumanSimWkbCommandHeader.ReassignHumanOperations.Title="ｵﾍﾟﾚｰｼｮﾝの再割り当て";
DNBHumanSimWkbCommandHeader.ReassignHumanOperations.Help="ｵﾍﾟﾚｰｼｮﾝの再割り当て";
DNBHumanSimWkbCommandHeader.ReassignHumanOperations.ShortHelp="ｵﾍﾟﾚｰｼｮﾝの再割り当て";
DNBHumanSimWkbCommandHeader.ReassignHumanOperations.LongHelp="ｵﾍﾟﾚｰｼｮﾝの再割り当て";

DNBHumanSimWkbCommandHeader.HTSSetActiveTask.Title="アクティブ タスクの設定";
DNBHumanSimWkbCommandHeader.HTSSetActiveTask.Help="アクティブ タスクの設定 ";
DNBHumanSimWkbCommandHeader.HTSSetActiveTask.ShortHelp="アクティビティに関連付けられたマネキン用のアクティブ タスクを設定";
DNBHumanSimWkbCommandHeader.HTSSetActiveTask.LongHelp="アクティビティに関連付けられたマネキン用のアクティブ タスクを設定します";

DNBHumanSimWkbCommandHeader.OpenActiveTask.Title="アクティブ タスク ウィンドウを開く";
DNBHumanSimWkbCommandHeader.OpenActiveTask.Help="アクティブ タスク ウィンドウを開く";
DNBHumanSimWkbCommandHeader.OpenActiveTask.ShortHelp="プロセス アクティビティに設定されたアクティブ タスクまたは TSA を別のウィンドウで開く";
DNBHumanSimWkbCommandHeader.OpenActiveTask.LongHelp="プロセス アクティビティに設定されたアクティブ タスクまたは TSA を別のウィンドウで開きます";

//Catalog Activity Commands
//-------------------------

DNBHumanSimWkbCommandHeader.SaveTaskInCatalog.Title="ﾀｽｸをｶﾀﾛｸﾞに保存";
DNBHumanSimWkbCommandHeader.SaveTaskInCatalog.Help="ﾀｽｸをｶﾀﾛｸﾞに保存 ";
DNBHumanSimWkbCommandHeader.SaveTaskInCatalog.ShortHelp="ﾀｽｸをｶﾀﾛｸﾞに保存 ";
DNBHumanSimWkbCommandHeader.SaveTaskInCatalog.LongHelp="将来のインスタンス化のために、タスクをカタログに保存します ";

DNBHumanSimWkbCommandHeader.InsertTaskInCatalog.Title="カタログからタスクの再使用";
DNBHumanSimWkbCommandHeader.InsertTaskInCatalog.Help="カタログからタスクを再使用 ";
DNBHumanSimWkbCommandHeader.InsertTaskInCatalog.ShortHelp="カタログからタスクを再使用します ";
DNBHumanSimWkbCommandHeader.InsertTaskInCatalog.LongHelp="リソース内で新しく使用可能になったタスクの下で、カタログからタスクを再使用します";

// Clash Detection Off command
// ---------------------------
DNBHumanSimWkbCommandHeader.DELMHTSDynClashOff.Title="干渉検出 (オフ)";
DNBHumanSimWkbCommandHeader.DELMHTSDynClashOff.ShortHelp="シミュレーション外での干渉検出\n(オフ)";
DNBHumanSimWkbCommandHeader.DELMHTSDynClashOff.Help="自動干渉検出の非活動化";
DNBHumanSimWkbCommandHeader.DELMHTSDynClashOff.LongHelp="シミュレーションの自動干渉検出を無効にします";

// Clash Detection On command
// --------------------------
DNBHumanSimWkbCommandHeader.DELMHTSDynClashOn.Title="干渉検出 (オン)";
DNBHumanSimWkbCommandHeader.DELMHTSDynClashOn.ShortHelp="シミュレーション外での干渉検出\n(オン)";
DNBHumanSimWkbCommandHeader.DELMHTSDynClashOn.Help="自動干渉検出を有効にします";
DNBHumanSimWkbCommandHeader.DELMHTSDynClashOn.LongHelp="シミュレーションの自動干渉検出を有効にします";

// Clash Detection Stop command
// ----------------------------
DNBHumanSimWkbCommandHeader.DELMHTSDynClashStop.Title="干渉検出 (停止)";
DNBHumanSimWkbCommandHeader.DELMHTSDynClashStop.ShortHelp="シミュレーション外での干渉検出\n(停止)";
DNBHumanSimWkbCommandHeader.DELMHTSDynClashStop.Help="干渉検出 (停止)";
DNBHumanSimWkbCommandHeader.DELMHTSDynClashStop.LongHelp="シミュレーションの干渉検出 (停止)";


// R12 Task Centric Manual Energy Expenditure
// ----------------------------
DNBHumanSimWkbCommandHeader.DELMEnergyExpendHdr.Title="単一ﾀｽｸ ｴﾈﾙｷﾞｰ消費計算";
DNBHumanSimWkbCommandHeader.DELMEnergyExpendHdr.ShortHelp="単一ﾀｽｸ ｴﾈﾙｷﾞｰ消費計算";
DNBHumanSimWkbCommandHeader.DELMEnergyExpendHdr.Help="単一ﾀｽｸ ｴﾈﾙｷﾞｰ消費計算";
DNBHumanSimWkbCommandHeader.DELMEnergyExpendHdr.LongHelp="単一ﾀｽｸ ｴﾈﾙｷﾞｰ消費計算";

// Swept Volume command
// --------------------
DNBHumanSimWkbCommandHeader.DELMHmuSweptVolumeHdr.Title="スイープしたボリューム...";
DNBHumanSimWkbCommandHeader.DELMHmuSweptVolumeHdr.Help="マネキンの動作からのスイープしたボリュームの作成";
DNBHumanSimWkbCommandHeader.DELMHmuSweptVolumeHdr.ShortHelp="マネキンの動作からスイープしたボリュームを作成";
DNBHumanSimWkbCommandHeader.DELMHmuSweptVolumeHdr.LongHelp="このコマンドでは、マネキンのセグメントの動作からスイープしたボリュームを作成します。";

// SEO - R19 (Open Active Task)
DNBHumanSimWkbCommandHeader.DNBOpenActiveTaskCmd.Title="アクティブ タスクを開く";
DNBHumanSimWkbCommandHeader.DNBOpenActiveTaskCmd.ShortHelp="アクティブ タスクを別のウィンドウで開く";
DNBHumanSimWkbCommandHeader.DNBOpenActiveTaskCmd.Help="アクティブ タスクを別のウィンドウで開く";
DNBHumanSimWkbCommandHeader.DNBOpenActiveTaskCmd.LongHelp="アクティブ タスクを別のウィンドウで開きます";

DNBHumanSimWkbCommandHeader.DNBResourceSeqTaskCmdHdr.Title="ロボット/デバイスのシーケンス タスク";
DNBHumanSimWkbCommandHeader.DNBResourceSeqTaskCmdHdr.Help="ロボット/デバイスのシーケンス タスク";
DNBHumanSimWkbCommandHeader.DNBResourceSeqTaskCmdHdr.ShortHelp="ロボット/デバイスのシーケンス タスク";
DNBHumanSimWkbCommandHeader.DNBResourceSeqTaskCmdHdr.LongHelp="ロボット/デバイスのシーケンス タスクです";

