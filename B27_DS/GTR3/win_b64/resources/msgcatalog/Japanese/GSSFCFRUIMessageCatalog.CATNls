
// the message box title to be used by all messages
MessageTitle="フィーチャーを認識";
MessageTitleUpdateFailed="更新の失敗";

UnspecifiedError="不明なエラーが発生しました｡\n同じエラーが続く場合は担当者に連絡してください｡\n";
NoIsoSolid="ドキュメント内に分離されたソリッドはありません｡\n[フィーチャーを認識]は分離されたソリッドでのみ使用できます｡\n分離されたソリッドは、ボディーを[コピー] -> [形式を選択して貼り付け] -> [結果として] から入手してください。 ";
InvalidIsoSolid="[フィーチャーを認識]で選択された分離されたソリッドには複数のランプがあります｡\n[フィーチャーを認識]は分離されたソリッドにランプが1つだけある場合に作動します｡";
SetSolidAsIWO="作業中のオブジェクトは分離されたソリッドではありません｡\n[フィーチャーを認識]を開始する前に分離されたソリッドに設定してください｡";
WeirdTopologyError="同じランプに属するオブジェクトを選択してください｡";
AttemptToRerecognize="[フィーチャーを認識]では再定義ができません｡\n分離されたソリッドに属するオブジェクトのみを選択してください｡";
VolumeDifference="フィーチャーの作成時にボリュームが大きく変更されています｡\n 最後のステップを元に戻すこともできます｡";
AttemptToSelFaceFromSelFaceListToUptoFace="選択されたオブジェクトはすでに定義されています｡";
AttemptToSelFaceFromUserPickedFaceListToUptoFace="選択されたオブジェクトはユーザによってピックされたフェースです。";
FullyParmeterized="作業中のオブジェクトは現在完全にパラメータ化されました｡\n また､現行ドキュメントに分離されたソリッドはありません｡";
FullyParmeterizedButMoreThanOneIsolatedSolid="入力された分離されたソリッドは現在完全にパラメータ化されています｡ \nさらにパラメータ化するには別の分離されたソリッドを作業中のオブジェクトとして設定してください｡";

FeatureCreationERR_0001.Request="フィーチャー作成に失敗!";
FeatureCreationERR_0001.Diagnostic="/p1 を作成できませんでした｡";

FeatureRecognitionFailure.Request="フィーチャー認識に失敗!";
FeatureRecognitionFailure.Diagnostic="/p1 /p2 /p3";

TryBooleanRecognition="\nブールとして認識できます｡";
TryGrooveOrBooleanRecognition="\n溝またはブールとして認識できます｡";
TryGrooveRecognition="\n溝として認識できます｡";

FeatureRecognitionCanceled="フィーチャーの認識はキャンセルされました｡\nフィーチャーは認識されませんでした｡";
FeatureCreationCanceled="フィーチャーの作成はキャンセルされました｡\n結果形状は初期形状と異なる可能性があり､ いくつかのコンテキスト\nフィーチャーは手動で転送する必要がある可能性があります｡";

UnknownFeatureRecognitionFailed="選択されたエンティティのセットからフィーチャーを認識できませんでした｡";
ChamferRecognitionFailed="選択されたエンティティのセットから面取りを認識できませんでした｡";
FilletRecognitionFailed="選択されたエンティティのセットからフィレットを認識できませんでした｡";
HoleRecognitionFailed="選択されたエンティティのセットから穴を認識できませんでした｡";
PadRecognitionFailed="選択されたエンティティのセットからパッドを認識できませんでした｡";
PocketRecognitionFailed="選択されたエンティティのセットからポケットを認識できませんでした｡";
GrooveRecognitionFailed="選択されたエンティティのセットから溝を認識できませんでした｡";
ShaftRecognitionFailed="選択されたエンティティのセットからシャフトを認識できませんでした｡";
DraftRecognitionFailed="選択されたエンティティのセットからドラフトを認識できませんでした｡";
BooleanRecognitionFailed="選択されたエンティティのセットからブールを認識できませんでした｡";
AFRFailed="フィーチャーを自動認識できませんでした｡\n";
TryManualRecognition="手動認識はできます｡";

DVErrorERR_0003="不明な例外が発生しました｡\n同じエラーが続く場合は担当者に連絡してください｡\n";

OneFeatureDeactivated="1つの認識されたフィーチャーを正常に作成できませんでしたので非活動化されています｡\n定義を編集することで修正することができます｡";
SomeFeaturesDeactivated="/p1 個の認識されたフィーチャーを正常に作成できませんでしたので非活動化されています｡\n定義を編集することで修正することができます｡";

UpdateERR_0001="更新の失敗!\n一部の指定されたフェースおよびエッジを使用できません｡\nパーツを更新して､不適切なフィーチャーを編集してください｡"; // ?@validatedUsage   XEN   2013/10/04 edit

RFFailed="\nいくつかのフェースが除去できなかったためです｡";
BOFailed="\nいくつかのブール演算が失敗したためです｡";
RFAndBOFailed="\nいくつかのフェースが除去できなかった､または､いくつかのブール演算が失敗したためです｡";
VCFailed="\nボリューム チェックが失敗したためです｡";

CreateRecognizedFeatureAfterCancel="これまでに認識されたフィーチャーを作成しますか?";
AllFacesChainedInLFR="制御不能連鎖\nボディーのフェースが全て連鎖しています : 連鎖フェースの除去\n適切な\"フェース/連鎖\"オプションを選択します。"; // ?@validatedUsage   XEN   2013/10/04 option

WorkOnlyOnActivePartDesignFeatures="フィーチャーの認識には作業中のオブジェクトをアクティブ パート デザイン フィーチャーにする必要があります。\n作業中のオブジェクトをアクティブ パート デザイン フィーチャーに設定してください。";
WorkOnlyOnValidPartDesignFeatures="フィーチャーの認識には作業中のオブジェクトを有効なパート デザイン フィーチャーにする必要があります。\n作業中のオブジェクトを有効なパート デザイン フィーチャーに設定してください。";












