//=============================================================================
//                                     CNEXT - CXR1
//                          COPYRIGHT DASSAULT SYSTEMES 1997 
//-----------------------------------------------------------------------------
// FILENAME    :    CATDrwDimFrameDressUpText
// LOCATION    :    DraftingIntUI/CNext/resources/msgcatalog
// AUTHOR      :    jmt
// DATE        :    Nov. 03 1997
//------------------------------------------------------------------------------
// DESCRIPTION :    Resources File for NLS purpose related to Drafting WorkShop
//                  
//------------------------------------------------------------------------------
// COMMENTS    :
//------------------------------------------------------------------------------
//MODIFICATIONS     user  date      purpose
//   HISTORY        ----  ----      -------
//     01			fgx	  23.03.99	Correction shell de nettoyage
//     02			fgx	  01.04.99	Ajout des options de scoring/framing de la cote
//------------------------------------------------------------------------------

FrameTitlePS.LabelTitlePS.Title="Prefix - Suffix ";
FramePS.LabelPSMain.Title="Main Value";
FramePS.LabelPSDual.Title="Dual Value";
FrameTitleBAUL.LabelTitleBAUL.Title="Associated Texts ";
FrameBAUL.LabelBAULMain.Title="Main Value";
FrameBAUL.LabelBAULDual.Title="Dual Value";

FrameTitlePS.LabelTitlePS.LongHelp="Prefix - Suffix 
Assigns a prefix and/or a suffix to the dimension.";

FrameTitleBAUL.LabelTitleBAUL.LongHelp="Associated Texts
Assigns an associated text to the dimension.";

FramePS.IconBoxPrefixMain.ShortHelp="Insert Symbol";
FramePS.IconBoxSuffixMain.ShortHelp="Insert Symbol";

FramePS.IconBoxPrefixMain.Help="Selects a symbol icon from the icon list";
FramePS.IconBoxSuffixMain.Help="Selects a symbol icon from the icon list";

FramePS.IconBoxPrefixMain.LongHelp="Insert symbol
Selects a symbol icon from the icon list.";
FramePS.IconBoxSuffixMain.LongHelp="Insert symbol
Selects a symbol icon from the icon list.";

FramePS.EditorPrefixMain.LongHelp="Assigns a prefix to the main value.";
FramePS.EditorSuffixMain.LongHelp="Assigns a suffix to the main value.";
FramePS.EditorPrefixDual.LongHelp="Assigns a prefix to the dual value.";
FramePS.EditorSuffixDual.LongHelp="Assigns a suffix to the dual value.";
FrameBAUL.EditorBeforeMain.LongHelp="Inserts a text before the main value.";
FrameBAUL.EditorAfterMain.LongHelp="Inserts a text after the main value.";
FrameBAUL.EditorUpperMain.LongHelp="Inserts a text above the main value.";
FrameBAUL.EditorLowerMain.LongHelp="Inserts a text below the main value.";
FrameBAUL.EditorBeforeDual.LongHelp="Inserts a text before the dual value.";
FrameBAUL.EditorAfterDual.LongHelp="Inserts a text after the dual value.";
FrameBAUL.EditorUpperDual.LongHelp="Inserts a text above the dual value.";
FrameBAUL.EditorLowerDual.LongHelp="Inserts a text below the dual value.";

FrameScoredTitle.LabelScoredTitle.Title="Dimension score options ";
FrameScoredElement.LabelScoredMain.Title="Main: ";
FrameScoredElement.LabelScoredDual.Title="Dual: ";
FrameFramedTitle.LabelFramedTitle.Title="Dimension frame options ";
FrameFramedElement.LabelFramedElement.Title="Element: ";
FrameFramedElement.LabelFramedGroup.Title="Group: ";

FrameScoredTitle.LabelScoredTitle.LongHelp="Dimension score options
Defines the dimension \"Main\" and \"Dual\" score options.";
FrameScoredElement.ComboScoredMain.LongHelp="Defines the main score option.";
FrameScoredElement.ComboScoredDual.LongHelp="Defines the dual score option.";
FrameFramedTitle.LabelFramedTitle.LongHelp="Dimension frame options 
Defines the \"element\" and \"group\" to be framed.";
FrameFramedElement.ComboFramedElement.LongHelp="Defines the element to be framed.";
FrameFramedElement.ComboFramedGroup.LongHelp="Defines the group to be framed.";

ComboScoredMainLine0="No score";
ComboScoredMainLine1="Value";
ComboScoredMainLine2="All";
ComboScoredDualLine0="No score";
ComboScoredDualLine1="Value";
ComboScoredDualLine2="All";
ComboFramedElementLine0="Value";
ComboFramedElementLine1="Value+tolerance";
ComboFramedElementLine2="Value+tolerance+texts";
ComboFramedGroupLine0="Main";
ComboFramedGroupLine1="Dual";
ComboFramedGroupLine2="Main and dual";
ComboFramedGroupLine3="Both groups";




