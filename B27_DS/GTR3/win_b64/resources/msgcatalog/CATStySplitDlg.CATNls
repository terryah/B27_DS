Title = "Trim/Split";

Frame0.FrameToCut.LabToCut.Title = "Element to Trim/Split";
Frame0.FrameToCut.LabToCut.LongHelp = "Displays the name of the mesh to trim or split.\nIt can be a multi-cells mesh.";

Frame0.FrameToCut.CuttingMultiList.Title = "Cutting Elements";
Frame0.FrameToCut.CuttingMultiList.LongHelp = "Lists the cutting elements.\nThey can be scans, curves, planes, surfaces or meshes.\nTheir projection curves on the Element to Trim/Split must intersect each other.";

Frame0.Direction.Title = "Projection";
Frame0.Direction.LongHelp = "Defines the projection.";
Frame0.Direction.ProjectionType.Title = "Type";
Frame0.Direction.ProjectionType.LongHelp = "Defines the direction along which all the Cutting elements are projected on the Element to Trim/Split:
- View: along the view direction,
- Compass: you define the direction with the compass,
- Normal: the direction is normal to the Element to Trim/Split, 
- Along a direction: you define the direction in the box below.";

Frame0.Direction.DirectionLabel.Title = "Direction";
Frame0.Direction.DirectionLabel.LongHelp = "Sets the direction of projection.";
Frame0.Direction.DirectionFrm.Title = "Direction";
Frame0.Direction.DirectionFrm.LongHelp = "Sets the direction of projection.";

Frame0.OperationFrm.Title = "Operation";
Frame0.OperationFrm.LongHelp = "Defines the operation.";
Frame0.OperationFrm.Preview.Title = "Preview";
Frame0.OperationFrm.Preview.LongHelp = "Displays the projection curves of the Cutting elements on the Element to Trim/Split.\n This option is time consuming.";
Frame0.OperationFrm.TrimFrame.Title = "Trim";
Frame0.OperationFrm.TrimFrame.Trim.Title = "Trim";
Frame0.OperationFrm.TrimFrame.Trim.ShortHelp = "Trim";
Frame0.OperationFrm.TrimFrame.Trim.LongHelp = "Trim";
Frame0.OperationFrm.TrimFrame.NoTrim.Title = "Keep";
Frame0.OperationFrm.TrimFrame.NoTrim.ShortHelp = "Keep";
Frame0.OperationFrm.TrimFrame.NoTrim.LongHelp = "Keep";
Frame0.OperationFrm.TrimFrame.TrimRemoveAll.Title = "Remove All";
Frame0.OperationFrm.TrimFrame.TrimRemoveAll.LongHelp = "Removes all the tools.";
Frame0.OperationFrm.Trim.Title = "Trim";
Frame0.OperationFrm.Trim.LongHelp = "Switches to trim mode.\nUse the scissors, crossed-scissors and Remove All to define which portions are kept or deleted.";
Frame0.OperationFrm.Split.Title = "Split";
Frame0.OperationFrm.Split.LongHelp = "Switches to split mode to create several meshes. ";

Frame0.ResultFrame.Title = "Result";
Frame0.ResultFrame.LongHelp = "Defines the result.";
Frame0.ResultFrame.KeepInitial.Title = "Keep Initial";
Frame0.ResultFrame.KeepInitial.LongHelp = "When selected, sends the Element to Trim/Split in the NoShow and creates new meshes.\nWhen cleared, deletes the Element to Trim/Split and creates new meshes.";
Frame0.ResultFrame.Distinct.Title = "Distinct";
Frame0.ResultFrame.Distinct.LongHelp = "Creates several distinct meshes.";
Frame0.ResultFrame.Grouped.Title = "Grouped";
Frame0.ResultFrame.Grouped.LongHelp = "Creates multi-cells meshes.";

Direction.View    = "View";
Direction.Compass = "Compass";
Direction.Normal  = "Normal";
Direction.Direction = "Along a direction";

CurveFittingError1 = " Curve fitting error. ";
CurveFittingError2 = " Curve fitting error. Too many points to fit, try different curve sampling settings.";

MeshInputError = "Unable to project curves onto the mesh due to nonmanifold topology.";
