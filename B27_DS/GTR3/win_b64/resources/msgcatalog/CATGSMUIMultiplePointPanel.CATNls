//----------------------------------------------
// Resource file for CATGSMUIMultiplePointPanel class
// En_US
//----------------------------------------------

Title="Points Repetition";
ToRepeat="Object";
RefPoint="Ref.Point";
RefCurve="Ref.Curve";
RefSurface="Ref.Surface";
RefAxisSystem ="Ref.Axis System";
RefDirection="Ref.Direction";
SecRefPoint="Second Ref.Point";
Inst="Instances";
InstSpacing="Instances & spacing";
Spacing="Spacing";
Dflt="Default";

AxisSystemRepetitionCloning = "Clones";
AxisSystemRepetitionAlongSurface = "Along Surface";
AxisSystemRepetitionDirectional = "Directional";

LabAxisSystemRepetitionCloning = "Axis System: ";
LabAxisSystemRepetitionAlongSurface = "Surface: ";
LabAxisSystemRepetitionDirectional = "Direction: ";

FraMultiple.FraPattern.Title="Object to repeat";
FraMultiple.FraPattern.LabelElem1.Title="Object: ";
FraMultiple.FraPattern.FraElem1.LongHelp="Specifies the object to repeat,\nor the curve on which points will be created.";
FraMultiple.FraPattern.FraRadio.Radio1.Title="Use same reference for repeated features";
FraMultiple.FraPattern.FraRadio.Radio2.Title="Reference previous repeated feature";
FraMultiple.FraPattern.FraRadio.LongHelp="Specifies whether the repeated object :\n - is linked to the same object as the first one\n - is linked to the previously repeated object.";

FraMultiple.FraInst.Title="Instances";
FraMultiple.FraInst.FraInst1.LabCmb.Title="Parameters: ";
FraMultiple.FraInst.FraInst1.CmbList.LongHelp="Specifies the parameters required\nto define the repetition.";
FraMultiple.FraInst.FraInst1.LabelEdt2.Title="Instance(s): ";
FraMultiple.FraInst.FraInst1.Edt2.LongHelp="Specifies the number of points to create.";
FraMultiple.FraInst.FraInst1.Edt2.Title="Instance(s)";
FraMultiple.FraInst.FraInst1.LabSpacing.Title="Spacing: ";
FraMultiple.FraInst.FraInst1.FraSpacing.EnglobingFrame.IntermediateFrame.Spinner.Title="Spacing";
FraMultiple.FraInst.FraInst1.LabelElem2.Title="Second point: ";
FraMultiple.FraInst.FraInst1.FraElem2.LongHelp="Specifies the second reference point.\nPoints will be created between both selected points.";
FraMultiple.FraInst.FraInst1.PushIcon.ShortHelp="Removes the reference point";
FraMultiple.FraInst.FraInst1.PushIcon.LongHelp="Allows to remove the second reference point,\nand to use the default one.";
FraMultiple.FraInst.FraInst1.LabelElemCurve.Title="Curve: ";
FraMultiple.FraInst.FraInst1.FraElemCurve.LongHelp="Specifies the support curve for point or the curve on which points will be created.";
FraMultiple.FraInst.FraInst1.LabelElem1.Title="First Point: ";
FraMultiple.FraInst.FraInst1.FraElem1.LongHelp="Specifies the point to repeat.";
FraMultiple.FraInst.FraRepeatMode.RadioAbs.Title="Absolute";
FraMultiple.FraInst.FraRepeatMode.RadioRel.Title="Relative";
FraMultiple.FraInst.FraSepar.LabRepetMode.Title="Mode for Repetition";
FraMultiple.FraInst.FraRepeatMode.LongHelp="Specifies Repetition mode: Absolute/Relative.";
FraMultiple.FraInst.FraInst2.PushB.Title="Reverse Direction";
FraMultiple.FraInst.FraInst2.PushB.LongHelp="Reverses the direction for point creation.";
FraMultiple.FraInst.FraInst2.CheckB.Title="With end points";
FraMultiple.FraInst.FraInst2.CheckB.LongHelp="In case of curve selection, specifies that\nthe end points of the curve are to be created.";

FraMultiple.FraCreate.CheckPln.Title="Create normal planes also";
FraMultiple.FraCreate.CheckPln.LongHelp="Creates planes normal to the curve at the created points.";
FraMultiple.FraCreate.CheckOBCompatible.Title="Create in a new Body";
FraMultiple.FraCreate.CheckOBCompatible.LongHelp="Creates all points or planes instances in a new Body.";
FraMultiple.FraCreate.CheckOB.Title="Create in a new editable Body";
FraMultiple.FraCreate.CheckOB.LongHelp="Creates all points or planes instances in a new editable Body.";

FraMultiple.FraCreate.CheckAx.Title="Create Axis System";
FraMultiple.FraCreate.FraAxisSystem.Title="Axis System Repetition";
FraMultiple.FraCreate.FraAxisSystem.LabAxisSystemType.Title="Type: ";
FraMultiple.FraCreate.CheckAx.LongHelp="Creates axis system as X Axis tangent to curve  and Y axis normal to the surface at the created points.";

AxisSystemRepetitionCloningLongHelp="Specifies the reference axis system to create axis system.";
AxisSystemRepetitionAlongSurfaceLongHelp ="Specifies the support surface to create axis system.";
