TitleBoxError                    = "Operator Error";
TitleBoxInfo                     = "Operator Information";
Warning_FillIncompatibility      = "Failed to fill: directions of extrusion are uncompatible.";
Warning_FillSameSupport          = "Failed to fill: two continuous edges cannot be connected with a same face.";
Warning_FillBoundaryNotClose     = "Failed to fill: edges must be connected to perform the fill operation.";
Warning_FillConfusedEdges        = "Failed to fill: two confused edges (totally or partially) disables the fill operation.";
Warning_FillEdgeLengthMin        = "Failed to fill: a null edge length disables the fill operation.";
Warning_FillNotTriangularFace    = "Failed to fill: a triangular face disables the fill operation.";
Warning_FillInvalidNbEdges       = "Failed to fill: two to four continuous edges are necessary for the fill operation.";
Warning_FillTooManyEdges         = "Failed to fill: too many edges disables the fill operation.";
Warning_FillImpairNbEdges        = "Failed to fill: impair number of edges disables the fill operation.";
Warning_FillOperatorError        = "Failed to fill: edges configuration disables the fill operation.";
EraseEdgeIncompatibility   = "Failed to erase: only closed propagations or propagations up to a free edge are available";
OperatorError              = "An unexpected error occurs while operator processing.";
CheckValidityError         = "Check the selection validity.";
ProcessError               = "You are not allowed to process such operation.";
TopoLoopError              = "Impossible to cut a non-closed topology.";
BadTopologyError           = "It would create a bad topology.";
AllFacesErasingError       = "Erasing all faces is not allowed.";
OnlyFreeEdgesError         = "Only free edge selection is authorized.";
NotTriangularFaceError     = "Generating a triangular face is not allowed.";
NotConnectedEdgesError     = "Edges must be connected.";
NotConnectedFacesError     = "Faces must be connected.";
DesEdgeLengthMinError      = "Null edge length is not allowed.";
DesTopoDifferentEdges      = "Edges must be have the same topology as the fill extrusion.";
DesCurveAllErasingError    = "Erasing the whole curve is not allowed.";
DesDialogSelectionNeeded   = "Select faces, edges or vertices before.";
DesNotYetAvalaible         = "Not Yet Avalaible." ;
DesDialogSegmentSelectionNeeded   = "Select a segment before.";
DesEmptyWorkingZoneForbidden = "Definition of an empty working zone is forbidden." ;
DesOtherPartSelectionForbidden = "Selection of an element in another part is forbidden." ;
DesSymmetrySelectionForbidden = "Selection of the current feature during its edition is forbidden." ;
DesMissingLicence = "The licence for this feature is not detected. You cannot edit it." ;
DesForbiddenCCP = "New subdivision has to be created in the container of the symmetry." ;
DesChildrenAlreadySelected = "Forbidden selection. \n\t Two features are associated. \n\t Cannot add \"/P01\" to selected features \n\t because one of its children \"/P02\" is already selected." ;
DesParentAlreadySelected = "Forbidden selection. \n\t Two features are associated. \n\t Cannot add \"/P01\" to selected features \n\t because one of its parent \"/P02\" is already selected." ;
DesImportObjGeneralError   = "Error during import" ;

DesMatchInvalidSide               = "Not respected conditions on matching side.";
DesMatchNotQuad                   = "Only quadrangular faces on matching side.";
DesMatchIncompatibilityContinuity = "Incompatibility continuity between matching side and target."; 
DesMatchClosedContinuity          = "Only sharp continuity for closed match."; 
DesHybridAreaIntersection         = "Area modification intersection."; 
DesMatchIncompatibilityClosure    = "Incompatibility closure between matching side and target."; 
DesMatchUndefinedSupport          = "Target support must be defined."; 

DesFadingEdgeConnexAndSharp= "\nThe edges support of fading edge must be sharp and connex,
with valence 4 for internal vertex(regular),
with no other sharp edges at end vertex,
with closed vertex (no free vertex allowed exept for first vertex).
(a free vertex is a vertex involved in free edge)" ;

CATStoCL_Invalid_NotExistingVertex = "Some vertex are missing in fading edge support." ;
CATStoCL_Invalid_NotExistingEdge  = "Some edges are missing in fading edge support." ;
CATStoCL_Invalid_BadVertexPath = "The vertex support is not connex or not correctly chained." ;
CATStoCL_Invalid_SharpnessOnIncidentEdge = "Character Line support contain neighbour edges that are not smooth." ;  
CATStoCL_Invalid_TooManyCharacterLinesOnEdge  = "Too many Character Lines on an edge.";
CATStoCL_Invalid_TwoEdgesBelongToTheSameFace = "Two edges belonging to the same face is not allowed." ;
CATStoCL_Invalid_InternalFreeVertex = "Free vertex are not allowed for Character Line support." ;  
CATStoCL_Invalid_FreeEdge = "No free edge allowed." ;         
CATStoCL_Invalid_NotRegularZone = "Character Line support contain not regular zone." ;
CATStoCL_Invalid_NoDisconnectedSupports = "The shortest number of faces between two CL support must be at least two." ;       

CATStoCL_Invalid_LocalParameters = "Bad local parameter value." ;
CATStoCL_Invalid_LocalParametersOutOfRange = "Local parameter out of range." ;
CATStoCL_Invalid_GlobalParameters = "Bad global parameter value." ;
CATStoCL_Invalid_GlobalParametersOutOfRange = "Global parameter out of range." ;
CATStoCL_Invalid_FadingEdgesIntersection = "No fading edge Intersection" ;     
CATStoCL_Invalid_LocalGlobalRatioConsistency = "Bad global parameter value." ;

DesCharacterLineGeneral ="\nThe edges support of Character Lines (CL) must be connex,
with valence 4 for internal vertices (regular).
The shortest number of faces between two CL supports must be at least two
(No CL crossing another CL).
The start of the two Fading Edges (FE) cannot lie on the same edge.
Two edges of a CL cannot lie on the same face." ;

DesCharacterLineMinimum = "A minimum of 2 edges is necessary for a Character Line." ;

DesFadingEdgeGeneral ="\nThe edges support of fading edge must be connex,
with valence 4 for internal vertex(regular),
with closed vertex (no free vertex allowed).
(a free vertex is a vertex involved in free edge)
(2 edges of the FE can't lie on the same face)" ;

CATStoFE_Valid = "Valid fading edge." ;
CATStoFE_Invalid = "Invalid fading edge." ;
CATStoFE_Invalid_NotExistingVertex = "Some vertex are missing in fading edge support." ;
CATStoFE_Invalid_NotExistingEdge = "Some edges are missing in fading edge support." ;
CATStoFE_Invalid_BadVertexPath  = "The vertex support is not connex or not correctly chained." ;

CATStoFE_Invalid_SharpnessOnIncidentEdge = "Fading edge support contain neighbour edges that are not smooth." ;
CATStoFE_Invalid_TooManyFadingEdgesOnEdge  = "Too many Fading Edges on an edge";
CATStoFE_Invalid_TwoEdgesBelongToTheSameFace = "Two edges of the fading edge can't lie on the same face";
CATStoFE_Invalid_FreeVertex  = "Free vertex are not allowed for fading edge support." ;
CATStoFE_Invalid_FreeEdge = "Some fading edge support are free." ;
CATStoFE_Invalid_NotRegularZone = "Fading edge support contain not regular zone." ;
CATStoFE_Invalid_NoDisconnectedSupports = "The shortest number of faces between two CL support must be at least two." ;
CATStoFE_Invalid_NotAllSharpEdges = "All edges are not sharp (100 for edge weight value)." ;
CATStoFE_Invalid_Unprocessed = "The Fading Edge can't be processed." ;

CATStoFE_Invalid_BadParameters = "Bad start and/or end parameters value.(Start and End must be different)" ;
CATStoFE_Invalid_ParametersOutOfRange = "Parameters out of range." ;

DesMeshIsNotQuadError = "That command is not available if the mesh has triangular face(s)." ;
DesTriangularFaceNotAllowedInThisArea = "This area contain triangular face(s) that are not allowed." ;
DesCellAlreadyModified = "A cell has already been modified : Add Cut between the mapping zones in conflict." ;
DesBorderEdgeAlreadyUsedPleaseReselect = "some of the border edge cannot be used in another Net feature, please reselect it." ;
DesIncompatibilityContinuityCoupling = "Incompatibility continuity between edge to match and target.";
DesIncompatibilityContinuityClosed = "Only point continuity for closed target.";
DesBigValenceError = "Operation Failed : a vertex canno't have so much incident edges" ;

DesNormalInvertBodyError = "Do you want to keep the normal orientation of the surface,
in order to avoid breaking some children of features \"/P01\" ( like Offset for exemple ).
If you answer \"Yes\" only Mesh Normal will be reversed." ;

DesGlobalSymmetrizeLostStart = "Because of global symmetrize." ;
DesGlobalSymmetrizeLostSymZoneCst = "All symmetric Zone constraints already created will be Removed from this Subdivision Surface." ;

ProjectionNoSupport      = "Failed to align : support of projection missing.";
ProjectionNoResultOrthog = "Failed to align : no orthogonal projection found.";
ProjectionNoResultDirect = "Failed to align : no projection found along selected direction.";
Warning_ProjectionNotAll = "No projection found for all selected elements.";

DesMigrationIsNecessaryTitle = "Old Feature Detected";
DesMigrationIsNecessary = "This feature is deprecated. \n A migration of the document is needed. \n Do you want to proceed ?";

CATDesDimInsufficientNumbersOfCells = "Insufficient Numbers of active cells. \n Switch off the half axis mode.";


		
UpdateErrorTitle = "Update Error";		
UpdateBuildErrorMsg = "The feature is out of date and it's body was not generated (may be due to Update Error). Please ensure that there are no error or out of date feature in the spec tree and try again";



