//=============================================================================
//                                     CNEXT - CXR1
//                          COPYRIGHT DASSAULT SYSTEMES 1997 
//-----------------------------------------------------------------------------
// FILENAME    :    CATDrwDimFrameForshortened
// LOCATION    :    DraftingIntUI/CNext/resources/msgcatalog/French
// AUTHOR      :    jmt
// DATE        :    Nov. 03 1997
//------------------------------------------------------------------------------
// DESCRIPTION :    Resources File for NLS purpose related to Drafting WorkShop
//                  
//------------------------------------------------------------------------------
// COMMENTS    :
//------------------------------------------------------------------------------
//MODIFICATIONS     user  date      purpose
//   HISTORY        ----  ----      -------
//			    lgk 25.06.2002 Revision
//------------------------------------------------------------------------------

FrameForshortenedData.FramePreview.Title="Description ";
FrameForshortenedData.FramePreview.LongHelp=
"Displays the description of the foreshortened dimension to be created.";

FrameForshortenedData.FrameForshortened.LabelValueLocationMode.Title="Text position: " ;
FrameForshortenedData.FrameForshortened.LabelAngle.Title="Angle: " ;
FrameForshortenedData.FrameForshortened.LabelOrientationMode.Title="Orientation: ";
FrameForshortenedData.FrameForshortened.LabelSymbolScale.Title="Point scale: ";
FrameForshortenedData.FrameForshortened.LabelSegmentRatio.Title="Ratio: ";
FrameForshortenedData.CheckButtonFixExtremityPoint.Title="Position extremity point manually";

FrameForshortenedData.FrameForshortened.ComboValueLocationMode.LongHelp=
"Defines the position of the text associated to the foreshortened
dimension: either on the \"long segment\" or on the \"short segment\". " ;
FrameForshortenedData.FrameForshortened.SpinnerAngle.LongHelp=
"Defines the angle associated to the foreshortened dimension." ;
FrameForshortenedData.FrameForshortened.ComboOrientationMode.LongHelp=
"Defines the orientation of the text associated to the dimension line. ";
FrameForshortenedData.FrameForshortened.SpinnerSymbolScale.LongHelp=
"Defines the scale of the symbol that represents 
the point used as the circle center.";
FrameForshortenedData.FrameForshortened.ComboSegmentRatio.LongHelp=
"Defines ratio d/D.";
FrameForshortenedData.FrameForshortened.CheckButtonFixExtremityPoint.LongHelp=
"Lets you position the extremity point of the 
foreshortened dimension line manually. When moving 
the dimension line, the extremity point will then 
be fixed in the position you specified.";

FrameForshortenedTitle.CheckButtonForshortened.Title="Foreshortened";
ComboValueLocationModeLine1="Short Segment";
ComboValueLocationModeLine2="Long Segment";
ComboOrientationModeLine1="Parallel";
ComboOrientationModeLine2="Convergent";

SmallTextString="d";
LargeCenteredTextString="D";
LargeParallelTextString="D";
AngleTextString="A";
RatioTextString="Ratio = d/D";
