// =====================================================================
//
// Erreurs Framework Manufacturing dedie a la generation de l'IPM
// =================================
//
// Deux manieres de formater vos messages : 
//
// a- Specifier en delimitant avec le caractere '\n' :
//       Request      (ce que l'on tentait de faire)
//       Diagnostic   (ce qui est arrive)
//       Advice       (...ce qu'il pourrait faire si possible)
//
//     Exemple :
//       ERR_3000 = "Initializing '/p1' attribute in '/p2'.\nAttribute does not exist.\nContact your local support.";  
//       
//               NOTE : Le parametrage des messages se fait via SetNLSParameter
//
// ou bien
//
// b- Specifier vos messages avec la cle + les 3 facettes Request/Diagnostic/Advice
//
//     Exemple :
//       ERR_3000.Request    "Initializing '/p1' attribute in '/p2'." 
//       ERR_3000.Diagnostic "Attribute does not exist."
//       ERR_3000.Advice     "\nContact your local support."
// 
//               NOTE : Le parametrage de chaque facette se fait par :
//                         Request    : SetNLSRequestParams
//                         Diagnostic : SetNLSDiagnosticParams
//                         Advice     : SetNLSAdviceParams
//
// =====================================================================


//************************************************************
// Plage reservee aux messages generiques ERR_0000 a ERR_0999
//************************************************************
ERR_0100.Request    = "Error when generating or displaying IPM Volume."; 
ERR_0100.Diagnostic = "\nIPM Volume cannot be generated or displayed because the user representation added\nto the tool or the tool assembly is not correct.";
ERR_0100.Advice     = "\nPlease check that the user representation of the tool or of the tool assembly is correctly defined.";

ERR_0150.Request    = "Warning when generating or displaying IPM Volume."; 
ERR_0150.Diagnostic = "\nIPM Volume is not generated or displayed because the user representation added\nto the tool or the tool assembly is a cleaning tool.";
ERR_0150.Advice     = "\nPlease change or remove the user representation of the tool or of the tool assembly to get IPM Volume";

ERR_0200.Request    = "Error when generating or displaying IPM Volume."; 
ERR_0200.Diagnostic = "\nIPM Volume generation and display are not yet supported for this machining operation.";
ERR_0200.Advice     = "\nThis machining operation will be ignored during IPM generation.";

ERR_0250.Diagnostic = "The current IPM generation mode is not supported for this machining operation.";
ERR_0250.Advice     = "\nPlease change the In-Process Model Generation Mode in Tools->Options.";

ERR_0300.Request    = "Error during computation of the input stock";
ERR_0300.Diagnostic = "\nInput stock update related to the operation '/p1' failed ";
ERR_0300.Advice     = "\nPlease modify the operation '/p1' and update the input stock ";

ERR_0350.Request    = "Error when generating or displaying IPM Volume."; 
ERR_0350.Diagnostic = "\nIPM Volume update related to the operation '/p1' failed.";
ERR_0350.Advice     = "\nPlease modify the operation '/p1' and update the IPM ";

ERR_0400.Request    = "Error when generating or updating the IPM."; 
ERR_0400.Diagnostic = "\nIPM creation from user defined template failed. IPM Generation will abort.";
ERR_0400.Advice     = "\nPlease check that the IPM user defined template path is valid in Tools->Options.";

ERR_0450.Request    = "Error when generating IPM Volume."; 
ERR_0450.Diagnostic = "\nIPM generation requires stock bodies to be published";
ERR_0450.Advice     = "\nPlease publish all stock bodies and update the IPM ";


ERR_0500.Request    = "Error when generating or displaying IPM Volume."; 
ERR_0500.Diagnostic = "\nIPM Volume cannot be generated, Selected IPM Generation mode is not supported";
ERR_0500.Advice     = "\nPlease change Localized IPM Generation Mode";

ERR_0600.Request    = "Error when generating or displaying IPM Volume."; 
ERR_0600.Diagnostic = "\nIPM Volume cannot be generated, Tool is not selected";
ERR_0600.Advice     = "\nPlease Select select the Tool to Generate IPM ";
 
ERR_0700.Request    = "Error when generating or displaying IPM Volume."; 
ERR_0700.Diagnostic = "MO is not Active For IPM Generation.";
ERR_0700.Advice     = "\nPlease Activate MO to participate in IPM Generation.";

//************************************************************
// Plage reservee aux erreurs Curve Following ERR_1000 a ERR_1499
//************************************************************
ERR_1000.Request    = "Error when generating or displaying IPM Volume."; 
ERR_1000.Diagnostic = "\nIPM Volume cannot be generated or displayed because the Tool Axis is not orthogonal to the curves.";
ERR_1000.Advice     = "\nPlease check that geometric definition of Curve Following operation is correct \nand that its Tool Axis is orthogonal to the curves.";

//************************************************************
// Plage reservee aux erreurs Point to Point ERR_1500 a ERR_1999
//************************************************************
ERR_1500.Request    = "Error when generating or displaying IPM Volume."; 
ERR_1500.Diagnostic = "\nIPM Volume cannot be generated or displayed because all motions are not in the same plane \nor/and that plane is not orthogonal to the Tool Axis.";
ERR_1500.Advice     = "\nPlease check that geometric definition of all motions of the Point to Point operation are in a same plane \nand that the Tool Axis is orthogonal to that plane.";

//************************************************************
// Area for Profile Contouring errors from ERR_2000 to ERR_2499
//************************************************************
ERR_2000.Request    = "Error when generating or displaying IPM Volume."; 
ERR_2000.Diagnostic = "\nIPM Volume cannot be generated or displayed because the selected Profile Contouring mode is not supported.";
ERR_2000.Advice     = "\nPlease select the Between Two Planes Profile Contouring mode in the geometry tabpage of the Profile Contouring editor.";

ERR_2010.Request    = "Error when generating or displaying IPM Volume."; 
ERR_2010.Diagnostic = "\nIPM Volume cannot be generated or displayed because the tool assigned to the activity is not supported.";
ERR_2010.Advice     = "\nPlease assign a Face Mill tool or End Mill tool or a TSlotter tool to the Profile Contouring operation.";

ERR_2020.Request    = "Error when generating or displaying IPM Volume."; 
ERR_2020.Diagnostic = "\nIPM Volume cannot be generated or displayed because user representation assigned to the tool is not supported for this operation.";
ERR_2020.Advice     = "\nPlease remove the user representation assigned to the tool.";

//************************************************************
// Area for Assembly Station errors from ERR_2500 to ERR_2999
//************************************************************
ERR_2500.Request    = "Error when generating IPM Volune for Assembly Station."; 
ERR_2500.Diagnostic = "\nIPM Volume cannot be generated or displayed because Assembly Station has one or more Child Activities.";
ERR_2500.Advice     = "\nPlease check that Assembly station does not have any child activity.";

//************************************************************
// Area for Pocketing errors from ERR_3000 to ERR_3499
//************************************************************
ERR_3000.Request    = "Error when generating or displaying IPM Volume.";  
ERR_3000.Diagnostic = "\nIPM Volume cannot be generated or displayed because automatic draft angle is not supported.";
ERR_3000.Advice     = "\nPlease set automatic draft angle to zero.";

//************************************************************
// Area for Prismatic Roughing errors from ERR_3500 to ERR_3999
//************************************************************
ERR_3500.Request    = "Error when generating or displaying IPM Volume."; 
ERR_3500.Diagnostic = "\nIPM Volume generation or display related to the Prismatic Roughing operation failed.";
ERR_3500.Advice     = "\nPlease modify the Prismatic Roughing operation and generate or display the IPM ";


//************************************************************
// Area for generic errors for pocketing/facing operations from ERR_4000 to ERR_4500
//************************************************************
ERR_4000.Request    = "Error when generating or displaying IPM Volume.";  
ERR_4000.Diagnostic = "\nIPM Volume cannot be generated or displayed because the tool axis is not normal to the bottom plane.";
ERR_4000.Advice     = "\nPlease modify the tool axis or select another bottom plane.";

ERR_4010.Request    = "Error when generating or displaying IPM Volume.";  
ERR_4010.Diagnostic = "\nIPM Volume cannot be generated or displayed because Offset on Top value and Offset on Bottom are not compatible.";
ERR_4010.Advice     = "\nPlease modify Offset on Top value or Offset on Bottom value";

ERR_4020.Request    = "Error when generating or displaying IPM Volume.";  
ERR_4020.Diagnostic = "\nIPM Volume cannot be generated or displayed because incorrect Hole or Thread or Tool diameter.";
ERR_4020.Advice     = "\nPlease modifiy Thread or Hole or Tool diameter";






ERR_5000.Request    = "Mismatch in Hybrid Option"; 
ERR_5000.Diagnostic = "\nIPM Volume cannot be generated or displayed because their is a Mismatch in the Mode in which the Design was created and the current mode in Tools->Option setting";
ERR_5000.Advice     = "\nPlease keep both the modes in sync";
