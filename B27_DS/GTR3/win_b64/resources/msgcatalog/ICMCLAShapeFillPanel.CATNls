DialogBoxTitle                                   = "Fill";

LabelFillType.Title                              = "Fill Type: ";
LabelFillType.LongHelp                           = "- Auto -
  The filling surface is a single patch.
- Reference -
  Selects a single patch to be projected onto the selected region.
  The reference defines order and segmentation of the result.
- Multiple Surfaces -
  Several surfaces are created with a star point allowing to
  modify the result.";
LabelFillType.ShortHelp                          = "Defines the filling surface type.";

LabelSpace.Title                                 = " ";

ComboFillType.LongHelp                           = "- Auto -
  The filling surface is a single patch.
- Reference -
  Selects a single patch to be projected onto the selected region.
  The reference defines order and segmentation of the result.
- Multiple Surfaces -
  Several surfaces are created with a star point allowing to
  modify the result.";
ComboFillType.ShortHelp                          = "Defines the filling surface type.";

//combobox options!
Combo.Auto                                       = "Auto";
Combo.Reference                                  = "Reference";
Combo.MultipleSurfaces                           = "Multiple Surfaces";

LabelCurve.Title                                 = "Curve: ";
LabelCurve.LongHelp                              = "Selects curves or surface edges.";
LabelCurve.ShortHelp                             = "Selects curves or surface edges.";

//Options tab**************************
TabPageOptions.Title                             = "Options";

//Frame Single *************************
FrameSingle.Title                                = "Single";
FrameSingle.LongHelp                             = "Single patch solution.
Only available for Fill Type Auto and Reference.";

LabelReference.Title                             = "Reference: ";
LabelReference.LongHelp                          = "Selects an additional patch which is projected onto the
selected curves or edges.
The reference defines order and segmentation of the result.";
LabelReference.ShortHelp                         = "Select a patch as reference";

CheckButtonTrim.LongHelp                         = "Only available for Fill Type Reference.
The created fill surface can be trimmed at the boundaries if
the untrimmed result overlaps the input curves.";
CheckButtonTrim.ShortHelp                        = "Trim result";

LabelProjBase.Title                              = "ProjBase: ";
LabelProjBase.LongHelp                           = "Selects the surfaces, curves, or edges adjacent to the filling
surface as projection base. On the adjacent geometry are computed
points to which the filling surface is approximated.";
LabelProjBase.ShortHelp                          = "Select adjacent elements as projection base.";

//Frame Multiple *************************
FrameMultiple.Title                              = "Multiple";
FrameMultiple.LongHelp                           = "Multiple patch solution.
Only available for Fill Type Multiple Surfaces.";

//System *********************************
LabelSystem.Title                                = "System: ";
LabelSystem.LongHelp                             = "Adjusts the coordinate system for the movement direction of the star point: 
- View -
  Moving plane parallel to screen.
- Model -
  Moving plane parallel to XY model plane.
- Compass Plane -
  Moving plane parallel to XY compass plane.
- Compass Normal -
  Moving in compass Z direction.
- Normal to Surface -
  Moving in filling surface normal direction.";
LabelSystem.ShortHelp                            = "Degrees of freedom for moving the star point.";

ComboSystem.ShortHelp                            = "Degrees of freedom for moving the star point.";
ComboSystem.LongHelp                             = "Adjusts the coordinate system for the movement direction of the star point: 
- View -
  Moving plane parallel to screen.
- Model -
  Moving plane parallel to XY model plane.
- Compass Plane -
  Moving plane parallel to XY compass plane.
- Compass Normal -
  Moving in compass Z direction.
- Normal to Surface -
  Moving in filling surface normal direction.";

//combobox options!
Combo.View                                       = "View";
Combo.Model                                      = "Model";
Combo.CompassPlane                               = "Compass Plane";
Combo.CompassNormal                              = "Compass Normal";
Combo.NormalToSurface                            = "Normal to Surface";

//Label Order
LabelOrder.Title                                 = "Order: ";
LabelOrder.LongHelp                              = "Defines the minimum order of the resulting filling surfaces.";
LabelOrder.ShortHelp                             = "Minimum order";
SpinnerOrder.LongHelp                            = "Defines the minimum order of the resulting filling surfaces.";
SpinnerOrder.ShortHelp                           = "Minimum order";

//Frame Edge Influence
FrameEdgeInfluence.Title                         = "Edge Influence";
FrameEdgeInfluence.LongHelp                      = "Controls the influence of the cross edge tangents on the patches
to be created. In case of a surface created in a round gap an
increased edge influence will keep the edge tangency to a larger extent.";
ScrollBarEdgeInfluence.LongHelp                  = "Controls the influence of the cross edge tangents on the patches
to be created. In case of a surface created in a round gap an
increased edge influence will keep the edge tangency to a larger extent.";
ScrollBarEdgeInfluence.ShortHelp                 = "Influence of the cross edge tangents";
SpinnerEdgeInfluence.LongHelp                    = "Controls the influence of the cross edge tangents on the patches
to be created. In case of a surface created in a round gap an
increased edge influence will keep the edge tangency to a larger extent.";
SpinnerEdgeInfluence.ShortHelp                   = "Influence of the cross edge tangents";

//Frame Flatness
FrameFlatness.Title                              = "Flatness";
FrameFlatness.LongHelp                           = "Controls the influence of the tangent plane of the star point.
High values create a large and very flat region around the
star point, whereas small values create a peak.";
ScrollBarFlatness.LongHelp                       = "Controls the influence of the tangent plane of the star point.
High values create a large and very flat region around the
star point, whereas small values create a peak.";
ScrollBarFlatness.ShortHelp                      = "Influence of the tangent plane";
SpinnerFlatness.LongHelp                         = "Controls the influence of the tangent plane of the star point.
High values create a large and very flat region around the
star point, whereas small values create a peak.";
SpinnerFlatness.ShortHelp                        = "Influence of the tangent plane";

DomainMode.Title                                 = "Domain Mode has been changed to Force-Single-Domain.
Domain Mode Use-First-Domain is no longer supported.";
