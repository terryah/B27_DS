// COPYRIGHT ImpactXoft 2005
//===================================================================
//
// This file contains the ISO screw catalog keywords for accessing
// screw data for supported screw types.  The format of text string
// must be a "comma" delimited string in the order defined below.
//
// Some table keys indicate that data values are computed/derived/constant as follows:
//
// Key "COMPUTE:FORMULA_1" = if( Shank_Diameter <= 52mm )
//                               Length - 2.5 * P_pitch
//                           else
//                               Length - 3.5 * P_pitch
//             
// Key "COMPUTE:FORMULA_2 = Length - Shank_Length 
//
// Key "COMPUTE:FORMULA_3 = Length - Thread_Length 
//
// Key "COMPUTE:FORMULA_4 = Shank_Length + Thread_Length 
//
// Key "CONSTANT:0" = 0
//
//===================================================================
//
//                                                 Countersunk       Length            Head_Diameter     Thread_Diameter  Thread_Length        Shank_Diameter  Shank_Length       Thread_Pitch
//                                                    Status           Key                  Key               Key              Key                   Key           Key                 Key
//                                                 -----------  ------------------   ------------------  ---------------  -------------------  --------------  -----------------  ----------

ISO_10642_HEXAGON_SOCKET_COUNTERSUNK_SCREW              = "YES, l_length_max,        dk_head_dia_max,    d_dia_,          COMPUTE:FORMULA_2,   d_dia_,         lg,                P_pitch";
ISO_10642_HEXAGON_SOCKET_COUNTERSUNK_SCREW_NONPREFERRED = "YES, l_length_max,        dk_head_dia_max,    d_dia_,          COMPUTE:FORMULA_2,   d_dia_,         lg,                p_pitch"; 
ISO_1207_GRADE_A_SLOTTED_CHEESE_HEAD_SCREW              = "NO , l_Length_nom,        dk_nom_head_dia,    d_dia,           threading,           d_dia,          COMPUTE:FORMULA_3, P_pitch";
ISO_2009_COUNTERSINK_SLOTTED_FLAT_HEAD_SCREW            = "YES, l_length_nom,        dk_head_dia_nom,    d_dia,           threading,           d_dia,          COMPUTE:FORMULA_3, p_pitch";
ISO_2009_COUNTERSUNK_SLOTTED_FLAT_HEAD_SCREW            = "YES, l_length_nom,        dk_head_dia_nom,    d_dia,           threading,           d_dia,          COMPUTE:FORMULA_3, p_pitch";
ISO_2010_GRADE_A_COUNTERSUNK_SLOTTD_RAISED_HEAD_SCREW   = "YES, l_length_nom,        dk_head_dia_nom,    d_dia,           threading,           d_dia,          COMPUTE:FORMULA_3, p_pitch";
ISO_2010_GRADE_A_COUNTERSUNK_SLOTTED_RAISED_HEAD_SCREW  = "YES, l_length_nom,        dk_head_dia_nom,    d_dia,           threading,           d_dia,          COMPUTE:FORMULA_3, p_pitch";
ISO_2342_SLOTTED_HEADLESS_SCREWS_FULL_THREAD            = "NO , l_length,            d_dia,              d_dia,           l_length,            d_dia,          CONSTANT:0,        Pitch";
ISO_2342_SLOTTED_HEADLESS_SCREWS_PART_THREAD            = "NO , l_length,            d_dia,              d_dia,           b_thread,            d_dia,          COMPUTE:FORMULA_3, Pitch";
ISO_2342_SLOTTED_HEADLESS_SCREWS_WITH_SHANK             = "NO , l_length,            d_dia,              d_dia,           b_thread,            d_dia,          COMPUTE:FORMULA_3, Pitch";
ISO_2342_SLOTTED_HEADLESS_SCREWS_WITH_SHANK_NONPREFERRED= "NO , l_length,            d_dia,              d_dia,           threading,           d_dia,          COMPUTE:FORMULA_3, p_pitch";
ISO_4017_GRADES_A_B_HEXAGON_HEAD_SCREW                  = "NO , l_length_max,        e_min,              d_dia,           threading,           d_dia,          COMPUTE:FORMULA_3, P_pitch";
ISO_4017_GRADES_A_B_HEXAGON_HEAD_SCREW_NONPREFERRED     = "NO , l_length_max,        e_min,              d_dia,           threading,           d_dia,          COMPUTE:FORMULA_3, P_pitch";
ISO_4018_GRADE_C_HEXAGON_HEAD_SCREW                     = "NO , l_length_max,        e_min,              d_dia,           threading,           d_dia,          COMPUTE:FORMULA_3, P_pitch";
ISO_4018_GRADE_C_HEXAGON_HEAD_SCREW_NONPREFERRED        = "NO , l_length,            e_min,              d_dia,           threading,           d_dia,          COMPUTE:FORMULA_3, P_pitch";
ISO_4026_GRADE_A_HEXAGON_SOCKET_SET_SCREW_FLAT_POINT    = "NO , l_nom_length,        d_dia,              d_dia,           threading,           d_dia,          CONSTANT:0,        p_pitch";
ISO_4027_GRADE_A_HEXAGON_SOCKET_SET_SCREW_CONE_POINT    = "NO , l_nom_length,        d_dia,              d_dia,           threading,           d_dia,          CONSTANT:0,        p_pitch";
ISO_4028_GRADE_A_HEXAGON_SOCKET_SET_SCREW_DOG_POINT     = "NO , l_nom_length,        d_dia,              d_dia,           threading,           d_dia,          CONSTANT:0,        p_pitch";
ISO_4029_GRADE_A_HEXAGON_SOCKET_SET_SCREW_CUP_POINT     = "NO , l_nom_length,        d_dia,              d_dia,           threading,           d_dia,          CONSTANT:0,        p_pitch";
ISO_4762_HEXAGON_SOCKET_HEAD_CAP_SCREW                  = "NO , l_nom,               dk_max,             d_dia,           threading,           ds_max,         COMPUTE:FORMULA_3, p_pitch";
ISO_4762_HEXAGON_SOCKET_HEAD_CAP_SCREW_NONPREFERRED     = "NO , l_nom,               dk_max,             d_dia,           threading,           ds_max,         COMPUTE:FORMULA_3, p_pitch";
ISO_4766_GRADE_A_SLOTTED_SET_SCREW_FLAT_POINT           = "NO , l_nom_length,        d_dia,              d_dia,           threading,           d_dia,          CONSTANT:0,        p_pitch";
ISO_7046_1_COUNTERSUNK_FLAT_HEAD_SCREW                  = "YES, l_nom,               dk_nom,             d_dia,           Threading,           d_dia,          COMPUTE:FORMULA_3, P_pitch";
ISO_7046_2_COUNTERSUNK_FLAT_HEAD_SCREW_CLASS_8_SERIES_1 = "YES, l_nom,               dk_nom,             d_dia,           Threading,           d_dia,          COMPUTE:FORMULA_3, P_pitch";
ISO_7046_2_COUNTERSUNK_FLAT_HEAD_SCREW_CLASS_8_SERIES_2 = "YES, l_nom,               dk_nom,             d_dia,           Threading,           d_dia,          COMPUTE:FORMULA_3, P_pitch";
ISO_7047_COUNTERSUNK_RAISED_HEAD_SCREW                  = "YES, l_nom,               dk_nom,             d_dia,           Threading,           d_dia,          COMPUTE:FORMULA_3, P_pitch";
ISO_7434_SLOTTED_SET_SCREW_WITH_CONE_POINT              = "NO , l_max,               d_dia,              d_dia,           threading,           d_dia,          CONSTANT:0,        P_pitch";
ISO_7435_GRADE_A_SLOTTED_SET_SCREW_LONG_DOG_POINT       = "NO , l_nom_length,        d_dia,              d_dia,           threading,           d_dia,          CONSTANT:0,        p_pitch";
ISO_7436_GRADE_A_SLOTTED_SET_SCREW_CUP_POINT            = "NO , l_nom_length,        d_dia,              d_dia,           threading,           d_dia,          CONSTANT:0,        p_pitch";
