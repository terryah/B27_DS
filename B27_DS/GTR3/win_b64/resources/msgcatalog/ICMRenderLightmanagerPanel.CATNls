DialogBoxTitle                                   = "Light Manager";

//Frame Lights
FrameLights.Title                                = "Lights";
FrameLights.LongHelp                             = "Displays the light sources defined in the current Part.
Each light source created is given a default name with a consecutive
number. It can be renamed after clicking twice onto the name.";

MultiList.ColumnTitle1                           = "Name";
MultiList.ColumnTitle2                           = "On/Off";
MultiList.ColumnTitle3                           = "Type";
MultiList.ColumnTitle4                           = "Attached";

//Frame Type
FrameType.Title                                  = "Type";
FrameType.LongHelp                               = "Three light source types are available whose position can
be modified by manipulators.
The manipulators are only visible if the Manipulation Mode
is selected. An active manipulator is highlighted";

RadioButtonSpotLight.ShortHelp                   = "Spot";
RadioButtonSpotLight.LongHelp                    = "The light of a spot light source is similar to a flashlight.
It is useful for tuning the lighting of each object individually.
Spot light sources are defined by a direction vector, a light
cone, the dehidral angle of which can be modified dynamically.
Spot light sources can be rotated and moved. The target point
can be changed.
If Spot light is selected, the Spot Light Parameter tab is available.";

RadioButtonPointLight.ShortHelp                  = "Point";
RadioButtonPointLight.LongHelp                   = "Points radiate light of a light source symmetrically into
all directions (like a plain light bulb).
The point light source can be moved along the axes of the
model coordinate system.";

RadioButtonDirectionLight.ShortHelp              = "Direction";
RadioButtonDirectionLight.LongHelp               = "Beam light sources produce parallel light from a constant direction
(like sunlight). The direction light source can only be rotated.";

//Frame Light Source Options
FrameLightOptions.Title                          = "Light Source Options";
FrameLightOptions.LongHelp                       = "Options for managing light sources";

PushButtonLightNew.ShortHelp                     = "New";
PushButtonLightNew.LongHelp                      = "Creates a new light source with the current settings depending
on the selected type.
The number of definable light sources is limited to 8.";

PushButtonLightDelete.ShortHelp                  = "Delete";
PushButtonLightDelete.LongHelp                   = "Deletes the light source selected in the lights table.";

CheckButtonLightOnOff.ShortHelp                  = "On/Off";
CheckButtonLightOnOff.LongHelp                   = "Switches the current light source on or off.";

FrameAttached.Title                              = "Attached";
FrameAttached.LongHelp                           = "Attach light source to the view or the model coordinate system";

RadioButtonView.Title                            = "View";
RadioButtonView.LongHelp                         = "Fixes the light source to the view. In case of geometry
rotations, its position remains unchanged.";
RadioButtonView.ShortHelp                        = "Fix to view";

RadioButtonModel.Title                           = "Model";
RadioButtonModel.LongHelp                        = "Fixes the light source to the model coordinate system.
It will be rotated together with the geometry.";
RadioButtonModel.ShortHelp                       = "Fix to model coordinate system";

FrameLightOptionsGlobal.Title                    = "Global";

CheckButtonLightSymbol.LongHelp                 = "Selects/clears the display of the light source symbols globally.
The illumination of the parts displayed in the Experience Area
does not change.";
CheckButtonLightSymbol.ShortHelp                = "Switch light source symbols on/off";


//Position tab
TabPagePosition.Title                           = "Position";

FramePosition.Title                             = "Position";
FramePosition.LongHelp                          = "Defines the light source position.";

TabContainerBottom.TabPagePosition.FramePosition.InvisibleFramePosition.LabelX.Title     = "X: ";
TabContainerBottom.TabPagePosition.FramePosition.InvisibleFramePosition.LabelX.LongHelp  = "Defines the X position coordinate
of the selected light source.";
TabContainerBottom.TabPagePosition.FramePosition.InvisibleFramePosition.LabelX.ShortHelp = "Position X coordinate";
TabContainerBottom.TabPagePosition.FramePosition.InvisibleFramePosition.LabelY.Title     = "Y: ";
TabContainerBottom.TabPagePosition.FramePosition.InvisibleFramePosition.LabelY.LongHelp  = "Defines the Y position coordinate
of the selected light source.";
TabContainerBottom.TabPagePosition.FramePosition.InvisibleFramePosition.LabelY.ShortHelp = "Position Y coordinate";
TabContainerBottom.TabPagePosition.FramePosition.InvisibleFramePosition.LabelZ.Title     = "Z: ";
TabContainerBottom.TabPagePosition.FramePosition.InvisibleFramePosition.LabelZ.LongHelp  = "Defines the Y position coordinate
of the selected light source.";
TabContainerBottom.TabPagePosition.FramePosition.InvisibleFramePosition.LabelZ.ShortHelp = "Position Z coordinate";


CheckButtonPosition.Title                        = "Position";
CheckButtonPosition.LongHelp                     = "Defines the light source position by selecting a point element.";
CheckButtonPosition.ShortHelp                    = "Select a point";

FrameDirection.Title                             = "Direction";
FrameDirection.LongHelp                          = "Only available for the light source types Direction and Spot.
The axis of orientation depends on the attachment View or Model
of the selected light source.
In the direction selection field, you can define the direction
by selecting a line element or an axis. For this definition
the attachment Model must be selected.";

TabContainerBottom.TabPagePosition.FrameDirection.InvisibleFrameDirection.LabelX.Title     = "X: ";
TabContainerBottom.TabPagePosition.FrameDirection.InvisibleFrameDirection.LabelX.LongHelp  = "Defines the X direction coordinate
of the selected light source.";
TabContainerBottom.TabPagePosition.FrameDirection.InvisibleFrameDirection.LabelX.ShortHelp = "Direction X coordinate";
TabContainerBottom.TabPagePosition.FrameDirection.InvisibleFrameDirection.LabelY.Title     = "Y: ";
TabContainerBottom.TabPagePosition.FrameDirection.InvisibleFrameDirection.LabelY.LongHelp  = "Defines the Y direction coordinate
of the selected light source.";
TabContainerBottom.TabPagePosition.FrameDirection.InvisibleFrameDirection.LabelY.ShortHelp = "Direction Y coordinate";
TabContainerBottom.TabPagePosition.FrameDirection.InvisibleFrameDirection.LabelZ.Title     = "Z: ";
TabContainerBottom.TabPagePosition.FrameDirection.InvisibleFrameDirection.LabelZ.LongHelp  = "Defines the Z direction coordinate
of the selected light source.";
TabContainerBottom.TabPagePosition.FrameDirection.InvisibleFrameDirection.LabelZ.ShortHelp = "Direction Z coordinate";

PushButtonDirectionX.Title                       = "X";
PushButtonDirectionX.LongHelp                    = "Aligns the light source direction to the X axis (View or Model).";
PushButtonDirectionX.ShortHelp                   = "Align to X axis";

PushButtonDirectionY.Title                       = "Y";
PushButtonDirectionY.LongHelp                    = "Aligns the light source direction to the Y axis (View or Model).";
PushButtonDirectionY.ShortHelp                   = "Align to Y axis";

PushButtonDirectionZ.Title                       = "Z";
PushButtonDirectionZ.LongHelp                    = "Aligns the light source direction to the Z axis (View or Model).";
PushButtonDirectionZ.ShortHelp                   = "Align to Z axis";

PushButtonDirectionInvert.Title                  = "Invert";
PushButtonDirectionInvert.LongHelp               = "Inverts the selected direction.";
PushButtonDirectionInvert.ShortHelp              = "Invert direction";


//Parameter tab
TabPageParameter.Title                           = "Parameter";

RadioButtonColorWhite.Title                      = "White";
RadioButtonColorWhite.LongHelp                   = "The light source emits white light.";
RadioButtonColorWhite.ShortHelp                  = "White light";

RadioButtonColor.Title                           = "Color";
RadioButtonColor.LongHelp                        = "The light source emits colored light.";
RadioButtonColor.ShortHelp                       = "Colored light";
ColorButtonParameter.LongHelp                    = "Opens the Color dialog box to define the color.";

LabelIntensity.Title                             = "Intensity: ";
LabelIntensity.LongHelp                          = "Defines the light intensity using the slider or by entering
a value directly in the text field.
The intensity of a light source is the maximal lightness
value of three colors (ambient, diffuse and specular). ";
LabelIntensity.ShortHelp                         = "Light intensity";

SliderIntensity.ShortHelp                        = "Change light intensity dynamically";

SpinnerIntensity.ShortHelp                       = "Change light intensity numerically";

LabelAmbient.Title                               = "Ambient: ";
LabelAmbient.LongHelp                            = "Defines the intensity of light emitted in any direction by
the object, even if not lit by any light source.";
LabelAmbient.ShortHelp                           = "Ambient light";

SliderAmbient.ShortHelp                          = "Change ambient light intensity dynamically";

SpinnerAmbient.ShortHelp                         = "Change ambient light intensity numerically";

LabelDiffuse.Title                               = "Diffuse: ";
LabelDiffuse.LongHelp                            = "Defines the intensity of light diffused
by the object when lit by a light source.";
LabelDiffuse.ShortHelp                           = "Diffusion intensity";

SliderDiffuse.ShortHelp                          = "Change diffusion intensity dynamically";

SpinnerDiffuse.ShortHelp                         = "Change diffusion intensity numerically";

LabelSpecular.Title                              = "Specular: ";
LabelSpecular.LongHelp                           = "Defines the intensity and color of light reflected in one
particular direction. This coefficient affects the highlight
on shiny surfaces.";
LabelSpecular.ShortHelp                          = "Specular light";

SliderSpecular.ShortHelp                         = "Change specular light intensity dynamically";

SpinnerSpecular.ShortHelp                        = "Change specular light intensity numerically";


//Spot Parameter tab
TabPageSpotParameter.Title                       = "Spot Parameter";

LabelAngle.Title                                 = "Angle: ";
LabelAngle.LongHelp                              = "Setting of the angle of the spot light cone. In case of
setting the Angle to 90deg and the Sharpness to 0 a point
light source with the same position will result.";
LabelAngle.ShortHelp                             = "Light cone angle";

SliderAngle.ShortHelp                            = "Change light cone angle dynamically";

SpinnerAngle.LongHelp                            = "Change light cone angle numerically";

LabelSharpness.Title                             = "Sharpness: ";
LabelSharpness.LongHelp                          = "Setting of the sharpness of the spot light cone. Higher values
concentrate the light beam in the centre of the light cone.";
LabelSharpness.ShortHelp                         = "Light cone sharpness";

SliderSharpness.ShortHelp                        = "Change light cone sharpness dynamically";

SpinnerSharpness.ShortHelp                       = "Change light cone sharpness numerically";
