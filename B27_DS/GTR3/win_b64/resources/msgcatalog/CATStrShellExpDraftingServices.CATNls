//=============================================================================
//                          COPYRIGHT Dassault Systemes 2007
//-----------------------------------------------------------------------------
// FILENAME    :    CATStrShellExpDraftingServices
// LOCATION    :    CATStrDrafting\CNext\resources\msgcatalog/
// AUTHOR      :    WSA
// DATE        :    October 2007
//------------------------------------------------------------------------------
// DESCRIPTION :    Resources File for NLS purpose related to ShellExpansion
//                  
//------------------------------------------------------------------------------
// COMMENTS    :	 ENGLISH VERSION
//------------------------------------------------------------------------------
//MODIFICATIONS     user  date      purpose
//   HISTORY        ----  ----      -------
//------------------------------------------------------------------------------

TopOpInShExpansionTangentsColinear_ERR_7691  =  "The input shell plate contains some faces having points that exhibit colinearity of Parametric Tangents: Modify the input body."; 

TopOpInShExpansionFaceNormalToRef_ERR_7692  =  "The input shell plate contains faces having portions that are normal to Reference Line: Modify the input body."; 

TopOpInFrFSUClosedSharpCorner_ERR_7673  =  "Closed Input body: Modify the input body."; 

TopOpInFrFSUInvalidSkin_ERR_7672  =  "Input body should be Mono-Domain, Manifold and Connex: Modify the input body.";

TopOpInFrFSUSelfIntersectingResult_ERR_7675  =  "Flattened skin has self intersecting boundary: Modify the input body.";  

TopOpInFrFSUResultIsTwisted_ERR_7678  =  "The result computation cannot complete as some of the faces in the skin develop surface twists on flattening."; 

TopOpInFrFSUResultFlatteningFailed_ERR_7679  =  "Flattening has failed due to an internal problem.";

TopOpInFrFSUResultComputationFailed_ERR_7677  =  "Final reconstruction of flattened skin fails due to an internal problem."; 

TopOpInFrFSUDegenSurface_ERR_7671 =  "Operation failed possibly because some of the Faces have partly or fully degenerated geometry: Modify the input body.";

TopOpInShExpansionSmallTopology_ERR_7694 =  "Internal Mesh generation has failed possibly due to small topology in input body: Modify the input body.";

TopOpInFrFSUResultAssemblyFailed_ERR_7680 =  "Final reconstruction of flattened skin has failed due to problem in assembling certain internal/external edges: Simplify the input body if it has got too many faces or the faces are vastly different sized.";

TopOpInShExpansionResultFlatteningFailed_ERR_7693 =  "Flattening has failed due to an internal problem.";

CATStrShellExpDraftingServices.TopOpInSurfaceUnfoldIniPlaneOffSkin_ERR_7666  =  "Unfold Operator: Initial location plane centre doesn't lie on input skin or the chosen face. Modify the initial location plane."; 

CATStrShellExpDraftingServices.TopOpInSurfaceUnfoldIniPlaneSkinNotParallel_ERR_7667  =  "Unfold Operator: Initial location plane is not tangential to input skin or the chosen face. Modify the initial location plane."; 

TopOpInNonConnexResult_ERR_7662  =  "Operation leads to a non connex result."; 

TopOpInFrFSUDeviationDueToClean_WNG_7676  =  "Free Form Unfold Operator: The transferred curves may be quite distorted due to automatic healing."; 

CATStrShellExpDraftingServices.TopOpInLayDownNotOnSkin_ERR_6450  =  "The body to operate is not on the input skin body: Project the body onto the skin."; 

CATStrShellExpDraftingServices.MissingDrawingTemplate  =  "Drawing template is missing in the list of inputs!";

CATStrShellExpDraftingServices.MissingGVSFile  =  "GVS file is missing in the list of inputs!"; 

CATStrShellExpDraftingServices.MissingShellPlate  =  "No shell plate in the list of inputs!"; 

CATStrShellExpDraftingServices.MissingReferenceLine  =  "Reference line is missing in the list of inputs!"; 

CATStrShellExpDraftingServices.MissingSymmetryPlane  =  "Symmetry plane is missing in the list of inputs!";
