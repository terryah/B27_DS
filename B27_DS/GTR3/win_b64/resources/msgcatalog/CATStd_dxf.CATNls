//=============================================================================
//                          COPYRIGHT DASSAULT SYSTEMES PROVENCE 2007 
//-----------------------------------------------------------------------------
// FILENAME    :    CATStd_dxf.CATNls
// AUTHOR      :    prb
// DATE        :    07/2007
//------------------------------------------------------------------------------
// DESCRIPTION :    NLS File for names of nodes in standard xml files for dxf
//                  
//------------------------------------------------------------------------------
// COMMENTS    :
//------------------------------------------------------------------------------
//MODIFICATIONS     user  date      purpose
//   HISTORY        ----  ----      -------
// BMT : 02/09 : Authorize yes/No : REport correction phase2->phase1 : A0633407
//------------------------------------------------------------------------------

DxfImport.Title = "DXF Import";

DxfImport.LineTypeMapping.Title = "Line Type Mapping";
DxfImport.LineTypeMapping.DxfLineTypeName.Title = "DXF Line Type name";
DxfImport.LineTypeMapping.CatiaLineTypeNumber.Title = "CATIA Line Type number";

DxfImport.ColorToThicknessMapping.Title = "Color To Thickness Mapping";
DxfImport.ColorToThicknessMapping.AuthorizeMapping.Title = "Activate Mapping";
DxfImport.ColorToThicknessMapping.AuthorizeMapping.Yes.Title = "Yes";
DxfImport.ColorToThicknessMapping.AuthorizeMapping.No.Title = "No";
DxfImport.ColorToThicknessMapping.DefaultThickness.Title = "Default Thickness";
DxfImport.ColorToThicknessMapping.DxfColorNumber.Title = "DXF Color number";
DxfImport.ColorToThicknessMapping.CatiaThicknessNumber.Title = "CATIA Thickness Number";

DxfImport.TextFontMapping.Title = "Text Font Mapping";
DxfImport.TextFontMapping.Default.Title = "Default";
DxfImport.TextFontMapping.Default.CatiaFontName.Title = "CATIA Font Name";
DxfImport.TextFontMapping.Default.Ratio.Title = "Ratio";
DxfImport.TextFontMapping.DefaultDBCS.Title = "Default for DBCS";
DxfImport.TextFontMapping.DefaultDBCS.CatiaFontName.Title = "CATIA Font Name";
DxfImport.TextFontMapping.DefaultDBCS.Ratio.Title = "Ratio";
DxfImport.TextFontMapping.DxfFontName.Title = "DXF Font Name";
DxfImport.TextFontMapping.CatiaFontName.Title = "CATIA Font Name";
DxfImport.TextFontMapping.Ratio.Title = "Ratio";

DxfImport.ThickPolylineMapping.Title = "Thick Polyline Mapping";
DxfImport.ThickPolylineMapping.CreateAreaFill.Title = "Activate AreaFill Creation";
DxfImport.ThickPolylineMapping.CreateAreaFill.Yes.Title = "Yes";
DxfImport.ThickPolylineMapping.CreateAreaFill.No.Title = "No";
DxfImport.ThickPolylineMapping.MinThicknessForAreaFill.Title = "Minimal Thickness for Area Fill (in mm)";

DxfImport.ColorMapping.Title = "Color Mapping";
DxfImport.ColorMapping.AdaptToBackground.Title = "Adapt to background";
DxfImport.ColorMapping.AdaptToBackground.Yes.Title = "Yes";
DxfImport.ColorMapping.AdaptToBackground.No.Title = "No";
DxfImport.ColorMapping.DarkBackgroundLuminosity.Title = "Dark background luminosity (0..255)";
DxfImport.ColorMapping.LightBackgroundLuminosity.Title = "Light background luminosity (0..255)";
