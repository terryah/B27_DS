DialogBoxTitle                                   = "Styling Corner";

//Selection options ------------------------------------------------------------------------------------

LabelCurve1.Title                                = "Curve 1: ";
LabelCurve1.LongHelp                             = "Select Curve 1 for the styling corner creation.";
LabelCurve1.ShortHelp                            = "Select Curve 1";

LabelCurve2.Title                                = "Curve 2: ";
LabelCurve2.LongHelp                             = "Select Curve 2 for the styling corner creation.";
LabelCurve2.ShortHelp                            = "Select Curve 2";

CheckButtonCurve2.Title                          = "";
CheckButtonCurve2.LongHelp                       = "OFF:
A single curve can be selected for the styling corner creation.
The styling corner is created between both ends of the curve.
If several solutions are possible, the computed solution depends on the
selection points on the curves or the selection point on the single curve.
The styling corner is created in the quadrant with the smallest parameter
distance of the styling corner end points to the selection points.
ON:
Curve 1 and Curve 2 must be selected for the styling corner creation.";
CheckButtonCurve2.ShortHelp                      = "Activate selection of two curves.";


CheckTrimCurve1.LongHelp                         = "The input curve 1 is trimmed at the corresponding
end point of the created styling corner.
The original input curve is placed in no-show mode.";
CheckTrimCurve1.ShortHelp                        = "Trim curve 1";


CheckTrimCurve2.LongHelp                         = "The input curve 2 is trimmed at the corresponding
end point of the created styling corner.
The original input curve is placed in no-show mode.";
CheckTrimCurve2.ShortHelp                        = "Trim Curve 2";


// Radius / Curve Type / Support frames ----------------------------------------------------------------

FrameRadius.Title                                = "Radius";
FrameRadius.LongHelp                             = "The lead-in points of the styling corner into the
input curves are defined by specifying an arc radius value.";
FrameRadius.ShortHelp                            = "Definition of the lead-in points by an arc radius value";

FrameCurveType.Title                             = "Curve Type";
FrameCurveType.LongHelp                          = "The fillet curve of the styling corner can be 
- an arc. All parameters for Shape are dimmed and the arc
  cannot be modified. The radius is valid for the whole curve.
- a Bezier curve with G1, G2 or G3 at the lead-in points,
  for G2 a lead-in radius can be specified.
  The styling corner has a fix shape whereby radius defines
  also the Center Radius.
- a Bezier curve with G1, G2 or G3 at the lead-in points,
  for G2 a lead-in radius can be specified.
  The styling corner has a variable shape controlable via
  the Center Radius, the Form Factor or the center point
  manipulator.
The Radius defines always the lead-in points.";

RadioButtonArc.LongHelp                          = "The styling corner will be an arc with a defined Radius, the arc radius.
The arc radius determines the position of the accumulating points of the styling corner.
If the curve tangent vectors of the original curves at the ends of the arc are
located in the same plane, tangent continuous transitions are realized.";
RadioButtonArc.ShortHelp                         = "Arc";

RadioButtonBlend.LongHelp                        = "The styling corner is calculated from the radius and the continuity specified.";
RadioButtonBlend.ShortHelp                       = "Blend";

RadioButtonShapeBlend.LongHelp                   = "The styling corner is calculated from the radius and the continuity specified.
With all continuities higher than G0, the shape can additionally
be influenced with the options Center Radius and Form Factor.";
RadioButtonShapeBlend.ShortHelp                  = "Shape Blend";

FrameSupport.Title                               = "Support";
FrameSupport.LongHelp                            = "The styling corner can be projected onto
a surface or a plane.";

CheckSupport.Title                               = "Support: ";
CheckSupport.LongHelp                            = "Selection of a surface or plane to be used as projection base.";
CheckSupport.ShortHelp                           = "Select support";

CheckProjection.Title                            = "Projection";
CheckProjection.LongHelp                         = "The styling corner will be projected in normal direction onto the selected support.";
CheckProjection.ShortHelp                        = "Projection";

//until R19 Hotfix 10
FrameProjection.Title                            = "Projection";
FrameProjection.LongHelp                         = "The styling corner can be projected onto a surface
or plane to be selected as projection base.";


//Curve Options frames
FrameOptionsCurve1.Title                         = "Curve 1 Options";
FrameOptionsCurve1.LongHelp                      = "Only available for the curve types 'Blend' and 'Shape Blend'.
Continuity setting and lead-in radius defintion for Curve 1.";
FrameOptionsCurve2.Title                         = "Curve 2 Options";
FrameOptionsCurve2.LongHelp                      = "Only available for the curve types 'Blend' and 'Shape Blend'.
Continuity setting and lead-in radius defintion for Curve 2.";

LabelContinuity1.Title                           = "Continuity: ";
LabelContinuity1.LongHelp                        = "Definition of the continuity between styling corner and Curve 1.";
LabelContinuity1.ShortHelp                       = "Continuity definition";

LabelContinuity2.Title                           = "Continuity: ";
LabelContinuity2.LongHelp                        = "Definition of the continuity between styling corner and Curve 2.";
LabelContinuity2.ShortHelp                       = "Continuity definition";

//Continuity Curve 1
Continuity1.Button_G0.LongHelp                   = "The styling corner will be created with position (G0) continuous transition to Curve 1.";
Continuity1.Button_G0.ShortHelp                  = "G0 continuity between styling corner and Curve 1";

Continuity1.Button_G1.LongHelp                   = "The styling corner will be created with a lead-in arc with
tangent (G1) continuous transition to Curve 1.
The lead-in points are defined by the Radius.
In case of using Curve Type Shape Blend, the styling corner
can be modified via Center Radius and Form Factor.
The transition quality will be preserved after the modification.";
Continuity1.Button_G1.ShortHelp                  = "G1 continuity between styling corner and Curve 1";

Continuity1.Button_G2.LongHelp                   = "The created styling corner will be created with a lead-in arc with
curvature (G2) continuous transition to Curve 1.
The lead-in points are defined by the Radius.
You can specify a lead-in radius at the lead-in points.
However, in this case the transition quality will NOT be preserved.
In case of using Curve Type Shape Blend, the styling corner
can be modified via Center Radius and Form Factor.
The transition quality will be preserved after the modification.";
Continuity1.Button_G2.ShortHelp                  = "G2 continuity between styling corner and Curve 1";

Continuity1.Button_G3.LongHelp                   = "The styling corner will be created with
torsion (G3) continuous transition to Curve 1.
The curvature and torsion radius at the lead-in point
will be adopted from the input curve.";
Continuity1.Button_G3.ShortHelp                  = "G3 continuity between styling corner and Curve 1";

//Continuity Curve 2
Continuity2.Button_G0.LongHelp                   = "The styling corner will be created with position (G0) continuous transition to Curve 2.";
Continuity2.Button_G0.ShortHelp                  = "G0 continuity between styling corner and Curve 2";

Continuity2.Button_G1.LongHelp                   = "The styling corner will be created with a lead-in arc with
tangent (G1) continuous transition to Curve 2.
The lead-in points are defined by the Radius.
In case of using Curve Type Shape Blend, the styling corner
can be modified via Center Radius and Form Factor.
The transition quality will be preserved after the modification.";
Continuity2.Button_G1.ShortHelp                  = "G1 continuity between styling corner and Curve 2";

Continuity2.Button_G2.LongHelp                   = "The created styling corner will be created with a lead-in arc with
curvature (G2) continuous transition to Curve 2.
The lead-in points are defined by the Radius.
You can specify a lead-in radius at the lead-in points.
However, in this case the transition quality will NOT be preserved.
In case of using Curve Type Shape Blend, the styling corner
can be modified via Center Radius and Form Factor.
The transition quality will be preserved after the modification.";
Continuity2.Button_G2.ShortHelp                  = "G2 continuity between styling corner and Curve 2";

Continuity2.Button_G3.LongHelp                   = "The styling corner will be created with
torsion (G3) continuous transition to Curve 1.
The curvature and torsion radius at the lead-in point
will be adopted from the input curve.";
Continuity2.Button_G3.ShortHelp                  = "G3 continuity between styling corner and Curve 2";

LinkButtonHLinkOrder.LongHelp                    = "The Continuity and lead-in radius settings for Curve 2 will be adopted from Curve 1.";
LinkButtonHLinkOrder.ShortHelp                   = "Equal Continuity and lead-in radius settings";

CheckOptionsAccRadius1.Title                     = "Lead-in radius: ";
CheckOptionsAccRadius1.LongHelp                  = "Only available for G2 continuity.
Definition of a lead-in radius at the lead-in point with
a lead-in arc with G2 continuous transition to Curve 1.
The G2 continuity will NOT be preserved.";
CheckOptionsAccRadius1.ShortHelp                 = "Lead-in radius at G2 continuous transition to Curve 1";

CheckOptionsAccRadius2.Title                     = "Lead-in radius: ";
CheckOptionsAccRadius2.LongHelp                  = "Only available for G2 continuity.
Definition of a lead-in radius at the lead-in point with
a lead-in arc with G2 continuous transition to Curve 2.
The G2 continuity will NOT be preserved.";
CheckOptionsAccRadius2.ShortHelp                 = "Lead-in radius at G2 continuous transition to Curve 2";

//Shape frame ------------------------------------------------------------------------------------------

FrameOptions.Title                               = "Shape";
FrameOptions.LongHelp                            = "Only available for Curve Type Shape Blend.
Definition of the fillet shape.";

RadioButtonCenterRadius.Title                    = "Center radius: ";
RadioButtonCenterRadius.LongHelp                 = "Only available for Continuity G1 and higher.
In case of a lead-in arc with G1, G2, or G3 transition to the input curves at the
lead-in points the radius in the middle of the styling corner can be specified.
The continuity will be preserved.";
RadioButtonCenterRadius.ShortHelp                = "Radius in the middle of the styling corner";

RadioButtonFormFactor.Title                      = "Form factor: ";
RadioButtonFormFactor.LongHelp                   = "Only available for Continuity G1 and higher.
If lead-in arcs have been created using G1, G2, or G3 transition
the tangent length of the styling corners is modified.
Values smaller than 1 create flat fillets, values higher than 1 steep fillet.
The continuity will be preserved.";
RadioButtonFormFactor.ShortHelp                  = "Influences the tangent length.";

LinkButtonLinkOrder.LongHelp                     = "A Center radius has a corresponding form factor.
Changing of the center radius changes the form factor and vice versa.";
LinkButtonLinkOrder.ShortHelp                    = "Center radius is linked with form factor";

//?????????????????????????????????????????????
CheckTrimCurve1.Title     = "Trim Curve 1";
CheckTrimCurve2.Title     = "Trim Curve 2";
