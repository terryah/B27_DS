Title = "Bend Definition";

More = "More >>";
Less = "<< Less";
Warning = "Warning";
Error = "Error";

_DefinitionFrame._WallsFrame._Wall1Label.Title    = "Wall 1 :";
_DefinitionFrame._WallsFrame._Wall1Label.LongHelp = "First wall";

_DefinitionFrame._WallsFrame._Wall1Editor.ShortHelp = "First wall identifier";
_DefinitionFrame._WallsFrame._Wall1Editor.LongHelp  = "Displays the first wall identifier";
_DefinitionFrame._WallsFrame._Wall1Editor.Help      = "First wall identifier";

_DefinitionFrame._WallsFrame._Wall2Label.Title    = "Wall 2 :";
_DefinitionFrame._WallsFrame._Wall2Label.LongHelp = "Second wall";

_DefinitionFrame._WallsFrame._Wall2Editor.ShortHelp = "Second wall identifier";
_DefinitionFrame._WallsFrame._Wall2Editor.LongHelp  = "Displays the second wall identifier";
_DefinitionFrame._WallsFrame._Wall2Editor.Help      = "Second wall identifier";

_DefinitionFrame._ValuesFrame._RadiusLabel.Title    = "Radius : ";
_DefinitionFrame._ValuesFrame._RadiusLabel.LongHelp = "Internal radius of the bend";

_DefinitionFrame._ValuesFrame._AngleLabel.Title    = "Angle : ";
_DefinitionFrame._ValuesFrame._AngleLabel.LongHelp = "Angle between the walls";

_DefinitionFrame._ValuesFrame._AngleEditor.ShortHelp = "Angle between the walls";
_DefinitionFrame._ValuesFrame._AngleEditor.LongHelp  = "Displays the angle between the walls";
_DefinitionFrame._ValuesFrame._AngleEditor.Help      = "Angle between the walls";

_MoreFrame._ExtremitiesFrame.Title    = "Extremities";
_MoreFrame._ExtremitiesFrame.LongHelp = "Enter the characteristics of bend extremities";
_MoreFrame._ExtremitiesFrame.Help     = "Extremities characteristics";

_MoreFrame._ExtremitiesFrame._ExtremitiesCombo.ShortHelp = "Select the extremity";
_MoreFrame._ExtremitiesFrame._ExtremitiesCombo.LongHelp  = "Select the extremity to modify";
_MoreFrame._ExtremitiesFrame._ExtremitiesCombo.Help      = "Current extremity";

_MoreFrame._ExtremitiesFrame._ExtremityIcons.ShortHelp = "Select the extremity type";
_MoreFrame._ExtremitiesFrame._ExtremityIcons.LongHelp  = "Choose the type of bend extremities";
_MoreFrame._ExtremitiesFrame._ExtremityIcons.Help      = "Select the extremity type";

_MoreFrame._ExtremitiesFrame._ExtremityIcons._MiniNoReliefButton.ShortHelp     = "Mini with no relief";
_MoreFrame._ExtremitiesFrame._ExtremityIcons._MiniSquareReliefButton.ShortHelp = "Mini with square relief";
_MoreFrame._ExtremitiesFrame._ExtremityIcons._MiniRoundReliefButton.ShortHelp  = "Mini with round relief";
_MoreFrame._ExtremitiesFrame._ExtremityIcons._LimitedLinearButton.ShortHelp    = "Linear shape";
_MoreFrame._ExtremitiesFrame._ExtremityIcons._LimitedCurvedButton.ShortHelp    = "Curved shape";
_MoreFrame._ExtremitiesFrame._ExtremityIcons._MaxiButton.ShortHelp             = "Maximum bend";

_MoreFrame._ExtremitiesFrame._StandardButton.Title     = "Standard";
_MoreFrame._ExtremitiesFrame._StandardButton.ShortHelp = "Reset standard bend extremity type";
_MoreFrame._ExtremitiesFrame._StandardButton.LongHelp  = "Set the bend extremity type to the Sheet Metal part standard type";
_MoreFrame._ExtremitiesFrame._StandardButton.Help      = "Reset standard bend extremity type";

_MoreFrame._ExtremitiesFrame._LengthFrame.LongHelp = "Relief lengths";
_MoreFrame._ExtremitiesFrame._LengthFrame.Help     = "Relief lengths";

_MoreFrame._ExtremitiesFrame._LengthFrame._L1Label.Title    = "L1 : ";
_MoreFrame._ExtremitiesFrame._LengthFrame._L1Label.LongHelp = "Relief first length";

_MoreFrame._ExtremitiesFrame._LengthFrame._L2Label.Title    = "L2 : ";
_MoreFrame._ExtremitiesFrame._LengthFrame._L2Label.LongHelp = "Relief second length";

_MoreFrame._AllowanceFrame.Title    = "Allowance";
_MoreFrame._AllowanceFrame.LongHelp = "Enter the parameters of the bend allowance";
_MoreFrame._AllowanceFrame.Help     = "Allowance";

_MoreFrame._AllowanceFrame._FrameKFactor._KFactorLabel.Title     = "K Factor : ";
_MoreFrame._AllowanceFrame._FrameKFactor._KFactorLabel.LongHelp  = "K Factor";

_MoreFrame._AllowanceFrame._DINNormaPushButton.Title     = "DIN Norma";
_MoreFrame._AllowanceFrame._DINNormaPushButton.ShortHelp = "DIN Norma";
_MoreFrame._AllowanceFrame._DINNormaPushButton.LongHelp  = "The K Factor is calculated using the DIN Norma";

_MoreFrame._AllowanceFrame._FrameAllowance._AllowanceLabel.Title     = "Allowance : ";
_MoreFrame._AllowanceFrame._FrameAllowance._AllowanceLabel.LongHelp  = "Allowance";

_MoreFrame._AllowanceFrame._FromKFactorPushButton.Title     = "From K Factor";
_MoreFrame._AllowanceFrame._FromKFactorPushButton.ShortHelp = "From K Factor";
_MoreFrame._AllowanceFrame._FromKFactorPushButton.LongHelp  = "The allowance is calculated using the K Factor";

_MoreFrame._BendCornerRelievesFrame.Title    = "Corner Relief";
_MoreFrame._BendCornerRelievesFrame.LongHelp = "Chooses the default type of corner relief";
_MoreFrame._BendCornerRelievesFrame.Help     = "Corner relief characteristics";

_MoreFrame._BendCornerRelievesFrame._CornerReliefCheckButton.Title     = "Automatic corner relief creation";
_MoreFrame._BendCornerRelievesFrame._CornerReliefCheckButton.LongHelp  = "Activation of the creation of corner relief";
_MoreFrame._BendCornerRelievesFrame._CornerReliefCheckButton.ShortHelp = "Creation of corner relief";
_MoreFrame._BendCornerRelievesFrame._CornerReliefCheckButton.Help      = "Activation of the creation of corner relief";

_MoreFrame._BendCornerRelievesFrame._CornerReliefTypeFrame._CornerRelievesIconBox._TriangularPushButton.Title  = "Triangular Corner Relief";
_MoreFrame._BendCornerRelievesFrame._CornerReliefTypeFrame._CornerRelievesIconBox._CircularPushButton.Title    = "Circular Corner Relief";
_MoreFrame._BendCornerRelievesFrame._CornerReliefTypeFrame._CornerRelievesIconBox._RectangularPushButton.Title = "Rectangular Corner Relief";

_MoreFrame._BendCornerRelievesFrame._CornerReliefTypeFrame._StandardCornerReliefButton.Title     = "Standard";
_MoreFrame._BendCornerRelievesFrame._CornerReliefTypeFrame._StandardCornerReliefButton.ShortHelp = "Reset standard corner relief type";
_MoreFrame._BendCornerRelievesFrame._CornerReliefTypeFrame._StandardCornerReliefButton.LongHelp  = "Set the corner relief type to the Sheet Metal part standard type";
_MoreFrame._BendCornerRelievesFrame._CornerReliefTypeFrame._StandardCornerReliefButton.Help      = "Reset standard corner relief type";

_MoreFrame._BendCornerRelievesFrame._CornerReliefTypeFrame._CornerReliefRadiusFrame._CornerReliefRadiusLabel.Title    = "Radius : ";
_MoreFrame._BendCornerRelievesFrame._CornerReliefTypeFrame._CornerReliefRadiusFrame._CornerReliefRadiusLabel.LongHelp = "Radius of the circular corner relief";

// Extremities...
_MoreContainer._BendExtremitiesTabPage.Title    = "Bend Extremities";
_MoreContainer._BendExtremitiesTabPage.LongHelp = "Chooses the default type of bend extremities";

_MoreContainer._BendExtremitiesTabPage._ExtremitiesFrame._ExtremityIcons.ShortHelp = "Select the extremity type";
_MoreContainer._BendExtremitiesTabPage._ExtremitiesFrame._ExtremityIcons.LongHelp  = "Choose the type of bend extremities";
_MoreContainer._BendExtremitiesTabPage._ExtremitiesFrame._ExtremityIcons.Help      = "Select the extremity type";
_MoreContainer._BendExtremitiesTabPage._ExtremitiesFrame._ExtremityIcons._MiniNoReliefButton.ShortHelp     = "Mini with no relief";
_MoreContainer._BendExtremitiesTabPage._ExtremitiesFrame._ExtremityIcons._MiniSquareReliefButton.ShortHelp = "Mini with square relief";
_MoreContainer._BendExtremitiesTabPage._ExtremitiesFrame._ExtremityIcons._MiniRoundReliefButton.ShortHelp  = "Mini with round relief";
_MoreContainer._BendExtremitiesTabPage._ExtremitiesFrame._ExtremityIcons._LimitedLinearButton.ShortHelp    = "Linear shape";
_MoreContainer._BendExtremitiesTabPage._ExtremitiesFrame._ExtremityIcons._LimitedCurvedButton.ShortHelp    = "Curved shape";
_MoreContainer._BendExtremitiesTabPage._ExtremitiesFrame._ExtremityIcons._MaxiButton.ShortHelp             = "Maximum bend";

_MoreContainer._BendExtremitiesTabPage._ExtremitiesFrame._StandardButton.Title     = "Standard";
_MoreContainer._BendExtremitiesTabPage._ExtremitiesFrame._StandardButton.ShortHelp = "Reset standard bend extremity type";
_MoreContainer._BendExtremitiesTabPage._ExtremitiesFrame._StandardButton.LongHelp  = "Set the bend extremity type to the Sheet Metal part standard type";
_MoreContainer._BendExtremitiesTabPage._ExtremitiesFrame._StandardButton.Help      = "Reset standard bend extremity type";

_MoreContainer._BendExtremitiesTabPage._ExtremitiesFrame._LengthFrame.LongHelp = "Relief lengths";
_MoreContainer._BendExtremitiesTabPage._ExtremitiesFrame._LengthFrame.Help     = "Relief lengths";
_MoreContainer._BendExtremitiesTabPage._ExtremitiesFrame._LengthFrame._L1Label.Title    = "L1 : ";
_MoreContainer._BendExtremitiesTabPage._ExtremitiesFrame._LengthFrame._L1Label.LongHelp = "Relief first length";
_MoreContainer._BendExtremitiesTabPage._ExtremitiesFrame._LengthFrame._L2Label.Title    = "L2 : ";
_MoreContainer._BendExtremitiesTabPage._ExtremitiesFrame._LengthFrame._L2Label.LongHelp = "Relief second length";

// Left Extremity...
_MoreContainer.LeftExtremity.Title    = "Left Extremity";
_MoreContainer.LeftExtremity.LongHelp = "Chooses the default type of left extremity";

_MoreContainer.LeftExtremity._ExtremitiesFrame._ExtremityIcons.ShortHelp = "Select the extremity type";
_MoreContainer.LeftExtremity._ExtremitiesFrame._ExtremityIcons.LongHelp  = "Choose the type of extremities";
_MoreContainer.LeftExtremity._ExtremitiesFrame._ExtremityIcons.Help      = "Select the extremity type";
_MoreContainer.LeftExtremity._ExtremitiesFrame._ExtremityIcons._MiniNoReliefButton.ShortHelp     = "Mini with no relief";
_MoreContainer.LeftExtremity._ExtremitiesFrame._ExtremityIcons._MiniSquareReliefButton.ShortHelp = "Mini with square relief";
_MoreContainer.LeftExtremity._ExtremitiesFrame._ExtremityIcons._MiniRoundReliefButton.ShortHelp  = "Mini with round relief";
_MoreContainer.LeftExtremity._ExtremitiesFrame._ExtremityIcons._LimitedLinearButton.ShortHelp    = "Linear shape";
_MoreContainer.LeftExtremity._ExtremitiesFrame._ExtremityIcons._LimitedCurvedButton.ShortHelp    = "Curved shape";
_MoreContainer.LeftExtremity._ExtremitiesFrame._ExtremityIcons._MaxiButton.ShortHelp             = "Maximum bend";

_MoreContainer.LeftExtremity._ExtremitiesFrame._StandardButton.Title     = "Standard";
_MoreContainer.LeftExtremity._ExtremitiesFrame._StandardButton.ShortHelp = "Reset standard left extremity type";
_MoreContainer.LeftExtremity._ExtremitiesFrame._StandardButton.LongHelp  = "Set the left extremity type to the Sheet Metal part standard type";
_MoreContainer.LeftExtremity._ExtremitiesFrame._StandardButton.Help      = "Reset standard left extremity type";

_MoreContainer.LeftExtremity._ExtremitiesFrame._LengthFrame.LongHelp = "Relief lengths";
_MoreContainer.LeftExtremity._ExtremitiesFrame._LengthFrame.Help     = "Relief lengths";
_MoreContainer.LeftExtremity._ExtremitiesFrame._LengthFrame._L1Label.Title    = "L1 : ";
_MoreContainer.LeftExtremity._ExtremitiesFrame._LengthFrame._L1Label.LongHelp = "Relief first length";
_MoreContainer.LeftExtremity._ExtremitiesFrame._LengthFrame._L2Label.Title    = "L2 : ";
_MoreContainer.LeftExtremity._ExtremitiesFrame._LengthFrame._L2Label.LongHelp = "Relief second length";

// Right Extremity...
_MoreContainer.RightExtremity.Title    = "Right Extremity";
_MoreContainer.RightExtremity.LongHelp = "Chooses the default type of right extremity";

_MoreContainer.RightExtremity._ExtremitiesFrame._ExtremityIcons.ShortHelp = "Select the extremity type";
_MoreContainer.RightExtremity._ExtremitiesFrame._ExtremityIcons.LongHelp  = "Choose the type of extremities";
_MoreContainer.RightExtremity._ExtremitiesFrame._ExtremityIcons.Help      = "Select the extremity type";
_MoreContainer.RightExtremity._ExtremitiesFrame._ExtremityIcons._MiniNoReliefButton.ShortHelp     = "Mini with no relief";
_MoreContainer.RightExtremity._ExtremitiesFrame._ExtremityIcons._MiniSquareReliefButton.ShortHelp = "Mini with square relief";
_MoreContainer.RightExtremity._ExtremitiesFrame._ExtremityIcons._MiniRoundReliefButton.ShortHelp  = "Mini with round relief";
_MoreContainer.RightExtremity._ExtremitiesFrame._ExtremityIcons._LimitedLinearButton.ShortHelp    = "Linear shape";
_MoreContainer.RightExtremity._ExtremitiesFrame._ExtremityIcons._LimitedCurvedButton.ShortHelp    = "Curved shape";
_MoreContainer.RightExtremity._ExtremitiesFrame._ExtremityIcons._MaxiButton.ShortHelp             = "Maximum bend";

_MoreContainer.RightExtremity._ExtremitiesFrame._StandardButton.Title     = "Standard";
_MoreContainer.RightExtremity._ExtremitiesFrame._StandardButton.ShortHelp = "Reset standard right extremity type";
_MoreContainer.RightExtremity._ExtremitiesFrame._StandardButton.LongHelp  = "Set the right extremity type to the Sheet Metal part standard type";
_MoreContainer.RightExtremity._ExtremitiesFrame._StandardButton.Help      = "Reset standard right extremity type";

_MoreContainer.RightExtremity._ExtremitiesFrame._LengthFrame.LongHelp = "Relief lengths";
_MoreContainer.RightExtremity._ExtremitiesFrame._LengthFrame.Help     = "Relief lengths";
_MoreContainer.RightExtremity._ExtremitiesFrame._LengthFrame._L1Label.Title    = "L1 : ";
_MoreContainer.RightExtremity._ExtremitiesFrame._LengthFrame._L1Label.LongHelp = "Relief first length";
_MoreContainer.RightExtremity._ExtremitiesFrame._LengthFrame._L2Label.Title    = "L2 : ";
_MoreContainer.RightExtremity._ExtremitiesFrame._LengthFrame._L2Label.LongHelp = "Relief second length";

_MoreContainer._BendCornerRelievesTabPage.Title    = "Bend Corner Relief";
_MoreContainer._BendCornerRelievesTabPage.LongHelp = "Chooses the default type of bend extremities";
_MoreContainer._BendCornerRelievesTabPage.Help     = "Corner relief characteristics";

_MoreContainer._BendCornerRelievesTabPage._BendCornerRelievesFrame.Title = "Corner Relief";
_MoreContainer._BendCornerRelievesTabPage._BendCornerRelievesFrame.LongHelp = "Chooses the default type of corner relief";
_MoreContainer._BendCornerRelievesTabPage._BendCornerRelievesFrame.Help     = "Corner relief characteristics";

_MoreContainer._BendCornerRelievesTabPage._BendCornerRelievesFrame._CornerReliefCheckButton.Title     = "Corner relief";
_MoreContainer._BendCornerRelievesTabPage._BendCornerRelievesFrame._CornerReliefCheckButton.LongHelp  = "Activation of the creation of corner relief";
_MoreContainer._BendCornerRelievesTabPage._BendCornerRelievesFrame._CornerReliefCheckButton.ShortHelp = "Creation of corner relief";
_MoreContainer._BendCornerRelievesTabPage._BendCornerRelievesFrame._CornerReliefCheckButton.Help      = "Activation of the creation of corner relief";

_MoreContainer._BendCornerRelievesTabPage._BendCornerRelievesFrame._CornerReliefTypeFrame._CornerRelievesIconBox._TriangularPushButton.Title  = "Triangular Corner Relief";
_MoreContainer._BendCornerRelievesTabPage._BendCornerRelievesFrame._CornerReliefTypeFrame._CornerRelievesIconBox._CircularPushButton.Title    = "Circular Corner Relief";
_MoreContainer._BendCornerRelievesTabPage._BendCornerRelievesFrame._CornerReliefTypeFrame._CornerRelievesIconBox._RectangularPushButton.Title = "Rectangular Corner Relief";

_MoreContainer._BendCornerRelievesTabPage._BendCornerRelievesFrame._CornerReliefTypeFrame._StandardCornerReliefButton.Title     = "Standard";
_MoreContainer._BendCornerRelievesTabPage._BendCornerRelievesFrame._CornerReliefTypeFrame._StandardCornerReliefButton.ShortHelp = "Reset standard corner relief type";
_MoreContainer._BendCornerRelievesTabPage._BendCornerRelievesFrame._CornerReliefTypeFrame._StandardCornerReliefButton.LongHelp  = "Set the corner relief type to the Sheet Metal part standard type";
_MoreContainer._BendCornerRelievesTabPage._BendCornerRelievesFrame._CornerReliefTypeFrame._StandardCornerReliefButton.Help      = "Reset standard corner relief type";

_MoreContainer._BendCornerRelievesTabPage._BendCornerRelievesFrame._CornerReliefTypeFrame._CornerReliefRadiusFrame._CornerReliefRadiusLabel.Title    = "Radius : ";
_MoreContainer._BendCornerRelievesTabPage._BendCornerRelievesFrame._CornerReliefTypeFrame._CornerReliefRadiusFrame._CornerReliefRadiusLabel.LongHelp = "Radius of the circular corner relief";


_MoreContainer._BendAllowanceTabPage.Title = "Bend Allowance";
_MoreContainer._BendAllowanceTabPage.LongHelp = "Enter the characteristics of bend allowance";
_MoreContainer._BendAllowanceTabPage.Help     = "Bend Allowance";

_MoreContainer._BendAllowanceTabPage._KFactorFrame._KFactorLabel.Title    = "K Factor :";
_MoreContainer._BendAllowanceTabPage._KFactorFrame._KFactorLabel.LongHelp = "K Factor";

_MoreContainer._BendAllowanceTabPage._DINNormaPushButton.Title     = "DIN Norma";
_MoreContainer._BendAllowanceTabPage._DINNormaPushButton.ShortHelp = "DIN Norma";
_MoreContainer._BendAllowanceTabPage._DINNormaPushButton.LongHelp  = "The K Factor is calculated using the DIN Norma";

Left="Left";
Right="Right";
