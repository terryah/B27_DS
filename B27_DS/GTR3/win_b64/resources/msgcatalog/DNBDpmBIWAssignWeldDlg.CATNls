// COPYRIGHT DASSAULT SYSTEMES Delmia 2000 
//=============================================================================
//
// DNBDpmBIWAssignWeldDlg
//
//=============================================================================
// Usages Notes:
//
//=============================================================================
// Nov. 2000  Creation                                                      AWN
// Apr. 2001  Modifications													                        CTA
// May  2002  Mod                                                           ssz
// Aug. 2002  Mod - Replace "Kind" by "Process Type" in Weld-info display   ssz
// Oct. 2002  Added CheckShowHideWeldId check button                        ssz
//=============================================================================


Title = "Fastener Assignment";

//CTA 04/20/01 : addition of the labels for the buttons
//Buttons titles
OKTitleId = "Fastener Info";
CancelTitleId = "Close";

//ErrorNoActivity.Message = "No fastening activity";
//ErrorNoWelds.Message = "No welds";
//ErrorNoProduct.Message = "No product";

ErrorNoWeld                    = "No valid fasteners available for assignment.";
ErrorNoWeldAssigned            = "No fasteners assigned.";
ErrorAllFilteredWeldsAssigned  = "All fasteners in filtered list already assigned.";
ErrorNoWeldForActivity.Message = "No qualified fasteners for selected activity.";

// for the first tab page


TabContainerId.TabPageAssignmentId.Title = "Assignment";
TabContainerId.TabPageAccountabilityId.Title = "Accountability";

//TabContainerId.TabPageAssignmentId.ShortHelp= "Tab pages for assignment management.";
//TabContainerId.TabPageAssignmentId.LongHelp = "Tab pages for assignment management.";
//TabContainerId.TabPageAssignmentId.HelpTabContainerId.TabPageAssignmentId.TabContainerId.TabPageAssignmentId. = "Tab pages for assignment management.";


TabContainerId.TabPageAssignmentId.SelectListLeftId.Title= "List of fasteners available for assignment.";
TabContainerId.TabPageAssignmentId.SelectListLeftId.ShortHelp= "Display the list of available fasteners for assignment";
TabContainerId.TabPageAssignmentId.SelectListLeftId.LongHelp = "Display the list of available fasteners for assignment under the current selected product";
TabContainerId.TabPageAssignmentId.SelectListLeftId.Help = "Display the list of available fasteners for assignment under the current selected product";

//CTA : 04/24/01 Modify the display; get rid of 'to assign' as the direct assignment is mandatory
// new
TabContainerId.TabPageAssignmentId.SelectListRightId.Title= "List of fasteners assigned (*).";
TabContainerId.TabPageAssignmentId.SelectListRightId.ShortHelp= "Display the list of fasteners assigned to the selected activity";
TabContainerId.TabPageAssignmentId.SelectListRightId.LongHelp = "Display the list of fasteners assigned to the selected activity";
TabContainerId.TabPageAssignmentId.SelectListRightId.Help = "Display the list of fasteners to assign to the current activity";
// old
//TabContainerId.TabPageAssignmentId.SelectListRightId.Title= "List of fasteners assigned(*) / selected for assignment.";
//TabContainerId.TabPageAssignmentId.SelectListRightId.ShortHelp= "Display the list of fasteners to assign";
//TabContainerId.TabPageAssignmentId.SelectListRightId.LongHelp = "Display the list of fasteners to assign to the current activity";
//TabContainerId.TabPageAssignmentId.SelectListRightId.Help = "Display the list of fasteners assigned to the selected activity";


TabContainerId.TabPageAssignmentId.LabelSelectListLeftId.Title= "Fasteners available for assignment:";
//TabContainerId.TabPageAssignmentId.LabelSelectListLeftId.Title= "List of fasteners available for assignment.";
TabContainerId.TabPageAssignmentId.LabelSelectListLeftId.ShortHelp= "Display the list of available fasteners for assignment";
TabContainerId.TabPageAssignmentId.LabelSelectListLeftId.LongHelp = "Display the list of available fasteners for assignment under the current selected product";
TabContainerId.TabPageAssignmentId.LabelSelectListLeftId.Help = "Display the list of available fasteners for assignment under the current selected product";

// new
TabContainerId.TabPageAssignmentId.LabelSelectListRightId.Title= "Fasteners assigned to selected activity :";
TabContainerId.TabPageAssignmentId.LabelSelectListRightId.ShortHelp= "Display the list of fasteners assigned to selected activity.";
TabContainerId.TabPageAssignmentId.LabelSelectListRightId.LongHelp = "Display the list of fasteners assigned to selected activity.";
TabContainerId.TabPageAssignmentId.LabelSelectListRightId.Help = "Display the list of fasteners assigned to selected activity.";
// old
//TabContainerId.TabPageAssignmentId.LabelSelectListRightId.Title= "List of fasteners to assign.";
//TabContainerId.TabPageAssignmentId.LabelSelectListRightId.ShortHelp= "Display the list of fasteners to assign";
//TabContainerId.TabPageAssignmentId.LabelSelectListRightId.LongHelp = "Display the list of fasteners to assign to the current activity";
//TabContainerId.TabPageAssignmentId.LabelSelectListRightId.Help = "Display the list of fasteners to assign to the current activity";


TabContainerId.TabPageAssignmentId.LabelComboDocumentLeftId.Title= "From document : ";
TabContainerId.TabPageAssignmentId.LabelComboDocumentLeftId.ShortHelp= "Display the list of available documents.";
TabContainerId.TabPageAssignmentId.LabelComboDocumentLeftId.LongHelp = "Display the list of available documents in the session.";
TabContainerId.TabPageAssignmentId.LabelComboDocumentLeftId.Help = "Display the list of available documents in the session.";

TabContainerId.TabPageAssignmentId.LabelComboDocumentRightId.Title= "From document : ";
TabContainerId.TabPageAssignmentId.LabelComboDocumentRightId.ShortHelp= "Display the list of available documents.";
TabContainerId.TabPageAssignmentId.LabelComboDocumentRightId.LongHelp = "Display the list of available documents in the session.";
TabContainerId.TabPageAssignmentId.LabelComboDocumentRightId.Help = "Display the list of available documents in the session.";


TabContainerId.TabPageAssignmentId.LabelComboProductId.Title= "List of Products with Fasteners: ";
TabContainerId.TabPageAssignmentId.LabelComboProductId.ShortHelp= "Display the list of available products.";
TabContainerId.TabPageAssignmentId.LabelComboProductId.LongHelp = "Display the list of available products in the document.";
TabContainerId.TabPageAssignmentId.LabelComboProductId.Help = "Display the list of available products in the document.";

TabContainerId.TabPageAssignmentId.LabelComboActivityId.Title= "List of activities : ";
TabContainerId.TabPageAssignmentId.LabelComboActivityId.ShortHelp= "Display the list of available Activities.";
TabContainerId.TabPageAssignmentId.LabelComboActivityId.LongHelp = "Display the list of available Activities in the document.";
TabContainerId.TabPageAssignmentId.LabelComboActivityId.Help = "Display the list of available Activities in the document.";


TabContainerId.TabPageAssignmentId.LabelButtonAssignId.Title= "Assign";
TabContainerId.TabPageAssignmentId.LabelButtonAssignId.ShortHelp= "Assign fasteners to activity.";
TabContainerId.TabPageAssignmentId.LabelButtonAssignId.LongHelp = "Assign selected fasteners to selected activity.";
TabContainerId.TabPageAssignmentId.LabelButtonAssignId.Help = "Assign selected fasteners to selected activity.";

TabContainerId.TabPageAssignmentId.LabelButtonUnassignId.Title= "Unassign";
TabContainerId.TabPageAssignmentId.LabelButtonUnassignId.ShortHelp= "Unassign fasteners from activity.";
TabContainerId.TabPageAssignmentId.LabelButtonUnassignId.LongHelp = "Unassign selected fasteners from selected activity.";
TabContainerId.TabPageAssignmentId.LabelButtonUnassignId.Help = "Unassign selected fasteners from selected activity.";

TabContainerId.TabPageAssignmentId.PushButtonAssignId.Title= "Assign";
TabContainerId.TabPageAssignmentId.PushButtonAssignId.ShortHelp= "Assign fasteners to activity.";
TabContainerId.TabPageAssignmentId.PushButtonAssignId.LongHelp = "Assign selected fasteners to selected activity.";
TabContainerId.TabPageAssignmentId.PushButtonAssignId.Help = "Assign selected fasteners to selected activity.";

TabContainerId.TabPageAssignmentId.PushButtonUnassignId.Title= "Unassign";
TabContainerId.TabPageAssignmentId.PushButtonUnassignId.ShortHelp= "Unassign fasteners from activity.";
TabContainerId.TabPageAssignmentId.PushButtonUnassignId.LongHelp = "Unassign selected fasteners from selected activity.";
TabContainerId.TabPageAssignmentId.PushButtonUnassignId.Help = "Unassign selected fasteners from selected activity.";

//CTA 04/20/01 Direct Assignment is henceforth mandatory
//TabContainerId.TabPageAssignmentId.CheckDirectActionId.Title= "Direct assignment/unassignment";
//TabContainerId.TabPageAssignmentId.CheckDirectActionId.ShortHelp= "Direct assignment/unassignment.";
//TabContainerId.TabPageAssignmentId.CheckDirectActionId.LongHelp = "Activate/Deactivate direct assignment/unassignment.";
//TabContainerId.TabPageAssignmentId.CheckDirectActionId.Help = "Activate/Deactivate direct assignment/unassignment.";


TabContainerId.TabPageAssignmentId.CheckDuplicateAssignmentId.Title= "Allow Duplicate Fastener Assignment";
TabContainerId.TabPageAssignmentId.CheckDuplicateAssignmentId.ShortHelp= "Activate/Deactivate fasteners multi assignment.";
TabContainerId.TabPageAssignmentId.CheckDuplicateAssignmentId.LongHelp = "Activate/Deactivate fasteners multi assignment.";
TabContainerId.TabPageAssignmentId.CheckDuplicateAssignmentId.Help = "Activate/Deactivate fasteners multi assignment.";

TabContainerId.TabPageAssignmentId.CheckShowHideWeldIdLeftList.Title= "Show IDs of fasteners in left list";
TabContainerId.TabPageAssignmentId.CheckShowHideWeldIdRightList.Title= "Show IDs of fasteners in right list";

// XYO:  merge Assign and Assign with Constraint
TabContainerId.TabPageAssignmentId.checkApplyPartLoadConstraintId.Title= "Apply Part Loading Constraints";
TabContainerId.TabPageAssignmentId.checkApplyPartLoadConstraintId.ShortHelp= "Activate/Deactivate Part Loading Constraints.";
TabContainerId.TabPageAssignmentId.checkApplyPartLoadConstraintId.LongHelp = "Activate/Deactivate Part Loading Constraints.";
TabContainerId.TabPageAssignmentId.checkApplyPartLoadConstraintId.Help = "Activate/Deactivate Part Loading Constraints.";


TabContainerId.TabPageAssignmentId.CheckAllProductId.Title= "Select all products";
TabContainerId.TabPageAssignmentId.CheckAllProductId.ShortHelp= "Select all products.";
TabContainerId.TabPageAssignmentId.CheckAllProductId.LongHelp = "Select all products.";
TabContainerId.TabPageAssignmentId.CheckAllProductId.Help = "Select all products.";


TabContainerId.TabPageAssignmentId.LabelLegendId.Title= "Legend : (*) : fastener is assigned";
TabContainerId.TabPageAssignmentId.LabelLegendId.ShortHelp= "Legend : (*) : fastener is assigned.";
TabContainerId.TabPageAssignmentId.LabelLegendId.LongHelp = "Legend : (*) : fastener is assigned.";
TabContainerId.TabPageAssignmentId.LabelLegendId.Help = "Legend : (*) : fastener is assigned.";

// CTA 04/26/01 : get rid of the parenthesis on the s (fasteners instead of fastener(s))
TabContainerId.TabPageAssignmentId.LabelNumberId.Title= "Number of fasteners available:";
TabContainerId.TabPageAssignmentId.LabelNumberId.ShortHelp= "Number of fasteners available for assignment.";
TabContainerId.TabPageAssignmentId.LabelNumberId.LongHelp = "Number of fasteners available for assignment.";
TabContainerId.TabPageAssignmentId.LabelNumberId.Help = "Number of fasteners available for assignment.";

// CTA 04/24/01 : add a new counter on assigned welds 
TabContainerId.TabPageAssignmentId.LabelAssignNumberId.Title= "Number of fasteners assigned :";
TabContainerId.TabPageAssignmentId.LabelAssignNumberId.ShortHelp= "Number of fasteners assigned to selected activity.";
TabContainerId.TabPageAssignmentId.LabelAssignNumberId.LongHelp = "Number of fasteners assigned to selected activity..";
TabContainerId.TabPageAssignmentId.LabelAssignNumberId.Help = "Number of fasteners assigned to selected activity..";

//// for the second tab page

//TabContainerId.TabPageAssignmentId,Title = "Assignment";
//TabContainerId.ShortHelp= "Tab pages for assignment management.";
//TabContainerId.LongHelp = "Tab pages for assignment management.";
//TabContainerId.Help//// = "Tab pages for assignment management.";


TabContainerId.TabPageAccountabilityId.FrameAccountabilityId.RadioOneProductId.Title= "Selected product";
TabContainerId.TabPageAccountabilityId.FrameAccountabilityId.RadioOneProductId.ShortHelp= "Show fasteners for the selected product only.";
TabContainerId.TabPageAccountabilityId.FrameAccountabilityId.RadioOneProductId.LongHelp = "Show fasteners for the selected product only.";
TabContainerId.TabPageAccountabilityId.FrameAccountabilityId.RadioOneProductId.Help = "Show fasteners for the selected product only.";

TabContainerId.TabPageAccountabilityId.FrameAccountabilityId.RadioAllProductId.Title= "All product(s)";
TabContainerId.TabPageAccountabilityId.FrameAccountabilityId.RadioAllProductId.ShortHelp= "Show fasteners for all product(s).";
TabContainerId.TabPageAccountabilityId.FrameAccountabilityId.RadioAllProductId.LongHelp = "Show fasteners for all product(s).";
TabContainerId.TabPageAccountabilityId.FrameAccountabilityId.RadioAllProductId.Help = "Show fasteners for all product(s).";

MultiListWeldAccountId.ColumnTitle0 = "Index";
MultiListWeldAccountId.ColumnTitle1 = "Fastener ID";
MultiListWeldAccountId.ColumnTitle2 = "Process Type";
MultiListWeldAccountId.ColumnTitle3 = "Assigned Activity(ies)";

ComboOptionId1 = "All fasteners";
ComboOptionId2 = "Assigned fasteners";
ComboOptionId3 = "Unassigned fasteners";
ComboOptionId4 = "Multi-assigned fasteners";

TabContainerId.TabPageAccountabilityId.FrameAccountabilityId.LabelOptionId.Title= "Select a filter";
TabContainerId.TabPageAccountabilityId.FrameAccountabilityId.LabelOptionId.ShortHelp= "Click on the combo to select a filter.";
TabContainerId.TabPageAccountabilityId.FrameAccountabilityId.LabelOptionId.LongHelp = "Click on the combo to select a filter.";
TabContainerId.TabPageAccountabilityId.FrameAccountabilityId.LabelOptionId.Help = "Click on the combo to select a filter.";

TabContainerId.TabPageAccountabilityId.FrameAccountabilityId.LabelWeldsAvailableId.Title= "Fasteners available";
TabContainerId.TabPageAccountabilityId.FrameAccountabilityId.LabelWeldsAvailableId.ShortHelp= "Number of Fasteners available.";
TabContainerId.TabPageAccountabilityId.FrameAccountabilityId.LabelWeldsAvailableId.LongHelp = "Number of Fasteners available.";
TabContainerId.TabPageAccountabilityId.FrameAccountabilityId.LabelWeldsAvailableId.Help = "Number of Fasteners available.";

TabContainerId.TabPageAccountabilityId.FrameAccountabilityId.LabelAssignedId.Title       = "Number of fasteners Assigned: ";
TabContainerId.TabPageAccountabilityId.FrameAccountabilityId.LabelNotAssignedId.Title    = "Number of fasteners not assigned: ";
TabContainerId.TabPageAccountabilityId.FrameAccountabilityId.LabelMultiAssignedId.Title  = "Number of fasteners multi assigned: ";

LabelAssignedId       = "Fasteners Assigned: ";
LabelNotAssignedId    = "Fasteners not assigned: ";
LabelMultiAssignedId  = "Fasteners multi assigned: ";


NoneTitleId = "None";
AllProducts = "All Products";
//AssignTag   = "(*)";


TitleNoDoubleAssignmentId    = "WARNING";
NoDoubleAssignmentStartId    = "No duplicate Assignment Allowed !!\n";
NoDoubleAssignmentSingularId = "The following fastener  :\n";
NoDoubleAssignmentPluralId   = "The following fasteners :\n";
NoDoubleAssignmentIsId       = "is ";
NoDoubleAssignmentAreId      = "are ";
NoDoubleAssignmentEndId      = "already assigned to the selected activity.";
