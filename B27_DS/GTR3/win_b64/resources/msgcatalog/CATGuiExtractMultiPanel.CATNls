//--------------------------------------------
// Resource file for CATGSMUIExtractMultiPanel class
// En_US
//--------------------------------------------

PanelTitre="Multiple Extract Definition";
ElementsFrame.ElementsSelector.Title="List of the selected elements";
ElementsFrame.ElementsSelector.LongHelp="List used to manage the element to extract";

ExtractCombo0="Point continuity";
ExtractCombo1="Tangent continuity";
ExtractCombo2="Curvature continuity";
ExtractCombo3="No propagation";
ComboTitre="Propagation type:";


// number of the selected elements
NumberColumnTitle="No.";

Element3DLabel="Element";
ElementsColumn="Elements";
PropagColumn="Propagation";
PointThresholdColumn="Distance Threshold";
TangentThresholdColumn="Angular Threshold";
CurvatureThresholdColumn="Curvature Threshold";


ElementsFrame.Type.LongHelp="Defines the propagation type.
You can create only the selected element (BRep Object),
or propagate the creation according to tangency conditions [Tangent continuity]
or point continuity (the extracted element will not present any hole).";

ElementsFrame.CmbList.LongHelp="Defines the propagation type.
You can create only the selected element (BRep Object),
or to propagate the creation according to tangency conditions [Tangent continuity]
or point continuity (the extracted element will not present any hole).";

ElementsFrame.ComplementaryMode.Title="Complementary mode";
ElementsFrame.ComplementaryMode.LongHelp="This option selects the elements that were not previously
selected, while deselecting the elements that were explicitly selected.";

ElementsFrame.Federation.Title="Federation";
ElementsFrame.Federation.LongHelp="Check this button to generate groups of elements belonging
to the resulting extracted element that will be detected together
with the pointer when selecting one of its sub-elements.";

ElementsFrame.BanDomainMerging.Title="Ban domain merging";
ElementsFrame.BanDomainMerging.LongHelp="Check this button for the Multiple Extract feature not to
merge connex domains that belong to the same feature.";

ElementsFrame.FraTolerance.PointThreshold.Title="Distance Threshold";
ElementsFrame.FraTolerance.FraTolerancePtCke.EnglobingFrame.IntermediateFrame.Spinner.Title="Distance Threshold";
ElementsFrame.FraTolerance.PointThreshold.LongHelp="Specifies the point discontinuity under which the extract is generated.";

ElementsFrame.FraTolerance.TangentThreshold.Title="Angular Threshold";
ElementsFrame.FraTolerance.FraToleranceTgtCke.EnglobingFrame.IntermediateFrame.Spinner.Title="Angular Threshold";
ElementsFrame.FraTolerance.TangentThreshold.LongHelp="Specifies the tangent discontinuity under which the extract is generated.";

ElementsFrame.FraTolerance.CurvatureThreshold.Title="Curvature Threshold";
ElementsFrame.FraTolerance.FraToleranceCurCke.EnglobingFrame.IntermediateFrame.Spinner.Title="Curvature Threshold";
ElementsFrame.FraTolerance.CurvatureThreshold.LongHelp="Specifies the curvature discontinuity above which the extract is generated.
r=1 corresponds to a continuous curvature.
A great discontinuity will require a low r to be taken into account.";

ElementsFrame.SeparFrame.LabelSepar.Title="Element(s) to extract";
ElementsFrame.LabelSeparType.Title="Type";

ElementsFrame.ButtonFrame.ReplaceButton.Title="Replace";
ElementsFrame.ButtonFrame.ReplaceButton.LongHelp="Allows you to replace the selected element by another element.";
ElementsFrame.ButtonFrame.RemoveButton.LongHelp="Allows you to remove the selected element from the list.";
ElementsFrame.ButtonFrame.RemoveButton.Title="Remove";
ElementsFrame.ButtonFrame.AddButton.Title="Add";
ElementsFrame.ButtonFrame.AddButton.LongHelp="Allows you to add a new element to the list.";


DeleteLineTitre.Title="Delete sub element entry line";
DeleteLineTitre.LongHelp="Used to suppress the extraction of one of the sub elements selected by the user";


ObjetSel="Selected object";
ObjetPropag="Propagation type";
ObjetPropagType1="Point propagation";

ObjetSel.LongHelp="Selected object";
ObjetPropag.LongHelp="Propagation type";
ObjetPropagType1.LongHelp="Type selected";

CATGuiExtractMultiReRoute="Brep Deleted. Please, replace the erroneous element";
CATGuiExtractMultiMauvaisePart="You cannot select an element from another part than the current one\n with this Propagation type. Change Propagation type to No propagation";

MoreInfo="Show parameters >>";
LessInfo="Hide parameters <<";
ElementsFrame.MoreInfoBt.ShortHelp="Hide or show extended information";

//CATGuiExtractMultiBesoinSupport="You need a support for this kind of propagation on this internal edge\n please select a support linked with this internal edge";
CATGuiExtractMultiBesoinSupport="You need a support for this kind of propagation on this internal edge, please select a support";

CATGuiExtractMultiDimension2="Incorrect dimension of the selected object. Expected dimension: 2   and selected: ";
CATGuiExtractMultiNomSupport="Support";

SupportTitre="Support :";

ElementsFrame.FraSupport.LongHelp="Defines the support.
You must select a support in order to compute
the propagation of this edge (internal one) 
because there is ambiguity (more than one support for this edge).";

ElementsFrame.Support.LongHelp="Defines the support.
You must select a support in order to compute
the propagation of this edge (internal one) 
because there is ambiguity (more than one support for this edge).";
