DialogBoxTitle                                       = "Adjust";

MultiListVectors.ColumnTitle1                        = " ";
MultiListVectors.ColumnTitle2                        = "Vector";
MultiListVectors.ColumnTitle3                        = "Start";
MultiListVectors.ColumnTitle4                        = "End";
MultiListVectors.LongHelp                            = "Lists the defined vectors with their start points and end points. 
Depending on the number of defined vectors, the following 
transformations are possible:
1 Vector: Translation
2 Vectors: Translation, Rotation 
3 Vectors: Translation, Rotation, Compensation
In case of more than 3 vectors the compensation works on the mean plane.";
MultiListVectors.ShortHelp = "List of defined vectors and their start and end points";

FrameSelection.LongHelp                              = "Options for selection";
FrameSelection.LabelElements.Title                   = "Elements: ";
FrameSelection.LabelElements.LongHelp                = "Select the elements to be adjusted.
Input elements can be points, curves, or surfaces.";
FrameSelection.LabelElements.ShortHelp               = "Elements to be adjusted";
FrameSelection.LabelPoint.Title                      = "Point: ";
FrameSelection.LabelPoint.LongHelp                   = "Select points to define the vector.
The points are listed in the vector table.";
FrameSelection.LabelPoint.ShortHelp                  = "Select points to define the vector.";

FrameOthers.FrameOptions.Title                       = "Options";
FrameOthers.FrameOptions.LongHelp                    = "The vectors have to be defined in the order Translation, Rotation, Compensation.
- Translation - 
  The elements are translated.
- Rotation -
  The rotation results from the Translation and Rotation vectors.
- Compensation -
  Defines together with Translation and Rotation the start and end planes for the transformation.
- Scale -
  Scaling of the result.";

FrameOthers.FrameOptions.CheckTranslation.Title      = "Translation";
FrameOthers.FrameOptions.CheckTranslation.LongHelp   = "This option requires one vector defined by two points.
Translation must always be selected.
Without translation, no transformation is possible.
If only Translation is selected, the element is moved and not rotated.
The element is moved from the start point to the end point of the Translation vector.";
FrameOthers.FrameOptions.CheckTranslation.ShortHelp  = "First vector - defines the translation.";

FrameOthers.FrameOptions.CheckScale.Title            = "Scale";
FrameOthers.FrameOptions.CheckScale.LongHelp         = "Only available if Translation and Rotation are switched on.
This option needs no additional vector.
The origin of scale is the end point of the vector Translation.
The scale factor results the from quotient of the difference between
the end points and start points of the vectors Translation and Rotation.";
FrameOthers.FrameOptions.CheckScale.ShortHelp        = "Scaling of the result.";

FrameOthers.FrameOptions.CheckRotation.Title         = "Rotation";
FrameOthers.FrameOptions.CheckRotation.LongHelp      = "A second vector is required.
The vector Translation defines in combination with the vector 
Rotation the origin and one orientation direction of the element.
If Scale is switched on, the quotient of the distance of the 
end points and the distance of the start points of both vectors 
is the scale factor.";
FrameOthers.FrameOptions.CheckRotation.ShortHelp     = "Second vector - defines the rotation.";

FrameOthers.FrameOptions.CheckCompensation.Title     = "Compensation";
FrameOthers.FrameOptions.CheckCompensation.LongHelp  = "Only available if Translation and Rotation are switched on.
A third vector is required which defines with the two first vectors two planes.
Start and end plane are defined by the start and end points of the three vectors.
In addition to the translation and rotation, the elements
are transformed from the start plane into the end plane.";
FrameOthers.FrameOptions.CheckCompensation.ShortHelp = "Third vector - defines the planes.";


//???????????????????????????????????????????????????
TranslatationVector                                  = "Translation";
RotationVector                                       = "Rotation";
