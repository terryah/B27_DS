// COPYRIGHT DASSAULT SYSTEMES 1999
//=============================================================================
//
// Resources File for NLS purpose related to TPS types
//
//=============================================================================
// Usage notes:
//
//
//=============================================================================
// Sep. 1999  Creation                                                  G. Bapt
//=============================================================================

//----------------------------------------------------------------------- Datum
CATTPS.75a60f95400611d3a92c006094b99c4b = "I_TPSStfDatum";       //Simple datum
CATTPS.75a60f96400611d3a92c006094b99c4b = "I_TPSStfDatumTarget"; //Datum target
CATTPS.75a60f97400611d3a92c006094b99c4b = "I_TPSStfDatum";       //Datum complexe
CATTPS.335352e6432a11d3a92d006094b99c4b = "I_TPSStfReferenceFrame"; //Datum reference frame

//------------------------------------------------------------------------ Form
CATTPS.75a60f81400611d3a92c006094b99c4b ="I_TPSStfStraightness";//Straightness
CATTPS.7c122933e8b011d586080010a4899d8f ="I_TPSStfAxis";        //Axis Straightness
CATTPS.75a60f82400611d3a92c006094b99c4b ="I_TPSStfFlatness";    //Flatness
CATTPS.75a60f83400611d3a92c006094b99c4b ="I_TPSStfCircularity"; //Circularity
CATTPS.75a60f84400611d3a92c006094b99c4b ="I_TPSStfCylindricity";//Cylindricity
CATTPS.75a60f85400611d3a92c006094b99c4b ="I_TPSStfProfline";    //Profile tolerance of any line
CATTPS.75a60f86400611d3a92c006094b99c4b ="I_TPSStfProfsurf";    //Profile tolerance of any surface
CATTPS.75a60f87400611d3a92c006094b99c4b ="I_TPSStfPosition";    //Pattern localization

//------------------------------------------------------------------------ Size
CATTPS.75a60f90400611d3a92c006094b99c4b = "I_TPSStfLinear";					//Linear
CATTPS.75a60f90400611d3a92c006094b99c4b.Info = "I_TPSStfLinearInfo";		//Linear Info
CATTPS.75a60f91400611d3a92c006094b99c4b = "I_TPSStfAngular";				//Angular
CATTPS.75a60f91400611d3a92c006094b99c4b.Info = "I_TPSStfAngularInfo";		//Angular Info
CATTPS.75a60f94400611d3a92c006094b99c4b = "I_TPSStfLinear";					//Chamfer Dim
CATTPS.75a60f94400611d3a92c006094b99c4b.Info = "I_TPSStfLinearInfo";		//Chamfer Dim Info
CATTPS.75a60f99400611d3a92c006094b99c4b = "I_TPSStfBasicDim";				//Basic dimension
CATTPS.75a60f99400611d3a92c006094b99c4b.Info = "I_TPSStfBasicDim";			//Basic dimension Info
CATTPS.75a60f92400611d3a92c006094b99c4b = "I_TPSStfLinear";					//Second Linear
CATTPS.75a60f92400611d3a92c006094b99c4b.Info = "I_TPSStfLinearInfo";		//Second Linear Info
CATTPS.17f49a80ec074a8a875dccac19a5cda9 = "I_TPSStfOrientedLinear";			//Oriented Linear
CATTPS.17f49a80ec074a8a875dccac19a5cda9.Info = "I_TPSStfLinearInfo";		//Oriented Linear Info
CATTPS.e41043ae361041eb81dcfff3ce045be4 = "I_TPSStfOrientedAngular";		//Oriented Amgular
CATTPS.e41043ae361041eb81dcfff3ce045be4.Info = "I_TPSStfAngularInfo";		//Oriented Amgular Info

//-------------------------------------------- Position Orientation and Run-Out
CATTPS.75a60f88400611d3a92c006094b99c4b = "I_TPSStfParallelism";     //Parallelism
CATTPS.75a60f89400611d3a92c006094b99c4b = "I_TPSStfPerpendicularity";//Perpendicularity
CATTPS.75a60f8a400611d3a92c006094b99c4b = "I_TPSStfAngularity";      //Angularity
CATTPS.75a60f8b400611d3a92c006094b99c4b = "I_TPSStfPosition";        //Positional
CATTPS.75a60f8c400611d3a92c006094b99c4b = "I_TPSStfConcentricity";   //Concentricity
CATTPS.75a60f8d400611d3a92c006094b99c4b = "I_TPSStfSymmetry";        //Symmetry
CATTPS.10bdbb717a3611d3a93f006094b99c4b = "I_TPSStfProfline";        //Position linear profile
CATTPS.10bdbb707a3611d3a93f006094b99c4b = "I_TPSStfProfsurf";        //Position surfacic profile
CATTPS.75a60f8e400611d3a92c006094b99c4b = "I_TPSStfTotalRO";         //Total run-out
CATTPS.75a60f8f400611d3a92c006094b99c4b = "I_TPSStfCircularRO";      //Circular run-out

//----------------------------------------------------------------- NonSemantic
CATTPS.f4a2d3943ddc11d3a929006094b99c4b = "I_TPSStfText";           //Text
CATTPS.75a60f98400611d3a92c006094b99c4b = "I_TPSStfFlagNote";       //Flag Note
CATTPS.7011eb2875cd11d59ee400508b13182d = "I_TPSStfNOA";            //NOA
CATTPS.7cecb0fca0ed11d3a94e006094b99c4b = "I_TPSStfDatum";          //Non Semantic Datum
CATTPS.75a60f80400611d3a92c006094b99c4b = "I_TPSStfNonSemanticGDT"; //Non Semantic GDT
CATTPS.a78a75cb3c8811d494b10004ac96129f = "I_TPSStfDatumTarget";    //Non Semantic Datum Target
CATTPS.3dccd85dccb211d394740004ac96129f = "I_TPSWeld";              //Weld
CATTPS.53ce38e693b911d4907c0004ac96df1d = "I_TPSStnDimension";         //Non semantic dimension
CATTPS.ea0e21c47f38456da94cb0c32cd3cead = "I_TPSStfCoordDim";       //Coordinate dimension
//------------------------------------------------------------------ Roughness
CATTPS.75a60f9a400611d3a92c006094b99c4b = "I_TPSStfRoughness";

//------------------------------------------------------------------ Deviation
CATTPS.c17f1520a3ef11d3a951006094b99c4b = "I_TPSDeviation";
CATTPS.f13928129f7611d484fa00008656f176 = "I_TPSDeviationCorrelated";
CATTPS.20237ab2083111d5856e00008656f176 = "I_TPSDistancePointPoint";
CATTPS.b0f4328ec20c11d5ab4900306e0aa2f3 = "I_TPSAnaGeomVar";
CATTPS.dff5b5137d6b4cf38e686e7229de5d96 = "I_TPSGravityAnnotation";
CATTPS.bba0626dd7124a5f80a8743d179af7bb = "I_TPSMorphingVar";

//-------------------------------------------------------------- RestrictedArea
CATTPS.444330305d8f11d6be950002b341dd3a = "I_TPSRestrictedArea";

//-----------------------------------------------------------------------------
// Suive la description des ordres d utilisation
// des ressources des types et supertypes de tolerances
// par la wnd filter (attention les retours chariot ne sont
// pas supporte par les CATString)
//-----------------------------------------------------------------------------

CATTPS.supertype="CATTPS.75a60f7f400611d3a92c006094b99c4b,CATTPS.75a60f7a400611d3a92c006094b99c4b,CATTPS.75a60f7e400611d3a92c006094b99c4b,CATTPS.75a60f7c400611d3a92c006094b99c4b,CATTPS.f4a2d3953ddc11d3a929006094b99c4b,CATTPS.75a60f9a400611d3a92c006094b99c4b,CATTPS.c17f1520a3ef11d3a951006094b99c4b,CATTPS.444330305d8f11d6be950002b341dd3a";

//Rq : Les targets n'apparaissent pas dans la liste des types a filtrer car elles ne peuvent pas
//     etre visualisees seules (toujours avec leur datum simple).                     
CATTPS.type="CATTPS.75a60f95400611d3a92c006094b99c4b,CATTPS.75a60f81400611d3a92c006094b99c4b,CATTPS.75a60f82400611d3a92c006094b99c4b,CATTPS.75a60f83400611d3a92c006094b99c4b,CATTPS.75a60f84400611d3a92c006094b99c4b,CATTPS.75a60f85400611d3a92c006094b99c4b,CATTPS.75a60f86400611d3a92c006094b99c4b,CATTPS.75a60f87400611d3a92c006094b99c4b,CATTPS.75a60f90400611d3a92c006094b99c4b,CATTPS.75a60f91400611d3a92c006094b99c4b,CATTPS.75a60f94400611d3a92c006094b99c4b,CATTPS.75a60f99400611d3a92c006094b99c4b,CATTPS.75a60f88400611d3a92c006094b99c4b,CATTPS.75a60f89400611d3a92c006094b99c4b,CATTPS.75a60f8a400611d3a92c006094b99c4b,CATTPS.75a60f8b400611d3a92c006094b99c4b,CATTPS.75a60f8c400611d3a92c006094b99c4b,CATTPS.75a60f8d400611d3a92c006094b99c4b,CATTPS.10bdbb717a3611d3a93f006094b99c4b,CATTPS.10bdbb707a3611d3a93f006094b99c4b,CATTPS.75a60f8e400611d3a92c006094b99c4b,CATTPS.75a60f8f400611d3a92c006094b99c4b,CATTPS.f4a2d3943ddc11d3a929006094b99c4b,CATTPS.75a60f98400611d3a92c006094b99c4b,CATTPS.7011eb2875cd11d59ee400508b13182d,CATTPS.7cecb0fca0ed11d3a94e006094b99c4b,CATTPS.75a60f80400611d3a92c006094b99c4b,CATTPS.75a60f9a400611d3a92c006094b99c4b,CATTPS.c17f1520a3ef11d3a951006094b99c4b,CATTPS.444330305d8f11d6be950002b341dd3a";

//-------------------------------------------------------- Constructed Geometry
CATTPS.034185c83eff4864836f7a1b018f2c38 = "I_TPSCGPoint";
CATTPS.a5b6149474c149e5872c9e3dcc098437 = "I_TPSCGLine";
CATTPS.186db32a65084a6b9d536bdcddc76b0d = "I_TPSCGPlane";
CATTPS.57e466515e9b4581b640cf912e713703 = "I_TPSCGCircle";
CATTPS.f1183e637bc04152837bc3c53dcecc8c = "I_TPSCGCylinder";
CATTPS.68c19fa5e4404676b4e6c74041f9ec41 = "I_TPSCGCurve";
CATTPS.8bc97d075c414fffbfcc2a451b4e3031 = "I_TPSCGThread";

//-------------------------------------------------------- Capture
CATTPS.c0074552099b11d6be990003479ec69d = "I_TPSCaptureAnnotations";
