//----------------------------------------------
// Resource file for CATGSMUICurveParPanel class
// En_US
//----------------------------------------------
Title="Parallel Curve Definition";
FraElem.LabCurvePar.Title="Curve:";
FraElem.LabSupport.Title="Support:";
FraElem.LabOffset.Title="Offset:";
//IR 193912: Function_036030's development: Geodesic parallel mode should be available in GS1 for SFD or SDD customers,
//provided they have set the right environment variable
FraElem.LabParallelMode.Title="Parallel mode: ";
FraElem.CheckLaw.Title = "Law... ";

FraOffset.Title = "Offset";
FraOffset.LabOffsetLit.Title = "Constant: ";
FraOffset.Value.EnglobingFrame.IntermediateFrame.Spinner.Title="Offset";
FraOffset.LabLaw.Title = "Law: ";
FraOffset.CheckLaw.Title = "Law... ";
FraOffset.LabPassingPoint.Title="Point:";

FraDir.PshDir.Title="Reverse Direction";
FraDir.CheckBothSides.Title="Both Sides";

FraParallelMode.Title="Parameters";
FraParallelMode.LabParallelMode.Title="Parallel mode: ";
ParallelModeEuclidean="Euclidean";
ParallelModeGeodesic="Geodesic";
FraParallelMode.LabParallelCorner.Title="Parallel corner type: ";
ParallelSharp="Sharp";
ParallelRound="Round";

FraParallelMode.LabParallelMode.LongHelp=
"Lists the available curve parallel
modes : euclidean or geodesic.";
FraParallelMode.CmbParallelMode.LongHelp=
"Lists the available curve parallel
modes : euclidean or geodesic.";

FraParallelMode.LabParallelCorner.LongHelp=
"Lists the available curve parallel
corner types : sharp or round.";
FraParallelMode.CmbParallelCorner.LongHelp=
"Lists the available curve parallel
corner types : sharp or round.";

OffsetModeCst="Constant";
OffsetModeLaw="Law";

FraElem.LabCurvePar.LongHelp=
"Specifies the reference curve to be offset.";

FraElem.FraCurvePar.LongHelp=
"Select the reference curve.";

FraElem.LabSupport.LongHelp=
"Specifies the support surface or plane.";

FraElem.FraSupport.LongHelp=
"Select the support.";

FraElem.LabOffset.LongHelp = 
"Constant specifying the offset 
of the parallel curve.";

FraDir.PshDir.LongHelp=
"Displays the offset curve on the other side 
of the reference curve.";

FraDir.CheckBothSides.LongHelp=
"Allows you to keep both sides. 
Two curves parallel will be computed.";

FraOffset.CmbOffsetMode.LongHelp= 
"The offset can be specified by 
a constant value or a law.";

FraOffset.LabOffsetLit.LongHelp = 
"Constant specifying the offset 
of the parallel curve.";

FraOffset.LabLaw.LongHelp = 
"Law specifying the offset 
of the parallel curve.";

FraOffset.LabPassingPoint.LongHelp = 
"Passing Point specifying the offset
of the parallel curve.";

FraOffset.FraPassingPoint.LongHelp=
"Select the point.";
