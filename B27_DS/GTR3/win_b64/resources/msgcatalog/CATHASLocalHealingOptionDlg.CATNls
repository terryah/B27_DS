//----------------------------------------------
// Resource file for CATHASLocalHealingOptionDlg class
// En_US
//----------------------------------------------

Title="Healing Option";

TabContainer.ParametersTabPage.Title="Parameters";

TabContainer.ParametersTabPage.LabContinuityType.Title="Continuity:";
TabContainer.ParametersTabPage.LabContinuityType.LongHelp=
"Lists the avalaible continuity types between
the elements to be healed.";
PointType="Point";
TangentType="Tangent";

TabContainer.ParametersTabPage.LabDevUser.Title="Merging distance:";
TabContainer.ParametersTabPage.FraDevUser.Value.EnglobingFrame.IntermediateFrame.Spinner.Title="Distance";
TabContainer.ParametersTabPage.LabDevUser.LongHelp=
"Defines the maximum distance below which two elements
are considered as only one element.";

TabContainer.ParametersTabPage.LabG0Obj.Title="Distance objective:";
TabContainer.ParametersTabPage.FraG0Obj.Value.EnglobingFrame.IntermediateFrame.Spinner.Title="Distance";
TabContainer.ParametersTabPage.LabG0Obj.LongHelp=
"Sets the maximum gap allowed between two healed elements. 
By default it is set to 0.001 mm, and can be increased to 0.1 mm. ";

TabContainer.ParametersTabPage.LabTangencyAngle.Title="Tangency angle:";
TabContainer.ParametersTabPage.FraTangencyAngle.Value.EnglobingFrame.IntermediateFrame.Spinner.Title="Angle";
TabContainer.ParametersTabPage.LabTangencyAngle.LongHelp=
"Defines the maximum angle below which two elements
are considered continuous in tangency.";

TabContainer.ParametersTabPage.LabG1Obj.Title="Tangency objective:";
TabContainer.ParametersTabPage.FraG1Obj.Value.EnglobingFrame.IntermediateFrame.Spinner.Title="Angle";
TabContainer.ParametersTabPage.LabG1Obj.LongHelp=
"Sets the maximum tangency deviation allowed between healed elements. 
The default value is 0.5 degree, but can range anywhere between 0.1 degree to 2 degrees. ";

TabContainer.ParametersTabPage.LabComputeMode.Title="Computation mode:";
TabContainer.ParametersTabPage.LabComputeMode.LongHelp=
"Specifies the mode of computation for healing : standard or rigorous.";

TabContainer.FixTabPage.Title="Freeze";
TabContainer.FixTabPage.Frame_Freeze.Title="Elements to freeze";
TabContainer.FixTabPage.Frame_Freeze.LongHelp=
"Specifies the edges or faces not to be affected by the healing.";
TabContainer.FixTabPage.Frame_Freeze.FixList.LongHelp=
"Specifies the edges or faces not to be affected by the healing.";

TabContainer.FixTabPage.CheckFacesPlanes.Title="Freeze Plane elements";
TabContainer.FixTabPage.CheckFacesPlanes.LongHelp=
"Specifies whether the plane elements 
should be affected by the healing.";

TabContainer.FixTabPage.CheckCanonics.Title="Freeze Canonic elements";
TabContainer.FixTabPage.CheckCanonics.LongHelp=
"Specifies whether the canonical elements 
should be affected by the healing.";

TabContainer.SharpTabPage.Title="Sharpness";
TabContainer.SharpTabPage.Frame_Sharp.Title="Edges to keep sharp";
TabContainer.SharpTabPage.Frame_Sharp.LongHelp=
"Specifies the edges not to be affected by the healing.";
TabContainer.SharpTabPage.Frame_Sharp.SharpList.LongHelp=
"Specifies the edges not to be affected by the healing.";

TabContainer.SharpTabPage.FraTolSharpness.LabFrame_Sharpness.Title="Sharpness angle:";
TabContainer.SharpTabPage.FraTolSharpness.FraSharpness.Value.EnglobingFrame.IntermediateFrame.Spinner.Title="Angle";
TabContainer.SharpTabPage.FraTolSharpness.LabFrame_Sharpness.LongHelp=
"Defines the maximum angle value below which an angle
is considered to be sharp.";
