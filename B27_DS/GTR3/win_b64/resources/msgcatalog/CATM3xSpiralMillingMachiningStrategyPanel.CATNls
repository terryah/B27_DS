Title = "Machining";

MachiningTolerance = "Machining tolerance";
M3xReverseTP = "Reverse tool path";
MfgDirectionOfCut = "Cutting mode";
MfgGlobalOffsetOnContour = "Offset on contour";
M3xSmLength2 = "Offset on soft boundaries";
M3xProgressDirectionMode = "Helical movement";

M3xReverseTP.LongHelp = "Starts the tool path from the opposite end.";

MachiningTolerance.LongHelp = "Specifies the maximum distance allowed
between the theoretical and computed tool path.";

MfgDirectionOfCut.LongHelp = "Specifies the movement of the tool with respect to the surface to process:
- Climb: The front of the tool (advancing in the machining direction) cuts into the material first.
- Conventional: The back of the tool (advancing in the machining direction) cuts into the material first.
For Concentric in Zig-Zag mode, the first pass occurs in the selected Cutting mode (standard direction),
the second pass is done in the other cutting mode (reverse direction).";

MfgGlobalOffsetOnContour.LongHelp = "Specifies the tool offset with respect to the contour.";
M3xSmLength2.LongHelp = "Specifies the length of the tool engagement in the material for the first path.";

M3xProgressDirectionMode.LongHelp = "Specifies whether the spiral goes from the outside in or from
the inside out.";

AlwaysStayOnBottom = "Always stay on bottom";
AlwaysStayOnBottom.LongHelp = "Forces always the tool to remain in contact with the pocket bottom.";

MfgZigZagMode = "Concentric movement";
MfgZigZagMode.LongHelp = "Specifies how the tool moves in a concentric movement:
- Zig-Zag: The tool moves first in the selected cutting mode, then in the reverse direction.
  This option reduces air cuts.
- One-Way: The tool moves only in the selected cutting mode.";

MfgZigZagRatio = "Reverse pass radial ratio";
MfgZigZagRatio.LongHelp = "Applies to Zig-Zag Concentric movement.
Defines the maximum radial depth of cut used by passes in the reverse direction.
It can differ from that used in the selected cutting mode.";
