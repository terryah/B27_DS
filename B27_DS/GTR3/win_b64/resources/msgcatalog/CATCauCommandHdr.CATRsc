//=============================================================================
//                                     CNEXT - CXRx
//                          COPYRIGHT DASSAULT SYSTEMES 2002 
//-----------------------------------------------------------------------------
// FILENAME    :    CATCauCommandHdr.CATRsc
// LOCATION    :    CATCompAccessUI/CNext/resources/msgcatalog
// AUTHOR      :    ehh 
// DATE        :    May, 2002
//------------------------------------------------------------------------------
// DESCRIPTION :    Resources File for NLS purpose related to C&A
//                  WorkShop
//                  
//------------------------------------------------------------------------------
// COMMENTS    :
//------------------------------------------------------------------------------
//MODIFICATION     user  date      purpose
//   HISTORY       ----  ----      -------
//     01 	       
//                 BCY   4/12/07   Comment out LongHelpID CATCauAddCptsToCatalogHdr	       

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// C&A Commands
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

//------------------------------------------------------------------------------
// Basic Compartment .. Added 12/2006, BCY.
//------------------------------------------------------------------------------
CATCauCommandHdr.CATCauGeomCmdHdr.Icon.Normal="I_Cau_BasicCompartment";
//CATCauCommandHdr.CATCauGeomCmdHdr.LongHelpId= "CATHCDCmdHeader.DefineBoundedZone"; //F1 Documentation

//------------------------------------------------------------------------------
// Complex Compartment a.k.a. Composite Volume .. Added 12/2006, BCY.
//------------------------------------------------------------------------------
CATCauCommandHdr.CATCauCompositeVolumeCmdHdr.Icon.Normal="I_Cau_CompositeVol";
//CATCauCommandHdr.CATCauCompositeVolumeCmdHdr.LongHelpId= "CATHCDCmdHeader.DefineBoundedZone"; //F1 Documentation

CATCauCommandHdr.CATCauSplitCmdHdr.Icon.Normal = "I_Cau_SplitCpt.bmp";
//CATCauCommandHdr.CATCauSplitCmdHdr.LongHelpId="CATHCDCmdHeader.CATSPLSplitCptHelp";

CATCauCommandHdr.CATCauBoundaryCmdHdr.Icon.Normal="I_Cau_Boundaries";
//CATCauCommandHdr.CATCauBoundaryCmdHdr.LongHelpId= "CATHCDCmdHeader.DefineBoundary"; //F1 Documentation

//------------------------------------------------------------------------------
// Create Catalog Part
//------------------------------------------------------------------------------
CATCauCommandHdr.CATCauCreateCatalogPartHdr.Icon.Normal="I_Cau_BuildComponent";
CATCauCommandHdr.CATCauCreateCatalogPartHdr.LongHelpId="CATCauCommandHdr.CATCauCreateCatalogPartHdr";

//------------------------------------------------------------------------------
// Create Reference Component
//------------------------------------------------------------------------------
CATCauCommandHdr.CATCauCreateConnectorHdr.Icon.Normal="I_Cau_BuildConnector";
CATCauCommandHdr.CATCauCreateConnectorHdr.LongHelpId="CATCauCommandHdr.CATCauCreateConnectorHdr";

//------------------------------------------------------------------------------
// Modify Sub-Type Command
//------------------------------------------------------------------------------
CATCauCommandHdr.CATCauModifySubTypeHdr.Icon.Normal="I_Cau_ModifySubType";
CATCauCommandHdr.CATCauModifySubTypeHdr.LongHelpId="CATCauCommandHdr.CATCauModifySubTypeHdr";

//------------------------------------------------------------------------------
// Build Wall System Command
//------------------------------------------------------------------------------
CATCauCommandHdr.CATCauBuildWallSystemHdr.Icon.Normal="I_Cau_BuildWallSystem";
CATCauCommandHdr.CATCauBuildWallSystemHdr.LongHelpId="CATCauCommandHdr.CATCauBuildWallSystemHdr";

//------------------------------------------------------------------------------
// Intersect Wall System Command
//------------------------------------------------------------------------------
CATCauCommandHdr.CATCauIntersectWallSystemHdr.Icon.Normal="I_Cau_IntersectWallSystem";
CATCauCommandHdr.CATCauIntersectWallSystemHdr.LongHelpId="CATCauCommandHdr.CATCauIntersectWallSystemHdr";

//------------------------------------------------------------------------------
// Extract Compartments From Wall System Command
//------------------------------------------------------------------------------
CATCauCommandHdr.CATCauExtractCptsFromWallSystemHdr.Icon.Normal="I_Cau_ExtractCptsFromWallSystem";
CATCauCommandHdr.CATCauExtractCptsFromWallSystemHdr.LongHelpId="CATCauCommandHdr.CATCauExtractCptsFromWallSystemHdr";

//------------------------------------------------------------------------------
// Extract Walls From Sketch Command
//------------------------------------------------------------------------------
CATCauCommandHdr.CATCauExtractWallsFromSketchHdr.Icon.Normal="I_Cau_ExtractWallsFromSketch";
CATCauCommandHdr.CATCauExtractWallsFromSketchHdr.LongHelpId="CATCauCommandHdr.CATCauExtractWallsFromSketchHdr";

//------------------------------------------------------------------------------
// Enable/Disable Compartment Extraction Command
//------------------------------------------------------------------------------
CATCauCommandHdr.CATCauEnableDisableCptExtractionHdr.Icon.Normal="I_Cau_EnableDisableCptExtract";
CATCauCommandHdr.CATCauEnableDisableCptExtractionHdr.LongHelpId="CATCauCommandHdr.CATCauEnableDisableCptExtractionHdr";

//------------------------------------------------------------------------------
// Place Access Command
//------------------------------------------------------------------------------
CATCauCommandHdr.CATCauPlaceAccessHdr.Icon.Normal="I_Cau_PlaceAccess";
CATCauCommandHdr.CATCauPlaceAccessHdr.LongHelpId="CATCauCommandHdr.CATCauPlaceAccessHdr";

//------------------------------------------------------------------------------
// Loose Parts Management
//------------------------------------------------------------------------------
CATCauCommandHdr.CATCauLoosePartHdr.Icon.Normal="I_Clo_LooseParts";
//CATCauCommandHdr.CATCauLoosePartHdr.LongHelpId="CATCauCommandHeader.CATCauLoosePartHdr";

//------------------------------------------------------------------------------
// Change User Type Command
//------------------------------------------------------------------------------
CATCauCommandHdr.CATCauChangeCptUserTypeHdr.Icon.Normal="I_Psp_ChangeCptUserType";
CATCauCommandHdr.CATCauChangeCptUserTypeHdr.LongHelpId="CATCauCommandHdr.CATCauChangeCptUserTypeHdr";

//------------------------------------------------------------------------------
// Change User Type Command
//------------------------------------------------------------------------------
CATCauCommandHdr.CATCauChangeBdryUserTypeHdr.Icon.Normal="I_Psp_ChangeBdryUserType";
CATCauCommandHdr.CATCauChangeBdryUserTypeHdr.LongHelpId="CATCauCommandHdr.CATCauChangeBdryUserTypeHdr";

//------------------------------------------------------------------------------
// Edit Part Parameters Command
//------------------------------------------------------------------------------
CATCauCommandHdr.CATCauEditPartParamsHdr.Icon.Normal="I_ArrEditPartParams";
CATCauCommandHdr.CATCauEditPartParamsHdr.LongHelpId="CATCauCommandHdr.CATCauEditPartParamsHdr";

//------------------------------------------------------------------------------
// Disconnect Access
//------------------------------------------------------------------------------
CATCauCommandHdr.CATCauDisconnectAccessHdr.Icon.Normal="I_Cau_DisconnectAccess";
CATCauCommandHdr.CATCauDisconnectAccessHdr.LongHelpId="CATCauCommandHdr.CATCauDisconnectAccessHdr";

//------------------------------------------------------------------------------
// Connect Access
//------------------------------------------------------------------------------
CATCauCommandHdr.CATCauConnectAccessHdr.Icon.Normal="I_Cau_ConnectAccess";
CATCauCommandHdr.CATCauConnectAccessHdr.LongHelpId="CATCauCommandHdr.CATCauConnectAccessHdr";

//------------------------------------------------------------------------------
// Manage Representation Command
//------------------------------------------------------------------------------
CATCauCommandHdr.CATCauActivateRepHdr.Icon.Normal="I_Cau_ManageGraphicReps";
CATCauCommandHdr.CATCauActivateRepHdr.LongHelpId="CATCauCommandHdr.CATCauActivateRepHdr";

//------------------------------------------------------------------------------
// Update Compartment Membership Command
//------------------------------------------------------------------------------
CATCauCommandHdr.CATCauUpdateCptMembershipHdr.Icon.Normal="I_Cpt_UpdateCptMembership";
CATCauCommandHdr.CATCauUpdateCptMembershipHdr.LongHelpId="CATCauCommandHdr.CATCauUpdateCptMembershipHdr";

//------------------------------------------------------------------------------
// Select/Query Compartment Command
//------------------------------------------------------------------------------
CATCauCommandHdr.CATCauSelQueryCptHdr.Icon.Normal="I_Cpt_SelQueryCpts";
CATCauCommandHdr.CATCauSelQueryCptHdr.LongHelpId="CATCauCommandHdr.CATCauSelQueryCptHdr";

//------------------------------------------------------------------------------
// Lock/Unlock Compartment Membership Command
//------------------------------------------------------------------------------
CATCauCommandHdr.CATCauSetLockCptMembershipHdr.Icon.Normal="I_Cpt_SetLockCptMembership";
CATCauCommandHdr.CATCauSetLockCptMembershipHdr.LongHelpId="CATCauCommandHdr.CATCauSetLockCptMembershipHdr";

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// ID Management
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

//------------------------------------------------------------------------------
// Rename ID Command
//------------------------------------------------------------------------------
CATCauCommandHdr.CATCauRenameIDHdr.Icon.Normal="I_Psp_Rename";
CATCauCommandHdr.CATCauRenameIDHdr.LongHelpId="CATCauCommandHdr.CATCauRenameIDHdr";

CATCauCommandHdr.CATCauCopyAttributesHdr.Icon.Normal="I_Psp_CopyPspAttributes";
CATCauCommandHdr.CATCauCopyAttributesHdr.LongHelpId="CATCauCommandHdr.CATCauCopyAttributesHdr";

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Update Management
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

//------------------------------------------------------------------------------
// Update Wall System Command
//------------------------------------------------------------------------------
CATCauCommandHdr.CATCauUpdateWallSystemHdr.Icon.Normal="I_Cau_UpdateWallSystem";
CATCauCommandHdr.CATCauUpdateWallSystemHdr.LongHelpId="CATCauCommandHdr.CATCauUpdateWallSystemHdr";

//------------------------------------------------------------------------------
// Update Compartment Command
//------------------------------------------------------------------------------
CATCauCommandHdr.CATCauUpdateCompartmentHdr.Icon.Normal="I_Cau_UpdateCpts";
CATCauCommandHdr.CATCauUpdateCompartmentHdr.LongHelpId="CATCauCommandHdr.CATCauUpdateCompartmentHdr";

//------------------------------------------------------------------------------
// Add Compartment to Catalog Command
//------------------------------------------------------------------------------
CATCauCommandHdr.CATCauAddCptsToCatalogHdr.Icon.Normal="I_Cau_AddCptsToCatlg";
//Id not in DOC This is removed by BCY 4/12/07
// CATCauCommandHdr.CATCauAddCptsToCatalogHdr.LongHelpId="CATCauCommandHdr.CATCauAddCptsToCatalogHdr";

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Measure
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

//------------------------------------------------------------------------------
// Measure Between Command
//------------------------------------------------------------------------------
CATCauCommandHdr.CATCauMeasureBetweenHdr.Icon.Normal ="I_MeasureBetween";
CATCauCommandHdr.CATCauMeasureBetweenHdr.Icon.Focused="IF_MeasureBetween";
CATCauCommandHdr.CATCauMeasureBetweenHdr.Icon.Pressed="IP_MeasureBetween";
// Removed 4/12/07 - BCY
//CATCauCommandHdr.CATCauMeasureBetweenHdr.LongHelpId= "CATCauCommandHdr.CATCauMeasureBetweenHdr";

//------------------------------------------------------------------------------
// Measure Edge Command
//------------------------------------------------------------------------------
CATCauCommandHdr.CATCauMeasureEdgeHdr.Icon.Normal="I_MeasureItem";
CATCauCommandHdr.CATCauMeasureEdgeHdr.Icon.Focused="IF_MeasureItem";
CATCauCommandHdr.CATCauMeasureEdgeHdr.Icon.Pressed="IP_MeasureItem";
// Removed 4/12/07 - BCY
//CATCauCommandHdr.CATCauMeasureEdgeHdr.LongHelpId= "CATCauCommandHdr.CATCauMeasureEdgeHdr";

//------------------------------------------------------------------------------
// Measure Inertia Command
//------------------------------------------------------------------------------
CATCauCommandHdr.CATCauMeasureInertiaHdr.Icon.Normal= "I_MeasureInertia";
CATCauCommandHdr.CATCauMeasureInertiaHdr.Icon.Focused= "IF_MeasureInertia";
CATCauCommandHdr.CATCauMeasureInertiaHdr.Icon.Pressed= "IP_MeasureInertia";
// Removed 4/12/07 - BCY
//CATCauCommandHdr.CATCauMeasureInertiaHdr.LongHelpId= "CATCauCommandHdr.CATCauMeasureInertiaHdr";


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// C&A Tools Pulldown Menu Items
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

//------------------------------------------------------------------------------
// Report Definition Command
//------------------------------------------------------------------------------
CATCauCommandHdr.CATCauReportDefHdr.Icon.Normal="I_Rpu_ReportDefinition";
CATCauCommandHdr.CATCauReportDefHdr.LongHelpId="CATCauCommandHdr.CATCauReportDefHdr";

//------------------------------------------------------------------------------
// Report Generation Command
//------------------------------------------------------------------------------
CATCauCommandHdr.CATCauReportGenHdr.Icon.Normal="I_Rpu_ReportGeneration";
CATCauCommandHdr.CATCauReportGenHdr.LongHelpId="CATCauCommandHdr.CATCauReportGenHdr";

//------------------------------------------------------------------------------
// Part Catalog Report Command
//------------------------------------------------------------------------------
CATCauCommandHdr.CATPspPartCatalogReportHdr.Icon.Normal="I_Psp_CatalogReport";
CATCauCommandHdr.CATPspPartCatalogReportHdr.LongHelpId="CATCauCommandHdr.CATPspPartCatalogReportHdr";

//------------------------------------------------------------------------------
// Export Query Command
//------------------------------------------------------------------------------
CATCauCommandHdr.CATCauExportQueryHdr.Icon.Normal="I_Rpu_ExportQuery";
// Removed 4/12/07 - BCY
//CATCauCommandHdr.CATCauExportQueryHdr.LongHelpId="CATCauCommandHdr.CATCauExportQueryHdr";

//------------------------------------------------------------------------------
// Import Query Command
//------------------------------------------------------------------------------
CATCauCommandHdr.CATCauImportQueryHdr.Icon.Normal="I_Rpu_ImportQuery";
// Comment out until documented
//CATCauCommandHdr.CATCauImportQueryHdr.LongHelpId="CATCauCommandHdr.CATCauImportQueryHdr";

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// C&A Analysis Pulldown Menu Items
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

//------------------------------------------------------------------------------
// Related Objects
//------------------------------------------------------------------------------
CATCauCommandHdr.CATCauRelatedObjectsHdr.Icon.Normal="I_Cau_RelatedObjects";
// Removed 4/12/07 - BCY
//CATCauCommandHdr.CATCauRelatedObjectsHdr.LongHelpId="CATCauCommandHdr.CATCauRelatedObjectsHdr";

//------------------------------------------------------------------------------
// Create Opening
//------------------------------------------------------------------------------
CATCauCommandHdr.CATCauCreateOpeningAccessHdr.Icon.Normal="I_Cau_CreateOpening";
CATCauCommandHdr.CATCauCreateOpeningAccessHdr.LongHelpId="CATCauCommandHdr.CATCauCreateOpeningAccessHdr";

//------------------------------------------------------------------------------
// Update Opening
//------------------------------------------------------------------------------
CATCauCommandHdr.CATCauUpdateOpeningAccessHdr.Icon.Normal="I_Cau_UpdateOpening";
//CATCauCommandHdr.CATCauUpdateOpeningAccessHdr.LongHelpId="CATCauCommandHdr.CATCauUpdateOpeningAccessHdr";

//------------------------------------------------------------------------------
// Manage Opening
//------------------------------------------------------------------------------
CATCauCommandHdr.CATCauManageOpeningAccessHdr.Icon.Normal="I_Cau_ManageOpening";
CATCauCommandHdr.CATCauManageOpeningAccessHdr.LongHelpId="CATCauCommandHdr.CATCauManageOpeningAccessHdr";

