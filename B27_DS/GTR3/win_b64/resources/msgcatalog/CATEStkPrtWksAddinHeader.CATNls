// COPYRIGHT Dassault Systemes 2005
//===================================================================
//
// CATEStkPrtWksAddin.CATNls
//
//===================================================================
//
// Usage notes:
//
//===================================================================
//  Nov 2005  Creation: Code generated by HY
//===================================================================


//=========================================================================
// CPD Interface
//=========================================================================

// Open (Import) Command
//-------------------------------------------------------------------------
// see CATEStkPrtWksPermanentAddinHeader

CATEStkPrtWksAddinHeader.CATStkOpenCmdId.Title        = "Import Composites Structure" ;
CATEStkPrtWksAddinHeader.CATStkOpenCmdId.ShortHelp    = "Import Composites Structure";
CATEStkPrtWksAddinHeader.CATStkOpenCmdId.Help         = "Import Composites Data Structure from Text File";
CATEStkPrtWksAddinHeader.CATStkOpenCmdId.LongHelp     = "This command enables to import Composites Structure in the Workbench";
CATEStkPrtWksAddinHeader.CATStkOpenCmdId.Category     = "Composites";

// Stacking Rules (from CPD) Command
//-------------------------------------------------------------------------
// see CATEStkPrtWksPermanentAddinHeader

CATEStkPrtWksAddinHeader.CATStkStackingRulesCmdId.Title        = "Stacking Rules" ;
CATEStkPrtWksAddinHeader.CATStkStackingRulesCmdId.ShortHelp    = "Stacking Rules";
CATEStkPrtWksAddinHeader.CATStkStackingRulesCmdId.Help         = "Manage stacking rules";
CATEStkPrtWksAddinHeader.CATStkStackingRulesCmdId.LongHelp     = "This command enables to manage Stacking Rules on the current model";
CATEStkPrtWksAddinHeader.CATStkStackingRulesCmdId.Category     = "Composites";

// Save (Export) Command
//-------------------------------------------------------------------------
CATEStkPrtWksAddinHeader.CATStkSaveCmdId.Title        = "Export to file" ;
CATEStkPrtWksAddinHeader.CATStkSaveCmdId.ShortHelp    = "Export to file";
CATEStkPrtWksAddinHeader.CATStkSaveCmdId.Help         = "Export stacking and staggering to a file";
CATEStkPrtWksAddinHeader.CATStkSaveCmdId.LongHelp     = "Using this command, you will be able to re-build your new stack and stagger in CPD";
CATEStkPrtWksAddinHeader.CATStkSaveCmdId.Category     = "Composites";

// Save (Export to CPD) Command
//-------------------------------------------------------------------------
CATEStkPrtWksAddinHeader.CATStkSaveToCPDCmdId.Title        = "Apply to Stack" ;
CATEStkPrtWksAddinHeader.CATStkSaveToCPDCmdId.ShortHelp    = "Apply to Stack";
CATEStkPrtWksAddinHeader.CATStkSaveToCPDCmdId.Help         = "Export stacking and staggering to CPD";
CATEStkPrtWksAddinHeader.CATStkSaveToCPDCmdId.LongHelp     = "Using this command, you will be able to re-build your new stack and stagger in CPD";
CATEStkPrtWksAddinHeader.CATStkSaveToCPDCmdId.Category     = "Composites";

 // Close Command
 //-------------------------------------------------------------------------
CATEStkPrtWksAddinHeader.CATStkCloseCmdId.Title        = "Close StackingRules" ;
CATEStkPrtWksAddinHeader.CATStkCloseCmdId.ShortHelp    = "Close StackingRules application";
CATEStkPrtWksAddinHeader.CATStkCloseCmdId.Help         = "Close StackingRules application";
CATEStkPrtWksAddinHeader.CATStkCloseCmdId.LongHelp     = "Close StackingRules application";
CATEStkPrtWksAddinHeader.CATStkCloseCmdId.Category     = "Composites";

//=========================================================================
// Global handling
//=========================================================================
    
// Move Command
//-------------------------------------------------------------------------
CATEStkPrtWksAddinHeader.CATStkMoveCmdId.Title        = "Stack Move" ;
CATEStkPrtWksAddinHeader.CATStkMoveCmdId.ShortHelp    = "Stack Move";
CATEStkPrtWksAddinHeader.CATStkMoveCmdId.Help         = "Moving ply, layer, user group entities";
CATEStkPrtWksAddinHeader.CATStkMoveCmdId.LongHelp     = "Moving ply, layer, user group entities";
CATEStkPrtWksAddinHeader.CATStkMoveCmdId.Category     = "Composites";

// Swap Command
//-------------------------------------------------------------------------
CATEStkPrtWksAddinHeader.CATStkSwapCmdId.Title        = "Swap" ;
CATEStkPrtWksAddinHeader.CATStkSwapCmdId.ShortHelp    = "Swap";
CATEStkPrtWksAddinHeader.CATStkSwapCmdId.Help         = "Swapping ply, layer, user group entities";
CATEStkPrtWksAddinHeader.CATStkSwapCmdId.LongHelp     = "Swapping ply, layer, user group entities";
CATEStkPrtWksAddinHeader.CATStkSwapCmdId.Category     = "Composites";

// Creating a ply Command
//-------------------------------------------------------------------------
CATEStkPrtWksAddinHeader.CATStkCreatePlyCmdId.Title        = "Create Ply" ;
CATEStkPrtWksAddinHeader.CATStkCreatePlyCmdId.ShortHelp    = "Add a new ply in the stack";
CATEStkPrtWksAddinHeader.CATStkCreatePlyCmdId.Help         = "Add a new ply in the stack";
CATEStkPrtWksAddinHeader.CATStkCreatePlyCmdId.LongHelp     = "This command enables to add a ply and set its zones covering";
CATEStkPrtWksAddinHeader.CATStkCreatePlyCmdId.Category     = "Composites";

// Creating a ply Set Command
//-------------------------------------------------------------------------
CATEStkPrtWksAddinHeader.CATStkCreatePlySetCmdId.Title        = "Create Ply Set" ;
CATEStkPrtWksAddinHeader.CATStkCreatePlySetCmdId.ShortHelp    = "Add a new set of plies";
CATEStkPrtWksAddinHeader.CATStkCreatePlySetCmdId.Help         = "Add a new set of plies";
CATEStkPrtWksAddinHeader.CATStkCreatePlySetCmdId.LongHelp     = "This command enables to add a set of plies and set its zones covering";
CATEStkPrtWksAddinHeader.CATStkCreatePlySetCmdId.Category     = "Composites";

// Deleting a ply Set Command
//-------------------------------------------------------------------------
CATEStkPrtWksAddinHeader.CATStkDeleteSetCmdId.Title        = "Delete Ply Set" ;
CATEStkPrtWksAddinHeader.CATStkDeleteSetCmdId.ShortHelp    = "Delete a set of plies";
CATEStkPrtWksAddinHeader.CATStkDeleteSetCmdId.Help         = "Delete a set of plies";
CATEStkPrtWksAddinHeader.CATStkDeleteSetCmdId.LongHelp     = "This command enables to delete a set of plies and set its zones covering";
CATEStkPrtWksAddinHeader.CATStkDeleteSetCmdId.Category     = "Composites";

// Edit a ply Command
//-------------------------------------------------------------------------
CATEStkPrtWksAddinHeader.CATStkEditPlyCmdId.Title        = "Edit Ply" ;
CATEStkPrtWksAddinHeader.CATStkEditPlyCmdId.ShortHelp    = "Edit value of ply";
CATEStkPrtWksAddinHeader.CATStkEditPlyCmdId.Help         = "Edit value of ply";
CATEStkPrtWksAddinHeader.CATStkEditPlyCmdId.LongHelp     = "This command enables to edit a ply and modify it s values";
CATEStkPrtWksAddinHeader.CATStkEditPlyCmdId.Category     = "Composites";

// Edit multi plies Command
//-------------------------------------------------------------------------
CATEStkPrtWksAddinHeader.CATStkEditMultiPliesCmdId.Title        = "Edit Multi Plies" ;
CATEStkPrtWksAddinHeader.CATStkEditMultiPliesCmdId.ShortHelp    = "Multiple plies edition";
CATEStkPrtWksAddinHeader.CATStkEditMultiPliesCmdId.Help         = "Multiple plies edition";
CATEStkPrtWksAddinHeader.CATStkEditMultiPliesCmdId.LongHelp     = "This command enables to edit a ply and modify it s values";
CATEStkPrtWksAddinHeader.CATStkEditMultiPliesCmdId.Category     = "Composites";
//=========================================================================
// Utilities
//=========================================================================
    
// Delete Command
//-------------------------------------------------------------------------
CATEStkPrtWksAddinHeader.CATStkDeleteCmdId.Title        = "Delete" ;
CATEStkPrtWksAddinHeader.CATStkDeleteCmdId.ShortHelp    = "Deleting";
CATEStkPrtWksAddinHeader.CATStkDeleteCmdId.Help         = "Deleting ply, layer, user group entities";
CATEStkPrtWksAddinHeader.CATStkDeleteCmdId.LongHelp     = "Deleting ply, layer, user group entities";
CATEStkPrtWksAddinHeader.CATStkDeleteCmdId.Category     = "Composites";

// Undo Command
//-------------------------------------------------------------------------
CATEStkPrtWksAddinHeader.CATStkUndoCmdId.Title        = "Stack Undo" ;
CATEStkPrtWksAddinHeader.CATStkUndoCmdId.ShortHelp    = "Stack Undo";
CATEStkPrtWksAddinHeader.CATStkUndoCmdId.Help         = "Undo the last operation";
CATEStkPrtWksAddinHeader.CATStkUndoCmdId.LongHelp     = "Undo the last operation";
CATEStkPrtWksAddinHeader.CATStkUndoCmdId.Category     = "Composites";

// Undo history Command
//-------------------------------------------------------------------------
CATEStkPrtWksAddinHeader.CATStkUndoHistoryCmdId.Title        = "Stack Undo history" ;
CATEStkPrtWksAddinHeader.CATStkUndoHistoryCmdId.ShortHelp    = "Undo history command";
CATEStkPrtWksAddinHeader.CATStkUndoHistoryCmdId.Help         = "Undo the last operation with history";
CATEStkPrtWksAddinHeader.CATStkUndoHistoryCmdId.LongHelp     = "Undo the last operation with history";
CATEStkPrtWksAddinHeader.CATStkUndoHistoryCmdId.Category     = "Composites";

// Redo Command
//-------------------------------------------------------------------------
CATEStkPrtWksAddinHeader.CATStkRedoCmdId.Title        = "Stack Redo" ;
CATEStkPrtWksAddinHeader.CATStkRedoCmdId.ShortHelp    = "Stack Redo";
CATEStkPrtWksAddinHeader.CATStkRedoCmdId.Help         = "Redo the last operation";
CATEStkPrtWksAddinHeader.CATStkRedoCmdId.LongHelp     = "Redo the last operation";
CATEStkPrtWksAddinHeader.CATStkRedoCmdId.Category     = "Composites";

// Redo history Command
//-------------------------------------------------------------------------
CATEStkPrtWksAddinHeader.CATStkRedoHistoryCmdId.Title        = "Stack Redo history" ;
CATEStkPrtWksAddinHeader.CATStkRedoHistoryCmdId.ShortHelp    = "Redo history";
CATEStkPrtWksAddinHeader.CATStkRedoHistoryCmdId.Help         = "Redo the last operation with history";
CATEStkPrtWksAddinHeader.CATStkRedoHistoryCmdId.LongHelp     = "Redo the last operation with history";
CATEStkPrtWksAddinHeader.CATStkRedoHistoryCmdId.Category     = "Composites";

// Lock Zone Command
//-------------------------------------------------------------------------
CATEStkPrtWksAddinHeader.CATStkZoneLockCmdId.Title        = "Lock Zone" ;
CATEStkPrtWksAddinHeader.CATStkZoneLockCmdId.ShortHelp    = "Lock Zone";
CATEStkPrtWksAddinHeader.CATStkZoneLockCmdId.Help         = "Locking a zone";
CATEStkPrtWksAddinHeader.CATStkZoneLockCmdId.LongHelp     = "Locking a zone";
CATEStkPrtWksAddinHeader.CATStkZoneLockCmdId.Category     = "Composites";

// Unlock Zone Command
//-------------------------------------------------------------------------
CATEStkPrtWksAddinHeader.CATStkZoneUnlockCmdId.Title        = "Unlock Zone" ;
CATEStkPrtWksAddinHeader.CATStkZoneUnlockCmdId.ShortHelp    = "Unlock Zone";
CATEStkPrtWksAddinHeader.CATStkZoneUnlockCmdId.Help         = "Unlocking a zone";
CATEStkPrtWksAddinHeader.CATStkZoneUnlockCmdId.LongHelp     = "Unlocking a zone";
CATEStkPrtWksAddinHeader.CATStkZoneUnlockCmdId.Category     = "Composites";

// Cut Zone Command
//-------------------------------------------------------------------------
CATEStkPrtWksAddinHeader.CATStkZoneCutCmdId.Title        = "Cut Zone" ;
CATEStkPrtWksAddinHeader.CATStkZoneCutCmdId.ShortHelp    = "Cut Zone";
CATEStkPrtWksAddinHeader.CATStkZoneCutCmdId.Help         = "Cutting a Zone";
CATEStkPrtWksAddinHeader.CATStkZoneCutCmdId.LongHelp     = "Cutting a Zone : the Zone cut can be used to be pasted later";
CATEStkPrtWksAddinHeader.CATStkZoneCutCmdId.Category     = "Composites";

// Copy Zone Command
//-------------------------------------------------------------------------
CATEStkPrtWksAddinHeader.CATStkZoneCopyCmdId.Title        = "Copy Zone" ;
CATEStkPrtWksAddinHeader.CATStkZoneCopyCmdId.ShortHelp    = "Copy Zone";
CATEStkPrtWksAddinHeader.CATStkZoneCopyCmdId.Help         = "Copying a Zone";
CATEStkPrtWksAddinHeader.CATStkZoneCopyCmdId.LongHelp     = "Copying a Zone : the Zone cut can be used to be pasted later";
CATEStkPrtWksAddinHeader.CATStkZoneCopyCmdId.Category     = "Composites";

// Paste Zone Command
//-------------------------------------------------------------------------
CATEStkPrtWksAddinHeader.CATStkZonePasteCmdId.Title        = "Paste Zone" ;
CATEStkPrtWksAddinHeader.CATStkZonePasteCmdId.ShortHelp    = "Paste Zone";
CATEStkPrtWksAddinHeader.CATStkZonePasteCmdId.Help         = "Pasting a Zone";
CATEStkPrtWksAddinHeader.CATStkZonePasteCmdId.LongHelp     = "Pasting a Zone : the Zone comes from cut or copy operation";
CATEStkPrtWksAddinHeader.CATStkZonePasteCmdId.Category     = "Composites";

// Lock Ply Command
//-------------------------------------------------------------------------
CATEStkPrtWksAddinHeader.CATStkPlyLockCmdId.Title        = "Lock Ply" ;
CATEStkPrtWksAddinHeader.CATStkPlyLockCmdId.ShortHelp    = "Lock Ply";
CATEStkPrtWksAddinHeader.CATStkPlyLockCmdId.Help         = "Locking a Ply";
CATEStkPrtWksAddinHeader.CATStkPlyLockCmdId.LongHelp     = "Locking a Ply";
CATEStkPrtWksAddinHeader.CATStkPlyLockCmdId.Category     = "Composites";

// Unlock Ply Command
//-------------------------------------------------------------------------
CATEStkPrtWksAddinHeader.CATStkPlyUnlockCmdId.Title        = "Unlock Ply" ;
CATEStkPrtWksAddinHeader.CATStkPlyUnlockCmdId.ShortHelp    = "Unlock Ply";
CATEStkPrtWksAddinHeader.CATStkPlyUnlockCmdId.Help         = "Unlocking a Ply";
CATEStkPrtWksAddinHeader.CATStkPlyUnlockCmdId.LongHelp     = "Unlocking a Ply";
CATEStkPrtWksAddinHeader.CATStkPlyUnlockCmdId.Category     = "Composites";

// Cut Ply Command
//-------------------------------------------------------------------------
CATEStkPrtWksAddinHeader.CATStkCutCmdId.Title        = "Cut Ply" ;
CATEStkPrtWksAddinHeader.CATStkCutCmdId.ShortHelp    = "Cut Ply";
CATEStkPrtWksAddinHeader.CATStkCutCmdId.Help         = "Cutting a Ply";
CATEStkPrtWksAddinHeader.CATStkCutCmdId.LongHelp     = "Cutting a Ply : the ply cut can be used to be pasted later";
CATEStkPrtWksAddinHeader.CATStkCutCmdId.Category     = "Composites";

// Copy Ply Command
//-------------------------------------------------------------------------
CATEStkPrtWksAddinHeader.CATStkCopyCmdId.Title        = "Copy Ply" ;
CATEStkPrtWksAddinHeader.CATStkCopyCmdId.ShortHelp    = "Copy Ply";
CATEStkPrtWksAddinHeader.CATStkCopyCmdId.Help         = "Copying a Ply";
CATEStkPrtWksAddinHeader.CATStkCopyCmdId.LongHelp     = "Copying a Ply : the ply cut can be used to be pasted later";
CATEStkPrtWksAddinHeader.CATStkCopyCmdId.Category     = "Composites";

// Paste Ply Command
//-------------------------------------------------------------------------
CATEStkPrtWksAddinHeader.CATStkPasteCmdId.Title        = "Paste Ply" ;
CATEStkPrtWksAddinHeader.CATStkPasteCmdId.ShortHelp    = "Paste Ply";
CATEStkPrtWksAddinHeader.CATStkPasteCmdId.Help         = "Pasting a Ply";
CATEStkPrtWksAddinHeader.CATStkPasteCmdId.LongHelp     = "Pasting a Ply : the ply comes from cut or copy operation";
CATEStkPrtWksAddinHeader.CATStkPasteCmdId.Category     = "Composites";

// Cut Layers Command
//-------------------------------------------------------------------------
CATEStkPrtWksAddinHeader.CATStkCutLayersCmdId.Title        = "Cut Layers" ;
CATEStkPrtWksAddinHeader.CATStkCutLayersCmdId.ShortHelp    = "Cut Layers";
CATEStkPrtWksAddinHeader.CATStkCutLayersCmdId.Help         = "Cutting a Layers";
CATEStkPrtWksAddinHeader.CATStkCutLayersCmdId.LongHelp     = "Cutting a Layers : the Layers cut can be used to be pasted later";
CATEStkPrtWksAddinHeader.CATStkCutLayersCmdId.Category     = "Composites";

// Copy Layers Command
//-------------------------------------------------------------------------
CATEStkPrtWksAddinHeader.CATStkCopyLayersCmdId.Title        = "Copy Layers" ;
CATEStkPrtWksAddinHeader.CATStkCopyLayersCmdId.ShortHelp    = "Copy Layers";
CATEStkPrtWksAddinHeader.CATStkCopyLayersCmdId.Help         = "Copying a Layers";
CATEStkPrtWksAddinHeader.CATStkCopyLayersCmdId.LongHelp     = "Copying a Layers : the Layers cut can be used to be pasted later";
CATEStkPrtWksAddinHeader.CATStkCopyLayersCmdId.Category     = "Composites";

// Paste Layers Command
//-------------------------------------------------------------------------
CATEStkPrtWksAddinHeader.CATStkPasteLayersCmdId.Title        = "Paste Layers" ;
CATEStkPrtWksAddinHeader.CATStkPasteLayersCmdId.ShortHelp    = "Paste Layers";
CATEStkPrtWksAddinHeader.CATStkPasteLayersCmdId.Help         = "Pasting a Layers";
CATEStkPrtWksAddinHeader.CATStkPasteLayersCmdId.LongHelp     = "Pasting a Layers : the Layers comes from cut or copy operation";
CATEStkPrtWksAddinHeader.CATStkPasteLayersCmdId.Category     = "Composites";

// Cut Set Command
//-------------------------------------------------------------------------
CATEStkPrtWksAddinHeader.CATStkCutSetCmdId.Title        = "Cut Set" ;
CATEStkPrtWksAddinHeader.CATStkCutSetCmdId.ShortHelp    = "Cut Set";
CATEStkPrtWksAddinHeader.CATStkCutSetCmdId.Help         = "Cutting a Set";
CATEStkPrtWksAddinHeader.CATStkCutSetCmdId.LongHelp     = "Cutting a Set : the Set cut can be used to be pasted later";
CATEStkPrtWksAddinHeader.CATStkCutSetCmdId.Category     = "Composites";

// Copy Set Command
//-------------------------------------------------------------------------
CATEStkPrtWksAddinHeader.CATStkCopySetCmdId.Title        = "Copy Set" ;
CATEStkPrtWksAddinHeader.CATStkCopySetCmdId.ShortHelp    = "Copy Set";
CATEStkPrtWksAddinHeader.CATStkCopySetCmdId.Help         = "Copying a Set";
CATEStkPrtWksAddinHeader.CATStkCopySetCmdId.LongHelp     = "Copying a Set : the Set cut can be used to be pasted later";
CATEStkPrtWksAddinHeader.CATStkCopySetCmdId.Category     = "Composites";

// Paste Set Command
//-------------------------------------------------------------------------
CATEStkPrtWksAddinHeader.CATStkPasteSetCmdId.Title        = "Paste Set" ;
CATEStkPrtWksAddinHeader.CATStkPasteSetCmdId.ShortHelp    = "Paste Set";
CATEStkPrtWksAddinHeader.CATStkPasteSetCmdId.Help         = "Pasting a Set";
CATEStkPrtWksAddinHeader.CATStkPasteSetCmdId.LongHelp     = "Pasting a Set : the Set comes from cut or copy operation";
CATEStkPrtWksAddinHeader.CATStkPasteSetCmdId.Category     = "Composites";

//=========================================================================
// Optimizer
//=========================================================================
CATEStkPrtWksAddinHeader.CATStkOptimizerCmdId.Title        = "Optimizer" ;
CATEStkPrtWksAddinHeader.CATStkOptimizerCmdId.ShortHelp    = "Launch optimizer function";
CATEStkPrtWksAddinHeader.CATStkOptimizerCmdId.Help         = "Launch the optimizer function to help the user";
CATEStkPrtWksAddinHeader.CATStkOptimizerCmdId.LongHelp     = "Launch the optimizer function to help the user";
CATEStkPrtWksAddinHeader.CATStkOptimizerCmdId.Category     = "Composites";

//=========================================================================
// Stack Scoring
//=========================================================================
CATEStkPrtWksAddinHeader.CATStkStackScoringCmdId.Title        = "Scores" ;
CATEStkPrtWksAddinHeader.CATStkStackScoringCmdId.ShortHelp    = "Launch the scores window";
CATEStkPrtWksAddinHeader.CATStkStackScoringCmdId.Help         = "Launch the scores window to evaluate the quality of the stack";
CATEStkPrtWksAddinHeader.CATStkStackScoringCmdId.LongHelp     = "Launch the scores window : with this function, the user can evaluate the stack quality";
CATEStkPrtWksAddinHeader.CATStkStackScoringCmdId.Category     = "Composites";

//=========================================================================
// Rename Layers
//=========================================================================
CATEStkPrtWksAddinHeader.CATStkRenameLayersCmdId.Title        = "Rename" ;
CATEStkPrtWksAddinHeader.CATStkRenameLayersCmdId.ShortHelp    = "Rename Layers";
CATEStkPrtWksAddinHeader.CATStkRenameLayersCmdId.Help         = "Rename the layers of the stack with the defined prefix in the option panel";
CATEStkPrtWksAddinHeader.CATStkRenameLayersCmdId.LongHelp     = "Rename the layers of the stack with the defined prefix in the option panel";
CATEStkPrtWksAddinHeader.CATStkRenameLayersCmdId.Category     = "Composites";

//=========================================================================
// Rename User Defined
//=========================================================================
CATEStkPrtWksAddinHeader.CATStkRenameUserDefinedCmdId.Title        = "Rename" ;
CATEStkPrtWksAddinHeader.CATStkRenameUserDefinedCmdId.ShortHelp    = "Rename User Defined Set Of Plies";
CATEStkPrtWksAddinHeader.CATStkRenameUserDefinedCmdId.Help         = "Rename User Defined Set Of Plies of the stack with the defined prefix in the option panel";
CATEStkPrtWksAddinHeader.CATStkRenameUserDefinedCmdId.LongHelp     = "Rename User Defined Set Of Plies of the stack with the defined prefix in the option panel";
CATEStkPrtWksAddinHeader.CATStkRenameUserDefinedCmdId.Category     = "Composites";

//=========================================================================
// Thickness Law
//=========================================================================
CATEStkPrtWksAddinHeader.CATStkThicknessLawDiffCmdId.Title        = "Thickness Law Comparaison";
CATEStkPrtWksAddinHeader.CATStkThicknessLawDiffCmdId.ShortHelp    = "Thickness Law Comparaison";
CATEStkPrtWksAddinHeader.CATStkThicknessLawDiffCmdId.Help         = "Thickness Law Comparaison";
CATEStkPrtWksAddinHeader.CATStkThicknessLawDiffCmdId.LongHelp     = "Thickness Law Comparaison with Reference Thickness Law";
CATEStkPrtWksAddinHeader.CATStkThicknessLawDiffCmdId.Category     = "Composites";

//=========================================================================
// Symmetrize stack
//=========================================================================
CATEStkPrtWksAddinHeader.CATStkSymmetrizeStackCmdId.Title        = "Symmetrize stack";
CATEStkPrtWksAddinHeader.CATStkSymmetrizeStackCmdId.ShortHelp    = "Symmetrize stack";
CATEStkPrtWksAddinHeader.CATStkSymmetrizeStackCmdId.Help         = "Symmetrize stack";
CATEStkPrtWksAddinHeader.CATStkSymmetrizeStackCmdId.LongHelp     = "Reinitialize stack to be symmetric";
CATEStkPrtWksAddinHeader.CATStkSymmetrizeStackCmdId.Category     = "Composites";

//=========================================================================
// Tooling Z to Z 
//=========================================================================
CATEStkPrtWksAddinHeader.CATStkSwappingZTZCmdId.Title = "Swapping";
CATEStkPrtWksAddinHeader.CATStkSwappingZTZCmdId.ShortHelp    = "Swapping GL";
CATEStkPrtWksAddinHeader.CATStkSwappingZTZCmdId.Help         = "Swapping 2 Geometrical Level";
CATEStkPrtWksAddinHeader.CATStkSwappingZTZCmdId.LongHelp     = "Swapping 2 Geometrical Level";
CATEStkPrtWksAddinHeader.CATStkSwappingZTZCmdId.Category     = "Composites";

CATEStkPrtWksAddinHeader.CATStkPyramidalStaggeringCmdId.Title	     = "Pyramidal Staggering";
CATEStkPrtWksAddinHeader.CATStkPyramidalStaggeringCmdId.ShortHelp    = "Pyramidal Staggering";
CATEStkPrtWksAddinHeader.CATStkPyramidalStaggeringCmdId.Help         = "Pyramidal Staggering";
CATEStkPrtWksAddinHeader.CATStkPyramidalStaggeringCmdId.LongHelp     = "Pyramidal Staggering";
CATEStkPrtWksAddinHeader.CATStkPyramidalStaggeringCmdId.Category     = "Composites";

//=========================================================================
// Zone Global Handling 
//=========================================================================
CATEStkPrtWksAddinHeader.CATStkInsertUserDefinedVirtualZoneCmdId.Title        = "Insert User Defined Virtual Zone";
CATEStkPrtWksAddinHeader.CATStkInsertUserDefinedVirtualZoneCmdId.ShortHelp    = "Insert User Defined Virtual Zone";
CATEStkPrtWksAddinHeader.CATStkInsertUserDefinedVirtualZoneCmdId.Help         = "Insert User Defined Virtual Zone";
CATEStkPrtWksAddinHeader.CATStkInsertUserDefinedVirtualZoneCmdId.LongHelp     = "Insert User Defined Virtual Zone";
CATEStkPrtWksAddinHeader.CATStkInsertUserDefinedVirtualZoneCmdId.Category     = "Composites";

CATEStkPrtWksAddinHeader.CATStkInsertInterleavingVirtualZoneCmdId.Title        = "Insert Interleaving Virtual Zone";
CATEStkPrtWksAddinHeader.CATStkInsertInterleavingVirtualZoneCmdId.ShortHelp    = "Insert Interleaving Virtual Zone";
CATEStkPrtWksAddinHeader.CATStkInsertInterleavingVirtualZoneCmdId.Help         = "Insert Interleaving Virtual Zone";
CATEStkPrtWksAddinHeader.CATStkInsertInterleavingVirtualZoneCmdId.LongHelp     = "Insert Interleaving Virtual Zone";
CATEStkPrtWksAddinHeader.CATStkInsertInterleavingVirtualZoneCmdId.Category     = "Composites";

CATEStkPrtWksAddinHeader.CATStkDeleteVirtualZoneCmdId.Title        = "Delete Virtual Zone";
CATEStkPrtWksAddinHeader.CATStkDeleteVirtualZoneCmdId.ShortHelp    = "Delete Virtual Zone";
CATEStkPrtWksAddinHeader.CATStkDeleteVirtualZoneCmdId.Help         = "Delete Virtual Zone";
CATEStkPrtWksAddinHeader.CATStkDeleteVirtualZoneCmdId.LongHelp     = "Delete Virtual Zone";
CATEStkPrtWksAddinHeader.CATStkDeleteVirtualZoneCmdId.Category     = "Composites";

CATEStkPrtWksAddinHeader.CATStkHideZoneCmdId.Title        = "Hide Zone";
CATEStkPrtWksAddinHeader.CATStkHideZoneCmdId.ShortHelp    = "Hide Zone";
CATEStkPrtWksAddinHeader.CATStkHideZoneCmdId.Help         = "Hide Zone";
CATEStkPrtWksAddinHeader.CATStkHideZoneCmdId.LongHelp     = "Hide Zone";
CATEStkPrtWksAddinHeader.CATStkHideZoneCmdId.Category     = "Composites";

CATEStkPrtWksAddinHeader.CATStkShowZonesCmdId.Title        = "Show Zone";
CATEStkPrtWksAddinHeader.CATStkShowZonesCmdId.ShortHelp    = "Show Zone";
CATEStkPrtWksAddinHeader.CATStkShowZonesCmdId.Help         = "Show Zone";
CATEStkPrtWksAddinHeader.CATStkShowZonesCmdId.LongHelp     = "Show Zone";
CATEStkPrtWksAddinHeader.CATStkShowZonesCmdId.Category     = "Composites";

CATEStkPrtWksAddinHeader.CATStkMoveZoneCmdId.Title        = "Move Zone";
CATEStkPrtWksAddinHeader.CATStkMoveZoneCmdId.ShortHelp    = "Move Zone";
CATEStkPrtWksAddinHeader.CATStkMoveZoneCmdId.Help         = "Move Zone";
CATEStkPrtWksAddinHeader.CATStkMoveZoneCmdId.LongHelp     = "Move Zone";
CATEStkPrtWksAddinHeader.CATStkMoveZoneCmdId.Category     = "Composites";

CATEStkPrtWksAddinHeader.CATStkDefineIdealStackCmdId.Title        = "Define Ideal Stack";
CATEStkPrtWksAddinHeader.CATStkDefineIdealStackCmdId.ShortHelp    = "Define Ideal Stack";
CATEStkPrtWksAddinHeader.CATStkDefineIdealStackCmdId.Help         = "Define Ideal Stack";
CATEStkPrtWksAddinHeader.CATStkDefineIdealStackCmdId.LongHelp     = "Define Ideal Stack";
CATEStkPrtWksAddinHeader.CATStkDefineIdealStackCmdId.Category     = "Composites";


//=========================================================================
// Menu bar : View menu
//=========================================================================

CATEStkPrtWksAddinHeader.CATStkHalfStackVisuHdrAccessorId.Title        = "Half Stack" ;
CATEStkPrtWksAddinHeader.CATStkHalfStackVisuHdrAccessorId.ShortHelp    = "Half Stack";
CATEStkPrtWksAddinHeader.CATStkHalfStackVisuHdrAccessorId.Help         = "Only show the half stack";
CATEStkPrtWksAddinHeader.CATStkHalfStackVisuHdrAccessorId.LongHelp     = "In case of a symmetric stack, it can be easier to only see the half stack";
CATEStkPrtWksAddinHeader.CATStkHalfStackVisuHdrAccessorId.Category     = "View";

CATEStkPrtWksAddinHeader.CATStkStaggeringVisuHdrAccessorId.Title        = "Staggering Mode" ;
CATEStkPrtWksAddinHeader.CATStkStaggeringVisuHdrAccessorId.ShortHelp    = "Staggering Mode";
CATEStkPrtWksAddinHeader.CATStkStaggeringVisuHdrAccessorId.Help         = "Display virtual zones";
CATEStkPrtWksAddinHeader.CATStkStaggeringVisuHdrAccessorId.LongHelp     = "Staggering mode enables to see the drop-off";
CATEStkPrtWksAddinHeader.CATStkStaggeringVisuHdrAccessorId.Category     = "View";

CATEStkPrtWksAddinHeader.CATStkDisplayModeVisuHdrAccessorId.Title        = "Display Mode..." ;
CATEStkPrtWksAddinHeader.CATStkDisplayModeVisuHdrAccessorId.ShortHelp    = "Display Mode";
CATEStkPrtWksAddinHeader.CATStkDisplayModeVisuHdrAccessorId.Help         = "Display Mode option";
CATEStkPrtWksAddinHeader.CATStkDisplayModeVisuHdrAccessorId.LongHelp     = "Display mode enables to select differents view";
CATEStkPrtWksAddinHeader.CATStkDisplayModeVisuHdrAccessorId.Category     = "View";

CATEStkPrtWksAddinHeader.CATStkFilterModeVisuHdrAccessorId.Title        = "Filter Mode..." ;
CATEStkPrtWksAddinHeader.CATStkFilterModeVisuHdrAccessorId.ShortHelp    = "Filter Mode";
CATEStkPrtWksAddinHeader.CATStkFilterModeVisuHdrAccessorId.Help         = "Filter Mode option";
CATEStkPrtWksAddinHeader.CATStkFilterModeVisuHdrAccessorId.LongHelp     = "Filter mode enables to sorting differents view";
CATEStkPrtWksAddinHeader.CATStkFilterModeVisuHdrAccessorId.Category     = "View";

//=========================================================================
// Menu bar : Tools menu
//=========================================================================
CATEStkPrtWksAddinHeader.CATStkSymmetryHdrAccessorId.Title        = "Symetrical mode" ;
CATEStkPrtWksAddinHeader.CATStkSymmetryHdrAccessorId.ShortHelp    = "Symetrical mode";
CATEStkPrtWksAddinHeader.CATStkSymmetryHdrAccessorId.Help         = "Each operation on stack is done symetrically";
CATEStkPrtWksAddinHeader.CATStkSymmetryHdrAccessorId.LongHelp     = "Each operation on stack is done symetrically";
CATEStkPrtWksAddinHeader.CATStkSymmetryHdrAccessorId.Category     = "Tools";

CATEStkPrtWksAddinHeader.CATStkThicknessLawHdrAccessorId.Title        = "Thickness Law mode" ;
CATEStkPrtWksAddinHeader.CATStkThicknessLawHdrAccessorId.ShortHelp    = "Thickness Law mode";
CATEStkPrtWksAddinHeader.CATStkThicknessLawHdrAccessorId.Help         = "You can't Edit, Insert or delete Ply in this mode";
CATEStkPrtWksAddinHeader.CATStkThicknessLawHdrAccessorId.LongHelp     = "You can't Edit, Insert or delete Ply in this mode";
CATEStkPrtWksAddinHeader.CATStkThicknessLawHdrAccessorId.Category     = "Tools";

//=========================================================================
// Pop up menu on stack only
//=========================================================================
CATEStkPrtWksAddinHeader.CATStkCollapseLayerPliesCmdId.Title        = "Collapse layer plies" ;
CATEStkPrtWksAddinHeader.CATStkCollapseLayerPliesCmdId.ShortHelp    = "Collapse layer plies";
CATEStkPrtWksAddinHeader.CATStkCollapseLayerPliesCmdId.Help         = "Collapse plies of the selected layer";
CATEStkPrtWksAddinHeader.CATStkCollapseLayerPliesCmdId.LongHelp     = "Collapse plies of the selected layer in a single row : \nBecause layers only groups plies with same orientation/material, we have those informations displayed in the Collapsed row.";
CATEStkPrtWksAddinHeader.CATStkCollapseLayerPliesCmdId.Category     = "Tools";

CATEStkPrtWksAddinHeader.CATStkCollapseUserDefinedPliesCmdId.Title        = "Collapse User Defined set plies" ;
CATEStkPrtWksAddinHeader.CATStkCollapseUserDefinedPliesCmdId.ShortHelp    = "Collapse User Defined set plies";
CATEStkPrtWksAddinHeader.CATStkCollapseUserDefinedPliesCmdId.Help         = "Collapse plies of the selected User Defined Set";
CATEStkPrtWksAddinHeader.CATStkCollapseUserDefinedPliesCmdId.LongHelp     = "Collapse plies of the selected User Defined Set in a single row.\n Because User Defined set can be heterogeneous, no information is displayed in collapsed row";
CATEStkPrtWksAddinHeader.CATStkCollapseUserDefinedPliesCmdId.Category     = "Tools";

CATEStkPrtWksAddinHeader.CATStkCollapseLayerAllPliesCmdId.Title        = "Collapse all layer plies" ;
CATEStkPrtWksAddinHeader.CATStkCollapseLayerAllPliesCmdId.ShortHelp    = "Collapse all layer plies";
CATEStkPrtWksAddinHeader.CATStkCollapseLayerAllPliesCmdId.Help         = "Collapse plies of all layers";
CATEStkPrtWksAddinHeader.CATStkCollapseLayerAllPliesCmdId.LongHelp     = "Collapse plies of all layers in a single row per layer: \nBecause layers only groups plies with same orientation/material, we have those informations displayed in the Collapsed row.";
CATEStkPrtWksAddinHeader.CATStkCollapseLayerAllPliesCmdId.Category     = "Tools";

CATEStkPrtWksAddinHeader.CATStkExpandPliesCmdId.Title        = "Expand plies" ;
CATEStkPrtWksAddinHeader.CATStkExpandPliesCmdId.ShortHelp    = "Expand plies";
CATEStkPrtWksAddinHeader.CATStkExpandPliesCmdId.Help         = "Expand plies";
CATEStkPrtWksAddinHeader.CATStkExpandPliesCmdId.LongHelp     = "Expand plies";
CATEStkPrtWksAddinHeader.CATStkExpandPliesCmdId.Category     = "Tools";

CATEStkPrtWksAddinHeader.CATStkExpandAllPliesCmdId.Title        = "Expand all" ;
CATEStkPrtWksAddinHeader.CATStkExpandAllPliesCmdId.ShortHelp    = "Expand all";
CATEStkPrtWksAddinHeader.CATStkExpandAllPliesCmdId.Help         = "Expand all";
CATEStkPrtWksAddinHeader.CATStkExpandAllPliesCmdId.LongHelp     = "Expand all";
CATEStkPrtWksAddinHeader.CATStkExpandAllPliesCmdId.Category     = "Tools";






