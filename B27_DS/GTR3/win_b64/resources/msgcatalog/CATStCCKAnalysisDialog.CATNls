//=============================================================================
// COPYRIGHT DASSAULT SYSTEMES  2005
//=============================================================================
//
// CATStCCKAnalysisDialog: Resource file for NLS purposes: 
// Connect Checker Dialog Box
//
//=============================================================================
//
// Implementation Notes:
//
//=============================================================================
// Nov 2004						   Ritesh Kanetkar
//=============================================================================

Title="Connect Checker";
MainFrame.CCKTypeFrame.Title="Type";
MainFrame.GapFrame.Title="Gap";
MainFrame.GapFrame.Connection.Title="Connection";
MainFrame.GapFrame.UpperLimit.Title="UpperLimit";
//MainFrame.TabContainer.Search.FullTabPage.Title="Search";
MainFrame.TabContainer.QuickTabPage.Title="Quick";
MainFrame.TabContainer.FullTabPage.Title="Full";
MainFrame.TabContainer.FullTabPage.ContinuityFrame.Title="Continuity";
MainFrame.TabContainer.FullTabPage.DisplayFrame.Title="Display";
MainFrame.TabContainer.FullTabPage.ScalingFrame.Title="Amplitude";

MainFrame.TabContainer.QuickTabPage.QuickModeFrame.G0Label = "            ";
MainFrame.TabContainer.QuickTabPage.QuickModeFrame.G1Label = "            ";
MainFrame.TabContainer.QuickTabPage.QuickModeFrame.G2Label = "            ";
MainFrame.TabContainer.QuickTabPage.QuickModeFrame.G3Label = "            ";
MainFrame.TabContainer.QuickTabPage.QuickModeFrame.G4Label = "            ";

//Start HRY 
MainFrame.CCKTypeFrame.SurSurCCKRadio.ShortHelp = "Surface-Surface";
MainFrame.CCKTypeFrame.SurSurCCKRadio.LongHelp = "Checks connection between two or more surfaces";
MainFrame.CCKTypeFrame.CurCurCCKRadio.ShortHelp = "Curve-Curve";
MainFrame.CCKTypeFrame.CurCurCCKRadio.LongHelp = "Checks connection between two or more curves";
MainFrame.CCKTypeFrame.SurCurCCKRadio.ShortHelp = "Surface-Curve";
MainFrame.CCKTypeFrame.SurCurCCKRadio.LongHelp = "Checks connection between surfaces and curves";

MainFrame.GapFrame.Connection.LongHelp = "Sets the minimum gap below which all gaps are analysed as connected";
MainFrame.GapFrame.UpperLimit.LongHelp = "Sets the maximum gap above which no analysis will be performed";
MainFrame.GapFrame.GapRadio.ShortHelp = "Gap";
MainFrame.GapFrame.GapRadio.LongHelp = "Allows user to select gap definition";
MainFrame.GapFrame.InternalEdgesRadio.ShortHelp = "Internal Edge";
MainFrame.GapFrame.InternalEdgesRadio.LongHelp = "The internal edges of the joint elements are taken into account to perform the analysis";

MainFrame.TabContainer.FullTabPage.ContinuityFrame.G0Radio.ShortHelp = "G0";
MainFrame.TabContainer.FullTabPage.ContinuityFrame.G0Radio.LongHelp = "Checks for G0 type continuity";
MainFrame.TabContainer.FullTabPage.ContinuityFrame.G1Radio.ShortHelp = "G1";
MainFrame.TabContainer.FullTabPage.ContinuityFrame.G1Radio.LongHelp = "Checks for G1 type continuity";
MainFrame.TabContainer.FullTabPage.ContinuityFrame.G2Radio.ShortHelp = "G2";
MainFrame.TabContainer.FullTabPage.ContinuityFrame.G2Radio.LongHelp = "Checks for G2 type continuity";
MainFrame.TabContainer.FullTabPage.ContinuityFrame.FullG2Concavity.ShortHelp = "Concavity Defect";
MainFrame.TabContainer.FullTabPage.ContinuityFrame.FullG2Concavity.LongHelp = "Checks for Concavity in G2 mode";
MainFrame.TabContainer.FullTabPage.ContinuityFrame.G3Radio.ShortHelp = "G3";
MainFrame.TabContainer.FullTabPage.ContinuityFrame.G3Radio.LongHelp = "Checks for G3 type continuity";
MainFrame.TabContainer.FullTabPage.ContinuityFrame.OverlapRadio.ShortHelp = "Overlap";
MainFrame.TabContainer.FullTabPage.ContinuityFrame.OverlapRadio.LongHelp = "Checks for Overlap type continuity";

MainFrame.TabContainer.FullTabPage.DisplayFrame.Comb.ShortHelp = "Comb";
MainFrame.TabContainer.FullTabPage.DisplayFrame.Comb.LongHelp = "Displays Comb";
MainFrame.TabContainer.FullTabPage.DisplayFrame.Envelop.ShortHelp = "Envelop";
MainFrame.TabContainer.FullTabPage.DisplayFrame.Envelop.LongHelp = "Displays Envelop";
MainFrame.TabContainer.FullTabPage.DisplayFrame.MinInfo.ShortHelp = "MinInfo";
MainFrame.TabContainer.FullTabPage.DisplayFrame.MinInfo.LongHelp = "Displays minimum values";
MainFrame.TabContainer.FullTabPage.DisplayFrame.MaxInfo.ShortHelp = "MaxInfo";
MainFrame.TabContainer.FullTabPage.DisplayFrame.MaxInfo.LongHelp = "Displays maximum values";
MainFrame.TabContainer.FullTabPage.DisplayFrame.Discretization.ShortHelp = "Discretization";
MainFrame.TabContainer.FullTabPage.DisplayFrame.Discretization.LongHelp = "Allows the user to switch between the four predefined discretization coefficients";
MainFrame.TabContainer.FullTabPage.DisplayFrame.ColorScale.ShortHelp = "ColorScale";
MainFrame.TabContainer.FullTabPage.DisplayFrame.ColorScale.LongHelp = "Displays the analysis with full color range";

MainFrame.TabContainer.FullTabPage.ScalingFrame.AutoScaling.ShortHelp = "Auto Scaling";
MainFrame.TabContainer.FullTabPage.ScalingFrame.AutoScaling.LongHelp = "Enables auto scaling";
MainFrame.TabContainer.FullTabPage.ScalingFrame.MultiplyTwo.ShortHelp = "MultiplyTwo";
MainFrame.TabContainer.FullTabPage.ScalingFrame.MultiplyTwo.LongHelp = "Multiplies scale factor by 2";
MainFrame.TabContainer.FullTabPage.ScalingFrame.DivideTwo.ShortHelp = "DivideTwo";
MainFrame.TabContainer.FullTabPage.ScalingFrame.DivideTwo.LongHelp = "Divides scale factor by 2";

MainFrame.TabContainer.QuickTabPage.QuickModeFrame.G0Radio.ShortHelp = "G0";
MainFrame.TabContainer.QuickTabPage.QuickModeFrame.G0Radio.LongHelp = "Checks for G0 type continuity";
MainFrame.TabContainer.QuickTabPage.QuickModeFrame.G1Radio.ShortHelp = "G1";
MainFrame.TabContainer.QuickTabPage.QuickModeFrame.G1Radio.LongHelp = "Checks for G1 type continuity";
MainFrame.TabContainer.QuickTabPage.QuickModeFrame.G2Radio.ShortHelp = "G2";
MainFrame.TabContainer.QuickTabPage.QuickModeFrame.G2Radio.LongHelp = "Checks for G2 type continuity";
MainFrame.TabContainer.QuickTabPage.QuickModeFrame.G3Radio.ShortHelp = "G3";
MainFrame.TabContainer.QuickTabPage.QuickModeFrame.G3Radio.LongHelp = "Checks for G3 type continuity";
MainFrame.TabContainer.QuickTabPage.QuickModeFrame.OverlapRadio.ShortHelp = "Overlap";
MainFrame.TabContainer.QuickTabPage.QuickModeFrame.OverlapRadio.LongHelp = "Checks for overlap type continuity";
//End HRY 

ResultOverlapping.Title = "Overlapping";
ResultNoOverlapping.Title = "No overlapping";

SurfaceSelected.Message = " surface(s)";
CurveSelected.Message = " Curve(s)";
ConnectionsDetected.Message = " Connection(s)";

SingularCurve.Title = "SingularCurve";

// Abreviation for display of Rectifying / Osculator angle
Rectifying.Message = " (R)";
Osculator.Message = " (O)";  


// New Dialog box for R17SP02 Icons

MainFrame.InputElementsFrame.InputElementsLable.Title = "Elements: ";
MainFrame.InputElementsFrame.InputElementsSelector.ShortHelp = "Shows Number of Elements selected";
MainFrame.InputElementsFrame.InputElementsSelector.LongHelp = "Shows Total Number of Elements selected for Connection Analysis";
InputElementsListTitle.Message = "Input Elements List";

MainFrame.SecondRowDummyFrame.CCKTypeFrame.Title="Type";
MainFrame.SecondRowDummyFrame.CCKTypeFrame.LongHelp = "3 Different types of connections to detect";
MainFrame.SecondRowDummyFrame.CCKTypeFrame.SurSurCCKChkButton.ShortHelp = "Surface-Surface Connection";
MainFrame.SecondRowDummyFrame.CCKTypeFrame.SurSurCCKChkButton.LongHelp = "Checks connection between two or more surfaces";
MainFrame.SecondRowDummyFrame.CCKTypeFrame.CurCurCCKChkButton.ShortHelp = "Curve-Curve Connection";
MainFrame.SecondRowDummyFrame.CCKTypeFrame.CurCurCCKChkButton.LongHelp = "Checks connection between two or more curves";
MainFrame.SecondRowDummyFrame.CCKTypeFrame.SurCurCCKChkButton.ShortHelp = "Surface-Curve Connection";
MainFrame.SecondRowDummyFrame.CCKTypeFrame.SurCurCCKChkButton.LongHelp = "Checks connection between surfaces and curves";

MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.Title="Quick";
MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.OverlapChkButton.ShortHelp = "Overlap Defect";
MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.OverlapChkButton.LongHelp = "Checks for overlap type continuity";
MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.G0ChkButton.ShortHelp = "G0 continuity";
MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.G0ChkButton.LongHelp = "Checks for G0 type continuity";
MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.G1ChkButton.ShortHelp = "G1 continuity";
MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.G1ChkButton.LongHelp = "Checks for G1 type continuity";
MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.G2ChkButton.ShortHelp = "G2 continuity";
MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.G2ChkButton.LongHelp = "Checks for G2 type continuity";
MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.G3ChkButton.ShortHelp = "G3 continuity";
MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.G3ChkButton.LongHelp = "Checks for G3 type continuity";

MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.G0Text.Title = ">";
MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.G1Text.Title = ">";
MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.G2Text.Title = ">";
MainFrame.ThirdRowDummyFrame.TabContainer.QuickTabPage.QuickModeFrame.G3Text.Title = ">";

MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.Title="Full";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDummyDisplayFrame.FullModeDisplayOptionsFrame.Title="Display";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDummyDisplayFrame.FullModeDisplayOptionsFrame.LongHelp = "Various Display options for connections detected";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDummyDisplayFrame.FullModeDisplayOptionsFrame.LimitedColorScale.ShortHelp = "Limited Color Scale";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDummyDisplayFrame.FullModeDisplayOptionsFrame.LimitedColorScale.LongHelp = "Displays the analysis with limited color range";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDummyDisplayFrame.FullModeDisplayOptionsFrame.FullColorScale.ShortHelp = "Full Color Scale";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDummyDisplayFrame.FullModeDisplayOptionsFrame.FullColorScale.LongHelp = "Displays the analysis with Full color range";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDummyDisplayFrame.FullModeDisplayOptionsFrame.Comb.ShortHelp = "Comb";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDummyDisplayFrame.FullModeDisplayOptionsFrame.Comb.LongHelp = "Displays Comb";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDummyDisplayFrame.FullModeDisplayOptionsFrame.Envelop.ShortHelp = "Envelop";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDummyDisplayFrame.FullModeDisplayOptionsFrame.Envelop.LongHelp = "Displays Envelop";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDummyDisplayFrame.FullModeDisplayOptionsFrame.FullG2Concavity.ShortHelp = "Concavity Defect";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDummyDisplayFrame.FullModeDisplayOptionsFrame.FullG2Concavity.LongHelp = "Checks for Concavity Defect in G2 mode";

MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDummyDisplayFrame.AmplitudeFrame.Title="Amplitude";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDummyDisplayFrame.AmplitudeFrame.LongHelp = "Defines Amplitude of Comb";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDummyDisplayFrame.AmplitudeFrame.AutoScaling.ShortHelp = "Auto Scaling";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDummyDisplayFrame.AmplitudeFrame.AutoScaling.LongHelp = "Enables auto scaling of Comb";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDummyDisplayFrame.AmplitudeFrame.MultiplyTwo.ShortHelp = "Multiply by Two";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDummyDisplayFrame.AmplitudeFrame.MultiplyTwo.LongHelp = "Multiplies scale factor by 2";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDummyDisplayFrame.AmplitudeFrame.DivideTwo.ShortHelp = "Divide by Two";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeDummyDisplayFrame.AmplitudeFrame.DivideTwo.LongHelp = "Divides scale factor by 2";

MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeContinuityFrame.OverlapRadio.ShortHelp = "Overlap Defect";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeContinuityFrame.OverlapRadio.LongHelp = "Checks for Overlap type continuity";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeContinuityFrame.G0Radio.ShortHelp = "G0 continuity";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeContinuityFrame.G0Radio.LongHelp = "Checks for G0 type continuity";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeContinuityFrame.G1Radio.ShortHelp = "G1 continuity";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeContinuityFrame.G1Radio.LongHelp = "Checks for G1 type continuity";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeContinuityFrame.G2Radio.ShortHelp = "G2 continuity";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeContinuityFrame.G2Radio.LongHelp = "Checks for G2 type continuity";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeContinuityFrame.G3Radio.ShortHelp = "G3 continuity";
MainFrame.ThirdRowDummyFrame.TabContainer.FullTabPage.FullModeContinuityFrame.G3Radio.LongHelp = "Checks for G3 type continuity";

MainFrame.ThirdRowDummyFrame.DummyDisplayFrame.MinMaxInfoDisplayFrame.Title = "Information";
MainFrame.ThirdRowDummyFrame.DummyDisplayFrame.MinMaxInfoDisplayFrame.LongHelp = "Shows all Min and Max values for each connection found";
MainFrame.ThirdRowDummyFrame.DummyDisplayFrame.MinMaxInfoDisplayFrame.MinInfo.ShortHelp = "MinInfo";
MainFrame.ThirdRowDummyFrame.DummyDisplayFrame.MinMaxInfoDisplayFrame.MinInfo.LongHelp = "Displays minimum values";
MainFrame.ThirdRowDummyFrame.DummyDisplayFrame.MinMaxInfoDisplayFrame.MaxInfo.ShortHelp = "MaxInfo";
MainFrame.ThirdRowDummyFrame.DummyDisplayFrame.MinMaxInfoDisplayFrame.MaxInfo.LongHelp = "Displays maximum values";

MainFrame.ThirdRowDummyFrame.DummyDisplayFrame.MaxValuesDisplayFrame.Title = "Max Values";
MainFrame.ThirdRowDummyFrame.DummyDisplayFrame.MaxValuesDisplayFrame.LongHelp = "Shows Max value for each continuity from the current connections";
MainFrame.ThirdRowDummyFrame.DummyDisplayFrame.MaxValuesDisplayFrame.G0Label = "----  ";
MainFrame.ThirdRowDummyFrame.DummyDisplayFrame.MaxValuesDisplayFrame.G1Label = "----  ";
MainFrame.ThirdRowDummyFrame.DummyDisplayFrame.MaxValuesDisplayFrame.G2Label = "----  ";
MainFrame.ThirdRowDummyFrame.DummyDisplayFrame.MaxValuesDisplayFrame.G3Label = "----  ";

MainFrame.FourthRowDummyFrame.ConnectionFrame.Title = "Maximum Gap";
MainFrame.FourthRowDummyFrame.ConnectionFrame.LongHelp = "Defines parameters to search connections between input elements";
MainFrame.FourthRowDummyFrame.ConnectionFrame.InternalEdgeChkButton.ShortHelp = "Internal Edge";
MainFrame.FourthRowDummyFrame.ConnectionFrame.InternalEdgeChkButton.LongHelp = "The internal edges of the joint elements are taken into account to perform the analysis";

MainFrame.FourthRowDummyFrame.DiscretizationFrame.Title = "Discretization";
MainFrame.FourthRowDummyFrame.DiscretizationFrame.LongHelp = "Allows the user to switch between the four predefined discretization steps";
MainFrame.FourthRowDummyFrame.DiscretizationFrame.LightDiscretRadio.ShortHelp = "Light Discretization";
MainFrame.FourthRowDummyFrame.DiscretizationFrame.LightDiscretRadio.LongHelp = "Sets the Discretization type to Light";
MainFrame.FourthRowDummyFrame.DiscretizationFrame.CoarseDiscretRadio.ShortHelp = "Coarse Discretization";
MainFrame.FourthRowDummyFrame.DiscretizationFrame.CoarseDiscretRadio.LongHelp = "Sets the Discretization type to Coarse";
MainFrame.FourthRowDummyFrame.DiscretizationFrame.MediumDiscretRadio.ShortHelp = "Medium Discretization";
MainFrame.FourthRowDummyFrame.DiscretizationFrame.MediumDiscretRadio.LongHelp = "Sets the Discretization type to Medium";
MainFrame.FourthRowDummyFrame.DiscretizationFrame.FineDiscretRadio.ShortHelp = "Fine Discretization";
MainFrame.FourthRowDummyFrame.DiscretizationFrame.FineDiscretRadio.LongHelp = "Sets the Discretization type to Fine";

