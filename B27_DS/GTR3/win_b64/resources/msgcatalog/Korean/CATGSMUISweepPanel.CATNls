//--------------------------------------------
// Resource file for CATGSMUISweepPanel class
// En_US
//--------------------------------------------

// General texts
//---------------------------------------------------------------
TitleSURFACE="Sweep 서피스 정의";
TitleVOLUME="스위프 볼륨 정의";

SweepTypeUnspec="명시";
SweepTypeSegment="선";
SweepTypeCircle="원";
SweepTypeConic="원추";

FraMenuBut.LabelList.Title="프로파일 유형: ";
FraMenuBut.LabelList.LongHelp="Sweep할 수 있는 사용 가능한 프로파일 유형을 나열합니다.";
FraMenuBut.LabelMini.Title="명시";
FraMenuBut.FraButtons.MenuBExpl.ShortHelp="명시";
FraMenuBut.FraButtons.MenuBExpl.LongHelp="명시적 사용자 정의 프로파일을 사용하여 Sweep 서피스를 작성합니다.";
FraMenuBut.FraButtons.MenuBCirc.ShortHelp="원";
FraMenuBut.FraButtons.MenuBCirc.LongHelp="암시적 원형 프로파일을 사용하여 Sweep 서피스를 작성합니다.";
FraMenuBut.FraButtons.MenuBLine.ShortHelp="선";
FraMenuBut.FraButtons.MenuBLine.LongHelp="암시적 원형 프로파일을 사용하여 Sweep 서피스를 작성합니다.";
FraMenuBut.FraButtons.MenuBConi.ShortHelp="원추";
FraMenuBut.FraButtons.MenuBConi.LongHelp="암시적 원추 프로파일을 사용하여 Sweep 서피스를 작성합니다.";

FraMenuButVol.LabelList.Title="프로파일 유형: ";
FraMenuButVol.LabelList.LongHelp="Sweep할 수 있는 사용 가능한 프로파일 유형을 나열합니다.";
FraMenuButVol.LabelMini.Title="명시";
FraMenuButVol.FraButtons.MenuBExpl.ShortHelp="명시";
FraMenuButVol.FraButtons.MenuBExpl.LongHelp="명시적 사용자 정의 프로파일을 사용하여 스위프 볼륨을 작성합니다.";
FraMenuButVol.FraButtons.MenuBCirc.ShortHelp="원";
FraMenuButVol.FraButtons.MenuBCirc.LongHelp="암시적 원형 프로파일을 사용하여 스위프 볼륨을 작성합니다.";
FraMenuButVol.FraButtons.MenuBLine.ShortHelp="선";
FraMenuButVol.FraButtons.MenuBLine.LongHelp="도면 방향으로 스위프 볼륨을 작성합니다.";

FraSweep.Illustration.LongHelp="현재 sweep 입력을 설명합니다.";

ChaineDefaut="디폴트";
ChaineProjection="프로젝션";
ChaineMeanPlane="디폴트(중간면)";

// Sweep unspec
//---------------------------------------------------------------
FraSweep.FraMandatory.LabProfile.Title="프로파일: ";
FraSweep.FraMandatory.LabProfile.LongHelp="Sweep할 프로파일을 지정합니다.";
FraSweep.FraMandatory.Profile.LongHelp="Sweep할 프로파일을 지정합니다.";
FraSweep.FraMandatory.LabGuideCurve.Title="가이드 커브: ";
FraSweep.FraMandatory.LabGuideCurve.LongHelp="첫 번째 가이드 커브를 지정합니다.";
FraSweep.FraMandatory.GuideCurve.LongHelp="첫 번째 가이드 커브를 지정합니다.";

FraSweep.FraOptional.ListeOnglet.Onglet1.Title="레퍼런스";
FraSweep.FraOptional.ListeOnglet.Onglet1.LongHelp="Sweep를 수행하는 동안 프로파일의 위치를 제어하는
레퍼런스 서피스 및 각도를 지정할 수 있습니다. ";
FraSweep.FraOptional.ListeOnglet.Onglet1.SubFrameElt.LabReference.Title="서피스: ";
FraSweep.FraOptional.ListeOnglet.Onglet1.SubFrameElt.LabReference.LongHelp="Sweep를 수행하는 동안 프로파일 위치를
제어하는 데 사용되는 레퍼런스 서피스를 지정합니다.
서피스를 지정하지 않은 경우 디폴트 서피스는 스파인에서
중간면입니다.";
FraSweep.FraOptional.ListeOnglet.Onglet1.SubFrameElt.Reference.LongHelp="Sweep를 수행하는 동안 프로파일 위치를
제어하는 데 사용되는 레퍼런스 서피스를 지정합니다.
서피스를 지정하지 않은 경우 디폴트 서피스는 스파인에서
중간면입니다.";
FraSweep.FraOptional.ListeOnglet.Onglet1.SubFrameLit.LabRefAngle.Title="각도: ";
FraSweep.FraOptional.ListeOnglet.Onglet1.SubFrameLit.LabRefAngle.LongHelp="Sweep를 수행하는 동안 프로파일의 위치를
제어하는 데 사용되는 각도를 지정합니다.";
FraSweep.FraOptional.ListeOnglet.Onglet1.SubFrameLit.FraRefAngle.EnglobingFrame.IntermediateFrame.Spinner.Title="각도";
FraSweep.FraOptional.ListeOnglet.Onglet1.SubFrameLit.LawButton.Title="법칙...";

FraSweep.FraOptional.ListeOnglet.Onglet2.Title="두 번째 가이드 커브";
FraSweep.FraOptional.ListeOnglet.Onglet2.LongHelp="두 번째 가이드 커브를 지정할 수 있습니다.";
FraSweep.FraOptional.ListeOnglet.Onglet2.LabGuideCurve2.Title="가이드 커브 2: ";
FraSweep.FraOptional.ListeOnglet.Onglet2.LabGuideCurve2.LongHelp="두 번째 가이드 커브를 지정합니다.";
FraSweep.FraOptional.ListeOnglet.Onglet2.GuideCurve2.LongHelp="두 번째 가이드 커브를 지정합니다.";
FraSweep.FraOptional.ListeOnglet.Onglet2.LabelDirComp.Title="고정 유형: ";
FraSweep.FraOptional.ListeOnglet.Onglet2.LabelDirComp.LongHelp="이중 가이드 명시 sweep 서피스의 경우,
다른 유형의 고정 옵션을 지정할 수 있습니다. ";
FraSweep.FraOptional.ListeOnglet.Onglet2.ComboDirComp.LongHelp="이중 가이드 명시 sweep 서피스의 경우,
사용 가능한 고정 옵션 유형을 나열합니다.";
SweepExpBirailPoints="두 점";
SweepExpBirailPtDir="점 및 방향";
FraSweep.FraOptional.ListeOnglet.Onglet2.LabFittingPoint1.Title="가이드 1 앵커 점: ";
FraSweep.FraOptional.ListeOnglet.Onglet2.LabFittingPoint1.LongHelp="프로파일에 대한 첫 번째 가이드 앵커 점을 지정합니다.";
FraSweep.FraOptional.ListeOnglet.Onglet2.FittingPoint1.LongHelp="프로파일에 대한 첫 번째 가이드 앵커 점을 지정합니다.";
FraSweep.FraOptional.ListeOnglet.Onglet2.LabFittingPoint2.Title="가이드 2 앵커 점: ";
FraSweep.FraOptional.ListeOnglet.Onglet2.LabFittingPoint2.LongHelp="프로파일에 대한 두 번째 가이드 앵커 점을 지정합니다.";
FraSweep.FraOptional.ListeOnglet.Onglet2.FittingPoint2.LongHelp="프로파일에 대한 두 번째 가이드 앵커 점을 지정합니다.";
FraSweep.FraOptional.ListeOnglet.Onglet2.LabXAxisDirection.Title="고정 방향: ";
FraSweep.FraOptional.ListeOnglet.Onglet2.LabXAxisDirection.LongHelp="프로파일에 대한 고정 방향을 지정합니다.";
FraSweep.FraOptional.ListeOnglet.Onglet2.FraDirection.LongHelp="프로파일에 대한 고정 방향을 지정합니다.";

FraSweep.FraOptional.FrameSpine.LabSpine.Title="스파인: ";
FraSweep.FraOptional.FrameSpine.LabSpine.LongHelp="첫 번째 가이드 커브와 다른 스파인을 사용하려면
스파인 커브를 지정합니다. ";
FraSweep.FraOptional.FrameSpine.Spine.LongHelp="첫 번째 가이드 커브와 다른 스파인을 사용하려면
스파인 커브를 지정합니다. ";

FraSweep.FraOptional.FrameSpine.LabRelimiter1.Title="  한계 재설정기 1: ";
FraSweep.FraOptional.FrameSpine.LabRelimiter1.LongHelp="스파인 한계를 재설정(즉 sweep)하기 위해 사용되는
첫 번째 요소(점 또는 평면)를 지정합니다.";
FraSweep.FraOptional.FrameSpine.Relimiter1.LongHelp="스파인 한계를 재설정(즉 sweep)하기 위해 사용되는
첫 번째 요소(점 또는 평면)를 지정합니다.";

FraSweep.FraOptional.FrameSpine.LabRelimiter2.Title="  한계 재설정기 2: ";
FraSweep.FraOptional.FrameSpine.LabRelimiter2.LongHelp="스파인 한계를 재설정(즉 sweep)하기 위해 사용되는
두 번째 요소(점 또는 평면)를 지정합니다.";
FraSweep.FraOptional.FrameSpine.Relimiter2.LongHelp="스파인 한계를 재설정(즉 sweep)하기 위해 사용되는
두 번째 요소(점 또는 평면)를 지정합니다.";

FraSweep.FraOptional.FrameTwist.SepStart.TitleLabel.Title="공간(Twisted) 영역 관리 ";
FraSweep.FraOptional.FrameTwist.TwistBut.Title="미리보기에서 커터 제거";
FraSweep.FraOptional.FrameTwist.TwistBut.LongHelp="공간(Twisted) 영역 관리에 의해 추가된 커터를 자동으로 제거할 수 있습니다. ";
FraSweep.FraOptional.FrameTwist.SetbackLabel.Title="세트백 ";
FraSweep.FraOptional.FrameTwist.SetbackSlider.ShortHelp="0 - 20%";
FraSweep.FraOptional.FrameTwist.SetbackSlider.LongHelp="가이드 길이의 백분율(0부터 20%까지)로 세트백 값을 설정합니다.";
FraSweep.FraOptional.FrameTwist.PercentLabel.Title=" %";
FraSweep.FraOptional.FrameTwist.FillButton.Title="왜곡 면적 채우기";
FraSweep.FraOptional.FrameTwist.FillButton.LongHelp="왜곡된 면적을 자동으로 채울 수 있습니다.";
FraSweep.FraOptional.FrameTwist.C0VerticesModeButton.Title="C0 정점을 왜곡 면적으로 계산";
FraSweep.FraOptional.FrameTwist.C0VerticesModeButton.LongHelp="가이드의 C0 정점을 왜곡 면적으로 관리할 수 있습니다.";
FraSweep.FraOptional.FrameTwist.ConnectionStrategyLabel.Title="연결 전략: ";
FraSweep.FraOptional.FrameTwist.ConnectionStrategyCombo.LongHelp="채운 면적에 대해 연결 전략을 선택할 수 있습니다(자동, 표준 연결 또는 가이드와 비슷함).";
ConnectionStrategyAutomatic="자동";
ConnectionStrategyStandard="표준";
ConnectionStrategySimilar="가이드와 비슷함";
FraSweep.FraOptional.FrameTwist.AddCutterButton.Title="커터 추가";
FraSweep.FraOptional.FrameTwist.AddCutterButton.LongHelp="면적을 제거하거나 채우도록 커터를 추가할 수 있습니다.";

// GSD_PULLINGDIREXPLICIT
//---------------------------------------------------------------
FraSweep.FraMandatory.FraComboExpl.LabComboExpl.Title="하위 유형: ";
FraSweep.FraMandatory.FraComboExpl.LabComboExpl.LongHelp="명시적 프로파일을 사용하여 Sweep 서피스 하위 유형을 나열합니다.";
FraSweep.FraMandatory.FraComboExpl.ComboExpl.LongHelp="명시적 프로파일을 사용하여 Sweep 서피스 하위 유형을 나열합니다.";

FraSweep.FraMandatory.FraComboExplVol.LabComboExplVol.Title="하위 유형: ";
FraSweep.FraMandatory.FraComboExplVol.LabComboExplVol.LongHelp="명시적 프로파일을 사용하여 스위프 볼륨 하위 유형을 나열합니다.";
FraSweep.FraMandatory.FraComboExplVol.ComboExplVol.LongHelp="명시적 프로파일을 사용하여 스위프 볼륨 하위 유형을 나열합니다.";

SweepExplicit1="레퍼런스 서피스 사용";
SweepExplicit2="두 가이드 커브 사용";
SweepExplicit3="당기는 방향 사용";

FraSweep.FraMandatory.SubFrameElt.LabProfile.Title="프로파일: ";
FraSweep.FraMandatory.SubFrameElt.LabProfile.LongHelp="Sweep할 프로파일을 지정합니다.";
FraSweep.FraMandatory.SubFrameElt.Profile.LongHelp="Sweep할 프로파일을 지정합니다.";

FraSweep.FraMandatory.SubFrameElt.LabGuideCurve.Title="가이드 커브: ";
FraSweep.FraMandatory.SubFrameElt.LabGuideCurve.LongHelp="가이드 커브를 지정합니다.";
FraSweep.FraMandatory.SubFrameElt.GuideCurve.LongHelp="가이드 커브를 지정합니다.";
FraSweep.FraMandatory.SubFrameElt.LabGuideCurve1.Title="가이드 커브 1: ";
FraSweep.FraMandatory.SubFrameElt.LabGuideCurve1.LongHelp="첫 번째 가이드 커브를 지정합니다.";
FraSweep.FraMandatory.SubFrameElt.GuideCurve1.LongHelp="첫 번째 가이드 커브를 지정합니다.";

FraSweep.FraMandatory.SubFrameElt.LabReference.Title="서피스: ";
FraSweep.FraMandatory.SubFrameElt.LabReference.LongHelp="Sweep를 수행하는 동안 프로파일 위치를
제어하는 데 사용되는 레퍼런스 서피스를 지정합니다.
서피스를 지정하지 않은 경우 디폴트 서피스는 스파인에서
중간면입니다.";
FraSweep.FraMandatory.SubFrameElt.Reference.LongHelp="Sweep를 수행하는 동안 프로파일 위치를
제어하는 데 사용되는 레퍼런스 서피스를 지정합니다.
서피스를 지정하지 않은 경우 디폴트 서피스는 스파인에서
중간면입니다.";
FraSweep.FraMandatory.SubFrameLit.LabRefAngle.Title="각도: ";
FraSweep.FraMandatory.SubFrameLit.LabRefAngle.LongHelp="Sweep를 수행하는 동안 프로파일의 위치를
제어하는 데 사용되는 각도를 지정합니다.";
FraSweep.FraMandatory.SubFrameLit.FraRefAngle.EnglobingFrame.IntermediateFrame.Spinner.Title="각도";
FraSweep.FraMandatory.SubFrameLit.LawButton.Title="법칙...";

FraSweep.FraMandatory.SubFrameElt.LabGuideCurve2.Title="가이드 커브 2: ";
FraSweep.FraMandatory.SubFrameElt.LabGuideCurve2.LongHelp="두 번째 가이드 커브를 지정합니다.";
FraSweep.FraMandatory.SubFrameElt.GuideCurve2.LongHelp="두 번째 가이드 커브를 지정합니다.";
FraSweep.FraMandatory.SubFrameElt.LabelDirComp.Title="고정 유형: ";
FraSweep.FraMandatory.SubFrameElt.LabelDirComp.LongHelp="이중 가이드 명시 sweep 서피스의 경우,
다른 유형의 고정 옵션을 지정할 수 있습니다. ";
FraSweep.FraMandatory.SubFrameElt.ComboDirComp.LongHelp="이중 가이드 명시 sweep 서피스의 경우,
사용 가능한 고정 옵션 유형을 나열합니다.";
FraSweep.FraMandatory.SubFrameElt.LabFittingPoint1.Title="  앵커 점 1: ";
//FraSweep.FraMandatory.SubFrameElt.LabFittingPoint1.LongHelp="Specifies the first guide anchor point on the profile.";
//FraSweep.FraMandatory.SubFrameElt.FittingPoint1.LongHelp="Specifies the first guide anchor point on the profile.";
FraSweep.FraMandatory.SubFrameElt.LabFittingPoint2.Title="  앵커 점 2: ";
//FraSweep.FraMandatory.SubFrameElt.LabFittingPoint2.LongHelp="Specifies the second guide anchor point on the profile.";
//FraSweep.FraMandatory.SubFrameElt.FittingPoint2.LongHelp="Specifies the second guide anchor point on the profile.";
FraSweep.FraMandatory.SubFrameElt.LabXAxisDirection.Title="- 고정 방향: ";
//FraSweep.FraMandatory.SubFrameElt.LabXAxisDirection.LongHelp="Specifies the anchor direction on the profile.";
//FraSweep.FraMandatory.SubFrameElt.FraDirection.LongHelp="Specifies the anchor direction on the profile.";
FittingPoint1LongHelp="프로파일에 대한 첫 번째 가이드 앵커 점을 지정합니다.";
FittingPoint2LongHelp="프로파일에 대한 두 번째 가이드 앵커 점을 지정합니다.";
LabXAxisDirectionLongHelp="프로파일에 대한 고정 방향을 지정합니다.";
//Automated Anchor Elements Computation
Computed="계산";
AutoFittingPoint1LongHelp="프로파일에 대한 첫 번째 가이드 앵커 점을 지정합니다. 디폴트는 프로파일 평면과 가이드 커브 1 사이의 교차점입니다. ";
AutoFittingPoint2LongHelp="프로파일에 대한 두 번째 가이드 앵커 점을 지정합니다. \n디폴트는 스파인에 수직인 앵커 점 1을 통과하는 평면과 가이드 커브 2 사이의 교차점입니다. ";
AutoLabXAxisDirectionLongHelp="프로파일에 대한 고정 방향을 지정합니다. 디폴트로, 이 방향은 프로파일 평면과 가이드 커브 1 및 가이드 커브 2 사이에 계산됩니다. ";

FraSweep.FraMandatory.SubFrameElt.LabPullingDir.Title="방향: ";
FraSweep.FraMandatory.SubFrameElt.LabPullingDir.LongHelp="Sweep를 수행하는 동안 프로파일의 위치를
제어하는 데 사용되는 당기기 방향을 지정합니다.";

FraSweep.FraOptional.GuideProjBut.Title="스파인으로 가이드 커브 프로젝션";
FraSweep.FraOptional.GuideProjBut.LongHelp="평면인 경우에는 레퍼런스 서피스로,
또는 프로젝션을 스파인으로 사용하려는 경우에는 당기는 방향에
수직인 평면으로 가이드 커브를 프로젝션할 수 있습니다.";


// Tolerant sweep : explicite, lineaire, circulaire et conique
//---------------------------------------------------------------
FraSweep.FraOptional.FrameSmooth.SepStart.TitleLabel.Title="Sweep 다듬기 ";
FraSweep.FraOptional.FrameSmooth.FrameLayoutAngle.SmoothAngleButton.Title="각도 정정: ";
FraSweep.FraOptional.FrameSmooth.FrameLayoutAngle.SmoothAngleButton.LongHelp="지정된 각도 값 아래에서
불연속성을 지워(프레임 및 접촉 네트 이동 시) sweep 작업을 매끄럽게 할 수 있습니다. ";
FraSweep.FraOptional.FrameSmooth.FrameLayoutAngle.FraLittSmoothAngle.EnglobingFrame.IntermediateFrame.Spinner.Title="스무드 각도";
FraSweep.FraOptional.FrameSmooth.FrameLayoutDev.SmoothDevButton.Title="가이드 커브에서 편차: ";
FraSweep.FraOptional.FrameSmooth.FrameLayoutDev.SmoothDevButton.LongHelp="지정된 반지름의 파이프 내에서
가이드 커브로부터 편차에 의해 sweep 작업을 매끄럽게 할 수 있습니다. ";
FraSweep.FraOptional.FrameSmooth.FrameLayoutDev.FraLittSmoothDev.EnglobingFrame.IntermediateFrame.Spinner.Title="SmoothDeviation";

// Sweep explicite : positionnement
//---------------------------------------------------------------
FraSweep.FraOptional.NotifFrame.StartSeparator.TitleLabel.Title="위치 지정 매개변수";

FraSweep.FraOptional.NotifFrame.FraButton.SelPosButton.Title="프로파일 위치 지정";
FraSweep.FraOptional.NotifFrame.FraButton.SelPosButton.LongHelp="디폴트로, 위치 지정된 프로파일이 사용됩니다.
첫 번째 sweep 평면에서 프로파일 위치를 수동으로 지정하려면
이 박스를 선택하고 지오메트리를 조작하거나 위치 지정 매개변수를 수정하십시오.";
LabelShowPosParam="표시 매개변수 >>";
LabelHidePosParam="숨기기 매개변수 <<";

FraSweep.FraOptional.NotifFrame.AdvFrame.LabelExplPos1.Title="첫 번째 sweep 평면의 원점";

FraSweep.FraOptional.NotifFrame.AdvFrame.FraOriginMain.RadBOrigin1.Title="원점 좌표";
FraSweep.FraOptional.NotifFrame.AdvFrame.FraOriginMain.RadBOrigin1.LongHelp="좌표를 사용하여 첫 번째 sweep 평면에서
위치 지정 좌표계의 원점을 지정합니다. ";
FraSweep.FraOptional.NotifFrame.AdvFrame.FraOriginMain.FraOrigin1.LabelLittX.Title="X: ";
FraSweep.FraOptional.NotifFrame.AdvFrame.FraOriginMain.FraOrigin1.FraLittX.EnglobingFrame.IntermediateFrame.Spinner.Title="X";
FraSweep.FraOptional.NotifFrame.AdvFrame.FraOriginMain.FraOrigin1.LabelLittY.Title="Y: ";
FraSweep.FraOptional.NotifFrame.AdvFrame.FraOriginMain.FraOrigin1.FraLittY.EnglobingFrame.IntermediateFrame.Spinner.Title="Y";

FraSweep.FraOptional.NotifFrame.AdvFrame.FraOriginMain.RadBOrigin2.Title="원점 선택";
FraSweep.FraOptional.NotifFrame.AdvFrame.FraOriginMain.RadBOrigin2.LongHelp="점을 선택하여 첫 번째 sweep 평면에서
위치 지정 좌표계의 원점을 지정합니다. ";
FraSweep.FraOptional.NotifFrame.AdvFrame.FraOriginMain.FraOrigin2.LabOriginP1.Title="점:              ";


FraSweep.FraOptional.NotifFrame.AdvFrame.SepAngleDir1.TitleLabel.Title="첫 번째 sweep 평면의 축";

FraSweep.FraOptional.NotifFrame.AdvFrame.FraDirection.RadBDirection1.Title="회전 각도";
FraSweep.FraOptional.NotifFrame.AdvFrame.FraDirection.RadBDirection1.LongHelp="원점 주위에서
초기 위치로부터 위치 지정 좌표계를 회전합니다.";
FraSweep.FraOptional.NotifFrame.AdvFrame.FraDirection.FraDirection1.FraLittAngle.EnglobingFrame.IntermediateFrame.Spinner.Title="각도";

FraSweep.FraOptional.NotifFrame.AdvFrame.FraDirection.RadBDirection2.Title="첫 번째 축 선택";
FraSweep.FraOptional.NotifFrame.AdvFrame.FraDirection.RadBDirection2.LongHelp="선 선택 또는 컴포넌트를 사용하여
위치 지정 좌표계의 X 축을 지정합니다. ";
FraSweep.FraOptional.NotifFrame.AdvFrame.FraDirection.FraDirection2.LabDirection.Title="방향: ";
FraSweep.FraOptional.NotifFrame.AdvFrame.FraDirection.FraDirection2.LabDirection.LongHelp="선 선택 또는 컴포넌트를 사용하여
위치 지정 좌표계의 X 축을 지정합니다. ";

FraSweep.FraOptional.NotifFrame.AdvFrame.SwapXAxisButtonV5.Title="X 축 반전됨";
FraSweep.FraOptional.NotifFrame.AdvFrame.SwapXAxisButtonV5.LongHelp="X축 방향을 반전합니다.";
FraSweep.FraOptional.NotifFrame.AdvFrame.SwapYAxisButtonV5.Title="Y 축 반전됨";
FraSweep.FraOptional.NotifFrame.AdvFrame.SwapYAxisButtonV5.LongHelp="Y축 방향을 반전합니다.";

FraSweep.FraOptional.NotifFrame.AdvFrame.SwapXAxisButtonBirailV5.Title="프로파일 극한 반전됨";
FraSweep.FraOptional.NotifFrame.AdvFrame.SwapXAxisButtonBirailV5.LongHelp="가이드에 연결된 프로파일 극한을 반전시킵니다.";
FraSweep.FraOptional.NotifFrame.AdvFrame.SwapYAxisButtonBirailV5.Title="수직 방향 반전됨";
FraSweep.FraOptional.NotifFrame.AdvFrame.SwapYAxisButtonBirailV5.LongHelp="프로파일의 수직 방향을 반전합니다.";

FraSweep.FraOptional.NotifFrame.AdvFrame.SwapXAxisButton.Title="X 축 선택";
FraSweep.FraOptional.NotifFrame.AdvFrame.SwapXAxisButton.LongHelp="X 축 방향을 반전합니다.";
FraSweep.FraOptional.NotifFrame.AdvFrame.SwapYAxisButton.Title="Y 축 반전됨";
FraSweep.FraOptional.NotifFrame.AdvFrame.SwapYAxisButton.LongHelp="Y 축 방향을 반전합니다.";

FraSweep.FraOptional.NotifFrame.AdvFrame.SwapXAxisButtonBirail.Title="프로파일 극한 반전됨";
FraSweep.FraOptional.NotifFrame.AdvFrame.SwapXAxisButtonBirail.LongHelp="가이드에 연결된 프로파일 극한을 반전시킵니다.";
FraSweep.FraOptional.NotifFrame.AdvFrame.SwapYAxisButtonBirail.Title="수직 방향 반전됨";
FraSweep.FraOptional.NotifFrame.AdvFrame.SwapYAxisButtonBirail.LongHelp="프로파일의 수직 방향을 반전합니다.";


FraSweep.FraOptional.NotifFrame.AdvFrame.SepSdAxisSyst2.TitleLabel.Title="프로파일의 고정 요소";

FraSweep.FraOptional.NotifFrame.AdvFrame.FraOriginSd.LabOriginP2.Title="점:              ";
FraSweep.FraOptional.NotifFrame.AdvFrame.FraOriginSd.LabOriginP2.LongHelp="프로파일에서 좌표계의 원점을 지정합니다.";
FraSweep.FraOptional.NotifFrame.AdvFrame.FraOriginSd.SelOriginP2.LongHelp="프로파일에서 좌표계의 원점을 지정합니다.";

FraSweep.FraOptional.NotifFrame.AdvFrame.FraOriginSd.LabDirection2.Title="X축 방향: ";
FraSweep.FraOptional.NotifFrame.AdvFrame.FraOriginSd.LabDirection2.LongHelp="프로파일에서 좌표계의 X축 방향을 지정합니다.";
FraSweep.FraOptional.NotifFrame.AdvFrame.FraOriginSd.FraDirection.LongHelp="프로파일에서 좌표계의 원점을 지정합니다.";



// Sweep segment
//---------------------------------------------------------------
FraSweep.FraLayout1.FraComboLine.LabelComboLine.Title="하위 유형: ";
FraSweep.FraLayout1.FraComboLine.LabelComboLine.LongHelp="선형 프로파일을 사용하여 Sweep 서피스 하위 유형을 나열합니다.";
FraSweep.FraLayout1.FraComboLine.ComboLine.LongHelp="선형 프로파일을 사용하여 Sweep 서피스 하위 유형을 나열합니다.";
FraSweep.FraLayout1.FraMandatory.FraLayout2.FraDirection.LongHelp="해당 방향이 방향을 판별하는 선이나 \n해당 수직이 방향을 판별하는 면을 지정합니다. \n또한 컨텍스처 메뉴를 사용하여 방향의 \nX, Y, Z 컴포넌트를 지정할 수도 있습니다.";

SweepLine1="두 개의 한계";
SweepLine2="한계 및 중간";
SweepLine3="레퍼런스 서피스 사용";
SweepLine4="레퍼런스 커브 사용";
SweepLine5="접촉 서피스 사용";
SweepLine6="도면 방향 사용";
SweepLine7="두 개의 접촉 서피스 사용";

// Good for sweep circle
FraSweep.FraLayout1.FraMandatory.Title="필수 요소";

FraSweep.FraLayout1.FraMandatory.FraLayout2.LabGuide.Title="가이드 : ";
FraSweep.FraLayout1.FraMandatory.FraLayout2.LabGuide.LongHelp="가이드를 지정합니다.";
FraSweep.FraLayout1.FraMandatory.FraLayout2.Guide.LongHelp="가이드를 지정합니다.";

FraSweep.FraLayout1.FraMandatory.FraLayout2.LabGuideCurve1.Title="가이드 커브 1: ";
FraSweep.FraLayout1.FraMandatory.FraLayout2.LabGuideCurve1.LongHelp="첫 번째 가이드 커브를 지정합니다.";
FraSweep.FraLayout1.FraMandatory.FraLayout2.GuideCurve1.LongHelp="첫 번째 가이드 커브를 지정합니다.";

FraSweep.FraLayout1.FraMandatory.FraLayout2.LabLimitCurve.Title="한계 커브 : ";
FraSweep.FraLayout1.FraMandatory.FraLayout2.LabLimitCurve.LongHelp="한계 커브를 정의합니다.";
FraSweep.FraLayout1.FraMandatory.FraLayout2.LimitCurve.LongHelp="한계 커브를 정의합니다.";

FraSweep.FraLayout1.FraMandatory.FraLayout2.LabGuideCurve2.Title="가이드 커브 2: ";
FraSweep.FraLayout1.FraMandatory.FraLayout2.LabGuideCurve2.LongHelp="두 번째 가이드 커브를 지정합니다.";
FraSweep.FraLayout1.FraMandatory.FraLayout2.GuideCurve2.LongHelp="두 번째 가이드 커브를 지정합니다.";

FraSweep.FraLayout1.FraMandatory.FraLayout2.LabCrvRef.Title="레퍼런스 커브: ";
FraSweep.FraLayout1.FraMandatory.FraLayout2.LabCrvRef.LongHelp="각도를 정의하는 데 사용하는 레퍼런스 커브를 지정합니다.";
FraSweep.FraLayout1.FraMandatory.FraLayout2.CrvRef.LongHelp="각도를 정의하는 데 사용하는 레퍼런스 커브를 지정합니다.";
FraSweep.FraLayout1.FraMandatory.FraLayout2.LabSurfRef.Title="레퍼런스 서피스: ";
FraSweep.FraLayout1.FraMandatory.FraLayout2.LabSurfRef.LongHelp="각도를 정의하는 데 사용하는 레퍼런스 서피스를 지정합니다.";
FraSweep.FraLayout1.FraMandatory.FraLayout2.SurfRef.LongHelp="각도를 정의하는 데 사용하는 레퍼런스 서피스를 지정합니다.";

FraSweep.FraLayout1.FraMandatory.FraLayout2.LabSurfTan.Title="접촉 서피스: ";
FraSweep.FraLayout1.FraMandatory.FraLayout2.LabSurfTan.LongHelp="결과가 접하는 서피스를 지정합니다.";
FraSweep.FraLayout1.FraMandatory.FraLayout2.SurfTan.LongHelp="결과가 접하는 서피스를 지정합니다.";

FraSweep.FraLayout1.FraMandatory.FraLayout2.LabDirection.Title="도면 방향: ";
FraSweep.FraLayout1.FraMandatory.FraLayout2.LabDirection.LongHelp="도면 방향을 지정합니다(선, 평면 또는 좌표).";

FraSweep.FraLayout1.FraMandatory.FrameDraftMode.LabDraftMode.Title="도면 계산 모드: ";
FraSweep.FraLayout1.FraMandatory.FrameDraftMode.LabDraftMode.LongHelp="도면 계산 모드를 지정합니다. ";
FraSweep.FraLayout1.FraMandatory.FrameDraftMode.RadBModeSquare.Title="정사각형";
FraSweep.FraLayout1.FraMandatory.FrameDraftMode.RadBModeSquare.LongHelp="도면에 대해 정사각형 계산 모드를 활성화합니다. ";
FraSweep.FraLayout1.FraMandatory.FrameDraftMode.RadBModeCone.Title="원추";
FraSweep.FraLayout1.FraMandatory.FrameDraftMode.RadBModeCone.LongHelp="도면에 대해 원추 계산 모드를 활성화합니다. ";

FraSweep.FraLayout1.FraMandatory.FraLittLayout0.LabRefAngle.Title="각도: ";
FraSweep.FraLayout1.FraMandatory.FraLittLayout0.FraRefAngle.EnglobingFrame.IntermediateFrame.Spinner.Title="각도";
FraSweep.FraLayout1.FraMandatory.FraLittLayout0.LawButton.Title="법칙...";

FraSweep.FraLayout1.FraMandatory.FraLittLayout1.LabLength1.Title="길이 1: ";
FraSweep.FraLayout1.FraMandatory.FraLittLayout1.FraLength1.EnglobingFrame.IntermediateFrame.Spinner.Title="길이 1";
FraSweep.FraLayout1.FraMandatory.FraLittLayout1.LawButton1.Title="법칙...";
FraSweep.FraLayout1.FraMandatory.FraLittLayout1.LabLength2.Title="길이 2: ";
FraSweep.FraLayout1.FraMandatory.FraLittLayout1.FraLength2.EnglobingFrame.IntermediateFrame.Spinner.Title="길이 2";
FraSweep.FraLayout1.FraMandatory.FraLittLayout1.LawButton2.Title="법칙...";

FraSweep.FraLayout1.FraMandatory.FraLayout2.LabSpine.Title="스파인: ";
FraSweep.FraLayout1.FraMandatory.FraLayout2.LabSpine.LongHelp="스파인 커브를 지정합니다.";
FraSweep.FraLayout1.FraMandatory.FraLayout2.Spine.LongHelp="스파인 커브를 지정합니다.";

FraSweep.FraLayout1.FraMandatory.FraLayout2.LabSurfTan1.Title="첫 번째 접촉 서피스: ";
FraSweep.FraLayout1.FraMandatory.FraLayout2.LabSurfTan1.LongHelp="결과가 접하는 첫 번째 서피스를 지정합니다.";
FraSweep.FraLayout1.FraMandatory.FraLayout2.SurfTan1.LongHelp="결과가 접하는 첫 번째 서피스를 지정합니다.";

FraSweep.FraLayout1.FraMandatory.FraLayout2.LabSurfTan2.Title="두 번째 접촉 서피스: ";
FraSweep.FraLayout1.FraMandatory.FraLayout2.LabSurfTan2.LongHelp="결과가 접하는 두 번째 서피스를 지정합니다.";
FraSweep.FraLayout1.FraMandatory.FraLayout2.SurfTan2.LongHelp="결과가 접하는 두 번째 서피스를 지정합니다.";

// Good for sweep circle
FraSweep.FraOptional.Title="선택적 요소";

FraSweep.FraOptional.FraSpineLayout.LabSpine.Title="스파인: ";
FraSweep.FraOptional.FraSpineLayout.LabSpine.LongHelp="첫 번째 가이드 커브와 다른 스파인을 사용하려면
스파인 커브를 지정합니다. ";
FraSweep.FraOptional.FraSpineLayout.Spine.LongHelp="첫 번째 가이드 커브와 다른 스파인을 사용하려면
스파인 커브를 지정합니다. ";

FraSweep.FraOptional.FraSpineLayout.LabRelimiter1.Title="  한계 재설정기 1: ";
FraSweep.FraOptional.FraSpineLayout.LabRelimiter1.LongHelp="스파인 한계를 재설정(즉 sweep)하기 위해 사용되는
첫 번째 요소(점 또는 평면)를 지정합니다.";
FraSweep.FraOptional.FraSpineLayout.Relimiter1.LongHelp="스파인 한계를 재설정(즉 sweep)하기 위해 사용되는
첫 번째 요소(점 또는 평면)를 지정합니다.";

FraSweep.FraOptional.FraSpineLayout.LabRelimiter2.Title="  한계 재설정기 2: ";
FraSweep.FraOptional.FraSpineLayout.LabRelimiter2.LongHelp="스파인 한계를 재설정(즉 sweep)하기 위해 사용되는
두 번째 요소(점 또는 평면)를 지정합니다.";
FraSweep.FraOptional.FraSpineLayout.Relimiter2.LongHelp="스파인 한계를 재설정(즉 sweep)하기 위해 사용되는
두 번째 요소(점 또는 평면)를 지정합니다.";

FraSweep.FraOptional.SegmentButton.Title="두 번째 커브를 중간 커브로";
FraSweep.FraOptional.SegmentButton.LongHelp="두 번째 가이드 커브를 중간 커브로 사용합니다.
각각의 sweep 평면에서 sweep 서피스 폭은
가이드 커브와 중간 커브 사이 거리의 두 배입니다. ";

FraSweep.FraOptional.TrimOptionButton.Title="접촉 서피스로 자르기";
FraSweep.FraOptional.TrimOptionButton.LongHelp="접촉 서피스를 사용하여 sweep 서피스를 자릅니다.";

FraSweep.FraOptional.TrimOptionButton1.Title="첫 번째 접촉 서피스로 자르기";
FraSweep.FraOptional.TrimOptionButton1.LongHelp="첫 번째 접촉 서피스를 사용하여 sweep 서피스를 자릅니다.";

FraSweep.FraOptional.TrimOptionButton2.Title="두 번째 접촉 서피스로 자르기";
FraSweep.FraOptional.TrimOptionButton2.LongHelp="두 번째 접촉 서피스를 사용하여 sweep 서피스를 자릅니다.";

FraSweep.FraOptional.FraLittLayout2.SolutionSelectorFrame.EspaceLabel1.Title="솔루션: ";
FraSweep.FraOptional.SolutionSelectorFrame.EspaceLabel1.Title="솔루션: ";
FraSweep.FraOptional.SolutionSelectorFrame.EspaceLabel1.LongHelp="솔루션 세트를 찾아볼 수 있습니다.";

FraSweep.FraOptional.FraLittLayout1.LabLength1.Title="길이 1: ";
FraSweep.FraOptional.FraLittLayout1.FraLength1.EnglobingFrame.IntermediateFrame.Spinner.Title="길이 1";
FraSweep.FraOptional.FraLittLayout1.LawButton1.Title="법칙...";
FraSweep.FraOptional.FraLittLayout1.LabLength2.Title="길이 2: ";
FraSweep.FraOptional.FraLittLayout1.FraLength2.EnglobingFrame.IntermediateFrame.Spinner.Title="길이 2";
FraSweep.FraOptional.FraLittLayout1.LawButton2.Title="법칙...";

FraSweep.FraOptional.FraLittLayout2.LabLength2.Title="길이 2: ";
FraSweep.FraOptional.FraLittLayout2.FraLength2.EnglobingFrame.IntermediateFrame.Spinner.Title="길이 2";

FraSweep.FraOptional.FraLittLayout1.LabLengthElem1.Title="한계 재설정 요소 1: ";
FraSweep.FraOptional.FraLittLayout1.LabLengthElem1.LongHelp="길이 1을 교체하기 위해 sweep 서피스 한계 재설정을 정의하는 점 또는 평면을 지정합니다.";
FraSweep.FraOptional.FraLittLayout1.LengthElem1.LongHelp="길이 1을 교체하기 위해 sweep 서피스 한계 재설정을 정의하는 점 또는 평면을 지정합니다.";
FraSweep.FraOptional.FraLittLayout2.LabLengthElem2.Title="한계 재설정 요소 2: ";
FraSweep.FraOptional.FraLittLayout2.LabLengthElem2.LongHelp="길이 2를 교체하기 위해 sweep 서피스 한계 재설정을 정의하는 점 또는 평면을 지정합니다.";
FraSweep.FraOptional.FraLittLayout2.LengthElem2.LongHelp="길이 2를 교체하기 위해 sweep 서피스 한계 재설정을 정의하는 점 또는 평면을 지정합니다.";

FraSweep.FraOptional.SolutionSelectorFrame.CurrentEditor.Title="솔루션 ";
FraSweep.FraOptional.SolutionSelectorFrame.CurrentEditor.LongHelp="현재 솔루션 번호를 보고 입력할 수 있습니다.";

FraSweep.FraOptional.OngletsDraftAngle.OngletCst.Title="전체적으로 정의";
FraSweep.FraOptional.OngletsDraftAngle.OngletCst.LongHelp="전체 sweep 작업에 대해 도면 각도 법칙을 지정할 수 있습니다. ";
FraSweep.FraOptional.OngletsDraftAngle.OngletCst.FraLittLayout0.LabRefAngle.Title="각도: ";
FraSweep.FraOptional.OngletsDraftAngle.OngletCst.FraLittLayout0.FraRefAngle.EnglobingFrame.IntermediateFrame.Spinner.Title="각도";
FraSweep.FraOptional.OngletsDraftAngle.OngletCst.FraLittLayout0.LawButton.Title="법칙...";

FraSweep.FraOptional.OngletsDraftAngle.OngletCstG1.Title="G1-상수";
FraSweep.FraOptional.OngletsDraftAngle.OngletCstG1.LongHelp="접촉에서 가이드 커브 연속 부분의 도면 각도 값을 지정할 수 있습니다. ";
FraSweep.FraOptional.OngletsDraftAngle.OngletCstG1.FraG1Angle.LabG1Angle.Title="현재 각도: ";
FraSweep.FraOptional.OngletsDraftAngle.OngletCstG1.FraG1Angle.LabG1Angle.LongHelp="가이드 커브의 현재 G1 부분에 대한 도면 각도를 지정합니다. ";
FraSweep.FraOptional.OngletsDraftAngle.OngletCstG1.FraG1Angle.CkeFraG1Angle.LongHelp="가이드 커브의 현재 G1 부분에 대한 도면 각도를 지정합니다. ";
FraSweep.FraOptional.OngletsDraftAngle.OngletCstG1.FraG1Angle.CkeFraG1Angle.EnglobingFrame.IntermediateFrame.Spinner.Title="현재 각도";

FraSweep.FraOptional.OngletsDraftAngle.OngletVar.Title="위치 값";
FraSweep.FraOptional.OngletsDraftAngle.OngletVar.LongHelp="가이드 커브상의 지정된 점에서 도면 각도 값을 지정할 수 있습니다. ";
FraSweep.FraOptional.OngletsDraftAngle.OngletVar.FraLocValAngle.LabLocValAngle.Title="현재 각도: ";
FraSweep.FraOptional.OngletsDraftAngle.OngletVar.FraLocValAngle.LabLocValAngle.LongHelp="현재 점에서 도면 각도를 지정합니다.";
FraSweep.FraOptional.OngletsDraftAngle.OngletVar.FraLocValAngle.CkeFraLocValAngle.LongHelp="현재 점에서 도면 각도를 지정합니다.";
FraSweep.FraOptional.OngletsDraftAngle.OngletVar.FraLocValAngle.CkeFraLocValAngle.EnglobingFrame.IntermediateFrame.Spinner.Title="현재 각도";
DraftLocationTitreColonne1="위치";
DraftLocationTitreColonne2="값";
FraSweep.FraOptional.OngletsDraftAngle.OngletVar.RemoveLocBut.Title="현재 위치 제거";
FraSweep.FraOptional.OngletsDraftAngle.OngletVar.RemoveLocBut.LongHelp="선택된 각도 위치를 제거할 수 있습니다.";

FraSweep.FraOptional.FraDraftLengthBut1.LabLengthType.Title="길이 유형 1: ";
FraSweep.FraOptional.FraDraftLengthBut1.LabLengthType.LongHelp="길이 정의 유형을 지정합니다.";
FraSweep.FraOptional.FraDraftLengthBut1.RadBFromCurve.LongHelp="커브에서: sweep 서피스가 커브에서 시작합니다.";
FraSweep.FraOptional.FraDraftLengthBut1.RadBFromCurve.ShortHelp="커브에서";
FraSweep.FraOptional.FraDraftLengthBut1.RadBStandard.LongHelp="표준: 길이가 sweep 평면에서 계산됩니다(0은 '커브에서' 옵션과 같습니다.). ";
FraSweep.FraOptional.FraDraftLengthBut1.RadBStandard.ShortHelp="표준";
FraSweep.FraOptional.FraDraftLengthBut1.RadBFromUpTo.LongHelp="시작 / 끝 범위: 길이가 평면 또는 서피스를 교차하여 계산됩니다. ";
FraSweep.FraOptional.FraDraftLengthBut1.RadBFromUpTo.ShortHelp="시작 / 끝 범위";
FraSweep.FraOptional.FraDraftLengthBut1.RadBExtremum.LongHelp="시작 극값: 길이가 극값 평면으로부터 도면 방향을 따라 취해집니다. ";
FraSweep.FraOptional.FraDraftLengthBut1.RadBExtremum.ShortHelp="시작 극값";
FraSweep.FraOptional.FraDraftLengthBut1.RadBLengthAlong.LongHelp="서피스 따라: 길이가 유클리드 평행 커브 거리로 사용되어 sweep 서피스 한계를 재설정합니다. ";
FraSweep.FraOptional.FraDraftLengthBut1.RadBLengthAlong.ShortHelp="서피스 따라";

FraSweep.FraOptional.FraDraftLengthBut2.LabLengthType.Title="길이 유형 2: ";
FraSweep.FraOptional.FraDraftLengthBut2.LabLengthType.LongHelp="길이 정의 유형을 지정합니다.";
FraSweep.FraOptional.FraDraftLengthBut2.RadBFromCurve.LongHelp="커브에서: sweep 서피스가 커브에서 시작합니다.";
FraSweep.FraOptional.FraDraftLengthBut2.RadBFromCurve.ShortHelp="커브에서";
FraSweep.FraOptional.FraDraftLengthBut2.RadBStandard.LongHelp="표준: 길이가 sweep 평면에서 계산됩니다(0은 '커브에서' 옵션과 같습니다.). ";
FraSweep.FraOptional.FraDraftLengthBut2.RadBStandard.ShortHelp="표준";
FraSweep.FraOptional.FraDraftLengthBut2.RadBFromUpTo.LongHelp="시작 / 끝 범위: 길이가 평면 또는 서피스를 교차하여 계산됩니다. ";
FraSweep.FraOptional.FraDraftLengthBut2.RadBFromUpTo.ShortHelp="시작 / 끝 범위";
FraSweep.FraOptional.FraDraftLengthBut2.RadBExtremum.LongHelp="시작 극값: 길이가 극값 평면으로부터 도면 방향을 따라 취해집니다. ";
FraSweep.FraOptional.FraDraftLengthBut2.RadBExtremum.ShortHelp="시작 극값";
FraSweep.FraOptional.FraDraftLengthBut2.RadBLengthAlong.LongHelp="서피스 따라: 길이가 유클리드 평행 커브 거리로 사용되어 sweep 서피스 한계를 재설정합니다. ";
FraSweep.FraOptional.FraDraftLengthBut2.RadBLengthAlong.ShortHelp="서피스 따라";


// Sweep circle
//---------------------------------------------------------------
FraSweep.FraLayout1.FraComboCircle.LabelComboCircle.Title="하위 유형: ";
FraSweep.FraLayout1.FraComboCircle.LabelComboCircle.LongHelp="원형 프로파일을 사용하여 Sweep 서피스 하위 유형을 나열합니다.";
FraSweep.FraLayout1.FraComboCircle.ComboCircle.LongHelp="원형 프로파일을 사용하여 Sweep 서피스 하위 유형을 나열합니다.";

FraSweep.FraLayout1.FraComboCircleVol.LabelComboCircleVol.Title="하위 유형: ";
FraSweep.FraLayout1.FraComboCircleVol.LabelComboCircleVol.LongHelp="원형 프로파일을 사용하여 스위프 볼륨 하위 유형을 나열합니다.";
FraSweep.FraLayout1.FraComboCircleVol.ComboCircleVol.LongHelp="원형 프로파일을 사용하여 스위프 볼륨 하위 유형을 나열합니다.";

SweepCircle1="세 가이드";
SweepCircle2="두 가이드 및 반지름";
SweepCircle3="중심 및 두 각도";
VolumeSweepCircle3="중심 및 레퍼런스 커브";
SweepCircle4="중심 및 반지름";
SweepCircle5="두 가이드 및 접촉 서피스";
SweepCircle6="한 가이드 및 접촉 서피스";
SweepCircleLimitCurveTangencySurface="한계 커브 및 접촉 서피스";

FraSweep.FraLayout1.FraMandatory.FraLayout2.LabGuideCurve3.Title="가이드 커브 3: ";
FraSweep.FraLayout1.FraMandatory.FraLayout2.LabGuideCurve3.LongHelp="세 번째 가이드 커브를 지정합니다.";
FraSweep.FraLayout1.FraMandatory.FraLayout2.GuideCurve3.LongHelp="세 번째 가이드 커브를 지정합니다.";

FraSweep.FraLayout1.FraMandatory.FraLayout2.LabCenterCurve.Title="중심 커브 : ";
FraSweep.FraLayout1.FraMandatory.FraLayout2.LabCenterCurve.LongHelp="중심 커브를 정의합니다.";
FraSweep.FraLayout1.FraMandatory.FraLayout2.CenterCurve.LongHelp="중심 커브를 정의합니다.";

FraSweep.FraLayout1.FraMandatory.FraLayout2.LabAngleRefCurve.Title="레퍼런스 커브: ";
FraSweep.FraLayout1.FraMandatory.FraLayout2.LabAngleRefCurve.LongHelp="레퍼런스 각도 커브를 지정합니다.";
FraSweep.FraLayout1.FraMandatory.FraLayout2.AngleRefCurve.LongHelp="레퍼런스 각도 커브를 지정합니다.";

FraSweep.FraLayout1.FraMandatory.FraLayout2.LabGuideCurveWithTangency.Title="접촉이 있는 한계 커브: ";
FraSweep.FraLayout1.FraMandatory.FraLayout2.LabGuideCurveWithTangency.LongHelp="접촉이 부과된 가이드 커브를 지정합니다.";
FraSweep.FraLayout1.FraMandatory.FraLayout2.GuideCurveWithTangency.LongHelp="접촉이 부과된 가이드 커브를 지정합니다.";

FraSweep.FraLayout1.FraMandatory.FraLayout2.LabLimitCurve.Title="한계 커브: ";
FraSweep.FraLayout1.FraMandatory.FraLayout2.LabLimitCurve.LongHelp="다른 한계 커브를 정의합니다.";
FraSweep.FraLayout1.FraMandatory.FraLayout2.LimitCurve.LongHelp="다른 한계 커브를 정의합니다.";

FraSweep.FraLayout1.FraMandatory.LimitCurveAngles.LawButton.Title="법칙...";

FraSweep.FraLayout1.FraMandatory.LimitCurveAngles.LabAngle1.Title="각도 1:";
FraSweep.FraLayout1.FraMandatory.LimitCurveAngles.LabAngle1.LongHelp="첫 번째 각도 값을 지정합니다.";
FraSweep.FraLayout1.FraMandatory.LimitCurveAngles.FraAngle1.EnglobingFrame.IntermediateFrame.Spinner.Title="각도 1";

FraSweep.FraLayout1.FraMandatory.LimitCurveAngles.LabAngle2.Title="각도 2:";
FraSweep.FraLayout1.FraMandatory.LimitCurveAngles.LabAngle2.LongHelp="두 번째 각도 값을 지정합니다.";
FraSweep.FraLayout1.FraMandatory.LimitCurveAngles.FraAngle2.EnglobingFrame.IntermediateFrame.Spinner.Title="각도 2";


FraSweep.FraLayout1.FraMandatory.FraLittLayout1.LabRadius.Title="반지름: ";
FraSweep.FraLayout1.FraMandatory.FraLittLayout1.LabRadius.LongHelp="반지름 값을 지정합니다.";
FraSweep.FraLayout1.FraMandatory.FraLittLayout1.FraRadius.EnglobingFrame.IntermediateFrame.Spinner.Title="반지름";
FraSweep.FraLayout1.FraMandatory.FraLittLayout1.LawButton.Title="법칙...";

FraSweep.FraLayout1.FraMandatory.FraLittLayout1.LabAngle1.Title="각도 1:";
FraSweep.FraLayout1.FraMandatory.FraLittLayout1.LabAngle1.LongHelp="첫 번째 각도 값을 지정합니다.";
FraSweep.FraLayout1.FraMandatory.FraLittLayout1.FraAngle1.EnglobingFrame.IntermediateFrame.Spinner.Title="각도 1";

FraSweep.FraLayout1.FraMandatory.FraLittLayout1.LabAngle2.Title="각도 2:";
FraSweep.FraLayout1.FraMandatory.FraLittLayout1.LabAngle2.LongHelp="두 번째 각도 값을 지정합니다.";
FraSweep.FraLayout1.FraMandatory.FraLittLayout1.FraAngle2.EnglobingFrame.IntermediateFrame.Spinner.Title="각도 2";

FraSweep.FraOptional.FraLittLayout2.ButOptionRadius.Title="상수 반지름 사용: ";
FraSweep.FraOptional.FraLittLayout2.ButOptionRadius.LongHelp="원 반지름을 지정할 수 있습니다.
활성화되지 않은 경우 원 반지름은 중심 및 레퍼런스 커브
사이의 거리에 해당됩니다. ";
FraSweep.FraOptional.FraLittLayout2.FraRadius.EnglobingFrame.IntermediateFrame.Spinner.Title="반지름";
FraSweep.FraOptional.FraLittLayout2.LawButton.Title="법칙...";


// Sweep conique
//---------------------------------------------------------------
FraSweep.FraLayout1.FraComboConic.LabelComboConic.Title="하위 유형: ";
FraSweep.FraLayout1.FraComboConic.LabelComboConic.LongHelp="원추 프로파일을 사용하여 Sweep 서피스 하위 유형을 나열합니다.";
FraSweep.FraLayout1.FraComboConic.ComboConic.LongHelp="원추 프로파일을 사용하여 Sweep 서피스 하위 유형을 나열합니다.";

SweepConic1="두 가이드 커브";
SweepConic2="세 가이드 커브";
SweepConic3="네 가이드 커브";
SweepConic4="다섯 가이드 커브";

FraSweep.FraLayout1.FraMandatory.FraGuide1.LabGuideCurve1.Title="가이드 커브 1: ";
FraSweep.FraLayout1.FraMandatory.FraGuide1.LabGuideCurve1.LongHelp="첫 번째 가이드 커브를 지정합니다.";
FraSweep.FraLayout1.FraMandatory.FraGuide1.GuideCurve1.LongHelp="첫 번째 가이드 커브를 지정합니다.";

FraSweep.FraLayout1.FraMandatory.FraTan1.Title="연계된 접촉 요소";
FraSweep.FraLayout1.FraMandatory.FraTan1.SubFra1.LabTangencyElement1.Title="접촉: ";
FraSweep.FraLayout1.FraMandatory.FraTan1.SubFra1.LabTangencyElement1.LongHelp="첫 번째 가이드 커브의 접촉 요소(커브 또는 서피스)를 지정합니다.";
FraSweep.FraLayout1.FraMandatory.FraTan1.SubFra1.TangencyElement1.LongHelp="첫 번째 가이드 커브의 접촉 요소(커브 또는 서피스)를 지정합니다.";
FraSweep.FraLayout1.FraMandatory.FraTan1.SubFra2.LabRefAngle1.Title="각도: ";
FraSweep.FraLayout1.FraMandatory.FraTan1.SubFra2.LabRefAngle1.LongHelp="첫 번째 가이드 커브 접촉 각도를 지정합니다.";
FraSweep.FraLayout1.FraMandatory.FraTan1.SubFra2.FraAngle1.EnglobingFrame.IntermediateFrame.Spinner.Title="Angle1";
FraSweep.FraLayout1.FraMandatory.FraTan1.SubFra2.LawButton.Title="법칙...";

FraSweep.FraLayout1.FraMandatory.FraGuideFin.LabGuideCurveFin.Title="마지막 가이드 커브: ";
FraSweep.FraLayout1.FraMandatory.FraGuideFin.LabGuideCurveFin.LongHelp="마지막 가이드 커브를 지정합니다.";
FraSweep.FraLayout1.FraMandatory.FraGuideFin.GuideCurveFin.LongHelp="마지막 가이드 커브를 지정합니다.";

FraSweep.FraLayout1.FraMandatory.FraTanFin.Title="연계된 접촉 요소";
FraSweep.FraLayout1.FraMandatory.FraTanFin.SubFra1.LabTangencyElementFin.Title="접촉: ";
FraSweep.FraLayout1.FraMandatory.FraTanFin.SubFra1.LabTangencyElementFin.LongHelp="마지막 가이드 커브의 접촉 요소(커브 또는 서피스)를 지정합니다.";
FraSweep.FraLayout1.FraMandatory.FraTanFin.SubFra1.TangencyElementFin.LongHelp="마지막 가이드 커브의 접촉 요소(커브 또는 서피스)를 지정합니다.";
FraSweep.FraLayout1.FraMandatory.FraTanFin.SubFra2.LabRefAngleFin.Title="각도: ";
FraSweep.FraLayout1.FraMandatory.FraTanFin.SubFra2.LabRefAngleFin.LongHelp="마지막 가이드 커브 접촉 각도를 지정합니다.";
FraSweep.FraLayout1.FraMandatory.FraTanFin.SubFra2.FraAngleFin.EnglobingFrame.IntermediateFrame.Spinner.Title="Angle2";
FraSweep.FraLayout1.FraMandatory.FraTanFin.SubFra2.LawButton.Title="법칙...";

FraSweep.FraLayout1.FraMandatory.FraExtra.LabGuideCurve2.Title="가이드 커브 2: ";
FraSweep.FraLayout1.FraMandatory.FraExtra.LabGuideCurve2.LongHelp="두 번째 가이드 커브를 지정합니다.";
FraSweep.FraLayout1.FraMandatory.FraExtra.GuideCurve2.LongHelp="두 번째 가이드 커브를 지정합니다.";

FraSweep.FraLayout1.FraMandatory.FraExtra.LabGuideCurve3.Title="가이드 커브 3: ";
FraSweep.FraLayout1.FraMandatory.FraExtra.LabGuideCurve3.LongHelp="세 번째 가이드 커브를 지정합니다.";
FraSweep.FraLayout1.FraMandatory.FraExtra.GuideCurve3.LongHelp="세 번째 가이드 커브를 지정합니다.";

FraSweep.FraLayout1.FraMandatory.FraExtra.LabGuideCurve4.Title="가이드 커브 4: ";
FraSweep.FraLayout1.FraMandatory.FraExtra.LabGuideCurve4.LongHelp="네 번째 가이드 커브를 지정합니다.";
FraSweep.FraLayout1.FraMandatory.FraExtra.GuideCurve4.LongHelp="네 번째 가이드 커브를 지정합니다.";

FraSweep.FraLayout1.FraMandatory.FraExtra.LabParam.Title="매개변수: ";
FraSweep.FraLayout1.FraMandatory.FraExtra.LabParam.LongHelp="원추 매개변수를 지정합니다.";
FraSweep.FraLayout1.FraMandatory.FraExtra.FraParam.EnglobingFrame.IntermediateFrame.Spinner.Title="매개변수";
FraSweep.FraLayout1.FraMandatory.FraExtra.FraParam.LongHelp="원추 매개변수를 지정합니다.";
FraSweep.FraLayout1.FraMandatory.FraExtra.LawButton.Title="법칙...";

//sweep angular sector
FraSweep.FraOptional.OngletsDraftAngle.OngletCst.FraLittLayout0.LabRefAngle.Title="각도: ";
FraSweep.FraOptional.OngletsDraftAngle.OngletCst.FraLittLayout0.LawButton.Title="법칙...";
FraSweep.FraOptional.OngletsDraftAngle.OngletCst.FraLittLayout0.SolutionSelectorFrame.EspaceLabel1.Title="각도 섹터: ";

FraSweep.FraLayout1.FraMandatory.FraLittLayoutSol.SolutionSelectorFrame.EspaceLabel1.Title="각도 섹터: ";
FraSweep.FraLayout1.FraMandatory.FraLittLayoutSol.SolutionSelectorFrame.EspaceLabel1.LongHelp="솔루션 세트를 찾아볼 수 있습니다.";
FraSweep.FraMandatory.SolutionSelectorFrame.EspaceLabel1.Title="각도 섹터: ";
FraSweep.FraMandatory.SolutionSelectorFrame.EspaceLabel1.LongHelp="솔루션 세트를 찾아볼 수 있습니다.";
FraSweep.FraOptional.OngletsDraftAngle.OngletVar.SolutionSelectorFrame.EspaceLabel1.Title="각도 섹터: ";
FraSweep.FraOptional.OngletsDraftAngle.OngletVar.SolutionSelectorFrame.EspaceLabel1.LongHelp="솔루션 세트를 찾아볼 수 있습니다.";
FraSweep.FraOptional.OngletsDraftAngle.OngletCstG1.SolutionSelectorFrame.EspaceLabel1.Title="각도 섹터: ";
FraSweep.FraOptional.OngletsDraftAngle.OngletCstG1.SolutionSelectorFrame.EspaceLabel1.LongHelp="솔루션 세트를 찾아볼 수 있습니다.";

//end 

//For Canonical Shape Detection Check Button Frame
FraSweep.CanonicalFrame.CanonicalCheckButton.LongHelp="정규 형상 검출 모드를 사용하려면/사용하지 않으려면 이 확인 버튼을 토글하십시오.";
FraSweep.CanonicalFrame.CanonicalCheckButton.Title="정규 형상 검출";

//For Canonical Shape Detection Check Button of Explicit Sweep Frame
FraSweep.CanonicalFrame.CanonicalCheckButtonForNonCanonical.LongHelp="비정규 결과에 대해 정규 형상 검출 모드를 사용하려면/사용하지 않으려면 이 확인 버튼을 토글하십시오.";
FraSweep.CanonicalFrame.CanonicalCheckButtonForNonCanonical.Title="비정규 결과의 정규 형상 검출";
