//=============================================================================
//                                     CNEXT - CXR1
//                          COPYRIGHT DASSAULT SYSTEMES 1997 
//-----------------------------------------------------------------------------
// FILENAME    :    CATFramePattern
// LOCATION    :    DraftingIntUI/CNext/resources/msgcatalog
// AUTHOR      :    Hichem BEN CHEIKH
// DATE        :    Janv. 01 1998
//------------------------------------------------------------------------------
// DESCRIPTION :    Resources File for NLS purpose related to Drafting WorkShop
//                  
//------------------------------------------------------------------------------
// COMMENTS    :
//------------------------------------------------------------------------------
//MODIFICATIONS     user  date      purpose
//   HISTORY        ----  ----      -------
//     01		lgk 17.12.02 Revision
//------------------------------------------------------------------------------

// * Label *
EditorMaterialTitle="도면";

Pattern.Hatching.labelNbHatching.Title="해치의 수:";

Pattern.Dotting.DottinglabelPitch.Title="피치:";
Pattern.Dotting.DottinglabelCol.Title="색상:";
Pattern.Dotting.DottinglabelZig.Title="지그재그";

Pattern.Coloring.ColoringlabelCol.Title="색상:";

Pattern.Motif.BrowseButton.Title="찾아보기... ";
Pattern.Motif.ModifAngle.Title="각도:";
Pattern.Motif.ModifScale.Title="축척:";

Pattern.None.NonelabelCol.Title="연계된 패턴이 없음";

Pattern.Hatching.labelNbHatching.LongHelp="해치의 수를 정의합니다.";
Pattern.Dotting.DottinglabelPitch.LongHelp="패턴 피치를 설정합니다.";
Pattern.Dotting.DottinglabelCol.LongHelp="패턴 색상을 설정합니다.";
Pattern.Dotting.DottinglabelZig.LongHelp="지그재그 패턴을 설정합니다.";
Pattern.Coloring.ColoringlabelCol.LongHelp="패턴 색상을 설정합니다.";
Pattern.Motif.BrowseButton.LongHelp="패턴으로 사용할 이미지를 정의하기 위한 파일을 선택할 수 있도록 합니다.";
Pattern.Motif.ModifAngle.LongHelp="이미지 각도를 설정합니다.";
Pattern.Motif.ModifScale.LongHelp="이미지 축척을 설정합니다.";
Pattern.None.NonelabelCol.LongHelp="연계된 패턴이 없음을 표시합니다.";

Parameters.PatternNameLabel.Title="이름:";
Parameters.TypePatternLabel.Title="유형:"; 

labelAngle="각도:";
labelPitch="피치:";
labelOffset="오프셋:";
labelThickness="두께:";
labelColor="색상:";
labelTxt="선 유형:";

labelAngle.LongHelp="각도를 설정합니다.";
labelPitch.LongHelp="피치를 설정합니다.";
labelOffset.LongHelp="오프셋을 설정합니다.";
labelThickness.LongHelp="두께를 설정합니다.";
labelColor.LongHelp="색상을 설정합니다.";
labelTxt.LongHelp="선 유형을 설정합니다.";

Parameters.TypePattern.ShortHelp="유형"; 
Pattern.Hatching.labelThk.Title="해치의 수";
TypeHatching="해치";
TypeDotting="도트";
TypeColoring="컬러";
TypeMotif="이미지";
TypeNone="없음";
TabTitle="패턴";

Parameters.TypePattern.LongHelp="유형을 설정합니다."; 
Pattern.Hatching.labelThk.LongHelp="해치의 수를 설정합니다.";
TypeHatching.LongHelp="해치를 설정합니다.";
TypeDotting.LongHelp="점을 설정합니다.";
TypeColoring.LongHelp="색상을 설정합니다.";
TypeNone.LongHelp="패턴을 사용하지 않습니다.";

Parameters.PatternTable.Title="...";
Parameters.PatternTable.LongHelp="패턴 테이블을 설정합니다.";
TabTitle.LongHelp="패턴을 설정합니다.";

MaterialTitle.Title="재질";
PreviewTitle.Title=" 미리보기";

HatchingPage="해치";
PatternChooser="패턴 선택기";

Material.LongHelp="재질 매개변수를 읽습니다.";
Parameters.LongHelp="매개변수를 설정합니다.";
Pattern.LongHelp="패턴을 설정합니다.";
Pattern.Hatching.LongHelp="해치를 설정합니다.";
Pattern.Dotting.LongHelp="점을 설정합니다.";
Pattern.Coloring.LongHelp="색상을 설정합니다.";
Pattern.None.LongHelp="패턴을 사용하지 않습니다.";
Preview.LongHelp="미리보기를 설정합니다.";
HatchingPage.LongHelp="해치를 설정합니다.";
PatternChooser.LongHelp="패턴 선택기를 설정합니다.";

Material.MaterialButton.Title="파트 재질 패턴 사용 다시 설정";
Material.HasNOMaterial.Title="파트에 재질 없음";

Material.MaterialButton.LongHelp="파트 재질에 대해 정의된 패턴을 사용하여 다시 설정합니다.";
