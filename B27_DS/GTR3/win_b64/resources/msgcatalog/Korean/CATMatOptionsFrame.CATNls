// COPYRIGHT DASSAULT SYSTEMES 1998
//===========================================================================
//
// CATMatOptionsFrame (English)
//
//===========================================================================
// 
//
materialFrame.HeaderFrame.Global.Title="옵션";

materialFrame.IconAndOptionsFrame.OptionsFrame.materialWarningDisplay.Title="재질에 등록 정보 추가 시 경고 표시";
materialFrame.IconAndOptionsFrame.OptionsFrame.materialWarningDisplay.Help="재질에 새 등록 정보 추가 시 경고 표시 켜기";
materialFrame.IconAndOptionsFrame.OptionsFrame.materialWarningDisplay.ShortHelp="새 등록 정보 추가 시 경고 표시 켜기";
materialFrame.IconAndOptionsFrame.OptionsFrame.materialWarningDisplay.LongHelp="적용 가능한 새 등록 정보가 재질에 추가될 때 경고가 표시됩니다.";

materialFrame.IconAndOptionsFrame.OptionsFrame.materialLinkMode.Title="재질 적용 시 기본적으로 링크 모드 사용";
materialFrame.IconAndOptionsFrame.OptionsFrame.materialLinkMode.Help="재질 적용 시 링크 모드 켜기";
materialFrame.IconAndOptionsFrame.OptionsFrame.materialLinkMode.ShortHelp="링크 모드 켜기";
materialFrame.IconAndOptionsFrame.OptionsFrame.materialLinkMode.LongHelp="프로덕트, 파트, 본체 또는 서피스에 재질을 적용할 때 링크 모드가 활성화됩니다.";

materialFrame.IconAndOptionsFrame.OptionsFrame.materialForceMode.Title="강제 실행 모드를 디폴트 상속 모드로 사용";
materialFrame.IconAndOptionsFrame.OptionsFrame.materialForceMode.Help="재질 적용 시 자동 상속 모드 켜기";
materialFrame.IconAndOptionsFrame.OptionsFrame.materialForceMode.ShortHelp="자동 모드 켜기";
materialFrame.IconAndOptionsFrame.OptionsFrame.materialForceMode.LongHelp="프로덕트, 파트, 본체 또는 서피스에 재질을 적용할 때 
자동 상속 모드가 활성화됩니다.";

materialFrame.IconAndOptionsFrame.OptionsFrame.materialSynchroMode.Title="재질 속성 수정 시 시각화 비동기화";
materialFrame.IconAndOptionsFrame.OptionsFrame.materialSynchroMode.Help="재질 수정 시 시각화 비동기화";
materialFrame.IconAndOptionsFrame.OptionsFrame.materialSynchroMode.ShortHelp="시각화 비동기화";
materialFrame.IconAndOptionsFrame.OptionsFrame.materialSynchroMode.LongHelp="재질 속성을 수정할 때 모델 시각화를 동기화하지 않습니다.";

materialFrame.IconAndOptionsFrame.OptionsFrame.catalogReadOnlyMode.Title="재질 적용 시 읽기/쓰기 모드에서 카탈로그 열기";
materialFrame.IconAndOptionsFrame.OptionsFrame.catalogReadOnlyMode.Help="재질 적용 시 읽기/쓰기에서 재질 카탈로그 열기";
materialFrame.IconAndOptionsFrame.OptionsFrame.catalogReadOnlyMode.ShortHelp="읽기/쓰기에서 카탈로그 열기";
materialFrame.IconAndOptionsFrame.OptionsFrame.catalogReadOnlyMode.LongHelp="재질을 적용할 때 읽기 전용 모드 대신 읽기/쓰기 모드에서 재질 카탈로그를 엽니다.";


materialFrame.IconAndOptionsFrame.OptionsFrame.displayBorkenLinkMode.Title="스펙 트리에 끊어진 재질 링크 표시";
materialFrame.IconAndOptionsFrame.OptionsFrame.displayBorkenLinkMode.Help="스펙 트리에 끊어진 재질 링크 표시";
materialFrame.IconAndOptionsFrame.OptionsFrame.displayBorkenLinkMode.ShortHelp="스펙 트리에 끊어진 재질 링크 표시";
materialFrame.IconAndOptionsFrame.OptionsFrame.displayBorkenLinkMode.LongHelp="재질 링크가 끊어진 경우 스펙 트리에 끊어진 재질 링크 아이콘을 표시합니다.";

parameterFrame.HeaderFrame.Global.Title="재질 매개변수";

parameterFrame.IconAndOptionsFrame.OptionsFrame.materialParameterCreation.Title="부품, 프로덕트, 본체 또는 서피스 작성 시 재질 매개변수를 작성합니다.";
parameterFrame.IconAndOptionsFrame.OptionsFrame.materialParameterCreation.Help="새 파트/본체/서비스 작성 시마다 재질 매개변수 자동 작성 켜기";
parameterFrame.IconAndOptionsFrame.OptionsFrame.materialParameterCreation.ShortHelp="재질 매개변수 자동 작성 켜기";
parameterFrame.IconAndOptionsFrame.OptionsFrame.materialParameterCreation.LongHelp="작성이 켜져 있으면 재질이 매핑되지 않은 경우에도 모든 새 파트, 새 본체 또는 새 서피스에 대해 재질 매개변수가 자동으로 작성됩니다.";

parameterFrame.IconAndOptionsFrame.OptionsFrame.materialParameterLink.Title="재질 매개변수 수정 시 카탈로그의 링크 작성";
parameterFrame.IconAndOptionsFrame.OptionsFrame.materialParameterLink.Help="매개변수가 재질을 카탈로그의 링크로 적용";
parameterFrame.IconAndOptionsFrame.OptionsFrame.materialParameterLink.ShortHelp="매개변수가 재질을 링크로 적용";
parameterFrame.IconAndOptionsFrame.OptionsFrame.materialParameterLink.LongHelp="재질 매개변수 수정 후에 적용된 재질이 링크로 적용되는지 여부를 정의합니다.";

parameterFrame.IconAndOptionsFrame.OptionsFrame.materialParameterUpdate.Title="PowerCopy 이후 재질 매개변수와 적용된 재질 동기화";
parameterFrame.IconAndOptionsFrame.OptionsFrame.materialParameterUpdate.Help="PowerCopy 이후 재질 매개변수와 CATPart의 적용된 재질 동기화";
parameterFrame.IconAndOptionsFrame.OptionsFrame.materialParameterUpdate.ShortHelp="PowerCopy 이후 적용된 재질과 매개변수 동기화";
parameterFrame.IconAndOptionsFrame.OptionsFrame.materialParameterUpdate.LongHelp="PowerCopy는 재질 매개변수를 수정할 수 있습니다. 갱신이 작동되면 CATPart에서 적용된 각 재질은 연계된 재질 매개변수와
동기화됩니다. 이 갱신은 프로덕트 및 파트 인스턴스의 적용된 재질에 대해 디폴트로 이미 수행되었습니다.";


pathFrame.HeaderFrame.Global.Title="디폴트 재질 카탈로그 경로";

pathFrame.IconAndOptionsFrame.OptionsFrame.materialCatalogEditor.Help="디폴트 재질 카탈로그의 경로 설정";
pathFrame.IconAndOptionsFrame.OptionsFrame.materialCatalogEditor.ShortHelp="디폴트 재질 카탈로그 경로";
pathFrame.IconAndOptionsFrame.OptionsFrame.materialCatalogEditor.LongHelp="Apply Material 명령에 사용되는 디폴트 재질 카탈로그의 경로 이름을 정의합니다.";
pathFrame.IconAndOptionsFrame.OptionsFrame.catalogPath.Title="재질 카탈로그 경로";
pathFrame.IconAndOptionsFrame.OptionsFrame.pathComment.Title="(이 경로는 재질 적용을 위해 디폴트 카탈로그 검색에 사용";
pathFrame.IconAndOptionsFrame.OptionsFrame.pathComment2.Title="및 CATStartupPath 환경 변수에 정의된 경로 대체)";


envImageFrame.HeaderFrame.Global.Title="환경 이미지 파일";

envImageFrame.IconAndOptionsFrame.OptionsFrame.envImageEditor.Help="환경 이미지에 사용되는 파일 설정";
envImageFrame.IconAndOptionsFrame.OptionsFrame.envImageEditor.ShortHelp="환경 이미지 파일 설정";
envImageFrame.IconAndOptionsFrame.OptionsFrame.envImageEditor.LongHelp="환경 맵핑에 사용되는 이미지 파일을 정의합니다.";
envImageFrame.IconAndOptionsFrame.OptionsFrame.ImagePath.Title="환경 이미지";

envImageFrame.IconAndOptionsFrame.OptionsFrame.EnvPushButton.Help="환경 이미지 생성기";
envImageFrame.IconAndOptionsFrame.OptionsFrame.EnvPushButton.ShortHelp="환경 이미지 생성기";
envImageFrame.IconAndOptionsFrame.OptionsFrame.EnvPushButton.LongHelp="환경 이미지 생성기입니다.";
