//=============================================================================
//                                     CNEXT - CXR2
//                          COPYRIGHT DASSAULT SYSTEMES 1999 
//-----------------------------------------------------------------------------
// FILENAME    :    CATDrwCoordDimTableCmd
// LOCATION    :    DraftingIntCommands/CNext/resources/msgcatalog
// AUTHOR      :    HBH
// DATE        :    Octobre 2001
//------------------------------------------------------------------------------
// DESCRIPTION :    Resources File for NLS purpose related to Interactive
//                  Drafting Commands.
//                  
//------------------------------------------------------------------------------
// COMMENTS    :
//------------------------------------------------------------------------------
//MODIFICATION     user  date          purpose
//   HISTORY       
//    01		lgk	09.10.2002 Revision
//------------------------------------------------------------------------------

Title="좌표계 및 테이블 매개변수";

_axisSystemComboFrame._axisSystemLabel.Title="좌표계: ";

_axisSystemFrame._xLabel.Title="X: ";
_axisSystemFrame._yLabel.Title="Y: ";
_axisSystemFrame._zLabel.Title="Z: ";
_axisSystemFrame._xSpinner.LongHelp="좌표계 원점의 X 좌표.";
_axisSystemFrame._ySpinner.LongHelp="좌표계 원점의 Y 좌표.";
_axisSystemFrame._zSpinner.LongHelp="좌표계 원점의 Z 좌표.";

_axisSystemFrame._angleFrame._angleLabel.Title="각도: ";
_axisSystemFrame._angleFrame._angleLabel.LongHelp="2D 좌표계 회전 각도를 정의합니다.";

_axisSystemFrame._flipFrame._flipLabel.Title="뒤집기: ";
_axisSystemFrame._flipFrame._flipHorizontalPush.ShortHelp="수평으로 뒤집기";
_axisSystemFrame._flipFrame._flipHorizontalPush.LongHelp="2D 좌표계를 수평으로 뒤집습니다.";
_axisSystemFrame._flipFrame._flipVerticalPush.ShortHelp="수직으로 뒤집기";
_axisSystemFrame._flipFrame._flipVerticalPush.LongHelp="2D 좌표계를 수직으로 뒤집습니다.";

_axisSystemFrame._axisCreationCheck.Title="레프리젠테이션 작성";
_axisSystemFrame._axisCreationCheck.LongHelp="2D 좌표계의 레프리젠테이션을 작성합니다.";

_tableContentTitle.Title="테이블 컨텐트";

_tableTitleFrame._tableTitleLabel.Title="제목: ";
_tableTitleFrame._tableTitleEditor.LongHelp="테이블 제목";

_tableFormatTitle.Title="테이블 형식";
_tableFormatFrame._InvertCheckButton.Title="테이블 교차";
_tableFormatFrame._InvertCheckButton.LongHelp="테이블의 컬럼과 선을 거꾸로 합니다. ";
_tableFormatFrame._sortFrame._sortCheck.Title="테이블 컨텐트 정렬";
_tableFormatFrame._sortFrame._sortCheck.LongHelp="지정된 기준에 따라 테이블 컬럼을 정렬합니다.";
_tableFormatFrame._sortFrame._sortPush.Title="...";
_tableFormatFrame._sortFrame._sortPush.LongHelp="지정된 기준에 따라 테이블 컬럼을 정렬합니다.";

_tableFormatFrame._splitFrame._splitCheck.Title="테이블 분할";
_tableFormatFrame._splitFrame._splitCheck.LongHelp="테이블을 몇 개의 테이블로 분할합니다.";
_tableFormatFrame._splitFrame._splitPush.Title="...";
_tableFormatFrame._splitFrame._splitPush.LongHelp="테이블을 몇 개의 테이블로 분할합니다.";

ColumnsFrame.Title2.Title="제목";
ColumnsFrame.Column.Title="컬럼";

ReferenceComboNone="없음";
ReferenceComboUpperCase="레이블 : A, B, C, ...";
ReferenceComboLowerCase="레이블 : a, b, c, ...";
ReferenceComboIndex="색인 : 1, 2, 3, ...";

ColumnsFrame.X.Title="    X    ";
ColumnsFrame.Y.Title="Y";
ColumnsFrame.Z.Title="Z";
REF="REF.";
XTITLE="    X    ";
YTITLE="Y";
ZTITLE="Z";
ColumnsFrame._ReferenceOption.Title="...";
ColumnsFrame._ReferenceOption.LongHelp="레퍼런스의 첫 번째 문자 또는
첫 번째 색인을 표시합니다.";
