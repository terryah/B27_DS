//=====================================================================================
//                                     CNEXT - CXR1
//                          COPYRIGHT DASSAULT SYSTEMES 1998 
//-------------------------------------------------------------------------------------
// FILENAME    :    DrwBG
// LOCATION    :    DraftingUI
// AUTHOR      :    fgx
// BUT         :    Workbench Drafting Background
// DATE        :    17.11.1998
//-------------------------------------------------------------------------------------
// DESCRIPTION :    
//-------------------------------------------------------------------------------------
// COMMENTS    :    
//-------------------------------------------------------------------------------------
// MODIFICATIONS     user  date        purpose
//    HISTORY        ----  ----        -------
//      00           fgx   ??.05.98    Creation
//      01           fgx   23.11.98    Nouvelle version CXR2
//      02           fgx   10.12.98    Changement de nom des toolbars background pour les records
//      03           fgx   26.02.99    Ajout tb de style
//      04           fgx   29.03.99    Nouvelles commandes sketcher
//      05           fgx   04.08.99    Nouveaux workbenches
//      06           fgx   17.11.99    Ajout des tb measure et browser
//      07           fgx   09.12.99    Ajout de tb analyze + container lines
//	  08	         lgk   15.10.02	   Revision
//=====================================================================================

// Configuration
// -------------
DrwBG.Title="도면";
DrwBG.Help = "배경 뷰용 도면 워크벤치";
DrwBG.ShortHelp = "도면";

// Toolbars
// --------
// Right
contTbSelectBG.Title="선택";
contTbDrawingBG.Title="도면";
contTbViewsBG.Title="뷰";
contTbDimensioningBG.Title="치수";
contTbGenerationBG.Title="생성";
contTbAnnotationsBG.Title="주석";
contTbDressupBG.Title="드레스업";
contTbGeometryCreationBG.Title="지오메트리 작성";
contTbGeometryEditionBG.Title="지오메트리 수정";
// Top
contTbStyleBG.Title="양식";
contTbTextPropertiesBG.Title="텍스트 등록 정보";
contTbDimensionPropertiesBG.Title="치수 등록 정보";
CATDrwMumericalPropertiesBGTlb.Title="숫자 등록 정보";
contTbGraphicPropertiesBG.Title="그래픽 등록 정보";
// Bottom
contTbUpdateBG.Title="갱신";
contTbToolsBG.Title="도구";
CATDrwVisualizationBckTlb.Title="시각화";
contTbMeasureBG.Title="측정";
contTbBrowserBG.Title="카탈로그 탐색기";
// UnDock
contTbOrientationBG.Title="위치 및 방향";
contTbPositionBG.Title="위치";
contTbAnalyzeBG.Title="분석";

// Sub-Toolbars
// ------------
contSheets.Title="시트";
contProjections.Title="프로젝션";
contSections.Title="섹션";
contDetails.Title="세부사항";
contClippings.Title="클리핑";
contBroken.Title="끊어진 뷰";
contViewWizard.Title="마법사";
contDimensions.Title="치수";
CATDrwFeatDimIcb.Title="기술 사양 치수";
CATDrwContInterruptTlb.Title="치수 편집";
contTolerancing.Title="공차";
contGenDim.Title="치수 생성";
contTexts.Title="텍스트";
contSymbols.Title="기호";
CATDrwContTableIcb.Title="테이블";
contAxis.Title="축 및 스레드";
contPoint.Title="점";
contLine.Title="선";
contCircle.Title="원 및 타원";
contProfiles.Title="프로파일";
contCurves.Title="커브";
contRelimit.Title="재제한사항";
contTransfor.Title="변형";
contCst.Title="제약조건";
CATDrwAreaFillIcb.Title="면적 채우기";
CATDrwSktAnalyseIcb.Title="2D 분석 도구";
CATDrwBOMIcb.Title="BOM";

// Sub-Menus
// ---------
// First level
contMenuViews.Title="뷰(V)";
contMenuViews.Mnemonic="V";
contMenuDrawing.Title="도면(R)";
contMenuDrawing.Mnemonic="r";
contMenuDimensioning.Title="치수(D)";
contMenuDimensioning.Mnemonic="D";
contMenuGeneration.Title="생성(N)";
contMenuGeneration.Mnemonic="n";
contMenuAnnotations.Title="주석(A)";
contMenuAnnotations.Mnemonic="A";
contMenuDressup.Title="드레스업(U)";
contMenuDressup.Mnemonic="u";
contMenuGeometryCreation.Title="지오메트리 작성(G)";
contMenuGeometryCreation.Mnemonic="G";
contMenuGeometryEdition.Title="지오메트리 수정(M)";
contMenuGeometryEdition.Mnemonic="m";
contMenuDrwGenDimTools.Title="치수 생성(T)";
contMenuDrwGenDimTools.Mnemonic="t";
contMenuDrwPositioning.Title="위치(P)";
contMenuDrwPositioning.Mnemonic="P";
contMenuDrwAnalyze.Title="분석(A)";
contMenuDrwAnalyze.Mnemonic="A";
contMenuCst.Title="제약조건(C)";
contMenuCst.Mnemonic="C";

// Second level
contMenuProjections.Title="프로젝션(P)";
contMenuProjections.Mnemonic="P";
contMenuSections.Title="섹션(S)";
contMenuSections.Mnemonic="S";
contMenuDetails.Title="세부사항(D)";
contMenuDetails.Mnemonic="D";
contMenuClippings.Title="클리핑(C)";
contMenuClippings.Mnemonic="C";
contMenuViewWizard.Title="마법사(W)";
contMenuViewWizard.Mnemonic="W";
// ---
contMenuSheets.Title="시트(S)";
contMenuSheets.Mnemonic="S";
// ---
contMenuDimensions.Title="치수(D)";
contMenuDimensions.Mnemonic="D";
CATDrwFeatDimSnu.Title="기술 사양 치수(F)";
CATDrwFeatDimSnu.Mnemonic="F";
contMenuTolerancing.Title="공차(T)";
contMenuTolerancing.Mnemonic="T";
// ---
contMenuTexts.Title="텍스트(T)";
contMenuTexts.Mnemonic="T";
contMenuSymbols.Title="기호(S)";
contMenuSymbols.Mnemonic="S";
CATDrwContTableMenuSnu.Title="테이블(A)";
CATDrwContTableMenuSnu.Mnemonic="a";
CATDrwAreaFillSnu.Title="면적 채우기";

// ---
contMenuAxis.Title="축 및 스레드(A)";
contMenuAxis.Mnemonic="A";
// ---
contMenuPoint.Title="점(P)";
contMenuPoint.Mnemonic="P";
contMenuLine.Title="선(L)";
contMenuLine.Mnemonic="L";
contMenuCircle.Title="원 및 타원(C)";
contMenuCircle.Mnemonic="C";
contMenuProfiles.Title="프로파일(F)";
contMenuProfiles.Mnemonic="f";
contMenuCurves.Title="커브(U)";
contMenuCurves.Mnemonic="u";
// ---
contMenuRelimit.Title="재제한사항(R)";
contMenuRelimit.Mnemonic="R";
contMenuTransfor.Title="변형(T)";
contMenuTransfor.Mnemonic="T";
CATDrwBOMSnu.Title="BOM";

// Third level
contMenuInterrupt.Title="치수 편집(E)";
contMenuInterrupt.Mnemonic="E";

// Contextual Sub-Menus
// --------------------
DrwPositionalLink.Title="위치 링크";
DrwOrientationLink.Title="방향 링크";
DrwViewPositioning.Title="뷰 위치 지정";
DrwDimInterruptSubMenu.Title="인터럽트";
CATDrwViewPositioningTlb.Title="뷰 위치 지정";
CATDrwGVSSnu.Title="생성 뷰 양식";
CATDrwDimensionRepresentation.Title="치수 레프리젠테이션";
CATDrwMeasureMode.Title="측정 모드";
CATDrwRepresentationMode.Title="레프리젠테이션 모드";
CATDrwAngleSector.Title="각도 섹터";
CATDrwClipBoxSnu.Title="3D 클리핑";
CATDrwPrintAreaSnu.Title="인쇄 영역";
