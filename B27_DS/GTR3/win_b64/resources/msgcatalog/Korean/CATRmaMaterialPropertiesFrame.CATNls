// COPYRIGHT DASSAULT SYSTEMES 1998
//===========================================================================
//
// CATRmaMaterialPropertiesFrame (English)
//
//===========================================================================


previewFrame.actionFrame.sizeFrame.sizeLabel.Title = "재질 크기:";
previewFrame.actionFrame.sizeFrame.sizeLabel.Help = "재질의 크기 설정";
previewFrame.actionFrame.sizeFrame.sizeLabel.LongHelp =
"재질 크기를 설정합니다.
이 값은 오브젝트에 재질을 적용할 때 재질 축척을
조정하는 데 사용됩니다.
참고: 재질 크기는 미리보기에 아무런 영향을 미치지 않습니다.";

previewFrame.actionFrame.sizeFrame.sizeEditor.Title = "미리보기 편집기";
previewFrame.actionFrame.sizeFrame.sizeEditor.Help = "재질의 크기 설정";
previewFrame.actionFrame.sizeFrame.sizeEditor.LongHelp =
"재질 크기를 설정합니다.
이 값은 오브젝트에 재질을 적용할 때 재질 축척을
조정하는 데 사용됩니다.
참고: 재질 크기는 미리보기에 아무런 영향을 미치지 않습니다.";

previewFrame.actionFrame.buttonsFrame.iconBox.Title = "미리보기 유형 변경";
previewFrame.actionFrame.buttonsFrame.iconBox.Help = "미리보기한 지원 변경";
previewFrame.actionFrame.buttonsFrame.iconBox.ShortHelp = "미리보기 변경";
previewFrame.actionFrame.buttonsFrame.iconBox.LongHelp =
"미리보기 창에서 재질이 적용되는 지원을 변경합니다.";

previewFrame.actionFrame.buttonsFrame.iconBox.planRadioButton.Title = "평면 지원";
previewFrame.actionFrame.buttonsFrame.iconBox.planRadioButton.Help = "미리보기한 지원이 평면입니다.";
previewFrame.actionFrame.buttonsFrame.iconBox.planRadioButton.ShortHelp = "평면 지원";
previewFrame.actionFrame.buttonsFrame.iconBox.planRadioButton.LongHelp =
"재질이 미리보기 창의 평면 지원에 적용되었습니다.";

previewFrame.actionFrame.buttonsFrame.iconBox.sphereRadioButton.Title = "구형 지원";
previewFrame.actionFrame.buttonsFrame.iconBox.sphereRadioButton.Help = "미리보기한 지원이 구입니다.";
previewFrame.actionFrame.buttonsFrame.iconBox.sphereRadioButton.ShortHelp = "구현 지원";
previewFrame.actionFrame.buttonsFrame.iconBox.sphereRadioButton.LongHelp =
"재질이 미리보기 창의 구에 적용되었습니다.";

previewFrame.actionFrame.buttonsFrame.iconBox.cylinderRadioButton.Title = "원통형 지원";
previewFrame.actionFrame.buttonsFrame.iconBox.cylinderRadioButton.Help = "미리보기한 지원이 실린더입니다.";
previewFrame.actionFrame.buttonsFrame.iconBox.cylinderRadioButton.ShortHelp = "원통형 지원";
previewFrame.actionFrame.buttonsFrame.iconBox.cylinderRadioButton.LongHelp =
"재질이 미리보기 창의 실린더에 적용되었습니다.";

previewFrame.actionFrame.buttonsFrame.iconBox.cubeRadioButton.Title = "상자 지원";
previewFrame.actionFrame.buttonsFrame.iconBox.cubeRadioButton.Help = "미리보기한 지원이 상자입니다.";
previewFrame.actionFrame.buttonsFrame.iconBox.cubeRadioButton.ShortHelp = "입체 지원";
previewFrame.actionFrame.buttonsFrame.iconBox.cubeRadioButton.LongHelp =
"재질이 미리보기 창의 상자에 적용되었습니다.";

previewFrame.actionFrame.buttonsFrame.iconBox.automaticRadioButton.Title = "커브 지원";
previewFrame.actionFrame.buttonsFrame.iconBox.automaticRadioButton.Help = "미리보기한 지원이 서피스입니다.";
previewFrame.actionFrame.buttonsFrame.iconBox.automaticRadioButton.ShortHelp = "커브 지원";
previewFrame.actionFrame.buttonsFrame.iconBox.automaticRadioButton.LongHelp =
"재질이 미리보기 창의 커브 패치에 적용되었습니다.";

previewFrame.actionFrame.buttonsFrame.reframeButton.Title = "전체 화면 맞춤 미리보기";
previewFrame.actionFrame.buttonsFrame.reframeButton.Help = "확대 또는 축소하여 모든 지오메트리를 미리보기 영역에 맞춤";
previewFrame.actionFrame.buttonsFrame.reframeButton.ShortHelp = "전체 화면 맞춤 미리보기";
previewFrame.actionFrame.buttonsFrame.reframeButton.LongHelp =
"확대 또는 축소하여 모든 지오메트리를 사용 가능한 공간에 맞춥니다.";

previewFrame.actionFrame.buttonsFrame.previewModeCheck.Title = "반무한선(광선) 추적";
previewFrame.actionFrame.buttonsFrame.previewModeCheck.Help = "반무한선(광선) 추적 미리보기 사용 가능";
previewFrame.actionFrame.buttonsFrame.previewModeCheck.ShortHelp = "반무한선(광선) 추적 미리보기";
previewFrame.actionFrame.buttonsFrame.previewModeCheck.LongHelp =
"반무한선(광선) 추적 미리보기를 활성화/비활성화합니다.
이 모드는 더 느리지만 굴절, 절차 텍스처 또는 충돌과 같은
효과를 볼 수 있습니다.";

previewFrame.actionFrame.synchroFrame.synchroButton.Title = "실시간 미리보기";
previewFrame.actionFrame.synchroFrame.synchroButton.Help = "실시간 미리보기 활성화";
previewFrame.actionFrame.synchroFrame.synchroButton.ShortHelp = "실시간 미리보기 활성화";
previewFrame.actionFrame.synchroFrame.synchroButton.LongHelp =
"실시간 미리보기를 활성화합니다.";

previewFrame.actionFrame.typeFrame.typeLabel.Title = "유형:";
previewFrame.actionFrame.typeFrame.typeLabel.Help = "음영처리 유형 변경";
previewFrame.actionFrame.typeFrame.typeLabel.ShortHelp = "음영처리 유형";
previewFrame.actionFrame.typeFrame.typeLabel.LongHelp =
"재질의 음영처리 유형을 변경합니다.";

previewFrame.actionFrame.typeFrame.typeCombo.Help = "음영처리 유형 변경";
previewFrame.actionFrame.typeFrame.typeCombo.ShortHelp = "음영처리 유형";
previewFrame.actionFrame.typeFrame.typeCombo.LongHelp =
"재질의 음영처리 유형을 변경합니다.";

DefaultShader = "디폴트";

CreateShaderTitle = "오류";
CreateShaderError = "음영처리를 작성할 수 없습니다.\n디폴트 음영처리로 다시 설정합니다.";
