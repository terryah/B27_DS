//=============================================================================
//                                     CNEXT - CXR1
//                          COPYRIGHT DASSAULT SYSTEMES 1999 
//-----------------------------------------------------------------------------
// FILENAME    :    DrwDeleteWarnings
// LOCATION    :    DraftingUI/CNext/resources/msgcatalog
// AUTHOR      :    mmr
// DATE        :    Jul. 05 1999
//------------------------------------------------------------------------------
// DESCRIPTION :    Resource File for NLS purpose related to Drafting Delete cmd
//                  
//------------------------------------------------------------------------------
// COMMENTS    :
//------------------------------------------------------------------------------
//MODIFICATIONS     user  date      purpose
//   HISTORY        ----  ----      -------
//------------------------------------------------------------------------------

DefaultDeleteTitle="삭제 확인";
DeleteInvalid="유효하지 않은 삭제";
CutInvalid="유효하지 않은 잘라내기";

//Drawing & Layout
//-------
DeleteDrawing="도면을 제거할 수 없습니다.";
DeleteLayout="레이아웃은 편집 중인 동안 삭제할 수 없습니다.\n레이아웃을 편집 중인 모든 창을 닫으십시오.";
CutLayout="레이아웃을 잘라낼 수 없습니다.";

//Sheet messages
//------------
DeleteSheet="마지막 시트를 제거할 수 없습니다.";

SheetDeleteUndoNotAvailableWarningTitle="시트 삭제 확인";
SheetDeleteUndoNotAvailableWarning="시트를 제거하면 실행 취소할 수 없습니다.
계속하시겠습니까?";

DetailSheetsDeleteWarning="선택한 시트가 적어도 하나의 뷰 또는 세부사항에서 인스턴스로 사용되는 세부사항을 포함합니다.
이 세부사항을 삭제하면 해당 인스턴스도 모두 삭제됩니다.
계속하시겠습니까?";

DetailSheetsNoUndoDeleteWarning="선택된 시트에 하나 이상의 뷰 또는 세부사항에서 인스턴스로 사용된 세부사항이 있습니다.
이를 삭제하면 모든 인스턴스가 삭제됩니다. 이 작업은 취소할 수 없습니다.
그래도 계속하시겠습니까?";

//View
//------
ViewWithCalloutDeleteWarning="선택한 뷰가 또다른 뷰를 정의하는 데 사용되는 적어도 하나의 발호(callout)를 포함합니다.
계속하시겠습니까?";

MainBackgroundViewDelete="기본 또는 배경 뷰를 제거할 수 없습니다.";

//Details messages
//--------------
DetailDeleteTitle="세부사항 삭제 확인";
DetailDeleteWarning=" 하나 이상의 뷰 또는 세부사항에서 인스턴스로 사용됩니다.
이 세부사항를 삭제하면 해당 인스턴스도 모두 삭제됩니다.
계속하시겠습니까?";

DetailsDeleteWarning="선택한 세부사항이 적어도 하나의 뷰 또는 세부사항에서 인스턴스로 사용됩니다.
이 세부사항을 삭제하면 해당 인스턴스도 모두 삭제됩니다.
계속하시겠습니까?";

//Datum curves
//-----------
DatumFeatureDeleteWarning="잘라내기 붙여넣기가 권장되지 않습니다.\nnurb 또는 동결된
스플라인의 경우, 원래 복사된 오브젝트를 삭제하기 전에 복사하여
붙여넣으십시오.\n계속하시겠습니까?";

//DrwDressup
//-----------
DrwDressupDelete="드레스업을 제거할 수 없습니다.";
DrwAnndDecodeLeafDelete="내부 오브젝트 삭제 금지";

//AreaFill
//-----------
AreaFillSupportGeometryDelete="면적 채우기 지원 지오메트리를 \n삭제하시겠습니까?";
