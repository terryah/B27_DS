START_BATCH="CATDMUUtility2D 시작 위치:";
STOP_BATCH="CATDMUUtility2D는 다음 지속 기간 후 중지함 : ";
ERR_PROCESSING="오류: CATDMUUtility2D 처리에 실패했습니다.";
ERR_TREATMENT="오류: 입력 문서를 처리할 수 없습니다.";
OK_TREATMENT="확인: 입력 문서가 성공적으로 처리되었습니다.";

INFO_HELP="사용법: \n
도움말 정보 검색:
 CATDMUUtility2D -h
입력 데이터 반출:
 CATDMUUtility2D -id inputId -cgm outputlocation
                [-sheet sheetName]
                [-db VPM|ENOVIAV5 -user user -spwd cryppwd -role role
                 -server srv]

Windows 예제:
   CATDMUUtility2D -h
   CATDMUUtility2D -id c:\u\input.model -cgm c:\tmp\model.cgm
Unix 예제:
   CATDMUUtility2D -h
   CATDMUUtility2D -id /u/input2/model.model -cgm c:\tmp\model.cgm -sheet Sheet.1

인수:
   -h                   : 도움말.
   -id inputId          : 처리할 입력 문서를 정의.
                          다음이 될 수 있습니다.
                          - 경로('CATDLN://'이 앞에 오는 DLName 포함)
                          - 데이터베이스 ID(VPM 또는 LCA)
                          서로 다른 문서 유형이 처리됨: 모델,
                          CATDrawing, dxf, cdd, dwg
   -db DatabaseType     : 데이터베이스 유형 :
                          VPM 데이터베이스의 경우 VPM
                          LCA 데이터베이스의 경우 ENOVIAV5
   -user user           : 데이터베이스 사용자를 정의.
   -spwd cryppwd        : 사용자의 데이터베이스 암호를 정의.
                          사용 전 암호 해독됩니다.
   -role role           : 사용자의 데이터베이스 역할을 정의.
   -server srv          : 데이터베이스 서버를 정의.
   -sheet sheetName     : 입력 데이터에서 특정 시트를 반출.

   참고: user, spwd, role 및 server 매개변수는 입력 문서에서 VPM 또는 LCA를 참조하는 경우 필요합니다.

진단:
   가능한 종료 상태 값은 다음과 같습니다.
      0   성공적 완료.
      1   다음 중 한 가지 요인에 따라 실패:
          - 라이센스가 사용 가능하지 않음
          - 인수 누락
          - 유효하지 않은 매개변수
          - 옵션 다음 매개변수 누락
          - 입력 파일 누락
          - 파일을 찾을 수 없음
          - 입력 파일을 열 수 없음
          - 잘못된 문서 유형
      2   처리 오류.
      3   부분적으로 처리.

문제점 해결:
   종료 상태 = 1 :
      표준 오류 파일에 포함된 정보를 사용하여 명령행 또는 환경을 수정하십시오.
   종료 상태 = 2:
      로컬 지원 담당자에 문의하십시오.
   종료 상태 = 3:
      문제점을 발생시킨 파일을 수정하십시오. 파일을 대화식으로 사용하면 문제점에 대한 정보를 얻을 수 있습니다.";
