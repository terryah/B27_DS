//====================================================================
//  Messages for CATIA V5 Batch
//  US Version
//====================================================================
ErrorDummy="내부 오류 - 코드= /p1";
Warning="경고 : ";
Error="오류   : ";
LicenseKO1="/p1을(를) 사용할 수 없습니다.";
LicenseKO2="현재 구성에서는 이 일괄 처리의 사용을 허용하지 않습니다.";
//====================================================================
// Directory
//====================================================================

Directory.NotExist="\"/p1\" 디렉토리가 없습니다.";
Directory.NoWritePermission="\"/p1\" 디렉토리가 쓰기 디렉토리와 일치하지 않습니다.
          액세스 권한을 변경하십시오.";
Directory.NoReadPermission="\"/p1\" 디렉토리가 읽기 디렉토리와 일치하지 않습니다.
          액세스 권한을 변경하십시오.";
Directory.Undefined="\"/p1\" 디렉토리가 정의되지 않았습니다.";
Directory.NotSelected="이를 변경하고 작업을 다시 실행해야 합니다.";
Directory.IsaFile="\"/p1\" 필드는 디렉토리가 아니라 파일입니다.";
Directory.Conflict="\"/p1\" 파일과 \"-id /p2 \" 디렉토리 사이에 충돌이 있습니다.";

//====================================================================
// Document
//====================================================================
Document.NoDomain="문서 유형에 도달할 수 없습니다. 사이트에서 선택할 문서가 없습니다. ... 환경을 확인하십시오.
일괄처리가 중단되었습니다.";
Document.NoDocTyp="정의된 문서 유형이 없습니다.
선택한 문서를 문서 유형에 첨부할 수 없습니다.
사이트에서 유효한 문서 유형은 /p1입니다.
일괄처리가 중단되었습니다.";
Document.NoDocEnv="현재 환경에서 오브젝트를 선택할 수 없습니다. 라이센스를 확인하십시오.
				";

Document.NotADoc="\"/p1\" 문서는 CATIA V5 문서가 아닙니다.";
Document.NotACatiaDoc="다음 문서 목록은 어플리케이션에서 지원하지 않습니다.";
Document.DocNotFound="\"/p1\" 문서가 없거나 찾을 수 없습니다.";
Document.NoReadPermission="/p1 문서에 대한 읽기 액세스가 없습니다.";
Document.AlreadySelected="\"/p1\" 문서가 이미 선택되었습니다.";
Document.BadExtension="\"/p1\" 문서는 유효하지 않은 문서 유형입니다.";
Document.GoodExtension="\t  지원되는 문서 유형은 다음과 같습니다. :";
Document.ProcessKo="문서를 처리할 수 없습니다. ....";
Document.ProcessoK="문서가 처리되었습니다. ....";
Document.NoWritePermission="/p1 문서에 대한 쓰기 액세스가 없습니다.";
Document.InUse="다른 어플리케이션에서 \"/p1\" 문서를 이미 사용하고 있습니다.";
Document.NoDoc="선택 기준에 일치하는 문서가 없습니다.";
Document.ReadError="\"/p1\" 문서를 읽을 수 없습니다.";
Document.WriteError="\"/p1\" 문서에 쓸 수 없습니다.";
Document.Write="\"/p1\" 문서가 기록되었습니다.";
Document.NotModified="작업으로 \"/p1\" 문서가 수정되지 않았습니다.";
Document.AlreadyExist="\"/p1\" 문서가 이미 있으며 겹쳐쓸 수 없습니다.
문서를 이동하거나 교체 옵션을 선택하십시오.";
Document.Overwritten="\"/p1\" 문서가 이미 있으므로 겹쳐쓰였습니다.";
Document.CreationError="\"/p1\" 문서를 작성할 수 없습니다.
문서 이름에 지원되지 않는 문자가 들어 있는지 확인하십시오.";
/////////////////////////////////////////////////////////////////////////////////
// New documents ////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
Document.NewDocNoRefOverwrite="새 문서 \"/p1\"은(는) 문서의 레퍼런스를 겹쳐쓸 수 없습니다.
대상 디렉토리를 변경하거나 새 문서의 이름을 바꾸십시오.";
Document.NewDocAlreadyExist="새 문서 \"/p1\"이(가) 이미 대상 디렉토리에 있으며 이 문서를 겹쳐쓸 수 없습니다.
       대상 디렉토리를 변경하거나
       새 문서의 이름을 변경하거나
       교체 옵션을 선택하거나
       이전 문서의 이름을 바꾸거나 다른 디렉토리로 이동하십시오.";

Document.NewDocNoWritePermission="새 문서 /p1을(를) 대상 디렉토리에 작성할 수 없습니다.
 	대상 디렉토리에 대한 쓰기 권한을 확인하십시오.";
Document.NewDocOverwritten="새 문서 \"/p1\"이(가) 이미 있으므로 겹쳐쓰였습니다.";
Document.NewDocBackuped="문서의 이전 버전이 \"/p1\"에 백업되었습니다.";
Document.NewDocBackupAbort="문서의 이전 버전을 \"/p1\"에 백업할 수 없습니다.";
Document.NewDocPowerCopied="수정되지 않은 \"/p1\" 문서에 대해 powercopy가 수행되었습니다.";
Document.NewDocWrite="새 문서 \"/p1\"을(를) 작성했습니다.";
Document.NewDocWriteError="새 문서 \"/p1\"을(를) 작성할 수 없습니다.";
// ----------------------------------------------------------------------------
// Messages for Input File  ---------------------------------------------------
// ----------------------------------------------------------------------------
InputFile.Inexistant="경고 : 입력 파일 \"/p1\"이(가) 없습니다.";
InputFile.NoReadAccess="경고 : 입력 파일 /p1에 대한 읽기 액세스가 없습니다.
\t  파일을 열 수 없습니다.";

// ----------------------------------------------------------------------------
// Messages for Parameters ----------------------------------------------------
// ----------------------------------------------------------------------------
InvalidParameter.Information="유효하지 않은 매개변수를 변경하고 작업을 다시 실행해야 합니다.";
Parameter.Invalid="경고 : \"/p1\" 매개변수가 유효하지 않습니다.";
Parameter.Doubly="\t  이미 정의된 매개변수 또는 올바르지 않은 조합";
Parameter.Unknown="경고 : \"/p1\"은(는) 알 수 없는 매개변수입니다. \t  알려진 매개변수 목록을 참고하려면 '-h' 옵션을 사용하십시오.";
Parameter.Skipped="\t  건너뛴 명령문";
Parameter.Missing="오류   : \"/p1\" 매개변수가 누락되었습니다. \t  필수 매개변수가 누락되었거나 잘못 통보되었습니다. \t  필수 매개변수 목록을 참고하려면 '-h' 옵션을 사용하십시오.";

//====================================================================
// Messages for ReportFile ----------------------------------------------------
//====================================================================
ReportFile.Exist="경고 : 보고서 파일 \"/p1\"이(가) 이미 있으며 이를 겹쳐쓰게 됩니다.";
ReportFile.NoWriteAccess="경고 : 보고서 파일 \"/p1\"이(가) 이미 존재하지만 읽기 전용입니다. \t  보고서는 표준 출력에 기록됩니다.";
ReportFile.NoDir="경고 : 보고서 파일 \"/p1\"을(를) 작성할 수 없습니다. \t  /p2 디렉토리가 없습니다. \t  생략한 명령문 - 보고서는 표준 출력에 기록됩니다.";
ReportFile.DirNoWriteAccess="경고 : 보고서 파일 \"/p1\"을(를) 작성할 수 없습니다. \t  /p2 디렉토리가 있지만 읽기 전용입니다.
\t  건너뛴 명령문 - 보고서는 표준 출력에 기록됩니다.";
ReportFile.CurDirNoWriteAccess="경고 : 보고서 파일 \"/p1\"을(를) 작성할 수 없습니다.
\t  현재 디렉토리가 있지만 읽기 전용입니다.
\t  건너뛴 명령문 - 보고서는 표준 출력에 기록됩니다.";
//====================================================================
// Macro
//====================================================================
Macro.IsaDir="\"/p1\" 필드는 매크로가 아니라 디렉토리입니다.";
Macro.MacroNotFound="\"/p1\" 매크로가 없거나 찾을 수 없습니다.";
Macro.NoReadPermission="/p1 매크로에 대한 읽기 액세스가 없습니다.";
Macro.MacroReadError="\"/p1\" 매크로를 읽을 수 없습니다.";
Macro.BadExtension="\"/p1\" 매크로는 유효하지 않은 매크로 유형입니다.";
Macro.InUse="다른 어플리케이션에서 \"/p1\" 매크로를 이미 사용하고 있습니다.";
Macro.AlreadySelected="매크로가 이미 정의되었습니다.
이전에 정의된 \"/p1\" 매크로 선택이 생략됩니다.";
Macro.NotSelected="매크로를 선택할 수 없습니다.....";
Macro.Successful="매크로 : 실행 성공";
Macro.Aborted="매크로 : 실행 중단됨 RC=/p1";
Macro.NoSelection="선택한 매크로가 없습니다.";

//====================================================================
// XmlFile
//====================================================================
XmlFile.IsaDir="\"/p1\" 필드는 XmlFile이 아니라 디렉토리입니다.";
XmlFile.XmlFileNotFound="XmlFile \"/p1\"이(가) 없거나 찾을 수 없습니다.";
XmlFile.NoReadPermission="/p1 XmlFile에 대한 읽기 액세스가 없습니다.";
XmlFile.NoWritePermission="/p1 XmlFile에 대한 쓰기 액세스가 없습니다.";
XmlFile.XmlFileReadError="XmlFile \"/p1\"을(를) 읽을 수 없습니다.";
XmlFile.XmlFileWriteError="XmlFile \"/p1\"에 쓸 수 없습니다.";
XmlFile.BadExtension="\"/p1\" XmlFile은 유효하지 않은 XmlFile 유형입니다.";
XmlFile.InUse="다른 어플리케이션에서 \"/p1\" XmlFile을 이미 사용하고 있습니다.";
XmlFile.AlreadySelected="XmlFile이 이미 정의되었습니다.
이전에 정의된 \"/p1\" XmlFile 선택이 생략됩니다.";
XmlFile.NotSelected="XmlFile을 선택할 수 없습니다.....";
XmlFile.NoSelection="선택된 XmlFile이 없습니다. ....";
XmlFile.Overwritten="\"/p1\" XmlFile이 이미 있으며 이를 겹쳐쓰게 됩니다.";


