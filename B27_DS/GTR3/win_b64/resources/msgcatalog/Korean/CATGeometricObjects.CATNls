//------------------------------------------------------
// Resource file for input geometric operators errors 
// En_EN
//================================================================
//                           INPUT
//================================================================
//----------------------------------Null or invalid input
//
GeoObInInvalidPointer="/p1 포인터는 널(null)입니다.";
//
GeoObInInvalidInput="/p1이(가) 음수입니다.";
//
GeoObInInvalidSupport="/p1의 경우는 CATPCurve 지원의 올바른 유형이 아닙니다.";
//
GeoObInInvalidInputValueSmallerThan="/p1 값은 /p2보다 작아야 합니다.";
//
GeoObInInvalidInputValueGreaterThan="/p1 값은 /p2보다 커야 합니다.";
//
GeoObInInvalidInputValueBetween="/p1 값은 /p2보다 크고 /p3보다 작아야 합니다.";
//
GeoObInInvalidInputType="/p1 오브젝트는 허용되는 유형이 아닙니다.";
//
GeoObInNotInInfiniteBox="/p1의 경우 무한 상자보다 큽니다.";
//
GeoObInTypeNotSupported="/p1은(는) /p2이어야 합니다.";
//
GeoObInEmpty="/p1 오브젝트는 /p2 오브젝트를 포함하고 있지 않습니다.";
//
GeoObInInvalidCoordinates="입력 좌표(/p1, /p2, /p3)가 이전 좌표(/p4, /p5, /p6)와 다릅니다.";
//
GeoObInNotOnSurface="입력 점(/p1, /p2, /p3)이 서피스에 충분히 가깝지 않습니다.";
//
GeoObInInvalidMacroElement="입력한 복합 지오메트리 오브젝트는 3D 컴포넌트를 여러 개 내포하고 있습니다.";
//
GeoObInImpossibleMerge="커브를 병합할 수 없습니다. 공통 영역이 충분하지 않습니다.";
//
GeoObInInvalidSurface="퇴화한 법선이 있는 서피스는 허용되지 않습니다.";
//
GeoObInCannotBeReferredTo="입력 오브젝트는 참조하는 CGM 오브젝트가 아닌, 같은 container 에 있는 CGM 오브젝트입니다.";
//
GeoObInOpenEquation="방정식이 열려 있는 동안에는 오브젝트를 수정할 수 없습니다.
      이 오브젝트를 사용하기 전에 방정식을 닫으십시오.";
//
GeoObInInvalidOrientation="방향값은 1 또는 -1이어야 합니다.";
//
GeoObInSeveralObjects="/p1 안으로 넣으려는 시도는 잘못입니다. /p3에 이미 속해 있는 /p2입니다.";
//
GeoObInNotBelongToEdgeCurve="지정한 CATCurve는 CATEdgeCurve에 속해야 합니다.
       Next 메소드로 CATEdgeCurve의 컨텐트를 확인하십시오.";
//
GeoObInSeveralInEdgeCurve=" 입력 CATCurve는 이미 CATEdgeCurve에 속해 있습니다.
       CATCurve는 하나의 CATEdgeCurve에 여러 번 들어갈 수 없습니다.
       커브를 새로 추가하기 앞서 NextThru 메소드를 사용하여 CATEdgeCurve를 확인하십시오.";
//
GeoObInCloudOfPointsNotDefined="벡터가 정의되어 있지 않은 상태에서 CloudOfPoints에 벡터값을 설정하려는 시도는 잘못입니다.";
//
//GeoObInSelfIntersectingSurface =
//    "Surface is self-intersecting. This /p1 surface cannot have /p2 /p3.";
//
GeoObInPointNotOnSurface="점(/p1,/p2,/p3)이 이 /p4 위에 없습니다.";
//
GeoObInBadOrder="인수의 순서가 잘못되었습니다. /p2 다음에 /p1이어야 합니다.";
//
GeoObInInvalidPeriodicConflict="/p1 간격 길이는 /p2 주기와 맞지 않습니다.";
//================================================================
//                           INTernal
//================================================================
// 
GeoObIntUnavailableMethod="/p2 오브젝트를 대상으로 하는 /p1 메소드는 아직 사용할 수 없습니다.";
//
GeoObIntBindingFailed="/p1 구현을 /p2 인터페이스로 바인딩하는 작업이 실패했습니다.
      딕셔너리를 확인하십시오.";
//
GeoObIntObjectCreationFailed="/p1 클래스의 오브젝트는 작성될 수 없습니다.";
//
GeoObIntMethodCallOrder="메소드 호출 순서가 올바르지 않습니다. /p1 메소드는 /p2 다음에 호출해야 합니다.";
//
GeoObIntInvalidParameterValue="매개변수의 수치가 올바른 범위를 벗어났습니다.";
//
GeoObIntObjectClass="/p1인 오브젝트 유형 값에 맞는 클래스가 없습니다.";
//
GeoObIntShouldBeA="오브젝트 유형이 올바르지 않습니다. /p1의 경우 /p2이어야 합니다.";
//
GeoObIntMethodCall="/p1 메소드는 이 /p2 오브젝트를 대상으로 호출해서는 안됩니다.";
//
GeoObIntNotValuableParameter="커브(CATCrvParam) 또는 서피스(CATSurParam)의 매개변수가 지오메트리 오브젝트와 연계되지 않습니다.";
//
GeoObIntGetLimitsOnEmptyBox="/p1 메소드에서는 /p2이(가) 비어 있지 않다고 가정합니다.
      /p2가 비어 있지 않은지 확인하려면 IsEmpty 메소드를 사용하십시오.";
//
GeoObIntSurfaceProjectionFailed="입력 점(/p1, /p2, /p3)을 서피스에 투영할 수 없습니다.";
//
GeoObIntUnavailableResult="다중 값 입력의 결과를 사용할 수 없습니다.
      /p2 연산자에게 질의한 다중 값 입력의 결과는 이전에 SetCommand 메소드로 순서를 정하지 않았습니다.";
//
GeoObIntUnsetObject="/p1의 경우 아직 설정하지 않았습니다. 먼저 이 오브젝트를 초기화화십시오.";
//
GeoObIntBadGeometry="/p1 내부 오류는 기하학적으로 잘못된 사양 정보를 발생시킵니다.";
//
GeoObIntMath="수학 패키지에 /p1 오류 발생";
//
GeoObIntCurveToSmall="커브가 너무 작습니다.";
//
GeoObIntArcOfCurveToSmall="커브의 /p1 호 번호가 너무 작습니다.";
//
GeoObIntCurveNotC2="커브는 /p1 호의 시작에서 C2 클래스가 아닙니다.";
//
GeoObIntNullTangentCurve="글로벌 매개변수가 w=/p1이면 커브의 접점은 너무 작아집니다.";
//
GeoObIntBadParametrization="이 커브의 매개변수 지정은 커브의(curvilinear) 매개변수 지정과 너무 멀어졌습니다.";
//
GeoObIntCurvatureTooLarge="글로벌 매개변수가 w=/p1이면 커브의 곡률이 너무 큽니다.";
//
GeoObIntCanonicalCurveDefTooFar="커브의 기준점이 지오메트리 모델의 한계를 벗어났습니다.";
//
GeoObIntCanonicalCurveDegenerated="정규 커브의 양식은 퇴화되었습니다.";
//
GeoObIntControlPointsTooClose="일부 제어점은 서로 너무 가까이 있습니다.";
//
GeoObIntGapTooLarge="모서리 커브에서 큰 틈을 발견했습니다.";
//
//------------------- New
//
//  1000-1099- general
//  1100-1149- cone
//  1150-1199- torus
//  1200-1249- Cl[ou]d entities
//  1900-1999- Internal Error (general)
//
GeoObInParameterOutsideLimits_ERR_1000.Request="매개변수 값 입력:";
GeoObInParameterOutsideLimits_ERR_1000.Diagnostic="입력 매개변수가 커브 또는 서피스 최대 한계를 벗어났습니다.";
GeoObInParameterOutsideLimits_ERR_1000.Advice="- 한계 안에 있는 매개변수를 선택하십시오.";
//
GeoObInDegeneratedSurface_ERR_1001="입력 서피스는 퇴화되었습니다. \n
예를 들어, 비틀릴 수 있거나 길이 중의 하나가 예를 들면 Factory Resolution보다 작습니다. \n
허용되는 경우는 Encyclopedia를 참조하십시오. ";
//
GeoObInIncompatibleLimitAngles_ERR_1002.Request="원 작성:";
GeoObInIncompatibleLimitAngles_ERR_1002.Diagnostic="시작 각도 값은 종료 각도 값보다 커야 합니다.";
GeoObInIncompatibleLimitAngles_ERR_1002.Advice="종료 값보다 작은 시작 값을 선택하십시오.";
//
GeoObInNotSimilitude_ERR_1003.Request="지오메트리 이동:";
GeoObInNotSimilitude_ERR_1003.Diagnostic="입력 변환은 동일한 형태가 아닙니다.";
GeoObInNotSimilitude_ERR_1003.Advice="지오메트리를 변환하려면 동일한 형태를 선택하십시오.";
//
GeoObInCurveTooSmall_ERR_1004.Request="커브 사용:";
GeoObInCurveTooSmall_ERR_1004.Diagnostic="입력 커브 길이는 지오메트리의 Factory Resolution보다 작습니다.";
GeoObInCurveTooSmall_ERR_1004.Advice="- 지오메트리의 Factory Resolution보다 긴 커브를 선택하십시오.";
//
GeoObInSurfaceTooSmall_ERR_1005.Request="서피스 사용:";
GeoObInSurfaceTooSmall_ERR_1005.Diagnostic="입력 서피스 치수는 지오메트리의 Factory Resolution보다 작습니다.";
GeoObInSurfaceTooSmall_ERR_1005.Advice="- 지오메트리의 Factory Resolution보다 큰 치수로 된 서피스를 선택하십시오.";
//
//
GeoObInIncompatibleFactoryModelSize_ERR_1006.Request="모순되는 지오메트리 모델 크기 :";
GeoObInIncompatibleFactoryModelSize_ERR_1006.Diagnostic="문서에 /P1 및 /P2처럼 모순되는 모델 크기가 있습니다.";
GeoObInIncompatibleFactoryModelSize_ERR_1006.Advice="- 모순되는 지오메트리 공차로 된 문서로는 복사나 붙여넣기를 시도하지 마십시오.";
//
GeoObInIncompatibleFactoryInfinite_ERR_1007.Request="모순되는 지오메트리 무한 :";
GeoObInIncompatibleFactoryInfinite_ERR_1007.Diagnostic="문서에 /P1 및 /P2처럼 모순되는 무한이 있습니다.";
GeoObInIncompatibleFactoryInfinite_ERR_1007.Advice="- 모순되는 지오메트리 공차로 된 문서로는 복사나 붙여넣기를 시도하지 마십시오.";
//
GeoObInIncompatibleFactoryTolerances_ERR_1008.Request="모순되는 지오메트리 공차:";
GeoObInIncompatibleFactoryTolerances_ERR_1008.Diagnostic="두 문서의 지오메트리 공차는 서로 모순입니다. \n
비교한 단위는 미터로 된 /P1 및 /P2입니다. \n
연속성 해석은 단위 /P3 및 /P4입니다. \n
접점 또는 접평면 해석은 라디안으로 된 /P5 및 /P6입니다. \n
곡률 해석은 단위 /P7 및 /P8입니다. \n";
GeoObInIncompatibleFactoryTolerances_ERR_1008.Advice="- 같은 지오메트리 공차로 된 문서로 작업하십시오.";
//
//
GeoObInDownwardStreamImpossible_ERR_1009.Request="하향 호환성 없음:";
GeoObInDownwardStreamImpossible_ERR_1009.Diagnostic="현재 /P1 버전인 문서를 이전 /P2 버전으로 저장하면 /P3 때문에 손상을 입게 됩니다.";
GeoObInDownwardStreamImpossible_ERR_1009.Advice="- 현재 버전의 문서로 작업하십시오.";
//
//
GeoObInDegeneratedCurve_ERR_1010="입력 커브는 퇴화되었습니다. \n
비틀린 상태이거나 길이 중의 하나가 예를 들면 Factory Resolution보다 작습니다. \n
허용되는 경우는 Encyclopedia를 참조하십시오. ";
//
//
GeoObInIncompatibleModelSize_ERR_1011.Request="입력 기하 크기는 새 팩토리와 호환되지 않습니다. :";
GeoObInIncompatibleModelSize_ERR_1011.Diagnostic="입력 기하 크기 /P1의 경우, 문서 모델 크기 /P2 밖에 있습니다.";
GeoObInIncompatibleModelSize_ERR_1011.Advice="- 대상 문서의 모델 크기를 벗어난 지오메트리로는 복사나 붙여넣기를 시도하지 마십시오.";
//
//
GeoObInIncompatibleModelSize_ERR_1012.Request="이 유형의 무한 지오메트리를 복사할 수 없습니다.";
GeoObInIncompatibleModelSize_ERR_1012.Diagnostic="이 유형의 무한 지오메트리는 다른 모델 크기 또는 무한 팩토리 사이에서 전송할 수 없습니다.";
GeoObInIncompatibleModelSize_ERR_1012.Advice="- 호환되지 않는 팩토리 사이에서 이 유형의 무한 지오메트리를 복사 또는 붙여넣지 마십시오.";
//
//
GeoObInIncompatibleModelSize_ERR_1013.Request="새 모델 크기와 교차 없음";
GeoObInIncompatibleModelSize_ERR_1013.Diagnostic="무한 지오메트리와 새 모델 크기 /P1 상이에 교차가 없습니다.";
GeoObInIncompatibleModelSize_ERR_1013.Advice="- 새 모델 크기와 교차가 없는 무한 지오메트리를 복사하거나 붙여넣지 마십시오.";
//
GeoObInCurveTooSmall_ERR_1014.Request="커브 사용:";
GeoObInCurveTooSmall_ERR_1014.Diagnostic="입력 커브 길이가 커브 맞춤의 최대 편차보다 작습니다.";
GeoObInCurveTooSmall_ERR_1014.Advice="- 길이가 최대 편차보다 큰 커브를 선택하거나 최대 편차를 작게 설정하십시오.";
//
GeoObImpossibleCreation_ERR_1015.Request="기준 위에 타원 작성 실패:";
GeoObImpossibleCreation_ERR_1015.Diagnostic="/P1 크기는 /P2mm의 최대 기준 크기를 초과할 수 없습니다.";
GeoObImpossibleCreation_ERR_1015.Advice="V5R14 이전에 작성된 파트의 경우, 기준 크기가 증가할 수 있으므로 새 파트를 복사/붙여넣기한 후에 같은 작업을 시도하십시오.";
//
GeoObImpossibleCreation_ERR_1016.Request="기준 위에 원 작성 실패:";
GeoObImpossibleCreation_ERR_1016.Diagnostic="/P1 크기는 /P2mm의 최대 기준 크기를 초과할 수 없습니다.";
GeoObImpossibleCreation_ERR_1016.Advice="V5R14 이전에 작성된 파트의 경우, 기준 크기가 증가할 수 있으므로 새 파트를 복사/붙여넣기한 후에 같은 작업을 시도하십시오.";
//
//
// the same way it's done in CATICkeScalesServicesAdaptor.CATNls 
// In french, this should be translated by : Etendue normale, Etendue elargie, Etendue extreme, Etendue petite,  Etendue tres petite

NanometricRange="나노 계측 범위";

ExtraSmallRange="매우 작은 범위";
SmallRange="작은 범위";

NormalRange="일반 범위";

LargeRange="큰 범위";
ExtraLargeRange="극한 범위";

//
GeoObInIncompatibleFactoryRange_ERR_1051.Request="호환되지 않는 지오메트리 디자인 제한:";
GeoObInIncompatibleFactoryRange_ERR_1051.Diagnostic="지오메트리 데이터에 /P1 및 /P2처럼 호환되지 않는 디자인 제한이 있습니다.";
GeoObInIncompatibleFactoryRange_ERR_1051.Advice="호환되는 디자인 제한의 제오메트리 데이터를 사용하십시오.";
//
GeoObInIncompatibleFactoryRange_ERR_1052.Request="/P2 디자인 제한의 지오메트리를 /P1에 복사:";
GeoObInIncompatibleFactoryRange_ERR_1052.Diagnostic="대상 디자인 제한(/P1)에서 소스 지오메트리를 준수하려는 시도가 실패했습니다.";
GeoObInIncompatibleFactoryRange_ERR_1052.Advice="소스 지오메트리를 수정하십시오.";
//
//
GeoObInConeSelfIntersecting_ERR_1100.Request="원추 작성:";
GeoObInConeSelfIntersecting_ERR_1100.Diagnostic="매개변수는 자체 교차하는 서피스를 정의합니다.";
GeoObInConeSelfIntersecting_ERR_1100.Advice="- 원추 각도 및/또는 룰 길이를 줄이십시오.";
//
//1150-torus
GeoObInTorusSelfIntersecting_ERR_1150.Request="원환 작성:";
GeoObInTorusSelfIntersecting_ERR_1150.Diagnostic="매개변수는 자체 교차하는 서피스를 정의합니다.";
GeoObInTorusSelfIntersecting_ERR_1150.Advice="- 큰 반지름 값을 늘리십시오. \n
- 짧은 각도 값을 줄이십시오. \n
- 작은 원에 대한 한계를 재설정하는 각도 정의를 수정하십시오.";
//
//1200- Cl[ou]d entities
GeoObInCldScanNotPlanar_ERR_1200.Request="평면 스캔 검색 중:";
GeoObInCldScanNotPlanar_ERR_1200.Diagnostic="스캔은 평면이 아닙니다.";
GeoObInCldScanNotPlanar_ERR_1200.Advice="평면인 스캔을 사용하십시오.";
GeoObInCldGridNoProbeAxis_ERR_1201.Request="격자 탐사축 검색 중:";
GeoObInCldGridNoProbeAxis_ERR_1201.Diagnostic="격자에는 탐사축이 없습니다.";
GeoObInCldGridNoProbeAxis_ERR_1201.Advice="탐사축이 있는 격자를 사용하십시오.";
GeoObInCldGridAddPoints_ERR_1202.Request="격자에 점 추가:";
GeoObInCldGridAddPoints_ERR_1202.Diagnostic="점의 갯수가 초기값과 다릅니다.";
GeoObInCldGridAddPoints_ERR_1202.Advice="점의 갯수를 똑같이 정의하십시오.";
GeoObInCldVectors_AddVector_ERR_1203.Request="벡터 추가:";
GeoObInCldVectors_AddVector_ERR_1203.Diagnostic="벡터를 추가할 수 없습니다.";
GeoObInCldVectors_AddVector_ERR_1203.Advice="문제점은 메모리 부족 때문일 수 있습니다. \n일부 오브젝트를 삭제하거나 파트를 저장하고 다시 여십시오.";
GeoObInCldVectors_Incoherence_ERR_1204.Request="벡터 편집:";
GeoObInCldVectors_Incoherence_ERR_1204.Diagnostic="원점 및 벡터 좌표의 주소가 동일하지 않습니다. \n그렇지 않으면 요청된 레퍼런스가 존재하지 않습니다.";
GeoObInCldVectors_Incoherence_ERR_1204.Advice="두 번째의 경우 해당 메소드를 호출하십시오.";
// 
// 1900-1999- Internal Error (general)
//-----------------------------------------
//
GeoObIntValidObject_InvalidAddress_ERR_1920.Request="올바르지 않은 지오메트리 포인터:";
GeoObIntValidObject_InvalidAddress_ERR_1920.Diagnostic="/P1 지오메트리 오브젝트의 주소가 올바르지 않은 메모리 영역을 가리키고 있습니다.";
GeoObIntValidObject_InvalidAddress_ERR_1920.Advice="시나리오를 식별하고 메인트넌스 정정 요청을 제출하십시오.";
//
GeoObIntValidObject_NotModelisation_ERR_1921.Request="유효하지 않은 지오메트리:";
GeoObIntValidObject_NotModelisation_ERR_1921.Diagnostic="기능은 기본 지오메트리로 제한되며 /P1의 경우 승인되지 않습니다.";
GeoObIntValidObject_NotModelisation_ERR_1921.Advice="기본 구현으로 작업하십시오.";
//
GeoObIntValidObject_UnexpectedType_ERR_1922.Request="예상치 않은 지오메트리 유형:";
GeoObIntValidObject_UnexpectedType_ERR_1922.Diagnostic="지오메트리 오브젝트 /P1은(는) 예상한 /P2 유형이 아닙니다.";
GeoObIntValidObject_UnexpectedType_ERR_1922.Advice="적절한 유형으로 작업하십시오.";
//
GeoObIntValidObject_RemovedObject_ERR_1923.Request="제거된 오브젝트:";
GeoObIntValidObject_RemovedObject_ERR_1923.Diagnostic="지오메트리 오브젝트 /P1을(를) 더이상 사용할 수 없습니다.";
GeoObIntValidObject_RemovedObject_ERR_1923.Advice="활동하는 오브젝트로 작업하십시오.";
//
GeoObIntValidObject_IncompatibleContainer_ERR_1924.Request="호환되지 않는 컨테이너:";
GeoObIntValidObject_IncompatibleContainer_ERR_1924.Diagnostic="/P1 및 /P2 등의 지오메트리 오브젝트는 같은 컨테이너에 속하지 않습니다.";
GeoObIntValidObject_IncompatibleContainer_ERR_1924.Advice="동일한 지오메트리 팩토리로 작업하십시오.";
//
GeoObIntIncompatibleScale_SmallScaleContainer_ERR_1925.Request="지오메트리 Container:";
GeoObIntIncompatibleScale_SmallScaleContainer_ERR_1925.Diagnostic="지오메트리 Container에 현재 세션과 다른 축척(작은 축척)이 포함된 데이터가 있으므로 로드할 수 없습니다.";
GeoObIntIncompatibleScale_SmallScaleContainer_ERR_1925.Advice="도구 - 옵션 - 매개변수 및 측정 - 축척에서 현재 세션의 축척을 변경하십시오.";
//
GeoObIntIncompatibleScale_StandardScaleContainer_ERR_1926.Request="지오메트리 Container:";
GeoObIntIncompatibleScale_StandardScaleContainer_ERR_1926.Diagnostic="지오메트리 Container에 현재 세션과 다른 축척(표준 축척)이 포함된 데이터가 있으므로 로드할 수 없습니다.";
GeoObIntIncompatibleScale_StandardScaleContainer_ERR_1926.Advice="도구 - 옵션 - 매개변수 및 측정 - 축척에서 현재 세션의 축척을 변경하십시오.";
//
GeoObIntIncompatibleScale_LargeScaleContainer_ERR_1927.Request="지오메트리 Container:";
GeoObIntIncompatibleScale_LargeScaleContainer_ERR_1927.Diagnostic="지오메트리 Container에 현재 세션과 다른 축척(큰 축척)이 포함된 데이터가 있으므로 로드할 수 없습니다.";
GeoObIntIncompatibleScale_LargeScaleContainer_ERR_1927.Advice="도구 - 옵션 - 매개변수 및 측정 - 축척에서 현재 세션의 축척을 변경하십시오.";
//
GeoObIntUnsolvedForeignGeometrySoftware_ERR_1930.Request="외부 지오메트리 소프트웨어:";
GeoObIntUnsolvedForeignGeometrySoftware_ERR_1930.Diagnostic="속성 /P1, 도메인 /P2에 링크된 해결되지 않은 외부 지오메트리 소프트웨어가 포함되어 있어서 지오메트리 컨테이너를 로드할 수 없습니다.";
GeoObIntUnsolvedForeignGeometrySoftware_ERR_1930.Advice="RunTimeView의 외부 추가 요소를 확인하십시오.";
//
GeoObIntForbiddenApplicativeAttribute_ERR_1931.Request="금지된 적용 속성:";
GeoObIntForbiddenApplicativeAttribute_ERR_1931.Diagnostic="/P2 도메인의 금지된 적용 속성 /P1이(가) 포함되어 있으므로 지오메트리 컨테이너를 로드할 수 없습니다.";
GeoObIntForbiddenApplicativeAttribute_ERR_1931.Advice="특정 환경 변수의 예상치를 확인합니다.";
//
GeoObIntMandatoryApplicativeAttribute_ERR_1932.Request="필수 적용 속성:";
GeoObIntMandatoryApplicativeAttribute_ERR_1932.Diagnostic="필수 적용 속성 /P1(첫 번째가 /P2)이(가) 포함되지 않았으므로 지오메트리 컨테이너를 로드할 수 없습니다.";
GeoObIntMandatoryApplicativeAttribute_ERR_1932.Advice="특정 환경 변수의 예상치를 확인합니다.";
//
GeoObIntFrozen_ERR_1966="/P1의 경우 수정할 수 없지만 /P2 연산자는 그 정의를 수정하려고 합니다.";
//
GeoObInInvalidInputValueGreaterThan.CurveLength="커브 길이";
//
