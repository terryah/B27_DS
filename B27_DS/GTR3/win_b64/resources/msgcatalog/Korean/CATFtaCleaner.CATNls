// COPYRIGHT DASSAULT SYSTEMES 2003
//=============================================================================
//
// Resources File for NLS purpose related to FTA cleaner
//
//=============================================================================
// Usage notes:
//
//=============================================================================
// Apr. 2003  Creation                                               Z. Y. Gong
//=============================================================================

FTA_1.ShortMessage="TTRS 컨테이너에 있는 피처의 유효성";
FTA_1.ErrorMessage="TTRS 컨테이너에 있는 일부 피처가 유효하지 않습니다.";
FTA_1.CleanMessage="TTRS 컨테이너에 있는 유효하지 않은 피처가 문서에서 제거됩니다.";

FTA_2.ShortMessage="3D 주석의 RGE 활용";
FTA_2.ErrorMessage="3D 주석에서 활용하는 RGE 피처가 없습니다.";
FTA_2.CleanMessage="활용되지 않은 RGE 피처는 문서에서 제거됩니다.";

FTA_3.ShortMessage="3D 주석의 TTRS 활용";
FTA_3.ErrorMessage="3D 주석에서 활용하는 TTRS 피처가 없습니다.";
FTA_3.CleanMessage="활용되지 않은 TTRS 피처는 문서에서 제거됩니다.";

FTA_4.ShortMessage="세트에서 도면 주석과 3D 주석 사이를 링크합니다.";
FTA_4.ErrorMessage="하나 또는 여러 도면 주석이 3D 주석에 사용되지 않습니다.";
FTA_4.CleanMessage="사용되지 않는 도면 주석은 문서에서 제거됩니다.";

FTA_5.ShortMessage="3D 주석 및 공차 세트 간 링크";
FTA_5.ErrorMessage="공차 세트에서 3D 주석을 활용하지 않습니다.";
FTA_5.CleanMessage="3D 공차가 문서에서 제거됩니다.";

FTA_6.ShortMessage="일치하지 않는 치수 레프리젠테이션";
FTA_6.ErrorMessage="FTA 치수 피처가 새로운 지오메트리에서 완전히 reroot 되지 않습니다.";
FTA_6.CleanMessage="FTA 치수가 지오메트리에 올바르게 다시 연결됩니다.";

FTA_7.ShortMessage="TTrs하에 중복된 지오메트리";
FTA_7.Error1Message="사용자 서피스에 같은 지오메트리를 가리키는 여러 컴포넌트가 있습니다. => /p1을(를) 수동으로 지오메트리와 다시 연결해야 합니다.";
FTA_7.Error2Message="사용자 서피스에 같은 지오메트리를 가리키는 여러 컴포넌트가 있습니다. => /p1을(를) 수동으로 지오메트리와 다시 연결해야 합니다. /p2이(가) 사용자 서피스에 작성됩니다.";
FTA_7.Error3Message="사용자 서피스에 동일한 지오메트리를 가리키는 여러 개의 컴포넌트가 있습니다. => '/p1'의 '/p3' 및 '/p4' 컴포넌트가 동일한 지오메트리를 가리킵니다. '/p1'을(를) 수동으로 지오메트리와 다시 연결해야 합니다. '/p2'은(는) 이 사용자 서피스에 작성됩니다.";

FTA_8.ShortMessage="CG 스레드 상태 계산 오류";
FTA_8.ErrorMessage="CG 스레드의 TTRS 표시가 올바로 다시 정의되지 않았습니다.";

FTA_9.ShortMessage="올바르지 않은 지오메트리 공차 표시";
FTA_9.ErrorMessage="지오메트리 공차의 3D 표시가 해당 의미 속성과 맞지 않습니다.
공차 영역이 의미 속성에 따라 실린더이지만 지름 기호가 콜아웃에 표시되지 않습니다.
오류를 수정하려면 지오메트리 공차와 다음 중 하나를 편집해야 합니다(지름 기호가 자동으로 추가됨).
1. 디자인 의도가 실린더 공차 영역 정의인 경우 확인 버튼을 누르거나,
2. 디자인 의도가 2개의 평행면 공차 영역 정의인 경우 공차 영역 방향을 선택한 후 확인 버튼을 누르십시오.";

FTA_10.ShortMessage="FTA 뷰의 일치하지 않는 TTRS 사용";
FTA_10.ErrorMessage="3D 축 시스템에서 작성된 FTA 뷰가 다른 문서의 TTRS 오브젝트를 사용 중입니다.";
FTA_10.CleanMessage="뷰가 사용한 TTRS가 정정되었습니다.";

FTA_11.ShortMessage="일치하지 않는 URL 링크";
FTA_11.ErrorMessage="FlagNote 또는 NOA 기능에 있는 하나 이상의 URL 링크가 비어 있습니다.";
FTA_11.CleanMessage="비어 있는 URL 링크가 제거되었습니다.";

FTA_12.ShortMessage="3D 주석과 3D 주석 뷰의 링크입니다.";
FTA_12.ErrorMessage="3D 주석의 3D 주석 뷰가 없습니다.";
FTA_12.CleanMessage="새 3D 주석 뷰가 작성되어 주석에 다시 연결되었습니다.";

FTA_13.ShortMessage="일치하지 않는 Datum 대상입니다.";
FTA_13.ErrorMessage="의미 Datum 대상이 의미 Datum과 연계되어 있지 않습니다.";
FTA_13.CleanMessage="연계된 의미 Datum이 없는 일치하지 않는 Datum 대상이 제거되었습니다.";

FTA_14.ShortMessage="시각화 치수 피처에 일치하지 않는 링크가 있습니다.";
FTA_14.ErrorMessage="시각화 치수 피처가 둘 이상의 주석을 사용하여 링크되어 있습니다.";
FTA_14.CleanMessage="이 시각화 치수 피처의 유효하지 않은 링크가 제거되었습니다.";

FTA_15.ShortMessage="FTA 캡처 외부 링크.";
FTA_15.ErrorMessage="하나 이상의 캡처 외부 링크가 존재하지 않는 문서를 가리킵니다.";
FTA_15.CleanMessage="존재하지 않는 외부 링크가 제거되었습니다.";

FTA_16.ShortMessage="FTA NOA 외부 링크.";
FTA_16.ErrorMessage="NOA의 외부 링크가 존재하지 않는 문서를 기리킵니다.";
FTA_16.CleanMessage="존재하지 않는 외부 링크가 제거되었습니다.";

FTA_17.ShortMessage="FTA NOA 및 Ditto.";
FTA_17.ErrorMessage="배경 뷰에 NOA의 Ditto가 작성되지 않았습니다.";
FTA_17.CleanMessage="NOA의 Ditto가 배경 뷰로 진로 변경되었습니다.";

FTA_18.ShortMessage="FTA 텍스트 외부 링크입니다.";
FTA_18.ErrorMessage="텍스트의 외부 링크가 존재하지 않는 문서를 기리킵니다.";
FTA_18.CleanMessage="존재하지 않는 외부 링크가 제거되었습니다.";

FTA_19.ShortMessage="일관되지 않은 반복 피처 ID입니다.";
FTA_19.ErrorMessage="반복 피처 ID가 치수가 링크된 패턴 요소의 수에 대응하지 않거나, 0개 또는 복수의 반복 피처 ID가 있거나, 반복 피처 ID와 그 앞뒤에 있는 텍스트 간에 공백(“ “) 문자가 없습니다.";
FTA_19.CleanMessage="패턴 치수의 일관되지 않은 반복 피처 ID가 정정되었습니다.";
