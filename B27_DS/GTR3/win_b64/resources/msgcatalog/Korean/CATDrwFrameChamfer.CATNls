//=====================================================================================
//                                     CNEXT - CXRn
//                          COPYRIGHT DASSAULT SYSTEMES 2000 
//-------------------------------------------------------------------------------------
// FILENAME    :    CATDrwFrameChamfer
// LOCATION    :    DraftingIntUI
// AUTHOR      :    fgx
// BUT         :    Properties : Tab "Chamfer" 
// DATE        :    07.03.2000
//-------------------------------------------------------------------------------------
// DESCRIPTION :    
//-------------------------------------------------------------------------------------
// COMMENTS    :    
//-------------------------------------------------------------------------------------
// MODIFICATIONS    user    date        purpose
//    HISTORY       ----    ----        -------
//=====================================================================================


_titleValFormat.Title="값 포맷";
_titleValFormat.LongHelp=
"값 표시 포맷을 설정합니다.";

_frameValFormat._radioValFormatDistDist.Title="거리 x 거리";
_frameValFormat._radioValFormatDistDist.LongHelp=
"\"거리 x 거리\" 유형의 값 포맷을
설정합니다.";

_frameValFormat._radioValFormatDistAngle.Title="거리 x 각도";
_frameValFormat._radioValFormatDistAngle.LongHelp=
"\"거리 x 각도\" 유형의 값 포맷을
설정합니다.";

_frameValFormat._radioValFormatAngleDist.Title="각도 x 거리";
_frameValFormat._radioValFormatAngleDist.LongHelp=
"\"각도 x 거리\" 유형의 값 포맷을
설정합니다.";

_frameValFormat._radioValFormatDistance.Title="거리";
_frameValFormat._radioValFormatDistance.LongHelp=
"\"거리\" 유형의 값 포맷을
설정합니다.";

_titleDescription.Title="숫자 표시";
_titleDescription.LongHelp=
"값의 숫자 표시 설명을 설정합니다.";

_frameDescription._frameMoreInfoMain1._pushMoreInfoMain1.ShortHelp="기본 설명 찾아보기";
_frameDescription._frameMoreInfoMain2._pushMoreInfoMain2.ShortHelp="기본 설명 찾아보기";
_frameDescription._frameMoreInfoDual1._pushMoreInfoDual1.ShortHelp="이중 설명 찾아보기";
_frameDescription._frameMoreInfoDual2._pushMoreInfoDual2.ShortHelp="이중 설명 찾아보기";

_frameDescription._labelDescriptionMain1.Title="기본 설명";
_frameDescription._labelUnitOrderMain.Title="표시: ";
_frameDescription._labelDescriptionDual1.Title="이중 설명";
_frameDescription._labelUnitOrderDual.Title="표시: ";

_frameDescription._comboDescriptionMain1.LongHelp=
"숫자 표시의 첫 번째 기본 값 설명을 설정합니다.";
_frameDescription._comboDescriptionMain2.LongHelp=
"숫자 표시의 두 번째 기본 값 설명을 설정합니다.";

_frameDescription._comboUnitOrderMain1.LongHelp=
"첫 번째 기본 값을 표시할 단위 요소 수를 설정합니다.";
_frameDescription._comboUnitOrderMain2.LongHelp=
"두 번째 기본 값을 표시할 단위 요소 수를 설정합니다.";

_frameDescription._comboDescriptionDual1.LongHelp=
"숫자 표시의 첫 번째 이중 값 설명을 설정합니다.";
_frameDescription._comboDescriptionDual2.LongHelp=
"숫자 표시의 두 번째 이중 값 설명을 설정합니다.";

_frameDescription._comboUnitOrderDual1.LongHelp=
"첫 번째 이중 값을 표시할 단위 요소 수를 설정합니다.";
_frameDescription._comboUnitOrderDual2.LongHelp=
"두 번째 이중 값을 표시할 단위 요소 수를 설정합니다.";

_titleRepType.Title="레프리젠테이션 유형";
_titleRepType.LongHelp=
"치수의 레프리젠테이션 유형을 설정합니다.";

_frameRepType._radioRepTypeDistance.Title="2 기호";
_frameRepType._radioRepTypeDistance.LongHelp=
"2 기호 유형의 레프리젠테이션을
설정합니다.";

_frameRepType._radioRepTypeRadius.Title="1 기호";
_frameRepType._radioRepTypeRadius.LongHelp=
"1 기호 유형의 레프리젠테이션을
설정합니다.";

ONE_FACTOR="1 요인";
TWO_FACTORS="2 요인";
THREE_FACTORS="3 요인";

ComboValueFormatLine0="소수";
ComboValueFormatLine1="분수";

_frameDescription.LabelFormatMain.Title="형식: ";
_frameDescription.LabelFormatDual.Title="형식: ";
_frameDescription.LabelPrecisionMain.Title="정밀도: ";
_frameDescription.LabelPrecisionDual.Title="정밀도: ";


_frameDescription.ComboValuePrecisionMain1.LongHelp="기본 값 정밀도를 설정합니다.";
_frameDescription.ComboValueFormatMain1.LongHelp="기본 값 포맷을 설정합니다.";
_frameDescription.ComboValuePrecisionMain2.LongHelp="기본 값 2차 정밀도를 설정합니다.";
_frameDescription.ComboValueFormatMain2.LongHelp="기본 값 2차 포맷을 설정합니다.";
_frameDescription.ComboValuePrecisionDual1.LongHelp="이중 값 정밀도를 설정합니다.";
_frameDescription.ComboValueFormatDual1.LongHelp="이중 값 포맷을 설정합니다.";
_frameDescription.ComboValuePrecisionDual2.LongHelp="이중 값 2차 정밀도를 설정합니다.";
_frameDescription.ComboValueFormatDual2.LongHelp="이중 값 2차 포맷을 설정합니다.";

_frameDescription.ComboValuePrecisionMain1.ShortHelp="기본 값 정밀도 삽입";
_frameDescription.ComboValueFormatMain1.ShortHelp="기본 값 포맷 삽입";
_frameDescription.ComboValuePrecisionMain2.ShortHelp="기본 값 2차 정밀도 삽입";
_frameDescription.ComboValueFormatMain2.ShortHelp="기본 값 2차 포맷 삽입";
_frameDescription.ComboValuePrecisionDual1.ShortHelp="이중 값 정밀도 삽입";
_frameDescription.ComboValueFormatDual1.ShortHelp="이중 값 포맷 삽입";
_frameDescription.ComboValuePrecisionDual2.ShortHelp="이중 값 2차 정밀도 삽입";
_frameDescription.ComboValueFormatDual2.ShortHelp="이중 값 2차 포맷 삽입";

