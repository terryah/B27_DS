//=============================================================================
//                          COPYRIGHT Dassault Systemes 2005
//-----------------------------------------------------------------------------
// FILENAME    :    CATDrwClippingBoxCmd
// LOCATION    :    DraftingGenUI/CNext/resources/msgcatalog/
// AUTHOR      :    wsa
// DATE        :    May 2005
//------------------------------------------------------------------------------
// DESCRIPTION :    Resources File for NLS purpose related to CATDrwClippingBoxCmd
//                  
//------------------------------------------------------------------------------
// COMMENTS    :	 ENGLISH VERSION
//------------------------------------------------------------------------------
//MODIFICATIONS     user  date        purpose
//   HISTORY        ----  ----        -------
//   01.            apr3  19.04.2016  Revision
//------------------------------------------------------------------------------

CATDrwClippingBoxCmd.initialState.Message="종료하려면 뷰어에서 아무 곳이나 누르십시오.";
CATDrwClippingBoxCmd.Title="클리핑 오브젝트";
CATDrwClippingBoxCmd.ModeFrame="클리핑 모드";
CATDrwClippingBoxCmd.Mode1="클리핑 박스";
CATDrwClippingBoxCmd.Mode2="슬라이스에 의한 클리핑";
CATDrwClippingBoxCmd.Mode3="뒷면 클리핑 평면";
CATDrwClippingBoxCmd.InputFrame="사용자 입력";
CATDrwClippingBoxCmd.PointFrame.Title="중심점";
CATDrwClippingBoxCmd.PointFrame.ShortHelp="클리핑 오브젝트의 중심점";
CATDrwClippingBoxCmd.PointFrame.Help="클리핑 오브젝트의 중심점";
CATDrwClippingBoxCmd.PointFrame.LongHelp="클리핑 오브젝트의 중심점";
CATDrwClippingBoxCmd.DimensionFrame.Title="치수";
CATDrwClippingBoxCmd.DimensionFrame.ShortHelp="클리핑 오브젝트의 치수. 가능한 최소 치수(도면 공차에 관계됨)는 1mm입니다.";
CATDrwClippingBoxCmd.DimensionFrame.Help="클리핑 오브젝트의 치수. 가능한 최소 치수(도면 공차에 관계됨)는 1mm입니다.";
CATDrwClippingBoxCmd.DimensionFrame.LongHelp="클리핑 오브젝트의 치수. 가능한 최소 치수(도면 공차에 관계됨)는 1mm입니다.";
CATDrwClippingBoxCmd.Label1=" X ";
CATDrwClippingBoxCmd.Label2=" Y ";
CATDrwClippingBoxCmd.Label3=" Z ";
CATDrwClippingBoxCmd.Label4=" 길이 ";
CATDrwClippingBoxCmd.Label5=" 폭 ";
CATDrwClippingBoxCmd.Label6=" 높이 ";
CATDrwClippingBoxCmd.CreateModifyPushButton.Title="      작성      ";
CATDrwClippingBoxCmd.CreateModifyPushButton.ShortHelp="클리핑 오브젝트 작성";
CATDrwClippingBoxCmd.CreateModifyPushButton.Help="뷰에서 클리핑 오브젝트 작성";
CATDrwClippingBoxCmd.CreateModifyPushButton.LongHelp="뷰에서 클리핑 오브젝트를 작성하고 갱신합니다. 또한 패널을 닫습니다.";
CATDrwClippingBoxCmd.CommandMode2.Title="      수정      ";
CATDrwClippingBoxCmd.OKPushButton.CommandMode2.ShortHelp="클리핑 오브젝트 수정";
CATDrwClippingBoxCmd.OKPushButton.CommandMode2.Help="뷰에서 클리핑 오브젝트 수정";
CATDrwClippingBoxCmd.OKPushButton.CommandMode2.LongHelp="뷰에서 클리핑 오브젝트를 수정하고 갱신합니다. 또한 패널을 닫습니다.";
CATDrwClippingBoxCmd.CancelPushButton.Title="      취소      ";
CATDrwClippingBoxCmd.CancelPushButton.ShortHelp="명령 취소";
CATDrwClippingBoxCmd.CancelPushButton.Help="명령을 취소하고 패널을 닫습니다.";
CATDrwClippingBoxCmd.CancelPushButton.LongHelp="명령을 취소하고 패널을 닫습니다. 클리핑 오브젝트를 작성하거나 수정하지 않습니다.";
CATDrwClippingBoxCmd.ApplyPushButton.Title="      미리보기      ";
CATDrwClippingBoxCmd.ApplyPushButton.ShortHelp="클리핑 오브젝트 적용";
CATDrwClippingBoxCmd.ApplyPushButton.Help="클리핑 오브젝트를 적용하고 뷰를 갱신합니다.";
CATDrwClippingBoxCmd.ApplyPushButton.LongHelp="클리핑 오브젝트를 적용하고 뷰를 갱신합니다. 패널을 닫지 않습니다.";
CATDrwClippingBoxCmd.ModeComboBox.ShortHelp="모드 선택";
CATDrwClippingBoxCmd.ModeComboBox.Help="클리핑 오브젝트의 세 가지 모드 중 하나를 선택할 수 있도록 합니다.";
CATDrwClippingBoxCmd.ModeComboBox.LongHelp="클리핑 오브젝트의 세 가지 모드 중 하나를 선택할 수 있도록 합니다.";
CATDrwClippingBoxCmd.PointSpinner1.ShortHelp="중심점의 X 좌표";
CATDrwClippingBoxCmd.PointSpinner1.Help="클리핑 오브젝트 중심점의 X 좌표입니다.";
CATDrwClippingBoxCmd.PointSpinner1.LongHelp="클리핑 오브젝트 중심점의 X 좌표입니다.";
CATDrwClippingBoxCmd.PointSpinner2.ShortHelp="중심점의 Y 좌표";
CATDrwClippingBoxCmd.PointSpinner2.Help="클리핑 오브젝트 중심점의 Y 좌표입니다.";
CATDrwClippingBoxCmd.PointSpinner2.LongHelp="클리핑 오브젝트 중심점의 Y 좌표입니다.";
CATDrwClippingBoxCmd.PointSpinner3.ShortHelp="중심점의 Z 좌표";
CATDrwClippingBoxCmd.PointSpinner3.Help="클리핑 오브젝트 중심점의 Z 좌표입니다.";
CATDrwClippingBoxCmd.PointSpinner3.LongHelp="클리핑 오브젝트 중심점의 Z 좌표입니다.";
CATDrwClippingBoxCmd.DimensionSpinner1.ShortHelp="3D 클리핑의 길이. ClippingBySlice 모드의 경우 뒷면과 정면 클리핑 평면 사이의 거리입니다.";
CATDrwClippingBoxCmd.DimensionSpinner1.Help="3D 클리핑의 길이. ClippingBySlice 모드의 경우 뒷면과 정면 클리핑 평면 사이의 거리입니다.";
CATDrwClippingBoxCmd.DimensionSpinner1.LongHelp="3D 클리핑의 길이. ClippingBySlice 모드의 경우 뒷면과 정면 클리핑 평면 사이의 거리입니다.";
CATDrwClippingBoxCmd.DimensionSpinner2.ShortHelp="3D 클리핑의 폭";
CATDrwClippingBoxCmd.DimensionSpinner2.Help="3D 클리핑의 폭입니다.";
CATDrwClippingBoxCmd.DimensionSpinner2.LongHelp="3D 클리핑의 폭입니다.";
CATDrwClippingBoxCmd.DimensionSpinner3.ShortHelp="3D 클리핑의 높이";
CATDrwClippingBoxCmd.DimensionSpinner3.Help="3D 클리핑의 높이입니다.";
CATDrwClippingBoxCmd.DimensionSpinner3.LongHelp="3D 클리핑의 높이입니다.";
CATDrwClippingBoxCmd.3DViewer.ShortHelp="클리핑 오브젝트를 사용한 3D 파트 또는 프로덕트";
CATDrwClippingBoxCmd.3DViewer.Help="클리핑 오브젝트를 사용한 3D 파트 또는 프로덕트. 3D 뷰어에서 클리핑 오브젝트를 정의합니다.";
CATDrwClippingBoxCmd.3DViewer.LongHelp="클리핑 오브젝트를 사용한 3D 파트 또는 프로덕트. 3D 뷰어에서 클리핑 오브젝트를 정의합니다. 파란색 화살표는 뷰 방향을 나타냅니다.";
ProgressTaskUI.Title="클리핑 박스";
CATDrwClippingBoxCmd.InitBox.Message="클리핑 오브젝트를 조작하십시오.";
CATDrwClippingBoxCmd.ModifiedClippingObject.Message="클리핑 오브젝트를 조작하십시오.";
CATDrwClippingBoxCmd.ResetManipButton="현재 조작기 지우기";
CATDrwClippingBoxCmd.UndoTitle="3D 클리핑 추가 또는 수정";

