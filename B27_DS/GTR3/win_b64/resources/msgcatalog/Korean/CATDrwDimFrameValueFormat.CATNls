//=============================================================================
//                                     CNEXT - CXR1
//                          COPYRIGHT DASSAULT SYSTEMES 1997 
//-----------------------------------------------------------------------------
// FILENAME    :    CATDrwDimFrameValueFormat
// LOCATION    :    DraftingIntUI/CNext/resources/msgcatalog
// AUTHOR      :    jmt
// DATE        :    Nov. 03 1997
//------------------------------------------------------------------------------
// DESCRIPTION :    Resources File for NLS purpose related to Drafting WorkShop
//                  
//------------------------------------------------------------------------------
// COMMENTS    :
//------------------------------------------------------------------------------
//MODIFICATIONS     user  date      purpose
//   HISTORY        ----  ----      -------
//     01			fgx	  18.02.99	Ajout des unites d'angle MINUTE et SECOND
//     02			fgx	  08.03.99	Ajout du mode driving
//     03			fgx	  18.11.99	Dim type
//     04			LGK     25.06.02  Revision
//------------------------------------------------------------------------------

_titleDimType.Title="치수 유형 ";

ONE_PROJECTED_DIM = "투영된 치수";
SEV_PROJECTED_DIM = "투영된 치수";
ONE_TRUE_DIM = "진짜 치수";
SEV_TRUE_DIM = "진짜 치수";
UNDEFINED = "정의되지 않음";

_frameDimType._checkDriving.Title="드라이빙";
_frameDimType._checkDriving.Help="치수에 대한 driven/driving 모드를 설정합니다.";
_frameDimType._checkDriving.LongHelp="드라이빙
치수에 대한 driven/driving 모드를
설정합니다.";

_frameDimType._editorDriving.ShortHelp="값";
_frameDimType._editorDriving.Help="지오메트리를 드라이빙할 때 치수 값을 편집합니다. ";
_frameDimType._editorDriving.LongHelp="값
지오메트리를 드라이빙할 때
치수 값을 편집합니다.";

FrameBase.FrameLayoutTitle.LabelLayoutTitle.Title="값 방향 ";
FrameBase.FrameDualValueTitle.LabelDualValueTitle.Title="이중 값 ";
FrameFormatTitle.LabelFormatTitle.Title="포맷 ";
FrameFakeTitle.CheckButtonFakeTitle.Title="거짓 치수 ";

FrameBase.FrameLayout.LabelReference.Title="레퍼런스: ";
FrameBase.FrameLayout.LabelOrientation.Title="방향: ";
FrameBase.FrameLayout.LabelAngle.Title="각도: ";
FrameBase.FrameLayout.LabelInOutValue.Title="위치: ";
FrameBase.FrameLayout.LabelOffset.Title="오프셋: ";
FrameFormat.LabelValue.Title="기본 값";
FrameFormat.LabelDualValue.Title="이중 값";

FrameFormat._labelUnitOrder.Title="표시: ";
FrameFormat._labelDescription.Title="설명: ";
FrameFormat._frameMoreInfoMain._pushMoreInfoMain.ShortHelp="기본 설명 찾아보기";
FrameFormat._frameMoreInfoDual._pushMoreInfoDual.ShortHelp="이중 설명 찾아보기";

FrameFormat.LabelPrecision.Title="정밀도: ";
FrameFormat.LabelFormat.Title="포맷:";
FrameFormat.LabelValueFrac.Title    ="   1 / ";
FrameFormat.LabelDualValueFrac.Title="   1 / ";
FrameFake.LabelFakeMain.Title="기본 값: ";
FrameFake.LabelFakeDual.Title="이중 값: ";
FrameFake.RadioButtonFakeNum.Title="숫자";
FrameFake.RadioButtonFakeAlpha.Title="영숫자";

FrameBase.FrameDualValue.CheckButtonDualValue.Title="이중 값 표시";

ComboReferenceLine0="화면";
ComboReferenceLine1="뷰";
ComboReferenceLine2="치수선";
ComboReferenceLine3="확장선";

ComboInOutValueAUTO="자동";
ComboInOutValueIN="내부";
ComboInOutValueOUT="외부";

ComboDualValueLine0="아래";
ComboDualValueLine1="분수";
ComboDualValueLine2="나란히";

ComboValueFormatLine0="소수";
ComboValueFormatLine1="분수";

ComboDualValueFormatLine0="소수";
ComboDualValueFormatLine1="분수";

ComboOrientationLine0="평행";
ComboOrientationLine1="수직";
ComboOrientationLine2="고정 각도";

ComboOrientationLine00="수평";
ComboOrientationLine01="수직";
ComboOrientationLine02="고정 각도";

ONE_FACTOR="1 요인";
TWO_FACTORS="2 요인";
THREE_FACTORS="3 요인";

DefaultFakeText="*거짓*";

FrameBase.FrameLayout.ComboReference.LongHelp=
"값 위치 지정을 위한
레퍼런스를 제공합니다.";
FrameBase.FrameLayout.ComboOrientation.LongHelp=
"해당 레퍼런스에 상대적으로 값의
방향을 정의합니다.";
FrameBase.FrameLayout.SpinnerAngle.LongHelp=
"해당 레퍼런스에 상대적으로 값의
각도를 정의합니다.";
FrameBase.FrameLayout.SpinnerOffset.LongHelp=
"치수선에서 값의 수직 오프셋을 정의합니다.";
FrameBase.FrameDualValue.CheckButtonDualValue.LongHelp=
"이중 값 표시
선택하면 이중 값을 표시합니다.";
FrameBase.FrameDualValue.ComboDualValue.LongHelp=
"이중 값의 표시 모드를 선택합니다.";

FrameFormat._labelUnitOrder.LongHelp=
"표시할 기본 및 이중 단위 요소 수를 설정합니다.";
FrameFormat._labelDescription.LongHelp=
"숫자 표시의 기본 및 이중 설명을 설정합니다.";

FrameFormat.LabelPrecision.LongHelp=
"기본 및 이중 값 정밀도를 설정합니다.";
FrameFormat.LabelFormat.LongHelp=
"기본 및 이중 값 포맷을 설정합니다.";

FrameFormat._comboUnitOrder.LongHelp=
"기본 값에 대해 표시할 단위 요소 수를 설정합니다. ";
FrameFormat._comboDescriptionMain.LongHelp=
"숫자 표시의 기본 값 설명을 설정합니다.";
FrameFormat._frameMoreInfoMain._pushMoreInfoMain.LongHelp=
"숫자 표시의 기본 설명을 찾아보거나 변경할
대화상자를 표시합니다. ";
FrameFormat.ComboValuePrecision.LongHelp=
"기본 값 정밀도를 설정합니다.";
FrameFormat.ComboValueFormat.LongHelp=
"기본 값 포맷을 설정합니다.";

FrameFormat._comboDualUnitOrder.LongHelp=
"이중 값에 대해 표시할 단위 요소 수를 설정합니다.";
FrameFormat._comboDescriptionDual.LongHelp=
"숫자 표시의 첫 번째 이중 값 설명을 설정합니다.";
FrameFormat._frameMoreInfoDual._pushMoreInfoDual.LongHelp=
"숫자 표시의 이중 설명을 찾아보거나 변경할
대화상자를 표시합니다. ";
FrameFormat.ComboDualValuePrecision.LongHelp=
"이중 값 정밀도를 설정합니다.";
FrameFormat.ComboDualValueFormat.LongHelp=
"이중 값 포맷을 설정합니다.";

FrameFakeTitle.CheckButtonFakeTitle.LongHelp=
"치수를 거짓 치수로 변경합니다.";

FrameFake.EditorFakeNumMain.LongHelp=
"기본 숫자 거짓 값을 표시합니다. ";
FrameFake.EditorFakeAlphaMain.LongHelp=
"기본 영숫자 거짓 값을 표시합니다. ";
FrameFake.EditorFakeNumDual.LongHelp=
"이중 숫자 거짓 값을 표시합니다. ";
FrameFake.EditorFakeAlphaDual.LongHelp=
"이중 영숫자 거짓 값을 표시합니다. ";

SeeChamfer="챔퍼 참조";

