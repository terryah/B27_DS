//---------------------------------------
// Resource file for CATGSMUIBlendPanel class
// En_US
//---------------------------------------

Title="혼합 정의";

LimitFrame.StartLimFrame.StartCurveLabel.Title = "첫 번째 커브: ";
LimitFrame.StartLimFrame.StartCurveLabel.LongHelp = "혼합의 첫 번째 커브를 지정합니다.";

LimitFrame.StartLimFrame.StartSupportLabel.Title = "첫 번째 기준:";
LimitFrame.StartLimFrame.StartSupportLabel.LongHelp = "혼합의 첫 번째 기준을 지정합니다.
첫 번째 커브는 이 기준에 놓여야 합니다. ";

LimitFrame.EndLimFrame.EndCurveLabel.Title = "두 번째 커브: ";
LimitFrame.EndLimFrame.EndCurveLabel.LongHelp = "혼합의 두 번째 커브를 지정합니다.";

LimitFrame.EndLimFrame.EndSupportLabel.Title = "두 번째 기준: ";
LimitFrame.EndLimFrame.EndSupportLabel.LongHelp = "혼합의 두 번째 기준을 지정합니다.
두 번째 커브는 이 기준에 놓여야 합니다. ";

ParameterFrame.ParamTabContainer.BasicPage.Title = "기본";
ParameterFrame.ParamTabContainer.BasicPage.LongHelp = "혼합의 두 한계에 대한 연속성,
자르기 기준을 지정합니다.";

ParameterFrame.ParamTabContainer.BasicPage.BasicFrame.ContFrame.StartContFrame.StartContLabel.Title = "첫 번째 연속성: ";
ParameterFrame.ParamTabContainer.BasicPage.BasicFrame.ContFrame.StartContFrame.StartContLabel.LongHelp = 
"혼합 및 첫 번째 기준 사이의
연속성을 지정합니다.
연속성은 점, 접점 또는 곡률이 될 수 있습니다.";

ParameterFrame.ParamTabContainer.BasicPage.BasicFrame.ContFrame.StartTrimButton.Title = "첫 번째 기준 자르기";
ParameterFrame.ParamTabContainer.BasicPage.BasicFrame.ContFrame.StartTrimButton.LongHelp = 
"첫 번째 기준이 첫 번째 커브에 의해 잘리는지
아니면 혼합에 결합되는지 여부를 지정합니다.";

ParameterFrame.ParamTabContainer.BasicPage.BasicFrame.ContFrame.StartBorderFrame.StartBorderLabel.Title = "첫 번째 접점 경계: ";
ParameterFrame.ParamTabContainer.BasicPage.BasicFrame.ContFrame.StartBorderFrame.LongHelp = 
"혼합 경계가 첫 번째 기준에 접해 있는지 여부를
지정합니다.
   - 두 극한 모두: 두 개의 첫 번째 커브 극한 모두에 있는 경계가 접해 있습니다.
   - 없음: 접촉에서 경계가 제한되지 않습니다.
   - 시작 극한만: 첫 번째 커브의 시작 극한에 있는 경계가 접해 있습니다.
   - 종료 극한만: 첫 번째 커브의 종료 극한에 있는 경계가 접해 있습니다.";

BothExtreTitle="두 극한 모두";
NoneExtreTitle="없음";
StartExtreTitle="말단만 시작";
EndExtreTitle="말단만 종료";
FirstOrExtreTitle="자유 첫 번째 커브 원점";
SecondOrExtreTitle="자유 두 번째 커브 원점";
FirstEndExtreTitle="자유 첫 번째 커브 끝";
SecondEndExtreTitle="자유 두 번째 커브 끝";
BothExtreConnectTitle="두 극단 연결";

ParameterFrame.ParamTabContainer.BasicPage.BasicFrame.TrimFrame.EndContFrame.EndContLabel.Title = "두 번째 연속성: ";
ParameterFrame.ParamTabContainer.BasicPage.BasicFrame.TrimFrame.EndContFrame.EndContLabel.LongHelp = 
"혼합 및 첫 번째 기준 사이의
연속성을 지정합니다.
연속성은 점, 접점 또는 곡률이 될 수 있습니다.";

ParameterFrame.ParamTabContainer.BasicPage.BasicFrame.TrimFrame.EndTrimButton.Title = "두 번째 기준 자르기";
ParameterFrame.ParamTabContainer.BasicPage.BasicFrame.TrimFrame.EndTrimButton.LongHelp = 
"두 번째 기준이 두 번째 커브에 의해 잘리는지
아니면 혼합에 결합되는지 여부를 지정합니다.";

ParameterFrame.ParamTabContainer.BasicPage.BasicFrame.TrimFrame.EndBorderFrame.EndBorderLabel.Title = "두 번째 접점 경계: ";
ParameterFrame.ParamTabContainer.BasicPage.BasicFrame.TrimFrame.EndBorderFrame.LongHelp = 
"혼합 경계가 두 번째 기준에 접해 있는지 여부를
지정합니다.
   - 두 극한 모두: 두 개의 첫 번째 커브 극한 모두에 있는 경계가 접해 있습니다.
   - 없음: 접촉에서 경계가 제한되지 않습니다.
   - 시작 극한만: 첫 번째 커브의 시작 극한에 있는 경계가 접해 있습니다.
   - 종료 극한만: 첫 번째 커브의 종료 극한에 있는 경계가 접해 있습니다.";

ParameterFrame.ParamTabContainer.PointPage.Title = "닫기 점";
ParameterFrame.ParamTabContainer.PointPage.LongHelp = "혼합의 닫힌 커브마다 닫기 점을 지정합니다.";

ParameterFrame.ParamTabContainer.PointPage.PointFrame.StartPointFrame.StartPointLabel.Title = "첫 번째 닫기 점: ";
ParameterFrame.ParamTabContainer.PointPage.PointFrame.StartPointFrame.StartPointLabel.LongHelp = 
"첫 번째 커브의 닫기 점을 지정합니다.
첫 번째 커브는 닫혀야 합니다.";

ParameterFrame.ParamTabContainer.PointPage.PointFrame.EndPointFrame.EndPointLabel.Title = "두 번째 닫기 점: ";
ParameterFrame.ParamTabContainer.PointPage.PointFrame.EndPointFrame.EndPointLabel.LongHelp =
"두 번째 커브의 닫기 점을 지정합니다.
두 번째 커브는 닫혀야 합니다.";

ParameterFrame.ParamTabContainer.CouplingPage.Title = "결합 / 스파인";
ParameterFrame.ParamTabContainer.CouplingPageWithoutSpine.Title = "결합";
ParameterFrame.ParamTabContainer.CouplingPage.LongHelp = "커브 결합 방법을 지정합니다.
- 비율: 커브가 커브 횡좌표 비율에 따라 결합됩니다.
- 접촉: 각 커브의 접촉 불연속성 점 개수가
동일하면 이 점들이 결합되고 그렇지 않으면
오류 메시지가 표시됩니다.
- 접촉 후 곡률: 각 커브의 접촉 및 곡률
불연속성 점 개수가 동일한 경우
접촉 불연속성 점이 결합된 후
곡률 불연속성 점이 결합됩니다.
그렇지 않으면 오류 메시지가 표시됩니다.
- 정점: 각 커브의 정점 수가 동일한 경우
이 점들이 결합되고 그렇지 않으면
오류 메시지가 표시됩니다.
- 스파인: 스파인 커브에서 결합이 드라이브됩니다.";

ParameterFrame.ParamTabContainer.TensionPage.Title = "장력";
ParameterFrame.ParamTabContainer.TensionPage.LongHelp = 
"혼합의 각 기준에 대한 장력을 지정합니다.";

ParameterFrame.ParamTabContainer.TensionPage.TensionFrame.StartTensionFrame.StartTensionButtonFrame.StartTension.Title = "첫 번째 장력: ";
ParameterFrame.ParamTabContainer.TensionPage.TensionFrame.StartTensionFrame.StartTensionButtonFrame.StartTension.LongHelp =
"혼합의 첫 번째 기준에 대한 장력을 지정합니다.";

ParameterFrame.ParamTabContainer.TensionPage.TensionFrame.EndTensionFrame.EndTensionButtonFrame.EndTension.Title = "두 번째 장력: ";
ParameterFrame.ParamTabContainer.TensionPage.TensionFrame.EndTensionFrame.EndTensionButtonFrame.EndTension.LongHelp =
"혼합의 두 번째 기준에 대한 장력을 지정합니다.";

ParameterFrame.ParamTabContainer.TensionPage.TensionFrame.StartTensionFrame.StartTensionButtonFrame.StartDefault.Title = "디폴트";
ParameterFrame.ParamTabContainer.TensionPage.TensionFrame.StartTensionFrame.StartTensionButtonFrame.StartDefault.LongHelp = 
"혼합의 첫 번째 기준에 대한 장력이
디폴트 장력임을 지정합니다.";

ParameterFrame.ParamTabContainer.TensionPage.TensionFrame.EndTensionFrame.EndTensionButtonFrame.EndDefault.Title = "디폴트";
ParameterFrame.ParamTabContainer.TensionPage.TensionFrame.EndTensionFrame.EndTensionButtonFrame.EndDefault.LongHelp = 
"혼합의 두 번째 기준에 대한 장력이
디폴트 장력임을 지정합니다.";

ActionFrame.ReplaceButton.Title = "교체";
ActionFrame.RemoveButton.Title = "제거";
ActionFrame.ReverseButton.Title = "반전";


PointContTitle="점";
TangencyContTitle="접점";
CurvatureContTitle="곡률";

ParameterFrame.ParamTabContainer.TensionPage.TensionFrame.StartTensionFrame.StartTensionButtonFrame.StartTensionComb.LongHelp = 
"혼합의 첫 번째 기준에 대한 장력이
상수 또는 선형임을 지정합니다.";

ParameterFrame.ParamTabContainer.TensionPage.TensionFrame.EndTensionFrame.EndTensionButtonFrame.EndTensionComb.LongHelp = 
"혼합의 두 번째 기준에 대한 장력이
상수 또는 선형임을 지정합니다.";

ConstantTensionTitle="상수";
LinearTensionTitle="선형";
STypeTensionTitle="S 유형";

RatioCouplTitle="비율";
TangencyCouplTitle="접점";
CurvatureCouplTitle="접촉 후 곡률";
VertexCouplTitleKey="정점";
SpineCouplTitleKey="축";
AvoidTwistsCouplTitleKey="뒤틀림 방지";

ParameterFrame.ParamTabContainer.TensionPage.TensionFrame.StartTensionFrame.StartValueFrame.StartParmFrame1.LongHelp = 
"장력 유형이 상수 또는 선형인 경우
혼합의 첫 번째 기준에 대한
첫 번째 장력 값을 지정합니다. ";

ParameterFrame.ParamTabContainer.TensionPage.TensionFrame.StartTensionFrame.StartValueFrame.EndParmFrame1.LongHelp = 
"장력 유형이 선형인 경우
혼합의 첫 번째 기준에 대한
두 번째 장력 값을 지정합니다. ";

ParameterFrame.ParamTabContainer.TensionPage.TensionFrame.EndTensionFrame.EndValueFrame.StartParmFrame2.LongHelp = 
"장력 유형이 상수 또는 선형인 경우
혼합의 두 번째 기준에 대한
첫 번째 장력 값을 지정합니다. ";

ParameterFrame.ParamTabContainer.TensionPage.TensionFrame.EndTensionFrame.EndValueFrame.EndParmFrame2.LongHelp = 
"장력 유형이 선형인 경우
혼합의 두 번째 기준에 대한
두 번째 장력 값을 지정합니다. ";

StartExtremityKey="T1: ";
EndExtremityKey="T2:  ";

ChaineDefaut="디폴트";
ParameterFrame.ParamTabContainer.CouplingPage.SpineFrame.SpineLabel.Title="스파인: ";
ParameterFrame.ParamTabContainer.CouplingPage.SpineFrame.SpineLabel.LongHelp="결합 모드가 '스파인'으로 설정된 경우 고려할 \n스파인 커브를 지정합니다.";
ParameterFrame.ParamTabContainer.CouplingPage.SpineFrame.SpineSelector.LongHelp="결합 모드가 '스파인'으로 설정된 경우 고려할 \n스파인 커브를 지정합니다.";

// Tolerant blend
//------------------
FrameSmooth.Title="다듬기 매개변수";

FrameSmooth.SmoothDevButton.Title="편차: ";
FrameSmooth.SmoothDevButton.LongHelp="작업 중 편차를 활성화/비활성화합니다.";
FrameSmooth.FrameLayoutDev.FraLittSmoothDev.EnglobingFrame.IntermediateFrame.Spinner.Title="편차";

FrameSmooth.SmoothAngleButton.Title="각도 정정: ";
FrameSmooth.SmoothAngleButton.LongHelp="작업 중 각도 임계값을 활성화/비활성화합니다.";
FrameSmooth.FrameLayoutAngle.FraLittSmoothAngle.EnglobingFrame.IntermediateFrame.Spinner.Title="각도 정정";

// Developable
ParameterFrame.ParamTabContainer.RuledDevPage.Title = "발전 가능";
ParameterFrame.ParamTabContainer.RuledDevPage.LongHelp = "서피스 경계 Isopar 연결을 사용 또는 사용하지 않고 발전 가능 메쉬서피스를 생성합니다.";

ParameterFrame.ParamTabContainer.RuledDevPage.RuledDevFrame.RuledDevButton.Title = "발전 가능 메쉬서피스 작성";
ParameterFrame.ParamTabContainer.RuledDevPage.RuledDevFrame.RuledDevButton.LongHelp = 
"연산자를 강제 실행하여 발전 가능 메쉬서피스를 생성합니다.";

//ParameterFrame.ParamTabContainer.RuledDevPage.RuledDevFrame.IsoConnectionFrame.Title = "Surface Boundary Isopar Connections";
ParameterFrame.ParamTabContainer.RuledDevPage.RuledDevFrame.IsoConnectionFrame.LabelSepar.Title="서피스 경계 Isopar 연결";

ParameterFrame.ParamTabContainer.RuledDevPage.RuledDevFrame.IsoConnectionFrame.StartFrame.StartLabel.Title = "시작: ";
ParameterFrame.ParamTabContainer.RuledDevPage.RuledDevFrame.IsoConnectionFrame.StartFrame.LongHelp=
"생성된 서피스의 시작 부분에서 연결 조건을 선택하십시오.";

ParameterFrame.ParamTabContainer.RuledDevPage.RuledDevFrame.IsoConnectionFrame.EndFrame.EndLabel.Title = "끝:";
ParameterFrame.ParamTabContainer.RuledDevPage.RuledDevFrame.IsoConnectionFrame.EndFrame.EndLabel.LongHelp=
"생성된 서피스의 끝 부분에서 연결 조건을 선택하십시오.";
