//---------------------------------------------------
// Ressource File for DYSCommandHeader Class
// En_US
//
// LGK Revision 	15/10/2002
// LGK Revision     15/01/2003
// NMB Revision     20/03/2003
//---------------------------------------------------

// Point creation by entering coordinates
DYSCommandHeader.2DPointCoord.Title="使用坐标创建点";
DYSCommandHeader.2DPointCoord.Help="通过输入点的坐标来创建点";
DYSCommandHeader.2DPointCoord.ShortHelp="使用坐标创建点";
DYSCommandHeader.2DPointCoord.LongHelp="点\n通过输入坐标并\n在需要时选择参考点来创建点。";

// Point creation by selection
DYSCommandHeader.2DPoint.Title="点";
DYSCommandHeader.2DPoint.Help="通过单击创建点";
DYSCommandHeader.2DPoint.ShortHelp="通过单击创建点";
DYSCommandHeader.2DPoint.LongHelp="点\n通过单击创建点。";

// Line creation by selecting two points
DYSCommandHeader.2DLine.Title="直线";
DYSCommandHeader.2DLine.Help="使用两点创建直线";
DYSCommandHeader.2DLine.ShortHelp="直线";
DYSCommandHeader.2DLine.LongHelp="直线\n通过选择 \n或单击两点来创建直线。";

// AxisLine creation by selecting two points
DYSCommandHeader.2DAxisLine.Title="轴";
DYSCommandHeader.2DAxisLine.Help="使用两点创建轴";
DYSCommandHeader.2DAxisLine.ShortHelp="轴";
DYSCommandHeader.2DAxisLine.LongHelp="轴\n通过选择 \n或单击两点来创建轴。";

// Circle creation by controlling the radius
DYSCommandHeader.2DCircleRadius.Title="圆";
DYSCommandHeader.2DCircleRadius.Help="使用两点创建圆";
DYSCommandHeader.2DCircleRadius.ShortHelp="圆";
DYSCommandHeader.2DCircleRadius.LongHelp="圆\n通过选择 \n或单击两点来创建圆。";

// Creation d'un arc en selectionnant trois points
DYSCommandHeader.2DCircle3Points.Title="三点圆";
DYSCommandHeader.2DCircle3Points.Help="使用三点创建圆";
DYSCommandHeader.2DCircle3Points.ShortHelp="三点圆";
DYSCommandHeader.2DCircle3Points.LongHelp="三点圆\n通过选择 \n或单击三点来创建圆。";

// Circle creation by entering its coordinates
DYSCommandHeader.2DCircleCoord.Title="使用坐标创建圆";
DYSCommandHeader.2DCircleCoord.Help="通过输入中心点坐标和半径值来创建圆";
DYSCommandHeader.2DCircleCoord.ShortHelp="使用坐标创建圆";
DYSCommandHeader.2DCircleCoord.LongHelp="使用坐标创建圆\n通过输入中心点坐标 \n和半径值来创建圆。";

// Arc creation by controlling the radius
DYSCommandHeader.2DArcRadius.Title="弧";
DYSCommandHeader.2DArcRadius.Help="创建弧";
DYSCommandHeader.2DArcRadius.ShortHelp="弧";
DYSCommandHeader.2DArcRadius.LongHelp="弧\n通过指定半径值创建弧。";

// Arc creation by selecting 3 points
DYSCommandHeader.2DArc3Points.Title="三点弧";
DYSCommandHeader.2DArc3Points.Help="通过三点创建弧";
DYSCommandHeader.2DArc3Points.ShortHelp="三点弧";
DYSCommandHeader.2DArc3Points.LongHelp="三点弧\通过选择或单击 \n三点来创建弧。";

// Arc creation by selecting 3 points first selecting the limits
DYSCommandHeader.2DArc3Points132.Title="起始受限的三点弧";
DYSCommandHeader.2DArc3Points132.Help="通过起始受限的三点来创建弧";
DYSCommandHeader.2DArc3Points132.ShortHelp="起始受限的三点弧";
DYSCommandHeader.2DArc3Points132.LongHelp="三点弧\n通过选择或单击三点 \n但通过选择\n弧限制开始\n来创建弧。";

// Ellipse creation by selecting the center and controlling the radii
DYSCommandHeader.2DEllipseRadius.Title="椭圆";
DYSCommandHeader.2DEllipseRadius.Help="使用三点创建椭圆";
DYSCommandHeader.2DEllipseRadius.ShortHelp="椭圆";
DYSCommandHeader.2DEllipseRadius.LongHelp="椭圆\n通过选择中心点\n并指定半径来创建椭圆。";

// Contour creation
DYSCommandHeader.2DContour.Title="轮廓";
DYSCommandHeader.2DContour.Help="创建由线和弧组成的轮廓";
DYSCommandHeader.2DContour.ShortHelp="轮廓";
DYSCommandHeader.2DContour.LongHelp="轮廓\n创建由线和弧组成的轮廓。";

// Rectangle creation
DYSCommandHeader.2DRectangle.Title="矩形";
DYSCommandHeader.2DRectangle.Help="使用两点创建矩形";
DYSCommandHeader.2DRectangle.ShortHelp="矩形";
DYSCommandHeader.2DRectangle.LongHelp="矩形\n通过单击 \n或选择两点来创建矩形。";

// Centered rectangle creation
DYSCommandHeader.2DCenteredRectangle.Title="居中矩形";
DYSCommandHeader.2DCenteredRectangle.Help="创建居中矩形";
DYSCommandHeader.2DCenteredRectangle.ShortHelp="居中矩形";
DYSCommandHeader.2DCenteredRectangle.LongHelp="居中矩形\n通过单击 \n或选择一个点来创建居中矩形。";

// Centered parallelogram creation
DYSCommandHeader.2DCenteredParallelogram.Title="居中平行四边形";
DYSCommandHeader.2DCenteredParallelogram.Help="创建居中平行四边形";
DYSCommandHeader.2DCenteredParallelogram.ShortHelp="居中平行四边形";
DYSCommandHeader.2DCenteredParallelogram.LongHelp="居中平行四边形\n通过单击或选择两条直线\n来创建居中平行四边形。";

// Oriented Rectangle creation
DYSCommandHeader.2DOrientedRectangle.Title="斜置矩形";
DYSCommandHeader.2DOrientedRectangle.Help="使用三点创建斜置矩形";
DYSCommandHeader.2DOrientedRectangle.ShortHelp="斜置矩形";
DYSCommandHeader.2DOrientedRectangle.LongHelp="斜置矩形\n通过选择或单击三点 \n创建斜置矩形。";

// Parallelogram creation
DYSCommandHeader.2DParallelogram.Title="平行四边形";
DYSCommandHeader.2DParallelogram.Help="通过选择三点创建平行四边形";
DYSCommandHeader.2DParallelogram.ShortHelp="平行四边形";
DYSCommandHeader.2DParallelogram.LongHelp="平行四边形\n通过选择三点创建平行四边形。";

// Curve creation
DYSCommandHeader.2DSpline.Title="样条线";
DYSCommandHeader.2DSpline.Help="通过单击或选择点来创建样条线";
DYSCommandHeader.2DSpline.ShortHelp="样条线";
DYSCommandHeader.2DSpline.LongHelp="样条线\n通过单击或选择点来创建样条线。";

// UseEdge by Intersection
DYSCommandHeader.2DUse3DByIntersect.Title="与 3D 元素相交";
DYSCommandHeader.2DUse3DByIntersect.Help="使 3D 元素与草图平面相交";
DYSCommandHeader.2DUse3DByIntersect.ShortHelp="与 3D 元素相交";
DYSCommandHeader.2DUse3DByIntersect.LongHelp="与 3D 相交\n使 3D 元素与草图平面相交。\n相交部位显示为黄色。";

// UseEdge by Intersection with panel
DYSCommandHeader.2DUseEdgeIntersection.Title="与 3D 元素相交";
DYSCommandHeader.2DUseEdgeIntersection.Help="使 3D 元素与草图平面相交";
DYSCommandHeader.2DUseEdgeIntersection.ShortHelp="与 3D 元素相交";
DYSCommandHeader.2DUseEdgeIntersection.LongHelp="与 3D 相交\n使 3D 元素与草图平面相交。\n相交部位显示为黄色。";

// UseEdge by Projection
DYSCommandHeader.2DUse3DByProject.Title="投影 3D 元素";
DYSCommandHeader.2DUse3DByProject.Help="将 3D 元素投影到草图平面上";
DYSCommandHeader.2DUse3DByProject.ShortHelp="投影 3D 元素";
DYSCommandHeader.2DUse3DByProject.LongHelp="投影 3D\n将 3D 元素投影到草图平面上。\n投影显示为黄色。";

// UseEdge by Projection with panel
DYSCommandHeader.2DUseEdgeProjection.Title="投影 3D 元素";
DYSCommandHeader.2DUseEdgeProjection.Help="将 3D 元素投影到草图平面上";
DYSCommandHeader.2DUseEdgeProjection.ShortHelp="投影 3D 元素";
DYSCommandHeader.2DUseEdgeProjection.LongHelp="投影 3D\n将 3D 元素投影到草图平面上。\n投影显示为黄色。";

// Isolate a UseEdge 
DYSCommandHeader.2DIsolate.Title="隔离";
DYSCommandHeader.2DIsolate.Help="将投影和相交部位与其原支持面隔离";
DYSCommandHeader.2DIsolate.ShortHelp="隔离投影和相交部位";
DYSCommandHeader.2DIsolate.LongHelp="将投影和相交部位\n与其原支持面隔离。";

// Fix Element 
DYSCommandHeader.2DFix.Title="固定";
DYSCommandHeader.2DFix.Help="固定元素位置";
DYSCommandHeader.2DFix.ShortHelp="固定元素";
DYSCommandHeader.2DFix.LongHelp="固定\n固定元素位置。\n但是，仍然可以编辑端点。";

// Horizontal Element 
DYSCommandHeader.2DHorizontal.Title="水平";
DYSCommandHeader.2DHorizontal.Help="使选定元素处于水平方向";
DYSCommandHeader.2DHorizontal.ShortHelp="使之处于水平方向";
DYSCommandHeader.2DHorizontal.LongHelp="水平\n使选定元素处于水平方向。";

// Vertical Element 
DYSCommandHeader.2DVertical.Title="竖直";
DYSCommandHeader.2DVertical.Help="使选定元素处于垂直方向";
DYSCommandHeader.2DVertical.ShortHelp="使之处于垂直方向";
DYSCommandHeader.2DVertical.LongHelp="垂直\n使选定元素处于垂直方向。";

// Unfix Element 
DYSCommandHeader.2DUnfix.Title="取消固定";
DYSCommandHeader.2DUnfix.Help="取消固定元素位置";
DYSCommandHeader.2DUnfix.ShortHelp="取消固定元素";
DYSCommandHeader.2DUnfix.LongHelp="取消固定\n取消固定元素位置。";

// 2D Constraint 
DYSCommandHeader.2DConstraint.Title="约束";
DYSCommandHeader.2DConstraint.Help="创建几何约束和尺寸约束";
DYSCommandHeader.2DConstraint.ShortHelp="约束";
DYSCommandHeader.2DConstraint.LongHelp="约束\n创建几何约束或 \n尺寸约束。";

// Corner 
DYSCommandHeader.2DCorner.Title="圆角";
DYSCommandHeader.2DCorner.Help="创建圆角";
DYSCommandHeader.2DCorner.ShortHelp="圆角";
DYSCommandHeader.2DCorner.LongHelp="圆角\n创建圆角。您可以指定是否应修剪元素。";

// Chamfer
DYSCommandHeader.2DChamfer.Title="倒角";
DYSCommandHeader.2DChamfer.Help="创建修剪或不修剪元素的倒角";
DYSCommandHeader.2DChamfer.ShortHelp="倒角";
DYSCommandHeader.2DChamfer.LongHelp="倒角\n创建倒角（有斜面的角）。\n您可以指定是否应修剪元素。";

// Relimitation
DYSCommandHeader.2DRelimitation.Title="修剪";
DYSCommandHeader.2DRelimitation.Help="修剪元素";
DYSCommandHeader.2DRelimitation.ShortHelp="修剪";
DYSCommandHeader.2DRelimitation.LongHelp="修剪\n修剪元素。光标的位置\n 确定如何修剪元素。";

// Break Curves
DYSCommandHeader.2DBreak.Title="断开";
DYSCommandHeader.2DBreak.Help="断开元素";
DYSCommandHeader.2DBreak.ShortHelp="断开";
DYSCommandHeader.2DBreak.LongHelp="断开\n断开元素。";

// 2D Mirror
DYSCommandHeader.2DSymmetry.Title="镜像";
DYSCommandHeader.2DSymmetry.Help="使用线或轴来创建对称元素";
DYSCommandHeader.2DSymmetry.ShortHelp="镜像";
DYSCommandHeader.2DSymmetry.LongHelp="镜像\n使用线或轴来创建对称元素。";

// 2D Symmetry
DYSCommandHeader.2DPureSymmetry.Title="对称";
DYSCommandHeader.2DPureSymmetry.Help="使用线或轴来创建对称元素（不重复）";
DYSCommandHeader.2DPureSymmetry.ShortHelp="对称";
DYSCommandHeader.2DPureSymmetry.LongHelp="对称\n使用线或轴来创建对称元素（不重复）。";

// Exit Sketcher
DYSCommandHeader.ExitSketcher.Title="退出草图编辑器";
DYSCommandHeader.ExitSketcher.Help="关闭“草图编辑器”应用程序";
DYSCommandHeader.ExitSketcher.ShortHelp="退出";
DYSCommandHeader.ExitSketcher.LongHelp="退出\n关闭“草图编辑器”应用程序。\n然后您便可以创建特征了。";

// Sketcher Settings
DYSCommandHeader.SketcherSettings.Title="设置";
DYSCommandHeader.SketcherSettings.Help="设置草图编辑器设置";
DYSCommandHeader.SketcherSettings.ShortHelp="设置";
DYSCommandHeader.SketcherSettings.LongHelp="设置\n设置草图编辑器设置";

// DYSNormalToView
DYSCommandHeader.NormalViewHeader.Title="草图的视图法向";
DYSCommandHeader.NormalViewHeader.Help="设置草图的视图法向";
DYSCommandHeader.NormalViewHeader.ShortHelp="草图的视图法向";
DYSCommandHeader.NormalViewHeader.LongHelp="草图的视图法向\n设置草图的视图法向。";

// Equation
DYSCommandHeader.CalculatorHeader.Title="公式";
DYSCommandHeader.CalculatorHeader.Help="创建公式";
DYSCommandHeader.CalculatorHeader.ShortHelp="公式";
DYSCommandHeader.CalculatorHeader.LongHelp="公式\n创建公式。";

// Update
DYSCommandHeader.UpdateSketchHeader.Title="更新";
DYSCommandHeader.UpdateSketchHeader.Help="更新零件";
DYSCommandHeader.UpdateSketchHeader.ShortHelp="更新 (Ctrl+U)";
DYSCommandHeader.UpdateSketchHeader.LongHelp="更新\n更新零件。在草图编辑器中 \n会自动执行更新。";

// DisplayGrid
DYSCommandHeader.DisplayGrid.Title="显示网格";
DYSCommandHeader.DisplayGrid.Help="交替地显示或隐藏草图编辑器网格";
DYSCommandHeader.DisplayGrid.ShortHelp="草图编辑器网格";
DYSCommandHeader.DisplayGrid.LongHelp="显示网格\n在您创建草图时显示网格以进行引导。";

// SnapGrid
DYSCommandHeader.SnapGrid.Title="点对齐";
DYSCommandHeader.SnapGrid.Help="与网格上的点对齐";
DYSCommandHeader.SnapGrid.ShortHelp="点对齐";
DYSCommandHeader.SnapGrid.LongHelp="使您创建的各点\n与网格的最近\n相交点对齐。";

// CreateConstraint
DYSCommandHeader.CreateCstCplt.Title="创建检测到的约束和内部约束";
DYSCommandHeader.CreateCstCplt.Help="在您绘制草图时创建检测到的约束和内部约束";
DYSCommandHeader.CreateCstCplt.ShortHelp="几何约束";
DYSCommandHeader.CreateCstCplt.LongHelp="创建检测到的约束和内部约束\n在您绘制草图时创建\n检测到的约束和内部约束。";

// ValidateConstraint
DYSCommandHeader.ValidateConstraint.Title="创建尺寸约束";
DYSCommandHeader.ValidateConstraint.Help="在您绘制草图时，根据元素参数中的关键字来创建约束";
DYSCommandHeader.ValidateConstraint.ShortHelp="尺寸约束";
DYSCommandHeader.ValidateConstraint.LongHelp="创建尺寸约束\n在您绘制草图时，根据元素参数中\n的关键字来创建约束。";

//CreateAutomaticDimensionalConstraints (E2Z)
DYSCommandHeader.CreateAutoDimCst.Title="自动尺寸约束";
DYSCommandHeader.CreateAutoDimCst.Help="在您绘制草图时，自动创建检测到的尺寸约束";
DYSCommandHeader.CreateAutoDimCst.ShortHelp="自动尺寸约束";
DYSCommandHeader.CreateAutoDimCst.LongHelp="自动创建检测到的尺寸约束\n在您绘制草图时，\n自动创建检测到的尺寸约束。";

// CATSwitchHeaderClipping
DYSCommandHeader.SwitchClipping.Title="切除面";
DYSCommandHeader.SwitchClipping.Help="按草图平面剪切零件";
DYSCommandHeader.SwitchClipping.ShortHelp="按草图平面剪切零件";
DYSCommandHeader.SwitchClipping.LongHelp="按草图平面剪切零件\n按草图平面剪切零件。";

// CATSwitchHeaderInSelect
DYSCommandHeader.SwitchEltType.Title="构造/标准元素";
DYSCommandHeader.SwitchEltType.Help="将草图编辑器元素交替地转换为“构造”或“标准”元素";
DYSCommandHeader.SwitchEltType.ShortHelp="构造/标准元素";
DYSCommandHeader.SwitchEltType.LongHelp="构造/标准元素\n将草图编辑器元素转换为“构造”或“标准”元素。";

// 2D Auto-Constraint 
DYSCommandHeader.2DAutoCst.Title="自动约束";
DYSCommandHeader.2DAutoCst.Help="自动创建几何约束和尺寸约束";
DYSCommandHeader.2DAutoCst.ShortHelp="自动约束";
DYSCommandHeader.2DAutoCst.LongHelp="自动约束\n自动创建几何约束或尺寸约束。";

// Animate Constraint 
DYSCommandHeader.2DAnimateCst.Title="对约束应用动画";
DYSCommandHeader.2DAnimateCst.Help="制作尺寸约束动画";
DYSCommandHeader.2DAnimateCst.ShortHelp="对约束应用动画";
DYSCommandHeader.2DAnimateCst.LongHelp="制作约束动画\n制作尺寸约束的动画，以显示受约束系统的反应。";

// Oblong creation  
DYSCommandHeader.2DTrouOblong.Title="延长孔";
DYSCommandHeader.2DTrouOblong.Help="创建延长孔";
DYSCommandHeader.2DTrouOblong.ShortHelp="延长孔 ";
DYSCommandHeader.2DTrouOblong.LongHelp="延长孔\n创建延长孔。";

// OblongArc creation   
DYSCommandHeader.2DTrouOblongArc.Title="圆柱形延长孔";
DYSCommandHeader.2DTrouOblongArc.Help="创建圆柱形延长孔";
DYSCommandHeader.2DTrouOblongArc.ShortHelp="圆柱形延长孔";
DYSCommandHeader.2DTrouOblongArc.LongHelp="圆柱形延长孔 \n创建圆柱形延长孔。";

// Keyhole profile creation   
DYSCommandHeader.2DKeyHole.Title="钥匙孔轮廓";
DYSCommandHeader.2DKeyHole.Help="创建钥匙孔轮廓";
DYSCommandHeader.2DKeyHole.ShortHelp="钥匙孔轮廓";
DYSCommandHeader.2DKeyHole.LongHelp="钥匙孔轮廓\n创建钥匙孔轮廓。";

// Hexagon profile creation   
DYSCommandHeader.2DHexagon.Title="六边形";
DYSCommandHeader.2DHexagon.Help="通过定义外接圆半径来创建六边形";
DYSCommandHeader.2DHexagon.ShortHelp="六边形";
DYSCommandHeader.2DHexagon.LongHelp="六边形\n通过定义外接圆半径来创建六边形。";

// Points spaces (equidistant) creation   
DYSCommandHeader.2DPointsSpaces.Title="等距点";
DYSCommandHeader.2DPointsSpaces.Help="在支持面上创建等距点";
DYSCommandHeader.2DPointsSpaces.ShortHelp="等距点";
DYSCommandHeader.2DPointsSpaces.LongHelp="等距点\n在线或曲线上，\n根据指定的原点、指定的间距\n以及指定的新点数创建等距点。";

// 2D Translate
DYSCommandHeader.2DTranslate.Title="平移";
DYSCommandHeader.2DTranslate.Help="平移元素";
DYSCommandHeader.2DTranslate.ShortHelp="平移";
DYSCommandHeader.2DTranslate.LongHelp="平移\n平移元素。";

// 2D Rotate
DYSCommandHeader.2DRotate.Title="旋转";
DYSCommandHeader.2DRotate.Help="旋转元素";
DYSCommandHeader.2DRotate.ShortHelp="旋转";
DYSCommandHeader.2DRotate.LongHelp="旋转\n旋转元素。";

// 2D Scale
DYSCommandHeader.2DScale.Title="缩放";
DYSCommandHeader.2DScale.Help="缩放元素";
DYSCommandHeader.2DScale.ShortHelp="缩放";
DYSCommandHeader.2DScale.LongHelp="缩放\n缩放元素。";

// 2D ParabolaFocus
DYSCommandHeader.2DParabolaFocus.Title="通过焦点创建抛物线";
DYSCommandHeader.2DParabolaFocus.Help="通过焦点创建抛物线";
DYSCommandHeader.2DParabolaFocus.ShortHelp="通过焦点创建抛物线";
DYSCommandHeader.2DParabolaFocus.LongHelp="抛物线\n通过焦点创建抛物线。";

// 2D HyperbolaFocus
DYSCommandHeader.2DHyperbolaFocus.Title="通过焦点创建双曲线";
DYSCommandHeader.2DHyperbolaFocus.Help="通过焦点创建双曲线";
DYSCommandHeader.2DHyperbolaFocus.ShortHelp="通过焦点创建双曲线";
DYSCommandHeader.2DHyperbolaFocus.LongHelp="双曲线\n通过焦点创建双曲线。";

// 2D Auto-Search
DYSCommandHeader.2DAutoSearch.Title="自动搜索";
DYSCommandHeader.2DAutoSearch.Help="自动选择 2D 元素";
DYSCommandHeader.2DAutoSearch.ShortHelp="自动搜索";
//DYSCommandHeader.2DAutoSearch.LongHelp="Auto Search\nAutomatically selects 2D elements.";

// 2D Auto-Search Tangency Propagation
DYSCommandHeader.2DAutoSearchTP.Title="使用相切拓展自动搜索";
DYSCommandHeader.2DAutoSearchTP.Help="自动选择相切 2D 元素";
DYSCommandHeader.2DAutoSearchTP.ShortHelp="自动搜索相切拓展";

// 2D Concatenate Curve
DYSCommandHeader.2DConcatenate.Title="连接曲线";
DYSCommandHeader.2DConcatenate.Help="连接选定的 2D 曲线元素";
DYSCommandHeader.2DConcatenate.ShortHelp="连接曲线";
DYSCommandHeader.2DConcatenate.LongHelp="连接选定的 2D 曲线元素";

// 2D Control Point
DYSCommandHeader.2DAddControlPoint.Title="添加控制点： ";
DYSCommandHeader.2DAddControlPoint.Help="向曲线添加控制点";
DYSCommandHeader.2DAddControlPoint.ShortHelp="添加控制点";
DYSCommandHeader.2DAddControlPoint.LongHelp="控制点\n向曲线添加控制点";

// MeasureBetween == 
DYSCommandHeader.MeasureBetween.Title="测量间距...";
DYSCommandHeader.MeasureBetween.Help="在两个元素之间进行测量";
DYSCommandHeader.MeasureBetween.LongHelp="测量间距\n在两个元素之间进行测量。";
DYSCommandHeader.MeasureBetween.ShortHelp="测量间距";

// MeasureItem == 
DYSCommandHeader.MeasureItem.Title="测量...";
DYSCommandHeader.MeasureItem.Help="测量元素";
DYSCommandHeader.MeasureItem.LongHelp="测量\n测量元素。";
DYSCommandHeader.MeasureItem.ShortHelp="测量";

// Bi-Tangent Line creation by selecting two elements
DYSCommandHeader.2DLine2Tangent.Title="双切线";
DYSCommandHeader.2DLine2Tangent.Help="通过选择两个元素来创建双切线";
DYSCommandHeader.2DLine2Tangent.ShortHelp="双切线";
DYSCommandHeader.2DLine2Tangent.LongHelp="双切线\n通过选择两个元素\n来创建双切线。";

// Tri-Tangent Circle creation by selecting three elements
DYSCommandHeader.2DCircle3Tangent.Title="三切线圆";
DYSCommandHeader.2DCircle3Tangent.Help="通过选择三个元素来创建三切线圆";
DYSCommandHeader.2DCircle3Tangent.ShortHelp="三切线圆";
DYSCommandHeader.2DCircle3Tangent.LongHelp="三切线圆\n通过选择三个元素 \n来创建三切线圆。";

// 2D Contact Constraint 
DYSCommandHeader.2DContactConstraint.Title="接触约束";
DYSCommandHeader.2DContactConstraint.Help="创建接触约束";
DYSCommandHeader.2DContactConstraint.ShortHelp="接触约束";
DYSCommandHeader.2DContactConstraint.LongHelp="接触约束\n创建诸如同心、相合和相切等接触约束。";

// 2D Analyse 
DYSCommandHeader.2DAnalyse.Title="草图分析";
DYSCommandHeader.2DAnalyse.Help="执行草图分析";
DYSCommandHeader.2DAnalyse.ShortHelp="草图分析";
DYSCommandHeader.2DAnalyse.LongHelp="草图分析\n分析当前草图中的所有几何图形。";

// 2D Light Analyse 
DYSCommandHeader.2DLightAnalyse.Title="草图求解状态";
DYSCommandHeader.2DLightAnalyse.Help="显示草图求解状态。";
DYSCommandHeader.2DLightAnalyse.ShortHelp="草图求解状态";
DYSCommandHeader.2DLightAnalyse.LongHelp="草图求解状态\n显示草图求解状态。";

// Activate a UseEdge inactivated
DYSCommandHeader.2DActivate.Title="激活";
DYSCommandHeader.2DActivate.Help="激活由于更新错误而取消激活的投影和相交。";
DYSCommandHeader.2DActivate.ShortHelp="激活投影和相交";
DYSCommandHeader.2DActivate.LongHelp="激活\n激活因更新错误而取消激活\n的投影和相交。";

// 2D Conic
DYSCommandHeader.2DConic.Title="二次曲线";
DYSCommandHeader.2DConic.Help="创建二次曲线";
DYSCommandHeader.2DConic.ShortHelp="创建二次曲线";
DYSCommandHeader.2DConic.LongHelp="二次曲线\n通过五个点或者一条或两条切线来创建二次曲线。";

// 2D Conic Curve
DYSCommandHeader.2DConicCurve.Title="二次曲线";
DYSCommandHeader.2DConicCurve.Help="通过单击或选择若干点或曲线来创建二次曲线";
DYSCommandHeader.2DConicCurve.ShortHelp="二次曲线";
DYSCommandHeader.2DConicCurve.LongHelp="二次曲线\n通过两个、四个和五个点以及两条切线来创建参数二次曲线。";

// 2D Offset Contour
DYSCommandHeader.2DOffsetContour.Title="偏移";
DYSCommandHeader.2DOffsetContour.Help="使用偏移创建元素";
DYSCommandHeader.2DOffsetContour.ShortHelp="偏移";
DYSCommandHeader.2DOffsetContour.LongHelp="偏移\n使用偏移创建元素。";

// --------------------------------------------------
// -- commandes CXR6 dans le sketcher --
// --------------------------------------------------
// Point creation by intersection
DYSCommandHeader.2DPointIntersect.Title="相交点";
DYSCommandHeader.2DPointIntersect.Help="通过使元素相交来创建点";
DYSCommandHeader.2DPointIntersect.ShortHelp="相交点";
DYSCommandHeader.2DPointIntersect.LongHelp="点\n在选定元素的相交部分创建点。";

// Point creation by Projection
DYSCommandHeader.2DPointProject.Title="投影点";
DYSCommandHeader.2DPointProject.Help="通过投影创建点";
DYSCommandHeader.2DPointProject.ShortHelp="投影点";
DYSCommandHeader.2DPointProject.LongHelp="点\n通过将点投影到元素上来创建点。";

// Infinite line (H/V) creation by selecting one or two points
DYSCommandHeader.2DLineHV.Title="无限长线";
DYSCommandHeader.2DLineHV.Help="创建水平的、垂直的或经过两点的无限长线";
DYSCommandHeader.2DLineHV.ShortHelp="无限长线";
DYSCommandHeader.2DLineHV.LongHelp="无限长线\n创建水平的、\n垂直的或经过两点的无限长线。";

// QuickTrim
DYSCommandHeader.2DQuickTrim.Title="快速修剪";
DYSCommandHeader.2DQuickTrim.Help="快速修剪元素";
DYSCommandHeader.2DQuickTrim.ShortHelp="快速修剪";
DYSCommandHeader.2DQuickTrim.LongHelp="快速修剪\n快速修剪元素。光标的位置\n 决定如何修剪元素。";

// Close
DYSCommandHeader.2DClose.Title="封闭弧";
DYSCommandHeader.2DClose.Help="封闭圆或椭圆弧，以及重新限定的样条线";
DYSCommandHeader.2DClose.ShortHelp="封闭弧";
DYSCommandHeader.2DClose.LongHelp="封闭弧\n封闭圆或椭圆弧，以及重新限定的样条线。";

// Complement Circle or Ellipse arc
DYSCommandHeader.2DComplement.Title="补充";
DYSCommandHeader.2DComplement.Help="补充圆或椭圆的弧";
DYSCommandHeader.2DComplement.ShortHelp="补充";
DYSCommandHeader.2DComplement.LongHelp="补充\n补充圆或椭圆的弧。";

// Infinite bissecting line creation by selecting two lines
DYSCommandHeader.2DBisectLine.Title="角平分线";
DYSCommandHeader.2DBisectLine.Help="选择两条直线来创建无限角平分线";
DYSCommandHeader.2DBisectLine.ShortHelp="角平分线";
DYSCommandHeader.2DBisectLine.LongHelp="角平分线\n选择两条现有直线\n来创建无限角平分线。";

// Line Normal To Curve
DYSCommandHeader.2DLineNormalToCurve.Title="曲线的法线";
DYSCommandHeader.2DLineNormalToCurve.Help="创建曲线的法线";
DYSCommandHeader.2DLineNormalToCurve.ShortHelp="曲线的法线";
DYSCommandHeader.2DLineNormalToCurve.LongHelp="曲线的法线\n通过创建一个点并选择一条现有曲线\n来创建曲线的法线。";

// Connect
DYSCommandHeader.2DConnect.Title="连接";
DYSCommandHeader.2DConnect.Help="创建连接曲线";
DYSCommandHeader.2DConnect.ShortHelp="连接";
DYSCommandHeader.2DConnect.LongHelp="连接\n创建用于连接两条曲线的弧。";

// UseEdge by Silhouette
DYSCommandHeader.2DUse3DBySilhouette.Title="投影 3D 轮廓边线";
DYSCommandHeader.2DUse3DBySilhouette.Help="对草图平面投影 3D 元素的轮廓边线";
DYSCommandHeader.2DUse3DBySilhouette.ShortHelp="投影 3D 轮廓边线";
DYSCommandHeader.2DUse3DBySilhouette.LongHelp="投影 3D 轮廓边线\n对草图平面投影 3D 元素的轮廓边线。\n轮廓边线的投影显示为黄色。";

// UseEdge by Canonical Silhouette
DYSCommandHeader.2DOldUse3DBySilhouette.Title="投影 3D 标准侧影轮廓边线";
DYSCommandHeader.2DOldUse3DBySilhouette.Help="对草图平面投影 3D 元素的正则轮廓边线";
DYSCommandHeader.2DOldUse3DBySilhouette.ShortHelp="投影 3D 标准侧影轮廓边线";
DYSCommandHeader.2DOldUse3DBySilhouette.LongHelp="投影 3D 轮廓边线\n对草图平面投影 3D 元素的正则轮廓边线。\n轮廓边线的投影显示为黄色。";


// UseEdge Silhouette HLR
DYSCommandHeader.2DSilhouetteHLR.Title="投影 3D 轮廓和可视边线";
DYSCommandHeader.2DSilhouetteHLR.Help="对草图平面投影 3D 元素的轮廓和可视边线";
DYSCommandHeader.2DSilhouetteHLR.ShortHelp="投影 3D 轮廓和可视边线";
DYSCommandHeader.2DSilhouetteHLR.LongHelp="投影 3D 轮廓和可视边线\n根据无关联运算对草图平面投影 3D 元素的轮廓和可视边线。";

// Explode Polyline
DYSCommandHeader.2DExplode.Title="分解";
DYSCommandHeader.2DExplode.Help="分解选定折线";
DYSCommandHeader.2DExplode.ShortHelp="分解折线";
DYSCommandHeader.2DExplode.LongHelp="通过将选定的折线转换成\n连续的线来分解它。";

// Commande d'Edition Line   
DYSCommandHeader.2DEditLine.Title="几何图形定义...";
DYSCommandHeader.2DEditLine.Help="编辑选定元素的几何图形";

// Commande d'Edition Point   
DYSCommandHeader.2DEditPoint.Title="几何图形定义...";
DYSCommandHeader.2DEditPoint.Help="编辑选定元素的几何图形";

// Commande d'Edition Cercle  
DYSCommandHeader.2DEditCircle.Title="几何图形定义...";
DYSCommandHeader.2DEditCircle.Help="编辑选定元素的几何图形";

// Commande d'Edition Ellipse   
DYSCommandHeader.2DEditEllipse.Title="几何图形定义...";
DYSCommandHeader.2DEditEllipse.Help="编辑选定元素的几何图形";

// 2D Curvature Analysis
DYSCommandHeader.2DCurvatureAnalysis.Title="曲率分析";
DYSCommandHeader.2DCurvatureAnalysis.Help="对当前草图执行或编辑曲率分析";
DYSCommandHeader.2DCurvatureAnalysis.ShortHelp="曲率分析";
DYSCommandHeader.2DCurvatureAnalysis.LongHelp="曲率分析\n对当前草图执行或编辑曲率分析。";

// 2D Output feature
DYSCommandHeader.2DOutput.Title="输出特征";
DYSCommandHeader.2DOutput.Help="创建输出特征。";
DYSCommandHeader.2DOutput.ShortHelp="输出特征";
DYSCommandHeader.2DOutput.LongHelp="输出特征\n通过选择 2D 几何图形来创建输出特征。";

// 2D Fix Together
DYSCommandHeader.2DFixTogether.Title="固联";
DYSCommandHeader.2DFixTogether.Help="将元素连接在一起以创建一个固定集合。";
DYSCommandHeader.2DFixTogether.ShortHelp="固联";
DYSCommandHeader.2DFixTogether.LongHelp="将元素连接在一起以创建一个固定集合。";

DYSCommandHeader.SelGeoms2DFixTogether.Title="选择几何元素";
DYSCommandHeader.SelGeoms2DFixTogether.Help="选择属于“固联”的几何元素。";
DYSCommandHeader.SelGeoms2DFixTogether.ShortHelp="选择几何元素";
DYSCommandHeader.SelGeoms2DFixTogether.LongHelp="选择属于“固联”的几何元素。";

// 2D OutputProfile feature
//DYSCommandHeader.2DOutputProfile.Title="Profile feature";
DYSCommandHeader.2DOutputProfile.Title="3D 轮廓";
DYSCommandHeader.2DOutputProfile.Help="创建轮廓特征。";
DYSCommandHeader.2DOutputProfile.ShortHelp="轮廓特征";
DYSCommandHeader.2DOutputProfile.LongHelp="轮廓特征\n通过选择 2D 几何图形来创建轮廓特征。";

// Commande d'Edition output profile  
DYSCommandHeader.2DEditOutputProfile.Title="轮廓特征定义...";
DYSCommandHeader.2DEditOutputProfile.Help="编辑与选定几何图形匹配的轮廓特征。";

//////////////visu toolbar ///////////////////////////////////////////
// Visu Grid
DYSCommandHeader.VisuGrid.Title="网格";
DYSCommandHeader.VisuGrid.Help="交替地显示或隐藏草图编辑器网格";
DYSCommandHeader.VisuGrid.ShortHelp="网格";
DYSCommandHeader.VisuGrid.LongHelp="显示网格\n在您创建草图时显示网格以进行引导。";

// 3D grid parameters
DYSCommandHeader.Skt3DGridParam.Title="3D 网格参数";
DYSCommandHeader.Skt3DGridParam.Help="获取当前 3D 网格的 3D 参数";
DYSCommandHeader.Skt3DGridParam.ShortHelp="3D 网格参数";
DYSCommandHeader.Skt3DGridParam.LongHelp="3D 网格参数\n获取当前 3D 网格的 3D 参数，并将其应用于草图。";

// Visu Diagnostics
DYSCommandHeader.VisuDiag.Title="诊断";
DYSCommandHeader.VisuDiag.Help="交替地显示或隐藏求解器的诊断";
DYSCommandHeader.VisuDiag.ShortHelp="诊断";
DYSCommandHeader.VisuDiag.LongHelp="显示诊断\n在草图编辑器的每个元素上用颜色显示解析诊断。";

// Visu Dimensional constraints
DYSCommandHeader.VisuDimCst.Title="尺寸约束";
DYSCommandHeader.VisuDimCst.Help="交替地显示或隐藏尺寸约束";
DYSCommandHeader.VisuDimCst.ShortHelp="尺寸约束";
DYSCommandHeader.VisuDimCst.LongHelp="显示尺寸约束\n显示草图编辑器的尺寸约束。";

// Visu Geometrical constraints
DYSCommandHeader.VisuGeomCst.Title="几何约束";
DYSCommandHeader.VisuGeomCst.Help="交替地显示或隐藏几何约束";
DYSCommandHeader.VisuGeomCst.ShortHelp="几何约束";
DYSCommandHeader.VisuGeomCst.LongHelp="显示几何约束\n显示草图编辑器的几何约束。";

// Visu No 3D Background

DYSCommandHeader.UsualVisu.Title="常用";
DYSCommandHeader.UsualVisu.Help="显示背景中的 3D 元素";
DYSCommandHeader.UsualVisu.ShortHelp="常用";
DYSCommandHeader.UsualVisu.LongHelp="显示草图编辑器背景中的 3D 元素。";

DYSCommandHeader.No3DBackground.Title="无 3D 背景";
DYSCommandHeader.No3DBackground.Help="不显示背景中的 3D 元素";
DYSCommandHeader.No3DBackground.ShortHelp="无 3D 背景";
DYSCommandHeader.No3DBackground.LongHelp="不显示草图编辑器背景中的 3D 元素。";

DYSCommandHeader.LowLightNoPick.Title="低光度";
DYSCommandHeader.LowLightNoPick.Help="以低光度显示背景中的 3D 元素 ";
DYSCommandHeader.LowLightNoPick.ShortHelp="低光度";
DYSCommandHeader.LowLightNoPick.LongHelp="以低光度显示草图编辑器背景中的 3D 元素。";

// Constraint Multi-Edition
DYSCommandHeader.2DConstraintMultiEdition.Title="编辑多重约束";
DYSCommandHeader.2DConstraintMultiEdition.Help="修改一组约束 ";
DYSCommandHeader.2DConstraintMultiEdition.ShortHelp="编辑多重约束";
DYSCommandHeader.2DConstraintMultiEdition.LongHelp="编辑约束值并在最后求出受约束的几何图形。";

// ---------------------------------------------------------------------------------------
// -- Temporaire BMW Section Management - Partage ressources Section toolbar impossible --
// ---------------------------------------------------------------------------------------

// == InsertSection ==
DYSCommandHeader.CATSdwInsertSectionHdr.Title="插入截面";
DYSCommandHeader.CATSdwInsertSectionHdr.Help="在零件中插入截面";
DYSCommandHeader.CATSdwInsertSectionHdr.ShortHelp="插入截面";
DYSCommandHeader.CATSdwInsertSectionHdr.LongHelp="插入截面
在零件中插入截面。";

// == Insert2DViewSection ==
DYSCommandHeader.CATSdwInsert2DViewSectionHdr.Title="插入 2D 视图截面";
DYSCommandHeader.CATSdwInsert2DViewSectionHdr.Help="在零件中插入 2D 视图截面";
DYSCommandHeader.CATSdwInsert2DViewSectionHdr.ShortHelp="插入 2D 视图截面";
DYSCommandHeader.CATSdwInsert2DViewSectionHdr.LongHelp="插入 2D 视图截面
在零件中插入 2D 视图截面。";

// == Selective loading ==
DYSCommandHeader.CATSdwSelectiveLoadingFromSketcherHdr.Title="选择性加载";
DYSCommandHeader.CATSdwSelectiveLoadingFromSketcherHdr.Help="向截面编辑添加部件";
DYSCommandHeader.CATSdwSelectiveLoadingFromSketcherHdr.ShortHelp="选择性加载";
DYSCommandHeader.CATSdwSelectiveLoadingFromSketcherHdr.LongHelp="选择性加载
向截面编辑添加部件。";

// == DefineInWorkSection ==
DYSCommandHeader.CATSdwDefineInWorkSectionHdr.Title="定义工作截面";
DYSCommandHeader.CATSdwDefineInWorkSectionHdr.Help="将截面定义为工作对象";
DYSCommandHeader.CATSdwDefineInWorkSectionHdr.ShortHelp="将截面定义为工作对象";
DYSCommandHeader.CATSdwDefineInWorkSectionHdr.LongHelp="将截面定义为工作对象";

// == SectionTemporaryHide ==
DYSCommandHeader.CATSdwSTHTemporaryHideHdr.Title="仅显示选定元素";
DYSCommandHeader.CATSdwSTHTemporaryHideHdr.Help="仅临时显示选定元素";
DYSCommandHeader.CATSdwSTHTemporaryHideHdr.ShortHelp="仅显示选定元素";
DYSCommandHeader.CATSdwSTHTemporaryHideHdr.LongHelp="在调用“恢复”前，仅临时过滤关于选定元素的视图";

// == SectionTemporary Low Light No Pick ==
DYSCommandHeader.CATSdwSTHTemporaryLLNPHdr.Title="仅突出显示选定元素";
DYSCommandHeader.CATSdwSTHTemporaryLLNPHdr.Help="仅临时突出显示选定元素";
DYSCommandHeader.CATSdwSTHTemporaryLLNPHdr.ShortHelp="仅突出显示选定元素";
DYSCommandHeader.CATSdwSTHTemporaryLLNPHdr.LongHelp="在调用“恢复”前，临时过滤关于选定元素的视图";

// == SectionTHRestore ==
DYSCommandHeader.CATSdwSTHRestoreHdr.Title="恢复";
DYSCommandHeader.CATSdwSTHRestoreHdr.Help="在“临时隐藏”之后恢复显示";
DYSCommandHeader.CATSdwSTHRestoreHdr.ShortHelp="恢复显示";
DYSCommandHeader.CATSdwSTHRestoreHdr.LongHelp="在“临时隐藏”之后恢复显示";

// == SectionSearch ==
DYSCommandHeader.CATSdwSectionSearchHdr.Title="截面搜索";
DYSCommandHeader.CATSdwSectionSearchHdr.Help="通过截面和属性搜索元素。";
DYSCommandHeader.CATSdwSectionSearchHdr.ShortHelp="截面搜索";
DYSCommandHeader.CATSdwSectionSearchHdr.LongHelp="通过截面和属性搜索元素。";

// == UpdateSectionContainer ==
DYSCommandHeader.CATSdwUpdateSectionContHdr.Title="更新截面容器";
DYSCommandHeader.CATSdwUpdateSectionContHdr.Help="更新所有截面容器。";
DYSCommandHeader.CATSdwUpdateSectionContHdr.ShortHelp="更新截面容器";
DYSCommandHeader.CATSdwUpdateSectionContHdr.LongHelp="搜索截面容器并对它们进行更新。";

// Temporaire BMW Section Management (cf. GSMUI CATGuiCheckHeader CATSdwSectionComponentsFilter )
DYSCommandHeader.2DSectionComponentsFilter.Title="过滤截面部件";
DYSCommandHeader.2DSectionComponentsFilter.Help="过滤结构树中截面的部件。";
DYSCommandHeader.2DSectionComponentsFilter.ShortHelp="过滤截面的部件";
DYSCommandHeader.2DSectionComponentsFilter.LongHelp="过滤结构树中截面的部件。";

DYSCommandHeader.2DArc1Tgt.Title="相切弧";
DYSCommandHeader.2DArc1Tgt.Help="创建相切弧";
DYSCommandHeader.2DArc1Tgt.ShortHelp="相切弧";
DYSCommandHeader.2DArc1Tgt.LongHelp="弧\n通过指定半径值创建相切弧。";

DYSCommandHeader.2DText.Title="文本";
DYSCommandHeader.2DText.Help="创建文本";
DYSCommandHeader.2DText.ShortHelp="文本";
DYSCommandHeader.2DText.LongHelp="创建文本。";

// Tangency and curvature for control point
DYSCommandHeader.2DAddControlPointTangencyCurv.Title="添加相切曲率";
DYSCommandHeader.2DAddControlPointTangencyCurv.Help="插入具有默认相切和曲率半径的控制点。";
DYSCommandHeader.2DAddControlPointTangencyCurv.ShortHelp="插入具有默认相切和曲率的控制点。";
DYSCommandHeader.2DAddControlPointTangencyCurv.LongHelp="插入具有默认相切和曲率的控制点。可通过各自的操纵器更改相切和曲率的值。";

DYSCommandHeader.2DEditControlPointTangencyCurv.Title="编辑相切和曲率";
DYSCommandHeader.2DEditControlPointTangencyCurv.Help="编辑控制点上的相切和曲率。";
DYSCommandHeader.2DEditControlPointTangencyCurv.ShortHelp="编辑控制点上的相切和曲率。";
DYSCommandHeader.2DEditControlPointTangencyCurv.LongHelp="编辑控制点上的相切和曲率。 可通过各自的操纵器更改相切和曲率的值。";

//Construction Element
DYSCommandHeader.2DEditConstructionElement.Title="构造元素";
DYSCommandHeader.2DEditConstructionElement.Help="转换为构造元素。";
DYSCommandHeader.2DEditConstructionElement.ShortHelp="转换为构造元素。";
DYSCommandHeader.2DEditConstructionElement.LongHelp="转换为构造元素。";

//Geometrical Element
DYSCommandHeader.2DEditGeometricalElement.Title="几何元素";
DYSCommandHeader.2DEditGeometricalElement.Help="转换为几何元素。";
DYSCommandHeader.2DEditGeometricalElement.ShortHelp="转换为几何元素。";
DYSCommandHeader.2DEditGeometricalElement.LongHelp="转换为几何元素。";

// 2D Output feature axis
//DYSCommandHeader.2DOutputAxis.Title="Output axis";
DYSCommandHeader.2DOutputAxis.Title="3D 轴";
DYSCommandHeader.2DOutputAxis.Help="创建轴特征。";
DYSCommandHeader.2DOutputAxis.ShortHelp="3D 轴 ";
DYSCommandHeader.2DOutputAxis.LongHelp="3D 轴\n通过选择 2D 点创建轴特征。";

// 2D Output feature plane
//DYSCommandHeader.2DOutputPlane.Title="Output plane";
DYSCommandHeader.2DOutputPlane.Title="3D 平面";
DYSCommandHeader.2DOutputPlane.Help="创建平面特征。";
DYSCommandHeader.2DOutputPlane.ShortHelp="3D 平面";
DYSCommandHeader.2DOutputPlane.LongHelp="3D 平面\n通过选择 2D 线创建平面特征。";

// Isolation of shared points - 2DIsolateSharedPoints
DYSCommandHeader.2DIsolateSharedPoints.Title="隔离点";
DYSCommandHeader.2DIsolateSharedPoints.Help="隔离选定点。";
DYSCommandHeader.2DIsolateSharedPoints.ShortHelp="隔离选定点。";
DYSCommandHeader.2DIsolateSharedPoints.LongHelp="隔离选定点。";

// Catalog
DYSCommandHeader.2DGenerateCatalog.Title="生成目录";
DYSCommandHeader.2DGenerateCatalog.Help="从固联创建目录";
DYSCommandHeader.2DGenerateCatalog.ShortHelp="生成目录";
DYSCommandHeader.2DGenerateCatalog.LongHelp="生成目录\n从零件的固联创建新的目录。";

DYSCommandHeader.2DInstantiateFixTogether.Title="实例化固联";
DYSCommandHeader.2DInstantiateFixTogether.Help="创建固联实例";
DYSCommandHeader.2DInstantiateFixTogether.ShortHelp="实例化固联";
DYSCommandHeader.2DInstantiateFixTogether.LongHelp="实例化固联\n从现有固联创建固联实例。";

// Exit
DYSCommandHeader.UnstackSketcher.Title="退出应用程序";
DYSCommandHeader.UnstackSketcher.Help="退出此应用程序以返回至命令";
DYSCommandHeader.UnstackSketcher.ShortHelp="退出应用程序";
DYSCommandHeader.UnstackSketcher.LongHelp="
退出当前应用程序，并返回至
未决命令
";

// Align Points command
DYSCommandHeader.2DAlignPoints.Title="对齐点";
DYSCommandHeader.2DAlignPoints.Help="对齐一组点。";
DYSCommandHeader.2DAlignPoints.ShortHelp="对齐点";
DYSCommandHeader.2DAlignPoints.LongHelp="对齐点命令\n对齐一组点。";

// edges selection 
DYSCommandHeader.2DSelectionEdges.Title="选择边界边线";
DYSCommandHeader.2DSelectionEdges.Help="选择面的边界边线";
DYSCommandHeader.2DSelectionEdges.ShortHelp="选择面的边界边线";
DYSCommandHeader.2DSelectionEdges.LongHelp="选择选定面的边界边线。";

// edges selection 
DYSCommandHeader.2DSelectionAxis.Title="选择轴";
DYSCommandHeader.2DSelectionAxis.Help="选择圆柱面的轴";
DYSCommandHeader.2DSelectionAxis.ShortHelp="选择面轴";
DYSCommandHeader.2DSelectionAxis.LongHelp="选择圆柱面的轴。";


// 2d Polygon
DYSCommandHeader.2DPolygon.Title="多边形";
DYSCommandHeader.2DPolygon.Help="通过定义内/外接圆半径和边数来创建多边形";
DYSCommandHeader.2DPolygon.ShortHelp="多边形";
DYSCommandHeader.2DPolygon.LongHelp="通过定义内/外接圆半径和边数来创建多边形。";

// Profile Offset
DYSCommandHeader.2DEditProfileOffset.Title="定义轮廓偏移...";
DYSCommandHeader.2DEditProfileOffset.Help="编辑轮廓偏移";
DYSCommandHeader.2DEditProfileOffset.ShortHelp="定义轮廓偏移";
DYSCommandHeader.2DEditProfileOffset.LongHelp="编辑轮廓偏移。";
